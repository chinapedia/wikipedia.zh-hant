《**Cartoon
KAT-TUN**》（）是一個由[日本電視台製作播放的](../Page/日本電視台.md "wikilink")[綜藝電視節目](../Page/綜藝節目.md "wikilink")，播放期為2007年4月4日至2010年3月24日之間、[日本當地時間每週日的](../Page/日本標準時間.md "wikilink")23:55至0:26。該節目是由男子偶像團體[KAT-TUN主持](../Page/KAT-TUN.md "wikilink")，是KAT-TUN諸多冠名電視節目之一。

## 演出者

  - [KAT-TUN](../Page/KAT-TUN.md "wikilink")
      - [龜梨和也](../Page/龜梨和也.md "wikilink")
      - [赤西仁](../Page/赤西仁.md "wikilink")
      - [田口淳之介](../Page/田口淳之介.md "wikilink")
      - [田中聖](../Page/田中聖.md "wikilink")
      - [上田龍也](../Page/上田龍也.md "wikilink")
      - [中丸雄一](../Page/中丸雄一.md "wikilink")
  - [伊藤政道](../Page/伊藤政道.md "wikilink")（旁白，第108集起）
  - [池田鐵洋](../Page/池田鐵洋.md "wikilink")（第61至107集）
  - [山本Shoo](../Page/山本Shoo.md "wikilink")（，旁白，至第60集為止）
  - [谷口節](../Page/谷口節.md "wikilink")（旁白，第61集起）

## 過去節目內容

### 100Q

  - 以[CG方式呈現事先詢問來賓的](../Page/CG.md "wikilink")100個問題，據此作為談話內容。

### DAT-TUN5

  - [KAT-TUN以射飛鏢的方式與來賓對決](../Page/KAT-TUN.md "wikilink")。在五乘五的方格中，先射中五個連成直線或斜線的方格的一方，即為勝者。
  - 如果來賓勝利，可獲得節目特製飛鏢；反之落敗的話，則沒收來賓的飛鏢。
  - 第5集與[Avril
    Lavigne對決時](../Page/Avril_Lavigne.md "wikilink")，特別修改規則為先射中3C（即中心點）的隊伍為勝。

## 來賓

### 第1章

#### 2007年

<table>
<thead>
<tr class="header">
<th><p>|集數</p></th>
<th><p>|日期</p></th>
<th><p>|來賓</p></th>
<th><p>|備註</p></th>
<th><p>|收視率</p></th>
<th><p>|片尾曲</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p>4月4日</p></td>
<td><p>100Q：<a href="../Page/AI.md" title="wikilink">AI</a></p></td>
<td></td>
<td><p>2.9%</p></td>
<td><p>Girlfriend （<a href="../Page/Avril_Lavigne.md" title="wikilink">Avril Lavigne</a>）</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>4月11日</p></td>
<td><p>100Q：<a href="../Page/森泉.md" title="wikilink">森泉</a><br />
DAT-TUN5：<a href="../Page/武蔵.md" title="wikilink">武蔵</a> 敗</p></td>
<td></td>
<td><p>4.2%</p></td>
<td><p>Girlfriend （<a href="../Page/Avril_Lavigne.md" title="wikilink">Avril Lavigne</a>）</p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p>4月18日</p></td>
<td><p>100Q：<a href="../Page/SEAMO.md" title="wikilink">SEAMO</a><br />
DAT-TUN5：<a href="../Page/堤下敦.md" title="wikilink">堤下敦</a> 敗</p></td>
<td></td>
<td><p>5.9%</p></td>
<td><p>Girlfriend （<a href="../Page/Avril_Lavigne.md" title="wikilink">Avril Lavigne</a>）</p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p>4月25日</p></td>
<td><p>100Q：<a href="../Page/所英男.md" title="wikilink">所英男</a><br />
DAT-TUN5：<a href="../Page/武田修宏.md" title="wikilink">武田修宏</a> 勝</p></td>
<td></td>
<td><p>4.8%</p></td>
<td><p>Peak（<a href="../Page/KAT-TUN.md" title="wikilink">KAT-TUN</a>）</p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p>5月2日</p></td>
<td><p>100Q：<a href="../Page/關根麻里.md" title="wikilink">關根麻里</a><br />
DAT-TUN5：<a href="../Page/Avril_Lavigne.md" title="wikilink">Avril Lavigne</a> 敗</p></td>
<td></td>
<td><p>6.9%</p></td>
<td><p>Girlfriend （<a href="../Page/Avril_Lavigne.md" title="wikilink">Avril Lavigne</a>）</p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p>5月9日</p></td>
<td><p>100Q：<a href="../Page/mihimaru_GT.md" title="wikilink">mihimaru GT</a><br />
DAT-TUN5：<a href="../Page/所喬治.md" title="wikilink">所喬治</a></p></td>
<td><p><a href="../Page/赤西仁.md" title="wikilink">赤西仁回國後初演出</a></p></td>
<td><p>5.1%</p></td>
<td><p>Jumpin' up（<a href="../Page/KAT-TUN.md" title="wikilink">KAT-TUN</a>）</p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p>5月16日</p></td>
<td><p>100Q：日本電視台・ベレーザ（<a href="../Page/澤穂稀.md" title="wikilink">澤穂稀</a>、<a href="../Page/荒川恵理子.md" title="wikilink">荒川恵理子</a>、<a href="../Page/岩清水梓.md" title="wikilink">岩清水梓</a>）<br />
DAT-TUN5：<a href="../Page/所喬治.md" title="wikilink">所喬治</a> 敗</p></td>
<td></td>
<td><p>4.0%</p></td>
<td><p>Jumpin' up（<a href="../Page/KAT-TUN.md" title="wikilink">KAT-TUN</a>）</p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p>5月23日</p></td>
<td><p>100Q：<a href="../Page/Crystal_Kay.md" title="wikilink">Crystal Kay</a><br />
DAT-TUN5：<a href="../Page/MEGUMI.md" title="wikilink">MEGUMI</a> 敗</p></td>
<td></td>
<td><p>4.8%</p></td>
<td><p>Peak（<a href="../Page/KAT-TUN.md" title="wikilink">KAT-TUN</a>）</p></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p>5月30日</p></td>
<td><p>100Q：<a href="../Page/真矢美季.md" title="wikilink">真矢美季</a></p></td>
<td></td>
<td><p>4.8%</p></td>
<td><p>Girlfriend （<a href="../Page/Avril_Lavigne.md" title="wikilink">Avril Lavigne</a>）</p></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p>6月6日</p></td>
<td><p>DAT-TUN5：<a href="../Page/真矢美季.md" title="wikilink">真矢美季</a> 勝</p></td>
<td><p>微笑運動會</p></td>
<td><p>4.7%</p></td>
<td><p><a href="../Page/喜びの歌.md" title="wikilink">喜びの歌</a>（<a href="../Page/KAT-TUN.md" title="wikilink">KAT-TUN</a>）</p></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p>6月13日</p></td>
<td><p>100Q：<a href="../Page/ベッキー.md" title="wikilink">ベッキー</a>（<a href="../Page/Becky.md" title="wikilink">Becky</a>）</p></td>
<td><p>MINI STAGE：Your side</p></td>
<td><p>4.9%</p></td>
<td><p>Girlfriend （<a href="../Page/Avril_Lavigne.md" title="wikilink">Avril Lavigne</a>）</p></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td><p>6月20日</p></td>
<td><p>100Q：<a href="../Page/阿部サダヲ.md" title="wikilink">阿部サダヲ</a></p></td>
<td><p>微笑運動會</p></td>
<td><p>6.3%</p></td>
<td><p><a href="../Page/喜びの歌.md" title="wikilink">喜びの歌</a>（<a href="../Page/KAT-TUN.md" title="wikilink">KAT-TUN</a>）</p></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td><p>6月27日</p></td>
<td><p>100Q：<a href="../Page/高田延彥.md" title="wikilink">高田延彥</a><br />
交換危險禮物：<a href="../Page/Ne-Yo.md" title="wikilink">Ne-Yo</a></p></td>
<td></td>
<td><p>4.1%</p></td>
<td><p><a href="../Page/喜びの歌.md" title="wikilink">喜びの歌</a>（<a href="../Page/KAT-TUN.md" title="wikilink">KAT-TUN</a>）</p></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td><p>7月4日</p></td>
<td><p>100Q：<a href="../Page/小池榮子.md" title="wikilink">小池榮子</a><br />
DAT-TUN5：<a href="../Page/佐佐木主浩.md" title="wikilink">佐佐木主浩</a> 敗</p></td>
<td></td>
<td><p>4.9%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td><p>7月11日</p></td>
<td><p>100Q：<a href="../Page/MCU.md" title="wikilink">MCU</a><br />
DAT-TUN：<a href="../Page/片岡安祐美.md" title="wikilink">片岡安祐美</a> 敗</p></td>
<td></td>
<td><p>4.0%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>16</p></td>
<td><p>7月18日</p></td>
<td><p>100Q：<a href="../Page/松下奈緒.md" title="wikilink">松下奈緒</a><br />
交換危險禮物：<a href="../Page/Rihanna.md" title="wikilink">Rihanna</a></p></td>
<td></td>
<td><p>4.7%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>17</p></td>
<td><p>7月25日</p></td>
<td><p>100Q：<a href="../Page/高島千紗子.md" title="wikilink">高島千紗子</a></p></td>
<td><p>微笑運動會</p></td>
<td><p>3.4%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>18</p></td>
<td><p>8月1日</p></td>
<td><p>100Q：<a href="../Page/辣妹曾根.md" title="wikilink">辣妹曾根</a><br />
乒乓球大賽：<a href="../Page/四元奈生美.md" title="wikilink">四元奈生美</a></p></td>
<td></td>
<td><p>5.3%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>19</p></td>
<td><p>8月8日</p></td>
<td><p>100Q：<a href="../Page/千原兄弟.md" title="wikilink">千原兄弟</a></p></td>
<td></td>
<td><p>4.6%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>20</p></td>
<td><p>8月15日</p></td>
<td><p>100Q：<a href="../Page/哀川翔.md" title="wikilink">哀川翔</a></p></td>
<td><p><a href="../Page/KAT-TUN.md" title="wikilink">KAT-TUN</a> Collection：<a href="../Page/GOLD.md" title="wikilink">GOLD</a></p></td>
<td><p>5.0%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>21</p></td>
<td><p>8月22日</p></td>
<td><p>100Q：<a href="../Page/西山茉希.md" title="wikilink">西山茉希</a><br />
♥disc：<a href="../Page/Sowelu.md" title="wikilink">Sowelu</a></p></td>
<td></td>
<td><p>5.0%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>22</p></td>
<td><p>8月29日</p></td>
<td><p><a href="../Page/所喬治.md" title="wikilink">所喬治</a></p></td>
<td><p>沖繩旅行（所喬治別墅）</p></td>
<td><p>7.4%</p></td>
<td><p>淚光閃閃</p></td>
</tr>
<tr class="odd">
<td><p>23</p></td>
<td><p>9月5日</p></td>
<td><p><a href="../Page/所喬治.md" title="wikilink">所喬治</a></p></td>
<td><p>沖繩旅行前篇（2人1組）</p></td>
<td><p>5.5%</p></td>
<td><p>淚光閃閃</p></td>
</tr>
<tr class="even">
<td><p>24</p></td>
<td><p>9月12日</p></td>
<td><p>100Q：<a href="../Page/Leah_Dizon.md" title="wikilink">Leah Dizon</a></p></td>
<td><p>沖繩旅行後篇（2人1組）</p></td>
<td><p>5.4%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>25</p></td>
<td><p>9月19日</p></td>
<td><p>100Q：<a href="../Page/柳原可奈子.md" title="wikilink">柳原可奈子</a><br />
羽毛球大決戰：<a href="../Page/Leah_Dizon.md" title="wikilink">Leah Dizon</a></p></td>
<td></td>
<td><p>6.5%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>26</p></td>
<td><p>9月26日</p></td>
<td><p>沖繩旅行（未公開映像）</p></td>
<td></td>
<td><p>4.8%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>27</p></td>
<td><p>10月3日</p></td>
<td><p>100Q：<a href="../Page/古田新太.md" title="wikilink">古田新太</a>（各種各樣Half&amp;Half）<br />
♥disc：<a href="../Page/徳永英明.md" title="wikilink">徳永英明</a></p></td>
<td></td>
<td><p>4.2%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>28</p></td>
<td><p>10月10日</p></td>
<td><p>100Q：<a href="../Page/KREVA.md" title="wikilink">KREVA</a>（焦躁檢定、戀愛選擇、各種各樣Half&amp;Half）<br />
♥disc：<a href="../Page/TRF.md" title="wikilink">TRF</a></p></td>
<td><p>有閑俱樂部預告</p></td>
<td><p>2.9%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>29</p></td>
<td><p>10月17日</p></td>
<td><p>100Q：<a href="../Page/美波.md" title="wikilink">美波</a>（各種各樣Half&amp;Half）、<a href="../Page/片岡鶴太郎.md" title="wikilink">片岡鶴太郎</a><br />
♥disc：<a href="../Page/上木彩矢.md" title="wikilink">上木彩矢</a></p></td>
<td></td>
<td><p>4.5%</p></td>
<td><p><a href="../Page/Keep_the_faith.md" title="wikilink">Keep the faith</a>（<a href="../Page/KAT-TUN.md" title="wikilink">KAT-TUN</a>）</p></td>
</tr>
<tr class="even">
<td><p>30</p></td>
<td><p>10月24日</p></td>
<td><p>100Q：<a href="../Page/山本KID徳郁.md" title="wikilink">山本KID徳郁</a>（各種各樣Half&amp;Half）</p></td>
<td></td>
<td><p>4.9%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>31</p></td>
<td><p>10月31日</p></td>
<td><p>100Q：<a href="../Page/堀北真希.md" title="wikilink">堀北真希</a></p></td>
<td></td>
<td><p>4.2%</p></td>
<td><p><a href="../Page/Keep_the_faith.md" title="wikilink">Keep the faith</a>（<a href="../Page/KAT-TUN.md" title="wikilink">KAT-TUN</a>）</p></td>
</tr>
<tr class="even">
<td><p>32</p></td>
<td><p>11月7日</p></td>
<td><p>100Q：<a href="../Page/堀北真希.md" title="wikilink">堀北真希</a>（料理對決）</p></td>
<td></td>
<td><p>5.3%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>33</p></td>
<td><p>11月14日</p></td>
<td><p>100Q：<a href="../Page/Chara.md" title="wikilink">Chara</a></p></td>
<td><p><a href="../Page/KAT-TUN.md" title="wikilink">KAT-TUN</a> Collection：<a href="../Page/Best_of_KAT-TUN#收錄曲.md" title="wikilink">ハルカナ約束</a></p></td>
<td><p>3.4%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>34</p></td>
<td><p>11月21日</p></td>
<td><p>100Q：<a href="../Page/長谷川潤.md" title="wikilink">長谷川潤</a>、<a href="../Page/Elii_Rose.md" title="wikilink">Elii Rose</a>、<a href="../Page/沙耶.md" title="wikilink">沙耶</a>（各種各樣Half&amp;Half）</p></td>
<td><p>新曲<a href="../Page/Keep_the_faith.md" title="wikilink">Keep the faith披露</a></p></td>
<td><p>4.9%</p></td>
<td><p><a href="../Page/Keep_the_faith.md" title="wikilink">Keep the faith</a>(<a href="../Page/KAT-TUN.md" title="wikilink">KAT-TUN</a>)</p></td>
</tr>
<tr class="odd">
<td><p>35</p></td>
<td><p>11月28日</p></td>
<td><p>100Q：<a href="../Page/hyde.md" title="wikilink">hyde</a>（各種各樣Half&amp;Half）、 <a href="../Page/ken.md" title="wikilink">ken</a>（<a href="../Page/L&#39;Arc～en～Ciel.md" title="wikilink">L'Arc～en～Ciel</a>）（繪畫接龍）<br />
Cartoon town club：<a href="../Page/Crystal_Kay.md" title="wikilink">Crystal Kay</a></p></td>
<td></td>
<td><p>4.1%</p></td>
<td><p><a href="../Page/Keep_the_faith.md" title="wikilink">Keep the faith</a>（<a href="../Page/KAT-TUN.md" title="wikilink">KAT-TUN</a>）</p></td>
</tr>
<tr class="even">
<td><p>36</p></td>
<td><p>12月5日</p></td>
<td><p>100Q：<a href="../Page/DJ_OZMA.md" title="wikilink">DJ OZMA</a>（各種各樣Half&amp;Half）</p></td>
<td></td>
<td><p>4.5%</p></td>
<td><p>TOKYO BOOGIE BACK（DJ OZMA）</p></td>
</tr>
<tr class="odd">
<td><p>37</p></td>
<td><p>12月12日</p></td>
<td><p>100Q：<a href="../Page/Mie.md" title="wikilink">Mie</a><br />
♥disc：<a href="../Page/中村中.md" title="wikilink">中村中</a></p></td>
<td></td>
<td><p>2.6%</p></td>
<td><p><a href="../Page/Keep_the_faith.md" title="wikilink">Keep the faith</a>(<a href="../Page/KAT-TUN.md" title="wikilink">KAT-TUN</a>)</p></td>
</tr>
<tr class="even">
<td><p>38</p></td>
<td><p>12月19日</p></td>
<td><p>100Q：<a href="../Page/陣内孝則.md" title="wikilink">陣内孝則</a><br />
交換危險禮物：<a href="../Page/Aly_&amp;_AJ.md" title="wikilink">Aly &amp; AJ</a></p></td>
<td></td>
<td><p>2.7%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>39</p></td>
<td><p>12月26日</p></td>
<td><p>總集編</p></td>
<td><p>留言演出：<a href="../Page/Becky.md" title="wikilink">Becky</a>、<a href="../Page/ギャル曽根.md" title="wikilink">ギャル曽根</a>、<a href="../Page/柳原可奈子.md" title="wikilink">柳原可奈子</a></p></td>
<td><p>4.4%</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### 2008年

<table>
<thead>
<tr class="header">
<th><p>|集數</p></th>
<th><p>|日期</p></th>
<th><p>|來賓</p></th>
<th><p>|備註</p></th>
<th><p>|收視率</p></th>
<th><p>|片尾曲</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>40</p></td>
<td><p>1月9日</p></td>
<td><p>100Q：<a href="../Page/戶田惠梨香.md" title="wikilink">戶田惠梨香</a></p></td>
<td></td>
<td><p>4.8%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>41</p></td>
<td><p>1月16日</p></td>
<td><p>♥disc：<a href="../Page/ET-KING.md" title="wikilink">ET-KING</a></p></td>
<td><p>特備節目未公開放送</p></td>
<td><p>6.4%</p></td>
<td><p><a href="../Page/LIPS.md" title="wikilink">LIPS</a>（<a href="../Page/KAT-TUN.md" title="wikilink">KAT-TUN</a>）</p></td>
</tr>
<tr class="odd">
<td><p>42</p></td>
<td><p>1月23日</p></td>
<td><p>100Q：<a href="../Page/F-BLOOD.md" title="wikilink">F-BLOOD</a><br />
交換危險禮物：<a href="../Page/艾麗西亞·基斯.md" title="wikilink">艾麗西亞·基斯</a></p></td>
<td></td>
<td><p>4.4%</p></td>
<td><p>Girlfriend （<a href="../Page/Avril_Lavigne.md" title="wikilink">Avril Lavigne</a>）</p></td>
</tr>
<tr class="even">
<td><p>43</p></td>
<td><p>1月30日</p></td>
<td><p>100Q：<a href="../Page/中川翔子.md" title="wikilink">中川翔子</a></p></td>
<td></td>
<td><p>6.2%</p></td>
<td><p><a href="../Page/LIPS.md" title="wikilink">LIPS</a>（<a href="../Page/KAT-TUN.md" title="wikilink">KAT-TUN</a>）</p></td>
</tr>
<tr class="odd">
<td><p>44</p></td>
<td><p>2月6日</p></td>
<td><p>100Q：<a href="../Page/土岐田麗子.md" title="wikilink">土岐田麗子</a>、<a href="../Page/山里亮太.md" title="wikilink">山里亮太</a></p></td>
<td></td>
<td><p>5.6%</p></td>
<td><p><a href="../Page/LIPS.md" title="wikilink">LIPS</a>(<a href="../Page/KAT-TUN.md" title="wikilink">KAT-TUN</a>)</p></td>
</tr>
<tr class="even">
<td><p>45</p></td>
<td><p>2月13日</p></td>
<td><p>100Q：<a href="../Page/m-flo.md" title="wikilink">m-flo</a></p></td>
<td></td>
<td><p>3.2%</p></td>
<td><p><a href="../Page/LIPS.md" title="wikilink">LIPS</a>（<a href="../Page/KAT-TUN.md" title="wikilink">KAT-TUN</a>）</p></td>
</tr>
<tr class="odd">
<td><p>46</p></td>
<td><p>2月20日</p></td>
<td><p>100Q：<a href="../Page/高杉里美.md" title="wikilink">高杉里美</a></p></td>
<td></td>
<td><p>4.7%</p></td>
<td><p><a href="../Page/LIPS.md" title="wikilink">LIPS</a>（<a href="../Page/KAT-TUN.md" title="wikilink">KAT-TUN</a>）</p></td>
</tr>
<tr class="even">
<td><p>47</p></td>
<td><p>2月27日</p></td>
<td><p>100Q：<a href="../Page/Every_Little_Thing.md" title="wikilink">Every Little Thing</a></p></td>
<td></td>
<td><p>5.4%</p></td>
<td><p>Girlfriend （<a href="../Page/Avril_Lavigne.md" title="wikilink">Avril Lavigne</a>）</p></td>
</tr>
<tr class="odd">
<td><p>48</p></td>
<td><p>3月5日</p></td>
<td><p>100Q：<a href="../Page/熊田曜子.md" title="wikilink">熊田曜子</a>、<a href="../Page/夏川純.md" title="wikilink">夏川純</a>、<a href="../Page/安田美沙子.md" title="wikilink">安田美沙子</a></p></td>
<td></td>
<td><p>5.6%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>49</p></td>
<td><p>3月12日</p></td>
<td><p><a href="../Page/MEGUMI.md" title="wikilink">MEGUMI</a></p></td>
<td><p>赤西&amp;中丸懲罰遊戲</p></td>
<td><p>北海道敞篷車之旅</p></td>
<td><p>5.9%</p></td>
</tr>
<tr class="odd">
<td><p>50</p></td>
<td><p>3月19日</p></td>
<td><p><a href="../Page/MEGUMI.md" title="wikilink">MEGUMI</a></p></td>
<td><p>赤西&amp;中丸懲罰遊戲</p></td>
<td><p>北海道敞篷車之旅完結編</p></td>
<td><p>5.8%</p></td>
</tr>
<tr class="even">
<td><p>51</p></td>
<td><p>3月26日</p></td>
<td></td>
<td><p>今夜解禁</p></td>
<td><p>未公開&amp;100Q總集特別篇</p></td>
<td><p>4.4%</p></td>
</tr>
<tr class="odd">
<td><p>52</p></td>
<td><p>4月2日</p></td>
<td><p>100Q：<a href="../Page/宮迫博之.md" title="wikilink">宮迫博之</a>、<a href="../Page/佐藤江梨子.md" title="wikilink">佐藤江梨子</a></p></td>
<td></td>
<td><p>4.8%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>53</p></td>
<td><p>4月9日</p></td>
<td></td>
<td><p>第１回「其實我○○</p></td>
<td><p>」頒獎禮</p></td>
<td><p>4.7%</p></td>
</tr>
<tr class="odd">
<td><p>54</p></td>
<td><p>4月16日</p></td>
<td></td>
<td><p>1週年紀念外景拍攝：沒有劇本！沒有計劃的旅行</p></td>
<td><p>4.3%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>55</p></td>
<td><p>4月23日</p></td>
<td></td>
<td><p>1週年紀念外景拍攝：沒有劇本！沒有計劃的旅行　第二彈</p></td>
<td><p>4.5%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>56</p></td>
<td><p>4月30日</p></td>
<td><p><a href="../Page/岡政偉.md" title="wikilink">岡政偉</a>、ハイキングウォーキング</p></td>
<td></td>
<td><p>3.8%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>57</p></td>
<td><p>5月7日</p></td>
<td><p><a href="../Page/友近.md" title="wikilink">友近</a></p></td>
<td><p>友近和<a href="../Page/KAT-TUN.md" title="wikilink">KAT-TUN即興戲劇表演挑戰</a></p></td>
<td><p>5.5%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>58</p></td>
<td><p>5月14日</p></td>
<td><p><a href="../Page/綾瀨遙.md" title="wikilink">綾瀨遙</a></p></td>
<td><p>新曲<a href="../Page/DON&#39;T_U_EVER_STOP.md" title="wikilink">DON'T U EVER STOP披露</a></p></td>
<td><p>5.6%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>59</p></td>
<td><p>5月21日</p></td>
<td><p><a href="../Page/綾瀨遙.md" title="wikilink">綾瀨遙</a></p></td>
<td><p>與嘉賓雜樣煎菜餅對決 龜梨和上田懲罰遊戲決定</p></td>
<td><p>5.0%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>60</p></td>
<td><p>5月28日</p></td>
<td><p><a href="../Page/黑色美乃滋.md" title="wikilink">黑色美乃滋</a></p></td>
<td></td>
<td><p>5.3%</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 第2章

#### 2008年

<table>
<thead>
<tr class="header">
<th><p>|集數</p></th>
<th><p>|日期</p></th>
<th><p>|來賓</p></th>
<th><p>|備註</p></th>
<th><p>|收視率</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>61</p></td>
<td><p>6月4日</p></td>
<td><p>赤西：川島カヨ、塩村文夏、長束あこ<br />
田中：みうらじゅん<br />
中丸：<a href="../Page/杉本彩.md" title="wikilink">杉本彩</a></p></td>
<td><p>《第２章始動　愛是什麼？》<br />
龜梨：在<a href="../Page/愛知縣.md" title="wikilink">愛知縣</a><a href="../Page/吉良町.md" title="wikilink">吉良町的外景拍攝</a><br />
田口：三行情書<br />
上田：練習中</p></td>
<td><p>4.1%</p></td>
</tr>
<tr class="even">
<td><p>62</p></td>
<td><p>6月11日</p></td>
<td><p>赤西：<a href="../Page/Leah_Dizon.md" title="wikilink">Leah Dizon</a><br />
田中：みうらじゅん<br />
中丸：<a href="../Page/杉本彩.md" title="wikilink">杉本彩</a></p></td>
<td><p>龜梨：在糧食店先生的外景拍攝<br />
田口：三行情書<br />
上田：大正時代的戀愛諮詢</p></td>
<td><p>3.3%</p></td>
</tr>
<tr class="odd">
<td><p>63</p></td>
<td><p>6月18日</p></td>
<td><p>赤西：<a href="../Page/Leah_Dizon.md" title="wikilink">Leah Dizon</a><br />
田中：酒缶<br />
中丸：山本モナ</p></td>
<td><p>龜梨：在<a href="../Page/山形縣.md" title="wikilink">山形縣</a><a href="../Page/高畠町.md" title="wikilink">高畠町的外景拍攝</a><br />
田口：魔術披露</p></td>
<td><p>2.9%</p></td>
</tr>
<tr class="even">
<td><p>64</p></td>
<td><p>6月25日</p></td>
<td><p>赤西：<a href="../Page/Leah_Dizon.md" title="wikilink">Leah Dizon</a><br />
田中：柴田英嗣<br />
中丸：山本モナ</p></td>
<td></td>
<td><p>3.4%</p></td>
</tr>
<tr class="odd">
<td><p>65</p></td>
<td><p>7月2日</p></td>
<td><p>赤西：安藤成子、深澤ゆうき、大石紗子</p></td>
<td><p>田口：三行情書<br />
上田：福島縣的女孩戀愛諮詢</p></td>
<td><p>4.0%</p></td>
</tr>
<tr class="even">
<td><p>66</p></td>
<td><p>7月9日</p></td>
<td><p>亀梨：<a href="../Page/Usher.md" title="wikilink">Usher</a><br />
中丸：国生さゆり</p></td>
<td><p>在杜之都·<a href="../Page/仙台.md" title="wikilink">仙台的旅行</a><br />
田口：三行情書</p></td>
<td><p>3.7%</p></td>
</tr>
<tr class="odd">
<td><p>67</p></td>
<td><p>7月16日</p></td>
<td></td>
<td><p>在杜之都·仙台的旅行（前篇）</p></td>
<td><p>5.3%</p></td>
</tr>
<tr class="even">
<td><p>68</p></td>
<td><p>7月23日</p></td>
<td></td>
<td><p>在杜之都·仙台的旅行（後篇）</p></td>
<td><p>5.3%</p></td>
</tr>
<tr class="odd">
<td><p>69</p></td>
<td><p>7月30日</p></td>
<td><p>赤西・田中：<a href="../Page/中川翔子.md" title="wikilink">中川翔子</a></p></td>
<td></td>
<td><p>6.3%</p></td>
</tr>
<tr class="even">
<td><p>70</p></td>
<td><p>8月6日</p></td>
<td><p>亀梨・赤西・田口・中丸：<a href="../Page/PUFFY.md" title="wikilink">PUFFY</a></p></td>
<td></td>
<td><p>4.4%</p></td>
</tr>
<tr class="odd">
<td><p>71</p></td>
<td><p>8月13日</p></td>
<td></td>
<td><p>在杜之都·仙台的旅行未公開集<br />
田口：三行情書</p></td>
<td><p>5.3%</p></td>
</tr>
<tr class="even">
<td><p>72</p></td>
<td><p>8月20日</p></td>
<td><p>亀梨・田口・上田・中丸：黛比夫人<br />
亀梨・中丸：<a href="../Page/Leona_Lewis.md" title="wikilink">Leona Lewis</a></p></td>
<td></td>
<td><p>2.3%</p></td>
</tr>
<tr class="odd">
<td><p>73</p></td>
<td><p>8月27日</p></td>
<td><p>柳澤慎吾</p></td>
<td></td>
<td><p>4.4%</p></td>
</tr>
<tr class="even">
<td><p>74</p></td>
<td><p>9月3日</p></td>
<td><p><a href="../Page/Will_Smith.md" title="wikilink">Will Smith</a><br />
赤西：<a href="../Page/Jodie_Foster.md" title="wikilink">Jodie Foster</a></p></td>
<td></td>
<td><p>4.3%</p></td>
</tr>
<tr class="odd">
<td><p>75</p></td>
<td><p>9月10日</p></td>
<td><p>亀梨：<a href="../Page/Patricia_Field.md" title="wikilink">Patricia Field</a>、<a href="../Page/大澤茜.md" title="wikilink">大澤茜</a>、<a href="../Page/陳怡_(藝人).md" title="wikilink">陳怡</a></p></td>
<td></td>
<td><p>4.7%</p></td>
</tr>
<tr class="even">
<td><p>76</p></td>
<td><p>9月17日</p></td>
<td><p><a href="../Page/友近.md" title="wikilink">友近</a></p></td>
<td></td>
<td><p>5.1%</p></td>
</tr>
<tr class="odd">
<td><p>77</p></td>
<td><p>9月24日</p></td>
<td></td>
<td><p>未公開大播放</p></td>
<td><p>3.8%</p></td>
</tr>
<tr class="even">
<td><p>78</p></td>
<td><p>10月1日</p></td>
<td><p><a href="../Page/真鍋香織.md" title="wikilink">真鍋香織</a></p></td>
<td></td>
<td><p>4.4%</p></td>
</tr>
<tr class="odd">
<td><p>79</p></td>
<td><p>10月8日</p></td>
<td><p><a href="../Page/安田美沙子.md" title="wikilink">安田美沙子</a></p></td>
<td></td>
<td><p>3.5%</p></td>
</tr>
<tr class="even">
<td><p>80</p></td>
<td><p>10月15日</p></td>
<td><p>赤西・田中・中丸：<a href="../Page/貫地谷詩穗梨.md" title="wikilink">貫地谷詩穗梨</a></p></td>
<td></td>
<td><p>5.4%</p></td>
</tr>
<tr class="odd">
<td><p>81</p></td>
<td><p>10月22日</p></td>
<td><p><a href="../Page/清水美智子.md" title="wikilink">清水美智子</a></p></td>
<td></td>
<td><p>6.0%</p></td>
</tr>
<tr class="even">
<td><p>82</p></td>
<td><p>10月29日</p></td>
<td><p>亀梨・上田・中丸：<a href="../Page/小倉優子.md" title="wikilink">小倉優子</a></p></td>
<td></td>
<td><p>4.6%</p></td>
</tr>
<tr class="odd">
<td><p>83</p></td>
<td><p>11月5日</p></td>
<td><p>ハリセンボン(針千本)</p></td>
<td></td>
<td><p>3.8%</p></td>
</tr>
<tr class="even">
<td><p>84</p></td>
<td><p>11月12日</p></td>
<td><p><a href="../Page/柳原可奈子.md" title="wikilink">柳原可奈子</a></p></td>
<td></td>
<td><p>6.6%</p></td>
</tr>
<tr class="odd">
<td><p>85</p></td>
<td><p>11月19日</p></td>
<td><p>亀梨・赤西・上田：小林劍道・綾部祐二<br />
田口・田中・中丸：小林劍道</p></td>
<td></td>
<td><p>4.4%</p></td>
</tr>
<tr class="even">
<td><p>86</p></td>
<td><p>11月26日</p></td>
<td><p><a href="../Page/春奈愛.md" title="wikilink">春奈愛</a></p></td>
<td></td>
<td><p>5.4%</p></td>
</tr>
<tr class="odd">
<td><p>87</p></td>
<td><p>12月3日</p></td>
<td><p><a href="../Page/坂井真紀.md" title="wikilink">坂井真紀</a></p></td>
<td><p>Mini Stage：<a href="../Page/White_X&#39;mas.md" title="wikilink">White X'mas</a></p></td>
<td><p>3.8%</p></td>
</tr>
<tr class="even">
<td><p>88</p></td>
<td><p>12月10日</p></td>
<td><p>灘儀武</p></td>
<td></td>
<td><p>4.4%</p></td>
</tr>
<tr class="odd">
<td><p>89</p></td>
<td><p>12月17日</p></td>
<td><p>栗田貫一</p></td>
<td></td>
<td><p>2.0%</p></td>
</tr>
<tr class="even">
<td><p>90</p></td>
<td><p>12月24日</p></td>
<td></td>
<td><p>聖誕節特別篇 登場人物圖鑑'08（總集篇&amp;未公開）<br />
Mini Stage：<a href="../Page/White_X&#39;mas.md" title="wikilink">White X'mas</a>（全）</p></td>
<td><p>5.5%</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### 2009年

<table>
<thead>
<tr class="header">
<th><p>|集數</p></th>
<th><p>|日期</p></th>
<th><p>|來賓</p></th>
<th><p>|備註</p></th>
<th><p>|收視率</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>91</p></td>
<td><p>1月7日</p></td>
<td></td>
<td><p>冬天的長野享受旅行（前篇）</p></td>
<td><p>4.7%</p></td>
</tr>
<tr class="even">
<td><p>92</p></td>
<td><p>1月14日</p></td>
<td></td>
<td><p>冬天的長野享受旅行（後篇）</p></td>
<td><p>4.7%</p></td>
</tr>
<tr class="odd">
<td><p>93</p></td>
<td><p>1月21日</p></td>
<td><p><a href="../Page/温水洋一.md" title="wikilink">温水洋一</a></p></td>
<td></td>
<td><p>5.5%</p></td>
</tr>
<tr class="even">
<td><p>94</p></td>
<td><p>1月28日</p></td>
<td><p><a href="../Page/西川史子.md" title="wikilink">西川史子</a></p></td>
<td></td>
<td><p>4.6%</p></td>
</tr>
<tr class="odd">
<td><p>95</p></td>
<td><p>2月4日</p></td>
<td><p>亀梨、上田、中丸、田中：<a href="../Page/奧黛麗_(搞笑組合).md" title="wikilink">奧黛麗</a>、<a href="../Page/南明奈.md" title="wikilink">南明奈</a><br />
赤西、田口：奧黛麗</p></td>
<td></td>
<td><p>5.6%</p></td>
</tr>
<tr class="even">
<td><p>96</p></td>
<td><p>2月11日</p></td>
<td><p>赤西、田口、中丸：石原良純、ゆうたろう<br />
亀梨、田中、上田：石原良純<br />
《<a href="../Page/神之水滴.md" title="wikilink">神之水滴</a>》宣傳嘉賓：竹中直人</p></td>
<td><p>Mini Stage：<a href="../Page/ONE_DROP.md" title="wikilink">ONE DROP</a>（全）</p></td>
<td><p>3.8%</p></td>
</tr>
<tr class="odd">
<td><p>97</p></td>
<td><p>2月18日</p></td>
<td><p>YOU、林克治</p></td>
<td></td>
<td><p>5.3%</p></td>
</tr>
<tr class="even">
<td><p>98</p></td>
<td><p>2月25日</p></td>
<td><p>羅伯特</p></td>
<td></td>
<td><p>3.9%</p></td>
</tr>
<tr class="odd">
<td><p>99</p></td>
<td><p>3月4日</p></td>
<td><p>亀梨、田口、田中、上田：羅伯特、古田新太</p></td>
<td><p>Mini Stage：<a href="../Page/RESCUE.md" title="wikilink">RESCUE</a></p></td>
<td><p>3.4%</p></td>
</tr>
<tr class="even">
<td><p>100</p></td>
<td><p>3月11日</p></td>
<td><p>はんにゃ（川島章良、金田哲）</p></td>
<td><p>特別節目</p></td>
<td><p>5.0%</p></td>
</tr>
<tr class="odd">
<td><p>101</p></td>
<td><p>3月18日</p></td>
<td><p>ベッキー（<a href="../Page/Becky.md" title="wikilink">Becky</a>）</p></td>
<td></td>
<td><p>3.9%</p></td>
</tr>
<tr class="even">
<td><p>102</p></td>
<td><p>3月25日</p></td>
<td><p>亀梨、田口、中丸：ベッキー（<a href="../Page/Becky.md" title="wikilink">Becky</a>）<br />
亀梨、赤西、田中、上田、中丸：ベッキー（<a href="../Page/Becky.md" title="wikilink">Becky</a>）、Joyman</p></td>
<td></td>
<td><p>3.9%</p></td>
</tr>
<tr class="odd">
<td><p>103</p></td>
<td><p>4月1日</p></td>
<td><p>赤西：<a href="../Page/江戶晴美.md" title="wikilink">江戶晴美</a><br />
亀梨、田口、田中、上田、中丸：<a href="../Page/江戶晴美.md" title="wikilink">江戶晴美</a>、COWCOW</p></td>
<td><p>中丸之東京、大阪巨蛋公演13日間連續高空彈跳演出決定</p></td>
<td><p>4.7%</p></td>
</tr>
<tr class="even">
<td><p>104</p></td>
<td><p>4月8日</p></td>
<td><p><a href="../Page/黑木美沙.md" title="wikilink">黑木美沙</a>、Cream（上田晉也、有田哲平）</p></td>
<td><p>4月5日播放的特別節目未公開Special</p></td>
<td><p>6.1%</p></td>
</tr>
<tr class="odd">
<td><p>105</p></td>
<td><p>4月15日</p></td>
<td><p><a href="../Page/黑木美沙.md" title="wikilink">黑木美沙</a>、Cream（上田晉也、有田哲平）、關根勤</p></td>
<td><p>4月5日播放的特別節目未公開Special<br />
下週第二章最終回預告</p></td>
<td><p>4.3%</p></td>
</tr>
<tr class="even">
<td><p>106</p></td>
<td><p>4月22日</p></td>
<td><p>桑波田理惠、<a href="../Page/友近.md" title="wikilink">友近</a></p></td>
<td><p>第二章 愛是什麼？ END</p></td>
<td><p>5.5%</p></td>
</tr>
<tr class="odd">
<td><p>107</p></td>
<td><p>4月29日</p></td>
<td></td>
<td><p>第二章 特別編</p></td>
<td><p>5.6%</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 第3章

#### 2009年

<table>
<thead>
<tr class="header">
<th><p>|集數</p></th>
<th><p>|日期</p></th>
<th><p>|來賓</p></th>
<th><p>|備註</p></th>
<th><p>|收視率</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>108</p></td>
<td><p>5月6日</p></td>
<td><p>赤西・中丸：藤田昭範<br />
田口：<a href="../Page/柳原可奈子.md" title="wikilink">柳原可奈子</a>・木暮慎二</p></td>
<td><p>《第３章始動 - ○○的規則（學習各式各樣不同類別的守則）》<br />
［馬戲團的秘密規則］探尋之旅</p></td>
<td><p>5.9%</p></td>
</tr>
<tr class="even">
<td><p>109</p></td>
<td><p>5月13日</p></td>
<td><p>亀梨・田中：秋山龍次・<a href="../Page/山本博.md" title="wikilink">山本博</a>・近藤正二郎<br />
上田：馬場裕之</p></td>
<td><p>［登山的守則１ ～ 高尾山的神秘規則］探尋之旅～米其林指南～</p></td>
<td><p>6.7%</p></td>
</tr>
<tr class="odd">
<td><p>110</p></td>
<td><p>5月20日</p></td>
<td><p>赤西・中丸：中川健<br />
田口：<a href="../Page/大澤茜.md" title="wikilink">大澤茜</a>・川口拓</p></td>
<td><p>［山梨縣的野營規則］探尋之旅</p></td>
<td><p>5.1%</p></td>
</tr>
<tr class="even">
<td><p>111</p></td>
<td><p>5月27日</p></td>
<td><p>亀梨：堤下 敦<br />
上田：板倉俊之</p></td>
<td><p>［時下愛犬人士的秘密規則］探尋之旅</p></td>
<td><p>5.6%</p></td>
</tr>
<tr class="odd">
<td><p>112</p></td>
<td><p>6月3日</p></td>
<td></td>
<td><p>東京巨蛋高空彈跳企劃特別篇～克服懼高症之旅～</p></td>
<td><p>4.5%</p></td>
</tr>
<tr class="even">
<td><p>113</p></td>
<td><p>6月10日</p></td>
<td></td>
<td><p>東京巨蛋～中丸高空彈跳企劃全紀錄篇～</p></td>
<td><p>5.2%</p></td>
</tr>
<tr class="odd">
<td><p>114</p></td>
<td><p>6月17日</p></td>
<td><p>亀梨：秋山龍次・<a href="../Page/山本博.md" title="wikilink">山本博</a>・田中家的長輩<br />
田中・上田：馬場裕之・田中家的晚輩</p></td>
<td><p>［大家族的神秘規則］探尋之旅(前篇)</p></td>
<td><p>5.5%</p></td>
</tr>
<tr class="even">
<td><p>115</p></td>
<td><p>6月24日</p></td>
<td><p>亀梨：秋山龍次・<a href="../Page/山本博.md" title="wikilink">山本博</a>・田中家的長輩<br />
田中・上田：馬場裕之・田中家的晚輩</p></td>
<td><p>［大家族的神秘規則］探尋之旅(後篇)</p></td>
<td><p>5.8%</p></td>
</tr>
<tr class="odd">
<td><p>116</p></td>
<td><p>7月1日</p></td>
<td><p>赤西・中丸：庄司智春・菅原初代<br />
田口：品川祐・菅原初代</p></td>
<td><p>［巨型饕餮美食的神秘規則］探尋之旅</p></td>
<td><p>5.7%</p></td>
</tr>
<tr class="even">
<td><p>117</p></td>
<td><p>7月8日</p></td>
<td><p>久本雅美</p></td>
<td><p>［被年長女性喜歡的規則］探尋之旅</p></td>
<td><p>4.4%</p></td>
</tr>
<tr class="odd">
<td><p>118</p></td>
<td><p>7月15日</p></td>
<td><p>赤西・田口・中丸：近藤春菜・井戶田潤</p></td>
<td><p>［超級救援隊的特別規則］探尋之旅</p></td>
<td><p>4.9%</p></td>
</tr>
<tr class="even">
<td><p>119</p></td>
<td><p>7月22日</p></td>
<td><p>亀梨：庄司智春・北村寶鈴<br />
田中・上田：板倉俊之・片柳至弘</p></td>
<td><p>［登山的守則２ ～ 御岳山的神秘規則］探尋之旅</p></td>
<td><p>5.7%</p></td>
</tr>
<tr class="odd">
<td><p>120</p></td>
<td><p>7月29日</p></td>
<td><p>赤西・中丸：羽生久美子・淺川加代子<br />
田口：<a href="../Page/柳原可奈子.md" title="wikilink">柳原可奈子</a>・羽生久美子・淺川加代子</p></td>
<td><p>［現代幼稚園的特別規則］探尋之旅</p></td>
<td><p>5.9%</p></td>
</tr>
<tr class="even">
<td><p>121</p></td>
<td><p>8月5日</p></td>
<td><p>亀梨・田中・上田：松岡修造・<a href="../Page/北陽.md" title="wikilink">北陽</a></p></td>
<td><p>［松岡修造的網球規則］探尋之旅(前篇)</p></td>
<td><p>4.9%</p></td>
</tr>
<tr class="odd">
<td><p>122</p></td>
<td><p>8月12日</p></td>
<td><p>亀梨：松岡修造・虻川美穂子・伊藤さおり<br />
田中・上田：松岡修造・伊藤さおり</p></td>
<td><p>［松岡修造的網球規則］探尋之旅(後篇)</p></td>
<td><p>4.8%</p></td>
</tr>
<tr class="even">
<td><p>123</p></td>
<td><p>8月19日</p></td>
<td><p>赤西・田口・中丸：桑波田理惠・柴崎蛾王・速水龍樹</p></td>
<td><p>［反派商會的秘密規則］探尋之旅</p></td>
<td><p>4.1%</p></td>
</tr>
<tr class="odd">
<td><p>124</p></td>
<td><p>8月26日</p></td>
<td><p>亀梨・田中・上田：羅伯特・佐佐木茂良</p></td>
<td><p>［登山的守則３ ～ 富士山的神秘規則］探尋之旅(前篇)</p></td>
<td><p>5.7%</p></td>
</tr>
<tr class="even">
<td><p>125</p></td>
<td><p>9月2日</p></td>
<td><p>亀梨・田中・上田：羅伯特・佐佐木茂良</p></td>
<td><p>［登山的守則３ ～ 富士山的神秘規則］探尋之旅(後篇)</p></td>
<td><p>5.5%</p></td>
</tr>
<tr class="odd">
<td><p>126</p></td>
<td><p>9月9日</p></td>
<td><p>赤西：武田修宏・白鳥純子<br />
田口・中丸：<a href="../Page/大澤茜.md" title="wikilink">大澤茜</a></p></td>
<td><p>［都道府縣的守則1 ～ 長崎縣的特別規則］探尋之旅(前篇)</p></td>
<td><p>3.3%</p></td>
</tr>
<tr class="even">
<td><p>127</p></td>
<td><p>9月16日</p></td>
<td><p>赤西：武田修宏・白鳥純子<br />
田口・中丸：<a href="../Page/大澤茜.md" title="wikilink">大澤茜</a></p></td>
<td><p>［都道府縣的守則1 ～ 長崎縣的特別規則］探尋之旅(後篇)</p></td>
<td><p>5.2%</p></td>
</tr>
<tr class="odd">
<td><p>128</p></td>
<td><p>9月23日</p></td>
<td></td>
<td><p>［秋天的100項特別規則連發］特別篇(前篇)</p></td>
<td><p>4.5%</p></td>
</tr>
<tr class="even">
<td><p>129</p></td>
<td><p>9月30日</p></td>
<td></td>
<td><p>［秋天的100項特別規則連發］特別篇(後篇)</p></td>
<td><p>3.3%</p></td>
</tr>
<tr class="odd">
<td><p>130</p></td>
<td><p>10月7日</p></td>
<td><p>亀梨・田中・上田：黑色美乃滋・金子哲雄</p></td>
<td><p>［超便宜巴士之旅的特別規則］探尋之旅(前篇)</p></td>
<td><p>5.1%</p></td>
</tr>
<tr class="even">
<td><p>131</p></td>
<td><p>10月14日</p></td>
<td><p>亀梨・田中・上田：黑色美乃滋・金子哲雄</p></td>
<td><p>［超便宜巴士之旅的特別規則］探尋之旅(後篇)</p></td>
<td><p>4.9%</p></td>
</tr>
<tr class="odd">
<td><p>132</p></td>
<td><p>10月21日</p></td>
<td><p>赤西：大島美幸<br />
田口：村上知子<br />
中丸：黑澤宗子</p></td>
<td><p>［森三中的特別規則］探尋之旅</p></td>
<td><p>5.5%</p></td>
</tr>
<tr class="even">
<td><p>133</p></td>
<td><p>10月28日</p></td>
<td><p>亀梨・田中・上田：藤彩子・尾山謙才</p></td>
<td><p>［週末名媛的生活規則］探尋之旅　　　　[1]</p></td>
<td><p>4.0%</p></td>
</tr>
<tr class="odd">
<td><p>134</p></td>
<td><p>11月4日</p></td>
<td><p><a href="../Page/柳原可奈子.md" title="wikilink">柳原可奈子</a>・NON STYLE・我們家</p></td>
<td><p>［絕對會想模仿的吐槽規則］特別篇(前篇)　[2]</p></td>
<td><p>3.8%</p></td>
</tr>
<tr class="even">
<td><p>135</p></td>
<td><p>11月11日</p></td>
<td><p><a href="../Page/柳原可奈子.md" title="wikilink">柳原可奈子</a>・NON STYLE・我們家</p></td>
<td><p>［絕對會想模仿的吐槽規則］特別篇(後篇)　[3]</p></td>
<td><p>4.3%</p></td>
</tr>
<tr class="odd">
<td><p>136</p></td>
<td><p>11月18日</p></td>
<td><p>赤西：大島美幸・小雀陣二・三澤慎太朗<br />
田口：黑澤宗子・小雀陣二・三澤慎太朗<br />
中丸：村上知子・小雀陣二・三澤慎太朗</p></td>
<td><p>［秋季戶外活動的露營規則］探尋之旅(前篇)</p></td>
<td><p>4.8%</p></td>
</tr>
<tr class="even">
<td><p>137</p></td>
<td><p>11月25日</p></td>
<td><p>赤西・田口・中丸：森三中・小雀陣二・三澤慎太朗</p></td>
<td><p>［秋季戶外活動的露營規則］探尋之旅(後篇)</p></td>
<td><p>4.4%</p></td>
</tr>
<tr class="odd">
<td><p>138</p></td>
<td><p>12月2日</p></td>
<td><p>亀梨・田中・上田：田村亮・兒島玲子</p></td>
<td><p>［女性特定的釣魚規則］探尋之旅</p></td>
<td><p>4.4%</p></td>
</tr>
<tr class="even">
<td><p>139</p></td>
<td><p>12月9日</p></td>
<td><p>赤西・田口・中丸：春奈愛・森荷葉</p></td>
<td><p>［受歡迎禮儀的特別規則］探尋之旅</p></td>
<td><p>4.5%</p></td>
</tr>
<tr class="odd">
<td><p>140</p></td>
<td><p>12月23日</p></td>
<td><p>羅伯特・伊藤麻子・もう中学生</p></td>
<td><p>［耶誕節豪華巴士之旅］聖誕節特別篇</p></td>
<td><p>3.8%</p></td>
</tr>
</tbody>
</table>

#### 2010年

<table>
<thead>
<tr class="header">
<th><p>|集數</p></th>
<th><p>|日期</p></th>
<th><p>|來賓</p></th>
<th><p>|備註</p></th>
<th><p>|收視率</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>141</p></td>
<td><p>1月6日</p></td>
<td><p><a href="../Page/水川麻美.md" title="wikilink">水川麻美</a>・Cream(上田晉也・有田哲平)・松子DELUXE</p></td>
<td><p>［<a href="../Page/KAT-TUN之前輩請教教我!大人的守則100!.md" title="wikilink">KAT-TUN之前輩請教教我!大人的守則100!part</a>３］未公開SP(前篇)</p></td>
<td><p>4.4%</p></td>
</tr>
<tr class="even">
<td><p>142</p></td>
<td><p>1月13日</p></td>
<td><p><a href="../Page/水川麻美.md" title="wikilink">水川麻美</a>・Cream(上田晉也・有田哲平)・松子DELUXE</p></td>
<td><p>［<a href="../Page/KAT-TUN之前輩請教教我!大人的守則100!.md" title="wikilink">KAT-TUN之前輩請教教我!大人的守則100!part</a>３］未公開SP(後篇)</p></td>
<td><p>5.0%</p></td>
</tr>
<tr class="odd">
<td><p>143</p></td>
<td><p>1月20日</p></td>
<td><p>亀梨・田中・上田：有吉弘行・樋口信義・清水靖夫</p></td>
<td><p>［下北澤的秘密規則］探尋之旅</p></td>
<td><p>5.6%</p></td>
</tr>
<tr class="even">
<td><p>144</p></td>
<td><p>1月27日</p></td>
<td><p>田口・中丸：小林劍道・どぶるっく・ずん</p></td>
<td><p>［小林風格的五反田規則］探尋之旅</p></td>
<td><p>3.6%</p></td>
</tr>
<tr class="odd">
<td><p>145</p></td>
<td><p>2月3日</p></td>
<td><p>亀梨・田中・上田：大久保佳代子・北野琴奈 ・高田賢史・山川浩司・柿沼常治・小島啟太郎</p></td>
<td><p>［有點受歡迎不動產的特別規則］探尋之旅</p></td>
<td><p>3.6%</p></td>
</tr>
<tr class="even">
<td><p>146</p></td>
<td><p>2月10日</p></td>
<td><p>田口・田中・上田・中丸：<a href="../Page/神戸蘭子.md" title="wikilink">神戸蘭子</a>・<a href="../Page/Suzanne.md" title="wikilink">Suzanne</a>・<a href="../Page/柳原可奈子.md" title="wikilink">柳原可奈子</a> ・金兒</p></td>
<td><p>［冬季約會的神秘規則］探尋之旅(前篇)<br />
新曲『<a href="../Page/Love_yourself～君が嫌いな君が好き～.md" title="wikilink">Love yourself～君が嫌いな君が好き～</a>』公告</p></td>
<td><p>5.4%</p></td>
</tr>
<tr class="odd">
<td><p>147</p></td>
<td><p>2月17日</p></td>
<td><p>田口・田中・上田・中丸：<a href="../Page/神戸蘭子.md" title="wikilink">神戸蘭子</a>・<a href="../Page/Suzanne.md" title="wikilink">Suzanne</a>・<a href="../Page/柳原可奈子.md" title="wikilink">柳原可奈子</a> ・橫山昭江</p></td>
<td><p>［冬季約會的神秘規則］探尋之旅(後篇)</p></td>
<td><p>4.7%</p></td>
</tr>
<tr class="even">
<td><p>148</p></td>
<td><p>2月24日</p></td>
<td><p>亀梨・田中・上田：麒麟・森崎友紀・江邊香織・中田彩</p></td>
<td><p>［美女老師○○的特別規則］探尋之旅</p></td>
<td><p>4.5%</p></td>
</tr>
<tr class="odd">
<td><p>149</p></td>
<td><p>3月3日</p></td>
<td><p>亀梨・田中：里田舞・芭比</p></td>
<td><p>［江之島冬天約會的神秘規則］探尋之旅</p></td>
<td><p>4.3%</p></td>
</tr>
<tr class="even">
<td><p>150</p></td>
<td><p>3月10日</p></td>
<td><p>上田・中丸：<a href="../Page/有吉弘行.md" title="wikilink">有吉弘行</a>・<a href="../Page/桐谷美玲.md" title="wikilink">桐谷美玲</a></p></td>
<td><p>［護花使者的侍從規則］探尋之旅</p></td>
<td><p>5.2%</p></td>
</tr>
<tr class="odd">
<td><p>151</p></td>
<td><p>3月17日</p></td>
<td><p>赤西・田口：<a href="../Page/克里斯蒂娜.md" title="wikilink">克里斯蒂娜</a>・<a href="../Page/トリンドル玲奈.md" title="wikilink">トリンドル玲奈</a>・野口洋一・田口節子</p></td>
<td><p>［老街約會的神祕規則］探尋之旅</p></td>
<td><p>5.3%</p></td>
</tr>
<tr class="even">
<td><p>152</p></td>
<td><p>3月24日</p></td>
<td></td>
<td><p>淚的最終回放送，3年間的回憶大公開</p></td>
<td><p>4.9%</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 註釋

<references/>

## 外部連結

  - [カートゥンKAT-TUN](https://web.archive.org/web/20070623050804/http://www.ntv.co.jp/cartoonkat-tun/)
    節目官網
  - [カートゥンKAT-TUN｜お父さんBLOG](https://web.archive.org/web/20071024122914/http://www1.ntv.co.jp/cartoonkat-tun/diary/)
    由 カートゥンKAT-TUN AD 原田大輔提供的節目情報

[Category:KAT-TUN](../Category/KAT-TUN.md "wikilink")
[Category:日本電視台綜藝節目](../Category/日本電視台綜藝節目.md "wikilink")
[Category:2007年日本電視節目](../Category/2007年日本電視節目.md "wikilink")

1.  赤西仁因季節性流感而缺席。
2.  赤西仁因季節性流感而缺席。
3.  赤西仁因季節性流感而缺席。