**湯姆·弗萊屈**（，）在英國流行搖滾樂團[McFLY裡擔任主唱和吉他手](../Page/McFLY.md "wikilink")，其餘成員還有[丹尼·瓊斯](../Page/丹尼·瓊斯.md "wikilink")（Danny
Jones）、[道基·波伊特](../Page/道基·波伊特.md "wikilink")（Dougie
Poynter）和[哈利·裘德](../Page/哈利·裘德.md "wikilink")（Harry Judd）。

在1985年7月17日出生於[英格蘭](../Page/英格蘭.md "wikilink")[倫敦](../Page/倫敦.md "wikilink")[哈洛的一個普通的工薪階層家庭](../Page/哈洛.md "wikilink")。\[1\]父親曾為[柯達工作](../Page/柯達.md "wikilink")，母親則是一位特殊教育教師，有一個小七歲的妹妹凱莉·弗萊屈（Carrie
Fletcher）。全家人都非常鼓勵他做音樂。\[2\]

## McFly

弗萊屈和丹尼瓊斯在McFLY裡共同擔任[吉他手](../Page/吉他手.md "wikilink")，兩人也一起負責主唱；而道基波伊特擔任貝斯手和合音員，哈利裘德則是[鼓手](../Page/鼓手.md "wikilink")。湯姆因為沒被選進英國最成功的樂團——霸子，另外創造了McFLY。他和丹尼、道基、哈利寫了McFLY裡大部分的歌詞，而湯姆的好友，前霸子樂團成員[詹姆斯·波尼](../Page/詹姆斯·波尼.md "wikilink")（James
Bourne），也會在重大場合裡幫他們寫歌。

McFly現在已發行五張專輯，第六張專輯正在製作中。

## 作詞作曲

弗萊屈已經寫了九首單曲，《Crashed The Wedding》、《Who's David》是為霸子寫的，其餘歌曲分別是《[Five
Colours in Her
Hair](../Page/Five_Colours_in_Her_Hair.md "wikilink")》（詹姆斯、丹尼、湯姆共同編寫）、《[Obviously](../Page/Obviously.md "wikilink")》（詹姆斯、丹尼、湯姆共同編寫）、《[All
About You/You've Got a
Friend](../Page/All_About_You/You've_Got_a_Friend.md "wikilink")》（湯姆編寫）、《[I'll
Be OK](../Page/I'll_Be_OK.md "wikilink")》〔丹尼、湯姆、道基共同編寫〕、《[Please,
Please](../Page/Please,_Please.md "wikilink")》（湯姆、丹尼、哈利、道基共同編寫）以及《[Star
Girl](../Page/Star_Girl.md "wikilink")》，這些歌都是為他的樂團McFLY所寫的。他也替霸子的第二張專輯《A
Present for Everyone》寫了八首歌，另外McFLY的第一張專輯《[Room on the 3rd
Floor](../Page/Room_on_the_3rd_Floor.md "wikilink")》，（中譯三樓飛行屋，但依照英式用法，應該是四樓才對）也幾乎都是湯姆一人獨挑大樑。《Room
on the 3rd
Floor》和他們的第二張專輯《[Wonderland](../Page/Wonderland.md "wikilink")》很快就進入英國排行榜的第一名。第二張專輯包括了《She
Falls
Asleep》，是一首湯姆用鋼琴歌頌、關於一個自殺女孩的史詩式歌曲。湯姆還和詹姆斯·波尼一起改寫了霸子第一名單曲《[Thunderbirds](../Page/Thunderbirds.md "wikilink")》的主曲調。

[2012年夏季奥林匹克运动会吉祥物](../Page/2012年夏季奥林匹克运动会.md "wikilink")（文洛克和曼德維爾）主題歌《On
A
Rainbow》為湯姆所作，演唱者為湯姆和他的妹妹，凱莉·弗萊屈。\[3\]他也為[一世代的首張專輯](../Page/一世代.md "wikilink")《整夜無眠》作了一首歌《I
Want》。\[4\]

湯姆被許多音樂家所影響：[約翰·威廉斯](../Page/約翰·威廉斯.md "wikilink")、[海灘男孩](../Page/海灘男孩.md "wikilink")、[披頭四](../Page/披頭四.md "wikilink")、[誰人](../Page/誰人.md "wikilink")，以及各種不同的藝人諸如[Green
Day和](../Page/Green_Day.md "wikilink")[Blink
182](../Page/Blink_182.md "wikilink")。

## 演藝生活

湯姆進入西維亞青年戲劇學校並在westend歌舞劇扮演「奧利佛」。湯姆也曾出現在愛滋體悟宣導短片、有關咖哩的電視廣告，有一次更是[East
Enders](../Page/East_Enders.md "wikilink")（英國有名的連續劇）的臨時演員，他在裡面扮演的腳色是狄恩。他還曾經在1995
Mike & The Mechanics歌曲《Over My Shoulder》的影片出現過。

湯姆也在McFLY的電影《[倒楣愛神](../Page/幸運之吻.md "wikilink")》和[琳賽·羅涵與](../Page/琳賽·羅涵.md "wikilink")[克里斯·派恩共同演出](../Page/克里斯·派恩.md "wikilink")。

## 私人生活

2011年4月18日，湯姆在自己曾就讀的西維亞青年戲劇學校向曾是同學的，已交往多年的女友喬凡娜·菲爾克求婚。\[5\]他現在和妻子以及他們的三隻貓（馬文、莉婭和奧羅娜）住在一起。\[6\]

2012年5月12日湯姆與喬凡娜結婚。\[7\]

## 外部連結

  - [McFly 官方網站](http://www.mcflyofficial.com)

## 參考資料

[Fletcher, Tom](../Category/1985年出生.md "wikilink") [Fletcher,
Tom](../Category/活著的人.md "wikilink") [Fletcher,
Tom](../Category/英國吉他手.md "wikilink") [Fletcher,
Tom](../Category/英國男歌手.md "wikilink") [Fletcher,
Tom](../Category/英國鋼琴家.md "wikilink") [Fletcher,
Tom](../Category/英國流行歌手.md "wikilink") [Fletcher,
Tom](../Category/英國流行鋼琴家.md "wikilink") [Fletcher,
Tom](../Category/McFly.md "wikilink") [Fletcher,
Tom](../Category/男樂團成員.md "wikilink") [Fletcher,
Tom](../Category/倫敦出生.md "wikilink")
[Category:英格蘭歌曲作家](../Category/英格蘭歌曲作家.md "wikilink")

1.
2.
3.
4.
5.
6.
7.