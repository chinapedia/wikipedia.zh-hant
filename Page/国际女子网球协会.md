[爱奇艺体育](../Page/爱奇艺.md "wikilink"){{\\}}[保时捷](../Page/保时捷.md "wikilink"){{\\}}[SAP](../Page/SAP公司.md "wikilink")
|url = <https://www.wtatennis.com> }}

**国际女子网球协会**（），简称“”，是世界女子职业[网球的管理组织](../Page/网球.md "wikilink")，由[比利·简·金等人于](../Page/比利·简·金.md "wikilink")1973年9月创立\[1\]\[2\]。国际女子网球协会拥有并管理着，这是全球女子职业网球巡回赛事，旨在为女子网球创造更美好的未来。国际女子网球协会总部位于[美国](../Page/美国.md "wikilink")[佛罗里达州的](../Page/佛罗里达州.md "wikilink")[圣彼得斯堡](../Page/聖彼德斯堡_\(佛羅里達州\).md "wikilink")；欧洲总部位于[英国](../Page/英国.md "wikilink")[伦敦](../Page/伦敦.md "wikilink")；亚太区总部位于[中国](../Page/中国.md "wikilink")[北京](../Page/北京.md "wikilink")；并在[新加坡还拥有一个总部](../Page/新加坡.md "wikilink")。\[3\]

国际女子网球协会成立于1973年，但是其历史最早可以追溯到“巡回赛”。该赛事由主办，而由[菲利普·莫里斯公司](../Page/菲利普莫里斯国际.md "wikilink")[首席执行官乔](../Page/首席执行官.md "wikilink")·卡尔曼（）赞助。“维珍妮香烟巡回赛”首次赛事于1970年9月23日在美国[德克萨斯州休士顿市的休士顿球拍俱乐部](../Page/德克萨斯州.md "wikilink")（）内举行\[4\]。取得首届赛事的金牌。

当国际女子网球协会成立的时候，协会成员是包括比利·简·金在内的九位女子选手，她们也被称为“创始九人（）”。创始九人包括：、、、、、、和\[5\]\[6\]。现在WTA巡回赛有超过2500名选手代表近100个国家或地区参赛，共同竞争高达1.46亿[美元的总奖金](../Page/美元.md "wikilink")\[7\]。

国际女子网球协会2018年赛季共计有54场比赛，这其中包括：（含皇冠赛、超五赛和常规定级赛）、、[联合会杯](../Page/联合会杯网球赛.md "wikilink")（由[国际网球协会主办](../Page/国际网球协会.md "wikilink")）、年终锦标赛（含[WTA年终总决赛和](../Page/WTA年终总决赛.md "wikilink")[WTA超级精英赛](../Page/珠海WTA超级精英赛.md "wikilink")）和“[四大满贯](../Page/大滿貫_\(網球\).md "wikilink")”（由国际网球协会监督）。这些赛事将在全球30个国家或地区举行。而2018赛季将在10月21日到28日于新加坡举行的[WTA年终总决赛和在](../Page/WTA年终总决赛.md "wikilink")10月30日至11月4日于中国[珠海举行的](../Page/珠海.md "wikilink")[WTA超级精英赛两大赛事中结束](../Page/珠海WTA超级精英赛.md "wikilink")\[8\]\[9\]。2018年赛季中还包括[霍普曼杯](../Page/霍普曼杯.md "wikilink")，这也是由国际网球协会所举办的赛事，但是该赛事不发放任何WTA积分。

## 历史

## 赛事

## 排名

## 选手理事会

## 全球顾问委员会

  - <small>（公司首席执行官）</small>

  - [理查德·布兰森爵士](../Page/理查德·布蘭森.md "wikilink")<small>（[维珍集团创始人兼董事长](../Page/维珍集团.md "wikilink")）</small>

  - <small>（监督理事会理事；法国葡萄酒与蒸馏酒联合会主席）</small>

  - <small>（《[华尔街日报](../Page/华尔街日报.md "wikilink")》前发行人）</small>

  - [比利·简·金](../Page/比利·简·金.md "wikilink")<small>（世界团体网球赛联合创始人；国际女子网球协会创始人）</small>

<!-- end list -->

  - 李倩玲<small>（[WPP中国集团首席执行官](../Page/WPP集团.md "wikilink")）</small>

  - [温斯顿·洛德](../Page/温斯顿·洛德.md "wikilink")<small>（国际救援委员会名誉主席；前美国驻华大使）</small>

  - <small>（[哈佛商学院人际关系学教授](../Page/哈佛商学院.md "wikilink")）</small>

  - <small>（里士满公园合伙公司创始合伙人兼总裁）</small>

  - [艾农·米尔臣](../Page/艾农·米尔臣.md "wikilink")<small>（创始人兼所有者）</small>

<!-- end list -->

  - <small>（龙门娱乐创始人兼首席执行官）</small>

  - <small>（[利丰集团总裁兼首席执行官](../Page/利豐.md "wikilink")）</small>

  - 哈德威克·西蒙斯<small>（国际网球名人堂前主席）</small>

  - 简·索德斯托姆<small>（[太阳能源公司](../Page/太阳能源.md "wikilink")[首席市场官](../Page/首席市场官.md "wikilink")）</small>

  - 金伯莱·威廉姆斯<small>（[美国美式足球大联盟](../Page/國家美式橄欖球聯盟.md "wikilink")、NFL电视网[首席运营官](../Page/首席运营官.md "wikilink")）</small>

## 注释

## 参考文献

  - 引用

## 外部链接

  - [国际女子网球协会官方网站](https://www.wtatennis.com/)

  - [国际女子网球协会官方中文网站](http://www.wta.cn/)

  - [国际女子网球协会官方视频站](http://www.wtatv.com/)

  -
  -
  -
  -
  -
{{-}}

[国际女子网球协会](../Category/国际女子网球协会.md "wikilink")
[Category:女子網球](../Category/女子網球.md "wikilink")
[Category:网球组织](../Category/网球组织.md "wikilink")
[Category:女子体育组织](../Category/女子体育组织.md "wikilink")
[Category:国际体育组织](../Category/国际体育组织.md "wikilink")
[Category:網球歷史](../Category/網球歷史.md "wikilink")
[Category:佛罗里达州](../Category/佛罗里达州.md "wikilink")
[Category:佛羅里達州體育](../Category/佛羅里達州體育.md "wikilink")
[Category:1973年建立的組織](../Category/1973年建立的組織.md "wikilink")

1.
2.
3.

4.
5.

6.

7.
8.

9.