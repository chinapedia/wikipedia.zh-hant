[Yale_Law_School_in_the_Sterling_Law_Building.jpg](https://zh.wikipedia.org/wiki/File:Yale_Law_School_in_the_Sterling_Law_Building.jpg "fig:Yale_Law_School_in_the_Sterling_Law_Building.jpg")\]\]
[Yale_Law_School_Library_Reading_Room_(L3).jpg](https://zh.wikipedia.org/wiki/File:Yale_Law_School_Library_Reading_Room_\(L3\).jpg "fig:Yale_Law_School_Library_Reading_Room_(L3).jpg")

**耶鲁法学院**（**Yale Law
School**）位于美国[康涅狄格州的](../Page/康涅狄格州.md "wikilink")[纽黑文市](../Page/纽黑文.md "wikilink")，成立于1843年。其自《[美国新闻与世界报道](../Page/美国新闻与世界报道.md "wikilink")》1987年开始发布[美国](../Page/美国.md "wikilink")[法学院排名以来](../Page/法学.md "wikilink")，一直名列第一位，常年被視為全美國最頂尖之法学院。

耶鲁法学院的學生规模较小，每年大约招收180名新生，学生教师比为7.5：1，是美国法学院比例最低的。耶鲁法学院的图书馆有80万册藏书。

耶鲁法学院的著名校友包括美国的[杰拉尔德·福特总统](../Page/杰拉尔德·福特.md "wikilink")、[比尔·克林顿总统](../Page/比尔·克林顿.md "wikilink")、[希拉里·克林顿国务卿](../Page/希拉里·克林顿.md "wikilink")。

## 著名校友

  - 美國總統

<!-- end list -->

  - [比尔·克林顿](../Page/比尔·克林顿.md "wikilink"):
    第42任[美國總統](../Page/美國總統.md "wikilink")
  - [傑拉爾德·福特](../Page/傑拉爾德·福特.md "wikilink"):
    第38任[美國總統](../Page/美國總統.md "wikilink")

<!-- end list -->

  - 內閣成員

<!-- end list -->

  - [希拉蕊·克林頓](../Page/希拉蕊·克林頓.md "wikilink"):
    前[美國第一夫人](../Page/美國第一夫人.md "wikilink")、第67任[美國國務卿](../Page/美國國務卿.md "wikilink")
  - [羅伯特·萊克](../Page/羅伯特·萊克.md "wikilink"):
    第22任[美國勞工部長](../Page/美國勞工部長.md "wikilink")

## 另见

  - [耶鲁法学院知名校友](../Page/耶鲁法学院知名校友.md "wikilink")

## 参考资料

<references/>

## 外部链接

  - [耶鲁法学院官方网站](http://www.law.yale.edu)

[Category:耶鲁大学](../Category/耶鲁大学.md "wikilink")
[Category:1843年創建的教育機構](../Category/1843年創建的教育機構.md "wikilink")