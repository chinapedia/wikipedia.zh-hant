[thumb](../Page/文件:Human_Language_Families_\(wikicolors\).png.md "wikilink")

**语言系属分类**（）是指根据语言的演化关系，对语言进行分类的方法，具有相同祖先的语言被归为一类，类似[生物分类法](../Page/生物分类法.md "wikilink")。分类依据为各语言[语音](../Page/语音.md "wikilink")、[词汇](../Page/词汇.md "wikilink")、[语法之间的对应特征和演变规律](../Page/语法.md "wikilink")。

[英语中](../Page/英语.md "wikilink")，一般将语言的最早出现的分类单元称为“语系”（language
family）。语系之外英语中參照生物的分類系統命名。[汉语中](../Page/汉语.md "wikilink")，习惯上将语系的下一级分类单元称为“[语族](../Page/语族.md "wikilink")”，语族的下一级分类单元称为“[语支](../Page/语支.md "wikilink")”，语支下为具体的语言。如英语属于[印欧语系](../Page/印欧语系.md "wikilink")[日耳曼语族](../Page/日耳曼语族.md "wikilink")[西日耳曼语支](../Page/西日耳曼语支.md "wikilink")。实际上，语言分层情况远多于这四层结构，因此也有“亚语族”、“亚语支”等分类单元。有人提出在语族和语支之间增设“语群”，在语支和语言之间增设“语组”或“语团”。而在語系之上，則有「超語系」、「大語系」，或稱為「[語門](../Page/語門.md "wikilink")」，语系之上的研究工作始于20世纪末期，目前存在较大争议。

当没有适合的分类单元可以用作后缀时，汉语习惯称为“某某诸语言”，英语则用复数的“xxx languages”表示。

原则上，分类学将能相互听懂的称作同一语言，不能相互听懂的称作不同语言。语言之下还分为方言，方言间可相互听懂但存在一些差别。但是，基于传统、政治、文化等原因，语言、方言的区分并不如上述定义那样严格。汉语传统上将[闽语](../Page/闽语.md "wikilink")、[粤语等不能听懂的语言称为方言](../Page/粤语.md "wikilink")（“方言”一词在汉语中原本并没有能不能听懂的区别，对译了英语“dialect”一词后才产生了能不能听懂的区别）。在日本，传统上将不能听懂的[琉球语称作沖繩方言](../Page/琉球语.md "wikilink")。而於其下的稱[片](../Page/片.md "wikilink")，如[閩南語](../Page/閩南語.md "wikilink")[潮汕片](../Page/潮州話.md "wikilink")、[漳泉片](../Page/泉漳片.md "wikilink")、[日語](../Page/日語.md "wikilink")[近畿方言](../Page/近畿方言.md "wikilink")[和內弁等](../Page/和內弁.md "wikilink")。而能相互听懂的[塞尔维亚语](../Page/塞尔维亚语.md "wikilink")、[克罗地亚语](../Page/克罗地亚语.md "wikilink")、[波斯尼亚语和](../Page/波斯尼亚语.md "wikilink")[黑山语则因政治原因被称作不同语言](../Page/黑山语.md "wikilink")。

现今全世界现存的语言約7000種，根据系属分类的方法，确定每种语言的祖先，称为“[祖语](../Page/祖语.md "wikilink")”。所有人类语言的祖语被称作[原始人类语言](../Page/原始人类语言.md "wikilink")（又叫“原世界语”），被假定为[智人出现后](../Page/智人.md "wikilink")[走出非洲前所使用的的语言](../Page/人類單地起源說.md "wikilink")，但这一观点争议巨大。

一般只对[自然语言进行系属分类](../Page/自然语言.md "wikilink")，而不考虑[人工语言](../Page/人工语言.md "wikilink")。

语言与生物物种不同，任何语言都会相互影响而产生新语言，因此判断语言系属分类并非易事，语言间究竟是同源、借用、[语言联盟还是三者都有](../Page/语言联盟.md "wikilink")，常常引发巨大争论。

## 系属分类方法

### 以母语使用人口排列

1.  [印欧语系](../Page/印欧语系.md "wikilink")（[欧洲](../Page/欧洲.md "wikilink")、[北美洲大部份地区](../Page/北美洲.md "wikilink")、[西南亚到](../Page/西南亚.md "wikilink")[南亚](../Page/南亚.md "wikilink")）
2.  [汉藏语系](../Page/汉藏语系.md "wikilink")（[东亚](../Page/东亚.md "wikilink")）
3.  [尼日尔-刚果语系](../Page/尼日尔-刚果语系.md "wikilink")（[撒哈拉以南非洲](../Page/撒哈拉以南非洲.md "wikilink")）
4.  [亚非语系](../Page/亚非语系.md "wikilink")，旧称[闪含语系](../Page/闪含语系.md "wikilink")（[北非到](../Page/北非.md "wikilink")[非洲之角](../Page/非洲之角.md "wikilink")、[西南亚](../Page/西南亚.md "wikilink")）
5.  [南岛语系](../Page/南岛语系.md "wikilink")（[大洋洲](../Page/大洋洲.md "wikilink")、[马达加斯加](../Page/马达加斯加.md "wikilink")、[东南亚](../Page/东南亚.md "wikilink")）
6.  [达罗毗荼语系](../Page/达罗毗荼语系.md "wikilink")（[南亚](../Page/南亚.md "wikilink")）
7.  [阿尔泰语系](../Page/阿尔泰语系.md "wikilink")（[中亚](../Page/中亚.md "wikilink")，有争议，有说法为[突厥语族和](../Page/突厥语族.md "wikilink")[日本－琉球语系分别为第](../Page/日本－琉球语系.md "wikilink")7和第8位）
8.  [南亚语系](../Page/南亚语系.md "wikilink")（[东南亚](../Page/东南亚.md "wikilink")）
9.  [壮侗语系](../Page/壮侗语系.md "wikilink")（[东亚](../Page/东亚.md "wikilink")）
10. [乌拉尔语系](../Page/乌拉尔语系.md "wikilink")（[北亚到](../Page/北亚.md "wikilink")[北欧](../Page/北欧.md "wikilink")）

### 以语言种类多少排列

以《[民族语：全世界的语言](../Page/民族語.md "wikilink")》“Ethnologue”分类，种类最多的15个语系如下：

1.  [尼日尔-刚果语系](../Page/尼日尔-刚果语系.md "wikilink")（1545种语言）

2.  [南島語系](../Page/南島語系.md "wikilink")（1257种语言）

3.  [跨新几内亚语系](../Page/跨新几内亚语系.md "wikilink")（477种语言）（种类有争议）

4.  [汉藏语系](../Page/汉藏语系.md "wikilink")（460种语言）

5.  [印欧语系](../Page/印欧语系.md "wikilink")（445种语言）

6.  [亚非语系](../Page/亚非语系.md "wikilink")，旧称[闪含语系](../Page/闪含语系.md "wikilink")（376种语言）

7.  [尼罗-撒哈拉语系](../Page/尼罗-撒哈拉语系.md "wikilink")（204种语言）

8.  （178种语言）（种类有争议）

9.  [歐托-曼格語系](../Page/歐托-曼格語系.md "wikilink")（174种语言）（数量有争议，认为只有27种）

10. [南亚语系](../Page/南亚语系.md "wikilink")（169种语言）

11. （100种语言）（种类有争议）

12. [壮侗语系](../Page/壮侗语系.md "wikilink")（92种语言）

13. [图皮语系](../Page/图皮语系.md "wikilink")（76种语言）

14. [达罗毗荼语系](../Page/达罗毗荼语系.md "wikilink")（73种语言）

15. [玛雅语系](../Page/玛雅语系.md "wikilink")（69种语言）

## 参考文献

## 延伸閱讀

  -
  - Boas, Franz. (1922). *Handbook of American Indian languages* (Vol.
    2). Bureau of American Ethnology, Bulletin 40. Washington:
    Government Print Office (Smithsonian Institution, Bureau of American
    Ethnology).

  - Boas, Franz. (1933). *Handbook of American Indian languages* (Vol.
    3). Native American legal materials collection, title 1227.
    Glückstadt: J.J. Augustin.

## 外部連結

  - [Ethnologue:世界主要語系列表及介紹](http://www.ethnologue.com/family_index.asp)

## 参见

  - [语系列表](../Page/语系列表.md "wikilink")
  - [语言的类型分类](../Page/语言的类型分类.md "wikilink")
  - [语言列表](../Page/语言列表.md "wikilink")
  - [人工語言](../Page/人工語言.md "wikilink")

{{-}}

[語系](../Category/語系.md "wikilink")
[Category:分类系统](../Category/分类系统.md "wikilink")
[Category:语言列表](../Category/语言列表.md "wikilink")