**寮國**分為十七省（[寮文](../Page/寮文.md "wikilink")：ແຂວງ，khoueng）、一個[直辖市](../Page/直辖市.md "wikilink")（[万象市](../Page/万象市.md "wikilink")，[寮文](../Page/寮文.md "wikilink")：ນະຄອນຫຼວງວຽງຈັນ,
Nakhonluang
Viengchan）。省下设乡、村。2013年复设了先前被撤销的[赛宋奔省](../Page/赛宋奔省.md "wikilink")。

| No. | 名稱                                   | [寮文](../Page/寮文.md "wikilink") | 首府                                     | 面积（km²） | 人口      | ISO   | 區  |
| --- | ------------------------------------ | ------------------------------ | -------------------------------------- | ------- | ------- | ----- | -- |
| 1   | [阿速坡省](../Page/阿速坡省.md "wikilink")   | ອັດຕະປື                        | [阿速坡](../Page/阿速坡.md "wikilink")       | 10,320  | 139,628 | LA-AT | 下寮 |
| 2   | [博胶省](../Page/博胶省.md "wikilink")     | ບໍ່ແກ້ວ                        | [会晒](../Page/会晒.md "wikilink")         | 6,196   | 179,243 | LA-BK | 上寮 |
| 3   | [博利坎赛省](../Page/博利坎赛省.md "wikilink") | ບໍລິຄໍາໄຊ                      | [北汕](../Page/北汕.md "wikilink")         | 14,863  | 163,847 | LA-BL | 中寮 |
| 4   | [占巴塞省](../Page/占巴塞省.md "wikilink")   | ຈຳປາສັກ                        | [巴色](../Page/巴色.md "wikilink")         | 15,415  | 694,023 | LA-CH | 下寮 |
| 5   | [华潘省](../Page/华潘省.md "wikilink")     | ຫົວພັນ                         | [桑怒](../Page/桑怒.md "wikilink")         | 16,500  | 289,393 | LA-HO | 上寮 |
| 6   | [甘蒙省](../Page/甘蒙省.md "wikilink")     | ຄໍາມ່ວນ                        | [他曲](../Page/他曲.md "wikilink")         | 16,315  | 392,052 | LA-KH | 中寮 |
| 7   | [琅南塔省](../Page/琅南塔省.md "wikilink")   | ຫຼວງນໍ້າທາ                     | [琅南塔](../Page/琅南塔.md "wikilink")       | 9,325   | 175,753 | LA-LM | 上寮 |
| 8   | [琅勃拉邦省](../Page/琅勃拉邦省.md "wikilink") | ຫຼວງພະບາງ                      | [琅勃拉邦](../Page/琅勃拉邦.md "wikilink")     | 16,875  | 431,889 | LA-LP | 上寮 |
| 9   | [乌多姆塞省](../Page/乌多姆塞省.md "wikilink") | ອຸດົມໄຊ                        | [芒赛](../Page/芒赛.md "wikilink")         | 15,370  | 307,622 | LA-OU | 上寮 |
| 10  | [丰沙里省](../Page/丰沙里省.md "wikilink")   | ຜົ້ງສາລີ                       | [丰沙里](../Page/丰沙里.md "wikilink")       | 16,270  | 177,989 | LA-PH | 上寮 |
| 11  | [沙拉湾省](../Page/沙拉湾省.md "wikilink")   | ສາລະວັນ                        | [沙拉湾](../Page/沙拉湾.md "wikilink")       | 10,691  | 396,942 | LA-SL | 下寮 |
| 12  | [沙湾拿吉省](../Page/沙湾拿吉省.md "wikilink") | ສະຫວັນນະເຂດ                    | [凯山丰威汉市](../Page/凯山丰威汉市.md "wikilink") | 21,774  | 969,697 | LA-SV | 下寮 |
| 13  | [万象省](../Page/万象省.md "wikilink")     | ວຽງຈັນ                         | [万荣](../Page/万荣.md "wikilink")         | 15,927  | 419,090 | LA-VI | 中寮 |
| 14  | [万象直辖市](../Page/万象直辖市.md "wikilink") | ນະຄອນຫຼວງວຽງຈັນ                | [万象](../Page/万象.md "wikilink")         | 3,920   | 820,940 | LA-VT | 中寮 |
| 15  | [沙耶武里省](../Page/沙耶武里省.md "wikilink") | ໄຊຍະບູລີ                       | [沙耶武里](../Page/沙耶武里.md "wikilink")     | 16,389  | 381,376 | LA-XA | 上寮 |
| 16  | [塞公省](../Page/塞公省.md "wikilink")     | ເຊກອງ                          | [班蓬](../Page/班蓬.md "wikilink")         | 7,665   | 113,048 | LA-XE | 下寮 |
| 17  | [川圹省](../Page/川圹省.md "wikilink")     | ຊຽງຂວາງ                        | [丰沙湾](../Page/丰沙湾.md "wikilink")       | 15,880  | 244,684 | LA-XI | 上寮 |
| 18  | [赛宋奔省](../Page/赛宋奔省.md "wikilink")   | ໄຊສົມບູນ                       | [班蒙查](../Page/班蒙查.md "wikilink")       | 8,300   | 84,168  | LA-XS | 上寮 |

## 参见

  - [ISO 3166-2:LA](../Page/ISO_3166-2:LA.md "wikilink")

## 參考資料

[老挝行政区划](../Category/老挝行政区划.md "wikilink")