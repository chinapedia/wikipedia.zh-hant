**马穆鲁克王朝**（1250年－1517年）是一個於十三世紀中至十六世紀初統治[埃及](../Page/埃及.md "wikilink")、[巴勒斯坦和](../Page/巴勒斯坦.md "wikilink")[敍利亞地區的國家](../Page/敍利亞.md "wikilink")，又称**马穆鲁克苏丹国**。马穆鲁克王朝的历史可分為前後二期：前期為[伯海里王朝](../Page/伯海里王朝.md "wikilink")，是由[欽察](../Page/欽察.md "wikilink")[突厥傭兵主政](../Page/突厥.md "wikilink")。伯海里意為[河洲](../Page/河洲.md "wikilink")，因為他們是在[尼羅河上進行管理事務](../Page/尼羅河.md "wikilink")。後期名為[布尔吉王朝](../Page/布尔吉王朝.md "wikilink")，始於1382年，由[高加索人](../Page/高加索.md "wikilink")，特別是[切爾克斯人組成](../Page/切爾克斯人.md "wikilink")。

## 历史

前期是由[阿尤布王朝最後一位蘇丹萨利赫死亡而產生的](../Page/阿尤布王朝.md "wikilink")。1260年[拜巴爾乘戰勝](../Page/拜巴爾.md "wikilink")[十字軍的餘威](../Page/十字軍.md "wikilink")，自立為王，把敘利亞併合埃及，在[阿音扎魯特戰役击败](../Page/阿音扎魯特戰役.md "wikilink")[蒙古帝国](../Page/蒙古帝国.md "wikilink")，同時擁立一位[阿拔斯王朝的男子為影子](../Page/阿拔斯王朝.md "wikilink")[哈里發](../Page/哈里發.md "wikilink")，象徵正式回歸[遜尼派](../Page/遜尼派.md "wikilink")。外交上，重視和[金帳汗國合作](../Page/金帳汗國.md "wikilink")，以便補充年輕的突厥傭兵，以及阻止[伊兒汗國入侵](../Page/伊兒汗國.md "wikilink")。疆域最南至[努比亚](../Page/努比亚.md "wikilink")。

马穆鲁克人是由各地雇得的傭兵組成，特別是[欽察人](../Page/欽察人.md "wikilink")，偶爾也有[希臘人](../Page/希臘人.md "wikilink")、[斯拉夫人](../Page/斯拉夫人.md "wikilink")、[庫爾德人](../Page/庫爾德人.md "wikilink")，甚至有些西歐的[拉丁人](../Page/拉丁人.md "wikilink")，他們大多數完全不懂阿拉伯語。馬木留克王朝建基於一個二元政府，軍事上，由雇傭兵負責。他們不許當地人充任傭兵，民政和軍事都是由傭兵負責，手下有當地文書人員負責。在傳位方式上，前期是世襲。在1383年開始，由勢力最大的將軍出任蘇丹。有些像[德里蘇丹國](../Page/德里蘇丹國.md "wikilink")。

## 人口

马穆鲁克没有具体的人口调查，根据估计在公元1300年统治了1090万居民。

## 经济

在經濟上，重視地中海与金帐汗国貿易，後來因受[帖木兒入侵和](../Page/帖木兒.md "wikilink")[蝗蟲侵襲](../Page/蝗蟲.md "wikilink")，加上[葡萄牙帝國打開了直接水路](../Page/葡萄牙帝國.md "wikilink")，一蹶不振，最後一位馬木留克蘇丹圖曼貝伊在1517年[里達尼亞戰役因不願投降](../Page/里達尼亞戰役.md "wikilink")，被[奥斯曼帝國蘇丹](../Page/奥斯曼帝國.md "wikilink")[塞利姆一世殺死](../Page/塞利姆一世.md "wikilink")，馬木留克王朝滅亡。

## 参見

  - [马穆鲁克](../Page/马穆鲁克.md "wikilink")

## 参考资料

[馬木留克王朝](../Category/馬木留克王朝.md "wikilink")
[Category:埃及历史](../Category/埃及历史.md "wikilink")
[Category:历史上的苏丹国](../Category/历史上的苏丹国.md "wikilink")