strips whitespace from parameter 1,

`      in case it is fed like this: ``. -->`

|Category:{{\#if:x|  }} |}}
}}<includeonly>{{\#if:||}}</includeonly><noinclude>

</noinclude><includeonly> {{\#switch:||=

`{{#if:{{#property:P373}}`
`| {{#if:`
` | {{#ifeq:{{#invoke:Wikidata|getRawValue|P373|FETCH_WIKIDATA}}|{{#invoke:StringReplace|encode wiki page name|``}}| `
`  |  `
`  }}`
` | {{#ifeq:{{#invoke:Wikidata|getRawValue|P373|FETCH_WIKIDATA}}|``|`
`  |  `
`  }}`
` }}`
`|  `
`}}`

}}</includeonly>

[Category:维基共享资源模板未填写分类](../Category/维基共享资源模板未填写分类.md "wikilink")
[Category:本地链接的维基共享资源分类与Wikidata不同](../Category/本地链接的维基共享资源分类与Wikidata不同.md "wikilink")
[Category:以页面标题链接的维基共享资源分类与Wikidata不同](../Category/以页面标题链接的维基共享资源分类与Wikidata不同.md "wikilink")
[Category:缺少Wikidata链接的维基共享资源分类](../Category/缺少Wikidata链接的维基共享资源分类.md "wikilink")