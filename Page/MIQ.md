**MIQ**，血型AB型，出身於[日本](../Page/日本.md "wikilink")[東京都](../Page/東京都.md "wikilink")[港區的動畫歌手](../Page/港區.md "wikilink")，歌迷常以「MIQ姐」來稱呼她。現主要於東京、[鳥取市兩地活動](../Page/鳥取市.md "wikilink")，並創辦「VLOMIQ」音樂教室。

## 人物

  - 在東京出生、鳥取成長。

  - 1982年時，以**MIO**的藝名在[戰鬥裝甲Xabungle中演唱插入曲兼出道曲](../Page/戰鬥裝甲Xabungle.md "wikilink")「HEY
    YOU」。

  - 出道之後都以演唱機器人動畫主題曲為其特色，像是演唱的[聖戰士DUNBINE](../Page/聖戰士DUNBINE.md "wikilink")、[重戰機L-Gaim等作品至今仍有不錯的人氣](../Page/重戰機L-Gaim.md "wikilink")。在聖戰士DUNBINE中也以[聲優客串演出](../Page/聲優.md "wikilink")。

  - 2001年回到故鄉鳥取市、改名為MIQ，

  - 也曾在1994、1995年於美國的[Anime
    East中表演](../Page/Anime_East.md "wikilink")，而在2004年時再度到美國的[Anime
    Expo](../Page/Anime_Expo.md "wikilink")2004登台演出。

  - 2007年起擔任鳥取市觀光大使。移居回東京並兩地活動。

  - 2011年4月加入事務所。

## 代表曲

  - HEY YOU（[戰鬥裝甲Xabungle](../Page/戰鬥裝甲Xabungle.md "wikilink")）

  - （[聖戰士DUNBINE](../Page/聖戰士DUNBINE.md "wikilink")）

  - \-Time for L.GAIM- / （[重戰機L-Gaim](../Page/重戰機L-Gaim.md "wikilink")）

  - 不思議CALL ME／夢銀河（[星銃士俾斯麥](../Page/星銃士俾斯麥.md "wikilink")，台譯為「宇宙奇兵」）

  - MEN OF DESTINY / Evergreen（[機動戰士GUNDAM0083
    星塵的回憶](../Page/機動戰士GUNDAM_0083：Stardust_Memory.md "wikilink")）

  - 熱風！疾風！（[魔裝機神 THE LORD OF ELEMENTAL](../Page/魔裝機神.md "wikilink")）

  - （[友都八喜印象歌曲](../Page/友都八喜.md "wikilink")）

## 註釋

## 參考資料

<div class="references-small">

  -

</div>

## 外部連結

  - [](http://himestory.com/)

  - [](https://archive.is/20090314035453/http://www7b.biglobe.ne.jp/~miqueen/)

  - [](http://beatmiqs.exblog.jp/)

  - [](http://anison.info/data/person/276.html)

  - [](http://anison.info/data/person/14502.html)

  - [](http://www.7andy.jp/cd/detail?accd=C1048968)

  -
[Category:日本女歌手](../Category/日本女歌手.md "wikilink")
[Category:動畫歌手](../Category/動畫歌手.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")