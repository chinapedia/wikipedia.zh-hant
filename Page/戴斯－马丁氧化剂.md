**戴斯－马丁氧化反应**（Dess-Martin
Oxidation）是现代[有机合成中常用的](../Page/有机合成.md "wikilink")[氧化反应](../Page/氧化反应.md "wikilink")。

  -
    [Center](https://zh.wikipedia.org/wiki/File:Reaction_scheme.png "fig:Center")

## 制备

  -
    [Preparation_of_DMP.png](https://zh.wikipedia.org/wiki/File:Preparation_of_DMP.png "fig:Preparation_of_DMP.png")

## 总体反应

  - [氧化是通过一个叫做戴斯](../Page/氧化.md "wikilink")－马丁高价碘烷的[高价碘化合物作为](../Page/高价碘化合物.md "wikilink")[氧化剂在室温下完成的](../Page/氧化剂.md "wikilink")。
  - 反应完成后[碘由五价变成三价](../Page/碘.md "wikilink")。
  - 该反应后处理很简单，只需要用[碳酸氢钠溶液洗去](../Page/碳酸氢钠.md "wikilink")[副产物即可](../Page/副产物.md "wikilink")。

## 反应机理

  -
    [Oxidation_by_Dess-Martin-Peridinan.png](https://zh.wikipedia.org/wiki/File:Oxidation_by_Dess-Martin-Peridinan.png "fig:Oxidation_by_Dess-Martin-Peridinan.png")

<!-- end list -->

  -
    [Water_addition.png](https://zh.wikipedia.org/wiki/File:Water_addition.png "fig:Water_addition.png")

## 参见

  - [Swern氧化反应](../Page/Swern氧化反应.md "wikilink")
  - [Parikh-Doering氧化反应](../Page/Parikh-Doering氧化反应.md "wikilink")
  - [Corey–Kim氧化反应](../Page/Corey–Kim氧化反应.md "wikilink")
  - [Pfitzner–Moffatt氧化反应](../Page/Pfitzner–Moffatt氧化反应.md "wikilink")
  - [氯铬酸吡啶盐](../Page/氯铬酸吡啶盐.md "wikilink")
  - [Jones氧化反应](../Page/Jones氧化反应.md "wikilink")
  - [Ley氧化反应](../Page/Ley氧化反应.md "wikilink")
  - [Oppenauer氧化反应](../Page/Oppenauer氧化反应.md "wikilink")
  - [化学反应列表](../Page/化学反应列表.md "wikilink")

[Category:有机氧化还原反应](../Category/有机氧化还原反应.md "wikilink")
[Category:人名反应](../Category/人名反应.md "wikilink")
[Category:高碘酸盐](../Category/高碘酸盐.md "wikilink")
[Category:高价碘烷](../Category/高价碘烷.md "wikilink")
[Category:缺少物质图片的化学品条目](../Category/缺少物质图片的化学品条目.md "wikilink")