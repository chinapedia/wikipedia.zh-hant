[《武英殿二十四史·后汉书·袁绍刘表列传上》（卷104-40页）.png](https://zh.wikipedia.org/wiki/File:《武英殿二十四史·后汉书·袁绍刘表列传上》（卷104-40页）.png "fig:《武英殿二十四史·后汉书·袁绍刘表列传上》（卷104-40页）.png")·[后汉书](../Page/后汉书.md "wikilink")·袁绍刘表列传上》中提到摸金校尉\]\]
**摸金校尉**是中国古代[盗墓者的一個稱呼](../Page/盗墓.md "wikilink")。据史书记载，摸金校尉起源于[东汉末年](../Page/东汉.md "wikilink")[三国时期](../Page/三国.md "wikilink")，当时[魏军的领袖](../Page/曹魏.md "wikilink")[曹操为了弥补军饷的不足](../Page/曹操.md "wikilink")，设立[发丘中郎將](../Page/发丘将军.md "wikilink")，摸金校尉等[军衔](../Page/军衔.md "wikilink")，专司[盗墓取财](../Page/盗墓.md "wikilink")，贴补军用。

## 起源

曹操因軍費困難，設“發丘中郎將”、“摸金校尉”等職，發兵盜掘永城芒碭山（今河南商丘永城芒碭山）的[漢梁孝王墓](../Page/劉武_\(梁王\).md "wikilink")。

[袁绍的幕僚](../Page/袁绍.md "wikilink")[陈琳起草讨曹](../Page/陈琳_\(三国\).md "wikilink")“檄文”：

清末，[杨守敬](../Page/杨守敬.md "wikilink")、[熊会贞](../Page/熊会贞.md "wikilink")《[水经注疏](../Page/水经注疏.md "wikilink")》记载：“操发兵入砀，发梁孝王冢，破棺，收金室数万斤。”

[魯迅說](../Page/魯迅.md "wikilink")，“曹操設了‘摸金校尉’之類的職員，專門盜墓”\[1\]。

## 内部组织

曹操之时，摸金校尉乃从正规的军事编制。

## 流行文化

摸金校尉是中国当代网路小说《[鬼吹灯](../Page/鬼吹灯.md "wikilink")》所突出描写的门派。故事的主人公都是属于该门派的人物。\[2\]

## 注釋

## 参考资料

  - 《[後漢書](../Page/後漢書.md "wikilink")》卷七十四 袁紹列傳

## 相關影視

  - [寻龙诀](../Page/寻龙诀.md "wikilink")

## 參見

  - [陳琳](../Page/陈琳_\(三国\).md "wikilink")
  - [發丘中郎將](../Page/發丘將軍.md "wikilink")

## 外部連結

  - [『摸金校尉』可能只是一句戲言是陳琳杜撰嘲諷曹操的言辭](https://www.nownews.com/news/20170327/2385838)

[Category:中国墓葬](../Category/中国墓葬.md "wikilink")
[Category:曹魏](../Category/曹魏.md "wikilink")

1.  《花邊文學‧清明時節》
2.  [施工人员偷青铜剑疑点重重 摸金校尉夜盗古墓无价之宝 - 社会 -
    舜网新闻](http://news.e23.cn/shehui/2017-03-09/2017030900383.html)