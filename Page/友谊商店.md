**友谊商店**是[中国大陆的一个](../Page/中国大陆.md "wikilink")[国营](../Page/国营.md "wikilink")[商店品牌](../Page/商店.md "wikilink")，成立1958年由早期的高级消费品商店轉型而來，友谊商店主要服务的是外国来访者和官員，販賣各種歐美质优商品的特殊供应和市面上稀缺的商品。

由於早期仅限服务于[外宾](../Page/外国人.md "wikilink")、[外交官和其他](../Page/外交官.md "wikilink")[政府官员](../Page/政府官员.md "wikilink")，也因此而一度成为“[特權階級](../Page/特權階級.md "wikilink")”的象征。1990年代后向公众开放，与一般的高档[百货商场无异](../Page/百货商场.md "wikilink")。在[市场经济的影响和冲击下](../Page/市场经济.md "wikilink")，只有为数不多的几家友谊商店得以存活，其它分店早已结束营业。作为对过去的怀念，[北京](../Page/北京.md "wikilink")、[天津](../Page/天津.md "wikilink")、[上海和](../Page/上海.md "wikilink")[广州的几家大型友谊商店被保留了下来](../Page/广州.md "wikilink")。

## 概况

[Guangzhou_friendship_store_times_square_branch.JPG](https://zh.wikipedia.org/wiki/File:Guangzhou_friendship_store_times_square_branch.JPG "fig:Guangzhou_friendship_store_times_square_branch.JPG")

1951年5月，北京，天津，上海，廣州和沈陽5個城市建立了許多高級消費品商店，提供稀缺，緊備和質優商品的特殊供應。比如。1952年10月，上海大廈底層開設「上海國際友人服務部」，當時這個商店規模並不大，很少有人知曉。

50年代中期起，由於歐州國家的交往日漸頻繁，為了滿足外賓不斷擴大的購物需求，該商店進行轉型，在上海南京東路345號東海大廈開設「上海友誼商店」，1958年2月26日起試營業。這是一家是專為外賓、僑民和首長幹部服務的綜合性商店，主要供應名特產和市場緊俏的食品、日用品以及進口商品。

1964年，「北京友誼商店」在東華門大街25號開業，當時友誼商店最為獨特是主要服務的是外國來訪者或僑民。友誼商店的口號就是：「市面上有的商品，我們這裡要最好;市面上缺的商品，我們必須有;外國時興的，我們也得有！」，因為評價良好，在廣州也開張新的分店。

1970年代后期，[中国](../Page/中华人民共和国.md "wikilink")[政府实行](../Page/中華人民共和國政府.md "wikilink")[对外开放政策](../Page/对外开放.md "wikilink")，并于[中国大陆境内的各个主要](../Page/中国大陆.md "wikilink")[大城市开设](../Page/大城市.md "wikilink")“友谊商店”，作为[中国內地在](../Page/中国內地.md "wikilink")[对外贸易方面的](../Page/对外贸易.md "wikilink")[友谊之](../Page/友谊.md "wikilink")[象征](../Page/象征.md "wikilink")。商店售卖从[西方國家进口的](../Page/西方國家.md "wikilink")[商品](../Page/商品.md "wikilink")，例如花生酱和[好时巧克力](../Page/好时.md "wikilink")，以及[中国的工艺品](../Page/中国.md "wikilink")。价格要比产地的市价高很多，但由于友谊商店[垄断售卖进口货](../Page/垄断.md "wikilink")，顾客别无选择。在政治约束力影响下，开店伊始的友谊商店仅接受使用[外汇兑换券来进行商品支付](../Page/外汇兑换券.md "wikilink")。[货品包括未被审查的西方读物如](../Page/货品.md "wikilink")《纽约时报》，所以守卫会阻止任何貌似[本國居民的人进入商店](../Page/中國公民.md "wikilink")。而在門口則時常有[路人駐足圍觀](../Page/路人.md "wikilink")，並且由外往內、窥看商店里到底卖什么货品。

## 分布

### 北京/天津

| 所属企业         | 店名                                                                                                       | 地址                                                                        | 备注  |
| ------------ | -------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------- | --- |
| 北京友谊商店股份有限公司 | 北京友谊商店                                                                                                   | [北京市](../Page/北京市.md "wikilink")[建国门外大街](../Page/建国门外大街.md "wikilink")17号 | 营业中 |
| 天津一商友谊股份有限公司 | 天津一商友谊商厦                                                                                                 | [天津市](../Page/天津市.md "wikilink")[友谊路](../Page/友谊路.md "wikilink")21号       | 营业中 |
| 天津一商友谊名都     | 天津市[黄海路](../Page/黄海路.md "wikilink")19号                                                                   | 营业中                                                                       |     |
| 天津一商友谊市民百货   | 天津市[新城西路](../Page/新城西路.md "wikilink")                                                                    | 营业中                                                                       |     |
| 天津一商友谊新天地广场  | 天津市[滨江道](../Page/滨江道.md "wikilink")208号                                                                  | 营业中                                                                       |     |
| 天津一商友谊新都市百货  | 天津市[狮子林大街](../Page/狮子林大街.md "wikilink")200号                                                              | 营业中                                                                       |     |
| 天津一商友谊大港百货   | 天津市[胜利街](../Page/胜利街.md "wikilink")651号                                                                  | 营业中                                                                       |     |
| 天津一商友谊武清百货   | 天津市[武清区](../Page/武清区.md "wikilink")[京津公路与](../Page/京津公路.md "wikilink")[振华道交口](../Page/振华道.md "wikilink") | 营业中                                                                       |     |

### 上海/广州

| 所属企业                                           | 店名                                                                                                  | 地址                                                                           | 备注  |
| ---------------------------------------------- | --------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------- | --- |
| 上海友谊股份有限公司                                     | 上海友谊商店                                                                                              | [上海市](../Page/上海市.md "wikilink")[北京东路](../Page/北京东路_\(上海\).md "wikilink")40号 | 搬迁  |
| 上海[虹桥友谊商城](../Page/虹桥.md "wikilink")           | 上海市[遵义南路](../Page/遵义南路.md "wikilink")6号                                                             | 营业中                                                                          |     |
| 上海友谊百货南方店                                      | 上海市[沪闵路](../Page/沪闵路.md "wikilink")7388号                                                            | 营业中                                                                          |     |
| 上海友谊古玩商店（[曹家渡总店](../Page/曹家渡.md "wikilink")）   | 上海市[长寿路](../Page/长寿路_\(上海\).md "wikilink")1188号                                                     | 营业中                                                                          |     |
| 上海友谊古玩商店（南京东分店）                                | 上海市[南京东路](../Page/南京东路_\(上海\).md "wikilink")800号                                                    | 已停业\[1\]                                                                     |     |
| 上海友谊古玩商店（华山分店）                                 | 上海市[华山路](../Page/华山路_\(上海\).md "wikilink")9号                                                        | 营业中                                                                          |     |
| 上海友谊古玩商店（北京西分店）                                | 上海市[北京西路](../Page/北京西路_\(上海\).md "wikilink")95号                                                     | 营业中                                                                          |     |
| [广州友谊集团股份有限公司](../Page/广州友谊.md "wikilink")     | 广州友谊商店（环市东总店）                                                                                       | [广州市](../Page/广州市.md "wikilink")[环市东路](../Page/环市路_\(广州\).md "wikilink")369号 | 营业中 |
| 广州友谊商店（[正佳分店](../Page/正佳广场.md "wikilink")）     | 广州市[天河路](../Page/天河路.md "wikilink")228号                                                             | 营业中                                                                          |     |
| 广州友谊商店（[时代分店](../Page/时代广场.md "wikilink")）     | 广州市[天河北路](../Page/天河北路.md "wikilink")28号                                                            | 营业中                                                                          |     |
| 广州友谊商店（[国金分店](../Page/广州国际金融中心.md "wikilink")） | 广州市[珠江西路](../Page/珠江西路.md "wikilink")5号                                                             | 营业中                                                                          |     |
| 广州友谊商店（佛山分店）                                   | [广东](../Page/广东.md "wikilink")[佛山市](../Page/佛山市.md "wikilink")[城门头路](../Page/城门头路.md "wikilink")18号 | 已停业                                                                          |     |
| 广州友谊商店（南宁分店）                                   | [广西](../Page/广西.md "wikilink")[南宁市](../Page/南宁市.md "wikilink")[金湖路](../Page/金湖路.md "wikilink")59号   | 营业中                                                                          |     |

## 參見

  - [小白樺商店](../Page/小白樺商店.md "wikilink")，蘇聯的類似特權商店。
  - [國際商店](../Page/國際商店_\(東德\).md "wikilink")，[東德的類似商店](../Page/東德.md "wikilink")。

## 备注

## 外部链接

  - 官方

<!-- end list -->

  - [北京友谊商店](http://www.bjyysd.com.cn/)
  - [天津友谊商厦](http://www.tffstore.com/)
  - [上海友谊股份有限公司](https://web.archive.org/web/20090831103425/http://www.shfriendship.com.cn/)
  - [广州友谊集团股份有限公司](http://www.cgzfs.com/)

<!-- end list -->

  - 其它

<!-- end list -->

  - [从友谊商店沉浮看中国社会变迁](http://news.xinhuanet.com/world/2006-08/25/content_5003974.htm)
  - [《纽约时报》旅游](https://web.archive.org/web/20051122231246/http://travel2.nytimes.com/top/features/travel/destinations/asia/china/shanghai/shop_details.html?vid=1083747038391)
  - [Frommer's
    北京](http://www.frommers.com/destinations/beijing/S22189.html)
  - [Frommer's
    上海](http://www.frommers.com/destinations/shanghai/S28884.html)

[Category:中华人民共和国商店](../Category/中华人民共和国商店.md "wikilink")
[Category:中国商场](../Category/中国商场.md "wikilink")
[Category:中國零售商](../Category/中國零售商.md "wikilink")

1.  [友谊商店古玩分店昨“谢幕”](http://old.jfdaily.com/gb/node2/node142/node144/userobject1ai1395611.html)