**ESPN**（英文：**E**ntertainment **S**ports **P**rogramming
**N**etwork，娛樂與體育節目電視網，一般簡稱ESPN）是一間24小時專門播放體育節目的[美國](../Page/美國.md "wikilink")[有線電視聯播網](../Page/有線電視.md "wikilink")。最初ESPN也播放娛樂節目，後來全力發展[體育節目](../Page/體育.md "wikilink")。該企業由斯科特·拉斯穆森（Scott
Rasmussen）與其父比尔·拉斯穆森（Bill
Rasmussen）創立，並於1979年9月7日開播，當時的主席兼執行長是切特·西蒙斯（Chet
Simmons）。現任ESPN主席為George
Bodenheimer（1998年11月19日至現在）。Bodenheimer亦從2003年3月3日起兼任[ABC体育的主席](../Page/美国广播公司.md "wikilink")。其著名節目[世界體育中心](../Page/世界體育中心.md "wikilink")（SportsCenter
presented by
CDW）從首播至2002年8月25日止共播出25,000集。ESPN主要播送的節目都在[康乃狄克州](../Page/康乃狄克州.md "wikilink")[布里斯托](../Page/布里斯托尔_\(康涅狄格州\).md "wikilink")（Bristol）攝影棚作業，辦公室設在[夏洛特](../Page/夏洛特_\(北卡罗莱那州\).md "wikilink")、[舊金山和](../Page/舊金山.md "wikilink")[洛杉磯](../Page/洛杉磯.md "wikilink")（2009年啟用）。ESPN及ESPN國際在美國9000萬家庭和全球147個國家可以收看。ESPN的公司名稱由1985年2月開始由“娱乐与体育节目网”縮短為“ESPN
Inc.”，ESPN亞洲版在2013年1月28日改名為[FOX體育台](../Page/FOX體育台.md "wikilink")（中國大陸地區除外）。

ESPN和美國三大電視網之一的[ABC同屬迪士尼公司所有](../Page/美國廣播公司.md "wikilink")，因此ABC的體育轉播也冠以[ESPN
on ABC的名稱播出](../Page/ESPN_on_ABC.md "wikilink")。

## 歷史

ESPN啟播時的定位是以播放標准電視新聞和體育資訊為主。由於最初營運規模細小，所以只能經常播放非正統的體育節目，如世界最強壯男士選舉，和美國不知名的國際體育盛事，如[澳式足球賽事和短暫的美國美式足球聯盟](../Page/澳式足球.md "wikilink")（[USFL](../Page/USFL.md "wikilink")），去吸引賽事。在1987年，ESPN得到了在星期日晚上播放[NFL的合約](../Page/國家美式足球聯盟.md "wikilink")，這份合約是ESPN發展史中由小型電視網絡到大型電視帝國的轉捩點，一個創作狂熱體育文化的基本條件。ESPN在2006年開始改在星期一晚上播放NFL賽事。

ESPN最初由格蒂石油公司（由德士購得）和纳比斯科所擁有。在1984年，整個ESPN網絡和經營權由[ABC](../Page/ABC.md "wikilink")（美國廣播公司，後在1996年成為華特迪士尼的一部分）（80%）和[Heasst公司](../Page/Heasst.md "wikilink")（20%）所擁有。

在2004年，ESPN在康涅狄格布里斯托成立[高清電視中心](../Page/高清.md "wikilink")。大部份電視節目，包括世界體育中心（SportsCenter），今日棒球，NFL直播，校園賽事日等和其他ESPN播放的賽事都開始以高清標准放送。ESPN以高清標准播放的首个節目是2003年3月開始的周日晚閒棒球（Sunday
Night
Baseball），一次以德州遊騎兵和洛杉磯天使為主題的節目。第一次由數碼中心放出的節目則是在美國東岸時間2004年6月7日晚上11時由琳达·科恩和Race
Davis主持的世界體育中心。

## 音樂方面

ESPN在音樂方面曾經有几年有自己的概念，可是早期只使用原創音樂。其旗艦節目體育中心的最初音樂概念為「Pulstar」，一種听起來活躍的電子音樂，这在Vangelis于1976年出版的的專輯《Albedo
0.39》中可以找到。當有[棒球](../Page/棒球.md "wikilink")、[英式足球](../Page/英式足球.md "wikilink")、[美式足球等動畫出現時就會播放和從中心向電視螢幕周邊擴展](../Page/美式足球.md "wikilink")。

## 流行文化方面

ESPN自成立之初就已成為了流行文化的一部份。它的名稱現在已在電視和電影經過媒體廣泛使用，很多有體育項目的電視都采用ESPN的報導或節目。很多不常看體育節目的人也認識ESPN。這可能與諷刺不同ESPN營運頻道的數目有關，以下有些例子：

  - 在電影[鐵男躲避球中](../Page/鐵男躲避球.md "wikilink")，播放躲避球錦標賽的是ESPN8（"The
    Ocho"）：「如果它还是一项体育运动，我们已经找到了！」（事實上並沒有ESPN
    8"）。2017年8月8日當天，ESPNU為了致敬該場景，改名為ESPN8一天，並且轉播另類的運動比賽。

<!-- end list -->

  - 在電視"主持人：传说中的罗恩·勃艮第"的DVD特別版中，有一個諷刺的部份，包含對虛构人物新聞報導員"罗恩·勃艮第（由[Will
    Ferrell所演](../Page/Will_Ferrell.md "wikilink")）的訪問"關於1979年在ESPN的工作。

## ESPN的商業投資

ESPN的商業投資包括ESPN.com（1995開始營運至今）、ESPN杂志（1998開始營運至今）、ESPN体育杂志（2005開始營運至今）、ESPN书籍（營運至今）、ESPN的区（1998開始營運至今）、ESPN的奖项（1993開始營運至今）、移动ESPN（2006開始營運至今）、ESPN整合（2006開始營運至今）。

## ESPN網絡（美國）

ESPN在美國營運了一系列的頻道，包括：

### 電視

  - **ESPN**，1979開始營運至今，24小時體育頻道，每年播出超過5100小時直播或原創電視節目，2001年啟用高畫質版本——**ESPN
    HD**；
  - **ESPN2**，1993開始營運至今，為美國除ESPN外第二大24小時體育頻道，作為ESPN的分流頻道，99%可收看ESPN的家庭選擇同時選擇收看ESPN2的節目，每年播出超過5000小時直播或原創電視節目，2005年啟用高畫質版本——**ESPN2
    HD**；
  - **ESPNEWS**，1996開始營運至今，24小時體育新聞頻道，通過屏幕下方滾動字幕方式更新各賽事的實時比分情況，擁有眾多節目對重大體育賽事進行深度報道及評析，2008年啟用高畫質版本；
  - **ESPN Classic**，1997開始營運至今，24小時體育頻道，旨在播出歷史上的精彩體育賽事；
  - **ESPNU**，2005年3月開始營運至今，營運首年直播出超過400場體育賽事，同時播出相關新聞及深度報道節目，為大學賽事的專屬頻道，2008年啟用高畫質版本；
  - **ESPN Deportes**，2004開始營運至今，ESPN的西班牙語版本頻道，2011年啟用高畫質版本；
  - **ESPN Plus**，又稱**ESPN Regional Television**，ESPN於地方的賽事電視網；
      - **Longhorn
        Network**，2011年開始營運，與[德州大學奧斯汀分校合作的體育頻道](../Page/德州大學奧斯汀分校.md "wikilink")；
      - **SEC Network**，2014年開始營運，前身是2009年**SEC Network**2013年更名**SEC
        TV**，起先只提供當地各電視台聯播，2014年起獨立出一個新頻道並改回原名，為[NCAA東南聯盟的專屬頻道](../Page/全美大學體育協會.md "wikilink")；
  - **ESPN PPV**，1999年以**ESPN Extra**的名義開始營運，2001年改為現名；於各大隨選視訊系統提供[Pay
    per view的服務](../Page/按次付费电视.md "wikilink")；
  - **[ESPN on
    ABC](../Page/ESPN_on_ABC.md "wikilink")**，ESPN於同母公司的**[美國廣播公司](../Page/美國廣播公司.md "wikilink")（ABC）**電視網製作轉播的體育賽事及節目品牌。

### 廣播電台

  - **[ESPN广播](../Page/ESPN广播.md "wikilink")**，1992開始營運至今；
  - **ESPN Deportes Radio**，2005開始營運至今；ESPN的西班牙語版本電台。

### 網路

  - **WatchESPN**，2010年以**ESPN
    Networks**的名義開始營運，2011年改為現名；ESPN於網路上的播放平台；
      - **ESPN3**，2005年開始營運，起初以**ESPN360.com**的名義開始營運網路影音，後併入2010年成立的ESPN
        Networks，成為其專有的網路頻道，部分時段與其他姊妹頻道聯播。
  - **ESPN+**，2018年開始營運，於各OTT平台的ESPN app提供針對ESPN+的獨家內容，如賽事、原創節目等。

## ESPN在亞洲

[Cap01.JPG](https://zh.wikipedia.org/wiki/File:Cap01.JPG "fig:Cap01.JPG")

亞洲地區，在1990年代以前，ESPN即已陸續與各國頻道有零星的授權合作關係；1992年4月起亞洲台開播。\[1\]除[日本與](../Page/日本.md "wikilink")[韓國外由](../Page/韓國.md "wikilink")[ESPN
STAR
Sports經營ESPN及](../Page/ESPN_STAR_Sports.md "wikilink")[衛視體育台](../Page/衛視體育台.md "wikilink")，ESPN
卫视体育台為[星空傳媒有限公司和ESPN各占一半成立的公司](../Page/星空傳媒有限公司.md "wikilink")，[香港](../Page/香港.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[東南亞等地區的觀眾均可以收看ESPN及](../Page/東南亞.md "wikilink")[衛視體育台](../Page/衛視體育台.md "wikilink")。ESPN卫视体育台專注於[足球](../Page/足球.md "wikilink")、[籃球](../Page/籃球.md "wikilink")、[高爾夫球](../Page/高爾夫球.md "wikilink")、[網球](../Page/網球.md "wikilink")、[一級方程式賽車](../Page/一級方程式賽車.md "wikilink")、[TNA美式摔角和](../Page/TNA.md "wikilink")[板球等各項較受亞洲觀眾歡迎的體育節目](../Page/板球.md "wikilink")。ESPN
HD在台灣的[中華電信MOD有上架播出](../Page/中華電信MOD.md "wikilink")。

在香港播出時，同樣被[廣東內的](../Page/廣東.md "wikilink")[廣州有線及](../Page/廣州有線.md "wikilink")[廣東有線盜取並直接播放](../Page/廣東有線.md "wikilink")，現在已經停止播放，原有頻道被兩線的體育頻道取代。

目前在中國大陸地區，ESPN和[衛視體育台可以在三星級以上賓館以及部分外國人居住區播出](../Page/衛視體育台.md "wikilink")。早年中国大陆众多地方电视台都会转播ESPN的中文解说赛事，现已不再安排。

隨著ESPN把持有的ESPN STAR Sports所有股份售予新聞集團，ESPN正式退出亞洲，其在亞洲的ESPN頻道也已經陸續更名。

2016年，ESPN與[索尼影視娛樂於印度合資設立新的體育頻道](../Page/索尼影視娛樂.md "wikilink")****，服務範圍為印度及南亞地區。

2017年，ESPN和菲律賓的[TV5合作](../Page/TV5_\(菲律賓\).md "wikilink")，並使TV5旗下的運動節目部門Sports5改名為****。

## 相关条目

  - [ESPN STAR Sports](../Page/ESPN_STAR_Sports.md "wikilink")
  - [ESPN on ABC](../Page/ESPN_on_ABC.md "wikilink")
  - [ESPN2](../Page/ESPN2.md "wikilink")
  - [ESPNews](../Page/ESPNews.md "wikilink")
  - [腾讯视频](../Page/腾讯视频.md "wikilink")
  - [中国中央电视台体育频道](../Page/中国中央电视台体育频道.md "wikilink")
  - [虎扑体育](../Page/虎扑体育.md "wikilink")

## 註釋

## 參考資料

  - ESPN Mediakit
    (2006)．<https://web.archive.org/web/20060327112007/http://mediakit.espn.go.com/index.aspx?id=41>．於2006年2月13日查阅

  -
## 延伸閱讀

  -
## 外部連結

  - [官方網站](http://www.espn.com)
  - [ESPN電視網絡](http://sports.espn.go.com/espntv/)

[Category:ESPN](../Category/ESPN.md "wikilink")
[Category:美國電視台](../Category/美國電視台.md "wikilink")
[Category:国际媒体](../Category/国际媒体.md "wikilink")
[Category:體育電視台](../Category/體育電視台.md "wikilink")
[E](../Category/迪士尼傳媒電視網.md "wikilink")
[Category:華特迪士尼公司子公司](../Category/華特迪士尼公司子公司.md "wikilink")

1.