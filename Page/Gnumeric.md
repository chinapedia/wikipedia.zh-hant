**Gnumeric**是一款[試算表軟件](../Page/試算表.md "wikilink")，為[自由軟件](../Page/自由軟件.md "wikilink")，也是[GNOME的一部份](../Page/GNOME.md "wikilink")。Gnumeric是由Miguel
de lcaza開始編寫，現在的主理人是Jody Goldberg。

Gnumeric能夠匯入及匯出多種數據格式，例如[CSV](../Page/CSV.md "wikilink")、[Microsoft
Excel](../Page/Microsoft_Excel.md "wikilink")（.xlsx格式的寫入功能還不完全）、[HTML](../Page/HTML.md "wikilink")、[LaTeX](../Page/LaTeX.md "wikilink")、[Lotus
1-2-3](../Page/Lotus_1-2-3.md "wikilink")、[OpenDocument及](../Page/OpenDocument.md "wikilink")[Quattro
Pro等](../Page/Quattro_Pro.md "wikilink")。其原生檔案格式'Gnumeric file
format'（後綴名.gnm
.gnumeric），是一種以[gzip壓縮的](../Page/gzip.md "wikilink")[XML檔案](../Page/XML.md "wikilink")。Gnumeric尚不支援[樞紐分析表與VBA巨集](../Page/樞紐分析表.md "wikilink")

Gnumeric的精確度讓他在統計分析以及科學的應用的市場上有一些占有率，為了提升其運算的精確度，Gnumeric與[R語言的開發社群合作](../Page/R語言.md "wikilink")，精進數值計算的準確度。

Gnumeric目前有Android的版本，它是Ubuntu noroot套件的一部份

Gnumeric的1.0版本在2001年12月31日推出。

## 参见

  - [AbiWord](../Page/AbiWord.md "wikilink")——[GNOME](../Page/GNOME.md "wikilink")
    Office的文字處理軟件
  - [GnomeFiles](../Page/GnomeFiles.md "wikilink")——其他GNOME軟件
  - [Linux文档计划](../Page/TLDP.md "wikilink")
  - [使用GNOME](../Page/Wikibooks:Using_GNOME.md "wikilink")——维基书

[Category:开放源代码](../Category/开放源代码.md "wikilink")
[Category:GNOME](../Category/GNOME.md "wikilink")