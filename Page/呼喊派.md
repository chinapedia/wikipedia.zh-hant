**呼喊派**是1982年2月[浙江省](../Page/浙江省.md "wikilink")[东阳](../Page/东阳.md "wikilink")、[义乌事件发生后](../Page/义乌.md "wikilink")，[中华人民共和国政府和华人基督教界针对中国境内一些以呼喊作为宗教仪式的宗教团体的称谓](../Page/中华人民共和国政府.md "wikilink")。

## 历史

[李常受](../Page/李常受.md "wikilink")1960年移居海外以后，以[倪柝声](../Page/倪柝声.md "wikilink")“[职事](../Page/职事.md "wikilink")”的主要继承者自居。以[加州](../Page/加州.md "wikilink")[安那翰为总部](../Page/阿納海姆_\(加利福尼亞州\).md "wikilink")，传扬和实行[李常受的神学和组织思想的](../Page/李常受.md "wikilink")[召会](../Page/召会.md "wikilink")，是发源于[中国大陆](../Page/中国大陆.md "wikilink")[倪柝声发起的](../Page/倪柝声.md "wikilink")。；在[美国等英语世界](../Page/美国.md "wikilink")，“召会”以“[地方教会](../Page/地方教会.md "wikilink")”（）为名，但未融入主流基督教。在亚洲的韩国、日本、中国香港、东南亚各国，欧洲、澳洲、南美以及非洲都有分支。

大致在1970年代到1990年代之间，[地方教会在分裂过程中](../Page/地方教会.md "wikilink")，一部分分支不同意[李常受后期的教导](../Page/李常受.md "wikilink")，反对较激烈者控告其为[异端](../Page/异端.md "wikilink")。其他教派的見解不一：有接納的，亦有視為[異端的](../Page/異端.md "wikilink")。但是在海外，争论只限于神学和组织、行为模式方面，

1970年代末期[中国大陆](../Page/中国大陆.md "wikilink")[改革开放后](../Page/改革开放.md "wikilink")，在一些地方发生抢夺教堂、聚会点的纠纷。1983年，业已加入[三自爱国教会的原](../Page/三自爱国教会.md "wikilink")[上海地方教会人士](../Page/上海地方教会.md "wikilink")[唐守临](../Page/唐守临.md "wikilink")、[任钟祥根据流传入中国的美国书籍](../Page/任钟祥.md "wikilink")《神人》（后被美国法庭判定为诽谤），编写小册子《坚决抵制李常受的异端邪说》，指外籍华人基督徒教师[李常受为](../Page/李常受.md "wikilink")“呼喊派”领袖。

## 东阳义乌事件

最先报导这起事件的是一份名为《主在中华》的海外刊物，在它的创刊号里，有一封日期为1982年4月3日油印代祷信。东阳义乌事件发生後，这封信便在中国的中部及南方流传。\[1\]
信上说到1982年2月14至16日有两位三自的代表来东阳扩展三自分支。有几千位来自不同团体的基督徒不同意这事，他们在三自会场前的广场上举办了三天的露天祷告会。2月28日，三自代表煽动地方人士突袭东阳基督徒的聚会点，有些基督徒遭殴打或被石灰撒伤眼睛。信中提到在义乌也发生了同样的事，与东阳不同之处在於义乌的三自代表教唆以电棒扰乱聚会。

位在香港的一个中国基督徒观察机构─中国教会研究中心─也报导了此事；他们将东阳代祷信的部分内容翻译成英文，刊在他们1982年6月发布China
Prayer Letter中。\[2\] 1982年的6月，香港的《百姓》杂志也提到了这封信。\[3\]
中国教会研究中心更将该信再度刊於1982年7/8月的China and the
Church Today（今日中国与教会）\[4\]，并在该机构7月的通讯中指出：

`    因着不肯屈服，浙江省东阳县的教会在三自和统战部手下受到严厉的迫害。`
`    那些人告诉他们，拒绝加入三自就是不爱国，未经许可的聚集就是非法聚集。他们的蒙头帽是反革命的标记，呼求主名是他们反动的口号。`\[5\]

1982年10月的《莫忘神州》里，报导了更多迫害事件。\[6\]

就在同时，另一个由多个主流基督教派组成的跨教派中国研究联合会（Ecumenical China Study Liaison
Group），正在试图与三自和解并合作。这个联合会的成员包括道风山基督教中心、香港基督教协进会、世界信义联合会、中国研究专案（圣公会）、天主教国际情报资料中心（Pro
Mundi Vita）和其他团体。Philip Wickeri、 Edmond Tang、Arne Sovik、和Bob
Whyte等人也参与其中。这些团体出版了一些刊物，如《桥》、Religion in the People’s
Republic of China（中华人民共和国下的宗教）、《景风》等，这些刊物经常刊登三自的官方文件和声明，编辑的方向也多倾向三自的观点。

东阳义乌事件及後续逼迫的消息传开後，跨教派中国研究联合会的代表组了一个访问团到中国访问三自代表，团员中包括了香港协进会的张喣羣和香港《景风》杂志的编辑林汝升。访问团回香港後，林汝升在1982年9月道风山基督教中国文化研究社出版的《景风》杂志上写了一篇文章。\[7\]这篇文章首度将三自所取的「呼喊派」一名带到海外，也是第一次对东阳义乌事件提出不同版本，以李常受和「呼喊派」为暴动主因。（这篇文章提到李常受曾在1980年回到中国，但事实上自1949年後，李氏就从未进入中国。）香港基督教协进会的张喣羣和另一位执行委员莫树恩也写了一篇文章，题目是「东阳义乌事件的另一面」，这篇文章被刊登在1982年10月号的《信息》杂志和11月号的《七十年代》杂志。\[8\]\[9\]《七十年代》中还以「异端教派在中国大陆流播」为其标题，文中附和林汝升在《景风》里的说法。

这两篇文章将那些呼求主名的信徒，从原本守法的公民，描绘成极度反政府的危险教派。然而这种说法缺乏正当理由，任何扰乱社会秩序和反政府的活动，都违背倪柝声和李常受的教导。李常受在教导呼求主名时，从未提倡任何政府归咎「呼喊派」的脱序行为。
将倪、李定为反革命份子，更是师出无名，因为二人明言教会不应参与政治，基督徒应当服从其所在国家的政权。

为了回应这些社会主义者的报导，前英国驻中国外交官，也是海外传教团契（原中国内地会）传教士的Tony
Lambert翻译了一份名为「主在中国：东阳义乌迫害的另一观点」的文件，文件中表示：「事实上，我们有足够的证据证明，在东阳、义乌被迫害的地下教会并不是属於李常受的『地方教会』或是『呼喊派』。」\[10\]
1982年12月号的《莫忘神州》和中国教会研究中心1983年1/2月号的《中国与教会》里，也重复此一说法。\[11\]\[12\]

在接下来的几个月里，双方各说各话。1983年2月，三自的官方刊物《天风》刊登了一篇文章，作者是参与当时事件的三自委员邓福村，文章题目是「所谓『东阳义乌事件』的真相」，将整起事件归咎於「呼喊派」\[13\]
，然而当时都持相反观点。1983年4月10日的《福音报》刊了一封东阳圣徒「给主内各地肢体的信」，表示三自「对广大不与他们同流合污的信徒加以种种迫害，多方制造矛盾，捏造的事实加上异端邪『道』、『不爱国』、『反革命』等莫须有的政治罪名一概打倒」。\[14\]
由上可见「呼喊派」这词已被扩大应用到许多不愿加入三自的人身上。

## 打击「呼喊派」

中共中央统战部副部长江平接获指令，成立了一个由公安局、宗教事务局（现国家宗教事务局）和民族事务局人员共同组成的「呼喊派」调查小组。\[15\]调查小组於1983年1月15日到了他们的第一站─上海，在那里与唐守临和任锺祥会谈。\[16\]

一些信徒被组成监督思想改造的「信徒学习委员会」，并由此委员会（而非上海教会）选出唐守临和任锺祥为长老。两人在控诉大会上与政府合作，指控倪柝声为反革命分子。\[17\]
唐守临早先被选为三自常委，上海教会的成员批评他出卖教会，唐氏因辞去职务。\[18\]
在公开批判倪柝声後，唐守临恢复三自职务，成为三自的副主席。\[19\]在唐、任两人的带领下，倪柝声在时所建的南阳路大会所於1958年被交给了三自，上海市教会的聚会也因此停止。经历了文革期间的斗争和劳改後，唐守临复职为新三自的副主席和中国基督教协会的常委，\[20\]
後者由三自於1980年设立。\[21\]
1981年7月，唐守临发表了一篇声明，题目是「向党倾诉一片心」，声明中说：「我就立下一个决定：凡是对党对祖国有利的事，只要我能做的，我要做而且要做好。」\[22\]

1983年1月16日，包括唐守临、任锺祥等多位三自委员，认定「呼喊派」是政治上的反革命、宗教上的异端，应尽速处理。唐、任两人也受命写作一本驳斥「呼喊派」的书。\[23\]

1983年4月，唐守临、任锺祥写的《坚决抵制李常受的异端邪说》被金陵神学院作为教材发行。书中多项指控都出自美国的《神人》一书，该书由伪灵剖析会出版，唐守临辗转得到一本。\[24\]
《神人》在1985年被美国法院裁定为不实和诽谤。\[25\]《坚决抵制李常受的异端邪说》的全文，被转刊於1983年9月香港道风山基督教研究中心发行的《景风》杂志里。\[26\]
该机构在1983年11月发行的另一刊物《桥》里，专访了唐、任二人，《神人》一书的封面也出现在该文的第二页里。\[27\]
唐守临、任锺祥在其第二版里，还特别将《神人》列为参考资料之一。\[28\]

《坚决抵制李常受的异端邪说》出版後，一场打压「呼喊派」并批判李常受教导的全国性运动就此展开。三自在各地举办批判李常受和「呼喊派」的座谈会。\[29\]
1983年7月和9月的《天风》杂志也刊登一些文章，响应唐守临、任锺祥的指控。

文革结束留下了严重的犯罪问题，\[30\] 邓小平的经济改革又精简了工人的数量，造成大量的失业。\[31\]
1983年8月，政府开始实施「严打」（严厉打击犯罪活动），各级政府机构发布了查禁邪教「呼喊派」的公告。\[32\]
虽然逮捕的重点是地方教会中接受李常受书报的领头人，\[33\]\[34\] 许多家庭教会的带领者也因此下监。\[35\]
有些消息指出，「呼喊派」一名已被广泛应用在一切不加入三自的基督徒团体上。\[36\]\[37\]\[38\]\[39\]
中国基督教研究中心的赵天恩推测，三自可能想用唐守临和任锺祥的书，「将李常受的团体从其他基督徒中间孤立出来，此举可能会奏效，因为家庭教会带领人没有理由为了支持李常受而与三自为敌。」\[40\]

1995年又被[中共中央办公厅](../Page/中共中央办公厅.md "wikilink")、[国务院办公厅转发的](../Page/国务院办公厅.md "wikilink")[公安部内部文件定为](../Page/中华人民共和国公安部.md "wikilink")[邪教组织](../Page/中华人民共和国政府认为的邪教.md "wikilink")，继续查禁取缔\[41\]。

## 词语释意

[李常受在海外领导](../Page/李常受.md "wikilink")“[召会](../Page/召会.md "wikilink")”以后，采用“[呼求主名](../Page/呼求主名.md "wikilink")”、“[祷读](../Page/祷读.md "wikilink")”等“操练、释放灵”的方式，被反对者蔑称为“呼喊派”。中共政府在不公开的内部文件中，接受[唐守临](../Page/唐守临.md "wikilink")、[任钟祥的说法](../Page/任钟祥.md "wikilink")，认定李常受是“呼喊派”创建者，执法部门把《[圣经恢复本](../Page/圣经恢复本.md "wikilink")》及其注释版、《[晨兴圣言](../Page/晨兴圣言.md "wikilink")》以及《[生命读经](../Page/生命读经.md "wikilink")》等[李常受著作等](../Page/:分类:李常受著作.md "wikilink")[召会出版物定为邪教类违禁出版物并在执法中以其为识别](../Page/:分类:召会出版物.md "wikilink")“呼喊派”的部分依据。

许多召会人士均不承认召会与“呼喊派”概念的相关性，部分召会人士辩称“呼喊派”只是对“常受主派”（实际由召会衍生）等邪教组织的统称，而且召会人士自认为并无有关文件所说的那些严重破坏社会秩序的行为。

在聚会中有类似集体重复“呼喊”现象的部分[中国家庭教会](../Page/中国家庭教会.md "wikilink")（“[召会](../Page/召会.md "wikilink")”人士也不承认自己的组织属于“家庭教会”概念）和[灵恩派团体](../Page/灵恩派.md "wikilink")，有时也被外界错误地称作“呼喊派”。

## 事實

2013年5月23日，[金陵协和神学院教授](../Page/金陵协和神学院.md "wikilink")[王艾明博士在](../Page/王艾明.md "wikilink")《自治：中国基督教唯一合理的教制设计》\[42\]一文认为，“呼喊派”及其变种组织是在中国大陆打着[地方教会和](../Page/地方教会.md "wikilink")[李常受本人旗号的一批违法犯罪集团](../Page/李常受.md "wikilink")，而李常受则被这类非法团体冒用为教主。

2013年8月27日，[新北市](../Page/新北市.md "wikilink")[新店區召会长老](../Page/新店區.md "wikilink")[欧阳家立](../Page/欧阳家立.md "wikilink")（多次担任台湾基督教教牧参访团团长）在[两岸基督教论坛上代表地方教会正式发表声明](../Page/两岸基督教论坛.md "wikilink")：[召会不是](../Page/召会.md "wikilink")“呼喊派”，全球超过四千处的地方教会都与“呼喊派”无关。2014年6月11日，[台北市召会就](../Page/台北市召会.md "wikilink")[中国反邪教协会针对](../Page/中国反邪教协会.md "wikilink")[山东省](../Page/山东省.md "wikilink")[招远市之](../Page/招远市.md "wikilink")“[全能神](../Page/全能神.md "wikilink")”邪教份子当众殴打妇人致死事件而将召会视为邪教之一“呼喊派”\[43\]提出声明：[地方教会不是](../Page/地方教会.md "wikilink")“呼喊派”，[李常受没有成立过](../Page/李常受.md "wikilink")“呼喊派”\[44\]。

## 衍生教派

按照[倪柝声关于](../Page/倪柝声.md "wikilink")“[地方教会](../Page/地方教会.md "wikilink")”的理论和[李常受关于](../Page/李常受.md "wikilink")“[召会](../Page/召会.md "wikilink")”的理论，每个地方教会独立自治,
各自向元首（即“[教会](../Page/教会.md "wikilink")/[召会](../Page/召会.md "wikilink")”的头，[耶稣](../Page/耶稣.md "wikilink")[基督](../Page/基督.md "wikilink"))负责；各地方教会，无论人数多寡，地方大小，一律平等；各地方教会之间只有属灵的关系，没有组织的关系；没有[罗马天主教会那样的](../Page/罗马天主教会.md "wikilink")[阶级制度和权力结构](../Page/天主教会圣统制.md "wikilink")，地方教会没有总会，也没有“最高领导人”。作为“神的仆人”李常受的责任就是供应真理和生命。至于各地方教会接受他的教训与否，或接受到什么程度，则完全由那一地的教会自己决定。并且，各地教会照自己所领悟的，负责教导和实行。至于采用什么样的形式、步骤，达到什么样的结果，[神的仆人并不参与或干涉](../Page/神的仆人.md "wikilink")。一些有野心及别有用心之人利用部分信徒对圣经认识不足，曲解正统信仰内容或在实行上有了偏差甚至走了极端，从召会分裂出来，衍生出一些异端教派，甚至有不法行为、扰乱社会秩序治安，被官方定为邪教组织，包括：

  - [常受主派](../Page/常受主派.md "wikilink")（[常受教](../Page/常受教.md "wikilink")）
  - [中华大陆行政执事站](../Page/中华大陆行政执事站.md "wikilink")
  - [被立王](../Page/被立王.md "wikilink")
      - 派生：[主神教](../Page/主神教.md "wikilink")
  - [全能神教会](../Page/全能神教会.md "wikilink")（[东方闪电](../Page/东方闪电.md "wikilink")、[实际神](../Page/实际神.md "wikilink")）

这些邪教组织的一个共同特征是[神化教主](../Page/神化.md "wikilink")；这被认为与李常受的“神成为人（指基督道成肉身）、为使人在生命和性情上、但不在神格上成为神。”的说法相悖。因这些衍生教派与[李常受系统](../Page/李常受.md "wikilink")[召会的渊源](../Page/召会.md "wikilink")，召会相当程度地被牵连。

## 参考文献

## 外部链接

  - 持守真实 - 呼喊派，常受主派与东方闪电

<!-- end list -->

  - [为何国内地方召会的信徒被称为“呼喊派”](https://holdtruthinlove.org/貳-為何國內地方召會的信徒被稱為呼喊派)

<!-- end list -->

  - 召会“[圣徒](../Page/圣徒.md "wikilink")”（会徒自称）否认“呼喊派”所指

<!-- end list -->

  - [我們是地方召會不是所謂『呼喊派』](https://web.archive.org/web/20140831020918/http://www.cftfc.com/com_chinese/heresies/reading.asp?title_no=3-08&chap_no=3-08-01)
  - [关于李常受的教导](http://www.witnessleeteaching.com)

<!-- end list -->

  - 华人基督徒评述

<!-- end list -->

  - [淺探聚會所 - 李常受帶領下的發展與轉變](http://ccn.fhl.net/editorial/LC.htm)

<!-- end list -->

  - 美国基督教研究院（Christian Research Institute）

<!-- end list -->

  - [我们错了-重新评估倪柝声、李常受的“地方教会”运动](http://www.equip.org/PDF/SimplifiedChineseOpt.pdf)

<!-- end list -->

  - [凯风网](../Page/凯风网.md "wikilink")

<!-- end list -->

  - [俄政府取缔非法渗透入境并从正教会抢羊的“呼喊派”活动](https://web.archive.org/web/20120525224848/http://www.kaiwind.com/hwzs/ywcz/201204/t143320.htm)
    [俄语原文](http://gazeta.aif.ru/_/online/saratov/635/02_04)
  - [“呼喊派”被俄罗斯专家列入国际极权主义教派名单](https://web.archive.org/web/20130101145633/http://www.zhanlu.org.cn/eastday/kl/xjdg/sjxj/u1a959169.html)
    [俄语原文](https://web.archive.org/web/20111210021040/http://spisok-sekt.ru/Pomestnaja-tserkov-Uitnessa-Li.html)
  - [乌克兰原“呼喊派”翻译叶夫根尼的的回忆](https://web.archive.org/web/20130804000737/http://www.kaiwind.com/hwzs/ywcz/201204/06/t20120406_863015.htm)[俄语原文](https://web.archive.org/web/20120511155939/http://iriney.ru/sects/li/002.htm)
  - [李常受与“地方召会”的骗局](http://news.xinhuanet.com/politics/2013-09/04/c_125320577.htm)

## 参见

  - [邪教](../Page/邪教.md "wikilink")、[中华人民共和国政府认定的邪教组织列表](../Page/中华人民共和国政府认定的邪教组织列表.md "wikilink")
  - [李常受](../Page/李常受.md "wikilink")、[召会](../Page/召会.md "wikilink")、[召会相关争议](../Page/召会相关争议.md "wikilink")
  - [地方教会](../Page/地方教会.md "wikilink")
  - [中国基督教史](../Page/中国基督教史.md "wikilink")、[中国基督教新教](../Page/中国基督教新教.md "wikilink")、[新兴宗教](../Page/新兴宗教.md "wikilink")

{{-}}

[Category:中国新兴宗教](../Category/中国新兴宗教.md "wikilink")
[Category:中国基督教新教教派](../Category/中国基督教新教教派.md "wikilink")
[Category:汉语基督教术语](../Category/汉语基督教术语.md "wikilink")
[Category:中华人民共和国特定人群称谓](../Category/中华人民共和国特定人群称谓.md "wikilink")
[Category:中华人民共和国基督教史](../Category/中华人民共和国基督教史.md "wikilink")
[Category:被中华人民共和国政府认定为邪教的团体](../Category/被中华人民共和国政府认定为邪教的团体.md "wikilink")

1.  主在中华, n.d..
2.  CCRC, China Prayer Letter, June 1982.
3.  黄亚可, 「宗教自由与政治迫害的交锋」,百姓, 第25期, 1982年6月1日. 此文以主在中华为其资料来源.
4.  “Persecution in Dongyang, Zhejiang Province; translation of an
    internal circular issued by house churches in central China.” China
    and the Church Today, July/August 1982.
5.  `CCRC``   ``News,``   ``July``   ``1982;``   ``reprinted``   ``in``
     ``China``   ``Prayer``   ``Letter``   ``24,``   ``September``
     ``1982.`
6.  莫忘神州, 1982年10月.
7.  林汝升, 「中国教会二三事」, 景风, 第71期, 1982年9月
8.  张煦羣、莫树恩, 「东阳义乌事件的另一面」,信息, 1982年10月
9.  张煦羣、莫树恩, 「异端教派在中国大陆流播─东阳义乌事件的另一面」,七十年代, 1982年11月
10. “The Lord in China: The Dongyang Yiwu Persecution: Another View,”
    translated by Tony Lambert, December 1982
11. 莫忘神州, 1982年9月
12. 中国与教会，第26期，1983年1-2月
13. 邓福村,「所谓『东阳义乌事件』的真相」,天风, 1983年2月. 这篇文章被译成英文并刊於Religion in the
    People’s Republic of China: Documentation 12, 1983年10月, 20-21.
14. 「给主内各地肢体的信」(来自浙江东阳教会), 福音报, 1157期, 1983年4月10日
15. 江平, 「呼喊派問題調查」, 考察調查旅遊日記輯要, 北京華文出版社, 2008年, 1–28
16. 張錫康回憶錄 (2012), p. 393
17. 张锡康回忆录 (2012), p. 202.
18. 张锡康回忆录 (2012), pp. 222－23
19. Francis P. Jones, ed., Documents of the Three-Self Movement: Source
    Materials for the Study of the Protestant Church in Communist China
    (New York: Far Eastern Office, Division of Foreign Missions,
    National Council of the Churches of Christ in the USA, 1963), 96,
    200.
20. Office of the East Asia and Pacific Division, National Council of
    the Churches of Christ in the USA, The People & Church in China: A
    Guide to Prayer (New York: Friendship Press for the China Program,
    1982), 9.
21. “A Statement of the Third National Congress of the Chinese
    Protestant Church, 13 October 1980,” LWF Marxism & China Study 31,
    November 1980:12－14.
22. 唐守临, 「向党倾诉一片心」, 1981年7月1日
23. 江平, 「呼喊派问题调查」, 考察调查旅游日记辑要, 北京华文出版社, 2008年, 1－28
24. 张锡康回忆录 (2012), pp. 388, 350
25. Seyranian, Leon G, "Statement of Decision", Lee et al v. Duddy et
    al, Superior Court of the State of California in and for the County
    of Alameda, Case no. 540 585－9
26. 唐守临、任锺祥, 「坚决抵制李常受的异端邪说」, 景风, 1983年9月, 15－18
27. 「从呼喊派说起─唐守临、任锺祥访问记」, 桥, 第2期, 1983年11月, 16－18
28. 唐守临、任锺祥, 为真道竭力争辩, 上海市基督教教务委员会, 1983年12月, p. 9
29. 「全国两会召开座谈会─座谈《坚决抵制李常受的异端邪说》」, 天风，1983年, 第4期, 12
30. The October and November 1983 issues of Zheng Ming contain several
    articles describing the Anti-Crime Campaign; its association with
    the persecution of Christians is discussed in “The Ins and Outs of
    the Persecution of the Church,” China Watch 60, May/June 1984.
31. Richard Baum, “The Fall and Rise of China, Lecture 38: The Fault
    Lines of Reform, 1984－1987” (Chantilly, VA: The Teaching Company,
    2010)
32. Xiangshan County People’s Government in Zhejiang Province, “Notice
    on the Suppression of the Reactionary ‘Shouters’ Organization,”
    August 15, 1983; Guangzhou Municipal People’s Government, “Notice
    Concerning Arresting the Counter-revolutionary Group ‘The
    Shouters’,” December 1, 1983
33. Overseas Missionary Fellowship, Pray for China Fellowship, December
    1983.
34. Chinese Church Research Center, China Prayer Letter 39.
35. China Prayer Letter 38, November 1982.
36. CCRC, China Prayer Letter 48, September 1984.
37. David H. Adeney, China: The Church’s Long March (Ventura, CA: Regal
    Books, 1985), 159.
38. Tony Lambert, China’s Christian Millions (London: Monarch Books,
    2000), 81.
39. Human Rights Watch, China: State Control of Religion (New York:
    Human Rights Watch, 1997), 31.
40. Jonathan Chao, China News and Church Report 8, June 10, 1983.
41. 1995年11月，中央办公厅、国务院办公厅下发《关于转发〈公安部关于查禁取缔" 呼喊派"
    等邪教组织的情况及工作意见〉的通知》（厅字\[1995\]50号），明确“呼喊派”（包括其演变出的"“常受教”
    、“中华大陆行政执事站”、“能力主”、“实际神”等派系）、“主神教”
42.
43.
44. [基督教论坛报2014年6月11日报道：“召会严重抗议在大陆被误导为呼喊派”](http://www.ct.org.tw/news/detail/2014-01614)