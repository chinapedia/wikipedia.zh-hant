**克萊爾郡** （County Clare；[愛爾蘭語](../Page/愛爾蘭語.md "wikilink")：Contae an
Chláir）是[愛爾蘭的一個郡](../Page/愛爾蘭共和國.md "wikilink")，位於[愛爾蘭島西岸](../Page/愛爾蘭島.md "wikilink")，東以[德格湖](../Page/德格湖.md "wikilink")-[香農河為界](../Page/香農河.md "wikilink")。歷史上屬[芒斯特省](../Page/芒斯特省.md "wikilink")。面積3,147平方公里。2011年人口117,196人。首府[恩尼斯](../Page/恩尼斯.md "wikilink")。\[1\]

## 地理和行政區劃

克萊爾位於[香農河西北方](../Page/香農河.md "wikilink")，總面積約3400平方公里（1300平方英里）。克萊爾是愛爾蘭傳統32郡中，面積第七大，人口排名第19名。克萊爾的綽號為**旗幟縣**。\[2\]

### 城鎮和村莊

  - [Ardnacrusha](../Page/Ardnacrusha_\(village\).md "wikilink")
  - [Ballynacally](../Page/Ballynacally.md "wikilink")
  - [Ballyvaughan](../Page/Ballyvaughan.md "wikilink")
  - [Barefield](../Page/Barefield.md "wikilink")
  - [Broadford](../Page/Broadford,_County_Clare.md "wikilink")
  - [Bunratty](../Page/Bunratty.md "wikilink")
  - [Carrigaholt](../Page/Carrigaholt.md "wikilink")
  - [Carron](../Page/Carron,_County_Clare.md "wikilink")
  - [Clarecastle](../Page/Clarecastle.md "wikilink")
  - [Clonlara](../Page/Clonlara.md "wikilink")
  - [Connolly](../Page/Connolly,_County_Clare.md "wikilink")
  - [Cooraclare](../Page/Cooraclare.md "wikilink")
  - [Corofin](../Page/Corofin,_County_Clare.md "wikilink")
  - [Cratloe](../Page/Cratloe.md "wikilink")
  - [Cree (Creegh)](../Page/Cree_\(Creegh\).md "wikilink")
  - [Cross](../Page/Cross.md "wikilink")
  - [Crusheen](../Page/Crusheen,_County_Clare.md "wikilink")
  - [Doolin](../Page/Doolin.md "wikilink")
  - [Doonaha](../Page/Doonaha.md "wikilink")
  - [Doonbeg](../Page/Doonbeg.md "wikilink")
  - [Doora](../Page/Doora.md "wikilink")
  - [Ennis](../Page/Ennis.md "wikilink")
  - [Ennistymon](../Page/Ennistymon.md "wikilink")
  - [Fanore](../Page/Fanore.md "wikilink")
  - [Feakle](../Page/Feakle,_County_Clare.md "wikilink")
  - [Inagh](../Page/Inagh.md "wikilink")
  - [Inch](../Page/Inch,_County_Clare.md "wikilink")
  - [Kilbaha](../Page/Kilbaha.md "wikilink")
  - [Kilfenora](../Page/Kilfenora.md "wikilink")
  - [Kilkee](../Page/Kilkee.md "wikilink")
  - [Kildysart](../Page/Kildysart.md "wikilink")
  - [Killaloe](../Page/Killaloe,_County_Clare.md "wikilink")
  - [Killimer](../Page/Killimer.md "wikilink")
  - [Kilmaley](../Page/Kilmaley.md "wikilink")
  - [Kilmihil](../Page/Kilmihil.md "wikilink")
  - [Kilmurry McMahon](../Page/Kilmurry_McMahon.md "wikilink")
  - [Kilnaboy](../Page/Kilnaboy.md "wikilink")
  - [Kilnamona](../Page/Kilnamona.md "wikilink")
  - [Kilrush](../Page/Kilrush.md "wikilink")
  - [Lahinch](../Page/Lahinch.md "wikilink")
  - [Liscannor](../Page/Liscannor.md "wikilink")
  - [Lisdoonvarna](../Page/Lisdoonvarna.md "wikilink")
  - [Lissycasey](../Page/Lissycasey.md "wikilink")
  - [Meelick](../Page/Meelick,_County_Clare.md "wikilink")
  - [Miltown Malbay](../Page/Miltown_Malbay.md "wikilink")
  - [Mountshannon](../Page/Mountshannon.md "wikilink")
  - [Mullagh](../Page/Mullagh,_County_Clare.md "wikilink")
  - [Newmarket-on-Fergus](../Page/Newmarket-on-Fergus.md "wikilink")
  - [O'Briensbridge](../Page/O'Briensbridge.md "wikilink")
  - [Ogonnelloe](../Page/Ogonnelloe.md "wikilink")
  - [Parteen](../Page/Parteen.md "wikilink")
  - [Quilty](../Page/Quilty,_County_Clare.md "wikilink")
  - [Quin](../Page/Quin,_County_Clare.md "wikilink")
  - [Ruan](../Page/Ruan,_County_Clare.md "wikilink")
  - [Scariff](../Page/Scariff.md "wikilink")
  - [Shannon](../Page/Shannon_Town.md "wikilink")
  - [Sixmilebridge](../Page/Sixmilebridge.md "wikilink")
  - [Tuamgraney](../Page/Tuamgraney.md "wikilink")
  - [Tubber](../Page/Tubber.md "wikilink")
  - [Tulla](../Page/Tulla.md "wikilink")
  - [Whitegate](../Page/Whitegate,_County_Clare.md "wikilink")

### 自然地理

[Cliffs_of_Moher,_Clare.jpg](https://zh.wikipedia.org/wiki/File:Cliffs_of_Moher,_Clare.jpg "fig:Cliffs_of_Moher,_Clare.jpg")\]\]
克萊爾郡的邊界大至由水域劃分。東南方是愛爾蘭最長的河流[香農河](../Page/香農河.md "wikilink")，郡的南方則是[香農河口](../Page/香農河口.md "wikilink")。郡界向東北方向為[德格湖是愛爾蘭第三大湖泊](../Page/德格湖.md "wikilink")。西面是[大西洋](../Page/大西洋.md "wikilink")，北部至[戈爾韋灣](../Page/戈爾韋灣.md "wikilink")。
克萊爾郡內的[布倫](../Page/布倫.md "wikilink")，是獨特的喀斯特地貌區，區內有珍貴花卉和物種。布倫的西部面至大西洋，這裡有著名的[莫赫懸崖](../Page/莫赫懸崖.md "wikilink")。克萊爾郡的最高點位於[瑪伊歐莎山](../Page/瑪伊歐莎山.md "wikilink")（532米），[斯里貝爾納山](../Page/斯里貝爾納山.md "wikilink")\[3\]
位於郡的東部。下列島嶼位在郡的外海：

  -
  -
  -
  - [斯卡特律島](../Page/斯卡特里島.md "wikilink")

### 氣候

## 資料來源

### 註釋

### 參考文獻

<div class="references-small" style="-moz-column-count:2; column-count:2;">

  -
  -
  -

</div>

## 外部連結

  - [War Of Independence in
    Clare](https://web.archive.org/web/20100310060013/http://www.warofindependence.net/)
  - [Clare County Council](http://www.clarecoco.ie/)
  - [Clare County Library](http://www.clarelibrary.ie/)
  - [Tourist
    Attractions](http://www.clare-rosecottage.com/news/10-great-reasons-to-visit-co-clare-in-the-west-of-ireland/)

[\*](../Category/克萊爾郡.md "wikilink")
[Category:愛爾蘭共和國的郡](../Category/愛爾蘭共和國的郡.md "wikilink")
[Category:芒斯特省](../Category/芒斯特省.md "wikilink")

1.
2.
3.  NB: not related to the [Slieve
    Bearnagh](../Page/Slieve_Bearnagh.md "wikilink") mountain in County
    Down.