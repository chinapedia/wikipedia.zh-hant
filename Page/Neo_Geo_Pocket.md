**Neo Geo
Pocket**（简称**NGP**）是[SNK第一款](../Page/SNK.md "wikilink")[攜帶型遊戲機](../Page/攜帶型遊戲機.md "wikilink")，於1998年10月28日在[日本發售](../Page/日本.md "wikilink")，但低於預期的銷售額導致在1999年停產，並立即被其後繼機種[Neo
Geo Pocket
Color取代](../Page/Neo_Geo_Pocket_Color.md "wikilink")。該系統只在日本與[香港市場以零售的方式發售](../Page/香港.md "wikilink")。
儘管該系統只有很短的壽命，其間仍發售了一些顯著的遊戲，如[侍魂\!與](../Page/侍魂.md "wikilink")[拳皇R-1等](../Page/拳皇.md "wikilink")。
Neo Geo
Pocket大致上可向上兼容，意即可執行大部分新的彩色遊戲，但亦有如[音速小子大冒險Pocket與](../Page/刺蝟索尼克.md "wikilink")[SNK
vs. Capcom：千年之戰等例外](../Page/SNK_vs._Capcom.md "wikilink")。Neo Geo Pocket
Color完全向下兼容並可執行所有為Neo Geo Pocket發佈的遊戲。

## 規格

  - CPU：16位[東芝TLCS](../Page/東芝.md "wikilink")-900H高性能核心[中央處理器](../Page/中央處理器.md "wikilink")
    。
  - 内置记忆体：32位／16位寄存器庫配置於6.144[兆赫](../Page/兆赫.md "wikilink")。
  - 屏幕解析度：Virtual屏幕160×152 - 16 alettes/plane、64 sprites/frame。
  - 声效：[SN76489声效芯片](../Page/SN76489.md "wikilink")（3路[矩形波音频发生器](../Page/矩形波.md "wikilink")，1路[白噪音发生器](../Page/白噪音.md "wikilink")，2个直接存取式数字-模拟转换器），由8-bit位[Z80处理器控制](../Page/Z80.md "wikilink")。
  - 总线：I/O串行纜線1條頻道每秒19200位。
  - 电源：使用两节AA型电池或外接AC适配器提供电力，使用一枚[CR2032型纽扣电池以保存日期](../Page/CR2032.md "wikilink")，设置等其他数据。
  - 耗电量：0.2w
  - 电池寿命：提供约20小时游戏时间，[CR2032锂电池寿命约为](../Page/CR2032.md "wikilink")1年。
  - 主机尺寸：122mm（长）×74mm（宽）×26.5mm（高）。（包含凸起部分）
  - 主机重量：约160g（含电池）
  - 主机型号：NEOP-A1

## POCKET菜单界面

在卡带没有插入的情况下，接通Neo Geo Pocket的电源，将会显示POCKET菜单界面，在该画面上可以选择5个功能菜单。

  - CALENDAR（日历）

可以预览1999年到2090年的日历

  - WORLDTIME（世界时间）

可以查看世界各地的时间

  - HOROSCOPE（星座占卦）

输入自己的生日后，可以根据系统时间在计算当天的星座运势（分为金钱、健康、爱情、总评四部分）

  - ALARM（闹铃）

设定闹钟

  - SETTING（设置）

可以重新设定系统语言（日语或英语）、日期、时间。

## 相关产品

  - [Neo Geo Pocket
    Color](../Page/Neo_Geo_Pocket_Color.md "wikilink")（具有彩色屏幕的NGP进化机种）

## 外部連結

  - [gpark.nl上的Neo Geo
    Pocket遊戲機與遊戲介紹（[荷蘭語](../Page/荷蘭語.md "wikilink")）](https://archive.is/20060627211607/http://www.gpark.nl/Neo_Geo_Pocket_Color__info.php)
  - [Planet-SNK.de](http://www.planet-snk.de/) "All about NeoGeo Pocket
    and SNK Playmore\!"

## 参考资料

  - NEOGEO POCKET 使用说明书

[Category:掌上遊戲機](../Category/掌上遊戲機.md "wikilink")
[Category:SNK](../Category/SNK.md "wikilink")
[Category:第五世代遊戲機](../Category/第五世代遊戲機.md "wikilink")
[Category:1998年面世的產品](../Category/1998年面世的產品.md "wikilink")