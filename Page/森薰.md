**森薰**（，），是[日本女性](../Page/日本.md "wikilink")[漫畫家](../Page/漫畫家.md "wikilink")。[東京都出身](../Page/東京都.md "wikilink")。

## 人物

曾用「縣文緒」、「杉良子」名義參與[同人誌活動](../Page/同人誌.md "wikilink")，及至2001年底在《[Comic
Beam](../Page/Comic_Beam.md "wikilink")》雜誌（[Enterbrain](../Page/Enterbrain.md "wikilink")）出道，陸續畫了以[維多利亞時代](../Page/維多利亞時代.md "wikilink")[女傭為主角之作品](../Page/女傭.md "wikilink")。

代表作《[艾瑪](../Page/艾瑪_\(漫畫\).md "wikilink")》獲得2005年第9屆[文化廳媒體藝術祭漫畫優秀獎](../Page/文化廳媒體藝術祭.md "wikilink")。其《[姊嫁物語](../Page/姊嫁物語.md "wikilink")》亦奪下2012年第39屆[安古蘭國際漫畫節跨世代獎](../Page/安古蘭國際漫畫節.md "wikilink")，以及漫畫大獎2014。

分別於2006年8月13日\[1\]、2012年12月5日\[2\]應邀來[台灣舉行簽名會](../Page/台灣.md "wikilink")：前者地點為2006年（第7屆）[漫畫博覽會](../Page/漫畫博覽會.md "wikilink")，後者地點為[太平洋崇光百貨臺北忠孝館](../Page/太平洋崇光百貨.md "wikilink")13樓。

## 作品列表

<div class="notice metadata"  id="spoiler">

<small>**※中文版由[台灣角川發行](../Page/台灣角川.md "wikilink")**</small>

</div>

  - [雪莉](../Page/雪莉_\(漫畫\).md "wikilink")（，不定期刊載，同人誌：1999年～2001年、單行本：2003年～）
  - [艾瑪](../Page/艾瑪_\(漫畫\).md "wikilink")（，全十冊，2002年～2008年）
  - Emma Victorian Guide 艾瑪 維多利亞導讀本（，全一冊，2003年）
  - [姊嫁物語](../Page/姊嫁物語.md "wikilink")（，連載中，2008年～）
  - 森薰拾遺集（，全一冊，2012年）

### 短篇

  - （『コミックビーム』2004年9月號附屬小冊子） -
    原作：[福島聰](../Page/福島聰.md "wikilink")、作画：森薰。福島聰的單行本《虎鵜之城》（）中收錄。

  - [シャーリー・メディスン](../Page/雪莉_\(漫畫\).md "wikilink")（『[コミックビームFellows\!](../Page/Harta_\(漫畫雜誌\)#コミックビームFellows!.md "wikilink")』Vol.2、『Fellows\!』volume10B・11B、『ハルタ』volume2・3・14・17）

  - [カバー・ストーリー](../Page/Fellows!#連載中.md "wikilink")（『Fellows\!』volume4）

  - \[3\]（『[Fellows\!総集編
    乙嫁語り&乱と灰色の世界](../Page/Harta_\(漫畫雜誌\)#特別企劃本.md "wikilink")』）

  - モードリン・ベイカー（『Fellows\!』volume5附屬小冊子） -
    [アンソロジーコミック](../Page/アンソロジーコミック.md "wikilink")『[Awesome
    Fellows\!](../Page/Harta_\(漫畫雜誌\)#特別企劃本.md "wikilink")』中收錄。

  - （『[Swimsuits Fellows\!
    2009](../Page/Harta_\(漫畫雜誌\)#特別企畫本.md "wikilink")』）

  - （『Fellows\!』volume8附屬小冊子） - アンソロジーコミック『Awesome Fellows\!』中收錄。

  - ブカちゃん（『[Costume Fellows\!
    2011](../Page/Harta_\(漫畫雜誌\)#主な応募者全員サービス.md "wikilink")』）

  - （『Fellows\!』volume16D）

  - （バロウ・ジェントルマンズ・クラブ）（『Fellows\!』volume17付属小冊子）

  - ガゼル（『Fellows\!』volume24附屬小冊子） - 單行本未收錄。

  - エンターブレイン刊fellows\!改め『Harta』創刊のお知らせ\[4\]（『[ゲッサン](../Page/ゲッサン.md "wikilink")』2013年3月号）
    - 單行本未收錄。

  - お茶のおけいこ（『[U12
    こどもフェローズ](../Page/Harta_\(漫畫雜誌\)#主な応募者全員サービス.md "wikilink")』）
    - 單行本未收錄。

### 書籍

#### 漫畫單行本

  - 《[艾瑪](../Page/艾瑪_\(漫畫\).md "wikilink")》（エマ）、[Enterbrain](../Page/Enterbrain.md "wikilink")
    〈ビームコミックス〉 2002年 - 2008年、全10卷 [台灣角川出版](../Page/台灣角川.md "wikilink")
  - 《[雪莉](../Page/雪莉_\(漫畫\).md "wikilink")》（シャーリー）、Enterbrain 〈ビームコミックス〉
    2003年 - 、共2卷 台灣角川出版
      - 出道前的同人作品收錄。同人誌時期：1999年～2001年（第1卷）
  - 《[姐嫁物語](../Page/姐嫁物語.md "wikilink")》（）、發行：Enterbrain /
    發售：[角川グループパブリッシング](../Page/角川グループパブリッシング.md "wikilink")
    〈ビームコミックス〉 2009年 - 刊行中、共8卷 台灣角川出版
  - 《森薫拾遺集》、發行：Enterbrain / 發售：角川グループパブリッシング 〈ビームコミックス〉
    2012年2月27日初版發行（2月15日發售）、短編集 ISBN 978-4-04-727824-0

#### 小說原作・插畫

  - 『[小說艾瑪](../Page/艾瑪_\(漫畫\)#小説.md "wikilink")』、Enterbrain
    〈[Fami通文庫](../Page/Fami通文庫.md "wikilink")〉 2005年、全2卷 -
    著：[久美沙織](../Page/久美沙織.md "wikilink")

#### 相關書籍

  - 《Emma Victorian Guide 艾瑪 維多利亞導讀本》（[エマ
    ヴィクトリアンガイド](../Page/艾瑪_\(漫畫\)#相關書籍.md "wikilink")）、Enterbrain
    〈ビームコミックス〉 2003年、全1卷 - 著：森薫・[村上リコ](../Page/村上リコ.md "wikilink")
  - 『[エマ アニメーションガイド](../Page/艾瑪_\(漫畫\)#動畫相關書籍.md "wikilink")』、Enterbrain
    〈ビームコミックス〉 2005年 - 2006年、全3卷 -
    著：森薫・村上リコ、監修：[小林常夫](../Page/小林常夫.md "wikilink")

## 參考資料

## 延伸閱讀

  -

## 外部連結

  -

  -

[Category:日本漫画家](../Category/日本漫画家.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")

1.  [《艾瑪》作者森薰老師簽名會\!\!](http://www.mightymedia.com.tw/news/bnews_detail.asp?bnewsid=373).
    [曼迪傳播](../Page/曼迪傳播.md "wikilink").
2.  [森薰老師簽名會　活動相關資訊](http://www.junkudo.tw/info/20121123).
    [淳久堂書店](../Page/淳久堂書店.md "wikilink") 台北忠孝店.
3.  第一話初出的同人誌。
4.  『Fellows\!』到『Harta』中雜誌名變更した事に伴う4ページのPR漫画。