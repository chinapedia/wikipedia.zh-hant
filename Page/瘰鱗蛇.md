**瘰鱗蛇**（[學名](../Page/學名.md "wikilink")：**''Acrochordus
granulatus**''）又名**疣鱗蛇**，主要分布於[東南亞](../Page/東南亞.md "wikilink")、[印度以及](../Page/印度.md "wikilink")[所羅門群島一帶](../Page/所羅門群島.md "wikilink")。瘰鱗蛇是一個完全生活於[水中的蛇種](../Page/水.md "wikilink")，暫時未有任何亞種被確認。

## 解剖學描述

瘰鱗蛇是水棲的蛇種，在陸地上幾近完全喪失行動能力。牠們的鱗皮表面質地雖然相當粗糙，但其實皮層偏薄，容易撕裂。瘰鱗蛇是[同種異形的](../Page/同種異形.md "wikilink")，雌雄各有不同型態，雄性瘰鱗蛇體型較小，身體顏色的深淺對比亦較為明顯。瘰鱗蛇經常出沒於河塘、沼澤等地，亦分布於海洋。

## 生態學描述

瘰鱗蛇主要分布於[印度半島及](../Page/印度半島.md "wikilink")[東南亞地帶](../Page/東南亞.md "wikilink")，亦出沒於[印尼](../Page/印尼.md "wikilink")[澳洲海域的](../Page/澳洲.md "wikilink")[群島](../Page/群島.md "wikilink")，與及澳洲北部的[所羅門群島等](../Page/所羅門群島.md "wikilink")。這些地區包括[緬甸](../Page/緬甸.md "wikilink")、[安達曼群島](../Page/安達曼群島.md "wikilink")、[尼科巴群島](../Page/尼科巴群島.md "wikilink")、[泰國](../Page/泰國.md "wikilink")、[柬埔寨](../Page/柬埔寨.md "wikilink")、[越南](../Page/越南.md "wikilink")、[中國](../Page/中國.md "wikilink")（主要指[海南島](../Page/海南島.md "wikilink")）、[菲律賓](../Page/菲律賓.md "wikilink")（[呂宋](../Page/呂宋.md "wikilink")、[宿霧等](../Page/宿霧.md "wikilink")）、[馬來西亞](../Page/馬來西亞.md "wikilink")、[印尼](../Page/印尼.md "wikilink")（[蘇門答臘](../Page/蘇門答臘.md "wikilink")、[爪哇](../Page/爪哇.md "wikilink")、[婆羅洲](../Page/婆羅洲.md "wikilink")、[蘇拉威西島](../Page/蘇拉威西島.md "wikilink")、[弗洛勒斯島](../Page/弗洛勒斯島.md "wikilink")、[帝汶等](../Page/帝汶.md "wikilink")）、[巴布亞新畿內亞](../Page/巴布亞新畿內亞.md "wikilink")、澳洲北部所羅門群島與及[昆士蘭東部](../Page/昆士蘭.md "wikilink")。

## 行為學描述

瘰鱗蛇主要進食[魚類](../Page/魚類.md "wikilink")。根據希恩（Shine,
1991）的瘰鱗蛇研究，指半數的瘰鱗蛇[胃部裡曾發現過彩虹魚](../Page/胃部.md "wikilink")、鱸魚等魚類，偶爾也會進食[鰻魚](../Page/鰻魚.md "wikilink")，而暫時仍未有發現瘰鱗蛇進食[兩棲動物的證據](../Page/兩棲動物.md "wikilink")。牠們粗糙的皮膚主要用途是為了抵禦水壓，便於在海底獵食。雌性瘰鱗蛇主要以伏擊的形式捕食獵物，相對地雄性瘰鱗蛇則會比較主動地搜掠獵物。

## 外部連結

  - [瘰鱗蛇專門網站](http://www.acrochordus.com/)

  - [瘰鱗蛇圖像](https://web.archive.org/web/20090918124426/http://itgmv1.fzk.de/www/itg/uetz/herp/photos/Acrochordus_granulatus.jpg)

[granulatus](../Category/瘰鳞蛇属.md "wikilink")