**真如**（[巴利文與](../Page/巴利文.md "wikilink")[梵文](../Page/梵文.md "wikilink")：）或**實如**（[梵文](../Page/梵文.md "wikilink")：），又譯為**如實**、**如如**、**本無**、**如**，[佛教術語](../Page/佛教.md "wikilink")，一般被解釋為[法](../Page/法_\(佛教\).md "wikilink")（）的[本性](../Page/本性_\(佛教\).md "wikilink")，即法的真實[本質](../Page/本質.md "wikilink")，也就是法的真實[自性](../Page/自性_\(佛教\).md "wikilink")。

## 字源

，在梵文與巴利文中，源自副詞，意為這樣地、如此地、如是地，在加上抽象名詞語尾tā之後，形成，一個哲學用語，用來表示「如是如是地呈現」、「如是呈現的狀況」，是讓事物（[法](../Page/法_\(佛教\).md "wikilink")，）如同它存在一般的存在、成為它成為的那個性質，也就是事物的真實本質。[釋迦牟尼住世時](../Page/釋迦牟尼.md "wikilink")，經常自稱「[如來](../Page/如來.md "wikilink")（）」，也是來自這個字根。漢譯佛經時，為了要正確地表示宇宙真實名相的真情實意，只能用「如」或「如如」來形容，意即「就是那樣子」的意思。

，可見於現存的《[金剛經](../Page/金剛經.md "wikilink")》梵文本中\[1\]，譯為「真實真如」或「實性真如」，義即《[解深密經](../Page/解深密經.md "wikilink")》七真如中的「實相真如」\[2\]，如來是無虛妄顛倒的諸法實性的異名。梵文（），源自於動詞bhū（）的過去分詞，意為已形成、已存在、已成為，可以被引申為[存在](../Page/存在.md "wikilink")、[本質](../Page/本質.md "wikilink")、[此在](../Page/此在.md "wikilink")、存有（）、真實、眾生、元素、萬物、神明等。漢譯為實、[真實](../Page/真實_\(佛教\).md "wikilink")。在表示實性的「如」前，加入表示無虛妄顛倒的修飾詞「真」，合為「真如」，表達了「這如是呈現的性質」，是比起事物表象存在，還要真實的存在\[3\]。

## 譯名

[佛教的理念認為](../Page/佛教.md "wikilink")，[眾生的根器不同](../Page/眾生.md "wikilink")，而現實中佛教[名相又深奧難通](../Page/名相.md "wikilink")，因而，在翻譯家思維、運用佛教語言、表達佛教名相、令眾生接受佛教理念的過程中，難免對概念名稱有所增減，難能恰到其位。

在[東晉之前的早期](../Page/東晉.md "wikilink")[漢傳佛教](../Page/漢傳佛教.md "wikilink")[佛經譯籍將](../Page/佛經.md "wikilink")「真如」譯為「本無」\[4\]，學者一般認為這是受[道家](../Page/道家.md "wikilink")[老子思想的用詞影響](../Page/老子.md "wikilink")，以[格義方式漢譯而成](../Page/格義.md "wikilink")。當時甚至曾出現「本無宗」。[鳩摩羅什譯師翻為](../Page/鳩摩羅什.md "wikilink")「如」\[5\]，[求那跋陀羅譯為](../Page/求那跋陀羅.md "wikilink")「如如」\[6\]，為直譯。[菩提流支](../Page/菩提流支.md "wikilink")\[7\]與[玄奘](../Page/玄奘.md "wikilink")\[8\]譯為「真如」，為直譯與意譯混合。在玄奘之後，多譯為真如\[9\]。

## 同義與近義詞

佛教中，真如被用來表示諸[法的真實相貌](../Page/法_\(佛教\).md "wikilink")，因為各宗派在教義上，對“真如”有不同角度的詮釋，因此“[法性](../Page/法性.md "wikilink")”（）\[10\]、“[實際](../Page/實際_\(佛教\).md "wikilink")”\[11\]、“[實相](../Page/實相.md "wikilink")”\[12\]、“[真實](../Page/真實_\(佛教\).md "wikilink")”、“[實性](../Page/自性_\(佛教\).md "wikilink")”（svabhāva）、“[法界](../Page/法界.md "wikilink")”、
“[法身](../Page/法身.md "wikilink")”、“[真性](../Page/真性.md "wikilink")”\[13\]都經常被當成真如的同義詞，來代換使用\[14\]。

依各宗派宗義，真如也被等同於一些特定術語，[中觀學派將真如等同於](../Page/中觀學派.md "wikilink")“[空性](../Page/空性.md "wikilink")”；[如來藏學派將真如等同於](../Page/如來藏學派.md "wikilink")“[如來藏](../Page/如來藏.md "wikilink")”、“[佛性](../Page/佛性.md "wikilink")”等。

[天台宗歸納](../Page/天台宗.md "wikilink")《[法華經](../Page/法華經.md "wikilink")》，提出[十如是](../Page/十如是.md "wikilink")\[15\]。

## 理論概述

在[分別說部宗義](../Page/分別說部.md "wikilink")、[龍樹](../Page/龍樹.md "wikilink")《[大智度論](../Page/大智度論.md "wikilink")》和[唯識學派](../Page/唯識學派.md "wikilink")[五位百法等中](../Page/五位百法.md "wikilink")，“如”\[16\]即“真如”屬[無為法](../Page/無為法.md "wikilink")\[17\]。

### 阿含經本源

[釋迦牟尼](../Page/釋迦牟尼.md "wikilink")[佛教導多聞聖弟子正知善見](../Page/佛.md "wikilink")[緣起](../Page/緣起.md "wikilink")[緣生法](../Page/緣生.md "wikilink")\[18\]，透過真實正觀，色法等[五蘊](../Page/五蘊.md "wikilink")\[19\]，為無常、苦、非我、非我所，而真實智生，最終解脫\[20\]。真實正觀諸法[真實的](../Page/真實.md "wikilink")[法性](../Page/法性.md "wikilink")，因難以用語言表示，又以“如”<ref>
《[雜阿含經](../Page/雜阿含經.md "wikilink")·二九九經》：「時，有異比丘來詣佛所，……白佛言：世尊！謂緣起法為世尊作，為餘人作耶？佛告比丘：緣起法者，非我所作，亦非餘人作，然彼如來出世，及未出世，法界常住，彼如來自覺此法，成等正覺，為諸眾生分別演說，開發顯示，所謂：此有故彼有，此起故彼起。謂：緣無明行，乃至純大苦聚集。無明滅故行滅，乃至純大苦聚滅。」
[龍樹](../Page/龍樹.md "wikilink")《[大智度論](../Page/大智度論.md "wikilink")》:「問曰：聲聞法中，何以不說是如、法性、實際，而摩訶衍法中處處說？答曰：聲聞法中亦有說處，但少耳。
\*如《雜阿含》中說，有一比丘問佛：「十二因緣法，為是佛作，為是餘人作？」佛告比丘：「我不作十二因緣，亦非餘人作；有佛無佛，諸法如、法相、法位常有，所謂是事有故是事有，是事生故是事生，……」如是生滅法，有佛、無佛**常爾**；是處說「如」。

  - 如《雜阿含·舍利弗師子吼經》中說：「……佛語比丘：『舍利弗語實不虛，所以者何？舍利弗善通達法性故。』」聲聞法中，觀諸法生滅相，是為「如」；滅一切諸觀，得諸法實相，是處說「法性」。

問曰：是處但說「如」、「法性」，何處復說「實際」？答曰：此二事，有因緣故說；實際無因緣，故不說實際。問曰：實際即是涅槃，為涅槃故，佛說十二部經，云何言無因緣？答曰：「涅槃」種種名字說，或名為「離」，或名為「妙」，或名為「出」；如是等，則為說「實際」，但不說名字故，言無因緣。」
[眾賢](../Page/眾賢.md "wikilink")《[阿毘達磨順正理論](../Page/阿毘達磨順正理論.md "wikilink")》：「有餘部師說：緣起是無為，以契經言：『佛告乞士，如是緣起，非我所作，非他所作，如來出世，若不出世，如是緣起，法性常住，乃至廣說。』」
</ref>、“如實”、“法如”、“法爾”等來形容它\[21\]。

### 部派佛教理論

[部派佛教時期](../Page/部派佛教.md "wikilink")，對於[實如觀察的結果](../Page/實如.md "wikilink")，起了種種學說。在[說一切有部所誦持的](../Page/說一切有部.md "wikilink")《[雜阿含經](../Page/雜阿含經.md "wikilink")》中，[因緣法是](../Page/因緣.md "wikilink")「此有故彼有」而[緣生法是](../Page/緣生.md "wikilink")「此法常住、法住法界、隨順緣起」\[22\]；而此經的《[相應部](../Page/相應部.md "wikilink")》和《[法蘊論](../Page/法蘊論.md "wikilink")》版本則為[緣起是](../Page/緣起.md "wikilink")「依此有彼有、法住法界」而[緣生法是](../Page/緣生.md "wikilink")「無常、有為、所造作」法\[23\]。《[大毘婆沙論](../Page/大毘婆沙論.md "wikilink")》將依據此段論述而認為“緣起是無為”者批判為[分別論者](../Page/分別論者.md "wikilink")\[24\]。

《[十八部論](../Page/十八部論.md "wikilink")》和《[部執異論](../Page/部執異論.md "wikilink")》記載[化地部的本宗同義有](../Page/化地部.md "wikilink")：「無為法有九種：一、思擇滅，二、非思擇滅，三、虛空，四、無我，五、善**如**，六、惡**如**，七、無記**如**。八、道**如**。九、緣生**如**。」

南傳《[論事](../Page/論事.md "wikilink")》註釋者[覺音記載](../Page/覺音.md "wikilink")，[北道派主張諸法的](../Page/北道派.md "wikilink")**如**性（tathatā），即[自性](../Page/自性_\(佛教\).md "wikilink")，屬無為法。學者據此認為在佛教部派中，[北道派是最早提出真如](../Page/北道派.md "wikilink")（bhūta-tathatā）學說的學派\[25\]。

### 大乘佛教理論

“真如”的最早理論詳述見於《[解深密經](../Page/解深密經.md "wikilink")》，有七真如：流轉真如，相真如，了別真如，安立真如，邪行真如，清淨真如，正行真如\[26\]。[玄奘所翻譯的](../Page/玄奘.md "wikilink")“了別真如”也有譯為“[心真如](../Page/心真如.md "wikilink")”。

在《[瑜伽師地論](../Page/瑜伽師地論.md "wikilink")》中“真如”是[轉依的所緣](../Page/轉依.md "wikilink")\[27\]，而[轉依成滿](../Page/轉依.md "wikilink")，是名[如來](../Page/如來.md "wikilink")[法身之相](../Page/法身.md "wikilink")\[28\]。

《[寶性論](../Page/寶性論.md "wikilink")》稱[如來藏為有垢真如](../Page/如來藏.md "wikilink")，佛的法身為無垢真如。

## 註釋與引用

## 外部链接

  - 廖明活：〈[初唐時期佛性論中的真如所緣緣種子問題](http://www.litphil.sinica.edu.tw/home/publish/PDF/Bulletin/27/27-167-183.pdf)〉。

[Category:佛教术语](../Category/佛教术语.md "wikilink")
[Category:大乘佛教](../Category/大乘佛教.md "wikilink")

1.
    [鳩摩羅什譯](../Page/鳩摩羅什.md "wikilink")《[金剛般若波羅蜜經](../Page/金剛般若波羅蜜經.md "wikilink")》：「何以故？如來者，即諸法如義。」
    [玄奘譯](../Page/玄奘.md "wikilink")《[大般若經](../Page/大般若經.md "wikilink")·第九會[能斷金剛分](../Page/金剛經.md "wikilink")》：「所以者何？善現！言如來者，即是真實真如增語。」
    [義淨譯](../Page/義淨.md "wikilink")《[佛說能斷金剛般若波羅蜜多經](../Page/佛說能斷金剛般若波羅蜜多經.md "wikilink")》：「何以故？妙生！言如來者，即是實性真如之異名也。」
    參見：《金剛經梵文注解》，善慧光、蓮花藏，中國人民大學國際佛學研究中心，瀋陽北塔翻譯組。

2.  [親光](../Page/親光.md "wikilink")《[佛地經論](../Page/佛地經論.md "wikilink")》：「經曰：『一切法真如，二障清淨相。』論曰：有義此顯，清淨法界，謂一切法，空無我性，所顯真如，永離二障，本性清淨，今復離染，能為一切善法所依，是故說名，清淨法界。一切法者，謂世、出世，有漏、無漏，蘊、界、處等。真如，即是諸法實性，無顛倒性。
    與一切法，不一不異，體唯一味，隨相分多。或說二種。……或說三種。……或說四種。……或說五種。……或說六種。……
    或說七種：一、流轉真如，謂一切行，無始世來，流轉實性。二、實相真如，謂一切法，二空無我，所顯實性。三、唯識真如，謂一切法，唯識實性。四、安立真如，謂有漏法，苦諦實性。五、邪行真如，謂業、煩惱，集諦實性。六、清淨真如，謂善、無為，滅諦實性。七、正行真如，謂諸有為、無漏善法，道諦實性。
    或說八種：謂不生、不滅，不斷、不常，不一、不異，不來、不去，[八遣相門](../Page/八不中道.md "wikilink")，所顯真如。或說九種：謂九品道，除九品障，所顯真如。或說十種：謂於[十地](../Page/十地.md "wikilink")，除十無明，所顯真如，即[十法界](../Page/十法界.md "wikilink")。如攝大乘，廣辯名相，如是增數，乃至窮盡。
    一切法門，皆是真如差別之相，而真如體，非一非多，分別言說，皆不能辯。由離一切虛妄顛倒，假名真如。能為一切善法所依，假名[法界](../Page/法界.md "wikilink")。離損減謗，假名實有。離增益謗，假名空無。分析推求，諸法虛假，極至於此，更不可度，唯此是真，假名[實際](../Page/實際_\(佛教\).md "wikilink")。是無分別，最勝聖智，所證境界，假名[勝義](../Page/勝義.md "wikilink")。如是廣說。
    言[二障者](../Page/二障.md "wikilink")，一、煩惱障，二、所知障。……清淨相者，謂此真如，[本性清淨](../Page/本性_\(佛教\).md "wikilink")，二障所覆，如淨虛空，烟雲等障，相似不淨。得出世間，證真如道，漸除二障所有種子，猶如大風吹烟雲等。[金剛喻定](../Page/金剛喻定.md "wikilink")，現在前時，滅離一切障種子盡，得淨法界，究竟[轉依](../Page/轉依.md "wikilink")，名清淨相。」

3.  《[成唯識論](../Page/成唯識論.md "wikilink")》卷9：「此諸法勝義亦即是真如。真，謂真實，顯非虛妄；如謂如常，表無變易。謂此真實，于一切位，常如其性，故曰真如。即是湛然不虛妄義。」

4.  支婁迦讖譯《道行般若經》卷5〈本無品〉：「舍利弗言：『是本無甚深，天中天！』佛言：『是本無甚深，甚深！』當說本無時，二百比丘僧皆得阿羅漢，五百比丘尼皆得須陀洹道，五百諸天人皆逮無所從生法樂，於中立六十新學菩薩皆得阿羅漢道。」
    曇摩蜱、竺佛念譯《摩訶般若鈔經》卷4〈本無品〉：「舍利弗白佛言：『本無者甚深，天中天。』佛言：『如是，本無實甚深。』說本無時，三百比丘皆得阿羅漢，五百比丘尼皆得須陀洹道，五百諸天及人悉逮得無所從生法樂忍，六十菩薩皆得阿羅漢道。」
    玄奘譯《大般若波羅蜜多經》卷548〈第四會第四分真如品〉：「
    爾時，舍利子白佛言：『世尊！如是真如甚深微妙。』爾時，佛告舍利子言：『如是！如是！如汝所說。如是真如甚深微妙。』當說如是真如相時，三百苾芻永盡諸漏心得解脫，成阿羅漢；復有五百苾芻尼眾遠塵離垢，於諸法中得淨法眼；五千天子宿業成熟，俱時證得無生法忍；六十菩薩不受諸漏心得解脫。」

5.  鳩摩羅什譯《金剛般若波羅蜜經》：「以實無有法得阿耨多羅三藐三菩提，是故燃燈佛與我授記，作是言：『汝於來世，當得作佛，號釋迦牟尼。』何以故？如來者，即諸法如義。」
    鳩摩羅什譯《維摩詰所說經》卷上：「云何彌勒受一生記乎？為從如生得受記耶？為從如滅得受記耶？若以如生得受記者，如無有生；若以如滅得受記者，如無有滅。一切眾生皆如也，一切法亦如也，眾聖賢亦如也，至於彌勒亦如也。若彌勒得受記者，一切眾生亦應受記。所以者何？夫如者不二不異，若彌勒得阿耨多羅三藐三菩提者，一切眾生皆亦應得。」

6.  求那跋陀羅《楞伽阿跋多羅寶經》卷4：「五法、自性、識、二無我分別趣相者，謂名、相、妄想、正智、如如。」

7.  菩提流支譯《金剛般若波羅蜜經》：「以實無有法，得阿耨多羅三藐三菩提，是故然燈佛與我受記，作如是言：『摩那婆！汝於來世，當得作佛，號釋迦牟尼。』何以故？須菩提！言如來者，即實真如。」
    菩提流支《入楞伽經》卷7：「佛告大慧：『我為汝說五法體相，二種無我差別行相。大慧！何等五法？一者、名，二者、相，三者、分別，四者、正智，五者、真如。』」

8.  玄奘譯《大般若波羅蜜多經》卷577：「善現。以如來無有少法能證阿耨多羅三藐三菩提。是故然燈如來應正等覺授我記言：『汝摩納婆於當來世，名釋迦牟尼如來應正等覺。』所以者何。善現。言如來者，即是真實真如增語。言如來者，即是無生法性增語。言如來者，即是永斷道路增語。言如來者，即是畢竟不生增語。」
    玄奘譯《說無垢稱經》卷2：「云何慈氏得授記耶？為依如生得授記耶？為依如滅得授記耶？若依如生得授記者，如無有生。若依如滅得授記者，如無有滅。無生無滅真如理中，無有授記。一切有情皆如也，一切法亦如也，一切聖賢亦如也，至於慈氏亦如也。若尊者慈氏得授記者，一切有情亦應如是而得授記。所以者何？夫真如者，非二所顯，亦非種種異性所顯。若尊者慈氏當證無上正等菩提，一切有情亦應如是當有所證。」

9.  義淨譯《佛說能斷金剛般若波羅蜜多經》卷1：「以無所得故，然燈佛與我授記：當得作佛號釋迦牟尼。何以故？妙生，言如來者即是實性真如之異名也。」

10. [龍樹](../Page/龍樹.md "wikilink")《[大智度論](../Page/大智度論.md "wikilink")》：「法性者，諸法實相，除心中無明諸結使，以清淨實觀，得諸法[本性](../Page/本性_\(佛教\).md "wikilink")，名為法性，性名真實。」

11. [龍樹](../Page/龍樹.md "wikilink")《[大智度論](../Page/大智度論.md "wikilink")》：「「實際」名入法性中，知法性無量無邊，最為微妙，更無有法勝於法性、出法性者，心則滿足，更不餘求，則便作證。譬如行道，日日發引而不止息，到所至處，無復去心；行者住於實際，亦復如是。」

12. [龍樹](../Page/龍樹.md "wikilink")《[大智度論](../Page/大智度論.md "wikilink")》：「問曰：如、法性、實際，是三事，為一？為異？若一，云何說三？若三，今應當分別說。答曰：是三皆是諸法「實相」異名。所以者何？凡夫無智，於一切法，作邪觀，所謂：常、樂、淨、實、我等。佛弟子，如法本相觀，是時不見常，是名無常；不見樂，是名苦；不見淨，是名不淨；不見實，是名空；不見我，是名無我。若不見常而見無常者，是則妄見，見苦、空、無我、不淨，亦如是，是名為如。如者，如本，無能敗壞。以是故，佛說三法為[法印](../Page/三法印.md "wikilink")，所謂：一切有為法無常印，一切法無我印，涅槃寂滅印。……有人著常顛倒，故捨常見，不著無常相，是名法印；非謂「捨常、著無常者，以為法印」；我乃至寂滅，亦如是。般若波羅蜜中，破著無常等見；非謂「破不受不著」。」

13. [龍樹](../Page/龍樹.md "wikilink")《[大智度論](../Page/大智度論.md "wikilink")》：「實性與無明合故，變異則不清淨；若除却無明等，得其**真性**，是名「法性」清淨。」
    [清辯](../Page/清辯.md "wikilink")《[大乘掌珍論](../Page/大乘掌珍論.md "wikilink")》：「為欲令彼易證真空，速入法性故，略製此掌珍論：真性有為空，如幻緣生故。無為無有實，不起似空華。」「以真性簡別立宗，真義自體，說名**真性**，即[勝義諦](../Page/勝義諦.md "wikilink")，就勝義諦，立有為空，非就世俗，眾緣合成。有所造作故，名有為。……眾緣所起，男、女、羊、鹿，諸幻事等，自性實無，顯現似有，所立能立法，皆通有為，同法喻故，說如幻。……所立有法，皆從緣生，為立此因，說緣生故。」「非有為故，說名無為，翻對有為，是無為義，即是虛空，擇、非擇滅，及真如性。……即此世間所知虛空，就真性故，空無有實，是名立宗。即此所立，就真性故，無實虛空，二宗皆許為不起故，或假立為不起法故，說名為因。空花無實，亦不起故，立為同喻。」

14. 玄奘譯《成唯識論》卷9：「亦言顯此復有多名，謂名法界及實際等，如餘論中隨義廣釋。此性即是唯識實性。」

15. 《妙法蓮華經》卷1：「佛所成就第一希有難解之法，唯佛與佛乃能究盡諸法實相，所謂諸法如是相、如是性、如是體、如是力、如是作、如是因、如是緣、如是果、如是報、如是本末究竟等。」

16. [龍樹](../Page/龍樹.md "wikilink")《[大智度論](../Page/大智度論.md "wikilink")》:「諸法「如」，有二種：一者、各各相，二者、實相。各各相者，如：地，堅相；水，濕相；火，熱相；風，動相，如是等分別諸法，各自有相。實相者，於各各相中分別，求實不可得，不可破，無諸過失。如自相空中說：……如是推求地相，則不可得；若不可得，其實皆空，空則是地之實相。一切別相，皆亦如是，是名為「如」。
    「法性」者，如前說各各法空，空有差品，是為「如」；同為一空，是為「法性」。是法性亦有二種：一者、用無著心，分別諸法，各自有性故；二者、名無量法，所謂諸法實相。……
    「實際」者，以法性為實，證故為際。如阿羅漢，名為住於實際。」
    「諸佛賢聖，種種方便說法，破無明等諸煩惱，令眾生還得實性，如本不異，是名為「如」。」

17. [龍樹](../Page/龍樹.md "wikilink")《[大智度論](../Page/大智度論.md "wikilink")》:「何等為無為法？不生、不住、不滅，若染盡、瞋盡、癡盡，如，不異，法相、法性、法住，實際，是名無為法。」

18. 《雜阿含經》卷12〈296經〉：「多聞聖弟子，於此[因緣法](../Page/因緣.md "wikilink")、[緣生法](../Page/緣生.md "wikilink")，正知善見。不求前際，言：『我過去世若有、若無，我過去世何等類，我過去世何如。』不求後際：『我於當來世為有、為無，云何類，何如。』內不猶豫：『此是何等，云何有此為前，誰終當云何之，此眾生從何來，於此沒當何之。』若沙門、婆羅門，起凡俗見所繫，謂說[我見所繫](../Page/我見.md "wikilink")，說[眾生見所繫](../Page/眾生見.md "wikilink")，說[壽命見所繫](../Page/壽命見.md "wikilink")，忌諱吉慶見所繫。爾時悉斷、悉知，斷其根本，如截多羅樹頭，於未來世，成不生法，是名多聞聖弟子，於[因緣法](../Page/因緣.md "wikilink")、[緣生法](../Page/緣生.md "wikilink")，如實正知，善見、善覺、善修、善入。」

19. 《[雜阿含經](../Page/雜阿含經.md "wikilink")》第2卷第58經：「世尊！何因何緣？名為色陰，何因何緣？名受、想、行、識陰。佛告比丘：[四大因](../Page/四大.md "wikilink")，四大緣，是名色陰，所以者何？諸所有色陰，彼一切悉皆四大緣，四大造故；[觸](../Page/觸_\(佛教\).md "wikilink")**因**、觸**緣**，**生**受、想、行，是故名受、想、行陰，所以者何？若所有受、想、行，彼一切觸緣故；[名色因](../Page/名色.md "wikilink")、名色緣，是故名為識陰，所以者何？若所有識，彼一切名色緣故。」

20. 《雜阿含經》卷1〈9經〉：「佛住舍衛國祇樹給孤獨園。爾時，世尊告諸比丘：「色無常，無常即苦，苦即非我，非我者亦非我所。如是觀者，名真實正觀。如是受、想、行、識無常，無常即苦，苦即非我，非我者亦非我所。如是觀者，名真實觀。聖弟子！如是觀者，厭於色，厭受、想、行、識，厭故不樂，不樂故得解脫。解脫者真實智生：『我生已盡，梵行已立，所作已作，自知不受後有。』」時，諸比丘聞佛所說，歡喜奉行。」

21. 《雜阿含經》卷12〈296經〉：「云何為**[因緣](../Page/因緣.md "wikilink")**法？謂此有故彼有，謂緣無明行，緣行識，乃至如是如是，**純大苦聚集**。
    云何**[緣生](../Page/緣生.md "wikilink")**法？謂無明、行，若佛出世，若未出世，**此法常住**，**法住法界**，彼如來自所覺知，成等正覺，為人演說，開示顯發；謂緣無明有行，乃至緣生有老死，若佛出世，若未出世，此法常住，法住法界，彼如來自覺知，成等正覺，為人演說，開示顯發；謂緣生故，有老、病、死、憂、悲、惱、苦，此等諸法，法住、法空、法如、法爾，法不離如，法不異如，審諦真實、不顛倒，如是隨順**[緣起](../Page/緣起.md "wikilink")**，是名緣生法。謂無明、行、識、名色、六入處、觸、受、愛、取、有、生、老、病、死、憂、悲、惱、苦，是名緣生法。」

22.
23. 《法蘊足論》卷11〈緣起品〉：「苾芻當知，生緣老死。若佛出世，若不出世，如是[緣起](../Page/緣起.md "wikilink")，**法住法界**。一切如來，自然通達，等覺宣說，施設建立，分別開示，令其顯了：謂生緣老死，如是乃至，無明緣行，應知亦爾。此中所有法性、法定、法理、法趣，是真是實，是諦是如，非妄非虛，非倒非異，是名緣起。
    云何名為[緣已生法](../Page/緣已生.md "wikilink")。謂無明、行、識、名色、六處、觸、受、愛、取、有、生、老死，如是名為緣已生法。苾芻當知，老死是**無常**，是**有為**，是所造作，是緣已生，盡法沒法，離法滅法。生、有、取、愛、受、觸、六處、名色、識、行、無明亦爾。」

24. 《[大毘婆沙論](../Page/大毘婆沙論.md "wikilink")》：「或復有執，**緣起是無為**，如**[分別論者](../Page/分別論者.md "wikilink")**。問：彼因何故作如是執？答：彼因經故，謂契經說：『如來出世若不出世，法住法性，佛自等覺，為他開示，乃至廣說。』故知緣起是無為法，為止彼宗顯示緣起是有為法，墮三世故，無無為法墮在三世。」

25. [演培譯](../Page/演培.md "wikilink")，[木村泰賢著](../Page/木村泰賢.md "wikilink")《小乘佛教思想論》第一章第四節〈形而上學的實在論的傾向〉：「然從佛教的立場說，世間偶然的東西，一樣也沒有，一切必是由於一定的規律而被規定，所以如極尅實的說，一切法的根底，終於不得不說有他的無為法。在這意義下，案達羅派及北道派，主張一切法在性質上，都是決定的，尤其北道派，到達所謂真如這觀念，主張諸法的真如是無為。於此所謂無為，據註者說，是sabhavata，指相當於種或類的觀念。如松是松，人是人，是就名為松的真如，人的真如。在小乘教諸派中，把真如用於這意義的，北道派是唯一的學派。」

26.
27. 《[瑜伽師地論](../Page/瑜伽師地論.md "wikilink")·攝決擇分》：「[阿賴耶識是一切雜染根本](../Page/阿賴耶識.md "wikilink")。……由緣**真如**境智，修習多修習故，而得[轉依](../Page/轉依.md "wikilink")，[轉依無間](../Page/轉依.md "wikilink")，當言已**斷**[阿賴耶識](../Page/阿賴耶識.md "wikilink")，由此斷故，當言已斷一切雜染，當知[轉依由相違故](../Page/轉依.md "wikilink")，能永對治[阿賴耶識](../Page/阿賴耶識.md "wikilink")。又[阿賴耶識體是無常](../Page/阿賴耶識.md "wikilink")，有取受性；[轉依是](../Page/轉依.md "wikilink")**常**，無取受性，緣**真如**境聖道方能轉依故。又[阿賴耶識](../Page/阿賴耶識.md "wikilink")，恒為一切麁重所隨；[轉依究竟遠離一切所有麁重](../Page/轉依.md "wikilink")。又[阿賴耶識](../Page/阿賴耶識.md "wikilink")，是煩惱轉因，聖道不轉因；[轉依是煩惱不轉因](../Page/轉依.md "wikilink")，聖道轉因，應知但是建立因性，非生因性。又[阿賴耶識](../Page/阿賴耶識.md "wikilink")，令於善淨無記法中不得自在；[轉依令於一切善淨無記法中得大自在](../Page/轉依.md "wikilink")。……如是建立雜染根本故，趣入通達修習作意故，建立[轉依故](../Page/轉依.md "wikilink")，當知建立[阿賴耶識雜染還滅相](../Page/阿賴耶識.md "wikilink")。」

28. 《[解深密經](../Page/解深密經.md "wikilink")》：「佛告曼殊室利菩薩曰：善男子\!
    若於諸地波羅蜜多，善修出離，轉依成滿，是名如來法身之相。」