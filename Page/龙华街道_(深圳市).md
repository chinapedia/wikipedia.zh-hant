[Long_hua_1.jpg](https://zh.wikipedia.org/wiki/File:Long_hua_1.jpg "fig:Long_hua_1.jpg")
**龙华街道**是[中国](../Page/中国.md "wikilink")[广东省](../Page/广东省.md "wikilink")[深圳市](../Page/深圳市.md "wikilink")[龙华区下辖的一个](../Page/龍華區_\(深圳市\).md "wikilink")[街道](../Page/街道_\(行政区划\).md "wikilink")，位處深圳的中北部。

龍華街道前身为**龙华镇**的一部分，2004年城市化中撤龍華镇政區建制，後再分拆出龙华、民治、大浪三个街道。

## 地理

龍華鎮在清末屬寶安縣一部分。1949年10月新中國成立，設立龍華鄉。後經多次改制，於1986年10月變成为「龙华镇」，管轄範圍包括了今日的[大浪街道和](../Page/大浪街道.md "wikilink")[民治街道](../Page/民治街道.md "wikilink")。1993年1月龍華鎮被編入[寶安區一部分](../Page/寶安區.md "wikilink")；2004年龍華鎮改稱「龍華街道」；2006年4月29日龍華街道再拆出民治、龍華、大浪三個街道。

龍華街道目前位于龙华区中部，东与[龙岗区](../Page/龙岗区.md "wikilink")[坂田接壤](../Page/坂田街道.md "wikilink")，西接[大浪](../Page/大浪街道.md "wikilink")，南邻[民治](../Page/民治街道.md "wikilink")，北与[观湖](../Page/觀湖街道.md "wikilink")、[福城相连](../Page/福城街道_\(深圳市\).md "wikilink")。总面积19.18平方公里，下辖景龙、龙园、三联、华联、清湖、
油松、松和等7个社区工作站，设景华、荔园、郭吓、老围、河背、牛地埔、墩背、玉翠、弓村、狮头岭、山咀头、清湖、上油松、下油松、水斗新围、水斗老围、
共和、伍屋、瓦窑排、富康等20个社区居委会\[1\]。

## 經濟

龙华經濟主要以制造业為主，生产领域涉及[电脑](../Page/电脑.md "wikilink")、[自行车](../Page/自行车.md "wikilink")、[塑胶](../Page/塑胶.md "wikilink")、[玩具](../Page/玩具.md "wikilink")、[电子](../Page/电子.md "wikilink")、机械制造、[卷烟等行业](../Page/卷烟.md "wikilink")。[富士康是辖区内最大的企业](../Page/富士康.md "wikilink")，员工20多万。

## 交通

### 道路

  - [梅观高速公路](../Page/梅观高速公路.md "wikilink")：連接[深圳經濟特區](../Page/深圳經濟特區.md "wikilink")
  - [布龙公路](../Page/布龙公路.md "wikilink")
  - [石观公路](../Page/石观公路.md "wikilink")

### 地铁

[深圳地铁4號綫](../Page/深圳地铁4號綫.md "wikilink")，使龙华与[深圳关内实现全面对接](../Page/深圳.md "wikilink")。

## 參考文獻

## 外部連結

  - [深圳龙华行政区划建制沿革
    区划网](https://web.archive.org/web/20100606233640/http://www.quhua.net/guangdong/shenzhen/1762.html)

[Category:深圳市龍華區行政區劃](../Category/深圳市龍華區行政區劃.md "wikilink")
[龍華区](../Category/深圳街道_\(行政区划\).md "wikilink")

1.