[LuXunPark_sculpture.jpg](https://zh.wikipedia.org/wiki/File:LuXunPark_sculpture.jpg "fig:LuXunPark_sculpture.jpg")
[Lu_Xun_Park.JPG](https://zh.wikipedia.org/wiki/File:Lu_Xun_Park.JPG "fig:Lu_Xun_Park.JPG")

**鲁迅公园**是上海主要历史文化纪念性公园和中国第一个体育公园。位于[上海市](../Page/上海市.md "wikilink")[虹口区东江湾路](../Page/虹口区.md "wikilink")146号，占地22.37公顷。

## 历史

[清朝](../Page/清朝.md "wikilink")[光绪二十二年](../Page/光绪.md "wikilink")（1896年），[上海公共租界工部局在界外的](../Page/上海公共租界工部局.md "wikilink")[北四川路底购得农田](../Page/北四川路.md "wikilink")237.288亩，在此圈地筹建[万国商团打靶场](../Page/万国商团.md "wikilink")，由英国园艺设计师，根据[英国](../Page/英国.md "wikilink")[格拉斯哥体育公园模式](../Page/格拉斯哥.md "wikilink")，建成“虹口娱乐场”。1905年改建为虹口体育游戏场和打靶场。1922年改名为“**虹口公园**”。

虹口公园开了上海乃至中国现代体育运动的风气之先。作为大型综合性体育公园，园内共有1个九孔高尔夫球场，75片草地网球场，8片硬地网球场，3片足球场，5片草地滚球场，还有曲棍球篮球、棒球、田径等场地。根据工部局统计，民国24年（1935年），租界外侨总共才3.8万人，而仅在虹口公园一处直接参加体育活动的就达86103人次，在虹口公园打[高尔夫球还要排队](../Page/高尔夫球.md "wikilink")。

1932年4月29日，在这里发生了[虹口公园爆炸案](../Page/虹口公园爆炸案.md "wikilink")。日军在虹口公园举行庆祝[天皇诞生日的](../Page/天皇.md "wikilink")[天长节大会](../Page/天长节.md "wikilink")。会间，[韩国临时政府派独立党党员](../Page/韩国临时政府.md "wikilink")[尹奉吉在主席台旁引爆炸弹](../Page/尹奉吉.md "wikilink")，当场炸死日本上海派遣军司令[白川义则](../Page/白川义则.md "wikilink")、居留团团长[河端贞次等](../Page/河端贞次.md "wikilink")，日本公使[重光葵](../Page/重光葵.md "wikilink")、总领事等均被炸成重伤，极大地震动了日本侵略军。至今园中还有尹奉吉义士的纪念亭。

民国26年（1937年）[八一三事变](../Page/八一三事变.md "wikilink")，公园部分建筑遭破坏，工部局面对日本势力的扩张，步步退缩，公园及靶场的建筑物被日军蚕食，到民国31年（1942年）9月，万国商团解散，靶场及公园全部被日军占领作为军用场地。1945年后改名为“**中正公园**”。1950年改回“**虹口公园**”。

1927年，[鲁迅从](../Page/鲁迅.md "wikilink")[广州搬来上海](../Page/广州.md "wikilink")，居住在虹口公园附近的[大陆新村](../Page/大陆新村.md "wikilink")，直至去世。鲁迅生前一直来公园散步。1956年，鲁迅的灵枢由[万国公墓迁此](../Page/万国公墓.md "wikilink")，并建鲁迅纪念亭、[鲁迅纪念馆等](../Page/鲁迅纪念馆.md "wikilink")。1988年改名为“**鲁迅公园**”。

## 建筑

公园经百年的历史积累和不断改造建设，不仅保留了英国公园的公布形式，保留了南大门、饮水器等历史景观和紫薇等百年大树，而且揉和了中国造园艺术。

梅轩尹奉吉纪念馆于2003年开馆，位于该公园内，纪念1932年在该公园行刺的[尹奉吉](../Page/尹奉吉.md "wikilink")。2013年9月，中国政府对鲁迅公园进行整修，韩国国家报勋处从2014年末开始全面更换纪念馆内展品，并在纪念馆广场陈列展品。2015年4月29日，鲁迅公园内的“梅轩尹奉吉纪念馆”重新开放。当天，尹奉吉义士义举83周年纪念仪式也在[韩国](../Page/韩国.md "wikilink")[首尔梅轩纪念馆举行](../Page/首尔.md "wikilink")。\[1\]

## 交通

  - [上海轨道交通](../Page/上海轨道交通.md "wikilink")
    [3号线](../Page/上海轨道交通3号线.md "wikilink")/[8号线](../Page/上海轨道交通8号线.md "wikilink")
    [虹口足球场站](../Page/虹口足球场站.md "wikilink")

## 参考文献

## 参见

  - [鲁迅墓](../Page/鲁迅墓.md "wikilink")
  - [鲁迅纪念馆](../Page/鲁迅纪念馆.md "wikilink")
  - [上海鲁迅故居](../Page/上海鲁迅故居.md "wikilink")
  - [虹口足球场](../Page/虹口足球场.md "wikilink")

{{-}}   [category:上海历史](../Page/category:上海历史.md "wikilink")
[category:上海租界](../Page/category:上海租界.md "wikilink")
[category:上海体育场馆](../Page/category:上海体育场馆.md "wikilink")

[Category:鲁迅公园](../Category/鲁迅公园.md "wikilink")
[Category:上海各博物馆](../Category/上海各博物馆.md "wikilink")
[Category:虹口区公园](../Category/虹口区公园.md "wikilink")

1.  [上海鲁迅公园内尹奉吉纪念馆修缮完毕重新开放](http://chinese.yonhapnews.co.kr/allheadlines/2015/04/29/0200000000ACK20150429001200881.HTML)，韩联网，2015-04-29