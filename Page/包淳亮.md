**包淳亮**（），[高雄市人](../Page/高雄市.md "wikilink")。[國立中興大學法商學院](../Page/國立中興大學法商學院.md "wikilink")(今[國立臺北大學](../Page/國立臺北大學.md "wikilink"))法律系學士，[國立中山大學大陸研究所碩士](../Page/國立中山大學大陸研究所.md "wikilink")，[國立政治大學東亞研究所博士](../Page/國立政治大學東亞研究所.md "wikilink")。

## 生平

包淳亮是[台灣青年教師](../Page/台灣.md "wikilink")，所撰寫之論述主要包括中國大陸與[蘇聯最高領導層接班制度化的問題](../Page/蘇聯.md "wikilink")，特別是有關[江澤民權力交卸的討論](../Page/江澤民.md "wikilink")，相關著作並涉及[世界體系](../Page/世界體系.md "wikilink")[霸權興衰](../Page/霸權.md "wikilink")，[台灣海峽兩岸關係](../Page/台灣海峽兩岸關係.md "wikilink")、[中華人民共和國－印度關係關係](../Page/中華人民共和國－印度關係.md "wikilink")、[中國人民解放軍發展等](../Page/中國人民解放軍.md "wikilink")。時論散見於[聯合早報](../Page/聯合早報.md "wikilink")、[中國時報](../Page/中國時報.md "wikilink")、[台灣蘋果日報](../Page/台灣蘋果日報.md "wikilink")、[自由時報](../Page/自由時報.md "wikilink")、[聯合報](../Page/聯合報.md "wikilink")、[中國評論](../Page/中國評論.md "wikilink")；學術論文發表於台灣[中國事務](../Page/中國事務.md "wikilink")、[東亞季刊](../Page/東亞季刊.md "wikilink")、[展望與探索](../Page/展望與探索.md "wikilink")、[中共研究](../Page/中共研究.md "wikilink")、[歷史月刊](../Page/歷史月刊.md "wikilink")，以及[香港社會科學學報等刊物中](../Page/香港社會科學學報.md "wikilink")。包氏並撰有諸多詩作。

## 經歷

包淳亮曾任[輔仁大學中國社會文化研究中心研究助理](../Page/輔仁大學.md "wikilink")(1997-9)，[民主進步黨中國事務部研究員](../Page/民主進步黨.md "wikilink")（1996-7,
1999-2003，襄助中國事務部歷任主任包括[陳忠信](../Page/陳忠信.md "wikilink")、[顏萬進](../Page/顏萬進.md "wikilink")、[顏建發](../Page/顏建發.md "wikilink")，襄助中國事務部歷任副主任包括[梁文傑](../Page/梁文傑.md "wikilink")、[張國城](../Page/張國城.md "wikilink")、[余莓莓等](../Page/余莓莓.md "wikilink")），並曾擔任民進黨中國事務部期刊《中國事務》執行編輯(2000-2003)，邀得大量大陸學者撰稿，期間曾被《[新華澳報](../Page/新華澳報.md "wikilink")》評論員文章稱為「民進黨的統派」。2003年春，從民進黨中國事務部離職。2004年取得博士學位後任[中國科技大學通識教育中心專任助理教授](../Page/中國科技大學_\(台灣\).md "wikilink")，2010-2011年間為[加州大學河濱分校全球研究學程](../Page/加州大學河濱分校.md "wikilink")[訪問學者](../Page/訪問學者.md "wikilink")。2013年出版《自由的兩岸關係》一書，主張台灣民意代表加入中國大陸[全國人大](../Page/全國人大.md "wikilink")，以一個[議會主權解決兩岸爭議](../Page/議會主權.md "wikilink")。2015年出版《一黨制下的雙首長制》一書，對俄國、越南與中國的政治體制進行了分析比較。2016年出版時論集《中國可以偉大的50個理由》。

## 論點

2005年11月8日，包淳亮說，[中國崩潰論的支持者們也許認為](../Page/中國崩潰論.md "wikilink")，[中國共產黨政權欺侮台灣](../Page/中國共產黨.md "wikilink")、鎮壓[法輪功](../Page/法輪功.md "wikilink")、欺貧媚富、貪污腐敗、專制落後，因此他們對中共政權的批評永遠立足於道德高地，「這個高地甚至已突出於雲端，乃至於無須審視現實」；出於對中共政權的厭惡，而不惜期望數億中國人隨著中國崩潰而流離失所、[饑饉而亡](../Page/饑饉.md "wikilink")，這是多麼痛苦、多麼可怕的意識形態，令人質疑在如此的情緒下是否真能「結出道德的果實」。包淳亮還說，他曾經是把《當代雜誌》從頭看到尾的大學生，也曾經購買《當代雜誌》近百期；但前幾年，《當代雜誌》總編輯[金恆煒成為](../Page/金恆煒.md "wikilink")[泛綠代言人](../Page/泛綠.md "wikilink")，已經損害《當代雜誌》的中立性與批判性\[1\]。

## 注釋

## 外部链接

  - [包淳亮的PChome個人新聞台](http://mypaper.pchome.com.tw/sol)
  - [包老師的教學網站](http://spaces.msn.com/members/solpao)
  - [自由的兩岸關係](http://politics.ntu.edu.tw/RAEC/comm2/Bian06.pdf)

[Category:台灣政治學家](../Category/台灣政治學家.md "wikilink")
[B](../Category/輔仁大學教授.md "wikilink")
[B](../Category/國立政治大學校友.md "wikilink")
[B](../Category/國立中山大學校友.md "wikilink")
[B](../Category/國立中興大學法商學院校友.md "wikilink")
[Category:民主進步黨黨員](../Category/民主進步黨黨員.md "wikilink")
[Category:高雄市人](../Category/高雄市人.md "wikilink")
[B包](../Category/包姓.md "wikilink")

1.