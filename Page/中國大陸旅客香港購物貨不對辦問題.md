**中國大陸旅客香港購物貨不對辦問題**是指一些[中國大陸的國內旅客參加](../Page/中國大陸.md "wikilink")[香港](../Page/香港.md "wikilink")[零團費旅行團後](../Page/零團費.md "wikilink")，報稱被強迫到一些以誤導手法售賣冒牌貨品的商戶消費到一定金額。

## 歷史

[旅遊業是](../Page/旅遊業.md "wikilink")[香港經濟的四大支柱之一](../Page/香港經濟.md "wikilink")。在鄰近[發展中地區](../Page/中國大陸.md "wikilink")，屢見不鮮出現[黑心產品之時](../Page/黑心食品.md "wikilink")，
「[香港製造](../Page/香港品牌產品.md "wikilink")」是一個[品牌](../Page/品牌.md "wikilink")，是香港經濟轉營的唯一出路。可惜，害群之馬在過去時有被發現，[香港政府及業界都相信此類只是](../Page/香港政府.md "wikilink")[個別事件](../Page/個別事件.md "wikilink")，只要給予[黑店](../Page/黑店.md "wikilink")「口頭警告」，以及教育[遊客](../Page/遊客.md "wikilink")「小心」便可。由於[香港旅遊業是香港經濟命脈](../Page/香港旅遊業.md "wikilink")，[香港傳媒也不便描黑之](../Page/香港傳媒.md "wikilink")。直至[轉捩點](../Page/轉捩點.md "wikilink")，[中央電視台](../Page/中央電視台.md "wikilink")《[經濟半小時](../Page/經濟半小時.md "wikilink")》跟隨內地到香港[旅行團](../Page/旅行團.md "wikilink")[放蛇](../Page/放蛇.md "wikilink")，之後在全國性廣播中，報導[香港旅遊業的黑材料](../Page/香港旅遊業.md "wikilink")。其後有關的某香港商店充滿自信，高調回應，相信此仍一場[誤會而已](../Page/誤會.md "wikilink")，期望只運用[公關](../Page/公關.md "wikilink")[機器](../Page/機器.md "wikilink")，化解是次風波。

## 部分事件

  - [皇室鐘錶珠寶](../Page/皇室鐘錶珠寶.md "wikilink")：售賣冒牌[手表](../Page/手表.md "wikilink")\[1\]
  - [香港（免稅品）數碼攝影器材中心售賣聲稱是日本品牌](../Page/香港（免稅品）數碼攝影器材中心.md "wikilink")[富士](../Page/富士.md "wikilink")，其實是「Fujimart」（[富仕美](../Page/富仕美.md "wikilink")）的[攝錄機](../Page/攝錄機.md "wikilink")。\[2\]
  - [紅磡鑽工業國際珠寶店](../Page/紅磡.md "wikilink")：[中國大陸](../Page/中國大陸.md "wikilink")[鍍金首飾附](../Page/鍍金.md "wikilink")[ITALY字樣](../Page/意大利.md "wikilink")。\[3\]

## 影響

[優質旅遊服務協會主席](../Page/優質旅遊服務協會.md "wikilink")[李子良批評](../Page/李子良.md "wikilink")，事件嚴重影響香港形象，希望港府正視問題。

## 相關

### 組織

  - [中國中央電視臺](../Page/中國中央電視臺.md "wikilink")《[經濟半小時](../Page/經濟半小時.md "wikilink")》
  - [香港旅遊業議會](../Page/香港旅遊業議會.md "wikilink")
  - [香港海關](../Page/香港海關.md "wikilink")
  - [香港消費者委員會](../Page/香港消費者委員會.md "wikilink")：總幹事呼籲懷疑有關事件的內地旅客要主動向委員會投訴，不過在過去一個月委員會只收到十件相關個案。

### 其他

  - [香港法例](../Page/香港法例.md "wikilink")《[商品說明條例](../Page/商品說明條例.md "wikilink")》第362章
  - [退貨保証](../Page/退貨保証.md "wikilink")
  - [冒牌貨](../Page/冒牌貨.md "wikilink")
  - [零團費](../Page/零團費.md "wikilink")
  - [定點購物](../Page/定點購物.md "wikilink")
  - [銷後服務](../Page/銷後服務.md "wikilink")
  - [合約精神](../Page/合約.md "wikilink")

## 參考資料

<div class="references-small">

<references />

</div>

## 外部参考

  - [2007年4月3日：海關調查商舖有否違反《商品說明條例》](http://www.info.gov.hk/gia/general/200704/03/P200704030135.htm)

[category:香港旅遊事件](../Page/category:香港旅遊事件.md "wikilink")
[category:香港罪案](../Page/category:香港罪案.md "wikilink")

[Category:2007年香港](../Category/2007年香港.md "wikilink")

1.  [內地旅客疑受騙未能退貨](http://hk.news.yahoo.com/070406/12/252e9.html)
2.  [又揭貨不對辦 千里退攝錄機](http://hk.news.yahoo.com/070407/60/25384.html)
3.  [鑽工業40元首飾竟售1000元](http://hk.news.yahoo.com/070419/60/25wbo.html)