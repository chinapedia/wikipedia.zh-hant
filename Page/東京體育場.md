是位於[日本](../Page/日本.md "wikilink")[東京都](../Page/東京都.md "wikilink")[調布市的多功能](../Page/調布市.md "wikilink")[體育場](../Page/體育場.md "wikilink")，由[東京都廳聯合](../Page/東京都廳.md "wikilink")[京王電鐵](../Page/京王電鐵.md "wikilink")、[瑞穗銀行共同出資的](../Page/瑞穗銀行.md "wikilink")[第三部門單位](../Page/第三部門.md "wikilink")「株式會社東京體育場」經營。該體育場自2003年3月1日起由[味之素取得](../Page/味之素.md "wikilink")（已取得4期，現行為2019年3月1日起的5年間），故又稱為，是日本第一座導入冠名制度的體育場。

## 概要

球場是[日本職業足球聯賽球隊](../Page/日本職業足球聯賽.md "wikilink")[FC東京和](../Page/FC東京.md "wikilink")[東京綠茵的主場](../Page/東京綠茵.md "wikilink")，另外亦有一些本土的[足球](../Page/足球.md "wikilink")、[欖球和](../Page/欖球.md "wikilink")[美式足球的比賽會上演](../Page/美式足球.md "wikilink")。雖然球場並沒有舉行任何一場[2002年世界盃的賽事](../Page/2002年世界盃.md "wikilink")，但亦成為了[沙地阿拉伯國家足球隊的習訓地點](../Page/沙地阿拉伯國家足球隊.md "wikilink")。

球場亦可以舉行[演唱會及](../Page/演唱會.md "wikilink")[跳蚤市場等活動](../Page/跳蚤市場.md "wikilink")。

## 图片

<File:Ajinomoto> Stadium 20101120.JPG|2010年11月撮影 <File:Main> gate of
Ajinomoto Stadium, Chofu, Tokyo.jpg|2004年11月撮影

## 参考资料

## 外部連結

  - [味之素體育場官方網站](http://www.ajinomotostadium.com/)

  - [WorldStadiums.com
    entry](http://www.worldstadiums.com/stadium_pictures/asia/japan/kanto/tokyo_ajinamoto.shtml)

[Category:FC東京](../Category/FC東京.md "wikilink")
[Category:日本足球場](../Category/日本足球場.md "wikilink")
[Category:橄欖球場](../Category/橄欖球場.md "wikilink")
[Category:美式足球場](../Category/美式足球場.md "wikilink")
[Category:東京體育場地](../Category/東京體育場地.md "wikilink")
[Category:2001年完工體育場館](../Category/2001年完工體育場館.md "wikilink")
[Category:2020年夏季奧林匹克運動會運動場](../Category/2020年夏季奧林匹克運動會運動場.md "wikilink")
[Category:奧運足球場館](../Category/奧運足球場館.md "wikilink")
[Category:奧運現代五項場館](../Category/奧運現代五項場館.md "wikilink")
[Category:調布市](../Category/調布市.md "wikilink")