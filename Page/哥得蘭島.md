**哥得蘭島**（[瑞典語](../Page/瑞典語.md "wikilink")：****，又译为“**哥特兰岛**”）位于[瑞典](../Page/瑞典.md "wikilink")，也是[瑞典及](../Page/瑞典.md "wikilink")[波羅的海最大的](../Page/波羅的海.md "wikilink")[島嶼](../Page/島嶼.md "wikilink")，面積為3,145.45平方公里，佔[瑞典国土的不足](../Page/瑞典.md "wikilink")1%。據2004年資料顯示，島上有居民5.76万，其中約2.26万居住在主要城市[维斯比](../Page/维斯比.md "wikilink")。岛上的主要收入来源于[旅游和](../Page/旅游.md "wikilink")[农业](../Page/农业.md "wikilink")。

哥得蘭島建有自己的[舊省](../Page/瑞典舊省.md "wikilink")、[省和](../Page/瑞典省份.md "wikilink")[自治市](../Page/瑞典自治市.md "wikilink")，也包括了北面的小岛[法羅和](../Page/法羅島.md "wikilink")[戈茨桑](../Page/戈茨桑島.md "wikilink")，以及西面更小的[Karlsö岛](../Page/Karlsö.md "wikilink")。島上共有57317名居民（2006年統計數字），其中22600名居住於主要城市[維斯比](../Page/維斯比.md "wikilink")。島上收入以旅遊業與農業為主。这一区域也是传说中[哥特人起源的一部分](../Page/哥特人.md "wikilink")。

## 地理

歷史上的哥得蘭島屬於哥得蘭舊省。現時管治哥得蘭島的[哥得蘭省](../Page/哥得蘭省.md "wikilink")，和其轄下的惟一一個自治市──[哥得蘭市](../Page/哥得蘭市.md "wikilink")，所覆蓋的土地和舊省相同。哥得蘭省及市的治所俱位於島上5分之2人口（約22600人）居住的[維斯比](../Page/維斯比.md "wikilink")。

哥得蘭位於瑞典大陸以東90公里，距離[波羅的海三國約](../Page/波羅的海三國.md "wikilink")130公里。雖然哥得蘭看來只是一個島嶼，但哥得蘭舊省也包括鄰近幾個小島嶼，同樣受哥得蘭文化影響：

  - [法羅](../Page/法羅島.md "wikilink")（）
  - [Karlsö群島](../Page/Karlsö群島.md "wikilink")（[Stora
    Karlsö和](../Page/Stora_Karlsö.md "wikilink")[Lilla
    Karlsö](../Page/Lilla_Karlsö.md "wikilink")）
  - [戈茨桑](../Page/戈茨桑島.md "wikilink")，瑞典的國家公園

## 地質

哥得蘭島於[志留紀為](../Page/志留紀.md "wikilink")[沉積岩形成](../Page/沉積岩.md "wikilink")，岩石向東南方向沉積。志留紀的[石灰岩和](../Page/石灰岩.md "wikilink")[頁岩由](../Page/頁岩.md "wikilink")13層組成，厚200至500米，其中南部的最厚，覆蓋75至125米厚的[奧陶紀岩石排列](../Page/奧陶紀.md "wikilink")。\[1\]它沉積於一個淺而熱，又充滿鹽分的海上的一個赤道大陸的邊緣\[2\]。其水深從未超越175至200米\[3\]，並隨着時間而變淺。礁石於[蘭多維列世開始形成](../Page/蘭多維列世.md "wikilink")，當時海深50至100米\[4\]。南部一些較晚形成的岩石有一些砂岩，表示沙塊於靠近海岸線的地方形成。\[5\]

哥得蘭島由[沉積岩形成](../Page/沉積岩.md "wikilink")，主要是[石灰岩](../Page/石灰岩.md "wikilink")，但南部也有[砂岩](../Page/砂岩.md "wikilink")。石灰石經風化而形成獨特的[喀斯特地形](../Page/喀斯特地形.md "wikilink")，又稱為[Rauk](../Page/Rauk.md "wikilink")。島上有很多主要由[rogosa](../Page/rogosa.md "wikilink")[珊瑚和](../Page/珊瑚.md "wikilink")[腕足動物組成的化石](../Page/腕足動物.md "wikilink")，海上還有很多保全良好的[海柱](../Page/海柱.md "wikilink")。\[6\]

## 歷史

[Gotland_map.png](https://zh.wikipedia.org/wiki/File:Gotland_map.png "fig:Gotland_map.png")》上的哥得蘭島地圖\]\]
[Gotland_flag.gif](https://zh.wikipedia.org/wiki/File:Gotland_flag.gif "fig:Gotland_flag.gif")
[Visby_Wall.jpg](https://zh.wikipedia.org/wiki/File:Visby_Wall.jpg "fig:Visby_Wall.jpg")
哥得蘭島是[哥得蘭人的居所](../Page/哥得蘭人.md "wikilink")，[Ajvide等遺蹟顯示哥得蘭人自](../Page/Ajvide.md "wikilink")[史前已定居於該島](../Page/史前.md "wikilink")。哥得蘭島很早便成為商業中心，而[維斯比城則是波羅的海上最重要的](../Page/維斯比.md "wikilink")[漢薩城市](../Page/漢薩同盟.md "wikilink")。於中世紀晚期，島上有20個「庭」（），各自選出法官在「島庭」（）作代表。「島庭」負責通過新律法，也會代表整個島作出其他決定。

《》講述[希亞費和其後裔定居於島上的傳說](../Page/希亞費.md "wikilink")。該書也講述三分之一的人口須移居至南歐，與哥特人移居的習俗有關，而哥特人與哥得蘭人（當地人自稱）兩者的名字同源。該書說後來哥得蘭人主動向瑞典國王投誠，並聲稱這是根據雙方當時的協議，而且瑞典國王和主教對哥得蘭有責任和義務。故此，這不但記載了哥得蘭島的歷史，而且指出了哥得蘭島獨立於瑞典。

書中指出，促進哥得蘭人與瑞典國王之間互利協議的人是[Awair
Strabain](../Page/Awair_Strabain.md "wikilink")，而這事發生於9世紀完結前。當時，Wulfstan記載說該島已屬於瑞典人：

  -
    然後，穿越[勃艮第人的土地後](../Page/博恩霍爾姆島.md "wikilink")，我們的左方有從古代已經如此稱呼的[布萊金厄](../Page/布萊金厄.md "wikilink")、[摩爾](../Page/摩爾_\(瑞典\).md "wikilink")、[厄蘭和哥得蘭](../Page/厄蘭.md "wikilink")，這些地方皆屬[斯韋阿人領地](../Page/斯韋阿人.md "wikilink")；[溫德蘭在我們右方](../Page/溫德蘭.md "wikilink")，就在[維斯瓦河口](../Page/維斯瓦河.md "wikilink")。\[7\]

維斯比城和島上其他地方是分別管治的。維斯比的德國商旅和鄉郊的商農因爭執而爆發內戰，由瑞典國王[馬格努斯三世於](../Page/馬格努斯三世.md "wikilink")1288年調停。1361年，丹麥國王[瓦爾德馬四世侵略哥得蘭](../Page/瓦爾德馬四世.md "wikilink")。海盜Victual兄弟於1394年佔據該島，以在維斯比建立堅固的基地。哥得蘭成為[條頓騎士的](../Page/條頓騎士.md "wikilink")[采邑](../Page/采邑.md "wikilink")，以攻打Victual兄弟的堡壘。條頓的侵略軍隊於1398年征服哥得蘭，摧毀維斯比城，並把Victual兄弟驅逐出去。

在哥得蘭島上發現的[阿拉伯](../Page/阿拉伯.md "wikilink")[迪拉姆幣的數目頗多](../Page/迪拉姆幣.md "wikilink")。在島上各處的藏寶處發現的銀幣，比歐亞大陸西部的其他地方發現得還要多，總值僅次於在[穆斯林世界出土的銀幣](../Page/穆斯林.md "wikilink")。這些銀幣透過羅斯商旅和阿拔斯王朝的貿易向北移，而斯堪的納維亞商人賺取的金錢使北歐（尤其是維京斯堪的納維亞和[卡洛林帝國](../Page/卡洛林王朝.md "wikilink")）在之後的幾個世紀成為主要交易中心。

條頓騎士團佔領哥得蘭島後，「島庭」的權威日漸下降，最終賣給[埃里克七世](../Page/埃里克七世.md "wikilink")，並自1449年起由[丹麥派置總督管治](../Page/丹麥.md "wikilink")。中世紀晚期，「島庭」由代表農民、財產完全保有人和佃戶的12個代表組成。自丹麥和瑞典於1645年簽訂《[布勒姆瑟布魯條約](../Page/布勒姆瑟布魯條約.md "wikilink")》起，哥得蘭島由瑞典統治至今。

### 紋章

儘管哥得蘭當時被[丹麥軍隊佔領](../Page/丹麥軍隊.md "wikilink")，但它於1560年獲得了紋章。紋章連同一個公爵冠冕。紋章的口號是：「Azure
a ram statant Argent armed Or holding on a cross-staff of the same a
banner Gules bordered and with five tails of the
third」哥得蘭省於1936獲得同樣紋章作省徽。1971年成立的哥得蘭市獲得的紋章也大致相同。

哥得蘭旗以白色為底，上有紅色哥得蘭紋章，是自13世紀哥得蘭共和國的印章圖案──公羊。上面寫着：「」，可翻譯為「我（即公羊）是哥得蘭人的標誌，小羊象徵基督。」

## 文化

[Axe_of_iron_from_Swedish_Iron_Age,_found_at_Gotland,_Sweden.jpg](https://zh.wikipedia.org/wiki/File:Axe_of_iron_from_Swedish_Iron_Age,_found_at_Gotland,_Sweden.jpg "fig:Axe_of_iron_from_Swedish_Iron_Age,_found_at_Gotland,_Sweden.jpg")

[維斯比的中世紀城鎮已被](../Page/維斯比.md "wikilink")[聯合國教科文組織列為](../Page/聯合國教科文組織.md "wikilink")[世界文化遺產](../Page/世界文化遺產.md "wikilink")，特色是自漢薩時期已包圍舊城的城牆。

哥得蘭島上的居民傳統上說自己的語言，稱為。他們現時說瑞典語的一種方言，稱為「哥得蘭語」（），有人認為是瑞典語中最美的一種方言。13世紀有文獻記載島上的律法，稱為「哥得蘭律法」（），也以古老的gutniska文字寫成。

哥得蘭島以其94座中世紀教堂馳名\[8\]，其中大部份已重建並繼續運作中。這些教堂展示兩大建築特色：[羅曼式和](../Page/羅曼式建築.md "wikilink")[哥德式](../Page/哥德式建築.md "wikilink")。較古老的是1150至1250年的羅曼式教堂，而較新的是在1250至1400年間流行的哥德式建築。其中一座教堂的古老油畫可追溯至12世紀。

哥得蘭島上的傳統遊戲包括[Kubb](../Page/Kubb.md "wikilink")、[Pärk和](../Page/Pärk.md "wikilink")[Varpa](../Page/Varpa.md "wikilink")，是島上夏季節慶「」的一部分。這些遊戲的名聲傳揚，遠至美國也有人玩這些遊戲。

## 當地名人

瑞典力學之父[克里斯多福·波勒姆](../Page/克里斯多福·波勒姆.md "wikilink")（Christopher
Polhem，1661─1751）於維斯比出生，他也被譽為「北方的[阿基米德](../Page/阿基米德.md "wikilink")」。

著名瑞典導演[英格瑪·柏格曼](../Page/英格瑪·柏格曼.md "wikilink")（1918─2007）自中年起住在哥得蘭主島北方的[法羅島上](../Page/法羅島.md "wikilink")，在那裏創造了一些電影，直至在當地去世。

## 哥得蘭公爵

自1772年起，瑞典王子都被封為不同舊省的公爵。這只是名義上的頭銜。

  - [奧斯卡王子](../Page/奧斯卡王子_\(哥得蘭公爵\).md "wikilink")（從1859年出生至1888年失去繼承權）

## 流行文化

暢銷的[瑞典語小說](../Page/瑞典語.md "wikilink")，由[Frans Gunnar
Bengtsson所著的](../Page/Frans_Gunnar_Bengtsson.md "wikilink")《》鮮明地描繪了維京時期的哥得蘭。小說其中一節專門講述駛往俄羅斯的維京船隻，在哥得蘭島上停留並僱用了當地一個舵手，他在航程中擔當重要角色。書中把維京時代的哥得蘭人描述為城市人，較其他斯堪的納維亞人心思縝密和見識廣博，並以其知識和技術自豪。

## 圖片

<File:Gotland> Landsat2000.jpeg|衞星圖片 <File:Gotland> Fårö-Raukar
Gamlehamn.jpg|法羅島上的[Raukar](../Page/Rauk.md "wikilink")
[File:Lärbro_kyrka,_Sweden,_från_Nordisk_Familjebok.jpg|哥得蘭島的Lärbro教堂](File:Lärbro_kyrka,_Sweden,_från_Nordisk_Familjebok.jpg%7C哥得蘭島的Lärbro教堂)

## 外部連結

  - [哥得蘭旅遊網頁](http://www.gotland.com/en/)

  - [波羅的海上的哥得蘭](http://www.selectgotland.com/)、[哥得蘭](https://web.archive.org/web/20061205062556/http://www.alltomgotland.se/)

  - [哥得蘭東部的生活](http://ostergarnslandet.se/)

## 參考資料

<references/>

[Gotland](../Category/瑞典島嶼.md "wikilink")
[Gotland](../Category/瑞典舊省.md "wikilink")

1.

2.  Creer 1973

3.  Gray, Laufield & Boucot, 1974

4.
5.

6.

7.  [Discovery of Muscovy](http://www.gutenberg.org/etext/4076)，Richard
    Hakluyt著

8.  [哥得蘭島以其94座中世紀教堂馳名](http://www.stavar.i.se/churches/mapindexeng.html)