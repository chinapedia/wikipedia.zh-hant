**飯田利信**是[日本的男性](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")。所屬於[Troubadour音樂事務所](../Page/Troubadour音樂事務所.md "wikilink")。[埼玉縣出身](../Page/埼玉縣.md "wikilink")。[血型是A型](../Page/血型.md "wikilink")。

## 經歷、人物

  - 興趣和專長是日本書法檢定初段、籃球、排球、畫畫塗鴉。
  - 和[近藤隆很親近](../Page/近藤隆.md "wikilink")，常一起去喝一杯。
  - 很有個性的性格，自稱「淘氣」。
  - 每年母親節都會寫信給母親說：「感謝您把我生下來」\[1\]
  - 以[家庭教師HITMAN
    REBORN\!中角色雲雀恭彌VS六道骸的名義發售的角色曲在Oricon](../Page/家庭教師HITMAN_REBORN!.md "wikilink")
    single每週排行榜初次上榜第6位、11月期每月排行榜第11位、是男性聲優的角色曲中令人驚異的長打記錄。

## 演出作品

### 電視動畫

  - [ラパヲとラプゾー](../Page/ラパヲとラプゾー.md "wikilink")（**ラパヲ**）

<!-- end list -->

  - 2001年

<!-- end list -->

  - [真·女神轉生 惡魔之子](../Page/真·女神轉生_惡魔之子.md "wikilink")（ネコショウグン
  - [魔力女管家](../Page/魔力女管家.md "wikilink")（マサ）
  - [無敵王トライゼノン](../Page/無敵王トライゼノン.md "wikilink")（討債的人、副師團長、操作人員、要員、研究員A、軍官A）

<!-- end list -->

  - 2002年

<!-- end list -->

  - [神鵰俠侶](../Page/神鵰俠侶_\(動畫\).md "wikilink")（道士）

<!-- end list -->

  - 2003年

<!-- end list -->

  - [クラッシュギアNitro](../Page/クラッシュギアNitro.md "wikilink")（**沖田英俊**）
  - [魔法少年賈修](../Page/魔法少年賈修.md "wikilink")（岡田、MJ12的テレパシス・レーダー、波基李歐）
  - [數碼寶貝最前線](../Page/數碼寶貝最前線.md "wikilink")（ピピスモン）
  - [灌籃少年](../Page/灌籃少年.md "wikilink")（下田利伸）

<!-- end list -->

  - 2004年

<!-- end list -->

  - [陰陽大戰記](../Page/陰陽大戰記.md "wikilink")（剣崎イゾウ）
  - [漫畫派對](../Page/漫畫派對.md "wikilink")（客人）
  - [光之美少女](../Page/光之美少女.md "wikilink")（支倉一樹）
  - [遊戲王GX](../Page/遊戲王GX.md "wikilink")（冥界看守者）

<!-- end list -->

  - 2005年

<!-- end list -->

  - [甲蟲王者](../Page/甲蟲王者.md "wikilink")（獵人）

<!-- end list -->

  - 2006年

<!-- end list -->

  - [飛輪少年](../Page/飛輪少年.md "wikilink")（A･Tライダー、レザボア・ドッグズNo.19、スカルセイダース、金鉄ブルズ、ガンツの先輩、サーベルタイガー、銭湯の店主、ストームライダー、村田）
  - [家庭教師HITMAN
    REBORN\!](../Page/家庭教師HITMAN_REBORN!.md "wikilink")（**六道骸**）
  - [數碼獸拯救者](../Page/數碼獸拯救者.md "wikilink")（ケラモン）
  - [暮蟬悲鳴時解](../Page/暮蟬悲鳴時.md "wikilink")（刑事、山狗）
  - [BLACK BLOOD
    BROTHERS](../Page/BLACK_BLOOD_BROTHERS.md "wikilink")（吸血鬼、ヒロユキ、九龍チャイルド）
  - [奏光之Strain](../Page/奏光之Strain.md "wikilink")（學生）
  - [血色花園](../Page/血色花園.md "wikilink")（尤安）
  - [夜明前的琉璃色](../Page/夜明前的琉璃色.md "wikilink")（男學生、貴族）

<!-- end list -->

  - 2007年

<!-- end list -->

  - [Yes\! 光之美少女5](../Page/Yes!_光之美少女5.md "wikilink")（ケロケロくん）
  - [家庭教師HITMAN
    REBORN\!](../Page/家庭教師HITMAN_REBORN!.md "wikilink")（**六道骸**）
  - [スイスイ\!フィジー\!](../Page/スイスイ!フィジー!.md "wikilink")（くねりん博士、ポチポチさん）
  - [悲慘世界 少女珂賽特](../Page/悲慘世界_少女珂賽特.md "wikilink")（男B、學生、警官C）

<!-- end list -->

  - 2008年

<!-- end list -->

  - [銀魂](../Page/銀魂_\(動畫\).md "wikilink")（謎之獵人"M"）
  - [BUS GAMER](../Page/BUS_GAMER.md "wikilink")（吉田）

<!-- end list -->

  - 2009年

<!-- end list -->

  - [空中鞦韆](../Page/空中鞦韆.md "wikilink")（雄太的朋友、黨羽）
  - [仰望天空的少女瞳中的世界](../Page/仰望天空的少女瞳中的世界.md "wikilink")（同學B）
  - [道子與哈金](../Page/道子與哈金.md "wikilink")（青年）

<!-- end list -->

  - 2010年

<!-- end list -->

  - [笨蛋，測驗，召喚獸](../Page/笨蛋，測驗，召喚獸.md "wikilink")（龍太）
  - [吸血鬼同盟](../Page/吸血鬼同盟.md "wikilink")（小津）

<!-- end list -->

  - 2014年

<!-- end list -->

  - 蠟筆小新（街燈機器人）
  - [普通女高中生要做當地偶像](../Page/普通女高中生要做當地偶像.md "wikilink")（宇佐美達也）

<!-- end list -->

  - 2017年

<!-- end list -->

  - [名侦探柯南](../Page/名侦探柯南_\(动画\).md "wikilink")（鬼、早见裕之 ）

### OVA

  - [デッドガールズ](../Page/デッドガールズ.md "wikilink") - バーガー屋兄さん
  - [豊川水源VP](../Page/豊川水源VP.md "wikilink") - 鬼
  - [Hunter × Hunter](../Page/Hunter_×_Hunter.md "wikilink") - コズフトロ

### 劇場版

  - GUILSTEIN ギルスティン（研究員）
  - [數碼獸拯救者THE](../Page/數碼獸拯救者.md "wikilink")　MOVIE　バーストモード！究極パワー発動（ゴブリモン）

### 廣播劇CD

  - [處女愛上姊姊](../Page/處女愛上姊姊.md "wikilink")（柏木）
  - [原獸文書](../Page/原獸文書.md "wikilink")（キュートス）
  - [伝説の勇者の伝説](../Page/伝説の勇者の伝説.md "wikilink")（リーレ･リンクル）
  - [家庭教師HITMAN
    REBORN\!シリーズ](../Page/家庭教師HITMAN_REBORN!.md "wikilink")（**六道骸**）
      - 家庭教師ヒットマンREBORN\! vsヴァリアー編 Battle.2（DVD初回特典CD）「骸・闇の向こうに」
      - 家庭教師ヒットマンREBORN\! vsヴァリアー編 Battle.6（DVD初回特典CD）「悪夢で会いましょう」
      - 家庭教師ヒットマンREBORN\! vsヴァリアー編
        Battle.8（DVD初回特典CD）「ラジオ並盛・ただいまONAIR中\!?」
      - 家庭教師ヒットマンREBORN\! 未来編 Burn.5（DVD初回特典CD）「クロームは見た\!黒曜ランドの愉快な日常\!」
  - [特攻天女](../Page/特攻天女.md "wikilink")（伊澤）
  - 9番目のムサシ（山本）
  - 京四郎（長崎）
  - スターライトスクランブル恋愛候補生 Radio Scramble Vol.1（記者）
  - 天下一★戦国LOVERS バラエティCD -奥州の月-（**片倉小十郎**）
  - [ヤングガン・カルナバル](../Page/ヤングガン・カルナバル.md "wikilink")（伊勢仁史）

### 音樂CD

  - [家庭教師HITMAN
    REBORN\!シリーズ](../Page/家庭教師HITMAN_REBORN!.md "wikilink")（**六道骸**）
      - 家庭教師ヒットマンREBORN\! キャラクターシングルCDシリーズ第3弾 「記憶の果て／涙の温度」
      - 家庭教師ヒットマンREBORN\! キャラクターシングルCDシリーズ第2弾 「消えない願い／セツナノキヲク」
      - 家庭教師ヒットマンREBORN\! ボンゴレファミリー総登場\!死ぬ気で語れ\!そして歌え\!／「霧の憑依」
      - 家庭教師ヒットマンREBORN\! キャラクターソングCD「Sakura
        addiction」雲雀恭弥vs六道骸(六道骸編)／「クフフのフ～僕と契約～」
      - 家庭教師ヒットマンREBORN\! キャラクターソングCD「かてきょー音頭」リボーンvs沢田綱吉（ドラマパートに出演）

### 遊戲

  - [ロマンシング サガ
    -ミンストレルソング](../Page/ロマンシング_サガ_-ミンストレルソング.md "wikilink")（ダウド、ルーイ）
  - [家庭教師HITMAN REBORN\!](../Page/家庭教師HITMAN_REBORN!.md "wikilink")
    シリーズ（**六道骸**）
      - 家庭教師ヒットマンREBORN\!DS 死ぬ気MAX\! ボンゴレカーニバル\!\!
      - 家庭教師ヒットマンREBORN\!DS フレイムランブル 骸強襲\!
      - 家庭教師ヒットマンREBORN\! ドリームハイパーバトル\! 死ぬ気の炎と黒き記憶
      - 家庭教師ヒットマン REBORN\! ドリームハイパーバトル\! wii
      - 家庭教師ヒットマンREBORN\!DS フレイムランブル開炎 リング争奪戦\!
      - 家庭教師ヒットマンREBORN\! Let's暗殺\!? 狙われた10代目\!
      - 家庭教師ヒットマンREBORN\!DS ボンゴレ式 対戦バトルすごろく
      - 家庭教師ヒットマンREBORN\!DS フェイトオブヒート 炎の運命
      - 家庭教師ヒットマンREBORN\!DS フレイムランブル超 燃えよ未来
      - 家庭教師ヒットマンREBORN\! 狙え\!? リング×ボンゴレトレーナーズ
      - 家庭教師ヒットマンREBORN\! バトルアリーナ
      - 家庭教師ヒットマンREBORN\! 禁断の闇のデルタ

### 其他

  - [saku saku](../Page/saku_saku.md "wikilink") - 解說員
  - [美佳子@ぱよぱよ](../Page/美佳子@ぱよぱよ.md "wikilink") - 第116回來賓
  - [リボラジ\!〜ぶっちゃけリング争奪戦〜](../Page/リボラジ!〜ぶっちゃけリング争奪戦〜.md "wikilink") -
    第11回來賓(途中參加)、第26回來賓
  - [家庭教師ヒットマン REBORN\!](../Page/家庭教師ヒットマン_REBORN!.md "wikilink") 日常編
    后編（初回特典DVD）「並盛オールキャスト座談会〜並盛ホームルーム〜」
  - [家庭教師ヒットマンREBORN\!
    ボンゴレ最強のカルネヴァ－レ](../Page/家庭教師ヒットマンREBORN!_ボンゴレ最強のカルネヴァ－レ.md "wikilink")2008
    〜in 中野サンプラザ〜
  - [家庭教師ヒットマンREBORN\!
    ボンゴレ最強のカルネヴァ－レ](../Page/家庭教師ヒットマンREBORN!_ボンゴレ最強のカルネヴァ－レ.md "wikilink")2009～橫浜にヴァリアー現る～
  - [家庭教師ヒットマンREBORN\!
    ボンゴレ最強のカルネヴァ－レ](../Page/家庭教師ヒットマンREBORN!_ボンゴレ最強のカルネヴァ－レ.md "wikilink")3
    ～白蘭・入江参戦\! in東京・名古屋～

## 參考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - [Troubadour音樂事務所](http://www.troubadour.co.jp/frame.html)

  - [AnimateTV介紹頁](http://www.animate.tv/radio_act/detail.php?id=sa000538&page=0&sea=)

[Category:日本男性配音員](../Category/日本男性配音員.md "wikilink")
[Category:埼玉縣出身人物](../Category/埼玉縣出身人物.md "wikilink")

1.  網路廣播「」