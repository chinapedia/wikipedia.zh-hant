[Huanghuagang_Mausoleum_of_72_Martyrs.jpg](https://zh.wikipedia.org/wiki/File:Huanghuagang_Mausoleum_of_72_Martyrs.jpg "fig:Huanghuagang_Mausoleum_of_72_Martyrs.jpg")
**先烈路**位于[中国](../Page/中国.md "wikilink")[广州市](../Page/广州市.md "wikilink")，为一条呈东北至西南走向的主干道。其西南起[东风东路](../Page/东风路_\(广州\).md "wikilink")，东北接[禺东西路](../Page/禺东西路.md "wikilink")，全长3670[米](../Page/米.md "wikilink")，宽约33米，分东、中、南三段。

## 历史

先烈路原名**东沙马路**，寓意由[大东门外至](../Page/大东门.md "wikilink")[沙河](../Page/沙河街道.md "wikilink")。馬路起始，可追溯到1906年（光緒三十二年）3月，當時由商人主持，於廣州城東郊開始修築第一條城郊馬路，拓闊原有自廣州城大東門到[白雲山的山間土路](../Page/白雲山.md "wikilink")，沿路基兩側掘排水渠，於路基
鋪上1:1混合的泥同沙，壓至4寸厚度。\[1\]，1907年（光绪三十三年）完工，方便了廣州人出城上[白雲山](../Page/白雲山.md "wikilink")，終點設有「息鞭亭」，以便遊人休息\[2\]。因中段有[黄花岗七十二烈士墓](../Page/黄花岗七十二烈士墓.md "wikilink")，为纪念[辛亥革命的烈士](../Page/辛亥革命.md "wikilink")，1921年改今名。

## 特色

### 墓葬

[File:红花岗四烈士墓.JPG|红花岗四烈士墓](File:红花岗四烈士墓.JPG%7C红花岗四烈士墓)
[File:興中會墳場大門.JPG|興中會墳場](File:興中會墳場大門.JPG%7C興中會墳場)
[File:伍漢持墓.JPG|伍烈士漢持墓](File:伍漢持墓.JPG%7C伍烈士漢持墓)
[File:十九路軍抗日陣亡將士墳園凱旋門正面.jpg|十九路軍抗日陣亡將士墳園](File:十九路軍抗日陣亡將士墳園凱旋門正面.jpg%7C十九路軍抗日陣亡將士墳園)
[File:朱执信墓.JPG|朱执信墓](File:朱执信墓.JPG%7C朱执信墓)
[File:新一軍征緬陣亡將士公墓右門.JPG|新一軍征緬陣亡將士公墓原大門](File:新一軍征緬陣亡將士公墓右門.JPG%7C新一軍征緬陣亡將士公墓原大門)

全路有20多个[墓葬](../Page/墓葬.md "wikilink")，曾经是广东省城东郊最好的墓葬区：耶教坟场、庚戌坟场、滇军坟场、海军坟场、警察坟场、五十四军公墓(部分)等，均高度集中在这一区域\[3\],大部分墳場在共和國後被清理或遠遷。

  - **先烈南路**，长800米。南端接[广州起义烈士陵园](../Page/广州起义烈士陵园.md "wikilink")，陵园内还有[红花岗四烈士墓](../Page/红花岗四烈士墓.md "wikilink")（[温生才](../Page/温生才.md "wikilink")、[林冠慈](../Page/林冠慈.md "wikilink")、[陈敬岳](../Page/陈敬岳.md "wikilink")、[钟明光](../Page/钟明光.md "wikilink")）及[叶剑英墓园](../Page/叶剑英.md "wikilink")；[中山大學腫瘤防治中心](../Page/中山大學.md "wikilink")（附屬腫瘤醫院）內有[伍漢持烈士墓](../Page/伍漢持.md "wikilink")；西侧有[兴中会坟场及](../Page/兴中会坟场.md "wikilink")[邓荫南墓](../Page/邓荫南.md "wikilink")。

<!-- end list -->

  - **先烈中路**，长1750米。西侧有[黄花岗七十二烈士墓园](../Page/黄花岗七十二烈士墓园.md "wikilink")，[史坚如](../Page/史坚如.md "wikilink")、[杨仙逸](../Page/杨仙逸.md "wikilink")、[潘达微](../Page/潘达微.md "wikilink")、[邓仲元](../Page/邓仲元.md "wikilink")、[冯如等墓及](../Page/冯如.md "wikilink")[越南](../Page/越南.md "wikilink")[范鸿泰烈士墓等在内](../Page/范鸿泰.md "wikilink")；西侧牛王庙山上有[庚戌新军起义烈士墓](../Page/庚戌新军起义.md "wikilink")；东侧靠近[广州动物园有](../Page/广州动物园.md "wikilink")[张民达墓和](../Page/张民达.md "wikilink")[华侨五烈士墓](../Page/华侨五烈士墓.md "wikilink")（[谢八尧](../Page/谢八尧.md "wikilink")、[邓伯曜](../Page/邓伯曜.md "wikilink")、[郑行果](../Page/郑行果.md "wikilink")、[谭振雄](../Page/谭振雄.md "wikilink")、[范运焜](../Page/范运焜.md "wikilink")）。

<!-- end list -->

  - **先烈东路**，长1120米。南侧沙河顶有[十九路军淞沪抗日阵亡将士陵园](../Page/十九路军淞沪抗日阵亡将士陵园.md "wikilink")；北侧驷马岗有[朱执信墓](../Page/朱执信.md "wikilink")。靠近[濂泉路一带有](../Page/濂泉路.md "wikilink")[新一軍公墓](../Page/新一軍公墓.md "wikilink")（现已被军区、市场、酒店霸占，受严重破坏）。

## 与之交汇道路

  - 顺序由西南往东北排列，**粗体字**表示主干道
  - **[东风东路](../Page/东风路_\(广州\).md "wikilink")**
  - [执信南路](../Page/执信南路.md "wikilink")
  - **[区庄立交](../Page/区庄立交.md "wikilink")**：**[环市东路](../Page/环市路_\(广州\).md "wikilink")**
  - [太和岗路](../Page/太和岗路.md "wikilink")
  - [永福路](../Page/永福路.md "wikilink")、云鹤北街
  - **[内环路](../Page/广州内环路.md "wikilink")**A线入口
  - [水荫路](../Page/水荫路.md "wikilink")
  - [龙岗路](../Page/龙岗路.md "wikilink")
  - [濂泉路](../Page/濂泉路.md "wikilink")
  - **[禺东西路](../Page/禺东西路.md "wikilink")**、**[广州大道北](../Page/广州大道.md "wikilink")**

## 參見

  - [粵軍第一師諸先烈紀念碑](../Page/粵軍第一師諸先烈紀念碑.md "wikilink")

## 外部链接

  - [广州有条先烈路](http://club.dayoo.com/view-14975968-1-1.html)
  - [没有黄花烈士墓就没有先烈路](https://web.archive.org/web/20150602065529/http://www.dayoo.com/gb/content/2001-10/10/content_239706.htm)

[Category:广州主干道](../Category/广州主干道.md "wikilink")
[Category:广州交通史](../Category/广州交通史.md "wikilink")
[Category:广州历史](../Category/广州历史.md "wikilink")
[Category:越秀区](../Category/越秀区.md "wikilink")
[Category:天河区](../Category/天河区.md "wikilink")

1.  [沙河故事](http://www.gzzxws.gov.cn/gxsl/gzwb/201707/t20170725_40980.htm)，盧潔峰。

2.
3.