**台泥國際集團有限公司**（TCC International Holdings
Limited），簡稱**台泥國際**，是[台灣水泥旗下的](../Page/台灣水泥.md "wikilink")[水泥公司](../Page/水泥.md "wikilink")，地區包括[中國](../Page/中國.md "wikilink")、[香港](../Page/香港.md "wikilink")、[菲律賓等區域](../Page/菲律賓.md "wikilink")。

公司於[1962年成立](../Page/1962年.md "wikilink")，最初是水泥生產業務；[1967年於香港開始從事水泥生產業務](../Page/1967年.md "wikilink")，到了[1980年](../Page/1980年.md "wikilink")，[青衣島水泥裝卸站正式開幕](../Page/青衣島.md "wikilink")，以便支持[香港水泥之水泥進口](../Page/香港水泥.md "wikilink")。[1994年](../Page/1994年.md "wikilink")，收購Koning
Concrete Limited全部已經發行股本，而Koning Concrete
Limited則持有[港九混凝土](../Page/港九混凝土.md "wikilink")40%股權；1996年，香港水泥、港九混凝土及[港興分別購入翼冠有限公司的](../Page/港興.md "wikilink")12.5%、15%及20%控股權益；[1997年](../Page/1997年.md "wikilink")10月，正式在[香港交易所主板上市](../Page/香港交易所.md "wikilink")1997年12月，[朱家橋水泥有限公司正式成立](../Page/朱家橋水泥.md "wikilink")，並已經開始動工興建年產能達700,000公噸的礦渣粉研磨廠；[2000年](../Page/2000年.md "wikilink")，更名為「台泥國際集團」；[2001年](../Page/2001年.md "wikilink")9月，台泥國際集團與賣方就收購[和信電訊合共約](../Page/和信電訊.md "wikilink")0.94%已經發行股本而簽訂收購協議，此收購事項的總代價約110,490,000港元，自此**台泥國際**最大的股東就是[台灣水泥公司的辜家佔有六成以上的持股](../Page/台灣水泥.md "wikilink")，其次才是[嘉新水泥](../Page/嘉新水泥.md "wikilink")。

[2013年](../Page/2013年.md "wikilink")11月27日，台灣水泥宣布：以每股收購價為3.9港元，較其停牌前3.06港元溢價27.45%，以及收購未持有優先股等，預計涉及總代價58.88億港元，私有化台泥國際集團，但於2014年5月，因第二大股東嘉泥的反對，而未竟全功\[1\]。

[2014年](../Page/2014年.md "wikilink")，台泥國際以12.7億元人民幣購併湖南金大地材料公司的全部股權，切入湖南地區的水泥建材市場\[2\]。

[2017年](../Page/2017年.md "wikilink")1月23日，前主席[辜成允在](../Page/辜成允.md "wikilink")[臺北市](../Page/臺北市.md "wikilink")[北投區](../Page/北投區.md "wikilink")[振興醫院去世](../Page/振興醫院.md "wikilink")，改由[張安平委任為主席及非執行董事](../Page/張安平.md "wikilink")\[3\]。

2017年4月20日，台灣水泥宣布，以每股不超過港幣3.6港元，斥資65.76億港元（約新台幣256.46億港元），並已獲[臺灣證交所](../Page/臺灣證交所.md "wikilink")、[香港聯交所及註冊地](../Page/香港聯交所.md "wikilink")[開曼群島法院核准](../Page/開曼群島.md "wikilink")，並於9月18日召開股東會表決收購台泥國際在外流通全部股權，私有化台泥國際\[4\]。

## 收購軼事

2007年6月，[摩根士丹利亞洲有限公司代表台泥國際集團提出之自願有條件](../Page/摩根士丹利.md "wikilink")[要約以收購](../Page/要約.md "wikilink")[嘉新水泥中國股本中全部已發行股份](../Page/嘉新水泥中國.md "wikilink")；到了2007年10月，已經轉為在各方面成為無條件；最後在2008年1月已完成強制收購嘉新水泥中國及撤銷其上市地位\[5\]\[6\]。

2009年12月，台泥國際集團以40億元港元向[昌興國際收購旗下水泥附屬公司Prosperity](../Page/昌興國際.md "wikilink")
Minerals。\[7\]

## 連結

  - [TTCHK.com](http://www.tcchk.com)

## 參考

[Category:中國民營企業](../Category/中國民營企業.md "wikilink")
[Category:台灣水泥](../Category/台灣水泥.md "wikilink")
[Category:1962年成立的公司](../Category/1962年成立的公司.md "wikilink")
[Category:和信集團](../Category/和信集團.md "wikilink")
[Category:香港交易所已除牌公司](../Category/香港交易所已除牌公司.md "wikilink")

1.  [嘉泥拒賣
    台泥國際私有化受挫](http://www.chinatimes.com/newspapers/20140508001840-260110)，中時電子報，2014-05-08
2.  路透香港，[台泥拟12.7亿购湖南金大地全部股权](http://www.ccement.com/news/content/7683520822898.html)，中國水泥網，2014-11-10
3.  [主席及執行董事離世、 委任主席、 委任提名委員會主席 及
    委任薪酬委員會成員公告](http://www.hkexnews.hk/listedco/listconews/sehk/2017/0123/LTN20170123483_C.pdf)
4.  高行，[台泥國際在港下市
    准了](https://udn.com/news/story/7252/2662762)，聯合新聞網，2017-08-25
5.  [台泥国际将收购嘉新水泥中国](http://info.china.alibaba.com/news/detail/v9-d1000390897.html)
6.  [台泥私有嘉新涉25億](http://the-sun.on.cc/channels/fina/20070616/20070616011644_0000.html)
    太陽報
7.  [台泥擬40億購昌興水泥業](http://hk.apple.nextmedia.com/template/apple/art_main.php?iss_id=20091201&sec_id=15307&subsec=15320&art_id=13476171)
    蘋果日報，2009年12月1日