《**鳥之星 ～Aerial
Planet～**》是[日本一軟件於](../Page/日本一軟件.md "wikilink")2008年2月28日發售的[PlayStation
2用](../Page/PlayStation_2.md "wikilink")[冒險遊戲及](../Page/冒險遊戲.md "wikilink")[模擬飛行遊戲](../Page/模擬飛行遊戲.md "wikilink")。是第一個發表收錄以「[初音未來](../Page/初音未來.md "wikilink")」製作的音樂的商業產品（但並非第一個發售\[1\]）。

## 故事簡介

時間是人類已超越[相對論而能夠做出](../Page/相對論.md "wikilink")[光速以上的移動](../Page/光速.md "wikilink")，可於[恆星間旅行的近來來](../Page/恆星.md "wikilink")。博士和其兒子正從[地球回到距離地球](../Page/地球.md "wikilink")3000[光年的](../Page/光年.md "wikilink")[白鳥座α](../Page/白鳥座.md "wikilink")[天津四附近](../Page/天津四.md "wikilink")，98%被水覆蓋的G型行星「」。然而進入行星軌道時發生意外而墜下，只有Hugo成功逃出，博士則生死不明。墜下的地點和行星基地距離數千公里，在失去通訊機器和只有數名搜索人員的情況下，Hugo可否成功到達目的地？

## 登場人物

  -


    博士

## 與初音未來的合作

此遊戲是第一個發表收錄以[音樂軟件](../Page/音樂軟件.md "wikilink")[VOCALOID2](../Page/VOCALOID.md "wikilink")[角色主唱系列](../Page/角色主唱系列.md "wikilink")「[初音未來](../Page/初音未來.md "wikilink")」製作的音樂的商業產品，選擇初音未來是因為遊戲的營業專案隊長小酒井省吾當初聽初音未來的音樂時，有一種是真人歌唱般的錯覺\[2\]。消息最初於2007年11月9日發售的遊戲雜誌「電撃
PlayStation」發表，但日本一軟件的官方網站在數日前已出現初音未來的剪影\[3\]\[4\]。四首歌曲中有三首會在網站上發放，剩下的一首會收錄於特典CD中\[5\]。第一首於2007年12月末公開，第二、三首續月公開。

## 參見

  - [日本一軟件](../Page/日本一軟件.md "wikilink")
  - [魔界戰記](../Page/魔界戰記.md "wikilink")
  - [初音未來](../Page/初音未來.md "wikilink")

## 参考資料

<div class="references-small">

<references />

  -

</div>

## 外部連結

  - [遊戲官方網站](http://nippon1.jp/consumer/torinohoshi/)

  - [日本一軟件產品](http://nippon1.jp/index.html)

  - [日本一軟件公司](http://nippon1.co.jp/index.html)

[Category:2008年电子游戏](../Category/2008年电子游戏.md "wikilink")
[Category:PlayStation 2游戏](../Category/PlayStation_2游戏.md "wikilink")
[Category:冒險遊戲](../Category/冒險遊戲.md "wikilink")
[Category:日本一软件游戏](../Category/日本一软件游戏.md "wikilink")

1.
2.  《DTM MAGAZINE》2008年1月號 P.28
3.  電撃 PlayStation 2007年 11/9號
4.
5.