[Goddard_aerial.gif](https://zh.wikipedia.org/wiki/File:Goddard_aerial.gif "fig:Goddard_aerial.gif")
**高達德太空飛行中心**（Goddard Space Flight
Center）是[美國國家航空暨太空總署一個主要研究中心](../Page/美國國家航空暨太空總署.md "wikilink")，位於[華盛頓特區東北方約](../Page/華盛頓特區.md "wikilink")6.5[公里處](../Page/公里.md "wikilink")[馬里蘭州的](../Page/馬里蘭州.md "wikilink")[綠帶城](../Page/綠帶城.md "wikilink")。高達德太空飛行中心成立於1959年5月1日，也是美國國家航空暨太空總署首座太空飛行中心，目前大約有10,000名員工。高達德太空飛行中心以太空先驅[羅伯特·戈達德來命名](../Page/羅伯特·戈達德.md "wikilink")。

## 設施

## 参考资料

## 外部連結

  - [NASA Headquarters](http://www.nasa.gov/centers/hq/home/)
  - [Goddard Space Flight
    Center](http://www.nasa.gov/centers/goddard/home/)
  - [Dateline Goddard
    newsletter](https://web.archive.org/web/20060211061422/http://www.gsfc.nasa.gov/gsfc/dateline.html)
  - [Goddard Employees Welfare Association](http://gewa.gsfc.nasa.gov/)
    (GEWA)
  - [Goddard Fact
    Sheets](http://www.nasa.gov/centers/goddard/about/fact_sheets.html)
  - [The Goddard Homer E. Newell Memorial
    Library](https://web.archive.org/web/20060316135812/http://library.gsfc.nasa.gov/public/)
  - [Goddard Visitor
    Center](http://www.nasa.gov/centers/goddard/visitor/home/)
  - [Independent Verification and Validation
    Facility](http://www.ivv.nasa.gov/)
  - [*Dreams, Hopes, Realities: NASA's Goddard Space Flight Center, The
    First Forty Years*](http://history.nasa.gov/SP-4312/sp4312.htm)
  - [Goddard Amateur Radio Club](http://garc.gsfc.nasa.gov/)

[Category:美國國家航空暨太空總署](../Category/美國國家航空暨太空總署.md "wikilink")
[Category:航天中心](../Category/航天中心.md "wikilink")