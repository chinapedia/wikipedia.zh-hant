**平庸原理**（，又稱**平庸的原則**）是一種[科學哲學觀念](../Page/科學哲學.md "wikilink")，指出人類或者地球在宇宙中不存在任何特殊地位或重要性。这是一种[哥白尼的理论](../Page/哥白尼.md "wikilink")。不管是由[启发法得出的](../Page/启发法.md "wikilink")[地球所在的位置](../Page/地球.md "wikilink")，还是根据[哲学原理阐述人类的地位](../Page/哲学.md "wikilink")，都能得出这种结论，此外还有下列论据：

  - 化石記錄以及[基因研究結論出所有人類是約](../Page/基因.md "wikilink")100,000年前的一個共同祖先的後代而人類祖先和黑猩猩在約六百萬年前在演化中分道揚鑣。因此人類只是生物圈的一部份，並非獨特的一種生物。
  - 人類的基因和[黑猩猩的基因有超過](../Page/黑猩猩.md "wikilink")98%相同，黑猩猩在和人類分開演化的過程內甚至擁有比人類更多的基因變異。\[1\]
  - 上世纪对[薛定谔提出的](../Page/薛定谔.md "wikilink")[生命是什么进行研究的结果](../Page/生命是什么.md "wikilink")，发现了[DNA的双螺旋结构](../Page/DNA.md "wikilink")，将生命简化到[有机化学领域](../Page/有机化学.md "wikilink")，否定了[生机论学说](../Page/生机论.md "wikilink")。[PaleBlueDot.jpg](https://zh.wikipedia.org/wiki/File:PaleBlueDot.jpg "fig:PaleBlueDot.jpg")
  - [愛德文·哈勃发现宇宙远比人们以前想象的要大](../Page/愛德文·哈勃.md "wikilink")，[詹姆斯·赫顿发现地球的年龄比人们以前认为的要更古老](../Page/詹姆斯·赫顿.md "wikilink")，[哈伯深空包括了成千上万个河外星系](../Page/哈伯深空.md "wikilink")，更加支持了平庸原理的观点。
  - 1990年2月14日，[旅行者1号从](../Page/旅行者1号.md "wikilink")64亿千米以外拍摄到地球的照片，只是一个[暗淡藍點](../Page/暗淡藍點.md "wikilink")。

## 地球只是一个普通的行星

哥白尼的平庸原理理论有以下的论点：以前古代西方和中东认为地球是宇宙的中心，但哥白尼推测太阳才是宇宙的中心，100年后[伽利略用望远镜发现了木星的卫星和金星绕日运行的轨道](../Page/伽利略.md "wikilink")，验证了[日心说](../Page/日心说.md "wikilink")。1930年，[特朗普勒发现太阳并不是](../Page/特朗普勒.md "wikilink")[银河系的中心](../Page/银河系.md "wikilink")，[雅各布斯·卡普坦阐述是在](../Page/雅各布斯·卡普坦.md "wikilink")[銀心外到边缘](../Page/銀心.md "wikilink")56%的地方。20世纪中叶，[乔治·伽莫夫等人根据](../Page/乔治·伽莫夫.md "wikilink")[哈勃定律认为银河系位于不断膨胀的宇宙中心](../Page/哈勃定律.md "wikilink")。20世纪末，[杰弗里·马西等人发现](../Page/杰弗里·马西.md "wikilink")[太陽系外行星相当普遍](../Page/太陽系外行星.md "wikilink")，证明了太阳具有行星并不是一个特例。所以哥白尼的平庸原理是被[天文学的一系列发现所证明的](../Page/天文学.md "wikilink")，地球不过是在广漠的宇宙中无数星系中的一个[普通行星](../Page/德雷克公式.md "wikilink")，也许是在无限个[多元宇宙中更为普通的星](../Page/平行宇宙.md "wikilink")。

### 反对的观点

为了证实地球和人类只是宇宙中最普遍的现象，启动了[搜寻地外文明计划](../Page/搜寻地外文明计划.md "wikilink")，[卡尔·萨根认为](../Page/卡尔·萨根.md "wikilink")“在银河系可能有成千上万个文明存在。”但迄今没有找到任何外星信号和[费米悖论的证明](../Page/费米悖论.md "wikilink")，所以這是对平庸原理的一个打击，又或許仍然存在著外星文明，但由于和我们距离太遥远，所以无法监测出来。

相同的，以尺度和機率的角度與視野來觀察，地球屬於適居帶的行星，擁有且滿足一切[生物](../Page/生物.md "wikilink")[物種維持](../Page/物種.md "wikilink")[生命](../Page/生命.md "wikilink")、[生存和](../Page/生存.md "wikilink")[演化的所有條件](../Page/演化.md "wikilink")，然而事實上從[地球歷史中的](../Page/地球歷史.md "wikilink")[顯生宙開始至今](../Page/顯生宙.md "wikilink")，在這長達五億多年的歲月間和數百萬的生物物種中，只有一個物種成功的演化成為高等智慧生命－「[人類](../Page/人類.md "wikilink")」，而非多種多元的高等智慧生物並存於地球上，這顯示了在「相同條件」下，「高等智慧生命」並非如此的輕易出現和存在。同地球殊異假說一般，這或許為費米悖論提供了一個答案。

和平庸原理相競爭的是[地球殊异假说](../Page/地球殊异假说.md "wikilink")，[吉勒莫·冈萨雷斯和理查德在](../Page/吉勒莫·冈萨雷斯.md "wikilink")2004年出版的《[独特的星球](../Page/独特的星球.md "wikilink")》一书中，就支持地球殊异假说：

支持地球殊异假说的人们认为，不仅这个宇宙为了生命产生而精心调整的，而且地球也是一个非常特殊的[暗淡藍點](../Page/暗淡藍點.md "wikilink")，2000年[皮特·瓦尔德发表了](../Page/皮特·瓦尔德.md "wikilink")《[稀有的地球：为什麽复杂的生命在宇宙中是不平常的](../Page/稀有的地球：为什麽复杂的生命在宇宙中是不平常的.md "wikilink")》。这种观点由下列论据支持：

  - [太阳是一个大小合适](../Page/太阳.md "wikilink")，光照稳定，光谱频率正合适的星球，如果太阳再大一些，就会燃烧更快，而生命无法即时产生；如果太阳再小一些，地球就需要离太阳更近一些，轨道就会不稳定。
  - 地球的轨道近乎圆形，是由于[木星重力的影响](../Page/木星.md "wikilink")，造成[太阳系行星几乎都是这种近乎圆形的轨道](../Page/太阳系.md "wikilink")，但在其他[太陽系外行星中](../Page/太陽系外行星.md "wikilink")，这种轨道是很少见的。
  - [地球是硅质岩石的大型](../Page/类地行星.md "wikilink")[板块和](../Page/板块构造论.md "wikilink")[铁质地心组成的](../Page/铁.md "wikilink")，保护了生命防止受到辐射的伤害。
  - [木星和其他外层大行星保护了地球不受来自太阳系外的小行星和](../Page/木星.md "wikilink")[彗星的撞击而导致轨道不稳定](../Page/彗星.md "wikilink")。
  - 地球含有大量的[水和稳定活跃的](../Page/水.md "wikilink")[水圈](../Page/水圈.md "wikilink")。
  - [月球相当大](../Page/月球.md "wikilink")，造成海洋的潮汐，因此稳定了地轴的倾角，根据[拉斯卡的计算](../Page/拉斯卡.md "wikilink")，如果没有月亮是无法如此稳定的。
  - 由于月球是从地球分离出来的，带走相当[火星质量的地壳物质](../Page/火星.md "wikilink")，导致地壳物质缺乏，因此形成[板块构造](../Page/板块构造论.md "wikilink")，如果没有这样的板块构造，地球可能和[金星一样](../Page/金星.md "wikilink")，全球到处分布着[火山](../Page/火山.md "wikilink")。
  - 地球在银河系中的位置非常罕见并重要：“不在银河中心，不在星系分支中，没有靠近任何[伽马射线源](../Page/伽马射线.md "wikilink")，不是在多[恒星星系中](../Page/恒星.md "wikilink")，不靠近太小的恒星，不靠近太大的恒星，不靠近很快要爆发超新星的恒星。”（《稀有的地球》，282页）
  - 几十亿年中，地球的轨道和温度非常稳定，没有非常特殊的极大灾难，着实是非常罕见的。

## 关于人类平凡的论点

  - 2003年的[人类基因组计划发现](../Page/人类基因组计划.md "wikilink")，人类只有24,000[基因](../Page/基因.md "wikilink")，1990年代还认为人类起码有300,000个复杂的基因。
  - [演化心理学发现的人类的合理极限](../Page/演化心理学.md "wikilink")，[生物心理学根据](../Page/生物心理学.md "wikilink")[功能性磁共振成像揭露了人类认识](../Page/功能性磁共振成像.md "wikilink")、道德和感觉的物质属性，[经济和](../Page/经济.md "wikilink")[政治的研究发现了人类群体的行为规律](../Page/政治.md "wikilink")。
  - [克里克的著作](../Page/佛朗西斯·克里克.md "wikilink")《令人惊讶的假说》认为[意识只不过是大脑的简单的功能](../Page/意识.md "wikilink")。

## 哲学描述

在[哲学范畴](../Page/哲学.md "wikilink")，从[文艺复兴时期就有符合平庸原理的各种理论出现](../Page/文艺复兴.md "wikilink")。根据罗马[天主教当时的教条](../Page/天主教.md "wikilink")，人类是上帝依据自己形象创造的最完美的造物，所以必然要放到宇宙的中心，所以哥白尼的理论根据神学的结论只是说人类并不是最完美的造物，但实际从[但丁的](../Page/但丁.md "wikilink")《[神曲](../Page/神曲.md "wikilink")》所描写的，虽然仍然是一个完美的天空，但已经将人类描述为并不是完美的，而是宇宙的垃圾和渣滓，地球只是一个地基，哥白尼的理论对他来说不仅没有将地球降格，反而是提升了，只是一个[思维转换而已](../Page/範式轉移.md "wikilink")。

## 相關條目

  - [地球殊異假說](../Page/地球殊異假說.md "wikilink")
  - [人擇原理](../Page/人擇原理.md "wikilink")

## 參考文献

  - Gonzalez, Richards, *The Privileged Planet: How Our Place in the
    Cosmos is Designed for Discovery*
    [2004](../Page/2004_in_literature.md "wikilink"), Regnery
    Publishing, ISBN 0-89526-065-4
  - Peter Ward and Donald Brownlee. *Rare Earth: Why Complex Life is
    Uncommon in the Universe*. **Copernicus** Books. January
    [2000](../Page/2000_in_literature.md "wikilink"). ISBN 0-387-98701-0

<div class="references-small" style="-moz-column-count:1; column-count:1;">

<references/>

</div>

## 外部链接

  - [Goodwin, Gribbin, and Hendry's 1997 Hubble Parameter measurement
    relying on the mediocrity
    principle](http://arxiv.org/PS_cache/astro-ph/pdf/9704/9704289.pdf)
    The authors call this the 'Principle of Terrestrial Mediocrity' even
    though the assumption they make is that the Milky Way *Galaxy* is
    typical (rather than Earth). This term was coined by Alexander
    Vilenkin (1995)

[Category:地球](../Category/地球.md "wikilink")
[Category:科學哲學](../Category/科學哲學.md "wikilink")
[Category:原則](../Category/原則.md "wikilink")
[Category:SETI](../Category/SETI.md "wikilink")

1.  <http://www.livescience.com/animalworld/070417_chimps_evolve.html>