[Busan_chinatown.jpg](https://zh.wikipedia.org/wiki/File:Busan_chinatown.jpg "fig:Busan_chinatown.jpg")中國街\]\]
**朝鲜半岛华人**即朝鮮半島的[漢族](../Page/漢族.md "wikilink")，指的是[-{zh-hans:朝鲜;zh-hant:北韓}-與](../Page/朝鮮民主主義人民共和國.md "wikilink")[-{zh-hans:韩国;zh-hant:南韓}-分治前](../Page/大韓民國.md "wikilink")，[移民至](../Page/移民.md "wikilink")[朝鲜半岛的](../Page/朝鲜半岛.md "wikilink")[華人](../Page/華人.md "wikilink")，是除了主體民族[朝鮮族以外的第二大本地民族與最大的](../Page/朝鮮族.md "wikilink")[少數民族](../Page/少數民族.md "wikilink")。現今至少發展至第三及四代。在-{zh-hans:韩国;zh-hant:南韓}-，他们自稱**韓華**（**韓**国**華**人），而-{zh-hans:韩国;zh-hant:南韓}-人通常稱呼他們為[華僑](../Page/華僑.md "wikilink")。

## 概述

朝鲜半岛华人於朝鮮半島上，人口雖然為僅次於[朝鮮族的第二大族群](../Page/朝鮮族.md "wikilink")，惟重要性有趨於弱化的發展。现今在-{zh-hans:韩国;zh-hant:南韓}-約有23,000人，在-{zh-hans:朝鲜;zh-hant:北韓}-約有22,133人。約佔半岛總人口7,000萬的0.07%。-{zh-hans:韩国;zh-hant:南韓}-华人大部份聚居於[仁川](../Page/仁川.md "wikilink")，其次是[釜山及](../Page/釜山.md "wikilink")[首爾](../Page/首爾.md "wikilink")；而-{zh-hans:朝鲜;zh-hant:北韓}-華人則居[平壤及中朝邊境地區為多](../Page/平壤.md "wikilink")。

與一般[海外華人來自](../Page/海外華人.md "wikilink")[廣東及](../Page/廣東.md "wikilink")[福建不同](../Page/福建.md "wikilink")，-{zh-hans:韩国;zh-hant:南韓}-華人95%以上來自[山東](../Page/山東.md "wikilink")，其中先祖[籍貫以](../Page/籍貫.md "wikilink")[煙台](../Page/煙台.md "wikilink")、[青島及](../Page/青島.md "wikilink")[威海居多](../Page/威海.md "wikilink")；-{zh-hans:朝鲜;zh-hant:北韓}-華人多數來自[日帝強佔期或](../Page/日帝強佔期.md "wikilink")[偽滿時期的移民](../Page/偽滿洲國.md "wikilink")。華人於世界各地在[經濟上多有所成就](../Page/經濟.md "wikilink")，唯獨-{zh-hans:韩国;zh-hant:南韓}-華僑的經濟難以發展，原因是1970年代[-{zh-hans:韩国;zh-hant:南韓}-總統](../Page/韓國總統.md "wikilink")[朴正熙開始](../Page/朴正熙.md "wikilink")，[-{zh-hans:韩国;zh-hant:南韓}-政府頒布許多排斥華商的政策與法令](../Page/大韓民國政府.md "wikilink")，包括每戶人家限於購買房產一筆，以稅法嚴格限制華僑的經濟活動。此致，1970年代尚有華僑十餘萬人的-{zh-hans:韩国;zh-hant:南韓}-，於1990年代大幅度地下滑至20,000多人。其中多數移民至[美國及](../Page/美國.md "wikilink")[加拿大](../Page/加拿大.md "wikilink")，一部分於70、80年代前往[臺灣](../Page/臺灣.md "wikilink")，並在[永和區形成](../Page/永和區.md "wikilink")[韓國街](../Page/頂溪商圈.md "wikilink")。留於-{zh-hans:韩国;zh-hant:南韓}-者，除了已經有相當的經濟成就者外，生活品質僅維持於小康。隨著-{zh-hans:韩国;zh-hant:南韓}-積極開發[中國大陸](../Page/中國大陸.md "wikilink")[市場](../Page/市場.md "wikilink")，有少數華僑從事[導遊或者](../Page/導遊.md "wikilink")[翻譯工作而居中受惠](../Page/翻譯.md "wikilink")。-{zh-hans:朝鲜;zh-hant:北韓}-華僑比本國民衆享有往返中國的自由，但没有[出身成分](../Page/出身成分.md "wikilink")，不能加入[朝鮮勞動黨](../Page/朝鮮勞動黨.md "wikilink")，不能服兵役。當局早年優待華僑，1960年代對華僑實施強迫歸順或回國政策，但後來中止這一政策。[-{zh-hans:朝鲜;zh-hant:北韓}-飢荒發生以後](../Page/朝鲜饥荒.md "wikilink")，一些華僑因從事跨國經濟活動而居中受惠，生活品質明顯提高，但近年來遭受當局的監視甚至逮捕。

華僑有別於[中華人民共和國與大韓民國建交後](../Page/中華人民共和國.md "wikilink")，從[中國大陸前往](../Page/中國大陸.md "wikilink")-{zh-hans:韩国;zh-hant:南韓}-就學、就業及居留的新移民（約54萬人：39萬爲[中國朝鮮族](../Page/中國朝鮮族.md "wikilink")，15萬爲[漢族或其他民族](../Page/漢族.md "wikilink")）。

## 歷史

根據朝鮮半島[歷史](../Page/歷史.md "wikilink")，-{zh-hans:朝鲜;zh-hant:韓國}-不少氏族的祖先皆源自[中原](../Page/中原.md "wikilink")。這些人主因戰亂因素來到朝鮮半島南部生活（[辰韓人族源之一是中原人](../Page/辰韓.md "wikilink")）。直到今日-{zh-hans:朝鲜;zh-hant:韓國}-戶籍冊所記錄的[本貫看到不少昔日中原人移居朝鮮半島的痕跡](../Page/本貫.md "wikilink")，如：[蘇州賈氏](../Page/蘇州賈氏.md "wikilink")。惟他們已經本土化，不再被視為華人。

清朝末年，為了逃避戰亂，不少華人從[山東半島乘船越](../Page/山東半島.md "wikilink")[黃海](../Page/黃海.md "wikilink")，登陸仁川、[釜山和當時的](../Page/釜山.md "wikilink")[漢城](../Page/漢城.md "wikilink")。登陸後，多數經營[農業](../Page/農業.md "wikilink")、[飲食業和](../Page/飲食業.md "wikilink")[建築業](../Page/建築業.md "wikilink")。1882年7月，清朝在朝鲜政府的請求派提督[吳長慶和](../Page/吳長慶.md "wikilink")3000部隊，以幫助平息了叛亂（[壬午軍亂](../Page/壬午軍亂.md "wikilink")）。伴隨部隊有40名中國商人和其他平民。

1910年[朝鮮半島日治時代時](../Page/朝鮮日佔時期.md "wikilink")，-{zh-hans:朝鲜;zh-hant:韓國}-華人中不乏[企業家及富商](../Page/企業家.md "wikilink")。[日本人一方面要利用華人資源](../Page/日本人.md "wikilink")，一方面又防止華人坐大，當時有規定：大規模的基礎建設僱用華人不得超過總工人人數的1/3。

[万宝山事件后](../Page/万宝山事件.md "wikilink")，在[朝鮮半島的華人受到迫害](../Page/朝鮮半島.md "wikilink")。

### \-{zh-hans:韩国;zh-hant:南韓}-

1948年大韓民國成立後，前往-{zh-hans:韩国;zh-hant:南韓}-的中國移民仍然不少。尤其最近一次來自中國大陸的移民潮出現在[國共內戰](../Page/國共內戰.md "wikilink")（1949年）前後。例如，中華民國政府撤出中國大陸時，大量華人自[中國東北或中國](../Page/中國東北.md "wikilink")[山東省進入](../Page/山東省.md "wikilink")-{zh-hans:韩国;zh-hant:南韓}-；少数来自中國沿海其他地區。

大韓民國成立後，中華民國政府在[韓戰期間曾支援大韓民國](../Page/韓戰.md "wikilink")。1961年，被-{zh-hans:韩国;zh-hant:南韓}-人視為現代化之父的前[-{zh-hans:韩国;zh-hant:南韓}-總統](../Page/韓國總統.md "wikilink")[朴正熙為了發展](../Page/朴正熙.md "wikilink")[韓國經濟](../Page/韓國經濟.md "wikilink")，開始實施“不歡迎華僑政策”。對華人經濟的限制變本加厲。-{zh-hans:韩国;zh-hant:南韓}-政府為保護當地人的企業，曾設立外資限額，禁止華人從事房地產、外貿等多項行業。限制華人無論在-{zh-hans:韩国;zh-hant:南韓}-居住多久，都不准在[政府](../Page/政府.md "wikilink")、[軍隊及大型企業任職](../Page/軍隊.md "wikilink")。至2002年為止，-{zh-hans:韩国;zh-hant:南韓}-華人要到[銀行借貸款](../Page/銀行.md "wikilink")，須付高出-{zh-hans:韩国;zh-hant:南韓}-人很多的[利息](../Page/利息.md "wikilink")；求職不得公平待遇，購買[手機亦須預付高額保證金](../Page/手機.md "wikilink")。另外-{zh-hans:韩国;zh-hant:南韓}-華人身份證號碼不同于-{zh-hans:韩国;zh-hant:南韓}-公民。現在中國人到-{zh-hans:韩国;zh-hant:南韓}-留學的，將近五千人，而去打工的更多，尤其是東北的[中國朝鮮族](../Page/中國朝鮮族.md "wikilink")，但是大部份從事家庭幫傭或者是打工仔。

2005年，-{zh-hans:韩国;zh-hant:南韓}- MChinatown
公司和中國的清華有限公司簽約，在[首爾西北](../Page/首爾.md "wikilink")、位於[京畿道](../Page/京畿道.md "wikilink")[高陽市的一山區建造一個新的](../Page/高陽市.md "wikilink")[中華街](../Page/唐人街.md "wikilink")。這個中華街預定於2007年3月落成，佔地1.5萬[坪](../Page/坪.md "wikilink")（約4.9500萬平方米），將會是-{zh-hans:韩国;zh-hant:南韓}-最大型的中華街。

2006年5月31日，-{zh-hans:韩国;zh-hant:南韓}-举行地方政府行政长官选举，首次讓在-{zh-hans:韩国;zh-hant:南韓}-有定居权並且合法居住3年以上，[年齡在](../Page/年齡.md "wikilink")19岁以上的外国人行使投票权。-{zh-hans:韩国;zh-hant:南韓}-華人首次有了投票權，但依然不能选举国会议员。

\-{zh-hans:韩国;zh-hant:南韓}-曾被人喻為“世界上唯一華人無法發展的國家”。由于-{zh-hans:韩国;zh-hant:南韓}-處處嚴格限制華人的[經濟與社會發展](../Page/經濟.md "wikilink")，當地華人人口從1970年的12萬降低到2004年的2萬3千人。其中90%以上为華僑。因為歷史原因，同時也因為-{zh-hans:韩国;zh-hant:南韓}-[入籍限制很嚴格](../Page/入籍.md "wikilink")，特別是對於出身[漢族的華僑更嚴格](../Page/漢族.md "wikilink")，因此絕大多數不擁有-{zh-hans:韩国;zh-hant:南韓}-國籍和-{zh-hans:韩国;zh-hant:南韓}-身份證號碼，而持有[中華民國護照](../Page/中華民國護照.md "wikilink")），僅持有特殊號碼的[身份證](../Page/身份證.md "wikilink")。另外，由於-{zh-hans:韩国;zh-hant:南韓}-政府對外籍人士的不動產有特別限制，極少數從事房地產相關行業的華僑，爲了經商便利而經特-{准}-加入-{zh-hans:韩国;zh-hant:南韓}-國籍。

### \-{zh-hans:朝鲜;zh-hant:北韓}-

\-{zh-hans:朝鲜;zh-hant:北韓}-華僑1922年高峰時達十萬人。1931年，[萬寶山事件發生后](../Page/萬寶山事件.md "wikilink")，大量華僑回國，人口降至三、四萬。至1945年日本投降時，微升至五萬人。60年代，华侨人数下降到二万人，70年代只剩下六、七千人。2002年，-{zh-hans:朝鲜;zh-hant:北韓}-華僑总数約2萬2133人。其中一半以上居住在平壤；[平安北道有](../Page/平安北道.md "wikilink")300户；[慈江道和](../Page/慈江道.md "wikilink")[咸镜北道有](../Page/咸镜北道.md "wikilink")200户。2009年，中國表示居住在-{zh-hans:朝鲜;zh-hant:北韓}-的華僑人數约有5000名\[1\]。

[日本投降後](../Page/日本投降.md "wikilink")，[蘇聯政府在](../Page/蘇聯.md "wikilink")-{zh-hans:朝鲜;zh-hant:北韓}-實施公民證制度，把百姓分為“平壤居民”、“地方居民”和“外國人”，華僑作為“[外國人](../Page/外國人.md "wikilink")”獲得了公民證，並擁有了朝鮮永駐權。儘管蘇聯政府承認[中華民國政府合法](../Page/中華民國政府.md "wikilink")，但是-{zh-hans:朝鲜;zh-hant:北韓}-華僑受[中共中央東北局的管理](../Page/中共中央東北局.md "wikilink")，1949年10月1日，[毛澤東宣布建立](../Page/毛澤東.md "wikilink")[中華人民共和國之後](../Page/中華人民共和國.md "wikilink")，-{zh-hans:朝鲜;zh-hant:北韓}-華僑擁有[中華人民共和國國籍](../Page/中華人民共和國國籍.md "wikilink")，但並不擁有中華人民共和國境內戶籍。1950年代末，-{zh-hans:朝鲜;zh-hant:北韓}-華僑通过“朝鮮華僑聯合會”接受着中國當局的領導，享有自治權。華僑學校從-{zh-hans:朝鲜;zh-hant:北韓}-當局獨立出來，採用了中國人教師、中國教育方式和中國教材。-{zh-hans:朝鲜;zh-hant:北韓}-政府還向華僑提供了物質支援，並免費重建了[-{zh-cn:朝鲜战争;
zh-tw:韓戰; zh-hk:韓戰;
zh-sg:韩战;}-時期遭受破壞的華僑住宅等](../Page/朝鲜战争.md "wikilink")。1960年，-{zh-hans:朝鲜;zh-hant:北韓}-當局針對華僑開展了放棄中國國籍，加入-{zh-hans:朝鲜;zh-hant:北韓}-國籍的活動，1963年對華僑學校按朝鮮方式進行了改革，教育用語也改為朝鮮語。中國發生[文化大革命以後](../Page/文化大革命.md "wikilink")，-{zh-hans:朝鲜;zh-hant:北韓}-當局強迫華僑歸順朝鮮或者回到中國。1966年朝鮮發生“平壤中國人中學事件”，平壤中國人中學的學生們經常集體收聽毛澤東要求在革命過程中加強青年人力量等的演說，學校也向朝鮮當局要求把毛澤東思想加入到教育內容，但-{zh-hans:朝鲜;zh-hant:北韓}-當局拒絕要求并關閉了學校。周恩來訪問朝鮮之後，朝中關係恢复正常，-{zh-hans:朝鲜;zh-hant:北韓}-停止了不平等對待華僑的政策，並允許1960年代放棄中國國籍的華僑們恢復身份和國籍。1979年中國當局開展了支援華僑歸國的措施，很多-{zh-hans:朝鲜;zh-hant:北韓}-華僑回到了中國。1990年代-{zh-hans:朝鲜;zh-hant:北韓}-爆發[飢荒以後](../Page/朝鲜饥荒.md "wikilink")，由於有往返的自由，加之-{zh-hans:朝鲜;zh-hant:北韓}-市場對中國產品的需求增多，大多數華僑開展了非法的經濟活動，生活水平也隨之逐漸提高。\[2\]2014年-{zh-hans:朝鲜;zh-hant:北韓}-因為擔心體制受到威脅而針對往返於中國的華僑加強了監視和追蹤，\[3\]并且誘捕2名對-{zh-hans:朝鲜;zh-hant:北韓}-進行傳教活動的華僑。\[4\]2015年由於-{zh-hans:朝鲜;zh-hant:北韓}-獲得一些華僑與[國家安全保衛部有利益關係並對外泄露情報的情報](../Page/國家安全保衛部.md "wikilink")，100多名華僑被逮捕。\[5\]

## 知名朝鮮半島華人

（按照出生先後順序排列）

  - [郝明義](../Page/郝明義.md "wikilink")：[臺灣作家](../Page/臺灣.md "wikilink")、出版家，大塊文化董事長。
  - [初安民](../Page/初安民.md "wikilink")：臺灣詩人、編輯，《印刻文學生活誌》總編輯。
  - [姜育恆](../Page/姜育恒.md "wikilink")：臺灣歌手。
  - [畢書盡](../Page/畢書盡.md "wikilink")：臺灣歌手。
  - [孫盛希](../Page/孫盛希.md "wikilink")：臺灣歌手。
  - [黃仁德](../Page/黃仁德.md "wikilink")：臺灣演員。
  - [劉家昌](../Page/劉家昌.md "wikilink")：臺灣歌手、作曲家。
  - [譚道良](../Page/譚道良.md "wikilink")：臺灣武打動作演員。

## 参见

  - [中華TV (韓國)](../Page/中華TV_\(韓國\).md "wikilink")
  - [中國朝鮮族](../Page/中國朝鮮族.md "wikilink")
  - [朝鮮族](../Page/朝鮮族.md "wikilink")
  - [漢族](../Page/漢族.md "wikilink")
  - [海外華人](../Page/海外華人.md "wikilink")
  - [華人](../Page/華人.md "wikilink")

## 参考文献

### 引用

### 书籍

  - 《[丑陋的韩国人](../Page/丑陋的韩国人.md "wikilink")》，作者：[金文學](../Page/金文學.md "wikilink")

  -
### 刊物

  - 《[亚洲周刊](../Page/亚洲周刊.md "wikilink")》（1993年5月16日期第39页）

### 网页

  - [雅虎新闻：韩国开发商要建韩国最广大的中華街](http://au.news.yahoo.com/051010/3/p/wag5.html)
  - 《韓國在線》網：[韩国选举
    华人首获投票权](http://www.hanguo.net.cn/news/?mid=8&job=v&no=88&start=45)
  - 《[人民網](../Page/人民網.md "wikilink")》：[华人在韩国仍然受歧視](http://world.people.com.cn/GB/41219/4774564.html)
  - 《DPRK TIME:时代朝鲜网》 朝鮮華僑同鄉會 朝鮮華僑史
  - [韓國法務部出入境管理事務所](../Page/韓國法務部.md "wikilink")，（2004年6月）：[1](http://www.gdoverseaschn.com.cn/qw2index/2006dzkwlsfbq/200610100007.htm)
  - 中國僑網
    [韓國華人華僑概況](http://www.chinaqw.com/node2/node2796/node2882/node2893/userobject6ai238069.html)
  - 《[Daily
    NK](../Page/Daily_NK.md "wikilink")》：[朝鲜华侨的兴衰史](http://www.dailynk.com/chinese/read.php?cataId=nk00201&num=14681)

## 外部链接

  - [韓國華僑人權論壇 - 韓文為主 部分中文](http://cafe.naver.com/koreanchinese.cafe)

  - [韩国华人网](http://www.krchinese.com/)

  - [仁川中華街的照片](http://www.skyscrapercity.com/showthread.php?t=240474)

  - [仁川的中華街](http://www.lifeinkorea.com/Travel2/inchon/336)

{{-}}

[\*](../Category/朝鲜半岛华人.md "wikilink")
[Category:在朝鲜半岛的外国人](../Category/在朝鲜半岛的外国人.md "wikilink")

1.
2.

3.

4.

5.