**NGC
35**是[鲸鱼座的一個](../Page/鲸鱼座.md "wikilink")[漩涡星系](../Page/漩涡星系.md "wikilink")。[星等為](../Page/星等.md "wikilink")14.2，[赤經為](../Page/赤經.md "wikilink")11分10.4秒，[赤緯為](../Page/赤緯.md "wikilink")-12°1'14"。在1886年11月21日首次被發現。

## 參見

  - [NGC天體列表](../Page/NGC天體列表.md "wikilink")

## 參考資料

## 外部連結

  - [NGC 35](https://web.archive.org/web/20050921025125/http://www.seds.org/~spider/ngc/ngc_fr.cgi?35)

[Category:鲸鱼座NGC天体](../Category/鲸鱼座NGC天体.md "wikilink")