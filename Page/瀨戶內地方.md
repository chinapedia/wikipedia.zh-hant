[缩略图](https://zh.wikipedia.org/wiki/File:Okayama_City_Kounan_industrial_zone.JPG "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:MinamiBisanSetoOhashi.jpg "fig:缩略图")

**瀨戶内地方**
（）是[日本的地理區劃之一](../Page/日本地理區劃.md "wikilink")，包括[本州西部](../Page/本州.md "wikilink")、[四國及](../Page/四國.md "wikilink")[九州北部瀕臨](../Page/九州_\(日本\).md "wikilink")[瀨戶内海的地區](../Page/瀨戶内海.md "wikilink")。一般包括[山陽](../Page/山陽地方.md "wikilink")（[兵庫縣](../Page/兵庫縣.md "wikilink")[播州地方及](../Page/播磨國.md "wikilink")[岡山縣](../Page/岡山縣.md "wikilink")、[廣島縣](../Page/廣島縣.md "wikilink")、[山口縣](../Page/山口縣.md "wikilink")）、[四國北部的](../Page/四國.md "wikilink")[香川縣和](../Page/香川縣.md "wikilink")[愛媛縣](../Page/愛媛縣.md "wikilink")。另一概念相似的地理分區為「[中國、四國地方](../Page/中國、四國地方.md "wikilink")」。該地區屬于[瀨戶内式氣候](../Page/瀨戶内式氣候.md "wikilink")，氣候乾燥降雨稀少。

## 關聯條目

  - [瀨戶内海](../Page/瀨戶内海.md "wikilink")
  - [瀨戶大橋](../Page/瀨戶大橋.md "wikilink")
  - [瀨戶淡路鳴門自動車道](../Page/瀨戶淡路鳴門自動車道.md "wikilink")
  - [瀨戶内海式氣候](../Page/瀨戶内海式氣候.md "wikilink")
  - [瀨戶内工業地域](../Page/瀨戶内工業地域.md "wikilink")
  - [西瀨戶](../Page/西瀨戶.md "wikilink")
  - [瀨戶内市](../Page/瀨戶内市.md "wikilink")

[Category:日本地理區劃](../Category/日本地理區劃.md "wikilink")
[Category:瀨戶內海](../Category/瀨戶內海.md "wikilink")
[Category:兵庫縣地理](../Category/兵庫縣地理.md "wikilink")
[Category:岡山縣地理](../Category/岡山縣地理.md "wikilink")
[Category:廣島縣地理](../Category/廣島縣地理.md "wikilink")
[Category:山口縣地理](../Category/山口縣地理.md "wikilink")
[Category:愛媛縣地理](../Category/愛媛縣地理.md "wikilink")
[Category:香川縣地理](../Category/香川縣地理.md "wikilink")
[Category:德島縣地理](../Category/德島縣地理.md "wikilink")