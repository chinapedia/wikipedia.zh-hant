**二王八司马**，指[中国](../Page/中国.md "wikilink")[唐](../Page/唐朝.md "wikilink")[顺宗](../Page/唐顺宗.md "wikilink")[永貞元年](../Page/永貞.md "wikilink")（[805年](../Page/805年.md "wikilink")），領導[永贞革新的十位](../Page/永贞革新.md "wikilink")[士大夫](../Page/士大夫.md "wikilink")。「二王」為[王叔文與](../Page/王叔文.md "wikilink")[王伾](../Page/王伾.md "wikilink")；「八司馬」為[韋執誼](../Page/韋執誼.md "wikilink")、[韓泰](../Page/韓泰.md "wikilink")、[陳諫](../Page/陳諫.md "wikilink")、[柳宗元](../Page/柳宗元.md "wikilink")、[劉禹錫](../Page/劉禹錫.md "wikilink")、[韓曄](../Page/韓曄.md "wikilink")、[凌准](../Page/凌准.md "wikilink")、[程異等八人](../Page/程異.md "wikilink")。

永贞革新罷除[宮市](../Page/宮市.md "wikilink")、[五坊小兒等弊政](../Page/五坊小兒.md "wikilink")，又試圖收回宦官的兵權；損及[神策軍系統的](../Page/神策軍.md "wikilink")[俱文珍](../Page/俱文珍.md "wikilink")、[劉光琦等宦官利益](../Page/劉光琦.md "wikilink")。當時順宗[中風](../Page/中風.md "wikilink")，只有王叔文一黨的[牛昭容](../Page/牛昭容.md "wikilink")、[太監](../Page/太監.md "wikilink")[李忠言](../Page/李忠言.md "wikilink")、[王伾等可以傳遞旨意](../Page/王伾.md "wikilink")，文武百官都非常疑懼。王叔文為人武斷任性，王伾則[貪腐](../Page/貪腐.md "wikilink")，使得改革團隊更受到朝廷百官的敵視。

俱文珍、劉光琦聯合[劍南節度使](../Page/劍南節度使.md "wikilink")[韋皋](../Page/韋皋.md "wikilink")、[荊南節度使](../Page/荊南節度使.md "wikilink")[裴均](../Page/裴均.md "wikilink")、[河東節度使](../Page/河東節度使.md "wikilink")[嚴綬等](../Page/嚴綬.md "wikilink")[外藩迫使順宗立皇長子](../Page/外藩.md "wikilink")[李純為太子](../Page/李純.md "wikilink")，而後俱文珍又迫使順宗[禪讓](../Page/禪讓.md "wikilink")[帝位](../Page/帝位.md "wikilink")，是為[永貞內禪](../Page/永貞內禪.md "wikilink")。

太子即位，是為憲宗，此十人均貶遠地，是為**二王八司馬事件**，貶王叔文為[渝州](../Page/渝州.md "wikilink")（今[重慶](../Page/重慶.md "wikilink")[巴南](../Page/巴南.md "wikilink")）[司戶](../Page/司戶.md "wikilink")，隨即[賜死](../Page/賜死.md "wikilink")。王伾被貶為[開州](../Page/開州.md "wikilink")（今[四川](../Page/四川.md "wikilink")[開縣](../Page/開縣.md "wikilink")）[司馬](../Page/司馬.md "wikilink")，不久離奇病死。韋執誼等八人則被貶為邊遠八州[司馬](../Page/司馬.md "wikilink")，韦执谊不久即病死，程異以善於理財，復召為[鹽鐵使](../Page/鹽鐵使.md "wikilink")，其他六人俱南貶多年。

## 名單

### 二王

  - [王叔文](../Page/王叔文.md "wikilink")
  - [王伾](../Page/王伾.md "wikilink")

### 八司马

  - [韦执谊](../Page/韦执谊.md "wikilink")（被贬为[崖州](../Page/崖州.md "wikilink")[司马](../Page/司马.md "wikilink")，今[海南](../Page/海南.md "wikilink")[瓊山](../Page/瓊山.md "wikilink")）
  - [韩泰](../Page/韩泰.md "wikilink")（被贬为[虔州司马](../Page/虔州.md "wikilink")，今[江西](../Page/江西.md "wikilink")[贛州](../Page/贛州.md "wikilink")）
  - [陈谏](../Page/陈谏.md "wikilink")（被贬为[台州司马](../Page/台州.md "wikilink")，今[浙江](../Page/浙江.md "wikilink")[臨海](../Page/臨海.md "wikilink")）
  - [柳宗元](../Page/柳宗元.md "wikilink")（被贬为[永州司马](../Page/永州.md "wikilink")，今[湖南](../Page/湖南.md "wikilink")[永州](../Page/永州.md "wikilink")）
  - [刘禹锡](../Page/刘禹锡.md "wikilink")（被贬为[朗州司马](../Page/朗州.md "wikilink")，今[湖南](../Page/湖南.md "wikilink")[常德](../Page/常德.md "wikilink")）
  - [韩晔](../Page/韩晔.md "wikilink")（被贬为[饶州司马](../Page/饶州.md "wikilink")，今[江西](../Page/江西.md "wikilink")[上饒](../Page/上饒.md "wikilink")）
  - [凌准](../Page/凌准.md "wikilink")（被贬为[连州司马](../Page/连州.md "wikilink")，今[廣東](../Page/廣東.md "wikilink")[連州](../Page/連州.md "wikilink")）
  - [程异](../Page/程异.md "wikilink")（被贬为[郴州司马](../Page/郴州.md "wikilink")，今[湖南](../Page/湖南.md "wikilink")[郴州](../Page/郴州.md "wikilink")）

[Category:唐顺宗](../Category/唐顺宗.md "wikilink")
[Category:唐朝中央官员](../Category/唐朝中央官员.md "wikilink")
[Category:唐朝政治人物并称](../Category/唐朝政治人物并称.md "wikilink")