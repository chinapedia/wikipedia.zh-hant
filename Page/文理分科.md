**文理分科**是在部分[東亞国家和地区实行的教育制度](../Page/東亞.md "wikilink")，即将教学课程分为[文科和](../Page/文科.md "wikilink")[理科让学生选择后进行分别教育](../Page/理科.md "wikilink")。

## 中國大陆

文理分科在中國始于1977年的恢复[高考](../Page/高考.md "wikilink")。但事实上这种制度源于对[苏联模式的照搬](../Page/苏联.md "wikilink")。[二战使](../Page/二战.md "wikilink")[苏联损失了大批](../Page/苏联.md "wikilink")[知识分子](../Page/知识分子.md "wikilink")，因此战后重建急需以分科教育来快速培养各类专业人才。

中國1949年后，同样急需专业人才，因此开始了文理分化的步伐，首先成立了[西安](../Page/西安.md "wikilink")、[南京](../Page/南京.md "wikilink")、[重庆等最早的](../Page/重庆.md "wikilink")8所外语学校，并在50年代开始了对[高校的合并](../Page/高校.md "wikilink")。当时实现[工业化是经济建设的主要目标](../Page/工业化.md "wikilink")，为了适应对专业人才的需求，自1952年起，将同类专业院校合并为专业学院，建起适应[计划经济体制的高校体制](../Page/计划经济.md "wikilink")。这样的合并加强了[工科院校的专业性](../Page/工科院校.md "wikilink")，但也造成专业面窄，且理工、文理分家，文法、财经等学科也被削减。

但在[文革结束前](../Page/文革.md "wikilink")，中國的[高中一般都没有实行文理分科](../Page/高中.md "wikilink")。1977年，恢复[高考制後](../Page/高考.md "wikilink")，为分类选拔人才，[高中文理分科才开始](../Page/高中.md "wikilink")。

分科一般是在高一进入高二阶段进行的。虽然实行文理分科，但是不管文、理都包括所分的科目为[政治](../Page/政治.md "wikilink")、[历史](../Page/历史.md "wikilink")、[地理](../Page/地理.md "wikilink")、[物理](../Page/物理.md "wikilink")、[化学](../Page/化学.md "wikilink")、[生物](../Page/生物.md "wikilink")。

  - 必修科：[语文](../Page/语文.md "wikilink")、[数学](../Page/数学.md "wikilink")、[外语](../Page/外语.md "wikilink")（[英语](../Page/英语.md "wikilink")、[俄语或](../Page/俄语.md "wikilink")[日语](../Page/日语.md "wikilink")，近年亦加入[西班牙语及](../Page/西班牙语.md "wikilink")[韩语](../Page/韩语.md "wikilink")）
  - 文科綜合：[政治](../Page/政治.md "wikilink")、[历史](../Page/历史.md "wikilink")、[地理](../Page/地理.md "wikilink")
  - 理科綜合：[物理](../Page/物理.md "wikilink")、[化学](../Page/化学.md "wikilink")、[生物](../Page/生物.md "wikilink")

但现行的制度要求文、理科仍保留其他科目教学。通常在高二开始的文理分科后，学生会将重点放在文综或理综上，实际的教学上老师也不太重视，而大部分的学生也轻视这些科目，随着学业水平测试的结束，高三时学生会全力攻克高考科目。而如果在会考或其它的检查之后学校就直接将这些科目从[课程表上删除](../Page/课程表.md "wikilink")。

文科、理科的数学和文理综教材上也有所区别。数学文科一般比理科要简单，在教学和考试都有很大差异。

现前各省的高考制度略有不同，某些地区增加学业水平测试作为参加考试的判定，或有综合能力测试。目前江苏省考生可在文理综中选考2门课，单科考试的难度会比文理综考试高。

对于学生选文还是选理，学生乃至家长、老师都有一番艰难的抉择。当然有人对分科持反对态度，但面对高考的指挥仍须作出决定，不是选文就是选理。总体而言选理的人数要多于选文，在一般的学校这个比例是3：1。影响学生作出选择的因素有和很多，主要包括：某个学生是擅长文还是理、未来就业形势（这主要是受家长及老师的影响）、个人的兴趣、爱好、及理想。但有时候其它因素也会影响到一个学生的选择；老师的实力对比，如果某科教师的实力强劲，则易吸引学生选择此科。

2017年开始，上海、浙江的考生不再进行文理分科，选课增加至三门。

## 香港

### 新高中學制 （2009年-）

  - 取消會考及高考，由[香港中學文憑考試取代](../Page/香港中學文憑考試.md "wikilink")。
  - 中英數為必修（包括非華裔學生）；而且所有學生必須修讀[通識教育科](../Page/通識教育.md "wikilink")，務求達到「文中有理、理中有文」。
  - 不再強制文理分科，學生可以選擇文理兼備的科目；但有些學校提供給學生的選擇組別仍是類似以往的文理分科
  - [附加數學](../Page/附加數學.md "wikilink")、純數學、應用數學、數學與統計學課程改為[數學科延伸部分](../Page/數學科延伸部分.md "wikilink")。

以往，香港大部份中學於中四（即高中）開始分科；過去[文法中學主要分為文](../Page/文法中學.md "wikilink")（文商）、理兩科，部份學校則分為文、理、[商三組](../Page/商科.md "wikilink")，[工業學校另以](../Page/工業學校.md "wikilink")[工科代替部份理科](../Page/工科.md "wikilink")。
各校修讀的科目會略有差異，因為並非所有學校都有開辦全部科目，部份學校亦容許學生選修不同組別的科目。

### 舊高中學制常見科目（2009年前）

  - [香港中學會考](../Page/香港中學會考.md "wikilink")（中四、中五課程；通常報考七至八科，最多十科）
      - 必修科目：[中文](../Page/中文.md "wikilink")\[1\]、[英文](../Page/英文.md "wikilink")、[數學](../Page/數學.md "wikilink")
      - [文科](../Page/文.md "wikilink")：[中國歷史](../Page/中國歷史.md "wikilink")、[歷史](../Page/歷史.md "wikilink")\[2\]、[地理](../Page/地理.md "wikilink")、[經濟](../Page/經濟.md "wikilink")、[中國文學](../Page/中國文學.md "wikilink")、[英語文學](../Page/英語文學.md "wikilink")、「人類[生物學](../Page/生物學.md "wikilink")(Human
        Biology)」（選考人類生物學多以獲取報考護士學校的資格）
      - [理科](../Page/理.md "wikilink")：[物理](../Page/物理.md "wikilink")、[化學](../Page/化學.md "wikilink")、[生物](../Page/生物.md "wikilink")、[附加數學](../Page/附加數學.md "wikilink")、「[電腦與](../Page/電腦.md "wikilink")[資訊科技](../Page/資訊科技.md "wikilink")」
      - [商科](../Page/商.md "wikilink")：[會計學原理](../Page/會計學.md "wikilink")、[商業](../Page/商學.md "wikilink")、[經濟](../Page/經濟學.md "wikilink")、英文[文書處理](../Page/文書處理.md "wikilink")、商業通訊\[3\]
      - [工科](../Page/工.md "wikilink")：「電子與電學」\[4\]或「設計與科技」\[5\]或「[電腦與](../Page/電腦.md "wikilink")[資訊科技](../Page/資訊科技.md "wikilink")」\[6\]、[工業繪圖](../Page/工業繪圖.md "wikilink")、工程科學、化學、生物、附加數學
      - 此外還可選修佛學、[宗教](../Page/宗教.md "wikilink")\[7\]、[普通話](../Page/普通話.md "wikilink")，[視覺藝術](../Page/視覺藝術.md "wikilink")、[體育](../Page/體育.md "wikilink")（絕大多數學生不會報考會考體育科）等科目
      - [體藝中學](../Page/賽馬會體藝中學.md "wikilink")：除了普遍文法中學的課程，學生需選擇修讀及報考[體育或](../Page/體育.md "wikilink")[視覺藝術其中一科](../Page/視覺藝術.md "wikilink")。

<!-- end list -->

  - [香港高級程度會考](../Page/香港高級程度會考.md "wikilink")（中六、中七課程［[預科](../Page/預科.md "wikilink")］）
      - 必修科目：[中國語文及文化](../Page/中國語文及文化科.md "wikilink")\[8\]、[英語運用](../Page/英語運用科.md "wikilink")
      - 文科：中國歷史、歷史、地理、中國文學、經濟、數學與統計學、音樂
      - 理科
          - 「數組」：物理、化學、[純粹數學](../Page/純粹數學.md "wikilink")、[應用數學](../Page/應用數學.md "wikilink")、電腦學
            或 電腦應用
          - 「生物組」：物理、化學、生物、數學與統計學
      - 商科：會計學原理、企業概論、經濟、電腦應用
      - 工科：電子與電學（AS-Level\[9\]，並非所有學校開辦）、設計與科技、工程科學等，其餘通常改為理科課程
      - [體育](../Page/體育.md "wikilink")（絕大多數學生不會報考高考體育科）

## 中華民國文理分科

從高中二年級開始分組。文、理都必修國文、英文、數學（文組與理組的數學在高二時分流，有A、B版，並於高三時分為自然組的「數學甲」，與社會組的「數學乙」），直至高三結束；歷史、地理、公民與社會等三科，文、理組至二年級皆為必修，高三文組會繼續修習；自然科之物理（有分流A、B版，理組上後者）、化學、生物、地球科學，文、理組高一皆為必修，高二視各校配課時數，於四科設定不同權重給文、理組，高三理組會繼續修習。

過去[聯考時代](../Page/大學聯合招生考試.md "wikilink")，文、理分為甲組（理工）、乙組（文哲）、丙組（農醫）、丁組（法商）共四組，其中甲組、丙組為自然組；乙組、丁組為社會組。

目前則分為三類組：

  - **第一類組**（簡稱一類）：社會組/**文組**，主修課程有國文、英文、數學（乙）、歷史、地理、公民與社會，物理、化學、生物、地球科學則僅在二年級必修或選修，通常在[指定科目考試中應考國文](../Page/大學入學指定科目考試.md "wikilink")、英文、數學乙、歷史、地理、公民與社會。大學可選擇[文](../Page/文學.md "wikilink")、[史](../Page/歷史學.md "wikilink")、[哲](../Page/哲學.md "wikilink")、[政](../Page/政治學.md "wikilink")、[法](../Page/法學.md "wikilink")、[商](../Page/商學.md "wikilink")、[傳播](../Page/傳播學.md "wikilink")、[教育](../Page/教育學.md "wikilink")、[藝術](../Page/藝術.md "wikilink")、[外語](../Page/外語.md "wikilink")、[社會科學等學系](../Page/社會科學.md "wikilink")。

<!-- end list -->

  - **第二類組**（簡稱二類）：自然組/**理組**，主修科目有國文、英文、數學（甲）、物理、化學，生物、地球科學、歷史、地理、公民與社會則僅在二年級必修，通常在指定科目考試應考國文、英文、數學甲、物理以及化學。二類與三類的主要差異為不著墨於生物，因此大學以[理學院](../Page/理學.md "wikilink")、[工學院為主要學系](../Page/工學.md "wikilink")。

<!-- end list -->

  - **第三類組**（簡稱三類）：自然組/**理組**，主修科目有國文、英文、數學（甲）、物理、化學、生物，地球科學、歷史、地理、公民與社會則僅在二年級必修，通常在指定科目考試應考國文、英文、數學甲、物理、化學以及生物。大學可選擇[醫學院](../Page/醫學.md "wikilink")、[生物學](../Page/生物學.md "wikilink")、[農學院](../Page/農學.md "wikilink")、[獸醫學等學系](../Page/獸醫學.md "wikilink")。

部分學校僅區分為社會組與自然組，不區分二類與三類。

目前每個類組都必須參加[學科能力測驗](../Page/大學學科能力測驗.md "wikilink")，且考試範圍相同。指定科目考試並未限制應考身分，因此類組可互相跨考，但由於商學院近來趨向只採計文、理組都有的國文、英文、數學，而自然組學生修習的版本較為高深，使得自然組學生較能跨考社會組科目，也較易拿到高分。2009年起列公民與社會為指定科目考試的考科之一。

## 選組的選擇

对于学生选組，学生乃至家长、老师都有一番艰难的抉择。当然有人对分組持反对态度，但面对將來升讀大學的選系，仍须作出决定。

影响学生作出选择的因素有和很多，主要包括：個人專長、未来就业形势、个人的[兴趣和](../Page/兴趣.md "wikilink")[理想](../Page/理想.md "wikilink")、家長的期望等。

但有时候其它因素也会影响到一个学生的选择，如成绩较差的学生更多会被建议选擇文組；老师的实力对比，如果某科教师的实力强劲，则易吸引学生选择此組。

## 争议

目前文理分科与[应试教育一样存在着大争议](../Page/应试教育.md "wikilink")。

### 反对者观点

有部分[教育家](../Page/教育家.md "wikilink")、[学者及教师](../Page/学者.md "wikilink")、家长反对文理分科。他们认为文理分科让理科学生不再学习历史、地理，更少阅读[文学经典等](../Page/文学经典.md "wikilink")，使理科生知识面偏窄，致使缺乏[人文精神](../Page/人文精神.md "wikilink")；而同时这也导致文科生对物理、化学、生物陌生，不知道基本的[自然科学知识](../Page/自然科学.md "wikilink")，导致缺乏[科学精神和](../Page/科学精神.md "wikilink")[理性](../Page/理性.md "wikilink")，也不懂得用已證明行之有效的[科学方法](../Page/科学方法.md "wikilink")[解決問題](../Page/解決問題.md "wikilink")，阻碍学生全面发展和提升[综合素质](../Page/素質教育.md "wikilink")。

### 支持者观点

也有部分的学生、家长和老师及其他人士认为文理分科有利学生个性发展，让他们自由选择所要学的科目，使学生学有专长。同时文理分科可以减轻学习负担、压力，尤其面对大學入學試，学生更可花充分时间在要考的科目上。
高中文理分科实际上解除了相当多学生的厌学感，让他们重新认识了学习之必要，进而“术业有专攻”。正是对文科与理科的正确认识，有学生很早就放弃了自己不想学又学不好的科目。如果强制性地不分科，真不知道有多少人知难而退，中辍学业。

## 改革

由于文理分科存在着弊端，因此部分人士提出改革方案或直接取消文理分科，支持改革者一般倾向认为应将文理分科推迟进行，不宜在高二，而是在高三总复习时期，由学生自由地作出选择。复习阶段选择文科的学生主攻文科，而理科生则主攻理科。他们这样可以较充分地保证学生综合素质的发展。但是，也有部分人士认为，推迟分科将进一步加大学生课业负担。

也有部分人士呼籲取消文理分科，在2005年，中國大陆[政协委员](../Page/政协.md "wikilink")[朱永新提出应组织专家进行取消高中](../Page/朱永新.md "wikilink")、高考文理分科的论证，争取从2006年高中入学和2008年高考取消文理分科。目前，中国大陆仍在實行文理分科制度。

### 香港

於2009年開始推行[新高中學制](../Page/三三四高中教育改革.md "wikilink")，[通識教育為必修科之一](../Page/通識教育.md "wikilink")，並鼓勵學校以彈性的選科組合和時間表取代傳統的文理分科制度。

## 相关链接

  - [应试教育](../Page/应试教育.md "wikilink")
  - [素質教育](../Page/素質教育.md "wikilink")
  - [香港三三四高中教育改革](../Page/三三四高中教育改革.md "wikilink")
  - [台灣教育改革](../Page/台灣教育改革.md "wikilink")

## 参考資料

<div class="references-small">

<references />

</div>

[Category:教育學](../Category/教育學.md "wikilink")

1.  非華裔學生可選修其他外語，例如[法文](../Page/法文.md "wikilink")[1](http://hk.apple.nextmedia.com/news/art/20070319/6914419)

2.  世界歷史，俗稱「世史」或「西史」

3.
4.  <http://www.hkeaa.edu.hk/tc/recognition/develop_hk_pub_exam/hkcee/sub_syllb/>

5.  前身為「設計與工藝」，再前身為「金工」；現時新高中學課程試亦設同名科目

6.  前身為「電腦學」

7.  有關[聖經的課程](../Page/聖經.md "wikilink")

8.  1992年開始新增

9.  <http://www.hkeaa.edu.hk/tc/recognition/develop_hk_pub_exam/hkale/sub_syllb/>