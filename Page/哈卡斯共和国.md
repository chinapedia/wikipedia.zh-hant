**哈卡斯共和国**（；[哈卡斯語](../Page/哈卡斯語.md "wikilink")：\[1\]或（）是[俄羅斯聯邦的一個](../Page/俄羅斯聯邦.md "wikilink")[聯邦主體](../Page/俄羅斯聯邦主體.md "wikilink")，位於[西伯利亞中南部](../Page/西伯利亞.md "wikilink")。首府[阿巴坎亦是共和國內最大城市](../Page/阿巴坎.md "wikilink")，有人口532,403人（[2010年俄羅斯人口統計](../Page/:en:Russian_Census_\(2010\).md "wikilink")）\[2\]。[哈卡斯語與俄語在當地具有法定地位](../Page/哈卡斯語.md "wikilink")。

俄联邦最大的[萨扬舒申斯克水电站在该共和国境内](../Page/萨扬舒申斯克水电站.md "wikilink")。

## 歷史

從6世紀開始，哈卡斯就位處於古代[吉爾吉斯民族的核心](../Page/吉爾吉斯人.md "wikilink")。在13世紀被[蒙古帝國襲擊之後](../Page/蒙古帝國.md "wikilink")，吉爾吉斯人的主體往西南移居到今日他們在[中亞的聚居地](../Page/中亞.md "wikilink")（[吉爾吉斯斯坦](../Page/吉爾吉斯斯坦.md "wikilink")）。今日的吉爾吉斯人認為他們是滯留在[西伯利亞的古化吉爾吉斯人的後人](../Page/西伯利亞.md "wikilink")。

1707年，哈卡斯被[俄羅斯帝國吞併](../Page/俄羅斯帝國.md "wikilink")。1727年，清國與沙俄確立邊界條約，沙俄開始把西伯利亞用作流放[歐裔俄國人的地方](../Page/歐裔俄國人.md "wikilink")。在這段期間，沙俄在哈卡斯當地快速建立了不少堡壘。流放犯人就在流放地定居，漸漸把當地游牧的哈卡斯人同化，令他們開始定居，並改信[俄羅斯東正教](../Page/俄羅斯東正教.md "wikilink")。

[1917年俄国革命發生之時](../Page/1917年俄国革命.md "wikilink")，[俄羅斯人佔當地人口接近一半](../Page/俄羅斯人.md "wikilink")。革命後的1920年代至1930年代，[蘇聯政府把](../Page/蘇聯.md "wikilink")25萬俄羅斯人遷徙至當地。當地於1930年10月10日獲得自治權，成為俄羅斯聯邦的一個[自治州](../Page/自治州.md "wikilink")，1934年併入[克拉斯诺亚斯克边疆区](../Page/克拉斯诺亚斯克边疆区.md "wikilink")。[第二次世界大戰爆發期間](../Page/第二次世界大戰.md "wikilink")，有一萬名[伏尔加德意志人被流放到這裡](../Page/伏尔加德意志人.md "wikilink")。到1959年進行人口統計時，哈卡斯人在人口比例中已減少至不足總人口的十分之一。1991年成為[俄羅斯聯邦主體之一](../Page/俄羅斯聯邦主體.md "wikilink")，並且升格成為共和國。

## 行政區劃

[Khakassia_republic_map_zh-tw.png](https://zh.wikipedia.org/wiki/File:Khakassia_republic_map_zh-tw.png "fig:Khakassia_republic_map_zh-tw.png")

哈卡斯的土地面积约6．19万平方公里，占全俄总而积的0．4％。有阿尔泰斯基、阿斯基兹、别亚、博格勒、奥尔忠尼启则、塔什特普、乌斯季阿巴坎、希拉8个行政区和阿巴坎(16．79万人)、萨彦诺戈尔斯克(5．58万人)、切尔诺戈尔斯克(7．93万人)、阿巴扎和索尔斯克5座城市(3个共和国直辖市)、13个镇和75个村。共和国首府是阿巴坎市，距克拉斯诺亚尔斯克市643公里，距莫斯科4218公里

## 參看

  - [阿巴坎遗址](../Page/阿巴坎遗址.md "wikilink")
  - [哈卡斯音樂](../Page/哈卡斯音樂.md "wikilink")

## 參考資料

  -
## 註釋

<references group="註"/>

## 外部連結

  -
  - [Khakassia live
    cam](https://archive.is/20110706085151/http://i.khak.asia/cam)

[Category:西伯利亚联邦管区](../Category/西伯利亚联邦管区.md "wikilink")
[哈卡斯共和國](../Category/哈卡斯共和國.md "wikilink")
[Category:突厥民族](../Category/突厥民族.md "wikilink")

1.  根據共和國政府的官方文件，即：。
2.