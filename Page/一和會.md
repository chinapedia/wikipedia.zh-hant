[Ichiwa-kai.png](https://zh.wikipedia.org/wiki/File:Ichiwa-kai.png "fig:Ichiwa-kai.png")
**一和會**（）、位於[兵庫縣](../Page/兵庫縣.md "wikilink")[神戶市](../Page/神戶市.md "wikilink")[中央區北長狭通](../Page/中央區_\(神戶市\).md "wikilink")3-2-3本部的設置，[日本的](../Page/日本.md "wikilink")[指定暴力團](../Page/指定暴力團.md "wikilink")・原第三代[山口組內](../Page/山口組.md "wikilink")[山廣派的分裂組成](../Page/山廣派.md "wikilink")、1989年解散。正式成員數在1984年約6000人、1985年初
約2800人、到後期的1989年初 約20人。

## 略年表

  - 1984年6月5日
    原總家若頭・[竹中正久](../Page/竹中正久.md "wikilink")（[竹中組組長](../Page/竹中組.md "wikilink")）就任第四代[山口組組長](../Page/山口組.md "wikilink")。同組、組長代行・[山本
    廣](../Page/山本廣.md "wikilink")（[山廣組組長](../Page/山廣組.md "wikilink")）為首的山廣派召開記者會表明反對。
  - 1984年6月13日 山廣派脫離山口組，另組成「一和會」與山口組對抗。
  - 1984年12月[伊原金一領導的](../Page/伊原金一.md "wikilink")[伊原組解散](../Page/伊原組.md "wikilink")。
  - 1985年1月26日
    旗下的暗殺部隊（出田邊豐紀、立花和夫、長尾宜美、長野修一）在[大阪](../Page/大阪.md "wikilink")[吹田槍殺第四代山口組組長](../Page/吹田.md "wikilink")・竹中正久，及同行的若頭・[中山勝正](../Page/中山勝正.md "wikilink")（[豪友會會長](../Page/豪友會.md "wikilink")）與若眾・[南
    力](../Page/南力.md "wikilink")（[南組組長](../Page/南組.md "wikilink")）。隔日
    竹中不治身亡。同年2月5日，山口組在新任代理組長・[中西一男](../Page/中西一男.md "wikilink")（[中西組組長](../Page/中西組.md "wikilink")）及副組長・[渡邊芳則](../Page/渡邊芳則.md "wikilink")（第二代[山健組組長](../Page/山健組.md "wikilink")）的帶領下，展開全面報復。「山一抗爭」就此爆發。
  - 1985年5月29日 山本
    廣的自家住宅遭山口組[後藤組](../Page/後藤組.md "wikilink")（組長・[後藤忠政](../Page/後藤忠政.md "wikilink")）、[美尾組](../Page/美尾組.md "wikilink")（組長・[美尾尚利](../Page/美尾尚利.md "wikilink")）的混成部隊襲擊。
  - 1985年10月27日 幹事長補佐・赤坂
    進（[赤坂組組長](../Page/赤坂組.md "wikilink")）在[鳥取縣](../Page/鳥取縣.md "wikilink")[倉吉遭](../Page/倉吉.md "wikilink")[竹中組](../Page/竹中組.md "wikilink")[杉本組系](../Page/杉本組.md "wikilink")（組長・杉本明政）的組員槍殺。
  - 1986年2月27日
    旗下[加茂田組第二代](../Page/加茂田組.md "wikilink")[花田組](../Page/花田組.md "wikilink")（組長・丹羽勝治）的組員在[兵庫縣](../Page/兵庫縣.md "wikilink")[姫路竹中正久的墓前遭山口組](../Page/姫路.md "wikilink")[竹中組](../Page/竹中組.md "wikilink")[柴田會](../Page/柴田會.md "wikilink")（會長・[柴田健吾](../Page/柴田健吾.md "wikilink")）的組員槍殺。
  - 1986年5月21日
    副本部長・中川宜治（[中川連合會會長](../Page/中川連合會.md "wikilink")）遭山口組[竹中組](../Page/竹中組.md "wikilink")[生島組](../Page/生島組.md "wikilink")（組長・生島仁吉）的幹部槍殺。
  - 1986年6月 溝橋正夫（[溝橋組組長](../Page/溝橋組.md "wikilink")）引退。
  - 1987年2月 常任顧問・白神英雄（[白神組組長](../Page/白神組.md "wikilink")）遭殺害。
  - 1988年4月11日
    第二代[花田組組長](../Page/花田組.md "wikilink")・丹羽勝治在[北海道](../Page/北海道.md "wikilink")[札幌市遭山口組](../Page/札幌市.md "wikilink")[弘道會系](../Page/弘道會.md "wikilink")（會長・[司
    忍](../Page/司忍.md "wikilink")）的組員槍殺。
  - 1988年5月8日 加茂田重政退出、[加茂田組亦解散](../Page/加茂田組.md "wikilink")。
  - 1988年5月14日 山本
    廣位於[神戶的自家住宅遭山口組](../Page/神戶.md "wikilink")[竹中組系](../Page/竹中組.md "wikilink")（組長・[竹中
    武](../Page/竹中武.md "wikilink")）的2名組員，駕駛裝滿炸藥的貨車意圖襲擊失敗。
  - 1988年5月21日 松本勝美（[松美會會長](../Page/松美會.md "wikilink")）引退（之後被破門）。
  - 1988年6月10日 福野 隆（[福野組組長](../Page/福野組.md "wikilink")）退出。
  - 1988年7月15日 井志繁雅（[井志組組長](../Page/井志組.md "wikilink")）、大川
    覺（[大川組組長](../Page/大川組.md "wikilink")）、坂井奈良芳（[坂井組組長](../Page/坂井組.md "wikilink")）、北山
    悟（[北山組組長](../Page/北山組.md "wikilink")）、松尾三郎（[松尾組組長](../Page/松尾組.md "wikilink")）、宮脇與一（[宮脇組組長](../Page/宮脇組.md "wikilink")）、清水光廣（第二代[清水組組長](../Page/清水組.md "wikilink")）、吉田好延（[吉田會會長](../Page/吉田會.md "wikilink")）、淺野二郎（[淺野組組長](../Page/淺野組.md "wikilink")）、松本博幸（第二代[松本組組長](../Page/松本組.md "wikilink")）、河内山憲法（[河内山組組長](../Page/河内山組.md "wikilink")）、宮本一利（第五代[水谷一家總長](../Page/水谷一家.md "wikilink")）等人一同退出一和會。
  - 1988年10月 中井啓一（[中井組組長](../Page/中井組.md "wikilink")）退出，成為獨立的團體。
  - 1988年10月4日[加茂田俊治退出](../Page/加茂田俊治.md "wikilink")，[神籠會解散](../Page/神籠會.md "wikilink")。
  - 1989年在獄中服刑的佐佐木 將城宣布退出、同時佐佐木組解散。
  - 1989年3月19日 山本 廣宣布一和會解散。
  - 1989年3月30日 山本
    廣在[稻川會本部長](../Page/稻川會.md "wikilink")・[稻川裕紘的協調及陪同下](../Page/稻川裕紘.md "wikilink")，至山口組本家登門拜訪謝罪。
  - 1993年8月27日 山本 廣在[神戶市的醫院去世](../Page/神戶市.md "wikilink")。

## 最高幹部

  - 會長・[山本
    廣](../Page/山本廣.md "wikilink")（初代[山廣組組長](../Page/山廣組.md "wikilink")、原第三代[山口組組長代行](../Page/山口組.md "wikilink")）
  - 最高顧問・[中井啓一](../Page/中井啓一.md "wikilink")（[中井組組長](../Page/中井組.md "wikilink")、原第三代山口組舍弟）
  - 副會長兼理事長・[加茂田重政](../Page/加茂田重政.md "wikilink")（[加茂田組組長](../Page/加茂田組.md "wikilink")、原第三代山口組組長代行補佐）
  - 常任顧問・[溝橋正夫](../Page/溝橋正夫.md "wikilink")（[溝橋組組長](../Page/溝橋組.md "wikilink")）
  - 常任顧問・[白神英雄](../Page/白神英雄.md "wikilink")（白神一朝。[白神組組長](../Page/白神組.md "wikilink")）
  - 幹事長・[佐々木將城](../Page/佐々木將城.md "wikilink")（佐々木道雄。[佐々木組組長](../Page/佐々木組_\(一和會\).md "wikilink")）
  - 特別相談役・[井志繁雅](../Page/井志繁雅.md "wikilink")（井志繁雄。[井志組組長](../Page/井志組.md "wikilink")）
  - 特別相談役・[大川
    覺](../Page/大川覺.md "wikilink")（[大川組組長](../Page/大川組.md "wikilink")）
  - 特別相談役・[坂井奈良芳](../Page/坂井奈良芳.md "wikilink")（[坂井組組長](../Page/坂井組.md "wikilink")）
  - 本部長・[松本勝美](../Page/松本勝美.md "wikilink")（神村勝美。[松美會會長](../Page/松美會.md "wikilink")）
  - 組織委員長・[北山
    悟](../Page/北山悟.md "wikilink")（[北山組組長](../Page/北山組.md "wikilink")）
  - 風紀委員長・[松尾三郎](../Page/松尾三郎.md "wikilink")（[松尾組組長](../Page/松尾組.md "wikilink")）
  - 專務理事・[宮脇與一](../Page/宮脇與一.md "wikilink")（[宮脇組組長](../Page/宮脇組.md "wikilink")）
  - 副組織委員長・[福野
    隆](../Page/福野隆.md "wikilink")（[福野組組長](../Page/福野組.md "wikilink")）
  - 副幹事長・[伊原金一](../Page/伊原金一.md "wikilink")（尹
    炯騏。[伊原組組長](../Page/伊原組.md "wikilink")）
  - 副幹事長・[清水光廣](../Page/清水光廣.md "wikilink")（第二代[清水組組長](../Page/清水組.md "wikilink")）
  - 副幹事長・[吉田好延](../Page/吉田好延.md "wikilink")（吉田義信。[吉田會會長](../Page/吉田會.md "wikilink")）
  - 事務局長・[淺野二郎](../Page/淺野二郎.md "wikilink")（[淺野組組長](../Page/淺野組_\(一和會\).md "wikilink")）
  - 理事長補佐・[加茂田俊治](../Page/加茂田俊治.md "wikilink")（[神龍會會長](../Page/神龍會.md "wikilink")）
  - 理事長補佐・[松本博幸](../Page/松本博幸.md "wikilink")（第二代[松本組組長](../Page/松本組_\(一和會\).md "wikilink")）
  - 副本部長・[河内山憲法](../Page/河内山憲法.md "wikilink")（河内山正義。[河内山組組長](../Page/河内山組.md "wikilink")）
  - 事務局次長・[宮本一利](../Page/宮本一利.md "wikilink")（五代目[水谷一家總長](../Page/水谷一家.md "wikilink")）
  - 常任理事・[東 健二](../Page/東健二.md "wikilink")（新家
    隆。二代目[山廣組組長](../Page/山廣組.md "wikilink")）

幹事長補佐18人

  - [乾勝彦](../Page/乾勝彦.md "wikilink")（二代目[山廣組舍弟頭](../Page/山廣組.md "wikilink")）
  - [弘田憲二](../Page/弘田憲二.md "wikilink")（[中井組若頭](../Page/中井組.md "wikilink")、[弘田組組長](../Page/弘田組.md "wikilink")）
  - [藤原照久](../Page/藤原照久.md "wikilink")（[溝橋組若頭](../Page/溝橋組.md "wikilink")、[藤原組組長](../Page/藤原組.md "wikilink")）
  - [木村弘](../Page/木村弘.md "wikilink")（[加茂田組舍弟頭](../Page/加茂田組.md "wikilink")、[木村組組長](../Page/木村.md "wikilink")）
  - [雲井武人](../Page/雲井武人.md "wikilink")（[佐々木組若頭](../Page/佐々木組.md "wikilink")、[雲井組組長](../Page/雲井組.md "wikilink")）
  - [石川裕雄](../Page/石川裕雄.md "wikilink")（[悟道連合會會長](../Page/悟道連合會.md "wikilink")）
  - [松岡正雄](../Page/松岡正雄.md "wikilink")（[正風會會長](../Page/正風會.md "wikilink")）
  - [池中幸一](../Page/池中幸一.md "wikilink")（[井志組相談役](../Page/井志組.md "wikilink")）
  - [柿本二三夫](../Page/柿本二三夫.md "wikilink")（[大川組](../Page/大川組.md "wikilink")）
  - [金澤吉雄](../Page/金澤吉雄.md "wikilink")（[金澤組組長](../Page/金澤組.md "wikilink")）
  - [首沢元人](../Page/首沢元人.md "wikilink")（[宮脇組若頭](../Page/宮脇組.md "wikilink")、[首龍會會長](../Page/首龍會.md "wikilink")）
  - [馬場武夫](../Page/馬場武夫.md "wikilink")（[馬場組組長](../Page/馬場組.md "wikilink")）
  - [中川宜治](../Page/中川宜治.md "wikilink")（[中川連合會會長](../Page/中川連合會.md "wikilink")）
  - [橋本勝義](../Page/橋本勝義.md "wikilink")（二代目[松本組](../Page/松本組.md "wikilink")、[橋本組組長](../Page/橋本組.md "wikilink")）
  - [西來功](../Page/西來功.md "wikilink")（二代目[清水組舎弟頭](../Page/清水組.md "wikilink")、[八生會會長](../Page/八生會.md "wikilink")）
  - [上田隆司](../Page/上田隆司.md "wikilink")（[河内山組舍弟頭](../Page/河内山組.md "wikilink")）
  - [岡本勇](../Page/岡本勇.md "wikilink")（[伊原組](../Page/伊原組.md "wikilink")、[岡本組組長](../Page/岡本組.md "wikilink")）
  - [園田和義](../Page/園田和義.md "wikilink")（[浅野組舍弟](../Page/浅野組.md "wikilink")）

其他

  - [村上幸二](../Page/村上幸二.md "wikilink")（[村上組組長](../Page/村上組.md "wikilink")）
  - [末次正宏](../Page/末次正宏.md "wikilink")（[末次組組長](../Page/末次組.md "wikilink")）
  - [澤井敏雄](../Page/澤井敏雄.md "wikilink")（[澤井組組長](../Page/澤井組.md "wikilink")）

[Category:近畿地區暴力團](../Category/近畿地區暴力團.md "wikilink") [Category:中央區
(神戶市)](../Category/中央區_\(神戶市\).md "wikilink")
[Category:一和会](../Category/一和会.md "wikilink")