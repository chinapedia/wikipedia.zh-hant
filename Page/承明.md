**承明**（476年六月—十二月）是[北魏的君主](../Page/北魏.md "wikilink")[魏孝文帝元宏的第二个](../Page/北魏孝文帝.md "wikilink")[年号](../Page/年号.md "wikilink")，共计數月。承明元年改元太和元年。

## 大事记

## 出生

## 逝世

## 纪年

| 承明                               | 元年                             |
| -------------------------------- | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 476年                           |
| [干支](../Page/干支纪年.md "wikilink") | [丙辰](../Page/丙辰.md "wikilink") |

## 參看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 同期存在的其他政权年号
      - [元徽](../Page/元徽.md "wikilink")（473年正月—477年七月）：[南朝宋](../Page/南朝宋.md "wikilink")[宋后废帝刘昱的年号](../Page/宋后废帝.md "wikilink")
      - [永康](../Page/永康_\(柔然\).md "wikilink")（464年-484年）：[柔然政权受罗部真可汗](../Page/柔然.md "wikilink")[予成年号](../Page/予成.md "wikilink")

## 參考文獻

  - 李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129

[Category:北魏年号](../Category/北魏年号.md "wikilink")
[Category:5世纪](../Category/5世纪.md "wikilink")