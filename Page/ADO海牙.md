[ADO_Den_Haag_Stadion,_Forepark.jpg](https://zh.wikipedia.org/wiki/File:ADO_Den_Haag_Stadion,_Forepark.jpg "fig:ADO_Den_Haag_Stadion,_Forepark.jpg")
**ADO海牙**（**ADO**全名是*Alles Door Oefening*，*Everything Through
Practice*，意思是*每件事都要經過訓練*）是[荷蘭](../Page/荷蘭.md "wikilink")[海牙的一間主要球會](../Page/海牙.md "wikilink")。球會的主場位於新建的[海牙體育場](../Page/海牙體育場.md "wikilink")（ADO
Den Haag Stadion），擁有15,000個座位。主場球衣的顏色為黃色和綠色。

球會先前稱為FC海牙（FC Den
Haag），而ADO則表示球會業餘隊的分支。雖然海牙市是荷蘭傳統三大城市之一，但ADO海牙的成就卻無法和[阿積士和](../Page/阿積士.md "wikilink")[飛燕諾在荷蘭的成就相比](../Page/飛燕諾.md "wikilink")。而和這兩間球會的競爭，經常引起雙方的球迷衝突。但是，由於和阿積士間的衝突比和飛燕諾的多，所以當阿積士和飛燕諾的球迷發生衝突時，ADO的球迷通常會比較傾向飛燕諾的一邊。

ADO海牙在1942年和1943年贏得[荷蘭甲組足球聯賽冠軍](../Page/荷蘭甲組足球聯賽.md "wikilink")，和在1968年與1975年贏得[荷蘭盃的冠軍](../Page/荷蘭盃.md "wikilink")（都是以FC海牙之名取得）。而ADO在歐洲賽最好的成績，就在1976年，球會打入[歐洲杯賽冠軍杯的準決賽](../Page/1975–76年歐洲盃賽冠軍盃.md "wikilink")，對手是[韋斯咸](../Page/韋斯咸.md "wikilink")。他們主場贏4-2，而作客輸1-3，標誌球會被淘汰。其後經過一段長時間在乙組的角逐，現在則重回甲組。

## 榮譽

  - **[荷兰足球甲级联赛](../Page/荷兰足球甲级联赛.md "wikilink")**（2）：1941－42年、1942－43年；
  - **[荷蘭盃](../Page/荷蘭盃.md "wikilink")**（2）：1967－68年、1974－75年；
  - **[荷兰足球乙级联赛](../Page/荷兰足球乙级联赛.md "wikilink")**（2）：1985－86年、2002－03年；

## 著名球員

  - 荷蘭

<!-- end list -->

  - [艾禾卡特](../Page/艾禾卡特.md "wikilink")

  - [博德](../Page/博德.md "wikilink")

  - [維尼斯](../Page/維尼斯.md "wikilink")

  - [Bram Appel](../Page/Bram_Appel.md "wikilink")

  - [卡斯迪倫](../Page/卡斯迪倫.md "wikilink")

  - [Mick Clavan](../Page/Mick_Clavan.md "wikilink")

  - [Johnny Dusbaba](../Page/Johnny_Dusbaba.md "wikilink")

  - [Hans Galjé](../Page/Hans_Galjé.md "wikilink")

  - [Oeki Hoekema](../Page/Oeki_Hoekema.md "wikilink")

  - [賀根杜柏](../Page/賀根杜柏.md "wikilink")

  - [Peter Houtman](../Page/Peter_Houtman.md "wikilink")

  - [Henk Houwaart](../Page/Henk_Houwaart.md "wikilink")

  - [馬田祖爾](../Page/馬田祖爾.md "wikilink")

  - [約翰迪莊](../Page/約翰迪莊.md "wikilink")

  - [迪莊](../Page/Aad_de_Jong.md "wikilink")

  - [Aad Kila](../Page/Aad_Kila.md "wikilink")

  - [Joop Korevaar](../Page/Joop_Korevaar.md "wikilink")

  - [Wim Landman](../Page/Wim_Landman.md "wikilink")

  - [Harry van der Laan](../Page/Harry_van_der_Laan.md "wikilink")

  - [Henk van Leeuwen](../Page/Henk_van_Leeuwen.md "wikilink")

  - [林球納](../Page/林球納.md "wikilink")

  - [Woody Louwerens](../Page/Woody_Louwerens.md "wikilink")

  - [Aad Mansveld](../Page/Aad_Mansveld.md "wikilink")

  - [摩爾斯](../Page/摩爾斯.md "wikilink")

  - [Rob Ouwehand](../Page/Rob_Ouwehand.md "wikilink")

  - [普拉克](../Page/普拉克.md "wikilink")

  - [Sjaak Roggeveen](../Page/Sjaak_Roggeveen.md "wikilink")

  - [Lex Schoenmaker](../Page/Lex_Schoenmaker.md "wikilink")

  - [Carol Schuurman](../Page/Carol_Schuurman.md "wikilink")

  - [泰倫](../Page/泰倫.md "wikilink")

  - [Wim Tap](../Page/Wim_Tap.md "wikilink")

  - [Theo Timmermans](../Page/Theo_Timmermans.md "wikilink")

  - [Marciano Vink](../Page/Marciano_Vink.md "wikilink")

  - [Simon van Vliet](../Page/Simon_van_Vliet.md "wikilink")

  - [麥克韋伯](../Page/麥克韋伯.md "wikilink")

  - [威斯爾](../Page/威斯爾.md "wikilink")

  - [Piet de Zoete](../Page/Piet_de_Zoete.md "wikilink")

## 球會經理

ADO海牙和FC海牙曾出產幾名著名的球會經理，例如：

  - Ernst Happel - 1962 - 1969
  - Vaclav Jezek - 1969 - 1972
  - Vujadin Boskov - 1974 - 1976
  - Anton Malatinský - 1976 - 1978
  - Co Adriaanse - 1988 - 1992
  - Rinus Israel - 2001 - 2004

## 外部連結

  - [官方網站](http://www.adodenhaag.nl/)
  - [球迷網站](http://www.adofans.nl/)
  - [球會新聞](http://www.epitch.co.uk/eredivisie/ado-den-haag/)

[A](../Category/荷蘭足球俱樂部.md "wikilink")
[Category:1905年建立的足球俱樂部](../Category/1905年建立的足球俱樂部.md "wikilink")