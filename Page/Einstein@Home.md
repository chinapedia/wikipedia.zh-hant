**Einstein@Home**，是一个由[威斯康星大學密爾沃基分校主辦](../Page/威斯康星大學密爾沃基分校.md "wikilink")，基于[BOINC计算平台的](../Page/BOINC.md "wikilink")[分布式计算项目](../Page/分布式计算.md "wikilink")。

## 尋找引力波

[阿尔伯特·爱因斯坦在很久以前发现](../Page/阿尔伯特·爱因斯坦.md "wikilink")，我们在充满着空间波动的[宇宙中随波逐流](../Page/宇宙.md "wikilink")。[黑洞碰撞](../Page/黑洞.md "wikilink")，[恒星崩塌](../Page/恒星.md "wikilink")，以及自旋[脉冲星激起了空间时间结构的波动](../Page/脉冲星.md "wikilink")，波动微妙地扭曲了我们周围的世界。这些引力波已经把科学家难倒了近一个世纪。這些計算需要大量的電腦才能完成。

**Einstein@Home**計劃通过位于[美国的](../Page/美国.md "wikilink")[激光干涉引力波天文台](../Page/激光干涉引力波天文台.md "wikilink")（Laser
Interferometer Gravitational wave Observatory，LIGO）和位于德国的[GEO
600引力波天文台收集数据](../Page/GEO_600.md "wikilink")，希望从这些收集来的数据当中寻找能够证实[爱因斯坦的](../Page/爱因斯坦.md "wikilink")[广义相对论中的](../Page/广义相对论.md "wikilink")[引力波存在的证据](../Page/引力波.md "wikilink")。根據愛因斯坦的理論，由於中子星的表面會有質量的崩坍，所以當中子星急速旋轉時，會向四周發放引力波。

### 科學的目的

[BOINCManager.png](https://zh.wikipedia.org/wiki/File:BOINCManager.png "fig:BOINCManager.png")
Einstein@Home應該不可能會成為首個直接量度得到[引力波的計劃](../Page/引力波.md "wikilink")，雖然計劃的成功意味着可得到[諾貝爾獎](../Page/諾貝爾獎.md "wikilink")，但由於現時LIGO/GEO已發現一對[黑洞正在合併](../Page/黑洞.md "wikilink")，這次合併所產生的引力波，很大機會會在2006年由LIGO/GEO直接觀察得到。而這個計劃所做的，只是把LIGO/GEO從其他脈衝星所發出的引力波重新整理。

## 参見

  - [分布式计算](../Page/分布式计算.md "wikilink")
  - [网格计算](../Page/网格计算.md "wikilink")
  - [BOINC](../Page/BOINC.md "wikilink")

## 外部链接

  - [Einstein@Home 主题中文站](http://boinc.equn.com/einstein/)
  - [Einstein@home
    中文项目信息](http://boinc.equn.com/einstein/einsteinathome.htm)
  - [Einstein@Home Website](http://einstein.phys.uwm.edu/)
  - [Einstein@home project
    information](https://web.archive.org/web/20041009233424/http://www.physics2005.org/events/einsteinathome/)
  - [Berkeley Open Infrastructure for Network Computing
    (BOINC)](http://boinc.berkeley.edu/)

<!-- end list -->

  - [BOINC 中文主页](http://boinc.equn.com/)

<!-- end list -->

  - [BOINC 计算平台使用详解](http://www.equn.com/forum/viewthread.php?tid=5753)

[Category:伯克利开放式网络计算平台](../Category/伯克利开放式网络计算平台.md "wikilink")
[Category:引力波望远镜](../Category/引力波望远镜.md "wikilink")