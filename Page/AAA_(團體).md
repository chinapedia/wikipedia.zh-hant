**AAA**（[日語](../Page/日語.md "wikilink")：トリプル・エー，[英語](../Page/英語.md "wikilink")：Triple
A）是[日本男女](../Page/日本.md "wikilink")6人組的[舞蹈流行音樂及](../Page/舞蹈流行音樂.md "wikilink")[表演藝術團體](../Page/表演藝術.md "wikilink")，團名取自英文“***A**TTACK*
***A**LL*
***A**ROUND*”的三個頭文字母，意指“全方位出擊”，團員全部經由愛貝克思甄選會發掘，愛貝克思藝術學院培養，也是唱片公司[愛貝克思集團轉型綜合娛樂公司的第一組唱片](../Page/愛貝克思集團.md "wikilink")、舞台、雜誌、電臺、電視、電影、公益等全面發展的直系[藝人](../Page/藝人.md "wikilink")。2003年，以5名男成員[西島隆弘](../Page/西島隆弘.md "wikilink")、[浦田直也](../Page/浦田直也.md "wikilink")、[日高光啟](../Page/日高光啟.md "wikilink")、[與真司郎](../Page/與真司郎.md "wikilink")、[末吉秀太](../Page/末吉秀太.md "wikilink")，以及1名女成員[宇野實彩子組成](../Page/宇野實彩子.md "wikilink")。2005年正式成立，加入2名女成員[後藤友香里](../Page/後藤友香里.md "wikilink")（5月－2007年）、[伊藤千晃](../Page/伊藤千晃.md "wikilink")（6月－2017年）至男女8人組，以西島隆弘為活動中心，實行[男子音樂組合與](../Page/男子音樂組合.md "wikilink")[女子音樂組合為主要的分組模式](../Page/女子音樂組合.md "wikilink")。2005年9月14日，AAA以[周杰倫主演](../Page/周杰倫.md "wikilink")[香港電影](../Page/香港電影.md "wikilink")《[頭文字D](../Page/頭文字D.md "wikilink")》的日本地區主題歌同名單曲《[BLOOD
on
FIRE](../Page/BLOOD_on_FIRE.md "wikilink")》正式出道，經3個月獲得第47屆[日本唱片大獎最佳新人獎](../Page/日本唱片大獎.md "wikilink")。2009年因公司改制，合約自[愛貝克思娛樂轉至](../Page/愛貝克思娛樂.md "wikilink")[愛貝克思管理](../Page/愛貝克思管理.md "wikilink")。

AAA的團長是最年長的成員浦田直也\[1\]，唱片的[執行製作人是愛貝克思集團董事長](../Page/執行製作人.md "wikilink")，初期曾由愛貝克思娛樂董事會董事原田淳領導[A\&R工作](../Page/A&R.md "wikilink")。出道前曾以愛貝克思超大型新人企劃團體“Avex
Audition
Allstar”（愛貝克思甄選全明星）名義活動。經由富樫明生（2005年）、鈴木大輔（2009年）、小室哲哉（2010年－2011年）等[音樂製作人聯合打造體制與音樂性](../Page/音樂製作人.md "wikilink")。團體通常被誤認是[偶像組合](../Page/日本偶像.md "wikilink")，實際是以原創舞蹈為中心的[藝術家身份與不同](../Page/藝術家.md "wikilink")[詞曲作家展開合作](../Page/詞曲作家.md "wikilink")，有時也會與其他舞蹈家共同完成編舞\[2\]。自第13張單曲《[Get
啾\! /
SHE的真相](../Page/Get_啾!_/_SHE的真相.md "wikilink")》及第3張原創專輯《[AROUND](../Page/AROUND_\(AAA專輯\).md "wikilink")》（2007年）以降，所有作品與活動皆出自團體自身企劃。AAA對[日本流行音乐](../Page/日本流行音乐.md "wikilink")，特别是[青少年流行音樂](../Page/青少年流行音樂.md "wikilink")、舞蹈、時尚領域的影響，以及“男女混合”“一人一色”“全員ACE”等有別於同類[流行樂團或偶像組合的特性](../Page/流行樂團.md "wikilink")，而被日本及海外媒體稱作“珍稀的男女團體”“日本最強男女組合”。團體更因積極投身社會活動，獲得2011年[日本內閣授予的文化交流貢獻獎狀](../Page/日本內閣.md "wikilink")，以及[2020年東京奧運會的](../Page/2020年東京奧運會.md "wikilink")[空手道項目官方大使任命等榮譽](../Page/空手道.md "wikilink")。

AAA自2005年開始，連續參加[愛貝克思夏日聯合國](../Page/愛貝克思夏日聯合國.md "wikilink")。2006年開始，每年於日本召開巡迴演唱會與出道紀念演唱會。2009年以後，成員浦田直也（）、日高光啟（）、伊藤千晃（chiaki）、與真司郎（）、西島隆弘（）陸續展開正式的個人名義活動。2010年開始，歌曲連續提名[日本唱片大獎的最優秀作品並參加](../Page/日本唱片大獎.md "wikilink")[紅白歌合戰](../Page/紅白歌合戰.md "wikilink")。2012年開始，每年全員或分組參加春夏、秋冬兩季的[東京女孩展演](../Page/東京女孩展演.md "wikilink")，正式進軍生活用品、時裝首飾設計領域。2016年完成大阪巨蛋與東京巨蛋演唱會。截至2015年，團體自身累計舉辦個人[演唱會等演出超過](../Page/演唱會.md "wikilink")200場，其中包括[美國](../Page/美國.md "wikilink")2場與[東南亞](../Page/東南亞.md "wikilink")4場，累計出版個人[單曲](../Page/單曲.md "wikilink")、[專輯](../Page/專輯.md "wikilink")、[錄像帶](../Page/錄像帶.md "wikilink")、[寫真集等作品超過](../Page/寫真集.md "wikilink")100部，其中包括[日本唱片協會認證的](../Page/日本唱片協會.md "wikilink")5張金碟與13首金曲。

## 歷史沿革

### 2001年－2005年：甄選全明星

[AAA_LOGO_1.svg](https://zh.wikipedia.org/wiki/File:AAA_LOGO_1.svg "fig:AAA_LOGO_1.svg")
2001年1月21日，就讀於[早稻田大學附屬學校的](../Page/早稻田大學.md "wikilink")[日高光啟通過](../Page/日高光啟.md "wikilink")[傑尼斯事務所甄選](../Page/傑尼斯事務所.md "wikilink")，成為[小山慶一郎](../Page/小山慶一郎.md "wikilink")（[NEWS](../Page/NEWS.md "wikilink")）同期的[小傑尼斯成員](../Page/小傑尼斯.md "wikilink")。日高光啟是個[早產兒](../Page/早產兒.md "wikilink")，曾夢想成為足球運動員，但左耳聽力逐漸喪失，致他聽不清教練的訓話而選擇放棄；因能聽到不一樣的聲音，在任[日本航空機長](../Page/日本航空.md "wikilink")、[法政大學航空部教授的父親日高光信的建議下選擇了音樂](../Page/法政大學.md "wikilink")\[3\]。

2001年4月8日－6月3日，[東京電視台播出專題節目](../Page/東京電視台.md "wikilink")《ASAYAN〈濱崎步甄選會〉》，旨在選出[濱崎步於福岡](../Page/濱崎步.md "wikilink")、名古屋、大阪、東京四大巨蛋巡迴演唱會的[舞者](../Page/舞者.md "wikilink")。當時從事[自由職業](../Page/自由職業.md "wikilink")，有過50余次[歌手甄選失敗經歷的](../Page/歌手.md "wikilink")[浦田直也合格](../Page/浦田直也.md "wikilink")，落選者有後來的加藤美穗（[加藤米莉亞](../Page/加藤米莉亞.md "wikilink")）和阿久津博子（[mihimaru
GT](../Page/mihimaru_GT.md "wikilink")）、等\[4\]。

同年，日高光啟於天主教會貴族女校的朋友的同校好友[宇野實彩子短期留學英國](../Page/宇野實彩子.md "wikilink")。宇野實彩子曾受到1998年《ASAYAN〈歌唱甄選會〉》總決賽冠軍，由音樂製作人[小室哲哉一手捧紅](../Page/小室哲哉.md "wikilink")、大獲成功的[鈴木亞美影響](../Page/鈴木亞美.md "wikilink")，而夢想成為一名電視明星、偶像藝人。因一次失敗的甄選經歷，讓宇野發現自己並不適合\[5\]。

2002年7月13日－12月28日，愛貝克思召開第二屆全國甄選“avex audition
2002”，旨在尋找有能力適應21世紀潮流，往國際方向發展的歌手、[演员](../Page/演员.md "wikilink")、[模特兒](../Page/模特兒.md "wikilink")\[6\]。留學期間因[倫敦西區音樂劇](../Page/西區_\(倫敦\).md "wikilink")《[貓](../Page/貓_\(音樂劇\).md "wikilink")》影響，決定嘗試舞台演出路線的宇野實彩子合格，成為成員里首位愛貝克思藝術學院（aaa）簽約[訓練生](../Page/訓練生.md "wikilink")\[7\]。同期包括[宮脇詩音](../Page/宮脇詩音.md "wikilink")、[瀧本美織](../Page/瀧本美織.md "wikilink")、共15人。

2003年1月1日，[VISION
FACTORY背景的星光學園](../Page/RISINGPRO.md "wikilink")（STARLIGHT
SCHOOL）長崎校開始預售第4張細碟《星光之星（STAR OF STARLIGHT
vol4）》，收錄了當時藝名SYUTA（シュウタ）的[末吉秀太擔當二把手](../Page/末吉秀太.md "wikilink")，來自學園選拔舞者加速男孩（ACCEL・BOYS）的4人[獨立音樂團體](../Page/獨立音樂.md "wikilink")“FRIENDS”原創曲目《在一起（Be
Together）》。同月，美國日本札幌本部校也從少年班選出[西島隆弘等五名男生組成偶像組合](../Page/西島隆弘.md "wikilink")“japs”，預定於4月、9月分別在[北海道當地及全日本發行出道單曲](../Page/北海道.md "wikilink")《[珍貴的愛
/
難以割舍](../Page/珍貴的愛_/_難以割舍.md "wikilink")（プレシャスラヴ/止まらない）》。當時japs的目標是擁有百萬銷量作品、一人一個性、全方位發展的[SMAP](../Page/SMAP.md "wikilink")，以西島隆弘為中心展開活動。

2003年5月8日，愛貝克思音樂節目《》宣佈由5名生於1990年代的2002級訓練生組成出道，宇野實彩子在內的其他訓練生按第一屆甄選未入選[Dream的](../Page/Dream.md "wikilink")[倖田來未模式培養](../Page/倖田來未.md "wikilink")，但宇野的目標是男女合作的國民作品，且覺察到個人女歌手的市場前景，以“沒自信個人活動”為理由拒絕\[8\]\[9\]。此舉促使正有計劃組建一支男女混合團體的原田淳開始“**EXIT**”（出路）為名的新團體企劃，兩人一拍即合\[10\]\[11\]。

2003月8日1日－9月30日，愛貝克思召開首次男藝人甄選的“男募集\!”，面向業內各大小事務所、演藝學校里有意尋求新出路的儲備生，最終選出為製作屬於自己的音樂而退出傑尼斯的日高光啟，為濱崎步伴舞的空閒就讀愛貝克思藝術學院聲樂班的浦田直也，在北海道zepp
sapporo擁有客滿記錄的japs成員西島隆弘，出道單曲「BeTogether」3000張售罄的Friends成員末吉秀太，以及京都與大阪地區活動的舞團成員、京都大正精工董事長與義廣之子[與真司郎](../Page/與真司郎.md "wikilink")\[12\]。

由西島隆弘、浦田直也、日高光啟、與真司郎、末吉秀太這5名擁有演藝學校或事務所背景的專業藝校生，以及[業餘愛好者的唯一女成員宇野實彩子組成最初的男女](../Page/業餘愛好者.md "wikilink")6人組體制，於愛貝克思藝術學院定期接受練習生課程。當時經宇野的多次申請，白百合學園校方批准6人成立以演出企劃為中心的舞蹈同好會，這一時期確立了浦田直也為團長，西島隆弘為演出時的中心成員\[13\]。期間，6人曾出演過白百合學園的文化祭“白百合祭”，因宇野忘記準備磁帶，首次採用[無伴奏合唱進行演出](../Page/無伴奏合唱.md "wikilink")\[14\]。

2004年8月15日，當時模特身份活躍於名古屋時尚雜志《Cheek》的[伊藤千晃](../Page/伊藤千晃.md "wikilink")，受濱崎步影響參加了第三屆愛貝克思全國甄選（Avex
Audition2004），以《よさこい鳴子踊り》合格，成為單獨的訓練生。同年，與真司郎考入[堀越高等学校](../Page/堀越高等学校.md "wikilink")，成為[Perfume與](../Page/Perfume.md "wikilink")[Ami](../Page/Ami_\(Dream\).md "wikilink")（[E-girls](../Page/E-girls.md "wikilink")）的同班同學；西島隆弘退出japs；富樫明生成為團體的第一位[音樂製作人](../Page/音樂製作人.md "wikilink")，籌備出道工作。

2005月1月，以無縫對接的“即戰力”為主題展開面向17歲到22歲女參賽者的限定甄選會（17歳-22歳限定Audition），用以尋找宇野的搭檔成員。最終由曾獲國際藝術體操團體比賽第四名的日本國家隊少年組成員、2002年全國星光甄選會（全国スターライトオーディション）優秀主唱賞獲得者、Platinum
Production出身的寫真偶像、浦田直也於愛貝克思藝術學院的同班[後藤友香里合格](../Page/後藤友香里.md "wikilink")。

6人於2005年4月以“**AVEX AUDITION ALLSTARS**”名義及歌曲《[Friday
Party](../Page/Friday_Party.md "wikilink")》宣傳影片在愛貝克思娛樂品牌經理及[A\&R主管採用情報官方網站個人主頁發表](../Page/A&R.md "wikilink")，加入後藤友香里至男女7人組，5月開始職業活動\[15\]。期間一行人有去到美國。5月16日，伊藤千晃參加富樫明生評審的日本電視台節目《歌星（歌スタ）》合格。24日，於六本木Velfarre召開的愛貝克思內部發表會“avex
showcase 2005”展示舞蹈、歌唱（《Friday
Party》）、MC才藝，公司決定讓AAA出道\[16\]\[17\]。西島因手腕骨折沒有參加舞蹈環節\[18\]。

2005年6月1日，官方網站建立，更名“**ATTACK ALL
AROUND**”。6月15日，加入伊藤千晃至男女8人組活動。6月30日，於duo
MUSIC
EXCHANGE舉辦的“第二屆救世主音樂節”（メシアフェスティバルVol.2）表演了[踢踏舞](../Page/踢踏舞.md "wikilink")，首唱新歌《歡迎來到這個世界（welcome
to this
world）》，開始正式使用簡稱“AAA”名義活動\[19\]\[20\]。8月1日，AAA於當時青山與表參道附近的愛貝克思總部門口進行個人街頭展演，當時只有2、3名觀眾\[21\]。

2005年8月8日，AAA召開出道發佈會暨愛貝克思娛樂签约儀式“IT'S SHOW TIME”（AAA debut entertainment
convention - IT'S SHOW
TIME-），以[音樂劇形式進行了歌曲表演](../Page/音樂劇.md "wikilink")\[22\]\[23\]。這一活動名稱後來成為2014年單曲《[SHOW
TIME](../Page/SHOW_TIME.md "wikilink")》的其中一句歌詞和企劃靈感。8月10日－11月13日，正式於澀谷、大阪、福岡、札幌等地展開[街頭藝人活動](../Page/街頭藝人.md "wikilink")\[24\]。8月27日，於[日產汽車展覽聽出演](../Page/日產汽車.md "wikilink")[日本電視台主辦的公益活動](../Page/日本電視台.md "wikilink")《[24小時電視
「愛心救地球」](../Page/24小時電視_「愛心救地球」.md "wikilink")》\[25\]。8月29日，出席於新宿召開的中國香港電影《[頭文字D](../Page/頭文字D.md "wikilink")》日本首映禮活動，表演了日本地區主題歌《[BLOOD
on FIRE](../Page/BLOOD_on_FIRE.md "wikilink")（热血沸腾）》\[26\]。

2005年9月9日，AAA於[日本放送協會播出的](../Page/日本放送協會.md "wikilink")《[POP
JAM](../Page/POP_JAM.md "wikilink")》首次亮相全國有線電視台，以《BLOOD on
FIRE》的歌舞表演取得“break
radar”環節72.2%觀眾支持率的高品質新人評價\[27\]。9月13日，出席於velfarre召開的《頭文字D》[Infoseek獨家試映會](../Page/Infoseek.md "wikilink")\[28\]。
2005年9月14日，AAA發行了出道單曲《BLOOD on FIRE》與自傳圖書《第一個挑戰（The First
Attack），單曲搭配電影主題歌的宣傳效果及500日元的定價，取得[ORICON細碟周榜第](../Page/ORICON.md "wikilink")9位的成績取得。接下來的10月、11月、12月，AAA保持每月一張單曲，完成四個月連續發行四張單曲的出道企劃。當年年末，單曲《BLOOD
on
FIRE》作為2005年日本所有新人出道唱片中成績最好的作品，使得AAA當選第47屆[日本唱片大獎的最優秀新人獎](../Page/日本唱片大獎.md "wikilink")\[29\]\[30\]。

### 2006年－2008年：全方位出擊

2006年1月，AAA發行了首張專輯《[ATTACK](../Page/ATTACK_\(AAA專輯\).md "wikilink")》。9月，AAA發行了首張迷你專輯《[ALL/2](../Page/ALL/2.md "wikilink")》。11月，男成員[西島隆弘和](../Page/西島隆弘.md "wikilink")[浦田直也兩人單獨演唱了AAA第十三張單曲的](../Page/浦田直也.md "wikilink")《口香糖》。AAA在2到12月一共發行了8張單曲，其中《[颶風·莉莉，波士頓·瑪麗](../Page/颶風·莉莉，波士頓·瑪麗.md "wikilink")》獲得[BEST
HITS歌謠祭的金曲獎](../Page/BEST_HITS歌謠祭.md "wikilink")。這一年女成員的[宇野實彩子出演了北美兩週票房冠軍的好萊塢電影](../Page/宇野實彩子.md "wikilink")[咒怨2](../Page/咒怨2.md "wikilink")，並創作演唱了國際版的主題歌。

2007年1月，AAA發行第二張原創專輯《[ALL](../Page/ALL_\(AAA專輯\).md "wikilink")》。2007年3月，AAA以「**AAA
DEN-O Form**」的名義發行了特別單曲《[Climax
Jump](../Page/Climax_Jump.md "wikilink")》，該曲是[假面騎士電王的主題曲](../Page/假面騎士電王.md "wikilink")，取得日本公信榜單曲周榜第5位和超過100,000張銷量的成績，并獲日本唱片協會認證，成為此後AAA銷量最高的作品。隨後發行的《Get
啾》成為首張以AAA名義賣出40,000張的單曲，並獲得第一份廣告合約，同時首次獲得了四家音樂節目的邀請。

同年6月，成員後藤友香里宣布因病停止活動。7月，AAA參與了[美國](../Page/美國.md "wikilink")[馬利蘭州巴爾的摩舉行的Otakon動漫展](../Page/巴爾的摩.md "wikilink")，並舉辦了時長一個小時的個人演唱會。在此期間，成員在巴爾的摩當地名為“High
Rock”的名勝拍照時，用噴漆在石上寫下『JPN AAA
2007』的字樣，回國後由伊藤千晃把照片上傳到官方的部落格。因為該處已被列入世界遺產，該舉動引起日本網民強烈批評，后來愛貝克思公司于AAA官網進行了道歉。

2008年，AAA發行了三張單曲，其中總第十七作的《[MIRAGE](../Page/MIRAGE_\(AAA單曲\).md "wikilink")》成為團體首張日本公信榜單週冠軍單曲，並且從該作開始AAA的單曲成績穩定在前十位。3月，AAA發行了涵蓋八人時期的首張精選專輯《[ATTACK
ALL AROUND](../Page/ATTACK_ALL_AROUND_\(AAA專輯\).md "wikilink")（全方位出擊）》。

同年5月的時候，AAA的歌曲《[Get
啾\!](../Page/Get_啾!_/_SHE的真相.md "wikilink")》被日本MTV提名為2007年最佳卡啦OK歌曲，但沒有得獎。8月，AAA以《Get
啾\!》被日本學生票選為MTV的最優秀舞蹈藝術家，並現場表演了《Get
啾\!》和新曲《MUSIC\!\!\!》。此後《MUSIC\!\!\!》成為AAA的其中一首代表作\[31\]\[32\]。

這一年，女成員伊藤千晃參與主演了的臺灣電視劇《[蜂蜜幸運草](../Page/蜂蜜幸運草.md "wikilink")》在華人地區播出，劇中扮演華本育，而AAA的全體成員首次共同出演並主演的電視劇《新世紀莎士比亞》也在[關西電視台播出](../Page/關西電視台.md "wikilink")。

### 2009年－2011年：啟程，引領風潮

2009年，AAA發行了三張單曲，其中從年初發行了的總第二十作《[啟程之歌](../Page/啟程之歌_\(AAA單曲\).md "wikilink")》開始，女成員伊藤千晃首次在全員演唱單曲中得到獨唱段落，升爲第四位主唱。而《[Break
Down / Break your name / Summer
Revolution](../Page/Break_Down_/_Break_your_name_/_Summer_Revolution.md "wikilink")》的《Break
Down》則成為AAA第二首[BEST
HITS歌謠祭金曲獎作品](../Page/BEST_HITS歌謠祭.md "wikilink")，也是[與真司郎參與電視劇](../Page/與真司郎.md "wikilink")《帝王》的主題歌。

同年2月的時候，AAA發行了第四張原創專輯《[depArture](../Page/depArture.md "wikilink")》，排在專輯周榜第四位，團體也開始專注於音樂事業。3月，AAA主持的[JFN電台脫口秀節目](../Page/JFN.md "wikilink")《RADIO
SESSIONS　AAA TALKING
ATTACK\!\!\!》播出最後一期，西島隆弘因拍攝電視劇而缺席，改用電話特別錄製了《Nissy傳說（『ニッシー伝説』）》
。
\[33\]這一年成員西島隆弘參與主演了電影《[愛的曝光](../Page/愛的曝光.md "wikilink")》，并在電影中反串演出，隔年他憑此角色獲得了2009年[體育日本電影格蘭披治最優秀新人獎](../Page/體育日本.md "wikilink")。

[2009年H1N1流感大流行期間](../Page/2009年H1N1流感大流行.md "wikilink")，AAA也受到該流感病毒影響，例如11月2日的第42回[城西大学高麗祭公演](../Page/城西大学.md "wikilink")，團長[浦田直也因病缺席](../Page/浦田直也.md "wikilink")。\[34\]11月6日的国連之友Music
Earthist
2009，當天早上成員們接受了化驗體檢，這次則是主唱[西島隆弘因結果呈陽性而缺席](../Page/西島隆弘.md "wikilink")。\[35\]\[36\]

2010年2月，AAA發行了第5張原創專輯《[HEARTFUL](../Page/HEARTFUL.md "wikilink")》，首次嘗試了不同人數、不同性別成員之間的混編演唱，專輯也第一次在周榜進入前三名。5月，發行了第24張單曲《[想見你的理由
/ Dream After Dream
～夢醒之夢～](../Page/想見你的理由_/_Dream_After_Dream_～夢醒之夢～.md "wikilink")》，成為AAA第二張周榜冠軍單曲，其中「想見你的理由」在第52屆[日本唱片大獎獲得了](../Page/日本唱片大獎.md "wikilink")「優秀作品獎」，並在[第61回NHK紅白歌合戰演出](../Page/第61回NHK紅白歌合戰.md "wikilink")。

同年6月25日的時候，AAA在Ameba
Pigs舉辦了首次網絡實況演唱會，但女主唱伊藤千晃因同月22日在家裡不慎摔倒，必須在家靜養的關係而缺席。\[37\]10月20日，AAA出席了福岡的[仁愛女子短期大学學園祭演出](../Page/仁愛女子短期大学.md "wikilink")，伊藤千晃因[腹膜炎住院的原因而缺席](../Page/腹膜炎.md "wikilink")。\[38\]

2011年2月，AAA發行了第6張原創專輯《[Buzz
Communication](../Page/Buzz_Communication.md "wikilink")》，該作取得專輯周榜亞軍，期間成員與真司郎因拍攝電視劇缺席了17日在神奈川縣lazona川崎Ruefer廣場的發售會，改用同等大小的展示板代替。\[39\]3月，AAA預訂出席[香港亞洲流行音樂節](../Page/香港亞洲流行音樂節.md "wikilink")，但因[2011年日本東北地方太平洋近海地震而取消演出](../Page/2011年日本東北地方太平洋近海地震.md "wikilink")。下半年，AAA應日本・越南特別大使[杉良太郎之邀](../Page/杉良太郎.md "wikilink")，前往越南舉辦日越建交35周年「第二屆
日越友好音樂祭」。之後又出席在韓國舉行的[亞洲音樂節](../Page/亞洲音樂節.md "wikilink")。

同年4月的時候，AAA開始30場公演的全國巡迴演唱會「AAA "Buzz Communication" Tour
2011」。5月，伊藤千晃因拍攝[伊藤洋華堂廣告的時候不慎從高處跌落到一層](../Page/伊藤洋華堂.md "wikilink")，導致右手[肘關節脫臼和右腿膝蓋碰撞傷](../Page/脫臼.md "wikilink")，住院接受治療四周，缺席9場演唱會和non・no四十週年慶、愛貝克思集團股東限定演唱會等音樂活動。\[40\]例行頂替伊藤歌唱部分的[宇野實彩子之後出現了發聲困難](../Page/宇野實彩子.md "wikilink")，並因此開始改變歌唱方式\[41\]。

9月14日的出道六周年紀念日，AAA發行了涵蓋四人主唱時期的第二張精選專輯《[＃AAABEST](../Page/＃AAABEST.md "wikilink")》，該作成為AAA首張單週冠軍專輯和鉑金認證專輯，其中伊藤千晃因突發的身體原因而缺席了17日的大阪發售會。\[42\]這一年結束前，AAA發行的單曲總數達到第三十作，其中《CALL
/
I4U》的《CALL》成為AAA第二作日本唱片大獎優秀作品和NHK紅白歌合戰出演曲目，也是[與真司郎與](../Page/與真司郎.md "wikilink")[末吉秀太正式升爲主唱的第一作](../Page/末吉秀太.md "wikilink")。

### 2012年：七人、七年、七輯

2012年，AAA將於2月22日發行第31張單曲「[SAILING](../Page/SAILING.md "wikilink")」，以及他們出道以來首次週年紀念巡迴演唱會「[AAA
6th Anniversary Tour 2011.9.28 at Zepp
Tokyo](../Page/AAA_6th_Anniversary_Tour_2011.9.28_at_Zepp_Tokyo.md "wikilink")」的錄影帶。3月21日發行第3枚精選專輯《[Another
side of
＃AAABEST](../Page/Another_side_of_＃AAABEST.md "wikilink")》，以非主打曲為主的精選專輯。
同年4月展開全國巡迴演唱會「AAA TOUR 2012 -777- TRIPLE
SEVEN」，一共38場公演。同年5月12日，AAA的官方網站出現了神秘的倒數時計和一名女子的剪影。其後於16日（單曲「[Still
Love
You](../Page/Still_Love_You.md "wikilink")」發行日）中午12时AAA的官方網站正式公佈七週年紀念活動「TRIPLE7
Campaign」，並宣佈AAA將會加入一名準成員，以「AAA第八名成員」作有限期活動。除了七週年紀念活動，AAA於7月和8月發行第33張單曲「[777
～We can sing a
song\!～](../Page/777_～We_can_sing_a_song!～.md "wikilink")」和第7枚原創專輯「[777
～TRIPLE
SEVEN～](../Page/777_～TRIPLE_SEVEN～.md "wikilink")」來迎接組合結成七週年，並在8月23日的「[777
～TRIPLE
SEVEN～](../Page/777_～TRIPLE_SEVEN～.md "wikilink")」專輯見面會上，宣布會在10月5日的前往台灣做宣傳活動，成員們都期待首次前往海外舉辦獨立活動，並表示將以台灣作為海外據點，從亞洲開始將音樂版圖拓展到全世界。這次公演創下千張門票57秒被搶光的紀錄，但其後艾迴宣佈因諸多考量而取消公演，引起歌迷不滿。

繼「[想見你的理由](../Page/想見你的理由_/_Dream_After_Dream_～夢醒之夢～.md "wikilink")」、「[CALL](../Page/CALL_/_I4U.md "wikilink")」後，AAA以「[777
～We can sing a
song\!～](../Page/777_～We_can_sing_a_song!～.md "wikilink")」連續第三年在[日本唱片大獎獲得](../Page/日本唱片大獎.md "wikilink")「優秀作品獎」，並於12月31日在[第63回NHK紅白歌合戰演出](../Page/第63回NHK紅白歌合戰.md "wikilink")，連續第三年登上紅白歌合戰。

### 2013年：第八個奇蹟

2013年，AAA將於1月23日發行第35張單曲「[Miss
you/笑容綻放的地方](../Page/Miss_you/笑容綻放的地方.md "wikilink")」。3月13日發行第36張單曲「[PARTY
IT UP](../Page/PARTY_IT_UP.md "wikilink")」和第4枚精選專輯「[Ballad
Collection](../Page/Ballad_Collection.md "wikilink")」，同年4月展開出道以來最大型全國巡迴演唱會「AAA
TOUR 2013 EIGHTH WONDER」，一共41場公演。

5月13日在「AAA キズナ合宿2」首次披露於6月26日發行第37張單曲「[Love Is In The
Air](../Page/Love_Is_In_The_Air.md "wikilink")」，繼「[夏日風物](../Page/夏日風物.md "wikilink")」、「[777
～We can sing a
song\!～](../Page/777_～We_can_sing_a_song!～.md "wikilink")」後第三張以夏天為主題的單曲。

除了組合的活動，成員都有各自的個人活動：[西島隆弘以](../Page/西島隆弘.md "wikilink")「Nissy」名義展開個人活動，並於「a-nation
island Asia Progress ～White
Beach～」演唱其個人單曲「該怎麼辦?」、「自私」、[宇野實彩子出演](../Page/宇野實彩子.md "wikilink")[東京電視台電視劇](../Page/東京電視台.md "wikilink")《[東京玩具箱](../Page/東京玩具箱.md "wikilink")》、[浦田直也再次以個人名義展開](../Page/浦田直也.md "wikilink")30歲慶祝紀念活動「URATA
NAOYA 30th Anniversary Cover
Project」，翻唱多首歌曲，並以配信形式發行、[日高光啓也以](../Page/日高光啓.md "wikilink")「SKY-HI」名義展開個人活動，2月舉行全國巡迴演唱會「THE
1ST FLIGHT」，8月發行首張單曲「[愛BLOOM /
RULE](../Page/愛BLOOM_/_RULE.md "wikilink")」，並以個人形式出演「a-nation island
Asia Progress ～White
Beach～」、[與真司郎也以](../Page/與真司郎.md "wikilink")「SHINJIRO
ATAE（from AAA）」名義展開個人活動，推出個人DVD「New Discovery in
Hawaii」、[末吉秀太出演舞台劇](../Page/末吉秀太.md "wikilink")「シダの群れ」、[伊藤千晃與化妝品公司](../Page/伊藤千晃.md "wikilink")[KOJI合作推出](../Page/KOJI.md "wikilink")「CharmingKiss」系列。

於9月4日發行第38張單曲、第8張原創專輯先行單曲「[戀歌與雨天](../Page/戀歌與雨天.md "wikilink")」，而原創專輯「[Eighth
Wonder](../Page/Eighth_Wonder_\(AAA\).md "wikilink")」於兩週後（9月18日）發行，這張專輯成為他們第2張張獲得Oricon公信榜每周銷量排名第1的專輯，同時亦是首張第1位的原創專輯。

單曲「[戀歌與雨天](../Page/戀歌與雨天.md "wikilink")」的PV於YouTube公開後，播放總數至今將近5000萬，成為AAA其中一首代表歌曲。

繼「[想見你的理由](../Page/想見你的理由_/_Dream_After_Dream_～夢醒之夢～.md "wikilink")」、「[CALL](../Page/CALL_/_I4U.md "wikilink")」、「[777
～We can sing a
song\!～](../Page/777_～We_can_sing_a_song!～.md "wikilink")」後，AAA以「[戀歌與雨天](../Page/戀歌與雨天.md "wikilink")」連續第四年在[日本唱片大獎獲得](../Page/日本唱片大獎.md "wikilink")「優秀作品獎」，並首次以紅組身份在[第64回NHK紅白歌合戰演出](../Page/第64回NHK紅白歌合戰.md "wikilink")，連續第四年登上紅白歌合戰。

### 2014年：黃金樂章

2014年，AAA將於2月19日發行第39張單曲「[Love](../Page/Love_\(AAA單曲\).md "wikilink")」，此曲為日本[紅十字會](../Page/紅十字會.md "wikilink")2014年捐血活動的宣傳歌曲。同年5月展開出道以來首次全國Arena巡迴演唱會「AAA
ARENA TOUR 2014 -GOLD SYMPHONY-」，一共25場公演。

3月26日發行第40張單曲「[SHOW
TIME](../Page/SHOW_TIME.md "wikilink")」，有別於以往的單曲，此單曲是〈AAA
Party〉限量盤，只發行於AAA的歌迷，並不作公開發行。

2014年年初，AAA被[富士電視台委任其動畫](../Page/富士電視台.md "wikilink")「[ONE
PIECE](../Page/ONE_PIECE.md "wikilink")」15週年的PR大使，設立任期1年，並演唱動畫的新主題曲「[Wake
up\!](../Page/Wake_up!.md "wikilink")」。「[Wake
up\!](../Page/Wake_up!.md "wikilink")」往後更成為AAA的第41張單曲，於7月2日發行。

AAA将于9月17日发行第42張單曲《[說再見之前](../Page/說再見之前.md "wikilink")》，作为第九张专辑《[GOLD
SYMPHONY](../Page/GOLD_SYMPHONY.md "wikilink")》的先行单曲

第九张原創專輯「[GOLD
SYMPHONY](../Page/GOLD_SYMPHONY.md "wikilink")」於10月1日發行，這張專輯成為他們第3張張獲得Oricon公信榜每周銷量排名第1的專輯，亦是繼「[Eighth
Wonder](../Page/Eighth_Wonder_\(AAA\).md "wikilink")」後第二張第1位的原創專輯。

10月18日，AAA於[北海道](../Page/北海道.md "wikilink")[札幌真駒內積水海姆冰場舉行](../Page/札幌.md "wikilink")「AAA
ARENA TOUR 2014 -GOLD SYMPHONY-」最終場公演，並宣佈來年（2015年）展開全國Arena巡迴演唱會「AAA
ARENA TOUR 2015 10th Anniversary -Attack All
Around-」，以及於2015年4月4日展開出道以來首次亞洲巡迴演唱會。

12月5日，AAA於官網正式公佈亞洲巡迴演唱會「AAA ASIA TOUR 2015 -Attack All Around- Supported
by
KOJI」的詳情，並落實於[新加坡](../Page/新加坡.md "wikilink")、[香港](../Page/香港.md "wikilink")、[印尼和](../Page/印尼.md "wikilink")[台灣舉行公演](../Page/台灣.md "wikilink")。

繼「[想見你的理由](../Page/想見你的理由_/_Dream_After_Dream_～夢醒之夢～.md "wikilink")」、「[CALL](../Page/CALL_/_I4U.md "wikilink")」、「[777
～We can sing a
song\!～](../Page/777_～We_can_sing_a_song!～.md "wikilink")」、「[戀歌與雨天](../Page/戀歌與雨天.md "wikilink")」後，AAA以「[告別之前](../Page/告別之前.md "wikilink")」連續第五年在日本唱片大獎獲得「優秀作品獎」，並於12月31日在[第65回NHK紅白歌合戰演出](../Page/第65回NHK紅白歌合戰.md "wikilink")，連續第五年登上紅白歌合戰

### 2015年：出道十週年

踏入2015年，AAA於元旦晚上舉行「AAA NEW YEAR PARTY 2015」。並宣佈出道十週年的計劃 -
連續7個月單曲發行，第一作為《[I'll be
there](../Page/I'll_be_there.md "wikilink")》，於1月28日發行；第二作為《[Lil'
Infinity](../Page/Lil'_Infinity.md "wikilink")》，於2月25日發行。

2015年4月展開亞洲巡迴演唱會「AAA ASIA TOUR 2015 -Attack All
Around-」，及後於5月展開十週年紀念全國Arena巡迴演唱會「AAA
ARENA TOUR 2015 10th Anniversary -Attack All Around-」。

十週年
連續7個月單曲發行中的第三作《[我的憂鬱與不開心的她](../Page/我的憂鬱與不開心的她.md "wikilink")》將於3月25日發行；第四作《[GAME
OVER?](../Page/GAME_OVER?.md "wikilink")》於4月29日發行。

AAA為[朝日電視台動畫](../Page/朝日電視台.md "wikilink")『[境界觸發者](../Page/境界觸發者.md "wikilink")』（WORLD
TRIGGER）演唱新主題曲「[明日之光](../Page/明日之光.md "wikilink")」。「明日之光」往後更成為AAA的第47張單曲（十週年
連續7個月單曲發行第五作），於5月27日發行。

5月1日，AAA宣佈在9月21日至23日於[富士急高原樂園舉行戶外演唱會](../Page/富士急高原樂園.md "wikilink")「AAA
10th Anniversary SPECIAL 野外LIVE in
富士急高原樂園」；10月初前往[夏威夷舉行粉絲見面會](../Page/夏威夷.md "wikilink")。

十週年 連續7個月單曲發行中的最後兩作 - 第六作、第48張單曲《[Flavor of
kiss](../Page/Flavor_of_kiss.md "wikilink")》於6月24日發行；第七作、第49張單曲《[LOVER](../Page/LOVER.md "wikilink")》於7月29日發行。

十週年單曲連發計劃的那七張單曲（《[I'll be
there](../Page/I'll_be_there_\(AAA單曲\).md "wikilink")》至《[LOVER](../Page/LOVER.md "wikilink")》），當中內藏一首未發行歌曲《STORY》，需要集齊七張單曲、並用特別的手機Apps「AAAR」，方可取得各成員演唱部份的音源。

為紀念出道10周年，AAA將於9月16日發行第50張單曲《[深愛著，卻不能愛](../Page/深愛著，卻不能愛.md "wikilink")》和第四張精選專輯及第十張原創專輯合輯《[AAA
10th ANNIVERSARY
BEST](../Page/AAA_10th_ANNIVERSARY_BEST.md "wikilink")》，而且精選專輯將收錄出道10年來所有經典歌曲及多首新曲。

9月28日，《[AAA 10th ANNIVERSARY
BEST](../Page/AAA_10th_ANNIVERSARY_BEST.md "wikilink")》於公信榜專輯週排行榜取得第1位。發行9日總銷量達10萬張，獲得金唱片銷量認證，並首次公信榜取得專輯月榜冠軍。

AAA憑「[深愛著，卻不能愛](../Page/深愛著，卻不能愛.md "wikilink")」連續第六年在日本唱片大獎獲得「優秀作品獎」；並連續第六年登上[紅白歌合戰](../Page/第66回NHK紅白歌合戰.md "wikilink")，第二次以紅組身份演出。

### 2016年：首次巨蛋演唱會

AAA於元旦晚上舉行「AAA NEW YEAR PARTY 2016」。並宣佈於5月展開全國Arena巡迴演唱會「AAA ARENA TOUR
2016 -LEAP
OVER-」。成員[與真司郎及後在網誌上表示將會到美國留學](../Page/與真司郎.md "wikilink")，但會定期往返日本繼續AAA的活動，否認會因而退出組合。

2016年初，AAA以成員個人活動為主。西島隆弘出演[富士電視台](../Page/富士電視台.md "wikilink")「[月9](../Page/月9.md "wikilink")」連續劇『[追憶潸然](../Page/追憶潸然.md "wikilink")』，並發行原創專輯《[HOCUS
POCUS](../Page/HOCUS_POCUS_\(西島隆弘專輯\).md "wikilink")》；末吉秀太於3月舉行首個單獨Live；女子組成員宇野實彩子和伊藤千晃於3月31日舉行首個單獨Fans
Meeting。

AAA於6月8日發行第51張單曲《[NEW](../Page/NEW_\(AAA單曲\).md "wikilink")》

AAA今年8月即將二度舉辦亞洲巡迴演唱會「AAA ASIA TOUR 2016」，落實於新加坡和台灣舉行公演\[43\]。

6月3日，AAA宣佈在11月12日和13日於[京瓷巨蛋大阪舉行出道以來首次巨蛋演唱會](../Page/大阪巨蛋.md "wikilink")「AAA
Special Live 2016 in Dome -FANTASTIC
OVER-」，及後於7月24日宣佈在11月16日追加[東京巨蛋公演](../Page/東京巨蛋.md "wikilink")

第52張單曲《[沒有眼淚的世界](../Page/沒有眼淚的世界.md "wikilink")》於10月5日發行。女子組成員宇野實彩子和伊藤千晃宣佈以『MisaChia』名義，在2017年3月舉行單獨巡迴演唱會「MisaChia
1st Tour ～Jewel In the Night～」

11月16日（AAA Special Live 2016 in Dome -FANTASTIC OVER-
最終場），宣佈第53張單曲於2017年2月8日發行；而第十一張原創專輯於兩週後（2月22日）發行。

AAA憑「[沒有眼淚的世界](../Page/沒有眼淚的世界.md "wikilink")」連續第七年在日本唱片大獎獲得「優秀作品獎」；並連續第七年登上[紅白歌合戰](../Page/第67回NHK紅白歌合戰.md "wikilink")

### 2017年：WAY OF GLORY

AAA於元旦晚上舉行「AAA NEW YEAR PARTY
2017」。早前宣佈發行的第53張單曲為《[MAGIC](../Page/MAGIC_\(AAA單曲\).md "wikilink")》，是[朝日電視台電視劇](../Page/朝日電視台.md "wikilink")『奪い愛、冬』的主題曲；第十一張原創專輯亦定名為《[WAY
OF GLORY](../Page/WAY_OF_GLORY.md "wikilink")》

1月12日，伊藤千晃宣佈與圈外男友結婚並懷孕3個月，經多番考慮，決定於3月末從AAA畢業。先前決定舉行的演唱會「MisaChia 1st
Tour ～Jewel In the Night～」亦中止了。

4月17日，AAA宣佈在9月舉行出道以來首次巨蛋巡迴演唱會「AAA DOME TOUR 2017 -WAY OF GLORY-」

5月2日，末吉秀太宣佈以「shuta sueyoshi」名義展開個人活動，以配信形式發行個人單曲「Switch」。

7月5日，發行的第54張單曲《[No Way
Back](../Page/No_Way_Back.md "wikilink")》，此單曲為6人體制後的第一張單曲。

第55張單曲《[LIFE](../Page/LIFE_\(AAA單曲\).md "wikilink")》於10月18日發行，是[富士電視台月](../Page/富士電視台.md "wikilink")9電視劇『[民眾之敵～在這世上，不是很奇怪嗎！？～](../Page/民眾之敵～在這世上，不是很奇怪嗎！？～.md "wikilink")』的主題曲。AAA憑「LIFE」連續第八年在[日本唱片大獎獲得](../Page/日本唱片大獎.md "wikilink")「優秀作品獎」。

12月27日，宇野實彩子宣佈在2018年2月14日展開個人活動，並發行個人出道單曲「你為什麼這樣愛我」。

### 2018年: FAN FUN FAN, COLOR A LIFE

踏入2018年，AAA於元旦晚上舉行「AAA NEW YEAR PARTY 2018」。並宣佈2018年團體首波計劃 - 巡迴粉絲見面會《AAA
FAN MEETING ARENA TOUR 2018 ～FAN FUN FAN～》

2018年，AAA以成員個人活動為主。

  - 西島隆弘以「Nissy」名義舉行多場《Nissy Entertainment
    Live》個人演唱會；4月首次以個人身份登上[東京巨蛋作演唱會](../Page/東京巨蛋.md "wikilink")。
  - 宇野實彩子正式展開個人活動。2018年發行單曲《你為什麼這樣愛我》和《Summer
    Mermaid》；並在10月舉行首個個人巡迴演唱『UNO MISAKO
    LIVE TOUR 2018-2019 "First love"』
  - 相隔1年零3個月，浦田直也於7月25日發行新專輯《unbreakable》；並在12月舉行個人巡迴演唱會『urata naoya
    LIVE TOUR 2018-2019 -unbreakable-』
  - 日高光啓以「SKY-HI」名義舉行多場個人演唱會，亦發行單曲《Snatchaway/Diver's
    High》和新專輯《JAPRISON》。
  - 與真司郎為慶祝自已踏入30歲，宣佈一連串《30th Anniversary
    Year》計劃。11月26日（生日正日）發行首張專輯《THIS
    IS WHO I AM》；並在2019年2月舉行個人巡迴演唱會『SHINJIRO ATAE Anniversary Live「THIS
    IS WHO I AM」』
  - 末吉秀太發行首張專輯《JACK IN THE BOX》，並舉行個人巡迴演唱會『Shuta Sueyoshi LIVE TOUR 2018
    -JACK IN THE BOX-』。2018年8月30日，末吉秀太獲邀與[DA
    PUMP主唱ISSA一起演唱](../Page/DA_PUMP.md "wikilink")「[假面騎士ZI-O](../Page/假面騎士ZI-O.md "wikilink")」的主題曲《Over
    "Quartzer"》；亦是相隔11年再為《[假面騎士系列](../Page/假面騎士系列.md "wikilink")》演唱主題曲。

5月26日（AAA FAN MEETING ARENA TOUR 2018 ～FAN FUN FAN～
初日），AAA宣布第十二張原創專輯於8月29日發行，定名為《[COLOR A
LIFE](../Page/COLOR_A_LIFE.md "wikilink")》。並在9月舉行巨蛋巡迴演唱會「AAA DOME TOUR
2018 -COLOR A LIFE-」

11月7日，宣佈新曲《[笑臉的循環](../Page/笑臉的循環.md "wikilink")》為[NHK節目](../Page/NHK.md "wikilink")「」2018年12月份至2019年1月份放送歌曲之一；此曲往後更成為AAA的第56張單曲，於2019年1月9日發行。

12月21日，首次登上[朝日電視台年末特別音樂節目](../Page/朝日電視台.md "wikilink")「[MUSIC STATION
SUPER
LIVE](../Page/MUSIC_STATION_SUPER_LIVE.md "wikilink")」，並演唱《[DEJAVU](../Page/COLOR_A_LIFE.md "wikilink")》。

## 團體及藝術性

### 印象與管理職能

AAA誕生於代表“avex系”招牌的[愛貝克思娛樂音樂事業本部第](../Page/愛貝克思娛樂.md "wikilink")4製作部，該部擁有直接向高層提出製作企劃的自主權，屬於愛貝克思社長的直系藝人，與[TRF](../Page/TRF.md "wikilink")、[濱崎步](../Page/濱崎步.md "wikilink")、[大無限樂團](../Page/大無限樂團.md "wikilink")、[大塚愛](../Page/大塚愛.md "wikilink")、[矢島美容室等藝人同門](../Page/矢島美容室.md "wikilink")\[44\]。2003年到2008年，由一手培養出大無限樂團、大塚愛等音樂新人的時任部長原田淳及其屬下部員全權負責了成員的選員、簽約、培養，以及作品的選材、合作、製作等成長期內容\[45\]。AAA在出道不滿4個月時獲得2005年[日本唱片大獎最佳新人獎](../Page/日本唱片大獎.md "wikilink")，成為與[冰川清志](../Page/冰川清志.md "wikilink")、[w-inds.](../Page/w-inds..md "wikilink")、[中島美嘉](../Page/中島美嘉.md "wikilink")、[一青窈](../Page/一青窈.md "wikilink")、[大塚愛](../Page/大塚愛.md "wikilink")、[絢香](../Page/絢香.md "wikilink")、[傑洛](../Page/傑洛_\(歌手\).md "wikilink")、[℃-ute](../Page/℃-ute.md "wikilink")、[BIGBANG並列的](../Page/BIGBANG.md "wikilink")[2000年代十位最佳新人](../Page/2000年代.md "wikilink")，也是2004年得主大塚愛以降連續出自原田淳的第二位最佳新人\[46\]。

AAA全員由相關愛貝克思甄選會（Avex Audition）的制作統籌大塚賢二評審合格，被愛貝克思官方認定為第一屆全國甄選會“Avex
Dream”合格者[倖田來未以降的第二代甄選代表](../Page/倖田來未.md "wikilink")。出道前經過愛貝克思藝術學院（avex
artist
academy）對聲樂、舞蹈、表演、綜藝、時尚在內的[練習生課程](../Page/練習生.md "wikilink")，以[流行樂團的](../Page/流行樂團.md "wikilink")[歌手及](../Page/歌手.md "wikilink")[舞者素養為基礎](../Page/舞者.md "wikilink")，同時掌握[演員的演技](../Page/演員.md "wikilink")、[模特兒的外表包裝](../Page/模特兒.md "wikilink")、[主持人的口才](../Page/主持人.md "wikilink")、製作人的自我企劃，以及部分成員掌握的[節奏口技](../Page/節奏口技.md "wikilink")、[芭蕾舞](../Page/芭蕾舞.md "wikilink")、[霹靂舞](../Page/霹靂舞.md "wikilink")、[英語](../Page/英語.md "wikilink")、[藝術體操等](../Page/藝術體操.md "wikilink")[達人特技](../Page/達人.md "wikilink")。

AAA音樂活動的團長是[浦田直也](../Page/浦田直也.md "wikilink")，通常由浦田負責在開場前檢查舞台基礎佈局，並代表或帶領團體致開幕詞、謝幕詞。簽約[伊藤洋華堂之後的時尚活動團長則是](../Page/伊藤洋華堂.md "wikilink")[伊藤千晃](../Page/伊藤千晃.md "wikilink")\[47\]\[48\]。出道當時由[愛貝克思娛樂的白鳥輝知擔任](../Page/愛貝克思娛樂.md "wikilink")[經紀人](../Page/經紀人.md "wikilink")，負責管理團體各方面的事務。2008年三週年演唱會，白鳥與另一位經紀人五十嵐宣佈卸任，2009年AAA的管理事務亦轉到[愛貝克思管理](../Page/愛貝克思管理.md "wikilink")，期間的經紀人有西岡遼平。截至2013年巡回演唱會，管理主管在內一共四位經紀人，主要負責演出現場的是金井佐季巳。\[49\]

### 音樂風格

#### 關於選曲、定調、主唱制

AAA成員的麥克風編號從一號到八號分別是[西島隆弘](../Page/西島隆弘.md "wikilink")、[宇野實彩子](../Page/宇野實彩子.md "wikilink")、[浦田直也](../Page/浦田直也.md "wikilink")、[日高光啟](../Page/日高光啟.md "wikilink")、[後藤友香里](../Page/後藤友香里.md "wikilink")、[伊藤千晃](../Page/伊藤千晃.md "wikilink")、[與真司郎](../Page/與真司郎.md "wikilink")、[末吉秀太](../Page/末吉秀太.md "wikilink")。團體開始活動到出道三週年期間，主要由西島、宇野、浦田三人主唱、日高負責自己作詞的說唱，後藤等其他成員則主要負責合唱和伴舞。出道開始亦會男、女成員分組，或其他不同性別、不同成員、不同人數的混搭主唱。
隨著出道四週年之後的伊藤，出道六周年之後的與、末吉逐渐確立主唱位置，AAA在出道七周年開始進入全員主唱體制。

全員演唱曲目的開場並不固定，如出道單曲《[BLOOD on
FIRE](../Page/BLOOD_on_FIRE.md "wikilink")》、3輯先行《[Red
Soul](../Page/Red_Soul.md "wikilink")》、5輯先行《[Heart and
Soul](../Page/Heart_and_Soul_\(AAA單曲\).md "wikilink")》說唱的日高，或多人合唱，如2輯先行《[Samurai
heart
-侍魂-](../Page/Black_&_White_\(AAA單曲\).md "wikilink")》合唱的與和末吉，7輯先行《[777
～We can sing a
song\!～](../Page/777_～We_can_sing_a_song!～.md "wikilink")》領唱的伊藤。第1節主歌通常開始於西島，因而全曲通常亦都從西島開始，如4輯《[啟程之歌](../Page/啟程之歌_\(AAA單曲\).md "wikilink")》、8輯《[戀歌與雨天](../Page/戀歌與雨天.md "wikilink")》、9輯《[說再見之前](../Page/說再見之前.md "wikilink")》、10輯《[深愛著，卻不能愛](../Page/深愛著，卻不能愛.md "wikilink")》等先行。宇野開場的曲目，通常還會獨唱全曲最高音，如6輯先行《[重要的事](../Page/重要的事.md "wikilink")》。

出道開始[松浦勝人擔任最高製作人](../Page/松浦勝人.md "wikilink")，負責最終審批提案。實際全方位負責選曲、主唱分配等工作的則是[原田淳](../Page/原田淳.md "wikilink")，當時AAA自我主導的作品較少，直到13單《[Get
啾\! /
SHE的真相](../Page/Get_啾!_/_SHE的真相.md "wikilink")》起的3輯作品開始全面自我規劃，對和聲與主聲的關係也有了考量。\[50\]原田淳卸任之後，誕生了如日高提案命名的4輯、6輯、9輯，又如2012年在對2013年預訂單曲《[PARTY
IT
UP](../Page/PARTY_IT_UP.md "wikilink")》選擇B面曲的會議上被宇野否決，最後改成8輯主打曲的《[戀歌與雨天](../Page/戀歌與雨天.md "wikilink")》。2015年收錄在10輯的《Distance》，則是經[朝日電視台節目](../Page/朝日電視台.md "wikilink")《musicる
TV》的百萬連發音樂作家塾，首次向公眾募曲並篩選而來。

#### 主題與價值觀

出道之前，AAA最初是經直接接手自己的[原田淳和](../Page/原田淳.md "wikilink")[富樫明生兩位](../Page/富樫明生.md "wikilink")[音樂製作人按照](../Page/音樂製作人.md "wikilink")[搖滾音樂和](../Page/搖滾音樂.md "wikilink")[嘻哈音樂方向組建和培養](../Page/嘻哈音樂.md "wikilink")，演唱會通常配有伴奏樂隊，因而較早創建的各語言維基百科有將AAA定義為嘻哈組合和樂隊的情況。出道之後，AAA通常會與不同的[作詞](../Page/作詞.md "wikilink")、[作曲](../Page/作曲.md "wikilink")、[編曲作者合作不同](../Page/編曲.md "wikilink")[音樂風格的作品](../Page/音樂風格.md "wikilink")。

早期的[歌詞多是帶有積極樂觀情緒的校園或夏季主題](../Page/歌詞.md "wikilink")，如[石田衣良的](../Page/石田衣良.md "wikilink")《[Shalala
希望之歌](../Page/Shalala_希望之歌.md "wikilink")》、[真島昌利的](../Page/真島昌利.md "wikilink")《[颶風·莉莉，波士頓·瑪麗](../Page/颶風·莉莉，波士頓·瑪麗.md "wikilink")》、[小森田實的](../Page/小森田實.md "wikilink")《[啟程之歌](../Page/啟程之歌_\(AAA單曲\).md "wikilink")》、[GReeeeN的](../Page/GReeeeN.md "wikilink")《[彩虹](../Page/彩虹_\(AAA單曲\).md "wikilink")》。偶爾亦涉及[性與](../Page/性.md "wikilink")[暴力等成年人題材](../Page/暴力.md "wikilink")。受眾以初中生、高中生為中心，特別是面向年輕女性的[青少年流行音樂](../Page/青少年流行音樂.md "wikilink")。

2009年，[愛貝克思集團社長](../Page/愛貝克思集團.md "wikilink")[松浦勝人開始親自為AAA監製單曲及專輯](../Page/松浦勝人.md "wikilink")，曲风逐漸偏向運用混音器材的[電子音樂](../Page/電子音樂.md "wikilink")。如採用同一曲調，但配上不同的編曲與歌詞，最終制作成不同版本收錄的單曲作品《[Hide-away](../Page/Hide-away.md "wikilink")》。

2010年，打造过[TRF](../Page/TRF.md "wikilink")、[globe等团体的音樂製作人](../Page/globe.md "wikilink")[小室哲哉借與AAA合作的機會復出](../Page/小室哲哉.md "wikilink")，製作單曲《[想見你的理由
/ Dream After Dream
～夢醒之夢～](../Page/想見你的理由_/_Dream_After_Dream_～夢醒之夢～.md "wikilink")》，這之後的單曲及專輯基本都是由小室哲哉提供樂曲及監製，曲風偏向1990年代的[日本流行音樂](../Page/日本流行音樂.md "wikilink")。

2012年的單曲《[Still Love
You](../Page/Still_Love_You.md "wikilink")》開始，AAA回歸到早期時候，與不同的作曲人合作，並嘗試不同的音樂風格。
歌詞內容則開始以表現內心情感細節的愛情題材，特別是失戀題材為主，受眾更多地從青少年向成年人群體延伸。

2013年發行了總結過往抒情作品的概念專輯《[抒情精選](../Page/Ballad_Collection.md "wikilink")》，AAA的音樂作品開始偏向中板及慢曲風格，如《[Miss
you](../Page/Miss_you_/_綻放笑容的場所.md "wikilink")》、《[戀歌與雨天](../Page/戀歌與雨天.md "wikilink")》、《[Love](../Page/Love_\(AAA單曲\).md "wikilink")》、《[微風徐徐的夏日記憶](../Page/Wake_up!.md "wikilink")》、《[說再見之前](../Page/說再見之前.md "wikilink")》、《[Lil'
Infinity](../Page/Lil'_Infinity.md "wikilink")》、《[深愛著，卻不能愛](../Page/深愛著，卻不能愛.md "wikilink")》。

#### 編曲

AAA從第一首原創歌曲《[Friday
Party](../Page/Friday_Party.md "wikilink")》開始，編曲基本使用前奏，第1節和第2節分別的主歌、前副歌、副歌，以及獨奏、間奏、第3節最後副歌、結尾伴奏的[日本流行音樂傳統](../Page/日本流行音樂.md "wikilink")[編曲形式進行](../Page/編曲.md "wikilink")，並會在主調（主音）之下配以副調（和音），如《[我的憂鬱與不開心的她](../Page/我的憂鬱與不開心的她.md "wikilink")》。

通常前奏為純伴奏，間奏為說唱，如《[明日之光](../Page/明日之光.md "wikilink")》，或技巧音、特效音，如純清唱或半清唱的《[Lil'
Infinity](../Page/Lil'_Infinity.md "wikilink")》、哼唱的《[Flavor of
kiss](../Page/Flavor_of_kiss.md "wikilink")》，或重複同一句歌詞的《[LOVER](../Page/LOVER.md "wikilink")》。前奏有時亦會配以說唱，如出道作品《[BLOOD
on FIRE](../Page/BLOOD_on_FIRE.md "wikilink")》，或原調、降調的副歌，如《[I'll be
there](../Page/I'll_be_there_\(AAA單曲\).md "wikilink")》。

主歌部分曾在早期的《[美麗的天空](../Page/美麗的天空.md "wikilink")》嘗試全說唱的方式進行，而後直接進入副歌的高音部分。高潮通常會出現在副歌，特別是第3節的最後副歌當中，或用獨唱、說唱配以與主調形成反差的副調獨奏來單獨表現，如《[深愛著，卻不能愛](../Page/深愛著，卻不能愛.md "wikilink")》、《[GAME
OVER?](../Page/GAME_OVER?.md "wikilink")》。

出道早期以中國[銅鑼開場的作品](../Page/銅鑼.md "wikilink")《[DRAGON
FIRE](../Page/DRAGON_FIRE.md "wikilink")》開始，偶爾亦會融入古典器樂或民族元素的和絃，如日本風的《[Samurai
heart
-侍魂-](../Page/Black_&_White_\(AAA單曲\).md "wikilink")》、阿拉伯風的《[MIRAGE](../Page/MIRAGE_\(AAA單曲\).md "wikilink")》、中國風的《[戀歌與雨天](../Page/戀歌與雨天.md "wikilink")》以及西洋交響樂的《[微風徐徐的夏日記憶](../Page/Wake_up!.md "wikilink")》、《[說再見之前](../Page/說再見之前.md "wikilink")》。

### 表演風格

AAA早期的演出通常會根據[音樂風格編排](../Page/音樂風格.md "wikilink")[舞蹈](../Page/舞蹈.md "wikilink")、[劇情](../Page/劇情.md "wikilink")，還會融入部分[芭蕾](../Page/芭蕾.md "wikilink")、[雜技](../Page/雜技.md "wikilink")、[體操等各种不同的次要元素](../Page/體操.md "wikilink")，如2006年的首次巡回演唱會《ATTACK》裡，依次由[日高光啓在融入雜技元素作品](../Page/日高光啓.md "wikilink")《[哈利路亞](../Page/哈利路亞_\(AAA單曲\).md "wikilink")》的說唱開場，期間會有日高被其他四名男成員拋向空中的特技，開場曲目之後會在[與真司郎](../Page/與真司郎.md "wikilink")、[伊藤千晃和觀眾互動下進入話劇環節](../Page/伊藤千晃.md "wikilink")。\[51\]

話劇之後繼續表演朝氣型的作品，期間會有一個接一個類似[浦田直也展露高音唱功](../Page/浦田直也.md "wikilink")、[末吉秀太連續後空翻的魄力型演出](../Page/末吉秀太.md "wikilink")。中場的部分以男女二人對唱抒情歌曲、男成員的舞蹈秀、男成員表演力量型舞蹈歌曲、女成員表演可愛型舞蹈歌曲的方式進行分組演出；後半場則用融入武術風格雜技的《[DRAGON
FIRE](../Page/DRAGON_FIRE.md "wikilink")》等作品活躍氣氛，最後以安可曲目結束。\[52\]

AAA亦有一些會引起觀眾，特別是女性觀眾尖叫的男女成員親密演出，例如2007年9月在[日本武道館召開出道兩週年演唱會](../Page/日本武道館.md "wikilink")，當表演到艷色風格作品《[Paradise
Paradise](../Page/AROUND_\(AAA專輯\)#Paradise_Paradise.md "wikilink")》，相應的出現了[伊藤千晃騎在仰面躺下的](../Page/伊藤千晃.md "wikilink")[西島隆弘腰腹部前後舞動的性感舞蹈](../Page/西島隆弘.md "wikilink")；表演到純愛題材作品《[邂逅的力量Ⅱ](../Page/AROUND_\(AAA專輯\)#邂逅的力量Ⅱ.md "wikilink")》的時候，出現了西島和[宇野實彩子告白的劇情](../Page/宇野實彩子.md "wikilink")。\[53\]

有時亦會在表演特定音樂作品時與觀眾進行互動，如在《[颶風·莉莉，波士頓·瑪麗](../Page/颶風·莉莉，波士頓·瑪麗.md "wikilink")》一起揮舞應援毛巾。七名成員的暱稱，如西島「Nissy」、宇野「Uno
Chan」、浦田「Naoya」、日高「Hidaka」、與「Shinjiro」、末吉「Shuta」、伊藤「Chiaki」，亦會在演唱[TRF的](../Page/TRF.md "wikilink")《[CRAZY
GONNA
CRAZY](../Page/CRAZY_GONNA_CRAZY.md "wikilink")》時由非演唱成員用來與觀眾一起為正在唱的成員助威。

2007年[後藤友香里退出](../Page/後藤友香里.md "wikilink")，體操元素不再表現，之後隨著AAA的作品向成熟風格方向發展，激烈的舞蹈或雜技亦淡出舞台，逐漸專注歌唱，劇情則主要作為作品前後的開場或結束使用。而作品演出的前後或中途所進行綜藝及脫口秀環節，則開始成為正式獨立的環節，並與演出環節分軌收錄在演唱會錄像帶，例如初試的《[AAA
TOUR 2012 -777- TRIPLE
SEVEN](../Page/AAA_TOUR_2012_-777-_TRIPLE_SEVEN.md "wikilink")》。

通常AAA的單曲和專輯主打曲都會根據歌曲主題拍攝[音樂影片](../Page/音樂影片.md "wikilink")，如收錄3首夏季主題歌曲的細碟《[夏日風物](../Page/夏日風物.md "wikilink")》，或早期曾嘗試戲劇化的《[我們的手](../Page/ATTACK_\(AAA專輯\).md "wikilink")》和《[最後的那句話](../Page/AROUND_\(AAA專輯\).md "wikilink")》。2012年的《[WISHES](../Page/SAILING.md "wikilink")》開始，偶爾會用花絮視頻剪輯製成。2015年的七張連續單曲，AAA嘗試了完全[沙畫](../Page/沙畫.md "wikilink")、16位元[電腦成像等表現形式](../Page/電腦成像.md "wikilink")，以及[三維投影的主打曲](../Page/三維投影.md "wikilink")《STORY》AAAR演唱會。

AAA從出道開始，陸續在[展演空間](../Page/展演空間.md "wikilink")、[演播大廳](../Page/多功能大廳.md "wikilink")、[競技場等不同規模和結構的場地舉辦演唱會](../Page/競技場.md "wikilink")，舞台元素亦會隨之做出調整，如2015年首次亞洲巡迴期間，場館較大的香港站採取了異於新加坡首站的開場燈光，又如[富士急高原樂園首次戶外演唱會](../Page/富士急高原樂園.md "wikilink")，舞台被設計成歐洲宮殿模樣，男女成員則身穿[軍服和](../Page/軍服.md "wikilink")[禮服](../Page/禮服.md "wikilink")，以童話世界的王子與公主為主題開場。\[54\]

### 影響評價

[愛貝克思官方網站的介紹是](../Page/愛貝克思.md "wikilink")「**超級表演組合**」（Super performance
Unit／スーパーパフォーマンスグループ）。\[55\]\[56\]愛貝克思台灣在2015年亞洲巡回期間的介紹是「**日本最強男女組合**」。\[57\]AAA自2005年開始，每年固定參加[a-nation](../Page/a-nation.md "wikilink")。2010年開始連續獲得[日本唱片大賞的優秀作品賞](../Page/日本唱片大賞.md "wikilink")，並登上NHK的[紅白歌合戰](../Page/紅白歌合戰.md "wikilink")，期間基本是以佔成員多數的男歌手，也就是白組名義。2013年時，NHK總導演表示想借用女成員來阻止紅組的收視率劣勢，於是這年是以女歌手名義出場。

2006年，AAA在首張專輯《ATTACK》推出了[西島隆弘和](../Page/西島隆弘.md "wikilink")[宇野實彩子配對主唱](../Page/宇野實彩子.md "wikilink")、[末吉秀太合唱的](../Page/末吉秀太.md "wikilink")《[邂逅的力量](../Page/ATTACK_\(AAA專輯\)#邂逅的力量.md "wikilink")》，並推出了後續的[II](../Page/AROUND_\(AAA專輯\).md "wikilink")、[Ⅲ](../Page/depArture.md "wikilink")，成為一個相識、相戀、相離主題的系列組曲。2010年秋，[Uta-Net為紀念網站成立](../Page/Uta-Net.md "wikilink")10週年，以「**有助戀愛的歌曲**」為題徵集線上投票，最後該曲作為12首之一收錄於2011年發行的合集《全輯為戀而歌～歌詞傳說～》，留言的情侶們透露，他和她是在[卡啦OK對唱](../Page/卡啦OK.md "wikilink")「此時此刻，倘若一言不語，就此分別。或許，你我再難相遇…」這句歌詞時「從朋友走到戀人」的傳說故事。\[58\]
2012年，該作又在推出約7年之後當選日本各大唱片公司聯合直營手機下載網站[RecoChoku的](../Page/RecoChoku.md "wikilink")「**聽在戀愛之初的樂曲**」第3位。\[59\]2014年播出的綜藝節目《[AKB大調查](../Page/AKB大調查.md "wikilink")》，AAA被日本歷史單曲銷量最高女歌手[AKB48相关的](../Page/AKB48.md "wikilink")240名成员評選為「到現場观看时，感觉演唱實力最佳的歌手」的第8名，成員[村山彩希評價到](../Page/村山彩希.md "wikilink")：「西島先生和宇野女士這兩個人，不管是合唱，還是獨唱，都很專業」。\[60\]

2011年，為紀念AAA出道6週年和第二張精選集《[\#AAABEST](../Page/#AAABEST.md "wikilink")》發行，[RecoChoku進行了一次為期一週的](../Page/RecoChoku.md "wikilink")「**AAA名曲榜**」的付費投票活動，最後選出的前五位曲目是《MUSIC\!\!\!》《[不屈的心](../Page/不屈的心.md "wikilink")》《Get
チュー\!》《想見你的理由》《Believe own way》，其中前三位曲目的評論指出：

2013年，歌曲《[戀歌與雨天](../Page/戀歌與雨天.md "wikilink")》[音樂影片獲得](../Page/音樂影片.md "wikilink")[YouTube](../Page/YouTube.md "wikilink")9月份的[日本音樂播放量冠軍](../Page/日本音樂.md "wikilink")，評論指出「高潮部分的『我喜歡你』這句歌詞會給人留下深刻的中慢版抒情曲目印象，而發售前關於這首歌有助於戀愛的傳聞亦增加了口碑」。\[61\]\[62\]\[63\]2014年、該作取得[愛貝克思集團與](../Page/愛貝克思集團.md "wikilink")[軟件銀行聯合運營的線上包月付費影音網站](../Page/軟件銀行.md "wikilink")[UULA年度音樂影片第](../Page/UULA.md "wikilink")3位。\[64\]2015年，該作的[歌詞在網站Uta](../Page/歌詞.md "wikilink")-Net累計獲得約120万次配信記錄，成為「百萬抒情歌曲（ミリオンリリック）」歷史上的第22位。\[65\]之後音樂影片在YouTube的播放量亦超過3000萬次，並進入谷歌日本統計的日本本土歷史最高播放量音樂影片前20位，評論指出「搖擺不定的戀愛心情寄託於初秋的雨天，譜寫出一曲悲傷情歌，通過最真摯的戀愛感情唱響，引起年輕女性為中心羣體的共鳴」。\[66\]

## 成員

※以下按官方網站順位排序，詳細信息請查閱成員各自主條目。

<table>
<thead>
<tr class="header">
<th><p>姓名</p></th>
<th><p>平假名記法</p></th>
<th><p>生日</p></th>
<th><p>星座</p></th>
<th><p>年齡</p></th>
<th><p>出身地</p></th>
<th><p>身高</p></th>
<th><p>血型</p></th>
<th><p>代表色</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>現役成員</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/西島隆弘.md" title="wikilink">西島隆弘</a></p></td>
<td></td>
<td></td>
<td><p>天秤座</p></td>
<td></td>
<td><p><a href="../Page/北海道.md" title="wikilink">北海道</a></p></td>
<td><p>170cm</p></td>
<td><p>O型</p></td>
<td><p><strong>橙</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/宇野實彩子.md" title="wikilink">宇野實彩子</a></p></td>
<td></td>
<td></td>
<td><p>巨蟹座</p></td>
<td></td>
<td><p><a href="../Page/東京都.md" title="wikilink">東京都</a></p></td>
<td><p>160cm</p></td>
<td><p>O型</p></td>
<td><p><strong>紫</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/浦田直也.md" title="wikilink">浦田直也</a></p></td>
<td></td>
<td></td>
<td><p>天蠍座</p></td>
<td></td>
<td><p><a href="../Page/東京都.md" title="wikilink">東京都</a></p></td>
<td><p>178cm</p></td>
<td><p>B型</p></td>
<td><p><strong>綠</strong></p></td>
<td><p>團長</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/日高光啓.md" title="wikilink">日高光啓</a></p></td>
<td></td>
<td></td>
<td><p>射手座</p></td>
<td></td>
<td><p><a href="../Page/千葉縣.md" title="wikilink">千葉縣</a></p></td>
<td><p>170cm</p></td>
<td><p>O型</p></td>
<td><p><strong>黃</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/與真司郎.md" title="wikilink">與真司郎</a></p></td>
<td></td>
<td></td>
<td><p>射手座</p></td>
<td></td>
<td><p><a href="../Page/京都府.md" title="wikilink">京都府</a></p></td>
<td><p>170cm</p></td>
<td><p>O型</p></td>
<td><p><strong>藍</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/末吉秀太.md" title="wikilink">末吉秀太</a></p></td>
<td></td>
<td></td>
<td><p>射手座</p></td>
<td></td>
<td><p><a href="../Page/長崎縣.md" title="wikilink">長崎縣</a></p></td>
<td><p>167cm</p></td>
<td><p>A型</p></td>
<td><p><strong>粉紅</strong></p></td>
<td><p>後藤友香里離開前，代表色為黑色。</p></td>
</tr>
<tr class="even">
<td><p>已離開成員</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/後藤友香里.md" title="wikilink">後藤友香里</a></p></td>
<td></td>
<td></td>
<td><p>魔羯座</p></td>
<td></td>
<td><p><a href="../Page/東京都.md" title="wikilink">東京都</a></p></td>
<td><p>161cm</p></td>
<td><p>B型</p></td>
<td><p><strong>粉紅</strong></p></td>
<td><p>退團前官方順位排行第6。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/伊藤千晃.md" title="wikilink">伊藤千晃</a></p></td>
<td></td>
<td></td>
<td><p>魔羯座</p></td>
<td></td>
<td><p><a href="../Page/愛知縣.md" title="wikilink">愛知縣</a></p></td>
<td><p>153cm</p></td>
<td><p>B型</p></td>
<td><p><strong>紅</strong></p></td>
<td><p>因<a href="../Page/結婚.md" title="wikilink">結婚及已</a><a href="../Page/懷孕.md" title="wikilink">懷孕</a>，於2017年3月畢業離團。</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 西島隆弘

出道前曾在一次雜技練習的過程中不慎從高處墜地，致使左腕骨折。 \[67\]

### 浦田直也

出道前自述在練習前、攝影前、練習的休息時間必食巧克力，是最年長的成員。\[68\]

### 宇野實彩子

出道前曾在側翻雜技練習的助跑過程中一頭栽倒，自述運動神經不佳。 \[69\]

### 日高光啓

出道前曾因對朋友炫耀鬍子時被對方說他不適合留鬍子，而為此苦惱。 \[70\]

### 與真司郎

出道前自述雖然是唯一的關西人，但沒有搞笑天賦，擅長的則是舞蹈。 \[71\]

### 後藤友香里

出道前以介紹成員特點為目的在官方博客寫下了七篇《後藤醫生診療所（Dr.ゴトー診療所）》，曾自述是臉色最差的成員。 \[72\]

### 末吉秀太

出道前從生活18年的家鄉長崎搬到東京，曾因改不掉長崎口音而苦惱。 \[73\]

### 伊藤千晃

出道前自述是稍顯圓潤的體型，喜歡食雞胸肉，飼養有一隻名叫“仁”的雄性長毛吉娃娃。\[74\]還有一只名叫“姬”的雌性長毛吉娃娃。\[75\]


## 軼事

### 工作生活

#### 舞台幕後

  - [西島隆弘在](../Page/西島隆弘.md "wikilink")2005年6月的雜技練習課手腕骨折以後，一直帶傷堅持活動至2006年10月取出鋼釘。\[76\]2010年巡迴演唱會過程期間，足部受傷，需要3個月時間康復，但沒有治療。\[77\]
  - 西島隆弘在2014年與團體出演TBS節目「A-Studio」時候表示自己有過十幾次、甚至上百次擺脫AAA，辭去AAA，或AAA解散掉的想法；曾認為團體會在5週年終結，也和成員們一起考慮過這件事，但長時間在一起的經歷已經讓成員們緊緊粘在一起。\[78\]
  - 西島隆弘在2011年綜藝節目「踊る！さんま御殿\!\!」透露說，團體成員出道早期的1、2年里每月收入只有3萬日元。\[79\]
  - [伊藤千晃在](../Page/伊藤千晃.md "wikilink")2010年因病缺席巡回演唱會的ameba
    pigs特輯，男成員表示並不把宇野實彩子當作是女成員。\[80\]
  - [末吉秀太在](../Page/末吉秀太.md "wikilink")2014年10月突發扁桃腺潰瘍入院接受喉嚨手術，治癒。\[81\]
  - 西島隆弘與末吉秀太在2006年廣播節目《A～御話》的合作主持板塊透露兩人曾在公司發生爭執，當時末吉按倒了西島，但被日高光啓制止，後來西島表示「現在說不定和自己關係最好的就是末吉」。\[82\]
  - 浦田直也和伊藤千晃在2011年2月做客uta-net專訪，宣傳專輯《引領風潮》和單曲《重要的事》，兩人透露日高光啟最關心社交網站和網路的最新熱門情報，其他成員之間也會互相交流。\[83\]

#### 藝人關係

  - AAA在2007年召開了男成員單獨的演唱會，當時募集了5名和成員相似的兒童演員作為嘉賓出演，分別是雨宮翔（）、穴戶多聞（）、安藝雄太朗（）、森田真弥（）和戶石悠太（），並參加了AAA在2008年的巡迴演唱會。2009年3月5日的2009年巡迴演唱會東京站，又加入兩名女成員久保玲奈和傳彩夏，分別扮演宇野實彩子和伊藤千晃。
  - 2012年，搞笑藝人組合[X-GUN](../Page/X-GUN.md "wikilink")、男子團體[Da-iCE](../Page/Da-iCE.md "wikilink")、男女團體[Dream5的高野洸和模特](../Page/Dream5.md "wikilink")[西田有沙也陸續參與了AAA的巡回演唱會](../Page/西田有沙.md "wikilink")。其中[X-GUN的西尾季隆在此後又連續作為巡演期間的綜藝節目記者](../Page/X-GUN.md "wikilink")，與愛好者和成員互動，他也曾在2008年電視劇《新世紀莎士比亞》的DVD與AAA有過合作。

### 公共交流

2005年6月1日，成員[西島隆弘在AAA官方網站的](../Page/西島隆弘.md "wikilink")「AAA
BLOG」更新了第一篇可自由瀏覽的網路日誌，之後依次由[後藤友香里](../Page/後藤友香里.md "wikilink")、[與真司郎](../Page/與真司郎.md "wikilink")、[浦田直也](../Page/浦田直也.md "wikilink")、[日高光啟](../Page/日高光啟.md "wikilink")、[末吉秀太](../Page/末吉秀太.md "wikilink")、[伊藤千晃](../Page/伊藤千晃.md "wikilink")、[宇野實彩子每天更新一篇](../Page/宇野實彩子.md "wikilink")，開始分享團體或個人的工作及生活。2005年7月1日，AAA工作組（AAA
staff）開始不定期更新工作人員視角的AAA日誌。\[84\]11月22日，AAA註冊了活力門部落格（），依次由西島、末吉、伊藤更新了3篇，之後停止更新。2006年4月30日，除後藤有涉及自己從高中畢業的日誌\[85\]，官方網站刪除了2006年4月1日之前更新的其他全部內容，並以更新者來對日誌進行了二級分類。2007年8月1日，因[世界自然遺產塗鴉問題停止更新](../Page/AAA_\(團體\)#世界自然遺產塗鴉.md "wikilink")。12月31日，AAA工作組更新了AAA拍攝單曲《[海市蜃樓](../Page/MIRAGE_\(AAA單曲\).md "wikilink")》的照片。

2008年1月1日，成員宇野帶領團體做了「AAA
BLOG」正式恢復的公告，並紀念2008年1月9日發行的單曲《海市蜃樓》，而公開形式則變更為最新發佈日誌可自由瀏覽；1月2日，西島更新了恢復之後的第一篇正式日誌；更新順序從星期一到星期日分別是日高、宇野、西島、浦田、伊藤、末吉、與。2010年2月22日，為紀念「AAA
Heart to Heart TOUR 2010」巡迴演唱會開幕，AAA工作組在Ameba註冊了完全自由瀏覽的期間限定日誌（），並由成員按AAA
BLOG的順序更新。 2012年9月13日，為紀念出道七周年，AAA
BLOG開始限制非會員只可自由閱讀標題和開頭文字部分，會員則完全可讀。12月10日，為紀念單曲《[Miss
you](../Page/Miss_you_/_綻放笑容的場所.md "wikilink")》的音樂電影公佈，AAA工作組在Ameba部落格註冊了工作組每天更新一篇的期間限定日誌（），可完全自由閱讀。

AAA成員自2008年12月開始，亦陸續在各類公共平台開設單獨的個人主頁，例如先後註冊社交網路[Ameba更新日誌的浦田](../Page/Ameba.md "wikilink")（）、日高（）、伊藤（）、與（），先後註冊微博客[Twitter個人主頁的日高](../Page/Twitter.md "wikilink")（）、伊藤（）、浦田（）、宇野（）、末吉（）、與（），先後註冊微博客[Instagram分享圖片的伊藤](../Page/Instagram.md "wikilink")（）、浦田（，已銷戶）、日高（）、與（）、宇野（）。

2014年7月2日，成員伊藤開設了面向華人的[新浪微博](../Page/新浪微博.md "wikilink")（），使用中文發佈。而在2010年、2011年，伊藤的父親（），以及两个姐姐奈奈（）及明奈（）亦分別註冊了Twitter，並會與愛好者互動或宣傳AAA。還可通過關注Twitter的AAA工作組（）瞭解團體動態，或關注SKY-HI工作組（）瞭解日高、關注Nissy工作組（）瞭解西島的個人動態。

西島本人未在任何平台開設個人首頁，原因是對電子產品不擅長。

### 商業文化

#### 周邊產業

  - 「」（トリエーパーティ）
      - 為紀念2006年5月31日發行的單曲《[颶風·莉莉，波士頓·瑪麗](../Page/颶風·莉莉，波士頓·瑪麗.md "wikilink")》，基於「AAA與愛好者是夥伴」理念建立的官方後援會，於6月4日開始接受日本本土會員申請，成員宇野實彩子代表。加入會員的愛好者除基本會員特權以外，每年6月、12月還會收到介紹團體及成員舞台幕後的官方會報。2009年12月21日，由應援會延伸的官方手機端網站「（トリエー・モバイル）」建站並接受會員申請。
      - 附屬網站包括用於社羣討論的「」、閱讀成員博客的「」收看YouTube官方影像的「」、優先在[mu-mo購買商品的](../Page/mu-mo.md "wikilink")「（え～ショップ）」
          - 「**AAA Party
            BLOG**」單獨提供會員或非會員向成員伊藤千晃進行提問的平台。\[86\]前身是2005年出道前開始更新的公共博客「**AAA
            BLOG**」，2008年轉為當日更新文章可自由瀏覽的半會員制。2012年轉為只限會員瀏覽，當日更新文章只顯示標題和開頭部分。
          - mu-mo的AAA購物首頁會發行一些會員專屬的作品或周邊，例如將2014年新年演唱會「AAA NEW YEAR
            PARTY 2014」公演和主持環節作為花絮收錄的第40張單曲《[SHOW
            TIME](../Page/SHOW_TIME.md "wikilink")》。
      - 2015年4月1日，全球性的後援會「」建站，並接受海外會員申請，紀念同年4月4日開始的首次亞洲巡回演唱會。
      - 根據2015年8月統計，包括停止續費的會員在內，註冊的實名會員約10萬人左右。
  - 「**A熊猫**」（）
      - AAA成員構思製作並向愛好者徵名的團體官方卡通形象，2009年8月1日在a-nation首次作為發售商品的圖案，當時是以熒光粉色替代黑色部分的白色熊貓，配以熒光黃色的背景色。同時也公布了7名成員代表色替代黑色的白色熊貓圖案。
          - 同年的4週年演唱會期間，首次作為鑰匙鏈大小實體公仔發售。之後陸續推出各種用途，或在主色調外配以其他次要圖案、裝飾物的樣式和款式。
          - 原型是2008年在AAA主演電視劇《新世紀莎士比亞》裡，喜歡熒光色系、特別是熒光粉和熒光黃綠的成員宇野實彩子曾變裝扮演熊貓，當時被成員和愛好者稱作的「**實彩熊貓**（ミサパンダ）」。
      - 2011年巡回演唱會期間，「AAA channel」分別公開了成員西島隆弘與宇野實彩子兩人以《[CRAZY GONNA
        CRAZY](../Page/CRAZY_GONNA_CRAZY.md "wikilink")》為背景音樂編舞的A熊貓舞蹈，以及融合成員七種代表色的新熊貓內部報告影像。
          - 熒光粉色和白色部分被替換為橙色的右耳廓、左腳，紫色的鼻頭、右腳、左耳膜，綠色的右手、右耳膜，黃色的頭部，藍色的鼻翼、身體，粉色的左眼虹膜、左耳廓，紅色的右眼虹膜、左手。
              - 保留兩個臉頰的酒窩和嘴巴為熒光粉色、瞳孔為白色。身體上的AAA從左至右則由綠色A、黃色A、紅色A組成。
          - 工作人員從同年演唱會開始，會身穿這套公仔裝出現在場館附近，或商品發售場地，進行現場通報並與愛好者互動。\[87\]
      - 2013年3月12日至24日，官方購物網站「A購物（え～ショップ ）」的實體店「A購物 in 裏原宿店」（え～ショップ in
        URAHARAJYUKU）在東京都涉谷區神宮前限期營業，特設用於展示A熊貓周邊的「A熊貓的臥室（え～パンダの部屋）
        」，並紀念同日發售的單曲《PARTY IT UP》。\[88\]

#### 流行現象

  - **很贊\! 很贊\! 超絕贊吧\!?**（いーじゃん\!いーじゃん\!スゲーじゃん\!?）
      - 出自AAA DEN-O form名義演唱日本特攝電視劇《假面騎士電王》主題歌之一《[Climax
        Jump](../Page/Climax_Jump.md "wikilink")》的一句歌詞。用於一個人介紹一件東西後的反問。這句歌詞後來還成為一張收錄AAA和配音演員合集的細碟名字。
  - **啾一口？**（チューする？）
      - 出自AAA代言並配唱主題歌《Get
        啾！》的好侍食品果粒吸男女兩版廣告裡西島隆弘和宇野實彩子各自的開場白。該詞隨廣告的熱播，開始流行於日本年輕男女情侶羣體。
  - **AAA風潮**（AAABC）
      - 名稱來自AAA在2011年初發行的專輯和同名巡迴演唱會《引領風潮（Buzz
        Communication）》。AAA在2010年底初次登上紅白歌合戰之後，隔年作為日本流行音樂界代表收到香港和韓國的亞洲音樂節邀請，提名亞洲最佳歌手。而成員宇野實彩子模仿自[AKB48的虛擬團體UNO](../Page/AKB48.md "wikilink")48，亦帶動了愛好者模仿AAA成員生活、舞台的穿著和打扮的[Cosplay潮流](../Page/Cosplay.md "wikilink")，例如名叫天野真隆（）和海野葵两名愛好者，通过模仿西島隆弘和宇野實彩子與AAA愛好者進行合影等互動，以此為契機而陸續在Twitter獲得了過萬名的關注者。\[89\]\[90\]
      - AAA風潮亦作為與AAA有關話題在Twitter的標籤（\#AAABC）填寫。同年發行的精選集《\#AAA極精選》和次年發行的《\#AAA極B面精選》亦用到“\#”這一元素。
  - **戀歌神社**（）
      - 社名出自AAA在2013年秋公開的歌曲《[戀歌與雨天](../Page/戀歌與雨天.md "wikilink")》。當時日本佔卜師富士川碧砂評價該曲「有助於戀愛的歌曲」「戀愛度高漲的歌曲」。基於社會反響，AAA為紀念該曲發行，也為鼓舞更多戀人，而在同年9月2日到9月8日期間的JOL原宿開設了戀歌神社。\[91\]
      - 2014年2月3日，為紀念單曲《Love》發售的「We Love
        AAA展」在涉谷PARCO開展，展示成員宇野實彩子為團體在紅白歌合戰、日本唱片大獎等場合主設計的舞台服裝，並結合戀歌神社、A熊貓和七福神元素特設「え～パンダ神社」用於參展者祈福。
          - 構想的「七福A熊貓（七福え～パンダ）
            」概念包括金錢運（橙）、戀愛運（紫）、工作運（綠）、學業運（黃）、平安運（藍）、健康運（粉）、美貌運（紅），同時追加發售團體成員設計的相關新款熊貓及宇野個人首次製作的香水。\[92\]\[93\]
  - **AAA狀態**（日語：、）
      - 2015年從日本SNS的年輕女性羣體流行開來的一種現實中的理想生活和社會現象，意指像AAA的兩名女成員宇野實彩子和伊藤千晃般，身旁被帥哥包圍起來的公主狀態。\[94\]
      - 前身是2002年DREAM COME
        TRUE的西川隆宏退團，剩下的三人因關係良好的活動狀態而帶動的「夢狀態」「」）。\[95\]
      - 並不限於1對1，也可不以戀愛為目的，核心在於男女之間愉快的友誼。 \[96\]
      - AAA狀態的人會將自己與其他人聚會或吃烤肉、旅行、喝飲品的相片發佈到SNS，並填為標籤。\[97\]

### 爭議話題

#### 公共事件

##### 冒名關係者詐騙案

2006年6月至8月期間，一名28歲左右的女社會無業者**野並玲央**偽裝成AAA的關係者，通過博客認識了另一名23歲左右在[茨城縣筑西市工作的男公司職員](../Page/筑西市.md "wikilink")，該女告訴這名職員「可以引薦你參加AAA的派對」「成員單飛出道需要贊助人。希望你能慷慨解囊」等信息，詐取了該職員的215萬日元。
\[98\]

2007年7月11日，該女在[大阪府淀川区被](../Page/淀川区.md "wikilink")[茨城县警察局筑西署警方面以涉嫌詐騙罪跨縣逮捕](../Page/茨城县.md "wikilink")。

##### 後藤友香里退團

2006年夏季，女成員後藤友香里在日本社交網站[mixi個人帳號爆料了一些私生活嗜好](../Page/mixi.md "wikilink")，並对[愛貝克思集團低廉的薪資水平和時間上的束縛等管理問題表達了不滿](../Page/愛貝克思集團.md "wikilink")，宣稱剩下的1年2個月合同到期之後將不會和公司續約，這些內容被雜誌《BUBKA》隐去全名之後以〈某偶像男女组合的18岁女成员在mixi写下自己逛夜店和性癖好的日记〉为主題報道，並配以臉部打有馬賽克的《[颶風·莉莉，波士頓·瑪麗](../Page/颶風·莉莉，波士頓·瑪麗.md "wikilink")》海報作為背景圖。\[99\]

2007年10月6日，已離隊數月的後藤友香里合約到期正式退社。

##### 世界自然遺產塗鴉

2007年7月25日，女成員[伊藤千晃把AAA出席美國](../Page/伊藤千晃.md "wikilink")[巴爾的摩動漫展](../Page/巴爾的摩.md "wikilink")「OTAKON」歸國之前，留在當地自然公園一塊岩石上的塗鴉照片發布到官方博客進行介紹，當時BBS的愛好者對此並無爭議。同月30日的時候，博客忽然毫無徵兆的遭到大量批判留言，例如批評團體的塗鴉行為是「日本的恥辱」和「不成熟」，要求團體必須向美國人謝罪，並警告團體必須在「謝罪、消去塗鴉」和「解散」之間兩選其一。\[100\]

對於批判，當時AAA的愛好者也留言進行了回擊，例如「可能這件事做的並不好…但還輪不到你們多嘴吧？大媽們？」和「還生氣了？大叔們把自己當什麼了？你們以為自己是世界的王啊？這可不好！」以及「岩石、岩石，煩死人了↓大家的塗鴉明明很好」。\[101\]

就愛好者的反擊，一位使用『**33歲的大媽**（33歳のおばさん）』名義的人物批評此次事件愛貝克思博客管理方面的不成熟，並以「什麽事是好的，什麽事是不好的」為題，開始對愛好者進行說教。該人物對愛好者表示「當你們二十幾歲的時候，想起你們在十幾歲時說的很可愛、很酷這些話的時候，你們會為自己曾經的言行感到羞恥。社會可不是你們想象的那麽幼稚」。\[102\]

2007年8月2日，事件經日本線上新聞網站J-CAST整理刊載，網民的評論數刷新了該站單一報道的記錄，並傳到[中國](../Page/中國.md "wikilink")。就此愛貝克思官方發表聲明，表示塗鴉之前諮詢過當地的導遊，並且噴塗用具是公園專門販賣給有噴塗需要的遊客使用，岩石也是用於遊客噴塗之後拍照的小道具。此外，愛貝克思還用[橫濱市櫻木町的塗鴉舉例](../Page/櫻木町.md "wikilink")。對此，[橫濱市都市整備局用](../Page/橫濱市.md "wikilink")「出現這種情況原非本意」和「從沒鼓勵過櫻木町的塗鴉行為」表達了對愛貝克思舉例的憤慨。8月4日，事件經過[產經新聞等日本主流媒體的報道之後](../Page/產經新聞.md "wikilink")，愛貝克思表示「圖畫事件卻有考慮不周。會進行反省」，但因為沒有觸犯法律，所以不會考慮進行處分。\[103\]

關於是否觸犯法律，媒體披露：根據馬裏蘭州憲法，該行為最小需要支付500美元罰金並拘留60天，最大則需要支付2500美元並處以3年以內的有期徒刑，以及單件塗鴉最高1萬美元的賠償。而巴爾的摩政府官網刊載於2005年的一起類似案件則顯示該地『一年需要花費84萬美元消除這些塗鴉』，政府官網還稱這種行為是搞破壞的「藝術家（artists）」。
\[104\]

2007年8月6日，巴爾的摩當地的日本人來到案發現場進行調查，顯示公園內並無販賣噴塗用具，且發現了其他署名AAA的「日本 AAA
2007.7.22」和「とりぷるえー」兩份塗鴉，使用塗料一致。對於疑問，愛貝克思表示「已經向公園管理方道歉」且「修補事項也在商討」，但拒絕對其他的話題表態。\[105\]

事件過後，启用AAA作為廣告代言人的[好侍食品也陸續收到顧客對於選人不滿和安慰公司選錯人的反饋信](../Page/好侍食品.md "wikilink")。同一時期，[朝日放送報道了一起三名年輕人因塗鴉而被判刑的新聞也牽連到AAA](../Page/朝日放送.md "wikilink")，援引安永健次法官的批評，網民表示「也請對AAA進行裁決」「也請這名法官對AAA進行教育」。
\[106\]

2007年8月13日，AAA以「AAA（伊藤千晃）」名義登上了偵探FILE抽取調查的「令人無奈」的藝人第二位，評論認為AAA令人無奈的原因是此前一直沒有什麼名聲，卻因為缺乏常識的圖畫而一下子成為全國焦點。\[107\]

##### 愛好者“施女”詐騙案

2008年底到2009年期間，台灣一名網路暱稱「Sammi0914」的台北市施姓無業女子假借「有日本朋友幫忙」代抽演唱會門票、代買周邊商品等名義，陸續從各路追星族手中收得累計愈80萬新臺幣的贓款，每次得手之後先以各種理由推脫，隨即失蹤。

2009年10月22日，與該女結識在一次[傑尼斯事務所歌迷見面會](../Page/傑尼斯事務所.md "wikilink")，而後被騙走30萬新臺幣的劉姓女子向警方報案，並自發在FMB免費留言板創建「施女詐騙受害者互助留言板」與其他受害者交流被騙經驗。\[108\]而劉姓女子在110mb創建的一個防詐騙公告正文提醒指出：

  - 該女曾是[KinKi
    Kids成員](../Page/KinKi_Kids.md "wikilink")[堂本剛和](../Page/堂本剛.md "wikilink")[w-inds.成員](../Page/w-inds..md "wikilink")[橘慶太愛好者](../Page/橘慶太.md "wikilink")，後來成為AAA成員[西島隆弘的狂熱愛好者](../Page/西島隆弘.md "wikilink")，所獲贓款全部用於個人出國追星和玩樂、購物，其中2009年3月、6月、9月三次赴日本參加AAA的演唱會等相關活動。\[109\]
  - 該女自2008年起失業近一年，主要施騙對象集中在長年追星所結識到的各路日本、韓國藝人愛好者，例如傑尼斯事務所的各團體、w-inds.、[東方神起以及其他的AAA愛好者](../Page/東方神起.md "wikilink")，受害者也包括身邊的追星好友、原公司同事。\[110\]

2009年11月7日，根據台灣東森新聞、TVBS新聞等公共媒體的報道，警方在接到包括劉姓女子在內的至少6名受害者報警之後已將該女逮捕並交由檢方，因累計80萬新臺幣的涉案金額如無法償還將構成詐騙罪，最後以該女答應每月2萬新臺幣分期償還於受害者結案。\[111\]\[112\]\[113\]

##### 冠名店贈品簽名造假

AAA在與連鎖品牌甜品天堂（Sweets Paradise）合作出道10週年限定營業的“AAA
Cafe”時，被指成員的簽名贈品造假，出現諸如[末吉秀太的](../Page/末吉秀太.md "wikilink")“shuta”寫成“suta”等低級錯誤。因獲取簽名是愛好者到店面進行高額消費的主要原因，AAA被批判為“把愛好者當成蠢貨”一樣對待。另一方面，擁護言論則表示不該懷疑AAA，及AAA不可能做這種事情，群體內部出現分歧。\[114\]

2015年10月5日，[日高光啟回答了出現在自己Twitter下的質問](../Page/日高光啟.md "wikilink")，表示「正品簽名是在借用的六本木愛貝克思公司的會議室裡寫完的。雖然時間緊張，但從官方發放的贈品裡，假貨的概率為0%」。但隨著越來越多的愛好者陸續貼出疑似造假的簽名，事件影響開始擴大。19日，AAA所屬經紀公司發佈公告，表示是因為贈品簽名斷供，一名工作人員私自偽造導致。同日，AAA團長[浦田直也及成員日高光啟](../Page/浦田直也.md "wikilink")、[與真司郎](../Page/與真司郎.md "wikilink")、末吉秀太、[伊藤千晃亦在個人SNS主頁單獨發表了謝罪及個人感想文章](../Page/伊藤千晃.md "wikilink")\[115\]\[116\]\[117\]，直接或間接聲明是工作人員的個人行為，對這名合作多年、一直信賴的工作人員的行為感到遺憾。\[118\]另一位擁有個人公共主頁的成員[宇野實彩子則未直接提及該事件](../Page/宇野實彩子.md "wikilink")，只是感謝了愛好者一直的支持和信任。

事件過後，AAA愛好者迎來第一次大批量的退出潮，打著“AAA親筆簽名”標題的周邊商品開始大量出現在各大拍賣網站上，除大多數售價較低的彩色紙、明信片、彩色球以外，還有一部分是以2万日圓以上的價格成交。\[119\]

#### 團內爭議

AAA的男女成員在台前和幕後的一些親密互動，在愛好者羣體或坊間也有各種猜想和流言傳出，但就團體內部私生活的問題，西島曾表示從沒把宇野當作女人，和成員是工作關係，而宇野則稱不會和任何成員發展成戀愛對象。[末吉秀太在](../Page/末吉秀太.md "wikilink")2015年對於團體戀愛的問題迴應表示完全沒有想過這種事，團體成員之間一直屬於換衣服時也在一起的家人感覺。\[120\]

愛好者對男女是否同組以及現有成員也存在爭議，例如2ch在2011年下半年以形象、性格、職業能力等為題目設立了女成員宇野實彩子和伊藤千晃兩人各自的反對貼系列。其中宇野在後藤友香里退團事件開始，就一直在愛好者中間流傳著不同的負面話題，例如官方的BBS和雅虎知識袋等答疑網站質疑宇野實彩子和不同成員交惡，特別是和伊藤千晃，原因是她和其他成員之間不像過去那麽親近。

關於隊內問題，成員[與真司郎則在](../Page/與真司郎.md "wikilink")2014年1月發行的雜誌《B=PASS》表示從來沒有和任何成員發生過爭執的只有宇野，相反較容易生氣的反倒是自己和[日高光啓](../Page/日高光啓.md "wikilink")。\[121\]同年西島、與、末吉參加ORICON的單曲《Love》專訪，被問道團體「有愛的存在」是什麼，與真司郎表示是宇野，[末吉秀太選擇了伊藤](../Page/末吉秀太.md "wikilink")，西島隆弘則是和成員們一起吃飯的時候。\[122\]

## 音樂作品

### 單曲

#### 常規單曲

<table>
<thead>
<tr class="header">
<th><p>#</p></th>
<th><p>發行日期</p></th>
<th><p>單曲名稱</p></th>
<th><p><a href="../Page/Oricon公信榜.md" title="wikilink">初動排名</a></p></th>
<th><p>原創專輯</p></th>
<th><p>精選集</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1st</p></td>
<td><p>2005年9月14日</p></td>
<td><p><a href="../Page/BLOOD_on_FIRE.md" title="wikilink">BLOOD on FIRE</a></p></td>
<td><p>9[123]</p></td>
<td><p><a href="../Page/ATTACK_(AAA專輯).md" title="wikilink">ATTACK</a></p></td>
<td><p><a href="../Page/ATTACK_ALL_AROUND_(AAA專輯).md" title="wikilink">ATTACK ALL AROUND</a></p></td>
</tr>
<tr class="even">
<td><p>2nd</p></td>
<td><p>2005年10月5日</p></td>
<td><p><a href="../Page/Friday_Party.md" title="wikilink">Friday Party</a></p></td>
<td><p>17[124]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3rd</p></td>
<td><p>2005年11月16日</p></td>
<td><p><a href="../Page/美麗的天空.md" title="wikilink">美麗的天空</a></p></td>
<td><p>17[125]</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>4th</p></td>
<td><p>2005年12月7日</p></td>
<td><p><a href="../Page/DRAGON_FIRE.md" title="wikilink">DRAGON FIRE</a></p></td>
<td><p>20[126]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5th</p></td>
<td><p>2006年2月15日</p></td>
<td><p><a href="../Page/哈利路亞_(AAA單曲).md" title="wikilink">哈利路亞</a></p></td>
<td><p>8[127]</p></td>
<td><p><a href="../Page/ALL_(AAA專輯).md" title="wikilink">ALL</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>6th</p></td>
<td><p>2006年3月23日</p></td>
<td><p><a href="../Page/Shalala_希望之歌.md" title="wikilink">Shalala 希望之歌</a></p></td>
<td><p>20[128]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>7th</p></td>
<td><p>2006年5月31日</p></td>
<td><p><a href="../Page/颶風·莉莉，波士頓·瑪麗.md" title="wikilink">颶風·莉莉，波士頓·瑪麗</a></p></td>
<td><p>10[129]</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>8th</p></td>
<td><p>2006年7月19日</p></td>
<td><p><a href="../Page/刀魂男孩_/_和服噴射女孩.md" title="wikilink">刀魂男孩 / 和服噴射女孩</a></p></td>
<td><p>12[130]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>9th</p></td>
<td><p>2006年8月31日</p></td>
<td><p><a href="../Page/Let_it_beat!.md" title="wikilink">Let it beat!</a></p></td>
<td><p>7[131]</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>10th</p></td>
<td><p>2006年9月6日</p></td>
<td><p><a href="../Page/&quot;Q&quot;.md" title="wikilink">"Q"</a></p></td>
<td><p>10[132]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>11th</p></td>
<td><p>2006年11月15日</p></td>
<td><p><a href="../Page/口香糖_(AAA單曲).md" title="wikilink">口香糖</a></p></td>
<td><p>7[133]</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>12th</p></td>
<td><p>2006年12月6日</p></td>
<td><p><a href="../Page/Black_&amp;_White_(AAA單曲).md" title="wikilink">Black &amp; White</a></p></td>
<td><p>15[134]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>13th</p></td>
<td><p>2007年4月18日</p></td>
<td><p><a href="../Page/Get_啾!_/_SHE的真相.md" title="wikilink">Get 啾! / SHE的真相</a></p></td>
<td><p>5[135]</p></td>
<td><p><a href="../Page/AROUND_(AAA專輯).md" title="wikilink">AROUND</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>14th</p></td>
<td><p>2007年5月16日</p></td>
<td><p><a href="../Page/紅唇羅曼蒂卡_/_That&#39;s_Right.md" title="wikilink">紅唇羅曼蒂卡 / That's Right</a></p></td>
<td><p>6[136]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>15th</p></td>
<td><p>2007年7月18日</p></td>
<td><p><a href="../Page/夏日風物.md" title="wikilink">夏日風物</a></p></td>
<td><p>5[137]</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>16th</p></td>
<td><p>2007年9月19日</p></td>
<td><p><a href="../Page/Red_Soul.md" title="wikilink">Red Soul</a></p></td>
<td><p>15[138]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>17th</p></td>
<td><p>2008年1月9日</p></td>
<td><p><a href="../Page/MIRAGE_(AAA單曲).md" title="wikilink">MIRAGE</a></p></td>
<td><p>1[139]</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>18th</p></td>
<td><p>2008年5月28日</p></td>
<td><p><a href="../Page/BEYOND～超越極限.md" title="wikilink">BEYOND～超越極限</a></p></td>
<td><p>6[140]</p></td>
<td><p><a href="../Page/depArture.md" title="wikilink">depArture</a></p></td>
<td><p><a href="../Page/＃AAABEST.md" title="wikilink">＃AAABEST</a><br />
<a href="../Page/Another_side_of_＃AAABEST.md" title="wikilink">Another side of ＃AAABEST</a></p></td>
</tr>
<tr class="odd">
<td><p>19th</p></td>
<td><p>2008年8月27日</p></td>
<td><p>[[MUSIC</p></td>
<td><p>!_/_ZERO|MUSIC</p></td>
<td><p>! / ZERØ]]</p></td>
<td><p>9[141]</p></td>
</tr>
<tr class="even">
<td><p>20th</p></td>
<td><p>2009年1月14日</p></td>
<td><p><a href="../Page/啟程之歌_(AAA單曲).md" title="wikilink">啟程之歌</a></p></td>
<td><p>4[142]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>21st</p></td>
<td><p>2009年7月29日</p></td>
<td><p><a href="../Page/Break_Down_/_Break_your_name_/_Summer_Revolution.md" title="wikilink">Break Down / Break your name / Summer Revolution</a></p></td>
<td><p>3[143]</p></td>
<td><p><a href="../Page/HEARTFUL.md" title="wikilink">HEARTFUL</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>22nd</p></td>
<td><p>2009年10月21日</p></td>
<td><p><a href="../Page/Hide-away.md" title="wikilink">Hide-away</a></p></td>
<td><p>2[144]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>23rd</p></td>
<td><p>2010年1月27日</p></td>
<td><p><a href="../Page/Heart_and_Soul_(AAA單曲).md" title="wikilink">Heart and Soul</a></p></td>
<td><p>3[145]</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>24th</p></td>
<td><p>2010年5月5日</p></td>
<td><p><a href="../Page/想見你的理由_/_Dream_After_Dream_～夢醒之夢～.md" title="wikilink">想見你的理由 / Dream After Dream ～夢醒之夢～</a></p></td>
<td><p>1[146]</p></td>
<td><p><a href="../Page/Buzz_Communication.md" title="wikilink">Buzz Communication</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>25th</p></td>
<td><p>2010年8月18日</p></td>
<td><p><a href="../Page/不屈的心.md" title="wikilink">不屈的心</a></p></td>
<td><p>3[147]</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>26th</p></td>
<td><p>2010年11月17日</p></td>
<td><p><a href="../Page/PARADISE_/_Endless_Fighters.md" title="wikilink">PARADISE / Endless Fighters</a></p></td>
<td><p>5[148]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>27th</p></td>
<td><p>2011年2月16日</p></td>
<td><p><a href="../Page/重要的事.md" title="wikilink">重要的事</a></p></td>
<td><p>4[149]</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>28th</p></td>
<td><p>2011年6月22日</p></td>
<td><p><a href="../Page/No_cry_No_more.md" title="wikilink">No cry No more</a></p></td>
<td><p>3[150]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>29th</p></td>
<td><p>2011年8月31日</p></td>
<td><p><a href="../Page/CALL_/_I4U.md" title="wikilink">CALL / I4U</a></p></td>
<td><p>5[151]</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>30th</p></td>
<td><p>2011年11月16日</p></td>
<td><p><a href="../Page/Charge_▶_Go!_/_Lights.md" title="wikilink">Charge ▶ Go! / Lights</a></p></td>
<td><p>5[152]</p></td>
<td><p><a href="../Page/777_～TRIPLE_SEVEN～.md" title="wikilink">777 ～TRIPLE SEVEN～</a></p></td>
<td><p><a href="../Page/AAA_10th_ANNIVERSARY_BEST.md" title="wikilink">AAA 10th ANNIVERSARY BEST</a></p></td>
</tr>
<tr class="odd">
<td><p>31st</p></td>
<td><p>2012年2月22日</p></td>
<td><p><a href="../Page/SAILING.md" title="wikilink">SAILING</a></p></td>
<td><p>4[153]</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>32nd</p></td>
<td><p>2012年5月16日</p></td>
<td><p><a href="../Page/Still_Love_You.md" title="wikilink">Still Love You</a></p></td>
<td><p>3[154]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>33rd</p></td>
<td><p>2012年7月25日</p></td>
<td><p><a href="../Page/777_～We_can_sing_a_song!～.md" title="wikilink">777 ～We can sing a song!～</a></p></td>
<td><p>4[155]</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>34th</p></td>
<td><p>2012年10月31日</p></td>
<td><p><a href="../Page/彩虹_(AAA單曲).md" title="wikilink">彩虹</a></p></td>
<td><p>3[156]</p></td>
<td><p><a href="../Page/Eighth_Wonder_(AAA專輯).md" title="wikilink">Eighth Wonder</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>35th</p></td>
<td><p>2013年1月23日</p></td>
<td><p><a href="../Page/Miss_you_/_綻放笑容的場所.md" title="wikilink">Miss you / 綻放笑容的場所</a></p></td>
<td><p>3[157]</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>36th</p></td>
<td><p>2013年3月13日</p></td>
<td><p><a href="../Page/PARTY_IT_UP.md" title="wikilink">PARTY IT UP</a></p></td>
<td><p>7[158]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>37th</p></td>
<td><p>2013年6月26日</p></td>
<td><p><a href="../Page/Love_Is_In_The_Air.md" title="wikilink">Love Is In The Air</a></p></td>
<td><p>3[159]</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>38th</p></td>
<td><p>2013年9月4日</p></td>
<td><p><a href="../Page/戀歌與雨天.md" title="wikilink">戀歌與雨天</a></p></td>
<td><p>3[160]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>39th</p></td>
<td><p>2014年2月26日</p></td>
<td><p><a href="../Page/Love_(AAA單曲).md" title="wikilink">Love</a></p></td>
<td><p>4[161]</p></td>
<td><p><a href="../Page/GOLD_SYMPHONY.md" title="wikilink">GOLD SYMPHONY</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>40th</p></td>
<td><p>2014年3月26日</p></td>
<td><p><a href="../Page/SHOW_TIME.md" title="wikilink">SHOW TIME</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>41st</p></td>
<td><p>2014年7月2日</p></td>
<td><p><a href="../Page/Wake_up!.md" title="wikilink">Wake up!</a></p></td>
<td><p>3[162]</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>42nd</p></td>
<td><p>2014年9月17日</p></td>
<td><p><a href="../Page/說再見之前.md" title="wikilink">說再見之前</a></p></td>
<td><p>5[163]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>43rd</p></td>
<td><p>2015年1月28日</p></td>
<td><p><a href="../Page/I&#39;ll_be_there_(AAA單曲).md" title="wikilink">I'll be there</a></p></td>
<td><p>4[164]</p></td>
<td><p><a href="../Page/AAA_10th_ANNIVERSARY_BEST.md" title="wikilink">AAA 10th ANNIVERSARY BEST</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>44th</p></td>
<td><p>2015年2月25日</p></td>
<td><p><a href="../Page/Lil&#39;_Infinity.md" title="wikilink">Lil' Infinity</a></p></td>
<td><p>3[165]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>45th</p></td>
<td><p>2015年3月25日</p></td>
<td><p><a href="../Page/我的憂鬱與不開心的她.md" title="wikilink">我的憂鬱與不開心的她</a></p></td>
<td><p>4</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>46th</p></td>
<td><p>2015年4月29日</p></td>
<td><p><a href="../Page/GAME_OVER?.md" title="wikilink">GAME OVER?</a></p></td>
<td><p>5</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>47th</p></td>
<td><p>2015年5月27日</p></td>
<td><p><a href="../Page/明日之光.md" title="wikilink">明日之光</a></p></td>
<td><p>4</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>48th</p></td>
<td><p>2015年6月24日</p></td>
<td><p><a href="../Page/Flavor_of_kiss.md" title="wikilink">Flavor of kiss</a></p></td>
<td><p>2</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>49th</p></td>
<td><p>2015年7月29日</p></td>
<td><p><a href="../Page/LOVER.md" title="wikilink">LOVER</a></p></td>
<td><p>4</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>50th</p></td>
<td><p>2015年9月16日</p></td>
<td><p><a href="../Page/深愛著，卻不能愛.md" title="wikilink">深愛著，卻不能愛</a></p></td>
<td><p>4</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>51th</p></td>
<td><p>2016年6月8日</p></td>
<td><p><a href="../Page/NEW_(AAA單曲).md" title="wikilink">NEW</a></p></td>
<td><p>3</p></td>
<td><p><a href="../Page/WAY_OF_GLORY.md" title="wikilink">WAY OF GLORY</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>52th</p></td>
<td><p>2016年10月5日</p></td>
<td><p><a href="../Page/沒有眼淚的世界.md" title="wikilink">沒有眼淚的世界</a></p></td>
<td><p>5</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>53th</p></td>
<td><p>2017年2月8日</p></td>
<td><p><a href="../Page/MAGIC_(AAA單曲).md" title="wikilink">MAGIC</a></p></td>
<td><p>3</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>54th</p></td>
<td><p>2017年7月5日</p></td>
<td><p><a href="../Page/No_Way_Back.md" title="wikilink">No Way Back</a></p></td>
<td><p>3</p></td>
<td><p><a href="../Page/COLOR_A_LIFE.md" title="wikilink">COLOR A LIFE</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>55th</p></td>
<td><p>2017年10月18日</p></td>
<td><p><a href="../Page/LIFE_(AAA單曲).md" title="wikilink">LIFE</a></p></td>
<td><p>4</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>56th</p></td>
<td><p>2019年1月9日</p></td>
<td><p><a href="../Page/笑臉的循環.md" title="wikilink">笑臉的循環</a></p></td>
<td><p>-</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### 合作單曲

<table>
<thead>
<tr class="header">
<th><p>#</p></th>
<th><p>發行日期</p></th>
<th><p>單曲名稱</p></th>
<th><p><a href="../Page/Oricon公信榜.md" title="wikilink">初動排名</a></p></th>
<th><p>作品名義</p></th>
<th><p><a href="../Page/精選集.md" title="wikilink">精選集</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1st</p></td>
<td><p>2007年3月21日</p></td>
<td><p><a href="../Page/Climax_Jump.md" title="wikilink">Climax Jump</a></p></td>
<td><p>5</p></td>
<td><p>AAA DEN-O form</p></td>
<td><p><a href="../Page/ATTACK_ALL_AROUND_(AAA專輯).md" title="wikilink">ATTACK ALL AROUND</a></p></td>
</tr>
<tr class="even">
<td><p>2nd</p></td>
<td><p>2007年3月21日</p></td>
<td><p><a href="../Page/前進吧_少鷹軍團2007.md" title="wikilink">前進吧 少鷹軍團2007</a><br />
（）</p></td>
<td><p>33</p></td>
<td><p><a href="../Page/福岡軟銀鷹隊.md" title="wikilink">福岡軟銀鷹隊</a> with AAA</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 專輯

#### 原創專輯

| \#   | 發行日期       | 專輯名稱                                                                             | [初動排名](../Page/Oricon公信榜.md "wikilink") | 先行單曲                                                                          | 收錄單曲               |
| ---- | ---------- | -------------------------------------------------------------------------------- | --------------------------------------- | ----------------------------------------------------------------------------- | ------------------ |
| 1st  | 2006年1月1日  | **[ATTACK](../Page/ATTACK_\(AAA專輯\).md "wikilink")**\[166\]                      | 16                                      | [DRAGON FIRE](../Page/DRAGON_FIRE.md "wikilink")                              | 4張細碟4首單曲+8首原創      |
| 2nd  | 2007年1月1日  | **[ALL](../Page/ALL_\(AAA專輯\).md "wikilink")**\[167\]                            | 24                                      | [Black & White](../Page/Black_&_White_\(AAA單曲\).md "wikilink")                | 8張細碟10首單曲+3首原創     |
| 3rd  | 2007年9月19日 | **[AROUND](../Page/AROUND_\(AAA專輯\).md "wikilink")**\[168\]                      | 8                                       | [Red Soul同步](../Page/Red_Soul.md "wikilink")                                  | 4張細碟8首單曲+4首原創      |
| 4th  | 2009年2月11日 | **[depArture](../Page/depArture.md "wikilink")**（啟程）\[169\]                      | 4                                       | [啟程之歌](../Page/啟程之歌_\(AAA單曲\).md "wikilink")                                  | 3張細碟6首單曲+7首原創      |
| 5th  | 2010年2月17日 | **[HEARTFUL](../Page/HEARTFUL.md "wikilink")**（全心全意）\[170\]                      | 3                                       | [Heart and Soul](../Page/Heart_and_Soul_\(AAA單曲\).md "wikilink")              | 3張細碟4首單曲+10首原創     |
| 6th  | 2011年2月16日 | **[Buzz Communication](../Page/Buzz_Communication.md "wikilink")**（引領風潮）\[171\]  | 4                                       | [重要的事同步](../Page/重要的事.md "wikilink")                                          | 4張細碟10首單曲+4首原創     |
| 7th  | 2012年8月22日 | **[777 ～TRIPLE SEVEN～](../Page/777_～TRIPLE_SEVEN～.md "wikilink")**\[172\]        | 2                                       | [777 ～We can sing a song\!～](../Page/777_～We_can_sing_a_song!～.md "wikilink") | 4張細碟7首單曲+6首原創+1首特別 |
| 8th  | 2013年9月18日 | **[Eighth Wonder](../Page/Eighth_Wonder_\(AAA\).md "wikilink")**（第八個奇蹟）\[173\]   | 1                                       | [戀歌與雨天](../Page/戀歌與雨天.md "wikilink")                                          | 5張細碟9首單曲+5首原創+2首隱藏 |
| 9th  | 2014年10月1日 | **[GOLD SYMPHONY](../Page/GOLD_SYMPHONY.md "wikilink")**（黃金樂章）                   | 1                                       | [說再見之前](../Page/說再見之前.md "wikilink")                                          | 4張細碟6首單曲+4首原創      |
| 10th | 2015年9月16日 | **[AAA 10th ANNIVERSARY BEST](../Page/AAA_10th_ANNIVERSARY_BEST.md "wikilink")** | 1                                       | [深愛著，卻不能愛同步](../Page/深愛著，卻不能愛.md "wikilink")                                  | 8張細碟8首單曲+2首新曲      |
| 11th | 2017年2月22日 | **[WAY OF GLORY](../Page/WAY_OF_GLORY.md "wikilink")**                           | 1                                       | [MAGIC](../Page/MAGIC_\(AAA單曲\).md "wikilink")                                | 3張細碟6首單曲+6首新曲      |
| 12th | 2018年8月29日 | **[COLOR A LIFE](../Page/COLOR_A_LIFE.md "wikilink")**                           | 1                                       | [LIFE](../Page/LIFE_\(AAA單曲\).md "wikilink")                                  | 2張細碟2首單曲           |
|      |            |                                                                                  |                                         |                                                                               |                    |

#### 精選專輯

<table>
<thead>
<tr class="header">
<th><p>#</p></th>
<th><p>發行日期</p></th>
<th><p>專輯名稱</p></th>
<th><p><a href="../Page/Oricon公信榜.md" title="wikilink">初動排名</a></p></th>
<th><p>關聯單曲</p></th>
<th><p>收錄單曲</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1st</p></td>
<td><p>2008年3月5日</p></td>
<td><p><strong><a href="../Page/ATTACK_ALL_AROUND_(AAA專輯).md" title="wikilink">ATTACK ALL AROUND</a></strong>（全方位出擊）[174]</p></td>
<td><p>5</p></td>
<td></td>
<td><p>19張細碟25首單曲</p></td>
</tr>
<tr class="even">
<td><p>2nd</p></td>
<td><p>2011年9月14日</p></td>
<td><p><strong><a href="../Page/＃AAABEST.md" title="wikilink">＃AAABEST</a></strong>（＃AAA極精選）[175]</p></td>
<td><p>1</p></td>
<td><p><a href="../Page/CALL_/_I4U.md" title="wikilink">CALL / I4U先行</a></p></td>
<td><p>12首A面單曲+3首翻錄</p></td>
</tr>
<tr class="odd">
<td><p>3rd</p></td>
<td><p>2012年3月21日</p></td>
<td><p><strong><a href="../Page/Another_side_of_＃AAABEST.md" title="wikilink">Another side of ＃AAABEST</a></strong>（＃AAA極B面精選）[176]</p></td>
<td><p>4</p></td>
<td></td>
<td><p>12首單曲+7首單人翻錄</p></td>
</tr>
<tr class="even">
<td><p>4th</p></td>
<td><p>2015年9月16日</p></td>
<td><p><strong><a href="../Page/AAA_10th_ANNIVERSARY_BEST.md" title="wikilink">AAA 10th ANNIVERSARY BEST</a></strong></p></td>
<td><p>1</p></td>
<td><p><a href="../Page/深愛著，卻不能愛.md" title="wikilink">深愛著，卻不能愛同步</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### 迷你專輯

<table>
<thead>
<tr class="header">
<th><p>#</p></th>
<th><p>發行日期</p></th>
<th><p>專輯名稱</p></th>
<th><p><a href="../Page/Oricon公信榜.md" title="wikilink">初動排名</a></p></th>
<th><p>關聯單曲</p></th>
<th><p>收錄單曲</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>2006年1月1日</p></td>
<td><p><strong><a href="../Page/ATTACK_(AAA專輯).md" title="wikilink">ATTACK</a></strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1st</p></td>
<td><p>2006年9月13日</p></td>
<td><p><strong><a href="../Page/ALL/2.md" title="wikilink">ALL/2</a></strong>[177]</p></td>
<td><p>12</p></td>
<td><p><a href="../Page/&quot;Q&quot;.md" title="wikilink">"Q"先行</a></p></td>
<td><p>5張細碟5首單曲+1首原創</p></td>
</tr>
<tr class="odd">
<td><p>2nd</p></td>
<td><p>2007年3月21日</p></td>
<td><p><strong><a href="../Page/alohAAA!.md" title="wikilink">alohAAA!</a></strong>[178]</p></td>
<td><p>39</p></td>
<td><p><a href="../Page/前進吧_少鷹軍團2007.md" title="wikilink">前進吧 少鷹軍團2007與</a><a href="../Page/Climax_Jump.md" title="wikilink">Climax Jump同步</a></p></td>
<td><p>6首單曲+1首原創</p></td>
</tr>
<tr class="even">
<td><p>3rd</p></td>
<td><p>2008年6月18日</p></td>
<td><p><strong><a href="../Page/CHOICE_IS_YOURS.md" title="wikilink">CHOICE IS YOURS</a></strong>[179]</p></td>
<td><p>10</p></td>
<td></td>
<td><p>6首男成員單曲</p></td>
</tr>
<tr class="odd">
<td><p>4th</p></td>
<td><p>2011年1月12日</p></td>
<td><p><a href="../Page/2011.02.16_6th_ALBUM_“Buzz_Communication”Pre-Release_Special_Mini_Album.md" title="wikilink">Buzz Communication Pre-Release Special Mini Album</a></p></td>
<td></td>
<td></td>
<td><p>3首A面單曲+3首現場</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### 混音專輯

<table>
<thead>
<tr class="header">
<th><p>#</p></th>
<th><p>發行日期</p></th>
<th><p>專輯名稱</p></th>
<th><p><a href="../Page/Oricon公信榜.md" title="wikilink">初動排名</a></p></th>
<th><p>關聯單曲</p></th>
<th><p>收錄單曲</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1st</p></td>
<td><p>2006年3月23日</p></td>
<td><p><strong><a href="../Page/REMIX_ATTACK.md" title="wikilink">REMIX ATTACK</a></strong>[180]</p></td>
<td><p>92</p></td>
<td><p><a href="../Page/Shalala_希望之歌.md" title="wikilink">Shalala 希望之歌同步</a></p></td>
<td><p>大碟ATTACK12首+細碟哈利路亞1曲</p></td>
</tr>
<tr class="even">
<td><p>2nd</p></td>
<td><p>2009年3月4日</p></td>
<td><p><strong><a href="../Page/AAA_REMIX_〜non-stop_all_singles〜.md" title="wikilink">AAA REMIX 〜non-stop all singles〜</a></strong></p></td>
<td><p>58</p></td>
<td></td>
<td><p>20張細碟24首單曲</p></td>
</tr>
<tr class="odd">
<td><p>3rd</p></td>
<td><p>2013年12月25日</p></td>
<td><p><strong><a href="../Page/Driving_MIX.md" title="wikilink">Driving MIX</a></strong>[181]</p></td>
<td><p>7</p></td>
<td></td>
<td><p>25首混音+10首現場</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### 翻唱專輯

<table>
<thead>
<tr class="header">
<th><p>#</p></th>
<th><p>發行日期</p></th>
<th><p>專輯名稱</p></th>
<th><p><a href="../Page/Oricon公信榜.md" title="wikilink">初動排名</a></p></th>
<th><p>關聯單曲</p></th>
<th><p>收錄單曲</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1st</p></td>
<td><p>2007年2月7日</p></td>
<td><p><strong><a href="../Page/CCC_-CHALLENGE_COVER_COLLECTION-.md" title="wikilink">CCC -CHALLENGE COVER COLLECTION-</a></strong>[182]</p></td>
<td><p>13</p></td>
<td></td>
<td><p>9首舊翻唱+1首新翻唱</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### 概念專輯

| \#  | 發行日期       | 專輯名稱                                                                          | [初動排名](../Page/Oricon公信榜.md "wikilink") | 關聯單曲                                               | 收錄單曲         |
| --- | ---------- | ----------------------------------------------------------------------------- | --------------------------------------- | -------------------------------------------------- | ------------ |
| 1st | 2013年3月13日 | **[Ballad Collection](../Page/Ballad_Collection.md "wikilink")**（抒情精選）\[183\] | 4                                       | [PARTY IT UP同步](../Page/PARTY_IT_UP.md "wikilink") | 13首單曲+7首個人翻錄 |
|     |            |                                                                               |                                         |                                                    |              |

## 影視作品

### 錄影帶

| \#                                                             | 發行日期        | 專輯名稱                                                                                                                                                 | [初動排名](../Page/Oricon公信榜.md "wikilink") | 備註                                 |
| -------------------------------------------------------------- | ----------- | ---------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------- | ---------------------------------- |
| DVD                                                            | 2006年3月23日  | [1st ATTACK at SHIBUYA-AX](../Page/1st_ATTACK_at_SHIBUYA-AX.md "wikilink")                                                                           | 36\[184\]                               | [演唱會](../Page/演唱會.md "wikilink")   |
| DVD                                                            | 2006年9月13日  | [2nd ATTACK at Zepp Tokyo on 29th of June 2006](../Page/2nd_ATTACK_at_Zepp_Tokyo_on_29th_of_June_2006.md "wikilink")                                 | 10\[185\]                               | 演唱會                                |
| DVD                                                            | 2007年1月1日   | [1st Anniversary Live -3rd ATTACK 060913- at 日本武道館](../Page/1st_Anniversary_Live_-3rd_ATTACK_060913-_at_日本武道館.md "wikilink")                         | 32\[186\]                               | 演唱會+[話劇](../Page/話劇.md "wikilink") |
| DVD                                                            | 2007年3月21日  | Channel@×AAA                                                                                                                                         | 25\[187\]                               | [綜藝節目](../Page/綜藝節目.md "wikilink") |
| DVD                                                            | 2007年6月20日  | Theater of AAA 〜ボクラノテ〜                                                                                                                               |                                         | 舞台劇。官方會員限定                         |
| DVD                                                            | 2007年7月18 日 | [AAA TOUR 2007 4th ATTACK at SHIBUYA-AX on 4th of April](../Page/AAA_TOUR_2007_4th_ATTACK_at_SHIBUYA-AX_on_4th_of_April.md "wikilink")               | 12\[188\]                               | 演唱會                                |
| DVD                                                            | 2008年1月9日   | [2nd Anniversary Live -5th ATTACK 070922- 日本武道館](../Page/2nd_Anniversary_Live_-5th_ATTACK_070922-_日本武道館.md "wikilink")                               | 4\[189\]                                | 演唱會                                |
| DVD                                                            | 2008年3月15日  | PPP -Premium Performance Party-                                                                                                                      |                                         | 官方會員限定，演唱會精選                       |
| DVD                                                            | 2008年8月27日  | [AAA TOUR 2008 -ATTACK ALL AROUND- at NHK HALL on 4th of April](../Page/AAA_TOUR_2008_-ATTACK_ALL_AROUND-_at_NHK_HALL_on_4th_of_April.md "wikilink") | 6\[190\]                                | 演唱會+話劇                             |
| DVD                                                            | 2009年1月14日  | [AAA 3rd Anniversary Live 080922-080923 日本武道館](../Page/AAA_3rd_Anniversary_Live_080922-080923_日本武道館.md "wikilink")                                   | 3\[191\]                                | 演唱會                                |
| DVD                                                            | 2009年3月4日   | Channel@×AAA -其の弐-                                                                                                                                   | 29\[192\]                               | 綜藝節目                               |
| DVD                                                            | 2009年8月19日  | [AAA TOUR 2009 - A depArture pArty-](../Page/AAA_TOUR_2009_-_A_depArture_pArty-.md "wikilink")                                                       | 4\[193\]                                | 演唱會                                |
| DVD                                                            | 2010年3月3日   | [AAA 4th Anniversary LIVE 090922 at Yokohama Arena](../Page/AAA_4th_Anniversary_LIVE_090922_at_Yokohama_Arena.md "wikilink")                         | 2\[194\]                                | 演唱會                                |
| DVD                                                            | 2010年9月29日  | [AAA Heart to ♥ TOUR 2010](../Page/AAA_Heart_to_♥_TOUR_2010.md "wikilink")                                                                           | 7\[195\]                                | 演唱會                                |
| DVD                                                            | 2011年1月12日  | AAA Party 秋の大運動会ツアー in 静岡 2010.9.25〜9.26（2011年1月12日）                                                                                                 |                                         | 官方會員限定                             |
| DVD                                                            | 2011年3月16日  | [AAA 5th Anniversary LIVE 20100912 at Yokohama Arena](../Page/AAA_5th_Anniversary_LIVE_20100912_at_Yokohama_Arena.md "wikilink")                     | 1\[196\]                                | 演唱會                                |
| AAA 5th Anniversary LIVE PREMIUM AWARD PARTY at Yokohama Arena |             |                                                                                                                                                      |                                         |                                    |
| DVD                                                            | 2011年11月16日 | [AAA Buzz Communication Deluxe Edition at SAITAMA SUPER ARENA](../Page/AAA_Buzz_Communication_Deluxe_Edition_at_SAITAMA_SUPER_ARENA.md "wikilink")   | 4\[197\]                                | 演唱會                                |
| DVD                                                            | 2012年2月22日  | [AAA 6th Anniversary Tour 2011.9.28 at Zepp Tokyo](../Page/AAA_6th_Anniversary_Tour_2011.9.28_at_Zepp_Tokyo.md "wikilink")                           | 4\[198\]                                | 演唱會                                |
| DVD                                                            | 2013年2月13日  | [AAA TOUR 2012 -777- TRIPLE SEVEN](../Page/AAA_TOUR_2012_-777-_TRIPLE_SEVEN.md "wikilink")                                                           | 1\[199\]                                | 演唱會                                |
| DVD                                                            | 2014年1月22日  | [AAA TOUR 2013 Eighth Wonder](../Page/AAA_TOUR_2013_Eighth_Wonder.md "wikilink")                                                                     | 1\[200\]                                | 演唱會                                |
| DVD                                                            | 2015年2月25日  | [AAA ARENA TOUR 2014 -Gold Symphony-](../Page/AAA_ARENA_TOUR_2014_-Gold_Symphony-.md "wikilink")                                                     | 1                                       | 演唱會                                |
| DVD                                                            | 2016年1月27日  | [AAA 10th Anniversary SPECIAL 野外LIVE in 富士急高原樂園](../Page/AAA_10th_Anniversary_SPECIAL_野外LIVE_in_富士急高原樂園.md "wikilink")                               | 2                                       | 演唱會                                |
| DVD                                                            | 2016年3月16日  | [AAA 10th ANNIVERSARY Documentary ～Road of 10th ANNIVERSARY～](../Page/AAA_10th_ANNIVERSARY_Documentary_～Road_of_10th_ANNIVERSARY～.md "wikilink")     | 2                                       | 紀錄片+演唱會                            |
| DVD                                                            | 2016年11月9日  | [AAA ARENA TOUR 2016 -LEAP OVER-](../Page/AAA_ARENA_TOUR_2016_-LEAP_OVER-.md "wikilink")                                                             | 1                                       | 演唱會                                |
| DVD                                                            | 2017年3月22日  | [AAA Special Live 2016 in Dome -FANTASTIC OVER-](../Page/AAA_Special_Live_2016_in_Dome_-FANTASTIC_OVER-.md "wikilink")                               | 1                                       | 演唱會                                |
| DVD                                                            | 2018年1月17日  | [AAA DOME TOUR 2017 -WAY OF GLORY-](../Page/AAA_DOME_TOUR_2017_-WAY_OF_GLORY-.md "wikilink")                                                         | 1                                       | 演唱會                                |
| DVD                                                            | 2018年3月28日  | [AAA NEW YEAR PARTY 2018](../Page/AAA_NEW_YEAR_PARTY_2018.md "wikilink")                                                                             | 3                                       | 演唱會                                |
| DVD                                                            | 2018年11月7日  | [AAA FAN MEETING ARENA TOUR 2018 〜FAN FUN FAN〜](../Page/AAA_FAN_MEETING_ARENA_TOUR_2018_〜FAN_FUN_FAN〜.md "wikilink")                                 | 1                                       | FM+演唱會                             |

## 其他作品

### 書籍

#### 寫真集

| \# | 發售日期        | 標題                                                | 攝影     | 出版商                                            | 備註                       |
| -- | ----------- | ------------------------------------------------- | ------ | ---------------------------------------------- | ------------------------ |
| 藝術 | 2005年9月14日  | AAA artistbook / the first attack                 | 木村尚樹   | [角川書店](../Page/角川書店.md "wikilink")             | ISBN 4048944630          |
| 紀實 | 2011年11月25日 | AAA Buzz Communication Documentary Extra book     | **合集** | Sony Magazines                                 | ISBN 978-4789735056      |
| 紀實 | 2012年9月7日   | AAA TOUR 2012 -777- TRIPLE SEVEN Documentary Book | **合集** | [Enterbrain](../Page/Enterbrain.md "wikilink") | ISBN 978-4047283152      |
| 藝術 | 2012年12月14日 | AAA 7TH ANNIVERSARY BOOK ABC〜AAA Book Chronicle〜  | 小林ばく   | 主婦與生活社                                         | ISBN 978-4391142860      |
| 紀實 | 2014年2月27日  | AAA 2013 TOUR BOOK Eighth Wonder                  | **合集** | WANI BOOKS                                     | ISBN 978-4847046261      |
| 紀實 | 2014年3月27日  | AAA 2013 TOUR Eighth Wonder PREMIUM BOX           | **合集** | ISBN 978-4847046377                            |                          |
| 藝術 | 2015年7月15日  | AAA 10th Anniversary BOOK                         |        | 主婦與生活社                                         | 附DVD，ISBN 978-4391147223 |

#### 連載

| \# | 起始日期        | 欄目名稱                                    | 雜誌                                             | 出版社                                      | 備註           |
| -- | ----------- | --------------------------------------- | ---------------------------------------------- | ---------------------------------------- | ------------ |
| 月刊 | 2005年9月14日  | AAAのA級雷（ライ）センス                          | [WHAT's IN?](../Page/WHAT's_IN?.md "wikilink") | [M-ON\!](../Page/M-ON!.md "wikilink")    |              |
| 月刊 | 2006年5月1日   | 噂のAAAファイル                               | [Popteen](../Page/Popteen.md "wikilink")       | [角川春樹事務所](../Page/角川春樹事務所.md "wikilink") | 同年8月1日完結     |
| 月刊 | 2006年10月1日  | CLOSE UP ARTIST AAA 今月のキャラクター           | [Fine](../Page/Fine.md "wikilink")             | [日之出出版社](../Page/日之出出版社.md "wikilink")   | 2007年5月30日完結 |
| 月刊 | 2010年6月23日  | AAAのBBB～Backstage Boogie Bestshot～      | [Ray](../Page/Ray.md "wikilink")               | [主婦之友社](../Page/主婦之友社.md "wikilink")     | 2010年12月完結   |
| 月刊 | 2011年11月22日 | AAAのBBB～Backstage Boogie Bestshot～again | 2012年6月完結                                      |                                          |              |
| 月刊 | 2012年9月23日  | AAAのDDD～do did done～トリエーの裏SNAP\~        | 2014年4月完結                                      |                                          |              |
| 月刊 | 2014年6月23日  | AAAのFFF～FUZZY FIZZY FEELING～な“恋愛相談”     |                                                |                                          |              |
|    |             |                                         |                                                |                                          |              |

### 參加作品

<table>
<thead>
<tr class="header">
<th><p>#</p></th>
<th><p>發售日期</p></th>
<th><p>作品標題</p></th>
<th><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>DVD</p></td>
<td><p>2005年10月26日</p></td>
<td><p>a-nation'05 BEST HIT LIVE</p></td>
<td><p>1 BLOOD on FIRE</p></td>
</tr>
<tr class="even">
<td><p>專輯</p></td>
<td><p>2006年7月19日</p></td>
<td><p>a-nation '06 BEST HIT SELECTION</p></td>
<td><p>7 BLOOD on FIRE</p></td>
</tr>
<tr class="odd">
<td><p>專輯</p></td>
<td><p>2006年9月6日</p></td>
<td><p>Super Eurobeat Presents Super GT 2006 - Second Round</p></td>
<td><p>26 BLOOD on FIRE (SG Sonic Edit)</p></td>
</tr>
<tr class="even">
<td><p>DVD</p></td>
<td><p>2006年11月8日</p></td>
<td><p>a-nation'06 BEST HIT LIVE</p></td>
<td><p>3 ハリケーン・リリ，ボストン・マリ</p></td>
</tr>
<tr class="odd">
<td><p>專輯</p></td>
<td><p>2007年1月17日</p></td>
<td><p>クラブ・ディズニー2007</p></td>
<td><p>4 きみもとべるよ![ピーター・パン]<br />
5 ビビディ・バビディ・ブー[シンデレラ]</p></td>
</tr>
<tr class="even">
<td><p>專輯</p></td>
<td><p>2007年2月14日</p></td>
<td><p>ザ・ベスト・オブ 東京プリン その2</p></td>
<td><p>14 東京プリンが10周年</p></td>
</tr>
<tr class="odd">
<td><p>專輯</p></td>
<td><p>2007年6月26日</p></td>
<td><p>仮面ライダー電王 ORIGINAL SOUNDTRACK</p></td>
<td><p>29 Climax Jump HIPHOP ver.<br />
34 Climax Jump TV-SIZE EDIT)</p></td>
</tr>
<tr class="even">
<td><p>DVD</p></td>
<td><p>2007年11月7日</p></td>
<td><p>a-nation'07 BEST HIT LIVE</p></td>
<td><p>2 SUNSHINE</p></td>
</tr>
<tr class="odd">
<td><p>DVD</p></td>
<td><p>2007年12月5日</p></td>
<td><p>BEST LIVE DVD -PREMIUM LIVE DREAM SELECTION-</p></td>
<td><p>2 SUNSHINE</p></td>
</tr>
<tr class="even">
<td><p>单曲</p></td>
<td><p>2008年10月1日</p></td>
<td><p>仮面ライダー電王 いーじゃん!いーじゃん!スゲーじゃん!?</p></td>
<td><p>1 Climax Jump the Final / AAA DEN-O form</p></td>
</tr>
<tr class="odd">
<td><p>DVD</p></td>
<td><p>2008年11月26日</p></td>
<td><p>a-nation'08 ~avex ALL CAST SPECIAL LIVE~</p></td>
<td><p>1 ハレルヤ</p></td>
</tr>
<tr class="even">
<td><p>專輯</p></td>
<td><p>2009年1月28日</p></td>
<td><p>ディズニー・ドリーム・ポップ～トリビュート・トゥ・トウキョウディズニーリゾート 25thアニバーサリー</p></td>
<td><p>3 リロ&amp;スティッチ::ハワイアンローラーコースターライド (EUROBEAT REMIX)</p></td>
</tr>
<tr class="odd">
<td><p>DVD</p></td>
<td><p>2009年11月18日</p></td>
<td><p>a-nation'09 BEST HIT LIVE</p></td>
<td><p>1 Break Down</p></td>
</tr>
<tr class="even">
<td><p>DVD</p></td>
<td><p>2010年11月24日</p></td>
<td><p>a-nation'10 BEST HIT LIVE</p></td>
<td><p>5 ハリケーン・リリ，ボストン・マリ<br />
6 CALL</p></td>
</tr>
<tr class="odd">
<td><p>DVD</p></td>
<td><p>2011年12月14日</p></td>
<td><p>a-nation for Life</p></td>
<td><p>7 逢いたい理由<br />
8 ハリケーン・リリ，ボストン・マリ</p></td>
</tr>
<tr class="even">
<td><p>專輯</p></td>
<td><p>2013年3月13日</p></td>
<td><p>TRF TRIBUTE ALBUM BEST</p></td>
<td><p>10 Love &amp; Peace Forever (Remix)</p></td>
</tr>
<tr class="odd">
<td><p>專輯</p></td>
<td><p>2013年9月25日</p></td>
<td><p>Disney Japanese Artists Best</p></td>
<td><p>18 輝く未来</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 數位作品

  - 首發平台或僅有數位版本的作品。

<table>
<thead>
<tr class="header">
<th><p>#</p></th>
<th><p>日期</p></th>
<th><p>標題</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>專輯</p></td>
<td><p>2006年9月13日</p></td>
<td><p>2nd ATTACK at Zepp Tokyo on 29th of June 2006</p></td>
<td><p>演唱會錄音</p></td>
</tr>
<tr class="even">
<td><p>單曲</p></td>
<td><p>2007年11月28日</p></td>
<td><p>男だけだと、・・・こうなりました！</p></td>
<td><p>演唱會錄音</p></td>
</tr>
<tr class="odd">
<td><p>專輯</p></td>
<td><p>2009年4月8日</p></td>
<td><p>1st Anniversary Live -3rd ATTACK 060913- at 日本武道館</p></td>
<td><p>演唱會錄音</p></td>
</tr>
<tr class="even">
<td><p>專輯</p></td>
<td><p>2009年4月15日</p></td>
<td><p>AAA TOUR 2007 4th ATTACK at SHIBUYA-AX on 4th of April</p></td>
<td><p>演唱會錄音</p></td>
</tr>
<tr class="odd">
<td><p>專輯</p></td>
<td><p>2009年4月22日</p></td>
<td><p>AAA 2nd Anniversary Live -5th ATTACK 070922- 日本武道館</p></td>
<td><p>演唱會錄音</p></td>
</tr>
<tr class="even">
<td><p>專輯</p></td>
<td><p>2009年4月29日</p></td>
<td><p>AAA TOUR 2008 -ATTACK ALL AROUND- at NHK HALL on 4th of April</p></td>
<td><p>演唱會錄音</p></td>
</tr>
<tr class="odd">
<td><p>專輯</p></td>
<td><p>2009年5月6日</p></td>
<td><p>3rd Anniversary Live 080922-080923 日本武道館</p></td>
<td><p>演唱會錄音</p></td>
</tr>
<tr class="even">
<td><p>寫真</p></td>
<td><p>2010年10月8日</p></td>
<td><p>AAA 5th anniversary LIVE Original Photo Book</p></td>
<td><p>GOODS ON DEMAND截至2011年1月9日</p></td>
</tr>
<tr class="odd">
<td><p>單曲</p></td>
<td><p>2010年12月19日</p></td>
<td><p>逢いたい理由 (winter version)</p></td>
<td><p>錄音室</p></td>
</tr>
<tr class="even">
<td><p>特別</p></td>
<td><p>2011年3月16日</p></td>
<td><ol>
<li>3月16日 Thank you</li>
<li>7月27日 Charge ▶ Go!</li>
<li>12月14日 Lights ~Winter Version~</li>
</ol></td>
<td><p>交換卡片</p></td>
</tr>
<tr class="odd">
<td><p>單曲</p></td>
<td><p>2011年9月14日</p></td>
<td><p>#AAABEST SPECIAL SELECTION</p>
<ul>
<li>AAABEST COMPLETEメドレー</li>
</ul></td>
<td></td>
</tr>
<tr class="even">
<td><p>寫真</p></td>
<td><p>2011年7月15日</p></td>
<td><p>AAA Buzz Communication Original Photo Book</p></td>
<td><p>GOODS ON DEMAND截止2011年10月14日</p></td>
</tr>
<tr class="odd">
<td><p>單曲</p></td>
<td><p>2011年11月16日</p></td>
<td><p>*MUSIC</p></td>
<td><p>! (from Buzz Communication Tour 2011 Deluxe Edition)</p>
<ul>
<li>Day by day (from Buzz Communication Tour 2011 Deluxe Edition)</li>
<li>逢いたい理由 (from Buzz Communication Tour 2011 Deluxe Edition)</li>
<li>PARADISE (from Buzz Communication Tour 2011 Deluxe Edition)</li>
<li>Believe own way (from Buzz Communication Tour 2011 Deluxe Edition)</li>
<li>Heart and Soul (from Buzz Communication Tour 2011 Deluxe Edition)</li>
<li>Charge ▶ Go! (from Buzz Communication Tour 2011 Deluxe Edition)</li>
<li>No Cry No More (from Buzz Communication Tour 2011 Deluxe Edition)</li>
<li>ダイジナコト (from Buzz Communication Tour 2011 Deluxe Edition)</li>
</ul></td>
</tr>
<tr class="even">
<td><p>單曲</p></td>
<td><p>2011年12月28日</p></td>
<td><p>AAA 2011シングル サビメドレー (ダイジナコト〜 No cry No more 〜 CALL 〜 Charge ▶ Go</p></td>
<td><p>)</p></td>
</tr>
<tr class="odd">
<td><p>單曲</p></td>
<td><p>2012年3月14日</p></td>
<td><p>AAA ソロセルフカヴァー・スペシャルメドレー</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>專輯</p></td>
<td><p>2013年2月23日</p></td>
<td><p>AAA TOUR 2012 -777- TRIPLE SEVEN</p></td>
<td><p>演唱會實況</p></td>
</tr>
<tr class="odd">
<td><p>專輯</p></td>
<td><p>2013年12月25日</p></td>
<td><p>AAA TOUR 2013 Eighth Wonder</p></td>
<td><p>演唱會實況</p></td>
</tr>
<tr class="even">
<td><p>專輯</p></td>
<td><p>2014年2月19日</p></td>
<td><ol>
<li>SAILING</li>
<li>旅ダチノウタ</li>
<li>STEP</li>
<li>Wonderful Life</li>
<li>負けない心</li>
<li>Charge ＆ Go！</li>
<li>shalala キボウの歌</li>
<li>FIELD</li>
<li>Thank you</li>
<li>No cry No more</li>
</ol></td>
<td><p>精選集</p></td>
</tr>
<tr class="odd">
<td><p>單曲</p></td>
<td><p>2014年1月19日</p></td>
<td><p>Wake up! (アニメver.)</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>單曲</p></td>
<td><p>2014年4月16</p></td>
<td><p>HANDs</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>單曲</p></td>
<td><p>2014年6月4日</p></td>
<td><p>風に薫る夏の記憶</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>專輯</p></td>
<td><p>2014年7月2日</p></td>
<td><ol>
<li>777 ～We can sing a song!～</li>
<li>Love Is In The Air</li>
<li>Get チュ-!</li>
</ol>
<p>#MUSIC</p></td>
<td><p>!</p>
<ol>
<li>Charge &amp; Go!</li>
<li>ハリケーン・リリ，ボストン・マリ</li>
<li>PARADISE</li>
<li>ハレルヤ</li>
<li>Summer Revolution</li>
<li>SUNSHINE</li>
</ol></td>
</tr>
<tr class="odd">
<td><p>單曲</p></td>
<td><p>2014年8月30日</p></td>
<td><p>Next Stage(アニメver.)</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>遊戲</p></td>
<td><p>2014年10月28日</p></td>
<td><p>Another story of AAA ～恋音と雨空～</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 演唱會

<table>
<thead>
<tr class="header">
<th><p>年分</p></th>
<th><p>標題</p></th>
<th><p>規模</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>'''明天是FridayParty</p></td>
<td><p>'''</p></td>
<td><p>11月3日 <a href="../Page/Shibuya_O-EAST.md" title="wikilink">Shibuya O-EAST</a> </p></td>
</tr>
<tr class="even">
<td><p>2006年</p></td>
<td><p><strong>AAA TOUR 2006 -1st ATTACK-</strong></p></td>
<td><p>1月22日 福岡DRUM LOGOSS<br />
1月23日 BIG CAT<br />
1月27日 CLUB DIAMOND HALL<br />
1月28日 <a href="../Page/SHIBUYA-AX.md" title="wikilink">SHIBUYA-AX</a> </p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>AAA TOUR 2006 -1st ATTACK ROUND 2-</strong></p></td>
<td><p>3月16日 <a href="../Page/Zepp.md" title="wikilink">Zepp Sendai</a><br />
3月19日 埼玉パストラルかぞ<br />
3月22日 <a href="../Page/NIIGATA_LOTS.md" title="wikilink">NIIGATA LOTS</a><br />
3月24日 <a href="../Page/SHIBUYA-AX.md" title="wikilink">SHIBUYA-AX</a><br />
4月9日 福岡DRUM LOGOS<br />
4月15日 <a href="../Page/横浜BLITZ.md" title="wikilink">横浜BLITZ</a> </p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>AAA TOUR 2006 -2nd ATTACK-</strong></p></td>
<td><p>6月11日 福岡DRUM LOGOS<br />
6月13日 <a href="../Page/Zepp.md" title="wikilink">Zepp Sendai</a><br />
6月15日 <a href="../Page/Zepp.md" title="wikilink">Zepp Sapporo</a><br />
6月17日 新潟Phase<br />
6月21日 <a href="../Page/Zepp.md" title="wikilink">Zepp Nagoya</a><br />
6月23日 広島CLUB QUATTRO<br />
6月27日 なんばHatch<br />
6月29日 <a href="../Page/Zepp.md" title="wikilink">Zepp Tokyo</a> </p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>AAA 1st Anniversary Live 060913 〜3rd ATTACK〜 日本武道館</strong></p></td>
<td><p>9月13日 <a href="../Page/日本武道館.md" title="wikilink">日本武道館</a> </p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2007年</p></td>
<td><p><strong>AAA TOUR 2007 -4th ATTACK-</strong></p></td>
<td><p>3月10日 ハーモニーホール座間<br />
3月23日 なんばHatch<br />
3月24日 <a href="../Page/Zepp.md" title="wikilink">Zepp Nagoya</a><br />
3月28日 <a href="../Page/SHIBUYA-AX.md" title="wikilink">SHIBUYA-AX</a><br />
3月29日 <a href="../Page/SHIBUYA-AX.md" title="wikilink">SHIBUYA-AX</a><br />
4月1日 <a href="../Page/Zepp.md" title="wikilink">Zepp Sendai</a><br />
4月3日 <a href="../Page/SHIBUYA-AX.md" title="wikilink">SHIBUYA-AX</a><br />
4月4日 <a href="../Page/SHIBUYA-AX.md" title="wikilink">SHIBUYA-AX</a><br />
4月15日 <a href="../Page/Zepp.md" title="wikilink">Zepp Sapporo</a> </p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>AAA TOUR 2007 -4th ATTACK ROUND 2-</strong></p></td>
<td><p>6月13日 <a href="../Page/Zepp.md" title="wikilink">Zepp Nagoya</a><br />
6月14日 なんばHatch<br />
6月19日 <a href="../Page/澀谷公會堂.md" title="wikilink">澀谷公會堂</a><br />
6月20日 <a href="../Page/澀谷公會堂.md" title="wikilink">澀谷公會堂</a><br />
6月22日 <a href="../Page/Zepp.md" title="wikilink">Zepp Sapporo</a><br />
6月27日 <a href="../Page/Zepp.md" title="wikilink">Zepp Fukuoka</a> </p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>OTAKON 2007 AAA Concert and signing</strong></p></td>
<td><p>7月20日 巴爾的摩會議中心 </p></td>
<td><p><a href="../Page/巴爾的摩.md" title="wikilink">巴爾的摩迷你演唱會</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>AAA 2nd Anniversary Live 〜5th ATTACK〜 日本武道館</strong></p></td>
<td><p>9月22日 <a href="../Page/日本武道館.md" title="wikilink">日本武道館</a> </p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2007 Winter Special LIVE 〜男だけだと、・・・こうなりました!?〜</strong></p></td>
<td><p>12月4日 川崎クラブチッタ<br />
12月5日 川崎クラブチッタ<br />
12月6日 川崎クラブチッタ </p></td>
<td><p>男成員單獨演唱會</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2008年</p></td>
<td><p><strong>AAA TOUR 2008 -ATTACK ALL AROUND-</strong></p></td>
<td><p>3月16日 埼玉市文化中心<br />
3月20日 グリーンホール相模大野<br />
3月22日 大阪厚生年金会館<br />
3月23日 愛知厚生年金会館<br />
3月29日 仙台市泉文化創造中心<br />
3月31日 <a href="../Page/澀谷公會堂.md" title="wikilink">澀谷公會堂</a><br />
4月1日 <a href="../Page/澀谷公會堂.md" title="wikilink">澀谷公會堂</a><br />
4月4日 <a href="../Page/NHK大廳.md" title="wikilink">NHK大廳</a><br />
4月6日 <a href="../Page/Zepp.md" title="wikilink">ZEPP福岡</a><br />
4月12日 <a href="../Page/Zepp.md" title="wikilink">ZEPP札幌</a> </p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>AAA 3rd Anniversary Live 080922-080923 日本武道館</strong></p></td>
<td><p>9月22日 <a href="../Page/日本武道館.md" title="wikilink">日本武道館</a><br />
9月23日 <a href="../Page/日本武道館.md" title="wikilink">日本武道館</a> </p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2009年</p></td>
<td><p><strong>AAA TOUR 2009 -A depArture pArty-</strong></p></td>
<td><p>3月7日 綾瀬市文化会館<br />
3月13日 新潟県民会館<br />
3月15日 大宮ソニックシティ<br />
3月21日 富士市文化中心玫瑰劇院<br />
3月22日 名古屋市民会館<br />
3月24日 <a href="../Page/澀谷公會堂.md" title="wikilink">澀谷公會堂</a><br />
3月25日 <a href="../Page/澀谷公會堂.md" title="wikilink">澀谷公會堂</a><br />
3月27日 神戸国際会館<br />
3月29日 廣島紫苑廣場<br />
4月2日 札幌市教育文化会館<br />
4月4日 青森縣大會堂<br />
4月5日 秋田県児童会館<br />
4月11日 宮城県民会館<br />
4月12日 神奈川縣大會堂<br />
4月18日 福岡市民会館<br />
4月19日 長崎市公会堂<br />
4月24日 名古屋市民会館<br />
4月26日 大阪厚生年金会館<br />
4月28日 <a href="../Page/澀谷公會堂.md" title="wikilink">澀谷公會堂</a><br />
4月29日 <a href="../Page/澀谷公會堂.md" title="wikilink">澀谷公會堂</a> </p></td>
<td><p>全國巡迴演唱</p></td>
</tr>
<tr class="even">
<td><p><strong>AAA 4th Anniversary LIVE at 橫濱體育館</strong></p></td>
<td><p>9月22日 <a href="../Page/橫濱體育館.md" title="wikilink">橫濱體育館</a> </p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2010年</p></td>
<td><p><strong>AAA Heart to ♥ TOUR 2010</strong></p></td>
<td><p>3月6日 市川市文化会館<br />
3月13日 羽生市產業文化館<br />
3月14日 よこすか芸術劇場<br />
3月20日 櫪木縣立文化中心<br />
3月22日 新潟県民会館<br />
3月24日 靜岡市民文化會館<br />
3月25日 三重県文化会館<br />
3月28日 埼玉市文化中心<br />
3月30日 郡山市文化中心<br />
4月1日 札幌市民會館<br />
4月3日 秋田県民会館<br />
4月4日 青森市文化会館<br />
4月10日 神戸国際会館<br />
4月11日 神戸国際会館<br />
4月17日 仙台市泉文化創造中心<br />
4月18日 大宮ソニックシティ<br />
4月24日 厚木市文化会館<br />
4月25日 福生市民会館<br />
4月29日 八幡市文化中心<br />
5月1日 長崎市公会堂<br />
5月2日 福岡市民会館<br />
5月4日 倉敷市民会館<br />
5月7日 松山市綜合社區中心<br />
5月8日 アステールプラザ<br />
5月16日 <a href="../Page/NHK大廳.md" title="wikilink">NHK大廳</a><br />
5月22日 <a href="../Page/名古屋國際會議場.md" title="wikilink">名古屋國際會議場</a> </p></td>
<td><p>全國巡迴演唱</p></td>
</tr>
<tr class="even">
<td><p><strong>AAA 5th Anniversary LIVE</strong></p></td>
<td><p>9月11日 <a href="../Page/橫濱體育館.md" title="wikilink">橫濱體育館</a><br />
9月12日 <a href="../Page/橫濱體育館.md" title="wikilink">橫濱體育館</a><br />
9月19日 神戶世界紀念館<br />
9月20日 神戶世界紀念館 </p></td>
<td><p>9月11日和19日的公演為《Premium Award》<br />
9月12日和20日的公演為正式週年紀念LIVE</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2011年</p></td>
<td><p><strong>AAA BUZZ COMMUNICATION TOUR 2011</strong></p></td>
<td><p>4月24日 さっぽろ芸術文化の館<br />
4月30日 廣島縣立文化藝術會館<br />
5月1日 松山市民会館<br />
5月3日 鹿兒島縣文化中心<br />
5月4日 宮崎市民文化會館<br />
5月6日 福岡太陽廣場<br />
5月7日 長崎市公会堂<br />
5月11日 滋賀縣立藝術劇場 琵琶湖音樂廳<br />
5月14日 周南市文化会館<br />
5月15日 倉敷市民会館<br />
5月19日 大宮ソニックシティ<br />
5月20日 神奈川縣大會堂<br />
5月22日 静岡市民文化会館<br />
5月23日 市川市文化会館<br />
5月26日 宇都宮市文化会館<br />
5月28日 奈良100年会館<br />
5月29日 金澤市文化會館<br />
6月3日 三重縣文化会館<br />
6月4日 <a href="../Page/名古屋國際會議場.md" title="wikilink">名古屋國際會議場</a><br />
6月5日 <a href="../Page/名古屋國際會議場.md" title="wikilink">名古屋國際會議場</a><br />
6月7日 府中 森藝術劇院<br />
6月11日 <a href="../Page/神戶世界紀念館.md" title="wikilink">神戶世界紀念館</a><br />
6月12日 <a href="../Page/神戶世界紀念館.md" title="wikilink">神戶世界紀念館</a><br />
6月17日 青森市文化会館<br />
6月18日 秋田縣大會堂<br />
6月25日 <a href="../Page/埼玉超級競技場.md" title="wikilink">埼玉超級競技場</a><br />
6月26日 <a href="../Page/埼玉超級競技場.md" title="wikilink">埼玉超級競技場</a><br />
6月28日 <a href="../Page/Zepp.md" title="wikilink">Zepp Sendai</a><br />
6月29日 <a href="../Page/Zepp.md" title="wikilink">Zepp Sendai</a><br />
7月1日 新潟縣民会館<br />
7月7日 伊勢原市民文化会館 </p></td>
<td><p>全國巡迴演唱</p></td>
</tr>
<tr class="even">
<td><p><strong>AAA 6th Anniversary Tour</strong></p></td>
<td><p>9月7日 <a href="../Page/Zepp.md" title="wikilink">Zepp Tokyo</a><br />
9月8日 <a href="../Page/Zepp.md" title="wikilink">Zepp Tokyo</a><br />
9月10日 <a href="../Page/Zepp.md" title="wikilink">Zepp Sendai</a><br />
9月12日 <a href="../Page/Zepp.md" title="wikilink">Zepp Nagoya</a><br />
9月13日 <a href="../Page/Zepp.md" title="wikilink">Zepp Nagoya</a><br />
9月15日 <a href="../Page/Zepp.md" title="wikilink">Zepp Fukuoka</a><br />
9月18日 <a href="../Page/Zepp.md" title="wikilink">Zepp Sapporo</a><br />
9月27日 <a href="../Page/Zepp.md" title="wikilink">Zepp Tokyo</a><br />
9月28日 <a href="../Page/Zepp.md" title="wikilink">Zepp Tokyo</a><br />
10月3日 <a href="../Page/Zepp.md" title="wikilink">Zepp Osaka</a><br />
10月4日 <a href="../Page/Zepp.md" title="wikilink">Zepp Osaka</a> </p></td>
<td><p>首次週年紀念全國巡迴演唱會</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2012年</p></td>
<td><p><strong>AAA TOUR 2012 -777- TRIPLE SEVEN</strong></p></td>
<td><p>4月21日 長野縣文化會館<br />
4月22日 長野縣文化會館<br />
4月28日 山梨縣文化會館<br />
4月30日 市川市文化會館<br />
5月3日 アルカスSASEBO<br />
5月6日 大分縣立文化中心<br />
5月12日 鳥取縣文化會館<br />
5月13日 島根縣民会館<br />
5月20日 八王子市民会館<br />
5月23日 札幌市大會堂<br />
5月24日 札幌市大會堂<br />
5月26日 仙台太陽廣場<br />
5月27日 群馬縣民会館<br />
6月2日 新潟縣民会館<br />
6月2日 富山市藝術文化大廳<br />
6月9日 青森市文化会館<br />
6月10日 秋田縣大會堂<br />
6月13日 大宮ソニックシティ<br />
6月15日 神奈川縣大會堂<br />
6月21日 <a href="../Page/名古屋國際會議場.md" title="wikilink">名古屋國際會議場</a><br />
6月22日 <a href="../Page/名古屋國際會議場.md" title="wikilink">名古屋國際會議場</a><br />
6月24日 静岡市民文化会館<br />
6月29日 広島市文化交流会館<br />
6月30日 広島市文化交流会館<br />
7月6日 宮崎市民文化會館<br />
7月7日 鹿兒島縣文化中心<br />
7月15日 倉敷市民会館<br />
7月16日 松山市民会館<br />
7月21日 神戸国際会館<br />
7月22日 神戸国際会館<br />
9月8日 <a href="../Page/名古屋國際會議場.md" title="wikilink">名古屋國際會議場</a><br />
9月9日 <a href="../Page/名古屋國際會議場.md" title="wikilink">名古屋國際會議場</a><br />
9月13日 福岡太陽廣場<br />
9月14日 福岡太陽廣場<br />
9月22日 <a href="../Page/橫濱體育館.md" title="wikilink">橫濱體育館</a><br />
9月23日 <a href="../Page/橫濱體育館.md" title="wikilink">橫濱體育館</a><br />
9月29日 大阪城大廳<br />
9月30日 大阪城大廳<br />
10月8日 沖繩會議中心 </p></td>
<td><p>全國巡迴演唱</p></td>
</tr>
<tr class="even">
<td><p>2013年</p></td>
<td><p><strong>AAA TOUR 2013 EIGHTH WONDER</strong></p></td>
<td><p>4月20日 八王子市民会館<br />
4月21日 八王子市民会館<br />
4月25日 アルカスSASEBO<br />
4月28日 大分縣立文化中心<br />
4月29日 佐賀市文化会館<br />
5月3日 和歌山縣民文化会館<br />
5月4日 三重縣文化会館<br />
5月6日 滋賀縣立藝術劇場 琵琶湖音樂廳<br />
5月11日 金沢歌劇座<br />
5月12日 福井 鳳凰廣場<br />
5月18日 大宮ソニックシティ<br />
5月26日 横須賀芸術劇場<br />
6月1日 青森市文化会館<br />
6月2日 岩手縣民会館<br />
6月8日 神奈川縣大會堂<br />
6月9日 神奈川縣大會堂<br />
6月15日 広島市文化交流会館<br />
6月16日 広島市文化交流会館<br />
6月20日 熊本市民会館<br />
6月21日 鹿兒島縣文化中心<br />
6月23日 宮崎市民文化會館<br />
6月29日 米子會議中心<br />
6月30日 香川縣大會堂<br />
7月6日 倉敷市民会館<br />
7月7日 高知縣立文化會館<br />
7月14日 神戸国際会館<br />
7月20日 富山市藝術文化大廳<br />
7月21日 長野縣文化會館<br />
7月24日 静岡市民文化会館<br />
7月27日 新潟縣民会館<br />
7月28日 新潟縣民会館<br />
8月3日 仙台太陽廣場<br />
9月13日 大阪城大廳<br />
9月14日 大阪城大廳<br />
9月16日 大阪城大廳<br />
9月21日 <a href="../Page/埼玉超級競技場.md" title="wikilink">埼玉超級競技場</a><br />
9月22日 <a href="../Page/埼玉超級競技場.md" title="wikilink">埼玉超級競技場</a><br />
9月28日 名古屋市總合体育館<br />
9月29日 名古屋市總合体育館<br />
10月14日 福岡國際中心<br />
10月19日 札幌 真駒內積水海姆冰場 </p></td>
<td><p>出道以來最大型全國巡迴演唱</p></td>
</tr>
<tr class="odd">
<td><p>2014年</p></td>
<td><p><strong>AAA NEW YEAR PARTY 2014</strong></p></td>
<td><p>1月1日 東京體育館 </p></td>
<td><p>首次新年演唱會</p></td>
</tr>
<tr class="even">
<td><p><strong>AAA ARENA TOUR 2014 -GOLD SYMPHONY-</strong></p></td>
<td><p>5月17日 名古屋市總合体育館<br />
5月18日 名古屋市總合体育館<br />
5月31日 <a href="../Page/國立代代木競技場.md" title="wikilink">東京 國立代代木競技場第一体育館</a><br />
6月1日 <a href="../Page/國立代代木競技場.md" title="wikilink">東京 國立代代木競技場第一体育館</a><br />
6月7日 大阪城大廳<br />
6月8日 大阪城大廳<br />
6月14日 靜岡縣小笠山總合運動公園體育館<br />
6月15日 靜岡縣小笠山總合運動公園體育館<br />
6月21日 廣島縣立總合體育館<br />
6月22日 廣島縣立總合體育館<br />
6月28日 <a href="../Page/朱鷺展覽館.md" title="wikilink">朱鷺展覽館</a><br />
7月5日 宮城 積水海姆超級競技場<br />
7月12日 長野市若里多用途運動場<br />
7月13日 長野市若里多用途運動場<br />
7月19日 福井 太陽巨蛋<br />
7月20日 福井 太陽巨蛋<br />
9月14日 福岡會展中心<br />
9月15日 福岡會展中心<br />
9月20日 <a href="../Page/橫濱體育館.md" title="wikilink">橫濱體育館</a><br />
9月21日 <a href="../Page/橫濱體育館.md" title="wikilink">橫濱體育館</a><br />
9月27日 名古屋市總合体育館<br />
9月28日 名古屋市總合体育館<br />
10月4日 大阪城大廳<br />
10月5日 大阪城大廳<br />
10月18日 札幌 真駒內積水海姆冰場 </p></td>
<td><p>首次全國Arena巡迴演唱會</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2015年</p></td>
<td><p><strong>AAA NEW YEAH PARTY 2015</strong></p></td>
<td><p>1月1日 東京體育館 </p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>WAKUWAKU JAPAN Presents AAA ASIA TOUR 2015 -ATTACK ALL AROUND- Supported by KOJI</strong></p></td>
<td><p>4月4日 <a href="../Page/新加坡.md" title="wikilink">新加坡</a> SCAPE<br />
4月6日 <a href="../Page/香港.md" title="wikilink">香港</a><a href="../Page/九龍灣國際展貿中心.md" title="wikilink">九龍灣國際展貿中心</a> Music Zone@E-Max<br />
4月8日 <a href="../Page/印尼.md" title="wikilink">印尼雅加達Upper</a> Room<br />
4月11日 <a href="../Page/台灣.md" title="wikilink">台灣</a><a href="../Page/ATT_4_FUN.md" title="wikilink">ATT Show Box</a> </p></td>
<td><p>首次亞洲巡迴演唱會</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>AAA ARENA TOUR 2015 10th Anniversary -Attack All Around-</strong></p></td>
<td><p>5月1日 <a href="../Page/日本武道館.md" title="wikilink">日本武道館</a><br />
5月2日 <a href="../Page/日本武道館.md" title="wikilink">日本武道館</a><br />
5月5日 福岡會展中心<br />
5月6日 福岡會展中心<br />
5月16日 廣島縣立總合體育館<br />
5月17日 廣島縣立總合體育館<br />
5月23日 名古屋市總合體育館<br />
5月24日 名古屋市總合體育館<br />
5月30日 福井 太陽巨蛋<br />
6月17日 大阪城大廳<br />
6月18日 大阪城大廳<br />
6月27日 札幌 真駒內積水海姆冰場<br />
7月4日 仙台 Xebio體育館<br />
7月5日 仙台 Xebio體育館<br />
7月22日 <a href="../Page/日本武道館.md" title="wikilink">日本武道館</a><br />
7月23日 <a href="../Page/日本武道館.md" title="wikilink">日本武道館</a> </p></td>
<td><p>全國Arena巡迴演唱會</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>AAA 10th Anniversary LIVE</strong></p></td>
<td><p>9月13日 <a href="../Page/國立代代木競技場.md" title="wikilink">東京 國立代代木競技場第一体育館</a><br />
9月14日 <a href="../Page/國立代代木競技場.md" title="wikilink">東京 國立代代木競技場第一体育館</a> </p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>AAA 10th Anniversary SPECIAL 野外LIVE in 富士急高原樂園</strong></p></td>
<td><p>9月21日 <a href="../Page/富士急高原樂園.md" title="wikilink">富士急高原樂園</a><br />
9月22日 <a href="../Page/富士急高原樂園.md" title="wikilink">富士急高原樂園</a><br />
9月23日 <a href="../Page/富士急高原樂園.md" title="wikilink">富士急高原樂園</a> </p></td>
<td><p>首次戶外演唱會</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>AAA Hawaii FANCLUB EVENT TOUR -SPECIAL LIVE-</strong></p></td>
<td><p>10月4日 <a href="../Page/檀香山.md" title="wikilink">檀香山</a> </p></td>
<td><p>官方後援會<a href="../Page/夏威夷.md" title="wikilink">夏威夷活動的戶外演唱會部分</a>，14曲。</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2016年</p></td>
<td><p><strong>AAA ARENA TOUR 2016 -LEAP OVER-</strong></p></td>
<td><p>5月11日 福岡會展中心<br />
5月12日 福岡會展中心<br />
5月21日 幕張展覽館 9-11 Hall<br />
5月22日 幕張展覽館 9-11 Hall<br />
5月26日 名古屋市総合体育館<br />
5月28日 名古屋市総合体育館<br />
5月29日 名古屋市総合体育館<br />
6月2日 大阪城大廳<br />
6月4日 大阪城大廳<br />
6月5日 大阪城大廳<br />
6月18日 廣島縣立總合體育館<br />
6月19日 廣島縣立總合體育館<br />
6月25日 朱鷺展覽館<br />
6月26日 朱鷺展覽館<br />
7月2日 幕張展覽館 9-11 Hall<br />
7月3日 幕張展覽館 9-11 Hall<br />
7月9日 札幌 真駒內積水海姆冰場<br />
7月10日 札幌 真駒內積水海姆冰場<br />
7月15日 福井 太陽巨蛋<br />
7月16日 福井 太陽巨蛋<br />
7月23日 宮城 積水海姆超級競技場<br />
7月24日 宮城 積水海姆超級競技場 </p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>AAA ASIA TOUR 2016</strong></p></td>
<td><p>8月12日 <a href="../Page/台灣.md" title="wikilink">台灣</a> <a href="../Page/ATT_4_FUN.md" title="wikilink">ATT Show Box</a><br />
8月13日 台灣 ATT Show Box<br />
8月14日 台灣 ATT Show Box<br />
9月3日 <a href="../Page/新加坡.md" title="wikilink">新加坡</a> SCAPE<br />
9月4日 新加坡 SCAPE </p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>AAA Special Live 2016 in Dome -FANTASTIC OVER-</strong></p></td>
<td><p>11月12日 <a href="../Page/大阪巨蛋.md" title="wikilink">京瓷巨蛋大阪</a><br />
11月13日 京瓷巨蛋大阪<br />
11月15日 <a href="../Page/東京巨蛋.md" title="wikilink">東京巨蛋</a> ※追加公演<br />
11月16日 東京巨蛋</p></td>
<td><p>首次巨蛋演唱會</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2017年</p></td>
<td><p><strong>AAA ARENA TOUR 2017 -WAY OF GLORY-</strong></p></td>
<td><p>6月17日 廣島 綠色競技場<br />
6月18日 廣島 綠色競技場<br />
6月24日 福井 太陽巨蛋<br />
6月25日 福井 太陽巨蛋<br />
6月30日 宮城 積水海姆超級競技場<br />
7月1日 宮城 積水海姆超級競技場<br />
7月8日 埼玉 超級競技場<br />
7月9日 埼玉 超級競技場<br />
7月15日 札幌 真駒內積水海姆冰場<br />
7月16日 札幌 真駒內積水海姆冰場 </p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>AAA DOME TOUR 2017 - WAY OF GLORY -</strong></p></td>
<td><p>9月2日 <a href="../Page/名古屋巨蛋.md" title="wikilink">名古屋巨蛋</a><br />
9月3日 名古屋巨蛋<br />
9月8日 <a href="../Page/大阪巨蛋.md" title="wikilink">京瓷巨蛋大阪</a><br />
9月9日 京瓷巨蛋大阪<br />
9月13日 <a href="../Page/東京巨蛋.md" title="wikilink">東京巨蛋</a><br />
9月14日 東京巨蛋<br />
9月30日 <a href="../Page/福岡巨蛋.md" title="wikilink">福岡巨蛋</a><br />
10月1日 福岡巨蛋</p>
</div>
</div></td>
<td><p>首次巨蛋巡迴演唱會</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2018年</p></td>
<td><p><strong>AAA NEW YEAH PARTY 2018</strong></p></td>
<td><p>1月1日 東京體育館 </p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>AAA FAN MEETING ARENA TOUR 2018 ～FAN FUN FAN～</strong></p></td>
<td><p>5月26日 大阪城大廳※<br />
5月27日 大阪城大廳<br />
6月2日 名古屋市総合體育館※<br />
6月3日 名古屋市総合體育館<br />
6月16日 宮城 積水海姆超級競技場※<br />
6月30日 札幌 真駒內積水海姆冰場※<br />
7月7日 埼玉 超級競技場※<br />
7月8日 埼玉 超級競技場<br />
7月15日 福岡會展中心<br />
7月16日 福岡會展中心※<br />
※為1日2回公演</p>
</div>
</div></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>AAA DOME TOUR 2018 -COLOR A LIFE-</strong></p></td>
<td><p>9月1日 <a href="../Page/東京巨蛋.md" title="wikilink">東京巨蛋</a><br />
9月2日 <a href="../Page/東京巨蛋.md" title="wikilink">東京巨蛋</a><br />
9月3日 <a href="../Page/東京巨蛋.md" title="wikilink">東京巨蛋</a><br />
9月15日 <a href="../Page/名古屋巨蛋.md" title="wikilink">名古屋巨蛋</a><br />
9月16日 <a href="../Page/名古屋巨蛋.md" title="wikilink">名古屋巨蛋</a><br />
9月29日 <a href="../Page/大阪巨蛋.md" title="wikilink">京瓷巨蛋大阪</a><br />
9月30日 <a href="../Page/大阪巨蛋.md" title="wikilink">京瓷巨蛋大阪</a><br />
11月23日 <a href="../Page/福岡巨蛋.md" title="wikilink">福岡巨蛋</a><br />
11月24日 <a href="../Page/福岡巨蛋.md" title="wikilink">福岡巨蛋</a> </p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 出演及其他活動

### 背景音樂

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>作品</p></th>
<th><p>引用方</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2005年</p></td>
<td><p><a href="../Page/BLOOD_on_FIRE.md" title="wikilink">BLOOD on FIRE</a></p></td>
<td><p>電影《<a href="../Page/頭文字D.md" title="wikilink">頭文字D</a>》主題歌<br />
<a href="../Page/東京電視臺.md" title="wikilink">東京電視臺</a>《激走!GT》開場曲</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Friday_Party.md" title="wikilink">Friday Party</a></p></td>
<td><p><a href="../Page/日本電視台.md" title="wikilink">日本電視台節目</a>《スポーツうるぐす》結束曲<br />
日本電視台節目《<a href="../Page/音樂戰士.md" title="wikilink">音樂戰士</a>》結束曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/美麗的天空.md" title="wikilink">美麗的天空</a></p></td>
<td><p>日本電視台節目《TVおじゃマンボウ》結束曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/DRAGON_FIRE.md" title="wikilink">DRAGON FIRE</a></p></td>
<td><p><a href="../Page/TBS電視台.md" title="wikilink">TBS電視台節目</a>《アイチテル!》結束曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2006年</p></td>
<td><p><a href="../Page/哈利路亞_(AAA單曲).md" title="wikilink">哈利路亞</a></p></td>
<td><p>TBS電視台節目《ガチバカ!》開場曲</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Shalala_希望之歌.md" title="wikilink">Shalala 希望之歌</a></p></td>
<td><p><a href="../Page/日本海電視台.md" title="wikilink">日本海電視台節目</a>《プリン・ス》開場曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/颶風·莉莉，波士頓·瑪麗.md" title="wikilink">颶風·莉莉，波士頓·瑪麗</a></p></td>
<td><p><a href="../Page/朝日電視台.md" title="wikilink">朝日電視台電視劇</a>《<a href="../Page/7人女律師.md" title="wikilink">7人女律師</a>》主題歌</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/刀魂男孩_/_和服噴射女孩.md" title="wikilink">刀魂男孩</a></p></td>
<td><p>網路遊戲<a href="../Page/仙境傳說.md" title="wikilink">仙境傳說的主題曲</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Let_it_beat!.md" title="wikilink">Let it beat!</a></p></td>
<td><p>電影《<a href="../Page/秋葉原@DEEP.md" title="wikilink">秋葉原@DEEP</a>》主題歌</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/&quot;Q&quot;.md" title="wikilink">"Q"</a></p></td>
<td><p>「第26屆全國高中測驗錦標賽」應援歌曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2007年</p></td>
<td><p><a href="../Page/CCC_-CHALLENGE_COVER_COLLECTION-.md" title="wikilink">全民明星!</a></p></td>
<td><p>電影《<a href="../Page/歌舞青春.md" title="wikilink">歌舞青春</a>》宣傳歌</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Climax_Jump.md" title="wikilink">Climax Jump</a></p></td>
<td><p>朝日電視臺特攝電視劇《<a href="../Page/假面騎士電王.md" title="wikilink">假面騎士電王</a>》主題曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Get_啾!_/_SHE的真相.md" title="wikilink">Get 啾!</a></p></td>
<td><p>House食品「フルーチェ ハンディータイプ」電視廣告歌曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Get_啾!_/_SHE的真相.md" title="wikilink">SHE的真相</a></p></td>
<td><p>House食品「フルーチェ Cの果実」電視廣告歌曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/紅唇羅曼蒂卡_/_That&#39;s_Right.md" title="wikilink">紅唇羅曼蒂卡</a></p></td>
<td><p><a href="../Page/東京電視台.md" title="wikilink">東京電視台電視劇</a>「美味學院」的主題曲和插曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/紅唇羅曼蒂卡_/_That&#39;s_Right.md" title="wikilink">That's Right</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/夏日風物.md" title="wikilink">SUNSHINE</a></p></td>
<td><p>日本電視台節目《スポーツうるぐす》6月〜8月的主題曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/AROUND_(AAA專輯).md" title="wikilink">邂逅的力量II</a></p></td>
<td><p>朝日電視臺特攝電視劇《<a href="../Page/假面騎士電王.md" title="wikilink">假面騎士電王</a>》插曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2008年</p></td>
<td><p><a href="../Page/BEYOND～超越極限.md" title="wikilink">BEYOND～超越極限</a></p></td>
<td><p><a href="../Page/美津濃.md" title="wikilink">美津濃</a>（Mizuno）的運動員應援歌曲</p></td>
</tr>
<tr class="even">
<td><p>[[MUSIC</p></td>
<td><p>!_/_ZERO|ZERØ]]</p></td>
<td><p>動畫「<a href="../Page/世界毀滅.md" title="wikilink">World Destruction~毀滅世界的六人~</a>」的主題曲</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/啟程之歌_(AAA單曲).md" title="wikilink">Mosaic</a></p></td>
<td><p><a href="../Page/關西電視台.md" title="wikilink">關西電視台電視劇</a>「新世紀莎士比亞」的主題曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/啟程之歌_(AAA單曲).md" title="wikilink">a piece of my word</a></p></td>
<td><p>京都和服「友禪」廣告歌曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2009年</p></td>
<td><p><a href="../Page/Break_Down_/_Break_your_name_/_Summer_Revolution.md" title="wikilink">Break Down</a></p></td>
<td><p><a href="../Page/每日放送.md" title="wikilink">每日放送和TBS電視台電視劇</a>「帝王」的主題曲</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Break_Down_/_Break_your_name_/_Summer_Revolution.md" title="wikilink">Break your name</a></p></td>
<td><p><a href="../Page/富士電視台.md" title="wikilink">富士電視台音樂節目</a>「HEY!HEY!HEY!MUSIC CHAMP」片尾曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Break_Down_/_Break_your_name_/_Summer_Revolution.md" title="wikilink">Summer Revolution</a></p></td>
<td><p>「プロミス集團」電視廣告歌曲<br />
<a href="../Page/TOKYO_MX.md" title="wikilink">TOKYO MX</a> 2009年高中棒球比賽的主題曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Hide-away.md" title="wikilink">Hide-away</a></p></td>
<td><p>「White Sacas」主題曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Hide-away.md" title="wikilink">With you</a></p></td>
<td><p>日本電視台動畫「犬夜叉 完結篇」的片尾曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2010年</p></td>
<td><p><a href="../Page/Heart_and_Soul_(AAA單曲).md" title="wikilink">Heart and Soul</a></p></td>
<td><p>TBS電視台節目「にうすざんす」主題曲</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Heart_and_Soul_(AAA單曲).md" title="wikilink">TWO ROADS</a></p></td>
<td><p>電影「<a href="../Page/再見，總有一天.md" title="wikilink">再見，總有一天</a>」插曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/HEARTFUL.md" title="wikilink">As I am</a></p></td>
<td><p>電影「幽會!」（ランデブー!）主題曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/HEARTFUL.md" title="wikilink">FIELD</a></p></td>
<td><p>「進研ゼミ-中學講座- 畢業・進級應援活動」電視廣告歌曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/想見你的理由_/_Dream_After_Dream_～夢醒之夢～.md" title="wikilink">想見你的理由</a></p></td>
<td><p>日本電視台節目「二匹目のどじょう」片尾曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/不屈的心.md" title="wikilink">不屈的心</a></p></td>
<td><p>朝日電視台電視劇「懸崖邊的繪里子」主題曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/不屈的心.md" title="wikilink">Day by day</a></p></td>
<td><p>「avex artist academy」廣告歌曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/PARADISE_/_Endless_Fighters.md" title="wikilink">PARADISE</a></p></td>
<td><p><a href="../Page/伊藤洋華堂.md" title="wikilink">伊藤洋華堂</a>「Body Heater」電視廣告歌曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/PARADISE_/_Endless_Fighters.md" title="wikilink">Endless Fighters</a></p></td>
<td><p>東京電視台節目「Pokemon Smash!」片尾曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2011年</p></td>
<td><p><a href="../Page/重要的事.md" title="wikilink">重要的事</a></p></td>
<td><p>富士電視台節目「ウチくる!?」片尾曲<br />
伊藤洋華堂「新生活」電視廣告歌曲</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Buzz_Communication.md" title="wikilink">STEP</a></p></td>
<td><p>「進研ゼミ-中學講座- 畢業・進級應援活動」電視廣告歌曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Charge_▶_Go!_/_Lights.md" title="wikilink">Charge ▶ Go!</a></p></td>
<td><p><a href="../Page/森永製菓.md" title="wikilink">森永製菓</a>「Wieder in 果凍」電視廣告歌曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/No_cry_No_more.md" title="wikilink">No cry No more</a></p></td>
<td><p>伊藤洋華堂「Body Cooler」電視廣告歌曲<br />
伊藤洋華堂「White Biz」電視廣告歌曲<br />
伊藤洋華堂「Cooler Magic / Dry Magic」電視廣告歌曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/CALL_/_I4U.md" title="wikilink">CALL</a></p></td>
<td><p>伊藤洋華堂「Body Heater - Heat Ring」電視廣告歌曲<br />
伊藤洋華堂「Body Heater @ Home」電視廣告歌曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/CALL_/_I4U.md" title="wikilink">I4U</a></p></td>
<td><p>電影「網球王子劇場版 決戰英國網球城! 」主題歌<br />
伊藤洋華堂「goodday」的電視廣告歌曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Charge_▶_Go!_/_Lights.md" title="wikilink">Lights</a></p></td>
<td><p>伊藤洋華堂「Kent in Tradition」的電視廣告歌曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2012年</p></td>
<td><p><a href="../Page/SAILING.md" title="wikilink">SAILING</a></p></td>
<td><p>伊藤洋華堂「7days@HOME」電視廣告歌曲<br />
伊藤洋華堂「good day 2012 春」電視廣告歌曲</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/SAILING.md" title="wikilink">WISHES</a></p></td>
<td><p>伊藤洋華堂「Kent 2012 春夏」電視廣告歌曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Still_Love_You.md" title="wikilink">Still Love You</a></p></td>
<td><p>伊藤洋華堂「Body Cooler」電視廣告歌曲<br />
伊藤洋華堂「Cool Biz」電視廣告歌曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/777_～We_can_sing_a_song!～.md" title="wikilink">777 ～We can sing a song!～</a></p></td>
<td><p>伊藤洋華堂「Cool Magic」電視廣告歌曲<br />
伊藤洋華堂「」電視廣告歌曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/777_～TRIPLE_SEVEN～.md" title="wikilink">We Are!</a></p></td>
<td><p>動畫「<a href="../Page/海賊王.md" title="wikilink">海賊王</a> 南美編 航海士淚水和友誼之羈絆」主題曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/虹_(AAA單曲).md" title="wikilink">虹</a></p></td>
<td><p>KONAMI「職業棒球Dream Nine」商業搭配歌曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/虹_(AAA單曲).md" title="wikilink">good day</a></p></td>
<td><p>伊藤洋華堂「goodday秋冬2012」電視廣告歌曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Miss_you_/_綻放笑容的場所.md" title="wikilink">Miss you</a></p></td>
<td><p>H.I.S.「蜜月編」電視廣告歌曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Miss_you_/_綻放笑容的場所.md" title="wikilink">綻放笑容的場所</a></p></td>
<td><p>伊藤洋華堂「Kent」電視廣告歌曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2013年</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/PARTY_IT_UP.md" title="wikilink">PARTY IT UP</a></p></td>
<td><p>伊藤洋華堂「Cool Biz」電視廣告歌曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Love_Is_In_The_Air.md" title="wikilink">Love Is In The Air</a></p></td>
<td><p>伊藤洋華堂「Cool Style」電視廣告歌曲<br />
伊藤洋華堂「」電視廣告歌曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/彩虹_(AAA單曲).md" title="wikilink">good day</a></p></td>
<td><p>伊藤洋華堂「goodday」電視廣告歌曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/戀歌與雨天.md" title="wikilink">戀歌與雨天</a></p></td>
<td><p>H.I.S.中部秋天競選活動的廣告歌曲<br />
伊藤洋華堂「あったかほっこりシリーズ」電視廣告歌曲<br />
伊藤洋華堂「Kent」電視廣告歌曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/戀歌與雨天.md" title="wikilink">MASK</a></p></td>
<td><p>電影「Tiger Mask」主題曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Eighth_Wonder_(AAA).md" title="wikilink">Eighth Wonder</a></p></td>
<td><p><a href="../Page/BeeTV.md" title="wikilink">BeeTV電視劇</a>「危險關係」主題曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2014年</p></td>
<td><p><a href="../Page/Love_(AAA單曲).md" title="wikilink">Love</a></p></td>
<td><p>日本紅十字會2014年捐血活動《二十歲的捐血》宣傳歌曲</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Wake_up!.md" title="wikilink">Wake up!</a></p></td>
<td><p><a href="../Page/富士電視台.md" title="wikilink">富士電視台動畫</a>「<a href="../Page/海賊王.md" title="wikilink">海賊王</a>」主題曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/GOLD_SYMPHONY.md" title="wikilink">HANDs</a></p></td>
<td><p>讀賣電視電視劇「特防部 警察廳特殊防犯課」主題曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Wake_up!.md" title="wikilink">微風徐徐的夏日記憶</a></p></td>
<td><p>伊藤洋華堂「Cool Style」電視廣告歌曲<br />
伊藤洋華堂「」電視廣告歌曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/說再見之前.md" title="wikilink">Next Stage</a></p></td>
<td><p>富士電視台動畫「ONE PIECE "3D2Y" 跨越艾斯之死！路飛與夥伴的誓言」主題曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/GOLD_SYMPHONY.md" title="wikilink">SHOUT &amp; SHAKE</a></p></td>
<td><p>ALPEN「衝撃スノーボードバーゲン」電視廣告歌曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/說再見之前.md" title="wikilink">說再見之前</a></p></td>
<td><p>伊藤洋華堂「Warm Style」電視廣告歌曲<br />
伊藤洋華堂「Kent『Touch Tradition』」電視廣告歌曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/GOLD_SYMPHONY.md" title="wikilink">V.O.L</a></p></td>
<td><p>VIP「Geo World VIP」電視廣告歌曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2015年</p></td>
<td><p><a href="../Page/Lil&#39;_Infinity.md" title="wikilink">Lil' Infinity</a></p></td>
<td><p>電影『<a href="../Page/生命中的美好缺憾_(電影).md" title="wikilink">生命中的美好缺憾</a>』主題曲</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/我的憂鬱與不開心的女朋友.md" title="wikilink">我的憂鬱與不開心的女朋友</a></p></td>
<td><p><a href="../Page/伊藤洋華堂.md" title="wikilink">伊藤洋華堂</a>「Kent in Tradition」電視廣告歌曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/GAME_OVER?.md" title="wikilink">GAME OVER?</a></p></td>
<td><p><a href="../Page/朝日電視台.md" title="wikilink">朝日電視台節目</a>「ドラマ！7人のアイドルゴーゴー！」主題曲<br />
手機遊戲《<a href="../Page/魔法氣泡系列.md" title="wikilink">魔法氣泡</a>》電視廣告歌曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/明日之光.md" title="wikilink">明日之光</a></p></td>
<td><p><a href="../Page/朝日電視台.md" title="wikilink">朝日電視台動畫</a>「<a href="../Page/境界觸發者.md" title="wikilink">境界觸發者</a>」主題曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>[[MUSIC</p></td>
<td><p>!_/_ZERO|MUSIC</p></td>
<td><p>!]]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/AAA_10th_ANNIVERSARY_BEST.md" title="wikilink">STORY</a></p></td>
<td><p>伊藤洋華堂「」電視廣告歌曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Flavor_of_kiss.md" title="wikilink">Flavor of kiss</a></p></td>
<td><p><a href="../Page/江崎固力果.md" title="wikilink">江崎固力果</a>《Seventeen ice》宣傳活動歌曲<br />
全道Netz Toyota『北海道限定RAV4特別規格車』電視廣告歌曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/LOVER.md" title="wikilink">LOVER</a></p></td>
<td><p><a href="../Page/伊藤洋華堂.md" title="wikilink">伊藤洋華堂</a>×AAA 『夏季飲料系列』電視廣告歌曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/深愛著，卻不能愛.md" title="wikilink">深愛著，卻不能愛</a></p></td>
<td><p>「Open House」電視廣告歌曲<br />
「LIVE DAM STADIUM」電視廣告歌曲<br />
伊藤洋華堂「WARM STYLE 2015」電視廣告歌曲<br />
<a href="../Page/朝日電視台.md" title="wikilink">朝日電視台節目</a>「プロマーシャル」插曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/AAA_10th_ANNIVERSARY_BEST.md" title="wikilink">Distance</a></p></td>
<td><p>手機遊戲《<a href="../Page/魔法氣泡系列.md" title="wikilink">魔法氣泡</a>》電視廣告歌曲<br />
VIP「Geo World VIP」電視廣告歌曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/沒有眼淚的世界.md" title="wikilink">Yell</a></p></td>
<td><p>全日本空手道連盟公認應援歌曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2016年</p></td>
<td><p><a href="../Page/NEW_(AAA單曲).md" title="wikilink">NEW</a></p></td>
<td><p>飲品品牌<a href="../Page/雪碧.md" title="wikilink">雪碧</a>「NEW すっきり爽快篇」電視廣告歌曲</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/沒有眼淚的世界.md" title="wikilink">Yell</a></p></td>
<td><p>進研ゼミプラス「勝ちにいく夏篇」電視廣告歌曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/沒有眼淚的世界.md" title="wikilink">沒有眼淚的世界</a></p></td>
<td><p><a href="../Page/日本電視台.md" title="wikilink">日本電視台節目</a>「スッキリ！！」10月的主題曲<br />
<a href="../Page/豪斯登堡.md" title="wikilink">豪斯登堡</a>「光之王國」電視廣告歌曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/沒有眼淚的世界.md" title="wikilink">Jewel</a></p></td>
<td><p>手機遊戲《<a href="../Page/魔法氣泡系列.md" title="wikilink">魔法氣泡</a>》電視廣告歌曲<br />
VIP「Geo World VIP」電視廣告歌曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2017年</p></td>
<td><p><a href="../Page/MAGIC_(AAA單曲).md" title="wikilink">MAGIC</a></p></td>
<td><p><a href="../Page/朝日電視台.md" title="wikilink">朝日電視台電視劇</a>『奪い愛、冬』主題曲</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/WAY_OF_GLORY.md" title="wikilink">S.O.L</a></p></td>
<td><p>時尚雜誌《<a href="../Page/CanCam.md" title="wikilink">CanCam</a>》2017年4月號電視廣告歌曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/No_Way_Back.md" title="wikilink">No Way Back</a></p></td>
<td><p>飲品品牌冰結（R）「氷結(R)×AAA Original WEB」的電視廣告歌曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/No_Way_Back.md" title="wikilink">Beat Together</a></p></td>
<td><p>LAGUNA TEN BOSCH的夏季活動推廣歌曲<br />
時尚雜誌《<a href="../Page/CanCam.md" title="wikilink">CanCam</a>》2017年10月號電視廣告歌曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/LIFE_(AAA單曲).md" title="wikilink">LIFE</a></p></td>
<td><p><a href="../Page/富士電視台.md" title="wikilink">富士電視台電視劇</a>『民衆の敵 〜世の中、おかしくないですか!?〜』主題曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2018年</p></td>
<td><p><a href="../Page/Eighth_Wonder.md" title="wikilink">drama</a></p></td>
<td><p><a href="../Page/AbemaTV.md" title="wikilink">AbemaTV電視劇</a>『真冬のオオカミくんには騙されない』片尾曲</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/COLOR_A_LIFE.md" title="wikilink">Tomorrow</a></p></td>
<td><p><a href="../Page/富士電視台週二晚間九點連續劇.md" title="wikilink">富士電視台火9電視劇</a>《<a href="../Page/健康而有文化的最低限度生活.md" title="wikilink">健康而有文化的最低限度生活</a>》主題曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/COLOR_A_LIFE.md" title="wikilink">Kiss the Sky</a></p></td>
<td><p><a href="../Page/AbemaTV.md" title="wikilink">AbemaTV和</a><a href="../Page/名古屋電視台.md" title="wikilink">名古屋電視台合拍劇</a>《<a href="../Page/星塵復仇者們.md" title="wikilink">星塵復仇者們</a>》主題曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/COLOR_A_LIFE.md" title="wikilink">DEJAVU</a></p></td>
<td><p>d Music powered by Recochoku電視廣告歌曲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/COLOR_A_LIFE.md" title="wikilink">C.O.L</a></p></td>
<td><p>時尚雜誌《<a href="../Page/CanCam.md" title="wikilink">CanCam</a>》2018年10月號電視廣告歌曲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/笑臉的循環.md" title="wikilink">笑臉的循環</a></p></td>
<td><p><a href="../Page/NHK.md" title="wikilink">NHK節目</a>「」2018年12月份至2019年1月份放送曲歌曲之一</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 廣告

<table>
<thead>
<tr class="header">
<th><p>企業</p></th>
<th><p>品牌</p></th>
<th><p>年份</p></th>
<th><p>出演</p></th>
<th><p>背景音樂</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>迪士尼頻道</p></td>
<td><p>歌舞青春</p></td>
<td><p>2007年</p></td>
<td><p>全員</p></td>
<td><p>みんなスター!（全民明星）</p></td>
</tr>
<tr class="even">
<td><p>House食品</p></td>
<td><p>フルーチェ ハンディータイプ</p></td>
<td><p>2007年</p></td>
<td><p>全員</p></td>
<td><p><a href="../Page/Get_啾!_/_SHE的真相.md" title="wikilink">Get 啾!</a></p></td>
</tr>
<tr class="odd">
<td><p>邦民日本財務</p></td>
<td><p>プロミスグループ</p></td>
<td><p>2009年</p></td>
<td><p>全員</p></td>
<td><p><a href="../Page/Break_Down_/_Break_your_name_/_Summer_Revolution.md" title="wikilink">Summer Revolution</a></p></td>
</tr>
<tr class="even">
<td><p>SEVEN&amp;i <a href="../Page/伊藤洋華堂.md" title="wikilink">伊藤洋華堂</a></p></td>
<td><p>Body Heater</p></td>
<td><p>2010年</p></td>
<td><p>西島</p></td>
<td><p><a href="../Page/PARADISE_/_Endless_Fighters.md" title="wikilink">PARADISE</a></p></td>
</tr>
<tr class="odd">
<td><p>新生活</p></td>
<td><p>2011年</p></td>
<td><p>全員</p></td>
<td><p><a href="../Page/重要的事.md" title="wikilink">重要的事</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>Body Cooler / White Biz</p></td>
<td><p>西島、浦田、日高、與、末吉</p></td>
<td><p><a href="../Page/No_cry_No_more.md" title="wikilink">No cry No more</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Cooler Magic / Dry Magic</p></td>
<td><p>宇野、伊藤</p></td>
<td><p><a href="../Page/No_cry_No_more.md" title="wikilink">No cry No more</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>Body Heater - Heat Ring</p></td>
<td><p>西島、浦田、日高、與、末吉</p></td>
<td><p><a href="../Page/CALL_/_I4U.md" title="wikilink">CALL</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Body Heater @ Home</p></td>
<td><p>宇野、伊藤</p></td>
<td><p><a href="../Page/CALL_/_I4U.md" title="wikilink">CALL</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>good day</p></td>
<td><p>全員</p></td>
<td><p><a href="../Page/CALL_/_I4U.md" title="wikilink">I4U</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Kent in Tradition</p></td>
<td><p>西島</p></td>
<td><p><a href="../Page/Charge_▶_Go!_/_Lights.md" title="wikilink">Lights</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>7day@HOME</p></td>
<td><p>2012年</p></td>
<td><p>全員</p></td>
<td><p><a href="../Page/SAILING.md" title="wikilink">SAILING</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>good day 2012 春</p></td>
<td><p>全員</p></td>
<td><p><a href="../Page/SAILING.md" title="wikilink">SAILING</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>Kent 2012 春夏</p></td>
<td><p>西島</p></td>
<td><p><a href="../Page/SAILING.md" title="wikilink">WISHES</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Body Cooler 2012 / Cool Biz</p></td>
<td><p>西島、浦田、日高、與、末吉</p></td>
<td><p><a href="../Page/Still_Love_You.md" title="wikilink">Still Love You</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>Cool Magic</p></td>
<td><p>宇野、伊藤</p></td>
<td><p><a href="../Page/777_～We_can_sing_a_song!～.md" title="wikilink">777 ～We can sing a song!～</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>全員</p></td>
<td><p><a href="../Page/777_～We_can_sing_a_song!～.md" title="wikilink">777 ～We can sing a song!～</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>WARM STYLE</p></td>
<td><p>宇野、伊藤</p></td>
<td><p>（無）</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>goodday秋冬2012</p></td>
<td><p>全員</p></td>
<td><p><a href="../Page/虹_(AAA單曲).md" title="wikilink">good day</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>Kent in Tradition</p></td>
<td><p>西島</p></td>
<td><p><a href="../Page/Miss_you/笑容綻放的地方.md" title="wikilink">笑容綻放的地方</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Cool Biz 2013</p></td>
<td><p>2013年</p></td>
<td><p>西島、浦田、日高、與、末吉</p></td>
<td><p><a href="../Page/PARTY_IT_UP.md" title="wikilink">PARTY IT UP</a></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>全員</p></td>
<td><p><a href="../Page/Love_Is_In_The_Air.md" title="wikilink">Love Is In The Air</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Cool Style</p></td>
<td><p>宇野、伊藤</p></td>
<td><p><a href="../Page/Love_Is_In_The_Air.md" title="wikilink">Love Is In The Air</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>good day</p></td>
<td><p>全員</p></td>
<td><p>good day</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>WARM STYLE</p></td>
<td><p>宇野、伊藤</p></td>
<td><p><a href="../Page/戀歌與雨天.md" title="wikilink">戀歌與雨天</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>Kent</p></td>
<td><p>西島</p></td>
<td><p><a href="../Page/戀歌與雨天.md" title="wikilink">戀歌與雨天</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Cool Style</p></td>
<td><p>2014年</p></td>
<td><p>西島、宇野、與、伊藤</p></td>
<td><p><a href="../Page/Wake_up!.md" title="wikilink">微風徐徐的夏日記憶</a></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>全員</p></td>
<td><p><a href="../Page/Wake_up!.md" title="wikilink">微風徐徐的夏日記憶</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>WARM STYLE</p></td>
<td><p>西島、宇野、與、伊藤</p></td>
<td><p><a href="../Page/說再見之前.md" title="wikilink">說再見之前</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>Kent</p></td>
<td><p>西島</p></td>
<td><p><a href="../Page/說再見之前.md" title="wikilink">說再見之前</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>kent</p></td>
<td><p>2015年</p></td>
<td><p>西島</p></td>
<td><p><a href="../Page/我的憂鬱與不開心的她.md" title="wikilink">我的憂鬱與不開心的她</a></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 時尚博覽

| 主辦方                                                          | 博覽標題                                                         | 年度         | 出演 | 支援單位                                      |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ---------- | -- | ----------------------------------------- |
| Girls Award實行委員會、[富士電視台](../Page/富士電視台.md "wikilink")        | Girls Award 2011 SPRING/SUMMER by CROOZ blog                 | 2011年      | 全員 | 外務省、觀光廳、東京都政府、澀谷區政府等                      |
| TOKYO GIRLS COLLECTION實行委員會                                  | TOKYO GIRLS COLLECTION 2012 AUTUMN/WINTER by girlswalker.com | 2012年      | 全員 | 外務省、 Visit Japan campaign實施本部、東京都政府、澀谷區政府 |
| TOKYO GIRLS COLLECTION 2013 AUTUMN/WINTER by girlswalker.com | 2013年                                                        | 西島、宇野、伊藤   |    |                                           |
| TOKYO GIRLS COLLECTION 2014 SPRING/SUMMER by girlswalker.com | 2014年                                                        | 宇野、浦田、與、伊藤 |    |                                           |
| TOKYO GIRLS COLLECTION 2014 AUTUMN/WINTER by girlswalker.com | 2014年                                                        | 西島、宇野、與、伊藤 |    |                                           |
|                                                              |                                                              |            |    |                                           |

### 甄選會

| 主辦方          | 甄選名稱                | 年份                | 出演      | 備註   |
| ------------ | ------------------- | ----------------- | ------- | ---- |
| 主婦與生活社       | ジュノン・スーパーボーイ・コンテスト  | 2011年（24回）\[201\] | 西島、宇野、與 | 決賽評委 |
| SEVEN & i    | KIDS DANCE FESTIVAL | 2012年\[202\]      | 宇野、末吉   | 決賽評委 |
| 2013年\[203\] |                     |                   |         |      |
| 2014年\[204\] |                     |                   |         |      |
| 2015年        |                     |                   |         |      |
|              |                     |                   |         |      |

### 社會活動

| 主辦方                        | 活動名稱                              | 年份    | 出演    | 備註                           |
| -------------------------- | --------------------------------- | ----- | ----- | ---------------------------- |
| 横浜税関                       | 横浜税関134周年記念税関記念日式典                | 2006年 | 全員    | 擔任一天海關長官，成員分別負責不同的關口。\[205\] |
| TOKYO MOVE UP PROJECT實行委員会 | 東京奧林匹克運動會申辦活動「TOKYO MOVE UP特別講談會」 | 2009年 | 宇野、日高 | 音樂界代表，地點東京ミッドタウンホール。\[206\]  |
|                            |                                   |       |       |                              |

## 實績

### 獲獎記錄

<table>
<thead>
<tr class="header">
<th><p>作品</p></th>
<th><p>頒獎信息</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2005年</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/BLOOD_on_FIRE.md" title="wikilink">BLOOD on FIRE</a></p></td>
<td><ul>
<li>第38回<a href="../Page/BEST_HITS歌謠祭.md" title="wikilink">BEST HITS歌謠祭新人獎</a></li>
<li>第38回<a href="../Page/日本有線大賞.md" title="wikilink">日本有線大賞有線音樂賞</a></li>
<li>第47回<a href="../Page/日本唱片大獎.md" title="wikilink">日本唱片大獎最優秀新人賞</a></li>
</ul></td>
</tr>
<tr class="odd">
<td><p>2006年</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/颶風·莉莉，波士頓·瑪麗.md" title="wikilink">颶風·莉莉，波士頓·瑪麗</a></p></td>
<td><p>第39回<a href="../Page/BEST_HIT歌謠祭.md" title="wikilink">BEST HIT歌謠祭金曲艺术家</a></p></td>
</tr>
<tr class="odd">
<td><p>2008年</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Get_啾!_/_SHE的真相.md" title="wikilink">Get 啾!</a></p></td>
<td><p>2007學級<a href="../Page/MTV日本音樂錄影帶大獎.md" title="wikilink">MTV日本音樂錄影帶大獎</a>“STUDENT VOICE”最優秀舞蹈藝術家</p></td>
</tr>
<tr class="odd">
<td><p>2009年</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Break_Down_/_Break_your_name_/_Summer_Revolution.md" title="wikilink">Break Down</a></p></td>
<td><p>第42回<a href="../Page/BEST_HIT歌謠祭.md" title="wikilink">BEST HIT歌謠祭金曲艺术家</a></p></td>
</tr>
<tr class="odd">
<td><p>2010年</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/想見你的理由_/_Dream_After_Dream_～夢醒之夢～.md" title="wikilink">想見你的理由</a></p></td>
<td><p>第52回<a href="../Page/日本唱片大獎.md" title="wikilink">日本唱片大獎優秀作品獎</a></p></td>
</tr>
<tr class="odd">
<td><p>2011年</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/CALL_/_I4U.md" title="wikilink">CALL</a></p></td>
<td><p>第53回<a href="../Page/日本唱片大獎.md" title="wikilink">日本唱片大獎優秀作品獎</a></p></td>
</tr>
<tr class="odd">
<td><p>2012年</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/777_～We_can_sing_a_song!～.md" title="wikilink">777 ～We can sing a song!～</a></p></td>
<td><p>第54回<a href="../Page/日本唱片大獎.md" title="wikilink">日本唱片大獎優秀作品獎</a></p></td>
</tr>
<tr class="odd">
<td><p>2013年</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/戀歌與雨天.md" title="wikilink">戀歌與雨天</a></p></td>
<td><p>第55回<a href="../Page/日本唱片大獎.md" title="wikilink">日本唱片大獎優秀作品獎</a></p></td>
</tr>
<tr class="odd">
<td><p>2014年</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/說再見之前.md" title="wikilink">說再見之前</a></p></td>
<td><p>第56回<a href="../Page/日本唱片大獎.md" title="wikilink">日本唱片大獎優秀作品獎</a></p></td>
</tr>
<tr class="odd">
<td><p>2015年</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/深愛著，卻不能愛.md" title="wikilink">深愛著，卻不能愛</a></p></td>
<td><p>第57回<a href="../Page/日本唱片大獎.md" title="wikilink">日本唱片大獎優秀作品獎</a></p></td>
</tr>
<tr class="odd">
<td><p>2016年</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/沒有眼淚的世界.md" title="wikilink">沒有眼淚的世界</a></p></td>
<td><p>第58回<a href="../Page/日本唱片大獎.md" title="wikilink">日本唱片大獎優秀作品獎</a></p></td>
</tr>
<tr class="odd">
<td><p>2017年</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/LIFE_(AAA單曲).md" title="wikilink">LIFE</a></p></td>
<td><p>第59回<a href="../Page/日本唱片大獎.md" title="wikilink">日本唱片大獎優秀作品獎</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 銷量認證

<table>
<thead>
<tr class="header">
<th><p>作品</p></th>
<th><p>認證信息</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>正版實體唱片</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>細碟《<a href="../Page/Climax_Jump.md" title="wikilink">Climax Jump</a>》（<strong>AAA DEN-O form</strong>名義）</p></td>
<td><p>2008年3月，金唱片 [207]</p></td>
</tr>
<tr class="odd">
<td><p>大碟《<a href="../Page/#AAABEST.md" title="wikilink">#AAABEST</a>》</p></td>
<td><p>2011年9月，金唱片 [208]</p></td>
</tr>
<tr class="even">
<td><p>大碟《<a href="../Page/GOLD_SYMPHONY.md" title="wikilink">GOLD SYMPHONY</a>》</p></td>
<td><p>2014年11月，金唱片[209]</p></td>
</tr>
<tr class="odd">
<td><p>大碟《<a href="../Page/AAA_10th_ANNIVERSARY_BEST.md" title="wikilink">AAA 10th ANNIVERSARY BEST</a>》</p></td>
<td><p>2015年9月，金唱片</p></td>
</tr>
<tr class="even">
<td><p>大碟《<a href="../Page/WAY_OF_GLORY.md" title="wikilink">WAY OF GLORY</a>》</p></td>
<td><p>2017年2月，金唱片</p></td>
</tr>
<tr class="odd">
<td><p>付費串流音樂</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>全曲《<a href="../Page/Climax_Jump.md" title="wikilink">Climax Jump</a>》（<strong>AAA DEN-O form</strong>名義）</p></td>
<td><p>2007年11月，金單曲 [210]<br />
2011年3月，白金單曲 [211]</p></td>
</tr>
<tr class="odd">
<td><p>全曲《<a href="../Page/戀歌與雨天.md" title="wikilink">戀歌與雨天</a>》</p></td>
<td><p>2013年11月，金單曲[212]<br />
2014年3月，白金單曲[213]<br />
2016年6月，雙白金單曲</p></td>
</tr>
<tr class="even">
<td><p>全曲《<a href="../Page/想見你的理由_/_Dream_After_Dream_～夢醒之夢～.md" title="wikilink">想見你的理由</a>》</p></td>
<td><p>2014年1月，金單曲[214]</p></td>
</tr>
<tr class="odd">
<td><p>全曲《<a href="../Page/不屈的心.md" title="wikilink">不屈的心</a>》</p></td>
<td><p>2014年1月，金單曲[215]</p></td>
</tr>
<tr class="even">
<td><p>全曲《<a href="../Page/Get_啾!_/_SHE的真相.md" title="wikilink">Get 啾!</a>》</p></td>
<td><p>2014年7月，金單曲[216]</p></td>
</tr>
<tr class="odd">
<td><p>全曲《<a href="../Page/說再見之前.md" title="wikilink">說再見之前</a>》</p></td>
<td><p>2014年12月，金單曲 [217] 2016年2月，白金單曲</p></td>
</tr>
<tr class="even">
<td><p>全曲《<a href="../Page/Wake_up!.md" title="wikilink">Wake up!</a>》</p></td>
<td><p>2015年3月，金單曲 [218]</p></td>
</tr>
<tr class="odd">
<td><p>全曲《<a href="../Page/No_cry_No_more.md" title="wikilink">No cry No more</a>》</p></td>
<td><p>2015年4月，金單曲[219]</p></td>
</tr>
<tr class="even">
<td><p>全曲《[[MUSIC</p></td>
<td><p>!_/_ZERO|MUSIC</p></td>
</tr>
<tr class="odd">
<td><p>全曲《<a href="../Page/深愛著，卻不能愛.md" title="wikilink">深愛著，卻不能愛</a>》</p></td>
<td><p>2015年12月，金單曲</p></td>
</tr>
<tr class="even">
<td><p>全曲《<a href="../Page/777_～We_can_sing_a_song!～.md" title="wikilink">777 ～We can sing a song!～</a>》</p></td>
<td><p>2016年9月，金單曲</p></td>
</tr>
<tr class="odd">
<td><p>全曲《<a href="../Page/Lil&#39;_Infinity.md" title="wikilink">Lil' Infinity</a>》</p></td>
<td><p>2017年2月，金單曲</p></td>
</tr>
<tr class="even">
<td><p>全曲《<a href="../Page/明日之光.md" title="wikilink">明日之光</a>》</p></td>
<td><p>2017年2月，金單曲</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
</tbody>
</table>

  - 暫無認證的鈴聲版楽曲。
  - 數據取自[日本唱片協會官方網站](../Page/日本唱片協會.md "wikilink")。

## 註解

## 參考文獻

## 外部連結

  -   -
      -
      -
      -
      -
  -   -
[Category:日本演唱團體](../Category/日本演唱團體.md "wikilink")
[Category:日本唱片大獎最優秀新人獎獲獎者](../Category/日本唱片大獎最優秀新人獎獲獎者.md "wikilink")
[Category:愛貝克思集團藝人](../Category/愛貝克思集團藝人.md "wikilink")
[Category:愛貝克思甄選參賽者](../Category/愛貝克思甄選參賽者.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.

9.

10.

11.

12.

13.

14.
15.

16.
17.

18.
19.
20.
21.
22.
23.
24.
25.
26.
27.

28.
29.

30.

31.

32.

33.

34.

35.
36.

37.

38.

39.

40.

41.

42.

43.

44.

45.
46.

47.

48.

49.

50.

51.

52.
53.

54.

55.

56.

57.

58.

59.

60.

61.

62.

63.

64.

65.

66.

67.

68.

69.

70.

71.

72.

73.

74.

75.

76.

77.

78.

79.

80.

81.

82.

83.

84.

85.

86.

87.

88.

89.

90.

91.

92.

93.

94.

95.
96.
97.
98.

99.

100.

101.
102.
103.

104.

105.

106.

107.

108.

109.

110.
111.

112.

113.

114.

115.

116.

117.

118.
119.
120.

121.

122.

123. [「ＢＬＯＯＤ　ｏｎ　ＦＩＲＥ」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/music/613293/1/).ORICON
     STYLE/2014年1月9日校对.

124. [「Ｆｒｉｄａｙ　Ｐａｒｔｙ」
     ＡＡＡ](http://contents.oricon.co.jp/prof/artist/373048/products/music/616232/1/).ORICON
     STYLE/2014年1月9日校对.

125. [「きれいな空」
     ＡＡＡ](http://contents.oricon.co.jp/prof/artist/373048/products/music/621363/1/).ORICON
     STYLE/2014年1月9日校对.

126. [「ＤＲＡＧＯＮ　ＦＩＲＥ」
     ＡＡＡ](http://contents.oricon.co.jp/prof/artist/373048/products/music/625385/1/).ORICON
     STYLE/2014年1月9日校对.

127. [「ハレルヤ」
     ＡＡＡ](http://contents.oricon.co.jp/prof/artist/373048/products/music/636531/1/).ORICON
     STYLE/2014年1月9日校对.

128. [「Ｓｈａｌａｌａ　キボウの歌」
     ＡＡＡ](http://contents.oricon.co.jp/prof/artist/373048/products/music/639478/1/).ORICON
     STYLE/2014年1月9日校对.

129. [「ハリケーン・リリ，ボストン・マリ」
     ＡＡＡ](http://contents.oricon.co.jp/prof/artist/373048/products/music/648107/1/).ORICON
     STYLE/2014年1月9日校对.

130. [「ソウルエッジボーイ／キモノジェットガール」
     ＡＡＡ](http://contents.oricon.co.jp/prof/artist/373048/products/music/657614/1/).ORICON
     STYLE/2014年1月9日校对.

131. [「Ｌｅｔ　ｉｔ　ｂｅａｔ！」
     ＡＡＡ](http://contents.oricon.co.jp/prof/artist/373048/products/music/663089/1/).ORICON
     STYLE/2014年1月9日校对.

132. [「“Ｑ”」
     ＡＡＡ](http://contents.oricon.co.jp/prof/artist/373048/products/music/666164/1/).ORICON
     STYLE/2014年1月9日校对.

133. [「チューインガム」
     ＡＡＡ](http://contents.oricon.co.jp/prof/artist/373048/products/music/681863/1/).ORICON
     STYLE/2014年1月9日校对.

134. [「Ｂｌａｃｋ　＆　Ｗｈｉｔｅ」
     ＡＡＡ](http://contents.oricon.co.jp/prof/artist/373048/products/music/686060/1/).ORICON
     STYLE/2014年1月9日校对.

135. [「Ｇｅｔ　チュー！／ＳＨＥの事実」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/music/701407/1/).ORICON
     STYLE/2014年1月9日校对.

136. [「唇からロマンチカ／Ｔｈａｔ’ｓ　Ｒｉｇｈｔ」
     ＡＡＡ](http://contents.oricon.co.jp/prof/artist/373048/products/music/705879/1/).ORICON
     STYLE/2014年1月9日校对.

137. [「夏もの」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/music/714861/1/).ORICON
     STYLE/2014年1月9日校对.

138. [「Ｒｅｄ　Ｓｏｕｌ」
     ＡＡＡ](http://contents.oricon.co.jp/prof/artist/373048/products/music/723393/1/).ORICON
     STYLE/2014年1月9日校对.

139. [「ＭＩＲＡＧＥ」
     ＡＡＡ](http://contents.oricon.co.jp/prof/artist/373048/products/music/743437/1/).ORICON
     STYLE/2014年1月9日校对.

140. [「ＢＥＹＯＮＤ～カラダノカナタ」
     ＡＡＡ](http://contents.oricon.co.jp/prof/artist/373048/products/music/755275/1/).ORICON
     STYLE/2014年1月9日校对.

141. [「ＭＵＳＩＣ！！！／ＺＥＲＯ」
     ＡＡＡ](http://contents.oricon.co.jp/prof/artist/373048/products/music/772894/1/).ORICON
     STYLE/2014年1月9日校对.

142. [「旅ダチノウタ」
     ＡＡＡ](http://contents.oricon.co.jp/prof/artist/373048/products/music/782295/1/).ORICON
     STYLE/2014年1月9日校对.

143. [「Ｓｕｍｍｅｒ　Ｒｅｖｏｌｕｔｉｏｎ／Ｂｒｅａｋ　Ｄｏｗｎ／Ｂｒｅａｋ　ｙｏｕｒ　ｎａｍｅ」
     ＡＡＡ／西風雲](http://www.oricon.co.jp/prof/artist/485198/products/music/825503/1/).ORICON
     STYLE/2014年1月9日校对.

144. [「Ｈｉｄｅ－ａｗａｙ」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/music/839054/1/).ORICON
     STYLE/2014年1月9日校对.

145. [「Ｈｅａｒｔ　ａｎｄ　Ｓｏｕｌ（女メンバーヴァージョン（Ｃｖｅｒ．））」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/music/859793/1/).ORICON
     STYLE/2014年1月9日校对.

146. [「Ｄｒｅａｍ　Ａｆｔｅｒ　Ｄｒｅａｍ　～夢から醒めた夢～／逢いたい理由」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/music/864092/1).ORICON
     STYLE/2014年1月9日校对.

147. [「負けない心」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/music/879713/1/).ORICON
     STYLE/2014年1月9日校对.

148. [「Ｅｎｄｌｅｓｓ　Ｆｉｇｈｔｅｒｓ／ＰＡＲＡＤＩＳＥ（ポケモンスマッシュ！盤）」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/music/892226/1/).ORICON
     STYLE/2014年1月9日校对.

149. [「ダイジナコト（完全限定生産盤）（ジャケットＣ）」
     ＡＡＡ](http://contents.oricon.co.jp/prof/artist/373048/products/music/901622/1/).ORICON
     STYLE/2014年1月9日校对.

150. [「Ｎｏ　ｃｒｙ　Ｎｏ　ｍｏｒｅ」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/music/912457/1/).ORICON
     STYLE/2014年1月9日校对.

151. [「Ｉ４Ｕ／ＣＡＬＬ（完全限定生産盤）（「劇場版テニスの王子様」盤）」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/music/927067/1/).ORICON
     STYLE/2014年1月9日校对.

152. [「Ｃｈａｒｇｅ　＆　Ｇｏ！／Ｌｉｇｈｔｓ」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/music/937312/1/).ORICON
     STYLE/2014年1月9日校对.

153. [「ＳＡＩＬＩＮＧ」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/music/949925/1/).ORICON
     STYLE/2014年1月9日校对.

154. [「Ｓｔｉｌｌ　Ｌｏｖｅ　Ｙｏｕ」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/music/961062/1/).ORICON
     STYLE/2014年1月9日校对.

155. [「７７７　～Ｗｅ　ｃａｎ　ｓｉｎｇ　ａ　ｓｏｎｇ！～（完全限定生産盤）」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/music/974173/1/).ORICON
     STYLE/2014年1月9日校对.

156. [「虹」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/music/988454/1/).ORICON
     STYLE/2014年1月9日校对.

157. [「Ｍｉｓｓ　ｙｏｕ／ほほえみの咲く場所」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/music/1002245/1/).ORICON
     STYLE/2014年1月9日校对.

158. [「ＰＡＲＴＹ　ＩＴ　ＵＰ」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/music/1010249/1/).ORICON
     STYLE/2014年1月9日校对.

159. [「Ｌｏｖｅ　Ｉｓ　Ｉｎ　Ｔｈｅ　Ａｉｒ」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/music/1023601/1/).ORICON
     STYLE/2014年1月9日校对.

160. [「恋音と雨空」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/music/1034693/1/).ORICON
     STYLE/2014年1月9日校对.

161. [「Ｌｏｖｅ」
     ＡＡＡ](http://www.oricon.co.jp/prof/373048/products/music/1060426/1/).ORICON
     STYLE/2015年3月14日校对.

162. [「Ｗａｋｅ　ｕｐ！」
     ＡＡＡ](http://www.oricon.co.jp/prof/373048/products/music/1077330/1/).ORICON
     STYLE/2015年3月14日校对.

163. [「さよならの前に」
     ＡＡＡ](http://www.oricon.co.jp/prof/373048/products/music/1086206/1/).ORICON
     STYLE/2015年3月14日校对.

164. [「Ｉ’ｌｌ　ｂｅ　ｔｈｅｒｅ」
     ＡＡＡ](http://www.oricon.co.jp/prof/373048/products/music/1109249/1/).ORICON
     STYLE/2015年3月14日校对.

165. [「Ｌｉｌ’　Ｉｎｆｉｎｉｔｙ」
     ＡＡＡ](http://www.oricon.co.jp/prof/373048/products/music/1109249/1/).ORICON
     STYLE/2015年3月14日校对.

166. [「 ＡＴＴＡＣＫ」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/music/628620/1/)－ORICON
     STYLE－2014年1月11日校对.

167. [「ＡＬＬ」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/music/683812/1/)－ORICON
     STYLE－2014年1月11日校对.

168. [「ＡＲＯＵＮＤ」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/music/723390/1/)－ORICON
     STYLE－2014年1月11日校对.

169. [「ｄｅｐＡｒｔｕｒｅ」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/music/801122/1/)－ORICON
     STYLE－2014年1月11日校对.

170. [「ＨＥＡＲＴＦＵＬ」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/music/854133/1/)－ORICON
     STYLE－2014年1月11日校对.

171. [「Ｂｕｚｚ　Ｃｏｍｍｕｎｉｃａｔｉｏｎ」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/music/901632/1/)－ORICON
     STYLE－2014年1月11日校对.

172. [「７７７　～ＴＲＩＰＬＥ　ＳＥＶＥＮ～」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/music/979329/1/)－ORICON
     STYLE－2014年1月11日校对.

173. [「Ｅｉｇｈｔｈ　Ｗｏｎｄｅｒ」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/music/1037459/1/)－ORICON
     STYLE－2014年1月11日校对.

174. [「ＡＴＴＡＣＫ　ＡＬＬ　ＡＲＯＵＮＤ」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/music/750938/1/)－ORICON
     STYLE－2014年1月11日校对.

175. [「＃ＡＡＡＢＥＳＴ」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/music/927470/1/)－ORICON
     STYLE－2014年1月11日校对.

176. [「Ａｎｏｔｈｅｒ　ｓｉｄｅ　ｏｆ　＃ＡＡＡＢＥＳＴ（ＤＶＤ付）」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/music/953510/1/)－ORICON
     STYLE－2014年1月11日校对.

177. [「ＡＬＬ／２」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/music/668604/1/)－ORICON
     STYLE－2014年1月11日校对.

178. [「ａｌｏｈＡＡＡ！」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/music/696462/1/)－ORICON
     STYLE－2014年1月11日校对.

179. [「ＣＨＯＩＣＥ　ＩＳ　ＹＯＵＲＳ」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/music/763504/1/)－ORICON
     STYLE－2014年1月11日校对.

180. [「ＲＥＭＩＸ　ＡＴＴＡＣＫ」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/music/639470/1/)－ORICON
     STYLE－2014年1月11日校对.

181. [「Ｄｒｉｖｉｎｇ　ＭＩＸ」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/music/1051568/1/)－ORICON
     STYLE－2014年1月11日校对.

182. [「ＣＣＣ－ＣＨＡＬＬＥＮＧＥ　ＣＯＶＥＲ　ＣＯＬＬＥＣＴＩＯＮ－」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/music/691988/1/)－ORICON
     STYLE－2014年1月11日校对.

183. [「Ｂａｌｌａｄ　Ｃｏｌｌｅｃｔｉｏｎ」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/music/1010245/1/)－ORICON
     STYLE－2014年1月11日校对.

184. [「１ｓｔ　ＡＴＴＡＣＫ　ａｔ　ＳＨＩＢＵＹＡ－ＡＸ」
     ＡＡＡ](http://contents.oricon.co.jp/prof/artist/373048/products/cinema/639408/1/)-ORICON
     STYLE/2014年1月11日校对.

185. [「２ｎｄ　ＡＴＴＡＣＫ　ａｔ　Ｚｅｐｐ　Ｔｏｋｙｏ　ｏｎ　２９ｔｈ　ｏｆ　Ｊｕｎｅ　２００６」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/cinema/671090/1/)-ORICON
     STYLE/2014年1月11日校对.

186. [「１ｓｔ　Ａｎｎｉｖｅｒｓａｒｙ　Ｌｉｖｅ－３ｒｄ　ＡＴＴＡＣＫ　０６０９１３－ａｔ　日本武道館」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/cinema/683765/1/)-ORICON
     STYLE/2014年1月11日校对.

187. [「Ｃｈａｎｎｅｌ＠×ＡＡＡ」
     ＡＡＡ](http://contents.oricon.co.jp/prof/artist/373048/products/cinema/696355/1/)-ORICON
     STYLE/2014年1月11日校对.

188. [「ＡＡＡ　ＴＯＵＲ　２００７　４ｔｈ　ＡＴＴＡＣＫ　ａｔ　ＳＨＩＢＵＹＡ－ＡＸ　ｏｎ　４ｔｈ　ｏｆ　Ａｐｒｉｌ」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/cinema/714012/1/)-ORICON
     STYLE/2014年1月11日校对.

189. [「ＡＡＡ　２ｎｄ　Ａｎｎｉｖｅｒｓａｒｙ　Ｌｉｖｅ－５ｔｈ　ＡＴＴＡＣＫ　０７０９２２－日本武道館（スペシャル盤）」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/cinema/743250/1/)-ORICON
     STYLE/2014年1月11日校对.

190. [「ＡＡＡ　ＴＯＵＲ　２００８－ＡＴＴＡＣＫ　ＡＬＬ　ＡＲＯＵＮＤ－ａｔ　ＮＨＫ　ＨＡＬＬ　ｏｎ　４ｔｈ　ｏｆ　Ａｐｒｉｌ」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/cinema/772840/1/)-ORICON
     STYLE/2014年1月11日校对.

191. [「ＡＡＡ　３ｒｄ　Ａｎｎｉｖｅｒｓａｒｙ　Ｌｉｖｅ　０８０９２２－０８０９２３　日本武道館（スペシャル盤）」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/cinema/796463/1/)-ORICON
     STYLE/2014年1月11日校对.

192. [「Ｃｈａｎｎｅｌ＠×ＡＡＡ　其の弐」
     ＡＡＡ](http://contents.oricon.co.jp/prof/artist/373048/products/cinema/804488/1/)-ORICON
     STYLE/2014年1月11日校对.

193. [「ＡＡＡ　ＴＯＵＲ　２００９－Ａ　ｄｅｐＡｒｔｕｒｅ　ｐＡｒｔｙ－」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/cinema/830191/1/)-ORICON
     STYLE/2014年1月11日校对.

194. [「ＡＡＡ　４ｔｈ　Ａｎｎｉｖｅｒｓａｒｙ　ＬＩＶＥ　０９０９２２　ａｔ　Ｙｏｋｏｈａｍａ　Ａｒｅｎａ」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/cinema/857351/1/)-ORICON
     STYLE/2014年1月11日校对.

195. [「ＡＡＡ　Ｈｅａｒｔ　ｔｏ　■　ＴＯＵＲ　２０１０」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/cinema/883834/1/)-ORICON
     STYLE/2014年1月11日校对.

196. [「ＡＡＡ　５ｔｈ　Ａｎｎｉｖｅｒｓａｒｙ　ＬＩＶＥ　２０１００９１２　ａｔ　Ｙｏｋｏｈａｍａ　Ａｒｅｎａ」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/cinema/904889/1/)-ORICON
     STYLE/2014年1月11日校对.

197. [「ＡＡＡ　ＢＵＺＺ　ＣＯＭＭＵＮＩＣＡＴＩＯＮ　ＴＯＵＲ　２０１１　ＤＥＬＵＸＥ　ＥＤＩＴＩＯＮ」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/cinema/940279/1/)-ORICON
     STYLE/2014年1月11日校对.

198. [「ＡＡＡ　６ｔｈ　Ａｎｎｉｖｅｒｓａｒｙ　Ｔｏｕｒ　２０１１．９．２８　ａｔ　Ｚｅｐｐ　Ｔｏｋｙｏ」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/cinema/949879/1/)-ORICON
     STYLE/2014年1月11日校对.

199. [「ＡＡＡ　ＴＯＵＲ　２０１２　－７７７－　ＴＲＩＰＬＥ　ＳＥＶＥＮ」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/cinema/1002303/1/)-ORICON
     STYLE/2014年1月11日校对.

200. [「ＡＡＡ　ＴＯＵＲ　２０１３　Ｅｉｇｈｔｈ　Ｗｏｎｄｅｒ」
     ＡＡＡ](http://www.oricon.co.jp/prof/artist/373048/products/cinema/1055643/1/)-ORICON
     STYLE/2015年3月15日校对.

201.

202.

203.

204.

205.

206.

207.

208.

209.

210.

211.

212.

213.

214.

215.
216.

217.

218.

219.