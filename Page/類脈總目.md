**類脈總目**是由[威利·亨尼希所建立的](../Page/威利·亨尼希.md "wikilink")，其下有[毛翅目和](../Page/毛翅目.md "wikilink")[鱗翅目兩個目](../Page/鱗翅目.md "wikilink")。2017年，第三個化石目[Tarachoptera被加上該組](../Page/Tarachoptera.md "wikilink")。\[1\]

毛翅目和鱗翅目有許多的相同裔徵（[共同衍徵](../Page/共同衍徵.md "wikilink")），這證明了牠們有著相同的祖先：

  - 雄性有一條[性染色體](../Page/性染色體.md "wikilink")，雌性有兩條；
  - [翅膀上有濃密的棘毛](../Page/昆蟲翅膀.md "wikilink")（在鱗翅目裡演化為鱗）；
  - 前翅有獨特的[翅脈圖案](../Page/翅脈.md "wikilink")（雙環臀脈）；
  - 幼蟲的嘴巴構造和唾腺可以生成和控制[絲](../Page/絲.md "wikilink")\[2\]。

因此，這兩個目被歸在類脈總目。這個總目可能是在[侏羅紀時演化出來的](../Page/侏羅紀.md "wikilink")，由已經滅絕了的[Necrotaulidae](../Page/Necrotaulidae.md "wikilink")\[3\]。鱗翅目和毛翅目有些許不同的特徵，包括翅脈、翅膀上的鱗、沒有[尾鬚](../Page/尾鬚.md "wikilink")、沒有[單眼](../Page/單眼.md "wikilink")，以及腳部上的變化\[4\]。

## 參考文獻

<references/>

[Category:昆蟲綱](../Category/昆蟲綱.md "wikilink")

1.

2.

3.
4.