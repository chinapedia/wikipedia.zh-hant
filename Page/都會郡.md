[MetCountiesEngland.png](https://zh.wikipedia.org/wiki/File:MetCountiesEngland.png "fig:MetCountiesEngland.png")
**都會郡**（），[英格蘭地區所使用的一種](../Page/英格蘭.md "wikilink")[郡](../Page/英格蘭的郡.md "wikilink")（county）級行政區劃。目前共有6個都會郡，涵蓋了大型都會地區。各郡的人口約120萬到280萬不等\[1\]。這些郡成立於1974年，各自又可細分為多個[都會區](../Page/都市自治市.md "wikilink")（metropolitan
district）。

各郡的[郡理事會](../Page/郡理事會.md "wikilink")（county
council）廢止（**虛級化**）於1986年，這些理事會的事務大多轉移到各個[自治市中](../Page/都市自治市.md "wikilink")，而這些[自治市在實務上也因此形成](../Page/都市自治市.md "wikilink")[單一管理區](../Page/單一管理區.md "wikilink")（unitary
authority）；其餘的事務則由聯合委員會（joint board）接管\[2\]。

各都會郡的人口密度大約在每平方公里800人（[南約克郡](../Page/南約克郡.md "wikilink")）到2800人（[西密德蘭](../Page/西密德蘭.md "wikilink")）之間。而其中的都會區人口密度，則大約每平方公里500人（[唐卡斯特](../Page/唐卡斯特.md "wikilink")）到4000人（[利物浦](../Page/利物浦.md "wikilink")）不等\[3\]。所有都會郡的總人口佔全英格蘭的22%，以及全[聯合王國的](../Page/聯合王國.md "wikilink")18%。

## 郡與區列表

6個都會郡與下轄都會區分別如下：

<table>
<thead>
<tr class="header">
<th><p>都會郡</p></th>
<th><p>都會自治市</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/大曼徹斯特.md" title="wikilink">大曼徹斯特</a><br />
</p></td>
<td><p><a href="../Page/曼徹斯特.md" title="wikilink">曼徹斯特市</a>、<a href="../Page/索爾福德市.md" title="wikilink">索尔福德市</a>、<a href="../Page/博爾頓都市自治市.md" title="wikilink">博爾頓</a>、<a href="../Page/貝里都市自治市.md" title="wikilink">貝里</a>、<a href="../Page/奧爾德姆都市自治市.md" title="wikilink">奧耳丹</a>、<a href="../Page/羅奇代爾都市自治市.md" title="wikilink">羅奇代爾</a>、<a href="../Page/斯托克波特都市自治市.md" title="wikilink">斯托克波特</a>、<a href="../Page/泰姆賽德.md" title="wikilink">塔姆塞德</a>、<a href="../Page/特拉福德.md" title="wikilink">特拉福德</a>、<a href="../Page/威根都市自治市.md" title="wikilink">韋根</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/默西賽德.md" title="wikilink">默西賽德</a><br />
Merseyside</p></td>
<td><p><a href="../Page/利物浦.md" title="wikilink">利物浦市</a>、<a href="../Page/諾斯利都市自治市.md" title="wikilink">諾斯利</a>、<a href="../Page/塞夫頓都會自治市.md" title="wikilink">塞夫頓</a>、<a href="../Page/聖海倫斯都會自治市.md" title="wikilink">聖希倫斯</a>、<a href="../Page/威勒爾都會自治市.md" title="wikilink">威勒爾</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/南約克郡.md" title="wikilink">南約克郡</a><br />
South Yorkshire</p></td>
<td><p><a href="../Page/雪菲爾.md" title="wikilink">雪菲爾市</a>、<a href="../Page/巴恩斯利都市自治市.md" title="wikilink">巴恩斯利</a>、<a href="../Page/唐卡斯特都市自治市.md" title="wikilink">唐卡斯特</a>、<a href="../Page/羅瑟漢姆都市自治市.md" title="wikilink">羅瑟漢姆</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/泰恩-威爾.md" title="wikilink">泰恩-威爾</a><br />
Tyne and Wear</p></td>
<td><p><a href="../Page/泰恩河畔紐塞.md" title="wikilink">泰恩河畔紐塞市</a>、<a href="../Page/巽得蘭市.md" title="wikilink">巽得蘭市</a>、<a href="../Page/加茲海得都會自治市.md" title="wikilink">加茲海得</a>、<a href="../Page/南泰恩賽德都會自治市.md" title="wikilink">南泰恩賽德</a>、<a href="../Page/北泰恩賽德都會自治市.md" title="wikilink">北泰恩賽德</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/西密德蘭郡.md" title="wikilink">西密德蘭郡</a><br />
West Midlands</p></td>
<td><p><a href="../Page/伯明罕.md" title="wikilink">伯明罕市</a>、<a href="../Page/科芬特里.md" title="wikilink">科芬特里市</a>、<a href="../Page/伍爾弗漢普頓.md" title="wikilink">伍爾弗漢普頓市</a>、<a href="../Page/達德利都會自治市.md" title="wikilink">達德利</a>、<a href="../Page/桑威爾都會自治市.md" title="wikilink">桑威爾</a>、<a href="../Page/索利哈爾都會自治市.md" title="wikilink">索利哈爾</a>、<a href="../Page/瓦索爾都會自治市.md" title="wikilink">瓦索爾</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/西約克郡.md" title="wikilink">西約克郡</a><br />
West Yorkshire</p></td>
<td><p><a href="../Page/里茲市.md" title="wikilink">里茲市</a>、<a href="../Page/布拉德福德市.md" title="wikilink">布拉德福德市</a>、<a href="../Page/韋克菲爾德市.md" title="wikilink">韋克菲爾德市</a>、<a href="../Page/卡爾德達爾.md" title="wikilink">卡爾德達爾</a>、<a href="../Page/柯爾克利斯都會自治市.md" title="wikilink">柯爾克利斯</a></p></td>
</tr>
</tbody>
</table>

## 參考來源

[Category:英格蘭行政區劃](../Category/英格蘭行政區劃.md "wikilink")
[都市郡](../Category/都市郡.md "wikilink")

1.  Jones, B. et al, *Politics UK*, (2004)
2.  Her Majesty's Stationery Office, *Aspects of Britain: Local
    Government*, (1996)
3.  [2001 census : KS01 Usual resident
    population](http://www.statistics.gov.uk/statbase/Expodata/Spreadsheets/D6555.xls)