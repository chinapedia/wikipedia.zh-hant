**丹羽長秀**（），[日本戰國時代](../Page/日本戰國時代.md "wikilink")、[安土桃山時代武將](../Page/安土桃山時代.md "wikilink")，仕奉織田信長。[織田四天王之一](../Page/織田四天王.md "wikilink")，別稱**鬼五郎左**、**米五郎左**，幼名萬千代。

## 經歷

生於[尾張春日井郡儿玉村](../Page/尾张国.md "wikilink")（今[愛知縣](../Page/愛知縣.md "wikilink")[名古屋市](../Page/名古屋市.md "wikilink")[西區](../Page/西區_\(名古屋市\).md "wikilink")），[丹羽長政次男](../Page/丹羽長政.md "wikilink")。[丹羽氏初時為](../Page/丹羽氏.md "wikilink")[斯波氏家臣](../Page/斯波氏.md "wikilink")，1550年出仕[织田信长](../Page/织田信长.md "wikilink")，1553年梅津表合戰首次上戰場。1556年在[稻生之戰為信長的一員](../Page/稻生之戰.md "wikilink")。1560年參與[桶狹間之戰](../Page/桶狹間之戰.md "wikilink")，但沒有編在攻擊[今川義元部隊中](../Page/今川義元.md "wikilink")。

依據『[信長公記](../Page/信長公記.md "wikilink")』，自攻打美濃國[稻葉山城齊藤龍興時開始受到信長重用](../Page/稻葉山城.md "wikilink")，曾消滅尾張[犬山城的織田信清勢力](../Page/犬山城.md "wikilink")（[犬山城之戰](../Page/犬山城之戰.md "wikilink")），並在攻打[美濃國猿喰城](../Page/美濃國.md "wikilink")、堂洞城時立下戰功（[稻葉山城之戰](../Page/稻葉山城之戰.md "wikilink")），1568年（永祿11年）在足利義昭奉命信長上京之際，攻打南近江六角氏觀音寺城獲得戰功。此外，在內政方面，負責建造[安土城](../Page/安土城.md "wikilink")，獲得[佐和山城](../Page/佐和山城.md "wikilink")、若狹等領地。1571年2月24日原佐和山城城主磯野員昌經勸告退城，將城主之位讓给丹羽。\[1\]1581年參與京都織田信長舉辦御馬揃閱兵儀式，獲第一名進場之榮耀。

### 本能寺之變後

1582年以[織田信孝副將身份支援出征](../Page/織田信孝.md "wikilink")[四國](../Page/四國.md "wikilink")，出發前得知爆發[本能寺之變](../Page/本能寺之變.md "wikilink")，並參與[山崎之戰](../Page/山崎之戰.md "wikilink")。清洲會議後與池田恆興共同支持[羽柴秀吉擁立織田秀信](../Page/羽柴秀吉.md "wikilink")（三法師）為織田當主，當天正11年（1583年）秀吉於賤岳之戰消滅[柴田勝家勢力後](../Page/柴田勝家.md "wikilink")，受封越前、若狹及加贺一百二十三万石领地。1585年於北之庄城患上[胃癌病逝](../Page/胃癌.md "wikilink")。另有丹羽長秀因豐臣秀吉找藉口逼迫[切腹自殺的說法](../Page/切腹.md "wikilink")。

長秀後來娶了信長的養女（實為信長之兄[織田信廣之女](../Page/織田信廣.md "wikilink")）為妻，而長秀的繼承人，兒子[丹羽長重則娶了信長的女兒為妻](../Page/丹羽長重.md "wikilink")，長秀本人被信長認為是朋友、也是兄弟，得到深厚的信賴。[丹羽長重後來在](../Page/丹羽長重.md "wikilink")[江戶幕府初期時](../Page/江戶幕府.md "wikilink")，受封為[陸奧](../Page/陸奧.md "wikilink")[白河藩的藩主](../Page/白河藩.md "wikilink")。此外，在織田四天王家系當中，只有丹羽氏在[江戶時代仍是](../Page/江戶時代.md "wikilink")[大名](../Page/大名.md "wikilink")。

## 鬼之稱號釋疑

雖有鬼五郎左稱號，但實際上並不善戰，大概是在戰場上夠勇敢而有所稱；在信長後期的重臣中，[柴田勝家](../Page/柴田勝家.md "wikilink")、[瀧川一益](../Page/瀧川一益.md "wikilink")、[羽柴秀吉](../Page/羽柴秀吉.md "wikilink")、[明智光秀都已是統兵一方的元帥](../Page/明智光秀.md "wikilink")，負責幫信長東征西討，反觀地位可算織田家前三、名望能與勝家並稱的長秀卻無如此待遇可見一般，且長秀在信長時代也從無擔任主將的記錄，擔任副將或隊長的紀錄倒是一大堆。

## 領地遭減之因

丹羽長秀死後，秀吉立刻找藉口將丹羽家由一百二十萬石的超級大名消減為十二萬石的小大名，是因為秀吉獨立之初仍需要織田家舊部的支持，織田派系中舉足輕重的丹羽長秀無疑是支持秀吉的舊部之首，安一人可安其他織田舊臣；後來秀吉自身根基已穩固，不再需要織田舊屬的支持，便馬上拔除勢力龐大、猶如芒刺在背的丹羽家。

## 家臣

  - [上田重安](../Page/上田重安.md "wikilink")
  - [大島光義](../Page/大島光義.md "wikilink")
  - [太田一吉](../Page/太田一吉.md "wikilink")
  - [戶田勝成](../Page/戶田勝成.md "wikilink")
  - [長束正家](../Page/長束正家.md "wikilink")
  - [溝口秀勝](../Page/溝口秀勝.md "wikilink")
  - [村上賴勝](../Page/村上賴勝.md "wikilink")
  - [桑山重晴](../Page/桑山重晴.md "wikilink")

## 關連作品

  - [信長 KING OF
    ZIPANGU](../Page/信長_KING_OF_ZIPANGU.md "wikilink")（1992年、NHK大河ドラマ、演：[杉本哲太](../Page/杉本哲太.md "wikilink")）
  - [秀吉](../Page/秀吉_\(NHK大河ドラマ\).md "wikilink")（1996年、NHK大河ドラマ、演：[篠田三郎](../Page/篠田三郎.md "wikilink")）
  - [利家とまつ～加賀百万石物語～](../Page/利家とまつ～加賀百万石物語～.md "wikilink")（2002年、NHK大河ドラマ、演：[梅沢富美男](../Page/梅沢富美男.md "wikilink")）
  - [功名が辻](../Page/功名が辻_\(NHK大河ドラマ\).md "wikilink")（2006年、NHK大河ドラマ、演：[名高達男](../Page/名高達男.md "wikilink")）

## 注释

## 關連項目

  - [觀音寺城之戰](../Page/觀音寺城之戰.md "wikilink")
  - [有岡城之戰](../Page/有岡城之戰.md "wikilink")
  - [小松寺砦跡](../Page/小松寺砦跡.md "wikilink")
  - [坂本城](../Page/坂本城.md "wikilink") -
    [城主修理](../Page/城主大名.md "wikilink")

## 外部連結

[丹羽長秀│書籍│PHP研究所](http://www.php.co.jp/books/detail.php?isbn=978-4-569-57250-5)

[Nagahide](../Category/丹羽氏.md "wikilink")
[Category:戰國武將](../Category/戰國武將.md "wikilink")
[Category:織豐政權大名](../Category/織豐政權大名.md "wikilink")
[Category:1535年出生](../Category/1535年出生.md "wikilink")
[Category:1585年逝世](../Category/1585年逝世.md "wikilink")
[Category:尾張國出身人物](../Category/尾張國出身人物.md "wikilink")

1.  『新修彦根市史 第1巻(通史編 古代・中世)}』彦根市史編集委員会、2007年1月