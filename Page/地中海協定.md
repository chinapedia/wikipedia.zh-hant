**地中海協定**（Mediterranean
Agreements）是1887年[大英帝國與](../Page/大英帝國.md "wikilink")[奧匈帝國](../Page/奧匈帝國.md "wikilink")、[義大利王國及](../Page/意大利王國_\(1861年–1946年\).md "wikilink")[西班牙王國簽訂的協定](../Page/西班牙王國.md "wikilink")，上述國家結合，以對抗[法國及](../Page/法國.md "wikilink")[俄罗斯在](../Page/俄罗斯帝国.md "wikilink")[地中海地區增加勢力的威脅](../Page/地中海.md "wikilink")。

## 經過

由於奧匈因在[1878年柏林會議上和](../Page/1878年柏林會議.md "wikilink")[俄罗斯就](../Page/俄罗斯帝国.md "wikilink")[巴爾幹勢力分配問題產生糾紛](../Page/巴爾幹.md "wikilink")，以逐漸令[三皇同盟瓦解](../Page/三皇同盟.md "wikilink")，[德國無意和俄罗斯公開決裂](../Page/德意志帝国.md "wikilink")，迫使奧匈因而轉而尋求英國協助，與此同時鑑於法國對[埃及的勢力擴張日益顯著](../Page/埃及.md "wikilink")，於是英國亦把握時機加強與地中海國家的關係以抗衡法國的威脅，而意大利又和法國在[突尼斯殖民地爭奪中失敗](../Page/突尼斯.md "wikilink")，甚為不滿法國，三國遂聯合於1887年3月及12月先後簽訂兩次地中海協定，同意對近東問題採取共同行動，著力保持地中海地區的現況。西班牙亦有參與第一次地中海協定的協作。

## 影響

地中海協定並非一項軍事協定，僅是一種接近軍事同盟的一种合作关系，此協定確保了英國對地中海的控制，不過因為英国既想確保對地中海的控制、又不愿承担如軍事同盟般以軍事行動支援盟友的责任，加上奧匈和意大利在[尚未收復的意大利](../Page/尚未收復的意大利.md "wikilink")——[特倫蒂諾-上阿迪傑及](../Page/特倫蒂諾-上阿迪傑.md "wikilink")[達爾馬提亞的領土爭奪](../Page/達爾馬提亞.md "wikilink")，[意大利統一也曾被奧匈大力阻撓](../Page/意大利統一.md "wikilink")，故此最後此一協定亦無疾而終。地中海協定打破了[俾斯麥同盟體系對歐洲局勢的壟斷](../Page/俾斯麥.md "wikilink")，而法國和俄國被英國的的外交孤立，亦為兩國日後的關係，進而結成[法俄同盟改善埋下伏筆](../Page/法俄同盟.md "wikilink")

## 參看

  - [三帝同盟](../Page/三帝同盟.md "wikilink")
  - [1878年柏林會議](../Page/1878年柏林會議.md "wikilink")
  - [德奧同盟](../Page/德奧同盟.md "wikilink")
  - [三國同盟](../Page/三國同盟.md "wikilink")
  - [再保險條約](../Page/再保險條約.md "wikilink")
  - [法俄同盟](../Page/法俄同盟.md "wikilink")
  - [英日同盟](../Page/英日同盟.md "wikilink")
  - [英法協約](../Page/英法協約.md "wikilink")
  - [英俄條約](../Page/英俄條約.md "wikilink")
  - [三國協約](../Page/三國協約.md "wikilink")
  - [巴爾幹同盟](../Page/巴爾幹同盟.md "wikilink")
  - [第一次世界大戰](../Page/第一次世界大戰.md "wikilink")

[Category:第一次世界大战](../Category/第一次世界大战.md "wikilink")
[Category:英國條約](../Category/英國條約.md "wikilink")
[Category:奧匈帝國條約](../Category/奧匈帝國條約.md "wikilink")
[Category:意大利王国条约](../Category/意大利王国条约.md "wikilink")
[Category:西班牙條約](../Category/西班牙條約.md "wikilink")
[Category:地中海歷史](../Category/地中海歷史.md "wikilink")
[Category:19世纪欧洲军事](../Category/19世纪欧洲军事.md "wikilink")
[Category:19世纪欧洲条约](../Category/19世纪欧洲条约.md "wikilink")
[Category:1887年欧洲](../Category/1887年欧洲.md "wikilink")