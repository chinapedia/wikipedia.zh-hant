**[道尼尔](../Page/道尼尔.md "wikilink") Do
217**是[第二次世界大战中](../Page/第二次世界大战.md "wikilink")[德国使用的一种双](../Page/德国.md "wikilink")[引擎重型轰炸机](../Page/引擎.md "wikilink")。该机由[道尼尔公司开发](../Page/道尼尔公司.md "wikilink")，主要用于替代性能已逐渐落后的[Do
17型轰炸机](../Page/道尼尔_Do_17.md "wikilink")。

## 概要

早在二战爆发前，道尼尔就已经认识到了[Do
17的弱点](../Page/Do_17轰炸机.md "wikilink")。尤其是在了解到[容克公司的](../Page/容克公司.md "wikilink")[Ju
88](../Page/Ju_88.md "wikilink")[原型机已经在任何一个方面都超过了前者后](../Page/原型机.md "wikilink")，更是决心研制出一种超越Do
17和Ju 88的新飞机来。

Do 217基本上可以被视为是的放大版。不过，虽然两种飞机都使用[DB
601发动机作为动力](../Page/DB_601發動機.md "wikilink")，Do
217在许多设计细节上都有着显著的不同。在1938－40年间，道尼尔公司的设计人员一直在试图为新飞机找到一种合适的动力，直到[宝马公司的](../Page/宝马.md "wikilink")[BMW
801问世后这一问题才得到完美的解决](../Page/BMW_801發動機.md "wikilink")。得益于发动机强劲的马力，Do
217拥有比其他德国双发轰炸机都大的[载弹量](../Page/载弹量.md "wikilink")。

从几乎所有的方面来看，Do 217都比较成功。它可以携带的炸弹比Ju 88的早期型号或是[He
111都多](../Page/He_111.md "wikilink")，而Do
217的速度也很快，在最大平飞速度这一项上甚至超过了Ju
88。在近两年的时间里，Do 217是德国最大的轰炸机，直到[He
177问世后这一纪录才被打破](../Page/He_177.md "wikilink")。在意大利投降后，Do
217参与拦截了试图向盟军投降的[意大利舰队](../Page/意大利.md "wikilink")，并使用[弗里茨
X](../Page/弗里茨_X.md "wikilink")[滑翔炸弹击沉了意大利](../Page/滑翔炸弹.md "wikilink")[战列舰](../Page/战列舰.md "wikilink")“”号。

然而，Do 217也有一些严重的问题。由于它几乎达到了双发飞机的尺寸上限，其机动性远远不如早期的Do
17轰炸机，操纵也显得笨拙。另外，最初飞机的稳定性也存在问题，直到在机翼前缘安装了[固定翼缝后才基本解决这个问题](../Page/固定翼缝.md "wikilink")。

就像Do 17和Ju 88，Do
217被用於各种任务，包括充当夜间战斗机。1943年，当德国对[战斗机的需求远远超过](../Page/战斗机.md "wikilink")[轰炸机时](../Page/轰炸机.md "wikilink")，Do
217的生产线被关闭。

当今，只有一些Do 217的部件被保存了下来，而Do 17和Do 217在二战中的作用也往往被人们忽视。

## 主要型别

### Do 217A-0

小批量试生产型，动力为两台1100马力的[DB
601液冷式發動機](../Page/DB_601發動機.md "wikilink")，主要被用作侦察任务，共生产八架。

### Do 217C-0

初期生产型，采用DB 601发动机为动力，并在原型机的基础上增强了防御火力，共制造四架。

### Do 217 E

  - Do 217E-0: E系列最初的型号，采用两台1,600马力的宝马[BMW
    801A/B引擎](../Page/BMW_801.md "wikilink")。
  - Do 217E-1：生产型，防御武器包括五挺7.92毫米[MG
    15机枪和一门](../Page/MG_15.md "wikilink")15毫米[MG
    151重机槍](../Page/MG_151.md "wikilink")。
  - Do 217E-2:
  - Do 217E-3:
  - Do 217E-4:
  - Do 217E-5:

### Do 217H

### Do 217K

动力装置采用两台1,730马力的[BMW
801G/H星型发动机](../Page/BMW_801.md "wikilink")，并延长了翼展以携带制导炸弹和导弹。

  - Do 217K-1:
  - Do 217K-2:
  - Do 217K-3:

### Do 217M

将Do 217K换上1,750马力[DB 603發動機后的产物](../Page/DB_603發動機.md "wikilink")。

  - Do 217M-1:
  - Do 217M-3:
  - Do 217M-5:
  - Do 217M-11:

### Do 217J

在Do 217E的基础上发展而来的夜间战斗机。金属机头内装备了四挺7,92毫米[MG
17机枪和四门](../Page/MG_17.md "wikilink")20毫米[MG
FF/M火炮](../Page/MG_FF.md "wikilink")。

  - Do 217J-1:
  - DO 217J-2:
  - Do 217L: Two experimental aircraft.

### Do 217N

在Do 217M基础上发展的夜间战斗机，机头武装与Do 217J相同，但在机背增裝四門指向斜上的[MG
151/20机炮](../Page/MG_151/20.md "wikilink")。

  - Do 217N-1:
  - Do 217N-2:

### Do 217P

专用高空侦察型，除了机翼前缘两台DB 603B发动机（1860马力）外，机身内还安装有一台DB
605T发动机专门负责为前者增压。据说该机的升限可以达到53000英尺。

### Do 217R

由於Do 217M和Do 217P的性能和載彈量未能夠達到要求，因此在Do217進行修改機身和機翼設計，演變成[Do
317](../Page/道尼爾Do_317.md "wikilink")，其機身和機翼稍微變大，特徵是載彈量更大、高空高速，進化為雙發重型轟炸機。

## 基本性能

### Do 217 J-2

### Do 217 M-1

## 相關條目

  - [Do 17轟炸機](../Page/Do_17轟炸機.md "wikilink")
  - [Ju 88轟炸機](../Page/Ju_88轟炸機.md "wikilink")
  - [夜間戰鬥機](../Page/夜間戰鬥機.md "wikilink")
  - [He 111轟炸機](../Page/He_111轟炸機.md "wikilink")
  - [Ju 287轟炸機](../Page/Ju_287轟炸機.md "wikilink")
  - [1919年－1945年德國軍用飛機列表](../Page/1919年－1945年德國軍用飛機列表.md "wikilink")

## 相關連結

[Category:德國轟炸機](../Category/德國轟炸機.md "wikilink")
[Category:多尼爾](../Category/多尼爾.md "wikilink")