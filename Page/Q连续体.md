**Q连续体**（）是《[星际迷航](../Page/星际迷航.md "wikilink")》虚拟[银河系中已知拥有最高智力且已存在很久的种族](../Page/银河系.md "wikilink")，有着掌控时空万物的超凡能力。「Q」是[全能的超次元存在的实体](../Page/全能.md "wikilink")，居住在Q连续体中。在《[下一代](../Page/星艦奇航記：銀河飛龍.md "wikilink")》、《[-{zh-hant:重返地球;zh-cn:航海家号;}-](../Page/星艦奇航記：重返地球.md "wikilink")》与《[深空九号](../Page/星艦奇航記：銀河前哨.md "wikilink")》中出现的几名「Q」都没有其他姓名，可以认为他们所有的人都共享一个代号「Q」。「Q」的本性善良，但「Q」的部分成员会表现出[小孩习气](../Page/小孩.md "wikilink")，所作的某些行为甚至可以用“捣蛋”来形容。「Q」的调皮习性让他们在[星系中的形象并不讨好](../Page/星系.md "wikilink")，被许多[种族视作敌人](../Page/种族.md "wikilink")。

[星际联邦第一次获知Q连续体的存在是在](../Page/星际联邦.md "wikilink")2364年，在[远点遭遇战这集中](../Page/远点遭遇战.md "wikilink")，「Q」（由[约翰·德·兰西飾演](../Page/约翰·德·兰西.md "wikilink")）扣押了[进取号-D](../Page/聯邦星艦企業號_\(NCC-1701-D\).md "wikilink")，并自导自演了一场法庭剧，指控人类“极端的残暴”。「Q」亦促使了[星际联邦与](../Page/星际联邦.md "wikilink")[博格人的第一次接觸](../Page/博格人.md "wikilink")。在一次Q连续体的[内战中](../Page/内战.md "wikilink")，「Q」曾请求[航海家号的舰长](../Page/联邦星舰航海家号_\(NCC-74656\).md "wikilink")[凯瑟琳·珍妮薇帮助其平息](../Page/凯瑟琳·珍妮薇.md "wikilink")[内战](../Page/内战.md "wikilink")。「Q」這個名字的來源，是为了纪念罗登伯里的一位朋友Janet
Quarton\[1\]。

## 參考文獻

## 另見

  - 《[星际之门](../Page/星际之门.md "wikilink")》中的[升天](../Page/升天_\(星際之門\).md "wikilink")（[Ascension](../Page/Ascension.md "wikilink")）

[ca:Q (Star Trek)\#Continuum
Q](../Page/ca:Q_\(Star_Trek\)#Continuum_Q.md "wikilink")

[Category:星际旅行种族](../Category/星际旅行种族.md "wikilink")
[Category:各物理或实体控制能力虚构角色](../Category/各物理或实体控制能力虚构角色.md "wikilink")
[Category:虚构变身者](../Category/虚构变身者.md "wikilink")
[Category:可操控时空的虚构角色](../Category/可操控时空的虚构角色.md "wikilink")
[Category:虛構瞬間移動能力者](../Category/虛構瞬間移動能力者.md "wikilink")
[Category:虛構分身能力者](../Category/虛構分身能力者.md "wikilink")
[Category:虛構變大縮小能力者](../Category/虛構變大縮小能力者.md "wikilink")

1.  [John de Lancie ("Q" - TNG, DS9,
    VOY)](http://www.startrek.com/startrek/view/community/chat/archive/transcript/1139.html)Q这个角色的名字，是为了纪念[吉恩·罗登伯里的一个朋友Janet](../Page/吉恩·罗登伯里.md "wikilink")
    Quarton。（The character of Q was named after a friend of Gene
    Roddenberry's named Janet Quarton.）