**Lazarus**是一个用于[应用程序快速开发（RAD）的自由](../Page/快速應用程式開發.md "wikilink")、跨平台的可视化[集成开发环境](../Page/集成开发环境.md "wikilink")（IDE）。使用[Free
Pascal编译器](../Page/Free_Pascal.md "wikilink")，支持[Object
Pascal语言](../Page/Object_Pascal.md "wikilink")，与[Delphi高度兼容](../Page/Delphi.md "wikilink")，并被视作后者的[自由软件替代品](../Page/自由软件.md "wikilink")。Lazarus目前支持多种语言，包括中文。软件开发者可使用Lazarus创建原生的命令行与[图形用户界面应用程序](../Page/图形用户界面.md "wikilink")，以及移动应用、Web应用、[Web服务](../Page/Web服务.md "wikilink")、可视化组件和各种函数库。Lazarus集成开发环境和Free
Pascal编译器支持多种操作系统，包括
[Windows](../Page/Microsoft_Windows.md "wikilink")、[GNU/Linux和](../Page/Linux.md "wikilink")[Mac](../Page/OS_X.md "wikilink")。

## 另请参见

  - [Delphi](../Page/Delphi.md "wikilink")
  - [Free Pascal](../Page/Free_Pascal.md "wikilink")
  - [Object Pascal](../Page/Object_Pascal.md "wikilink")
  - [Beyond Compare](../Page/Beyond_Compare.md "wikilink")

## 链接

  - [Lazarus官方网站](http://www.lazarus-ide.org/)
  - [SourceForge上的Lazarus项目](../Page/sourceforge:projects/lazarus/.md "wikilink")
  - [Lazarus
    Wiki](http://wiki.lazarus.freepascal.org/Overview_of_Free_Pascal_and_Lazarus/zh_TW)（正体中文）
  - [Lazarus和Free
    Pascal的Wiki世界](http://wiki.lazarus.freepascal.org/Main_Page/zh_CN)（中文）
  - [Lazarus
    Wiki](http://wiki.lazarus.freepascal.org/Lazarus_Documentation/zh_CN)（简体中文）
  - [Lazarus中文社区](http://www.fpccn.com/)（简体中文）

[Category:程序设计语言](../Category/程序设计语言.md "wikilink")
[Category:集成开发环境](../Category/集成开发环境.md "wikilink")