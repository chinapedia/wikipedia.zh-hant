**周慧婷**（），是[台灣的資深](../Page/台灣.md "wikilink")[新聞主播](../Page/新聞主播.md "wikilink")，[輔仁大學社會學系畢業](../Page/輔仁大學.md "wikilink")，[美國](../Page/美國.md "wikilink")[德州大學奧斯汀分校廣電研究所畢業](../Page/德州大學奧斯汀分校.md "wikilink")，曾先後擔任[中國電視公司](../Page/中國電視公司.md "wikilink")、[超級電視台及](../Page/超級電視台.md "wikilink")[三立新聞台的](../Page/三立新聞台.md "wikilink")[主播](../Page/主播.md "wikilink")，於2007年5月14日請辭。也曾擔任[中廣流行網](../Page/中廣流行網.md "wikilink")《愛的練習曲》主持人。其夫為前[中華民國](../Page/中華民國.md "wikilink")[國安會](../Page/國安會.md "wikilink")[秘書長](../Page/秘書長.md "wikilink")[金溥聰](../Page/金溥聰.md "wikilink")。

## 經歷

1990年考進中視。1994年進入新聞部，擔任[中視氣象台主播](../Page/中視氣象台.md "wikilink")。1995年，[黃晴雯與新婚夫婿赴美度假發生車禍](../Page/黃晴雯.md "wikilink")，周慧婷代班《[中視夜線新聞](../Page/中視夜線新聞.md "wikilink")》主播（與[王育誠搭檔](../Page/王育誠.md "wikilink")）。

## 爭議

1997年11月18日，[白曉燕命案三主嫌之一](../Page/白曉燕命案.md "wikilink")[陳進興闖入南非駐中華民國大使武官卓懋祺](../Page/陳進興_\(台灣罪犯\).md "wikilink")（McGill
Alexander）家中，挾持卓懋祺一家五口。18日晚間起，國內媒體爭相與陳進興電話連線,主播與電視台的脫序行為引起社會嘩然，亦導致各界日後一波波對「新聞專業倫理」的批判。（參見[臺灣媒體亂象](../Page/臺灣媒體亂象.md "wikilink")）

## 著作

|        |                           |                                    |                    |
| ------ | ------------------------- | ---------------------------------- | ------------------ |
| **年份** | **書名**                    | **出版社**                            | **ISBN**           |
| 2002年  | 《好孕自然來：婷主播的懷孕壓箱寶》         | [時報文化](../Page/時報文化.md "wikilink") | ISBN 9571335657    |
| 2010年  | 《120天，給孩子一生的養分：一趟愛與發現的旅程》 | [天下文化](../Page/天下文化.md "wikilink") | ISBN 9789862165973 |

## 參考文獻

[H慧](../Category/周姓.md "wikilink")
[C周](../Category/輔仁大學校友.md "wikilink")
[Category:德州大學奧斯汀分校校友](../Category/德州大學奧斯汀分校校友.md "wikilink")
[C周](../Category/台灣記者.md "wikilink")
[C周](../Category/台灣電視主播.md "wikilink")
[C周](../Category/台灣新聞節目主持人.md "wikilink")
[C周](../Category/中視主播.md "wikilink")
[C周](../Category/超視主播.md "wikilink")
[C周](../Category/三立主播.md "wikilink")
[C周](../Category/台灣廣播主持人.md "wikilink")