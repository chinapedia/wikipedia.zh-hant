**質子**（）是一種帶有1个[單位电荷正電的穩定](../Page/單位电荷.md "wikilink")[強子](../Page/強子.md "wikilink")，通常標記為****或****。每個[原子的](../Page/原子.md "wikilink")[原子核內部至少會含有一個質子](../Page/原子核.md "wikilink")，質子的數量稱為[原子序數](../Page/原子序數.md "wikilink")；另外，還可能含有[中子](../Page/中子.md "wikilink")，這些質子與中子都被稱為[核子](../Page/核子.md "wikilink")。由於每種[元素的原子都含有獨特數量的質子](../Page/元素.md "wikilink")，每種元素具有獨特的原子序數。

1917年，[欧内斯特·卢瑟福做實驗發現](../Page/欧内斯特·卢瑟福.md "wikilink")，使用[α粒子撞擊](../Page/α粒子.md "wikilink")[氮原子核](../Page/氮.md "wikilink")，可以提取[氫原子核](../Page/氫.md "wikilink")。盧瑟福因此推斷，氫原子核是氮原子核與所有更重的原子核的基礎材料。由於這重要結果，卢瑟福被公認為質子的發現者。

在[粒子物理學的現代](../Page/粒子物理學.md "wikilink")[標準模型裏](../Page/標準模型.md "wikilink")，質子是由兩個[上夸克與一個](../Page/上夸克.md "wikilink")[下夸克組成的強子](../Page/下夸克.md "wikilink")。夸克的[靜質量只貢獻出大約](../Page/靜質量.md "wikilink")1%質子質量，剩餘的質子質量主要源自於夸克的動能與綑綁夸克的的能量。\[1\]

因為質子是由三個夸克組成，質子不是[基本粒子](../Page/基本粒子.md "wikilink")，質子具有物理尺寸，但這尺寸並不能完美良好定義，由於質子的表面很模糊，因為這表面是由作用力的影響來定義，而這作用力不會突然終止。質子的半徑（更仔細地說，）大約為0.84–0.87[飛米](../Page/飛米.md "wikilink")。\[2\]

自由質子是不與其它核子或[電子結合在一起的質子](../Page/電子.md "wikilink")。自由質子很穩定，尚未被觀察到自發[衰變成其它種粒子](../Page/衰變.md "wikilink")。質子與電子之間會相互親和，但當能量或溫度高到足以將質子與電子分離之時，就可以自然地找到自由質子。在[等離子體裏](../Page/等離子體.md "wikilink")，溫度非常高，質子無法與電子結合在一起，因此自由質子可以存在。在太空裏，傳播了星際距離的[宇宙線](../Page/宇宙線.md "wikilink")，其成分有90%是具有高能量與高動量的自由質子。自由中子不穩定，會進行[衰變](../Page/衰變.md "wikilink")，這過程的產物是質子、電子與[反中微子](../Page/反中微子.md "wikilink")。

## 物理性質

質子是[自旋為](../Page/自旋.md "wikilink")½的[費米子](../Page/費米子.md "wikilink")，是由三個[夸克組成](../Page/夸克.md "wikilink")，因此是一種[重子](../Page/重子.md "wikilink")。在這三個夸克之中，有兩個是[上夸克](../Page/上夸克.md "wikilink")，一個是[下夸克](../Page/下夸克.md "wikilink")，它們被由[膠子傳遞的](../Page/膠子.md "wikilink")[強相互作用綑綁在一起](../Page/強相互作用.md "wikilink")。\[3\]\[4\]從現代角度來看，更仔細地說明，質子是由三個[價夸克](../Page/價夸克.md "wikilink")（兩個上夸克，一個下夸克）、膠子與短暫存在的[海夸克對組成](../Page/夸克#海夸克.md "wikilink")。<ref name="Bahr">

</ref>
質子的正電荷呈指數遞減分佈，離質心越遠，密度越低，[方均根半徑約為](../Page/方均根.md "wikilink")0.8[飛米](../Page/飛米.md "wikilink")。\[5\]

在原子核裏的質子與中子都被稱為[核子](../Page/核子.md "wikilink")，它們被[核力綑綁在原子核內部](../Page/核力.md "wikilink")。\[6\]
[氫元素的原子核只有一個質子](../Page/氫.md "wikilink")；在它的所有[同位素之中](../Page/同位素.md "wikilink")，最常見的是氫-1，符號為<sup>1</sup>H或「**H**」，它的原子核不含有任何中子，是個孤寂的質子。另外還有兩種較重的天然同位素，[重氫](../Page/重氫.md "wikilink")**<sup>2</sup>H**或**D**與[超重氫](../Page/超重氫.md "wikilink")**<sup>3</sup>H**或**T**，它們的原子核分別擁有一個與兩個中子。所有其它種元素的原子核都是由至少兩個質子與各種數量個中子所組成。\[7\]

### 穩定性

質子極為穩定，不會自行衰變，至今為止，還沒有任何實驗觀察到質子的自發性衰變。但是，在粒子物理學裏，有些[大統一理論主張](../Page/大統一理論.md "wikilink")，質子衰變應該會發生，例如，聲稱，對於衰變管道→+，[平均壽命低於](../Page/指数衰减#平均寿命.md "wikilink")，\[8\]有些理論預測，質子平均壽命低於\[9\]\[10\]
\[11\]。

在日本的[超級神岡探測器完成的實驗](../Page/超級神岡探測器.md "wikilink")，對於衰變成[反緲子與](../Page/反緲子.md "wikilink")\[\[π介子|中性

[Category:重子](../Category/重子.md "wikilink")
[质子](../Category/质子.md "wikilink")
[Category:陽離子](../Category/陽離子.md "wikilink")
[Category:氫原子物理](../Category/氫原子物理.md "wikilink")
[Category:1910年代科学](../Category/1910年代科学.md "wikilink")

1.
2.

3.

4.

5.

6.

7.

8.
9.

10.

11.