**吳相湘**，[湖南省](../Page/湖南省.md "wikilink")[常德人](../Page/常德.md "wikilink")，中國歷史學家。

## 生平

1912年1月出生於湖南常德旧家，早年受教於[孟森](../Page/孟森.md "wikilink")、[傅斯年](../Page/傅斯年.md "wikilink")、[胡適等著名學者](../Page/胡適.md "wikilink")，1937年[國立北京大學歷史系畢業](../Page/國立北京大學.md "wikilink")，任職長沙[中央研究院](../Page/中央研究院.md "wikilink")（中研院）。抗日戰爭爆發，入國民黨第九戰區司令部工作，收集戰爭資料，撰有《第三次長沙會戰》等文稿。抗戰勝利後，歷任[北平故宮博物院編纂](../Page/北平故宮博物院.md "wikilink")、[蘭州大學副教授](../Page/蘭州大學.md "wikilink")，中華民國政府遷台後，曾任[國立臺灣大學歷史系教授](../Page/國立臺灣大學.md "wikilink")、[新加坡南洋大學歷史系主任](../Page/新加坡南洋大學.md "wikilink")、[中國文化學院史學研究所教授等職](../Page/中國文化學院.md "wikilink")。

吳相湘脾氣硬，堅持原則，遇事絕不妥協。中研院[歷史語言研究所副研究員](../Page/歷史語言研究所.md "wikilink")[李孝悌說](../Page/李孝悌.md "wikilink")：「他是個有狂傲霸氣的人。」吳相湘是少數[李永熾](../Page/李永熾.md "wikilink")、[李敖在台大敬重的師長](../Page/李敖.md "wikilink")。曾參加[哥倫比亞大學](../Page/哥倫比亞大學.md "wikilink")《中國民國時期人名典》的撰述工作。1962年11月，被[中国国民党开除党籍](../Page/中国国民党.md "wikilink")。1975年赴美定居，不問世事。1996年曾回大陆参加「孙中山130周年诞辰国际学术会议」。2007年9月21日病逝[伊利諾州自由市](../Page/伊利諾州.md "wikilink")\[1\]。

## 著作

  - 《民國百人傳》
  - 《[宋教仁](../Page/宋教仁.md "wikilink")：中國民主憲政先驅》
  - 《民國政治人物傳》
  - 《[晏陽初傳](../Page/晏陽初.md "wikilink")：為全球鄉村改造奮鬥六十年》
  - 《中華民國國父──孫逸仙傳》
  - 《第二次中日战争史》
  - 《俄帝侵略中國史》
  - 《晚清宮廷實紀》等。

主编或合编有

  - 《中国现代史丛刊》
  - 《中国现代史料丛书》
  - 《中国史学丛书》
  - 《民国史料丛刊》等\[2\]；《三生有幸》是其回憶錄。

## 注釋

<div class="references-sall">

<references />

</div>

## 參考書目

  - 2007年9月24日《中國時報》

[Category:中華民國歷史學家](../Category/中華民國歷史學家.md "wikilink")
[Category:中國文化大學教授](../Category/中國文化大學教授.md "wikilink")
[Category:新加坡南洋大學教師](../Category/新加坡南洋大學教師.md "wikilink")
[Category:國立臺灣大學教授](../Category/國立臺灣大學教授.md "wikilink")
[Category:蘭州大學教授](../Category/蘭州大學教授.md "wikilink")
[Category:北京大學校友](../Category/北京大學校友.md "wikilink")
[Category:移民美國的中華民國人](../Category/移民美國的中華民國人.md "wikilink")
[Category:台灣戰後湖南移民](../Category/台灣戰後湖南移民.md "wikilink")
[Category:常德人](../Category/常德人.md "wikilink")
[X](../Category/吳姓.md "wikilink")
[Category:被開除中國國民黨黨籍者](../Category/被開除中國國民黨黨籍者.md "wikilink")

1.  [現代史家吳相湘病逝美國](http://www.epochtimes.com/b5/7/9/22/n1843123.htm)
2.  [著名歷史學家吳相湘病逝美國](http://blog.udn.com/jason080/1267822)