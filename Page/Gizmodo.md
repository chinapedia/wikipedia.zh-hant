**Gizmodo**是一個報道[消費電子產品相關消息的流行科技](../Page/消費電子產品.md "wikilink")[網誌](../Page/網誌.md "wikilink")\[1\]，是由[尼克·丹頓營運的](../Page/尼克·丹頓.md "wikilink")[Gawker
Media網絡的一部份](../Page/Gawker_Media.md "wikilink")。
[Gizmodo.svg](https://zh.wikipedia.org/wiki/File:Gizmodo.svg "fig:Gizmodo.svg")

Gizmodo於2002年開辦，最初由Peter
Rojas負責編輯，但他後來受雇於[Weblogs公司開辦相似的科技網誌](../Page/Weblogs.md "wikilink")[Engadget](../Page/Engadget.md "wikilink")\[2\]。至2004年年中Gizmodo與Gawker合共帶來每月6,000元的收入\[3\]。

2005年，[VNU與Gawker](../Page/VNU.md "wikilink")
Media結盟將Gizmodo跨越[歐洲發行](../Page/歐洲.md "wikilink")，由VNU將內容翻譯到[法語](../Page/法語.md "wikilink")、[德語](../Page/德語.md "wikilink")、[荷蘭語](../Page/荷蘭語.md "wikilink")、[西班牙語與](../Page/西班牙語.md "wikilink")[意大利語並增加與歐洲當地相關的資料](../Page/意大利語.md "wikilink")\[4\]。除了歐洲以外，另外還在[澳洲](../Page/澳洲.md "wikilink")、[巴西](../Page/巴西.md "wikilink")、[波蘭及](../Page/波蘭.md "wikilink")[日本等國設立以當地語言所撰寫的分站](../Page/日本.md "wikilink")。

一名Gizmodo的[網誌作者曾拍攝首張](../Page/網誌作者.md "wikilink")2007年[國際消費電子展場地的照片](../Page/國際消費電子展.md "wikilink")\[5\]，另外根據[路透社所載曾有記者在同時舉行的](../Page/路透社.md "wikilink")[Macworld會議與博覽會上爭論Gizmodo還是](../Page/Macworld會議與博覽會.md "wikilink")[Engadget對](../Page/Engadget.md "wikilink")2007年[史提夫·賈伯斯的演說有較好的實況報導](../Page/史提夫·賈伯斯.md "wikilink")\[6\]。

2010年4月，Gizmodo付費取得一部為[蘋果公司員工遺落而被他人拾獲的](../Page/蘋果公司.md "wikilink")[iPhone樣機](../Page/iPhone.md "wikilink")，隨即拍照並拆解研究（因蘋果公司以遠端遙控清除該手機的資料，因此無法開機研究其新版[iPhone
OS](../Page/iPhone_OS.md "wikilink")），刊登於網誌上推測應該是同年將要發表的[iPhone
4G](../Page/iPhone_4G.md "wikilink")。隨後蘋果公司發函要求Gizmodo歸還該手機，而發表文章的亞裔作者陳傑森（譯音，Jason
Chen）住家遭警方搜索\[7\]。

## 工作人員

  - Brian Lam 主編
      - Jason Chen 副編輯
      - Nicholas Deleon 副編輯
      - Travis Hudson 副編輯
      - Charlie White 副編輯

## 參考

<references />

## 外部連結

  - [Gizmodo.com](http://www.gizmodo.com)

[Category:網誌](../Category/網誌.md "wikilink")
[Category:2002年美國建立](../Category/2002年美國建立.md "wikilink")

1.
2.
3.
4.
5.
6.
7.