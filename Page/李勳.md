**李勳**（，），[韓國](../Page/韓國.md "wikilink")[男演員](../Page/男演員.md "wikilink")。

## 演出作品

### 電視劇

  - 1997年：[KBS](../Page/韓國放送公社.md "wikilink")《[Star](../Page/Star_\(電視劇\).md "wikilink")》
  - 1999年：[MBC](../Page/文化廣播_\(韓國\).md "wikilink")《[王朝](../Page/王朝_\(韓國電視劇\).md "wikilink")》又譯《[铁汉柔情](../Page/王朝_\(韓國電視劇\).md "wikilink")》
  - 2002年：MBC《[馬上馬下](../Page/馬上馬下.md "wikilink")》飾演 崔長雨
  - 2003年：SBS《[王的女子](../Page/王的女子.md "wikilink")》
  - 2005年：SBS《[三葉草](../Page/三葉草_\(電視劇\).md "wikilink")》
  - 2006年：SBS《[愛情與野望](../Page/愛情與野望#2006年.md "wikilink")》飾演 朴泰修
  - 2007年：SBS《[尋找兒子三萬里](../Page/尋找兒子三萬里.md "wikilink")》飾演 韓啟弼
  - 2007年：SBS《[我男人的女人](../Page/我男人的女人.md "wikilink")》飾演 李東何
  - 2008年：[SBS](../Page/SBS_\(韓國\).md "wikilink")《[幸福百分百](../Page/幸福百分百.md "wikilink")》飾演
    李俊秀
  - 2009年：SBS《[Dream](../Page/Dream_\(電視劇\).md "wikilink")》
  - 2011年：MBC《[不屈的兒媳婦](../Page/不屈的兒媳婦.md "wikilink")》飾演 文真宇
  - 2012年：tvN《[21世紀家族](../Page/21世紀家族.md "wikilink")》飾演 李誠奇
  - 2012年：MBC《[May Queen](../Page/May_Queen.md "wikilink")》飾演 尹靖宇
  - 2012年：[TBS](../Page/TBS電視台.md "wikilink")/[SBS](../Page/SBS.md "wikilink")
    Plus《[浪漫滿屋 TAKE2](../Page/浪漫滿屋_TAKE2.md "wikilink")》飾演 李俊
  - 2013年：KBS《[一絲的純情](../Page/一絲的純情.md "wikilink")》飾演 河正佑
  - 2016年：SBS《[愛情來了](../Page/愛情來了_\(電視劇\).md "wikilink")》飾演 琴方碩

### 電影

  - 2007年：《[第一街的奇蹟](../Page/第一街的奇蹟.md "wikilink")》

### MV

  - 2010年：Aurora《Tattabeul》
  - 2012年：Aurora《捉迷藏》

## 外部連結

  - [EPG](https://web.archive.org/web/20110527145816/http://epg.epg.co.kr/Star/Profile/index.asp?actor_id=180)


[L](../Category/韓國電視演員.md "wikilink")
[L](../Category/高麗大學校友.md "wikilink")
[L](../Category/光州廣域市出身人物.md "wikilink")
[L](../Category/李姓.md "wikilink")