**古神翼龍屬**（[屬名](../Page/屬.md "wikilink")：）又譯**塔佩雅拉翼龍**，屬名在[圖皮語裡意為古老的主宰](../Page/圖皮語.md "wikilink")，是種生存於[白堊紀](../Page/白堊紀.md "wikilink")[巴西的](../Page/巴西.md "wikilink")[翼龍類](../Page/翼龍類.md "wikilink")。古神翼龍在體型上呈多樣性，有些物種翼展長6公尺。每個物種有不同大小、形狀的冠飾，可能作為與其他古神翼龍的信號與展示物，如同[大嘴鳥用牠們鮮豔的鳥喙傳達信號給彼此](../Page/大嘴鳥.md "wikilink")。

[模式種是](../Page/模式種.md "wikilink")**沃氏古神翼龍**，其冠飾是由口鼻部上的半圓冠飾，以及頭部後方延伸出來的骨質分岔，兩者所構成。

## 分類與種

[Tapejarines_mmartyniuk.png](https://zh.wikipedia.org/wiki/File:Tapejarines_mmartyniuk.png "fig:Tapejarines_mmartyniuk.png")的頭部復原圖\]\]
古神翼龍目前只有一個有效種，即[模式種](../Page/模式種.md "wikilink")**沃氏古神翼龍**（*T.
wellnhoferi*）。在歷來被歸類為古神翼龍屬的三個物種中，沃氏古神翼龍是體型最小，也是最早被發表者，沒有發現過冠飾的軟組織。

其餘的兩個物種目前都被認為屬於[雷神翼龍屬](../Page/雷神翼龍屬.md "wikilink")，分別是皇帝古神翼龍（*Tapejara
imperator*），體型最大，頭骨前後連接者兩個骨質分岔，可能用於支撐大型的角質頭冠，以及帆冠古神翼龍（*Tapejara
navigans*），體型居中，頭冠類似皇帝古神翼龍，但形狀較狹窄、呈船帆狀，而且缺乏頭後方的骨質分岔。

在2007年，數名科學家認為古神翼龍的三個種具有不同的特徵，足以成立個別的屬。Kellner與Campos認為只有皇帝古神翼龍可以建立新屬，因此在2007年建立了雷神翼龍\[1\]同年，[大衛·安文](../Page/大衛·安文.md "wikilink")（David
Unwin）與D. M. Martill認為皇帝古神翼龍、*T. navigans*是相同物種，因此分別建立為*Ingridia
imperator*、*Ingridia
navigans*。\[2\]Kellner與Campos的雷神翼龍研究，比安文與Martill的*Ingridia*研究早了數個月公佈。由於兩組人員都使用*imperator*作為模式種，因此*Ingridia*成為雷神翼龍的[次客觀異名](../Page/次客觀異名.md "wikilink")。皇帝古神翼龍被改名為雷神翼龍，但*T.
navigans*仍需要建立新的屬名\[3\]。

在2011年，另一群科學家將*T.
navigans*改歸類為[雷神翼龍的第二個種](../Page/雷神翼龍.md "wikilink")\[4\]。

## 古生物學

在2011年，科學家比較[翼龍類](../Page/翼龍類.md "wikilink")、現代[鳥類與](../Page/鳥類.md "wikilink")[爬行動物的](../Page/爬行動物.md "wikilink")[鞏膜環大小](../Page/鞏膜環.md "wikilink")，提出古神翼龍可能屬於無定時活躍性的動物，覓食、移動行為跟白天黑夜沒有正相關，只休息短暫時間\[5\]。

## 大眾文化

*Tapejara
navigans*出現在[BBC](../Page/BBC.md "wikilink")《[與恐龍共舞](../Page/與恐龍共舞.md "wikilink")》（*Walking
with
Dinosaurs*）電視節目，假設牠們在[巴西度過繁殖季節](../Page/巴西.md "wikilink")。有趣的是，*Tapejara
navigans*出現在1999年的電視節目，但在2003年才被正式命名。

## 參考資料

  - In the Sky (Dinosaurs) (Library Binding) (ISBN 0-8368-2918-2)

## 外部連結

  - [Joe Tucciarone所繪想像圖](http://members.aol.com/Dinofiles/tap.html)

[Category:神龍翼龍超科](../Category/神龍翼龍超科.md "wikilink")
[Category:白堊紀翼龍類](../Category/白堊紀翼龍類.md "wikilink")

1.
2.  Unwin, D. M. and Martill, D. M. (2007). "Pterosaurs of the Crato
    Formation." In Martill, D. M., Bechly, G. and Loveridge, R. F.
    (eds), *The Crato Fossil Beds of Brazil: Window into an Ancient
    World.* Cambridge University Press (Cambridge), pp. 475-524.
3.  Naish, D. (2008). "Crato Formation fossils and the new tapejarids."
    Weblog entry. *Tetrapod Zoology*. 18 January 2008. Accessed 31
    January 2008 ().
4.  Pinheiro, F.L., Fortier, D.C., Schultz, C.L., De Andrade, J.A.F.G.
    and Bantim, R.A.M. (in press). "New information on *Tupandactylus
    imperator*, with comments on the relationships of Tapejaridae
    (Pterosauria)." *Acta Palaeontologica Polonica*, in press, available
    online 03 Jan 2011.
5.