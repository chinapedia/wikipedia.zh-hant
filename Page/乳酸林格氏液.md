**乳酸林格氏液**（，**RL**），又稱**乳酸鈉溶液**（sodium lactate
solution）或**哈特曼氏液**（），是一种含有[氯化钠](../Page/氯化钠.md "wikilink")、[乳酸鈉](../Page/乳酸鈉.md "wikilink")、[氯化鉀](../Page/氯化鉀.md "wikilink")，以及[氯化鈣的水溶液](../Page/氯化鈣.md "wikilink")\[1\]。本品可作為[低血容或](../Page/血容量減少.md "wikilink")[低血壓患者的補液](../Page/低血壓.md "wikilink")\[2\]。本品可用於治療非[乳酸性酸中毒的](../Page/乳酸性酸中毒.md "wikilink")，也可用於眼睛在後的清洗\[3\]\[4\]。本品可經由[靜脈注射給予](../Page/靜脈注射.md "wikilink")，也可以直接塗敷需要的部位\[5\]\[6\]。

副作用包含[過敏](../Page/過敏.md "wikilink")、[高鉀血症](../Page/高鉀血症.md "wikilink")、\[7\]。本品不適合與某些藥物合用，且不建議作為[輸血時的稀釋液](../Page/輸血.md "wikilink")\[8\]。乳酸林格氏液較[生理食鹽水不易造成](../Page/生理食鹽水.md "wikilink")\[9\]\[10\]。[妊娠及](../Page/妊娠.md "wikilink")[哺乳期間使用通常安全](../Page/母乳哺育.md "wikilink")\[11\]，本品屬於\[12\]，其滲透壓與[血液相等](../Page/血液.md "wikilink")\[13\]。

於1880年發明，並於1930年開始加入乳酸\[14\]。本品列名於[世界卫生组织基本药物标准清单之中](../Page/世界卫生组织基本药物标准清单.md "wikilink")，為基礎公衛體系必備藥物之一\[15\]。乳酸林格氏液已有[通用名藥物生產上市](../Page/通用名藥物.md "wikilink")\[16\]。在[发展中国家](../Page/发展中国家.md "wikilink")，其每升批發價介於0.60至2.30之間\[17\]。對於[肝功能不佳者](../Page/肝衰竭.md "wikilink")，醋酸林格氏液可能可以做為乳酸林格氏液的代用品，其乳酸成分由[醋酸代替](../Page/乙酸盐.md "wikilink")\[18\]。在[斯堪的纳维亚地區一般使用醋酸林格氏液](../Page/斯堪的纳维亚.md "wikilink")\[19\]。

## 成分

每[公升乳酸林格氏液含有](../Page/公升.md "wikilink")：

  - 130 毫[当量](../Page/当量.md "wikilink")[钠离子](../Page/钠.md "wikilink")
  - 109 毫当量[氯离子](../Page/氯.md "wikilink")
  - 28 毫当量[乳酸质](../Page/乳酸质.md "wikilink")
  - 4 毫当量[钾离子](../Page/钾.md "wikilink")
  - 3 毫当量[钙离子](../Page/钙.md "wikilink")

**注：毫当量=[摩尔数](../Page/摩尔.md "wikilink")×化学价／1000**。上述成分一般来自[氯化钠](../Page/氯化钠.md "wikilink")、[乳酸钠](../Page/乳酸钠.md "wikilink")、[氯化钙和](../Page/氯化钙.md "wikilink")[氯化钾](../Page/氯化钾.md "wikilink")。

## 应用

林格氏液用来治疗外伤、手术、烧伤等造成的失血，和使[肾衰竭病人促进造尿](../Page/肾衰竭.md "wikilink")。这两种情况下产生的[酸中毒被](../Page/酸中毒.md "wikilink")[乳酸在](../Page/乳酸.md "wikilink")[肝脏中的代谢产物平衡](../Page/肝脏.md "wikilink")，因此獸醫也常用乳酸林格氏液來治療[腎衰竭的](../Page/腎衰竭.md "wikilink")[貓](../Page/貓.md "wikilink")。

用量由估计的体液流失计算。补充体液的时候，一般用量是每[公斤体重每小时](../Page/公斤.md "wikilink")2-3[毫升](../Page/毫升.md "wikilink")。[电解质的浓度上](../Page/电解质.md "wikilink")，乳酸林液含[钠过多而含](../Page/钠.md "wikilink")[钾不足](../Page/钾.md "wikilink")，故不宜用于长期输液，特别是对儿童病人。

乳酸林液是比较接近[两栖动物内环境的液体](../Page/两栖动物.md "wikilink")，在实验中可以用来延长[青蛙](../Page/青蛙.md "wikilink")[心脏在体外的跳动时间](../Page/心脏.md "wikilink")、保持[两栖类其他离体组织器官的生理活性等](../Page/两栖类.md "wikilink")。

## 附註

[Category:药物](../Category/药物.md "wikilink")
[Category:静脉注射液](../Category/静脉注射液.md "wikilink")

1.

2.
3.
4.
5.

6.

7.
8.
9.
10.
11.
12.

13.
14.

15.

16.
17.

18.

19.