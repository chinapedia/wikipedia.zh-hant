[Realgar1.jpg](https://zh.wikipedia.org/wiki/File:Realgar1.jpg "fig:Realgar1.jpg")
[Realgar2.jpg](https://zh.wikipedia.org/wiki/File:Realgar2.jpg "fig:Realgar2.jpg")

**雄黄**，又称作**石黄**、**黄金石**、**鸡冠石**，是一种含[硫和](../Page/硫.md "wikilink")[砷的矿石](../Page/砷.md "wikilink")，质软、性脆，通常为粒状、紧密状块，或者粉末，[条痕呈浅桔红色](../Page/条痕.md "wikilink")。主要成分是[四硫化四砷](../Page/四硫化四砷.md "wikilink")（As<sub>4</sub>S<sub>4</sub>，佔90%以上）。雄黄主要产于低温热液矿床中，常与[雌黄](../Page/雌黄.md "wikilink")（As<sub>2</sub>S<sub>3</sub>）、[辉锑矿](../Page/辉锑矿.md "wikilink")、[辰砂共生](../Page/辰砂.md "wikilink")；产于温泉沉积物和硫质[火山喷气孔内沉积物的雄黄](../Page/火山喷气孔.md "wikilink")，则常与雌黄共生。不溶于水和[盐酸](../Page/盐酸.md "wikilink")，可溶于[硝酸](../Page/硝酸.md "wikilink")，溶液呈黄色。置于阳光下曝晒，会变为黄色的雌黄和[砷华](../Page/砒霜.md "wikilink")，所以保存应避光以免受风化。雄黄要被[煅烧才会被氧化为剧毒成分](../Page/煅烧.md "wikilink")[三氧化二砷](../Page/三氧化二砷.md "wikilink")，即[砒霜](../Page/砒霜.md "wikilink")。

## 用途

砷礦物主要用於提煉元素砷、製造砷酸和砷的化合物，如[砷酸鈣](../Page/砷酸鈣.md "wikilink")、[砷酸鈉](../Page/砷酸鈉.md "wikilink")、[砷酸鉛等](../Page/砷酸鉛.md "wikilink")。

在冶金工業中用於煉製[砷合金](../Page/砷合金.md "wikilink")；[砷鉛合金在軍事工業中用以製造子彈頭](../Page/砷鉛合金.md "wikilink")、軍用毒藥和煙火；[砷銅合金等用於製造雷達零件和汽車](../Page/砷銅合金.md "wikilink")。

在輕工業中用以製造乳白色玻璃、玻璃脫色、浸洗羊毛、製革藥劑以及用於木材防腐。

在農業上用作殺蟲劑、除草劑、滅鼠藥等含砷農藥。

人工煉製的砷華藥物名稱為砒霜。

雄黃精礦粉可用於製造鞭炮、煙花。

雄黃加雙氧水生成砷酸，再加熱脫水，便脫氧生成三氧化二砷，即精製砒霜。

## 雄黄晶体结构

雄黄属单斜晶系，[晶体群为P](../Page/晶体群.md "wikilink")2(1)/n。结晶为桔红色，透明到半透明的雄黄晶体，其柱状晶体长短参差，粗细相伴，多方向生长。晶体呈金刚光泽，断口为树脂光泽。单晶呈细小的柱状、针状，但天然单晶较为少见。

[File:Realgar-3D-balls.png|雄黄](File:Realgar-3D-balls.png%7C雄黄)[晶体结构](../Page/晶体结构.md "wikilink")
[File:Realgar-unit-cell-3D-balls.png|雄黄](File:Realgar-unit-cell-3D-balls.png%7C雄黄)[晶胞包含分子](../Page/晶体结构#晶胞.md "wikilink")
As<sub>4</sub>S<sub>4</sub>

## 文化

[漢字文化圈在每年農曆五月初五](../Page/漢字文化圈.md "wikilink")[端午節有喝](../Page/端午節.md "wikilink")[雄黃酒以闢邪的傳統習慣](../Page/雄黃酒.md "wikilink")。

## 中藥

[中醫認為雄黃性味辛](../Page/中醫.md "wikilink")、苦、平，歸心、肝、脾、胃、[大腸經](../Page/大腸經.md "wikilink")。

含雄黃的中成藥：七珍丸、小兒化毒散、小兒至寶丸、小兒驚風散、小兒清熱片、牙痛一粒丸、牛黃至寶丸、牛黃抱龍丸、牛黃消炎片、牛黃清心丸、牛黃解毒丸（片）、牛黃鎮驚丸、六應丸、安宮牛黃丸（散）、紅靈散、醫癇丸、局方至寶散、阿魏化痞膏、純陽正氣丸、珠黃吹喉散、梅花點舌丸、紫金錠、暑症片、痧藥、升麻鱉甲湯。

## 毒性

雄黄有毒，虽然毒性不像砒霜那么大。如果从一天口服大约10毫克雄黄，即可导致慢性砷中毒。中医常用药[牛黄解毒片每片](../Page/牛黄解毒片.md "wikilink")（大片）含有50毫克雄黄，按药典规定一天服4～6片。\[1\]

## 参见

  - [雌黄](../Page/雌黄.md "wikilink")

## 参考文献

<div class="references-small">

<references />

  - [雄黄 - Webmineral.com](http://webmineral.com/data/Realgar.shtml)
  - [雄黄 - Mindat.org](http://www.mindat.org/min-3375.html)

</div>

## 外部链接

  - [Realgar](http://www.mindat.org/min-3375.html) mindat.org - 矿物数据库。
  - [Realgar](http://www.reciprocalnet.org/recipnet/showsample.jsp?sampleId=27344418)
    Reciprocal Net - 结构数据。
  - [雄黄晶体结构](http://database.iem.ac.ru/mincryst/s_carta.php?REALGAR+3938)
    WWW-MINCRYST
  - [雄黃
    Xionghuang](http://libproject.hkbu.edu.hk/was40/detail?channelid=47953&searchword=pid=B00406)
    中藥材圖像數據庫 (香港浸會大學中醫藥學院)
  - [雄黃 Xiong
    Huang](http://libproject.hkbu.edu.hk/was40/detail?channelid=44273&searchword=herb_id=D00205)
    中藥標本數據庫 (香港浸會大學中醫藥學院)

[Category:硫化物礦物](../Category/硫化物礦物.md "wikilink")
[Category:含砷矿物](../Category/含砷矿物.md "wikilink")

1.  [常用中成药的真相——牛黄解毒片](http://www.xys.org/xys/netters/Fang-Zhouzi/blog/niuhuangjiedupian.txt)[方舟子](../Page/方舟子.md "wikilink")，[新语丝](../Page/新语丝.md "wikilink")，2008-12-17