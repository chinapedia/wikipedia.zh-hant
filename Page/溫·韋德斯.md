**恩斯特·威廉·“溫”·韋德斯**（，），出生於德國[杜塞爾多夫的](../Page/杜塞爾多夫.md "wikilink")[電影導演](../Page/電影導演.md "wikilink")、[攝影師](../Page/攝影師.md "wikilink")；曾榮獲法國[坎城影展及德國](../Page/坎城影展.md "wikilink")[柏林影展最佳影片導演](../Page/柏林影展.md "wikilink")；是德國當代電影大師之一，也是[德國新浪潮導演群裡重要成員之一](../Page/德國新浪潮.md "wikilink")；影片風格以清新抒情、運鏡優美著稱。

## 早年

溫·韋德斯畢業自[魯爾區](../Page/魯爾區.md "wikilink")[奧伯豪森的一所文理中學](../Page/奧伯豪森.md "wikilink")；之後他於[杜塞爾多夫與](../Page/杜塞爾多夫.md "wikilink")[弗萊堡研習](../Page/弗萊堡.md "wikilink")[醫學](../Page/醫學.md "wikilink")（1963年─1964年）及哲學。然而，他放棄了他的學業，並在1966年10月搬到[巴黎想要成為](../Page/巴黎.md "wikilink")[畫家](../Page/畫家.md "wikilink")。他未能通过法国高等电影研究院（la
femis前身）考试，轉而成為美國[藝術家](../Page/藝術家.md "wikilink")）位於[蒙帕納斯的工作室中的雕刻師](../Page/蒙帕納斯.md "wikilink")。這段時間他開始著迷電影，曾經在當地電影院裡一天連看五部電影。

這樣對電影的熱情使他著迷並也決定了他一生的志業，1967年他返回[德國在](../Page/德國.md "wikilink")[聯美影業公司的](../Page/聯美影業公司.md "wikilink")[杜塞爾多夫工作室工作](../Page/杜塞爾多夫.md "wikilink")。接著，他進入[慕尼黑電視與電影大學就讀](../Page/慕尼黑電視與電影大學.md "wikilink")。在1967年至1970年這段在慕尼黑電視與電影大學的時間中，溫德斯也在《電影評論》（FilmKritik）雜誌擔任影評，後來也在《[南德意志報](../Page/南德意志報.md "wikilink")》（慕尼黑當地的日報）、《Twen》雜誌與《[明鏡](../Page/明鏡.md "wikilink")》等報章雜誌撰寫影評。

溫德斯在大學期間拍了幾部短片，而在畢業時則拍出《》這部16mm的[黑白電影](../Page/黑白電影.md "wikilink")。

## 电影列表

|        |                                                                                                                                     |                                          |                                                                                                                                 |
| ------ | ----------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------- |
| **年份** | **中英文片名**                                                                                                                           | **德语片名**                                 | **备注**                                                                                                                          |
| 1970   | [夏日記遊](../Page/夏日記遊.md "wikilink")/*Summer in the City*                                                                             |                                          | 首部劇情長片                                                                                                                          |
| 1972   | *[守門員的焦慮](../Page/守門員的焦慮.md "wikilink")/The Goalkeeper's Fear of the Penalty* (UK)/*The Goalie's Anxiety at the Penalty Kick* (USA) | **                                       | 改編自[彼得·漢德克小說](../Page/彼得·漢德克.md "wikilink")                                                                                     |
| 1973   | *[紅字](../Page/紅字_\(電影\).md "wikilink")*                                                                                             | *Der Scharlachrote Buchstabe*            | 改編自[納撒尼爾·霍桑小說](../Page/納撒尼爾·霍桑.md "wikilink")[紅字](../Page/紅字.md "wikilink")                                                     |
| 1974   | [爱丽丝漫游城市](../Page/爱丽丝漫游城市.md "wikilink")/*Alice in the Cities*                                                                      | *Alice in den Städten*                   | 「公路片三部曲」的首部                                                                                                                     |
| 1975   | *[歧路](../Page/歧路.md "wikilink")*（The Wrong Move）                                                                                    | *Falsche Bewegung*                       | 「公路片三部曲」的第二部，与[娜妲莎·金斯基合作](../Page/娜妲莎·金斯基.md "wikilink")                                                                        |
| 1976   | *[公路之王](../Page/公路之王.md "wikilink")*（Kings of the Road）                                                                             | *Im Lauf der Zeit*                       | 「公路片三部曲」的第三部                                                                                                                    |
| 1977   | *[美國朋友](../Page/美國朋友.md "wikilink")*（The American Friend）                                                                           | *Der Amerikanische Freund*               | 改編自[派翠西亞·海史密斯小說](../Page/派翠西亞·海史密斯.md "wikilink")[雷普利遊戲](../Page/雷普利遊戲.md "wikilink")                                           |
| 1980   | *[水上回光](../Page/水上回光.md "wikilink")*（Lightning Over Water）                                                                          |                                          | [尼古拉斯·雷紀錄片](../Page/尼古拉斯·雷.md "wikilink")                                                                                       |
| 1982   | *[神探汉密特](../Page/神探汉密特.md "wikilink")*（Hammett）                                                                                     |                                          | 改編自[達許·漢密特小說](../Page/達許·漢密特.md "wikilink")                                                                                     |
| 1982   | *[Room 666](../Page/Room_666.md "wikilink")*                                                                                        | *Chambre 666*                            | [史蒂芬·史匹柏](../Page/史蒂芬·史匹柏.md "wikilink")、[尚盧·高達](../Page/尚盧·高達.md "wikilink")、[寧那·華納·法斯賓德紀錄片](../Page/寧那·華納·法斯賓德.md "wikilink") |
| 1982   | *[Reverse Angle](../Page/Reverse_Angle.md "wikilink")*                                                                              |                                          | 短篇紀錄片                                                                                                                           |
| 1982   | [事物的狀態](../Page/事物的狀態.md "wikilink")/*The State of Things*                                                                          | *Stand der Dinge*                        |                                                                                                                                 |
| 1984   | [巴黎，德州](../Page/巴黎，德州.md "wikilink")/*Paris, Texas*                                                                                 |                                          |                                                                                                                                 |
| 1985   | *[尋找小津](../Page/尋找小津.md "wikilink")*（Tokyo-Ga）                                                                                      |                                          | [小津安二郎紀錄片](../Page/小津安二郎.md "wikilink")                                                                                         |
| 1987   | [慾望之翼](../Page/柏林蒼穹下.md "wikilink")/*Wings of Desire* （又譯作「柏林蒼穹下」）                                                                  | *Der Himmel über Berlin*                 | 与[彼得·漢德克共同编剧](../Page/彼得·漢德克.md "wikilink")，1988年第41届[戛納電影節最佳導演獎](../Page/戛納電影節.md "wikilink")                                  |
| 1989   | *[城市時裝速記](../Page/城市時裝速記.md "wikilink")*（Notebook on Cities and Clothes）                                                            | *Aufzeichnungen zu Kleidern und Städten* | [山本耀司紀錄片](../Page/山本耀司.md "wikilink")                                                                                           |
| 1990   | *[Red Hot + Blue](../Page/Red_Hot_+_Blue.md "wikilink")*                                                                            |                                          | Music video for "Night and Day" performed by [U2](../Page/U2.md "wikilink")                                                     |
| 1991   | *[直到世界末日](../Page/直到世界末日.md "wikilink")*（Until the End of the World）                                                                | *Bis ans Ende der Welt*                  |                                                                                                                                 |
| 1992   | *[Arisha, the Bear and the Stone Ring](../Page/Arisha,_the_Bear_and_the_Stone_Ring.md "wikilink")*                                  | *Arisha, der Bär und der steinerne Ring* |                                                                                                                                 |
| 1993   | *[咫尺天涯](../Page/咫尺天涯.md "wikilink")*（Faraway, So Close\!）                                                                           | *In weiter Ferne, so nah\!*              | *[慾望之翼](../Page/慾望之翼.md "wikilink")*續集                                                                                          |
| 1994   | *[里斯本的故事](../Page/里斯本的故事.md "wikilink") /*Lisbon Story''                                                                            |                                          | *[事物的狀態](../Page/事物的狀態.md "wikilink")*續集                                                                                        |
| 1995   | [在雲端上的情與慾](../Page/在雲端上的情與慾.md "wikilink")（Beyond the Clouds）                                                                       | *Jenseits der Wolken*                    | (與[米開朗基羅·安東尼奧尼合作](../Page/米開朗基羅·安東尼奧尼.md "wikilink"))                                                                           |
| 1995   | *[光之幻影](../Page/光之幻影.md "wikilink")*（A Trick of Light）                                                                              | *Die Gebrüder Skladanowsky*              | *The Brothers Skladanowsky*                                                                                                     |
| 1997   | *[終結暴力](../Page/終結暴力.md "wikilink")*（The End of Violence）                                                                           |                                          |                                                                                                                                 |
| 1998   | *[Willie Nelson at the Teatro](../Page/Willie_Nelson_at_the_Teatro.md "wikilink")*                                                  |                                          |                                                                                                                                 |
| 1999   | *[樂士浮生錄](../Page/樂士浮生錄.md "wikilink") /*Buena Vista Social Club''                                                                   |                                          | [古巴伊布拉印飛列紀錄片](../Page/古巴.md "wikilink")                                                                                         |
| 2000   | [百万美元酒店](../Page/百万美元酒店.md "wikilink")/*The Million Dollar Hotel*                                                                   |                                          |                                                                                                                                 |
| 2001   | *[Souljacker Part 1](../Page/Souljacker_Part_1.md "wikilink")*                                                                      |                                          | Music Video for "Souljacker Pt 1" by Eels                                                                                       |
| 2002   | *[Ode to Cologne: A Rock 'N' Roll Film](../Page/Ode_to_Cologne:_A_Rock_'N'_Roll_Film.md "wikilink")*                                | *Viel passiert - Der BAP-Film*           | 紀錄片                                                                                                                             |
| 2002   | *[十分鐘前：小號響起](../Page/十分鐘前：小號響起.md "wikilink")*（Ten Minutes Older）                                                                   |                                          | "Twelve Miles to Trona"                                                                                                         |
| 2003   | *[布鲁斯之魂](../Page/布鲁斯之魂.md "wikilink")*（The Soul of a Man）                                                                           |                                          | 紀錄片                                                                                                                             |
| 2004   | [迷失天使城](../Page/迷失天使城.md "wikilink")（Land of Plenty）                                                                                |                                          |                                                                                                                                 |
| 2005   | *[別來敲門](../Page/別來敲門.md "wikilink")*（Don't Come Knocking）                                                                           |                                          |                                                                                                                                 |
| 2008   | *[巴勒摩獵影](../Page/巴勒摩獵影.md "wikilink")*（Palermo Shooting）                                                                            |                                          |                                                                                                                                 |
| 2011   | *[碧娜鮑許](../Page/碧娜鮑許\(電影\).md "wikilink") /Pina*                                                                                    |                                          | [第61屆柏林影展首映](../Page/第61屆柏林影展.md "wikilink")\[1\]                                                                               |
| 2014   | *[薩爾加多的凝視](../Page/薩爾加多的凝視.md "wikilink")*（The Salt of the Earth）                                                                   |                                          | 紀錄片                                                                                                                             |
| 2016   | 戀夏絮語 （Les beaux jours d'Aranjuez）                                                                                                   |                                          |                                                                                                                                 |
|        |                                                                                                                                     |                                          |                                                                                                                                 |

## 獎項

  - [威尼斯国际电影节](../Page/威尼斯国际电影节.md "wikilink")[金狮奖](../Page/金狮奖.md "wikilink")：The
    State of Things (1982)
  - [康城影展](../Page/康城影展.md "wikilink")[金棕榈奖](../Page/金棕榈奖.md "wikilink")：[巴黎，德州](../Page/巴黎，德州.md "wikilink")
    (1984)
  - [英国电影学院奖最佳导演](../Page/英国电影学院奖.md "wikilink")：[巴黎，德州](../Page/巴黎，德州.md "wikilink")
    (1984)
  - 第41届戛納電影節最佳導演獎：[柏林蒼穹下](../Page/柏林蒼穹下.md "wikilink")（1988年）
  - 康城影展评委会大奖：Faraway, So Close\! (1993)
  - [柏林国际电影节评委会银熊奖](../Page/柏林国际电影节.md "wikilink")：The Million Dollar
    Hotel (2000)
  - [第65届柏林影展荣誉](../Page/第65届柏林影展.md "wikilink")[金熊奖](../Page/金熊奖.md "wikilink")
    (2015)

## 名言

  - 「性（Sex）與暴力（violence）從来都不是我的菜；我都是更喜歡[薩克斯風](../Page/薩克斯風.md "wikilink")（sax）與[小提琴](../Page/小提琴.md "wikilink")（violins）。」
  - 「我把[小津當作是我一生中最重要的老師](../Page/小津安二郎.md "wikilink")。」
  - 「我是拍個人（personal）電影，不是私人（財產，private）電影。」（即反對商業[劇情片](../Page/劇情片.md "wikilink")）

## 参考资料

<references/>

## 外部連結

  - [Official website](http://www.wim-wenders.com/)

  -
  - [Angels and the Modern City: Essay on Wenders' film *Wings of
    Desire*](http://www.necessaryprose.com/wenders.html)

  - [Senses of Cinema: Great Directors Critical
    Database](http://www.sensesofcinema.com/2003/great-directors/wenders/)

  - \[<https://web.archive.org/web/20071109164957/http://www.filmportal.de/df/bb/Uebersicht,,,,,,,,EFC0CAA3F03B03C1E03053D50B372D46>,,,,,,,,,,,,,,,,,,,,,,,,,,,.html
    filmportal.de\] including biography, filmography and photos

  - [Wenders Bibliography (via UC
    Berkeley)](http://www.lib.berkeley.edu/MRC/Germanfilmbib.html#Wenders)

  - [Wim Wenders: Image and Cultural
    Influence](http://www.toxicuniverse.com/review.php?aid=1000404)
    ToxicUniverse.com article by Tony Pellum

  - [Interview with Wim Wenders (in
    German)](http://www.avinus-magazin.eu/html/wenders_interview.html)

  - [Naked Punch Review
    Interview](https://web.archive.org/web/20080104230817/http://www.nakedpunch.com/nakedpunch4.html)
    with Wim Wenders.

  - [Drinks with Tony
    interview](http://www.drinkswithtony.com/wimwenders.html) with Wim
    Wenders.

  - [Wim Wenders to be part of "Global Town Hall"
    summit](http://www.msnbc.msn.com/id/14216322/)

  - [CNN interview with Wim
    Wenders](http://edition.cnn.com/2006/TRAVEL/02/02/berlin.qa/)

  - [Informative German language website](http://www.wimwenders.de/)

  - [Wim Wenders Interview (portuguese
    subtitles)](http://www.youtube.com/watch?v=mFIHnl4rmd0)

  - [Wim Wenders's Cinématon - A 4 minutes portrait by Gérard Courant
    (1982)](http://www.gerardcourant.com/projection.php?t=c&film=212)

[W](../Category/德國導演.md "wikilink")
[Category:坎城影展獲獎者](../Category/坎城影展獲獎者.md "wikilink")
[Category:北萊因-西發里亞人](../Category/北萊因-西發里亞人.md "wikilink")
[W](../Category/慕尼黑電視與電影大學校友.md "wikilink")
[Category:英国电影学院奖最佳导演得主](../Category/英国电影学院奖最佳导演得主.md "wikilink")
[Category:欧洲电影奖最佳导演获得者](../Category/欧洲电影奖最佳导演获得者.md "wikilink")
[Category:威尼斯电影节评审团主席](../Category/威尼斯电影节评审团主席.md "wikilink")
[Category:柏林影展獲獎者](../Category/柏林影展獲獎者.md "wikilink")

1.