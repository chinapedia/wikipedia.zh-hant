**AM
0644-741**是[無棒透鏡星系和](../Page/無棒透鏡星系.md "wikilink")[環星系的組合](../Page/環星系.md "wikilink")，位於南天[飛魚座的方向](../Page/飛魚座.md "wikilink")，距離地球3億光年。淡黃的核曾經是一個正常的螺旋星系的中心，環繞著中心的環[直徑是](../Page/直徑.md "wikilink")150,000[光年](../Page/光年.md "wikilink")\[1\]。理論上，環是星系形成之後與另一個星系交互作用，觸發了[引力的擾動](../Page/引力.md "wikilink")，使灰塵凝結導致恆星的形成，並迫使它們離開星系成為環狀。這個環是年輕、大質量、高溫的藍色恆星猖獗形成的區域。沿著環的粉紅色區域是受到來自炙熱藍色恆星的強烈紫外線照射，發出螢光的稀薄氫氣體雲層。星系模擬的模型顯示AM
0644-741的環還會繼續擴大，而大約在3億年後就會開始瓦解\[2\]。

## 參考資料

[Category:環星系](../Category/環星系.md "wikilink")
[Category:無棒透鏡星系](../Category/無棒透鏡星系.md "wikilink")
[Category:飛魚座](../Category/飛魚座.md "wikilink")
[019481](../Category/PGC天體.md "wikilink")
[34-11](../Category/ESO_天體.md "wikilink")
[06443-7411](../Category/IRAS_目錄天體.md "wikilink")

1.
2.