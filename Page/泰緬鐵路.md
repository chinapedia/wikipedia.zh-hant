[Death_Railway.png](https://zh.wikipedia.org/wiki/File:Death_Railway.png "fig:Death_Railway.png")
**泰緬鐵路**（又稱**緬甸鐵路**或**死亡鐵路**）是[大日本帝國在](../Page/大日本帝國.md "wikilink")[第二次世界大戰期间為了占領](../Page/第二次世界大戰.md "wikilink")[英屬緬甸修建连结](../Page/英屬緬甸.md "wikilink")[泰國](../Page/泰國.md "wikilink")[曼谷和](../Page/曼谷.md "wikilink")[緬甸](../Page/緬甸.md "wikilink")[仰光的](../Page/仰光.md "wikilink")[鐵路](../Page/鐵路.md "wikilink")。死亡鐵路的名字来自建设时工人的死亡率。工程中總共募集了1萬2千名[日本皇軍及](../Page/日本皇軍.md "wikilink")[泰國軍隊](../Page/第二次世界大戰期間的泰國.md "wikilink")（第五[鐵道聯隊](../Page/鐵道聯隊.md "wikilink")）、[盟軍俘虜](../Page/同盟國.md "wikilink")6萬2千人（俘虏6,318来自[英国](../Page/英国.md "wikilink")，2,815自[澳洲](../Page/澳洲.md "wikilink")，2,490自[荷兰](../Page/荷兰.md "wikilink")，剩下大概来自[美国和其他国家](../Page/美国.md "wikilink")。\[1\]戰爭結束前1萬2千人死亡）、數萬[泰國人](../Page/泰國人.md "wikilink")、18萬[緬甸人](../Page/緬甸.md "wikilink")（4萬人死亡）、8萬[馬來亞人](../Page/馬來亞.md "wikilink")（4萬2千人死亡）、4萬5千印尼人進行施工。

## 歷史

[日本帝國在](../Page/日本帝國.md "wikilink")[太平洋戰爭初期泰國成為其盟友和佔領緬甸](../Page/太平洋戰爭.md "wikilink")，在无法安全使用[马六甲海峡之后](../Page/马六甲海峡.md "wikilink")，運往緬甸的日軍以及後續補給必須靠陸路維持；20世纪初，英国曾勘测一条通泰国和缅甸的铁路，因建设难度太高而放弃。日本在戰爭逼迫下採取了英國人的探勘路線，並趕在緬甸戰區稍告平定的1942年6月开工，工程在泰国和缅甸两边一起开工，泰國的工程起點為[北碧](../Page/北碧.md "wikilink")，緬甸的起點為[丹彪扎亞](../Page/丹彪扎亞.md "wikilink")（Thanbyuzayat），鐵路經由[三塔山口連結兩國](../Page/三塔山口.md "wikilink")。从铁轨的記號顯示泰緬鐵路的建材来自从爪哇、马来西亚等地拆毁的[英屬马来亚聯邦鐵路](../Page/英屬马来亚.md "wikilink")。开始时[盟军经常空袭路途](../Page/盟军.md "wikilink")，但是日军告知盟軍修路者多为同盟國战俘，盟军只好停止轰炸工程。

[POWs_Burma_Thai_RR.jpg](https://zh.wikipedia.org/wiki/File:POWs_Burma_Thai_RR.jpg "fig:POWs_Burma_Thai_RR.jpg")折磨的澳大利亚籍与荷兰籍战俘，1943年摄于泰国Tarsau。\]\]

原计划6年完工的鐵路在工人高死亡率的代價下17个月之后（1943年10月17日）完成全長415公里的鐵路。两边来的铁路在Konkuita连接。修路者的生活和工作条件低的不可想象，约25%的战俘因过度疲劳，营养不良，虐待或各种没人管的[传染病](../Page/传染病.md "wikilink")（如[霍乱](../Page/霍乱.md "wikilink")、[疟疾及](../Page/疟疾.md "wikilink")[痢疾](../Page/痢疾.md "wikilink")）丧生。亚洲工人死亡率更高，但是日军没有详细记录。竣工之后，大部分战俘被转到日本本土，剩下的维修者不但生活恶劣，还不时遭到盟军空袭。

## 桂河大橋

[Bridge_over_River_Kwai.jpg](https://zh.wikipedia.org/wiki/File:Bridge_over_River_Kwai.jpg "fig:Bridge_over_River_Kwai.jpg")\]\]
铁路上最著名的地点是，在[同名电影中描述](../Page/桂河大桥_\(影片\).md "wikilink")。1943年2月先修好一座木桥，同年6月建钢筋水泥桥。两座桥都在1945年4月2日炸毁，但是已经被破坏再修理多次。现在的桂河大桥桥墩在日本制造，送到泰国赔偿战债。

## 現況

二战之后，铁路几乎已经完全报废。泰国重建部分，铁轨加宽。1949年4月修复Kanchanaburi到Nong
Pladuk，1952年到Wampo，1957年7月到Nam
Tok，总共约50公里现在可以应用，缅甸部分没有修复。为修铁路炸开的山谷只能步行，現在有從緬甸來的非法入境者从这里进入泰国。1990年代一直提出计划重修整个铁路，但是一直没有進行。

## 紀念

泰国境内有多处纪念馆。桂河大桥有一个纪念碑，也有一个旧式火车。澳大利亚在[Hellfire
Pass](../Page/Hellfire_Pass.md "wikilink")（现在Nam
Tok车站北边）建立纪念堂，纪念死亡最多的山地点。北碧盟军战俘公墓位于北碧市北一公里，埋葬6,982名战俘，大多数是英国，荷兰和美国人。再靠郊外有澳大利亚战俘公墓，埋葬1,750人。

北碧市还有2003年3月开放的和。英国有为死亡铁路特定部分。

桂河大橋旁有一座《孤軍永垂，[中國遠征軍功高如天](../Page/中國遠征軍.md "wikilink")，中華英烈浩氣長存》白色紀念碑及外型是一個[中華民國國軍頭像](../Page/中華民國.md "wikilink")，戴了有青天白日徽的鋼盔的孤軍塚。

## 相關條目

  - [泰国铁路运输](../Page/泰国铁路运输.md "wikilink")

  -
## 注释

<references />

[category:緬甸鐵路](../Page/category:緬甸鐵路.md "wikilink")

[Category:泰緬關係](../Category/泰緬關係.md "wikilink")
[Category:緬甸日治時期](../Category/緬甸日治時期.md "wikilink")
[Category:日軍進駐泰國時期](../Category/日軍進駐泰國時期.md "wikilink")
[Category:日军在太平洋战争中的战争罪行](../Category/日军在太平洋战争中的战争罪行.md "wikilink")
[Category:泰國鐵路](../Category/泰國鐵路.md "wikilink")
[Category:太平洋战争相关建筑](../Category/太平洋战争相关建筑.md "wikilink")
[Category:日本战俘集中营](../Category/日本战俘集中营.md "wikilink")

1.  The Japanese Thrust - Australia in the War of 1939-1945, Lionel
    Wigmore, Australian War Memorial, Canberra, 1957. p588