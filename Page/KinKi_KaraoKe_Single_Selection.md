**KinKi KaraoKe Single
Selection**是[日本二人組合](../Page/日本.md "wikilink")[近畿小子的第](../Page/近畿小子.md "wikilink")1張[單曲精選](../Page/單曲.md "wikilink")[卡拉OK](../Page/卡拉OK.md "wikilink")[專輯](../Page/音樂專輯.md "wikilink")。於2000年7月19日由[傑尼斯娛樂唱片公司發行](../Page/傑尼斯娛樂.md "wikilink")。

## 解說

本專輯是對於[J-POP來說非常罕有](../Page/J-POP.md "wikilink")，全碟均以除掉歌聲的樂曲為主的[卡拉OK](../Page/卡拉OK.md "wikilink")[專輯](../Page/專輯.md "wikilink")。收錄曲目主要是本專輯發售前兩個月推出，擁有百萬銷量記錄的首張單曲精選專輯『[KinKi
Single
Selection](../Page/KinKi_Single_Selection.md "wikilink")』的收錄曲，撇除了KinKi
Kids沒有參與製作的純音樂新曲『Theme of KinKi Kids
'00』。專輯名稱中所有「K」字均為大草，即『**K**in**K**i
**K**arao**K**e Single Selection』。

由於收錄的歌曲全部都是從該單曲裡的“Instrumental”或者“Backing
Track”中抽出，並沒有任何為了製作這張專輯而另作新錄的曲目存在。近年除了演歌以外的流行音樂，基本都不會推出純音樂的卡拉OK專輯，所以本專輯在當時的音樂市場上尤為罕見。不過因為正如前述一樣，歌曲的卡拉OK版本早就收錄於各單曲之中，所以樂迷們對本專輯的需要性又不是太高。

封面照片使用了現成照片（其實是拍攝[夏之王者／除了你誰都不愛單曲內頁時的照片](../Page/夏之王者/除了你誰都不愛.md "wikilink")）。因為卡拉OK專輯的關係，售價比一般專輯來得便宜，連[消售稅在內定價](../Page/消售稅.md "wikilink")1,980[日圓](../Page/日圓.md "wikilink")。

本專輯雖然是一件非常商業性的商品，在Oricon銷量榜上仍有約19,000張的銷量，這是因為還有一些非常忠心的歌迷購入的結果。不過自此之後就再沒有後繼的卡拉OK專輯推出，即使KinKi
Kids所屬的唱片公司[傑尼斯娛樂的官方網站上亦把這張專輯除名](../Page/傑尼斯娛樂.md "wikilink")，沒有記載在唱片記錄之中。就連在2004年推出第2張單曲精選專輯『[KinKi
Single Selection
II](../Page/KinKi_Single_Selection_II.md "wikilink")』之際，也再沒有像這次一樣跟隨的卡拉OK專輯。

由於最初已經決定只會少量製作，所以現在仍在市場上供應的數量少之又少。雖然嚴格上並不算是已經停止生產的廢盤，不過從在官方網站上被刪除來看，已經接近事實上已停止生產的廢盤狀態。

由於本專輯在[Oricon銷量榜上最高記錄是第](../Page/Oricon.md "wikilink")28位，所以當然可以說是中斷了自『[A
album](../Page/A_album.md "wikilink")』開始所有專輯新上榜即奪取週間銷量榜第1位的記錄。不過由於本專輯在Oricon的銷量榜上已沒有實質名義入榜，所以並不計算在KinKi
Kids的專輯記錄之中。就是說雖然是KinKi Kids的專輯，但就被當作並非KinKi
Kids名義的作品。發售時雖然沒有實質名義，不過現在一樣被視為KinKi
Kids的專輯，與其他專輯並列於網站之中，可以檢索到相關的唱片資料。對於卡拉OK專輯能夠打入Oricon銷量榜，實在前所未聞的異例。

基於會把沒有歌聲的餘韻部份也一併除去，所以時間長度並非單純地是除去第1首純音樂的『KinKi Single
Selection』。尤其在『不要停止的PURE』一曲上就比原來版本短了9秒。不過又由於有幾首歌曲比原來版本長，所以結果時間長度又與除去第1首純音樂的『KinKi
Single Selection』差不多一樣。

## 收錄歌曲

1.  **玻璃少年**（****）
      - 作曲：[山下達郎](../Page/山下達郎.md "wikilink")
      - 編曲：山下達郎
2.  **被愛不如愛人**（****）
      - ※KinKi
        Kids二人參與演出的[日本電視台](../Page/日本電視台.md "wikilink")[連續劇](../Page/連續劇.md "wikilink")『我們的勇氣
        未滿都市』主題曲。
      - 作曲：[馬飼野康二](../Page/馬飼野康二.md "wikilink")
      - 編曲：[CHOKKAKU](../Page/CHOKKAKU.md "wikilink")
      - 和音編排：[岩田雅之](../Page/岩田雅之.md "wikilink")
3.  **雲霄飛車羅曼史**（**Jetcoaster Romance；**）
      - ※KinKi
        Kids參與演出的日本[全日空](../Page/全日本空輸.md "wikilink")[航空公司廣告](../Page/航空公司.md "wikilink")『'98
        Paradise [沖繩](../Page/沖繩.md "wikilink")』主題曲。
      - 作曲：山下達郎
      - 編曲：[船山基紀](../Page/船山基紀.md "wikilink")
4.  **擁抱全部**（****）
      - ※KinKi Kids參與演出的[富士電視台音樂節目](../Page/富士電視台.md "wikilink")『LOVE
        LOVE我愛你』主題曲。
      - 作曲：[吉田拓郎](../Page/吉田拓郎.md "wikilink")
      - 編曲：[武部聰志](../Page/武部聰志.md "wikilink")
5.  **青春時代**（****）
      - ※[堂本剛參與演出的](../Page/堂本剛.md "wikilink")[東京放送](../Page/東京放送.md "wikilink")[連續劇](../Page/連續劇.md "wikilink")『青之時代』主題曲。
      - 作曲：[canna](../Page/canna.md "wikilink")
      - 編曲：[新川　博](../Page/新川博.md "wikilink")
6.  **Happy Happy Greeting**
      - ※KinKi
        Kids參與演出的[Panasonic廣告](../Page/松下電器.md "wikilink")『數碼攝錄機』主題曲。
      - 作曲：山下達郎
      - 編曲：山下達郎
7.  **Cinderella Christmas**（****）
      - 作曲：[谷本　新](../Page/谷本新.md "wikilink")
      - 編曲：[長岡成貢](../Page/長岡成貢.md "wikilink")
8.  **不要停止的純真**（****，**PURE**）
      - ※堂本剛參與演出的[日本電視台](../Page/日本電視台.md "wikilink")[連續劇](../Page/連續劇.md "wikilink")『為了與你的未來～I'll
        be back～』主題曲。
      - 作曲：[筒美京平](../Page/筒美京平.md "wikilink")
      - 編曲：[WACKY KAKI](../Page/WACKY_KAKI.md "wikilink")
9.  **Flower**（****）
      - ※KinKi
        Kids參與演出的日本[全日空](../Page/全日本空輸.md "wikilink")[航空公司廣告](../Page/航空公司.md "wikilink")『'99
        Paradise [沖繩](../Page/沖繩.md "wikilink")』主題曲。
      - 作曲：[HΛL](../Page/HΛL.md "wikilink")、[音妃](../Page/音妃.md "wikilink")
      - 編曲：[船山基紀](../Page/船山基紀.md "wikilink")
      - 和音編排：[松下　誠](../Page/松下誠.md "wikilink")
10. **雨的旋律**（**Melody**）
      - 作曲：[坂井秀陽](../Page/坂井秀陽.md "wikilink")、[武藤敏史](../Page/武藤敏史.md "wikilink")
      - 編曲：[有賀啟雄](../Page/有賀啟雄.md "wikilink")
11. **to Heart**
      - ※堂本剛參與演出的[東京放送](../Page/東京放送.md "wikilink")[連續劇](../Page/連續劇.md "wikilink")『to
        Heart～至死不渝～』主題曲。
      - 作曲：[宮崎　步](../Page/宮崎步.md "wikilink")
      - 編曲：[CHOKKAKU](../Page/CHOKKAKU.md "wikilink")
12. **越來越喜歡　越來越愛**（****）
      - ※KinKi Kids參與演出的[富士電視台音樂節目](../Page/富士電視台.md "wikilink")『LOVE
        LOVE我愛你』第2代主題曲。
      - 作曲：[堂本光一](../Page/堂本光一.md "wikilink")
      - 編曲：[武部聰志](../Page/武部聰志.md "wikilink")

[category:卡拉OK](../Page/category:卡拉OK.md "wikilink")

[Category:近畿小子專輯](../Category/近畿小子專輯.md "wikilink")
[Category:2000年音樂專輯](../Category/2000年音樂專輯.md "wikilink")