[Ling_Heuk.JPG](https://zh.wikipedia.org/wiki/File:Ling_Heuk.JPG "fig:Ling_Heuk.JPG")

**林旭**（），字**暾谷**，[福建](../Page/福建.md "wikilink")[侯官](../Page/侯官.md "wikilink")（今[福州](../Page/福州.md "wikilink")）人。[清朝末年](../Page/清朝.md "wikilink")[维新派人士](../Page/维新.md "wikilink")。出身[举人](../Page/举人.md "wikilink")。是[康有為的弟子](../Page/康有為.md "wikilink")，[戊戌六君子之一](../Page/戊戌六君子.md "wikilink")。[梁启超在](../Page/梁启超.md "wikilink")《[戊戌政变记](../Page/戊戌政变记.md "wikilink")》中称其“其于诗词、散文皆天授，文如汉魏人；诗如宋人，波澜老成，奥深侬，流行京师，名动一时”。有《[晚翠轩集](../Page/晚翠轩集.md "wikilink")》遗世。

## 生平

[光绪十九年](../Page/光绪.md "wikilink")（1893年）癸巳恩科福建[鄉試](../Page/鄉試.md "wikilink")[解元](../Page/解元.md "wikilink")。光緒二十一年（1895年）曾任内阁中书。1898年3月倡立[闽学会](../Page/闽学会.md "wikilink")，与粤、蜀、浙、陕各学会相呼应，推动维新运动。又是[保国会倡始董事之一](../Page/保国会.md "wikilink")。同年9月5日，百日维新后期，授四品卿衔军机[章京](../Page/章京.md "wikilink")，此后十日间，上书言事甚多，不少上谕出其手笔。

[戊戌变法失败被捕](../Page/戊戌变法.md "wikilink")，与[谭嗣同等](../Page/谭嗣同.md "wikilink")（总称[戊戌六君子](../Page/戊戌六君子.md "wikilink")）同时被杀害于[菜市口](../Page/菜市口.md "wikilink")。著作有《晚翠轩诗集》。妻子是晚清名臣[沈葆桢子](../Page/沈葆桢.md "wikilink")[沈瑜慶之長女](../Page/沈瑜慶.md "wikilink")[沈鹊应](../Page/沈鹊应.md "wikilink")\[1\]，擅詩詞，在林旭就义后，服毒[自尽](../Page/自尽.md "wikilink")（一说抑郁以终）。

## 著作

林旭在獄中題有絕命詩，感懷身世如下：

青蒲飲泣：用汉[史丹典故](../Page/史丹.md "wikilink")。[竟宁元年](../Page/竟宁.md "wikilink")（前33年），[汉元帝病重](../Page/汉元帝.md "wikilink")，欲废[太子](../Page/汉成帝.md "wikilink")，改立[定陶王](../Page/劉康_\(定陶王\).md "wikilink")。史丹“直入卧内，顿首伏青蒲上，梯泣固谏”，“太子得不易”。一作“青袍飲泣”。

國事：一作“国士”。

千里草：指[东汉末年](../Page/东汉.md "wikilink")[董卓](../Page/董卓.md "wikilink")。当时有民谣“千里草，何青青，十日卜，不得生”，暗指董卓。

本初：本初是[东汉末年军阀](../Page/东汉.md "wikilink")[袁绍的字](../Page/袁绍.md "wikilink")，暗喻[袁世凱](../Page/袁世凱.md "wikilink")。

健者：汉末[董卓专政](../Page/董卓.md "wikilink")，董卓思废[汉少帝](../Page/刘辩.md "wikilink")，袁绍不同意，董卓说：“天下之事，岂不在我，我欲为之，谁敢不从？”，袁绍则回答道：“天下健者，岂唯董公”。

## 林旭墓

林旭死後，義僕朱德貴將其遺體運回福州，停柩於金雞山地藏寺。光緒二十六年，林旭的夫人沈鵲應以悲憤病歿。次年[沈瑜慶歸閩營生壙](../Page/沈瑜慶.md "wikilink")，並葬林旭夫婦於左側。林旭墓聯：千秋晚翠孤忠草；一卷崦樓絕命詞。

1965年，吳家瓊寫信報告，在北門外貓頭山上發現林旭墓，王鐵藩去他家了解情況\[2\]。

## 註釋

[Category:清朝福建鄉試解元](../Category/清朝福建鄉試解元.md "wikilink")
[Category:清朝內閣中書](../Category/清朝內閣中書.md "wikilink")
[Category:清朝被處決者](../Category/清朝被處決者.md "wikilink")
[Category:福州人](../Category/福州人.md "wikilink")
[X旭](../Category/林姓.md "wikilink")
[Category:光緒十九年癸巳恩科福建鄉試舉人](../Category/光緒十九年癸巳恩科福建鄉試舉人.md "wikilink")

1.  曾祖母為林則徐之女林普晴。福建舉子多姓林，《了凡四訓》說有一老母樂善好施，其后子弟興旺，有“無林不開榜”之說。
2.  《王鐵藩年譜》