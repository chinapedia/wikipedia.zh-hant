**C**，**c**是[拉丁字母中的第](../Page/拉丁字母.md "wikilink")3个[字母](../Page/字母.md "wikilink")。在[伊特鲁里亚语中](../Page/伊特鲁里亚语.md "wikilink")，[爆破辅音没有明显的发音](../Page/爆破辅音.md "wikilink")，所以他们用[希腊语中的](../Page/希腊语.md "wikilink")（Gamma）来书写他们的/k/。开始的时候，罗马人同时使用它来书写/k/和/g/，后来在它的右中部加了一横杠变成[G](../Page/G.md "wikilink")。可能在更早的时候，只有/g/，而用[K表示](../Page/K.md "wikilink")/k/。

一些学者表示，[闪族语的ג是](../Page/闪族语.md "wikilink")[骆驼的图形](../Page/骆驼.md "wikilink")。/k/在[拉丁语中发展成](../Page/拉丁语.md "wikilink")[上腭音和](../Page/上腭音.md "wikilink")[软腭音](../Page/软腭音.md "wikilink")[音位变体](../Page/音位变体.md "wikilink")，这可能是由于伊特鲁里亚语的影响。因此，今天的C有很多不同的音值：在[法语和](../Page/法语.md "wikilink")[西班牙语中为](../Page/西班牙语.md "wikilink")：和，在[意大利中的](../Page/意大利.md "wikilink")和（像英语中的CH）等等。

## 字母C的含意

## 字元編碼

| 字元編碼                              | [ASCII](../Page/ASCII.md "wikilink") | [Unicode](../Page/Unicode.md "wikilink") | [EBCDIC](../Page/EBCDIC.md "wikilink") | [摩斯電碼](../Page/摩斯電碼.md "wikilink") |
| --------------------------------- | ------------------------------------ | ---------------------------------------- | -------------------------------------- | ---------------------------------- |
| [大寫C](../Page/大寫字母.md "wikilink") | 67                                   | 0043                                     | 195                                    | `-·-·`                             |
| [小寫c](../Page/小寫字母.md "wikilink") | 99                                   | 0063                                     | 131                                    |                                    |

## 其他表示法

## 另見

  -
## 参看

### C的变体

  - [¢](../Page/¢.md "wikilink")（一分货币）
  - [©](../Page/©.md "wikilink")（版权）

### 其他字母中的相近字母

  - （[希腊字母Gamma](../Page/希腊字母.md "wikilink")）

[Category:拉丁字母](../Category/拉丁字母.md "wikilink")