**雷切爾·亨特**（****，），[紐西蘭](../Page/紐西蘭.md "wikilink")[奧克蘭出生](../Page/奧克蘭.md "wikilink")，她是一位[紐西蘭](../Page/紐西蘭.md "wikilink")[超模](../Page/超模.md "wikilink")，[演員及](../Page/演員.md "wikilink")[電視監製](../Page/電視監製.md "wikilink")，她是[英國](../Page/英國.md "wikilink")[真人秀我要當超模](../Page/真人秀.md "wikilink")（Make
Me A Supermodel）的主持人。

## 早年

在[紐西蘭](../Page/紐西蘭.md "wikilink")[奧克蘭出生的雷切爾](../Page/奧克蘭.md "wikilink")，十七歲就已經開始做[模特兒](../Page/模特兒.md "wikilink")。

## 模特兒生涯

## 外部連結

  - [1](https://web.archive.org/web/20080211100949/http://www.rachelhunter.com/home.asp)
    - 官方網站

## 參考資料

[Category:紐西蘭模特兒](../Category/紐西蘭模特兒.md "wikilink")
[Category:紐西蘭人](../Category/紐西蘭人.md "wikilink")