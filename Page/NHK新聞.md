**NHK新聞**（）指的是[日本放送協會的](../Page/日本放送協會.md "wikilink")[電視](../Page/電視.md "wikilink")、[廣播服務所提供的](../Page/廣播.md "wikilink")[新聞節目](../Page/新聞.md "wikilink")。現在的的標識是自1990年（平成2年）開始使用的。NHK在日本國內的54個放送局、放送支局與報導室時時刻刻進行采訪活動，其記者也多加入了[記者俱樂部](../Page/記者俱樂部.md "wikilink")。另外，在海外有位于[紐約的](../Page/紐約.md "wikilink")[美國總局](../Page/美國.md "wikilink")、位于[倫敦的](../Page/倫敦.md "wikilink")[歐洲總局和位于](../Page/歐洲.md "wikilink")[北京的](../Page/北京.md "wikilink")[中國總局](../Page/中國.md "wikilink")。在日本以外共有21個采訪據點。另外，其在日本各地共有400個以上的[機器人](../Page/機器人.md "wikilink")[照相機](../Page/照相機.md "wikilink")，統一由東京的新聞中心管理；當發生[地震時](../Page/地震.md "wikilink")，能提供最即時的畫面。

目前NHK新聞也可在[全日本空輸](../Page/全日本空輸.md "wikilink")（ANA）的班機上收看\[1\]。

## 現任主播

**男性**

  - [伊藤鑛二](../Page/伊藤鑛二.md "wikilink")
  - [明石勇](../Page/明石勇.md "wikilink")
  - [畠山智之](../Page/畠山智之.md "wikilink")
  - [阿部渉](../Page/阿部渉.md "wikilink")
  - [武田真一](../Page/武田真一.md "wikilink")
  - [野村正育](../Page/野村正育.md "wikilink")
  - [末田正雄](../Page/末田正雄.md "wikilink")
  - [伊藤博英](../Page/伊藤博英.md "wikilink")
  - [石澤典夫](../Page/石澤典夫.md "wikilink")

**女性**

  - [渡邊あゆみ](../Page/渡邊あゆみ.md "wikilink")
  - [加賀美幸子](../Page/加賀美幸子.md "wikilink")
  - [櫻井洋子](../Page/櫻井洋子.md "wikilink")
  - [山田敦子](../Page/山田敦子.md "wikilink")
  - [森田美由紀](../Page/森田美由紀.md "wikilink")
  - [小郷知子](../Page/小郷知子.md "wikilink")

## 前任主播

**男性**

  - （1959年）

  - [名取將](../Page/名取將.md "wikilink")（1962年）

  - [森本毅郎](../Page/森本毅郎.md "wikilink")（1963年）

  - [園田矢](../Page/園田矢.md "wikilink")（1965年）

  - [平野次郎](../Page/平野次郎.md "wikilink")（1965年）

  - [松平定知](../Page/松平定知.md "wikilink")（1967年）

  - [草野仁](../Page/草野仁.md "wikilink")（1967年）

  - [宮川泰夫](../Page/宮川泰夫.md "wikilink")（1968年）

  - [宮田修](../Page/宮田修.md "wikilink")（1970年）

  - [石戶谷健一](../Page/石戶谷健一.md "wikilink")（1971年）

  - [川端義明](../Page/川端義明.md "wikilink")（1975年）

  - 梶原四郎

**女性**

  - [村田幸子](../Page/村田幸子.md "wikilink")（1963年）
  - [山根基世](../Page/山根基世.md "wikilink")（1970年）
  - [介川裕子](../Page/介川裕子.md "wikilink")（1973年）
  - [頼近美津子](../Page/頼近美津子.md "wikilink")（1978年）
  - [池田裕子](../Page/池田裕子.md "wikilink")（1979年）
  - [草野満代](../Page/草野満代.md "wikilink")（1989年）

## 過去曾經擔任主播公眾人物

**男性**

<table style="width:10%;">
<colgroup>
<col style="width: 2%" />
<col style="width: 2%" />
<col style="width: 2%" />
<col style="width: 2%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li><a href="../Page/相田洋.md" title="wikilink">相田洋</a></li>
<li><a href="../Page/安住淳.md" title="wikilink">安住淳</a></li>
<li><a href="../Page/池上彰.md" title="wikilink">池上彰</a></li>
<li><a href="../Page/磯村尚德.md" title="wikilink">磯村尚德</a></li>
<li><a href="../Page/植木光教.md" title="wikilink">植木光教</a></li>
<li><a href="../Page/上杉隆.md" title="wikilink">上杉隆</a></li>
<li><a href="../Page/上田哲.md" title="wikilink">上田哲</a></li>
<li><a href="../Page/浦達也.md" title="wikilink">浦達也</a></li>
<li><a href="../Page/大久保怜.md" title="wikilink">大久保怜</a></li>
<li><a href="../Page/太田恒太郎.md" title="wikilink">太田恒太郎</a></li>
<li><a href="../Page/小野耕世.md" title="wikilink">小野耕世</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/角間隆.md" title="wikilink">角間隆</a></li>
<li><a href="../Page/柏倉康夫.md" title="wikilink">柏倉康夫</a></li>
<li><a href="../Page/勝部領樹.md" title="wikilink">勝部領樹</a></li>
<li><a href="../Page/苅田久徳.md" title="wikilink">苅田久徳</a></li>
<li><a href="../Page/川崎秀二.md" title="wikilink">川崎秀二</a></li>
<li><a href="../Page/川崎泰資.md" title="wikilink">川崎泰資</a></li>
<li><a href="../Page/木村淳.md" title="wikilink">木村淳</a></li>
<li><a href="../Page/木村太郎_(主播).md" title="wikilink">木村太郎</a></li>
<li><a href="../Page/草野仁.md" title="wikilink">草野仁</a></li>
<li><a href="../Page/胡桃澤耕史.md" title="wikilink">胡桃澤耕史</a></li>
<li><a href="../Page/小出五郎.md" title="wikilink">小出五郎</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/小中陽太郎.md" title="wikilink">小中陽太郎</a></li>
<li><a href="../Page/小林和男.md" title="wikilink">小林和男</a></li>
<li><a href="../Page/佐野剛平.md" title="wikilink">佐野剛平</a></li>
<li><a href="../Page/高島肇久.md" title="wikilink">高島肇久</a></li>
<li><a href="../Page/龍村仁.md" title="wikilink">龍村仁</a></li>
<li><a href="../Page/田畑彦右衛門.md" title="wikilink">田畑彦右衛門</a></li>
<li><a href="../Page/辻真先.md" title="wikilink">辻真先</a></li>
<li><a href="../Page/手嶋龍一.md" title="wikilink">手嶋龍一</a></li>
<li><a href="../Page/中島洋次郎.md" title="wikilink">中島洋次郎</a></li>
<li><a href="../Page/橋本大二郎.md" title="wikilink">橋本大二郎</a></li>
<li><a href="../Page/浜野崇好.md" title="wikilink">浜野崇好</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/林家竹丸.md" title="wikilink">林家竹丸</a></li>
<li><a href="../Page/平野次郎.md" title="wikilink">平野次郎</a></li>
<li><a href="../Page/日高義樹.md" title="wikilink">日高義樹</a></li>
<li><a href="../Page/深町幸男.md" title="wikilink">深町幸男</a></li>
<li><a href="../Page/藤田太寅.md" title="wikilink">藤田太寅</a></li>
<li><a href="../Page/水野清.md" title="wikilink">水野清</a></li>
<li><a href="../Page/柳田邦男.md" title="wikilink">柳田邦男</a></li>
<li><a href="../Page/和田勉.md" title="wikilink">和田勉</a></li>
<li><a href="../Page/柳川喜郎.md" title="wikilink">柳川喜郎</a></li>
</ul></td>
</tr>
</tbody>
</table>

**女性**

  - [畑恵](../Page/畑恵.md "wikilink")

## 製播節目

以下各節目名稱後面符號代表：※為雙語播出

  - [News Wide](../Page/News_Wide.md "wikilink")（）
  - [NHK MORNING WIDE](../Page/Morning_Wide.md "wikilink")（）
  - [早安日本](../Page/早安日本.md "wikilink")（）※
  - [NHK午間新聞](../Page/NHK午間新聞.md "wikilink")（）
  - [NHK1點新聞](../Page/NHK1點新聞.md "wikilink")（）
  - [NHK2點新聞](../Page/NHK2點新聞.md "wikilink")（）
  - [NHK3點新聞](../Page/NHK3點新聞.md "wikilink")（）
  - [NHK4點新聞](../Page/NHK4點新聞.md "wikilink")（）
  - [NHK5點新聞](../Page/NHK5點新聞.md "wikilink")（）
  - [NHK6點新聞](../Page/NHK6點新聞.md "wikilink")（）
  - [NHK晚間新聞](../Page/NHK晚間新聞.md "wikilink")（）※
  - [NHK7點新聞](../Page/NHK7點新聞.md "wikilink")（）※
  - [新聞中心9點](../Page/新聞中心9點.md "wikilink")（）
  - [NHK News Today](../Page/NHK_News_Today.md "wikilink")（）
  - [NHK21點新聞](../Page/NHK21點新聞.md "wikilink")（）
  - [NHK9點新聞](../Page/NHK9點新聞.md "wikilink")（）※
  - [News Watch 9](../Page/News_Watch_9.md "wikilink")（）※
  - [NHK10點新聞](../Page/NHK10點新聞.md "wikilink")（）
  - [今日新聞](../Page/今日新聞_\(NHK\).md "wikilink")（）
  - [今日的新聞和體育](../Page/今日的新聞和體育.md "wikilink")（）
  - [NHK Night Wide](../Page/NHK_Night_Wide.md "wikilink")（）
  - [Night News](../Page/Night_News.md "wikilink")（）
  - [MIDNIGHT JOURNAL](../Page/MIDNIGHT_JOURNAL.md "wikilink")（）
  - [Sport Time](../Page/Sport_Time.md "wikilink")（）
  - [NHK11點新聞](../Page/NHK11點新聞.md "wikilink")（）
  - [體育&新聞](../Page/體育&新聞.md "wikilink")（）
  - [今天的新聞&體育](../Page/今天的新聞&體育.md "wikilink")（）
  - [Business\&Sports](../Page/Business&Sports.md "wikilink")（）
  - [NHK24點新聞](../Page/NHK24點新聞.md "wikilink")（）
  - [NHK新聞解說](../Page/NHK新聞解說.md "wikilink")（）
  - [視點 論點](../Page/視點_論點.md "wikilink")（）
  - [明日閱讀](../Page/明日閱讀.md "wikilink")（）
  - [時事公論](../Page/時事公論.md "wikilink")（）
  - [NHK NEWSLINE](../Page/NHK_NEWSLINE.md "wikilink")（）
  - [NHK雜誌](../Page/NHK雜誌.md "wikilink")（）
  - [NHK新聞週刊](../Page/NHK新聞週刊.md "wikilink")（）
  - [NHK手語新聞](../Page/NHK手語新聞.md "wikilink")（）
  - [NHK電台晚報](../Page/NHK電台晚報.md "wikilink")（）
  - [元氣日本列島](../Page/元氣日本列島.md "wikilink")（）
  - [早安世界](../Page/早安世界.md "wikilink")（）
  - [早安五點](../Page/早安五點.md "wikilink")（）
  - [NHK World Network](../Page/NHK_World_Network.md "wikilink")（）
  - [今日世界](../Page/今日世界.md "wikilink")（）
  - [經濟最前線](../Page/經濟最前線.md "wikilink")（）
  - [NHK經濟雜誌](../Page/NHK經濟雜誌.md "wikilink")（）
  - [Business Wide Vision
    e](../Page/Business_Wide_Vision_e.md "wikilink")（）
  - [兒童新聞週刊](../Page/兒童新聞週刊.md "wikilink")（）
  - [細說新聞週刊](../Page/細說新聞週刊.md "wikilink")（）
  - [NHK手語新聞845](../Page/NHK手語新聞845.md "wikilink")（）
  - [現代觀察](../Page/現代觀察.md "wikilink")（）

## 參考

<references/>

## 外部連結

  - [NHK新聞](http://www.nhk.or.jp/news/)
  - [NHK廣播新聞](http://www.nhk.or.jp/r-news)

[NHK新聞](../Category/NHK新聞.md "wikilink")

1.  [ANA](http://www.ana.co.jp/dom/inflight/audio/pdf/skychannel_1004.pdf)