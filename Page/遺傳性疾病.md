**遺傳性疾病**是指以基因為主要致病原因的疾病。依據成因又可以細分成：單一基因缺陷的遺傳疾病、染色體變異所引起的遺傳疾病及由多重基因共同影響所造成的遺傳疾病及[粒線體基因變異所引起的疾病](../Page/粒線體.md "wikilink")。其中因單一基因缺陷而引起的遺傳疾病又稱為[孟德爾型病症](../Page/孟德爾型病症.md "wikilink")。臨床上大多透過[遺傳基因檢測來輔助診斷以及帶因篩檢](../Page/遺傳基因檢測.md "wikilink")。

## 分类

### 單一基因缺陷造成的遺傳疾病

  - 體染色體隱性遺傳疾病 (autosomal recessive inheritable disease
    ,又可稱為自體隱性遺傳疾病、常染色體隱性遺傳疾病)
      - [囊胞性纖維症](../Page/囊胞性纖維症.md "wikilink")
      - [苯丙酮尿症](../Page/苯丙酮尿症.md "wikilink")
      - [鎌刀型貧血症](../Page/鎌刀型貧血症.md "wikilink")
      - [地中海貧血](../Page/地中海貧血症.md "wikilink")
      - [白化症](../Page/白化症.md "wikilink")
      - [甲一型抗胰蛋白酶缺乏症](../Page/甲一型抗胰蛋白酶缺乏症.md "wikilink")
      - [線嘌呤去胺酶缺乏症](../Page/線嘌呤去胺酶缺乏症.md "wikilink")

<!-- end list -->

  - 體染色體顯性遺傳疾病 (autosomal dominant inheritable disease
    ,又可稱為自體顯性遺傳疾病、常染色體顯性遺傳疾病）
      - [家族性高膽固醇症](../Page/家族性高膽固醇症.md "wikilink")
      - [亨丁頓舞蹈症](../Page/亨丁頓舞蹈症.md "wikilink")
      - [軟骨發育不全症](../Page/軟骨發育不全症.md "wikilink")
      - [多指畸形](../Page/多指畸形.md "wikilink")

<!-- end list -->

  - X染色體聯鎖隱性遺傳疾病
      - [葡萄糖六磷酸鹽脫氫酶缺乏症](../Page/葡萄糖六磷酸鹽脫氫酶缺乏症.md "wikilink")（蠶豆症）
      - [裘馨氏肌肉萎縮症](../Page/裘馨氏肌肉萎縮症.md "wikilink")
      - [血友病](../Page/血友病.md "wikilink")（A及B型）
      - [戴薩克斯症](../Page/戴薩克斯症.md "wikilink")
      - [色盲](../Page/色盲.md "wikilink")
      - [X染色體易裂症](../Page/X染色體易裂症.md "wikilink")

<!-- end list -->

  - X染色體聯鎖顯性遺傳疾病
      - [低磷酸鹽性佝僂症](../Page/低磷酸鹽性佝僂症.md "wikilink")（[維他命D阻礙性佝僂症](../Page/維他命D阻礙性佝僂症.md "wikilink")）

### 染色體異常所引起的遺傳疾病

  - [唐氏症](../Page/唐氏症.md "wikilink")
  - [克林菲爾德症候群](../Page/克林菲爾德症候群.md "wikilink")
  - [透納氏症候群](../Page/透納氏症候群.md "wikilink")
  - [Léri-Weill軟骨骨生成障礙綜合症](../Page/Léri-Weill軟骨骨生成障礙綜合症.md "wikilink")

### 多重基因共同影響所造成的遺傳疾病

  -
    冠狀心臟疾病、高血壓、中風及許多種類的癌症。有關這方面的研究目前仍在進行中。

### [粒線體基因變異所引起的遺傳疾病](../Page/粒線體.md "wikilink")

有數種遺傳性疾病是因為粒線體DNA發生突變所導致，由於受精時只有來自卵子的粒線體會留給胚胎，故此線粒體DNA突變的疾病是母系遺傳。如[賴博氏遺傳性視覺神經症](../Page/賴博氏遺傳性視覺神經症.md "wikilink")（Leber
hereditary optic
neuroatrophy）這種視覺神經疾病，就與此類基因突變有關。但即使帶有具這種突變基因的粒線體，超過50%的男性和85%的女性都不會發病\[1\]。[凱恩斯-沙耶症候群](../Page/凱恩斯-沙耶症候群.md "wikilink")（Kearns-Sayre
syndrome）則主要源於粒線體DNA中的基因缺失而導致粒線體功能下降，影響[細胞呼吸而引起](../Page/細胞呼吸.md "wikilink")，從而影響器官功能，對[肌肉](../Page/肌肉.md "wikilink")、[眼睛及](../Page/眼睛.md "wikilink")[心臟影響尤甚](../Page/心臟.md "wikilink")。不過粒線體基因突變的原因以及帶有突變基因者的發病率仍然在研究中\[2\]。

## 怀疑与遗传有关或与遗传有关但机理不详的疾病

## 检查和预防

1.  做好[產前檢查](../Page/產前檢查.md "wikilink")。
2.  防止[近親結婚](../Page/近親結婚.md "wikilink")。
3.  適齡生育。
4.  有這類基因的夫婦，可採用[體外受精](../Page/體外受精.md "wikilink")，並在受精卵階段就檢查DNA，只使用健康的胚胎，降低後代罹病的風險。

## 参考文献

## 外部链接

  - [人類的遺傳疾病](https://web.archive.org/web/20050408232249/http://science.scu.edu.tw/micro/1024/learn/01class_gene/human_dis.htm)
  - [行政院衛生署國民健康局遺傳疾病諮詢服務窗口](https://web.archive.org/web/20131230012128/http://gene.bhp.doh.gov.tw/)
  - [遺傳基因檢測諮詢服務窗口](https://www.drgene.com)

## 参见

  - [基因](../Page/基因.md "wikilink")
      - [基因檢測](../Page/基因檢測.md "wikilink")
  - [遺傳](../Page/遺傳.md "wikilink")
      - [遺傳因子](../Page/遺傳因子.md "wikilink")
  - [传染](../Page/传染.md "wikilink")
      - [传染性疾病](../Page/传染性疾病.md "wikilink")

{{-}}

[遗传病](../Category/遗传病.md "wikilink")
[Category:遗传学](../Category/遗传学.md "wikilink")

1.  [Leber hereditary optic
    neuropathy](http://ghr.nlm.nih.gov/condition/leber-hereditary-optic-neuropathy)Gene
    Home Reference
2.  [Kearns-Sayre
    syndrome](http://ghr.nlm.nih.gov/condition/kearns-sayre-syndrome)Gene
    Home Reference