**伊巴丹**（[英语](../Page/英语.md "wikilink")：****），[尼日利亚](../Page/尼日利亚.md "wikilink")[奥约州首府](../Page/奥约州.md "wikilink")。位于尼日利亚西南部，是全国第二大城市，2006年人口2,550,593。始建于18世纪末。是奥约州重要的[工商业城镇和](../Page/工商业.md "wikilink")[交通中心](../Page/交通.md "wikilink")。以生产精致的手工纺织品和金属工具而闻名。

## 參考文獻

[Category:尼日利亚城市](../Category/尼日利亚城市.md "wikilink")