**聖公會港澳教區**是1951年至1998年間，[香港和](../Page/香港.md "wikilink")[澳門的](../Page/澳門.md "wikilink")[聖公宗](../Page/聖公宗.md "wikilink")[教會](../Page/教會.md "wikilink")。是香港的其中一個主要的基督教會。港澳教區前身是[中華聖公會華南（前稱港粵）教區](../Page/中華聖公會港粵教區.md "wikilink")，1998年解散，由[香港聖公會](../Page/香港聖公會.md "wikilink")[教省取代](../Page/教省.md "wikilink")。

## 歷史

  - 1951年5月17日：[中華人民共和國在](../Page/中華人民共和國.md "wikilink")[中國大陸成立後](../Page/中國大陸.md "wikilink")，第二十屇[聖公會維多利亞教區](../Page/聖公會維多利亞教區.md "wikilink")/[中華聖公會華南教區議會決議港澳教會脫離](../Page/中華聖公會華南教區.md "wikilink")[中華聖公會港粵教區](../Page/中華聖公會港粵教區.md "wikilink")，香港及澳門之聖堂脫離[中華聖公會](../Page/中華聖公會.md "wikilink")[教省](../Page/教省.md "wikilink")，成立「聖公會港澳教區」，沒有隸屬於任何教省，由[坎特伯里大主教作為教區的轄治權人](../Page/坎特伯里大主教.md "wikilink")。後來雖然中華聖公會已不復存在，但港澳教區依然保持中華聖公會的信仰及禮儀，及中華聖公會1948年的總議會憲綱和憲法則例，港澳教區聖品受封職的誓書內，亦一直保持願遵守中華聖公會的教義，教政及禮儀的宣誓字句。
  - 1962年：東南亞各教區組成「[東南亞教區議會](../Page/聖公會東亞議會.md "wikilink")」(後稱東亞議會)，港澳教區為成員之一。
  - 1965年：因[坎特伯里大主教認為東南亞教區議會將發展為一獨立的教省](../Page/坎特伯里大主教.md "wikilink")，故宣佈放棄對港澳教區的轄治權。然而後來東亞議會並沒有成為一教省，而只作為中華聖公會憲章規條信託人。自此港澳教區成為一游離的個體。
  - 1981年：選出[鄺廣傑](../Page/鄺廣傑.md "wikilink")[牧師為港澳教區](../Page/牧師.md "wikilink")[主教](../Page/主教.md "wikilink")，並為港澳教區首位華人主教。
  - 1991年：由於港澳教區工作隨著社會發展而日益增多，加上長期以來沿用中華聖公會的憲章和規例因中華聖公會的不存在而無法更新，因此港澳教區決定籌備成立教省。計劃中的教省下轄[香港島教區](../Page/香港聖公會香港島教區.md "wikilink")、[東九龍教區](../Page/香港聖公會東九龍教區.md "wikilink")、[西九龍教區及](../Page/香港聖公會西九龍教區.md "wikilink")[澳門傳道地區](../Page/香港聖公會澳門傳道地區.md "wikilink")。
  - 1995年：選出並祝聖兩分區主教，[徐贊生為東九龍分區主教](../Page/徐贊生.md "wikilink")，[蘇以葆為西九龍分區主教](../Page/蘇以葆.md "wikilink")
  - 1998年：選出[鄺廣傑](../Page/鄺廣傑.md "wikilink")[主教為香港聖公會教省首任](../Page/主教.md "wikilink")[大主教及](../Page/大主教.md "wikilink")[教省](../Page/教省.md "wikilink")[主教長](../Page/主教長.md "wikilink")，並於十月二十五日舉行教省成立崇拜及大主教陞座禮。港澳教區時代結束。

## 聖公會港澳教區歷任會督/主教

  - [何明華](../Page/何明華.md "wikilink")[會督](../Page/會督.md "wikilink")(1951年至1967年)
  - [白約翰](../Page/白約翰.md "wikilink")[會督](../Page/會督.md "wikilink")(1967年至1981年)
  - [鄺廣傑](../Page/鄺廣傑.md "wikilink")[主教](../Page/主教.md "wikilink")(1981年至1998年)

## 参考文献

## 参见

  - [香港聖公會](../Page/香港聖公會.md "wikilink")
  - [普世聖公宗](../Page/普世聖公宗.md "wikilink")
  - [坎特伯里大主教](../Page/坎特伯里大主教.md "wikilink")
  - [中華聖公會](../Page/中華聖公會.md "wikilink")
  - [聖公會東亞議會](../Page/聖公會東亞議會.md "wikilink")
  - [黑皮公禱書](../Page/黑皮公禱書.md "wikilink")
  - [聖餐崇拜禮文 (第二式)](../Page/聖餐崇拜禮文_\(第二式\).md "wikilink")
  - [聖約翰座堂](../Page/聖約翰座堂.md "wikilink")

{{-}}

[Category:中華聖公會](../Category/中華聖公會.md "wikilink")
[Category:香港聖公會](../Category/香港聖公會.md "wikilink")
[Category:香港基督教新教组织](../Category/香港基督教新教组织.md "wikilink")
[Category:澳门基督教新教组织](../Category/澳门基督教新教组织.md "wikilink")