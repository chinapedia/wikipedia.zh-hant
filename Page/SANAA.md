[Koga_Park_Café.jpg](https://zh.wikipedia.org/wiki/File:Koga_Park_Café.jpg "fig:Koga_Park_Café.jpg")
[Kanazawa_21st_Century_Museum.jpg](https://zh.wikipedia.org/wiki/File:Kanazawa_21st_Century_Museum.jpg "fig:Kanazawa_21st_Century_Museum.jpg")
[Zollverein_School_of_Management_and_Design_3116754.jpg](https://zh.wikipedia.org/wiki/File:Zollverein_School_of_Management_and_Design_3116754.jpg "fig:Zollverein_School_of_Management_and_Design_3116754.jpg")
**SANAA**（，）是由日本[建築師](../Page/建築師.md "wikilink")[妹島和世和](../Page/妹島和世.md "wikilink")[西澤立衛兩人共同成立的](../Page/西澤立衛.md "wikilink")[建築事務所](../Page/建築事務所.md "wikilink")。2010年，SANAA獲得建築界最高榮譽[普利兹克奖](../Page/普利兹克奖.md "wikilink")。

## 主要作品

建築

  - [熊野古道博物館](../Page/熊野古道博物館.md "wikilink")　<small>（1996年）</small>
  - [岐阜縣立國際情報科學藝術學院](../Page/岐阜縣立國際情報科學藝術學院.md "wikilink")
    多媒體工作室　<small>（1996年／
    第50回[日本建築學會獎作品獎](../Page/日本建築學會獎.md "wikilink")）</small>
  - K本社屋<small>（茨城縣日立市／1997年）</small>
  - 飯田市小笠原資料館　<small>（長野縣飯田市／1999年）</small>
  - [古河綜合公園飲食設施](../Page/古河綜合公園.md "wikilink")　<small>（茨城縣古河市／1998年）</small>
  - 橫濱市六之川地方關懷之家　<small>（橫濱市南區／2000年）</small>
  - [三宅一生專賣店](../Page/三宅一生.md "wikilink")　<small>（東京都港區／2003年）</small>
  - Dior表参道　<small>（東京都澁谷區／2003年）</small>
  - [金澤21世紀美術館](../Page/金澤21世紀美術館.md "wikilink")　<small>（2004年／得到[威尼斯雙年展第](../Page/威尼斯雙年展.md "wikilink")9回國際建築展最高獎・金獅子獎）</small>
  - 托雷多美術館玻璃藝術中心<small>（美國／2006年）</small>
  - 礦業同盟設計管理學院<small>（德國／2006年）</small>
  - 諾華園區 WSJ-158<small>（瑞士／2006年）</small>
  - [直島渡輪碼頭](../Page/直島渡輪碼頭.md "wikilink")<small>（香川縣[直島町](../Page/直島町.md "wikilink")／2006年）</small>
  - [新當代藝術博物館](../Page/新當代藝術博物館.md "wikilink")<small>（美國／紐約／2007年）</small>
  - [中国国际建筑艺术实践展展馆](../Page/中国国际建筑艺术实践展展馆.md "wikilink")<small>（南京／中国／2007年）</small>
  - 洛桑联邦理工学院[劳力士学习中心](../Page/劳力士学习中心.md "wikilink") <small>
    （[洛桑](../Page/洛桑.md "wikilink")／瑞士／2010 年） </small>

裝置藝術

  - 空<small>（香川縣[直島町](../Page/直島町.md "wikilink")／2006年）</small>

產品設計

  - SANAA CHAIR

<!-- end list -->

  -
    由妹島所設計，為迎接hhstyle.com3週年紀念而製作的椅子，有鍍鉻和鍍鋅熔接兩種。很快地，鍍鉻的那個會在椅墊下打上「SANAA
    CHAIR KAZUYO SEJIMA+RYUE NISHIZAWA」的刻印和產品編號。（由hhstyle.com發售）

<!-- end list -->

  - nextmaruni project chair No.2946-23

<!-- end list -->

  -
    廣島MARUNI木工新企畫「nextmaruni」其中的一個。椅背分開成兩岔，讓人聯想到兔子的耳朵。於2005年米蘭設計週和其他設計師的椅子共同發表。

## 相關連結

  - [SANAA建築設計事務所網站](http://www.sanaa.co.jp)
  - [nextmaruni project](http://www.nextmaruni.com)

[Category:日本建築師](../Category/日本建築師.md "wikilink")
[Category:建築師事務所](../Category/建築師事務所.md "wikilink")
[Category:罗尔夫·朔克奖获得者](../Category/罗尔夫·朔克奖获得者.md "wikilink")