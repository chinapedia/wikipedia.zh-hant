**中共中央办公厅国务院办公厅信访局**（简称**中办国办信访局**）是1986年至2000年期间[中国共产党中央委员会和](../Page/中国共产党中央委员会.md "wikilink")[中华人民共和国国务院的](../Page/中华人民共和国国务院.md "wikilink")[信访工作机构](../Page/:分类:中央信访工作机构.md "wikilink")，正司局级单位，由[中华人民共和国国务院办公厅直接领导](../Page/中华人民共和国国务院办公厅.md "wikilink")。2000年升格为副部级的**[国家信访局](../Page/国家信访局.md "wikilink")**。

## 职责权限

**中办国办信访局**主要按照[中共中央办公厅和](../Page/中共中央办公厅.md "wikilink")[国务院办公厅确定的工作范围](../Page/国务院办公厅.md "wikilink")，负责办理以下各项工作：

  - 处理群众来信、接待群众来访
  - 向[党中央](../Page/中国共产党中央委员会.md "wikilink")、[国务院领导提供群众来信来访中反映的重要信息](../Page/中华人民共和国国务院.md "wikilink")
  - 办理[党中央](../Page/中国共产党中央委员会.md "wikilink")、[国务院领导交办的有关](../Page/中华人民共和国国务院.md "wikilink")[信访事项](../Page/信访.md "wikilink")
  - 指导部门和地方的信访业务工作
  - 处理跨部门、跨地区的信访疑难问题
  - 其他相关工作

## 参考文献

## 参见

  - [中共中央办公厅](../Page/中共中央办公厅.md "wikilink")、[国务院办公厅](../Page/国务院办公厅.md "wikilink")
  - [信访](../Page/信访.md "wikilink")：[国家信访局](../Page/国家信访局.md "wikilink")

{{-}}

[-](../Category/国家信访局.md "wikilink")
[-](../Category/中央信访工作机构.md "wikilink")
[Category:中共中央办公厅下属机构](../Category/中共中央办公厅下属机构.md "wikilink")
[Category:中华人民共和国国务院办公厅](../Category/中华人民共和国国务院办公厅.md "wikilink")
[Category:正司局级单位](../Category/正司局级单位.md "wikilink")
[Category:党政合一机构](../Category/党政合一机构.md "wikilink")