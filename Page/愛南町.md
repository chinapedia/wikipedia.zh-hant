**愛南町**（）是位於[愛媛縣西南部的](../Page/愛媛縣.md "wikilink")[町](../Page/町.md "wikilink")。

轄區內多為山地，在海岸部分為[溺灣地形](../Page/溺灣.md "wikilink")，河流出海口附近略有平地。產業以[珍珠養殖](../Page/珍珠.md "wikilink")、[柑橘為主](../Page/柑橘.md "wikilink")。

由於位於愛媛縣最南端，因此以「愛南」為名。\[1\]

## 歷史

### 年表

  - 1889年12月15日：實施[町村制](../Page/町村制.md "wikilink")，現在的轄區在當時分屬[南宇和郡御莊村](../Page/南宇和郡.md "wikilink")、城邊村、東外海村、[內海村](../Page/內海村.md "wikilink")、綠僧都村、西外海村、一本松村。
  - 1923年2月11日：
      - 御莊村改制為[御莊町](../Page/御莊町.md "wikilink")。
      - 城邊村改制為[城邊町](../Page/城邊町_\(愛媛縣\).md "wikilink")。
  - 1948年11月3日：[南內海村從內海村分出](../Page/南內海村.md "wikilink")，同時內海村的部份地區被併入御莊町。
  - 1952年4月1日：東外海村改制為[東外海町](../Page/東外海町.md "wikilink")。
  - 1952年9月1日：綠僧都村和城邊町[合併為新設置的](../Page/市町村合併.md "wikilink")[城邊町](../Page/城邊町_\(愛媛縣\).md "wikilink")。
  - 1952年10月1日：西外海村改制並改名為[西海町](../Page/西海町_\(愛媛縣\).md "wikilink")。
  - 1956年9月21日：城邊町和東外海町合併為新設置的城邊町。
  - 1956年9月30日：南內海村和御莊町合併為新設置的御莊町。
  - 1962年1月1日：一本松村改制為[一本松町](../Page/一本松町.md "wikilink")。
  - 2004年10月1日：御莊町、內海村、城邊町、一本松町、西海町合併為**愛南町**。\[2\]

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1889年4月1日</p></th>
<th><p>1889年 - 1926年</p></th>
<th><p>1926年 - 1954年</p></th>
<th><p>1955年 - 1989年</p></th>
<th><p>1989年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>御莊村</p></td>
<td><p>1923年2月11日<br />
御莊町</p></td>
<td><p>1956年9月30日<br />
合併為御莊町</p></td>
<td><p>2004年10月1日<br />
合併為愛南町</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>內海村</p></td>
<td><p>1948年11月3日<br />
南內海村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>內海村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>城邊村</p></td>
<td><p>1923年2月11日<br />
城邊町</p></td>
<td><p>1952年9月1日<br />
合併為城邊町</p></td>
<td><p>1956年9月21日<br />
合併為城邊町</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>綠僧都村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>東外海村</p></td>
<td><p>1952年4月1日<br />
東外海町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>西外海村</p></td>
<td><p>1952年10月1日<br />
改制並改名為西海町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>一本松村</p></td>
<td><p>1962年1月1日<br />
一本松町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 行政

### 歷任首長

1.  谷口長治（前城邊町町長）
2.  清水雅文（2008年10月-現任）

## 交通

轄區內無鐵路通過，距離最近的鐵路車站為位於[宇和島市的](../Page/宇和島市.md "wikilink")[宇和島車站和位於](../Page/宇和島車站.md "wikilink")[高知縣](../Page/高知縣.md "wikilink")[宿毛市的](../Page/宿毛市.md "wikilink")[宿毛車站](../Page/宿毛車站.md "wikilink")。

在過去曾規劃興建[宿毛線並通過轄區](../Page/宿毛線.md "wikilink")，不過最終僅完成位於[中村車站至宿毛車站之間的路段](../Page/中村車站.md "wikilink")（現在的[土佐黑潮鐵道](../Page/土佐黑潮鐵道.md "wikilink")[宿毛線](../Page/宿毛線.md "wikilink")），路線並未延伸到轄區內。

## 觀光資源

[Koumo1.JPG](https://zh.wikipedia.org/wiki/File:Koumo1.JPG "fig:Koumo1.JPG")
[Kanjizaiji_04.JPG](https://zh.wikipedia.org/wiki/File:Kanjizaiji_04.JPG "fig:Kanjizaiji_04.JPG")
[Funakoshi_Canal_01.JPG](https://zh.wikipedia.org/wiki/File:Funakoshi_Canal_01.JPG "fig:Funakoshi_Canal_01.JPG")

  - [高茂岬](../Page/高茂岬.md "wikilink")
  - [觀自在寺](../Page/觀自在寺.md "wikilink")
  - [船越運河](../Page/船越運河.md "wikilink")

## 教育

[MinamiuwaHS.jpg](https://zh.wikipedia.org/wiki/File:MinamiuwaHS.jpg "fig:MinamiuwaHS.jpg")

  - 大學

<!-- end list -->

  - [愛媛大學南予水産研究中心](../Page/愛媛大學.md "wikilink")

<!-- end list -->

  - 高等學校

<!-- end list -->

  - [愛媛縣立南宇和高等學校](../Page/愛媛縣立南宇和高等學校.md "wikilink")

## 本地出身之名人

  - [岡本吉起](../Page/岡本吉起.md "wikilink")：[遊戲](../Page/遊戲.md "wikilink")[製作人](../Page/製作人.md "wikilink")
  - [岡本美登](../Page/岡本美登.md "wikilink")：演員
  - [夏井一季](../Page/夏井一季.md "wikilink")：俳人
  - [羽田敬介](../Page/羽田敬介.md "wikilink")：[職業足球選手](../Page/職業足球.md "wikilink")
  - [山本智子](../Page/山本智子.md "wikilink")：歌手

## 參考資料

## 外部連結

  - [愛南町觀光協會](http://www.ainan-marugoto.jp/)

  - [愛南町教育委員會](http://ainan-t.esnet.ed.jp/)

<!-- end list -->

1.

2.