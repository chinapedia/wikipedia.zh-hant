[Cro-Magnon-male-skull.png](https://zh.wikipedia.org/wiki/File:Cro-Magnon-male-skull.png "fig:Cro-Magnon-male-skull.png")。\]\]
[Cro-Magnon.jpg](https://zh.wikipedia.org/wiki/File:Cro-Magnon.jpg "fig:Cro-Magnon.jpg")
[Silex_cromagnon_noir.jpg](https://zh.wikipedia.org/wiki/File:Silex_cromagnon_noir.jpg "fig:Silex_cromagnon_noir.jpg")

**克羅馬儂人**（，又譯**克洛曼儂人**或**克魯麥農人**）是[智人](../Page/智人.md "wikilink")（*Homo
sapiens*，其中包括所有現代[人類](../Page/人類.md "wikilink")）中的一支，生存於[舊石器時代晚期](../Page/舊石器時代.md "wikilink")。原來是指發現於[法國西南部克羅馬儂](../Page/法國.md "wikilink")（）石窟裡的一系列[化石](../Page/化石.md "wikilink")，現在的則包含遷入歐洲以前的早期智慧人種在內。

1868年3月，地質學家在[法國](../Page/法國.md "wikilink")[多爾多涅省](../Page/多爾多涅省.md "wikilink")[埃齊耶的克羅馬儂石窟裡發現了](../Page/埃齊耶.md "wikilink")5具骨骼遺骸。其中的模式標本是一顆[顱骨](../Page/顱骨.md "wikilink")，被命名為「」。這些骨骼與現代人類擁有相同特徵，包括較高的[前額](../Page/前額.md "wikilink")、直挺的姿態，以及纖細的骨架。之後在歐洲其他地區以及[中東](../Page/中東.md "wikilink")，也發現了相同特徵的標本。根據[遺傳學的研究](../Page/遺傳學.md "wikilink")，克羅馬儂人可能源自[非洲東部](../Page/东非.md "wikilink")，經過了[南亞](../Page/南亞.md "wikilink")、[中亞](../Page/中亞.md "wikilink")、中東甚至[北非來到了歐洲](../Page/北非.md "wikilink")\[1\]。

克羅馬儂人在法國、西班牙等國留下大量史前岩畫，表示他们具有一定水準的藝術水平。

有欧洲人类学家認為，现在的[巴斯克人是克羅馬儂人的直接后裔](../Page/巴斯克人.md "wikilink")。

## 參見

  - [尼安德特人](../Page/尼安德特人.md "wikilink")

## 參考文獻

[Category:人屬](../Category/人屬.md "wikilink")
[Category:早期人類](../Category/早期人類.md "wikilink")

1.