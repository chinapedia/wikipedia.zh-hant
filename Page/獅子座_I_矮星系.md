{{ Galaxy | | image =
[Ugc5470.jpg](https://zh.wikipedia.org/wiki/File:Ugc5470.jpg "fig:Ugc5470.jpg")
|caption = Leo I appears as a faint patch to the right of the bright
star, [Regulus](../Page/Regulus.md "wikilink"). Credit:Scott Anttila. |
name = 獅子座 I 矮星系 | epoch = [J2000](../Page/J2000.md "wikilink") | type =
E;dSph\[1\] | ra = \[2\] | dec = \[3\] | dist_ly = 820000 ± 70000
[ly](../Page/光年.md "wikilink")
(250000 ± 20000 [pc](../Page/秒差距.md "wikilink"))\[4\]\[5\] | z = 285 ± 2
[km](../Page/公里.md "wikilink")/s\[6\] | appmag_v = 11.2\[7\] | size_v
= 9′.8 × 7′.4\[8\] | constellation name =
[獅子座](../Page/獅子座.md "wikilink") | notes = | names =
[UGC](../Page/Uppsala_General_Catalogue.md "wikilink") 5470,\[9\]
[PGC](../Page/Principal_Galaxies_Catalogue.md "wikilink") 29488,\[10\]
[DDO](../Page/David_Dunlap_Observatory_Catalogue.md "wikilink")
74,\[11\] A1006,\[12\] Harrington-Wilson \#1,\[13\] Regulus Dwarf\[14\]
}} **獅子座 I
矮星系**是一個位在[獅子座的星系](../Page/獅子座.md "wikilink")，屬於[矮橢球星系](../Page/矮橢球星系.md "wikilink")，[本星系團的成員之一](../Page/本星系團.md "wikilink")。獅子座
I 矮星系同時也是[銀河系的衛星星系](../Page/銀河系.md "wikilink")，而且是所有裡面最遠的。獅子座 I
矮星系在1950年首度被[艾伯特·喬治·威爾遜發現於](../Page/艾伯特·喬治·威爾遜.md "wikilink")[帕洛馬山天文台](../Page/帕洛馬山天文台.md "wikilink")。

## 参考文献

<div class="references-small">

<references />

</div>

[sk:Trpasličia galaxia Lev
I](../Page/sk:Trpasličia_galaxia_Lev_I.md "wikilink")

1.

2.
3.
4.

5.

6.
7.
8.
9.
10.
11.
12.
13.
14.