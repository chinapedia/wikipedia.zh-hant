**西瓜**（[學名](../Page/學名.md "wikilink")：），古稱**寒瓜**，是[葫蘆科](../Page/葫蘆科.md "wikilink")[西瓜屬的一種植物或其果實](../Page/西瓜屬.md "wikilink")。原產於[非洲](../Page/非洲.md "wikilink")，是一種[雙子葉](../Page/雙子葉.md "wikilink")[開花植物](../Page/開花植物.md "wikilink")，形狀像[蔓藤](../Page/蔓藤.md "wikilink")，葉子呈羽毛狀。它所結出的果實是[假果](../Page/假果.md "wikilink")，且屬於[植物學家稱為](../Page/植物學.md "wikilink")[假漿果的一類](../Page/假漿果.md "wikilink")。果實外皮光滑，呈綠色或黃色及有深綠色的花纹，果瓤多汁為红色或黃色，內有籽。

西瓜可分為野生或種植的。野生的西瓜稱為**野生西瓜種質**（學名：），瓤和瓢顏色分別接近[冬瓜](../Page/冬瓜.md "wikilink")。而種植的西瓜則稱為**栽培西瓜品系**（學名：）。

## 歷史

[Watermelon-garden.jpg](https://zh.wikipedia.org/wiki/File:Watermelon-garden.jpg "fig:Watermelon-garden.jpg")
[Kustodiev_Merchants_Wife.jpg](https://zh.wikipedia.org/wiki/File:Kustodiev_Merchants_Wife.jpg "fig:Kustodiev_Merchants_Wife.jpg")
現時很難知道種植[西瓜是由何時開始](../Page/西瓜.md "wikilink")，而自有紀錄以來，第一次記有西瓜收成的要在5000年前的[古埃及](../Page/古埃及.md "wikilink")。西瓜果實經常被放在[法老的陵墓](../Page/法老.md "wikilink")，作為來世享用。事實上，在[埃及神話中](../Page/埃及神話.md "wikilink")，他們相信西瓜是由[賽特的](../Page/賽特.md "wikilink")[精液所產出](../Page/精液.md "wikilink")。

直至公元10世紀，[契丹人首次從](../Page/契丹.md "wikilink")[中亞將西瓜帶到中國](../Page/中亞.md "wikilink"){{\#tag:ref|[胡嶠](../Page/胡嶠.md "wikilink")〈陷虜記〉：“自上京東去四十里，至真珠寨，始食菜。明日，東行，地勢漸高，西望平地松林鬱然數十里。遂入平川，多草木，始食西瓜，云契丹破回紇得此種，以牛糞覆棚而種，大如中國冬瓜而味甘。”。}}。[陶弘景在](../Page/陶弘景.md "wikilink")《[本草經集注](../Page/本草經集注.md "wikilink")》時，曾述“[永嘉有寒瓜甚大](../Page/永嘉.md "wikilink")，可藏至春”。[李時珍引用其言](../Page/李時珍.md "wikilink")，認同其所述“盖五代之先，瓜种已入浙东，但无西瓜之名，未遍中国尔”，相信“寒瓜”就是西瓜。南宋[紹興年間](../Page/紹興_\(南宋\).md "wikilink")，[洪皓](../Page/洪皓.md "wikilink")《松漠紀聞續》裏始有西瓜之名，洪皓提到“西瓜形如扁蒲而圓，色極青翠，經歲則變黃。其瓞類甜瓜，味甘脆，中有汁，尤冷。《[五代史](../Page/五代史.md "wikilink")‧四夷附錄》云：‘以牛糞覆棚種之。’予攜以歸，今禁圃鄉囿皆有。”《[事物纪原](../Page/事物纪原.md "wikilink")》一书中说“中国初无西瓜，洪忠宣（洪皓）使金，贬递阴山，得食之”。绍兴十三年（1143年），洪皓歸宋，带回了西瓜种子，种植於中原地區和杭州、饶州、英州等地。

13世紀，[摩爾人入侵](../Page/摩爾人.md "wikilink")[歐洲](../Page/歐洲.md "wikilink")，同時將西瓜引入。1615年，西瓜（Watermelon）這個字才在英文字典上首次出現。1500年代，西瓜才被引進至[北美洲的](../Page/北美洲.md "wikilink")[印第安人](../Page/印第安人.md "wikilink")。早期的[法國探險家發現原住民在](../Page/法國.md "wikilink")[密西西比河種植西瓜](../Page/密西西比河.md "wikilink")，直至1629年，西瓜才從英國被引入美國，首次種植於[麻薩諸塞州](../Page/麻薩諸塞州.md "wikilink")。而[非洲的黑人奴隸及歐洲殖民亦有協助於將西瓜帶往世界各地](../Page/非洲.md "wikilink")。到20世纪，[美国](../Page/美国.md "wikilink")、[俄罗斯皆发展为西瓜生产大国](../Page/俄罗斯.md "wikilink")。\[1\]

首位跨越[非洲的西方人](../Page/非洲.md "wikilink")[戴维·利文斯通指野生西瓜在非洲西南部的](../Page/大衛·李文斯頓_\(探險家\).md "wikilink")[喀拉哈里沙漠非常豐富](../Page/喀拉哈里沙漠.md "wikilink")，當地人稱為，是他們主要的水源及食糧。

## 名稱

西瓜的拉丁學名中，“”是“”（[柑橘](../Page/柑橘.md "wikilink")、[香櫞](../Page/香櫞.md "wikilink")）的指小詞，描述的是西瓜的果肉顏色；而[種加詞](../Page/種加詞.md "wikilink")“”意為“綿狀的，具綿狀毛的”（来自拉丁语，“羊毛”），指其莖具毛。西瓜原來所用的學名為，其中種加詞“”意為“尋常的，普通的”。

西瓜在[日本稱為](../Page/日本.md "wikilink")“”，本來為[漢字](../Page/漢字.md "wikilink")“西瓜”的[唐音讀法](../Page/唐音.md "wikilink")，後來受到的影響，有時也用同音的漢字“水瓜”來[表記](../Page/表記.md "wikilink")。

歐洲語言中，英語稱西瓜為“”，意為“水瓜”（但在粵語裡「[水瓜](../Page/水瓜.md "wikilink")」是指絲瓜），對應的德語為，另外其他對應的歐洲語言還有：，等。

除了稱其為“水瓜”之外，還有稱其產地的，如：

  - （），為（）的簡稱，意即“痕都斯坦瓜”（[痕都斯坦即](../Page/痕都斯坦.md "wikilink")[印度斯坦](../Page/印度斯坦.md "wikilink")）。

  - ，，來自于[阿拉伯語](../Page/阿拉伯語.md "wikilink")（），意為“[信德](../Page/信德.md "wikilink")”，為印度的一個地區。

在[阿富汗等地的](../Page/阿富汗.md "wikilink")[達里波斯語中](../Page/達里語.md "wikilink")，一般稱西瓜為（），但是在[伊朗的波斯語中](../Page/伊朗.md "wikilink")，可以指所有的長型瓜類，該詞在其他語言中的衍生詞有：

  -
  -
  - （）

  - （）

  -
在波斯語中，還可以用（）或（）來表示西瓜，其在其他語言中的衍生詞有：

  -
  - （）

  - （）

在[阿拉伯語中](../Page/阿拉伯語.md "wikilink")，西瓜稱為（），在其他語言中的衍生詞有：

  -
  -
## 食品和飲料

### 营养价值

西瓜果瓤（音同攘）多汁，含有豐富的[維生素A及](../Page/維生素A.md "wikilink")[維生素C](../Page/維生素C.md "wikilink")，通常是生食或者搾汁飲用，可以消暑解渴。根據[美國農業部的研究顯示](../Page/美國農業部.md "wikilink")，每100克西瓜的食用價值如下：

  - [碳水化合物](../Page/碳水化合物.md "wikilink")——7.6克
      - [膳食纖維](../Page/膳食纖維.md "wikilink")——0.4克
  - [脂肪](../Page/脂肪.md "wikilink")——0.2克
  - [蛋白質](../Page/蛋白質.md "wikilink")——0.6克
  - [水份](../Page/水.md "wikilink")——91克
  - [維生素C](../Page/維生素C.md "wikilink")——8.1毫克

一杯西瓜汁可以提供約48千[卡路里](../Page/卡路里.md "wikilink")、14.59毫克的維生素C及556.32[IU的維生素A](../Page/國際單位.md "wikilink")。另外，西瓜汁亦可提供可觀的[維生素B<sub>1</sub>及](../Page/硫胺.md "wikilink")[維生素B<sub>6</sub>](../Page/吡哆醇.md "wikilink")、[鉀及](../Page/鉀.md "wikilink")[鎂](../Page/鎂.md "wikilink")，有能對抗[癌症或保護](../Page/癌症.md "wikilink")[太陽灼傷](../Page/太陽.md "wikilink")[皮膚功效的](../Page/皮膚.md "wikilink")[胡蘿蔔素](../Page/胡蘿蔔素.md "wikilink")[抗氧化劑](../Page/抗氧化劑.md "wikilink")：[茄紅素及](../Page/茄紅素.md "wikilink")[胡蘿蔔素](../Page/胡蘿蔔素.md "wikilink")。西瓜盅富含的钾元素，可以对血压控制和心脏健康有帮助\[2\]。

### 食用方式

  - 利用西瓜汁浸泡在[酒精中或](../Page/酒精.md "wikilink")[發酵後](../Page/發酵.md "wikilink")，可以製作出酸甜度適中及帶有果香的西瓜酒。

[Watermelonpile.jpg](https://zh.wikipedia.org/wiki/File:Watermelonpile.jpg "fig:Watermelonpile.jpg")

  - 西瓜皮亦是可吃的，而有時亦是[蔬菜](../Page/蔬菜.md "wikilink")，整個西瓜也充滿價值。在[中國菜中](../Page/中國.md "wikilink")，有[炒](../Page/炒.md "wikilink")、[-{zh-hans:炖;zh-hant:燉;}-及](../Page/炖.md "wikilink")[醃西瓜皮來吃](../Page/醃.md "wikilink")，甚至取其白皮剁碎加肉包[饺子](../Page/饺子.md "wikilink")。有一份炒西瓜皮的菜谱：先除去皮衣及果肉，再與[橄欖油](../Page/橄欖油.md "wikilink")、[大蒜](../Page/大蒜.md "wikilink")、[辣椒](../Page/辣椒.md "wikilink")、[蔥](../Page/蔥.md "wikilink")、[糖及](../Page/糖.md "wikilink")[朗姆酒來一起煮](../Page/朗姆酒.md "wikilink")。醃西瓜皮則在[俄羅斯非常盛行](../Page/俄羅斯.md "wikilink")。
  - 西瓜的[種子](../Page/種子.md "wikilink")，亦即**瓜子**充滿[脂肪及](../Page/脂肪.md "wikilink")[蛋白質](../Page/蛋白質.md "wikilink")，既可以當作[小吃](../Page/小吃.md "wikilink")，亦可以作為配菜或[植物油](../Page/植物油.md "wikilink")。利用特別的配種可以生產少果肉而多核的西瓜。在中國，瓜子是非常普遍的小吃，有[烤及](../Page/烤.md "wikilink")[調味的](../Page/調味.md "wikilink")。在[西非](../Page/西非.md "wikilink")，西瓜種子被壓榨出油，亦是非常出名的[埃古斯湯](../Page/埃古斯.md "wikilink")。但是，有時會把多核的西瓜會與有著相當接近特徵及用途的[苦西瓜](../Page/苦西瓜.md "wikilink")（，別名「柯羅辛」colocynth、「苦蘋果」bitter
    apple等等\[3\]）混淆。当然，近年也有无籽的西瓜出售，见[无籽西瓜](../Page/无籽西瓜.md "wikilink")。
  - 西瓜按[重量計算有](../Page/重量.md "wikilink")92%的[水份](../Page/水.md "wikilink")，是水份比例最高的水果，可以榨成[果汁](../Page/果汁.md "wikilink")。在[美國及](../Page/美國.md "wikilink")[南非](../Page/南非.md "wikilink")，有一種新奇的西瓜是含有[酒精成份的](../Page/酒精.md "wikilink")「硬西瓜」。製作過程是鑽孔入西瓜內，並加入[烈酒與果肉融合](../Page/烈酒.md "wikilink")，享用時與吃普通西瓜無異。
  - 将西瓜切成三角状，放入[冰柜中冷藏](../Page/冰柜.md "wikilink")，即为[冰棒](../Page/冰棒.md "wikilink")。

## 醫藥療效

西瓜在清朝[張璐的](../Page/張璐_\(清朝\).md "wikilink")《[本經逢源](../Page/本經逢源.md "wikilink")》中被指是天生的[白虎湯](../Page/白虎湯.md "wikilink")，能清熱生津，解渴除煩。西瓜果肉內亦含有[瓜氨酸及](../Page/瓜氨酸.md "wikilink")[精氨酸等成份](../Page/精氨酸.md "wikilink")，能加增[尿素的形成](../Page/尿素.md "wikilink")，有[利尿作用](../Page/利尿作用.md "wikilink")。若[酒精中毒或酒醉後頭暈](../Page/酒精.md "wikilink")，可喝一杯西瓜汁，運用其排尿作用，幫助排走[肝臟的酒精成份](../Page/肝臟.md "wikilink")。
西瓜中所含的[糖](../Page/糖.md "wikilink")、[蛋白質和微量的](../Page/蛋白質.md "wikilink")[鹽](../Page/鹽.md "wikilink")，能降低[血脂軟化](../Page/血脂.md "wikilink")[血管](../Page/血管.md "wikilink")，對醫治心血管病，如[高血壓等亦有療效](../Page/高血壓.md "wikilink")。西瓜皮及種子殼所製成的西瓜霜，能夠治療[口瘡](../Page/口瘡.md "wikilink")、[口疳](../Page/口疳.md "wikilink")，[牙疳](../Page/牙疳.md "wikilink")，[急性咽喉炎待喉症](../Page/急性咽喉炎.md "wikilink")。

[美國一些](../Page/美國.md "wikilink")[藥理學家亦發現西瓜中的](../Page/藥理學家.md "wikilink")[瓜氨酸](../Page/瓜氨酸.md "wikilink")，有著與[威而剛類似的藥理作用](../Page/威而剛.md "wikilink")，能增加進入[陰莖海綿體內的](../Page/陰莖海綿體.md "wikilink")[血液量](../Page/血液.md "wikilink")，及促進血管內釋放[一氧化氮](../Page/一氧化氮.md "wikilink")。可是，若要有一顆威而剛一樣的效力，就必須吃最少30個西瓜。

[糖尿病患者則不宜吃西瓜](../Page/糖尿病.md "wikilink")。西瓜內的糖份及其利尿作用，會增加糖尿病患者[腎臟的負擔](../Page/腎臟.md "wikilink")，提升血糖尿糖。

由於，西瓜多水份的緣故，吃太多西瓜會沖淡[胃裡的](../Page/胃.md "wikilink")[胃酸](../Page/胃酸.md "wikilink")，引致[胃炎](../Page/胃炎.md "wikilink")、[消化不良或](../Page/消化不良.md "wikilink")[腹瀉等病](../Page/腹瀉.md "wikilink")。

## 挑選方法

[Single_Watermelon_Leaf_2000px.jpg](https://zh.wikipedia.org/wiki/File:Single_Watermelon_Leaf_2000px.jpg "fig:Single_Watermelon_Leaf_2000px.jpg")

  - 西瓜在結果時，常會在其內出現空洞，導致纖維化而影響口感。故選擇西瓜時常用敲擊西瓜外皮的方式，以回音來辨識西瓜的品質，声音清脆大多没熟，声音沉闷表示适合食用。
  - 雨季時所採收的西瓜，常因吸收過多的水份，導致甜度降低\[4\]，或是從中心開始腐爛，以致口感不佳，故消費者應避免於雨季購買西瓜。

非破壞性水果光學糖度計可以測西瓜甜度而不損傷西瓜。\[5\]\[6\]

## 药用

《本草纲目》记载：西瓜又名寒瓜。皮甘、凉、无毒。

主治：

  - 口舌生疮。用西瓜皮烧过，研末，放入口内含噙。
  - 内挫腰痛。用西瓜青皮阴干，研为末，盐酒调服15克。
  - 食瓜过多，人感不适。用瓜皮煎服汤饮即可。
  - 瓜藤解酒。

<table>
<thead>
<tr class="header">
<th><p>西瓜产量前五名（2016年，<a href="../Page/吨.md" title="wikilink">吨</a>）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>世界合计</p></td>
</tr>
<tr class="odd">
<td><center>
<p><small>來源：<a href="https://web.archive.org/web/20110713020710/http://faostat.fao.org/site/339/default.aspx">UN FAOSTAT</a> [7][8]</small></p>
</center></td>
</tr>
</tbody>
</table>

## 禁忌

西瓜在[美国被視為](../Page/美国.md "wikilink")[種族歧視黑人的象徵符號](../Page/種族歧視.md "wikilink")。\[9\]\[10\]\[11\]

## 注释

## 参考文献

## 外部链接

  -
  - [西瓜
    Xigua](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D00064)
    藥用植物圖像數據庫（香港士忽大學中西醫藥學院）

  - [西瓜品種、行銷及營養與應用研討會專輯](https://web.archive.org/web/20130629015642/http://book.tndais.gov.tw/Other/2013watermelon.htm)
    - 台南區農業改良場

## 参见

  - [無籽西瓜](../Page/無籽西瓜.md "wikilink")
  - [瓜子](../Page/瓜子.md "wikilink")
  - [水果](../Page/水果.md "wikilink")
  - [果汁](../Page/果汁.md "wikilink")
  - [中國西瓜品種列表](../Page/中國西瓜品種列表.md "wikilink")

[西瓜屬](../Category/西瓜屬.md "wikilink")
[Category:水果](../Category/水果.md "wikilink")
[Category:农作物](../Category/农作物.md "wikilink")
[Category:原產於非洲的水果](../Category/原產於非洲的水果.md "wikilink")

1.  [黄盛墇《西瓜引种中国与发展考信录》](http://www.agri-history.net/scholars/huangshengzhang1.htm)
2.
3.
4.
5.
6.
7.
8.
9.
10.
11. William Black. "[How Watermelons Became a Racist
    Trope](https://www.theatlantic.com/national/archive/2014/12/how-watermelons-became-a-racist-trope/383529/)",
    *[The Atlantic](../Page/The_Atlantic.md "wikilink")*, 8 December
    2014. Retrieved 20 August 2016