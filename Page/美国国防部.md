**美国国防部**（，簡稱或）是[美國聯邦行政部門之一](../Page/美國聯邦行政部門.md "wikilink")，主要負責統合[國防與](../Page/國防.md "wikilink")[陸海空軍（美軍）](../Page/美国军事.md "wikilink")，其總部大樓位於**[五角大楼](../Page/五角大楼.md "wikilink")**（），因此人们也常用五角大楼作為美國國防部的代稱。国防部的行政首長是[國防部長](../Page/美国国防部长.md "wikilink")，依照[美国法律由](../Page/美国法律.md "wikilink")[文官擔任](../Page/文官.md "wikilink")。美國國防部設有三個軍事部門：[陆军部](../Page/美国陆军部.md "wikilink")、[海军部與](../Page/美国海军部.md "wikilink")[空軍部](../Page/美國空軍部.md "wikilink")，涵蓋除[海岸防衛隊外的所有美國軍隊](../Page/美国海岸警卫队.md "wikilink")。除此之外，美國國防部亦設有若干國防幕僚與研究單位，同時管轄各[軍校](../Page/軍校.md "wikilink")。

1947年，[美国政府將軍隊管理中央化](../Page/美国政府.md "wikilink")，將軍事指揮權統一交由新成立的「國家軍事機構」（），除了將[陆军部與](../Page/美国陆军部.md "wikilink")[海军部交由其管轄之外](../Page/美国海军部.md "wikilink")，同时将[美國陸軍航空軍升格为独立的](../Page/美國陸軍航空軍.md "wikilink")[美國空軍](../Page/美國空軍.md "wikilink")，建立一个直屬於該機構的空军部。至1949年8月10日，國家軍事機構才更名為國防部。美国国防部现在的组织是按照[美国国会通过的](../Page/美国国会.md "wikilink")1986年的《戈德华特－尼科尔斯国防部改组法》（Goldwater-Nichols
Act of
1986）。按照这个[法案](../Page/法案.md "wikilink")，军事命令是从[美国总统通过](../Page/美国总统.md "wikilink")[美国国防部长直接到](../Page/美国国防部长.md "wikilink")[美国战区司令官](../Page/美国战区司令官.md "wikilink")。[參謀長聯席會議有责任管理美国武器和後备军人](../Page/參謀長聯席會議.md "wikilink")，参谋长聯席會議主席也担任总统和国家安全委员会的首席军事顾问，但是参联会没有指挥权。

## 組織架構

[The_Pentagon_US_Department_of_Defense_building.jpg](https://zh.wikipedia.org/wiki/File:The_Pentagon_US_Department_of_Defense_building.jpg "fig:The_Pentagon_US_Department_of_Defense_building.jpg")
国防部由六个实体部门组成：

### 國防部部長辦公室

  - [国防部部长办公室](../Page/美国国防部部长办公室.md "wikilink")
      - （国防政策顧問委員會）
      - 美国国防部[净评估办公室](../Page/净评估办公室.md "wikilink")（Office of Net
        Assessment）
      - 总督察长室（Office of Inspector General）
          - [国防刑事调查处](../Page/国防刑事调查处.md "wikilink")（Defense Criminal
            Investigative Service）

### 軍事部門

  - [陆军部](../Page/美国陆军部.md "wikilink") -
    包括[美国陆军](../Page/美国陆军.md "wikilink")
  - [海军部](../Page/美国海军部.md "wikilink") -
    包括[美國海軍和](../Page/美國海軍.md "wikilink")[美国海军陆战队](../Page/美国海军陆战队.md "wikilink")（戰時包括[美國海岸防衛隊](../Page/美國海岸防衛隊.md "wikilink")）
  - [空军部](../Page/美国空军部.md "wikilink") -
    包括[美国空军](../Page/美国空军.md "wikilink")

檔案:US Department of the Army
Seal.png|[美国陆军徽章](../Page/美国陆军.md "wikilink")
檔案:United States Department of the Navy
Seal.svg|[美國海軍徽章](../Page/美國海軍.md "wikilink") 檔案:USMC
logo.svg|[美国海军陆战队徽章](../Page/美国海军陆战队.md "wikilink") 檔案:USCG S
W.svg|[美國海岸防衛隊徽章](../Page/美國海岸防衛隊.md "wikilink") 檔案:Seal of the
US Air Force.svg|[美国空军徽章](../Page/美国空军.md "wikilink")

### 參謀長聯席會議

  - [参谋长联席会议](../Page/参谋长联席会议.md "wikilink")
      - [美国海军天文台](../Page/美国海军天文台.md "wikilink")

檔案:Joint Chiefs of Staff
seal.svg|[参谋长联席会议徽章](../Page/参谋长联席会议.md "wikilink")
檔案:U.S. Naval
Observatory-seal.gif|[美国海军天文台徽章](../Page/美国海军天文台.md "wikilink")

### 作戰司令部

  - [北方司令部](../Page/美国北方司令部.md "wikilink")（NORTHCOM）
  - [南方司令部](../Page/美国南方司令部.md "wikilink")（SOUTHCOM）
  - [中央司令部](../Page/美国中央司令部.md "wikilink")（CENTCOM）
  - [欧洲司令部](../Page/美国欧洲司令部.md "wikilink")（EUCOM）
  - [非洲司令部](../Page/美国非洲司令部.md "wikilink")（AFRICOM）
  - [印太司令部](../Page/美国印太司令部.md "wikilink")（INDOPACOM）
  - [特种作战司令部](../Page/美国特种部队司令部.md "wikilink")（SOCOM）
  - [战略司令部](../Page/美国战略司令部.md "wikilink")（STRATCOM）
  - [运输司令部](../Page/美国运输司令部.md "wikilink")（TRANSCOM）
  - [网络司令部](../Page/美国网络司令部.md "wikilink")（CYBERCOM）

<file:United> States Special Operations Command
Insignia.svg|[美国特种部队司令部徽章](../Page/美国特种部队司令部.md "wikilink")
<File:USPACOM> seal.png|[美国太平洋司令部徽章](../Page/美国太平洋司令部.md "wikilink")
<File:Seal> of the United States Northern Command.png|美国北方司令部徽章

### 國防部局

  - [国防高级研究计划局](../Page/國防高等研究計劃署.md "wikilink")（Defense Advanced
    Research Projects Agency）

  - [國防部軍營超市管理處](../Page/美國國防部軍營超市管理處.md "wikilink")（Defense Commissary
    Agency）

  - [国防合约审计局](../Page/国防合约审计局.md "wikilink")（Defense Contract Audit
    Agency）

  - [国防合约管理局](../Page/国防合约管理局.md "wikilink")（Defense Contract Management
    Agency）

  - [国防财务会计处](../Page/国防财务会计处.md "wikilink")（Defense Finance and
    Accounting Service）

  - （Defense Information Systems Agency）

  - [国防情报局](../Page/美国国防情报局.md "wikilink")（Defense Intelligence Agency）

  - [国防法务局](../Page/国防法务局.md "wikilink")（Defense Legal Services Agency）

  - [國防後勤局](../Page/美國國防後勤局.md "wikilink")（Defense Logistics Agency）

  - [国防安全合作局](../Page/美国国防部国防安全合作局.md "wikilink")（Defense Security
    Cooperation Agency）

  - [国防安全处](../Page/国防安全处.md "wikilink")（Defense Security Service）

  - [国防威胁降低局](../Page/美国国防威胁降低局.md "wikilink")（Defense Threat Reduction
    Agency）

  - [导弹防御局](../Page/导弹防御局.md "wikilink")（Missile Defense Agency）

  - [国家安全局](../Page/美国国家安全局.md "wikilink")（National Security Agency）

  - [国家侦察局](../Page/美国国家侦察局.md "wikilink")（National Reconnaissance
    Office）

  - [国家地理空间情报局](../Page/美国国家地理空间情报局.md "wikilink")（National
    Geospatial-Intelligence Agency）

  - [五角大楼保护局](../Page/五角大楼保护局.md "wikilink")（Pentagon Force Protection
    Agency）

檔案:Defense Logistics
Agency.jpg|[美國國防後勤局徽章](../Page/美國國防後勤局.md "wikilink")
檔案:National Security
Agency.svg|[美国国家安全局徽章](../Page/美国国家安全局.md "wikilink")
檔案:US-NationalReconnaissanceOffice-Seal.svg|[美国国家侦察局徽章](../Page/美国国家侦察局.md "wikilink")
<File:US-NationalGeospatialIntelligenceAgency-2008Seal.svg>|[美国国家地理空间情报局徽章](../Page/美国国家地理空间情报局.md "wikilink")

### 國防直屬業務處

  - [美国军队信息局](../Page/美国军队信息局.md "wikilink")（American Forces Information
    Service）
  - [国防部战俘/失踪人员办公室](../Page/国防部战俘/失踪人员办公室.md "wikilink")（Defense
    Prisoner of War/Missing Personnel Office）
  - [国防部教育处](../Page/国防部教育处.md "wikilink")（Department of Defense
    Education Activity）
      - [国防部附属学校](../Page/国防部附属学校.md "wikilink")（Department of Defense
        Dependents Schools）
  - [国防部人力资源处](../Page/国防部人力资源处.md "wikilink")（DoD Human Resources
    Activity）
  - [经济调节办公室](../Page/经济调节办公室.md "wikilink")（Office of Economic
    Adjustment）
  - [三军医疗保健管理处](../Page/三军医疗保健管理处.md "wikilink")（Tricare Management
    Activity）
  - [华盛顿总部勤务处](../Page/华盛顿总部勤务处.md "wikilink")（Washington Headquarters
    Services）

2003年，[全国通信系统](../Page/全国通信系统.md "wikilink")（National Communications
System）改歸[美国国土安全部管轄](../Page/美国国土安全部.md "wikilink")。

## 参见

  - [美国国防部长](../Page/美国国防部长.md "wikilink")
      - [美国国防部副部长](../Page/美国国防部副部长.md "wikilink")
  - [美國參謀長聯席會議](../Page/美國參謀長聯席會議.md "wikilink")
      - [美國參謀長聯席會議主席](../Page/美國參謀長聯席會議主席.md "wikilink")

## 参考文献

## 外部連結

  -

{{-}}

[G](../Category/美國政府行政部門.md "wikilink")
[美國國防部](../Category/美國國防部.md "wikilink")
[USA](../Category/國防部門.md "wikilink")
[Category:1947年建立政府機構](../Category/1947年建立政府機構.md "wikilink")