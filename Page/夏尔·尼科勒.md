**夏尔·朱尔·亨利·尼科勒**（，）是一位[法國細菌學家](../Page/法國.md "wikilink")，曾經因為關於辨認出[虱子為](../Page/虱子.md "wikilink")[斑疹傷寒的傳染者](../Page/斑疹傷寒.md "wikilink")，而獲得1928年諾貝爾生理學或醫學獎。

尼科勒出生於法國[魯昂](../Page/魯昂.md "wikilink")，他的父親是當地醫院的一位醫師，因此他很早便開始學習生物學知識。1893年，他獲得了[巴斯德研究院授與的](../Page/巴斯德研究院.md "wikilink")[醫學博士學位](../Page/醫學博士.md "wikilink")。之後他回到魯昂。

1903年，尼科勒開始擔任巴斯德研究院的主任，直到1936年逝世為止。在這段期間，他獲得了諾貝爾獎。

除了微生物的研究之外，他也些了一些[小說與](../Page/小說.md "wikilink")[哲學書籍](../Page/哲學.md "wikilink")，包括《Le
Pâtissier de Bellone》、《Les deux Larrons》與《Les Contes de Marmouse》。

他在1895年結婚，擁有兩個小孩。

[Category:諾貝爾生理學或醫學獎獲得者](../Category/諾貝爾生理學或醫學獎獲得者.md "wikilink")
[Category:法國生物學家](../Category/法國生物學家.md "wikilink")
[Category:魯昂人](../Category/魯昂人.md "wikilink")