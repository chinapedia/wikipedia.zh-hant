[Lower_Wutun_Monastery.jpg](https://zh.wikipedia.org/wiki/File:Lower_Wutun_Monastery.jpg "fig:Lower_Wutun_Monastery.jpg")
**隆务寺**，全称“隆务贡德钦却科尔林”，著名[藏传佛教](../Page/藏传佛教.md "wikilink")[格鲁派寺庙](../Page/格鲁派.md "wikilink")，位于[中国](../Page/中国.md "wikilink")[青海省](../Page/青海省.md "wikilink")[黄南藏族自治州](../Page/黄南藏族自治州.md "wikilink")[同仁县](../Page/同仁县.md "wikilink")[隆务镇](../Page/隆务镇.md "wikilink")，为[安多藏区仅次于](../Page/安多.md "wikilink")[拉卜楞寺和](../Page/拉卜楞寺.md "wikilink")[塔尔寺的第三大寺](../Page/塔尔寺.md "wikilink")，[全国重点文物保护单位](../Page/全国重点文物保护单位.md "wikilink")。

隆务寺原为[萨迦派寺院](../Page/萨迦派.md "wikilink")，始建于[元朝](../Page/元朝.md "wikilink")[大德五年](../Page/大德_\(元朝\).md "wikilink")（1301年），名为“智卡贡康”\[1\]，[明朝](../Page/明朝.md "wikilink")[洪武三年](../Page/洪武.md "wikilink")（1370年），获得[中国](../Page/中国.md "wikilink")[明朝政府认可](../Page/明朝政府.md "wikilink")。[宣德元年](../Page/宣德.md "wikilink")（1462年），[三木旦仁钦重建隆务寺](../Page/三木旦仁钦.md "wikilink")，后其弟[罗哲森格继任为法台](../Page/罗哲森格.md "wikilink")，被[宣德帝封为](../Page/宣德帝.md "wikilink")“弘修妙悟国师”，成为当地政教合一的领袖。[万历三十三年](../Page/万历.md "wikilink")（1605年），隆务寺皈依格鲁派，再次被扩建。

1630年，[雅杰蔼丹嘉措成为隆务寺法台](../Page/雅杰蔼丹嘉措.md "wikilink")，多有建树，后被认为是三木旦仁钦转世，成为一世[夏日仓呼图克图活佛](../Page/夏日仓呼图克图.md "wikilink")，下属有黄南地区[昂锁](../Page/昂锁.md "wikilink")，称“额尔德尼昂锁”，下辖十二部族，占地约为今日同仁县和[泽库县两县](../Page/泽库县.md "wikilink")。经历辈夏日仓活佛不断扩充，隆务寺成为安多三大寺之一，下辖十八座子寺。根据1958年统计，隆务寺寺院占地面积380亩，周边耕地1000亩，园林300亩，寺内有大小殿堂35座1730间，活佛府邸43府4201间，僧舍303院2734间，共有僧侣1712人，其中活佛43名。隆务寺辖区人口6926户，32509人。

1959年后隆务寺屡遭破坏，特别是在[文化大革命中受到很大冲击](../Page/文化大革命.md "wikilink")，1980年后经修复重新开放，现有佛堂7座，活佛府邸6座，僧舍300余间，僧人300余名。寺内还设有十世[班禅额尔德尼·确吉坚赞](../Page/班禅额尔德尼·确吉坚赞.md "wikilink")[行宫](../Page/行宫.md "wikilink")。

## 参考资料

  - [萨迦派在青海河湟地区的传播·蒲文成](http://www.budd.cn/book/readari.asp?no=49500)
  - [黄南文化旅游网·隆务寺](https://web.archive.org/web/20110617132553/http://www.tibetinfor.com.cn/t/040701hn/20040200472141615.htm)

<!-- end list -->

  - 无序列表项

[Category:青海藏传佛教寺院](../Category/青海藏传佛教寺院.md "wikilink")
[Category:黄南文物保护单位](../Category/黄南文物保护单位.md "wikilink")
[Category:黄南佛寺](../Category/黄南佛寺.md "wikilink")
[Category:同仁县](../Category/同仁县.md "wikilink")

1.  [青海黄南隆务寺](http://xy.tibetcul.com/sy/gl/200708/6549.html) 2007年08月06日