**逸仙公園**位於[台灣](../Page/台灣.md "wikilink")[台北市](../Page/台北市.md "wikilink")[中正區](../Page/中正區_\(臺北市\).md "wikilink")[台北車站附近](../Page/台北車站.md "wikilink")，又名**國父史蹟紀念館**，為紀念[中華民國國父](../Page/中華民國.md "wikilink")、已故大總統[孫中山的](../Page/孫中山.md "wikilink")[公園](../Page/公園.md "wikilink")。該館為[孫逸仙於](../Page/孫逸仙.md "wikilink")1913年來台時下榻之[料亭](../Page/料亭.md "wikilink")，亦為當年[台灣總督](../Page/台灣總督.md "wikilink")[佐久間左馬太與孫逸仙會談政情的場所](../Page/佐久間左馬太.md "wikilink")。現址為臺北市中正區中山北路一段46號，北靠[市民大道一段](../Page/市民大道.md "wikilink")、東靠[中山北路一段](../Page/中山北路_\(台北市\).md "wikilink")、西靠台北車站。

## 簡介

逸仙公園面積約3025[平方公尺](../Page/平方公尺.md "wikilink")，園內主要建物為約50坪的「梅屋敷」。因庭園栽種「[梅樹](../Page/梅樹.md "wikilink")」而得名的梅屋敷興建於1900年，本為日人[大和宗吉所經營的料亭](../Page/大和宗吉.md "wikilink")。

梅屋敷是[日治時代台北著名的高級料亭](../Page/台灣日治時期.md "wikilink")，位在[北門町](../Page/北門町.md "wikilink")，舉凡台灣總督、商界大亨或社會名士都是座上賓，席間亦可召[藝妓來助興](../Page/藝妓.md "wikilink")。梅屋敷為長方形的和式建築，屋頂覆蓋著老式暗黑色的[理想瓦](../Page/理想瓦.md "wikilink")。相傳，梅屋敷曾以[高砂族藝妓表演舞蹈做宣傳來招攬旅客](../Page/高砂族.md "wikilink")。梅屋敷不只曾於20世紀初招待過[孫逸仙](../Page/孫逸仙.md "wikilink")、[胡漢民等知名政要](../Page/胡漢民.md "wikilink")。1907年知名[導演](../Page/導演.md "wikilink")[高松豐次郎接受](../Page/高松豐次郎.md "wikilink")[台灣總督府委託所拍攝的](../Page/台灣總督府.md "wikilink")[電影](../Page/電影.md "wikilink")《台灣實況紹介》中，還把梅屋敷的高砂族藝妓表演列為台灣廿景之一。

## 戰後重建

[原梅屋敷.jpg](https://zh.wikipedia.org/wiki/File:原梅屋敷.jpg "fig:原梅屋敷.jpg")
戰後，因梅屋敷曾為中華民國國父孫中山所下榻，1947年[中華民國總統](../Page/中華民國總統.md "wikilink")[蔣中正特別指定該地為](../Page/蔣中正.md "wikilink")「國父史蹟紀念館」，也稱為逸仙公園。除保持園內的梅樹栽種外，中華民國政府還將日式庭園重新改建成小型的[蘇州古典園林式花園](../Page/蘇州古典園林.md "wikilink")（一說為臺人[吳東碧獨資修建](../Page/吳東碧.md "wikilink")）。

逸仙公園內主要的建築物梅屋敷料亭，則改成陳列孫逸仙四次來台的相關資料的紀念館，二次革命期間下榻梅屋敷的孫逸仙留影、手稿等資料為主要陳列項目。除此，也因該紀念館位置接近日治時期的[敕使街道](../Page/敕使街道.md "wikilink")，[臺北市政府特地將整段敕使街道重新命名為](../Page/臺北市政府.md "wikilink")[中山北路](../Page/中山北路.md "wikilink")，也將中山北路貫穿的[宮前町等地重新命名為中山區](../Page/宮前町.md "wikilink")。

1954年，蔣中正於逸仙公園親題「匡復中華的起點，重建民國的基地」十四字，勒石、立碑、建亭，以紀念[中國國民黨建黨六十周年](../Page/中國國民黨.md "wikilink")。

## 遷移與復舊

[逸仙公園庭園.jpg](https://zh.wikipedia.org/wiki/File:逸仙公園庭園.jpg "fig:逸仙公園庭園.jpg")
1983年，[交通部臺北市區地下鐵路工程處着手興建長達](../Page/交通部臺北市區地下鐵路工程處.md "wikilink")14000公尺的[台北市區鐵路地下隧道](../Page/臺北鐵路地下化專案.md "wikilink")，隧道東線頂端部分恰巧經過逸仙公園；為此，逸仙公園於同年遷移重建。因為逸仙公園頗具歷史價值，交通部臺北市區地下鐵路工程處會同臺北市政府與[中國國民黨中央委員會黨史委員會](../Page/中國國民黨中央委員會黨史委員會.md "wikilink")，以原址原貌重建於原址東北約50公尺處。1986年11月12日，逸仙公園重建工程竣工，公園外人行道立[秦孝儀撰文](../Page/秦孝儀.md "wikilink")〈國父史蹟紀念館重建記〉。1987年完成庭園工程。

重建的逸仙公園仍保有梅樹的特徵，更種植[櫻花](../Page/櫻花.md "wikilink")、[杜鵑花等](../Page/杜鵑花.md "wikilink")。該公園佔地雖只有1000坪，但十分接近台北交通樞紐的台北車站，在交通方便因素下，該公園參訪人數頗眾。

## 備註

  - 孫中山訪台4次。第1次來台為1900年策劃[惠州起義前夕](../Page/惠州起義.md "wikilink")。該次來台停留台灣最久，達44天。滯臺期間，曾得到台灣總督[兒玉源太郎資助該起義的承諾](../Page/兒玉源太郎.md "wikilink")，也獲得[菲律賓製](../Page/菲律賓.md "wikilink")[中古武器一批](../Page/中古.md "wikilink")。該次會面，也為兒玉總督於同年出兵佔領[廈門的遠因之一](../Page/廈門.md "wikilink")。
  - 孫逸仙下榻梅敷屋則是在1913年[二次革命](../Page/二次革命.md "wikilink")（反對袁世凱帝制）失敗後。至於第3次與第4次則分別於1918年與1924年。不過孫中山最後一次登台，只隨船泊港於[基隆](../Page/基隆.md "wikilink")，並未上岸。
  - 2005年，台灣知名學者[李敖曾以孫中山下榻梅屋敷的史實](../Page/李敖.md "wikilink")，質疑孫逸仙1913年的台灣之旅有所謂[嫖妓行為](../Page/嫖妓.md "wikilink")。該考證曾引起部分[榮民的嚴重抗議](../Page/榮民.md "wikilink")。一般學者則認為，該史實所稱的高砂族藝妓，應只是單純歌唱舞蹈表演的[台灣原住民](../Page/台灣原住民.md "wikilink")。

## 參考資料

  - [黃昭堂著](../Page/黃昭堂.md "wikilink")，《台灣總督府》，2003年。臺北市：鴻儒堂出版社。
  - 又吉盛清著，《台灣今昔之旅》，1997年，台北市：[前衛出版社](../Page/前衛出版社.md "wikilink")。

## 外部連結

  - [逸仙公園 -
    公園走透透](https://parks.taipei/parks/m2/pkl_parks_m2C.php?sid=645)
  - [逸仙公園
    臺北旅遊網](https://www.travel.taipei/zh-tw/attraction/details/1733)
  - [國父史蹟館
    文化部文化資產局](https://nchdb.boch.gov.tw/assets/advanceSearch/historicalBuilding/20071017000001)

## 參見

  - [孫中山](../Page/孫中山.md "wikilink")
  - [二次革命](../Page/二次革命.md "wikilink")
  - [台灣總督府](../Page/台灣總督府.md "wikilink")

[Category:台北市公園](../Category/台北市公園.md "wikilink")
[台](../Category/中山公園.md "wikilink")
[Category:孫中山紀念館](../Category/孫中山紀念館.md "wikilink")
[Category:傳記博物館](../Category/傳記博物館.md "wikilink")
[Category:日本以外的日式庭園](../Category/日本以外的日式庭園.md "wikilink")
[Category:台灣日式建築](../Category/台灣日式建築.md "wikilink")
[Category:臺北市歷史建築](../Category/臺北市歷史建築.md "wikilink")
[Category:台灣日治時期建築](../Category/台灣日治時期建築.md "wikilink")
[Category:台北車站特定專用區](../Category/台北車站特定專用區.md "wikilink")