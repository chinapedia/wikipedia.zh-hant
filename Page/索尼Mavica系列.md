[Sony_mavica_fd5_cleaned.jpg](https://zh.wikipedia.org/wiki/File:Sony_mavica_fd5_cleaned.jpg "fig:Sony_mavica_fd5_cleaned.jpg")
**Mavica**是[Sony一種已停產的](../Page/Sony.md "wikilink")[數碼相機品牌](../Page/數碼相機.md "wikilink")，在台灣的中文譯名“魔影佳”，於1981年8月問世。初期機種採用3.5英寸的[軟磁碟作儲存媒體](../Page/軟磁碟.md "wikilink")，[解像度只有](../Page/解像度.md "wikilink")[VGA的](../Page/VGA.md "wikilink")640×480，比現今[照相手機的拍攝能力還要低](../Page/照相手機.md "wikilink")，但在當年來說已是很好的解像度。後來的[CD400](../Page/CD400.md "wikilink")、[CD-500已採用](../Page/CD-500.md "wikilink")8cm光碟為儲存媒介，在當時[記憶卡價格高昂的時代](../Page/記憶卡.md "wikilink")，獲得不錯的評價。「Mavica」取名於英語「Magnetic　Video　Camera」的縮寫，意思就是「採用磁性儲存的錄像裝置」。一般人以為Mavica系列只有數碼數機，但其實這個系列最早期的產品，是一部於1981年生產的錄像機。而這系列的攝錄機後來亦改用CD作儲存裝置，而非磁性記錄裝置。

首部數位化的Mavica使用3.5英寸的磁碟作為儲存媒體，此功能令它們在[北美大受歡迎](../Page/北美.md "wikilink")。而後來所推出的Mavica也改良了不少，像素的提高、使用[USB作為傳送介面和使用](../Page/USB.md "wikilink")[Memory
Stick以提高儲存容量](../Page/Memory_Stick.md "wikilink")。而後來使用8cm直徑（大小與3.5英寸磁碟相若）的迷你CD-R/CD-RW的Mavica在功能上更加有提升，如設有10倍光學變焦鏡頭。

隨著Memory
Stick記憶卡技術愈趨成熟，容量亦逐漸增加，相對使得採用CD-R/CD-DW的Mavica系列相機的體積看起來龐大許多。Mavica品牌最後的產品於2003年發表，之後市場上逐漸由[Cybershot系列來取代其在數位相機市場的位置](../Page/Cybershot.md "wikilink")。

## 規格

是以小磁碟代替軟片，攝影部使用[電荷耦合元件](../Page/電荷耦合元件.md "wikilink")（[CCD](../Page/CCD.md "wikilink")），周邊電路全部使用[積體電路](../Page/積體電路.md "wikilink")（[IC](../Page/IC.md "wikilink")）。

## Mavica系列產品

### 使用3.5"磁碟

  - MVC-FD5（1997年後旬至1998年上旬推出，固定焦距鏡頭）
  - MVC-FD7（1997年後旬至1998年上旬推出，10倍光學變焦鏡頭）
  - MVC-FD75（10倍光學變焦鏡頭）
  - MVC-FD73
  - MVC-FD71（1998年中旬推出，10倍光學變焦鏡頭）
  - MVC-FD51（1998年中旬推出，固定焦距鏡頭）
  - MVC-FD87
  - MVC-FD92
  - MVC-FD83
  - MVC-FD81
  - MVC-FD85
  - MVC-FD90
  - MVC-FD91（14倍光學變焦鏡頭）
  - MVC-FD92（8倍光學變焦，最大16倍變焦，首部同時支援3.5英寸磁碟與Memory Stick的Mavica相機）
  - MVC-FD88
  - MVC-FD95
  - MVC-FD97（10倍光學變焦鏡頭，4倍速電腦磁盤和[Memory
    Stick插槽](../Page/Memory_Stick.md "wikilink")，與MVC-CD1000相似）
  - MVC-FD100（磁碟與Memory Stick）
  - MVC-FD200（與上相同，但已有200萬像素，此機採用SONY自製鏡頭）

### 使用CD

  - MVC-CD200
  - MVC-CD250（此機採用SONY自製鏡頭）
  - MVC-CD300
  - MVC-CD350
  - MVC-CD400（首部相機使用紅外線激光對焦）
  - MVC-CD500（此機採用Carl Zeiss Vario-Sonnar鍍膜鏡頭）
  - MVC-CD1000（除了使用CD-R外和MVC-FD97相同）

## 參看

  - [Microdrive](../Page/Microdrive.md "wikilink")
  - [數碼相機列表](../Page/數碼相機列表.md "wikilink")

[Category:数码照相机](../Category/数码照相机.md "wikilink")
[Category:索尼數位相機](../Category/索尼數位相機.md "wikilink")