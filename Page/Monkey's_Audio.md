**Monkey's
Audio**，是一种常见的无损音訊压缩编码格式，[副檔名为](../Page/副檔名.md "wikilink").ape。与有损音訊压缩（如[MP3](../Page/MP3.md "wikilink")、[Ogg
Vorbis或者](../Page/Ogg_Vorbis.md "wikilink")[AAC等](../Page/AAC.md "wikilink")）不同的是，Monkey's
Audio压缩时不会丢失数据。一个压缩为Monkey's Audio的音訊文件听起来与原文件完全一样。Monkey's
Audio文件的播放列表使用.apl。

Monkey's Audio亦可指压缩／解压缩**Monkey's
Audio**文件的软件。因其主界面上有个[猴子图样而得名](../Page/猴子.md "wikilink")。Monkey's
Audio是压缩[ape格式的重要工具](../Page/ape.md "wikilink")；也可以对ape文件进行解压缩。安装该软件时可以选择是否向[winamp添加插件](../Page/winamp.md "wikilink")，使得winamp也可以播放ape文件。通常与Monkey's
Audio配合使用的软件有[Exact Audio
Copy](../Page/Exact_Audio_Copy.md "wikilink")（EAC）、[foobar2000等](../Page/foobar2000.md "wikilink")。

## 特点

Monkey's Audio是一种无损音訊压缩格式，而较之于其他无损音訊压缩格式，有长处亦有缺陷。

Monkey's
Audio压缩比高于其他常见的无损音訊压缩格式，约在55%上下，但编解码速度略慢。在搜寻回放位置时，如果文件压缩比过高，在配备较差的电脑会有延迟的现象。另外，由於它没有提供错误处理的功能，若发生文件损坏，损坏位置之后的数据有可能会丢失。

Monkey's
Audio是[開放原始碼的](../Page/開放原始碼.md "wikilink")[免費軟體](../Page/免費軟體.md "wikilink")，但因其授權協議並非[自由軟體而是准自由軟體](../Page/自由軟體.md "wikilink")（Semi-free
Software）而受到排擠。因为这意味着许多基于[GNU/Linux的](../Page/GNU/Linux.md "wikilink")[Linux发行套件或是其他只能基于](../Page/Linux发行套件.md "wikilink")[自由软件的](../Page/自由软件.md "wikilink")[作業系統不能将其收入](../Page/作業系統.md "wikilink")。较之其他使用更自由的许可证的无损音訊编码器（如FLAC），受其他软件的支持也更少。

因为Monkey's
Audio是一种无损压缩格式，所以不适于同有损压缩格式相比较——这两者有不同的目标和用途。无损压缩的目标是能够精确再现原文件的前提下将之压缩到尽可能小的体积。而有损压缩则是在丢失一部分信息的情况下，在用户指定的体积／比特率中尽可能保持接近原来的音质。

所以，无损压缩的音訊文件体积往往远大于从同样原文件压缩而来的有损压缩文件。例如：在压缩CD音訊时，一个典型的Monkey's
Audio文件往往有接近600～700K Bit/sec，而MP3最高不会超过320K
Bit/sec，一般情况下用户只会指定到128～192K
Bit/sec。虽然它们也可能达到可以接受的音質，但不会有Monkey's
Audio等无损压缩格式般逼真。

Hydrogenaudio的Wiki提供了一个Monkey's Audio与其它无损音訊压缩格式的全面比较。\[1\]

## 支援平台

目前官方只提供[Windows支援](../Page/Windows.md "wikilink")。虽然也有提供[GNU/Linux和](../Page/GNU/Linux.md "wikilink")[Macintosh平台的官方支援的讨论](../Page/Macintosh.md "wikilink")，但是没有结果。目前只有一位名为SuperMMX的开发者于2003年7月释出了一个[非官方移植版本](http://supermmx.org/linux/mac/)。它包括了供[XMMS与](../Page/XMMS.md "wikilink")[Beep
Media Player回放Monkey](../Page/Beep_Media_Player.md "wikilink")'s
Audio使用的插件。该移植本来只支援GNU/Linux，但从3.99 update 4 build 4版本开始支援[Mac OS
X和基于](../Page/Mac_OS_X.md "wikilink")[PowerPC](../Page/PowerPC.md "wikilink")、[SPARC平台的](../Page/SPARC.md "wikilink")[GNU/Linux](../Page/GNU/Linux.md "wikilink")。但是这个非官方移植计划没有得到官方的承认，受制于官方发行许可证的限制，其未來並不明朗。不过据称Monkey's
Audio的Win32库可以借助[Wine在GNU](../Page/Wine.md "wikilink")/Linux平台运行。

## 文件格式APE

**APE**是一种音訊[文件格式](../Page/文件格式.md "wikilink")，一般用.ape的[副檔名](../Page/副檔名.md "wikilink")，有时也采用.MAC的副檔名。APE格式採用[无损数据压缩](../Page/无损数据压缩.md "wikilink")，在不降低音质的前提下，能有限地压缩WAV音轨文件，压缩比率一般在55%左右。在音质上，相对于[WMA](../Page/WMA.md "wikilink")、[MP3](../Page/MP3.md "wikilink")、[AAC等](../Page/AAC.md "wikilink")[有损数据压缩的格式有着絕對的優勢](../Page/有损数据压缩.md "wikilink")。

APE文件结构是由**Monkey's Audio**定义的。Monkey's
Audio提供软件进行与其它音訊文件格式的转换。通过插件，APE文件可以在[foobar2000](../Page/foobar2000.md "wikilink")、[Nullsoft的](../Page/Nullsoft.md "wikilink")[Winamp和](../Page/Winamp.md "wikilink")[微软的](../Page/微软.md "wikilink")[媒体播放器等不同系统平台的多媒体软件中播放](../Page/媒体播放器.md "wikilink")，近来越来越多的便携式媒体播放器也较多的加入对APE文件的支援。

## 参见

  - [Apple Lossless](../Page/Apple_Lossless.md "wikilink")
  - [Exact Audio Copy](../Page/Exact_Audio_Copy.md "wikilink")（EAC）
  - [FLAC](../Page/FLAC.md "wikilink")
  - [MP3](../Page/MP3.md "wikilink")
  - [Meridian Lossless
    Packing](../Page/Meridian_Lossless_Packing.md "wikilink")
  - [TTA](../Page/TTA.md "wikilink")
  - [WAV](../Page/WAV.md "wikilink")
  - [WavPack](../Page/WavPack.md "wikilink")
  - [foobar2000](../Page/foobar2000.md "wikilink")
  - [文件格式列表](../Page/文件格式列表.md "wikilink")
  - [非破壞性資料壓縮](../Page/非破壞性資料壓縮.md "wikilink")
  - [音频编码格式的比较](../Page/音频编码格式的比较.md "wikilink")

## 註解

## 外部链接

  - [Monkey's Audio官方网站（英文）](http://www.monkeysaudio.com/)
  - [Sound Normalizer](http://www.kanssoftware.com/)

[Category:音频格式](../Category/音频格式.md "wikilink")
[Category:免費軟件](../Category/免費軟件.md "wikilink")
[Category:无损音频编解码器](../Category/无损音频编解码器.md "wikilink")
[Category:使用GStreamer的軟體](../Category/使用GStreamer的軟體.md "wikilink")

1.  [Lossless
    comparison](http://wiki.hydrogenaudio.org/index.php?title=Lossless_comparison)