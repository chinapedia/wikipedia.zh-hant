**亚历山大·詹姆斯·达拉斯**（**Alexander James Dallas**，1759年6月21日 -
1817年1月16日），[美国政治家](../Page/美国.md "wikilink")，[美国民主-共和党成员](../Page/美国民主-共和党.md "wikilink")，曾任[美国财政部长](../Page/美国财政部长.md "wikilink")（1814年-1816年）。

## 外部链接

  - [Biography and portrait at the University of
    Pennsylvania](https://web.archive.org/web/20060828022722/http://www.archives.upenn.edu/histy/features/1700s/people/dallas_alex.html)
  - [Alexander J.
    Dallas](http://www.findagrave.com/cgi-bin/fg.cgi?page=gr&GRid=6661789)

[D](../Category/1759年出生.md "wikilink")
[D](../Category/1817年逝世.md "wikilink")
[D](../Category/美国财政部长.md "wikilink")
[D](../Category/愛丁堡大學校友.md "wikilink")