**鹤山**（官方音譯：，传统外文：）是[中国](../Page/中华人民共和国.md "wikilink")[广东省](../Page/广东省.md "wikilink")[江門市下轄](../Page/江門市.md "wikilink")[縣級市](../Page/縣級市.md "wikilink")，位于广东省南部[珠江三角洲腹地](../Page/珠江三角洲.md "wikilink")，与[佛山市](../Page/佛山市.md "wikilink")[南海区](../Page/南海区.md "wikilink")、[顺德区隔](../Page/顺德区.md "wikilink")[西江相望](../Page/西江.md "wikilink")，因市内有山形似仙鹤而得名。鹤山的[地理坐标在](../Page/地理坐标.md "wikilink")[北纬](../Page/北纬.md "wikilink")22.29°\~22.52°，[东经](../Page/东经.md "wikilink")112.28°\~113.25°。

## 歷史

[清](../Page/清.md "wikilink")[雍正九年七月己丑](../Page/雍正.md "wikilink")（1731年8月30日）划新会，开平两地部分地区建立县制，因縣城北有山形似鶴而得名鶴山。1958年与[高明合併為高鶴縣](../Page/高明区.md "wikilink")。1981年12月7日撤銷高鶴縣，恢復高明和鶴山兩縣。1993年撤縣建县级市。

## 語言

鹤山市全境通行标准[粵語和](../Page/粵語.md "wikilink")[普通话](../Page/普通话.md "wikilink")，本地方言有鹤山话、[四邑方言](../Page/四邑方言.md "wikilink")、[客家話三种](../Page/客家話.md "wikilink")。从地域分布来看，北部龙口、桃源、沙坪、古劳一带通行鹤山话（沙坪话、古劳话），雅瑶一带通行四邑方言（雅瑶话），中部鹤城、共和等地通行客家话，

1980年以後外來人口大量增加，使得[四邑方言使用人口比例下降](../Page/四邑方言.md "wikilink")。造成不少家庭一家多语。今日一個鹤山[客家移民家庭的語言情況很可能是](../Page/客家.md "wikilink")：祖父母讲[客家話](../Page/客家話.md "wikilink")，父母兩人用[四邑方言對話](../Page/四邑方言.md "wikilink")，子女私下以[標準粤语交談](../Page/標準粤语.md "wikilink")，子女在校用[普通话溝通](../Page/普通话.md "wikilink")。

## 地理

鶴山市地處珠江三角洲西側，中部較高，東南低平，東部[西江沿岸為](../Page/西江.md "wikilink")[沖積平原](../Page/沖積平原.md "wikilink")。

### 氣候

1月份均溫12.9℃，七月均溫28.3℃，年均降水量1,800毫米。属于[熱帶季風氣候](../Page/熱帶季風氣候.md "wikilink")，境内具有[海洋气候特征](../Page/海洋气候.md "wikilink")。

## 行政区划

现辖：

  - 1街道：[沙坪街道](../Page/沙坪街道.md "wikilink")
  - 9镇:[龙口镇](../Page/龙口镇.md "wikilink") ·
    [雅瑶镇](../Page/雅瑶镇.md "wikilink") ·
    [古劳镇](../Page/古劳镇.md "wikilink") ·
    [桃源镇](../Page/桃源镇.md "wikilink") ·
    [鹤城镇](../Page/鹤城镇.md "wikilink") ·
    [共和镇](../Page/共和镇.md "wikilink") ·
    [址山镇](../Page/址山镇.md "wikilink") ·
    [宅梧镇](../Page/宅梧镇.md "wikilink") ·
    [双合镇](../Page/双合镇.md "wikilink")

## 資源

  - 礦藏：[稀土](../Page/稀土金属.md "wikilink")、[鉛](../Page/鉛.md "wikilink")、[鋅](../Page/鋅.md "wikilink")、[銅](../Page/銅.md "wikilink")、[磷](../Page/磷.md "wikilink")、[鉀](../Page/鉀.md "wikilink")、[長石](../Page/長石.md "wikilink")、[泥炭土等](../Page/泥炭土.md "wikilink")。
  - 林木：[勒竹](../Page/勒竹.md "wikilink")、[石竹](../Page/石竹.md "wikilink")、[馬尾松](../Page/馬尾松.md "wikilink")、[濕地松](../Page/濕地松.md "wikilink")、[杉](../Page/杉.md "wikilink")、[柏](../Page/柏.md "wikilink")、[格木等](../Page/格木.md "wikilink")。
  - 中藥材：[崩大碗](../Page/崩大碗.md "wikilink")、[了哥王](../Page/了哥王.md "wikilink")、[半枝蓮](../Page/半枝蓮.md "wikilink")、[金銀花等](../Page/金銀花.md "wikilink")。
  - 野生動物：[鷓鴣](../Page/鷓鴣.md "wikilink")、[斑鳩](../Page/斑鳩.md "wikilink")、[山狸](../Page/山狸.md "wikilink")、[果子狸](../Page/果子狸.md "wikilink")、[黃猄](../Page/黃猄.md "wikilink")、[白鼻仔](../Page/白鼻仔.md "wikilink")、[蠎蛇等](../Page/蟒蛇科.md "wikilink")。

## 經濟

鶴山市是廣東省商品糧生產基地。

  - 農產品：[稻](../Page/稻.md "wikilink")、[花生](../Page/花生.md "wikilink")、[茶葉](../Page/茶葉.md "wikilink")、[煙草](../Page/煙草.md "wikilink")、[甘蔗](../Page/甘蔗.md "wikilink")、[黃麻](../Page/黃麻.md "wikilink")、[水果](../Page/水果.md "wikilink")、[蠶桑等](../Page/蠶桑.md "wikilink")。
  - 工業：[紡織](../Page/紡織.md "wikilink")、[機械](../Page/機械.md "wikilink")、[製衣](../Page/製衣.md "wikilink")、[食品](../Page/食品.md "wikilink")、[建材](../Page/建材.md "wikilink")、[水泥](../Page/水泥.md "wikilink")、[造紙](../Page/造紙.md "wikilink")、[印刷](../Page/印刷.md "wikilink")、[造船](../Page/造船.md "wikilink")、[電子](../Page/電子.md "wikilink")、[化工等](../Page/化工.md "wikilink")。

2005年鶴山市生產總值87億7千萬元[人民幣](../Page/人民幣.md "wikilink")，人均生产总值19321人民币。

## 能源

綠湖沼氣電站

## 交通

[佛開高速公路](../Page/佛開高速公路.md "wikilink")、[325國道斜貫市境](../Page/325國道.md "wikilink")，[鶴山港為對外開放口岸](../Page/鶴山港.md "wikilink")。

## 旅遊

[大雁山](../Page/大雁山.md "wikilink")、[東坡亭](../Page/東坡亭.md "wikilink")、[鐵夫畫閣](../Page/鐵夫畫閣.md "wikilink")（畫家[李鐵夫的书阁](../Page/李鐵夫.md "wikilink")）、[大凹关帝庙](../Page/大凹关帝庙.md "wikilink")、霄乡古村、古劳水乡。

## 特產

[古勞](../Page/古勞.md "wikilink")[面豉](../Page/面豉.md "wikilink")、[南洞鹹脆](../Page/南洞鹹脆.md "wikilink")[花生](../Page/花生.md "wikilink")、[雲鄉和](../Page/雲鄉.md "wikilink")[鶴城](../Page/鶴城镇.md "wikilink")[腐竹](../Page/腐竹.md "wikilink")、[雙合](../Page/雙合.md "wikilink")[西瓜](../Page/西瓜.md "wikilink")、[址山](../Page/址山.md "wikilink")[蒜頭](../Page/蒜頭.md "wikilink")、大頂[苦瓜](../Page/苦瓜.md "wikilink")、古蠶[粉葛等](../Page/粉葛.md "wikilink")。

## 名人

古代：

  - 方獻夫，鶴山市坡山鄉方屋村人，明代弘治十八年（ 公元一五零五年）進士，任吏部員外郎，公元一五四四年逝世，遺著有《周易傳義約說》，

《 西樵遺稿》（ 八卷）（ 參考資料：《 鶴山文史》（ 人物專輯）一九八九年十月出版）

  - 李山官七，鶴山市祿洞中心村人，清初農民起義領袖，一六七九年被清軍燒死. （ 參考資料：\< 鹤山文史資料》（ 人物專輯）（
    一九八九年十月出版）

近現代人物：

  - 著名油畫家[李鐵夫](../Page/李鐵夫.md "wikilink")
  - [詠春拳師祖](../Page/詠春.md "wikilink")[梁贊](../Page/梁贊.md "wikilink")
  - [王老吉凉茶创始人](../Page/王老吉凉茶.md "wikilink")[王泽邦](../Page/王泽邦.md "wikilink")
  - [结构化学家](../Page/结构化学.md "wikilink")，[中国科学院院士](../Page/中国科学院院士.md "wikilink")[麦松威](../Page/麦松威.md "wikilink")
  - 民国时期电影女明星[胡蝶](../Page/胡蝶.md "wikilink")
  - [陸佑家族](../Page/陸佑.md "wikilink")（陸佑，后人包括[新加坡和](../Page/新加坡.md "wikilink")[香港电影制片人](../Page/香港.md "wikilink")[陆运涛](../Page/陆运涛.md "wikilink")）
  - 著名作曲家[宋军](../Page/宋军.md "wikilink")
  - 香港著名電影從業員、監製[黃百鳴](../Page/黃百鳴.md "wikilink")
  - 籃球運动員[易建联](../Page/易建联.md "wikilink")
  - 奥运会举重冠军[陈曉敏](../Page/陈曉敏.md "wikilink")
  - 香港[李佩材家族](../Page/李佩材家族.md "wikilink")
  - 李美珊，客家人，祖籍廣東省鶴山市鶴城鎮東坑村，

`  香港出生，八歲時移居美國，取得紐約大學生物系碩`
`   士學位，同時是一九八六年度香港小姐冠軍.`

## 教育

  - **高中**(职高)
      - [鹤山市第一中学](../Page/鹤山市第一中学.md "wikilink")
      - [鹤山市鹤华中学](../Page/鹤山市鹤华中学.md "wikilink")
      - [鹤山市纪元中学](../Page/鹤山市纪元中学.md "wikilink")
      - [鹤山市鶴城鎮三堡學校](../Page/鹤山市鶴城鎮三堡學校.md "wikilink")
      - [鶴山市址山中學](../Page/鶴山市址山中學.md "wikilink")
      - [鹤山市古劳中学](../Page/鹤山市古劳中学.md "wikilink")
  - **初中**
      - [鹤山市实验中学](../Page/鹤山市实验中学.md "wikilink")
      - [鹤山市沙坪中学](../Page/鹤山市沙坪中学.md "wikilink")
      - [鹤山市昆仑学校](../Page/鹤山市昆仑学校.md "wikilink")(民办)
      - [鹤山市碧桂园学校](../Page/鹤山市碧桂园学校.md "wikilink")(民办)
      - [鹤山市古劳中学](../Page/鹤山市古劳中学.md "wikilink")
      - [鹤山市第二中学](../Page/鹤山市第二中学.md "wikilink")
      - [鹤山市第三中学](../Page/鹤山市第三中学.md "wikilink")
      - [鹤山市雅瑶中学](../Page/鹤山市雅瑶中学.md "wikilink")
      - [鹤山市共和中学](../Page/鹤山市共和中学.md "wikilink")
      - [鹤山市桃源中学](../Page/鹤山市桃源中学.md "wikilink")
      - [鹤山市址山中学](../Page/鹤山市址山中学.md "wikilink")
      - [鹤山市龙口中学](../Page/鹤山市龙口中学.md "wikilink")

## 参见

  - [鶴山紅旗渠](../Page/鶴山紅旗渠.md "wikilink")

## 参考文献

## 外部链接

  - [鹤山在线](http://www.heshan1.com/)

[鹤山市](../Category/鹤山市.md "wikilink") [市](../Category/江门区市.md "wikilink")
[江门市](../Category/广东县级市.md "wikilink")