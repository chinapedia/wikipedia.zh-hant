**三省**，[中國古代的中央政權體系](../Page/中國.md "wikilink")，是[尚書省](../Page/尚書省.md "wikilink")、[門下省與](../Page/門下省.md "wikilink")[中書省三個單位的合稱](../Page/中書省.md "wikilink")。

受中國文化的影響，[朝鮮和](../Page/朝鮮.md "wikilink")[越南也曾設置三省](../Page/越南.md "wikilink")。

## 簡介

三省是中国从[隋朝和](../Page/隋朝.md "wikilink")[唐朝开始正式设立的中央政权体系](../Page/唐朝.md "wikilink")，所謂的三省分别是指：

  - [尚书省](../Page/尚书省.md "wikilink")：最高[行政机构](../Page/行政机构.md "wikilink")，负责执行国家的重要[政令](../Page/政令.md "wikilink")；
  - [门下省](../Page/门下省.md "wikilink")：审议机构，负责审核政令；
  - [中书省](../Page/中书省.md "wikilink")：决策机构，负责草拟和颁发[皇帝的](../Page/皇帝.md "wikilink")[诏令](../Page/诏.md "wikilink")。

中书省主要负责与皇帝讨论法案的起草，草拟皇帝诏令。门下省负责审查诏令内容，并根据情况退回给中书省。这两个部门是决策机构，通过审查的法令交由尚书省执行。尚书省下设有[六部](../Page/六部.md "wikilink")，分别为：

  - [吏部](../Page/吏部.md "wikilink")：负责考核、任免四品以下官员
  - [户部](../Page/户部.md "wikilink")：负责财政、国库
  - [礼部](../Page/礼部.md "wikilink")：负责[贡举](../Page/贡举.md "wikilink")、祭祀、典礼，控制全国学校、[科举](../Page/科举.md "wikilink")
  - [兵部](../Page/兵部.md "wikilink")：负责军事
  - [刑部](../Page/刑部.md "wikilink")：负责司法、审计事务。具体审判另有[大理寺负责](../Page/大理寺.md "wikilink")。重大案件组织刑部、[御史台](../Page/御史台.md "wikilink")、[大理寺会审](../Page/大理寺.md "wikilink")。谓之[三司审](../Page/三司审.md "wikilink")。
  - [工部](../Page/工部.md "wikilink")：负责工程建设、交通

中书省长官在[隋朝称为](../Page/隋.md "wikilink")[内史令](../Page/内史令.md "wikilink")，[唐朝称为](../Page/唐.md "wikilink")[中书令](../Page/中书令.md "wikilink")，副职称[中书侍郎](../Page/中书侍郎.md "wikilink")。门下省长官在隋朝称为[纳言](../Page/纳言.md "wikilink")，唐朝改叫[侍中](../Page/侍中.md "wikilink")，[门下侍郎副之](../Page/门下侍郎.md "wikilink")。尚书省长官称为[尚书令](../Page/尚书令.md "wikilink")，[尚书仆射副之](../Page/尚书仆射.md "wikilink")。
中书省内设[中书舍人若干](../Page/中书舍人.md "wikilink")，掌草拟诏命。门下省内设[给事中](../Page/给事中.md "wikilink")、[散骑常侍](../Page/散骑常侍.md "wikilink")、[谏议大夫](../Page/谏议大夫.md "wikilink")、[起居郎](../Page/起居郎.md "wikilink")、[拾遗等官](../Page/拾遗.md "wikilink")，掌规[谏](../Page/谏.md "wikilink")。尚书省下设左右[丞](../Page/丞.md "wikilink")，分管六部。六部的长官都称为[尚书](../Page/尚书.md "wikilink")。

**三省**之中，尚书令、仆射位高，为人臣之极而无实权。实际行使相权的是中书、门下两省大臣。

## 三省的历史演变

### 秦朝

[秦朝在](../Page/秦.md "wikilink")[少府下设尚书](../Page/少府.md "wikilink")，主管典籍，为後世尚书省的发端。

### 汉朝

[汉武帝夺外朝](../Page/汉武帝.md "wikilink")（以丞相为主的正式的政府机关）权，设立以[大将军为首的](../Page/大将军.md "wikilink")[内朝](../Page/内朝.md "wikilink")，专门处理军国大事。以原属[少府的](../Page/少府.md "wikilink")[尚书以及](../Page/尚书_\(官职\).md "wikilink")[中书谒者来转达表章](../Page/中书谒者.md "wikilink")，沟通内外朝，为后世[尚书省的发端](../Page/尚书省.md "wikilink")。

[东汉时设置](../Page/东汉.md "wikilink")[尚书台](../Page/尚书台.md "wikilink")，统领百官政事，长官称[尚书令](../Page/尚书令.md "wikilink")。但是此时的[尚书台在体制上仍然属于](../Page/尚书台.md "wikilink")[少府序列](../Page/少府.md "wikilink")，官品低微，所以另派[大将军](../Page/大将军.md "wikilink")、[大司马](../Page/大司马.md "wikilink")、[太傅以](../Page/太傅.md "wikilink")[录尚书事的名义秉政](../Page/录尚书事.md "wikilink")。

东汉末设[侍中寺](../Page/侍中寺.md "wikilink")，为皇帝的侍从、顾问机构。置侍中、散骑常侍等官，多以卓有学识的士人充任，为一时清选。

### 魏晋南北朝

[三国](../Page/三国.md "wikilink")[曹操自任](../Page/曹操.md "wikilink")[丞相](../Page/丞相.md "wikilink")，移原隶少府的尚书吏部曹、选部曹等尚书诸曹为丞相属官，将汉武帝建立的内朝转为外朝官署，这是尚书省建立的开端。

按：东汉时虽有尚书台、已经成为处理政务的主要机关，但毕竟是隶属于内廷的官署，不能说是国家的正式政务机关。

[曹丕以尚书诸曹权力过大](../Page/曹丕.md "wikilink")，遂以[曹操设立的专门处理机密文书的](../Page/曹操.md "wikilink")[秘书省为基础另设中书省](../Page/秘书省.md "wikilink")，掌管机要，起草和发布诏令。置中书监、令各一人，以中书监为长官。

南北朝时侍中、散骑常侍的权力逐渐扩大。北朝则政出门下，成为中央政治机构的重心。[晋代](../Page/晋.md "wikilink")，正式有门下省的建制。

### 隋

隋朝建立后，将[北周仿照](../Page/北周.md "wikilink")[周礼设立的官爵制度](../Page/周礼.md "wikilink")(六官之制)废除，设立了以[三省六部制为主体的中央官僚体系](../Page/三省六部制.md "wikilink")。隋设尚书、门下、内史、秘书、内侍五省，[秘书省类似于后来的](../Page/秘书省.md "wikilink")[馆阁](../Page/馆阁.md "wikilink")，[内侍省则是一个专门的宦官机构](../Page/内侍省.md "wikilink")，主持中央政权的就是尚书、门下、内史三省。三省互相牵制，共同向皇帝负责。其中，决策者为[内史省](../Page/内史省.md "wikilink")，长官称[内史令](../Page/内史令.md "wikilink")；审议者是门下省，长官称[纳言](../Page/纳言.md "wikilink")；处理日常政务的机构是尚书省、置[尚书令](../Page/尚书令.md "wikilink")、左右[仆射各一人](../Page/仆射.md "wikilink")，下设吏、礼、兵、[度支](../Page/度支.md "wikilink")（后改称[民部](../Page/民部.md "wikilink")）、[都官](../Page/都官.md "wikilink")（后改称[刑部](../Page/刑部.md "wikilink")）、工六部。
隋朝，三省长官均为相职，同时，[三公](../Page/三公.md "wikilink")、[三师也参预朝政](../Page/三师.md "wikilink")，也是[宰相](../Page/宰相.md "wikilink")。

### 唐

唐代三省制的特点是在建立不久就向二省、一省转变。这种变化的动因在於皇权对於相权的控制、以及提高行政效率。

首先为了抑制相权，皇帝逐渐使用一些资历较轻的官员参预朝政，实际行使宰相的权力，但是由於没有正宰相崇高的職銜，所以便易於控制。渐渐的，中书令、侍中、尚书令以及左右僕射这些宰相职务已经变成了一个崇高的虚衔，而真正的宰相却成为一种临时性质的职务，这符合从[汉代开始的相权不断下降的规律](../Page/汉.md "wikilink")。

唐代以三省首长“品位既崇，不欲轻以授人，故常以他官居宰相职，而假以他名。”（《[新唐书](../Page/新唐书.md "wikilink")》卷46《百官志》）主要有“[平章事](../Page/平章事.md "wikilink")”和“[同中书门下三品](../Page/同中书门下三品.md "wikilink")”等。唐太宗[贞观八年](../Page/贞观.md "wikilink")，僕射[李靖因病辞去宰相职务](../Page/李靖.md "wikilink")，太宗不同意，要求他“疾小瘳，三两日一至[中书门下平章事](../Page/中书门下平章事.md "wikilink")。”“[平章事](../Page/平章事.md "wikilink")”之名始於此。[唐高宗](../Page/唐高宗.md "wikilink")[永淳元年](../Page/永淳.md "wikilink")，始以某官（[黄门侍郎郭待举](../Page/黄门侍郎.md "wikilink")、兵部[侍郎岑长倩](../Page/侍郎.md "wikilink")）带“同中书门下平章事”衔者为宰相。[长兴四年为避讳](../Page/长兴.md "wikilink")（慕容延钊父名章），曾改为“[同中书门下二品](../Page/同中书门下二品.md "wikilink")”，因为尚书僕射是[职事官从二品](../Page/职事官.md "wikilink")。[贞观十七年](../Page/贞观.md "wikilink")，[萧瑀](../Page/萧瑀.md "wikilink")、[李勣并](../Page/李勣.md "wikilink")“同中书门下三品”，因为[侍中](../Page/侍中.md "wikilink")、[中书令是正三品](../Page/中书令.md "wikilink")，“同中书门下三品”之名始於此。高宗以後，宰相必须加“同中书门下三品”之衔，否则即使担任中书令也不能称为宰相，品位高者亦如此（有三公、三师头衔的除外）。

其次，三省合署议事、办公，三省职能逐渐趋向混同合一。

三省分权，势必造成相互牽-{制}-、效率低下等弊端。为了三省之间协调行动，三省首长定期在门下省的[政事堂议事](../Page/政事堂.md "wikilink")。自[武德年间开始](../Page/武德.md "wikilink")，中书、门下集议於政事堂，政事堂设於门下省。（《[通典](../Page/通典.md "wikilink")》：“旧制，宰相常於门下省议事，谓之政事堂。”）。唐高宗永淳年间，“[裴炎自侍中](../Page/裴炎.md "wikilink")[迁中书令](../Page/迁.md "wikilink")，乃徙政事堂於中书省。”由此，确立了中书省的中心地位。[开元十一年](../Page/开元.md "wikilink")，中书令[张说奏改政事堂为](../Page/张说.md "wikilink")[中书门下](../Page/中书门下.md "wikilink")，[政事堂印也改为](../Page/政事堂印.md "wikilink")[中书门下印](../Page/中书门下印.md "wikilink")，且於其後分列吏、枢机、兵、户、刑礼[五房](../Page/五房.md "wikilink")。从此，中书门下正式成为宰相的办事机构。

尚书省在[武則天時代一度改称文昌台](../Page/武則天.md "wikilink")、都台、中台，旋复旧称。

中书省在武則天時代一度改称西台、凤阁、紫微省，旋复旧称。

门下省在武則天時代一度改称东台、鸾台、黄门省等，旋复旧称。

### 五代

\-{制}-度与唐无异，但是由於连年-{征}-战，实际上宰相並不掌握权力。三省形同虚设。政事多由皇帝特派[使臣办理](../Page/使臣.md "wikilink")，成为[宋代以](../Page/宋.md "wikilink")[差遣特使为](../Page/差遣.md "wikilink")[正官的开始](../Page/正官.md "wikilink")。

### 宋

有宋一代，虽然三省名义始终存在，但是已经混同为一省。同时由于[枢密院](../Page/枢密院.md "wikilink")、[三司的设立](../Page/三司.md "wikilink")，宰相的军权、财权被剥夺。

宋代官制“有[官](../Page/官.md "wikilink")、有[职](../Page/职.md "wikilink")、有[差遣](../Page/差遣.md "wikilink")：官以寓[禄](../Page/禄.md "wikilink")[秩](../Page/秩.md "wikilink")、敘位著，[职以待](../Page/职.md "wikilink")[文学之选](../Page/文学.md "wikilink")，而别为[差遣以治内外之事](../Page/差遣.md "wikilink")。其次又有[阶](../Page/散官.md "wikilink")、有[勋](../Page/勳官.md "wikilink")、有[爵](../Page/爵位.md "wikilink")。故士人以登[台阁](../Page/台阁.md "wikilink")、升[禁从为](../Page/禁从.md "wikilink")[显宦](../Page/显宦.md "wikilink")，而不以官之迟速为荣滞；以[差遣要剧为贵途](../Page/差遣.md "wikilink")，而不以阶、勋、爵邑有无为轻重。”（《[宋史](../Page/宋史.md "wikilink")》卷一六一《职官志》）。不但宰相为临时职务，天下无官不为临时职务，至于“非奉别[敕](../Page/敕.md "wikilink")，不得治本官事”。

按《[宋会要](../Page/宋会要.md "wikilink")·职官》“中书令，侍中，及丞郎以上至三师、同中书门下平章事，並为正宰相。”但实际上，有以侍中衔出任宰相职务的现象、却没有以中书令衔出任为宰相者，中书令与尚书令一样，都是荣誉头衔。其他皆以同中书门下平章事拜相。（《[古今源流至论](../Page/古今源流至论.md "wikilink")》後集卷二《三省》：“国初三省长官第为空名，惟侍中有真拜者。”）

宋代通常二相並任，较唐朝时少了许多。也有独相一人或三相並置的。三相並置时，以[昭文馆大学士为](../Page/昭文馆大学士.md "wikilink")[首相](../Page/首相.md "wikilink")，次相[监修国史](../Page/监修国史.md "wikilink")，末相兼[集贤殿大学士](../Page/集贤殿大学士.md "wikilink")。二相並任的，首相並兼昭文馆大学士、监修国史。

[神宗](../Page/神宗.md "wikilink")[元豐改制时](../Page/元豐改制.md "wikilink")，尽废差遣职。以三省长官任宰相，诸官均以本官治事。以尚书令、中书令、侍中，官高不便轻授。遂以尚书左、右僕射为宰相。左僕射例兼门下侍郎，为门下省长官；右僕射例兼中书侍郎，为中书省长官。

徽宗[政和二年](../Page/政和.md "wikilink")，废尚书令，改侍中为[左辅](../Page/左辅.md "wikilink")、中书令为[右弼](../Page/右弼.md "wikilink")，皆虚其位；並改左僕射为[太宰](../Page/太宰.md "wikilink")，右僕射为[少宰](../Page/少宰.md "wikilink")，仍兼中书、门下两省[侍郎](../Page/侍郎.md "wikilink")。[靖康元年](../Page/靖康.md "wikilink")，复以尚书左、右僕射为宰相，三省长官名称皆依[元豐官制](../Page/元豐官制.md "wikilink")。

[南渡後](../Page/南渡.md "wikilink")，凡事力图太祖、太宗旧制。[建炎三年](../Page/建炎.md "wikilink")，尚书左、右僕射皆加同中书门下平章事，併中书与门下二省为中书门下。

[孝宗](../Page/孝宗.md "wikilink")[乾道八年](../Page/乾道.md "wikilink")，改尚书左、右僕射为左、右[丞相](../Page/丞相.md "wikilink")，废侍中、中书令、尚书令虚称，遂为定-{制}-。

### 元

[元代以中书省总领百官](../Page/元.md "wikilink")，与枢密院、[御史臺分掌政](../Page/御史臺.md "wikilink")、军、监察三权，尚书省时置时废、门下省不复置，故中书省较前代尤为重要。中書省下亦再設[行中書省](../Page/行中書省.md "wikilink")，以中央政府名義全權管治地方行政，「行中書省」概念其後發展成為[省級](../Page/省.md "wikilink")[地方政府](../Page/地方政府.md "wikilink")。

### 明

[明初不设中书令](../Page/明.md "wikilink")，仍以中书省统六部，长官称左右丞相。[太祖](../Page/明太祖.md "wikilink")[洪武十三年](../Page/洪武.md "wikilink")，废中书省与丞相，六部直接对皇帝负责。至此，三省六部-{制}-的三省彻底废止。

### 清

[清沿明制](../Page/清.md "wikilink")，以六部尚书任天下事，其上先后有[议政王大臣会议](../Page/议政王大臣会议.md "wikilink")、[南书房](../Page/南书房.md "wikilink")、[军机处](../Page/军机处.md "wikilink")。

## 注释

## 参考文献

## 参见

  - [六部](../Page/六部.md "wikilink")

{{-}}

[Category:三省六部](../Category/三省六部.md "wikilink")
[Category:中国古代官制](../Category/中国古代官制.md "wikilink")
[Category:朝鮮古代官制](../Category/朝鮮古代官制.md "wikilink")
[Category:越南古代官制](../Category/越南古代官制.md "wikilink")