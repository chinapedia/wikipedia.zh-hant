**T55E1自走砲**（T55E1 Motor
Carriage）是[美國](../Page/美國.md "wikilink")在1943年為[美國陸軍研制試驗型裝甲車輛](../Page/美國陸軍.md "wikilink")。

T55E1裝有4軸共8個大型驅動車輪、兩個卡迪拉克8汽缸水冷引擎，武器為3寸固定式主炮及[12.7×99毫米 NATO（.50
BMG）的](../Page/12.7×99_NATO.md "wikilink")[白朗寧M2重機槍](../Page/白朗寧M2重機槍.md "wikilink")，但由於當時主要生產反坦克車輛，T55E1計劃最終取消。

## 参考文献

<div class="references-small">

<references />

  - WarWheels [WarWheels-'T55E1 3-Inch 8x8 Motor
    Carriage](http://www.warwheels.net/T55E1MotorCarriageINDEX.html)
  - Duncan Crow and Robert J. Icks *Encyclopedia of Armoured Cars*
  - Haugh, David R. *Searching for Perfection: An Encyclopaedia of U.S.
    Army T-Series Vehicle Development (1925-1958)*

</div>

[Category:二戰美國裝甲戰鬥車輛](../Category/二戰美國裝甲戰鬥車輛.md "wikilink")
[Category:自走砲](../Category/自走砲.md "wikilink")