**大洲岛**，是[中國](../Page/中國.md "wikilink")[海南島](../Page/海南島.md "wikilink")[東部的一個](../Page/東.md "wikilink")[島嶼](../Page/島嶼.md "wikilink")，属[万宁市](../Page/万宁市.md "wikilink")。由于大洲島[金丝燕群栖营巢之地](../Page/金丝燕.md "wikilink")，出產[燕窩](../Page/燕窩.md "wikilink")，故又名為**燕窝岛**。岛嶼[面積約](../Page/面積.md "wikilink")1[平方公里](../Page/平方公里.md "wikilink")，離海南島東部的[東澳鄉最近距離為](../Page/東澳鄉.md "wikilink")2[海里](../Page/海里.md "wikilink")。

1996年，中国政府发布关于领海范围的声明，大洲岛有两个中国[领海基点](../Page/领海基点.md "wikilink")，分別是：

  - 大洲岛（1）北纬18ø 39·7' 东经110ø 29·6'
  - 大洲岛（2）北纬18ø 39·4' 东经110ø 29·1'

## 沿革

依史籍記載，大洲島產燕窩已有400年之久。隨著人類的采燕窝活動，大洲岛金丝燕目前濒临灭绝。1990年9月，万宁大洲岛成立海洋生态国家级自然保护区，重点保护金丝燕。1992年，保护区开始实施3年禁采燕窝的措施；直至1995年，恢复采摘燕窝。2002年仅仅采摘到2个燕窝，在2003年第三次进行为期3年的禁采。直到2006年，金丝燕种群的数量恢复並不明显，禁采燕窝措施仍继续推行。

## 燕窝洞

著名的燕窝洞在岛的东南侧，燕窝洞原為一块高约300余米之巨岩，沿花岗岩节理裂成两半。此缝隙约40厘米宽，从[水面直裂向岩頂](../Page/水.md "wikilink")，岩頂洞口狹小，从陆路難以入内，下面洞口則較大。采燕窝者會驾[船至洞口](../Page/船.md "wikilink")，然后潜入水洞，再攀登洞壁采集燕窝。

## 参考

<div class="references-small">

<references/>

</div>

## 另见

  - [双帆](../Page/双帆.md "wikilink")
  - [双帆石](../Page/双帆石.md "wikilink")

## 外部链接

  - [中国东海１０座领海基点石碑建成](http://news.xinhuanet.com/politics/2006-09/14/content_5090042.htm)

  - [中华人民共和国政府关于中华人民共和国领海基线的声明（1996年5月15日）](https://web.archive.org/web/20070216040721/http://www.soa.gov.cn/law/960515a.htm)

  - [Declaration of the Government of the People's Republic of China on
    the baselines of the territorial
    sea（May15th, 1996)](http://www.un.org/Depts/los/LEGISLATIONANDTREATIES/PDFFILES/CHN_1996_Declaration.pdf)

  - [在GOOGLEMAP查看大洲岛（1）位置](http://www.google.com/maps?f=q&hl=en&q=+18%C2%B039%2742.00%22N,110%C2%B029%2736.00%22E&ie=UTF8&z=12&ll=18.668039,110.493279&spn=0.144741,0.43396&t=k&om=1&iwloc=A)

  - [在GOOGLEMAP查看大洲岛（2）位置](http://www.google.com/maps?f=q&hl=en&q=+18%C2%B039%2724.00%22N,110%C2%B029%276.00%22E&ie=UTF8&z=12&ll=18.656654,110.485039&spn=0.14475,0.43396&t=k&om=1&iwloc=A)

  - [海南省民政厅受权公告500㎡以上海岛（礁）及海南岛沿岸岬角共108条地名标准化](https://web.archive.org/web/20070310185604/http://www.hainan.gov.cn/data/news/2006/08/16869/)

[Category:万宁市](../Category/万宁市.md "wikilink")
[Category:海南岛屿](../Category/海南岛屿.md "wikilink")
[琼](../Category/中国大陆领海基点.md "wikilink")