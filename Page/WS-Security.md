**WS-Security**（**Web服务安全**）是一种提供在[Web服务上应用安全的方法的](../Page/Web服务.md "wikilink")[网络传输协议](../Page/网络传输协议.md "wikilink")。2004年4月19日，[OASIS组织发布了WS](../Page/結構化資訊標準促進組織.md "wikilink")-Security标准的1.0版本。
2006年2月17日，发布了1.1版本。

WS-Security是最初[IBM](../Page/IBM.md "wikilink")，[微软](../Page/微软.md "wikilink")，[VeriSign和](../Page/VeriSign.md "wikilink")[Forum
Systems开发的](../Page/Forum_Systems.md "wikilink")，现在协议由Oasis-Open下的一个委员会开发，官方名称为WSS。

协议包含了关于如何在Web服务消息上保证完整性和机密性的规约。WSS协议包括[SAML](../Page/SAML.md "wikilink")（安全断言标记语言）、[Kerberos和认证证书格式](../Page/Kerberos.md "wikilink")（如[X.509](../Page/X.509.md "wikilink")）的使用的详细信息。

WS-Security描述了如何将签名和加密头加入SOAP消息。除此以外，还描述了如何在消息中加入[安全令牌](../Page/安全令牌.md "wikilink")，包括二进制安全令牌，如X.509认证证书和Kerberos门票（ticket）。

WS-Security将安全特性放入一个[SOAP消息的消息头中](../Page/SOAP.md "wikilink")，在[应用层处理](../Page/应用层.md "wikilink")。这样协议保证了端到端的安全。

WS主要是可利用HTTP，穿透防火墙。而Remoting可以利用TCP/IP，二进制传送提高效率。

## 相关规范

下面的规范草案与WS-Security有关。

  - [WS-SecureConversation](../Page/WS-SecureConversation.md "wikilink")
  - [WS-Federation](../Page/WS-Federation.md "wikilink")
  - [WS-Authorization](../Page/WS-Authorization.md "wikilink")
  - [WS-Policy](../Page/WS-Policy.md "wikilink")
  - [WS-Trust](../Page/WS-Trust.md "wikilink")
  - [WS-Privacy](../Page/WS-Privacy.md "wikilink")

## 参见

  - [WS-I Basic Security
    Profile](../Page/WS-I_Basic_Security_Profile.md "wikilink")
  - [Web服务](../Page/Web服务.md "wikilink")
  - [SAML](../Page/SAML.md "wikilink")
  - [XML firewall](../Page/XML_firewall.md "wikilink")
  - [XACML](../Page/XACML.md "wikilink")
  - [X.509](../Page/X.509.md "wikilink")

## 外部链接

  - [OASIS Web Services Security
    TC](http://www.oasis-open.org/committees/tc_home.php?wg_abbrev=wss)
    (Contains links to download specification documents)
  - [WS-Security
    Specification](http://www-128.ibm.com/developerworks/library/specification/ws-secure/)
  - [WS-I Basic Security
    Profile](http://www.ws-i.org/Profiles/BasicSecurityProfile-1.0.html)
  - [Web Services Security
    Documentation](http://www.cgisecurity.com/ws/)
  - [Web Service Security
    Patterns](http://msdn2.microsoft.com/en-us/library/aa480545.aspx)
  - [WSS4J](http://ws.apache.org/wss4j/) (WS-Security Java
    Implementation from Apache)
  - [WSIT](https://web.archive.org/web/20070711152141/https://wsit.dev.java.net/)
    Web Services Interoperability Technologies (WSIT) that enable
    interoperability between the Java platform and Windows Communication
    Foundation (WCF)

[Category:标准](../Category/标准.md "wikilink")
[Category:安全软件](../Category/安全软件.md "wikilink")
[Category:基于XML的标准](../Category/基于XML的标准.md "wikilink")
[Category:Web服务规范](../Category/Web服务规范.md "wikilink")