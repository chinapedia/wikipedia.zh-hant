[Institut_Pasteur,_Paris_1.jpg](https://zh.wikipedia.org/wiki/File:Institut_Pasteur,_Paris_1.jpg "fig:Institut_Pasteur,_Paris_1.jpg")
**巴斯德研究院**（）總部位於[巴黎](../Page/巴黎.md "wikilink")，是[法國的一個私立的非營利研究中心](../Page/法國.md "wikilink")，致力於[生物學](../Page/生物學.md "wikilink")、[微生物學](../Page/微生物學.md "wikilink")、[疾病和](../Page/疾病.md "wikilink")[疫苗的相關](../Page/疫苗.md "wikilink")[研究](../Page/研究.md "wikilink")，其創建者[路易·巴斯德於](../Page/路易·巴斯德.md "wikilink")1885年研發出第一劑[狂犬病疫苗](../Page/狂犬病.md "wikilink")。1887年此機構成立，於隔年獲國家認可而開始營運，此後巴斯德研究院對於傳染病的防治研究一直處於領先地位，1983年成為第一個成功分離出[人類免疫不全病毒的機構](../Page/人類免疫不全病毒.md "wikilink")，對於[白喉](../Page/白喉.md "wikilink")、[破傷風](../Page/破傷風.md "wikilink")、[結核](../Page/結核.md "wikilink")、[小兒麻痺](../Page/小兒麻痺.md "wikilink")、[流行性感冒](../Page/流行性感冒.md "wikilink")、[黃熱病和](../Page/黃熱病.md "wikilink")[鼠疫等疾病](../Page/鼠疫.md "wikilink")，也成就許多革命性的發現，自1908年起，共有八位科學家於此機構獲得[諾貝爾生理醫學獎](../Page/諾貝爾生理醫學獎.md "wikilink")。

## 参考文献

## 外部連結

  - [Institut Pasteur](http://www.pasteur.fr) 官方網站

## 参见

  - [美國疾病控制與預防中心](../Page/美國疾病控制與預防中心.md "wikilink")（CDC, Center for
    Disease Control and Prevention）

{{-}}

[Category:法國研究機構](../Category/法國研究機構.md "wikilink")