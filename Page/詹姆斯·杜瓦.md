**詹姆斯·杜瓦**（，），[蘇格蘭物理學家](../Page/蘇格蘭.md "wikilink")、化學家。他设计了[杜瓦瓶](../Page/杜瓦瓶.md "wikilink")，成功液化了氧气、氢气等多种气体，为[低温物理的研究提供了条件](../Page/低温物理.md "wikilink")。

## 早年

杜瓦出生于[苏格兰](../Page/苏格兰.md "wikilink")[法夫地区的](../Page/法夫.md "wikilink")[科因卡丁](../Page/科因卡丁.md "wikilink")(Kincardine)，这是位于[福斯湾北岸的一座小镇](../Page/福斯湾.md "wikilink")，在家裡是六兄弟中最年幼的一位，父母在他15歲時雙雙離世。他进入[道勒学院](../Page/道勒学院.md "wikilink")（Dollar
Academy)学习，后考入[爱丁堡大学](../Page/爱丁堡大学.md "wikilink")，在[莱昂·普莱费尔指导下进行研究](../Page/莱昂·普莱费尔.md "wikilink")，毕业之后成为普莱菲尔的助手，还曾到[根特大学](../Page/根特大学.md "wikilink")，在[凯库勒的指导下学习过](../Page/弗里德里希·奥古斯特·凯库勒·冯·斯特拉多尼茨.md "wikilink")。

## 科学研究

1867年詹姆斯·杜瓦提出了几种[苯](../Page/苯.md "wikilink")（化学式为C6H6）的结构设想，他认为最合理的结构就是凯库勒之前提出的。之后科学家发现了苯的一种同分异构体，把它以杜瓦命名，称作[杜瓦苯](../Page/杜瓦苯.md "wikilink")，有趣的是，杜瓦苯的结构并不在杜瓦所提出的几种结构中\[1\]。

1875年他成为[剑桥大学教授](../Page/剑桥大学.md "wikilink")，是[剑桥大学彼得学院的一员](../Page/剑桥大学彼得学院.md "wikilink")\[2\]。他和[乔治·道宁·利维因](../Page/乔治·道宁·利维因.md "wikilink")（George
Downing
Liveing）合作，通过光谱来分析物质的组成。期间与[诺曼·洛克尔就太阳的组成发生了争论](../Page/诺曼·洛克尔.md "wikilink")\[3\]。1877年他成为[英国皇家学会会员](../Page/英国皇家学会.md "wikilink")。

## 气体液化

[Dewar_James_flask.jpg](https://zh.wikipedia.org/wiki/File:Dewar_James_flask.jpg "fig:Dewar_James_flask.jpg")
他主要的工作是在[气体液化和低温物理的研究上](../Page/气体液化.md "wikilink")。1878年他在皇家学会介绍了Louis
Paul Cailletet和Raoul
Pictet的工作，在英国第一次引进了cailletet的通过焦耳-汤姆逊实验液化气体的装置。1880年左右他开始研究[液氧](../Page/氧.md "wikilink")，1884年他又在皇家学会介绍了Karol
Olszewski和Zygmunt Florenty
Wróblewski液化氧气的工作。1885年通过改进工艺他可以集齐1瓶液氧。1891年他提出了可以生产工业级产量液氧的生产过程。

1890年代初期，他奉命组建了皇家学会[戴维-法拉第实验室](../Page/戴维-法拉第实验室.md "wikilink")，继续自己液化[氢气的目标](../Page/氢气.md "wikilink")。1892年他发明了以他名字命名的真空绝热容器[杜瓦瓶用于低温现象的研究](../Page/杜瓦瓶.md "wikilink")，后被德国公司Thermos商业化为现在的[保温瓶](../Page/保温瓶.md "wikilink")。1899年他成功液化了氢气，并可以获得-260摄氏度的低温，为后人研究低温现象提供了条件。杜瓦曾尝试继续液化氦气，但由于所需的氦气量不够，使得[海克·卡末林·昂内斯首先完成对氦气的液化](../Page/海克·卡末林·昂内斯.md "wikilink")。

1904年他与[皮埃尔·居里一同研究镭衰变成氦的过程](../Page/皮埃尔·居里.md "wikilink")，同年被授予爵士。他与别人共同研究金属的电阻在零下200摄氏度到200摄氏度温度范围内的变化情况，预言接近绝对零度时，金属的电阻会变为零。1905年他发现把椰子壳烧成的[木炭冷却到零下](../Page/木炭.md "wikilink")185度时，木炭非常容易吸收空气，从而可以产生真空，这一技术在原子物理的实验中非常有用。他还和阿贝尔一起发展了[无烟火药](../Page/无烟火药.md "wikilink")。第一次世界大战期间，他发明的[无烟火药起到了很大作用](../Page/无烟火药.md "wikilink")，但是他的低温学研究却因为资金缺乏而终止，他开始转向气泡表面张力研究。1923年，他在[倫敦離世](../Page/倫敦.md "wikilink")，享年79歲。

## 荣誉与获奖

杜瓦曾多次获得诺贝尔奖提名，却未曾得奖。他曾获得过英国皇家学会的Rumford奖章、[戴维奖章和](../Page/戴维奖章.md "wikilink")[科普利奖章](../Page/科普利奖章.md "wikilink")。1904年他被封为爵士，并获得了法国科学院的拉瓦锡奖章，他是第一个获此荣誉的英国人。

## 参考资料

## 外部連結

  - 《[詹姆斯·杜瓦爵士
    (1842-1923)](https://web.archive.org/web/20050310092030/http://www.bbc.co.uk/history/historic_figures/dewar_james.shtml)》(英語).
    British Broadcasting Corporation, Broadcasting House, Portland
    Place, London. bbc.co.uk. 2004.
  - Bellis, Mary, "*[Inventors Sir James
    Dewar](http://inventors.about.com/library/inventors/blthermos.htm)*".
    about.com.

[D](../Category/1842年出生.md "wikilink")
[D](../Category/1923年逝世.md "wikilink")
[Category:苏格兰化学家](../Category/苏格兰化学家.md "wikilink")
[D](../Category/苏格兰物理学家.md "wikilink")
[D](../Category/發明家.md "wikilink")
[D](../Category/愛丁堡大學校友.md "wikilink")
[Category:马泰乌奇奖章获得者](../Category/马泰乌奇奖章获得者.md "wikilink")
[Category:戴维奖章](../Category/戴维奖章.md "wikilink")
[Category:科普利獎章獲得者](../Category/科普利獎章獲得者.md "wikilink")
[Category:拉姆福德奖章获得者](../Category/拉姆福德奖章获得者.md "wikilink")
[Category:富兰克林奖章获得者](../Category/富兰克林奖章获得者.md "wikilink")

1.  Baker and Rouvray, Journal of Chemical Education, 1978, vol. 55, p.
    645
2.
3.  <http://www.chemistryexplained.com/Co-Di/Dewar-James.html>