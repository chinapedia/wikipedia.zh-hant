[East_end_of_Jervois_Street.jpg](https://zh.wikipedia.org/wiki/File:East_end_of_Jervois_Street.jpg "fig:East_end_of_Jervois_Street.jpg")
[HK_Sheung_Wan_Jervois_Street_Old_Tea_Shop_3.JPG](https://zh.wikipedia.org/wiki/File:HK_Sheung_Wan_Jervois_Street_Old_Tea_Shop_3.JPG "fig:HK_Sheung_Wan_Jervois_Street_Old_Tea_Shop_3.JPG")[源吉林](../Page/源吉林.md "wikilink")\]\]
**蘇杭街**（**Jervois Street**），舊稱
**乍畏街**，是[香港](../Page/香港.md "wikilink")[香港島](../Page/香港島.md "wikilink")[中西區的一條](../Page/中西區_\(香港\).md "wikilink")[街道](../Page/街道.md "wikilink")，由西向東單向單程行車，位於[上環](../Page/上環.md "wikilink")[皇后大道中以北](../Page/皇后大道中.md "wikilink")，[德輔道中以南](../Page/德輔道中.md "wikilink")，[摩利臣街](../Page/摩利臣街.md "wikilink")[上環市政大廈以東](../Page/上環市政大廈.md "wikilink")，[中遠大廈及](../Page/中遠大廈.md "wikilink")[新紀元廣場以西](../Page/新紀元廣場.md "wikilink")。

## 歷史

[威廉·乍畏是一名](../Page/乍畏.md "wikilink")[英國](../Page/英國.md "wikilink")[將軍](../Page/將軍.md "wikilink")（William
Jervois）的名字，是[港督](../Page/港督.md "wikilink")[文咸時代的英軍司令](../Page/文咸.md "wikilink")。
之前，在1851年12月28日，[上環街市附近一帶](../Page/上環街市.md "wikilink")，發生一場大[火災](../Page/火災.md "wikilink")，有幾百間屋燒毀，30多人[遇害](../Page/死.md "wikilink")。
火災之後重建，由威廉‧乍畏總司令官領導，以災後廢料[填海取地](../Page/填海.md "wikilink")，開發了一條[街道](../Page/街道.md "wikilink")，自封其名「乍畏街」（Jervois音譯）。
不過，由於乍畏街多數[商店都是銷售](../Page/商店.md "wikilink")[蘇州](../Page/蘇州.md "wikilink")、[杭州的](../Page/杭州.md "wikilink")[布疋](../Page/布.md "wikilink")[絲綢及](../Page/絲綢.md "wikilink")[女性用品等](../Page/女性.md "wikilink")，布料商號接近80間，生意興隆，所以華人習慣稱之為「蘇杭街」。
當時，蘇杭街毗鄰跨境[碼頭](../Page/碼頭.md "wikilink")、[三角碼頭及](../Page/三角碼頭.md "wikilink")[煙花之地等](../Page/紅燈區.md "wikilink")，所以生意極盛，有「夜中環」之稱。
\[1\]\[2\]

## 交通

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{港島綫色彩}}">█</font>[港島綫](../Page/港島綫.md "wikilink")：[上環站](../Page/上環站.md "wikilink")([永樂街出口](../Page/永樂街.md "wikilink"))

## 近況

現在的蘇杭街是一條多樣化的街道。
路上有5層高的[唐樓](../Page/唐樓.md "wikilink")、10多層高的[洋樓](../Page/洋樓.md "wikilink")、20層高以上的[商業大廈](../Page/商業大廈.md "wikilink")，集處其中。
兩旁是臨街的商鋪，有布疋[批發商](../Page/批發.md "wikilink")、[小食店](../Page/小食店.md "wikilink")、[裕記](../Page/裕記.md "wikilink")[雜貨店](../Page/雜貨店.md "wikilink")、[凍肉店](../Page/凍肉.md "wikilink")、舊式[米鋪](../Page/米鋪.md "wikilink")、西式[花店](../Page/花店.md "wikilink")、[必勝客](../Page/必勝客.md "wikilink")、[廣告招牌工程公司](../Page/廣告招牌.md "wikilink")、[裝修物料](../Page/裝修.md "wikilink")[供應商](../Page/供應商.md "wikilink")、[潮州](../Page/潮州.md "wikilink")[食品](../Page/食品.md "wikilink")[外賣店](../Page/外賣店.md "wikilink")、[廢料回收商等](../Page/廢料回收商.md "wikilink")。

## 鄰近

  - [禧利街](../Page/禧利街.md "wikilink")
  - [急庇利街](../Page/急庇利街.md "wikilink")
  - [孖沙街](../Page/孖沙街.md "wikilink")
  - [文咸東街](../Page/文咸東街.md "wikilink")

## 相關

  - [香港規劃史](../Page/香港規劃史.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

  - [【文化籽】嫌譯名不吉
    乍畏街譯名蘇杭街](http://hk.apple.nextmedia.com/supplement/special/art/20150823/19266470)
    蘋果日報 (香港)，2015年8月23日

[Category:上環街道](../Category/上環街道.md "wikilink")
[Category:冠以人名的香港道路](../Category/冠以人名的香港道路.md "wikilink")

1.  [香港掌故街道名稱](http://hk.epochtimes.com/7/4/27/43840.htm)
2.  [鍾偉明](../Page/鍾偉明.md "wikilink")[《清晨爽利》今日想當年之「蘇杭街」。](http://www.rthk.org.hk/rthk/radio1/fresh_morning/20061220.html)