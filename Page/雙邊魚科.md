**雙邊魚科**為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鱸形目](../Page/鱸形目.md "wikilink")[鱸亞目的其中一個](../Page/鱸亞目.md "wikilink")[科](../Page/科.md "wikilink")。

## 分類

**雙邊魚科**下分8個屬，如下：

### 雙邊魚屬（*Ambassis*）

  - [阿氏雙邊魚](../Page/阿氏雙邊魚.md "wikilink")（*Ambassis agassizii*）
  - [帆鰭雙邊魚](../Page/帆鰭雙邊魚.md "wikilink")（*Ambassis agrammus*）
  - [康氏雙邊魚](../Page/康氏雙邊魚.md "wikilink")（*Ambassis ambassis*）
  - [彎線雙邊魚](../Page/彎線雙邊魚.md "wikilink")（*Ambassis buruensis*）
  - [印尼雙邊魚](../Page/印尼雙邊魚.md "wikilink")（*Ambassis buton*）
  - [杜氏雙邊魚](../Page/杜氏雙邊魚.md "wikilink")（*Ambassis dussumieri*）
  - [黃鰭雙邊魚](../Page/黃鰭雙邊魚.md "wikilink")（*Ambassis elongatus*）
  - [封氏雙邊魚](../Page/封氏雙邊魚.md "wikilink")（*Ambassis fontoynonti*）
  - [裸頭雙邊魚](../Page/裸頭雙邊魚.md "wikilink")（*Ambassis
    gymnocephalus*）：又稱[眶棘雙邊魚](../Page/眶棘雙邊魚.md "wikilink")。
  - [斷線雙邊魚](../Page/斷線雙邊魚.md "wikilink")（*Ambassis interrupta*）
  - [傑克遜雙邊魚](../Page/傑克遜雙邊魚.md "wikilink")（*Ambassis jacksoniensis*）
  - [古氏雙邊魚](../Page/古氏雙邊魚.md "wikilink")（*Ambassis kopsii*）
  - [麥氏雙邊魚](../Page/麥氏雙邊魚.md "wikilink")（*Ambassis macleayi*）
  - [大棘雙邊魚](../Page/大棘雙邊魚.md "wikilink")（*Ambassis macracanthus*）
  - [瑪麗雙邊魚](../Page/瑪麗雙邊魚.md "wikilink")（*Ambassis marianus*）
  - [少棘雙邊魚](../Page/少棘雙邊魚.md "wikilink")（*Ambassis miops*）
  - [貝紋雙邊魚](../Page/貝紋雙邊魚.md "wikilink")（*Ambassis nalua*）
  - [南非雙邊魚](../Page/南非雙邊魚.md "wikilink")（*Ambassis natalensis*）
  - [細尾雙邊魚](../Page/細尾雙邊魚.md "wikilink")（*Ambassis
    urotaenia*）：又稱[尾紋雙邊魚](../Page/尾紋雙邊魚.md "wikilink")。
  - [維氏雙邊魚](../Page/維氏雙邊魚.md "wikilink")（*Ambassis vachellii*）

### 玻璃魚屬（圓鱗鋸蓋魚屬）（*Chanda*）

  - [肩斑玻璃魚](../Page/肩斑玻璃魚.md "wikilink")（*Chanda nama*）

### 澳洲雙邊魚屬（*Denariusa*）

  - [澳洲雙邊魚](../Page/澳洲雙邊魚.md "wikilink")（*Denariusa australis*）

### 裸玻璃魚屬（*Gymnochanda*）

  - [長絲裸玻璃魚](../Page/長絲裸玻璃魚.md "wikilink")（*Gymnochanda filamentosa*）
  - [焰色裸玻璃魚](../Page/焰色裸玻璃魚.md "wikilink")（*Gymnochanda flamea*）
  - [利姆氏裸玻璃魚](../Page/利姆氏裸玻璃魚.md "wikilink")（*Gymnochanda limi*）
  - (*Gymnochanda ploegi*)
  - [真裸玻璃魚](../Page/真裸玻璃魚.md "wikilink")（*Gymnochanda verae*）

### 副高鱸屬（*Paradoxodacna*）

  - [副高鱸](../Page/副高鱸.md "wikilink")（*Paradoxodacna piratica*）

### 副雙邊魚屬（*Parambassis*）

  - [阿氏副雙邊魚](../Page/阿氏副雙邊魚.md "wikilink")（*Parambassis alleni*）
  - [高鰭副雙邊魚](../Page/高鰭副雙邊魚.md "wikilink")（*Parambassis altipinnis*）
  - [似天竺副雙邊魚](../Page/似天竺副雙邊魚.md "wikilink")（*Parambassis apogonoides*）
  - [莖副雙邊魚](../Page/莖副雙邊魚.md "wikilink")（*Pseudambassis
    baculis*）：又稱[莖擬雙邊魚](../Page/莖擬雙邊魚.md "wikilink")。
  - [新畿內亞副雙邊魚](../Page/新畿內亞副雙邊魚.md "wikilink")（*Parambassis confinis*）
  - [戴氏副雙邊魚](../Page/戴氏副雙邊魚.md "wikilink")（*Parambassis dayi*）
  - [格氏副雙邊魚](../Page/格氏副雙邊魚.md "wikilink")（*Parambassis gulliveri*）
  - [印度副雙邊魚](../Page/印度副雙邊魚.md "wikilink")（*Parambassis lala*）
  - [大鱗副雙邊魚](../Page/大鱗副雙邊魚.md "wikilink")（*Parambassis macrolepis*）
  - [高身副雙邊魚](../Page/高身副雙邊魚.md "wikilink")（*Parambassis pulcinella*）
  - [蘭副雙邊魚](../Page/蘭副雙邊魚.md "wikilink")（*Parambassis
    ranga*）：又稱[印度玻璃魚](../Page/印度玻璃魚.md "wikilink")。
  - [暹羅副雙邊魚](../Page/暹羅副雙邊魚.md "wikilink")（*Parambassis siamensis*）
  - [緬甸副雙邊魚](../Page/緬甸副雙邊魚.md "wikilink")（*Parambassis
    tenasserimensis*）
  - [托氏副雙邊魚](../Page/托氏副雙邊魚.md "wikilink")（*Parambassis thomassi*）
  - [伏氏副雙邊魚](../Page/伏氏副雙邊魚.md "wikilink")（*Parambassis vollmeri*）
  - [沃氏副雙邊魚](../Page/沃氏副雙邊魚.md "wikilink")（*Parambassis wolffii*）

### 擬雙邊魚屬（*Pseudambassis*）

  - [羅氏副雙邊魚](../Page/羅氏副雙邊魚.md "wikilink")（*Pseudambassis roberti*）

### 四點雙邊魚屬（*Tetracentrum*）

  - [似竺四點雙邊魚](../Page/似竺四點雙邊魚.md "wikilink")（*Tetracentrum apogonoides*）
  - [尾飾四點雙邊魚](../Page/尾飾四點雙邊魚.md "wikilink")（*Tetracentrum
    caudovittatus*）
  - [霍式四點雙邊魚](../Page/霍式四點雙邊魚.md "wikilink")（*Tetracentrum honessi*）

## 参考文献

  - [台灣魚類資料庫](http://fishdb.sinica.edu.tw/)

[\*](../Category/雙邊魚科.md "wikilink")