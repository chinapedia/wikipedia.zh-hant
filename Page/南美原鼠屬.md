**南美原鼠屬**（***Akodon***），[哺乳綱](../Page/哺乳綱.md "wikilink")、[囓齒目](../Page/囓齒目.md "wikilink")、[倉鼠科的一屬](../Page/倉鼠科.md "wikilink")，而與南美原鼠屬（奔原鼠）同科的動物尚有[纏尾鼠屬](../Page/纏尾鼠屬.md "wikilink")、阿根廷纏尾鼠屬（草地纏尾鼠）、隆鼠屬（哀隆鼠）、闊面倉鼠屬（闊面倉鼠）等之數種[哺乳動物](../Page/哺乳動物.md "wikilink")。

## 參考文獻

  - 中國科學院，《中國科學院動物研究所的世界動物名稱資料庫》，[1](https://web.archive.org/web/20050718232917/http://vzd.brim.ac.cn/division/fauna/index.asp)

[Category:南美原鼠屬](../Category/南美原鼠屬.md "wikilink")