**交易所交易基金、交易所買賣基金**（，常常縮寫為**ETF**，臺譯指數型證券投資信託基金），是一種在[證券交易所交易](../Page/證券交易所.md "wikilink")，提供投資人參與指數表現的[指數基金](../Page/指數基金.md "wikilink")。ETF將[指數](../Page/股價指數.md "wikilink")[證券化](../Page/资产证券化.md "wikilink")，投資人不以傳統方式直接進行一籃子證券之投資，而是透過持有表彰指數標的證券權益的受益憑證來間接投資。

ETF基金以持有與指數相同之[證券為主](../Page/證券.md "wikilink")，分割成眾多單價較低之投資單位，進而發行[受益憑證](../Page/受益憑證.md "wikilink")\[1\]。ETF商品將指數的價值由傳統的證券市場漲跌指標，轉變成具有流動性的資本證券，指數成分股票的管理由專業機構來進行，指數變動的損益直接反映在憑證價值的漲跌中。因此，只會因為連動指數成分股內容及權重改變而調整投資組合之內容或比重以符合「被動式管理」之目的\[2\]。

投資者可以通過兩種方式購買ETF：可以在[證券市場收盤之後](../Page/證券市場.md "wikilink")，按照當天的基金[净值向基金發行商購買](../Page/净值.md "wikilink")（同開放式[共同基金](../Page/共同基金.md "wikilink")）；也可以在證券市場上直接從其他投資者那裡購買（同封閉式共同基金），購買的價格由買賣雙方共同决定。透過實物申購與買回機制，ETF市價可以貼近淨值。

絕大多數ETF的指數成分是[股票](../Page/股票.md "wikilink")，但基於固定收益證券、[債券](../Page/債券.md "wikilink")、商品和[貨幣的ETF也在發展中](../Page/貨幣.md "wikilink")。對於投資者來說，ETF的[交易費用和管理費用都很低廉](../Page/交易費用.md "wikilink")，持股組合比較穩定，[風險往往比較分散](../Page/風險.md "wikilink")，而且[流動性很高](../Page/流動性.md "wikilink")，單筆投資便可獲得多元化投資效果，節省大量之時間及金錢。

到目前為止，幾乎所有的交易所都有ETF基金。

## 歷史

世界上最早的指數型ETF是1993年發行的[標準普爾500指数ETF](../Page/標準普爾500指数.md "wikilink")，此後，針對[道瓊](../Page/道瓊工業平均指數.md "wikilink")、[那斯達克](../Page/納斯達克100指數.md "wikilink")、[羅素](../Page/羅素指數.md "wikilink")、[威爾夏等指數的ETF紛紛問世](../Page/道瓊威爾夏5000綜合指數.md "wikilink")。

世界上最早的債券型ETF在2001年被編製出來，是由加拿大證券交易所推出的5年期政府公債基金。

## 產品分類

  - 指數股票型證券投資信託基金，這是最多的一種。
  - 指數債券型證券投資信託基金，俗稱債券型ETF。
  - 原物料ETF
  - 放空ETF

## 交易特性

ETF同时具備開放式基金的能够申購和贖回的特性和封閉式基金的交易特性，被認為是过去十幾年中最偉大的金融創新之一。

## 收益來源

ETF的收益來源可能有以下情況：

例如：在股票型ETF中，該被動式基金的成份股之現金與股票股利配息。如果基金有被證券監管當局開放借券，則也可以在市場進行博弈。當指數成分股及權重調整時，在被動式基金交易中的買賣價差金額，或能成為該基金的損益來源。

例如：在債券型ETF中，該類型的基金通常通過持有債券的固定利息回報作為基金收益，少部份的情況是透過信評機構對市場上的債券進行升評或降評，使的債券型ETF經理人或經理團隊可以透過公開市場操作來調整其債券持股，從中進而獲取交易利益或損失。

## 各地的基金產品

### ETF

  - DIAMONDS Trust（[道瓊工業指數基金](../Page/道瓊工業平均指數.md "wikilink")）\[3\]
  - PowerShares QQQ（[那斯達克100科技指數基金](../Page/納斯達克100指數.md "wikilink")）
  - SPDR S\&P 500 ETF（[標準普爾500指數基金](../Page/標準普爾500指數.md "wikilink")）
  - [稀土ETF](../Page/稀土.md "wikilink")\[4\]
  - 美國指數/ETF比較和簡介\[5\]\[6\]

### ETF

2010年為了活絡日本股市，日本央行（BOJ）計畫首度自市場「直接購買」追蹤[東證股價指數](../Page/東證股價指數.md "wikilink")（TOPIX）的ETF\[7\]。

### 指數股票型基金

2003年首檔ETF寶來台灣卓越50基金（現[元大台灣卓越50證券投資信託基金](../Page/元大台灣卓越50證券投資信託基金.md "wikilink")）（參卓[臺灣50指數](../Page/臺灣50指數.md "wikilink")，）掛牌[上市](../Page/上市.md "wikilink")。

| 年           | 年費 (SITCA [各項費用比率](http://www.sitca.org.tw/ROC/Industry/IN2211.aspx?pid=IN2222_01)) |
| ----------- | ----------------------------------------------------------------------------------- |
| 0050 元大台灣50 | 0057 FB摩台                                                                           |
| 2015        | 0.42%                                                                               |
| 2014        | 0.43%                                                                               |
| 2013        | 0.47%                                                                               |

### 交易所交易基金

  - 510230 国泰上证180金融ETF基金
  - 159911 鹏华深证民营ETF基金
  - 510220 华泰柏瑞中小盘ETF基金
  - 159909 招商深证TMT50ETF基金
  - 510170 国联安上证商品ETF基金
  - 510150 招商上证消费80ETF基金
  - 510130 易方达上证中盘ETF基金
  - 510210 富国上证综指ETF基金
  - 510030 华宝上证180价值ETF基金
  - 510110 海富通上证周期ETF基金
  - 510190 华安上证龙头ETF基金
  - 159902 华夏中小板ETF基金
  - 510880 华泰柏瑞红利ETF基金
  - 510120 海富通上证非周期ETF基金
  - 510050 华夏[上证50ETF基金](../Page/上证50ETF.md "wikilink")
  - 159916 建信深证60ETF基金
  - 510060 工银上证央企50ETF基金
  - 159903 南方深成ETF基金
  - 159913 交银深证300ETF基金
  - 159917 国泰中小板300ETF基金
  - 510070 鹏华上证民企50ETF基金
  - 510280 华宝上证180成长ETF基金
  - 159912 汇添富深证300ETF基金
  - 510290 南方上证380ETF基金
  - 159910 嘉实深证120ETF基金
  - 159907 广发中小板300ETF基金
  - 510090 建信上证责任ETF基金
  - 159908 博时深证200ETF基金
  - 159905 工银深证红利ETF基金
  - 159906 大成深证成长40ETF基金
  - 510260 诺安上证新兴ETF基金
  - 510270 中银上证国企100ETF基金
  - 159915 易方达创业板ETF基金
  - 510010 交银上证180治理ETF基金
  - 159901 易方达深证100ETF基金
  - 510180 华安上证180ETF基金
  - 510160 南方中证小康产业ETF基金
  - 510020 博时上证超大盘ETF基金
  - 510420 景顺长城上证180ETF基金
  - 159919 嘉实[沪深300ETF基金](../Page/沪深300.md "wikilink")
  - 159918 嘉实中创400ETF基金
  - 510410 博时上证自然资源ETF基金
  - 510300 300ETF基金\[8\]

### 交易所買賣基金

港交所ETF (2800-2848)|香港上市公司列表\#交易所ETF (3000-3087){{\!}}港交所ETF
(3000-3087)}}

  - [交易所買賣基金-證券名單](http://www.hkex.com.hk/Market-Data/Securities-Prices/Exchange-Traded-Products?sc_lang=zh-hk)

  - [盈富基金](../Page/盈富基金.md "wikilink")

  - [南方富時中國A50ETF](../Page/南方富時中國A50ETF.md "wikilink")

  - [沛富基金](../Page/沛富基金.md "wikilink")

  - [安碩A50中國基金](../Page/安碩A50中國基金.md "wikilink")

  - [恒生H股指數ETF](../Page/恒生H股指數ETF.md "wikilink")

  - [恒生指數ETF](../Page/恒生指數ETF.md "wikilink")

  - [價值黃金ETF](../Page/價值黃金ETF.md "wikilink")

  - [易方達中證100 A股指數ETF](../Page/易方達中證100.md "wikilink")

  - [華夏滬深300指數ETF](../Page/華夏滬深300指數ETF.md "wikilink")

## 跨地区ETF

2011年4月[臺灣證券交易所與](../Page/臺灣證券交易所.md "wikilink")[上海證券交易所合作爭取ETF互相掛牌](../Page/上海證券交易所.md "wikilink")，臺灣證券交易所、上海證券交易所、[深圳證券交易所與](../Page/深圳證券交易所.md "wikilink")[香港交易所更共同授權編製](../Page/香港交易所.md "wikilink")「中證兩岸三地500指數」，納編[臺灣海峽兩岸](../Page/臺灣海峽.md "wikilink")500檔個股\[9\]\[10\]。

## ETF联接基金

基金公司為了擴大ETF的銷售管道，開發了ETF联接基金，ETF联接基金本質上是開放式基金的一種，其申購贖回方式、管道與普通的開放式基金完全一樣。其特點在於，普通開放式基金和ETF的投資標的均為股票、固定收益類等資產，而ETF联接基金的大多數投資標的是ETF，即“投资ETF基金的基金”。

## 如何選擇ETF

1.  研究指數與成分股：指數編製方式、成分股選擇標準、收益分配計算方法。
2.  閱讀公開說明書：基本資料、基金簡介、受益人之權利及費用負擔、投資風險揭露。
3.  注意追蹤誤差：追蹤誤差是ETF報酬率與標的指數表現之間的差異，可檢視ETF追蹤指數的成效。
4.  注意流動性：流動性的高低可衡量投資人買賣的難易度。\[11\]\[12\]。

## 相關條目

  - [證券投資基金](../Page/證券投資基金.md "wikilink")

## 註釋

## 參考文獻

  - [美股ETF知识介绍](http://www.usgupiao.com/us-stock/about-etf/)，2015年7月6日

## 延伸閱讀

  - Carrell, Lawrence. *ETFs for the Long Run: What They Are, How They
    Work, and Simple Strategies for Successful Long-Term Investing*. JW
    Wiley, 2008. ISBN 978-0-470-13894-6
  - Ferri, Richard A. *The ETF Book: All You Need to Know About
    Exchange-Traded Funds*. Wiley, 2009. ISBN 0-470-53746-9
  - Humphries, William. *Leveraged ETFs: The Trojan Horse Has Passed the
    Margin-Rule Gates*. 34 Seattle U.L. Rev. 299 (2010), available at
    [1](http://lawpublications.seattleu.edu/sulr/vol34/iss1/8/).
  - Koesterich, Russ. *The ETF Strategist: Balancing Risk and Reward for
    Superior Returns*. Portfolio, 2008. ISBN 978-1-59184-207-1
  - Lemke, Thomas P; Lins, Gerald T. & McGuire, W. John. *Regulation of
    Exchange-Traded Funds*. Matthew Bender, 2015. ISBN 978-0-7698-9131-6

## 外部連結

  - [指數股票型基金（ETF）簡介](https://web.archive.org/web/20100318054009/http://www.twse.com.tw/ch/products/securities/ETF/introduce.php)

  - [ETF主題展](https://web.archive.org/web/20101228061212/http://www.twse.com.tw/ch/about/press_room/ETF_WEB/index.html)

  - [用ETF掌握世界脈動（上）](https://archive.is/20121223025023/http://www.fund.gov.tw/ct.asp?xItem=2752&ctNode=473&mp=1)[（下）](https://archive.is/20121222070742/http://www.fund.gov.tw/ct.asp?xItem=3074&ctNode=473&mp=1)

  - [交易所買賣基金名單及交易安排](http://www.hkex.com.hk/chi/etfrc/ETFTA/ETFTradingArrangement_c.htm)
    - [香港交易及結算所有限公司](../Page/香港交易及結算所有限公司.md "wikilink")

[Category:金融工程學](../Category/金融工程學.md "wikilink")
[Category:交易所交易產品](../Category/交易所交易產品.md "wikilink")
[Category:股市](../Category/股市.md "wikilink")
[Category:基金](../Category/基金.md "wikilink")
[Category:期貨](../Category/期貨.md "wikilink")
[Category:金融創新](../Category/金融創新.md "wikilink")

1.  [高價股當道
    買ETF搭上股王列車](http://mag.chinatimes.com/mag-cnt.aspx?artid=5714&page=1)
2.  [ETF像股票也像基金
    追蹤特定指數](http://www.libertytimes.com.tw/2010/new/nov/18/today-e24-2.htm)

3.  [美國掛牌ETF一覽表](http://www.ibts.com.tw/oversea/etf_03.htm)
4.  [美國首創稀土ETF周四起於NYSE掛牌交易](http://211.20.76.50/099/10/0991028/1020102812401D89D65A84D049D1AAE9C697FBF262DA43A3.htm)

5.  [美國指數/ETF比較和簡介](http://www.tradeoptions4living.com/%e5%b8%b8%e5%95%8f%e5%95%8f%e9%a1%8c/%e6%8c%87%e6%95%b8index-or-%e6%8c%87%e6%95%b8%e8%82%a1%e7%a5%a8%e5%9e%8b%e5%9f%ba%e9%87%91etf/)
6.  [美股ETF知识介绍](http://www.usgupiao.com/us-stock/about-etf/)
7.  [日央行傳將首度「直接」自市場購買ETF及J-REIT](http://www.funddj.com/KMDJ/News/NewsViewer.aspx?a=39d2a640-3eba-4b07-b556-98f58039025e)
8.  [天天基金网-ETF基金净值表](http://fund.eastmoney.com/ETF_dwjz.html)
9.  [兩岸證交所
    爭取ETF互相掛牌](http://money.chinatimes.com/news/news-content.aspx?id=20110422000963&cid=1208)
10. [兩岸三地500指數
    台灣成分股](http://money.udn.com/report/storypage.jsp?f_ART_ID=203270)

11. [懶人投資
    選ETF 3步搞定](http://money.udn.com/fund/storypage.jsp?f_MAIN_ID=420&f_SUB_ID=3097&f_ART_ID=231076)

12.