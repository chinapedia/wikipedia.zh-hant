**查理·亚历山大·维拉纽瓦**（，），生于[纽约市](../Page/纽约市.md "wikilink")[皇后区](../Page/皇后区.md "wikilink")，[美国籍多米尼加职业篮球运动员](../Page/美国.md "wikilink")，司职[大前锋](../Page/大前锋.md "wikilink")。

维拉纽瓦是第二代美国移民，父亲罗伯托·维拉纽瓦、母亲Doris
Mejia是从[多米尼加共和国移民到美国](../Page/多米尼加共和国.md "wikilink")，他生长于[皇后区随后迁往](../Page/皇后区.md "wikilink")[新泽西州](../Page/新泽西州.md "wikilink")，现居住在纽约[布鲁克林区](../Page/布鲁克林区.md "wikilink")。
6尺11寸（2.11米）的他在[2005年NBA选秀中第一轮第](../Page/2005年NBA选秀.md "wikilink")7顺位被[多伦多猛龙选中](../Page/多伦多猛龙.md "wikilink")。

## 高中时期

他在高中时期就读于布莱尔学院，与现役球星[罗尔·邓是校友](../Page/罗尔·邓.md "wikilink")，也同时高中篮球队的队长。高中时期获得过新泽西州篮球先生称号，维拉纽瓦最初准备参加[2003年NBA选秀](../Page/2003年NBA选秀.md "wikilink")，但中途退出，进入了[康涅狄格大学学习打球](../Page/康涅狄格大学.md "wikilink")。

## 大学生涯

维拉纽瓦最初口头承诺进入[伊利诺伊大学打球](../Page/伊利诺伊大学.md "wikilink")，但是大学主教练[比尔·塞尔夫转投](../Page/比尔·塞尔夫.md "wikilink")[堪萨斯大学](../Page/堪萨斯大学.md "wikilink")，他也随即取消了承诺，想要追随塞尔夫去堪萨斯大学，但是最终他还是选择加盟了NCAA劲旅康涅狄格大学。[1](http://www2.ljworld.com/news/2003/jun/20/villanueva_snubs_draft/)
在康涅狄格大学的首个赛季，维拉纽瓦入选NCAA大东区最佳新秀第二阵容，并且是康涅狄格大学夺取2004年NCAA总冠军球队的主要替补球员。在大学二年级，他以场均13.6分和8.3个篮板成功入选NCAA大东区全明星第二阵容。

2004年夏天，维拉纽瓦成为美国国家男子篮球21岁以下青年队成员，帮助球队夺取了世青赛的冠军。

## NBA生涯

[多伦多猛龙在](../Page/多伦多猛龙.md "wikilink")[2005年NBA选秀中摘下维拉纽瓦后受到了广泛的质疑](../Page/2005年NBA选秀.md "wikilink")\[1\]，当时他在新秀赛季的比赛中却有着很稳定可靠的表现，取得了场均13分和6.4个篮板，在2005-06赛季[NBA最佳新秀的投票竞选中名列联盟第二](../Page/NBA最佳新秀.md "wikilink")，\[2\]入选2005-06赛季NBA最佳新秀第一阵容。

在2006年3月26日，维拉纽瓦在一场116-125输给[密尔沃基雄鹿队的比赛中打出了自己职业生涯最高的单场](../Page/密尔沃基雄鹿队.md "wikilink")48分的数据，这也是猛龙队历史上的新秀得分纪录，这一纪录也是1996-97赛季[阿伦·艾弗森拿下](../Page/阿伦·艾弗森.md "wikilink")50分后的最高新秀得分。

2006年6月30日，维拉纽瓦被交换去[密尔沃基雄鹿以换取](../Page/密尔沃基雄鹿.md "wikilink")[T·J·福特和一些现金优惠](../Page/T·J·福特.md "wikilink")。

在2007年季后赛中，维拉纽瓦取得了场均12.6分、6.2个篮板、1.0个助攻和0.7次抢断。

2009年夏季，维拉诺瓦作为自由球员，和底特律活塞队签约。

## 个人

维拉纽瓦患有[全身性禿髮症](../Page/全身性禿髮症.md "wikilink")（[:en:Alopecia
universalis](../Page/:en:Alopecia_universalis.md "wikilink")），这种病症不会对身体产生其他伤害。维拉纽瓦成为美国全国秃头症基金会(National
Alopecia Areata Foundation)
的发言人，帮助一些和他患着同样疾病的人。\[3\]2006年3月，为了表彰他在公益方面的努力，NBA联盟授予了他2月份联盟公共协助奖。\[4\]

## 荣誉

  - 2003 - 新泽西州高中篮球先生，和罗尔·邓分享
  - 2003 - 入选新泽西州高中全明星队
  - 2003 - 麦当劳高中全明星阵容
  - 2004 - NCAA大东区（Big East Conference）最佳新秀阵容
  - 2004 - NCAA总冠军成员，康涅狄格大学Huskies篮球队
  - 2004 - USA青年队世青赛金牌成员
  - 2005 - NCAA大东区全明星第二阵容
  - 2005 - 多伦多猛龙队社区MVP奖
  - 2005 - NBA东部联盟11月最佳新秀
  - 2006 - 被选入2006年休斯敦NBA全明星周末[新秀挑战赛](../Page/NBA新秀挑战赛.md "wikilink")
  - 2006 - 2月份联盟公共协助奖
  - 2006 - 多伦多猛龙队新秀历史纪录：单场48分
  - 2006 - 多伦多猛龙队新秀历史纪录：单场18个篮板
  - 2006 -
    [NBA最佳新秀全联盟排名第二](../Page/NBA最佳新秀.md "wikilink")，排在[克里斯·保罗之后](../Page/克里斯·保罗.md "wikilink")
  - 2006 - NBA新秀第一阵容
  - 2007 - 被选入2006年拉斯维加斯NBA全明星周末新秀挑战赛（因伤缺席）

## 参考资料

## 外部链接

  - [Charlie Villanueva Official Myspace
    Page](http://www.myspace.com/cv31)

  - [ESPN Player
    Profile](http://sports.espn.go.com/nba/players/profile?statsId=3933)

  - [Yahoo\! Player Profile](http://sports.yahoo.com/nba/players/3933)

  - [SI.com Player
    Profile](http://sportsillustrated.cnn.com/basketball/nba/players/3933/)

  - [The Charlie Villanueva Foundation](http://www.tenisparaninos.org/)

  - [National Alopecia Areata Foundation,
    Spokesperson](http://www.naaf.org/)

  - [Charlie
    Inspires](https://web.archive.org/web/20070930210019/http://boss.streamos.com/real/nba/teams/raptors/charlie_newsworld_06.smi)

[Category:美国男子篮球运动员](../Category/美国男子篮球运动员.md "wikilink")
[Category:多米尼加男子篮球运动员](../Category/多米尼加男子篮球运动员.md "wikilink")
[Category:多伦多猛龙队球员](../Category/多伦多猛龙队球员.md "wikilink")
[Category:密尔沃基雄鹿队球员](../Category/密尔沃基雄鹿队球员.md "wikilink")
[Category:底特律活塞队球员](../Category/底特律活塞队球员.md "wikilink")
[Category:康乃狄克大學校友](../Category/康乃狄克大學校友.md "wikilink")
[Category:達拉斯小牛隊球員](../Category/達拉斯小牛隊球員.md "wikilink")

1.
2.
3.  [Charlie Villanueva
    Biography](http://www.naaf.org/Charlie/Charlie-Bio.asp) , National
    Alopecia Areata Foundation, accessed 2007年2月8日.
4.  <http://www.nba.com/raptors/news/pressrelease_CV31_commAssist_060313.html>