**AKABEiSOFT2**是位於[日本](../Page/日本.md "wikilink")[福岡縣](../Page/福岡縣.md "wikilink")[福岡市的](../Page/福岡市.md "wikilink")[成人遊戲製作公司](../Page/成人遊戲.md "wikilink")，旗下有、、、曉WORKS黑的品牌，和的姊妹品牌、、。

## 歷史

AKABEiSOFT2的前身是創作[同人誌及](../Page/同人誌.md "wikilink")[同人遊戲的團體](../Page/同人遊戲.md "wikilink")「**AKABEiSOFT**」（），作品多有《[校園迷糊大王](../Page/校園迷糊大王.md "wikilink")》的[戲仿](../Page/戲仿.md "wikilink")。2004年冬季，把[一次和](../Page/原創.md "wikilink")[二次創作物的發行分別分拆為](../Page/二次創作.md "wikilink")「**AKABEiSOFT**」和「****」，翌年把前者[法人化](../Page/法人.md "wikilink")，社名登記為「有限公司AKABEiSOFT2」，品牌名稱則為「（一樣的意思和讀法）」。伴隨法人化，同人團體的「AKABEiSOFT」合併至「有葉與愉快的夥伴們」，而負責原畫的有葉（）則自成個人團體，亦結束開發同人遊戲，同人創作只剩下同人誌方面。2007年，再設立****、**曉WORKS**兩品牌。社內公司株式會社エフォルダム旗下品牌エフォルダムソフト於2014年4月16日解散。\[1\]

## 作品列表

### 同人作品

  - 2004年6月20日 - アザゼル、アザゼル SPECIAL SOUND TRACK
  - 2004年9月19日 - 夜の破片

### 有葉與愉快的朋友們

  - 2004年12月30日 - SCHOOL×SCHOOL
      -
        改編自《校園迷糊大王》的十八禁[冒險遊戲](../Page/冒險遊戲.md "wikilink")，於[C67預先發售](../Page/Comic_Market.md "wikilink")，2005年1月20日委託部分書店發售。

### mixed up×AKABEiSOFT

  - 2005年1月21日 - （ア・プロフィール）

### SKILL UP

  - 2006年 - 妹 〜doublesite〜
  - 2006年 - 旋律の大攻勢MASTER(音樂CD)
  - 2007年 - えむ すくらんぶる
  - 2007年 - あなたの傍で笑っていたい

###

<table>
<thead>
<tr class="header">
<th><p>#</p></th>
<th><p>發行日期</p></th>
<th><p>原名</p></th>
<th><p>原畫</p></th>
<th><p>劇本</p></th>
<th><p>中文譯名</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p>2005年5月27日</p></td>
<td><p><strong></strong></p></td>
<td></td>
<td><p>鳴海、HOT☆ぢる、Ｉ</p></td>
<td><p>魂響</p></td>
</tr>
<tr class="even">
<td><p>1 Another Story</p></td>
<td><p>2005年7月8日</p></td>
<td><p><strong></strong></p></td>
<td><p>虛空北星、眼間成珠</p></td>
<td><p>魂響～凌辱side～</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p>2005年11月25日</p></td>
<td><p><strong></strong></p></td>
<td></td>
<td><p>車輪之國，向日葵的少女</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td><p>2006年3月24日</p></td>
<td><p><strong></strong></p></td>
<td><p>有葉、、こんぺいとう</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td><p>2006年9月29日</p></td>
<td><p>'''{{tsl|ja|こんな娘がいたら僕はもう…</p></td>
<td><p>}}'''</p></td>
<td><p>有葉</p></td>
<td><p>衣笠彰梧</p></td>
</tr>
<tr class="even">
<td><p>2 FD</p></td>
<td><p>2007年1月26日</p></td>
<td><p><strong></strong>[2]</p></td>
<td><p>るーすぼーい</p></td>
<td><p>車輪之國，悠久的少年少女</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1 逆移植版</p></td>
<td><p>2007年7月26日</p></td>
<td><p><strong></strong></p></td>
<td><p>あやね、遼闊早夫、霧生紗夜、衣笠彰梧</p></td>
<td><p>魂響～圓環的羈絆～</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>電腦桌面周邊集</p></td>
<td><p>2008年1月31日</p></td>
<td><p><strong></strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p>2008年5月29日</p></td>
<td><p><strong></strong></p></td>
<td><p>有葉</p></td>
<td><p>るーすぼーい</p></td>
<td><p>G弦上的魔王</p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p>2008年6月26日</p></td>
<td><p><strong></strong></p></td>
<td><p>有葉、、、憂姬はぐれ、淺美朝海、えきすぱあと、七海綾音、、吳マサヒロ、</p></td>
<td><p>千歲</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p>2009年3月26日</p></td>
<td><p><strong></strong></p></td>
<td><p>有葉、淺海朝美</p></td>
<td><p>安堂こたつ</p></td>
<td><p>W.L.O.世界戀愛機構</p></td>
</tr>
<tr class="even">
<td><p>7 FD</p></td>
<td><p>2009年9月24日</p></td>
<td><p><strong></strong>[3]</p></td>
<td><p>W.L.O.世界戀愛機構L.L.S. -LOVE LOVE SHOW-</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>8</p></td>
<td><p>2010年5月27日</p></td>
<td><p><strong></strong></p></td>
<td><p>有葉、ikki</p></td>
<td><p><a href="../Page/健速.md" title="wikilink">健速</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>9</p></td>
<td><p>2010年6月24日</p></td>
<td><p><strong></strong></p></td>
<td><p>有葉、憂姬はぐれ</p></td>
<td><p>澀谷ハヤト</p></td>
<td><p>光輪之町、薰衣草的少女</p></td>
</tr>
<tr class="odd">
<td><p>電腦桌面周邊集2</p></td>
<td><p>2010年9月23日</p></td>
<td><p><strong></strong></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>8 FD</p></td>
<td><p>2011年11月25日</p></td>
<td><p><strong></strong>[4]</p></td>
<td><p>有葉、ikki</p></td>
<td><p>健速</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>10</p></td>
<td><p>2015年2月27日</p></td>
<td><p><strong></strong></p></td>
<td><p>有葉</p></td>
<td><p>るーすぼーい</p></td>
<td><p>我一個人的戰爭</p></td>
</tr>
<tr class="even">
<td><p>11</p></td>
<td><p>2018年3月30日</p></td>
<td><p><strong></strong></p></td>
<td><p>おりょう</p></td>
<td><p>富岡征士郎</p></td>
<td><p>母性女友 -子宮 帰還篇-</p></td>
</tr>
<tr class="odd">
<td><p>12</p></td>
<td><p>2019年3月29日</p></td>
<td><p><strong></strong></p></td>
<td><p>有葉</p></td>
<td><p>泰良則充</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>13</p></td>
<td><p>2019年4月26日</p></td>
<td><p><strong></strong></p></td>
<td><p>おりょう</p></td>
<td></td>
<td><p>母性女友2 -知性 崩壊篇-</p></td>
</tr>
</tbody>
</table>

###

  - 2010年10月28日 -
  - 2011年7月29日 -

###

  - 2007年6月28日 -
  - 2008年3月27日 -
  - 2008年12月25日 -
  - 2010年4月22日 -

### しゃんぐりらすまーと

  - 2011年5月27日 -

### 曉WORKS

  - 2007年10月26日 -
  - 2008年6月26日 -
  - 2009年2月26日 - DEVILS DEVEL CONCEPT
  - 2009年10月22日 -
  - 2010年9月23日 -
  - 2012年3月30日 -
  - 2014年1月24日 -
  - 2014年3月28日 -
  - 2014年12月19日 -
  - 2017年1月27日 -
  - 2017年2月24日 -
  - 2018年11月30日 -

### 曉WORKS黑

  - 2008年9月25日 -
  - 2009年9月24日 -
  - 2011年10月28日 -
  - 2011年11月25日 -
  - 2011年12月22日 -

### hibiki works（旧名：曉WORKS響）

  - 2009年11月26日 -
  - 2011年6月24日 - [LOVELY×CATION](../Page/LOVELY×CATION.md "wikilink")
  - 2013年4月26日 - [LOVELY×CATION2](../Page/LOVELY×CATION2.md "wikilink")
  - 2014年4月25日 - [PRETTY×CATION](../Page/PRETTY×CATION.md "wikilink")
  - 2015年4月24日 - [PRETTY×CATION2](../Page/PRETTY×CATION2.md "wikilink")
  - 2016年4月29日 - [PURELY×CATION](../Page/PURELY×CATION.md "wikilink")
  - 2017年4月28日 -
    [新妻LOVELY×CATION](../Page/新妻LOVELY×CATION.md "wikilink")
  - 2017年12月22日 -
  - 2018年4月27日 -

### WHEEL

  - 2009年6月25日 -
  - 2010年2月25日 -
  - 2011年3月31日 -
  - 2012年8月30日 -

### あかべぇめでぃあ

  - 2009年1月9日 - G線上の魔王 サウンドドラマ -償いの章- 第一巻
  - 2009年6月26日 - G線上の魔王 サウンドドラマ -償いの章- 第二巻

### てぃ〜ぐる

  - 2013年5月31日 -
  - 2014年6月27日 -
  - 2015年7月24日 -
  - 2016年10月28日 -
  - 2017年預定 -

### スペルマニアックス

  - 2011年10月28日 -

### あかべぇそふとすりぃ

  - 2012年6月29日 -
  - 2012年7月27日 -
  - 2014年2月28日 -
  - 2014年8月29日 - [できない私が、くり返す。](../Page/できない私が、くり返す。.md "wikilink")
  - 2015年9月18日 -
  - 2015年12月11日 -
  - 2016年1月29日 -
  - 2016年3月25日 -
  - 2016年6月24日 -
  - 2016年8月26日 -
  - 2016年10月28日 -
  - 2016年11月25日 -
  - 2016年12月22日 -
  - 2017年3月24日 -
  - 2017年8月25日 -
  - 2017年11月24日 -
  - 2018年1月26日 -
  - 2018年2月23日 -
  - 2018年3月30日 -
  - 2018年7月27日 -
  - 2018年9月28日 -
  - 2018年10月26日 -

### COSMIC CUTE

  - 2013年3月22日 -
  - 2013年7月26日 -
  - 2015年5月29日 - love,VAMPIRE FLOWERS
  - 2016年10月28日 -

### CollaborationS

  - 發售日未定 - 太陽の子

### NostalgicChord

  - 2014年5月30日 -
  - 2015年2月27日 -

### CD

  - 2007年2月23日 -
      -
        由[Five
        Records的精選CD](../Page/5pb..md "wikilink")，但AKABEiSOFT2沒有在自己的網站作出任何告知。
  - 2008年8月15日 -
      -
        由ArielWave發售角色歌
  - 2009年4月24日 -
      -
        由Five Records發售[原聲音樂](../Page/原聲音樂.md "wikilink")

## 主要的工作人員

  - （劇本）

  - （劇本）

  - （劇本）

  - （）（人物設計、原畫）

  - （原畫）

  - 司城憲幸/憲yuki（程式編寫）

  - 春山学（製作人・導演）

  - （漫畫）

  - （原畫）

  - （導演）

  - （導演）

  - （導演）

  - （劇本）

## 參考資料

<references />

## 外部連結

**需年齡確認**

  - [官方網站](https://web.archive.org/web/20090228211628/http://www.akabeesoft2.com/)　
  - [官方網站](http://www.syangrila.com/)
  - [暁WORKS :: WEB
    SITE](https://web.archive.org/web/20080525070915/http://www.akatsukiworks.com/)
  - [同人團體「有葉與愉快的夥伴們HP」](https://web.archive.org/web/20081217115302/http://home.att.ne.jp/omega/akabeisoft/)（包括AKABEiSOFT）
  - [原畫擔當有葉個人網站「It
    buds.」](https://web.archive.org/web/20081201074551/http://home.att.ne.jp/omega/akabeisoft/alpha/index.html)
  - [原画擔當友瀨俊作網站「無限軌道」](http://mugenkidou.sakura.ne.jp/)

[Category:日本成人遊戲公司](../Category/日本成人遊戲公司.md "wikilink")
[Category:日本電子遊戲公司](../Category/日本電子遊戲公司.md "wikilink")
[Category:福岡縣公司](../Category/福岡縣公司.md "wikilink")

1.
2.  同人展（[Comic Market](../Page/Comic_Market.md "wikilink")）・廠商限定商品。
3.  同人展（[Comic Market](../Page/Comic_Market.md "wikilink")）・廠商限定商品。
4.  同人展（[Comic Market](../Page/Comic_Market.md "wikilink")）・廠商限定商品。