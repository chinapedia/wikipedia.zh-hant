**北投溫泉博物館**（****）是位於[台灣](../Page/台灣.md "wikilink")[台北市](../Page/台北市.md "wikilink")[北投區的史蹟保存類](../Page/北投區.md "wikilink")[博物館](../Page/博物館.md "wikilink")，過去是公共的[溫泉浴場](../Page/溫泉.md "wikilink")，因此又名**北投溫泉浴場**。

## 歷史

[北投溫泉博物館榻榻米活動大廳.jpg](https://zh.wikipedia.org/wiki/File:北投溫泉博物館榻榻米活動大廳.jpg "fig:北投溫泉博物館榻榻米活動大廳.jpg")活動大廳\]\]
[北投溫泉博物館電影院.jpg](https://zh.wikipedia.org/wiki/File:北投溫泉博物館電影院.jpg "fig:北投溫泉博物館電影院.jpg")
[北投溫泉博物館的樓梯.jpg](https://zh.wikipedia.org/wiki/File:北投溫泉博物館的樓梯.jpg "fig:北投溫泉博物館的樓梯.jpg")
[北投溫泉博物館大浴池.jpg](https://zh.wikipedia.org/wiki/File:北投溫泉博物館大浴池.jpg "fig:北投溫泉博物館大浴池.jpg")
[北投溫泉博物館小浴池.jpg](https://zh.wikipedia.org/wiki/File:北投溫泉博物館小浴池.jpg "fig:北投溫泉博物館小浴池.jpg")
[北投溫泉博物館個人浴池.jpg](https://zh.wikipedia.org/wiki/File:北投溫泉博物館個人浴池.jpg "fig:北投溫泉博物館個人浴池.jpg")
[北投溫泉博物館的彩繪玻璃.jpg](https://zh.wikipedia.org/wiki/File:北投溫泉博物館的彩繪玻璃.jpg "fig:北投溫泉博物館的彩繪玻璃.jpg")
北投溫泉浴場建於[1913年](../Page/1913年.md "wikilink")[6月17日](../Page/6月17日.md "wikilink")，為[臺北州仿照](../Page/臺北州.md "wikilink")[日本](../Page/日本.md "wikilink")[靜岡縣伊豆山溫泉的方式興建而成](../Page/靜岡縣.md "wikilink")，是當時規模最大、最華麗的[公共浴場](../Page/公共浴場.md "wikilink")，由[臺北廳公共衛生單位管理經營](../Page/臺北廳.md "wikilink")。[1921年日本](../Page/1921年.md "wikilink")[皇太子](../Page/皇太子.md "wikilink")[裕仁來台](../Page/裕仁.md "wikilink")，乃增建二樓30多[坪御休所](../Page/坪.md "wikilink")；[1997年二月內政部定為](../Page/1997年.md "wikilink")[三級古蹟](../Page/三級古蹟.md "wikilink")，之後由[台北市政府民政局整修為北投溫泉博物館](../Page/台北市政府民政局.md "wikilink")。占地約700坪，是兩層樓仿英式[磚造建築](../Page/磚造.md "wikilink")，一樓為磚造[浴池](../Page/浴池.md "wikilink")，二樓為木造[休息區](../Page/休息區.md "wikilink")，並設有[娛樂室等](../Page/娛樂室.md "wikilink")。

[1945年](../Page/1945年.md "wikilink")，[國民政府接收](../Page/國民政府.md "wikilink")[台灣之後](../Page/台灣.md "wikilink")，此浴場曾一度成為[國民黨](../Page/國民黨.md "wikilink")[北投鎮](../Page/北投鎮.md "wikilink")[黨部](../Page/黨部.md "wikilink")，及[民眾服務社](../Page/民眾服務社.md "wikilink")，除了辦國家慶典外，也出租供民眾作為辦[喜宴的場所](../Page/喜宴.md "wikilink")。當時該建築一樓後面部分，即現博物館辦公室區及浴場部分，也做為「[台北縣議會招待所](../Page/台北縣議會招待所.md "wikilink")」兼營[游泳池](../Page/游泳池.md "wikilink")。該建築同時還有[警察局](../Page/警察局.md "wikilink")[北投分局](../Page/北投分局.md "wikilink")[光明派出所](../Page/光明派出所.md "wikilink")，共三單位同時使用該建築。國民黨部遷出後，北投溫泉業也沒落了，最後台北縣議會也不再使用為招待所。而光明派出所亦於[1988年](../Page/1988年.md "wikilink")[2月9日起遷移至新址](../Page/2月9日.md "wikilink")[光明路](../Page/光明路.md "wikilink")131之2號。於是該建築變成台北縣人員[陳師堯先生占用之住宅](../Page/陳師堯.md "wikilink")，部分空間出租變成[電影道具間](../Page/電影道具.md "wikilink")，開始急速傾頹。

[1994年](../Page/1994年.md "wikilink")[逢甲大學](../Page/逢甲大學.md "wikilink")[建築系的學生](../Page/建築系.md "wikilink")[陳林頌由於小時候遊歷北投的美好記憶](../Page/陳林頌.md "wikilink")，以北投溫泉谷地的[北投溪](../Page/北投溪.md "wikilink")、[北投溫泉公共浴場](../Page/北投溫泉公共浴場.md "wikilink")、[北投公園等北投歷史空間之研究](../Page/北投公園.md "wikilink")，為題處理逢甲大學[畢業論文](../Page/畢業論文.md "wikilink")，並測繪了北投公共浴場平面、立面圖、北投溪谷地模型。因陳林頌到處呼籲搶救北投公共浴場，不要被拆除成為[纜車站](../Page/纜車.md "wikilink")。

[1994年](../Page/1994年.md "wikilink")，[北投國小的](../Page/台北市立北投國民小學.md "wikilink")[呂鴻文](../Page/呂鴻文.md "wikilink")、[黃桂冠](../Page/黃桂冠.md "wikilink")、[謝淑玲](../Page/謝淑玲.md "wikilink")、[許家寶老師開會規劃八十三年度資源班教學主題](../Page/許家寶.md "wikilink")，決定以「[鄉土教學](../Page/鄉土教學.md "wikilink")」為重點。呂鴻文、黃桂冠老師在探勘[北投溪時](../Page/北投溪.md "wikilink")，進入「[台北縣議會招待所](../Page/台北縣議會招待所.md "wikilink")」，赫然發現這棟建築即是「[北投溫泉公共浴場](../Page/北投溫泉公共浴場.md "wikilink")」。她們清楚知道這棟建築的歷史價值，幾經[陳情要求保存這棟建築都沒有下文](../Page/陳情.md "wikilink")，最後[蔡麗美老師發起師生](../Page/蔡麗美.md "wikilink")[聯署了一張](../Page/聯署.md "wikilink")[陳情書](../Page/陳情書.md "wikilink")，最後這張陳情書拿給居住[北投](../Page/北投.md "wikilink")，時任[國大代表的](../Page/國大代表.md "wikilink")[許陽明](../Page/許陽明.md "wikilink")。而陳林頌的資料也在這時彙集到了許陽明辦公室，並被採納成為重建北投最重要的理論依據與宣傳資料。許陽明於是利用其與當時[陳水扁](../Page/陳水扁.md "wikilink")[市長的緊密關係](../Page/市長.md "wikilink")，親自向陳水扁說明，因此很快就指定成了[三級古蹟](../Page/三級古蹟.md "wikilink")，許陽明開始邀集當地居民組成「北投溫泉博物館」推動小組，並提出主張推動成為「居民親手打造自己社區的博物館」，也積極地向[台北市政府爭取經費](../Page/台北市政府.md "wikilink")，最後獲得市政府採納成為市府的重要專案。為了實踐「居民親手打造自己社區的博物館」這個想法，許陽明徵求全部不支薪的社區[志工](../Page/志工.md "wikilink")，組織「北投溫泉博物館」籌備處，以提出社區主張，協調社區主張，並督促實現主張。同時也協助收集溫泉相關文物無償捐贈「北投溫泉博物館」，並記錄志工籌備工作日誌以昭公信，用以實踐這個台灣史無前例的社區運動。

## 修復

1998年北投溫泉博物館由[台北市政府著手修復成史蹟類保存博物館](../Page/台北市政府.md "wikilink")，並在10月31日對外開放。當時花費總計有1億1181萬餘元；工程分為兩個階段，第一階段是拆除一些岌岌可危的建築體，以及補強管線等，花費336萬元；此階段留下來舊有的溫泉系統包括分水管、[磺底石](../Page/磺底石.md "wikilink")、溫泉陶管，[口徑磚等](../Page/口徑磚.md "wikilink")，這些在營造籌備階段由許陽明、陳林頌等人在工地發現保存，或專程收集的無償文物。

第二階段則是修復與再利用工程，花費9845萬209元。此次主要是為了與鄰近的[北投溫泉親水公園一起規劃](../Page/北投溫泉親水公園.md "wikilink")。這一段時期的修復，參考了許多舊照片和耆老口述，將溫泉博物館內部與外部盡量回復[日治時期舊貌](../Page/日治時期.md "wikilink")。可惜博物館仍有不少珍貴的文物已經遺失，如西側一樓[拱窗的](../Page/拱窗.md "wikilink")[彩繪玻璃](../Page/彩繪玻璃.md "wikilink")，就已在修護前遺失，現在是複製品。

## 現況

該館現已列為台灣第三級古蹟。館內規劃有一樓二樓展區，以北投溫泉發展史為主題，從溫泉的原理、[北投石](../Page/北投石.md "wikilink")、火山（一樓展場）到後期北投溫泉鄉發展史、[凱達格蘭族歷史](../Page/凱達格蘭.md "wikilink")「北投社原住民」、北投產業和台灣好萊塢等規劃（二樓展場）。目前館內參觀人數有限制，同時不得超過一百人在館內參觀，入館也必須更換拖鞋。戶外由於草坪優美，景觀典雅，常可見到即將結婚的新人在此拍攝[婚紗照](../Page/婚紗照.md "wikilink")。溫泉博物館館內最珍貴的眾多北投石，全部由許陽明出面向中華礦石協會，特別是陳嘉林理事長，還有諸多礦石收藏家商量，呼籲「讓北投石回家」時，紛紛捐出讓許陽明一起交由溫泉博物館收藏的珍貴國寶。

## 交通

  - [台北捷運](../Page/台北捷運.md "wikilink")[新北投站](../Page/新北投站.md "wikilink")。

## 圖片集

[File:北投溫泉博物館(2).jpg|thumb|博物館建築物北半部](File:北投溫泉博物館\(2\).jpg%7Cthumb%7C博物館建築物北半部)
[File:北投溫泉博物館(3).jpg|thumb|博物館建築物南半部](File:北投溫泉博物館\(3\).jpg%7Cthumb%7C博物館建築物南半部)
[File:北投溫泉博物館(4).jpg|thumb|博物館建築物北半部(另一角度)](File:北投溫泉博物館\(4\).jpg%7Cthumb%7C博物館建築物北半部\(另一角度\))
[File:北投溫泉博物館(5).jpg|thumb|博物館建築物北半部與水池](File:北投溫泉博物館\(5\).jpg%7Cthumb%7C博物館建築物北半部與水池)
[File:北投溫泉博物館(6).jpg|thumb|由草坪方向觀看博物館建築物](File:北投溫泉博物館\(6\).jpg%7Cthumb%7C由草坪方向觀看博物館建築物)

## 外部連結

  - [北投溫泉博物館 官方網站](http://beitoumuseum.taipei.gov.tw/)

  - [北投溫泉浴場-文化部文化資產局](https://nchdb.boch.gov.tw/assets/advanceSearch/monument/19970220000004)

  - [北投溫泉博物館-臺北旅遊網](https://www.travel.taipei/zh-tw/attraction/details/33)

  - [許陽明辦公室：社區志工籌備「北投溫泉博物館」工作日誌
    從上樑日至開館日（1998.8.5-1998.10.31）](http://blog.udn.com/ecomuseum/6767673)從.北投府城到港都ecomuseum
    的部落格 - udn部落格.2012-08-26

  -
## 参考文献

[category:台灣歷史博物館](../Page/category:台灣歷史博物館.md "wikilink")

[Category:北投區](../Category/北投區.md "wikilink")
[Category:台北市博物館](../Category/台北市博物館.md "wikilink")
[Category:台北市古蹟](../Category/台北市古蹟.md "wikilink")
[Category:臺灣溫泉](../Category/臺灣溫泉.md "wikilink")
[Category:台灣日式建築](../Category/台灣日式建築.md "wikilink")
[Category:臺北市文化局](../Category/臺北市文化局.md "wikilink")
[Category:1998年台灣建立](../Category/1998年台灣建立.md "wikilink")
[Category:1998年建立的博物館](../Category/1998年建立的博物館.md "wikilink")