**陳興濤**（），[香港商人](../Page/香港.md "wikilink")，[農場餐廳創辦人](../Page/農場餐廳.md "wikilink")，其後曾經[破產](../Page/破產.md "wikilink")。

## 簡歷

### 早年

陳興濤出身寒微，小學六年級曾派[報紙以幫補家計](../Page/報紙.md "wikilink")，於[聖伯多祿中學畢業後](../Page/聖伯多祿中學.md "wikilink")，曾以自修生身份報考[香港中文大學新聞系](../Page/香港中文大學.md "wikilink")，不過未獲取錄；其後進入[香港大學主修](../Page/香港大學.md "wikilink")[中國歷史](../Page/中國歷史.md "wikilink")，1981年於文學院畢業後，以5,000港元到[歐洲旅遊](../Page/歐洲.md "wikilink")9個月，並於餐廳工作賺取旅費。他返回香港後，曾任職家具店[推銷員](../Page/推銷員.md "wikilink")，之後擔任[教師](../Page/教師.md "wikilink")。1983年他進入飲食界。

### 創業

1987年，陳興濤與其他股東張生合資75萬港元創辦農場餐廳。他以獨特的方法管理餐廳，例如禁止員工說[粗口](../Page/粗口.md "wikilink")，也禁止[廚房員工](../Page/廚房.md "wikilink")[抽煙等](../Page/抽煙.md "wikilink")。\[1\]
由於陳興濤為文人出身，他喜歡撰寫文章，向股東、員工以至顧客表達公司的經營方向及面對的問題，亦曾把其文章刊登在[報章](../Page/報章.md "wikilink")[廣告上](../Page/廣告.md "wikilink")；而農場餐廳各分店的牆上，餐廳內的餐牌及餐墊紙上，都印有陳興濤撰寫的人生哲理金句等文章。

全盛時期，農場餐廳擴展至48家分店，僱員達1,300人，每天營業額接近20萬港元。當時他居於[半山區](../Page/半山區.md "wikilink")[旭龢道](../Page/旭龢道.md "wikilink")3,400呎住宅，擁有3部[汽車](../Page/汽車.md "wikilink")，3名[傭人及](../Page/傭人.md "wikilink")[司機](../Page/司機.md "wikilink")
。

好

### 破產

1997年[亞洲金融風暴爆發](../Page/亞洲金融風暴.md "wikilink")，當時陳興濤以為事件很快會過去，反而私人擔保向[銀行借貸](../Page/銀行.md "wikilink")，取得一筆流動資金繼續經營，以支付租金及員工薪酬。可惜一年後資金耗盡，生意無法維持，結果於被業主入稟[高等法院申請把公司](../Page/高等法院.md "wikilink")[清盤及申請個人](../Page/清盤.md "wikilink")[破產](../Page/破產.md "wikilink")，2001年3月20日，被法庭頒令清盤。\[2\]
他欠債超過1,000萬港元。

破產後，陳興濤轉為[小販售賣](../Page/小販.md "wikilink")[兒童](../Page/兒童.md "wikilink")[書籍](../Page/書籍.md "wikilink")，主要出售[小說](../Page/小說.md "wikilink")《[哈利波特](../Page/哈利波特.md "wikilink")》，並以3個膠箱盛載140本中英文書籍，於香港各區擺檔出售；並一家遷往[西灣河一個](../Page/西灣河.md "wikilink")400平方呎單位居住，月租4,500港元。2001年12月18日，他曾借用兒子的校[呔參加香港大學](../Page/領帶.md "wikilink")90周年校慶晚宴。2002年1月，他曾為[蘋果日報撰寫文章](../Page/蘋果日報.md "wikilink")，講述其創業至破產等人生經歷。2003年[SARS疫症期間](../Page/SARS.md "wikilink")，他又擺攤檔出售[口罩及消毒衛生用品](../Page/口罩.md "wikilink")。

### 東山再起

陳興濤的破產令在2005年解除，2006年初，[破產管理署申請於高等法院頒令取消陳興濤的](../Page/破產管理署.md "wikilink")[董事資格](../Page/董事.md "wikilink")4年，他向上訴庭提出上訴。2007年，他任職於[珠海嘉信食品發展有限公司](../Page/珠海.md "wikilink")，擔任食品推銷工作。他亦於2月1日在[九龍](../Page/九龍.md "wikilink")[藍田一商場開設店舖](../Page/藍田.md "wikilink")，銷售圖書、文具與食品。他正計劃東山再起，重新創辦農場餐廳，並已透過其好友籌得部份資金。\[3\]

2008年，他分別在[上水石湖墟市政大樓街市熟食中心和](../Page/上水.md "wikilink")[葵芳葵涌廣場重新開業](../Page/葵芳.md "wikilink")，店名「農家意粉」\[4\]。同年7月，他就被撤銷董事資格4年的裁決提訴得值，獲准減至3年半至2009年8月。他表示與數名舊同學已決意在禁任董事限期屆滿後，在[灣仔鬧市開設](../Page/灣仔.md "wikilink")「農家意粉」，但會到餐廳賺的錢可以清還舊債時，他才會用回「農場餐廳」的名字\[5\]。

2008年起，農家意粉先後在上水、葵涌、長沙灣、深水埗及粉嶺開設五間分店，因經營不善致入不敷支，六年蝕過千萬元，四間分店已先後結束營業，上水的最後一間分店，亦於2015年1月7日結束營業，陳興濤揚言半年後籌措三百萬元，再捲土重來\[6\]。

## 家庭

陳興濤已婚，有一子兩女。他在破產後曾一家遷往[廣東](../Page/廣東.md "wikilink")[番禺居住](../Page/番禺.md "wikilink")。

## 資料來源

<div class="references-small">

<references />

  - [撥著問號 尋出一堆感歎號 可惜風流半截腰
    動人情處不曾描](https://web.archive.org/web/20070930182650/http://www1.appledaily.atnext.com/template/apple/art_main.cfm?iss_id=20020104&sec_id=4104&subsec_id=11867&art_id=2358132)
    蘋果日報，2002年1月4日
  - [非常人語 真與假不重要
    陳興濤](http://next.atnext.com//template/next/art_main.cfm?iss_id=618&sec_id=1000853&art_id=2365702)
    壹週刊，2002年1月9日
  - [壹號頭條
    破產富豪大翻身](http://next.atnext.com//template/next/art_main.cfm?iss_id=755&sec_id=1000853&art_id=4264139)
    壹週刊，2004年8月26日
  - [浮沈風暴中：與農場餐廳創辦人陳興濤先生對談](http://www.guoshi.net/chan.pdf)
  - [青年視窗](http://www.icac.org.hk/me/icac/prospective/20.htm) 廉政公署
  - 《[拾年](../Page/拾年_\(資訊節目\).md "wikilink")》，無線電視，2007年6月
  - [不死鳥陳興濤破產期過，上水石湖墟熟食中心開設農場意粉](http://www.review33.com/chat/forum_message.php?topic=98080614092828)，[星島日報](../Page/星島日報.md "wikilink")2008年6月14日，第A22頁
  - [農場餐廳老闆又生意失敗](http://www.orientaldaily.on.cc/cnt/news/20150106/00176_059.html)，東方日報2015年1月6日

</div>

[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:香港大學校友](../Category/香港大學校友.md "wikilink")
[Category:陳姓](../Category/陳姓.md "wikilink")
[Category:聖伯多祿中學校友](../Category/聖伯多祿中學校友.md "wikilink")

1.  [奇招管員工
    禁煙讀日記](http://www1.appledaily.atnext.com//template/apple/art_main.cfm?iss_id=20020101&sec_id=4104&subsec_id=11866&art_id=2350709)
     蘋果日報，2002年1月1日
2.  [破產期滿開小店 上訴求重做董事
    農場餐廳創辦人再戰江湖](http://appledaily.atnext.com/template/apple/art_main.cfm?iss_id=20070117&sec_id=4104&subsec_id=11867&art_id=6724358)
    蘋果日報，2007年1月17日
3.  [往事只能回味：錢債人情還](http://www1.appledaily.atnext.com/template/apple/art_main.cfm?iss_id=20070130&sec_id=4104&subsec_id=15333&art_id=6765652)
     蘋果日報，2007年1月30日
4.  風雲人物：仲笑得出　陳興濤《飲食男女》第668期，2008年5月16日
5.  [農場餐廳創辦人﹕香港有公義 上訴得直
    禁任董事期減半年](http://hk.news.yahoo.com/article/080722/4/7b4h.html)
     明報，2008年7月23日
6.  [農場餐廳老闆又生意失敗](http://orientaldaily.on.cc/cnt/news/20150106/00176_059.html)