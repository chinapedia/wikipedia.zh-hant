**皇后鎮**（英語：**Queenstown**；[毛利語](../Page/毛利語.md "wikilink")：**Tahuna**）位于[新西兰](../Page/新西兰.md "wikilink")[南岛](../Page/南島_\(新西蘭\).md "wikilink")[奥塔哥大区的西南部](../Page/奥塔哥大区.md "wikilink")，是南岛的旅游度假地。整个城市环绕建造于[瓦卡蒂普湖的弗兰克敦湾](../Page/瓦卡蒂普湖.md "wikilink")（Frankton
Arm），瓦卡蒂普湖呈一个消瘦的“S”型，附近被非常壮观的群山环绕。景色变化万千、湖光山色宛如仙境。

皇后镇是的最大城市，但出于一些管理上的理由，皇后镇通常被看作为新西兰南岛的[南地大区的一部分](../Page/南地大区.md "wikilink")。因为是旅游度假胜地的关系，皇后镇上有很多间餐厅旅馆，特色美食是烧羊肉与酥皮卷，这两项美食一直都为新西兰的传统食物。美国前总统[比尔·克林顿也曾在汽船码头的Boardwalk餐厅尝过海鲜](../Page/比尔·克林顿.md "wikilink")。

## 命名

关于皇后镇(Queenstown)一名来源，有很多不同的说法。最主要的说法是当时当地的[淘金者为了纪念](../Page/淘金者.md "wikilink")[英国](../Page/英国.md "wikilink")[女王维多利亚](../Page/维多利亚_\(英国君主\).md "wikilink")（Queen
Victoria），取这个名字是为了女王的健康。现在，皇后镇成为了全新西兰乃至世界闻名的旅游度假胜地。

## 地理

皇后镇附近的城镇有[箭鎮](../Page/箭鎮.md "wikilink")、[瓦納卡](../Page/瓦納卡.md "wikilink")、亚历山大和克伦威尔。比邻曾经的淘金地——箭镇（Arrowtown），这里留有中国人在新西兰淘金的遗迹——中国村（Arrowtown
Chinese
Settlement）。最近的城市则是[但尼丁和](../Page/但尼丁.md "wikilink")[因弗卡吉爾](../Page/因弗卡吉爾.md "wikilink")。

### 交通

[皇后鎮國際機場可以为皇后镇的旅客提供服务](../Page/皇后鎮國際機場.md "wikilink")。

[Remarkables.jpg](https://zh.wikipedia.org/wiki/File:Remarkables.jpg "fig:Remarkables.jpg")

### 氣候

皇后鎮[氣候宜人](../Page/氣候.md "wikilink")，屬於[溫帶海洋性氣候](../Page/溫帶海洋性氣候.md "wikilink")，溫度冬暖夏涼。

## 人口

在2001年的人口调查中，皇后镇的市区[常住居民](../Page/常住人口.md "wikilink")（包含Frankton和Kelvin
Heights地区）为8535人，比較与1996年的增长率为19.3%。到2007年城市长住人口則增長到10,990；流动人口25,000-35,000（冬季）。皇后镇湖区人口22,956（2006年政府估计）。

## 旅游

至2012年，皇后镇已有220项旅游项目。热门的项目包括[滑雪](../Page/滑雪.md "wikilink")，[单板滑雪](../Page/單板滑雪.md "wikilink")，快艇，[漂流](../Page/漂流.md "wikilink")，[蹦极](../Page/蹦极.md "wikilink")，[山地自行车](../Page/山地自行车.md "wikilink")，[滑板](../Page/滑板.md "wikilink")，[登山](../Page/登山运动.md "wikilink")，[滑翔伞](../Page/滑翔伞.md "wikilink")，[跳伞和](../Page/跳伞.md "wikilink")[飞蝇钓](../Page/飛蠅釣.md "wikilink")。

皇后镇地区拥有四个主要滑雪场：[Cardrona Alpine
Resort](../Page/Cardrona_Alpine_Resort.md "wikilink"), [Coronet
Peak](../Page/Coronet_Peak.md "wikilink"), [The
Remarkables和](../Page/The_Remarkables.md "wikilink")[Treble
Cone](../Page/Treble_Cone.md "wikilink")；[Waiorau
Snowfarm](../Page/Waiorau_Snowfarm.md "wikilink")
提供[越野滑雪活动](../Page/越野滑雪.md "wikilink")。

有“湖上女士”之称的蒸汽船[TSS
Earnslaw](../Page/TSS_Earnslaw.md "wikilink")，建造于1912年，每天从皇后镇码头往返Walter
Peak。

箭镇地区拥有一个优秀的小型红酒产地以及酒庄。

## 教育

[瓦卡蒂普高中](../Page/瓦卡蒂普高中.md "wikilink")，公立高中，提供九至十三年级教学。

英语学校包括新西兰语言学校（Language Schools New Zealand），ABC英语学校（ABC College of
English）和南部湖区英语学校（Southern Lakes English College）。

提供一至八年级小学教育的学校：KingsView, Queenstown, Remarkables, St Joseph's 和
Shotover.

其他学校：Queenstown Resort College，ACE Wakatipu。

## 相关图片

<File:Queenstown> & Remarkable Mountains.jpg| <File:Ledge> Bungy,
Queenstown, New Zealand 01.jpg|俯瞰瓦卡蒂普湖週遭的皇后镇

## 姊妹城市

  - [日本](../Page/日本.md "wikilink")[益田市](../Page/益田市.md "wikilink")

  - [美国](../Page/美国.md "wikilink")[科罗拉多州](../Page/科罗拉多州.md "wikilink")[阿斯彭](../Page/阿斯彭.md "wikilink")

  - [中華人民共和國](../Page/中華人民共和國.md "wikilink")[浙江](../Page/浙江.md "wikilink")[杭州](../Page/杭州.md "wikilink")\[1\]

## 參考文獻

[Category:紐西蘭城市](../Category/紐西蘭城市.md "wikilink")
[Category:奥塔哥大区](../Category/奥塔哥大区.md "wikilink")
[Category:大洋洲地標](../Category/大洋洲地標.md "wikilink")

1.