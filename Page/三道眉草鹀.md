**三道眉草鹀**，学名*Emberiza
cioides*，又名-{zh-cn:草鹀;zh-tw:三道眉草鵐;}-、大白眉、三道眉、犁雀儿、山带子、山麻雀、韩鹀、小栗鹀。

## 生态环境

三道眉草鹀喜欢在开阔地环境中活动，见于[丘陵地带和半山区地稀疏](../Page/丘陵.md "wikilink")[阔叶林地](../Page/阔叶林.md "wikilink")，山麓[平原或山沟的灌丛和草丛中以及远离村庄的树丛和农田](../Page/平原.md "wikilink")。本物种垂直分布于[海拔](../Page/海拔.md "wikilink")500[米](../Page/米.md "wikilink")－1100米的中山和低山地带，有的亚种在夏季可能漂泊到海拔2800米以上的高山地带。

## 分布地域

三道眉草鹀主要分布在[亚洲东部地区](../Page/亚洲.md "wikilink")，[俄罗斯的远东地区](../Page/俄罗斯.md "wikilink")、[蒙古国](../Page/蒙古国.md "wikilink")、[朝鲜半岛](../Page/朝鲜半岛.md "wikilink")、[日本列岛有本物种分布](../Page/日本列岛.md "wikilink")，[中国东部北起](../Page/中国.md "wikilink")[东北三省南至](../Page/东北三省.md "wikilink")[广东北部和](../Page/广东.md "wikilink")[福建南部均有本物种分布](../Page/福建.md "wikilink")，其分布西限可达[陕西](../Page/陕西.md "wikilink")、[甘肃](../Page/甘肃.md "wikilink")、[青海](../Page/青海.md "wikilink")、[四川直至](../Page/四川.md "wikilink")[云](../Page/云.md "wikilink")[贵一线](../Page/贵.md "wikilink")，另外在[新疆西部](../Page/新疆.md "wikilink")[天山山脉一线有本物种](../Page/天山山脉.md "wikilink")*cioides*亚种分布，[纬度较低的](../Page/纬度.md "wikilink")[台湾](../Page/台湾.md "wikilink")、[香港等地有本物种迷鸟记录](../Page/香港.md "wikilink")。

## 特征

[Emberiza_cioides_eating_seed.JPG](https://zh.wikipedia.org/wiki/File:Emberiza_cioides_eating_seed.JPG "fig:Emberiza_cioides_eating_seed.JPG")
[_Emberiza_cioides_MHNT.ZOO.2011.11.219_Blazac_Naurois_Fuji_Hondo.jpg](https://zh.wikipedia.org/wiki/File:_Emberiza_cioides_MHNT.ZOO.2011.11.219_Blazac_Naurois_Fuji_Hondo.jpg "fig:_Emberiza_cioides_MHNT.ZOO.2011.11.219_Blazac_Naurois_Fuji_Hondo.jpg")
三道眉草鹀体形略大，约16厘米左右，雄雌个体同形异色。**雄性**头顶至脑后枕部深栗色，面颊以饱满的黑色为底色，有净[白色的宽大眉纹和瓷纹](../Page/白色.md "wikilink")，眉纹颊纹都很宽与[黑色区域几乎等宽](../Page/黑色.md "wikilink")，颌及喉部污白色；上体肩背部以栗褐色为基色，各羽羽干黑色，形成稍宽的黑色纵纹，尾上覆羽纯栗色；两枚中央尾羽栗红色，羽干黑色，其余[尾羽黑褐色](../Page/羽毛.md "wikilink")，外土黄色，最外侧的两枚尾羽外甲各自有一块白色条带，飞行时非常明显，这两块白色也是鹀属鸟类的共同特征；双翅小覆羽灰色，形成灰色肩角，各级飞羽均以栗色、褐色棕色为基调，其中三级飞羽和大覆羽羽干黑色，在两翅部位形成黑色纵斑；上胸栗红色；两胁栗红色至栗黄色；腹部土黄色，愈向尾部颜色愈浅淡，至尾下覆羽则呈现略现土黄色的污白色。**雌性**体形与雄性接近，但头面部羽色不似雄鸟那样黑白分明，头顶至枕部以褐色为基色，具少量黑色纵斑，眉纹宽大，自嘴基延伸至颈侧略现极浅的棕黄色，耳羽和颊部栗色，在栗色区域与浅黄色区域之间以一条幼细的黑色贯眼纹分隔，在相当于雄性白色瓷纹的部位有一条浅黄褐色瓷纹。
虹膜呈深褐；上[喙色深](../Page/喙.md "wikilink")，下[喙蓝灰而嘴端色深](../Page/喙.md "wikilink")；双足粉色。

## 食物

鹀属鸟类多系食谷鸟，三道眉草鹀亦不例外。据野外观察，三道眉草鹀冬春季食谱以野生草种为主，[夏季以](../Page/夏季.md "wikilink")[昆虫为主](../Page/昆虫.md "wikilink")。他们所取食的草籽包括[蓼](../Page/蓼科.md "wikilink")、[稗](../Page/稗.md "wikilink")、[狗尾草](../Page/狗尾草.md "wikilink")、[葶荠](../Page/葶荠.md "wikilink")、[萝卜](../Page/萝卜.md "wikilink")、[鹅观草](../Page/鹅观草.md "wikilink")、[稻谷](../Page/稻谷.md "wikilink")、[小麦等的种子](../Page/小麦.md "wikilink")；取食的动物性食物包括[鞘翅目](../Page/鞘翅目.md "wikilink")、[直翅目](../Page/直翅目.md "wikilink")、[双翅目](../Page/双翅目.md "wikilink")、[膜翅目](../Page/膜翅目.md "wikilink")、[鳞翅目](../Page/鳞翅目.md "wikilink")、[同翅目昆虫](../Page/同翅目.md "wikilink")、[蜘蛛等](../Page/蜘蛛.md "wikilink")，在5－8月的育雏期其食谱几乎全部是[鳞翅目昆虫的](../Page/鳞翅目.md "wikilink")[幼虫](../Page/幼虫.md "wikilink")。

## 繁殖与保护

三道眉草鹀的繁殖开始于每年的4月，在这期间雄鸟开始[占区](../Page/占区.md "wikilink")，与雌鸟配对，4月底至5月初间开始营巢，巢呈碗状，以草茎交织而成，内垫[马鬃](../Page/马.md "wikilink")[兽毛](../Page/哺乳纲.md "wikilink")，平均外径约11厘米，平均内径约6厘米，高10厘米，深6厘米，巢址选择在光线充足的林间灌木中，山坡草丛的地面上，全部营巢工作均由雌鸟完成。本物种产卵期在每年的5－6月间，每巢产卵5枚左右，卵色呈白色或浅[天蓝色](../Page/天蓝色.md "wikilink")，表面散布[褐色细纹稍具斑点](../Page/褐色.md "wikilink")，孵化期12－13天。雏鸟生长迅速，留巢期12－13天，据郑光美等人的研究，雏鸟体重生长符合[logistic方程](../Page/logistic方程.md "wikilink")，[渐近线](../Page/渐近线.md "wikilink")16.6[g](../Page/克.md "wikilink")，拐点3.7[天](../Page/天.md "wikilink")，增长率0.580，其生长方程的具体形式为：

<center>

\(W=\frac{16.6}{1+e^{-0.580(t-3.7)}}\)

</center>

  -
    离巢的雏鸟在亲鸟带领下活动一段时间后于8月末结成20只左右的小群活动。
    本物种未被列入保护动物目录，其在中国的分布地域虽然非常广泛，种群数量较大。

## 外部链接

  - [三道眉草鹀观测记录](http://www.cnbirder.com/birdinfo.asp?id=1307)

[Category:鹀科](../Category/鹀科.md "wikilink")