**穆迪公司**（**Moody's
Corporation**，常簡稱為**Moody's**），是美國著名的三大[信用評級公司之一](../Page/信用評級.md "wikilink")，创始人是[约翰·穆迪](../Page/约翰·穆迪.md "wikilink")（John
-{Moody's}-），他在1909年首创对[铁路](../Page/铁路.md "wikilink")[债券进行信用评级](../Page/债券.md "wikilink")。1913年，穆迪开始对[公用事业和工业](../Page/公用事业.md "wikilink")[债券进行信用评级](../Page/债券.md "wikilink")。目前，穆迪在全球有800名分析专家，1700多名助理分析员，在17个国家设有机构，股票在[纽约证券交易所上市交易](../Page/纽约证券交易所.md "wikilink")（）。

1975年
[美国证券交易委员会](../Page/美国证券交易委员会.md "wikilink")（SEC）认可穆迪公司、[标准普尔](../Page/标准普尔.md "wikilink")、[惠誉国际為](../Page/惠誉国际.md "wikilink")「全國認定的評級組織」（NRSRO,
Nationally Recognized Statistical Rating Organization）。

## 參見

  - [债券](../Page/债券.md "wikilink")
  - [大公国际](../Page/大公国际.md "wikilink")
  - [信用评级](../Page/信用评级.md "wikilink")
  - [标准普尔](../Page/标准普尔.md "wikilink")
  - [惠誉国际](../Page/惠誉国际.md "wikilink")
  - [邓白氏](../Page/邓白氏.md "wikilink")
  - [DBRS](../Page/DBRS.md "wikilink")

## 参考文献

## 外部链接

  - [穆迪官方网站](http://www.moodys.com)

  - [穆迪的历史](http://www.washingtonpost.com/wp-dyn/articles/A5573-2004Nov22.html)

[Category:金融机构](../Category/金融机构.md "wikilink")
[Category:信用評等機構](../Category/信用評等機構.md "wikilink")
[Category:1909年成立的公司](../Category/1909年成立的公司.md "wikilink")