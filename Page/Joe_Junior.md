**羅利期**\[1\]（，，），澳門著名[土生葡人歌手及影星](../Page/澳門土生葡人.md "wikilink")，但在拍攝《[三個小生去旅行](../Page/三個小生去旅行.md "wikilink")》前未曾到過葡國，更完全不諳[葡萄牙語](../Page/葡萄牙語.md "wikilink")\[2\]。在[澳門出生](../Page/澳門.md "wikilink")，曾就讀澳門的[粤華中學](../Page/粤華中學.md "wikilink")，[聖若瑟書院及](../Page/聖若瑟書院.md "wikilink")[新法書院](../Page/新法書院.md "wikilink")（在校時與已故藝人[鄧光榮為同學](../Page/鄧光榮.md "wikilink")）。

## 歌唱生涯

Joe
Junior聲線嘹亮，[音域廣闊](../Page/音域.md "wikilink")，形象莊諧並重。1965年，他組成了Zouncrackers[樂隊](../Page/樂隊.md "wikilink")，後拆夥。1967年他另組Side
Effects樂隊，成員包括他本人（主音）、Alex Toa（主音[結他](../Page/結他.md "wikilink")）、James
Fong（節奏結他）、David Tong Fong（Rhythm結他）及Robert
Lee（[鼓手](../Page/鼓手.md "wikilink")）。同年重唱《Here's a
heart》，連續七周高據流行榜榜首，紀錄至今仍未有人打破，銷量達2萬張，刷新了當時的[唱片銷量紀錄](../Page/唱片.md "wikilink")，被譽為「[香港](../Page/香港.md "wikilink")[六十年代樂壇寵兒](../Page/六十年代.md "wikilink")」，[歌手](../Page/歌手.md "wikilink")[鄭中基的](../Page/鄭中基.md "wikilink")[父親](../Page/父親.md "wikilink")[鄭東漢](../Page/鄭東漢.md "wikilink")（Norman
Cheng）也曾任他們的唱片[監製](../Page/監製.md "wikilink")。後Side Effects拆夥，Joe
Jr.轉以個人名義[出版](../Page/出版.md "wikilink")[單曲及](../Page/單曲.md "wikilink")[專輯](../Page/專輯.md "wikilink")，產量甚豐。

現時他除了到各[社區](../Page/社區.md "wikilink")、[學校及](../Page/學校.md "wikilink")[社團演唱以外](../Page/社團.md "wikilink")，他亦有在[電視上演出](../Page/電視.md "wikilink")。雖然Joe
Junior聲線嘹亮，但於無綫參演的角色聲綫多以溫和爲主。而他於2011年無線劇集[天與地飾演Dr](../Page/天與地.md "wikilink").
Dylan中一句「This city is dying, you
know?」，令他廣為人知。2015年9月，他與[胡楓及女歌手](../Page/胡楓.md "wikilink")[黑妹合作舉行](../Page/李麗霞.md "wikilink")「香港經典演唱會」。

## 演出作品

### 電視劇（[無綫電視](../Page/無綫電視.md "wikilink")）

  - 1997年
      - [狀王宋世傑](../Page/狀王宋世傑.md "wikilink") 飾
        [法官](../Page/法官.md "wikilink")
  - 1998年
      - [扫黄先锋](../Page/扫黄先锋.md "wikilink") 饰 老鬼
  - 1999年
      - [狀王宋世傑II](../Page/狀王宋世傑II.md "wikilink") 飾 梵堤多
      - [騙中傳奇](../Page/騙中傳奇.md "wikilink") 飾 江守言
      - [先生貴性](../Page/先生貴性.md "wikilink") 飾 梁祖蔭
      - [茶是故鄉濃](../Page/茶是故鄉濃.md "wikilink") 飾 蒙扎特
  - 2000年
      - [陀槍師姐II](../Page/陀槍師姐II.md "wikilink") 飾 Uncle Roy
      - [倚天屠龍記](../Page/倚天屠龍記.md "wikilink") 飾 平等王
      - [男親女愛](../Page/男親女愛.md "wikilink") 飾 盧亨利
  - 2001年
      - [廉政追擊之足球夢](../Page/廉政追擊.md "wikilink") 飾 辯方律師
      - [酒是故鄉醇](../Page/酒是故鄉醇.md "wikilink")
      - [封神榜](../Page/封神榜.md "wikilink") 飾 莊公公
  - 2002年
      - [皆大歡喜](../Page/皆大歡喜_\(古裝電視劇\).md "wikilink") 飾 羅剎國王
      - [絕世好爸](../Page/絕世好爸.md "wikilink") 飾 財叔
      - [談判專家](../Page/談判專家_\(電視劇\).md "wikilink") 飾 鬼佬
  - 2003年
      - [智勇新警界](../Page/智勇新警界.md "wikilink")
      - [衛斯理](../Page/衛斯理.md "wikilink") 飾 Uncle Sam
      - [西關大少](../Page/西關大少.md "wikilink") 飾
        馬田[校長](../Page/校長.md "wikilink")（[廣州商會學校](../Page/廣州.md "wikilink")）
  - 2004年
      - 飾 游智輝友人

      - [棟篤神探](../Page/棟篤神探.md "wikilink") 飾 法官 (客串)
  - 2005年
      - [皆大歡喜](../Page/皆大歡喜_\(時裝電視劇\).md "wikilink") 飾 遊客 (客串)
      - [楚漢驕雄](../Page/楚漢驕雄.md "wikilink") 飾 喀隆
      - [隨時候命](../Page/隨時候命.md "wikilink") 飾 蔣學占 (Jimmy叔)
      - [御用閒人](../Page/御用閒人.md "wikilink")
      - [學警雄心](../Page/學警雄心.md "wikilink")
      - [妙手仁心III](../Page/妙手仁心III.md "wikilink")
      - [窈窕熟女](../Page/窈窕熟女.md "wikilink") 飾 村長
      - [翻新大少](../Page/翻新大少.md "wikilink") 飾 根叔
  - 2006年
      - [刑事情報科](../Page/刑事情報科.md "wikilink") 飾 毛國邦 (CIB主管)
      - [鳳凰四重奏](../Page/鳳凰四重奏.md "wikilink") 飾 「日星之夜」歌唱比賽參賽者 (第19集)
      - [滙通天下](../Page/滙通天下_\(電視劇\).md "wikilink") 飾
        施丹([法蘭西商人](../Page/法蘭西.md "wikilink"))
  - 2007年
      - [溏心風暴](../Page/溏心風暴.md "wikilink") 飾
        [法官](../Page/法官.md "wikilink")
      - [廉政行動2007](../Page/廉政行動2007.md "wikilink") 飾
        [校長](../Page/校長.md "wikilink")
  - 2008年
      - [古靈精探](../Page/古靈精探.md "wikilink") 飾 費格遜
      - [疑情別戀](../Page/疑情別戀.md "wikilink") 飾 董仁和朋友
      - [當狗愛上貓](../Page/當狗愛上貓.md "wikilink") 飾 白
  - 2010年
      - [秋香怒點唐伯虎](../Page/秋香怒點唐伯虎.md "wikilink") 飾 欽天監
      - [飛女正傳](../Page/飛女正傳.md "wikilink") 飾 鍾耀成
      - [談情說案](../Page/談情說案.md "wikilink") 飾 Robinson
      - [天天天晴](../Page/天天天晴.md "wikilink") 飾
        唐[醫生](../Page/醫生.md "wikilink")
      - [巾幗梟雄之義海豪情](../Page/巾幗梟雄之義海豪情.md "wikilink") 飾
        唐[老闆](../Page/老闆.md "wikilink")
      - [刑警](../Page/刑警_\(無綫電視劇\).md "wikilink") 飾
        [法官](../Page/法官.md "wikilink")
  - 2011年
      - [七號差館](../Page/七號差館_\(電視劇\).md "wikilink") 飾
        黎[醫生](../Page/醫生.md "wikilink")
      - [誰家灶頭無煙火](../Page/誰家灶頭無煙火.md "wikilink") 飾 Paco
      - [點解阿Sir係阿Sir](../Page/點解阿Sir係阿Sir.md "wikilink") 飾 Mark
      - [花花世界花家姐](../Page/花花世界花家姐.md "wikilink") 飾 Baggio
      - [怒火街頭](../Page/怒火街頭.md "wikilink") 飾 馬迪倫
      - [潛行狙擊](../Page/潛行狙擊.md "wikilink") 飾 法官
      - [紫禁驚雷](../Page/紫禁驚雷.md "wikilink") 飾 徐太醫
      - [天與地](../Page/天與地_\(無綫電視劇\).md "wikilink") 飾 Dr. Dylan
  - 2012年
      - [缺宅男女](../Page/缺宅男女.md "wikilink") 飾 施逸文
      - [換樂無窮](../Page/換樂無窮.md "wikilink") 飾 Joe
      - [愛·回家 (第一輯)](../Page/愛·回家_\(第一輯\).md "wikilink") 飾
        凡叔（第1集）、蘇校長（第124-125集）
      - [耀舞長安](../Page/耀舞長安.md "wikilink") 飾 西域師父
      - [心戰](../Page/心戰_\(電視劇\).md "wikilink") 飾 Brian
      - [護花危情](../Page/護花危情.md "wikilink") 飾 鍾爵士
      - [回到三國](../Page/回到三國.md "wikilink") 飾 曹大威（第25集）
      - [天梯](../Page/天梯_\(電視劇\).md "wikilink") 飾 費度（第25集）
      - [名媛望族](../Page/名媛望族.md "wikilink") 飾 安德魯神父
      - [大太監](../Page/大太監.md "wikilink") 飾 傳教士
  - 2013年
      - [初五啟市錄](../Page/初五啟市錄.md "wikilink") 飾 岑老闆
      - [仁心解碼II](../Page/仁心解碼II.md "wikilink") 飾 麥永佳（第15-19集）
      - [神探高倫布](../Page/神探高倫布.md "wikilink") 飾 金路易
      - [好心作怪](../Page/好心作怪.md "wikilink") 飾 郝名翰
      - [熟男有惑](../Page/熟男有惑.md "wikilink") 飾 馬大訟
      - [神鎗狙擊](../Page/神鎗狙擊.md "wikilink") 飾 蔡萬承
      - [On Call 36小時II](../Page/On_Call_36小時II.md "wikilink") 飾 何振金
      - [舌劍上的公堂](../Page/舌劍上的公堂.md "wikilink") 飾 劉父老
  - 2014年
      - [叛逃](../Page/叛逃_\(電視劇\).md "wikilink") 飾 Stephen
      - [愛我請留言](../Page/愛我請留言.md "wikilink") 飾 Paul
      - [愛·回家 (第一輯)](../Page/愛·回家_\(第一輯\).md "wikilink") 飾 嚴格
      - [寒山潛龍](../Page/寒山潛龍.md "wikilink") 飾 單眼英
      - [使徒行者](../Page/使徒行者.md "wikilink") 飾 法官
      - [大藥坊](../Page/大藥坊.md "wikilink") 飾 湯神父
      - [飛虎II](../Page/飛虎II.md "wikilink") 飾 舒啟達
  - 2015年
      - [八卦神探](../Page/八卦神探.md "wikilink") 飾 四大天王
      - [宦海奇官](../Page/宦海奇官.md "wikilink") 飾 范西斯神父
      - [天眼](../Page/天眼_\(無綫電視劇\).md "wikilink")
      - [以和為貴](../Page/以和為貴.md "wikilink") 飾 麥明明
      - [華麗轉身](../Page/華麗轉身.md "wikilink") 飾 楊生
      - [鬼同你OT](../Page/鬼同你OT.md "wikilink") 飾 貝一鳴
      - [陪著你走](../Page/陪著你走_\(電視劇\).md "wikilink") 飾 從父
  - 2016年
      - [鐵馬戰車](../Page/鐵馬戰車.md "wikilink") 飾 刀允才
      - [愛·回家 (第二輯)](../Page/愛·回家_\(第二輯\).md "wikilink") 飾 嚴格
      - [愛·回家之八時入席](../Page/愛·回家之八時入席.md "wikilink") 飾 伍向榮
      - [純熟意外](../Page/純熟意外.md "wikilink") 飾 黃伯
      - [超能老豆](../Page/超能老豆.md "wikilink") 飾 龔先生
      - [為食神探](../Page/為食神探.md "wikilink") 飾 甘大偉
      - [幕後玩家](../Page/幕後玩家.md "wikilink") 飾 富豪
      - [流氓皇帝](../Page/流氓皇帝_\(2016年電視劇\).md "wikilink") 飾 羅白神父
      - [巾帼枭雄之谍血长天](../Page/巾帼枭雄之谍血长天.md "wikilink") 飾 馬洛神父
  - 2017年
      - [乘勝狙擊](../Page/乘勝狙擊.md "wikilink") 飾 神父
      - [同盟](../Page/同盟.md "wikilink") 飾 游嘉華 (客串)
      - [老表，畢業喇！](../Page/老表，畢業喇！.md "wikilink") 飾 J神父
  - 2018年
      - [性在有情](../Page/性在有情.md "wikilink") 飾 張伯
      - [波士早晨](../Page/波士早晨.md "wikilink") 飾 榮福全
      - [愛·回家之開心速遞](../Page/愛·回家之開心速遞.md "wikilink") 飾 神父（第276集）
      - [三個女人一個「因」](../Page/三個女人一個「因」.md "wikilink") 飾 Benny（第11集）

### 電影

  - 半斤八兩 (1976) ... 後巷遇劫事主之一
  - 喝采 (1980)
  - 摩登保鑣 (1981) ... 賭 客
  - 我愛夜來香 (1983)
  - 呷醋大丈夫 (1987)
  - [籠民](../Page/籠民.md "wikilink") (1992)
  - [新不了情](../Page/新不了情.md "wikilink") (1993)
  - [愛的種籽](../Page/愛的種籽.md "wikilink") (1994)
  - [運財智叻星](../Page/運財智叻星.md "wikilink") 飾 神仙
  - [橫紋刀劈扭紋柴](../Page/橫紋刀劈扭紋柴_\(電影\).md "wikilink") 飾 羅家英(1995)
  - [新與龍共舞之偷偷愛你](../Page/新與龍共舞之偷偷愛你.md "wikilink")(1996)
  - [天才與白痴](../Page/天才與白痴.md "wikilink")(1997)
  - [特種飛虎](../Page/特種飛虎.md "wikilink") (1997)
  - 强奸3 : OL诱惑 (1998)
  - [新古惑仔之少年激鬪篇](../Page/新古惑仔之少年激鬪篇.md "wikilink") (1998)
  - [美少年の戀](../Page/美少年の戀.md "wikilink")
  - [没有你，沒有我](../Page/没有你，沒有我.md "wikilink") (2000)
  - 真愛 (2000)
  - [老夫子2001](../Page/老夫子2001.md "wikilink") (2001)
  - [陰陽路十四之雙鬼拍門](../Page/陰陽路十四之雙鬼拍門.md "wikilink") (2002)
  - [賭俠之人定勝天](../Page/賭俠之人定勝天.md "wikilink") (2003)
  - [刑警兄弟](../Page/刑警兄弟.md "wikilink") 飾 犀利哥 (2016)
  - [我的情敵女婿](../Page/我的情敵女婿.md "wikilink") 飾 (2018)

### 舞台劇

  - 感受鄧麗君

### 綜藝節目

  - 2013年：[三個小生去旅行](../Page/三個小生去旅行.md "wikilink")
  - 2015年：[兄弟幫](../Page/兄弟幫.md "wikilink")
  - 2015年：[今日VIP](../Page/今日VIP.md "wikilink")
  - 2015年：[四個小生去旅行](../Page/四個小生去旅行.md "wikilink")
  - 2015年：[超強選擇1分鐘](../Page/超強選擇1分鐘.md "wikilink")
  - 2015年：[今晚睇李](../Page/今晚睇李.md "wikilink")
  - 2016年: [湊仔攻略](../Page/湊仔攻略.md "wikilink")
  - 2018年:[街坊廚神重出江湖](../Page/街坊廚神重出江湖.md "wikilink")

### 廣告

  - 2014年、2015年：惠康超級市場
  - 2015年：旅遊事務署、保險業監理處「旅遊保險睇清楚」

## 注釋

<div class="references-small">

</div>

## 參考資料

## 外部連結

  - [UBeat Magazine Online：Joe Junior:
    我等了廿五年\!](http://www.com.cuhk.edu.hk/ubeat_past/991132/junior_s.htm)

[Category:香港男歌手](../Category/香港男歌手.md "wikilink")
[Category:無綫電視男藝員](../Category/無綫電視男藝員.md "wikilink")
[Category:香港天主教徒](../Category/香港天主教徒.md "wikilink")
[Category:土生葡人](../Category/土生葡人.md "wikilink")
[Category:新法書院校友](../Category/新法書院校友.md "wikilink")
[Category:聖若瑟書院校友](../Category/聖若瑟書院校友.md "wikilink")
[Category:香港甘草演員](../Category/香港甘草演員.md "wikilink")

1.  [明報周刊第](../Page/明報周刊.md "wikilink")2248期，頁144。
2.  [三個小生去旅行 -
    第八集](http://programme.tvb.com/foodandtravel/threeamigosbonvoyage/episode/8/)