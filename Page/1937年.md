## 大事记

### [4月](../Page/4月.md "wikilink")

  - [4月1日](../Page/4月1日.md "wikilink")——[台北州立台北第三中學校成立](../Page/台北州立台北第三中學校.md "wikilink")，校長為[大欣鐵馬](../Page/大欣鐵馬.md "wikilink")。[4月20日](../Page/4月20日.md "wikilink")，正式上課。
  - [4月24日](../Page/4月24日.md "wikilink")——[新华书店在延安清凉山成立](../Page/新华书店.md "wikilink")
  - [4月26日](../Page/4月26日.md "wikilink")——[德国空军部队](../Page/德国.md "wikilink")[神鹰军团在](../Page/秃鹰军团.md "wikilink")[西班牙内战中摧毁了](../Page/西班牙内战.md "wikilink")[格尔尼卡城](../Page/格尔尼卡_\(西班牙\).md "wikilink")。

### [5月](../Page/5月.md "wikilink")

  - [5月6日](../Page/5月6日.md "wikilink")——[興登堡號飛船在](../Page/興登堡號飛船.md "wikilink")[美国雷克霍斯特失事焚毁](../Page/美國.md "wikilink")。
  - [5月27日](../Page/5月27日.md "wikilink")——[美國](../Page/美國.md "wikilink")[金门大桥落成](../Page/金门大桥.md "wikilink")。
  - [5月28日](../Page/5月28日.md "wikilink")——[内维尔·张伯伦任](../Page/内维尔·张伯伦.md "wikilink")[英国](../Page/英国.md "wikilink")[首相](../Page/英国首相.md "wikilink")。

### [6月](../Page/6月.md "wikilink")

  - [6月1日](../Page/6月1日.md "wikilink")——[中国共产党代表](../Page/中国共产党.md "wikilink")[周恩来与](../Page/周恩来.md "wikilink")[蒋介石在](../Page/蒋介石.md "wikilink")[庐山谈判](../Page/庐山.md "wikilink")。
  - [6月3日](../Page/6月3日.md "wikilink")——“不爱江山爱美人”，退位后的[温莎公爵迎娶](../Page/温莎公爵.md "wikilink")[华里丝·辛普森](../Page/华里丝·辛普森.md "wikilink")。

### [7月](../Page/7月.md "wikilink")

  - [7月7日](../Page/7月7日.md "wikilink")——[蘆溝橋事變爆發](../Page/蘆溝橋事變.md "wikilink")；日軍在蘆溝橋附近演習，夜11時藉口搜查失縱哨兵，突砲轟宛平城；國軍第二十九軍第三十七師[馮治安部](../Page/馮治安.md "wikilink")[吉星文團奮起抵抗](../Page/吉星文.md "wikilink")，雙方互有傷亡，成為[中國抗日戰爭導火線](../Page/中國抗日戰爭.md "wikilink")\[1\]。
  - [7月17日](../Page/7月17日.md "wikilink")——蔣中正于[庐山发表抗战演说](../Page/庐山.md "wikilink")。
  - [7月29日](../Page/7月29日.md "wikilink")——[日本军攻克](../Page/日本军.md "wikilink")[北平](../Page/北平.md "wikilink")。
  - [7月29日](../Page/7月29日.md "wikilink")——通州保安隊的中國士兵[屠殺日本僑民](../Page/通州事件.md "wikilink")
  - [7月30日](../Page/7月30日.md "wikilink")——日本军攻克[天津市](../Page/天津市.md "wikilink")。

### [8月](../Page/8月.md "wikilink")

  - [8月13日](../Page/8月13日.md "wikilink")——[淞沪会战在上海及其附近爆发](../Page/淞沪会战.md "wikilink")。
  - [8月14日](../Page/8月14日.md "wikilink")——[筧橋空戰](../Page/筧橋空戰.md "wikilink")，[中華民國空軍節的由來](../Page/中華民國空軍.md "wikilink")。
  - [8月22日](../Page/8月22日.md "wikilink")——[中国工农红军改编为](../Page/中国工农红军.md "wikilink")[国民革命军第八路军](../Page/八路军.md "wikilink")。
  - [8月22日](../Page/8月22日.md "wikilink")——中共中央举行[洛川会议](../Page/洛川会议.md "wikilink")。

### [9月](../Page/9月.md "wikilink")

  - [9月2日](../Page/9月2日.md "wikilink")——[香港發生](../Page/香港.md "wikilink")[丁丑風災](../Page/丁丑風災.md "wikilink")（又稱「九．二風災」），造成2,565\[2\]至約11,000人死亡，大量魚棚損毀。
  - [9月23日和](../Page/9月23日.md "wikilink")[9月24日](../Page/9月24日.md "wikilink")——參加江陰保衛戰的中國海軍[寧海級輕巡洋艦被日軍戰機炸沉於](../Page/寧海級輕巡洋艦.md "wikilink")[長江](../Page/長江.md "wikilink")。

### [10月](../Page/10月.md "wikilink")

  - [10月11日](../Page/10月11日.md "wikilink")——日軍進攻[山西](../Page/山西.md "wikilink")，中日兩軍激戰至11月2日由日軍獲勝，史稱[忻口戰役](../Page/忻口戰鬥.md "wikilink")。
  - [10月13日](../Page/10月13日.md "wikilink")——[新四军建军](../Page/新四军.md "wikilink")。

### [11月](../Page/11月.md "wikilink")

  - [11月4日](../Page/11月4日.md "wikilink")——[太原会战中的最后一场战役](../Page/太原会战.md "wikilink")——[太原保卫战打响](../Page/太原保卫战.md "wikilink")，9日，日军攻陷[太原城](../Page/太原城.md "wikilink")。[太原沦陷后](../Page/太原.md "wikilink")，中国[华北地区的大城市已全部沦陷于日军之手](../Page/华北地区.md "wikilink")。
  - [11月11日](../Page/11月11日.md "wikilink")——中国军队撤离[上海市](../Page/上海市_\(中華民國\).md "wikilink")，日军占领[租界以外的半个](../Page/租界.md "wikilink")[上海](../Page/上海.md "wikilink")。
  - [11月20日](../Page/11月20日.md "wikilink")——[國民政府宣布迁都](../Page/國民政府.md "wikilink")[重庆](../Page/重庆.md "wikilink")。

### [12月](../Page/12月.md "wikilink")

  - [12月13日](../Page/12月13日.md "wikilink")——日本军攻克[中國國民政府首都](../Page/中國.md "wikilink")[南京](../Page/南京.md "wikilink")，发生[南京大屠杀](../Page/南京大屠殺.md "wikilink")。
  - [12月14日](../Page/12月14日.md "wikilink")——[日本扶持的傀儡政權](../Page/日本.md "wikilink")[中華民國臨時政府
    (北京)於北京成立](../Page/中華民國臨時政府_\(北京\).md "wikilink")。
  - [12月21日](../Page/12月21日.md "wikilink")——世界上第一部[动画片](../Page/动画.md "wikilink")《[白雪公主和七个小矮人](../Page/白雪公主.md "wikilink")》在[好莱坞卡泰剧场首映](../Page/好莱坞.md "wikilink")。
  - [12月29日](../Page/12月29日.md "wikilink")——[爱尔兰采用新的爱尔兰宪法](../Page/爱尔兰.md "wikilink")，将国名正式定为“爱尔兰”（Éire）。

## 出生

  - [3月23日](../Page/3月23日.md "wikilink")——[鍾景輝](../Page/鍾景輝.md "wikilink")，香港[導演及](../Page/导演.md "wikilink")[演员](../Page/演員.md "wikilink")。
  - [4月22日](../Page/4月22日.md "wikilink")——[杰克·尼科尔森](../Page/杰克·尼科尔森.md "wikilink")，[美国](../Page/美國.md "wikilink")[电影](../Page/电影.md "wikilink")[演员](../Page/演員.md "wikilink")。
  - [4月28日](../Page/4月28日.md "wikilink")——[萨达姆](../Page/萨达姆·侯赛因.md "wikilink")，[伊拉克总统](../Page/伊拉克总统.md "wikilink")。（[2006年逝世](../Page/2006年.md "wikilink")）
  - [5月8日](../Page/5月8日.md "wikilink")——[托马斯·品钦](../Page/托马斯·品钦.md "wikilink")，[美国作家](../Page/美国作家列表.md "wikilink")。
  - [5月11日](../Page/5月11日.md "wikilink")——[釋證嚴](../Page/釋證嚴.md "wikilink")，台灣[慈濟基金會創辦人](../Page/慈濟基金會.md "wikilink")。
  - [7月7日](../Page/7月7日.md "wikilink")——[董建華](../Page/董建華.md "wikilink")，[中華人民共和國](../Page/中华人民共和国.md "wikilink")[香港特別行政區首任](../Page/香港.md "wikilink")[行政長官](../Page/香港特別行政區行政長官.md "wikilink")。
  - [7月29日](../Page/7月29日.md "wikilink")——[桥本龙太郎](../Page/橋本龍太郎.md "wikilink")，曾任[日本首相](../Page/日本內閣總理大臣.md "wikilink")，[自民党](../Page/自由民主党_\(日本\).md "wikilink")[总裁](../Page/自由民主党总裁.md "wikilink")。（[2006年逝世](../Page/2006年.md "wikilink")）
  - [8月8日](../Page/8月8日.md "wikilink")——[达斯汀·霍夫曼](../Page/德斯汀·荷夫曼.md "wikilink")，美国电影演员。
  - [8月15日](../Page/8月15日.md "wikilink")——[本揚·沃拉吉](../Page/本揚·沃拉吉.md "wikilink")，[寮國國家副主席](../Page/老挝.md "wikilink")。
  - [8月17日](../Page/8月17日.md "wikilink")——[劉炳森](../Page/刘炳森.md "wikilink")，中国著名书法家（[2005年逝世](../Page/2005年.md "wikilink")）
  - [8月29日](../Page/8月29日.md "wikilink")——[樂蒂](../Page/樂蒂.md "wikilink")，素有古典美人之稱
    ([1968年逝世](../Page/1968年.md "wikilink"))
  - [9月29日](../Page/9月29日.md "wikilink")——[龔如心](../Page/龔如心.md "wikilink")，[香港](../Page/香港.md "wikilink")[華懋集團主席](../Page/華懋集團.md "wikilink")，[亞洲第一女富豪](../Page/亚洲.md "wikilink")。（[2007年逝世](../Page/2007年.md "wikilink")）
  - [10月6日](../Page/10月6日.md "wikilink")——[夏萍](../Page/夏萍.md "wikilink")，香港[演員](../Page/演員.md "wikilink")。
  - [10月8日](../Page/10月8日.md "wikilink")——[何藩](../Page/何藩.md "wikilink")，香港[演員](../Page/演員.md "wikilink")。
  - [12月](../Page/12月.md "wikilink")——[司马东](../Page/司马东.md "wikilink")，中国钢笔书法家。

## 逝世

  - [2月](../Page/2月.md "wikilink")——[印貞中](../Page/印貞中.md "wikilink")，(年齡不詳)，[中日抗戰時任中國國民黨中央統計局黨務處副處長](../Page/中日抗戰.md "wikilink")。
  - [5月23日](../Page/5月23日.md "wikilink")——[约翰·戴维森·洛克菲勒](../Page/约翰·戴维森·洛克菲勒.md "wikilink")，[美国](../Page/美国.md "wikilink")[企业家](../Page/實業家.md "wikilink")、[慈善家](../Page/慈善家.md "wikilink")。（[1839年出生](../Page/1839年.md "wikilink")）
  - [7月28日](../Page/7月28日.md "wikilink")——[赵登禹](../Page/赵登禹.md "wikilink")、[佟麟阁](../Page/佟麟阁.md "wikilink")，中国抗日将领。
  - [8月14日](../Page/8月14日.md "wikilink")——[乐灵生](../Page/乐灵生.md "wikilink")，[美南浸信会在华传教士](../Page/美南浸信会.md "wikilink")。（[1871年出生](../Page/1871年.md "wikilink")）
  - [9月2日](../Page/9月2日.md "wikilink")——[皮埃尔·德·顾拜旦](../Page/皮埃尔·德·顾拜旦.md "wikilink")，[法国](../Page/法国.md "wikilink")[教育家](../Page/教育家.md "wikilink")，现代[奥运创始人](../Page/奥运.md "wikilink")。（[1863年出生](../Page/1863年.md "wikilink")）
  - [10月16日](../Page/10月16日.md "wikilink")——[郝梦龄](../Page/郝梦龄.md "wikilink")，国民革命军陆军第9军军长。
  - [10月19日](../Page/10月19日.md "wikilink")——[欧内斯特·卢瑟福](../Page/欧内斯特·卢瑟福.md "wikilink")，[新西兰物理学家](../Page/新西兰.md "wikilink")，[1908年](../Page/1908年.md "wikilink")[诺贝尔化学奖得主](../Page/诺贝尔化学奖.md "wikilink")。（[1881年出生](../Page/1881年.md "wikilink")）
  - [11月25日](../Page/11月25日.md "wikilink")——[王凤仪](../Page/王凤仪.md "wikilink")，中国近代伟大的民间教育家、伦理道德宣传家、女子教育的开拓者。
  - [12月](../Page/12月.md "wikilink")——[萬全策](../Page/萬全策.md "wikilink")，36歲，[中日抗戰時任中央軍校教導總隊第一旅司令部參謀長](../Page/中日抗戰.md "wikilink")。
  - [12月20日](../Page/12月20日.md "wikilink")——[埃里希·魯登道夫](../Page/埃里希·鲁登道夫.md "wikilink")，德国军事家。（[1865年出生](../Page/1865年.md "wikilink")）
  - [12月30日](../Page/12月30日.md "wikilink")——[陆伯鸿](../Page/陆伯鸿.md "wikilink")，中国企业家、慈善家和天主教人士。（[1875年出生](../Page/1875年.md "wikilink")）

## 诺贝尔奖

  - [物理](../Page/诺贝尔物理学奖.md "wikilink")：[克林顿·戴维森和](../Page/克林顿·戴维森.md "wikilink")[乔治·汤姆孙](../Page/乔治·汤姆孙.md "wikilink")
  - [化学](../Page/诺贝尔化学奖.md "wikilink")：[沃尔特·诺尔曼·霍沃思和](../Page/沃尔特·诺尔曼·霍沃思.md "wikilink")[保罗·卡雷](../Page/保罗·卡雷.md "wikilink")
  - [生理和医学](../Page/诺贝尔生理学或医学奖.md "wikilink")：[聖捷爾吉·阿爾伯特](../Page/聖捷爾吉·阿爾伯特.md "wikilink")
  - [文学](../Page/诺贝尔文学奖.md "wikilink")：
    [罗歇·马丁·杜·伽尔](../Page/罗歇·马丁·杜·伽尔.md "wikilink")
  - [和平](../Page/诺贝尔和平奖.md "wikilink")： Viscount Cecil of Chelwood

## [奥斯卡金像奖](../Page/奥斯卡金像奖.md "wikilink")

（第10届，[1938年颁发](../Page/1938年.md "wikilink")）

  - [奥斯卡最佳影片奖](../Page/奥斯卡最佳影片奖.md "wikilink")——《[左拉传](../Page/左拉传.md "wikilink")》
  - [奥斯卡最佳导演奖](../Page/奥斯卡最佳导演奖.md "wikilink")——[利奥·麦凯里](../Page/利奥·麦凯里.md "wikilink")（Leo
    McCarey）《[春闺风月](../Page/春闺风月.md "wikilink")》
  - [奥斯卡最佳男主角奖](../Page/奥斯卡最佳男主角奖.md "wikilink")——[史宾塞·屈赛](../Page/史宾塞·屈赛.md "wikilink")《[怒海余生](../Page/怒海余生.md "wikilink")》
  - [奥斯卡最佳女主角奖](../Page/奥斯卡最佳女主角奖.md "wikilink")——[露薏絲·蕾娜](../Page/露薏絲·蕾娜.md "wikilink")（Luise
    Rainer）《[大地](../Page/大地_\(1937年电影\).md "wikilink")》
  - [奥斯卡最佳男配角奖](../Page/奥斯卡最佳男配角奖.md "wikilink")——[约瑟夫·希尔德克劳特](../Page/约瑟夫·希尔德克劳特.md "wikilink")（Joseph
    Schildkraut）《左拉传》
  - [奥斯卡最佳女配角奖](../Page/奥斯卡最佳女配角奖.md "wikilink")——[艾丽思·布雷迪](../Page/艾丽思·布雷迪.md "wikilink")（Alice
    Brady）《[芝加哥大火记](../Page/芝加哥大火记.md "wikilink")》

（其他奖项参见[奥斯卡金像奖获奖名单](../Page/奥斯卡金像奖.md "wikilink")）

## [國家公園](../Page/國家公園.md "wikilink")

[12月27日](../Page/12月27日.md "wikilink")，台灣總督府國立公園委員會成立[次高太魯閣國立公園](../Page/次高太魯閣國立公園.md "wikilink")

## 資料來源

[\*](../Category/1937年.md "wikilink")
[7年](../Category/1930年代.md "wikilink")
[3](../Category/20世纪各年.md "wikilink")

1.
2.  [港英政府關於丁丑風災的修訂報告](../Page/港英政府.md "wikilink")