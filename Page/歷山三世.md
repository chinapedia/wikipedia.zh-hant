[教宗](../Page/教宗.md "wikilink")**亞歷山大三世**（，約1105年—1181年8月30日），本名羅蘭多巴弟內利**Orlando
Bandinelli**，於1159年9月7日至1181年8月30日岀任教宗。從14世紀以來，他被認為是Baldinelli貴族家族中的成員，但這還沒有被證實。\[1\]

亞歷山大三世是神學及法律家。1140年他在[波羅那教書時](../Page/波羅那.md "wikilink")，寫了一本法學[大綱](../Page/大綱.md "wikilink")，而且為[格拉齊安敕令注釋](../Page/格拉齊安敕令.md "wikilink")，另外則寫有神學論文多篇。他在教宗[尤金三世](../Page/尤金三世.md "wikilink")（Eugenius
III）時代被擢升為執事級樞機，在教宗[亞得里安四世](../Page/亞得里安四世.md "wikilink")（Hadrianus
IV）時為最得力的顧問\[2\]。

[腓特烈一世](../Page/腓特烈一世.md "wikilink")（巴巴羅沙、腓勒德力Frederick
Barbarossa），是中古世紀德國最偉大的皇帝，他夢想仿效[查理曼能有效的統治義大利](../Page/查理曼.md "wikilink")。教宗[亞得里安四世看出了他的野心](../Page/亞得里安四世.md "wikilink")，如果不抑制腓特烈的野心，他除了會成為全歐洲的主人外，連教會都會受他的控制\[3\]。1159年，亞得里安死後，半數的[樞機們選出了一位精通法典又意志堅強的卓越人物擔任教宗](../Page/樞機.md "wikilink")，就是亞歷山大三世。其餘的少數樞機們，得到羅馬教士及皇帝的支持，另選一人為教皇，稱[威克多四世](../Page/威克多四世.md "wikilink")（Victor
Ⅳ），雖然選舉具有合法性，但腓特烈對亞歷山大三世的堅強感到不安，所以他支持威克多，甚至召集一個教會會議，叫他們公開表示擁護威克多。但是亞歷山大三世受到[英格蘭](../Page/英格蘭.md "wikilink")，[法蘭西](../Page/法蘭西.md "wikilink")，[西班牙](../Page/西班牙.md "wikilink")，[匈牙利](../Page/匈牙利.md "wikilink")，及[西西里的支持](../Page/西西里.md "wikilink")\[4\]。這種作法使得教會的自主性受到了威脅，亞歷山大三世不得不處理，他將腓特烈和偽教宗開除了教籍。可是[德國大軍已壓境](../Page/德國.md "wikilink")，他只好先放棄[羅馬前往](../Page/羅馬.md "wikilink")[法國避難](../Page/法國.md "wikilink")，亞歷山大三世聯合[義大利各城作戰](../Page/義大利.md "wikilink")，雙方互有勝負，戰爭拖了17年，1176年皇軍在[萊拿鬧](../Page/萊拿鬧.md "wikilink")（Legnan）慘敗，腓特烈不得不認輸。1177年，亞歷山大三世當時在[威尼斯](../Page/威尼斯.md "wikilink")（Venice），腓特烈前去求饒，在[聖馬爾谷大殿前跪伏亞歷山大三世腳下請求寬恕](../Page/聖馬爾谷.md "wikilink")。亞歷山大三世把腓特烈拉起來，恢復了他的教籍，並賜他一個「平安之吻」（Kiss
of Peace）\[5\]，表示寬恕。至此，腓特烈企圖控制教宗的計畫破滅，他只能對德國境內各主教行使其威權\[6\]。

在亞歷山大三世的任內，[坎特伯雷](../Page/坎特伯雷.md "wikilink")[大主教](../Page/大主教.md "wikilink")[托马斯·贝克特](../Page/托马斯·贝克特.md "wikilink")(Thomas
Becket)和英王[亨利二世在教會法庭的獨立權力問題上發生嚴重爭論](../Page/亨利二世.md "wikilink")\[7\]。亨利二世在1164年[克拉林敦憲章裡](../Page/克拉林敦憲章.md "wikilink")，禁止教會向羅馬上訴，衝突的結果，柏克特被放逐到[法國](../Page/法國.md "wikilink")。1170年聖誕，柏克特回到[坎特伯里](../Page/坎特伯里.md "wikilink")，在他自己的祭壇前被殺害。基督徒震怒，立刻奉柏克特為殉道者。亞歷山大三世在1173年封柏克特為[聖徒](../Page/聖徒.md "wikilink")，並訂立法規，規定以後一切敕封聖徒是[教皇特定的權力](../Page/教皇.md "wikilink")。1179年，亞歷山大召開[第三次拉特朗公會議](../Page/第三次拉特朗公會議.md "wikilink")，為改革通過法案\[8\]。亞歷山大三世死在羅馬近郊[（Civita
Castellana）](../Page/（Civita_Castellana）.md "wikilink")，他的屍體安葬在[拉特郎堂內](../Page/拉特郎堂.md "wikilink")\[9\]。

## 亞歷山大的政治

亞歷山大三世是第一位直接關注波羅的海以東地區宣教活動的教宗，他可能於1164年在瑞典設立了[烏普薩拉大主教](../Page/烏普薩拉.md "wikilink")，\[10\]
這件事是在他最親密的朋友[隆德大主教埃斯基爾的建議下做成的](../Page/隆德.md "wikilink")。
埃斯基爾曾經任命一位[本篤會的修士福克為](../Page/本篤會.md "wikilink")[愛沙尼亞的主教](../Page/愛沙尼亞.md "wikilink")，但他因為和丹麥國王有衝突，而被流放到法國的克萊爾沃。1171年亞歷山大成為第一位解決芬蘭教會問題的[教宗](../Page/教宗.md "wikilink")，據稱芬蘭人騷擾牧師並且只有在戰爭時期才倚靠上帝\[11\]
亞歷山大不僅擊敗了[巴巴羅薩](../Page/巴巴羅薩.md "wikilink")（德語：Friedrich I
Barbarossa，腓特烈一世（紅鬍子）
(神聖羅馬帝國)），還因著1170年發生的[托馬斯·貝克特謀殺案](../Page/托馬斯·貝克特.md "wikilink")，使英格蘭國王[亨利二世屈服](../Page/亨利二世.md "wikilink")。他和貝克特的關係非同尋常，後來在1173年，貝克特被封為聖徒。\[12\]
\[13\]
這是由亞歷山大封聖的第二位聖徒，而第一位是1161年的[懺悔者愛德華](../Page/懺悔者愛德華.md "wikilink")。\[14\]
不過，他在1172年確認了亨利愛爾蘭之領主的地位。
通過1179年5月23日簽發的[教宗詔令](../Page/教宗.md "wikilink")，他承認了[阿方索一世葡萄牙國王的權利](../Page/阿方索一世.md "wikilink")，而這個國王的稱號是阿方索自封的。這在葡萄牙成為一個被世界承認的獨立王國的進程中，是非常重要的一步。（阿方索從1139年開始就已經在使用國王的稱號。）\[15\]

## 改革的成就

即使作為一名逃亡者，亞歷山大仍然受到法國[路易七世的青睞和保護](../Page/路易七世.md "wikilink")。
1163年，他召集來自英國，法國，意大利，和西班牙的牧師與主教參加[圖爾會議](../Page/圖爾.md "wikilink")(Council
of Tours)，除其他事項外，主要解決以下幾個問題：教會聖職的非法劃分、教會牧者放高利貸，占用十一奉獻。\[16\]
1179年三月，[亞歷山大三世召開第三次拉特朗](../Page/亞歷山大三世.md "wikilink")[大公會議](../Page/大公會議.md "wikilink")（即[第三次拉特朗公會議](../Page/第三次拉特朗公會議.md "wikilink")
英文：The Third Council of the
Lateran），這是中世紀最重要的教會會議之一，天主教稱之為第十一次[大公會議](../Page/大公會議.md "wikilink")。這個法案包括了好幾個[教宗為改善教會狀況而製定的計劃書](../Page/教宗.md "wikilink")，其中規定，如果未達到三分之二[樞機主教的選票](../Page/樞機主教.md "wikilink")，沒有人可以被選為[教宗](../Page/教宗.md "wikilink")。\[17\]
該規則於1996年略有改變，但2007年又恢復原狀。這次會議標誌著[亞歷山大三世的權力達到了頂峰](../Page/亞歷山大三世.md "wikilink")。
然而，這次會議結束後不久，羅馬共和國就迫使[亞歷山大三世離開](../Page/亞歷山大三世.md "wikilink")[羅馬這座城市](../Page/羅馬.md "wikilink")，他從此就再也沒有重新回到[羅馬](../Page/羅馬.md "wikilink")。1179年9月29日，一些貴族建立了反[亞歷山大三世聯盟](../Page/亞歷山大三世.md "wikilink")。但教宗通過明智地使用金錢，仍然把握大權，控制局面，結果他在1180年1月被世俗權貴免職。1181年，[亞歷山大三世開除了蘇格蘭國王](../Page/亞歷山大三世.md "wikilink")[威廉一世的教籍](../Page/威廉一世.md "wikilink")，並且禁止他參加任何聖事活動。\[18\]
他於1181年8月30日在[奇維塔卡斯泰拉納](../Page/奇維塔卡斯泰拉納.md "wikilink")(Civita
Castellana)去世。

## 參考文獻

<div class="references-small">

<references />

</div>

## 譯名列表

  - 亞歷山大：[天主教香港教區禮儀委員會：禧年專頁](http://catholic-dlc.org.hk/2000.htm)作、[《大英簡明百科知識庫》2005年版](http://wordpedia.britannica.com/concise/quicksearch.aspx?keyword=alexander&mode=3)、《[世界人名翻譯大辭典](../Page/世界人名翻譯大辭典.md "wikilink")》1993年版作亞歷山大。
  - 歷山：[香港天主教教區檔案　歷任教宗](https://web.archive.org/web/20051023073732/http://archives.catholic.org.hk/popes/index.htm)作歷山。
  - 亞力山大：[真理辯證中文網 兩千年教會歷史巡禮 第四篇
    中世紀前期教會歷史](https://web.archive.org/web/20080721104238/http://www.cftfc.com/churchhistory/Big5/history/04.htm)作亞力山大。
  - 亞力山大、亞歷山大：[國立編譯舘](http://www.nict.gov.tw/tc/dic/index1.php)作亞力山大、亞歷山大。

[Category:1181年逝世](../Category/1181年逝世.md "wikilink")
[A](../Category/教宗.md "wikilink")
[Category:義大利出生的教宗](../Category/義大利出生的教宗.md "wikilink")

1.  Maleczek, W. (1984). Papst und Kardinalskolleg von 1191 bis 1216 (in
    German). Wien. p. 233 note 168. ISBN 3-7001-0660-2.
2.  鄒保祿，《歷代教宗簡史》，(台南：聞道出版社，1983)，167。
3.  幕啟蒙，《天主教史（卷二）》，侯景文譯， (台北：光啟文化事業，2002)，124。
4.  培克爾，《基督教史略》，蕭維元譯， (香港：浸信會出版部，1961)，159。
5.  祈柏爾，《基督的軌跡－二千年教會史》，李林靜芝譯， (台北：校園書房出版社，1986)，147。
6.  華爾克，《基督教會史》，謝受靈、趙毅之譯， (香港：基督教文藝出版社，1987)，450。
7.  比爾奧斯汀，《基督教發展史》，馬傑偉、許建人譯， (香港：種籽出版有限公司，1991)，205-206。
8.  陶理，《基督教二千年史》，李伯明、林牧野譯， (香港：海天書樓有限公司，2007)，290。
9.  鄒保祿，《歷代教宗簡史》，(台南：聞道出版社，1983)，169。
10. Papal Letters to Scandinavia and their Preservation, Anders Winroth,
    Charters, Cartularies and Archives: The Preservation and
    Transmission of Documents in the Medieval West, ed. Adam J. Kosto
    and Anders Winroth, (Pontifical Institute of Mediaeval Studies,
    2002), 178.
11. "Letter by Pope Alexander III to the Archbishop of Uppsala".
    Archived from the original on 2007-09-27.. In Latin. Hosted by the
    National Archive of Finland. See \[1\] and Diplomatarium Fennicum
    from the menu.
12. Norton, Christopher (2006). St. William of York. Rochester, NY:
    Boydell Press. p. 193. ISBN 1-903153-17-4.
13. St. Thomas Becket. Biography.com. Retrieved: 21 March 2013.
14. Norton, Christopher (2006). St. William of York. Rochester, NY:
    Boydell Press. p. 193. ISBN 1-903153-17-4.
15. Peter Linehan and Janet Laughland Nelson, The Medieval World,
    Vol.10, (Routledge, 2001), 524.
16. Pennington, Kenneth. "Pope Alexander III", The Great Popes through
    History: An Encyclopedia, (Frank J. Coppa, ed.), Westport: Greenwood
    Press, (2002) 1.113-122
17. Joseph F. Kelly, The Ecumenical Councils of the Catholic Church: A
    History, (Liturgical Press, 2009), 83.
18. Joseph F. Kelly, The Ecumenical Councils of the Catholic Church: A
    History, (Liturgical Press, 2009), 83.