**胡卫东**（），[江苏](../Page/江苏.md "wikilink")[徐州人](../Page/徐州.md "wikilink")，[中国](../Page/中国.md "wikilink")[篮球](../Page/篮球.md "wikilink")[运动员](../Page/运动员.md "wikilink")；身高1.98米，司职[得分后卫](../Page/得分后卫.md "wikilink")，战袍号码为8号。在中国篮坛号称「中国乔丹」。

## 职业生涯

胡卫东1985年加入江苏省青年队，1987年加入国家青年队，1991年至2002年在中国国家队服役；他弹跳力好、技巧全面，除了最為人们所稱道的远投能力外，在進攻觀察以及傳球能力方面，也是中國、甚至全[亞洲男籃球员的佼佼者](../Page/亞洲.md "wikilink")。1999年，还荣获[亚洲](../Page/亚洲.md "wikilink")[最有价值球员的称号](../Page/最有价值球员.md "wikilink")。

1998年，[达拉斯小牛队通过对胡卫东的长期观察](../Page/达拉斯小牛队.md "wikilink")，决定提供给胡卫东正式的NBA十天合同，但是胡卫东未能成行，2000年3月17日，[奥兰多魔术队传真](../Page/奥兰多魔术队.md "wikilink")，希望胡卫东签一个10天合同，因为伤病，胡卫东再次错过机会。
2005年8月1日，“中国乔丹圆梦NBA”仪式在[北京朝阳公园进行](../Page/北京.md "wikilink")，小牛前总裁兼主教练[老尼尔森向已经退役的胡卫东赠送了小牛队的球衣](../Page/老尼尔森.md "wikilink")。

在中國國內聯賽部份，胡衛東是1990年代的代表性人物；曾两度获得[中國男子籃球職業聯賽](../Page/中國男子籃球職業聯賽.md "wikilink")（CBA）[最有价值球员](../Page/最有价值球员.md "wikilink")、抢断王、助攻王，以及三度获得得分王、三分球王。

## 教练生涯

2004年至2005年胡卫东以球员身份兼任[江苏南钢龙篮球队](../Page/江苏南钢龙篮球队.md "wikilink")[主教练](../Page/主教练.md "wikilink")，2005年胡卫东正式[退役并担任球队](../Page/退役.md "wikilink")[主教练直至](../Page/主教练.md "wikilink")2008年。

2011年12月21日，由于球队战绩不佳，[江苏南钢龙篮球队火线换帅](../Page/江苏南钢龙篮球队.md "wikilink")，由之前担任教练组第一[助理教练的胡卫东重新出任主教练一职](../Page/助理教练.md "wikilink")，但这并没有改变江苏南钢的命运，最后球队创历史的排名联赛垫底，客场甚至遭到全败，胡卫东被迫下课。

2013年，胡卫东确定出任江苏省篮球队主教练备战辽宁[全运会](../Page/全运会.md "wikilink")，而并非是时任江苏南钢的主教练[邱大宗](../Page/邱大宗.md "wikilink")。

## 个人荣誉

  - 1999年 亚洲最有价值球员
  - 1999年 被选为新中国篮球运动50杰之一
  - 两届CBA最有价值球员
  - 三届CBA得分王、三分王
  - 两届CBA抢断王、助攻王
  - 六届江苏省十佳运动员

## 外部链接

  - [胡卫东中文网站](http://www.southcn.com/sports/star5/huweidong/) @ 南方网体育频道

[W](../Category/胡姓.md "wikilink")
[Category:徐州籍运动员](../Category/徐州籍运动员.md "wikilink")
[Category:中国男子篮球运动员](../Category/中国男子篮球运动员.md "wikilink")
[Category:中国奥运篮球运动员](../Category/中国奥运篮球运动员.md "wikilink")
[Category:中国篮球教练](../Category/中国篮球教练.md "wikilink")
[Category:江苏南钢队球员](../Category/江苏南钢队球员.md "wikilink")
[Category:1992年夏季奥林匹克运动会篮球运动员](../Category/1992年夏季奥林匹克运动会篮球运动员.md "wikilink")
[Category:1996年夏季奥林匹克运动会篮球运动员](../Category/1996年夏季奥林匹克运动会篮球运动员.md "wikilink")
[Category:2000年夏季奧林匹克運動會籃球運動員](../Category/2000年夏季奧林匹克運動會籃球運動員.md "wikilink")
[Category:1994年亚洲运动会金牌得主](../Category/1994年亚洲运动会金牌得主.md "wikilink")
[Category:1998年亞洲運動會金牌得主](../Category/1998年亞洲運動會金牌得主.md "wikilink")
[Category:2002年亞洲運動會銀牌得主](../Category/2002年亞洲運動會銀牌得主.md "wikilink")