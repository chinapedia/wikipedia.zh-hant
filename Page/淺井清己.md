**淺井清己**（）是[日本](../Page/日本.md "wikilink")[女性](../Page/女性.md "wikilink")[声優](../Page/声優.md "wikilink")，生於[東京都](../Page/東京都.md "wikilink")。属[D-COLOR事務所](../Page/D-COLOR.md "wikilink")。

## 人物特點

  - 非常愛玩[角子機及](../Page/角子機.md "wikilink")[電視遊戲](../Page/电视游戏.md "wikilink")。
  - 擁有日本商工會[簿記](../Page/簿記.md "wikilink")2級和[珠算](../Page/珠算.md "wikilink")2級資格。

## 主要出演作品

### 電視動畫

  - [小魔女Doremi](../Page/小魔女Doremi.md "wikilink")（佐藤なつみ）
  - [火影忍者](../Page/火影忍者.md "wikilink")（アゲハ、日向ハナビ）
  - [死神](../Page/死神.md "wikilink")（莉莉妮特）

**1997年**

  - [烈火之炎](../Page/烈火之炎.md "wikilink")（森川願子）

**1999年**

  - [麻辣教師GTO](../Page/麻辣教師GTO.md "wikilink")（淺野麻由子、宮崎、オルテガ）

**2000年**

  - [學校怪談](../Page/學校怪談_\(動畫\).md "wikilink")（學生們、運動會的廣播、女學生、江藤綾）

**2002年**

  - [东京喵喵](../Page/东京喵喵.md "wikilink")（達路、ホンチャ、女學生、アナウンス、壞心腸的女學生、部員、狙われた女性）
  - [最终兵器少女](../Page/最终兵器少女.md "wikilink")（せいこ、娘）

**2003年**

  - [魔偵探洛基](../Page/魔偵探洛基.md "wikilink")（グリンブルスティ）
  - [恋爱小魔女](../Page/恋爱小魔女.md "wikilink")（バンブー）
  - [願此刻永恆](../Page/願此刻永恆.md "wikilink")（大空寺亞由）
  - [星球流浪記](../Page/星球流浪記.md "wikilink")（梅洛里（幼年時代））

**2004年**

  - [舞-HiME](../Page/舞-HiME.md "wikilink")（深優·葛利亞）
  - [神無月巫女](../Page/神無月巫女.md "wikilink")（イズミ）

**2005年**

  - [舞-乙HiME](../Page/舞-乙HiME.md "wikilink")（深優、卡拉·貝里尼）

**2006年**

  - [櫻蘭高校男公關部](../Page/櫻蘭高校男公關部.md "wikilink")（櫻塚希美子、宗像亞弓、少年、小學部時期的女孩）

**2007年**

  - [DARKER THAN BLACK
    -黑之契約者-](../Page/DARKER_THAN_BLACK.md "wikilink")（July）

**2009年**

  - [花冠之淚](../Page/花冠之淚.md "wikilink")（愛寶那）
  - [DARKER THAN BLACK
    -流星之雙子-](../Page/DARKER_THAN_BLACK.md "wikilink")（**July**）

**2010年**

  - [神奇寶貝鑽石&珍珠](../Page/神奇寶貝鑽石&珍珠.md "wikilink")（真鳥）
  - [神奇寶貝超級願望](../Page/神奇寶貝超級願望.md "wikilink")（真鳥）

**2011年**

  - [我們沒有翅膀](../Page/我們沒有翅膀.md "wikilink")（日野英里子）

**2013年**

  - [神奇寶貝XY](../Page/神奇寶貝XY.md "wikilink")（真鳥）

**2017年**

  - [精靈寶可夢 太陽&月亮](../Page/精靈寶可夢_太陽&月亮.md "wikilink")（真鳥\[1\]）

### OVA

  - [Akane Maniax](../Page/願此刻永恆.md "wikilink")（大空寺あゆ）
  - [Princess Holiday](../Page/Princess_Holiday.md "wikilink")（戴安娜）

### 網絡動畫

  - [あゆまゆ劇場](../Page/願此刻永恆.md "wikilink")（大空寺あゆ）

### 遊戲

  - [櫻花大戰V](../Page/櫻花大戰.md "wikilink")（蘭丸）
  - [舞-HiME](../Page/舞-HiME.md "wikilink")（深優·グリーア）
  - [洛克人ZX](../Page/洛克人ZX.md "wikilink")（深淵鮟鱇魚‧露鈴莉）

### 外語配音

  - [哈利·波特與密室](../Page/哈利·波特.md "wikilink")（角色不明）

## 參考資料

## 外部連結

  - [淺井清己](http://d-color.co.jp/talent/girls)

[Category:日本女性配音員](../Category/日本女性配音員.md "wikilink")
[Category:1974年出生](../Category/1974年出生.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")

1.