[GAIN-Ad-image001.png](https://zh.wikipedia.org/wiki/File:GAIN-Ad-image001.png "fig:GAIN-Ad-image001.png")

**GAIN**是**Gator Advertising and Information
Network**的簡寫，是臭名昭著的[間諜軟體製造商](../Page/間諜軟體.md "wikilink")[Claria股份有限公司的三家子公司之一](../Page/Claria股份有限公司.md "wikilink")。總部設於[美國](../Page/美國.md "wikilink")[加州](../Page/加州.md "wikilink")[紅木城](../Page/紅木城.md "wikilink")，於1998年由[Denis
Coleman成立](../Page/Denis_Coleman.md "wikilink")。它的特色是經常利用「[訴諸恐懼](../Page/訴諸恐懼.md "wikilink")」的技巧，利用虛假的廣告假扮系統的警告，使新手或使用電腦程度較淺的用者墮入陷阱，在電腦系統中安裝了他們公司的間諜軟件。另一方面，他們的間諜軟件亦是出名很難清除。

GAIN的母公司**Claria集團**原名**Gator集團**，這與他們的旗艦“產品”Gator而得名。Gator從首度出現到現在，已經侵入過超過三千萬台電腦。由於Gator為集團帶來豐厚的廣告收入，2003年10月，Gator集團籌備[上市](../Page/上市.md "wikilink")。但由於Gator帶來的惡名，所以公司決定把集團改名為Claria。不過，但這並沒有為公司重新建立正名的形象，而到現在Claria還未能上市。而清除Gator的問題在各方面多年投訴之後，到了2005年才有改善。到了2006年第二季，Claria完全退出了廣告軟件業\[1\]，到2008年10月，公司完全關閉。不過，正到現在，仍然有上百萬的電腦依然被安裝了來自Gator的軟件。

## ErrorGuard

**ErrorGuard**是一個由GAIN製作的[间谍软件](../Page/间谍软件.md "wikilink")，聲稱可以「幫你解決你的電腦問題」。但其實它是間諜軟件的外殼程序，借著為你的電腦掃描之際而在你的電腦安裝上超過7隻[Spyware](../Page/Spyware.md "wikilink")。ErrorGuard通常透過模仿Windows的錯誤信息來吸引用戶安裝。

[Malware
Destructor](../Page/Malware_Destructor.md "wikilink")\[2\]：聲稱是「最有效的反間諜軟件」，透過[彈出視窗來恐嚇用戶](../Page/彈出視窗.md "wikilink")，指他們在瀏覽[兒童色情網頁](../Page/兒童色情.md "wikilink")，而要向[FBI告發](../Page/FBI.md "wikilink")，從而誘騙用戶安裝他們的軟件\[3\]\[4\]。事後，有其他用戶發現它其實亦是間諜軟件\[5\]。

## Errorsafe

  - Adware：在网页上方或后方的弹出广告的软件，此时主用户界面还不可见，或与产品没有什么关联。
  - Annoyance：仅对用户造成妨碍的任何特洛伊木马程序，例如将屏幕上的文字上下颠倒，或使鼠标莫名其妙地移动。

於2006年1月發佈。

## GAIN公司的間諜軟件帶來的問題

現時已知GAIN的間諜軟件會帶來以下問題：

  - 它會把[入境事務處網站的換領](../Page/入境事務處.md "wikilink")[智能身份證的](../Page/香港身份證.md "wikilink")[Pop-up錯誤以為是廣告而把它攔截](../Page/Pop-up.md "wikilink")，並把Pop-up換成他們的賭博廣告。

## 參看

  - [間諜軟件](../Page/間諜軟件.md "wikilink")
  - [ErrorSafe](../Page/ErrorSafe.md "wikilink")

<references />

## 外部連結

  - [如何把ErrorGuard從你的電腦上移除（英語）](https://web.archive.org/web/20050523081511/http://www.spyany.com/program/article_adw_rm_ErrorGuard.html)
  - [ErrorGuard（英語）](https://web.archive.org/web/20050105103546/http://www.webuser.co.uk/cgi-bin/forums/showflat.pl?Cat=)

[Category:間諜軟件](../Category/間諜軟件.md "wikilink")

1.  根據Claria企業的新聞稿*Claria exiting adware
    business*，於2006-04-30發表，原來網址：http://www.claria.com/companyinfo/press/releases/pr060321.html
2.  [官方網頁](http://malwaredestructor.com/)
3.  [Rip-off Report Malwaredestructor Warning. WARNING.
    MALWAREDESTRUCTOR IS INTIMIDATING PEOPLE INTO BUYING THEIR SOFTWARE
    Internet](http://www.ripoffreport.com/reports/0/273/RipOff0273798.htm)
4.  [馬來西亞雅虎知識](http://malaysia.answers.yahoo.com/question/index?qid=20070909073517AAdv55e)
5.  [\> Geeks to Go\! \> Security \> Malware Removal - HijackThis™ Logs
    Go
    Here](http://www.geekstogo.com/forum/New-virus-guess-t149877.html)