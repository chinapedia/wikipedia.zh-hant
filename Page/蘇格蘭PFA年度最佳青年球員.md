**蘇格蘭PFA年度最佳青年球員**（）於每年[蘇格蘭球季末頒發](../Page/蘇格蘭.md "wikilink")，由球員工會[PFA蘇格蘭](../Page/蘇格蘭職業足球員協會.md "wikilink")（PFA
Scotland）的會員投票選出上年度的最佳年青球員。提名名單於每年4月份公佈，而在稍後於[格拉斯哥舉行的頒獎典禮中揭曉獲獎者](../Page/格拉斯哥.md "wikilink")。

## 歷屆獲獎人

以下為歷屆獲獎人的名單：

<table>
<thead>
<tr class="header">
<th><p>年度</p></th>
<th><p>球員</p></th>
<th><p>球會</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/1977–78_in_Scottish_football.md" title="wikilink">1977–78年</a></p></td>
<td><p><a href="../Page/Graeme_Payne.md" title="wikilink">格林美·潘恩</a>（Graeme Payne）</p></td>
<td><p><a href="../Page/邓迪联足球俱乐部.md" title="wikilink">-{zh-hans:邓迪联; zh-hant:登地聯;}-</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1978–79_in_Scottish_football.md" title="wikilink">1978–79年</a></p></td>
<td><p><a href="../Page/Ray_Stewart_(footballer).md" title="wikilink">雷文·史超活</a>（Raymond Stewart）</p></td>
<td><p><a href="../Page/邓迪联足球俱乐部.md" title="wikilink">-{zh-hans:邓迪联; zh-hant:登地聯;}-</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1979–80_in_Scottish_football.md" title="wikilink">1979–80年</a></p></td>
<td><p><a href="../Page/John_MacDonald_(footballer).md" title="wikilink">約翰·麥當奴</a>（John MacDonald）</p></td>
<td><p><a href="../Page/流浪者足球俱乐部.md" title="wikilink">-{zh-hans:流浪者; zh-hant:格拉斯哥流浪;}-</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1980–81_in_Scottish_football.md" title="wikilink">1980–81年</a></p></td>
<td><p><a href="../Page/Charlie_Nicholas.md" title="wikilink">查理·尼古拉斯</a>（Charlie Nicholas）</p></td>
<td><p><a href="../Page/凯尔特人足球俱乐部.md" title="wikilink">-{zh-hans:凯尔特人; zh-hant:些路迪;}-</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1981–82_in_Scottish_football.md" title="wikilink">1981–82年</a></p></td>
<td><p><a href="../Page/Frank_McAvennie.md" title="wikilink">法蘭·麥艾雲尼</a>（Frank McAvennie）</p></td>
<td><p><a href="../Page/圣米伦足球俱乐部.md" title="wikilink">-{zh-hans:圣米伦; zh-hant:聖美倫;}-</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1982–83_in_Scottish_football.md" title="wikilink">1982–83年</a></p></td>
<td><p><a href="../Page/Paul_McStay.md" title="wikilink">保羅·麥史地</a>（Paul McStay）</p></td>
<td><p><a href="../Page/凯尔特人足球俱乐部.md" title="wikilink">-{zh-hans:凯尔特人; zh-hant:些路迪;}-</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1983–84_in_Scottish_football.md" title="wikilink">1983–84年</a></p></td>
<td><p><a href="../Page/John_Robertson_(footballer_born_1964).md" title="wikilink">約翰·羅拔臣</a>（John Robertson）</p></td>
<td><p><a href="../Page/哈茨足球俱乐部.md" title="wikilink">-{zh-hans:哈茨; zh-hant:赫斯;}-</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1984–85_in_Scottish_football.md" title="wikilink">1984–85年</a></p></td>
<td><p><a href="../Page/Craig_Levein.md" title="wikilink">奇治·尼雲</a>（Craig Levein）</p></td>
<td><p><a href="../Page/哈茨足球俱乐部.md" title="wikilink">-{zh-hans:哈茨; zh-hant:赫斯;}-</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1985–86_in_Scottish_football.md" title="wikilink">1985–86年</a></p></td>
<td><p><a href="../Page/Craig_Levein.md" title="wikilink">奇治·尼雲</a>（Craig Levein）</p></td>
<td><p><a href="../Page/哈茨足球俱乐部.md" title="wikilink">-{zh-hans:哈茨; zh-hant:赫斯;}-</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1986–87_in_Scottish_football.md" title="wikilink">1986–87年</a></p></td>
<td><p><a href="../Page/Robert_Fleck.md" title="wikilink">羅拔·費力克</a>（Robert Fleck）</p></td>
<td><p><a href="../Page/流浪者足球俱乐部.md" title="wikilink">-{zh-hans:流浪者; zh-hant:格拉斯哥流浪;}-</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1987–88_in_Scottish_football.md" title="wikilink">1987–88年</a></p></td>
<td><p><a href="../Page/John_Collins_(footballer).md" title="wikilink">約翰·哥連斯</a>（John Collins）</p></td>
<td><p><a href="../Page/希伯尼安足球俱乐部.md" title="wikilink">-{zh-hans:希伯尼安; zh-hant:喜百年;}-</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1988–89_in_Scottish_football.md" title="wikilink">1988–89年</a></p></td>
<td><p><a href="../Page/比利·麥堅尼.md" title="wikilink">比利·麥堅尼</a>（Billy McKinlay）</p></td>
<td><p><a href="../Page/邓迪联足球俱乐部.md" title="wikilink">-{zh-hans:邓迪联; zh-hant:登地聯;}-</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1989–90_in_Scottish_football.md" title="wikilink">1989–90年</a></p></td>
<td><p><a href="../Page/Scott_Crabbe.md" title="wikilink">史葛·加力比</a>（Scott Crabbe）</p></td>
<td><p><a href="../Page/哈茨足球俱乐部.md" title="wikilink">-{zh-hans:哈茨; zh-hant:赫斯;}-</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1990–91_in_Scottish_football.md" title="wikilink">1990–91年</a></p></td>
<td><p><a href="../Page/Eoin_Jess.md" title="wikilink">伊恩·傑斯</a>（Eoin Jess）</p></td>
<td><p><a href="../Page/阿伯丁足球俱乐部.md" title="wikilink">-{zh-hans:阿伯丁; zh-hant:鴨巴甸;}-</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1991–92_in_Scottish_football.md" title="wikilink">1991–92年</a></p></td>
<td><p><a href="../Page/菲臘·奧當尼.md" title="wikilink">-{zh-hans:菲尔·奥多内尔; zh-hant:菲臘·奧當奴;}-</a>（Phil O'Donnell）</p></td>
<td><p><a href="../Page/马瑟韦尔足球俱乐部.md" title="wikilink">馬瑟韋爾</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1992–93_in_Scottish_football.md" title="wikilink">1992–93年</a></p></td>
<td><p><a href="../Page/Eoin_Jess.md" title="wikilink">伊恩·傑斯</a>（Eoin Jess）</p></td>
<td><p><a href="../Page/阿伯丁足球俱乐部.md" title="wikilink">-{zh-hans:阿伯丁; zh-hant:鴨巴甸;}-</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1993–94_in_Scottish_football.md" title="wikilink">1993–94年</a></p></td>
<td><p><a href="../Page/菲臘·奧當尼.md" title="wikilink">-{zh-hans:菲尔·奥多内尔; zh-hant:菲臘·奧當奴;}-</a>（Phil O'Donnell）</p></td>
<td><p><a href="../Page/马瑟韦尔足球俱乐部.md" title="wikilink">馬瑟韋爾</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1994–95_in_Scottish_football.md" title="wikilink">1994–95年</a></p></td>
<td><p><a href="../Page/Charlie_Miller.md" title="wikilink">查理·米拿</a>（Charlie Miller）</p></td>
<td><p><a href="../Page/流浪者足球俱乐部.md" title="wikilink">-{zh-hans:流浪者; zh-hant:格拉斯哥流浪;}-</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1995–96_in_Scottish_football.md" title="wikilink">1995–96年</a></p></td>
<td><p><a href="../Page/杰基·麦克纳马拉.md" title="wikilink">-{zh-hans:杰基·麦克纳马拉; zh-hant:麥拿馬拉;}-</a>（Jackie McNamara）</p></td>
<td><p><a href="../Page/凯尔特人足球俱乐部.md" title="wikilink">-{zh-hans:凯尔特人; zh-hant:些路迪;}-</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1996–97_in_Scottish_football.md" title="wikilink">1996–97年</a></p></td>
<td><p><a href="../Page/Robbie_Winters.md" title="wikilink">羅比·溫達斯</a>（Robbie Winters）</p></td>
<td><p><a href="../Page/邓迪联足球俱乐部.md" title="wikilink">-{zh-hans:邓迪联; zh-hant:登地聯;}-</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1997–98_in_Scottish_football.md" title="wikilink">1997–98年</a></p></td>
<td><p><a href="../Page/加里·奈史密斯.md" title="wikilink">-{zh-hans:加里·奈史密斯; zh-hant:加利·尼史密夫;}-</a>（Gary Naysmith）</p></td>
<td><p><a href="../Page/哈茨足球俱乐部.md" title="wikilink">-{zh-hans:哈茨; zh-hant:赫斯;}-</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1998–99_in_Scottish_football.md" title="wikilink">1998–99年</a></p></td>
<td><p><a href="../Page/巴里·弗格森.md" title="wikilink">-{zh-hans:巴里·弗格森; zh-hant:巴利·費格遜;}-</a>（Barry Ferguson）</p></td>
<td><p><a href="../Page/流浪者足球俱乐部.md" title="wikilink">-{zh-hans:流浪者; zh-hant:格拉斯哥流浪;}-</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1999–2000_in_Scottish_football.md" title="wikilink">1999–00年</a></p></td>
<td><p><a href="../Page/肯尼·米勒.md" title="wikilink">-{zh-hans:肯尼·米勒; zh-hant:堅尼·米拿;}-</a>（Kenny Miller）</p></td>
<td><p><a href="../Page/希伯尼安足球俱乐部.md" title="wikilink">-{zh-hans:希伯尼安; zh-hant:喜百年;}-</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2000–01_in_Scottish_football.md" title="wikilink">2000–01年</a></p></td>
<td><p><a href="../Page/斯蒂利揚·彼得羅夫.md" title="wikilink">-{zh-hans:彼得罗夫;zh-hk:柏度夫;zh-tw:彼得羅夫;}-</a>（Stilian Petrov）</p></td>
<td><p><a href="../Page/凯尔特人足球俱乐部.md" title="wikilink">-{zh-hans:凯尔特人; zh-hant:些路迪;}-</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2001–02_in_Scottish_football.md" title="wikilink">2001–02年</a></p></td>
<td><p><a href="../Page/凯文·麦克诺顿.md" title="wikilink">-{zh-hans:凯文·麦克诺顿; zh-hant:麥諾頓;}-</a>（Kevin McNaughton）</p></td>
<td><p><a href="../Page/阿伯丁足球俱乐部.md" title="wikilink">-{zh-hans:阿伯丁; zh-hant:鴨巴甸;}-</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2002–03_in_Scottish_football.md" title="wikilink">2002–03年</a></p></td>
<td><p><a href="../Page/詹姆斯·麦克法登.md" title="wikilink">-{zh-hans:詹姆斯·麦克法登; zh-hant:麥法頓;}-</a>（James McFadden）</p></td>
<td><p><a href="../Page/马瑟韦尔足球俱乐部.md" title="wikilink">馬瑟韋爾</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2003–04_in_Scottish_football.md" title="wikilink">2003–04年</a></p></td>
<td><p><a href="../Page/斯蒂芬·皮尔森.md" title="wikilink">-{zh-hans:斯蒂芬·皮尔森; zh-hant:皮雅臣;}-</a>（Stephen Pearson）</p></td>
<td><p><a href="../Page/凯尔特人足球俱乐部.md" title="wikilink">-{zh-hans:凯尔特人; zh-hant:些路迪;}-</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2004–05_in_Scottish_football.md" title="wikilink">2004–05年</a></p></td>
<td><p><a href="../Page/德雷克·里奥丹.md" title="wikilink">-{zh-hans:德雷克·里奥丹; zh-hant:里奧丹;}-</a>（Derek Riordan）</p></td>
<td><p><a href="../Page/希伯尼安足球俱乐部.md" title="wikilink">-{zh-hans:希伯尼安; zh-hant:喜百年;}-</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2005–06_in_Scottish_football.md" title="wikilink">2005–06年</a></p></td>
<td><p><a href="../Page/肖恩·马洛尼.md" title="wikilink">-{zh-hans:肖恩·马洛尼; zh-hant:馬隆尼;}-</a>（Shaun Maloney）</p></td>
<td><p><a href="../Page/凯尔特人足球俱乐部.md" title="wikilink">-{zh-hans:凯尔特人; zh-hant:些路迪;}-</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2006–07_in_Scottish_football.md" title="wikilink">2006–07年</a></p></td>
<td><p><a href="../Page/Steven_Naismith.md" title="wikilink">-{zh-hans:斯蒂文·奈史密斯; zh-hant:史提芬·尼史密夫;}-</a>（Steven Naismith）</p></td>
<td><p><a href="../Page/基马诺克足球俱乐部.md" title="wikilink">-{zh-hans:基马诺克; zh-hant:基爾馬諾克;}-</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2007–08_in_Scottish_football.md" title="wikilink">2007–08年</a></p></td>
<td><p><a href="../Page/埃登·麦克格迪.md" title="wikilink">-{zh-hans:埃登·麦克格迪; zh-hk:艾登·麥基迪;}-</a>（Aiden McGeady）<small>[1]</small></p></td>
<td><p><a href="../Page/凯尔特人足球俱乐部.md" title="wikilink">-{zh-hans:凯尔特人; zh-hant:些路迪;}-</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2008–09_in_Scottish_football.md" title="wikilink">2008–09年</a></p></td>
<td><p><a href="../Page/詹姆斯·麦卡锡.md" title="wikilink">詹姆斯·麦卡锡</a>（James McCarthy）<small>[2]</small></p></td>
<td><p><a href="../Page/咸美顿学院足球俱乐部.md" title="wikilink">咸美顿</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2009–10_in_Scottish_football.md" title="wikilink">2009–10年</a></p></td>
<td><p><a href="../Page/丹尼·威爾遜.md" title="wikilink">-{zh-hans:丹尼·威尔森; zh-hk:丹尼·威爾遜;}-</a>（Danny Wilson）<small>[3]</small></p></td>
<td><p><a href="../Page/流浪者足球俱乐部.md" title="wikilink">-{zh-hans:流浪者; zh-hant:格拉斯哥流浪;}-</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2010–11_in_Scottish_football.md" title="wikilink">2010–11年</a></p></td>
<td><p><a href="../Page/大卫·古德威利.md" title="wikilink">-{zh-hk:大卫·古德威利; zh-hk:大衛·葛維尼;}-</a>（David Goodwillie）<small>[4]</small></p></td>
<td><p><a href="../Page/邓迪联足球俱乐部.md" title="wikilink">-{zh-hans:邓迪联; zh-hk:登地聯;}-</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2011–12_in_Scottish_football.md" title="wikilink">2011–12年</a></p></td>
<td><p><a href="../Page/詹姆斯·福里斯特.md" title="wikilink">詹姆斯·福里斯特</a>（James Forrest）[5]</p></td>
<td><p><a href="../Page/凯尔特人足球俱乐部.md" title="wikilink">-{zh-hans:凯尔特人; zh-hk:些路迪;}-</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2012–13_in_Scottish_football.md" title="wikilink">2012–13年</a></p></td>
<td><p><a href="../Page/利·格里菲思.md" title="wikilink">利·格里菲思</a>（Leigh Griffiths）[6]</p></td>
<td><p><a href="../Page/希伯尼安足球俱乐部.md" title="wikilink">-{zh-hans:希伯尼安; zh-hk:喜百年;}-</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2013–14_in_Scottish_football.md" title="wikilink">2013–14年</a></p></td>
<td><p><a href="../Page/安祖·羅拔臣.md" title="wikilink">-{zh-hans:安德鲁·罗伯逊;zh-hk:安祖·羅拔臣;}-</a>[7]</p></td>
<td><p><a href="../Page/邓迪联足球俱乐部.md" title="wikilink">-{zh-hans:邓迪联; zh-hk:登地聯;}-</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參見

  - [蘇格蘭PFA足球先生](../Page/蘇格蘭PFA足球先生.md "wikilink")
  - [蘇格蘭FWA年度最佳青年球員](../Page/蘇格蘭FWA年度最佳青年球員.md "wikilink")

## 註腳

[Category:蘇格蘭足球](../Category/蘇格蘭足球.md "wikilink")
[Category:英国足球奖项](../Category/英国足球奖项.md "wikilink")
[Category:1977年建立的獎項](../Category/1977年建立的獎項.md "wikilink")

1.  [McGeady wins Player of Year
    award](http://news.bbc.co.uk/sport2/hi/football/scot_prem/7357815.stm)
2.
3.
4.
5.
6.
7.