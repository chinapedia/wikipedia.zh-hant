[西夏区](../Page/西夏区.md "wikilink"){{.w}}[灵武市](../Page/灵武市.md "wikilink"){{.w}}[永宁县](../Page/永宁县.md "wikilink"){{.w}}[贺兰县](../Page/贺兰县.md "wikilink")

|group3 = [石嘴山市](../Page/石嘴山市.md "wikilink") |list3 =
[大武口区](../Page/大武口区.md "wikilink"){{.w}}[惠农区](../Page/惠农区.md "wikilink"){{.w}}[平罗县](../Page/平罗县.md "wikilink")

|group4 = [吴忠市](../Page/吴忠市.md "wikilink") |list4 =
[利通区](../Page/利通区.md "wikilink"){{.w}}[红寺堡区](../Page/红寺堡区.md "wikilink"){{.w}}[青铜峡市](../Page/青铜峡市.md "wikilink"){{.w}}[盐池县](../Page/盐池县.md "wikilink"){{.w}}[同心县](../Page/同心县.md "wikilink")

|group5 = [固原市](../Page/固原市.md "wikilink") |list5 =
[原州区](../Page/原州区.md "wikilink"){{.w}}[西吉县](../Page/西吉县.md "wikilink"){{.w}}[隆德县](../Page/隆德县.md "wikilink"){{.w}}[泾源县](../Page/泾源县.md "wikilink"){{.w}}[彭阳县](../Page/彭阳县.md "wikilink")

|group6 = [中卫市](../Page/中卫市.md "wikilink") |list6 =
[沙坡头区](../Page/沙坡头区.md "wikilink"){{.w}}[中宁县](../Page/中宁县.md "wikilink"){{.w}}[海原县](../Page/海原县.md "wikilink")
}}

|belowstyle = text-align: left; font-size: 80%; |below =
参见：[中华人民共和国县级以上行政区列表](../Page/中华人民共和国县级以上行政区列表.md "wikilink")、[宁夏回族自治区乡级以上行政区列表](../Page/宁夏回族自治区乡级以上行政区列表.md "wikilink")。
}}<noinclude>

</noinclude>

[Category:中华人民共和国各省级行政区行政区划模板](../Category/中华人民共和国各省级行政区行政区划模板.md "wikilink")
[\*](../Category/宁夏行政区划.md "wikilink")
[宁夏行政区划模板](../Category/宁夏行政区划模板.md "wikilink")