<div class="notice metadata" id="disambig">

[Disambig.svg](https://zh.wikipedia.org/wiki/File:Disambig.svg "fig:Disambig.svg")<small>本文講述一位作家，若想了解小說人物向達倫，請參考[向達倫大冒險以及](../Page/向達倫大冒險.md "wikilink")[各集小說](../Category/向達倫大冒險系列小說.md "wikilink")。</small>

</div>

**向達倫**( Darren Shan )是愛爾蘭作家**達倫·歐沙納西**( Darren O'Shaughnessy
)的筆名，也是[向達倫大冒險系列的主角名稱](../Page/向達倫大冒險.md "wikilink")（在美國叫做[怪奇馬戲團系列](../Page/怪奇馬戲團系列.md "wikilink")）。

## 簡介

達倫·歐沙納希（Darren
O'Shaughnessy）於1972年7月2日出生於英國倫敦的工人階層家庭，母親是一名小學老師。他住在倫敦東南部，靠近大象城堡的地方。他在三歲的時候開始上學，六歲的時候隨著父母和弟弟移居愛爾蘭的Limerick，從此定居愛爾蘭。他曾在愛爾蘭就讀大學，其後轉往倫敦唸社會學系及英文系。

他從五、六歲起就一直想當作家，十四歲時擁有了第一部打字機，從此開始了寫作生涯，曾獲愛爾蘭RTE編劇比賽第二名。十七歲時完成第一部小說《無言的追逐》，但一直未出版，後曾以本名出版過《愛猶瑪卡》、《地獄之界》二本小說。

二十八歲時開始推出《向達倫大冒險》系列，不但榮獲「IRA-CBC」最佳童書、「親子指南兒童多媒體」傑出圖書獎，更榮登愛爾蘭、[英國](../Page/英國.md "wikilink")、[美國](../Page/美國.md "wikilink")、[日本](../Page/日本.md "wikilink")、[台灣等地的暢銷排行榜](../Page/台灣.md "wikilink")，全球銷量已突破九百萬冊。[環球影業已經購買了](../Page/環球影業.md "wikilink")[向達倫大冒險系列前三集的電影版權](../Page/向達倫大冒險.md "wikilink")，打算拍成一集電影。布萊恩·海格蘭（）正在撰寫劇本，製片是羅蘭·舒勒·唐納（Lauren
Shuler Donner）。片名依照系列名稱取做**怪奇馬戲團**（Cirque du Freak），並將於2008年2月開拍。

向達倫目前正全心發展最新的故事《[魔域大冒險](../Page/魔域大冒險.md "wikilink")》系列，並嘗試不同的寫作策略。新系列不但節奏更快、情節更驚險，而且將出現三個不同的主角，但命運將把各個主角的未來牽繫在一起，最終並導向一個出乎預料的結局，最新一本的小說是《Hell's
Heroes》。

## 著作

### 向達倫大冒險

<table>
<tbody>
<tr class="odd">
<td><p><strong>三部曲篇名</strong></p></td>
<td><p><strong>作品名稱</strong></p></td>
<td><p><strong>出版時間</strong></p></td>
<td><p><strong>中文版資訊</strong></p></td>
<td><p><strong>獲獎紀錄</strong></p></td>
</tr>
<tr class="even">
<td><p>吸血鬼之血</p></td>
<td><p><a href="../Page/怪奇馬戲團.md" title="wikilink">怪奇馬戲團</a><br />
<span style="font-size:smaller;"></span></p></td>
<td><p>2000年1月4日</p></td>
<td><p>2002年7月<a href="../Page/台灣.md" title="wikilink">台灣</a><a href="../Page/皇冠出版.md" title="wikilink">皇冠出版</a></p></td>
<td><p>2001年贏得Sheffield Children's Book Award</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鬼不理的助手.md" title="wikilink">鬼不理的助手</a><br />
<span style="font-size:smaller;"></span></p></td>
<td><p>2000年9月30日</p></td>
<td><p>2002年10月台灣皇冠出版</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/地下血道.md" title="wikilink">地下血道</a><br />
<span style="font-size:smaller;"></span></p></td>
<td><p>2000年11月6日</p></td>
<td><p>2002年12月台灣皇冠出版</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>吸血鬼禮記</p></td>
<td><p><a href="../Page/魔山印石.md" title="wikilink">魔山印石</a><br />
<span style="font-size:smaller;"></span></p></td>
<td><p>2001年1月4日</p></td>
<td><p>2003年4月台灣皇冠出版</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/死亡審判.md" title="wikilink">死亡審判</a><br />
<span style="font-size:smaller;"></span></p></td>
<td><p>2001年10月1日</p></td>
<td><p>2003年7月台灣皇冠出版</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/吸血鬼王子.md" title="wikilink">吸血鬼王子</a><br />
<span style="font-size:smaller;"></span></p></td>
<td><p>2002年2月4日</p></td>
<td><p>2003年10月台灣皇冠出版</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>吸血鬼戰爭</p></td>
<td><p><a href="../Page/薄暮獵人.md" title="wikilink">薄暮獵人</a><br />
<span style="font-size:smaller;"></span></p></td>
<td><p>2002年7月1日</p></td>
<td><p>2003年12月台灣皇冠出版</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/暗夜盟友.md" title="wikilink">暗夜盟友</a><br />
<span style="font-size:smaller;"></span></p></td>
<td><p>2002年11月4日</p></td>
<td><p>2004年4月台灣皇冠出版</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黎明殺手.md" title="wikilink">黎明殺手</a><br />
<span style="font-size:smaller;"></span></p></td>
<td><p>2003年2月3日</p></td>
<td><p>2004年7月台灣皇冠出版</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>吸血鬼命運</p></td>
<td><p><a href="../Page/靈魂之湖.md" title="wikilink">靈魂之湖</a><br />
<span style="font-size:smaller;"></span></p></td>
<td><p>2003年10月6日</p></td>
<td><p>2004年10月台灣皇冠出版</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/暗黑之王.md" title="wikilink">暗黑之王</a><br />
<span style="font-size:smaller;"></span></p></td>
<td><p>2004年1月7日</p></td>
<td><p>2005年1月台灣皇冠出版</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/命運之子.md" title="wikilink">命運之子</a><br />
<span style="font-size:smaller;"></span></p></td>
<td><p>2004年10月4日</p></td>
<td><p>2005年4月台灣皇冠出版</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

<div style="text-align: left;">

### 魔域大冒險

<table>
<thead>
<tr class="header">
<th><p><strong>集數</strong></p></th>
<th><p><strong>作品名稱</strong></p></th>
<th><p><strong>出版時間</strong></p></th>
<th><p><strong>中文版資訊</strong></p></th>
<th><p><strong>年代順序</strong></p></th>
<th><p><strong>時間點</strong></p></th>
<th><p><strong>主角</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><a href="../Page/喪王降臨.md" title="wikilink">喪王降臨</a><br />
<span style="font-size:smaller;"></span></p></td>
<td><p>2005年6月6日</p></td>
<td><p>2005年7月<a href="../Page/台灣.md" title="wikilink">台灣</a><a href="../Page/皇冠出版.md" title="wikilink">皇冠出版</a></p></td>
<td><p>3</p></td>
<td><p>21世紀初</p></td>
<td><p>葛柏</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><a href="../Page/惡魔賊.md" title="wikilink">惡魔賊</a><br />
<span style="font-size:smaller;"></span></p></td>
<td><p>2005年10月5日</p></td>
<td><p>2006年1月台灣皇冠出版</p></td>
<td><p>2</p></td>
<td><p>1970年代中期，較《喪王降臨》早三十年</p></td>
<td><p>康諾</p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p><a href="../Page/嗜血魔.md" title="wikilink">嗜血魔</a><br />
<span style="font-size:smaller;"></span></p></td>
<td><p>2006年6月1日</p></td>
<td><p>2006年7月台灣皇冠出版</p></td>
<td><p>4</p></td>
<td><p>《<a href="../Page/喪王降臨.md" title="wikilink">喪王降臨</a>》一年後</p></td>
<td><p>葛柏</p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p><a href="../Page/女祭司.md" title="wikilink">女祭司</a><br />
<span style="font-size:smaller;"></span></p></td>
<td><p>2006年10月2日</p></td>
<td><p>2007年1月台灣皇冠出版</p></td>
<td><p>1</p></td>
<td><p>西元450年至500年中間，較《惡魔賊》早1500年</p></td>
<td><p>貝可</p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p><a href="../Page/變身狼.md" title="wikilink">變身狼</a><br />
<span style="font-size:smaller;"></span></p></td>
<td><p>2007年6月4日</p></td>
<td><p>2007年7月台灣皇冠出版</p></td>
<td><p>5</p></td>
<td><p>《嗜血魔》一年後</p></td>
<td><p>葛柏</p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p><a href="../Page/魔界大開.md" title="wikilink">魔界大開</a><br />
<span style="font-size:smaller;"></span></p></td>
<td><p>2007年10月1日</p></td>
<td><p>2008年1月台灣皇冠出版</p></td>
<td><p>6</p></td>
<td><p>接續《變身狼》</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p><a href="../Page/死亡幻影.md" title="wikilink">死亡幻影</a><br />
<span style="font-size:smaller;"></span></p></td>
<td><p>2008年5月1日</p></td>
<td><p>2008年7月台灣皇冠出版</p></td>
<td><p>7</p></td>
<td><p>接續《魔界大開》，三本約略同步</p></td>
<td><p>貝可</p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p><a href="../Page/魔狼島.md" title="wikilink">魔狼島</a><br />
<span style="font-size:smaller;"></span></p></td>
<td><p>2008年10月1日</p></td>
<td><p>2009年1月台灣皇冠出版</p></td>
<td><p>葛柏</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p><a href="../Page/黑暗召喚.md" title="wikilink">黑暗召喚</a><br />
<span style="font-size:smaller;"></span></p></td>
<td><p>2009年5月1日</p></td>
<td><p>2009年7月台灣皇冠出版</p></td>
<td><p>康諾</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p><a href="../Page/地獄英雄.md" title="wikilink">地獄英雄</a><br />
<span style="font-size:smaller;"></span></p></td>
<td><p>2009年10月1日</p></td>
<td><p>2010年1月台灣皇冠出版</p></td>
<td><p>8</p></td>
<td><p>接續《黑暗召喚》</p></td>
<td><p>葛柏</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### 起源

《[喪王降臨](../Page/喪王降臨.md "wikilink")》本來只是向達倫所寫的一首詩（附錄在《喪王降臨》書中正文前）。《喪王降臨》和《[魔域大冒險](../Page/魔域大冒險.md "wikilink")》系列是在向達倫創作《[向達倫大冒險](../Page/向達倫大冒險.md "wikilink")》時想到的。他把這本書留到《向達倫大冒險》結束後才出版。最初《魔域大冒險》只是兩本講述兩個分別遇上魔主喪王的小孩的書，但向達倫在寫作這兩本書時想到一個方法把兩個獨立的故事連在一起，形成一系列錯綜複雜、規模龐大的故事。

### 城市三部曲

**城市三部曲**是為成人讀者而寫，並以本名出版。

<table>
<thead>
<tr class="header">
<th><p><strong>集數</strong></p></th>
<th><p><strong>作品名稱</strong></p></th>
<th><p><strong>出版時間</strong></p></th>
<th><p><strong>中文版資訊</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><a href="../Page/愛猶瑪卡名單.md" title="wikilink">愛猶瑪卡名單</a><br />
<span style="font-size:smaller;"></span></p></td>
<td><p>1999年2月<br />
2008年3月重新出版</p></td>
<td><p>2010年6月25日<a href="../Page/台灣.md" title="wikilink">台灣</a><a href="../Page/皇冠出版.md" title="wikilink">皇冠出版</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><a href="../Page/地獄邊界.md" title="wikilink">地獄邊界</a><br />
<span style="font-size:smaller;"></span></p></td>
<td><p>2000年2月<br />
2009年3月重新出版</p></td>
<td><p>2010年9月24日台灣皇冠出版</p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p><a href="../Page/謎幻蛇城.md" title="wikilink">謎幻蛇城</a><br />
<span style="font-size:smaller;"></span></p></td>
<td><p>2010年3月</p></td>
<td><p>2010年12月3日台灣皇冠出版</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 鬼不理大冒險

橫跨兩百年的四部曲故事，主角為《向達倫大冒險》主角之一拉登·鬼不理。

<table>
<thead>
<tr class="header">
<th><p><strong>集數</strong></p></th>
<th><p><strong>作品名稱</strong></p></th>
<th><p><strong>出版時間</strong></p></th>
<th><p><strong>中文版資訊</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><a href="../Page/殺手誕生.md" title="wikilink">殺手誕生</a><br />
<span style="font-size:smaller;"></span></p></td>
<td><p>2010年9月30日</p></td>
<td><p>2011年6月24日<a href="../Page/台灣.md" title="wikilink">台灣</a><a href="../Page/皇冠出版.md" title="wikilink">皇冠出版</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><a href="../Page/噬血迷航.md" title="wikilink">噬血迷航</a><br />
<span style="font-size:smaller;"></span></p></td>
<td><p>2011年4月28日</p></td>
<td><p>2011年11月7日<a href="../Page/台灣.md" title="wikilink">台灣</a><a href="../Page/皇冠出版.md" title="wikilink">皇冠出版</a></p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p><a href="../Page/死者之殿.md" title="wikilink">死者之殿</a><br />
<span style="font-size:smaller;"></span></p></td>
<td><p>2011年10月3日</p></td>
<td><p>2012年2月29日<a href="../Page/台灣.md" title="wikilink">台灣</a><a href="../Page/皇冠出版.md" title="wikilink">皇冠出版</a></p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p><a href="../Page/血盟兄弟.md" title="wikilink">血盟兄弟</a><br />
<span style="font-size:smaller;"></span></p></td>
<td><p>2011年10月3日</p></td>
<td><p>2012年7月2日<a href="../Page/台灣.md" title="wikilink">台灣</a><a href="../Page/皇冠出版.md" title="wikilink">皇冠出版</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### ZOM-B

此套集以貝琪.史密斯為主角

<table>
<thead>
<tr class="header">
<th><p><strong>集數</strong></p></th>
<th><p><strong>作品名稱</strong></p></th>
<th><p><strong>出版時間</strong></p></th>
<th><p><strong>中文版資訊</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><a href="../Page/進擊的殭屍.md" title="wikilink">進擊的殭屍</a><br />
<span style="font-size:smaller;"><a href="../Page/Zom-B.md" title="wikilink">Zom-B</a></span></p></td>
<td><p>2012年9月27日</p></td>
<td><p>2013年8月12日<a href="../Page/台灣.md" title="wikilink">台灣</a><a href="../Page/皇冠出版.md" title="wikilink">皇冠出版</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><a href="../Page/復活者聯盟.md" title="wikilink">復活者聯盟</a><br />
<span style="font-size:smaller;"><a href="../Page/Zom-B_Underground.md" title="wikilink">Zom-B Underground</a></span></p></td>
<td><p>2013年1月3日</p></td>
<td><p>2013年11月11日台灣皇冠出版</p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p><a href="../Page/墓光之城.md" title="wikilink">墓光之城</a><br />
<span style="font-size:smaller;"><a href="../Page/Zom-B_City.md" title="wikilink">Zom-B City</a></span></p></td>
<td><p>2013年3月14日</p></td>
<td><p>2014年2月17日台灣皇冠出版</p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p><a href="../Page/一代宗屍.md" title="wikilink">一代宗屍</a><br />
<span style="font-size:smaller;"><a href="../Page/Zom-B_Angels.md" title="wikilink">Zom-B Angels</a></span></p></td>
<td><p>2013年6月20日</p></td>
<td><p>2014年5月19日台灣皇冠出版</p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p><a href="../Page/嬰屍路.md" title="wikilink">嬰屍路</a><br />
<span style="font-size:smaller;"><a href="../Page/Zom-B_Baby.md" title="wikilink">Zom-B Baby</a></span></p></td>
<td><p>2013年9月26日</p></td>
<td><p>2014年8月18日台灣皇冠出版</p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p><a href="../Page/神鬼戰屍.md" title="wikilink">神鬼戰屍</a><br />
<span style="font-size:smaller;"><a href="../Page/Zom-B_Gladiator.md" title="wikilink">Zom-B Gladiator</a></span></p></td>
<td><p>2014年1月2日</p></td>
<td><p>2014年11月17日台灣皇冠出版</p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p>（暫無中文標題）<br />
<span style="font-size:smaller;"><a href="../Page/Zom-B_Mission.md" title="wikilink">Zom-B Mission</a></span></p></td>
<td><p>2014年3月27日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p>（暫無中文標題）<br />
<span style="font-size:smaller;"><a href="../Page/Zom-B_Clans.md" title="wikilink">Zom-B Clans</a></span></p></td>
<td><p>2014年7月3日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p>（暫無中文標題）<br />
<span style="font-size:smaller;"><a href="../Page/Zom-B_Family.md" title="wikilink">Zom-B Family</a></span></p></td>
<td><p>2014年9月25日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p>（暫無中文標題）<br />
<span style="font-size:smaller;"><a href="../Page/Zom-B_Bride.md" title="wikilink">Zom-B Bride</a></span></p></td>
<td><p>2015年2月24日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p>（暫無中文標題）<br />
<span style="font-size:smaller;"><a href="../Page/Zom-B_Fugitive.md" title="wikilink">Zom-B Fugitive</a></span></p></td>
<td><p>2015年9月10日</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td><p>（暫無中文標題）<br />
<span style="font-size:smaller;"><a href="../Page/Zom-B_Goddess.md" title="wikilink">Zom-B Goddess</a></span></p></td>
<td><p>2016年4月7日</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 其他書籍

1.  *[Koyasan](../Page/Koyasan_\(novel\).md "wikilink")* - A special
    book released for [World Book
    Day](../Page/World_Book_Day.md "wikilink") 2006
2.  *Saga of the Vile Thing*

### 短篇故事

1.  "[Hagurosan](http://www.darrenshan.com/extras/extras/02-hagurosan.html)"
    - Originally written for *Kids' Night In*.
2.  "[Young Alan
    Moore](http://www.darrenshan.com/extras/extras/01-alanmoore.html)" -
    Originally written for *Alan Moore: Portrait of an Extraordinary
    Gentleman*, a book written to celebrate the 50th birthday of [Alan
    Moore](../Page/Alan_Moore.md "wikilink").
3.  "[The Good Ship
    Tree](http://www.darrenshan.com/extras/extras/18-goodshiptree.html)"
    - Originally written for the *[Times Educational
    Supplement](../Page/Times_Educational_Supplement.md "wikilink")*,
    and accepted.
4.  "[Life's a
    Beach](http://www.darrenshan.com/extras/extras/04-lifesabeach.html)"
    - Originally written for the *Times Educational Supplement*, but
    rejected.
5.  "Guyifesto–Who We Are" - Originally written for *Guys Write For Guys
    Read*. (Play on words of
    "[*man*ifesto](../Page/manifesto.md "wikilink")")
6.  *[The Saga of Darren
    Shan](../Page/The_Saga_of_Darren_Shan.md "wikilink")* tie-in short
    stories - *[See
    above](../Page/#The_Saga_of_Darren_Shan.md "wikilink")*

## 參見

  - [黑暗奇幻](../Page/黑暗奇幻.md "wikilink")

## 外部連結

  - [向達倫冒險遊園地（向達倫台灣官方網站）](http://www.crown.com.tw/darrenshan/)

  - [向達倫官方網站](http://www.darrenshan.com/)

  - [向達倫blog](http://darrenshan.blogdrive.com/)

[S](../Category/向達倫大冒險.md "wikilink")
[S](../Category/1972年出生.md "wikilink")
[S](../Category/愛爾蘭作家.md "wikilink")