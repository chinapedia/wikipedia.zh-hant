**永和**是[日本之](../Page/日本.md "wikilink")[南北朝時代的](../Page/南北朝時代_\(日本\).md "wikilink")[年號之一](../Page/年號.md "wikilink")。是[北朝方使用的](../Page/持明院統.md "wikilink")。[應安之後](../Page/應安.md "wikilink")，[康曆之前](../Page/康曆.md "wikilink")。指1375年到1379年的期間。這個時代的[天皇](../Page/天皇.md "wikilink")，北朝方是[後圓融天皇](../Page/後圓融天皇.md "wikilink")。[南朝方是](../Page/大覚寺統.md "wikilink")[長慶天皇](../Page/長慶天皇.md "wikilink")。[室町幕府的將軍是](../Page/室町幕府.md "wikilink")[足利義滿](../Page/足利義滿.md "wikilink")。

## 改元

  - 應安8年[2月27日](../Page/2月27日_\(陰曆\).md "wikilink")（[陽曆](../Page/陽曆.md "wikilink")1375年3月29日）
    改元
  - 永和5年[3月22日](../Page/3月22日_\(陰曆\).md "wikilink")（陽曆1379年4月9日） 改元為康曆

## 出典

出自『[尚書](../Page/書經.md "wikilink")』的「詩言志、歌永言、聲依**永**、律**和**聲、八音克諧、無相奪倫、神人以和」以及『[藝文類聚](../Page/藝文類聚.md "wikilink")』的「九功六義之興、依**永和**聲之製、志由興作、情以詞宣」。

## 永和年間要事

  - 元年

<!-- end list -->

  - 8月、[南朝方的](../Page/南朝_\(日本\).md "wikilink")[橋本正督在室町幕府中大起大落](../Page/橋本正督.md "wikilink")。在[九州](../Page/九州.md "wikilink")，[今川貞世](../Page/今川貞世.md "wikilink")（了俊）於[水島之陣中謀殺了](../Page/水島之陣.md "wikilink")[少弐冬資](../Page/少弐冬資.md "wikilink")。[島津氏久轉投南朝方](../Page/島津氏久.md "wikilink")。

<!-- end list -->

  - 2年

<!-- end list -->

  - 8月、室町幕府為了討伐島津氏久、[島津伊久](../Page/島津伊久.md "wikilink")，將今川貞世任命為[大隅國](../Page/大隅國.md "wikilink")、[薩摩國守護](../Page/薩摩國.md "wikilink")。

<!-- end list -->

  - 3年

<!-- end list -->

  - 6月、[越前國的](../Page/越前國.md "wikilink")[國人與](../Page/國人.md "wikilink")[守護代之爭延燒到](../Page/守護代.md "wikilink")[管領](../Page/管領.md "wikilink")[細川賴之的領地](../Page/細川賴之.md "wikilink")，賴之雨[斯波義將開始對立](../Page/斯波義將.md "wikilink")。
  - 10月、南九州國人歸服今川貞世。

## 紀年、西曆、干支對照表

| 永和                             | 元年                             | 2年                             | 3年                             | 4年                             | 5年                             |
| ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| 西曆                             | 1375年                          | 1376年                          | 1377年                          | 1378年                          | 1379年                          |
| 南朝                             | 天授元年                           | 天授二年                           | 天授三年                           | 天授四年                           | 天授五年                           |
| [干支](../Page/干支.md "wikilink") | [乙卯](../Page/乙卯.md "wikilink") | [丙辰](../Page/丙辰.md "wikilink") | [丁巳](../Page/丁巳.md "wikilink") | [戊午](../Page/戊午.md "wikilink") | [己未](../Page/己未.md "wikilink") |

## 參考

  - [日本年號索引](../Page/日本年號索引.md "wikilink")
  - 其他使用[永和為紀元的政權](../Page/永和.md "wikilink")
  - 同期存在的其他政權之紀年
      - [天授](../Page/天授_\(長慶天皇\).md "wikilink")（1375年五月二十七日至1381年二月十日）：[長慶天皇之年號](../Page/長慶天皇.md "wikilink")
      - [宣光](../Page/宣光_\(元昭宗\).md "wikilink")（1371年正月—1379年六月）：[北元](../Page/北元.md "wikilink")—[元昭宗愛猷識里達臘之年號](../Page/元昭宗.md "wikilink")
      - [洪武](../Page/洪武.md "wikilink")（1368年正月—1398年十二月）：[明朝](../Page/明朝.md "wikilink")—明太祖[朱元璋之年號](../Page/朱元璋.md "wikilink")
      - [隆慶](../Page/隆慶_\(陳睿宗\).md "wikilink")（1373年—1377年）：[陳朝](../Page/陳朝_\(越南\).md "wikilink")—[陳睿宗陳曔之年號](../Page/陳睿宗.md "wikilink")
      - [昌符](../Page/昌符.md "wikilink")（1377年—1387年）：陳朝—陳廢帝[陳晛之年號](../Page/陳晛.md "wikilink")

## 參考文獻

  - 松橋達良，《元号はやわかり—東亜歷代建元考》，砂書房，1994年7月，ISBN 4915818276
  - 李崇智，《中国历代年号考》，中华书局，2001年1月ISBN 7101025129

[Category:14世纪日本年号](../Category/14世纪日本年号.md "wikilink")
[Category:1370年代日本](../Category/1370年代日本.md "wikilink")