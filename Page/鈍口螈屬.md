**鈍口螈屬**（[學名](../Page/學名.md "wikilink")：**）是多種分布於[北美的](../Page/北美.md "wikilink")[蠑螈](../Page/蠑螈.md "wikilink")。體型中等。有明顯的肋骨間溝。尾部側向壓縮。頭部寬大、眼小。不好動。通常棲息於地底。由於[棲息地的破壞](../Page/棲息地的破壞.md "wikilink")，多個物種均為[瀕危物種](../Page/瀕危物種.md "wikilink")。

## 種類

約33種：

  - *A. altamirani* <small>Dugès, 1895</small>
  - [钝头钝口螈](../Page/钝头钝口螈.md "wikilink") - *A. amblycephalum*
    <small>Taylor, 1940</small>
  - *A. andersoni* <small>Krebs and Brandon, 1984</small>
  - [环纹钝口螈](../Page/环纹钝口螈.md "wikilink") - *A. annulatum* <small>Cope,
    1886</small>
  - *A. barbouri* <small>Kraus and Petranka, 1989</small>
  - *A. bombypellum* <small>Taylor, 1940</small>
  - [加州虎紋鈍口螈](../Page/加州虎紋鈍口螈.md "wikilink") - *A. californiense*
    <small>Gray, 1853</small>
  - [网纹钝口螈](../Page/网纹钝口螈.md "wikilink") - *A. cingulatum* <small>Cope,
    1868</small>
  - [鈍口螈](../Page/鈍口螈.md "wikilink") - *A. dumerilii* <small>(Dugès,
    1870)</small>
  - *A. flavipiperatum* <small>Dixon, 1963</small>
  - [棕纤钝口螈](../Page/棕纤钝口螈.md "wikilink") - *A. gracile* <small>(Baird,
    1859)</small>
  - *A. granulosum* <small>Taylor, 1944</small>
  - [杰斐逊钝口螈](../Page/杰斐逊钝口螈.md "wikilink") - *A. jeffersonianum*
    <small>(Green, 1827)</small>
  - [蓝点钝口螈](../Page/蓝点钝口螈.md "wikilink") - *A. laterale*
    <small>Hallowell, 1856</small>
  - *A. leorae* <small>(Taylor, 1943)</small>
  - *A. lermaense* <small>(Taylor, 1940)</small>
  - [马比钝口螈](../Page/马比钝口螈.md "wikilink") - *A. mabeei* <small>Bishop,
    1928</small>
  - [长趾钝口螈](../Page/长趾钝口螈.md "wikilink") - *A. macrodactylum*
    <small>Baird, 1850</small>
  - [斑点钝口螈](../Page/斑点钝口螈.md "wikilink") - *A. maculatum* <small>(Shaw,
    1802)</small>
  - [橫帶虎斑蝾螈](../Page/橫帶虎斑蝾螈.md "wikilink") - *A. mavortium*
    <small>Baird, 1850</small>
  - [美西钝口螈](../Page/美西钝口螈.md "wikilink") - *A. mexicanum* <small>(Shaw
    and Nodder, 1798)</small>
  - [暗斑钝口螈](../Page/暗斑钝口螈.md "wikilink") - *A. opacum*
    <small>(Gravenhorst, 1807)</small>
  - [庸钝口螈](../Page/庸钝口螈.md "wikilink") - *A. ordinarium* <small>Taylor,
    1940</small>
  - *A. rivulare* <small>(Taylor, 1940)</small>
  - [红钝口螈](../Page/红钝口螈.md "wikilink") - *A. rosaceum* <small>Taylor,
    1941</small>
  - [鼹钝口螈](../Page/鼹钝口螈.md "wikilink") - *A. talpoideum*
    <small>(Holbrook, 1838)</small>
  - *A. taylori* <small>Brandon, Maruska and Rumph, 1982</small>
  - [小口钝口螈](../Page/小口钝口螈.md "wikilink") - *A. texanum* <small>(Matthes,
    1855)</small>
  - [虎纹钝口螈](../Page/虎纹钝口螈.md "wikilink") - *A. tigrinum* <small>(Green,
    1825)</small>
  - [高地虎纹钝口螈](../Page/高地虎纹钝口螈.md "wikilink") - *A. velasci*
    <small>(Dugès, 1888)</small>
  - [墨西哥钝口螈](../Page/墨西哥钝口螈.md "wikilink") - *A. mexicanum*<small>(Shaw,
    1789)</small>

## 參考

  -
[Category:蠑螈亞目](../Category/蠑螈亞目.md "wikilink")
[Category:鈍口螈科](../Category/鈍口螈科.md "wikilink")