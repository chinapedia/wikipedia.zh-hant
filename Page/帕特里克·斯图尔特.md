**帕特里克·斯图尔特**[爵士](../Page/爵士.md "wikilink")，[OBE](../Page/OBE.md "wikilink")（Sir
**Patrick
Stewart**，），[英国](../Page/英国.md "wikilink")[电影](../Page/电影.md "wikilink")、[电视剧和](../Page/电视剧.md "wikilink")[舞台剧演员](../Page/舞台.md "wikilink")，曾獲得[艾美奖和](../Page/艾美奖.md "wikilink")[金球奖提名](../Page/金球奖.md "wikilink")，他还是[哈德斯菲尔德大学的名誉校长](../Page/哈德斯菲尔德大学.md "wikilink")。\[1\]在将近50年的时间中，斯图尔特在戏剧事业中享有盛誉，他饰演过许多[莎士比亚笔下的角色](../Page/威廉·莎士比亚.md "wikilink")。不过，也许他最为世人所知的角色是在《[星际旅行：下一代](../Page/星际旅行：下一代.md "wikilink")》中饰演的[星舰进取号的](../Page/联邦星舰进取号_\(NCC-1701-D\).md "wikilink")[让-吕克·皮卡尔舰长](../Page/让-吕克·皮卡尔.md "wikilink")，以及在「[X戰警系列電影](../Page/X戰警系列電影.md "wikilink")」中饰演的[X教授](../Page/X教授.md "wikilink")。

## 个人传记

### 早年生活

斯图尔特出生于[英格兰](../Page/英格兰.md "wikilink")[西约克郡的](../Page/西约克郡.md "wikilink")[莫菲尔德](../Page/莫菲尔德.md "wikilink")。他的父母分别是阿尔弗雷德·斯图尔特（Alfred
Stewart）和格拉迪斯·巴罗克拉夫（Gladys
Barrowclough）。阿尔弗雷德是[英国军队](../Page/英国军队.md "wikilink")[国王嫡系约克郡轻步兵](../Page/国王嫡系约克郡轻步兵.md "wikilink")（）的一名[团级军士长](../Page/团级军士长.md "wikilink")（**RSM**，），在此之前他是一名普通的邮递员。格拉迪斯则是一名[纺织工人](../Page/纺织.md "wikilink")。\[2\]在1951年时，12岁的斯图尔特进入了[莫菲尔德第二现代学校](../Page/莫菲尔德第二现代学校.md "wikilink")（Mirfield
Secondary Modern School，今「The Mirfield Free Grammar」中學）\[3\]。在那里他开始学习戏剧。

15岁时，斯图尔特从学校中辍学，并在当地剧院登台表演。他还取得了一份报社记者的工作，不过在一年后，他的雇主要求他必须在表演工作和新闻工作中作出选择。\[4\]于是，斯图尔特辞掉了记者的工作。

1957年，17岁的斯图尔特来到了[布里斯托老维克戏剧学院](../Page/布里斯托老维克戏剧学院.md "wikilink")，开始了为期两年的戏剧表演学习。在19岁的时候他几乎已经掉光了所有的头发，但他成功地将自己推销给了剧院的制作人。\[5\]

### 事业

在曼彻斯特[图书馆剧院](../Page/图书馆剧院.md "wikilink")（Library
Theatre）表演了一段时期后，斯图尔特在1966年加入了[皇家莎士比亚剧团](../Page/皇家莎士比亚剧团.md "wikilink")。他在[百老汇剧院参与表演的首个戏剧是](../Page/百老汇剧院.md "wikilink")[彼得·布鲁克](../Page/彼得·布鲁克.md "wikilink")（Peter
Brook）导演的传奇作品《[仲夏夜之梦](../Page/仲夏夜之梦.md "wikilink")》，他饰演补锅匠斯诺特。在1980年代早期，他改在[国立皇家剧院](../Page/国立皇家剧院.md "wikilink")（Royal
National
Theatre）演出。在这几年里，斯图尔特出演了许多主流电视剧中的角色，但他却从来没能让自己的名字家喻户晓过。他曾饰演过《Fall
of
Eagles》中的[列宁](../Page/列宁.md "wikilink")，《[罗马帝国兴亡史](../Page/罗马帝国兴亡史.md "wikilink")》（*I,
Claudius*）中的[塞扬努斯](../Page/塞扬努斯.md "wikilink")（Sejanus），《[锅匠、裁缝、士兵、间谍](../Page/锅匠、裁缝、士兵、间谍.md "wikilink")》（*Tinker,
Tailor, Soldier,
Spy*）中的Karla，以及1980年BBC改编版《[哈姆雷特](../Page/哈姆雷特.md "wikilink")》里的克劳迪斯（Claudius）。他还在[BBC改编的](../Page/BBC.md "wikilink")[盖斯凯尔夫人名著](../Page/盖斯凯尔.md "wikilink")《[南与北](../Page/南与北.md "wikilink")》中饰演一名极富浪漫清调的男性（带有假发）。

他还曾成为几部电影的主角。如[John
Boorman](../Page/John_Boorman.md "wikilink")《[黑暗时代](../Page/黑暗时代.md "wikilink")》（*Excalibur*）里的[King
Leondegrance](../Page/Leondegrance.md "wikilink")，[戴维·林奇](../Page/戴维·林奇.md "wikilink")1984年版《[沙丘](../Page/沙丘_\(电影\).md "wikilink")》里的[Gurney
Halleck与](../Page/Gurney_Halleck.md "wikilink")[Tobe
Hooper](../Page/Tobe_Hooper.md "wikilink")《[宇宙天魔](../Page/宇宙天魔.md "wikilink")》（*Lifeforce*）里的阿姆斯特朗博士（Dr.
Armstrong）。

1987年，在出席了[聖塔芭芭拉加利福尼亞大學的一次莎士比亚研讨会后](../Page/聖塔芭芭拉加利福尼亞大學.md "wikilink")，应[吉恩·罗登伯里的邀请](../Page/吉恩·罗登伯里.md "wikilink")，斯图尔特来到了[洛杉矶出演](../Page/洛杉矶.md "wikilink")《[星际旅行：下一代](../Page/星际旅行：下一代.md "wikilink")》（[1987年](../Page/1987年电视.md "wikilink")－1994年）的[让-吕克·皮卡尔上校](../Page/让-吕克·皮卡尔.md "wikilink")。在1994年以后，他还在多部衍生电影中出演了皮卡尔，其中包括：《[星际旅行VII：日换星移](../Page/星际旅行VII：日换星移.md "wikilink")》（[1994](../Page/1994年电影.md "wikilink")）、《[星际旅行VIII：第一类接触](../Page/星际旅行VIII：第一类接触.md "wikilink")》（[1996](../Page/1996年电影.md "wikilink")）、《[星际旅行IX：起义](../Page/星际旅行IX：起义.md "wikilink")》（[1998](../Page/1998年电影.md "wikilink")）与《[星际旅行X：复仇女神](../Page/星际旅行X：复仇女神.md "wikilink")》（[2002](../Page/2002年电影.md "wikilink")）。他还曾在《[星际旅行：深空九号](../Page/星际旅行：深空九号.md "wikilink")》的首集“Emissary”中出演皮卡尔。
在2012年的電影[熊麻吉中擔任旁白](../Page/熊麻吉.md "wikilink")

### 榮譽

2010年為表彰其戲劇貢獻，封為[下級勳位爵士](../Page/下級勳位爵士.md "wikilink")。

## 作品列表

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>标题</p></th>
<th><p>角色</p></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1967</p></td>
<td><p><a href="../Page/加冕街.md" title="wikilink">加冕街</a></p></td>
<td><p>消防员</p></td>
<td><p>第一集</p></td>
</tr>
<tr class="even">
<td><p>1974</p></td>
<td><p><em><a href="../Page/Fall_of_Eagles.md" title="wikilink">Fall of Eagles</a></em></p></td>
<td><p><a href="../Page/列宁.md" title="wikilink">列宁</a></p></td>
<td><p>电视迷你剧</p></td>
</tr>
<tr class="odd">
<td><p>1974</p></td>
<td><p><em><a href="../Page/Antony_and_Cleopatra_(1974_TV_drama).md" title="wikilink">Antony and Cleopatra</a></em></p></td>
<td><p>Enobarbus</p></td>
<td><p>电视电影</p></td>
</tr>
<tr class="even">
<td><p>1975</p></td>
<td><p><em><a href="../Page/Hedda_(film).md" title="wikilink">Hedda</a></em></p></td>
<td><p>Ejlert Løvborg</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1975</p></td>
<td><p><em><a href="../Page/Hennessy_(1975_film).md" title="wikilink">Hennessy</a></em></p></td>
<td><p>Tilney</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1976</p></td>
<td><p><em><a href="../Page/I,_Claudius_(TV_series).md" title="wikilink">我，克劳狄斯</a></em></p></td>
<td><p><a href="../Page/塞扬努斯.md" title="wikilink">塞扬努斯</a></p></td>
<td><p>电视迷你剧</p></td>
</tr>
<tr class="odd">
<td><p>1979</p></td>
<td><p><em><a href="../Page/Tinker,_Tailor,_Soldier,_Spy#Television.md" title="wikilink">Tinker, Tailor, Soldier, Spy</a></em></p></td>
<td><p><a href="../Page/Karla_(fictional_character).md" title="wikilink">Karla</a></p></td>
<td><p>电视迷你剧系列</p></td>
</tr>
<tr class="even">
<td><p>1980</p></td>
<td><p><em><a href="../Page/Little_Lord_Fauntleroy_(1980_film).md" title="wikilink">Little Lord Fauntleroy</a></em></p></td>
<td><p>Wilkins</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1980</p></td>
<td><p><em><a href="../Page/Hamlet.md" title="wikilink">Hamlet, Prince of Denmark</a></em></p></td>
<td><p>Claudius</p></td>
<td><p>电视电影</p></td>
</tr>
<tr class="even">
<td><p>1981</p></td>
<td><p><a href="../Page/神剑.md" title="wikilink">神剑</a></p></td>
<td><p>Leondegrance</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1982</p></td>
<td><p><em></em></p></td>
<td><p>Major</p></td>
<td><p>配音角色</p></td>
</tr>
<tr class="even">
<td><p>1982</p></td>
<td><p><em><a href="../Page/Smiley&#39;s_People#Adaptations.md" title="wikilink">Smiley's People</a></em></p></td>
<td><p>Karla</p></td>
<td><p>电视迷你剧系列</p></td>
</tr>
<tr class="odd">
<td><p>1984</p></td>
<td><p><em>Uindii</em></p></td>
<td><p>Mr. Duffner</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1984</p></td>
<td><p><a href="../Page/沙丘_(电影).md" title="wikilink">沙丘</a></p></td>
<td><p>Gurney Halleck</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1985</p></td>
<td><p><em><a href="../Page/Lifeforce_(film).md" title="wikilink">Lifeforce</a></em></p></td>
<td><p>Dr. Armstrong</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1985</p></td>
<td><p><em><a href="../Page/Wild_Geese_II.md" title="wikilink">Wild Geese II</a></em></p></td>
<td><p>俄罗斯将军</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1985</p></td>
<td><p><em><a href="../Page/Code_Name:_Emerald.md" title="wikilink">Code Name: Emerald</a></em></p></td>
<td><p>Colonel Peters</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1985</p></td>
<td><p><em></em></p></td>
<td><p>Professor Macklin</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1985</p></td>
<td><p><em><a href="../Page/Walls_of_Glass.md" title="wikilink">Walls of Glass</a></em></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1986</p></td>
<td><p><a href="../Page/梦断英伦.md" title="wikilink">梦断英伦</a></p></td>
<td><p><a href="../Page/亨利·格雷，第一代萨福克公爵.md" title="wikilink">亨利·格雷，第一代萨福克公爵</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1987–1994</p></td>
<td><p><a href="../Page/星际旅行：下一代.md" title="wikilink">星际旅行：下一代</a>''</p></td>
<td><p><a href="../Page/让-吕克·皮卡尔.md" title="wikilink">让-吕克·皮卡尔</a></p></td>
<td><p>电视系列剧<br />
Nominated-<a href="../Page/Screen_Actors_Guild_Award_for_Outstanding_Performance_by_a_Male_Actor_in_a_Drama_Series.md" title="wikilink">Screen Actors Guild Award for Outstanding Performance by a Male Actor in a Drama Series</a></p></td>
</tr>
<tr class="even">
<td><p>1987</p></td>
<td><p><em></em></p></td>
<td><p>Anthony Anderson</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1991</p></td>
<td><p><em><a href="../Page/L.A._Story.md" title="wikilink">L.A. Story</a></em></p></td>
<td><p>Mr. Perdue/ <a href="../Page/Maître_d&#39;.md" title="wikilink">Maitre D'</a> at L'Idiot</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1993</p></td>
<td><p><em><a href="../Page/Robin_Hood:_Men_in_Tights.md" title="wikilink">Robin Hood: Men in Tights</a></em></p></td>
<td><p><a href="../Page/理查一世.md" title="wikilink">理查一世</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1993</p></td>
<td><p><em><a href="../Page/Death_Train.md" title="wikilink">Death Train</a></em></p></td>
<td><p>Malcolm Philpott</p></td>
<td><p>电视电影</p></td>
</tr>
<tr class="even">
<td><p>1994</p></td>
<td><p><em><a href="../Page/Gunmen_(film).md" title="wikilink">Gunmen</a></em></p></td>
<td><p>Loomis</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1994</p></td>
<td><p><a href="../Page/星际旅行VII：日换星移.md" title="wikilink">星际旅行VII：日换星移</a></p></td>
<td><p><a href="../Page/让-吕克·皮卡尔.md" title="wikilink">让-吕克·皮卡尔</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1994</p></td>
<td><p><em></em></p></td>
<td><p>Adventure</p></td>
<td><p>配音角色</p></td>
</tr>
<tr class="odd">
<td><p>1994</p></td>
<td><p><em><a href="../Page/Lands_of_Lore:_The_Throne_of_Chaos.md" title="wikilink">Lands of Lore: The Throne of Chaos</a></em></p></td>
<td><p>King Richard</p></td>
<td><p>配音角色 / Video game</p></td>
</tr>
<tr class="even">
<td><p>1994</p></td>
<td><p><em><a href="../Page/In_Search_of_Dr._Seuss.md" title="wikilink">In Search of Dr. Seuss</a></em></p></td>
<td><p>Sgt. Mulvaney</p></td>
<td><p>Puppet-voice over / 电视电影</p></td>
</tr>
<tr class="odd">
<td><p>1995</p></td>
<td><p><em><a href="../Page/Jeffrey_(film).md" title="wikilink">Jeffrey</a></em></p></td>
<td><p>Sterling</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1995</p></td>
<td><p><em><a href="../Page/Let_It_Be_Me_(film).md" title="wikilink">Let It Be Me</a></em></p></td>
<td><p>John</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1995</p></td>
<td><p><a href="../Page/辛普森一家.md" title="wikilink">辛普森一家</a></p></td>
<td><p>Number 1</p></td>
<td><p>Episode: Homer the Great</p></td>
</tr>
<tr class="even">
<td><p>1995</p></td>
<td><p><em><a href="../Page/500_Nations.md" title="wikilink">500 Nations</a></em></p></td>
<td></td>
<td><p>配音角色 / 电视迷你剧</p></td>
</tr>
<tr class="odd">
<td><p>1996</p></td>
<td><p><a href="../Page/星艦迷航記VIII：戰鬥巡航.md" title="wikilink">星艦迷航記VIII：戰鬥巡航</a></p></td>
<td><p><a href="../Page/让-吕克·皮卡尔.md" title="wikilink">让-吕克·皮卡尔</a></p></td>
<td><p>Nominated-<a href="../Page/Saturn_Award_for_Best_Actor.md" title="wikilink">Saturn Award for Best Actor</a><br />
Nominated-<a href="../Page/Blockbuster_LLC.md" title="wikilink">Blockbuster Entertainment Award for Favorite Actor</a></p></td>
</tr>
<tr class="even">
<td><p>1996</p></td>
<td><p><em></em></p></td>
<td><p>Sir Simon de Canterville</p></td>
<td><p>电视电影</p></td>
</tr>
<tr class="odd">
<td><p>1997</p></td>
<td><p><em><a href="../Page/Conspiracy_Theory_(film).md" title="wikilink">Conspiracy Theory</a></em></p></td>
<td><p>Dr. Jonas</p></td>
<td><p><a href="../Page/Blockbuster_LLC.md" title="wikilink">Blockbuster Entertainment Award for Favorite Supporting Actor</a></p></td>
</tr>
<tr class="even">
<td><p>1997</p></td>
<td><p><em><a href="../Page/Masterminds_(film).md" title="wikilink">Masterminds</a></em></p></td>
<td><p>Rafe Bentley</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1998</p></td>
<td><p><em><a href="../Page/Star_Trek:_The_Experience.md" title="wikilink">Star Trek: The Experience</a>: The Klingon Encounter</em></p></td>
<td><p><a href="../Page/让-吕克·皮卡尔.md" title="wikilink">让-吕克·皮卡尔</a></p></td>
<td><p>配音角色</p></td>
</tr>
<tr class="even">
<td><p>1998</p></td>
<td><p><em><a href="../Page/Dad_Savage.md" title="wikilink">Dad Savage</a></em></p></td>
<td><p>Dad Savage</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1998</p></td>
<td><p><em><a href="../Page/Moby_Dick_(1998_miniseries).md" title="wikilink">Moby Dick</a></em></p></td>
<td><p>亚哈船长</p></td>
<td><p>Television film<br />
Nominated-<a href="../Page/Golden_Globe_Award_for_Best_Actor_-_Miniseries_or_Television_Film.md" title="wikilink">Golden Globe Award for Best Actor - Miniseries or Television Film</a><br />
Nominated-<a href="../Page/Primetime_Emmy_Award_for_Outstanding_Lead_Actor_in_a_Miniseries_or_a_Movie.md" title="wikilink">Primetime Emmy Award for Outstanding Lead Actor in a Miniseries or a Movie</a><br />
Nominated-<a href="../Page/Satellite_Award_for_Best_Actor_-_Miniseries_or_Television_Film.md" title="wikilink">Satellite Award for Best Actor - Miniseries or Television Film</a></p></td>
</tr>
<tr class="even">
<td><p>1998</p></td>
<td><p><em><a href="../Page/Safe_House_(1998_film).md" title="wikilink">Safe House</a></em></p></td>
<td><p>Mace Sowell</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1998</p></td>
<td><p><a href="../Page/星际旅行IX：起义.md" title="wikilink">星际旅行IX：起义</a></p></td>
<td><p><a href="../Page/让-吕克·皮卡尔.md" title="wikilink">让-吕克·皮卡尔</a></p></td>
<td><p>Also associate producer</p></td>
</tr>
<tr class="even">
<td><p>1998</p></td>
<td><p><a href="../Page/埃及王子.md" title="wikilink">埃及王子</a></p></td>
<td><p>法老<a href="../Page/塞提一世.md" title="wikilink">塞提一世</a></p></td>
<td><p>配音角色</p></td>
</tr>
<tr class="odd">
<td><p>1999</p></td>
<td><p><em></em></p></td>
<td><p><a href="../Page/Ebenezer_Scrooge.md" title="wikilink">Ebenezer Scrooge</a></p></td>
<td><p>电视电影<br />
Nominated-<a href="../Page/Saturn_Award.md" title="wikilink">Saturn Award for Best Genre TV Actor</a></p></td>
</tr>
<tr class="even">
<td><p>1999</p></td>
<td><p><em><a href="../Page/Animal_Farm_(1999_film).md" title="wikilink">动物庄园</a></em></p></td>
<td><p><a href="../Page/Napoleon_(Animal_Farm).md" title="wikilink">拿破仑</a></p></td>
<td><p>配音角色 /电视电影</p></td>
</tr>
<tr class="odd">
<td><p>2000</p></td>
<td><p><a href="../Page/X战警_(电影).md" title="wikilink">X战警</a></p></td>
<td><p><a href="../Page/X教授.md" title="wikilink">X教授</a></p></td>
<td><p>Nominated-<a href="../Page/Saturn_Award_for_Best_Supporting_Actor.md" title="wikilink">Saturn Award for Best Supporting Actor</a><br />
Nominated-<a href="../Page/Blockbuster_LLC.md" title="wikilink">Blockbuster Entertainment Award for Favorite Actor</a></p></td>
</tr>
<tr class="even">
<td><p>2001</p></td>
<td><p><a href="../Page/天才小子吉米.md" title="wikilink">天才小子吉米</a></p></td>
<td><p>King Goobot</p></td>
<td><p>配音角色</p></td>
</tr>
<tr class="odd">
<td><p>2002</p></td>
<td><p><a href="../Page/星际旅行X：复仇女神.md" title="wikilink">星际旅行X：复仇女神</a></p></td>
<td><p><a href="../Page/让-吕克·皮卡尔.md" title="wikilink">让-吕克·皮卡尔</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2002</p></td>
<td><p><em><a href="../Page/King_of_Texas.md" title="wikilink">King of Texas</a></em></p></td>
<td><p>John Lear</p></td>
<td><p>电视电影<br />
Nominated-<a href="../Page/Satellite_Award_for_Best_Actor_-_Miniseries_or_Television_Film.md" title="wikilink">Satellite Award for Best Actor - Miniseries or Television Film</a></p></td>
</tr>
<tr class="odd">
<td><p>2002</p></td>
<td><p><em><a href="../Page/X-Men:_Next_Dimension.md" title="wikilink">X-Men: Next Dimension</a></em></p></td>
<td><p><a href="../Page/X教授.md" title="wikilink">X教授</a></p></td>
<td><p>配音角色 / Video game</p></td>
</tr>
<tr class="even">
<td><p>2003</p></td>
<td><p><a href="../Page/X战警2.md" title="wikilink">X战警2</a></p></td>
<td><p><a href="../Page/X教授.md" title="wikilink">X教授</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2003</p></td>
<td><p><em></em></p></td>
<td><p><a href="../Page/亨利二世_(英格兰).md" title="wikilink">亨利二世</a></p></td>
<td><p>电视电影<br />
Nominated-<a href="../Page/Golden_Globe_Award_for_Best_Actor_-_Miniseries_or_Television_Film.md" title="wikilink">Golden Globe Award for Best Actor - Miniseries or Television Film</a><br />
Nominated-<a href="../Page/Primetime_Emmy_Award_for_Outstanding_Miniseries.md" title="wikilink">Primetime Emmy Award for Outstanding Miniseries</a></p></td>
</tr>
<tr class="even">
<td><p>2003</p></td>
<td><p><a href="../Page/欢乐一家亲.md" title="wikilink">欢乐一家亲</a></p></td>
<td><p>Alastair Burke</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2004</p></td>
<td><p><em>Boo, Zino &amp; the Snurks</em></p></td>
<td><p>Albert Drollinger</p></td>
<td><p>配音角色</p></td>
</tr>
<tr class="even">
<td><p>2004</p></td>
<td><p><a href="../Page/蒸汽男孩.md" title="wikilink">蒸汽男孩</a></p></td>
<td><p>Dr. Lloyd Steam</p></td>
<td><p>English dubbing</p></td>
</tr>
<tr class="odd">
<td><p>2005</p></td>
<td><p><em></em></p></td>
<td><p>Older Dent McSkimming</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2005</p></td>
<td><p><a href="../Page/四眼天鸡.md" title="wikilink">四眼天鸡</a></p></td>
<td><p>Mr. Woolensworth</p></td>
<td><p>配音角色</p></td>
</tr>
<tr class="odd">
<td><p>2005</p></td>
<td><p><em><a href="../Page/Mysterious_Island_(2005_film).md" title="wikilink">Mysterious Island</a></em></p></td>
<td><p><a href="../Page/Captain_Nemo.md" title="wikilink">Nemo</a></p></td>
<td><p>电视电影</p></td>
</tr>
<tr class="even">
<td><p>2005</p></td>
<td><p><em><a href="../Page/Nausicaä_of_the_Valley_of_the_Wind_(film).md" title="wikilink">Nausicaä of the Valley of the Wind</a></em></p></td>
<td><p>Lord Yupa</p></td>
<td><p>English dubbing</p></td>
</tr>
<tr class="odd">
<td><p>2005</p></td>
<td><p><em></em></p></td>
<td><p>The Raven</p></td>
<td><p>配音角色 /电视电影</p></td>
</tr>
<tr class="even">
<td><p>2005–present</p></td>
<td><p><a href="../Page/特工老爹.md" title="wikilink">特工老爹</a></p></td>
<td><p>Avery Bullock</p></td>
<td><p>配音角色 / Television cartoon</p></td>
</tr>
<tr class="odd">
<td><p>2005</p></td>
<td><p><em><a href="../Page/Extras.md" title="wikilink">Extras</a></em></p></td>
<td><p>Himself</p></td>
<td><p>TV series<br />
Nominated-<a href="../Page/Primetime_Emmy_Award_for_Outstanding_Guest_Actor_in_a_Comedy_Series.md" title="wikilink">Primetime Emmy Award for Outstanding Guest Actor in a Comedy Series</a></p></td>
</tr>
<tr class="even">
<td><p>2006</p></td>
<td><p><em><a href="../Page/Eleventh_Hour_(UK_TV_series).md" title="wikilink">Eleventh Hour (UK TV series)</a></em></p></td>
<td><p>Professor Ian Hood</p></td>
<td><p>TV Series</p></td>
</tr>
<tr class="odd">
<td><p>2006</p></td>
<td><p><a href="../Page/小鹿斑比2.md" title="wikilink">小鹿斑比2</a></p></td>
<td><p>The Great Prince/Stag</p></td>
<td><p>配音角色</p></td>
</tr>
<tr class="even">
<td><p>2006</p></td>
<td><p><a href="../Page/X戰警：最後戰役.md" title="wikilink">X戰警：最後戰役</a></p></td>
<td><p><a href="../Page/X教授.md" title="wikilink">X教授</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2006</p></td>
<td><p><a href="../Page/上古卷轴IV：湮没.md" title="wikilink">上古卷轴IV：湮没</a></p></td>
<td><p>Emperor Uriel Septim VII</p></td>
<td><p>配音角色 / video game</p></td>
</tr>
<tr class="even">
<td><p>2007</p></td>
<td><p><a href="../Page/忍者神龟_(2007年电影).md" title="wikilink">忍者神龟</a></p></td>
<td><p>Max Winters/Yaotl</p></td>
<td><p>配音角色</p></td>
</tr>
<tr class="odd">
<td><p>2007</p></td>
<td><p><em><a href="../Page/Earth_(2007_film).md" title="wikilink">Earth</a></em></p></td>
<td><p>Narrator</p></td>
<td><p>配音角色</p></td>
</tr>
<tr class="even">
<td><p>2009</p></td>
<td><p><a href="../Page/X战警前传：金刚狼.md" title="wikilink">X战警前传：金刚狼</a>[6]</p></td>
<td><p><a href="../Page/X教授.md" title="wikilink">X教授</a></p></td>
<td><p>客串</p></td>
</tr>
<tr class="odd">
<td><p>2009</p></td>
<td><p><em><a href="../Page/Hamlet_(2009_television_film).md" title="wikilink">Hamlet</a></em></p></td>
<td><p><a href="../Page/King_Claudius.md" title="wikilink">Claudius</a>/the Ghost</p></td>
<td><p>电视电影<br />
Nominated-<a href="../Page/Primetime_Emmy_Award_for_Outstanding_Supporting_Actor_in_a_Miniseries_or_a_Movie.md" title="wikilink">Primetime Emmy Award for Outstanding Supporting Actor in a Miniseries or a Movie</a></p></td>
</tr>
<tr class="even">
<td><p>2010</p></td>
<td><p><a href="../Page/恶魔城：黑暗领主.md" title="wikilink">恶魔城：黑暗领主</a></p></td>
<td><p>Zobek / Narrator</p></td>
<td><p>配音角色</p></td>
</tr>
<tr class="odd">
<td><p>2010</p></td>
<td><p><em><a href="../Page/Macbeth_(2010_film).md" title="wikilink">Macbeth</a></em></p></td>
<td><p><a href="../Page/Macbeth_(character).md" title="wikilink">Macbeth</a></p></td>
<td><p>电视电影</p></td>
</tr>
<tr class="even">
<td><p>2010</p></td>
<td><p><a href="../Page/居家男人.md" title="wikilink">居家男人</a></p></td>
<td><p>Dick Pump</p></td>
<td><p>配音角色</p></td>
</tr>
<tr class="odd">
<td><p>2011</p></td>
<td><p><a href="../Page/居家男人.md" title="wikilink">居家男人</a></p></td>
<td><p>Himself</p></td>
<td><p>配音角色</p></td>
</tr>
<tr class="even">
<td><p>2011</p></td>
<td><p><em><a href="../Page/Gnomeo_&amp;_Juliet.md" title="wikilink">Gnomeo &amp; Juliet</a></em></p></td>
<td><p><a href="../Page/William_Shakespeare.md" title="wikilink">William Shakespeare</a></p></td>
<td><p>配音角色</p></td>
</tr>
<tr class="odd">
<td><p>2011</p></td>
<td><p><a href="../Page/居家男人.md" title="wikilink">居家男人</a></p></td>
<td><p>Susie Swanson's inner monologue</p></td>
<td><p>配音角色</p></td>
</tr>
<tr class="even">
<td><p>2011</p></td>
<td><p><em><a href="../Page/The_Captains_(film).md" title="wikilink">The Captains</a></em></p></td>
<td><p><a href="../Page/Himself.md" title="wikilink">Himself</a> / <a href="../Page/Captain_Jean-Luc_Picard.md" title="wikilink">Captain Jean-Luc Picard</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2012</p></td>
<td><p><a href="../Page/冰川时代4.md" title="wikilink">冰川时代4</a></p></td>
<td><p>Ariscratle</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2012</p></td>
<td><p><a href="../Page/泰迪熊_(电影).md" title="wikilink">泰迪熊</a></p></td>
<td><p>Narrator</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2012</p></td>
<td><p><a href="../Page/理查二世_(电影).md" title="wikilink">理查二世</a></p></td>
<td><p><a href="../Page/冈特的约翰.md" title="wikilink">冈特的约翰</a></p></td>
<td><p>电视电影</p></td>
</tr>
<tr class="even">
<td><p>2012</p></td>
<td><p><a href="../Page/飞出个未来.md" title="wikilink">飞出个未来</a></p></td>
<td><p>The Huntmaster</p></td>
<td><p>Episode: 31st Century Fox</p></td>
</tr>
<tr class="odd">
<td><p>2012</p></td>
<td><p><a href="../Page/每日秀.md" title="wikilink">每日秀</a></p></td>
<td><p>Correspondent</p></td>
<td><p>Air Date 26 September 2012</p></td>
</tr>
<tr class="even">
<td><p>2012</p></td>
<td><p><em>Racing Legends: Stirling Moss</em></p></td>
<td><p>Presenter</p></td>
<td><p>Air Date 27 December 2012</p></td>
</tr>
<tr class="odd">
<td><p>2013</p></td>
<td><p><em>Sinbad: The Fifth Voyage</em></p></td>
<td><p>Narrator</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2013</p></td>
<td><p><a href="../Page/奧茲國的桃樂絲.md" title="wikilink">奧茲國的桃樂絲</a></p></td>
<td><p>Tugg</p></td>
<td><p>配音角色</p></td>
</tr>
<tr class="odd">
<td><p>2013</p></td>
<td><p><em>Hunting Elephants</em></p></td>
<td><p>Michael Simpson</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2013</p></td>
<td><p><a href="../Page/金鋼狼：武士之戰.md" title="wikilink">金鋼狼：武士之戰</a></p></td>
<td><p><a href="../Page/X教授.md" title="wikilink">X教授</a></p></td>
<td><p>客串</p></td>
</tr>
<tr class="odd">
<td><p>2014</p></td>
<td><p><a href="../Page/X戰警：未來昔日.md" title="wikilink">X戰警：未來昔日</a>[7]</p></td>
<td><p><a href="../Page/X教授.md" title="wikilink">X教授</a></p></td>
<td><p>與<a href="../Page/詹姆斯·麥艾維.md" title="wikilink">詹姆斯·麥艾維共同飾演</a></p></td>
</tr>
<tr class="even">
<td><p>2014</p></td>
<td><p><a href="../Page/百萬種硬的方式.md" title="wikilink">百萬種硬的方式</a></p></td>
<td><p>羊</p></td>
<td><p>未掛名；配音角色</p></td>
</tr>
<tr class="odd">
<td><p>2014</p></td>
<td><p><a href="../Page/匹配人生.md" title="wikilink">匹配人生</a></p></td>
<td><p>Tobi Powell</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2015</p></td>
<td><p><a href="../Page/泰迪熊2.md" title="wikilink">泰迪熊2</a></p></td>
<td><p>旁白</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2015</p></td>
<td><p><a href="../Page/愛的平安夜.md" title="wikilink">愛的平安夜</a></p></td>
<td><p>哈里斯</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2015</p></td>
<td><p><a href="../Page/納粹龐克.md" title="wikilink">納粹龐克</a></p></td>
<td><p>Darcy Banker</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2017</p></td>
<td><p><a href="../Page/羅根_(電影).md" title="wikilink">羅根</a></p></td>
<td><p><a href="../Page/X教授.md" title="wikilink">X教授</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2019</p></td>
<td><p><a href="../Page/魔劍少年.md" title="wikilink">魔劍少年</a></p></td>
<td><p><a href="../Page/梅林_(亞瑟王傳說).md" title="wikilink">梅林</a></p></td>
<td></td>
</tr>
</tbody>
</table>

## 参考文献

## 外部链接

  -
  - [帕特里克·斯图尔特为最终幻想XII配音](http://www.strategyinformer.com/member/3/blog/2080)

  -
  -
  -
  -
  - [PatrickStewart.org](https://web.archive.org/web/20071017024107/http://www.patrickstewart.org/)－帕特里克·斯图尔特网络（官方影迷俱乐部）

  - [BBC有关《星际旅行》的采访](http://news.bbc.co.uk/1/hi/entertainment/tv_and_radio/3455463.stm)

  - [莫菲尔德](https://web.archive.org/web/20071028120500/http://www.mirfieldinpictures.net/Patrick_Stewart.html)－帕特里克·斯图尔特的出生地

[Category:葛莱美奖获得者](../Category/葛莱美奖获得者.md "wikilink")
[Category:英格蘭男電影演員](../Category/英格蘭男電影演員.md "wikilink")
[Category:英格蘭舞台演員](../Category/英格蘭舞台演員.md "wikilink")
[Category:英格蘭男電視演員](../Category/英格蘭男電視演員.md "wikilink")
[Category:英格蘭配音員](../Category/英格蘭配音員.md "wikilink")
[Category:西約克郡人](../Category/西約克郡人.md "wikilink")
[Category:下級勳位爵士](../Category/下級勳位爵士.md "wikilink")
[Category:OBE勳銜](../Category/OBE勳銜.md "wikilink")
[Category:星际旅行相关人物](../Category/星际旅行相关人物.md "wikilink")

1.
2.  <http://www.telegraph.co.uk/news/main.jhtml?xml=/news/exclusions/familyhistory/nosplit/fd120107.xml>
3.  Revealed in interview on the 'Parkinson' show, ITV-1, 12 May 2007
4.
5.
6.
7.