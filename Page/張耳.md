**張耳**（），秦末[大梁](../Page/大梁.md "wikilink")（今[開封](../Page/開封.md "wikilink")）人，早年為[游俠](../Page/游俠.md "wikilink")，曾被[西楚霸王封為](../Page/項羽.md "wikilink")[常山王](../Page/常山王.md "wikilink")，後來被[汉太祖封為](../Page/汉太祖.md "wikilink")[趙王](../Page/趙王.md "wikilink")。

## 生平

他年少时是[魏国](../Page/魏国.md "wikilink")[信陵君的](../Page/信陵君.md "wikilink")[门客](../Page/门客.md "wikilink")。信陵君死后脱离[户籍亡命至](../Page/户籍.md "wikilink")[外黄县](../Page/外黄县.md "wikilink")，当时外黄有一户富豪家有一远近闻名的绝色美女，此美女嫌弃自己的丈夫没本事，向自己父亲的门客寻求帮助。该门客与张耳相识，表示没人比张耳更合适了。于是美女请门客打听张耳的意思，此时张耳孤身一人流落于异乡，有富家美人愿意嫁与自己，当即同意。在门客的帮助下，美女休掉前夫与张耳结合。此后张耳以信陵君作为榜样，仗义疏财，结交有才能的[游侠](../Page/游侠.md "wikilink")。自己成为有名的县侠，名声传到了魏国以外，还担任了外黄[县令](../Page/县令.md "wikilink")。

此期间[刘邦慕名而成為张耳的](../Page/刘邦.md "wikilink")[门客](../Page/门客.md "wikilink")，在外黄住了几个月。秦灭魏后，张耳和友人[陈餘不愿事秦](../Page/陈餘.md "wikilink")，一齐改名换姓逃亡。

### 大泽起义

[陈胜與](../Page/陈胜.md "wikilink")[吳廣發動](../Page/吳廣.md "wikilink")[大澤起義](../Page/大澤起義.md "wikilink")，张耳、[陈餘二人投靠陈胜](../Page/陈餘.md "wikilink")，被任为左右[校尉](../Page/校尉.md "wikilink")。

秦二世元年（前209年）八月，張耳、陳餘見陳勝無心立六國宗室，故建言陳勝派大將[武臣攻擊](../Page/武臣.md "wikilink")[邯鄲](../Page/邯鄲.md "wikilink")，陳勝在陳地稱王，張耳與陳餘亦勸武臣稱王。武臣遂自立為趙王，以張耳為右[丞相](../Page/丞相.md "wikilink")，[邵騷為左丞相](../Page/邵騷.md "wikilink")，陳餘為[大將軍](../Page/大將軍.md "wikilink")。武臣死後，張耳等立舊趙國王族[趙歇為趙王](../Page/趙歇.md "wikilink")。

秦將[章邯攻趙時](../Page/章邯.md "wikilink")，张耳与赵歇被困[钜鹿](../Page/钜鹿.md "wikilink")，陈餘则北收[常山數萬人](../Page/常山.md "wikilink")，屯驻于钜鹿之北，以人數太少，不肯相救。秦二世三年（前207年），项羽率军北渡漳河，解钜鹿之围，十二月，张耳见陈餘，责其不肯相救。陈餘怒解印绶推给张耳，起身去廁所，張耳一時不知所措，幕僚对张耳道：“此乃天赐于你，若不取，反受其害。”於是張耳收取了陳餘兵權。從此二人失和，陳餘常獨自與軍中好友數百人漁獵於河上澤中。

### 常山王

汉王元年（前207年十月），项羽自立为西楚霸王，此時張耳和[启琛入關](../Page/启琛.md "wikilink")。[西楚霸王素闻张耳之名](../Page/项羽.md "wikilink")，乃分赵地北部，立张耳为常山王，治[信都](../Page/信都.md "wikilink")，信都更名襄国。最初張耳、陳餘有隙，陳餘聞知大怒，对人道：“张耳和我功相等。今张耳为王，我却称侯，项羽对我不公。”陈餘使夏说說服齐王[田荣發兵](../Page/田荣.md "wikilink")，加三县之兵袭击常山王张耳。汉高帝二年（前205年），张耳败走，投靠汉王刘邦，陳餘迎[代王](../Page/代王.md "wikilink")[趙歇為趙王](../Page/趙歇.md "wikilink")，赵歇感谢陈餘，封陈餘为代王。張耳次年與[韓信出井陉击趙](../Page/韓信.md "wikilink")，大败赵军，與劉邦會合襄國殺陳餘及趙歇。

### 赵王

汉高帝四年（前203年），韩信报请汉王劉邦以张耳为赵王，劉邦同意。汉高帝五年（前202年）張耳病逝，卒[諡景](../Page/諡.md "wikilink")。

## 家庭

子[張敖嗣立為趙王](../Page/張敖.md "wikilink")，尚高祖长女[鲁元公主](../Page/鲁元公主.md "wikilink")。

## 世系图

<center>

</center>

## 後裔

  - [張酺](../Page/張酺.md "wikilink")，[東漢世族之一](../Page/東漢.md "wikilink")
  - [张郃](../Page/张郃.md "wikilink")，[曹魏名将](../Page/曹魏.md "wikilink")
  - [張軌](../Page/張軌.md "wikilink")，[前涼武王](../Page/前涼.md "wikilink")
  - [张之洞](../Page/张之洞.md "wikilink")，[晚清大臣](../Page/晚清.md "wikilink")

## 參考資料

  -
  -
  - 张凤岭 译注，本篇是秦楚之际随从汉高祖刘邦起事的三位近卫侍从官员傅宽、靳歙和周緤的合传。

传中主要记述了傅、靳、周三人随从刘邦征战及升迁的过程

[Z张](../Category/西漢軍事人物.md "wikilink")
[Z张](../Category/汉朝异姓王.md "wikilink")
[z張](../Category/汉朝外戚.md "wikilink")
[z張](../Category/開封人.md "wikilink")
[E耳](../Category/张姓.md "wikilink")
[Z张](../Category/楚漢戰爭人物.md "wikilink")