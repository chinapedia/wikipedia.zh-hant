**西直门天主堂**，全称**加尔默罗圣母圣衣堂**，俗称**西堂**，名列北京四大天主堂之一，位于[北京市](../Page/北京市.md "wikilink")[西城区](../Page/西城区.md "wikilink")[西直门内大街](../Page/西直门内大街.md "wikilink")130号，是北京四大天主堂中历史最短规模最小的一个，也是四大天主教堂中唯一一个不是由[耶稣会士建立的教堂](../Page/耶稣会.md "wikilink")。目前西直门天主教堂已经正式成为[西城区文物保护单位](../Page/西城区文物保护单位.md "wikilink")。堂区奉“加尔默罗圣母圣衣”为主保，瞻礼是在每年的7月16日。

## 历史

康熙四十四年[罗马天主教](../Page/罗马天主教.md "wikilink")[教宗](../Page/教宗.md "wikilink")[克勉十一世派遣](../Page/克勉十一世.md "wikilink")[多罗](../Page/多罗.md "wikilink")[枢机主教作为特使道中国宣示教皇敕令](../Page/枢机.md "wikilink")。多罗主教的随员，[意大利籍](../Page/意大利.md "wikilink")[味增爵会传教士](../Page/味增爵会.md "wikilink")[德理格神父](../Page/德理格.md "wikilink")
(拉丁语：Teodoricus Pedrini，意大利语：Teodorico Pedrini，1670-1746)
受到[康熙皇帝的任命](../Page/康熙皇帝.md "wikilink")，成为专门教授皇子[西学的教师](../Page/西学.md "wikilink")，于是德里格神父便留在了[中国](../Page/中国.md "wikilink")。雍正元年（1723年）德里格在[西直门内购置土地建设了西直门天主堂](../Page/西直门.md "wikilink")，并一直在这里从事传教，直到1764年12月10日去世。

由于创建西直门天主堂的德里格神父是受到罗马传信部指派的神职人员，因而西堂直接隶属于罗马教廷。北京四大天主教堂的另外三座则均为耶稣会士创建，直到1773年罗马教皇宣布解散耶稣会之后才转归划归味增爵会士管辖，成为直接隶属于教廷的教堂。

西直门天主堂是一座比较典型的[哥特式建筑](../Page/哥特式建筑.md "wikilink")，拥有一座三层的尖顶塔楼。

清嘉庆十六年，清政府颁布命令严禁天主教，天主教传教士除在政府供职者外，一概不许居住京城，并不许从事传教活动，同年西直门天主堂的四位神父被驱逐出境，西直门天主堂被拆除，地产查没。

1860年[第二次鸦片战争后](../Page/第二次鸦片战争.md "wikilink")，根据[清政府与](../Page/清朝.md "wikilink")[英](../Page/英国.md "wikilink")[法等国达成的协议](../Page/法国.md "wikilink")，恢复天主教在中国活动的权利，由于禁教的缘故，到[咸丰年间西堂就早已已改为民居多年](../Page/咸丰.md "wikilink")，以至于无人知晓这里还曾经是一座颇具规模的天主教堂，在[咸丰十年](../Page/咸丰十年.md "wikilink")，英法等国洋人要求清政府归还教产的时候，咸丰皇帝发出了“东西二堂究在何处”的疑问。据《[筹办夷务始末](../Page/筹办夷务始末.md "wikilink")》记载，[步军统领衙门奉咸丰皇帝之命察访西堂](../Page/步军统领衙门.md "wikilink")，找到*“西直门横桥，有粉房一座，官房排子房等房十八间。后面空院，四至约十六丈零。并汉军陈姓、民人刘姓各住宅，均系旧西天主堂基。”*同治六年（1867年）西直门天主教堂重建落成。清光绪二十六年，[义和团运动爆发](../Page/义和团运动.md "wikilink")，6月15日西直门天主堂又被[义和团毁坏](../Page/义和团.md "wikilink")，此后直到1912年，在原址第三次重建西直门天主堂。圣堂在1913年第三次重建时，由原来的“圣母七苦堂”更名为“加尔默罗圣母圣衣堂”。

[文化大革命期间西直门天主堂被没收](../Page/文化大革命.md "wikilink")，曾经先后被用作过[纽扣厂](../Page/纽扣.md "wikilink")、[电扇厂和](../Page/电扇.md "wikilink")[同仁堂制药厂的仓库](../Page/同仁堂.md "wikilink")。其间教堂三层高的尖顶钟楼也被拆除。1994年，西堂重新开放，恢复了正常的宗教活动。
[Western_church04.jpg](https://zh.wikipedia.org/wiki/File:Western_church04.jpg "fig:Western_church04.jpg")
教堂修复工程由前任主任[司铎庞文贤](../Page/司铎.md "wikilink")[神父](../Page/神父.md "wikilink")
(Fr. Peter PANG Wenxian)
策划，本来希望2006年底[圣诞节后动工](../Page/圣诞节.md "wikilink")，不过因为图纸有小问题，需要修整，所以顺延至2007年[复活节后开工](../Page/复活节.md "wikilink")。
这次修葺工程包括：

  - 拆除教堂门前搭建的临时接待室，重建尖塔钟楼，尽量恢复原貌；
  - 安装新的[中世纪风格的](../Page/中世纪.md "wikilink")[祭台中央](../Page/祭台.md "wikilink")[圣像壁](../Page/圣像.md "wikilink")；
  - 安装彩玻璃，内容描述西堂的历史，以及诠释圣经里的救恩史；
  - 安装新的电子[管风琴和音响系统](../Page/管风琴.md "wikilink")；
  - 教堂围墙基本恢复至原来民国时期的模样；
  - 新建的教堂大门、新建的[汉白玉](../Page/汉白玉.md "wikilink")[祭台和](../Page/祭台.md "wikilink")[读经台](../Page/读经台.md "wikilink")、新建的浸洗式领洗池等，都以[加纳婚宴为设计主题](../Page/加纳婚宴.md "wikilink")。

2007年7月，西直门教堂被北京市[西城区政府列为区级文物保护单位](../Page/西城区.md "wikilink")。

2009年7月19日，北京教区为圣母圣衣堂举行了新堂开堂祝圣仪式（奉献礼）。[李山](../Page/李山.md "wikilink")[主教开启了修葺一新的圣母圣衣堂](../Page/主教.md "wikilink")，并为复建后的钟楼和新建的领洗池祝圣。

2009年[天主圣三节](../Page/天主圣三节.md "wikilink")，西直门教堂组建了第一个青年聚会性质的青年团体——鲍斯高青年营，活动时间每隔一个主日（周日）进行，活动地点一般在教堂东侧的西堂慕道室。

2011年夏天起，因为[北京下了大暴雨](../Page/北京.md "wikilink")，[西直门教堂的附属堂区建筑已经出现了危险情况](../Page/西直门.md "wikilink")，后来，经[北京教区组织对西直门教堂的附属建筑](../Page/北京教区.md "wikilink")（[神父住宅](../Page/神父.md "wikilink")，食堂等）进行拆除重建。不过据该堂教友反映，新建的附属楼（预计2013年初完工）将会高[教堂堂顶](../Page/教堂.md "wikilink")，并且该楼将不完全由堂区使用。据信当地的[爱国会也会入驻新建的附属楼](../Page/爱国会.md "wikilink")。此事遭到了该堂[教友们私下的反对](../Page/教友.md "wikilink")。目前具体尚不得而知。

### 石碑

在西堂入口处的墙壁上，左右两边各镶嵌了[石碑](../Page/石碑.md "wikilink")，记载了西堂的历史： -{

<div style="width: 25em; padding: 1em; border: 1em solid grey; font-family: Kaiti,SimKai-GB2312,標楷體,DFKai-SB,AR PL UKai TW,AR PL ZenKai Uni,標楷,BiauKai,cursive; font-size: large; background: #333; color: #ccc;">

</div>

}- 以下是对应的拉丁文石碑：

<div lang="la" xml:lang="la" style="width: 34em; padding: 1em; border: 1em solid grey; border: 1em solid grey; font-family: serif; background: #333; color: #ccc; font-weight: bold">

  -

    <center>

    D. O. M.

    </center>

    TEODORICUS PEDRINI. PRESB. CONGR. MISS., INFAN-

    TIUM IMPERATORIS KANGHSI PRÆCEPTOR, AN. DOM. 1723

    HUNC FUNDUM PROPRIO ÆRE EMIT, IN EOQUE ECCLESIAM

    SUB AUSPICIIS SEPTEM DOLORUM B.M.V. DEO DEDICAVIT,

    QUÆ TEMPORE PERSECUTIONIS KIATSING (1811) FUNDI-

    TUS EST DESTRUCTA.

    POSTEA A.D. 1867, ILL. DD. MOULY, C.M., HIC NOVUM SA-

    CRUM ÆDIFICAVIT. QUOD DIE 15 JUNII 1900 BOXORES

    FLAMMIS TRADIDERUNT, DUM PAROCHUM ECCLESIÆ

    MAURITIUM DORE, C.M. SACERDOTEM CRUDELITER

    TRUCIDANT.

    TANDEM, ANNO DOMINI 1912 LARGITATE BENEMERITÆ

    ROSALIÆ BRANSSIER, SOCIETATIS PUELLARUM CA-

    RITATIS, TERTIA HÆC ECCLESIA SUB TITULO B.M. DE

    MONTE CARMELO ÆDIFICATA EST.

    IN QUORUM MEMORIAM ILL. DD. JARLIN, C.M., VIC. AP.

    PEKINESIS, HUNC LAPIDEM EREXIT.

</div>

## 教堂建筑

西直门天主堂为灰砖砌筑，[哥特式建筑](../Page/哥特式.md "wikilink")，南为祭台，北为钟楼，教堂东侧有复数建筑。在2009年西堂大面积复建整修之前，教堂原本建有一座三层高度的尖顶钟楼，自从文化大革命期间钟楼拆除后，仅仅在教堂主体建筑北侧余下一座一层楼高度的八角形[墩台](../Page/墩台.md "wikilink")，教堂建筑的表面被浓密的[爬山虎覆盖](../Page/爬山虎.md "wikilink")。整座教堂在周围建筑的映衬下显得颇不起眼，但教堂内部高大的科林斯柱和尖顶券窗使得从内部看来教堂依然高大华丽。现在教堂正面的北墙是倚着原来的北墙后接出来的，[圣母山建在了门内](../Page/圣母山.md "wikilink")，成为北京唯一一座建在室内的圣母山。教堂新修后，新建洗礼池，重新强调入门圣事的完整性。洗礼池为八角形，采用北京本土出产的白色大理石。池中嵌有浅蓝色马赛克，池底中央有一金十字架。洗礼使用后的圣水，排放到院子里圣母山前的水池中。

## [主任司铎](../Page/主任司铎.md "wikilink")

  - 主任司铎

<!-- end list -->

1.  [张继雄神父](../Page/张继雄_\(北京神父\).md "wikilink")（1995年—？）\[1\]
2.  [庞文贤神父](../Page/庞文贤.md "wikilink")（？—？）\[2\]
3.  [方斌神父](../Page/方斌_\(北京神父\).md "wikilink")（？—2000年）\[3\]
4.  [伯多禄](../Page/伯多禄.md "wikilink")·[刘永斌神父](../Page/刘永斌_\(北京神父\).md "wikilink")（2000年3月—2002年8月）\[4\]
5.  庞文贤神父（2002年—？）
6.  [若瑟](../Page/若瑟.md "wikilink")·[任力军神父](../Page/任力军.md "wikilink")（Fr.
    Joseph REN Lijun，？—2011年2月）
7.  [犹思定](../Page/犹思定.md "wikilink")·[刘振田神父](../Page/刘振田.md "wikilink")（Fr.
    Justin LIU Zhentian，2011年2月—2012年）
8.  [若瑟](../Page/若瑟.md "wikilink")·[张洪波神父](../Page/张洪波_\(北京神父\).md "wikilink")（Rev.
    Joseph ZHANG Hongbo，2012年—）\[5\]

<!-- end list -->

  - 副主任司铎

……

  - [邓剑神父](../Page/邓剑_\(北京神父\).md "wikilink")（2011年2月—2013年）
  - [孙晓野神父](../Page/孙晓野.md "wikilink")（2013年—）\[6\]

## 参考文献

## 外部链接

  - [西直门天主堂简介](http://www.tianguangbao.org/tangqu/xi.htm)
  - [西直门天主堂博客](http://blog.sina.com.cn/westchurch)

## 参见

  - [西什库天主堂](../Page/西什库天主堂.md "wikilink")
  - [宣武门天主堂](../Page/宣武门天主堂.md "wikilink")
  - [王府井天主堂](../Page/王府井天主堂.md "wikilink")

{{-}}

[Category:北京市西城区天主教堂](../Category/北京市西城区天主教堂.md "wikilink")

1.

2.
3.
4.

5.

6.