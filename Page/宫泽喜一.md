**宫泽喜一**（），祖籍[廣島縣福山市金江町](../Page/廣島縣.md "wikilink")，生於[東京市](../Page/東京市.md "wikilink")，是日本政坛少有的先后担任过内阁四大要职（内阁官房长官、大藏大臣、外务大臣、通产大臣）的政治家，后担任[日本第](../Page/日本.md "wikilink")78任[首相](../Page/日本首相.md "wikilink")。

1993年6月，[自民党的](../Page/自由民主黨_\(日本\).md "wikilink")[小泽一郎](../Page/小泽一郎.md "wikilink")、[羽田孜等人在一次关键性投票中的倒戈导致宫泽喜一政府垮台](../Page/羽田孜.md "wikilink")，继而由[细川护熙组成了八党联合政府](../Page/细川护熙.md "wikilink")。

後來再任[大藏大臣](../Page/大藏大臣.md "wikilink")，創下了戰後前首相再任大臣的先例。被稱為“**平成時代之[高橋是清](../Page/高橋是清.md "wikilink")**”。

## 簡歷

  - 1919年10月──為東京市山下汽船社員─[宮澤裕之長子](../Page/宮澤裕.md "wikilink")。
  - 1941年12月──[東京帝國大學法學部政治學科畢業](../Page/東京帝國大學.md "wikilink")
  - 1942年1月──加入大藏省
  - 1943年11月──與[伊地知庸子](../Page/伊地知庸子.md "wikilink")（父為伊地知純正為早稻田大學教授）結婚
  - 1945年8月──為津島壽一大藏大臣相秘書官事務取扱（同時兼任大平正芳的秘書官）
  - 1949年1月──任[池田勇人大藏大臣秘書官](../Page/池田勇人.md "wikilink")
  - 1951年8月──為全權隨員出席舊金山講和會議
  - 1953年4月──退職，參選参議院議員廣島地方選區、當選（～1965年7月）
  - 1962年7月──任第2次池田改造内閣經濟企畫廳長官 （～1964年7月）
  - 1966年12月──任第1次佐藤改造内閣經濟企畫廳長官官（～1967年11月）
  - 1967年2月──轉跑道出戰衆議院議員、當選（～2003年10月）
  - 1970年1月──任第3次佐藤内閣的通商產業大臣（～1971年7月）
  - 1974年12月──任三木内閣的外務大臣（～1976年9月）
  - 1977年11月──任福田改造内閣經濟企畫廳長官
  - 1980年7月──任鈴木内閣的内閣官房長官
  - 1984年11月──任[日本自民黨總務會長](../Page/日本自民黨總務會長.md "wikilink")(總裁[中曾根康弘](../Page/中曾根康弘.md "wikilink"))
  - 1986年7月──任第3次中曽根内閣大藏大臣
  - 1986年8月──就任宏池会（宮澤派）第5代會長
  - 1987年11月──任竹下内閣的副總理兼大藏大臣
  - 1988年12月──因涉入[瑞克魯特事件而被迫辭去副總理並大臣大臣](../Page/瑞克魯特事件.md "wikilink")
  - 1991年11月5日──當選自民黨總裁、內閣總理大臣
  - 1993年4月──訪美時、史上第一次內閣總理大臣使用政府專用飛機。
  - 1993年6月──因自民黨分裂使內閣不信任案成立，故把眾議院解散（世稱撒謊解散）後的總選舉，自民黨大敗失去執政權而辭職。
  - 1998年8月──任小淵内閣的大藏大臣
  - 2000年4月──留任森内閣大藏大臣。
  - 2001年1月──因中央省廳再編任最後的大藏大臣及初代財務大臣（～4月）
  - 2003年11月──眾議院大選，宣布不連任，退出政壇。
  - 2007年6月28日──13時16分因年老之自然原因於[東京都](../Page/東京都.md "wikilink")[澀谷區神宮前之寓所去世](../Page/澀谷區.md "wikilink")，喪禮於7月1日舉行。

[Category:日本內閣總理大臣](../Category/日本內閣總理大臣.md "wikilink")
[Category:二战后日本政治人物](../Category/二战后日本政治人物.md "wikilink")
[Category:日本副總理](../Category/日本副總理.md "wikilink")
[Category:日本內閣官房長官](../Category/日本內閣官房長官.md "wikilink")
[Category:日本外務大臣](../Category/日本外務大臣.md "wikilink")
[Category:日本大藏大臣](../Category/日本大藏大臣.md "wikilink")
[Category:日本財務大臣](../Category/日本財務大臣.md "wikilink")
[Category:日本經濟企劃廳長官](../Category/日本經濟企劃廳長官.md "wikilink")
[Category:日本通商產業大臣](../Category/日本通商產業大臣.md "wikilink")
[Category:日本農林水產大臣](../Category/日本農林水產大臣.md "wikilink")
[Category:日本郵政大臣](../Category/日本郵政大臣.md "wikilink")
[Category:第二次池田內閣閣僚](../Category/第二次池田內閣閣僚.md "wikilink")
[Category:第三次池田內閣閣僚](../Category/第三次池田內閣閣僚.md "wikilink")
[Category:第一次佐藤內閣閣僚](../Category/第一次佐藤內閣閣僚.md "wikilink")
[Category:第二次佐藤內閣閣僚](../Category/第二次佐藤內閣閣僚.md "wikilink")
[Category:第三次佐藤內閣閣僚](../Category/第三次佐藤內閣閣僚.md "wikilink")
[Category:三木內閣閣僚](../Category/三木內閣閣僚.md "wikilink")
[Category:福田赳夫內閣閣僚](../Category/福田赳夫內閣閣僚.md "wikilink")
[Category:鈴木善幸內閣閣僚](../Category/鈴木善幸內閣閣僚.md "wikilink")
[Category:第三次中曾根內閣閣僚](../Category/第三次中曾根內閣閣僚.md "wikilink")
[Category:竹下內閣閣僚](../Category/竹下內閣閣僚.md "wikilink")
[Category:宮澤內閣閣僚](../Category/宮澤內閣閣僚.md "wikilink")
[Category:小淵內閣閣僚](../Category/小淵內閣閣僚.md "wikilink")
[Category:第一次森內閣閣僚](../Category/第一次森內閣閣僚.md "wikilink")
[Category:第二次森內閣閣僚](../Category/第二次森內閣閣僚.md "wikilink")
[Category:日本參議院議員
1953–1959](../Category/日本參議院議員_1953–1959.md "wikilink")
[Category:日本參議院議員
1959–1965](../Category/日本參議院議員_1959–1965.md "wikilink")
[Category:日本眾議院議員
1967–1969](../Category/日本眾議院議員_1967–1969.md "wikilink")
[Category:日本眾議院議員
1969–1972](../Category/日本眾議院議員_1969–1972.md "wikilink")
[Category:日本眾議院議員
1972–1976](../Category/日本眾議院議員_1972–1976.md "wikilink")
[Category:日本眾議院議員
1976–1979](../Category/日本眾議院議員_1976–1979.md "wikilink")
[Category:日本眾議院議員
1979–1980](../Category/日本眾議院議員_1979–1980.md "wikilink")
[Category:日本眾議院議員
1980–1983](../Category/日本眾議院議員_1980–1983.md "wikilink")
[Category:日本眾議院議員
1983–1986](../Category/日本眾議院議員_1983–1986.md "wikilink")
[Category:日本眾議院議員
1986–1990](../Category/日本眾議院議員_1986–1990.md "wikilink")
[Category:日本眾議院議員
1990–1993](../Category/日本眾議院議員_1990–1993.md "wikilink")
[Category:日本眾議院議員
1993–1996](../Category/日本眾議院議員_1993–1996.md "wikilink")
[Category:日本眾議院議員
1996–2000](../Category/日本眾議院議員_1996–2000.md "wikilink")
[Category:日本眾議院議員
2000–2003](../Category/日本眾議院議員_2000–2003.md "wikilink")
[Category:日本自由民主黨總裁](../Category/日本自由民主黨總裁.md "wikilink")
[Category:日本自由民主黨總務會長](../Category/日本自由民主黨總務會長.md "wikilink")
[Category:日本財務官僚](../Category/日本財務官僚.md "wikilink")
[Category:宏池會會長](../Category/宏池會會長.md "wikilink")
[Category:東京大學校友](../Category/東京大學校友.md "wikilink")
[Category:廣島縣出身人物](../Category/廣島縣出身人物.md "wikilink")
[Category:廣島縣選出日本參議院議員](../Category/廣島縣選出日本參議院議員.md "wikilink")
[Category:廣島縣選出日本眾議院議員](../Category/廣島縣選出日本眾議院議員.md "wikilink")
[Category:中國比例代表區選出日本眾議院議員](../Category/中國比例代表區選出日本眾議院議員.md "wikilink")