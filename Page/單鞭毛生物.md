**單鞭毛生物**（）是指細胞中拥有（或其祖先拥有）单個[鞭毛的](../Page/鞭毛.md "wikilink")[真核生物](../Page/真核生物.md "wikilink")。現今的研究猜測單鞭毛生物會是[後鞭毛生物](../Page/後鞭毛生物.md "wikilink")（動物、真菌和其他相關類型）和[變形蟲的祖先](../Page/變形蟲界.md "wikilink")，而[雙鞭毛生物](../Page/雙鞭毛生物.md "wikilink")（有兩個鞭毛的真核細胞）則是[泛植物](../Page/泛植物.md "wikilink")（植物和其近親）、[古蟲界](../Page/古蟲界.md "wikilink")、[有孔蟲界和](../Page/有孔蟲界.md "wikilink")[囊泡藻界等生物的祖先](../Page/囊泡藻界.md "wikilink")。

## 特征

### 酶

單鞭毛生物中存在一個三基因的融合：与[嘧啶生物合成的有关的](../Page/嘧啶.md "wikilink")[氨甲酰磷酸合成酶Ⅱ](../Page/氨甲酰磷酸合成酶Ⅱ.md "wikilink")（**C**）、[天冬氨酸转氨甲酰酶](../Page/天冬氨酸转氨甲酰酶.md "wikilink")（**A**）和[二氫乳清酸酶](../Page/二氫乳清酸酶.md "wikilink")（**D**）融合成了一个[CAD基因](../Page/CAD_\(基因\).md "wikilink")，而這三個基因在原核生物或雙鞭毛生物中是单独表达的\[1\]。這必定包含有兩次的融合，一對很稀少的事件，使之成為了後鞭毛生物和變形蟲的共同祖先。

### 中心粒

湯瑪斯·卡弗利爾-史密斯曾认为單鞭毛生物的共同祖先有單一個[中心粒](../Page/中心粒.md "wikilink")\[2\]\[3\]，然而最近的观点是單鞭毛生物的共同祖先是有兩個中心粒但只有一条鞭毛的生物，双中心粒（如动物细胞内由两个中心粒组成的[中心体](../Page/中心体.md "wikilink")）的來源和雙鞭毛生物上的不同，為[趨同進化的一種](../Page/趨同進化.md "wikilink")。\[4\]

## 註釋

## 參考文獻

  -
## 外部連結

  - [Tree of Life.org: Eukaryotes](http://tolweb.org/Eukaryotes/3)

[D](../Category/真核生物.md "wikilink") [D](../Category/原生生物.md "wikilink")

1.

2.
3.

4.