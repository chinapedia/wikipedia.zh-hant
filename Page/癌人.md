<table>
<tbody>
<tr class="odd">
<td><p>「我曾試著把我個人的演戲技巧達到符合戲中人物角色的性格，並且以戲中人物角色的觀點來看整個世界。結果，戲中的反派角色還是不覺得他們自己是壞人。」<br />
（）</p></td>
</tr>
<tr class="even">
<td><p>— 演員<a href="../Page/William_B._Davis.md" title="wikilink">威廉·戴維斯說明他所飾演的人物角色</a>。[1]</p></td>
</tr>
</tbody>
</table>

**吸煙男**（），亦稱為**癌人**（）、**煙鏟**（）或**煙鏟史賓德**（），是著名美国[科幻电视连续剧](../Page/科幻.md "wikilink")《[X档案](../Page/X档案.md "wikilink")》中的人物之一，在[电视剧中他始终抽着香烟](../Page/电视剧.md "wikilink")，没有名字，他刺杀了[約翰·肯尼迪和](../Page/約翰·肯尼迪.md "wikilink")[马丁·路德·金](../Page/马丁·路德·金.md "wikilink")，是[影子政府的重要成员和主要行动负责人](../Page/影子政府.md "wikilink")，大部分X档案影迷认为他是男主角[福克斯·穆德的真正父亲](../Page/福克斯·穆德.md "wikilink")，因为他始终在背地以某种方式保护穆德。

癌人是个业余[作家](../Page/作家.md "wikilink")，但是不被[出版社看好](../Page/出版社.md "wikilink")，唯一一次发表的作品被出版社篡改严重。

## 参见

  - [X檔案](../Page/X檔案.md "wikilink")

## 參考資料

<div class="references-small">

<references/>

</div>

[he:תיקים באפלה\#דמויות
מרכזיות](../Page/he:תיקים_באפלה#דמויות_מרכזיות.md "wikilink")
[sv:Lista över figurer i Arkiv X\#Cigarette Smoking
Man](../Page/sv:Lista_över_figurer_i_Arkiv_X#Cigarette_Smoking_Man.md "wikilink")

[Category:虛構角色](../Category/虛構角色.md "wikilink")
[Category:X檔案](../Category/X檔案.md "wikilink")

1.