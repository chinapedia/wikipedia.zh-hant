**蒙特贝洛**（**Montebello**,
California，又譯**蒙提贝罗**）是[美国](../Page/美国.md "wikilink")[加利福尼亚州南部](../Page/加利福尼亚州.md "wikilink")[洛杉矶县的一座城市](../Page/洛杉矶县_\(加利福尼亚州\).md "wikilink")，是[洛杉矶市的住宅和工业区](../Page/洛杉矶市.md "wikilink")。2000年人口59,564人。

## 友好城市

  - [阿尔札赫共和国](../Page/阿尔札赫共和国.md "wikilink")[斯捷潘纳克特](../Page/斯捷潘纳克特.md "wikilink")

  - [日本](../Page/日本.md "wikilink")[芦屋市](../Page/芦屋市.md "wikilink")

[category:洛杉矶县城市](../Page/category:洛杉矶县城市.md "wikilink")

[M](../Category/洛杉矶县.md "wikilink")
[Category:1920年加利福尼亞州建立](../Category/1920年加利福尼亞州建立.md "wikilink")
[Category:1920年建立的聚居地](../Category/1920年建立的聚居地.md "wikilink")