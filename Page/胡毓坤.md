**胡毓坤**（）字**凌塵**，[遼寧](../Page/遼寧.md "wikilink")[海城人](../Page/海城.md "wikilink")，[中华民国军事将领](../Page/中华民国.md "wikilink")。\[1\]\[2\]

## 生平

[清朝](../Page/清朝.md "wikilink")[光緒二十一年](../Page/光緒.md "wikilink")（1895年）出生。畢業於[北京陸軍大學](../Page/北京.md "wikilink")。1925年任直隸第四混成旅旅長（第四混成旅隶属第一师，[李景林任师长](../Page/李景林.md "wikilink")）。1926年，李景林因反[張作霖而被解除军职](../Page/張作霖.md "wikilink")。李景林的部隊被改编为[安国軍第十六軍](../Page/安国軍.md "wikilink")、第十七軍，胡毓坤任[安國軍第四方面軍團第十六軍軍長](../Page/安國軍.md "wikilink")。\[3\]\[4\]

1928年，[張学良](../Page/張学良.md "wikilink")[东北易帜](../Page/东北易帜.md "wikilink")，胡毓坤任東北邊防司令長官公署參議。1929年，張学良强行接收[中東铁路](../Page/中東铁路.md "wikilink")，奉系和[苏联发生紛争](../Page/苏联.md "wikilink")。胡毓坤出任防俄軍第2軍軍長，同[苏联红军作战失利](../Page/苏联红军.md "wikilink")。\[5\]\[6\]1930年，因在[中東路事件立下的戰功](../Page/中東路事件.md "wikilink")，與張學良等人同受[青天白日勳章](../Page/青天白日勳章.md "wikilink")，為該勳章的首批受獎者之一。\[7\]1930年（民国19年），[中原大战爆发](../Page/中原大战.md "wikilink")，胡毓坤任討逆軍第3軍軍長，随張学良开入[山海关内同反](../Page/山海关.md "wikilink")[蒋介石軍交战](../Page/蒋介石.md "wikilink")。\[8\]\[9\]

1933年（民国22年）5月，任[國民政府軍事委員會北平分會委員](../Page/國民政府軍事委員會.md "wikilink")。1935年（民国24年）4月，获授陸軍中將。1935年12月，任[冀察政務委員會委員](../Page/冀察政務委員會.md "wikilink")。同年兼任中央绥靖委员会主任。\[10\]

1936年（民国25年）12月，[西安事变爆发之际](../Page/西安事变.md "wikilink")，胡毓坤支持張学良逮捕蒋介石。事变解決後，張学良反遭蒋介石扣押，引起胡毓坤不满。\[11\]\[12\]

1939年（民国29年），在[鮑文樾](../Page/鮑文樾.md "wikilink")、[杨毓珣的引诱下](../Page/杨毓珣.md "wikilink")，胡毓坤开始参加[汪精卫的投靠](../Page/汪精卫.md "wikilink")[日本](../Page/日本.md "wikilink")、反对蒋介石活動。1940年（民国29年）3月，[汪精卫政权建立](../Page/汪精卫政权.md "wikilink")，胡毓坤被任命为軍事委員会委員。同年4月，任蘇豫边区綏靖司令。1942年12月，改任豫皖苏鲁边区绥靖总司令。1943年10月5日，豫皖苏鲁边区绥靖总司令部奉命裁撤。1943年（民国32年）10月，调任軍事委員会特派駐華北委員，获授陸軍上将。翌年3月，任駐華北軍務長官。1945年（民国34年）4月，任軍事委員会参謀总長。翌月，任軍令部部長。\[13\]\[14\]\[15\]

[抗日战争胜利后](../Page/抗日战争.md "wikilink")，胡毓坤被蒋介石的国民政府逮捕。1946年（民国35年）5月24日，以[漢奸罪在](../Page/漢奸.md "wikilink")[南京被判处死刑](../Page/南京市.md "wikilink")，6月25日在南京被处决，享年62岁。\[16\]\[17\]\[18\]

## 参考文献

{{-}}

| [Flag_of_the_Republic_of_China-Nanjing_(Peace,_Anti-Communism,_National_Construction).svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_Republic_of_China-Nanjing_\(Peace,_Anti-Communism,_National_Construction\).svg "fig:Flag_of_the_Republic_of_China-Nanjing_(Peace,_Anti-Communism,_National_Construction).svg") 南京[国民政府](../Page/国民政府.md "wikilink")（[汪精卫政权](../Page/汪精卫政权.md "wikilink")） |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |

[Category:中華民國陸軍中将](../Category/中華民國陸軍中将.md "wikilink")
[Category:直军将领](../Category/直军将领.md "wikilink")
[Category:东北军将领](../Category/东北军将领.md "wikilink")
[Category:汪精卫政权军事人物](../Category/汪精卫政权军事人物.md "wikilink")
[Category:海城人](../Category/海城人.md "wikilink")
[Yu](../Category/胡姓.md "wikilink")
[Category:青天白日勳章獲得者](../Category/青天白日勳章獲得者.md "wikilink")

1.  徐友春主编，民国人物大辞典，河北人民出版社，2007年

2.

3.
4.
5.
6.
7.  祝康明，青天白日勳章，台北：知兵堂出版社，2011年

8.
9.
10.
11.
12.
13.
14.
15. 刘景泉编，中国抗日战争人物大词典，天津大学出版社，1999年

16.
17.
18. 任宝根，中国国民党高级将领征略，作家出版社，2007年