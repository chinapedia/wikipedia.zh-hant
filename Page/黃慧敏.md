**黃慧敏**（英文名：），曾為[香港](../Page/香港.md "wikilink")[亞洲電視藝員](../Page/亞洲電視.md "wikilink")，於2002年的[亞洲電視藝員訓練班畢業](../Page/亞洲電視藝員訓練班.md "wikilink")，其後再就讀2008年[aTV藝員訓練班](../Page/亞洲電視藝員訓練班.md "wikilink")，曾經為一名业餘[模特兒](../Page/模特兒.md "wikilink")。

至2008年8月，正式離開[亞洲電視](../Page/亞洲電視.md "wikilink")，進軍[電影界](../Page/電影.md "wikilink")。

## 演出作品

### 電視劇([亞洲電視](../Page/亞洲電視.md "wikilink"))

  - 2002年：《[暴風型警](../Page/暴風型警.md "wikilink")》
  - 2004年：《[媽媽我真的愛你](../Page/媽媽我真的愛你.md "wikilink")》
  - 2005年：《[美麗傳說II星願](../Page/美麗傳說II星願.md "wikilink")》
  - 2005年：《[危險人物](../Page/危險人物.md "wikilink")–滾油殺夫案》
  - 2005年：《[情陷夜中環](../Page/情陷夜中環.md "wikilink")》
  - 2007年：《[靈舍不同](../Page/靈舍不同.md "wikilink")》

### 節目主持([亞洲電視](../Page/亞洲電視.md "wikilink"))

  - 2003年-2005年：《[慧珊時尚坊](../Page/慧珊時尚坊.md "wikilink")》
  - 2003年：《[粵港越好玩](../Page/粵港越好玩.md "wikilink")》
  - 2004年：《[攪笑快閃黨](../Page/攪笑快閃黨.md "wikilink")》
  - 2004年：《[大王駕到](../Page/大王駕到.md "wikilink")》
  - 2004年：《[新会](../Page/新会.md "wikilink")[古兜温泉之旅](../Page/古兜温泉.md "wikilink")》
  - 2004年：《[至IN至潮中日韓](../Page/至IN至潮中日韓.md "wikilink")》
  - 2005年：《[香港奇蹟QTS](../Page/香港奇蹟QTS.md "wikilink")》
  - 2005年：《[攪笑快閃黨2](../Page/攪笑快閃黨2.md "wikilink")》
  - 2005年：《[大遊俠](../Page/大遊俠.md "wikilink")-[從化篇](../Page/從化.md "wikilink")》
  - 2006年-2007年：《[亞洲電視兒童台](../Page/亞洲電視兒童台.md "wikilink")》
  - 2006年-2007年：《[第一手真相](../Page/第一手真相.md "wikilink")》(外景主持)
  - 2007年：《[群星情報站](../Page/群星情報站.md "wikilink")》
  - 2007年-2008年：《[數碼天使信息](../Page/數碼天使信息.md "wikilink")》
  - 2007年-2008年：《[有腦E班](../Page/有腦E班.md "wikilink")》

### 電影

  - 2003年：《[龍咁威2003](../Page/龍咁威2003.md "wikilink")》
  - 2004年：《[新警察故事](../Page/新警察故事.md "wikilink")》
  - 2004年：《[絕代三嬌](../Page/絕代三嬌.md "wikilink")》
  - 2005年：《[Isabella](../Page/Isabella.md "wikilink")》
  - 2008年：《[貪污帝國](../Page/貪污帝國.md "wikilink")》

## 外部連結

[亞洲電視藝員資訊](https://web.archive.org/web/20070929132950/http://www.hkatv.com/info/atvstar/06/)
[黃慧敏之奧馬日記](http://www.hkflash.com/diary/diary.asp?id=omawong99) [OMA
LOVE YOU 黃慧敏 網誌(Yahoo！Blog)](http://hk.myblog.yahoo.com/omawong99)

[Category:前亞洲電視藝員](../Category/前亞洲電視藝員.md "wikilink")
[Category:香港女性模特兒](../Category/香港女性模特兒.md "wikilink")
[Hui慧敏](../Category/黃姓.md "wikilink")