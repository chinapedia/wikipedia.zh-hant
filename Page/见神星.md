**(114)
見神星**是一顆大且暗淡的[主帶小行星](../Page/主帶小行星.md "wikilink")，屬於[T-型小行星](../Page/T-型小行星.md "wikilink")。它是在1871年7月13日被[C.
H. F.
彼得斯發現的](../Page/克里斯蒂安·亨利·弗里德里希·彼得斯.md "wikilink")，被以[特洛伊戰爭中的](../Page/特洛伊戰爭.md "wikilink")[卡珊卓命名](../Page/卡珊卓.md "wikilink")，是人類發現的第114顆小行星。這顆小行星在2009年的影片：[隕石浩劫](../Page/隕石浩劫.md "wikilink")
(Meteor: Path to Destruction)
中被假設受到彗星撞擊而分裂成兩塊，並進入與[地球碰撞的軌道](../Page/地球.md "wikilink")。

這顆小行星的光譜分類為罕見的[T-型小行星](../Page/T-型小行星.md "wikilink")，它的部分光譜顯示有與[隕硫鐵性質相似的礦物](../Page/隕硫鐵.md "wikilink")，還有[碳質球粒隕石](../Page/碳質球粒隕石.md "wikilink")\[1\]。頻譜的特性也類似於1968年墜落在法國[奧農](../Page/奧農.md "wikilink")，顆粒細小的[奧農隕石](../Page/奧農隕石.md "wikilink")\[2\]。這顆小行星的[光度曲線顯示自轉週期為](../Page/光度曲線.md "wikilink")10.758
± 0.004小時，並且光度有0.25 ± 0.01 [星等的變化](../Page/星等.md "wikilink")\[3\]。

在2001年，[阿雷西博天文台曾以雷達觀測這顆小行星](../Page/阿雷西博天文台.md "wikilink")，返回的訊號匹配的有效直徑是100±14公里，這與經由其他方法計算的小行星尺寸一致\[4\]。

## 參考資料

[Category:主帶小行星](../Category/主帶小行星.md "wikilink")
[Category:T-型小行星](../Category/T-型小行星.md "wikilink")
[Category:以希臘神話命名的天體](../Category/以希臘神話命名的天體.md "wikilink")
[Category:小行星带天体](../Category/小行星带天体.md "wikilink")
[Category:1871年发现的小行星](../Category/1871年发现的小行星.md "wikilink")

1.
2.
3.
4.