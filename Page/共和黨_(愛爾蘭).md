**共和黨**（Fianna Fáil – The Republican
Party，是[爱尔兰语](../Page/爱尔兰语.md "wikilink")，意为“命运的士兵”）是成立于1926年的[爱尔兰政党](../Page/爱尔兰共和国.md "wikilink")，其成立建基于反對[英愛條約](../Page/英愛條約.md "wikilink")，支持[爱尔兰統一](../Page/爱尔兰統一.md "wikilink")，但反對採取武力手段。

共和黨是一個[中間偏右政黨](../Page/中間偏右.md "wikilink")，與另外兩大政黨[愛爾蘭統一黨及](../Page/愛爾蘭統一黨.md "wikilink")[愛爾蘭工黨並列共和國三大政黨](../Page/愛爾蘭工黨.md "wikilink")。至今一直影響愛爾蘭政治。

## 历史

該黨由1932－2011年，一直是[愛爾蘭下議院最大的第一政黨](../Page/愛爾蘭下議院.md "wikilink")，1932－1948年，1951－1954年，1957－1973年，1977－1981年，1982年，1987－1994年及1997－2011年是愛爾蘭的[執政黨](../Page/執政黨.md "wikilink")，曾長期執政。1990年代後，曾與工黨、綠黨及已解散的進步民主黨組建聯合政府。

2011年大選，共和黨大敗，喪失近八十年的最大黨地位，首次淪為第三大黨。共和黨的敗選，是自1921年愛爾蘭脫離英國統治獨立以來，該國任何政黨所經歷的最大支持度崩盤。\[1\]但到2016年大選，共和黨取得43席，增加23席，成為第二大黨，共和黨其後支持長期對立的統一黨組建[少數政府](../Page/少數政府.md "wikilink")。

## 歷任领袖

  - [埃蒙·德·瓦莱拉](../Page/埃蒙·德·瓦莱拉.md "wikilink")
    (1926–1959)（1932－1948年、1951－1954年、1957－1959年任總理）
  - [Seán Lemass](../Page/Seán_Lemass.md "wikilink")
    (1959–1966)（1959－1966年任總理）
  - [Jack Lynch](../Page/Jack_Lynch.md "wikilink")
    (1966–1979)（1966－1973年、1977－1979年任總理）
  - [Charles Haughey](../Page/Charles_Haughey.md "wikilink")
    (1979–1992)（1979－1981年、1982年、1987－1992年任總理）
  - [艾伯特·雷諾茲](../Page/艾伯特·雷諾茲.md "wikilink") (1992–1994)（1992–1994年任總理）
  - [伯蒂·埃亨](../Page/伯蒂·埃亨.md "wikilink") (1994–2008)（1997－2008年任總理）
  - [布雷恩·考恩](../Page/布雷恩·考恩.md "wikilink") (2008–2011) (2008–2011任總理)
  - [米高·馬丁](../Page/米高·馬丁.md "wikilink") (2011–)

## 大选结果

<table>
<thead>
<tr class="header">
<th><p>选举</p></th>
<th><p>众议院</p></th>
<th><p>得票率</p></th>
<th><p>议席</p></th>
<th style="text-align: center;"><p>结果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td></td>
<td><p>26.2%</p></td>
<td><p>44</p></td>
<td style="text-align: center;"><p>Cumann na nGaedhael 政府</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>35.2%</p></td>
<td><p>57</p></td>
<td style="text-align: center;"><p>Cumann na nGaedhael 政府</p></td>
</tr>
<tr class="odd">
<td><p>1932年</p></td>
<td></td>
<td><p>44.5%</p></td>
<td><p>72</p></td>
<td style="text-align: center;"><p><strong>共和党政府</strong></p></td>
</tr>
<tr class="even">
<td><p>1933年</p></td>
<td></td>
<td><p>49.7%</p></td>
<td><p>76</p></td>
<td style="text-align: center;"><p><strong>共和党政府</strong></p></td>
</tr>
<tr class="odd">
<td><p>1937年</p></td>
<td></td>
<td><p>45.2%</p></td>
<td><p>68</p></td>
<td style="text-align: center;"><p><strong>共和党政府</strong></p></td>
</tr>
<tr class="even">
<td><p>1938年</p></td>
<td><p>10</p></td>
<td><p>51.9%</p></td>
<td><p>76</p></td>
<td style="text-align: center;"><p><strong>共和党政府</strong></p></td>
</tr>
<tr class="odd">
<td><p>1943年</p></td>
<td><p>11</p></td>
<td><p>41.8%</p></td>
<td><p>66</p></td>
<td style="text-align: center;"><p><strong>共和党政府</strong></p></td>
</tr>
<tr class="even">
<td><p>1944年</p></td>
<td><p>12</p></td>
<td><p>48.9%</p></td>
<td><p>75</p></td>
<td style="text-align: center;"><p><strong>共和党政府</strong></p></td>
</tr>
<tr class="odd">
<td><p>1948年</p></td>
<td><p>13</p></td>
<td><p>41.9%</p></td>
<td><p>67</p></td>
<td style="text-align: center;"><p>統一黨－工黨政府</p></td>
</tr>
<tr class="even">
<td><p>1951年</p></td>
<td><p>14</p></td>
<td><p>46.3%</p></td>
<td><p>68</p></td>
<td style="text-align: center;"><p><strong>共和党政府</strong></p></td>
</tr>
<tr class="odd">
<td><p>1954年</p></td>
<td><p>15</p></td>
<td><p>43.4%</p></td>
<td><p>65</p></td>
<td style="text-align: center;"><p>統一黨－工黨政府</p></td>
</tr>
<tr class="even">
<td><p>1957年</p></td>
<td><p>16</p></td>
<td><p>48.3%</p></td>
<td><p>78</p></td>
<td style="text-align: center;"><p><strong>共和党政府</strong></p></td>
</tr>
<tr class="odd">
<td><p>1961年</p></td>
<td><p>17</p></td>
<td><p>43.8%</p></td>
<td><p>70</p></td>
<td style="text-align: center;"><p><strong>共和党政府</strong></p></td>
</tr>
<tr class="even">
<td><p>1965年</p></td>
<td><p>18</p></td>
<td><p>47.7%</p></td>
<td><p>72</p></td>
<td style="text-align: center;"><p><strong>共和党政府</strong></p></td>
</tr>
<tr class="odd">
<td><p>1969年</p></td>
<td><p>19</p></td>
<td><p>44.6%</p></td>
<td><p>74</p></td>
<td style="text-align: center;"><p><strong>共和党政府</strong></p></td>
</tr>
<tr class="even">
<td><p>1973年</p></td>
<td><p>20</p></td>
<td><p>46.2%</p></td>
<td><p>68</p></td>
<td style="text-align: center;"><p>統一黨－工黨政府</p></td>
</tr>
<tr class="odd">
<td><p>1977年</p></td>
<td><p>21</p></td>
<td><p>50.6%</p></td>
<td><p>84</p></td>
<td style="text-align: center;"><p><strong>共和党政府</strong></p></td>
</tr>
<tr class="even">
<td><p>1981年</p></td>
<td><p>22</p></td>
<td><p>45.3%</p></td>
<td><p>77</p></td>
<td style="text-align: center;"><p>統一黨－工黨政府</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>23</p></td>
<td><p>47.3%</p></td>
<td><p>81</p></td>
<td style="text-align: center;"><p><strong>共和党政府</strong></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>24</p></td>
<td><p>45.2%</p></td>
<td><p>75</p></td>
<td style="text-align: center;"><p>統一黨－工黨政府</p></td>
</tr>
<tr class="odd">
<td><p>1987年</p></td>
<td><p>25</p></td>
<td><p>44.2%</p></td>
<td><p>81</p></td>
<td style="text-align: center;"><p><strong>共和党政府</strong></p></td>
</tr>
<tr class="even">
<td><p>1989年</p></td>
<td><p>26</p></td>
<td><p>44.2%</p></td>
<td><p>77</p></td>
<td style="text-align: center;"><p><strong>共和党–進步民主黨政府</strong></p></td>
</tr>
<tr class="odd">
<td><p>1992年</p></td>
<td><p>27</p></td>
<td><p>39.1%</p></td>
<td><p>68</p></td>
<td style="text-align: center;"><p><strong>共和党–工黨政府</strong>(1992至1994年)<br />
統一黨－工黨政府(1994至1997年)</p></td>
</tr>
<tr class="even">
<td><p>1997年</p></td>
<td><p>28</p></td>
<td><p>39.3%</p></td>
<td><p>77</p></td>
<td style="text-align: center;"><p><strong>共和党–進步民主黨政府</strong></p></td>
</tr>
<tr class="odd">
<td><p>2002年</p></td>
<td><p>29</p></td>
<td><p>41.5%</p></td>
<td><p>81</p></td>
<td style="text-align: center;"><p><strong>共和党–進步民主黨政府</strong></p></td>
</tr>
<tr class="even">
<td><p>2007年</p></td>
<td><p>30</p></td>
<td><p>41.6%</p></td>
<td><p>77</p></td>
<td style="text-align: center;"><p><strong>共和党–綠黨–進步民主黨政府</strong></p></td>
</tr>
<tr class="odd">
<td><p>2011年</p></td>
<td><p>31</p></td>
<td><p>15.1%</p></td>
<td><p>20</p></td>
<td style="text-align: center;"><p>統一党–工黨政府</p></td>
</tr>
<tr class="even">
<td><p>2016年</p></td>
<td><p>32</p></td>
<td><p>24.3%</p></td>
<td><p>44</p></td>
<td style="text-align: center;"><p>統一党政府</p></td>
</tr>
</tbody>
</table>

## 参考

[Category:愛爾蘭政黨](../Category/愛爾蘭政黨.md "wikilink")
[Category:自由主義政黨](../Category/自由主義政黨.md "wikilink")
[Category:共和主義政黨](../Category/共和主義政黨.md "wikilink")
[Category:自由保守主義政黨](../Category/自由保守主義政黨.md "wikilink")

1.  [愛爾蘭變天
    執政黨大選慘敗](http://www.libertytimes.com.tw/2011/new/feb/27/today-int7.htm)
    [自由時報](../Page/自由時報.md "wikilink") 2011-02-27