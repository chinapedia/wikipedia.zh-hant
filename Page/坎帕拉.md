[KampalaDot.png](https://zh.wikipedia.org/wiki/File:KampalaDot.png "fig:KampalaDot.png")
[KampalaSkyline.jpg](https://zh.wikipedia.org/wiki/File:KampalaSkyline.jpg "fig:KampalaSkyline.jpg")
[Laika_ac_Gaddafi_National_Mosque,_Kampala_(6693328097).jpg](https://zh.wikipedia.org/wiki/File:Laika_ac_Gaddafi_National_Mosque,_Kampala_\(6693328097\).jpg "fig:Laika_ac_Gaddafi_National_Mosque,_Kampala_(6693328097).jpg")
[Africa's_Bahai_temple_in_Kampala.jpg](https://zh.wikipedia.org/wiki/File:Africa's_Bahai_temple_in_Kampala.jpg "fig:Africa's_Bahai_temple_in_Kampala.jpg")
**坎帕拉**（），[烏干達首都](../Page/烏干達.md "wikilink")，人口150万。海拔1,189公尺。

[巴哈伊教七座靈曦堂之一座落於此](../Page/巴哈伊信仰.md "wikilink")。 　

## 参见

  - [马凯雷雷大学](../Page/马凯雷雷大学.md "wikilink")

## 參考資料

Vimeo Cookie Policy

## 外部連結

  -
[坎帕拉](../Category/坎帕拉.md "wikilink")
[Category:烏干達城市](../Category/烏干達城市.md "wikilink")
[Category:非洲首都](../Category/非洲首都.md "wikilink")
[Category:维多利亚湖](../Category/维多利亚湖.md "wikilink")