[真福](../Page/真福.md "wikilink")[教宗](../Page/教宗.md "wikilink")**額我略十世**（，約1210年－1276年1月10日），本名**Theobald
Visconti**，於1271年9月1日－1276年1月10日岀任教宗，是意大利籍的教宗，而他的即位，也代表結束了史上最著名的[宗座從缺期](../Page/宗座從缺.md "wikilink")。

## 譯名列表

  - 額我略十世：[天主教香港教區禮儀委員會：禧年專頁](http://catholic-dlc.org.hk/2000.htm) 作額我略。
  - 國瑞十世：[香港天主教教區檔案　歷任教宗](https://web.archive.org/web/20051023073732/http://archives.catholic.org.hk/popes/index.htm)
    作國瑞。
  - 格列高列十世：[《大英簡明百科知識庫》2005年版](http://wordpedia.britannica.com/concise/quicksearch.aspx?keyword=gregory&mode=3)
    作格列高列。
  - 格雷戈里十世：《[世界人名翻譯大辭典](../Page/世界人名翻譯大辭典.md "wikilink")》1993年版作格雷戈里。
  - 格列哥里十世：[國立編譯舘](http://www.nict.gov.tw/tc/dic/index1.php) 作格列哥里。

[G](../Category/教宗.md "wikilink")
[Category:義大利出生的教宗](../Category/義大利出生的教宗.md "wikilink")