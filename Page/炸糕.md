**炸糕**，是[北京](../Page/北京.md "wikilink")、[天津两地人民愛吃的面食](../Page/天津.md "wikilink")，常做早點。

## 概述

由元朝蒙族人的饮食沿袭下来，成为北京天津地区的饮食文化\[1\]。炸糕有以下幾種：

  - [奶油炸糕](../Page/奶油炸糕.md "wikilink")（以面粉、白糖、奶油、鸡蛋等制成饼装油炸，比如北京[东来顺饭庄](../Page/东来顺饭庄.md "wikilink")\[2\]）
  - [黃米麵炸糕](../Page/黃米麵炸糕.md "wikilink")（以黄米、豆馅为主料）\[3\]
  - [江米麵炸糕](../Page/江米麵炸糕.md "wikilink")，[江米粉加入适当发酵面](../Page/江米.md "wikilink")、成团面，做成小剂压扁，中间包裹[桂花](../Page/桂花.md "wikilink")、[红小豆馅](../Page/红小豆.md "wikilink")，温油中油炸。外脆里内，呈金黄色\[4\]。
  - [燙麵炸糕](../Page/燙麵炸糕.md "wikilink")，把水烧开后，倒入面粉搅拌均匀，面烫好后出锅分成大块，摊开晾凉，对上发面和适量碱面，揉匀揪成小剂，摁扁，包上用红糖、桂花、面干拌匀制成的馅儿，用温油炸\[5\]。
  - [耳朵眼炸糕](../Page/耳朵眼炸糕.md "wikilink")，[天津著名小吃](../Page/天津.md "wikilink")，是天津的“三绝”食品之一。

## 相关

  - [油炸糕](../Page/油炸糕.md "wikilink")

## 參見

## 外部链接

  - [華夏之旅 -
    炸糕](https://web.archive.org/web/20051126060738/http://big5.ccnt.com.cn/culture/gudubeijing/jingchengfengsu/xiaochi/xiaochi09.htm)

[Z](../Page/category:京津小吃.md "wikilink")

[Z](../Category/油炸麵食.md "wikilink") [Z](../Category/中式糕點.md "wikilink")

1.
2.
3.
4.
5.