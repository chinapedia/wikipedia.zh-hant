**戴淮清**Tai Huai
Ching（1908年-1995年）新加坡星洲日报社新闻记者，摄影家，加拿大汉语音转学家。祖籍[广东](../Page/广东.md "wikilink")[大埔](../Page/大埔縣.md "wikilink")，1908年出生于马来亚（今[马来西亚](../Page/马来西亚.md "wikilink")）[槟城](../Page/槟城.md "wikilink")，原为槟城望族，后迁居霹雳州（太平）\[1\]
。父戴月初为前清秀才，博览群书，诗文并茂。童时由家父讲解[左传](../Page/左传.md "wikilink")、[诗经](../Page/诗经.md "wikilink")、[史记](../Page/史记.md "wikilink")、[古文观止](../Page/古文观止.md "wikilink")、和[唐诗三百首](../Page/唐诗三百首.md "wikilink")。1919年，父亲逝世，乃由南洋回中国[潮州上小学](../Page/潮州.md "wikilink")。1926年入黄埔军校。当时校长是蒋中正，教导主任人是周恩平，政治部主任是郭沫若。1927年入[上海](../Page/上海.md "wikilink")[真茹](../Page/真茹.md "wikilink")[暨南大学文哲学院外国文学系](../Page/暨南大学.md "wikilink")，师从[叶公超](../Page/叶公超.md "wikilink")\[2\]；同年与陈翔斌、郑泗水、温梓川等南洋侨生创立中国第一个南洋文学社团秋野社\[3\]，戴为《秋野月刊》撰写文坛消息\[4\]
。1930年l老师叶公超到[燕京大学任教](../Page/燕京大学.md "wikilink")，乃转入[燕京大学英语系](../Page/燕京大学.md "wikilink")，师从[叶公超](../Page/叶公超.md "wikilink")，[洪深](../Page/洪深.md "wikilink")；并到[清华大学旁听](../Page/清华大学.md "wikilink")[艾弗·阿姆斯壮·理查兹](../Page/艾弗·阿姆斯壮·理查兹.md "wikilink")(I.A.Richards)的文艺批评理论。1934年燕京大学毕业\[5\]，与清华大学英语系毕业生唐锦云女士结婚，婚后赴[广州](../Page/广州.md "wikilink")[中山大学文学院英文系任教](../Page/中山大学.md "wikilink")，授理查兹带来中国只用850个单词的[基本英语和诗学](../Page/基本英语.md "wikilink")\[6\]。1937年移居[香港](../Page/香港.md "wikilink")，出任[星岛日报记者](../Page/星岛日报.md "wikilink")。开始研究彩色摄影术，以相当十兩黄金的价格，从美国德雯彩色照相机公司（Devin
Colorgraph Co)购买一台德雯三分色相机（Devin One Shot Tricolor camera)，一次拍摄红色、绿色、蓝色
6.5x9厘米玻璃底片各一张，分别印制蓝绿色、洋红色、黄色正片，将色胶层从玻璃剥下，重叠而成彩色相片，为广告商在电影院投射用\[7\]。1941年12月8日[日军侵略香港](../Page/日军.md "wikilink")，戴淮清转赴[越南](../Page/越南.md "wikilink")，在[湄公河三角洲](../Page/湄公河.md "wikilink")[檳椥市任崇正学校校长](../Page/檳椥省.md "wikilink")，推广汉字教学。1942年移居題岸，同燕京大學老同學溫志達（唐吉珂德最早漢譯者）合作開辦化妝品廠。1945年[太平洋战争结束](../Page/太平洋战争.md "wikilink")，经友人[连士升介绍](../Page/连士升.md "wikilink")：“[南洋是个好地方](../Page/南洋.md "wikilink")”，乃与连士升一家结伴搭轮船同赴[新加坡](../Page/新加坡.md "wikilink")。连士升任南洋商报主编。戴淮清入《[星洲日报](../Page/星洲日报.md "wikilink")》任国际版主编。戴淮清经常在该报发表社论，并在星洲日报社《星洲周刊》发表[摄影术和](../Page/摄影术.md "wikilink")《汉语音转学》的文章，后由星洲日报社印发蓝布面精装单行本。1946年-1986年为[南洋学会会员](../Page/南洋学会.md "wikilink")。五十年代初曾被星洲日报社派往欧洲考察，返新后者星洲日报副刊发表《欧游杂记》。1964年代表星洲日报社出席在[台北举行的第](../Page/台北.md "wikilink")11届亚洲电影节，新马电影界代表团团长是[陆运涛](../Page/陆运涛.md "wikilink")，影展闭幕后，新马代表团曾受[蒋介石总统和夫人](../Page/蒋介石.md "wikilink")[宋美龄接见](../Page/宋美龄.md "wikilink")，并摄影留念\[8\]。后来陆运涛夫妇及代表团其他成员，应邀乘飞机到台中縣霧峰鄉参观雾峰故宫国宝，不幸飞机失事遇难\[9\]。戴淮清因故先行返回新加坡。1973年到北京旅游，被国务院邀请在天安门观礼台参加十月一日国庆典礼，晚间被邀请参加周恩来总理在人民大会堂招待华侨的晚宴。

1986年移民[加拿大](../Page/加拿大.md "wikilink")[埃德蒙顿](../Page/埃德蒙顿.md "wikilink")，从事汉语音韵学的研究。1995年7月15日在埃德蒙顿逝世\[10\]

## 著作

[Sin_Chew_Weekly_Issue_26_Singapore_Table_of_content.jpg](https://zh.wikipedia.org/wiki/File:Sin_Chew_Weekly_Issue_26_Singapore_Table_of_content.jpg "fig:Sin_Chew_Weekly_Issue_26_Singapore_Table_of_content.jpg")
[Introduction_o_Photography.jpg](https://zh.wikipedia.org/wiki/File:Introduction_o_Photography.jpg "fig:Introduction_o_Photography.jpg")
[A_Study_of_Chinese_Words.jpg](https://zh.wikipedia.org/wiki/File:A_Study_of_Chinese_Words.jpg "fig:A_Study_of_Chinese_Words.jpg")
[Chinese_Phonetics_SG.jpg](https://zh.wikipedia.org/wiki/File:Chinese_Phonetics_SG.jpg "fig:Chinese_Phonetics_SG.jpg")

  - 《给《南洋时报》副刊《南洋的文艺》的编者陈旧燕的一封信》 1930年\[11\]
  - 《灯光人像》 \[12\].
  - 《灯光人像续》 \[13\]
  - 《静物》 \[14\]
  - 《摄影入门》1952 星洲日报社出版。
  - 《文字的研究 》戴淮清著 星洲世界書局 1960
  - 《音转学发凡》 戴淮清著 星洲世界书局 1962.3
  - 《以音转原理 正名辨义》(戴淮清) \[15\]
  - 《汉语音转学》戴淮清著 1974 新加坡
  - 《中國語音轉化》 臺北市 中國民俗學會 1974
  - (加拿大)戴淮清著《汉语音转学》 中国友谊出版公司 1986.12
  - 《释诗三篇》《星洲日报》 1959年6月12，13
  - 《古文疑义新解》 《星洲日报》1960年5月26-6月8日
  - 戴淮清
    《揭開大秦[景教的秘密](../Page/景教.md "wikilink")》，《[明報月刊](../Page/明報月刊.md "wikilink")》1989年
    3月 88-91
  - 《大秦历史重考》戴维清　《文学知识》1989年第三期（总九十四期）
  - (加拿大)戴淮清著《[汉书](../Page/汉书.md "wikilink")·[西域传所记](../Page/西域.md "wikilink")"[乌弋](../Page/乌弋.md "wikilink")"地望辨正》,
    《中国边疆史地研究》1993年第2期。
  - 戴淮清序 牛汝辰编 《 新疆地名概说》中央民族学院出版社 1994 ISBN 9787810016988

## 摄影作品

戴淮清先生战前爱用康泰时（CONTAX）IIA相机配 蔡司Sonnar 5cm
F1.5镜头，战后爱用莱卡（LEICA）IIIf相机配莱卡50毫米F1.5
Summarit镜头或F2 Summitar镜头\[16\]

<File:1-Zi> Luolian.jpg|华南女明星[紫罗莲](../Page/紫罗莲.md "wikilink") (Leica
IIIf + 50毫米F1.5 Summarit 镜头） <File:Kasmah> Booty in 1952.jpg|马来女明星Kasmah
Booty（Leica　IIIｆ　＋５０毫米Ｆ２　Ｓｕｍｍｉｔａｒ镜头）

## 家庭

妻子 唐锦云 （1949-1955年） 新加坡华侨中学国文教师。

长子戴冕，1960年北京大学物理系毕业。曾编译出版《达芬奇论绘画》，北京人民美术出版社1963年。1973年移民香港在中环美资公司任设计工程师。1974年移民加拿大。曾在加拿大原子能研究所及加拿大航天工厂任工程师。

次子戴渊 （Dr Willian Yuen Tai)，香港大学历史系博士，荷兰大学东南亚研究所研究员,主要著作 Chinese
Capitalism in Colonial Malaya\[17\] 2005年出版 The Origins of China's
Awareness of Newzealand 1674-1911 Universty of Auckland, Newzealand Asia
Institude. 2018年 出版《英属马来亚华人资本主义经济》，

子戴超博士，（Dr.Chao Tai)，University of Alberta 医学博士，加拿大,埃德蒙顿市 脑神经科专家。

女 戴融， Dr. Janet Tai Landa （多伦多约克大学经济学教授） 主要著作 Economic Success of
Chinese Merchants in Southeast Asia: Identity, Ethnic Cooperation and
Conflict \[18\]

## 参考文献

<div class="references-small">

<references />

</div>

[category:香港记者](../Page/category:香港记者.md "wikilink")
[category:新加坡记者](../Page/category:新加坡记者.md "wikilink")
[分类:新加坡客家人](../Page/分类:新加坡客家人.md "wikilink")
[category:汉语音韵学家](../Page/category:汉语音韵学家.md "wikilink")
[category:大埔人](../Page/category:大埔人.md "wikilink")
[category:国立中山大学教授](../Page/category:国立中山大学教授.md "wikilink")
[category:1908年出生](../Page/category:1908年出生.md "wikilink")
[category:1995年逝世](../Page/category:1995年逝世.md "wikilink")
[T](../Page/分类:马来西亚客家人.md "wikilink")
[T](../Page/分类:戴姓.md "wikilink")

[Category:燕京大学校友](../Category/燕京大学校友.md "wikilink")

1.  温梓川《文人的另一面》广西大学出版社 2004年 第157页
2.  吴长斌《怀念叶师公超》 -《叶崇德（叶公超之胞妹）:《回忆叶公超》学林出版社,第51页：
    “葉師在暨大任教時之得意門生是戴淮清〈後轉學燕京外文系)及温玉舒(梓川》”
    1993
3.  [《南洋文艺第一个文学社团秋野社》](http://www.docin.com/p-1452862831.html)
4.  [马来西亚](../Page/马来西亚.md "wikilink")[温梓川](../Page/温梓川.md "wikilink")
    《文人的另一面:民国风景之一种》146页
5.  《[燕京大学人物志](../Page/燕京大学.md "wikilink")》 425页 北京大学出版社 2002
6.  《文字的研究》 第112页
7.  《摄影入门》，184-190页
8.  [美联社台北](../Page/美联社.md "wikilink")1964年6月18日照片与电文
9.
10. 新加坡联合早报1995年7月15日讣告
11. 杨松年著 《战前新马文学本地意识的形成与发展》 - Page 61“1930 年 2
    月,身在中国的一位作者戴淮清,在致给《南洋时报》副刊《南洋的文艺》的编者陈旧燕的一封信中表示:
    "人们只知道经济,政治需要革命,却不知人性也需要革命,人们要求经济和政治上的自由平等,却未曾听过他们说过要人性的自由平等” 1930年
12. 1951年 《星洲周刊》 第25期 第19页
13. 1951年 《星洲周刊》 第26期 第19页
14. 1951年 《星洲周刊》 第 27 期 第 19页
15. 汉学研究之回顾与前瞻: 新加坡国立大学中文系主办国际汉学会议论文选集,中华书局 1995
16. 《摄影入门》第9页
17. [](http://williamyuentai.com/Author.htm)
18. [1](https://books.google.ca/books?id=LseiDQAAQBAJ&pg=PP13&dq=%E6%88%B4%E6%B7%AE%E6%B8%85&hl=en&sa=X&redir_esc=y#v=onepage&q=%E6%88%B4%E6%B7%AE%E6%B8%85&f=false)