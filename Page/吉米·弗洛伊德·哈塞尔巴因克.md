**杰热尔·弗洛伊德·哈塞尔巴因克**（，常被称为**吉米·弗洛伊德·哈塞尔巴因克**（，）是一名前[荷蘭職業足球員](../Page/荷蘭.md "wikilink")，曾代表[荷蘭國家隊](../Page/荷蘭國家足球隊.md "wikilink")。在2008年夏季不獲效力的[英冠球會](../Page/英格蘭足球冠軍聯賽.md "wikilink")[卡迪夫城續約](../Page/卡迪夫城足球俱乐部.md "wikilink")，9月15日宣佈退役，哈索賓基在[英超共射入](../Page/英超.md "wikilink")128球\[1\]。退役後擔任教練工作，於2013/14球季擔任比乙球會[安特卫普的領隊](../Page/皇家安特卫普足球俱乐部.md "wikilink")，於取得第七位後離隊謀求返回英格蘭執教，前[英格蘭足球冠軍聯賽球會](../Page/英格蘭足球冠軍聯賽.md "wikilink")[女王公园巡游者領隊](../Page/女王公园巡游者足球俱乐部.md "wikilink")。

## 生平

哈索賓基早年出身於[荷蘭一些小型球會](../Page/荷蘭.md "wikilink")，曾在[葡萄牙聯賽效力多年](../Page/葡萄牙.md "wikilink")。至1997年哈索賓基轉到[英超加盟英超球會](../Page/英超.md "wikilink")[列斯聯](../Page/利兹联足球俱乐部.md "wikilink")，曾協助球會打入聯賽前列。

兩季後哈索賓基轉投[西甲球會](../Page/西甲.md "wikilink")[馬德里體育會](../Page/馬德里體育會.md "wikilink")。這次轉會是哈索賓基職業生涯的最低潮，因為第一年球會正要面臨降班威脅，加上球會大亂，最後導致降級。哈索賓基最後只好於2000年重返[英超加盟](../Page/英超.md "wikilink")[車路士](../Page/車路士.md "wikilink")。

效力[車路士期間哈索賓基表現相當突出](../Page/車路士.md "wikilink")。入球能力高超的哈索賓基，曾一度成為兩屆[英超神射手](../Page/英超.md "wikilink")（1998/99及2000/01）。當時哈索賓基還是球會入球量最多的球員之一。

哈索賓基當時表現突出，獲得[英超神射手等個人獎項](../Page/英超.md "wikilink")。惟自球會由[俄羅斯億萬富翁](../Page/俄羅斯.md "wikilink")[阿巴莫域治收購](../Page/阿巴莫域治.md "wikilink")，球會實力大幅提升；而這時哈索賓基年事已高狀態下滑，正選地位不保。最後於2004年轉投[米杜士堡](../Page/米杜士堡足球會.md "wikilink")，再轉到[查爾頓](../Page/查尔顿竞技足球俱乐部.md "wikilink")。

效力[查爾頓僅一個球季因球隊降級](../Page/查尔顿竞技足球俱乐部.md "wikilink")，哈索賓基被球會解約放棄。於2007-08球季展開後加盟[卡迪夫城](../Page/卡迪夫城足球俱乐部.md "wikilink")，簽約一年\[2\]。加盟首季，卡迪夫城自1927年奪冠後首次晉身[英格蘭足總盃](../Page/英格蘭足總盃.md "wikilink")，但哈索賓基的重要性不及隊中主力，球季結束後他继续讨要续约合同时遭到拒绝，并就此退役\[3\]

2011年7月21日，哈索賓基加盟[英冠球會](../Page/英冠.md "wikilink")[诺丁汉森林為一隊教練](../Page/诺丁汉森林足球俱乐部.md "wikilink")，與同時新加盟領隊[史提夫·麥卡倫繼](../Page/史提夫·麥卡倫.md "wikilink")[米杜士堡後再次合作](../Page/米杜士堡足球會.md "wikilink")\[4\]。

2013年1月2日，哈索賓基離開[诺丁汉森林一隊教練職位](../Page/诺丁汉森林足球俱乐部.md "wikilink")\[5\]。

2013年5月29日，[比利時足球乙級聯賽球會](../Page/比利時足球乙級聯賽.md "wikilink")[安特卫普任命哈索賓基為領隊](../Page/皇家安特卫普足球俱乐部.md "wikilink")，合約為期一年\[6\]。

## 家庭

哈索賓基的哥哥（Carlos
Hasselbaink）同樣是一名足球員，曾效力[荷甲球隊](../Page/荷甲.md "wikilink")[AZ阿爾克馬爾及](../Page/AZ阿爾克馬爾.md "wikilink")[德國的小型球會](../Page/德國.md "wikilink")，現已退役。

## 榮譽

  - 英超神射手：1998-99、2000-01。

## 参考資料

## 外部連結

  - [Premier League
    profile](http://www.premierleague.com/en-gb/players/profile.career-history.html/jimmy-floyd-hasselbaink)

  - [Charlton
    profile](https://web.archive.org/web/20090917101128/http://www.cafc.co.uk/playerdisplay.ink?skip=14&season=2006%2F2007)

  -
[Category:荷兰足球运动员](../Category/荷兰足球运动员.md "wikilink")
[Category:蘇里南足球運動員](../Category/蘇里南足球運動員.md "wikilink")
[Category:足球前鋒](../Category/足球前鋒.md "wikilink")
[Category:阿爾克馬爾球員](../Category/阿爾克馬爾球員.md "wikilink")
[Category:博維斯塔球員](../Category/博維斯塔球員.md "wikilink")
[Category:列斯聯球員](../Category/列斯聯球員.md "wikilink")
[Category:馬德里體育會球員](../Category/馬德里體育會球員.md "wikilink")
[Category:車路士球員](../Category/車路士球員.md "wikilink")
[Category:米杜士堡球員](../Category/米杜士堡球員.md "wikilink")
[Category:查爾頓球員](../Category/查爾頓球員.md "wikilink")
[Category:卡迪夫城球員](../Category/卡迪夫城球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:葡超球員](../Category/葡超球員.md "wikilink")
[Category:西甲球員](../Category/西甲球員.md "wikilink")
[Category:荷蘭足球主教練](../Category/荷蘭足球主教練.md "wikilink")
[Category:安特衛普主教練](../Category/安特衛普主教練.md "wikilink")
[Category:保頓艾爾賓領隊](../Category/保頓艾爾賓領隊.md "wikilink")
[Category:昆士柏流浪領隊](../Category/昆士柏流浪領隊.md "wikilink")
[Category:蘇里南裔荷蘭人](../Category/蘇里南裔荷蘭人.md "wikilink")
[Category:歸化英國公民](../Category/歸化英國公民.md "wikilink")
[Category:1998年世界盃足球賽球員](../Category/1998年世界盃足球賽球員.md "wikilink")
[Category:英格蘭足球聯賽球員](../Category/英格蘭足球聯賽球員.md "wikilink")
[Category:英格蘭足球聯賽領隊](../Category/英格蘭足球聯賽領隊.md "wikilink")

1.  [Hasselbaink retires from
    football](http://news.bbc.co.uk/sport2/hi/football/7617250.stm)
2.  [Hasselbaink signs for
    Bluebirds](http://news.bbc.co.uk/sport2/hi/football/teams/c/cardiff_city/6949220.stm)
3.  [哈塞尔巴因克讨要续约合同](http://china.goal.com/cn/Articolo.aspx?ContenutoId=762597)
4.
5.
6.