## 艺术

  - J0　[艺术理论](../Page/艺术.md "wikilink")

:\*J01 [艺术美学](../Page/艺术美学.md "wikilink")

:\*J02 艺术理论的基本问题

:\*J03 [艺术工作者](../Page/艺术工作者.md "wikilink")

:\*J04 [艺术创作方法和经验](../Page/艺术创作.md "wikilink")

:\*J05 [艺术评论欣赏](../Page/艺术评论.md "wikilink")

  - J1　世界各国艺术概况

:\*J11 世界艺术

:\*J12 [中国艺术](../Page/中国艺术.md "wikilink")

:::\*J120.9 [中国艺术史](../Page/中国艺术史.md "wikilink")

  - J2　[绘画](../Page/绘画.md "wikilink")

::\*J2-61 [美术辞典](../Page/美术辞典.md "wikilink")

:\*J20 [绘画理论](../Page/绘画理论.md "wikilink")

::\*J201 [绘画美学](../Page/绘画美学.md "wikilink")

::\*J202 绘画艺术理论的基本问题

::\*J203 [绘画工作者](../Page/绘画工作者.md "wikilink")

::\*J204 绘画[创作方法和经验](../Page/创作.md "wikilink")

::\*J205 [绘画评论与欣赏](../Page/绘画评论.md "wikilink")

:\*J21 [绘画技法](../Page/绘画技法.md "wikilink")

::\*J211 一般技法

::\*J212 [中国画画技法](../Page/中国画.md "wikilink")

::\*J213 [油画技法](../Page/油画.md "wikilink")

::\*J214 [素描](../Page/素描.md "wikilink")、[速写技法](../Page/速写.md "wikilink")

::\*J215
[粉画](../Page/粉画.md "wikilink")、[蜡笔画技法](../Page/蜡笔画.md "wikilink")

::\*J217 [版画技法](../Page/版画.md "wikilink")

::\*J218 各种用途画技法

:\*J22 [中国绘画](../Page/中国绘画.md "wikilink")

::\*J221 作品综合集

::\*J222 [国画](../Page/国画.md "wikilink")

::\*J223
[油画](../Page/油画.md "wikilink")、[漆画](../Page/漆画.md "wikilink")、[涂料画](../Page/涂料画.md "wikilink")

::\*J224 [素描](../Page/素描.md "wikilink")、[速写](../Page/速写.md "wikilink")

::\*J225 [水彩](../Page/水彩.md "wikilink")、[水粉画](../Page/水粉画.md "wikilink")

::\*J226 [粉画](../Page/粉画.md "wikilink")、[蜡笔画](../Page/蜡笔画.md "wikilink")

::\*J227 [版画](../Page/版画.md "wikilink")

::\*J228 各种用途画

:::\*J228.1 [宣传画](../Page/宣传画.md "wikilink")

:::\*J228.2 [漫画](../Page/漫画.md "wikilink")

:::\*J228.3 [年画](../Page/年画.md "wikilink")

:::\*J228.4 [连环画](../Page/连环画.md "wikilink")

:::\*J228.7
[动画](../Page/动画.md "wikilink")（[卡通](../Page/卡通.md "wikilink")）

:\*J23 各国绘画

::\*J231 作品综合集

::\*J232 民族技法画

::\*J233
[油画](../Page/油画.md "wikilink")、[漆画](../Page/漆画.md "wikilink")、[涂料画](../Page/涂料画.md "wikilink")

::\*J234 [素描](../Page/素描.md "wikilink")、[速写](../Page/速写.md "wikilink")

::\*J235 [水彩](../Page/水彩.md "wikilink")、[水粉画](../Page/水粉画.md "wikilink")

::\*J236 [粉画](../Page/粉画.md "wikilink")、[蜡笔画](../Page/蜡笔画.md "wikilink")

::\*J237 [版画](../Page/版画.md "wikilink")

::\*J238 各种用途画

:::\*J238.1
[漫画](../Page/漫画.md "wikilink")、[宣传画](../Page/宣传画.md "wikilink")

:::\*J238.7
[动画](../Page/动画.md "wikilink")（[卡通](../Page/卡通.md "wikilink")）

:\*J29 [书法](../Page/书法.md "wikilink")[篆刻](../Page/篆刻.md "wikilink")

::\*J292 中国书法、篆刻

::::\*J292.11 [毛笔字](../Page/毛笔.md "wikilink")

::::\*J292.12 [硬笔字](../Page/硬笔字.md "wikilink")

::::\*J292.13 [美术字](../Page/美术字.md "wikilink")

:::\*J292.2 [碑贴](../Page/碑贴.md "wikilink")、书法作品：按时代分

::::\*J292.21 碑贴丛刻

:::\*J292.3 书法作品：按书体分

:::\*J292.4 [篆刻](../Page/篆刻.md "wikilink")、治印及作品

::\*J293 [外文书法](../Page/外文书法.md "wikilink")

  - J3　[雕塑](../Page/雕塑.md "wikilink")

::\*J305 雕塑评论、欣赏

:\*J31 [雕塑技法](../Page/雕塑技法.md "wikilink")

:\*J32 [中国雕塑](../Page/中国雕塑.md "wikilink")

:\*J33 各国雕塑

  - J4　[摄影艺术](../Page/摄影.md "wikilink")

:\*J40 摄影艺术理论

::\*J406 摄影造型艺术理论

:\*J41 [摄影技术](../Page/摄影技术.md "wikilink")

:\*J42 中国摄影艺术

:\*J43 各国摄影艺术

  - J5　[工艺美术](../Page/工艺美术.md "wikilink")

:\*J51 [图案学](../Page/图案学.md "wikilink")

:\*J52 中国工艺美术

::\*J522 图案集

::\*J523
[染织](../Page/染织.md "wikilink")、[刺绣](../Page/刺绣.md "wikilink")、[服装](../Page/服装.md "wikilink")

::\*J524 工商工艺美术

::\*J525 [装饰美术](../Page/装饰美术.md "wikilink")

::\*J526 [金属工艺美术](../Page/金属工艺.md "wikilink")

::\*J527 [陶瓷](../Page/陶瓷.md "wikilink")、[漆器](../Page/漆器.md "wikilink")

::\*J528 民间[工艺美术](../Page/工艺美术.md "wikilink")

:\*J59 [建筑艺术](../Page/建筑.md "wikilink")

  - J6　[音乐](../Page/音乐.md "wikilink")

::\*J6-61 音乐词典

:\*J60 音乐理论

:\*J61 音乐技术理论与方法

::\*J613 基本乐科

:::\*J613.1 [视唱练耳](../Page/视唱.md "wikilink")

:::\*J613.2
[读谱法](../Page/读谱法.md "wikilink")、[记谱法](../Page/记谱法.md "wikilink")

::\*J616 [声乐理论](../Page/声乐理论.md "wikilink")

::::.1 [发声法](../Page/发声法.md "wikilink")

:::\*J616.2 [歌唱法](../Page/歌唱法.md "wikilink")

::\*J617 [戏剧音乐理论](../Page/戏剧音乐理论.md "wikilink")

:\*J62 [器乐理论与](../Page/器乐.md "wikilink")[演奏法](../Page/演奏.md "wikilink")

:\*J63 [民族器乐理论与方法](../Page/民族器乐.md "wikilink")

::\*J632 [中国民族器乐](../Page/中国民族器乐.md "wikilink")

:\*J64 中国音乐作品

::\*J642 [歌曲](../Page/歌曲.md "wikilink")（综合性歌曲集）

:::\*J642.1 [群众歌曲](../Page/群众歌曲.md "wikilink")

:::\*J642.2
[民族歌曲](../Page/民族歌曲.md "wikilink")、[地方歌曲](../Page/地方歌曲.md "wikilink")

:::\*J642.3 [练声曲](../Page/练声曲.md "wikilink")

:::\*J642.4 戏剧电影歌曲

:::\*J642.5 创作歌曲

:::\*J642.6 [儿童歌曲](../Page/儿童歌曲.md "wikilink")

:::\*J642.7 [古代歌曲](../Page/古代歌曲.md "wikilink")

::\*J643 戏剧音乐、[配乐音乐曲谱](../Page/配乐.md "wikilink")

::\*J647 [器乐曲](../Page/器乐曲.md "wikilink")

::\*J648 [民族器乐曲](../Page/民族器乐曲.md "wikilink")

:\*J65 世界各国音乐作品

::\*J652 [歌曲](../Page/歌曲.md "wikilink")

  - J7　[舞蹈](../Page/舞蹈.md "wikilink")

:\*J70 [舞蹈理论](../Page/舞蹈理论.md "wikilink")

:\*J71 舞蹈技术、方法

:\*J72 中国舞蹈、[舞剧](../Page/舞剧.md "wikilink")

::\*J721 舞蹈图谱

::\*J722 [舞蹈](../Page/舞蹈.md "wikilink")

:::\*J722.1 [集体舞蹈](../Page/集体舞蹈.md "wikilink")

:::\*J722.2 [民族舞蹈](../Page/民族舞蹈.md "wikilink")

:::\*J722.3 [儿童舞蹈](../Page/儿童舞蹈.md "wikilink")

:::\*J722.5 [芭蕾舞蹈](../Page/芭蕾舞蹈.md "wikilink")

:::\*J722.8 [交际舞](../Page/交际舞.md "wikilink")

:\*J73 各国舞蹈、舞剧

:\*J79 舞蹈事业

  - J8　[戏剧艺术](../Page/戏剧.md "wikilink")

:\*J82 [中国戏剧](../Page/中国戏剧.md "wikilink")

::\*J821 [京剧艺术](../Page/京剧.md "wikilink")

:\*J83 各国戏剧艺术

  - J9 [电影](../Page/电影.md "wikilink")、[电视艺术](../Page/电视.md "wikilink")

::\*J905 电影、电视的评论、欣赏

:::\*J991.7 [世界电影评奖](../Page/世界电影评奖.md "wikilink")

:::\*J992.7 [中国电视评奖](../Page/中国电视评奖.md "wikilink")

-----

[Category:中国图书馆图书分类法](../Category/中国图书馆图书分类法.md "wikilink")