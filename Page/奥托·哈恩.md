**奥托·哈恩**（，），生於[法兰克福逝於](../Page/法兰克福.md "wikilink")[格丁根](../Page/格丁根.md "wikilink")，[德国放射化学家和物理学家](../Page/德国.md "wikilink")，曾获1944年[诺贝尔化学奖](../Page/诺贝尔化学奖.md "wikilink")。

## 主要贡献

哈恩1904年从镭盐中分离出一种新的放射性物质射钍（<sup>228</sup>[Th](../Page/钍.md "wikilink")）。以后又发现射锕（<sup>227</sup>Th）、新钍1（<sup>228</sup>[Ra](../Page/镭.md "wikilink")）、新钍2（<sup>228</sup>[Ac](../Page/锕.md "wikilink")）、铀Z（<sup>234</sup>[Pa](../Page/镤.md "wikilink")）、原锕（<sup>231</sup>Pa）和一些被称为放射性淀质的核素，为阐明天然放射系各核素间的关系起了重要作用。放射化学中常用的反冲分离法和研究固态物质结构的射气法都是哈恩提出的。他还在同晶共沉淀方面提出了哈恩定律。1917年发现放射性元素镤。1921年发现了天然放射性元素的同质异能现象。

哈恩一生中最大的贡献是1938年和[弗里茨·施特拉斯曼一起发现](../Page/弗里茨·施特拉斯曼.md "wikilink")[核裂变现象](../Page/核裂变.md "wikilink")，揭示了利用[核能的可能性](../Page/核能.md "wikilink")。[铀经中子照射后产生了一些β](../Page/铀.md "wikilink")-放射性[核素](../Page/核素.md "wikilink")，开始不少科学家认为是[超铀元素](../Page/超铀元素.md "wikilink")。哈恩和斯特拉斯曼在仔细鉴定核反应产物后，肯定其中之一是放射性[钡](../Page/钡.md "wikilink")，当时在瑞典的[莉泽·迈特纳和](../Page/莉泽·迈特纳.md "wikilink")[奥托·罗伯特·弗里施认为这是铀核被](../Page/奥托·罗伯特·弗里施.md "wikilink")[中子击中后分裂成的质量相近的碎片](../Page/中子.md "wikilink")。哈恩因为发现了[核裂变现象获得](../Page/核裂变.md "wikilink")1944年[诺贝尔化学奖](../Page/诺贝尔化学奖.md "wikilink")。他的主要著作有《应用放射化学》和《新原子》等。

## 马克斯·普朗克学会

从1948年到1960年，哈恩是新建立的促进科学进步的[马克斯·普朗克学会的创建主席](../Page/马克斯·普朗克学会.md "wikilink")。在紧接着二战之后，因为落在广岛和长崎原子弹的事件，哈恩的反应是强烈反对原子能的军事用途。

## 榮譽

月球上的[哈恩坑以他與](../Page/哈恩_\(月坑\).md "wikilink")[弗里德里希二世·格拉夫·馮·哈恩共同命名](../Page/弗里德里希二世·格拉夫·馮·哈恩.md "wikilink")，以示對他們的尊榮。

[Category:德国化学家](../Category/德国化学家.md "wikilink")
[Category:诺贝尔化学奖获得者](../Category/诺贝尔化学奖获得者.md "wikilink")
[Category:德國諾貝爾獎獲得者](../Category/德國諾貝爾獎獲得者.md "wikilink")
[Category:柏林自由大學教師](../Category/柏林自由大學教師.md "wikilink")
[Category:柏林洪堡大學校友](../Category/柏林洪堡大學校友.md "wikilink")
[Category:慕尼黑大學校友](../Category/慕尼黑大學校友.md "wikilink")
[Category:馬爾堡大學校友](../Category/馬爾堡大學校友.md "wikilink")
[Category:黑森人](../Category/黑森人.md "wikilink")
[Category:核化学家](../Category/核化学家.md "wikilink")
[Category:丹麦皇家科学院院士](../Category/丹麦皇家科学院院士.md "wikilink")
[Category:恩里科·費米獎獲獎者](../Category/恩里科·費米獎獲獎者.md "wikilink")
[Category:马克斯·普朗克学会人物](../Category/马克斯·普朗克学会人物.md "wikilink")
[Category:奧地利科學院院士](../Category/奧地利科學院院士.md "wikilink")
[Category:马克斯·普朗克奖章获得者](../Category/马克斯·普朗克奖章获得者.md "wikilink")
[Category:宗座科学院院士](../Category/宗座科学院院士.md "wikilink")