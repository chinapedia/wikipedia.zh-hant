《**宋史翼**》40卷，[清代](../Page/清代.md "wikilink")[陸心源編著](../Page/陸心源.md "wikilink")，補《[宋史](../Page/宋史.md "wikilink")》列傳之不備。

## 概要

[元修](../Page/元朝.md "wikilink")《宋史》，[度宗以前多本宋國史](../Page/宋度宗.md "wikilink")、實錄，故敘事詳於北宋而略於南宋\[1\]；[恭帝以後](../Page/宋恭帝.md "wikilink")，宋元交兵之際，記載多闕，元人又成書倉促，搜訪不周，宋末元初死事與隱逸之士未及立傳者更眾\[2\]。《宋史翼》多取文集碑傳、[方志](../Page/地方志.md "wikilink")，輔以其他史籍，增補《宋史》列傳781人、附傳64人，每篇皆注明出處。如《宋史》循吏傳僅12人，而程師孟已見列傳，實11人，且均為北宋人，《宋史翼》所補多至5卷、128人；文苑傳96人，而南宋僅11人，《宋史翼》則增102人、附傳6人。有清[光緒](../Page/光緒.md "wikilink")32年（1906年）歸安陸氏刊本，[俞樾](../Page/俞樾.md "wikilink")、[繆荃孫為序](../Page/繆荃孫.md "wikilink")。

## 總目

  - 卷一 - [張沔](../Page/張沔.md "wikilink")
    <small>子[諷](../Page/張諷.md "wikilink")</small>、[刁約](../Page/刁約.md "wikilink")、[畢從古](../Page/畢從古.md "wikilink")、[王尚恭](../Page/王尚恭.md "wikilink")、[孫抗](../Page/孫抗.md "wikilink")、[孔延之](../Page/孔延之.md "wikilink")、[吳師孟](../Page/吳師孟.md "wikilink")、[周表權](../Page/周表權.md "wikilink")、[周尹](../Page/周尹.md "wikilink")、[呂希道](../Page/呂希道.md "wikilink")、[呂希績](../Page/呂希績.md "wikilink")
  - 卷二 -
    [林積](../Page/林積.md "wikilink")、[蘇耆](../Page/蘇耆.md "wikilink")、[蘇承禧](../Page/蘇承禧.md "wikilink")、[郟亶](../Page/郟亶.md "wikilink")、[姚勔](../Page/姚勔.md "wikilink")
  - 卷三 -
    [趙彥若](../Page/趙彥若.md "wikilink")、[計用章](../Page/計用章.md "wikilink")、[謝景初](../Page/謝景初.md "wikilink")、[劉誼](../Page/劉誼.md "wikilink")、[黃隱](../Page/黃隱.md "wikilink")、[朱紱](../Page/朱紱.md "wikilink")
  - 卷四 -
    [王慎言](../Page/王慎言.md "wikilink")、[劉唐老](../Page/劉唐老.md "wikilink")、[韓治](../Page/韓治.md "wikilink")、[象求](../Page/象求.md "wikilink")、[蘇嘉](../Page/蘇嘉.md "wikilink")
    <small>弟[京](../Page/蘇京.md "wikilink")、[攜](../Page/蘇攜.md "wikilink")
    京子[師德](../Page/蘇師德.md "wikilink")
    師德子[玭](../Page/蘇玭.md "wikilink")</small>、[蘇遲](../Page/蘇遲.md "wikilink")
    <small>子[籀](../Page/蘇籀.md "wikilink")、[簡](../Page/蘇簡.md "wikilink")、[策](../Page/蘇策.md "wikilink")
    簡子[諤](../Page/蘇諤.md "wikilink")
    諤子[林](../Page/蘇林.md "wikilink")</small>、[蘇符](../Page/蘇符.md "wikilink")、[蘇峴](../Page/蘇峴.md "wikilink")
    <small>叔[籍](../Page/蘇籍.md "wikilink")</small>
  - 卷五 -
    [王古](../Page/王古.md "wikilink")、[李深](../Page/李深.md "wikilink")、[滿中行](../Page/滿中行.md "wikilink")、[陳并](../Page/陳并.md "wikilink")、[洪中孚](../Page/洪中孚.md "wikilink")、[周鍔](../Page/周鍔.md "wikilink")
  - 卷六 -
    [楊康國](../Page/楊康國.md "wikilink")、[吳安詩](../Page/吳安詩.md "wikilink")、[李新](../Page/李新.md "wikilink")
  - 卷七 -
    [王獻可](../Page/王獻可.md "wikilink")、[楊瓌寶](../Page/楊瓌寶.md "wikilink")、[翁彥國](../Page/翁彥國.md "wikilink")、[劉安節](../Page/劉安節.md "wikilink")、[劉安上](../Page/劉安上.md "wikilink")、[詹至](../Page/詹至.md "wikilink")、[王庭珪](../Page/王庭珪.md "wikilink")
    <small>[吳師古](../Page/吳師古.md "wikilink")、[陳剛中](../Page/陳剛中.md "wikilink")、[張元幹](../Page/張元幹.md "wikilink")</small>、[湯東野](../Page/湯東野.md "wikilink")
    <small>姪[喬年](../Page/湯喬年.md "wikilink")
    [范寥](../Page/范寥.md "wikilink")</small>、[余應球](../Page/余應球.md "wikilink")
  - 卷八 -
    [滕庾](../Page/滕庾.md "wikilink")、[李復](../Page/李復.md "wikilink")、[喻汝礪](../Page/喻汝礪.md "wikilink")、[王縉](../Page/王縉_\(宋朝\).md "wikilink")、[方軫](../Page/方軫.md "wikilink")、[陳朝老](../Page/陳朝老.md "wikilink")、[陳吉老](../Page/陳吉老.md "wikilink")、[劉大中](../Page/劉大中_\(宋朝\).md "wikilink")
  - 卷九 -
    [連南夫](../Page/連南夫.md "wikilink")、[方廷寔](../Page/方廷寔.md "wikilink")、[賀允中](../Page/賀允中.md "wikilink")、[蔡伸](../Page/蔡伸.md "wikilink")、[王晞亮](../Page/王晞亮.md "wikilink")、[宋昭](../Page/宋昭.md "wikilink")
  - 卷十 - [林季仲](../Page/林季仲.md "wikilink")
    <small>弟[叔豹](../Page/林叔豹.md "wikilink")</small>、[馮時行](../Page/馮時行.md "wikilink")、[王之道](../Page/王之道.md "wikilink")、[邵溥](../Page/邵溥.md "wikilink")
    <small>弟[博](../Page/邵博.md "wikilink")</small>、[呂廣問](../Page/呂廣問.md "wikilink")、[譚章](../Page/譚章.md "wikilink")、[陳正彙](../Page/陳正彙.md "wikilink")
  - 卷十一 -
    [江端友](../Page/江端友.md "wikilink")、[張宇發](../Page/張宇發.md "wikilink")、[沈長卿](../Page/沈長卿.md "wikilink")、[胡珵](../Page/胡珵.md "wikilink")、[陳剛](../Page/陳剛_\(宋朝\).md "wikilink")、[楊煒](../Page/楊煒.md "wikilink")
  - 卷十二 -
    [傅自得](../Page/傅自得.md "wikilink")、[王趯](../Page/王趯.md "wikilink")、[吳元美](../Page/吳元美.md "wikilink")、[張戒](../Page/張戒.md "wikilink")、[霍篪](../Page/霍篪.md "wikilink")、[凌景夏](../Page/凌景夏.md "wikilink")、[毛叔度](../Page/毛叔度.md "wikilink")、[周操](../Page/周操.md "wikilink")、[沈介](../Page/沈介.md "wikilink")
  - 卷十三 -
    [周麟之](../Page/周麟之.md "wikilink")、[劉度](../Page/劉度.md "wikilink")、[鄭伯熊](../Page/鄭伯熊.md "wikilink")、[苪燁](../Page/苪燁.md "wikilink")
    <small>弟[煇](../Page/苪煇.md "wikilink")</small>、[唐仲友](../Page/唐仲友.md "wikilink")、[沈复](../Page/沈复.md "wikilink")、[張郯](../Page/張郯.md "wikilink")、[王佐](../Page/王佐.md "wikilink")
  - 卷十四 -
    [沈清臣](../Page/沈清臣.md "wikilink")、[吳儆](../Page/吳儆.md "wikilink")、[韓元吉](../Page/韓元吉.md "wikilink")、[程宏圖](../Page/程宏圖.md "wikilink")、[袁說友](../Page/袁說友.md "wikilink")、[蔡戡](../Page/蔡戡.md "wikilink")、[王柟](../Page/王柟.md "wikilink")、[黃由](../Page/黃由.md "wikilink")、[汪義和](../Page/汪義和.md "wikilink")、[周必正](../Page/周必正.md "wikilink")
  - 卷十五 -
    [衛涇](../Page/衛涇.md "wikilink")、[劉靖之](../Page/劉靖之.md "wikilink")、[梁季珌](../Page/梁季珌.md "wikilink")、[吳琚](../Page/吳琚.md "wikilink")、[徐瑄](../Page/徐瑄.md "wikilink")、[余古](../Page/余古.md "wikilink")、[趙綸](../Page/趙綸.md "wikilink")、[趙昱](../Page/趙昱.md "wikilink")、[丁伯桂](../Page/丁伯桂.md "wikilink")、[方大琮](../Page/方大琮.md "wikilink")
  - 卷十六 -
    [劉磊卿](../Page/劉磊卿.md "wikilink")、[胡夢昱](../Page/胡夢昱.md "wikilink")、[李昴英](../Page/李昴英.md "wikilink")
    <small>子[志道](../Page/李志道.md "wikilink")</small>、[虞剛簡](../Page/虞剛簡.md "wikilink")、[周端朝](../Page/周端朝.md "wikilink")、[焦炳炎](../Page/焦炳炎.md "wikilink")、[王應鳳](../Page/王應鳳.md "wikilink")
  - 卷十七 -
    [方逢辰](../Page/方逢辰.md "wikilink")、[鄭起](../Page/鄭起.md "wikilink")、[方岳](../Page/方岳.md "wikilink")、[盧鉞](../Page/盧鉞.md "wikilink")、[家大酉](../Page/家大酉.md "wikilink")、[洪天驥](../Page/洪天驥.md "wikilink")、[趙順孫](../Page/趙順孫.md "wikilink")、[趙必𤩪](../Page/趙必𤩪.md "wikilink")、[丁黼](../Page/丁黼.md "wikilink")
  - 卷十八‧循吏一 -
    [胡令儀](../Page/胡令儀.md "wikilink")、[西門成允](../Page/西門成允.md "wikilink")、[蔡黃裳](../Page/蔡黃裳.md "wikilink")、[陳耿](../Page/陳耿.md "wikilink")、[王益](../Page/王益.md "wikilink")、[王絲](../Page/王絲.md "wikilink")、[歐陽頴](../Page/歐陽頴.md "wikilink")、[張式](../Page/張式.md "wikilink")、[葉湜](../Page/葉湜.md "wikilink")、[王平](../Page/王平.md "wikilink")、[李夷庚](../Page/李夷庚.md "wikilink")、[梁蒨](../Page/梁蒨.md "wikilink")、[趙諴](../Page/趙諴.md "wikilink")、[寇平](../Page/寇平.md "wikilink")、[范仲溫](../Page/范仲溫.md "wikilink")、[沈衡](../Page/沈衡.md "wikilink")、[閻充國](../Page/閻充國.md "wikilink")、[石牧之](../Page/石牧之.md "wikilink")、[黃照](../Page/黃照.md "wikilink")、[李彤](../Page/李彤.md "wikilink")、[常琪](../Page/常琪.md "wikilink")、[蔡天球](../Page/蔡天球.md "wikilink")、[蕭固](../Page/蕭固.md "wikilink")、[崇大年](../Page/崇大年.md "wikilink")
  - 卷十九‧循吏二 -
    [侍其瑋](../Page/侍其瑋.md "wikilink")、[黃莘](../Page/黃莘.md "wikilink")
    <small>從子[陞](../Page/黃陞.md "wikilink")</small>、[陳郛](../Page/陳郛.md "wikilink")、[皇甫鑑](../Page/皇甫鑑.md "wikilink")、[韓正彥](../Page/韓正彥.md "wikilink")、[司馬京](../Page/司馬京.md "wikilink")、[費琦](../Page/費琦.md "wikilink")、[陸琮](../Page/陸琮.md "wikilink")、[孫載](../Page/孫載.md "wikilink")、[王默](../Page/王默.md "wikilink")、[家定國](../Page/家定國.md "wikilink")、[李撰](../Page/李撰.md "wikilink")、[陳廓](../Page/陳廓.md "wikilink")、[黃彥](../Page/黃彥.md "wikilink")、[鄭景平](../Page/鄭景平.md "wikilink")、[段縫](../Page/段縫.md "wikilink")、[蔡奕](../Page/蔡奕.md "wikilink")、[郭子皋](../Page/郭子皋.md "wikilink")、[吳革](../Page/吳革.md "wikilink")、[李傑](../Page/李傑.md "wikilink")、[潘鯁](../Page/潘鯁.md "wikilink")、[吳思](../Page/吳思.md "wikilink")、[吳與](../Page/吳與.md "wikilink")、[楊存](../Page/楊存.md "wikilink")、[郭大昕](../Page/郭大昕.md "wikilink")、[蔣圓](../Page/蔣圓.md "wikilink")
  - 卷二十‧循吏三 -
    [任宗誼](../Page/任宗誼.md "wikilink")、[汪愷](../Page/汪愷.md "wikilink")、[王汝舟](../Page/王汝舟.md "wikilink")、[程邁](../Page/程邁.md "wikilink")、[韓復](../Page/韓復.md "wikilink")、[高元常](../Page/高元常.md "wikilink")、[李鞉](../Page/李鞉.md "wikilink")、[王復](../Page/王復.md "wikilink")、[毛友](../Page/毛友.md "wikilink")、[黃子游](../Page/黃子游.md "wikilink")、[喬大臨](../Page/喬大臨.md "wikilink")、[章元振](../Page/章元振.md "wikilink")、[孫時升](../Page/孫時升.md "wikilink")、[張琯](../Page/張琯.md "wikilink")、[黃徹](../Page/黃徹.md "wikilink")、[滕膺](../Page/滕膺.md "wikilink")、[凌哲](../Page/凌哲.md "wikilink")、[夏穎達](../Page/夏穎達.md "wikilink")、[向伯奮](../Page/向伯奮.md "wikilink")、[富元衡](../Page/富元衡.md "wikilink")、[趙子嶙](../Page/趙子嶙.md "wikilink")、[羅棐恭](../Page/羅棐恭.md "wikilink")、[張震](../Page/張震_\(宋朝\).md "wikilink")、[樓璹](../Page/樓璹.md "wikilink")、[路彬](../Page/路彬.md "wikilink")、[潘疇](../Page/潘疇.md "wikilink")、[方擴](../Page/方擴.md "wikilink")
  - 卷二十一‧循吏四 -
    [黃齊](../Page/黃齊.md "wikilink")、[李守柔](../Page/李守柔.md "wikilink")、[樊光遠](../Page/樊光遠.md "wikilink")、[方翥](../Page/方翥.md "wikilink")、[張敦頤](../Page/張敦頤.md "wikilink")、[張維](../Page/張維.md "wikilink")、[何耕](../Page/何耕.md "wikilink")、[趙善佐](../Page/趙善佐.md "wikilink")、[宇文師獻](../Page/宇文師獻.md "wikilink")、[鄧深](../Page/鄧深.md "wikilink")、[沈度](../Page/沈度.md "wikilink")、[家炎](../Page/家炎.md "wikilink")、[趙善待](../Page/趙善待.md "wikilink")、[楊方](../Page/楊方.md "wikilink")、[方崧卿](../Page/方崧卿.md "wikilink")、[俞亨宗](../Page/俞亨宗.md "wikilink")、[朱晞顏](../Page/朱晞顏.md "wikilink")、[陳琦](../Page/陳琦.md "wikilink")、[黃洧](../Page/黃洧.md "wikilink")、[蒲堯仁](../Page/蒲堯仁.md "wikilink")、[張奭](../Page/張奭.md "wikilink")、[李訦](../Page/李訦.md "wikilink")、[宋煜](../Page/宋煜.md "wikilink")
    <small>孫[翊](../Page/宋翊.md "wikilink")</small>、[傅大聲](../Page/傅大聲.md "wikilink")
  - 卷二十二‧循吏五 -
    [郭份](../Page/郭份.md "wikilink")、[黃犖](../Page/黃犖.md "wikilink")、[李大訓](../Page/李大訓.md "wikilink")、[吳炎](../Page/吳炎.md "wikilink")、[曾噩](../Page/曾噩.md "wikilink")、[汪大定](../Page/汪大定.md "wikilink")、[桂萬榮](../Page/桂萬榮.md "wikilink")、[幸元龍](../Page/幸元龍.md "wikilink")、[張方](../Page/張方.md "wikilink")、[麋溧](../Page/麋溧.md "wikilink")
    <small>子[弇](../Page/麋弇.md "wikilink")</small>、[羅博文](../Page/羅博文.md "wikilink")、[陶崇](../Page/陶崇.md "wikilink")、[張汝明](../Page/張汝明.md "wikilink")、[曾治鳳](../Page/曾治鳳.md "wikilink")、[吳懿德](../Page/吳懿德.md "wikilink")、[楊長孺](../Page/楊長孺.md "wikilink")、[宋慈](../Page/宋慈.md "wikilink")、[李義山](../Page/李義山.md "wikilink")、[樓大年](../Page/樓大年.md "wikilink")、[陳景魏](../Page/陳景魏.md "wikilink")、[吳一鳴](../Page/吳一鳴.md "wikilink")、[木天駿](../Page/木天駿.md "wikilink")、[陳琥](../Page/陳琥.md "wikilink")、[謝子強](../Page/謝子強.md "wikilink")、[陳介](../Page/陳介.md "wikilink")、[孫嶸叟](../Page/孫嶸叟.md "wikilink")、[李申巽](../Page/李申巽.md "wikilink")
  - 卷二十三‧儒林一 -
    [宋咸](../Page/宋咸.md "wikilink")、[阮逸](../Page/阮逸.md "wikilink")、[士建中](../Page/士建中.md "wikilink")、[黎淳](../Page/黎淳.md "wikilink")、[劉顏](../Page/劉顏.md "wikilink")、[王開祖](../Page/王開祖.md "wikilink")、[劉牧](../Page/劉牧.md "wikilink")、[倪天隱](../Page/倪天隱.md "wikilink")、[單鍔](../Page/單鍔.md "wikilink")
    <small>兄[錫](../Page/單錫.md "wikilink")</small>、[吳攷](../Page/吳攷.md "wikilink")、[朱臨](../Page/朱臨.md "wikilink")、[范柔中](../Page/范柔中.md "wikilink")、[周行己](../Page/周行己.md "wikilink")、[蕭楚](../Page/蕭楚.md "wikilink")、[馬大年](../Page/馬大年.md "wikilink")、[何兌](../Page/何兌.md "wikilink")、[王普](../Page/王普.md "wikilink")、[曹粹中](../Page/曹粹中.md "wikilink")、[彪虎臣](../Page/彪虎臣.md "wikilink")、[陳長方](../Page/陳長方.md "wikilink")、[李樗](../Page/李樗.md "wikilink")
  - 卷二十四‧儒林二 -
    [黎明](../Page/黎明_\(宋朝\).md "wikilink")、[傅寅](../Page/傅寅.md "wikilink")、[李郁](../Page/李郁.md "wikilink")、[王蘋](../Page/王蘋.md "wikilink")、[周爭先](../Page/周爭先.md "wikilink")、[關注](../Page/關注.md "wikilink")、[黃公度](../Page/黃公度.md "wikilink")、[吳棫](../Page/吳棫.md "wikilink")、[陳鵬飛](../Page/陳鵬飛.md "wikilink")、[石墪](../Page/石墪.md "wikilink")、[鄭汝諧](../Page/鄭汝諧.md "wikilink")、[劉夙](../Page/劉夙.md "wikilink")
    <small>弟[朔](../Page/劉朔.md "wikilink")</small>、[范浚](../Page/范浚.md "wikilink")、[方慤](../Page/方慤.md "wikilink")、[閭邱昕](../Page/閭邱昕.md "wikilink")、[林湜](../Page/林湜.md "wikilink")、[沈度](../Page/沈度.md "wikilink")、[汪逵](../Page/汪逵.md "wikilink")、[王炎](../Page/王炎.md "wikilink")、[趙師淵](../Page/趙師淵.md "wikilink")、[施德操](../Page/施德操.md "wikilink")
    <small>[楊子平附](../Page/楊子平.md "wikilink")</small>、[劉炳](../Page/劉炳.md "wikilink")
  - 卷二十五‧儒林三 -
    [楊子謨](../Page/楊子謨.md "wikilink")、[時瀾](../Page/時瀾.md "wikilink")
    <small>子[少章附](../Page/時少章.md "wikilink")</small>、[黃𤪠](../Page/黃𤪠.md "wikilink")、[范仲黼](../Page/范仲黼.md "wikilink")、[朱黼](../Page/朱黼.md "wikilink")、[陳文蔚](../Page/陳文蔚.md "wikilink")、[車若水](../Page/車若水.md "wikilink")、[滕璘](../Page/滕璘.md "wikilink")、[李繪](../Page/李繪.md "wikilink")、[楊復](../Page/楊復.md "wikilink")、[董鼎](../Page/董鼎.md "wikilink")、[余𦬊孫](../Page/余𦬊孫.md "wikilink")、[陳埴](../Page/陳埴.md "wikilink")、[張淳](../Page/張淳.md "wikilink")、[李𡌴](../Page/李𡌴.md "wikilink")、[度正](../Page/度正.md "wikilink")、[胡方平](../Page/胡方平.md "wikilink")、[游九言](../Page/游九言.md "wikilink")
    <small>弟[九功](../Page/游九功.md "wikilink")</small>、[饒魯](../Page/饒魯.md "wikilink")、[輔廣](../Page/輔廣.md "wikilink")、[衛湜](../Page/衛湜.md "wikilink")、[程永奇](../Page/程永奇.md "wikilink")、[陳著](../Page/陳著.md "wikilink")、[董楷](../Page/董楷.md "wikilink")、[黃仲元](../Page/黃仲元.md "wikilink")
  - 卷二十六‧文苑一 -
    [張景](../Page/張景.md "wikilink")、李畋、[張先](../Page/張先.md "wikilink")、[齊唐](../Page/齊唐.md "wikilink")、[邵餗](../Page/邵餗.md "wikilink")、[強至](../Page/強至.md "wikilink")、[韋驤](../Page/韋驤.md "wikilink")、[章友直](../Page/章友直.md "wikilink")、[晁仲衍](../Page/晁仲衍.md "wikilink")、[楊景略](../Page/楊景略.md "wikilink")、[鄧忠臣](../Page/鄧忠臣.md "wikilink")、[華鎮](../Page/華鎮.md "wikilink")、[謝逸](../Page/謝逸.md "wikilink")
    <small>弟[薖](../Page/謝薖.md "wikilink")</small>、[何去非](../Page/何去非.md "wikilink")、[黃裳](../Page/黃裳.md "wikilink")、[李彭](../Page/李彭.md "wikilink")、[馮正符](../Page/馮正符.md "wikilink")、[王合](../Page/王合.md "wikilink")、[袁百之](../Page/袁百之.md "wikilink")、[馬存](../Page/馬存.md "wikilink")、[林虑](../Page/林虑.md "wikilink")、[商倚](../Page/商倚.md "wikilink")、[曾紆](../Page/曾紆.md "wikilink")、[黃策](../Page/黃策.md "wikilink")、[王鞏](../Page/王鞏.md "wikilink")
  - 卷二十七‧文苑二 -
    [洪朋](../Page/洪朋.md "wikilink")、[洪炎](../Page/洪炎.md "wikilink")、[高茂華](../Page/高茂華.md "wikilink")、[雍孝聞](../Page/雍孝聞.md "wikilink")、[慕容彥達](../Page/慕容彥達.md "wikilink")
    <small>孫[綸](../Page/慕容綸.md "wikilink")</small>、[于世封](../Page/于世封.md "wikilink")
    <small>正封</small>、[毛淓](../Page/毛淓.md "wikilink")、[林宋卿](../Page/林宋卿.md "wikilink")、[葉庭珪](../Page/葉庭珪.md "wikilink")、[朱翌](../Page/朱翌.md "wikilink")、[傅崧卿](../Page/傅崧卿.md "wikilink")、[徐兢](../Page/徐兢.md "wikilink")、[王洋](../Page/王洋_\(宋朝\).md "wikilink")、[董逌](../Page/董逌.md "wikilink")
    <small>子[棻](../Page/董棻.md "wikilink")</small>、[黃次山](../Page/黃次山.md "wikilink")、[王昇](../Page/王昇.md "wikilink")、[任申先](../Page/任申先.md "wikilink")、[康與之](../Page/康與之.md "wikilink")、[鄭厚](../Page/鄭厚.md "wikilink")、[王銍](../Page/王銍.md "wikilink")、[畢良史](../Page/畢良史.md "wikilink")、[周紫芝](../Page/周紫芝.md "wikilink")
  - 卷二十八‧文苑三 -
    [姚寬](../Page/姚寬.md "wikilink")、[任盡言](../Page/任盡言.md "wikilink")、[張行成](../Page/張行成.md "wikilink")、[仲并](../Page/仲并.md "wikilink")、[孫松壽](../Page/孫松壽.md "wikilink")、[李石](../Page/李石.md "wikilink")、[蕭德藻](../Page/蕭德藻.md "wikilink")、[施元之](../Page/施元之.md "wikilink")、[崔敦詩](../Page/崔敦詩.md "wikilink")
    <small>兄[敦禮附](../Page/崔敦禮_\(宋朝\).md "wikilink")</small>、[吳說](../Page/吳說.md "wikilink")、[翟耆年](../Page/翟耆年.md "wikilink")、[姜夔](../Page/姜夔.md "wikilink")、[袁文](../Page/袁文.md "wikilink")、[張有](../Page/張有.md "wikilink")、[喻良弼](../Page/喻良弼.md "wikilink")、[趙師秀](../Page/趙師秀.md "wikilink")、[徐照](../Page/徐照.md "wikilink")、翁卷、[徐璣](../Page/徐璣.md "wikilink")、[王厚之](../Page/王厚之.md "wikilink")、[周孚](../Page/周孚.md "wikilink")、[王卿月](../Page/王卿月.md "wikilink")、[曾丰](../Page/曾丰.md "wikilink")、[鞏豐](../Page/鞏豐.md "wikilink")
    <small>弟[嶸](../Page/鞏嶸.md "wikilink")</small>
  - 卷二十九‧文苑四 -
    [孫應時](../Page/孫應時.md "wikilink")、[高似孫](../Page/高似孫.md "wikilink")、[王偁](../Page/王偁.md "wikilink")、[陳造](../Page/陳造.md "wikilink")、[吳曾](../Page/吳曾.md "wikilink")、[倪朴](../Page/倪朴.md "wikilink")、[吳仁傑](../Page/吳仁傑.md "wikilink")、[劉過](../Page/劉過.md "wikilink")、[徐次鐸](../Page/徐次鐸.md "wikilink")、[施宿](../Page/施宿.md "wikilink")、[喻保](../Page/喻保.md "wikilink")、[敖陶孫](../Page/敖陶孫.md "wikilink")、[留元剛](../Page/留元剛.md "wikilink")、[李劉](../Page/李劉.md "wikilink")、[王明清](../Page/王明清.md "wikilink")、[章樵](../Page/章樵.md "wikilink")、[陳耆卿](../Page/陳耆卿.md "wikilink")、[吳子良](../Page/吳子良.md "wikilink")、[戴復古](../Page/戴復古.md "wikilink")、[左緯](../Page/左緯.md "wikilink")、[劉克莊](../Page/劉克莊.md "wikilink")、[陳振孫](../Page/陳振孫.md "wikilink")、[趙孟堅](../Page/趙孟堅.md "wikilink")、[陳均](../Page/陳均.md "wikilink")、[張端義](../Page/張端義.md "wikilink")、[陳容](../Page/陳容.md "wikilink")、[薛據](../Page/薛據.md "wikilink")、[姚勉](../Page/姚勉.md "wikilink")、[羅泌](../Page/羅泌.md "wikilink")、[鮑雲龍](../Page/鮑雲龍.md "wikilink")、[舒嶽祥](../Page/舒嶽祥.md "wikilink")
  - 卷三十‧忠義一 -
    [石待舉](../Page/石待舉.md "wikilink")、[李英](../Page/李英.md "wikilink")、[唐子正](../Page/唐子正.md "wikilink")、[葉居申](../Page/葉居申.md "wikilink")、[毛奎](../Page/毛奎.md "wikilink")、[王履](../Page/王履.md "wikilink")、[李階](../Page/李階.md "wikilink")、周隨亨、[潘中](../Page/潘中_\(宋朝\).md "wikilink")、[夏承](../Page/夏承.md "wikilink")、[王霽](../Page/王霽.md "wikilink")、[史徽](../Page/史徽.md "wikilink")、[劉滂](../Page/劉滂.md "wikilink")、[余光庭](../Page/余光庭.md "wikilink")、[熊安上](../Page/熊安上.md "wikilink")、[鄭柟](../Page/鄭柟.md "wikilink")、[林子立](../Page/林子立.md "wikilink")、[羅復](../Page/羅復.md "wikilink")、[鄭中立](../Page/鄭中立.md "wikilink")、[翁開](../Page/翁開.md "wikilink")、[詹友端](../Page/詹友端.md "wikilink")、[黃璘](../Page/黃璘.md "wikilink")、[包汝諧](../Page/包汝諧.md "wikilink")、[曹夬](../Page/曹夬.md "wikilink")、[董公健](../Page/董公健.md "wikilink")、[江汝度](../Page/江汝度.md "wikilink")、[葉鑛](../Page/葉鑛.md "wikilink")、[詹良臣](../Page/詹良臣.md "wikilink")、[毛𣓨](../Page/毛𣓨.md "wikilink")、[張理](../Page/張理.md "wikilink")、周承已
    <small>婁淵
    潘守真</small>、[王行之](../Page/王行之.md "wikilink")、鮑琢、[程全](../Page/程全.md "wikilink")、[盧榕](../Page/盧榕.md "wikilink")
    <small>子[沂](../Page/盧沂.md "wikilink")</small>、[陳自仁](../Page/陳自仁.md "wikilink")、[張撝](../Page/張撝.md "wikilink")、[葉顗](../Page/葉顗.md "wikilink")、[李琪](../Page/李琪.md "wikilink")、[林師益](../Page/林師益.md "wikilink")、呂由誠、危翁一、[魏孝友](../Page/魏孝友.md "wikilink")、[晏溥](../Page/晏溥.md "wikilink")、[翁廷慶](../Page/翁廷慶.md "wikilink")、[劉位](../Page/劉位.md "wikilink")、[蔡楙](../Page/蔡楙.md "wikilink")、王寵、陳士尹、盧以中、陳迪、於琳、[閻勍](../Page/閻勍.md "wikilink")、王相如、馮安國、程端中、王佐才、豐治、[上官悟](../Page/上官悟.md "wikilink")、盛修已、沈攸、方致堯、陳德、李喆
  - 卷三十一‧忠義二 -
    [王玠](../Page/王玠.md "wikilink")、[張憲](../Page/張憲.md "wikilink")、[邱祈福](../Page/邱祈福.md "wikilink")、[陳希造](../Page/陳希造.md "wikilink")、[蔡青福](../Page/蔡青福.md "wikilink")、[劉頜](../Page/劉頜.md "wikilink")、[陳適](../Page/陳適.md "wikilink")、楊世永、梁楚、[虞輔國](../Page/虞輔國.md "wikilink")
    <small>李憲</small>、[詹世勛](../Page/詹世勛.md "wikilink")、趙良坡、[毛士毅](../Page/毛士毅.md "wikilink")、楊九鼎、黃樞、江應淇、[鄭勳](../Page/鄭勳.md "wikilink")、應純之、[昔橫](../Page/昔橫.md "wikilink")、[唐璟](../Page/唐璟.md "wikilink")、程光庭、梁滿、[秦綱](../Page/秦綱.md "wikilink")、陳旦福、[戴衍](../Page/戴衍.md "wikilink")、黃從龍、王𧻓、曹孝先、徐干能、黃復、廖居仕、吳從龍、劉純、朱浚、顏公衮、[高談](../Page/高談.md "wikilink")、張愁、[卓得慶](../Page/卓得慶.md "wikilink")、吳駿發、徐斌、徐梅龜、徐夢發、[劉仕龍](../Page/劉仕龍.md "wikilink")、[鮑廉](../Page/鮑廉.md "wikilink")、曾如驥、[陳夢立](../Page/陳夢立.md "wikilink")、吳洪德
    <small>弟雄飛</small>、樓斌、程洙、徐宗仁、趙良坦、楊夢斗、陳壽孫、陳龍復、[蔡蒙吉](../Page/蔡蒙吉.md "wikilink")、陳士奠、卓子信、曾逢龍、高應松、曾塤、豐存芳、胡德廣、韓觀國、劉揚祖、林逢龍、陳虞之、章如旦、邱必明、何新之
  - 卷三十二‧忠義三 -
    [林景曦](../Page/林景曦.md "wikilink")、[袁鏞](../Page/袁鏞.md "wikilink")、胡廷桂、夏師堯、張漢英、王子翼、唐泰嶽、周真、許伯繼、章堉、[劉源](../Page/劉源_\(宋朝\).md "wikilink")、王貴行、周偉德、楊義忠、[林澤](../Page/林澤.md "wikilink")、吳楚材、李成大、[李梓發](../Page/李梓發.md "wikilink")、何時、李天勇、趙時踐、顏希孔、李長庚、吳世鳴、鄭宋翁、葉儞祖、[黃俊](../Page/黃俊_\(宋朝\).md "wikilink")、吳寶信、劉觡、周宣、林琦福、程楚翁、江友直、張履翁
    <small>顏斯理</small>、吳觀
    <small>陳非熊</small>、方淇、王小觀、鮑叔廉、唐元章、毛附鳳、張炎、夏椅、[陳瓚](../Page/陳瓚.md "wikilink")、[熊飛](../Page/熊飛.md "wikilink")、傅高、劉伯文、孫璹、[馬南寶](../Page/馬南寶.md "wikilink")、胡敬方、黃介、謝徽明、陳琚、羅開禮、錢淵龍、吳希奭、胡文可、周來奕、廖明哲、趙必煜、趙必𩐨、陳尉德、蔡振先、趙與珞、謝明
    <small>謝富、冉安、黃之傑</small>、伍隆起、雷龍濟、陳堯則、王德欽、彭九萬、柳敘、蘇十萬、王用龍
  - 卷三十三‧孝義 -
    [李平](../Page/李平_\(宋朝\).md "wikilink")、[郭琮](../Page/郭琮.md "wikilink")、[許逈](../Page/許逈.md "wikilink")
    <small>姪[俞](../Page/許俞.md "wikilink")</small>、朱道誠 <small>子冕、衮
    冕子浩</small>、[西門楫](../Page/西門楫.md "wikilink")、[郭長孺](../Page/郭長孺.md "wikilink")、[趙彥霄](../Page/趙彥霄.md "wikilink")、[黃汝楫](../Page/黃汝楫.md "wikilink")、樓蘊、唐傑東、趙善應、戴松、李仲發、[翁蒙之](../Page/翁蒙之.md "wikilink")、[楊文修](../Page/楊文修.md "wikilink")、[張宗孚](../Page/張宗孚.md "wikilink")、龍震翁、喻南強、王公衮、黃國華
  - 卷三十四‧遺獻一 -
    朱元昇、[許月卿](../Page/許月卿.md "wikilink")、[胡三省](../Page/胡三省.md "wikilink")、[唐珏](../Page/唐珏.md "wikilink")、柴望
    <small>從弟隨亨、元亨、元彪、蒙亨附</small>、[汪晫](../Page/汪晫.md "wikilink")
    <small>孫[夢斗](../Page/汪夢斗.md "wikilink")</small>、劉應登、[王幼孫](../Page/王幼孫.md "wikilink")、[錢選](../Page/錢選.md "wikilink")、[牟巘](../Page/牟巘.md "wikilink")、[陳存](../Page/陳存.md "wikilink")、[周密](../Page/周密.md "wikilink")、文及翁、[胡一桂](../Page/胡一桂.md "wikilink")、[何夢桂](../Page/何夢桂.md "wikilink")、方逢振、[趙若恢](../Page/趙若恢.md "wikilink")、[孫潼發](../Page/孫潼發.md "wikilink")、蔡逢甲、謝國光、陸霆龍、[熊禾](../Page/熊禾.md "wikilink")、文天禎、[史蒙卿](../Page/史蒙卿.md "wikilink")、[吳思齊](../Page/吳思齊.md "wikilink")、[黃丙炎](../Page/黃丙炎.md "wikilink")、[鄭思肖](../Page/鄭思肖.md "wikilink")、[鄧光薦](../Page/鄧光薦.md "wikilink")
  - 卷三十五‧遺獻二 -
    [謝翺](../Page/謝翺.md "wikilink")、劉辰翁、王奕、胡次焱、[孟文龍](../Page/孟文龍.md "wikilink")、張慶之、[俞琰](../Page/俞琰.md "wikilink")、[汪元量](../Page/汪元量.md "wikilink")、徐天祐、徐欽、曾子良、龔開、龍泌、周垕、程時登、曹應符
    <small>曹光遠附</small>、[吳錫疇](../Page/吳錫疇.md "wikilink")、壺𡸅、周樸、陸釗、劉友益、張千載、羅公升、皇甫明子、衛富益、[方鳳](../Page/方鳳.md "wikilink")、胡大壯、汪忠臣、[汪庭桂](../Page/汪庭桂.md "wikilink")、[汪炎㫤](../Page/汪炎㫤.md "wikilink")、江愷、孫嵩、滕塛、陳深
    <small>子植</small>、于石、趙德、莊肅、[殷澄](../Page/殷澄.md "wikilink")
  - 卷三十六‧隱逸 -
    [郭震](../Page/郭震.md "wikilink")、[趙宗萬](../Page/趙宗萬.md "wikilink")、[吳復古](../Page/吳復古.md "wikilink")、邵炳、林巽、杜子野、[楊適](../Page/楊適.md "wikilink")、[杜醇](../Page/杜醇.md "wikilink")、王致、王伯起、孔旻、[魏閑](../Page/魏閑.md "wikilink")、王鴻、崔唐臣、管師復、鍾棐、崔閑、黎子雲、廖邃明、王公輔、賈收、張維、蔣湋、陳輔、[馮貫道](../Page/馮貫道.md "wikilink")、[褚承亮](../Page/褚承亮.md "wikilink")、饒子儀、吳沆、李逈、張志行、趙占龜、林彖、楊無咎、程先、程鼎、[龔明之](../Page/龔明之.md "wikilink")、曾季貍、劉迂、[陸維之](../Page/陸維之.md "wikilink")、[胡仔](../Page/胡仔.md "wikilink")、[方勺](../Page/方勺.md "wikilink")、[彭興宗](../Page/彭興宗.md "wikilink")、[傅子雲](../Page/傅子雲.md "wikilink")、黎道華、[滕宬](../Page/滕宬.md "wikilink")、林憲、[汪莘](../Page/汪莘.md "wikilink")、孫惟信、林大有、李龏、方暹、危固、許棐、常詵孫、常棠、陸能仁、陸正
  - 卷三十七‧方技一 -
    [費孝先](../Page/費孝先.md "wikilink")、周文矩、[趙昌](../Page/趙昌.md "wikilink")、[徐熙](../Page/徐熙.md "wikilink")
    <small>孫崇嗣、崇勳、崇矩附</small>、唐希雅、李廷珪、[李成](../Page/李成.md "wikilink")、[范寬](../Page/范寬.md "wikilink")、石恪、[燕文貴](../Page/燕文貴.md "wikilink")、僧[巨然](../Page/巨然.md "wikilink")、許希善、僧[奉真](../Page/奉真.md "wikilink")、杜任、吳景鸞、郝允、[孫兆](../Page/孫兆.md "wikilink")、[徐守信](../Page/徐守信.md "wikilink")、[程惟象](../Page/程惟象.md "wikilink")、[易元吉](../Page/易元吉.md "wikilink")、[衛朴](../Page/衛朴.md "wikilink")、[崔白](../Page/崔白.md "wikilink")
    <small>弟[慤](../Page/孫慤.md "wikilink")</small>、潘谷、諸葛高、[王詵](../Page/王詵.md "wikilink")、[劉寀](../Page/劉寀.md "wikilink")
  - 卷三十八‧方技二 -
    [朱肱](../Page/朱肱.md "wikilink")、[唐慎微](../Page/唐慎微.md "wikilink")、張濟、趙令穰、[郭熙](../Page/郭熙.md "wikilink")、許道寧、周順、[士衮](../Page/士衮.md "wikilink")、[張擴](../Page/張擴.md "wikilink")、[董元](../Page/董元.md "wikilink")、李仲寧、[史堪](../Page/史堪.md "wikilink")、[張銳](../Page/張銳.md "wikilink")、[宋道方](../Page/宋道方.md "wikilink")、[王況](../Page/王況.md "wikilink")、[何澄](../Page/何澄.md "wikilink")、[謝石](../Page/謝石_\(宋朝\).md "wikilink")、[楊介](../Page/楊介.md "wikilink")、廖瑀、鄒寬、傅伯通、[許叔微](../Page/許叔微.md "wikilink")、馬和之、[潘璟](../Page/潘璟.md "wikilink")、劉松年、[馬遠](../Page/馬遠.md "wikilink")、[夏珪](../Page/夏珪.md "wikilink")、李嵩
    <small>孫章</small>、葉子仁、[史崧](../Page/史崧.md "wikilink")、陸曮、李明甫、卜則巍、楊賁亨
  - 卷三十九‧宦者 -
    [岑宗旦](../Page/岑宗旦.md "wikilink")、[樂士宣](../Page/樂士宣.md "wikilink")、[李正臣](../Page/李正臣.md "wikilink")、[李仲宣](../Page/李仲宣.md "wikilink")、[楊日言](../Page/楊日言.md "wikilink")、[劉瑗](../Page/劉瑗.md "wikilink")、[梁揆](../Page/梁揆.md "wikilink")、[羅存](../Page/羅存.md "wikilink")、[馮覲](../Page/馮覲.md "wikilink")、[賈祥](../Page/賈祥.md "wikilink")、[梁惟簡](../Page/梁惟簡.md "wikilink")、[張士良](../Page/張士良.md "wikilink")、[裴彥臣](../Page/裴彥臣.md "wikilink")、[蔡克明](../Page/蔡克明.md "wikilink")
  - 卷四十‧姦臣 - [林特](../Page/林特_\(宋朝\).md "wikilink")
    <small>子[濰](../Page/林濰.md "wikilink")、[洙](../Page/林洙.md "wikilink")</small>、[林顏](../Page/林顏.md "wikilink")、[呂升卿](../Page/呂升卿.md "wikilink")
    <small>弟[溫卿](../Page/呂溫卿.md "wikilink")</small>、[練亨甫](../Page/練亨甫.md "wikilink")、[錢遹](../Page/錢遹.md "wikilink")、[林自](../Page/林自.md "wikilink")、[方天若](../Page/方天若.md "wikilink")、[蔡碩](../Page/蔡碩.md "wikilink")、[蔡懋](../Page/蔡懋.md "wikilink")、[蔡鞗](../Page/蔡鞗.md "wikilink")、[蔡絛](../Page/蔡絛.md "wikilink")、[蔡崈](../Page/蔡崈.md "wikilink")、[黃唐傳](../Page/黃唐傳.md "wikilink")、[章傑](../Page/章傑_\(宋朝\).md "wikilink")、[吳幵](../Page/吳幵.md "wikilink")、[黎確](../Page/黎確.md "wikilink")、[呂源](../Page/呂源.md "wikilink")、[朱宗](../Page/朱宗.md "wikilink")、[葉宗諤](../Page/葉宗諤.md "wikilink")、[葉份](../Page/葉份.md "wikilink")、[黃潛厚](../Page/黃潛厚.md "wikilink")、[李文會](../Page/李文會.md "wikilink")、[林一飛](../Page/林一飛.md "wikilink")、[傅伯壽](../Page/傅伯壽.md "wikilink")、[林行可](../Page/林行可.md "wikilink")、[廖瑩中](../Page/廖瑩中.md "wikilink")

## 書誌情報

  - 《宋史翼》（中華書局、1991年） ISBN 7-101-00806-2　　影印清光緒32年陸氏初刊本，附曾貽芬編人名索引。

## 腳注

[Category:史部傳記類](../Category/史部傳記類.md "wikilink")
[Category:清朝史書](../Category/清朝史書.md "wikilink")

1.  [蘇天爵](../Page/蘇天爵.md "wikilink")《滋溪文稿》卷25·三史質疑：「如宋中興四朝史，諸傳尤少，蓋當理宗初年，諸公猶多在世故也。」
2.  參考[趙翼](../Page/趙翼.md "wikilink")《[二十二史劄記](../Page/二十二史劄記.md "wikilink")》卷23‧宋史多國史原本、卷24‧宋史列傳又有遺漏者、卷26‧宋史缺傳條。