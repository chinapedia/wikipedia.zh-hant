[Sig07-009.jpg](https://zh.wikipedia.org/wiki/File:Sig07-009.jpg "fig:Sig07-009.jpg")、[史匹哲太空望遠鏡和](../Page/史匹哲太空望遠鏡.md "wikilink")[星系演化探測器太空望遠鏡的資料合成的](../Page/星系演化探測器.md "wikilink")[M81影像](../Page/M81.md "wikilink")。\]\]
[spiral_galaxy_arms_diagram.svg](https://zh.wikipedia.org/wiki/File:spiral_galaxy_arms_diagram.svg "fig:spiral_galaxy_arms_diagram.svg")
**密度波理論**或是**林-徐密度波理論**是[林家翹和](../Page/林家翹.md "wikilink")[徐遐生兩位在](../Page/徐遐生.md "wikilink")1960年代中期為解釋[螺旋星系的旋臂結構所推出的理論](../Page/螺旋星系.md "wikilink")，他們的理論引進了長期存在的理想**[準靜態密度波](../Page/準靜態.md "wikilink")**（也稱為**heavy
sound**）\[1\])，他們選擇讓[星系盤有較高一點的密度](../Page/星系盤.md "wikilink")（大約高10-20%）\[2\]。這個理論也成功的運用在[土星環](../Page/土星環.md "wikilink")。

## 星系旋臂

由于[旋涡星系的自转是](../Page/旋涡星系.md "wikilink")[较差自转](../Page/较差自转.md "wikilink")，内部的自转角速度大于外部的角速度，旋臂本应当越缠越紧，最终完全缠绕在一起。实际情况却并非如此。1942年，瑞典天文学家[林德布拉德首先提出了密度波的思想](../Page/贝蒂尔·林德布拉德.md "wikilink")，1964年，旅美台灣天文学家[林家翘](../Page/林家翘.md "wikilink")、[徐遐生等人建立了系统的密度波理论](../Page/徐遐生.md "wikilink")。

他们建议螺旋臂是旋转的密度波表现：恒星的轨道是椭圆形，而恒星轨道方向的相关变化，即椭圆方向的变化随著与星系中心距离（从一个到另一个）的增加而平稳的变化，就像图中所显示的。很显然的，椭圆轨道在某些区域靠在一起产生了螺旋的效果。组成旋臂的恒星并非始终处于旋臂中，而是有进有出。在旋臂后方，恒星不断进入旋臂，由于恒星密集，引力场加强而被减速，在旋臂前方，旋臂中的恒星速度加快，走出旋臂。因此旋涡星系能够在整体上维持旋臂结构的图案，并且旋臂是与星系的自转方向同向的。密度波理论成功地解释了旋臂结构的成因，得到了很多观测事实的支持，但仍有一些问题尚不清楚。

## 應用在土星環

從1970年代晚期開始，[彼得·戈德赖希(Peter
Goldreich)](../Page/彼得·戈德赖希.md "wikilink")、[徐遐生和其他一些人將密度波理論應用在](../Page/徐遐生.md "wikilink")[土星環](../Page/土星環.md "wikilink")\[3\]\[4\]\[5\]。土星環（特別是[A環](../Page/土星環#A環.md "wikilink")）包含許多由[衛星激發出](../Page/土星的衛星.md "wikilink")[林達博共振與垂直共振產生的螺旋臂密度波](../Page/林達博共振.md "wikilink")。雖然土星環因為中心的質量（土星自己）相較於盤面是非常大的，造成螺旋臂是更緊密並且受到傷害（通常只能延伸數百公里），但主要的物理機制與星系相同\[6\]。[卡西尼任務揭露了許多由環中的衛星](../Page/卡西尼-惠更斯號.md "wikilink")[潘和](../Page/土衛十八.md "wikilink")[阿特拉斯激發出的非常小的密度波](../Page/土衛十五.md "wikilink")，和由巨大的衛星造成的高階共振\[7\]，並且波的形式會因為[土衛十(Janus)和](../Page/土衛十.md "wikilink")[土衛十一(Epimetheus)的軌道變化而隨著時間改變](../Page/土衛十一.md "wikilink")\[8\]。

## 參考資料

<div class="references-2column">

<references/>

</div>

## 外部資料來源

Bertin, Giuseppe. 2000. *Dynamics of Galaxies.* Cambridge: Cambridge
University Press.

Bertin, G. and C.C. Lin. 1996. *Spiral Structure in Galaxies: A Density
Wave Theory.* Cambridge: MIT Press.

C.C. Lin, Yuan, C., and F.H. Shu,
\[<http://adsabs.harvard.edu/abs/1969ApJ>...155..721L|"On the Spiral
Structure of Disk i Galaxies III. Comparison with Observations"\], Ap.J.
155, 721 (1969). (SCI)

Yuan,
C.,\[<http://adsabs.harvard.edu/cgi-bin/bib_query?1969ApJ>...158..889Y|"Application
of Density-Wave Theory to the Spiral Structure of the Milky Way System
I. Systematic Motion of Neutral Hydrogen"\], Ap.J., 158, 871 (1969).
(SCI)

## 进一步阅读

<div class="references-small">

  -

</div>

[Category:星系天文学](../Category/星系天文学.md "wikilink")
[D](../Category/銀河系天文學.md "wikilink")

1.  \[<http://adsabs.harvard.edu/full/1974ARA&A>..12..113K
    1974ARA\&A..12..113K Page 113 \]

2.

3.

4.

5.

6.
7.

8.