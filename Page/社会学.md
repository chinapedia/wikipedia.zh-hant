**社會學**（）起源於19世紀末期，是一門研究[社會的](../Page/社會.md "wikilink")[學科](../Page/學科.md "wikilink")\[1\]。社會學使用各種[研究方法進行](../Page/社會調查.md "wikilink")[實證調查](../Page/經驗主義.md "wikilink")\[2\]\[3\]和[批判分析](../Page/批判理論.md "wikilink")\[4\]，以發展及完善一套有關[人類社會](../Page/人類社會.md "wikilink")[結構及](../Page/社會結構.md "wikilink")[活動的](../Page/社會活動.md "wikilink")[知識體系](../Page/學問.md "wikilink")，並會以運用這些知識去尋求或改善[社會福利為目標](../Page/社會福利.md "wikilink")。社會學的研究範圍廣泛，包括了由[微觀層級的](../Page/微觀社會學.md "wikilink")[社會行動](../Page/機構_\(社會學\).md "wikilink")(agency)或[人際互動](../Page/人際關係.md "wikilink")，至[宏觀層級的社會系統或結構](../Page/宏觀社會學.md "wikilink")\[5\]，社會學的[本體有社會中的個人](../Page/本體.md "wikilink")、[社會結構](../Page/社會結構.md "wikilink")、[社會變遷](../Page/社會變遷.md "wikilink")、[社會問題](../Page/社會問題.md "wikilink")、和[社會控制](../Page/社會控制.md "wikilink")，因此社會學通常跟[經濟學](../Page/經濟學.md "wikilink")、[政治學](../Page/政治學.md "wikilink")、[人類學](../Page/人類學.md "wikilink")、[心理學等學科並列於](../Page/心理學.md "wikilink")[社會科學領域之下](../Page/社會科學.md "wikilink")。

社會學在研究題材上或研究法則上均有相當的廣泛性，其傳統研究對象包括了[社會分層](../Page/社會分層.md "wikilink")、[社會階級](../Page/社會階級.md "wikilink")、[社會流動](../Page/社會流動.md "wikilink")、[社會宗教](../Page/宗教社會學.md "wikilink")、[社會法律](../Page/法律社會學.md "wikilink")、[越軌行為等](../Page/越軌社會學.md "wikilink")，而採取的模式則包括[定性和](../Page/定性分析.md "wikilink")[定量的研究方法](../Page/定量分析.md "wikilink")。由於人類活動的所有領域都是由社會結構、個體機構的影響下塑造而成，所以隨著社會發展，社會學進一步擴大其研究重點至其他相關科目，例如[醫療](../Page/醫療.md "wikilink")、[護理](../Page/護理.md "wikilink")、[軍事或](../Page/軍事.md "wikilink")[刑事](../Page/刑事.md "wikilink")[制度](../Page/制度.md "wikilink")、[網際網路等](../Page/網際網路.md "wikilink")，甚至是例如科學知識發展在社會活動中的作用一類的課題。另一方面，社會科學方法(social
scientific
methods)的範圍也越來越廣泛。在20世紀中葉以來多樣化的[語言](../Page/語言.md "wikilink")、[文化轉變也同時產生了更多更具](../Page/文化.md "wikilink")[詮釋性](../Page/解釋學.md "wikilink")、[哲學性的社會研究模式](../Page/哲學.md "wikilink")。然而，自20世紀末起的科技浪潮，也為社會學帶來了嶄新的數學化計算分析技術，例如[個體為本模型和](../Page/個體為本模型.md "wikilink")[社交網路](../Page/社交網路.md "wikilink")\[6\]\[7\]。

因其興起的歷史背景，社會學研究的重心很大一部份放在[現代性社會中的各種](../Page/現代性.md "wikilink")[生活實態](../Page/生活.md "wikilink")，或是當代社會如何形成演進以至今日的過程，不但注重描述現況，也不忽略社會變遷。社會學的研究對象範圍廣泛，小到幾個人面對面的日常互動，大到全球化的社會趨勢及潮流。家庭、各式各樣的組織、企業工廠等經濟體、城市、市場、政黨、國家、文化、媒體等都是社會學研究的對象，而這些研究對象的共通點是一些具有社會性的社會事實。雖然「社會性」的定義在不同學派之間仍有爭執，但社會事實外在於個人，且對個人的行為跟認知有影響，這一點是大致上為社會學者所共同接受的。

## 概观

在19世纪初期，[孔德認為過去](../Page/奥古斯特·孔德.md "wikilink")[神學及崇尚](../Page/神學.md "wikilink")[武力的社會慢慢消失](../Page/武力.md "wikilink")，以[理性](../Page/理性.md "wikilink")、[科學](../Page/科學.md "wikilink")、[工業為主的年代正在抬頭](../Page/工業.md "wikilink")，兩者之間的衝突正是社會動盪的源頭。在[社會制度轉變的時期](../Page/社會制度.md "wikilink")，這種過渡性的衝突及混亂是無可避免。在舊制度消失前，人們無法體驗新制度，往往需要多次社會轉變才能改組社會\[8\]。社會學就是針對社會改組的問題（[現代性](../Page/現代性.md "wikilink")）而發展成為一門社會科學。[涂爾幹學派的社会学家不但希望了解什么使得社会团体聚集起来](../Page/涂爾幹學派.md "wikilink")，更希望了解[社会瓦解的发展过程](../Page/社会瓦解.md "wikilink")，從而作出“糾正”。糾正的觀念並不暗示社會學家預設一個「正確社會」的模板。[涂爾幹本人都不認為有一個終極的](../Page/涂爾幹.md "wikilink")、完美的社會可以參考。簡單來說，當一群人互動形成一個穩定的參考架構，運行良久，對他們來說就是「常態」。當社會制度問題積累下來，此一穩定狀態被打破，形成混亂，即上述所謂「瓦解」時，就形成病態。因此涂爾幹認為，社會學家職責在於找出一個社會的「日常常態」，如此才能確定出「病態」的判準。社會學家需要不斷觀察與分析，不是找出模板，宣稱終極事實就結束。

相反地，[法兰克福学派的社會學家并不探索对糾正社会的方法](../Page/法兰克福学派.md "wikilink")。因为他们认为对社会病疾提出的糾正方案，往往是以一个小群体的观念强加到绝大多数人的身上，这不但解决不了问题，还會使问题加重。20世紀初，如[霍克海默與](../Page/霍克海默.md "wikilink")[阿多諾等人](../Page/阿多諾.md "wikilink")，受到上一代如[韋伯與](../Page/马克斯·韦伯.md "wikilink")[齊美爾學說影響](../Page/格奥尔格·齐美尔.md "wikilink")，加之參考[馬克思理論](../Page/馬克思理論.md "wikilink")，針對現代社會與[資本主義制度產生悲觀看法](../Page/資本主義.md "wikilink")，其中尤其看到社會雖然高度分工，卻也產生生活的分裂化。尤其是在[藝術文化領域](../Page/藝術.md "wikilink")，大量複製技術的出現，造成藝術精神的喪失。法蘭克福第二代[哈伯瑪斯繼承上述觀點](../Page/哈伯瑪斯.md "wikilink")，並參照美國[结构功能主义與](../Page/结构功能主义.md "wikilink")[符号互动论](../Page/符号互动论.md "wikilink")、歐陸的[現象學等](../Page/現象學.md "wikilink")，提出[溝通行動論](../Page/溝通行動論.md "wikilink")，與第一代悲觀觀點不同，認為必須致力建構一個可溝通的社會，才是悲觀現代性的出路。而上述的觀念，類似[社會衝突論](../Page/社會衝突論.md "wikilink")，知名人士如[達倫道夫](../Page/達倫道夫.md "wikilink")，也是以[馬克思對社會階級分析為主](../Page/馬克思.md "wikilink")，認為社會有掌握資源的的階級，以及受統治的階級，惟不同的是，馬克思認為[資產階級握有生產工具](../Page/資產階級.md "wikilink")，[無產階級因無工具而受宰制](../Page/無產階級.md "wikilink")，需要反抗[革命](../Page/革命.md "wikilink")，觀點較屬於[政治經濟學](../Page/政治經濟學.md "wikilink")，但[衝突論者](../Page/衝突論.md "wikilink")，結合韋伯對階層──加入社會地位等角度，認為社會不平等，源自多方面，不只經濟不平等。

今天，社会学家对社会的研究包括了一系列的从宏观结构到微观行为的研究，包括对[种族](../Page/种族.md "wikilink")、[民族](../Page/民族.md "wikilink")、[阶级和](../Page/阶级.md "wikilink")[性别](../Page/性别.md "wikilink")，到细如[家庭结构](../Page/家庭.md "wikilink")[个人社会关系模式的研究](../Page/个人社会关系.md "wikilink")。社会学系分成更多更细的研究方向，包括像[犯罪和](../Page/犯罪.md "wikilink")[离婚](../Page/离婚.md "wikilink")，在微观方面例如有人与人之间的关系。一些社会学家使用[定量研究](../Page/定量研究.md "wikilink")的方法从数量上来描述一个社会总体结构，以此来研究可以预见[社会变迁和人们对社会变迁反应的定量模型](../Page/社会变迁.md "wikilink")。这种由[拉扎斯费尔德倡导的研究方法](../Page/保羅·F·拉扎斯菲爾德.md "wikilink")，现在是社会学研究中的两个主要方法论之一。社会学研究方法的另外一个主要流派是[定性研究](../Page/定性研究.md "wikilink")，包括[歷史社會學](../Page/歷史社會學.md "wikilink")、[参与观察](../Page/参与观察.md "wikilink")、[深度访谈](../Page/深度访谈.md "wikilink")、[专题小组讨论等收集资料的方法](../Page/专题小组讨论.md "wikilink")，以及基于[扎根理论](../Page/扎根理论.md "wikilink")、[内容分析等定性资料的分析及歸納方法](../Page/内容分析.md "wikilink")。从事定性分析的社会学家相信，这是一种更好的方法，因為这可以加強理解“离散”性的社会和独特性的人文。这种方法从不寻求有一致观点，但却可以互相欣赏各自所采取的独特方式并互相借鉴。主流的观点认为，定量和定性这两种研究方式是互补的，而不是矛盾的。[涂爾幹與](../Page/涂爾幹.md "wikilink")[韋伯都強調兩者的重要性](../Page/马克斯·韦伯.md "wikilink")──亦即從主觀（質化）與客觀（量化）交錯研究，到後來的象徵互動派大師[布魯默](../Page/布魯默.md "wikilink")（Herbert
Blumer）相當強調質化的影響力。

## 社會學的角度

社會學的角度就是讓我們通过社會學看事物的方法去認識自己及分析社會。其中兩種比較著名的社會學角度就是「社會學的想像力」（Sociological
Imagination）及「破解謎思」（debunking）。
「社會學的想像力」由[赖特·米尔斯](../Page/赖特·米尔斯.md "wikilink")（C.
Wright Mills）所創，其主要意思是指我們可以透過「社會學的想像力」去了解社會。\[9\]
至於「破解謎思」（debunking）是[彼得L．貝加](../Page/彼得·柏格_\(美國社會學家\).md "wikilink")（Peter
L.
Berger）的概念。他指出了「社會學的意識中是存在著一種揭穿真相、破解謎思（debunking）的動機」。\[10\]這句話指出了社會學家是關注所有的人類活動，但他們關注的層面並不是一般人所關注的事實之表面，而是以一個質疑的心態去思考及破解一件事情的謎思。

## 历史

### 起源

人类对社会变化发展的整体研究源远流长，社会学的[逻辑和理念早在其形成学科基础之前就已经出现](../Page/逻辑.md "wikilink")。对社会的分析和研究基本上是源于[西方文化](../Page/西方文化.md "wikilink")、[哲学思想的长年累积](../Page/哲学.md "wikilink")，并至少在早于[柏拉图时代之前产生](../Page/柏拉图.md "wikilink")。而更加正规的社会统计调查的起源更可以追溯到至少早在1086年的“[末日审判书](../Page/末日审判书.md "wikilink")”\[11\]\[12\]。另一方面，在[中世纪的](../Page/中世纪.md "wikilink")[伊斯兰社会也有早期社会学的踪影](../Page/伊斯兰.md "wikilink")，不少研究均指出历史上第一位[社会学家](../Page/社会学家.md "wikilink")，是一位十四世纪来自[北非的](../Page/北非.md "wikilink")[阿拉伯学者](../Page/阿拉伯人.md "wikilink")——[伊本·赫勒敦](../Page/伊本·赫勒敦.md "wikilink")，其最著名的著作《[历史绪论](../Page/历史绪论.md "wikilink")》（Muqaddimah）首次介绍了有关[社会凝聚力和](../Page/社会凝聚力.md "wikilink")[社会冲突议题的社会科学理论](../Page/社会冲突.md "wikilink")\[13\]\[14\]\[15\]\[16\]\[17\]\[18\]。到了[十七世纪](../Page/十七世纪.md "wikilink")，以[笛卡儿为代表的](../Page/笛卡儿.md "wikilink")[理性主义者即注意到](../Page/理性主义.md "wikilink")[科学拥有发现](../Page/科学.md "wikilink")[真理的能力](../Page/真理.md "wikilink")，而开始尝试运用科学方法来研究社会现象\[19\]。

[社会学一词的英语](../Page/wiktionary:社会学.md "wikilink")“sociology”（或“sociologie”），是由[拉丁语](../Page/拉丁语.md "wikilink")“[socius](../Page/wiktionary:socius.md "wikilink")”（同伴）、“-ology”（研究），和[希腊语](../Page/希腊语.md "wikilink")“[λόγος](../Page/wiktionary:λόγος.md "wikilink")”（即“lógos”，意指字词、知识）组合而成，这个名称最早由[法国](../Page/法国.md "wikilink")[散文家](../Page/散文家.md "wikilink")[埃马纽埃尔-约瑟夫·西哀士](../Page/埃马纽埃尔-约瑟夫·西哀士.md "wikilink")（1748年—1836年）在一份当时未公开的手稿中创造\[20\]\[21\]。至1838年，法国[科学哲学家](../Page/科学哲学.md "wikilink")[奧古斯特·孔德](../Page/奧古斯特·孔德.md "wikilink")（1798年—1857年）正式确立了“社会学”一词\[22\]。孔德在初期曾使用“社会物理学”（social
physics）一词，但后来被别人挪用，其中包括著名的[比利时](../Page/比利时.md "wikilink")[统计学家](../Page/统计学家.md "wikilink")[朗伯·阿道夫·雅克·凱特勒](../Page/朗伯·阿道夫·雅克·凱特勒.md "wikilink")。孔德寄望通过对社会领域整体的科学认识，而令[历史学](../Page/历史学.md "wikilink")、[心理学和](../Page/心理学.md "wikilink")[经济学形成一体化](../Page/经济学.md "wikilink")，社会学正式由此而生。

### 社会转型的产物

[auguste_Comte.jpg](https://zh.wikipedia.org/wiki/File:auguste_Comte.jpg "fig:auguste_Comte.jpg")\]\]
社会学作为一门独立的学科确立于十九世纪中叶，其出现并非历史的偶然，而是与剧烈的社会变迁引起的社会运行状态失调有着密切的关系。自十八世纪末开始，先后发生了两次来自欧洲的大[革命](../Page/革命.md "wikilink")，第一次是以1789年[法国大革命为标志的政治思想大革命](../Page/法国大革命.md "wikilink")，另一次是从[英国开始](../Page/英国.md "wikilink")、一直扩展到欧洲和[美国的](../Page/美国.md "wikilink")[工业革命](../Page/工业革命.md "wikilink")，这两件重大事件使西方社会发生了根本性转型。[资本主义确立后](../Page/资本主义.md "wikilink")，一方面[生产力得到突飞猛进的发展](../Page/生产力.md "wikilink")，令社会的[经济基础](../Page/经济.md "wikilink")、思想观念和[政治制度都发生重大变化](../Page/政治制度.md "wikilink")；另一方面，社会运行也暴露出许多不协调因素，如[周期性经济危机](../Page/经济危机.md "wikilink")、[失业](../Page/失业.md "wikilink")、[犯罪等](../Page/犯罪.md "wikilink")。对于这些问题，传统方法和原有科学体系无法作出满意的解释和回答，于是社会学作为一门不同于其他社会科学的独立新学科便应运而生。

孔德所身处的年代适逢法国大革命之后的持续动荡，他认为社会上的各种弊病和问题可以透过社会学[实证主义消除](../Page/实证主义.md "wikilink")，这是在其著作《[实证哲学教程](../Page/实证哲学教程.md "wikilink")》、《[实证主义概论](../Page/实证主义概论.md "wikilink")》（A
General View of
Positivism）中明确提出的一个[认识论模式](../Page/认识论.md "wikilink")。他相信在人类知识发展“[三阶段律](../Page/三阶段律.md "wikilink")”（Law
of three
stages）中，在继[神学阶段和](../Page/神学.md "wikilink")[形而上学阶段](../Page/形而上学.md "wikilink")（或称[玄学阶段](../Page/玄学.md "wikilink")）之后，实证阶段将占据最终的位置，也是他最重视的部分\[23\]\[24\]。[孔德認為過去神學主导及崇尚武力的社會会慢慢消失](../Page/奥古斯特·孔德.md "wikilink")，以理性、科學、工業為主的年代正在抬頭，兩者之間的衝突正是社會動盪的源頭。在社會制度轉變的時期，這種過渡性的衝突及混亂是無可避免的。在舊制度消失前，人們無法體驗新制度，往往需要多次社會轉變才能改組社會。\[25\]社會學就是針對社會改組問題（[現代性](../Page/現代性.md "wikilink")）而發展成為一門社會科學，它在社会转型中产生，以解决社会运行和发展中的不协调因素，以寻求社会良性运行和协调发展的规律为内容，并且将这条主线贯穿于整个社会学发展历史。

[Karl_Marx.jpg](https://zh.wikipedia.org/wiki/File:Karl_Marx.jpg "fig:Karl_Marx.jpg")\]\]
总体而言，1830年代到19世纪末叶普遍被视为西方社会学的创立时期。这一时期的社会学特点是：（一）与哲学关系密切；（二）受实证主义思潮影响极大；（三）创立社会学的目的在于维护协调现存的社会关系。此时期以[孔德和](../Page/奥古斯特·孔德.md "wikilink")[英国哲学家](../Page/英国.md "wikilink")[赫伯特·斯宾塞为主要代表](../Page/赫伯特·斯宾塞.md "wikilink")。孔德认为，社会学是研究社会秩序和社会进步的科学\[26\]。整个社会和自然界是建立在协调一致的基础之上，这就是[社会秩序](../Page/社会秩序论.md "wikilink")；社会变迁中旧的社会秩序被新的社会秩序所取代，成为[社会进步](../Page/社会变迁论.md "wikilink")。斯宾塞则把社会变迁论运用于社会学研究，他认为社会是一个有机体，社会进化和[生物进化遵循着同一条规律](../Page/生物进化论.md "wikilink")，具有人类社会等同[有机生物的含义](../Page/生物.md "wikilink")，因此他的理论被称为[社会有机论](../Page/社会有机论.md "wikilink")。1874年，斯賓塞编写了历史上首本以社会学为名的书籍——《社会学研究》（The
Study of Sociology）。

当时孔德和[马克思双方均提出在](../Page/马克思.md "wikilink")[欧洲实现](../Page/欧洲.md "wikilink")[工业化和](../Page/工业化.md "wikilink")[世俗化之后](../Page/世俗化.md "wikilink")，发展一种科学上合理的社会制度，但同时马克思又否定了以孔德为首的社会学实证主义。在孔德提出社会学构想的同时，马克思在《[1844年经济学哲学手稿](../Page/1844年哲学和经济学手稿.md "wikilink")》中已经形成了新的[唯物主义历史观](../Page/历史唯物主义.md "wikilink")。马克思认为人与[自然界的关系是以](../Page/自然界.md "wikilink")[劳动为中介的](../Page/劳动.md "wikilink")，他从[经济学上分析了在](../Page/经济学.md "wikilink")[生产资料](../Page/生产资料.md "wikilink")[私有制度下](../Page/私有制.md "wikilink")，人的劳动本性异化的表现，提出了劳动的解放将标志着人性的复归和社会的[人性化](../Page/人性.md "wikilink")，从而使对资本主义的经济学批判转入到在社会学上关于资本主义对人限定的地位的批判\[27\]。马克思唯物史观社会学的产生，使社会学在本质上变成为批判的、革命的科学，与孔德强调秩序、均衡，目的在于维护和改良现存制度的实证主义社会学形成鲜明的对立。这也是马克思本人一直拒绝把自己的社会理论称为“社会学”的基本原因，直至二十世纪初[列宁才明确把马克思的社会理论称之为](../Page/列宁.md "wikilink")[马克思主义社会学](../Page/马克思主义社会学.md "wikilink")，使之为早期社会学的重要支柱之一。二十世纪著名哲学家[以赛亚·伯林也认为](../Page/以赛亚·伯林.md "wikilink")，马克思可以被视为“真正的现代社会学之父”\[28\]。

### 形成学术学科

[Emile_Durkheim.jpg](https://zh.wikipedia.org/wiki/File:Emile_Durkheim.jpg "fig:Emile_Durkheim.jpg")\]\]
十九世纪末期开始，社会学开始蓬勃发展，归功于法国社会学家[爱米尔·涂尔干出版了大量社会学研究专著](../Page/爱米尔·涂尔干.md "wikilink")，为社会学的规范化和科学化奠定了坚实基础，并为确立社会学的学科地位作出重大贡献。他一生致力推动社会学发展，将源自孔德的实证主义发展成实际社会研究的基础。1895年，涂尔干在法国[波尔多大学创立了欧洲首个社会学系](../Page/波尔多大学.md "wikilink")，并出版了影响后世的重要著作《[社会学方法的规则](../Page/社会学方法的规则.md "wikilink")》\[29\]，为社会学确立了有别于其他社会科学学科的独立研究对象，即[社会事实](../Page/社会事实.md "wikilink")。涂尔干认为，一切社会的观念都具有无形的强制力，塑造了人们的意识。人类大多数的意向不是个人自己生成的，而是在外界的引导、熏陶和压迫下形成的。社会事实无法用生理学、心理学或其他研究个体的方法来解释，而必须用社会学的方法、观点解释\[30\]。至1896年，涂尔干又创办了《[社會學年鑒](../Page/社會學年鑒.md "wikilink")》（L'Année
Sociologique）期刊，以刊登、传播他与学生的研究成果\[31\]。在1897年出版的《[自杀论](../Page/自杀论.md "wikilink")》中，他以[罗马天主教教徒和](../Page/罗马天主教.md "wikilink")[新教教徒人口](../Page/新教.md "wikilink")[自杀率的差异为例](../Page/自杀率.md "wikilink")，引证社会学角度和心理学、哲学角度在分析社会事实上的区别，另外此著作也进一步发展了[结构功能主义理论](../Page/结构功能主义.md "wikilink")。对涂尔干来说，社会学可以被形容为：

踏入20世纪，社会学的学术地位迅速提高，欧洲各地的大学相继成立社会学部。[英国第一个社会学系在](../Page/英国.md "wikilink")1904年创建于[伦敦政治经济学院](../Page/伦敦政治经济学院.md "wikilink")，这里也是[英国社会学杂志](../Page/英国社会学杂志.md "wikilink")（British
Journal of
Sociology）的创刊地\[32\]。1907年起，英国第一位社会学教授[伦纳德·特里劳尼-霍布豪斯](../Page/伦纳德·特里劳尼-霍布豪斯.md "wikilink")（Leonard
Trelawny
Hobhouse）开始在[伦敦大学担任社会学讲师](../Page/伦敦大学.md "wikilink")\[33\]。1909年，[德国社会学家](../Page/德国.md "wikilink")[斐迪南·滕尼斯和](../Page/斐迪南·滕尼斯.md "wikilink")[马克斯·韦伯共同成立了](../Page/马克斯·韦伯.md "wikilink")[德国社会学协会](../Page/德国社会学协会.md "wikilink")（Deutsche
Gesellschaft für
Soziologie），随后，韦伯又在1919年于德国[慕尼黑大学设立了德国首个社会学系](../Page/慕尼黑大学.md "wikilink")，并首次提出了[反实证主义社会学的理念](../Page/反实证主义.md "wikilink")\[34\]。[弗洛里安·兹纳涅茨基](../Page/弗洛里安·兹纳涅茨基.md "wikilink")（Florian
Znaniecki）在1920年于[波兰](../Page/波兰.md "wikilink")[亚当·密茨凯维奇大学](../Page/亚当·密茨凯维奇大学.md "wikilink")（Adam
Mickiewicz
University）创立波兰首个社会学系。1923年，[法兰克福大学设立了社会研究院](../Page/法兰克福大学.md "wikilink")（Institut
für Sozialforschung）\[35\]。

与此同时，美国在社会学方面也有长足的发展。[美国社会学家](../Page/美国.md "wikilink")[威廉·格雷厄姆·萨姆纳](../Page/威廉·格雷厄姆·萨姆纳.md "wikilink")（William
Graham
Sumner）于1875年在[耶鲁大学开始教授名为](../Page/耶鲁大学.md "wikilink")“社会学”的课程\[36\]。1890年2月，[法兰克·威尔森·布莱克默](../Page/法兰克·威尔森·布莱克默.md "wikilink")（Frank
Wilson
Blackmar）在美国[堪萨斯大学设立了](../Page/堪萨斯大学.md "wikilink")“社会学元素”（Elements
of
Sociology）课程，并延续至今\[37\]。1892年，[芝加哥学派的代表人物](../Page/芝加哥学派.md "wikilink")——[阿尔比昂·伍德伯里·斯莫尔](../Page/阿尔比昂·伍德伯里·斯莫尔.md "wikilink")（Albion
Woodbury
Small），在[芝加哥大学创立了社会学系](../Page/芝加哥大学.md "wikilink")\[38\]。[社会心理学始创者之一的](../Page/社会心理学.md "wikilink")[喬治·賀伯特·米德](../Page/喬治·賀伯特·米德.md "wikilink")，于[密歇根大学任教时遇到了](../Page/密歇根大学.md "wikilink")[查理斯·霍顿·库利和](../Page/查理斯·霍顿·库利.md "wikilink")[约翰·杜威](../Page/约翰·杜威.md "wikilink")；米德和杜威两人在1894年转往芝加哥大学任教\[39\]，这使得社会心理学、[象徵互動論](../Page/象徵互動論.md "wikilink")（symbolic
interactionism，或称符號相互作用論）获得极大的发展\[40\]。1895年，阿尔比昂·W·斯莫尔创办了美国首份社会学学术期刊——[美国社会学杂志](../Page/美国社会学杂志.md "wikilink")（American
Journal of
Sociology）。1905年，[美国社会学协会](../Page/美国社会学协会.md "wikilink")（American
Sociological
Association，ASA）成立\[41\]，并成为现今世界上规模最大的社会学家学会。深受涂尔干和[马克斯·韦伯思想所影响的美国社会学者](../Page/马克斯·韦伯.md "wikilink")[塔尔科特·帕森斯](../Page/塔尔科特·帕森斯.md "wikilink")\[42\]，一生致力推动社会学的专业化以及在美国的发展。受地理和历史因素影响，相比起欧洲的经历，美国社会学的发展在历史上较少受到[马克思主义的影响](../Page/马克思主义.md "wikilink")，这个文化传统一直延续至今，而美国社会学更着重统计学上的研究方式\[43\]。

在[中国](../Page/中国.md "wikilink")，[上海](../Page/上海.md "wikilink")[沪江大学于](../Page/沪江大学.md "wikilink")1913年创立了中国大学第一个社会学系（该系在1952年并入[华东师范大学](../Page/华东师范大学.md "wikilink")），开设了包括人类学、理论与应用社会学、社会调查与社会问题以及公民学在内的诸多课程，之后在1917年设立的[沪东公社则是中国第一个大学社会学实验基地](../Page/沪东公社.md "wikilink")，被视为中国社会工作服务的前身。

第一次关于社会学的国际合作发生于1893年。[法国政府国会](../Page/法国政府.md "wikilink")[核数师勒内](../Page/核数师.md "wikilink")·沃尔姆斯（René
Worms）创刊了[国际社会学杂志](../Page/国际社会学杂志.md "wikilink")（Revue Internationale
de Sociologie）并担当其主编，随后又成立了“国际社会学图书馆”（Bibliothèque Sociologique
Internationale）、“巴黎社会学学会”（Société de Sociologie de
Paris）、“国际社会学研究所”（Institut International de
Sociologie
），其中国际社会学研究所后来与创立于1949年的[国际社会学协会合并](../Page/国际社会学协会.md "wikilink")\[44\]。

总体而言，社会学形成学术规模的成长期大约处于19世纪末到20世纪30年代。社会学的发展历程可以被看作为对例如[工业化](../Page/工业化.md "wikilink")、[城市化](../Page/城市化.md "wikilink")、[世俗化等](../Page/世俗化.md "wikilink")[现代性问题的积极回应](../Page/现代性.md "wikilink")，或是对社会[理性化](../Page/理性化.md "wikilink")（rationalization）的理解和反思\[45\]。在社会学创始初期这段时期期间，社会学领域在[欧洲大陆占据了主导地位](../Page/欧洲大陆.md "wikilink")，并与[英国的](../Page/英国.md "wikilink")[人类学和](../Page/人类学.md "wikilink")[统计学遵循着类似的发展轨迹](../Page/统计学.md "wikilink")、举头并进；但踏入[二十世纪以后情况开始逆转](../Page/二十世纪.md "wikilink")，越来越多的社会学家活跃在所谓的“[盎格鲁势力范围](../Page/盎格鲁势力范围.md "wikilink")”（Anglosphere），即以英语为主的地区。与此同时，社会学确立了自己的研究范围和方法，独立的学科体系基本形成，而研究范围越来越具体化和专门化。很少早期的社会学家会限定于特定、狭隘的研究主题，例如[经济学](../Page/经济学.md "wikilink")、[法学](../Page/法学.md "wikilink")、[心理学](../Page/心理学.md "wikilink")、[哲学等理论相关领域也是其关注范围](../Page/哲学.md "wikilink")。自社会学在十八世纪奠基以来，社会学的认识论、方法、研究架构等一直经历着扩展和分歧，形成百家争鸣的情况\[46\]。

这一时期形成了以[埃米尔·涂尔干为代表的实证主义路线](../Page/埃米尔·涂尔干.md "wikilink")，以[马克斯·韦伯为代表的反实证主义路线和以美国](../Page/马克斯·韦伯.md "wikilink")[芝加哥学派为代表的社会调查的传统的社会学研究局面](../Page/芝加哥学派.md "wikilink")。涂尔干的社会学理论以社会关系和社会团结为主线展开，坚持社会唯识论和社会整体观，主张在社会整体层次上进行实证性研究。同时他还对社会反常现象和社会偏离行为产生兴趣。韦伯则是倾向于个体主义，即主张对特定的社会行动和社会现象背后的个人动机及隐藏的秘密进行解释。他试图验证任何社会现象背后都有一种无形的、精神的力量在支撑。

### 当代社会学

当代社会学的发展时期大致从20世纪40年代至今。这一时期的特点是：

  - 第二次世界大战后，西方社会学得到前所未有的发展，其重心从欧洲转移到美国
  - 理论多元化，学派林立，观点分歧
  - 分科化，社会学与其他社会科学的渗透加强，分科社会学种类增多
  - 方法科学化，计算机的广泛运用，大大加快了资料处理速度，使社会学的研究更准确、快速、规范

## 社会学理论

[Max_Weber_1894.jpg](https://zh.wikipedia.org/wiki/File:Max_Weber_1894.jpg "fig:Max_Weber_1894.jpg")|缩略图\]\]

社会学理论经过一百多年的发展，尽管存在着缺乏完整的理论构架和很多彼此矛盾的方面，但已经成为成熟丰富且脉络清晰的理论体系，其中很多理论都有着鲜明的实践指向并在现实生活中发挥了有益的作用。也有很多理论虽然现在还很抽象概要，但也不乏启发性的地方。依照发展程度和时间顺序，社会学理论一般分作古典社会学理论、现代社会学理论和当代社会学理论。

古典社会学理论很大程度上是一些著名社会学家的理论，也就是说这一时期的社会学理论还缺乏完备的理论流派，单个社会学家的理论就能代表这一流派的特征。所以，人们在阐述这一阶段的理论的时候，多是介绍某几个社会学家的理论成果。不过这样的弊端也显而易见，单个社会学家理论成果对整个理论体系的代表在一些情况下过于勉强。所以，通过对其特征的归类和总结，将其归纳为以下四个理论框架。这样一来在对这一时期的理论有较为明晰的认识之后，又能与下一阶段的流派分类衔接。但要对各个理论有深入的了解，还是要回到这些社会学家身上。

  - [结构功能主义](../Page/结构功能主义.md "wikilink")，研究社会结构如何构造，以满足由一个至高共享的规范体系所限定的各项功能的要求，和社会结构中各部分的功能和影响。社会被看做是一个有机统一的整体，人被看作是社会整体的一部分，是宗教和文化的遵奉者，没有社会和道德方面的支持就难以生存。[涂尔干的社会团结和](../Page/涂尔干.md "wikilink")[社会分工论](../Page/社会分工论.md "wikilink")、[斯宾塞](../Page/斯宾塞.md "wikilink")[社会进化论在这个领域的建树都尤为突出](../Page/社会进化论.md "wikilink")。

<!-- end list -->

  - [冲突论](../Page/冲突论.md "wikilink")，也称批判理论，通过对社会物质发展的考察，对照人类社会的演进，探析出两者之间的紧密联系，从而得出人和社会是社会历史发展的产物，是具体历史社会情境中的产物，并被其所在的社会经济和历史方面的定位任意摆布、操纵和扭曲，使他们真实的自我发生畸变，进而产生持续的矛盾冲突体，反过来这样的冲突又推动着社会的发展。[马克思的批判结构主义是这一方面的集大成者](../Page/马克思.md "wikilink")。

<!-- end list -->

  - [交换理论](../Page/交换理论.md "wikilink")，建立在个体[功利主义的基础上](../Page/功利主义.md "wikilink")，通过对个人利益的计算和实现手段的考察来解释人的行为，社会现象被还原为始终处于理性利益计算之中的个人。[霍曼斯的行为主义](../Page/霍曼斯.md "wikilink")[交换论](../Page/交换论.md "wikilink")，[布劳的结构交换论和](../Page/布劳.md "wikilink")[艾默森的社会交换网络分析都对理解这一理论范式有很大裨益](../Page/艾默森.md "wikilink")。

<!-- end list -->

  - [建构理论](../Page/建构理论.md "wikilink")，将人看作是能动的社会建构者，人有资格，也有能力积极主动地创造或建构着社会，而人类的理性始终有着重要的指导作用。他将社会行为的理解置于对个人和主体间的意义和动机的理解。[韦伯的](../Page/韦伯.md "wikilink")[理解社会学](../Page/理解社会学.md "wikilink")，[齐美尔的](../Page/齐美尔.md "wikilink")[形式社会学以及随后一系列学者构建的符号互动论](../Page/形式社会学.md "wikilink")，虽然看上去彼此差异很大，但质性方法论和对能动性的承认将其联系到了一起。

经过古典阶段的积淀，社会学理论摆脱了对哲学和其他社会科学的依赖，真正走上了特色鲜明的学科发展之路。现代社会学理论在取得重大发展的同时，其中的古典社会学理论影子依然清晰可见，这也说明二者的延续性和承袭性。事实上，现代与古典的阶段划分并非是界限分明的，二者除代表性人物和理论流派之外，都存在不少的交集。社会学理论在古典时期的四个理论框架，在此有不断的丰富和发展，随着特征差异增大，这些流派又不断分化，形成新的框架体系。

社會学理論當中常常用一些抽象和甚至複雜理論框架來解釋和分析社會樣式和宏觀社會結構。社會学理論總跟經典的學科有一個令人不安的關係，就是大部分的重要社會學家從未擔任大學職務。現今社會理論被認為社會學分支，涉足多個科學區域譬如人類學、[經濟](../Page/經濟.md "wikilink")、[神學](../Page/神學.md "wikilink")、[歷史等等](../Page/歷史.md "wikilink")。第一種社會理論幾乎跟社會學同時誕生。**社會學之父**——孔德創立第一社會理論——社會演化理論。在19世紀，三大社會理論分別是[社會進化論](../Page/社會進化論.md "wikilink")、[社會週期論和](../Page/社會週期論.md "wikilink")[馬克思主義的](../Page/馬克思主義.md "wikilink")[歷史唯物主義](../Page/歷史唯物主義.md "wikilink")。雖然它們現在被認為是過時，但是它們卻產生了新理論，像社会积淀论、[新進化論](../Page/新進化論.md "wikilink")、現代化的[社會生物學](../Page/社會生物學.md "wikilink")、[後工業化的社會理論及](../Page/後工業化的社會理論.md "wikilink")[多邊理論](../Page/多邊理論.md "wikilink")。跟实验性的自然科學不同，社會理論家很少使用自然科學方法及其他事證方式來證明論點。反而，他們面對非常大型的社會走勢時候都使用一些假說。可是，這些假說需要很長的時間來證明。這正是反對者所批評的重點。對於解構主義者及後現代主義者，他們更質疑所有的研究及方法都是錯誤地承襲下來，很多時候，社會理論被認定為不可證明的。实际上，社会实践是检验社会学说的唯一标准。社会学说在社会实践中产生，在社会实践中检验，在社会实践中发展。

### 西方社会思想源流

#### 古希腊

发生在[古希腊社会剧烈的社会转型](../Page/古希腊.md "wikilink")，促使当时的思想家们不得不认真思考人以及有人组成的群体的问题，[黄金时代的提出就是这样一个例子](../Page/黄金时代.md "wikilink")。当时的思想家对于奴隶制度的黑暗也甚为愤慨，但苦于没有更好的制度来替代，对人类命运的发展趋势也难以把握，于是追述往昔，希望回到奴隶制形成之前的社会，恢复人在社会中的尊严。

其中两位哲人的社会思想尤其能说明问题。[柏拉图眼中的人类社会是有机统一的](../Page/柏拉图.md "wikilink")，国家的各个组成部分组成了国家的功能，也从属于整体的调配，社会是一个分工的和不平衡的统一体。圣贤（哲学王）则是居于社会最顶端掌控国家机器的最佳人选，因为他有足够多的经验和知识，能够超越具体部分的利益，实现整个社会的最大福祉。在柏拉图看来，私有财产和家庭这样分散且独立的因素应该为社会整体的发展发挥自己的作用。[亚里士多德与此相反](../Page/亚里士多德.md "wikilink")。在《[政治学](../Page/政治学.md "wikilink")》中一系列对人类社会和国家的看法虽然还较为混沌，但其中的社会学思想却是显而易见的，他就认为社会各个部分的独立要素在构成社会整体的同时，也能保持各自的独立性。他指出，社会起源于人的社会本性，即人有集合为群体的本能，但复杂、且异常分化的社会并非由个人组成的，它是建立在一个个群体的基础上，这样有相应的功能和财产的共同体最终组成了社会。\[47\]

#### 启蒙思想

[启蒙思想对于西方文明的影响不言而喻](../Page/启蒙思想.md "wikilink")，诸多社会科学在一次次的思想解放和突破中诞生，对社会转型产生的社会问题的关注，很大程度上也助推了整个启蒙思想的发展。

[社会契约论是这一时期最重要的社会思想](../Page/社会契约论.md "wikilink")，诞生于与教权和王权思想斗争中，由此发展出来的一种契约义务和社会关系结构的世界观，成为社会学起源的直接动因之一。由“天赋人权”为逻辑起点，从而推演出社会发展的趋势，是从自然状态向社会状态的不断演进，人们相互订立契约、建立国家来保障自己天赋之权，并由此发展对私有财产、自由、平等和享乐权利的认识。同时，众多思想家中还有很多对于人类社会的洞见都对之后的社会学思想产生重要影响。

[孟德斯鸠关于自然地理条件对社会](../Page/孟德斯鸠.md "wikilink")、国民心态影响的论述，及由此发展的历史演进理论，合同[三权分立学说共同构成了他的社会整体论](../Page/三权分立.md "wikilink")。[伏尔泰的自然法权论则是在寻找符合人自然本性的社会立法原则](../Page/伏尔泰.md "wikilink")。[狄德罗的小康社会思想](../Page/狄德罗.md "wikilink")，[让-雅克·卢梭关于社会发展阶段的思想](../Page/让-雅克·卢梭.md "wikilink")，[休谟关于权威对社会影响的观点](../Page/休谟.md "wikilink")，[亚当·斯密研究的关于商业社会中产生社会分化的命题](../Page/亚当·斯密.md "wikilink")，[福格森关于市民社会的研究](../Page/福格森.md "wikilink")，[圣西门在工业社会方面的研究以及对](../Page/圣西门.md "wikilink")[实证主义的运用等等](../Page/实证主义.md "wikilink")，都是这一时期的思想家们对社会思想发展的重要贡献。\[48\]

### 社会学理论具体方向

  - [衝突理論](../Page/衝突理論.md "wikilink")
  - [結構功能主義](../Page/結構功能主義.md "wikilink")
  - [马克思唯物史观](../Page/马克思唯物史观.md "wikilink")
  - [社会有机体论](../Page/社会有机体论.md "wikilink")
  - 社会发展论
  - 社会积淀论
  - 社会互动論
  - 社会认知论
  - 社会控制論

## 社會研究方法

社會研究主要有“定性研究”和“定量研究”两大类。會用各種方法搜集經驗實證，包括問卷、面談、參與者觀察及統計研究。历史社会学研究属于定性社会研究方法。同样的，社会学与[人类学学科的交融催生越来越多社会学家运用](../Page/人类学.md "wikilink")[民族志的方法对现代社会进行研究](../Page/民族志.md "wikilink")。

不同的方法所面對的困難是它們都根據研究員各自採納的理論基礎來解釋及了解社會。作為[功能主義者](../Page/功能主義.md "wikilink")，[艾彌爾·涂爾幹喜歡以社會大規模結構來解釋任何東西](../Page/艾彌爾·涂爾幹.md "wikilink")。如果他是符號互動者，他便專注人們如何理解別人。[馬克思主義者或](../Page/馬克思主義.md "wikilink")[新馬克思主義者把什麼都化作](../Page/新馬克思主義.md "wikilink")[階級鬥爭](../Page/階級鬥爭.md "wikilink")。[現象學家只是思考大眾對現實如何建立自己的意義](../Page/現象學.md "wikilink")。當各方都面對現實社會問題時，常常爭論誰對誰錯，而實際上會把不同方法學結合。

一般觀點將社會學區分為宏觀或微觀兩類，認為分析單位如國家發展，就屬於前者；分析個人精神與家庭等，較屬於後者。這是相當僵化的解決看法。事實上，所謂微觀並非依照研究單位的規模看，微觀較屬於處理社會學基本原型的東西，更觀察在不同單位之間的互動，事實上，在企業之間、乃至於國家之間，也可以視為是一個個個體，研究他們之間的互動關係，以微觀方式處理之─諸如用象徵互動論、現象學方法等。

[互聯網是社會學家的興趣所在](../Page/互聯網.md "wikilink")，原因有四。

1.  它是研究工具。例如網上問卷調查代替紙張問卷。
2.  它成為討論平臺。
3.  它本身是研究課題。[互聯網的社會學研究網上社區](../Page/互聯網.md "wikilink")、虛擬社區。
4.  因為[互聯網而產生社會組織上的改變](../Page/互聯網.md "wikilink")，例如由工業社會轉型到知識社會的大型社會改變。

一般歐美注意到互聯網問題，較傾向從媒介角度出發。認為它不僅是一個溝通工具而已，更在於改變人們互動的模式，一如20世紀以前，貨幣出現連帶影響人們的現代都會生活等；或像[古騰堡印刷術在歐洲蓬勃之後](../Page/古騰堡印刷術.md "wikilink")，對歐洲知識與教育等面向的改變。

## 社會學與其他社會科學

社會學家研究時常常分析群體，如社会阶级、社會組織、宗教組織、政治組織及商業組織。他們研究社會群體間的互動、跟蹤源頭及發展過程、分析群體活動對各個成員的影響。社會學家關心社會群體的特徵、群體間或成員間的互動影響及社會特徵（例如：[性別](../Page/性別.md "wikilink")、[年齡](../Page/年齡.md "wikilink")、阶级、[種族](../Page/種族.md "wikilink")）對日常生活所帶來的效果。這些社會研究結果能夠協助教育家、立法者、行政人員、社工等解決社會問題並制定公共政策。大部分的社會學家都有多項專長，例如社會組織、社會分層及[社會流動](../Page/社會流動.md "wikilink")、種族關係、[教育](../Page/教育.md "wikilink")、[家庭](../Page/家庭.md "wikilink")、[社會心理學](../Page/社會心理學.md "wikilink")、[城市](../Page/城市.md "wikilink")、[農村](../Page/農村.md "wikilink")、[政治及](../Page/政治.md "wikilink")[比較社會學](../Page/比較社會學.md "wikilink")、[性別角色及關係](../Page/性別角色.md "wikilink")、[人口地理學](../Page/人口地理學.md "wikilink")、[老年學](../Page/老年學.md "wikilink")、[犯罪心理學及社會學應用](../Page/犯罪心理學.md "wikilink")。近年来，很多社会学家也开始研究性别、阶级和种族相结合而产生的种种社会不平等问题。\[49\].

今天，社會學研究人類組織、社會群體、社會互動等等都使用大量比較方法。社會學的發展對其它科學帶來額外的需求，這對研究現代工業社會是十分重要。近年，得到了人類學的啟示，加速了對多元文化及多元國民的研究。

另一方面，社會學朝著微社會結構發展，例如：[種族](../Page/種族.md "wikilink")、[社會階級](../Page/社會階級.md "wikilink")、[性別及](../Page/性別.md "wikilink")[家庭](../Page/家庭.md "wikilink")。因為有很多罪案發生、家庭問題產生了很大的社會壓力，急需要社會學來尋找解決方法。

社會學家透過定性研究和定量研究來研究社會關係以預測社會變動。他們希望透過[質性研究](../Page/質性研究.md "wikilink")，如面談及小組討論，對社會運作有更深入的理解。有些社會學家正辯論着從中作出平衡填補兩者之間的空隙。例如：[定量研究描述大型社會現象](../Page/定量研究.md "wikilink")，而[定性研究描述個人如何理解大型社會現象](../Page/定性研究.md "wikilink")。在[二十世紀早期](../Page/二十世紀.md "wikilink")，社會學家及[心理學家曾對](../Page/心理學.md "wikilink")[工業社會作出研究](../Page/工業社會.md "wikilink")，對[人類學作出了貢獻](../Page/人類學.md "wikilink")。要留意一點的是[人類學家都曾對工業社會作出研究](../Page/人類學家.md "wikilink")。今天社會學及人類學主要分別在於研究不同的理論和方法而不是對象。

[社會生物學是綜合社會學及生物學的一門新科學](../Page/社會生物學.md "wikilink")。雖然它很快獲得接受，但仍然有很多爭論的地方因為它嘗試使用進化及生物過程來解釋社會行為及結構。社會生物學家常被社會學家批評過份倚賴基因對行為的影響。社會生物學家卻說在自然之間和哺育存在一個複雜關係。故此社會生物學跟[人類學](../Page/人類學.md "wikilink")、[動物學](../Page/動物學.md "wikilink")、[進化心理學有密切關係](../Page/進化心理學.md "wikilink")。這仍然是其他科學所不能接受的。一些社會生物學家像Richard
Machalek要求使用社會學來研究非人類社會。

社會學跟[社會心理學有關係](../Page/社會心理學.md "wikilink")，前者關心[社會結構](../Page/社會結構.md "wikilink")，後者關心[社會行為](../Page/社會行為.md "wikilink")。

## 主要的社会学主题

  - 群体组织方面：[家庭](../Page/家庭.md "wikilink")－[社区](../Page/社区.md "wikilink")—[乡村](../Page/乡村.md "wikilink")－[集镇](../Page/集镇.md "wikilink")－[城市](../Page/城市.md "wikilink")－[部落](../Page/部落.md "wikilink")－[民族](../Page/民族.md "wikilink")－[团体](../Page/团体.md "wikilink")—[文化](../Page/文化.md "wikilink")
      - [群体与组织](../Page/群体与组织.md "wikilink")
      - [初级社会群体](../Page/初级社会群体.md "wikilink")
          - [家庭](../Page/家庭.md "wikilink")
          - [邻里](../Page/邻里.md "wikilink")
          - [儿童游戏群体](../Page/儿童游戏群体.md "wikilink")
      - [社会组织](../Page/社会组织.md "wikilink")
      - [科层制](../Page/科层制.md "wikilink")
  - [社会制度方面](../Page/社会制度.md "wikilink")：[亲属](../Page/亲属.md "wikilink")－[婚姻](../Page/婚姻.md "wikilink")－[经济](../Page/经济.md "wikilink")－[政治](../Page/政治.md "wikilink")－[法律](../Page/法律.md "wikilink")－[宗教](../Page/宗教.md "wikilink")－[教育](../Page/教育.md "wikilink")－[文化](../Page/文化.md "wikilink")－[体育](../Page/体育.md "wikilink")
  - [社会过程方面](../Page/社会过程.md "wikilink")：[合作](../Page/合作.md "wikilink")－[竞争](../Page/竞争.md "wikilink")－[战争](../Page/战争.md "wikilink")－[改革](../Page/改革.md "wikilink")－[革命](../Page/革命.md "wikilink")－[社会舆论](../Page/社会舆论.md "wikilink")－[社会价值观](../Page/社会价值观.md "wikilink")－[社会一体化](../Page/社会一体化.md "wikilink")
      - [社会化](../Page/社会化.md "wikilink")
      - [社会角色](../Page/社会角色.md "wikilink")
      - [规范与越轨](../Page/规范与越轨.md "wikilink")
      - [社会变迁](../Page/社会变迁.md "wikilink")
      - [分层与流动](../Page/分层与流动.md "wikilink")
          - [中国社会分层的结构与演变](https://web.archive.org/web/20061116153803/http://www.sachina.edu.cn/Htmldata/longbook/liyi_structure_china/index.html)
      - [城市化](../Page/城市化.md "wikilink")
      - [现代化](../Page/现代化.md "wikilink")
  - [社会问题方面](../Page/社会问题.md "wikilink")：[就业](../Page/就业.md "wikilink")－[民族分裂](../Page/民族分裂.md "wikilink")－[犯罪](../Page/犯罪.md "wikilink")－[环境污染](../Page/污染.md "wikilink")－[人口](../Page/人口.md "wikilink")－[移民](../Page/移民.md "wikilink")－[种族歧视](../Page/种族歧视.md "wikilink")－[暴力](../Page/暴力.md "wikilink")—[贫困](../Page/贫困.md "wikilink")

## 社会学学派

  - [历史社会学派](../Page/历史社会学派.md "wikilink")
      - [人类社会历史发展的基本轮廓](http://www.sachina.edu.cn/Htmldata/longbook/liyi_structure_china/461.html)
  - [社会统计学派](../Page/社会统计学派.md "wikilink")
  - [芝加哥学派](../Page/芝加哥学派.md "wikilink")
  - [法蘭克福學派](../Page/法蘭克福學派.md "wikilink")

## 社会学次領域

  - 社会分层学（或分层社会学）
  - [社会学史](../Page/社会学史.md "wikilink")
  - [社会思想史](../Page/社会思想史.md "wikilink")
  - [社会学方法](../Page/社会学方法.md "wikilink")
      - [社会调查方法](../Page/社会调查方法.md "wikilink")、[社会统计学](../Page/社会统计学.md "wikilink")
  - [比较社会学](../Page/比较社会学.md "wikilink")
  - [数理社会学](../Page/数理社会学.md "wikilink")
  - [实验社会学](../Page/实验社会学.md "wikilink")
  - [教育社会学](../Page/教育社会学.md "wikilink")
  - [政治社會學](../Page/政治社會學.md "wikilink")
  - [军事社会学](../Page/军事社会学.md "wikilink")
  - [发展社会学](../Page/发展社会学.md "wikilink")
  - [福利社会学](../Page/福利社会学.md "wikilink")
  - [经济社会学](../Page/经济社会学.md "wikilink")
  - [文化社会学](../Page/文化社会学.md "wikilink")
      - [艺术社会学](../Page/艺术社会学.md "wikilink")、[知识社会学](../Page/知识社会学.md "wikilink")、[道德社会学](../Page/道德社会学.md "wikilink")
  - [历史社会学](../Page/历史社会学.md "wikilink")
      - [研究中国的历史社会学](../Page/研究中国的历史社会学.md "wikilink")
  - [社会地理学](../Page/社会地理学.md "wikilink")
  - [应用社会学](../Page/应用社会学.md "wikilink")
      - [职业社会学](../Page/职业社会学.md "wikilink")、[工业社会学](../Page/工业社会学.md "wikilink")、[医学社会学](../Page/医学社会学.md "wikilink")、[都市社会学](../Page/都市社会学.md "wikilink")、[乡村社会学](../Page/乡村社会学.md "wikilink")、[家庭社会学](../Page/家庭社会学.md "wikilink")、[环境社会学](../Page/环境社会学.md "wikilink")、[青年社会学](../Page/青年社会学.md "wikilink")、[老年社会学](../Page/老年社会学.md "wikilink")、[犯罪社会学](../Page/犯罪社会学.md "wikilink")、[越轨社会学](../Page/越轨社会学.md "wikilink")、[妇女问题](../Page/妇女问题.md "wikilink")、[种族问题](../Page/种族问题.md "wikilink")、[社会问题](../Page/社会问题.md "wikilink")、[社区](../Page/社区.md "wikilink")、[社会保障](../Page/社会保障.md "wikilink")、[社会工作](../Page/社会工作.md "wikilink")、[微观社会学](../Page/微观社会学.md "wikilink")、[政治社会学](../Page/政治社会学.md "wikilink")、[宗教社会学](../Page/宗教社会学.md "wikilink")、[体育社会学](../Page/体育社会学.md "wikilink")、[发展社会学](../Page/发展社会学.md "wikilink")、[人口社会学](../Page/人口社会学.md "wikilink")、[传播社会学](../Page/传播社会学.md "wikilink")
  - [社会心理学](../Page/社会心理学.md "wikilink")
      - [社会心理学史](../Page/社会心理学史.md "wikilink")、[实验社会心理学](../Page/实验社会心理学.md "wikilink")
  - [公共关系学](../Page/公共关系学.md "wikilink")
  - [组织社会学](../Page/组织社会学.md "wikilink")
  - [人口学](../Page/人口学.md "wikilink")
      - [人口经济学](../Page/人口经济学.md "wikilink")、[人口社会学](../Page/人口社会学.md "wikilink")、[人口学说史](../Page/人口学说史.md "wikilink")、[人口史](../Page/人口史.md "wikilink")、[人口地理学](../Page/人口地理学.md "wikilink")、[人口生态学](../Page/人口生态学.md "wikilink")、[区域人口学](../Page/区域人口学.md "wikilink")、[人口系统工程](../Page/人口系统工程.md "wikilink")、[人口预测学](../Page/人口预测学.md "wikilink")、[人口规划学](../Page/人口规划学.md "wikilink")、[人口政策学](../Page/人口政策学.md "wikilink")、[计划生育学](../Page/计划生育学.md "wikilink")

## 注释

## 参考文献

### 引用

### 来源

  - 中文書籍

<!-- end list -->

  -
  - ［德］韋伯. 《社會學的基本概念》, 廣西師範大學出版社, 2005年, ISBN 978-7-5633-5205-0

  - 葉至誠. 《社會學是什麼》，揚智文化事業股份有限公司，2005年, ISBN 978-957-818-718-4

  - ［美］喬治·瑞澤爾. 《當代社會學理論及其古典根源》, 北京大學出版社, 2005年, ISBN 978-7-301-08798-5

  - ［美］乔纳森·特纳. 《社会学理论的结构》, 桂冠圖書股份有限公司, 2001年, ISBN 978-957-551-498-3

<!-- end list -->

  - 英文書籍

<!-- end list -->

  - John J. Macionis, Sociology (10th Edition), Prentice Hall, 2004,
    ISBN 978-0-13-184918-1

  - Stephen H. Aby, *Sociology: A Guide to Reference and Information
    Sources*. 3rd edn. Littleton, CO, Libraries Unlimited Inc., 2005,
    ISBN 1563089475

  - Anthony Giddens, *Conversations with Anthony Giddens*, Polity,
    Cambridge, 1998. （對[古典社會學作很好的介紹](../Page/古典社會學.md "wikilink")）

  - Anthony Giddens, *Sociology*, Polity, Cambridge

  - Anthony Giddens, *Human Societies: Introduction Reading in
    Sociology*

  - Robert A. Nisbet, *The Sociological Tradition*, London, Heinemann
    Educational Books, 1967, ISBN 978-1-56000-667-1

  - Evan Willis, *The Sociological Quest: An introduction to the study
    of social life*, 3rd edn, New Brunswick, NJ, Rutgers University
    Press, 1996, ISBN 978-0-8135-2367-5

  - Andrey , Artemy Malkov, and Daria Khaltourina, *Introduction to
    Social Macrodynamics*, Moscow: URSS, 2006. ISBN 978-5-484-00414-0
    [1](http://urss.ru/cgi-bin/db.pl?cp=&lang=en&blang=en&list=14&page=Book&id=34250).

  - Li Yi. 2005. The Structure and Evolution of Chinese Social
    Stratification。University Press of America。ISBN 978-0-7618-3331-4

  - [Samuel William Bloom](../Page/Samuel_William_Bloom.md "wikilink"),
    *The Word as Scalpel: A History of Medical Sociology*, Oxford
    University Press 2002

  - [雷蒙·布东](../Page/雷蒙·布东.md "wikilink") *A Critical Dictionary of
    Sociology*. Chicago: University of Chicago Press, 1989

  - [Deegan, Mary Jo](../Page/Mary_Jo_Deegan.md "wikilink"), ed. *Women
    in Sociology: A Bio-Bibliographical Sourcebook*, New York: Greenwood
    Press, 1991.

  - , *A History of Sociology in Britain: Science, Literature, and
    Society*, Oxford University Press 2004

  - [Barbara Laslett](../Page/Barbara_Laslett.md "wikilink") (editor),
    (editor), *Feminist Sociology: Life Histories of a Movement*,
    Rutgers University Press 1997

  - Levine, Donald N. *Visions of the Sociological Tradition,*
    University Of Chicago Press, 1995: ISBN 978-0-226-47547-9

  - [T.N. Madan](../Page/T.N._Madan.md "wikilink"), *Pathways :
    approaches to the study of society in India*. New Delhi: Oxford
    University Press, 1994

  - [Wiggershaus, Rolf](../Page/Rolf_Wiggershaus.md "wikilink"), *The
    Frankfurt School : its history, theories and political
    significance*, Polity Press, 1994.

<!-- end list -->

  - 德文書籍

<!-- end list -->

  - [Bálint Balla](../Page/Bálint_Balla.md "wikilink"), *Soziologie und
    Geschichte. Geschichte der Soziologie*, Hamburg (Reinhold Krämer)
    1995

  - Wolfgang Bonß, *Die Einübung des Tatsachenblicks. Zur Struktur und
    Veränderung empirischer Sozialforschung*, Frankfurt am Main:
    Suhrkamp, 1982, 324 p.

  - [Hermann Korte](../Page/Hermann_Korte_\(Soziologe\).md "wikilink"),
    *Einführung in die Geschichte der Soziologie*, 8. überarb. Auflage,
    Wiesbaden (VS Verlag für Sozialwissenschaften) 2006, ISBN
    978-3-531-14774-1

  - (Hg.), *Geschichte der Soziologie. Studien zur kognitiven, sozialen
    und historischen Identität einer Disziplin*, 4 Bände, Frankfurt am
    Main (Suhrkamp) 1981, ISBN 978-3-518-07967-6

  - [Heinz Maus](../Page/Heinz_Maus.md "wikilink"), Geschichte der
    Soziologie; in: Werner Ziegenfuß (Hg.), Handbuch der Soziologie.
    Stuttgart: Enke, 1956: 1-120

  - ders., *Bericht über die Soziologie in Deutschland 1933-1945*; in: ,
    11 (1959) 1: 72-99

  - ders., *Zur Vorgeschichte der empirischen Sozialforschung*; in:
    (Hg.), *Handbuch der empirischen Sozialforschung* Bd. I, Stuttgart:
    Enke, ²1967: 18-37

  - Reinhard Müller, Marienthal. Das Dorf – Die Arbeitslosen – Die
    Studie. Innsbruck-Wien-Bozen: StudienVerlag, 2008, 423 p. (ISBN
    978-3-7065-4377-7)

  - Rolf Wiggershaus, *Die Frankfurter Schule*, München: dtv 2001, ISBN
    978-3-423-30174-9

  - , Zur Geschichte der Soziographie \[1933\]; in:  / der., Die
    Arbeitslosen von Marienthal. Ein soziographischer Versuch über die
    Wirkungen langandauernder Arbeitslosigkeit \[...\]; m. Vorw. v.
    [保罗·F·拉扎斯菲尔德](../Page/保罗·F·拉扎斯菲尔德.md "wikilink"), Leipzig 1933;
    Allensbach/Bonn 1960; Frankfurt am Main (Suhrkamp) 1975 \[= ed.
    suhrkamp 769\] 113-142 \[und\] 145-148

<!-- end list -->

  - 法文書籍

<!-- end list -->

  - [雷蒙·阿隆](../Page/雷蒙·阿隆.md "wikilink"), *Les étapes de la pensée
    sociologique*, Paris, Gallimard, 1976

  - , *La construction de la sociologie*, Paris, PUF, 1991

  - [Pierre-Jean Simon](../Page/Pierre-Jean_Simon.md "wikilink"),
    *Histoire de la sociologie*. Paris : PUF, pp. 7–24, 1991

  - [Charles-Henry Cuin](../Page/Charles-Henry_Cuin.md "wikilink"),
    [François Gresle](../Page/François_Gresle.md "wikilink"), *Histoire
    de la sociologie*, tome 1 : *avant 1918*, tome 2 : *depuis 1918*,
    Nouvelle édition, Éditions La découverte, Collection Repères

  - [Gilles Ferréol](../Page/Gilles_Ferréol.md "wikilink") & alii,
    *Histoire de la pensée sociologique*, Colin Cursus, 1994

  - [Jean Étienne](../Page/Jean_Étienne.md "wikilink"), [Henri
    Mendras](../Page/Henri_Mendras.md "wikilink"), *Les grands auteurs
    en sociologie*, Paris, Hatier, 1996

  - [Karl Van Meter](../Page/Karl_Van_Meter.md "wikilink"), (dir.) *La
    sociologie*, Larousse, coll. Textes essentiels, 1999

  - [Michel Lallement](../Page/Michel_Lallement.md "wikilink"),
    *Histoire des idées sociologiques*, Tome 1 : *Des origines à Weber*,
    Nathan, coll. Circa, 1994

  - [Jean-Pierre Delas](../Page/Jean-Pierre_Delas.md "wikilink"), [Bruno
    Milly](../Page/Bruno_Milly.md "wikilink"), *Histoire des pensées
    sociologiques*, Sirey coll. Synthèse +, 1997

  - [Jean-Pierre
    Durand](../Page/Jean-Pierre_Durand_\(sociologue\).md "wikilink"), ,
    *Sociologie contemporaine*, Vigot, rééd. 1997

  - [Michel Forsé](../Page/Michel_Forsé.md "wikilink") & alii, *Histoire
    de la pensée sociologique - Les grands classiques*, Armand Colin,
    1998

<!-- end list -->

  - 波蘭文書籍

<!-- end list -->

  - Piotr Sztompka, Socjologia, Znak, 2002, ISBN 978-83-240-0218-4

## 研究書目

  - [李明堃](../Page/李明堃.md "wikilink")、[黃紹倫](../Page/黃紹倫.md "wikilink")
    編：《社會學新論》（香港：商務印書館，1992年）.
  - 胡偉：《戰後法國社會學的發展》（香港：三聯書店【香港】有限公司，1988年）.
  - [清水幾太郎](../Page/清水幾太郎.md "wikilink") (1978年)
    『オーギュスト・コント――社会学とは何か』（岩波書店［岩波新書］,
    1978年）.
  - [富永健一](../Page/富永健一.md "wikilink") (2008年) 『思想としての社会学』（新曜社, 2008年）.

## 外部链接

  - 中國大陆資料來源

<!-- end list -->

  - [社会学视野网](https://web.archive.org/web/20170716210610/http://www.sociologyol.org/)
  - [中國社會學網](https://web.archive.org/web/20051206141404/http://www.sociology.cass.net.cn/shxw/history/default.htm)
  - [社会学人类学中国网](http://www.sachina.edu.cn)
  - [天睿网（原社会学吧网）](https://web.archive.org/web/20090606133515/http://www.tianroo.com/)
  - [社会学博客网](https://web.archive.org/web/20170305034902/http://blog.sociology.org.cn/)
  - 《[新华文摘](http://www.xinhuawz.com/)》
  - 《中国社会科学》
  - 《[社会学研究](https://web.archive.org/web/20100822234923/http://www.sociology.cass.net.cn/shxs/xskw/shxyj/default.htm)》

<!-- end list -->

  - 台灣資料來源

<!-- end list -->

  - [台灣社會學會](http://proj3.sinica.edu.tw/~tsa/)
      - [台灣社會學刊](http://proj3.sinica.edu.tw/~tsa/modules/tadnews/index.php?ncsn=14)
  - [台灣中央研究院社會學研究所](http://www.ios.sinica.edu.tw/)

<!-- end list -->

  - 自學課程

<!-- end list -->

  - [社會學入門的免費講義，加拿大特倫特大學](http://www.trentu.ca/trentradio/tklassen/)
  - [社會學課程入門的免費講義,
    東卡羅萊納大學](http://core.ecu.edu/soci/juskaa/SOCI2110/soci1.htm)

<!-- end list -->

  - 其他資料來源

<!-- end list -->

  - [美國宗教數據檔案](http://www.thearda.com)
  - [美國社會學協會](http://www.asanet.org/)
  - [國際社會學協會](http://www.ucm.es/info/isa/)
  - [社會研究的方法](http://gsociology.icaap.org/methods/)
  - [SocioSite - 社會科學系訊系統](http://www.sociosite.net/)
  - [社會理論及理論學家](http://www.sociologyprofessor.com/)
  - [社會學的詳盡介紹](http://www.sociolog.com/)
  - [Theory.org.uk](http://www.theory.org.uk) David Gauntlett
  - \[[http://www3.telus.net/public/t5837479/\]社会现象](http://www3.telus.net/public/t5837479/%5D社会现象)
    王滕

## 参见

  - [社會學家列表](../Page/社會學家列表.md "wikilink")
  - [心理学](../Page/心理学.md "wikilink")
  - [情报学](../Page/情报学.md "wikilink")
  - [政治学](../Page/政治学.md "wikilink")
  - [社会主义](../Page/社会主义.md "wikilink")
  - [社会发展规律](../Page/社会发展规律.md "wikilink")
  - [社会结构](../Page/社会结构.md "wikilink")
  - [人才学](../Page/人才学.md "wikilink")
  - [國際社會學協會](../Page/國際社會學協會.md "wikilink")
  - [社會科學](../Page/社會科學.md "wikilink")
  - [社会人格学](../Page/社会人格学.md "wikilink")

{{-}}

[社会学](../Category/社会学.md "wikilink")
[Category:日語借詞](../Category/日語借詞.md "wikilink")

1.

2.

3.

4.

5.
6.

7.

8.  邱澤奇，《社會學是什麼》，天地圖書，ISBN 978-988-201-450-3

9.

10.

11.

12.

13. H. Mowlana (2001). "Information in the Arab World", *Cooperation
    South Journal* **1**.

14. Dr. S. W. Akhtar (1997). "The Islamic Concept of Knowledge",
    *Al-Tawhid: A Quarterly Journal of Islamic Thought & Culture* **12**
    (3).

15. Amber Haque (2004), "Psychology from Islamic Perspective:
    Contributions of Early Muslim Scholars and Challenges to
    Contemporary Muslim Psychologists", *Journal of Religion and Health*
    **43** (4): 357–377 \[375\].

16.

17.

18.

19.

20. *Des Manuscrits de Sieyès. 1773–1799*, Volumes I and II, published
    by Christine Fauré, Jacques Guilhaumou, Jacques Vallier et Françoise
    Weil, Paris, Champion, 1999 and 2007

21. See also Christine Fauré and Jacques Guilhaumou, *Sieyès et le
    non-dit de la sociologie: du mot à la chose*, in *Revue d'histoire
    des sciences humaines*, Numéro 15, novembre 2006: Naissances de la
    science sociale.

22.
23.
24.
25.

26.

27.

28.

29.

30.

31.
32.

33.

34.

35.

36.

37.

38.

39.

40.

41.
42. Camic, Charles. 1992. "Reputation and Predecessor Selection: Parsons
    and the Institutionalists", American Sociological Review, Vol. 57,
    No. 4 (Aug., 1992), pp. 421-445

43.

44.

45.

46.

47. 侯钧生：西方社会学理论教程.南开大学出版社.p3-7.2006

48.
49. Bose,Christine. 2012. "Intersectionality and Global Gender
    Inequality" Gender & Society 26(1):67-72