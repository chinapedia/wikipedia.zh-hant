[海珠区](../Page/海珠区.md "wikilink"){{.w}}[天河区](../Page/天河区.md "wikilink"){{.w}}[白云区](../Page/白云区_\(广州市\).md "wikilink"){{.w}}[黄埔区](../Page/黄埔区.md "wikilink"){{.w}}[番禺区](../Page/番禺区.md "wikilink"){{.w}}[花都区](../Page/花都区.md "wikilink"){{.w}}[南沙区](../Page/南沙区.md "wikilink"){{.w}}[从化区](../Page/从化区.md "wikilink"){{.w}}[增城区](../Page/增城区.md "wikilink")

|group2 = [深圳市](../Page/深圳市.md "wikilink") |list2 =
[福田区](../Page/福田区.md "wikilink"){{.w}}[罗湖区](../Page/罗湖区.md "wikilink"){{.w}}[南山区](../Page/南山区_\(深圳市\).md "wikilink"){{.w}}[宝安区](../Page/宝安区.md "wikilink"){{.w}}[龙岗区](../Page/龙岗区.md "wikilink"){{.w}}[盐田区](../Page/盐田区.md "wikilink"){{.w}}[坪山区](../Page/坪山区.md "wikilink"){{.w}}[龙华区](../Page/龙华区_\(深圳市\).md "wikilink"){{.w}}[光明区](../Page/光明区.md "wikilink"){{.w}}[大鹏新区](../Page/大鹏新区.md "wikilink")\*{{.w}}[深汕特别区](../Page/深汕特别合作区.md "wikilink")\*
}}

|group3 = [地级市](../Page/地级市.md "wikilink") |list3 =
[武江区](../Page/武江区.md "wikilink"){{.w}}[曲江区](../Page/曲江区.md "wikilink"){{.w}}[乐昌市](../Page/乐昌市.md "wikilink"){{.w}}[南雄市](../Page/南雄市.md "wikilink"){{.w}}[始兴县](../Page/始兴县.md "wikilink"){{.w}}[仁化县](../Page/仁化县.md "wikilink"){{.w}}[翁源县](../Page/翁源县.md "wikilink"){{.w}}[新丰县](../Page/新丰县.md "wikilink"){{.w}}[乳源瑶族自治县](../Page/乳源瑶族自治县.md "wikilink")

|group2 = [珠海市](../Page/珠海市.md "wikilink") |list2 =
[香洲区](../Page/香洲区.md "wikilink"){{.w}}[斗门区](../Page/斗门区.md "wikilink"){{.w}}[金湾区](../Page/金湾区.md "wikilink")

|group3 = [汕头市](../Page/汕头市.md "wikilink") |list3 =
[金平区](../Page/金平区.md "wikilink"){{.w}}[龙湖区](../Page/龙湖区.md "wikilink"){{.w}}[濠江区](../Page/濠江区.md "wikilink"){{.w}}[潮阳区](../Page/潮阳区.md "wikilink"){{.w}}[潮南区](../Page/潮南区.md "wikilink"){{.w}}[澄海区](../Page/澄海区.md "wikilink"){{.w}}[南澳县](../Page/南澳县.md "wikilink")

|group4 = [佛山市](../Page/佛山市.md "wikilink") |list4 =
[禅城区](../Page/禅城区.md "wikilink"){{.w}}[南海区](../Page/南海区.md "wikilink"){{.w}}[顺德区](../Page/顺德区.md "wikilink"){{.w}}[三水区](../Page/三水区.md "wikilink"){{.w}}[高明区](../Page/高明区.md "wikilink")

|group5 =[江门市](../Page/江门市.md "wikilink") |list5 =
[蓬江区](../Page/蓬江区.md "wikilink"){{.w}}[江海区](../Page/江海区.md "wikilink"){{.w}}[新会区](../Page/新会区.md "wikilink"){{.w}}[台山市](../Page/台山市.md "wikilink"){{.w}}[开平市](../Page/开平市.md "wikilink"){{.w}}[鹤山市](../Page/鹤山市.md "wikilink"){{.w}}[恩平市](../Page/恩平市.md "wikilink")

|group6 = [湛江市](../Page/湛江市.md "wikilink") |list6 =
[赤坎区](../Page/赤坎区.md "wikilink"){{.w}}[霞山区](../Page/霞山区.md "wikilink"){{.w}}[坡头区](../Page/坡头区.md "wikilink"){{.w}}[麻章区](../Page/麻章区.md "wikilink"){{.w}}[廉江市](../Page/廉江市.md "wikilink"){{.w}}[雷州市](../Page/雷州市.md "wikilink"){{.w}}[吴川市](../Page/吴川市.md "wikilink"){{.w}}[遂溪县](../Page/遂溪县.md "wikilink"){{.w}}[徐闻县](../Page/徐闻县.md "wikilink")

|group7 = [茂名市](../Page/茂名市.md "wikilink") |list7 =
[茂南区](../Page/茂南区.md "wikilink"){{.w}}[电白区](../Page/电白区.md "wikilink"){{.w}}[高州市](../Page/高州市.md "wikilink"){{.w}}[化州市](../Page/化州市.md "wikilink"){{.w}}[信宜市](../Page/信宜市.md "wikilink")

|group8 = [肇庆市](../Page/肇庆市.md "wikilink") |list8 =
[端州区](../Page/端州区.md "wikilink"){{.w}}[鼎湖区](../Page/鼎湖区.md "wikilink"){{.w}}[高要区](../Page/高要区.md "wikilink"){{.w}}[四会市](../Page/四会市.md "wikilink"){{.w}}[广宁县](../Page/广宁县.md "wikilink"){{.w}}[怀集县](../Page/怀集县.md "wikilink"){{.w}}[封开县](../Page/封开县.md "wikilink"){{.w}}[德庆县](../Page/德庆县.md "wikilink")

|group9 = [惠州市](../Page/惠州市.md "wikilink") |list9 =
[惠城区](../Page/惠城区.md "wikilink"){{.w}}[惠阳区](../Page/惠阳区.md "wikilink"){{.w}}[博罗县](../Page/博罗县.md "wikilink"){{.w}}[惠东县](../Page/惠东县.md "wikilink"){{.w}}[龙门县](../Page/龙门县.md "wikilink")

|group10 = [梅州市](../Page/梅州市.md "wikilink") |list10 =
[梅江区](../Page/梅江区.md "wikilink"){{.w}}[梅县区](../Page/梅县区.md "wikilink"){{.w}}[兴宁市](../Page/兴宁市.md "wikilink"){{.w}}[大埔县](../Page/大埔县.md "wikilink"){{.w}}[丰顺县](../Page/丰顺县.md "wikilink"){{.w}}[五华县](../Page/五华县.md "wikilink"){{.w}}[平远县](../Page/平远县.md "wikilink"){{.w}}[蕉岭县](../Page/蕉岭县.md "wikilink")

|group11 = [汕尾市](../Page/汕尾市.md "wikilink") |list11 =
[城区](../Page/城区_\(汕尾市\).md "wikilink"){{.w}}[陆丰市](../Page/陆丰市.md "wikilink"){{.w}}[海丰县](../Page/海丰县.md "wikilink"){{.w}}[陆河县](../Page/陆河县.md "wikilink")

|group12 = [河源市](../Page/河源市.md "wikilink") |list12 =
[源城区](../Page/源城区.md "wikilink"){{.w}}[紫金县](../Page/紫金县.md "wikilink"){{.w}}[龙川县](../Page/龙川县.md "wikilink"){{.w}}[连平县](../Page/连平县.md "wikilink"){{.w}}[和平县](../Page/和平县.md "wikilink"){{.w}}[东源县](../Page/东源县.md "wikilink")

|group13 = [阳江市](../Page/阳江市.md "wikilink") |list13 =
[江城区](../Page/江城区.md "wikilink"){{.w}}[阳东区](../Page/阳东区.md "wikilink"){{.w}}[阳春市](../Page/阳春市.md "wikilink"){{.w}}[阳西县](../Page/阳西县.md "wikilink")
|group14 = [清远市](../Page/清远市.md "wikilink") |list14 =
[清城区](../Page/清城区.md "wikilink"){{.w}}[清新区](../Page/清新区.md "wikilink"){{.w}}[英德市](../Page/英德市.md "wikilink"){{.w}}[连州市](../Page/连州市.md "wikilink"){{.w}}[佛冈县](../Page/佛冈县.md "wikilink"){{.w}}[阳山县](../Page/阳山县.md "wikilink"){{.w}}[连山壮族瑶族自治县](../Page/连山壮族瑶族自治县.md "wikilink"){{.w}}[连南瑶族自治县](../Page/连南瑶族自治县.md "wikilink")

|group15 = [东莞市](../Page/东莞市.md "wikilink") |list15 =
[*无县级行政区*](../Page/:T:东莞市行政区划.md "wikilink")

|group16 = [中山市](../Page/中山市.md "wikilink") |list16
=[*无县级行政区*](../Page/:T:中山市行政区划.md "wikilink")

|group17 = [潮州市](../Page/潮州市.md "wikilink") |list17 =
[湘桥区](../Page/湘桥区.md "wikilink"){{.w}}[潮安区](../Page/潮安区.md "wikilink"){{.w}}[饶平县](../Page/饶平县.md "wikilink")

|group18 = [揭阳市](../Page/揭阳市.md "wikilink") |list18 =
[榕城区](../Page/榕城区.md "wikilink"){{.w}}[揭东区](../Page/揭东区.md "wikilink"){{.w}}[普宁市](../Page/普宁市.md "wikilink"){{.w}}[揭西县](../Page/揭西县.md "wikilink"){{.w}}[惠来县](../Page/惠来县.md "wikilink")

|group19 = [云浮市](../Page/云浮市.md "wikilink") |list19 =
[云城区](../Page/云城区.md "wikilink"){{.w}}[云安区](../Page/云安区.md "wikilink"){{.w}}[罗定市](../Page/罗定市.md "wikilink"){{.w}}[新兴县](../Page/新兴县.md "wikilink"){{.w}}[-{zh-hant:鬱南;zh-hans:郁南}-县](../Page/郁南县.md "wikilink")
}}

|belowstyle = text-align: left; font-size: 80%; |below =
注1：[广州市](../Page/广州市.md "wikilink")、[深圳市为](../Page/深圳市.md "wikilink")[副省级城市](../Page/副省级城市.md "wikilink")。
注2：[东莞市](../Page/东莞市.md "wikilink")、[中山市的行政区划是地级市](../Page/中山市.md "wikilink")、镇（街道）两级编-{制}-，没有县级编-{制}-。
注3：中华人民共和国政府将[东沙群岛划归](../Page/东沙群岛.md "wikilink")[广东省](../Page/广东省.md "wikilink")[陆丰市](../Page/陆丰市.md "wikilink")[碣石镇管辖](../Page/碣石镇.md "wikilink")，但未实际统治，参见[兩岸分治](../Page/兩岸分治.md "wikilink")。
注4：[深汕特别区由深圳市按照普通区管理](../Page/深汕特别合作区.md "wikilink")，居民户籍、GDP等均纳入深圳市。
参见：[中华人民共和国县级以上行政区列表](../Page/中华人民共和国县级以上行政区列表.md "wikilink")、[广东省乡级以上行政区列表](../Page/广东省乡级以上行政区列表.md "wikilink")。
}}<noinclude>

</noinclude>

[Category:中华人民共和国各省级行政区行政区划模板](../Category/中华人民共和国各省级行政区行政区划模板.md "wikilink")
[\*](../Category/中华人民共和国广东省行政区划.md "wikilink")
[中华人民共和国广东省行政区划模板](../Category/中华人民共和国广东省行政区划模板.md "wikilink")