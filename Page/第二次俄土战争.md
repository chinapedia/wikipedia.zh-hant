**第二次俄土战争**发生于1686年至1700年间。1683年，奥斯曼帝国联合对[哈布斯堡王朝不满的一部分](../Page/哈布斯堡王朝.md "wikilink")[匈牙利封建主对奥地利发动战争](../Page/匈牙利.md "wikilink")。1683年7月土军围困[维也纳](../Page/维也纳.md "wikilink")。1684年奥地利、波兰和威尼斯结成反奥斯曼帝国的“神圣同盟”，1686年俄国加盟。

1686年-1688年，奥军发起反击，先后占领土军控制的[布达](../Page/布达.md "wikilink")、匈牙利东部、[斯拉沃尼亚](../Page/斯拉沃尼亚.md "wikilink")、[贝尔格莱德等地](../Page/贝尔格莱德.md "wikilink")。1689年在[维丁城附近的](../Page/维丁城.md "wikilink")[多瑙河上击败土耳其海军](../Page/多瑙河.md "wikilink")。

1695年及1696年俄皇[彼得一世兩度进攻](../Page/彼得大帝.md "wikilink")[亚速海](../Page/亚速海.md "wikilink")，被奥斯曼帝国与其属国[克里米亚汗国的联军击败](../Page/克里米亚汗国.md "wikilink")。

1697年9月奥军在[蒂萨河畔进行的](../Page/蒂萨河.md "wikilink")中大胜土军。同年，彼得派俄军占领了[顿河河口](../Page/頓河_\(俄羅斯\).md "wikilink")。

1699年奥地利、波兰、威尼斯与奥斯曼帝国签订《[卡尔洛维茨和约](../Page/卡尔洛维茨和约.md "wikilink")》，奥地利获得匈牙利、[斯拉沃尼亚](../Page/斯拉沃尼亚.md "wikilink")、[特兰西瓦尼亚和](../Page/特兰西瓦尼亚.md "wikilink")[克罗地亚大片领土](../Page/克罗地亚.md "wikilink")，波兰获得[第聂伯河西岸](../Page/第聂伯河.md "wikilink")[乌克兰南部和](../Page/乌克兰.md "wikilink")[波多里亚](../Page/波多里亚.md "wikilink")，威尼斯获得[摩里亚和](../Page/摩里亚.md "wikilink")[爱琴海中的土属各岛](../Page/爱琴海.md "wikilink")。

1700年俄国与奥斯曼帝国签订《伊斯坦布尔和约》（《君士坦丁堡和约》），俄国获得[亚速要塞](../Page/亚速.md "wikilink")，在[黑海建立了第一个出海口](../Page/黑海.md "wikilink")。

## 参考文献

<div class="references-small">

<references />

</div>

## 参考条目

  - [俄土战争](../Page/俄土战争.md "wikilink")
  - [奥土战争](../Page/奥土战争.md "wikilink")
  - [威尼斯土耳其战争](../Page/威尼斯土耳其战争.md "wikilink")

[Category:俄土战争](../Category/俄土战争.md "wikilink")