## 摘要

这是一张《[星际旅行：航海家号](../Page/星际旅行：航海家号.md "wikilink")》里面星舰的视频截图：[联邦星舰航海家号
(NCC-74656)](../Page/联邦星舰航海家号_\(NCC-74656\).md "wikilink")。来自“”一集。它使用于辨认、评论和注释这艘虚构的“联邦星舰航海家号”。

## 在[联邦星舰航海家号 (NCC-74656)中使用之依据](../Page/联邦星舰航海家号_\(NCC-74656\).md "wikilink")

1.  There exists no free alternative, nor can one be created. the USS
    Voyager is a fictional starship, all images of it are necessarily
    copyrighted.
2.  This image respects commercial opportunties - a cropped screenshot
    cannot meaningfully compete with a 44 minute episode.
3.  The image represents a minimal use - it is a cropped screenshot -
    less than a single frame from an episode that has over 50 000
    frames. It is of a reasonable low resolution.
4.  The image has been previously published, in the broadcasted, as well
    as sold on DVD Star Trek Voyager episode "Emanations"
5.  The image is of a major starship from a culturally significant
    series of television shows. It is encyclopaedic.
6.  The image generally meets the image use policy.
7.  The image is used in at least one article, [联邦星舰航海家号
    (NCC-74656)](../Page/联邦星舰航海家号_\(NCC-74656\).md "wikilink").
8.  The image contributes strongly to the article. As a fictional
    starship, the subject is more easily visualised.
9.  The image is used only in the article namespace.
10. The image is from <http://memory-alpha.org/en/wiki/USS_Voyager> and
    the copyright is owned by [Paramount
    Pictures](../Page/派拉蒙影业.md "wikilink").

## 原出处

<http://memory-alpha.org/en/wiki/Image:Intrepid_class_top_quarter_aft.jpg>

## 许可协议