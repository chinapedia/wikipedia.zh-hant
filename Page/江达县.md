**江达县**（）是[中華人民共和國](../Page/中華人民共和國.md "wikilink")[西藏自治區](../Page/西藏自治區.md "wikilink")[昌都市東北部的一個](../Page/昌都市.md "wikilink")[縣](../Page/縣.md "wikilink")。該縣為1959年由江達宗和鄧柯宗合併而成立。盛產[銅礦](../Page/銅礦.md "wikilink")。交通有[川藏公路經過](../Page/川藏公路.md "wikilink")。

## 交通

  - 国道317线横穿全县境143公里，联接着岗托、同普、江达、卡贡、青泥洞5个乡镇所在地。
  - 县道1条，14公里；乡道7条，共418公里。

## 行政区划

下辖1个镇，2个区，18个乡，152个村民委员会：\[1\] 。

## 参考资料

  - A. Gruschke: *The Cultural Monuments of Tibet’s Outer Provinces:
    Kham - Volume 1. The Xizang Part of Kham (TAR)*, White Lotus Press,
    Bangkok 2004. ISBN 974-480-049-6
  - Tsering Shakya: *The Dragon in the Land of Snows. A History of
    Modern Tibet Since 1947*, London 1999, ISBN 0-14-019615-3

[江达县](../Category/江达县.md "wikilink")
[Category:昌都地区县份](../Category/昌都地区县份.md "wikilink")
[Category:国家级贫困县](../Category/国家级贫困县.md "wikilink")

1.