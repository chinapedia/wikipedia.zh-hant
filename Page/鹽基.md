在[香港和](../Page/香港.md "wikilink")[台湾](../Page/台湾.md "wikilink")，**鹽基**或**鹼基**，有時也稱做**鹼**\[1\]（儘管「鹼」具有多種意義）。這是根據[布朗斯特-劳里學說關於酸和鹽基的部份](../Page/酸鹼理論.md "wikilink")，鹽基可以簡單想像成吸收[質子的物質](../Page/質子.md "wikilink")。額外的定義包括提供[孤立電子對](../Page/孤立電子對.md "wikilink")（由[劉易斯提出](../Page/劉易斯.md "wikilink")），以及是氫氧根離子的來源（由[阿伦尼乌斯提出](../Page/阿伦尼乌斯.md "wikilink")）。

鹽基也可想像為與[酸化學相對](../Page/酸.md "wikilink")。酸和鹽基的反應稱為[中和作用](../Page/中和作用.md "wikilink")。鹽基可想像為與[酸化學相對是因為酸增加了](../Page/酸.md "wikilink")[水合氫離子](../Page/水合氫離子.md "wikilink")（H<sub>3</sub>O<sup>+</sup>）在水中的[濃度](../Page/濃度.md "wikilink")，反之鹽基降低這種濃度。鹽基與酸反應生成[水和](../Page/水.md "wikilink")[鹽](../Page/鹽.md "wikilink")。鹽基的一般性質包括：

  - **味道**：[苦味](../Page/苦味.md "wikilink")（相對於[酸的](../Page/酸.md "wikilink")[酸味以及](../Page/酸味.md "wikilink")[醛和](../Page/醛.md "wikilink")[酮的](../Page/酮.md "wikilink")[甜味](../Page/甜味.md "wikilink")）
  - **觸感**：具黏滑和像肥皂的感覺
  - **活潑性**：對有機物呈[苛性](../Page/苛性.md "wikilink")，與酸性或可還原的物質劇烈反應
  - **導電性**：水溶液以及熔融鹽基電離而導電
  - 將紅色[石蕊試紙變為藍色](../Page/石蕊試紙.md "wikilink")

## 鹽基的定義

法國化學家於1754年製定了鹽基這一概念。他注意到當時的酸大多是揮發性液體（例如[乙酸](../Page/乙酸.md "wikilink")），與特定物質化合便會轉變為固態鹽。這些特定物質便是鹽的「*根基*」\[2\]並因此得名。[鹼](../Page/鹼.md "wikilink")（Alkali）是鹽基的一個特殊例子，會在水溶液中釋出[氫氧根離子](../Page/氫氧根離子.md "wikilink")。**阿累尼烏斯鹽基**是指水溶而水溶液[pH值經常大於](../Page/pH值.md "wikilink")7的鹽基。還有其他更廣義和深層的[酸鹼定義](../Page/酸鹼理論.md "wikilink")。

  - 阿瑞尼士鹽基（Arrhenius base）
    氫氧根離子的來源：
      -
        NaOH → Na<sup>+</sup> + OH<sup>−</sup>
  - 布忍斯特鹽基（Brønsted base）
    吸收質子的物質，下式中**B**<sup>−</sup>為鹽基：
      -
        **B** + H<sup>+</sup> → B<sup>+</sup>H
        **B**<sup>−</sup> + H<sup>+</sup> → BH
  - 路易士鹽基（Lewis base）
    孤立電子對的提供者，下式中**B**為鹽基：

[Lewis_acid-base_equilibrium.png](https://zh.wikipedia.org/wiki/File:Lewis_acid-base_equilibrium.png "fig:Lewis_acid-base_equilibrium.png")

## 鹽基和pH值

量度（不純的）水的[pH值](../Page/pH值.md "wikilink")，便是測試它的[酸性](../Page/酸性.md "wikilink")。純水中，大約每千萬個水分子便有一個根據以下方程式電離為水合氫離子和氫氧根離子：

  -
    2H<sub>2</sub>O(l) → H<sub>3</sub>O<sup>+</sup>(aq) +
    OH<sup>−</sup>(aq)

[濃度即是以](../Page/濃度.md "wikilink")[摩爾濃度計算](../Page/摩爾濃度.md "wikilink")（*M*或[摩爾每立方分米](../Page/摩爾.md "wikilink")（mol/
dm<sup>3</sup>））水合氫離子和氫氧根離子的濃度；是為水的[離解常數](../Page/離解常數.md "wikilink")，數值為10<sup>−7</sup>
*M*。pH值定義為−log \[H<sub>3</sub>O<sup>+</sup>\]；因此，純水的pH值是7。（這些數據均取於23 °C，並於不同溫度時有所差別。）

鹽基接收（或去除）溶液中的水合氫離子，或釋放氫氧根離子到溶液中。兩者均降低了水合氫離子在水中的濃度，從而提升pH值。相反，酸釋放水合氫離子或接收氫氧根離子而降低pH值。

例如，1莫耳的氫氧化鈉（40
[克](../Page/克.md "wikilink")）溶於1公升的水中，氫氧根離子的濃度便是1摩爾每公升。所以氫離子濃度為每公升\[H<sup>+</sup>\] = 10<sup>−14</sup> 莫耳，而pH值 = −log 10<sup>−14</sup> = 14.

**鹼度係數**（**pK<sub>b</sub>**）是鹼性的測量，並和[酸度係數](../Page/酸度係數.md "wikilink")（**pK<sub>a</sub>**）有以下關係pK<sub>a</sub> + pK<sub>b</sub> = 14.

[鹼濃度是量度溶液相對碳酸鹽或碳酸氫鹽中和酸的能力](../Page/鹼濃度.md "wikilink")。

## 與酸的中和作用

當鹽基溶解於水中，[氫氧化鈉离解為氫氧根離子和鈉離子](../Page/氫氧化鈉.md "wikilink")：

  -
    NaOH → Na<sup>+</sup> + OH<sup>−</sup>

同樣地，[氯化氫於水中生成水合氫離子和氯離子](../Page/氯化氫.md "wikilink")：

  -
    HCl + H<sub>2</sub>O → H<sub>3</sub>O<sup>+</sup> + Cl<sup>−</sup>

當兩種溶液混合，水合氫離子（H<sub>3</sub>O<sup>+</sup>）和氫氧根離子（OH<sup>−</sup>）化合生成水分子：

  -
    H<sub>3</sub>O<sup>+</sup> + OH<sup>−</sup> → 2 H<sub>2</sub>O

如果以相同份量的氫氧化鈉溶液和氫氯酸混合，鹽基和酸會恰好完全中和，於溶液中生成氯化鈉，即[食鹽](../Page/食鹽.md "wikilink")。

弱鹽基，如蘇打和蛋白，用於酸洩漏事件中的中和劑。以強鹽基如[氫氧化鈉或](../Page/氫氧化鈉.md "wikilink")[氫氧化鉀中和會引起劇烈的放熱反應](../Page/氫氧化鉀.md "wikilink")，同時鹽基也製造了和酸洩漏同等的破壞。

## 非金屬氧化物的鹼性

[碳酸鈉和](../Page/碳酸鈉.md "wikilink")[氨亦是鹽基](../Page/氨.md "wikilink")，但它們都沒有氫氧根離子（OH<sup>−</sup>）。這是因為兩種都在溶解時接收氫離子（H<sup>+</sup>）：

  -
    Na<sub>2</sub>CO<sub>3</sub> + H<sub>2</sub>O → 2 Na<sup>+</sup> +
    HCO<sub>3</sub><sup>−</sup> + OH<sup>−</sup>
    NH<sub>3</sub> + H<sub>2</sub>O → NH<sub>4</sub><sup>+</sup> +
    OH<sup>−</sup>

## 鹽基的強弱

強鹽基是一種會完全[水解的鹽基](../Page/水解.md "wikilink")，並提升[pH值到](../Page/pH值.md "wikilink")14或以上。強鹽基，像強酸一樣，會侵襲生物組織並引致灼傷。它們的反應不同，所以強酸具腐蝕性，而強鹽基則是[苛性的](../Page/苛性度.md "wikilink")。[超強鹽基是一類特別的鹼性化合物而](../Page/超強鹽基.md "wikilink")[非親核鹼](../Page/非親核鹼.md "wikilink")（暫無譯名）是一類缺少親核性的特殊強鹽基。而[弱鹽基包括用於清潔的](../Page/弱鹽基.md "wikilink")[氨](../Page/氨.md "wikilink")。

### 強鹽基

強鹽基是鹼性化合物所以能夠以酸鹼反應將弱鹽基去質子化。酸度係數大於及等於13的化合物都稱為強鹽基。常見例子包括鹼金屬和鹼土金屬的氫氧化物如氫氧化鈉及氫氧化鈣。非常強的鹽基甚至能在缺少水時將微酸性的烴去質子化。

以下是一些氫氧化物的強鹽基：

  - [氫氧化鉀](../Page/氫氧化鉀.md "wikilink")（KOH）
  - [氫氧化鋇](../Page/氫氧化鋇.md "wikilink")（Ba(OH)<sub>2</sub>）
  - [氫氧化銫](../Page/氫氧化銫.md "wikilink")（CsOH）
  - [氫氧化鈉](../Page/氫氧化鈉.md "wikilink")（NaOH）
  - [氫氧化鍶](../Page/氫氧化鍶.md "wikilink")（Sr(OH)<sub>2</sub>）
  - [氫氧化鈣](../Page/氫氧化鈣.md "wikilink")（Ca(OH)<sub>2</sub>）
  - [氫氧化鋰](../Page/氫氧化鋰.md "wikilink")（LiOH）
  - [氫氧化銣](../Page/氫氧化銣.md "wikilink")（RbOH）}}

這些金屬的陽離子出現於元素週期表的第1和第2族上（即鹼金屬和鹼土金屬）。

  - [正丁基鋰](../Page/正丁基鋰.md "wikilink")（n-BuLi）
  - [二異丙基氨基鋰](../Page/二異丙基氨基鋰.md "wikilink")（LDA）（C<sub>6</sub>H<sub>14</sub>LiN）
  - [氨基鈉](../Page/氨基鈉.md "wikilink")（NaNH<sub>2</sub>）
  - [氫化鈉](../Page/氫化鈉.md "wikilink")（NaH）

## 鹽基作為異構催化劑

鹼性物質可用作[化學反應的異構](../Page/化學反應.md "wikilink")[不可溶](../Page/溶解度.md "wikilink")[催化劑](../Page/催化劑.md "wikilink")。例如[氧化鎂](../Page/氧化鎂.md "wikilink")，[氧化鈣以及](../Page/氧化鈣.md "wikilink")[氧化鋇等金屬氧化物以及](../Page/氧化鋇.md "wikilink")[氧化鋁上的](../Page/氧化鋁.md "wikilink")[氟化鉀和某幾種](../Page/氟化鉀.md "wikilink")[沸石](../Page/沸石.md "wikilink")。大量[過渡金屬製造良好催化劑](../Page/過渡金屬.md "wikilink")，其中不少是鹼性的。鹼性催化劑可用於[氫化](../Page/氫化.md "wikilink")，[雙键的移動](../Page/共價鍵.md "wikilink")，[米尔温–庞多夫–韦尔莱还原反应](../Page/米尔温–庞多夫–韦尔莱还原反应.md "wikilink")，[Michael反應](../Page/Michael反應.md "wikilink")，以及許多反應。

## 鹽基和鹼的混淆

鹽基和鹼這兩個術語交換使用，這是由於大部分的鹽基都是鹼。「測量泥土中的[鹼性](../Page/鹼性.md "wikilink")」實際是量度[pH值](../Page/pH值.md "wikilink")。有時錯誤地將[氨歸類為鹼](../Page/氨.md "wikilink")。

注意不是大部分[碱金属鹽都是鹼](../Page/碱金属.md "wikilink")，只有本身是鹼性的鹽才是鹼。

儘管大部分[電陽的金屬](../Page/電負性.md "wikilink")[氧化物都是鹼性](../Page/氧化物.md "wikilink")，只有可溶的鹼金屬及鹼土金屬氧化物才可稱為鹼。

鹼是鹼金屬及鹼土金屬的鹼性鹽，但根據辭典[1](http://en.wiktionary.org/wiki/alkali)[2](http://dictionary.reference.com/search?q=alkali),
對鹼的定義卻有相矛盾，包括：

  - 所有可溶的鹽基[3](https://web.archive.org/web/20060523041052/http://www.tiscali.co.uk/reference/encyclopaedia/hutchinson/m0029936.html)[4](http://www.thefreedictionary.com/alkali)，這應稱作[阿累尼烏斯鹼](../Page/阿累尼烏斯鹼.md "wikilink")。
  - 鹽基的水溶液[5](http://www.krysstal.com/acidbase.html) 。

## 不可溶於水的例子

  - [氧化銅等大部份金屬氧化物](../Page/氧化銅.md "wikilink")

## 可溶於水的例子

  - [氨](../Page/氨.md "wikilink")
  - 所有[鹼](../Page/鹼.md "wikilink")

## 註釋

## 參見

  - [酸](../Page/酸.md "wikilink")
  - [鹼](../Page/鹼.md "wikilink")

[Category:碱](../Category/碱.md "wikilink")

1.  [大英百科全書線上繁體中文版](http://tw.britannica.com/MiniSite/Article/id00007259.html)
2.  *The Origin of the Term Base* William B. Jensen Journal of Chemical
    Education · 1130第83卷，2006年8月8日