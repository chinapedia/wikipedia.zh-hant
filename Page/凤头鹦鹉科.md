**凤头鹦鹉**，是指属于**凤头鹦鹉總科**（[學名](../Page/學名.md "wikilink")：**）
**凤头鹦鹉科**（[學名](../Page/學名.md "wikilink")：**）內的21[种](../Page/种_\(生物\).md "wikilink")[鸚鵡](../Page/鸚鵡.md "wikilink")。其外觀特徵為頭上有可收展的頭冠。

## 辞源

拉丁属名 *Cacatua* 来源于[马来语](../Page/马来语.md "wikilink") 'kakaktua'
，意思是**老姐姐**（**kakak**：姐姐；**tua**：老）。

## 特点

凤头鹦鹉和其他的[鹦鹉有相同的特点](../Page/鹦鹉.md "wikilink")，它们都有钩曲的[喙和](../Page/喙.md "wikilink")[对趾足](../Page/对趾足.md "wikilink")（参阅:
[鹦形目](../Page/鹦形目.md "wikilink")）。但它们的不同主要在于几个[解剖学的特征](../Page/解剖学.md "wikilink")，包括能够收展的头冠，以及羽毛中缺少令其他鹦鹉[羽毛呈现出虹彩的结构](../Page/羽毛.md "wikilink")。凤头鹦鹉平均要比其他的鹦鹉体型大，但[鸡尾鹦鹉却是一种很小的凤头鹦鹉](../Page/鸡尾鹦鹉.md "wikilink")。最大的鹦鹉也并非它们，体长最长的是[紫蓝金刚鹦鹉](../Page/紫蓝金刚鹦鹉.md "wikilink")，体重最重的是[鸮鹦鹉](../Page/鸮鹦鹉.md "wikilink")。凤头鹦鹉是最容易患上[鹦鹉喙羽症的鹦鹉之一](../Page/鹦鹉喙羽症.md "wikilink")。

## 分佈

作为一个科来说，凤头鹦鹉的分布比其他鹦鹉类群来的狭窄。自然状态下仅见于[澳大利亚和其邻近岛屿](../Page/澳大利亚.md "wikilink")。

## 分类

本科作為一個獨立科存在，並沒有多少爭議；唯一問題是目前在鸚鵡科內的如[吸蜜鸚鵡亞科](../Page/吸蜜鸚鵡亞科.md "wikilink")（*Loriinae*）等是否也應獨立成科存在。在較舊的分類系統中，有時也作为凤头鹦鹉亚科（Cacatuinae），置于广义的[鹦鹉科](../Page/鹦鹉科.md "wikilink")（Psittacidae）之下。

## 種

### 棕树凤头鹦鹉族 Microglossini

  - [棕树凤头鹦鹉](../Page/棕树凤头鹦鹉.md "wikilink"), *Probosciger aterrimus*

### 黑凤头鹦鹉族 Calyptorhynchini

  - [红尾黑凤头鹦鹉](../Page/红尾黑凤头鹦鹉.md "wikilink"), *Calyptorhynchus banksii*
  - [辉凤头鹦鹉](../Page/辉凤头鹦鹉.md "wikilink"), *Calyptorhyncus lathami*
  - [黑凤头鹦鹉](../Page/黑凤头鹦鹉.md "wikilink"), *Calyptorhyncus funereus*
  - [短喙黑凤头鹦鹉](../Page/短喙黑凤头鹦鹉.md "wikilink"), *Calyptorhyncus
    latirostris*
  - [长喙黑凤头鹦鹉](../Page/长喙黑凤头鹦鹉.md "wikilink"), *Calyptorhyncus baudinii*

### 凤头鹦鹉族 Cacatuini

  - [红冠灰凤头鹦鹉](../Page/红冠灰凤头鹦鹉.md "wikilink"), *Callocephalon fimbriatum*
  - [粉红凤头鹦鹉](../Page/粉红凤头鹦鹉.md "wikilink"), *Eolophus roseicapilla*
  - [长喙凤头鹦鹉](../Page/长喙凤头鹦鹉.md "wikilink"), *Cacatua tenuirostris*
  - [西长喙凤头鹦鹉](../Page/西长喙凤头鹦鹉.md "wikilink"), *Cacatua pastinator*
  - [小凤头鹦鹉](../Page/小凤头鹦鹉.md "wikilink"), *Cacatua sanguinea*
  - [米切氏凤头鹦鹉](../Page/米切氏凤头鹦鹉.md "wikilink"), *Cacatua leadbeateri*
  - [大葵花凤头鹦鹉](../Page/大葵花凤头鹦鹉.md "wikilink"), *Cacatua galerita*
  - [小葵花凤头鹦鹉](../Page/小葵花凤头鹦鹉.md "wikilink"), *Cacatua sulphurea*
      - [浅黄冠凤头鹦鹉](../Page/浅黄冠凤头鹦鹉.md "wikilink"), *Cacatua sulphurea
        citroncristata*
  - [蓝眼凤头鹦鹉](../Page/蓝眼凤头鹦鹉.md "wikilink"), *Cacatua ophthalmica*
  - [鲑色凤头鹦鹉](../Page/鲑色凤头鹦鹉.md "wikilink") 或 橙冠凤头鹦鹉, *Cacatua
    moluccensis*
  - [大白凤头鹦鹉](../Page/大白凤头鹦鹉.md "wikilink"), *Cacatua alba*
  - [菲律宾凤头鹦鹉](../Page/菲律宾凤头鹦鹉.md "wikilink"), *Cacatua haematuropygia*
  - [戈芬氏凤头鹦鹉](../Page/戈芬氏凤头鹦鹉.md "wikilink"), *Cacatua goffini*
  - [杜氏凤头鹦鹉](../Page/杜氏凤头鹦鹉.md "wikilink"), *Cacatua ducorpsii*

### 鸡尾鹦鹉族 Nymphicini

  - [鸡尾鹦鹉](../Page/鸡尾鹦鹉.md "wikilink"), *Nymphicus hollandicus*

## 參見

  - [鹦形目分类表](../Page/鹦形目分类表.md "wikilink")

## 參考文獻

  -
  -
  -
  -
  -
  -
  -
## 外部連結

  - [Australian Faunal
    Directory](https://web.archive.org/web/20090701034706/http://www.environment.gov.au/biodiversity/abrs/online-resources/fauna/afd/taxa/Cacatua_\(Cacatua\))
  - [MyToos.com](http://www.mytoos.com/) – explaining many of the
    responsibilities of cockatoo ownership
  - [Cockatoo
    videos](http://ibc.lynxeds.com/family/cockatoos-cacatuidae) on the
    Internet Bird Collection

[\*](../Category/凤头鹦鹉科.md "wikilink")