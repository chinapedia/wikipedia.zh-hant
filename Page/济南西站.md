**济南西站**，建设时称为**济南西客站**，济南市民多称其为**西客站**或**西站**，位于[中国](../Page/中国.md "wikilink")[山東省](../Page/山東省.md "wikilink")[济南市](../Page/济南市.md "wikilink")[槐荫区](../Page/槐荫区.md "wikilink")[兴福街道原](../Page/兴福街道.md "wikilink")[张庄机场西侧](../Page/张庄机场.md "wikilink")，距济南市中心8.5km，为[中国铁路济南局集团有限公司特等客运站](../Page/中国铁路济南局集团有限公司.md "wikilink")，2011年6月30日随[京沪高速铁路建成启用](../Page/京沪高速铁路.md "wikilink")。

济南西站为[京沪高速铁路的客运站](../Page/京沪高速铁路.md "wikilink")，[胶济客运专线](../Page/胶济客运专线.md "wikilink")、[石济客运专线](../Page/石济客运专线.md "wikilink")、[济青高速铁路及规划中的](../Page/济青高速铁路.md "wikilink")[郑济客运专线也分别通过联络线引入济南西站](../Page/郑济客运专线.md "wikilink")。

## 历史

  - 2008年，京沪高铁济南西客站开工建设。\[1\]
  - 2011年6月30日，随着[京沪高铁的通车](../Page/京沪高铁.md "wikilink")，京沪高铁济南西客站启用，同时正式定名为**济南西站**，原名为济南西站的编组站更名为[济西站](../Page/济西站.md "wikilink")。
  - 2013年1月，济南西站公交枢纽投入使用，可实现6条BRT线路，12条常规公交线路同时发车。\[2\]
  - 2013年5月1日，济南西站东广场投入使用，东广场为既有西广场面积的3倍，最大可疏散3万名旅客，可满足整个济南西站的客流负荷。\[3\]

## 车站结构

### 站房

站房建筑面积为58000平方米。站房采用线上高架候车结构，并设置站前高架和落客平台。

### 站场

车站设8站台17线，其中正线2条，到发线15条，包括7个岛式站台和1个侧式站台。

## 旅客列车目的地

截至2019年4月，济南西站每天接发超过300班高铁动车，其中有超过50趟始发终到列车。济南西站同时预留[郑济高铁列车接入条件](../Page/郑济高铁.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>目的地所属路局</p></th>
<th><p>站点</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><center></td>
<td><p>、、、、、、、、、、、、、、、、、、、</p></td>
</tr>
<tr class="even">
<td><center></td>
<td><p>、、、</p></td>
</tr>
<tr class="odd">
<td><center></td>
<td><p>、、、、、、、、、、、、、</p></td>
</tr>
<tr class="even">
<td><center></td>
<td><p>、、</p></td>
</tr>
<tr class="odd">
<td><center></td>
<td><p>、、、、、、、、、、、、、、、、、、、、、、<a href="../Page/淄博北站.md" title="wikilink">淄博北</a>、章丘北、邹平、临淄北、潍坊北、青州市北、高密北、胶州北、青岛西、日照西、岚山西</p></td>
</tr>
<tr class="even">
<td><center></td>
<td><p>、、、</p></td>
</tr>
<tr class="odd">
<td><center></td>
<td><p>、、、、</p></td>
</tr>
<tr class="even">
<td><center></td>
<td><p>、、、、、、、、、、、、、、、、、、、、、、</p></td>
</tr>
<tr class="odd">
<td><center></td>
<td><p>、、、、、、</p></td>
</tr>
<tr class="even">
<td><center></td>
<td><p>、、、、、、、、、、、、、、、、、、、、、、、、、、、、、、、、、、、、、、、、、、、、、盐城北</p></td>
</tr>
<tr class="odd">
<td><center></td>
<td><p>、、、、、、、、、、、、、、、、、、、、</p></td>
</tr>
<tr class="even">
<td><center></td>
<td><p>、、、、、、、、、、、</p></td>
</tr>
<tr class="odd">
<td><center></td>
<td><p>、、、、、、、</p></td>
</tr>
<tr class="even">
<td><center></td>
<td><p>、、、、、、</p></td>
</tr>
<tr class="odd">
<td><center></td>
<td><p>、、、、、、、、、、、、</p></td>
</tr>
</tbody>
</table>

## 配套交通

[West_Jinan_Railway_Station_20171105-5.jpg](https://zh.wikipedia.org/wiki/File:West_Jinan_Railway_Station_20171105-5.jpg "fig:West_Jinan_Railway_Station_20171105-5.jpg")

### 公共汽车

#### [济南快速公交](../Page/济南快速公交.md "wikilink")

  - **[BRT-1](../Page/济南快速公交#一号线.md "wikilink")** 济南西站公交枢纽–全福立交桥西

#### 常规公共汽车

站台位于**济南西站公交枢纽**：

  - **K58** 济南西站公交枢纽-动物园
  - **K109** 济南西站公交枢纽–大明湖
  - **K156** 济南西站公交枢纽–[火车站](../Page/济南站.md "wikilink")（车站街经一路路口北）
  - **K157** 济南西站公交枢纽–东八里洼
  - **K167** 济南西站公交枢纽–宝华街官扎营东街
  - **K202** 济南西站公交枢纽–公交新泺大街东口车场
  - **T202** 济南西站公交枢纽–齐鲁软件园东

站台位于**济南西站东广场**：

  - **K19** 彭庄–经六纬九
  - **K141**方特东方神画–灵岩路玉府街
  - **K910** 顺安苑小区–董家

站台位于**济南西站（日照路齐鲁大道）**

  - **K909** 济南西站客运枢纽–齐河公交停车场
  - **K910** 顺安苑小区–董家
  - **K911** 济南西站客运枢纽–齐鲁软件园

站台位于**济南西站（长途汽车西站）**

  - **K909** 济南西站客运枢纽–齐河公交停车场
  - **K911** 济南西站客运枢纽–齐鲁软件园

站台位于**济南西站（顺安路）**

  - **K38** 经十路营市西街–位里

#### 快速直达专线

票价20元，行驶于高速公路

  - **济南西站-[济南东站](../Page/济南东站.md "wikilink")** 济南西站公交枢纽–济南东站公交枢纽
  - **济南西站-[遥墙机场](../Page/济南遥墙国际机场.md "wikilink")** 济南西站公交枢纽–济南遥墙国际机场

### 轨道交通

  - [济南轨道交通1号线](../Page/济南轨道交通1号线.md "wikilink")

### 长途汽车

  - 济南长途汽车西站

## 参见

  - [济南站](../Page/济南站.md "wikilink")

## 邻近车站

|-    |-

## 参考資料

[J](../Category/济南市铁路车站.md "wikilink")
[Category:石济客运专线车站](../Category/石济客运专线车站.md "wikilink")
[Category:京沪高速铁路车站](../Category/京沪高速铁路车站.md "wikilink")
[Category:槐荫区](../Category/槐荫区.md "wikilink")
[Category:2011年启用的铁路车站](../Category/2011年启用的铁路车站.md "wikilink")

1.
2.
3.