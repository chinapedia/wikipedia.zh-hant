**101**是[100与](../Page/100.md "wikilink")[102之间的](../Page/102.md "wikilink")[自然数](../Page/自然数.md "wikilink")。

## 数学性质

  - 101是5個連續質數之和：[13](../Page/13.md "wikilink") +
    [17](../Page/17.md "wikilink") + [19](../Page/19.md "wikilink") +
    [23](../Page/23.md "wikilink") + [29](../Page/29.md "wikilink")
  - 101是[整數分拆](../Page/整數分拆.md "wikilink")[13的方法數目](../Page/13.md "wikilink")。
  - 101是[中心十邊形數](../Page/中心多邊形數.md "wikilink")。
  - 101是[五角星數](../Page/五角星數.md "wikilink")。
  - 數[5的](../Page/5.md "wikilink")[二進制表示法](../Page/二進制.md "wikilink")。
  - 101是[十进制中](../Page/十进制.md "wikilink")，最小的三位[回文數](../Page/回文數.md "wikilink")。
  - 101 = 5\! - 4\! + 3\! - 2\! + 1\!
  - [11與](../Page/11.md "wikilink")101是僅有的兩個已知的100000...000001質數

## 科學上

  - 101是[鍆的](../Page/钔.md "wikilink")[原子序數](../Page/原子序数.md "wikilink")\[1\]。
  - [天文學上](../Page/天文学.md "wikilink")，有[小行星101](../Page/小行星101.md "wikilink")。

## 文章标题上

越来越多的英文书籍和文章带有101字样， 101超过了100， 在英文里头形容很多的意思。 类似中文的“九”和“三”， 通常用来表示数量很多，
比如 101种方法， 101种问题，101种答案。

## 在其它领域中

  - [台北101](../Page/台北101.md "wikilink")：位於[臺灣](../Page/臺灣.md "wikilink")[台北市的地標建築](../Page/台北市.md "wikilink")，是目前世界第三高樓、環太平洋及全球最高的綠建築大樓。
  - 《[101忠狗](../Page/101忠狗.md "wikilink")》：[迪士尼](../Page/华特迪士尼公司.md "wikilink")[經典動畫長片](../Page/迪士尼经典动画长片.md "wikilink")。
  - [挑戰101](../Page/挑戰101.md "wikilink")：[中視](../Page/中視.md "wikilink")（現改至[中天娛樂台](../Page/中天娛樂台.md "wikilink")）的益智猜謎節目。
  - [Now 101台](../Page/Now_101台.md "wikilink") :
    [now寬頻電視的一條](../Page/now寬頻電視.md "wikilink")[香港中文資訊娛樂頻道](../Page/香港.md "wikilink")。
  - [101空降師](../Page/101空降師.md "wikilink")：[美國陸軍編制部隊](../Page/美國陸軍.md "wikilink")。
  - [中華民國陸軍101兩棲偵察營](../Page/中華民國陸軍101兩棲偵察營.md "wikilink")
  - [新界區專線小巴101M線](../Page/新界區專線小巴101M線.md "wikilink")，前稱小巴101線
  - [PRODUCE 101系列](../Page/PRODUCE_101.md "wikilink")
  - [過海隧道巴士101線](../Page/過海隧道巴士101線.md "wikilink")，來往[堅尼地城及](../Page/堅尼地城（西寧街）巴士總站.md "wikilink")[觀塘（裕民坊）](../Page/觀塘仁愛圍公共運輸交匯處.md "wikilink")。是香港首批過海隧道巴士路線
  - [动感101](../Page/动感101.md "wikilink")，上海广播电台
  - [IOI](../Page/IOI.md "wikilink")
  - 《[創造101](../Page/創造101.md "wikilink")》，中國2018年真人秀网综，其组成出道了全新偶像女团“[火箭少女101](../Page/火箭少女101.md "wikilink")”女星团体
  - [韓國世韓汽車](../Page/韓國.md "wikilink")(Saehan)/[大宇巴士多款巴士車款名稱都有](../Page/大宇巴士.md "wikilink")「101」字樣，例如世韓/大宇BF101(前置引擎車款)。
  - [一零一铁路](../Page/一零一铁路.md "wikilink")，位于[北京市的军用铁路](../Page/北京市.md "wikilink")，从[西郊机场西边的](../Page/西郊机场.md "wikilink")[一零一站开始](../Page/一零一站.md "wikilink")，经[衙门口站](../Page/衙门口站.md "wikilink")、[石景山南站](../Page/石景山南站.md "wikilink")，最终连接到[长辛店站](../Page/长辛店站.md "wikilink")。
  - [徐虹铁路](../Page/徐虹铁路.md "wikilink")，位于[上海市的飞起军用铁路](../Page/上海市.md "wikilink")，又名101专用线。

## 参考文献

[Category:整數素數](../Category/整數素數.md "wikilink")

1.  [Royal Society of Chemistry - Visual Element Periodic
    Table](http://www.rsc.org/periodic-table)