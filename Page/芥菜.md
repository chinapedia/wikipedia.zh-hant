**芥菜**（[学名](../Page/学名.md "wikilink")：**）又名**刈菜**、**大菜**、**大芥**、**芥子**，當年菜時又名**長年菜**，是[十字花科](../Page/十字花科.md "wikilink")[芸苔屬一年生](../Page/芸苔屬.md "wikilink")[草本植物](../Page/草本植物.md "wikilink")。在[芥菜类蔬菜中](../Page/芥菜类蔬菜.md "wikilink")，[褐芥菜的栽培品种最多](../Page/褐芥菜.md "wikilink")。其中在[中国有多个褐芥菜品种](../Page/中国.md "wikilink")，被统称为“芥菜”。在中国，芥菜的用途十分广泛。

[臺灣常見的蔬菜](../Page/臺灣.md "wikilink")。常寫成閩南語諧音的「刈菜」，客家語則稱芥菜為「大菜」\[1\]。一般來說，中北部福佬人的過年口中所謂的長年菜是芥菜，但是南部福佬人是帶根的[菠菜](../Page/菠菜.md "wikilink")\[2\]。而南部客家人跟中北部客家人長年菜一樣是芥菜，應該與客家人善於用芥菜製作各種鹹菜有關。客家人以芥菜做成酸菜（客家語稱鹹菜）、福菜（客家語稱覆菜）、梅乾菜（客家語稱鹹菜乾）\[3\]。

## 芥菜的变种

芥菜分为三大变种：叶用芥菜、根用芥菜和茎用芥菜。

  - [子芥菜](../Page/子芥菜.md "wikilink") *Brassica juncea* var. ''gracilis
    ''Tsen et
    Lee：别名辣油菜、种子含有[硫代葡萄糖苷](../Page/硫代葡萄糖苷.md "wikilink")、[烯丙基异硫氰酸盐](../Page/烯丙基异硫氰酸盐.md "wikilink")、[羟苄基异硫氰酸盐](../Page/羟苄基异硫氰酸盐.md "wikilink")、具有刺激舌头和鼻窦的辛辣味、用它来生产[芥末](../Page/芥末.md "wikilink")、[芥末油](../Page/芥末油.md "wikilink")、[咖喱粉等](../Page/咖喱粉.md "wikilink")[香辛料](../Page/香辛料.md "wikilink")。
  - [榨菜](../Page/榨菜.md "wikilink") *Brassica juncea* var. *tsatsai*
    Mao：大头菜、茎瘤芥、菜头（茎用芥菜）
  - [叶用芥菜](../Page/叶用芥菜.md "wikilink") *Brassica juncea* var. *foliosa*
    Bailey：其中[盖菜](../Page/盖菜.md "wikilink")（又叫菜），叶面多皱纹，可使用，是叶用芥菜的典型
  - [雪里蕻](../Page/雪里蕻.md "wikilink") *Brassica juncea* var.
    *crispifolia* Bailey：又称雪里红
  - [根用芥菜](../Page/根用芥菜.md "wikilink") *Brassica juncea* var.
    *megarrhiza* Tsen et Lee

其它同名但不同种的蔬菜：

  - [黑芥菜](../Page/黑芥菜.md "wikilink") *Brassica nigra*
  - [白芥菜](../Page/白芥菜.md "wikilink") ''Sinapis alba ''

## 植物修复

这种植物用于去除重金属，如[铅](../Page/铅.md "wikilink")，从有害废物处置场所的土壤，因为它有对这些物质更高的耐受性和能存储重金属在它的细胞中\[4\]。然后该植物被收割和妥善处理。这种方法比传统方法对去除重金属更容易和更便宜。它还可以防止水土流失从这些场地防止进一步污染。

## 食用 

改善消化且有助於脂肪的代謝。內服的方式使用，有助於改善胸部充血、發炎、受傷和伴隨的疼痛。

## 图集

<File:Brassica> juncea var. juncea.JPG|植株
[File:RoterSenfRedGiantBlatt.jpg|葉](File:RoterSenfRedGiantBlatt.jpg%7C葉)
<File:Brassica> juncea var. juncea 3.JPG|花
[File:Moutarde.jpg|種子](File:Moutarde.jpg%7C種子) <File:Mustard> seed
closeup.jpg|種子近觀 [File:Senf-3.jpg|芥末醬](File:Senf-3.jpg%7C芥末醬)

## 參考文獻

  - 《台灣蔬果實用百科第一輯》，薛聰賢 著，薛聰賢出版社，2001年，ISDN:957-97452-1-8

## 外部链接

  - [Brassica
    juncea](http://www.hort.purdue.edu/newcrop/duke_energy/Brassica_juncea.html)
  - [Culinary description of the many tasty varieties of mustard
    greens](https://web.archive.org/web/20051126170450/http://www.innvista.com/health/foods/vegetables/mustard.htm)
  - [芥菜
    Jiecai](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D01367)
    藥用植物圖像數據庫（香港浸會大學中醫藥學院）
  - [芥子
    Jiezi](http://libproject.hkbu.edu.hk/was40/detail?channelid=47953&searchword=pid=B00186)
    中藥材圖像數據庫（香港浸會大學中醫藥學院）

[\*](../Category/芥菜.md "wikilink")
[Category:葉菜類](../Category/葉菜類.md "wikilink")
[Category:植物修复植物](../Category/植物修复植物.md "wikilink")

1.

2.  [過年必吃"長年菜"
    南北大不同\!](https://news.cts.com.tw/cts/life/201802/201802121913360.html)

3.
4.  [Phytoremediation of Arsenic and Lead in Contaminated Soil Using
    Chinese Brake Ferns (Pteris vittata) and Indian Mustard (Brassica
    juncea)](https://www.tandfonline.com/doi/abs/10.1080/713610173)