**法爾納瓦茲一世**（）是[格魯吉亞歷史上第一位君主](../Page/格魯吉亞.md "wikilink")，於前302年至前237年上任，他亦是創造[喬治亞字母的始祖](../Page/喬治亞字母.md "wikilink")，是[法爾納瓦齊烏尼王朝的皇室成員](../Page/法爾納瓦齊烏尼王朝.md "wikilink")。

## 生平

法爾納瓦茲一世為[高加索伊比利亞王國首任君主](../Page/高加索伊比利亞王國.md "wikilink")，據文獻所載，他於前3世紀發明文字。他於前237年逝世，享年92歲。

## 參考文獻

  - Rapp, Stephen H. (2003), Studies In Medieval Georgian
    Historiography: Early Texts And Eurasian Contexts. Peeters Bvba ISBN
    90-429-1318-5.
  - Toumanoff, Cyril (1963), Studies in Christian Caucasian History.
    Georgetown University Press.
  - Suny, Ronald Grigor (1994), The Making of the Georgian Nation (2nd
    edition). Indiana University Press, ISBN 0-253-20915-3.

[Category:格鲁吉亚君主](../Category/格鲁吉亚君主.md "wikilink")
[Category:前237年逝世](../Category/前237年逝世.md "wikilink")
[Category:格鲁吉亚字母](../Category/格鲁吉亚字母.md "wikilink")
[Category:開國君主](../Category/開國君主.md "wikilink")