[Calibri_example.svg](https://zh.wikipedia.org/wiki/File:Calibri_example.svg "fig:Calibri_example.svg")

**Calibri**，一種[無襯線字體](../Page/無襯線字體.md "wikilink")，為[微軟](../Page/微軟.md "wikilink")[Microsoft
Office 2007和](../Page/Microsoft_Office_2007.md "wikilink")[Microsoft
Office 2008 for
Mac套裝軟體的預設字體](../Page/Microsoft_Office_2008_for_Mac.md "wikilink")，取代先前[Microsoft
Word的預設字體](../Page/Microsoft_Word.md "wikilink")[Times New
Roman以及](../Page/Times_New_Roman.md "wikilink")[PowerPoint](../Page/Microsoft_PowerPoint.md "wikilink")、[Excel和](../Page/Microsoft_Excel.md "wikilink")[Outlook的預設字體](../Page/Microsoft_Outlook.md "wikilink")[Arial](../Page/Arial.md "wikilink")。

Calibri是搭發布於微軟[Windows
Vista六種西方](../Page/Windows_Vista.md "wikilink")[ClearType字體的其中一種](../Page/ClearType.md "wikilink")，是[Microsoft
Word預設字體的第一個](../Page/Microsoft_Word.md "wikilink")[無襯線字體](../Page/無襯線字體.md "wikilink")，先前則是使用[Times
New Roman為預設字體](../Page/Times_New_Roman.md "wikilink")。

Calibri為字型設計師替[微軟開發的字型](../Page/微軟.md "wikilink")，曾於2005年字型設計競賽（Type
Design Competition）中獲得系統字型（Type
System）類的獎項。其也包含了[拉丁文](../Page/拉丁文.md "wikilink")、[希臘文以及](../Page/希臘文.md "wikilink")[西里爾字母的字母](../Page/西里爾字母.md "wikilink")。

在某個由[威奇托州立大學執行的研究中](../Page/威奇托州立大學.md "wikilink")，Calibri是[電子郵件](../Page/電子郵件.md "wikilink")、[即時通和](../Page/即時通.md "wikilink")[PowerPoint簡報中最常被使用的字體](../Page/PowerPoint.md "wikilink")，同時其用於網頁設計的排名也相當高。\[1\]

2017年，因[巴拿馬文件而爆發的](../Page/巴拿馬文件.md "wikilink")[巴基斯坦總理](../Page/巴基斯坦.md "wikilink")[納瓦茲·謝里夫涉貪案中](../Page/納瓦茲·謝里夫.md "wikilink")，此字型成為當地調查單位指控總理女兒提供偽造文件的證據，因為她在一份宣稱是2006年印製的文件上，用了這份2007年才開始使用的字形\[2\]。

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

  -
  - [Microsoft
    ClearType字體總目錄](http://www.microsoft.com/typography/ClearTypeFonts.mspx)

  - [Microsoft
    ClearType字體總目錄](http://www.ascendercorp.com/ctfonts.html)at
    Ascender Corporation

  - [下載Calibri字體](http://www.ascendercorp.com/font/calibri/)

  - [下載PPTViewer兼Calibri字體](http://www.microsoft.com/downloads/details.aspx?familyid=048DC840-14E1-467D-8DCA-19D2A8FD7485&displaylang=en)

[Category:无衬线字体](../Category/无衬线字体.md "wikilink")
[Category:微软字体](../Category/微软字体.md "wikilink")
[Category:Windows Vista字體](../Category/Windows_Vista字體.md "wikilink")

1.  <http://psychology.wichita.edu/surl/usabilitynews/81/PersonalityofFonts.htm>
    survey
2.  [為何一款Calibri字體
    讓巴基斯坦美女政二代遭控造假？](http://www.chinatimes.com/realtimenews/20170713002270-260408)