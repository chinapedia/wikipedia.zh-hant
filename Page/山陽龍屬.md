**山陽龍屬**（[學名](../Page/學名.md "wikilink")：*Shanyangosaurus*）是[獸腳亞目](../Page/獸腳亞目.md "wikilink")[恐龍的一屬](../Page/恐龍.md "wikilink")，化石發現於[中國](../Page/中國.md "wikilink")[陕西省](../Page/陕西省.md "wikilink")，其[化石只有一些後肢骨頭碎片](../Page/化石.md "wikilink")。這些骨頭的內部空心\[1\]，加上[股骨及腳掌的一些特徵](../Page/股骨.md "wikilink")，顯示山陽龍是屬於[虛骨龍類](../Page/虛骨龍類.md "wikilink")，但不能確定確實的演化與分類位置。[湯瑪斯·荷茲](../Page/湯瑪斯·荷茲.md "wikilink")（Thomas
Holtz）等人將山陽龍分類為[鳥獸腳類](../Page/鳥獸腳類.md "wikilink")\[2\]。

## 參考資料

<div class="references-small">

<references>

</references>

</div>

## 外部連結

  - [恐龍百科全書的山陽龍](https://web.archive.org/web/20071005120452/http://www.dinoruss.com/de_4/5a63fa4.htm)
  - [古生物數據庫的山陽龍](http://paleodb.org/cgi-bin/bridge.pl?action=checkTaxonInfo&taxon_no=68140)
  - [山陽龍](http://web.me.com/dinoruss/de_4/5a63fa4.htm) Dino Russ's Lair

[Category:亞洲恐龍](../Category/亞洲恐龍.md "wikilink")
[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")
[Category:虛骨龍類](../Category/虛骨龍類.md "wikilink")

1.  Xue, Zhang, Bi, Yue and Chen (1996). The development and
    environmental changes of the intermontane basins in the Eastern part
    of Qinling Mountains. Geological Publishing House, Beijing. ISBN
    7-116-02125-6. 179 pages.
2.  T. R. Holtz, R. E. Molnar, and P. J. Currie (2004). Basal Tetanurae.
    In D. B. Weishampel, P. Dodson, & H. Osmólska (eds.), The Dinosauria
    (second edition). University of California Press, Berkeley 71-110