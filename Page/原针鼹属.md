**原针鼹属**，是[单孔目下的一個](../Page/单孔目.md "wikilink")[属](../Page/属.md "wikilink")。屬下共有5個品種，其中2個已經滅絕。原針鼴屬動物身體都是長滿刺的，仍然存活的3個品種居住在[新畿內亞及](../Page/新畿內亞.md "wikilink")[澳洲西部](../Page/澳洲.md "wikilink")，但卻都是瀕危的物種。

## 品種

  - [阿滕伯勒長喙針鼴鼠](../Page/阿滕伯勒長喙針鼴鼠.md "wikilink")（*Z.
    attenboroughi*）：生活於[新畿內亞西部的山脈中](../Page/新畿內亞.md "wikilink")，是瀕危的物種。
  - [大長吻針鼴](../Page/大長吻針鼴.md "wikilink")（*Z.
    bartoni*）：生活在[新畿內亞中央位於Paniai湖及Nanneau](../Page/新畿內亞.md "wikilink")
    Range之間的山脈，是瀕危的物種。
  - [長吻针鼴](../Page/長吻针鼴.md "wikilink")（*Z.
    bruijnii*）：生活於新畿內亞山脈森林，是瀕危的物種。
  - ***Z.
    hacketti***：曾生活在[澳大利亚西部](../Page/澳大利亚.md "wikilink")，已经绝灭。从化石上看出它有一米左右，是特大的針鼴。
  - ***Z.
    robustus***：曾生活在[塔斯马尼亚岛](../Page/塔斯马尼亚岛.md "wikilink")，已经绝灭。从头部化石上看出它有65厘米左右长。

## 瀕危保護

根據由各締約國簽訂的《[濒危野生动植物种国际贸易公约](../Page/濒危野生动植物种国际贸易公约.md "wikilink")》，原针鼹属動物屬於附錄Ⅱ中受保護的列明物種。雖然原针鼹属並未屬於瀕臨絕種的物種，但其貿易可能會導致絕種的危險。因此，公約上規定原针鼹属是可以貿易，但必須先獲得公約許可證。

[category:原針鼴屬](../Page/category:原針鼴屬.md "wikilink")