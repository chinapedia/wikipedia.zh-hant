**SK Gaming**是著名的国际[电子竞技组织](../Page/电子竞技.md "wikilink")，麾下拥有擅长不同游戏的游戏高手。

## 历史

SK成立于1997年，最初是由3兄弟组建的[德国](../Page/德国.md "wikilink")[雷神之锤战队](../Page/雷神之锤.md "wikilink")。他们第一次取得重大成功是把[瑞典当时著名的](../Page/瑞典.md "wikilink")[反恐精英战队](../Page/反恐精英.md "wikilink")[NiP收归麾下](../Page/NiP.md "wikilink")，后来这支队伍成为了著名的SK.swe。SK在当时几乎赢得了所有世界反恐精英大赛的冠军，并且成为[CPL中独树一帜的强队](../Page/CPL.md "wikilink")。而SK旗下的其他游戏分队也取得了不错的成绩。

2003年2月1日，SK成为第一支和队员签订合同的电子竞技队伍，之后的2004年5月18日，他们又成为操作有偿转会电子竞技玩家的第一队，当时他们的Ola
'element' Moum转投死对头NoA。

但是在2005年初，SK.swe开始因为赞助问题分崩离析。队员整体离开，并且重组了[NiP](../Page/NiP.md "wikilink")。但是后来又有一些队员回归SK，并且直到现在还在为SK效力。

2007年後影響力漸低，在2011年成立英雄聯盟職業隊伍，只是成績普通，還曾在2014年英雄聯盟世界賽時引發種族歧視事件，後面解散今年又重組隊伍。而在2016年買下絕對武力:全球攻勢的Luminosity
Gaming職業隊伍時亦引發爭議，不過儘管議論紛紛仍在2016-2017年拿下最多國際賽冠軍，現今隊伍大部分成員過檔miBR戰隊。

## 队名来源

在玩雷神之锤的死亡竞赛模式时候，SK的队员之一习惯于在拿到双管猎枪之后，高喊“Schröööt\!”（字面意思为“霰彈”，雷神之锤的武器一种），于是他们决定就把这个口号当成自己的队伍名称“Schroet
Kommando”，翻译成中文意思为“霰彈特工队”。目前人们基本上不提及这个冗长的名字而直呼他们SK，队员也是如此，可能因为他们觉得原来的名字和自己给公众留下职业印象的目标不合。

## 荣誉

SK曾经在下面的比赛中取得过前三名：

### 反恐精英

#### 2005年

  - [CPL冬季冠军赛](../Page/CPL.md "wikilink")：冠军
  - [CPL夏季冠军赛](../Page/CPL.md "wikilink")：冠军
  - [ESWC](../Page/ESWC.md "wikilink")：亚军（SK.dk）

#### 2004年

  - CPL夏季冠军赛：出线

#### 2003年

  - CPL冬季冠军赛：冠军
  - CPL[哥本哈根站](../Page/哥本哈根.md "wikilink")：冠军
  - CPL夏季冠军赛：冠军
  - CPL[嘎纳站](../Page/嘎纳.md "wikilink")：冠军
  - [WCG](../Page/WCG.md "wikilink")：冠军
  - ESWC：季军

#### 2002年

  - CPL夏季冠军赛：冠军
  - CPL冬季冠军赛：季军

### 斩除妖魔

#### 2005年

  - CPL世界巡回赛[西班牙站](../Page/西班牙.md "wikilink")：冠军（Stephan 'SteLam'
    Lammert）

### 虚幻竞技场

#### 2004年

  - CPL夏季冠军赛：冠军

### 魔兽争霸III

#### 2005年

  - 欧洲杯：冠军

#### 2003年

  - WCG：冠军

### 絕對武力：全球攻勢

#### 2016年

  - ESL科隆站：冠军

## 現役隊員

### 人員

  - Andreas 'bds' Thorstensson

  - Alexander 'Theslash' Müller-Rodic

### 魔獸爭霸3(DotA)

  - Jonathan "Loda" Berg (Team Captain)

  - Erik "Renji" Olsson

  - Gustav "Drayich" Bergström

  - Joakim "Akke" Akterhall

  - Edvin "KwoM" Börjesson

  - Erik "Bogdan" Olsson

  - Adam "zBob" Nordgren

### [魔獸世界](../Page/魔獸世界.md "wikilink")

  - Stefan 'zeksy' Baudin (Team Captain)

  - Michael 'Dosferra' Tofer

  - Thomas 'Lomas' Lindmark

  - Jonas 'Tarde' Jakobsson

  - Erik 'daimx' Olofsson

  - Mattias 'Oteus' Franck

### [雷神之鎚4](../Page/雷神之鎚4.md "wikilink")

  - Fredrik "gopher" Quick

  - Karl "fooKi" Johansson

  - Pelle "fazz" Söderman

### FIFA

  - Daniel "hero" Schellhase

  - Dennis "styla" Schellhase

  - Marc "WurST" Knieb

  - Andreas "Kaddy" Varlemann

  - Florian "Flo" Brellochs

  - Mario "mario" Viska

  - Mohamed "TornAdo" El-eter

  - Daniel "eNnu" Ennulat

  - Nils "niLssoN" Nolting

### 絕對武力：全球攻勢<已轉至miBR戰隊>

  - Gabriel "FalleN" Toledo

  - Fernando "fer" Alvarenga

  - Marcelo "coldzera" David

  - Jacky "Stewie2K" Yip

  - Ricardo "boltz" Prass

## 著名隊員

  - [埃米尔·克里斯坦森](../Page/埃米尔·克里斯坦森.md "wikilink")－HeatoN（2005年1月1日轉至[NiP](../Page/NiP.md "wikilink")）

  - [湯米·因格馬森](../Page/湯米·因格馬森.md "wikilink")－Potti（2005年1月1日轉至[NiP](../Page/NiP.md "wikilink")）

  - Michael "ahl" Korduner

  - [Ola "elemeNt"
    Moum](../Page/Ola_"elemeNt"_Moum.md "wikilink")（2004年5月18日轉隊至[Team
    NoA](../Page/Team_NoA.md "wikilink")）

  - Daniel "Hyper" Kuusisto

  - Madfrog|Fredrik "MaDFroG" Johanson（退休兩次）

  - Johan "Toxic" Quick（2007年1月1日釋出）

  - vilden|Christian "vilden" Lidström

  - Snajdan|Mattias "Snajdan" Andersen

  - Abdisamad 'SpawN' Mohamed

  - Marcus "Delpan" Larsson

  - Johan "face" Klasson

  - Patrik "f0rest" Lindberg

  - Robert "RobbaN" Dahlström

  - Christopher "GeT_RiGhT" Alesund

  - Gabriel "FalleN" Toledo

  - Marcelo "coldzera" David (獲得2016年TGA最佳電競選手獎項)

## 外部链接

  - [SK官方网站](http://www.sk-gaming.com/)

[Category:电子竞技团队](../Category/电子竞技团队.md "wikilink")
[Category:英雄聯盟競技團隊](../Category/英雄聯盟競技團隊.md "wikilink")