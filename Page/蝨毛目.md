**蝨毛目**（[學名](../Page/學名.md "wikilink")：）是原蝨目和食毛目的合稱，通稱**虱**或**虱子**（）。全世界約有3,000種。虱[寄生于人体](../Page/寄生.md "wikilink")、其他[哺乳动物](../Page/哺乳动物.md "wikilink")（除了[单孔目和](../Page/单孔目.md "wikilink")[蝙蝠外](../Page/蝙蝠.md "wikilink")）和[鳥類的身上](../Page/鳥類.md "wikilink")。以人类为宿主的虱有三种：[人头虱](../Page/人头虱.md "wikilink")、[人体虱和](../Page/人体虱.md "wikilink")[阴虱](../Page/阴虱.md "wikilink")（又称耻阴虱）。其中，[人头虱和](../Page/人头虱.md "wikilink")[人体虱属于](../Page/人体虱.md "wikilink")[人虱科](../Page/人虱科.md "wikilink")（Pediculus
humanus），[阴虱属于阴虱科](../Page/阴虱.md "wikilink")（Pthirus pubis）。

虱体型较小，无[翅](../Page/翅.md "wikilink")，身体扁平，寄生于毛发处，有善于勾住毛发的[足](../Page/足.md "wikilink")（攫握器）。虱为[渐变态发育](../Page/渐变态.md "wikilink")，终生寄生于[宿主体表](../Page/宿主.md "wikilink")，以宿主[血液](../Page/血液.md "wikilink")、[毛发](../Page/毛发.md "wikilink")、皮屑等为食。寄生于人体的虱主要以宿主[血液为食](../Page/血液.md "wikilink")，其若虫每日吸血1次，成虫每日吸血数次。

## 分類

傳統上，本分類可分為兩個亞目：[蝨亞目](../Page/蝨亞目.md "wikilink")（Anoplura，又名[吸蝨亞目](../Page/吸蝨亞目.md "wikilink")）和[食毛亞目](../Page/食毛亞目.md "wikilink")（Mallophaga）；後來發現食毛亞目原來是個[並系群](../Page/並系群.md "wikilink")，所以再重新分類為以下四個亞目：

  - [蝨亞目](../Page/蝨亞目.md "wikilink")（Anoplura）：蝨子，主要生長於哺乳類動物身上。
  - [象蝨亞目](../Page/象蝨亞目.md "wikilink")（Rhyncophthirina）：大象跟疣豬身上的寄生蟲，其口器能穿透宿主厚厚的表皮。
  - [絲角亞目](../Page/絲角亞目.md "wikilink")（Ischnocera）：主要寄生在[鳥綱物種的毛髮](../Page/鳥綱.md "wikilink")，但亦有一個科寄生在哺乳類動物身上。
  - [鈍角亞目](../Page/鈍角亞目.md "wikilink")（Amblycera）：絲角亞目的近親，但比較原始，在鳥類間很常見，但亦見於南美洲及澳大利亞的哺乳類動物身上。

到了2000年代後期，根據[分子生物學及](../Page/分子生物學.md "wikilink")[形態學的研究確認了蝨毛目與](../Page/形態學_\(生物學\).md "wikilink")[啮虫目是近親](../Page/啮虫目.md "wikilink")，而蝨毛目其實是從[粉啮虫亚目演變而成的](../Page/粉啮虫亚目.md "wikilink")\[1\]\[2\]，因此這兩個目應該重組成為[啮虫总目](../Page/啮虫总目.md "wikilink")（Psocodea，又名[嚙總目](../Page/嚙總目.md "wikilink")\[3\]）\[4\]。

本條目仍列出與啮虫目合併前本分類以下的四個亞目。

### 蝨亞目 Anoplura

[蝨亞目又名](../Page/蝨亞目.md "wikilink")[吸蝨亞目](../Page/吸蝨亞目.md "wikilink")\[5\]。

  - 海獸蝨科 Echinophthiriidae
  - 松鼠蝨科 Enderleinellidae
  - [獸蝨科](../Page/獸蝨科.md "wikilink")
    Haematopinidae：又名[血虱科](../Page/血虱科.md "wikilink")\[6\]
  - 鉤蝨科 Hamophthiriidae
  - 甲脅蝨科 Hoplopleuridae
  - 土豬蝨科 Hybothiridae
  - [毛蝨科](../Page/毛蝨科.md "wikilink")
    Linognathidae：又名[鄂虱科](../Page/鄂虱科.md "wikilink")\[7\]
  - 駱蝨科（微胸蝨科）Microthoraciidae
  - 新毛蝨科（新顎蝨科）Neolinognathidae
  - 蠻豬蝨科 Pecaroecidae
  - 猿蝨科 Pedicinidae
  - [蝨科](../Page/蝨科.md "wikilink") Pediculidae
      - [人头虱](../Page/人头虱.md "wikilink")：寄生于人类[头发中](../Page/头发.md "wikilink")，体长约3mm，雌虱略大于雄虱，身体狭长，呈灰白色或白色，分为头胸腹三部分，有3对足。头虱产[卵于发根处](../Page/卵.md "wikilink")，以[耳后居多](../Page/耳.md "wikilink")，卵[椭圆形](../Page/椭圆.md "wikilink")，[白色](../Page/白.md "wikilink")，俗称虮子。卵孵化后的若虫称为虮，虮较虱外形相似、但体型较小，尤其是腹部较成虫短小。若虫[蜕皮](../Page/蜕皮.md "wikilink")3次后成为成虫。
      - [人体虱](../Page/人体虱.md "wikilink")：人体虱又称衣虱，寄生于人类躯干和四肢，不吸血时隐藏于衣物缝隙褶皱内。体虱外表与头虱相似，但体型略大。
  - 陰蝨科 Phthiridae
      - [阴虱](../Page/阴虱.md "wikilink")：主要寄生于人体[阴毛处](../Page/阴毛.md "wikilink")，也有寄生于[睫毛](../Page/睫毛.md "wikilink")、[腋毛](../Page/腋毛.md "wikilink")、[眉毛](../Page/眉毛.md "wikilink")、[头发及其他浓密体毛处的报道](../Page/头发.md "wikilink")。阴虱体型较体虱、头虱为小，约1\~3mm，身体扁平，呈灰白色，外形如同[螃蟹](../Page/螃蟹.md "wikilink")，故又称蟹虱（crab
        louse）。阴虱卵产于阴毛根部，椭圆形，红褐色或铁锈色。卵孵化后的若虫比成虫小，也以血液为食。
  - 細毛蝨科 Polyplacidae
  - 驢蝨科（馬蝨科）Ratemiidae

### [象蝨亞目](../Page/象蝨亞目.md "wikilink") Rhyncophthirina

  - [象蝨科](../Page/象蝨科.md "wikilink") Haematomyzidae

### 絲角亞目 Ischnocera

  - [Heptapsogasteridae](../Page/Heptapsogasteridae.md "wikilink")
  - [Goniodidae](../Page/Goniodidae.md "wikilink")
  - [長角羽蝨科](../Page/長角羽蝨科.md "wikilink")（[長角鳥蝨](../Page/長角鳥蝨.md "wikilink")）[Philopteridae](../Page/Philopteridae.md "wikilink")
  - [獸羽蝨科](../Page/獸羽蝨科.md "wikilink")
    [Trichodectidae](../Page/Trichodectidae.md "wikilink")

### 鈍角亞目 Amblycera

  - 袋鼠羽蝨科（袋鼠鳥蝨科）Boopidae
  - 鼠鳥蝨科 Gyropidae
  - 水鳥蝨科 Laemobothriidae
  - 短角鳥蝨科 Menoponidae
  - [鳥蝨科](../Page/鳥蝨科.md "wikilink") Ricinidae
  - 毛羽蝨科（毛鳥蝨科）Trimenoponidae

## 疾病

虱病本身是[寄生虫疾病](../Page/寄生虫.md "wikilink")，虱吸血时会向人体内注入[唾液以防止血液凝聚](../Page/唾液.md "wikilink")，因而使人产生搔痒感。人亦可能因抓搔而使[皮肤破损](../Page/皮肤.md "wikilink")、溃烂和[细菌感染](../Page/细菌感染.md "wikilink")。此外，人体虱还可传播[流行性斑疹伤寒](../Page/流行性斑疹伤寒.md "wikilink")、[战壕热](../Page/战壕热.md "wikilink")、[回归热等恶性传染病](../Page/回归热.md "wikilink")。

[阴虱本身并不传播其他疾病](../Page/阴虱.md "wikilink")，但是由于阴虱主要通过[人类性行为传播](../Page/人类性行为.md "wikilink")，故属于[性传播疾病的一种](../Page/性传播疾病.md "wikilink")。

## 注釋

## 參考來源

[\*](../Category/蝨毛目.md "wikilink")
[Category:寄生蟲](../Category/寄生蟲.md "wikilink")

1.

2.

3.

4.  Bess, Emilie, Vince Smith, Charles Lienhard, and Kevin P. Johnson
    (2006) Psocodea. Parasitic Lice (=Phthiraptera), Book Lice, and Bark
    Lice. Version 8 October 2006 (under construction).
    <http://tolweb.org/Psocodea/8235/2006.10.08> in The Tree of Life Web
    Project, <http://tolweb.org/>

5.

6.
7.