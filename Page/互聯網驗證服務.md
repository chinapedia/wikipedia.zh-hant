**互聯網驗證服務**(，簡寫**IAS**)是[微軟對](../Page/微軟.md "wikilink")[RADIUS服務的實現](../Page/RADIUS.md "wikilink")，是一種中央處理的用戶驗證系統。

## 參看

  - [有線等效加密 (WEP)](../Page/有線等效加密.md "wikilink") - [Wi-Fi Protected
    Access (WPA)](../Page/WPA.md "wikilink")

## 外部連結

  - [微軟技術：互聯網驗證服務](https://web.archive.org/web/20060423033739/http://www.microsoft.com/technet/itsolutions/network/ias/default.mspx)
  - [Word Document (whitepaper?) from Microsoft explaining IAS
    Logging](https://web.archive.org/web/20070928074306/http://www.only4gurus.com/v3/download.asp?resource=6954)
  - [Article describing how to log IAS (RADIUS) + DHCP to
    SQL](https://web.archive.org/web/20070312010951/http://www.tcs.auckland.ac.nz/~james/wlan-logging/)

[Category:Microsoft
Windows](../Category/Microsoft_Windows.md "wikilink")