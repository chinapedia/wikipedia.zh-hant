**倫敦藝術大學**（**University of the Arts London**，簡稱**UAL**\[1\]，前稱**London
Institute**）是一所位於[英國](../Page/英國.md "wikilink")[倫敦的](../Page/倫敦.md "wikilink")[書院聯邦制大學](../Page/書院聯邦制.md "wikilink")。由六間教授[藝術](../Page/藝術.md "wikilink")、[設計](../Page/設計.md "wikilink")、[時尚和](../Page/時裝.md "wikilink")[媒體的學院組成](../Page/媒體研究.md "wikilink")，提供超過100個藝術，設計，時尚，媒體，通信和表演藝術的本科課程。倫敦藝術大學是[歐洲最大的藝術](../Page/歐洲.md "wikilink")、設計、媒體傳達和表演藝術的教育機構\[2\]。**2019年[QS世界大學排名](../Page/QS世界大學排名.md "wikilink")**在藝術與設計排名中將其列在**世界第二**，僅次於英國倫敦皇家藝術學院\[3\]。

## 學院

| 中文名稱                                                     | 英文名稱                                            | 校園位置                                                                                                                         | 成立年份 |
| -------------------------------------------------------- | ----------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------- | ---- |
| [中央聖馬丁藝術與設計學院](../Page/中央聖馬丁藝術與設計學院.md "wikilink") (CSM) | Central Saint Martins College of Art and Design | [國王十字](../Page/国王十字车站_\(伦敦\).md "wikilink")、[拱门站](../Page/拱門站.md "wikilink") King's Cross、Archway                            | 1854 |
| [倫敦時裝學院](../Page/倫敦時裝學院.md "wikilink")（LCF）              | London College of Fashion                       | [牛津街、肖迪尔奇、老街、白城 等](../Page/牛津街.md "wikilink") [Oxford circus](../Page/牛津街.md "wikilink"), Shoreditch, Old street, White City | 1906 |
| [倫敦傳播學院](../Page/倫敦傳播學院.md "wikilink") (LCC)             | London College of Communication                 | [象堡](../Page/象堡.md "wikilink") [Elephant and Castle](../Page/Elephant_Castle.md "wikilink")                                  | 1894 |
| [切爾西藝術學院](../Page/:切爾西藝術與設計學院.md "wikilink") (CCW)       | Chelsea College of Arts                         | [皮姆利科](../Page/皮姆利科站.md "wikilink") [Pimlico](../Page/Pimlico.md "wikilink")                                                 | 1895 |
| [坎伯韋爾藝術區學院](../Page/坎伯韋爾藝術區學院.md "wikilink") (CCW)       | Camberwell College of Arts                      | [坎伯韋爾](../Page/坎伯韋爾和佩卡姆_\(英國國會選區\).md "wikilink") [Camberwell](../Page/坎伯韦尔.md "wikilink")                                   | 1898 |
| [溫布頓藝術學院](../Page/溫布頓藝術學院.md "wikilink") (CCW)           | Wimbledon College of Arts                       | [温布尔登](../Page/温布尔登_\(伦敦\).md "wikilink") [Merton Park](../Page/Merton_Park.md "wikilink")                                   | 1890 |
|                                                          |                                                 |                                                                                                                              |      |

## 歷史

組成倫敦藝術大學的六所[藝術](../Page/藝術.md "wikilink")、[設計](../Page/設計.md "wikilink")、[時尚](../Page/時裝.md "wikilink")、[媒體學院有著各自的起源和歷史](../Page/媒體研究.md "wikilink")，這些學院於1986年在政府的政策之下結合成立了「倫敦學院」（London
Institute）。這六所學院的歷史可回溯到十九世紀中葉。在[1988年教育改革法的推動下](../Page/1988年教育改革法.md "wikilink")，倫敦學院成為單一的法人機構，而第一個監管機構則在隔年的1989年成立。指派的首任校長為約翰·麥坎錫教授（John
McKenzie）。倫敦學院在1991年被納入[高等教育體系](../Page/高等教育.md "wikilink")，並隨後在1993年於[英國樞密院的許可下](../Page/英國樞密院.md "wikilink")，獲准提供[學位](../Page/學位.md "wikilink")。[威廉·懷特](../Page/威廉·懷特.md "wikilink")（Will
Wyatt）同年被指派擔任學校的董事。

## 学术

该大学时英国最好的艺术大学之一，但整体排名在国内并不靠前。

## 著名校友

<table>
<thead>
<tr class="header">
<th><p>學院</p></th>
<th><p>姓名(職業)</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>中央聖馬丁校友包括：</p></td>
<td><ul>
<li><a href="../Page/Jarvis_Cocker.md" title="wikilink">Jarvis Cocker</a> (音樂家)</li>
<li><a href="../Page/Anthony_James_(artist).md" title="wikilink">Anthony James</a> (藝術家)</li>
<li><a href="../Page/Sophia_Kokosalaki.md" title="wikilink">Sophia Kokosalaki</a> (時裝設計師)</li>
<li><a href="../Page/Stella_McCartney.md" title="wikilink">Stella McCartney</a> (時裝設計師)</li>
<li><a href="../Page/Alexander_McQueen.md" title="wikilink">Alexander McQueen</a> (時裝設計師)</li>
<li><a href="../Page/Matthew_Williamson.md" title="wikilink">Matthew Williamson</a> (時裝設計師)</li>
<li><a href="../Page/Archelmis_Kwok.md" title="wikilink">Archelmis Kwok</a> (時裝公關)</li>
</ul></td>
</tr>
<tr class="even">
<td><p>倫敦時裝學院校友包括:</p></td>
<td><ul>
<li><a href="../Page/Erin_O&#39;Connor.md" title="wikilink">Erin O'Connor</a> (模特兒)</li>
<li><a href="../Page/Ioana_Ciolacu.md" title="wikilink">Ioana Ciolacu</a> (時裝設計師)</li>
<li><a href="../Page/Mich_Dulce.md" title="wikilink">Mich Dulce</a> (時裝設計師)</li>
<li><a href="../Page/Kirsty_Gallacher.md" title="wikilink">Kirsty Gallacher</a> (體育主持人)</li>
<li><a href="../Page/Bibi_Russell.md" title="wikilink">Bibi Russell</a> (時裝設計師)</li>
<li><a href="../Page/Chris_Liu.md" title="wikilink">Chris Liu</a> (時裝設計師)</li>
<li><a href="../Page/Patrick_Cox.md" title="wikilink">Patrick Cox</a> (鞋類設計師)</li>
<li><a href="../Page/Rachel_Stevens.md" title="wikilink">Rachel Stevens</a> (明星)</li>
<li><a href="../Page/Alek_Wek.md" title="wikilink">Alek Wek</a> (模特兒)</li>
<li><a href="../Page/William_Tempest.md" title="wikilink">William Tempest</a> (時裝設計師)</li>
<li><a href="../Page/Alexander_McQueen.md" title="wikilink">Alexander McQueen</a> (時裝設計師)</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>倫敦時裝學院學生Cordwainers學院校友包括：</p></td>
<td><ul>
<li><a href="../Page/Jimmy_Choo.md" title="wikilink">Jimmy Choo</a> (設計師-鞋)</li>
<li><a href="../Page/Patrick_Cox.md" title="wikilink">Patrick Cox</a> (設計師-鞋)</li>
<li><a href="../Page/Beatrix_Ong.md" title="wikilink">Beatrix Ong</a> (設計師-鞋)</li>
</ul></td>
</tr>
<tr class="even">
<td><p>倫敦傳播學院校友包括：</p></td>
<td><ul>
<li><p>(工程繪圖師)</p></li>
<li>Sarah Lucas (藝術家)</li>
<li>Michael Kenna （攝影師）</li>
<li>Bob Carlos Clarke （攝影師）</li>
<li>Bonnie Francesca Wright（演員）</li>
<li>Jefferson hack (Dazed雜誌的CEO)</li>
</ul></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 學費

<table>
<thead>
<tr class="header">
<th><p>本科學費/年</p></th>
<th><p>本地/歐盟學生</p></th>
<th><p>國際學生</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2016-17學年</p></td>
<td><p>£9,000</p></td>
<td><p>新生開始於2016年9月: £17,230 舊生開始於2015年9月: £16,270</p>
<p>舊生開始於2014年9月: £15,760</p>
<p>舊生開始於2012年9月或2013: £14,750</p></td>
</tr>
<tr class="even">
<td><p>2015-16學年</p></td>
<td><p>£9,000</p></td>
<td><p>新生開始於2015年9月:£15,950 舊生開始於2014年9月: £15,450</p>
<p>舊生開始於2012年9月或2013: £14,450</p></td>
</tr>
</tbody>
</table>

\*國際學生受到學生課程後續幾年的通貨膨脹調整（學費按年最多增加5％）

## 資料來源

## 外部連結

  - [倫敦藝術大學](http://www.arts.ac.uk/)
  - [倫敦藝術大學學生會](http://www.suarts.org/)
  - [倫敦藝術大學台灣學生會](https://web.archive.org/web/20060912094047/http://ual.taiwanese.co.uk/)

[Category:伦敦艺术大学](../Category/伦敦艺术大学.md "wikilink")
[Category:1986年創建的教育機構](../Category/1986年創建的教育機構.md "wikilink")

1.
2.
3.