**阳朔**（[前24年](../Page/前24年.md "wikilink")－[前21年](../Page/前21年.md "wikilink")）是[西汉时期](../Page/西汉.md "wikilink")[汉成帝刘骜的第三个](../Page/汉成帝.md "wikilink")[年号](../Page/年号.md "wikilink")，共计4[年](../Page/年.md "wikilink")。

## 起止時間

[辛德勇推測](../Page/辛德勇.md "wikilink")，陽朔五年四至五月間改元鴻嘉\[1\]。

## 纪年

| 阳朔                               | 元年                                 | 二年                                 | 三年                                 | 四年                                 | 五年                                 |
| -------------------------------- | ---------------------------------- | ---------------------------------- | ---------------------------------- | ---------------------------------- | ---------------------------------- |
| [公元](../Page/公元纪年.md "wikilink") | [前24年](../Page/前24年.md "wikilink") | [前23年](../Page/前23年.md "wikilink") | [前22年](../Page/前22年.md "wikilink") | [前21年](../Page/前21年.md "wikilink") | [前20年](../Page/前20年.md "wikilink") |
| [干支](../Page/干支纪年.md "wikilink") | [丁酉](../Page/丁酉.md "wikilink")     | [戊戌](../Page/戊戌.md "wikilink")     | [己亥](../Page/己亥.md "wikilink")     | [庚子](../Page/庚子.md "wikilink")     | [辛丑](../Page/辛丑.md "wikilink")     |

## 注釋

## 参看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 其他有关[阳朔的解释](../Page/阳朔.md "wikilink")。

[Category:西汉年号](../Category/西汉年号.md "wikilink")
[Category:前1世纪年号](../Category/前1世纪年号.md "wikilink")
[Category:前20年代中国](../Category/前20年代中国.md "wikilink")

1.  辛德勇《[改订西汉新莽纪年表](http://www.zggds.pku.edu.cn/004/001/213.pdf)
    》，初刊于《北大史学》2012年第1期，后收录于氏著《建元与改元——西汉新莽年号研究》。