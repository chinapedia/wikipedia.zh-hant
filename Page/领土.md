[Territorial_waters_-_World.svg](https://zh.wikipedia.org/wiki/File:Territorial_waters_-_World.svg "fig:Territorial_waters_-_World.svg")
**领土**，亦稱**疆域**，是指[主权国家所管辖的](../Page/主权国家.md "wikilink")[地区範圍](../Page/地区.md "wikilink")\[1\]，通常包括一个该国[国界](../Page/边界.md "wikilink")（[邊境](../Page/邊境.md "wikilink")）内的[陆地](../Page/陆地.md "wikilink")（即[领陆](../Page/领土#領陸.md "wikilink")）、[內水](../Page/內水.md "wikilink")（包括[河流](../Page/河流.md "wikilink")、[湖泊](../Page/湖泊.md "wikilink")、[内海](../Page/内海.md "wikilink")），以及它们的[底床](../Page/底床.md "wikilink")、[底土和上空](../Page/底土.md "wikilink")（[领空](../Page/领空.md "wikilink")），有时亦会包括[领海](../Page/领海.md "wikilink")。\[2\]

領土與[國家存亡有密切關係](../Page/國家.md "wikilink")，因為領土是[国家的一部份](../Page/国家.md "wikilink")，形成國家的必要[條件](../Page/條件.md "wikilink")，國家行使[主權的地域及顯示出國家獨有的](../Page/主權.md "wikilink")[主權的方式](../Page/主權.md "wikilink")。\[3\]

## 起源

領土的起源可追溯至公元前，當時[人類逐漸由](../Page/人.md "wikilink")[游牧轉至](../Page/游牧.md "wikilink")[農耕](../Page/农业.md "wikilink")，並由[氏族轉至](../Page/氏族.md "wikilink")[部落](../Page/部落.md "wikilink")，擁有獨自的[首領](../Page/領袖.md "wikilink")。部落擁有自己的農田、活動範圍，且[階級制度開始形成](../Page/社會階層.md "wikilink")，漸漸形成[勢力範圍](../Page/勢力範圍.md "wikilink")。勢力範圍是一個部落的管控地區，領土的基本概念形成。後來，[勢力範圍便轉變成領土](../Page/勢力範圍.md "wikilink")，部落轉變成國家，首領轉變成[皇帝或](../Page/皇帝.md "wikilink")[君主](../Page/君主.md "wikilink")，領土正式形成，成為國家的一個象徵。\[4\]

領土在初時定義模糊，且疆界不清，難以考察國家的實際領土。直至近幾百年，領土疆界才逐漸清晰。\[5\]

## 國家領土

[Geo_land2.PNG](https://zh.wikipedia.org/wiki/File:Geo_land2.PNG "fig:Geo_land2.PNG")
國家擁有的領土理論上是不可分割、不可侵犯的。\[6\]國家在其領土上有完全的主權和管轄權，且其他國家理論上無權干涉。\[7\]\[8\]領土大小也象徵著國家的強弱，如冷戰時兩大國[美国與](../Page/美国.md "wikilink")[苏联也是領土超過](../Page/苏联.md "wikilink")9,000,000平方公里的國家。又如殖民時代英法等歐洲列強殖民領土都象徵帝國強弱。\[9\]\[10\]\[11\]領土擁有權糾紛甚會直接導致戰爭，致使國家滅亡。\[12\]

每個國家都擁有不同大小的領土，其現今的領土視乎不同因素，如地理、歷史、民族等。\[13\]

沒有領土的國家稱之為[流亡政府](../Page/流亡政府.md "wikilink")，如二战时期[戴高乐领导的](../Page/夏爾·戴高樂.md "wikilink")[自由法國政府等等](../Page/自由法國.md "wikilink")。\[14\]\[15\]而[教廷亦是一個典型例子](../Page/教廷.md "wikilink")。教廷在1870年[教皇国被吞併至](../Page/教皇国.md "wikilink")1929年[拉特兰条约](../Page/拉特兰条约.md "wikilink")[梵蒂岡城成立期間](../Page/梵蒂冈.md "wikilink")，都是一個沒有任何領土的流亡政府。\[16\]

以下為一個國家在不同種類的領域中所擁有的主權：

|                                                                                                                                                                |
| -------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [外层空间](../Page/外层空间.md "wikilink") <small>（包括[地球轨道](../Page/地球轨道.md "wikilink")、[月球](../Page/月球.md "wikilink")、所有[天體和其軌道](../Page/天体列表.md "wikilink")）</small> |
| 國家[领空](../Page/领空.md "wikilink")                                                                                                                               |
| [領陸表面](../Page/领土#領陸.md "wikilink")                                                                                                                            |
| [內水](../Page/內水.md "wikilink")                                                                                                                                 |
| [底土](../Page/底土.md "wikilink")                                                                                                                                 |
| 大陆架底土                                                                                                                                                          |
|                                                                                                                                                                |
|                                                                                                                                                                |

國家在不同種類的領域中所擁有的主權

## 構成

[中华人民共和国的領土和領水：
](https://zh.wikipedia.org/wiki/File:China_Exclusive_Economic_Zones.png "fig:中华人民共和国的領土和領水：    ")
[Ph_Territorial_Map.png](https://zh.wikipedia.org/wiki/File:Ph_Territorial_Map.png "fig:Ph_Territorial_Map.png")的領陸、領海、毗連區和專屬經濟區\]\]

領土可分為四個部份：[領陸](../Page/领土#領陸.md "wikilink")、[领空](../Page/领空.md "wikilink")、[領水和](../Page/领土#領水.md "wikilink")[底土](../Page/底土.md "wikilink")。\[17\]

### 領陸

领陆是指國家擁有的陸地領土，包括[国界范围内的](../Page/国界.md "wikilink")[大陆部分和所属](../Page/大陆.md "wikilink")[岛屿以至其底土](../Page/岛屿.md "wikilink")。主權國家在領土中有絕對操控權，对所属陆地地表以下深度无限的地下[资源进行开发利用](../Page/资源.md "wikilink")。领陆是国家领土组成的基本部分。領空、領水和底土也是由領陸洐生而來的。\[18\]

### 領水

[Zonmar-zh.png](https://zh.wikipedia.org/wiki/File:Zonmar-zh.png "fig:Zonmar-zh.png")
領水即國家擁有的水系領土，包括[河流](../Page/河流.md "wikilink")、[运河](../Page/运河.md "wikilink")、[领海](../Page/领海.md "wikilink")（包括[内海](../Page/内海.md "wikilink")）與[湖泊](../Page/湖泊.md "wikilink")（或[內陸海](../Page/陸間海.md "wikilink")）。\[19\]

#### 内水

内水是一个由一國領土完全包圍的封閉性湖、河、內海，都被稱為內水。海岸邊緣沿岸國所屬島嶼外緣，與海岸國海岸的連接線內側都屬於內水。注入海中的河流河口，其靠岸邊一側的注口連線內側也算內水。由內水起算12海浬，為領海。主權國家對其內水有完全之司法管轄權，外國船隻在領海中允許的無害通過權，在內水裡是不允許的。\[20\]

[海灣則另有特別規定](../Page/海灣.md "wikilink")。所謂灣乃指兩地岬(即海岸端突出之土地端點)所圈圍的大於半圓的水域。若此水域離陸皆不超過24海浬，則全部視為內水。\[21\]

##### 河流

河流在主權國家領陸內便自動轉變為該國家的領土部份，主權國家在這些河流中有絕對操控權。例如[长江和](../Page/长江.md "wikilink")[黄河是](../Page/黄河.md "wikilink")[中华人民共和国領土的一部份](../Page/中华人民共和国.md "wikilink")。\[22\]

  - 國內河流

國內河流（internal
river）是發源地和河口都在一國境內的河流，也稱為內河。國內河流完全處在所在國管轄之下，除經國家同意，外國船舶無權進入和航行\[23\]。例如法國的[塞納河](../Page/塞納河.md "wikilink")、[羅亞爾河](../Page/羅亞爾河.md "wikilink")、[加龍河](../Page/加龍河.md "wikilink")，義大利的[台伯河](../Page/台伯河.md "wikilink")，英格蘭、蘇格蘭、愛爾蘭的所有河流等\[24\]。

  - 界河

河流在主權國家領陸之間稱為[界河](../Page/界河.md "wikilink")（boundary
river）。界河主權屬於擁有疆界的兩個或以上的主權國家。例如[黑龙江屬於](../Page/黑龙江.md "wikilink")[中華人民共和國和](../Page/中華人民共和國.md "wikilink")[俄羅斯聯邦](../Page/俄羅斯聯邦.md "wikilink")。\[25\]

  - 多國河流

多國河流（multi-national
river）是流經兩個或兩個以上國家的河流。例如中國的[元江流入越南為](../Page/元江.md "wikilink")[紅河](../Page/紅河.md "wikilink")。多國河流分屬於沿岸國所有，各國對流經本國境內的河段享有主權。由於多國河流涉及各沿岸國的共同利益，沿岸國在行使主權時必須顧及其他各段沿岸國的利益，不得濫用，比如上游國家不得故意使河流改道。\[26\]

  - 國際河流

一些跨國河流會被承認為國際河流（international
river），任何國家都能自由[通商](../Page/国际贸易.md "wikilink")，自由[航行](../Page/航行.md "wikilink")，如[欧洲的](../Page/欧洲.md "wikilink")[多瑙河](../Page/多瑙河.md "wikilink")。\[27\]

##### 湖泊或內陸海

湖泊或內陸海在主權國家領陸內便自動轉變為該國家的領土部份，如[贝加尔湖屬於俄羅斯](../Page/贝加尔湖.md "wikilink")。\[28\]

湖泊或內陸海在主權國家的邊疆，便會分由不同國家管理，如[裏海](../Page/裏海.md "wikilink")、[鹹海](../Page/鹹海.md "wikilink")、[五大湖等](../Page/五大湖.md "wikilink")。\[29\]

##### 運河

運河不管由誰挖掘，運河在主權國家的領陸內便屬於該國家的領土。主權國家在這些運河中有絕對操控權。例如[巴拿马运河由美國挖掘](../Page/巴拿马运河.md "wikilink")，但主權屬於[巴拿马共和國](../Page/巴拿马共和國.md "wikilink")。\[30\]

#### 領海

[领海是一个从领海基线](../Page/领海.md "wikilink")，即沿岸国陆地领土及其内水以外，或者[岛国群岛水域以外向海洋延伸](../Page/岛国.md "wikilink")3-12[海里的海域](../Page/海里.md "wikilink")。但各國視實際狀況可能另有規定。一国主权及于领海及其上空和底土。要注意的是，[专属经济区不是領海](../Page/专属经济区.md "wikilink")。\[31\]同樣，國家擁有對海洋的[制海權不代表國家擁有該片海域](../Page/制海權.md "wikilink")。[公海沒有被任何主權國家擁有](../Page/公海.md "wikilink")，因此船隻能自由航行。\[32\]

### 領空

領空是一個主權國家領陸和領水的上空部份，主權國家在這些領空中有絕對或部份操控權。主權國家可以劃分[禁飛區](../Page/禁飛區.md "wikilink")，甚至禁止任何他國飛機進入領空。\[33\]

### 底土

底土是一個主權國家領陸的地底部份，主權國家在這些地下土中有絕對操控權。主權國家能自由地開發底土的地下資源，如[煤](../Page/煤.md "wikilink")、[石油等](../Page/石油.md "wikilink")。有時領海地底屬於底土。\[34\]\[35\]

## 領土取得方式

國家在長期歷史發展過程中都有自己的領土，但國家領土很少是一成不變的。由於某種原因，國家可以取得新的領土，也可以失去領土。國家領土變更是指由於某種自然原因或人為原因，國家領土增加或喪失從而使領土面積發生變化\[36\]。依[國際法](../Page/國際法.md "wikilink")，領土之取得有以下方式。

### 傳統

傳統的領土取得方式有先佔、時效、添附、割讓、征服。\[37\]\[38\]\[39\]

#### 先佔

先佔（occupation）意指一個國家對[無主地行使有效佔領而取得領土主權](../Page/無主地.md "wikilink")\[40\]。例如[俄羅斯先佔](../Page/俄罗斯沙皇国.md "wikilink")[西伯利亞的無人居住之地](../Page/西伯利亞.md "wikilink")，即是典型的先佔例子。

先佔的構成要件有兩項。一為正式佔有，即一個國家做出佔有該領土的意思表示，例如透過發表聲明、宣言或通告的方式，將該領土置於其國家主權之下。二為行政管理，即佔領的形式必須是「有效佔領」，有效佔領的方式如建立行政、立法、司法機構，設立居民點，懸掛國旗等。\[41\]

近代，先佔已經是不可能發生的領土獲得方式，因為除了[南极洲外](../Page/南极洲.md "wikilink")（部份國家對其宣稱擁有主權），世界上所有領土都已被不同國家所擁有。\[42\]

#### 時效

時效（prescription）意指一個國家長期、和平地在他國領土上行使主權，且他國不持續抗議或不提出反對主張，以致於在歷史發展下產生了普遍信念，認為事物現狀符合國際秩序，因而取得該領土的主權。\[43\]\[44\]然而，自從國際法學問世以來，一直有法學家反對以時效作為領土取得方式。\[45\]

時效的應用必須是對著本來屬於他國主權之下的領土，如果是對著[無主地](../Page/無主地.md "wikilink")，那便是[先佔](../Page/#先佔.md "wikilink")（occupation），而不是時效的開始。另一方面，佔領國行使的必須是主權權利，如果僅是行政性質的權利，那便是臨時管理，因此於[軍事佔領區行使的](../Page/軍事佔領.md "wikilink")，承租國於[租借地上行使的](../Page/租借地.md "wikilink")，無論經過多久都不能取得領土主權。\[46\]

時效的完成必須不受干擾以及經過一段連續的必要歲月。如果他國繼續不斷地提出抗議和主張，主權的實際行使就不是不受干擾的，也就未產生所需要的認為事物現狀符合國際秩序的普遍信念。\[47\]至於連續的歲月應該多長才算原主權國已正式拋棄其權利和名義，有的國際法學者認為應該要「超出記憶」（going
beyond
memory），有的國際法學者則認為不能硬性規定，須視個別情況而定。1897年，[英國與](../Page/英國.md "wikilink")[委內瑞拉兩國在其邊界仲裁條約](../Page/委內瑞拉.md "wikilink")《華盛頓條約》中，則訂明英、委邊界土地的時效期限為五十年。一般說來，如果事實主權已與國際秩序相調和，時效期限應可縮短，如果仍未調和，時效就需要較長的歲月。近年交通方便，各國對其領土的每一角落都能控制，放棄領土主權的時效期限應可短於五十年了。\[48\]

由於藉由時效取得領土主權的實例，從來未曾有過，所以許多法學家不承認時效是國際法上取得領土的方式。雖然曾被當作適用時效原則的一例，但該案仲裁員胡伯（Max
Huber）並不明示其裁定是基於時效原則，何況完成時效所須的時日又無國際法規則加以規定。\[49\]

1959年，在[荷蘭與](../Page/荷蘭.md "wikilink")[比利時兩國將其邊界糾紛訴諸](../Page/比利時.md "wikilink")[國際法庭仲裁的](../Page/國際法庭.md "wikilink")「邊境地案」（The
Frontier Land
Case）中，國際法庭的判決就排斥時效的概念，否決荷蘭以時效取得領土主權的訴求，並把爭議的邊界判給了比利時。\[50\]

現今不論成文法或習慣法，對於時效的期間並無具體規定，有說30年、有說50年、或100年、或完全否定時效的概念，至今仍沒有定論。在早期的國際習慣上（如果兩國間的問題有條約規定，則可不依照國際習慣、判例、學說），任何國家若確信其國土被敵意佔有，便有責任提出正式抗議（不必興訟，但該抗議需備案）；若超過了時限下不作抗議或興訟，便不能宣稱該佔有為非法，佔有國便自然成為合法擁有者（起源於早期[列強為爭奪瓜分殖民地利益所設下的規則](../Page/列強.md "wikilink")，然而近百年來此規則已不被普遍接受）。時效是一個具爭議性的領土獲取方式，因此其在現實中實行的可能性不大。

#### 添附

添附（）是指一國領土由於自然原因或人為原因而增加。\[51\]

添附有兩種情況，即自然添附和人工添附。自然添附是指由於自然的作用使國家領土增加，如、[三角洲](../Page/三角洲.md "wikilink")、新生島嶼、廢河床等。人工添附是指通過人為作用使領土面積擴大，如[圍海造田](../Page/圍墾.md "wikilink")、築堤等。\[52\]

無論是自然添附或人工添附都獲得國際社會的普遍承認。按國際慣例，添附使領土面積增加，國家無須宣告，也無須其他國家承認。但人工添附有一定的限制，例如[界河的沿岸國在改變其本國領土的自然狀態時不得使鄰國領土的自然狀態遭受不利](../Page/界河.md "wikilink")；依據《[聯合國海洋法公約](../Page/聯合國海洋法公約.md "wikilink")》，一國的近海設施和人工島嶼，一國在[專屬經濟區](../Page/專屬經濟區.md "wikilink")、[大陸棚及](../Page/大陸棚.md "wikilink")[公海上建造的人工島嶼](../Page/公海.md "wikilink")、設施和結構都不構成領土的添附。\[53\]

|                                                                                                                                                              |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| [Hong_Kong_reclamation.png](https://zh.wikipedia.org/wiki/File:Hong_Kong_reclamation.png "fig:Hong_Kong_reclamation.png")的填海工程，綠色為天然地，灰色為填海所得地，紅色為計劃填海\]\] |

#### 割讓

割讓（）是指一國的領土依據[條約移轉給另一國](../Page/條約.md "wikilink")。割讓可分為強制性的和非強制性的。\[54\]

強制性割讓通常是戰爭或武力脅迫的結果，即一國通過武力以簽定[和約的形式迫使他國將領土無代價地移轉給自己](../Page/和約.md "wikilink")。傳統國際法上的割讓，嚴格意義上是指這類割讓，當時戰爭是解決國家爭議的合法手段，強制性割讓也是取得領土的合法方式\[55\]。歷史上的例子有：1870年[普法戰爭](../Page/普法戰爭.md "wikilink")，法國割讓[亞爾薩斯與洛林兩省給德國](../Page/亞爾薩斯-洛林.md "wikilink")；1842年[鴉片戰爭後](../Page/第一次鸦片战争.md "wikilink")[中國割讓香港島給](../Page/南京條約.md "wikilink")[英國](../Page/英国.md "wikilink")，1856年[第二次鴉片戰爭後四年再度](../Page/第二次鸦片战争.md "wikilink")[割讓九龍半島](../Page/北京条约.md "wikilink")，1894年[甲午戰爭後](../Page/甲午戰爭.md "wikilink")，[中國割讓](../Page/马关条约.md "wikilink")[遼東半島](../Page/辽东半岛.md "wikilink")[(後因三國干涉而贖回)](../Page/三國干涉還遼.md "wikilink")、[台灣島及附屬島嶼](../Page/台灣.md "wikilink")、[澎湖群島給日本](../Page/澎湖群島.md "wikilink")；1905年[日俄戰爭](../Page/日俄戰爭.md "wikilink")，[俄羅斯割讓](../Page/朴資茅斯條約.md "wikilink")[庫頁島南部及其附近一切島嶼給日本](../Page/庫頁島.md "wikilink")；1898年[美西戰爭](../Page/美西戰爭.md "wikilink")，西班牙割讓[菲律賓](../Page/菲律賓.md "wikilink")、[波多黎各](../Page/波多黎各.md "wikilink")、[西印度群島](../Page/西印度群島.md "wikilink")、[關島給美國](../Page/關島.md "wikilink")\[56\]。由於[現代國際法已廢除了國家的戰爭權](../Page/巴黎非戰公約.md "wikilink")，強制性割讓也隨之失去其合法性\[57\]。

非強制性割讓是國家在平等自願的基礎上，締結條約和平移轉部份領土，這在現代國際法上是合法有效的方式。其形式有購買、贈與、交換：\[58\]

  - 購買（purchase），即國家向領土原持有者購買領土。例如1803年美國以一千五百萬美元向法國[購買路易斯安那](../Page/路易斯安那購地.md "wikilink")；1867年美國以七百二十萬美元向俄羅斯[購買阿拉斯加](../Page/阿拉斯加易手.md "wikilink")；1899年德國以二千五百萬[比塞塔向西班牙購買](../Page/比塞塔.md "wikilink")[加羅林群島](../Page/加羅林群島.md "wikilink")；1916年美國以二千五百萬美元向丹麥購買[西印度群島](../Page/丹麥屬西印度群島.md "wikilink")。\[59\]\[60\]\[61\]
  - 贈與（gift），即一國贈送領土給另一國。例如1950年英國贈與[伊利湖中的馬蹄礁](../Page/伊利湖.md "wikilink")（Horseshoe
    Reef）給美國；1866年奧地利將[威尼斯贈與法國](../Page/威尼斯.md "wikilink")，法國再轉贈與義大利。\[62\]\[63\]
  - 交換（exchange），即國與國以各自的領土進行交換。例如1875年[日本與俄羅斯簽訂條約](../Page/庫頁島千島群島交換條約.md "wikilink")，以[庫頁島換取俄羅斯的](../Page/庫頁島.md "wikilink")[千島群島](../Page/千島群島.md "wikilink")；1890年英國與德國簽訂《》，以[黑爾戈蘭島交換德國的](../Page/黑爾戈蘭島.md "wikilink")[尚西巴群島](../Page/尚西巴群島.md "wikilink")；\[64\]1960年[中國與緬甸簽訂條約交換部份領土](../Page/中華人民共和國和緬甸聯邦邊界條約.md "wikilink")。\[65\]

|                                                                                                                                                                                                  |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| [U.S._Territorial_Acquisitions-zh-classical.png](https://zh.wikipedia.org/wiki/File:U.S._Territorial_Acquisitions-zh-classical.png "fig:U.S._Territorial_Acquisitions-zh-classical.png")圖。\]\] |

#### 征服

征服（）是指國家以武力佔領他國領土的部份或全部，戰後經由兼併（annexation）而取得領土的主權。\[66\]\[67\]

|                                                                                                                         |                                                                                                                              |
| ----------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------- |
| [Mongol_Empire_map.gif](https://zh.wikipedia.org/wiki/File:Mongol_Empire_map.gif "fig:Mongol_Empire_map.gif")的大征服\]\] | [The_British_Empire.png](https://zh.wikipedia.org/wiki/File:The_British_Empire.png "fig:The_British_Empire.png")征服世界各地\]\] |

依據傳統國際法，有效的征服必須滿足一定的條件：征服國正式表示兼併戰敗國領土的意思。如果兼併的是戰敗國的部份領土，戰敗國須放棄收復失地的企圖；如果兼併的是戰敗國的全部領土，征服國須對該國的全部領土實行有效的控制，同時戰敗國及其盟國須放棄一切抵抗。\[68\]

現代國際法嚴格禁止侵略戰爭，因此以征服取得被征服者的領土主權，是無效的、非法的。例如1990年，伊拉克武力侵佔科威特，並將其兼併，宣佈其為伊拉克的第19個省，聯合國不僅宣佈伊拉克的行為無效，而且授權其成員國對其進行制裁。\[69\]

### 其他

#### 公民投票

公民投票（referendum），簡稱公投，即由公民[投票決定領土屬於何國](../Page/投票.md "wikilink")，或是否要獨立建國\[70\]\[71\]。例如1919年《[凡爾賽條約](../Page/凡爾賽條約.md "wikilink")》規定德國[薩爾地區](../Page/薩爾.md "wikilink")[交由國際聯盟託管十五年後再由當地居民決定其歸屬](../Page/薩爾_\(國際聯盟\).md "wikilink")，後來，結果為回歸德國；1961年[英屬喀麥隆舉行公民投票](../Page/喀麦隆#.E8.84.B1.E7.A6.BB.E6.AE.96.E6.B0.91.E5.9C.B0.E6.97.B6.E6.9C.9F.md "wikilink")，結果北部居民決定與[奈及利亞合併](../Page/奈及利亞.md "wikilink")，南部居民決定與[喀麥隆合併](../Page/喀麥隆.md "wikilink")；1967年決定領土歸屬；1989年[納米比亞舉行公民投票決定領土歸屬](../Page/納米比亞.md "wikilink")\[72\]；2014年[克里米亞舉行](../Page/克里米亞自治共和國.md "wikilink")[公民投票決定領土歸屬](../Page/2014年克里米亞歸屬公投.md "wikilink")。

|                                                                                                                                                                           |                                                                                                                                                                                                                                 |                                                                                                                                                                                    |                                                                                                                                                                                                                                    |                                                                                                                                                    |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------- |
| [Algeria_(orthographic_projection).svg](https://zh.wikipedia.org/wiki/File:Algeria_\(orthographic_projection\).svg "fig:Algeria_(orthographic_projection).svg")而獨立的\]\] | [Mongolian_People's_Republic_Orthographic_projection.svg](https://zh.wikipedia.org/wiki/File:Mongolian_People's_Republic_Orthographic_projection.svg "fig:Mongolian_People's_Republic_Orthographic_projection.svg")而獨立的\]\] | [Locator_map_Montenegro_in_Yugoslavia.svg](https://zh.wikipedia.org/wiki/File:Locator_map_Montenegro_in_Yugoslavia.svg "fig:Locator_map_Montenegro_in_Yugoslavia.svg")而獨立的\]\] | [South_Sudan_Sudan_Locator_(orthographic_projection).svg](https://zh.wikipedia.org/wiki/File:South_Sudan_Sudan_Locator_\(orthographic_projection\).svg "fig:South_Sudan_Sudan_Locator_(orthographic_projection).svg")而獨立的\]\] | [Crimean_referendum_2014.svg](https://zh.wikipedia.org/wiki/File:Crimean_referendum_2014.svg "fig:Crimean_referendum_2014.svg")而獨立并加入俄羅斯聯邦的和\]\] |

## 領土受到暫時控制

在特殊情況下，領土可能只受到暫時控制，而沒有被控制者真正取得。

### 租借

租借（），即在不同情況下（如[戰敗](../Page/戰敗.md "wikilink")），將領土在一個有效期內[租借予別國](../Page/租借.md "wikilink")，借地國擁有該地區的行政權以治理該地區（如警察權），但租國仍保留主權（只是已經喪失當地的行政權），稱為租借。例子如清朝租借[新界予大英帝國](../Page/新界.md "wikilink")。\[73\]\[74\]

### 共管

[共管](../Page/共管_\(國際法\).md "wikilink")（condominium），即一片領土受兩個或以上的國家管理，形成沒有國家擁有對該領土的絕對主權。例子有20世紀初的[英埃共管蘇丹](../Page/英埃共管蘇丹.md "wikilink")（[英國與](../Page/英國.md "wikilink")[埃及共同管治](../Page/埃及.md "wikilink")）。\[75\]

|                                                                                                                                                                   |
| ----------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [Egypt_sudan_under_british_control.jpg](https://zh.wikipedia.org/wiki/File:Egypt_sudan_under_british_control.jpg "fig:Egypt_sudan_under_british_control.jpg") |

### 託管

[League_of_Nations_mandate_Middle_East_and_Africa.png](https://zh.wikipedia.org/wiki/File:League_of_Nations_mandate_Middle_East_and_Africa.png "fig:League_of_Nations_mandate_Middle_East_and_Africa.png")

託管（），戰敗國將殖民地交給戰勝國（如日本二戰戰敗後期將領土暫時交予美國管理）或其他原因，暫時將領土交予別國，稱為託管。接受託管領土的國家職責在於保護該領土或協助其獨立，而且不會擁有對該領土的絕對主權，該國家要在一些情況下（如託管期限結束），放棄該領土。\[76\]

### 軍事佔領

軍事佔領（military occupation）只是對佔領地進行暫時的管理，並不能取得佔領地的領土主權。\[77\]\[78\]\[79\]

### 國際管理

即領土受國際管理，沒有主權國家擁有該領土，稱為國際託管。[南极洲便是一例子](../Page/南极洲.md "wikilink")。南極洲因於1961年生效的《[南极条约体系](../Page/南极条约体系.md "wikilink")》凍結一切對南極的領土要求，並禁止新的領土要求，所以沒有任何一國對南極洲擁有主權。而且，《南極條約體系》亦列明要求締約國對各自派出的觀察員和科學家行使管轄權，即受國際管理。\[80\]

## 未來

在[未来](../Page/未来.md "wikilink")，領土可能不只包括在[地球上](../Page/地球.md "wikilink")。領土可能擴展至[月球](../Page/月球.md "wikilink")、[行星](../Page/行星.md "wikilink")、[太阳系甚至是](../Page/太阳系.md "wikilink")[银河系](../Page/银河系.md "wikilink")。但在現代，根據《[外层空间条约](../Page/外层空间条约.md "wikilink")》，這些天體暫時不會成為任何一國的領土。\[81\]\[82\]

## 領土爭議

領土爭議，是指兩個國家或以上對同一塊土地均宣示主權而引發的糾紛。\[83\][主權國與主權國之間的](../Page/主权国家.md "wikilink")[領土紛爭](../Page/領土糾紛.md "wikilink")，通常是因歸還領土主權、時效、先佔等原因而引發出來的。\[84\]

### 部份領土爭議列表

#### 非洲

[Lake_Malawi_seen_from_orbit.jpg](https://zh.wikipedia.org/wiki/File:Lake_Malawi_seen_from_orbit.jpg "fig:Lake_Malawi_seen_from_orbit.jpg")

  - [卡宾达](../Page/卡宾达市.md "wikilink")：[安哥拉和](../Page/安哥拉.md "wikilink")[刚果](../Page/刚果.md "wikilink")
  - [休达和](../Page/休达.md "wikilink")[梅利利亚](../Page/梅利利亚.md "wikilink")：[西班牙和](../Page/西班牙.md "wikilink")[摩洛哥](../Page/摩洛哥.md "wikilink")
  - [佩雷希爾島](../Page/佩雷希爾島.md "wikilink")：西班牙和摩洛哥
  - [格洛里厄斯群岛](../Page/格洛里厄斯群岛.md "wikilink")：[法国](../Page/法国.md "wikilink")、[马达加斯加和](../Page/马达加斯加.md "wikilink")[塞舌尔](../Page/塞舌尔.md "wikilink")
  - [马拉维湖](../Page/马拉维湖.md "wikilink")：[马拉维和](../Page/马拉维.md "wikilink")[坦桑尼亚](../Page/坦桑尼亚.md "wikilink")
  - [马约特](../Page/马约特.md "wikilink")：法国和[葛摩](../Page/葛摩.md "wikilink")
  - [欧加登](../Page/欧加登.md "wikilink")：[埃塞俄比亚和](../Page/埃塞俄比亚.md "wikilink")[索马里](../Page/索马里.md "wikilink")
  - [特罗姆兰岛](../Page/特罗姆兰岛.md "wikilink")：法国、[毛里求斯和马达加斯加](../Page/毛里求斯.md "wikilink")

#### 美洲

[Rio_San_Juan_Department_in_Nicaragua.svg](https://zh.wikipedia.org/wiki/File:Rio_San_Juan_Department_in_Nicaragua.svg "fig:Rio_San_Juan_Department_in_Nicaragua.svg")

  - [福克兰群岛](../Page/福克兰群岛.md "wikilink")：[英国和](../Page/英国.md "wikilink")[阿根廷](../Page/阿根廷.md "wikilink")
  - [聖胡安河省](../Page/聖胡安河省.md "wikilink")：[哥斯达黎加和](../Page/哥斯达黎加.md "wikilink")[尼加拉瓜](../Page/尼加拉瓜.md "wikilink")
  - [南喬治亞島與南桑威奇群島](../Page/南喬治亞島與南桑威奇群島.md "wikilink")：英国和阿根廷

#### [亚洲和大洋洲](../Page/亚太地区.md "wikilink")

[Senkaku-Diaoyu-Tiaoyu-Islan.jpg](https://zh.wikipedia.org/wiki/File:Senkaku-Diaoyu-Tiaoyu-Islan.jpg "fig:Senkaku-Diaoyu-Tiaoyu-Islan.jpg")

  - [阿克塞钦](../Page/阿克塞钦.md "wikilink")：[中华人民共和国和印度](../Page/中华人民共和国.md "wikilink")
  - [藏南地區](../Page/藏南地區.md "wikilink")：印度和中华人民共和国
  - [突厥斯坦州](../Page/突厥斯坦.md "wikilink")：[乌兹别克斯坦和](../Page/乌兹别克斯坦.md "wikilink")[哈萨克斯坦](../Page/哈萨克斯坦.md "wikilink")
  - [不丹北部](../Page/中不边界争议.md "wikilink")：[中华人民共和国和不丹](../Page/中华人民共和国.md "wikilink")
  - [东耶路撒冷](../Page/东耶路撒冷.md "wikilink")：[以色列](../Page/以色列.md "wikilink")、[联合国和](../Page/联合国.md "wikilink")[巴勒斯坦](../Page/巴勒斯坦.md "wikilink")
  - [戈蘭高地](../Page/戈蘭高地.md "wikilink")：以色列和[叙利亚](../Page/叙利亚.md "wikilink")
  - [克什米尔](../Page/克什米尔.md "wikilink")：印度和[巴基斯坦](../Page/巴基斯坦.md "wikilink")
  - [臺灣](../Page/臺灣.md "wikilink")、[澎湖](../Page/澎湖群島.md "wikilink")、[金門](../Page/金門縣.md "wikilink")、[馬祖及](../Page/馬祖列島.md "wikilink")[中国大陆](../Page/中国大陆.md "wikilink")：中華民國和中华人民共和国
  - [朝鲜半岛](../Page/朝鲜半岛.md "wikilink")：[朝鲜民主主义人民共和国和](../Page/朝鲜民主主义人民共和国.md "wikilink")[大韩民国](../Page/大韩民国.md "wikilink")
  - [南千岛群岛](../Page/南千岛群岛.md "wikilink")：[俄罗斯和](../Page/俄罗斯.md "wikilink")[日本](../Page/日本.md "wikilink")
  - [獨島](../Page/獨島.md "wikilink")（日稱：竹島）：大韓民國和日本
  - [中沙群島](../Page/中沙群島.md "wikilink")：[中华人民共和国](../Page/中华人民共和国.md "wikilink")、中华民国和菲律宾
  - [西沙群岛](../Page/西沙群岛.md "wikilink")：中華民國、越南和[中华人民共和国](../Page/中华人民共和国.md "wikilink")
  - [釣魚臺列嶼](../Page/釣魚臺列嶼.md "wikilink")（日稱：尖閣諸島）：中華民國、日本和中华人民共和国
  - [南沙群岛](../Page/南沙群岛.md "wikilink")：中華民國、中华人民共和国、越南、
    菲律宾（部分）、[马来西亚](../Page/马来西亚.md "wikilink")（部分）、[文莱](../Page/文莱.md "wikilink")（部分）和[印尼](../Page/印度尼西亚.md "wikilink")（部分）

## 参考文献

### 引用

### 来源

  -
  -
  -
  -
  -
## 外部連結

  - [世界各国和地区面积列表](../Page/世界各国和地区面积列表.md "wikilink")

[疆域](../Category/疆域.md "wikilink") [LT](../Category/國家權力.md "wikilink")
[Category:国际法](../Category/国际法.md "wikilink")
[Category:行政领土实体](../Category/行政领土实体.md "wikilink")

1.

2.

3.

4.
5.

6.

7.  Newton, Kenneth. Foundations of comparative politics: democracies of
    the modern world. Cambridge: Cambridge University Press, 2005.

8.

9.

10.

11.

12. [Power Transitions as the cause of
    war](http://jcr.sagepub.com/cgi/content/abstract/32/1/87).

13.

14.

15. [Princeton University
    WordNet](http://wordnetweb.princeton.edu/perl/webwn?s=government-in-exile)

16. Robert Araujo and John Lucal, Papal Diplomacy and the Quest for
    Peace, the Vatican and International Organizations from the early
    years to the League of Nations, Sapienza Press (2004), ISBN
    978-1-932589-01-6, p. 16. *See also* James Crawford, The Creation of
    States in International Law, (1979) p. 154.

17. [Peace Palace Library - Research
    Guide](http://www.peacepalacelibrary.nl/research-guides/public-international-law-special-topics/territory/)

18.

19.
20. [UN Convention on the Law of the Sea: Territorial Sea and Contiguous
    Zone](http://www.un.org/Depts/los/convention_agreements/texts/unclos/part2.htm)

21.

22.
23.
24.

25.
26.
27.
28.
29.
30.
31. [UN Convention on the Law of the Sea: Exclusive Economic
    Zone](http://www.un.org/Depts/los/convention_agreements/texts/unclos/part5.htm)

32. [UN Convention on the Law or the Sea: Continental
    Shelf](http://www.un.org/Depts/los/convention_agreements/texts/unclos/part6.htm)

33. [U.S. airspace, as described in the Aeronautical Information
    Manual](http://www.faa.gov/air_traffic/publications/ATpubs/AIM/aim0301.html)


34.

35.

36.
37.
38.

39.
40.
41.
42. [Occupation and other forms of administration of foreign
    territory](http://www.icrc.org/eng/assets/files/publications/icrc-002-4094.pdf)

43.
44.
45.
46.
47.
48.
49.
50.
51.
52.
53.
54.
55.
56.

57.
58.
59.
60.
61.
62.
63.
64.
65.
66.
67.
68.
69.
70.
71.
72.
73. [WorldStatesmen—also by concession
    holder](http://www.worldstatesmen.org/COLONIES.html)—or by country
    for non-colonial territories

74. Chisholm, Hugh, ed. (1911). Encyclopædia Britannica (11th ed.).
    Cambridge University Press

75.

76.

77.

78.
79.
80. [维基文库：《南極條約體系》](../Page/:s:南极条约.md "wikilink")

81.

82.

83. [USState Department/ FloridaStateUniversity International Border
    Studies](http://www.law.fsu.edu/library/collection/LimitsinSeas/numericalibs-template.html)


84. [Lectures](http://untreaty.un.org/cod/avl/ls/Shaw_BD.html) by
    Malcolm Shaw entitled *The International Legal Principles Relating
    to Territorial Disputes: The Acquisition of Title to Territory* and
    *Settling Territorial Disputes* in the [Lecture Series of the United
    Nations Audiovisual Library of International
    Law](http://untreaty.un.org/cod/avl/lectureseries.html)