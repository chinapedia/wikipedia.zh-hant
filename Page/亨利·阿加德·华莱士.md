**亨利·阿加德·华莱士**（**Henry Agard
Wallace**，），[美国政治家](../Page/美国.md "wikilink")，曾任[美国农业部部长](../Page/美国农业部.md "wikilink")、[美国副总统和](../Page/美国副总统.md "wikilink")[美国商务部长](../Page/美国商务部长.md "wikilink")。

## 副總統

1940年，华莱士獲[美國總統](../Page/美國總統.md "wikilink")[富蘭克林·德拉諾·羅斯福提名為民主黨副總統候選人](../Page/富蘭克林·德拉諾·羅斯福.md "wikilink")，在羅斯褔第三個總統任期中擔任副總統。1944年[第二次世界大戰快結束時](../Page/第二次世界大戰.md "wikilink")，由於华莱士與羅斯福的政策理念出現分歧，因此即使民主黨內有意願讓他出選，他不獲再度提名，羅斯福提名[哈利·杜魯門為副總統候選人](../Page/哈利·杜魯門.md "wikilink")；而當時羅斯褔的健康狀況極度欠佳、被認為不久於人世，副總統人選相當於羅斯福的接班人。他在第四個任期開始不到三個月便去世，华莱士差點便成為美國總統。

## 商務部長

华莱士卸任副總統後，他獲羅斯福總統提名為商務部長，他亦是至今為止，在卸任副總統後進入內閣的人。1946年，美國總統[哈里·S·杜鲁门采取步骤开始与](../Page/哈里·S·杜鲁门.md "wikilink")[苏联的](../Page/苏联.md "wikilink")[冷战时](../Page/冷战.md "wikilink")，华莱士公开表示反对，被杜鲁门撤职。此时的民主党各派已面临分崩离析的局面。

## 1948年總統選舉

[1948年美國總統選舉](../Page/1948年美國總統選舉.md "wikilink")，华莱士退出民主黨並成立了进步党參選總統，试图与杜鲁门分庭抗礼。华莱士的竞选纲领反对[冷战](../Page/冷战.md "wikilink")，也反对[马歇尔计划和](../Page/马歇尔计划.md "wikilink")[杜鲁门主义](../Page/杜鲁门主义.md "wikilink")。他希望政府管制更加有力并对加强对大企业的控制，要求结束对黑人和妇女的歧视，支持[最低工资](../Page/最低工资.md "wikilink")。在[麥卡錫主義时代](../Page/麥卡錫主義.md "wikilink")，他呼吁解散旨在调查美国政府机构和工会中“[共谍](../Page/共产主义.md "wikilink")”的[众议院](../Page/美国众议院.md "wikilink")[非美调查委员会](../Page/非美调查委员会.md "wikilink")，理由是后者以反共为名侵犯了[公民自由](../Page/公民自由.md "wikilink")。然而，进步党自成立之初便颇受争议，主要原因即是人们普遍认为该党受到[美国共产党的秘密控制](../Page/美国共产党.md "wikilink")，而后者对苏联比起对美国更忠诚。华莱士否认自己是共产党人，但他也不肯表态拒绝共产党的支持，他甚至有一次说：“我看那些共产党人倒是和[基督教早期的](../Page/基督教.md "wikilink")[殉道者最为相像](../Page/殉道者.md "wikilink")。”\[1\]这种立场引来了很多批评。在[费城举行的进步党大会召集了](../Page/费城.md "wikilink")3200名代表，这个数目甚至超过了民主、共和两党代表大会的与会人数之和，但这次大会也引起了很多争议：许多著名记者，如H·L·门肯和多萝西·汤普森，都公开谴责进步党为共产党所秘密控制。最终，华莱士选择了[爱达荷州参议员](../Page/爱达荷州.md "wikilink")[格伦·泰勒作为其竞选伙伴](../Page/格伦·泰勒.md "wikilink")。即使在民主黨分裂的情形下，华莱士在总统选举失败，之后进步党解散。

华莱士評論[法西斯主義時說](../Page/法西斯主義.md "wikilink")，與[主權國家的政府比起來](../Page/主權國家.md "wikilink")，真正掌控全球權力的是[跨國企業](../Page/跨國企業.md "wikilink")，而且以美國的跨國企業為主：「如果將法西斯的內涵定義在一種『以金錢與權力最大化為終極目標，並不惜利用各種手段達到它』，那美國已有成千上萬個法西斯主義者了。（If
we define an American fascist as one who in case of conflict puts money
and power ahead of human beings, then there are undoubtedly several
million fascists in the United States.）」

## 参考文献

{{-}}

[Category:美國總統候選人](../Category/美國總統候選人.md "wikilink")
[Category:美國民主黨副總統候選人](../Category/美國民主黨副總統候選人.md "wikilink")
[Category:1948年美國總統選舉](../Category/1948年美國總統選舉.md "wikilink")
[Category:1940年美國總統選舉](../Category/1940年美國總統選舉.md "wikilink")
[Category:美国农业部长](../Category/美国农业部长.md "wikilink")
[Category:美国商务部长](../Category/美国商务部长.md "wikilink")
[Category:美国民主党副总统](../Category/美国民主党副总统.md "wikilink")
[Category:愛荷華州立大學校友](../Category/愛荷華州立大學校友.md "wikilink")
[Category:美國聖公宗教徒](../Category/美國聖公宗教徒.md "wikilink")
[Category:愛爾蘭裔美國人](../Category/愛爾蘭裔美國人.md "wikilink")
[Category:蘇格蘭裔美國人](../Category/蘇格蘭裔美國人.md "wikilink")
[Category:愛荷華州人](../Category/愛荷華州人.md "wikilink")
[Category:苏格兰－爱尔兰裔美国人](../Category/苏格兰－爱尔兰裔美国人.md "wikilink")

1.  (Ross, p. 162)