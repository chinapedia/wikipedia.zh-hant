**贝加尔山脉**（Байкальский
хребет），是[俄罗斯](../Page/俄罗斯.md "wikilink")[贝加尔湖西北沿岸的一座陡峭高山山脉](../Page/贝加尔湖.md "wikilink")，东北—西南走向，为[中西伯利亚高原的南缘](../Page/中西伯利亚高原.md "wikilink")，长约300公里，为贝加尔湖水系和[勒拿河水系的分水岭](../Page/勒拿河.md "wikilink")，勒拿河发源于此。贝加尔山脉最高峰[切尔斯基峰海拔](../Page/切尔斯基峰.md "wikilink")2，572米，以波兰探险家[扬·切尔斯基命名](../Page/扬·切尔斯基.md "wikilink")。

## 外部链接

  - [照片](http://www.magicbaikal.ru/album-en/burkhan/pages/baikal-40f10.htm)
  - [贝加尔湖和贝加尔山脉照片](http://www.irkutsk.org/baikal/gallery2.htm)

[B](../Category/俄罗斯山脉.md "wikilink")