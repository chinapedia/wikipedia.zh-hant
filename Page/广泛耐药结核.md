**广泛耐药结核**（**Extensively drug-resistant
tuberculosis**，即**XDR-TB**）是[结核病的一种](../Page/肺结核.md "wikilink")，专指对[利福平](../Page/利福平.md "wikilink")（rifampicin）和[异烟肼](../Page/异烟肼.md "wikilink")（isoniazid）具[抗药性](../Page/抗药性.md "wikilink")（对这些一线治疗结核药物具有抗药性的结核称作“[多药抗药性结核](../Page/多药抗药性结核.md "wikilink")”——MDR-TB），且对全部[喹诺酮](../Page/喹诺酮.md "wikilink")（quinolone）类药物以及至少对二线治疗结核药物中[卡那霉素](../Page/卡那霉素.md "wikilink")（kanamycin）、[卷曲霉素](../Page/卷曲霉素.md "wikilink")（capreomycin）、和[阿米卡霉素](../Page/阿米卡霉素.md "wikilink")（amikacin）之一具有抗药性的结核病。\[1\]

XDR-TB的旧定义是“对六类二线结核治疗药物中三类或三类以上有抗药性的MDR-TB”。\[2\] 该旧定义在旧出版物中被引用，今后则不再使用。

对XDR-TB的治疗手段与MDR-TB的相同。但由于治疗的有效率的降低，XDR-TB的[死亡率要比MDR](../Page/死亡率.md "wikilink")-TB高很多。\[3\]针对XDR-TB的流行病学研究相对有限。目前的认识是，XDR-TB在健康人群中不易传播，但在[免疫力低的人群](../Page/免疫力.md "wikilink")，比如[HIV广泛传播的人群中](../Page/HIV.md "wikilink")，则能造成[流行感染](../Page/流行病.md "wikilink")。\[4\]

## 耐药结核的流行病学研究

1997年的一次在35个国家进行的结核病调查发现结核病在三分之一的国家的死亡率高于2%。死亡率最高的国家是[前苏联](../Page/前苏联.md "wikilink")，[阿根廷](../Page/阿根廷.md "wikilink")、[印度和](../Page/印度.md "wikilink")[中国](../Page/中国.md "wikilink")。这一现象被归因于缺乏有效的结核病控制计划。与此相似，1990年代初在[纽约出现的MDR](../Page/纽约.md "wikilink")-TB高感染率被认为是由公共卫生计划的废止造成。\[5\]\[6\]

MDR-TB可以在普通非耐药结核病治疗过程中产生，无例外的是病人漏服药物或不完成整个治疗周期的结果。

MDR-TB的传播性比普通结核较低。\[7\]死亡率与肺癌相似。

## 南非疫情

2006年末以来，XDR-TB的传染疫情出现在[南非](../Page/南非.md "wikilink")。疫情的爆发最先出现于夸祖鲁那他（KwaZulu-Natal）郊区的一所医院，53个病人中有52名死亡。\[8\]尤其令人担忧的是，病人从唾液取样到死亡的中位存活率只有16天，而且大多数病人之前从来没有接受过结核治疗。虽然符合这次疫情特征的结核病在此前也被发现过，\[9\]\[10\]这次疫情却是最大规模的关联病例，XDR-TB这一名称也从此开始被使用。从2006年9月的初期报告开始\[11\]，南非的多数省份至今都出现了病例。到2007年3月16日统计，共收到314个病例报告，其中215例死亡。\[12\]这次结核病大规模传播明显与当地高HIV感染率以及对传染病控制的缺乏相关。在其他发现XDR-TB的国家，结核病的抗药性均由普通结核病病人未能遵从治疗引起，而非人与人直接传播。\[13\]这次出现的结核病变种对南非当前应用的所有一线二线药物都有抗药性，病情已经存在的时间显然比卫生部门官员所宣称的要长很多，程度也严重很多。\[14\]严厉的隔离措施正被施行，其中包括限制某些病人的人身权利的手段，但为控制病情的传播而不得不被采用。\[15\]

## 安德鲁·斯皮克

2007年5月30日，[美国疾病控制与预防中心](../Page/美国疾病控制与预防中心.md "wikilink")（CDC）主管朱莉·格伯丁（Julie
Gerberding）发布通告，称有一名感染XDR-TB的病人搭乘跨大西洋航线，机上其他乘客及机组成员被因此暴露于XDR-TB。\[16\]后来，该乘客的名字被证实为安德鲁·斯皮克（Andrew
Speaker）。\[17\]斯皮克感染有结核病，在2007年5月12日乘[法航](../Page/法航.md "wikilink")385航班从[亚特兰大飞往](../Page/亚特兰大.md "wikilink")[巴黎](../Page/巴黎.md "wikilink")，之后飞往[希腊举行婚礼](../Page/希腊.md "wikilink")，最后飞往[意大利度蜜月](../Page/意大利.md "wikilink")。据他的家人声称，美国卫生官员告诉斯皮克他的病情不具有传染性。\[18\]到达罗马后，CDC确认斯皮克感染了XDR-TB，并要求他与意大利卫生机构联系，而不要飞回美国。

然而，知道自己被放上了美国的禁飞名单，斯皮克转而乘坐[捷克航空公司](../Page/捷克航空.md "wikilink")0104航班从[布拉格飞到](../Page/布拉格.md "wikilink")[蒙特利尔](../Page/蒙特利尔.md "wikilink")。\[19\]然后他租车开到[纽约市](../Page/纽约市.md "wikilink")，并寻求治疗。CDC把他空运到[亚特兰大](../Page/亚特兰大.md "wikilink")，在格蕾笛纪念医院（Grady
Memorial
Hospital）接受武装警卫控制下的联邦隔离，虽然他并未被起诉。斯皮克的隔离指令是根据公共医疗服务法案的条款发出的，他因此成为美国1963年以来被第一个被CDC隔离的人。\[20\]5月31日，他被转往位于[丹佛的国家犹太医学与研究中心](../Page/丹佛.md "wikilink")（National
Jewish Medical and Research
Center）。CDC正与同斯皮克搭乘同一航班的乘客与机组人员联系，以进行XDR-TB测试。\[21\]

## 参见

  - [肺結核](../Page/肺結核.md "wikilink")
  - [抗药性](../Page/抗药性.md "wikilink")
  - 耐药疾病
      - [抗藥性金黃色葡萄球菌](../Page/抗藥性金黃色葡萄球菌.md "wikilink")
      - [抗萬古黴素腸球菌](../Page/抗萬古黴素腸球菌.md "wikilink")

## 參考資料

## 外部連結

  - [向超级结核杆菌宣战_环球科学_新浪博客](http://blog.sina.com.cn/s/blog_49a555cb0100c6to.html)

[pl:Gruźlica XDR-TB](../Page/pl:Gruźlica_XDR-TB.md "wikilink")

[Category:細菌性疾病](../Category/細菌性疾病.md "wikilink")
[Category:抗生素抗藥性細菌](../Category/抗生素抗藥性細菌.md "wikilink")

1.

2.

3.
4.

5.

6.

7.   [Free Full
    Text](http://www.pubmedcentral.nih.gov/picrender.fcgi?artid=43383&blobtype=pdf).

8.
9.

10.

11.

12.

13.

14.

15.

16.

17.

18. <http://abcnews.go.com/GMA/story?id=3241394&page=1>

19.
20.

21.