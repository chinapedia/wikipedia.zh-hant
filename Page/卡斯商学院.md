|                          |
| :----------------------: |
|                          |
|          卡斯商学院           |
|   Cass Business School   |
|  <small>**校訓**</small>   |
| <small>**建校时间**</small>  |
| <small>**人才培养**</small>  |
| <small>**教学员工**</small>  |
| <small>**本科学生**</small>  |
| <small>**硕士研究生**</small> |
| <small>**博士研究生**</small> |
| <small>**现任院长**</small>  |
| <small>**学校所在地**</small> |
| <small>**官方网站**</small>  |

**卡斯商学院**（），或按官方称呼为**倫敦大學城市學院约翰·卡斯爵士商学院**（），坐落于[英国](../Page/英国.md "wikilink")[伦敦市金融城中心](../Page/倫敦市.md "wikilink")，是[伦敦大学城市學院的商學院](../Page/倫敦大學城市學院.md "wikilink")，為**英國前10強、歐洲前20強、全球前40名的商學院。**

在商业管理研究领域，卡斯商学院在2008英国政府官方的学术研究评估报告（Research Assessment
Exercise）中的得分位居全英商学院前十名。此外，卡斯商学院在各类商学院排名中均连续多年取得了极高的名次、排名最高的為財金類碩士。据英国金融时报（Financial
Times）2013年欧洲商学院排名，卡斯全英位居第4、全欧第18。2017年卡斯財金MBA(MBA for
Finance)排名全球第25位、全英第4位**。**

其精算学科尤其出色，位列欧洲第一，全球第二（仅次于沃顿商学院），也是英国精算师体系内最富盛名的学校。

卡斯商學院也是少數獲得認證[三冠王的商學院](http://www.cass.city.ac.uk/more-about-cass/about-the-school),包括Association
to Advance Collegiate Schools of Business(AACSB), the Association of
MBAs(AMBA) 和 European Quality Improvement System (EQUIS) 的三重認證,
全世界僅有不到70間商學院擁有三重認證的殊榮。

最近卡斯商学院在东伦敦著名的新兴金融区：[金丝雀码头](../Page/金丝雀码头.md "wikilink")（Canary
Wharf）开启了其专为EMBA暨高级经理培训（Executive Education）而设立的第二个校区。此校区位于[One
Canada Square](../Page/加拿大廣場一號.md "wikilink")。

卡斯商学院分成三个系，即：[精算系](../Page/精算.md "wikilink")、[金融系](../Page/金融.md "wikilink")、和[管理系](../Page/管理.md "wikilink")。

学院前任院长为[大衛·柯里教授](../Page/:en:David_Currie,_Baron_Currie_of_Marylebone.md "wikilink")（[馬里波恩](../Page/馬里波恩.md "wikilink")[男爵](../Page/男爵.md "wikilink")，[英国上议院议员](../Page/英国上议院.md "wikilink")），他于2007年四月卸任，继任者是Richard
Gillingwater\[1\]。

## 学校综述

卡斯商学院（伦敦城[约翰·卡斯爵士商学院](../Page/:en:John_Cass.md "wikilink")）是[倫敦大學城市學院的一部分](../Page/倫敦大學城市學院.md "wikilink")。此前名为伦敦城市大学商学院，后于2002年8月因约翰·卡斯爵士基金数百万[英镑的巨额捐赠而改为现名](../Page/英镑.md "wikilink")。

其研究所教學特點為以實務導向多於理論導向，因此其大部分的教授必須有長時間的業界工作經驗才能於卡斯商學院任教,也因此讓卡斯商學院在就業力排名中排全歐洲前20強。

## 排名

  - QS世界大學排名:

<!-- end list -->

1.  根據2013年 QS 全球200強商學院報告 (Global 200 Business Schools Report 2013/14),
    **卡斯商學院在財金(Finance)分類下排名世界第27名,** 全英國第三名, 並且被歸類在最高等之中(Band 3),
    而在營運管理(Operations Management) 分類下排名世界第42位。
2.  根據2017年 [QS
    世界大學排名](https://www.topuniversities.com/universities/city-university-london),
    卡斯商學院在會計與財金,商業與管理項目皆為英國前10名。
3.  根據2017年 QS 世界250強商學院總排名.卡斯商學院排名非常出色。在就業力(Employability)
    項目排名英國第8名,歐洲第18名。並在學術研究中(Research
    Excellence)依舊排名英國第8名,並為歐洲前20強。排名僅次於傳統歐洲名校們之後, 如倫敦商學院LBS,
    法國INSEAD, 德國Mannheim, 義大利Bocconi等。**Cass被推類於"頂尖就業力商學院"(Top-Tier
    Employability business schools)。**

<!-- end list -->

  - 英國[Financial Times財金時報](../Page/金融時報.md "wikilink"):

<!-- end list -->

1.  根据2013年排名，卡斯財金碩士(MSc Finance)世界排名第15位，管理学硕士世界排名第17位，全英國排名第2位。
2.  根据2014年排名，卡斯商學院排名全英國第4位,全歐洲第19位。
3.  根據2017年排名， 其財金碩士(MSc Finance)為世界第20名,全英國排名第4名。

<!-- end list -->

  - 2017 [EDUNIVERSAL
    BestMaster](http://www.best-masters.com/master-at-city-university-cass-business-school.html)排名:

<!-- end list -->

1.  在會計與審計(Accounting & Audit)分類下MSc in International Accounting &
    Finance(國際會計與財金碩士)排名全英國第五位。
2.  在保險(Insurance)分類下 MSc in Insurance & Risk
    Management(保險與風險管理碩士)排名世界第12名,英國第一名。
3.  地產碩士(MSc in Real Estate)世界排名25,英國第二名。
4.  在行銷學分類下其行銷碩士(MSc in Marketing Strategy and Innovation) 排名為全英國第6位。

<!-- end list -->

  - 其他排名:

<!-- end list -->

1.  根據2017年 [TIMES
    高等教育排名](https://www.timeshighereducation.com/world-university-rankings/city-university-london#ranking-dataset/595516),
    其商學和經濟排名為全英國第八名**。**(註: City, University of
    London之經濟系歸類在"藝術與社會科學學院"下非在卡斯商學院下)。
2.  根據2015 [BBC
    報導](http://www.bbc.com/ukchina/trad/uk_education/2015/01/150113_edu_graduate_salary_list),
    卡斯商學院**畢業生收入排名全英國第五位**,僅位於London Business School, University of
    Oxford, Warwick Business School, University of Cambridge等名校之後。
3.  根据[卫报](../Page/卫报.md "wikilink")2013年Good University
    Guide排名，卡斯商学院的本科商科专业全英國排名第二位。
4.  根據英國 Research Excellence Framework 2014排名, 卡斯在商業與管理研究類排名全英國第六位。

## 卡斯 MBA排名

卡斯提供为期一年的全职MBA学习项目和为期两年的兼职EMBA学习项目。

1.  根據[金融時報2017年排名,](http://rankings.ft.com/businessschoolrankings/city-university-cass/top-mbas-for-entrepreneurship-2017#top-mbas-for-entrepreneurship-2017)其MBA在創業(Top
    MBAs for entrepreneurship)項目中排名全英國第2**,世界第7**.
2.  根据[金融时报](../Page/金融时报.md "wikilink")[2017年排名](http://rankings.ft.com/businessschoolrankings/city-university-cass/executive-mba-ranking-2016#executive-mba-ranking-2016),卡斯[EMBA项目全球排名第](../Page/EMBA.md "wikilink")31位,全英國第3位。
3.  根劇[金融時報2017年排名](http://rankings.ft.com/businessschoolrankings/city-university-cass),卡斯MBA项目全球排名第37位,全英國第5位。高於英國[Warwick](../Page/華威商學院.md "wikilink"),[ICL](../Page/伦敦帝国学院.md "wikilink"),澳洲[UNSW](../Page/新南威尔士大学.md "wikilink"),多倫多[Rotman等世界知名商學院](../Page/多倫多大學.md "wikilink")。
4.  根據[金融時報2017年排名](http://rankings.ft.com/businessschoolrankings/top-mbas-for-finance-2017),卡斯財金MBA(Top
    MBAs for Finance)排名**全球第25位**,全英第四位。排名高於許多世界名校如: HEC Paris,
    耶魯大學,杜克大學,柏克萊大學,南加大,德州大學奧斯丁分校,多倫多大學,墨爾本大學。充分支持業界稱金融專業為其強項的說法。
5.  2012年經濟學人 The Economist's Which MBA? 排名, Cass 全職 MBA 位全球第30名。
6.  2014年經濟學人 The Economist's Which MBA? 排名, Cass 全職 MBA
    位全英國第8位,全歐洲第18位。
7.  2016年經濟學人 [The Economist's Which
    MBA?](http://www.economist.com/whichmba/full-time-mba-ranking?year=2016&term_node_tid_depth=77644)
    排名, Cass 全職 MBA 位全英國第5位。

## 研究生项目

卡斯商学院可授予20多种研究生专业，以利于学生侧重于不同的职业发展方向。

卡斯的研究生项目第一无二的设计充分考虑了学科的广度及知识的深度，它涵盖了非常广泛的专业领域，从[零售银行](../Page/零售银行.md "wikilink")、[中央银行](../Page/中央银行.md "wikilink")、[投资银行的](../Page/投资银行.md "wikilink")[资产管理到](../Page/资产管理.md "wikilink")[数量金融](../Page/数量金融.md "wikilink")，从[资产定价到](../Page/资产定价.md "wikilink")[金融服务业及](../Page/金融服务业.md "wikilink")[慈善业的](../Page/慈善业.md "wikilink")[投资管理](../Page/投资管理.md "wikilink")。所有课程共有的一个显著特点是卡斯将金融、管理以及专业学科的广泛理论与更广泛的从业实践相结合，通过案例分析、实地考察或聘请相关领域专家的方式来推行卡斯的理念。

## 研究中心

  - 精算研究中心（The Actuarial Research Centre）
  - 非传统投资研究中心（Alternative Investments Research Centre，AIRC）
  - 亚洲商业研究中心（Asian Business Research Centre）
  - 银行学研究中心（Centre for Banking Studies）
  - 慈善业效力研究中心（Centre for Charity Effectiveness）
  - 计量分析研究中心（Centre for Econometric Analysis，CEA@Cass）
  - 金融法规和金融犯罪研究中心（Centre for Financial Regulation and Crime，CFRC）
  - 领导力、学习和转变研究中心（Centre for Leadership, Learning and Change，CLLC）
  - 新技术、创新和创业研究中心（Centre for New Technologies, Innovation and
    Entrepreneurship，CENTIVE）
  - 公司管理研究中心（Centre for Research in Corporate Governance）
  - 欧洲金融市场和金融机构研究中心（Centre for Research on European Financial Markets
    and Institutions，CREFMI）
  - 新兴市场研究小组（Emerging Markets Group, EMG）
  - 电影商业研究中心（Film Business Research Centre）
  - 国际海运、贸易和金融研究中心（International Centre for Shipping, Trade and Finance）
  - 养老金研究协会（Pensions Institute）
  - 私募股权和风险资本研究中心（Private Equity and Venture Capital Research
    Centre，PERC）
  - 房地产金融研究中心（Research Centre for Real Estate Finance）
  - 风险研究协会（Risk Institute）

## 著名校友

  - Stelios Haji-Ioannou爵士
    -[easyGroup创始人](../Page/easyGroup.md "wikilink")
  - Muhtar Kent -
    [可口可乐公司CEO](../Page/可口可乐.md "wikilink")。此前为可口可乐公司高级执行副主席，可口可乐国际主席和首席运营官COO
  - Peter Cullum - 欧洲最大独立保险中介[Towergate
    Partnership](http://www.towergate.co.uk)创始人
  - David Kelly -
    旅游网站[lastminute.com创始人和首席运营官COO](../Page/lastminute.com.md "wikilink")
  - [傅帆](http://www.sailing-capital.com/index.php?m=content&c=index&a=lists&catid=21)
    -
    [赛领国际投资基金董事长](../Page/赛领国际投资基金.md "wikilink")，[赛领资本管理有限公司董事长](../Page/赛领资本管理有限公司.md "wikilink")，[上海国际集团有限公司](../Page/上海国际集团有限公司.md "wikilink")
    董事、总裁，[上海股权托管交易中心](../Page/上海股权托管交易中心.md "wikilink")
    董事长，[上海科创中心股权投资基金管理有限公司](../Page/上海科创中心股权投资基金管理有限公司.md "wikilink")
    董事长
  - David Essex - [Bemrose集团主席](../Page/Bemrose集团.md "wikilink")
  - [刘明康](../Page/刘明康.md "wikilink") -
    [中国银监会前主席](../Page/中国银监会.md "wikilink")
  - Dick Olver - [BAE系统](../Page/BAE系统.md "wikilink")（BAE
    Systems）主席，[路透社董事局董事](../Page/路透社.md "wikilink")
  - Andrew Pople - Kessler Financial Services International
    (KFSI)主席，以及The Kessler Group副主席
  - Syed Ali Raza - [巴基斯坦国家银行总裁及主席](../Page/巴基斯坦国家银行.md "wikilink")
  - Kiran Rao - [空中巴士](../Page/空中巴士.md "wikilink")（Airbus）营销及定价政策高级副总裁
  - Kelvin Milgate - [荷兰银行](../Page/荷兰银行.md "wikilink")（ABN Amro
    Bank）商品衍生物（Commodity
    Derivatives）交易员。在全英30岁以下30名顶级交易员的排名中名列第17（来源:
    Trader Monthly）
  - Carol Sergeant -
    [勞埃德TSB銀行集團首席风险董事](../Page/:en:Lloyds_TSB_Group_Plc.md "wikilink")
  - Mark Turrell -
    [Imaginatik协同创始人](../Page/Imaginatik.md "wikilink")、CEO
  - Gareth Wong - [GamBond和](../Page/GamBond.md "wikilink")[Gaming Money
    Summit创始人](../Page/Gaming_Money_Summit.md "wikilink")，[Network
    Europe首席体验官CXO](../Page/Network_Europe.md "wikilink")
  - Durmuş Yılmaz - [土耳其中央银行行长](../Page/土耳其中央银行.md "wikilink")
  - David Woodward - [Aabar Petroleum
    Investments公司CEO](../Page/Aabar_Petroleum_Investments.md "wikilink")，此前为[BP石油总裁](../Page/英国石油公司.md "wikilink")
  - Phillip Monks - [Europe Arab Bank
    plc](../Page/Europe_Arab_Bank_plc.md "wikilink")，CEO

## 备注

<div class="references">

<references/>

## 外部链接

  - [卡斯商学院 Cass Business School](http://www.cass.city.ac.uk/)
  - [倫敦大學城市學院 City, University of London](http://www.city.ac.uk/)
  - [卡斯MBA网志 Cass MBA blog](https://blogs.city.ac.uk/cassmba/)

[Category:伦敦大学城市學院](../Category/伦敦大学城市學院.md "wikilink")
[Category:英国商学院](../Category/英国商学院.md "wikilink")
[Category:1966年創建的教育機構](../Category/1966年創建的教育機構.md "wikilink")

1.