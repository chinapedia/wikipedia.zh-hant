**雅克·罗格**伯爵（，），前任[國際奧委會主席](../Page/國際奧委會.md "wikilink")，亦為[比利时体育官员](../Page/比利时.md "wikilink")。

## 经历

罗格出生在[根特](../Page/根特.md "wikilink")，居住在比利時北部的[德因澤](../Page/德因澤.md "wikilink")。他参加了[1968年](../Page/1968年夏季奥林匹克运动会.md "wikilink")、[1972年和](../Page/1972年夏季奥林匹克运动会.md "wikilink")[1976年夏季奥林匹克运动会](../Page/1976年夏季奥林匹克运动会.md "wikilink")，并且加入了比利时[橄榄球队](../Page/橄榄球.md "wikilink")。罗格从1989年到1991年担任[比利时奥林匹克委员会主席](../Page/比利时奥林匹克委员会.md "wikilink")，1989年到2001年担任[欧洲奥林匹克委员会主席](../Page/欧洲奥林匹克委员会.md "wikilink")。1991年他成为国际奥委会成员，并于1998年加入[执行委员会](../Page/国际奥林匹克委员会执行委员会.md "wikilink")。

2001年7月16日，罗格在[莫斯科的](../Page/莫斯科.md "wikilink")[IOC会议上被选为](../Page/国际奥林匹克委员会第112次全体会议.md "wikilink")[国际奥林匹克委员会主席](../Page/国际奥林匹克委员会.md "wikilink")，替代之前领导IOC達21年之久的[萨马兰奇](../Page/萨马兰奇.md "wikilink")。

罗格的IOC政策中，核心的一点是将夏季奥运会的参赛人数限制在10,000以下。他说，他将严格禁止[腐败和使用](../Page/腐败.md "wikilink")[興奮劑的行为](../Page/興奮劑.md "wikilink")。在[2002年冬季奥林匹克运动会上](../Page/2002年冬季奥林匹克运动会.md "wikilink")，罗格成为第一个居住在[奥运村的国际奥委会主席](../Page/奥运村.md "wikilink")，并且亲自致力于比赛监督工作。

在他的任期中，[棒球和](../Page/棒球.md "wikilink")[垒球在](../Page/垒球.md "wikilink")2005年7月的[新加坡会议上被表决從奥运会比赛项目中取消](../Page/新加坡.md "wikilink")，于[2012年伦敦奥运会生效](../Page/2012年夏季奥林匹克运动会.md "wikilink")。

罗格同時獲[比利时國王](../Page/比利时國王.md "wikilink")[阿爾贝二世封為](../Page/阿尔贝二世_\(比利时\).md "wikilink")[伯爵](../Page/伯爵.md "wikilink")。

{{-}}

[Category:国际奥林匹克委员会主席](../Category/国际奥林匹克委员会主席.md "wikilink")
[Category:國際奧林匹克委員會委員](../Category/國際奧林匹克委員會委員.md "wikilink")
[Category:歐洲奧林匹克委員會主席](../Category/歐洲奧林匹克委員會主席.md "wikilink")
[Category:比利時貴族](../Category/比利時貴族.md "wikilink")
[Category:橄欖球運動員](../Category/橄欖球運動員.md "wikilink")
[Category:比利時運動員](../Category/比利時運動員.md "wikilink")
[Category:比利時天主教徒](../Category/比利時天主教徒.md "wikilink")
[Category:根特大學校友](../Category/根特大學校友.md "wikilink")
[Category:根特人](../Category/根特人.md "wikilink")
[Category:外科醫生](../Category/外科醫生.md "wikilink")