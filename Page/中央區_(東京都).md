**中央区**（）是[日本](../Page/日本.md "wikilink")[東京都內的](../Page/東京都.md "wikilink")23個[特別區之一](../Page/東京23區.md "wikilink")，由於其位於23區的正中央位置之故，因而得名。

中央區不仅地理位置处于東京的中心地，更是日本经济、信息、商业等的中心。著名的[日本銀行](../Page/日本銀行.md "wikilink")、[东京证券交易所等日本經濟核心都位於中央区](../Page/东京证券交易所.md "wikilink")。除此之外，包括[銀座](../Page/銀座.md "wikilink")、大型布莊與[百貨公司總社聚集的](../Page/百貨公司.md "wikilink")[日本橋](../Page/日本橋.md "wikilink")、以及以魚貨市場闻名的[築地皆位於本區範圍之內](../Page/築地.md "wikilink")。

## 概要

大約位於[東京23區的](../Page/東京23區.md "wikilink")[中央](../Page/中央.md "wikilink")。區内有[日本橋](../Page/日本橋_\(東京都中央區\).md "wikilink")、[八重洲](../Page/八重洲.md "wikilink")・[築地](../Page/築地.md "wikilink")、[月島](../Page/月島.md "wikilink")、[銀座等街區](../Page/銀座.md "wikilink")。

面積為10.094km²，是東京23區中第二小的特別區，僅次於[台東區](../Page/台東區.md "wikilink")。

人口130,483人，次於[千代田區](../Page/千代田區.md "wikilink")（2013年6月1日）。（2013年4月定居人口達13萬人，是1967年以來的新高）區內有日本橋與銀座等大型商業區域，讓日間人口達60.6萬人。此外，本區商業強烈，高樓林立，住宅方面以高層公寓與團地等[集合住宅為主](../Page/集合住宅.md "wikilink")，獨戶低層住宅或低層分租公寓較為少見（多在南部的[佃和](../Page/佃_\(東京都中央區\).md "wikilink")[月島附近](../Page/月島.md "wikilink")）。

道路呈現棋盤狀，與他區相比較為整齊。

## 人口

### 日夜間人口差

2005年夜間人口（居住者）為98,220人，區外通勤者與通學生及區內活動居住者合計的日間人口達647,733人，日夜差距6.595倍。

## 地理

區域西側在江戶時代是日本橋與京橋等繁榮的下町地區，東側是同時代填海造陸形成的土地。現在，中央區在行政上可分為[日本橋](../Page/日本橋_\(東京都中央區\).md "wikilink")、[京橋](../Page/京橋.md "wikilink")（銀座、築地等）與[月島三個地區](../Page/月島.md "wikilink")。

中央區在[第二次世界大戰結束以前是運河與水運發達的地區](../Page/第二次世界大戰.md "wikilink")。區内有[京橋川](../Page/京橋川_\(東京都\).md "wikilink")、[櫻川](../Page/櫻川_\(東京都\).md "wikilink")、[築地川](../Page/築地川.md "wikilink")、[汐留川](../Page/汐留川.md "wikilink")、[三十間堀川](../Page/三十間堀川.md "wikilink")、[鐵砲洲川](../Page/鐵砲洲川.md "wikilink")、[箱崎川](../Page/箱崎川.md "wikilink")、濱町川、龍閑川等河船或運河。這些河川在戰後陸續填埋。雖然已看不到過去的景色，但許多町界與區界都是沿河川所劃定。

區北端與區界為龍閑川遺跡，西端是外堀遺與[日本橋川](../Page/日本橋川.md "wikilink")，南端有[汐留川遺](../Page/汐留川.md "wikilink")。區的東端是[隅田川](../Page/隅田川.md "wikilink")。隅田川下游分為兩條水路（西側為本流，東側為「派流晴海運河」），河中的中州是[佃](../Page/佃_\(東京都中央區\).md "wikilink")、月島，勝鬨，[豐海町](../Page/豐海町.md "wikilink")，[晴海](../Page/晴海.md "wikilink")。區東南部面[東京灣](../Page/東京灣.md "wikilink")。

  - 河川

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/隅田川.md" title="wikilink">隅田川</a></li>
<li><a href="../Page/神田川_(東京都).md" title="wikilink">神田川</a></li>
<li><a href="../Page/日本橋川.md" title="wikilink">日本橋川</a></li>
<li><a href="../Page/龜島川.md" title="wikilink">龜島川</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/築地川.md" title="wikilink">築地川</a></li>
<li><a href="../Page/汐留川.md" title="wikilink">汐留川</a></li>
<li><a href="../Page/月島川.md" title="wikilink">月島川</a></li>
<li><a href="../Page/新月島川.md" title="wikilink">新月島川</a></li>
<li><a href="../Page/佃川支川.md" title="wikilink">佃川支川</a></li>
</ul></td>
</tr>
</tbody>
</table>

  - 運河

<!-- end list -->

  - [晴海運河](../Page/晴海運河.md "wikilink")、[朝潮運河](../Page/朝潮運河.md "wikilink")

<!-- end list -->

  - 隣接的自治體

<!-- end list -->

  - [千代田區](../Page/千代田區.md "wikilink")、[港區](../Page/港區_\(東京都\).md "wikilink")、[台東區](../Page/台東區.md "wikilink")、[墨田區](../Page/墨田區.md "wikilink")、[江東區](../Page/江東區_\(東京都\).md "wikilink")

## 歷史

  - 近世以前

<!-- end list -->

  - 大約是[江戶鄉内前嶋的位置](../Page/江戶.md "wikilink")。

<!-- end list -->

  - 近世

<!-- end list -->

  - 相當於江戶町。因[寬政改革的雇用政策](../Page/寬政改革.md "wikilink")，石川島（現在的大川端）設置人足寄場。
  - 1603年4月14日 - 初代日本橋完工。

<!-- end list -->

  - 近・現代

<!-- end list -->

  - 1869年 - 築地鐵砲洲（今日明石町附近）設置外國人居留地（～1899年）。
  - 1953年 - 戰後復興，定居人口創下172,183人的記錄。
  - 1967年 - 高度經濟成長，人口脫離都心，定居人口跌破13萬人。
  - 1997年4月 - 定居人口創下71,806人的記錄。
  - 2007年4月4日 - 定居人口達10萬人，31年來首度。
  - 2011年 - 日本橋（第十九代）架橋100周年。
  - 2013年4月4日 - 定住人口破13萬人，46年來首度。

<!-- end list -->

  - 沿革

<!-- end list -->

  - 1878年11月2日施行[郡區町村編制法](../Page/郡區町村編制法.md "wikilink")，日本橋區與京橋區誕生。相當於現在的中央區。
  - 1947年3月15日 - 兩區合併為現在的中央區。舊日本橋區域的町名冠上「日本橋」。
  - 1947年5月3日 -
    [地方自治法施行](../Page/地方自治法.md "wikilink")，中央區成為[特別區](../Page/特別區.md "wikilink")。
  - 1997年3月15日 - 中央區制施行50年。

### 地名的由來

  - 因大約在[東京市](../Page/東京市.md "wikilink")（現[東京都區部](../Page/東京都區部.md "wikilink")）中央位置而得名。

## 區政

  - 區長

<!-- end list -->

  - [矢田美英](../Page/矢田美英.md "wikilink")（7期目）
  - 任期：2015年4月26日

<!-- end list -->

  - 區議會

<!-- end list -->

  - 席次：30人
  - 任期：2015年4月30日

## 友好都市、姊妹市

<table>
<thead>
<tr class="header">
<th><p>國家</p></th>
<th><p>一級行政區</p></th>
<th><p>城市</p></th>
<th><p>締結日期</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p><a href="../Page/山形縣.md" title="wikilink">山形縣</a></p></td>
<td><p><a href="../Page/東根市.md" title="wikilink">東根市</a></p></td>
<td><p>1991年</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/薩瑟蘭市.md" title="wikilink">薩瑟蘭市</a></p></td>
<td><p>1991年</p></td>
</tr>
</tbody>
</table>

## 地域

<table>
<tbody>
<tr class="odd">
<td><dl>
<dt></dt>
<dd><a href="../Page/明石町_(東京都中央區).md" title="wikilink">明石町</a>
</dd>
<dd><a href="../Page/入船_(東京都中央區).md" title="wikilink">入船</a>
</dd>
<dd><a href="../Page/勝鬨.md" title="wikilink">勝鬨</a>
</dd>
<dd><a href="../Page/京橋_(東京都中央區).md" title="wikilink">京橋</a>
</dd>
<dd><a href="../Page/銀座.md" title="wikilink">銀座</a>
</dd>
<dd><a href="../Page/新川_(東京都中央區).md" title="wikilink">新川</a>
</dd>
<dd><a href="../Page/新富_(東京都中央區).md" title="wikilink">新富</a>
</dd>
<dd><a href="../Page/月島.md" title="wikilink">月島</a>
</dd>
</dl></td>
<td><dl>
<dt></dt>
<dd><a href="../Page/築地.md" title="wikilink">築地</a>
</dd>
<dd><a href="../Page/佃_(東京都中央區).md" title="wikilink">佃</a>
</dd>
<dd><a href="../Page/豐海町_(東京都中央區).md" title="wikilink">豐海町</a>
</dd>
<dd><a href="../Page/日本橋_(東京都中央區).md" title="wikilink">日本橋</a>
</dd>
<dd><a href="../Page/日本橋大傳馬町.md" title="wikilink">日本橋大傳馬町</a>
</dd>
<dd><a href="../Page/日本橋蠣殼町.md" title="wikilink">日本橋蠣殼町</a>
</dd>
<dd><a href="../Page/日本橋兜町.md" title="wikilink">日本橋兜町</a>
</dd>
<dd><a href="../Page/日本橋茅場町.md" title="wikilink">日本橋茅場町</a>
</dd>
</dl></td>
<td><dl>
<dt></dt>
<dd><a href="../Page/日本橋小網町.md" title="wikilink">日本橋小網町</a>
</dd>
<dd><a href="../Page/日本橋小傳馬町.md" title="wikilink">日本橋小傳馬町</a>
</dd>
<dd><a href="../Page/日本橋小舟町.md" title="wikilink">日本橋小舟町</a>
</dd>
<dd><a href="../Page/日本橋富澤町.md" title="wikilink">日本橋富澤町</a>
</dd>
<dd><a href="../Page/日本橋中洲.md" title="wikilink">日本橋中洲</a>
</dd>
<dd><a href="../Page/日本橋人形町.md" title="wikilink">日本橋人形町</a>
</dd>
<dd><a href="../Page/日本橋箱崎町.md" title="wikilink">日本橋箱崎町</a>
</dd>
<dd><a href="../Page/日本橋濱町.md" title="wikilink">日本橋濱町</a>
</dd>
</dl></td>
<td><dl>
<dt></dt>
<dd><a href="../Page/日本橋馬喰町.md" title="wikilink">日本橋馬喰町</a>
</dd>
<dd><a href="../Page/日本橋久松町.md" title="wikilink">日本橋久松町</a>
</dd>
<dd><a href="../Page/日本橋堀留町.md" title="wikilink">日本橋堀留町</a>
</dd>
<dd><a href="../Page/日本橋本石町.md" title="wikilink">日本橋本石町</a>
</dd>
<dd><a href="../Page/日本橋本町.md" title="wikilink">日本橋本町</a>
</dd>
<dd><a href="../Page/日本橋室町.md" title="wikilink">日本橋室町</a>
</dd>
<dd><a href="../Page/日本橋橫山町.md" title="wikilink">日本橋橫山町</a>
</dd>
<dd><a href="../Page/八丁堀_(東京都中央區).md" title="wikilink">八丁堀</a>
</dd>
</dl></td>
<td><dl>
<dt></dt>
<dd><a href="../Page/濱離宮庭園.md" title="wikilink">濱離宮庭園</a>
</dd>
<dd><a href="../Page/晴海_(東京都中央區).md" title="wikilink">晴海</a>
</dd>
<dd><a href="../Page/東日本橋.md" title="wikilink">東日本橋</a>
</dd>
<dd><a href="../Page/湊_(東京都中央區).md" title="wikilink">湊</a>
</dd>
<dd><a href="../Page/八重洲.md" title="wikilink">八重洲</a>
</dd>
</dl></td>
</tr>
</tbody>
</table>

### 防災

[東京消防廳](../Page/東京消防廳.md "wikilink")

  - 京橋消防署（京橋3-14-1）救急隊1
      - 築地出張所（明石町1-27）特別消火中隊、救急隊無
      - 銀座出張所（銀座7-11-17）救急隊1
  - 日本橋消防署（日本橋兜町14-12）特別消火中隊、消防活動二輪部隊・救急隊1
      - 堀留出張所（日本橋堀留町1-2-6）救急隊無
      - 人形町出張所（日本橋人形町2-27-8）救急隊無
      - 濱町出張所（日本橋濱町3-45-12）水難救助隊、救急隊無
  - 臨港消防署（勝鬨5-1-23）水難救助隊、救急隊無
      - 月島出張所（勝鬨4-5-14）特別消火中隊、救急隊1

## 交通

本區中央部是[日本道路網的起點日本橋](../Page/日本道路網.md "wikilink")，日本最早的地下鐵是沿[中央通興建](../Page/中央通.md "wikilink")。

### 鐵路

  - [東日本旅客鐵道](../Page/東日本旅客鐵道.md "wikilink")（JR東日本）

<!-- end list -->

  - [JR_JO_line_symbol.svg](https://zh.wikipedia.org/wiki/File:JR_JO_line_symbol.svg "fig:JR_JO_line_symbol.svg")
    [橫須賀·總武快速線](../Page/橫須賀·總武快速線.md "wikilink")：[新日本橋站](../Page/新日本橋站.md "wikilink")
    - [馬喰町站](../Page/馬喰町站.md "wikilink")
  - [JR_JE_line_symbol.svg](https://zh.wikipedia.org/wiki/File:JR_JE_line_symbol.svg "fig:JR_JE_line_symbol.svg")
    [京葉線](../Page/京葉線.md "wikilink")：[八丁堀站](../Page/八丁堀站_\(東京都\).md "wikilink")
      - [京濱東北線](../Page/京濱東北線.md "wikilink")、[山手線](../Page/山手線.md "wikilink")、[中央本線](../Page/中央本線.md "wikilink")（[中央線](../Page/中央線快速.md "wikilink")）的[神田站](../Page/神田站_\(東京都\).md "wikilink")－[東京站間](../Page/東京站.md "wikilink")，以及[東北](../Page/東北新幹線.md "wikilink")、[上越新幹線](../Page/上越新幹線.md "wikilink")、[東北本線](../Page/東北本線.md "wikilink")（[上野東京線](../Page/上野東京線.md "wikilink")）的[上野站](../Page/上野站.md "wikilink")－東京站間都掠過此區，未有設站。

<!-- end list -->

  - [東京地下鐵](../Page/東京地下鐵.md "wikilink")

<!-- end list -->

  - [Subway_TokyoGinza.png](https://zh.wikipedia.org/wiki/File:Subway_TokyoGinza.png "fig:Subway_TokyoGinza.png")
    [銀座線](../Page/銀座線.md "wikilink")：[銀座站](../Page/銀座站_\(日本\).md "wikilink")
    - [京橋站](../Page/京橋站_\(東京都\).md "wikilink") -
    [日本橋站](../Page/日本橋站_\(東京都\).md "wikilink") -
    [三越前站](../Page/三越前站.md "wikilink")
  - [Subway_TokyoMarunouchi.png](https://zh.wikipedia.org/wiki/File:Subway_TokyoMarunouchi.png "fig:Subway_TokyoMarunouchi.png")
    [丸之內線](../Page/丸之內線.md "wikilink")：[銀座站](../Page/銀座站_\(日本\).md "wikilink")
  - [Subway_TokyoHibiya.png](https://zh.wikipedia.org/wiki/File:Subway_TokyoHibiya.png "fig:Subway_TokyoHibiya.png")
    [日比谷線](../Page/日比谷線.md "wikilink")：[銀座站](../Page/銀座站_\(日本\).md "wikilink")
    - [東銀座站](../Page/東銀座站.md "wikilink") -
    [築地站](../Page/築地站.md "wikilink") -
    [八丁堀站](../Page/八丁堀站_\(東京都\).md "wikilink") -
    [茅場町站](../Page/茅場町站.md "wikilink") -
    [人形町站](../Page/人形町站.md "wikilink") -
    [小傳馬町站](../Page/小傳馬町站.md "wikilink")
  - [Subway_TokyoTozai.png](https://zh.wikipedia.org/wiki/File:Subway_TokyoTozai.png "fig:Subway_TokyoTozai.png")
    [東西線](../Page/東西線_\(東京地下鐵\).md "wikilink")：[日本橋](../Page/日本橋站_\(東京都\).md "wikilink")
    - [茅場町站](../Page/茅場町站.md "wikilink")
  - [Subway_TokyoYurakucho.png](https://zh.wikipedia.org/wiki/File:Subway_TokyoYurakucho.png "fig:Subway_TokyoYurakucho.png")
    [有樂町線](../Page/有樂町線.md "wikilink")：[銀座一丁目站](../Page/銀座一丁目站.md "wikilink")
    - [新富町站](../Page/新富町站_\(東京都\).md "wikilink") -
    [月島站](../Page/月島站.md "wikilink")
  - [Subway_TokyoHanzomon.png](https://zh.wikipedia.org/wiki/File:Subway_TokyoHanzomon.png "fig:Subway_TokyoHanzomon.png")
    [半藏門線](../Page/半藏門線.md "wikilink")：[三越前站](../Page/三越前站.md "wikilink")
    - [水天宮前站](../Page/水天宮前站.md "wikilink")

<!-- end list -->

  - [東京都交通局](../Page/東京都交通局.md "wikilink")（[都營地下鐵](../Page/都營地下鐵.md "wikilink")）

<!-- end list -->

  - [Subway_TokyoAsakusa.png](https://zh.wikipedia.org/wiki/File:Subway_TokyoAsakusa.png "fig:Subway_TokyoAsakusa.png")
    [淺草線](../Page/淺草線.md "wikilink")：[東銀座站](../Page/東銀座站.md "wikilink")
    - [寶町站](../Page/寶町站_\(東京都\).md "wikilink") -
    [日本橋站](../Page/日本橋站_\(東京都\).md "wikilink") -
    [人形町站](../Page/人形町站.md "wikilink") -
    [東日本橋站](../Page/東日本橋站.md "wikilink")
  - [Subway_TokyoShinjuku.png](https://zh.wikipedia.org/wiki/File:Subway_TokyoShinjuku.png "fig:Subway_TokyoShinjuku.png")
    [新宿線](../Page/新宿線_\(都營地下鐵\).md "wikilink")：[馬喰橫山站](../Page/馬喰橫山站.md "wikilink")
    - [濱町站](../Page/濱町站_\(東京都\).md "wikilink")
  - [Subway_TokyoOedo.png](https://zh.wikipedia.org/wiki/File:Subway_TokyoOedo.png "fig:Subway_TokyoOedo.png")
    [大江戶線](../Page/大江戶線.md "wikilink")：[月島站](../Page/月島站.md "wikilink")
    - [勝鬨站](../Page/勝鬨站.md "wikilink") -
    [築地市場站](../Page/築地市場站.md "wikilink")

### 水上巴士

  - [東京都觀光汽船](../Page/東京都觀光汽船.md "wikilink")
      - [隅田川線](../Page/隅田川線.md "wikilink")
          - 濱離宮碼頭
      - [台場線](../Page/台場線.md "wikilink")
          - 晴海碼頭

<!-- end list -->

  - [東京都公園協會](../Page/東京都公園協會.md "wikilink")
      - [東京水邊線](../Page/東京水邊線.md "wikilink")
          - [濱町碼頭](../Page/濱町碼頭.md "wikilink")、[明石町、聖路加花園前碼頭](../Page/明石町、聖路加花園前碼頭.md "wikilink")、[濱離宮碼頭](../Page/濱離宮碼頭.md "wikilink")（濱町碼頭與明石町、聖路加花園前碼頭之間的[越中島碼頭在江東區](../Page/越中島碼頭.md "wikilink")）

### 巴士

[Hitachi-Jidosha-Kotsu_1038_Edo-Bus.jpg](https://zh.wikipedia.org/wiki/File:Hitachi-Jidosha-Kotsu_1038_Edo-Bus.jpg "fig:Hitachi-Jidosha-Kotsu_1038_Edo-Bus.jpg")

  - [東京都交通局](../Page/東京都交通局.md "wikilink")
      - [都營巴士](../Page/都營巴士.md "wikilink")
  - [日之丸自動車興業](../Page/日之丸自動車興業.md "wikilink")
      - [MetroLink日本橋](../Page/MetroLink日本橋.md "wikilink")
  - [日立自動車交通](../Page/日立自動車交通.md "wikilink")
      - [中央區社區巴士](../Page/中央區社區巴士.md "wikilink")（江戶巴士）

### 道路

  - [首都高速道路](../Page/首都高速道路.md "wikilink")
      - [1號上野線](../Page/首都高速1號上野線.md "wikilink")
      - [6號向島線](../Page/首都高速6號向島線.md "wikilink")
      - [9號深川線](../Page/首都高速9號深川線.md "wikilink")
      - [C1都心環狀線](../Page/首都高速都心環狀線.md "wikilink")
  - [東京高速道路](../Page/東京高速道路.md "wikilink")
  - [國道1號](../Page/國道1號.md "wikilink")
  - [國道4號](../Page/國道4號.md "wikilink")
  - [國道6號](../Page/國道6號.md "wikilink")（江戶通）
  - [國道14號](../Page/國道14號.md "wikilink")（京葉道路）
  - [國道15號](../Page/國道15號.md "wikilink")（中央通）
  - [國道17號](../Page/國道17號.md "wikilink")（中央通）
  - [國道20號](../Page/國道20號.md "wikilink")
  - [晴海通](../Page/晴海通.md "wikilink")
  - [外堀通](../Page/東京都道405號外濠環狀線.md "wikilink")
  - [昭和通](../Page/昭和通_\(東京都\).md "wikilink")

[Harumi_triton_square.jpg](https://zh.wikipedia.org/wiki/File:Harumi_triton_square.jpg "fig:Harumi_triton_square.jpg")
[Tsukishima_monja_street.jpg](https://zh.wikipedia.org/wiki/File:Tsukishima_monja_street.jpg "fig:Tsukishima_monja_street.jpg")
[Ookawabata_river_city.jpg](https://zh.wikipedia.org/wiki/File:Ookawabata_river_city.jpg "fig:Ookawabata_river_city.jpg")

## 觀光

  - 日本橋地域

<!-- end list -->

  - [日本橋商店街](../Page/日本橋_\(東京都中央區\).md "wikilink")、薬品街、繊維街、批發街

  - [兜町證券街](../Page/日本橋兜町.md "wikilink")（[東京證券交易所等](../Page/東京證券交易所.md "wikilink")）

  - [日本銀行本店](../Page/日本銀行.md "wikilink")

  - [三井本館](../Page/三井本館.md "wikilink")

  - [三越本店](../Page/三越.md "wikilink")

  - [明治座](../Page/明治座.md "wikilink")

  - （T-CAT）

  - [山口縣東京觀光物產中心](../Page/山口縣東京觀光物產中心.md "wikilink")（不是山口館）

  - [MetroLink日本橋](../Page/MetroLink日本橋.md "wikilink")（免費巡迴巴士）

<!-- end list -->

  - 京橋、月島地域

<!-- end list -->

  - [銀座](../Page/銀座.md "wikilink")
  - [永代橋](../Page/永代橋.md "wikilink")
  - [普利司通美術館](../Page/普利司通美術館.md "wikilink")
  - [歌舞伎座](../Page/歌舞伎座.md "wikilink")
  - [新橋演舞場](../Page/新橋演舞場.md "wikilink")
  - [和光](../Page/和光_\(商業設施\).md "wikilink")（舊服部時計店、通稱「銀座的時計台」）
  - [東京都中央卸賣市場 築地市場](../Page/築地市場.md "wikilink")
  - [築地本願寺](../Page/築地本願寺.md "wikilink")
  - [國立癌研究中心](../Page/國立癌研究中心.md "wikilink")
  - [聖路加國際醫院](../Page/聖路加國際醫院.md "wikilink")
  - [濱離宮庭園](../Page/濱離宮恩賜庭園.md "wikilink")
  - [勝鬨橋](../Page/勝鬨橋.md "wikilink")
  - [月島](../Page/月島.md "wikilink")
  - [晴海埠頭](../Page/東京港.md "wikilink")

## 神社

  - [水天宮](../Page/水天宮_\(東京都中央區\).md "wikilink")
  - [住吉神社](../Page/住吉神社_\(東京都中央區\).md "wikilink")
  - [波除稻荷神社](../Page/波除稻荷神社.md "wikilink")
  - [鐵砲洲稻荷神社](../Page/鐵砲洲稻荷神社.md "wikilink")
  - [椙森神社](../Page/椙森神社.md "wikilink")
  - [小網神社](../Page/小網神社.md "wikilink")
  - [松島神社](../Page/松島神社.md "wikilink")
  - [末廣神社](../Page/末廣神社.md "wikilink")
  - [笠間稻荷神社](../Page/笠間稻荷神社.md "wikilink")（分社）

等

## 教育機關

### 公立學校

  - [中學校](../Page/中學校.md "wikilink")（全4校）

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/中央區立銀座中學校.md" title="wikilink">銀座中學校</a></li>
<li><a href="../Page/中央區立佃中學校.md" title="wikilink">佃中學校</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/中央區立晴海中學校.md" title="wikilink">晴海中學校</a></li>
<li><a href="../Page/中央區立日本橋中學校.md" title="wikilink">日本橋中學校</a></li>
</ul></td>
</tr>
</tbody>
</table>

  - [小學校](../Page/小學校.md "wikilink")（全16校）

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/中央區立城東小學校.md" title="wikilink">城東小學校</a></li>
<li><a href="../Page/中央區立泰明小學校.md" title="wikilink">泰明小學校</a></li>
<li><a href="../Page/中央區立中央小學校.md" title="wikilink">中央小學校</a></li>
<li><a href="../Page/中央區立京橋築地小學校.md" title="wikilink">京橋築地小學校</a></li>
<li><a href="../Page/中央區立明石小學校.md" title="wikilink">明石小學校</a></li>
<li><a href="../Page/中央區立明正小學校.md" title="wikilink">明正小學校</a></li>
<li><a href="../Page/中央區立常盤小學校.md" title="wikilink">常盤小學校</a></li>
<li><a href="../Page/中央區立日本橋小學校.md" title="wikilink">日本橋小學校</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/中央區立久松小學校.md" title="wikilink">久松小學校</a></li>
<li><a href="../Page/中央區立阪本小學校.md" title="wikilink">阪本小學校</a></li>
<li><a href="../Page/中央區立有馬小學校.md" title="wikilink">有馬小學校</a></li>
<li><a href="../Page/中央區立佃島小學校.md" title="wikilink">佃島小學校</a></li>
<li><a href="../Page/中央區立月島第一小學校.md" title="wikilink">月島第一小學校</a></li>
<li><a href="../Page/中央區立月島第二小學校.md" title="wikilink">月島第二小學校</a></li>
<li><a href="../Page/中央區立月島第三小學校.md" title="wikilink">月島第三小學校</a></li>
<li><a href="../Page/中央區立豐海小學校.md" title="wikilink">豐海小學校</a></li>
</ul></td>
</tr>
</tbody>
</table>

  - [幼稚園](../Page/幼稚園.md "wikilink")（全14校）

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/泰明幼稚園.md" title="wikilink">泰明幼稚園</a></li>
<li><a href="../Page/中央幼稚園.md" title="wikilink">中央幼稚園</a></li>
<li><a href="../Page/明石幼稚園.md" title="wikilink">明石幼稚園</a></li>
<li><a href="../Page/京橋朝海幼稚園.md" title="wikilink">京橋朝海幼稚園</a></li>
<li><a href="../Page/明正幼稚園.md" title="wikilink">明正幼稚園</a></li>
<li><a href="../Page/常盤幼稚園.md" title="wikilink">常盤幼稚園</a></li>
<li><a href="../Page/日本橋幼稚園.md" title="wikilink">日本橋幼稚園</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/久松幼稚園.md" title="wikilink">久松幼稚園</a></li>
<li>有馬幼稚園</li>
<li><a href="../Page/月島幼稚園.md" title="wikilink">月島幼稚園</a></li>
<li><a href="../Page/月島第一幼稚園.md" title="wikilink">月島第一幼稚園</a></li>
<li><a href="../Page/月島第二幼稚園.md" title="wikilink">月島第二幼稚園</a></li>
<li><a href="../Page/晴海幼稚園.md" title="wikilink">晴海幼稚園</a></li>
<li><a href="../Page/豐海幼稚園.md" title="wikilink">豐海幼稚園</a></li>
</ul></td>
</tr>
</tbody>
</table>

### 私立學校

  - [日本橋女學館中學校、高等學校](../Page/日本橋女學館中學校、高等學校.md "wikilink")

## 國外設施

  - 大使館

  - 大使館

## 區立圖書館

中央區立圖書館共有3館，區民每人可分配的區立圖書館藏書數為5.5冊，是23區第二高，僅次於千代田區。23區平均為3.4冊（2012年）。

## 大眾媒體

  - 新聞社

<!-- end list -->

  - [朝日新聞社](../Page/朝日新聞社.md "wikilink")
  - [讀賣新聞集團本社](../Page/讀賣新聞集團本社.md "wikilink")、[讀賣新聞東京本社](../Page/讀賣新聞東京本社.md "wikilink")（過去本社在銀座。現在登記的本社在[千代田區](../Page/千代田區.md "wikilink")[大手町](../Page/大手町.md "wikilink")。因進行大樓改建，從2010年10月1日至2014年暫時移至銀座）
  - [日刊體育新聞社](../Page/日刊體育.md "wikilink")

<!-- end list -->

  - 放送局（廣播）

<!-- end list -->

  - [中央FM](../Page/中央FM.md "wikilink")

## 主要活動

  - [東京灣大華火祭](../Page/東京灣大華火祭.md "wikilink")（每年8月、東京港晴海埠頭）

## 出身的著名人

  - [青島幸男](../Page/青島幸男.md "wikilink") - 作家、政治家、演員、作詞家。日本橋人形町
  - [芥川龍之介](../Page/芥川龍之介.md "wikilink") - 作家、入船町
  - [泉平子](../Page/泉平子.md "wikilink") - 女演員。銀座
  - [内田有紀](../Page/内田有紀.md "wikilink") - 女演員。日本橋
  - [車田正美](../Page/車田正美.md "wikilink") - 漫畫家。月島
  - [鈴木清順](../Page/鈴木清順.md "wikilink") - 電影監督。日本橋
  - [谷崎潤一郎](../Page/谷崎潤一郎.md "wikilink") - 作家。日本橋蠣殼町（現在屬人形町）
  - [早川德次](../Page/早川德次_\(夏普\).md "wikilink") -
    [夏普創始人](../Page/夏普.md "wikilink")。日本橋久松町
  - [藤山一郎](../Page/藤山一郎.md "wikilink") - 歌手。日本橋蛎殻町
  - [松木安太郎](../Page/松木安太郎.md "wikilink") - 前J聯盟監督。日本橋小傳馬町
  - [若林正恭](../Page/若林正恭.md "wikilink") - 搞笑藝人、[奧黛麗
    (搞笑組合)](../Page/奧黛麗_\(搞笑組合\).md "wikilink")。築地

## 以中央區為舞台的作品

  - [電影](../Page/電影.md "wikilink")
      - 日本橋（1956年 大映）
      - 銀座の恋の物語（1962年 [日活](../Page/日活.md "wikilink")）
      - [女帝 SUPER QUEEN](../Page/女帝_\(漫畫\).md "wikilink")
        (2000年[金澤克次監督](../Page/金澤克次.md "wikilink")、[小澤真珠主演](../Page/小澤真珠.md "wikilink")
        )
      - [築地魚河岸三代目](../Page/築地魚河岸三代目.md "wikilink")（2008年
        [松竹](../Page/松竹.md "wikilink")、[松原信吾監督](../Page/松原信吾.md "wikilink")、[大澤隆夫主演](../Page/大澤隆夫.md "wikilink")）

<!-- end list -->

  - [連續劇](../Page/日劇.md "wikilink")
      - 水戶黃門（[TBS](../Page/TBS電視台.md "wikilink")）
      - [午餐女王](../Page/午餐女王.md "wikilink")（[富士電視台](../Page/富士電視台.md "wikilink")）
      - 瞳（[NHK](../Page/日本放送協會.md "wikilink")[晨間小說連續劇](../Page/晨間小說連續劇.md "wikilink")）
      - [黒革的手帖](../Page/黒革的手帖.md "wikilink")
      - [女帝 SUPER
        QUEEN](../Page/女帝_\(漫畫\).md "wikilink")（[朝日放送](../Page/朝日放送.md "wikilink")）
      - [女帝薫子](../Page/女帝薫子.md "wikilink")（[朝日電視台](../Page/朝日電視台.md "wikilink")）
      - [筆談女公關](../Page/筆談女公關.md "wikilink")
      - [新參者](../Page/新參者.md "wikilink")
      - [遺留捜査](../Page/遺留捜査.md "wikilink")
        第二季、第三季（[朝日電視台](../Page/朝日電視台.md "wikilink")）
      - [天氣姐姐
        (2013年電視劇)](../Page/天氣姐姐_\(2013年電視劇\).md "wikilink")（[朝日電視台](../Page/朝日電視台.md "wikilink")）

<!-- end list -->

  - [動畫](../Page/動畫.md "wikilink")、[特攝片](../Page/特攝片.md "wikilink")
      - [歸來的超人](../Page/超人力霸王傑克.md "wikilink")（TBS）
      - [月面兎兵器米娜](../Page/月面兎兵器米娜.md "wikilink")（富士電視台）

<!-- end list -->

  - [歌曲](../Page/歌曲.md "wikilink")
      - 銀座の恋の物語（[石原裕次郎](../Page/石原裕次郎.md "wikilink")、[牧村旬子](../Page/牧村旬子.md "wikilink")）

### 車牌號碼

千代田區屬品川號碼（[東京運輸支局本廳舍](../Page/東京運輸支局.md "wikilink")）。

  - 品川號碼區域

<!-- end list -->

  - 中央區、千代田區、港區、品川區、目黑區、大田區、世田谷區、澀谷區與島嶼部[1](http://wwwtb.mlit.go.jp/kanto/s_tokyo/map_riku.html)。

## 外部連結

  - [中央區](https://web.archive.org/web/20090623071655/http://www.city.chuo.lg.jp/foreign/chinese/index.html)
  - [中央區](http://www.city.chuo.lg.jp/) (JA)