**興梠里美**是一名[日本女性](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")。隸屬於[Production
baobab](../Page/Production_baobab.md "wikilink")。[東京都](../Page/東京都.md "wikilink")[武藏野市出身](../Page/武藏野市.md "wikilink")。前夫是聲優[中原茂](../Page/中原茂.md "wikilink")。

代表作品有《[少年アシベ](../Page/少年アシベ.md "wikilink")》（ゴマちゃん）、《[蠟筆小新](../Page/蠟筆小新.md "wikilink")》（野原向日葵）、《[超時空保姆](../Page/超時空保姆.md "wikilink")》（水木夏美）等。

## 人物

  - [姓氏相當難念](../Page/姓氏.md "wikilink")（從她父母的出身地[宮崎縣來看](../Page/宮崎縣.md "wikilink")，為[日本神話所由來的姓氏](../Page/日本神話.md "wikilink")\[1\]。因此將藝名取為平假名。正確的念法是「（Kourogi）」。
  - 經常飾演動物的聲音（叫聲或會講話的動物）。特技是用小孩子的聲音拒絕電話推銷\[2\]。
  - 高中時代與聲優的[辻谷耕史同校同班](../Page/辻谷耕史.md "wikilink")，座位正好也在隔壁。另外據說與原[職業棒球選手的](../Page/職業棒球選手.md "wikilink")[和田豐是隔壁班](../Page/和田豐.md "wikilink")。
  - 高中畢業後就讀短期大學當上[幼稚園](../Page/幼稚園.md "wikilink")[教師](../Page/教師.md "wikilink")。當時受到電影《》的影響立志成為聲優。在山田太平底下接受課程，透過其介紹進入PRODUCTION
    baobab。
  - 出道作是《[麵包超人](../Page/麵包超人.md "wikilink")》的小綿羊。第一個固定演出是《[少棒小魔投](../Page/少棒小魔投.md "wikilink")》的麻生薰役。本人言「快出道當時不曉得是什麼聲音所以被選上了」。在隔年《》中飾演小玉之後開始受到注目。
  - 在中擔任一角，從此時開始被稱為動物聲優。
  - 飾演了的、《[機動戰士V鋼彈](../Page/機動戰士V_GUNDAM.md "wikilink")》的、《[蠟筆小新](../Page/蠟筆小新.md "wikilink")》的**野原向日葵**等許多還不能說話的[嬰兒的聲音](../Page/嬰兒.md "wikilink")。（本人為了這件事在廣播節目中感嘆「都不讓我講人話」。）
  - 另一方面，演技的幅度逐漸增大，能夠飾演少年、母親、老婆婆之類的角色。在《機動戰士V鋼彈》中扮演了（嬰兒）、（幼女）和（成年女性）三個角色。
  - 2002年時因[聲帶疲勞近](../Page/聲帶.md "wikilink")1年間無法出聲，痛苦的程度曾令她一度考慮放棄事業。
  - 興趣是[園藝](../Page/園藝.md "wikilink")。

## 演出作品

### 電視動畫

  -
    ※ 主要角色、固定班底的角色名稱為**粗體**
    ※各作品以**開始播放年份**為基準，可能與**實際演出年份**有出入。

<!-- end list -->

  - [反斗小王子](../Page/反斗小王子.md "wikilink")（**田村愛**、亀田トメ&カメ他）
  - [哆啦A夢](../Page/哆啦A夢.md "wikilink")（ツトムくん）
  - [ローリー・ポーリー・オーリー](../Page/ローリー・ポーリー・オーリー.md "wikilink")（**ゾーイー**）
  - [超時空要塞7 銀河在呼喚我](../Page/超時空要塞7_銀河在呼喚我.md "wikilink")（ペドロ）

<!-- end list -->

  - 1988年

<!-- end list -->

  - [ハーイあっこです](../Page/ハーイあっこです.md "wikilink")（ハナコ）

<!-- end list -->

  - 1989年

<!-- end list -->

  - [獸神萊卡](../Page/獸神萊卡.md "wikilink")（轟りえ）
  - [少棒小魔投](../Page/少棒小魔投.md "wikilink")（麻生薰）
  - [樂一通](../Page/樂一通.md "wikilink")（**トゥイティー**）
  - [YAWARA\!](../Page/柔道英雌.md "wikilink") - **花園富薰子**
  - [レスラー軍団＜銀河編＞ 聖戦士ロビンJr](../Page/GO!レスラー軍団.md "wikilink")（ミミ）

<!-- end list -->

  - 1990年

<!-- end list -->

  - [忍者貓](../Page/忍者貓.md "wikilink") - 小玉
  - [小俏妞](../Page/小俏妞.md "wikilink") - 里美（第28話）、貓子（第36話）
  - [からくり剣豪伝ムサシロード](../Page/からくり剣豪伝ムサシロード.md "wikilink")（オハル）（第7、19、23、24話）

<!-- end list -->

  - 1991年

<!-- end list -->

  - ／少年アシベ2 -

  - [緊急発進セイバーキッズ](../Page/緊急発進セイバーキッズ.md "wikilink")（アリス）

  - [我與我 兩個綠蒂](../Page/我與我_兩個綠蒂.md "wikilink")（克莉絲汀娜）

<!-- end list -->

  - 1992年

<!-- end list -->

  - [-{zh-tw:超時空保姆;zh-sg:妈妈是小学四年生;zh-hk:媽媽是小學四年生;zh-cn:妈妈是小学四年生;zh-hant:超時空保姆;zh-hans:妈妈是小学四年生;zh-mo:媽媽是小學四年生;}-](../Page/媽媽是小學四年生.md "wikilink")
    - **水木夏美** ※主役作品
  - [俏皮小花仙](../Page/俏皮小花仙.md "wikilink") -
  - [蠟筆小新](../Page/蠟筆小新.md "wikilink") -
    **野原向日葵**（第203回（**1996年**）以降）、小惠
  - [少年アシベ2](../Page/少年アシベ.md "wikilink")（**ゴマちゃん**、ゆみこちゃん）
  - [風中少女 金髮珍妮](../Page/風中少女_金髮珍妮.md "wikilink")（吉米）

<!-- end list -->

  - 1993年

<!-- end list -->

  - [機動戰士V GUNDAM](../Page/機動戰士V_GUNDAM.md "wikilink") -

<!-- end list -->

  - 1994年

<!-- end list -->

  - [飛天少女豬](../Page/飛天少女豬.md "wikilink")（**國分周平**、**ぶたセッションレッド**）
  - [魔法騎士雷阿斯](../Page/魔法騎士.md "wikilink") -
  - [魔法陣天使](../Page/魔法陣天使.md "wikilink")（）

<!-- end list -->

  - 1995年

<!-- end list -->

  - [依莎美大冒險](../Page/依莎美大冒險.md "wikilink") - 東上別府菊丸
  - [ロボットパルタのいたずらテレビ](../Page/ロボットパルタのいたずらテレビ.md "wikilink")（**パルタ**）
  - [酷媽寶貝蛋](../Page/酷媽寶貝蛋.md "wikilink")（**保與田莎拉**）
  - [怪盜聖少女](../Page/怪盜聖少女.md "wikilink") - **Ruby**、涼子

<!-- end list -->

  - 1996年

<!-- end list -->

  - [神劍闖江湖](../Page/神劍闖江湖.md "wikilink") - 櫻
  - [VS騎士檸檬汽水&40炎](../Page/VS騎士檸檬汽水&40炎.md "wikilink") - **PQ**
  - [玩偶遊戲紐約篇](../Page/玩偶遊戲.md "wikilink") - 西露·漢彌頓／西西爾·哈米爾頓
  - [蒙面俠蘇洛](../Page/蒙面俠蘇洛.md "wikilink")（席薇亞）（第29話）
  - [YAT安心\!宇宙旅行](../Page/YAT安心!宇宙旅行.md "wikilink")（カネア・マリーゴールド）

<!-- end list -->

  - 1997年

<!-- end list -->

  - [神奇寶貝](../Page/神奇寶貝.md "wikilink") -
    小霞的[波克比](../Page/波克比.md "wikilink")、雷恩（阿弘的[皮卡丘](../Page/皮卡丘.md "wikilink")）
  - [少女革命](../Page/少女革命.md "wikilink") - 、**影繪少女B子**
  - [金田一少年之事件簿](../Page/金田一少年之事件簿_\(動畫\).md "wikilink") - 安岡真奈美（第47～50話）

<!-- end list -->

  - 1998年

<!-- end list -->

  - [餓沙羅鬼](../Page/餓沙羅鬼.md "wikilink") - **豪和美鈴**
  - [甜蜜小天使（第3作）](../Page/甜蜜小天使.md "wikilink")（少將）
  - [機械女神JtoX](../Page/機械女神.md "wikilink") -

<!-- end list -->

  - 1999年

<!-- end list -->

  - [快傑蒸氣偵探團](../Page/快傑蒸氣偵探團.md "wikilink")（瑪麗安妮）（第16、18話）
  - [封神演義](../Page/封神演義.md "wikilink") - 黑點虎
  - [霹靂酷樂貓](../Page/霹靂酷樂貓.md "wikilink")（奶奶）
  - [迷糊女戰士](../Page/迷糊女戰士.md "wikilink") - **肉餅**、珊卓拉、由香里、

<!-- end list -->

  - 2000年

<!-- end list -->

  - [純情房東俏房客](../Page/純情房東俏房客.md "wikilink") - 萌（第20話）

<!-- end list -->

  - 2001年

<!-- end list -->

  - [漫畫派對](../Page/漫畫派對.md "wikilink")（廣播節目主持（第2話）、桜井あさひ（第10話））
  - [魔法戰士李維](../Page/魔法戰士李維.md "wikilink")（凱薩琳）（第12話）
  - [心之圖書館](../Page/心之圖書館.md "wikilink") - 井上光（第8、9話）、井上明（光的母親、第11話）

<!-- end list -->

  - 2002年

<!-- end list -->

  - [星空的邂逅](../Page/星空的邂逅.md "wikilink") - （風見真帆）（第6、7、12話）

<!-- end list -->

  - 2003年

<!-- end list -->

  - [青檸色之戰奇譚](../Page/青檸色之戰奇譚.md "wikilink") - **九鬼**
  - [魔法少年賈修](../Page/魔法少年賈修.md "wikilink") - **小馬怪**
  - [廢棄公主](../Page/廢棄公主.md "wikilink")- 娜塔莉
  - [鋼之鍊金術師](../Page/鋼之鍊金術師.md "wikilink") - 妮娜·塔卡（第6、7話）
  - [惑星奇航](../Page/惑星奇航.md "wikilink") -
  - [トムトム☆ブー](../Page/トムトム☆ブー.md "wikilink")（**ブー**）

<!-- end list -->

  - 2005年

<!-- end list -->

  - [らいむいろ流奇譚X ～恋、教ヘテクダサイ。～](../Page/青檸色之戰奇譚.md "wikilink")（**九鬼**）
  - [漫畫派對](../Page/漫畫派對.md "wikilink") Revolution（**櫻井あさひ**、モモ）
  - [不可思議星球的雙胞胎公主](../Page/不可思議星球的雙胞胎公主.md "wikilink") - **波夢**
  - [甲蟲王者](../Page/甲蟲王者.md "wikilink")（小時的巴薩）（第21話）
  - [ともだち、なんだもん\!―コウモリのステラルーナの話](../Page/ともだち、なんだもん!―コウモリのステラルーナの話.md "wikilink")（**ステラルーナ**）
  - [地獄少女](../Page/地獄少女.md "wikilink") (インコ)(第13話)

<!-- end list -->

  - 2006年

<!-- end list -->

  - [幸運女神 各自的翅膀](../Page/幸運女神.md "wikilink") -
    長谷川空（第4～6、8、11話）※[大谷育江代演](../Page/大谷育江.md "wikilink")
  - [不可思議星球的雙胞胎公主Gyu\!](../Page/不可思議星球的雙胞胎公主.md "wikilink") - **波夢**
  - [聲優白書](../Page/聲優白書.md "wikilink") - （第12話）
  - [機神咆哮](../Page/機神咆哮.md "wikilink")（**ソーニャ**、稻田比呂乃、ルルイエ異本）
  - [飛天小女警Z](../Page/飛天小女警Z.md "wikilink")（筆怪）

<!-- end list -->

  - 2007年

<!-- end list -->

  - [青空下的約定](../Page/青空下的約定.md "wikilink") （澤城凜奈）
  - [GO！純情水泳社！](../Page/GO！純情水泳社！.md "wikilink")（武田聖斗）
  - [少女的奇幻森林](../Page/少女的奇幻森林.md "wikilink") （未由里（小寶寶））

<!-- end list -->

  - 2008年

<!-- end list -->

  - [奇奇的異想世界](../Page/奇奇的異想世界.md "wikilink") （奇奇）
  - [CLANNAD \~After Story\~](../Page/CLANNAD.md "wikilink")（**岡崎汐**）
  - [RD 潛腦調查室](../Page/RD_潛腦調查室.md "wikilink")（Elisa）

<!-- end list -->

  - 2009年

<!-- end list -->

  - [FRESH光之美少女\!(幸福精靈)](../Page/光之美少女.md "wikilink")（希風）
  - [你好 安妮 ～Before Green
    Gables](../Page/你好_安妮_～Before_Green_Gables.md "wikilink")（諾亞·湯瑪斯、瑪姬·布朗、艾拉·漢蒙）
  - [怪談餐館](../Page/怪談餐館.md "wikilink")（小菊）

<!-- end list -->

  - 2010年

<!-- end list -->

  - [祝福的鐘聲](../Page/祝福的鐘聲.md "wikilink")(フィオーレ・ベルリッティ)

<!-- end list -->

  - 2011年

<!-- end list -->

  - [日常](../Page/日常.md "wikilink")（第12話預告旁白）
  - [這樣算是殭屍嗎？](../Page/這樣算是殭屍嗎？.md "wikilink")（優克莉伍德·海爾賽茲〈第五話〉）
  - [FAIRY TAIL](../Page/FAIRY_TAIL.md "wikilink")（佛洛修）

<!-- end list -->

  - 2015年

<!-- end list -->

  - [血界戰線](../Page/血界戰線.md "wikilink")（偏執王亞莉基菈）
  - [Go！Princess 光之美少女](../Page/Go！Princess_光之美少女.md "wikilink")（緹娜）

<!-- end list -->

  - 2016年

<!-- end list -->

  - [龙珠超](../Page/龙珠超.md "wikilink")（全王）
  - [小貓奇奇 毛絨絨大冒險](../Page/奇奇的異想世界.md "wikilink")（**奇奇**）

<!-- end list -->

  - 2017年

<!-- end list -->

  - [小魔女學園](../Page/小魔女學園.md "wikilink")（魚君）

<!-- end list -->

  - 2018年

<!-- end list -->

  - [POP TEAM
    EPIC](../Page/POP_TEAM_EPIC.md "wikilink")（**POP子**〈第7話A段〉）

### OVA

  -
    ※ 主角、主要角色的角色名為**粗體**

<!-- end list -->

  - [聖傳](../Page/聖傳_\(漫畫\).md "wikilink")（愛染明王／1991年）

  - [萬能文化貓娘](../Page/萬能文化貓娘.md "wikilink") - 佳美（1992、1994年）

  - [魔物獵人妖子5·光陰霸王之亂](../Page/魔物獵人妖子.md "wikilink")（真野圓（妖子的奶奶年輕時））（1994）

  - [同級生](../Page/同級生.md "wikilink") - **田中美沙**（1994年、1995年、1996年）

  - \- ****（1994年、1995年）

  - \- 彌生（1994年）

  - [銀河少女傳說](../Page/銀河少女傳說.md "wikilink") - （1995年～1997年）

  - [偶像計畫](../Page/偶像計畫.md "wikilink") - ****（1995年～1997年）

  - [妖精姬](../Page/妖精姬.md "wikilink") - **夢乃館真理**（1995年、1996年）

  - [麵包超人 生日影帶系列](../Page/麵包超人.md "wikilink")（つくしくん／1996年）

  - \- 橘夏音（1996年）

  - [同級生2](../Page/同級生.md "wikilink") - **田中美沙**（1996年）

  - [TATOON MASTER](../Page/TATOON_MASTER.md "wikilink") - 藤松（1996年）

  - [VS騎士檸檬汽水&40FRESH](../Page/VS騎士檸檬汽水&40FRESH.md "wikilink") -
    **PQ**（1997年）

  - [魍魎游擊隊](../Page/魍魎游擊隊.md "wikilink") - **菊島雄佳**（1998年、2000年、2001年）

  - [神奇寶貝系列](../Page/神奇寶貝.md "wikilink") -
    小霞的[波克比](../Page/波克比.md "wikilink")（1998年、1999年、2000年）

  - \- 八雲未來（1999年、2000年）

  - \- （2001年）

  - [純情房東俏房客 Again](../Page/純情房東俏房客.md "wikilink") - 萌（2002年）

  - \- **八朔繪理**（2002年）

  - [Happy World\!](../Page/Happy_World!.md "wikilink") - （2003年）

**2016年**

  - [水母食堂](../Page/水母食堂.md "wikilink") - **八島夏子**

### 網路動畫

**2016年**

  - [寶可夢世代](../Page/寶可夢世代.md "wikilink")（[吉利蛋](../Page/寶可夢列表_\(102-151\).md "wikilink")）

### 劇場版動畫

  - [哆啦A夢](../Page/哆啦A夢_\(朝日電視台版電視動畫\).md "wikilink")
    [のび太と雲の王国](../Page/のび太と雲の王国.md "wikilink")（ＴＶアナウンサー／1992年）
  - [クレヨンしんちゃん ブリブリ王国の秘宝](../Page/蠟筆小新.md "wikilink")（子ザル／1994年）
  - [哆啦A夢](../Page/哆啦A夢_\(朝日電視台版電視動畫\).md "wikilink")
    [のび太の創世日記](../Page/のび太の創世日記.md "wikilink")（チュン子／1995年）
  - [クレヨンしんちゃん 暗黒タマタマ大追跡以降の作品](../Page/蠟筆小新.md "wikilink")（ひまわり／1997年～）
  - [神奇寶貝系列](../Page/神奇寶貝劇場版.md "wikilink")（小霞的[波克比](../Page/波克比.md "wikilink")／1998年～）
  - [少女革命 アドレッセンス黙示録](../Page/少女革命.md "wikilink")（チュチュ、影絵少女Ｆ子／1999年）
  - [アリーテ姫](../Page/アリーテ姫.md "wikilink")（魔女／2001年）
  - [東京教父](../Page/東京教父.md "wikilink")（清子／2003年）
  - [劇場版 魔法少年賈修 101番目的魔物](../Page/魔法少年賈修#電影.md "wikilink")（小馬怪／2004年）
  - [神奇寶貝劇場版：夢幻與波導的勇者
    路卡利歐](../Page/神奇寶貝劇場版：夢幻與波導的勇者_路卡利歐.md "wikilink")（[夢幻](../Page/夢幻.md "wikilink")／2005年）
  - [劇場版 金色のガッシュベル\!\!
    メカバルカンの来襲](../Page/魔法少年賈修#電影.md "wikilink")（ウマゴン／2005年）
  - [勇者物語](../Page/勇者物語.md "wikilink")（喬佐／2006年）
  - [劇場版 遙遠時空～舞一夜～](../Page/遙遠時空#劇場版.md "wikilink")（藤姬／2006年）※大谷育江の代役
  - [盗梦侦探](../Page/盗梦侦探.md "wikilink") (日本娃娃/2006)
  - [CLANNAD剧场版](../Page/CLANNAD.md "wikilink")（岡崎汐/2007）
  - [映画\! たまごっち
    うちゅーいちハッピーな物語\!?](../Page/映画!_たまごっち_うちゅーいちハッピーな物語!?.md "wikilink")（**ハピハピっち**／2008年）
  - [映画 プリキュアオールスターズDX
    みんなともだちっ☆奇跡の全員大集合\!](../Page/FRESH光之美少女!#劇場版.md "wikilink")（**雪芳**／2009年）
  - [映画 プリキュアオールスターズDX2
    希望の光☆レインボージュエルを守れ\!](../Page/FRESH光之美少女!#劇場版.md "wikilink")（**雪芳**／2010年）

### 遊戲

  -
    ※ 主角、主要角色的角色名為**粗體**

<!-- end list -->

  - [雀偵物語2 宇宙探偵ディバン 出動編](../Page/雀偵物語.md "wikilink")（魔獣メルリス／1992年）
  - [Rise of the Dragon ～A BLADE HUNTER
    MYSTERY～](../Page/Rise_of_the_Dragon_～A_BLADE_HUNTER_MYSTERY～.md "wikilink")（キャンディ／1992年）
  - [ムーンライトレディ](../Page/ムーンライトレディ.md "wikilink")（**彩月日和子**（いろつきひよこ／ムーンライトレディ
    アルテミス）／1993年）
  - [雀偵物語3
    セイバーエンジェル](../Page/雀偵物語.md "wikilink")（**麻尾索紀**（あさおさき／マジカルサキ）／1993年）
  - [アイドル雀士スーチーパイシリーズ](../Page/アイドル雀士スーチーパイ.md "wikilink")（櫻井美優里／1993年～2002年）
  - [夢見館の物語](../Page/夢見館の物語.md "wikilink")（妹·淑女／1993年）
  - [遊戲天國系列](../Page/遊戲天國.md "wikilink")（みさと／1995年、1997年、1998年）
  - [魔法騎士](../Page/魔法騎士.md "wikilink")（賽拉／1995年）
  - [時空幻境
    幻想傳奇SFC版](../Page/時空幻境_幻想傳奇.md "wikilink")（**ミント・アドネード**／1995年）
  - [鬼神童子ZENKI 金剛炎鬥](../Page/鬼神童子ZENKI_金剛炎鬥.md "wikilink")（望美／1995年）
  - [第4次超級機器人大戰S以後的作品](../Page/超級機器人大戰系列.md "wikilink")（プレシア・ゼノサキス、コニー・フランシス／1996年～）
  - [メルティランサー 銀河少女警察2086](../Page/銀河少女警察.md "wikilink")（スキュラ／1996年）
  - [対戦麻雀ファイナルロマンスR](../Page/ファイナルロマンス.md "wikilink")（小泉りな／1996年）
  - [クラッシュ・バンディクーシリーズ](../Page/クラッシュ・バンディクー.md "wikilink")（ポーラ、ニーナ・コルテックス／1996年～）
  - [同級生](../Page/同級生_\(遊戲\).md "wikilink")（**[田中美沙](../Page/田中美沙.md "wikilink")**／1996年）
      - [同級生麻雀](../Page/同級生_\(遊戲\).md "wikilink")（**田中美沙**／1997年）
      - [同級生2](../Page/同級生.md "wikilink")（**田中美沙**／1996年、1997年）
  - [流星雀士キララ☆スター](../Page/流星雀士キララ☆スター.md "wikilink")（川奈繪-{里}-／1996年）
  - [ライトニング・レジェンド
    ～大悟の大冒険～](../Page/ライトニング・レジェンド_～大悟の大冒険～.md "wikilink")（ノーティ・ノウ／1996年）
  - [ラダマントゥスG](../Page/ラダマントゥスG.md "wikilink")（沖田寛子／1996年）
  - [だいすき](../Page/だいすき.md "wikilink")（藍·瑠璃／1997年）
  - [バルクスラッシュ](../Page/バルクスラッシュ.md "wikilink")（キナ・デビアス／1997年）
  - [DESIRE](../Page/DESIRE.md "wikilink") （エレナ·フランソワ／1997年）
  - [魔法学園ルナ](../Page/LUNAR.md "wikilink")（アンチ／1997年）
  - ファーランドストーリー～四つの封印～（アリシア／1997年）
  - [麻雀ハイパーリアクション2](../Page/麻雀ハイパーリアクション2.md "wikilink")（ヒュパティア／1997年）
  - [みさきアグレッシヴ！](../Page/みさきアグレッシヴ！.md "wikilink")（揚麗花／1998年）
  - [AZITO2](../Page/AZITO.md "wikilink")（ピロン星人／1998年）
      - AZITO3（ピロン星人／2000年）
  - [GUILTY GEARシリーズ](../Page/GUILTY_GEAR.md "wikilink")（**メイ**／1998年～）
  - [がんばれゴエモン～来るなら恋\!
    綾繁一家の黒い影～](../Page/がんばれゴエモン.md "wikilink")（サン／1998年）
  - [ポポローグ](../Page/ポポローグ.md "wikilink")（メル／1998年）
  - [ツインズストーリー
    きみにつたえたくて…](../Page/ツインズストーリー_きみにつたえたくて….md "wikilink")（**青梅理奈**／1999年）
  - [LOVE^2 トロッコ](../Page/LOVE^2_トロッコ.md "wikilink")（イリヤ／1999年）
  - [ぷよぷよ～ん](../Page/ぷよぷよ～ん.md "wikilink")（ハーピー／1999年）
  - [ラングリッサーミレニアム](../Page/ラングリッサー.md "wikilink")（リン・マオシン／1999年）

#### 2000年以後

  - [超鋼戦紀キカイオー](../Page/超鋼戦紀キカイオー.md "wikilink")（魔法少女ポリン／2000年）
  - [仙界大戦](../Page/封神演義_\(漫画\).md "wikilink")（黒点虎／2000年）
      - 仙界通録正史（黒点虎／2001年）
  - フランベルジュの精霊（**シャルロット**／2000年）
  - 顔の無い月（**倉木鈴菜**／2000年）
  - [キミにSteady](../Page/キミにSteady.md "wikilink")（栗原成美／2000年）
  - [高機動幻想ガンパレードマーチ](../Page/高機動幻想ガンパレードマーチ.md "wikilink")（東原ののみ／2000年）
      - [ガンパレード・オーケストラ](../Page/ガンパレード・オーケストラ.md "wikilink")（東原希望／2006年）
  - [ケロケロキング](../Page/ケロケロキング.md "wikilink")（チューチュー、ルミエル、カチンコチンJr.／2000年）
      - ケロケロキングDX（ペイペイ／2003年）
  - [てんたま](../Page/てんたま.md "wikilink") （**花梨**／2001年、2003年）
  - [風のクロノア2 ～世界が望んだ忘れもの～](../Page/風のクロノア.md "wikilink")（タット／2001年）
  - [真瑠璃色之雪](../Page/真瑠璃色之雪.md "wikilink")（小琉璃／2001年4月6日）
  - [漫畫派對](../Page/漫畫派對.md "wikilink")（**櫻井旭**／2001年）
      - 漫畫派對 PORTABLE（**櫻井旭**／2005年）
  - [大乱闘スマッシュブラザーズDX](../Page/大乱闘スマッシュブラザーズDX.md "wikilink")
    （トゲピー、ピチュー／2001年）
  - [Prism Heart](../Page/Prism_Heart.md "wikilink")
    DC版（アンジェ、アーニャ／2001年）
  - [レガイア デュエルサーガ](../Page/レガイア_デュエルサーガ.md "wikilink")（マリエンヌ／2001年）
  - [王子さまLV1](../Page/王子さまLV1.md "wikilink") PS版（ナタブームの子分／2002年）
      - 王子さまLV1.5 PS版（ナタブームの子分／2003年）
  - [すべてがＦになる ～THE PERFECT
    INSIDER～](../Page/すべてがFになる.md "wikilink")（真賀田四季／2002年）
  - [フーリガン～君のなかの勇気～](../Page/フーリガン～君のなかの勇気～.md "wikilink")（マルグレーテ／2003年）
  - [スイートレガシー
    ～ボクと彼女の名もないお菓子～](../Page/Sweet_Legacy.md "wikilink")（**吉野屋よしの**／2003年）
  - ファントム ～ PHANTOM OF INFERNO ～（**キャル･ディヴェンス**／2003年）
  - [プライベートナース
    ～まりあ～](../Page/プライベートナース_～まりあ～.md "wikilink")（**宮森彩乃**、宮森由乃／2003年）
  - [らいむいろ戦奇譚☆純](../Page/青檸色之戰奇譚.md "wikilink")（**九鬼**／2004年）
  - [機神咆吼デモンベイン](../Page/機神咆吼デモンベイン.md "wikilink")（ソーニャ、稻田比呂乃、ルルイエ異本／2004年）
  - [ちゅ～かな雀士てんほー牌娘](../Page/ちゅ～かな雀士てんほー牌娘.md "wikilink")（**安倍鈴音**／2005年）
  - [CLANNADPS](../Page/CLANNAD.md "wikilink")2版（岡崎汐／2006年）
  - 山佐DigiワールドSP 燃えよ\! 功夫淑女（チーパオ）
  - [Jeanne
    d'Arc](../Page/Jeanne_d'Arc_\(ゲーム\).md "wikilink")（亨利6世／ヘンリー6世／2006年）
  - [大乱闘スマッシュブラザーズ SPECIAL](../Page/大乱闘スマッシュブラザーズ_SPECIAL.md "wikilink")
    （ピチュー／2018年）

### 外語片配音

  - ア－ス2（トゥルー・ダンジガー=J. Madison Wright）
  - [アボンリーへの道](../Page/アボンリーへの道.md "wikilink")（ドーラ・キース）（第39話以降）
  - [大長今](../Page/大長今_\(動畫\).md "wikilink")（キョンウォン王子）
  - ノック！ノック！ようこそベアーハウス（オジョ（子熊））
  - [走れ！ケリー](../Page/走れ！ケリー.md "wikilink")（クリス・パターソン）
  - [フルハウス](../Page/フルハウス.md "wikilink")（ニコラス・コクラン（ニッキー）、アレクサンダー・コクラン（アレックス））
  - [ボイスラッガー](../Page/ボイスラッガー.md "wikilink")（μ（ミュー））
  - [マイ・ボディガード](../Page/マイ・ボディガード.md "wikilink")（ピタ・ラモス）
  - [ミレニアム](../Page/ミレニアム_\(ドラマ\).md "wikilink")（ジョーダン・ブラック）
  - [ユー・ガット・メール](../Page/ユー・ガット・メール.md "wikilink")（DVD）（キャスリーン・ケリー=メグ・ライアン）
  - リトルフットシリーズ（ダッキー）

### 實寫

  - [女優魂](../Page/女優魂.md "wikilink")（テーマ女優の回に本物の声優として出演）

### 廣播

  - [同級生
    恋愛専科](../Page/同級生.md "wikilink")（[文化放送](../Page/文化放送.md "wikilink")·[大阪放送](../Page/大阪放送.md "wikilink")　[ショッカーO野と](../Page/ショッカーO野.md "wikilink")）
  - ひの出街探偵事務所・ひので街探偵事務所108（[AM神戶](../Page/AM神戶.md "wikilink")　ショッカーO野と）　※HPは数年更新されていません（正確には内部コンテンツは随時更新中）

### CD

  - 蒼のマハラジャ（モイラ）

  - [姐姐3次方](../Page/姐姐3次方.md "wikilink")（奈乃菜レモン）

  - [女孩萬歲](../Page/女孩萬歲.md "wikilink") CD綜藝 3時的點心（福山リサ）

  - [秋櫻之空](../Page/秋櫻之空.md "wikilink") ～ひなたの子鹿～（姉倉彌繪）

  - （玻）

  - [星方天使エンジェルリンクス](../Page/星方天使.md "wikilink") DRAMA TRACKSII（ハマー）

  - [老師的時間](../Page/老師的時間.md "wikilink") 2時間目（お姉さん、猿、女子校生B）

  - [天使禁獵區](../Page/天使禁獵區.md "wikilink")（メタトロン）

  - [魔法陣天使](../Page/魔法陣天使.md "wikilink") 廣播劇CD ～大迷惑\!熱血妖精の恩返し～（ミグ）

  - [銀河少女警察](../Page/銀河少女警察.md "wikilink") イースタンメトロポリス事件ファイル \#2（スキュラ）

  - [Refrain Love 2
    ～この空を、いつか見たように～](../Page/Refrain_Love.md "wikilink")（甘木柏）

### 其他

  - [山佐](../Page/山佐.md "wikilink") 燃えよ\!功夫淑女（チーパオ）（スロット機の液晶キャラ）
  - [任天堂](../Page/任天堂.md "wikilink")
    [伝説のスタフィーシリーズの](../Page/伝説のスタフィー.md "wikilink")[CM](../Page/CM.md "wikilink")（スタフィー）

## 參考資料

## 外部連結

  - [事務所公開簡歷](http://pro-baobab.jp/ladies/kourogi_s/)
  - [](https://web.archive.org/web/20061116023621/http://straycat.jpn.org/web/index.html)

[Category:Production
baobab](../Category/Production_baobab.md "wikilink")
[Category:日本女性配音員](../Category/日本女性配音員.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")

1.  [專欄―的由来―](http://www.kuis.ac.jp/chinese/column-korogi.htm)。
2.  [銀河少女警察](../Page/銀河少女警察.md "wikilink") 廣播劇CD \#2 的幕後花絮。