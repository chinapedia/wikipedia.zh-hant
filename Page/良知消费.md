**良知消費**（[英語](../Page/英語.md "wikilink")：**Ethical
consumerism**）又稱**道德消費**，是指購買符合[道德](../Page/道德.md "wikilink")[良知的](../Page/良知.md "wikilink")[商品](../Page/商品.md "wikilink")。一般而言，這是指沒有傷害或剝削[人類](../Page/人類.md "wikilink")、[動物或](../Page/動物.md "wikilink")[自然環境的商品](../Page/自然環境.md "wikilink")。良知消费除了「正面購買」符合道德的商品，或支持注重[世界整體利益而非自身](../Page/世界.md "wikilink")[利益營運模式之外](../Page/利益.md "wikilink")，亦可採取「道德抵制」的方式，拒絕購買不符合道德的商品，或是抵制違反道德的公司。

## 基本概念

### 全球道德

在《作為道德體系的全球市場》（The Global Markets As An Ethical
System）中，[約翰·麥克默特](../Page/約翰·麥克默特.md "wikilink")（John
McMurtry）主張[貨品並沒有統一的價格](../Page/貨品.md "wikilink")，因為它們本身就是不道德的舉動。這反映舊時的[爭論](../Page/爭論.md "wikilink")，尤其是[孟諾教派這樣的](../Page/阿米什人.md "wikilink")[洗禮](../Page/洗禮.md "wikilink")[教派](../Page/教派.md "wikilink")。[猶太教和](../Page/猶太教.md "wikilink")[基督教相信](../Page/基督教.md "wikilink")[上帝賜下](../Page/上帝.md "wikilink")[聖經](../Page/聖經.md "wikilink")，以[管理人們](../Page/管理.md "wikilink")，這一種論說已得到部份人的[認可](../Page/認可.md "wikilink")。部份人認為這種[論說能夠有效地阻止所有不符合道德標準的貨品](../Page/論說.md "wikilink")。

[評論家通常會爭辯](../Page/評論家.md "wikilink")，有能力改變市場結構的是[消費者至上主義](../Page/消費者至上主義.md "wikilink")，他們通常會引用消費者至上主義的[優勢來爭辯](../Page/優勢.md "wikilink")。他們還會爭論消費者至上主義根本就是[民主的](../Page/民主.md "wikilink")，更多的[金錢更能表決公司有多少市場](../Page/金錢.md "wikilink")。評論家還會爭論，持續的[信賴會反民主地導向社會](../Page/信賴.md "wikilink")。這見解暗示，儘管[民主主義是公正的](../Page/民主主義.md "wikilink")，但支投的人必須是平均[分配在任何](../Page/分配.md "wikilink")[意見上](../Page/意見.md "wikilink")，或者小[團體的少數投票人數是不會影響到](../Page/團體.md "wikilink")[市場](../Page/市場.md "wikilink")[結構](../Page/結構.md "wikilink")。

### 道德支出

[信心標準被認為是消費者消費的信心來源](../Page/信心.md "wikilink")。通常，道德標準是[貨品](../Page/貨品.md "wikilink")[市場的](../Page/市場.md "wikilink")[生產動力](../Page/生產.md "wikilink")，從生產、[收集](../Page/收集.md "wikilink")、[處理到售賣](../Page/處理.md "wikilink")，被認為是[價值鏈的一部分和是對](../Page/價值鏈.md "wikilink")[消費者應負的](../Page/消費者.md "wikilink")[責任](../Page/責任.md "wikilink")。

現在一些人正在[爭辯一個問題](../Page/爭辯.md "wikilink")，那就是「[購物比](../Page/購物.md "wikilink")[支投更](../Page/支投.md "wikilink")[重要](../Page/重要.md "wikilink")」和「購物比支投更重要」是[經濟最基礎的](../Page/經濟.md "wikilink")[推動來源](../Page/推動.md "wikilink")。一些專家相信「購物比支投更重要」是我們表達道德購物的最佳[途徑](../Page/途徑.md "wikilink")，即如果我們在意一些東西但我們去買另一方的東西，但那件我們在意的事是[高風險性的](../Page/高風險性.md "wikilink")，所以我們是不會真正地去留意或在意那件事，這就是一種簡單的[偽善行為](../Page/偽善.md "wikilink")。

## 积极购买

不論是採購[公平貿易](../Page/公平貿易.md "wikilink")、[有機農業](../Page/有機農業.md "wikilink")、[循環再造業或是當地生產所生產的貨品](../Page/循環再造.md "wikilink")，這種消費模式可以說是最重要的，因為它直接支持到[公司的](../Page/公司.md "wikilink")[經濟能力](../Page/經濟.md "wikilink")。

### 标准和标签

[Recycle001.svg](https://zh.wikipedia.org/wiki/File:Recycle001.svg "fig:Recycle001.svg")'''國際標誌。\]\]

一些标准和标签被引入来鼓励人们积极购买，例如：

  - [公平交易](../Page/公平貿易認證.md "wikilink")
  - [有機食品](../Page/有機食品.md "wikilink")
  - [有機食品贸易协会](../Page/w:en:Organic_Trade_Association.md "wikilink")
  - [美洲协作](../Page/w:en:Co-op_America.md "wikilink")
  - [树荫下栽种的](../Page/w:en:shade-grown.md "wikilink")[咖啡](../Page/咖啡.md "wikilink")
  - [符合猶太教教規的食物](../Page/符合教規的食物_\(猶太教\).md "wikilink")
  - [符合伊斯蘭教教規的食物](../Page/符合教規的食物_\(伊斯蘭教\).md "wikilink")
  - [素食主义](../Page/純素食主義.md "wikilink")
  - [放养](../Page/w:en:free-range.md "wikilink")
  - [草料喂养的牛](../Page/w:en:grass_fed_beef.md "wikilink")
  - [工人工会制造](../Page/工会.md "wikilink")
  - [海豚安全](../Page/w:en:Cetacean_bycatch.md "wikilink")
  - [資源回收再利用](../Page/資源回收再利用.md "wikilink")
  - [FSC-认证](../Page/FSC-认证.md "wikilink")
  - [Product Red](../Page/Product_Red.md "wikilink")

一些[發展中國家被規定](../Page/發展中國家.md "wikilink")，所生產的[衣物或](../Page/衣物.md "wikilink")[食物必需標明是在那發展中國家所生產的](../Page/食物.md "wikilink")，例如：[中國所有生產的衣物用標籤必須標明](../Page/中國.md "wikilink")[廠商的名稱電話和傳真號碼](../Page/廠商.md "wikilink")，这样买家能夠更加放心地購買貨品。更重要的是這種[標籤](../Page/標籤.md "wikilink")，是為了證明那貨品並不是[地下勞工所生產的](../Page/地下勞工.md "wikilink")，就好像[德國早在](../Page/德國.md "wikilink")1887年已引入這種標籤。這些標籤是貨品已經過[檢驗過程的](../Page/檢驗.md "wikilink")[象徵](../Page/象徵.md "wikilink")，但是一些標籤有可能是假的，可能因為負責檢驗的[審查員可能被](../Page/審查員.md "wikilink")[不法商人](../Page/不法商人.md "wikilink")[賄賂](../Page/賄賂.md "wikilink")。

## 拒绝购买

道德抵制的做法是避免或抵制那些消费者认为与不道德的行为相关的產品。另外，道德抵制可反映[個人](../Page/個人.md "wikilink")、[家庭](../Page/家庭.md "wikilink")、[工會或其他](../Page/工會.md "wikilink")[集團的](../Page/集團.md "wikilink")[採購決策](../Page/採購.md "wikilink")。

### 产品

  - 消費者抵制商品的原因包括：
      - [工厂化畜牧](../Page/w:en:factory_farming.md "wikilink")
      - 對[自然环境和人類健康有害](../Page/自然环境.md "wikilink")
      - [罷工](../Page/罷工.md "wikilink")

### 公司

  - 抵制公司的原因包括：
      - 被指出支持子公司的不道德行为
      - 把利润的一部分投入到例如[军需工业](../Page/军需工业.md "wikilink")

这样的抵制不仅造成利润的损失，更重要的是造成公司声誉的巨大损害，因此，在某种程度上，导致了[企業社會責任概念的发展](../Page/企業社會責任.md "wikilink")。

### 國家

例子包括：

  -
  - 消費者因為[南非种族隔离制度而抵制](../Page/南非种族隔离制度.md "wikilink")[南非的商品](../Page/南非.md "wikilink")。這些對[商品的抵制亦反映於當時](../Page/商品.md "wikilink")[德國的](../Page/德國.md "wikilink")[國家政策上](../Page/國家.md "wikilink")，並促成了[白人政府下台](../Page/白人.md "wikilink")。

## 政府的集体道德选择

大部份人們都不[贊同在](../Page/贊同.md "wikilink")[個人的](../Page/個人.md "wikilink")[道德標準上被](../Page/道德.md "wikilink")[制裁](../Page/制裁.md "wikilink")，這論說主張市民的道德選擇是經過[投票給每一方和](../Page/投票.md "wikilink")[政府的](../Page/政府.md "wikilink")[參政人員](../Page/參政人員.md "wikilink")。因此，在[民主統治的國家的大部分人民都不贊同這種論說](../Page/民主統治.md "wikilink")，例如：[和平主義國家](../Page/和平主義.md "wikilink")[哥斯大黎加拒絕購買](../Page/哥斯大黎加.md "wikilink")[軍事武器或者只會購買適量的](../Page/軍事武器.md "wikilink")[裝備](../Page/裝備.md "wikilink")、[紐西蘭拒絕允許任何人帶備](../Page/紐西蘭.md "wikilink")[武器進入](../Page/武器.md "wikilink")[國土](../Page/國土.md "wikilink")。

而且，政府以各種方式處理各種不同[類型的](../Page/類型.md "wikilink")[毒藥](../Page/毒藥.md "wikilink")，例如：人們可購買[古柯或](../Page/古柯.md "wikilink")[罌粟用於](../Page/罌粟.md "wikilink")[醫學認可的渠道](../Page/醫學.md "wikilink")，但政府已禁止[大麻貿易的運動因大麻本是](../Page/大麻.md "wikilink")[毒品](../Page/毒品.md "wikilink")。在公民中收集[藥物上所需要的花費](../Page/藥物.md "wikilink")，但政府都避免使用這些藥物，但不法分子會把這些藥物用於[不法途徑](../Page/不法途徑.md "wikilink")，例如：[恐怖分子售賣毒品以換取購買武器的](../Page/恐怖分子.md "wikilink")[經費](../Page/經費.md "wikilink")。

有證據顯示，[政府公開購買一些他們並不想要的貨品](../Page/政府.md "wikilink")。但事實上，這是一個[偽善的舉動](../Page/偽善.md "wikilink")。支持這種[政治進程的](../Page/政治.md "wikilink")[評論家說](../Page/評論家.md "wikilink")，這種[舉動是為了自己的](../Page/舉動.md "wikilink")[利益](../Page/利益.md "wikilink")，但忽略了人民的[意志和](../Page/意志.md "wikilink")[道德](../Page/道德.md "wikilink")。例如：拿[大麻和](../Page/大麻.md "wikilink")[可卡因比較](../Page/可卡因.md "wikilink")，世界上任何地方許多大麻吸食者，都助長大麻的供應，但拆散了大麻整個[供應鏈](../Page/供應鏈.md "wikilink")，因大麻有不同的製作商。相反可卡因在世界上大量生產和由[企業聯合小數目](../Page/企業.md "wikilink")[運輸即供應](../Page/運輸.md "wikilink")。儘管許多[國家已相對地把這兩種](../Page/國家.md "wikilink")[藥物管制在](../Page/藥物.md "wikilink")[法律之內](../Page/法律.md "wikilink")。

### 预算

政府決定用其它方式衝擊國內[經濟](../Page/經濟.md "wikilink")，例如：在[教育上](../Page/教育.md "wikilink")[花費更多或在](../Page/花費.md "wikilink")[武器上花費更少](../Page/武器.md "wikilink")，但最主要的問題是政府在[維持治安和](../Page/維持治安.md "wikilink")[軍隊上的花費有多少](../Page/軍隊.md "wikilink")。

通常，政府都是花費在[市民上](../Page/市民.md "wikilink")，因為政府相信他們這樣做是[值得的](../Page/值得.md "wikilink")，但此舉使政府在市民的政策，和實際的政策或[官員的](../Page/官員.md "wikilink")[政策上變得方向不明](../Page/政策.md "wikilink")，這個問題是[政治](../Page/政治.md "wikilink")[經濟學中的其中一個焦點](../Page/經濟學.md "wikilink")。[國際認可是用來鼓勵其他國家的](../Page/國際認可.md "wikilink")[經濟能力邁向](../Page/經濟.md "wikilink")[國際化](../Page/國際化.md "wikilink")，例如：防止其他國家大規模購買富[殺傷力的](../Page/殺傷力.md "wikilink")[武器](../Page/武器.md "wikilink")。

## 相关研究

市場研究集團[英國市調顧問公司](../Page/英國市調顧問公司.md "wikilink")（GfK
NOP）在五個不同的國家分別對消費者進行了調查，研究他們對各大公司的信心。該項研究的報告登於2007年2月20日的[金融時報中](../Page/金融時報.md "wikilink")。\[1\]\[2\]超於一半受訪的德國和美國消費者認為那些大公司的合作性已嚴重降低，而在英國、法國和西班牙的受訪者則有接近一半持這樣的想法。

約三分之一的受訪者表示，他們為了選擇信譽好的公司，會不惜支付更多的金錢。

由受訪者選出信譽最好的公司有：[合作集團](../Page/合作集團.md "wikilink")（The Co-operative
Group，英國公司）、[可口可樂](../Page/可口可樂.md "wikilink")（美國公司）、[達能](../Page/達能.md "wikilink")（法國公司）、[愛迪達](../Page/愛迪達.md "wikilink")（德國公司）及[雀巢](../Page/雀巢.md "wikilink")（瑞士公司）。[耐克雖是德國](../Page/耐克.md "wikilink")、美國、法國和西班牙四國消費者選出信譽最好的公司之一，但英國並不在此列。

在英國，[合作銀行](../Page/合作銀行.md "wikilink")（Co-operative
Bank）從2001年開始發表《良知消費報告》（*Ethical Consumerism
Report*）\[3\]。

## 相关定义

### 消费意识

強烈意識（Conscious
Consuming）是一個[社會舉動](../Page/社會.md "wikilink")，此舉動基於[消費者對消費的衝擊而增加了對周圍](../Page/消費者.md "wikilink")[觀察和消費者的](../Page/觀察.md "wikilink")[健康處於普通狀態](../Page/健康.md "wikilink")。這舉動也與[公司掛慮媒介的財物和公司的](../Page/公司.md "wikilink")[廣告有關](../Page/廣告.md "wikilink")，強烈意識已在[世界各地發生但是這舉動不是在一個連著的](../Page/世界.md "wikilink")[情況下發生](../Page/情況.md "wikilink")。

2003年夏天，像[阿帕影像工作站](../Page/阿帕影像工作站.md "wikilink")（Adbusters）和[新美國夢中心](../Page/新美國夢中心.md "wikilink")（Center
for a New American
Dream），強烈意識的舉動開始出現在[波士頓](../Page/波士頓.md "wikilink")，有一組人集合起來計劃了舉辦一個名為「Gift
It
Up！」的[禮品](../Page/禮品.md "wikilink")[嘉年華](../Page/嘉年華.md "wikilink")。在整個2004年，另外一組[波士頓人開始聚在一起討論一個大](../Page/波士頓.md "wikilink")[範圍的](../Page/範圍.md "wikilink")[題目](../Page/題目.md "wikilink")，這些題目包括[環境](../Page/環境.md "wikilink")[消費的](../Page/消費.md "wikilink")[衝擊到](../Page/衝擊.md "wikilink")[媒介和](../Page/媒介.md "wikilink")[廣告方面都有討論](../Page/廣告.md "wikilink")。

強烈意識已在人們的心扎下穩固的根，在[市民會](../Page/市民.md "wikilink")[評估他們的](../Page/評估.md "wikilink")[工作或](../Page/工作.md "wikilink")[生活是否](../Page/生活.md "wikilink")[平衡和花費在](../Page/平衡.md "wikilink")[物品上的](../Page/物品.md "wikilink")[金錢是否太多](../Page/金錢.md "wikilink")。如果人們減少工作，他們就會花比較多的[時間在](../Page/時間.md "wikilink")[家庭和](../Page/家庭.md "wikilink")[朋友上](../Page/朋友.md "wikilink")，人們便會自願地花費更少的時間在[購物上](../Page/購物.md "wikilink")。在他們購買新[物品時](../Page/物品.md "wikilink")，這個[決定是出自](../Page/決定.md "wikilink")[內心的](../Page/內心.md "wikilink")。
一位自稱是[顧客的人問道](../Page/顧客.md "wikilink")：「這個項目是不是根據我的[價值](../Page/價值.md "wikilink")[製造的](../Page/製造.md "wikilink")？我是不是在支持當地的[經濟呢](../Page/經濟.md "wikilink")？負責這個項目的人是不是會[公平對待這各](../Page/公平.md "wikilink")[項目](../Page/項目.md "wikilink")？這個項目是不是在最後立定的？」
這些問題的[結論就是](../Page/結論.md "wikilink")，有[意識的](../Page/意識.md "wikilink")[消費者偶然會發現他們正在支持](../Page/消費者.md "wikilink")[公平貿易和](../Page/公平貿易.md "wikilink")[辛酸行業所製造的](../Page/辛酸.md "wikilink")[商品](../Page/商品.md "wikilink")。像可以擁有這樣大的[商機的就只有美國的合作商店](../Page/商機.md "wikilink")（Co-op
America）。

### 消费主体

在Boader Regulation
System思想中，公司是個要對[顧客需求作服從的](../Page/顧客.md "wikilink")[團體](../Page/團體.md "wikilink")，這是一個[經濟](../Page/經濟.md "wikilink")、[政治和](../Page/政治.md "wikilink")[哲學分析和其中一種](../Page/哲學.md "wikilink")[政治思想](../Page/政治思想.md "wikilink")。

### 选择性礼品

由於良知消費的要求增加，消費者甚至在購物時希望得到[礼品](../Page/礼品.md "wikilink")，因此一些英國慈善團體發展了一個选择性礼品的市場，從2004年開始愈來愈普遍。這些礼品是為一些朋友和家庭買的，而最常見的礼品是[山羊](../Page/山羊.md "wikilink")，因為當把山羊寄往一個家庭後，山羊可以為該家庭提供[奶及](../Page/奶.md "wikilink")[肥料](../Page/肥料.md "wikilink")，也能生產更多的小山羊。

[樂施會](../Page/樂施會.md "wikilink")（Oxfam's gift
catalogue）就是一個售賣礼品的成功例子，共售了超於700,000份礼品，這些礼品包括漁網、[洗手間](../Page/洗手間.md "wikilink")、[水](../Page/水.md "wikilink")、教師培訓、[保險套](../Page/保險套.md "wikilink")、[山羊和](../Page/山羊.md "wikilink")[驢子](../Page/驢子.md "wikilink")。樂施會有能力籌得數百萬[英鎊救濟世界各地的貧戶](../Page/英鎊.md "wikilink")。世界上還有很多像這種售賣选择性礼品的機構，這些機構包括[基督徒互援會](../Page/基督徒互援會.md "wikilink")（Christian
Aid）和[世界展望會](../Page/世界展望會.md "wikilink")（World Vision）等。

## 批評

良知消費又稱為道德購買、[購買道德和道德](../Page/購買.md "wikilink")[購物](../Page/購物.md "wikilink")。這些另類[術語共有三個共同的](../Page/術語.md "wikilink")[假設](../Page/假設.md "wikilink")，以下是常見的三種假設：

  - **道德購物**的[結論是道德購物會影響到每一個人](../Page/結論.md "wikilink")。
  - **個別**或**集體購物**可足夠總結產品、[服務性的資料和](../Page/服務.md "wikilink")[廣告的選擇](../Page/廣告.md "wikilink")，更能使買主和賣方容易配合。
  - 集體購物不只是剝奪消費者的選擇權利還可能會增加[消費者的](../Page/消費者.md "wikilink")[花費](../Page/花費.md "wikilink")，但實際上是在改變[市場的結構](../Page/市場.md "wikilink")。

以上三項假設已經受到[質疑](../Page/質疑.md "wikilink")。例如，[完全](../Page/完全.md "wikilink")[價格](../Page/價格.md "wikilink")[會計的](../Page/會計.md "wikilink")[提倡者同樣地假設過量](../Page/提倡者.md "wikilink")[傷害](../Page/傷害.md "wikilink")、[損壞](../Page/損壞.md "wikilink")[製造](../Page/製造.md "wikilink")、[遞送和](../Page/遞送.md "wikilink")[處理的整個程序](../Page/處理.md "wikilink")，但這是有問題的做法。即使可以得到想要找的資料，但還是受到來自[發展中國家和](../Page/發展中國家.md "wikilink")[貧窮](../Page/貧窮.md "wikilink")[國家的批評](../Page/國家.md "wikilink")。但最後，公司還將會奪回流失的消費者。

## 參看

  - [反營銷](../Page/反營銷.md "wikilink")：指由經營者的努力對有害需求的[商品](../Page/商品.md "wikilink")（如[煙](../Page/煙.md "wikilink")、[酒](../Page/酒.md "wikilink")、[人口販賣](../Page/人口販賣.md "wikilink")、[賣淫](../Page/賣淫.md "wikilink")、[色情刊物](../Page/成人雜誌.md "wikilink")、[軍火和](../Page/國防工業.md "wikilink")[毒品等](../Page/毒品.md "wikilink")）進行[市場營銷層面](../Page/市場營銷.md "wikilink")（如[通路](../Page/通路.md "wikilink")）的壓抑，與良知消費這種[消費者運動在](../Page/消費者運動.md "wikilink")[價值鏈上的方向和操作者相反](../Page/價值鏈.md "wikilink")。因為利益衝突的原因，只有產業領先者才會執行，其他經營者不會以此作為[經營責任](../Page/企業社會責任.md "wikilink")。所以反營銷的操作者通常是[政府](../Page/政府.md "wikilink")，作為[宏觀調控的手段](../Page/宏觀調控.md "wikilink")。
  - [消费者](../Page/消费者.md "wikilink")
  - [道德](../Page/道德.md "wikilink")
  - [消費](../Page/消費.md "wikilink")

## 外部連結

### 一般性道德消费

  - [消費者聯盟的購物指南](http://www.eco-labels.org/home.cfm)
  - [產品與服務的證明](http://www.greenseal.org/)
  - [商店組織](http://www.ota.org/)

<!-- end list -->

  - [道德購物](http://www.ethicalshopping.blogspot.com)

### 公司和品牌信息

  - [企業評論家](http://www.corporatecritic.org/)

<!-- end list -->

  - [地球綠色市場](http://www.greenearthmarket.com)

<!-- end list -->

  - [美洲商业协作网](http://www.coopamerica.org/cabn/)

## 参考資料

<div class="references-small">

<references />

</div>

[分類:可持續性](../Page/分類:可持續性.md "wikilink")

[Category:杯葛](../Category/杯葛.md "wikilink")

1.  [1](http://www.ft.com/cms/s/d54c45ec-c086-11db-995a-000b5df10621.html)
    - 研究德國、美國、英國、法國和西班牙的市民對各大公司的信心，這研究的結果於2007年2月20日的金融時報中刊登。
2.   - 英國市調顧問公司（GfK NOP）的官方網址。
3.