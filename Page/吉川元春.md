**吉川元春**（1530年－1586年12月25日）是[日本戰國時代至](../Page/日本戰國時代.md "wikilink")[安土桃山時代的武將](../Page/安土桃山時代.md "wikilink")。父親是[毛利元就](../Page/毛利元就.md "wikilink")。

被父親元就送到[藤原南家流的](../Page/藤原南家.md "wikilink")[安藝國名門](../Page/安藝國.md "wikilink")[吉川氏為養子](../Page/吉川氏.md "wikilink")，其後繼承吉川家的[家督](../Page/家督.md "wikilink")。以後以[毛利兩川之一的身份](../Page/毛利兩川.md "wikilink")，與弟弟[隆景一同建構起](../Page/小早川隆景.md "wikilink")[毛利家發展的基礎](../Page/毛利家.md "wikilink")，主要負責管理[山陰地方](../Page/山陰地方.md "wikilink")。

## 生平

### 年幼時期

在[享祿](../Page/享祿.md "wikilink")3年（1530年）於[安藝](../Page/安藝.md "wikilink")[吉田郡山城出生](../Page/吉田郡山城.md "wikilink")，家中次男。[天文](../Page/天文_\(後奈良天皇\).md "wikilink")9年（1540年），在[出雲國的](../Page/出雲國.md "wikilink")[尼子晴久侵攻之際](../Page/尼子晴久.md "wikilink")，無視父親反對（理由是未[元服](../Page/元服.md "wikilink")）而出陣，此戰成為[初陣](../Page/初陣.md "wikilink")（[吉田郡山城之戰](../Page/吉田郡山城之戰.md "wikilink")）。

天文12年（1543年）8月，接受哥哥[毛利隆元的](../Page/毛利隆元.md "wikilink")[偏諱](../Page/偏諱.md "wikilink")（「元」字）而改名為**元春**（因為「元」字是[毛利氏的](../Page/毛利氏.md "wikilink")[通字](../Page/通字.md "wikilink")，所以不是隆元所賜的可能性相當高）。天文16年（1547年），以自己的意願而與[熊谷信直的女兒](../Page/熊谷信直.md "wikilink")[新庄局結婚](../Page/新庄局.md "wikilink")。

### 繼承吉川家

在[天文](../Page/天文_\(後奈良天皇\).md "wikilink")16年（1547年）7月成為母方的從兄[吉川興經的養子](../Page/吉川興經.md "wikilink")，這是與興經不和的叔父[吉川經世為首的吉川家臣團勸告](../Page/吉川經世.md "wikilink")，於是興經逼不得已才答應。條件是對興經的生命作出保証，於是興經的兒子[千法師成為元春的養子](../Page/吉川千法師.md "wikilink")，約定在成長後令千法師繼承[家督](../Page/家督.md "wikilink")。天文19年（1550年），元就強制令興經隱居，於是元春繼任家督並成為[吉川氏的當主](../Page/吉川氏.md "wikilink")。而后[熊谷信直等人受命殺害興經和兒子千法師](../Page/熊谷信直.md "wikilink")，於是毛利家成功奪取名門吉川家。

此後元春進入[安藝大朝的](../Page/安藝.md "wikilink")[小倉山城](../Page/小倉山城.md "wikilink")，在要地築起[日野山城並把據點移動到該城](../Page/日野山城.md "wikilink")。与弟弟[隆景一同被稱為](../Page/小早川隆景.md "wikilink")「[毛利的兩川](../Page/毛利兩川.md "wikilink")」（），負責[山陰地方的政治和軍事](../Page/山陰地方.md "wikilink")。

### 與大內、尼子爭戰

在[弘治元年](../Page/弘治.md "wikilink")（1555年）的[嚴島之戰中率領吉川軍幫助小早川軍](../Page/嚴島之戰.md "wikilink")，撃滅義兄弟[陶晴賢率領的大內軍](../Page/陶晴賢.md "wikilink")。在弘治2年（1556年）開始遠征[石見國](../Page/石見國.md "wikilink")，數度擊退[尼子晴久](../Page/尼子晴久.md "wikilink")（[忍原崩](../Page/忍原崩.md "wikilink")、[降露坂之戰](../Page/降露坂之戰.md "wikilink")）。弘治3年（1557年），在父親隱居後，與隆景一同支持毛利家並成為家中核心人物。

在[永祿](../Page/永祿.md "wikilink")8年（1565年）的[第二次月山富田城之戰中以主力身份參戰並立下極大武功](../Page/第二次月山富田城之戰.md "wikilink")，於永祿9年（1566年）降伏[尼子義久](../Page/尼子義久.md "wikilink")。

從永祿12年（1569年）開始與希望再興[尼子氏的尼子家舊臣](../Page/尼子氏.md "wikilink")[山中幸盛等人率領的尼子軍戰鬥](../Page/山中幸盛.md "wikilink")。在[布部山之戰中撃破尼子軍](../Page/布部山之戰.md "wikilink")，同年，與毛利家敵對的[大友宗麟之下寄身的](../Page/大友宗麟.md "wikilink")[大內氏一族](../Page/大內氏.md "wikilink")[大內輝弘侵攻](../Page/大內輝弘.md "wikilink")[周防國](../Page/周防國.md "wikilink")。對此，被授與軍權的元春在大友家的援軍還未完全集結時，一口氣進攻輝弘並令其自殺（[大內輝弘之亂](../Page/大內輝弘之亂.md "wikilink")）。在[元龜](../Page/元龜.md "wikilink")2年（1571年）使用謀略攻撃[尼子勝久堅守的](../Page/尼子勝久.md "wikilink")[末石城並捕獲山中幸盛](../Page/末石城.md "wikilink")，令勝久敗走（此後，幸盛用計成功逃走）。

### 與織田信長爭戰

元龜2年（1571年），父親[元就死去後](../Page/毛利元就.md "wikilink")，與弟弟[隆景一同負責輔佐姪兒](../Page/小早川隆景.md "wikilink")[毛利輝元](../Page/毛利輝元.md "wikilink")（[隆元的嫡男](../Page/毛利隆元.md "wikilink")）。

被元春擊敗的尼子勝久等人投靠在中央擴大勢力的[織田信長](../Page/織田信長.md "wikilink")，於是勝久等人在得到信長援助之下持續抵抗。還有在[天正](../Page/天正_\(日本\).md "wikilink")4年（1576年），於最後的[室町幕府將軍](../Page/室町幕府.md "wikilink")[足利義昭投靠](../Page/足利義昭.md "wikilink")[毛利氏而下向至](../Page/毛利氏.md "wikilink")[安藝國鞆後](../Page/安藝.md "wikilink")，毛利家決定與[織田氏對立](../Page/織田氏.md "wikilink")。天正5年（1577年），受織田信長之命的織田氏重臣[羽柴秀吉率領中國遠征軍侵攻](../Page/羽柴秀吉.md "wikilink")[播磨國](../Page/播磨國.md "wikilink")。元春對此迎撃，於天正6年（1578年）攻撃尼子勝久和山中幸盛死守的[上月城](../Page/上月城.md "wikilink")，勝久等人在降伏後自殺。而山中幸盛亦遭到處刑，於是令尼子再興軍被消滅（[上月城之戰](../Page/上月城之戰.md "wikilink")）。

此後元春在各地持續與織田軍戰鬥，天正8年（1580年），[三木城被攻陷](../Page/三木城.md "wikilink")，城主[別所長治自殺](../Page/別所長治.md "wikilink")。於是[備前國的](../Page/備前國.md "wikilink")[宇喜多直家和](../Page/宇喜多直家.md "wikilink")[伯耆國的](../Page/伯耆國.md "wikilink")[南條元續投向織田家](../Page/南條元續.md "wikilink")，[豐後國的](../Page/豐後國.md "wikilink")[大友宗麟亦與信長呼應而侵攻毛利的領地](../Page/大友宗麟.md "wikilink")。天正9年（1581年），[因幡國](../Page/因幡國.md "wikilink")[鳥取城的吉川一族](../Page/鳥取城.md "wikilink")[吉川經家自殺等](../Page/吉川經家.md "wikilink")，毛利家漸漸處於劣勢。

天正10年（1582年），[清水宗治等人守備的](../Page/清水宗治.md "wikilink")[高松城被羽柴秀吉攻撃](../Page/高松城.md "wikilink")，元春與輝元、隆景等人一同前往救援（[備中高松城之戰](../Page/備中高松城之戰.md "wikilink")）。不過因為秀吉的水攻而沒能作出積極的行動，而秀吉亦因為害怕與元春等人的戰鬥會令損失擴大而沒有迎撃，於是戰線陷入膠着狀態。

就在這樣的情況下，[織田信長在同年](../Page/織田信長.md "wikilink")6月2日因為[明智光秀謀反而横死](../Page/明智光秀.md "wikilink")（[本能寺之變](../Page/本能寺之變.md "wikilink")）。羽柴秀吉把本能寺之變的消息向毛利方隱密，更向毛利氏的外交僧[安國寺惠瓊送出](../Page/安國寺惠瓊.md "wikilink")「毛利家的武將中已經有很多人會投向我們」的消息，因此毛利方陷入疑惑當中，於是雙方達成和睦。結果備中高松城開城，城主清水宗治等人[切腹](../Page/切腹.md "wikilink")。織田軍從備中國撤退。

根據『[川角太閤記](../Page/川角太閤記.md "wikilink")』中記載，元春主張在此時追撃，不過被隆景制止。另一方面，在『[吉川家文書](../Page/吉川家文書.md "wikilink")』中，元春和隆景都認為追撃是無謀之舉，如果失敗的話會令毛利滅亡，於是放過為了討伐光秀而撤走的秀吉。

### 最後

在[天正](../Page/天正_\(日本\).md "wikilink")10年（1582年）末期把[家督讓予嫡男](../Page/家督.md "wikilink")[元長並隱居](../Page/吉川元長.md "wikilink")。一說這是因為不想仕於[秀吉](../Page/羽柴秀吉.md "wikilink")。此後在[吉川氏一族的](../Page/吉川氏.md "wikilink")[石經有讓出的地方建設隱居館](../Page/石經有.md "wikilink")。此館後來被稱為「[吉川元春館](../Page/吉川元春館.md "wikilink")」，不過其實在元春生前還未完成。

後來[毛利氏幫助秀吉奪取天下](../Page/毛利氏.md "wikilink")，天正13年（1585年），[隆景積極地參加秀吉的](../Page/小早川隆景.md "wikilink")[四國征伐](../Page/四國征伐.md "wikilink")，而吉川軍則以元長為總大將出陣，元春本人沒有出陣。

天正14年（1586年），受到已經成為天下人的[豐臣秀吉強烈邀請](../Page/豐臣秀吉.md "wikilink")，加上因為弟弟隆景和姪兒[輝元等人遊説](../Page/毛利輝元.md "wikilink")，於是以隱居的身份參加[九州征伐](../Page/九州征伐.md "wikilink")。不過此時元春因為[化膿性炎症](../Page/炎症.md "wikilink")（一說為[癌](../Page/癌.md "wikilink")）而令身體狀況轉差，因此在出征前就在[豐前](../Page/豐前.md "wikilink")[小倉城二之丸中死去](../Page/小倉城.md "wikilink")。享年57歲。

## 關於妻子的逸話

元春把[熊谷信直的女兒](../Page/熊谷信直.md "wikilink")[新庄局娶為正室](../Page/新庄局.md "wikilink")，一生中都沒有娶過側室並誕下4男2女。

通說指新庄局是醜女，在[宣阿的](../Page/宣阿.md "wikilink")『[陰德太平記](../Page/陰德太平記.md "wikilink")』卷十六「元春娶熊谷信直之女事」中記載，在元春與[兒玉就忠談及婚事時提出要迎娶被評為醜女的](../Page/兒玉就忠.md "wikilink")[熊谷信直的女兒](../Page/熊谷信直.md "wikilink")，在驚訝的就忠確認時，說出「明明信直的女兒醜到誰都不想跟她結婚，如果元春會娶的話，信直會很高興，為了元春而連性命都肯付出吧」（）的說話。

這段婚姻被當作是要把勇猛的[熊谷信直勢力拉成己方的政略結婚](../Page/熊谷信直.md "wikilink")，另一方面亦意味著令自己不要沉迷女色。而這段夫婦關係最後亦相當圓滿，元春和新庄局之間誕下[吉川元長](../Page/吉川元長.md "wikilink")、[毛利元氏](../Page/毛利元氏.md "wikilink")、[吉川廣家等子女](../Page/吉川廣家.md "wikilink")。但是嫁給[武田光和的信直妹妹卻被稱為絕世美人](../Page/武田光和.md "wikilink")（這個妹妹與[安藝武田氏離婚亦可能是](../Page/安藝武田氏.md "wikilink")[熊谷氏背叛安藝武田氏的原因](../Page/熊谷氏.md "wikilink")），姑姪之間的樣貌如此不同亦成為疑問，實況不明。而與此事相似有「[諸葛亮選婦](../Page/諸葛亮.md "wikilink")」的故事。

在吉川廣家氏前出現的『[安西軍策](../Page/安西軍策.md "wikilink")』中並沒有記述元春夫人（熊谷氏）的樣貌有問題。不過在[香川正矩的](../Page/香川正矩.md "wikilink")『[陰德記](../Page/陰德記.md "wikilink")』中則出現「器量（相貌？）很差」（）的記述，應該是從『陰德太平記』中轉述過來。一説是因為患有[天花而造成容顔醜惡](../Page/天花.md "wikilink")，而信直以此為理由拒絕婚約，不過元春則認為以此理由而違背約定不好，於是雙方結婚。還有，在史料中沒有記載容貌而在後期的作品中卻形容為醜陋的例子有[龐統](../Page/龐統.md "wikilink")，而答應迎娶因為生病而令容貌惡化的女子的例子有[明智光秀和](../Page/明智光秀.md "wikilink")[高橋紹運的逸話](../Page/高橋紹運.md "wikilink")，實情不明。

不過在後來元春送給被形容為傻子（）的吉川廣家的夫婦連名書狀中，似乎形容新庄局是吉川家中的良妻賢母。

## 吉川本太平記

『[太平記](../Page/太平記.md "wikilink")』是描述[日本南北朝時代爭亂的軍記故事](../Page/日本南北朝時代.md "wikilink")，所以在[日本戰國時代的武人亦相當愛讀](../Page/日本戰國時代.md "wikilink")。

在討伐尼子氏的陣中，元春曾抄寫『太平記』40卷，即是現在流傳的『吉川本』。因為吉川本太平記是元春自身書寫，現在是屬於[財團法人](../Page/財團法人.md "wikilink")[吉川報效會](../Page/吉川報效會.md "wikilink")，在[岩國市的](../Page/岩國市.md "wikilink")[吉川史料館中保管](../Page/吉川史料館.md "wikilink")。而且太平記本文是以[片假名記錄](../Page/片假名.md "wikilink")，因此亦保留了古代[日文的形式](../Page/日文.md "wikilink")。

根據最後的紅字記載，元春在[永祿](../Page/永祿.md "wikilink")6年（1563年）12月寫下第1冊，此後開始書寫其餘各卷，在永祿8年（1565年）7月完成第39冊。亦有自己寫的太平記目錄。

『太平記』有許多異本存在，其中被認為最接近原作的是『神田太平記』，不過神田本缺失了第14卷。吉川本與神田本的內容亦相當接近，而且因為全卷完備，所以被當作研究日本古典文學的貴重資料。在[昭和](../Page/昭和.md "wikilink")34年（1959年）12月18日被指定為[日本重要文化財](../Page/日本重要文化財.md "wikilink")（書蹟）。

## 登場作品

  - 小說

<!-- end list -->

  - 吉川元春：支撐毛利的勇將（[PHP研究所](../Page/PHP研究所.md "wikilink")、著）
  - 不向關白低頭的男人：吉川治部少輔元春（[文藝春秋](../Page/文藝春秋.md "wikilink")、著）

<!-- end list -->

  - 電視劇

<!-- end list -->

  - [太閤記](../Page/:JA:太閤記_\(NHK大河ドラマ\).md "wikilink")（1965年、[NHK大河劇](../Page/NHK大河劇.md "wikilink")、演：）

  - [毛利元就](../Page/毛利元就_\(大河劇\).md "wikilink")（1997年、NHK大河劇、演：[松重豐](../Page/松重豐.md "wikilink")）

  - （2011年、TX、演：）

  - [軍師官兵衛](../Page/軍師官兵衛.md "wikilink")（2014年、NHK大河劇、演：）

<!-- end list -->

  - 漫畫

<!-- end list -->

  - [戰國天正記](../Page/戰國天正記.md "wikilink")（[講談社](../Page/講談社.md "wikilink")、作）

<!-- end list -->

  - 遊戲

<!-- end list -->

  - （[SEGA](../Page/SEGA.md "wikilink")）

## 相關條目

  - [毛利氏](../Page/毛利氏.md "wikilink")
  - [吉川氏](../Page/吉川氏.md "wikilink")

## 外部連結

  - [吉川史料館](http://www.sky.icn-tv.ne.jp/~kikkawa7/)
  - [安芸吉川元春館](http://www.asahi-net.or.jp/~qb2t-nkns/kitukawa.htm)
  - [吉川元春史跡巡り](http://www.kikkawamotoharu.com)

[Category:安藝毛利氏](../Category/安藝毛利氏.md "wikilink") [Category:吉川氏
(元春流)](../Category/吉川氏_\(元春流\).md "wikilink")
[Category:戰國武將](../Category/戰國武將.md "wikilink")
[Category:1530年出生](../Category/1530年出生.md "wikilink")
[Category:1586年逝世](../Category/1586年逝世.md "wikilink")
[Category:安藝國出身人物](../Category/安藝國出身人物.md "wikilink")
[Category:日本人物神](../Category/日本人物神.md "wikilink")