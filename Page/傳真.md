[Kuroneko-FAX.jpg](https://zh.wikipedia.org/wiki/File:Kuroneko-FAX.jpg "fig:Kuroneko-FAX.jpg")

**傳真**（，全称为，源自[拉丁文](../Page/拉丁文.md "wikilink")“”，意为“製造相似”）是一種用以傳送文件[複印本的](../Page/複印本.md "wikilink")[電訊技術](../Page/電訊.md "wikilink")；而傳真機就是負責傳送這些文件的機器。隨著時代的演進，傳真的使用率持續減少。

## 概覽

傳真機其實是[影像掃描器](../Page/影像掃描器.md "wikilink")、[數據機及](../Page/數據機.md "wikilink")[電腦](../Page/電腦.md "wikilink")[打印機的一種合體](../Page/打印機.md "wikilink")，掃瞄器把文件的內容轉化成數碼影像，數據機則把影像資料透過電話線傳送，在另一端的打印機則把影像變成原文件的複印本。

現代傳真技術要到1970年代中期，當以上三種技術皆有足夠進展及合理成本後才進入實用階段。傳真機首先在[日本流行](../Page/日本.md "wikilink")，因它比其他例如teleprinter等類似技術有明顯優勢。當時由於未有易用的輸入法工具，手寫[漢字比鍵盤輸入文字來得更方便](../Page/漢字.md "wikilink")。傳真機的成本逐漸降低，到1980年代中期，傳真機已在全世界流行起來。

## 功能

傳真機使用黑白（bitonal）模式，以100x200或200x200[dpi的](../Page/dpi.md "wikilink")[解像度](../Page/解像度.md "wikilink")，每分鐘約可傳送一頁或以上的印刷或手寫文件。傳送速率為14.4kbit/s（千[比特每秒](../Page/比特.md "wikilink")）或更高，傳真機支援由2400bit/s起的速率，這種傳送影像格式稱為[ITU-T](../Page/ITU-T.md "wikilink")（前稱CCITT）fax
group 3或4。

最基本的傳真模式只可傳送黑白影像，A4大小的原件以每行1728[像素及每頁](../Page/像素.md "wikilink")1145行掃瞄，所得的資料將以專為手寫文字優化的[哈夫曼編碼技術](../Page/哈夫曼树.md "wikilink")[壓縮](../Page/壓縮.md "wikilink")，可達到1/20的壓縮率。以9600
bit/s的速率，每頁1728×1145
bits，平均1頁需要10秒作傳送，相比下未經壓縮的資料則需要3分鐘作傳送。壓縮技術採用哈夫曼codebook把每條掃瞄線中黑與白的長度編碼，亦由於兩條相鄰的掃瞄線通常十分相似，因此只把有分別之處編碼以節省[頻寬](../Page/頻寬.md "wikilink")。

用以傳送傳真的編碼技術有：改良哈夫曼編碼（Modified
Huffman，[MH编码](../Page/MH编码.md "wikilink")）、[Modified
READ](../Page/Modified_READ.md "wikilink")（MR，亦被稱為CCITT Group 3 fax
encoding或CCITT [T.4](../Page/T.4.md "wikilink")）及[Modified Modified
READ](../Page/Modified_Modified_READ.md "wikilink")（MMR，亦被稱為CCITT Group
4 fax encoding或CCITT [T.6](../Page/T.6.md "wikilink")）。

有多種傳真標準（fax classes），包括Class 1、Class 2及Intel CAS.

傳真機採用多種電話線[調製技術](../Page/調製.md "wikilink")，在數據機接通時的握手（handshaking）決定，傳真機會使用雙方皆支持的最高傳送速度，通常最低為14.4
kbit/s。

| ITU標準 | 公佈日期 | 傳送速率（bit/s）              | 編碼方法                             |
| ----- | ---- | ------------------------ | -------------------------------- |
| V.27  | 1988 | 4800, 2400               | [PSK](../Page/PSK.md "wikilink") |
| V.29  | 1988 | 9600, 7200, 4800         | [QAM](../Page/QAM.md "wikilink") |
| V.17  | 1991 | 14400, 12000, 9600, 7200 | [TCM](../Page/TCM.md "wikilink") |
| V.34  | 1994 | 28800                    | [QAM](../Page/QAM.md "wikilink") |

1970年代至1990年代的傳真機通常採用[熱感打印](../Page/熱感打印.md "wikilink")（thermal
printer）技術，但自1990年代中期開始，漸漸轉為以[熱傳打印](../Page/熱傳打印.md "wikilink")（thermal
transfer）及[噴墨打印技術為主流](../Page/噴墨打印.md "wikilink")。

噴墨打印的其中一個優點，是可以合理價格印製出彩色文件，因此，不少噴墨打印傳真機都聲稱具有彩色傳真功能。彩色傳真已經有名為ITU-T30e的標準，但這種技術仍未被廣泛支援，而大部份彩色傳真機只可與同一品牌的傳真機傳送彩色文件。

## 代替品

傳真機的現代代替品就是透過[電子郵件把電腦影像檔案以附件方式傳送](../Page/電子郵件.md "wikilink")。如此便可傳送彩色影像，並可享有更高[解像度](../Page/解像度.md "wikilink")。

如下圖的[網路傳真則是較複雜的轉換技術](../Page/網路傳真.md "wikilink")，它可以把電腦發出的文字檔或掃描圖片檔透過網路上傳至一個特殊[伺服器](../Page/伺服器.md "wikilink")，該伺服器可以把檔案跨接傳送到電話線訊號進入一般傳統傳真機的收訊方；本科技的發明在於電腦普及化的過程中還是有一些老公司沒有電腦，或是老一輩的員工不習慣使用電腦，所以需要把電子郵件和傳真訊號做雙向轉換，以使雙方均能交流。
[Processus_envoi_fax_par_internet.JPG](https://zh.wikipedia.org/wiki/File:Processus_envoi_fax_par_internet.JPG "fig:Processus_envoi_fax_par_internet.JPG")
已有不少研究讓接收一方可更有效率地處理接收到的傳真，現在的數碼儲存遠比1970年代廉宜，但[垃圾傳真則成為常見問題](../Page/垃圾傳真.md "wikilink")，也造成不少紙張浪費。一些較昂貴的通訊伺服器不會立即把接收到的傳真印出，而會把傳真集合至一個信箱，並以其他方法儲存及轉發，例如電子郵件或話音郵件，參看[統一訊息](../Page/統一訊息.md "wikilink")（Unified
Messaging）。

## 歷史

[蘇格蘭發明家Alexander](../Page/蘇格蘭.md "wikilink")
Bain於1843年取得的[專利](../Page/專利.md "wikilink")，被認為是與傳真機的出現有關，他以其對[電鐘](../Page/電鐘.md "wikilink")[鐘擺的知識製作了第一個由前至後逐行掃瞄機制](../Page/鐘擺.md "wikilink")。

1861年，第一部傳真機Pantelegraph由Giovanni Caselli出售，當時電話尚未正式被發明。

1924年，[RCA的一位設計師Richard](../Page/RCA.md "wikilink") H.
Ranger發明-{了}-wireless
photoradiogram，或[無線電傳真](../Page/無線電.md "wikilink")，是今日傳真機的前身。一幅[美國總統](../Page/美國總統.md "wikilink")[卡爾文·柯立芝的照片](../Page/卡爾文·柯立芝.md "wikilink")，在1924年11月29日由[紐約傳送至](../Page/紐約.md "wikilink")[倫敦](../Page/倫敦.md "wikilink")，成為首幅以無線電傳真技術傳送的影像，在兩年後開始了商業應用。無線電傳真直至今日仍被用作傳送天氣圖像及資訊。

一種早期的傳真方法Hellschreiber在1929年被Rudolf Hell發明，是當時機械影像掃瞄及傳送的先驅。

早期的傳真機採用[氨水來作顯影液](../Page/氨水.md "wikilink")，到後期才出現才用特別熱感紙的傳真機。

1985年，GammaLink的創辦人Hank
Magnuski博士，製作第一張[電腦傳真擴充卡GammaFax](../Page/電腦.md "wikilink")。

現時的傳真機大多數都採用[噴墨或](../Page/噴墨打印機.md "wikilink")[雷射列印](../Page/雷射打印機.md "wikilink")。

## 參看

  - [網路傳真](../Page/網路傳真.md "wikilink")

## 外部連結

  - [甚麼是傳真及傳真的歷史](http://www.worldfax.com/fax_history.shtml)
  - [FAQ:怎樣由互聯網發送傳真?](http://www.savetz.com/fax/)
  - [傳真機之死\!](http://www.duxcw.com/digest/editorial/fax.htm)
  - [網絡傳真技術](https://web.archive.org/web/20051211204610/http://www.gfi.com/whitepapers/network-fax-technology.pdf)
  - [GFI FAXmaker for Exchange/SMTP](http://www.gfi.com/faxmaker/)
    Commercial network fax server for Exchange/SMTP
  - [Hylafax](http://www.hylafax.org/)免費，開放源碼網絡傳真伺服器
  - [傳真書籍及其他數碼作品-來自喬治亞大學圖書館](https://web.archive.org/web/20060905155235/http://fax.libs.uga.edu/)

{{-}}

[Category:办公设备](../Category/办公设备.md "wikilink")
[Category:電腦週邊設備](../Category/電腦週邊設備.md "wikilink")
[傳真機](../Category/傳真機.md "wikilink")
[Category:德国发明](../Category/德国发明.md "wikilink")