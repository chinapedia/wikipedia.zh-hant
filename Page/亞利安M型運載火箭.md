**亞利安M型運載火箭**（Ariane
M），是一台臆測的[運載火箭](../Page/運載火箭.md "wikilink")，也是一台可飛行於[低地球軌道及](../Page/低地球軌道.md "wikilink")[地球同步軌道的](../Page/地球同步軌道.md "wikilink")[運載火箭](../Page/運載火箭.md "wikilink")，由[歐洲太空總署研製](../Page/歐洲太空總署.md "wikilink")，為不可重複使用的[運載火箭](../Page/運載火箭.md "wikilink")。

## 歷史

亞利安M型運載火箭第一次再1991年的會議被提及，但並未繼續發展，因為價格過於昂貴；且超過其需求。

## 未來

在2001年，[歐洲太空總署制定了](../Page/歐洲太空總署.md "wikilink")[黎明計畫](../Page/黎明計畫.md "wikilink")（Dawn），是一個為人類到[火星所做準備的計畫](../Page/火星.md "wikilink")，目標在2030年達成此里程碑，所以需要一台大型的發射運載火箭（台）。

[Category:運載火箭](../Category/運載火箭.md "wikilink")
[Category:化學火箭](../Category/化學火箭.md "wikilink")
[Category:一次性火箭](../Category/一次性火箭.md "wikilink")