**室女座星系團**（Virgo
Cluster）是一個距離在53.8±0.3百萬[光年](../Page/光年.md "wikilink")（16.5±0.1百萬[秒差距](../Page/秒差距.md "wikilink")）\[1\]，位置在[室女座方向上的](../Page/室女座.md "wikilink")[星系團](../Page/星系團.md "wikilink")。它擁有約1,300（也可能高達2,000）個星系，并組成更巨大的[室女座超星系團的中心部份](../Page/室女座超星系團.md "wikilink")，而我們銀河系所在的[本星系群只是這個集團的外圍成員](../Page/本星系群.md "wikilink")。估計這個集團的中心8度半徑（約220萬秒差距）範圍內的質量大約是1.2[M<sub>☉</sub>](../Page/太陽質量.md "wikilink")。
\[2\]

這個集團中較明亮的一些星系，包括巨大[橢圓星系](../Page/橢圓星系.md "wikilink")[M87](../Page/M87.md "wikilink")，都在1770年代末至1780年代初被梅西爾收錄在他的類似彗星天體的[目錄中](../Page/梅西爾天體列表.md "wikilink")。它們最初被形容為「不含恆星的[星雲](../Page/星雲.md "wikilink")」（nebulae
without stars），直到1920年代人們才認清它們的真正本質。

這個星系集團的中心部分在室女座中延伸的弧度長達8度，其中有許多星系都能用小望遠鏡看見。

## 特徵

這個集團中的螺旋和橢圓星系的分布相當均勻，在2004年，這個集團中的螺旋星系被認為是分布在一個舒展在長度[擴展為寬度](../Page/擴展的球體.md "wikilink")4倍的長方形細絲（從銀河系看過去）。\[3\]橢圓星系則比螺旋星系較為集中在中心。

這個星系團被認為至少分成三個[次集團](../Page/星系團.md "wikilink")，分別以[M87](../Page/M87.md "wikilink")、[M86和](../Page/M86.md "wikilink")[M49為各自的中心](../Page/M49.md "wikilink")。三個次集團之中，以包含M87的次集團佔有星系團最大比重的質量，擁有大約10<sup>14</sup>個[太陽質量](../Page/太陽質量.md "wikilink")。\[4\]

有許多星系的速度相對於星系團中心的速度高達1,600[公里](../Page/公里.md "wikilink")/秒，由這麼高的[本動速度顯示出這群星系擁有更大的質量](../Page/本動速度.md "wikilink")。

室女座星系團位於[本超星系團之中](../Page/本超星系團.md "wikilink")，它的重力影響減緩了鄰近星系的速度。這個集團巨大的質量大約減緩了本地群10%的退行速度。

<div style="clear: both">

</div>

## 室女座星系團中顯著的亮星系

下面是室女座星系團中較明亮而顯著的星系清單，他們有些是在次級團內。需要注意的是，有些星系在不同的星表中會分類至不同的子集團內（sources
\[5\] \[6\])：

### 梅西爾目錄中的天體

[M49](../Page/M49.md "wikilink")（*室女座B*）,
[M58](../Page/M58.md "wikilink")（*室女座A*）,
[M59](../Page/M59.md "wikilink")（*室女座A*或*室女座E*）,
[M60](../Page/M60.md "wikilink")（*室女座A*或*室女座E*）,
[M61](../Page/M61.md "wikilink")（*Cloud S*）,
[M84](../Page/M84.md "wikilink")（*室女座A*）,
[M85](../Page/M85.md "wikilink")（*室女座A*）,
[M86](../Page/M86.md "wikilink")（自己的子集團或*室女座A*）,
[M87](../Page/M87.md "wikilink")（*室女座A*）,
[M88](../Page/M88.md "wikilink")（*室女座A*）,
[M89](../Page/M89.md "wikilink")（*室女座A*）,
[M90](../Page/M90.md "wikilink")（*室女座A*）,
[M91](../Page/M91.md "wikilink")（*室女座A*）,
[M98](../Page/M98.md "wikilink")（*室女座A*或*Cloud N*）,
[M99](../Page/M99.md "wikilink")（*室女座A*或*Cloud N*）,
[M100](../Page/M100.md "wikilink")（*室女座A*）,

### 不在梅西爾目錄中的NGC天體

[NGC 4216](../Page/NGC_4216.md "wikilink")（*室女座A*或*Cloud N*）, [NGC
4435](../Page/雙眼星系.md "wikilink")（*室女座A*）, [NGC
4438](../Page/雙眼星系.md "wikilink")（*室女座A*）, [NGC
4450](../Page/NGC_4450.md "wikilink")（*室女座A*）, [NGC
4526](../Page/NGC_4526.md "wikilink")（*室女座B*）, [NGC
4536](../Page/NGC_4536.md "wikilink")（*Cloud S*）, [NGC
4567](../Page/NGC_4567和NGC_4568.md "wikilink")（*室女座A*）, [NGC
4568](../Page/NGC_4567和NGC_4568.md "wikilink")（*室女座A*）, [NGC
4651](../Page/NGC_4651.md "wikilink")（在星系團邊緣，不屬於任何子集團）。

<div style="clear: both">

</div>

## 相關條目

  - 靠近本星系群的星系集團，包括：[后髮座星系團](../Page/后髮座星系團.md "wikilink")、[天爐座星系團](../Page/天爐座星系團.md "wikilink")。

## 外部連結

  - [The Virgo Cluster at An Atlas of the
    Universe](http://www.atlasoftheuniverse.com/galgrps/vir.html)
  - [California Institute of Technology
    site](http://nedwww.ipac.caltech.edu/level5/Binggeli/Bin4.html) on
    Virgo cluster.
  - [Heron Island
    Proceedings](http://msowww.anu.edu.au/~heron/Tonry/tonry_p2.html)
  - [Chandra X-Ray Observatory: The Virgo
    Cluster](https://web.archive.org/web/20070203032021/http://chandra.harvard.edu/xray_sources/virgo/)
  - [The Virgo Cluster of
    Galaxies](https://web.archive.org/web/20070203003922/http://www.seds.org/messier/more/virgo.html),
    SEDS Messier pages
  - [**WIKISKY.ORG**: SDSS image, Virgo
    Cluster](http://www.wikisky.org/?object=Virgo+Cluster&img_source=SDSS&zoom=8)

## 參考資料

<div class="references-small">

<references/>

</div>

[Category:室女座](../Category/室女座.md "wikilink")
[Category:星系团](../Category/星系团.md "wikilink")
[Category:室女座超星系团](../Category/室女座超星系团.md "wikilink")

1.
2.
3.
4.  *[The Virgo Super Cluster: home of
    M87](http://nedwww.ipac.caltech.edu/level5/Binggeli/frames.html)*（with
    frames）
5.
6.