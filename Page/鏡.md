[Mirror.jpg](https://zh.wikipedia.org/wiki/File:Mirror.jpg "fig:Mirror.jpg")
**鏡子**，也稱**鑑**，是一種表面光滑，具[反射光線能力的物品](../Page/反射_\(物理學\).md "wikilink")。最常見的鏡子是平面鏡，常被人們利用來整理儀容。在科學方面，鏡子也常被使用在和[望遠鏡](../Page/望遠鏡.md "wikilink")、[雷射](../Page/雷射.md "wikilink")、工業器械等儀器上。

## 历史

|               |                                                                                                                                                                                                                                                   |
| ------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 公元前6500－5700年 | [土耳其人的](../Page/土耳其人.md "wikilink")[黑曜岩石片是距今已知最早的镜子](../Page/黑曜岩.md "wikilink")。\[1\]                                                                                                                                                             |
| 公元前2920－2770年 | [埃及人在第一王朝时首先使用金属铜制作镜子](../Page/古埃及.md "wikilink")，而居住在今[墨西哥东南部的](../Page/墨西哥.md "wikilink")[奥尔梅克人则使用磁铁](../Page/奥尔梅克文明.md "wikilink")、赤铁和黄铜。                                                                                                      |
| 公元前2500-1500年 | [中國](../Page/中國.md "wikilink")[齊家文化考古發現已使用銅鏡](../Page/齊家文化.md "wikilink")，據傳說中國銅鏡最早起源於黃帝時代。                                                                                                                                                       |
| 中世纪           | 玻璃镜子首先由[罗马人发明](../Page/罗马人.md "wikilink")，其背面是[铅制成的](../Page/铅.md "wikilink")，照出来灰蒙蒙的，因此此时的[骑士所使用的镜子都还是青铜的](../Page/骑士.md "wikilink")。                                                                                                            |
| 1460年         | 现代镜子的发明者是[威尼斯人](../Page/威尼斯.md "wikilink")，確切時間不明，此為約略年代。\[2\]                                                                                                                                                                                    |
| 1507年         | 安德里亚和盖罗首次将锡和水银的合金用于镜子背面，从而制造出了第一面精致复杂的镜子，此后镜子制造工艺成为威尼斯最重要的机密。                                                                                                                                                                                     |
| 1664年         | [法国财政大臣](../Page/法国.md "wikilink")[柯尔贝尔设法将这一工艺秘密引入法国](../Page/柯尔贝尔.md "wikilink")，威尼斯对镜子工业维持了150多年之久的垄断局面才告结束。[路易十四为了展示这一新兴工业](../Page/路易十四.md "wikilink")，还特意在[凡尔赛宫兴建了一座名為](../Page/凡尔赛宫.md "wikilink")「[镜厅](../Page/镜厅.md "wikilink")」的长廊。\[3\] |

## 成像原理

[Mirror_image_reversal.svg](https://zh.wikipedia.org/wiki/File:Mirror_image_reversal.svg "fig:Mirror_image_reversal.svg")
不論是平面鏡或者是非平面鏡（凹面鏡或凸面鏡），光線都會遵守[反射定律而被面鏡](../Page/反射定律.md "wikilink")[反射](../Page/反射.md "wikilink")，反射光線進入眼中後即可在[視網膜中形成視覺](../Page/視網膜.md "wikilink")。

在平面鏡上，當一束平行光束碰到鏡子，光束會被平行地反射出去，此時的成像和眼睛所看到的像相同。平面鏡成的像所在位置並無實際物體或光線，所以形成的是[虛像](../Page/虛像_\(光學\).md "wikilink")。

## 鏡子的運用

[MFSTWLEndOfPlaform.JPG](https://zh.wikipedia.org/wiki/File:MFSTWLEndOfPlaform.JPG "fig:MFSTWLEndOfPlaform.JPG")
[ICAC_Building_Identification_Parade_Suite_View1.jpg](https://zh.wikipedia.org/wiki/File:ICAC_Building_Identification_Parade_Suite_View1.jpg "fig:ICAC_Building_Identification_Parade_Suite_View1.jpg")
[道路安全反射鏡.JPG](https://zh.wikipedia.org/wiki/File:道路安全反射鏡.JPG "fig:道路安全反射鏡.JPG")

  - [梳妝鏡](../Page/梳妝鏡.md "wikilink")：被用來協助[化妝](../Page/化妝.md "wikilink")、刮鬍子及梳髮等整理儀容的工具。
  - [儀器](../Page/儀器.md "wikilink")：許多光學儀器、例如[望遠鏡及](../Page/望遠鏡.md "wikilink")[顯微鏡的光路中](../Page/顯微鏡.md "wikilink")，會利用鏡子來進行反射。
  - 安全：例如交通工具的後視鏡和照後鏡。有些道路的轉角會擺置凸面鏡，提醒往來行人注意安全。
  - [單面反光鏡](../Page/单向玻璃.md "wikilink")：例如證人在警局認人手續中，用作辨認疑犯用的單面鏡。
    [1](https://web.archive.org/web/20060903120841/http://phlife.7456.net/show/2/23/)

## 玻璃镀银制镜技术

### 原料及要求

1.  [玻璃应是平整无缺](../Page/玻璃.md "wikilink")，中间无气泡的透明玻璃；
2.  [硝酸银](../Page/硝酸银.md "wikilink")，含量在99.5%以上；
3.  [氨水](../Page/氨水.md "wikilink")，[浓度为](../Page/浓度.md "wikilink")25—28%；
4.  [酒石酸钾钠](../Page/酒石酸钾钠.md "wikilink")，要化学纯。

### 制作方法

1.  清洗[玻璃](../Page/玻璃.md "wikilink")：按规格裁好[玻璃后](../Page/玻璃.md "wikilink")，先用[自来水冲洗正反两面](../Page/自来水.md "wikilink")，然后将铁红粉带水涂在要镀的一面，待干后擦去铁红粉，水洗干净。再用微量的[氯化锡](../Page/氯化锡.md "wikilink")[溶液擦洗](../Page/溶液.md "wikilink")[玻璃要镀的面](../Page/玻璃.md "wikilink")。洗后用水冲净残余的[氯化亚锡](../Page/氯化亚锡.md "wikilink")。最后用干净的水（最好用蒸馏水）冲一下[玻璃](../Page/玻璃.md "wikilink")。
2.  镀银：将洗干净的[玻璃平放在水平的木架或木条上](../Page/玻璃.md "wikilink")，取银液一份和还原液一份搅拌匀倒上。药液以不流掉为度。约每平方[米](../Page/公尺.md "wikilink")2[分升左右](../Page/分升.md "wikilink")。待其渐渐在[玻璃上反应出银镜](../Page/玻璃.md "wikilink")，将多余的药液倒掉，用水冲洗，倒上百分之一的[明胶晾干](../Page/明胶.md "wikilink")。干后再在上面涂一层铁红底漆或其它防锈漆液便成了镜子。

### 药液配方

1.  银液：蒸馏水（冷开水也可）2500毫升，[硝酸银](../Page/硝酸银.md "wikilink")25克，[氨水](../Page/氨.md "wikilink")18.5毫升（经化学反应澄清为止）。
2.  还原液：蒸馏水（冷开水也可）2500毫升，[酒石酸钾钠](../Page/酒石酸钾钠.md "wikilink")25克，上液加热澄清后再放入[硝酸银](../Page/硝酸银.md "wikilink")0.5%，药液守滤后备用。
3.  [明胶液](../Page/明胶.md "wikilink")：水1000毫升，[明胶](../Page/明胶.md "wikilink")10克，隔水蒸化。
4.  铁红底[漆加适量](../Page/漆.md "wikilink")[香蕉水溶液](../Page/香蕉水.md "wikilink")。

## 相關

  - [後照鏡](../Page/後照鏡.md "wikilink")
  - [哈哈鏡](../Page/哈哈鏡.md "wikilink")
  - [曲面鏡](../Page/曲面鏡.md "wikilink")

## 参考

[Category:美容](../Category/美容.md "wikilink")
[Category:消費品](../Category/消費品.md "wikilink")
[Category:工具](../Category/工具.md "wikilink")
[镜子](../Category/镜子.md "wikilink")

1.

2.
3.