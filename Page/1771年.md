## 大事记

  - [紀曉嵐奉旨調回](../Page/紀曉嵐.md "wikilink")[北京](../Page/北京.md "wikilink")。

  - [莫斯科瘟疫](../Page/莫斯科瘟疫.md "wikilink")

  - [俄国军队占领了整个](../Page/俄国.md "wikilink")[克里木半岛](../Page/克里木半岛.md "wikilink")。

  - 越南[阮岳](../Page/阮岳.md "wikilink")、[阮惠](../Page/阮惠.md "wikilink")、[阮侶三兄弟在西山起義](../Page/阮侶.md "wikilink")，最終建立了[西山朝](../Page/西山朝.md "wikilink")。

  - [4月16日](../Page/4月16日.md "wikilink")，德國[明斯特大學創立](../Page/明斯特大學.md "wikilink")。

  - [4月24日](../Page/4月24日.md "wikilink")，[琉球](../Page/琉球國.md "wikilink")[八重山發生](../Page/八重山群島.md "wikilink")[芮氏](../Page/芮氏地震規模.md "wikilink")8級的[大地震](../Page/八重山地震.md "wikilink")，並造成大海嘯。

  - 发现[苦味酸](../Page/苦味酸.md "wikilink")。

## 出生

  - [3月16日](../Page/3月16日.md "wikilink")：[安托萬-讓·格羅](../Page/安托萬-讓·格羅.md "wikilink")，法國古典主義歷史畫家。（[1835年逝世](../Page/1835年.md "wikilink")）
  - [4月13日](../Page/4月13日.md "wikilink")：[理查·特里維西克](../Page/理查·特里維西克.md "wikilink")，英國發明家和礦業工程師。（[1833年逝世](../Page/1833年.md "wikilink")）
  - [4月18日](../Page/4月18日.md "wikilink")：[卡爾一世·菲利普](../Page/卡爾一世·菲利普_\(施瓦岑貝格\).md "wikilink")，奧地利陸軍元帥和外交家。（[1820年逝世](../Page/1820年.md "wikilink")）
  - [4月30日](../Page/4月30日.md "wikilink")：[何希爾·巴盧](../Page/何希爾·巴盧.md "wikilink")，美國普救派神學家、神學作家。（[1852年逝世](../Page/1852年.md "wikilink")）
  - [5月14日](../Page/5月14日.md "wikilink")：[羅伯特·歐文](../Page/羅伯特·歐文.md "wikilink")，英國空想社會主義者，也是一位企業家、慈善家。（[1858年逝世](../Page/1858年.md "wikilink")）
  - [6月5日](../Page/6月5日.md "wikilink")：[恩斯特·奧古斯特一世](../Page/恩斯特·奧古斯特一世_\(漢諾瓦\).md "wikilink")，漢諾瓦國王、聯合王國坎伯蘭和特維奧特戴爾公爵。（[1851年逝世](../Page/1851年.md "wikilink")）
  - [7月29日](../Page/7月29日.md "wikilink")：[惲珠](../Page/惲珠.md "wikilink")，[清朝女詩人](../Page/清朝.md "wikilink")。（[1833年逝世](../Page/1833年.md "wikilink")）
  - [8月15日](../Page/8月15日.md "wikilink")：[沃爾特·司各特](../Page/沃爾特·司各特.md "wikilink")，蘇格蘭歷史小說家、詩人。（[1832年逝世](../Page/1832年.md "wikilink")）
  - [9月5日](../Page/9月5日.md "wikilink")：[卡爾大公](../Page/卡爾大公_\(奧地利-特申\).md "wikilink")，奧地利元帥。（[1847年逝世](../Page/1847年.md "wikilink")）
  - [9月23日](../Page/9月23日.md "wikilink")：[光格天皇](../Page/光格天皇.md "wikilink")。（[1840年逝世](../Page/1840年.md "wikilink")）
  - [11月4日](../Page/11月4日.md "wikilink")：[詹姆斯·蒙哥馬利](../Page/詹姆斯·蒙哥馬利.md "wikilink")，英國編輯和詩人。（[1854年逝世](../Page/1854年.md "wikilink")）
  - [11月6日](../Page/11月6日.md "wikilink")：[阿羅斯·塞尼菲爾德](../Page/阿羅斯·塞尼菲爾德.md "wikilink")，奧地利作家及劇作家。（[1834年逝世](../Page/1834年.md "wikilink")）
  - 日期不明：[陳壽祺](../Page/陳壽祺.md "wikilink")，嘉慶四年進士。（[1834年逝世](../Page/1834年.md "wikilink")）
  - 日期不明：[陳長興](../Page/陳長興.md "wikilink")，武術家。（[1853年逝世](../Page/1853年.md "wikilink")）
  - 日期不明：[黃承吉](../Page/黃承吉.md "wikilink")，嘉慶十年進士。（[1842年逝世](../Page/1842年.md "wikilink")）
  - 日期不明：[吳衡照](../Page/吳衡照.md "wikilink")，嘉慶十六年進士，浙西詞派論家。（逝年不詳）
  - 日期不明：[伊萬·伊萬諾維奇·馬丁諾夫](../Page/伊萬·伊萬諾維奇·馬丁諾夫.md "wikilink")，俄羅斯植物學家。（[1833年逝世](../Page/1833年.md "wikilink")）
  - 日期不明：[英和](../Page/英和.md "wikilink")，乾隆五十八年進士。（[1840年逝世](../Page/1840年.md "wikilink")）

## 逝世

  - [2月12日](../Page/2月12日.md "wikilink")：[阿道夫·弗雷德里克](../Page/阿道夫·弗雷德里克.md "wikilink")，瑞典國王。（[1710年出生](../Page/1710年.md "wikilink")）
  - [3月10日](../Page/3月10日.md "wikilink")：[倫子女王](../Page/五十宮倫子女王.md "wikilink")，德川幕府第10代將軍德川家治的正室。（[1738年出生](../Page/1738年.md "wikilink")）
  - [10月14日](../Page/10月14日.md "wikilink")：[弗朗蒂舍克·布里克西](../Page/弗朗蒂舍克·布里克西.md "wikilink")，波西米亞作曲家。（[1732年出生](../Page/1732年.md "wikilink")）
  - [12月18日](../Page/12月18日.md "wikilink")：[菲利普·米勒](../Page/菲利普·米勒.md "wikilink")，蘇格蘭的植物學家。（[1691年出生](../Page/1691年.md "wikilink")）
  - 日期不明：[徐大椿](../Page/徐大椿.md "wikilink")，清朝醫學家。（[1693年出生](../Page/1693年.md "wikilink")）
  - 日期不明：[陳弘謀](../Page/陳弘謀.md "wikilink")，清朝哲學家。（[1696年出生](../Page/1696年.md "wikilink")）
  - 日期不明：[陳兆崙](../Page/陳兆崙.md "wikilink")，清朝雍正八年進士。（[1700年出生](../Page/1700年.md "wikilink")）
  - 日期不明：[恩信君](../Page/恩信君.md "wikilink")，朝鮮王朝時代的王族成員。（[1755年出生](../Page/1755年.md "wikilink")）
  - 日期不明：[林秀俊](../Page/林秀俊.md "wikilink")，台灣開拓之先驅。（[1699年出生](../Page/1699年.md "wikilink")）
  - 日期不明：[尹繼善](../Page/尹繼善.md "wikilink")，雍正元年進士。（[1696年出生](../Page/1696年.md "wikilink")）

[\*](../Category/1771年.md "wikilink")
[1年](../Category/1770年代.md "wikilink")
[7](../Category/18世纪各年.md "wikilink")