**行動資訊裝置設定檔**（，简称“MIDP”）是为类似[手机和](../Page/手机.md "wikilink")[PDA这样的](../Page/PDA.md "wikilink")[无线通讯领域的](../Page/无线通讯.md "wikilink")[嵌入式设备上的](../Page/嵌入式设备.md "wikilink")[Java语言制订的规范](../Page/Java.md "wikilink")。

MIDP是[J2ME](../Page/J2ME.md "wikilink")[架构的一部分](../Page/架构.md "wikilink")，它基于[CLDC](../Page/CLDC.md "wikilink")
Configuration ，目前版本2.0。规范内容主要是对适应于类似手机这样的产品的应用的Java
API，但限于一些必须和很常见的功能，因此经常厂商会附加一些可选包以提供对更多功能的支持。MIDP部分解决了无线终端产品上Java应用程序的兼容问题。

## 版本特性演变

MIDP 1.0对游戏和多媒体的支持很弱，在2.0中增加了诸如按键状态查询、主动渲染和基本的声音支持。
MIDP规定了一些需要支持的具体网络协议类型：1.0支持HTTP协议，2.0补充了socket、UDP、逻辑串口、HTTPS、TLS、SSL等等

## 基本API

其核心API由下层的CLDC Configuration规定，MIDP1.0增加的部分如下：

### javax.microedition.io

J2ME的I/O操作类。（CLDC中关于**通用连接**的类路径也在javax.microedition.io内）

### javax.microedition.lcdui

J2ME的[GUI类](../Page/GUI.md "wikilink")。由于手机一般使用LCD显示器，因此称为
"[LCD](../Page/LCD.md "wikilink") [UI](../Page/UI.md "wikilink")"
；但其[API并非为此特别裁剪的](../Page/API.md "wikilink")。

### javax.microedition.rms

为J2ME提供持久存储。

### javax.microedition.midlet

这是J2ME程序——[MIDlet](../Page/MIDlet.md "wikilink")——的基类。

## MIDP 2.0新增的部分API

*MIDP 2.0* saw the introduction of gaming and multimedia APIs.

### javax.microedition.media

多媒体播放的基类。

### javax.microedition.lcdui.game

为简单的2D基于sprite（用帧实现动画）游戏API。

### javax.microedition.pki

为安全连接而提供鉴权API。

## 版本歷史

  - MIDP 1.0 : 2000年9月29日
  - MIDP 2.0 : 2002年11月20日
  - MIDP 3.0 : 2009年12月9日

## 外部链接

  - [MIDP规范](http://java.sun.com/products/midp/)
  - [J2ME手机游戏开发站](http://www.j2megame.org)
  - [支持MIDP的手机列表](https://web.archive.org/web/20050613083720/http://www.club-java.com/TastePhone/J2ME/MIDP_mobile.jsp)
  - [J2MEDeveloper.com](https://web.archive.org/web/20161021134747/http://www.j2medeveloper.com/)

[Category:Java](../Category/Java.md "wikilink")