**根谷美智子**是[日本](../Page/日本.md "wikilink")[女性](../Page/女性.md "wikilink")[聲優](../Page/聲優.md "wikilink")。[福井縣](../Page/福井縣.md "wikilink")[武生市](../Page/武生市.md "wikilink")（現[越前市](../Page/越前市.md "wikilink")）出身。

代表作包括《[靈異教師神眉](../Page/靈異教師神眉.md "wikilink")》（高橋律子）、《[鋼之鍊金術師](../Page/鋼之鍊金術師.md "wikilink")》（莉莎・霍克爱）、《[交响诗篇](../Page/交响诗篇.md "wikilink")》（）、《[蠟筆小新](../Page/蜡笔小新.md "wikilink")》（小山夢冴）等。

## 人物

  - 目前剪短髮，不過初演時留的是長直的黑髮。
  - 由於外表端莊秀麗，初次出演聲優時成為當時在場業界人員議論的話題。
  - 現在是独身，本人亦急於早些結婚。
  - 親友是同為聲優的菅原祥子。
  - 與其共演《[鋼之鍊金術師](../Page/鋼之鍊金術師.md "wikilink")》的[朴璐美稱呼她大姐](../Page/朴璐美.md "wikilink")「」。
  - 愛好是健身。
  - 曾經屬[ARTSVISION事務所](../Page/ARTSVISION.md "wikilink")，現在是自由身。
  - 雖然出早期有少女角色，但現時角色類型以成熟女性居多，其熟女型聲線與[三石琴乃有相似之處](../Page/三石琴乃.md "wikilink")。

## 主要配音作品

**粗體字**為主要角色。

### 電視動畫

**1992年**

  - （）

**1994年**

  - [美少女戰士S](../Page/美少女戰士S.md "wikilink")（）
  - [咕嚕咕嚕魔法陣](../Page/咕嚕咕嚕魔法陣.md "wikilink")（）
  - [美少女戰鬥隊](../Page/美少女戰鬥隊.md "wikilink")（）
  - [勇者警察](../Page/勇者警察.md "wikilink") （）

**1995年**

  - [愛天使傳說](../Page/愛天使傳說.md "wikilink") （）
  - [羅密歐的藍天](../Page/羅密歐的藍天.md "wikilink")（伊莎貝拉・蒙特巴尼(孩子時期)）
  - [H2](../Page/H2.md "wikilink")（木根龍太郎（小學時代）、其他）
  - [鬼神童子ZENKI](../Page/鬼神童子ZENKI.md "wikilink")（奈美）
  - [恐竜冒険記ジュラトリッパー](../Page/恐竜冒険記ジュラトリッパー.md "wikilink")（）
  - [獣戦士ガルキーバ](../Page/獣戦士ガルキーバ.md "wikilink")（）
  - [新機動戰記鋼彈 W](../Page/新機動戰記鋼彈_W.md "wikilink")（女學生A、播音、女學生2）
  - [ビット・ザ・キューピッド](../Page/ビット・ザ・キューピッド.md "wikilink")（）

**1996年**

  - [天才寶貝](../Page/天才寶貝.md "wikilink")（、木村智子）
  - [靈異教師神眉](../Page/靈異教師神眉.md "wikilink")（高橋律子）
  - [近所物語](../Page/近所物語.md "wikilink")（由紀）
  - [逮捕令](../Page/逮捕令.md "wikilink")（相模大野千恵）
  - [超者ライディーン](../Page/超者ライディーン.md "wikilink")（）
  - [VS騎士檸檬汽水&40炎](../Page/VS騎士檸檬汽水&40炎.md "wikilink")（賀爾蒙·紅唇（）
  - [美少女戰士SuperS](../Page/美少女戰士SuperS.md "wikilink")（智子）
  - [魔法少女プリティサミー](../Page/魔法少女プリティサミー.md "wikilink")（香織）

**1997年**

  - [アニメがんばれゴエモン](../Page/アニメがんばれゴエモン.md "wikilink")（石川洋子）
  - [VIRUS](../Page/VIRUS.md "wikilink")（）
  - [幸運騎士](../Page/幸運騎士.md "wikilink")（）
  - [神奇寶貝第](../Page/神奇寶貝_\(1997-2002年動畫\).md "wikilink")20話（乙女）
  - [爆笑吸血鬼](../Page/爆笑吸血鬼.md "wikilink")（）

**1998年**

  - [EAT-MAN'98](../Page/EAT-MAN.md "wikilink")（）
  - [頭文字D](../Page/頭文字D.md "wikilink")（佐藤真子）
  - [白色十字架](../Page/白色十字架.md "wikilink")（麻川百合子）
  - [百變小櫻](../Page/百變小櫻.md "wikilink")（辻谷彰子）
  - [快傑蒸汽偵探團 TV ANIMATION SERIES](../Page/快傑蒸汽偵探團.md "wikilink")（）
  - [玲音](../Page/玲音.md "wikilink")（吉井惠子）
  - [麗佳公主](../Page/麗佳公主.md "wikilink")（**娃娃騎士小泉**）
  - [DTエイトロン](../Page/DTエイトロン.md "wikilink")（）
  - [危險調查員](../Page/危險調查員.md "wikilink")（）
  - [萬能文化貓娘](../Page/萬能文化貓娘.md "wikilink")（貝原二葉）
  - [夢幻拉拉](../Page/夢幻拉拉.md "wikilink")（川口理理香、**摩古（莫莫）**）
  - [桃色姊妹花](../Page/桃色姊妹花.md "wikilink")（倉方由貴）
  - [遊戲王](../Page/遊戲王.md "wikilink")（川井靜香）
  - [LEGEND OF BASARA](../Page/婆娑羅.md "wikilink")（柚香）
  - [失落的宇宙](../Page/失落的宇宙.md "wikilink")（）

**1999年**

  - [神風怪盜貞德](../Page/神風怪盜貞德.md "wikilink")（加奈子）
  - [鋼鐵天使](../Page/鋼鐵天使.md "wikilink")（小金井佳乃）
  - [網絡安琪兒](../Page/網絡安琪兒.md "wikilink")（）
  - [宇宙戰艦山本洋子](../Page/宇宙戰艦山本洋子.md "wikilink")（）
  - [地球防禦企業](../Page/地球防禦企業.md "wikilink")（大山紀子）
  - [To Heart](../Page/To_Heart.md "wikilink")（）
  - [神奇寶貝第](../Page/神奇寶貝_\(1997-2002年動畫\).md "wikilink")94話（）

**2000年**

  - [銀河冒險戰記](../Page/銀河冒險戰記.md "wikilink")（）

  - [電童](../Page/電童.md "wikilink")（）

  - （鎬臣麗子）

  - [幻想魔傳　最伝記](../Page/最遊記.md "wikilink")（花喃）

  - （綾）

  - （）

  - （）

  - （）

**2001年**

  - [銀河冒險戰記 the second stage](../Page/銀河冒險戰記.md "wikilink")（）
  - [X](../Page/X_\(漫畫\).md "wikilink") （桃生紗鵺）
  - [ガイスターズ](../Page/ガイスターズ.md "wikilink")（）
  - [カスミン](../Page/カスミン.md "wikilink")（）
  - [激闘\!クラッシュギアTURBO](../Page/激闘!クラッシュギアTURBO.md "wikilink")（）
  - [心之圖書館](../Page/心之圖書館.md "wikilink")（）
  - [魂狩](../Page/魂狩.md "wikilink")（時逆琉奈）
  - [通靈王](../Page/通靈王.md "wikilink")（）
  - [逮捕令 SECOND SEASON](../Page/逮捕令.md "wikilink")（相模大野千恵）
  - [神奇寶貝第](../Page/神奇寶貝_\(1997-2002年動畫\).md "wikilink")196話（）

**2002年**

  - [Weiß kreuz Glühen](../Page/白色十字架.md "wikilink")（伊藤明日香）
  - [攻殼機動隊 STAND ALONE
    COMPLEX](../Page/攻殼機動隊_STAND_ALONE_COMPLEX.md "wikilink")（山口之妻）
  - [東京喵喵](../Page/東京喵喵.md "wikilink")（）
  - [PIANO](../Page/PIANO.md "wikilink")（野村曉子）
  - [ヒートガイジェイ](../Page/ヒートガイジェイ.md "wikilink")（）
  - [驚爆危機](../Page/驚爆危機.md "wikilink")（**梅麗莎・毛**）

**2003年**

  - [R.O.D -THE TV-](../Page/R.O.D_-THE_TV-.md "wikilink")（**蘭西．幕張**）
  - [铁臂阿童木](../Page/铁臂阿童木_\(2003年动画\).md "wikilink")（）
  - [宇宙星路](../Page/宇宙星路.md "wikilink")（蓮花蓮）
  - [拜託了雙子星](../Page/拜託了雙子星.md "wikilink")（織部椿）
  - [神秘智慧石](../Page/神秘智慧石.md "wikilink")（、比雅朗熊（））
  - [クラッシュギアNitro](../Page/クラッシュギアNitro.md "wikilink")（真羽舞）
  - [聖槍修女](../Page/聖槍修女.md "wikilink")（**紗蝶拉‧哈本海特**）
  - [貯金大冒險](../Page/貯金大冒險.md "wikilink")（）
  - [時空冒險記](../Page/時空冒險記.md "wikilink")（姬娜）
  - [廢棄公主](../Page/廢棄公主.md "wikilink")（斯蒂爾・茱麗安）
  - [星空防衛隊](../Page/星空防衛隊.md "wikilink")（）
  - [鋼之鍊金術師](../Page/鋼之鍊金術師_\(動畫\).md "wikilink")（**莉莎・霍克愛**）
  - [PEACE MAKER鐵](../Page/PEACE_MAKER鐵.md "wikilink")（明里）
  - 驚爆危機？校園篇（**梅麗莎・毛**）
  - [神奇寶貝超世代第](../Page/神奇寶貝超世代.md "wikilink")11話（）
  - [ポポロクロイス](../Page/ポポロクロイス.md "wikilink")（）
  - [魔法使的條件](../Page/魔法使的條件.md "wikilink")（森川陽子）
  - [最後流亡](../Page/最後流亡.md "wikilink")（戴芬妮・艾拉克萊）

**2004年**

  - [阿加莎・克里絲蒂的名偵探波洛](../Page/阿加莎・克里絲蒂的名偵探波洛.md "wikilink")（）
  - [陰陽大戰記](../Page/陰陽大戰記.md "wikilink")（瑞樹）
  - [超重神Zwei](../Page/超重神GRAVION.md "wikilink")（紅綾香、）
  - [月詠 -MOON PHASE-](../Page/月詠.md "wikilink")（安西裕美）
  - [鐵人28號](../Page/鐵人28號.md "wikilink")（第4作目）（敷島鐵雄）
  - [To Heart 〜Remember my
    memories〜](../Page/To_Heart.md "wikilink")（Serio）
  - [酷伊忍者傳](../Page/酷伊忍者傳.md "wikilink")（泉）
  - [馳風！競艇王系列](../Page/馳風！競艇王.md "wikilink")（）
  - [烘焙王](../Page/烘焙王.md "wikilink")（）
  - [遊戲王GX](../Page/遊戲王GX.md "wikilink")（鮎川惠美）
  - [洛克人EXE Stream](../Page/洛克人系列.md "wikilink")（）

**2005年**

  - [UG☆アルティメットガール](../Page/UG☆アルティメットガール.md "wikilink")（）
  - [機動戰士GUNDAM SEED
    DESTINY](../Page/機動戰士GUNDAM_SEED_DESTINY.md "wikilink")（安比、希爾達·哈肯）
  - [真相之眼](../Page/真相之眼.md "wikilink")（）
  - [強殖裝甲](../Page/強殖裝甲.md "wikilink")（尾沼志津）
  - [交響詩篇](../Page/交響詩篇.md "wikilink")（**塔荷（Talho）**、）
  - [地獄少女 第一季](../Page/地獄少女_第一季.md "wikilink")（海部里穂）(第5話)
  - [哆啦A夢 (朝日電視台版本)](../Page/哆啦A夢_\(電視動畫\).md "wikilink")（）
  - [聖魔之血](../Page/聖魔之血.md "wikilink")（亞絲塔洛雪‧愛斯蘭）
  - [蜂蜜幸運草](../Page/蜂蜜幸運草.md "wikilink")（勅使河原美和子）
  - [驚爆危機\!The Second Raid](../Page/驚爆危機.md "wikilink")（**梅麗莎・毛**）
  - [神奇寶貝超世代第](../Page/神奇寶貝超世代.md "wikilink")152話（）
  - [MÄR 魔法世界](../Page/MÄR_魔法世界.md "wikilink")（黛安娜、美奈子）
  - [洛克人exe BEAST](../Page/洛克人exe系列.md "wikilink")（）

**2006年**

  - [櫻蘭高校男公關部](../Page/櫻蘭高校男公關部.md "wikilink")（綾小路姫、料理女）
  - [極速方程式](../Page/極速方程式.md "wikilink")（玉木薰）
  - [偶像宣言](../Page/偶像宣言.md "wikilink")（雲井霞）
  - [蠟筆小新](../Page/蠟筆小新.md "wikilink")（[小山夢冴](../Page/蠟筆小新角色列表#野原家近親.md "wikilink")）
  - [ザ・サード 〜蒼い瞳の少女〜](../Page/ザ・サード.md "wikilink")（）
  - [蜂蜜幸運草II](../Page/蜂蜜幸運草.md "wikilink")（勅使河原美和子）
  - [完美小姐進化論](../Page/完美小姐進化論.md "wikilink")（恭平之母）

**2007年**

  - [機動戰士GUNDAM 00](../Page/機動戰士GUNDAM_00.md "wikilink")（席琳・巴弗提亞爾）
  - [月面兔兵器米娜](../Page/月面兔兵器米娜.md "wikilink")（白姬味菜子・白鳥米娜、香西播音員）
  - [節哀唷♥二之宮同學](../Page/節哀唷♥二之宮同學.md "wikilink")（二之宮涼子）
  - [史上最強弟子兼一](../Page/史上最強弟子兼一.md "wikilink")（）
  - [獸裝機攻](../Page/獸裝機攻.md "wikilink")（）
  - [人造昆虫カブトボーグ V×V](../Page/人造昆虫カブトボーグ_V×V.md "wikilink")（）
  - [神靈狩/GHOST HOUND](../Page/神靈狩/GHOST_HOUND.md "wikilink")（鳳麗華）
  - [鬼面騎士](../Page/鬼面騎士.md "wikilink")（真行寺麗奈）
  - [零之使魔 ～雙月的騎士～](../Page/零之使魔.md "wikilink")（雅涅絲）
  - [天元突破 紅蓮螺巖](../Page/天元突破_紅蓮螺巖.md "wikilink")（亞迪妮）
  - [ぷるるんっ\!しずくちゃん](../Page/ぷるるんっ!しずくちゃん.md "wikilink")（）
  - [MOONLIGHT MILE 1stシーズン -Lift
    off-](../Page/MOONLIGHT_MILE.md "wikilink")（）
  - [MOONLIGHT MILE 2ndシーズン -Touch
    down-](../Page/MOONLIGHT_MILE.md "wikilink")（）

**2008年**

  - [ウルトラヴァイオレット:コード044](../Page/致命紫羅蘭.md "wikilink")（）
  - [機動戰士GUNDAM 00 第2季](../Page/機動戰士GUNDAM_00.md "wikilink")（席琳・巴弗提亞爾）
  - [偶像宣言 STAGE3](../Page/偶像宣言.md "wikilink")（雲井霞）
  - [鶺鴒女神](../Page/鶺鴒女神.md "wikilink")（響）
  - [零之使魔 ～三美姬的輪舞～](../Page/零之使魔.md "wikilink")（雅涅絲）
  - [SOUL EATER](../Page/SOUL_EATER.md "wikilink")（奧拉克妮）
  - [深淵傳奇](../Page/深淵傳奇.md "wikilink")（**娜塔莉亞・盧慈・奇姆拉斯卡・藍巴爾迪亞**）
  - [鐵腕女警 DECODE](../Page/鐵腕女警.md "wikilink")（）
  - [のらみみ](../Page/のらみみ.md "wikilink")（母親）
  - [破天荒遊戯](../Page/破天荒遊戯.md "wikilink")（）
  - [Battle Spirits
    少年突破馬神](../Page/Battle_Spirits_少年突破馬神.md "wikilink")（）
  - [BUS GAMER](../Page/BUS_GAMER.md "wikilink")（一之宮惠子）
  - [向陽素描×365](../Page/向陽素描.md "wikilink")（由乃之母）
  - [非常辣妹](../Page/非常辣妹.md "wikilink")（**伊井塚龍姫**）
  - [ぷるるんっ\!しずくちゃん あはっ☆](../Page/ぷるるんっ!しずくちゃん.md "wikilink")（）

**2009年**

  - [你好 安妮 ～Before Green
    Gables](../Page/你好_安妮_～Before_Green_Gables.md "wikilink")（蘇珊）
  - [迷宮塔 ～烏魯克之劍～](../Page/迷宮塔_～烏魯克之劍～.md "wikilink")（格雷米卡）
  - [FRESH光之美少女\!](../Page/FRESH光之美少女!.md "wikilink")（山吹尚子）
  - [天降之物](../Page/天降之物.md "wikilink")（哈比1）
  - [動物偵探奇魯米](../Page/動物偵探奇魯米.md "wikilink")（御子神遙）

**2010年**

  - [守護貓娘緋鞠](../Page/守護貓娘緋鞠.md "wikilink")（如月冴）
  - [刀語](../Page/刀語.md "wikilink")（真庭狂犬）
  - [天降之物Forte](../Page/天降之物.md "wikilink")（哈比1）

**2011年**

  - [GOSICK](../Page/GOSICK.md "wikilink")（蘇菲）
  - [妖怪少爺](../Page/妖怪少爺.md "wikilink")（淀殿，羽衣狐（妖怪身））
  - [最後流亡-銀翼的飛夢-](../Page/最後流亡-銀翼的飛夢-.md "wikilink")（姬素副官）

**2012年**

  - [ONE PIECE](../Page/ONE_PIECE.md "wikilink")（乙姬王妃）
  - [零之使魔F](../Page/零之使魔.md "wikilink")（雅涅絲）

**2013年**

  - [八犬傳－東方八犬異聞－](../Page/八犬傳－東方八犬異聞－.md "wikilink")（犬田小夜子）
  - [革神語](../Page/革神語.md "wikilink")（革的母親）
  - 八犬傳－東方八犬異聞－ 第2期（犬田小夜子）

**2014年**

  - [星刻龍騎士](../Page/星刻龍騎士.md "wikilink")（拉坦諾店長）
  - [CROSSANGE
    天使與龍的輪舞](../Page/CROSSANGE_天使與龍的輪舞.md "wikilink")（左菈/佐菈.埃克斯）

**2016年**

  - [3月的獅子](../Page/3月的獅子.md "wikilink")（美咲）

**2017年**

  - [ONE PIECE](../Page/ONE_PIECE.md "wikilink")（賓什莫克·麗珠/文斯莫克·蕾玖）
  - [潔癖男子青山！](../Page/潔癖男子青山！.md "wikilink")（武井美和）
  - 3月的獅子 第2期（美咲）

**2018年**

  - [伊藤潤二驚選集](../Page/伊藤潤二驚選集.md "wikilink")（女性）
  - 驚爆危機！Invisible Victory（**梅麗莎・毛**）
  - [KIRA KIRA HAPPY★
    打開吧！見習神仙精靈](../Page/KIRA_KIRA_HAPPY★_打開吧！見習神仙精靈.md "wikilink")（影浦）

### OVA

  - [科學小飛俠](../Page/科學小飛俠.md "wikilink")（）
  - [靈異教師神眉](../Page/靈異教師神眉.md "wikilink")（律子老師）
  - [甜心戰士](../Page/甜心戰士.md "wikilink")（/）
  - [鋼之鍊金術師](../Page/鋼之鍊金術師.md "wikilink")（莉莎・霍克爱）
  - [電影少女](../Page/電影少女.md "wikilink")（女子生徒）
  - [R.O.D -Read or
    Die-](../Page/R.O.D_-Read_or_Die-.md "wikilink")（ナンシー・幕張）
  - [湘南純愛組\!](../Page/湘南純愛組!#OVA.md "wikilink")（南風、夕奈）
  - [刀語](../Page/刀語.md "wikilink")（真庭 狂犬）

### 劇場版

  - [頭文字D](../Page/頭文字D.md "wikilink")（佐藤真子）
  - [靈異教師神眉](../Page/靈異教師神眉.md "wikilink")（律子老師）
  - [鋼之鍊金術師 香巴拉的征服者](../Page/鋼之鍊金術師_香巴拉的征服者.md "wikilink")（莉莎・霍克爱）

### Web動畫

  - [義呆利 Axis
    Powers](../Page/義呆利_Axis_Powers.md "wikilink")（匈牙利（伊麗莎白·海德薇莉））

### 遊戲

  - [龍騎士4](../Page/龍騎士4.md "wikilink")（）
  - [YU-NO 在這世界盡頭詠唱愛的少女](../Page/YU-NO_在這世界盡頭詠唱愛的少女.md "wikilink")（朝倉香織）
  - [神秘的世界](../Page/神秘的世界.md "wikilink")（）
  - [超級機器人大戰原創世紀](../Page/超级机器人大战·原创世纪.md "wikilink")（）
  - [深淵傳奇](../Page/深淵傳奇.md "wikilink")（）
  - [To Heart](../Page/To_Heart.md "wikilink")（）
  - [NAMCO x CAPCOM](../Page/NAMCO_x_CAPCOM.md "wikilink")（LeiLei、羅茲）
  - [鋼之鍊金術師 無法飛翔的天使](../Page/鋼之鍊金術師.md "wikilink")（莉莎・霍克爱）
  - [鋼之鍊金術師2 紅色靈藥的惡魔](../Page/鋼之鍊金術師.md "wikilink")（莉莎・霍克爱）
  - [鋼之鍊金術師3 繼承神之少女](../Page/鋼之鍊金術師.md "wikilink")（莉莎・霍克爱）
  - [鋼之鍊金術師 夢想嘉年華](../Page/鋼之鍊金術師.md "wikilink")（莉莎・霍克爱）
  - [BALDR FORCE EXE](../Page/BALDR_FORCE.md "wikilink")（紫藤彩音）
  - [劍魂系列](../Page/劍魂.md "wikilink")（蘇菲提婭）

### 廣播劇CD

  - （雪花）

### 外語配音

  - [少数派报告](../Page/少数派报告.md "wikilink")（阿加莎）
  - [神鵰俠侶](../Page/神鵰俠侶.md "wikilink")（郭芙）
  - [CSI犯罪現場: 邁阿密](../Page/CSI犯罪現場:_邁阿密.md "wikilink")（）

## 外部連結

  - [根谷美智子](http://d.hatena.ne.jp/keyword/%BA%AC%C3%AB%C8%FE%C3%D2%BB%D2)
  - [根谷美智子（IMDB資料）](http://www.imdb.com/name/nm0628649/)

[Category:日本女性配音員](../Category/日本女性配音員.md "wikilink")
[Category:福井縣出身人物](../Category/福井縣出身人物.md "wikilink")