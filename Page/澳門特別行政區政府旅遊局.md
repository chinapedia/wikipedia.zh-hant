**旅遊局**（葡文名稱：****，英文名稱：**Macau Government Tourist
Office**）是[澳門](../Page/澳門.md "wikilink")[社會文化司屬下的部門](../Page/社會文化司.md "wikilink")，負責協助制定及執行旅遊政策，以及舉辦宣傳活動推廣澳門\[1\]。澳門旅遊局前身是[澳葡政府於](../Page/澳葡政府.md "wikilink")1980年1月1日設立的**新聞旅遊司**（葡文名稱：**Direcção
dos Serviços de Turismo e Comunicação
Social**），1981年9月1日，原新聞旅遊司轄下新聞廳（葡文名稱：Repartição
de Comunicação Social）獲升格為獨立機關，並命名為**新聞署**（葡文名稱：**Gabinete de
Comunicação Social**），而原新聞旅遊司則更名為**旅遊司**（葡文名稱：**Direcção dos Serviços
de
Turismo**）\[2\]。兩部門在澳門回歸中國後改稱**[新聞局](../Page/澳門特別行政區政府新聞局.md "wikilink")**和**旅遊局**（葡文名稱不變）。前身為**旅遊司**，該司司長最後一任為霍天樂(Rodolfo
Manuel Baptista Faustino)。

## 組織架構

  - 局長、副局長
      - 行政財政廳
          - 行政及人力資源管理處
          - 財政處
      - 組織計劃及發展廳
          - 研究計劃處
          - 資訊處
      - 旅遊推廣廳
          - 市場處
          - 宣傳及製作處
          - 公共關係處
      - 傳播及對外關係廳
      - 執照及監察廳
          - 執照處
          - 監察處
      - 旅遊產品及活動廳
          - 旅遊產品處
          - 商務旅遊及活動處
          - 設施管理處
      - 培訓及質量管理廳

## 職責

  - 協助制定並執行澳門特別行政區的旅遊政策，從而將澳門特別行政區提升為世界旅遊休閒中心；
  - 促進服務質素的提高，尤其是透過旅遊方面的培訓進行；
  - 協助澳門特別行政區旅遊資源的保護及增值工作，並促進有關資源的妥善利用及作出指引；
  - 協助、鼓勵澳門特別行政區旅遊產品的改進、拓展及多元化；
  - 鼓勵及促進舉辦有利於旅遊業的活動；
  - 在主要旅遊市場推廣旅遊；
  - 與其他公共或私人實體合作全面推廣澳門特別行政區，鼓勵他們參與旅遊業的發展；
  - 在澳門特別行政區及外地作為旅遊官方代表，並確保與區域及國際旅遊組織的聯繫；
  - 就旅遊服務及旅遊產品提出立法措施；
  - 確保有關旅遊業的法律和法規獲遵守；
  - 對法律和法規上受其監管的場所及活動發出執照，並作出監督；
  - 法律和法規賦予的其他職責。

## 組織法例

  - 第28/94/M號法令（1994年6月6日《澳門政府公報》）通過旅遊基金架構及運作－若干廢止。（經一九九五年八月二十八日第45/95/M號法令修改）。
  - 第18/2011號行政法規（2011年7月18日《澳門特別行政區公報》）旅遊局的組織及運作。

## 駐外代表

  - 亞太區：[澳洲](../Page/澳洲.md "wikilink")；[紐西蘭](../Page/紐西蘭.md "wikilink")；[中國](../Page/中國.md "wikilink")
    -
    [澳門特別行政區駐北京辦事處旅遊組](../Page/澳門特別行政區駐北京辦事處.md "wikilink")、[香港](../Page/香港.md "wikilink")、[台灣](../Page/台灣.md "wikilink")；[日本](../Page/日本.md "wikilink")；[韓國](../Page/韓國.md "wikilink")；[新加坡](../Page/新加坡.md "wikilink")；[馬來西亞](../Page/馬來西亞.md "wikilink")；[泰國](../Page/泰國.md "wikilink")；[印尼](../Page/印尼.md "wikilink")；[菲律賓](../Page/菲律賓.md "wikilink")；[印度](../Page/印度.md "wikilink")
    - [新德里](../Page/新德里.md "wikilink")、[孟買](../Page/孟買.md "wikilink")
  - 美洲：[美國](../Page/美國.md "wikilink") -
    [加利福尼亞州](../Page/加利福尼亞州.md "wikilink")、[紐約](../Page/紐約.md "wikilink")
  - 歐洲：[德國](../Page/德國.md "wikilink")；[英國](../Page/英國.md "wikilink")；[比利時](../Page/比利時.md "wikilink")
    -
    澳門特區政府駐[歐盟經濟貿易辦事處](../Page/歐盟.md "wikilink")；[葡萄牙](../Page/葡萄牙.md "wikilink")
    -
    [澳門駐葡萄牙旅遊推廣暨諮詢中心](../Page/澳門駐葡萄牙旅遊推廣暨諮詢中心.md "wikilink")；[法國](../Page/法國.md "wikilink")

## 論區行賞巴士服務（已停運）

### 簡介

  - 2015年8月1日，旅遊局為促進社區旅遊及旅遊產品多元化發展，透過本地旅行社推出三條《論區行賞》巴士精華遊循環路線，將接駁至八條步行路線，方便旅客到各個社區及世界遺產「澳門歷史城區」邊行邊賞。巴士精華遊路線由駐車導遊在車上作景點講解，導賞語言包括廣東話、普通話及英語。旅客可按其興趣下車前往各區作步行觀光。
    \[3\]
  - 2015年8月1至8月14日為試運行階段，8月15日起將正式推出。
  - 2015年10月19日起對本路線調整。
  - 2015年12月底，所有路線停駛。

### 路線

  - 半島線（合併原有路線A及路線B）：
    [外港客運碼頭](../Page/外港客運碼頭.md "wikilink") →
    [漁人碼頭](../Page/漁人碼頭.md "wikilink") →
    [觀音蓮花苑](../Page/觀音蓮花苑.md "wikilink") → 財神商業中心（新八佰伴） →
    [聖地牙哥古堡酒店](../Page/聖地牙哥古堡酒店.md "wikilink") →
    [司打口](../Page/司打口.md "wikilink") →
    [關閘](../Page/關閘.md "wikilink") → 觀音堂 →
    [塔石廣場](../Page/塔石廣場.md "wikilink")
  - 路氹線（原有路線C調整）：
    [外港客運碼頭](../Page/外港客運碼頭.md "wikilink") →
    [氹仔臨時客運碼頭](../Page/氹仔臨時客運碼頭.md "wikilink")
    → [澳門國際機場](../Page/澳門國際機場.md "wikilink") →
    [官也街](../Page/官也街.md "wikilink") →
    [路環市區](../Page/路環市區.md "wikilink")

## 旅遊發展委員會

### 成員

[委員會成員](http://www.cdt.gov.mo/introduction_sub.php?id=2&lang=zh)

#### 專責工作小組

為更有系統地針對本澳旅遊業的熱門課題諮詢業界的意見及建議，旅遊發展委員會設立四個專責小組進行有關工作，分別為：

  - [人力資源專責工作小組](http://www.cdt.gov.mo/introduction_group.php?id=5&lang=zh)
  - [旅遊市場管理專責工作小組](http://www.cdt.gov.mo/introduction_group.php?id=6&lang=zh)
  - [交通、基建及航空事務專責工作小組](http://www.cdt.gov.mo/introduction_group.php?id=7&lang=zh)
  - [世界旅遊休閒中心研究專責工作小組](http://www.cdt.gov.mo/introduction_group.php?id=8&lang=zh)

## 舉行活動

  - [澳門農曆新年花車巡遊匯演](../Page/澳門農曆新年花車巡遊匯演.md "wikilink")

## 参考文献

## 外部链接

  - [日本駐外表網站](https://web.archive.org/web/20070506215945/http://www.tabicom.com/macau/)

  - [韓國駐外表網站](http://www.macau.or.kr)

  - [馬來西亞駐外表網站](https://web.archive.org/web/20100729140738/http://www.macautourism.com.my/)

  - [泰國駐外表網站](http://www.macau-thai.com)

  - [德國駐外表網站](https://web.archive.org/web/20160407015305/http://www.macau-info.de/)

  - [法國駐外表網站](http://www.macautourisme.com)

|-   |-    |-

[Category:社會文化司](../Category/社會文化司.md "wikilink")
[Category:澳门旅游](../Category/澳门旅游.md "wikilink")

1.
2.
3.