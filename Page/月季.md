**月季**（[學名](../Page/學名.md "wikilink")：**），又名**月月红**（[江苏](../Page/江苏.md "wikilink")[浙江](../Page/浙江.md "wikilink")）、**月月花**、**长春花**（[吉林](../Page/吉林.md "wikilink")[长春](../Page/长春.md "wikilink")）、**庚申蔷薇**，为[蔷薇科](../Page/蔷薇科.md "wikilink")[蔷薇属的一种](../Page/蔷薇属.md "wikilink")，原產於[中國中部的](../Page/中國.md "wikilink")[贵州](../Page/贵州.md "wikilink")、[湖北](../Page/湖北.md "wikilink")、[四川等地](../Page/四川.md "wikilink")，現遍布世界各地。

[薔薇屬](../Page/薔薇屬.md "wikilink")[栽培物種名稱登錄](../Page/栽培种.md "wikilink")，是由ISHS指定的ICRA，目前為ARS\[1\]

## 形态

矮小直立[灌木](../Page/灌木.md "wikilink")；小枝有粗壮而略带钩状的皮刺或无刺。羽状复叶，小叶3到5片，少数7片，宽[卵形或卵状](../Page/卵形.md "wikilink")[矩圆形](../Page/矩圆形.md "wikilink")，长2-6厘米，宽1-3厘米，先端渐尖，基部宽[楔形或近](../Page/楔形.md "wikilink")[圆形](../Page/圆形.md "wikilink")，边缘呈锐锯齿状，两面无毛；[叶柄和](../Page/叶柄.md "wikilink")[叶轴散生皮刺和短](../Page/叶轴.md "wikilink")[腺毛](../Page/腺毛.md "wikilink")；托叶大部附生于[叶柄之上](../Page/叶柄.md "wikilink")，边缘有[腺毛](../Page/腺毛.md "wikilink")。花常以数朵聚生；花梗长，少数较短，散生短[腺毛](../Page/腺毛.md "wikilink")；花红色或[玫瑰色](../Page/玫瑰色.md "wikilink")，直径约5厘米，微香；萼裂片卵形，羽状分裂，边缘有腺毛。[蔷薇果卵圆形或梨形](../Page/蔷薇.md "wikilink")，长1.5-2厘米，红色{{\#tag:ref|<http://www.cvh.org.cn/lsid/index.php?lsid=urn:lsid:cvh.org.cn:names:cnpc_38284&vtype=img,spm,ref,link>,
月季
中国数字植物标本馆|name=|group=}}。廣泛用於園藝栽培和切花，被世界各地所喜愛，外形與許多[薔薇屬植物相似](../Page/薔薇屬.md "wikilink")，英文中就稱為「中國玫瑰」（China
Rose），與[薔薇屬其他物種](../Page/薔薇屬.md "wikilink")[雜交後培育出多種顏色且花型各异的品種](../Page/雜交.md "wikilink")，並被冠以各種名稱。月季是很多现在园艺月季（雜交月季，Rosa
hybrida）品种的亲本:例如:Bourbons, Chinas, Hybrid Perpetuals,
Noisettes,及Teas相關\[2\],其提供了長花期，四季開花，,枝條主要向上生長與花序突出開放於植株頂部等適合商業利用的特性，目前的月季品种更是丰富，有[树状月季](../Page/树状月季.md "wikilink")、[藤本月季](../Page/藤本月季.md "wikilink")、[丰花月季](../Page/丰花月季.md "wikilink")、[大花月季](../Page/大花月季.md "wikilink")、[地被月季](../Page/地被月季.md "wikilink")、[微型月季](../Page/微型月季.md "wikilink")。

## 产地

最大的月季生產地位於[中國](../Page/中國.md "wikilink")[南陽市](../Page/南陽市.md "wikilink")[石橋鎮](../Page/石橋鎮.md "wikilink")，产品目前畅销全国各地及日本、德国、荷兰等市场。紅色月季的切花枝更成為情人間必送的禮物，並成為愛情[詩歌的主題](../Page/詩歌.md "wikilink")。

## 类别与名称

[玫瑰](../Page/玫瑰.md "wikilink")、月季和[薔薇都是薔薇屬植物](../Page/薔薇.md "wikilink")，之間有種類上的區別，沒有科屬上的差異。在[漢語中人們習慣把花朵直徑大](../Page/漢語.md "wikilink")、單生的品種稱為玫瑰或月季，小朵叢生的稱為薔薇。但在[英語中它們均稱為](../Page/英語.md "wikilink")。

### 相似物种

中國植物誌薔薇屬中:\[3\]

  - 以**玫瑰**相關為名者有4種
      - [桂味組Sect](../Page/桂味組.md "wikilink"). Cinnamomeae
          - [宿萼大叶系](../Page/宿萼大叶系.md "wikilink") Ser. Cinnamomeae
              - [玫瑰](../Page/玫瑰.md "wikilink")（[学名](../Page/学名.md "wikilink")：**）。
              - [山刺玫](../Page/山刺玫.md "wikilink") Rosa davurica
      - [小叶组](../Page/小叶组.md "wikilink") Sect. Microphyllae Crep.
          - [中甸刺玫](../Page/中甸刺玫.md "wikilink") Rosa praeluceus
      - [芹叶组](../Page/芹叶组.md "wikilink") Sect. Pimpinellifoliae
          - [五数花系](../Page/五数花系.md "wikilink") Ser. Spinosissimae
              - [黄刺玫](../Page/黄刺玫.md "wikilink") Rosa xanthina
  - 以**月季**為名者有3[種](../Page/種.md "wikilink")
      - [月季组Sect](../Page/月季组.md "wikilink"). Chinenses
          - 月季 Rosa chinensis Jacq.
          - [亮叶月季](../Page/亮叶月季.md "wikilink") Rosa lucidissima Levl.
          - [香水月季](../Page/香水月季.md "wikilink") Rosa odorata (Andr.)
            Sweet.。
  - 其餘[物種多以](../Page/物種.md "wikilink")**薔薇**為名。

## 用途

### 中國植物誌經濟用途

#### 藥用\[4\]\[5\]

  - 单瓣白木香 Rosa banksiae var. normalis
  - 美蔷薇 Rosa bella
  - 硕苞蔷薇 Rosa bracteata
  - 月季花 Rosa chinensis
  - 山刺玫 Rosa davurica
  - 光叶山刺玫 Rosa davurica var. glabra
  - 金樱子 Rosa laevigata
  - 大叶蔷薇 Rosa macrophylla
  - 伞花蔷薇 Rosa maximowicziana
  - 粉团蔷薇 Rosa multiflora var. cathayensis
  - 峨眉蔷薇 Rosa omeiensis
  - 缫丝花 Rosa roxburghii
  - 玫瑰 Rosa rugosa

#### 油脂(精油\\树胶\\鞣质)\[6\]\[7\]

  - 木香花 Rosa banksiae
  - 单瓣白木香 Rosa banksiae var. normalis
  - 美蔷薇 Rosa bella
  - 月季花 Rosa chinensis
  - 突厥蔷薇 Rosa damascena
  - 山刺玫 Rosa davurica
  - 光叶山刺玫 Rosa davurica var. glabra
  - 金樱子 Rosa laevigata
  - 粉团蔷薇 Rosa multiflora var. cathayensis
  - 峨眉蔷薇 Rosa omeiensis
  - 悬钩子蔷薇 Rosa rubus

#### 中國藥典中的月季\[8\]

Rosa chinensis Jacq.的干燥花

  - 【性味与归经】 甘，温。归肝经。
  - 【功能与主治】 活血调经，疏肝解郁。用于气滞血瘀，月经不调，痛经，闭经，胸胁胀痛。

但須注意的是因為其列於中國藥典中，但未出現於食品相關名錄例如既是食品又是药品的物品名单\[9\]因此在作為食品的的情況，法規上可能僅限於具有月季食品地方標準的地區。另亦未列於可用于保健食品的物品名单中\[10\]，因此在作為保健品的狀況，法規上可能僅限於具有月季保健使用地方標準的地區。

## 图片展示

[File:Rosa-chinensis.JPG|月季](File:Rosa-chinensis.JPG%7C月季)
[File:Rosa_chinensis.jpg|重瓣栽培种](File:Rosa_chinensis.jpg%7C重瓣栽培种)
[File:Rosa_chinensis0.jpg|月季](File:Rosa_chinensis0.jpg%7C月季) 'Old
Blush'
[File:Rosa_chinensis_1795.jpg|18世纪的绘图](File:Rosa_chinensis_1795.jpg%7C18世纪的绘图)
<File:Chinese-Rose.jpg> <File:Rosa> chinensis 04-08-2012 01.jpg
<File:Werners> Liebling.jpg <File:Rosa> Bengale Rouge1a.UME.jpg
<File:Rosa> chinensis petals.jpg <File:Rosa> chinensis 04-08-2012 02.jpg

## 城市象征

月季现为[北京市市花](../Page/北京市.md "wikilink")\[11\]、[天津市市花](../Page/天津市.md "wikilink")\[12\]、[石家庄市市花](../Page/石家庄市.md "wikilink")\[13\]、[南昌市市花](../Page/南昌市.md "wikilink")\[14\]、[南阳市市花](../Page/南阳市.md "wikilink")\[15\]、[葫芦岛市市花](../Page/葫芦岛市.md "wikilink")\[16\]、[常州市市花](../Page/常州市.md "wikilink")\[17\]、[淮安市市花](../Page/淮安市.md "wikilink")\[18\]、[衡阳市市花](../Page/衡阳市.md "wikilink")\[19\]等中国城市的市花，[伊拉克](../Page/伊拉克.md "wikilink")、[美國](../Page/美國.md "wikilink")、[盧森堡國花](../Page/盧森堡.md "wikilink")\[20\]。

## 参考文献

## 外部链接

  - \[<http://www.cvh.org.cn/lsid/index.php?lsid=urn:lsid:cvh.org.cn:names:cnpc_38284&vtype=img,spm,ref,link>,
    月季 中国数字植物标本馆\]
  - [月季
    Yueji](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D00092)
    藥用植物圖像數據庫 (香港浸會大學中醫藥學院)
  - [月季花
    Yuejihua](http://libproject.hkbu.edu.hk/was40/detail?channelid=47953&searchword=pid=B00318)
    中藥材圖像數據庫 (香港浸會大學中醫藥學院)
  - [月季花 Yue Ji
    Hua](http://libproject.hkbu.edu.hk/was40/outline?page=1&channelid=44273&searchword=%E6%9C%88%E5%AD%A3%E8%8A%B1&sortfield=+name_chi_sort)
    中藥標本數據庫 (香港浸會大學中醫藥學院)

{{-}}

[Category:花卉](../Category/花卉.md "wikilink")
[Category:蔷薇属](../Category/蔷薇属.md "wikilink")

1.  [ARS](http://www.ishs.org/sci/icralist/14.htm)ISHS ICRA
2.  [History of Roses Part One
    A](http://www.ars.org/wp-content/uploads/2012/01/History-of-Roses-Species-Part-A.pdf)
     ARS
3.  [薔薇屬](http://frps.eflora.cn/frps/Rosa)中國植物誌
4.  [藥用](http://frps.eflora.cn/jingji/2?page=189)中國植物誌 經濟用途
5.  [藥用](http://frps.eflora.cn/jingji/2?page=190)中國植物誌 經濟用途
6.  [油脂(精油\\树胶\\鞣质)](http://frps.eflora.cn/jingji/8?page=53)中國植物誌 經濟用途
7.  [油脂(精油\\树胶\\鞣质)](http://frps.eflora.cn/jingji/8?page=54)中國植物誌 經濟用途
8.  [月季](http://db.yaozh.com/yaocai_bz/19700101/3110.html)中國藥典
9.  [既是食品又是药品的物品名单](http://www.sda.gov.cn/WS01/CL1159/)国家食品药品监督管理总局
10. [可用于保健食品的物品名单](http://www.sda.gov.cn/WS01/CL1160/)国家食品药品监督管理总局
11. 首都之窗
12. [1](http://www.tianjin.gov.cn/NewsInfo.asp?Id=81613)天津之窗
13.
14.
15.
16. <http://www.hldnews.com/hldyw/2014/06/30/18108.html>
17.
18.
19.
20.