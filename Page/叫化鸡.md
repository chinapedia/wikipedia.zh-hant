**叫化鸡**，也称“叫化子鸡”、“黄泥煨鸡”、“乞兒雞”、“富貴雞”等，是[苏菜的一道名菜](../Page/苏菜.md "wikilink")，流行于[江苏](../Page/江苏.md "wikilink")、[浙江](../Page/浙江.md "wikilink")。

## 起源

相传[明末](../Page/明.md "wikilink")[清初](../Page/清.md "wikilink")，在[江苏](../Page/江苏.md "wikilink")[常熟](../Page/常熟.md "wikilink")[虞山脚下一](../Page/虞山_\(常熟\).md "wikilink")[乞丐偶得一](../Page/乞丐.md "wikilink")[鸡](../Page/鸡.md "wikilink")，但又苦于没有炊具和调料，连煺毛的开水也无法找到。无奈乞丐突发奇想，将鸡破肚带毛涂泥放入柴火堆中煨烤。待泥土乾硬後拍碎，鸡毛随泥巴一起脱落，鸡香味扑鼻。后来这一做法为他人仿效，成为常熟名点，并因其创始人而得名叫化鸡。

。在1972年版的《中国名菜谱·浙江》和1990年版的《中国名菜谱·浙江风味》中，均有“叫化童鸡”。

## 作法

现代叫化鸡的做法是选头小体大、肥而嫩的[三黄鸡](../Page/三黄鸡.md "wikilink")，将鸡膛内填加了十几种鲜料，以[猪网油](../Page/猪网油.md "wikilink")、鲜荷叶包之再泥烤，味道更为鲜美，且有荷香。现在也有以面团代替泥土进行制作的，且谓之“富贵鸡”\[1\]。

## 流行文化

[中國](../Page/中國.md "wikilink")[武侠小說作家](../Page/武侠小說.md "wikilink")[金庸的](../Page/金庸.md "wikilink")[小說](../Page/小說.md "wikilink")《[射鵰英雄傳](../Page/射鵰英雄傳.md "wikilink")》裡，黃蓉曾經烹調這味菜色給師父九指神丐洪七公吃，令他滿心歡喜。

[Category:苏菜](../Category/苏菜.md "wikilink")
[Category:雞肉料理](../Category/雞肉料理.md "wikilink")

1.  [海味入馔年菜添贵气](http://www.zaobao.com/special/report/singapore/chinese-new-year-2017/food-entertainment/story20170115-713763)，联合早报