**向阳区**是[黑龙江省](../Page/黑龙江省.md "wikilink")[鹤岗市下辖的一个区](../Page/鹤岗.md "wikilink")。位于市区北中部。是市政府、矿务局机关的驻地。向阳区，原名“老街基”。

## 行政区划

下辖5个街道：\[1\] 。

## 参考资料

## 外部链接

  - [鹤岗向阳区政府网](http://www.hgxyq.gov.cn/)

[\*](../Category/向阳区_\(鹤岗市\).md "wikilink")
[Category:鹤岗市辖区](../Category/鹤岗市辖区.md "wikilink")

1.