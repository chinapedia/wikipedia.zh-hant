**馬林郡**（**Marin
County**）是[美國](../Page/美國.md "wikilink")[加利福尼亞州的一個郡](../Page/加利福尼亞州.md "wikilink")，郡治[聖拉斐爾](../Page/聖拉斐爾\(加利福尼亞州\).md "wikilink")。根據[美國人口調查局](../Page/美國人口調查局.md "wikilink")2000年統計，共有人口247,289，其中[白人佔](../Page/白人.md "wikilink")84.03%、[亞裔美國人佔](../Page/亞裔美國人.md "wikilink")4.53%、[非裔美國人佔](../Page/非裔美國人.md "wikilink")2.89%。

馬林郡透過[金門大橋與](../Page/金門大橋.md "wikilink")[舊金山市區接連](../Page/舊金山.md "wikilink")。

## 旅遊景點

馬林郡境內的Samuel P. Taylor State
Park是北加州著名森林公園之一，每年吸引眾多遊客前往露營與遠足。森林公園內的Pioneer
Tree據說已經有數百年歷史。

馬林郡西部的 Point Reyes National Seashore是美國西海岸著名的一處海濱景點，以大霧與優良的生態系統著稱。
[Wpdms_usgs_photo_point_reyes_national_seashore.jpg](https://zh.wikipedia.org/wiki/File:Wpdms_usgs_photo_point_reyes_national_seashore.jpg "fig:Wpdms_usgs_photo_point_reyes_national_seashore.jpg")
[Point_reyes_lighthouse_02.jpg](https://zh.wikipedia.org/wiki/File:Point_reyes_lighthouse_02.jpg "fig:Point_reyes_lighthouse_02.jpg")

[M](../Category/加利福尼亞州行政區劃.md "wikilink")
[Category:舊金山灣區](../Category/舊金山灣區.md "wikilink")
[Category:旧金山湾区行政区划](../Category/旧金山湾区行政区划.md "wikilink")