<u style="display: none;">... no changes ... no changes ... no changes
... no changes ... no changes ... no changes ... no changes ... no
changes ... no changes ... no changes ... no changes ... </u>

<table>
<thead>
<tr class="header">
<th style="text-align: center;"><p><strong>菌藻植物门</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><strong><a href="../Page/科学分类.md" title="wikilink">科学分类</a></strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><table>
<tbody>
<tr class="odd">
<td><p><a href="../Page/域_(生物).md" title="wikilink">域</a>：</p></td>
<td><p><a href="../Page/真核生物域.md" title="wikilink">真核生物域</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/门_(生物).md" title="wikilink">门</a>：</p></td>
<td><p><strong>菌藻门（Mycophycophyta）</strong></p></td>
</tr>
</tbody>
</table></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong><a href="../Page/纲.md" title="wikilink">纲</a></strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><ul>
<li><a href="../Page/子囊衣纲.md" title="wikilink">子囊衣纲</a>（Ascolichens）</li>
<li><a href="../Page/担子衣纲.md" title="wikilink">担子衣纲</a>（Basidiolichens］</li>
<li><a href="../Page/半知衣纲.md" title="wikilink">半知衣纲</a>（Deuterolichens）</li>
</ul></td>
</tr>
</tbody>
</table>

[Lichen.jpg](https://zh.wikipedia.org/wiki/File:Lichen.jpg "fig:Lichen.jpg")
[Lichen_squamulose.jpg](https://zh.wikipedia.org/wiki/File:Lichen_squamulose.jpg "fig:Lichen_squamulose.jpg")

**地衣**是[真菌和](../Page/真菌.md "wikilink")[绿藻](../Page/绿藻.md "wikilink")（或[蓝细菌](../Page/蓝细菌.md "wikilink")）的[共生体](../Page/共生.md "wikilink")，呈灰白、暗绿、淡黄、鲜红等多种颜色，长在干燥的岩石或树皮上。[光合自养的绿藻](../Page/光合自养.md "wikilink")（或蓝细菌）提供营养物质，真菌（通常属于[子囊菌門](../Page/子囊菌門.md "wikilink")，少数属于[擔子菌門](../Page/擔子菌門.md "wikilink")）提供水和矿物质、提供保护，防止水分过度蒸发。地衣可以在严峻的环境条件下生长，常是生物占领新陆地的先锋。在极度干燥的条件下，地衣也可以脱去水分，进入[休眠状态](../Page/休眠.md "wikilink")，待到条件好转，再恢复高速度增长。地衣可以长寿，有的寿命可达千年\[1\]。

地衣通过包含真菌和绿藻（或蓝细菌）二者的地衣碎片在空气中传播，长出新地衣的方式进行[无性生殖](../Page/无性生殖.md "wikilink")。他们各自也可以单独进行有性和无性的生殖。其中真菌部分单独生殖的后代，需要与有关绿藻（或蓝细菌）重新组合才可生存。

地衣植物約有1.8萬餘種，地球陸地面積的6％被地衣覆蓋。北极地区的地衣是北极[驯鹿的主要食物](../Page/驯鹿.md "wikilink")，地衣也已用於製造[染料](../Page/染料.md "wikilink")（如[石蕊](../Page/石蕊.md "wikilink")）和[香料](../Page/香料.md "wikilink")，以及在[傳統醫學作为藥物](../Page/傳統醫學.md "wikilink")。\[2\]

## 詞源

實際上，地衣（Lichen）中生長于地上者并不多，然而何為稱之地衣？最近[日本學者](../Page/日本.md "wikilink")[久保輝幸考察地衣的詞源](../Page/久保輝幸.md "wikilink")\[3\]
\[4\]：據此，地衣一詞首見于早期[道教書籍](../Page/道教.md "wikilink")。但是，所指植物為現今所見的[車前](../Page/車前.md "wikilink")。其后，唐人[陳藏器在本草書中首次記載](../Page/陳藏器.md "wikilink")[地衣草](../Page/地衣草.md "wikilink")，這個地衣草應是現在的地衣（Lichen）的詞源。[李時珍將之與](../Page/李時珍.md "wikilink")《日華子諸家本草》中所記載的地衣視為同一種植物并記載至《[本草綱目](../Page/本草綱目.md "wikilink")》中。而後至[清朝](../Page/清朝.md "wikilink")，[李善蘭同兩位傳教士編輯](../Page/李善蘭.md "wikilink")《植物學》\[5\]時，對「lichen」的詞譯采取本草書中的植物名稱「地衣」。。

## 形态和结构

地衣生活在各種表面上：[土壤](../Page/土壤.md "wikilink")、[樹木](../Page/樹木.md "wikilink")、[岩石和牆上](../Page/岩石.md "wikilink")。他們通常會在一些環境惡劣的地方生長，如海拔數千米的高山、[沙漠和接近](../Page/沙漠.md "wikilink")[極地的](../Page/極地.md "wikilink")[凍土](../Page/凍土.md "wikilink")。由於地衣對[二氧化硫相當的敏感](../Page/二氧化硫.md "wikilink")，所以[生態學上](../Page/生態學.md "wikilink")，地衣常被當成一種空氣品質的指標。

形態上地衣有三種：

1.  ：植物體成殼狀，緊貼在樹皮和石頭上。底面和基質緊密相連，難以分離。例如，（Lecanora）、（Graphis）。

2.  ：植物體平鋪，僅由假根狀的菌絲與附著的和體相邊，易於分開。例如，（Parmelia）、蜈蚣衣屬（Physica）。

3.  ：植物直立或下垂如絲，多數片段有分枝。例如，[石蕊属](../Page/石蕊.md "wikilink")（Cladonia）、[松萝属](../Page/松萝.md "wikilink")（Usnea）。

而地衣結構一般可分為上皮層、藻胞層、髓層和下皮層。

1.  上下皮層：由菌絲緊密交織而成，特稱為假皮層，下皮層一般能長出假根。
2.  藻胞層：上皮層的下部，在排列疏鬆的藻胞層之間夾雜有許多藻細胞，這些[細胞成層排列稱異層細胞](../Page/細胞.md "wikilink")，若散生則稱為同層細胞。
3.  髓：藻細胞下面為髓層，由比較疏鬆而粗大的菌絲體交織而成，專門貯存[空氣](../Page/空氣.md "wikilink")、[水分](../Page/水分.md "wikilink")。

## 地衣的同名異物

  - 一種類似于的[珠藻科](../Page/珠藻科.md "wikilink")[蓝藻](../Page/蓝藻.md "wikilink")，又稱作[地耳或](../Page/地耳.md "wikilink")[地木耳](../Page/地木耳.md "wikilink")，亦于一些大陸地區中稱作「地衣」，以之作為食品。此種地衣不同于一般所稱的地衣（Lichen）。
  - 現今所見的[車前草](../Page/車前草.md "wikilink")\[6\]。
  - 一種[唐朝時舖於地上的](../Page/唐朝.md "wikilink")[織品](../Page/織品.md "wikilink")（敷物）\[7\]。

## 圖庫

<center>

<File:Lichen>
squamulose.jpg|[玄武岩上一個葉狀地衣](../Page/玄武岩.md "wikilink")*[Xanthoparmelia](../Page/Xanthoparmelia.md "wikilink")*，比照*lavicola*。
<File:Usnea> australis.jpg|*Usnea australis*，一種以fruticose形式生長在樹枝上的地衣。
<File:Rhizocarpon>
geographicum01.jpg|在岩石上的[地圖地衣](../Page/地圖地衣.md "wikilink")（*Rhizocarpon
geographicum*）。 <File:Hyella> caespitosa hypae.jpg|地衣*Pyrenocollema
halodytes*，其中含有[真菌菌絲的](../Page/真菌.md "wikilink")[藍藻](../Page/藍藻.md "wikilink")*Hyella
caespitosa*。 <File:LogLichen.jpg>|*Physcia
millegrana*（一種葉狀[苔蘚](../Page/苔蘚.md "wikilink")），帶有一種無形的多孔真菌（右下），落在腐爛的木頭上。
<File:caribou> moss.jpg|[馴鹿苔](../Page/馴鹿苔.md "wikilink")（*Cladonia
rangiferina*）。
<File:Kananakislichen.jpg>|*Hypogymnia*，參見*tubulosa*與*Bryoria*
和*Tuckermannopsis*，在[加拿大](../Page/加拿大.md "wikilink")[洛基山脈](../Page/洛基山脈.md "wikilink")。
<File:Lichen> on the riverside.jpg|*Lecanora*，參見蒂米甚瓦拉貝加運河岸邊的*muralis*地衣。
<File:Caloplaca> marina.JPG|*[Caloplaca
marina](../Page/Caloplaca_marina.md "wikilink")*，海洋地衣。 \[8\]

</center>

## 参考文献

## 参见

  -
[地衣](../Category/地衣.md "wikilink")
[Category:指示生物](../Category/指示生物.md "wikilink")
[Category:隐花植物](../Category/隐花植物.md "wikilink")
[Category:多嗜极生物](../Category/多嗜极生物.md "wikilink")
[Category:贫营养生物](../Category/贫营养生物.md "wikilink")
[Category:指示种](../Category/指示种.md "wikilink")
[Category:真菌學](../Category/真菌學.md "wikilink")
[Category:共生](../Category/共生.md "wikilink")

1.  10.1016/j.cub.2009.04.034

2.

3.  [科學史研究第48巻](http://ci.nii.ac.jp/Detail/detail.do?LOCALID=ART0009116428)


4.  [Lichen如何翻成地衣（日文）](http://mayanagi.hum.ibaraki.ac.jp/students/kubo/chap1.html)


5.  [韋廉臣輯訳；李善蘭筆述，《植物學》](http://catalogue.nla.gov.au/Record/854803/Details?lookfor=author:%22Williamson,+Alexander,+1829-1890%22&max=7&offset=7)，這是復刊影本出版，於合輯的後半部，此連結為澳洲國家圖書館的連結。正文中所述的二位傳教士在書中似乎沒有提到，而是韋廉臣（Alexander
    Williamson, 1829-1890）翻譯的。但在註解一的網路文件中，則詳提了第三位作者艾約瑟（J. Edkins,
    1823-1905）。

6.
7.
8.  This was scraped from a dry, concrete-paved section of a drainage
    ditch.  This entire image covers a square that is approximately 1.7
    millimeters on a side.  The numbered ticks on the scale represent
    distances of 230 micrometers, or slightly less than ¼ of a
    millimeter.