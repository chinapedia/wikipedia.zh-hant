**美國國家航空暨太空法案**（）是創制出[美國國家航空暨太空總署](../Page/美國國家航空暨太空總署.md "wikilink")（NASA）的[美國聯邦法案](../Page/美國法律.md "wikilink")。在[蘇聯發射](../Page/蘇聯.md "wikilink")[人造衛星後](../Page/人造衛星.md "wikilink")，[美國眾議院航空暨太空探索委員會](../Page/美國眾議院.md "wikilink")﻿就緊跟著草擬了法案，並於1958年7月29日由[美國總統](../Page/美國總統.md "wikilink")[艾森豪簽署通過](../Page/艾森豪.md "wikilink")。在這法案之前，因為蘇聯發射第一顆衛星的模式，太空探索被視為一種[軍事行為](../Page/軍事.md "wikilink")。這法案的制定很大程度上是因為軍事設施似乎已經無法跟上[太空競賽了](../Page/太空競賽.md "wikilink")。

除了創建NASA，還將先前建立的[國家航空諮詢委員會](../Page/國家航空諮詢委員會.md "wikilink")（NACA）轉型成一個可以直接向總統報告的獨立機構。這法案也建立了[民間軍事諮詢委員會](../Page/民間軍事諮詢委員會.md "wikilink")（Civilian-Military
Advisory
Panel），在[加州的](../Page/加州.md "wikilink")[范登堡空軍基地負責提供諮詢級協調太空的軍事用途](../Page/范登堡空軍基地.md "wikilink")。因為先前許多發射軍事和監探設施的介入和為預先計畫反制蘇聯的核武器進入太空所制定的[部分禁止核試驗條約](../Page/部分禁止核試驗條約.md "wikilink")，現在美國已經將軍事和民間太空計畫分開了。

此外，新法案也對專利法進行了廣泛的修改，並且將民間承包商的發明和它所帶來的太空旅行的權利將歸于政府所有。法案讓政府壟斷了太空運輸，有效遏止了太空運輸的私有發展。但是這情況之後被1984年制定的[商業太空發射法案推翻了](../Page/商業太空發射法案.md "wikilink")，它讓民間能夠藉由NASA發射太空載具來發展太空運輸。

## 外部連結

  - [美國國家航空暨太空法案內文](http://www.nasa.gov/offices/ogc/about/space_act1.html)來自NASA.gov
  - [在制定之前美國國會委員會的證詞概要](http://www.worldcatlibraries.org/wcpa/ow/19f90a4ad301971ca19afeb4da09e526.html)
  - [艾森豪總統的備忘錄，關於國防部建立美國航天局和軍事的角色](https://web.archive.org/web/20060527000711/http://www.eisenhowermemorial.org/presidential-papers/second-term/documents/780.cfm)
  - [美國太空政策的歷史](http://www.nap.edu/html/streamlining_range/ch2.html)

## 參考資料

[Category:1958年法律](../Category/1958年法律.md "wikilink")
[Category:美国国家航空航天局](../Category/美国国家航空航天局.md "wikilink")
[Category:美国联邦法案](../Category/美国联邦法案.md "wikilink")