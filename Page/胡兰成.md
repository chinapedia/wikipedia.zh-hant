**胡蘭成**（），原名胡積蕊，小名蕊生，[浙江](../Page/浙江.md "wikilink")[绍兴人](../Page/绍兴.md "wikilink")，為[中國近代](../Page/中國.md "wikilink")[作家](../Page/作家.md "wikilink")。為[張愛玲第一任丈夫](../Page/張愛玲.md "wikilink")。曾任職[汪精衛政權](../Page/汪精衛政權.md "wikilink")。

## 生平

胡蘭成出生於[浙江](../Page/浙江.md "wikilink")[绍兴](../Page/绍兴.md "wikilink")[嵊縣三界鎮胡村](../Page/嵊縣.md "wikilink")\[1\]，在杭州[惠蘭中學讀書](../Page/惠蘭中學.md "wikilink")，青年時於[燕京大學旁聽課程](../Page/燕京大學.md "wikilink")，善写作，追随[汪精卫](../Page/汪精卫.md "wikilink")，[對日抗戰時期任](../Page/對日抗戰.md "wikilink")[汪精衛政權宣传部次长](../Page/汪精衛政權.md "wikilink")、行政院法制局长，被列为著名[漢奸](../Page/漢奸.md "wikilink")。

1943年，胡與上海女作家[張愛玲相戀](../Page/張愛玲.md "wikilink")，第一次見面時，胡說“你的身材這樣高，這怎麼可以？”

1944年与张爱玲结婚，1947年离婚。

上海[极司非尔路](../Page/极司非尔路.md "wikilink")[76号特工总部警卫队长](../Page/国民党中央执行委员会特务委员会特工总部.md "wikilink")[吴四宝被暗杀后](../Page/吴四宝.md "wikilink")，胡蘭成与其遗孀[佘爱珍同居](../Page/佘爱珍.md "wikilink")。他晚年出版的最後一本書《今生今世》詳細的描寫了他在愛情與政治上搖擺的個性，人们戲稱《今生今世》是一部《[群芳譜](../Page/群芳譜.md "wikilink")》。

1944年，對日戰爭晚期，在日本駐南京外交官[池田篤紀的支持下](../Page/池田篤紀.md "wikilink")，胡蘭成至漢口辦《大楚報》，任社長。他負責連絡[中共與日軍](../Page/中共.md "wikilink")，希望共同對抗中華民國國民革命軍。

1945年，日本投降，胡蘭成策劃兵變，希望結合激進日軍與殘餘親日勢力，成立獨立政府，對抗[中華民國](../Page/中華民國.md "wikilink")。在蔣介石接到緊急電報後，迅速與日軍總指揮官[岡村寧次與日本外相](../Page/岡村寧次.md "wikilink")[谷正之連絡](../Page/谷正之.md "wikilink")，在兵變未成形前就加以撲滅\[2\]。胡蘭成在浙江一带匿名逃亡，後潛藏於溫州，開始写《山河岁月》。

1950年，胡蘭成、鄒平凡與其他二名友人，由上海搭火車至廣州，在香港認識唐君毅，同年，在[佘愛珍與](../Page/佘愛珍.md "wikilink")[熊劍東之妻的資助下](../Page/熊劍東.md "wikilink")，搭輪船，偷渡到[日本橫濱](../Page/日本.md "wikilink")，投奔日本前駐華大使館的官員[清水董三和](../Page/清水董三.md "wikilink")[池田篤紀](../Page/池田篤紀.md "wikilink")。

1951年，移居東京，之後长期定居日本。

1954年3月与[佘爱珍结婚](../Page/佘爱珍.md "wikilink")\[3\]。開始學習[日語](../Page/日語.md "wikilink")，結識數學家[岡潔和](../Page/岡潔.md "wikilink")[諾貝爾物理學獎得主](../Page/諾貝爾物理學獎.md "wikilink")[湯川秀樹](../Page/湯川秀樹.md "wikilink")。

1972年10月，[中國文化學院](../Page/中國文化學院.md "wikilink")（今[中國文化大學](../Page/中國文化大學.md "wikilink")）邀請胡蘭成至台灣授課。1974年，[蔣介石批准申請](../Page/蔣介石.md "wikilink")，胡蘭成來到台灣教書。其文學才能影響了部份臺灣文人，尤其是[朱西甯](../Page/朱西甯.md "wikilink")、[朱天文](../Page/朱天文.md "wikilink")、[朱天心父女](../Page/朱天心.md "wikilink")，受其影響頗深。在台灣文學界對他的評價也是兩極化，有人相當欣賞他的散文風格，有人則對他有相當嚴厲的評論。在新儒家學者中，他與[唐君毅交好](../Page/唐君毅.md "wikilink")，但[牟宗三](../Page/牟宗三.md "wikilink")、[徐復觀等人則極不欣賞他](../Page/徐復觀.md "wikilink")。

1975年，蔣介石過世，[蔣經國成為國民黨黨主席](../Page/蔣經國.md "wikilink")。胡蘭成曾上書蔣經國，希望進行政治改革。同年，[趙滋蕃在](../Page/趙滋蕃.md "wikilink")《中央日報》專欄中，攻擊胡蘭成曾在汪精衛政權處工作，指責他是漢奸。[余光中與](../Page/余光中.md "wikilink")[胡秋原繼而在報章上發表文章攻擊](../Page/胡秋原.md "wikilink")，[警備總部查禁了胡蘭成的書籍](../Page/警備總部.md "wikilink")《山河歲月》。胡蘭成被文化大學解職，在朱西甯家開設私人[易經課程](../Page/易經.md "wikilink")。包括作家[郑愁予](../Page/郑愁予.md "wikilink")、[蔣勳](../Page/蔣勳.md "wikilink")、[张晓风](../Page/张晓风.md "wikilink")、[管管](../Page/管管.md "wikilink")、[袁琼琼](../Page/袁琼琼.md "wikilink")、[曹又方](../Page/曹又方.md "wikilink")、[苦苓](../Page/苦苓.md "wikilink")、[渡也](../Page/陳啟佑.md "wikilink")、[向阳](../Page/向阳.md "wikilink")、[杨泽](../Page/杨泽.md "wikilink")、[蒋晓云](../Page/蒋晓云.md "wikilink")、[履疆等都聽過他講課](../Page/蘇進強.md "wikilink")\[4\]。

1976年，胡蘭成離開台灣，再度客居日本。其妻佘愛珍與女兒在東京開[酒吧](../Page/酒吧.md "wikilink")，成為胡蘭成主要經濟來源。

1979年他的書《禪是一枝花》在台灣由三三出版社出版，书中「郭涣」指朱西甯，「堂妹」指朱天文，將朱天文比喻张爱玲，作者在序言：「小孩儿有时候说谎话，是为了想说更真的话。」

1981年7月25日，因[心臟衰竭](../Page/心臟衰竭.md "wikilink")，死於[東京都](../Page/東京都.md "wikilink")，享年75歲。身後葬於東京都[福生市的墓園](../Page/福生市.md "wikilink")，其子胡紀元（胡蘭成與全慧文所生的最小兒子）於2016年4月4日從南京市搭機飛抵東京為其父[掃墓](../Page/掃墓.md "wikilink")，父子於1950年之後除書信往返外未曾再相見\[5\]。

## 主要著作

  - 《今生今世》
  - 《山河岁月》
  - 《禅是一枝花》
  - 《中国文学史话》
  - 《革命要诗与学问》
  - 《建国新书》

## 家庭

胡兰成有三子三女，长子胡启为[唐玉凤所生](../Page/唐玉凤.md "wikilink")，幼女胡晋明是在日本时所收养，其余两子两女为[全慧文所生](../Page/全慧文.md "wikilink")。

## 相關作品

  - 台灣[三毛名作](../Page/三毛_\(作家\).md "wikilink")《[滾滾紅塵](../Page/滾滾紅塵.md "wikilink")》[電影](../Page/電影.md "wikilink")[劇本](../Page/劇本.md "wikilink")，劇情參考張愛玲與胡蘭成經歷改編，男主角即是影射胡蘭成。
  - 「胡蘭成傳」張桂華著
  - 有指出張愛玲著作《[色戒](../Page/色戒.md "wikilink")》中的易先生影射胡蘭成
  - 張愛玲著作《[小团圆](../Page/小团圆.md "wikilink")》中的邵之雍影射胡蘭成

## 参考文献

<div class="references-small">

<references />

</div>

<山河岁月>

## 外部链接

  - [胡宁生：有关父亲胡兰成](http://cul.sohu.com/20090409/n263300309.shtml)
  - [胡兰成六个子女今何在？长子文革时自杀身亡(图)](http://book.ifeng.com/culture/3/detail_2010_04/19/532745_0.shtml)

[L兰](../Page/category:胡姓.md "wikilink")

[Category:浙江作家](../Category/浙江作家.md "wikilink")
[Category:中華民國散文家](../Category/中華民國散文家.md "wikilink")
[Category:汪精衛政權人物](../Category/汪精衛政權人物.md "wikilink")
[Category:中國第二次世界大戰人物](../Category/中國第二次世界大戰人物.md "wikilink")
[Category:日本華人](../Category/日本華人.md "wikilink")
[Category:嵊州人](../Category/嵊州人.md "wikilink")
[Category:中國文化大學教授](../Category/中國文化大學教授.md "wikilink")
[Category:台灣戰後浙江移民](../Category/台灣戰後浙江移民.md "wikilink")
[Category:中国国民党第六届中央执行委员会委员
(汪精卫政权)](../Category/中国国民党第六届中央执行委员会委员_\(汪精卫政权\).md "wikilink")

1.  橋墩村
2.
3.  胡兰成：《今生今世》
4.
5.