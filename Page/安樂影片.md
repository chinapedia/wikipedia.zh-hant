'''安樂影片有限公司
'''（），[香港](../Page/香港.md "wikilink")[電影公司](../Page/電影公司.md "wikilink")，主要業務為包括電影製作、電影和影碟發行、廣告製作，同時經營有12間戲院的[百老匯院線](../Page/百老匯院線.md "wikilink")，由現時公司執行董事之一[江志強的父親江祖貽於](../Page/江志強.md "wikilink")1950年成立，為全港歷史最悠久的電影發行公司，過去已發行逾100套電影。目前安樂影片員工有超過200人，年度營業額介乎10,000,000－49,999,999[港元之間](../Page/港元.md "wikilink")。

安樂影片現已與[索尼影業簽訂了](../Page/索尼影業.md "wikilink")[香港戲院發行協定](../Page/香港.md "wikilink")，負責把它們的電影發行至全香港各大戲院\[1\]。另一方面，安樂影片亦曾經與[二十世紀福斯](../Page/二十世紀福斯.md "wikilink")﹙由1990年代初開始直至2001年9月，2001年9月起直接由二十世紀福斯﹙香港﹚分部負責﹚、[希杰娛樂](../Page/希杰娛樂.md "wikilink")—[夢工場](../Page/夢工場.md "wikilink")﹙於1998年期間合作至1998年末，1998年末起由希杰娛樂自行發行直至2004年10月；2004年10月起由[嘉禾旗下的泛亞影業及其合作伙伴](../Page/嘉禾.md "wikilink")
—
[聯合國際影業與希杰娛樂聯合擁有夢工場的發行權直至](../Page/聯合國際影業.md "wikilink")2006年7月，2006年7月起由[洲立影片發行](../Page/洲立影片.md "wikilink")﹚及[環球影業](../Page/環球影業.md "wikilink")﹙於2007年至2018年12月期間擁有其發行權，2018年12月起直接由環球發行﹙香港﹚分部自行負責﹚簽訂了香港戲院發行協定，同時亦有自行買片以作發行。江志強近年來亦參與不少華語片的投資和製作，例如《[臥虎藏龍](../Page/臥虎藏龍_\(電影\).md "wikilink")》、《[英雄](../Page/英雄_\(電影\).md "wikilink")》、《[色，戒](../Page/色，戒.md "wikilink")》和《[寒戰](../Page/寒戰.md "wikilink")》等。

2012年3月15日，[now
TV與](../Page/now寬頻電視.md "wikilink")[華誼兄弟](../Page/華誼兄弟.md "wikilink")、安樂影片及[陳可辛](../Page/陳可辛.md "wikilink")[我們製作共同籌組的](../Page/我們製作.md "wikilink")[now爆谷台於](../Page/now爆谷台.md "wikilink")[香港啟播](../Page/香港.md "wikilink")。\[2\]

## 部分發行電影

  - 《[臥虎藏龍](../Page/臥虎藏龍_\(電影\).md "wikilink")》
  - 《[蜘蛛人](../Page/蜘蛛人_\(電影\).md "wikilink")》
  - 《[英雄](../Page/英雄_\(電影\).md "wikilink")》
  - 《[蜘蛛人2](../Page/蜘蛛人2.md "wikilink")》
  - 《[蜘蛛人3](../Page/蜘蛛人3.md "wikilink")》
  - 《[色，戒](../Page/色，戒.md "wikilink")》
  - 《[贖罪](../Page/贖罪.md "wikilink")》
  - 《[斷背山](../Page/斷背山.md "wikilink")》
  - 《[魔鬼的情詩](../Page/魔鬼的情詩.md "wikilink")》
  - 《[愛到病](../Page/愛到病.md "wikilink")》
  - 《[神經大帝](../Page/神經大帝.md "wikilink")》（*The Madness of King George*）
  - 《[鎖住的天空](../Page/鎖住的天空.md "wikilink")》（*Nothing Personal*）
  - 《[孖你有得發](../Page/孖你有得發.md "wikilink")》（*Waking Ned*）
  - 《[中央車站](../Page/中央車站_\(電影\).md "wikilink")》
  - 《[妳想丈夫](../Page/妳想丈夫.md "wikilink")》（*An Ideal Husband*）
  - 《[心慌方](../Page/心慌方.md "wikilink")》
  - 《[涉谷二十四小時](../Page/涉谷二十四小時.md "wikilink")》
  - 《[壞孩子的天空](../Page/壞孩子的天空.md "wikilink")》
  - 《[一人有一點顏色](../Page/一人有一點顏色.md "wikilink")》
  - 《[我的爸 是你嗎](../Page/我的爸_是你嗎.md "wikilink")》
  - 《[小企鵝大長征](../Page/小企鵝大長征.md "wikilink")》
  - 《[論盡我阿媽](../Page/論盡我阿媽.md "wikilink")》
  - 《[對她有話兒](../Page/對她有話兒.md "wikilink")》
  - 《[聖·教·慾](../Page/壞教慾.md "wikilink")》
  - 《[恐怖死人快線](../Page/恐怖死人快線.md "wikilink")》
  - 《[浮花](../Page/玩美女人.md "wikilink")》
  - 《[三條人](../Page/三條人.md "wikilink")》
  - 《[擊浪青春](../Page/擊浪青春.md "wikilink")》
  - 《[我在伊朗長大](../Page/我在伊朗長大.md "wikilink")》
  - 《[情尋貓腳印](../Page/情尋貓腳印.md "wikilink")》
  - 《[仲夏夜之夢](../Page/仲夏夜之夢.md "wikilink")》
  - 《[戀愛夢遊中](../Page/戀愛夢遊中.md "wikilink")》
  - 《[血光光五人幫](../Page/血光光五人幫.md "wikilink")》
  - 《[爆肚風雲](../Page/爆肚風雲.md "wikilink")》
  - 《[妙在大門後](../Page/妙在大門後.md "wikilink")》
  - 《[藍色大門](../Page/藍色大門.md "wikilink")》
  - 《[神婆美少女](../Page/神婆美少女.md "wikilink")》
  - 《[紅提琴](../Page/紅提琴.md "wikilink")》
  - 《[聲光伴我飛](../Page/聲光伴我飛.md "wikilink")》
  - 《[盜寶偷心三人行](../Page/盜寶偷心三人行.md "wikilink")》
  - 《[盛夏光年](../Page/盛夏光年.md "wikilink")》
  - 《[龍貓](../Page/龍貓.md "wikilink")》
  - 《[天空之城](../Page/天空之城.md "wikilink")》
  - 《[風之谷](../Page/風之谷.md "wikilink")》
  - 《[夢幻街少女](../Page/夢幻街少女.md "wikilink")》
  - 《[魔女宅急便](../Page/魔女宅急便.md "wikilink")》
  - 《[幽靈公主](../Page/幽靈公主.md "wikilink")》
  - 《[我的野蠻女友](../Page/我的野蠻女友.md "wikilink")》
  - 《[小鞋子](../Page/小鞋子.md "wikilink")》
  - 《[小孩子走天涯](../Page/小孩子走天涯.md "wikilink")》
  - 《[隔牆故事](../Page/隔牆故事.md "wikilink")》
  - 《[三步上天堂](../Page/三步上天堂.md "wikilink")》
  - 《[求戀狂熱](../Page/求戀狂熱.md "wikilink")》
  - 《[事先張揚的身後事件](../Page/事先張揚的身後事件.md "wikilink")》
  - 《[桃色風雲搖擺狗](../Page/桃色風雲搖擺狗.md "wikilink")》
  - 《[最遙遠的距離](../Page/最遙遠的距離.md "wikilink")》
  - 《[天邊一朵雲](../Page/天邊一朵雲.md "wikilink")》
  - 《[喇叭書院](../Page/喇叭書院.md "wikilink")》
  - 《[談談情跳跳舞](../Page/談談情跳跳舞.md "wikilink")》
  - 《[兩性三人痕](../Page/兩性三人痕.md "wikilink")》（*Gazon maudit*/*French
    Twist*）
  - 《[毒太陽](../Page/毒太陽.md "wikilink")》
  - 《[點虫虫](../Page/點虫虫.md "wikilink")》
  - 《[沒有天空的都市](../Page/沒有天空的都市.md "wikilink")》
  - 《[愛與夢飛行](../Page/愛與夢飛行.md "wikilink")》
  - 《[等救火的日子](../Page/等救火的日子.md "wikilink")》
  - 《[童夢失魂夜](../Page/童夢失魂夜.md "wikilink")》
  - 《[華麗孽緣](../Page/華麗孽緣.md "wikilink")》
  - 《[十面埋伏](../Page/十面埋伏_\(电影\).md "wikilink")》
  - 《[聽說你愛我](../Page/聽說你愛我.md "wikilink")》
  - 《[滿城盡帶黃金甲](../Page/滿城盡帶黃金甲.md "wikilink")》
  - 《[不能說的秘密](../Page/不能說的秘密.md "wikilink")》
  - 《奏出我未來》
  - 《貓咪咕咕》
  - 《[血戰新世紀](../Page/血戰新世紀.md "wikilink")》
  - 《[一百萬零一夜](../Page/一百萬零一夜.md "wikilink")》
  - 《東京少女》
  - 《兩生花》
  - 《[霍元甲](../Page/霍元甲_\(2006年電影\).md "wikilink")》
  - 《[英雄](../Page/英雄_\(电影\).md "wikilink")》
  - 《[魔間迷宮](../Page/魔間迷宮.md "wikilink")》
  - 《[撞車](../Page/撞车_\(电影\).md "wikilink")》
  - 《[竊聽者](../Page/竊聽風暴.md "wikilink")》
  - 《[熱浪球愛戰](../Page/熱浪球愛戰.md "wikilink")》
  - 《[寒戰](../Page/寒戰.md "wikilink")》
  - 《[北京遇上西雅图](../Page/北京遇上西雅图.md "wikilink")》
  - 《[风暴](../Page/风暴_\(电影\).md "wikilink")》
  - 《[黄飞鸿之英雄有梦](../Page/黄飞鸿之英雄有梦.md "wikilink")》
  - 《[捉妖记](../Page/捉妖记_\(2015年电影\).md "wikilink")》
  - 《[华丽上班族](../Page/华丽上班族_\(电影\).md "wikilink")》
  - 《[归来](../Page/归来_\(电影\).md "wikilink")》
  - 《[特工爷爷](../Page/特工爷爷.md "wikilink")》
  - 《[寒战II](../Page/寒战II.md "wikilink")》
  - 《[北京遇上西雅图之不二情书](../Page/北京遇上西雅图之不二情书.md "wikilink")》
  - 《[长城](../Page/长城_\(电影\).md "wikilink")》
  - 《[屍殺列車](../Page/屍殺列車.md "wikilink")》
  - 《[與神同行](../Page/與神同行_\(電影\).md "wikilink")》
  - 《[與神同行：最終審判](../Page/與神同行：最終審判.md "wikilink")》

## 旗下藝人

<table>
<thead>
<tr class="header">
<th><p><strong>女藝人</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/湯唯.md" title="wikilink">湯唯</a></p></td>
</tr>
<tr class="even">
<td><p>'''男藝人 '''</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/吳秀波.md" title="wikilink">吳秀波</a><br />
（香港區經理人合約）</p></td>
</tr>
<tr class="even">
<td></td>
</tr>
</tbody>
</table>

## 旗下企業

  - [百老匯院線](../Page/百老匯院線.md "wikilink") PALACE院線 AMC院線——影院經營事務
  - 安樂影視有限公司——影碟發行事務
  - Suntech International Management
    Limited——[AMC戲院管理事務](../Page/AMC電影院.md "wikilink")
  - 雅德廣告公司——影片宣傳事務

## 參考文獻

<div class="references-small">

<references />

</div>

## 外部链接

  -
  - [安樂影片官方網站（百老匯院線）](http://www.cinema.com.hk)

  - [安樂影片有限公司](http://www.hktdc.com/sourcing/hk_company_directory.htm?locale=zh_TW&companyid=1X02SA33)，香港貿易發展局

  - [安樂影片有限公司](http://hkmdb.com/db/companies/view.mhtml?id=1526&display_set=big5)，香港影庫

  - [安樂](http://www.dianying.com/ft/company/Anle)，中文電影資料庫

  - [EDKO Film
    Ltd.](https://web.archive.org/web/20101218142456/http://hk.cnmdb.com/company/5130)，中國影視資料館

  - [EDKO Film](http://www.imdb.com/company/co0010281/)，imdb.com

  - [Edko Films](http://www.hkcinemagic.com/en/studio.asp?id=147)，Hong
    Kong Cinemagic

  - [Edko
    Films](https://web.archive.org/web/20080818163844/http://www.variety.com/profiles/Company/main/2044698/Edko%2BFilms.html)，Variety.com

[Category:香港電影公司](../Category/香港電影公司.md "wikilink")
[Category:1950年成立的公司](../Category/1950年成立的公司.md "wikilink")
[Category:香港電影發行公司](../Category/香港電影發行公司.md "wikilink")
[安乐电影](../Category/安乐电影.md "wikilink")

1.
2.