**霍集占**（[维吾尔语](../Page/维吾尔语.md "wikilink")：****
**；滿語：Hojijan；），[清代](../Page/清代.md "wikilink")[新疆](../Page/新疆.md "wikilink")[回部](../Page/回部.md "wikilink")[白山派首领](../Page/白山派.md "wikilink")，是[玛哈图木·阿杂木后裔](../Page/玛哈图木·阿杂木.md "wikilink")，[阿帕克和卓曾孙](../Page/阿帕克和卓.md "wikilink")，[波罗尼都之弟](../Page/波罗尼都.md "wikilink")，又称「小和卓」。因其自称“巴图尔汗”，被称为“汗和卓”。

霍集占年幼时与其父、兄被准噶尔汗[策妄阿拉布坦关押在](../Page/策妄阿拉布坦.md "wikilink")[伊犁](../Page/伊犁.md "wikilink")。[乾隆二十年](../Page/乾隆.md "wikilink")（1755年）五月，清朝派兵滅准噶尔汗国，释放了波羅尼都和霍集占，命波羅尼都前往[叶尔羌招抚旧部](../Page/叶尔羌.md "wikilink")，霍集占留在伊犁。不久，[阿睦尔撒纳在原准噶尔地区发动叛乱](../Page/阿睦尔撒纳.md "wikilink")，霍集占乘机逃回[喀什噶尔](../Page/喀什噶尔.md "wikilink")，组织军队，和清军展开斗争爭取獨立。

1758年秋天，定边将军[兆惠統兵](../Page/兆惠.md "wikilink")3000進攻葉爾羌，久攻不下，屯兵黑水河边，稱為黑水營。堅守三月\[1\]。1759年1月，清朝援軍到達，大破大小[和卓軍](../Page/和卓.md "wikilink")。大小和卓分別撤至喀什噶爾和葉爾羌，清軍分兵直取葉爾羌和喀什噶爾，“共计收获贼众一万二千余人，军器二千余件，驼骡牛羊万余”\[2\]。大小和卓又逃往[巴達克山](../Page/巴達克山.md "wikilink")。巴達克山國王[素勒坦沙在清朝的反复军事威胁下將二人杀害](../Page/素勒坦沙.md "wikilink")。波羅尼都之子[薩木薩克逃居](../Page/薩木薩克.md "wikilink")[浩罕汗国](../Page/浩罕汗国.md "wikilink")，後來薩木薩克及其子[張格爾又數度策劃復國](../Page/張格爾.md "wikilink")。

## 家庭

  - 妻子**法蒂玛**，父[艾力和卓木](../Page/艾力和卓木.md "wikilink")，弟[图尔都和卓木](../Page/图尔都和卓木.md "wikilink")。家族与霍集占家族世代不合。被霍集占离弃。[乾隆二十四年](../Page/乾隆.md "wikilink")（1759年）九月，法蒂玛作为大小和卓家眷被解往北京。二十五年正月底抵达北京，被送往[内务府](../Page/内务府.md "wikilink")。此后，图尔都和卓木的胞姐[和卓氏成为](../Page/容妃_\(乾隆帝\).md "wikilink")[乾隆帝的妃嫔](../Page/乾隆帝.md "wikilink")，出现在宫中，最终封为容妃。有学者据此认为法蒂玛和[容妃为同一人](../Page/容妃_\(乾隆帝\).md "wikilink")\[3\]。

## 注釋

[Category:清朝軍事人物](../Category/清朝軍事人物.md "wikilink")
[Category:清朝被处决者](../Category/清朝被处决者.md "wikilink")
[Category:清朝穆斯林](../Category/清朝穆斯林.md "wikilink")
[Category:喀什人](../Category/喀什人.md "wikilink")
[Category:白山派](../Category/白山派.md "wikilink")

1.  赵翼：《簷曝杂记》，卷1，《黑水营之围》
2.  《清高宗实录》，卷595，7页
3.