__NOTOC__ __NOEDITSECTION__ \<div style="margin: 0 auto;\>

<div style="margin:3px 0; border:1.5px solid #CCFF99; padding:0.5em; background-color:#fff; font-size:100%; -moz-border-radius: 8px;-webkit-border-radius: 8px;border-radius: 8px;" id="branches">

<div style="margin:3px 0; border:0; padding:0.5em 0.5em 0.3em; background-color:#FFFFCC; color:#330000; text-align:center; vertical-align:middle; font-size:220%; -moz-border-radius: 6px;;-webkit-border-radius: 6px;border-radius: 6px;">

**維基百科臺灣主題頁**

</div>

歡迎光臨[臺灣](../Page/臺灣.md "wikilink")。一個位於[西太平洋](../Page/太平洋.md "wikilink")，隔[臺灣海峽與](../Page/臺灣海峽.md "wikilink")[亞洲大陸對望的](../Page/亞洲大陸.md "wikilink")[島嶼](../Page/島嶼.md "wikilink")！
兼具[中華](../Page/中華文化.md "wikilink")、[西洋](../Page/西方文化.md "wikilink")、[南島](../Page/南島語系.md "wikilink")、[日本雜揉而成的獨特文化](../Page/日本文化.md "wikilink")。綿亙380年，有著古樸亦現代的風貌！
\<div class="center" style="border-bottom:8px solid
\#FFFFFF;\>[101.portrait.altonthompson.jpg](https://zh.wikipedia.org/wiki/File:101.portrait.altonthompson.jpg "fig:101.portrait.altonthompson.jpg")[Taipei_MRT_Jiantan_Station.jpg](https://zh.wikipedia.org/wiki/File:Taipei_MRT_Jiantan_Station.jpg "fig:Taipei_MRT_Jiantan_Station.jpg")[Mount_Yu_Shan_-_Taiwan.jpg](https://zh.wikipedia.org/wiki/File:Mount_Yu_Shan_-_Taiwan.jpg "fig:Mount_Yu_Shan_-_Taiwan.jpg")[Kongmiau.JPG](https://zh.wikipedia.org/wiki/File:Kongmiau.JPG "fig:Kongmiau.JPG")[日月潭日出.JPG](https://zh.wikipedia.org/wiki/File:日月潭日出.JPG "fig:日月潭日出.JPG")

</div>

<table>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Map_of_Taiwan.jpg" title="fig:Map_of_Taiwan.jpg">Map_of_Taiwan.jpg</a><a href="https://zh.wikipedia.org/wiki/File:LocationTaiwan.svg" title="fig:LocationTaiwan.svg">LocationTaiwan.svg</a> <strong><a href="../Page/臺灣.md" title="wikilink">臺灣</a></strong>位於東經120°至122°、北緯22°至25°間（<a href="../Page/UTC+8.md" title="wikilink">UTC+8時區</a>），面積35,759km<sup>2</sup>，係由<a href="../Page/歐亞大陸板塊.md" title="wikilink">歐亞大陸板塊</a>、<a href="../Page/菲律賓板塊.md" title="wikilink">菲律賓板塊擠壓隆起而形成</a>，故全島山勢高峻、<a href="../Page/平原.md" title="wikilink">平原狹窄</a>、<a href="../Page/地震.md" title="wikilink">地震頻仍</a>、川短流急、<a href="../Page/溫泉.md" title="wikilink">溫泉</a><a href="../Page/火山.md" title="wikilink">火山皆多</a>。<a href="../Page/氣候.md" title="wikilink">氣候方面</a>，高溫多雨、最冷月均溫在14℃以上，<a href="../Page/雨量.md" title="wikilink">雨量</a>2500mm以上。<a href="../Page/北臺灣.md" title="wikilink">北部</a>、<a href="../Page/東臺灣.md" title="wikilink">東部全年有雨</a>，<a href="../Page/中臺灣.md" title="wikilink">中</a>、<a href="../Page/南臺灣.md" title="wikilink">南部則降雨集中於夏季</a>，<a href="../Page/颱風.md" title="wikilink">颱風亦常在夏</a>、秋兩季侵襲臺灣。由於擁有超過兩百座三千公尺以上之高山，造成自然景觀相當多變，擁有從<a href="../Page/熱帶.md" title="wikilink">熱帶</a>、<a href="../Page/亞熱帶.md" title="wikilink">亞熱帶</a>、<a href="../Page/溫帶.md" title="wikilink">溫帶到</a><a href="../Page/寒帶.md" title="wikilink">寒帶等一系列氣候</a>。</p>
<p>目前臺灣本島與附屬島嶼由<strong><a href="../Page/中華民國.md" title="wikilink">中華民國</a></strong>管轄，並且為現今中華民國的<a href="../Page/中華民國疆域.md" title="wikilink">主要領土</a>。至2018年11月止，臺灣總人口數約有23,32萬 ，主要由<a href="../Page/臺灣漢人.md" title="wikilink">漢族與</a><a href="../Page/臺灣原住民族.md" title="wikilink">原住民族構成</a>，或劃分為原住民族、<a href="../Page/閩南裔台灣人.md" title="wikilink">河洛</a>、<a href="../Page/客家裔臺灣人.md" title="wikilink">客家</a>、<a href="../Page/臺灣外省人.md" title="wikilink">外省等</a>4大<a href="../Page/臺灣族群.md" title="wikilink">族群</a>，其中河洛族群占多數；人口高度集中分布於<a href="../Page/臺灣西部.md" title="wikilink">西部各大</a><a href="../Page/臺灣都會區.md" title="wikilink">都會區</a>。語言主要有<a href="../Page/漢語.md" title="wikilink">漢語</a>、<a href="../Page/台灣南島語言.md" title="wikilink">南島兩大</a><a href="../Page/語系.md" title="wikilink">語系</a>，其中<a href="../Page/通用語.md" title="wikilink">通用語文為</a><a href="../Page/中華民國國語.md" title="wikilink">標準漢語</a>，<a href="../Page/英語.md" title="wikilink">英語與</a><a href="../Page/日語.md" title="wikilink">日語亦通曉於部分社群</a>。產業方面則以高科技製品、熱帶農產品等來賺取<a href="../Page/外匯.md" title="wikilink">外匯</a>，進口則以<a href="../Page/能源.md" title="wikilink">能源為主</a>。目前朝向<a href="../Page/奈米.md" title="wikilink">奈米</a>、<a href="../Page/光電工程.md" title="wikilink">光電</a>、<a href="../Page/生物科技.md" title="wikilink">生物科技發展</a>，亦極力推動<a href="../Page/觀光.md" title="wikilink">觀光事業</a>。<a href="../Page/臺灣.md" title="wikilink">詳情...</a></p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><dl>
<dt><a href="../Page/Wikipedia:臺灣主題公告欄.md" title="wikilink">更多訊息...</a></dt>

</dl></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>colspan=2 </p></td>
</tr>
<tr class="odd">
<td><p>colspan=2 </p></td>
</tr>
</tbody>
</table>

</div>

</div>

[Portal](../Category/台灣.md "wikilink")
[地](../Category/主題首頁.md "wikilink")
[Category:台灣維基資源](../Category/台灣維基資源.md "wikilink")