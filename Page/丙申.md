**丙申**为[干支之一](../Page/干支.md "wikilink")，顺序为第33个。前一位是[乙未](../Page/乙未.md "wikilink")，后一位是[丁酉](../Page/丁酉.md "wikilink")。論[陰陽](../Page/陰陽.md "wikilink")[五行](../Page/五行.md "wikilink")，[天干之丙屬陽之火](../Page/天干.md "wikilink")，[地支之申屬陽之金](../Page/地支.md "wikilink")，是火尅金相尅。

## 丙申年

中国传统[纪年](../Page/纪年.md "wikilink")[农历的](../Page/农历.md "wikilink")[干支纪年中一个循环的第](../Page/干支纪年.md "wikilink")33年称“**丙申年**”。以下各个[公元](../Page/公元.md "wikilink")[年份](../Page/年.md "wikilink")，年份數除以60餘36，或年份數減3，除以10的餘數是3，除以12的餘數是0，自當年[立春起至次年立春止的歲次内均为](../Page/立春.md "wikilink")“丙申年”：

<table>
<caption><strong>丙申年</strong></caption>
<thead>
<tr class="header">
<th><p><a href="../Page/第1千年.md" title="wikilink">第1千年</a></p></th>
<th><p><a href="../Page/第2千年.md" title="wikilink">第2千年</a></p></th>
<th><p><a href="../Page/第3千年.md" title="wikilink">第3千年</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/36年.md" title="wikilink">36年</a></li>
<li><a href="../Page/96年.md" title="wikilink">96年</a></li>
<li><a href="../Page/156年.md" title="wikilink">156年</a></li>
<li><a href="../Page/216年.md" title="wikilink">216年</a></li>
<li><a href="../Page/276年.md" title="wikilink">276年</a></li>
<li><a href="../Page/336年.md" title="wikilink">336年</a></li>
<li><a href="../Page/396年.md" title="wikilink">396年</a></li>
<li><a href="../Page/456年.md" title="wikilink">456年</a></li>
<li><a href="../Page/516年.md" title="wikilink">516年</a></li>
<li><a href="../Page/576年.md" title="wikilink">576年</a></li>
<li><a href="../Page/636年.md" title="wikilink">636年</a></li>
<li><a href="../Page/696年.md" title="wikilink">696年</a></li>
<li><a href="../Page/756年.md" title="wikilink">756年</a></li>
<li><a href="../Page/816年.md" title="wikilink">816年</a></li>
<li><a href="../Page/876年.md" title="wikilink">876年</a></li>
<li><a href="../Page/936年.md" title="wikilink">936年</a></li>
<li><a href="../Page/996年.md" title="wikilink">996年</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/1056年.md" title="wikilink">1056年</a></li>
<li><a href="../Page/1116年.md" title="wikilink">1116年</a></li>
<li><a href="../Page/1176年.md" title="wikilink">1176年</a></li>
<li><a href="../Page/1236年.md" title="wikilink">1236年</a></li>
<li><a href="../Page/1296年.md" title="wikilink">1296年</a></li>
<li><a href="../Page/1356年.md" title="wikilink">1356年</a></li>
<li><a href="../Page/1416年.md" title="wikilink">1416年</a></li>
<li><a href="../Page/1476年.md" title="wikilink">1476年</a></li>
<li><a href="../Page/1536年.md" title="wikilink">1536年</a></li>
<li><a href="../Page/1596年.md" title="wikilink">1596年</a></li>
<li><a href="../Page/1656年.md" title="wikilink">1656年</a></li>
<li><a href="../Page/1716年.md" title="wikilink">1716年</a></li>
<li><a href="../Page/1776年.md" title="wikilink">1776年</a></li>
<li><a href="../Page/1836年.md" title="wikilink">1836年</a></li>
<li><a href="../Page/1896年.md" title="wikilink">1896年</a></li>
<li><a href="../Page/1956年.md" title="wikilink">1956年</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/2016年.md" title="wikilink">2016年</a></li>
<li><a href="../Page/2076年.md" title="wikilink">2076年</a></li>
<li><a href="../Page/2136年.md" title="wikilink">2136年</a></li>
<li><a href="../Page/2196年.md" title="wikilink">2196年</a></li>
<li><a href="../Page/2256年.md" title="wikilink">2256年</a></li>
<li><a href="../Page/2316年.md" title="wikilink">2316年</a></li>
<li><a href="../Page/2376年.md" title="wikilink">2376年</a></li>
<li><a href="../Page/2436年.md" title="wikilink">2436年</a></li>
<li><a href="../Page/2496年.md" title="wikilink">2496年</a></li>
<li><a href="../Page/2556年.md" title="wikilink">2556年</a></li>
<li><a href="../Page/2616年.md" title="wikilink">2616年</a></li>
<li><a href="../Page/2676年.md" title="wikilink">2676年</a></li>
<li><a href="../Page/2736年.md" title="wikilink">2736年</a></li>
<li><a href="../Page/2796年.md" title="wikilink">2796年</a></li>
<li><a href="../Page/2856年.md" title="wikilink">2856年</a></li>
<li><a href="../Page/2916年.md" title="wikilink">2916年</a></li>
<li><a href="../Page/2976年.md" title="wikilink">2976年</a></li>
</ul></td>
</tr>
</tbody>
</table>

## 丙申月

天干丙年和辛年，[立秋到](../Page/立秋.md "wikilink")[白露的時間段](../Page/白露.md "wikilink")，就是**丙申月**：

  - ……
  - [1976年](../Page/1976年.md "wikilink")8月立秋到9月白露
  - [1981年](../Page/1981年.md "wikilink")8月立秋到9月白露
  - [1986年](../Page/1986年.md "wikilink")8月立秋到9月白露
  - [1991年](../Page/1991年.md "wikilink")8月立秋到9月白露
  - [1996年](../Page/1996年.md "wikilink")8月立秋到9月白露
  - [2001年](../Page/2001年.md "wikilink")8月立秋到9月白露
  - [2006年](../Page/2006年.md "wikilink")8月立秋到9月白露
  - ……

## 丙申日

## 丙申時

天干丙日和辛日，[北京时间](../Page/北京时间.md "wikilink")（[UTC](../Page/协调世界时.md "wikilink")+8）15時到17時，就是**丙申時**。

## 丙申年丙申月丙申日丙申時

  - 1896年8月11日15時到17時（[UTC](../Page/协调世界时.md "wikilink")+8）
  - 2076年8月27日15時到17時（[UTC](../Page/协调世界时.md "wikilink")+8）

## 參考資料

1.
[Category:干支](../Category/干支.md "wikilink")
[\*](../Category/丙申年.md "wikilink")