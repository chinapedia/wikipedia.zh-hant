[NHK_Broadcasting_Center_Parabola_antenna_3.JPG](https://zh.wikipedia.org/wiki/File:NHK_Broadcasting_Center_Parabola_antenna_3.JPG "fig:NHK_Broadcasting_Center_Parabola_antenna_3.JPG")（[廣播衛星](../Page/衛星電視.md "wikilink")）類比電視訊號專用[碟型天線](../Page/碟型天線.md "wikilink")\]\]
[NHK_Studio_Park_01.jpg](https://zh.wikipedia.org/wiki/File:NHK_Studio_Park_01.jpg "fig:NHK_Studio_Park_01.jpg")

**NHK放送中心**（，）是[日本放送協會](../Page/日本放送協會.md "wikilink")（NHK）的[總部所在地](../Page/總部.md "wikilink")，位於[東京都](../Page/東京都.md "wikilink")[澀谷區的](../Page/澀谷區.md "wikilink")[神南](../Page/神南.md "wikilink")。1964年啟用，最早做為該年[東京奧運的](../Page/1964年夏季奧林匹克運動會.md "wikilink")使用，經過擴建之後，NHK總部在1973年從東京[內幸町的](../Page/內幸町.md "wikilink")轉移至此。NHK的大部分節目均在此製作，除了肩負NHK總台的職責（含[電台及](../Page/電台廣播.md "wikilink")[電視](../Page/電視.md "wikilink")[廣播](../Page/廣播.md "wikilink")）之外，也兼設[首都圈放送中心](../Page/首都圈_\(日本\).md "wikilink")，負責對[東京都會區的廣播業務](../Page/東京.md "wikilink")。其廣播區域為[關東廣域圈](../Page/關東廣域圈.md "wikilink")（數位[綜合頻道不含](../Page/NHK綜合頻道.md "wikilink")[茨城縣](../Page/茨城縣.md "wikilink")、[栃木县及](../Page/栃木县.md "wikilink")[群马县](../Page/群马县.md "wikilink")）。

NHK放送中心的建地內共有6棟建築（包含[NHK音樂廳](../Page/NHK音樂廳.md "wikilink")），除了主要功能的廣播製播設施外，NHK放送中心亦附設數個設施：

  - [NHK攝影棚公園](../Page/NHK攝影棚公園.md "wikilink")

<!-- end list -->

  -
    1995年3月22日開幕，為面向[視聽人與一般](../Page/閱聽人.md "wikilink")[民眾的廣播體驗設施](../Page/民眾.md "wikilink")。

<!-- end list -->

  - NHK音樂廳

<!-- end list -->

  -
    1973年6月20日開館，為東京著名[表演廳之一](../Page/多功能大厅.md "wikilink")。毎年[除夕](../Page/除夕.md "wikilink")[現場直播的](../Page/現場直播.md "wikilink")[NHK紅白歌合戰在此舉行](../Page/NHK紅白歌合戰.md "wikilink")。

<!-- end list -->

  - [中國中央電視台](../Page/中國中央電視台.md "wikilink")、[韓國放送公社](../Page/韓國放送公社.md "wikilink")、[澳大利亞廣播公司等海外](../Page/澳大利亞廣播公司.md "wikilink")[公共媒體與](../Page/公共媒體.md "wikilink")[國營媒體機構的駐日站點也在NHK放送中心中](../Page/國營媒體.md "wikilink")。

由於NHK放送中心使用時間近半世紀，為了因應耐震、[節能與災害應對上的需求](../Page/節約能源.md "wikilink")，同時強化自身做為世界領先媒體機構的地位，NHK在2016年8月30日發表改建基本計畫\[1\]，將以設計及施工[統包的方式改建NHK音樂廳以外的所有建築](../Page/統包.md "wikilink")，預計在[2020年東京奧運結束後分階段動工](../Page/2020年夏季奧林匹克運動會.md "wikilink")、2025年部分啟用、2036年全部完工\[2\]。

## 參考資料

## 外部鏈結

  - [NHK首都圈放送中心](http://www.nhk.or.jp/shutoken/index.html)
  - [NHK音樂廳](http://www.nhk-sc.or.jp/nhk_hall/)

[Category:東京都建築物](../Category/東京都建築物.md "wikilink")
[Category:澀谷](../Category/澀谷.md "wikilink")
[Category:NHK](../Category/NHK.md "wikilink")
[Category:BCS獎](../Category/BCS獎.md "wikilink")
[Category:100米至149米高的摩天大樓](../Category/100米至149米高的摩天大樓.md "wikilink")
[Category:1973年完工建築物](../Category/1973年完工建築物.md "wikilink")

1.

2.