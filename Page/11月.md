**11月**是[公历年中的第十一个月](../Page/公历.md "wikilink")，是一年當中最後一個[小月](../Page/月.md "wikilink")，共有30天。

在[北半球](../Page/北半球.md "wikilink")，11月是[冬季的第一个月](../Page/冬季.md "wikilink")，本月[节气](../Page/节气.md "wikilink")：[立冬](../Page/立冬.md "wikilink")、[小雪](../Page/小雪.md "wikilink")。

在[英文中](../Page/英语.md "wikilink")，11月（November）源自[拉丁语](../Page/拉丁语.md "wikilink")*November*（第9个月）。

## 11月的節日／紀念日

  - [天主教的](../Page/天主教.md "wikilink")[諸聖節](../Page/萬聖節.md "wikilink")，庆祝仪式在[11月1日](../Page/11月1日.md "wikilink")，在[諸聖節之夜](../Page/萬聖夜.md "wikilink")（Halloween/All
    Hallows Eve）后。以及11月2日為[追思亡者節](../Page/追思亡者節.md "wikilink")。
    在[瑞典](../Page/瑞典.md "wikilink")，萬聖節官方假日在11月的第一个星期六。

<!-- end list -->

  - [11月8日是](../Page/11月8日.md "wikilink")[國際陰陽人團結日](../Page/國際陰陽人團結日.md "wikilink")。

  - 在[中国](../Page/中国.md "wikilink")，[11月8日是](../Page/11月8日.md "wikilink")[记者节](../Page/记者节.md "wikilink")。

  - 在[中国](../Page/中国.md "wikilink")，[11月9日是](../Page/11月9日.md "wikilink")[消防节](../Page/消防节.md "wikilink")。

  - [美国總統選舉規定在選舉年的](../Page/美国總統.md "wikilink")11月裡第一個星期一之後的一個星期二舉行。因此通常在[11月2日](../Page/11月2日.md "wikilink")－[11月8日之间举行](../Page/11月8日.md "wikilink")。

  - [11月11日是](../Page/11月11日.md "wikilink")[波蘭的](../Page/波兰.md "wikilink")[國慶節](../Page/国庆日.md "wikilink")。光棍節(單身節)

  - [11月12日是](../Page/11月12日.md "wikilink")[中華民國國父](../Page/中華民國國父.md "wikilink")[孫中山先生壽辰紀念日](../Page/孫中山.md "wikilink")，在很多[華人社區都會紀念這一天](../Page/华人.md "wikilink")。

  - [11月17日是](../Page/11月17日.md "wikilink")[国际大学生节](../Page/世界学生节.md "wikilink")。

  - 美国在11月的第四个星期四庆祝[感恩节](../Page/感恩节.md "wikilink")。

  - 在[11月17日前后](../Page/11月17日.md "wikilink")，[狮子座流星雨达到顶峰](../Page/狮子座流星雨.md "wikilink")。

  - [11月20日是](../Page/11月20日.md "wikilink")[國際跨性別紀念日](../Page/國際跨性別紀念日.md "wikilink")。

  - 在[11月24日](../Page/11月24日.md "wikilink")，為[中華民國政黨](../Page/中華民國.md "wikilink")[中國國民黨的黨慶日](../Page/中國國民黨.md "wikilink")，同日為國民黨的前身—興中會的創立日。

  - [11月25日是](../Page/11月25日.md "wikilink")[國際消除對婦女的暴力日以及](../Page/國際消除對婦女的暴力日.md "wikilink")[國際素食日](../Page/國際素食日.md "wikilink")。

  -
## 其它

  - 11月开始于与3月一个星期的同样一天。
  - 在[平年](../Page/平年.md "wikilink")，11月开始于与2月一个星期的同样一天。

## 参看

  - [四季](../Page/四季.md "wikilink")：[春季](../Page/春季.md "wikilink") --
    [夏季](../Page/夏季.md "wikilink") -- [秋季](../Page/秋季.md "wikilink")
    -- [冬季](../Page/冬季.md "wikilink")

[月11](../Category/月份.md "wikilink") [月11](../Category/日历.md "wikilink")