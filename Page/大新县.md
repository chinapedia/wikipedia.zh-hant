[大新縣車站.jpg](https://zh.wikipedia.org/wiki/File:大新縣車站.jpg "fig:大新縣車站.jpg")
**大新县**是[中国](../Page/中国.md "wikilink")[广西壮族自治区](../Page/广西壮族自治区.md "wikilink")[崇左市所辖的一个](../Page/崇左市.md "wikilink")[县](../Page/县.md "wikilink")。总面积为2756平方公里，2004年人口为36万。与[越南](../Page/越南.md "wikilink")[高平省接壤](../Page/高平省.md "wikilink")。县政府驻[桃城镇](../Page/桃城镇.md "wikilink")，距南宁市116公里。

## 历史沿革

1952年以[雷平县](../Page/雷平县.md "wikilink")、[养利县](../Page/养利县.md "wikilink")、[万承县为基础](../Page/万承县.md "wikilink")，撤销此三县，新设大新县，以万承县[大岭乡的](../Page/大岭乡.md "wikilink")“大”字和养利县[宝新乡的](../Page/宝新乡.md "wikilink")“新”字二字为名，属[桂西壮族自治区](../Page/桂西壮族自治区.md "wikilink")[崇左专区](../Page/崇左专区.md "wikilink")。1971年改属[南宁地区](../Page/南宁地区.md "wikilink")。\[1\]

## 经济

盛产龙眼。\[2\]矿产资源以[锰为主](../Page/锰.md "wikilink")，全国唯一储量超过1亿吨的锰矿下雷锰矿即位于大新县\[3\]\[4\]，相邻的越南[高平省](../Page/高平省.md "wikilink")[重庆县等地亦产锰矿](../Page/重庆县.md "wikilink")。

## 行政区划

大新县辖5个镇、9个乡：桃城镇、全茗镇、雷平镇、硕龙镇、下雷镇、五山乡、龙门乡、昌明乡、福隆乡、那岭乡、恩城乡、榄圩乡、宝圩乡、堪圩乡。县人民政府驻桃城镇。

2003年，振兴乡与雷平镇合并为雷平镇。 2005年7月，土湖乡并入下雷镇，组建新的下雷镇。

## 旅游景点

  - [德天瀑布](../Page/德天瀑布.md "wikilink")，国家4A级景区
  - [明仕田园风光](../Page/明仕田园风光.md "wikilink")，国家4A级景区
  - [宝贤石林](../Page/宝贤石林.md "wikilink")

## 参考资料

## 外部链接

  - [崇左市大新县简介](http://www.gx.xinhuanet.com/dtzx/dxwz/index.htm)

[大新县](../Category/大新县.md "wikilink")
[县](../Category/崇左区县市.md "wikilink")
[崇左](../Category/广西县份.md "wikilink")

1.  ，p.1402-1406.

2.
3.  [广西锰矿重晶石储量全国第一，矿产新闻](http://www.mlr.gov.cn/xwdt/kyxw/201602/t20160226_1397425.htm)，中华人民共和国国土资源部

4.  [锰行天下五十年——中信大锰矿业有限责任公司发展实录](http://www.czmy.net.cn/News/GeneralNews/NewsDetail.aspx?id=20141224000002)
    ，崇左市锰业信息服务网