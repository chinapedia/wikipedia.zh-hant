[Miki.png](https://zh.wikipedia.org/wiki/File:Miki.png "fig:Miki.png")
[YES\!959期封面.jpg](https://zh.wikipedia.org/wiki/File:YES!959期封面.jpg "fig:YES!959期封面.jpg")

《**Yes\!**》（1990年11月20日－2014年7月25日，），是[香港一本以](../Page/香港.md "wikilink")[青少年為對象的](../Page/青少年.md "wikilink")[雜誌](../Page/雜誌.md "wikilink")，口號為「全天候年輕人雜誌」。於[1990年](../Page/香港1990年.md "wikilink")11月20日由[倪震與](../Page/倪震.md "wikilink")[邵國華創辦](../Page/邵國華_\(主持人\).md "wikilink")。初期為逢5日、20日出版之半月刊，售價為[港幣](../Page/港幣.md "wikilink")10元，後轉為逢1日、11日、21日出版（但通常會遲3至5天才能在市面上買到），再後來轉為逢星期五出版，售價也改為12元，主要在[香港與](../Page/香港.md "wikilink")[澳門發行](../Page/澳門.md "wikilink")，另外從一些非正式渠道流向[中國大陸](../Page/中國大陸.md "wikilink")[珠江三角洲地區](../Page/珠江三角洲.md "wikilink")，但售價往往高達[人民幣](../Page/人民幣.md "wikilink")20-30元。當中較長時間出現過的虛構角色包括：Yes\!
sir、古域、華少、阿四、木哥、雯雯（原名李穎雯，當時說她是邵國華的表妹）、徐意。至停刊前《Yes\!》的封面，主要都是雙封面出版，有時會以三封面出版（例：965期、974期），975、1000期《Yes\!》更特別以四封面出版，絕少會以單封面出版（除了971期、983期、984期）。每期均會送一些偶像用品（例：953、966期偶像A4文件夾）。至2014年7月25日停刊，告別號為1219期\[1\]。

## 歷史

[1990年代初](../Page/香港1990年代.md "wikilink")，著名作家[倪匡之兒子](../Page/倪匡.md "wikilink")[倪震](../Page/倪震.md "wikilink")，連同[梁繼璋和邵國華在](../Page/梁繼璋.md "wikilink")[香港電台第二台主持一個名為](../Page/香港電台第二台.md "wikilink")《[三個寂寞的心](../Page/三個寂寞的心.md "wikilink")》的節目。其後，倪震和邵國華憑電台的知名度，開辦名為《Yes\!》的年青人[雜誌](../Page/雜誌.md "wikilink")。

《Yes\!》是當時少數以[香港](../Page/香港.md "wikilink")[青少年](../Page/青少年.md "wikilink")、[中學生為對象](../Page/中學生.md "wikilink")，以突出版面，為青少年分擔戀愛的煩惱的刊物。創刊不久即深受青少年、中學生歡迎，發行量不斷上升。後來發行以[藝人照片製作的](../Page/藝人.md "wikilink")“[Yes\!Card](../Page/Yes!Card.md "wikilink")”亦大受歡迎，主要在[旺角](../Page/旺角.md "wikilink")[信和中心等地銷售](../Page/信和中心.md "wikilink")，其銷量更曾成為藝人受歡迎與否的指標。

創刊後發生了“毒瘤明”事件：當時的男歌手[劉錫明對傳媒表示欣賞](../Page/劉錫明.md "wikilink")[周慧敏](../Page/周慧敏.md "wikilink")，惹來其男友，即《Yes\!》創辦人兼主編之一的倪震不滿，於是在其[娛樂圈軼事專欄](../Page/娛樂圈.md "wikilink")，不斷抨擊一名叫“[毒瘤明](http://joenieh.blogspot.com/2006/06/blog-post.html)”的人物長達三年，包括誣衊對方偷窺、嫖妓等，引起YES\!讀者及[周慧敏支持者對](../Page/周慧敏.md "wikilink")[劉錫明的強烈反感](../Page/劉錫明.md "wikilink")，事業發展陷入低谷，逼使[劉錫明離港發展](../Page/劉錫明.md "wikilink")。

[1998年](../Page/香港1998年.md "wikilink")11月20日第400期的「學生報」中，因封面小字標題「元朗愛心測試可道學生至口賤元朗官中至豪爽」引起[可道中學師生不滿](../Page/可道中學_\(嗇色園主辦\).md "wikilink")，[雜誌社總編輯黃仁傑於](../Page/雜誌.md "wikilink")404期首頁澄清事件「絕無泛指全體學生」、收回該標題並由衷致歉。

期數跳動方面，1998年11月20日出版的定為400期（原為316期）。

### 宣布停刊

2014年7月31日，《Yes\!》在其[Facebook專頁宣布停刊](../Page/Facebook.md "wikilink")，1219期（7月25日出版）為告別號。同時宣布，為了緊貼新聞媒體電子化的大趨勢，將會於不久的將來推出[電子版雜誌](../Page/電子雜誌.md "wikilink")。\[2\]\[3\]

1219期《Yes\!》[封面人物分別是](../Page/封面人物.md "wikilink")[江若琳](../Page/江若琳.md "wikilink")，獨家專訪江若琳入行10年的心路歷程、[日本小天后](../Page/日本.md "wikilink")[板野友美](../Page/板野友美.md "wikilink")，獨家專訪板野友美的秘密生活、和[韓流天團](../Page/韓流.md "wikilink")[Super
Junior-M](../Page/Super_Junior-M.md "wikilink")，探討他們如何自揭自己的真面目。隨書附送偶像[海報](../Page/海報.md "wikilink")4張，分別是[李敏鎬](../Page/李敏鎬.md "wikilink")、[B.A.P](../Page/B.A.P.md "wikilink")、[G.E.M.和](../Page/鄧紫棋.md "wikilink")[BOP天堂鳥](../Page/天堂鳥_\(組合\).md "wikilink")。

### 實體版停刊後發展

2015年由新的投資者益思文化收購。在2016年6月3日於[灣仔](../Page/灣仔.md "wikilink")[香港會議展覽中心Hall](../Page/香港會議展覽中心.md "wikilink")
5BC舉辦了Yes25週年演唱會\[4\]，邀請了[譚詠麟](../Page/譚詠麟.md "wikilink")、[草蜢](../Page/草蜢.md "wikilink")、[孫耀威](../Page/孫耀威.md "wikilink")、[王灝兒](../Page/王灝兒.md "wikilink")、[葉佩雯](../Page/葉佩雯.md "wikilink")、[Super
Girls](../Page/Super_Girls.md "wikilink")、[劉錫明等演出](../Page/劉錫明.md "wikilink")；因劉為上述“毒瘤明事件”之主角，縱使策劃人[倪震早已退股](../Page/倪震.md "wikilink")，劉參與拍攝Yes
Card及演唱會，被傳媒視為不計前嫌的大方之舉，贏得一時佳話\[5\]\[6\]\[7\]。

## 社會影響

《Yes\!》雜誌透過分擔戀愛的煩惱，初推出時深受青少年、中學生歡迎，更破天荒地設立讓學生讀者來信抨擊學校、老師的專欄，在當時老師權威龐大的[香港社會](../Page/香港社會.md "wikilink")，造成甚多的爭議和哄動，此外，雜誌內有不少鼓勵中學生談戀愛的內容，時尚潮流情報亦經常介紹名牌服飾。因而令不少家長、教育界人士不滿，批評此雜誌煽動青少年、中學生作出挑戰師權、求學時期談戀愛、崇尚名牌等「反叛」行為，視之為洪水猛獸。同時，亦有不少人跟風推出類似雜誌，令市面出現大量以英文單字為名的年青人雜誌，但都只是曇花一現，當中以[黃一山出資的](../Page/黃一山.md "wikilink")《夠Pop》支持得最長久，但也難逃在數個月內倒閉的命運。[1991年](../Page/香港1991年.md "wikilink")[暑假](../Page/暑假.md "wikilink")，更有一部名為《[Yes一族](../Page/Yes一族.md "wikilink")》的[港產片上映](../Page/香港電影.md "wikilink")。而既有市場的佔有者，如以女性讀者為主的《姊妹》以及長期以來以中學生為對象的《[突破少年](../Page/突破機構#歷史.md "wikilink")》，都因着這股衝擊而改版應付。《突破少年》及至2000年代亦停刊改為網上出版[1](https://web.archive.org/web/20061205015914/http://uzone.com.hk/)，實體版雜誌亦兩度易名為《U+》(2001年)及《突破書誌
Breakazine！》(2009年)。

到了約[1990年代後期](../Page/香港1990年代.md "wikilink")，早期看《Yes\!》成長的中學生已進入[專上院校或投身社會工作](../Page/香港專上教育.md "wikilink")，以中學生為主要對象的青少年消閒雜誌已不適合他們。相反，他們多數改看[壹傳媒所出版的](../Page/壹傳媒.md "wikilink")《[壹本便利](../Page/壹本便利.md "wikilink")》，使這類雜誌的需求與日俱增，其他同類型的雜誌也相繼推出，各自有自己的特色。到了[2000年代](../Page/香港2000年代.md "wikilink")，亦曾出版過一本名為《Teens\!Weekly》的雜誌作為抗衡。《新假期》、《新Monday》、《Milk》、《Cream》、《COOL@Link》這類雜誌就或多或少亦能表現出年青人希望緊貼潮流脈搏的同時，亦希望自己會有獨特空間，與群眾分別出來。

另，雜誌內之「城市驚喜」欄目，以[星探形式或自薦形式](../Page/星探.md "wikilink")，每期邀請不同的少女當[模特兒拍照](../Page/模特兒.md "wikilink")，因此直接或間接捧紅部分少女成為[電視](../Page/電視.md "wikilink")[藝員或](../Page/藝員.md "wikilink")[歌星](../Page/歌星.md "wikilink")（如[張栢芝](../Page/張栢芝.md "wikilink")、[韓君婷](../Page/韓君婷.md "wikilink")、[鄧麗欣](../Page/鄧麗欣.md "wikilink")、[楊愛瑾及](../Page/楊愛瑾.md "wikilink")[傅穎等](../Page/傅穎.md "wikilink")），其中息影女藝人，[李嘉誠次子](../Page/李嘉誠.md "wikilink")[李澤楷之前度女友](../Page/李澤楷.md "wikilink")[梁洛施](../Page/梁洛施.md "wikilink")，於13歲時參加《Yes\!》雜誌的城市驚喜專欄，並成為該期雜誌封面，一鳴驚人而加入[娛樂圈](../Page/娛樂圈.md "wikilink")。

## 封面

《Yes\!》的封面主要是雙封面出版，有時會以三封面出版（例：965期、974期），975、1000期《Yes\!》更特別以四封面出版，絕少會以單封面出版（除了971期、983期、984期）。

### 封面人物

| 期數 | 封面1 | 封面2 | 封面3 | 備註 |
| -- | --- | --- | --- | -- |
|    |     |     |     |    |

| 期數 | 封面1 | 封面2 | 封面3 | 備註 |
| -- | --- | --- | --- | -- |
|    |     |     |     |    |

| 期數 | 封面1 | 封面2 | 封面3 | 備註 |
| -- | --- | --- | --- | -- |
|    |     |     |     |    |

| 期數 | 封面1 | 封面2 | 封面3 | 備註 |
| -- | --- | --- | --- | -- |
|    |     |     |     |    |

| 期數 | 封面1 | 封面2 | 封面3 | 備註 |
| -- | --- | --- | --- | -- |
|    |     |     |     |    |

| 期數 | 封面1 | 封面2 | 封面3 | 備註 |
| -- | --- | --- | --- | -- |
|    |     |     |     |    |

| 期數 | 封面1 | 封面2 | 封面3 | 備註 |
| -- | --- | --- | --- | -- |
|    |     |     |     |    |

| 期數 | 封面1 | 封面2 | 封面3 | 備註 |
| -- | --- | --- | --- | -- |
|    |     |     |     |    |

| 期數 | 封面1 | 封面2 | 封面3 | 備註 |
| -- | --- | --- | --- | -- |
|    |     |     |     |    |

| 期數 | 封面1 | 封面2 | 封面3 | 備註 |
| -- | --- | --- | --- | -- |
|    |     |     |     |    |

#### 2010年代

<table>
<thead>
<tr class="header">
<th><p>期數</p></th>
<th><p>封面1</p></th>
<th><p>封面2</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>981</p></td>
<td><p>Hotcha</p></td>
<td><p>小薰、丫頭、勇兔<br />
糖果、溫家恒</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>982</p></td>
<td><p>飛輪海</p></td>
<td><p>江若琳</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>983</p></td>
<td><p>鄧麗欣</p></td>
<td><p>本期只有一個封面</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>984</p></td>
<td><p>黑Girl</p></td>
<td><p>本期只有一個封面</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>985</p></td>
<td><p>鍾嘉欣</p></td>
<td><p>Circus</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>986</p></td>
<td><p>HotCha</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>987</p></td>
<td><p>鍾舒漫、洪卓立<br />
泳兒</p></td>
<td><p>汪東城、王心凌<br />
辰亦儒</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>988</p></td>
<td><p>GEM鄧紫棋</p></td>
<td><p>羅志祥</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>989</p></td>
<td><p>棒棒堂</p></td>
<td><p>SHINee</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>990</p></td>
<td><p>江若琳</p></td>
<td><p>FTIsland</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>991</p></td>
<td><p>楊丞琳</p></td>
<td><p>w-inds</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>992</p></td>
<td><p>洪卓立</p></td>
<td><p>鬼鬼</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>993</p></td>
<td><p>蔡卓妍</p></td>
<td><p>羅志祥</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>994</p></td>
<td><p>飛輪海</p></td>
<td><p><a href="../Page/李民浩.md" title="wikilink">李民浩</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>995</p></td>
<td><p>GEM鄧紫棋</p></td>
<td><p>小薰、丫頭</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>996</p></td>
<td><p>鄧麗欣</p></td>
<td><p>By2</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>997</p></td>
<td><p>School Tour</p></td>
<td><p>本期只有一個封面</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>998</p></td>
<td><p>Wonder Girls</p></td>
<td><p>羅志祥</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>999</p></td>
<td><p>Hotcha</p></td>
<td><p>王子、毛弟和小杰</p></td>
<td></td>
</tr>
</tbody>
</table>

<table>
<thead>
<tr class="header">
<th><p>期數</p></th>
<th><p>封面1</p></th>
<th><p>封面2</p></th>
<th><p>封面3</p></th>
<th><p>封面4</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1000</p></td>
<td><p>鄭秀文</p></td>
<td><p>Twins</p></td>
<td><p><a href="../Page/Big_Four.md" title="wikilink">Big Four</a></p></td>
<td><p>農夫、歐陽靖</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1001</p></td>
<td><p>S.H.E</p></td>
<td><p><a href="../Page/超級巨聲.md" title="wikilink">超級巨聲</a></p></td>
<td><p> </p></td>
<td><p> </p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1002</p></td>
<td><p>Super Junior</p></td>
<td><p>鍾欣桐</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1003</p></td>
<td><p>林峯</p></td>
<td><p>F.CUZ</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1004</p></td>
<td><p>鍾嘉欣</p></td>
<td><p>黑Girl</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1005</p></td>
<td><p>王子、洪詩<br />
小傑、玉兔</p></td>
<td><p>江若琳</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1006</p></td>
<td><p>Hotcha</p></td>
<td><p>By2</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1007</p></td>
<td><p>GEM鄧紫棋</p></td>
<td><p>蔡卓妍</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1008</p></td>
<td><p>鄧麗欣</p></td>
<td><p>方力申、鍾欣桐</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1009</p></td>
<td><p>FTIsland</p></td>
<td><p>鬼鬼</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1010</p></td>
<td><p>林峯</p></td>
<td><p>汪東城</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1011</p></td>
<td><p>鍾嘉欣</p></td>
<td><p>羅志祥、黃鴻升</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1012</p></td>
<td><p>野仔</p></td>
<td><p>大牙、小傑、Apple</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1013</p></td>
<td><p>鄧麗欣</p></td>
<td><p>江若琳</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1014</p></td>
<td><p>FTIsland</p></td>
<td><p>衛蘭、文穎珊</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1015</p></td>
<td><p>王子、小傑、毛弟、鮪魚</p></td>
<td><p>金賢重</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1016</p></td>
<td><p>周柏豪</p></td>
<td><p>CNBLUE</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1017</p></td>
<td><p>洪卓立</p></td>
<td><p>By2</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1018</p></td>
<td><p>鍾舒漫</p></td>
<td><p>飛輪海</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1019</p></td>
<td><p>楊丞琳</p></td>
<td><p>林欣彤</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1020</p></td>
<td><p>鍾嘉欣</p></td>
<td><p>AK</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1021</p></td>
<td><p>GEM</p></td>
<td><p>飛輪海</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1022</p></td>
<td><p>陳柏宇</p></td>
<td><p>蔡卓妍</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1023</p></td>
<td><p>少女時代</p></td>
<td><p>江若琳</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1024</p></td>
<td><p>JYJ</p></td>
<td><p>周麗淇</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1025</p></td>
<td><p>飛輪海</p></td>
<td><p>School Tour</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1026</p></td>
<td><p>GEM</p></td>
<td><p>meimei、王子、毛弟、</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1027</p></td>
<td><p>少女時代</p></td>
<td><p>蔡卓妍</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1028</p></td>
<td><p>FTIsland</p></td>
<td><p>鍾欣桐</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1029</p></td>
<td><p>鍾嘉欣</p></td>
<td><p>陳偉霆</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1030</p></td>
<td><p>鄧麗欣</p></td>
<td><p>炎亞綸、辰亦儒</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1031</p></td>
<td><p>GEM</p></td>
<td><p>狄易達</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1032</p></td>
<td><p>汪東城</p></td>
<td><p>江若琳</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1033</p></td>
<td><p>BEAST</p></td>
<td><p>AK</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1034</p></td>
<td><p>Kara</p></td>
<td><p>小傑、王子、毛弟<br />
鮪魚、筱婕、meimei</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1035</p></td>
<td><p>羅力威</p></td>
<td><p>Lollopop@F</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1036</p></td>
<td><p>楊丞琳</p></td>
<td><p>蔡卓妍</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1037</p></td>
<td><p>Hotcha</p></td>
<td><p>羅志祥</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1038</p></td>
<td><p>洪卓立</p></td>
<td><p>江若琳</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1039</p></td>
<td><p>鍾嘉欣</p></td>
<td><p>鄧麗欣</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1040</p></td>
<td><p>林峯</p></td>
<td><p>4Minute</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1041</p></td>
<td><p>GEM</p></td>
<td><p>朴政珉</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1042</p></td>
<td><p>林峯</p></td>
<td><p>糖妹</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1043</p></td>
<td><p>Hotcha</p></td>
<td><p>江若琳</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1044</p></td>
<td><p>Super Junior-M</p></td>
<td><p>炎亞綸</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1045</p></td>
<td><p>鍾嘉欣</p></td>
<td><p>蔡卓妍</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1046</p></td>
<td><p>GEM</p></td>
<td><p>張根碩</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1047</p></td>
<td><p>少女時代</p></td>
<td><p>王子</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1048</p></td>
<td><p>張敬軒</p></td>
<td><p>江若琳</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1049</p></td>
<td><p>文穎珊</p></td>
<td><p>Justin Bieber</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1050</p></td>
<td><p>AKB48</p></td>
<td><p>朴政珉</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1051</p></td>
<td><p>School Tour</p></td>
<td><p>炎亞綸</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1052</p></td>
<td><p>鍾嘉欣</p></td>
<td><p>黑Girl</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1053</p></td>
<td><p>洪卓立</p></td>
<td><p>Justin Bieber</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1054</p></td>
<td><p>FTIsland</p></td>
<td><p>林峯</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1055</p></td>
<td><p>2AM</p></td>
<td><p>羅志祥</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1056</p></td>
<td><p>少女時代</p></td>
<td><p>Circus</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1057</p></td>
<td><p>2PM</p></td>
<td><p>Twins</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1058</p></td>
<td><p>金賢重</p></td>
<td><p>GEM</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1059</p></td>
<td><p>韓庾</p></td>
<td><p>Twins</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1060</p></td>
<td><p>井柏然</p></td>
<td><p>Lady Gaga</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1061</p></td>
<td><p>鄭容和</p></td>
<td><p>楊丞琳</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1062</p></td>
<td><p>林峯</p></td>
<td><p>金亨俊</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1063</p></td>
<td><p>GEM</p></td>
<td><p>羅志祥</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1064</p></td>
<td><p>Super Junior</p></td>
<td><p>小鬼</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1065</p></td>
<td><p>敖犬</p></td>
<td><p>江若琳</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1066</p></td>
<td><p>少女時代</p></td>
<td><p>楊丞琳</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1067</p></td>
<td><p>Hotcha</p></td>
<td><p>汪東城</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1068</p></td>
<td><p>糖妹</p></td>
<td><p>f(x)</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1069</p></td>
<td><p>2PM</p></td>
<td><p>JPM</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1070</p></td>
<td><p>Super Junior</p></td>
<td><p>吳尊</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1071</p></td>
<td><p>林峯</p></td>
<td><p>AK</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1072</p></td>
<td><p>鍾嘉欣</p></td>
<td><p>張根碩</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1073</p></td>
<td><p>SHINee</p></td>
<td><p>Lollopop@F</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1074</p></td>
<td><p>黑Girl</p></td>
<td><p>AKB48</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1075</p></td>
<td><p>少女時代</p></td>
<td><p>W-inds</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1076</p></td>
<td><p>羅志祥</p></td>
<td><p>School Tour</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1077</p></td>
<td><p>GEM</p></td>
<td><p>Justin Bieber</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1078</p></td>
<td><p>林欣彤</p></td>
<td><p>洪卓立、陳偉霆</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1079</p></td>
<td><p>羅志祥</p></td>
<td><p>Lollopop@F</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1080</p></td>
<td><p>JPM</p></td>
<td><p>黑Girl</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1081</p></td>
<td><p>少女時代</p></td>
<td><p>Lollopop@F</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1082</p></td>
<td><p>林峯</p></td>
<td><p>Kara</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1083</p></td>
<td><p>柯震東</p></td>
<td><p>Taylor Lautner、Kristen Stewart、Robert Pattinson</p></td>
<td><p>封面2為《<a href="../Page/暮光之城4：破曉.md" title="wikilink">吸血新世紀4：破曉傳奇上集</a>》的三位主角</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1084</p></td>
<td><p>JPM</p></td>
<td><p>黑Girl、東-{于}-哲</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1085</p></td>
<td><p>少女時代</p></td>
<td><p>江若琳</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1086</p></td>
<td><p>鍾嘉欣</p></td>
<td><p>許廷鏗</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1087</p></td>
<td><p>林峯</p></td>
<td><p>CNBLUE</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1088</p></td>
<td><p>糖妹</p></td>
<td><p>JPM</p></td>
<td><p>板野友美</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1089</p></td>
<td><p><a href="../Page/C_AllStar.md" title="wikilink">C AllStar</a></p></td>
<td><p>蔡卓妍</p></td>
<td><p> </p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1090</p></td>
<td><p>2PM</p></td>
<td><p>羅志祥</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1091</p></td>
<td><p>鍾一憲、麥貝夷</p></td>
<td><p>黑Girl、東-{于}-哲</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1092</p></td>
<td><p>鍾嘉欣</p></td>
<td><p>AKB48</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1093</p></td>
<td><p>少女時代</p></td>
<td><p>Hey!Say!Jump</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1094</p></td>
<td><p>洪卓立</p></td>
<td><p>W-inds</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1095</p></td>
<td><p>Super Junior</p></td>
<td><p>吳雨霏</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1096</p></td>
<td><p>2PM</p></td>
<td><p>AK</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1097</p></td>
<td><p>BIGBANG</p></td>
<td><p>羅志祥</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1098</p></td>
<td><p>JPM</p></td>
<td><p>BEAST</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1099</p></td>
<td><p>SHINee</p></td>
<td><p>陳妍希、陳漢典</p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

<table>
<thead>
<tr class="header">
<th><p>期數</p></th>
<th><p>封面1</p></th>
<th><p>封面2</p></th>
<th><p>封面3</p></th>
<th><p>封面4</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1100</p></td>
<td><p>羅志祥</p></td>
<td><p>林欣彤</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1101</p></td>
<td><p>2PM</p></td>
<td><p>柯震東</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1102</p></td>
<td><p>AKB48</p></td>
<td><p>School Tour</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1103</p></td>
<td><p><a href="../Page/M_Countdown.md" title="wikilink">M Countdown</a></p></td>
<td><p>陳柏宇</p></td>
<td><p>封面1為五隊南韓組合</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1104</p></td>
<td><p>少女時代</p></td>
<td><p>羅志祥</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1105</p></td>
<td><p><a href="../Page/金賢重.md" title="wikilink">金賢重</a></p></td>
<td><p>賀軍翔、陳嘉樺</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1106</p></td>
<td><p>FTIsland</p></td>
<td><p>鍾嘉欣</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1107</p></td>
<td><p><a href="../Page/One_Direction.md" title="wikilink">One Direction</a></p></td>
<td><p><a href="../Page/鍾一憲.md" title="wikilink">鍾一憲</a>、<a href="../Page/麥貝夷.md" title="wikilink">麥貝夷</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1108</p></td>
<td><p><a href="../Page/Kara.md" title="wikilink">Kara</a></p></td>
<td><p>羅志祥</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1109</p></td>
<td><p>BIGBANG</p></td>
<td><p>AKB48</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1110</p></td>
<td><p><a href="../Page/Justin_Bieber.md" title="wikilink">Justin Bieber</a></p></td>
<td><p>少女時代</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1111</p></td>
<td><p><a href="../Page/S.H.E.md" title="wikilink">S.H.E</a></p></td>
<td><p>GEM鄧紫棋</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1112</p></td>
<td><p>Super Junior</p></td>
<td><p>林峯</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1113</p></td>
<td><p><a href="../Page/Greyson_Chance.md" title="wikilink">Greyson Chance</a></p></td>
<td><p>炎亞綸</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1114</p></td>
<td><p>FTIsland</p></td>
<td><p>鍾嘉欣</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1115</p></td>
<td><p>BEAST</p></td>
<td><p>楊丞琳</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1116</p></td>
<td><p>AKB48</p></td>
<td><p>羅志祥</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1117</p></td>
<td><p>SHINee</p></td>
<td><p>吳雨霏</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1118</p></td>
<td><p>金秀賢</p></td>
<td><p>Lollipop@F</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1119</p></td>
<td><p>Taylor Swift</p></td>
<td><p>林峯</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1120</p></td>
<td><p>鍾嘉欣</p></td>
<td><p><a href="../Page/Boyfriend_(男子团体).md" title="wikilink">Boyfriend</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1121</p></td>
<td><p>GEM鄧紫棋</p></td>
<td><p>汪東城</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1122</p></td>
<td><p>少女時代</p></td>
<td><p>羅志祥</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1123</p></td>
<td><p>楊丞琳</p></td>
<td><p>T-ara</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1124</p></td>
<td><p>SHINee</p></td>
<td><p>林峯</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1125</p></td>
<td><p>BIGBANG</p></td>
<td><p>糖妹</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1126</p></td>
<td><p>少女時代</p></td>
<td><p>炎亞綸</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1127</p></td>
<td><p>鍾嘉欣</p></td>
<td><p>JPM</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1128</p></td>
<td><p>Super Junior</p></td>
<td><p>School Tour</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1129</p></td>
<td><p>SHINee</p></td>
<td><p>羅志祥</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1130</p></td>
<td><p>少女時代</p></td>
<td><p>汪東城</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1131</p></td>
<td><p>鍾嘉欣</p></td>
<td><p>炎亞綸</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1132</p></td>
<td><p>BIGBANG</p></td>
<td><p>林峯</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1133</p></td>
<td><p><a href="../Page/Super_Junior-K.R.Y..md" title="wikilink">Super Junior-K.R.Y.</a></p></td>
<td><p>羅志祥、楊丞琳</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1134</p></td>
<td><p>SHINee</p></td>
<td><p>GEM鄧紫棋</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1135</p></td>
<td><p>少女時代</p></td>
<td><p>許廷鏗</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1136</p></td>
<td><p>Taylor Swift</p></td>
<td><p>炎亞綸</p></td>
<td><p>最後一次以歐美女明星作封面人物</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1137</p></td>
<td><p>Super Junior</p></td>
<td><p>周杰倫</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1138</p></td>
<td><p>少女時代</p></td>
<td><p>JPM</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1139</p></td>
<td><p>Super Junior-M</p></td>
<td><p>Lollipop@F</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1140</p></td>
<td><p><a href="../Page/山田涼介.md" title="wikilink">山田涼介</a></p></td>
<td><p>S.H.E</p></td>
<td><p>最後一次以日本男明星作封面人物</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1141</p></td>
<td><p>Justin Bieber</p></td>
<td><p>林峯</p></td>
<td><p>最後一次以歐美男明星作封面人物</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1142</p></td>
<td><p>少女時代</p></td>
<td><p>GEM鄧紫棋</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1143</p></td>
<td><p>羅志祥</p></td>
<td><p>糖妹</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1144</p></td>
<td><p>楊丞琳</p></td>
<td><p>鍾嘉欣</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1145</p></td>
<td><p>SHINee</p></td>
<td><p>炎亞綸</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1146</p></td>
<td><p>B.A.P</p></td>
<td><p>羅志祥</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1147</p></td>
<td><p>少女時代</p></td>
<td><p>江若琳</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1148</p></td>
<td><p>Super Junior</p></td>
<td><p>羅力威、洪卓立</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1149</p></td>
<td><p>G-Dragon</p></td>
<td><p>林峯</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1150</p></td>
<td><p>INFINITE</p></td>
<td><p>GEM鄧紫棋</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1151</p></td>
<td><p>EXO</p></td>
<td><p>鍾嘉欣</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1152</p></td>
<td><p>CNBLUE</p></td>
<td><p>炎亞綸</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1153</p></td>
<td><p>少女時代</p></td>
<td><p>HKT48</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1154</p></td>
<td><p>G-Dragon</p></td>
<td><p>School Tour</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1155</p></td>
<td><p>SHINee</p></td>
<td><p>AKB48</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1156</p></td>
<td><p><a href="../Page/2PM.md" title="wikilink">2PM</a></p></td>
<td><p><a href="../Page/程予希.md" title="wikilink">程予希</a>、<a href="../Page/汪東城.md" title="wikilink">汪東城</a><br />
<a href="../Page/蔡旻佑.md" title="wikilink">蔡旻佑</a>、<a href="../Page/黃仁德.md" title="wikilink">黃仁德</a></p></td>
<td><p>封面2為劇集《<a href="../Page/原來是美男_(台灣電視劇).md" title="wikilink">原來是美男</a>》的四位主角</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1157</p></td>
<td><p>CNBLUE</p></td>
<td><p><a href="../Page/Jessica_(韓國歌手).md" title="wikilink">Jessica</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1158</p></td>
<td><p>G-Dragon</p></td>
<td><p>EXO</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1159</p></td>
<td><p><a href="../Page/銀赫.md" title="wikilink">銀赫</a>、<a href="../Page/東海_(藝人).md" title="wikilink">東海</a></p></td>
<td><p>炎亞綸</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1160</p></td>
<td><p>少女時代</p></td>
<td><p>黃鴻升</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1161</p></td>
<td><p>BIGBANG</p></td>
<td><p>羅志祥</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1162</p></td>
<td><p>Super Junior</p></td>
<td><p><a href="../Page/金鍾國.md" title="wikilink">金鍾國</a>、<a href="../Page/Haha.md" title="wikilink">Haha</a><br />
<a href="../Page/池石鎮.md" title="wikilink">池石鎮</a>、<a href="../Page/Gary_(韓國歌手).md" title="wikilink">Gary</a></p></td>
<td><p>封面2為綜藝節目《<a href="../Page/Running_Man.md" title="wikilink">Running Man</a>》的四位主持</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1163</p></td>
<td><p>SHINee</p></td>
<td><p><a href="../Page/B1A4.md" title="wikilink">B1A4</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1164</p></td>
<td><p>EXO</p></td>
<td><p>鍾嘉欣</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1165</p></td>
<td><p>Super Junior</p></td>
<td><p><a href="../Page/After_School.md" title="wikilink">After School</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1166</p></td>
<td><p><a href="../Page/劉憲華.md" title="wikilink">Henry</a></p></td>
<td><p>Henry本期佔有兩個封面</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1167</p></td>
<td><p>少女時代</p></td>
<td><p>INFINITE</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1168</p></td>
<td><p><a href="../Page/f(x)_(組合).md" title="wikilink">f(x)</a></p></td>
<td><p><a href="../Page/金秀賢.md" title="wikilink">金秀賢</a></p></td>
<td><p>黃鴻升</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1169</p></td>
<td><p>EXO</p></td>
<td><p><a href="../Page/B.A.P.md" title="wikilink">B.A.P</a></p></td>
<td><p>楊丞琳</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1170</p></td>
<td><p>Super Junior</p></td>
<td><p>T-ara</p></td>
<td><p> </p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1171</p></td>
<td><p><a href="../Page/起範.md" title="wikilink">起範</a></p></td>
<td><p>INFINITE</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1172</p></td>
<td><p>G-Dragon</p></td>
<td><p>Jessica</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1173</p></td>
<td><p>EXO</p></td>
<td><p><a href="../Page/BEAST.md" title="wikilink">BEAST</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1174</p></td>
<td><p>少女時代</p></td>
<td><p><a href="../Page/張惠雅.md" title="wikilink">Regan</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1175</p></td>
<td><p>G-Dragon</p></td>
<td><p>Super Junior</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1176</p></td>
<td><p>SHINee</p></td>
<td><p><a href="../Page/宋智孝.md" title="wikilink">宋智孝</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1177</p></td>
<td><p>少女時代</p></td>
<td><p><a href="../Page/崔真理.md" title="wikilink">Sulli</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1178</p></td>
<td><p>羅志祥</p></td>
<td><p>AKB48</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1179</p></td>
<td><p>SHINee</p></td>
<td><p>EXO</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1180</p></td>
<td><p>Jessica、<a href="../Page/鄭秀晶.md" title="wikilink">Krystal</a></p></td>
<td><p>林峯</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1181</p></td>
<td><p>B.A.P</p></td>
<td><p>黃鴻升</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1182</p></td>
<td><p><a href="../Page/Super_Junior-M.md" title="wikilink">Super Junior-M</a></p></td>
<td><p>羅志祥</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1183</p></td>
<td><p><a href="../Page/朴信惠.md" title="wikilink">朴信惠</a>、<a href="../Page/李敏鎬.md" title="wikilink">李敏鎬</a></p></td>
<td><p>少女時代</p></td>
<td><p>封面1為劇集《<a href="../Page/繼承者們.md" title="wikilink">繼承者們</a>》的兩位主角<br />
少女時代本期佔有三個封面</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1184</p></td>
<td><p>G-Dragon</p></td>
<td><p>宋智孝</p></td>
<td><p> </p></td>
<td><p> </p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1185</p></td>
<td><p>EXO</p></td>
<td><p><a href="../Page/Crayon_Pop.md" title="wikilink">Crayon Pop</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1186</p></td>
<td><p>少女時代</p></td>
<td><p>金鍾國</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1187</p></td>
<td><p>BIGBANG</p></td>
<td><p><a href="../Page/李鍾碩.md" title="wikilink">李鍾碩</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1188</p></td>
<td><p>Super Junior-M</p></td>
<td><p>EXO</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1189</p></td>
<td><p>AKB48</p></td>
<td><p>Gary</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1190</p></td>
<td><p>SHINee</p></td>
<td><p><a href="../Page/Miss_A.md" title="wikilink">Miss A</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1191</p></td>
<td><p>G-Dragon</p></td>
<td><p>INFINITE</p></td>
<td><p>FTIsland</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1192</p></td>
<td><p>CNBLUE</p></td>
<td><p>B1A4</p></td>
<td><p>朴信惠</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1193</p></td>
<td><p><a href="../Page/SISTAR.md" title="wikilink">SISTAR</a></p></td>
<td><p>Gary</p></td>
<td><p> </p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1194</p></td>
<td><p><a href="../Page/吳千語.md" title="wikilink">吳千語</a>、<a href="../Page/林德信.md" title="wikilink">林德信</a></p></td>
<td><p><a href="../Page/吳若希.md" title="wikilink">吳若希</a>、<a href="../Page/鄭欣宜.md" title="wikilink">鄭欣宜</a><br />
<a href="../Page/胡鴻鈞.md" title="wikilink">胡鴻鈞</a>、<a href="../Page/許廷鏗.md" title="wikilink">許廷鏗</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1195</p></td>
<td><p>糖妹</p></td>
<td><p>宋智孝</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1196</p></td>
<td><p>銀赫、東海</p></td>
<td><p><a href="../Page/4Minute.md" title="wikilink">4Minute</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1197</p></td>
<td><p>金秀賢、全智賢</p></td>
<td><p>羅志祥</p></td>
<td><p>封面1為劇集《<a href="../Page/來自星星的你.md" title="wikilink">來自星星的你</a>》的兩位主角</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1198</p></td>
<td><p>少女時代</p></td>
<td><p><a href="../Page/金宇彬.md" title="wikilink">金宇彬</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1199</p></td>
<td><p>EXO</p></td>
<td><p><a href="../Page/2NE1.md" title="wikilink">2NE1</a></p></td>
<td><p>EXO本期佔有兩個封面</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

<table>
<thead>
<tr class="header">
<th><p>期數</p></th>
<th><p>封面1</p></th>
<th><p>封面2</p></th>
<th><p>封面3</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1200</p></td>
<td><p>少女時代</p></td>
<td><p>Hero籃球隊</p></td>
<td><p> </p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1201</p></td>
<td><p>金秀賢</p></td>
<td><p>G-Dragon</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1202</p></td>
<td><p>Super Junior-M</p></td>
<td><p>宋智孝</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1203</p></td>
<td><p>李敏鎬</p></td>
<td><p>李鍾碩</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1204</p></td>
<td><p><a href="../Page/EXO.md" title="wikilink">EXO</a> [8][9]</p></td>
<td><p>朴信惠</p></td>
<td><p>EXO本期佔有兩個封面</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1205</p></td>
<td><p><a href="../Page/CNBLUE.md" title="wikilink">CNBLUE</a></p></td>
<td><p><a href="../Page/Toheart.md" title="wikilink">Toheart</a></p></td>
<td><p> </p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1206</p></td>
<td><p>金秀賢</p></td>
<td><p>EXO</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1207</p></td>
<td><p>少女時代</p></td>
<td><p><a href="../Page/AKB48.md" title="wikilink">AKB48</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1208</p></td>
<td><p><a href="../Page/SHINee.md" title="wikilink">SHINee</a>、f(x)</p></td>
<td><p>李鍾碩</p></td>
<td><p>Lollipop@F</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1209</p></td>
<td><p><a href="../Page/INFINITE.md" title="wikilink">INFINITE</a></p></td>
<td><p>EXO</p></td>
<td><p> </p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1210</p></td>
<td><p>CNBLUE</p></td>
<td><p>宋智孝</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1211</p></td>
<td><p>EXO</p></td>
<td><p>2NE1</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1212</p></td>
<td><p>金秀賢</p></td>
<td><p><a href="../Page/炎亞綸.md" title="wikilink">炎亞綸</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1213</p></td>
<td><p>少女時代</p></td>
<td><p><a href="../Page/Gary_(韓國歌手).md" title="wikilink">Gary</a></p></td>
<td><p>汪東城</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1214</p></td>
<td><p>B.A.P</p></td>
<td><p>李鍾碩、<a href="../Page/朴海鎮.md" title="wikilink">朴海鎮</a><br />
<a href="../Page/陳世娫.md" title="wikilink">陳世娫</a>、<a href="../Page/姜素拉.md" title="wikilink">姜素拉</a></p></td>
<td><p> </p></td>
<td><p>封面2為劇集《<a href="../Page/異鄉人醫生.md" title="wikilink">異鄉人醫生</a>》的四位主角</p></td>
</tr>
<tr class="even">
<td><p>1215</p></td>
<td><p><a href="../Page/GOT7.md" title="wikilink">GOT7</a></p></td>
<td><p>BEAST</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1216</p></td>
<td><p>李敏鎬</p></td>
<td><p><a href="../Page/EXO-K.md" title="wikilink">EXO-K</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1217</p></td>
<td><p>f(x)</p></td>
<td><p>Super Junior-M</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1218</p></td>
<td><p>少女時代</p></td>
<td><p>Lollipop@F</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1219</p></td>
<td><p><a href="../Page/劉憲華.md" title="wikilink">Henry</a></p></td>
<td><p><a href="../Page/板野友美.md" title="wikilink">板野友美</a></p></td>
<td><p><a href="../Page/江若琳.md" title="wikilink">江若琳</a></p></td>
<td><p>實體版最後一期</p></td>
</tr>
</tbody>
</table>

## 主要欄目

### 前期

  - Yes學生報
  - 城市驚喜／Camera Face
  - 自學心理分析／A-Level心理考試／星座備忘錄／星星會明白你心
  - 少年古域的秘密日記／光脫脫漫畫（梁榮淳；SEJU Cultural Co. Ltd.）／Loving Q（徐廷仁；DAI WON
    Publishing Co. Ltd.）／Miss Sex漫畫（李勉之）
  - Miss Sex熱線／戀愛& Sex／實用性教育
  - 寫出個性／誘畫年代／愛情ER
  - 情書（蔡康年）／戀愛教室
  - 眾裡尋他／她／Ecchange Square
  - 隨意美

### 自962期以後

  - 〈yes\!\!〉'stuff
  - Anicom09
      - [G.E.M.](../Page/鄧紫棋.md "wikilink")，[HotCha專輯卡](../Page/HotCha.md "wikilink")
      - reader
      - just for you
      - pic it out
      - winning seven
      - card express
  - cover story
      - 封面故事
  - g shock
      - 城市驚喜
  - school star
  - idol live Messenger
  - idol buzz
  - shinig star
  - hot dog Taiwan
  - hot dog Korea
  - hot dog Japan
  - hot dog China
  - coming movie
  - hot drama
  - TV buddy
  - west live
  - star runner
      - 這個欄目毎個月由不同偶像負責
  - song
  - fashion
  - special
  - brand story
  - stop feature
  - my cheapy things
  - make up
  - back to school
  - web hot hit
  - yummy yummy
  - thriller
  - love can be.... （愛情單元故事、**讀者投稿**）

## 附送贈品

| 期數  | 附送贈品            | 款式                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| --- | --------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 950 | 偶像Meno Pad      | [HotCha](../Page/HotCha.md "wikilink")、[S.H.E](../Page/S.H.E.md "wikilink")、[G.E.M.](../Page/鄧紫棋.md "wikilink")、[蔡卓妍](../Page/蔡卓妍.md "wikilink")、[傅穎](../Page/傅穎.md "wikilink")、[江若琳](../Page/江若琳.md "wikilink")                                                                                                                                                                                                                                          |
| 951 | 偶像Postcard Book | [G.E.M.](../Page/鄧紫棋.md "wikilink")、[HotCha](../Page/HotCha.md "wikilink")、[林峯](../Page/林峯.md "wikilink")、[Hey\! Say\! JUMP](../Page/Hey!_Say!_JUMP.md "wikilink")                                                                                                                                                                                                                                                                                        |
| 952 | 偶像4R相簿          | [HotCha](../Page/HotCha.md "wikilink")、[S.H.E](../Page/S.H.E.md "wikilink")、[蔡卓妍](../Page/蔡卓妍.md "wikilink")、[張敬軒](../Page/張敬軒.md "wikilink")、[陳偉霆](../Page/陳偉霆.md "wikilink")、[G.E.M.](../Page/鄧紫棋.md "wikilink")                                                                                                                                                                                                                                        |
| 953 | 偶像A4文件夾         | [蔡卓妍](../Page/蔡卓妍.md "wikilink")、[G.E.M.](../Page/鄧紫棋.md "wikilink")、[HotCha](../Page/HotCha.md "wikilink")、[林峯](../Page/林峯.md "wikilink")、[洪卓立](../Page/洪卓立.md "wikilink")、[By2](../Page/BY2.md "wikilink")                                                                                                                                                                                                                                              |
| 954 | 偶像滑鼠墊           | [蔡卓妍](../Page/蔡卓妍.md "wikilink")、[鄧麗欣](../Page/鄧麗欣.md "wikilink")、[HotCha](../Page/HotCha.md "wikilink")、[張敬軒](../Page/張敬軒.md "wikilink")、[周柏豪](../Page/周柏豪.md "wikilink")、[G.E.M.](../Page/鄧紫棋.md "wikilink")                                                                                                                                                                                                                                            |
| 955 | 偶像乘涼小膠扇         | [蔡卓妍](../Page/蔡卓妍.md "wikilink")、[江若琳](../Page/江若琳.md "wikilink")、[洪卓立](../Page/洪卓立.md "wikilink")、[鄧麗欣](../Page/鄧麗欣.md "wikilink")、[林峯](../Page/林峯.md "wikilink")、[張敬軒](../Page/張敬軒.md "wikilink")、[G.E.M.](../Page/鄧紫棋.md "wikilink")、[HotCha](../Page/HotCha.md "wikilink")                                                                                                                                                                            |
| 956 | 偶像杯墊            | [張敬軒](../Page/張敬軒.md "wikilink")、[HotCha](../Page/HotCha.md "wikilink")、[By2](../Page/BY2.md "wikilink")、[林峯](../Page/林峯.md "wikilink")、[鄧麗欣](../Page/鄧麗欣.md "wikilink")、[鬼鬼](../Page/吳映潔.md "wikilink")                                                                                                                                                                                                                                                  |
| 957 | 偶像相架            | [G.E.M.](../Page/鄧紫棋.md "wikilink")、[鄧麗欣](../Page/鄧麗欣.md "wikilink")、[By2](../Page/BY2.md "wikilink")、[蔡卓妍](../Page/蔡卓妍.md "wikilink")、[衛蘭](../Page/衛蘭.md "wikilink")、[飛輪海](../Page/飛輪海.md "wikilink")                                                                                                                                                                                                                                                    |
| 958 | 動漫節攻略手冊         | 動漫節攻略手冊                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| 959 | 偶像4R相           | [江若琳](../Page/江若琳.md "wikilink")、[陳柏宇](../Page/陳柏宇.md "wikilink")、[鬼鬼](../Page/吳映潔.md "wikilink")、[薛凱琪](../Page/薛凱琪.md "wikilink")、[Lollipop棒棒堂](../Page/Lollipop棒棒堂.md "wikilink")、[衛蘭](../Page/衛蘭.md "wikilink")                                                                                                                                                                                                                                        |
| 960 | 偶像海報            | [韓版花樣男子](../Page/韓版花樣男子.md "wikilink")、[SS501](../Page/SS501.md "wikilink")、[Lollipop棒棒堂](../Page/Lollipop棒棒堂.md "wikilink")、[飛輪海](../Page/飛輪海.md "wikilink")、[S.H.E](../Page/S.H.E.md "wikilink")、[Super Junior](../Page/Super_Junior.md "wikilink")                                                                                                                                                                                                     |
| 961 | 偶像4R相簿          | [超克7](../Page/超克7.md "wikilink")、[By2](../Page/BY2.md "wikilink")、[鬼鬼](../Page/吳映潔.md "wikilink")、[林依晨](../Page/林依晨.md "wikilink")、[洪卓立](../Page/洪卓立.md "wikilink")、[AngelaBaby](../Page/AngelaBaby.md "wikilink")                                                                                                                                                                                                                                        |
| 962 | 偶像長間尺           | [G.E.M.](../Page/鄧紫棋.md "wikilink")、[HotCha](../Page/HotCha.md "wikilink")、[鍾嘉欣](../Page/鍾嘉欣.md "wikilink")、[洪卓立](../Page/洪卓立.md "wikilink")、[By2](../Page/BY2.md "wikilink")、[鄧麗欣](../Page/鄧麗欣.md "wikilink")、[Lollipop棒棒堂](../Page/Lollipop棒棒堂.md "wikilink")、[張敬軒](../Page/張敬軒.md "wikilink")、[Hey Say Jump](../Page/Hey_Say_Jump.md "wikilink")、[鬼鬼](../Page/吳映潔.md "wikilink")、[S.H.E](../Page/S.H.E.md "wikilink")、[狄易達](../Page/狄易達.md "wikilink") |
| 963 | 偶像筆袋            | [超克7](../Page/超克7.md "wikilink")、[By2](../Page/BY2.md "wikilink")、[鬼鬼](../Page/吳映潔.md "wikilink")、[張敬軒](../Page/張敬軒.md "wikilink")、[江若琳](../Page/江若琳.md "wikilink")、[林峯](../Page/林峯.md "wikilink")                                                                                                                                                                                                                                                        |
| 964 | 偶像書籤            | [飛輪海](../Page/飛輪海.md "wikilink")、[棒棒堂](../Page/棒棒堂.md "wikilink")、[Super Junior-M](../Page/Super_Junior-M.md "wikilink")、[林依晨](../Page/林依晨.md "wikilink")、[By2](../Page/BY2.md "wikilink")、[AngelaBaby](../Page/AngelaBaby.md "wikilink")、[江若琳](../Page/江若琳.md "wikilink")、[蔡卓妍](../Page/蔡卓妍.md "wikilink")、[SS501](../Page/SS501.md "wikilink")、[S.H.E](../Page/S.H.E.md "wikilink")                                                                     |
| 965 | 偶像文件座           | [鄧麗欣](../Page/鄧麗欣.md "wikilink")、[By2](../Page/BY2.md "wikilink")、[G.E.M.](../Page/鄧紫棋.md "wikilink")、[飛輪海](../Page/飛輪海.md "wikilink")、[Lollipop棒棒堂](../Page/Lollipop棒棒堂.md "wikilink")、[S.H.E](../Page/S.H.E.md "wikilink")                                                                                                                                                                                                                              |
| 966 | 偶像A4 文件夾        | [鄧麗欣](../Page/鄧麗欣.md "wikilink")、[江若琳](../Page/江若琳.md "wikilink")、[Choc7](../Page/Choc7.md "wikilink")、[林峯](../Page/林峯.md "wikilink")、[洪卓立](../Page/洪卓立.md "wikilink")、[S.H.E](../Page/S.H.E.md "wikilink")                                                                                                                                                                                                                                               |
| 967 | 偶像記事簿           | [林峯](../Page/林峯.md "wikilink")、[鬼鬼](../Page/吳映潔.md "wikilink")、[G.E.M.](../Page/鄧紫棋.md "wikilink")、[By2](../Page/BY2.md "wikilink")                                                                                                                                                                                                                                                                                                                       |
| 968 | 偶像原子筆           | [G.E.M.](../Page/鄧紫棋.md "wikilink")、[HotCha](../Page/HotCha.md "wikilink")、[林峯](../Page/林峯.md "wikilink")、[蔡卓妍](../Page/蔡卓妍.md "wikilink")、[鄧麗欣](../Page/鄧麗欣.md "wikilink")、[飛輪海](../Page/飛輪海.md "wikilink")                                                                                                                                                                                                                                              |
| 969 | 偶像4R相           | [鍾嘉欣](../Page/鍾嘉欣.md "wikilink")、[Hey Say Jump](../Page/Hey_Say_Jump.md "wikilink")、[AngelaBaby](../Page/AngelaBaby.md "wikilink")、[江若琳](../Page/江若琳.md "wikilink")、[陳偉霆](../Page/陳偉霆.md "wikilink")、[Janice Man](../Page/Janice_Man.md "wikilink")                                                                                                                                                                                                       |
| 970 | 偶像門把吊牌          | [G.E.M.](../Page/鄧紫棋.md "wikilink")、鄧麗欣、飛輪海、洪卓立、S.H.E、[By2](../Page/BY2.md "wikilink")                                                                                                                                                                                                                                                                                                                                                                    |
| 971 | 偶像迷你紙相簿         | [林峯](../Page/林峯.md "wikilink")、[HotCha](../Page/HotCha.md "wikilink")、鬼鬼、洪卓立、SS501、Super Junior                                                                                                                                                                                                                                                                                                                                                           |
| 972 | 偶像散紙包           | [G.E.M.](../Page/鄧紫棋.md "wikilink")、鄧麗欣、[HotCha](../Page/HotCha.md "wikilink")、蔡卓妍、張敬軒、韓版花樣男子                                                                                                                                                                                                                                                                                                                                                             |
| 973 | 偶像八達通套          | [G.E.M.](../Page/鄧紫棋.md "wikilink")、[HotCha](../Page/HotCha.md "wikilink")、鍾嘉欣、江若琳、張敬軒、Lollipop棒棒堂                                                                                                                                                                                                                                                                                                                                                        |
| 974 | 偶像海報            | [鄧麗欣](../Page/鄧麗欣.md "wikilink")&[方力申](../Page/方力申.md "wikilink")、[江若琳](../Page/江若琳.md "wikilink")、[鍾嘉欣](../Page/鍾嘉欣.md "wikilink")、Twins（[蔡卓妍](../Page/蔡卓妍.md "wikilink")&[鍾欣桐](../Page/鍾欣桐.md "wikilink")）                                                                                                                                                                                                                                              |

  - 資料源自《Yes\!》網頁
  - 自962期開始，每期送出2款偶像親筆簽名相，名額2個。

## 雜誌曾舉辦之較大型選舉

  - 全港校花校草選舉
  - 2005年[東森電視國際模特兒大賽香港區選拔賽](../Page/東森電視.md "wikilink")
  - 微博 X YES！校花校草 2018

## 電影投資

  - 《一夜再成名》
  - 《潛龍追擊》
  - 《[辣警霸王花](../Page/辣警霸王花.md "wikilink")》
  - 《[不義之戰](../Page/不義之戰.md "wikilink")》

## 演唱會

  - Yes25週年演唱會（2016年）
  - Yes rock大混戰音樂節（2017年）
  - YES\! 林明禎2017 \#Me & U 派對香港限定（2017年）

## 附屬產品、商店

  - 《Yes idol》明星寫真專刊，每半月出版。
  - 《[Yes\!Card](../Page/Yes!Card.md "wikilink")》明星卡
  - 《Yes
    Station》全盛時期約有9間（香港8間、澳門1間；連同香港和廣州之特許專賣店計共26間）主要是售賣雜誌內介紹之精品、換領相片等業務
  - 《星願事務所》（Legend Production Ltd.），與1St Case Model
    Agency合作的娛樂事業，最近力捧的新人有：小龍鳳
  - 《寫真集「Miko 純。潔琪」》2018 推出馬來西亞新女神黃潔琪MikoWong個人首本寫真《Miko 純 ‧ 潔琪》

## 參見

## 相關連結

  - [《Yes\!》雜誌主頁](http://www.yesmagazine.com.hk)
  - [《Yes\!》雜誌官方討論區](https://web.archive.org/web/20090430220601/http://forum.yesmagazine.com.hk/)
  - [《Yes\!》Facebook](https://www.facebook.com/Yescomhk/)
  - [《Yes\!》新浪微博](https://www.weibo.com/yesmagazinehk)
  - [《Yes\!》Instagram](https://www.instagram.com/yesculture/)

[從新一代的青少年雜誌看青少年的文化特色](https://web.archive.org/web/20070930093936/http://www.swik.org.hk/SWIKPortal/DesktopDefault.aspx?tabIndex=0&tabid=50&ItemID=263)

  - [劉錫明為什麼叫「毒瘤明」？](http://joenieh.blogspot.com/2006/06/blog-post.html)

[Category:香港雜誌](../Category/香港雜誌.md "wikilink")
[Category:香港已停刊雜誌](../Category/香港已停刊雜誌.md "wikilink")
[Category:香港流行文化](../Category/香港流行文化.md "wikilink")
[Category:1990年創辦的雜誌](../Category/1990年創辦的雜誌.md "wikilink")
[Category:2014年停刊的雜誌](../Category/2014年停刊的雜誌.md "wikilink")
[Category:1990年香港建立](../Category/1990年香港建立.md "wikilink")
[Category:2014年香港廢除](../Category/2014年香港廢除.md "wikilink")

1.

2.
3.  [Yes\! Facebook專頁
    公告](http://zh-tw.facebook.com/permalink.php?story_fbid=796292463736053&id=536103449754957)

4.

5.

6.

7.

8.  [1204期EXO封面1](http://www.quick-china.com/mag/thumb/l/zz00478l.jpg)

9.  [1204期EXO封面2](http://www.quick-china.com/mag/thumb/l/zz00479l.jpg)