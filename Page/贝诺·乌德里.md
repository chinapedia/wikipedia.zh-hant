**贝诺·乌德里**（，），[斯洛文尼亚职业](../Page/斯洛文尼亚.md "wikilink")[篮球运动员](../Page/篮球.md "wikilink")，场上司职[组织后卫](../Page/组织后卫.md "wikilink")，现效力于[NBA](../Page/NBA.md "wikilink")[底特律活塞](../Page/底特律活塞.md "wikilink")。

這樁交易熱火送出查尔默斯(Mario Chalmers)與前鋒James
Ennis，換來灰熊後衛[貝諾·烏德里](../Page/貝諾·烏德里.md "wikilink")(Beno
Udrih)與前鋒Jarnell Stokes。

## NBA生涯统计

|   |                                                  |
| - | ------------------------------------------------ |
| † | 表示賽季中乌德里贏得[NBA總冠軍](../Page/NBA總冠軍.md "wikilink") |

### 例行賽

|- | style="text-align:left;background:\#afe6ba;" | † | align="left" |
[聖安東尼奧馬刺](../Page/聖安東尼奧馬刺.md "wikilink") | **80** || 2 || 14.4 || .444
|| .408 || .753 || 1.0 || 1.9 || .5 || .1 || 5.9 |- | align="left" |  |
align="left" | [聖安東尼奧馬刺](../Page/聖安東尼奧馬刺.md "wikilink") | 55 || 3 ||
10.7 || .455 || .343 || .780 || 1.0 || 1.7 || .3 || .0 || 5.1 |- |
style="text-align:left;background:\#afe6ba;" | † | align="left" |
[聖安東尼奧馬刺](../Page/聖安東尼奧馬刺.md "wikilink") | 73 || 1 || 13.0
|| .369 || .287 || .883 || 1.1 || 1.7 || .4 || .0 || 4.7 |- |
align="left" |  | align="left" | [沙加緬度國王](../Page/沙加緬度國王.md "wikilink")
| 65 || 51 || 32.0 || .463 || .387 || .850 || 3.3 || 4.3 || .9 || **.2**
|| 12.8 |- | align="left" |  | align="left" |
[沙加緬度國王](../Page/沙加緬度國王.md "wikilink") | 73 ||
**72** || 31.1 || .461 || .310 || .820 || 3.0 || 4.7 || 1.1 || **.2** ||
11.0 |- | align="left" |  | align="left" |
[沙加緬度國王](../Page/沙加緬度國王.md "wikilink") | 79 ||
41 || 31.4 || .493 || .377 || .837 || 2.8 || 4.7 || 1.1 || .1 || 12.9 |-
| align="left" |  | align="left" |
[沙加緬度國王](../Page/沙加緬度國王.md "wikilink") | 79 ||
64 || **34.6** || **.500** || .357 || .864 || **3.4** || 4.9 || **1.2**
|| .1 || **13.7** |- | align="left" |  | align="left" |
[密爾瓦基公鹿](../Page/密爾瓦基公鹿.md "wikilink") | 59 || 0 || 18.3 ||
.440 || .288 || .709 || 1.7 || 3.8 || .6 || .0 || 5.9 |- | align="left"
|  | align="left" | [密爾瓦基公鹿](../Page/密爾瓦基公鹿.md "wikilink") | 39 || 0 ||
18.4 || .475 || .265 || .727 || 2.0 || 3.5 || .4 || .1 || 6.7 |- |
align="left" |  | align="left" | [奧蘭多魔術](../Page/奧蘭多魔術.md "wikilink") |
27 || 9 || 27.3 || .408 || .396 || .857 || 2.3 || **6.1** || .9 || .0 ||
10.2 |- | style="text-align:left;"|  | style="text-align:left;"|
[紐約尼克](../Page/紐約尼克.md "wikilink") | 31 || 12 || 19.0 || .425 ||
.425 || .833 || 1.8 || 3.5 || .7 || .1 || 5.6 |- |
style="text-align:left;"|  | style="text-align:left;"|
[曼斐斯灰熊](../Page/曼斐斯灰熊.md "wikilink") | 10 || 0 || 5.5 ||
.556 || **1.000** || .833 || 0.2 || 0.6 || .1 || .1 || 2.7 |- |
style="text-align:left;"|  | style="text-align:left;"|
[曼斐斯灰熊](../Page/曼斐斯灰熊.md "wikilink") | 79 || 12 || 18.9 ||
.487 || .268 || .853 || 1.8 || 2.6 || .6 || .1 || 7.7 |- |
style="text-align:left;"|  | style="text-align:left;"|
[曼斐斯灰熊](../Page/曼斐斯灰熊.md "wikilink") | 8 || 0 || 15.0 ||
.435 || .364 || **1.000** || 1.1 || 3.3 || .4 || .1 || 5.9 |-
class="sortbottom" | style="text-align:left;"| 生涯 |
style="text-align:left;"| | 756 || 267 || 22.5 || .463 || .349 || .829
|| 2.1 || 3.5 || .7 || .1 || 8.7

### 季後賽

|- | style="text-align:left;background:\#afe6ba;" |
[2005](../Page/2005年NBA季後賽.md "wikilink")† | align="left" |
[聖安東尼奧馬刺](../Page/聖安東尼奧馬刺.md "wikilink") | **21** || 0 ||
11.5 || .359 || .270 || .857 || .8 || 1.0 || .4 || .0 || 3.7 |- |
align="left" | [2006](../Page/2006年NBA季後賽.md "wikilink") | align="left"
| [聖安東尼奧馬刺](../Page/聖安東尼奧馬刺.md "wikilink") | 7 || 0 || 6.7 || .333 ||
.167 || .800 || .6 || 1.1 || .0 || .0 || 3.6 |- |
style="text-align:left;background:\#afe6ba;" |
[2007](../Page/2007年NBA季後賽.md "wikilink")† | align="left" |
[聖安東尼奧馬刺](../Page/聖安東尼奧馬刺.md "wikilink") | 8 || 0 || 2.5 || .000
|| .000 || **1.000** || .1 || .1 || .0 || .0 || .3 |- | align="left" |
[2014](../Page/2014年NBA季後賽.md "wikilink") | align="left" |
[曼斐斯灰熊](../Page/曼斐斯灰熊.md "wikilink") | 7 || 0 || 16.4 ||
**.467** || **.333** || .692 || 1.7|| 1.7 || .4 || .0 || **7.9** |- |
align="left" | [2015](../Page/2015年NBA季後賽.md "wikilink") | align="left"
| [曼斐斯灰熊](../Page/曼斐斯灰熊.md "wikilink") | 10 || 0 || **17.5** || .425 ||
.250'|| .833 || **2.0** || **2.1** || **.5** || .0 || 7.6 |-
class="sortbottom" | style="text-align:left;"| 生涯 |
style="text-align:left;"| | 43 || 0 || 11.3 || .388 || .260 || .804 ||
1.0 || 1.2 || .3 || .0 || 4.5

## 參考资料

[Category:斯洛文尼亚男子篮球运动员](../Category/斯洛文尼亚男子篮球运动员.md "wikilink")
[Category:圣安东尼奥马刺队球员](../Category/圣安东尼奥马刺队球员.md "wikilink")
[Category:萨克拉门托国王队球员](../Category/萨克拉门托国王队球员.md "wikilink")
[Category:邁阿密熱火隊球員](../Category/邁阿密熱火隊球員.md "wikilink")
[Category:密尔沃基雄鹿隊球員](../Category/密尔沃基雄鹿隊球員.md "wikilink")
[Category:奧蘭多魔術隊球員](../Category/奧蘭多魔術隊球員.md "wikilink")
[Category:孟菲斯灰熊队球员](../Category/孟菲斯灰熊队球员.md "wikilink")
[Category:纽约尼克斯队球员](../Category/纽约尼克斯队球员.md "wikilink")
[Category:底特律活塞队球员](../Category/底特律活塞队球员.md "wikilink")