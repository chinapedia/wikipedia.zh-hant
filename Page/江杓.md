**江杓**（），號**星初**，[上海市人](../Page/上海市.md "wikilink")，中華民國政治人物。早年在[天津就讀小學](../Page/天津.md "wikilink"),旋入[上海浦東中學](../Page/上海.md "wikilink")。1919年[五四運動時](../Page/五四運動.md "wikilink")，擔任學校代表，爲上海學生會評議部評議員。浦東中學畢業後,赴德國留學。四年後以資罄輟學返國,入[瀋陽大冶工廠](../Page/瀋陽.md "wikilink"),初任秘書,後升工程師。再赴德國求學，畢業後，考得德國特證工程師。1954年擔任[經濟部部長](../Page/經濟部.md "wikilink")、[行政院政務委員等官職](../Page/行政院.md "wikilink")，\[1\]\[2\]於1981年2月3日病逝於臺北，享年81歲。\[3\]\[4\]

## 參考資料

[category:上海人](../Page/category:上海人.md "wikilink")
[Shao杓](../Page/category:江姓.md "wikilink")

[Category:中華民國總統府國策顧問](../Category/中華民國總統府國策顧問.md "wikilink")
[Category:行政院政務委員](../Category/行政院政務委員.md "wikilink")
[Category:中華民國經濟部部長](../Category/中華民國經濟部部長.md "wikilink")
[Category:中華民國工程師](../Category/中華民國工程師.md "wikilink")
[Category:台灣戰後上海移民](../Category/台灣戰後上海移民.md "wikilink")
[Category:柏林工業大學校友](../Category/柏林工業大學校友.md "wikilink")
[Category:中華民國國防部次長](../Category/中華民國國防部次長.md "wikilink")

1.  《[俞大維傳](../Page/俞大維.md "wikilink")》.
    作者：[李元平](../Page/李元平.md "wikilink");
    出版社：[臺灣日報社](../Page/臺灣日報.md "wikilink");
    出版日：1992年01月05日，392 頁;
    ，[01](http://books.google.com.tw/books/about/%E4%BF%9E%E5%A4%A7%E7%B6%AD%E5%82%B3.html?id=PGHemgEACAAJ&redir_esc=y)，[02](http://books.google.com.tw/books/about/%E4%BF%9E%E5%A4%A7%E7%B6%AD%E5%82%B3.html?id=VqxoAAAAIAAJ&redir_esc=y)，[增訂版](http://www.lib.hcu.edu.tw/Webpac2/store.dll/?ID=24348&T=0)

2.  《江杓談召琳免刑　卡車二十二輛退還建國公司》，[存檔](http://contentdm.lib.nccu.edu.tw/cdm/ref/collection/38clip/id/11782)，上海：[大公報](../Page/大公報.md "wikilink")，1947-06-11
3.  《民國人物小傳(83)--[江杓](http://readopac1.ncl.edu.tw/nclserialFront/search/search_result.jsp?la=ch&relate=XXX&dtdId=000040&search_index=all&search_value=%E6%B1%9F%E6%9D%93%24&search_mode=#)、[嚴慶齡](../Page/嚴慶齡.md "wikilink")、[趙聚鈺](../Page/趙聚鈺.md "wikilink")、[汪大夑](../Page/汪大夑.md "wikilink")、[包天笑](../Page/包天笑.md "wikilink")、[蕭耀南](../Page/蕭耀南.md "wikilink")、[胡伯翰](../Page/胡伯翰.md "wikilink")、[戴戟](../Page/戴戟.md "wikilink")、[張之江](../Page/張之江.md "wikilink")、[劉文輝](../Page/劉文輝.md "wikilink")》，[劉紹唐主編](../Page/劉紹唐.md "wikilink")，傳記文學，39:2，1981年8月（民70.08），頁142-148，，系統識別號「A81061704」，\[<http://readopac1.ncl.edu.tw/nclserialFront/search/detail.jsp?sysId=0005121220&dtdId=000040&search_type=detail&la=ch&checked=&unchecked=0010005120623,0020005121220,0030006153698,0040006384549>,
    NCL\]，[國家圖書館](../Page/國家圖書館.md "wikilink")
4.  《追懷江杓(星初)先生》，[孫希宗](../Page/孫希宗.md "wikilink")，傳記文學，39:5，1981年11月（民70.11），頁26-27，系統識別號「A81061106」，\[<http://readopac1.ncl.edu.tw/nclserialFront/search/detail.jsp?sysId=0005120623&dtdId=000040&search_type=detail&la=ch&checked=&unchecked=0010005120623,0020005121220,0030006153698,0040006384549>,
    NCL\]，[國家圖書館](../Page/國家圖書館.md "wikilink")