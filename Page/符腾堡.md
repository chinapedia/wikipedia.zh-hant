**符腾堡**（**Württemberg**），是一个历史地名，位于[德国西南部的](../Page/德国.md "wikilink")[施瓦本](../Page/施瓦本.md "wikilink")，今天[巴登-符腾堡州的一部分](../Page/巴登-符腾堡.md "wikilink")。东边与[巴伐利亚接壤](../Page/巴伐利亚.md "wikilink")，南边与[奥地利](../Page/奥地利.md "wikilink")，[瑞士共享](../Page/瑞士.md "wikilink")[博登湖](../Page/博登湖.md "wikilink")，西部与[巴登](../Page/巴登.md "wikilink")、[霍亨索伦](../Page/霍亨索伦.md "wikilink")，北边又与[普法尔茨](../Page/普法尔茨.md "wikilink")，[黑森为邻](../Page/黑森.md "wikilink")。历史上很长一段时间，符腾堡首都在[斯图加特](../Page/斯图加特.md "wikilink")，其他时间，君主们则选择[路德维希堡或](../Page/路德维希堡.md "wikilink")[乌拉赫为首都](../Page/乌拉赫.md "wikilink")。符腾堡得名于[斯图加特一座叫Wirtemberg的山](../Page/斯图加特.md "wikilink")。

1143年，符腾堡成为[神圣罗马帝国的一块](../Page/神圣罗马帝国.md "wikilink")[伯爵领地](../Page/伯爵.md "wikilink")。1419年分为[乌拉赫和](../Page/乌拉赫.md "wikilink")[斯图加特两支](../Page/斯图加特.md "wikilink")。1495年成为[公国](../Page/符腾堡公国.md "wikilink")。1496年[乌拉赫支灭绝](../Page/乌拉赫.md "wikilink")。1806年，符腾堡公国被升为[符腾堡王国](../Page/符騰堡王國.md "wikilink")，公爵[弗里德里希三世成为王国的第一任国王](../Page/弗里德里希一世_\(符腾堡\).md "wikilink")**弗里德里希一世**。

1871年，符腾堡作为自治王国加入[普鲁士首相](../Page/普鲁士.md "wikilink")[俾斯麦通过几次成功的战争和外交手段缔造的](../Page/俾斯麦.md "wikilink")[德意志帝国](../Page/德意志帝国.md "wikilink")。[第一次世界大战后](../Page/第一次世界大战.md "wikilink")，君主制垮台，国王[威廉二世被推翻](../Page/威廉二世_\(符腾堡\).md "wikilink")。符腾堡作为[符腾堡人民邦](../Page/符腾堡人民邦.md "wikilink")，成了[魏玛共和国的一部分](../Page/魏玛共和国.md "wikilink")。1920年，[斯图加特是德国国民政府的所在地](../Page/斯图加特.md "wikilink")。[第二次世界大战期间](../Page/第二次世界大战.md "wikilink")，[斯图加特的市中心几乎被空袭完全摧毁](../Page/斯图加特.md "wikilink")。

1945年[盟军占领了](../Page/盟军.md "wikilink")[德国](../Page/德国.md "wikilink")。他们将[巴登和符腾堡合并](../Page/巴登.md "wikilink")，1952年成立新的民主的[巴登-符腾堡州](../Page/巴登-符腾堡.md "wikilink")（德国第三大州），首府在[斯图加特](../Page/斯图加特.md "wikilink")。

## 相关条目

  - [神圣罗马帝国](../Page/神圣罗马帝国.md "wikilink")
  - [德意志帝国](../Page/德意志帝国.md "wikilink")
  - [巴登-符腾堡](../Page/巴登-符腾堡.md "wikilink")
  - [符腾堡统治者列表](../Page/符腾堡统治者列表.md "wikilink")
  - [符腾堡王朝](../Page/符腾堡王朝.md "wikilink")

[Meyers_b16_s0772a.jpg](https://zh.wikipedia.org/wiki/File:Meyers_b16_s0772a.jpg "fig:Meyers_b16_s0772a.jpg")

[Category:神聖羅馬帝國諸侯國](../Category/神聖羅馬帝國諸侯國.md "wikilink")
[Category:德国历史上的君主国](../Category/德国历史上的君主国.md "wikilink")
[Category:巴登-符腾堡州历史](../Category/巴登-符腾堡州历史.md "wikilink")
[Category:士瓦本](../Category/士瓦本.md "wikilink")