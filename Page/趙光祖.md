**趙光祖**（，），字**孝直**，號**静庵**，是16世紀[朝鮮王朝的著名思想家及政治家](../Page/朝鮮王朝.md "wikilink")，改革家。他在[士林派的領首同](../Page/士林派.md "wikilink")[己卯士禍中被朝鮮君主](../Page/己卯士禍.md "wikilink")[中宗賜死](../Page/朝鮮中宗.md "wikilink")，後來在[仁宗登位後獲平反](../Page/朝鮮仁宗.md "wikilink")，諡號**文正**。[金宏弼](../Page/金宏弼.md "wikilink")、[柳崇祖的門人](../Page/柳崇祖.md "wikilink")。

## 生平

[Letter_of_Cho_Kwang-jo.jpg](https://zh.wikipedia.org/wiki/File:Letter_of_Cho_Kwang-jo.jpg "fig:Letter_of_Cho_Kwang-jo.jpg")
趙光祖出身於官僚家庭，父亲趙元纲是下級官吏。[本貫](../Page/本貫.md "wikilink")[漢陽趙氏](../Page/漢陽趙氏.md "wikilink")，是开国功臣[良節公趙溫](../Page/良節公.md "wikilink")（良節公溫，）的[玄孫](../Page/玄孫.md "wikilink")。十九歲時喪父，事母至孝，他的孝行令其在朝鮮國內一早揚名。他是[金宏弼的弟子](../Page/金宏弼.md "wikilink")。

中宗五年(1510年)，趙光祖在[科舉中考取了進士](../Page/科舉.md "wikilink")；1515年殿試文科（[進士試](../Page/進士試.md "wikilink")）及第（[状元](../Page/状元.md "wikilink")），進入朝廷任官，官拜[同副承旨](../Page/同副承旨.md "wikilink")。

趙光祖希望把[儒學思想實踐在政治上](../Page/儒學.md "wikilink")。在1516年，趙光祖與大批[士林派儒生請求中宗廢除主要供作進行](../Page/士林派.md "wikilink")[道教活動的](../Page/道教.md "wikilink")[昭格署](../Page/昭格署.md "wikilink")，獲接納。1518年，中宗批准開設由趙光祖等人提倡的賢良科考試。在這數年間，趙光祖受到中宗重用，官位升至“[大司憲](../Page/大司憲.md "wikilink")”，並積極在國內宣揚儒學。後來趙光祖認為[中宗反正的功臣過份濫封](../Page/中宗反正.md "wikilink")，要求削去部份“[靖國功臣](../Page/靖國功臣.md "wikilink")”的封號，史稱“[偽勳削除案](../Page/偽勳削除案.md "wikilink")”。中宗猶疑未決，在眾多士林派官員威脅辭官後，中宗終於答應。趙光祖在政治主張上過份強調道學思想，令中宗開始反感。

“偽勳削除案”激發[勳舊派功臣聯手反擊趙光祖](../Page/勳舊派.md "wikilink")，中宗後宮[熙嬪洪氏](../Page/熙嬪洪氏.md "wikilink")（勳舊派[洪景舟之女](../Page/洪景舟.md "wikilink")）在王宫后山通过涂[蜂蜜和虫子咬製造了上有](../Page/蜂蜜.md "wikilink")“走肖為王”字樣的樹葉，再把它們呈上中宗，令中宗對趙光祖的信任開始動搖。中宗後來寫信給[洪景舟](../Page/洪景舟.md "wikilink")，表示有意除去趙光祖。1519年農曆[十一月十五](../Page/十一月十五.md "wikilink")，洪景舟等人取得中宗詔書後派人捉拿了趙光祖及一些士林派官員。在[領議政](../Page/領議政.md "wikilink")[鄭光弼等人求情下](../Page/鄭光弼.md "wikilink")，趙光祖等八人暫時免死，被流放外地，然而趙光祖不久後在流放地被中宗以毒藥賜死，趙光祖服毒後很久仍未死，使者遂把他縊死了。

此次士林派失勢及遭受重大打擊的事件，後世稱為“[己卯士禍](../Page/己卯士禍.md "wikilink")”。“偽勳削除案”一事就此作罷。

仁宗即位後，平反了趙光祖的罪名，恢復了[賢良科](../Page/賢良科.md "wikilink")。1570年官方頒布的《[國朝儒先錄](../Page/國朝儒先錄.md "wikilink")》把趙光祖列為“[朝鲜四賢](../Page/朝鲜四賢.md "wikilink")”之一。（朝鲜四賢：趙光祖、[金宏弼](../Page/金宏弼.md "wikilink")、[鄭汝昌](../Page/鄭汝昌.md "wikilink")、[李彦迪](../Page/李彦迪.md "wikilink")）。

趙光祖的墓在位於[京畿道中部](../Page/京畿道.md "wikilink")、距離[汉城以南約](../Page/汉城.md "wikilink")40公里的[龍仁市](../Page/龍仁市.md "wikilink")[水枝邑](../Page/水枝區.md "wikilink")，鄰近[韓國民俗村](../Page/韓國民俗村.md "wikilink")。

## 曾饰演赵光祖的艺人

  - 《[朝鮮王朝五百年](../Page/朝鮮王朝五百年.md "wikilink")》—《[風蘭](../Page/風蘭.md "wikilink")》：1984年[MBC電視劇](../Page/文化广播_\(韩国\).md "wikilink")，
    飾。
  - 《》：1996年[KBS電視劇](../Page/韩国放送公社.md "wikilink")，[劉東根](../Page/劉東根.md "wikilink")
    飾。
  - 《林巨正》：1996年[SBS電視劇](../Page/SBS_\(韩国\).md "wikilink")，[太玫榮](../Page/太玫榮.md "wikilink")
    飾。
  - 《[女人天下](../Page/女人天下.md "wikilink")》：2002年[SBS電視劇](../Page/SBS_\(韩国\).md "wikilink")，
    飾。
  - 《[七日的王妃](../Page/七日的王妃.md "wikilink")》：2017年KBS電視劇，[姜其永](../Page/姜其永.md "wikilink")
    飾。

## 著作

  - 《靜庵集》

## 參見

  - [金宗直](../Page/金宗直.md "wikilink")
  - [南袞](../Page/南袞.md "wikilink")
  - [金宏弼](../Page/金宏弼.md "wikilink")
  - [李之芳](../Page/李之芳.md "wikilink")
  - [沈貞](../Page/沈貞.md "wikilink")

## 外部連結

  - [淡江大學公共行政學系論壇：人物列傳：趙光祖](http://www2.tku.edu.tw/~tmpx/pabbs/read.php?tid=8128)
  - [趙光祖](http://100.naver.com/100.nhn?docid=138166)
  - [趙光祖:Navercast](http://navercast.naver.com/contents.nhn?contents_id=2928)

[Category:朝鮮王朝中央官員](../Category/朝鮮王朝中央官員.md "wikilink")
[J](../Category/朝鲜王朝儒学学者.md "wikilink")
[Category:朝鲜王朝被处决者](../Category/朝鲜王朝被处决者.md "wikilink")
[G光祖](../Category/赵姓.md "wikilink")
[Category:諡文正](../Category/諡文正.md "wikilink")