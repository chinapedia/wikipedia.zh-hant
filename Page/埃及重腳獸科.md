**重腳獸科**（Arsinoitheriidae）是已[滅絕的](../Page/滅絕.md "wikilink")[重腳目下的一類](../Page/重腳目.md "wikilink")[哺乳動物](../Page/哺乳動物.md "wikilink")。牠們的[化石於](../Page/化石.md "wikilink")[中東](../Page/中東.md "wikilink")、[非洲](../Page/非洲.md "wikilink")、[亞洲及](../Page/亞洲.md "wikilink")[羅馬尼亞發現](../Page/羅馬尼亞.md "wikilink")。重腳獸科的外表像[犀牛](../Page/犀牛.md "wikilink")，但是牠們卻與犀牛並不怎麼接近，反而與[蹄兔目](../Page/蹄兔目.md "wikilink")、[象及](../Page/象.md "wikilink")[索齒獸目更為接近](../Page/索齒獸目.md "wikilink")。

## 化石紀錄

重腳獸科首先於[始新世中期出現](../Page/始新世.md "wikilink")，[土耳其原始的](../Page/土耳其.md "wikilink")*Palaeomasia*也是在該時期出現。\[1\]重腳獸科下最後存在的[屬是](../Page/屬.md "wikilink")[重腳獸屬](../Page/埃及重脚兽.md "wikilink")，牠們首先於始新世末的[法揚出現](../Page/法揚.md "wikilink")，並於[漸新世早期與其他埃及重腳科一同](../Page/漸新世.md "wikilink")[滅絕](../Page/滅絕.md "wikilink")。*Crivadiatherium*是在[羅馬尼亞的](../Page/羅馬尼亞.md "wikilink")[特蘭西瓦尼亞發現](../Page/特蘭西瓦尼亞.md "wikilink")，當地是[雷獸出沒最西邊的地方](../Page/雷獸.md "wikilink")。

## 命名

重腳獸科的學名是為紀念[埃及](../Page/埃及.md "wikilink")[托勒密二世的皇后](../Page/托勒密二世.md "wikilink")[阿爾西諾伊二世而取的](../Page/阿爾西諾伊二世.md "wikilink")，因為最初的重腳獸[化石是在其宮殿的廢墟附近發現](../Page/化石.md "wikilink")。

## 參考

## 外部連結

  - [The Paleobiology
    Database](http://paleodb.org/cgi-bin/bridge.pl?action=checkTaxonInfo&taxon_no=43310)
  - [Mikko's Phylogeny
    Archive](https://web.archive.org/web/20060621070910/http://www.fmnh.helsinki.fi/users/haaramo/Metazoa/Deuterostoma/Chordata/Synapsida/Eutheria/Basal_Ungulata/Embrithopoda.htm)

[Category:重腳目](../Category/重腳目.md "wikilink")
[Category:重腳獸科](../Category/重腳獸科.md "wikilink")
[Category:漸新世哺乳類](../Category/漸新世哺乳類.md "wikilink")

1.