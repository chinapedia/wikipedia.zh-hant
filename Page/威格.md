[Wenger_logo.svg](https://zh.wikipedia.org/wiki/File:Wenger_logo.svg "fig:Wenger_logo.svg")
[Wenger_EvoGrip_S17.JPG](https://zh.wikipedia.org/wiki/File:Wenger_EvoGrip_S17.JPG "fig:Wenger_EvoGrip_S17.JPG")
[Swiss_Army_Knife_Wenger_Opened_20050627.jpg](https://zh.wikipedia.org/wiki/File:Swiss_Army_Knife_Wenger_Opened_20050627.jpg "fig:Swiss_Army_Knife_Wenger_Opened_20050627.jpg")

**威戈**（**Wenger**）是一家以製造[瑞士軍刀等工具產品作為主業的](../Page/瑞士軍刀.md "wikilink")[瑞士公司](../Page/瑞士.md "wikilink")，也是兩個官方瑞士軍刀品牌之一。2005年時被另一品牌[瑞士維氏](../Page/維氏.md "wikilink")（Victorinox）收購。\[1\]

2005年Victorinox收購Wenger後成為唯一為瑞士軍方供應瑞士軍刀的公司，在合併之前，兩家公司會各自售予瑞士軍隊相同數量的軍刀。

2013年1月30日，Victorinox正式合并Wenger的刀具生产业务，今后将不再生产Wenger品牌的瑞士军刀。\[2\]

## 經歷

  - 1893年 - Paul Boéchat於Courtételle創立Paul Boéchat & Cie.刀廠。
  - 1895年 - 一群Delsberg的企業家接管該公司並將其更名為Fabrique suisse de coutellerie
    S.A.。
  - 1898年 - Theo Wenger被任命為主管。
  - 1900年 - Basler Besteckfabrik被收購，兩家公司合併後的名為Schweizer
    Besteckfabrik，業務移到Delsberg。
  - 1901年 - 瑞士軍隊配備Wenger的小型刀子。
  - 1907年 - Theo Wenger收購該公司並更名為Wenger & Co. S.A.。
  - 1908年 -
    瑞士軍隊決定撕毀合約將半數訂單交給位於[瑞士使用](../Page/瑞士.md "wikilink")[德語地區的Victorinox](../Page/德語.md "wikilink")，另一半則交給Wenger。他們聲稱這樣做是為了國家和諧，但亦有可能是為了加入[競爭](../Page/競爭.md "wikilink")。
  - 1922年 - 公司更名為Wenger S.A.。Kaspar Oertli購入大量股權。
  - 1929年 - Theo Wenger離世後Kaspar Oertli獲得該公司多數股權。
  - 1947年 - Max Oertli接管該公司的管理，生產程序變得機械化。
  - 1980年 - Max Oertli的後代營運該公司。
  - 1986年 - 一場火災燒毀了製造廠的大部份，令業務重建並擴張，設備變得現代化並修訂了產品線。
  - 1988年 - Wenger開始製造[手錶](../Page/手錶.md "wikilink")。
  - 1992年 - [NASA首次將瑞士軍刀帶上太空](../Page/NASA.md "wikilink")。
  - 1997年 - Wenger SA獲得[ISO-9001認證](../Page/ISO_9000.md "wikilink")。
  - 2004年 - Wenger更改瑞士軍刀的外形。
  - 2005年 - Victorinox收購Wenger並計劃保留該品牌。Wenger以"純正瑞士軍刀（Genuine Swiss Army
    Knife）"作為口號，Victorinox則為"正宗瑞士軍刀（Original Swiss Army
    Knife）"。同年，Wenger發表演進（Evolution），這是一系列擁有人體工學握柄的瑞士軍刀。被稱為是："自瑞士軍刀出現以來首次對紅色握柄的設計改變"。
  - 2007年 - Wenger發表EvoGrip，這是符合人體工學形狀並擁有用來加強握把安全性的嵌合橡膠的瑞士軍刀。
  - 2013年1月30日 - Victorinox宣布正式合并Wenger刀具生产业务，今后将不再生产Wenger品牌的瑞士军刀。

## 其他產品

除瑞士軍刀外Wenger亦有售賣[手錶](../Page/手錶.md "wikilink")，由於與加拿大的*Wenger Watch
Company*有商標衝突，所有加拿大的Wenger手錶均以*Swiss Military*的商標銷售，其他地方則為*Wenger*。

Wenger同時有生產以*SWIBO*與*Grand-Maître*為品牌的厨房用刀以及*Swissors*品牌剪刀。

Wenger亦有在北美銷售行李箱、公事包、書包與個人產品。

Wenger亦于2009年全球发布鞋品，并于2011年进入中国大陆地区。

## 參考

<references/>

## 外部連結

  - [官方網站](http://www.wenger-knife.ch)
  - [Wenger北美網站](https://web.archive.org/web/19991013021603/http://wengerna.com/)

<!-- end list -->

  - [Wenger手錶消息](http://www.all-watches.com/wenger-watches.html)

[分類:工具製造商](../Page/分類:工具製造商.md "wikilink")

[Category:瑞士公司](../Category/瑞士公司.md "wikilink")

1.  [Victorinox收購對手Wegner。](http://www.europastar.com/europastar/headlines/article_display.jsp?vnu_content_id=1000904500)
2.