**薄巳**（薄复礼，Rudolf Alfred
Bosshardt，也有人译为勃沙特，）[内地会的](../Page/内地会.md "wikilink")[传教士](../Page/传教士.md "wikilink")。

## 生平

生于英国[曼彻斯特](../Page/曼彻斯特.md "wikilink")，是瑞士移民的后代。9岁时经历复兴，17岁[受浸](../Page/受浸.md "wikilink")。1918年他选择保留[瑞士国籍](../Page/瑞士国籍.md "wikilink")，而家中其他人都入了[英国国籍](../Page/英国国籍.md "wikilink")。1922年他被内地会派往中国，在[鎮江内地会语言学校学习一年后](../Page/鎮江内地会语言学校.md "wikilink")，被派往[贵州北部的](../Page/贵州.md "wikilink")[遵義传教](../Page/遵義.md "wikilink")。1931年6月10日，他与内地会女传教士Rose
Piaget(1894-1965，家族在瑞士侏罗山拥有著名的钟表工厂)在[贵阳结婚](../Page/贵阳.md "wikilink")，随后调往[镇远](../Page/镇远.md "wikilink")。

[Bosshardt_on_horse_following_release_1936.jpg](https://zh.wikipedia.org/wiki/File:Bosshardt_on_horse_following_release_1936.jpg "fig:Bosshardt_on_horse_following_release_1936.jpg")
1934年10月1日，他被[红军第二方面军抓到并扣为](../Page/中国工农红军第二方面军.md "wikilink")[人质](../Page/人质.md "wikilink")，红军期望以此获得药品和经费。[肖克多年后回忆](../Page/肖克.md "wikilink")：“我们西征以来，转战50多天，又是暑天行军，伤、病兵员日多，苦于无药医治。我们知道这几位传教士有条件弄到药品和经费，于是，我们提出释放他们的条件是给红军提供一定数量的药品或经费。”\[1\]。他随[肖克和](../Page/肖克.md "wikilink")[贺龙的部队](../Page/贺龙.md "wikilink")[长征达](../Page/长征.md "wikilink")560天，经过贵州、四川、湖南、湖北、云南5省，行程达一万公里，1936年4月在云南被释放。11月，他将在红军中的经历出书*The
Restraining Hand: Captivity for Christ in China*，中譯有兩個文本：《神灵之手:
一个西方传教土随红军长征亲历记》和《一个西方传教士的长征亲历记》，是西方最早介绍中国红军[长征的专著](../Page/长征.md "wikilink")。\[2\]\[3\]\[4\]

1940年他重返贵州，到贵州[盘县一带传教](../Page/盘县.md "wikilink")。1948年至1949年期间，他们在盘县创办了“明恩小学”，并训练了当地一批教会领袖。1951年，薄巳夫妇与其他传教士一同被遣送回国。薄巳没有回国，而是经香港去了[老挝与在当地传教的瑞士弟兄会](../Page/老挝.md "wikilink")（Swiss
Brethren）回合。1957年，正式设立传教站，一直工作到1966年回到曼彻斯特。回国以后，他又帮助当地的华人基督教会。1984年，肖克途经法国时，曾委托他人寻找薄巳。寻找到他后，肖克在1986年委托中国驻英大使[冀朝铸前去拜访](../Page/冀朝铸.md "wikilink")\[5\]。1990年，薄巳移居[肯特郡](../Page/肯特郡.md "wikilink")。1993年，薄巳去世，中国方面称他是“[中国人民的老朋友](../Page/中国人民的老朋友.md "wikilink")”，但其实60年前他是被抓捕的。

薄巳夫妇没有留下子女。

## 注释

[Category:内地会传教士](../Category/内地会传教士.md "wikilink")
[Category:中国人民的老朋友](../Category/中国人民的老朋友.md "wikilink")
[Category:瑞士新教徒](../Category/瑞士新教徒.md "wikilink")
[Category:英國新教徒](../Category/英國新教徒.md "wikilink")
[Category:瑞士裔英国人](../Category/瑞士裔英国人.md "wikilink")
[Category:曼徹斯特人](../Category/曼徹斯特人.md "wikilink")
[Category:长征](../Category/长征.md "wikilink")

1.
2.
3.
4.
5.