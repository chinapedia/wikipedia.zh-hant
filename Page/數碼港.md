[Cyberport.jpg](https://zh.wikipedia.org/wiki/File:Cyberport.jpg "fig:Cyberport.jpg")
[Night_View_of_Cyberport.jpg](https://zh.wikipedia.org/wiki/File:Night_View_of_Cyberport.jpg "fig:Night_View_of_Cyberport.jpg")
[The_Podium.jpg](https://zh.wikipedia.org/wiki/File:The_Podium.jpg "fig:The_Podium.jpg")
**數碼港**（）位於[香港島](../Page/香港島.md "wikilink")[南區](../Page/南區.md "wikilink")[薄扶林](../Page/薄扶林.md "wikilink")[鋼綫灣](../Page/鋼綫灣.md "wikilink")，佔地24公頃。為一個雲集科技與數碼內容業務租戶的創意數碼社區，由香港特區政府全資擁有的[香港數碼港管理有限公司負責數碼港計劃的發展](../Page/香港數碼港管理有限公司.md "wikilink")、管理及營運。

數碼港設有四座[甲級寫字樓](../Page/甲級寫字樓.md "wikilink")、一間[五星級酒店及一個商場](../Page/五星級酒店.md "wikilink")。目標是培育本地資訊及通訊科技初創企業，並提升其發展實力。

## 歷史

1999年3月3日，由時任[行政長官](../Page/香港特別行政區行政長官.md "wikilink")[董建華發表的施政報告指出](../Page/董建華.md "wikilink")，香港需要發展[資訊科技](../Page/資訊科技.md "wikilink")，以配合一日千里的高科技發展，以及應付本地[企業對資訊科技的需求](../Page/企業.md "wikilink")。此外，施政報告期望發展資訊科技可以使香港從1997年[亞洲金融風暴中儘快復原](../Page/亞洲金融危機.md "wikilink")、及藉以提供重要的基礎建設，吸引資訊服務公司匯聚香港。

因此，香港政府於2000年5月17日，透過3間在財政司司長法團之下成立的全資擁有的私人公司（分別為香港數碼港發展控股有限公司、香港數碼港管理有限公司及香港數碼港（附屬發展）有限公司）與[盈科數碼動力](../Page/電訊盈科.md "wikilink")（即併購[香港電訊前的電訊盈科](../Page/香港電訊.md "wikilink")）全資附屬公司「資訊港有限公司」（Cyber-Port
Limited）簽立數碼港計劃協議書。

協議書訂明，「香港數碼港發展控股有限公司」獲得批地，以發展數碼港計劃。香港政府豁免數碼港78億港元地價，以及投資10億港元於[鋼綫灣興建基建](../Page/鋼綫灣.md "wikilink")，其餘建築費用需由電訊盈科全資附屬公司「資訊港有限公司」方面一手包辦。整個項目耗資約158億港元。電訊盈科可從住宅部分取得利益。

當數碼港落成啟用後，數碼港控股需將住宅部分（即「[貝沙灣](../Page/貝沙灣.md "wikilink")」）轉讓予一家附屬公司「香港數碼港（附屬發展）有限公司」，而數碼港部分其所有業權及管理權均需交回香港政府管理（即「數碼港管理有限公司」）。數碼港控股及其兩家附屬公司統稱為數碼港公司。

2015年1月20日，[聯想集團與數碼港簽署戰略夥伴框架協議](../Page/聯想集團.md "wikilink")，合作於數碼港建立亞太區首個雲端服務及產品研發中心。

## 建築

數碼港商業部份由四座寫字樓、數碼港商場及[數碼港艾美酒店組成](../Page/數碼港艾美酒店.md "wikilink")，並於2002年至2004年分批落成，提供合共逾100,000平方米的甲級寫字樓，以容納百多家資訊科技及有關產業的公司。數碼港艾美酒店是一座提供173座房間的的5星級酒店。住宅項目[貝沙灣包括約](../Page/貝沙灣.md "wikilink")2,800個高級住宅單位，於2004至2008年分批落成。

數碼港附設的零售及娛樂中心面積為27,000平方米，設施包括[商場](../Page/商場.md "wikilink")、[戲院](../Page/戲院.md "wikilink")、[酒店](../Page/酒店.md "wikilink")、科技中心、網絡操作中心、[香港移動體驗中心](../Page/香港移動體驗中心.md "wikilink")、[會議及](../Page/會議.md "wikilink")[展覽中心及](../Page/展覽中心.md "wikilink")[公共交通交匯處等](../Page/公共交通.md "wikilink")。

### 擴建和改建工程

《[2018年至2019年度香港政府財政預算案](../Page/2018年至2019年度香港政府財政預算案.md "wikilink")》中，政府表示向數碼港撥款一億元，將商場中庭改建為電競比賽場地，設有500個座位，其他工程包括加裝屏幕升降台、遮光布和天幕等，將於2019年6月竣工。\[1\]

《[2019年至2020年度香港政府財政預算案](../Page/2019年至2020年度香港政府財政預算案.md "wikilink")》中，政府宣布預留55億元發展數碼港第5期。選址為數碼港商場對開的[數碼港公園](../Page/數碼港公園.md "wikilink")。大樓樓面面積達6.6萬平方米，至少4萬平方米用作寫字樓和共享工作間，估計可容納100間科企及約700間初創。同時商業零售和設可容納800人的會議場地。而4億多元將用作翻新[數碼港公園作永久休憩空間](../Page/數碼港公園.md "wikilink")。\[2\]

## 管理層

  - [董事局主席](../Page/董事局主席.md "wikilink")：[林家禮博士](../Page/林家禮.md "wikilink")
  - [行政總裁](../Page/行政總裁.md "wikilink")：[林向陽先生](../Page/林向陽.md "wikilink")
  - [營運總監](../Page/營運總監.md "wikilink")：[麥傑夫先生](../Page/麥傑夫.md "wikilink")
  - 公眾使命總監：[湛家揚博士](../Page/湛家揚.md "wikilink")
  - 投資總監：[賴宗志博士](../Page/賴宗志.md "wikilink")

## 業績

2012年12月20日首次公布公司業績：

  - 收入達4.15億元
  - 辦公室以及商場租金收入佔1.57億元
  - 管理費、停車場等物業管理收入佔1億元、
  - 酒店收入佔1.2億元
  - 未計折舊總支出3.38億元
  - 物業管理支出佔1.2億元
  - 虧損1.35億元

## 設施、服務及活動領域

[Cyberport_Network_Operations_Centre.jpg](https://zh.wikipedia.org/wiki/File:Cyberport_Network_Operations_Centre.jpg "fig:Cyberport_Network_Operations_Centre.jpg")
[Cyberport_The_Arcade_Atrium_2017.jpg](https://zh.wikipedia.org/wiki/File:Cyberport_The_Arcade_Atrium_2017.jpg "fig:Cyberport_The_Arcade_Atrium_2017.jpg")
[Cyberport_Tower_3_Lobby_2007.jpg](https://zh.wikipedia.org/wiki/File:Cyberport_Tower_3_Lobby_2007.jpg "fig:Cyberport_Tower_3_Lobby_2007.jpg")

### 資訊科技/電訊基建設施及服務

  - 1至10千兆內部專用網絡
  - 120條頻道衛星電視共用天線系統
  - IP電話及綜合訊息傳送系統
  - 光纖或電纜樓宇布線系統
  - 網絡操作中心
  - 租戶專用數據中心
  - 無線局部區域網絡

### 設施

  - 數碼港會議及展覽中心
  - 科技中心
  - 企業發展中心
  - 香港移動體驗中心
  - 數碼港艾美酒店
  - 數碼港商場
  - GoGYM健身空間
  - [數碼港公園](../Page/數碼港公園.md "wikilink")

<File:The> Arcade Media Orb.jpg|位於數碼港商場內的官感廊 <File:Cyberport> The Arcade
Food Court 2007.jpg|位於數碼港商場內的美食廣場（2007年） <File:Cyberport> Tower 3 Zone C
2007.JPG|數碼港第3座E區園景廊（2007年） <File:Cyberport> Tower 3 Digital Media
Centre 2007.jpg|第3座數碼媒體中心（2007年） <File:Le> Meridien Cyberport
2007.jpg|數碼港艾美酒店（2007年）

### 核心活動領域

[Cyberport_residence_bel-air.svg](https://zh.wikipedia.org/wiki/File:Cyberport_residence_bel-air.svg "fig:Cyberport_residence_bel-air.svg")

  - 園區創建：發展及管理數碼港園區，務求為數碼港建立發展框架以實踐所訂立之發展目標，尤其著重積極推動可持續發展之方案。
  - 企業發展：營運數碼港數碼娛樂培育暨培訓中心及數碼港創意微型基金（CCMF），投入資源以接納更多高發展潛力的本地新成立企業及創業家，並擴充CCMF的規模以激發資訊及通訊科技業界的創意及創新科技。
  - 科技：應用創新及高端科技以提供領先的資訊及通訊科技基礎設施，從而推動香港策略性資訊及通訊科技業集中地的建立和發展。
  - 協作：協助本地資訊及通訊科技業與海外地區在兩大方面合作：商業聯盟及人才交流；建立互惠網絡以協助資訊及通訊科技業的中小企開拓及把握海外及內地市場的商機。
  - 知識及人才：推動以知識為本的計劃，轉化香港成為共融的知識型數碼經濟體系；舉辦世界級會議、啟發性培訓及比賽，同時促進創造更多資訊及通訊科技業相關的職位，以及推動與數碼共融有關的計劃以縮窄數碼鴻溝。

## 交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color={{南港島綫西段色彩}}>█</font>[南港島綫
    (西段)](../Page/南港島綫_\(西段\).md "wikilink")：[數碼港站](../Page/數碼港站.md "wikilink")（規劃中）

<!-- end list -->

  - [數碼港公共運輸交匯處](../Page/數碼港公共運輸交匯處.md "wikilink")

<!-- end list -->

  - [數碼港道](../Page/數碼港道.md "wikilink")

<!-- end list -->

  - [非專利巴士](../Page/非專利巴士.md "wikilink")

<!-- end list -->

  - 來往[南昌站](../Page/南昌站_\(香港\).md "wikilink")、[九龍塘及](../Page/九龍塘.md "wikilink")[油塘](../Page/油塘.md "wikilink")（繁忙時間服務，由數碼港的物業管理公司安排）
  - 另外在數碼港舉行大型展覽會時，主辦機構有時會開設免費穿梭巴士來往港島各區。

</div>

</div>

## 爭議

在2000年，政府在未經公開招標的情況下，把[鋼綫灣臨海優質地皮免地價批予](../Page/鋼綫灣.md "wikilink")[李澤楷的](../Page/李澤楷.md "wikilink")[盈科拓展](../Page/盈科拓展.md "wikilink")，意圖發展成類似美國[矽谷的高科技中心](../Page/矽谷.md "wikilink")。然而，此舉引起[官商勾結的指控](../Page/官商勾結.md "wikilink")，當時各大地產商都強烈不滿，指政府偏袒[李嘉誠家族](../Page/李嘉誠.md "wikilink")，數十名在商政界有一定地位的地產商人到[香港政府總部與董建華會面表示抗議](../Page/香港政府總部.md "wikilink")。

## 圖集

Cyberport Tower 2.jpg|數碼港第2座 Cyberport Tower 3 2007.jpg|數碼港第3座 Science
park.jpg|數碼港第4座（2005年） Cyberport-cinema.jpg|數碼港戲院

## 參考資料

## 外部連結

  - [數碼港官方網頁](http://www.cyberport.hk/cyberport/tw/home/home_flash.html)
  - [數碼港商場官方網頁](http://arcade.cyberport.hk/)
  - [數碼港艾美酒店官方網頁](http://www.cyberport.hk/cyberport/tw/home/facilities_n_services/hotel.htm)
  - [新浪香港文章](http://news.sina.com.hk/cgi-bin/nw/show.cgi/2/1/1/412699/1.html)
  - [數碼港住宅地價78億](http://www.singtao.com.hk/archive/fullstory.asp?id=20000804d01)

## 參見

  - [香港科學園](../Page/香港科學園.md "wikilink")

{{-}}  {{-}}  {{-}}

[Category:鋼綫灣](../Category/鋼綫灣.md "wikilink") [Category:南區寫字樓
(香港)](../Category/南區寫字樓_\(香港\).md "wikilink")
[Category:香港科技](../Category/香港科技.md "wikilink")
[Category:香港互聯網](../Category/香港互聯網.md "wikilink")
[Category:香港地標](../Category/香港地標.md "wikilink")
[Category:香港法定機構](../Category/香港法定機構.md "wikilink")
[Category:南區商場 (香港)](../Category/南區商場_\(香港\).md "wikilink")
[Category:中國科技園區](../Category/中國科技園區.md "wikilink")
[Category:紅色公共小巴禁區](../Category/紅色公共小巴禁區.md "wikilink")

1.
2.