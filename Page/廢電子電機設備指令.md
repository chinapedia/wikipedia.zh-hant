[WEEE_symbol_vectors.svg](https://zh.wikipedia.org/wiki/File:WEEE_symbol_vectors.svg "fig:WEEE_symbol_vectors.svg")
**廢棄電子電機設備指令**（Waste Electrical and Electronic Equipment Directive
2002/96/EC，縮寫：**WEEE**）是[歐洲聯盟在](../Page/歐洲聯盟.md "wikilink")2003年2月所通過的一項[環保指令](../Page/環保.md "wikilink")，制訂所有廢棄電子電機設備收集、回收、再生的目標。

## 規範對象

WEEE的規範對象為工作電壓小於1000[V](../Page/V.md "wikilink")
[AC或](../Page/AC.md "wikilink")1500V
[DC的設備](../Page/直流電.md "wikilink")，分別為：

  - 大型家用電器
  - 小型家用電器
  - 資訊技術及電信通訊設備
  - 消費性耐久設備
  - 照明設備
  - 电气和电子工具（大型静态工业工具除外）
  - 玩具、休闲和运动设备
  - 医用设备（所有被植入和被感染产品除外）
  - 監視、控制設備
  - [自动售货机](../Page/自动售货机.md "wikilink")

WEEE指令要求所有在[歐盟販賣上述物品的製造商必須考慮到產品廢棄時所造成的](../Page/歐盟.md "wikilink")[環境污染問題](../Page/環境污染.md "wikilink")，採用易於回收且[環保的設計](../Page/環保.md "wikilink")，並負起回收的責任和費用。

## 參看

  - [危害性物質限制指令](../Page/危害性物質限制指令.md "wikilink")（**RoHS**）
  - [能源使用產品生態化設計指令](../Page/能源使用產品生態化設計指令.md "wikilink")（**EuP**）
  - [綠色化學](../Page/綠色化學.md "wikilink")

## 外部連結

  - [European Commission WEEE
    page](http://ec.europa.eu/environment/waste/weee_index.htm)
      - [RoHS
        directive](http://europa.eu/eur-lex/pri/en/oj/dat/2003/l_037/l_03720030213en00190023.pdf)
        (PDF)
      - [WEEE
        directive](http://europa.eu/eur-lex/pri/en/oj/dat/2003/l_037/l_03720030213en00240038.pdf)
        (PDF)
      - [WEEE
        directive报废电子电气设备指令](http://www.weeeregistration.com/WEEE-directive_gb.html)
        (in Simplified Chinese简体中文)
  - [Guidance on WEEE registration in every EEA (EU+EFTA) member
    state](http://www.weeeregistration.com/weee-registration/)
  - [RoHS resource for lead-free
    test](https://web.archive.org/web/20080928211442/http://www.lead-freetest.com/)
  - [EIATRACK - Electronic Industries Alliance Regulatory Tracking
    Tool](https://web.archive.org/web/20180712214443/https://www.eiatrack.org/)
  - [WEEE Man](http://www.weeeman.org)
  - [The Royal Society of Arts](http://www.theRSA.org)
  - [Technosteria- an informative page on WEEE and
    RoHS](http://www.technosteria.com/)
  - [WEEE Recycling Directory](http://www.weeedirectory.com)
  - [WEEE and RoHS Legislation in Europe](http://www.perchards.com)
  - [Product Development for RoHS and WEEE
    Compliance](http://pcdandm.com/cms/cms/content/view/2570/95/)
  - [RoHS, WEEE, and ELV Environmental
    Compliance](https://web.archive.org/web/20061111010643/http://www.synapsistech.com/Products_Environmental_Compliance.asp)
  - [WEEE Recycling Directory for the UK](http://www.weeedirectory.com)
  - [Basic summary of the WEEE
    Directive](https://web.archive.org/web/20061017084159/http://www.radio-electronics.com/info/manufacture/standards/weee/weee_directive.php)

[Category:歐盟政策](../Category/歐盟政策.md "wikilink")
[Category:环境保护](../Category/环境保护.md "wikilink")
[Category:2003年環境](../Category/2003年環境.md "wikilink")