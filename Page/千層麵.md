[Lasagna_with_salad,_May_2011.jpg](https://zh.wikipedia.org/wiki/File:Lasagna_with_salad,_May_2011.jpg "fig:Lasagna_with_salad,_May_2011.jpg")

[Lasagna.jpg](https://zh.wikipedia.org/wiki/File:Lasagna.jpg "fig:Lasagna.jpg")

**千層麵**是一種[麵食](../Page/麵食.md "wikilink")，特點是用多張寬如[手帕的大麵皮疊起來](../Page/手帕.md "wikilink")，內層夾上多種[乳酪及肉醬](../Page/乳酪.md "wikilink")（[素食版可用](../Page/素食.md "wikilink")[菠菜代替](../Page/菠菜.md "wikilink")），經焗製調味而成。西方一般稱這種食品為**Lasagne**或者**Lasagna**，此字源自[希臘文Lasanon](../Page/希臘文.md "wikilink")（痰罐），後來被[羅馬人引申成煮食爐](../Page/羅馬人.md "wikilink")，[意大利人再借用此字指千層麵](../Page/意大利人.md "wikilink")，有時亦會叫作Lasagne
al
forno（千層麵在焗爐），多會配合[莫薩里拉乾酪](../Page/莫薩里拉乾酪.md "wikilink")、[帕馬森乾酪或](../Page/帕馬森乾酪.md "wikilink")[利可他乳水乳酪](../Page/利可他乳水乳酪.md "wikilink")，而且有許多食譜會加入[白汁作為調味](../Page/白汁.md "wikilink")。

## 起源

千層麵普遍被視作意大利的食品，但有關這種麵食起源目前仍眾說紛紜。在英國，14世紀時一種名叫Loseyns的近似食品，曾是英王[理查二世的宮廷御膳](../Page/理查二世_\(英格蘭\).md "wikilink")，但此一說法曾受意大利駐英大使非議\[1\]。北歐地區中，從維京人時代開始已進食一種Langkake的食品，被指可能是千層麵的前身。　

## 參看

  - [義大利菜](../Page/義大利菜.md "wikilink")
  - [義大利麵食](../Page/義大利麵食.md "wikilink")
  - [加菲猫](../Page/加菲猫.md "wikilink")

## 外部連結

  - [Cooking For Engineers: Meat
    Lasagna](http://www.cookingforengineers.com/recipe.php?id=36&title=Meat+Lasagna)
    - 圖片食譜
  - [Lasagna recipe](http://www.lasagna-recipe.com/) - 食譜

## 參考

<references />

[Category:意式麵食](../Category/意式麵食.md "wikilink")

1.