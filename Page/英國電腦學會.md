**英國電腦學會**（[英語](../Page/英語.md "wikilink")：British Computer
Society，[縮寫](../Page/縮寫.md "wikilink")：BCS）成立於1957年，代表資訊科技業者的團體。它是最大的以[英國為基地的](../Page/英國.md "wikilink")[電腦領域團體現有](../Page/電腦.md "wikilink")7萬名會員分佈在100个国家和地区。專業會員分為專業級會員（[英文](../Page/英文.md "wikilink")：Professional
Member，[縮寫](../Page/縮寫.md "wikilink")：MBCS）和會士級會員（[英文](../Page/英文.md "wikilink")：Fellow，[縮寫](../Page/縮寫.md "wikilink")：FBCS）。

## 香港分會

**英國電腦學會（香港分會）**成立於1991年\[1\]，在[香港提供一個平台以便香港的會員聯繫及溝通](../Page/香港.md "wikilink")，並為英國電腦學會總部在香港建立專業風範。母會英國電腦學會為[資訊科技界的專業人士及註册工程師學會的業界團體](../Page/資訊科技.md "wikilink")。一直以來，香港分會為會員組織了很多活動及聚會，並積極參與資訊科技有關的公開或政府邀請的諮詢。香港分會現有約一千名會員。

## 參考注釋

## 外部連結

  - [官方網站](http://www.bcs.org/)
      - [香港分會](http://www.bcs.org.hk/)
  - [BCSrecruit.com Official
    jobsite](https://web.archive.org/web/20100105180917/http://bcsrecruit.com/)
  - [BCS YouTube channel](http://www.youtube.com/user/BCS1957)

[Category:英国学会](../Category/英国学会.md "wikilink")
[Category:非政府间国际组织](../Category/非政府间国际组织.md "wikilink")
[Category:同業公會](../Category/同業公會.md "wikilink")
[Category:1957年建立](../Category/1957年建立.md "wikilink")
[Category:工程协会](../Category/工程协会.md "wikilink")

1.  [關於英國電腦學會(香港分會)](http://www.bcs.org.hk/chi/about_hk.php)