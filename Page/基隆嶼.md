[眺望基隆嶼.jpg](https://zh.wikipedia.org/wiki/File:眺望基隆嶼.jpg "fig:眺望基隆嶼.jpg")。\]\]
[Keelung_Island.JPG](https://zh.wikipedia.org/wiki/File:Keelung_Island.JPG "fig:Keelung_Island.JPG")忘憂谷遠眺基隆嶼\]\]
**基隆嶼**，又稱**基隆島**，是位於[台灣東北方的](../Page/台灣.md "wikilink")[東海海域之](../Page/東海.md "wikilink")[島嶼](../Page/島嶼.md "wikilink")，古稱**雞籠嶼**、**雞籠杙**\[1\]，與[和平島直線距離約](../Page/和平島.md "wikilink")3.3公里、[基隆港口距離約](../Page/基隆港.md "wikilink")4公里。其為[無人島](../Page/無人島.md "wikilink")，行政區屬[基隆市](../Page/基隆市.md "wikilink")[中正區](../Page/中正區_\(基隆市\).md "wikilink")。由於孤懸海上，故成為[基隆與北海岸地區明顯的自然](../Page/基隆北海岸.md "wikilink")[地標](../Page/地標.md "wikilink")，現亦開放登島觀光。

## 地形地質

東北─西南長約960米，寬約400米，面積約24公頃，島上最高[海拔為](../Page/海拔.md "wikilink")182米，狀如[鯨魚](../Page/鯨魚.md "wikilink")。與[棉花嶼](../Page/棉花嶼.md "wikilink")、[彭佳嶼及](../Page/彭佳嶼.md "wikilink")[花瓶嶼並為基隆外海四個](../Page/花瓶嶼.md "wikilink")[火山島嶼](../Page/火山島.md "wikilink")，是由[角閃石](../Page/角閃石.md "wikilink")、[黑雲母](../Page/黑雲母.md "wikilink")、[石英](../Page/石英.md "wikilink")、[安山岩所構成的島嶼](../Page/安山岩.md "wikilink")，四周皆為峭壁，幾無平地，多見[海蝕洞穴與岩礁](../Page/海蝕.md "wikilink")。

## 生態資源

島上由於受到基隆多雨環境與岩石[風化影響](../Page/風化.md "wikilink")，[植被豐富](../Page/植被.md "wikilink")，多生[榕樹](../Page/榕樹.md "wikilink")，並且生長大量的[野百合](../Page/野百合.md "wikilink")、[金花石蒜](../Page/金花石蒜.md "wikilink")、[石板菜](../Page/石板菜.md "wikilink")、[女貞](../Page/女貞.md "wikilink")、[防葵](../Page/防葵.md "wikilink")、[萬年松](../Page/萬年松.md "wikilink")、[木槿等植物](../Page/木槿.md "wikilink")。目前基隆市政府逐步復育百合、金花石蒜。五、六月份為百合花季；八至九月為金花石蒜花季。島上除了珍貴的[火山地形之外](../Page/火山.md "wikilink")，也有獨有的火山螃蟹。島上並無野生的大型[哺乳動物](../Page/哺乳動物.md "wikilink")。

## 島上狀況

  - 水源：收集雨水和台灣本島運補。所收集之雨水主要是備用，日常用水以本島運補水為主。
  - 電力：柴油發電機供電，發電用油需由台灣運補，部分電力來自[太陽能裝置](../Page/太陽能.md "wikilink")。
  - 人口：目前由[海巡署人員駐守](../Page/海巡署.md "wikilink")，單位為基隆嶼安檢所，並無一般平民居住。

## 觀光景點

  - 聳立于基隆港外，為[日治時期](../Page/台灣日治時期.md "wikilink")[基隆八景之一](../Page/基隆八景.md "wikilink")，稱為「杙峰聳翠」，基隆仕紳[許梓桑有詩吟詠](../Page/許梓桑.md "wikilink")：「萬人星羅繞杙峰，杙峰不與眾山同，孤高千仞凌霄漢，一望蒼茫鎮海東。」
  - 島上遊憩設施以步道和涼亭為主，視野所及，西南面是野柳至鼻頭角的北海岸，東北面則是廣闊的東海。
  - 島上有[基隆島燈塔](../Page/基隆島燈塔.md "wikilink")，興建於1980年，採太陽能發電，原歸[海關管轄](../Page/財政部關務署.md "wikilink")，2013年移撥[交通部航港局](../Page/交通部航港局.md "wikilink")。另有日治時期遺留之「楠田上等兵殉職碑」，用以紀念因工事殉職兵士。
  - 基隆嶼過去由於軍事管制因素並未對外開放，2001年才正式開放登島觀光，目前可由[碧砂漁港與](../Page/碧砂漁港.md "wikilink")[基隆火車站前小艇碼頭登船前往](../Page/基隆火車站.md "wikilink")。附近的海域也是基隆外海有名的[磯釣場](../Page/磯釣.md "wikilink")，盛產[白帶魚](../Page/白帶魚.md "wikilink")、[鎖管等豐富漁產](../Page/鎖管.md "wikilink")，每到入夜之後，可見一艘艘的海釣船停泊在基隆嶼附近，也是基隆嶼晚上美麗的另一面。

## 參考資料

### 注釋

### 影視資料

  - 三立節目部製作，2008，在台灣的故事。

[Category:基隆市島嶼](../Category/基隆市島嶼.md "wikilink")
[Category:基隆北海岸景點](../Category/基隆北海岸景點.md "wikilink")
[Category:基隆市旅遊景點](../Category/基隆市旅遊景點.md "wikilink")
[Category:火山島](../Category/火山島.md "wikilink")
[Category:台灣火山](../Category/台灣火山.md "wikilink")

1.  「杙」，讀音通「易」（[漢語拼音](../Page/漢語拼音.md "wikilink")：yī
    (yi4)，[注音符號](../Page/注音符號.md "wikilink")：ㄧˋ，[威妥瑪拼音](../Page/威妥瑪拼音.md "wikilink")：yi4）。