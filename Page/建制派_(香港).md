**建制派**，另稱“**親北京陣營**”、“**親共派**”、“**親中派**”（），其政治立場為擁護[中國共產黨及](../Page/中國共產黨.md "wikilink")[香港特別行政區](../Page/香港特別行政區.md "wikilink")，並且支持[中國共產黨](../Page/中國共產黨.md "wikilink")[一黨專政](../Page/一黨專政.md "wikilink")。

自香港有选举以来，建制派在歷次全港选举中的總得票數一直低於民主派。但憑籍立法會[功能界別議席成為了議會大多數](../Page/功能界別_\(香港\).md "wikilink")。

建制派構成頗為複雜，分為[多個黨派及團體](../Page/香港建制派列表.md "wikilink")，包括紅色資本家、前港英政府的部分官僚和公务员、本地[工商界人士](../Page/商界_\(香港政治派別\).md "wikilink")、專業階層的[親共人士](../Page/香港親共團體.md "wikilink")、[傳統左派](../Page/香港親共團體.md "wikilink")[工會](../Page/工會.md "wikilink")、[地區](../Page/地區.md "wikilink")[社團及同鄉會](../Page/社團.md "wikilink")，普遍支持[一國兩制及擁護](../Page/一國兩制.md "wikilink")[基本法](../Page/香港特別行政區基本法.md "wikilink")。在政治[意識形態上](../Page/意識形態.md "wikilink")，與[泛民主派在政治立場方面幾乎對立](../Page/泛民主派.md "wikilink")，但部分派別在某些經濟議題上持有相同意見。但反過來，建制派內部雖然政治立場大致相同，但不同派別在經濟議題上的立場也可能對立。

部分人認為，建制派實際上是[香港政府的](../Page/香港政府.md "wikilink")[執政聯盟](../Page/執政聯盟.md "wikilink")，但根據《行政長官選舉條例》的規定，[香港行政長官是不能隸屬任何](../Page/香港行政長官.md "wikilink")[政黨](../Page/政黨.md "wikilink")，政黨成員雖然可用個人身份出選特首選舉，但成功當選後必須退黨，所以現時香港法律上沒有執政黨\[1\]。

## 簡介

建制派有來自不同背景的社團組織，涵蓋香港主流社會的工商界、金融界、地產界（合稱[**商界**](../Page/商界_\(香港政治派別\).md "wikilink")）和專業階層等，主要是指支持現[香港特別行政區政府的政治組織](../Page/香港特別行政區政府.md "wikilink")，支持大部分香港特區政府的政策和提出的議案。

[民建聯及](../Page/民建聯.md "wikilink")[工聯會是建制派的主流力量](../Page/工聯會.md "wikilink")，獲工聯會屬下工會、政府工會、公屋基層、[新移民等支持](../Page/香港新移民.md "wikilink")，而[經民聯](../Page/經民聯.md "wikilink")、[新民黨](../Page/新民黨_\(香港\).md "wikilink")、[經民聯](../Page/香港經濟民生聯盟.md "wikilink")、[自由黨主要獲得工商界](../Page/自由黨_\(香港\).md "wikilink")、大財團及部分中產階級支持。

## 歷史

### 回歸前的工會和社團

在[香港殖民地時期已經支持](../Page/香港殖民地時期.md "wikilink")[中國共產黨的](../Page/中國共產黨.md "wikilink")[香港親共人士](../Page/香港親共人士.md "wikilink")，包括工人組織如[工聯會](../Page/工聯會.md "wikilink")，有「紅校」之稱的[香島中學及](../Page/香島中學.md "wikilink")[培僑中學](../Page/培僑中學.md "wikilink")、當時的[學聯](../Page/學聯.md "wikilink")、[學友社等](../Page/學友社.md "wikilink")，被形容為「根正苗紅」的香港親共勢力。整個1950至60年代，傳統左派明來暗往地與親[中國國民黨的](../Page/中國國民黨.md "wikilink")[右派鬥法](../Page/香港親台團體.md "wikilink")，長期被反共的英國殖民地政府打壓及箝制，傳統左派學校的學生甚至因為校方杯葛港府的教育制度而無法升讀大專院校。

1967年，在[中国大陆的](../Page/中国大陆.md "wikilink")[文化大革命風潮下](../Page/文化大革命.md "wikilink")，包括[楊光](../Page/楊光_\(鬥委會\).md "wikilink")、[費彝民](../Page/費彝民.md "wikilink")、[謝鴻惠](../Page/謝鴻惠.md "wikilink")、[黃建立](../Page/黃建立.md "wikilink")、[王寬誠等在內的傳統左派發起反對英國殖民地統治的](../Page/王寬誠.md "wikilink")「反英抗暴」\[2\]\[3\]。惟傳統左派在[六七暴動中的](../Page/六七暴動.md "wikilink")[恐怖主義行為普遍受到市民強烈譴責](../Page/恐怖主義.md "wikilink")，一些人的[恐共情緒至今亦不能清除](../Page/恐共.md "wikilink")，傳統左派至今拒絕道歉\[4\]。

1970年代正值「火紅年代」，社會運動熾熱，以[梁錦松為代表的](../Page/梁錦松.md "wikilink")[香港大學學生會親北京陣營](../Page/香港大學學生會.md "wikilink")「國粹派」，曾積極參與[保釣](../Page/保釣.md "wikilink")、[中文運動](../Page/中文運動.md "wikilink")\[5\]，1980年代亦因香港前途問題，開始與民主派針鋒相對\[6\]。

1992年7月10日，緊隨民主派的[民主黨](../Page/民主黨_\(香港\).md "wikilink")（當時仍為[香港民主同盟及](../Page/香港民主同盟.md "wikilink")[匯點](../Page/匯點.md "wikilink")）、工商界的[自由黨](../Page/自由黨_\(香港\).md "wikilink")（前身為啟聯資源中心）成立之後，傳統左派人士以[曾鈺成](../Page/曾鈺成.md "wikilink")、[馬力](../Page/馬力.md "wikilink")、[程介南](../Page/程介南.md "wikilink")、[葉國謙](../Page/葉國謙.md "wikilink")、[楊耀忠等為首](../Page/楊耀忠.md "wikilink")，創立政黨[民主建港聯盟](../Page/民主建港聯盟.md "wikilink")（民建聯）參選[立法局](../Page/立法局.md "wikilink")，成員不少來自[工聯會及](../Page/工聯會.md "wikilink")[教聯會](../Page/教聯會.md "wikilink")。

1997年，加入[臨時立法會](../Page/臨時立法會.md "wikilink")，推翻了不少被認為「對特區政府運作做成障礙」的法例。同時民建聯及工聯會亦加入[香港行政會議](../Page/香港行政會議.md "wikilink")，其中工聯會的[鄭耀棠更身兼行政會議非官守成員長達二十年](../Page/鄭耀棠.md "wikilink")。

傳統建制派政團以[民建聯](../Page/民建聯.md "wikilink")、[工聯會](../Page/工聯會.md "wikilink")、[新界社團聯會](../Page/新界社團聯會.md "wikilink")、[九龍社團聯會及](../Page/九龍社團聯會.md "wikilink")[香港島各界聯合會等為主](../Page/香港島各界聯合會.md "wikilink")，而民建聯是建制派的最大政黨，與[泛民主派關係惡劣](../Page/泛民主派.md "wikilink")，並在地區上存在競爭。

### 工業界和商界

工商派自[香港回歸後](../Page/香港回歸.md "wikilink")，一直主導[香港特別行政區政府的經濟政策](../Page/香港特別行政區政府.md "wikilink")。目前工商界政黨以[自由黨](../Page/自由黨_\(香港\).md "wikilink")、[經民聯](../Page/經民聯.md "wikilink")、[新民黨等為主](../Page/新民黨_\(香港\).md "wikilink")。2012年後，因自由黨反對[梁振英](../Page/梁振英.md "wikilink")（並非反對香港政府），經民聯和新民黨從自由黨分裂，其中新民黨只在直選中參選，對象是[中產選民](../Page/中產.md "wikilink")，加入[公民力量後在港島及新界均有區議員](../Page/公民力量.md "wikilink")，經民聯則以[功能界別為主](../Page/功能界別.md "wikilink")，但其中[梁美芬及加入經民聯的](../Page/梁美芬.md "wikilink")[西九新動力在九龍西有不少區議員](../Page/西九新動力.md "wikilink")。

### 鄉事派

以[新界鄉議局成員](../Page/新界鄉議局.md "wikilink")、[新界社團聯會等新界鄉事勢力為主](../Page/新界社團聯會.md "wikilink")，主要成員以[新界原居民及鄉郊社團組織為主](../Page/新界原居民.md "wikilink")，1997年前鄉事派支持[港英政府及](../Page/港英政府.md "wikilink")[中華民國政府](../Page/中華民國政府.md "wikilink")，但1997年[香港回歸後成為](../Page/香港回歸.md "wikilink")「愛國愛港」的政治力量之一，堅定擁護中華人民共和國政府和香港特別行政區政府，主張「愛國愛港愛鄉」，以維護新界原居民的[特權利益為主要特點](../Page/特權.md "wikilink")；與[民主派關係惡劣](../Page/民主派_\(香港\).md "wikilink")，惟民主派難以撼動鄉事派。部分鄉事派及新界社團聯會成員會以民建聯名義參選。

## 政治派別

建制派包括多個[香港政黨](../Page/香港政黨.md "wikilink")、[政治組織及一些](../Page/政治組織.md "wikilink")[獨立人士](../Page/獨立人士.md "wikilink")，現時立法會的政黨主要包括[民建聯](../Page/民建聯.md "wikilink")、[經民聯](../Page/經民聯.md "wikilink")、[工聯會](../Page/工聯會.md "wikilink")、[新民黨](../Page/新民黨_\(香港\).md "wikilink")、[自由黨等](../Page/自由黨_\(香港\).md "wikilink")。目前各建制派政黨之[政治光譜可以被如此理解](../Page/政治光譜.md "wikilink")。

  - [傳統親共派](../Page/香港親共團體.md "wikilink")：[民建聯](../Page/民建聯.md "wikilink")、[工聯會](../Page/工聯會.md "wikilink")、[港九劳工社团联会等](../Page/港九劳工社团联会.md "wikilink")
  - [商界](../Page/商界_\(香港政治派別\).md "wikilink")：[經民聯](../Page/經民聯.md "wikilink")、[新民黨](../Page/新民黨_\(香港\).md "wikilink")、[自由黨](../Page/自由黨_\(香港\).md "wikilink")、[實政圓桌等](../Page/實政圓桌.md "wikilink")
  - [激進親共派](../Page/激進建制派.md "wikilink")：[新界關注大聯盟](../Page/新界關注大聯盟.md "wikilink")、[新世紀論壇](../Page/新世紀論壇.md "wikilink")、[愛護香港力量](../Page/愛護香港力量.md "wikilink")、[愛港之聲](../Page/愛港之聲.md "wikilink")、[保衛香港運動](../Page/保衛香港運動.md "wikilink")、[幫幫香港出聲行動](../Page/幫幫香港出聲行動.md "wikilink")、[香港青年關愛協會和](../Page/香港青年關愛協會.md "wikilink")[中華人民共和國香港市人民委員會等](../Page/中華人民共和國香港市人民委員會.md "wikilink")
  - [鄉事派及其他](../Page/鄉事委員會.md "wikilink")：[創建力量](../Page/創建力量.md "wikilink")、[新界鄉議局及各鄉事委員會等](../Page/新界鄉議局.md "wikilink")

## 回歸以來發展

[香港回歸後](../Page/香港回歸.md "wikilink")，在[董建華管治下](../Page/董建華.md "wikilink")，建制派政黨領袖如自由黨主席[田北俊](../Page/田北俊.md "wikilink")、[民建聯主席](../Page/民建聯.md "wikilink")[曾鈺成](../Page/曾鈺成.md "wikilink")、[工聯會會長](../Page/工聯會.md "wikilink")[鄭耀棠等](../Page/鄭耀棠.md "wikilink")，皆委任為行政會議成員。建制派憑籍[功能組別](../Page/功能組別.md "wikilink")，縱使直選議席不及[泛民主派](../Page/泛民主派.md "wikilink")，但仍對立法會取得控制權。

[2003年七一遊行](../Page/2003年七一遊行.md "wikilink")，超過五十萬名香港市民上街反對[基本法第二十三條立法以及各種市民認為的施政失誤](../Page/基本法第二十三條.md "wikilink")，時任自由黨主席田北俊辭去行政會議成員並改變立場反對立法，其後部分工商界功能組別議員跟隨自由黨改變立場，在失去議會多數支持下，特區政府最終撤回方案，而自由黨在[2004年香港立法會選舉中成功取回直選議席](../Page/2004年香港立法會選舉.md "wikilink")。七一遊行後，民建聯在[2003年香港區議會選舉慘敗](../Page/2003年香港區議會選舉.md "wikilink")，但在[2004年香港立法會選舉取得](../Page/2004年香港立法會選舉.md "wikilink")10席，[2007年香港區議會選舉中](../Page/2007年香港區議會選舉.md "wikilink")，共得117席大勝，民建聯從此坐大，成為建制派第一大黨。民建聯于2005年併入[香港協進聯盟后](../Page/香港協進聯盟.md "wikilink")，改走跨階層路線，以擴大在[立法會的影響力](../Page/立法會.md "wikilink")。此後，工聯會在2008年起獨立參選，不再以民建聯名義參選立法會。

因民建聯主席馬力逝世而出缺的[2007年港島區補選由因推銷基本法二十三條立法而下台的前](../Page/2007年香港立法會港島選區補選.md "wikilink")[保安局局長](../Page/保安局局長.md "wikilink")[葉劉淑儀對撼獲泛民主派支持的前](../Page/葉劉淑儀.md "wikilink")[政務司司長](../Page/政務司司長.md "wikilink")[陳方安生](../Page/陳方安生.md "wikilink")。雖然葉劉淑儀落敗，但她在[2008年香港立法會選舉中成功當選](../Page/2008年香港立法會選舉.md "wikilink")。

[2008年香港立法會選舉](../Page/2008年香港立法會選舉.md "wikilink")，建制派直選減少一席，直選議席與民主派的比例是11：19，但憑功能組別繼續控制立法會。

2011年，葉劉淑儀與[史泰祖及](../Page/史泰祖.md "wikilink")[田北辰創立](../Page/田北辰.md "wikilink")[新民黨](../Page/新民黨_\(香港\).md "wikilink")，以中產及專業界別選民為對象。近年來，愈來愈多標榜「獨立」的建制派人士參選立法會，但被指獲[中聯辦支持](../Page/中聯辦.md "wikilink")。\[7\]

[2011年香港區議會選舉](../Page/2011年香港區議會選舉.md "wikilink")，由於成功建立重視地區工作的形象，建制派「大獲全勝」，全取十八區區議會的控制權。近年民建聯及建制派社團坐大，在區議會和立法會均坐擁最多議席，行政會議、局長、副局長及政治助理均有其成員，因而成為泛民主派的最大政治對手。

[2012年香港立法會選舉](../Page/2012年香港立法會選舉.md "wikilink")，雖然總得票仍不及泛民主派，但直選議席比例因泛民互搶選票導致分薄票源，由11:19變為17:18，取得五個新增地方直選議席，連同佔優的傳統功能組別議席，維持立法會的多數。選後，經民聯及新民黨加入行政會議，自由黨拒絕加入。不過，若是計算「反梁」及「親梁」兩大派別，連同堅定「反梁」的泛民主派議員，加上「反梁」的自由黨5名議員及獨立[林大輝](../Page/林大輝.md "wikilink")，「反梁」及「親梁」人數近乎相同。

[梁振英於](../Page/梁振英.md "wikilink")2012年7月成為香港第四任行政長官後，泛民主派指控梁振英報復反梁陣營，下「封殺令」禁止司、局長級官員出席自由黨2013年12月舉行的20周年黨慶酒會，田北俊因此直言梁振英「小器」。

梁振英上台後，逐漸出現了其他[激進建制派組織](../Page/激進建制派.md "wikilink")，如[愛港之聲](../Page/愛港之聲.md "wikilink")、[幫港出聲](../Page/幫港出聲.md "wikilink")、[保衛香港運動](../Page/保衛香港運動.md "wikilink")、「正義聯盟」等。由於他們的態度比一般建制派激進，以及積極與泛民主派打對台，例如「參與」民陣舉行的[七一大遊行](../Page/七一大遊行.md "wikilink")（在示威時喊挑釁口號），公開表明撐[香港警察](../Page/香港警察.md "wikilink")、親中共、反對港獨等，並在泛民主派發起的示威遊行中出現並辱罵泛民主派的支持者，需要警方進行分隔。

2015年6月18日，立法會第二天審議[政改方案](../Page/2016年及2017年香港政治制度改革.md "wikilink")，中午約12時開始表決，但大批建制派議員在表決前突然離場，政改方案最終被大比數否決。建制派議員表決後見記者，稱是為了等身體不適的劉皇發前來才集體離場，劉皇發亦就自己遲到一事道歉。

2015年末，原屬公民黨的立法會議員[湯家驊](../Page/湯家驊.md "wikilink")，退出公民黨並辭去立法會議席，改為成立中間派組織[民主思路](../Page/民主思路.md "wikilink")，並出任召集人。

[2016年香港立法會選舉](../Page/2016年香港立法會選舉.md "wikilink")，建制派支持政府以「確認書」方式，阻止支持[香港獨立的候選人參與立法會選舉](../Page/香港獨立.md "wikilink")。民主思路亦派出兩條名單出選香港島和新界東，可是兩者最終分別以10,028票及8,084票低票落選並被沒收選舉按金。最終建制派在立法會選舉中維持約四成選票，地區直選取得16席，功能組別取得24席。

因為[香港立法會宣誓風波](../Page/香港立法會宣誓風波.md "wikilink")，6名民主派議員被[香港高等法院取消資格](../Page/香港高等法院.md "wikilink")，建制派在香港回歸後首次取得[地區直選的過半數議席](../Page/地區直選.md "wikilink")，並在[2018年3月香港立法會補選中勝出](../Page/2018年3月香港立法會補選.md "wikilink")[九龍西選區及另一功能界別議席](../Page/九龍西選區.md "wikilink")，維持地區直選及功能界別的過半數議席，及後同一年11月獲得[九龍西地方選區補選席次後](../Page/2018年11月香港立法會九龍西地方選區補選.md "wikilink")，在地區直選共獲得18席次，獲得穩定的分組點票否決權，在兩邊獲得穩固的多數形態。

## 選舉

### 立法會選舉

<table>
<caption>歷屆立法會選舉數字</caption>
<thead>
<tr class="header">
<th><p>選舉</p></th>
<th><p>地區直選得票</p></th>
<th><p>地區直選得票比例</p></th>
<th><p>地區直選議席</p></th>
<th><p>功能界別議席</p></th>
<th><p>超級區議會議席</p></th>
<th><p>選前議席變化</p></th>
<th><p>選舉取得議席</p></th>
<th><p>選後總議席變化</p></th>
<th><p>+/-</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/1998年香港立法會選舉.md" title="wikilink">1998年</a></p></td>
<td><p>449,968</p></td>
<td><p>30.38%</p></td>
<td><p>5</p></td>
<td><p>35</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>40</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2000年香港立法會選舉.md" title="wikilink">2000年</a></p></td>
<td><p>461,048</p></td>
<td><p>34.94%</p></td>
<td><p>8</p></td>
<td><p>31</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>1</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2000年香港立法會香港島地方選區補選.md" title="wikilink">2000年<br />
<small>(香港島補選)</small></a></p></td>
<td><p>78,282</p></td>
<td><p>37.63%</p></td>
<td><p>0</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>1</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2004年香港立法會選舉.md" title="wikilink">2004年</a></p></td>
<td><p>661,972</p></td>
<td><p>37.40%</p></td>
<td><p>12</p></td>
<td><p>23</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>3</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2007年香港立法會香港島地方選區補選.md" title="wikilink">2007年<br />
<small>(香港島補選)</small></a></p></td>
<td><p>137,550</p></td>
<td><p>42.89%</p></td>
<td><p>0</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>1</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2008年香港立法會選舉.md" title="wikilink">2008年</a></p></td>
<td><p>601,824</p></td>
<td><p>39.71%</p></td>
<td><p>11</p></td>
<td><p>25</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>2</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2012年香港立法會選舉.md" title="wikilink">2012年</a></p></td>
<td><p>772,487</p></td>
<td><p>42.66%</p></td>
<td><p>17</p></td>
<td><p>23</p></td>
<td><p>2</p></td>
<td></td>
<td></td>
<td></td>
<td><p>6</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2016年香港立法會新界東地方選區補選.md" title="wikilink">2016年<br />
<small>(新界東補選)</small></a></p></td>
<td><p>150,329</p></td>
<td><p>34.75%</p></td>
<td><p>0</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>0</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2016年香港立法會選舉.md" title="wikilink">2016年</a></p></td>
<td><p>871,016</p></td>
<td><p>40.17%</p></td>
<td><p>16</p></td>
<td><p>22</p></td>
<td><p>2</p></td>
<td></td>
<td></td>
<td></td>
<td><p>2</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2018年3月香港立法會補選.md" title="wikilink">2018年3月<br />
<small>(補選)</small></a></p></td>
<td><p>388,017</p></td>
<td><p>43.19%</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>2</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2018年11月香港立法會九龍西地方選區補選.md" title="wikilink">2018年11月<br />
<small>(補選)</small></a></p></td>
<td><p>106,457</p></td>
<td><p>49.52%</p></td>
<td><p>1</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>1</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 已停止舉辦的選舉

#### 立法局選舉

<table>
<caption>歷屆立法局選舉數字</caption>
<thead>
<tr class="header">
<th><p>選舉</p></th>
<th><p>地區直選得票</p></th>
<th><p>地區直選得票比例</p></th>
<th><p>地區直選議席</p></th>
<th><p>功能界別議席</p></th>
<th><p>選舉委員會議席</p></th>
<th><p>議席</p></th>
<th><p>+/-</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/1991年香港立法局選舉.md" title="wikilink">1991年</a></p></td>
<td><p>262,325</p></td>
<td><p>19.16%</p></td>
<td><p>1</p></td>
<td><p>10</p></td>
<td></td>
<td></td>
<td><p>11</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1995年香港立法局選舉.md" title="wikilink">1995年</a></p></td>
<td><p>278,850</p></td>
<td><p>30.58%</p></td>
<td><p>3</p></td>
<td><p>20</p></td>
<td><p>6</p></td>
<td></td>
<td><p>18</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 参考文献

## 參見

  - [香港親共團體](../Page/香港親共團體.md "wikilink")
  - [香港建制派列表](../Page/香港建制派列表.md "wikilink")
  - [民主派 (香港)](../Page/民主派_\(香港\).md "wikilink")
  - [中間派 (香港)](../Page/中間派_\(香港\).md "wikilink")
  - [激進建制派](../Page/激進建制派.md "wikilink")

{{-}}

[Category:香港特定人群稱謂](../Category/香港特定人群稱謂.md "wikilink")
[Category:香港政治](../Category/香港政治.md "wikilink")
[建制派](../Category/建制派.md "wikilink")
[中國民族主義](../Category/中國民族主義.md "wikilink")

1.
2.
3.
4.
5.
6.
7.  [立會選舉無間道——配票機器是怎樣煉成的](https://archive.is/20130105104404/hk.news.yahoo.com/%E7%AB%8B%E6%9C%83%E9%81%B8%E8%88%89%E7%84%A1%E9%96%93%E9%81%93-%E9%85%8D%E7%A5%A8%E6%A9%9F%E5%99%A8%E6%98%AF%E6%80%8E%E6%A8%A3%E7%85%89%E6%88%90%E7%9A%84-211505640.html)
    明報 2012-09-16