《**至Net奇兵：追求無限**》（''，）是[至Net奇兵的第二款遊戲](../Page/至Net奇兵.md "wikilink")，由[The
Game
Factory製作](../Page/The_Game_Factory.md "wikilink")，在2007年11月16日於美國推出在[Wii平臺的](../Page/Wii.md "wikilink")[動作遊戲](../Page/動作遊戲.md "wikilink")，之後亦於[澳洲及](../Page/澳洲.md "wikilink")[歐洲推出](../Page/歐洲.md "wikilink")。此遊戲的故事情節概括《[至Net奇兵](../Page/至Net奇兵.md "wikilink")》[動畫版的第四輯](../Page/動畫.md "wikilink")。

而這款遊戲在2008年7月21日推出在[Playstation
Portable及](../Page/Playstation_Portable.md "wikilink")[Playstation
2這兩個遊戲平臺上](../Page/Playstation_2.md "wikilink")。

## 故事

威廉因為被山拿控制而成為山拿的手下，為了令威廉回復清醒，所以Lyoko小組的五名Lyoko戰士——亞烈達、傑理明、治狼、尤美及阿奇便繼續和邪惡的山拿戰鬥，希望能夠令威廉擺脫山拿控制回復清醒以及消滅山拿。

## 遊戲方式

[至NET奇兵：探索無限遊戲截圖.jpg](https://zh.wikipedia.org/wiki/File:至NET奇兵：探索無限遊戲截圖.jpg "fig:至NET奇兵：探索無限遊戲截圖.jpg")
本遊戲和《[至Net奇兵](../Page/至Net奇兵.md "wikilink")》的動畫版一樣，一共分為現實世界和虛擬世界兩部份，在現實世界的部份是以第一人稱的角度遊戲，玩家可選擇玩不玩現實世界的部份；而在虛擬世界的部份則是以第三人稱的角度遊戲。

[C](../Category/2007年电子游戏.md "wikilink")
[C](../Category/Wii遊戲.md "wikilink")
[C](../Category/PlayStation_Portable游戏.md "wikilink")
[C](../Category/PlayStation_2游戏.md "wikilink")
[C](../Category/動作遊戲.md "wikilink")
[Category:至Net奇兵](../Category/至Net奇兵.md "wikilink")