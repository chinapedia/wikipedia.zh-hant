[thumb](../Page/文件:Daiton_National_2sen_stamp.JPG.md "wikilink")
**大屯國立公園**\[1\]，為[日治時期成立於](../Page/台灣日治時期.md "wikilink")[台灣的](../Page/台灣.md "wikilink")[國家公園](../Page/國家公園.md "wikilink")。範圍約為今**[陽明山國家公園](../Page/陽明山國家公園.md "wikilink")**與[北海岸及觀音山國家風景區](../Page/北海岸及觀音山國家風景區.md "wikilink")。

## 歷史

  - 1937年 -
    12月27日，[台灣總督府國立公園委員會指定](../Page/台灣總督府.md "wikilink")[大屯](../Page/大屯山.md "wikilink")、[觀音兩山一帶為國立公園](../Page/觀音山_\(新北市\).md "wikilink")。
  - 1941年 -
    太平洋戰爭爆發，台灣總督府廢止「台灣國立公園委員會官制」。（[台灣國家公園網站](http://np.cpami.gov.tw/index.php?option=com_content&task=view&id=2042&Itemid=10200)，21\~30年）
  - 1945年 - 日本戰敗，「大屯國立公園」廢止。
  - 1985年 - 大屯山一帶成立「陽明山國家公園」。
  - 2002年 - 觀音山連同台北縣濱海鄉鎮一帶成立「北海岸及觀音山國家風景區」。

## 關聯區域

昭和12年（1937年）當時

  - [台北州](../Page/台北州.md "wikilink")（8,265公頃，100.0%）
      - [七星郡](../Page/七星郡.md "wikilink")：[士林街](../Page/士林街.md "wikilink")、[北投庄](../Page/北投庄.md "wikilink")
      - [淡水郡](../Page/淡水郡.md "wikilink")：[淡水街](../Page/淡水街.md "wikilink")、[三芝庄](../Page/三芝庄.md "wikilink")、[八里庄](../Page/八里庄.md "wikilink")
      - [新莊郡](../Page/新莊郡.md "wikilink")：[五股庄](../Page/五股庄.md "wikilink")

## 關聯項目

  - [台北市](../Page/台北市.md "wikilink")
  - [新北市](../Page/新北市.md "wikilink")

## 參考資料

## 外部連結

  - [陽明山國家公園](http://www.ymsnp.gov.tw/)
  - [北海岸及觀音山國家風景區](http://www.northguan-nsa.gov.tw/)

[Category:1937年台灣建立](../Category/1937年台灣建立.md "wikilink")
[Category:1945年台灣廢除](../Category/1945年台灣廢除.md "wikilink")
[D大](../Category/日本国立公园.md "wikilink")
[Category:台灣日治時期國立公園](../Category/台灣日治時期國立公園.md "wikilink")
[Category:台北州](../Category/台北州.md "wikilink")
[Category:陽明山國家公園](../Category/陽明山國家公園.md "wikilink")

1.  [大屯國立公園的成立與台灣國家公園的濫觴](http://www.ymsnp.gov.tw/web/view_20year.aspx?f=1d.htm)，雙十年華話草山－影像回顧展。