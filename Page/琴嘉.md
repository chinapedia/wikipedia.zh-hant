**琴嘉**（全名：JiJaYanin
Vismitananda，1984年3月31日－），出生於[泰國](../Page/泰國.md "wikilink")[曼谷](../Page/曼谷.md "wikilink")，是一位泰國著名[女演員](../Page/女演員.md "wikilink")。琴嘉在代表作的電影《[致命巧克力](../Page/致命巧克力.md "wikilink")》中使出大量[泰拳](../Page/泰拳.md "wikilink")[跆拳道及高難度的危險動作](../Page/跆拳道.md "wikilink")，因而聲名大噪，更獲得「泰國功夫小天后」之稱號。

## 個人風格及特色

琴嘉的功夫主要以[跆拳道為主](../Page/跆拳道.md "wikilink")，拍攝電影過程中亦會親身上陣，做出危險的高難度動作，不會使用替身代替，因此拍攝時曾多次受傷。電影《[致命巧克力](../Page/致命巧克力.md "wikilink")》上映時獲得觀眾好評，票房也獲得成功。此外，琴嘉因強勁俐落的好身手成為新一代的動作演員，在國外亦繼[東尼嘉之後](../Page/東尼嘉.md "wikilink")，另一個能代表泰國的動作女演員。

## 電影作品

  - 2008年《[致命巧克力](../Page/致命巧克力.md "wikilink")》
  - 2009年《[致命火鳳凰](../Page/致命火鳳凰.md "wikilink")》
  - 2011年《[拳霸家族](../Page/拳霸家族.md "wikilink")》\[1\]
  - 2011年《[致命風火輪](../Page/致命風火輪.md "wikilink")》（This Girl Is
    Bad-Ass）\[2\]
  - 2013年《[冬蔭功2](../Page/冬蔭功2.md "wikilink")》
  - 2019年《[三重威脅之跨國大營救](../Page/三重威脅之跨國大營救.md "wikilink")》

## 參考

## 外部連結

  - [Jeeja Yanin
    Facebook專頁](https://www.facebook.com/Jeejayaninofficial)

[Category:泰國演員](../Category/泰國演員.md "wikilink")

1.  [泡菜拳霸(The Kick) -
    Yahoo\!奇摩電影](http://tw.movie.yahoo.com/movieinfo_main.html/id=4228)
2.  [致命風火輪 The Girl Is Badass -- @movies【開眼電影】
    @movies](http://app.atmovies.com.tw/movie/fgti52595182/)