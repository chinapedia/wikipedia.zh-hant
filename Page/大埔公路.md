**大埔公路**（），暱稱**舊路**，1902年落成\[1\]，是[香港](../Page/香港.md "wikilink")[新界地區第一條落成的](../Page/新界.md "wikilink")[公路](../Page/公路.md "wikilink")，全長近22公里，現時共分為9段。大埔公路南起[九龍西部的](../Page/九龍.md "wikilink")[深水埗](../Page/深水埗.md "wikilink")，繞經[新界東主要地區](../Page/新界東.md "wikilink")（[沙田](../Page/沙田.md "wikilink")、[大埔](../Page/大埔.md "wikilink")），現時止於大埔[林村](../Page/林村.md "wikilink")。過去大埔公路作為新界地區舊環迴公路的東線，終點曾經北至[粉嶺](../Page/粉嶺.md "wikilink")，連接西線[青山公路組成來往新界全境的交通要道](../Page/青山公路.md "wikilink")。

## 歷史及概要

1899年，[英國依](../Page/英國.md "wikilink")《[展拓香港界址專條](../Page/展拓香港界址專條.md "wikilink")》接管[深圳河以南](../Page/深圳河.md "wikilink")、後被統稱為「新界」的土地，成為[英屬香港的一部分](../Page/英屬香港.md "wikilink")。基於軍事佈防及發展新土地的需要，[香港政府隨即為新界地區的交通基建進行規劃](../Page/英屬香港.md "wikilink")，其中包括興建連結新界各區及九龍市區的公路，而大埔公路於1902年率先落成，成為[新界地區第一條落成的公路](../Page/新界.md "wikilink")。隨著青山公路於稍後通車，此後大半個世紀，大埔公路與青山公路分別以東、西兩線，合組為新界地區的舊環迴公路，是連接九龍與新界僅有的兩條公路。

大埔公路由處於九龍舊市區邊界的[深水埗](../Page/深水埗.md "wikilink")[界限街交界處接續](../Page/界限街.md "wikilink")[彌敦道起](../Page/彌敦道.md "wikilink")，於[桂林街交界分出青山公路後](../Page/桂林街.md "wikilink")，沿[新九龍](../Page/新九龍.md "wikilink")（現九龍一部分）西部及新界東部而建，途經[長沙灣](../Page/長沙灣.md "wikilink")、[金山](../Page/金山.md "wikilink")、[大圍](../Page/大圍.md "wikilink")、[沙田](../Page/沙田.md "wikilink")、[火炭](../Page/火炭.md "wikilink")、[馬料水等地向北伸延](../Page/馬料水.md "wikilink")，經過當時新界的行政中心[大埔](../Page/大埔.md "wikilink")，直至[粉嶺終結](../Page/粉嶺.md "wikilink")，再次連接[青山公路](../Page/青山公路.md "wikilink")，通往新界西部及返回九龍。

大埔公路與青山公路的分段安排相似，落成時共有12段：位於新界的各路段均會附加地區後綴以作辨識，如「大埔公路－沙田段」；而位於九龍市區的一段在中文則會稱為「[大埔道](../Page/大埔道.md "wikilink")」（而英文名稱不變），且不設地區後綴（不論中英）。此外，兩條公路往日沿路亦設有多塊里程碑，但缺乏保護下，至今僅存數塊\[2\]。里程碑所示長度以英里計算，在早年新界沿路標誌性建築不多的情況下，有市民會以大埔公路多少「咪」（即[英里之粵語音譯](../Page/英里.md "wikilink")）表達所在地，但流通性遠遠不及青山公路。

1910年，[九廣鐵路](../Page/九廣鐵路.md "wikilink")（英段）（現[東鐵綫](../Page/東鐵綫.md "wikilink")）通車，採用與大埔公路相近的新界東路線北上。故此，往日大埔公路大圍段近銅鑼灣村、沙田段與馬料水段交界、大埔滘段近元州仔、粉嶺段近和合石及粉嶺圍都曾設有平交道。當火車即將駛至，都會發出「咇咇」聲響並隨即下閘，車輛便須停下讓火車通過。[1980年代鐵路電氣化](../Page/1980年代.md "wikilink")，各平交道被取消或改建為橫跨鐵路的行車天橋，自此車輛不再需要駛經鐵路平交道。

1936年，大埔墟內的大埔公路被更名為大埔大街，及後再更名為[廣福道](../Page/廣福道.md "wikilink")\[3\]。[1980年代](../Page/1980年代.md "wikilink")，政府於[大埔新市鎮興建](../Page/大埔新市鎮.md "wikilink")[太和邨](../Page/太和邨.md "wikilink")，[林村河北岸大部分廣福道消失](../Page/林村河.md "wikilink")。同期，政府亦發展[粉嶺／上水新市鎮](../Page/粉嶺／上水新市鎮.md "wikilink")，大埔公路粉嶺段及部分大窩段被改建、更名或撤銷，亦已消失，致使大埔公路現已縮短至大埔林錦公路交匯處，並不再連接青山公路新界終點。[2010年代](../Page/2010年代.md "wikilink")，為配合興建[青沙公路](../Page/青沙公路.md "wikilink")，大圍段與沙田段不再連接。種種發展因素，令大埔公路現時只剩下不連續的9段。

大埔公路是傳統上來往新界東及九龍的必經要道，至今部分路段仍是重要幹道。軍事方面，[駐港英軍曾設有](../Page/駐港英軍.md "wikilink")[深水埗軍營](../Page/深水埗軍營.md "wikilink")（現[西九龍中心及](../Page/西九龍中心.md "wikilink")[麗閣邨](../Page/麗閣邨.md "wikilink")）及[天祥軍營](../Page/天祥軍營.md "wikilink")（現[北區醫院](../Page/北區醫院.md "wikilink")）\[4\]，掌握大埔公路與青山公路的九龍起點與新界終點，兩者早年之軍事重要性可見一斑；及至[二次世界大戰的](../Page/二次世界大戰.md "wikilink")[香港保衞戰期間](../Page/香港保衞戰.md "wikilink")，[駐港英軍亦曾派士兵摧毀大埔公路的基建](../Page/駐港英軍.md "wikilink")，作為拖延[日軍南進的戰略之一](../Page/日軍.md "wikilink")。郵政方面，位於[油麻地](../Page/油麻地.md "wikilink")[彌敦道的九龍中央郵政局曾是兼管新界各地的郵政中樞](../Page/彌敦道.md "wikilink")，每天早上從該處出發的郵車，就由彌敦道起分東西兩線，分別沿大埔公路及青山公路環繞新界一周，收集新界各地於郵筒投寄的信件\[5\]。至今，大埔公路沿途仍存留少量歷史悠久、仍在使用中的舊式郵筒\[6\]。

時至今日，大部分新界東來往九龍的路面交通仍需途經沙田段，縱使該段已擴建為近代[快速公路](../Page/快速公路.md "wikilink")，但仍經常出現嚴重交通擠塞。而其他路段隨著沿途各區的新快速公路及行車隧道（包括[獅子山隧道](../Page/獅子山隧道.md "wikilink")、[尖山隧道及沙田嶺隧道](../Page/尖山隧道及沙田嶺隧道.md "wikilink")、[吐露港公路等](../Page/吐露港公路.md "wikilink")）相繼落成，其作為區域幹道的重要性才大大減低。踏入[2010年代](../Page/2010年代.md "wikilink")，政府亦有計劃繼續改善大埔公路沙田段以應付增長的車流。另外，在正常情況下，來往九龍和新界東的第二、第二及第五類危險品車輛必須行駛大埔公路。

## 特點

大埔公路車速限制有每小時50、70及80公里三種，大埔道、琵琶山段南段部分、沙田嶺段北段部分、元洲仔段及大窩段均為50公里；琵琶山段北段部分、沙田嶺段南段部分、大圍段、馬料水段及大埔滘段為70公里；而沙田段則為限速80公里的[快速公路](../Page/快速公路.md "wikilink")。

比較特別的是，“大埔道”和 “大埔公路”門牌編排是不同方向的，大埔道是由南向北由小至大編配，大埔道以東為雙數 (2-488)，大埔道以西為單數
(13-389)。

而大埔公路則由北向南由小至大編配，大埔公路以東為單數 (3985-8623)，大埔道以西為雙數 (2800-8696)。

由於“大埔道”和“大埔公路”英文是相同，事實上“大埔道” 和
“大埔公路”為同一道路，所以大埔道的門牌會出現大埔道以西由門牌389接8696，以東則由488接8623的奇怪現象。

## 分段介紹

### 九龍西

#### 大埔道

大埔道由[彌敦道](../Page/彌敦道.md "wikilink")、[界限街](../Page/界限街.md "wikilink")、[長沙湾道交界以北起](../Page/長沙湾道.md "wikilink")，經過[深水埗市區](../Page/深水埗.md "wikilink")，有分層交匯處連接[呈祥道](../Page/呈祥道.md "wikilink")，全段為雙程行車。大埔道於[郝德傑道後通往琵琶山段](../Page/郝德傑道.md "wikilink")。

<File:Tai> Po Road (Hong Kong).jpg|大埔道的南端起始位置 <File:Tai> Po Road Sham
Shui Po Section.jpg|大埔道近深水埗 <File:HK> Tai Po Road n Pak Tin Street
Garden Bakery a.jpg|大埔道近[嘉頓烘焙中心](../Page/嘉頓麵包.md "wikilink") <File:Tai>
Po Road 2011.jpg|大埔道近路德會救主堂

### 新界東

#### 琵琶山段

由[郝德傑道起](../Page/郝德傑道.md "wikilink")，經過[金山郊野公園](../Page/金山郊野公園.md "wikilink")（馬騮山）至[山坳以](../Page/山坳.md "wikilink")[金山路為界與沙田嶺段相接](../Page/金山路.md "wikilink")，是連接九龍與新界東的主要道路，全段為不分隔雙程行車。

<File:HK> Tai Po Road Pipers Hill Section.jpg|大埔公路－琵琶山段

#### 沙田嶺段

上接琵琶山段，經過沙田嶺山腰、徑口路，至沙田下城門道，是連接九龍與新界東的主要道路，全段為不分隔雙程行車。

由於本段公路夜間較少車輛經過，因此亦成為[非法賽車黑點](../Page/非法賽車.md "wikilink")（尤其是[澳門格蘭披治大賽車舉行前夕](../Page/澳門格蘭披治大賽車.md "wikilink")）。

<File:HK> Tai Po Road Shatin Height Section.jpg|大埔公路－沙田嶺段

#### 大圍段

大埔公路－大圍段由[美田路至](../Page/美田路.md "wikilink")[沙田正街近](../Page/沙田正街.md "wikilink")[香港文化博物館](../Page/香港文化博物館.md "wikilink")，屬間斷路段。

該段路自[青沙公路通車後](../Page/青沙公路.md "wikilink")，東西行方向行車線被分隔開。現時[大圍新村前](../Page/大圍新村.md "wikilink")，往沙田方向的一段道路，已連接至新建[青沙公路大圍段的高架天橋往](../Page/青沙公路.md "wikilink")[大埔公路沙田段方向](../Page/大埔公路沙田段.md "wikilink")。往九龍方向則保留在青沙公路天橋底下，直至[美田路為止](../Page/美田路.md "wikilink")。為改善交通，現時大埔公路沙田段已不能通往大圍段。

由於近大圍新村的一段路段，在改道後並不設行人路，行人如要步行前往大圍新村或[富山公眾殮房](../Page/富山公眾殮房.md "wikilink")，需經由[積運街](../Page/積運街.md "wikilink")[宣道會鄭榮之中學旁的樓梯前往](../Page/宣道會鄭榮之中學.md "wikilink")。

<File:Tai> Wai Nullah 09.jpg|大埔公路－大圍段經過重建和改道 <File:Tai> Po Road, Tai Wai
section (Hong
Kong).jpg|前方建築物為[香港文化博物館](../Page/香港文化博物館.md "wikilink")，大埔公路－大圍段在其後方

#### 沙田段

由[沙田市中心至](../Page/沙田市中心.md "wikilink")[沙田馬場](../Page/沙田馬場.md "wikilink")，為快速公路。此段南端原本連接大圍段，但經常擠塞，自[青沙公路通車後](../Page/青沙公路.md "wikilink")，當局為了改善擠塞情況，遂於2014年6月8日將蔚景園旁邊一段大埔公路永久封閉，現時沙田段已不再連接大圍段，只連接[城門隧道公路及](../Page/城門隧道公路.md "wikilink")[青沙公路](../Page/青沙公路.md "wikilink")。前往大圍段須途經沙田市中心的路段前往。

無論由此段道路前往大埔公路－沙田嶺段還是[青沙公路往九龍方向](../Page/青沙公路.md "wikilink")，均需先利用九號幹線1號出口駛入[青沙公路再選擇相應行車線](../Page/青沙公路.md "wikilink")。沙田段由[吐露港公路至城門隧道公路一段屬於](../Page/吐露港公路.md "wikilink")[九號幹線](../Page/九號幹線.md "wikilink")。

沙田段[城門隧道公路至](../Page/城門隧道公路.md "wikilink")[沙田路一段是雙程二線分隔行車](../Page/沙田路.md "wikilink")，而[沙田路至](../Page/沙田路.md "wikilink")[吐露港公路一段是雙程三線分隔行車](../Page/吐露港公路.md "wikilink")。沙田段[火炭路至](../Page/火炭路.md "wikilink")[吐露港公路一段是](../Page/吐露港公路.md "wikilink")[快速公路](../Page/快速公路.md "wikilink")。

<File:Tai> Po Road Shatin Section
201403.jpg|大埔公路－沙田段近[新城市廣場和港鐵](../Page/新城市廣場.md "wikilink")[沙田站](../Page/沙田站.md "wikilink")
TaiPoRoad Shatin.jpg|大埔公路－沙田段近[沙田馬場](../Page/沙田馬場.md "wikilink")

#### 何東樓段

此段現已消失。何東樓段原為[火炭路至](../Page/火炭路.md "wikilink")[沙田馬場之間一段的大埔公路](../Page/沙田馬場.md "wikilink")，因位處火炭[何東樓對開而得名](../Page/何東樓.md "wikilink")。此段現已併入了沙田段。\[7\]

#### 馬料水段

此段離開高速公路範圍，經過馬場、馬料水、[中文大學至大埔尾](../Page/中文大學.md "wikilink")，並與大埔滘段幾乎成90度，主要服務[九肚山及附近一帶村落居民](../Page/九肚山.md "wikilink")，自[吐露港公路通車起已取代其出入新界北的地位](../Page/吐露港公路.md "wikilink")。全段為不分隔雙程行車。此段路以北地方開始是[**<span style="color: #00B14F;">新界的士</span>**可經營範圍](../Page/香港的士.md "wikilink")（但新界的士可經校門進入中大校園）。

在最初的道路規劃中，本段公路將會擴闊、拉直成高速公路（在中大建校時的校園規劃，亦有為此預留空間）；後來，亦有中大學生代表提議在校門建造一條橫跨公路的行人天橋，方便師生出入（尤其是夜間返回學生宿舍的宿生）（但最後上述工程沒有推行）。

由於本段公路夜間較少車輛經過，因此亦成為非法賽車黑點（尤其是舉行澳門格蘭披治大賽車時），相關嘈音遠至中大校園內亦可聽見。

[File:HK_TaiPoRoad_MaLiuShuiSection.JPG|大埔公路－馬料水段](File:HK_TaiPoRoad_MaLiuShuiSection.JPG%7C大埔公路－馬料水段)

#### 大埔滘段

由[大埔尾](../Page/大埔尾_\(香港\).md "wikilink")（並於此與馬料水段幾乎成90度），經[大埔滘至](../Page/大埔滘.md "wikilink")[黃宜凹](../Page/黃宜凹.md "wikilink")，主要服務附近一帶村落居民，自[吐露港公路通車起已取代其出入新界北的地位](../Page/吐露港公路.md "wikilink")。全段為不分隔雙程行車。
2018年2月10日傍晚約6時15分，一輛[872號線雙層](../Page/九龍巴士872線.md "wikilink")[九巴接載從](../Page/九巴.md "wikilink")[沙田馬場離開的馬迷往](../Page/沙田馬場巴士總站.md "wikilink")[大埔中心](../Page/大埔中心巴士總站.md "wikilink")，途經大埔公路大埔滘段時，疑因車速過快，失控翻側路邊並壓毀大埔尾[巴士站](../Page/巴士.md "wikilink")，造成17名乘客及1名侯車市民當場死亡，1人送往[聯合醫院搶救無效](../Page/聯合醫院.md "wikilink")，另66人受傷，為香港最多人死亡車禍第2位。\[8\]

[File:HK_TaiPoRoad_TaiPoKauSection.JPG|大埔公路－大埔滘段](File:HK_TaiPoRoad_TaiPoKauSection.JPG%7C大埔公路－大埔滘段)

#### 元洲仔段

上接大埔滘段，由黃宜凹起經過廣福迴旋處至[廣福道和](../Page/廣福道.md "wikilink")[南運路交界連接廣福道](../Page/南運路.md "wikilink")，進入[大埔墟範圍](../Page/大埔墟.md "wikilink")。

此段是昔日鐵路的[平交道](../Page/平交道.md "wikilink")，當時九廣鐵路（現在的港鐵[東鐵綫](../Page/東鐵綫.md "wikilink")）部份路軌會橫跨此段，此段當時有兩閘，當有火車駛近，兩閘便會降下，並發出咇咇咇聲響及截停駛近車輛，或通知跨越路軌車輛盡快離開。當火車駛過，閘會再次發出聲響並升高，讓車輛通過路軌往大埔和九龍。可是有時車輛或火車，因其它事故或壞車停在路軌上，做成不便。1983年鐵路電氣化，兴建天桥后再不需橫跨此段，車輛亦不需跨越路軌駛往大埔和九龍，此段部份路段亦因此重建。

[File:HK_TaiPoRoad_YuenChauTsaiSection.JPG|大埔公路－元洲仔段](File:HK_TaiPoRoad_YuenChauTsaiSection.JPG%7C大埔公路－元洲仔段)

#### 廣福道

廣福道原為大埔公路由南運路交界處至現[太和邨的一部分](../Page/太和邨.md "wikilink")，穿過了大埔墟的核心地區，並在[林村河上建有行車之](../Page/林村河.md "wikilink")[廣福橋](../Page/廣福橋.md "wikilink")。1936年，此段大埔公路被更名為大埔大街，及後再更名為[廣福道](../Page/廣福道.md "wikilink")\[9\]。1980年代，政府發展[大埔新市鎮](../Page/大埔新市鎮.md "wikilink")，行車之[廣福橋被拆除](../Page/廣福橋.md "wikilink")，使廣福道於林村河兩岸分成不相連之兩段\[10\]；及後興建的[太和邨亦使](../Page/太和邨.md "wikilink")[林村河北岸大部分的廣福道消失](../Page/林村河.md "wikilink")，不再和大窩段相連。

<File:Kwong> Fuk Road near Po Heung Street.jpg|廣福道

#### 大窩段

大窩段於大埔太和邨附近相接廣福道，最初北面伸延至粉嶺近[和合石相接粉嶺段](../Page/和合石.md "wikilink")。1980年代，[林村河北岸大部分的廣福道消失](../Page/林村河.md "wikilink")；而林村至和合石一部分大窩段被擴建為一條三線來回高速公路，並在旁加建大窩西支路及大窩東支路以連接沿路鄉村，但及後這段高速公路更名為[粉嶺公路](../Page/粉嶺公路.md "wikilink")。因此，現今大窩段並不途經大窩，只餘下太和邨至[林錦公路交匯處的單獨一段](../Page/林錦公路.md "wikilink")，而起點與終點皆沒有與大埔公路其他路段相連。\[11\]由於整段粉嶺段亦告消失，林錦公路交匯處就成為了大埔公路的北面終點，而整條大埔公路現時亦至此為止。

[File:HK_TaiPoRoad_TaiWoSection.JPG|大埔公路－大窩段](File:HK_TaiPoRoad_TaiWoSection.JPG%7C大埔公路－大窩段)

#### 粉嶺段

此段現已消失。粉嶺段原本由[和合石開始接連大窩段](../Page/和合石.md "wikilink")，直至[粉嶺高爾夫球場附近](../Page/香港哥爾夫球會#粉嶺高爾夫球場.md "wikilink")。往日大埔公路的終點位於與[青山公路古洞段](../Page/青山公路.md "wikilink")、[粉錦公路及原新豐路交界的十字路口](../Page/粉錦公路.md "wikilink")\[12\]，即現[粉嶺公路上水迴旋處](../Page/粉嶺公路.md "wikilink")。隨著[粉嶺／上水新市鎮的發展](../Page/粉嶺／上水新市鎮.md "wikilink")，此段現已消失，並被改建為區內各段道路。

  - 和合石至[上水警署一段成為](../Page/上水警署.md "wikilink")[馬會道的延伸部分](../Page/馬會道.md "wikilink")，並繼承了該段大埔公路的時速70公里車速限制\[13\]。
  - 上水警署至[粉嶺站一段併入了](../Page/粉嶺站.md "wikilink")[沙頭角公路龍躍頭段](../Page/沙頭角公路.md "wikilink")。
  - 粉嶺站至[粉嶺圍一段成為](../Page/粉嶺圍.md "wikilink")[新運路一部分](../Page/新運路.md "wikilink")。
  - 近粉嶺圍平交道被取消。
  - 粉嶺圍至青山公路一段則改建為[粉嶺公路](../Page/粉嶺公路.md "wikilink")。

<File:TaiPoRoad> CastlePeakRrad NT.png|青山公路及大埔公路新界終點（1945年）

## 里程碑

[HK_TaiPoRoad_11-5MilageStone.JPG](https://zh.wikipedia.org/wiki/File:HK_TaiPoRoad_11-5MilageStone.JPG "fig:HK_TaiPoRoad_11-5MilageStone.JPG")附近的11½咪[里程碑](../Page/里程碑.md "wikilink")
\]\]
大埔公路有使用「大埔公路××咪」作為地址標示，當中「咪」是英文「mile」（[英里](../Page/英里.md "wikilink")）的音譯，代表該處與[尖沙咀碼頭](../Page/尖沙咀天星碼頭.md "wikilink")（經[彌敦道](../Page/彌敦道.md "wikilink")）的距離。[青山公路和](../Page/青山公路.md "wikilink")[清水灣道也有類似系統](../Page/清水灣道.md "wikilink")。

|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |

## 未來發展

2016年7月，政府提出擴建大埔公路沙田段，將近[沙田鄉事會路至](../Page/沙田鄉事會路.md "wikilink")[禾輋邨一段長約](../Page/禾輋邨.md "wikilink")1.1公里道路，從雙程雙線分隔行車道擴闊至雙程三線分隔行車道，安裝隔音屏障，並改建其與沙田鄉事會路的交匯處，以應付日益增長的車流量，工程造價約27.4億元\[14\]。2018年1月，政府擬向立法會申請撥款8.5億元，為兩段大埔公路沙田段，包括[蔚景園至](../Page/蔚景園.md "wikilink")[連城廣場](../Page/連城廣場.md "wikilink")，以及[禾輋邨至](../Page/禾輋邨.md "wikilink")[火炭路加建隔音屏障和隔音罩](../Page/火炭路.md "wikilink")，指出工程完成後可減少噪音1至5分貝，有2147多戶居民受惠。計劃2018年第二季施工，預計2023年下半年完成。

## 相關條目

  - [青沙公路](../Page/青沙公路.md "wikilink")
  - [青山公路](../Page/青山公路.md "wikilink")
  - [吐露港公路](../Page/吐露港公路.md "wikilink")
  - [粉嶺公路](../Page/粉嶺公路.md "wikilink")

## 參考資料

[Category:香港公路](../Category/香港公路.md "wikilink")
[Category:深水埗區街道](../Category/深水埗區街道.md "wikilink")
[Category:沙田區街道](../Category/沙田區街道.md "wikilink")
[Category:大埔區街道](../Category/大埔區街道.md "wikilink")
[Category:深水埗](../Category/深水埗.md "wikilink")
[Category:石硤尾](../Category/石硤尾.md "wikilink")
[Category:蘇屋](../Category/蘇屋.md "wikilink")
[Category:大圍](../Category/大圍.md "wikilink")
[Category:沙田](../Category/沙田.md "wikilink")
[Category:火炭](../Category/火炭.md "wikilink")
[Category:馬料水](../Category/馬料水.md "wikilink")
[Category:白石角](../Category/白石角.md "wikilink")
[Category:大埔滘](../Category/大埔滘.md "wikilink") [Category:大埔
(香港)](../Category/大埔_\(香港\).md "wikilink")
[Category:粉嶺](../Category/粉嶺.md "wikilink")
[Category:上水](../Category/上水.md "wikilink")
[Category:香港之最](../Category/香港之最.md "wikilink")

1.
2.
3.
4.
5.
6.
7.  [香港地方，道路及鐵路－已消失道路](http://www.hk-place.com/view.php?id=305)
8.
9.
10.
11. [香港地方，道路及鐵路－香港公路(一)大埔公路](http://www.hk-place.com/view.php?id=302)
12. ["Hong Kong and New Territories-Tai Po", Digital Archive @ McMaster
    University
    Library](http://digitalarchive.mcmaster.ca/islandora/object/macrepo:66896)
13. [香港法例第374M章第3條時速為70公里的車速限制](http://www.legislation.gov.hk/blis_ind.nsf/CurAllChinDoc/FF0726F796E652578825648C0007D7F8?OpenDocument)
14.