**关向应**（），原名**关致祥**，又名**应禀**，笔名**始炎**、**仲冰**，曾用名**李仕真**、**郑勤**，[满族](../Page/满族.md "wikilink")。[中国](../Page/中国.md "wikilink")[辽宁金县](../Page/辽宁.md "wikilink")（今[大连市](../Page/大连市.md "wikilink")[金州区](../Page/金州区.md "wikilink")）大关家屯（现隶属[向应街道](../Page/向应街道.md "wikilink")）人，[中国共产党高级军事将领](../Page/中国共产党.md "wikilink")。

1924年加入中国社会主义青年团，次年加入[中国共产党](../Page/中国共产党.md "wikilink")。先后在[中共河南省委和共青团中央组织部工作](../Page/中共河南省委.md "wikilink")。[中共六大后任](../Page/中共六大.md "wikilink")[中国共产主义青年团中央书记](../Page/中国共产主义青年团.md "wikilink")。后从事中国工农红军军事委员会及[中共中央长江局工作](../Page/中共中央长江局.md "wikilink")。1932年，任湘鄂西军委主席和[红三军政治委员](../Page/红三军.md "wikilink")。[长征途中](../Page/长征.md "wikilink")，任[红二方面军副政治委员](../Page/红二方面军.md "wikilink")。[抗日战争时期](../Page/抗日战争.md "wikilink")，任[八路军](../Page/八路军.md "wikilink")[120师政治委员](../Page/120师.md "wikilink")，与[贺龙一起开辟了晋绥根据地](../Page/贺龙.md "wikilink")。1946年病逝于[延安](../Page/延安.md "wikilink")。

## 生平

### 早年经历

关向應是[满洲镶白旗人](../Page/镶白旗.md "wikilink")，出身[长白山瓜尔佳氏](../Page/瓜尔佳.md "wikilink")\[1\]。1922年，自当地[伏见台商业学校毕业](../Page/伏见台商业学校.md "wikilink")，进入一家日资印刷企业工作，不久与[日本雇员发生矛盾](../Page/日本.md "wikilink")，愤而辞职\[2\]。1924年4月在[李震瀛介绍下加入](../Page/李震瀛.md "wikilink")[中國社會主義青年團](../Page/中國社會主義青年團.md "wikilink")，5月随李前往[上海](../Page/上海.md "wikilink")，进入[上海大学读书](../Page/上海大学_\(民国\).md "wikilink")，并在[中国国民党第一区党部从事秘密活动](../Page/中国国民党.md "wikilink")。同年12月，被派往[苏联](../Page/苏联.md "wikilink")[莫斯科](../Page/莫斯科.md "wikilink")[东方劳动大学学习](../Page/东方劳动大学.md "wikilink")\[3\]。次年，经[陈乔年介绍](../Page/陈乔年.md "wikilink")，在莫斯科加入[中国共产党](../Page/中国共产党.md "wikilink")。1925年[五卅运动爆发后](../Page/五卅运动.md "wikilink")，关向應主动要求回国，在上海工作。8月，前往[济南](../Page/济南.md "wikilink")、[青岛负责当地共青团活动](../Page/青岛.md "wikilink")。次年初回到上海。1927年[四一二事变发生后](../Page/四一二事变.md "wikilink")，逃亡[武汉](../Page/武汉.md "wikilink")，[七一五事变后](../Page/七一五事变.md "wikilink")，被派往[河南](../Page/河南.md "wikilink")，任[中共河南省委书记](../Page/中共河南省委.md "wikilink")\[4\]。

1928年，被调回上海，后前往莫斯科出席[中国共产党第六次全国代表大会](../Page/中国共产党第六次全国代表大会.md "wikilink")，被选为[中共中央政治局候补委员](../Page/中共中央政治局.md "wikilink")、[中共中央军委委员](../Page/中共中央军委.md "wikilink")，并担任[中国共青团中央委员会书记](../Page/中国共青团.md "wikilink")\[5\]。1930年3月，关向應任中央军委书记，主持军事工作。同年冬，任[中共中央长江局书记](../Page/中共中央长江局.md "wikilink")，调上海工作\[6\]。1931年4月在上海被捕，7月被营救出狱，前往[湘鄂边苏区](../Page/湘鄂边苏区.md "wikilink")\[7\]。

[Red_Army_2.jpg](https://zh.wikipedia.org/wiki/File:Red_Army_2.jpg "fig:Red_Army_2.jpg")、[贺炳炎](../Page/贺炳炎.md "wikilink")、关向应、[王震](../Page/王震.md "wikilink")、[李井泉](../Page/李井泉.md "wikilink")、[朱瑞](../Page/朱瑞_\(1905年\).md "wikilink")、[贺龙](../Page/贺龙.md "wikilink")。后排左起[张子意](../Page/张子意.md "wikilink")、[刘亚球](../Page/刘亚球.md "wikilink")、[廖汉生](../Page/廖汉生.md "wikilink")、[朱明](../Page/朱明.md "wikilink")、[陈伯钧](../Page/陈伯钧.md "wikilink")、[卢冬生](../Page/卢冬生.md "wikilink")\]\]

### 红军时期

1932年1月，任[中共中央湘鄂西分局委员](../Page/中共中央湘鄂西分局.md "wikilink")、湘鄂西军事委员会主席、[红三军政委](../Page/红三军.md "wikilink")，同贺龙一起领导了湘鄂西革命根据地建设和红军的发展。在这期间，关向应曾执行了[肃反扩大化的路线](../Page/肃反.md "wikilink")\[8\]。1934年10月，红三军与[红六军团会师](../Page/红六军团.md "wikilink")，创建了[湘鄂川黔苏区](../Page/湘鄂川黔苏区.md "wikilink")\[9\]。11月，部队攻入湖南，并建立[湘鄂川黔边革命根据地](../Page/湘鄂川黔边革命根据地.md "wikilink")，关向应任[红二军团兼军区副政治委员](../Page/红二军团.md "wikilink")，先后参与指挥陈家河、桃子溪、忠堡、板栗园等战斗。1935年10月，[蒋介石率领](../Page/蒋介石.md "wikilink")[国民革命军采用分进合击进攻湘鄂川黔边](../Page/国民革命军.md "wikilink")。11月，他与贺龙、[任弼时等从湖南省桑植县统一指挥红二](../Page/任弼时.md "wikilink")、六军团突围国军封锁，开始[长征](../Page/长征.md "wikilink")\[10\]。

1936年2月，在[黔西县城参加中共湘鄂川黔省委会议](../Page/黔西县.md "wikilink")，决定两军团创建[川滇黔边根据地](../Page/川滇黔边根据地.md "wikilink")，之后与贺龙、任弼时率红二、六军团在[乌蒙山区与国民革命军周旋](../Page/乌蒙山.md "wikilink")。7月2日，红二、六军团到达四川[甘孜](../Page/甘孜.md "wikilink")，与[张国焘](../Page/张国焘.md "wikilink")、[徐向前的](../Page/徐向前.md "wikilink")[红四方面军会合](../Page/红四方面军.md "wikilink")\[11\]。红二、六军团及[红三十二军组成](../Page/红三十二军.md "wikilink")[红二方面军](../Page/红二方面军.md "wikilink")，关向应任红二方面军副政治委员兼[红二军副政委](../Page/红二军.md "wikilink")\[12\]，期间与张国焘作斗争\[13\]。10月，红二方面军与[红一方面军在](../Page/红一方面军.md "wikilink")[将台堡会师](../Page/将台堡.md "wikilink")，长征结束\[14\]。12月，关向应任[红二方面军政委](../Page/红二方面军.md "wikilink")、[中共中央革命军事委员会委员](../Page/中共中央革命军事委员会.md "wikilink")\[15\]。

### 抗日战争时期

[Peng_Zhen_in_1938.jpg](https://zh.wikipedia.org/wiki/File:Peng_Zhen_in_1938.jpg "fig:Peng_Zhen_in_1938.jpg")时，关向应、[彭真](../Page/彭真.md "wikilink")、[聂荣臻三人合影](../Page/聂荣臻.md "wikilink")\]\]
1937年8月22日，关向应出席[洛川会议](../Page/洛川会议.md "wikilink")，随后任命为中央军委前方分会组成人员。25日，红二方面军改编为[八路军](../Page/八路军.md "wikilink")[120师](../Page/120师.md "wikilink")，关向应任政训处主任\[16\]，同贺龙率部参加[抗日战争](../Page/抗日战争.md "wikilink")。10月，关向应率领工作团到晋西北地区开辟根据地\[17\]。1938年2月，与贺龙指挥[晋西北七城战役](../Page/晋西北七城战役.md "wikilink")，收复七座县城。1938年12月，与贺龙率120师主力开赴冀中，任冀中区总指挥部政治委员，参与指挥[齐会](../Page/齐会战斗.md "wikilink")、[陈庄等战斗](../Page/陈庄战斗.md "wikilink")\[18\]。1940年2月，关向应与贺龙率部回师晋绥。1940年11月起任晋西北军区政治委员，参加指挥[百团大战](../Page/百团大战.md "wikilink")\[19\]。

1941年，关向應患有的[肺结核病情恶化](../Page/肺结核.md "wikilink")，被迫退职回[延安休养](../Page/延安.md "wikilink")。1942年，他担任[中共中央晋绥分局书记](../Page/中共中央晋绥分局.md "wikilink")、[陕甘宁晋绥联防军政治委员](../Page/陕甘宁晋绥联防军.md "wikilink")\[20\]。1945年，在中共七大上当选中央委员。1946年7月21日，关向应在延安病逝。[毛泽东为其题的挽词是](../Page/毛泽东.md "wikilink")：“忠心耿耿，为党为国，向应同志不死”。

## 家庭

关向应第一任妻子[秦曼云](../Page/秦曼云.md "wikilink")，1928年6月与关向应结婚，1929年回国之后相继担任团中央女工部、军委秘书处机要秘书、共产国际代表联络处主任兼上海中央执行局总会计。二人有一子关拯，后下落不明\[21\]。秦曼云于1934年被捕后投降（一说后经保释出狱），后又说服中央上海中央局宣传部长[盛忠亮投降](../Page/盛忠亮.md "wikilink")，后与其结婚。

关向应第二任妻子[马丹](../Page/马丹.md "wikilink")，1938年10月10日与关向应结婚。1940年5月，马丹到兴县妇女救国会工作。[中华人民共和国成立后](../Page/中华人民共和国.md "wikilink")，曾任广东省民政厅办公室主任、省旅游局副局长\[22\]。

## 参考文献

## 外部链接

  - [永远的丰碑：关向应，新华网，2005年2月28日](http://news.xinhuanet.com/newscenter/2005-02/28/content_2629609.htm)

[Category:中国共产党第六届中央委员会委员](../Category/中国共产党第六届中央委员会委员.md "wikilink")
[Category:中国共产党第七届中央委员会委员](../Category/中国共产党第七届中央委员会委员.md "wikilink")
[Category:中国工农红军将领](../Category/中国工农红军将领.md "wikilink")
[Category:中华苏维埃共和国中央执行委员会委员](../Category/中华苏维埃共和国中央执行委员会委员.md "wikilink")
[Category:上海大学校友 (民国)](../Category/上海大学校友_\(民国\).md "wikilink")
[Category:莫斯科东方大学校友](../Category/莫斯科东方大学校友.md "wikilink")
[Category:中国共产党党员
(1925年入党)](../Category/中国共产党党员_\(1925年入党\).md "wikilink")
[Category:中华民国大陆时期满族政治人物](../Category/中华民国大陆时期满族政治人物.md "wikilink")
[Category:满洲镶白旗人](../Category/满洲镶白旗人.md "wikilink")
[G](../Category/长白山瓜尔佳氏.md "wikilink")
[Category:葬于延安](../Category/葬于延安.md "wikilink")
[Category:大连人](../Category/大连人.md "wikilink")
[X](../Category/关姓.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14.
15.
16.
17.
18.
19.
20.
21.
22.