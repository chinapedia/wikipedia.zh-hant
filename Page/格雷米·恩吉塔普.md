**格雷米·恩吉塔普**（**Geremi Sorele Njitap Fotso**，簡稱
**Geremi**，），[喀麥隆足球運動員](../Page/喀麥隆.md "wikilink")，現時是[紐卡素和](../Page/纽卡斯尔联足球俱乐部.md "wikilink")[喀麥隆國家足球隊的中場球員](../Page/喀麥隆國家足球隊.md "wikilink")，身為喀麥隆的主力中場，從無數場的征伐中自自然然的吸收了豐富的經驗，這是格雷米球技出眾的本錢，能勝任多個位置，如：防守中場、右中場、右後衛等，可謂「全能球员」。

## 生平

### 球會

謝列美曾是[皇家馬德里的球員](../Page/皇家馬德里.md "wikilink")，曾協助球隊取得2001/02年度[歐洲聯賽冠軍盃](../Page/歐洲聯賽冠軍盃.md "wikilink")。
其後外借到[英超中游份子](../Page/英超.md "wikilink")[米杜士堡](../Page/米德尔斯堡足球俱乐部.md "wikilink")，在此得到了更加之多人的認同。
於2003年，車路士以700萬[英鎊簽入謝列美](../Page/英鎊.md "wikilink")。

在車路士，謝列美多數出任右後衛，但上陣機會不多。2006/07年球季完結後轉會至紐卡素。

### 國家隊

謝利美曾參與2000年的[-{zh-hans:悉尼;
zh-hk:悉尼;}-奧運會](../Page/悉尼奧運.md "wikilink")、[2002年世界杯以及](../Page/2002年世界杯.md "wikilink")2000年、2002年、2004年[非洲國家杯](../Page/非洲國家杯.md "wikilink")，並且是喀麥隆國家和奧運足球隊取得2000年、2002年非洲國家杯和2000年-{zh-hans:悉尼;
zh-hk:悉尼;}-奧運會冠軍的主力成員。

## 參考資料

## 外部連結

  -
  - [BBC
    profile](http://news.bbc.co.uk/sport1/shared/bsp/hi/football/statistics/players/g/geremi_152715.stm)

[Category:喀麥隆足球運動員](../Category/喀麥隆足球運動員.md "wikilink")
[Category:皇家馬德里球員](../Category/皇家馬德里球員.md "wikilink")
[Category:米杜士堡球員](../Category/米杜士堡球員.md "wikilink")
[Category:車路士球員](../Category/車路士球員.md "wikilink")
[Category:紐卡素球員](../Category/紐卡素球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:喀麥隆奧林匹克運動會金牌得主](../Category/喀麥隆奧林匹克運動會金牌得主.md "wikilink")
[Category:2000年夏季奧林匹克運動會金牌得主](../Category/2000年夏季奧林匹克運動會金牌得主.md "wikilink")
[Category:波特諾山丘球員](../Category/波特諾山丘球員.md "wikilink")
[Category:拉里沙球員](../Category/拉里沙球員.md "wikilink")
[Category:足球中場](../Category/足球中場.md "wikilink")
[Category:土超球員](../Category/土超球員.md "wikilink")
[Category:西甲球員](../Category/西甲球員.md "wikilink")
[Category:英格蘭足球聯賽球員](../Category/英格蘭足球聯賽球員.md "wikilink")
[Category:2000年夏季奧林匹克運動會足球運動員](../Category/2000年夏季奧林匹克運動會足球運動員.md "wikilink")
[Category:2001年洲際國家盃球員](../Category/2001年洲際國家盃球員.md "wikilink")
[Category:2002年世界盃足球賽球員](../Category/2002年世界盃足球賽球員.md "wikilink")
[Category:2003年洲際國家盃球員](../Category/2003年洲際國家盃球員.md "wikilink")
[Category:1998年非洲國家盃球員](../Category/1998年非洲國家盃球員.md "wikilink")
[Category:2000年非洲國家盃球員](../Category/2000年非洲國家盃球員.md "wikilink")
[Category:2002年非洲國家盃球員](../Category/2002年非洲國家盃球員.md "wikilink")
[Category:2006年非洲國家盃球員](../Category/2006年非洲國家盃球員.md "wikilink")
[Category:2008年非洲國家盃球員](../Category/2008年非洲國家盃球員.md "wikilink")
[Category:2010年非洲國家盃球員](../Category/2010年非洲國家盃球員.md "wikilink")
[Category:2010年世界盃足球賽球員](../Category/2010年世界盃足球賽球員.md "wikilink")
[Category:FIFA世纪俱乐部](../Category/FIFA世纪俱乐部.md "wikilink")