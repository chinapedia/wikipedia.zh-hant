[Fr-Regions-18juil2014.svg](https://zh.wikipedia.org/wiki/File:Fr-Regions-18juil2014.svg "fig:Fr-Regions-18juil2014.svg")
[France_assembly_vote.svg](https://zh.wikipedia.org/wiki/File:France_assembly_vote.svg "fig:France_assembly_vote.svg")
**大区**（）是[法国行政区划的](../Page/法国行政区划.md "wikilink")[第一级单位](../Page/一级行政区.md "wikilink")，下分为[省](../Page/省_\(法国\).md "wikilink")。法国共有18个大区，其中13个位于法国本土（其中[科西嘉地位较特殊](../Page/科西嘉.md "wikilink")，称为“领土集体”*collectivité
territoriale*），其余5个则位于海外。

一些[法语系国家](../Page/法语系国家.md "wikilink")，如[马里](../Page/马里.md "wikilink")，一级行政区也称作**大区**（）。

## 作用

法国是一个[单一制国家](../Page/单一制.md "wikilink")，大区并不是一个[法律或规章意义上的自治体](../Page/法律.md "wikilink")。不过，它从政府接受一部分国家[税收](../Page/税收.md "wikilink")，拥有相应的[预算](../Page/预算.md "wikilink")，他的职责就是将这些资源分配使用到不同的领域。

大区最主要的功用是在建设和财政上支持法国的[高级中学](../Page/高级中学.md "wikilink")。除了这个法律职责之外，它的预算还参与到社会各个领域中，如[基础设施](../Page/基础设施.md "wikilink")、[教育](../Page/教育.md "wikilink")、[公共交通](../Page/公共交通.md "wikilink")、[科学研究](../Page/科学研究.md "wikilink")、[企业援助等](../Page/企业.md "wikilink")。

在法国，一直以来对于是否授予大区以法律[自治权存在着激烈的争议](../Page/自治.md "wikilink")；也有提议将[省议会取消](../Page/省委员会_\(法国\).md "wikilink")，而将它们并入[大区议会](../Page/大区委员会_\(法国\).md "wikilink")，并将[省作为大区的附属行政级别](../Page/省_\(法国\).md "wikilink")。不过目前并无后文。

## 行政

自从1982年颁布第一个[地方分权法案起](../Page/权力分散.md "wikilink")，大区拥有行政区域的地位，并且各由一个每6年直接[普选产生的](../Page/普选权.md "wikilink")[大区议会管理](../Page/大区委员会_\(法国\).md "wikilink")。

[科西嘉大区由地方](../Page/科西嘉.md "wikilink")[议会管理](../Page/议会.md "wikilink")。

每一个大区同样有一个[政府任命的](../Page/政府.md "wikilink")（大区的）主席，他的职责是在各省中代表政府。

## 历史

在1789年[法国大革命之前](../Page/法国大革命.md "wikilink")，[法兰西](../Page/法兰西.md "wikilink")[王国曾经在封建](../Page/王国.md "wikilink")[领地的基础上分为各个](../Page/领地.md "wikilink")[行省](../Page/行省_\(法国\).md "wikilink")，其中不少大小大致相当于现在的大区。1789年，这些行省被撤销，取而代之为83个省。

1917年8月25日发布了一份由大区理论所启发的商业部行政通告，随后于1919年4月5日，部门签发了法令，设立一些“区域经济联合体”，亦称“克雷芒（大）区”（région
Clémentel），其中第一个是东部区域（南希），它包含了现在的[洛林和](../Page/洛林.md "wikilink")[香槟](../Page/香槟.md "wikilink")。这些“经济区”在自愿的前提下，于法国本土集合了各个商会，它们能够自由地依附于自己所选的区域，也可以自由地变更选择，因此最初设计的17个大区很快就成为了21个。

1919年9月，旅游事业联合会基于这个模式构造了19个“旅游区域”，划分主要基于[地理](../Page/地理.md "wikilink")、[民族](../Page/民族.md "wikilink")、[历史和](../Page/历史.md "wikilink")[旅游的标准](../Page/旅游.md "wikilink")，并不限于各省的界限。

伴随着这些运动，最早在1915年，随后于1920年（Hennessy法案）和1921年（Charles
Rebel法案，Millerand-Marraud-Doumer法律草案）分别提出了行政权力下放的议案，也包括了建立大区宪法和成立大区议会的想法。但这些项目都没有付诸实施。

自第一次世界大战始，交通的发展、城市构造的转变以及区域理念的强化促使更多的人思考成立比省更大级别的行政区划的可能性。讽刺的是，这个[第三共和国的想法在某种程度上于](../Page/法兰西第三共和国.md "wikilink")[维希政府治下实现过](../Page/维希政府.md "wikilink")，当时[贝当元帅签署了](../Page/贝当.md "wikilink")1941年4月19日法，将一些省重新组织成了（法国历史上的）[行省](../Page/行省_\(法国\).md "wikilink")。这种组织形式在1945年贝当政权倒台后就废止了。

[戴高乐将军解放法國後于](../Page/戴高乐.md "wikilink")1944年1月10日发布命令，着手解放后法國领土的行政组织。他建立了一些行政区域，但在1946年他下台之后被放弃。

[第四共和治下的领土布置同样考虑到建立超越省级的行政组织](../Page/法兰西第四共和国.md "wikilink")。1956年10月28日颁布了一项法令，规定了22个“计划中的大区”，其界限由计划的行政总专员让·韦尔若（Jean
Vergeot）划定。这些大区仅为行政公器，不作它用。尽管他们的划定完全基于技术上的考量，但还是在很多区域与旧行省吻合。

戴高乐将军在1958年重新上台執政後，1964年，大区主席们在“区域行为的界限”框架下被任命。1969年，一项旨在扩大大区权限的[全民公决的失败导致了戴高乐再次退位](../Page/公投.md "wikilink")。这次拒绝或许解释了为何创建大区委员会的1972年7月5日法律颁布的时候，对于大区的权限施加了如此多的限制，赋予如此少的权力。然而它们被正式命名为了“大区”（région），这个称谓为后来的1982年《权力下放法案》所用。

1982年3月2日法律规定了大区议会的选举为在省的范围内，以六年为任期的直接普选。第一次选举于1986年3月16日举行。大区成为了和**省**及**市镇**同一个框架下的行政区域。

最后，法国政府于2004年3月推出一项备受争议的计划，旨在将一部分非教育领域的成员转移至大区一级。反对意见指出，大区并不具有和这一功能相匹配的预算，并且这样的措施会加剧区域间的不平等。

目前的区域划分是源于1950年的法国行政区域整合，及更早的一些计划。它一直处于争论之中。比如对于[大西洋卢瓦尔省](../Page/大西洋卢瓦尔省.md "wikilink")，一直有很强烈的呼声将其并入[布列塔尼大区](../Page/布列塔尼.md "wikilink")；将历史上的[諾曼底拆分为两个大区](../Page/諾曼底.md "wikilink")（[上诺曼底和](../Page/上诺曼底.md "wikilink")[下诺曼底](../Page/下诺曼底.md "wikilink")）也同样具有疑义，不少人鼓吹将它们合并。

2014年法国总统奥朗德推出大区合并计划，本土22个大区将合并缩减为13个大区，获得议会通过。

2016年1月1日，法國實施將本土現有22個大區合併為13個。

## 列表

<table>
<thead>
<tr class="header">
<th><p>标志</p></th>
<th><p>地图</p></th>
<th><p>名称</p></th>
<th><p>代码[1]</p></th>
<th><p>下辖地区</p></th>
<th><p>行政中心</p></th>
<th><p>人口[2]</p></th>
<th><p>面积(km2)</p></th>
<th><p>人口密度(人/km2)</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Alsace-Champagne-Ardenne-Lorraine_region_locator_map.svg" title="fig:Alsace-Champagne-Ardenne-Lorraine_region_locator_map.svg">Alsace-Champagne-Ardenne-Lorraine_region_locator_map.svg</a></p></td>
<td><p>'''<a href="../Page/阿爾薩斯-香檳-阿登-洛林.md" title="wikilink">大東部</a> '''</p></td>
<td><p>44</p></td>
<td><p><a href="../Page/上莱茵省.md" title="wikilink">上莱茵省</a> (67)<br />
<a href="../Page/下莱茵省.md" title="wikilink">下莱茵省</a> (68)<br />
<a href="../Page/阿登省.md" title="wikilink">阿登省</a> (08)<br />
<a href="../Page/奥布省.md" title="wikilink">奥布省</a> (10)<br />
<a href="../Page/马恩省.md" title="wikilink">马恩省</a> (51)<br />
<a href="../Page/上马恩省.md" title="wikilink">上马恩省</a> (52)<br />
<a href="../Page/默尔特-摩泽尔省.md" title="wikilink">默尔特-摩泽尔省</a><br />
(54)<br />
<a href="../Page/默兹省.md" title="wikilink">默兹省</a> (55)<br />
<a href="../Page/摩泽尔省.md" title="wikilink">摩泽尔省</a> (57)<br />
<a href="../Page/孚日省.md" title="wikilink">孚日省</a> (88)</p></td>
<td><p><a href="../Page/斯特拉斯堡.md" title="wikilink">斯特拉斯堡</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Aquitaine-Limousin-Poitou-Charentes_region_locator_map.svg" title="fig:Aquitaine-Limousin-Poitou-Charentes_region_locator_map.svg">Aquitaine-Limousin-Poitou-Charentes_region_locator_map.svg</a></p></td>
<td><p>'''<a href="../Page/新阿基坦大區.md" title="wikilink">新阿基坦</a> '''</p></td>
<td><p>75</p></td>
<td><p><a href="../Page/多尔多涅省.md" title="wikilink">多尔多涅省</a> (24)<br />
<a href="../Page/吉伦特省.md" title="wikilink">吉伦特省</a> (33)<br />
<a href="../Page/朗德省.md" title="wikilink">朗德省</a> (40)<br />
<a href="../Page/洛特-加龙省.md" title="wikilink">洛特-加龙省</a> (47)<br />
<a href="../Page/比利牛斯-大西洋省.md" title="wikilink">比利牛斯-大西洋省</a><br />
(64)<a href="../Page/科雷兹省.md" title="wikilink">科雷兹省</a> (19)<br />
<a href="../Page/克勒兹省.md" title="wikilink">克勒兹省</a> (23)<br />
<a href="../Page/上维埃纳省.md" title="wikilink">上维埃纳省</a> (87)<br />
<a href="../Page/夏朗德省.md" title="wikilink">夏朗德省</a> (16)<br />
<a href="../Page/滨海夏朗德省.md" title="wikilink">滨海夏朗德省</a> (17)<br />
<a href="../Page/德塞夫勒省.md" title="wikilink">德塞夫勒省</a> (79)<br />
<a href="../Page/维埃纳省.md" title="wikilink">维埃纳省</a> (86)</p></td>
<td><p><a href="../Page/波尔多.md" title="wikilink">波尔多</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Auvergne-Rhône-Alpes_region_locator_map.svg" title="fig:Auvergne-Rhône-Alpes_region_locator_map.svg">Auvergne-Rhône-Alpes_region_locator_map.svg</a></p></td>
<td><p>'''<a href="../Page/奧文尼-隆-阿爾卑斯.md" title="wikilink">奧文尼-隆-阿爾卑斯</a> '''</p></td>
<td><p>84</p></td>
<td><p><a href="../Page/阿列省.md" title="wikilink">阿列省</a> (03)<br />
<a href="../Page/康塔尔省.md" title="wikilink">康塔尔省</a> (15)<br />
<a href="../Page/上卢瓦尔省.md" title="wikilink">上卢瓦尔省</a> (43)<br />
<a href="../Page/多姆山省.md" title="wikilink">多姆山省</a> (63)<br />
<a href="../Page/安省.md" title="wikilink">安省</a> (01)<br />
<a href="../Page/阿尔代什省.md" title="wikilink">阿尔代什省</a> (07)<br />
<a href="../Page/德龙省.md" title="wikilink">德龙省</a> (26)<br />
<a href="../Page/伊泽尔省.md" title="wikilink">伊泽尔省</a> (38)<br />
<a href="../Page/卢瓦尔省.md" title="wikilink">卢瓦尔省</a> (42)<br />
<a href="../Page/罗讷省.md" title="wikilink">罗讷省</a> (69)<br />
<a href="../Page/萨瓦省.md" title="wikilink">萨瓦省</a> (73)<br />
<a href="../Page/上萨瓦省.md" title="wikilink">上萨瓦省</a> (74)</p></td>
<td><p><a href="../Page/里昂.md" title="wikilink">里昂</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Bourgogne-Franche-Comté_region_locator_map.svg" title="fig:Bourgogne-Franche-Comté_region_locator_map.svg">Bourgogne-Franche-Comté_region_locator_map.svg</a></p></td>
<td><p>'''<a href="../Page/布根地-法蘭琪-康堤.md" title="wikilink">布根地-法蘭琪-康堤</a> '''</p></td>
<td><p>27</p></td>
<td><p><a href="../Page/科多尔省.md" title="wikilink">科多尔省</a> (21)<br />
<a href="../Page/涅夫勒省.md" title="wikilink">涅夫勒省</a> (58)<br />
<a href="../Page/索恩-卢瓦尔省.md" title="wikilink">索恩-卢瓦尔省</a> (71)<br />
<a href="../Page/约讷省.md" title="wikilink">约讷省</a> (89)<br />
<a href="../Page/杜省.md" title="wikilink">杜省</a> (25)<br />
<a href="../Page/汝拉省.md" title="wikilink">汝拉省</a> (39)<br />
<a href="../Page/上索恩省.md" title="wikilink">上索恩省</a> (70)<br />
<a href="../Page/贝尔福地区.md" title="wikilink">贝尔福地区</a> (90)</p></td>
<td><p><a href="../Page/第戎.md" title="wikilink">第戎</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Brittany_(Gwenn_ha_du).svg" title="fig:Flag_of_Brittany_(Gwenn_ha_du).svg">Flag_of_Brittany_(Gwenn_ha_du).svg</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Bretagne_region_locator_map2.svg" title="fig:Bretagne_region_locator_map2.svg">Bretagne_region_locator_map2.svg</a></p></td>
<td><p>'''<a href="../Page/布列塔尼.md" title="wikilink">布列塔尼</a> '''</p></td>
<td><p>53</p></td>
<td><p><a href="../Page/阿摩尔滨海省.md" title="wikilink">阿摩尔滨海省</a> (22)<br />
<a href="../Page/非尼斯泰尔省.md" title="wikilink">非尼斯泰尔省</a> (29)<br />
<a href="../Page/伊勒-维莱讷省.md" title="wikilink">伊勒-维莱讷省</a> (35)<br />
<a href="../Page/莫尔比昂省.md" title="wikilink">莫尔比昂省</a> (56)</p></td>
<td><p><a href="../Page/雷恩.md" title="wikilink">雷恩</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Centre_(France).svg" title="fig:Flag_of_Centre_(France).svg">Flag_of_Centre_(France).svg</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Centre-Val_de_Loire_region_locator_map.svg" title="fig:Centre-Val_de_Loire_region_locator_map.svg">Centre-Val_de_Loire_region_locator_map.svg</a></p></td>
<td><p>'''<a href="../Page/中央-卢瓦尔山谷大区.md" title="wikilink">中央-羅亞爾河谷</a> '''</p></td>
<td><p>24</p></td>
<td><p><a href="../Page/谢尔省.md" title="wikilink">谢尔省</a> (18)<br />
<a href="../Page/厄尔-卢瓦尔省.md" title="wikilink">厄尔-卢瓦尔省</a> (28)<br />
<a href="../Page/安德尔省.md" title="wikilink">安德尔省</a> (36)<br />
<a href="../Page/安德尔-卢瓦尔省.md" title="wikilink">安德尔-卢瓦尔省</a> (37)<br />
<a href="../Page/卢瓦尔-谢尔省.md" title="wikilink">卢瓦尔-谢尔省</a> (41)<br />
<a href="../Page/卢瓦雷省.md" title="wikilink">卢瓦雷省</a> (45)</p></td>
<td><p><a href="../Page/奥尔良.md" title="wikilink">奥尔良</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Île-de-France.svg" title="fig:Flag_of_Île-de-France.svg">Flag_of_Île-de-France.svg</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Île-de-France_region_locator_map2.svg" title="fig:Île-de-France_region_locator_map2.svg">Île-de-France_region_locator_map2.svg</a></p></td>
<td><p><strong><a href="../Page/法兰西岛.md" title="wikilink">法兰西岛</a></strong></p></td>
<td><p>11</p></td>
<td><p><a href="../Page/巴黎省.md" title="wikilink">巴黎省</a> (75)<br />
<a href="../Page/塞纳-马恩省.md" title="wikilink">塞纳-马恩省</a> (77)<br />
<a href="../Page/伊夫林省.md" title="wikilink">伊夫林省</a> (78)<br />
<a href="../Page/埃松省.md" title="wikilink">埃松省</a> (91)<br />
<a href="../Page/上塞纳省.md" title="wikilink">上塞纳省</a> (92)<br />
<a href="../Page/塞纳-圣但尼省.md" title="wikilink">塞纳-圣但尼省</a> (93)<br />
<a href="../Page/瓦勒德马恩省.md" title="wikilink">瓦勒德马恩省</a> (94)<br />
<a href="../Page/瓦勒德瓦兹省.md" title="wikilink">瓦勒德瓦兹省</a> (95)</p></td>
<td><p><a href="../Page/巴黎.md" title="wikilink">巴黎</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Languedoc-Roussillon.svg" title="fig:Flag_of_Languedoc-Roussillon.svg">Flag_of_Languedoc-Roussillon.svg</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Languedoc-Roussillon-Midi-Pyrénées_region_locator_map.svg" title="fig:Languedoc-Roussillon-Midi-Pyrénées_region_locator_map.svg">Languedoc-Roussillon-Midi-Pyrénées_region_locator_map.svg</a></p></td>
<td><p><strong><a href="../Page/奧克西塔尼大區.md" title="wikilink">奧克西塔尼</a></strong></p></td>
<td><p>76</p></td>
<td><p><a href="../Page/奥德省.md" title="wikilink">奥德省</a> (11)<br />
<a href="../Page/加尔省.md" title="wikilink">加尔省</a> (30)<br />
<a href="../Page/埃罗省.md" title="wikilink">埃罗省</a> (34)<br />
<a href="../Page/洛泽尔省.md" title="wikilink">洛泽尔省</a> (48)<br />
<a href="../Page/东比利牛斯省.md" title="wikilink">东比利牛斯省</a> (66)<br />
<a href="../Page/阿列日省.md" title="wikilink">阿列日省</a> (09)<br />
<a href="../Page/阿韦龙省.md" title="wikilink">阿韦龙省</a> (12)<br />
<a href="../Page/上加龙省.md" title="wikilink">上加龙省</a> (31)<br />
<a href="../Page/热尔省.md" title="wikilink">热尔省</a> (32)<br />
<a href="../Page/洛特省.md" title="wikilink">洛特省</a> (46)<br />
<a href="../Page/上比利牛斯省.md" title="wikilink">上比利牛斯省</a> (65)<br />
<a href="../Page/塔恩省.md" title="wikilink">塔恩省</a> (81)<br />
<a href="../Page/塔恩-加龙省.md" title="wikilink">塔恩-加龙省</a> (82)</p></td>
<td><p><a href="../Page/圖盧茲.md" title="wikilink">圖盧茲</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Hauts-de-France_region_locator_map.svg" title="fig:Hauts-de-France_region_locator_map.svg">Hauts-de-France_region_locator_map.svg</a></p></td>
<td><p>'''<a href="../Page/上法蘭西大區.md" title="wikilink">上法蘭西</a> '''</p></td>
<td><p>32</p></td>
<td><p><a href="../Page/北部省.md" title="wikilink">北部省</a> (59)<br />
<a href="../Page/加莱海峡省.md" title="wikilink">加莱海峡省</a> (62)<br />
<a href="../Page/埃纳省.md" title="wikilink">埃纳省</a> (02)<br />
<a href="../Page/瓦兹省.md" title="wikilink">瓦兹省</a> (60)<br />
<a href="../Page/索姆省.md" title="wikilink">索姆省</a> (80)</p></td>
<td><p><a href="../Page/里尔.md" title="wikilink">里尔</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Normandie2014_region_locator_map.svg" title="fig:Normandie2014_region_locator_map.svg">Normandie2014_region_locator_map.svg</a></p></td>
<td><p><strong><a href="../Page/诺曼底大区.md" title="wikilink">諾曼底</a></strong></p></td>
<td><p>28</p></td>
<td><p><a href="../Page/卡尔瓦多斯省.md" title="wikilink">卡尔瓦多斯省</a> (14)<br />
<a href="../Page/芒什省.md" title="wikilink">芒什省</a> (50)<br />
<a href="../Page/奥恩省.md" title="wikilink">奥恩省</a> (61)<br />
<a href="../Page/厄尔省.md" title="wikilink">厄尔省</a> (27)<br />
<a href="../Page/滨海塞纳省.md" title="wikilink">滨海塞纳省</a> (76)</p></td>
<td><p><a href="../Page/魯昂.md" title="wikilink">魯昂</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Pays-de-la-Loire.svg" title="fig:Flag_of_Pays-de-la-Loire.svg">Flag_of_Pays-de-la-Loire.svg</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Pays_de_la_Loire_region_locator_map2.svg" title="fig:Pays_de_la_Loire_region_locator_map2.svg">Pays_de_la_Loire_region_locator_map2.svg</a></p></td>
<td><p><strong><a href="../Page/卢瓦尔河地区.md" title="wikilink">卢瓦尔河地区</a></strong></p></td>
<td><p>52</p></td>
<td><p><a href="../Page/大西洋卢瓦尔省.md" title="wikilink">大西洋卢瓦尔省</a> (44)<br />
<a href="../Page/曼恩-卢瓦尔省.md" title="wikilink">曼恩-卢瓦尔省</a> (49)<br />
<a href="../Page/马耶讷省.md" title="wikilink">马耶讷省</a> (53)<br />
<a href="../Page/萨尔特省.md" title="wikilink">萨尔特省</a> (72)<br />
<a href="../Page/旺代省.md" title="wikilink">旺代省</a> (85)</p></td>
<td><p><a href="../Page/南特.md" title="wikilink">南特</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Provence-Alpes-Cote_dAzur.svg" title="fig:Flag_of_Provence-Alpes-Cote_dAzur.svg">Flag_of_Provence-Alpes-Cote_dAzur.svg</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Provence-Alpes-Côte_d&#39;Azur_region_locator_map2.svg" title="fig:Provence-Alpes-Côte_d&#39;Azur_region_locator_map2.svg">Provence-Alpes-Côte_d'Azur_region_locator_map2.svg</a></p></td>
<td><p><strong><a href="../Page/普罗旺斯-阿尔卑斯-蓝色海岸.md" title="wikilink">普罗旺斯-阿尔卑斯-蓝色海岸</a></strong></p></td>
<td><p>93</p></td>
<td><p><a href="../Page/上普罗旺斯阿尔卑斯省.md" title="wikilink">上普罗旺斯阿尔卑斯省</a> (04)<br />
<a href="../Page/上阿尔卑斯省.md" title="wikilink">上阿尔卑斯省</a> (05)<br />
<a href="../Page/阿尔卑斯滨海省.md" title="wikilink">阿尔卑斯滨海省</a> (06)<br />
<a href="../Page/罗讷河口省.md" title="wikilink">罗讷河口省</a> (13)<br />
<a href="../Page/瓦尔省.md" title="wikilink">瓦尔省</a> (83)<br />
<a href="../Page/沃克吕兹省.md" title="wikilink">沃克吕兹省</a> (84)</p></td>
<td><p><a href="../Page/马赛.md" title="wikilink">马赛</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>科西嘉的地位为“<a href="../Page/地方行政区域.md" title="wikilink">地方行政区域</a>”，与其余各大区不同</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Corsica.svg" title="fig:Flag_of_Corsica.svg">Flag_of_Corsica.svg</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Corse_region_locator_map2.svg" title="fig:Corse_region_locator_map2.svg">Corse_region_locator_map2.svg</a></p></td>
<td><p><strong><a href="../Page/科西嘉_(大区).md" title="wikilink">科西嘉</a></strong></p></td>
<td><p>94</p></td>
<td><p><a href="../Page/南科西嘉省.md" title="wikilink">南科西嘉省</a> (2A)<br />
<a href="../Page/上科西嘉省.md" title="wikilink">上科西嘉省</a> (2B)</p></td>
<td><p><a href="../Page/阿雅克肖.md" title="wikilink">阿雅克肖</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>5个海外大区（每一个也同时是一个<a href="../Page/海外省.md" title="wikilink">海外省</a>）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Guadeloupe_(local).svg" title="fig:Flag_of_Guadeloupe_(local).svg">Flag_of_Guadeloupe_(local).svg</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Guadeloupe_in_France.svg" title="fig:Guadeloupe_in_France.svg">Guadeloupe_in_France.svg</a></p></td>
<td><p><strong><a href="../Page/瓜德罗普岛.md" title="wikilink">瓜德罗普岛</a></strong></p></td>
<td><p>01</p></td>
<td><p><a href="../Page/瓜德罗普.md" title="wikilink">瓜德罗普</a> (971)</p></td>
<td><p><a href="../Page/巴斯特尔.md" title="wikilink">巴斯特尔</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Drapeau_aux_serpents_de_la_Martinique.svg" title="fig:Drapeau_aux_serpents_de_la_Martinique.svg">Drapeau_aux_serpents_de_la_Martinique.svg</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Martinique_in_France.svg" title="fig:Martinique_in_France.svg">Martinique_in_France.svg</a></p></td>
<td><p><strong><a href="../Page/马提尼克.md" title="wikilink">马提尼克</a></strong></p></td>
<td><p>02</p></td>
<td><p><a href="../Page/马提尼克.md" title="wikilink">马提尼克</a> (972)</p></td>
<td><p><a href="../Page/法兰西堡.md" title="wikilink">法兰西堡</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_French_Guiana.svg" title="fig:Flag_of_French_Guiana.svg">Flag_of_French_Guiana.svg</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:French_Guiana_in_France.svg" title="fig:French_Guiana_in_France.svg">French_Guiana_in_France.svg</a></p></td>
<td><p><strong><a href="../Page/法属圭亚那.md" title="wikilink">法属圭亚那</a></strong></p></td>
<td><p>03</p></td>
<td><p><a href="../Page/法属圭亚那.md" title="wikilink">法属圭亚那</a> (973)</p></td>
<td><p><a href="../Page/卡宴.md" title="wikilink">卡宴</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Réunion.svg" title="fig:Flag_of_Réunion.svg">Flag_of_Réunion.svg</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Reunion_in_France.svg" title="fig:Reunion_in_France.svg">Reunion_in_France.svg</a></p></td>
<td><p><strong><a href="../Page/留尼汪.md" title="wikilink">留尼汪</a></strong></p></td>
<td><p>04</p></td>
<td><p><a href="../Page/留尼汪.md" title="wikilink">留尼汪</a> (974)</p></td>
<td><p><a href="../Page/圣但尼.md" title="wikilink">圣但尼</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Mayotte_(local).svg" title="fig:Flag_of_Mayotte_(local).svg">Flag_of_Mayotte_(local).svg</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:LocationMayotte.png" title="fig:LocationMayotte.png">LocationMayotte.png</a></p></td>
<td><p><strong><a href="../Page/马约特.md" title="wikilink">马约特</a></strong></p></td>
<td><p>05</p></td>
<td><p><a href="../Page/马约特.md" title="wikilink">马约特</a> (976)</p></td>
<td><p><a href="../Page/马穆楚.md" title="wikilink">马穆楚</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 2016年行政区划改革

2016年1月1日，法国将本土现有22个大区合并为13个。（以下各大區參右圖代號）

其调整为：

  - 整体保留

<!-- end list -->

  - [法兰西岛](../Page/法兰西岛.md "wikilink")(12)
  - [中央-羅亞爾河谷](../Page/中央-盧瓦爾河谷大區.md "wikilink")(7)
  - [卢瓦尔河地区](../Page/卢瓦尔河地区大区.md "wikilink")(18)
  - [布列塔尼](../Page/布列塔尼大區.md "wikilink")(6)
  - [普罗旺斯-阿尔卑斯-蓝色海岸](../Page/普罗旺斯-阿尔卑斯-蓝色海岸.md "wikilink")(21)
  - [科西嘉](../Page/科西嘉领土集体.md "wikilink")(9)

<!-- end list -->

  - 两区合并

<!-- end list -->

  - [北部-加来海峡](../Page/北部-加来海峡.md "wikilink")(17) +
    [皮卡第](../Page/皮卡第.md "wikilink")(19) =
    [上法蘭西](../Page/上法蘭西大區.md "wikilink")
  - [上](../Page/上诺曼底.md "wikilink")(11)、
    [下](../Page/下诺曼底.md "wikilink")[诺曼底](../Page/诺曼底.md "wikilink")(4)
    = [諾曼第](../Page/諾曼第大區.md "wikilink")
  - [勃艮第](../Page/勃艮第.md "wikilink")(5) +
    [弗朗什-孔泰](../Page/弗朗什-孔泰.md "wikilink")(10) =
    [布根地-法蘭琪-康堤](../Page/勃艮第-弗朗什-孔泰大區.md "wikilink")
  - [罗讷-阿尔卑斯](../Page/罗讷-阿尔卑斯.md "wikilink")(22) +
    [奥弗涅](../Page/奥弗涅.md "wikilink")(3) =
    [奧文尼-隆-阿爾卑斯](../Page/奧弗涅-羅納-阿爾卑斯大區.md "wikilink")
  - [南部-比利牛斯](../Page/南部-比利牛斯.md "wikilink")(16) +
    [朗格多克-鲁西永](../Page/朗格多克-鲁西永.md "wikilink")(13) =
    [奧克西塔尼](../Page/奧克西塔尼大區.md "wikilink")

<!-- end list -->

  - 三区合并

<!-- end list -->

  - [香槟-阿登](../Page/香槟-阿登.md "wikilink")(8) +
    [洛林](../Page/洛林.md "wikilink")(15) +
    [阿尔萨斯](../Page/阿尔萨斯.md "wikilink")(1) =
    [大東部大區](../Page/大東部大區.md "wikilink")
  - [普瓦图-夏朗德](../Page/普瓦图-夏朗德.md "wikilink")(20) +
    [利穆赞](../Page/利穆赞.md "wikilink")(14) +
    [阿基坦](../Page/阿基坦.md "wikilink")(2) =
    [新阿基坦大區](../Page/新阿基坦大區.md "wikilink")

## 参考文献

## 参见

  - [大区委员会 (法国)](../Page/大区委员会_\(法国\).md "wikilink")
  - [法国行政区划](../Page/法国行政区划.md "wikilink")
      - [行省 (法国)](../Page/行省_\(法国\).md "wikilink")
      - [省 (法国)](../Page/省_\(法国\).md "wikilink")

{{-}}

[Category:法国一级行政区](../Category/法国一级行政区.md "wikilink")
[法国大区](../Category/法国大区.md "wikilink")
[France](../Category/各国大区.md "wikilink")

1.  [Liste des
    régions](http://www.insee.fr/fr/methodes/nomenclatures/cog/region.asp)
    官方的地区代码.
2.  [Populations légales 2010 des
    régions](http://www.legifrance.gouv.fr/affichTexte.do;jsessionid=?cidTexte=JORFTEXT000026855244&dateTexte=&oldAction=rechJO&categorieLien=id)