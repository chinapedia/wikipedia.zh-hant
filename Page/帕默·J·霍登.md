**帕默·J·霍登**（）是一名[美国养猪技术专家](../Page/美国.md "wikilink")，主要研究领域为猪营养和管理。他从1972起所领导的《爱荷华州立大学生命周期猪营养》（Iowa
State University LIFE CYCLE SWINE
NUTRITION）具有较大的国际影响，被认为是[爱荷华州作为国际猪营养信息中心的重要因素之一](../Page/爱荷华州.md "wikilink")\[1\]。

## 履历

作为一名[北达科他州人](../Page/北达科他州.md "wikilink")，1965年，霍登从[北达科塔州立大学获得了](../Page/北达科塔州立大学.md "wikilink")[学士学位](../Page/学士学位.md "wikilink")。1967年，霍登在爱荷华州立大学取得[硕士学位并于](../Page/硕士学位.md "wikilink")1970年取得了[哲学博士学位](../Page/哲学博士.md "wikilink")。[越南战争中](../Page/越南战争.md "wikilink")，霍登在美军担任农业顾问。1972年5月1日，霍登开始在[爱荷华州立大学担任](../Page/爱荷华州立大学.md "wikilink")[动物科学及猪推广专业助教](../Page/动物科学.md "wikilink")。

2000年，霍登获得到[美国动物科学学会](../Page/美国动物科学学会.md "wikilink")（ASAS）推广奖\[2\]并被评为特别会员\[3\]。
2002年10月25日，霍登从爱荷华州立大学退休\[4\]。

## 贡献

霍登从1972起所领导的《爱荷华州立大学生命周期猪营养》（Iowa State University LIFE CYCLE SWINE
NUTRITION）被认为是爱荷华州作为国际猪营养信息中心的重要因素之一，后来还翻译成[西班牙文](../Page/西班牙文.md "wikilink")、[中文](../Page/中文.md "wikilink")、[俄文及](../Page/俄文.md "wikilink")[日文四种语言](../Page/日文.md "wikilink")。这本手册的[公制版本成了爱荷华州养猪业中心网站下载最多的文件之一](../Page/公制.md "wikilink")\[5\]。

1983年，霍登在启动了爱荷华州养猪会议的“咨询专家（）”讲台，带动了超过50名教师和推广人员志愿来帮助养殖人员。

1998年，霍登主持的一场国际[卫星会议联络了美国](../Page/卫星会议.md "wikilink")、[加拿大及](../Page/加拿大.md "wikilink")[巴西的](../Page/巴西.md "wikilink")26个地点，并发布[NRC猪营养需求](../Page/NRC.md "wikilink")。

2003年，霍登和[M. E.
恩斯明格合作出版了](../Page/M._E._恩斯明格.md "wikilink")《养猪学》（第7版）（Swine
Science, Seventh Edition）。

霍登在13个国家进行个演讲或担任顾问工作。他还曾经为[大英百科全书撰写了两个条目](../Page/大英百科全书.md "wikilink")\[6\]。霍登同时也指导出版一个年度报告《爱荷华州立大学养猪研究报告》。

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  - [爱荷华州立大学生命周期猪营养（Iowa State University LIFE CYCLE SWINE
    NUTRITION）](https://web.archive.org/web/20070822033955/http://www.ipic.iastate.edu/LCSN/LCSNutrition.pdf)

[Category:北达科他州人](../Category/北达科他州人.md "wikilink")
[Category:畜牧学家](../Category/畜牧学家.md "wikilink")
[Category:北达科他州立大学校友](../Category/北达科他州立大学校友.md "wikilink")
[Category:爱荷华州立大学校友](../Category/爱荷华州立大学校友.md "wikilink")

1.
2.
3.
4.
5.
6.