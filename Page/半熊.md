**半熊**（*Hemicyon*）是已[滅絕像](../Page/滅絕.md "wikilink")[熊的](../Page/熊.md "wikilink")[動物](../Page/動物.md "wikilink")，衍生出[半狗科分支](../Page/半狗科.md "wikilink")。牠們長約1.5米，高約70厘米，身體比例像[虎](../Page/虎.md "wikilink")，而[牙齒像](../Page/牙齒.md "wikilink")[狗](../Page/狗.md "wikilink")。牠們生存於2200萬年前[中新世的](../Page/中新世.md "wikilink")[歐洲](../Page/歐洲.md "wikilink")、[亞洲及](../Page/亞洲.md "wikilink")[北美洲](../Page/北美洲.md "wikilink")。一般認為半熊是高級[肉食性動物及](../Page/肉食性.md "wikilink")[掠食的](../Page/掠食.md "wikilink")。半熊不像現今的熊是[蹠行的](../Page/蹠行.md "wikilink")，而是趾行及有長的[中骨](../Page/中骨.md "wikilink")。這顯示半熊是活躍的掠食者及奔跑能手，估計是在平原群體捕獵的。牠們因此與熊有所區別，而更像狗。

半熊的[物種是於](../Page/物種.md "wikilink")1800萬年前的[亥明佛德階出現](../Page/亥明佛德階.md "wikilink")，故牠們是最早在北美洲出現的。這些北美洲物種與[波爾迪階歐洲的物種是近親](../Page/波爾迪階.md "wikilink")。\[1\]其他半熊[化石包括](../Page/化石.md "wikilink")：

  - 在[中國](../Page/中國.md "wikilink")[甘肅](../Page/甘肅.md "wikilink")[臨夏盆地中新世中期地層發現的](../Page/臨夏盆地.md "wikilink")*H.
    teilhardi*；
  - 在中國發現中新世早期的半熊標本；
  - [青藏高原東北邊界臨夏盆地發現的中新世中期半熊標本](../Page/青藏高原.md "wikilink")；
  - [西班牙](../Page/西班牙.md "wikilink")[薩拉戈薩省發現的中新世中期](../Page/薩拉戈薩省.md "wikilink")*H.
    sansaniensis*；
  - 西班牙[塔拉索納發現中新世中期的](../Page/塔拉索納.md "wikilink")*H. mayorali*；
  - [馬德里發現的半熊標本](../Page/馬德里.md "wikilink")；
  - [土耳其](../Page/土耳其.md "wikilink")[安卡拉發現中新世中期的](../Page/安卡拉.md "wikilink")*H.
    sansaniensis*；
  - [波斯尼亞發現下中新世的](../Page/波斯尼亞.md "wikilink")*H stehlini*；
  - [特拉華州發現亥明佛德階早期的半熊標本](../Page/特拉華州.md "wikilink")；及
  - [新墨西哥州中新世](../Page/新墨西哥州.md "wikilink")[聖塔非地層發現完整的半熊標本](../Page/聖塔非地層.md "wikilink")。

## 參考

[Category:半狗科](../Category/半狗科.md "wikilink")

1.