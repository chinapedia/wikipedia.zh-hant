**佐伯市**（）是位于[日本](../Page/日本.md "wikilink")[大分縣東南部的一個](../Page/大分縣.md "wikilink")[城市](../Page/城市.md "wikilink")，為[九州裡面積最大的市町村級行政區劃](../Page/九州_\(日本\).md "wikilink")。\[1\]

東部面向[豐後水道](../Page/豐後水道.md "wikilink")，其海岸由於為[谷灣地形](../Page/谷灣.md "wikilink")，已被列為[日豐海岸國定公園的範圍](../Page/日豐海岸國定公園.md "wikilink")。西部和南部的內陸地區則屬於[祖母傾國定公園](../Page/祖母傾國定公園.md "wikilink")。

## 歷史

過去曾為[佐伯藩的](../Page/佐伯藩.md "wikilink")[城下町](../Page/城下町.md "wikilink")。

舊佐伯市與周邊的[南海部郡地區由於屬於相同的生活圈](../Page/南海部郡.md "wikilink")，過去被合稱為「佐伯南郡」；因此在[平成大合併開始推行第二年的](../Page/平成大合併.md "wikilink")2000年12月27日佐伯市與南海部郡轄下的5町3村變合組了「任意合併協議會」，並於2002年5月1日進一步設置「法定合併協議會」；最終在2003年8月31日完成合併簽約，2005年3月3日完成合併。

### 年表

  - 1889年4月1日：實施[町村制](../Page/町村制.md "wikilink")，現在的轄區在當時分屬以下地區。
      - [南海部郡](../Page/南海部郡.md "wikilink")：
          - **佐伯町**：由佐伯村改制而成。
          - 上堅田村：由池田村、長谷村合併而成。
          - 下堅田村：由堅田村、長良村合併而成。
          - 鶴岡村：由**鶴**望村、上**岡**村、稻垣村合併而成。
          - 八幡村：由戶穴村、霞浦村、海崎村合併而成。
          - 大入島村：由石間浦、守後浦、荒網代浦、鹽內浦、久保浦、片神浦、高松浦、日向泊浦合併而成。
          - 西上浦村：由狩生村、二榮浦、護江浦合併而成。
          - 東上浦村：由最勝海浦、津井浦、淺海井浦合併而成。
          - 青山村：由青山村改制而成。
          - 木立村：由木立村改制而成。
          - 切畑村：由細田村、平井村、門田村、江良村、提內村合併而成。
          - 上野村：由山梨子村、井崎村、小田村、上小倉村合併而成。
          - 明治村：由庄木村、大坂本村、尺間村合併而成。
          - 中野村：由笠掛村、風戶村、三股村、宇津津村、波寄村、小川村、小半村合併而成。
          - 因尾村：由因尾村、上津川村、山部村、堂野間村、井之上村合併而成。
          - 川原木村：由横**川**村、仁田**原**村、赤**木**村合併而成。
          - 直見村：由上直見村,、下直見村合併而成。
          - 東中浦村：由羽出浦、中越浦、丹賀浦、梶寄浦、大島合併而成。
          - 西中浦村：由吹浦、地松浦、沖松浦、有明浦合併而成。
          - 米水津村：由浦代浦、竹野浦、小浦、色利浦、宮野浦合併而成。
          - 蒲江村：由蒲江浦、猪串浦合併而成。
          - 名護屋村：由野野河內浦、森崎浦、丸市尾浦、葛原浦、波當津浦合併而成。
          - 上入津村：由畑野浦、楠本浦合併而成。
          - 下入津村：由竹野浦、河內、西野浦合併而成。
      - [大野郡](../Page/大野郡_\(大分縣\).md "wikilink")：
          - 小野市村：由小野市村、南田原村、木浦內村、木浦礦山合併而成。
          - 重岡村：由重岡村、鹽見園村、大平村、河內村、千束村合併而成。
  - 1911年1月1日：蒲江村改制為[蒲江町](../Page/蒲江町.md "wikilink")。
  - 1922年9月1日：[中浦村從東中浦村分村](../Page/中浦村.md "wikilink")。
  - 1937年4月1日：佐伯町、[鶴岡村](../Page/鶴岡村.md "wikilink")、[上堅田村](../Page/上堅田村.md "wikilink")[合併為新設置的](../Page/市町村合併.md "wikilink")**[佐伯町](../Page/佐伯町_\(大分縣\).md "wikilink")**。
  - 1941年4月29日：佐伯町、[八幡村](../Page/八幡村.md "wikilink")、[大入島村](../Page/大入島村.md "wikilink")、[西上浦村合併為](../Page/西上浦村.md "wikilink")**佐伯市**。
  - 1950年1月1日：大野郡小野市村、重岡村改隸屬南海部郡。
  - 1950年12月19日：東上浦村改名為上浦村。
  - 1951年1月1日：上浦村改制為[上浦町](../Page/上浦町_\(大分縣\).md "wikilink")。
  - 1951年4月1日：[川原木村和](../Page/川原木村.md "wikilink")[直見村合併為](../Page/直見村.md "wikilink")[直川村](../Page/直川村.md "wikilink")。
  - 1955年3月31日：
      - [下堅田村](../Page/下堅田村.md "wikilink")、[木立村](../Page/木立村.md "wikilink")、[青山村被併入佐伯市](../Page/青山村.md "wikilink")。
      - [小野市村](../Page/小野市村.md "wikilink")、[重岡村合併為宇目村](../Page/重岡村.md "wikilink")。
      - [中浦村](../Page/中浦村.md "wikilink")、[東中浦村](../Page/東中浦村.md "wikilink")、[西中浦村合併為鶴見村](../Page/西中浦村.md "wikilink")。
      - 蒲江町、[名護屋村](../Page/名護屋村.md "wikilink")、[上入津村](../Page/上入津村.md "wikilink")、[下入津村合併為蒲江町](../Page/下入津村.md "wikilink")。
  - 1955年6月1日：[中野村](../Page/中野村_\(大分縣\).md "wikilink")、[因尾村合併為](../Page/因尾村.md "wikilink")[本匠村](../Page/本匠村.md "wikilink")'''。
  - 1956年2月1日：[切畑村](../Page/切畑村.md "wikilink")、[上野村](../Page/上野村_\(大分縣\).md "wikilink")、[明治村合併為昭和村](../Page/明治村_\(大分縣南海部郡\).md "wikilink")。
  - 1957年4月1日：昭和村改名為彌生村。
  - 1961年2月11日：鶴見村改制為[鶴見町](../Page/鶴見町.md "wikilink")。
  - 1961年11月3日：宇目村改制為[宇目町](../Page/宇目町.md "wikilink")。
  - 1966年2月1日：彌生村改制為[彌生町](../Page/彌生町.md "wikilink")。
  - 2005年3月3日：佐伯市、上浦町、彌生町、本匠村、宇目町、直川村、鶴見町、[米水津村](../Page/米水津村.md "wikilink")、蒲江町合併為新設置的**佐伯市**。

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1889年以前</p></th>
<th><p>1889年4月1日</p></th>
<th><p>1889年 - 1944年</p></th>
<th><p>1945年 - 1954年</p></th>
<th><p>1955年 - 1964年</p></th>
<th><p>1965年 - 1988年</p></th>
<th><p>1988年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>佐伯町</p></td>
<td><p>1937年4月1日<br />
佐伯町</p></td>
<td><p>1941年4月29日<br />
佐伯市</p></td>
<td><p>佐伯市</p></td>
<td><p>佐伯市</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>鶴岡村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>上堅田村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>西上浦村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>八幡村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>大入島村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>下堅田村</p></td>
<td><p>1955年3月31日<br />
併入佐伯市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>青山村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>木立村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>東上浦村</p></td>
<td><p>1950年12月19日<br />
改名為上浦村</p></td>
<td><p>1951年1月1日<br />
上浦町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>明治村</p></td>
<td><p>1956年2月1日<br />
昭和村</p></td>
<td><p>1957年4月1日<br />
改名為彌生村</p></td>
<td><p>1966年2月1日<br />
彌生町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>上野村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>切畑村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>中野村</p></td>
<td><p>1955年6月1日<br />
本匠村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>因尾村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>直見村</p></td>
<td><p>1951年4月1日<br />
直川村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>川原木村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>東中浦村</p></td>
<td><p>東中浦村</p></td>
<td><p>1955年3月31日<br />
鶴見村</p></td>
<td><p>1961年2月11日<br />
鶴見町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1922年9月1日<br />
中浦村分村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>西中浦村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>米水津村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>蒲江村</p></td>
<td><p>1911年1月1日<br />
蒲江町</p></td>
<td><p>1955年3月31日<br />
蒲江町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>上入津村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>下入津村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>名護屋村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>大野郡<br />
重岡村</p></td>
<td><p>1950年1月1日<br />
南海部郡重岡村</p></td>
<td><p>1955年3月31日<br />
宇目村</p></td>
<td><p>1961年11月3日<br />
宇目町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>大野郡<br />
小野市村</p></td>
<td><p>1950年1月1日<br />
南海部郡小野市村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 交通

### 機場

  - [佐伯直升機起降場](../Page/佐伯直升機起降場.md "wikilink")

### 鐵路

  - [九州旅客鐵道](../Page/九州旅客鐵道.md "wikilink")
      - [日豐本線](../Page/日豐本線.md "wikilink")：[淺海井車站](../Page/淺海井車站.md "wikilink")
        - [狩生車站](../Page/狩生車站.md "wikilink") -
        [海崎車站](../Page/海崎車站.md "wikilink") -
        [佐伯車站](../Page/佐伯車站.md "wikilink") -
        [上岡車站](../Page/上岡車站.md "wikilink") -
        [直見車站](../Page/直見車站.md "wikilink") -
        [直川車站](../Page/直川車站.md "wikilink") -
        [重岡車站](../Page/重岡車站.md "wikilink") -
        [宗太郎車站](../Page/宗太郎車站.md "wikilink")

### 道路

  - 高速道路

<!-- end list -->

  - [東九州自動車道](../Page/東九州自動車道.md "wikilink")：[彌生休息站](../Page/彌生休息站.md "wikilink")
    - [佐伯交流道](../Page/佐伯交流道.md "wikilink") -
    [蒲江交流道](../Page/蒲江交流道.md "wikilink") -
    [波當津交流道](../Page/波當津交流道.md "wikilink")

## 觀光資源

### 祭典活動

  - 佐伯春祭
  - 海洋慶典in佐伯
  - 佐伯番匠之火祭
  - 豐後舟盛祭
  - 佐伯市鄉土祭
  - 直川獨角仙祭
  - 直川鄉土盆踊大會、花火大會

### 名勝

  - [城山](../Page/城山_\(大分縣\).md "wikilink")、佐伯城遺跡
  - 歴史和文学之道：[日本之道100選之ㄧ](../Page/日本之道100選.md "wikilink")
  - 茶室「汲心亭」
  - 城下町佐伯國木田獨歩館
  - [豐後二見浦](../Page/豐後二見浦.md "wikilink")
  - 蒲戶崎自然公園
  - [小半鍾乳洞](../Page/小半鍾乳洞.md "wikilink")
  - 小半森林公園
  - 藤河內溪谷
  - 豐後水道海事資料館
  - 候鳥館

## 教育

### 高等學校

  - [大分縣立佐伯鶴城高等學校](../Page/大分縣立佐伯鶴城高等學校.md "wikilink")
  - [大分學立佐伯豊南高等學校](../Page/大分學立佐伯豊南高等學校.md "wikilink")
  - [大分學立佐伯鶴岡高等學校](../Page/大分學立佐伯鶴岡高等學校.md "wikilink")
  - [日本文理大學附屬高等學校](../Page/日本文理大學附屬高等學校.md "wikilink")

### 特殊學校

  - 大分縣立佐伯養護學校

## 姊妹、友好都市

### 海外

  - [邯鄲市](../Page/邯鄲市.md "wikilink")
    （[中華人民共和國](../Page/中華人民共和國.md "wikilink")[河北省](../Page/河北省.md "wikilink")）：於1994年4月13日締結為友好都市。\[2\]

  - [格拉德斯通](../Page/格拉德斯通_\(昆士蘭州\).md "wikilink")（[澳大利亚](../Page/澳大利亚.md "wikilink")[昆士蘭州](../Page/昆士蘭州.md "wikilink")）：於1996年9月4日締結為姊妹都市。\[3\]

## 本地出身之名人

  - [阿南準郎](../Page/阿南準郎.md "wikilink")：[廣島東洋鯉魚前監督](../Page/廣島東洋鯉魚.md "wikilink")、現任球團部長
  - あべこ：[藝人](../Page/藝人.md "wikilink")
  - [川崎憲次郎](../Page/川崎憲次郎.md "wikilink")：[職業棒球解説員](../Page/職業棒球.md "wikilink")、前職業棒球選手
  - 大地洋輔：[藝人](../Page/藝人.md "wikilink")
  - 大谷ノブ彥：藝人
  - [竹內力](../Page/竹內力.md "wikilink")：[俳優](../Page/俳優.md "wikilink")
  - [富永一朗](../Page/富永一朗.md "wikilink")：[漫畫家](../Page/漫畫家.md "wikilink")，生於[京都市](../Page/京都市.md "wikilink")，於佐伯市接受教育。
  - [西田和昭](../Page/西田和昭.md "wikilink")：藝人、電影評論家、俳優
  - [野村謙二郎](../Page/野村謙二郎.md "wikilink")：前職業棒球選手
  - [御手洗毅](../Page/御手洗毅.md "wikilink")：[佳能第一任社長](../Page/佳能.md "wikilink")
  - [御手洗冨士夫](../Page/御手洗冨士夫.md "wikilink")：佳能現任執行長、[日本經濟團體聯合會現任會長](../Page/日本經濟團體聯合會.md "wikilink")
  - [村上勇](../Page/村上勇.md "wikilink")：政治家
  - [井脇ノブ子](../Page/井脇ノブ子.md "wikilink")：政治家
  - [高槻真裕](../Page/高槻真裕.md "wikilink")：[作曲家](../Page/作曲家.md "wikilink")、[編曲家](../Page/編曲家.md "wikilink")、音樂[製作人](../Page/製作人.md "wikilink")
  - [嘉風雅繼](../Page/嘉風雅繼.md "wikilink")：[大相撲](../Page/大相撲.md "wikilink")[力士](../Page/力士.md "wikilink")
  - [矢野龍溪](../Page/矢野龍溪.md "wikilink")：[小説家](../Page/小説家.md "wikilink")
  - [星野亮](../Page/星野亮.md "wikilink")：小説家
  - [成迫健兒](../Page/成迫健兒.md "wikilink")：[田徑選手](../Page/田徑.md "wikilink")
  - [河井誠](../Page/河井誠.md "wikilink")：[俳優](../Page/俳優.md "wikilink")

## 參考資料

## 外部連結

  - [佐伯商工會議所](https://web.archive.org/web/20081029223331/http://saikicci.rgr.jp/joomla!test/)

  - [佐伯鐵工業協同組合](http://www.cts-net.ne.jp/~saikitkk/)

  - [佐伯番匠商工會](http://yayoi.oita-shokokai.or.jp/)

<!-- end list -->

1.

2.
3.