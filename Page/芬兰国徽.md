**芬兰国徽**（）于1560年在[古斯塔夫一世的葬礼时被采用](../Page/古斯塔夫一世.md "wikilink")，同时也是[芬兰共和国国徽和](../Page/芬兰共和国.md "wikilink")[芬兰大公国国徽](../Page/芬兰大公国.md "wikilink")。

在芬兰国徽法案中对国徽的描述为：“红色，在九朵银色玫瑰中，一只咆哮着的戴着皇冠的狮子，右前蹄为被覆盔甲的人手，挥舞着长剑，脚踩着翻转的弯刀。”\[1\]

据推测，国徽上的狮子来源于[福尔孔家族](../Page/福尔孔王朝.md "wikilink")，它同样也出现在[瑞典国徽上](../Page/瑞典国徽.md "wikilink")。剑和弯刀与[卡累利阿国徽相类似](../Page/卡累利阿国徽.md "wikilink")。踩在狮子脚下的[俄罗斯弯刀则反映了当时的政治形势](../Page/俄罗斯.md "wikilink")。彼时，[瑞典与](../Page/瑞典.md "wikilink")[俄罗斯帝国正处于长期战争中](../Page/俄罗斯帝国.md "wikilink")。九朵玫瑰据推测是代表了芬兰历史上的九个省，但玫瑰的数量几经变化。

芬兰国徽在[芬兰国家机关旗帜上也有体现](../Page/芬兰国旗.md "wikilink")。

## 注释

[Category:国徽](../Category/国徽.md "wikilink")
[徽](../Category/芬蘭國家象徵.md "wikilink")

1.