[Custard_French_Toast_from_the_Modern_Diner,_Pawtucket.jpg](https://zh.wikipedia.org/wiki/File:Custard_French_Toast_from_the_Modern_Diner,_Pawtucket.jpg "fig:Custard_French_Toast_from_the_Modern_Diner,_Pawtucket.jpg")
[Toast-3.jpg](https://zh.wikipedia.org/wiki/File:Toast-3.jpg "fig:Toast-3.jpg")
[TW_BaGuio_Cafe_toast_breakfast.jpg](https://zh.wikipedia.org/wiki/File:TW_BaGuio_Cafe_toast_breakfast.jpg "fig:TW_BaGuio_Cafe_toast_breakfast.jpg")
[Tosti-ijzer.jpg](https://zh.wikipedia.org/wiki/File:Tosti-ijzer.jpg "fig:Tosti-ijzer.jpg")
[Eggs-as-food.jpg](https://zh.wikipedia.org/wiki/File:Eggs-as-food.jpg "fig:Eggs-as-food.jpg")吐司\]\]

**多士**（），也称烤面包片，為西方[麵包](../Page/麵包.md "wikilink")，在歐式早餐中常見。主要成份為[高筋麵粉](../Page/高筋麵粉.md "wikilink")、[酵母粉](../Page/酵母粉.md "wikilink")、[糖](../Page/糖.md "wikilink")、[食鹽](../Page/食鹽.md "wikilink")、[蛋白](../Page/蛋白.md "wikilink")（用於烤出吐司上方金黃色的外皮）。經由攝氏約190度的[焗爐](../Page/焗爐.md "wikilink")，焗約30分鐘，出爐後冷卻切片。面包经过加热后，表面逐渐变成褐色。这种变化过程称为[美拉德反应](../Page/美拉德反应.md "wikilink")。溫度高時較為鬆軟好吃，低溫的狀態下會變硬，風味口感會差很多。因此市售的常温软吐司，必须加入[食品添加劑以维持其口感](../Page/食品添加劑.md "wikilink")。

## 種類

### 依烘烤前混入的製作材料命名

  - [牛奶吐司](../Page/牛奶.md "wikilink")
  - [朱古力吐司](../Page/朱古力.md "wikilink")
  - [葡萄吐司](../Page/葡萄.md "wikilink")（加入[葡萄乾](../Page/葡萄乾.md "wikilink")）
  - [芋頭吐司](../Page/芋頭.md "wikilink")（加入芋蓉）
  - [紅豆吐司](../Page/紅豆.md "wikilink")
  - [椰子吐司](../Page/椰子.md "wikilink")

### 依塗抹的佐料命名

  - [草莓吐司](../Page/草莓.md "wikilink")
  - [花生吐司](../Page/花生.md "wikilink")

### 依加工命名

  - [三明治](../Page/三明治.md "wikilink")
  - [俱乐部三明治](../Page/俱乐部三明治.md "wikilink")
  - [法國吐司](../Page/西多士.md "wikilink")

### [港式](../Page/香港.md "wikilink")[茶餐廳吐司](../Page/茶餐廳.md "wikilink")

将切片的面包放在-{zh-hans:烤面包机（香港称作多士炉）;zh-hk:多士爐;
zh-tw:烤麵包機（香港稱作多士爐）;}-烤至芳香取出，在吐司的一邊抹上[煉奶](../Page/煉奶.md "wikilink")、[黄油](../Page/黄油.md "wikilink")、[果醬](../Page/果醬.md "wikilink")、[美乃滋等配料](../Page/美乃滋.md "wikilink")，用兩塊吐司夾起來熱食。多士因為有多種變化，所以有各種名稱。香港稱吐-{}-司為多-{}-士。

  - 奶油多，包含[煉奶和黄油](../Page/煉奶.md "wikilink")
  - 奶醬多，包含[煉奶和](../Page/煉奶.md "wikilink")[花生醬](../Page/花生醬.md "wikilink")
  - 油占多，包含黄油和[果醬](../Page/果醬.md "wikilink")
  - [西-{多士}-](../Page/法國吐司.md "wikilink")：法兰西式多-{}-士的简称

### [臺式吐司](../Page/臺灣.md "wikilink")

[臺語的吐司發音為](../Page/臺語.md "wikilink")“sio̍k-pháng”（[台羅](../Page/台羅.md "wikilink")，[國際音標](../Page/國際音標.md "wikilink")：//\[1\]，發音似華語**修胖**），不是便宜的麵包，而是日語發音的“”（*shokupan*）傳入[臺灣演變而成的辭彙](../Page/臺灣.md "wikilink")。[日語中所说的](../Page/日語.md "wikilink")“”（*pan*）源自於[葡萄牙語的](../Page/葡萄牙語.md "wikilink")“”。大约是在17世纪，到[日本的](../Page/日本.md "wikilink")[葡萄牙人把面包及其名称带入日本](../Page/葡萄牙人.md "wikilink")。臺灣自[日治時代](../Page/台灣日治時期.md "wikilink")，傳入麵包，但在1930年之前一般人少有吃過[麵包](../Page/麵包.md "wikilink")，而名稱則是經由日語的“”（*pan*）再流傳成為[臺灣話](../Page/臺灣話.md "wikilink")“pháng”並沿用至今。

興起於1980年代的[臺式](../Page/臺灣.md "wikilink")[泡沫紅茶店](../Page/泡沫紅茶.md "wikilink")，**早餐店**提供的常見麵包有：椰香吐司、藍莓吐司、草莓吐司、花生吐司、巧克力吐司、培根蛋吐司、火腿蛋吐司、豬排三明治、香雞排三文治等，搭配種類繁多。而在香港的臺灣餐廳，通常把多-{}-士稱為吐-{}-司。另外，有時因為個人喜好，有人會把吐司拿來炸，如[臺灣](../Page/臺灣.md "wikilink")[小吃](../Page/小吃.md "wikilink")[棺材板](../Page/棺材板.md "wikilink")。

## 相關條目

  - [厚多士事件](../Page/厚多士事件.md "wikilink")

## 外部連結

## 註腳

<references/>

[Category:麵包](../Category/麵包.md "wikilink")
[Category:西方甜食](../Category/西方甜食.md "wikilink")
[Category:英語借詞](../Category/英語借詞.md "wikilink")

1.