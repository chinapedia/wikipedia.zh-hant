**钱海燕**，[中国现代女](../Page/中国.md "wikilink")[漫画家](../Page/漫画.md "wikilink")，她的[妮名包括](../Page/妮名.md "wikilink")“燕子”和“钱小邪”等。白羊座，钱海燕1997年毕业于[山东大学中文系](../Page/山东大学.md "wikilink")，现任《[济南时报](../Page/济南时报.md "wikilink")》[编辑](../Page/编辑.md "wikilink")。\[1\]

## 生平

生于[山东省](../Page/山东.md "wikilink")[济南市](../Page/济南.md "wikilink")。1998年开始漫画创作，她的著名漫画作品包括《胭脂与砒霜》、《红袖倚袈裟》、《岂有此女》、《红袖添乱》、《[小女贼的细软](../Page/小女贼的细软.md "wikilink")》等。其作品被《[人民日报](../Page/人民日报.md "wikilink")》、《洛杉矶侨报》、《[羊城晚报](../Page/羊城晚报.md "wikilink")》、《[京华时报](../Page/京华时报.md "wikilink")》、《深圳特区报》、《杭州日报》、《[读者](../Page/读者.md "wikilink")》、《[女友](../Page/女友_\(漫画\).md "wikilink")》、《朋友》（澳洲版）等近百家[报刊大量转载](../Page/报刊.md "wikilink")、介绍或以[专栏形式发表](../Page/专栏.md "wikilink")，受到各年龄层读者的喜爱。作品机智幽默，妩媚俏皮，具有典雅迷人的[女性色彩和深沉的](../Page/女性色彩.md "wikilink")[人文主义精神](../Page/人文主义.md "wikilink")。她的[绘画和](../Page/绘画.md "wikilink")[写作在中国和世界各地多次获奖](../Page/写作.md "wikilink")。

## 赞赏与批评

作为畅销漫画集的作者，钱海燕获得了许多漫画家和读者的广泛推崇。但她的作品也遭到了部分读者的斥责与不满。有人认为她在漫画中表现了极强的[女权主义风格](../Page/女权主义.md "wikilink")，也有人认为她的作品过于新式，超越了传统的漫画创作思维。当然，也有人赞扬她的新式创作思维，认为这正好表现了当代中国女性的变化，在[艺术方面既不拘泥与](../Page/艺术.md "wikilink")[中国传统文化](../Page/中国传统文化.md "wikilink")，也不盲目地追求[西方的艺术理念](../Page/西方.md "wikilink")。

## 著作

  - 《[小女贼的细软](../Page/小女贼的细软.md "wikilink")》\[2\]
  - 《小女贼在惦记》
  - 《小女贼之偷香》
  - 《小女贼私房画》
  - 《小女贼的黑店》
  - 《[小女贼的猫腻](../Page/小女贼的猫腻.md "wikilink")》\[3\]
  - 《历史的脸谱》
  - 《那个超过100分的男人》
  - 《[单身女子的138条军规](../Page/单身女子的138条军规.md "wikilink")》\[4\]
  - 《[有关第三者的111个建议](../Page/有关第三者的111个建议.md "wikilink")》\[5\]
  - 《[这一生我愿为你做的123件事](../Page/这一生我愿为你做的123件事.md "wikilink")》\[6\]

## 参考资料

[\*](../Page/category:钱海燕.md "wikilink")
[category:中国漫画家](../Page/category:中国漫画家.md "wikilink")
[category:济南人](../Page/category:济南人.md "wikilink")
[category:山东大学校友](../Page/category:山东大学校友.md "wikilink")
[category:钱姓](../Page/category:钱姓.md "wikilink")

1.
2.
3.
4.
5.
6.