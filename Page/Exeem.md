[Exeem020b.png](https://zh.wikipedia.org/wiki/File:Exeem020b.png "fig:Exeem020b.png")

**eXeem**是個全新的點對點（[P2P](../Page/P2P.md "wikilink")）檔案分享[客戶端](../Page/客戶端.md "wikilink")，大致上（但不完全是）是以
[BitTorrent通訊協定為基礎](../Page/BitTorrent協議.md "wikilink")。eXeem是設計用來代替需要中央追蹤伺服器（tracker）的需求。tracker是BitTorrent網路中參與中繼資料傳輸的伺服器。eXeem在許多著名的tracker因被[美国电影协会於](../Page/美国电影协会.md "wikilink")2004年12月的法律行動而被迫關閉的情況下，對檔案分享社群更顯得重要。

## 概要

*eXeem*是位於[聖克里斯多福及尼維斯](../Page/聖克里斯多福及尼維斯.md "wikilink")（[加勒比海国家](../Page/加勒比海国家.md "wikilink")）的[Swarm
Systems
Inc.公司所創](../Page/Swarm_Systems_Inc..md "wikilink")。該公司僱用了創立Suprnova.org的[Sloncek為eXeem的發言人和代表](../Page/Sloncek.md "wikilink")。五千位
Suprnova.org使用者被邀請參加eXeem的測試，而後2005年1月21日eXeem放出了「public beta」（公共測試版）。

大家公認eXeem應有的功能（但不一定有）：

  - 即時[加密與解密](../Page/加密.md "wikilink")
  - 透過檔案hash（雜湊）進行搜尋
  - QoS功能
  - 應有的通用隨插即用支援

確認有的功能：

  - 使用者註解及評價，但只能在下載完檔案後用（避免假的評價）
  - 最低上傳率限度（5 KB/s）以防止只下不上的人

## eXeem 之相關批評

eXeem在首次出現後，就開始出現許多批評，如：

  - eXeem的營利運作模式，包括廣告（與[Kazaa相似](../Page/Kazaa.md "wikilink")）以及含有廣告軟體Cydoor。於是便有人推出不含廣告軟體的版本，如*[eXeem
    Lite](../Page/eXeem_Lite.md "wikilink")*和*[BIT
    eXeem](../Page/BIT_eXeem.md "wikilink")*，造成eXeem從0.21版開始不包含Cydoor。

<!-- end list -->

  - 封閉原始碼研發，與eXeem想要取代的[BitTorrent的開放原始碼規格成對比](../Page/BitTorrent.md "wikilink")。

<!-- end list -->

  - 沒有非Windows版本，也無法讓第三者將程式碼移植到Windows以外的平台（因為eXeem不公開原始碼）。

## 参見

  - [p2p](../Page/p2p.md "wikilink")
  - [BitTorrent协议](../Page/BitTorrent协议.md "wikilink")
  - [BitTorrent软件](../Page/BitTorrent软件.md "wikilink")
  - [版权](../Page/版权.md "wikilink")

## 外部連結

[Category:P2P](../Category/P2P.md "wikilink")