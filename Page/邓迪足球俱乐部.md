**登地足球會**（）是位於[蘇格蘭第四大](../Page/蘇格蘭.md "wikilink")[城市](../Page/城市.md "wikilink")[邓迪的](../Page/邓迪.md "wikilink")[職業足球俱乐部](../Page/職業.md "wikilink")，成立於1893年，暱稱「*The
Dee*」或「*The Dark Blues*」，傳統穿著深藍色球衣，主場在（Dens
Park）。現時在[蘇格蘭超級足球聯賽作賽](../Page/蘇格蘭超級足球聯賽.md "wikilink")。

以[邓迪這樣規模的城市居然可以擁有兩支頂級的職業足球會](../Page/邓迪.md "wikilink")，邓迪及[邓迪联](../Page/邓迪联足球俱乐部.md "wikilink")，兩隊的球場相距僅數百碼，是[英倫三島內距離最接近的職業足球會主場球場](../Page/英倫三島.md "wikilink")。雖然登地联已降級而減少兩隊對賽的機會，但登地及登地聯仍然是傳統激烈的敵對球隊。

## 球會歷史

登地是於1893年由當地兩支球隊「**」及「**」合併而成，並於同年8月12日在聯賽中首戰[-{zh-hans:流浪者;
zh-hk:格拉斯哥流浪;}-](../Page/流浪者足球俱乐部.md "wikilink")，雙方踢成3-3平手。1899年登地搬遷到現今仍使用的（Dens
Park）。

在球隊成立的初年並沒有突出的成績，直到1910年球隊在第二次重賽才以2-1擊敗[克萊德首次奪得](../Page/克莱德足球俱乐部.md "wikilink")[蘇格蘭足總盃](../Page/蘇格蘭足總盃.md "wikilink")。在[戰後的首個十年](../Page/第二次世界大戰.md "wikilink")，登地在1949年差一點就贏得聯賽錦標、獲得兩次[聯賽盃](../Page/蘇格蘭聯賽盃.md "wikilink")（1952年及1953年）、晉級1952年[足總盃決賽](../Page/蘇格蘭足總盃.md "wikilink")（0-4負於[馬瑟韋爾](../Page/马瑟韦尔足球俱乐部.md "wikilink")）及付出破蘇格蘭轉會費紀錄的15,000[英鎊收購比利](../Page/英鎊.md "wikilink")·史梯爾（Billy
Steel）。這時登地主場觀眾平均超過20,000人。

1959年登地在足總盃以0-1負於高地聯賽（Highland
League）的[弗雷泽堡](../Page/Fraserburgh_F.C..md "wikilink")（Fraserburgh），被廣泛認為是登地歷史中最尷尬的敗仗。

在[-{zh-hans:比尔·香克利;
zh-hk:比爾·辛奇利;}-的兄弟](../Page/比尔·香克利.md "wikilink")[-{zh-hans:鲍勃·香克利;
zh-hk:卜·辛奇利;}-](../Page/鲍勃·香克利.md "wikilink")（Bob
Shankly）帶領一班出色的球員如[-{zh-hans:阿兰·吉尔泽安;
zh-hk:阿倫·基路臣;}-](../Page/阿兰·吉尔泽安.md "wikilink")（Alan
Gilzean）、[-{zh-hans:戈登·史密斯;
zh-hk:哥頓·史勿夫;}-](../Page/Gordon_Smith_\(footballer_born_1924\).md "wikilink")（Gordon
Smith）及[-{zh-hans:亚里克斯·汉密尔顿;
zh-hk:阿歷斯·咸美頓;}-](../Page/亚里克斯·汉密尔顿.md "wikilink")（Alex
Hamilton）下，登地奪得1961/62年球季的頂級聯賽冠軍（當時稱為甲組聯賽），在戰勝[聖莊士東](../Page/圣庄士东足球俱乐部.md "wikilink")（St
Johnstone）贏取錦標的同時，亦將聖莊士東送落乙組聯賽。

登地在翌季參加[1962–63年歐洲冠軍盃](../Page/1962–63年歐洲冠軍盃.md "wikilink")，在初賽圈兩回合累計8-5擊敗[科隆](../Page/科隆足球俱乐部.md "wikilink")，第一輪兩回合累計4-2淘汰[-{zh-hans:里斯本竞技;
zh-hk:士砵亭;}-](../Page/士砵亭.md "wikilink")，半準決賽兩回合累計6-2殺敗[-{zh-hans:安德莱赫特;
zh-hk:安德列治;}-](../Page/安德莱赫特足球俱乐部.md "wikilink")，晉級準決賽遭遇[AC米蘭](../Page/米兰足球俱乐部.md "wikilink")，首回合作客大敗1-5，次回合返回主場小勝1-0，兩場累計2-5被當屆最終冠軍AC米蘭淘汰。

登地參加1967/68年[欧洲博览会杯](../Page/欧洲联盟杯足球赛.md "wikilink")，第一輪兩回合累計4-2淘汰[DWS](../Page/Door_Wilskracht_Sterk.md "wikilink")（AFC
DWS），第二輪兩回合累計7-2大勝[RFC列日](../Page/R.F.C._de_Liège.md "wikilink")（Liège），第三輪輪空，半準決賽兩回合累計2-0小勝[FC苏黎世](../Page/苏黎世足球俱乐部.md "wikilink")（FC
Zürich），晉級準決賽對[利兹联](../Page/利兹联足球俱乐部.md "wikilink")，首回合主場1-1平手，次回合僅負0-1，兩場累計1-2再一次被最終冠軍列斯聯淘汰。

在1960年代的黃金期後，登地只曾於1973/74年球季罕有地奪得[聯賽盃冠軍](../Page/蘇格蘭聯賽盃.md "wikilink")，在咸普頓公園球場舉行的決賽登地以1-0擊敗[-{zh-hans:凯尔特人;
zh-hk:些路迪;}-](../Page/凯尔特人足球俱乐部.md "wikilink")，當時登地的隊長正是前[里斯本雄狮之一的](../Page/里斯本雄狮.md "wikilink")[托米·格梅尔](../Page/Tommy_Gemmell.md "wikilink")（Tommy
Gemmell），格梅尔其後曾短時間擔任登地的領隊。

在2000年登地作出球隊最重要的收購，簽入綽號「風之子」的[阿根廷國腳](../Page/阿根廷國家足球隊.md "wikilink")[克劳迪奥·卡尼吉亚](../Page/Claudio_Caniggia.md "wikilink")（Claudio
Caniggia）<small>\[1\]</small>，由於表色，僅效力一季即過檔到[-{zh-hans:流浪者;
zh-hk:格拉斯哥流浪;}-](../Page/流浪者足球俱乐部.md "wikilink")<small>\[2\]</small>。肯尼基亞是登地在2000年代初期簽入無數高姿態的外援球員之一，當時的領隊是[伊万诺·博内蒂](../Page/Ivano_Bonetti.md "wikilink")（Ivano
Bonetti）<small>\[3\]</small>，僅帶領球隊獲得聯賽頭六位的名次<small>\[4\]</small>，於2002年7月3日被辭退<small>\[5\]</small>。

  - 2002/03年蘇格蘭足總盃

登地在第三圈作客2-0淘汰[格拉斯哥球隊](../Page/格拉斯哥.md "wikilink")[巴特里](../Page/Partick_Thistle_F.C..md "wikilink")，第四圈返回主場2-0淨勝[-{zh-hans:阿伯丁;
zh-hk:鴨巴甸;}-](../Page/阿伯丁足球俱乐部.md "wikilink")，在半準決賽遭到[-{zh-hans:法尔科克;
zh-hk:福爾柯克;}-的頑抗](../Page/法尔科克足球俱乐部.md "wikilink")，作客1-1平手，返回主場重賽才在加時後4-1擊潰對手，晉級準決賽於2003年4月20日在中立的咸普頓公園球場對[高地球隊](../Page/高地_\(苏格兰行政区\).md "wikilink")[-{zh-hans:因弗内斯;
zh-hk:恩華尼斯;}-](../Page/因弗内斯足球俱乐部.md "wikilink")，只有少於15,000名球迷入場觀戰，結果憑著[格鲁吉亚的](../Page/格鲁吉亚.md "wikilink")[格奥尔吉·内姆萨泽](../Page/Giorgi_Nemsadze.md "wikilink")（Giorgi
Nemsadze）的入球，將登地帶入自1964年後的首次足總盃決賽<small>\[6\]</small>，決賽在5月31日同樣在咸普頓公園球場舉行，對手是[流浪者](../Page/流浪者足球俱乐部.md "wikilink")，[-{zh-hans:洛伦佐·阿莫鲁索;
zh-hk:亞武羅素;}-](../Page/Lorenzo_Amoruso.md "wikilink")（Lorenzo
Amoruso）在66分鐘一箭定江山，格拉斯哥流浪克完成本土錦標三冠王的美夢，亦結束登地的足總盃之旅<small>\[7\]</small>。

  - 財務危機

於2003年年終登地由於入不敷支，負債約2,000萬[英鎊](../Page/英鎊.md "wikilink")，11月24日球會班主马尔兄弟（Peter
& James
Marr）要求[安永會計師事務所作債務重組](../Page/安永會計師事務所.md "wikilink")，翌日即裁減25名職球員，包括高薪球星如[-{zh-hans:法布里齐奥·拉瓦内利;
zh-hk:拉雲拿利;}-](../Page/法布里齐奥·拉瓦内利.md "wikilink")、[-{zh-hans:法比安·卡瓦莱罗;
zh-hk:卡巴尼路;}-](../Page/Fabián_Caballero.md "wikilink")（Fabián
Caballero）、[-{zh-hans:克雷格·伯利;
zh-hk:奇治貝利;}-](../Page/Craig_Burley.md "wikilink")（Craig
Burley）及[格奥尔吉·内姆萨泽](../Page/Giorgi_Nemsadze.md "wikilink")（Giorgi
Nemsadze）等<small>\[8\]</small>。2004年5月26日登地的債權人投票支持債務重組方案，接受債權人自願協議（creditors'
voluntary
agreement），使登地可以脫離債務重組免於[清盤及逃過來季被扣](../Page/清盤.md "wikilink")10分的罰則<small>\[9\]</small>。在登地債務重組期間由球迷成立拯救球隊的「*Dee4Life*
基金」共籌募16萬[英鎊](../Page/英鎊.md "wikilink")，獲董事局分配160萬股，而马尔兄弟再加送150萬股，約佔球隊5%的股權<small>\[10\]</small>。

2005年5月24日球季最後一輪比賽，賽前共有4隊陷入降級危機，包括[利文斯顿](../Page/利文斯顿足球俱乐部.md "wikilink")（34分，主場對登地）、[邓迪联](../Page/邓迪联足球俱乐部.md "wikilink")（33分，作客對[因弗内斯](../Page/因弗内斯足球俱乐部.md "wikilink")）、[鄧弗姆林](../Page/邓弗姆林足球俱乐部.md "wikilink")（34分，作客對[基马诺克](../Page/基马诺克足球俱乐部.md "wikilink")）及登地（32分，作客對利雲斯頓），由於登地及利雲斯頓對戰，有34分而[得失球差佔優的鄧弗姆林基本上岸](../Page/得失球差.md "wikilink")<small>\[11\]</small>，最後登地只能1-1賽和利雲斯頓，其餘兩場賽果已不重要，登地降級到甲組<small>\[12\]</small>。降級後球隊要求全體球員減薪25%<small>\[13\]</small>。8月25日與球隊共渡艱難的領隊占·杜菲（Jim
Duffy）被辭退<small>\[14\]</small>。

2006年5月15日登地剩餘700萬英鎊債務由马尔兄弟私人承擔後脫離[赤字](../Page/赤字.md "wikilink")<small>\[15\]</small>，5月17日球隊宣佈重組計劃，將（Dens
Park）出售給一個慈善基金，再租回給登地，马尔兄弟出售控制股權，只保留26%股權<small>\[16\]</small>。2007年3月12日马尔兄弟辭任登地的董事<small>\[17\]</small>。

## 球會榮譽

  - **[甲組聯賽〈第一級〉](../Page/蘇格蘭足球聯賽.md "wikilink")**

:\***冠軍 (1)：** 1961/62年；

:\***亞軍 (4)：** 1902/03年、1906/07年、1908/09年、1948/49年；

  - **[甲組聯賽〈第二級〉](../Page/蘇格蘭甲組足球聯賽.md "wikilink")**

:\***冠軍 (3)：** 1978/79年、1991/92年、1997/98年；

:\***亞軍 (3)：** 1980/81年、2007/08年、2009/10年；

  - **[乙組聯賽〈第二級〉](../Page/蘇格蘭足球聯賽.md "wikilink")**

:\***冠軍 (1)：** 1946/47年；

  - **[蘇格蘭足總盃](../Page/蘇格蘭足總盃.md "wikilink")**

:\***冠軍 (1)：** 1909/10年；

:\***亞軍 (4)：** 1924/25年、1951/52年、1963/64年、2002/03年；

  - **[蘇格蘭聯賽盃](../Page/蘇格蘭聯賽盃.md "wikilink")**

:\***冠軍 (3)：** 1951/52年、1952/53年、1973/74年；

:\***亞軍 (3)：** 1967/68年、1980/81年、1995/96年；

  - **[蘇格蘭聯賽挑戰杯](../Page/蘇格蘭聯賽挑戰杯.md "wikilink")**

:\***冠軍 (2)：** 1990/91年、2009/10年；

:\***亞軍 (1)：** 1994/95年；

  - **「Evening Telegraph」挑戰盃**（Evening Telegraph Challenge Cup）

:\***冠軍 (1)：** 2006年；

## 著名球員

  - （Dariusz Adamczuk，1996-99）

  - （Ivano Bonetti，2000-02）

  - （Fabián Caballero，2000-05）

  - [-{zh-hans:克劳迪奥·卡尼吉亚;
    zh-hk:肯尼基亞;}-](../Page/克劳迪奥·卡尼吉亚.md "wikilink")（Claudio
    Caniggia，2000-01）

  - （Tommy Gemmell，1973-76）

  - （Alan Gilzean，1957-64）

  - [-{zh-hans:基辛尼舒維尼;
    zh-hk:基薩尼舒華列;}-](../Page/蘇拉比·基辛尼舒維尼.md "wikilink")（Zurab
    Khizanishvili，2001-03）

  - （Nacho Novo，2002-04）

  - （Gavin Rae，1996-04）

  - [-{zh-hans:法布里齐奥·拉瓦内利;
    zh-hk:拉雲拿利;}-](../Page/法布里齐奥·拉瓦内利.md "wikilink")（2003）

  - [-{zh-hans:布伦特·桑乔;
    zh-hk:賓特山曹;}-](../Page/布伦特·桑乔.md "wikilink")（2003-05）

  - （Julián Speroni，2001-04）

  - [范志毅](../Page/范志毅.md "wikilink")（2001-02）

## 註腳

## 外部連結

  - [邓迪官方網站](http://www.dundeefc.co.uk/)
  - [Dee4Life](http://www.dee4life.com/)

[Category:蘇格蘭足球俱樂部](../Category/蘇格蘭足球俱樂部.md "wikilink")
[Category:球迷拥有的足球俱乐部](../Category/球迷拥有的足球俱乐部.md "wikilink")
[Category:1893年建立的足球俱樂部](../Category/1893年建立的足球俱樂部.md "wikilink")

1.  [Caniggia signs up at
    Dundee](http://news.bbc.co.uk/sport2/hi/football/teams/d/dundee/956129.stm)
2.  [Caniggia puts Ibrox move on
    hold](http://news.bbc.co.uk/sport2/hi/football/teams/r/rangers/1326773.stm)
3.  [The Bonetti
    years](http://news.bbc.co.uk/sport2/hi/scotland/2085203.stm)
4.  [Dundee clinch top six
    place](http://news.bbc.co.uk/sport2/hi/football/scot_prem/1265682.stm)
5.  [Bonetti departure 'by mutual
    consent'](http://news.bbc.co.uk/sport2/hi/football/teams/d/dundee/2083141.stm)
6.  [Dundee book final
    place](http://news.bbc.co.uk/sport2/hi/football/scot_div_1/2959781.stm)
7.  [Rangers complete
    Treble](http://news.bbc.co.uk/sport2/hi/football/scot_div_1/2946904.stm)
8.  [Dundee sack 25
    staff](http://news.bbc.co.uk/sport2/hi/football/teams/d/dundee/3286019.stm)
9.  [Dundee rescue plan
    agreed](http://news.bbc.co.uk/sport2/hi/football/teams/d/dundee/3749583.stm)
10. [Supporters take shares in
    Dundee](http://news.bbc.co.uk/sport2/hi/football/teams/d/dundee/4148277.stm)
11. [Survival battle captures the
    fans](http://news.bbc.co.uk/sport2/hi/football/scot_prem/4535073.stm)
12. [Dundee lose fight to stay in
    SPL](http://news.bbc.co.uk/sport2/hi/football/scot_prem/4569111.stm)
13. [Wilkie among players facing
    cuts](http://news.bbc.co.uk/sport2/hi/football/teams/d/dundee/4115150.stm)
14. [Dundee move to sack manager
    Duffy](http://news.bbc.co.uk/sport2/hi/football/teams/d/dundee/4183576.stm)
15. [Dundee move back into the
    black](http://news.bbc.co.uk/sport2/hi/football/teams/d/dundee/4773763.stm)
16. [Dundee confirms restructure
    plan](http://news.bbc.co.uk/2/hi/uk_news/scotland/tayside_and_central/4989840.stm)
17. [Marrs to end Dundee
    involvement](http://news.bbc.co.uk/sport2/hi/football/scot_div_1/6443653.stm)