**渐台二**（**β Lyr** /
**天琴座β**）是在[天琴座的一個](../Page/天琴座.md "wikilink")[双星系統](../Page/聯星.md "wikilink")，距離[地球大約](../Page/地球.md "wikilink")882[光年](../Page/光年.md "wikilink")。在[阿拉伯的傳統名稱是](../Page/阿拉伯.md "wikilink")**الشلياق
Sheliak**，意思是"烏龜"或"豎琴"。

[Eclipsing_binary_star_animation_3.gif](https://zh.wikipedia.org/wiki/File:Eclipsing_binary_star_animation_3.gif "fig:Eclipsing_binary_star_animation_3.gif")

天琴座β中，A、B星是由藍白色（B7II）主星和深埋在气体中的蓝色（B0.5V）伴星組成的一對[半分离双星系統](../Page/半分离双星.md "wikilink")。天琴座β是這一類[食變星的原型](../Page/聯星#食雙星.md "wikilink")，系統內的成員很接近，因而相互間的萬有引力足以將對方[光球層的物质拉出來](../Page/光球層.md "wikilink")，因此恆星已經變形成為橢球的形狀，并发生质量转移。A星初始质量很大，因此很快膨胀到了巨星阶段，它的物质源源不断流向B星，使得原本较轻的B星变成双星中较重的一颗，并且减慢了B星的演化，使得B尚在[主序星阶段](../Page/主序星.md "wikilink")。现今A星已经损失了初始质量的75%以上，目前质量略小于太阳的3倍，B星深埋在A抛出的大量气体尘埃之中，显得非常昏暗。但是B目前的质量已经达到了太阳的13-15倍之大。天琴座β的[視星等以](../Page/視星等.md "wikilink")12.9075天的週期在+3.4等至+4.6等之間變化，A、B星因為太接近而無法光學望遠鏡解析出來，它们是[光譜聯星](../Page/聯星#光譜雙星.md "wikilink")。

這個系統的视线方向还有好几颗恒星。其中一对[光学双星](../Page/光学双星.md "wikilink")（在同一视线方向，但实际距离很远，没有物理上的关系）距離A/B星系统45.7"，光譜為B7V，視星等+7.2，可以輕鬆的用雙筒望遠鏡看見；它们的亮度約為太陽的80倍，也是光譜雙星，週期4.34天。另外还有一顆较远的恆星——天琴座βF，角距離86"，視星等+9.9，光度是太陽的7倍。

## 相關條目

  - [科幻小說中的天琴座β](../Page/科幻小說中的恆星和行星系統#天琴座β\(Sheliak\).md "wikilink")
  - [大犬座29](../Page/大犬座29.md "wikilink")
  - [天蝎座π](../Page/天蝎座π.md "wikilink")
  - [環狀星雲](../Page/環狀星雲.md "wikilink")
  - [織女星](../Page/織女星.md "wikilink")
  - [仙女座ζ](../Page/仙女座ζ.md "wikilink")

## 組成

| 名稱                  | [赤經](../Page/赤經.md "wikilink")                    | [赤緯](../Page/赤緯.md "wikilink") | [視星等](../Page/視星等.md "wikilink") (V) | [光譜類型](../Page/光譜類型.md "wikilink") | 參考資料庫                                                                                                                                            |
| ------------------- | ------------------------------------------------- | ------------------------------ | ------------------------------------ | ---------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------ |
| 天琴座βB (HD 174664)   | 18<sup>h</sup> 50<sup>m</sup> 06.7053<sup>s</sup> | \+33° 21' 06.678''             | 7.13                                 | B5V                                | [Simbad](http://simbad.u-strasbg.fr/simbad/sim-id?protocol=html&Ident=HD+174664&NbIdent=1&Radius=2&Radius.unit=arcmin&submit=submit+id)          |
| 天琴座βC (HD 174639)   | 18<sup>h</sup> 50<sup>m</sup> 01.2<sup>s</sup>    | \+33° 21' 26''                 |                                      | B2                                 | [Simbad](http://simbad.u-strasbg.fr/simbad/sim-id?protocol=html&Ident=HD+174639&NbIdent=1&Radius=2&Radius.unit=arcmin&submit=submit+id)          |
| 天琴座βD (BD+33 3223D) | 18<sup>h</sup> 50<sup>m</sup> 09.4<sup>s</sup>    | \+33° 22' 09''                 | 15.15                                |                                    | [Simbad](http://simbad.u-strasbg.fr/simbad/sim-id?protocol=html&Ident=BD%2b33%20%203223D&NbIdent=1&Radius=2&Radius.unit=arcmin&submit=submit+id) |
| 天琴座βE (BD+33 3222)  | 18<sup>h</sup> 50<sup>m</sup> 01.1654<sup>s</sup> | \+33° 22' 34.957''             | 10.5                                 | G5                                 | [Simbad](http://simbad.u-strasbg.fr/simbad/sim-id?protocol=html&Ident=BD%2b33%20%203223D&NbIdent=1&Radius=2&Radius.unit=arcmin&submit=submit+id) |
| 天琴座βF (BD+33 3225)  | 18<sup>h</sup> 50<sup>m</sup> 06.6524<sup>s</sup> | \+33° 23' 07.211''             | 10.6                                 | G5                                 | [Simbad](http://simbad.u-strasbg.fr/simbad/sim-id?protocol=html&Ident=BD%2b33%20%203225&NbIdent=1&Radius=2&Radius.unit=arcmin&submit=submit+id)  |

## 外部連結

  -
  -
  -
  - <http://www.alcyone.de/cgi-bin/search.pl?object=HR7106>

[Category:天琴座](../Category/天琴座.md "wikilink")
[Category:天琴β型变星](../Category/天琴β型变星.md "wikilink")
[天琴座β](../Category/拜耳天體.md "wikilink")
[Category:聯星](../Category/聯星.md "wikilink")
[Category:B-型主序星](../Category/B-型主序星.md "wikilink")
[Category:光譜聯星](../Category/光譜聯星.md "wikilink")
[Category:有固有名的恆星](../Category/有固有名的恆星.md "wikilink")
[Category:渐台 (星官)](../Category/渐台_\(星官\).md "wikilink")