**贝拉娜·朵芮丝**（**B'Elanna
Torres**），由[罗克珊·道森饰演](../Page/罗克珊·道森.md "wikilink")，是[科幻](../Page/科幻.md "wikilink")[电视剧](../Page/电视剧.md "wikilink")《[星际旅行：航海家号](../Page/星际旅行：航海家号.md "wikilink")》的一名虚构角色。她是[联邦星舰航海家号的轮机长](../Page/联邦星舰航海家号_\(NCC-74656\).md "wikilink")。

朵芮丝是《星际旅行》中唯一有着西裔背景的角色。

## 概览

于2349年出生在[联邦殖民地](../Page/星际联邦.md "wikilink")[克西克IV星](../Page/克西克IV星.md "wikilink")（Kessik
IV）的朵芮丝，拥有着一个不幸福的童年。她的人类父亲与[克林贡母亲经常会发生争斗](../Page/克林贡.md "wikilink")，当她5岁时，她的父亲终于离开了这个家（但后来的剧情又说到这件事发生在她12岁时）。他回到了[地球](../Page/地球.md "wikilink")，抛弃了她们母女俩。

作为人类与克林贡的混血儿，朵芮丝继承了克林贡人好斗的天性。她曾因为同学丹尼尔·伯德（Daniel Byrd）不断地嘲笑自己为“乌龟头小姐”
（Miss Turtlehead）而揍了对方一顿。朵芮丝一生都带着这种好斗的习性，但她在后来学会了如何控制它。

### 星际舰队学院与马奇游击队

2368年，19岁的朵芮丝从[星际舰队学院辍学](../Page/星际舰队学院.md "wikilink")，并成为了[马奇反抗组织的一员](../Page/马奇游击队.md "wikilink")。她在那里产生了对[卡达西人深恶痛绝的恨意](../Page/卡达西.md "wikilink")。朵芮丝开始和马奇舰长[查可泰交往](../Page/查可泰.md "wikilink")，并在他的星舰[瓦尔·吉恩号上担任轮机长的职位](../Page/瓦尔·吉恩号.md "wikilink")。他们后来一同被[守护者带到了](../Page/守护者.md "wikilink")[第四象限](../Page/银河系象限#第四象限.md "wikilink")。

在马奇游击队的时候，朵芮丝曾经重新给一颗卡达西[导弹](../Page/导弹.md "wikilink")“无畏号”（Dreadnought）编程。这颗由[人工智能构建的导弹最初的目标是马奇游击队的军事基地](../Page/人工智能.md "wikilink")。朵芮丝将其改为了卡达西人的军事基地，但这颗“无畏号”却也被守护者意外地带到了第四象限。

## 外部链接

  -
  - [贝拉娜·朵芮丝](http://www.startrek.com/startrek/view/series/VOY/character/1112388.html)－StarTrek.com，《星际旅行》的官方网站

[Category:星际旅行角色](../Category/星际旅行角色.md "wikilink")