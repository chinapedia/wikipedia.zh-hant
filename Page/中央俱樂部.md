**中央俱樂部組織**（**C**entral
**C**lub，簡稱**CC**）是[中國國民黨主要派系](../Page/中國國民黨.md "wikilink")，也有人認為CC的領導是[陳立夫與](../Page/陳立夫.md "wikilink")[陳果夫兄弟都姓陳](../Page/陳果夫.md "wikilink")（Chen），所以簡稱CC。\[1\]

## 成因

[陳果夫及](../Page/陳果夫.md "wikilink")[陳立夫](../Page/陳立夫.md "wikilink")，人稱**二陳**，是蔣介石的[結義兄長](../Page/結義.md "wikilink")[陳其美之姪子](../Page/陳其美.md "wikilink")，憑此關係，1928年2月，[蔣介石任命陳立夫代理國民黨中央組織部長](../Page/蔣介石.md "wikilink")，由二陳著手組建“[國民黨中央執行委員會調查統計局](../Page/中统局.md "wikilink")”。在[八年抗战爆发前夕它的顶峰期间](../Page/八年抗战.md "wikilink")，CC派已有上万成员，成為四大家族之一，民間有所謂「蔣家[天下陳家](../Page/天下.md "wikilink")[黨](../Page/黨.md "wikilink")，宋家姐妹孔家财」之說，陳果夫被譽為“[國民黨之](../Page/國民黨.md "wikilink")[教父](../Page/教父.md "wikilink")”。\[2\]

## 消亡

否定CC的人則認為，黨同伐異，大陸失守，CC派難辭其咎。中华民国政府撤退到臺灣時，[李宗仁](../Page/李宗仁.md "wikilink")、[白崇禧](../Page/白崇禧.md "wikilink")[桂系集團已徹底崩潰](../Page/桂系.md "wikilink")，CC派的利用價值已經降低，加上蔣中正決定培植[蔣經國](../Page/蔣經國.md "wikilink")，於是決定削藩。蔣中正召見陳立夫，問他對“國民黨改造”有何想法，陳立夫說：大陸失敗，黨、政、軍三方面都應有人出面承擔責任，黨的方面由他和陳果夫承擔，因此他們兄弟不宜參加黨的改造。蔣中正默默不語。蔣中正下令免去陳果夫[中央財務委員會主任職務](../Page/中央財務委員會.md "wikilink")，裁撤中央合作金庫、[中國農民銀行](../Page/中國農民銀行.md "wikilink")，削去了CC的經濟權力。

不久[陳誠以坐擁軍權而攻陳](../Page/陳誠.md "wikilink")，陳誠曾說：“把[陳立夫送上](../Page/陳立夫.md "wikilink")[火燒島](../Page/綠島.md "wikilink")”，陳果夫久病在床，[陳立夫以參加](../Page/陳立夫.md "wikilink")“道德重整會議”的名義，自願放逐流亡[美國](../Page/美國.md "wikilink")[紐澤西州湖林城外养鸡](../Page/新澤西州.md "wikilink")，直至陳誠死後才被蔣中正召回臺灣。\[3\]

## 參考文獻

{{-}}

[Category:中国国民党大陆时期派系](../Category/中国国民党大陆时期派系.md "wikilink")
[CC系](../Category/CC系.md "wikilink")

1.

2.

3.