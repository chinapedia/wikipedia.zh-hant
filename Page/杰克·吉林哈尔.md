**傑可布·班傑明·「傑克」·葛倫霍**（，又譯為**吉倫荷**，），[美国電影演員](../Page/美国.md "wikilink")。他的父親是導演[史帝芬·葛倫霍](../Page/史帝芬·葛倫霍.md "wikilink")，[瑞典](../Page/瑞典.md "wikilink")[貴族後裔](../Page/貴族.md "wikilink")。母親是[猶太人家庭出身的編劇](../Page/猶太人.md "wikilink")[娜歐蜜·芳納](../Page/娜歐蜜·芳納.md "wikilink")。姊姊[瑪姬·葛倫霍也是演員](../Page/瑪姬·葛倫霍.md "wikilink")。葛倫霍從七歲開始演戲，正式出道後在短時間內接演了許多風格迥異的角色。他曾獲頒[英國電影學院獎](../Page/英國電影學院獎.md "wikilink")，也曾獲得[奧斯卡金像獎提名](../Page/奧斯卡金像獎.md "wikilink")。

葛倫霍首次在大型電影中演出是1999年的《[十月的天空](../Page/十月的天空.md "wikilink")》，在電影中他飾演[荷默·希坎姆](../Page/荷默·希坎姆.md "wikilink")，一个夢想是與朋友們一起製造並且試射[火箭](../Page/火箭.md "wikilink")，迫於現實環境，曾經一度放棄後，再度在老師的鼓勵下，重新完成夢想的角色。之後在2004年的賣座電影《[明日之後](../Page/明日之後.md "wikilink")》中，他扮演一位遭遇劇烈[全球寒冷化現象的學生](../Page/全球寒冷化.md "wikilink")，在片中葛倫霍和[丹尼斯·奎德合作演出](../Page/丹尼斯·奎德.md "wikilink")。之後他一改之前的角色形象，在電影《[鍋蓋頭](../Page/鍋蓋頭.md "wikilink")》（*Jarhead*，2005年）中飾演感到失意困惑的[美國海軍陸戰隊員](../Page/美國海軍陸戰隊.md "wikilink")；同一年，葛倫霍也以在《[斷背山](../Page/斷背山.md "wikilink")》中的[同性戀](../Page/同性戀.md "wikilink")[牛仔角色獲得贏得評論的讚賞](../Page/牛仔.md "wikilink")。

葛倫霍也多次參與支持各種政治和社會運動。他在[美國民主黨為](../Page/民主党_\(美国\).md "wikilink")[2004年總統大選所企劃的](../Page/2004年美国总统选举.md "wikilink")「Rock
the
Vote」宣傳廣告中現身，他也參與了[環境保護活動和](../Page/环境保护主义.md "wikilink")[美國公民自由聯盟的宣傳](../Page/美國公民自由聯盟.md "wikilink")。

## 早期生活和教育

葛倫霍出生於美國加利福尼亞州[洛杉磯](../Page/洛杉磯.md "wikilink")，父親是電影導演[史帝芬·葛倫霍](../Page/史帝芬·葛倫霍.md "wikilink")，母親是電影製作人和編劇[娜歐蜜·芳納](../Page/娜歐蜜·芳納·吉倫荷.md "wikilink")\[1\]\[2\]。葛倫霍的父親在一個[斯維登堡哲學思想家庭中成長](../Page/斯維登堡哲學思想.md "wikilink")，他同時也是[瑞典貴族家庭的後裔](../Page/瑞典.md "wikilink")\[3\]\[4\]；該家族最後一個瑞典籍祖先是他的曾曾曾祖父[安得斯·李奧納多·葛倫霍](../Page/李奧納多·吉倫荷.md "wikilink")（Anders
Leonard
Gyllenhaal）\[5\]。葛倫霍的母親來自[紐約市的美國](../Page/紐約市.md "wikilink")[猶太人家庭](../Page/猶太人.md "wikilink")\[6\]\[7\]\[8\]\[9\]，她是[哥倫比亞大學歷史學教授](../Page/哥倫比亞大學.md "wikilink")[艾瑞克·方納](../Page/艾瑞克·方納.md "wikilink")（Eric
Foner）的[前妻](../Page/離婚.md "wikilink")。

葛倫霍在[猶太教的環境下成長](../Page/猶太教.md "wikilink")，而他成為[律法之子](../Page/律法之子.md "wikilink")（B'nai
Mitzvah）的儀式是在遊民收容中心舉行，是因為父母希望他能夠對於現在優渥的生活知所感恩\[10\]\[11\]\[12\]。葛倫霍的父母堅持他必須在暑假打工供應自己的生活，於是他便在家族朋友經營的餐廳擔任警衛和服務生\[13\]。

## 演藝生涯

### 早期

由於他的家人與電影工業有不淺的淵源，葛倫霍幼时便經常接觸電影製作。十一歲時，葛倫霍將首次演出獻給了1991年喜劇片《[城市鄉巴佬](../Page/城市鄉巴佬.md "wikilink")》，飾演[比利·克里斯托的兒子](../Page/比利·克里斯托.md "wikilink")。他的雙親不同意他演出1992年的電影《[野鴨變鳳凰](../Page/野鴨變鳳凰.md "wikilink")》，因為葛倫霍將因此必須離家兩個月\[14\]。接下來的二年，他的雙親准許葛倫霍進行角色試鏡，但禁止他在被選中後帶他們到現場\[15\]。他們多次讓葛倫霍出現在他父親的電影。葛倫霍曾於1993年電影《》（與姐姐[美姬·佳蘭賀同演](../Page/瑪姬·吉倫荷.md "wikilink")）、1994年的影集《[謀殺：街上的故事](../Page/謀殺：街上的故事.md "wikilink")》（Homicide:
Life on the
Street）、1998年的喜劇《[王牌追殺令](../Page/王牌追殺令.md "wikilink")》（Homegrown）中演出，並曾與母親、姐姐美姬在義大利的烹飪節目《摩托·瑪莉歐》（Molto
Mario）中出現過兩集。而在就讀高中前，葛倫霍唯一演過不是父親執導的電影是《喬許與S.A.M.》（Josh and S.A.M.）。

1998年，葛倫霍於位於洛杉磯的[哈佛西湖學校](../Page/哈佛西湖學校.md "wikilink")（Harvard-Westlake
School）畢業，隨後就讀於[哥倫比亞大學](../Page/哥倫比亞大學.md "wikilink")，攻讀東方宗教以及哲學，並於兩年後輟學、把精力專注於演藝事業上，待來日再完成學位\[16\]。葛倫霍首次領銜主演的電影是改編自[荷默·希坎姆自傳](../Page/荷默·希坎姆.md "wikilink")《[火箭男孩](../Page/火箭男孩.md "wikilink")》（*Rocket
Boys*）、由[喬·約翰斯頓執導的](../Page/喬·約翰斯頓.md "wikilink")《[十月的天空](../Page/十月的天空.md "wikilink")》，葛倫霍在片中飾演一位來自[西維吉尼亞州](../Page/西維吉尼亞州.md "wikilink")、一心一意想獲得科學獎學金以避免成為一位[煤礦工人的年輕人](../Page/煤礦工人.md "wikilink")。該片票房達到三千兩百萬美元，薩克拉門托新聞與評論並形容該片是葛倫霍的「突破之作」\[17\]\[18\]。

2006年，葛倫霍以《[斷背山](../Page/斷背山.md "wikilink")》提名[奥斯卡最佳男配角奖](../Page/奥斯卡最佳男配角奖.md "wikilink")，不過當時風采全被[希斯·萊傑搶走](../Page/希斯·萊傑.md "wikilink")，傑克不但不介意，私底下與希斯有著深厚交情，還是對方小孩的教父，希斯過世時，他更在喪禮上與希斯的妻子[蜜雪兒·威廉絲互相攙扶痛哭](../Page/蜜雪兒·威廉絲.md "wikilink")。2015年，他宣傳新片《[震撼擂台](../Page/震撼擂台.md "wikilink")》，提到希斯也感性表示：「只要我還活著，我就會一直想他。」讓人見到他重情義的一面。他曾在接受訪問時，聊到《斷》片中跟萊傑的床戲，他敬業表示：「雖然很不自在，我們都深深相信電影想闡述的意念與想法，畢竟他不是我們的電影，而是所有人的電影\[19\]。」

### 轉型

傑克在描述現今全球嗜血化的[媒體環境現狀的電影](../Page/媒體.md "wikilink")《[獨家腥聞](../Page/獨家腥聞.md "wikilink")》中，首次擔綱製片，除了擔綱製片外也是本片的男主角，傑克本人更為了這部電影爆瘦了30磅（約13.6公斤）以達成效果，而傑克激烈減重的方法便是每天從家中跑到片場。傑克在本片的表現獲得各界好評，多名影評人表示這是傑克繼《[十月的天空](../Page/十月的天空.md "wikilink")》後從影以來最突破之作，傑克也因為本片入圍了第72屆[金球獎戲劇纇最佳男主角](../Page/金球獎.md "wikilink")。\[20\]

### 個人生活

2007年初傑克曾和知名女星[瑞絲·薇斯朋交往](../Page/瑞絲·薇斯朋.md "wikilink")，兩人交往了兩年多，最後在2009年11月分手。
傑克曾和美國知名歌手[泰勒絲於](../Page/泰勒絲.md "wikilink")2010年間交往過，兩人是透過[葛妮絲·派特洛牽引下認識](../Page/葛妮絲·派特洛.md "wikilink")，但交往一個多月隨即分手，隔年年初傑克本人有意願想要復合但遭到拒絕，泰勒絲收錄在專輯《[Red](../Page/Red.md "wikilink")》中的火紅單曲《[We
Are Never Ever Getting Back
Together](../Page/We_Are_Never_Ever_Getting_Back_Together.md "wikilink")》便是在描述他們之間的短暫戀情。

## 電影作品

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>片名</p></th>
<th><p>角色</p></th>
<th><p>附註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>中文</p></td>
<td><p>原文</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1991年電影.md" title="wikilink">1991</a></p></td>
<td><p><a href="../Page/城市鄉巴佬.md" title="wikilink">城市鄉巴佬</a></p></td>
<td><p><em>City Slickers</em></p></td>
<td><p>Danny Robbins</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1993年電影.md" title="wikilink">1993</a></p></td>
<td></td>
<td><p><em>A Dangerous Woman</em></p></td>
<td><p>Edward</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1993年電影.md" title="wikilink">1993</a></p></td>
<td></td>
<td><p><em>Josh and S.A.M.</em></p></td>
<td><p>Leon Coleman</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1998年電影.md" title="wikilink">1998</a></p></td>
<td></td>
<td><p><em>Homegrown</em></p></td>
<td><p>Jake / Blue Kahan</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1999年電影.md" title="wikilink">1999</a></p></td>
<td><p><a href="../Page/十月的天空.md" title="wikilink">十月的天空</a></p></td>
<td><p><em>October Sky</em></p></td>
<td><p>Homer Hickam Jr.</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2001年電影.md" title="wikilink">2001</a></p></td>
<td><p><a href="../Page/死亡幻覺.md" title="wikilink">死亡幻覺</a></p></td>
<td><p><em>Donnie Darko</em></p></td>
<td><p>Donnie Darko</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><em>Bubble Boy</em></p></td>
<td><p>Jimmy Livingston</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><em>Lovely &amp; Amazing</em></p></td>
<td><p>Jordan</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2002年電影.md" title="wikilink">2002</a></p></td>
<td></td>
<td><p><em>Highway</em></p></td>
<td><p>Pilot Kelson</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/讓愛自由.md" title="wikilink">讓愛自由</a></p></td>
<td><p><em>Moonlight Mile</em></p></td>
<td><p>Joe Nast</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/麥田捕手的女孩.md" title="wikilink">麥田捕手的女孩</a></p></td>
<td><p><em>The Good Girl</em></p></td>
<td><p>Thomas 'Holden' Worther</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2004年電影.md" title="wikilink">2004</a></p></td>
<td><p><a href="../Page/明日之後.md" title="wikilink">明日之後</a></p></td>
<td><p><em>The Day After Tomorrow</em></p></td>
<td><p>Sam Hall</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2005年電影.md" title="wikilink">2005</a></p></td>
<td><p><a href="../Page/斷背山.md" title="wikilink">斷背山</a></p></td>
<td><p><em>Brokeback Mountain</em></p></td>
<td><p>Jack Twist</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鍋蓋頭.md" title="wikilink">鍋蓋頭</a></p></td>
<td><p><em>Jarhead</em></p></td>
<td><p>Anthony Swofford ("Swoff")</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><em>Proof</em></p></td>
<td><p>Harold 'Hal' Dobbs</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>-</p></td>
<td><p><em></em></p></td>
<td><p>Narrator</p></td>
<td><p>配音</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2007年電影.md" title="wikilink">2007</a></p></td>
<td><p><a href="../Page/索命黃道帶.md" title="wikilink">索命黃道帶</a></p></td>
<td><p><em>Zodiac</em></p></td>
<td><p>Robert Graysmith</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><em>Rendition</em></p></td>
<td><p>Douglas Freeman</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2009年電影.md" title="wikilink">2009</a></p></td>
<td><p><a href="../Page/雙情路.md" title="wikilink">雙情路</a></p></td>
<td><p><em>Brothers</em></p></td>
<td><p>Tommy Cahill</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2010年電影.md" title="wikilink">2010</a></p></td>
<td><p><a href="../Page/波斯王子：時之刃.md" title="wikilink">波斯王子：時之刃</a></p></td>
<td><p><em>Prince of Persia: The Sands of Time</em></p></td>
<td><p>Prince Dastan</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/愛情藥不藥.md" title="wikilink">愛情藥不藥</a></p></td>
<td><p><em>Love and Other Drugs</em></p></td>
<td><p>Jamie Randall</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2011年電影.md" title="wikilink">2011</a></p></td>
<td><p><a href="../Page/啟動原始碼.md" title="wikilink">啟動原始碼</a></p></td>
<td><p><em>Source Code</em></p></td>
<td><p>Colter Stevens</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2012年電影.md" title="wikilink">2012</a></p></td>
<td><p><a href="../Page/火線赤子情.md" title="wikilink">火線赤子情</a></p></td>
<td><p><em>End of Watch</em></p></td>
<td><p>Officer Taylor</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2013年電影.md" title="wikilink">2013</a></p></td>
<td><p><a href="../Page/私法爭鋒.md" title="wikilink">私法爭鋒</a></p></td>
<td><p><em>Prisoners</em></p></td>
<td><p>Detective Loki</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><em>Enemy</em></p></td>
<td><p>Adam Bell / Anthony Clair</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2014年電影.md" title="wikilink">2014</a></p></td>
<td><p><a href="../Page/獨家腥聞.md" title="wikilink">獨家腥聞</a></p></td>
<td><p><em>Nightcrawler</em></p></td>
<td><p>Lou Bloom</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2015年電影.md" title="wikilink">2015</a></p></td>
<td><p><a href="../Page/震撼擂台.md" title="wikilink">震撼擂台</a></p></td>
<td><p><em>Southpaw</em></p></td>
<td><p>Billy Hope</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/聖母峰_(電影).md" title="wikilink">聖母峰</a></p></td>
<td><p><em>Everest</em></p></td>
<td><p>Scott Fischer</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/崩壞人生.md" title="wikilink">崩壞人生</a></p></td>
<td><p><em>Demolition</em></p></td>
<td><p>Davis Mitchell</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><em>Accidental Love</em></p></td>
<td><p>Howard Birdwell</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2016年電影.md" title="wikilink">2016</a></p></td>
<td><p><a href="../Page/夜行動物_(電影).md" title="wikilink">夜行動物</a></p></td>
<td><p><em>Nocturnal Animals</em></p></td>
<td><p>Edward Sheffield / Tony Hastings</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2017年電影.md" title="wikilink">2017</a></p></td>
<td><p><a href="../Page/異星智慧.md" title="wikilink">異星智慧</a></p></td>
<td><p><em>Life</em></p></td>
<td><p>Dr. David Jordan</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/玉子_(電影).md" title="wikilink">玉子</a></p></td>
<td><p><em>Okja</em></p></td>
<td><p>Dr. Johnny Wilcox</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/你是我的勇氣.md" title="wikilink">你是我的勇氣</a></p></td>
<td><p><em>Stronger</em></p></td>
<td><p>Jeff Bauman</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2018年電影.md" title="wikilink">2018</a></p></td>
<td></td>
<td><p><em>Wildlife</em></p></td>
<td><p>Jerry Brinson</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/淘金殺手.md" title="wikilink">淘金殺手</a></p></td>
<td><p><em>The Sisters Brothers</em></p></td>
<td><p>John Morris</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2019年電影.md" title="wikilink">2019</a></p></td>
<td><p><a href="../Page/絲絨電鋸.md" title="wikilink">絲絨電鋸</a></p></td>
<td><p><em>Velvet Buzzsaw</em></p></td>
<td><p>Morf Vandewalt</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蜘蛛人：離家日.md" title="wikilink">蜘蛛人：離家日</a></p></td>
<td><p><em>Spider-Man: Far From Home</em></p></td>
<td><p>「<a href="../Page/神秘法師_(漫威漫畫).md" title="wikilink">神秘法師</a>」昆汀·貝克</p></td>
<td></td>
</tr>
</tbody>
</table>

## 奖项

| 年份                                         | 頒獎團體                                     | 項目          | 結果                                       | 演出作品                                   |
| ------------------------------------------ | ---------------------------------------- | ----------- | ---------------------------------------- | -------------------------------------- |
| 2001                                       | [獨立精神獎](../Page/獨立精神獎.md "wikilink")     | 最佳男主角       | 提名                                       | [死亡幻覺](../Page/死亡幻覺.md "wikilink")     |
| Chlotrudis Awards                          | 最佳演员                                     | 获奖          |                                          |                                        |
| 2002                                       | [DVD獨家獎](../Page/DVD獨家獎.md "wikilink")   | DVD首映獎、最佳演員 | 提名                                       | [高速驚魂](../Page/高速驚魂.md "wikilink")     |
| [青少年票選獎](../Page/青少年票選獎.md "wikilink")     | 票選最佳突破男星                                 | 提名          | [麥田捕手的女孩](../Page/麥田捕手的女孩.md "wikilink") |                                        |
| 好萊塢青年獎                                     | 突破演出男星                                   | 獲獎          |                                          |                                        |
| 2006                                       | [MTV電影大獎](../Page/MTV電影大獎.md "wikilink") | 最佳演出        | 獲獎                                       | [斷背山](../Page/斷背山.md "wikilink")       |
| 最佳螢幕接吻                                     | 獲獎                                       |             |                                          |                                        |
| [電影演員公會獎](../Page/電影演員公會獎.md "wikilink")   | 最佳男配角                                    | 提名          |                                          |                                        |
| 最佳服裝                                       | 提名                                       |             |                                          |                                        |
| [國家影評人委員會](../Page/國家影評人委員會.md "wikilink") | 最佳男配角                                    | 獲獎          |                                          |                                        |
| [影評人票選獎](../Page/廣播電影影評人協會.md "wikilink")  | 最佳男配角                                    | 提名          |                                          |                                        |
| [英國電影學院獎](../Page/英國電影學院獎.md "wikilink")   | 最佳男配角                                    | 獲獎          |                                          |                                        |
| [奧斯卡金像獎](../Page/奧斯卡金像獎.md "wikilink")     | [最佳男配角](../Page/奧斯卡最佳男配角獎.md "wikilink") | 提名          |                                          |                                        |
| [衛星獎](../Page/衛星獎.md "wikilink")           | 最佳傑出男配角                                  | 提名          |                                          |                                        |
| [美國藝術協會獎](../Page/美國藝術協會獎.md "wikilink")   | 2006年青年藝術獎 - 傑出藝術性獎項                     | 獲獎          |                                          |                                        |
| [衛星獎](../Page/衛星獎.md "wikilink")           | 最佳傑出男配角                                  | 提名          | [鍋蓋頭](../Page/鍋蓋頭.md "wikilink")         |                                        |
| 2017                                       | [好萊塢電影獎](../Page/好萊塢電影獎.md "wikilink")   | 最佳男主角       | 獲獎                                       | [你是我的勇氣](../Page/你是我的勇氣.md "wikilink") |
|                                            |                                          |             |                                          |                                        |

## 註釋

## 參考文獻

## 外部链接

  - [Official website](http://www.jakegyllenhaal.com)

  -
  - [Official
    E-group](http://movies.groups.yahoo.com/group/gyllenhaalgroup/)

  - ["100 Hottest
    Bachelors 2006"](http://people.aol.com/people/gallery/0,26335,1201344_1127388,00.html)，from
    People magazine

[Category:20世纪美国男演员](../Category/20世纪美国男演员.md "wikilink")
[Category:美國電影男演員](../Category/美國電影男演員.md "wikilink")
[Category:MTV電影大獎得主](../Category/MTV電影大獎得主.md "wikilink")
[Category:美國猶太人](../Category/美國猶太人.md "wikilink")
[Category:瑞典裔美國人](../Category/瑞典裔美國人.md "wikilink")
[Category:加利福尼亚州民主党人](../Category/加利福尼亚州民主党人.md "wikilink")
[Category:英国电影学院奖最佳男配角获得者](../Category/英国电影学院奖最佳男配角获得者.md "wikilink")
[Category:洛杉矶市男演员](../Category/洛杉矶市男演员.md "wikilink")
[Category:哥倫比亞大學校友](../Category/哥倫比亞大學校友.md "wikilink")
[Category:英格蘭裔美國人](../Category/英格蘭裔美國人.md "wikilink")
[Category:美國舞台男演員](../Category/美國舞台男演員.md "wikilink")
[Category:21世紀美國男演員](../Category/21世紀美國男演員.md "wikilink")

1.

2.  弗雷德·史切努爾
    (2005年10月30日)，["傑克的發展"](http://www.guardian.co.uk/film/2005/oct/30/awardsandprizes.oscars2006)，*保護者*。2006-9-19查閱。

3.

4.

5.  Nate
    Bloom（2004年6月11日），[Rootsweb.com](http://worldconnect.rootsweb.com/cgi-bin/igm.cgi?op=GET&db=arc150&id=I098841)，2006年9月19日引用查證

6.

7.

8.

9.

10. Contact Music（2005年9月6日），[Gyllenhaal's Homeless Shelter
    Bar-Mitzvah](http://www.contactmusic.com/new/xmlfeed.nsf/mndwebpages/gyllenhaals%20homeless%20shelter%20bar-mitzvah)，2006年9月19日引用查證。

11.

12. Nate Bloom（2004年6月11日），[Celebrity
    Jews](http://www.jewishsf.com/content/2-0-/module/displaystory/story_id/22714/edition_id/455/format/html/displaystory.html)，*Jewish
    News Weekly*，2006年9月19日引用查證。

13. 史蒂芬·霍恩(2004)，["傑克·吉倫荷的採訪"](http://movies.ign.com/articles/519/519613p1.html)，*Ign.com*。2006-9-19查閱。

14.
15.
16.
17. 多明尼哥·威爾斯(2006)，["J傑克·吉倫荷的傳記"](http://www.tiscali.co.uk/entertainment/film/biographies/jake_gyllenhaal_biog/5)
    ，*Tiscali.com*，第5頁。2006-9-16查閱。

18. 馬克·哈維森(1998)，[十月的天空評論](http://www.rottentomatoes.com/m/october_sky/articles/167357/a_breakout_performance_by_jake_gyllenhaal)，*News
    & Review*。2006-9-21查閱。

19.

20.