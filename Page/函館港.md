[Hakodate_Port_Hokkaido_Japan01s5.jpg](https://zh.wikipedia.org/wiki/File:Hakodate_Port_Hokkaido_Japan01s5.jpg "fig:Hakodate_Port_Hokkaido_Japan01s5.jpg")

**函館港**（）是位於日本[函館市的一個港口](../Page/函館市.md "wikilink")。面向[函館灣](../Page/函館灣.md "wikilink")。港灣管理者是[函館市](../Page/函館市.md "wikilink")。在港灣法上，函館港被指定為重要港灣\[1\]。在港則法上，函館港則被指定為特定港。

## 概要

[Hachimanzaka_Hakodate_Hokkaido_pref_Japan01n.jpg](https://zh.wikipedia.org/wiki/File:Hachimanzaka_Hakodate_Hokkaido_pref_Japan01n.jpg "fig:Hachimanzaka_Hakodate_Hokkaido_pref_Japan01n.jpg")
函館港是一座波浪較少的天然良港，自中世時期開始就開闢為港口。在[江戶時代](../Page/江戶時代.md "wikilink")，函館港是[北前船的停留地](../Page/北前船.md "wikilink")。自[明治時代之後](../Page/明治.md "wikilink")，函館港則是[北海道的玄關及](../Page/北海道.md "wikilink")[北洋漁業的本據地](../Page/北洋漁業.md "wikilink")，十分繁榮。現在函館港主要是一座日本國內貿易港口，主要出口的貨物是[水泥和](../Page/水泥.md "wikilink")[石材](../Page/石材.md "wikilink")，主要進口的貨物則是[石油製品和](../Page/石油製品.md "wikilink")[廢棄物](../Page/廢棄物.md "wikilink")。函館港的貨物貿易量在北海道所有的港口中僅次於[苫小牧港](../Page/苫小牧港.md "wikilink")，位於第二位\[2\]。另外為了處理港灣內部堆積的大量砂土，函館港利用疏浚得到的砂土在港灣南部填海出了面積約為8公頃的人工島綠之島\[3\]。

## 歷史

### 海外開港以前

[函館灣內的](../Page/函館灣.md "wikilink")[箱館一帶在過去被稱為宇須岸](../Page/箱館.md "wikilink")（）\[4\]，在15世紀時期由[河野氏統治](../Page/河野氏.md "wikilink")。雖然受到[愛努人攻撃的影響](../Page/愛努人.md "wikilink")，貿易曾一度減少。但在16世紀早期時，[若狹國的商船每年都會到達宇須岸三次](../Page/若狹國.md "wikilink")，海岸上有不少批發商。宇須岸是一座天然良港，貿易曾十分繁榮。但在1512年，宇須岸被[愛努人攻擊](../Page/愛努人.md "wikilink")。之後日本人主要聚集在[松前和](../Page/松前町_\(北海道\).md "wikilink")[上之國](../Page/上之國町.md "wikilink")，宇須岸陷入衰退。

在18世紀早期，[松前藩將](../Page/松前藩.md "wikilink")[龜田劃為](../Page/龜田郡.md "wikilink")[和人地的東限](../Page/和人地.md "wikilink")\[5\]。當時在函館灣沿岸，箱館已有無人居住的房屋，並且在這一時期開始，箱館的名稱自宇須岸改為箱館湊。但相比之下，流入函館灣的[龜田川河口一帶的龜田湊更加繁榮](../Page/龜田川.md "wikilink")。然而據1717年（[享保](../Page/享保.md "wikilink")2年）《松前蝦夷記》的記載，由於砂土的流入，廻船已經無法停留在龜田湊。取而代之的是，箱館湊逐漸得到了開發。在箱館，[龜田半島南岸生產的](../Page/龜田半島.md "wikilink")[海帶是最主要的貿易商品](../Page/海帶.md "wikilink")。在享保年間，自松前藩將當地的貿易經營改為城堡制之後，貿易活動更加興盛，入港的船舶也增加了。

箱館湊除了是[松前城下近郊採獲的海帶的貿易港之外](../Page/松前城.md "wikilink")，也是[蝦夷地東部貨物輸入進入和人地的貿易港](../Page/蝦夷地.md "wikilink")，港口實現了飛躍性的發展。因此在1741年，龜田番所由龜田遷至箱館。另外在1785年，在箱館還設置了長崎俵物會所，箱館成為北國的海產品的集荷據點
\[6\]。據《東遊記》記載，和松前及江差相比，無論風向如何，箱館湊都利於船舶停靠。進入19世紀後，幕府於1801年（[享和元年](../Page/享和.md "wikilink")）在內澗町開鑿了水渠。在1804年（[文化元年](../Page/文化_\(年號\).md "wikilink")）修建了人工島，並且在島上建造了造船廠，1811年（文化8年）又修建了沖之口番所。被任命為蝦夷地御用定雇船頭的[高田屋嘉兵衛以箱館為據點](../Page/高田屋嘉兵衛.md "wikilink")\[7\]，開展箱館和[國後島](../Page/國後島.md "wikilink")、[擇捉島](../Page/擇捉島.md "wikilink")、[根室之間的貿易](../Page/根室支廳.md "wikilink")，對箱館的發展做出了貢獻。

### 開港之後

#### 幕末

[Hakodatemaru_Hakodate_Hokkaido_Japan01bs5.jpg](https://zh.wikipedia.org/wiki/File:Hakodatemaru_Hakodate_Hokkaido_Japan01bs5.jpg "fig:Hakodatemaru_Hakodate_Hokkaido_Japan01bs5.jpg")（復原船）\]\]
1854年3月31日，日本和美國簽訂[日美和親條約](../Page/日美和親條約.md "wikilink")，箱館於同日開港\[8\]。在開港之後，[馬修·培理率領旗艦以下的](../Page/馬修·培理.md "wikilink")5艘船隻在同年5月17日進入箱館湊，進行測量等工作，當時有多位船員都認為箱館頗為類似[伊比利亞半島先端的](../Page/伊比利亞半島.md "wikilink")[直布羅陀](../Page/直布羅陀.md "wikilink")\[9\]。在這一條約簽訂翌年的1855年，由於箱館已作為燃料、水和食物的補給港開港，[江戶幕府將箱館附近改為直轄領地](../Page/江戶幕府.md "wikilink")，並設置[箱館奉行](../Page/箱館奉行.md "wikilink")。為了加強港口的防衛和外交，還搬遷了[奉行所](../Page/奉行所.md "wikilink")。1859年，在安政條約簽訂之後，幕府在沙洲的中央開鑿了南北方向的水渠，使得用於搬運物資的小船得以通航。同年7月1日，箱館正式對各國開港。在開始和外國進行貿易之後，箱館主要的貿易品是對[英國和](../Page/英國.md "wikilink")[美國出口](../Page/美國.md "wikilink")[魷魚絲和海帶等產品](../Page/魷魚絲.md "wikilink")，但箱館的貿易額要遠低於[横濱港及](../Page/横濱港.md "wikilink")[長崎港](../Page/長崎港.md "wikilink")\[10\]。

#### 近代

在[明治維新之後](../Page/明治維新.md "wikilink")，箱館於1869年被劃入[開拓使管轄](../Page/開拓使.md "wikilink")，改名為函館港。自1879年開始，進行了港湾整備的調査。根據擔當的[內務省技師](../Page/內務省_\(日本\).md "wikilink")[羅文霍斯特·穆爾德爾](../Page/羅文霍斯特·穆爾德爾.md "wikilink")（*Anthonie
Thomas Lubertus Rouwenhorst
Mulder*）的提案，新政府改變了龜田川的河道，改使其流入面向[津輕海峽的](../Page/津輕海峽.md "wikilink")[大森濱](../Page/大森濱.md "wikilink")，以防止泥沙流入港口\[11\]。在同年，開拓使將[青函航路的運營委託給](../Page/青函航路.md "wikilink")[三菱會社](../Page/三菱財閥.md "wikilink")，但業者對三菱的評價卻不高。1882年，開拓使被廢止之後，開拓使的業務被移交至[農商務省](../Page/農商務省_\(日本\).md "wikilink")，船舶也被借給北海道運輸會社。同年，在北海道運輸被合併入[共同運輸會社之後](../Page/共同運輸會社.md "wikilink")，共同運輸會社也開始經營青函航路的業務，並和三菱之間展開激烈競爭。為防止兩家公司都陷入倒閉，經過政府的仲裁，1885年9月，兩家公司合併，並設立了[日本郵船株式會社](../Page/日本郵船.md "wikilink")。

[Kanemori_Red_Brick_Warehouse_Hakodate_Hokkaido_pref_Japan01n.jpg](https://zh.wikipedia.org/wiki/File:Kanemori_Red_Brick_Warehouse_Hakodate_Hokkaido_pref_Japan01n.jpg "fig:Kanemori_Red_Brick_Warehouse_Hakodate_Hokkaido_pref_Japan01n.jpg")，背景是[函館山](../Page/函館山.md "wikilink")\]\]
1896年（明治29年）開始，函館港開始了大規模築港工作，政府對港內進行疏浚，並且設置了砂防堤、[防波堤](../Page/防波堤.md "wikilink")、[燈塔](../Page/燈塔.md "wikilink")，並且[填海修建了](../Page/填海.md "wikilink")[碼頭](../Page/碼頭.md "wikilink")。1907年（明治40年），函館港被指定為第二種[重要港灣](../Page/重要港灣.md "wikilink")。在翌年的1908年（明治41年），隨著[國鐵開設了](../Page/日本國有鐵道.md "wikilink")[青函連絡船](../Page/青函連絡船.md "wikilink")\[12\]，函館港成為連接[北海道和](../Page/北海道.md "wikilink")[本州之間的港口](../Page/本州.md "wikilink")，地位更加重要。另外在1907年（明治40年），隨著日俄漁業條約的簽訂，日本在[俄羅斯](../Page/俄羅斯.md "wikilink")[領海內的](../Page/領海.md "wikilink")[漁業權得到擴大](../Page/漁業權.md "wikilink")\[13\]。函館港在[大正時代作為](../Page/大正.md "wikilink")[北洋漁業的據點](../Page/北洋漁業.md "wikilink")，海產品的出口十分發達。在[第二次世界大戰中](../Page/第二次世界大戰.md "wikilink")，函館港扮演了將北海道的[煤炭運往本州的角色](../Page/煤炭.md "wikilink")。在1945年7月，美軍對函館港進行空襲，12艘連絡船幾乎全部被毀。

#### 戰後

在1951年，函館港被指定為重要港灣，同時也被適用於[港灣運送事業法](../Page/港灣運送事業法.md "wikilink")。此外基於[出入國管理令](../Page/出入國管理令.md "wikilink")，政府規定有外國人出如的港口需按照[檢疫法管理](../Page/檢疫法.md "wikilink")，函館港被指定為檢疫港。函館港自1952年開始修築的中央碼頭也在1958年完成。在此期間，於1953年，函館港被移交給[函館市管理](../Page/函館市.md "wikilink")。1952年，隨著北洋漁業重新開始，函館港的[大馬哈魚](../Page/大馬哈魚.md "wikilink")、[鱒魚漁業一度重新發達](../Page/鱒魚.md "wikilink")。但在1977年，蘇聯設定專屬經濟區之後，函館港的北洋漁業據點的地位也宣告結束。函館近海的烏賊捕撈的風景被稱為函館象徵之一\[14\]，但烏賊漁業在1950年代到達高峰之後也陷入蕭條。

[Rera_ferry.jpg](https://zh.wikipedia.org/wiki/File:Rera_ferry.jpg "fig:Rera_ferry.jpg")
在1964年（昭和39年），函館港開通了和[大間港](../Page/大間港.md "wikilink")（現[青森縣](../Page/青森縣.md "wikilink")[大間町](../Page/大間町.md "wikilink")）之間的航線。1967年（昭和42年），[道南海運](../Page/道南海運.md "wikilink")（後改名為[東日本渡船](../Page/東日本渡船.md "wikilink")，現在的名稱是[津輕海峽渡船](../Page/津輕海峽渡船.md "wikilink")）開通了函館港和[青森港之間的渡船](../Page/青森港.md "wikilink")。另外在1974年，萬代碼頭開始投入使用。另一方面，在1988年，隨著[青函隧道的開通](../Page/青函隧道.md "wikilink")，青函連絡船結束了其歷史任務。1991年開始，函館港開始在港町地區修建大型公共碼頭。在2002年和2003年，水深達14米和12米的碼頭分別開始投入使用\[15\]。在2007年和2014年，為對應為渡輪公司開始使用新型高速船和大型船，函館港啟用了新的客運渡輪樞紐。

## 港口現狀

客運是函館港的主要機能之一。現在有兩家渡輪公司經營函館港和其他港口之間的定期航線。[津輕海峽渡輪公司經營有函館港至](../Page/津輕海峽渡輪.md "wikilink")[青森港和函館港至](../Page/青森港.md "wikilink")[大間港的航線](../Page/大間港.md "wikilink")。青森和函館之間每天有8艘往返客船，而青森和大間之間每天有兩艘往返客船\[16\]。而青函渡輪則每天運營有8班往返函館港至青森港的渡輪\[17\]。在貨運方面，2012年，函館港的總貿易量達到3617萬噸，其中161.7萬噸是外貿，3455.3萬噸是內貿\[18\]。

在港口設施方面，函館港內水域主要劃分為三條航路\[19\]。函館港的主要碼頭包括了西碼頭、豐川碼頭、中央碼頭、萬代碼頭、北碼頭、港町碼頭。其中西碼頭和豐川碼頭主要進行水產品貿易，而其他的碼頭的主要貿易產品則多為工業產品或穀物等農業產品\[20\]。而客運碼頭則集中在港町地區。除了物流機能之外，函館港在近年著重發展觀光、研究、親水、休閒的新的港口機能\[21\]。

## 参考文献

<references />

## 參見

  - [安政條約開港五港中的其他四港](../Page/安政條約.md "wikilink")：[横濱港](../Page/横濱港.md "wikilink")
    - [新潟港](../Page/新潟港.md "wikilink") -
    [神戶港](../Page/神戶港.md "wikilink") -
    [長崎港](../Page/長崎港.md "wikilink")

## 外部連結

  - [函館市港湾空港部](http://www.city.hakodate.hokkaido.jp/soshiki/kouwan_dept/)
  - [インフォ函館](http://www.infohakodate.com/index.html)

[Category:北海道港口](../Category/北海道港口.md "wikilink")
[Category:函館市](../Category/函館市.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.

9.

10.

11.

12.

13.

14.

15.
16.

17.

18.
19.

20.

21.