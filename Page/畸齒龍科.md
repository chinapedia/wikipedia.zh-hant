**畸齒龍科**（Heterodontosauridae），又名**異齒龍科**，意為「有不同牙齒的蜥蜴」，是[鳥臀目](../Page/鳥臀目.md "wikilink")[恐龍的一科](../Page/恐龍.md "wikilink")，經常被認為是群基礎[鳥腳類恐龍](../Page/鳥腳類.md "wikilink")，但最近的研究顯示畸齒龍科與[頭飾龍類關係密切](../Page/頭飾龍類.md "wikilink")。雖然畸齒龍科的化石很罕見，牠們從[侏羅紀早期存活在全球各地](../Page/侏羅紀.md "wikilink")，少數物種存活到[白堊紀早期](../Page/白堊紀.md "wikilink")。

畸齒龍科的體型如[狐狸般大小](../Page/狐狸.md "wikilink")，身長少於2公尺。畸齒龍科因牠們的獨特牙齒而著名，包括類似[犬齒的長牙](../Page/犬齒.md "wikilink")、以及適合咀嚼的頰齒，頰齒與[白堊紀](../Page/白堊紀.md "wikilink")[鴨嘴龍科的牙齒類似](../Page/鴨嘴龍科.md "wikilink")。畸齒龍科是[草食性恐龍](../Page/草食性.md "wikilink")，或可能是[雜食性](../Page/雜食性.md "wikilink")。

## 描述

在畸齒龍科中，只有[畸齒龍發現了完整的骨骸](../Page/畸齒龍.md "wikilink")。已發現[醒龍的零散骨骸](../Page/醒龍.md "wikilink")，但並沒有完整的敘述，其他的畸齒龍科恐龍只有發現零散的下頜與牙齒。因此，大多數畸齒龍科恐龍的[共有衍徵來自於牙齒與頜部骨頭](../Page/共有衍徵.md "wikilink")\[1\]\[2\]。畸齒龍的身長超過1公尺\[3\]，但是[狼鼻龍的零散化石顯示牠們身長可達](../Page/狼鼻龍.md "wikilink")2倍\[4\]。
[Heterodontosaurus_model.jpg](https://zh.wikipedia.org/wiki/File:Heterodontosaurus_model.jpg "fig:Heterodontosaurus_model.jpg")的頭顱骨與重建圖\]\]

### 頭顱骨

醒龍與畸齒龍都有非常大的眼睛。眼睛之下的[顴骨往兩側發展](../Page/顴骨.md "wikilink")，這特徵同時見於[角龍下目恐龍](../Page/角龍下目.md "wikilink")。大多數[鳥腳下目恐龍的](../Page/鳥腳下目.md "wikilink")[前上頜骨前段是沒有牙齒的](../Page/前上頜骨.md "wikilink")，可能形成角質喙狀嘴；但畸齒龍的前上頜骨前段具有牙齒。許多鳥腳下目恐龍的前上頜骨牙齒與[上頜骨牙齒之間](../Page/上頜骨.md "wikilink")，有個大型牙縫，但畸齒龍科的這個牙縫呈現弓型。下頜的前段是[前齒骨](../Page/前齒骨.md "wikilink")，前齒骨是鳥臀目恐龍特有的骨頭。前齒骨也具有角質喙狀嘴，類似前上頜骨。下頜的所有牙齒都在[齒骨之上](../Page/齒骨.md "wikilink")\[5\]。

### 牙齒

[Heterodontosaurus_and_Tianyulong.tif](https://zh.wikipedia.org/wiki/File:Heterodontosaurus_and_Tianyulong.tif "fig:Heterodontosaurus_and_Tianyulong.tif")與[天宇龍的頭顱骨比較圖](../Page/天宇龍.md "wikilink")\]\]
畸齒龍科恐龍的名稱來自於牠們的[異型齒](../Page/異型齒.md "wikilink")[齒列](../Page/齒列.md "wikilink")。牠們的[前上頜骨有三種牙齒](../Page/前上頜骨.md "wikilink")。生存於早[侏儸紀的](../Page/侏儸紀.md "wikilink")[醒龍](../Page/醒龍.md "wikilink")、[畸齒龍](../Page/畸齒龍.md "wikilink")、[狼鼻龍](../Page/狼鼻龍.md "wikilink")，前上頜骨的兩種牙齒是小而圓錐狀的，而第三種較大的牙齒外表類似[哺乳類](../Page/哺乳類.md "wikilink")[食肉目的](../Page/食肉目.md "wikilink")[犬齒](../Page/犬齒.md "wikilink")，這第三種牙齒常被稱為犬齒形牙齒。下頜的犬齒形牙齒比上頜的大，並位在[齒骨的第一個位置](../Page/齒骨.md "wikilink")，在嘴部閉合時，可與上頜的弓型牙縫相符\[6\]。畸齒龍與狼鼻龍的犬齒形牙齒的前緣與後緣都是鋸齒狀的，而醒龍的犬齒形牙齒只有前緣是鋸齒狀的\[7\]\[8\]。生存於早[白堊紀的](../Page/白堊紀.md "wikilink")[棘齒龍](../Page/棘齒龍.md "wikilink")，上頜有兩個犬齒形牙齒，並位在[上頜骨而非](../Page/上頜骨.md "wikilink")[前上頜骨](../Page/前上頜骨.md "wikilink")\[9\]；而存活在晚[侏儸紀](../Page/侏儸紀.md "wikilink")[北美洲的](../Page/北美洲.md "wikilink")[果齒龍](../Page/果齒龍.md "wikilink")，下頜的[齒骨可能共有兩個犬齒形牙齒](../Page/齒骨.md "wikilink")\[10\]\[11\]。

如同獨特的長牙，畸齒龍科恐龍的頰齒在早期[鳥臀目中也是相當獨特的](../Page/鳥臀目.md "wikilink")，是個衍化特徵。鳥臀目恐龍的頰齒位在頰部邊緣，這些小型牙齒具有鋸齒邊緣，是用來咀嚼[植被用](../Page/植被.md "wikilink")。所有畸齒龍科恐龍的頰齒，離[齒冠的前](../Page/齒冠.md "wikilink")1/3段才有鋸齒邊緣；其他鳥臀目的頰齒，鋸齒邊緣從齒冠延伸到近[齒根處](../Page/齒根.md "wikilink")。基礎物種（例如醒龍）的頰齒位在上頜骨與齒骨，並類似其他鳥臀目恐龍的頰齒：佔寬廣空間、低齒冠、齒冠到齒根間有明顯的[舌面隆突](../Page/舌面隆突.md "wikilink")（Cingulum）。更衍化的物種如狼鼻龍、畸齒龍，牠們的牙齒成鑿狀、高齒冠、沒有舌面隆突，所以齒冠與齒根的寬度是一樣的\[12\]。

這些衍化的頰齒是重疊的，所以當咀嚼時，這些齒冠形成連續的表面。齒列在嘴部兩側的稍微內側處，使得齒列外側形成肌肉構成的頰部，在咀嚼時發生作用。較晚的[白堊紀鴨嘴龍類與角龍下目恐龍](../Page/白堊紀.md "wikilink")，以及許多[草食性](../Page/草食性.md "wikilink")[哺乳類](../Page/哺乳類.md "wikilink")，都[趨同演化出類似的齒列](../Page/趨同演化.md "wikilink")。相對於鴨嘴龍類有數千顆不停生長、取代的牙齒，畸齒龍科的牙齒取代發生得更慢；目前已發現數個標本沒有正取代、生長中的牙齒。畸齒龍科恐龍的頜部內側缺乏[下頜孔](../Page/下頜孔.md "wikilink")，下頜孔被認為有助於牙齒發展，大多數[鳥臀目恐龍都有下頜孔](../Page/鳥臀目.md "wikilink")。畸齒龍科的[齒骨與](../Page/齒骨.md "wikilink")[前齒骨間有獨特的球狀關節](../Page/前齒骨.md "wikilink")，使得嘴部鼻合時，下頜往外側轉，頰齒互相磨合。因為牙齒取代率低，這些磨碎用牙齒磨損，因此較老牙齒的鋸齒邊緣有磨損跡象，但是最長的齒冠讓牙齒有更長的使用週期\[13\]。

### 身體骨骼

[Tianyulong.jpg](https://zh.wikipedia.org/wiki/File:Tianyulong.jpg "fig:Tianyulong.jpg")的化石\]\]
塔克畸齒龍（*Heterodontosaurus
tucki*）的顱後骨骼已有良好的敘述，但塔克畸齒龍被認為是早[侏儸紀畸齒龍科中最衍化的物種](../Page/侏儸紀.md "wikilink")，所以無法得知塔克畸齒龍與其他近親有哪些共同特徵\[14\]。畸齒龍科的前肢長度，約是後肢的70%長度，對恐龍而言相當長。[肱骨的發展良好](../Page/肱骨.md "wikilink")[三角嵴](../Page/三角嵴.md "wikilink")（Deltopectoral
crest，讓[胸大肌](../Page/胸大肌.md "wikilink")[三角肌三角附著的部分](../Page/三角肌.md "wikilink")），以及[尺骨的明顯](../Page/尺骨.md "wikilink")[鷹嘴突](../Page/鷹嘴突.md "wikilink")（Olecranon
process，讓[肱三頭肌附著的部份](../Page/肱三頭肌.md "wikilink")），顯示出強壯的前肢。前掌有五個[趾骨](../Page/趾骨.md "wikilink")；第一手指大，前端有銳利的彎曲指爪，手指可以往內轉\[15\]；第二手指最長，稍長於第三手指，第二、三手指都有指爪；而第四、第五指骨都沒有指爪，與第二、三手指相比，非常小。

畸齒龍科後肢的[脛骨長於](../Page/脛骨.md "wikilink")[股骨約](../Page/股骨.md "wikilink")30%長度，被認為是增加速度的適應演化結果。脛骨與[腓骨與踝部的](../Page/腓骨.md "wikilink")[距骨](../Page/距骨.md "wikilink")、[跟骨固定者](../Page/跟骨.md "wikilink")，形成[脛腓跗骨](../Page/脛腓跗骨.md "wikilink")；這是與現代[鳥類](../Page/鳥類.md "wikilink")[趨同演化的後果](../Page/趨同演化.md "wikilink")。[跗骨與](../Page/跗骨.md "wikilink")[蹠骨固定形成](../Page/蹠骨.md "wikilink")[跗蹠骨](../Page/跗蹠骨.md "wikilink")；也類似鳥類。腳掌上有四個腳趾，只有第二、三、四腳趾接觸到地面。尾巴沒有骨化[肌腱可以保持堅硬狀態](../Page/肌腱.md "wikilink")，所以可能是可彎曲的；不類似許多其他鳥臀目恐龍\[16\]。
[Tianyulong_BW.jpg](https://zh.wikipedia.org/wiki/File:Tianyulong_BW.jpg "fig:Tianyulong_BW.jpg")的想像圖\]\]
醒龍的破碎化石還沒被完整敘述過，但前肢與手掌比畸齒龍的小。但手掌的第四、第五手指有較少的[指骨](../Page/指骨.md "wikilink")\[17\]。

### 原始羽毛

生存在[白堊紀早期](../Page/白堊紀.md "wikilink")[中國的](../Page/中國.md "wikilink")[天宇龍](../Page/天宇龍.md "wikilink")，從頸部到尾巴覆蓋者原始[羽毛](../Page/羽毛.md "wikilink")。過去只有較晚期的[獸腳類恐龍發現這些原始的管狀羽毛](../Page/獸腳類.md "wikilink")，天宇龍的發現，顯示[鳥臀目與](../Page/鳥臀目.md "wikilink")[蜥臀目恐龍可能都是](../Page/蜥臀目.md "wikilink")[溫血動物](../Page/溫血動物.md "wikilink")\[18\]。

## 分類系統


<u>

<center>

理查德·巴特勒等人版本，2011年\[19\]

</center>

</u>
<u>

<center>

理查德·巴特勒等人版本，2010年\[20\]

</center>

</u>
[南非古生物學家](../Page/南非.md "wikilink")[羅伯特·布魯姆](../Page/羅伯特·布魯姆.md "wikilink")（Robert
Broom）在1911年建立[鶴龍](../Page/鶴龍.md "wikilink")，該化石為缺乏牙齒的下頜\[21\]。在1924年，[西德尼·霍頓](../Page/西德尼·霍頓.md "wikilink")（Sidney
H.
Haughton）建立[狼鼻龍](../Page/狼鼻龍.md "wikilink")，將狼鼻龍分類於[犬齒獸亞目](../Page/犬齒獸亞目.md "wikilink")\[22\]。[畸齒龍在](../Page/畸齒龍.md "wikilink")1962年命名，而狼鼻龍與鶴龍被認為是與畸齒龍關係最接近的[鳥臀目恐龍](../Page/鳥臀目.md "wikilink")\[23\]。[阿爾弗雷德·羅默](../Page/阿爾弗雷德·羅默.md "wikilink")（Alfred
Sherwood
Romer）在1966年成立畸齒龍科，包括畸齒龍與狼鼻龍\[24\]。畸齒龍科在1998年由[保羅·塞里諾](../Page/保羅·塞里諾.md "wikilink")（Paul
Sereno）定義為[演化支](../Page/演化支.md "wikilink")\[25\]，並在2005年重新定義為：包含塔克畸齒龍，以及所有親緣關係接近塔克畸齒龍，而離[沃克氏副櫛龍](../Page/副櫛龍.md "wikilink")、[懷俄明厚頭龍](../Page/厚頭龍.md "wikilink")、[恐怖三角龍](../Page/三角龍.md "wikilink")、[大面甲龍等屬較遠的所有物種](../Page/甲龍.md "wikilink")，所構成的[演化支](../Page/演化支.md "wikilink")\[26\]。

畸齒龍科經常包括以下屬：醒龍、狼鼻龍、畸齒龍，全都來自[南非](../Page/南非.md "wikilink")。Richard
Thulborn一度將這三屬都歸類於狼鼻龍\[27\]。其他所有作者都認為這三屬都是個別獨立的\[28\]。在本科中，畸齒龍與狼鼻龍被認為是姐妹屬，而醒龍是個基礎成物種\[29\]。鶴龍也被認為屬於畸齒龍科，但通常被認為是個[疑名](../Page/疑名.md "wikilink")，因為鶴龍[模式標本缺乏牙齒](../Page/模式標本.md "wikilink")，使得很難與本科其他屬做區隔\[30\]。最近，[棘齒龍屬在數個研究裡](../Page/棘齒龍.md "wikilink")，被認為屬於畸齒龍科\[31\]\[32\]。[羊毛龍在](../Page/羊毛龍.md "wikilink")1975年以一個下頜而命名\[33\]，但最近的發現顯示[羊毛龍](../Page/羊毛龍.md "wikilink")（*Lanasaurus*）屬於狼鼻龍，使得羊毛龍成為狼鼻龍屬的[次同物異名](../Page/次同物異名.md "wikilink")\[34\]。[滇中龍一度被認為是](../Page/滇中龍.md "wikilink")[亞洲的畸齒龍科恐龍](../Page/亞洲.md "wikilink")\[35\]，但已經證實滇中龍化石是[原蜥腳下目與](../Page/原蜥腳下目.md "wikilink")[中真鱷類化石的](../Page/中真鱷類.md "wikilink")[嵌合體](../Page/嵌合體.md "wikilink")\[36\]。[約瑟·波拿巴](../Page/約瑟·波拿巴.md "wikilink")（Jose
Bonaparte）一度將[南美洲的](../Page/南美洲.md "wikilink")[皮薩諾龍歸類於畸齒龍科](../Page/皮薩諾龍.md "wikilink")\[37\]，但皮薩諾龍現在被認為是更基礎的[鳥臀目恐龍](../Page/鳥臀目.md "wikilink")\[38\]。

相較牠們的不確定[種系發生學位置](../Page/種系發生學.md "wikilink")，畸齒龍科的內部物種是較容易確立的。數個早期研究顯示畸齒龍科是群非常原始的[鳥臀目恐龍](../Page/鳥臀目.md "wikilink")\[39\]\[40\]。基於前肢形態上的相似處，[羅伯特·巴克](../Page/羅伯特·巴克.md "wikilink")（Robert
T.
Bakker）假設畸齒龍科與早期[蜥腳形亞目如](../Page/蜥腳形亞目.md "wikilink")[近蜥龍有關係](../Page/近蜥龍.md "wikilink")，讓畸齒龍科成為[蜥臀目與鳥臀目之間的橋樑](../Page/蜥臀目.md "wikilink")\[41\]。過去數十年的主要假說，將畸齒龍科置為基礎[鳥腳下目恐龍](../Page/鳥腳下目.md "wikilink")\[42\]\[43\]\[44\]\[45\]。然而，有人提出畸齒龍科與[頭飾龍類有共同祖先](../Page/頭飾龍類.md "wikilink")，頭飾龍類包含[角龍下目與](../Page/角龍下目.md "wikilink")[厚頭龍下目](../Page/厚頭龍下目.md "wikilink")\[46\]\[47\]，而許多最近的研究支持這個假說\[48\]\[49\]。這個包含畸齒龍科與頭飾龍類的演化支名為[畸齒龍形類](../Page/畸齒龍形類.md "wikilink")\[50\]。畸齒龍科也被視為同時是鳥腳下目與頭飾龍類的基礎物種\[51\]\[52\]。在2007年的一個[親緣分支分類法研究](../Page/親緣分支分類法.md "wikilink")，提出畸齒龍科是[鳥臀目的基礎物種](../Page/鳥臀目.md "wikilink")，僅次於皮薩諾龍；這是一些較早研究的迴響\[53\]\[54\]。

## 地理分佈

[Heterodontosaurus.jpg](https://zh.wikipedia.org/wiki/File:Heterodontosaurus.jpg "fig:Heterodontosaurus.jpg")
在過去的研究裡，畸齒龍科僅出現於早[侏儸紀的](../Page/侏儸紀.md "wikilink")[南非](../Page/南非.md "wikilink")，現在有四個大陸發現畸齒龍科的化石。畸齒龍科剛出現時，[盤古大陸仍結合在一起](../Page/盤古大陸.md "wikilink")，使得畸齒龍科恐龍幾乎分布於各地\[55\]。目前已知最古老的化石是一個頜部碎片與分離的牙齒，發現於[阿根廷的Laguna](../Page/阿根廷.md "wikilink")
Colorado組，地質年代為晚[三疊紀](../Page/三疊紀.md "wikilink")。這些化石有衍化的形態，類似[畸齒龍](../Page/畸齒龍.md "wikilink")，包括一個犬齒形牙齒，前後緣都為鋸齒邊緣；以及上頜骨的高齒冠牙齒，缺乏[舌面隆突](../Page/舌面隆突.md "wikilink")（Cingulum）\[56\]。最多樣化的畸齒龍科[動物群](../Page/動物群.md "wikilink")，發現於南部[非洲的早](../Page/非洲.md "wikilink")[侏儸紀地層](../Page/侏儸紀.md "wikilink")，在該地區發現了[畸齒龍](../Page/畸齒龍.md "wikilink")、[狼鼻龍](../Page/狼鼻龍.md "wikilink")、以及可疑屬[鶴龍](../Page/鶴龍.md "wikilink")\[57\]
。

在[美國](../Page/美國.md "wikilink")\[58\]與[墨西哥](../Page/墨西哥.md "wikilink")\[59\]各自發現了未敘述的早[侏儸紀畸齒龍科化石](../Page/侏儸紀.md "wikilink")。在70年代晚期，美國[科羅拉多州](../Page/科羅拉多州.md "wikilink")[弗魯塔市附近的晚](../Page/弗魯塔_\(科羅拉多州\).md "wikilink")[侏儸紀](../Page/侏儸紀.md "wikilink")[莫里遜組發現了大量的畸齒龍科化石](../Page/莫里遜組.md "wikilink")。這些化石曾長時間沒有被正式研究，但這些牙齒類似[英格蘭的](../Page/英格蘭.md "wikilink")[棘齒龍](../Page/棘齒龍.md "wikilink")，而四肢化石非常類似[畸齒龍](../Page/畸齒龍.md "wikilink")，顯示這些物種也屬於畸齒龍科\[60\]。在2009年，這些小型化石被正式研究並建立為新屬，[果齒龍](../Page/果齒龍.md "wikilink")（*Fruitadens*）\[61\]。

在[西班牙](../Page/西班牙.md "wikilink")[葡萄牙的晚](../Page/葡萄牙.md "wikilink")[侏儸紀與早](../Page/侏儸紀.md "wikilink")[白堊紀地層](../Page/白堊紀.md "wikilink")，也發現了缺乏舌面隆突（Cingulum）的畸齒龍科牙齒\[62\]。在2002年，棘齒龍的化石最近被重新敘述，棘齒龍生存於早[白堊紀](../Page/白堊紀.md "wikilink")[貝里阿斯階的南](../Page/貝里阿斯階.md "wikilink")[英格蘭](../Page/英格蘭.md "wikilink")，牠們可能代表存活至晚期的畸齒龍科物種\[63\]。發現於[中國早](../Page/中國.md "wikilink")[侏儸紀地層的](../Page/侏儸紀.md "wikilink")[滇中龍](../Page/滇中龍.md "wikilink")，不再認為是畸齒龍科恐龍\[64\]。唯一生存於亞洲的畸齒龍科恐龍是[天宇龍](../Page/天宇龍.md "wikilink")，生存於[白堊紀早期](../Page/白堊紀.md "wikilink")\[65\]
。
[Abrictosaurus.jpg](https://zh.wikipedia.org/wiki/File:Abrictosaurus.jpg "fig:Abrictosaurus.jpg")的頭顱骨化石\]\]

## 古生物學

大多數畸齒龍科化石發現於乾旱或半乾旱的地質環境，包括[南非的](../Page/南非.md "wikilink")[上艾略特組](../Page/上艾略特組.md "wikilink")，以及[英格蘭南部的](../Page/英格蘭.md "wikilink")[波白克層](../Page/波白克層.md "wikilink")\[66\]。過去曾認為畸齒龍科在一年最乾旱的時候，會經歷過季節性的[夏眠與](../Page/夏眠.md "wikilink")[冬眠](../Page/冬眠.md "wikilink")。因為大多數畸齒龍科化石缺乏取代用牙齒，因此這些牙齒被認為在休眠期間全部替換，因為不斷生長的分散牙齒，會中斷由單一齒列形成的咀嚼用表面\[67\]。然而，這是基於對畸齒龍科的頜部結構的誤解\[68\]。科學家過去曾認為，畸齒龍科恐龍仍會不斷替換牠們的牙齒，但速率比其他[爬蟲類慢](../Page/爬蟲類.md "wikilink")。但[電腦斷層掃描顯示](../Page/電腦斷層掃描.md "wikilink")，畸齒龍的幼年與成年個體都沒有替換用的牙齒\[69\]。目前也沒有證據支持畸齒龍科有夏眠的[假設](../Page/假設.md "wikilink")\[70\]。

畸齒龍科恐龍的頰齒已演化成可磨碎堅硬植物，牠們也可能是[雜食性恐龍](../Page/雜食性.md "wikilink")。[前上頜骨的尖狀牙齒](../Page/前上頜骨.md "wikilink")，以及前肢的銳利、彎曲指爪，顯示某種程度的捕食行為。過去曾提出畸齒龍科恐龍的前肢長而強壯，可能用來挖開[昆蟲的巢](../Page/昆蟲.md "wikilink")，類似現代的[食蟻獸](../Page/食蟻獸.md "wikilink")。這些前肢也可能用來挖掘，可能用來挖掘[根部與](../Page/根部.md "wikilink")[塊莖](../Page/塊莖.md "wikilink")\[71\]。

畸齒龍的前肢與後肢長度接近，顯示牠們可能是部分四足動物；而突出的[鷹嘴突](../Page/鷹嘴突.md "wikilink")（Olecranon
process）以及前肢的可高度延展[指骨](../Page/指骨.md "wikilink")，可在許多四足動物身上找到。然而，手部結構明顯是用來抓握，而非支撐重量。後肢的許多特徵，包括長[脛骨與腳掌](../Page/脛骨.md "wikilink")，以及固定的[脛腓跗骨與](../Page/脛腓跗骨.md "wikilink")[跗蹠骨](../Page/跗蹠骨.md "wikilink")（Tarsometatarsus），顯示畸齒龍科恐龍可以用後肢快速奔跑，所以畸齒龍不太可能以四足移動，除非進食\[72\]。

所有已知畸齒龍科恐龍都發現了短尖牙，與現代[麝鹿](../Page/麝鹿.md "wikilink")、[西貒](../Page/西貒.md "wikilink")、以及[豬的長牙極度類似](../Page/豬.md "wikilink")。對於這些有長牙的動物（以及[海象與](../Page/海象.md "wikilink")[亞洲象](../Page/亞洲象.md "wikilink")），這些長牙作為[兩性異形特徵](../Page/兩性異形.md "wikilink")，並只有[雄性個體才有長牙](../Page/雄性.md "wikilink")。醒龍的[模式標本缺乏尖牙](../Page/模式標本.md "wikilink")，而該標本起初被敘述為雌性個體\[73\]，而未固定的[薦椎以及短臉部顯示這個標本是未成年特體](../Page/薦椎.md "wikilink")。而醒龍的第二個較大標本擁有明顯的尖牙。所以這些尖牙可能只發現於成年個體，而非[雄性的](../Page/雄性.md "wikilink")[第二性徵](../Page/第二性徵.md "wikilink")。這些尖牙可能用打鬥用，或是作為與同種動物或其他種動物的視覺展示物\[74\]。醒龍的幼年標本缺少長牙，而畸齒龍的幼年標本具有長牙，因此這成為辨別兩個物種的特徵之一。關於長牙的功能，另有防衛、[雜食性的食性等理論](../Page/雜食性.md "wikilink")\[75\]。

## 參考資料

## 外部連結

  - [畸齒龍科的分類](http://paleodb.org/cgi-bin/bridge.pl?user=Guest&action=beginTaxonInfo)
    - The Paleobiology Database

  - [畸齒龍科的分類歷史](https://web.archive.org/web/20080612143407/http://www.taxonsearch.org/dev/taxon_edit.php?Action=View&tax_id=165)
    - Taxon Search

[\*](../Category/畸齒龍科.md "wikilink")

1.

2.

3.

4.

5.
6.
7.

8.

9.

10.

11.

12.
13.

14.
15.

16.
17.

18.
19.

20.
21.

22.

23.

24.

25.

26.

27.
28.
29.
30.
31.
32.
33.

34.
35.

36.

37.

38.

39.
40.
41.
42.
43.
44.
45.

46.

47.

48.

49.

50.

51.

52.

53.

54.

55.
56.

57.
58.

59.

60.
61. Butler, R.J., P.M. Galton, L.B. Porro, L.M. Chiappe, D.M. Henderson,
    and G.M. Erickson. (2009). "Lower limits of ornithischian dinosaur
    body size inferred from a new Upper Jurassic heterodontosaurid from
    North America." *Proceedings of the Royal Society B*,
    10.1098/rspb.2009.1494

62.

63.
64.
65. Zheng, X-.T.,H.-L. You, X. Xu & Z.-M. Dong (2009). "An Early
    Cretaceous heterodontosaurid dinosaur with filamentous integumentary
    structures." *Nature*, 458, pp. 333-336.

66.
67.
68.

69.

70.
71.
72.
73.
74.
75.