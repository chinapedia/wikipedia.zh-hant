**保大**（[1121年](../Page/1121年.md "wikilink")－[1125年二月](../Page/1125年.md "wikilink")）是[辽朝君主](../Page/辽朝.md "wikilink")[辽天祚帝耶律延禧的](../Page/辽天祚帝.md "wikilink")[年號](../Page/年號.md "wikilink")，共計5年。是辽朝最后一个年号。

## 纪年

| 保大                               | 元年                                   | 二年                                   | 三年                                   | 四年                                   | 五年                                   |
| -------------------------------- | ------------------------------------ | ------------------------------------ | ------------------------------------ | ------------------------------------ | ------------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | [1121年](../Page/1121年.md "wikilink") | [1122年](../Page/1122年.md "wikilink") | [1123年](../Page/1123年.md "wikilink") | [1124年](../Page/1124年.md "wikilink") | [1125年](../Page/1125年.md "wikilink") |
| [干支](../Page/干支纪年.md "wikilink") | [辛丑](../Page/辛丑.md "wikilink")       | [壬寅](../Page/壬寅.md "wikilink")       | [癸卯](../Page/癸卯.md "wikilink")       | [甲辰](../Page/甲辰.md "wikilink")       | [乙巳](../Page/乙巳.md "wikilink")       |

## 參看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 其他政权使用的[保大年号](../Page/保大.md "wikilink")
  - 同期存在的其他政权年号
      - [宣和](../Page/宣和_\(宋徽宗\).md "wikilink")（1119年二月至1125年十二月）：[北宋](../Page/北宋.md "wikilink")—[宋徽宗趙佶之年號](../Page/宋徽宗.md "wikilink")
      - [永樂](../Page/永樂_\(方臘\).md "wikilink")（1120十一月至1121年四月）：北宋時期—[方臘之年號](../Page/方臘.md "wikilink")
      - [建福](../Page/建福_\(耶律淳\).md "wikilink")（1122年三月至六月）：[北遼](../Page/北遼.md "wikilink")—宣宗[耶律淳之年號](../Page/耶律淳.md "wikilink")
      - [德興](../Page/德興_\(蕭普賢女\).md "wikilink")（1122年六月至十二月）：北遼—德妃[蕭普賢女之年號](../Page/蕭普賢女.md "wikilink")
      - [神曆](../Page/神曆_\(耶律雅里\).md "wikilink")（1123年五月至十月）：北遼—梁王[耶律雅里之年號](../Page/耶律雅里.md "wikilink")
      - [天復](../Page/天復_\(回離保\).md "wikilink")（1123年正月至八月）：[奚](../Page/奚.md "wikilink")—[回離保之年號](../Page/回離保.md "wikilink")
      - [天嗣](../Page/天嗣.md "wikilink")（1123年）：奚—[蕭幹之年號](../Page/蕭幹.md "wikilink")
      - [延慶](../Page/延慶_\(耶律大石\).md "wikilink")（1124年二月至1133年十二月）：[西遼](../Page/西遼.md "wikilink")—德宗[耶律大石之年號](../Page/耶律大石.md "wikilink")
      - [天輔](../Page/天輔_\(完顏阿骨打\).md "wikilink")（1117年正月至1123年九月）：[金](../Page/金朝.md "wikilink")—[金太祖完顏阿骨打之年號](../Page/金太祖.md "wikilink")
      - [天會](../Page/天會_\(金太宗\).md "wikilink")（1123年九月至1137年十二月）：金—[金太宗吳乞買](../Page/金太宗.md "wikilink")、[金熙宗完颜亶之年號](../Page/金熙宗.md "wikilink")
      - [元德](../Page/元德.md "wikilink")（1119年正月至1127年三月）：[西夏](../Page/西夏.md "wikilink")—夏崇宗[李乾順之年號](../Page/李乾順.md "wikilink")
      - [天符睿武](../Page/天符睿武.md "wikilink")（1120年至1126年）：[李朝](../Page/李朝_\(越南\).md "wikilink")—[李乾德之年號](../Page/李乾德.md "wikilink")
      - [保安](../Page/保安_\(鳥羽天皇\).md "wikilink")（1120年四月十日至1124年四月三日）：日本[鳥羽天皇與](../Page/鳥羽天皇.md "wikilink")[崇德天皇年号](../Page/崇德天皇.md "wikilink")
      - [天治](../Page/天治.md "wikilink")（1124年四月三日至1126年一月二十二日）：日本崇德天皇年号

[Category:辽国年号](../Category/辽国年号.md "wikilink")
[Category:12世纪中国年号](../Category/12世纪中国年号.md "wikilink")
[Category:1120年代中国政治](../Category/1120年代中国政治.md "wikilink")