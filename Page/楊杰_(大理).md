\-{H|zh-hans:杰;zh-hant:杰;}-
[Yang_Jie2.jpg](https://zh.wikipedia.org/wiki/File:Yang_Jie2.jpg "fig:Yang_Jie2.jpg")
**楊-{杰}-**（），[字](../Page/表字.md "wikilink")**耿先**，[白族](../Page/白族.md "wikilink")，生於[雲南省](../Page/雲南省.md "wikilink")[大理縣](../Page/大理縣.md "wikilink")（今云南省[大理市](../Page/大理市.md "wikilink")），[國軍陸軍中將加上將銜](../Page/國軍.md "wikilink")，中国军事家。\[1\]\[2\]

## 生平

### 從滇-{系}-到国民軍

1900年（[光緒](../Page/光緒.md "wikilink")26年），楊-{杰}-入[大理数文書院](../Page/大理数文書院.md "wikilink")，5年後畢業。此後入[雲南陸軍講武堂](../Page/雲南陸軍講武堂.md "wikilink")，後轉入[保定北洋軍官学堂學習軍事](../Page/保定陸軍軍官学校.md "wikilink")。1911年（[宣統](../Page/宣統.md "wikilink")3年）春，赴[日本留学](../Page/日本.md "wikilink")，入[陸軍士官學校第](../Page/陸軍士官學校_\(日本\).md "wikilink")10期砲兵科。其間加入[中国同盟会](../Page/中国同盟会.md "wikilink")。同年[武昌起義爆發後](../Page/武昌起義.md "wikilink")，歸国任[武昌革命派的北伐聯軍總務部次長](../Page/武昌区.md "wikilink")。後來回到日本復学，1913年畢業。\[3\]\[4\]

歸国後，楊-{杰}-成爲[貴州都督](../Page/貴州省_\(中華民国\).md "wikilink")、[滇系領袖](../Page/滇系.md "wikilink")[唐繼堯下屬的貴州武威軍步兵第](../Page/唐繼堯.md "wikilink")10團團長。以後他歷任貴州騎兵第1團團長、滇軍第9旅旅長、[重慶衛戌司令官兼](../Page/重慶市.md "wikilink")[四川省政務廳廳長](../Page/四川省_\(中華民国\).md "wikilink")。1915年（[民国](../Page/民国紀元.md "wikilink")4年）12月[護国戰爭爆發](../Page/護国戰爭.md "wikilink")，任唐繼堯的護国軍第3軍第2梯團第5混成支隊長，後來任第4軍参謀長兼第1縦隊司令，進擊四川省東部。護国戰争結束後，獲授陸軍[中将](../Page/中将.md "wikilink")。\[5\]\[6\]

1917年（民国6年），楊-{杰}-一度在[北京政府任職](../Page/北京政府.md "wikilink")，後來再度回[昆明投靠](../Page/昆明市.md "wikilink")[唐繼堯](../Page/唐繼堯.md "wikilink")，此後歷任靖国聯軍第4軍参謀長、靖国軍中央軍總指揮兼[瀘州衛戌司令](../Page/瀘州市.md "wikilink")、靖国聯軍高級顧問、[雲南陸軍講武学校](../Page/雲南陸軍講武学校.md "wikilink")（講武堂的後身）教官。1920年，到日本留学[陸軍大学校](../Page/陸軍大学校.md "wikilink")，1923年畢業。翌年，歸国，在[奉系領袖](../Page/奉系.md "wikilink")[張作霖麾下任上校参謀](../Page/張作霖.md "wikilink")。同年冬[馮玉祥的](../Page/馮玉祥.md "wikilink")[国民軍建成](../Page/国民軍.md "wikilink")，楊-{杰}-任[孫岳所率的国民軍第](../Page/孫岳.md "wikilink")3軍的参謀長。1925年（民国14年）9月，任国民軍前敵指揮官兼[河南軍官教育團教育長](../Page/河南省_\(中華民国\).md "wikilink")。\[7\]\[8\]

### 蒋介石心腹軍人

1926年（民国15年）5月，楊-{杰}-任[程潜率領的](../Page/程潜.md "wikilink")[國民革命軍第六軍總参議](../Page/國民革命軍第六軍.md "wikilink")，參加[北伐](../Page/北伐.md "wikilink")，后來任第6軍第17師師長。翌年，他升任第6軍副軍長。同年第6軍攻[南京](../Page/南京市.md "wikilink")，他轉任總司令部淮南行營主任兼總預備隊指揮官。[南京事件後](../Page/南京事件_\(1927年\).md "wikilink")[程潜失勢](../Page/程潜.md "wikilink")，8月第6軍改組為第18軍，楊-{杰}-任該軍軍長。\[9\]\[10\]

[北伐成功後的](../Page/北伐.md "wikilink")1928年（民国17年）3月，他被提升為軍事委員会常務委員兼辦公庁主任，翌月他升任第1集團軍總司令部總参謀長。此後，楊-{杰}-成爲[蒋介石直属的心腹](../Page/蒋介石.md "wikilink")。10月，他任[北平憲兵学校校長](../Page/北京市.md "wikilink")。\[11\]\[12\]

1929年（民国18年）11月，楊-{杰}-任討伐[唐生智軍的總指揮](../Page/唐生智.md "wikilink")，討伐勝利後他轉任[洛陽行營主任兼第](../Page/洛陽市.md "wikilink")10軍軍長。1930年（民国19年）2月，他任長江要塞總司令，同年5月[中原大戰爆發](../Page/中原大戰.md "wikilink")，他歷任第2砲兵集團指揮官、總司令部總参謀長，為蒋介石的勝利作出了貢献。1931年（民国20年）11月，他當選[中国国民党第](../Page/中国国民党.md "wikilink")4屆中央執行委員。\[13\]\[14\]

1932年（民国21年）1月，楊-{杰}-任[陸軍大学校長](../Page/陸軍大学.md "wikilink")，後來校長由蒋介石親自擔任，楊-{杰}-轉任教育長。1933年，他任軍政委員会北平分会参謀長、第8軍團總指揮。9月，他任[欧洲軍事考察團團長](../Page/欧洲.md "wikilink")，赴欧洲各国考察。1934年秋歸国，他復任陸軍大学教育長。12月他兼任[国民政府軍事委員会参謀次長](../Page/国民政府.md "wikilink")、防空委員会主任。1935年（民国24年）4月，他升任陸軍中将，11月當選中國国民党第5屆中央執行委員。\[15\]\[16\]

### 反對国共内戰

[Yang_Jie1.jpg](https://zh.wikipedia.org/wiki/File:Yang_Jie1.jpg "fig:Yang_Jie1.jpg")
1937年（民国26年）8月，楊-{杰}-任實業考察團團長，訪問[蘇聯](../Page/蘇聯.md "wikilink")。10月，他被授与陸軍[上将銜](../Page/上将.md "wikilink")。1938年（民国27年）5月，他任駐[蘇聯](../Page/蘇聯.md "wikilink")[大使](../Page/大使.md "wikilink")，1940年4月卸任。歸国後，他任中央訓練團教官。1944年（民国33年）他任軍事代表團團長，再度訪問欧美各国。翌年5月，他當選中國国民党第6屆中央執行委員。\[17\]\[18\]

[抗日戰争結束後](../Page/抗日戰争.md "wikilink")，楊-{杰}-反感[国共内戰](../Page/第二次國共内戰.md "wikilink")，和蒋介石逐漸疏遠。1945年，他和[譚平山等人組織](../Page/譚平山.md "wikilink")[三民主義同志聯合會](../Page/中國國民黨革命委員會.md "wikilink")。此後他任国民政府戰略顧問委員会、[行憲国民大会代表](../Page/行憲国民大会.md "wikilink")。1948年他任[中国国民党革命委員会](../Page/中国国民党革命委員会.md "wikilink")（民革）中央執行委員。此後，他和滇系原領導人[龍雲以](../Page/龍雲.md "wikilink")[香港為據點活動](../Page/香港.md "wikilink")，推動了[雲南省政府主席](../Page/雲南省_\(中華民国\).md "wikilink")[盧漢投共](../Page/盧漢.md "wikilink")。

1949年（民国38年）6月，他應邀擔任[中国人民政治協商会議第一屆全体会議代表](../Page/中国人民政治協商会議第一屆全体会議代表.md "wikilink")。\[19\]\[20\]9月10日往[香港](../Page/香港.md "wikilink")。\[21\]9月19日，楊-{杰}-在[灣仔](../Page/灣仔.md "wikilink")[軒尼詩道](../Page/軒尼詩道.md "wikilink")302號4A室其友人[李焜的寓所遭到暗殺身亡](../Page/李焜.md "wikilink")，享年61岁。\[22\]1951年，[广州市公安局以殺害楊](../Page/广州市公安局.md "wikilink")-{杰}-的主凶的罪名逮捕了中國國民黨[軍統特务](../Page/軍統.md "wikilink")[陳家慶](../Page/陳家慶.md "wikilink")，对其进行公审后处决。\[23\]1982年6月5日，[中華人民共和国民政部追認楊](../Page/中華人民共和国民政部.md "wikilink")-{杰}-為「[革命烈士](../Page/革命烈士.md "wikilink")」。\[24\]\[25\]

## 著作

  - 国防新論
  - 軍事與國防
  - 国民軍事必讀
  - [蘇聯的國防政策](../Page/蘇聯.md "wikilink")
  - 戰争抉要
  - 總司令学
  - [孫子浅釋](../Page/孫子.md "wikilink")
  - [欧洲各国軍事考察報告](../Page/欧洲.md "wikilink")

## 参考文献

{{-}}

[Category:中華民國國防大學校長](../Category/中華民國國防大學校長.md "wikilink")
[Category:中華民國駐蘇聯大使](../Category/中華民國駐蘇聯大使.md "wikilink")
[Category:中华民国大陆时期遇刺身亡者](../Category/中华民国大陆时期遇刺身亡者.md "wikilink")
[Category:滇军将领](../Category/滇军将领.md "wikilink")
[Category:护国军将领](../Category/护国军将领.md "wikilink")
[Category:云南靖国军将领](../Category/云南靖国军将领.md "wikilink")
[Category:西北军将领](../Category/西北军将领.md "wikilink")
[Category:国民革命军将领](../Category/国民革命军将领.md "wikilink")
[Category:中華民國陸軍中將](../Category/中華民國陸軍中將.md "wikilink")
[Category:中国人民政治協商会議第一屆全体会議代表](../Category/中国人民政治協商会議第一屆全体会議代表.md "wikilink")
[Category:中华人民共和国烈士](../Category/中华人民共和国烈士.md "wikilink")
[Category:大理市人](../Category/大理市人.md "wikilink")
[Category:白族人](../Category/白族人.md "wikilink")
[Jie](../Category/楊姓.md "wikilink")
[Category:中国国民党第四届中央执行委员会委员](../Category/中国国民党第四届中央执行委员会委员.md "wikilink")

1.

2.

3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14.
15.
16.
17.
18.
19.
20.
21. [蔣經國](../Page/蔣經國.md "wikilink")<危急存亡之秋>，刊《風雨中的寧靜》，[台北](../Page/台北.md "wikilink")，[正中書局](../Page/正中書局.md "wikilink")，1988年，第244頁。

22. [赵子云](../Page/赵子云.md "wikilink")，杨-{杰}-将军遇刺之谜，《文史月刊》2004年第10期

23. [缅怀杨-{杰}-将军，中国国民党革命委员会云南省委员会，2011-09-07](http://www.ynmg.yn.gov.cn/ReadInfo.aspx?KindID=963b58e3-afdd-4693-8563-eb8149bcdd23&InfoID=ac74c3b8-16dc-471d-b28c-c77a4ccd58e0)


24.
25.