**錦綉花園**（）是位於[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")[元朗區](../Page/元朗區.md "wikilink")[大生圍](../Page/大生圍.md "wikilink")\[1\]的大型私人住宅屋苑，總面積為1,160,000[平方米](../Page/平方米.md "wikilink")，總共有5024間不超過3層的獨立屋式[洋房](../Page/洋房.md "wikilink")。園內有13種不同種類的房屋，每間面積由79至157[平方米](../Page/平方米.md "wikilink")。每間房屋有泊車位，前後均有一個花園。錦綉花園有一個15,000平方米的人工湖，亦有一個私人[污水處理廠](../Page/污水處理.md "wikilink")。處理完的清水用來灌溉園內植物、清洗街道和送到附近[米埔的一個養蝦場](../Page/米埔.md "wikilink")。錦綉花園有100條街道，總長度27[公里](../Page/公里.md "wikilink")。園內有3間[學校](../Page/學校.md "wikilink")、60個花園、一間[宣道會](../Page/宣道會.md "wikilink")[教堂](../Page/教堂.md "wikilink")（宣道會錦綉堂）、[超級市場](../Page/超級市場.md "wikilink")、[消防局](../Page/消防局.md "wikilink")、醫療中心、郵政局、[酒樓和櫃員機等](../Page/酒樓.md "wikilink")。

根據錦綉花園的官方網站及售樓說明書上的用字，錦綉花園的「綉」字採用[港台俗字](../Page/港台俗字.md "wikilink")「」，而非正字「」。

## 歷史

錦綉花園由[加拿大海外發展有限公司在](../Page/加拿大海外發展有限公司.md "wikilink")1976年開始建造。

## 圖集

Fairview Park Town Centre (shopping arcade).JPG|錦綉花園商場 Fairview Park
Town Centre (market).jpg|錦綉花園街市 FairviewParkPostOffice.JPG|錦綉花園郵政局
Fairview Park artificial lake.JPG|錦綉花園人工湖 Fairview Park vehicle entrance
and exit gate.jpg|錦綉花園車輛出入閘通道 Fairview Park bus terminus.jpg|錦綉花園穿梭巴士總站
HK Prince Edward Nathan Road Fairview Park Bus Stop near Mongkok Police
Station.JPG|錦綉花園的交通 Fairview Park shuttle bus (Hong
Kong).jpg|錦綉花園收費居民巴士服務 SY1237 NR93
08-10-2017.jpg|錦綉花園收費居民巴士服務 TS2412 NR93
08-10-2017.jpg|錦綉花園收費居民巴士服務（低地台巴士） Sales
Brochure of Fairview Park (Hong
Kong).jpg|錦綉花園的售樓說明書（[香港歷史博物館藏品](../Page/香港歷史博物館.md "wikilink")）

## 爭議

  - 2007年1月31日，一名12歲男童騎單車上學時，在元朗[錦綉花園大道遭一輛重型工程車輾斃](../Page/錦綉花園大道.md "wikilink")。錦綉花園居民指摘[政府逾億元興建](../Page/香港政府.md "wikilink")，並已建成多年的[錦壆路遲遲不通車](../Page/錦壆路.md "wikilink")，導致這條[私家路變成一條流量巨大的交通幹線](../Page/私家路.md "wikilink")。
  - 2007年2月2日，錦綉花園居民列席元朗[區議會的特別會議](../Page/區議會.md "wikilink")，[運輸署在會上宣布於](../Page/運輸署.md "wikilink")2月5日開放錦壆路，但仍容許逾7米長重型車輛駛入錦綉大道。錦綉花園居民對此非常不滿，高舉橫額抗議。部分區議員亦離場以示抗議。錦綉花園居民決定2月3日召開居民大會商討對策，不排除會以躺[馬路阻車駛入的激烈行動表達不滿](../Page/馬路.md "wikilink")。\[2\]
    錦綉花園居民表示如果運輸署決定禁止重型車輛駛入錦綉大道，他們會立即撤消訴訟。運輸署指事件涉及一宗法庭官司，要在法庭裁決後才能決定怎樣做。運輸署被指是因為害怕某些團體反對而遲遲未能下決定。運輸署助理署長[羅鳳萍在](../Page/羅鳳萍.md "wikilink")[香港電台千禧年代節目中對主持詢問哪一個團體反對時](../Page/香港電台.md "wikilink")，顯得支吾以對和模棱兩可，答案極不清澈。節目主持人[周融直斥運輸署](../Page/周融.md "wikilink")「[戇鳩](../Page/戇鳩.md "wikilink")」。\[3\]

### 衝突

  - 2007年2月5日，[運輸署上午](../Page/運輸署.md "wikilink")10:00開放錦壆路。根據[有線電視](../Page/有線電視.md "wikilink")[記者鄭映雪和陳意映在](../Page/記者.md "wikilink")12:00新聞報導表示，100名錦綉花園居民在錦綉大道單車意外現場舉起標語抗議。錦綉花園物業管理公司職員擺放鐵馬，封閉該條私家路的部份路面，阻止重型車輛轉彎。100名新田大生圍村民不滿，與錦綉花園居民對罵。新田鄉事委員會副主席馮根祥表示，他們依靠出入的無名小路，是他們唯一進出的主要道路。錦綉花園居民梁小姐表示，禁止重型車輛使用錦綉大道後，絲毫也不會影響大生圍村民普通私家車輛出入的。她表示，「我們以前用和平手法來爭取，但現在人善被人欺，政府是不理我們的。」\[4\]
    附近的貨櫃場公司負責人反對封路，指錦壆路連接貨櫃場的道路太窄，重型車輛無法使用，因此有需要使用錦綉大道，與管理公司職員一度發生衝突。\[5\]\[6\]錦綉花園物業管理公司總經理林覺暉和千禧年代節目主持人[周融質疑有什麽地方勢力可迫使](../Page/周融.md "wikilink")[香港政府不開放興建多年的錦壆路](../Page/香港政府.md "wikilink")。運輸署助理署長[羅鳳萍亦再次無法合理地解釋延遲開放錦壆路的原因](../Page/羅鳳萍.md "wikilink")。\[7\]
  - 2007年2月7日，錦綉花園居民登報章廣告（[明報A](../Page/明報.md "wikilink")17版），質疑元朗地政署沒有盡本份取締非法貨櫃場，並要求[地政總署再不要害怕](../Page/地政總署.md "wikilink")，盡力執法。\[8\]

## 交通

錦綉花園設有多條居民巴士前往各區，除了錦綉花園居民外，一般民眾亦可搭乘：\[9\]

<div class="NavFrame collapsed" style="color: black; background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px;">

  - 錦綉花園巴士總站

<!-- end list -->

  - [青山公路](../Page/青山公路.md "wikilink") - [潭尾段](../Page/潭尾.md "wikilink")

<!-- end list -->

  - [紅色小巴](../Page/紅色小巴.md "wikilink")

<!-- end list -->

  - 元朗至上水線\[10\]

</div>

</div>

另設村內免費穿梭巴士，約半小時一班。

## 名人住客

  - [伍經衡](../Page/伍經衡.md "wikilink")：香港著名補習天王、[遵理學校創辦人之一](../Page/遵理學校.md "wikilink")
  - [劉　江](../Page/劉江_\(香港\).md "wikilink")：香港著名男演員
  - [張國榮](../Page/張國榮.md "wikilink")：已故香港著名歌星及影星
  - [梁靜雯](../Page/梁靜雯_\(有線\).md "wikilink")：已故有線財經資訊台財經記者及主播
  - 劉嘉麟：已故香港名廚（有「少年廚神」之稱）
  - [程介南](../Page/程介南.md "wikilink")：前[民建聯立法會議員](../Page/民建聯.md "wikilink")
  - [吳耀明](../Page/吳耀明.md "wikilink")：海天堂龜苓膏創辦人和代言人（已搬遷）
  - [蔡少芬](../Page/蔡少芬.md "wikilink")：香港著名女演員
  - [張　晉](../Page/張晉.md "wikilink")：著名男演員
  - [黃芳雯](../Page/黃芳雯.md "wikilink")：有線電視娛樂台主播
  - [李思雅](../Page/李思雅.md "wikilink")：香港女藝人（已搬遷）
  - [毛舜筠](../Page/毛舜筠.md "wikilink")：香港著名女演員
  - [黎美嫻](../Page/黎美嫻.md "wikilink")：香港女演員
  - [林錦堂](../Page/林錦堂.md "wikilink")：已故粵劇名伶
  - [王書麒](../Page/王書麒.md "wikilink")：香港著名男藝人
  - [許秋怡](../Page/許秋怡.md "wikilink")：香港著名女藝人
  - [伍衛國](../Page/伍衛國.md "wikilink")：香港著名男演員
  - [王見秋](../Page/王見秋.md "wikilink")：前香港高等法院法官及前香港平等機會委員會主席
  - [謝　寧](../Page/謝寧_\(香港演員\).md "wikilink")：香港女演員、商人
  - [楊　羚](../Page/楊羚.md "wikilink")：香港女演員

## 鄰近

  - [加州花園](../Page/加州花園.md "wikilink")
  - [加州豪園](../Page/加州豪園.md "wikilink")

## 參考

<references/>

## 外部連結

  - [錦綉花園官方網站](http://cn.fairviewpark.hk/)
  - [錦綉花園地圖](http://maps.google.com.hk/maps?hl=zh-TW&ie=UTF8&ll=22.47933,114.048042&spn=0.016774,0.027595&z=15&brcurrent=3,0x3403f6cc438b273f:0x6e962e63266e3bec,1,0x3403f00a63639461:0xba0588d41c361655)
  - [2005年7月24日香港政府解釋不始用錦壆路的理由](http://www.info.gov.hk/gia/general/200507/24/07230162.htm)
  - [2007年1月31日一名12歲錦綉花園男童踏車返學，被重型貨車撞死。](https://web.archive.org/web/20071008033817/http://news.readmetro.com.hk/news.php?startDate=01022007&newscat=1&newsid=35111)
  - [錦繡花園三句鐘兩獨立屋遇竊-戶損失160萬首飾手表](https://hk.news.yahoo.com/錦繡花園三句鐘兩獨立屋遇竊-戶損失160萬首飾手表-065200007.html)

{{-}}

[Category:元朗區私人屋苑](../Category/元朗區私人屋苑.md "wikilink")

1.
2.  [明報新聞條目](http://www.mingpaonews.com/20070203/gsk1.htm)
3.  [香港電台千禧年代錄音](http://www.rthk.org.hk/asx/rthk/radio1/HK2000/20070201.asx)
4.  [有線電視 新聞報導](http://www.cabletv.com.hk)
5.  [香港電台新聞條目](http://www.rthk.org.hk/rthk/news/clocal/news.htm?clocal&20070205&55&376477)
6.  [香港電台新聞條目](http://www.rthk.org.hk/rthk/news/clocal/news.htm?clocal&20070205&55&376477)
7.  [香港電台千禧年代節目錄音](http://www.rthk.org.hk/asx/rthk/radio1/HK2000/20070205.asx)
8.  [香港電台千禧年代錄音](http://www.rthk.org.hk/asx/rthk/radio1/HK2000/20070207.asx)
9.  [錦綉花園 - 專巴時間表](http://www.fairviewpark.hk/bus_timetable.php)
10. [元朗水車館街　—　上水新發街](http://www.16seats.net/chi/rmb/r_n17.html)