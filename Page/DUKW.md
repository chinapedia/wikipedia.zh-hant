[DUKW_2.jpg](https://zh.wikipedia.org/wiki/File:DUKW_2.jpg "fig:DUKW_2.jpg")
**DUKW**（又名**DUCK**，鴨子）是[二戰時期](../Page/二戰.md "wikilink")[盟軍的](../Page/盟軍.md "wikilink")6輪[兩棲裝甲車](../Page/兩棲裝甲車.md "wikilink")，由[通用動力（GMC）製造](../Page/通用動力.md "wikilink")，二戰時用於運載物資及部隊穿越水陸及登陸作戰用途，二戰後改為運載遊客作旅遊用途。

## 簡介

[FLMM_-_DUKW_353_aft.jpg](https://zh.wikipedia.org/wiki/File:FLMM_-_DUKW_353_aft.jpg "fig:FLMM_-_DUKW_353_aft.jpg")
DUKW由的遊艇設計師Dennis Puleston（英國水兵）及Frank W.
Speir（麻省理工學院中尉）設計，[美國國防部科研委員會及](../Page/美國國防部科研委員會.md "wikilink")[美國科學研究與開發辦公室開發](../Page/美國科學研究與開發辦公室.md "wikilink")。DUKW的車身設計深受[福特GPA兩棲吉普車的影響](../Page/福特GPA兩棲吉普車.md "wikilink")，如同是GPA的放大，DUKW的車尾有水上行駛用的螺旋槳和船舵，其車身內部結構設計則來自CCKW-353卡車。

最初軍方拒絕裝備DUKW，但當[美國海岸警衛隊的一隻巡邏艇在](../Page/美國海岸警衛隊.md "wikilink")[麻薩諸塞州](../Page/麻薩諸塞州.md "wikilink")[普文斯鎮海灘擱淺後](../Page/普文斯鎮.md "wikilink")，試驗型DUKW被派至該區作搜救工作，當時正直大浪、下雨而且風速高達110公里每小時，DUKW仍可穩定航行，並救回7名海岸警衛隊隊員，令軍方的拒絕態度有所軟化，其後DUKW更橫渡[英吉利海峽以顯示其適航性](../Page/英吉利海峽.md "wikilink")。

*DUKW*的名字其實是軍隊中的[雙關語](../Page/雙關語.md "wikilink")，亦是[二戰時](../Page/二戰.md "wikilink")[美軍軍用車輛術語](../Page/美軍.md "wikilink")，其中**D**是設計年份“designed”（1942年），**U**解作“utility”（兩棲／多用途車輛），**K**解作[四輪驅動](../Page/四輪驅動.md "wikilink")，**W**解為兩條驅動後軸\[1\]。初期量產試驗的DUKW為六輪軍其於[GMC
ACKWX](../Page/GMC_ACKWX.md "wikilink")（[GMC
CCKW的COE版本](../Page/GMC_CCKW.md "wikilink")）[平頭車](../Page/平頭車.md "wikilink")（cab-over-engine）而設計，裝有防水車殼及螺旋槳，最終定型的量產版本改為基於[GMC
CCKW原版](../Page/GMC_CCKW.md "wikilink")，DUKW裝有4.416公升[GMC直-6引擎](../Page/GMC直-6引擎.md "wikilink")，總重7.5噸，水上時速為10公里，陸上為80公里，長9.3米，寬2.4米，高2.6米（連帆布頂篷），戰時制造了超過21000架。由於要減低在水上航行時的重量及提高穩定性，車身只裝有1/8及1/16寸的鋼鐵薄板，所以DUKW並非裝甲車輛，但配備高力量船底水泵以防薄車殼在水中穿孔時沉沒。

DUKW是第一種可在駕駛室內改變輪胎氣壓的車輛，輪胎可完全充氣以應付硬地路面，亦可降低輪胎氣壓以應付如沙灘的軟陸，其後大量軍用車輛亦裝備此功能。

## 服役歷史

[DUKW_1.jpg](https://zh.wikipedia.org/wiki/File:DUKW_1.jpg "fig:DUKW_1.jpg")
二戰時期DUKW被用於[太平洋戰爭](../Page/太平洋戰爭.md "wikilink")、[北非及](../Page/北非.md "wikilink")[D日](../Page/D日.md "wikilink")[諾曼第等的登陸任務](../Page/諾曼第.md "wikilink")，當時敵軍佔領了所有港口，DUKW在90日內為盟軍運送了18萬噸物資。

後來在二戰以後，DUKW也大量外銷給世界其他地區的美國盟邦，並在[韓戰與](../Page/韓戰.md "wikilink")[八二三砲戰中發揮了扭轉戰局的功用](../Page/八二三砲戰.md "wikilink")。

## 戰後用途

在40年代後期至50年代，陸軍的項目工程師正在開發一種“更大及更好”的兩棲車輛，名為‘超級鴨子（Super
Duck）’、‘公鴨（Drake）’及‘BARC
’。當時大量剩餘的DUKW被消防部門用作兩棲援救車輛，海岸警衛隊亦有裝備。

現在，大量DUKW仍然在使用，如建築用途、水陸觀光旅行團，以下是使用DUKW的城市：

  - [奧爾巴尼](../Page/奧爾巴尼.md "wikilink")
  - [巴爾的摩](../Page/巴爾的摩.md "wikilink")
  - [波士頓](../Page/波士頓.md "wikilink")
  - [布蘭森](../Page/布蘭森.md "wikilink")（[密蘇里州](../Page/密蘇里州.md "wikilink")）
  - [查特怒加](../Page/查特怒加.md "wikilink")
  - [都柏林](../Page/都柏林.md "wikilink")（[愛爾蘭首都](../Page/愛爾蘭.md "wikilink")）
  - [葡萄藤](../Page/葡萄藤.md "wikilink")（[德州](../Page/德州.md "wikilink")）（兩架，紫色及白色）
  - [哈利法克斯](../Page/哈利法克斯.md "wikilink")，[新斯科細亞](../Page/新斯科細亞.md "wikilink")，[加拿大](../Page/加拿大.md "wikilink")
  - [檀香山](../Page/檀香山.md "wikilink")
  - [凱契根](../Page/凱契根.md "wikilink")，[阿拉斯加](../Page/阿拉斯加.md "wikilink")
  - [利物浦](../Page/利物浦.md "wikilink")
  - [倫敦](../Page/倫敦.md "wikilink")
  - [孟菲斯](../Page/孟菲斯_\(田納西州\).md "wikilink")
  - [費城](../Page/費城.md "wikilink")
  - [匹茲堡](../Page/匹茲堡.md "wikilink")
  - [波特蘭（緬因州）](../Page/波特蘭_\(緬因州\).md "wikilink")
  - [波特蘭（奧勒岡州）](../Page/波特蘭_\(奧勒岡州\).md "wikilink")
  - [普洛威頓斯 (羅德島州)](../Page/普洛威頓斯_\(羅德島州\).md "wikilink")
  - [羅托路亞](../Page/羅托路亞.md "wikilink")
  - [西雅圖](../Page/西雅圖.md "wikilink")
  - [舊金山](../Page/舊金山.md "wikilink")（兩架，一架名叫‘北京鴨（Peking Duck）’）
  - [聖赫利爾](../Page/聖赫利爾.md "wikilink")
  - [喬治亞州石山公園](../Page/喬治亞州石山公園.md "wikilink")（[喬治亞州](../Page/喬治亞州.md "wikilink")）
  - [華盛頓](../Page/華盛頓哥倫比亞特區.md "wikilink")
  - [威斯康星](../Page/威斯康星.md "wikilink")
  - [高雄市](../Page/高雄市.md "wikilink")
    （[駁二特區](../Page/駁二特區.md "wikilink")，遊覽[愛河](../Page/愛河.md "wikilink")）
  - [悉尼](../Page/悉尼.md "wikilink")

旅遊用途的DUKW全部被重新塗漆，換上現代化柴油引擎及固定車頂，令其類似普通巴士以獲得公共車輛牌照及客船牌照。

[英國皇家海軍仍保留小量兩棲車輛在](../Page/英國皇家海軍.md "wikilink")[蘇格蘭作登陸訓練用途](../Page/蘇格蘭.md "wikilink")。

<File:london.duck.arp.jpg>|[倫敦的DUKW觀光旅行團](../Page/倫敦.md "wikilink")，留意車殼的前輛位置被改裝。
<File:DUKW.Thames.Canthusus.jpg>|[倫敦](../Page/倫敦.md "wikilink")[泰晤士河的DUKW](../Page/泰晤士河.md "wikilink")。
<File:DuckTourBoston.agr.jpg>|[波士頓的DUKW](../Page/波士頓.md "wikilink")。
<File:Ride> the Ducks 01A.jpg|[西雅圖的DUKW](../Page/西雅圖.md "wikilink")(Ride
the Ducks觀光旅行團)。

## 仿制

[POL_MWP_ZiL_485.JPG](https://zh.wikipedia.org/wiki/File:POL_MWP_ZiL_485.JPG "fig:POL_MWP_ZiL_485.JPG")
[蘇聯於](../Page/蘇聯.md "wikilink")1951年仿制DUKW，名為**BAV
485**，[越南人民海軍](../Page/越南人民海軍.md "wikilink")（Vietnam
People's Navy）在1970年代的亦有裝備。 \[2\] \[3\]

## 参考文献

<div class="references-small">

<references />

</div>

## 參見

  - [兩棲裝甲車](../Page/兩棲裝甲車.md "wikilink")
  - [水龜兩棲裝甲車](../Page/水龜兩棲裝甲車.md "wikilink")—英國相同用途的兩棲車輛

## 資料來源

  - —[美國陸軍戰車博物館](http://www.transchool.eustis.army.mil/museum/transportation%20museum/dukw.htm)

## 外部連結

  - —[斯帕克曼與史蒂芬公司（Sparkman &
    Stephens）主頁](http://www.sparkmanstephens.com/)

[Category:兩棲裝甲車](../Category/兩棲裝甲車.md "wikilink")
[Category:二戰軍用車輛](../Category/二戰軍用車輛.md "wikilink")

1.  [infantry.army.mil-Amphibious Vehicle
    DUKW 353](https://www.infantry.army.mil/museum/off_site_tour/warehouse04.htm)

2.  [autogallery.org.ru-ZIS-485,
    ZIL-485](http://www.autogallery.org.ru/m/bav.htm)
3.  *Concord，ARMOR OF THE VIETNAM WAR：(2)ASIAN FORCES* ISBN
    962-361-622-8