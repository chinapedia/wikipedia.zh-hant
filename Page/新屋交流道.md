**新屋交流道**為[台灣](../Page/中華民國.md "wikilink")[台66線的交流道](../Page/台66線.md "wikilink")，位於[桃園市](../Page/桃園市.md "wikilink")[新屋區](../Page/新屋區.md "wikilink")，指標為10k，在2002年2月6日通車，可通往[新屋區與](../Page/新屋區.md "wikilink")[中壢區](../Page/中壢區.md "wikilink")，往東可與[國道一號連接](../Page/中山高速公路.md "wikilink")。

[中壢交流道有時也被稱為新屋交流道](../Page/中壢交流道.md "wikilink")，因多數人以為距離[新屋區不遠](../Page/新屋區.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p><a href="https://zh.wikipedia.org/wiki/File:TW_PHW66.svg" title="fig:TW_PHW66.svg">TW_PHW66.svg</a></p></th>
<th></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:TW_CHW114.svg" title="fig:TW_CHW114.svg">TW_CHW114.svg</a></p></td>
<td><p><strong>中壢</strong> <strong>新屋</strong></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 鄰近交流道

|- \!colspan="5" style="background:\#F0F0F0;"|

<center>

示意圖

</center>

|- |align="center" colspan="5"|

[Category:2002年台灣建立的交流道](../Category/2002年台灣建立的交流道.md "wikilink")
[Category:新屋區](../Category/新屋區.md "wikilink")
[Category:桃園市交流道](../Category/桃園市交流道.md "wikilink")