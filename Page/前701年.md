<table>
<tbody>
<tr class="odd">
<td><p><small><strong><a href="../Page/世纪.md" title="wikilink">世纪</a>：</strong></small></p></td>
<td style="text-align: center;"><p><small> <a href="../Page/前9世纪.md" title="wikilink">前9世纪</a> | <strong><a href="../Page/前8世纪.md" title="wikilink">前8世纪</a></strong> | <a href="../Page/前7世纪.md" title="wikilink">前7世纪</a></small><br />
</p></td>
</tr>
<tr class="even">
<td><p><small><strong><a href="../Page/年代.md" title="wikilink">年代</a>：</strong></small></p></td>
<td style="text-align: center;"><p><small> <a href="../Page/前720年代.md" title="wikilink">前720年代</a> <a href="../Page/前710年代.md" title="wikilink">前710年代</a> | <em>' <a href="../Page/前700年代.md" title="wikilink">前700年代</a></em>' | <a href="../Page/前690年代.md" title="wikilink">前690年代</a> <a href="../Page/前680年代.md" title="wikilink">前680年代</a></small><br />
</p></td>
</tr>
<tr class="odd">
<td><p><small><strong><a href="../Page/年.md" title="wikilink">年份</a>：</strong></small></p></td>
<td style="text-align: center;"><p><small><a href="../Page/前706年.md" title="wikilink">前706年</a> <a href="../Page/前705年.md" title="wikilink">前705年</a> <a href="../Page/前704年.md" title="wikilink">前704年</a> <a href="../Page/前703年.md" title="wikilink">前703年</a> <a href="../Page/前702年.md" title="wikilink">前702年</a> | <strong>前701年</strong> | <a href="../Page/前700年.md" title="wikilink">前700年</a> <a href="../Page/前699年.md" title="wikilink">前699年</a> <a href="../Page/前698年.md" title="wikilink">前698年</a> <a href="../Page/前697年.md" title="wikilink">前697年</a> <a href="../Page/前696年.md" title="wikilink">前696年</a> </small><br />
</p></td>
</tr>
<tr class="even">
<td><p> </p></td>
<td style="text-align: center;"><p> </p></td>
</tr>
<tr class="odd">
<td><p><strong>中国传统纪年：</strong></p></td>
<td style="text-align: center;"><p><small><a href="../Page/周桓王.md" title="wikilink">周桓王十九年</a>；<a href="../Page/魯桓公.md" title="wikilink">魯桓公十一年</a>；<a href="../Page/秦出子.md" title="wikilink">秦出子三年</a>；<a href="../Page/陳厲公.md" title="wikilink">陳厲公六年</a>；<a href="../Page/蔡桓侯.md" title="wikilink">蔡桓侯十四年</a>；<a href="../Page/鄭莊公.md" title="wikilink">鄭莊公四十三年</a>；<a href="../Page/宋莊公.md" title="wikilink">宋莊公十年</a>；<a href="../Page/楚武王.md" title="wikilink">楚武王四十年</a>；<a href="../Page/齊僖公.md" title="wikilink">齊僖公三十年</a>；<a href="../Page/晉侯緡.md" title="wikilink">晉侯緡六年</a>；<a href="../Page/曲沃武公.md" title="wikilink">曲沃武公十五年</a>；<a href="../Page/燕宣侯.md" title="wikilink">燕宣侯十年</a>；<a href="../Page/衛宣公.md" title="wikilink">衛宣公十八年</a>；<a href="../Page/曹莊公.md" title="wikilink">曹莊公元年</a>；<a href="../Page/杞靖公.md" title="wikilink">杞靖公三年</a></p></td>
</tr>
</tbody>
</table>

-----

## 大事记

  - [蒲騷之戰](../Page/蒲騷之戰.md "wikilink")，[屈瑕盟貳](../Page/屈瑕.md "wikilink")、軫，與[鄖國戰於蒲騷](../Page/鄖國.md "wikilink")，勝之，得城下之盟而還。
  - [宋莊公脅](../Page/宋莊公.md "wikilink")[祭足立](../Page/祭足.md "wikilink")[鄭厲公突](../Page/鄭厲公.md "wikilink")，[鄭昭公奔衛](../Page/鄭昭公.md "wikilink")\[1\]

## 出生

## 逝世

  - [郑庄公](../Page/郑庄公.md "wikilink")

## 参考

[前701年](../Category/前701年.md "wikilink")
[年1](../Category/前700年代.md "wikilink")
[Category:前8世纪各年](../Category/前8世纪各年.md "wikilink")

1.