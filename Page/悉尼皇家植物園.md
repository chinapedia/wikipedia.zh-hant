[Sydney_Royal_Botanic_Gardens_01.jpg](https://zh.wikipedia.org/wiki/File:Sydney_Royal_Botanic_Gardens_01.jpg "fig:Sydney_Royal_Botanic_Gardens_01.jpg")
**悉尼皇家植物園**（Royal Botanic Gardens,
Sydney），位於[澳洲](../Page/澳洲.md "wikilink")[新南威爾斯州](../Page/新南威爾斯州.md "wikilink")，其西北端即[悉尼歌劇院](../Page/悉尼歌劇院.md "wikilink")。
雪梨皇家植物園是[悉尼的一個大型的植物公園](../Page/悉尼.md "wikilink")，由禁苑[基金會管理](../Page/基金會.md "wikilink")，全年開放，免費入場。

## 外部連線

  - [澳洲悉尼皇家植物園官方網址](http://www.rbgsyd.nsw.gov.au/)
    [位置地圖](http://www.rbgsyd.nsw.gov.au/royal_botanic_gardens/visitor_information/map_and_photos/feature_gardens)

[Category:悉尼公园](../Category/悉尼公园.md "wikilink")
[Category:悉尼建築物](../Category/悉尼建築物.md "wikilink")
[Category:悉尼旅遊景點](../Category/悉尼旅遊景點.md "wikilink")
[Category:澳大利亚植物园](../Category/澳大利亚植物园.md "wikilink")