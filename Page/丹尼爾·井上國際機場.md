**丹尼爾·井上國際機場**（**Daniel K. Inouye International
Airport**；[IATA](../Page/IATA.md "wikilink")：**HNL**，[ICAO](../Page/ICAO.md "wikilink")：**PHNL**），又称**檀香山国际机场**（），是一座位於[美國](../Page/美國.md "wikilink")[夏威夷](../Page/夏威夷.md "wikilink")[檀香山的民用機場](../Page/檀香山.md "wikilink")，是檀香山市與縣以及夏威夷州的航空門戶，為全美最繁忙機場的其中之一，2017年4月27日以夏威夷州已故参议员[丹尼爾·井上命名](../Page/丹尼爾·井上.md "wikilink")。\[1\]

丹尼爾·井上國際機場是[夏威夷航空的樞杻機場和](../Page/夏威夷航空.md "wikilink")[日本航空的重點城市](../Page/日本航空.md "wikilink")，也是前往美國、[亞洲及](../Page/亞洲.md "wikilink")[太平洋的樞杻](../Page/太平洋.md "wikilink")。

## 航空公司與航点

### 客運

[Air_routes_from_HNL.PNG](https://zh.wikipedia.org/wiki/File:Air_routes_from_HNL.PNG "fig:Air_routes_from_HNL.PNG")

### 貨運

## 事故

  - 1982年8月11日，一顆炸彈在
    **[泛美航空830號班機](../Page/泛美航空830號班機.md "wikilink")**上爆炸，1人死亡、15人受傷。飛機安全緊急降落。

<!-- end list -->

  - **[阿羅哈航空243號班機](../Page/阿羅哈航空243號班機.md "wikilink")**
  - **[聯合航空811號班機](../Page/聯合航空811號班機.md "wikilink")**

## 設施

机场设有四条主要[跑道](../Page/跑道.md "wikilink")。其中主跑道在8R/26L方向，又名Reef跑道，是世界上第一条完全在水上建造的主跑道。Reef跑道于1977年建成，它是[美国国家航空航天局](../Page/美国国家航空航天局.md "wikilink")[航天飞机的备降跑道](../Page/航天飞机.md "wikilink")，夏威夷[希卡姆空军基地也在本机场部署](../Page/希卡姆空军基地.md "wikilink")。

除四条铺成跑道之外，檀香山国际机场还有两条分别在8W/26W和4W/22W方向的水上跑道，供[水上飞机起降](../Page/水上飞机.md "wikilink")。

机场航站楼设有24小时医疗服务，餐厅，购物中心，以及一个拥有私人会议室的商务中心。机场还有各种短期和长期停车设施可供旅客选择。

进出机场的主要道路為尼米兹高速公路和利留卡拉妮女王高速公路，后者属于州级公路H-1。

## 未來計劃

在2006年3月24日，[夏威夷州州長批准了一項計劃](../Page/夏威夷.md "wikilink")，該劃是在12年內會動用23億美金為機場進行現代化工程的計劃，短期內主要會改善乘客服務及保安系統，包括了把客運大樓現代化、機長售票處、行李運輸的操作、機長跑道和停機坪、機場的空調系統、休息室設備、升降機、電梯、電力系統以及灑水系統。

同時，此計劃亦亦要確保跑道安全、機場周邊安全、雨水系統和機場消防系統的正常運作。此計劃的長遠目標是增加機場容量、提升機場的運作效率、增加登機橋、停機坪、行李輸送帶和停車場的車位數量。

## 參考

<div class="references-2column">

<references/>

</div>

## 外部鍵結

  - [檀香山國際機場](http://www.hawaii.gov/dot/airports/hnl/)

  -
  -
[Category:美國機場](../Category/美國機場.md "wikilink")
[Category:檀香山](../Category/檀香山.md "wikilink")
[Category:1927年启用的机场](../Category/1927年启用的机场.md "wikilink")
[Daniel K. Inouye](../Category/冠以人名的大洋洲機場.md "wikilink")

1.