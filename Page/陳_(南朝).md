**陈**（557年－589年）是[中国历史上](../Page/中国历史.md "wikilink")[南北朝时期](../Page/南北朝.md "wikilink")[南朝最后一个](../Page/南北朝.md "wikilink")[朝代](../Page/朝代.md "wikilink")，由[陈霸先代](../Page/陈霸先.md "wikilink")[梁所建立](../Page/南梁.md "wikilink")，以建康（今[南京](../Page/南京.md "wikilink")）为[首都](../Page/首都.md "wikilink")，[国号陈](../Page/国号.md "wikilink")。陈朝名称来自陈霸先即位前被封的陈公、陈王，但陈王的封号来源又有二说，一说认为来自陈王封地中排第一郡的陈留郡（见《陈书》），另一说认为陈霸先姓出于陈，陈为圣人舜后裔，故强迫梁帝封其为陈王（见胡三省《资治通鉴·陳纪一》下注）。

## 歷史

陈朝建立时，已经出现南朝转弱，北朝转强的局面。南朝已失去不少領土，到陳朝時，雖然北朝分裂為[西魏和](../Page/西魏.md "wikilink")[東魏](../Page/東魏.md "wikilink")（後被[北周及](../Page/北周.md "wikilink")[北齊取代](../Page/北齊.md "wikilink")），長江以北為北齊所佔，西南被北周所佔，只能靠長江天險維持南北對峙的局面。

陈朝刚建立时（557年）面临北朝的入侵，形势十分危急。陈朝开国皇帝[陈霸先带领军队一举击败敌军](../Page/陈霸先.md "wikilink")，形势有所好转。陳霸先於559年病逝，其侄[陳文帝陳蒨即位](../Page/陳文帝.md "wikilink")，先後消滅各地的割據勢力，大力革除前朝[蕭梁奢侈之風](../Page/梁_\(南朝\).md "wikilink")，使陳朝政治稍為安定。天康元年（566年），文帝死，遺詔太子[陳伯宗繼位](../Page/陳伯宗.md "wikilink")，568年被文帝弟[陳宣帝陈顼以陈霸先章太后的名义所廢](../Page/陳宣帝.md "wikilink")。宣帝繼續實行文帝時輕徭薄賦之策，使江南經濟逐漸恢復過來。

陈朝在經濟文化上比较发达，但在軍事上卻已難以與北方抗衡，即使北方已分裂為[北周及](../Page/北周.md "wikilink")[北齊](../Page/北齊.md "wikilink")。到了陳宣帝時試圖結好[北周](../Page/北周.md "wikilink")，夾擊[北齊](../Page/北齊.md "wikilink")。太建四年（572年），周陳互派使者。翌年的兩年內，陳宣帝以[吳明徹為征討大都督](../Page/吳明徹.md "wikilink")，統兵十萬北伐攻打北齊，佔領淮、陰、泗諸城。太建九年（577年），北周滅北齊。翌年，周陳在呂梁展開激戰，陳敗周勝，吳明徹被俘，淮南之地得而復失，江北州郡盡為北周所有，回復南北隔江對峙的被動局面。

太建十四年（582年），宣帝病死，太子[陳叔寶繼位](../Page/陳叔寶.md "wikilink")，是為後主。陳後主不問政事，荒於酒色，陳朝政治江河日下，後主亦自恃長江天險不思進取，被動固守。至於北朝，建立漢人政权[隋朝的](../Page/隋朝.md "wikilink")[隋文帝積極準備滅陳](../Page/隋文帝.md "wikilink")。陳朝最後被北方的[隋朝在](../Page/隋朝.md "wikilink")[南征之戰中所滅](../Page/隋滅陳之戰.md "wikilink")，南北統一。

## 文化

首都[建康為重要的文化](../Page/建康.md "wikilink")、政治、宗教中心，吸引[東南亞](../Page/東南亞.md "wikilink")、[印度的商人及僧侶前來](../Page/印度.md "wikilink")。南朝文化在梁朝时达到巅峰，经历了[侯景之乱的文化洗劫](../Page/侯景之乱.md "wikilink")，到陳朝时已经进入尾声。文学方面以[徐陵为文宗](../Page/徐陵.md "wikilink")，有文学集《[玉台新咏](../Page/玉台新咏.md "wikilink")》传世，其中最著名篇章《[孔雀东南飞](../Page/孔雀东南飞.md "wikilink")》。艺术方面以[姚最的评论](../Page/姚最.md "wikilink")《[续画品录](../Page/续画品录.md "wikilink")》影响最大。\[1\]

## 君主

### 君主列表

### 君主世系图

<center>

</center>

## 藩王

## 参考文献

### 引用

### 来源

  - 《[陳書](../Page/s:陳書.md "wikilink")》 [姚思廉](../Page/姚思廉.md "wikilink")
    著
  - 《[南史](../Page/s:南史.md "wikilink")》 [李延壽](../Page/李延壽.md "wikilink")
    著
  - 《[資治通鑑](../Page/s:資治通鑑.md "wikilink")》
    [司馬光](../Page/司馬光.md "wikilink") 著

{{-}}

[Category:南朝](../Category/南朝.md "wikilink")
[南朝陳](../Category/南朝陳.md "wikilink")
[Category:中国历史政权](../Category/中国历史政权.md "wikilink")
[Category:557年建立](../Category/557年建立.md "wikilink")
[Category:589年废除](../Category/589年废除.md "wikilink")

1.  [藝術與建築索引典—陳](http://db1x.sinica.edu.tw/caat/caat_rptcaatc.php?_op=?SUBJECT_ID:300018412)
    於2011 年4 月1 日查閱