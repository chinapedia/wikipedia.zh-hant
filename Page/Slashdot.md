**Slashdot**是一個[資訊科技](../Page/資訊科技.md "wikilink")[網站](../Page/網站.md "wikilink")，每天以[網誌的形式在主頁发表科技资讯](../Page/網誌.md "wikilink")，所有的新聞都源于网友投稿，经編輯筛选后發表，文中会附上源新闻的链接。该网站2014年alexa全球排名1631-{位}-，主要访问量前五名为印度（23.5%）、美国（27.8%）、英国、加拿大和巴基斯坦。网友可在每条新闻下评论，通常頭版的新聞會有几十個评论，較受歡迎的新聞會有上千的评论。而該站的新聞主題也包括電影和書籍的評論、訪-{}-問和[Ask
Slashdot](../Page/Ask_Slashdot.md "wikilink")。

該站的口號是"[News](../Page/新闻.md "wikilink") for
[nerds](../Page/nerd.md "wikilink"). Stuff that matters."
[Slashdot效应是指當成众多的Slashdot讀者访问某一在Slashdot公佈的網址時](../Page/Slashdot效应.md "wikilink")，該網址服务器不堪重负而癱瘓的情況。

"Slashdot"這個名字的字面含义是"/."——这也是该网站的标志，创始人有意选择了这个容易让人和[URL混淆的名称](../Page/统一资源定位符.md "wikilink")，因为http://slashdot.org/读起来就会成为：h-t-t-p-colon-slash-slash-slash-dot-dot-org。\[1\]

## 管理

1997年，Slashdot在[Rob
Malda手中初創](../Page/Rob_Malda.md "wikilink")，現在Slashdot則屬於[VA
Software底下的](../Page/VA_Software.md "wikilink")[Open Source Technology
Group所有](../Page/Open_Source_Technology_Group.md "wikilink")。此站主要主要營運者有Malda、Hemos、Bates（協助處理文章、保存回顧好文與銷售[廣告者](../Page/廣告.md "wikilink")），以及處理更多站務、張貼文章的Robin
"Roblimo" Miller（*請參考[Slashdot
的歷史](../Page/Slashdot_的歷史.md "wikilink")*）。

用以運作Slashdot的軟體被稱為[Slash或](../Page/Slash_\(网络日志系统\).md "wikilink")[slashcode](../Page/slashcode.md "wikilink")，採用[自由軟體基金會下的](../Page/自由軟體基金會.md "wikilink")[GNU通用公共許可證授權](../Page/GNU通用公共許可證.md "wikilink")。在很多其他網站，其客製化變體版本被廣泛用於處理網站論壇。

## 讀者

Slashdot的核心讀者群包括[Linux與](../Page/Linux.md "wikilink")[開放原始碼運動的狂熱份子](../Page/開放原始碼.md "wikilink")，顯然也包含支持Windows的讀者群。在一次Slashdot的投票中顯示有幾乎半數投票者使用[Microsoft
Windows](../Page/Microsoft_Windows.md "wikilink")[作業系統](../Page/作業系統.md "wikilink")，三分之一使用Linux,
[Mac OS
X使用者則大概佔了](../Page/Mac_OS_X.md "wikilink")10%左右。但最有意思的可能在於一定數目的跨平台使用者，也就是不只使用一種上述提到之作業系統的使用者。\[2\]

如同大多數網際網路上的相似活動，Slashdot上的投票亦著名地不可信賴。甚至蒐集來的瀏覽器類別資訊都較投票可靠，此結果顯示Microsoft的客戶遠小於一般網際網路上的量，而Linux與Mac使用者則高出許多。然而，很多Slashdot的訊息都與[Microsoft
Windows之遊戲](../Page/Microsoft_Windows.md "wikilink")、應用程式或是安全臭蟲有關。關於Slashdot為Linux導向的臆測則因著名的[比尔·盖茨](../Page/比尔·盖茨.md "wikilink")“[博格](../Page/博格人.md "wikilink")”圖示不脛而走。此外，在判斷Slashdot使用者作業系統結果傾斜方面，一個事實是很多Slashdot使用者在日常工作中接觸此網站，而在只有在家裡他們才使用Linux。

著名的"Slashdotters"（Slashdot者）包含演員[Wil
Wheaton](../Page/Wil_Wheaton.md "wikilink")（使用者代號"[CleverNickName](http://slashdot.org/~CleverNickName)"）,
[id
Software的技術指導](../Page/id_Software.md "wikilink")[約翰·卡馬克](../Page/約翰·卡馬克.md "wikilink")（使用者代號"[John
Carmack](http://slashdot.org/~John%20Carmack)"）,
[ReiserFS創始者](../Page/ReiserFS.md "wikilink")[Hans
Reiser](../Page/Hans_Reiser.md "wikilink")（使用者代號"[hansreiser](http://slashdot.org/~hansreiser)"）與[開放原始碼的宣揚者](../Page/開放原始碼.md "wikilink")[Bruce
Perens](../Page/Bruce_Perens.md "wikilink")（使用者代號"[Bruce
Perens](http://slashdot.org/~Bruce%20Perens)"）。另外一些值得注意的人士是參予[火星探勘計畫](../Page/火星.md "wikilink")（rover
exploration projects）的NASA工程師。

## 注釋

<references />

## 参见

  - [Solidot](../Page/Solidot.md "wikilink")

## 外部連結

  - [Slashdot](http://www.slashdot.org)

[Category:網誌](../Category/網誌.md "wikilink")
[Category:社群網站](../Category/社群網站.md "wikilink")
[Category:虚拟社区](../Category/虚拟社区.md "wikilink")
[Category:網站](../Category/網站.md "wikilink")
[Category:1997年建立的网站](../Category/1997年建立的网站.md "wikilink")

1.  [Slashdot FAQ: What does the name "Slashdot"
    mean?](http://slashdot.org/faq/slashmeta.shtml#sm150)
2.  [Slashdot Poll: My Main Computer
    Runs...](http://slashdot.org/pollBooth.pl?qid=848&aid=-1) (2002)