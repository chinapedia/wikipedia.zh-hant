[HK_2008_Lego_Vote_Ronny_Tong_Taxi_Ads_Civic_Party.JPG](https://zh.wikipedia.org/wiki/File:HK_2008_Lego_Vote_Ronny_Tong_Taxi_Ads_Civic_Party.JPG "fig:HK_2008_Lego_Vote_Ronny_Tong_Taxi_Ads_Civic_Party.JPG")期間，公民黨新界東團隊的宣傳\]\]
**曾國豐**（Tsang Kwok
Fung，），[香港人](../Page/香港.md "wikilink")，現任[公民黨執行委員會成員](../Page/公民黨_\(香港\).md "wikilink")（黨務發展），曾任[公民黨前總策劃幹事及經濟及公共財務政策支部主席](../Page/公民黨_\(香港\).md "wikilink")，[梁耀忠議員助理](../Page/梁耀忠.md "wikilink")，[前綫創會執行委員](../Page/前綫.md "wikilink")。在[2007年區議會選舉挑戰](../Page/2007年區議會選舉.md "wikilink")[大埔區](../Page/大埔區.md "wikilink")[大埔墟](../Page/大埔墟.md "wikilink")[區議員](../Page/區議員.md "wikilink")、[民建聯](../Page/民建聯.md "wikilink")[立法會議員](../Page/立法會.md "wikilink")[李國英](../Page/李國英.md "wikilink")，結果以189票之差落敗\[1\]。

曾國豐的[父親在區內開藥行當](../Page/父親.md "wikilink")[中醫](../Page/中醫.md "wikilink")，[母親活躍於區內活動](../Page/母親.md "wikilink")。而他在[香港浸會大學就讀時](../Page/香港浸會大學.md "wikilink")，就活躍於學運界，曾任[香港浸會大學學生會第二十六任會長](../Page/香港浸會大學學生會.md "wikilink")，畢業後曾於時任葵青區議員[梁耀忠辦事處當助理](../Page/梁耀忠.md "wikilink")，後到英國深造，並曾在[壹週刊及](../Page/壹週刊.md "wikilink")[東方報業集團當記者](../Page/東方報業集團.md "wikilink")。曾國豐每次落區拉票，都打著「年輕、[民主](../Page/民主.md "wikilink")、翻新舊區」旗幟，身邊總有大班親朋戚友、街坊街-{里}-現身助選。他直言，黨資源不及對手，其助選團都靠親戚朋友「搭膊頭」仗義襄助。亦有地區人士直言，論地區政績和知名度，「空降」的曾國豐的表現可謂「完全等於零」，全賴父母協調拉票。\[2\]

泛民曾國豐與民建聯李國英爭奪大埔墟議席相當激烈，香港各大新聞台均直播該區選情。

在獲選感言之中，民建聯的李國英表示，感謝曾國豐是一位高質素的對手\[3\]。

區選一役高票落選激發他再接再厲，他辭去公民黨總策劃幹事一職，專注地區工作，並且於[2008年香港立法會選舉夥拍上屆議員](../Page/2008年香港立法會選舉.md "wikilink")[湯家驊及另一個新人](../Page/湯家驊.md "wikilink")[曾健超代表公民黨出選新界東](../Page/曾健超.md "wikilink")，但最終僅得名單首位的湯家驊以第六的得票率取得議席。

曾國豐於2010年轉職[香港職工會聯盟屬下的國泰空中服務員工會擔任總幹事](../Page/香港職工會聯盟.md "wikilink")，續擔當公民黨執委至2012年。

2016年他繼續協助公民黨黨員楊岳橋邁向立法會。

## 参考文献

## 外部連結

  - [曾國豐的雅虎網誌](http://hk.myblog.yahoo.com/fung64-beta)
  - [區選監察網站](https://web.archive.org/web/20071124080542/http://www.1to99.org/map.php?code=290)

[Category:香港公民黨成員](../Category/香港公民黨成員.md "wikilink")
[Category:前綫前成員](../Category/前綫前成員.md "wikilink")
[K](../Category/曾姓.md "wikilink")
[Category:香港浸會大學校友](../Category/香港浸會大學校友.md "wikilink")
[Category:聖公會莫壽增會督中學校友](../Category/聖公會莫壽增會督中學校友.md "wikilink")

1.  <http://www.info.gov.hk/gia/general/200711/19/P200711190044.htm>
2.  [文匯報：忽然回歸參選
    曾國豐靠父母拉票](http://paper.wenweipo.com/2007/11/14/HK0711140037.htm)
3.  [YouTube -
    李國英當選大埔墟區議員，評論對手曾國豐](http://www.youtube.com/watch?v=SmvZ-lgJ0S4)