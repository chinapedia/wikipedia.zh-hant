**[天津市](../Page/天津市.md "wikilink")**是[中華人民共和國](../Page/中華人民共和國.md "wikilink")4個[直轄市之一](../Page/直轄市.md "wikilink")。

天津現轄16[市轄區](../Page/市轄區.md "wikilink")。

天津現有113街道、121鎮、5鄉、1民族鄉。

## 城區中心

### [和平區](../Page/和平區_\(天津市\).md "wikilink")：<small>6[街道](../Page/街道.md "wikilink")</small>

### [河東區](../Page/河東區_\(天津市\).md "wikilink")：<small>13[街道](../Page/街道.md "wikilink")</small>

### [河西區](../Page/河西區_\(天津市\).md "wikilink")：<small>13[街道](../Page/街道.md "wikilink")</small>

### [南開區](../Page/南開區.md "wikilink")：<small>12[街道](../Page/街道.md "wikilink")</small>

### [河北區](../Page/河北區.md "wikilink")：<small>10[街道](../Page/街道.md "wikilink")</small>

### [紅橋區](../Page/紅橋區.md "wikilink")：<small>10[街道](../Page/街道.md "wikilink")</small>

## 城區郊區

### [東麗區](../Page/東麗區.md "wikilink")：<small>9[街道](../Page/街道.md "wikilink")</small>

### [西青區](../Page/西青區.md "wikilink")：<small>2[街道](../Page/街道.md "wikilink")、7[鎮](../Page/鎮.md "wikilink")</small>

### [津南區](../Page/津南區.md "wikilink")：<small>8[鎮](../Page/鎮.md "wikilink")</small>

### [北辰區](../Page/北辰區.md "wikilink")：<small>4[街道](../Page/街道.md "wikilink")、9[鎮](../Page/鎮.md "wikilink")</small>

## 遠郊區

### [武清區](../Page/武清區.md "wikilink")：<small>6[街道](../Page/街道.md "wikilink")、19[鎮](../Page/鎮.md "wikilink")、5[鄉](../Page/鄉.md "wikilink")</small>

### [寶坻區](../Page/寶坻區.md "wikilink")：<small>3[街道](../Page/街道.md "wikilink")、21[鎮](../Page/鎮.md "wikilink")</small>

### [靜海區](../Page/靜海區.md "wikilink")：<small>16[鎮](../Page/鎮.md "wikilink")、2[鄉](../Page/鄉.md "wikilink")</small>

### [寧河區](../Page/寧河區.md "wikilink")：<small>11[鎮](../Page/鎮.md "wikilink")、3[鄉](../Page/鄉.md "wikilink")</small>

### [蓟州区](../Page/蓟州区.md "wikilink")：<small>1[街道](../Page/街道.md "wikilink")、25[鎮](../Page/鎮.md "wikilink")、1[民族鄉](../Page/民族鄉.md "wikilink")</small>

## 濱海區

### [濱海新區](../Page/濱海新區.md "wikilink")：<small>19[街道](../Page/街道.md "wikilink")、7[鎮](../Page/鎮.md "wikilink")</small>

## 沿革

天津現轄16[市轄區](../Page/市轄區.md "wikilink")。天津現有113街道、121鎮、5鄉、1民族鄉。

### 縣級行政區

|                                            |
| ------------------------------------------ |
| **天津市**                                    |
| 說明：本表為現今天津市所轄的縣份；以深灰色表示者，為曾經建置，後來廢除的市轄區及縣份 |
| 地級                                         |
| 天津市直轄                                      |
| [河東區](../Page/河東區.md "wikilink")           |
| [河西區](../Page/河西區.md "wikilink")           |
| [南開區](../Page/南開區.md "wikilink")           |
| [河北區](../Page/河北區.md "wikilink")           |
| [紅橋區](../Page/紅橋區.md "wikilink")           |
| [東麗區](../Page/東麗區.md "wikilink")           |
| [西青區](../Page/西青區.md "wikilink")           |
| [津南區](../Page/津南區.md "wikilink")           |
| [北辰區](../Page/北辰區.md "wikilink")           |
| [武清區](../Page/武清區.md "wikilink")           |
| [寶坻區](../Page/寶坻區.md "wikilink")           |
| [濱海新區](../Page/濱海新區.md "wikilink")         |
| [寧河區](../Page/寧河區.md "wikilink")           |
| [靜海區](../Page/靜海區.md "wikilink")           |
| [蓟州区](../Page/蓟州区.md "wikilink")           |

## 歷年統計

<table>
<caption>1949年至1999年天津市行政區劃統計列表</caption>
<tbody>
<tr class="odd">
<td></td>
<td style="text-align: center;"><p><a href="../Page/地級行政區.md" title="wikilink">地級行政區</a></p></td>
<td style="text-align: center;"><p><a href="../Page/縣級行政區.md" title="wikilink">縣級行政區</a></p></td>
</tr>
<tr class="even">
<td><p>小計</p></td>
<td style="text-align: center;"><p><a href="../Page/地級市.md" title="wikilink">地級市</a></p></td>
<td style="text-align: center;"><p><a href="../Page/地區_(中國行政區劃).md" title="wikilink">地區</a></p></td>
</tr>
<tr class="odd">
<td><p>1949年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>12</p></td>
</tr>
<tr class="even">
<td><p>1950年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>12</p></td>
</tr>
<tr class="odd">
<td><p>1951年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>12</p></td>
</tr>
<tr class="even">
<td><p>1952年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>10</p></td>
</tr>
<tr class="odd">
<td><p>1953年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>13</p></td>
</tr>
<tr class="even">
<td><p>1954年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>13</p></td>
</tr>
<tr class="odd">
<td><p>1955年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>13</p></td>
</tr>
<tr class="even">
<td><p>1956年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>13</p></td>
</tr>
<tr class="odd">
<td><p>1957年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>13</p></td>
</tr>
<tr class="even">
<td><p>1967年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>13</p></td>
</tr>
<tr class="odd">
<td><p>1968年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>13</p></td>
</tr>
<tr class="even">
<td><p>1969年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>12</p></td>
</tr>
<tr class="odd">
<td><p>1970年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>12</p></td>
</tr>
<tr class="even">
<td><p>1971年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>12</p></td>
</tr>
<tr class="odd">
<td><p>1972年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>12</p></td>
</tr>
<tr class="even">
<td><p>1973年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>17</p></td>
</tr>
<tr class="odd">
<td><p>1974年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>17</p></td>
</tr>
<tr class="even">
<td><p>1975年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>17</p></td>
</tr>
<tr class="odd">
<td><p>1976年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>17</p></td>
</tr>
<tr class="even">
<td><p>1977年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>17</p></td>
</tr>
<tr class="odd">
<td><p>1978年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>17</p></td>
</tr>
<tr class="even">
<td><p>1979年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>18</p></td>
</tr>
<tr class="odd">
<td><p>1980年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>18</p></td>
</tr>
<tr class="even">
<td><p>1981年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>18</p></td>
</tr>
<tr class="odd">
<td><p>1982年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>18</p></td>
</tr>
<tr class="even">
<td><p>1983年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>18</p></td>
</tr>
<tr class="odd">
<td><p>1984年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>18</p></td>
</tr>
<tr class="even">
<td><p>1985年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>18</p></td>
</tr>
<tr class="odd">
<td><p>1986年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>18</p></td>
</tr>
<tr class="even">
<td><p>1987年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>18</p></td>
</tr>
<tr class="odd">
<td><p>1988年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>18</p></td>
</tr>
<tr class="even">
<td><p>1989年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>18</p></td>
</tr>
<tr class="odd">
<td><p>1990年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>18</p></td>
</tr>
<tr class="even">
<td><p>1991年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>18</p></td>
</tr>
<tr class="odd">
<td><p>1992年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>18</p></td>
</tr>
<tr class="even">
<td><p>1993年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>18</p></td>
</tr>
<tr class="odd">
<td><p>1994年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>18</p></td>
</tr>
<tr class="even">
<td><p>1995年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>18</p></td>
</tr>
<tr class="odd">
<td><p>1996年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>18</p></td>
</tr>
<tr class="even">
<td><p>1997年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>18</p></td>
</tr>
<tr class="odd">
<td><p>1998年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>18</p></td>
</tr>
<tr class="even">
<td><p>1999年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>18</p></td>
</tr>
</tbody>
</table>

<table>
<caption>2000年至現今天津市各級行政區統計列表</caption>
<tbody>
<tr class="odd">
<td></td>
<td style="text-align: center;"><p><a href="../Page/地級行政區.md" title="wikilink">地級行政區</a></p></td>
<td style="text-align: center;"><p><a href="../Page/縣級行政區.md" title="wikilink">縣級行政區</a></p></td>
<td style="text-align: center;"><p><a href="../Page/鄉級行政區.md" title="wikilink">鄉級行政區</a></p></td>
</tr>
<tr class="even">
<td><p>小計</p></td>
<td style="text-align: center;"><p><a href="../Page/地級市.md" title="wikilink">地級市</a></p></td>
<td style="text-align: center;"><p><a href="../Page/地區_(中國行政區劃).md" title="wikilink">地區</a></p></td>
<td style="text-align: center;"><p><a href="../Page/自治州.md" title="wikilink">自治州</a></p></td>
</tr>
<tr class="odd">
<td><p>2000年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>18</p></td>
<td style="text-align: center;"><p>14</p></td>
</tr>
<tr class="even">
<td><p>2001年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>18</p></td>
<td style="text-align: center;"><p>15</p></td>
</tr>
<tr class="odd">
<td><p>2002年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>18</p></td>
<td style="text-align: center;"><p>15</p></td>
</tr>
<tr class="even">
<td><p>2003年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>18</p></td>
<td style="text-align: center;"><p>15</p></td>
</tr>
<tr class="odd">
<td><p>2004年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>18</p></td>
<td style="text-align: center;"><p>15</p></td>
</tr>
<tr class="even">
<td><p>2005年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>18</p></td>
<td style="text-align: center;"><p>15</p></td>
</tr>
<tr class="odd">
<td><p>2006年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>18</p></td>
<td style="text-align: center;"><p>15</p></td>
</tr>
<tr class="even">
<td><p>2007年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>18</p></td>
<td style="text-align: center;"><p>15</p></td>
</tr>
<tr class="odd">
<td><p>2008年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>18</p></td>
<td style="text-align: center;"><p>15</p></td>
</tr>
<tr class="even">
<td><p>2009年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>18</p></td>
<td style="text-align: center;"><p>15</p></td>
</tr>
<tr class="odd">
<td><p>2010年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p>13</p></td>
</tr>
<tr class="even">
<td><p>2011年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p>13</p></td>
</tr>
<tr class="odd">
<td><p>2012年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p>13</p></td>
</tr>
<tr class="even">
<td><p>2013年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p>13</p></td>
</tr>
<tr class="odd">
<td><p>2014年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p>13</p></td>
</tr>
<tr class="even">
<td><p>2015年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p>15</p></td>
</tr>
<tr class="odd">
<td><p>2016年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>16</p></td>
<td style="text-align: center;"><p>16</p></td>
</tr>
</tbody>
</table>

## 參考文獻

  - 《中華人民共和國行政區劃沿革地圖集》，中國地圖出版社

## 外部連結

  - [行政區劃網：歷年行政區劃調整](http://www.xzqh.org/html/list/10100.html)

## 參見

  - [天津市乡级以上行政区列表](../Page/天津市乡级以上行政区列表.md "wikilink")
  - [天津行政区划史](../Page/天津行政区划史.md "wikilink")

{{-}}

[\*](../Category/天津行政区划.md "wikilink")
[Category:天津列表](../Category/天津列表.md "wikilink")