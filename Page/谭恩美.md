**谭恩美**（，）是一名[美国华裔](../Page/美国华裔.md "wikilink")[女作家](../Page/女作家.md "wikilink")，她的作品多以表现母女关系和讲述[美国华人经历为主](../Page/美国华人.md "wikilink")。她的小说《[喜福会](../Page/喜福会.md "wikilink")》在1993年被[导演](../Page/导演.md "wikilink")[王颖改编为电影](../Page/王颖.md "wikilink")。\[1\]

谭恩美还撰写过其他一些小说：《》、《》、《》、《》和《。她还写过一本名为《事与愿违》（）的非虚构类叙事[散文集](../Page/散文.md "wikilink")。除此之外，谭恩美还写过两本[儿童文学读物](../Page/儿童文学.md "wikilink")：《月亮夫人》（）和《》。这两本书还被[美国公共电视台改编为](../Page/美国公共电视台.md "wikilink")[动画片](../Page/动画片.md "wikilink")。

尽管谭恩美在西方出版界获得了多项奖项，但是也因为“（她）合谋延续对种族的刻板印象和歪曲事实，以及她对中国传统文化细节描述的严重失实”而遭致了许多争议。\[2\]\[3\]
这与她在作品中频频否定中国文化有关，一些评论家和作者指责她的作品助长了西方人对中国人的刻板印象和偏见。\[4\]\[5\]\[6\]

## 个人生活

谭恩美生於[加利福尼亚州](../Page/加利福尼亚州.md "wikilink")[奧克蘭的華裔家庭](../Page/奧克蘭市.md "wikilink"),在家中排行老二。父親是一名電子工程師及浸信會牧師,為了逃離國共內戰而來到美國。他的父親以及哥哥在他十五歲的時候相繼因為腦瘤而過世。

隨後，他的母親帶著她與她的弟弟到了瑞士。在這期間，她知道了她母親在與她的父親結婚之前在中國有過另一段婚姻，以及她母親留在上海的四個孩子(一個兒子在嬰兒時期過世，另外還有三個女兒)，這件事促成了譚恩美的第一部小說《喜福會》，而在1987年，她與她的母親一同到中國旅行，並見到了他三個同母異父的姐妹。

## 创作主题

## 作品改编

## 评价争议

儘管她的作品贏得了多個獎項，但她也因為“在回憶起中國文化遺產細節時長期存在種族刻板印象和歪曲事實以及嚴重不准確”而受到嚴厲批評。\[7\]加利福尼亞大學伯克利分校的教授Sau-ling
Cynthia
Wong寫道，她的小說“似乎具有真實性的權威，但往往是美國出生的作家自己對中國事物的深刻理解的產物”。\[8\]另一位作家表示，她的工作受歡迎程度主要歸功於西方消費者“對於她的作品復制了刻板印象方面感到安慰”。\[9\]

在她的作品中經常出現對中國文化和中國男人的負面描述引起了人們的注意，一位學者甚至說她的小說故事情節“毫不掩飾地以最壞的方式描寫中國男人”。此點以及她的作品在描寫中國文化和歷史失真，引致幾位作家和學者指責她迎合西方人對中國人的流行想像。\[10\]

## 作品列表

## 奖项荣誉

## 参考资料

## 外部链接

  -
  - [谭恩美](https://book.douban.com/author/202437/)在[豆瓣上的资料](../Page/豆瓣.md "wikilink")

[Category:加州人](../Category/加州人.md "wikilink")
[Category:美國散文家](../Category/美國散文家.md "wikilink")
[Category:美国华人作家](../Category/美国华人作家.md "wikilink")
[Category:美國女性作家](../Category/美國女性作家.md "wikilink")
[Category:廣東裔美國人](../Category/廣東裔美國人.md "wikilink")
[Category:美國浸禮宗教徒](../Category/美國浸禮宗教徒.md "wikilink")
[Category:美国短篇小说家](../Category/美国短篇小说家.md "wikilink")
[Category:20世紀美國小說家](../Category/20世紀美國小說家.md "wikilink")
[Category:21世紀美國小說家](../Category/21世紀美國小說家.md "wikilink")
[Category:美國兒童文學作家](../Category/美國兒童文學作家.md "wikilink")
[Category:聖荷西州立大學校友](../Category/聖荷西州立大學校友.md "wikilink")

1.

2.  Lee, Jonathan (2015). *Chinese Americans: The History and Culture of
    a People: The History and Culture of a People*. p. 334.

3.  Wong, Sau-ling Cynthia (1995). *Sugar Sisterhood: Situating the Amy
    Tan Phenomenon*. p. 55.

4.  Lee, Lily (2003). *中國婦女傳記詞典: The Twentieth Century, 1912-2000*. p.
    503.

5.  Yin, Xiao-huang (2000). *Chinese American Literature Since the
    1850s*. p. 235.

6.  Huntley, E. D. (2001). *Maxine Hong Kingston: A Critical Companion*.
    p. 58.

7.
8.
9.
10.