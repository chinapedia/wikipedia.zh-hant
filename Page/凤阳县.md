**凤阳县**位于[中华人民共和国](../Page/中华人民共和国.md "wikilink")[安徽省中东部](../Page/安徽省.md "wikilink")，是[滁州市下辖的一个县](../Page/滁州市.md "wikilink")。

## 地名由来

明朝洪武七年（公元1374年），凤阳人[朱元璋取](../Page/朱元璋.md "wikilink")“丹凤朝阳”之意，赐名家乡“凤阳”。\[1\]

## 历史

[商代钟离子国](../Page/商代.md "wikilink")，位于现凤阳县临淮关东板桥镇古城村，典故吴楚之争“钟离之畔”来源于此。[西汉高祖四年](../Page/西汉.md "wikilink")（203年）设钟离县；刘邦封[英布为淮南王](../Page/英布.md "wikilink")，改[九江郡为](../Page/九江郡.md "wikilink")[淮南国](../Page/淮南国.md "wikilink")，钟离县为淮南国的领地。

明[洪武二年](../Page/洪武.md "wikilink")（1369年）改为中立县，属于[中书省南京](../Page/南直隶.md "wikilink")，同时在濠州西南凤凰山南麓建中都；洪武三年（1370年）因县城北临淮河，改为临淮县，这是临淮地名之始；洪武六年（1373年）临濠府改为中立府，洪武七年（1374年）中立府改名[凤阳府](../Page/凤阳府.md "wikilink")，同时割临淮县的太平、清洛、广德、永丰4乡设凤阳县；因县城位于凤凰山之阳，所以得名。

明崇禎八年（1635年），[高迎祥](../Page/高迎祥.md "wikilink")、[李自成](../Page/李自成.md "wikilink")、[張獻忠等人擬議打擊](../Page/張獻忠.md "wikilink")[大明王朝的氣勢](../Page/大明王朝.md "wikilink")，率農民軍分路合擊，直取中都鳳陽，以不到十日的時間，便攻陷了鳳陽。農民軍入城後，斬殺了六十名宦官、四千名官兵，並擒獲鳳陽知府[顏容暄](../Page/顏容暄.md "wikilink")，並以杖殺之，留守[朱國相力抗農民軍](../Page/朱國相.md "wikilink")，最後仍不敵而自殺。農民軍搜刮城內戰利品，釋放囚犯，火燒[明皇陵](../Page/明皇陵.md "wikilink")，並焚燬[明太祖](../Page/明太祖.md "wikilink")[朱元璋出家的](../Page/朱元璋.md "wikilink")[皇覺寺](../Page/皇覺寺.md "wikilink")。

1978年末，因长期吃不饱饭，凤阳县小岗村18位农民私自定下了实行“大包干”的“生死状”，秘密将村内土地分开承包，搞私有化生产。此举在当地取得成效后，应顺应时局变化，得到国家认可和推广，并被称作“[家庭联产承包责任制](../Page/家庭联产承包责任制.md "wikilink")”，小岗村也被作为典型示范村大力宣传。实行“大包干”后，大部分人实现了快速脱贫，但因为缺乏进一步发展的有利条件，当地经济一度发展缓慢，至今未能成为富裕之乡，与沿海地带的发展差距明显。\[2\]\[3\]\[4\]中央现已很少宣传小岗村的发展现状。

## 地理

### 区位

凤阳县位于北纬32°37′-33°03′、东经117°19′-117°57′之间，地处淮河中游南岸。地形北低南高，北部是沿淮冲积平原，中部是倾降平缓的岗丘，南部是山区；平均海拔15-17米，最高山峰为狼窝山，海拔340.3米。境内石英岩和石灰岩资源丰富，特别是石英岩矿已初步探明储量约100亿吨，储量和品位均位居华东之首、全国前列。\[5\]

### 气候

凤阳县气候属[亚热带季风气候](../Page/亚热带季风气候.md "wikilink")，年平均气温14.9℃，年降雨量904.4毫米，年蒸发量1609.7毫米。

### 自然灾害

凤阳地处江淮分水岭地区，[旱](../Page/旱灾.md "wikilink")、[涝灾害十分频繁](../Page/洪灾.md "wikilink")，历史上就有“十年九灾”之说。自1919年有水文记载以来的89年中，该县共发生较为严重的洪灾36次，旱灾42次，有的年份旱涝灾害交替或同时发生。\[6\]

## 经济

凤阳在历史上一直以贫穷知名，20世纪六七十年代更是几乎家家有过乞讨经历。\[7\]凤阳经济目前仍以农业为主。\[8\]\[9\]\[10\]20世纪80年代至21世纪头几年，凤阳仍主要依靠农业支撑经济，经济发展较慢，但开始了当地工业的初步发展。\[11\]2005年至2010年期间，从农业向工业和服务业的产业转型加快，而且重工业发展明显快于轻工业，经济有了明显提升，工人年平均工资从2005年的11531元上升至2010年的27017元(需考虑物价上浮的因素)。连农民年收入也从很低的2658元上升至5760元。\[12\]2015年，当地约有近万贫困户。\[13\]

当地发展经济仍有一定盲目性。自当年摁手印的大包干带头人现在办起“[农家乐](../Page/农家乐.md "wikilink")”后，当地许多农民也纷纷跟风办起了农家乐。结果因市场需求快速饱和，未达到最佳预期效果。\[14\]\[15\]

## 行政区划

凤阳县下辖14个镇和1个乡：

  - 镇：[府城镇](../Page/府城镇_\(凤阳县\).md "wikilink")，[临淮关镇](../Page/临淮关镇.md "wikilink")，[武店镇](../Page/武店镇.md "wikilink")，[总铺镇](../Page/总铺镇.md "wikilink")，[板桥镇](../Page/板桥镇_\(凤阳县\).md "wikilink")，[大庙镇](../Page/大庙镇_\(凤阳县\).md "wikilink")，[西泉镇](../Page/西泉镇.md "wikilink")，[殷涧镇](../Page/殷涧镇.md "wikilink")，[红心镇](../Page/红心镇.md "wikilink")，[刘府镇](../Page/刘府镇.md "wikilink")，[大溪河镇](../Page/大溪河镇.md "wikilink")，[小溪河镇](../Page/小溪河镇.md "wikilink")，[黄泥铺镇](../Page/黄泥铺镇.md "wikilink")。
  - 乡：[黄湾乡](../Page/黄湾乡_\(凤阳县\).md "wikilink")。

## 交通

  - 公路：[合徐高速](../Page/合徐高速.md "wikilink")，[宁洛高速](../Page/宁洛高速.md "wikilink")，[蚌淮高速](../Page/蚌淮高速.md "wikilink")
  - 铁路：[京沪铁路](../Page/京沪铁路.md "wikilink")，[淮南铁路](../Page/淮南铁路.md "wikilink")，[京沪高铁](../Page/京沪高铁.md "wikilink")，合蚌铁路客运
  - 水运：凤阳港——“千里淮河第一港”\[16\]

## 旅游

古凤阳八景：濠梁观鱼，九华屏障，谯楼归市，龙兴晚钟，浮桥烟锁，钓台春涨，明陵风雨，韭山仙境。

凤阳是历史文化名城，近年来旅游业发展迅速。目前已有国家4A级景区2个、3A级景区3个。其中大包干纪念馆被列为国家红色旅游经典景区，凤阳韭山地质公园申报为国家级地质公园。\[17\]

  - [明皇陵](../Page/明皇陵.md "wikilink")
  - [龙兴寺](../Page/龙兴寺_\(凤阳\).md "wikilink")
  - [明中都遗址](../Page/明中都遗址.md "wikilink")
  - [小岗村](../Page/小岗村.md "wikilink")

## 文化

[凤阳花鼓又称](../Page/凤阳花鼓.md "wikilink")“花鼓小锣”、“双条鼓”，是凤阳传统的民间艺术。“凤阳花鼓”最早是当地人外出乞讨谋生敲出来名的。\[18\]

## 名人

  - [朱元璋](../Page/朱元璋.md "wikilink")（1328年－1398年）——[明朝开国皇帝](../Page/明朝.md "wikilink")。
  - [徐达](../Page/徐达.md "wikilink")(1332～1385)——字天德，濠州钟离（今安徽凤阳）人。
  - [汤和](../Page/汤和.md "wikilink")(1326—1395)——字鼎臣，濠州钟离(今安徽凤阳)人。
  - [韩德彩](../Page/韩德彩.md "wikilink")——[中國人民解放軍將領](../Page/中國人民解放軍.md "wikilink")、[中國人民解放軍空軍中將](../Page/中國人民解放軍中將.md "wikilink")，曾任[南京軍區空軍副司令員](../Page/南京軍區.md "wikilink")。

[File:Hongwu1.jpg|朱元璋](File:Hongwu1.jpg%7C朱元璋) <File:Xu> Da.jpg|徐达

## 参考文献

## 参见

  - [凤阳府](../Page/凤阳府.md "wikilink"),
    [明中都遗址](../Page/明中都遗址.md "wikilink"),
    [明皇陵](../Page/明皇陵.md "wikilink")
  - [家庭联产承包责任制](../Page/家庭联产承包责任制.md "wikilink")

{{-}}     [滁州](../Page/category:安徽县份.md "wikilink")

[凤阳县](../Category/凤阳县.md "wikilink")
[县](../Category/滁州区县市.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14.
15.
16.
17.
18.