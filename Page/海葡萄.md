**海葡萄**（[学名](../Page/学名.md "wikilink")：**）是一種[灌木植物](../Page/灌木.md "wikilink")，主要分佈在[美洲及](../Page/美洲.md "wikilink")[加勒比海的海灘](../Page/加勒比海.md "wikilink")，包括[佛羅里達州及](../Page/佛羅里達州.md "wikilink")[百慕達](../Page/百慕達.md "wikilink")。\[1\]它最高可達8米，但大部份標本都只有2米多少許。它的[葉子很大](../Page/葉子.md "wikilink")、圓形及革質，[直徑達](../Page/直徑.md "wikilink")25厘米，主脈呈紅色，衰老後的葉子會整塊轉為紅色。樹皮平滑及呈黃色。夏末會產出紫色的[果實](../Page/果實.md "wikilink")，直徑約有2厘米，像[葡萄般是一蔟的](../Page/葡萄.md "wikilink")。

海葡萄不能抵禦寒冷的天氣，較能忍耐陰暗的天色，而對[鹽則有高度的抵抗力](../Page/鹽.md "wikilink")，故它們有時被種植來穩定海灘邊。它們亦會作為觀賞植物。其果實會用來製造[果漿或直接食用](../Page/果漿.md "wikilink")。

## 分類

[SeaGrapeLeaf.jpg](https://zh.wikipedia.org/wiki/File:SeaGrapeLeaf.jpg "fig:SeaGrapeLeaf.jpg")
海葡萄最初的[學名是由](../Page/學名.md "wikilink")[漢斯·斯隆](../Page/漢斯·斯隆.md "wikilink")（Hans
Sloane）於1696年所取的，名為*Prunus maritima
racemosa*，意即「成蔟葡萄的海岸李」，是[海岸李的](../Page/海岸李.md "wikilink")[亞種](../Page/亞種.md "wikilink")。[普拉肯内特](../Page/普拉肯内特.md "wikilink")（Leonard
Plukenet）則將之命名為*Uvifera
littorea*，意即「海岸邊的[葡萄](../Page/葡萄.md "wikilink")」。\[2\]兩者都反映出[歐洲對葡萄的觀念](../Page/歐洲.md "wikilink")。[原住民則看它們為大型的](../Page/原住民.md "wikilink")[桑](../Page/桑树.md "wikilink")。

[卡爾·林奈](../Page/卡爾·林奈.md "wikilink")（Carolus
Linnaeus）《[植物種誌](../Page/植物種誌.md "wikilink")》的最初版本中，將海葡萄分類在[蓼屬中](../Page/蓼屬.md "wikilink")，並命名為*Polygonum
uvifera*，附錄著他未曾見過這種植物。後來[帕特里克·布朗](../Page/帕特里克·布朗.md "wikilink")（Patrick
Browne）為它定立了[海葡萄屬](../Page/海葡萄屬.md "wikilink")。林奈在其第二版中修改了海葡萄的分類，定名為*Coccolobus
uvifera*。\[3\]

## 參考

[uvifera](../Category/海葡萄屬.md "wikilink")

1.
2.
3.