[Brain-1.jpg](https://zh.wikipedia.org/wiki/File:Brain-1.jpg "fig:Brain-1.jpg")
[Cerebrum_animation_small.gif](https://zh.wikipedia.org/wiki/File:Cerebrum_animation_small.gif "fig:Cerebrum_animation_small.gif")
[Lobes_of_the_brain_NL.svg](https://zh.wikipedia.org/wiki/File:Lobes_of_the_brain_NL.svg "fig:Lobes_of_the_brain_NL.svg")

**大脑**（），由[端脑与](../Page/端脑.md "wikilink")[间脑组成](../Page/间脑.md "wikilink")。在[醫學及](../Page/醫學.md "wikilink")[解剖学上](../Page/解剖学.md "wikilink")，多用大脑一词來指代[端脑](../Page/端脑.md "wikilink")。

端脑有左右两个[大脑半球](../Page/大脑半球.md "wikilink")（端脑半球）。将两个半球隔开的是称为大脑纵隔的沟壑，两个半球除了[胼胝體相连以外完全左右分开](../Page/胼胝體.md "wikilink")。半球表面布满**脑沟**，沟与沟之间所夹细长的部分称为**脑回**。脑沟并非是在脑的成长过程中随意形成，什么形态出现在何处都完全有规律（其深度和弯曲度因人稍有差异）。每一条脑沟在解剖学上都有专有名称。脑沟与脑回的形态基本左右半球对称，是对脑进行分叶和定位的重要标志。有关大脑两半球功能单侧化的研究表明，大多数人的言语活动中枢在大脑左半球。比较重要的脑沟有[外侧沟起于半球下面](../Page/外侧沟.md "wikilink")，行向后上方，至上外侧面；[中央沟起于半球上绿中点稍后方](../Page/中央沟.md "wikilink")，斜向前下方，下端与外侧沟隔一脑回，上端延伸至半球内侧面；位于半球内侧面后部，自下向上。在外侧沟上方和中央沟以前的部分为额叶；外侧沟以下的部分为颞叶；枕叶位于半球后部，其前界在内侧面为顶枕沟，在上外侧面的界限是自顶枕沟至枕前切迹（在枕叶后端前方约4cm处）的连线；顶叶为外侧沟上方、中央沟后方、枕叶以前的部分；岛叶呈三角形岛状，位于外侧沟深面，被额、顶、颞叶所掩盖，与其他部分不同布满细小的浅沟（非脑沟）。

左右大脑半球有各自的称为[侧脑室的腔隙](../Page/侧脑室.md "wikilink")。侧脑室与间脑的[第三脑室](../Page/第三脑室.md "wikilink")，以及[小脑和延脑及脑桥之间的](../Page/小脑.md "wikilink")[第四脑室之间有孔道连通](../Page/第四脑室.md "wikilink")。脑室中的脉络丛产生脑的液体称为[脑脊液](../Page/脑脊液.md "wikilink")。脑脊液在各脑室与蛛网膜下腔之间循环，如果脑室的通道阻塞，脑室中的脑脊液积多，将形成[脑积水](../Page/脑积水.md "wikilink")。

广义的大脑的[脑神经有](../Page/脑神经.md "wikilink")，端脑出发的[嗅神经](../Page/嗅神经.md "wikilink")，间脑出发的[视神经](../Page/视神经.md "wikilink")。

大脑的断面分为[白质与](../Page/白质.md "wikilink")[灰白质](../Page/灰白质.md "wikilink")。端脑的灰白质是指表层的数厘米厚的称为[大脑皮质的一层](../Page/大脑皮质.md "wikilink")，大脑皮质是[神经细胞聚集的部分](../Page/神经细胞.md "wikilink")，具有六层的构造，含有复杂的回路是思考等活动的中枢。相对大脑皮质白质又称为大脑髓质。

间脑由[丘脑与](../Page/丘脑.md "wikilink")[下丘脑构成](../Page/下丘脑.md "wikilink")。丘脑与大脑皮质，[脑干](../Page/脑干.md "wikilink")，小脑，脊髓等联络，负责感觉的中继，控制运动等。下丘脑与保持身体恒常性，控制自律神经系统，感情等相关。
大腦的神經細胞只要在3分鐘內得不到氧氣，人就會失去知覺；而5、6分鐘後仍缺氧，神經細胞便會陸續死去。

## 参考文献

## 外部連結

  - [head and neck (頭頸部) - 大腦溝與迴 (cerebral gyru and
    sulcus)](http://smallcollation.blogspot.com/2013/03/head-and-neck-cerebral-gyru-and-sulcus.html)

## 参见

  - [脑化指数](../Page/脑化指数.md "wikilink")

{{-}}

[Category:中枢神经系统](../Category/中枢神经系统.md "wikilink")
[大脑](../Category/大脑.md "wikilink")