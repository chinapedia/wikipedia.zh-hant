**巡查**是[日本警察官的](../Page/日本警察.md "wikilink")[階級之一](../Page/階級.md "wikilink")，為組織之中的最基層階級，，同台灣警察官等的[警員位階](../Page/警員.md "wikilink")。相當於[自衛隊下士官的階級](../Page/自衛隊.md "wikilink")\[1\]。

## 概要

巡查負責[派出所與日常巡邏等工作](../Page/派出所.md "wikilink")，[警察署的署員亦進行搜查等事務](../Page/警察署.md "wikilink")。通常經[都道府縣的警察官採用考試所採用者](../Page/都道府縣.md "wikilink")，採用之後大多擔任巡查（有職務經驗者等則多採用為「特別捜查官」）。這時已經為警察官身分，除了大規模事件如[日本航空123號班機墜落事故](../Page/日本航空123號班機.md "wikilink")、[淺間山莊事件等](../Page/淺間山莊事件.md "wikilink")，會因後方支援的需要而出動，初入職者很少機會參與現場活動。

## 晉昇

巡查要晉昇至[巡查部長](../Page/巡查部長.md "wikilink")，通常必須接受一年一度的昇任[考試](../Page/考試.md "wikilink")。亦有由署長推薦、通過選拔考試及因立大功而被特別昇任者，但不常出現。

## 補足

  - [司法警察職員](../Page/司法警察職員.md "wikilink")　司法警察職員中、有分為[司法巡查與](../Page/司法巡查.md "wikilink")[司法警察員](../Page/司法警察員.md "wikilink")：（[刑事訴訟法第](../Page/刑事訴訟法.md "wikilink")39条）
      - [司法警察員](../Page/司法警察員.md "wikilink")　原則上是巡查部長或以上階級的警察官（例外後述）。　
      - [司法巡査](../Page/司法巡査.md "wikilink")　巡查階級的警察官。
      - 例外：[離島等](../Page/離島.md "wikilink")[駐在所員與専務課所屬捜査員等](../Page/駐在所.md "wikilink")，部份巡查被付與司法警察員的權限。

## 相等職級

  - 香港警察[員佐級的警員](../Page/香港警察員佐級.md "wikilink")、高級警員。
  - [臺灣警察基層警佐的](../Page/中華民國警察.md "wikilink")
    **[一線三星](../Page/基層警員.md "wikilink")**

## 有名之虛構人物

  - 秋本治作品《[烏龍派出所](../Page/烏龍派出所.md "wikilink")》：[秋本麗子](../Page/秋本麗子.md "wikilink")、[中川圭一](../Page/中川圭一.md "wikilink")、[寺井洋一](../Page/寺井洋一.md "wikilink")
  - 藤島康介作品《[逮捕令](../Page/逮捕令_\(漫画\).md "wikilink")》：[小早川美幸](../Page/小早川美幸.md "wikilink")、[辻本夏實](../Page/辻本夏實.md "wikilink")、[二階堂賴子](../Page/二階堂賴子.md "wikilink")、[葵雙葉](../Page/葵雙葉.md "wikilink")
  - 《[相棒](../Page/相棒.md "wikilink")》中特命系成员，前[法务省刑事局特别企划调查室室长](../Page/法务省.md "wikilink")[冠城亘](../Page/冠城亘.md "wikilink")

[Category:日本警衔](../Category/日本警衔.md "wikilink")

1.