[Lhu-pic1.jpg](https://zh.wikipedia.org/wiki/File:Lhu-pic1.jpg "fig:Lhu-pic1.jpg")
[Lunghwa_University_of_Science_and_Technology2018Mars.jpg](https://zh.wikipedia.org/wiki/File:Lunghwa_University_of_Science_and_Technology2018Mars.jpg "fig:Lunghwa_University_of_Science_and_Technology2018Mars.jpg")
**龍華科技大學**，簡稱**龍華科大**（[英語](../Page/英語.md "wikilink")：**Lunghwa
University of Science and
Technology**，[縮寫](../Page/縮寫.md "wikilink")：**LHU**）是一所位於[桃園市](../Page/桃園市.md "wikilink")[龜山區的](../Page/龜山區.md "wikilink")[私立](../Page/私立大學.md "wikilink")[科技大學](../Page/科技大學_\(學制\).md "wikilink")，前身為**龍華工業專科學校**，同時是[北臺灣第一所改名科技大學之私立技專校院](../Page/北臺灣.md "wikilink")。
[Lhu-pic2.jpg](https://zh.wikipedia.org/wiki/File:Lhu-pic2.jpg "fig:Lhu-pic2.jpg")

## 簡介

該校於1969年由前太平洋電線電纜公司董事長孫法民及其夫人孫陳淑娟所創辦，初期設立時以工業技藝為主要專科，後來才逐漸擴大至其他領域，該校位於桃園市龜山區的迴龍地區
。

## 沿革

[龍華科技大學法民大樓_20120320.jpg](https://zh.wikipedia.org/wiki/File:龍華科技大學法民大樓_20120320.jpg "fig:龍華科技大學法民大樓_20120320.jpg")

  - 1969年11月22日奉准立案成立「**私立龍華工業技藝[專科學校](../Page/專科學校.md "wikilink")**」，設立二年制專科部[機械](../Page/機械.md "wikilink")[技術](../Page/技術.md "wikilink")、[電工技術](../Page/電工.md "wikilink")、工業[電子](../Page/電子.md "wikilink")、電化技術四科，即目前[機械工程](../Page/機械工程.md "wikilink")、電機工程、[電子工程](../Page/電子工程.md "wikilink")、化工與材料工程四系前身。
  - 1971年 更名「**私立龍華工業專科學校**」。
  - 1972年 增設工業管理科，即工業管理系前身。
  - 1983年 增設夜間部。
  - 1988年
    更名「**私立龍華工商專科學校**」，增設[國際貿易科](../Page/國際貿易.md "wikilink")，即國際企業系前身。
  - 1990年 增設電子[資料處理科](../Page/資料.md "wikilink")，即資訊管理系前身。
  - 1998年
    奉准改制「**龍華[技術學院](../Page/技術學院.md "wikilink")**」，並附設專科[進修學校](../Page/進修學校.md "wikilink")。設立四年制大學部。
  - 1999年
    增設[財務](../Page/財務.md "wikilink")[金融系](../Page/金融.md "wikilink")、[企業管理系及](../Page/企業管理.md "wikilink")[應用外語系](../Page/應用外語系.md "wikilink")。
  - 2001年
    8月1日奉准改名「**龍華科技大學**」，設立[工程](../Page/工程.md "wikilink")[學院](../Page/學院.md "wikilink")、[管理學院](../Page/管理學院.md "wikilink")、[電資學院及](../Page/電資學院.md "wikilink")[人文學院](../Page/人文.md "wikilink")，增設[資訊網路工程系](../Page/資訊工程系.md "wikilink")、[多媒體與](../Page/多媒體.md "wikilink")[遊戲發展科學系](../Page/遊戲.md "wikilink")\[1\]、[商學與管理](../Page/商學.md "wikilink")[研究所及機械工程系碩士班](../Page/研究所.md "wikilink")。
  - 2002年 增設[電機工程系碩士班](../Page/電機工程系.md "wikilink")、電子工程系碩士班。
  - 2004年
    [化學工程系更名化工與](../Page/化學工程系.md "wikilink")[材料工程系](../Page/材料工程.md "wikilink")，機械工程系碩士班更名工程技術研究所。
  - 2005年 配合多媒體與遊戲發展科學系改隸人文學院，人文學院更名人文暨科學學院。
  - 2006年 增設商學與管理研究所在職專班。
  - 2007年
    國際貿易系更名[國際](../Page/國際.md "wikilink")[企業系](../Page/企業.md "wikilink")。停止招收二技日間部及二技[進修學院](../Page/進修學校.md "wikilink")，招收二技[在職專班](../Page/在職專班.md "wikilink")。
  - 2008年
    增設工程技術研究所在職專班。招收四技在職專班。台北[推廣](../Page/推廣.md "wikilink")[教育中心遷移至迴龍校本部](../Page/教育.md "wikilink")。
  - 2009年 增設[資訊管理系碩士班](../Page/資訊管理.md "wikilink")、電子工程系四技產學專班及四技在職專班。
  - 2010年 增設工業管理系四技、機械工程科二專產學專班、數位遊戲設計學士後學位學程、手機行動數位平台遊戲設計與應用學士後學位學程。
  - 2011年 停招數位遊戲設計學士後學位學程、手機行動數位平台遊戲設計與應用學士後學位學程。
  - 2012年
    裁撤電資學院，原有系所併入工程學院。人文暨科學學院更名人文暨設計學院。增設[文化創意與數位媒體設計系](../Page/文化創意產業.md "wikilink")、[觀光](../Page/觀光.md "wikilink")[休閒系](../Page/休閒.md "wikilink")，招收四技日間部、進修部及在職專班各一班。增設機械工程系碩士班及企業管理系碩士班。
  - 2014年 增設多媒體與遊戲發展科學系碩士班、化工與材料工程系碩士班（化材組）。
  - 2015年
    增設化工與材料工程系碩士班電漿科技組、應用外語系國際觀光與會展碩士班、[五專部化工與材料工程科](../Page/五專.md "wikilink")。
  - 2016年 增設資訊網路工程系碩士班。
  - 2018年 增設五專部機械工程科、電機工程科及電子工程科。

### 校徽

| 校徽                                                                                                                                | 年代          | 校徽說明                                      |
| --------------------------------------------------------------------------------------------------------------------------------- | ----------- | ----------------------------------------- |
| [Lunghwa_University_Logo.jpg](https://zh.wikipedia.org/wiki/File:Lunghwa_University_Logo.jpg "fig:Lunghwa_University_Logo.jpg") | 2001年至2011年 | 原龍華科大校徽，中央龍之圖騰於工專時期啟用，周圍中英文校名隨改名及改制而有所變動。 |
| [LHU_name.png](https://zh.wikipedia.org/wiki/File:LHU_name.png "fig:LHU_name.png")                                               | 2011年\~     | 100學年度啟用的學校象徵圖案，左側為英文校名縮寫"LHU"的立體字樣。      |

## 校歌

美哉龍華，大哉龍華，背海依山，一望無涯。
研新技，創新猷，建教合作協力奮發。
一技在身值連城，雙手萬能報國家。
建設三民主義的模範省，建設富強康樂的大中華。
美哉龍華，大哉龍華，龍華是我家，我愛他。
美哉龍華，大哉龍華，龍華是我家，我愛他。

## 校園環境

[LHU_Humanities_and_Design_College.jpg](https://zh.wikipedia.org/wiki/File:LHU_Humanities_and_Design_College.jpg "fig:LHU_Humanities_and_Design_College.jpg")

### 校地

龍華校園全境位於[省道](../Page/台灣省道.md "wikilink")[台一線旁的山坡上](../Page/台一線.md "wikilink")，校門前的龍華街則沿著[山坡地由下而上延伸](../Page/山坡地.md "wikilink")，進入龍華校內直到停車場及學生活動中心後方，將校園分隔為東西兩側。另外龍華街也由於[坡度陡峭而常被學生稱為](../Page/坡度.md "wikilink")「龍華坡」，因此也是校內最經常發生交通意外的地點，而被[教官列為宣導行車安全的重點](../Page/軍訓教官_\(中華民國\).md "wikilink")。

### 周邊環境

龍華校園雖然位處龜山市郊，而便捷的交通提供了相當方便的通勤選擇，[萬壽路兩側皆設有](../Page/萬壽路_\(桃園市\).md "wikilink")[台北市市區公車與](../Page/臺北市市區公車.md "wikilink")[新北市市區公車站牌且有眾多路線公車通往](../Page/新北市公車.md "wikilink")[台北](../Page/台北市.md "wikilink")、[新莊](../Page/新莊區.md "wikilink")、[板橋](../Page/板橋區_\(新北市\).md "wikilink")、[樹林及](../Page/樹林區.md "wikilink")[桃園等地](../Page/桃園區.md "wikilink")。另外[台北捷運](../Page/台北捷運.md "wikilink")[新莊線](../Page/新莊線.md "wikilink")[迴龍站已於](../Page/迴龍站.md "wikilink")2013年6月底通車，距離校區僅一公里（3分鐘車程），未來[桃園捷運](../Page/桃園捷運.md "wikilink")[棕線](../Page/桃園捷運棕線.md "wikilink")[BRH07站將規劃興建於龍華街口](../Page/龍華科大站.md "wikilink")，目前已有[先導公車行駛](../Page/桃園捷運棕線先導公車.md "wikilink")。鄰近的中小學有[光啟高中](../Page/光啟高中.md "wikilink")、[迴龍國中小及三多國小](../Page/迴龍國中小.md "wikilink")，公共設施則有[樂生療養院](../Page/樂生療養院.md "wikilink")、[新北市立圖書館三多圖書閱覽室以及](../Page/新北市立圖書館.md "wikilink")[桃園市立圖書館迴龍分館](../Page/桃園市立圖書館.md "wikilink")，為學生或社團參與服務學習活動及自修的良好環境。

### 校內建築

  - **法民大樓**（N棟）：行政大樓，於2002年落成啟用，以紀念創辦人孫法民，多數行政處室位於此棟大樓，與圖書館相連。內有**國際會議中心**，橫跨3、4樓兩層，**藝文中心**位於8樓，1樓為第一停車場。因建於山坡地，位於接近教學大樓的5樓出入口為常用大門。
  - **圖書館大樓**（L棟）：於1998年完工啟用，為龍華第三代圖書館所在地，提供師生完整資訊與研究的空間，2018年12月改名孫陳淑娟圖書館。
  - **商科大樓**（K棟）：地上7層，管理學院各系所（資管系所除外）辦公室及院長室皆位於本棟大樓，大門位於三樓，階梯廣場對面。樓層由下而上依序為工管系、管理學院、國企系、企管系所、財金系等院系所辦公室所在。
  - **綜合大樓**（F棟）：地上7層，因外形宛如城堡而常被稱為「城堡」，於1991年完工。樓層由下而上依序為電機系所、工程學院、資網系所、資管系所等院系所辦公室所在。頂樓為舊體育場(已不存在)。
  - **第一教學大樓**（S棟）：地上6層，2\~7樓為共同科目的上課場所，於2001年完工。一樓由左至右分別為保健室、學務處衛保組、進修部主任辦公室及會議室，二樓部分教室為資管系研究生教室。三樓S311為微縮教室。
  - **第二教學大樓**（T棟）：地上6層，4\~7樓為共同科目的上課場所，於2001年完工。一樓由左至右分別為進修部各組辦公室及軍訓室，二樓為證照中心與推廣中心辦公室及教室，三樓為觀光系辦公室及專業教室。五六樓及六七樓各有一間階梯教室。
  - **階梯廣場**：位於商科大樓前方與第一、第二教學大樓中間，上方陸橋連接兩棟大樓的三樓。
  - **警衛室**：校門所在。
  - **電子工程系**（C棟）
  - **機械工程系**
  - **U棟**

## 歷任校長

| 校名                                      | 姓名                               | 任職時間        | 備註 |
| --------------------------------------- | -------------------------------- | ----------- | -- |
| 私立龍華工業技藝專科學校                            | [李振雲](../Page/李振雲.md "wikilink") | 1969年—1972年 |    |
| 私立龍華工業專科學校                              | 1972年—1984年                      |             |    |
| [孫道亨](../Page/孫道亨.md "wikilink")        | 1984年—1987年                      |             |    |
| [李華昌](../Page/李華昌.md "wikilink")        | 1987年—1988年                      |             |    |
| 私立龍華工商專科學校                              | 1988年—1989年                      |             |    |
| [鄭進山](../Page/鄭進山.md "wikilink")        | 1989年—1997年                      |             |    |
| 龍華技術學院                                  | 1997年—1999年                      | 改制後首任       |    |
| [施　河](../Page/施河.md "wikilink")         | 1999年—2001年                      |             |    |
| 龍華科技大學                                  | 2001年—2002年                      | 改大後首任       |    |
| [張文雄](../Page/張文雄_\(臺灣\).md "wikilink") | 2002年—2005年                      |             |    |
| [嚴文方](../Page/嚴文方.md "wikilink")        | 2005年—2008年                      |             |    |
| [黃深勳](../Page/黃深勳.md "wikilink")        | 2008年—2009年                      | 代理          |    |
| [葛自祥](../Page/葛自祥.md "wikilink")        | 2009年—至今                         | 現任          |    |
|                                         |                                  |             |    |

## 教學單位

目前設有工程、管理、人文暨設計等三學院，合計大學部14系、五專部1科、碩士班9所。

| <span style="font-size:14pt;"> **工程學院** | 機械工程科系暨碩士班 | 化工與材料工程科系暨碩士班 | 電機工程科系暨碩士班 | 電子工程科系暨碩士班 |
| --------------------------------------- | ---------- | ------------- | ---------- | ---------- |
| 資訊網路工程系暨碩士班                             |            |               |            |            |

| <span style="font-size:14pt;"> **管理學院** | 國際企業系 | 財務金融系 | 企業管理系暨碩士班 | 資訊管理系暨碩士班 |
| --------------------------------------- | ----- | ----- | --------- | --------- |
| 工業管理系                                   |       |       |           |           |

| <span style="font-size:14pt;"> **人文暨設計學院** | 應用外語系暨國際觀光與會展碩士班 | 多媒體與遊戲發展科學系暨碩士班 | 文化創意與數位媒體設計系 | 觀光休閒系 |
| ------------------------------------------ | ---------------- | --------------- | ------------ | ----- |

## 教學研究中心

| <span style="font-size:14pt;"> 工程學院 | 電漿應用技術研發中心 | 貴重儀器中心 |
| ----------------------------------- | ---------- | ------ |

<table>
<thead>
<tr class="header">
<th><p><span style="font-size:14pt;"> 管理學院</p></th>
<th><p>國際認證中心</p></th>
<th><p>企業管理系</p>
<dl>
<dt></dt>
<dd>專案管理中心
</dd>
</dl></th>
</tr>
</thead>
<tbody>
</tbody>
</table>

<table>
<thead>
<tr class="header">
<th><p><span style="font-size:14pt;"> 人文暨設計學院</p></th>
<th><p>通識教育中心</p>
<dl>
<dt></dt>
<dd>職場倫理教學研究中心
</dd>
</dl></th>
<th><p>語言中心</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>華語文中心</p></td>
<td><p>藝文中心</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>數位內容多媒體技術研發中心</p></td>
<td><p>外語自學中心</p></td>
<td></td>
</tr>
</tbody>
</table>

## 校園生活

### 傳統季節活動

#### 龍華我最神系列

  - 創意樂舞校歌比賽
  - 校慶嘉年華比賽
  - 十四系接龍成果發表

### 學生自治組織

龍華科技大學學生會為全校最高自治組織，前身為「學生活動中心」，於民國94年本校由龍華工專改為龍華科技大學，學生活動中心遂改為「學生會」，其組織底下分為行政部門-行政中心與立法部門-學生議會，歸類於學校自治性社團。

### 學生社團

<table>
<tbody>
<tr class="odd">
<td><h4 id="自治性">自治性</h4>
<ul>
<li>學生會</li>
<li>畢聯會</li>
</ul>
<h4 id="學藝性">學藝性</h4>
<ul>
<li>龍華資訊開源社</li>
<li>晨風攝影社</li>
<li>傳奇手語社</li>
<li>藝術欣賞社</li>
<li>動漫社</li>
<li>棋藝社</li>
<li>造型氣球社</li>
<li>真理研究社</li>
<li>龍騰獅陣社</li>
<li>陶瓷工藝研習社</li>
<li>創意銀飾社</li>
<li>英語國際演講社</li>
<li>影音研究社</li>
<li>創新創業社</li>
</ul>
<h4 id="服務性">服務性</h4>
<ul>
<li>心園工作坊</li>
<li>崇德志工社</li>
<li>翔耘志工社</li>
<li>藍海社</li>
</ul>
<h4 id="體育性">體育性</h4>
<ul>
<li>劍道社</li>
<li>跆拳社</li>
<li>棒壘社</li>
<li>桌球社</li>
<li>羽球社</li>
<li>籃球社</li>
<li>撞球社</li>
<li>翔鷹棒球社</li>
<li>單車研習社</li>
<li>運動塑身社</li>
<li>民俗藝陣社</li>
<li>龍韻鼓陣社</li>
</ul></td>
<td><h4 id="康樂性">康樂性</h4>
<ul>
<li>羅浮群</li>
<li>龍崗琴韻吉他社</li>
<li>龍耀室內樂團</li>
<li>柳絮世界舞蹈社</li>
<li>熱舞社</li>
<li>山水康輔社</li>
<li>熱門音樂社</li>
<li>魔術社</li>
</ul>
<h4 id="聯誼性">聯誼性</h4>
<ul>
<li>福音社</li>
<li>親善大使團</li>
<li>國粹社</li>
</ul></td>
</tr>
</tbody>
</table>



## 全球姐妹學校

以下是龍華科大的全球姊妹校列表：\[2\]

<table style="width:115%;">
<colgroup>
<col style="width: 90%" />
<col style="width: 25%" />
</colgroup>
<thead>
<tr class="header">
<th><p>國家</p></th>
<th><p>學校名稱</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><ul>
<li><a href="../Page/河內.md" title="wikilink">河內經營管理大學</a></li>
<li>河內阮必成經濟大學</li>
<li><a href="../Page/河內百科工藝大學.md" title="wikilink">河內百科工藝大學</a></li>
<li><a href="../Page/胡志明市大學.md" title="wikilink">胡志明市大學</a></li>
<li><a href="../Page/河內國家大學.md" title="wikilink">河內國家大學</a></li>
<li><a href="../Page/西貢大學.md" title="wikilink">西貢大學</a></li>
<li><a href="../Page/峴港大學.md" title="wikilink">峴港大學</a></li>
<li>胡志明市技術教育大學</li>
<li>維宏工業學校</li>
<li>戴南大學</li>
<li>越南技術與經濟貿易學院</li>
<li>河內觀光學院</li>
<li>孫德生大學</li>
<li>胡志明市開放大學</li>
<li>胡志明市私立科技大學</li>
<li>覓港技術學院</li>
</ul></td>
</tr>
<tr class="even">
<td></td>
<td><ul>
<li><a href="../Page/坎培拉大學.md" title="wikilink">坎培拉大學</a></li>
<li><a href="../Page/南澳大學.md" title="wikilink">南澳大學</a></li>
<li><a href="../Page/皇家布里斯本國際學院.md" title="wikilink">皇家布里斯本國際學院</a></li>
</ul></td>
</tr>
<tr class="odd">
<td></td>
<td><ul>
<li><a href="../Page/東京工學院.md" title="wikilink">東京工學院</a></li>
<li><a href="../Page/宇都宮大學.md" title="wikilink">宇都宮大學</a></li>
<li><a href="../Page/秋田大學.md" title="wikilink">秋田大學</a></li>
<li><a href="../Page/東洋大學.md" title="wikilink">東洋大學</a></li>
</ul></td>
</tr>
<tr class="even">
<td></td>
<td><ul>
<li><a href="../Page/英格蘭.md" title="wikilink">英格蘭</a><a href="../Page/西敏寺大學.md" title="wikilink">西敏寺大學</a></li>
<li>英格蘭<a href="../Page/桑德蘭大學.md" title="wikilink">桑德蘭大學</a></li>
<li><a href="../Page/北愛爾蘭.md" title="wikilink">北愛爾蘭</a><a href="../Page/貝爾法斯特女王大學.md" title="wikilink">貝爾法斯特女王大學</a></li>
</ul></td>
</tr>
<tr class="odd">
<td></td>
<td><ul>
<li><a href="../Page/莫斯科.md" title="wikilink">莫斯科無線電及電子自動化科技大學</a></li>
<li>莫斯科航太學院</li>
</ul></td>
</tr>
<tr class="even">
<td></td>
<td><ul>
<li><a href="../Page/加利西亞自治區.md" title="wikilink">加利西亞自治區</a><a href="../Page/:en:University_of_Santiago_de_Compostela.md" title="wikilink">孔波斯特拉聖地牙哥大學</a></li>
<li><a href="../Page/馬德里自治區.md" title="wikilink">馬德里自治區</a><a href="../Page/阿爾卡拉大學.md" title="wikilink">阿爾卡拉大學</a></li>
</ul></td>
</tr>
<tr class="odd">
<td></td>
<td><ul>
<li><a href="../Page/密西根州.md" title="wikilink">密西根州</a><a href="../Page/底特律.md" title="wikilink">底特律</a><a href="../Page/聖母大學.md" title="wikilink">聖母大學</a></li>
<li><a href="../Page/喬治亞州.md" title="wikilink">喬治亞州</a><a href="../Page/:en:Mercer_University.md" title="wikilink">MERCER大學</a></li>
<li><a href="../Page/密西根州.md" title="wikilink">密西根州</a><a href="../Page/勞倫斯理工大學.md" title="wikilink">勞倫斯理工大學</a></li>
<li><a href="../Page/佛蒙特州.md" title="wikilink">佛蒙特州</a><a href="../Page/西北理工大學.md" title="wikilink">西北理工大學</a></li>
<li><a href="../Page/康乃狄克州.md" title="wikilink">康乃狄克州</a><a href="../Page/橋港大學.md" title="wikilink">橋港大學</a></li>
<li><a href="../Page/堪薩斯州.md" title="wikilink">堪薩斯州</a><a href="../Page/:en:Pittsburg_State_University.md" title="wikilink">匹茲堡州立大學</a></li>
<li><a href="../Page/肯塔基州.md" title="wikilink">肯塔基州西肯塔基大學ICSET</a></li>
<li><a href="../Page/俄亥俄州.md" title="wikilink">俄亥俄州</a><a href="../Page/楊斯鎮州立大學.md" title="wikilink">揚斯頓州立大學</a></li>
<li><a href="../Page/明尼蘇達州.md" title="wikilink">明尼蘇達州</a><a href="../Page/明尼蘇達大學.md" title="wikilink">明尼蘇達大學克魯克斯頓分校</a></li>
<li><a href="../Page/加利福尼亞州.md" title="wikilink">加利福尼亞州惠特學院</a></li>
<li><a href="../Page/加利福尼亞州.md" title="wikilink">加利福尼亞州曼羅學院</a></li>
<li><a href="../Page/堪薩斯州.md" title="wikilink">堪薩斯州</a><a href="../Page/堪薩斯州立大學.md" title="wikilink">堪薩斯州立大學</a></li>
<li><a href="../Page/佛羅里達大學.md" title="wikilink">佛羅里達大學</a></li>
<li>霍奇斯大學</li>
</ul></td>
</tr>
<tr class="even">
<td></td>
<td><ul>
<li><a href="../Page/哈爾科夫大學.md" title="wikilink">VN卡拉辛哈爾科夫國立大學</a></li>
</ul></td>
</tr>
<tr class="odd">
<td></td>
<td><ul>
<li>普林摩斯科大學</li>
</ul></td>
</tr>
<tr class="even">
<td></td>
<td><ul>
<li><a href="../Page/首都經濟貿易大學.md" title="wikilink">首都經濟貿易大學</a></li>
<li><a href="../Page/山東科技大學.md" title="wikilink">山東科技大學</a></li>
<li><a href="../Page/南京理工大學.md" title="wikilink">南京理工大學</a></li>
<li><a href="../Page/北京工業大學.md" title="wikilink">北京工業大學</a></li>
<li><a href="../Page/北京工商大學.md" title="wikilink">北京工商大學</a></li>
<li><a href="../Page/深圳職業技術學院.md" title="wikilink">深圳職業技術學院</a></li>
<li><a href="../Page/寧波.md" title="wikilink">寧波職業技術學院</a></li>
<li><a href="../Page/廈門.md" title="wikilink">廈門南洋職業學院</a></li>
<li>廈門華天涉外職業技術學院</li>
<li><a href="../Page/上海電機學院.md" title="wikilink">上海電機學院</a></li>
<li><a href="../Page/江蘇經貿職業技術學院.md" title="wikilink">江蘇經貿職業技術學院</a></li>
</ul></td>
</tr>
<tr class="odd">
<td></td>
<td><ul>
<li>捷比商業學院</li>
<li><a href="../Page/孟買.md" title="wikilink">孟買</a><a href="../Page/:en:Don_Bosco_Institute_of_Technology.md" title="wikilink">Don Bosco Institute of Technology</a></li>
</ul></td>
</tr>
<tr class="even">
<td></td>
<td><ul>
<li><a href="../Page/都柏林大學.md" title="wikilink">都柏林大學</a></li>
</ul></td>
</tr>
<tr class="odd">
<td></td>
<td><ul>
<li><a href="../Page/慶旼大學.md" title="wikilink">慶旼大學</a></li>
<li><a href="../Page/慶雲大學.md" title="wikilink">慶雲大學</a></li>
<li><a href="../Page/仁德大學.md" title="wikilink">仁德大學</a></li>
<li><a href="../Page/群長大學.md" title="wikilink">群長大學</a></li>
<li><a href="../Page/大邱科學大學.md" title="wikilink">大邱科學大學</a></li>
<li><a href="../Page/新安山大學.md" title="wikilink">新安山大學</a></li>
<li><a href="../Page/仁川才能大學.md" title="wikilink">仁川才能大學</a></li>
</ul></td>
</tr>
<tr class="even">
<td></td>
<td><ul>
<li><a href="../Page/南方大學學院.md" title="wikilink">南方大學學院</a></li>
<li><a href="../Page/馬來西亞理科大學.md" title="wikilink">馬來西亞理科大學</a></li>
<li><a href="../Page/拉曼大學學院.md" title="wikilink">拉曼大學學院</a></li>
<li><a href="../Page/新紀元學院.md" title="wikilink">新紀元學院</a></li>
</ul></td>
</tr>
<tr class="odd">
<td></td>
<td><ul>
<li>莎格森專業大學</li>
</ul></td>
</tr>
<tr class="even">
<td></td>
<td><ul>
<li><a href="../Page/德拉蕯大學.md" title="wikilink">德拉蕯大學</a></li>
</ul></td>
</tr>
</tbody>
</table>

## 策略聯盟學校

以下是龍華科大的策略聯盟學校列表（以[國中](../Page/國民中學.md "wikilink")[基本學力測驗之](../Page/基本學力測驗.md "wikilink")[登記分發區作為區分](../Page/登記分發.md "wikilink")）：

<table>
<tbody>
<tr class="odd">
<td><h3 id="基北區">基北區</h3>
<ul>
<li><a href="../Page/國立基隆高級商工職業學校.md" title="wikilink">國立基隆高級商工職業學校</a></li>
<li><a href="../Page/台北市立松山高級工農職業學校.md" title="wikilink">台北市立松山高級工農職業學校</a></li>
<li><a href="../Page/台北市私立大同高級中學.md" title="wikilink">台北市私立大同高級中學</a></li>
<li><a href="../Page/台北市私立強恕高級中學.md" title="wikilink">台北市私立強恕高級中學</a></li>
<li><a href="../Page/新北市私立清傳高級商業職業學校.md" title="wikilink">新北市私立清傳高級商業職業學校</a></li>
<li><a href="../Page/新北市私立豫章高級工商職業學校.md" title="wikilink">新北市私立豫章高級工商職業學校</a></li>
</ul></td>
<td><h3 id="桃園區">桃園區</h3>
<ul>
<li><a href="../Page/國立臺北科技大學附屬桃園農工高級中等學校.md" title="wikilink">國立臺北科技大學附屬桃園農工高級中等學校</a></li>
<li><a href="../Page/桃園市立中壢商業高級中等學校.md" title="wikilink">桃園市立中壢商業高級中等學校</a></li>
<li><a href="../Page/桃園市私立光啟高級中學.md" title="wikilink">桃園市私立光啟高級中學</a></li>
<li><a href="../Page/桃園市私立振聲高級中學.md" title="wikilink">桃園市私立振聲高級中學</a></li>
<li><a href="../Page/桃園市私立啟英高級中學.md" title="wikilink">桃園市私立啟英高級中學</a></li>
<li><a href="../Page/桃園市私立清華高級中學.md" title="wikilink">桃園市私立清華高級中學</a></li>
<li><a href="../Page/桃園市私立方曙高級商工職業學校.md" title="wikilink">桃園市私立方曙高級商工職業學校</a></li>
</ul></td>
<td><h3 id="竹苗區">竹苗區</h3>
<ul>
<li><a href="../Page/國立竹東高級中學.md" title="wikilink">國立竹東高級中學</a></li>
<li><a href="../Page/國立大湖高級農工職業學校.md" title="wikilink">國立大湖高級農工職業學校</a></li>
</ul></td>
</tr>
</tbody>
</table>

## 知名校友

  - [齊柏林](../Page/齊柏林_\(臺灣\).md "wikilink")：二專部工業管理科畢，攝影師兼導演，執導紀錄片《[看見台灣](../Page/看見台灣.md "wikilink")》。（於2017年因拍攝《[看見台灣II](../Page/看見台灣II.md "wikilink")》而在[空難中去世](../Page/2017年凌天航空直升機事故.md "wikilink")。）
  - [應天華](../Page/應天華.md "wikilink")：二專部機械工程科畢，[美國](../Page/美國.md "wikilink")[洛杉磯I](../Page/洛杉磯郡.md "wikilink")/O
    Controls電子公司創辦人兼[總裁](../Page/總裁.md "wikilink")，首位駕駛[飛機環繞](../Page/飛機.md "wikilink")[地球一圈的](../Page/地球.md "wikilink")[華裔](../Page/華裔.md "wikilink")[飛行家](../Page/飛行家.md "wikilink")。（於2017年在美國加利福尼亞州[聖蓋博谷機場墜機去世](../Page/聖蓋博谷.md "wikilink")）
  - [谷阿莫](../Page/谷阿莫.md "wikilink")：四技部資訊網路工程系畢，台灣[網路紅人](../Page/網路紅人.md "wikilink")。
  - [詹益樺](../Page/詹益樺.md "wikilink")：[台灣獨立運動人士](../Page/台灣獨立運動.md "wikilink")。
  - [曹來旺](../Page/曹來旺.md "wikilink")：[民主進步黨籍前](../Page/民主進步黨.md "wikilink")[立法委員](../Page/立法委員.md "wikilink")、前[台北縣議員](../Page/台北縣議員.md "wikilink")。
  - 丘昌其：[台灣知識庫](../Page/臺灣知識庫.md "wikilink")[董事長兼](../Page/董事長.md "wikilink")[執行長](../Page/執行長.md "wikilink")。
  - 李仟萬：二專部工業管理科畢，[台灣專案管理學會理事長](../Page/台灣專案管理學會.md "wikilink")、龍華科技大學工業管理系系友會會長。

## 相關條目

  - [技職教育](../Page/技職教育.md "wikilink")
  - [科技大學](../Page/科技大學_\(臺灣學制\).md "wikilink")
  - [台灣大專院校列表](../Page/台灣大專院校列表.md "wikilink")
  - [桃園市大專院校列表](../Page/桃園市大專院校列表.md "wikilink")
  - [發展典範科技大學計畫](../Page/發展典範科技大學計畫.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

  - [龍華科技大學](http://www.lhu.edu.tw)
  - [北區技專校院教學資源中心](http://www.ctle.ntut.edu.tw/)

[\*](../Category/龍華科技大學.md "wikilink")
[Category:1969年創建的教育機構](../Category/1969年創建的教育機構.md "wikilink")
[Category:龜山區](../Category/龜山區.md "wikilink")

1.  為全臺灣首間以多媒體與遊戲發展科學為名之科系。
2.  [龍華科大的姐妹校列表](http://ice.lhu.edu.tw/website/international.html)