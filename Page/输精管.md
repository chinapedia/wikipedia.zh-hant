**输精管**是人和[动物体内输送](../Page/动物.md "wikilink")[精子的生殖管道](../Page/精子.md "wikilink")。

[人的输精管是一对由壁厚腔小的](../Page/人.md "wikilink")[肌肉组成的长约](../Page/肌肉.md "wikilink")30－50厘米的传送[精子的管道](../Page/精子.md "wikilink")，自[附睾下端向上](../Page/附睾.md "wikilink")，从[阴囊上升](../Page/阴囊.md "wikilink")，穿过[腹股沟管进入](../Page/腹股沟.md "wikilink")[盆腔内](../Page/盆腔.md "wikilink")，其末端与精囊腺的排泄管汇合成射精管，开口于[尿道的](../Page/尿道.md "wikilink")[前列腺部](../Page/前列腺.md "wikilink")；它是[附睾管的延续](../Page/附睾管.md "wikilink")，它负责将精子输送到[射精管](../Page/射精管.md "wikilink")；是[男性生殖系统中的组成部分](../Page/男性生殖系统.md "wikilink")。

输精管相对比较表浅，直接在[阴囊及其上部即可通过](../Page/阴囊.md "wikilink")[皮肤直接触摸到](../Page/皮肤.md "wikilink")。所以，相较於[输卵管結紮术](../Page/输卵管結紮术.md "wikilink")，[输精管結紮术具有安全快捷的特点](../Page/输精管結紮术.md "wikilink")。

## 外部連結

  - \- "Inguinal Region, Scrotum and Testes: Layers of the Spermatic
    Cord"

  - \- "The Male Pelvis: Distribution of the Peritoneum in the Male
    Pelvis"

  - (dead link 2015/11/15)

  -
  - ()

[Category:男性生殖系统](../Category/男性生殖系统.md "wikilink")