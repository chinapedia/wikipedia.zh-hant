**陳建銘**，[中華民國政治人物](../Page/中華民國.md "wikilink")，台灣台北市人，現任無黨籍[臺北市議員](../Page/臺北市議員.md "wikilink")。

## 簡介

陳建銘是[美國加州專業心理學院](../Page/美國.md "wikilink")[博士](../Page/博士.md "wikilink")，父親[陳安邦是](../Page/陳安邦.md "wikilink")[明德樂園大股東之一](../Page/明德樂園.md "wikilink")，曾擔任明德樂園總經理，在政壇也擔任過[國大代表](../Page/國大代表.md "wikilink")，而陳建銘之前也曾任明德樂園副總。1996年陳建銘獲[中國國民黨提名當選](../Page/中國國民黨.md "wikilink")[國大代表](../Page/國大代表.md "wikilink")，但2001年適[台灣團結聯盟](../Page/台灣團結聯盟.md "wikilink")（台聯）成立，改加入台聯，由台聯提名參選[立法委員並當選](../Page/立法委員.md "wikilink")。

[2004年連任立委失敗後](../Page/2004年中華民國立法委員選舉.md "wikilink")，陳建銘在台聯黨內安排下，代表[泛綠參選](../Page/泛綠.md "wikilink")[2005年基隆市長選舉](../Page/2005年中華民國地方公職人員選舉.md "wikilink")，後以17919票差距輸給了[許財利](../Page/許財利.md "wikilink")。

陳建銘在競選[基隆市市長落敗後](../Page/基隆市市長.md "wikilink")，轉而角逐[台北市議員](../Page/台北市議員.md "wikilink")。在[2006年台北市議員選舉中](../Page/2006年中華民國直轄市市長暨市議員選舉.md "wikilink")，陳建銘在第一選區（[北投](../Page/北投區.md "wikilink")、[士林](../Page/士林區.md "wikilink")）以28,698票當選，選前搶救奏效，成為全國第一高票當選的議員；加上第四選區（[中山](../Page/中山區_\(台北市\).md "wikilink")、[大同](../Page/大同區_\(台北市\).md "wikilink")）的[簡余晏](../Page/簡余晏.md "wikilink")，讓台聯在[台北市議會首次獲得席次](../Page/台北市議會.md "wikilink")。

2007年，台聯路線引起爭議，陳建銘表示不想辜負選民的期望，決定[退黨](../Page/退黨.md "wikilink")；但2008年之後，台聯陸續對[錢林慧君與](../Page/錢林慧君.md "wikilink")[賴幸媛做出黨紀處分](../Page/賴幸媛.md "wikilink")，清除黨內親[馬英九勢力](../Page/馬英九.md "wikilink")。2010年，在前總統[李登輝與黨主席](../Page/李登輝.md "wikilink")[黃昆輝約見後](../Page/黃昆輝.md "wikilink")，4月20日陳建銘重返台聯。2010年11月27日，陳建銘以17,136票連任台北市議員，成為台聯在[台北市議會唯一的席次](../Page/台北市議會.md "wikilink")。2014年陳建銘再次連任台北市議員。

2016年11月21日，陳建銘在[ETtoday東森新聞雲直播節目上說](../Page/ETtoday東森新聞雲.md "wikilink")，針對[台灣數位光訊科技宣布購買](../Page/台灣數位光訊科技.md "wikilink")[東森電視事件](../Page/東森電視.md "wikilink")，敬告曾經支持[反媒體壟斷運動的](../Page/反媒體壟斷運動.md "wikilink")[民主進步黨不要](../Page/民主進步黨.md "wikilink")[換了位置就換了腦袋](../Page/髮夾彎.md "wikilink")，「別以為執政了就不會有[社會運動](../Page/社會運動.md "wikilink")」\[1\]。

2017年5月2日，陳建銘女兒[陳思宇接任](../Page/陳思宇_\(政治人物\).md "wikilink")[台北市政府副發言人](../Page/台北市政府.md "wikilink")。2018年1月17日，陳思宇轉任[臺北市政府觀光傳播局局長](../Page/臺北市政府觀光傳播局.md "wikilink")\[2\]。陳建銘兒子陳冠廷就讀於美國波士頓大學。2018年11月，其妹[陳靜敏亦就職為民進黨籍立法委員](../Page/陳靜敏.md "wikilink")。

[2018年九合一選舉](../Page/2018年九合一選舉.md "wikilink")，陳建銘宣布以無黨籍競選連任臺北市議員，亦會因此被台聯開除黨籍\[3\]；最終以一萬三千餘票「吊車尾」驚險連任，順利四連霸。

## 政治經歷

### 2005年基隆市長選舉

| 2005年[基隆市市長選舉結果](../Page/基隆市市長.md "wikilink") |
| --------------------------------------------- |
| 號次                                            |
| 1                                             |
| 2                                             |
| 3                                             |
| 4                                             |
| **選舉人數**                                      |
| **投票數**                                       |
| **有效票**                                       |
| **無效票**                                       |
| **投票率**                                       |

### 2018年臺北市議員選舉

#### [臺北市第一選舉區（北投、士林區）](../Page/2018年中華民國直轄市議員及縣市議員選舉選舉區投票結果列表#臺北市第一選舉區（北投、士林區）.md "wikilink")

## 相關條目

  - [陳安邦](../Page/陳安邦.md "wikilink")
  - [陳思宇](../Page/陳思宇.md "wikilink")
  - [台灣團結聯盟](../Page/台灣團結聯盟.md "wikilink")

## 參考資料

## 外部連結

[Category:第3屆中華民國國民大會代表](../Category/第3屆中華民國國民大會代表.md "wikilink")
[Category:第5屆中華民國立法委員](../Category/第5屆中華民國立法委員.md "wikilink")
[Category:第10屆臺北市議員](../Category/第10屆臺北市議員.md "wikilink")
[Category:第11屆臺北市議員](../Category/第11屆臺北市議員.md "wikilink")
[Category:第12屆臺北市議員](../Category/第12屆臺北市議員.md "wikilink")
[Category:第13屆臺北市議員](../Category/第13屆臺北市議員.md "wikilink")
[Category:前中國國民黨黨員](../Category/前中國國民黨黨員.md "wikilink")
[Category:前台灣團結聯盟黨員](../Category/前台灣團結聯盟黨員.md "wikilink")
[Category:清雲科技大學教授](../Category/清雲科技大學教授.md "wikilink")
[Category:國立臺北大學校友](../Category/國立臺北大學校友.md "wikilink")
[Category:輔仁大學校友](../Category/輔仁大學校友.md "wikilink")
[Category:臺北城市科技大學校友](../Category/臺北城市科技大學校友.md "wikilink")
[Category:士林人](../Category/士林人.md "wikilink")
[C建](../Category/陳姓.md "wikilink")
[Category:台北市政治人物](../Category/台北市政治人物.md "wikilink")

1.
2.
3.  [無黨參選 陳建銘要跳脫藍綠
    體民所苦](https://www.chinatimes.com/realtimenews/20180829002908-260407)