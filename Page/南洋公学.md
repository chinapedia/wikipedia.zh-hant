**南洋公学**为中国近代历史上最早创办的大学之一，于光绪廿二年（1896年）由[盛宣怀创建于上海](../Page/盛宣怀.md "wikilink")，是为[交通大学沪校前身](../Page/交通大学.md "wikilink")。原址位于现[上海交通大学徐汇校区](../Page/上海交通大学徐汇校区.md "wikilink")。

它与[武汉大学前身](../Page/武汉大学.md "wikilink")[自强学堂](../Page/自强学堂.md "wikilink")、[天津大学前身](../Page/天津大学.md "wikilink")[北洋大学堂](../Page/北洋大学堂.md "wikilink")、[四川大学前身](../Page/四川大学.md "wikilink")[四川中西学堂](../Page/四川中西学堂.md "wikilink")、[北京大学前身](../Page/北京大学.md "wikilink")[京师大学堂属同时期的近代新式高校](../Page/京师大学堂.md "wikilink")。《[清史稿](../Page/清史稿.md "wikilink")》记载1902年管学大臣[张百熙关于](../Page/张百熙.md "wikilink")“筹办大学堂”的奏折中所言：“查京外学堂，办有成效者，以湖北[自强学堂](../Page/自强学堂.md "wikilink")、上海**南洋公学**为最。”

[The_Gate_of_Nanyang_College_color.jpg](https://zh.wikipedia.org/wiki/File:The_Gate_of_Nanyang_College_color.jpg "fig:The_Gate_of_Nanyang_College_color.jpg")

## 历史

[SXH001.jpg](https://zh.wikipedia.org/wiki/File:SXH001.jpg "fig:SXH001.jpg")、南洋公学的创办人[盛宣怀](../Page/盛宣怀.md "wikilink")\]\]
南洋公学由[盛宣怀](../Page/盛宣怀.md "wikilink")1895年（光绪21年）创建北洋大学堂后，调任上海后创办，是中国近代历史上最早创办的大学之一。天津海关道盛宣怀在天津创办[北洋大学堂后](../Page/北洋大学堂.md "wikilink")，曾与两江总督[刘坤一商讨设立](../Page/刘坤一.md "wikilink")**南洋公学**。1896年10月，盛宣怀上奏《条陈自强大计折》并附《请设学堂片》奏请在上海设置南洋公学，提议由[轮船招商局和电报局提供经费](../Page/轮船招商局.md "wikilink")。12月6日[光绪帝谕令批准盛宣怀设立学堂](../Page/光绪.md "wikilink")。\[1\]南洋公学隸属于招商局和电报局，校內设置了师范學院、外院、中院和上院四院，盛宣怀首任督办。

1900年，[八国联军入侵津京地区](../Page/八国联军.md "wikilink")，同为盛宣怀所创办的[北洋大学堂在](../Page/北洋大学.md "wikilink")[天津一度被迫停办](../Page/天津.md "wikilink")，部分师生因战乱南迁至上海，在盛宣怀的安排下入读南洋公学在上院专门为接受北洋师生所开设的铁路班，直至北方战局平息，北洋师生及铁路班迁回天津\[2\]。

1905年改隶商部，更名为**商部上海高等实业学堂**。1907年改隶邮传部，更名为**邮传部上海高等实业学堂**。辛亥革命后1911年到1912年间为**南洋大学堂**。1912年[中华民国成立后划归继承清政府邮传部的](../Page/中华民国.md "wikilink")[中华民国交通部](../Page/中华民国交通部.md "wikilink")，更名为**交通部上海工业专门学校**，直至1921年与交通部下属的另外三校合并为交通大学。\[3\]

## 校园

[Shanghai_Jiao_Tong_University_Layout_1912-1921.svg](https://zh.wikipedia.org/wiki/File:Shanghai_Jiao_Tong_University_Layout_1912-1921.svg "fig:Shanghai_Jiao_Tong_University_Layout_1912-1921.svg")


初建时的校区四面环河，位置现今校区的东部，徐汇校区的多数历史建筑亦分布于此。现今东侧草坪原为露天体育场，建筑环绕四周分布，建于1899年的[中院位于草坪北侧](../Page/中院.md "wikilink")，是徐汇校区现存最早的教学楼。后经多次购地扩建，逐步形成今日的校区格局。其中，南洋公学时期建筑位列第二批[上海市优秀历史建筑](../Page/上海市优秀历史建筑.md "wikilink")。

南洋公学初建时学生居住于[上院](../Page/上院_\(南洋公学\).md "wikilink")，后在校区西侧修筑有[执信西斋作为宿舍使用](../Page/执信西斋.md "wikilink")。

  - [上院](../Page/上院_\(南洋公学\).md "wikilink")
  - [中院](../Page/中院.md "wikilink")
  - [老图书馆](../Page/交通大学老图书馆.md "wikilink")
  - [老校门](../Page/交通大学老校门.md "wikilink")
  - [南洋公学界碑和石碾](../Page/南洋公学界碑和石碾.md "wikilink")

## 院系设置

外院{{·}}中院{{·}}东文学堂{{·}}铁路班{{·}}附属小学{{·}}特班{{·}}政治班{{·}}商务班

}}

## 历任校长

[Sheng_Xuanhuai.jpg](https://zh.wikipedia.org/wiki/File:Sheng_Xuanhuai.jpg "fig:Sheng_Xuanhuai.jpg")

| width="150" align ="center"|职务 | width="100" align ="center"|姓名                            | width="200" align ="center"|任期 |
| ------------------------------ | --------------------------------------------------------- | ------------------------------ |
|                                |                                                           |                                |
| **南洋公学**                       |                                                           |                                |
| 督辦                             | align ="center"|[盛宣懷](../Page/盛宣懷.md "wikilink")          | align ="center"|1896年冬-1905年春  |
| 總理                             | align ="center"|[何嗣琨](../Page/何嗣琨.md "wikilink")          | align ="center"|1897年春-1901年春  |
| 總理                             | align ="center"|[張元濟](../Page/張元濟.md "wikilink")          | align ="center"|1901年春-1901年夏  |
| 總理                             | align ="center"|[勞乃宣](../Page/勞乃宣.md "wikilink")          | align ="center"|1901年秋-1901年冬  |
| 總理                             | align ="center"|[沈曾植](../Page/沈曾植.md "wikilink")          | align ="center"|1901年冬-1902年春  |
| 總理                             | align ="center"|[汪風藻](../Page/汪風藻.md "wikilink")          | align ="center"|1902年春-1902年冬  |
| 總理                             | align ="center"|[劉樹屏](../Page/劉樹屏.md "wikilink")          | align ="center"|1902年冬-1903年夏  |
| 提調兼總理                          | align ="center"|[張美翊](../Page/張美翊.md "wikilink")          | align ="center"|1903年夏-1903年冬  |
| 總理                             | align ="center"|[張鶴齡](../Page/張鶴齡_\(光緒進士\).md "wikilink") | align ="center"|1903年冬-1904年夏  |
| 提調兼總理                          | align ="center"|[張美翊](../Page/張美翊.md "wikilink")          | align ="center"|1904年夏-1904年冬  |

## 知名校友

  - 南洋公学至合校前毕业知名校友

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><small></p>
<ul>
<li><a href="../Page/蔡锷.md" title="wikilink">蔡锷</a>：1898年考入南洋公学，中国<a href="../Page/政治家.md" title="wikilink">政治家</a>、<a href="../Page/军事家.md" title="wikilink">军事家</a>。</li>
<li><a href="../Page/蔡元培.md" title="wikilink">蔡元培</a>：1901年任南洋公学特班总教习，近代革命家、<a href="../Page/教育家.md" title="wikilink">教育家</a>、政治家。</li>
<li><a href="../Page/黄炎培.md" title="wikilink">黄炎培</a>：1901年入讀南洋公学，中華人民共和國教育家，实业家，政治家，中國民主同盟主要發起人之一。</li>
<li><a href="../Page/李叔同.md" title="wikilink">李叔同</a>：1901年就读于南洋公学，現代中國著名<a href="../Page/艺术家.md" title="wikilink">艺术家</a>、艺术教育家。以弘一法師名世。律宗第十一代祖師。</li>
<li><a href="../Page/邵力子.md" title="wikilink">邵力子</a>：1901年就读于南洋公学特班，中国共产党发起人之一。</li>
<li><a href="../Page/蔣夢麟.md" title="wikilink">蔣夢麟</a>：1904年入讀南洋公学，近代<a href="../Page/教育家.md" title="wikilink">教育家</a>、政治家，曾任北大校長、中山大學校長、國民政府教育部長。</li>
<li><a href="../Page/温应星.md" title="wikilink">温应星</a>：1904年秋，入<a href="../Page/維吉尼亞軍校.md" title="wikilink">維吉尼亞軍校學習</a>。7個月後，奉清廷之命，轉入<a href="../Page/西點軍校.md" title="wikilink">西點軍校</a>，與<a href="../Page/乔治·巴顿.md" title="wikilink">乔治·巴顿同學</a>。1909年畢業，第一位從西點軍校畢業的中國留學生。</li>
</ul>
<p></small></p></td>
<td><p><small></p>
<ul>
<li><a href="../Page/凌鸿勋.md" title="wikilink">凌鸿勋</a>：1910年考入<a href="../Page/郵傳部上海高等實業學堂.md" title="wikilink">郵傳部上海高等實業學堂</a>，鐵道工程學家。</li>
<li><a href="../Page/邹韬奋.md" title="wikilink">邹韬奋</a>：1912年就读于<a href="../Page/交通部上海工业专门学校.md" title="wikilink">交通部上海工业专门学校电机工程科</a>。大学二年级后，由于兴趣原因转而投考上海<a href="../Page/圣约翰大学.md" title="wikilink">圣约翰大学文科</a>。后成为知名記者、出版家。</li>
<li><a href="../Page/侯绍裘.md" title="wikilink">侯绍裘</a>：1918年考入南洋公学，“五卅”运动领导人之一。</li>
<li><a href="../Page/陆定一.md" title="wikilink">陆定一</a>：1922年入<a href="../Page/南洋大学.md" title="wikilink">南洋大学电机工程科学习</a>，中国共产党重要<a href="../Page/作家.md" title="wikilink">作家和文化人物</a>。</li>
<li><a href="../Page/陈一白.md" title="wikilink">陈一白</a>：1925年入<a href="../Page/南洋大学.md" title="wikilink">南洋大学电机工程科</a>，国民政府航空委员会防空总台首任总台长，空军少将。<a href="../Page/抗日战争.md" title="wikilink">抗日战争</a><a href="../Page/淞沪会战.md" title="wikilink">淞沪会战指挥</a><a href="../Page/中日首次空战.md" title="wikilink">中日首次空战获</a><a href="../Page/八一四空军大捷.md" title="wikilink">八一四空军大捷</a>。</li>
</ul>
<p></small></p></td>
</tr>
</tbody>
</table>

## 参考文献

## 参见

  - [交通大学](../Page/交通大学.md "wikilink")
      - [上海交通大学](../Page/上海交通大学.md "wikilink")：[徐汇校区](../Page/上海交通大学徐汇校区.md "wikilink")
  - [南洋模范中学](../Page/南洋模范中学.md "wikilink")（原**南洋公学**附属小学）

{{-}}

[Category:清朝学校](../Category/清朝学校.md "wikilink")
[Category:1896年創建的教育機構](../Category/1896年創建的教育機構.md "wikilink")
[Category:上海交通大学](../Category/上海交通大学.md "wikilink")
[Category:上海教育史](../Category/上海教育史.md "wikilink")
[南洋公学](../Category/南洋公学.md "wikilink")
[Category:徐家汇](../Category/徐家汇.md "wikilink")
[Category:國立交通大學校史](../Category/國立交通大學校史.md "wikilink")

1.  [南洋公学1896年（光绪二十二年）纪事](http://jdxiaoshi.sjtu.edu.cn/szdjwz_view.jsp?id=2)

2.
3.  [1](http://news.sciencenet.cn/sbhtmlnews/2010/1/228453.html)