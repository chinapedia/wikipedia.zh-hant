[Luo_Wengan3.jpg](https://zh.wikipedia.org/wiki/File:Luo_Wengan3.jpg "fig:Luo_Wengan3.jpg")
**羅文榦**（），字**鈞任**，[廣東](../Page/廣東.md "wikilink")[番禺县](../Page/番禺县.md "wikilink")（今[廣州](../Page/廣州.md "wikilink")[海珠區](../Page/海珠區.md "wikilink")[瀝滘](../Page/瀝滘.md "wikilink")）人。民国政治家。

## 生平

早年留學英國[牛津大學學習法律](../Page/牛津大學.md "wikilink")，[宣統元年](../Page/宣統.md "wikilink")（1909年）畢業獲[法學碩士學位](../Page/法學碩士.md "wikilink")，同年於[英國](../Page/英國.md "wikilink")[倫敦](../Page/倫敦.md "wikilink")[內殿律師學院獲](../Page/內殿律師學院.md "wikilink")[英格蘭及威爾斯高等法院](../Page/英格蘭及威爾斯高等法院.md "wikilink")[大律師資格](../Page/大律師.md "wikilink")\[1\]。回國後評為留學生最優，賜法政科進士，任教於北大，與[胡適交誼甚篤](../Page/胡適.md "wikilink")，自命為“有職業而不靠政治吃飯”的自由主義知識分子。1912年在北洋政府歷任檢察長及司法、財政等部長。[籌安會成立後](../Page/籌安會.md "wikilink")，羅以檢察長身份參劾籌安會为組織非法。1915年辭職南下，參加反袁。1921年12月复任北洋政府司法总长，1922年1月改任大理院院长，同年9月任[王宠惠内阁财政总长](../Page/王宠惠.md "wikilink")。

1922年11月18日因訂立《奧國借款展期合同》，被众议院议长[吴景濂告发受贿被捕](../Page/吴景濂.md "wikilink")，并导致[王宠惠内阁辞职](../Page/王宠惠.md "wikilink")，史稱“羅文榦案”。次年6月无罪释放。[蔡元培曾为此憤而辭職](../Page/蔡元培.md "wikilink")。当时[顾维钧被摄政国务总理](../Page/顾维钧.md "wikilink")[高凌霨任命为外交部长时也曾表示](../Page/高凌霨.md "wikilink")，羅文榦案不澄清，绝不上任。

1928年被聘為東北邊防司令長官公署顧問。1931年擔任國民政府司法行政部長，1932年兼[外交部長等職](../Page/中华民国外交部.md "wikilink")。1932年5月审判[共产国际特工](../Page/共产国际.md "wikilink")[牛兰夫妇事件中](../Page/牛兰.md "wikilink")，反对宋庆龄、蔡元培保释被告。1933年5月，在《塘沽停战协定》签字后，呈请辞职，经蒋介石挽留，直到12月2日，最终辞去外长职位。1934年10月又辞去司法行政部长职务。之后，在[西南联大教授](../Page/西南联大.md "wikilink")《中国法制史》。1941年10月，在粤北乐昌县城，因恶性疟疾不治身亡，享年53岁。

## 逸事

1933年羅文榦在[新疆串聯](../Page/新疆.md "wikilink")[張培元](../Page/張培元.md "wikilink")、[馬仲英以推翻](../Page/馬仲英.md "wikilink")[盛世才](../Page/盛世才.md "wikilink")。當時罗文榦離開新疆未帶行李，被盛世才没收，在1937年新疆财政监察委员会拍卖时，标有「罗文榦逆产一项，仅手提小皮箱一个，折扇一把，绸衫一件。」\[2\]

## 参考文献

  - 《剑桥中华民国史·宪政理想的衰落·1922—1928年》
  - 郭廷以《中华民国史事日志·1922年》

| （[北京政府](../Page/北京政府.md "wikilink")） |
| ------------------------------------ |
| （[国民政府](../Page/国民政府.md "wikilink")） |

[Category:中華民國法學家](../Category/中華民國法學家.md "wikilink")
[Category:中華民國法務部部長](../Category/中華民國法務部部長.md "wikilink")
[Category:中華民國外交部部長](../Category/中華民國外交部部長.md "wikilink")
[Category:國立西南聯合大學教授](../Category/國立西南聯合大學教授.md "wikilink")
[Category:北京大學教授](../Category/北京大學教授.md "wikilink")
[Category:牛津大學校友](../Category/牛津大學校友.md "wikilink")
[Category:广州人](../Category/广州人.md "wikilink")
[W文](../Category/罗姓.md "wikilink")
[Category:中華民國外交總長](../Category/中華民國外交總長.md "wikilink")
[Category:中華民國財政總長](../Category/中華民國財政總長.md "wikilink")
[Category:中華民國司法總長](../Category/中華民國司法總長.md "wikilink")
[Category:中華民國檢察官](../Category/中華民國檢察官.md "wikilink")
[Category:第一届国民参政会参政员](../Category/第一届国民参政会参政员.md "wikilink")
[Category:第二届国民参政会参政员](../Category/第二届国民参政会参政员.md "wikilink")

1.  Who's Who in China, 5th ed. Shanghai: The China Weekly Review. 1936.
2.