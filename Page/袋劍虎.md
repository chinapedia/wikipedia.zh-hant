**袋劍虎屬**（[学名](../Page/学名.md "wikilink")：*Thylacosmilus*）是[袋犬目](../Page/袋犬目.md "wikilink")[袋剑虎科的一](../Page/袋剑虎科.md "wikilink")[屬](../Page/屬.md "wikilink")，是拥有劍齒的肉食[有袋類](../Page/有袋類.md "wikilink")，生存於晚[中新世至](../Page/中新世.md "wikilink")[上新世](../Page/上新世.md "wikilink")。牠們的遺骸在[南美洲發現](../Page/南美洲.md "wikilink")，主要都是在[阿根廷](../Page/阿根廷.md "wikilink")[卡塔馬卡省](../Page/卡塔馬卡省.md "wikilink")、[恩特雷里奧斯省與](../Page/恩特雷里奧斯省.md "wikilink")[拉潘帕省](../Page/拉潘帕省.md "wikilink")\[1\]。牠並非[斯劍虎的近親](../Page/斯劍虎.md "wikilink")，只是因[趨同演化的關係而有類似的外形](../Page/趨同演化.md "wikilink")。

[Tylacosmilus_DB.jpg](https://zh.wikipedia.org/wiki/File:Tylacosmilus_DB.jpg "fig:Tylacosmilus_DB.jpg")

## 描述

[Thylacosmilus_3D_model.jpg](https://zh.wikipedia.org/wiki/File:Thylacosmilus_3D_model.jpg "fig:Thylacosmilus_3D_model.jpg")
袋劍虎具有一對如軍刀的上犬齒，在一生中會不停生長，牙根的位置位於眼窩上方\[2\]。劍齒的特化進而影響了袋劍虎門齒的退化，不過其他劍齒虎如[斯劍虎與](../Page/斯劍虎.md "wikilink")[巴博劍齒虎依然保有完整的門牙齒式](../Page/巴博劍齒虎.md "wikilink")\[3\]。牠的[下顎亦有一對突出的邊](../Page/下顎.md "wikilink")，平時可以保護其劍齒。

雖然袋劍虎身體部份的化石並不完整，但足以發現許多與其他劍齒虎類似獲相同的特徵。袋劍虎的[頸椎十分粗壯](../Page/頸椎.md "wikilink")，類似於[劍齒虎亞科](../Page/劍齒虎亞科.md "wikilink")\[4\]。前肢[肱骨與](../Page/肱骨.md "wikilink")[股骨十分粗壯](../Page/股骨.md "wikilink")，肱骨有發達的胸肌與[三角肌附著](../Page/三角肌.md "wikilink")，可用來捕捉獵物並吸收獵物造成的碰撞\[5\]後腳肱骨也很粗壯，短[脛骨與](../Page/脛骨.md "wikilink")[蹠行足顯示袋劍虎並不擅長奔跑](../Page/蹠行足.md "wikilink")，可能採伏擊策略。和[巴博劍齒虎科與](../Page/巴博劍齒虎科.md "wikilink")[獵貓科不一樣的是](../Page/獵貓科.md "wikilink")，袋劍虎的爪子並無法收縮\[6\]。
[Sabertoothed_predators_size_comparison_(all_6_groups).jpg](https://zh.wikipedia.org/wiki/File:Sabertoothed_predators_size_comparison_\(all_6_groups\).jpg "fig:Sabertoothed_predators_size_comparison_(all_6_groups).jpg")

## 滅絕

一些較早的文獻會寫道：袋劍虎的滅絕是在[南北美洲生物大遷徙贏不過更為強勢的斯劍虎而絕種](../Page/南北美洲生物大遷徙.md "wikilink")。然而更新的研究顯示，袋劍虎大約於上新世（3.6–2.58
百萬年前）就已滅絕，而斯劍虎最早則是在中[更新世](../Page/更新世.md "wikilink")（781,000-126,000
年前）才抵達南美洲\[7\]。因此兩種袋劍虎與斯劍虎的消長之間其實存在著 150 萬年的空窗期。

## 參考文獻

## 外部連結

  - [Thylacosmilus (marsupial
    sabre-tooth)](https://web.archive.org/web/20061213161639/http://www.paleocraft.com/thylacosmilus.html)
  - [Art by Maximo
    Salas](http://members.tripod.com/~megalania/salas.html)
  - [Skull of Thylacosmilus](http://www.fossils.com/thylac1.jpg)
  - [Comparison of Thylacosmilus (upper) and Smilodon
    (below)](http://www.manwb.ru/pub/News_2/060413_6.jpg)

[Category:袋劍虎屬](../Category/袋劍虎屬.md "wikilink")
[Category:南美洲史前哺乳動物](../Category/南美洲史前哺乳動物.md "wikilink")

1.

2.

3.  Turnbull, W. D., Butler, P. M., & Joysey, K. A. (1978). Another look
    at dental specialization in the extinct saber-toothed marsupial,
    *Thylacosmilus*, compared with its placental counterparts.
    *Development, function and evolution of teeth*. Academic Press,
    London, 399-414.

4.  Benes, Josef. Prehistoric Animals and Plants. Prague, Artua, 1979.
    Pg. 237-8

5.  Argot, Christine., 2002: Evolution of the locomotion in the
    Borhyaenoids Marsupialia, Mammalia Morphofunctional and
    phylogenetical study and paleoecological implications. Evolution de
    la locomotion chez les Borhyaenoides Marsupialia, Mammalia Etude
    morphofonctionnelle, phylogenetique et implications
    paleoecologiques. *Bulletin de la Societe Zoologique de France*,
    1273: 291-300

6.
7.