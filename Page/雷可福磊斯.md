**雷可福磊斯**（**Rascal
Flatts**）是[美國的一支曾贏得](../Page/美國.md "wikilink")[葛萊美獎的](../Page/葛萊美獎.md "wikilink")[鄉村音樂樂團](../Page/鄉村音樂.md "wikilink")，成立於[田納西](../Page/田納西.md "wikilink")[纳什维尔](../Page/纳什维尔_\(田纳西州\).md "wikilink")。雷可福磊斯自成立就由三名成員組成：主唱[Gary
LeVox](../Page/:en:Gary_LeVox.md "wikilink")、低音吉他手與和聲[Jay
DeMarcus和主奏吉他手與和聲](../Page/:en:Jay_DeMarcus.md "wikilink")[Joe
Don Rooney](../Page/:en:Joe_Don_Rooney.md "wikilink")。\[1\]

雷可福磊斯已發行錄製了六張專輯和一張現場剪輯，皆由[Lyric
Street唱片公司發行](../Page/:en:Lyric_Street_Records.md "wikilink")。他們最早的兩張專輯，2000年的*[Rascal
Flatts](../Page/:en:Rascal_Flatts_\(album\).md "wikilink")*和2002年的*[Melt](../Page/:en:Melt_\(album\).md "wikilink")*，前者在美國取得了雙白金認証，後者是三白金，而2004年的*[Feels
Like Today](../Page/:en:Feels_Like_Today.md "wikilink")*有六白金，2006年的*[Me
and My Gang](../Page/:en:Me_and_My_Gang.md "wikilink")*則逼近五白金認証。

至今，他們已發行38首歌曲登上美國[Billboard雜誌熱門鄉村歌曲排行榜](../Page/:en:Billboard_\(magazine\).md "wikilink")，其中10首登上第一名。在電影[汽車總動員翻唱](../Page/汽車總動員.md "wikilink")[Tom
Cochran的](../Page/:en:Tom_Cochran.md "wikilink")[Life Is a
Highway一曲也同樣因非點播播放登上鄉村音樂排行榜](../Page/:en:Life_Is_a_Highway.md "wikilink")。

## 參考文獻

參考文獻

## 外部連結

  - [Rascal Flatts Official Website](http://www.rascalflatts.com/)

  -
[Category:美國樂團](../Category/美國樂團.md "wikilink")
[Category:美國鄉村音樂歌手](../Category/美國鄉村音樂歌手.md "wikilink")
[Category:葛莱美奖获得者](../Category/葛莱美奖获得者.md "wikilink")

1.