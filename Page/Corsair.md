**Corsair**
公司位於[美國](../Page/美國.md "wikilink")[加州](../Page/加州.md "wikilink")[佛利蒙](../Page/佛利蒙.md "wikilink")，於1994年由
Don Lieberman、John Beekley 與 Andy
Paul創立，它的商標是一排風格獨特的[帆](../Page/帆.md "wikilink")，推測來自
16 至 19 世紀作為[海盗象徵的帆船](../Page/海盗.md "wikilink")，因此中文社区也称其为**海盗船**。

起初該公司為英特爾系統生產 [Cache-On-A-Stick](../Page/Cache-On-A-Stick.md "wikilink")
(COAST)模組。不過英特爾將快取記憶體由主機板移至處理器晶體中，Corsair 就隨著将它們的產品線移動於電腦記憶體上。在90年代後期
Corsair 的产品重心从普通記憶體改為被稱為“超頻記憶體”的高規格的記憶體。其性能高效并且可靠性高，在愛好者的社區內擁有较好的評價。XMS
生產線為“高效能”或“超頻”生產線，而 Value Select 生產線則為一般用家設計。

Corsair亦有生產加入了抗水橡膠的[隨身碟](../Page/隨身碟.md "wikilink") Flash
Voyager，其高耐久度也同樣取得了認可。就像 Value Select 產品線一样，Corsair
亦有相對低價的 USB 隨身碟，但不像 Flash Voyager 搭載精密外殼。

Corsair 也有生產許多的水冷散熱模組。從 Hydrocool，一個外置、擁有 LED 溫度顯示與提手的設備開始，直到後來轉型為基於嚴密
Swiftech 設計的 Corsair Cool 內部模組。Corsair 也設計出了小型的水冷產品 Nautilus 500，一個由
120mm 風扇，水冷排，低噪音水泵與可變風速風扇組成的獨立外置元件。Nautilus 500
與其他水冷散熱模組相比是一個意義深長的出發點，因其在保證效果與安裝簡易的前提下仍具有性價比。

2006 年 Corsair 開始製造電源供應器。有TX與HX產品線，但均為[代工產品](../Page/代工.md "wikilink")。

## 产品

[H115i_liquid_cooler.jpg](https://zh.wikipedia.org/wiki/File:H115i_liquid_cooler.jpg "fig:H115i_liquid_cooler.jpg")

Corsair生产的产品包括：

  - 笔记本电脑及台式机电脑[DRAM和](../Page/动态随机存取存储器.md "wikilink")[DIMM存储模块](../Page/DIMM.md "wikilink")

  - [闪存盘](../Page/闪存盘.md "wikilink")

  - ATX和SFX[電源供應器](../Page/電源供應器.md "wikilink")

  - [机箱](../Page/机箱.md "wikilink")

  - 预组装高端游戏电脑

  - CPU和GPU液冷系统

  -
  - [固态硬盘](../Page/固态硬盘.md "wikilink")

  - 游戏[耳机](../Page/耳麦.md "wikilink")

  - 耳机架

  - [电脑键盘](../Page/电脑键盘.md "wikilink")

  - [鼠标](../Page/鼠标.md "wikilink")

  - [鼠标垫](../Page/鼠标垫.md "wikilink")

## 其他

2019年4月1日，Corsair宣布将推出一个游戏启动器启动器(Game Launcher
Launcher)作为愚人节玩笑。该启动器旨在整合市场上所有游戏平台，只需在游戏中轻触Tab就能依次调取所有平台的聊天窗口。除此以外还提供游戏融合功能，支持玩家将永远不去游玩的三款游戏合成为另一款游戏。\[1\]

## 外部連結

  - [Corsair 主頁](http://www.corsair.com)

[Category:電腦硬件公司](../Category/電腦硬件公司.md "wikilink")
[Category:矽谷公司](../Category/矽谷公司.md "wikilink")

1.