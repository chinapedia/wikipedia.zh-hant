[Joseph_Lee.JPG](https://zh.wikipedia.org/wiki/File:Joseph_Lee.JPG "fig:Joseph_Lee.JPG")

**李宗德**
[GBS](../Page/金紫荊星章.md "wikilink")、[OStJ](../Page/聖約翰官佐勳章.md "wikilink")、[太平紳士](../Page/太平紳士.md "wikilink")（Joseph
Lee,
），[香港](../Page/香港.md "wikilink")[企业家](../Page/企业家.md "wikilink")、和富塑化集团主席、社会活动家，前[公民教育委員會](../Page/公民教育委員會.md "wikilink")[主席](../Page/主席.md "wikilink")。

## 简介

籍贯[宁波](../Page/宁波.md "wikilink")[镇海县](../Page/镇海.md "wikilink")（今[北仑区](../Page/北仑区.md "wikilink")），为[李坤之子](../Page/李坤_\(香港\).md "wikilink")，亦是[李美辰的父親](../Page/李美辰.md "wikilink")。李宗德早年就讀[喇沙小學和](../Page/喇沙小學.md "wikilink")[喇沙書院](../Page/喇沙書院.md "wikilink")。在校時與已故全國人大代表[王敏剛為小學同學](../Page/王敏剛.md "wikilink")\[1\]。

李宗德曾担任過[香港青年联会主席](../Page/香港青年联会.md "wikilink")，[香港工业总会](../Page/香港工业总会.md "wikilink")“香港塑胶业协会”副主席，[中华人民共和国国务院](../Page/中华人民共和国国务院.md "wikilink")[港事顾问](../Page/港事顾问.md "wikilink")、[香港特別行政區第一屆政府推選委員會委員](../Page/香港特別行政區第一屆政府推選委員會.md "wikilink")、第九、十屆[港區全國人大代表等职务](../Page/港區全國人大代表.md "wikilink")。并任[中華全國青年聯合會常委](../Page/中華全國青年聯合會.md "wikilink")，[浙江省](../Page/浙江省.md "wikilink")[宁波市第十届政协委员](../Page/宁波市.md "wikilink")，亦曾担任[国际扶轮第](../Page/国际扶轮.md "wikilink")3450地区（[香港](../Page/香港.md "wikilink")、[澳门](../Page/澳门.md "wikilink")）地区总监。

2007年4月至2009年3月任[青年事務委員會主席](../Page/青年事務委員會.md "wikilink")。2008年起担任全国人大代表\[2\]。

2009年4月至2015年3月任[公民教育委員會主席](../Page/公民教育委員會.md "wikilink")。

2016年7月開始，由於[和富社會企業冠名贊助](../Page/和富社會企業.md "wikilink")[大埔足球會](../Page/大埔足球會.md "wikilink")，李宗德亦兼任該會會長。

## 名下馬匹

他是[香港賽馬會會員](../Page/香港賽馬會.md "wikilink")，名下馬匹包括與[張明敏](../Page/張明敏.md "wikilink")、[張英豪](../Page/張英豪.md "wikilink")、[蔡國璽合夥的](../Page/蔡國璽.md "wikilink")「民和國富」、「人和家富」（已退役）、「仁者富足」（現役馬），而「民和國富」、「人和家富」的馬名源自和富集團的格言：「人和家富，民和國富」。

## 榮譽

  - [銅紫荊星章](../Page/銅紫荊星章.md "wikilink")（2000年）
  - 非官守[太平紳士](../Page/太平紳士.md "wikilink")（2003年）
  - [銀紫荊星章](../Page/銀紫荊星章.md "wikilink")（2008年）

## 注释

## 外部链接

  - [李宗德
    简介](https://web.archive.org/web/20060529050435/http://www.nbwh.gov.cn/homepage/page05-01-01.php?id=1038294930&theme=43&uptheme=true)
  - [新华网 十届全国人大代表李宗德
    简介](http://news.xinhuanet.com/zhengfu/2004-03/06/content_1348618.htm)

|width=25% align=center|**前任：**
[彭敬慈](../Page/彭敬慈.md "wikilink") |width=50% align=center
colspan="2"|**[公民教育委員會主席](../Page/公民教育委員會.md "wikilink")**
2009年4月1日-2015年3月31日 |width=25% align=center|**繼任：**
彭韻僖

[L李](../Category/香港寧波人.md "wikilink")
[L](../Category/镇海县人.md "wikilink")
[Category:中国企业家](../Category/中国企业家.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[L](../Category/香港馬主.md "wikilink")
[C](../Category/李姓.md "wikilink")
[L](../Category/喇沙書院校友.md "wikilink")
[Category:公民教育](../Category/公民教育.md "wikilink")
[Z](../Category/三江李氏.md "wikilink")
[Category:香港浸會大學榮譽博士](../Category/香港浸會大學榮譽博士.md "wikilink")
[Category:民主建港協進聯盟成員](../Category/民主建港協進聯盟成員.md "wikilink")
[Category:前香港協進聯盟成員](../Category/前香港協進聯盟成員.md "wikilink")
[Category:第九屆港區全國人大代表](../Category/第九屆港區全國人大代表.md "wikilink")
[Category:第十屆港區全國人大代表](../Category/第十屆港區全國人大代表.md "wikilink")
[Category:第十一屆港區全國人大代表](../Category/第十一屆港區全國人大代表.md "wikilink")
[Category:國際扶輪地區總監](../Category/國際扶輪地區總監.md "wikilink")

1.
2.