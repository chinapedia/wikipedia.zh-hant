**Netscape Navigator
9**是[網景領航員](../Page/網景領航員.md "wikilink")[瀏覽器最後的版本](../Page/瀏覽器.md "wikilink")，由美國線上旗下的網景製作。

自2004年將第8版的[Netscape
Browser委交Mercurial](../Page/Netscape_Browser.md "wikilink")
Communications製作以來，第一個重回網景製作的版本\[1\]。Netscape Navigator
9重回最早Navigator的名稱，此名稱曾經被用於Netscape
1到4.08之間，當時的官方翻譯名稱即是「[網景領航員](../Page/網景領航員.md "wikilink")」\[2\]。

## 特點

### 新功能

Netscape Navigator 9
Beta版中有對於[消息來源支援及和](../Page/消息來源.md "wikilink")[Netscape.com入口網站更好的結合](../Page/網景#Netscape.com網站.md "wikilink")\[3\]，包含改進討論方式和網頁上的投票\[4\]。因為Netscape.com由原來的社群網站改成一般入口網站，在RC1版裏面全部相關功能已取下，以上的功能僅存於Beta版中。

網景發佈稱為「Netstripe」的Netscape Navigator 9預設佈景主題給Firefox
2.0使用\[5\]，在Netscape Navigator 9中此佈景主題則直接被稱為「Netscape」。

和Netscape Browser 8一樣，Netscape Navigator 9建構且修改自受歡迎的Mozilla Firefox
2.0，而且完整支援所有的Firefox附加元件和外掛程式，其中也有部份來自網景本身提供。網景亦以擴充套件的形式發布了Netscape
Navigator
9部分特色供Firefox使用\[6\]，包括[站內信箱提醒器與好友活動資方塊列](https://web.archive.org/web/20070106101125/http://blog.netscape.com/2006/12/06/browser-extensions/)、[Digg追蹤器](https://web.archive.org/web/20070824131612/http://blog.netscape.com/2006/12/08/the-netscape-digg-tracker/)等。

其他修正包括近30種的常見網址拼字錯誤\[7\]、針對FTP的介面有重新設計\[8\]和整合於原Netscape.com的提供新聞選單。Netscape
Navigator 9也重回[跨平台的設計](../Page/跨平台.md "wikilink")：包含[Mac OS
X](../Page/Mac_OS_X.md "wikilink")、[Linux和](../Page/Linux.md "wikilink")[Windows版](../Page/Windows.md "wikilink")\[9\]。

### 移除的功能

不同於Netscape Browser，該瀏覽器不包含[Internet
Explorer的](../Page/Internet_Explorer.md "wikilink")[Trident排版引擎](../Page/Trident_\(排版引擎\).md "wikilink")。Netscape
Navigator
9並沒有包含任何[電子郵件客戶端](../Page/電子郵件客戶端.md "wikilink")、[新聞群組軟體和](../Page/新聞群組.md "wikilink")[即時通訊軟體](../Page/即時通訊軟體.md "wikilink")。不過，網景之前曾製作一個稱為[Netscape
Mercury的郵件程式輔助Netscape](../Page/Netscape_Messenger_9.md "wikilink")
Navigator 9\[10\]\[11\]。在Netscape Mercury上市之前，網景建議用戶先使用Netscape
7的郵件程式\[12\]。後來網景於11月15日公佈[Netscape Messenger
9的alpha](../Page/Netscape_Messenger_9.md "wikilink")1版，這即是原來計畫的Netscape
Mercury\[13\]，隨著網景停止Netscape Navigator 9的更新計畫，這個軟體的後續研發也無疾而終。

### 其他特色

知名的彩蛋[Mozilla之書在Netscape](../Page/Mozilla之書#Mozilla之書，8:20.md "wikilink")
Navigator 9出現新的一章。內容是*"And thus the Creator looked upon the beast reborn
and saw that it was
good."*，中譯為*「然後接著造物者凝視著那野獸並看著它茁壯。」*如果想要看到這個畫面在網址列中輸入<about:mozilla>即可。

因應社群的意見，Netscape Navigator 9舉辦了啟動畫面（Splash Screen）的徵選\[14\]，最後由Mario
Herbert製作的啟動畫面得獎\[15\]，並且於RC1版中正式啟用\[16\]。

## 版本歷史

網景於2007年1月23日公開要製作Netscape Navigator
9的情報。2007年6月5日此程式公開了第一個beta版本\[17\]，並且於10月15日推出正式版\[18\]，但是突然宣布將於2008年2月1日起停止支援所有的瀏覽器\[19\]，之後又宣佈這個期限延到同年的3月1日\[20\]。並且在2月20日发布了9.0.0.6版后，宣布停止更新，並提供轉換工具及建议用户改用系出同門的[Firefox和](../Page/Firefox.md "wikilink")[Flock浏览器](../Page/Flock.md "wikilink")\[21\]。

| 版號        | 發布日期        | 基於Mozilla Firefox版本 | 說明                                             |
| --------- | ----------- | ------------------- | ---------------------------------------------- |
| 9.0 beta1 | 2007年6月5日   | 2.0.0.4             |                                                |
| 9.0 beta2 | 2007年7月13日  | 2.0.0.4             | 修正beta1的數個錯誤                                   |
| 9.0 beta3 | 2007年8月15日  | 2.0.0.6             | 對於分頁瀏覽功能的加強等                                   |
| 9.0 RC1   | 2007年10月1日  | 2.0.0.7             | 移除社群功能等                                        |
| 9.0       | 2007年10月15日 | 2.0.0.7             | 正式版                                            |
| 9.0.0.1   | 2007年10月22日 | 2.0.0.8             | 隨Firefox安全性修正更新                                |
| 9.0.0.2   | 2007年11月1日  | 2.0.0.8             | 修改搜尋引擎和移除回報臭蟲按鈕                                |
| 9.0.0.3   | 2007年11月2日  | 2.0.0.9             | 隨Firefox安全性修正更新                                |
| 9.0.0.4   | 2007年11月27日 | 2.0.0.10            | 隨Firefox安全性修正更新                                |
| 9.0.0.5   | 2007年12月10日 | 2.0.0.11            | 隨Firefox安全性修正更新                                |
| 9.0.0.6   | 2008年2月20日  | 2.0.0.12            | 隨Firefox安全性修正更新，新增轉移設定檔到Firefox和Flock的套件\[22\] |

## 參考文獻

## 外部連結

  - [MozTW的Netscape討論版](http://forum.moztw.org/viewforum.php?f=5)
  - [MozTW的Netscape FAQ頁面](http://wiki.moztw.org/index.php/Netscape_FAQ)

## 相關文章

  - [Netscape](../Page/Netscape.md "wikilink")
  - [網景 (瀏覽器)](../Page/網景_\(瀏覽器\).md "wikilink")
  - [Netscape Messenger 9](../Page/Netscape_Messenger_9.md "wikilink")
  - [Mozilla Firefox](../Page/Mozilla_Firefox.md "wikilink")
  - [网页浏览器列表](../Page/网页浏览器列表.md "wikilink")
  - [网页浏览器比较](../Page/网页浏览器比较.md "wikilink")

[Category:基於Mozilla
Firefox的網頁瀏覽器](../Category/基於Mozilla_Firefox的網頁瀏覽器.md "wikilink")
[Category:Netscape](../Category/Netscape.md "wikilink")
[Category:2007年軟體](../Category/2007年軟體.md "wikilink")
[Category:網頁瀏覽器](../Category/網頁瀏覽器.md "wikilink")

1.

2.

3.  [Netscape 9－2007年1月23日通告](https://web.archive.org/web/20071015194947/http://community.netscape.com/n/pfx/forum.aspx?tsn=1&nav=messages&webtag=ws-nscpbrowser&tid=6418)
    造訪於 2007年[1月24日](../Page/1月24日.md "wikilink")

4.  [Netscape 9－2007年2月20日通告](https://web.archive.org/web/20071015195013/http://community.netscape.com/n/pfx/forum.aspx?tsn=1&nav=messages&webtag=ws-nscpbrowser&tid=6926)
    造訪於 2007年[2月20日](../Page/2月20日.md "wikilink")

5.

6.

7.

8.

9.

10.

11.

12.

13.

14.

15.

16.

17.

18.

19.

20.

21.

22.