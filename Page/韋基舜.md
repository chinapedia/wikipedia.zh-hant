**韋基舜**（）籍貫[廣東](../Page/廣東.md "wikilink")[三水](../Page/三水.md "wikilink")，[香港商人及體育界人士](../Page/香港.md "wikilink")，曾任《[天天日報](../Page/天天日報.md "wikilink")》社長，以及擔任多個體育組織主席等職位，有「韋八少」及「韋翁」稱號。

## 簡歷

韋基舜出身富裕家族，家族的[二天堂藥廠經營製藥業務](../Page/二天堂藥廠.md "wikilink")。他早年於[美國](../Page/美國.md "wikilink")[百蘭尼大學取得](../Page/百蘭尼大學.md "wikilink")[經濟碩士學位](../Page/經濟.md "wikilink")，大學畢業後於1956年從美國返港。其家族開設的二天堂印務，因印刷機未盡其用，決定出版報紙。1960年，韋基舜的兄長[韋基澤創辦](../Page/韋基澤.md "wikilink")《天天日報》及《[南華晚報](../Page/南華晚報.md "wikilink")》，並由韋基舜擔任社長。當時《天天日報》號稱為全世界首份彩色[柯式印刷的報紙](../Page/柯式印刷.md "wikilink")，曾經有不錯的銷量。1977年，韋氏家族以160萬港元把《天天日報》售予[劉天就](../Page/劉天就.md "wikilink")。

他又曾在1978年，參演[佳藝電視劇集](../Page/佳藝電視.md "wikilink")《[名流情史](../Page/名流情史.md "wikilink")》，飾演男主角「孫達生」，但因為其演技木無表情，卻被觀眾謔稱為「港版[三木武夫](../Page/三木武夫.md "wikilink")」。1980年代末，出任[香港電台電視部節目](../Page/香港電台.md "wikilink")《[城市論壇](../Page/城市論壇.md "wikilink")》主持。

## 體育

韋基舜熱心於體育事務，1956年，年僅23歲時已競逐[香港籃球總會主席職位](../Page/香港籃球總會.md "wikilink")，但因與對手同票，經抽籤而落敗，同年出任[東方體育會籃球部主任](../Page/東方體育會.md "wikilink")，1958年則出任[香港乒乓球總會及](../Page/香港乒乓球總會.md "wikilink")[東華體育會主席](../Page/東華體育會.md "wikilink")。同年在[香港電台擔任](../Page/香港電台.md "wikilink")[1958年亞運會](../Page/1958年亞運會.md "wikilink")[乒乓球賽評述員](../Page/乒乓球.md "wikilink")。近年他亦擔任[香港足球總會副會長](../Page/香港足球總會.md "wikilink")。

他對[拳擊尤其熱愛](../Page/拳擊.md "wikilink")，1963年至1964年出任[香港拳擊總會主席](../Page/香港拳擊總會.md "wikilink")。1960至1970年代期間亦曾擔任多屆[香港節體育項目委員會主席](../Page/香港節.md "wikilink")。1969年成立的香港[中國國術總會任首席顧問](../Page/中國國術總會.md "wikilink")。他又曾多次擔任電視體育節目，包括[奧運會等的主持人](../Page/奧運會.md "wikilink")。2001年他亦曾任支持足球博彩合法化同盟召集人。
[缩略图](https://zh.wikipedia.org/wiki/File:WKSew1959.JPG "fig:缩略图")

## 公職

韋基舜歷任港區[全國人大代表](../Page/全國人大.md "wikilink")、[全國政協委員](../Page/全國政協.md "wikilink")，亦曾任[香港特別行政區第一屆政府推選委員會委員](../Page/香港特別行政區第一屆政府推選委員會.md "wikilink")，選出第一任[行政長官和](../Page/香港特別行政區行政長官.md "wikilink")[臨時立法會](../Page/臨時立法會.md "wikilink")。

## 寫作

近年他在多份香港報章雜誌撰寫專欄，內容主要為香港歷史掌故，如《[成報](../Page/成報.md "wikilink")》的《吾土吾情》。作品結集成多本作品。他亦曾因香港歷史相關問題，多次與其他歷史學者以至[古物古蹟辦事處爆發筆戰](../Page/古物古蹟辦事處.md "wikilink")，如他曾力證[孫中山先生曾被香港殖民地政府拘禁於](../Page/孫中山.md "wikilink")[域多利道扣押中心](../Page/域多利道扣押中心.md "wikilink")，並曾被驅逐出境；又認為把[甘棠第改裝成為孫中山博物館是](../Page/甘棠第.md "wikilink")「不尊重歷史之舉」。

## 作品

  - 《[匠心錄](../Page/匠心錄.md "wikilink")》
  - 《[都市傳奇人](../Page/都市傳奇人.md "wikilink")》
  - 《[邁向九七](../Page/邁向九七.md "wikilink")》
  - 《[吾土吾情](../Page/吾土吾情.md "wikilink")》
  - 《[韋基舜精解黃大仙百籤](../Page/韋基舜精解黃大仙百籤.md "wikilink")》

## 公職

  - [東華三院戊戌年](../Page/東華三院.md "wikilink")（1958年）總理
  - [保良局總理](../Page/保良局.md "wikilink")
  - [香港拳擊總會主席](../Page/香港拳擊總會.md "wikilink")
  - [香港乒乓球總會主席](../Page/香港乒乓球總會.md "wikilink")
  - [東華體育會主席](../Page/東華體育會.md "wikilink")
  - [華協會主席](../Page/香港中華業餘體育協會.md "wikilink")
  - [香港足球總會副會長](../Page/香港足球總會.md "wikilink")
  - [香港泰拳理事會永遠榮譽會長](../Page/香港泰拳理事會.md "wikilink")

## 榮譽

  - [銀紫荊星章](../Page/銀紫荊星章.md "wikilink") (2002年)

## 電視節目

  - 1977年：[並無虛言](../Page/並無虛言.md "wikilink")
    ([香港電台](../Page/香港電台.md "wikilink"))
  - 1978年：[對症下藥](../Page/對症下藥.md "wikilink")
    ([香港電台](../Page/香港電台.md "wikilink"))
  - 1978年：[名流情史](../Page/名流情史.md "wikilink") 飾 孫達生
    ([佳藝電視](../Page/佳藝電視.md "wikilink"))
  - 1978年：[浣花洗劍錄](../Page/浣花洗劍錄_\(1978年電視劇\).md "wikilink") 飾 木郎君
    ([麗的電視](../Page/麗的電視.md "wikilink"))
  - 1990年-1996年：[城市論壇](../Page/城市論壇.md "wikilink")
    ([香港電台](../Page/香港電台.md "wikilink"))

## 資料來源

  - [韋基舜 體育難騰飛
    錯在走彎路](https://web.archive.org/web/20070314022032/http://www.wenweipo.com/news.phtml?news_id=MR0510050001&loc=any&cat=114MR&no_combo=1)
    文匯報2005年10月5日
  - [作家簡介](http://www.hkauthors.com.hk/authors.php?intro=2)
  - [吾土吾情：全球第一份彩色新聞報紙](http://www.singpao.com/20051005/feature/766285_main.html)
    成報2005年10月5日
  - [富貴閒人博覽群書](https://web.archive.org/web/20050507155914/http://www.singpao.com/20050222/local/678036.html)
    成報，2005年2月22日
  - [韋基舜嘆政府錯配歷史 見證大時代
    真相我最知](https://web.archive.org/web/20070311002137/http://www.singpao.com/20050222/local/678029.html)
    成報，2005年2月22日
  - [勳銜頒授典禮](http://www.info.gov.hk/gia/general/200210/12/1012161.htm)
    2002年10月

[Category:三水人](../Category/三水人.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:香港作家](../Category/香港作家.md "wikilink")
[Category:香港主持人](../Category/香港主持人.md "wikilink")
[Category:香港足球界人士](../Category/香港足球界人士.md "wikilink")
[Category:前香港全國人大代表](../Category/前香港全國人大代表.md "wikilink")
[W](../Category/香港華仁書院校友.md "wikilink")
[Category:東華三院總理](../Category/東華三院總理.md "wikilink")
[K](../Category/韋姓.md "wikilink")
[Category:新法書院校友](../Category/新法書院校友.md "wikilink")
[Category:香港建制派人士](../Category/香港建制派人士.md "wikilink")