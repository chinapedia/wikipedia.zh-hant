**《宝葫芦的秘密》**是中国[儿童文学家](../Page/儿童文学家.md "wikilink")[张天翼](../Page/张天翼.md "wikilink")[童话作品](../Page/童话.md "wikilink")，1958年发行。

## 电影改编

《宝葫芦的秘密》曾经两次被搬上银幕。

1963年[上海](../Page/上海.md "wikilink")[天马电影制片厂摄制的黑白影片](../Page/天马电影制片厂.md "wikilink")《宝葫芦的秘密》。2007年中国电影集团公司拍摄彩色影片《宝葫芦的秘密》，影片中加入了许多[电脑特效](../Page/电脑特效.md "wikilink")。

## 情节介绍

故事讲述小学生王葆是一个活泼好动、富于幻想的儿童。当碰到困难的时候就幻想有能有一个宝贝来帮他。一天，王葆在钓鱼时遇到宝葫芦，宝葫芦要王葆为其保守秘密并答应帮助王葆。然而王葆发现宝葫芦并不能创造东西，而是只能把王葆想要的东西[瞬間移動到王葆的身上或者家中](../Page/瞬間移動.md "wikilink")。王葆努力摆脱掉宝葫芦，但宝葫芦并不容易摆脱。

### 2007年电影情节

王葆遇到宝葫芦大喜过望，在宝葫芦的帮助下，他在学校的表现是突飞猛进，让同学们大为惊诧。在宝葫芦帮助下，他加入了校游泳队。可是，王葆渐渐发现宝葫芦原来只懂盲目服从，最后在数学考试中帮王葆把同学试卷的答案与名字都抄了过来，结果被老师批评。这一事件最终导致王葆和宝葫芦绝裂，凭自己的实力帮校游泳队赢得区小学生游泳大赛的冠军。

## 电影主要演职员

### 1963年版

  - 王葆：徐方 饰
  - 宝葫芦：[茂路饰](../Page/茂路.md "wikilink")
  - 摄像：[石凤岐](../Page/石凤岐.md "wikilink")
  - 美术：[王月白](../Page/王月白.md "wikilink")

### 2007年版

  - 王葆：[朱祺隆饰](../Page/朱祺隆.md "wikilink")
  - 宝葫芦：[陈佩斯配音](../Page/陈佩斯.md "wikilink")
  - 刘老师：[梁咏琪饰](../Page/梁咏琪.md "wikilink")
  - 蘇鳴鳳：[魏莱飾](../Page/魏莱.md "wikilink")
  - 楊栓：[鄭嘉昊飾](../Page/鄭嘉昊.md "wikilink")
  - 鄭小登：[王嘉堃飾](../Page/王嘉堃.md "wikilink")
  - 姚俊：[勞奕嘉飾](../Page/勞奕嘉.md "wikilink")
  - 父親：[郭凱敏飾](../Page/郭凱敏.md "wikilink")
  - 母親：[何晴飾](../Page/何晴.md "wikilink")
  - 奶奶：[孟謙飾](../Page/孟謙.md "wikilink")
  - 妺妺：[胡倩琳飾](../Page/胡倩琳.md "wikilink")
  - 游泳教練：[龍德飾](../Page/龍德.md "wikilink")

## 參考資料

  - [《宝葫芦》曾风靡全国
    群众小演员一天"片酬"两块烧饼](http://media.people.com.cn/n/2013/1231/c40606-23984647.html)
  - [《宝葫芦的秘密》](http://news.southcn.com/dishi/jiangmen/content/2012-04/09/content_42578886.htm)
  - [《宝葫芦的秘密》首映
    中国“土葫芦”变洋卡通](http://ent.sina.com.cn/m/c/2007-06-26/12041613546.shtml)

[B](../Category/儿童文学.md "wikilink") [B](../Category/迪士尼電影.md "wikilink")
[B](../Category/2007年電腦動畫電影.md "wikilink")
[B](../Category/童話故事改編電影.md "wikilink")
[B](../Category/2007年劇情片.md "wikilink")
[B](../Category/中國劇情片.md "wikilink")
[B](../Category/真人動畫電影.md "wikilink")
[Category:中国大陆动画电影](../Category/中国大陆动画电影.md "wikilink")
[Category:植物題材電影](../Category/植物題材電影.md "wikilink")
[Category:蔬菜題材作品](../Category/蔬菜題材作品.md "wikilink")