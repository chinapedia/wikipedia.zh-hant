**下屋則子**，[日本女性](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")。隸屬於[81
Produce](../Page/81_Produce.md "wikilink")。出生於[千葉縣](../Page/千葉縣.md "wikilink")。愛稱為****、****等。在2015年4月22日當天，她不僅慶祝自己的生日，同時也發表結婚喜訊。男方不是聲優，而是一般的圈外人。\[1\]

## 主要參與作品

### 電視動畫

  - 2001年

<!-- end list -->

  -
  -
  - [哈姆太郎](../Page/哈姆太郎.md "wikilink")（）

  - [動感小子](../Page/動感小子.md "wikilink")

  -
  -
<!-- end list -->

  - 2002年

<!-- end list -->

  - [巷說百物語](../Page/巷說百物語.md "wikilink")（千代）

  - [初音島](../Page/初音島.md "wikilink")（繪美）

  - （青野香苗）

  - [東京地底奇兵](../Page/東京地底奇兵.md "wikilink")（）

  - [火影忍者](../Page/火影忍者.md "wikilink")（萌黃）

  - [小公主優希](../Page/小公主優希.md "wikilink")（女孩子、學生）

  - [炎之蜃氣樓](../Page/炎之蜃氣樓.md "wikilink")（學生）

  - [魔法咪路咪路](../Page/魔法咪路咪路.md "wikilink")（漢吉、梅園桃）

<!-- end list -->

  - 2003年

<!-- end list -->

  - [真珠美人魚](../Page/真珠美人魚.md "wikilink")（咪咪）
  - [陽光伴我行](../Page/陽光伴我行.md "wikilink")（）（早乙女直）
  - [愛的魔法](../Page/愛的魔法.md "wikilink")（鳴尾來花）
  - [洛克人EXE](../Page/洛克人EXE.md "wikilink")（）

<!-- end list -->

  - 2004年

<!-- end list -->

  - [變異體少女](../Page/變異體少女.md "wikilink")（孤兒院的少女）

  - [神無月的巫女](../Page/神無月的巫女.md "wikilink")（**来栖川姬子**）

  - [今天是魔王](../Page/魔之系列#電視動畫.md "wikilink")（）

  -
  - [老師的時間](../Page/老師的時間.md "wikilink")（女學生A、想像的老師）

  - [BLEACH](../Page/BLEACH.md "wikilink")（紬屋雨）

  -
  - [瑪莉亞的凝望](../Page/瑪莉亞的凝望.md "wikilink")（桂）

  - [洛克人AXESS](../Page/洛克人EXE.md "wikilink")（）

<!-- end list -->

  - 2005年

<!-- end list -->

  - [幸運女神](../Page/幸運女神.md "wikilink")（聖）
  - [英國戀物語 艾瑪](../Page/英國戀物語_艾瑪.md "wikilink")（柯林·瓊斯）
  - [玻璃假面](../Page/玻璃假面.md "wikilink")（田中圭子）
  - [地獄少女](../Page/地獄少女.md "wikilink")（上川美紀）
  - [初音島 第二季](../Page/初音島.md "wikilink")（日野圓）
  - [TSUBASA翼](../Page/TSUBASA翼.md "wikilink")（琴子）
  - [洛克人EXE BEAST](../Page/洛克人EXE.md "wikilink")（）

<!-- end list -->

  - 2006年

<!-- end list -->

  - [幸運女神](../Page/幸運女神.md "wikilink")（聖）

  - [飛輪少年](../Page/飛輪少年.md "wikilink")（富田毬（小迷糊老師））

  - [偶像宣言](../Page/偶像宣言.md "wikilink")（藤堂吹雪）

  - （チェルシィ）

  - [D.Gray-man](../Page/D.Gray-man.md "wikilink")（）

  - [Fate/stay night](../Page/Fate/stay_night.md "wikilink")（**間桐櫻**）

  - [不可思議星球的雙胞胎公主 Gyu\!](../Page/不可思議星球的雙胞胎公主.md "wikilink")（蘿絲瑪莉）

  - [武裝鍊金](../Page/武裝鍊金.md "wikilink")（河井沙織）

  - [洛克人EXE BEAST+](../Page/洛克人EXE.md "wikilink")（）

  - [銀魂](../Page/銀魂_\(動畫\).md "wikilink")（少女）※第21話

<!-- end list -->

  - 2007年

<!-- end list -->

  - [京四郎與永遠的天空](../Page/京四郎與永遠的天空.md "wikilink")（日美子）
  - [守護甜心](../Page/守護甜心.md "wikilink")（姬川舞香）
  - [Venus Versus Virus](../Page/Venus_Versus_Virus.md "wikilink")（京子）
  - [花鈴的魔法戒](../Page/花鈴的魔法戒.md "wikilink")（九條姬香）
  - [英國戀物語 艾瑪第二幕](../Page/英國戀物語_艾瑪.md "wikilink")（柯林·瓊斯、伊爾絲·莫爾德斯）

<!-- end list -->

  - 2008年

<!-- end list -->

  - [我的狐仙女友](../Page/我的狐仙女友.md "wikilink")（砂原幾）
  - [狂亂家族日記](../Page/狂亂家族日記.md "wikilink")（美露卡哆比）
  - [武裝機甲](../Page/武裝機甲.md "wikilink")（新山理沙子）

<!-- end list -->

  - 2009年

<!-- end list -->

  - [黑神](../Page/黑神.md "wikilink")（**黑**）
  - [四葉遊戲](../Page/四葉遊戲.md "wikilink")（月島紅葉）

<!-- end list -->

  - 2011年

<!-- end list -->

  - [GOSICK](../Page/GOSICK.md "wikilink")（**艾薇兒·布萊德利**）
  - [IS〈Infinite
    Stratos〉](../Page/IS〈Infinite_Stratos〉.md "wikilink")（山田
    真耶）
  - [這樣算是殭屍嗎？](../Page/這樣算是殭屍嗎？.md "wikilink")（京子）
  - [Fate/Zero](../Page/Fate/Zero.md "wikilink")（間桐櫻）
  - [魔劍姬！](../Page/魔劍姬！.md "wikilink")（**天谷春戀**）

<!-- end list -->

  - 2012年

<!-- end list -->

  - [這樣算是殭屍嗎？OF THE DEAD](../Page/這樣算是殭屍嗎？.md "wikilink")（京子）

<!-- end list -->

  - 2013年

<!-- end list -->

  - [名偵探柯南](../Page/名偵探柯南.md "wikilink")（笹森薫 \#689）
  - [IS〈Infinite
    Stratos〉2](../Page/IS〈Infinite_Stratos〉.md "wikilink")（山田真耶）
  - [神奇寶貝XY](../Page/神奇寶貝XY.md "wikilink")（小堇）

<!-- end list -->

  - 2014年

<!-- end list -->

  - [魔女的使命](../Page/魔女的使命.md "wikilink")（乙女橘璃音）
  - [魔劍姬！通](../Page/魔劍姬！.md "wikilink")（**天谷春戀**）
  - [Fate/stay
    night（ufotable版）](../Page/Fate/stay_night.md "wikilink")（**間桐櫻**）
  - [-{zh-cn:临时女友;zh-tw:女友伴身邊}-](../Page/臨時女友.md "wikilink")（玉井麗巳）

<!-- end list -->

  - 2016年

<!-- end list -->

  - [熱情傳奇 The X](../Page/熱情傳奇.md "wikilink")（**萊菈**）
  - [Fate/kaleid liner 魔法少女☆伊莉雅
    3rei\!\!](../Page/Fate/kaleid_liner_魔法少女☆伊莉雅.md "wikilink")（黑色女子）
  - [Regalia 三聖星](../Page/Regalia_三聖星.md "wikilink")（真矢·埃斯托利亞）
  - [魔法少女育成計劃](../Page/魔法少女育成計劃.md "wikilink")（小雪的母親）
  - [見習神仙 秘密的心靈](../Page/見習神仙_秘密的心靈.md "wikilink")（霍特妮、優香）

<!-- end list -->

  - 2017年

<!-- end list -->

  - [TRICKSTER
    －來自江戶川亂步「少年偵探團」－](../Page/TRICKSTER_－來自江戶川亂步「少年偵探團」－.md "wikilink")（小林七美）
  - [黑色推銷員NEW](../Page/黑色推銷員.md "wikilink")（妻）

<!-- end list -->

  - 2018年

<!-- end list -->

  - [Fate/EXTRA Last Encore](../Page/Fate/EXTRA.md "wikilink")（**間桐櫻**）

### OVA

  - [大魔法峠](../Page/大魔法峠.md "wikilink")（国鉄子）
  - [BALDR FORCE EXE
    RESOLUTION](../Page/BALDR_FORCE_EXE_RESOLUTION.md "wikilink")（水坂憐）
  - [Carnival
    Phantasm](../Page/Carnival_Phantasm.md "wikilink")（間桐櫻、聖杯君）

<!-- end list -->

  - 2014年

<!-- end list -->

  - [IS〈Infinite
    Stratos〉世界消除篇](../Page/IS〈Infinite_Stratos〉.md "wikilink")（山田真耶）

### 劇場版動畫

  - 2010年

<!-- end list -->

  - [劇場版 Fate/stay night UNLIMITED BLADE
    WORKS](../Page/Fate/stay_night.md "wikilink")（間桐櫻）

<!-- end list -->

  - 2017年

<!-- end list -->

  - [劇場版 Fate/kaleid liner 魔法少女☆伊莉雅
    雪下的誓言](../Page/Fate/kaleid_liner_魔法少女☆伊莉雅_雪下的誓言.md "wikilink")（**間桐櫻**）
  - [劇場版 Fate/stay night Heaven's Feel I.presage
    flower](../Page/Fate/stay_night.md "wikilink")（**間桐櫻**）

<!-- end list -->

  - 2019年

<!-- end list -->

  - [劇場版 Fate/stay night Heaven's Feel II.lost
    butterfly](../Page/Fate/stay_night.md "wikilink")（**間桐櫻**）

### 网路动画

  - 2018年

<!-- end list -->

  - [衛宮家今天的餐桌風景](../Page/衛宮家今天的餐桌風景.md "wikilink")（間桐櫻）

### 遊戲

  -
  - [Ever17 —the out of infinity—](../Page/時空輪迴.md "wikilink")（**田中優** ）

  -
  -
  - Final Fantasy II（PS版）（）

  - [魔法咪路咪路 對戰魔法球](../Page/魔法咪路咪路.md "wikilink")

  - [魔法咪路咪路 心跳回憶大恐慌](../Page/魔法咪路咪路.md "wikilink")

  - [DUEL SAVIOR DESTINY](../Page/DUEL_SAVIOR.md "wikilink")（當真未亞）

  - [半熟英雄対3D](../Page/半熟英雄.md "wikilink")（）

  - [Fate/tiger
    colosseum](../Page/Fate/tiger_colosseum.md "wikilink")（**間桐櫻**）

  - [Fate/EXTRA CCC](../Page/Fate/EXTRA_CCC.md "wikilink")（**間桐櫻**、BB）

  - [Fate/unlimited
    codes](../Page/Fate/unlimited_codes.md "wikilink")（**間桐櫻**）

  - [Fate/Grand
    Order](../Page/Fate/Grand_Order.md "wikilink")（BB、帕爾瓦蒂、伽摩）

### 電台節目

  - ラジオどっとあい 下屋則子のあったかデリバリー（文化放送インターネットラジオ：2002年10月－12月）
  - ラジオ神無月（TE-A room：2004年10月－2005年3月）

### CD

  - AFC-3012 2004/7/23（真紅杏樹）

  - [瑪莉亞的凝望](../Page/瑪莉亞的凝望.md "wikilink") 集英社廣播劇CD SCD905576 2004/01（桂）

  - [瑪莉亞的凝望 黄薔薇革命](../Page/瑪莉亞的凝望.md "wikilink") 集英社廣播劇CD SCD905665
    2004/07（桂）

  - [瑪莉亞的凝望 荊棘之森](../Page/瑪莉亞的凝望.md "wikilink") 集英社廣播劇CD SCD905807
    2004/10（桂）

  - [嬉皮笑園](../Page/嬉皮笑園.md "wikilink") 嬉皮笑園～愛與青春的演劇祭！～編（）FCCC-0026
    2005/02/25（6号）

  - [武裝鍊金](../Page/武裝鍊金.md "wikilink") 集英社廣播劇CD SCD901143
    2006/05/26（河合沙織）

## 参考資料

## 外部連結

  - [81
    Produce的介紹網站](https://web.archive.org/web/20070712033550/http://www.81produce.co.jp/list.cgi?lady+2231714582150)

  - [下屋則子 Official BLOG
    ～のりのり♪Diary～](http://yaplog.jp/norinori-diary/)（已在2010年4月21日停止更新）

  - [下屋則子 Official BLOG -
    GREE](http://gree.jp/shitaya_noriko)（自2010年4月21日至今）

[Category:日本女性配音員](../Category/日本女性配音員.md "wikilink")
[Category:千葉縣出身人物](../Category/千葉縣出身人物.md "wikilink")

1.