本列表是[中国全国重点文物保护单位的子列表](../Page/中国全国重点文物保护单位.md "wikilink")，列举在[广西壮族自治区境内的全国重点文物保护单位](../Page/广西壮族自治区.md "wikilink")。

<center>

<table>
<thead>
<tr class="header">
<th><p>名称</p></th>
<th><p>编号</p></th>
<th><p>分类</p></th>
<th><p>地点</p></th>
<th><p>时代</p></th>
<th><p>图片</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/金田起义地址.md" title="wikilink">金田起义地址</a></p></td>
<td><p>1-2</p></td>
<td><p>革命遗址及革命纪念建筑物</p></td>
<td><p><a href="../Page/桂平市.md" title="wikilink">桂平市</a></p></td>
<td><p>1851年</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/经略台真武阁.md" title="wikilink">经略台真武阁</a></p></td>
<td><p>2-33</p></td>
<td><p>古建筑及历史纪念建筑物</p></td>
<td><p><a href="../Page/容县.md" title="wikilink">容县</a></p></td>
<td><p><a href="../Page/明.md" title="wikilink">明</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/程阳永济桥.md" title="wikilink">程阳永济桥</a></p></td>
<td><p>2-42</p></td>
<td><p>古建筑及历史纪念建筑物</p></td>
<td><p><a href="../Page/三江县.md" title="wikilink">三江县</a></p></td>
<td><p><a href="../Page/民国时期.md" title="wikilink">民国</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Chengyangqiao,_Guangxi,_China.jpg" title="fig:Chengyangqiao,_Guangxi,_China.jpg">Chengyangqiao,_Guangxi,_China.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/中国工农红军第七军、第八军军部旧址.md" title="wikilink">中国工农红军第七军、第八军军部旧址</a></p></td>
<td><p>3-28</p></td>
<td><p>革命遗址及革命纪念建筑物</p></td>
<td><p><a href="../Page/百色市.md" title="wikilink">百色市</a></p></td>
<td><p>1929—1930年</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/灵渠.md" title="wikilink">灵渠</a></p></td>
<td><p>3-54</p></td>
<td><p>古建筑及历史纪念建筑物</p></td>
<td><p><a href="../Page/兴安县.md" title="wikilink">兴安县</a></p></td>
<td><p><a href="../Page/秦朝.md" title="wikilink">秦</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Lingqu_Canal.jpg" title="fig:Lingqu_Canal.jpg">Lingqu_Canal.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/大士阁.md" title="wikilink">大士阁</a></p></td>
<td><p>3-103</p></td>
<td><p>古建筑及历史纪念建筑物</p></td>
<td><p><a href="../Page/合浦县.md" title="wikilink">合浦县</a></p></td>
<td><p><a href="../Page/明.md" title="wikilink">明</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Dashi_Pavilion_S.jpg" title="fig:Dashi_Pavilion_S.jpg">Dashi_Pavilion_S.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/花山岩画.md" title="wikilink">花山岩画</a></p></td>
<td><p>3-165</p></td>
<td><p>石刻及其他</p></td>
<td><p><a href="../Page/宁明县.md" title="wikilink">宁明县</a></p></td>
<td><p><a href="../Page/战国.md" title="wikilink">战国</a>—<a href="../Page/东汉.md" title="wikilink">东汉</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Rock_painting_hua_mountain_1.jpg" title="fig:Rock_painting_hua_mountain_1.jpg">Rock_painting_hua_mountain_1.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/合浦汉墓群.md" title="wikilink">合浦汉墓群</a></p></td>
<td><p>4-65</p></td>
<td><p>古墓葬</p></td>
<td><p><a href="../Page/合浦县.md" title="wikilink">合浦县</a></p></td>
<td><p><a href="../Page/汉朝.md" title="wikilink">汉</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/莫土司衙署.md" title="wikilink">莫土司衙署</a></p></td>
<td><p>4-153</p></td>
<td><p>古建筑</p></td>
<td><p><a href="../Page/忻城县.md" title="wikilink">忻城县</a></p></td>
<td><p><a href="../Page/明.md" title="wikilink">明</a>、<a href="../Page/清.md" title="wikilink">清</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/靖江王府.md" title="wikilink">靖江王府及</a><a href="../Page/靖江王陵.md" title="wikilink">王陵</a></p></td>
<td><p>4-154</p></td>
<td><p>古建筑</p></td>
<td><p><a href="../Page/桂林市.md" title="wikilink">桂林市</a></p></td>
<td><p><a href="../Page/明.md" title="wikilink">明</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Guilin_Jingjiang_Wangfu_2012.09.28_12-13-34.jpg" title="fig:Guilin_Jingjiang_Wangfu_2012.09.28_12-13-34.jpg">Guilin_Jingjiang_Wangfu_2012.09.28_12-13-34.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李宗仁故居.md" title="wikilink">李宗仁故居</a><br />
（含<a href="../Page/李宗仁官邸.md" title="wikilink">李宗仁官邸</a>）</p></td>
<td><p>4-223</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/临桂县.md" title="wikilink">临桂县</a></p></td>
<td><p>1921、1948年</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Guilin_Li_Zongren_Guandi_2012.09.29_10-13-14.jpg" title="fig:Guilin_Li_Zongren_Guandi_2012.09.29_10-13-14.jpg">Guilin_Li_Zongren_Guandi_2012.09.29_10-13-14.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/李济深故居.md" title="wikilink">李济深故居</a></p></td>
<td><p>4-224</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/苍梧县.md" title="wikilink">苍梧县</a></p></td>
<td><p><a href="../Page/民国时期.md" title="wikilink">民国</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/右江工农民主政府旧址.md" title="wikilink">右江工农民主政府旧址</a></p></td>
<td><p>4-231</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/田东县.md" title="wikilink">田东县</a></p></td>
<td><p>1929年</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/八路军桂林办事处旧址.md" title="wikilink">八路军桂林办事处旧址</a></p></td>
<td><p>4-240</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/桂林市.md" title="wikilink">桂林市</a></p></td>
<td><p>1938年</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Balujun_Guilin_Banshichu_Jiuzhi.JPG" title="fig:Balujun_Guilin_Banshichu_Jiuzhi.JPG">Balujun_Guilin_Banshichu_Jiuzhi.JPG</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/百谷和高岭坡遗址.md" title="wikilink">百谷和高岭坡遗址</a></p></td>
<td><p>5-99</p></td>
<td><p>古遗址</p></td>
<td><p><a href="../Page/百色市.md" title="wikilink">百色市</a></p></td>
<td><p><a href="../Page/旧石器时代.md" title="wikilink">旧石器时代</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/甑皮岩遗址.md" title="wikilink">甑皮岩遗址</a></p></td>
<td><p>5-100</p></td>
<td><p>古遗址</p></td>
<td><p><a href="../Page/桂林市.md" title="wikilink">桂林市</a></p></td>
<td><p><a href="../Page/新石器时代.md" title="wikilink">新石器时代</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Guilin_Zengpiyan_Yizhi_2012.10.03_15-57-59.jpg" title="fig:Guilin_Zengpiyan_Yizhi_2012.10.03_15-57-59.jpg">Guilin_Zengpiyan_Yizhi_2012.10.03_15-57-59.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/顶蛳山遗址.md" title="wikilink">顶蛳山遗址</a></p></td>
<td><p>5-101</p></td>
<td><p>古遗址</p></td>
<td><p><a href="../Page/南宁市.md" title="wikilink">南宁市</a></p></td>
<td><p><a href="../Page/新石器时代.md" title="wikilink">新石器时代</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/岜团桥.md" title="wikilink">岜团桥</a></p></td>
<td><p>5-378</p></td>
<td><p>古建筑</p></td>
<td><p><a href="../Page/三江县.md" title="wikilink">三江县</a></p></td>
<td><p><a href="../Page/清.md" title="wikilink">清</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/临贺故城.md" title="wikilink">临贺故城</a></p></td>
<td><p>5-379</p></td>
<td><p>古建筑</p></td>
<td><p><a href="../Page/贺州市.md" title="wikilink">贺州市</a></p></td>
<td><p><a href="../Page/汉朝.md" title="wikilink">汉</a>—<a href="../Page/清.md" title="wikilink">清</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/桂林石刻.md" title="wikilink">桂林石刻</a></p></td>
<td><p>5-462</p></td>
<td><p>石窟寺及石刻</p></td>
<td><p><a href="../Page/桂林市.md" title="wikilink">桂林市</a></p></td>
<td><p><a href="../Page/唐朝.md" title="wikilink">唐</a>—<a href="../Page/清.md" title="wikilink">清</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Guilin_Longyinyan_Longyindong_Moya_Shike_2012.09.28_15-40-53.jpg" title="fig:Guilin_Longyinyan_Longyindong_Moya_Shike_2012.09.28_15-40-53.jpg">Guilin_Longyinyan_Longyindong_Moya_Shike_2012.09.28_15-40-53.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/北海近代建筑.md" title="wikilink">北海近代建筑</a></p></td>
<td><p>5-504</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/北海市.md" title="wikilink">北海市</a></p></td>
<td><p><a href="../Page/近代.md" title="wikilink">近代</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Church_of_shengtang_village_Weizhou_Island.jpg" title="fig:Church_of_shengtang_village_Weizhou_Island.jpg">Church_of_shengtang_village_Weizhou_Island.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/三宣堂.md" title="wikilink">刘永福</a>、<a href="../Page/冯子材旧居.md" title="wikilink">冯子材旧居建筑群</a></p></td>
<td><p>5-505</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/钦州市.md" title="wikilink">钦州市</a></p></td>
<td><p><a href="../Page/清.md" title="wikilink">清</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:三宣堂_2.jpg" title="fig:三宣堂_2.jpg">三宣堂_2.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/白莲洞遗址.md" title="wikilink">白莲洞遗址</a></p></td>
<td><p>6-169</p></td>
<td><p>古遗址</p></td>
<td><p><a href="../Page/柳州市.md" title="wikilink">柳州市</a></p></td>
<td><p><a href="../Page/旧石器时代.md" title="wikilink">旧石器至</a><a href="../Page/新石器时代.md" title="wikilink">新石器时代</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Liuzhou_Bailiandong_Yizhi_2014.03.01_14-57-39.jpg" title="fig:Liuzhou_Bailiandong_Yizhi_2014.03.01_14-57-39.jpg">Liuzhou_Bailiandong_Yizhi_2014.03.01_14-57-39.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鲤鱼嘴遗址.md" title="wikilink">鲤鱼嘴遗址</a></p></td>
<td><p>6-170</p></td>
<td><p>古遗址</p></td>
<td><p><a href="../Page/柳州市.md" title="wikilink">柳州市</a></p></td>
<td><p><a href="../Page/旧石器时代.md" title="wikilink">旧石器至</a><a href="../Page/新石器时代.md" title="wikilink">新石器时代</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Liuzhou_Liyuzui_Yizhi_2014.03.03_12-15-08.jpg" title="fig:Liuzhou_Liyuzui_Yizhi_2014.03.03_12-15-08.jpg">Liuzhou_Liyuzui_Yizhi_2014.03.03_12-15-08.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/感驮岩遗址.md" title="wikilink">感驮岩遗址</a></p></td>
<td><p>6-171</p></td>
<td><p>古遗址</p></td>
<td><p><a href="../Page/那坡县.md" title="wikilink">那坡县</a></p></td>
<td><p><a href="../Page/新石器时代.md" title="wikilink">新石器时代至</a><a href="../Page/战国.md" title="wikilink">战国</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/秦城遗址.md" title="wikilink">秦城遗址</a></p></td>
<td><p>6-172</p></td>
<td><p>古遗址</p></td>
<td><p><a href="../Page/兴安县.md" title="wikilink">兴安县</a></p></td>
<td><p><a href="../Page/秦朝.md" title="wikilink">秦至</a><a href="../Page/晋朝.md" title="wikilink">晋</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/智城城址.md" title="wikilink">智城城址</a></p></td>
<td><p>6-173</p></td>
<td><p>古遗址</p></td>
<td><p><a href="../Page/上林县.md" title="wikilink">上林县</a></p></td>
<td><p><a href="../Page/唐朝.md" title="wikilink">唐</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/江头村和长岗岭村古建筑群.md" title="wikilink">江头村和长岗岭村古建筑群</a></p></td>
<td><p>6-688</p></td>
<td><p>古建筑</p></td>
<td><p><a href="../Page/灵川县.md" title="wikilink">灵川县</a></p></td>
<td><p><a href="../Page/明.md" title="wikilink">明至</a><a href="../Page/民国时期.md" title="wikilink">民国</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Changgangling_relic.JPG" title="fig:Changgangling_relic.JPG">Changgangling_relic.JPG</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/马殷庙.md" title="wikilink">马殷庙</a></p></td>
<td><p>6-689</p></td>
<td><p>古建筑</p></td>
<td><p><a href="../Page/富川县.md" title="wikilink">富川县</a></p></td>
<td><p><a href="../Page/明.md" title="wikilink">明至</a><a href="../Page/清.md" title="wikilink">清</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/燕窝楼.md" title="wikilink">燕窝楼</a></p></td>
<td><p>6-690</p></td>
<td><p>古建筑</p></td>
<td><p><a href="../Page/全州县.md" title="wikilink">全州县</a></p></td>
<td><p><a href="../Page/明.md" title="wikilink">明至</a><a href="../Page/清.md" title="wikilink">清</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/恭城古建筑群.md" title="wikilink">恭城古建筑群</a></p>
<ul>
<li><a href="../Page/恭城文庙.md" title="wikilink">恭城文庙</a></li>
<li><a href="../Page/恭城武庙.md" title="wikilink">恭城武庙</a></li>
<li><a href="../Page/周渭祠.md" title="wikilink">周渭祠</a></li>
<li><a href="../Page/恭城湖南会馆.md" title="wikilink">湖南会馆</a></li>
</ul></td>
<td><p>6-691</p></td>
<td><p>古建筑</p></td>
<td><p><a href="../Page/恭城县.md" title="wikilink">恭城县</a></p></td>
<td><p><a href="../Page/明.md" title="wikilink">明至</a><a href="../Page/清.md" title="wikilink">清</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Gongcheng_Wumiao_2012.09.29_16-58-22.jpg" title="fig:Gongcheng_Wumiao_2012.09.29_16-58-22.jpg">Gongcheng_Wumiao_2012.09.29_16-58-22.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/柳侯祠.md" title="wikilink">柳侯祠碑刻</a></p></td>
<td><p>6-850</p></td>
<td><p>石窟寺及石刻</p></td>
<td><p><a href="../Page/柳州市.md" title="wikilink">柳州市</a></p></td>
<td><p><a href="../Page/宋朝.md" title="wikilink">宋至</a><a href="../Page/民国时期.md" title="wikilink">民国</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Liuzhou_Liuhou_Ci_2012.10.01_15-12-19.jpg" title="fig:Liuzhou_Liuhou_Ci_2012.10.01_15-12-19.jpg">Liuzhou_Liuhou_Ci_2012.10.01_15-12-19.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/连城要塞遗址.md" title="wikilink">连城要塞遗址和</a><a href="../Page/友谊关.md" title="wikilink">友谊关</a></p></td>
<td><p>6-1021</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/北海市.md" title="wikilink">北海市</a>、<a href="../Page/防城港市.md" title="wikilink">防城港市</a>、<br />
<a href="../Page/宁明县.md" title="wikilink">宁明县</a>、<a href="../Page/凭祥市.md" title="wikilink">凭祥市</a>、<br />
<a href="../Page/龙州县.md" title="wikilink">龙州县</a>、<a href="../Page/大新县.md" title="wikilink">大新县</a>、<br />
<a href="../Page/靖西市.md" title="wikilink">靖西市</a>、<a href="../Page/那坡县.md" title="wikilink">那坡县</a></p></td>
<td><p><a href="../Page/明.md" title="wikilink">明至</a><a href="../Page/清.md" title="wikilink">清</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Youyi_Guan.jpg" title="fig:Youyi_Guan.jpg">Youyi_Guan.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/容县近代建筑.md" title="wikilink">容县近代建筑</a></p></td>
<td><p>6-1022</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/容县.md" title="wikilink">容县</a></p></td>
<td><p><a href="../Page/清.md" title="wikilink">清至</a><a href="../Page/民国时期.md" title="wikilink">民国</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/太平天国永安活动旧址.md" title="wikilink">太平天国永安活动旧址</a></p></td>
<td><p>6-1023</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/蒙山县.md" title="wikilink">蒙山县</a></p></td>
<td><p>1851年</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/马胖鼓楼.md" title="wikilink">马胖鼓楼</a></p></td>
<td><p>6-1024</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/三江县.md" title="wikilink">三江县</a></p></td>
<td><p><a href="../Page/民国时期.md" title="wikilink">民国</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/梧州中山纪念堂.md" title="wikilink">梧州中山纪念堂</a></p></td>
<td><p>6-1025</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/梧州市.md" title="wikilink">梧州市</a></p></td>
<td><p><a href="../Page/民国时期.md" title="wikilink">民国</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:中山纪念堂（梧州）.JPG" title="fig:中山纪念堂（梧州）.JPG">中山纪念堂（梧州）.JPG</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/广西农民运动讲习所旧址.md" title="wikilink">广西农民运动讲习所旧址</a></p></td>
<td><p>6-1026</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/东兰县.md" title="wikilink">东兰县</a></p></td>
<td><p>1925年</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/红军标语楼_(河池).md" title="wikilink">红军标语楼</a></p></td>
<td><p>6-1027</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/河池市.md" title="wikilink">河池市</a></p></td>
<td><p>1930年</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/湘江战役旧址.md" title="wikilink">湘江战役旧址</a></p></td>
<td><p>6-1028</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/兴安县.md" title="wikilink">兴安县</a></p></td>
<td><p>1934年</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/昆仑关战役旧址.md" title="wikilink">昆仑关战役旧址</a><br />
* <a href="../Page/陆军第五军昆仑关战役阵亡将士墓园.md" title="wikilink">陆军第五军昆仑关战役阵亡将士墓园</a></p>
<ul>
<li><a href="../Page/桂南会战检讨会旧址.md" title="wikilink">桂南会战检讨会旧址</a>（含<a href="../Page/护蒋洞.md" title="wikilink">护蒋洞</a>）</li>
<li><a href="../Page/昆仑关战役白岩前线指挥部旧址.md" title="wikilink">昆仑关战役白岩前线指挥部旧址</a></li>
</ul></td>
<td><p>6-1029</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/南宁市.md" title="wikilink">南宁市</a>、<a href="../Page/宾阳县.md" title="wikilink">宾阳县</a>、<a href="../Page/柳州市.md" title="wikilink">柳州市</a></p></td>
<td><p>1939～1940年</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Liuzhou_Hujiangdong_2014.03.03_11-25-26.jpg" title="fig:Liuzhou_Hujiangdong_2014.03.03_11-25-26.jpg">Liuzhou_Hujiangdong_2014.03.03_11-25-26.jpg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/胡志明旧居.md" title="wikilink">胡志明旧居</a><br />
含：</p>
<ul>
<li><a href="../Page/南洋客棧舊址.md" title="wikilink">南洋客棧舊址</a></li>
<li><a href="../Page/乐群社旧址.md" title="wikilink">乐群社旧址</a></li>
<li><a href="../Page/蟠龙山扣留所旧址.md" title="wikilink">蟠龙山扣留所旧址</a></li>
<li><a href="../Page/红楼旧址_(柳州市).md" title="wikilink">红楼旧址</a></li>
</ul></td>
<td><p>6-1030</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/柳州市.md" title="wikilink">柳州市</a></p></td>
<td><p>1942～1954年</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Liuzhou_Nanyang_Kezhan_Jiuzhi_2014.03.01_13-46-40.jpg" title="fig:Liuzhou_Nanyang_Kezhan_Jiuzhi_2014.03.01_13-46-40.jpg">Liuzhou_Nanyang_Kezhan_Jiuzhi_2014.03.01_13-46-40.jpg</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/柳城巨猿洞.md" title="wikilink">柳城巨猿洞</a></p></td>
<td><p>7-0390</p></td>
<td><p>古遗址</p></td>
<td><p><a href="../Page/柳城县.md" title="wikilink">柳城县</a></p></td>
<td><p>旧石器时代</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/布兵盆地洞穴遗址群.md" title="wikilink">布兵盆地洞穴遗址群</a></p></td>
<td><p>7-0391</p></td>
<td><p>古遗址</p></td>
<td><p><a href="../Page/田东县.md" title="wikilink">田东县</a></p></td>
<td><p>旧石器时代</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/那赖遗址.md" title="wikilink">那赖遗址</a></p></td>
<td><p>7-0392</p></td>
<td><p>古遗址</p></td>
<td><p><a href="../Page/田阳县.md" title="wikilink">田阳县</a></p></td>
<td><p>旧石器时代</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/晓锦遗址.md" title="wikilink">晓锦遗址</a></p></td>
<td><p>7-0393</p></td>
<td><p>古遗址</p></td>
<td><p><a href="../Page/资源县.md" title="wikilink">资源县</a></p></td>
<td><p>新石器时代</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/大浪古城遗址.md" title="wikilink">大浪古城遗址</a></p></td>
<td><p>7-0394</p></td>
<td><p>古遗址</p></td>
<td><p><a href="../Page/合浦县.md" title="wikilink">合浦县</a></p></td>
<td><p>汉</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/草鞋村遗址.md" title="wikilink">草鞋村遗址</a></p></td>
<td><p>7-0395</p></td>
<td><p>古遗址</p></td>
<td><p><a href="../Page/合浦县.md" title="wikilink">合浦县</a></p></td>
<td><p>汉</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/越州故城.md" title="wikilink">越州故城</a></p></td>
<td><p>7-0396</p></td>
<td><p>古遗址</p></td>
<td><p><a href="../Page/浦北县.md" title="wikilink">浦北县</a></p></td>
<td><p>南朝</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/中和窑址.md" title="wikilink">中和窑址</a></p></td>
<td><p>7-0397</p></td>
<td><p>古遗址</p></td>
<td><p><a href="../Page/藤县.md" title="wikilink">藤县</a></p></td>
<td><p>宋</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/凤腾山古墓群.md" title="wikilink">凤腾山古墓群</a></p></td>
<td><p>7-0642</p></td>
<td><p>古墓葬</p></td>
<td><p><a href="../Page/环江毛南族自治县.md" title="wikilink">环江毛南族自治县</a></p></td>
<td><p>清</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/湘山寺塔群与石刻.md" title="wikilink">湘山寺塔群与石刻</a></p></td>
<td><p>7-1283</p></td>
<td><p>古建筑</p></td>
<td><p><a href="../Page/全州县.md" title="wikilink">全州县</a></p></td>
<td><p>宋至清</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/永宁州城城墙.md" title="wikilink">永宁州城城墙</a></p></td>
<td><p>7-1284</p></td>
<td><p>古建筑</p></td>
<td><p><a href="../Page/永福县.md" title="wikilink">永福县</a></p></td>
<td><p>明</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/大芦村古建筑群.md" title="wikilink">大芦村古建筑群</a></p></td>
<td><p>7-1285</p></td>
<td><p>古建筑</p></td>
<td><p><a href="../Page/灵山县.md" title="wikilink">灵山县</a></p></td>
<td><p>明至清</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/富川瑶族风雨桥群.md" title="wikilink">富川瑶族风雨桥群</a></p></td>
<td><p>7-1286</p></td>
<td><p>古建筑</p></td>
<td><p><a href="../Page/富川瑶族自治县.md" title="wikilink">富川瑶族自治县</a></p></td>
<td><p>明至清</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/伏波庙_(横县).md" title="wikilink">伏波庙</a></p></td>
<td><p>7-1287</p></td>
<td><p>古建筑</p></td>
<td><p><a href="../Page/横县.md" title="wikilink">横县</a></p></td>
<td><p>清</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/和里三王宫.md" title="wikilink">和里三王宫</a></p></td>
<td><p>7-1288</p></td>
<td><p>古建筑</p></td>
<td><p><a href="../Page/三江侗族自治县.md" title="wikilink">三江侗族自治县</a></p></td>
<td><p>清</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/惠爱桥.md" title="wikilink">惠爱桥</a></p></td>
<td><p>7-1289</p></td>
<td><p>古建筑</p></td>
<td><p><a href="../Page/合浦县.md" title="wikilink">合浦县</a></p></td>
<td><p>清</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/西林岑氏家族建筑群.md" title="wikilink">西林岑氏家族建筑群</a></p></td>
<td><p>7-1290</p></td>
<td><p>古建筑</p></td>
<td><p><a href="../Page/西林县.md" title="wikilink">西林县</a></p></td>
<td><p>清</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/百寿岩石刻.md" title="wikilink">百寿岩石刻</a></p></td>
<td><p>7-1571</p></td>
<td><p>石窟寺及石刻</p></td>
<td><p><a href="../Page/永福县.md" title="wikilink">永福县</a></p></td>
<td><p>宋至民国</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/会仙山摩崖石刻.md" title="wikilink">会仙山摩崖石刻</a></p></td>
<td><p>7-1572</p></td>
<td><p>石窟寺及石刻</p></td>
<td><p><a href="../Page/宜州市.md" title="wikilink">宜州市</a></p></td>
<td><p>宋至民民国</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/梧州近代建筑群.md" title="wikilink">梧州近代建筑群</a></p></td>
<td><p>7-1855</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/梧州市.md" title="wikilink">梧州市</a></p></td>
<td><p>清至民国</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/谢鲁山庄.md" title="wikilink">谢鲁山庄</a></p></td>
<td><p>7-1856</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/陆川县.md" title="wikilink">陆川县</a></p></td>
<td><p>1920年</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/越南共产党驻龙州秘密机关旧址.md" title="wikilink">越南共产党驻龙州秘密机关旧址</a></p></td>
<td><p>7-1857</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/龙州县.md" title="wikilink">龙州县</a></p></td>
<td><p>1926年</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/柳州旧机场及城防工事群旧址.md" title="wikilink">柳州旧机场及城防工事群旧址</a></p></td>
<td><p>7-1858</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/柳州市.md" title="wikilink">柳州市</a></p></td>
<td><p>1929年</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/南宁育才学校旧址.md" title="wikilink">南宁育才学校旧址</a></p></td>
<td><p>7-1859</p></td>
<td><p>近现代重要史迹及代表性建筑</p></td>
<td><p><a href="../Page/南宁市.md" title="wikilink">南宁市</a></p></td>
<td><p>1951年</p></td>
<td></td>
</tr>
</tbody>
</table>

</center>

## 参考文献

  -
## 参见

  - [广西壮族自治区文物保护单位](../Page/广西壮族自治区文物保护单位.md "wikilink")

{{-}}

[桂](../Category/中国全国重点文物保护单位列表.md "wikilink")
[Category:广西文物保护单位列表](../Category/广西文物保护单位列表.md "wikilink")
[广西全国重点文物保护单位](../Category/广西全国重点文物保护单位.md "wikilink")