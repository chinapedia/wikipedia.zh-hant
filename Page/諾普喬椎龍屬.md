**諾普喬椎龍屬**（學名：*Nopcsaspondylus*）意為「諾普喬的脊椎」，是以最初敘述者[法蘭茲·諾普喬](../Page/法蘭茲·諾普喬.md "wikilink")（Franz
Nopcsa）為名。諾普喬椎龍是[蜥腳下目](../Page/蜥腳下目.md "wikilink")[雷巴齊斯龍科的一屬](../Page/雷巴齊斯龍科.md "wikilink")，化石發現於[阿根廷](../Page/阿根廷.md "wikilink")[內烏肯省的Candeleros地層](../Page/內烏肯省.md "wikilink")，年代為上[白堊紀的](../Page/白堊紀.md "wikilink")[科尼亞克階](../Page/科尼亞克階.md "wikilink")。化石是一個[背椎](../Page/背椎.md "wikilink")，是在1902年由諾普喬所敘述，但當時並沒有命名，該化石現已遺失\[1\]\[2\]。諾普喬椎龍具有小型椎體與大型側腔，現在被認為是種典型的雷巴齊斯龍科動物\[3\]。

## 参考资料

## 外部連結

  - [諾普喬椎龍](http://www.dinodata.org/index.php?option=com_content&task=view&id=9600&Itemid=67)
    DinoData.org

[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")
[Category:南美洲恐龍](../Category/南美洲恐龍.md "wikilink")
[Category:雷巴齊斯龍科](../Category/雷巴齊斯龍科.md "wikilink")

1.

2.

3.