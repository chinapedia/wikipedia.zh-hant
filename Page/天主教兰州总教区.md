**兰州总教区**是[罗马天主教在中国](../Page/罗马天主教.md "wikilink")[甘肃省设立的一个](../Page/甘肃省.md "wikilink")[教区](../Page/教区.md "wikilink")。

## 历史

1878年6月21日，从陝西代牧区分设甘肅代牧区。1905年4月28日，甘肅代牧区分为甘肅北境代牧区和甘肅南境代牧区。1918年，[陶福音退休](../Page/陶福音.md "wikilink")。1922年3月8日，甘肅北境代牧区和甘肅南境代牧区更名为甘肅西境代牧区和甘肅东境代牧区。1924年12月3日，甘肅西境代牧区以主教驻地更名为兰州府代牧区。1946年4月11日，升格为兰州总教区，德国[圣言会士](../Page/圣言会.md "wikilink")[濮登博为首任总主教](../Page/濮登博.md "wikilink")。1950年，主教濮登博、副主教赵承明被捕，1952年被驱逐出境。\[1\]可统计信徒3万多人，宗教活动场所83处。

## 所辖教区

  - [天主教秦州教区](../Page/天主教秦州教区.md "wikilink")
  - [天主教平凉教区](../Page/天主教平凉教区.md "wikilink")
  - [天主教西宁监牧区](../Page/天主教西宁监牧区.md "wikilink")
  - [天主教新疆监牧区](../Page/天主教新疆监牧区.md "wikilink")

教区部分教堂 兰州教区主教府（兰州小沟头教堂，兰州市城关区金昌南路328号，东方红广场浦发银行大楼后） 甘肃省陇西县城天主堂
甘肃省张掖市民乐县杨坊天主堂 甘肃省兰州市下川（新城）天主堂 甘肃省武威市古浪县土门镇天主堂
甘肃省永昌天主堂 甘肃省山丹县潘庄村（甘泉）天主堂 甘肃省榆中县葛家湾天主堂 甘肃省武威市凉州区达子沟天主堂
甘肃省武威市凉州区金山（河南坝）天主堂 甘肃省山丹县陈户（徐家庄）天主堂 安西教友活动点 定西天主堂 大和乐天主堂
永丰滩北台天主堂 武威市凉州区头坝天主堂 嘉峪关九号门天主堂 甘肃省武威市凉州区松树天主堂 武威胡家庄 张掖甘州区城区天主堂
（张掖市甘州区劳动街教堂路口十字，市基督教礼拜堂对面） 张掖甘州区上寺闸天主堂 张掖甘州区六号天主堂
张掖甘州区四号天主堂（张掖天主教公墓） 张掖甘州区园艺天主堂 武威东乡天主堂
兰州市小沟头天主堂 武威城区若瑟堂

## 历任主教

  - [韩默理](../Page/韩默理.md "wikilink")（Ferdinand Hamer）1878年－1889年
  - [陶福音](../Page/陶福音.md "wikilink")（Bishop Hubert Otto,
    C.I.C.M.）1890年－1918年
  - [费达德](../Page/费达德.md "wikilink")（Bishop Godefroy Frederix,
    C.I.C.M.）1920年－1922年
  - [濮登博](../Page/濮登博.md "wikilink")（Archbishop Theodor Buddenbrock,
    S.V.D.）1924年－1959年
  - [杨立柏](../Page/杨立柏.md "wikilink")（1987年－1998年）
  - [韩志海](../Page/韩志海.md "wikilink")（2003年1月6日－）

## 信徒

1950年，信徒17,465人\[2\]。

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  - [天主教兰州教区](http://www.catholiclz.org/)

[Category:中国天主教教区](../Category/中国天主教教区.md "wikilink")
[Category:甘肃天主教](../Category/甘肃天主教.md "wikilink")
[Category:兰州宗教](../Category/兰州宗教.md "wikilink")

1.  [外办历史](http://www.lzwsb.com/lzfao/his1.asp)
2.  [Archdiocese of
    Lanchow](http://www.catholic-hierarchy.org/diocese/dlnch.html)