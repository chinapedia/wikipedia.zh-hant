**阿爾伯特·史懷哲**（[德语](../Page/德语.md "wikilink")：****，），[法國](../Page/法國.md "wikilink")[阿爾薩斯](../Page/阿爾薩斯.md "wikilink")（出生時屬於[德國](../Page/德意志帝國.md "wikilink")）的[通才](../Page/通才.md "wikilink")，擁有[神學](../Page/神學家.md "wikilink")、[音樂](../Page/音樂家.md "wikilink")、[哲學及](../Page/哲學家.md "wikilink")[醫學四個博士學位](../Page/醫師.md "wikilink")。他生於當時德國[阿爾薩斯-洛林](../Page/阿爾薩斯-洛林.md "wikilink")[凱塞爾斯貝爾](../Page/凱塞爾斯貝爾.md "wikilink")，半年後舉家遷往[甘斯巴克定居](../Page/甘斯巴克.md "wikilink")。[阿爾薩斯位於法國與德國的邊境](../Page/阿爾薩斯.md "wikilink")，雖以德裔為主但曾長期屬於法國，所以在孩童時期能運用德法兩個語言。因為他在[中非西部](../Page/中部非洲.md "wikilink")[加蓬創立](../Page/加蓬.md "wikilink")[阿爾伯特史懷哲醫院](../Page/阿爾伯特史懷哲醫院.md "wikilink")（），而在1953年獲得1952年度的[諾貝爾和平獎](../Page/諾貝爾和平獎.md "wikilink")。

## 神學

當他還是年輕[神學家的时候](../Page/神學家.md "wikilink")，在1906年出版了他首部著作《[歷史耶穌的探索](../Page/歷史耶穌的探索.md "wikilink")》（），獲得很高的名譽。在這本著作中，他以[末世學的信念解釋耶穌的一生](../Page/基督教末世論.md "wikilink")。史懷哲示範了一種新的作法：為了重新塑造耶穌而利用歷史研究來描繪耶穌一生，俨如歷史學家的描繪一般。這部著作成為[新約聖經研究的次領域長達數十年之久](../Page/新約聖經.md "wikilink")。

他憑著神學博士論文，包括了1911年的《耶穌的精神病學研究》和1930年的《使徒保羅的神秘主義》建立了聲譽，並且成為《[新約聖經](../Page/新約聖經.md "wikilink")》學者。他對保羅的研究中，他檢驗了保羅的末世論理念，並以此檢驗了《新約聖經》。

當時他在[斯特拉斯堡的](../Page/斯特拉斯堡.md "wikilink")[圣尼古拉教堂擔任](../Page/圣尼古拉教堂_\(斯特拉斯堡\).md "wikilink")[归正会牧師](../Page/归正会.md "wikilink")，他曾祝福[特奥多尔·豪斯的婚姻](../Page/特奥多尔·豪斯.md "wikilink")。特奥多尔·豪斯後來成為[西德的首任](../Page/西德.md "wikilink")[總統](../Page/總統.md "wikilink")。

## 音樂

[Albert_Schweitzer,_Etching_by_Arthur_William_Heintzelman.jpg](https://zh.wikipedia.org/wiki/File:Albert_Schweitzer,_Etching_by_Arthur_William_Heintzelman.jpg "fig:Albert_Schweitzer,_Etching_by_Arthur_William_Heintzelman.jpg")
當時，史懷哲是一位著名的管風琴師。他對[巴哈的音樂特別情有獨鍾](../Page/巴哈.md "wikilink")。他自己建立一種簡單的表演風格，他認為比較接近[巴哈的原意](../Page/巴哈.md "wikilink")。他對[巴哈的理解建基於重新評估](../Page/巴哈.md "wikilink")[巴哈的宗教意圖](../Page/巴哈.md "wikilink")。十八歲，他到巴黎師從管風琴泰斗[魏多](../Page/魏多.md "wikilink")。當時他利用唱頌讚美詩內容來解釋[巴哈詩歌前奏的意象完全不是老人家所見的方法](../Page/巴哈.md "wikilink")，讓[魏多感到十分驚訝](../Page/魏多.md "wikilink")。在1908年最終版本的《约翰·塞巴斯蒂安·巴赫》一書中，他所主張的這個新風格對現今處理[巴哈音樂的手法有重大影響](../Page/巴哈.md "wikilink")。

[魏多與史懷哲合作編定了](../Page/魏多.md "wikilink")[巴哈管風琴的作品集](../Page/巴哈.md "wikilink")。他的小冊子《德法兩國管風琴的製造與演奏風琴的技巧》（1906年）牽起了二十世紀一股[風琴改革運動](../Page/風琴改革運動.md "wikilink")（Orgelbewegung）。雖然改革運動遠超史懷哲的預期，但它讓人擺脫了浪漫主義的極端，並重新發現巴洛克的信條。

## 哲學

史懷哲的世界觀就是以尊重生命（"*Ehrfurcht vor dem
Leben*"）為基礎，他相信這就是他對人類作出最偉大的貢獻。他認為西方文明因為慢慢放棄肯定生命為倫理基礎，所以變得腐化。

以尊重生命為最高原則就是他牢固的信念。類似[尼采對生命的興奮](../Page/尼采.md "wikilink")，史懷哲誠然跟隨了和俄國人[列夫·托爾斯泰一樣的路線](../Page/列夫·托爾斯泰.md "wikilink")。當年有些人把他的哲學與[聖方濟各的作比較](../Page/聖方濟各_\(亞西西\).md "wikilink")。在《文明的哲學》的第26章，他寫道：

> True philosophy must start from the most immediate and comprehensive
> fact of consciousness: 'I am life that wants to live, in the midst of
> life that wants to live'.

> 真正的哲學必須由全面而直接的知覺事實開始：'我是想要活下去的生命，與許多也想活下去的生命一起存活。'。

[生命與](../Page/生命.md "wikilink")[愛在他眼中也是沿著同樣的信念](../Page/愛.md "wikilink")，尊重每一個生命，尊重個人與宇宙的精神關係。對他來說，倫理包含了強迫對每個願意活著的生命表示同樣的尊敬。縱使有時環境不容許，不應該引領到失敗主義之上。因為願意活著會促使自我更新，是演變必要的結果，是精神領域的現象。

史懷哲在他的一生都主張尊重生命。他堅持歷史上的啟蒙時期慢慢自行沒落和墮落的原因是沒有足夠的思想準備但卻強行跟從這樣的倫理。因此，他期望有一個更新的有深度的[文藝復興和人道啟蒙](../Page/文藝復興.md "wikilink")。他不斷期盼人類對自己在宇宙中有更深的醒覺。他的樂觀全依對真理的信賴。「來自真理的精神比環境所推動的精神更偉大。」他恆常地強調思考的必要性多於為刺激作反應或跟從最廣泛的意見行事。

## 晚年

1939年至1948年，因為歐洲正陷入戰爭中，他便滯留於[中非西部](../Page/中部非洲.md "wikilink")[加蓬](../Page/加蓬.md "wikilink")[蘭巴雷內市](../Page/蘭巴雷內.md "wikilink")。[第二次世界大戰結束三年後](../Page/第二次世界大戰.md "wikilink")，即1948年，他第一次返回歐洲，並且以後不斷往返，有一趟更遠赴美國，直到1965年逝世那年為止。

## 部分著作

  - *The Quest Of The Historical Jesus; A Critical Study Of Its Progress
    From Reimarus To
    Wrede*[1](http://archive.org/details/questofhistorica00schwrich),
    (1906), Augsburg Fortress Publishers, 2001 edition: ISBN
    0-8006-3288-5
  - *The Psychiatric Study of Jesus: Exposition and Criticism*, (1911),
    Gloucester, Massachusetts: Peter Smith Publisher, 1948, ISBN
    0-8446-2894-8
  - *The Mystery of the Kingdom of God: The Secret of Jesus' Messiahship
    and Passion*, (1914), Prometheus Books, 1985, ISBN 0-87975-294-7
  - *The Decay and the Restoration of Civilization* and *Civilization
    and Ethics*(1923)combined in one volume, Prometheus Books, 1987,
    ISBN 0-87975-403-6
  - *The Mysticism of Paul the Apostle*, (1930), Johns Hopkins
    University Press, 1998, ISBN 0-8018-6098-9
  - *Out of My Life and Thought: An Autobiography*(1933),Johns Hopkins
    University Press, 1998 edition with forward by [Jimmy
    Carter](../Page/Jimmy_Carter.md "wikilink"): ISBN 0-8018-6097-0
  - *Indian Thought and Its Development*（1935）
  - *Peace or Atomic War* 1958
  - *The Kingdom of God and Primitive Christianity*（pub.1967）

## 大事表

  - 1893 -
    在[巴黎和](../Page/巴黎.md "wikilink")[柏林](../Page/柏林.md "wikilink")[凱瑟斯堡的大學修讀](../Page/凱瑟斯堡.md "wikilink")[哲學和](../Page/哲學.md "wikilink")[神學](../Page/神學.md "wikilink")
  - 1900 - 在[凱瑟斯堡聖尼克拉斯教堂出任牧師](../Page/凱瑟斯堡.md "wikilink")
  - 1901 - [凱瑟斯堡神學院的院長](../Page/凱瑟斯堡.md "wikilink")
  - 1905-1913修讀醫學和手術
  - 1912 - 與[海倫娜·布雷斯勞結婚](../Page/海倫娜·布雷斯勞.md "wikilink")
  - 1913 -
    [加蓬](../Page/加蓬.md "wikilink")[蘭巴雷內出任醫師](../Page/蘭巴雷內.md "wikilink")
  - 1915 - 建立他的倫理《尊重生命》
  - 1917 - 在法國被扣留
  - 1918 - 在[凱瑟斯堡擔任醫藥助理和助理牧師](../Page/凱瑟斯堡.md "wikilink")
  - 1919 - 在瑞典[烏普薩拉大學首次發表關於](../Page/烏普薩拉大學.md "wikilink")《尊重生命》的演講
  - 1919 - 女兒Rhena出生
  - 1924 - 返回[蘭巴雷內出任醫師](../Page/蘭巴雷內.md "wikilink")；不斷往返歐洲作演講
  - 1939-1948 [蘭巴雷內](../Page/蘭巴雷內.md "wikilink")
  - 1949 - 到[美國探訪](../Page/美國.md "wikilink")
  - 1948-1965 - 蘭巴雷內與歐洲
  - 1953 - [諾貝爾和平獎](../Page/諾貝爾和平獎.md "wikilink")
  - 1957 - 1958 - 反對核軍備和核測試的四次演說

## 参考文献

## 外部链接

  - *Albert Schweitzer: a Biography* by [James
    Brabazon](../Page/James_Brabazon.md "wikilink") - 史懷哲的生平

  - [艾伯特史懷哲](http://www.albertschweitzer.info/) - 史懷哲的一生與思想

  - [2](http://www.iseps.org.uk) -
    [艾伯特史懷哲學會](../Page/艾伯特史懷哲學會.md "wikilink")

  - [史懷哲之友(英國)](http://www.albertschweitzer.org.uk/) - 宣揚尊重生命的慈善機構

  - [The Albert Schweitzer Page](http://home.pcisys.net/~jnf/)

  - [Albert Schweitzer Fellowship](http://www.schweitzerfellowship.org/)

  - [尊重生命的讀本](http://www1.chapman.edu/schweitzer/reverence_readings.html)

  - [Biography information on the 1952 Nobel Peace Prize
    Laureate](https://web.archive.org/web/20060428130211/http://medlem.spray.se/atarme/albert.html)

  - [Page at the Nobel
    e-Museum](http://www.nobel.se/peace/laureates/1952/schweitzer-bio.html)

  - [Schweitzer Nobel Presentation Speech by Gunnar
    Jahn](http://nobelprize.org/peace/laureates/1952/press.html#not_10)

  - [Schweitzerforlaget](https://web.archive.org/web/20060422110235/http://albert-schweitzer.com/)（挪威語）

  - [Phelps, Lawrence. A Short History of the Organ Revival (describes
    Schweitzer's work to reform organ
    building)](http://www.lawrencephelps.com/Documents/Articles/Phelps/ashorthistory.shtml)

  - [Paul and His
    Interpreters](http://thedcl.org/christia/s/schweitzer/paulahi/paulahi.html)
    at The DCL.

  - （中文）[【原始森林的邊緣】](http://haodoo.net/?M=book&P=1737)（好讀網站）

{{-}}

[Category:諾貝爾和平獎獲得者](../Category/諾貝爾和平獎獲得者.md "wikilink")
[Category:德國諾貝爾獎獲得者](../Category/德國諾貝爾獎獲得者.md "wikilink")
[Category:法國諾貝爾獎獲得者](../Category/法國諾貝爾獎獲得者.md "wikilink")
[Category:20世紀哲學家](../Category/20世紀哲學家.md "wikilink")
[Category:德國醫學家](../Category/德國醫學家.md "wikilink")
[Category:德國新教神學家](../Category/德國新教神學家.md "wikilink")
[Category:德国哲学家](../Category/德国哲学家.md "wikilink")
[Category:德國音樂家](../Category/德國音樂家.md "wikilink")
[Category:法國新教神學家](../Category/法國新教神學家.md "wikilink")
[Category:法國哲學家](../Category/法國哲學家.md "wikilink")
[Category:法國音樂家](../Category/法國音樂家.md "wikilink")
[Category:人權活動家](../Category/人權活動家.md "wikilink")
[Category:和平主義者](../Category/和平主義者.md "wikilink")
[Category:加蓬健康](../Category/加蓬健康.md "wikilink")
[Category:蒂賓根大學校友](../Category/蒂賓根大學校友.md "wikilink")
[Category:阿爾薩斯人](../Category/阿爾薩斯人.md "wikilink")
[Category:功績勳章成員](../Category/功績勳章成員.md "wikilink")
[Category:19世纪法国人](../Category/19世纪法国人.md "wikilink")