**霍爾縣** (**Hall County,
Nebraska**)是[美國](../Page/美國.md "wikilink")[內布拉斯加州東南部的一個縣](../Page/內布拉斯加州.md "wikilink")。面積1,430平方公里。根據[美國2000年人口普查估計](../Page/美國2000年人口普查.md "wikilink")，共有人口53,534人。縣治[格蘭德艾蘭](../Page/格蘭德艾蘭_\(內布拉斯加州\).md "wikilink")
(Grand Island)。

成立於1858年11月4日，縣政府成立於1867年1月7日。縣名紀念代表[艾奧瓦州的](../Page/艾奧瓦州.md "wikilink")[聯邦眾議員](../Page/美國眾議院.md "wikilink")、[內布拉斯加準州大法官](../Page/內布拉斯加準州.md "wikilink")[奧克斯都·霍爾](../Page/奧克斯都·霍爾.md "wikilink")。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

[H](../Category/內布拉斯加州行政區劃.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.