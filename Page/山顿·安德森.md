**山顿·罗德里格斯·安德森**（，），生于[佐治亚州的](../Page/佐治亚州.md "wikilink")[亚特兰大](../Page/亚特兰大.md "wikilink")，美国职业篮球运动员，司职[得分后卫](../Page/得分后卫.md "wikilink")、小前鋒
。

## 职业生涯

安德森以[佐治亚大学球员身份在](../Page/佐治亚大学.md "wikilink")1996年NBA选秀中第二轮被[犹他爵士队选中](../Page/犹他爵士.md "wikilink")，之后他效力过[休斯敦火箭和](../Page/休斯敦火箭.md "wikilink")[纽约尼克斯](../Page/纽约尼克斯.md "wikilink")。安德森表现最好的赛季是在1999-00赛季，在那个赛季中他为休斯敦火箭队贡献了场均12.3分。目前为止他的职业生涯场均得分为7.8分

他是前[迈阿密热火队和纽约尼克斯队球员](../Page/迈阿密热火.md "wikilink")[威利·安德森的弟弟](../Page/威利·安德森_\(篮球运动员\).md "wikilink")。

安德森在2006年以替补身份随迈阿密热火队取得过[NBA总冠军](../Page/NBA总冠军.md "wikilink")。

## 外部链接

  - [NBA.com Profile - Shandon
    Anderson](http://www.nba.com/playerfile/shandon_anderson/)
  - [ESPN.com - Shandon
    Anderson](http://sports.espn.go.com/nba/players/profile?statsId=3081)

[Category:美国男子篮球运动员](../Category/美国男子篮球运动员.md "wikilink")
[Category:非洲裔美國籃球運動員](../Category/非洲裔美國籃球運動員.md "wikilink")
[Category:猶他爵士隊球員](../Category/猶他爵士隊球員.md "wikilink")
[Category:休斯頓火箭隊球員](../Category/休斯頓火箭隊球員.md "wikilink")
[Category:纽约尼克斯队球员](../Category/纽约尼克斯队球员.md "wikilink")
[Category:邁阿密熱火隊球員](../Category/邁阿密熱火隊球員.md "wikilink")