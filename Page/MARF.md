**Modular Audio Recognition Framework** (MARF)
模件音频识别框架是一个用[JAVA写的采集](../Page/JAVA.md "wikilink")[声音](../Page/声音.md "wikilink")／[语音](../Page/语音.md "wikilink")／[语言](../Page/语言.md "wikilink")／[文本和](../Page/文字.md "wikilink")[自然语言处理](../Page/自然语言处理.md "wikilink")（NLP）算法的研究平台。它被安排进一个模件和延伸性框架用来试图促进新的[算法](../Page/算法.md "wikilink")。MARF可在[应用软件中用作](../Page/应用软件.md "wikilink")[库来使用或作为](../Page/库.md "wikilink")[学习和](../Page/学习.md "wikilink")[扩展的原始资料](../Page/扩展.md "wikilink")。MARF提供了几种应用的例子来展示如何使用[框架](../Page/框架.md "wikilink")。同时MARF提供了详细的JAVA
DOC格式使用手册和[API 参考](http://marf.sourceforge.net/api-dev/)。MARF
及其应用程序以[BSD](../Page/BSD.md "wikilink") license发行。

## 参考资料

  -
  -
  -
[Category:语音识别](../Category/语音识别.md "wikilink")
[Category:自然语言处理](../Category/自然语言处理.md "wikilink")
[Category:Java](../Category/Java.md "wikilink")
[Category:BSD](../Category/BSD.md "wikilink")
[Category:SourceForge专案](../Category/SourceForge专案.md "wikilink")