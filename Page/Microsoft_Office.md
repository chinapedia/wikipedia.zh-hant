**Microsoft
Office**是一套由[微软公司开发的](../Page/微软公司.md "wikilink")[办公软件套裝](../Page/办公软件套裝.md "wikilink")，它可以在[Microsoft
Windows](../Page/Microsoft_Windows.md "wikilink")、[Windows
Phone](../Page/Windows_Phone.md "wikilink")、[Mac系列](../Page/Mac系列.md "wikilink")、[iOS和](../Page/iOS.md "wikilink")[Android等系統上運行](../Page/Android.md "wikilink")。与其他办公室应用程式一样，它包括联合的[伺服器和基于](../Page/伺服器.md "wikilink")[網際網路的服务](../Page/網際網路.md "wikilink")。從2007版的Office被称为“Office
System”而不叫“Office Suite”，反映出它们包括伺服器的事实。

Office最初是一个推广名称，出现于1990年代早期，指一些以前曾单独发售的软件的合集。当时主要的推广重点是：购买合集比单独购买要省很多钱。最初的Office版本包含Word、Excel和PowerPoint。「专业版」包含Microsoft
Access，Microsoft Outlook
当时不存在。随着时间的改變，Office应用程序逐渐整合，共享特性，例如：拼写和语法检查、OLE数据整合和[VBA](../Page/Visual_Basic_for_Applications.md "wikilink")[脚本语言](../Page/脚本语言.md "wikilink")。微软目前将Office延伸作为一个开发平台，可以藉由專用商店下載第三方軟體搭配使用。

Office常是製作文件檔案的标准，而且有一些其他产品不存在的特性，但是其他产品也有Office缺少的特性。自從[Microsoft 2007
Office
System開始](../Page/Microsoft_Office_2007.md "wikilink")，有一个和以前版本差异很大的用户界面，该界面被称为[Ribbon界面](../Page/Ribbon.md "wikilink")，并沿用在[Microsoft
Office 2010](../Page/Microsoft_Office_2010.md "wikilink")、[Microsoft
Office 2013和](../Page/Microsoft_Office_2013.md "wikilink")[Microsoft
Office 2016中](../Page/Microsoft_Office_2016.md "wikilink")。

## 版本歷史

Microsoft Office最初的版本在1989年所推出，最初的運作是在蘋果電腦的Macintosh平台之上。Microsoft
Office自從推出文字處理、試算表以及簡報製作的軟體後，直至近年一直居於領導地位。從 Microsoft Office 2013
(Office 15) 起，Office
更多的功能提供跨平台支援，並提供了供行動[作業系統](../Page/作業系統.md "wikilink")
[Android](../Page/Android.md "wikilink") 和
[IOS](../Page/IOS.md "wikilink") 版本的程式，功能主要可用於 Desktop 版本。自 Microsoft
Office 2016 版本發行起，各平台相同版本號的更新內容大致相等。

### Windows版的歷史

  - **[Microsoft Office
    3.0](../Page/Microsoft_Office_3.0.md "wikilink")**：是第一版針對[Microsoft
    Windows平台所發行的Office](../Page/Microsoft_Windows.md "wikilink")，光盘版包括：Word
    2.0c、Excel 4.0a、PowerPoint 3.0、Mail，发行于1993年8月30日。

<!-- end list -->

  - **Microsoft Office 4.0**：包含[Word
    6.0](../Page/Word_6.0.md "wikilink")、[Excel
    5.0](../Page/Excel_5.0.md "wikilink")、[PowerPoint
    4.0](../Page/PowerPoint_4.0.md "wikilink")、[Mail和](../Page/Mail.md "wikilink")[Access](../Page/Access.md "wikilink")。Word在此時被稱為Word
    6.0，而非2.0，是為了和[Mac
    OS版本的版號相同](../Page/Mac_OS.md "wikilink")，发行于1994年1月17日。

<!-- end list -->

  - **Microsoft Office 4.2**：用于Windows NT，（Word
    6.0\[32位，i386，Alpha\]、Excel 5.0\[32位，i386，Alpha\]、PowerPoint
    4.0\[16位\]、“Microsoft Office Manager”），发行于1994年7月3日。

<!-- end list -->

  - **Microsoft Office 4.3**：是最後一版16位元的版本，同時也是最後一版支援[Windows
    3.x](../Page/Windows_3.1x.md "wikilink")、[Windows NT
    3.1和](../Page/Windows_NT_3.1.md "wikilink")[Windows NT
    3.5的版本](../Page/Windows_NT_3.5.md "wikilink")，包含Word 6.0、Excel
    5.0、PowerPoint 4.0。在专业版中还有Access 2.0，发行于1994年6月2日。

<!-- end list -->

  - **[Microsoft Office
    95](../Page/Microsoft_Office_95.md "wikilink")**：為符合[Windows
    95](../Page/Windows_95.md "wikilink")，完全是32位元的版本。Office
    95有兩個版本，Office 95標準版和Office
    95專業版。標準版包含有Word、Excel、PowerPoint和[Schedule+](../Page/Schedule+.md "wikilink")。專業版則包含所有標準版的軟體再加Access。假如購買的是[CD型態的專業版](../Page/CD.md "wikilink")，則另外包含[Bookshelf](../Page/Microsoft_Bookshelf.md "wikilink")，发行于1995年8月30日。

<!-- end list -->

  - **[Microsoft Office
    97](../Page/Microsoft_Office_97.md "wikilink")**：是一個重大的里程碑。這一版中包含許多的新功能和改進。其同時也引入[命令欄](../Page/命令欄.md "wikilink")（Command
    Bars）的功能以及-{zh-hant:文法檢查; zh-hans:拼写检查;}-的功能。
    Office 97是最後一版支援[Windows NT
    3.51的Office套裝軟體](../Page/Windows_NT_3.51.md "wikilink")，发行于1996年12月30日（既发行于光盘又发行于一套45张的3.5英寸[软盘](../Page/软盘.md "wikilink")）。

<!-- end list -->

  - **[Microsoft Office
    2000](../Page/Microsoft_Office_2000.md "wikilink")**：加入自適應功能表（adaptive
    menus），較少使用的選項將會被隱藏起來。他同時也加入許多新的安全功能。包含加入[數位簽章的功能](../Page/數位簽章.md "wikilink")，減少巨集病毒的威脅。
    Office 2000是最後一版支援[Windows
    95的Office](../Page/Windows_95.md "wikilink")。Office2000同時也是最後一版沒有包含[產品啟動這個功能的版本](../Page/產品啟動.md "wikilink")，发行于1999年1月27日。

<!-- end list -->

  - **[Microsoft Office
    XP](../Page/Microsoft_Office_XP.md "wikilink")**：是為了配合[Windows
    XP而推出](../Page/Windows_XP.md "wikilink")。Office
    XP引入[安全模式](../Page/安全模式.md "wikilink")（Safe
    Mode）的功能。它允許像是Outlook這類的應用程式無法正常啟動時自動以適當的狀態運行。Microsoft
    Office
    XP包含完整的語音辨識系統及手寫辨識系統。另外，引進新的功能“[產品啟動](../Page/產品啟動.md "wikilink")”，使用者需要用[序號來啟動Office](../Page/產品金鑰.md "wikilink")
    XP\[1\]，而這個同能也同時被引入[Windows
    XP及其之後的Windows和Office各版中](../Page/Windows_XP.md "wikilink")。
    Office XP是最後一版支援[Windows
    98](../Page/Windows_98.md "wikilink")、[ME和](../Page/Windows_ME.md "wikilink")[NT
    4.0的Office](../Page/Windows_NT_4.0.md "wikilink")。Outlook
    2002（XP）有個嚴重的問題：在[Vista之下無法記住電子信箱帳戶和密碼](../Page/Windows_Vista.md "wikilink")，发行于2001年5月31日。

<!-- end list -->

  - **[Microsoft Office
    2003](../Page/Microsoft_Office_2003.md "wikilink")**：在2003年時發行。有兩個新的程式加入其中：[Microsoft
    InfoPath和](../Page/Microsoft_InfoPath.md "wikilink")[OneNote](../Page/Microsoft_OneNote.md "wikilink")。Office
    2003是第一個使用[Windows
    XP風格圖示的版本](../Page/Windows_XP.md "wikilink")。另外，Outlook
    2003在許多地方做出了改進，包含[Kerberos驗證](../Page/Kerberos.md "wikilink")、[遠端程序呼叫](../Page/遠端程序呼叫.md "wikilink")（Remote
    Procedure Call，RPC），以及快取交換模式（Cached Exchange
    Mode）。Outlook在這一版中加入垃圾郵件篩選器，協助解決垃圾郵件的問題。
    Office 2003是最後一版支援[Windows
    2000的Office套裝軟體](../Page/Windows_2000.md "wikilink")，发行于2003年11月17日。

<!-- end list -->

  - **[Microsoft Office
    2007](../Page/Microsoft_Office_2007.md "wikilink")**：是為了配合[Windows
    Vista而推出的](../Page/Windows_Vista.md "wikilink")。Office
    2007包含許多新功能，最引人注目的是[Ribbon新介面](../Page/Ribbon.md "wikilink")，它取代舊有的功能表及工具列，使其更易使用。
    Office 2007-{只}-能在Service Pack 2或3的[Windows
    XP](../Page/Windows_XP.md "wikilink")、Service Pack 1以上的[Windows
    Server 2003或Windows](../Page/Windows_Server_2003.md "wikilink")
    Vista之上的版本中安裝。Microsoft Office 2007推出Service Pack
    3，修正不少漏洞，发行于2006年11月30日。

<!-- end list -->

  - **[Microsoft Office
    2010](../Page/Microsoft_Office_2010.md "wikilink")**：在2009年1月，微軟開始Office
    2010的內測（TP），包括：Access 2010, Excel 2010, Groove 2010, InfoPath
    Designer 2010, InfoPath Filler 2010, InterConnect 2010, Lync 2010,
    OneNote 2010, Outlook 2010, PowerPoint 2010, Project 2010, Publisher
    2010, SharePoint Designer 2010, Visio 2010, Word 2010 全系列產品。Office
    2010 Beta在2009年5月份開始進行研發，最終版本于2010年4月15日发布。Microsoft Office
    2010有32位元和64位元版本，微軟稱32位元更為穩定，建議64位元的電腦安裝32位元的Office。由Office
    2010起，大量授權版也和一般版一樣需要[產品啟動](../Page/產品啟動.md "wikilink")。Microsoft
    Office 2010已有Service Pack 1，版本為14.0.6029.1000。Office
    2010是最後一版支援[Windows
    XP](../Page/Windows_XP.md "wikilink")（只限Service Pack 3）、[Windows
    Vista](../Page/Windows_Vista.md "wikilink")（Service Pack
    1或以上）、[Windows Server
    2003](../Page/Windows_Server_2003.md "wikilink")（只限Service Pack
    2）和[Windows Server
    2008的Office套裝軟體](../Page/Windows_Server_2008.md "wikilink")，於2010年6月發行正式版。

<!-- end list -->

  - **[Microsoft Office
    2013](../Page/Microsoft_Office_2013.md "wikilink")**：2012年10月11日正式版發佈，在2013年1月26日開賣。具有32位和64位两个版本。此Office正式开始整合Microsoft帳戶，雲端储存用户的修改、設定并且默认存放文件的路径设为[OneDrive](../Page/OneDrive.md "wikilink")。该版本对Excel、OneNote以及Outlook做了许多明显的改进。版本具有[Office
    Apps](../Page/Office_Apps.md "wikilink")--一种應用程式擴充。这种擴充可以在office.com和Office軟體中的專用商店下载並安装，在文档中提供动态的、可交互的网络信息相关内容。
    Office 2013-{只}-能在[Windows
    7](../Page/Windows_7.md "wikilink")、[Windows Server 2008
    R2或](../Page/Windows_Server_2008_R2.md "wikilink")[Windows
    8以上的版本中安裝](../Page/Windows_8.md "wikilink")，于2013年1月发行正式版。

<!-- end list -->

  - **[Microsoft Office
    2016](../Page/Microsoft_Office_2016.md "wikilink")**：此版本是配合[Windows
    10所推出](../Page/Windows_10.md "wikilink")。介面對2013版本進行中等程度修改，加入更整合化的商店。由於盛傳[Windows
    10附贈內建Office](../Page/Windows_10.md "wikilink")，因此引起關注。最後微軟官方出面澄清只會附贈預覽功能。2015年9月23日正式上市。

<!-- end list -->

  - **[Microsoft Office
    2019](../Page/Microsoft_Office_2019.md "wikilink")**：Office
    2019，2019年上半年推出正式版。新版Office办公套装将包括Word、Excel、PowerPoint和Outlook，以及服务器版本的Exchange、SharePoint和Skype
    for Business。

### Mac版的歷史

[Microsoft_Office_2004.png](https://zh.wikipedia.org/wiki/File:Microsoft_Office_2004.png "fig:Microsoft_Office_2004.png")

  - **Microsoft Office for
    Mac**：在1989年時在[Mac平台上推出](../Page/Mac_OS.md "wikilink")，這一版的Office中包含了Word
    4.0、Excel 2.20和PowerPoint
    2.01。此版本比運作在[Windows平台上的Office更早被推出](../Page/Windows.md "wikilink")。

<!-- end list -->

  - **Microsoft Office 1.5 for Mac**：在1991年時推出，其中Excel升級為Excel
    3.0。是首套支援蘋果電腦[System
    7](../Page/System_7.md "wikilink")
    [作業系統的Office](../Page/作業系統.md "wikilink")。

<!-- end list -->

  - **Microsoft Office 2.9 for Mac**：在1992年時推出，其中Excel
    4.0是首套支援新[AppleScript的軟體](../Page/AppleScript.md "wikilink")。

<!-- end list -->

  - **Microsoft Office 4.0 for Mac**：於1993年時發行。他是第一套支援[Power
    Macintosh的Office套裝軟體](../Page/Power_Macintosh.md "wikilink")，最後一版針對Mac釋出的Office是：Office
    4.2.1。

<!-- end list -->

  - **[Microsoft Office 98 Macintosh
    Edition](../Page/Microsoft_Office_98_Macintosh_Edition.md "wikilink")**：在1998年6月6日的[MacWorld
    Expo/San
    Francisco首次露面](../Page/Macworld_Conference_&_Expo.md "wikilink")。它加入了[Internet
    Explorer](../Page/Internet_Explorer.md "wikilink") 4.0
    [browser和](../Page/browser.md "wikilink")[Outlook
    Express](../Page/Outlook_Express.md "wikilink")。首版支援[QuickTime的Office套裝軟體](../Page/QuickTime.md "wikilink")，发行于1998年3月15日。

<!-- end list -->

  - **Microsoft Office 2001**：在2000年10月11日開始發售。其所需的環境為Mac OS
    8.1（建議使用8.5或更高版本）。Office
    2001中加入了[Entourage這套郵件軟體](../Page/Microsoft_Entourage.md "wikilink")。

<!-- end list -->

  - **Microsoft Office v. X**：在2001年時為了新的Mac OS X平台而推出，发行于2001年9月3日。

<!-- end list -->

  - **[Microsoft Office 2004 for
    Mac](../Page/Microsoft_Office_2004_for_Mac.md "wikilink")**：在2004年5月11日推出。

<!-- end list -->

  - **[Microsoft Office 2008 for
    Mac](../Page/Microsoft_Office_2008_for_Mac.md "wikilink")**：於2008年1月15日發售。它是首款使用[通用二進位](../Page/通用二進位.md "wikilink")（[universal
    binary](../Page/universal_binary.md "wikilink")）的Office套裝軟體。這意味著它可以原生支援[Intel和](../Page/Apple-Intel_architecture.md "wikilink")[Power
    PC兩者之上的Macs](../Page/Power_PC.md "wikilink")。

<!-- end list -->

  - **[Microsoft Office 2011 for
    Mac](../Page/Microsoft_Office_2011_for_Mac.md "wikilink")**：於2010年9月26日發售，改用Ribbon介面，並改以[Outlook
    for
    Mac取代](../Page/Outlook_for_Mac.md "wikilink")[Entourage](../Page/Entourage.md "wikilink")。Microsoft
    Office 2011 for Mac已推出Service Pack 3更新，版本為14.5.2。

<!-- end list -->

  - **[Microsoft Office 2016 for
    Mac](../Page/Microsoft_Office_2016.md "wikilink")**：發行於2015年9月，與
    Windows 版本統稱為 Microsoft Office 2016。

<!-- end list -->

  - **[Microsoft Office
    2019](../Page/Microsoft_Office_2019.md "wikilink")**：Office 2019 for
    Mac Preview 已开放试用，根据Microsoft 支持服务显示，Office 2019 for Mac
    也将于2019年上半年发布。

### Android 版的歷史

  - **[Microsoft Office
    Mobile](../Page/Microsoft_Office_Mobile.md "wikilink")** **(Office
    15):** 是Microsoft為行動裝置提供的Office版本。主要功能是讓Office
    365訂閱用戶能夠在行動裝置上使用他們的Office訂閱。此版本一直為人詬病，因為該程式只提供給付費訂閱用戶使用，對非訂閱用戶功能有限（只能閱讀文件內容，不能作出修改），不能夠正確顯示文件格式，而且不能將Office文件存於裝置當中。現時該程式仍在Google
    Play上架，但僅支援運行Android 4.4版本或以下的裝置\[2\]。
  - **[Microsoft Office
    2016](../Page/Microsoft_Office_2016.md "wikilink")**:
    2015年4月15日，Microsoft在Google Play發行4個Office程式，其中包括[Microsoft
    Word](../Page/Microsoft_Word.md "wikilink"), [Microsoft
    Powerpoint](../Page/Microsoft.md "wikilink"), [Microsoft
    OneNote和](../Page/Microsoft_OneNote.md "wikilink")[Microsoft
    Outlook](../Page/Microsoft_Outlook.md "wikilink")，當時此版本只供平板電腦使用，但現在已經開放給Android
    4.4以上的所有裝置，用戶需要獨立下載和更新各應用程式。屏幕尺寸小於10.1吋的[Android裝置無需訂閱都能夠正常使用大部份功能](../Page/Android.md "wikilink")。

## 組件

### 桌面產品

这些程序包括在Microsoft Office 2010的所有版本中，除了Microsoft Office Starter
2010。Microsoft Office Starter
2010並非公開銷售的產品，它是購買品牌電腦時隨機安裝的，只包括簡易版Word及Excel，而且在版面的右方有著無法移除的微軟廣告。

#### Word

**Microsoft
Word**是[文書处理软件](../Page/文書处理软件.md "wikilink")，被认为是Office的主要程序，在文字处理软件市场上拥有统治份额，其私有的DOC[格式被尊为一个行业的标准](../Page/文件格式.md "wikilink")，虽然由Word
2007年已經轉用DOCX格式。Word也适宜某些版本的[Microsoft
Works](../Page/Microsoft_Works.md "wikilink")。它适宜Windows和Macintosh平台。它的主要竞争者是[LibreOffice](../Page/LibreOffice.md "wikilink")、[Corel
WordPerfect和Apple](../Page/WordPerfect.md "wikilink")
[Pages](../Page/Pages.md "wikilink")。

#### Excel

**Microsoft
Excel**是[电子试算表程序](../Page/电子试算表.md "wikilink")（进行数字和预算运算的软件程序），与Microsoft
Word一样，它在市场拥有统治份额。它最初对占优势的[Lotus
1-2-3是个竞争者](../Page/Lotus_1-2-3.md "wikilink")，但最后它卖得比它多、快，于是它成为了实际标准。它适宜Windows和Macintosh平台。它的主要竞争者是[OpenOffice.org
Calc](../Page/OpenOffice.org_Calc.md "wikilink")、[Apple](../Page/Apple.md "wikilink")
[Numbers和](../Page/Numbers.md "wikilink")[Corel](../Page/Corel.md "wikilink")
[Quattro Pro](../Page/Quattro_Pro.md "wikilink")。

#### Outlook

**Microsoft Outlook**（不要与[Outlook
Express混淆](../Page/Microsoft_Outlook_Express.md "wikilink")）是[个人信息管理程序和](../Page/个人信息管理系统.md "wikilink")[电子邮件通信软件](../Page/电子邮件.md "wikilink")。在Office
97版接任[Microsoft
Mail](../Page/Microsoft_Mail.md "wikilink")。它包括一个电子邮件客户端，日历，任务管理者，和地址本。它的电子邮件程序的主要竞争者是[Mozilla
Thunderbird](../Page/Mozilla_Thunderbird.md "wikilink")（[Mozilla](../Page/Mozilla_Application_Suite.md "wikilink")）和[Eudora](../Page/Eudora.md "wikilink")。它的个人信息管理程序主要竞争者是Mozilla、[苹果公司的](../Page/苹果公司.md "wikilink")[Mail和](../Page/Mail.md "wikilink")[Lotus
Organizer](../Page/Lotus_Organizer.md "wikilink")。它的一个版本也被包括在大多数[Pocket
PC掌上电脑裡](../Page/Pocket_PC.md "wikilink")。

  - Microsoft Mail——邮件客户端（包含在Office的老版本中，后来由Microsoft Outlook替代）。
  - Microsoft Outlook Express——邮件客户端（包含在Office 98
    Macintosh版中，后来由Microsoft Entourage替代）。
  - Microsoft Entourage——邮件客户端（包含在Office 2008及之前版本中，后来由Microsoft Outlook
    for Mac替代）。

#### PowerPoint

**Microsoft
PowerPoint**是一个在[Windows和](../Page/Windows.md "wikilink")[Macintosh下流行的介绍程序](../Page/Macintosh.md "wikilink")。它可以创建由文字组合，图片，电影以及其它事物组成的[幻灯片](../Page/幻灯片.md "wikilink")。[幻灯片可以在屏幕上显示](../Page/幻灯片.md "wikilink")，并且可以通过阐述者操控，[幻灯片也可以使用](../Page/幻灯片.md "wikilink")[反映机或](../Page/反映机.md "wikilink")[投影仪投射到屏幕上](../Page/投影仪.md "wikilink")。Windows
Mobile 2005（Magneto）将使用本程序在市场上拥有统治地位。它的主要竞争者是[OpenOffice.org
Impress](../Page/OpenOffice.org_Impress.md "wikilink")，[Corel](../Page/Corel.md "wikilink")
[Word Perfect和](../Page/Word_Perfect.md "wikilink")[Apple
Keynote](../Page/Keynote.md "wikilink")。

#### Access

**Microsoft
Access**是由微软发布的关联式[数据库管理系统](../Page/数据库.md "wikilink")。它可以用于制作处理数据的桌面系统。它也常被用来开发简单的网页应用程序。（只提供
Windows 版本）

### 附带的程序

  - [Microsoft Internet
    Explorer常用的](../Page/Microsoft_Internet_Explorer.md "wikilink")[网络浏览器](../Page/网络浏览器.md "wikilink")。現在
    [Internet Explorer](../Page/Internet_Explorer.md "wikilink") 不綁捆附帶於
    Office 中，在 Windows 10 中保留兼容性。
  - [Microsoft Photo
    Editor](../Page/Microsoft_Photo_Editor.md "wikilink")——在更旧的版本和XP版的Office中的相片编辑／纲板图形软件。它由[Microsoft
    PhotoDraw暂时地补充在了Office](../Page/Microsoft_PhotoDraw.md "wikilink")
    2000 Premium版中。
  - [Microsoft Office Picture
    Manager](../Page/Microsoft_Office_Picture_Manager.md "wikilink")——基本的相片管理软件（与一个基本版的[Google的](../Page/Google.md "wikilink")[Picasa或](../Page/Picasa.md "wikilink")[Adobe的](../Page/Adobe_Systems.md "wikilink")[Photoshop
    Elements相似](../Page/Photoshop_Elements.md "wikilink")）。

### 高级版本包含的其它程序

  - [Microsoft
    FrontPage](../Page/Microsoft_FrontPage.md "wikilink")——网站设计软件（并且需要它自己的服务器程序）。对于2003版，它包含在Microsoft
    Office Professional Edition 2003和Microsoft Office Professional
    Enterprise Edition之中。它并不包含在Microsoft Office System
    2007中，取而代之的是另外發布的[Microsoft
    Expression產品線](../Page/Microsoft_Expression.md "wikilink")。
  - [Microsoft
    Visio](../Page/Microsoft_Visio.md "wikilink")——作图程序（單獨購買）。
  - [Microsoft
    Publisher](../Page/Microsoft_Publisher.md "wikilink")——[桌面出版软件](../Page/桌面出版.md "wikilink")。对于2016版，它包含在Microsoft
    Office Small Business Edition 2007、Microsoft Office Professional
    (Plus) Edition 2007、Microsoft Office Enterprise Edition
    2007和Microsoft Office Professional (Plus) Edition 2010里面。
  - [Microsoft
    Project](../Page/Microsoft_Project.md "wikilink")——计划管理器（單獨購買）。
  - [Microsoft
    OneNote](../Page/Microsoft_OneNote.md "wikilink")——笔记软件，适和于使用平板电脑和一般个人计算机。它包含在Microsoft
    Office System 2016里面。
  - [Microsoft SharePoint
    Designer](../Page/Microsoft_SharePoint_Designer.md "wikilink")——WYSIWYG（所见即所得）HTML编辑器暨Microsoft通用网页设计程序，用于替代Microsoft
    Office FrontPage.
  - [Microsoft
    InfoPath](../Page/Microsoft_InfoPath.md "wikilink")——使用户设计rich
    XML-based的形式的应用。包含在Microsoft Office Professional Plus Edition
    2016、Microsoft Office Enterprise Edition 2007和Microsoft Office
    Professional Plus Edition 2010之中。
  - [Live Communication
    Server](../Page/Live_Communication_Server.md "wikilink")——实时通信软件。

### 其他程式

  - [Microsoft
    Binder](../Page/Microsoft_Binder.md "wikilink")——把多個文件合併為一個文件。
      - Binder對微軟來說是一個大規模的失敗，並且普及程度很低。因此Office的新版本經常沒有這個軟體。
  - [Microsoft
    Entourage](../Page/Microsoft_Entourage.md "wikilink")——僅用於Macintosh的個人資訊管理程式和電子郵件軟體。（與Outlook類似）。
  - [Microsoft
    MapPoint](../Page/Microsoft_MapPoint.md "wikilink")——繪圖和旅行計劃軟體。（自2016版开始作为Excel的组件附赠）
  - [Microsoft Office Communicator
    2005](../Page/Microsoft_Office_Communicator_2005.md "wikilink") -
    [1](https://web.archive.org/web/20050809082521/http://weblog.infoworld.com/techwatch/archives/001172.html)
  - Developer Tools——開發工具（只包含於開發版）

### 插件

#### Office插件

  - **2007 Microsoft Office加载项：將Microsoft文件儲存成PDF或XPS**：运用该加载项在八种2007
    Microsoft
    Office程序中导出文件，并存为[PDF和](../Page/PDF.md "wikilink")[XPS格式](../Page/XPS.md "wikilink")，而無須借助第三方軟件。

#### 第三方外掛程式支援

Office應用程式的一個主要特性是用戶和第三方業者可以編寫以[COM為基礎的](../Page/COM.md "wikilink")*Office套件*，來擴充應用程式的功能，以及新增自訂的命令和界面。

### 基于互联网的服务

  - [Microsoft Office
    Website](../Page/Microsoft_Office_Website.md "wikilink")——网站。包含在Microsoft
    Office 2007、Microsoft Office 2010的所有版本。 -
    [2](https://products.office.com/zh-cn/)
  - [Microsoft Office
    Update](../Page/Microsoft_Office_Update.md "wikilink")——网站。Office2000、XP、2003和2007版、2010版的补丁查找和安装服务。
    - [3](https://www.microsoft.com/zh-cn/download/office.aspx)
  - [Microsoft AutoUpdate](../Page/Microsoft_AutoUpdate.md "wikilink") -
    供Mac 版本用戶更新 Office 至最新版本的安裝器

从1997年版本开始，[Microsoft
Agent](../Page/Microsoft_Agent.md "wikilink")（在9.0/2000版和更高版本）和一个相似的演员技术（在8.0/97版）开始提供**[Office助手](../Page/Office助手.md "wikilink")**，一个交互式帮助工具。“助手”的绰号经常是“Clippy”或“Clippit”。因为它缺省对应CLIPPIT.ACS。

从Macintosh Office
1998开始，Office的Macintosh版本和视窗版本分享同样的文件格式。结果，任意一个安装Office
1998或更晚版本的Macintosh均能读取Windows平台的Office 8.0（1997）版或更晚版本的文件，而且反之亦然。

### 彩蛋

部分版本中，藏有開發人員設計的一些彩蛋。

## 其他資訊

基于某些原因，大多数Microsoft Office版本（包含97和更高版本，可能也包含4.3）使用独有的界面库，没有使用操作系统的界面库。

尽管Windows使用“服务包”，但是Office习惯于发布可以单独安装的升级版。但是，在Office 2000 Service Release
1之后，Office现在只发布服务包。

## 支援的生命週期

每一個版本的Office的支援期都有5年或新版本Office發佈後的2年，視乎哪個時間較遲。Office XP/2002、Office
2003、Office 2007現已不被支援。

### Windows版本

| Windows作業系統版本                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      | 最後版本                                                            | 主流支援結束日期    | 延伸支援結束日期    |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | --------------------------------------------------------------- | ----------- | ----------- |
| [Windows NT 3.51](../Page/Windows_NT_3.51.md "wikilink")（需要Service Pack 5）                                                                                                                                                                                                                                                                                                                                                                                                                                         | [Office 97](../Page/Microsoft_Office_97.md "wikilink")（8）       | 2001年8月31日  | 2002年2月28日  |
| [Windows 95](../Page/Windows_95.md "wikilink")                                                                                                                                                                                                                                                                                                                                                                                                                                                                     | [Office 2000](../Page/Microsoft_Office_2000.md "wikilink")（9）   | 2004年6月30日  | 2009年7月14日  |
| [Windows NT 4.0](../Page/Windows_NT_4.0.md "wikilink")/[98](../Page/Windows_98.md "wikilink")/[Windows 2000](../Page/Windows_2000.md "wikilink")/[Me](../Page/Windows_Me.md "wikilink")/[Windows XP](../Page/Windows_XP.md "wikilink")/[Windows Server 2003](../Page/Windows_Server_2003.md "wikilink")/[Vista](../Page/Windows_Vista.md "wikilink")                                                                                                                                                               | [Office XP/2002](../Page/Microsoft_Office_XP.md "wikilink")（10） | 2006年7月11日  | 2011年7月12日  |
| [Windows 2000](../Page/Windows_2000.md "wikilink")（需要Service Pack 3或者Service Pack 4）/[Windows XP](../Page/Windows_XP.md "wikilink")/[Windows Server 2003](../Page/Windows_Server_2003.md "wikilink")/[Vista](../Page/Windows_Vista.md "wikilink")/[Server 2008](../Page/Windows_Server_2008.md "wikilink")/[Windows 7](../Page/Windows_7.md "wikilink")                                                                                                                                                            | [Office 2003](../Page/Microsoft_Office_2003.md "wikilink")（11）  | 2009年4月14日  | 2014年4月8日   |
| [Windows XP](../Page/Windows_XP.md "wikilink")（需要Service Pack 2）/[Server 2003](../Page/Windows_Server_2003.md "wikilink")/[Vista](../Page/Windows_Vista.md "wikilink")/[Server 2008](../Page/Windows_Server_2008.md "wikilink")/[Windows 7](../Page/Windows_7.md "wikilink")/[Server 2008 R2](../Page/Windows_Server_2008_R2.md "wikilink")/[Windows 8](../Page/Windows_8.md "wikilink")/[Server 2012](../Page/Windows_Server_2012.md "wikilink")/[Windows 10](../Page/Windows_10.md "wikilink")                   | [Office 2007](../Page/Microsoft_Office_2007.md "wikilink")（12）  | 2012年4月10日  | 2017年10月10日 |
| [Windows XP](../Page/Windows_XP.md "wikilink")（需要Service Pack 3）/[Server 2003](../Page/Windows_Server_2003.md "wikilink")/[Vista](../Page/Windows_Vista.md "wikilink")（需要Service Pack 1）/[Server 2008](../Page/Windows_Server_2008.md "wikilink")/[Windows 7](../Page/Windows_7.md "wikilink")/[Server 2008 R2](../Page/Windows_Server_2008_R2.md "wikilink")/[Windows 8](../Page/Windows_8.md "wikilink")/[Server 2012](../Page/Windows_Server_2012.md "wikilink")/[Windows 10](../Page/Windows_10.md "wikilink") | [Office 2010](../Page/Microsoft_Office_2010.md "wikilink")（14）  | 2015年10月13日 | 2020年10月13日 |
| [Windows 7](../Page/Windows_7.md "wikilink")/[Server 2008 R2](../Page/Windows_Server_2008_R2.md "wikilink")/[Windows 8](../Page/Windows_8.md "wikilink")/[Server 2012](../Page/Windows_Server_2012.md "wikilink")/[Windows 10](../Page/Windows_10.md "wikilink")                                                                                                                                                                                                                                                   | [Office 2013](../Page/Microsoft_Office_2013.md "wikilink")（15）  | 2018年4月10日  | 2023年4月11日  |
| [Windows 7](../Page/Windows_7.md "wikilink")（需要Service Pack 1）/[Server 2008 R2](../Page/Windows_Server_2008_R2.md "wikilink")/[Windows 8](../Page/Windows_8.md "wikilink")/[Server 2012](../Page/Windows_Server_2012.md "wikilink")/[Windows 10](../Page/Windows_10.md "wikilink")                                                                                                                                                                                                                                 | [Office 2016](../Page/Microsoft_Office_2016.md "wikilink")（16）  | 2020年10月13日 | 2025年10月14日 |
|                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |                                                                 |             |             |

### Mac版本

| Macintosh版本                    | 最後版本                                                               |
| ------------------------------ | ------------------------------------------------------------------ |
| (68K) System 7.0-Mac OS 8.3    | Office 4.2.1                                                       |
| (PPC) System 7.1.2             | Office 4.2.1                                                       |
| (PPC) System 7.5-Mac OS 8.0    | Office 98                                                          |
| (PPC) Mac OS 8.1-9.2.2         | Office 2001                                                        |
| Mac OS X 10.1-10.5             | Office v. X                                                        |
| Mac OS X 10.2-10.5             | [Office 2004](../Page/Office_2004_for_Mac.md "wikilink")           |
| Mac OS X 10.4-10.7             | [Office 2011](../Page/Microsoft_Office_2011_for_Mac.md "wikilink") |
| OS X 10.10-10.11 /macOS 10.12+ | [Office 2016](../Page/Office_2016.md "wikilink")                   |

### Android版本

| Android 作業系統版本                                                                                      | 最後版本                                                                       |
| --------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------- |
| [Android 4.0](../Page/Android_4.0.md "wikilink") - [Android 4.4](../Page/Android_4.4.md "wikilink") | [Office Mobile](../Page/Microsoft_Office_Mobile.md "wikilink") (Office 15) |
| [Android 4.4](../Page/Android_4.4.md "wikilink")+                                                   | [Office 2016](../Page/Office_2016.md "wikilink")                           |

## 參見

  - [Sway](../Page/Sway.md "wikilink")
  - [LibreOffice](../Page/LibreOffice.md "wikilink")，[開放的辦公室套件軟體](../Page/開放原始碼.md "wikilink")。

## 參考文獻

## 外部链接

  -
  - [Microsoft Office 官方培训、支持和帮助页面](https://support.office.com)

  - [Microsoft Office 2016 for
    Mac](https://products.office.com/mac/microsoft-office-for-mac)

  - [MSDN Office Developer Center](https://msdn.microsoft.com/office/)

  - [Microsoft
    Office二进制文件格式](https://web.archive.org/web/20080218212338/http://www.microsoft.com/interop/docs/officebinaryformats.mspx)

{{-}}

[Category:办公室自动化软件](../Category/办公室自动化软件.md "wikilink")
[Category:Microsoft Office](../Category/Microsoft_Office.md "wikilink")
[Category:Mac OS软件](../Category/Mac_OS软件.md "wikilink")
[Category:MacOS軟體](../Category/MacOS軟體.md "wikilink")

1.
2.