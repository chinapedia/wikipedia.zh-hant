[Tephra_fall_from_1991_eruption_of_Mt_Pinatubo.gif](https://zh.wikipedia.org/wiki/File:Tephra_fall_from_1991_eruption_of_Mt_Pinatubo.gif "fig:Tephra_fall_from_1991_eruption_of_Mt_Pinatubo.gif")分佈\]\]

**皮纳图博火山**是一座活跃的[层状火山](../Page/层状火山.md "wikilink")，位于[菲律宾](../Page/菲律宾.md "wikilink")[吕宋岛](../Page/吕宋岛.md "wikilink")[三描礼士](../Page/三描礼士省.md "wikilink")、[打拉和](../Page/打拉省.md "wikilink")[邦板牙三省的交界处](../Page/邦板牙省.md "wikilink")，岩层主要由[安山岩和](../Page/安山岩.md "wikilink")[英安岩构成](../Page/英安岩.md "wikilink")。1991年前，皮纳图博火山并不知名，而且受到严重侵蚀。当时它被茂密的树林覆盖，住有数千名[矮黑族原住民](../Page/矮黑族.md "wikilink")，他们的祖先在1565年[西班牙人征服菲律宾后从低地逃往山区](../Page/西班牙帝国.md "wikilink")。

1991年6月，皮纳图博火山爆发，是20世纪发生在陆地上第二大规模的火山爆发。\[1\]这次爆发的[火山爆發指数为](../Page/火山爆發指数.md "wikilink")6，与上一次已知的爆发相距450至500年（火山爆发指数为5），与上一次达到火山爆发指数6的爆发则相距500至1000年。\[2\]这次爆发高潮被成功预测，火山附近的数以万计居民得以及时疏散，但爆发引起的[火山碎屑流](../Page/火山碎屑流.md "wikilink")、火山灰和后来由[颱風容雅登陸後的雨水触发的](../Page/颱風容雅_\(1991年\).md "wikilink")[火山泥流严重破坏了邻近的地区](../Page/火山泥流.md "wikilink")，数千间房屋和其他建筑物被摧毁，而旅遊業損失亦高達300億菲律賓披索。\[3\]

火山爆发的效应蔓延至全球各地，火山喷出约100亿吨的岩浆和2千万吨的[二氧化硫](../Page/二氧化硫.md "wikilink")，为地表帶來大量的矿物和金属；又对[平流层注入大量的](../Page/平流层.md "wikilink")[气溶胶](../Page/气溶胶.md "wikilink")，数月后，气溶胶在形成一层[硫酸雾](../Page/硫酸雾.md "wikilink")，全球平均气温下降约0.5[°C](../Page/摄氏度.md "wikilink")，[臭氧层损耗亦短暂大幅增加](../Page/臭氧层损耗.md "wikilink")。

## 参見

  - [菲律賓活火山列表](../Page/菲律賓活火山列表.md "wikilink")

## 注释

## 参考文献

  - Decker, R. and Decker, B. (1997) *Volcanoes*, 3rd edition, WH
    Freeman, New York.

  - Hiromu Shimizu (2002), *[Struggling for Existence after the Pinatubo
    Eruption 1991: Catastrophe, Suffering and Rebirth of Ayta
    Communities](https://web.archive.org/web/20040815181730/http://www.scs.kyushu-u.ac.jp/~hs1/anthro/shimizu_hp/introduction/essay/Ayta_Struggling.htm)*.
    Paper presented inter-congress of the International Union of
    Anthropological and Ethnological Sciences, Tokyo, Japan.

  -
  - Newhall, C. and Punongbayan, R., eds. (1997) *[Fire and Mud:
    Eruptions and Lahars of Mount Pinatubo,
    Philippines](http://pubs.usgs.gov/pinatubo/contents.html)*. ISBN
    0-295-97585-7.

  - Scaillet, B. and Evans, B. W. (1999) *The 15 June 1991 Eruption of
    Mount Pinatubo. I. Phase Equilibria and Pre-eruption P–T–fO2–fH2O
    Conditions of the Dacite Magma.* Journal of Petrology, v. 40,
    381–411.

  - Stimac J.A., Goff F., Counce D., Larocque A.C.L., Hilton D.R.
    (2003), *The crater lake and hydrothermal system of Mount Pinatubo,
    Philippines: evolution in the decade after eruption*, Bulletin of
    Volcanology, v. 66, p. 149–167

  - Wiesner, M.G., Wetzel, A. Catane, S.G., Listanco, E.L. and
    Mirabueno, H.T. (2004) Grain size, areal thickness distribution and
    controls on sedimentation of the 1991 Mount Pinatubo tephra layer in
    the South China Sea. Bulletin of Volcanology, v. 66, 226–242.

  - Dhot, S. Mt Pinatubo Safety.

[Category:菲律宾火山](../Category/菲律宾火山.md "wikilink")
[Category:菲律宾山峰](../Category/菲律宾山峰.md "wikilink")
[Category:20世纪火山事件](../Category/20世纪火山事件.md "wikilink")
[Category:活火山](../Category/活火山.md "wikilink")
[Category:1991年自然灾害](../Category/1991年自然灾害.md "wikilink")
[Category:菲律宾自然灾害](../Category/菲律宾自然灾害.md "wikilink")

1.

2.

3.