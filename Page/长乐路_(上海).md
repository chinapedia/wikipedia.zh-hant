[800_Changle_Rd._Shanghai.JPG](https://zh.wikipedia.org/wiki/File:800_Changle_Rd._Shanghai.JPG "fig:800_Changle_Rd._Shanghai.JPG")
**长乐路**是[中国](../Page/中国.md "wikilink")[上海市](../Page/上海市.md "wikilink")[黄浦区](../Page/黄浦区.md "wikilink")、[徐汇区和](../Page/徐汇区.md "wikilink")[静安区一条重要东西向街道](../Page/静安区.md "wikilink")，在1914年—1943年之间的路名为**蒲石路**（**Rue
Bourgeat**），属于[上海法租界](../Page/上海法租界.md "wikilink")。东起[淡水路](../Page/淡水路.md "wikilink")，西至[华山路](../Page/华山路_\(上海\).md "wikilink")。全长3327米。长乐路沿路主要为住宅区，也包括了许多花园洋房、新式[里弄等](../Page/里弄.md "wikilink")。

## 历史

长乐路最早建成的一段是位于杜美路（Route
Doumer）\[1\]和善钟路\[2\]之间的部分，1902年由上海法租界公董局越界修筑，当时是杜美路的一部分。1914年，[上海法租界向西大规模拓展后划入界内](../Page/上海法租界.md "wikilink")。同年，法租界公董局修筑圣母院路（Route
des
Soeurs）\[3\]至迈尔西爱路段，以法国律师名命名。以后向两端延伸，东端通至萨坡赛路，西端与杜美路、善钟路相接。1943年，又从善钟路继续向西修筑至麦琪路（Route
Alfread Magy
）\[4\]，新修路段称为刘家弄。同年，汪精卫政府接收上海法租界，将蒲石路全路连同刘家弄一并改名为长乐路。1946年，长乐路继续向西延伸至华山路。

## 沿路历史建筑

  - 长乐路109号——[华懋公寓](../Page/华懋公寓.md "wikilink")，建于1929年，今锦江宾馆北楼，第一批[上海市优秀历史建筑](../Page/上海市优秀历史建筑.md "wikilink")
  - 长乐路570弄——[蒲园](../Page/蒲园.md "wikilink")，建成于1942年，第三批上海市优秀历史建筑
  - 长乐路613弄——[沪江别墅](../Page/沪江别墅.md "wikilink")，建于1939年，第四批上海市优秀历史建筑
  - 长乐路746号——[合众图书馆旧址](../Page/合众图书馆.md "wikilink")，静安区登记不可移动文物
  - 长乐路752-762号（富民路210弄2-14号）——住宅，建于1941年，第三批上海市优秀历史建筑
  - 长乐路764弄——[长乐新村](../Page/长乐新村.md "wikilink")，原杜美新村，第三批上海市优秀历史建筑
  - 长乐路784、786号——刘氏花园住宅，第四批上海市优秀历史建筑
  - 长乐路788号——[周信芳故居](../Page/周信芳.md "wikilink")，建于1895年
  - 长乐路800号——[住宅](../Page/长乐路800号住宅.md "wikilink")，第三批上海市优秀历史建筑

## 交汇道路（由东向西）

  - [淡水路](../Page/淡水路.md "wikilink")
  - [重庆中路](../Page/重庆中路_\(上海\).md "wikilink")
  - [老重庆中路](../Page/老重庆中路.md "wikilink")
  - [成都南路](../Page/成都南路_\(上海\).md "wikilink")
  - [瑞金一路](../Page/瑞金一路.md "wikilink")
  - [茂名南路](../Page/茂名南路.md "wikilink")
  - [陕西南路](../Page/陕西南路_\(上海\).md "wikilink")
  - [襄阳北路](../Page/襄阳北路.md "wikilink")、[襄阳南路](../Page/襄阳南路.md "wikilink")
  - [富民路](../Page/富民路.md "wikilink")
  - [东湖路](../Page/东湖路.md "wikilink")
  - [华亭路](../Page/华亭路.md "wikilink")
  - [常熟路](../Page/常熟路_\(上海\).md "wikilink")
  - [乌鲁木齐中路](../Page/乌鲁木齐中路.md "wikilink")
  - [华山路](../Page/华山路_\(上海\).md "wikilink")

## 注释

<references />

[Category:卢湾区](../Category/卢湾区.md "wikilink")
[Category:徐汇区](../Category/徐汇区.md "wikilink")
[Category:静安区](../Category/静安区.md "wikilink")
[Category:上海法租界](../Category/上海法租界.md "wikilink")
[Category:上海道路](../Category/上海道路.md "wikilink")
[Category:上海市风貌保护道路](../Category/上海市风貌保护道路.md "wikilink")

1.  今东湖路
2.  今[常熟路](../Page/常熟路_\(上海\).md "wikilink")
3.  今瑞金一路
4.  今乌鲁木齐中路