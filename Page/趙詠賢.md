**趙詠賢**（****，），[綽號](../Page/綽號.md "wikilink")「**小旋風**」，退役[香港女子](../Page/香港.md "wikilink")[壁球](../Page/壁球.md "wikilink")[運動員](../Page/運動員.md "wikilink")，於[2010年亞洲運動會後宣佈退役](../Page/2010年亞洲運動會.md "wikilink")，現時為壁球教練。

## 簡歷

趙詠賢小學時期就讀[香港黃族宗親會黃嗚謙紀念學校](../Page/香港黃族宗親會黃嗚謙紀念學校.md "wikilink")（即現時的[保良局黃族宗親會小學](../Page/保良局黃族宗親會小學.md "wikilink")），9歲時透過小學老師介紹下接觸[壁球](../Page/壁球.md "wikilink")；其後獲推薦進入[香港體育學院集訓](../Page/香港體育學院.md "wikilink")，12歲已成為港隊成員\[1\]。中學時期就讀[賽馬會體藝中學](../Page/賽馬會體藝中學.md "wikilink")，後於2000年在[香港中文大學體育運動科學系畢業](../Page/香港中文大學.md "wikilink")；期間參與多項大賽，並贏得獎項，包括：1997年[亞洲青少年壁球賽及](../Page/亞洲青少年壁球賽.md "wikilink")[世界青少年壁球錦標賽冠軍](../Page/世界青少年壁球錦標賽.md "wikilink")、1999年[曼谷亞運女單銀牌](../Page/曼谷亞運.md "wikilink")、2000年[亞洲壁球錦標賽女單銀牌和團體金牌](../Page/亞洲壁球錦標賽.md "wikilink")，以及[世界大學生運動會女單銀牌](../Page/世界大學生運動會.md "wikilink")。

趙於2000年中正式成為全職壁球運動員，並代表[中國香港出戰](../Page/中國香港.md "wikilink")[2002年釜山亞運會](../Page/2002年釜山亞運會.md "wikilink")，在壁球女子單打項目取得[金牌](../Page/金牌.md "wikilink")。2006年，她在第13屆[亞洲壁球錦標賽取得女子個人亞軍](../Page/亞洲壁球錦標賽.md "wikilink")。2007年1月21日，趙詠賢在香港壁球中心舉行壁球挑戰杯女子組決賽以第一種子身份直落3局撃倒[澳洲選手](../Page/澳洲.md "wikilink")[布朗奪冠](../Page/布朗.md "wikilink")，得到其首個壁球挑戰杯錦標。趙詠賢的教練為港隊總教練[蔡玉坤](../Page/蔡玉坤.md "wikilink")。為準備[2006年多哈亞運會](../Page/2006年多哈亞運會.md "wikilink")，趙詠賢邀請[澳洲女子壁球手](../Page/澳洲.md "wikilink")，世界排名第3位的[喬佩琛與她一同訓練](../Page/喬佩琛.md "wikilink")。

[2009年12月](../Page/2009年12月.md "wikilink")，趙詠賢率領香港壁球代表隊在[東亞運動會橫掃全部](../Page/2009年東亞運動會.md "wikilink")7面金牌，個人則勇奪3金1銀。在東亞運之後，她決定由[2010年4月起轉型為兼職運動員](../Page/2010年4月.md "wikilink")，亦兼任培訓員\[2\][2010年11月](../Page/2010年11月.md "wikilink")，趙最後一次代表[中國香港參加](../Page/2010年亞洲運動會中國香港代表團.md "wikilink")[2010年亞洲運動會](../Page/2010年亞洲運動會.md "wikilink")，夥拍[陳浩鈴](../Page/陳浩鈴.md "wikilink")、[歐詠芝和](../Page/歐詠芝.md "wikilink")[廖梓苓取得女子團體銀牌](../Page/廖梓苓.md "wikilink")；其後宣佈「掛拍」，結束其20多年的運動員生涯\[3\]。2010年9月25日，趙詠賢與拍拖10年的[未婚夫](../Page/未婚夫.md "wikilink")[馮天祐結婚](../Page/馮天祐.md "wikilink")，設宴約30席\[4\]。

## 主要賽事成績

  - 1998年：[亞洲運動會](../Page/1998年亞洲運動會.md "wikilink") 女子單打 銀牌
  - 2002年：[亞洲運動會](../Page/2002年亞洲運動會.md "wikilink") 女子單打 金牌
  - 2006年：第13屆亞洲壁球錦標賽 女子單打 亞軍
  - 2006年：世界女子壁球錦標賽 16強
  - 2006年：[多哈亞運會](../Page/2006年亞洲運動會.md "wikilink") 女子單打 銀牌
  - 2007年：[壁球挑戰杯](../Page/壁球挑戰杯.md "wikilink") 冠軍
  - 2009年：[東亞運動會](../Page/2009年東亞運動會.md "wikilink") 女子單打、女子雙打、女子團體 金牌
  - 2010年：[亞洲運動會](../Page/2010年亞洲運動會.md "wikilink") 女子團體 銀牌

## 榮譽

  - **香港特區政府嘉獎**

<!-- end list -->

  -
    [榮譽勳章](../Page/榮譽勳章_\(香港\).md "wikilink")（MH）（2003年）

<!-- end list -->

  - **[香港傑出運動員選舉](../Page/香港傑出運動員選舉.md "wikilink")**

<!-- end list -->

  -
    「香港傑出運動員」（2000年、2002年、2009年 - 共3次當選）
    「香港最佳進步運動員」（2002年）

## 資料來源

## 外部連結

  - [Squash Info - Rebecca Chiu's
    Profile](http://www.squashinfo.com/players/312-rebecca-chiu)
  - [WISPA Player
    Profile](http://www.horizonsoftware.net/entry/wispa/ranking.php?player=T00220)

[Category:香港壁球運動員](../Category/香港壁球運動員.md "wikilink")
[Category:香港傑出運動員](../Category/香港傑出運動員.md "wikilink")
[Category:2002年亞洲運動會金牌得主](../Category/2002年亞洲運動會金牌得主.md "wikilink")
[Category:2010年亞洲運動會銀牌得主](../Category/2010年亞洲運動會銀牌得主.md "wikilink")
[Category:香港中文大學校友](../Category/香港中文大學校友.md "wikilink")
[Category:賽馬會體藝中學校友](../Category/賽馬會體藝中學校友.md "wikilink")
[W](../Category/趙姓.md "wikilink")
[Category:獲頒授香港榮譽勳章者](../Category/獲頒授香港榮譽勳章者.md "wikilink")

1.  [用汗水映照青春火花
    《中大校友》2000年9月](http://www.alumni.cuhk.edu.hk/magazine/200009/html/p18-20.htm)
2.  [趙詠賢轉型做兼職選手《太陽報》2010年3月24日。](http://the-sun.on.cc/cnt/sport/20100324/00514_006.html)
3.  [趙詠賢掛拍開心無憾《太陽報》2010年11月26日。](http://the-sun.on.cc/cnt/sport/20101126/00514_004.html?pubdate=20101126)
4.  [趙詠賢怕醜　結婚唔肯咀](http://hk.apple.nextmedia.com/sports/art/20100926/14489990)