在[线性代数中](../Page/线性代数.md "wikilink")，**循环矩阵**是一种特殊形式的
[Toeplitz矩阵](../Page/Toeplitz矩阵.md "wikilink")，它的行向量的每个元素都是前一个行向量各元素依次右移一个位置得到的结果。由于可以用[离散傅立叶变换快速解循环矩阵](../Page/离散傅立叶变换.md "wikilink")，所以在[数值分析中有重要的应用](../Page/数值分析.md "wikilink")。

## 定义

形式为

\[C=
\begin{bmatrix}
c_1     & c_2 & c_3 & \dots  & c_n     \\
c_n     & c_1 & c_2 &        & c_{n-1} \\
c_{n-1} & c_n & c_1 &        & c_{n-2} \\
\vdots  &     &     & \ddots & \vdots  \\
c_2     & c_3 & c_4 & \dots  & c_1
\end{bmatrix}\]

的 \(n\times n\) 矩阵 *C* 就是**循环矩阵**。

## 特性

循环矩阵遵循[代数运算法则](../Page/代数.md "wikilink")。对于两个循环矩阵 *A* 与 *B* 来说，*A* + *B*
也是循环矩阵。*AB* 也是循环矩阵，并且 \(AB = BA\)。

循环矩阵的[特征向量矩阵是同样维数的离散](../Page/特征向量.md "wikilink")[傅立叶变换矩阵](../Page/傅立叶变换.md "wikilink")，因此循环矩阵的[特征值可以很容易地通过](../Page/特征值.md "wikilink")[快速傅立叶变换计算出来](../Page/快速傅立叶变换.md "wikilink")。
具体对应关系为傅里叶变换矩阵=N\*特征矩阵，N为循环矩阵维度

## 用循环矩阵来解线性方程

设矩阵方程

\[\mathbf{C} \mathbf{x} = \mathbf{b}\]

其中 *C* 是 *n* 维方形循环矩阵，这样就可以将方程表示成循环[卷积](../Page/卷积.md "wikilink")

\[\mathbf{c} * \mathbf{x} = \mathbf{b}\]

其中 *c* 是循环矩阵 *C*
的第一列，*c*、*x*与*b*分别向每个方向循环。用[离散傅立叶变换将循环卷积转换成两个变量之间的乘积](../Page/离散傅立叶变换.md "wikilink")

\[\mathcal{F}_{n}(\mathbf{c} * \mathbf{x}) = \mathcal{F}_{n}(\mathbf{c}) \mathcal{F}_{n}(\mathbf{x}) = \mathcal{F}_{n}(\mathbf{b})\]

因此

\[\mathbf{x} = \mathcal{F}_{n}^{-1}
\left [
\left (
\frac{(\mathcal{F}_n(\mathbf{b}))_{\nu}}
{(\mathcal{F}_n(\mathbf{c}))_{\nu}}
\right )_{\nu \in \mathbf{Z}}
\right ].\]

这个算法比标准的[高斯消去法的速度要快很多](../Page/高斯消去法.md "wikilink")，尤其是当使用[快速傅立叶变换的时候更是如此](../Page/快速傅立叶变换.md "wikilink")。

## 在图论中的应用

在[图论中](../Page/图论.md "wikilink")，[邻接矩阵为循环矩阵的](../Page/邻接矩阵.md "wikilink")[图与](../Page/图.md "wikilink")[有向图叫作](../Page/有向图.md "wikilink")**轮换图**。同样，如果图的[自同构群包含全部的循环](../Page/自同构群.md "wikilink")，那么图就是轮换图。[Möbius
ladder](../Page/Möbius_ladder.md "wikilink") 就是轮换图的例子。

## 外部链接

  - [Toeplitz and Circulant Matrices: A Review, by R. M.
    Gray](http://www-ee.stanford.edu/~gray/toeplitz.pdf)

[X](../Category/矩陣.md "wikilink") [X](../Category/数值线性代数.md "wikilink")