[Satire_(Orazio)_-_pag._12.JPG](https://zh.wikipedia.org/wiki/File:Satire_\(Orazio\)_-_pag._12.JPG "fig:Satire_(Orazio)_-_pag._12.JPG")
**讽刺**（）是一种[文学手法](../Page/文学.md "wikilink")，用于暴露對象的[矛盾或缺點](../Page/矛盾.md "wikilink")。常采用[夸张或](../Page/夸张.md "wikilink")[反讽等方式](../Page/反讽.md "wikilink")，从而产生[幽默的效果](../Page/幽默.md "wikilink")。当然拙劣模仿、作戏、毗邻、并置、并列、对比、类似、类推等也经常用于讽刺手法种。如果说反讽就是讽刺的话，是一个很大的错误。严格来说，讽刺是一种俗称类型；而反讽则是一种比较具体的修饰手法。

## 歷史

中文的「讽刺」一词在[唐代所写的](../Page/唐代.md "wikilink")《[隋书](../Page/隋书.md "wikilink")》中即已经出现，收录于《[全唐诗](../Page/全唐诗.md "wikilink")》的[高骈](../Page/高骈.md "wikilink")《途次内黄马病，寄僧舍呈诸友人》\[1\]一诗中也提到了该词。諷刺在[日文漢字中寫作](../Page/日文漢字.md "wikilink")「風刺」或「諷刺」。

西方諷刺小說，大概可以上推到[古羅馬帝國希臘語作家](../Page/古羅馬帝國.md "wikilink")[琉善的](../Page/琉善.md "wikilink")「真實的故事」。作者以第一人稱，寫一群人到月亮上面、極樂世界\[2\]等好幾個地方遊歷的經過，標題「真實的故事」就是極大的反諷。

## 作品

後世傑出的諷刺小說有法國[拉伯雷的](../Page/拉伯雷.md "wikilink")《[巨人傳](../Page/巨人傳.md "wikilink")》、英國[斯威夫特的](../Page/斯威夫特.md "wikilink")《[格列佛遊記](../Page/格列佛遊記.md "wikilink")》，都受到路吉阿諾斯小說很深的影響，有很濃的奇幻色彩。也有[法國勒薩日](../Page/法國.md "wikilink")《[吉爾·布拉斯](../Page/吉爾·布拉斯.md "wikilink")》、[西班牙](../Page/西班牙.md "wikilink")[塞萬提斯](../Page/塞萬提斯.md "wikilink")《[堂吉訶德](../Page/堂吉訶德.md "wikilink")》，諷刺性非常強烈，但已經是寫實，不比前面幾種是虛實交錯，以虛為主。[捷克的](../Page/捷克.md "wikilink")《[好兵帥克](../Page/好兵帥克.md "wikilink")》也是諷刺小說的名作。

現代中國諷刺小說名篇有[魯迅](../Page/魯迅.md "wikilink")《[阿Q正傳](../Page/阿Q正傳.md "wikilink")》、《[故事新編](../Page/故事新編.md "wikilink")》；[老舍](../Page/老舍.md "wikilink")《[趙子曰](../Page/趙子曰.md "wikilink")》、《[二馬](../Page/二馬.md "wikilink")》、《[馬褲先生](../Page/馬褲先生.md "wikilink")》、《[駱駝祥子](../Page/駱駝祥子.md "wikilink")》；沈從文《[阿麗思中國遊記](../Page/阿麗思中國遊記.md "wikilink")》；[錢鍾書](../Page/錢鍾書.md "wikilink")《[圍城](../Page/围城_\(小说\).md "wikilink")》等。

讽刺风格的电影有《[大独裁者](../Page/大独裁者.md "wikilink")》、《[独裁者](../Page/独裁者_\(电影\).md "wikilink")》、《[波拉特](../Page/波拉特.md "wikilink")》、《[六亿解放军占领巴黎](../Page/六亿解放军占领巴黎.md "wikilink")》、《[人生遥控器](../Page/人生遥控器.md "wikilink")》、《[预告犯](../Page/预告犯.md "wikilink")》、《[令人讨厌的松子的一生](../Page/令人讨厌的松子的一生_\(电影\).md "wikilink")》、《[诚实国度的爱丽丝](../Page/诚实国度的爱丽丝.md "wikilink")》、《》和《[暴雪将至](../Page/暴雪将至.md "wikilink")》等。在网络时代也有出现专门讽刺低劣流行文化的恶搞作品，如漫画《花开张美丽》（讽刺[玛丽苏情节](../Page/玛丽苏.md "wikilink")）、小说《小明修仙记》（讽刺[修真小说](../Page/修真.md "wikilink")）、小说《禽兽不如的穿越女》（讽刺[穿越文](../Page/穿越.md "wikilink")）和[赵日天系列作品](../Page/赵日天.md "wikilink")。

## 註

<references group="注"/>

[F](../Category/諷刺.md "wikilink") [F](../Category/幽默.md "wikilink")

1.  高駢《途次內黃馬病，寄僧舍呈諸友人》全文：官閒馬病客深秋，肯學張衡詠四愁。 紅葉寺多詩景緻，白衣人盡酒交遊。
    依違諷刺因行得，淡泊供需不在求。 好與高陽結吟社，況無名跡達珠旒。
2.  [周作人譯](../Page/周作人.md "wikilink")「福人島」