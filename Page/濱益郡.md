**濱益郡**（）為過去[日本](../Page/日本.md "wikilink")[北海道](../Page/北海道.md "wikilink")[石狩支廳轄下的郡](../Page/石狩支廳.md "wikilink")，轄區包含現在[石狩市的北部地区](../Page/石狩市.md "wikilink")。已於2005年10月1日因無管轄町村而廢除。
[Sapporo-shicho.png](https://zh.wikipedia.org/wiki/File:Sapporo-shicho.png "fig:Sapporo-shicho.png")

## 沿革

  - 1869年8月15日：北海道設置11國86郡，[石狩國濱益郡成立](../Page/石狩國.md "wikilink")。
  - 1897年11月：北海道實施支廳制，此時濱益郡下轄茂生村、群別村、川下村、尻苗村、清水村、柏木村、實田村。\[1\]（7村）
  - 1902年4月1日：茂生村、群別村合併為[濱益村](../Page/濱益村.md "wikilink")，川下村、尻苗村、清水村、柏木村、實田村合併為[黃金村](../Page/黃金村.md "wikilink")，兩村皆成為北海道二級村。（2村）
  - 1907年4月1日：黃金村併入濱益村，並成為北海道一級村。（1村）
  - 2005年10月1日：濱益村和[厚田郡](../Page/厚田郡.md "wikilink")[厚田村併入](../Page/厚田村.md "wikilink")[石狩市](../Page/石狩市.md "wikilink")；同時濱益郡因無管轄町村而廢除。

## 參考資料

[Category:石狩國](../Category/石狩國.md "wikilink")

1.