}}
[Photographic_Studies_in_Hypnosis,_Abnormal_Psychology_(1938).ogv](https://zh.wikipedia.org/wiki/File:Photographic_Studies_in_Hypnosis,_Abnormal_Psychology_\(1938\).ogv "fig:Photographic_Studies_in_Hypnosis,_Abnormal_Psychology_(1938).ogv")
**催眠术**（Mesmerism or
Hypnotism）最早出现于18世纪中叶的奥地利，[弗朗茨·梅斯梅尔](../Page/弗朗茨·梅斯梅尔.md "wikilink")（Franz
Anton Mesmer 1734-1815)将其理论化和系统化，
然后以他的名字定名催眠术为Mesmerism，后来英国人[詹姆斯·布雷德](../Page/詹姆斯·布雷德.md "wikilink")（James
Braid）命名为Hypnotism，此后逐渐传播到世界各地。

## 历史

催眠或已有数千年的[历史](../Page/历史.md "wikilink")，早在五千多年前的[埃及](../Page/埃及.md "wikilink")，以及[中国古老的](../Page/中国.md "wikilink")[祝由术](../Page/祝由术.md "wikilink")、导引术，或是其他民族的[巫术都有催眠的痕迹可寻](../Page/巫术.md "wikilink")，但都高深於現代催眠。現代催眠最早以催眠术鼻祖麦斯买的名字命名为Mesmerism，至今约有两百多年的历史。
后来由[英国曼彻斯特的外科医生](../Page/英国.md "wikilink")[詹姆斯·布雷德](../Page/詹姆斯·布雷德.md "wikilink")（James
Braid）将催眠定义为Hypnosis。在[希腊神话中Hypnos是](../Page/希腊神话.md "wikilink")[睡神](../Page/睡神.md "wikilink")，相传Hypnos住在[冥界](../Page/冥界.md "wikilink")，他的左手拿着[罂粟花蕾](../Page/罂粟花.md "wikilink")，右手则是持着一支[牛角](../Page/牛角.md "wikilink")，牛角里装满了液体，这种[液体可以令人进入睡眠](../Page/液体.md "wikilink")。如果被他的[魔法棒轻触到](../Page/魔法棒.md "wikilink")[眼睛](../Page/眼睛.md "wikilink")，无论是[人或者](../Page/人.md "wikilink")[神](../Page/神.md "wikilink")，都会无法抗拒的进入梦乡。

中国现代催眠术是由国外传入，1903年留日学生王若俨翻译了《催眠术实施法》。1905年有留日学生江吞等翻译《催眠术精理》，1905年陶成章在上海讲授催眠术，其后《催眠术讲义》出版。留日学生余萍客1909年在日本横滨创立了中国心灵俱乐部，研究传播催眠术。留日学生鲍芳洲1910年在日本神户组织华侨催眠术研究社，传播教授催眠术。

## 基本要素

大多数人认为，催眠能否成功，主要应该看[催眠师的](../Page/催眠师.md "wikilink")[技艺如何](../Page/技艺.md "wikilink")，认为催眠师在这里扮演最重要的[角色](../Page/角色.md "wikilink")，事實上一个人的催眠能否成功其关键因素除了催眠师本身的[經驗及技藝](../Page/經驗.md "wikilink")\[1\]外，另一個重要的[因素是共同的](../Page/因素.md "wikilink")[信任](../Page/信任.md "wikilink")，在某个[层面上而言](../Page/层面.md "wikilink")，催眠师本身应该具备快速与个案建立[亲和感的能力](../Page/亲和感.md "wikilink")，被催眠者是否具有高度的[受暗示性](../Page/受暗示性.md "wikilink")（可催眠性），也应该是决定催眠进入何种深度的重要关键。
然而深度不一定能夠決定一切，多數的催眠在一級及二級左右深度就可做到，那樣的狀態下，被催眠者十分的清醒，多數的被催眠者僅是感覺自己閉上眼睛而已，並感覺不到跟清醒中有太多的不同。
事实上，每个人都具备被催眠的能力，却又因为每个人的状态各不相同，所以进入催眠的速度及深度以及接受催眠的建议也都各不相同。
其实人类每天都活在[自我暗示或他人暗示之中](../Page/自我暗示.md "wikilink")，这种受暗示性我们每一个人每一天都在进行着，只不过一般人接受到暗示也不以为意，就像是我们在看[电视时](../Page/电视.md "wikilink")，看到的[广告一样](../Page/广告.md "wikilink")。只不过平日的接受的程度没有那么高罢了。有些具有高度的受暗示性（可催眠性）的人，可以非常容易的被催眠师用暗示的方法进行催眠，这些人不仅容易受到别人的暗示而进入催眠状态，进行自我催眠的能力也会很强，事实上每个人都可以进行自我催眠，人类的每一个想法及意识都可以算是一种自我催眠。高度的受暗示性（可催眠性）可以被看作是一种與生俱来的[属性](../Page/属性.md "wikilink")（天赋），然而也可以经过专业后天的开发。

## 一般人的可催眠性

每个人都有受暗示性（可催眠性），只不过程度不同。一般来说这种可受暗示性的程度是相对稳定的，但据说也受[年龄的影响](../Page/年龄.md "wikilink")。一般认为在[儿童时期较高](../Page/儿童.md "wikilink")，在[青少年时期达到最高](../Page/青少年.md "wikilink")，之后随着年龄增长逐渐下降。有证据表明这种可催眠性是受[遗传影响的](../Page/遗传.md "wikilink")，但同[人格](../Page/人格.md "wikilink")，[个性都没有直接的关系](../Page/个性.md "wikilink")。据说这种可催眠性也可以通过[训练而进行强化](../Page/训练.md "wikilink")。有[学者认为感觉剥夺也可以使可催眠性有所改变](../Page/学者.md "wikilink")，但改变不会很大。
可催眠性与人的性格有关系，一般来说[想象力较丰富的人他们的受暗示性会比较高一些](../Page/想象力.md "wikilink")，而批审性较强的人他们的的受暗示性会比较低一些。
最重要是被催眠者的意愿，即使他对暗示的感受性高，但是没有意愿的话，是进不了催眠的。

## 催眠的方法

大多数是使被催眠者完全放松，然后再使用暗示的方法对被催眠者进行催眠，包括[言语的暗示](../Page/言语.md "wikilink")、[环境的暗示等](../Page/环境.md "wikilink")。言语暗示是用言语的形式，将一些暗示的信息传达给被催眠者。如对被催眠者说：“你现在置身在一个非常幽静的[森林裡](../Page/森林.md "wikilink")。”环境暗示是让被催眠者处在一个适合催眠，有助于使被催眠者进入催眠状态的场所，如室内灯光的光线，室内的[音乐](../Page/音乐.md "wikilink")，室内的陈设等。但亦可在紧张和嘈杂的地方进行。
多數在電影裡出現的彈手指聲為催眠開局，是因彈手指聲音能夠對腦波產生一種提醒與暗示性。

## 催眠原理

說到催眠原理，一定要從[腦波開始講起](../Page/腦波.md "wikilink")。腦波有四種：β、α、θ、δ波。
當人在日常生活中，所呈現的是β波，稱為一般狀態。 當人在心情平靜下，所呈現的是α波，稱為放鬆狀態。
θ是打盹波，稱為打盹狀態。 δ是酣睡波，稱為熟睡狀態。 而催眠就是在α波和θ波狀態下進行的。

## 為什麼要催眠

醫學報告指出，許多的疾病都是由長期壓力和煩惱的形成的，當然做人難免會有壓力和煩惱，當人們遭遇到壓力時，大多數的人都會選擇，喝酒、唱歌、跳舞、看電影、逛街等等，來舒解個人的壓力和煩惱，這種放鬆是有益的，畢竟心情要平靜才比較有能力省察放鬆前的煩惱，腦波呈現α波時（也就是催眠狀態下），才是潛意識打開的時候，同時也帶動間腦的啟動，有助於解決所有問題的緊張根源。部分[LGAT課程也會運用催眠術](../Page/LGAT課程.md "wikilink")。

## 功效

催眠疗法：催眠疗法（Hypnotherapy）是指用催眠的方法使求治者的意识范围变得极度狭窄，借助暗示性语言，以消除病理[心理和躯体障碍的一种心理治疗方法](../Page/心理.md "wikilink")。通过催眠方法，将人诱导进入一种特殊的意识状态，将[医生的言语或动作整合入患者的思维和情感](../Page/医生.md "wikilink")，从而产生治疗效果。

催眠可以很好的推动人潜在的能力，现在一些[心理治疗的方法是使用催眠来治疗人的一些](../Page/心理治疗.md "wikilink")[心理疾病](../Page/心理疾病.md "wikilink")，如[强迫症](../Page/强迫症.md "wikilink")，[忧郁症](../Page/忧郁症.md "wikilink")，坏习惯，[情绪问题等](../Page/情绪.md "wikilink")。

1969年美國的賓州大學的麥格拉山發現，不容易被催眠的人，使用催眠止痛的效果和強效止痛劑的結果是一樣的。但可被高度催眠的人利用催眠來止痛可達到止痛劑效果的3倍。

## 催眠運用

催眠的運用在於：治療[憂鬱症](../Page/憂鬱症.md "wikilink")、治療[肥胖](../Page/肥胖.md "wikilink")、協助戒煙、改善[睡眠品質](../Page/睡眠.md "wikilink")、解除心理壓力，[信心重建](../Page/信心.md "wikilink")、治療[創傷症候群](../Page/創傷症候群.md "wikilink")（如[美國衛生部輔導的](../Page/美國衛生部.md "wikilink")[伊拉克戰場](../Page/伊拉克.md "wikilink")[士兵回國的創傷治療計畫](../Page/士兵.md "wikilink")）、[恐懼症狀克服](../Page/恐懼.md "wikilink")（如：[幽闭恐惧症](../Page/幽闭恐惧症.md "wikilink")、[飛行恐懼症](../Page/飛行.md "wikilink")、[演講恐懼症](../Page/演講.md "wikilink")）、戒除[強迫性行為](../Page/強迫性行為.md "wikilink")......

## 臨床案例

### 前世回溯

[前世回溯](../Page/前世回溯.md "wikilink")，是透過運用心理學及催眠的方式，進行記憶回溯，讓人們回溯自己對[前世的經歷及記憶](../Page/前世.md "wikilink")。

## 催眠的深度

  - 催眠状态的深浅可分成：[浅度催眠](../Page/浅度催眠.md "wikilink")；[中度催眠](../Page/中度催眠.md "wikilink")；[深度催眠](../Page/深度催眠.md "wikilink")，一般粗分为6级深度。

六級深度測試順序：1級眼皮膠黏→2級手臂僵直→3級數字遺忘→4級疼痛喪失→5級正性幻覺→6級負性幻覺

## 催眠的理论

[實驗發現](../Page/實驗.md "wikilink")，每個人進入催眠狀態都有些不同，進入催眠時的[腦波形態也有許多種型態](../Page/腦波.md "wikilink")，有些人腦波會與清醒時的相同，因此不支持催眠是睡眠的另一種特殊狀態的看法。

下面是三种较重要的理论：

### 部份退化（partial regression）理论

催眠使受试者思维[退化至某种较](../Page/退化.md "wikilink")[幼稚的阶段](../Page/幼稚.md "wikilink")，失去了正常清醒时所具有的控制，落入一种较原始的思维方式，因而凭冲动行事并进行幻想与幻觉的制作（Gill,
1972）。

### 角色扮演（role playing）理论

认为是受试者在催眠者的诱导下过度合作地扮演了另外一个角色。受试者对角色的期望和情景因素，使他们以高度合作的态度做出了某些动作（Barber,
1979 & Spannos,
1986）。但很多学者坚持催眠是意识的另一种状态，而不是角色扮演，因为即使最合作的受试者也不会同意在不给[麻醉药的情况下进行手术](../Page/麻醉药.md "wikilink")。角色扮演學者則認為，在適當的誘導下譬如酬勞或獎賞，還是有可能使受試者接受無麻醉的手術。

### 意识分离（dissociation in consciousness）理论

[欧内斯特·希尔加德](../Page/欧内斯特·希尔加德.md "wikilink")（Ernest Ropiequet Hilgard,
1977）根据实验观察，认为催眠将受试者的心理过程分离为两个（或两个以上）同时进行的分流。第一个分流是受试者所经历的意识活动，性质可能是扭曲的；第二个分流是受试者难于察觉、被掩蔽的意识活动，但其性质是比较真实的，希尔加德称之为「[隐蔽观察者](../Page/隐蔽观察者.md "wikilink")」。意识分离是生活中一种经常出现的正常体验，例如长途驾车的人对路上状况作出了一些反应但多不能回忆，就是由于当时意识明显地分离为驾驭汽车与个人思考两部份了。

## 參見

  - [暗示](../Page/暗示.md "wikilink")
  - [意识](../Page/意识.md "wikilink")
  - [潜意识](../Page/潜意识.md "wikilink")

## 参考文献

  - Hypnosis for the Seriously Curious, by Kenneth Bowers. NY: W. W.
    Norton (1993).
  - Hypnosis and Suggestion in the Treatment of Pain: A Clinical Guide,
    by Joseph Barber. NY: Norton (1996).
  - Mind control, Research by G. Wagstaff, Dept. of Psychology,
    University of Liverpool
  - Hypnosis, Compliance and Belief by G. Wagstaff, (1981).
  - The Highly Hypnotizable Person, Michael Heap, Richard J. Brown &
    David A. Oakley, (2004), Routledge
  - Better and Better Every Day, [Emile
    Coue](../Page/Emile_Coue.md "wikilink"), (1960).
  - Uncommon Therapy, [Jay Haley](../Page/Jay_Haley.md "wikilink")（about
    the psychotherapeutic intervention techniques of [Milton
    Erickson](../Page/Milton_Erickson.md "wikilink")）
  - Advanced Self Hypnosis, [Melvin
    Powers](../Page/Melvin_Powers.md "wikilink"), Thorsons Publishers,
    1973, ISBN 0-7225-0058-0
  - Molly Moon's Incredible Book of Hypnotism, [Georgia
    Byng](../Page/Georgia_Byng.md "wikilink")
  - Open to suggestion. The uses and abuses of hypnosis. [Robert
    Temple](../Page/Robert_Temple.md "wikilink"), 1989, ISBN
    1-85030-710-4
  - Hypnosis With Friends and Lovers [Freda
    Morris](../Page/Freda_Morris.md "wikilink"), 1979, ISBN
    0-06-250600-5
  - Clinical and Experimental Hypnosis [William S. Kroger,
    M.D.](../Page/William_S._Kroger,_M.D..md "wikilink"), 1977, ISBN
    0-397-50377-6
  - EBooks: [The Power of Creative
    Visualization](https://web.archive.org/web/20051223105343/http://pradeepaggarwal.com/cvisual.HTM),
    [Personal Transformation in 7
    Weeks](https://web.archive.org/web/20051223110520/http://pradeepaggarwal.com/7weeks.html)
    by [Pradeep Aggarwal](http://www.pradeepaggarwal.com).
  - [催眠是甚麼？](https://www.jcmtherapy.com/%E5%82%AC%E7%9C%A0/)JC催眠治療.

[Category:心理学](../Category/心理学.md "wikilink")
[Category:治疗](../Category/治疗.md "wikilink")
[Category:民間療法](../Category/民間療法.md "wikilink")
[催眠](../Category/催眠.md "wikilink")

1.   芳網|accessdate=2018-11-26|work=fangweb.com}}