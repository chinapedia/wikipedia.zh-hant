**鸭嘴龙**（[学名](../Page/学名.md "wikilink")*Hadrosaurus*）意為「健壯的蜥蜴」，是[鴨嘴龍科的一個](../Page/鴨嘴龍科.md "wikilink")[疑名](../Page/疑名.md "wikilink")。在1858年，發現了第一個鴨嘴龍的化石，這也是[北美洲所發現的第一個完整的](../Page/北美洲.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")[化石](../Page/化石.md "wikilink")；在1868年，鴨嘴龍成為第一個架設起來的恐龍化石模型。**佛克鴨嘴龍**（*H.
foulkii*）為鴨嘴龍的唯一[物種](../Page/物種.md "wikilink")，並在1991年成為[紐澤西州的](../Page/紐澤西州.md "wikilink")[州恐龍](../Page/州恐龍.md "wikilink")。

## 敘述

鴨嘴龍生存於當時[美國](../Page/美國.md "wikilink")[紐澤西州的海岸附近](../Page/紐澤西州.md "wikilink")，時間是晚[白堊紀](../Page/白堊紀.md "wikilink")，約8,000萬年前。大多數資料顯示鴨嘴龍身長7到10公尺，高達3公尺，重量約7噸。鴨嘴龍可能以二足方式奔跑，但當進食時可用前肢支撐自己；如同所有鴨嘴龍科，鴨嘴龍是[草食性](../Page/草食性.md "wikilink")。鴨嘴龍的[牙齒顯示牠們吃樹枝](../Page/牙齒.md "wikilink")、樹葉。

## 發現

[Hadrosaurus_lithograph.jpg](https://zh.wikipedia.org/wiki/File:Hadrosaurus_lithograph.jpg "fig:Hadrosaurus_lithograph.jpg")\]\]
在1838年，William Estaugh
Hopkins在[紐澤西州](../Page/紐澤西州.md "wikilink")[哈登菲爾德Hopkins](../Page/哈登菲爾德.md "wikilink")
Pond的一個泥灰坑裡挖掘時，他發現了大型骨頭。他將骨頭運回[哈登菲爾德的住家中展示](../Page/哈登菲爾德.md "wikilink")。在1858年，這些骨頭引起一位訪客，[威廉·帕克·佛克](../Page/威廉·帕克·佛克.md "wikilink")（William
Parker
Foulke）的興趣。佛克在1858年將泥灰坑中的骨骸挖出。同年，[古生物學家](../Page/古生物學家.md "wikilink")[約瑟夫·萊迪](../Page/約瑟夫·萊迪.md "wikilink")（Joseph
Leidy）將這標本命名，這標本有[骨盆](../Page/骨盆.md "wikilink")、幾乎完整的後腿、腳掌的數個部份、28個[脊椎骨](../Page/脊椎骨.md "wikilink")（其中8個是[尾椎](../Page/尾椎.md "wikilink")）、8個[牙齒](../Page/牙齒.md "wikilink")、以及頜部的兩個部份。萊迪從骨頭與[禽龍的相似處](../Page/禽龍.md "wikilink")，認為這些骨頭來自於[恐龍](../Page/恐龍.md "wikilink")，但這鴨嘴龍的骨骸遠比數十年前發現的禽龍骨骸還完整，也是當時所發現最完整的恐龍化石。在萊迪的論文《美國的白堊紀爬行動物》（*Cretaceous
Reptiles of the United
States*）裡，用想像圖將鴨嘴龍敘述的更完整；萊迪在1860年完成了該論文，但因[南北戰爭而延遲到](../Page/南北戰爭.md "wikilink")1865年才出版。萊迪將鴨嘴龍重建為二足恐龍，有別於該時代認為恐龍為四足動物的觀點。整副骨骸在1868年組合完成，由一個[英國雕刻家與博物學家](../Page/英國.md "wikilink")[班傑明·瓦特豪斯·郝金斯](../Page/班傑明·瓦特豪斯·郝金斯.md "wikilink")（Benjamin
Waterhouse Hawkins）組成的團隊完成，並在[費城自然科學館展出](../Page/費城自然科學館.md "wikilink")。
[Hadrosaurus_foulkii.jpg](https://zh.wikipedia.org/wiki/File:Hadrosaurus_foulkii.jpg "fig:Hadrosaurus_foulkii.jpg")與鴨嘴龍的骨架模型，這是全球第一個恐龍骨架模型\]\]
雖然[鴨嘴龍科的名稱來自於鴨嘴龍屬](../Page/鴨嘴龍科.md "wikilink")，但從沒發現過鴨嘴龍的[頭顱骨](../Page/頭顱骨.md "wikilink")，而骨骸不易與其他鴨嘴龍亞科恐龍分辨出來，讓大部分科學家認為鴨嘴龍是個[疑名](../Page/疑名.md "wikilink")。許多藝術家重建鴨嘴龍時，都是參考相關物種的頭顱，例如[格理芬龍](../Page/格理芬龍.md "wikilink")、[短冠龍](../Page/短冠龍.md "wikilink")。

一個由哈登菲爾德居民John Giannotti完成的鴨嘴龍塑像，現在豎立在哈登菲爾德的市中心，以紀念這個發現。

## 大眾文化

[艾瑞克·加西亞](../Page/艾瑞克·加西亞.md "wikilink")（Eric Garcia）小說《Anonymous
Rex》的角色Glenda，是隻鴨嘴龍。

在[麥可·克萊頓](../Page/麥可·克萊頓.md "wikilink")（Michael
Crichton）的小說《[侏儸紀公園](../Page/侏羅紀公園_\(小說\).md "wikilink")》裡，鴨嘴龍是InGen機構所複製的恐龍之一。

## 外部連結

  - [恐龙.NET](https://web.archive.org/web/20170527222928/http://dinosaur.net.cn/)——中国古生物门户网站。
  - [The discovery of *H.
    foulkii*](http://www.levins.com/dinosaur.shtml)
  - [Hadrosaurids on
    UCMP](http://www.ucmp.berkeley.edu/diapsids/ornithischia/hadrosauria.html)
  - [紐澤西州博物館](http://www.state.nj.us/dep/njgs/enviroed/hadro.htm)
  - [The Academy of Natural
    Sciences](http://www.acnatsci.org/museum/leidy/paleo/hadrosaurus.html)

[Category:鴨嘴龍亞科](../Category/鴨嘴龍亞科.md "wikilink")
[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")
[Category:北美洲恐龍](../Category/北美洲恐龍.md "wikilink")
[Category:可疑的恐龍](../Category/可疑的恐龍.md "wikilink")