[M-V_with_ASTRO-E2_on_launch_pad.jpeg](https://zh.wikipedia.org/wiki/File:M-V_with_ASTRO-E2_on_launch_pad.jpeg "fig:M-V_with_ASTRO-E2_on_launch_pad.jpeg")

**内之浦宇宙空間観測所**（**Uchinoura Space Center**
，略稱USC）是[日本](../Page/日本.md "wikilink")[宇宙科學研究所](../Page/宇宙科學研究所.md "wikilink")（現為[宇宙航空研究開發機構](../Page/宇宙航空研究開發機構.md "wikilink")）所有的火箭發射中心，位於[鹿兒島縣](../Page/鹿兒島縣.md "wikilink")[內之浦町](../Page/內之浦町.md "wikilink")，本火箭發射中心也是世界罕見的山地發射台。

在此發射的火箭主要是做為科學探測用途，以及相對小型的[固體燃料火箭](../Page/固體燃料火箭.md "wikilink")。

## 概要

本太空中心在1962年動工、1963年12月落成當時是宇宙科學研究所底下的鹿兒島宇宙空間觀測所，現名是[JAXA在](../Page/JAXA.md "wikilink")2003年整編後改稱的。

日本從這邊發射了其第一顆衛星[大隅號之後](../Page/大隅號.md "wikilink")，宇宙科學研究所也在這邊繼續發射了後來著手研發的科學探測衛星、宇宙探測機等，包括在天文迷中普遍稱為[哈雷艦隊內](../Page/哈雷艦隊.md "wikilink")，屬於日本的[先驅號](../Page/先驅號.md "wikilink")、[彗星號](../Page/彗星號.md "wikilink")，以及後來首次達成小行星樣本採集返回任務的[隼鳥號都是在這裡發射的](../Page/隼鳥號.md "wikilink")。

此外，內之浦發射台很特別的一點是發射台採用斜向發射台（而不是一般常見的直立發射台），這是為了萬一火箭發生意外的時候，靠海的這個發射台發射的火箭會直接落海，而能將影響地面造成傷亡的程度減到最小，不過在[Epsilon運載火箭研發完成後](../Page/艾普斯龍運載火箭.md "wikilink")，因為火箭穩定度提高，外加斜向發射時無法設置煙道，因此產生的摩擦、噴射反作用力與振動都容易對搭載於火箭上的衛星產生損害，因此發射Epsilon火箭時便改回直立發射台。

## 外部連結

  - [](http://www.jaxa.jp/about/centers/usc/)

[Category:航天发射中心](../Category/航天发射中心.md "wikilink")
[Category:日本航天](../Category/日本航天.md "wikilink")
[Category:肝付町](../Category/肝付町.md "wikilink")
[Category:航天中心](../Category/航天中心.md "wikilink")