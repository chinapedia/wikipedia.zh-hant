{{ Expand |time=2010-09-24}}
**蓬萊米**，是一種在[台灣廣為食用的](../Page/台灣.md "wikilink")[稻米品種的概稱](../Page/稻米.md "wikilink")，依時代的不同，而有不同的定義。現今概稱的蓬萊米，為[粳米和](../Page/粳稻.md "wikilink")[籼米的混種](../Page/籼稻.md "wikilink")，初始的蓬萊米品種，係由[日本稻作專家](../Page/日本.md "wikilink")[磯永吉與](../Page/磯永吉.md "wikilink")[末永仁改良成功](../Page/末永仁.md "wikilink")\<ref
="人物特寫：男發現失落手稿 修正稻米史"\></ref>。
1921年，於[台北](../Page/台北.md "wikilink")[草山](../Page/草山.md "wikilink")（今[陽明山](../Page/陽明山.md "wikilink")）上的[竹子湖栽種成功](../Page/竹子湖.md "wikilink")，後逐步推廣。

1926年（[大正](../Page/大正.md "wikilink")15年）4月24日，由[伊澤多喜男總督在](../Page/伊澤多喜男.md "wikilink")[臺灣鐵道飯店召開的日本米榖大會中正式公布命名為](../Page/臺灣鐵道飯店.md "wikilink")「蓬萊米」，以有別於台灣本土品種的「[在來米](../Page/在來米.md "wikilink")」（另外幾個候選名稱包括「新台米」、「新高米」），蓬萊米遂逐漸成為這種新品種稻米的概稱，包括日後廣為種植的「臺中六十五號」等品種。

## 由來

[台灣種植稻米](../Page/台灣.md "wikilink")，可追溯至新石器時代。後隨中國移民大量遷徙前來，帶來秈稻品種的稻種，而成為秈稻農業區域的一環；此種稻米在[日治時期被稱為](../Page/台灣日治時期.md "wikilink")[在來米](../Page/在來米.md "wikilink")〈即原有的本地秈米〉。在臺灣的環境中，可一年兩收，甚至少部分地域內並可有三熟，長期以來成為銷往中國地區的重要糧食及經濟作物。

但在日治時期，臺灣在來米的口感並不合習慣粳米的[日本人之口味](../Page/日本人.md "wikilink")，[臺灣總督府農業試驗所因此引進](../Page/臺灣總督府.md "wikilink")[日本本土品種](../Page/內地_\(大日本帝國\).md "wikilink")[粳米](../Page/粳稻.md "wikilink")，經過多次試驗改良研究後，[磯永吉技士於](../Page/磯永吉.md "wikilink")1922年培植出成功的新品種。1926年，由[伊澤多喜男總督在台北鐵道飯店召開的日本米榖大會中正式命名為](../Page/伊澤多喜男.md "wikilink")「蓬萊米」，而[磯永吉也被尊稱為](../Page/磯永吉.md "wikilink")「蓬萊米之父」，蓬萊米的口味也廣為台灣人接受。

蓬莱米的外觀，比在來米短而粗，呈椭圆形，米粒較大、黏性強，能適應高溫，肥料反應性高，故在多肥的配合下，產量可比在來米高。但也因如此，蓬萊米的栽種也必須使用大量的[肥料](../Page/肥料.md "wikilink")。

蓬萊米也是台灣外銷米種中口碑甚好的一種，曾大量輸往日本本土。在台灣，也廣被用來[釀酒](../Page/釀酒.md "wikilink")，如[米酒就是使用蓬萊米所釀成的](../Page/米酒.md "wikilink")。

## 天皇米

  - 臺灣日治時期，[天皇所吃的大米多從台灣](../Page/日本天皇.md "wikilink")「葫蘆墩郡」（今[台中市](../Page/臺中市.md "wikilink")[豐原區南郊](../Page/豐原區.md "wikilink")）[進貢](../Page/進貢.md "wikilink")，[御田有日軍](../Page/御田.md "wikilink")[連隊駐守](../Page/連隊.md "wikilink")、收割（現仍存日軍營房），即[葫蘆墩米](../Page/葫蘆墩米.md "wikilink")；日本戰敗後到公元1953年，日本政府仍向台灣購買葫蘆墩米做為御用米。

## 參考資料

## 相關

  - [添壽米](../Page/添壽米.md "wikilink")
  - [越光米](../Page/越光米.md "wikilink")

[Category:稻米品種](../Category/稻米品種.md "wikilink")
[Category:臺灣栽培品種](../Category/臺灣栽培品種.md "wikilink")
[Category:台灣食材](../Category/台灣食材.md "wikilink")
[Category:粮食](../Category/粮食.md "wikilink")
[Category:谷物](../Category/谷物.md "wikilink")
[Category:台灣日治時期](../Category/台灣日治時期.md "wikilink")