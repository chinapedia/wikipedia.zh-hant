**大地懒**（[學名](../Page/學名.md "wikilink")：）是一種巨大的[動物](../Page/動物.md "wikilink")，屬於[異關節總目](../Page/異關節總目.md "wikilink")[披毛目](../Page/披毛目.md "wikilink")[樹懒亞目下的大地懒屬的物種](../Page/樹懒亞目.md "wikilink")。見於[上新世早期至](../Page/上新世.md "wikilink")[更新世](../Page/更新世.md "wikilink")\[1\]晚期的[中美洲和](../Page/中美洲.md "wikilink")[南美洲](../Page/南美洲.md "wikilink")。在所有其他陸生哺乳動物當中，只有少數生物體形可以超越大地懶，包括[猛瑪象以及更古老的](../Page/猛瑪象.md "wikilink")[巨犀](../Page/巨犀.md "wikilink")。

## 描述

[Megatherum_DB.jpg](https://zh.wikipedia.org/wiki/File:Megatherum_DB.jpg "fig:Megatherum_DB.jpg")
[Megatherium_cuvieri_-_skull.JPG](https://zh.wikipedia.org/wiki/File:Megatherium_cuvieri_-_skull.JPG "fig:Megatherium_cuvieri_-_skull.JPG")
大地懒是少數已知體型最大的陸生[哺乳動物之一](../Page/哺乳動物.md "wikilink")，體重可達4[噸](../Page/噸.md "wikilink")\[2\]，全長從頭至尾端則可達6[公尺](../Page/公尺.md "wikilink")\[3\]\[4\]，屬於目前已知最大型的[地懶](../Page/地懶.md "wikilink")，體型甚至可比擬現代的[大象](../Page/大象.md "wikilink")。大地懶屬於[更新世巨型動物群中的一員](../Page/更新世巨型動物群.md "wikilink")，為當時更新世最大型的數種哺乳動物之一。

大地懶擁有厚重的[骨架結構](../Page/骨骼系統.md "wikilink")、寬厚的骨盆與強壯的尾巴，搭配其巨大的體型讓大地懶可以使用後腳和尾巴如三腳架般穩定直立，如此一來便可以獲取其他同時期[食草動物所不能搆到的食物](../Page/食草動物.md "wikilink")。大地懶可以運用[前肢上的彎爪勾取樹枝並取食其上之樹葉](../Page/前肢.md "wikilink")。和[食蟻獸一樣](../Page/食蟻獸.md "wikilink")，大地懶的足上也有[爪](../Page/爪.md "wikilink")，這使得大地懶的腳掌無法平整的貼於地面而必須以足外緣著地來行走。大地懶為[四足動物](../Page/四足動物.md "wikilink")，但是從牠的[行跡化石上與生物力學](../Page/行跡化石.md "wikilink")\[5\]的角度觀察來看，大地懶也具有不差的[雙足行走能力](../Page/兩足動物.md "wikilink")。

有些科學家相信大地懶擁有一條長舌頭（類似於[長頸鹿和](../Page/長頸鹿.md "wikilink")[樹懶](../Page/樹懶.md "wikilink")），這樣一來就可以透過舌頭將樹葉捲入口中，但這個理論並沒有得到全面性的認同。

## 生活習性

大地懶主要在[南美洲](../Page/南美洲.md "wikilink")\[6\]疏林地區上的[林地和](../Page/林地.md "wikilink")[草原生活](../Page/草原.md "wikilink")，為當地之[特有種並且持續存活直到約](../Page/特有種.md "wikilink")10,000年前。大地懶可以適應溫帶、乾旱性與半乾旱性氣侯的環境，其近親[泛美地懶](../Page/泛美地懶.md "wikilink")（Eremotherium）\[7\]居住在更加北部的熱帶地區，甚至在[南北美洲生物大遷徙時](../Page/南北美洲生物大遷徙.md "wikilink")，成功入侵到溫帶的[北美洲](../Page/北美洲.md "wikilink")。

## 古生物學

[Megatherium_americanum.jpg](https://zh.wikipedia.org/wiki/File:Megatherium_americanum.jpg "fig:Megatherium_americanum.jpg")
大地懶通常為群體生活，但也有部分個體會單獨生活在[洞穴中](../Page/洞穴.md "wikilink")。大地懶並沒有什麼天敵，因此可能為晝行性生物。

大地懶為草食動物，以啃食[絲蘭](../Page/絲蘭屬.md "wikilink")、[龍舌蘭的葉子或者是吃](../Page/龍舌蘭.md "wikilink")[草為主](../Page/禾本科.md "wikilink")，但也會透過後腳直立來取得高樹上的嫩枝與樹葉。大地懶透過強壯的頰肌與構造簡單的牙齒來咀嚼，並透過胃來消化這些包含粗劣纖維的食物，也因此大地懶可能會耗費大量時間休息來協助消化。

近年來的研究\[8\]指出美洲大地懶（*M.
americanum*）具有強大的垂直咬合力，從牙齒的形狀指出大地懶可能更適合切斷而不是去研磨食物，也因此高纖食物可能不是主要的食物來源。

有些仍有爭論性的理論指出大地懶可能為部分[肉食性](../Page/肉食動物.md "wikilink")，研究指出其肘突（手肘上[三頭肌附著點](../Page/肱三頭肌.md "wikilink")）很短，適合強化揮動速度而不是力量，這個特徵通常只會在[肉食動物上發現](../Page/肉食動物.md "wikilink")，這讓大地懶可以讓其利爪像匕首那樣揮舞。大地懶可以透過食肉來攝取一些額外的營養，可能透過奪取[斯劍虎的戰利品](../Page/斯劍虎.md "wikilink")。透過分析其[二頭肌的機械功率與預估強度](../Page/二頭肌.md "wikilink")，大地懶甚至有能力可以翻過成年的[雕齒獸](../Page/雕齒獸.md "wikilink")，也因此可以將其當作獵物獵殺\[9\]。但是大地懶缺乏一般掠食者所擁有的[裂肉齒且在大地懶糞便中也沒有發現到骨頭碎片](../Page/裂肉齒.md "wikilink")，這個理論仍然缺乏重要的證據。

## 滅絕

在[更新世中](../Page/更新世.md "wikilink")，大地懒和其他原始[哺乳動物受到影響](../Page/哺乳動物.md "wikilink")，造成絕種的危機，原始哺乳動物滅絕，同時大地懒滅絕。在南美洲，大地懶存活至約
10,500 年前，一般認為是由於人類領域的擴張與狩獵才導致其絕種的\[10\]。也有些指出可能有部分個體至 8,000 至
7,000年前仍然存活著\[11\]，但普遍依然認為是在 10,000
年絕種的\[12\]。透過分析顯示適合大地懶生存的棲息地在全新世中期時大幅縮減並變得破碎，這也許不是其滅絕的主要原因，但仍然為可能的要素之一\[13\]。

## 參見

  - [已滅絕動物列表](../Page/已滅絕動物列表.md "wikilink")

## 參考資料

[M](../Category/化石哺乳類.md "wikilink")
[M](../Category/上新世哺乳类.md "wikilink")
[M](../Category/更新世哺乳类.md "wikilink")
[Category:南美洲史前哺乳動物](../Category/南美洲史前哺乳動物.md "wikilink")

1.  A. E. Zurita, A. A. Carlini, G. J. Scillato-Yané and E. P. Tonni.
    2004. Mamíferos extintos del Cuaternario de la Provincia del Chaco
    (Argentina) y su relación con aquéllos del este de la región
    pampeana y de Chile. *Revista geológica de Chile* 31(1):65-87

2.

3.

4.

5.

6.

7.

8.
9.

10.

11.

12.

13.