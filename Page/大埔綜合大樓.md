[Tai_Po_Complex.jpg](https://zh.wikipedia.org/wiki/File:Tai_Po_Complex.jpg "fig:Tai_Po_Complex.jpg")
[Tai_Po_Complex_Market.jpg](https://zh.wikipedia.org/wiki/File:Tai_Po_Complex_Market.jpg "fig:Tai_Po_Complex_Market.jpg")
**大埔綜合大樓**（[英文](../Page/英文.md "wikilink")：**Tai Po
Complex**），是[香港一座多用途的](../Page/香港.md "wikilink")[市政大樓](../Page/市政大樓.md "wikilink")，位處[新界](../Page/新界.md "wikilink")[大埔](../Page/大埔_\(香港\).md "wikilink")[鄉事會街](../Page/鄉事會街.md "wikilink")8號，於2004年9月1日正式啟用，內設「大埔墟街市及熟食中心」、[公共圖書館及康樂設施等](../Page/大埔公共圖書館.md "wikilink")，大樓內亦有[康樂及文化事務署及](../Page/康樂及文化事務署.md "wikilink")[土地註冊處等](../Page/土地註冊處.md "wikilink")[香港政府部門辦事處](../Page/香港政府部門.md "wikilink")。綜合大樓由[伍振民建築師（香港）有限公司設計](../Page/劉榮廣.md "wikilink")，造價達二億五千萬。

大埔墟街市及熟食中心合共佔地約12,000平方米，地下及一樓為街市，設有260個檔位，是全香港最多檔攤的街市；二樓為[熟食中心](../Page/熟食中心.md "wikilink")，設有約40個檔位。戶外設露天平台，可讓街坊休憩。

## 設施

<table>
<tbody>
<tr class="odd">
<td><p>6樓及7樓</p></td>
<td><p><a href="../Page/大埔墟體育館.md" title="wikilink">大埔墟體育館</a></p></td>
</tr>
<tr class="even">
<td><p>5樓</p></td>
<td><p><a href="../Page/大埔公共圖書館.md" title="wikilink">大埔公共圖書館</a>、學生自修室</p></td>
</tr>
<tr class="odd">
<td><p>3樓至4樓</p></td>
<td><p>政府部門辦事處</p>
<ul>
<li>4樓：
<ul>
<li><a href="../Page/社會福利署.md" title="wikilink">社會福利署大埔及</a><a href="../Page/北區_(香港).md" title="wikilink">北區福利辦事處</a>（包括大埔及北區策劃及統籌小組）</li>
<li><a href="../Page/社會福利署.md" title="wikilink">社會福利署保護家庭及兒童服務課</a>（大埔及北區）</li>
<li>社會福利署南大埔社會保障辦事處</li>
<li><a href="../Page/土地註冊處.md" title="wikilink">土地註冊處大埔查冊中心</a></li>
<li><a href="../Page/食物環境衞生署.md" title="wikilink">食物環境衞生署新界牌照辦事處</a></li>
<li><a href="../Page/香港警務處.md" title="wikilink">香港警務處</a><a href="../Page/福利服務組.md" title="wikilink">福利服務組</a><a href="../Page/新界北總區.md" title="wikilink">新界北總區福利辦事處</a></li>
<li><a href="../Page/大埔區議會.md" title="wikilink">大埔區議會秘書處</a></li>
</ul></li>
<li>3樓：
<ul>
<li><a href="../Page/食物環境衞生署.md" title="wikilink">食物環境衞生署大埔區環境衛生辦事處</a></li>
<li><a href="../Page/香港康樂及文化事務署.md" title="wikilink">康樂及文化事務署大埔區康樂事務辦事處</a></li>
</ul></li>
</ul></td>
</tr>
<tr class="even">
<td><p>2樓</p></td>
<td><p>熟食中心</p></td>
</tr>
<tr class="odd">
<td><p>地下及1樓</p></td>
<td><p><a href="../Page/大埔墟.md" title="wikilink">大埔墟街市</a></p></td>
</tr>
</tbody>
</table>

## 途經的公共交通服務

<div class="NavFrame collapsed" style="color: black; background-color: yellow; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: yellow; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: yellow; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{東鐵綫色彩}}">█</font>[東鐵綫](../Page/東鐵綫.md "wikilink")：[大埔墟站](../Page/大埔墟站.md "wikilink")

<!-- end list -->

  - [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - [專線小巴](../Page/香港小巴.md "wikilink")

</div>

</div>

## 外部連結

[Category:香港市政大廈](../Category/香港市政大廈.md "wikilink") [Category:大埔
(香港)](../Category/大埔_\(香港\).md "wikilink")