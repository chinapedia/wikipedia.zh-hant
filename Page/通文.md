**通文**（936年三月-939年七月）是[闽康宗](../Page/闽.md "wikilink")[王繼鵬的年号](../Page/王繼鵬.md "wikilink")，共计4年。

## 纪年

| 通文                               | 元年                             | 二年                             | 三年                             | 四年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 936年                           | 937年                           | 938年                           | 939年                           |
| [干支](../Page/干支纪年.md "wikilink") | [丙申](../Page/丙申.md "wikilink") | [丁酉](../Page/丁酉.md "wikilink") | [戊戌](../Page/戊戌.md "wikilink") | [己亥](../Page/己亥.md "wikilink") |

## 參看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 同期存在的其他政权年号
      - [清泰](../Page/清泰.md "wikilink")（934年四月至936閏十一月）：後唐—[李從珂之年號](../Page/李從珂.md "wikilink")
      - [天福](../Page/天福_\(石敬瑭\).md "wikilink")（936年十一月至944年六月）：[後晉](../Page/後晉.md "wikilink")—[石敬瑭之年號](../Page/石敬瑭.md "wikilink")
      - [天祚](../Page/天祚.md "wikilink")（935年九月至937年十月）：吳—楊溥之年號
      - [昇元](../Page/昇元.md "wikilink")（937年十月至943年二月）：[南唐](../Page/南唐.md "wikilink")—[李昪之年號](../Page/李昪.md "wikilink")
      - [大有](../Page/大有_\(年號\).md "wikilink")（928年三月至942年三月）：[南漢](../Page/南漢.md "wikilink")—[劉龑之年號](../Page/劉龑.md "wikilink")
      - [明德](../Page/明德_\(孟知祥\).md "wikilink")（934年四月至937年）：[後蜀](../Page/後蜀.md "wikilink")—[孟知祥之年號](../Page/孟知祥.md "wikilink")
      - [廣政](../Page/廣政_\(孟昶\).md "wikilink")（938年正月至965年正月）：後蜀—[孟昶之年號](../Page/孟昶.md "wikilink")
      - [大明](../Page/大明_\(楊干真\).md "wikilink")（931年至937年）：[大義寧](../Page/大義寧.md "wikilink")—[楊干真之年號](../Page/楊干真.md "wikilink")
      - [鼎新](../Page/鼎新.md "wikilink")：大義寧—楊干真之年號
      - [光聖](../Page/光聖.md "wikilink")：大義寧—楊干真之年號
      - [文德](../Page/文德_\(段思平\).md "wikilink")（938年起）：[大理國](../Page/大理國.md "wikilink")—[段思平之年號](../Page/段思平.md "wikilink")
      - [天顯](../Page/天顯.md "wikilink")（926年二月至938年）：[契丹](../Page/遼朝.md "wikilink")—[耶律阿保機](../Page/耶律阿保機.md "wikilink")、[耶律德光之年號](../Page/耶律德光.md "wikilink")
      - [會同](../Page/会同_\(辽朝\).md "wikilink")（938年十一月至947年正月）：[契丹](../Page/契丹.md "wikilink")—耶律德光之年號
      - [甘露](../Page/甘露_\(耶律倍\).md "wikilink")（926年至936年）：[東丹](../Page/東丹.md "wikilink")—[耶律倍之年號](../Page/耶律倍.md "wikilink")
      - [同慶](../Page/同慶_\(尉遲烏僧波\).md "wikilink")（912年至966年）：[于闐](../Page/于闐.md "wikilink")—[尉遲烏僧波之年號](../Page/尉遲烏僧波.md "wikilink")
      - [承平](../Page/承平_\(朱雀天皇\).md "wikilink")（931年四月二十六日至938年五月二十二日）：日本[朱雀天皇年号](../Page/朱雀天皇.md "wikilink")
      - [天慶](../Page/天慶_\(朱雀天皇\).md "wikilink")（938年五月二十二日至947年四月二十二日）：日本[朱雀天皇與](../Page/朱雀天皇.md "wikilink")[村上天皇年号](../Page/村上天皇.md "wikilink")

[Category:闽年号](../Category/闽年号.md "wikilink")
[Category:930年代中国政治](../Category/930年代中国政治.md "wikilink")