**香港旗幟列表**列出[香港現時和曾經使用的旗幟](../Page/香港.md "wikilink")，包括[香港英國管治時期的旗幟](../Page/香港殖民地時期.md "wikilink")。香港現時使用的是[特區區旗](../Page/香港特別行政區區旗.md "wikilink")。

## 官方

| 旗幟                                                                                                               | 日期                                           | 設計          | 備註                                                                    |
| ---------------------------------------------------------------------------------------------------------------- | -------------------------------------------- | ----------- | --------------------------------------------------------------------- |
| [Flag_of_Hong_Kong.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Hong_Kong.svg "fig:Flag_of_Hong_Kong.svg") | [香港特別行政區區旗](../Page/香港特別行政區區旗.md "wikilink") | 1997年7月1日至今 | 五星花蕊洋紫荊花紅旗，以紅色作底色，中央有一朵五星花蕊的白色洋紫荊花圖案。底色與中華人民共和國國旗的紅色為標準，而洋紫荊花則是香港的象徵。 |

## 歷史旗幟

### 官方

<table style="width:175%;">
<colgroup>
<col style="width: 15%" />
<col style="width: 88%" />
<col style="width: 43%" />
<col style="width: 28%" />
</colgroup>
<thead>
<tr class="header">
<th><p>旗幟</p></th>
<th><p>日期</p></th>
<th><p>設計</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Portugal_(1495).svg" title="fig:Flag_of_Portugal_(1495).svg">Flag_of_Portugal_(1495).svg</a></p></td>
<td><p><strong><a href="../Page/葡萄牙國旗#1495.E5.B9.B4.E2.80.931667.E5.B9.B4.md" title="wikilink">葡萄牙王國國旗</a></strong></p></td>
<td><p>1514年－1521年</p></td>
<td><p>葡萄牙在1514年佔領<a href="../Page/東莞市.md" title="wikilink">東莞縣</a>（今香港）<a href="../Page/大嶼山.md" title="wikilink">屯門島</a>、<a href="../Page/屯門軍鎮.md" title="wikilink">屯門海澳及</a><a href="../Page/葵涌.md" title="wikilink">葵涌海澳</a></p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_the_United_Kingdom.svg" title="fig:Flag_of_the_United_Kingdom.svg">Flag_of_the_United_Kingdom.svg</a></p></td>
<td><p><strong><a href="../Page/英國國旗.md" title="wikilink">大英帝國國旗</a></strong></p></td>
<td><p>1841年－1868年</p></td>
<td><p>1841年在佔據港島後不久，英國殖民者即在當地升起了聯合王國國旗</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_the_Qing_Dynasty_(1862-1889).svg" title="fig:Flag_of_the_Qing_Dynasty_(1862-1889).svg">Flag_of_the_Qing_Dynasty_(1862-1889).svg</a></p></td>
<td><p><strong><a href="../Page/大清國旗.md" title="wikilink">大清國旗</a></strong></p></td>
<td><p>1862年－1889年</p></td>
<td><p>黃色及龍都是大清皇帝的象徵，以黃龍旗作為國旗有「朕即國家」的意思。而據五行學說，認為中央屬土，黃色。以黃色代表中國</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Hong_Kong_1871.svg" title="fig:Flag_of_Hong_Kong_1871.svg">Flag_of_Hong_Kong_1871.svg</a></p></td>
<td><p><strong><a href="../Page/香港旗.md" title="wikilink">香港旗</a></strong></p></td>
<td><p>1871年－1876年</p></td>
<td><p>由<a href="../Page/藍色軍旗.md" title="wikilink">英國藍旗和加冕的</a>「H.K.」字所組成</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Hong_Kong_1876.svg" title="fig:Flag_of_Hong_Kong_1876.svg">Flag_of_Hong_Kong_1876.svg</a></p></td>
<td><p>1876年－1941年，<br />
1945年－1955年</p></td>
<td><p>由英國藍旗和<a href="../Page/阿群帶路圖.md" title="wikilink">阿群帶路圖所組成</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_the_Qing_Dynasty_(1889-1912).svg" title="fig:Flag_of_the_Qing_Dynasty_(1889-1912).svg">Flag_of_the_Qing_Dynasty_(1889-1912).svg</a></p></td>
<td><p><strong>大清國旗</strong></p></td>
<td><p>1889年－1898年</p></td>
<td><p>黃色及龍都是大清皇帝的象徵，以黃龍旗作為國旗有「朕即國家」的意思。而據五行學說，認為中央屬土，黃色。以黃色代表中國</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Merchant_flag_of_Japan_(1870).svg" title="fig:Merchant_flag_of_Japan_(1870).svg">Merchant_flag_of_Japan_(1870).svg</a></p></td>
<td><p><strong><a href="../Page/日本国旗.md" title="wikilink">大日本帝國國旗</a></strong></p></td>
<td><p>1941年－1945年</p></td>
<td><p>日本國旗也稱作「日章旗」，旗面上一輪紅日居中，輝映著白色的旗面。傳說日本是天照大神創造的，而天皇就是天照大神的子孫</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Hong_Kong_1955.svg" title="fig:Flag_of_Hong_Kong_1955.svg">Flag_of_Hong_Kong_1955.svg</a></p></td>
<td><p><strong>香港旗</strong></p></td>
<td><p>1955年－1959年</p></td>
<td><p>由英國藍旗和阿群帶路圖所組成</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Hong_Kong_1959.svg" title="fig:Flag_of_Hong_Kong_1959.svg">Flag_of_Hong_Kong_1959.svg</a></p></td>
<td><p>1959年－1997年</p></td>
<td><p>由英國藍旗和<a href="../Page/香港盾徽.md" title="wikilink">香港盾徽所組成</a></p></td>
<td><p>香港殖民地時期最後一面旗幟</p></td>
</tr>
</tbody>
</table>

### 民用

| 旗幟                                                                                                                                                                                                            | 日期                                           | 設計          | 備註                                          |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------- | ----------- | ------------------------------------------- |
| [Flag_of_Hong_Kong_1959_(unofficial_Red_Ensign).svg](https://zh.wikipedia.org/wiki/File:Flag_of_Hong_Kong_1959_\(unofficial_Red_Ensign\).svg "fig:Flag_of_Hong_Kong_1959_(unofficial_Red_Ensign).svg") | [香港殖民地非官方民用旗](../Page/香港殖民地旗幟.md "wikilink") | 1959年－1997年 | 由英國紅旗和[香港盾徽所組成](../Page/香港盾徽.md "wikilink") |

### 總督

<table style="width:175%;">
<colgroup>
<col style="width: 15%" />
<col style="width: 88%" />
<col style="width: 43%" />
<col style="width: 28%" />
</colgroup>
<thead>
<tr class="header">
<th><p>旗幟</p></th>
<th><p>日期</p></th>
<th><p>設計</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_the_Governor_of_Hong_Kong_1910.svg" title="fig:Flag_of_the_Governor_of_Hong_Kong_1910.svg">Flag_of_the_Governor_of_Hong_Kong_1910.svg</a></p></td>
<td><p><a href="../Page/香港總督旗.md" title="wikilink">香港總督旗</a></p></td>
<td><p>1910年－1941年，<br />
1945年－1959年</p></td>
<td><p>由<a href="../Page/英國國旗.md" title="wikilink">英國國旗和</a><a href="../Page/阿群帶路圖.md" title="wikilink">阿群帶路圖所組成</a></p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_the_Governor_of_Hong_Kong.svg" title="fig:Flag_of_the_Governor_of_Hong_Kong.svg">Flag_of_the_Governor_of_Hong_Kong.svg</a></p></td>
<td><p>1959年－1997年</p></td>
<td><p>由<a href="../Page/英國國旗.md" title="wikilink">英國國旗和</a><a href="../Page/香港盾徽.md" title="wikilink">香港盾徽所組成</a></p></td>
<td><p>香港最後一面總督旗幟</p></td>
</tr>
</tbody>
</table>

### 市政機構

| 旗幟                                                                                                                                    | 日期                                          | 設計           | 備註                                                                                                                                                                                                                                                                                                                                       |
| ------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------- | ------------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [HKUrbanCouncil.svg](https://zh.wikipedia.org/wiki/File:HKUrbanCouncil.svg "fig:HKUrbanCouncil.svg")                                  | **[市政局](../Page/市政局_\(香港\).md "wikilink")** | 1960年代－1999年 | [粉紅色旗幟中央有白色市政局標誌](../Page/粉紅色.md "wikilink")。香港市政局旗縱橫比例為1:2，[洋紫荊紫作為使用色代表神聖](../Page/紫色.md "wikilink")、尊貴、慈愛，同時寓意香港華洋和平友好，共同相處。中間繪有一片非寫實化[香港市花](../Page/香港市花.md "wikilink")[洋紫荊](../Page/洋紫荊.md "wikilink")\[1\]五片花瓣標誌圖案，以展現全世界和平友好，圖案的花瓣組合緊密而有序，象徵著香港人民的團結和互助的精神。香港市政局旗是香港人希望和理想的象徵，這是屬於香港的象徵。她帶給香港人自尊、自重、自豪和榮耀，也能增強香港人對香港的熱愛、忠誠、認同感、歸屬感 |
| [HK_Regional_Council_Logo.svg](https://zh.wikipedia.org/wiki/File:HK_Regional_Council_Logo.svg "fig:HK_Regional_Council_Logo.svg") | **[區域市政局](../Page/區域市政局.md "wikilink")**    | 1986年－1999年  | [綠色旗幟中央有白色區域市政局標誌](../Page/綠色.md "wikilink")                                                                                                                                                                                                                                                                                             |

## 参考文献

## 参见

  - [中华人民共和国旗帜](../Page/中华人民共和国旗帜.md "wikilink")
  - [香港旗幟列表](../Page/香港旗幟列表.md "wikilink")
  - [兩岸四地旗幟列表](../Page/兩岸四地旗幟列表.md "wikilink")
  - [香港特别行政区区旗](../Page/香港特别行政区区旗.md "wikilink")
  - [中國國旗](../Page/中國國旗.md "wikilink")
  - [英國國旗](../Page/英國國旗.md "wikilink")

{{-}}

[\*](../Category/香港旗帜.md "wikilink")
[Category:香港相關列表](../Category/香港相關列表.md "wikilink")
[Category:旗幟列表](../Category/旗幟列表.md "wikilink")

1.  [香港市花洋紫荊的故事](http://www.greening.gov.hk/tc/planting_knowledge/historical_account.html)