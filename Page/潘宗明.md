**潘宗明**（Poon Chung
Ming，），別名「阿潘」、「潘sir」。1980年代末至1990年代中為[香港](../Page/香港.md "wikilink")[無綫電視節目主持人](../Page/無綫電視.md "wikilink")，曾主持《[球迷世界](../Page/球迷世界.md "wikilink")》及《[體育世界](../Page/體育世界.md "wikilink")》，並曾多次主持足球賽事的現場直播主持。他於1970年代末畢業於[荃灣官立中學](../Page/荃灣官立中學.md "wikilink")，後分別於1982年及1983年畢業於[柏立基師範學院和](../Page/柏立基師範學院.md "wikilink")[葛量洪師範學院](../Page/葛量洪師範學院.md "wikilink")。在加入[無綫電視主持節目前曾在](../Page/無綫電視.md "wikilink")[明愛沙田馬登基金中學任職教師](../Page/明愛沙田馬登基金中學.md "wikilink")。

他主持現場直播節目時，能帶動現場歡樂的氣氛，其中一次，是他在1995年於[香港大球場主持賀歲杯決賽現場直播時](../Page/香港大球場.md "wikilink")，成功令現場觀眾玩「人浪」，令人印象深刻。

1990年代後期，潘宗明移民[加拿大至今](../Page/加拿大.md "wikilink")。現時在[加拿大中文電台AM](../Page/加拿大中文電台.md "wikilink")1430及[新時代電視擔任節目主持](../Page/新時代電視.md "wikilink")。

曾是1992年巴塞隆拿奧運、1996年亞特蘭大奧運、2000年悉尼奧運、2004年雅典奧運、北京奧運、倫敦奧運節目主持。1996年8月參與[無綫電視亞特蘭大奧運](../Page/無綫電視.md "wikilink")，夥拍[陳百祥等作節目主持](../Page/陳百祥.md "wikilink")。2008年8月回港參與[無綫電視](../Page/無綫電視.md "wikilink")2008北京奧運，夥拍[蔡康年](../Page/蔡康年.md "wikilink")、[楊婉儀等作節目主持](../Page/楊婉儀.md "wikilink")。2012年7月再次回港參與無綫[2012倫敦奧運](../Page/2012倫敦奧運.md "wikilink")，夥拍[鍾志光和](../Page/鍾志光.md "wikilink")[徐嘉樂等作節目主持](../Page/徐嘉樂.md "wikilink")。

潘宗明的太太[許加恩亦是電視人](../Page/許加恩.md "wikilink")，曾為[無綫電視](../Page/無綫電視.md "wikilink")《體育世界》前資料搜集員、[翡翠台和](../Page/翡翠台.md "wikilink")[明珠台的前天氣報導員](../Page/明珠台.md "wikilink")。後來潘宗明在其電台節目透露，許加恩亦是[香港有線電視婦女台的創台元老](../Page/香港有線電視.md "wikilink")\[1\]。

## 演出作品

### 節目主持([無綫電視](../Page/無綫電視.md "wikilink"))

  - [球迷世界](../Page/球迷世界.md "wikilink")
  - [體育世界](../Page/體育世界.md "wikilink")
  - [香港早晨](../Page/香港早晨.md "wikilink")
  - [寰宇風情](../Page/寰宇風情.md "wikilink")
  - [歡樂今宵](../Page/歡樂今宵.md "wikilink")（1990-1994）
  - [K-100](../Page/K-100.md "wikilink")
  - 1990年：電視先鋒群星會
  - 1994年：[周五紅人館](../Page/周五紅人館.md "wikilink")
  - 1995年：[周三紅friend區](../Page/周三紅friend區.md "wikilink")
  - [世界盃](../Page/世界盃.md "wikilink")
  - [巴塞隆拿奧運](../Page/巴塞隆拿奧運.md "wikilink")
  - [亞特蘭大奧運](../Page/亞特蘭大奧運.md "wikilink")
  - [悉尼奧運](../Page/悉尼奧運.md "wikilink")
  - [雅典奧運](../Page/雅典奧運.md "wikilink")
  - [北京奧運](../Page/北京奧運.md "wikilink")
  - [2012倫敦奧運](../Page/2012倫敦奧運.md "wikilink")

### 節目主持([新時代電視](../Page/新時代電視.md "wikilink"))

  - [大城小聚](../Page/大城小聚.md "wikilink")
  - [魅力凝聚新時代](../Page/魅力凝聚新時代.md "wikilink")
  - [新秀歌唱大賽多倫多選拔賽](../Page/新秀歌唱大賽多倫多選拔賽.md "wikilink")
  - [世界盃](../Page/世界盃.md "wikilink")

## 參考文獻

## 外部連結

  - 加拿大中文電台主持檔案──[潘宗明](http://www.am1430.com/dj_detail.php?id=11)

[category:香港主持人](../Page/category:香港主持人.md "wikilink")

[Category:香港足球評述員](../Category/香港足球評述員.md "wikilink")
[Category:前無綫電視男藝員](../Category/前無綫電視男藝員.md "wikilink")
[Category:华裔加拿大人](../Category/华裔加拿大人.md "wikilink")
[Category:多倫多人](../Category/多倫多人.md "wikilink")
[Category:新時代電視藝員](../Category/新時代電視藝員.md "wikilink")
[Category:AM1430主持人](../Category/AM1430主持人.md "wikilink")
[chung](../Category/潘姓.md "wikilink")
[Category:荃灣官立中學校友](../Category/荃灣官立中學校友.md "wikilink")

1.