**東五反田**（）是[東京都](../Page/東京都.md "wikilink")[品川區的地名](../Page/品川區.md "wikilink")，設一丁目至五丁目。2013年（平成25年）8月1日為止的人口有13,939人\[1\]。[郵遞區號](../Page/郵遞區號.md "wikilink")141-0022。

## 地理

位於品川區北部。北鄰品川區[上大崎](../Page/上大崎.md "wikilink")、[港區](../Page/港區_\(東京都\).md "wikilink")[白金台](../Page/白金台.md "wikilink")，東接[港區](../Page/港區_\(東京都\).md "wikilink")[高輪](../Page/高輪.md "wikilink")，東南通品川區[北品川](../Page/北品川.md "wikilink")，南與品川區[大崎之間以](../Page/大崎_\(品川區\).md "wikilink")[目黑川為界](../Page/目黑川.md "wikilink")，西與品川區[西五反田之間以](../Page/西五反田.md "wikilink")[山手線路廊為界](../Page/山手線.md "wikilink")。

町域西部有[首都高速2號目黑線通過](../Page/首都高速2號目黑線.md "wikilink")。[櫻田通貫穿町內](../Page/國道1號_\(日本\).md "wikilink")。西邊有[山手線](../Page/山手線.md "wikilink")、[都營淺草線](../Page/都營地下鐵淺草線.md "wikilink")、[東急池上線](../Page/東急池上線.md "wikilink")[五反田站](../Page/五反田站.md "wikilink")。

五反田站周邊為商業區，其他地區辦公大樓、公寓、低層住宅混雜。東五反田三丁目附近俗稱「島津山」，東五反田五丁目附近俗稱「池田山」，兩者皆為山之手代表的高級住宅區。[皇后美智子也是本地](../Page/皇后美智子.md "wikilink")（東五反田五丁目）出身。

## 歷史

1932年（昭和7年）10月1日編入東京市。

### 地名由來

此地為[五反田東部而得名](../Page/五反田.md "wikilink")。

## 交通

### 鐵路

  - [五反田站](../Page/五反田站.md "wikilink")（[JR](../Page/東日本旅客鐵道.md "wikilink")[山手線](../Page/山手線.md "wikilink")、[東急池上線](../Page/東急池上線.md "wikilink")、[都營地下鐵](../Page/都營地下鐵.md "wikilink")[淺草線](../Page/都營地下鐵淺草線.md "wikilink")）

### 巴士

  - [都營巴士](../Page/都營巴士.md "wikilink")
      - [五反田站前停留所](../Page/五反田站.md "wikilink")
      - 東五反田三丁目停留所

## 設施

[Tokyo_Health_Care_University_Gotanda_Campus.JPG](https://zh.wikipedia.org/wiki/File:Tokyo_Health_Care_University_Gotanda_Campus.JPG "fig:Tokyo_Health_Care_University_Gotanda_Campus.JPG")

  - 東五反田一丁目

<!-- end list -->

  - [五反田站](../Page/五反田站.md "wikilink")
  - [電波新聞社](../Page/電波新聞社.md "wikilink")
  - 五反田有樂街
  - 寶塔寺
  - [雉子神社](../Page/雉子神社.md "wikilink")
  - [幸福科學總合本部](../Page/幸福科學.md "wikilink")

<!-- end list -->

  - 東五反田二丁目

<!-- end list -->

  - Oval Court大崎
      - [COMSYS Holdings本社](../Page/COMSYS_Holdings.md "wikilink")
      - [神鋼建機本社](../Page/神鋼建機.md "wikilink")
      - [神鋼起重機本社](../Page/神鋼起重機.md "wikilink")
  - [富士通電子零件本社](../Page/富士通電子零件.md "wikilink")
  - [IMAGICA本店](../Page/IMAGICA.md "wikilink")
  - [品川區立日野學園](../Page/品川區立日野學園.md "wikilink")

<!-- end list -->

  - 東五反田三丁目

<!-- end list -->

  - [清泉女子大學](../Page/清泉女子大學.md "wikilink")
  - 住友不動產高輪公園塔

<!-- end list -->

  - 東五反田四丁目

<!-- end list -->

  - [東京醫療保健大學](../Page/東京醫療保健大學.md "wikilink")
  - 相生坂

<!-- end list -->

  - 東五反田五丁目

<!-- end list -->

  - [NTT東日本關東醫院](../Page/NTT東日本關東醫院.md "wikilink")
  - 品川區立[池田山公園](../Page/池田山公園.md "wikilink")
  - 品川區立[合歡木庭](../Page/合歡木庭.md "wikilink")（前正田邸，皇后美智子舊家）
  - [印尼駐日大使館](../Page/印尼.md "wikilink")
  - [渡邊拳擊館](../Page/渡邊拳擊館.md "wikilink")

## 圖片

<File:JR> Gotanda sta 001.jpg|五反田站
[File:Seisenwemensuniv.jpg|清泉女子大學](File:Seisenwemensuniv.jpg%7C清泉女子大學)
<File:Shinagawa> City Unified Elementary Through Junior High School
Hinogakuen 1.jpg|日野學園
[File:Nttkantoubyouin.jpg|NTT東日本關東醫院](File:Nttkantoubyouin.jpg%7CNTT東日本關東醫院)

## 備註

## 相關條目

  - [大崎副都心](../Page/大崎副都心.md "wikilink")（含東五反田一部分）
  - [池田氏](../Page/池田氏.md "wikilink")（當地的「池田山」是因池田氏的下屋敷而得名）
  - [五反田](../Page/五反田.md "wikilink")

[\*Higashi](../Category/五反田.md "wikilink")
[Category:大崎副都心](../Category/大崎副都心.md "wikilink")
[Category:品川區町名](../Category/品川區町名.md "wikilink")

1.  [世帯と人口‐例月表
    【地区別、町丁別、地域センター別】 2013年8月1日現在](http://www.city.shinagawa.tokyo.jp/ct/other000044500/201308a.xls)