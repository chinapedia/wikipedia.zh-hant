**海卫十二**（Laomedeia, S/2002 N
3）是环绕[海王星运行的一颗](../Page/海王星.md "wikilink")[不規則衛星](../Page/不規則衛星.md "wikilink")，于2002年8月13日由[馬修·霍爾曼等学者发现](../Page/馬修·霍爾曼.md "wikilink")\[1\]，以[希臘神話](../Page/希臘神話.md "wikilink")[涅柔斯和](../Page/涅柔斯.md "wikilink")[多里斯的五十個女兒之一的](../Page/多里斯.md "wikilink")「拉俄墨得亞」而命名。

海衛十二直徑大約，於距離海王星的軌道上運行\[2\]。

## 概述

## 參考

## 外部連結

  - [Neptune's Known
    Satellites](https://web.archive.org/web/20081221181025/http://www.dtm.ciw.edu/sheppard/satellites/nepsatdata.html)
    (by [Scott S. Sheppard](../Page/Scott_S._Sheppard.md "wikilink"))
  - [David Jewitt
    pages](https://web.archive.org/web/20070624084542/http://www.ifa.hawaii.edu/~jewitt/irregulars.html)
  - [MPC: Natural Satellites Ephemeris
    Service](http://www.minorplanetcenter.org/iau/NatSats/NaturalSatellites.html)
  - Mean orbital parameters (NASA)[1](http://ssd.jpl.nasa.gov/?sat_elem)

[海卫12](../Category/海王星的卫星.md "wikilink")

1.
2.