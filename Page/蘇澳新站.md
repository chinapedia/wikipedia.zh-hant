[Outline_map_of_SUAO_railway_stations.png](https://zh.wikipedia.org/wiki/File:Outline_map_of_SUAO_railway_stations.png "fig:Outline_map_of_SUAO_railway_stations.png")，C：[永樂](../Page/永樂車站_\(宜蘭縣\).md "wikilink")\]\]
[North-Link_Line_near_its_starting_point.jpg](https://zh.wikipedia.org/wiki/File:North-Link_Line_near_its_starting_point.jpg "fig:North-Link_Line_near_its_starting_point.jpg")
**蘇澳新站**位於[台灣](../Page/台灣.md "wikilink")[宜蘭縣](../Page/宜蘭縣.md "wikilink")[蘇澳鎮](../Page/蘇澳鎮_\(臺灣\).md "wikilink")，坐落於[台9線旁邊](../Page/台9線.md "wikilink")，為[臺灣鐵路管理局](../Page/臺灣鐵路管理局.md "wikilink")[宜蘭線](../Page/宜蘭線.md "wikilink")、[北迴線的](../Page/北迴線.md "wikilink")[鐵路車站](../Page/鐵路車站.md "wikilink")。舊名**南聖湖車站**，是位在[北迴線的起點](../Page/北迴線.md "wikilink")，與[宜蘭線的交會點](../Page/宜蘭線.md "wikilink")（宜蘭線的終點是[蘇澳車站](../Page/蘇澳車站.md "wikilink")）。

## 車站構造

  - [島式月台兩座](../Page/島式月台.md "wikilink")。（第2月台是宜蘭線）
  - [跨站式站房](../Page/跨站式站房_\(台鐵\).md "wikilink")，接送者可以直接開車上二樓送客。

## 利用狀況

  - 目前為[一等站](../Page/臺灣鐵路管理局車站等級#一等站.md "wikilink")。
  - [普悠瑪號](../Page/普悠瑪列車.md "wikilink")：全部不停靠。
  - [太魯閣號](../Page/太魯閣列車.md "wikilink")：停靠順行228次；逆行229/6229次。
  - [復興號](../Page/復興號列車.md "wikilink")：南下（逆行）683、689、北上（順行）684、688/6688次。
  - 除6691次、6692次外不停靠。
  - [自強號](../Page/自強號列車.md "wikilink")：順行停靠204、272、212、172次；逆行停靠407、175/6175、251/6251、249次。
  - [莒光號](../Page/莒光號列車.md "wikilink")：除[觀光團體列車](../Page/觀光列車.md "wikilink")、南下（順行）616、624/6624次。北上（逆行）6615次外均有停靠。
  - [區間快車](../Page/臺鐵區間快車.md "wikilink")：皆停靠。
  - 4022次、4043次、4030[區間快車](../Page/臺鐵區間快車.md "wikilink")(終點)。
  - [區間車](../Page/臺鐵區間車.md "wikilink")：皆停靠。
  - 對號列車乘客前往武荖坑風景區之最近車站。
  - 站內設有[新馬車站所屬的](../Page/新馬車站.md "wikilink")「鐵路之旅 - 小站巡禮紀念章」戳章，供旅客蓋戳留念。
  - 本站開放使用[悠遊卡](../Page/悠遊卡.md "wikilink")、[一卡通及](../Page/一卡通_\(台灣\).md "wikilink")[icash
    2.0付費](../Page/icash.md "wikilink")。

| 年度   | 日均進站旅次（人） |
| ---- | --------- |
| 1998 | 663       |
| 1999 | 727       |
| 2000 | 904       |
| 2001 | 909       |
| 2002 | 901       |
| 2003 | 912       |
| 2004 | 1,044     |
| 2005 | 1,131     |
| 2006 | 1,093     |
| 2007 | 981       |
| 2008 | 966       |
| 2009 | 957       |
| 2010 | 1,079     |
| 2011 | 1,132     |
| 2012 | 1,349     |
| 2013 | 1,450     |
| 2014 | 1,610     |
| 2015 | 1,568     |
| 2016 | 1,656     |

歷年旅客人次紀錄

## 歷史

  - 1968年4月15日：設「南新城車站」。
  - 1975年1月1日：改為「南聖湖車站」。
  - 1982年1月1日：改為「蘇澳新站」。
  - 2003年11月14日：[跨站式站房落成啟用](../Page/跨站式站房_\(台鐵\).md "wikilink")，接送者可以直接開車上二樓。
  - 2014年7月18日：由二等站升為一等站，與[蘇澳車站對調級別](../Page/蘇澳車站.md "wikilink")。
  - 2015年6月30日：啟用多卡通刷卡機。

## 其他

  - 由于2010年10月底的[蘇花公路大坍方](../Page/蘇花公路.md "wikilink")，造成重大傷亡事件，使得來台旅遊的中國大陸旅客就以“鐵路加公路”的方式，來避開蘇花公路最難行的路段。此轉運方式讓位在宜蘭縣、距離蘇花公路山區路段最近的主要火車站蘇澳新站成為陸客的轉運車站\[1\]。根據資料顯示，蘇澳新站原本旅客流量每天約一千人，蘇花公路坍方過後，每天約有80輛的遊覽車前來載客\[2\]。而蘇澳新站的旅客量於2011年較前一年成長了44.23%\[3\]。

## 車站週邊

  - 武荖坑風景區
  - 馬賽地區（[宜蘭縣](../Page/宜蘭縣.md "wikilink")[蘇澳鎮永榮里](../Page/蘇澳鎮_\(臺灣\).md "wikilink")）

## 公車資訊

### [宜蘭縣市區公車](../Page/宜蘭縣市區公車.md "wikilink")

  - [首都客運](../Page/首都客運.md "wikilink")：
      - **<font color="red">241</font>** 羅東轉運站－蘇澳新站
      - **<font color="red">242</font>** 羅東轉運站－利澤工業區－蘇澳新站
  - [大都會客運](../Page/大都會客運.md "wikilink")：
      - **<font color="red">121</font>** 蘇澳車站－武荖坑
  - [國光客運](../Page/國光客運.md "wikilink")：
      - **<font color="red">紅2</font>** 宜蘭轉運站－南方澳(假日行駛)
      - **<font color="green">綠28</font>** 蘇澳新火車站－豆腐岬風景區(假日行駛)
      - **<font color="red">1766</font>** 南方澳－烏石港(平日行駛)
      - **<font color="red">1767</font>** 南方澳－純精路－頭城(平日行駛)

## 攝影集

<File:Suaosin> Station 2F Entrance.jpg|實際上位於蘇澳新站二樓的主入口 <File:Su'aoxin>
Station 1.JPG|由蘇澳新站後方遠望蘇澳新站跨站式的站房及多股鐵道 <File:Su'aoxin> Station
2.jpg|蘇澳新站後站出入口 <File:Su'aoxin> Station
3.jpg|蘇澳新站售票窗口、自動售票機及牆上圖示附近的景點及自行車道地圖
<File:Su'aoxin> Station 4.jpg|蘇澳新站以步行天橋連接兩座島式月台 <File:Su'aoxin> Station
5.jpg|由蘇澳新站前站方向遠眺站內的月台及鐵軌股道 <File:Su'aoxin> Station 6.jpg|蘇澳新站前站出入口門廳一景
<File:Su'aoxin> Station
7.jpg|蘇澳新站剪票閘口，剪票閘口旁亦設置藍色的電子票證刷卡機柱，出入口上方LED燈顯示近期到站列車時刻表
<File:Su'aoxin> Station 8.jpg|五角造型的蘇澳新站站名告示及前後站距及站名標註版 <File:Su'aoxin>
Station 9.jpg|一輛E300電力機車頭通過蘇澳新站月台

## 鄰近車站

  - **廢止營運模式**


<small>直通運行宜蘭線</small>

  - **現行營運模式**

|-

## 註釋與參考文獻

## 外部連結

  - [蘇澳新站（臺灣鐵路管理局）](http://www.railway.gov.tw/Su-aosin/index.aspx)

[Category:北迴線車站](../Category/北迴線車站.md "wikilink")
[Category:宜蘭縣鐵路車站](../Category/宜蘭縣鐵路車站.md "wikilink")
[Category:1968年启用的铁路车站](../Category/1968年启用的铁路车站.md "wikilink")
[Category:蘇澳鎮 (台灣)](../Category/蘇澳鎮_\(台灣\).md "wikilink")
[Category:宜蘭線車站](../Category/宜蘭線車站.md "wikilink")

1.  [蘇澳新站擠　每天6千陸客轉乘《好房網》](https://news.housefun.com.tw/news/article/898896109445.html)
2.  [台灣走親：蘇澳新站，宜蘭陸客安全轉運站](http://www.chinareviewnews.com/crn-webapp/search/allDetail.jsp?id=101559010&sw=%E5%8F%B0%E6%B9%BE%E8%B5%B0%E4%BA%B2%EF%BC%9A%E8%8B%8F%E6%BE%B3%E6%96%B0%E7%AB%99%EF%BC%8C%E5%AE%9C%E5%85%B0%E9%99%86%E5%AE%A2%E5%AE%89%E5%85%A8%E8%BD%AC%E8%BF%90%E7%AB%99)
3.