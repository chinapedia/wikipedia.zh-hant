**黑冠长臂猿属**（学名*Nomascus*）是[长臂猿科下的一属](../Page/长臂猿科.md "wikilink")，包括四种长臂猿。体色一般为黑色，两颊为白色或黄色。主要分布在[中国](../Page/中国.md "wikilink")[云南到](../Page/云南.md "wikilink")[越南南部](../Page/越南.md "wikilink")，[海南岛上也有分布](../Page/海南岛.md "wikilink")。[东部黑冠长臂猿是世界上最为濒危的猿类](../Page/东部黑冠长臂猿.md "wikilink")。

[Category:長臂猿科](../Category/長臂猿科.md "wikilink")