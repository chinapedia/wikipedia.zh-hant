**卡昂馬萊伯體育會**（**Stade Malherbe de
Caen**），乃一家位於[法國北部](../Page/法國.md "wikilink")[諾曼第的城市](../Page/諾曼第.md "wikilink")[卡昂之小型足球會](../Page/卡昂.md "wikilink")，成立於1913年，球隊以當地著名[詩人](../Page/詩人.md "wikilink")[馬萊伯](../Page/馬萊伯.md "wikilink")（）為名，於2007-08年度季再度升上[法甲聯賽](../Page/法甲.md "wikilink")。主場為「米歇尔·多尔纳诺球场」（Stade
Michel d'Ornano），可容納21,215人。

卡昂於2004/05年球季以乙組亞軍首次獲得升上甲組，但在季尾最後一仗敗陣僅排第18位回降乙組。在季中打入[聯賽盃決賽](../Page/法國聯賽盃.md "wikilink")，以1-2僅負於[斯特拉斯堡獲得亞軍](../Page/斯特拉斯堡足球俱乐部.md "wikilink")。

## 球會榮譽

| 年度      | 冠軍                                                             |
| ------- | -------------------------------------------------------------- |
| 1996年   | 乙組聯賽                                                           |
| 年度      | 亞軍                                                             |
| 1959年   | [甘巴德拉盃](../Page/:en:Coupe_Gambardella.md "wikilink")（U-18青年盃賽） |
| 1987年\* | Division 2-A                                                   |
| 1988年\* | Division 2-B                                                   |
| 1994年   | [甘巴德拉盃](../Page/:en:Coupe_Gambardella.md "wikilink")（U-18青年盃賽） |
| 2001年   | [甘巴德拉盃](../Page/:en:Coupe_Gambardella.md "wikilink")（U-18青年盃賽） |
| 2004年\* | 乙組聯賽                                                           |
| 2005年   | [聯賽盃](../Page/法國聯賽盃.md "wikilink")                             |
| 2007年\* | 乙組聯賽                                                           |

*\* 表示雖然未能奪得冠軍，但球隊仍然獲得升級*

  - [法國盃](../Page/法國盃.md "wikilink")

卡昂最佳成績是於2004年打入最後16強。

  - [聯賽盃](../Page/法國聯賽盃.md "wikilink")

卡昂於2005年聯賽盃決賽以1-2負於[斯特拉斯堡獲得亞軍](../Page/斯特拉斯堡足球俱乐部.md "wikilink")。

## 球員名單

<small>更新日期：2015年2月6日\[1\]</small>

### 現時效力（2014/15）

<table style="width:204%;">
<colgroup>
<col style="width: 47%" />
<col style="width: 47%" />
<col style="width: 14%" />
<col style="width: 47%" />
<col style="width: 14%" />
<col style="width: 9%" />
<col style="width: 14%" />
<col style="width: 9%" />
</colgroup>
<thead>
<tr class="header">
<th><p><font color="white">編號</p></th>
<th><p><font color="white">國籍</p></th>
<th><p><font color="white">球員名字</p></th>
<th><p><font color="white">位置</p></th>
<th><p><font color="white">出生日期</p></th>
<th><p><font color="white">加盟年份</p></th>
<th><p><font color="white">前屬球會</p></th>
<th><p><font color="white">身價</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><font color="white"><strong>門 將</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>1</strong></p></td>
<td></td>
<td><p><a href="../Page/:en:Rémy_Vercoutre.md" title="wikilink">雷米·维科特</a>（Rémy Vercoutre）</p></td>
<td><p>門將</p></td>
<td><p>1980.06.26</p></td>
<td><p>2014</p></td>
<td><p><a href="../Page/奧林匹克里昂.md" title="wikilink">里昂</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>16</strong></p></td>
<td></td>
<td><p><a href="../Page/:en:Damien_Perquis_(footballer_born_1986).md" title="wikilink">達米恩·柏基斯</a>（Damien Perquis）</p></td>
<td><p>門將</p></td>
<td><p>1986.03.08</p></td>
<td><p>2008</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>30</strong></p></td>
<td></td>
<td><p><a href="../Page/保罗·卢拉特.md" title="wikilink">保罗·卢拉特</a>（Paul Reulet）</p></td>
<td><p>門將</p></td>
<td><p>1994.01.14</p></td>
<td></td>
<td><p>青年隊</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><font color="white"><strong>後 衛</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>5</strong></p></td>
<td></td>
<td><p><a href="../Page/:en:Alaeddine_Yahia.md" title="wikilink">阿莱丁·雅西亚</a>（Alaeddine Yahia）</p></td>
<td><p>後衛</p></td>
<td><p>1981.09.26</p></td>
<td><p>2014</p></td>
<td><p><a href="../Page/朗斯競賽會.md" title="wikilink">朗斯</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>12</strong></p></td>
<td></td>
<td><p><a href="../Page/:en:Dennis_Appiah.md" title="wikilink">丹尼斯·阿皮亚</a>（Dennis Appiah）</p></td>
<td><p>後衛</p></td>
<td><p>1992.06.09</p></td>
<td><p>2013</p></td>
<td><p><a href="../Page/摩納哥體育會足球會.md" title="wikilink">摩納哥</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>15</strong></p></td>
<td></td>
<td><p><a href="../Page/:en:Emmanuel_Imorou.md" title="wikilink">埃马努埃尔·伊莫鲁</a>（Emmanuel Imorou）</p></td>
<td><p>後衛</p></td>
<td><p>1988.09.16</p></td>
<td><p>2014</p></td>
<td><p><a href="../Page/克莱蒙足球俱乐部.md" title="wikilink">克莱蒙</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>19</strong></p></td>
<td></td>
<td><p><a href="../Page/:en:Felipe_Saad.md" title="wikilink">費利佩·薩阿德</a>（Felipe Saad）</p></td>
<td><p>後衛</p></td>
<td><p>1983.09.11</p></td>
<td><p>2013</p></td>
<td><p><a href="../Page/阿些斯奧足球會.md" title="wikilink">阿些斯奧</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>22</strong></p></td>
<td></td>
<td><p><a href="../Page/:en:Alexandre_Raineau.md" title="wikilink">亚历山大·莱尼奥</a>（Alexandre Raineau）</p></td>
<td><p>後衛</p></td>
<td><p>1986.06.21</p></td>
<td><p>2006</p></td>
<td><p>青年隊</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>23</strong></p></td>
<td></td>
<td><p><a href="../Page/:en:Jean_Calvé.md" title="wikilink">让·卡尔夫</a>（Jean Calvé）</p></td>
<td><p>後衛</p></td>
<td><p>1984.04.30</p></td>
<td><p>2012</p></td>
<td><p><a href="../Page/南錫體育會.md" title="wikilink">南錫</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>28</strong></p></td>
<td></td>
<td><p><a href="../Page/:en:Damien_Da_Silva.md" title="wikilink">达米安·达席尔瓦</a>（Damien Da Silva）</p></td>
<td><p>後衛</p></td>
<td><p>1988.05.17</p></td>
<td><p>2014</p></td>
<td><p><a href="../Page/克莱蒙足球俱乐部.md" title="wikilink">克莱蒙</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>29</strong></p></td>
<td></td>
<td><p><a href="../Page/:en:Yrondu_Musavu-King.md" title="wikilink">隆杜·穆萨武-金</a>（Yrondu Musavu-King）</p></td>
<td><p>後衛</p></td>
<td><p>1992.01.08</p></td>
<td><p>2012</p></td>
<td><p>青年隊</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><font color="white"><strong>中 場</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2</strong></p></td>
<td></td>
<td><p><a href="../Page/:en:Nicolas_Seube.md" title="wikilink">尼古拉斯·塞乌贝</a>（Nicolas Seube）</p></td>
<td><p>中場</p></td>
<td><p>1979.08.11</p></td>
<td><p>2001</p></td>
<td><p><a href="../Page/土魯斯足球俱樂部.md" title="wikilink">土魯斯</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>3</strong></p></td>
<td></td>
<td><p><a href="../Page/:en:Yannick_M&#39;Bone.md" title="wikilink">扬尼克·姆博内</a>（Yannick M'Bone）</p></td>
<td><p>中場</p></td>
<td><p>1993.04.16</p></td>
<td><p>2011</p></td>
<td><p>青年隊</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>7</strong></p></td>
<td></td>
<td><p><a href="../Page/:en:Nicolas_Benezet.md" title="wikilink">尼克拉斯·贝内泽</a>（Nicolas Benezet）</p></td>
<td><p>中場</p></td>
<td><p>1991.02.24</p></td>
<td><p>2015</p></td>
<td><p>租借自<a href="../Page/伊维恩托农盖拉德足球俱乐部.md" title="wikilink">伊維恩</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>17</strong></p></td>
<td></td>
<td><p><a href="../Page/:en:N&#39;Golo_Kanté.md" title="wikilink">恩戈洛·康德</a>（N'Golo Kanté）</p></td>
<td><p>中場</p></td>
<td><p>1991.03.29</p></td>
<td><p>2013</p></td>
<td><p><a href="../Page/布羅尼體育會.md" title="wikilink">布羅尼</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>18</strong></p></td>
<td></td>
<td><p><a href="../Page/:en:Jordan_Adéoti.md" title="wikilink">乔丹·阿德奥蒂</a>（Jordan Adéoti）</p></td>
<td><p>中場</p></td>
<td><p>1989.03.12</p></td>
<td><p>2014</p></td>
<td><p><a href="../Page/拉瓦勒足球俱樂部.md" title="wikilink">拉瓦勒</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>21</strong></p></td>
<td></td>
<td><p><a href="../Page/:en:José_Saez.md" title="wikilink">何塞·賽斯</a>（José Saez）</p></td>
<td><p>中場</p></td>
<td><p>1982.05.07</p></td>
<td><p>2014</p></td>
<td><p><a href="../Page/瓦朗謝訥足球俱樂部.md" title="wikilink">瓦朗謝訥</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>25</strong></p></td>
<td></td>
<td><p><a href="../Page/:en:Julien_Féret.md" title="wikilink">朱利安·弗伦特</a>（Julien Féret）</p></td>
<td><p>中場</p></td>
<td><p>1982.07.05</p></td>
<td><p>2014</p></td>
<td><p><a href="../Page/雷恩足球俱樂部.md" title="wikilink">雷恩</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>26</strong></p></td>
<td></td>
<td><p><a href="../Page/:en:Jonathan_Beaulieu.md" title="wikilink">乔纳森·比利</a>（Jonathan Beaulieu）</p></td>
<td><p>中場</p></td>
<td><p>1993.03.11</p></td>
<td><p>2010</p></td>
<td><p>青年隊</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>27</strong></p></td>
<td></td>
<td><p><a href="../Page/:en:Thomas_Lemar.md" title="wikilink">托马斯·利马</a>（Thomas Lemar）</p></td>
<td><p>中場</p></td>
<td><p>1995.11.12</p></td>
<td><p>2013</p></td>
<td><p>青年隊</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><font color="white"><strong>前 鋒</strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>9</strong></p></td>
<td></td>
<td><p><a href="../Page/:en:Sloan_Privat.md" title="wikilink">斯洛安·普里瓦特</a>（Sloan Privat）</p></td>
<td><p>前鋒</p></td>
<td><p>1989.07.24</p></td>
<td><p>2014</p></td>
<td><p>租借自<a href="../Page/KAA根特.md" title="wikilink">KAA根特</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>10</strong></p></td>
<td></td>
<td><p><a href="../Page/:en:Lenny_Nangis.md" title="wikilink">莱尼·昂尼斯</a>（Lenny Nangis）</p></td>
<td><p>前鋒</p></td>
<td><p>1994.03.24</p></td>
<td><p>2011</p></td>
<td><p>青年隊</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>11</strong></p></td>
<td></td>
<td><p><a href="../Page/:en:Bengali-Fodé_Koita.md" title="wikilink">本加里·科伊塔</a>（Bengali-Fodé Koita）</p></td>
<td><p>前鋒</p></td>
<td><p>1990.10.21</p></td>
<td><p>2013</p></td>
<td><p><a href="../Page/蒙彼利埃體育會.md" title="wikilink">蒙彼利埃</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>14</strong></p></td>
<td></td>
<td><p><a href="../Page/:en:Emiliano_Sala.md" title="wikilink">埃米利亚诺·萨拉</a>（Emiliano Sala）</p></td>
<td><p>前鋒</p></td>
<td><p>1990.10.31</p></td>
<td><p>2015</p></td>
<td><p>租借自<a href="../Page/波爾多足球俱樂部.md" title="wikilink">波爾多</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>20</strong></p></td>
<td></td>
<td><p><a href="../Page/:en:Hervé_Bazile.md" title="wikilink">埃尔韦·巴兹勒</a>（Hervé Bazile）</p></td>
<td><p>前鋒</p></td>
<td><p>1990.03.18</p></td>
<td><p>2014</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 著名球員

  - [堅尼安達臣](../Page/:en:Kennet_Andersson.md "wikilink")

  - [加拿斯](../Page/加拿斯.md "wikilink")

  - [法蘭克‧杜馬斯](../Page/:en:Franck_Dumas.md "wikilink")

  - [謝斯柏·奧臣](../Page/謝斯柏·奧臣.md "wikilink")

  - [比利爾](../Page/:en:Stéphane_Paille.md "wikilink")

  - [熱羅姆·羅滕](../Page/熱羅姆·羅滕.md "wikilink")

  - [阿倫·巴古沙](../Page/阿倫·巴古沙.md "wikilink")

## 參考來源

## 外部連結

  - [球會官方網站](http://www.smcaen.fr/) (法文)

[Caen](../Category/法國足球俱樂部.md "wikilink")
[Category:1913年建立的足球俱樂部](../Category/1913年建立的足球俱樂部.md "wikilink")
[Category:1913年法國建立](../Category/1913年法國建立.md "wikilink")

1.  [Effectif Caen – Equipe
    Pro](http://www.smcaen.fr/groupe-pro/effectif)