**2006年九鐵人事變動風波**是由2006年3月10日起[九廣鐵路公司出現的一場高級行政人員人事變動事件](../Page/九廣鐵路公司.md "wikilink")。事件直接導致九廣鐵路公司署理[行政總裁](../Page/行政總裁.md "wikilink")[黎文熹辭職](../Page/黎文熹.md "wikilink")，市務[總經理](../Page/總經理.md "wikilink")[黎啟憲被解約及潘錦全被申訴和廉政專員公署飭令停薪留職](../Page/黎啟憲.md "wikilink")，另有19名高級行政人員收到警告信。

## 背景

### 公司文化

由於[九廣鐵路公司於](../Page/九廣鐵路公司.md "wikilink")1982年成立，但部份職員早於該公司成立之前已加入其前身「[九廣鐵路局](../Page/九廣鐵路局.md "wikilink")」工作，基於他們的工作習慣，令該公司的公司文化一直被外界所詬病，並認為是不合時宜。

在管理層面方面，一些中層管理人員只准下屬按上級的意願來工作，並不喜歡其下屬自發地做一些原有工作範圍以外的工作，即使下屬多做一點點也要責備。結果令到前線員工有一個「少做少錯，不做便會沒有錯」的心理。因為員工們就認為鐵路意外是需要數項錯誤同時出現才會發生。故此員工們都有一個心態，若只犯上一個工作上的輕微失誤，員工們都只會感到該失誤是無傷大雅。

而九鐵內部亦有一種「[理想主義文化](../Page/理想主義.md "wikilink")」，所有問題都要有待事情解決後，有詳細的數據才上報，而有關的員工大多也只會自行解決問題。而上報的時候，即使中層員工沒有蓄意隱瞞，但由於資料不清晰，令到即使是管理局的成員也不能即時知道有關運作上的問題。九鐵管理局主席[田北辰亦指出](../Page/田北辰.md "wikilink")，這做法已不能符合現今社會對透明度的迫切性要求，即使田氏自從2001年以來擔任董事局主席，也未能成功改變這種文化。

### 管理局主席和行政總裁分拆

自從香港政府於1996年委任[楊啟彥擔任九鐵](../Page/楊啟彥.md "wikilink")[管理局主席兼](../Page/董事局主席.md "wikilink")[行政總裁以來](../Page/行政總裁.md "wikilink")，九鐵就開始被外界冠上「退休高官俱樂部」的惡名，原因是由於當時管理局主席以及行政總裁是由同一人出任，故此管理局主席有絕對的控制權。時任九鐵主席的[楊啟彥](../Page/楊啟彥.md "wikilink")，甫上任便安排[詹伯樂](../Page/詹伯樂.md "wikilink")、[梁國新](../Page/梁國新_\(香港\).md "wikilink")、[丘李賜恩等前香港政府官員加入九鐵](../Page/丘李賜恩.md "wikilink")，而九鐵主席更可以自行釐訂薪酬。另外，當九鐵要求加價之時，有傳媒揭發了九鐵管理局以公司名義，獨享3艘遊艇、8輛名貴私家車、40多個公司俱樂部會籍等奢侈而非必要的福利，為外界所非議。

政府有感該等問題日漸嚴重，如同「獨立王國」。為此，政府於2001年修訂《九廣鐵路公司條例》，將管理局主席及行政總裁交由兩人出任，以加強政府對九鐵的管治，並於同年12月委任田北辰為九鐵管理局主席，而[楊啟彥則為九鐵行政總裁](../Page/楊啟彥.md "wikilink")。

## 事件經過及結果

2006年3月10日，九鐵署理[行政總裁](../Page/行政總裁.md "wikilink")[黎文熹連同全部](../Page/黎文熹.md "wikilink")5名總監及19名總經理，寫了一封機密信給公司管理局信去管理局，指主席田北辰在九鐵內部干預過多、要求透明度過高及過份要求問責、公司主席和行政總裁權責不清等十大指控，形成一種「[責難文化](../Page/責難文化.md "wikilink")」，並在兩日內收集到九鐵約6000多名員工中近80%員工的逾3000個簽名。

3月12日，田北辰獲悉後感到震驚，[田北辰和行政長官](../Page/田北辰.md "wikilink")[曾蔭權會面](../Page/曾蔭權.md "wikilink")，並向曾提請辭職要求。在3月12日下午，田北辰召開記者會，宣佈辭職，但生效日期由曾蔭權決定。

田北辰辭職後，他在記者會上成功打造了一個改革者遇到挫折的景象，一些員工和市民便開始支持他而令到形勢逆轉。

另一方面，為數20名總經理及部門主管，趁着管理局進行會議期間，在該地點旁邊舉行記者會；任職市務總經理的黎啟憲發言時指田北辰為了短期利益而忽視長遠發展，故此辭職是最佳方案。

3月14日，《[明報](../Page/明報.md "wikilink")》公開了一封黎文熹遞交九鐵管理局關於對主席田北辰表示不滿的信件內容，事件曝光後黎文熹雖然在信內明確指出不能與田北辰共事，卻否認有要求田北辰下台，田北辰對黎文熹的回應表示「驚訝」。另一方面，九鐵管理局召開特別會議處理事件，[田北辰和](../Page/田北辰.md "wikilink")[黎文熹均有出席](../Page/黎文熹.md "wikilink")。當日下午2時，以九鐵市務[總經理](../Page/總經理.md "wikilink")[黎啟憲為首的](../Page/黎啟憲.md "wikilink")20名九鐵高層人員集體請假，到位於[中環的西鐵總部召開特別會議的會議室旁的房間召開記者會](../Page/中環.md "wikilink")，聲援黎文熹及支持田北辰辭職。會後時任[環境運輸及工務局局長](../Page/環境運輸及工務局.md "wikilink")[廖秀冬博士宣報會議取得有建設性的進展](../Page/廖秀冬.md "wikilink")。九鐵3個工會對事件表示中立。

3月15日，[曾蔭權](../Page/曾蔭權.md "wikilink")、廖秀冬、田北辰和黎文熹在[政府總部召開記者會](../Page/政府總部.md "wikilink")，宣布田北辰將留任九鐵管理局主席，田黎二人更四手相握以示和好。曾蔭權並指出九鐵出現嚴重紀律問題，故他已下令九鐵管理局處理事件。

3月16日，九鐵管理局中午召開會議後，黎文熹親自宣佈管理局的決定，管理局與黎啟憲解除[合約](../Page/合約.md "wikilink")，原因是黎啟憲在聲援黎文熹而帶領19名九鐵高層人士集體請假有擅離職守之問題；而其餘19名管理層則獲發警告信，如一年內再犯則解僱。同時黎文熹宣佈為九鐵出現嚴重紀律問題[問責而辭去署理行政總裁一職](../Page/問責.md "wikilink")，由曾任[新鐵路工程高級總監及前](../Page/西鐵綫.md "wikilink")[工務司的](../Page/工務司.md "wikilink")[詹伯樂出任署理行政總裁](../Page/詹伯樂.md "wikilink")。根據香港明報的報導，管理局與黎啟憲解約和黎文熹引咎辭職，是黎文熹本人提出的，而1979年加入無線電視的潘錦全則於4月1日執行紀律性處分。

黎文熹在兩鐵合併之後的一星期，發表了報告文學《九天風雲》；內容是講述這件事情的一些不為人知的內幕和來龍去脈。

## 事後紀錄

於2007年12月，即事隔超過一年半，兩鐵剛正式[合併後不久](../Page/兩鐵合併.md "wikilink")，[黎文熹為其親自撰寫](../Page/黎文熹.md "wikilink")，並講述當年九廣鐵路「[兵變](../Page/兵變.md "wikilink")」事件過程的書籍《[九天風雲](../Page/九天風雲.md "wikilink")》舉行發佈會。於會中，他表示是以平實的態度去寫，而非為發洩不滿。他亦表示之所以在兩鐵合併後才出書，是因為免影響到合併前的地鐵和九鐵員工。
到2008年3月5日，潘錦全宣布由6月1日起辭任港鐵助理營運總裁，準備轉投中電集團出任助理營運總監。

## 參看

  - [九廣鐵路公司](../Page/九廣鐵路公司.md "wikilink")
  - [田北辰](../Page/田北辰.md "wikilink")
  - [黎文熹](../Page/黎文熹.md "wikilink")
  - [黎啟憲](../Page/黎啟憲.md "wikilink")

## 外部連結

  - [九廣鐵路公司](http://www.kcrc.com/tc/)

[Category:九廣鐵路](../Category/九廣鐵路.md "wikilink")
[Category:2006年香港](../Category/2006年香港.md "wikilink")