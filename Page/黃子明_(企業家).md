**黃子明**（；；），是[泰國及](../Page/泰國.md "wikilink")[香港](../Page/香港.md "wikilink")[企業家](../Page/企業家.md "wikilink")，香港兩間上市公司的創辦人，原籍[廣東省](../Page/廣東省.md "wikilink")[普寧市](../Page/普寧市.md "wikilink")[流沙鎮](../Page/流沙南街道.md "wikilink")[馬柵村](../Page/馬柵村.md "wikilink")，[中國出生](../Page/中國.md "wikilink")。

## 簡歷

年青時代到[泰國工作](../Page/泰國.md "wikilink")，26歲與[朋友合資在](../Page/朋友.md "wikilink")[曼谷創立通城錶行](../Page/曼谷.md "wikilink")。1960年代，業務發展至香港，成立[寶光實業](../Page/寶光實業.md "wikilink")，[代理](../Page/代理.md "wikilink")[精工錶](../Page/精工錶.md "wikilink")。1970年代，開始代工生產[瑞士名錶](../Page/瑞士名錶.md "wikilink")[鐵達時及](../Page/鐵達時.md "wikilink")[寶路華](../Page/寶路華.md "wikilink")。另外又成立[華基泰代理銷售](../Page/華基泰.md "wikilink")[德國](../Page/德國.md "wikilink")[品牌](../Page/品牌.md "wikilink")[彪馬及](../Page/彪馬.md "wikilink")[意大利](../Page/意大利.md "wikilink")[Lotto](../Page/Lotto.md "wikilink")[運動鞋等](../Page/運動鞋.md "wikilink")。在1972年及1988年，以上兩間公司在[香港成為](../Page/香港.md "wikilink")[上市公司](../Page/上市公司.md "wikilink")。在1991及1992年，黃子明也把從事[泰國地產業的](../Page/泰國地產業.md "wikilink")[泰華榮](../Page/泰華榮.md "wikilink")（[BTS集團控股前身](../Page/BTS集團控股.md "wikilink")）及[曼谷置地兩間公司在泰國上市](../Page/曼谷置地.md "wikilink")。在黃子明的黃金年代，他控有[上市公司總共有](../Page/上市公司.md "wikilink")4家，總市值達600億港元。

黃子明於2003年6月因[心臟病離世](../Page/心臟病.md "wikilink")。\[1\]

## 家族

黃子明的父親[黃邦賢於](../Page/黃邦賢.md "wikilink")1928年移民到[泰國](../Page/泰國.md "wikilink")。\[2\]
黃子明的妻子是[莊麗華](../Page/莊麗華.md "wikilink")，兩人育有六子五女，六個兒子名叫：\[3\]\[4\]

  - [黃創保](../Page/黃創保.md "wikilink") （Anant Kanjanapas）
  - [黃創江](../Page/黃創江.md "wikilink")
  - [黃創山](../Page/黃創山.md "wikilink")
  - [黃創增](../Page/黃創增.md "wikilink")
  - [黃創耀](../Page/黃創耀.md "wikilink")
  - [黃創華](../Page/黃創華.md "wikilink")

[香港大學有](../Page/香港大學.md "wikilink")[明華合樓](../Page/明華合樓.md "wikilink")，包括[黃子明樓與](../Page/黃子明樓.md "wikilink")[黃莊麗華樓](../Page/黃莊麗華樓.md "wikilink")。

## 相關

  - [黃創光](../Page/黃創光.md "wikilink")：黃子明的姪兒

## 参考文献

[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:泰國華人](../Category/泰國華人.md "wikilink")
[黃子明](../Category/普宁人.md "wikilink")
[Zi子明](../Category/黃姓.md "wikilink")
[Category:潮商](../Category/潮商.md "wikilink")

1.  [黃子明家族](http://appledaily.atnext.com/template/apple/art_main.cfm?iss_id=20070725&sec_id=4104&subsec_id=11866&art_id=7362704)

2.  [黃子明家族](http://taohwang.xinwen520.net/a4-101.htm)

3.
4.  [寶光實業公司董事簡介](http://main.ednews.hk/listedco/listconews/sehk/20060728/00084/CWF116.pdf)