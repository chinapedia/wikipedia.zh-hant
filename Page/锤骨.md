**錘骨**(malleus, hammer) 是人耳中錘狀的小[骨](../Page/骨.md "wikilink"),
是三块[聽小骨](../Page/聽小骨.md "wikilink")（ossicles）中的一個,连接耳膜和砧骨。

[Malleus_Gray.jpg](https://zh.wikipedia.org/wiki/File:Malleus_Gray.jpg "fig:Malleus_Gray.jpg")<small>人的槌骨</small>

  - <small>*Head*：槌骨頭</small>
  - <small>*Neck*：槌骨頚</small>
  - <small>*Lateral process*：外側突起</small>
  - <small>*Anterior process*：前突起</small>
  - <small>*Manubrium*：槌骨柄</small>

## 相片

<File:Illu>` auditory ossicles.jpg|Ossicles`
[`File:Gray43.png|Head`](File:Gray43.png%7CHead)` and neck of a human embryo eighteen weeks old, with Meckel’s cartilage and hyoid bar exposed.`
[`File:Gray907.png|External`](File:Gray907.png%7CExternal)` and middle ear, opened from the front. Right side.`
[`File:Gray919.png|Chain`](File:Gray919.png%7CChain)` of ossicles and their ligaments, seen from the front in a vertical, transverse section of the tympanum. `

## 参考资料

<references/>

## 外部連結

  - [The Anatomy Wiz](http://www.anatomywiz.com?Malleus1)Malleus

## 参见

  - [人体骨骼列表](../Page/人体骨骼列表.md "wikilink")

[nl:Gehoorbeentje\#Hamer](../Page/nl:Gehoorbeentje#Hamer.md "wikilink")

[Category:解剖学](../Category/解剖学.md "wikilink")
[Category:耳科](../Category/耳科.md "wikilink")
[Category:頭頸骨](../Category/頭頸骨.md "wikilink")