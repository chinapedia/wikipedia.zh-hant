[古埃及文明大约出现在公元](../Page/古埃及.md "wikilink")[前5千年](../Page/前5千年.md "wikilink")，在约前3100年埃及成为统一的[国家](../Page/国家.md "wikilink")，由[法老统治](../Page/法老.md "wikilink")，之后的埃及因此也称为法老埃及。前332年时埃及被亚历山大大帝征服，后被[托勒密王朝](../Page/托勒密王朝.md "wikilink")（虽然也自称法老）统治，古埃及时代结束。[埃及学家通常把](../Page/埃及学.md "wikilink")**古埃及历史**划分为[前王朝时期](../Page/前王朝时期.md "wikilink")、[早王朝时期](../Page/早王朝时期.md "wikilink")、[古王国时期](../Page/古王国时期.md "wikilink")、[第一中间期](../Page/第一中间期.md "wikilink")、[中王国时期](../Page/中王国时期.md "wikilink")、[第二中间期](../Page/第二中间期.md "wikilink")、[新王国时期](../Page/新王国时期.md "wikilink")、[第三中间期和](../Page/第三中间期.md "wikilink")[古埃及后期等](../Page/古埃及后期.md "wikilink")9个时期，其中前王朝时期是埃及统一之前的文明初期，早王朝时期是古埃及法老统治逐渐形成的时期，古、中、新王国时期则是国家统一，法老中央集权的文明繁荣时期，而第一、二、三中间期则是国家分裂，或被外族侵略，法老权力没落的时期，最后的古埃及后期是埃及受到外族全面侵略和统治，逐渐被其他民族征服的时期。

## 古埃及时期和王朝

古埃及历史一般被划分为8或9个时期，即[前王朝时期](../Page/前王朝时期.md "wikilink")、[早王朝时期](../Page/早王朝时期.md "wikilink")、[古王国时期](../Page/古王国时期.md "wikilink")、[第一中间期](../Page/第一中间期.md "wikilink")、[中王国时期](../Page/中王国时期.md "wikilink")、[第二中间期](../Page/第二中间期.md "wikilink")、[新王国时期](../Page/新王国时期.md "wikilink")、[第三中间期和](../Page/第三中间期.md "wikilink")[古埃及后期](../Page/古埃及后期.md "wikilink")，这是后来埃及学家根据古埃及国家统一、繁荣以及中央政府权力等情况进行的划分，而不是古埃及人自己的划分标准。古埃及人對於歷史編年的劃分，並沒有朝代與特定紀年標準的概念，甚至也沒有以統治者在位時期進行紀年的概念（例如兩河流域的紀年方式）。相較之下，古埃及早期的埃及人似乎并不划分历史时期，例如[巴勒莫石碑](../Page/巴勒莫石碑.md "wikilink")（[:en:Palermo
Stone](../Page/:en:Palermo_Stone.md "wikilink")，刻有古埃及最初到第五王朝中期的法老列表）列举法老时并不进行任何划分。后来埃及人则根据法老的出身地进行划分，如[都林王名表](../Page/都林王名表.md "wikilink")（Turin
Kinglist，至少记录至第二十王朝，但残缺不全）。现代通用的30或31个埃及王朝则是由托勒密王朝早期古埃及祭祀曼涅托在《埃及史》（[希腊文轉寫](../Page/希腊文.md "wikilink")：Aegyptiaca）一书中提出的，由于《埃及史》原书已经遗失，其划分王朝的标准现在不清楚。例如埃及第十八王朝第一任法老[雅赫摩斯一世是埃及第十七王朝最后一任法老](../Page/雅赫摩斯一世.md "wikilink")[卡摩斯的弟弟](../Page/卡摩斯.md "wikilink")，按说应该划为一个王朝中。而之后的[图特摩斯一世则不是雅赫摩斯一世的后代](../Page/图特摩斯一世.md "wikilink")，却被同归入第十八王朝。埃及学家推测曼涅托的划分标准是部分根据神话传说、部分根据历史事实、部分根据已有的划分进行的，并不完全根据法老家族进行。例如上面的例子可以解释为雅赫摩斯一世统一埃及，驱逐了外族侵略者，因此开始了一个新的时代（王朝）。在第一王朝和第二王朝的划分上，曼涅托可能根据神话和宗教标准，使两个王朝都有9个法老，因为9这一数字在古埃及是神圣的。一些埃及学家试图抛弃曼涅托的划分，而重新进行更合理的劃分，但由于约定俗成的因素，大多数著作和学者都还一直沿用这一劃分。因為，這種以法老名稱進行的王者表列，事實容易產生許多的問題。\[1\]\[2\]

## 前王朝时期

距今9000多年前，人们在[尼罗河](../Page/尼罗河.md "wikilink")[河谷定居](../Page/河谷.md "wikilink")，开始在岸边建立房屋，并进行[农业和](../Page/农业.md "wikilink")[畜牧业生产活动](../Page/畜牧业.md "wikilink")。\[3\]
距今7000多年前，埃及人开始使用[铜器](../Page/铜器.md "wikilink")，为文明的形成奠定了基础。之后古埃及进入前王朝一期，又称为[阿姆拉特时期](../Page/阿姆拉特时期.md "wikilink")，[私有制和](../Page/私有制.md "wikilink")[阶级萌芽](../Page/阶级.md "wikilink")。到前王朝二期（即[格尔塞时期](../Page/格尔塞时期.md "wikilink")），埃及私有制和王权确立，在出土的文物中可以找到象征王权的[荷鲁斯鹰神的形象](../Page/荷鲁斯.md "wikilink")。格尔塞时期后期，[国家出现](../Page/国家.md "wikilink")，但面积很小，人口不多，随后国家之间不断征战，逐渐统一成为尼罗河上游河谷地区和尼罗河入海口三角洲地区的[上埃及和](../Page/上埃及.md "wikilink")[下埃及两个国家](../Page/下埃及.md "wikilink")。[象形文字也在这个时候出现](../Page/象形文字.md "wikilink")，并沿用了3500余年。

公元前3100年左右，传说上埃及国王[美尼斯统一上](../Page/美尼斯.md "wikilink")、下埃及，建立[第一王朝](../Page/第一王朝.md "wikilink")，定都[孟斐斯](../Page/孟斐斯_\(埃及\).md "wikilink")（今[开罗西郊](../Page/开罗.md "wikilink")），成为古埃及第一个[法老](../Page/法老.md "wikilink")，古埃及从此开始了王朝时期。此时的埃及已经具备了[文明的几个基本特征](../Page/文明.md "wikilink")，比如有行政官员、士兵、宗教、文字等。\[4\]

## 早王朝时期、古王国时期和第一中间期

古埃及统一之后，在很长一段时间是稳定的，这段时间经历了从[第一王朝到](../Page/埃及第一王朝.md "wikilink")[第六王朝共六个王朝](../Page/埃及第六王朝.md "wikilink")，时间大约为前3100年到前2270年。古埃及历史学家[曼涅托将其称为](../Page/曼涅托.md "wikilink")“[古王国时期](../Page/古王国时期.md "wikilink")”。这是古埃及史上农业、[手工业](../Page/手工业.md "wikilink")、[商业](../Page/商业.md "wikilink")、[建筑业等各项事业全面发展的第一个伟大时代](../Page/建筑业.md "wikilink")。确立了以官僚体制为基础的、君主独裁的专制统治，并且出现了[金字塔](../Page/金字塔.md "wikilink")。[第六王朝以后](../Page/埃及第六王朝.md "wikilink")，王权衰落，法老失去了对国家各地区的控制，国家开始分裂，史称“[第一中间时期](../Page/第一中间时期.md "wikilink")”（前2270年－前2060年）。这种分裂形式到[十一王朝重新统一](../Page/埃及第十一王朝.md "wikilink")。\[5\]

## 中王国时期、第二中间期和新王国时期

之后埃及进入第二个政治隐定期即[中王国时期](../Page/中王国时期.md "wikilink")（前2060年－前1785年）。埃及在[十二王朝时迁都](../Page/埃及第十二王朝.md "wikilink")[底比斯](../Page/底比斯_\(埃及\).md "wikilink")（今[埃及](../Page/埃及.md "wikilink")[卢克索](../Page/卢克索.md "wikilink")），开始使用[青铜器](../Page/青铜器.md "wikilink")。此时期埃及与[叙利亚](../Page/叙利亚.md "wikilink")、[克里特的交往扩大](../Page/克里特.md "wikilink")。[十三王朝时政权又瓦解](../Page/埃及第十三王朝.md "wikilink")，“第二中间时期”开始，此时期埃及第一次遭到外族的侵略，侵略者为驾车作战的[喜克索人](../Page/喜克索人.md "wikilink")，他们占领了埃及北部的大部分地区，建立了长达100多年的“[太阳神不在的统治](../Page/太阳神.md "wikilink")”（前1720年到前1570年）。埃及人在这期间学习了喜克索人的战术和武器，[十七王朝的](../Page/埃及第十七王朝.md "wikilink")[阿赫摩斯一世于前](../Page/阿赫摩斯一世.md "wikilink")1570年将喜克索人逐出国境，重新统一埃及，开始了[第十八王朝](../Page/第十八王朝.md "wikilink")，这之后被称为[新王国时期](../Page/新王国时期.md "wikilink")（前1570年－前1070年）。第十八王朝国力强盛，对外频繁发动战争。[十九王朝时埃及与](../Page/埃及第十九王朝.md "wikilink")[赫梯帝国发生了](../Page/赫梯帝国.md "wikilink")[卡迭石战役](../Page/卡迭石战役.md "wikilink")，经过16年之久的战争，最后以[拉美西斯二世与赫梯王](../Page/拉美西斯二世.md "wikilink")[哈图西利斯二世签订和约告终](../Page/哈图西利斯二世.md "wikilink")。此时的埃及成为了一个大帝国，统治范围北起叙利亚，南到尼罗河第四瀑布，横跨[北非和](../Page/北非.md "wikilink")[西亚](../Page/西亚.md "wikilink")。

## 第三中间时期、后期和结束

埃及到了[二十王朝以后](../Page/埃及第二十王朝.md "wikilink")，一系列的奴隶起义导致国力衰竭，开始了跨越5个王朝的[第三中间时期](../Page/第三中间时期.md "wikilink")（前1070年－前664年），其间的王朝有本土的[第二十一王朝](../Page/埃及第二十一王朝.md "wikilink")，利比亞人的[二十二](../Page/埃及第二十二王朝.md "wikilink")、[二十三](../Page/埃及第二十三王朝.md "wikilink")、[二十四和努比亚人的](../Page/埃及第二十四王朝.md "wikilink")[二十五王朝](../Page/埃及第二十五王朝.md "wikilink")。埃及自[第二十六王朝进入](../Page/埃及第二十六王朝.md "wikilink")[古埃及后期](../Page/古埃及后期.md "wikilink")，最终在前525年被[波斯](../Page/波斯.md "wikilink")[阿契美尼德帝国所灭](../Page/阿契美尼德帝国.md "wikilink")，古埃及时代结束了。波斯人在埃及建立了第二十七王朝，埃及二十六王朝后裔反抗波斯人成功，建立了短暂的第二十八、二十九和三十王朝。至前343年再被波斯人征服（[第三十一王朝](../Page/第三十一王朝.md "wikilink")），前332年埃及又被[亚历山大大帝所统治](../Page/亚历山大大帝.md "wikilink")，亚历山大死后，其部将[托勒密一世占领了埃及](../Page/托勒密一世.md "wikilink")，建立了[托勒密王朝](../Page/托勒密王朝.md "wikilink")，也被称为[法老](../Page/法老.md "wikilink")，但当时的埃及已经是彻底在外族人的统治下了。
托勒密王朝最後於[克麗奧佩特拉七世去世後被羅馬滅亡](../Page/埃及艳后.md "wikilink")。

## 参见

  - [古埃及](../Page/古埃及.md "wikilink")
  - [埃及学](../Page/埃及学.md "wikilink")

## 参考來源

[Category:古埃及](../Category/古埃及.md "wikilink")
[Category:已不存在的國家歷史](../Category/已不存在的國家歷史.md "wikilink")

1.  《古埃及》[1](http://www.ancient-egypt.org/index.html)，古埃及网（ancient-eqypt.org）

2.  《曼涅托》（Manetho），古埃及网（ancient-eqypt.org）

3.  英国博物馆，《古埃及·古埃及历史年表》（Ancient Egypt-A time line of ancient Egyptian
    history）[2](http://www.ancientegypt.co.uk/time/explore/main-time.html)，2007年6月26日访问

4.  斯塔夫里阿诺斯著，吴象婴、梁赤民译，《全球通史》，上海社会科学院出版社，2001年出版

5.