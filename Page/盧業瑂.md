**盧業瑂**（），[香港](../Page/香港.md "wikilink")[女歌手](../Page/女歌手.md "wikilink")，電台[節目主持人](../Page/節目主持.md "wikilink")，曾擔任[電視台](../Page/電視台.md "wikilink")[編導](../Page/編導.md "wikilink")。

盧業瑂在1980年代初成為[亞洲電視的](../Page/亞洲電視.md "wikilink")[編導](../Page/編導.md "wikilink")，協助製作「亞洲業餘歌唱大賽」，其間與[區瑞強於民歌餐廳演出](../Page/區瑞強.md "wikilink")。幾乎同時加入[商業電台成為](../Page/商業電台.md "wikilink")[唱片騎師](../Page/唱片騎師.md "wikilink")，並成為電台內[六pair半](../Page/六啤半.md "wikilink")13人組合其中一員。1980年在六啤半大碟中主唱著名的以[日本歌手](../Page/日本.md "wikilink")[五輪真弓改編的隽永歌曲](../Page/五輪真弓.md "wikilink")「為甚麼」、「泥路上」（同為[鄭國江填詞](../Page/鄭國江.md "wikilink")）和「甜甜廿四味」等並在1980年代初簽約[EMI與](../Page/EMI.md "wikilink")[飛利浦發行個人唱片曲集](../Page/飛利浦.md "wikilink")。1985年至1988年3月間在[商業二台主持](../Page/商業二台.md "wikilink")《月光光四人幫》晚間節目（與[楊振耀](../Page/楊振耀.md "wikilink")、[黃鐵雄](../Page/黃鐵雄.md "wikilink")、[張麗瑾](../Page/張麗瑾.md "wikilink")、[劉煥君](../Page/劉煥君.md "wikilink")、[陳輝虹](../Page/陳輝虹.md "wikilink")）

1992年她移民[加拿大](../Page/加拿大.md "wikilink")[溫哥華後加入當時的](../Page/溫哥華.md "wikilink")[温哥華中文電台即是現在的](../Page/温哥華中文電台.md "wikilink")[加拿大中文電台至今](../Page/加拿大中文電台.md "wikilink")。現為[加拿大中文電台高級副總裁](../Page/加拿大中文電台.md "wikilink")\[1\]。另外她曾在[美國](../Page/美國.md "wikilink")[洛杉磯](../Page/洛杉磯.md "wikilink")[KMRB中文電台](../Page/KMRB中文電台.md "wikilink")(AM1430)頻道主持周末節目《Music
Cafe》。雖然退出[香港樂壇](../Page/香港.md "wikilink")，但她亦不時回港參與懷舊金曲表演、基督教團體組織的音樂會節目如2006年9月底舉行之「關懷社區好歌音樂會\[2\]」。

現時盧業瑂以駐[溫哥華](../Page/溫哥華.md "wikilink")[加拿大中文電台主持人身份](../Page/加拿大中文電台.md "wikilink")，為[香港電台第一台每星期主持一天關於](../Page/香港電台.md "wikilink")[海外華人及](../Page/華僑.md "wikilink")[溫哥華近況的節目](../Page/溫哥華.md "wikilink")《[大城小事](../Page/大城小事_\(電台節目\).md "wikilink")》。
前香港[雷霆881唱片騎師](../Page/商業廣播有限公司.md "wikilink")

## 作品

### 現主持節目

  - 《Brenda點唱機》--[加拿大中文電台](../Page/加拿大中文電台.md "wikilink")[AM1470](../Page/AM1470.md "wikilink")（[温哥華](../Page/温哥華.md "wikilink")）
  - 《無瑂不至》--[加拿大中文電台](../Page/加拿大中文電台.md "wikilink")[FM96.1](../Page/AM1470.md "wikilink")（全[加拿大廣播](../Page/加拿大.md "wikilink")）
  - 《大城小事》--[香港電台第一台](../Page/香港電台.md "wikilink")（[香港](../Page/香港.md "wikilink")）

### 前主持節目

  - 日日好時光﹐朝早呢分鐘--[商業一台](../Page/雷霆881.md "wikilink")
    ([香港](../Page/香港.md "wikilink")) 1989-1993。
      - [楊美琪](../Page/楊美琪.md "wikilink")﹐[勞浩榮](../Page/勞浩榮.md "wikilink")﹐
        [劉婉芬](../Page/劉婉芬.md "wikilink")

### 曾主持節目

  - 《Music
    Cafe》--[美國](../Page/美國.md "wikilink")[洛杉磯KMRB中文電台AM](../Page/洛杉磯.md "wikilink")1430（[洛杉磯](../Page/洛杉磯.md "wikilink")）

### 電影

  - [喝采](../Page/喝采.md "wikilink")(1980)
  - [檸檬可樂](../Page/檸檬可樂_\(電影\).md "wikilink")(1982)
  - [我愛夜來香](../Page/我愛夜來香.md "wikilink")(1983)
  - [開心鬼](../Page/開心鬼.md "wikilink")(1984)
  - [壞女孩](../Page/壞女孩.md "wikilink")（1985）
  - [生命因愛動聽](../Page/生命因愛動聽.md "wikilink")(2001)

### 音樂專輯

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>專輯 #</p></th>
<th style="text-align: left;"><p>專輯名稱</p></th>
<th style="text-align: left;"><p>類型</p></th>
<th style="text-align: left;"><p>發行公司</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>1st</p></td>
<td style="text-align: left;"><p><a href="../Page/心思思.md" title="wikilink">心思思</a></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/百代唱片_(香港).md" title="wikilink">百代唱片 (香港)</a></p></td>
<td style="text-align: left;"><p>1981年</p></td>
<td style="text-align: left;"><ol>
<li>心思思</li>
<li>愛情最緊要</li>
<li>我全屬於你</li>
<li>從沒有的感覺</li>
<li>音樂盒</li>
<li>天使的翅膀</li>
<li>笑一聲</li>
<li>當天的你</li>
<li>浪踏浪</li>
<li>五線譜上的早晨</li>
<li>歸鄉</li>
<li>為什麼要騙自己</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>2nd</p></td>
<td style="text-align: left;"><p><a href="../Page/織個彩色夢.md" title="wikilink">織個彩色夢</a></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/百代唱片_(香港).md" title="wikilink">百代唱片 (香港)</a></p></td>
<td style="text-align: left;"><p>1982年</p></td>
<td style="text-align: left;"><ol>
<li>坭路上</li>
<li>織個彩色夢</li>
<li>臨別依依</li>
<li>年少</li>
<li>七彩世界</li>
<li>甜甜廿四味</li>
<li>朋友</li>
<li>披雨午夜行</li>
<li>我願高飛</li>
<li>寧願</li>
<li>無花果</li>
<li>夜半無人私語時</li>
<li>一束小菊花</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>3rd</p></td>
<td style="text-align: left;"><p><a href="../Page/我曾嚮往.md" title="wikilink">我曾嚮往</a></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/環球唱片.md" title="wikilink">環球唱片</a></p></td>
<td style="text-align: left;"><p>1985年</p></td>
<td style="text-align: left;"><ol>
<li>雲淚</li>
<li>California 我曾嚮往</li>
<li>童年密友</li>
<li>已不可鬆綁</li>
<li>疲倦再會</li>
<li>願</li>
<li>夢的剪影</li>
<li>被遺忘的人</li>
<li>悠悠清風</li>
<li>風</li>
<li>心</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>4th</p></td>
<td style="text-align: left;"><p><a href="../Page/點滴情懷.md" title="wikilink">點滴情懷</a></p></td>
<td style="text-align: left;"><p>EP</p></td>
<td style="text-align: left;"><p><a href="../Page/銀星.md" title="wikilink">銀星</a></p></td>
<td style="text-align: left;"><p>1988年9月</p></td>
<td style="text-align: left;"><ol>
<li>點滴情懷</li>
<li>全球傳聲新一代</li>
<li>Brenda點唱機</li>
<li>全心傳意</li>
</ol></td>
</tr>
</tbody>
</table>

### 單曲

  - 1980年：為甚麼（收錄於《[六啤半](../Page/六啤半_\(專輯\).md "wikilink")》專輯中）

## 參看

  - [加拿大中文電台](../Page/AM1470.md "wikilink")
  - [香港電台](../Page/香港電台.md "wikilink")

## 參考資料

## 外部連結

  - 個人簡介：加拿大中文電台[AM 1470](http://www.am1470.com/dj_detail.php?id=21)、[FM 96.1](http://www.am1470.com/dj_detail.php?id=63)、[KMRB
    AM1430](https://web.archive.org/web/20061013092425/http://www.am1430.net/dj/dj-lu.htm)

[盧](../Category/香港新教徒.md "wikilink")
[Category:华裔加拿大人](../Category/华裔加拿大人.md "wikilink")
[Category:大温华裔加拿大人](../Category/大温华裔加拿大人.md "wikilink")
[Category:温哥華人](../Category/温哥華人.md "wikilink")
[Category:香港人](../Category/香港人.md "wikilink")
[L](../Category/海外港人.md "wikilink")
[Category:香港女歌手](../Category/香港女歌手.md "wikilink")
[Category:香港廣播主持人](../Category/香港廣播主持人.md "wikilink")
[Category:英皇書院校友](../Category/英皇書院校友.md "wikilink")
[盧](../Category/香港中文大學校友.md "wikilink")
[Category:加拿大中文電台主持人](../Category/加拿大中文電台主持人.md "wikilink")
[Category:AM1470主持人](../Category/AM1470主持人.md "wikilink")
[Yip](../Category/盧姓.md "wikilink")

1.
2.