[Husky-Chibi.jpeg](https://zh.wikipedia.org/wiki/File:Husky-Chibi.jpeg "fig:Husky-Chibi.jpeg")
[Clyde_Rouge.png](https://zh.wikipedia.org/wiki/File:Clyde_Rouge.png "fig:Clyde_Rouge.png")
[Tail_Concerto_Game_Role_-_Panta.jpg](https://zh.wikipedia.org/wiki/File:Tail_Concerto_Game_Role_-_Panta.jpg "fig:Tail_Concerto_Game_Role_-_Panta.jpg")
**獸人正太**（日文：**ケモショタ**），是指與[正太擁有相同特質的](../Page/正太.md "wikilink")[獸人人物](../Page/獸人.md "wikilink")，被歸屬於獸人的大分類之下，[男性獸人的其中一種表現方式](../Page/男性.md "wikilink")。與其年齡相近但性別為[女性的獸人稱之為](../Page/女性.md "wikilink")「[獸人蘿莉](../Page/獸人蘿莉.md "wikilink")」。

## 詞源和由來

中文**獸人正太**一詞是非正式用語，取材自日文**ケモショタ**，結合**獸人**和**正太**兩個詞。英文的專門稱呼則叫「**Furry
Shota**」。

日文**ケモショタ**則源自**ケモノ**（「**毛物**」或「**獸**」，取[片假名突顯](../Page/片假名.md "wikilink")「**獸人**」之意，反而與直接的[平假名](../Page/平假名.md "wikilink")「じゅうじん」的核心意思有所出入）和**ショタ**兩詞。

與[正太一樣](../Page/正太.md "wikilink")，**獸人正太**同樣具有二次元文化中[萌的特性](../Page/萌.md "wikilink")，然而，因人對各種居家常飼養的動物（[寵物](../Page/寵物.md "wikilink")）的喜愛，他們有可能會被人類小孩更加「萌」和可愛。而受注目的元素已經不單單是對於少年、小童或弟弟的可愛或寵愛，隨著「毛茸」特性的附加，人們想去親身接觸的動機就會更大，使攻和受的兩方關係更加微妙。

另外，這標籤擁有的兩重特性，使其同時是[同人文化也是](../Page/同人.md "wikilink")[獸人文化中一種愛好的分支](../Page/獸人.md "wikilink")。\[1\]

## 表現方式

少年一般形象都較剛陽，故此獸部一般是以[獅](../Page/獅.md "wikilink")、[虎](../Page/虎.md "wikilink")、[狼](../Page/狼.md "wikilink")、[犬](../Page/犬.md "wikilink")、[龍等活潑好動的動物作代表](../Page/龍.md "wikilink")。但相對來說，「萌度」較高的會有[貓](../Page/貓.md "wikilink")、[兔](../Page/兔.md "wikilink")、[狐等](../Page/狐.md "wikilink")，這類偏向本來屬於[獸人蘿莉](../Page/獸人蘿莉.md "wikilink")（女性）居多的獸類型，令其「正太化」後其形態看來比較可愛，加上多數的創作設定中皆以弱者身份為角色，使他們擁有一份很強的吸引力和俱使別人憐惜的心。

從兩個方向簡單來說，就是：

  - 身體的「**一半**」變成了動物外型或特徵的人類[正太](../Page/正太.md "wikilink")（少年或兒童）。

<!-- end list -->

  - 身體的「**一半**」變成了人外型或特徵的[雄性幼小野獸](../Page/雄性.md "wikilink")。

當然，獸人的定義十分廣泛，兩者指向的也是同一形象。

## 各種作品中的設定

相對於擁有眾多二次元作品的獸人蘿莉系列，「獸人正太」的創作比較少見，尤其在日本地區，但在歐美地區的獸迷創作中則佔有極重要的一塊，絕大部分的獸人正太作品皆以插畫或是CG為主，漫畫與動畫類型較少，而不論日本或美國，相關的作品出版仍以個人網站刊登和[同人誌](../Page/同人誌.md "wikilink")（英語系國家稱同好誌）為主。

而近年來，受到歐美地區許多動漫以及[迪士尼卡通中](../Page/迪士尼.md "wikilink")「擬人化」動物作品的大量推出，已經有不少的[媒體作品開始使用獸人作為其主角或是角色設定中的一環](../Page/媒體.md "wikilink")，其中亦不乏全球知名的角色，下面列舉之。

  - 「**爆裂丸**」，出自日本動畫[十二生肖爆烈戰士](../Page/十二生肖爆烈戰士.md "wikilink")，十二生肖中的[鼠](../Page/鼠.md "wikilink")，[老鼠型的獸人正太](../Page/老鼠.md "wikilink")，也是其中的主角。
  - 「**庫羅諾亞**」，出自作品[風之少年](../Page/風之少年.md "wikilink")，原本是日本遊戲生產商[Namco的](../Page/Namco.md "wikilink")[吉祥物](../Page/吉祥物.md "wikilink")，後來自己發展成為一套遊戲，外型是[貓型獸人正太](../Page/貓.md "wikilink")。
  - 「**索尼克**」與「**塔爾斯、狄路**」，出自遊戲大廠[SEGA的](../Page/SEGA.md "wikilink")[音速小子系列](../Page/音速小子.md "wikilink")，前者是世界上最廣為人知的[刺蝟型獸人正太](../Page/刺蝟.md "wikilink")，後者是擁有兩條尾巴的[狐狸型獸人正太](../Page/狐狸.md "wikilink")。
  - 「**虎源太**」，出自日本的漫畫作品[陰陽大戰記](../Page/陰陽大戰記.md "wikilink")，之後被改編為動畫，是作品中一位[虎外型的獸人正太](../Page/虎.md "wikilink")。
  - 「**空**」，出自日本的[Broccoli企業為其角色精品店所做的角色企畫](../Page/Broccoli.md "wikilink")[Di
    Gi
    Charat](../Page/Di_Gi_Charat.md "wikilink")，作品中是[狗型的獸人正太](../Page/狗.md "wikilink")。
  - 「**犬上小太郎**」，出自日本漫畫作品[魔法老師](../Page/魔法老師.md "wikilink")，以[犬作為外型設定的獸人少年](../Page/犬.md "wikilink")，作品中是來自關西地區的狗族人。
  - 「**櫻太郎**」，出自作品[海貓悲鳴時](../Page/海貓悲鳴時.md "wikilink")，是日本[同人社團](../Page/同人社團.md "wikilink")[07th
    Expansion所製作的](../Page/07th_Expansion.md "wikilink")[同人遊戲以及由此改編的漫畫與動畫](../Page/同人遊戲.md "wikilink")，獅子外型的獸人正太。
  - 「**潘達君**」，出自日本遊戲[貓犬協奏曲](../Page/貓犬協奏曲.md "wikilink")，極少數使用獸人作為遊戲種族以及世界觀的作品，是主角於警察局的同事兼學弟兼後勤支援。

## 相關條目

  - [獸耳](../Page/獸耳.md "wikilink")
  - [貓耳](../Page/貓耳.md "wikilink")
  - [日本動漫迷使用術語列表](../Page/日本動漫迷使用術語列表.md "wikilink")
  - [女性獸人](../Page/女性獸人.md "wikilink")
  - [蒼空騎士～飛向CODA～](../Page/蒼空騎士～飛向CODA～.md "wikilink")
  - [DOG DAYS](../Page/DOG_DAYS.md "wikilink")

## 註解

## 補充資料

  - \[<https://web.archive.org/web/20130118034037/http://zh.wikifur.com/wiki/%E9%A6%96%E9%A1%B5>　WikiFur
    獸人愛好者百科全書\]
  - \[<http://dic.pixiv.net/a/%E3%82%B1%E3%83%A2%E3%83%8E>　ケモノ(けもの)とは【ピクシブ百科事典】\]
  - \[<https://web.archive.org/web/20121214094750/http://dic.pixiv.net/a/%E7%8D%A3%E4%BA%BA>　獣人(じゅうじん)とは【ピクシブ百科事典】\]
  - \[<http://dic.pixiv.net/a/%E3%82%B1%E3%83%A2%E3%82%B7%E3%83%A7%E3%82%BF>　ケモショタ
    (けもしょた)とは【ピクシブ百科事典】\]

[ja:ケモショタ](../Page/ja:ケモショタ.md "wikilink")

[Category:次文化](../Category/次文化.md "wikilink")
[Category:萌屬性](../Category/萌屬性.md "wikilink")
[Category:兽人迷](../Category/兽人迷.md "wikilink")

1.  \[<http://dic.pixiv.net/a/%E3%82%B1%E3%83%A2%E3%82%B7%E3%83%A7%E3%82%BF>　ケモショタ(けもしょた)とは【ピクシブ百科事典】\]