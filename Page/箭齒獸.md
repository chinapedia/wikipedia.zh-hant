**箭齒獸**（*Toxodon*）是[南方有蹄目中已](../Page/南方有蹄目.md "wikilink")[滅絕的一](../Page/滅絕.md "wikilink")[屬](../Page/屬.md "wikilink")，[生態上與](../Page/生態.md "wikilink")[河馬相似](../Page/河馬.md "wikilink")，生存於[上新世至](../Page/上新世.md "wikilink")[更新世的](../Page/更新世.md "wikilink")[南美洲](../Page/南美洲.md "wikilink")。箭齒獸與一些南蹄目經歷了[南北美洲生物大遷徙](../Page/南北美洲生物大遷徙.md "wikilink")，相信因更新世的[氣候轉變而滅絕](../Page/氣候.md "wikilink")。[查爾斯·達爾文](../Page/查爾斯·達爾文.md "wikilink")（Charles
Darwin）在《[小獵犬號航行之旅](../Page/小獵犬號航行之旅.md "wikilink")》中首先研究了牠的[化石](../Page/化石.md "wikilink")。有指箭齒獸的生活模式有點似南美洲的河馬，有可能被[一般劍齒虎所掠食](../Page/一般劍齒虎.md "wikilink")。

[Toxodon.jpg](https://zh.wikipedia.org/wiki/File:Toxodon.jpg "fig:Toxodon.jpg")
箭齒獸中的*T.
platensis*是一種體型很大的[草食性動物](../Page/草食性.md "wikilink")，長約2.75米及高1.5米，外觀像[犀牛](../Page/犀牛.md "wikilink")。

## 外部連結

  - [簡介*Toxodon
    platensis*及其頭顱骨化石](https://web.archive.org/web/20080318144449/http://www.toyen.uio.no/palmus/galleri/montre/english/a31962_63.htm)

[Category:箭齒獸科](../Category/箭齒獸科.md "wikilink")
[Category:上新世哺乳类](../Category/上新世哺乳类.md "wikilink")
[Category:更新世哺乳类](../Category/更新世哺乳类.md "wikilink")