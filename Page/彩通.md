**彩通**（，又譯**潘通**\[1\]）總部位於[美國](../Page/美國.md "wikilink")[新澤西州卡爾士達特市](../Page/新澤西州.md "wikilink")（），是一家專門開發和研究色彩而聞名全球的權威機構，也是色彩系統的供應商，提供許多行業包括[印刷及其他關于顏色如](../Page/印刷.md "wikilink")[數碼技術](../Page/數碼技術.md "wikilink")、[紡織](../Page/紡織.md "wikilink")、[塑膠](../Page/塑膠.md "wikilink")、[建築以及](../Page/建築.md "wikilink")[室內設計等的專業色彩選擇和精確的交流語言](../Page/室內設計.md "wikilink")。

彩通於1962年由現任公司董事長、總裁兼執行長劳伦斯·赫伯特（）所收購，當時只是為[化妝品公司生產顏色卡的小公司](../Page/化妝品.md "wikilink")。赫伯特於1963年推出第一本的「彩通配色系統」色標。

2007年10月，色彩管理與設備公司以1.8億美元收購彩通為全資子公司\[2\]。

## 彩通配色系統

「彩通配色系統®」（，簡稱）是選擇、確定、配對和控制油墨色彩方面的權威性國際參照標準。其中的「彩通配方指南」由大量每頁約6×2[吋或](../Page/吋.md "wikilink")15×5[公分的薄](../Page/公分.md "wikilink")-{zh-hans:卡;zh-hk:咭;zh-sg:卡;zh-tw:卡;}-紙，單面印有一系列相關的[顏色](../Page/顏色.md "wikilink")、彩通顏色編號及其混色配方，裝訂成一本長條形可翻開成[扇形的小冊子](../Page/扇形.md "wikilink")，因[油墨會隨時間或經常翻閱而退色](../Page/油墨.md "wikilink")，所以彩通每年更新配色系統的版本。2005年新版本分為三冊分別印於[-{zh-hans:光面铜版纸;
zh-hk:光粉紙;}-](../Page/光面铜版纸.md "wikilink")、[-{zh-hans:胶版纸;
zh-hk:書紙;}-和](../Page/胶版纸.md "wikilink")[-{zh-hans:哑面铜版纸;
zh-hk:啞粉紙;}-上](../Page/哑面铜版纸.md "wikilink")，共有1,114種顏色。

當[平面設計師從](../Page/平面設計.md "wikilink")「彩通配色系統」中挑選了一個指定顏色（舉例深藍）交印刷廠付印，印刷廠需按本身設備及油墨調製出產品與指定顏色一致的顏色，當然設計師與印刷廠需採用相同年份[版本的](../Page/版本.md "wikilink")「彩通配色系統」，因不同版本相同編號的顏色會有輕微的差別。

人們常誤以為「彩通」是[色彩管理系統](../Page/色彩管理.md "wikilink")，大部分家用或辦公室的彩色[印表機是採用](../Page/印表機.md "wikilink")[CMYK系統](../Page/印刷四分色模式.md "wikilink")，而「彩通」則為油墨混色配方，在印刷行業通稱為[專色](../Page/專色.md "wikilink")，所以「彩通」不屬於[RGB](../Page/三原色光模式.md "wikilink")（螢幕顏色）或[CMYK](../Page/印刷四分色模式.md "wikilink")（四色印刷）的[色域](../Page/色域.md "wikilink")，而是用於延伸更多可供印刷的顏色（包括及[夜光色](../Page/夜光油墨.md "wikilink")）。

「彩通配色系統」因應不同的用途而發展出不同的產品，較出名的包括：

### 平面設計

1.  彩通®配方指南
2.  彩通®專色色票
3.  彩通®色階
4.  彩通®金屬色配方指南
5.  彩通®粉彩色配方指南
6.  彩通®四色疊印指南套裝

### 服裝和家居

1.  彩通®服裝和家居色彩手冊 - 棉布版
2.  彩通®服裝和家居色彩指南

### 塑膠

1.  彩通®塑膠不透明色與透明色選色手冊
2.  彩通®塑膠選色片

## 日常使用

[Flag_of_Scotland.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Scotland.svg "fig:Flag_of_Scotland.svg")
彩通顏色以編號指示顏色更用於法律指定用途，特別是用於指定[旗幟的顏色](../Page/旗幟.md "wikilink")。2003年1月[蘇格蘭](../Page/蘇格蘭.md "wikilink")[議會辯論編號](../Page/議會.md "wikilink")的議案是訂定蘇格蘭X字形旗的底色為PMS300藍色。而[加拿大和](../Page/加拿大.md "wikilink")[南韓亦以彩通顏色作為生產國旗時的指引](../Page/南韓.md "wikilink")。

彩通聲稱擁有其彩通顏色編號及配方的[知識產權](../Page/知識產權.md "wikilink")，自由取用是不被允許的。所以彩通顏色並不受[开放源代码的](../Page/开放源代码.md "wikilink")[軟體如](../Page/軟體.md "wikilink")[GIMP支援](../Page/GIMP.md "wikilink")，在其他低檔的軟體亦不常見。但仍有印刷廠印製彩通顏色色標免費送給客戶。彩通亦宣佈擁有其開發的高保真六色色彩系統®（）的專利權。

其他常用的顏色標示還有由[瑞典開發的](../Page/瑞典.md "wikilink")[NCS色票](../Page/NCS色票.md "wikilink")（）、[德國的](../Page/德國.md "wikilink")[RAL色票](../Page/RAL色票.md "wikilink")（）及[日本的](../Page/日本.md "wikilink")[DIC色票](../Page/DIC色票.md "wikilink")（大日本油墨化工）等。

## 參見

  - [四色](../Page/印刷四分色模式.md "wikilink")
  - [專色](../Page/專色_\(印刷\).md "wikilink")

## 參考文獻

## 外部連結

  - [Pantone官方网站](https://www.pantone.com)

  - [彩通中国大陆官方网站](https://www.pantonecn.com/)

  - [彩通香港官方网站](https://store.pantone.com/hk/tc/)

  -
  -
  -
  -
{{-}}

[P](../Page/category:印刷.md "wikilink")

[Category:美国公司](../Category/美国公司.md "wikilink")
[P](../Category/颜色.md "wikilink") [P](../Category/设计.md "wikilink")

1.
2.  .