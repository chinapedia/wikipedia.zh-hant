**比兰**（Birán）是位于[古巴东部](../Page/古巴.md "wikilink")[奥尔金省](../Page/奥尔金省.md "wikilink")[马亚里](../Page/马亚里.md "wikilink")（Mayarí）区内的一个小镇，由于[菲德尔·卡斯特罗及](../Page/菲德尔·卡斯特罗.md "wikilink")[劳尔·卡斯特罗分别于](../Page/劳尔·卡斯特罗.md "wikilink")1926年和1931年在此出生而闻名。卡斯特罗的父亲在那里拥有一座93平方公里的种植园。

[Category:古巴城市](../Category/古巴城市.md "wikilink")