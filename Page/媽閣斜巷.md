[Calcada_da_barra.JPG](https://zh.wikipedia.org/wiki/File:Calcada_da_barra.JPG "fig:Calcada_da_barra.JPG")
**媽閣斜巷**（又稱：**萬里長城**；）是一條位於[澳門西南面的街道](../Page/澳門.md "wikilink")，由[媽閣街起始至](../Page/媽閣街.md "wikilink")[媽閣廟前地為止](../Page/媽閣廟前地.md "wikilink")，屬於[風順堂區](../Page/風順堂區.md "wikilink")。

## 歷史

媽閣斜巷歷史悠久。早於19世紀末，萬里長城一名已被列入《澳門海外省街道名冊》之內，從屬於當時的[媽閣村轄下](../Page/媽閣村.md "wikilink")。媽閣斜巷之所以被稱為萬里長城有兩種說法，其一源於其斜巷的長度相當長，有如澳門的[萬里長城般](../Page/萬里長城.md "wikilink")，故曰其名。（事實上，萬里長城一名早於百多年前已被一般澳門市民所傳述，於當時而言，媽閣斜巷確為一條遙長的街道，但時至今日，有不少街道比它還要長很多，如[俾利喇街](../Page/俾利喇街.md "wikilink")。）

另一說法則由於百多年前其巷沿途並未建有任何高層樓宇，隱隱可見[澳門舊城的城牆遺跡](../Page/澳門舊城.md "wikilink")，並順延至[媽閣廟旁](../Page/媽閣廟.md "wikilink")，故被當時人們稱為萬里長城。關於澳門舊城的城牆，是[葡萄牙人於](../Page/葡萄牙人.md "wikilink")[明](../Page/明朝.md "wikilink")[天啟年間所修築的](../Page/天啟.md "wikilink")，基本上分為[南](../Page/南.md "wikilink")[北兩面](../Page/北.md "wikilink")，其中位於媽閣斜巷的相信屬於南面城牆西翼，延伸至北面媽閣廟[海鏡石的一段](../Page/海鏡石.md "wikilink")，城牆早於18世紀中葉被拆毁，而該段城牆的痕跡已難復尋。

另外，由於萬里長城一名的普及，媽閣斜巷的街道牌上亦括有萬里長城一名，位於該地亦有樓宇名為「長城大廈」。[澳門新福利公共汽車有限公司之](../Page/澳門新福利公共汽車有限公司.md "wikilink")[28B路線的](../Page/新福利28B路線.md "wikilink")[路線字軌膠牌亦使用萬里長城之名](../Page/路線字軌膠牌.md "wikilink")，此舉肯定了萬里長城之名在市民心中的地位。

## 特色建築

[Macauchenghouse.jpg](https://zh.wikipedia.org/wiki/File:Macauchenghouse.jpg "fig:Macauchenghouse.jpg")
[Quartel_dos_Mouros_.JPG](https://zh.wikipedia.org/wiki/File:Quartel_dos_Mouros_.JPG "fig:Quartel_dos_Mouros_.JPG")
媽閣斜巷屬於昔日的「基督城」街區的其中一部分，附近有[鄭觀應於澳門的故居](../Page/鄭觀應.md "wikilink")[鄭家大屋](../Page/鄭家大屋.md "wikilink")，鄭觀應是[中國近代著名的](../Page/中國.md "wikilink")[文學家及](../Page/文學家.md "wikilink")[思想家](../Page/思想家.md "wikilink")，著名作有《[盛世危言](../Page/盛世危言.md "wikilink")》一書，當年鄭觀應因公務屢遭挫折，身心俱疲，故退居澳門，並潛心於此地著成《盛世危言》一書，後來他離開澳門轉赴[上海專注教育事業](../Page/上海.md "wikilink")，於1922年逝世。鄭家大屋於1950年代曾分租，業權曾多次易手，亦曾荒廢一時，屋內的一些文物亦成為賊人的目標，其後[澳門特區政府則於](../Page/澳門特區政府.md "wikilink")2001年成功接收了鄭家大屋的業權，重新整修以作保護。

另外正位於媽閣斜巷的尚有[港務局大樓](../Page/港務局大樓.md "wikilink")，此一大樓建成於1874年，亦被稱為「嚤囉兵營」或「水師廠」，原為澳門[印度籍](../Page/印度.md "wikilink")[警察的營地](../Page/警察.md "wikilink")，後來改作[港務局和澳門水警稽查隊的辦公地點](../Page/港務局.md "wikilink")。建築風格屬於歐洲式與阿拉伯式建築的融合，既有典型的葡式建築風格，亦有如穆斯林式的穹頂建築。現時港務局的辦公地點已遷往[林茂塘的海港樓](../Page/林茂塘.md "wikilink")，但港務局大樓以及鄭家大屋則被納入[澳門歷史城區的一部分而成為](../Page/澳門歷史城區.md "wikilink")[世界文化遺產之一](../Page/世界文化遺產.md "wikilink")。

現時的媽閣斜巷並不算相當繁華的街道，街道四周多為住宅，但亦有一些遊客慕名而來於港務局大樓拍照留念。

## 參考資源

  - 《澳門古今》，[李鵬翥著](../Page/李鵬翥.md "wikilink")，三聯書店(香港)有限公司出版，2001。
  - 《澳門歷史建築的故事》，澳門培道中學歷史學會，2004。 ISBN 9993771066
  - 《澳門街道的故事》，澳門培道中學歷史學會，2005。 ISBN 9993784001
  - [新福利巴士路線28B](http://www.i-busnet.com/macau/busroute/r28b.php)，巴士資訊網

## 外部連結

  - [澳門百科全書:萬里長城](https://web.archive.org/web/20050222095720/http://www.macaudata.com/macauweb/Encyclopedia/html/36306.htm)

## 相關條目

  - [港務局大樓](../Page/港務局大樓.md "wikilink")
  - [鄭家大屋](../Page/鄭家大屋.md "wikilink")
  - [媽閣廟](../Page/媽閣廟.md "wikilink")

[Category:澳門街道](../Category/澳門街道.md "wikilink")