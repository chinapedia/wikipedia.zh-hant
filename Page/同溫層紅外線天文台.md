{{
Redirect2|SOFIA|[保加利亞的](../Page/保加利亞.md "wikilink")[首都](../Page/首都.md "wikilink")|索菲亞}}
[SOFIA_with_open_telescope_doors.jpg](https://zh.wikipedia.org/wiki/File:SOFIA_with_open_telescope_doors.jpg "fig:SOFIA_with_open_telescope_doors.jpg")
[SOFIA_09.25.06.1247.jpg](https://zh.wikipedia.org/wiki/File:SOFIA_09.25.06.1247.jpg "fig:SOFIA_09.25.06.1247.jpg")

**同溫層紅外線天文台**
（）是[NASA](../Page/NASA.md "wikilink")、[德國航空太空中心](../Page/德國航空太空中心.md "wikilink")（）和[大學太空研究協會](../Page/大學太空研究協會.md "wikilink")（URSA）共同合作，在1996年由NASA授權優先發展的計畫。

## 簡介

SOFIA是由一架曾在[泛美航空和](../Page/泛美航空.md "wikilink")[聯合航空的商業航線上服務過的](../Page/聯合航空.md "wikilink")[波音747SP](../Page/波音747SP.md "wikilink")
改裝上2.5 米直徑[反射望遠鏡](../Page/反射望遠鏡.md "wikilink")，在41,000英尺（約12,496公尺）的高空進行[紅外線天文學研究的飛機](../Page/紅外線天文學.md "wikilink")。他的能力使它幾乎能在地球大氣層內所有的[水蒸氣之上飛行](../Page/水蒸氣.md "wikilink")（能夠觀察到一些被大氣層攔阻不能被地基設備觀察到的紅外波長），並且飛行至地球表面上的任何一點進行觀測。從外面看，這架望遠鏡像是在[機身的機尾附近的大門](../Page/機身.md "wikilink")，初期將裝置九套觀測儀器，進行在紅外線天文學[波長](../Page/波長.md "wikilink")1-655[微米](../Page/微米.md "wikilink")，和在波長0.3制1.1微米的高速[光學天文學觀測](../Page/光學天文學.md "wikilink")。

一旦可以使用，希望能在未來的20年間，每週能進行3或4晚的飛行觀測。SOFIA的基地是設在加州[藍開斯特](../Page/藍開斯特.md "wikilink")（Lancaster）附近[艾德華空軍基地內的NASA](../Page/艾德華空軍基地.md "wikilink")[德來登飛行研究中心](../Page/德來登飛行研究中心.md "wikilink")（Dryden
Flight Research Center）。.

## 望遠鏡

SOFIA是2.5米的反射望遠鏡，但使用的鏡片是與多數的紅外望遠鏡相同，超大的2.7米的主鏡。雖然SOFIA顯然是安置在飛機上最大的望遠鏡，但相較於地基研究用的望遠鏡，它仍然只是中型的尺寸。

這個計畫在[莫菲特聯邦機場](../Page/莫菲特聯邦機場.md "wikilink")（Moffett Federal
Airfield）有自己的鍍膜設備，可以快速的重新鍍鏡。

DLR負責整架望遠鏡的組裝（NASA負責飛機），望遠鏡的製造被轉包給歐洲的產業界（德國製造望遠鏡，[法國製造主鏡](../Page/法國.md "wikilink")，副鏡則是[瑞士製造](../Page/瑞士.md "wikilink")）。

每一次的任務都可以更換與望遠鏡連結的科學儀器。有兩組是一般目的通用的儀器，另外也能未特別的研究設計和建造專用的儀器。

望遠鏡將安置在一個天窗內，暴露於高速動盪的風中；另一方面，飛機的振動和運動也都會增加觀測上的困難。在飛機起飛之前必須先預冷望遠鏡所在的貨艙，以便能與外面的溫度相同（避免溫度造成的變形）。為了避免濕氣和結露，在冷卻之前貨艙還必須灌滿[氮氣](../Page/氮氣.md "wikilink")\[1\]。

## 飛機

[SOFIA_in_air.jpg](https://zh.wikipedia.org/wiki/File:SOFIA_in_air.jpg "fig:SOFIA_in_air.jpg")
[Boeing_747SP-21,_Pan_American_World_Airways_-_Pan_Am_AN1132071.jpg](https://zh.wikipedia.org/wiki/File:Boeing_747SP-21,_Pan_American_World_Airways_-_Pan_Am_AN1132071.jpg "fig:Boeing_747SP-21,_Pan_American_World_Airways_-_Pan_Am_AN1132071.jpg")\]\]
SOFIA使用的飛機是一架波音[747SP](../Page/波音747SP.md "wikilink")（出廠編號21441/306）。它最初在1977年5月交付給泛美航空\[2\]。聯合航空在1986年2月購買了這架飛機，並在1995年將之除役。兩年後，NASA從聯合航空購買了這架飛機給SOFIA計畫使用。為了進行天文學觀測的飛行，從1997年起由位於[德州](../Page/德克萨斯州.md "wikilink")[威克市](../Page/韋科_\(德克薩斯州\).md "wikilink")（Waco）的[L-3通信整合系統公司](../Page/L-3通信.md "wikilink")（L-3
Communications Integrated
Systems）進行了一系列基礎性的試飛。從另一架747SP取下的部分被用於測試[全尺寸模型](../Page/全尺寸模型.md "wikilink")，以確保設備的實用性。

泛美航空當初將這架飛機命名為「林白快帆號」（Clipper
Lindbergh），以尊崇有名的飛行員[查爾斯·林白](../Page/查爾斯·林白.md "wikilink")（Charles
Augustus
Lindbergh），並在林白於1927年由[巴黎飛抵](../Page/巴黎.md "wikilink")[紐約的](../Page/紐約.md "wikilink")50周年紀念慶典上，由其遺孀[安妮·莫里·林白](../Page/安妮·莫里·林白.md "wikilink")（Anne
Morrow
Lindbergh）親自主持命名儀式。雖然這架飛機在1977年5月6日已經開始服務，但在2007年5月21日，還是由林白的[孫子艾里](../Page/孫子.md "wikilink")·克林白，在德州威科市L-3的機房內再度恢復原來的名稱
－ 林白快帆號。

SOFIA的處女航是2007年4月26日，在威科市進行了簡要的測試程序和部份飛行狀態與基礎的維護檢查之後，這架飛機在2007年5月31日被移交給[艾德華空軍基地](../Page/艾德華空軍基地.md "wikilink")，繼續後續主要的測試飛行。在測試完成之後，SOFIA將在加州聖荷西[矽谷附近](../Page/矽谷.md "wikilink")，莫菲特機場的NASA[埃姆斯研究中心](../Page/埃姆斯研究中心.md "wikilink")（Ames
Research Center），由[美國長青航空](../Page/美國長青航空.md "wikilink")（Evergreen
International Aviation, Inc.）執行維修和操作\[3\]。

## 計畫展望

SOFIA在2004年8月18日至19日進行了在地面上的測試，觀察了[北極星的影像](../Page/北極星.md "wikilink")。在2006年2月，因為計劃的延宕，預算需由1億8500萬調整為3億3000萬美金\[4\]，於是NASA將計劃改列成"待重審"，並且凍結了預算。2006年6月15日，
NASA認為沒有不能克服的技術或綱會挑戰到SOFIA的後續發展，於是通過了重審\[5\]\[6\]。

SOFIA的第一次試飛在2007年4月26日由L-3 Integrated
Systems公司在美國德州的威科市完成\[7\]，來自威科市蒙特棱利學校的小學生是第一批看見它的學生。SOFIA目前仍在NASA的德賴登飛行研究中心接受裝載和試飛。

## 科學研究

SOFIA的主要科學目標是研究[行星的表面和](../Page/行星.md "wikilink")[大氣層組成](../Page/大氣層.md "wikilink")；研究[彗星的組成](../Page/彗星.md "wikilink")、演化和結構；以及探討[星際物質的](../Page/星際物質.md "wikilink")[物理和](../Page/物理.md "wikilink")[化學性質](../Page/化學.md "wikilink")、探索[恆星和其它天體的形成](../Page/恆星.md "wikilink")。SOFIA這架飛機是由NASA位於加州德來登山景中的艾美斯研究中心管理，它的家稱為SOFIA科學中心。

## 參見

  - [古柏機載天文台](../Page/柯伊伯機載天文台.md "wikilink")
  - [紅外線天文學](../Page/紅外線天文學.md "wikilink")
  - [遠紅外線天文學](../Page/遠紅外線天文學.md "wikilink")

## 參考資料

## 外部連結

  - [USRA SOFIA website](http://www.sofia.usra.edu/)
  - [SOFIA Mission
    Profile](https://web.archive.org/web/20070802045045/http://solarsystem.nasa.gov/missions/profile.cfm?MCode=SOFIA)
    by [NASA's Solar System Exploration](http://solarsystem.nasa.gov)
  - [SOFIA Website des
    DLR](https://web.archive.org/web/20060616022217/http://solarsystem.dlr.de/Missions/SOFIA/)
    (de)
  - [SOFIA on Astronomy Picture of the
    Day](http://antwrp.gsfc.nasa.gov/apod/ap060225.html)
  - [L-3 Communications Integrated Systems](http://www.l-3com.com/)
  - [ground-based "on-sky"
    test](https://web.archive.org/web/20061012152127/http://www.sofia.usra.edu/News/news_2004/09-09-04_Observatory_Sees_Stars.html)
  - [Evergreen International
    Airlines](http://www.evergreenairlines.com/EIA/index.html)
  - [Airliners.net](http://www.airliners.net/search/photo.search?cnsearch=21441/306)
    此機在泛美及聯合時期的照片

[Category:波音747](../Category/波音747.md "wikilink")
[Category:天文觀測](../Category/天文觀測.md "wikilink")
[Category:望遠鏡](../Category/望遠鏡.md "wikilink")
[Category:美国航空航天局项目](../Category/美国航空航天局项目.md "wikilink")

1.
2.
3.
4.
5.  。 NASA Headquarters press release 06-240
6.
7.