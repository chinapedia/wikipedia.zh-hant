**鐵道擬人化**，是一種把[鐵道車輛或鐵道相關事物](../Page/鐵道車輛.md "wikilink")（如車站等）以[擬人化方式演繹成人型](../Page/擬人化.md "wikilink")「[角色](../Page/角色.md "wikilink")」的藝術概念／手法，屬於[二次創作眾多手法之一](../Page/二次創作.md "wikilink")。

**鐵道擬人化**又按性質分為不同類型的擬人化手法。按目標原形態分類，最常見的有譬如「**鐵道車輛擬人化**」、「**鐵道車站擬人化**」和「**[鐵道路線擬人化](../Page/鐵道路線擬人化.md "wikilink")**」；按程度分類，則有分為「直接擬人化」（基本擬人化，大致保留了目標原形態的外形、線條和各種細節比喻等）、「間接擬人化」（如：[萌擬人化](../Page/萌擬人化.md "wikilink")）或上述兩者之間任何程度。

## 兒童文學上的鐵道擬人化圖像

## 鐵道雜誌上的鐵道擬人化圖像

## 御宅族文化與鐵道擬人化

### 御宅族調鐵道擬人化的歷史

#### 上海地鐵擬人化

上海地鐵擬人化屬於鐵道路線擬人化，為網友自發創作。擬人化后上海的13條地鐵線路的關係為兄弟姐妹。1號線是一位穿紅色衣服的16歲男孩，性格成熟穩重；2號線是一位帶著眼鏡、喜歡琢磨高科技的酷酷的少年；而排行第6、第7、第11和第13的則都還是懵懵懂懂的小妹妹，一家子其樂融融……這戶人家的家長名叫“上鐵先生”。[链接标签](http://news.sina.com.hk/news/9/1/1/1995354/1.html)擬人化的軌交形象在網絡上深入人心，並得到了相關部門的支持。上海地鐵擬人化的形象受到日本動漫的影響，同時結合該線路運行的特點和乘客的特徵。

## 「朱鷺325號」

2004年10月23日17時56分，一列在[日本](../Page/日本.md "wikilink")至區間中行駛中的[上越新幹線列車](../Page/上越新幹線.md "wikilink")（車次為「とき325號」〔即「[朱鷺](../Page/朱鷺.md "wikilink")325號」〕，車款為[200系翻新版](../Page/新幹線200系電聯車.md "wikilink")，編成序號為K25）遇上[新潟縣](../Page/新潟縣.md "wikilink")[中越地方發生](../Page/中越.md "wikilink")[黎克特制](../Page/黎克特制.md "wikilink")6.9級[地震引致脱線](../Page/地震.md "wikilink")──這是自1964年日本新幹線系統啟用以來首次有新幹線列車在營業運行時間脱線（詳見[上越新幹線出軌事故](../Page/上越新幹線出軌事故.md "wikilink")）。肇事列車就在[二頻道網站的](../Page/2ch.md "wikilink")「臨時地震板」被[二次創作成為](../Page/二次創作.md "wikilink")**鐵道擬人化**角色「とき娘」（朱鷺娘），並把脫線事故給[擬人化演繹](../Page/擬人化.md "wikilink")。

## 港鐵人物誌

[香港鐵路官方於](../Page/香港鐵路.md "wikilink")2009年末推出了一系列鐵道車站擬人化的角色，方法是把部份[香港鐵路車站的歷史](../Page/香港鐵路.md "wikilink")、背景、使用情況等參數隱喻於一些自創形象角色的服裝、性格、成份背景之設定當中，屬於間接式的鐵道車站擬人化手法\[1\]。

## 電光快車俠

[日本動畫作品](../Page/日本動畫.md "wikilink")《[電光快車俠](../Page/電光快車俠.md "wikilink")》中，將所有的[新幹線火車頭擬人化](../Page/新幹線.md "wikilink")，成為故事中最亮眼的主要角色群；另外與主要角色對立的反派人物則以舊式的[蒸氣火車加以擬人化](../Page/蒸氣火車.md "wikilink")，與新穎的新幹線火車頭作為強烈對比。

## 奇蹟列車

## 相關項目

  - [萌擬人化](../Page/萌擬人化.md "wikilink")
  - [天朝铁道少女](../Page/天朝铁道少女.md "wikilink")
  - 「[Fastech娘](../Page/FASTECH_TAN.md "wikilink")」（, Fastech-tan）
  - 「[K-Train公主](../Page/K-Train公主.md "wikilink")」（Princess K-Train）
  - 「[KTT公主](../Page/KTT公主.md "wikilink")」（Princess
    KTT），又稱《九廣通公主》，以[九廣鐵路Ktt客車](../Page/九鐵Ktt客車.md "wikilink")（「九廣通」雙層廣九直通車）為藍圖設計的[鐵道車輛擬人化角色](../Page/鐵道車輛擬人化.md "wikilink")。\[2\]\[3\]\[4\]\[5\]
  - [香港](../Page/香港.md "wikilink")[鐵道娘](../Page/鐵道擬人化.md "wikilink")「[千九公主](../Page/千九公主.md "wikilink")」（[Princess
    SP1900](../Page/Princess_SP1900.md "wikilink")），以[九廣鐵路SP1900型電動列車](../Page/九鐵SP1900型電聯車.md "wikilink")（俗稱「千九」）為藍圖設計的[鐵道車輛擬人化角色](../Page/鐵道車輛擬人化.md "wikilink")。\[6\]\[7\]\[8\]\[9\]\[10\]\[11\]\[12\]

## 參考資料

<div class="references-small">

<references/>

</div>

## 外部連結

  - [飛行京急娘](http://kqtan.run.buttobi.net/index.html)
  - [京急娘　鐵系擬人化車庫](http://www.t-square.jp/keikyu-tan/index.html)
  - [みふぁやど](https://web.archive.org/web/20080421092429/http://icoca.hp.infoseek.co.jp/index.html)
  - [鐵系擬人化工房](http://www7a.biglobe.ne.jp/~keikyu-tan/index2.html)
  - [Rail-G Station](http://rail-g.net/index.shtml) - 「Rail-G」網站
  - [愛☆鐵道娘【櫻月亭・香港】](http://www.ayakado.com/)
  - [京急娘角色扮演衣裝](https://web.archive.org/web/20080418045837/http://mia.shop-pro.jp/?pid=3017058)
  - [Fastech娘角色扮演衣裝](https://web.archive.org/web/20120723103432/http://www.rakuten.co.jp/miacos/858406/842575/803346/)
  - [朱鷺娘給修復之繪圖大全](https://web.archive.org/web/20080420203448/http://ueno.cool.ne.jp/toki325/index.html)
  - [二頻道網站巨大掲示板](http://www.2ch.net/index.html)

[Category:鐵路迷](../Category/鐵路迷.md "wikilink")
[Category:鐵道文化](../Category/鐵道文化.md "wikilink")
[Category:萌擬人化](../Category/萌擬人化.md "wikilink")

1.  [港鐵人物誌](http://www.mtr.com.hk/mtrclub/club_fun_corner.php?p=story)
2.  [《軌角九號》海報一，其中在右下方擁抱著的兩人，左方為千九公主，右方為KTT公主](http://kcr.sohome.info/images/RailNo9-Poster1.jpg)；摘自[《軌角九號》海報集](http://forumhk.proplux.info/viewthread.php?tid=750)
3.  [《軌角九號》海報四，其中七人，最左方為千九公主，最右方為KTT公主](http://kcr.sohome.info/images/RailNo9-Poster4A.jpg)；摘自[《軌角九號》海報集](http://forumhk.proplux.info/viewthread.php?tid=750)
4.  [拍攝《軌角九號》所屬之私影行動「KCR Forever」的角色及劇本編排](http://kcr.sohome.info)
5.  [改編港鐵海報：九廣通公主](http://www.sohome.info/sohome/PRKTT-Scrabble.jpg)；摘自
    "[Imitations of MTR
    Scrabble](http://www.sohome.info/sohome/Scrabbles.html)"
6.  [千九公主官方網站](http://princesssp1900.sohome.info/)
7.  [千九公主實體化扮演照片集（2008年4月27日．澳門）](http://www.chinacos.net/bbs/dispbbs.asp?boardID=2&ID=10995)

8.
9.  [改編港鐵海報：千九公主（二）](http://www.sohome.info/sohome/PSP1900-Scrabble.jpg)；摘自
    "[Imitations of MTR
    Scrabble](http://www.sohome.info/sohome/Scrabbles.html)"
10. [「軌角九號」海報一，其中在右下方擁抱著的兩人，左方為千九公主，右方為KTT公主](http://kcr.sohome.info/images/RailNo9-Poster1.jpg)
    ；摘自[「軌角九號」海報集](http://forumhk.proplux.info/viewthread.php?tid=750)
11. [「軌角九號」海報四，其中七人，最左方為千九公主，最右方為KTT公主](http://kcr.sohome.info/images/RailNo9-Poster4A.jpg)
    ；摘自[「軌角九號」海報集](http://forumhk.proplux.info/viewthread.php?tid=750)
12. [拍攝「軌角九號」所屬之\[\[私影](http://kcr.sohome.info/)\]行動「KCR
    Forever」的角色及劇本編排\]