[Dante_exile.jpg](https://zh.wikipedia.org/wiki/File:Dante_exile.jpg "fig:Dante_exile.jpg")\]\]
**流亡**或**逃亡**，指任何[人或](../Page/人.md "wikilink")[團體](../Page/群体.md "wikilink")，因為[自然災害](../Page/自然災害.md "wikilink")、受到[侵略](../Page/侵略.md "wikilink")、[迫害或其他負面因素而離開](../Page/迫害.md "wikilink")[出生或](../Page/出生.md "wikilink")[定居的](../Page/定居.md "wikilink")[國家](../Page/國家.md "wikilink")、[地區](../Page/地區.md "wikilink")。流亡未必指在國外流亡，如[唐玄宗](../Page/唐玄宗.md "wikilink")[幸](../Page/安史之亂.md "wikilink")[蜀](../Page/蜀郡.md "wikilink")，也可以稱做流亡。

## 被視為正在流亡者

  - [斯諾登](../Page/爱德华·斯诺登.md "wikilink")，前[美國](../Page/美國.md "wikilink")[中央情報局職員](../Page/中央情報局.md "wikilink")，美國國家安全局外判技術員，於2013年6月在香港將[美國國家安全局關於](../Page/美國國家安全局.md "wikilink")[稜鏡計劃監聽專案的秘密文件披露給了英國](../Page/稜鏡計畫.md "wikilink")《[衛報](../Page/衛報.md "wikilink")》和美國《[華盛頓郵報](../Page/華盛頓郵報.md "wikilink")》，遭到[美國和](../Page/美國.md "wikilink")[英國通緝](../Page/英國.md "wikilink")，於2013年6月從香港飛抵莫斯科，獲得俄羅斯政府許可居留。
  - [第十四世达赖喇嘛](../Page/第十四世达赖喇嘛.md "wikilink")，1959年[中國人民解放軍進軍](../Page/中國人民解放軍.md "wikilink")[拉薩後](../Page/拉萨市.md "wikilink")，在[美國](../Page/美國.md "wikilink")[中央情報局安排下流亡至](../Page/中央情報局.md "wikilink")[印度](../Page/印度.md "wikilink")[達蘭薩拉](../Page/達蘭薩拉.md "wikilink")。著有自傳《流亡中的自在》。
  - [王军涛](../Page/王军涛.md "wikilink")，[六四事件后在中国国内逃亡](../Page/六四事件.md "wikilink")。1990年11月24日被捕，并以颠覆政府罪、反革命宣传煽动罪被判刑13年。1994年以“保外就医”的名义被直接送美国。
  - [王丹](../Page/王丹_\(1969年\).md "wikilink")、[吾爾開希](../Page/吾爾開希.md "wikilink")：原[六四事件的學生領袖](../Page/六四事件.md "wikilink")。事件爆發後流亡到美國。后在台定居并接受[政治庇護](../Page/庇護權.md "wikilink")。2017年7月，王丹离开台湾，返回美国。
  - [王有才](../Page/王有才.md "wikilink")，1989年六四事件“学生运动”的组织参与者之一。1998年参与组建中国民主党，多次被中国政府逮捕入狱。现居住在美国。
  - [方觉](../Page/方觉.md "wikilink")
  - [黄长烨](../Page/黄长烨.md "wikilink")，[北韩思想家](../Page/朝鲜民主主义人民共和国.md "wikilink")，1997年进入北京的韩国驻华大使馆寻求庇护，2010年病逝於[首爾](../Page/首爾.md "wikilink")。
  - [陈用林](../Page/陈用林.md "wikilink")，前中国外交官，2005年5月26日出走。
  - [袁紅冰](../Page/袁紅冰.md "wikilink")，現在流亡至[澳洲](../Page/澳大利亚.md "wikilink")。
  - [許家屯](../Page/許家屯.md "wikilink")，原[新華社香港分社社長](../Page/新華通訊社香港分社.md "wikilink")。六四事件後流亡到[美國](../Page/美國.md "wikilink")，2016年逝世。
  - [熱比婭·卡德爾](../Page/熱比婭·卡德爾.md "wikilink")，原為[新疆維吾爾自治區的富商](../Page/新疆維吾爾自治區.md "wikilink")，後來投入[民族主義運動而被中國政府視為主張](../Page/民族主義.md "wikilink")[新疆独立运动的](../Page/东突厥斯坦独立运动.md "wikilink")「[分離主義者](../Page/分離主義.md "wikilink")」，流亡至美國尋求政治庇護，曾三度獲提名[諾貝爾和平獎](../Page/諾貝爾和平獎.md "wikilink")。\[1\]\[2\]\[3\]<ref>

</ref>

  - [宰因·阿比丁·本·阿里](../Page/宰因·阿比丁·本·阿里.md "wikilink")，[突尼西亞強人領袖](../Page/突尼西亞.md "wikilink")，因為[茉莉花革命而被迫下台](../Page/茉莉花革命.md "wikilink")，流亡至[沙烏地阿拉伯](../Page/沙特阿拉伯.md "wikilink")「養病」。
  - [英拉·西那瓦](../Page/英拉·西那瓦.md "wikilink")，前泰国总理，因渎职被迫下台并被泰国军方监禁，2017年泰国最高法院审理其案件前逃亡柬埔寨，现居住在英国。
  - [他信·西那瓦](../Page/他信·西那瓦.md "wikilink")，前泰国总理，2006年在其访问[纽约联合国总部期间因泰国国内发生政变而辞职并流亡海外](../Page/纽约.md "wikilink")。
  - [陈光诚](../Page/陈光诚.md "wikilink")，中国维权人士，因揭露山东临沂地方政府强制堕胎的行为被判刑，刑满释放后被软禁在家，2012年4月经他人帮助逃离山东抵达北京，并进入美国驻华大使馆避难，当年5月陈光诚离开中国流亡美国。
  - [刘霞](../Page/刘霞.md "wikilink")，诺贝尔和平奖得主[刘晓波的妻子](../Page/刘晓波.md "wikilink")，2011年以后被中国政府软禁，2018年7月被释放并离开中国经芬兰抵达德国柏林。

## 曾經流亡的人物

  - [晉文公](../Page/晉文公.md "wikilink")，[春秋時代因](../Page/春秋時代.md "wikilink")[驪姬之亂四處流亡](../Page/驪姬之亂.md "wikilink")，十九年後回國登基。
  - [屈原](../Page/屈原.md "wikilink")，名平。[戰國時代](../Page/戰國時代.md "wikilink")[楚國人](../Page/楚國.md "wikilink")，曾為三閭大夫。為忠臣，然被[靳尚](../Page/靳尚.md "wikilink")、[鄭袖中傷](../Page/鄭袖.md "wikilink")，第一次被[楚懷王流放](../Page/楚懷王.md "wikilink")，後因[楚懷王被](../Page/楚懷王.md "wikilink")[張儀所欺](../Page/張儀.md "wikilink")，與[齊](../Page/齐国.md "wikilink")、[宋等諸侯斷交](../Page/宋国.md "wikilink")，向[秦國進軍](../Page/秦國.md "wikilink")，然而屢戰屢敗，遂將屈原找回，派其出使齊國，重新邦交。可是[楚懷王最後被秦國騙至是國](../Page/楚懷王.md "wikilink")，薨於秦國，子[楚頃襄王立](../Page/楚頃襄王.md "wikilink")。頃襄王以弟[子蘭為](../Page/子兰.md "wikilink")[令尹](../Page/令尹.md "wikilink")，[子蘭再遣](../Page/子兰.md "wikilink")[靳尚中傷屈原](../Page/靳尚.md "wikilink")，終於屈原再次被放逐於江南之地。被放逐時曾作《[離騷](../Page/離騷.md "wikilink")》、《[九歌](../Page/九歌.md "wikilink")》冀頃襄王能醒悟。最後秦將[白起於](../Page/白起.md "wikilink")[鄢郢之戰大破楚軍](../Page/鄢郢之戰.md "wikilink")，屈原不願見亡國，遂投[汨羅江而死](../Page/汨羅江.md "wikilink")。屈原死後，[蒙騖再攻擊楚國](../Page/蒙騖.md "wikilink")，[項燕](../Page/項燕.md "wikilink")（即[項梁的父親](../Page/項梁.md "wikilink")、[項羽之祖父](../Page/項羽.md "wikilink")）等戰死，楚國遂亡。屈原是《[楚辭](../Page/楚辭.md "wikilink")》的代表，其學生[宋玉也是](../Page/宋玉.md "wikilink")[楚辭名家](../Page/楚辭.md "wikilink")。
  - [源賴朝](../Page/源賴朝.md "wikilink")，日本知名武將，因其父祖與[平清盛之](../Page/平清盛.md "wikilink")[平氏爭權失利](../Page/平氏.md "wikilink")，其父、伯、叔、表兄皆被[平清盛以謀反罪處死](../Page/平清盛.md "wikilink")，賴朝在平清盛之母見其面目清秀也年幼，遂被存活，但被流放。日後成長成為知名武將，與其殘存族人包含[源義經率領之下將](../Page/源義經.md "wikilink")[平清盛擊殺](../Page/平清盛.md "wikilink")，報了家仇，並創立幕府。
  - [拿破崙](../Page/拿破仑一世.md "wikilink")，法國[波拿巴帝國皇帝](../Page/法兰西第一帝国.md "wikilink")，1814年萊比錫戰敗後被流放至[厄爾巴島](../Page/厄爾巴島.md "wikilink")；於1815年3月逃回巴黎[復辟](../Page/百日王朝.md "wikilink")，6月[滑鐵盧戰敗後被英國流放到](../Page/滑铁卢战役.md "wikilink")[聖赫勒拿島](../Page/圣赫勒拿.md "wikilink")。
  - [金大中](../Page/金大中.md "wikilink")，南韓已故總統。曾在[朴正熙時期流亡至日本](../Page/朴正熙.md "wikilink")，後來於[全斗煥](../Page/全斗煥.md "wikilink")、[盧泰愚政權下台後得以返回南韓](../Page/盧泰愚.md "wikilink")，並順利選上[總統](../Page/大韩民国总统.md "wikilink")。
  - [金日成](../Page/金日成.md "wikilink")，1925年，他随父亲[金亨稷逃亡到](../Page/金亨稷.md "wikilink")[中华民国](../Page/中华民国.md "wikilink")，後在二戰結束後返回[朝鮮半島北部](../Page/朝鮮半島.md "wikilink")，建立[朝鮮民主主義人民共和國](../Page/朝鮮民主主義人民共和國.md "wikilink")（[北韓](../Page/朝鲜民主主义人民共和国.md "wikilink")）。
  - [卡斯特罗](../Page/菲德尔·卡斯特罗.md "wikilink")，在古巴总统[巴蒂斯塔宣布](../Page/富尔亨西奥·巴蒂斯塔.md "wikilink")[大赦后](../Page/大赦.md "wikilink")，他流亡[美国](../Page/美国.md "wikilink")、[墨西哥](../Page/墨西哥.md "wikilink")；最後於1959年推翻巴蒂斯塔並建立社會主義政權。
  - [塔克辛](../Page/達新·欽那瓦.md "wikilink")，前[泰國](../Page/泰國.md "wikilink")[總理](../Page/泰国总理列表.md "wikilink")。
  - [彭明敏](../Page/彭明敏.md "wikilink")，[台灣獨立運動指標人物](../Page/台灣獨立運動.md "wikilink")。於[戒嚴時流亡到美國](../Page/臺灣省戒嚴令.md "wikilink")，並在[解嚴後順利返台](../Page/臺灣省戒嚴令.md "wikilink")；之後於1996年代表[民主進步黨參選](../Page/民主進步黨.md "wikilink")[中華民國總統](../Page/中華民國總統.md "wikilink")，敗給[中國國民黨的](../Page/中國國民黨.md "wikilink")[李登輝](../Page/李登輝.md "wikilink")。
  - [許信良](../Page/許信良.md "wikilink")，台灣民運人士，曾任[桃園縣縣長](../Page/桃園市.md "wikilink")。於[中壢事件爆發後流亡](../Page/中壢事件.md "wikilink")，後來在返台時，縣民前往[中正國際機場](../Page/臺灣桃園國際機場.md "wikilink")（今[桃園國際機場](../Page/臺灣桃園國際機場.md "wikilink")）接機並與警方爆發衝突。
  - [金正男](../Page/金正男.md "wikilink")，北韓領導人[金正日長子](../Page/金正日.md "wikilink")。因為涉入與同父異母弟弟[金正恩的領導人交棒爭奪戰](../Page/金正恩.md "wikilink")，曾遭到金正恩派人追殺；金正男長居[澳門](../Page/澳門.md "wikilink")，受到中國方面保護。2017年2月13日在[馬來西亞的](../Page/馬來西亞.md "wikilink")[吉隆坡國際機場遇害身亡](../Page/吉隆坡國際機場.md "wikilink")。
  - [巴勒維國王](../Page/穆罕默德-礼萨·巴列维.md "wikilink")，[伊朗末代君主](../Page/伊朗.md "wikilink")，於[伊朗伊斯蘭革命後流亡美國](../Page/伊朗伊斯蘭革命.md "wikilink")，病逝當地。
  - [何梅尼](../Page/鲁霍拉·穆萨维·霍梅尼.md "wikilink")，首位[伊朗最高領袖](../Page/伊朗最高领袖.md "wikilink")，發動伊朗伊斯蘭革命，並從法國返回[德黑蘭](../Page/德黑蘭.md "wikilink")。
  - [索尔仁尼琴](../Page/索尔仁尼琴.md "wikilink")，前苏联作家，1945年因在信中称[斯大林为](../Page/斯大林.md "wikilink")“八字胡的家伙”在东普鲁士前线被捕，1956年被释放。赫鲁晓夫下台后索尔仁尼琴再次被捕并被流放到[西德](../Page/西德.md "wikilink")，时任苏共中央总书记[勃列日涅夫剥夺了他的苏联国籍](../Page/勃列日涅夫.md "wikilink")。索尔仁尼琴后移居美国[佛蒙特州](../Page/佛蒙特州.md "wikilink")，1994年[苏联解体后返回](../Page/苏联解体.md "wikilink")[俄罗斯](../Page/俄罗斯.md "wikilink")。
  - [托洛斯基](../Page/列夫·达维多维奇·托洛茨基.md "wikilink")，前蘇聯[布爾什維克領導幹部](../Page/布爾什維克.md "wikilink")，因為他所提倡的[托派理論與](../Page/托洛茨基主义.md "wikilink")[史達林理念不合](../Page/约瑟夫·维萨里奥诺维奇·斯大林.md "wikilink")，被迫流亡，最後遭史達林派出的特工刺殺身亡。
  - [阮文紹](../Page/阮文紹.md "wikilink")，[南越總統](../Page/越南共和国.md "wikilink")，最後在[胡志明領軍的越共軍隊攻進南越首都](../Page/胡志明.md "wikilink")[西貢時](../Page/胡志明市.md "wikilink")，經由台灣流亡到美國，病逝當地。
  - [班尼格諾·艾奎諾](../Page/班尼格諾·艾奎諾.md "wikilink")，[菲律賓民運人士](../Page/菲律賓.md "wikilink")，主張反對[馬可仕的獨裁貪腐政治](../Page/费迪南德·马科斯.md "wikilink")，最後從台灣搭機返回馬尼拉時，遭馬可仕派出的特工刺殺身亡。
  - [賴昌星](../Page/賴昌星.md "wikilink")，前[遠華集團總裁](../Page/遠華案.md "wikilink")，因涉嫌大規模[走私的](../Page/走私.md "wikilink")[遠華案](../Page/遠華案.md "wikilink")，加上[全國政協主席](../Page/中国人民政治协商会议全国委员会主席.md "wikilink")[賈慶林疑似被捲入此案](../Page/賈慶林.md "wikilink")，曾流亡至加拿大，後來在2011年中加雙方協商將其引渡回中國受審。
  - [馮正虎](../Page/冯正虎.md "wikilink")，中國知名維權人士，2009年曾多次被迫流亡日本不被允許返國，在當年11月4日至隔年（2010年）2月1日間滯留成田機場的入境審查處拒絕入境。經協調後於2月2日入境日本、2月12日從日本搭機順利返回上海。
  - [劉鶚](../Page/劉鶚.md "wikilink")，[老殘遊記的作者](../Page/老殘遊記.md "wikilink")，被流放至[烏魯木齊](../Page/乌鲁木齐市.md "wikilink")
  - [方励之](../Page/方励之.md "wikilink")，原中国科学技术大学副校长，六四事件后被开除党籍并被撤销职务，随后方励之在六四事件结束后进入美国驻华大使馆避难，1990年6月经中国政府允许离开中国，经英国最终抵达美国，2012年4月方励之在美国[亚利桑那州](../Page/亚利桑那州.md "wikilink")[图森市病逝](../Page/图森市.md "wikilink")。

## 参考文献

## 參見

  - [投誠](../Page/投誠.md "wikilink")
  - [流亡政府](../Page/流亡政府.md "wikilink")

{{-}}

[Category:人口流動](../Category/人口流動.md "wikilink")
[流亡](../Category/流亡.md "wikilink")

1.
2.
3.  [Fighting for her peoples’ rights: Rebiya Kadeer visits
    Australia](http://www.amnesty.org.au/china/comments/9233), Amnesty
    International, February 2008