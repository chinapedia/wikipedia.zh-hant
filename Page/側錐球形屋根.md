| 側錐球形屋根                                                                                                                        |
| ----------------------------------------------------------------------------------------------------------------------------- |
| [Augmented_sphenocorona.png](https://zh.wikipedia.org/wiki/File:Augmented_sphenocorona.png "fig:Augmented_sphenocorona.png") |
| 類別                                                                                                                            |
| [面](../Page/面.md "wikilink")                                                                                                  |
| [邊](../Page/邊.md "wikilink")                                                                                                  |
| [頂點](../Page/頂點.md "wikilink")                                                                                                |
| [頂點佈局](../Page/頂點佈局.md "wikilink")                                                                                            |
| [點群](../Page/點群.md "wikilink")                                                                                                |
| [對偶多面體](../Page/對偶多面體.md "wikilink")                                                                                          |
| 性質                                                                                                                            |

**側錐球形屋根**（**J<sub>87</sub>, Augmented
sphenocorona**）是[Johnson多面體的其中一個](../Page/Johnson多面體.md "wikilink")。它雖然可由[球形屋根](../Page/球形屋根.md "wikilink")（J<sub>86</sub>）於側面增加一[正四角錐](../Page/正四角錐.md "wikilink")（J<sub>1</sub>），但無法由[柏拉圖立體](../Page/柏拉圖立體.md "wikilink")（正多面體）和[阿基米得立體](../Page/阿基米得立體.md "wikilink")（半正多面體）經過切割、增補而得來。這92種詹森多面體最早在1996年由（Norman
Johnson）命名並給予描述。

## 參見

  - [詹森多面體](../Page/詹森多面體.md "wikilink")
  - [球形屋根](../Page/球形屋根.md "wikilink")
  - [正四角錐](../Page/正四角錐.md "wikilink")
  - [多面體](../Page/多面體.md "wikilink")

## 外部連結

  -   -
[Category:约翰逊多面体](../Category/约翰逊多面体.md "wikilink")