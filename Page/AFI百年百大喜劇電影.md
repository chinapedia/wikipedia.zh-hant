**AFI百年百大喜劇電影**（**AFI's 100 Years... 100
Laughs**），[美国电影学会](../Page/美国电影学会.md "wikilink")（AFI）於2000年從400部被提名[電影中](../Page/電影.md "wikilink")，評選出100部百年來最偉大的[喜劇片名單](../Page/喜劇.md "wikilink")，作為美国电影学院的“百年百大”系列的一个重要部分。

## AFI百年百大喜劇電影

| 項次    | 中文片名                                                                      | 英文片名                                  | 年份     |
| ----- | ------------------------------------------------------------------------- | ------------------------------------- | ------ |
| 1\.   | [熱情如火](../Page/熱情如火.md "wikilink")                                        | Some Like It Hot                      | 1959   |
| 2\.   | [窈窕淑男](../Page/窈窕淑男.md "wikilink")                                        | Tootsie                               | 1982   |
| 3\.   | [奇愛博士](../Page/奇愛博士.md "wikilink")                                        | Dr. Strangelove                       | 1964   |
| 4\.   | [安妮霍爾](../Page/安妮霍爾.md "wikilink")                                        | Annie Hall                            | (1977) |
| 5\.   | [鴨羹](../Page/鴨羹.md "wikilink")                                            | Duck Soup                             | (1933) |
| 6\.   | [閃亮的馬鞍](../Page/閃亮的馬鞍.md "wikilink")                                      | Blazing Saddles                       | (1974) |
| 7\.   | [風流軍醫俏護士](../Page/風流軍醫俏護士.md "wikilink")                                  | M\*A\*S\*H                            | (1970) |
| 8\.   | [一夜風流](../Page/一夜風流.md "wikilink")                                        | It Happened One Night                 | (1934) |
| 9\.   | [畢業生](../Page/畢業生_\(電影\).md "wikilink")                                   | The Graduate                          | (1967) |
| 10\.  | [空前絕後滿天飛](../Page/空前絕後滿天飛.md "wikilink")                                  | Airplane\!                            | (1980) |
| 11\.  | [金牌製片人](../Page/金牌製片人.md "wikilink")                                      | The Producers                         | (1968) |
| 12\.  | [歌劇之夜](../Page/歌劇之夜.md "wikilink")                                        | A Night at the Opera                  | (1935) |
| 13\.  | [新科學怪人](../Page/新科學怪人.md "wikilink")                                      | Young Frankenstein                    | (1974) |
| 14\.  | [育嬰奇譚](../Page/育嬰奇譚.md "wikilink")                                        | Bringing Up Baby                      | (1938) |
| 15\.  | [費城故事](../Page/費城故事.md "wikilink")                                        | The Philadelphia Story                | (1940) |
| 16\.  | [萬花嬉春](../Page/萬花嬉春.md "wikilink")                                        | Singin' in the Rain                   | (1952) |
| 17\.  | [單身公寓](../Page/單身公寓.md "wikilink")                                        | The Odd Couple                        | (1968) |
| 18\.  | [將軍號](../Page/將軍號.md "wikilink")                                          | The General                           | (1927) |
| 19\.  | [女友禮拜五](../Page/女友禮拜五.md "wikilink")                                      | His Girl Friday                       | (1940) |
| 20\.  | [公寓春光](../Page/公寓春光.md "wikilink")                                        | The Apartmen                          | (1960) |
| 21\.  | [笨賊一籮筐](../Page/笨賊一籮筐.md "wikilink")                                      | A Fish Called Wanda                   | (1988) |
| 22\.  | [金屋藏嬌](../Page/金屋藏嬌_\(電影\).md "wikilink")                                 | Adam's Rib                            | (1949) |
| 23\.  | [當哈利碰上莎莉](../Page/當哈利碰上莎莉.md "wikilink")                                  | When Harry Met Sally                  | (1989) |
| 24\.  | [絳帳海堂春](../Page/絳帳海堂春.md "wikilink")                                      | Born Yesterday                        | (1950) |
| 25\.  | [淘金記](../Page/淘金記.md "wikilink")                                          | The Gold Rush                         | (1925) |
| 26\.  | [無為而治](../Page/無為而治_\(電影\).md "wikilink")                                 | Being There                           | (1979) |
| 27\.  | [哈啦瑪莉](../Page/哈啦瑪莉.md "wikilink")                                        | There's Something About Mary          | (1998) |
| 28\.  | [魔鬼剋星](../Page/魔鬼剋星.md "wikilink")                                        | Ghost Busters                         | (1984) |
| 29\.  | [搖滾萬萬歲](../Page/搖滾萬萬歲.md "wikilink")                                      | This Is Spinal Tap                    | (1984) |
| 30\.  | [毒藥與老婦](../Page/毒藥與老婦.md "wikilink")                                      | Arsenic and Old Lace                  | (1944) |
| 31\.  | [撫養亞利桑那](../Page/撫養亞利桑那.md "wikilink")                                    | Raising Arizona                       | (1987) |
| 32\.  | [瘦子](../Page/瘦子_\(電影\).md "wikilink")                                     | The Thin Man                          | (1934) |
| 33\.  | [摩登時代](../Page/摩登時代.md "wikilink")                                        | Modern Times                          | (1936) |
| 34\.  | [今天暫時停止](../Page/今天暫時停止.md "wikilink")                                    | Groundhog Day                         | (1993) |
| 35\.  | [迷離世界](../Page/迷離世界.md "wikilink")                                        | Harvey                                | (1950) |
| 36\.  | [動物屋](../Page/動物屋.md "wikilink")                                          | Animal House                          | (1978) |
| 37\.  | [大獨裁者](../Page/大獨裁者.md "wikilink")                                        | The Great Dictator                    | (1940) |
| 38\.  | [城市之光](../Page/城市之光.md "wikilink")                                        | City Lights                           | (1931) |
| 39\.  | [蘇利文的旅行](../Page/蘇利文的旅行.md "wikilink")                                    | Sullivan's Travels                    | (1941) |
| 40\.  | [瘋狂世界](../Page/瘋狂世界_\(電影\).md "wikilink")                                 | It's a Mad Mad Mad Mad World          | (1963) |
| 41\.  | [月滿抱佳人](../Page/月滿抱佳人.md "wikilink")                                      | Moonstruck                            | (1987) |
| 42\.  | [飛進未來](../Page/飛進未來.md "wikilink")                                        | Big                                   | (1988) |
| 43\.  | [美國風情畫](../Page/美國風情畫.md "wikilink")                                      | American Graffiti                     | (1973) |
| 44\.  | [妙管家](../Page/妙管家.md "wikilink")                                          | My Man Godfrey                        | (1936) |
| 45\.  | [哈洛與茂德](../Page/哈洛與茂德.md "wikilink")                                      | Harold And Maude                      | (1972) |
| 46\.  | [曼哈頓](../Page/曼哈頓_\(電影\).md "wikilink")                                   | Manhattan                             | (1979) |
| 47\.  | [洗髮精](../Page/洗髮精_\(電影\).md "wikilink")                                   | Shampoo                               | (1975) |
| 48\.  | [烏龍謀殺案](../Page/烏龍謀殺案.md "wikilink")                                      | A Shot In The Dark                    | (1964) |
| 49\.  | [你逃我也逃](../Page/你逃我也逃_\(1942年電影\).md "wikilink")                          | To Be Or Not To Be                    | (1942) |
| 50\.  | [狼城脂粉俠](../Page/狼城脂粉俠.md "wikilink")                                      | Cat Ballou                            | (1965) |
| 51\.  | [七年之癢](../Page/七年之癢_\(電影\).md "wikilink")                                 | The Seven Year Itch                   | (1955) |
| 52\.  | [妮諾奇嘉](../Page/妮諾奇嘉.md "wikilink")                                        | Ninotchka                             | (1939) |
| 53\.  | [二八佳人花公子](../Page/二八佳人花公子.md "wikilink")                                  | Arthur                                | (1981) |
| 54\.  | [摩根河的奇蹟](../Page/摩根河的奇蹟.md "wikilink")                                    | The Miracle Of Morgan's Creek         | (1944) |
| 55\.  | [淑女伊芙](../Page/淑女伊芙.md "wikilink")                                        | The Lady Eve                          | (1941) |
| 56\.  | [兩傻大戰科學怪人](../Page/兩傻大戰科學怪人.md "wikilink")                                | Abbott And Costello Meet Frankenstein | (1948) |
| 57\.  | [餐館](../Page/餐館_\(電影\).md "wikilink")                                     | Diner                                 | (1982) |
| 58\.  | [禮物](../Page/禮物_\(電影\).md "wikilink")                                     | It's A Gift                           | (1934) |
| 59\.  | [賽馬場的一天](../Page/賽馬場的一天.md "wikilink")                                    | A Day At The Races                    | (1937) |
| 60\.  | [逍遙鬼侶](../Page/逍遙鬼侶.md "wikilink")                                        | Topper                                | (1937) |
| 61\.  | [愛的大追蹤](../Page/愛的大追蹤.md "wikilink")                                      | What's Up, Doc?                       | (1972) |
| 62\.  | [福尔摩斯二世](../Page/福尔摩斯二世.md "wikilink")                                    | Sherlock, Jr.                         | (1924) |
| 63\.  | [比佛利山超級警探](../Page/比佛利山超級警探.md "wikilink")                                | Beverly Hills Cop                     | (1984) |
| 64\.  | [收播新聞](../Page/收播新聞.md "wikilink")                                        | Broadcast News                        | (1987) |
| 65\.  | [趾高氣昂](../Page/趾高氣昂.md "wikilink")                                        | Horse Feathers                        | (1932) |
| 66\.  | [拿了錢就跑](../Page/拿了錢就跑.md "wikilink")、[傻瓜入獄記](../Page/傻瓜入獄記.md "wikilink") | Take The Money And Run                | (1969) |
| 67\.  | [窈窕奶爸](../Page/窈窕奶爸.md "wikilink")                                        | Mrs. Doubtfire                        | (1993) |
| 68\.  | [春閨風月](../Page/春閨風月.md "wikilink")                                        | The Awful Truth                       | (1937) |
| 69\.  | [傻瓜大鬧香蕉城](../Page/傻瓜大鬧香蕉城.md "wikilink")                                  | Bananas                               | (1971) |
| 70\.  | [富貴浮雲](../Page/富貴浮雲_\(電影\).md "wikilink")                                 | Mr. Deeds Goes To Town                | (1936) |
| 71\.  | [瘋狂高爾夫](../Page/瘋狂高爾夫.md "wikilink")                                      | Caddyshack                            | (1980) |
| 72\.  | [燕雀香巢](../Page/燕雀香巢.md "wikilink")                                        | Mr. Blandings Builds His Dream House  | (1948) |
| 73\.  | [胡闹](../Page/胡闹_\(1931年电影\).md "wikilink")                                | Monkey Business                       | (1931) |
| 74\.  | [朝九晚五](../Page/朝九晚五_\(1980年電影\).md "wikilink")                            | 9 To 5                                | (1980) |
| 75\.  | [儂本多情](../Page/儂本多情_\(1933年電影\).md "wikilink")                            | She Done Him Wrong                    | (1933) |
| 76\.  | [雌雄莫辨](../Page/雌雄莫辨_\(電影\).md "wikilink")                                 | Victor/Victoria                       | (1982) |
| 77\.  | [棕櫚海灘的故事](../Page/棕櫚海灘的故事.md "wikilink")                                  | The Palm Beach Story                  | (1942) |
| 78\.  | [荒漠迷宮](../Page/荒漠迷宮.md "wikilink")                                        | Road To Morocco                       | (1942) |
| 79\.  | [新鮮人](../Page/新鮮人_\(1925年電影\).md "wikilink")                              | The Freshman                          | (1925) |
| 80\.  | [傻瓜大鬧科學城](../Page/傻瓜大鬧科學城.md "wikilink")                                  | Sleeper                               | (1973) |
| 81\.  | [航海家](../Page/航海家_\(電影\).md "wikilink")                                   | The Navigator                         | (1924) |
| 82\.  | [小迷糊當大兵](../Page/小迷糊當大兵.md "wikilink")                                    | Private Benjamin                      | (1980) |
| 83\.  | [岳父大人](../Page/岳父大人.md "wikilink")                                        | Father Of The Bride                   | (1950) |
| 84\.  | [迷失的美國人](../Page/迷失的美國人.md "wikilink")                                    | Lost In America                       | (1985) |
| 85\.  | [晚宴](../Page/晚宴_\(電影\).md "wikilink")                                     | Dinner At Eight                       | (1933) |
| 86\.  | [城市鄉巴佬](../Page/城市鄉巴佬.md "wikilink")                                      | City Slickers                         | (1991) |
| 87\.  | [開放的美國學府](../Page/開放的美國學府.md "wikilink")                                  | Fast Times At Ridgemont High          | (1982) |
| 88\.  | [陰間大法師](../Page/陰間大法師.md "wikilink")                                      | Beetlejuice                           | (1988) |
| 89\.  | [大笨蛋](../Page/大笨蛋.md "wikilink")                                          | The Jerk                              | (1979) |
| 90\.  | [小姑獨處](../Page/小姑獨處.md "wikilink")                                        | Woman Of The Year                     | (1942) |
| 91\.  | [花花大少](../Page/花花大少.md "wikilink")                                        | The Heartbreak Kid                    | (1972) |
| 92\.  | [火球](../Page/火球.md "wikilink")                                            | Ball Of Fire                          | (1942) |
| 93\.  | [冰血暴](../Page/冰血暴.md "wikilink")                                          | Fargo                                 | (1996) |
| 94\.  | [梅姑](../Page/梅姑_\(電影\).md "wikilink")                                     | Auntie Mame                           | (1958) |
| 95\.  | [銀線號大血案](../Page/銀線號大血案.md "wikilink")                                    | Silver Streak                         | (1976) |
| 96\.  | [沙漠之子](../Page/沙漠之子.md "wikilink")                                        | Sons Of The Desert                    | (1933) |
| 97\.  | [百萬金臂](../Page/百萬金臂.md "wikilink")                                        | Bull Durham                           | (1988) |
| 98\.  | [金殿福星](../Page/金殿福星.md "wikilink")                                        | The Court Jester                      | (1956) |
| 99\.  | [隨身變](../Page/隨身變.md "wikilink")                                          | The Nutty Professor                   | (1963) |
| 100\. | [早安越南](../Page/早安越南.md "wikilink")                                        | Good Morning, Vietnam                 | (1987) |

## 外部連結

  - [AFI's list](http://www.afi.com/tvevents/100years/laughs.aspx)

[A](../Category/美國電影學院百年百大系列.md "wikilink")