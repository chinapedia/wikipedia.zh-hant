**傅老榕**（），原名**傅德用**，字**廣源**，號**偉生**，[廣東省](../Page/廣東省.md "wikilink")[佛山](../Page/佛山.md "wikilink")[南海縣](../Page/南海縣.md "wikilink")[西樵山碧雲村人](../Page/西樵山.md "wikilink")。他是港澳地區著名實業家，亦為[澳門一代](../Page/澳門.md "wikilink")[賭王](../Page/賭王.md "wikilink")，於[何鴻燊承投](../Page/何鴻燊.md "wikilink")[澳門博彩業專營權之前](../Page/澳門博彩業.md "wikilink")，專營[澳門賭業達二十多年之久](../Page/澳門博彩業.md "wikilink")，其家族當時與[高可寧家族](../Page/高可寧家族.md "wikilink")、[何東家族和](../Page/何啟東家族.md "wikilink")[羅文錦家族合稱香港四大家族](../Page/羅文錦家族.md "wikilink")。

## 生平

傅德用生於[廣東省](../Page/廣東省.md "wikilink")[南海縣一戶貧窮人家](../Page/南海縣.md "wikilink")。8歲時，因家鄉遭逢旱災，其父[傅球芝離鄉遠赴](../Page/傅球芝.md "wikilink")[香港謀生](../Page/香港.md "wikilink")，擔任[五金工人](../Page/五金.md "wikilink")。傅德用自小對[賭博便有興趣](../Page/賭博.md "wikilink")，1913年他跟隨父親到[香港擔任輪船機械學徒](../Page/香港.md "wikilink")，亦經常流連賭攤、碰碰運氣。慢慢地，賭場的人們都叫他做“老用”，後來他便直接取其諧音為自己改名為傅老榕。

不過，傅老榕其後卻因鬥毆被[禁錮於監獄達](../Page/禁錮.md "wikilink")10個月。刑滿出獄後，他離開香港到[廣西省一帶營商](../Page/廣西省.md "wikilink")，更結交了不少達官貴人。1930年，傅老榕有意承投澳門賭牌，最後更投得賭牌，但當時[范潔朋](../Page/范潔朋.md "wikilink")、[霍芝庭及](../Page/霍芝庭.md "wikilink")[李聲炬等人合營之](../Page/李聲炬.md "wikilink")[豪興公司財雄勢大](../Page/豪興公司.md "wikilink")，傅老榕只好轉到[深圳發展](../Page/深圳.md "wikilink")。後來，霍芝庭與傅老榕在深圳合辦了一間賭場，風頭一時無兩。1937年，[抗日戰爭全面爆發](../Page/抗日戰爭.md "wikilink")，傅老榕將深圳的賭場賣掉，帶同大批資金回到澳門，與[高可寧聯手成立](../Page/高可寧.md "wikilink")[泰興公司](../Page/泰興公司_\(澳門\).md "wikilink")，以每年180萬兩[銀錢的賭稅](../Page/銀錢.md "wikilink")，成功投得了澳門博彩專營權。投得賭牌後，他收購了[新馬路的](../Page/新馬路.md "wikilink")[中央酒店並開設賭場](../Page/新中央酒店.md "wikilink")，開展了他叱吒澳門賭業二十多年的序幕。

除賭業之外，傅老榕亦有經營其他業務，包括德記船務貿易公司、大來輪船及十六號碼頭
(現為[十六浦](../Page/十六浦.md "wikilink"))，風頭一時無兩。

1946年2月10日晚上，傅老榕遭遇到綁架。當時傅老榕正於[普濟禪院內休息](../Page/普濟禪院.md "wikilink")，一班歹徒突然闖入，將他綁走。歹徒要求贖款900萬元，經過[何賢一輪斡旋之後](../Page/何賢.md "wikilink")，綁匪同意降低贖款至50萬元。但是，在交付贖金前夕，傅老榕的兒子[傅蔭權查出父親被困之地](../Page/傅蔭權.md "wikilink")，隨即報警求助。豈料走漏風聲，警方截至藏參地點時已人去樓空，綁匪更因此舉而大怒，並割下傅老榕的右耳及堅持原定900萬的贖款。不過，其後在[粵劇界名人](../Page/粵劇.md "wikilink")[新馬師曾和他的一位朋友協助下](../Page/新馬師曾.md "wikilink")，最終令匪徒妥協同意50萬元的贖款，在交付贖金後傅老榕便獲釋了。

在一輪風波後，傅老榕繼續發展他的事業。他於1952年投得[富麗華酒店的地皮](../Page/富麗華酒店.md "wikilink")，1956年成功收購了香港[太古洋行遠東總行大廈](../Page/太古洋行.md "wikilink")。1960年，傅老榕病逝，享年66歲。

## 家族

[傅老榕家族於傅老榕死後逐漸退出澳門](../Page/傅老榕家族.md "wikilink")，並於香港投資富麗華酒店以及房地產業務。傅老榕的長子[傅蔭釗為富麗華酒店創辦人](../Page/傅蔭釗.md "wikilink")，酒店於1973年落成啟用，並於1985年上市。家族持有資產估計約80億港元。

家族公司廣興置業集團持有物業包括:

香港 司徒拔道46號眺馬閣 Raceview Mansions 46 Stubbs Road

香港中環紅棉路8號東昌大廈 Fairmont House 8 Cotton Tree Drive, Central.

香港黃竹坑黃竹坑道38號 AXA Southside 38 Wong Chuck Hang Rd, Wong Chuk Hang, Hong
Kong

## 相關條目

  - [傅老榕家族](../Page/傅老榕家族.md "wikilink")
  - [高可寧](../Page/高可寧.md "wikilink")
  - [南生圍](../Page/南生圍.md "wikilink")

## 參考資源

  - 《何厚鏵家族傳》，吳楠著，1999。
  - 《何鴻燊傳》，冷夏著，1995。

[L](../Page/category:傅姓.md "wikilink")
[分類:澳門億萬富豪](../Page/分類:澳門億萬富豪.md "wikilink")

[Category:澳門企業家](../Category/澳門企業家.md "wikilink")
[Category:中国企业家](../Category/中国企业家.md "wikilink")
[Category:博彩業企業家](../Category/博彩業企業家.md "wikilink")
[Category:澳門南海人](../Category/澳門南海人.md "wikilink")
[Category:广州历史人物](../Category/广州历史人物.md "wikilink")
[Category:南海人](../Category/南海人.md "wikilink")