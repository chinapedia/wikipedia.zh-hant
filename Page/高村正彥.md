**高村正彦**（），[日本](../Page/日本.md "wikilink")[政治人物](../Page/政治人物.md "wikilink")。[自由民主黨憲法改正推進本部最高顧問](../Page/自由民主黨_\(日本\).md "wikilink")，元自由民主黨副總裁，[番町政策研究所](../Page/番町政策研究所.md "wikilink")（高村派）元會長。曾歷任法務大臣與防衛大臣（3代）、防衛厅政务次官、[大蔵省政务次官](../Page/大蔵省.md "wikilink")，还曾任村山内阁[经济企划厅长官](../Page/经济企划厅长官.md "wikilink")、第一届小渊内阁[外务大臣和第三届森内阁](../Page/外务大臣.md "wikilink")[法务大臣](../Page/法务大臣.md "wikilink")。2001年[河本敏夫去世后](../Page/河本敏夫.md "wikilink")，继任旧[河本派会长](../Page/河本派.md "wikilink")，[河本派因此改为](../Page/河本派.md "wikilink")[高村派](../Page/高村派.md "wikilink")。是自民党内主张对华友好的著名人士。

2007年9月26日改任新首相[福田康夫內閣](../Page/福田康夫.md "wikilink")[外務大臣](../Page/外務大臣.md "wikilink")。

## 從政經歷

  - 1980年6月22日
    [第36屆日本眾議院議員總選舉](../Page/第36屆日本眾議院議員總選舉.md "wikilink")（舊山口2區・自民黨公認）第1次當選。
  - 1983年12月18日
    [第37屆日本眾議院議員總選舉](../Page/第37屆日本眾議院議員總選舉.md "wikilink")（舊山口2區・自民黨公認）第2次當選。
  - 1986年7月6日
    [第38屆日本眾議院議員總選舉](../Page/第38屆日本眾議院議員總選舉.md "wikilink")（舊山口2區・自民黨公認）第3次當選。
  - 1990年1月24日
    [第39屆日本眾議院議員總選舉](../Page/第39屆日本眾議院議員總選舉.md "wikilink")（舊山口2區・自民黨公認）第4次當選。
  - 1993年7月18日
    [第40屆日本眾議院議員總選舉](../Page/第40屆日本眾議院議員總選舉.md "wikilink")（舊山口2區・自民黨公認）第5次當選。
  - 1994年6月30日　就任[經濟企畫廳長官](../Page/經濟企畫廳.md "wikilink")。
  - 1996年10月20日
    [第41屆日本眾議院議員總選舉](../Page/第41屆日本眾議院議員總選舉.md "wikilink")（山口1區・自民黨公認）第6次當選。
  - 1998年7月30日　就任[外務大臣](../Page/外務大臣.md "wikilink")。
  - 2000年6月25日　[第42屆日本眾議院議員總選舉](../Page/第42屆日本眾議院議員總選舉.md "wikilink")（山口1區・自民黨公認）第7次當選。
  - 2000年7月4日　就任[法務大臣](../Page/法務大臣.md "wikilink")。
  - 2003年9月20日　出馬競選[自由民主黨總裁選舉](../Page/自由民主黨總裁選舉.md "wikilink")，败给[小泉纯一郎](../Page/小泉纯一郎.md "wikilink")
  - 2003年11月9日　[第43屆日本眾議院議員總選舉](../Page/第43屆日本眾議院議員總選舉.md "wikilink")（山口1區・自民黨公認）第8次當選。
  - 2005年9月11日　[第44屆日本眾議院議員總選舉](../Page/第44屆日本眾議院議員總選舉.md "wikilink")（山口1區・自民黨公認）第9次當選。
  - 2007年8月27日就任首相[安倍晉三內閣](../Page/安倍晉三.md "wikilink")[防衛大臣](../Page/防衛大臣.md "wikilink")
  - 2007年9月26日就任首相[福田康夫內閣](../Page/福田康夫.md "wikilink")[外務大臣](../Page/外務大臣.md "wikilink")。
  - 2009年8月30日　[第45屆日本眾議院議員總選舉](../Page/第45屆日本眾議院議員總選舉.md "wikilink")（山口1區・自民黨公認）第10次當選。
  - 2012年12月16日　[第46屆日本眾議院議員總選舉](../Page/第46屆日本眾議院議員總選舉.md "wikilink")（山口1區・自民黨公認）第11次當選。
  - 2014年12月14日　[第47屆日本眾議院議員總選舉](../Page/第47屆日本眾議院議員總選舉.md "wikilink")（山口1區・自民黨公認）第12次當選。
  - 2017年9月 [第48屆日本眾議院議員總選舉不出馬](../Page/第48屆日本眾議院議員總選舉.md "wikilink"),
    引退。

[Category:二战后日本政治人物](../Category/二战后日本政治人物.md "wikilink")
[Category:日本外務大臣](../Category/日本外務大臣.md "wikilink")
[Category:日本防衛大臣](../Category/日本防衛大臣.md "wikilink")
[Category:日本法務大臣](../Category/日本法務大臣.md "wikilink")
[Category:日本經濟企劃廳長官](../Category/日本經濟企劃廳長官.md "wikilink")
[Category:村山內閣閣僚](../Category/村山內閣閣僚.md "wikilink")
[Category:小淵內閣閣僚](../Category/小淵內閣閣僚.md "wikilink")
[Category:第二次森內閣閣僚](../Category/第二次森內閣閣僚.md "wikilink")
[Category:第一次安倍內閣閣僚](../Category/第一次安倍內閣閣僚.md "wikilink")
[Category:福田康夫內閣閣僚](../Category/福田康夫內閣閣僚.md "wikilink")
[Category:番町政策研究所會長](../Category/番町政策研究所會長.md "wikilink")
[Category:日本中央大學校友](../Category/日本中央大學校友.md "wikilink")
[Category:山口縣出身人物](../Category/山口縣出身人物.md "wikilink") [Category:日本眾議院議員
1980–1983](../Category/日本眾議院議員_1980–1983.md "wikilink")
[Category:日本眾議院議員
1983–1986](../Category/日本眾議院議員_1983–1986.md "wikilink")
[Category:日本眾議院議員
1986–1990](../Category/日本眾議院議員_1986–1990.md "wikilink")
[Category:日本眾議院議員
1990–1993](../Category/日本眾議院議員_1990–1993.md "wikilink")
[Category:日本眾議院議員
1993–1996](../Category/日本眾議院議員_1993–1996.md "wikilink")
[Category:日本眾議院議員
1996–2000](../Category/日本眾議院議員_1996–2000.md "wikilink")
[Category:日本眾議院議員
2000–2003](../Category/日本眾議院議員_2000–2003.md "wikilink")
[Category:日本眾議院議員
2003–2005](../Category/日本眾議院議員_2003–2005.md "wikilink")
[Category:日本眾議院議員
2005–2009](../Category/日本眾議院議員_2005–2009.md "wikilink")
[Category:日本眾議院議員
2009–2012](../Category/日本眾議院議員_2009–2012.md "wikilink")
[Category:日本眾議院議員
2012–2014](../Category/日本眾議院議員_2012–2014.md "wikilink")
[Category:日本眾議院議員
2014–2017](../Category/日本眾議院議員_2014–2017.md "wikilink")
[Category:山口縣選出日本眾議院議員](../Category/山口縣選出日本眾議院議員.md "wikilink")