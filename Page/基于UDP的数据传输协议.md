{{ Infobox_Software | name = UDT | logo = | screenshot = | caption = |
developer = 谷云洪 | latest_release_version = 4.10 |
latest_release_date =  | latest_preview_version = |
latest_preview_date = | operating_system =
[跨平台](../Page/跨平台.md "wikilink") | programming_language =
[C++](../Page/C++.md "wikilink") | genre = | license =
[BSD许可证](../Page/BSD许可证.md "wikilink") | website =
<http://udt.sourceforge.net/> }}

**基于UDP的数据传输协议**（，缩写：**UDT**）是一种[互联网数据传输协议](../Page/互联网.md "wikilink")。UDT的主要目的是支持高速[广域网上的海量数据传输](../Page/广域网.md "wikilink")，而[互联网上的标准数据传输协议](../Page/互联网.md "wikilink")[TCP在高带宽长距离网络上性能很差](../Page/TCP.md "wikilink")。

顾名思义，UDT建于[UDP之上](../Page/UDP.md "wikilink")，并引入新的拥塞控制和数据可靠性控制机制。UDT是面向连接的双向的应用层协议。它同时支持可靠的数据流传输和部分可靠的数据报传输。

由于UDT完全在UDP上实现，它也可以应用在除了高速数据传输之外的其它应用领域，例如[点到点技术](../Page/点到点技术.md "wikilink")（P2P），[防火墙穿透](../Page/防火墙.md "wikilink")，[多媒体数据传输等等](../Page/多媒体.md "wikilink")。

UDT由开源软件作者谷云洪在美国[伊利诺伊大学芝加哥分校攻读博士期间开发](../Page/伊利诺伊大学.md "wikilink")，并由他在毕业后继续维护和升级。UDT的开源软件可以在[SourceForge上获取](../Page/SourceForge.md "wikilink")。

## 外部链接

  - [UDT官方网站](http://udt.sourceforge.net/)
  - [UDT中文技术讨论区](https://sourceforge.net/forum/forum.php?forum_id=852996)

[Category:网际协议](../Category/网际协议.md "wikilink")