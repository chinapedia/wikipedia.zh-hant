**奧格爾县**（**Ogle County,
Illinois**）是[美國](../Page/美國.md "wikilink")[伊利諾伊州北部的一個縣](../Page/伊利諾伊州.md "wikilink")。面積1,977平方公里。根据[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口51,032人。縣治[俄勒岡](../Page/俄勒岡_\(伊利諾伊州\).md "wikilink")
（Oregon）。

成立於1836年1月16日，縣政府成立於1839年。縣名紀念[伊利諾伊準州民兵司令約瑟](../Page/伊利諾伊準州.md "wikilink")·奧格爾。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

[O](../Category/伊利諾州行政區劃.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.