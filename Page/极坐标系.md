[CircularCoordinates.png](https://zh.wikipedia.org/wiki/File:CircularCoordinates.png "fig:CircularCoordinates.png")

在[数学中](../Page/数学.md "wikilink")，**极坐标系**（）是一个[二维](../Page/二维.md "wikilink")[坐标系统](../Page/坐标系统.md "wikilink")。该坐标系统中任意位置可由一个**夹[角](../Page/角.md "wikilink")**和一段相对[原点](../Page/原点.md "wikilink")—极点的**距离**来表示。极坐标系的应用领域十分广泛，包括[数学](../Page/数学.md "wikilink")、[物理](../Page/物理.md "wikilink")、[工程](../Page/工程.md "wikilink")、[航海](../Page/航海.md "wikilink")、[航空以及](../Page/航空.md "wikilink")[机器人领域](../Page/机器人.md "wikilink")。在两点间的关系用夹角和距离很容易表示时，极坐标系便显得尤为有用；而在平面直角坐标系中，这样的关系就只能使用[三角函数来表示](../Page/三角函数.md "wikilink")。对于很多类型的曲线，极坐标方程是最简单的表达形式，甚至对于某些曲线来说，只有极坐标方程能够表示。

## 历史

[Hipparchos_1.jpeg](https://zh.wikipedia.org/wiki/File:Hipparchos_1.jpeg "fig:Hipparchos_1.jpeg")\]\]

众所周知，[希腊人最早使用了角度和弧度的概念](../Page/希腊人.md "wikilink")。天文学家[喜帕恰斯](../Page/喜帕恰斯.md "wikilink")（190-120
BC）制成了一张求各角所对[弦的弦长函数的表格](../Page/弦.md "wikilink")。并且，曾有人引用了他的极坐标系来确定恒星位置。在螺线方面，[阿基米德描述了他的著名的螺线](../Page/阿基米德.md "wikilink")，一个半径随角度变化的方程。希腊人作出了贡献，尽管最终并没有建立整个坐标系统。

关于是谁首次将极坐标系应用为一个正式的坐标系统，流传着有多种观点。关于这一问题的较详尽历史，[哈佛大学教授](../Page/哈佛大学.md "wikilink")[朱利安·科利奇](../Page/朱利安·科利奇.md "wikilink")（）的《极坐标系起源》\[1\]\[2\]作了阐述。[格雷瓜·德·聖-萬桑特](../Page/格雷瓜·德·聖-萬桑特.md "wikilink")（）和[博納文圖拉·卡瓦列里](../Page/博納文圖拉·卡瓦列里.md "wikilink")，被认为在几乎同时、并独立地各自引入了极坐标系这一概念。聖-萬桑特在1625年的私人文稿中进行了论述并发表于1647年，而卡瓦列里在1635进行了发表，而后又于1653年进行了更正。卡瓦列里首次利用极坐标系来解决一个关于[阿基米德螺线内的面积问题](../Page/阿基米德螺线.md "wikilink")。[布莱士·帕斯卡随后使用极坐标系来计算](../Page/布莱士·帕斯卡.md "wikilink")[抛物线的长度](../Page/抛物线.md "wikilink")。

在1671年写成，1736年出版的《流数术和无穷级数》（）一书中，[艾萨克·牛顿第一个将极坐标系应用于表示平面上的任何一点](../Page/艾萨克·牛顿.md "wikilink")。牛顿在书中验证了极坐标和其他九种坐标系的變换关系。在1691年出版的《博學通報》（，*Acta
eruditorum*）一书中[雅各布·伯努利正式使用定点和从定点引出的一条射线](../Page/雅各布·伯努利.md "wikilink")，定点称为极点，射线称为极轴。平面内任何一点的坐标都通过该点与定点的距离和与极轴的夹角来表示。伯努利通过极坐标系对曲线的曲率半径进行了研究。

实际上应用“极坐标”（）這個术语的是由[格雷古廖·豐塔納](../Page/格雷古廖·豐塔納.md "wikilink")（）开始的，并且被18世纪的意大利数学家所使用。该术语是由[喬治·皮科克](../Page/喬治·皮科克.md "wikilink")（）在1816年翻译[席維斯·拉克魯克斯](../Page/席維斯·拉克魯克斯.md "wikilink")（）的《微分學與積分學》（）\[3\]\[4\]\[5\]
一书时，被翻译为英语的。

[亚历克西斯·克莱罗和](../Page/亚历克西斯·克莱罗.md "wikilink")[莱昂哈德·欧拉被认为是将平面极坐标系扩展到三维空间的数学家](../Page/莱昂哈德·欧拉.md "wikilink")。

## 点的表示

正如所有的二维坐标系，极坐标系也有两个坐标轴：\(r\)（半径坐标）和\(\theta\)（角坐标、极角或[方位角](../Page/方位角.md "wikilink")，有时也表示为\(\phi\)或\(t\)）。\(r\)坐标表示与极点的距离，\(\theta\)坐标表示按逆时针方向坐标距离0°[射线](../Page/射线.md "wikilink")（有时也称作极轴）的角度，极轴就是在平面[直角坐标系中的x轴正方向](../Page/直角坐标系.md "wikilink")。\[6\]

比如，极坐标中的（3, 60°）表示了一个距离极点3个单位长度、和极轴夹角为60°的点。（−3, 240°）和(3,
60°)表示了同一点，因为该点的半径为在夹角射线反向延长线上距离极点3个单位长度的地方（240° −
180° = 60°）。

极坐标系中一个重要的特性是，平面直角坐标中的任意一点，可以在极坐标系中有无限种表达形式。通常来说，点（r, θ）可以任意表示为（*r*, θ ±
*n*×360°）或(−*r*, θ ± (2*n* +
1)180°)，这里*n*是任意整数。\[7\]如果某一点的*r*坐标为0，那么无论θ取何值，该点的位置都落在了极点上。

### 使用弧度单位

[Polar_graph_paper.svg](https://zh.wikipedia.org/wiki/File:Polar_graph_paper.svg "fig:Polar_graph_paper.svg")
极坐标系中的角度通常表示为角度或者[弧度](../Page/弧度.md "wikilink")，使用公式2π rad=
360°。具体使用哪一种方式，基本都是由使用场合而定。[航海方面经常使用角度来进行测量](../Page/航海.md "wikilink")，而[物理学的某些领域大量使用到了半径和圆周的比来作运算](../Page/物理.md "wikilink")，所以物理方面更倾向使用弧度。\[8\]

### 极坐标系与平面直角坐标系之间的變换

[Polar_to_cartesian.svg](https://zh.wikipedia.org/wiki/File:Polar_to_cartesian.svg "fig:Polar_to_cartesian.svg")
從极坐标\(r\)和\(\theta\)可以變換為[直角坐标](../Page/直角坐标.md "wikilink")：

\[r = \sqrt{y^2 + x^2} \quad\]（參閱[畢氏定理](../Page/畢氏定理.md "wikilink")）

\[\theta = \operatorname{atan2}(y, x) \quad\]（[atan2是已將](../Page/atan2.md "wikilink")[象限納入考量的](../Page/象限.md "wikilink")[反正切函數](../Page/反正切.md "wikilink")）
或

\[\theta =
\begin{cases}
\arctan(\frac{y}{x}) & \text{if } x > 0\\
\arctan(\frac{y}{x}) + \pi & \text{if } x < 0 \text{ and } y \ge 0\\
\arctan(\frac{y}{x}) - \pi & \text{if } x < 0 \text{ and } y < 0\\
\frac{\pi}{2} & \text{if } x = 0 \text{ and } y > 0\\
-\frac{\pi}{2} & \text{if } x = 0 \text{ and } y < 0\\
0 & \text{if } x = 0 \text{ and } y = 0
\end{cases}\]

从直角坐标\(x\)和\(y\)也可以變換為极坐标：

\[x = r \cos \theta\]

\[y = r \sin \theta\]

這方程式給出\(\theta\)在值域\((-\pi, \pi]\)的弧度。\[9\]改用角度單位，值域為\((-180^{\circ},180^{\circ}]\)。這些方程式假定極點是直角坐標系的原點\((0,0)\)，極軸為x-坐標軸，而y-坐標軸方向的弧度為\(+\pi/2\)，角度為\(+90^{\circ}\)。

大多數常用程式語言會特別設定一個函數，專門從\(x\)和\(y\)坐標計算出正確的角坐標\(\theta\)。例如，在[C語言裏](../Page/C語言.md "wikilink")，這函數標記為`atan2(y,x)`，在[Common
Lisp裏](../Page/Common_Lisp.md "wikilink")，標記為`(atan y
x)`。對於這兩種案例，計算結果是在值域\((-\pi, \pi]\)內的弧度。這\(\theta\)的數值是複函數[輻角的](../Page/複數.md "wikilink")[主值](../Page/主值.md "wikilink")（principal
value），注意到當\(x\)和\(y\)都等於零時，輻角沒有定義值；對於這案例，為了方便起見，將輻角設定為零。

假若需要，將角坐標\(\theta\)在值域\((-\pi, \pi]\)的數值加上\(\pi\)，則可得到在值域\([0,2\pi)\)的數值。

## 极坐标系方程

  - 函数：用极坐标系描述的[曲线方程称作](../Page/曲线.md "wikilink")*极坐标方程*，通常表示为*r*为自变量θ的[函数](../Page/函数.md "wikilink")。

<!-- end list -->

  - 对称：极坐标方程经常会表现出不同的[对称形式](../Page/对称.md "wikilink")，如果*r*(−θ) =
    *r*（θ），则曲线关于极点（0°/180°）对称，如果r(π−θ) =
    r(θ)，则曲线关于极点（90°/270°）对称，如果r(θ−α) =
    r(θ)，则曲线相当于从极点[逆时针方向](../Page/順時針方向.md "wikilink")[旋转α](../Page/旋转.md "wikilink")°。

### 圆

[circle_r=1.PNG](https://zh.wikipedia.org/wiki/File:circle_r=1.PNG "fig:circle_r=1.PNG")。\]\]
在极坐标系中，圆心在（*r*<sub>0</sub>, \(\varphi\)）半径为*a*的圆的一般方程为

\[r^2 - 2 r r_0 \cos(\theta - \varphi) + r_0^2 = a^2\]

特定情况：比如方程

\[r(\theta)=a\] 表示一个以极点为中心半径为*a*的圆。\[10\]

#### 導引

设圆的半径为\(r\)，圆心的极坐标为\((p_0,\alpha)\)，并變換为直角坐标：\((
p_0\cos\alpha,p_0\sin\alpha)\)。则圆上的点的直角坐标系方程为：

\[(x-p_0\cos\alpha)^2+(y-p_0\sin\alpha)^2=r^2\]

设圆上的点的极坐标为\((p,\beta)\)，则

\[x=p\cos\beta,\qquad y=p\sin\beta\]

因此，

\[p^2-2pp_0(\sin\beta\sin\alpha+\cos\beta\cos\alpha)+p_0^2=r^2\]，

化简为

\[p^2+p_0^2-2pp_0\cos(\beta-\alpha)=r^2\]

### 直线

过极点的射线方程：

\[\theta = \varphi\],
其中φ为射线的倾斜角。若m为[直角坐标系的射线的](../Page/直角坐标系.md "wikilink")[斜率](../Page/斜率.md "wikilink")，则有φ
= arctan *m*。任何不经过极点的直线都会与某条射线[垂直](../Page/垂直.md "wikilink")。\[11\]
这些在点（*r*<sub>0</sub>, φ）处的直线与射线θ = φ垂直，其方程为

\[r(\theta) = {r_0}\sec(\theta-\varphi)\].

### 玫瑰线

[rose_r=2sin(4theta).PNG](https://zh.wikipedia.org/wiki/File:rose_r=2sin\(4theta\).PNG "fig:rose_r=2sin(4theta).PNG")。\]\]
极坐标的玫瑰线（）是数学曲线中非常著名的曲线，看上去像花瓣，它只能用极坐标方程来描述，方程如下：

\[r(\theta) = a \cos k\theta\]或者

\[r(\theta) = a \sin k\theta\]
如果*k*是整数，当*k*是奇数时那么曲线将会是k个花瓣，当*k*是偶数时曲线将是2k个花瓣。如果*k*为非整数，将产生[圆盘状图形](../Page/圆盘.md "wikilink")，且花瓣数也为非整数。注意：该方程不可能产生4的倍数加2（如2，6，10……）个花瓣。变量*a*代表玫瑰线花瓣的长度。

### 阿基米德螺线

[Archimedian_spiral.PNG](https://zh.wikipedia.org/wiki/File:Archimedian_spiral.PNG "fig:Archimedian_spiral.PNG")。\]\]
[阿基米德螺线在极坐标里使用以下方程表示](../Page/阿基米德螺线.md "wikilink")：

\[r(\theta) = a+b\theta\]
改变参数*a*将改变螺线形状，*b*控制螺线间距离，通常其为常量。阿基米德螺线有两条螺线，一条θ\>0，另一条θ\<0。两条螺线在极点处平滑地连接。把其中一条翻转
90°/270°得到其镜像，就是另一条螺线。

### 圆锥曲线

[Elps-slr.png](https://zh.wikipedia.org/wiki/File:Elps-slr.png "fig:Elps-slr.png")，展示了[半正焦弦](../Page/半正焦弦.md "wikilink")\]\]
[圆锥曲线方程如下](../Page/圆锥曲线.md "wikilink")：

  -
    \(r = {l\over (1 - e \cos \theta)}\)

其中*l*表示[半正焦弦](../Page/椭圆#半正焦弦和极坐标.md "wikilink")，*e*表示[离心率](../Page/离心率.md "wikilink")。

如果*e* \< 1，曲线为[椭圆](../Page/椭圆.md "wikilink")，如果*e* =
1，曲线为[抛物线](../Page/抛物线.md "wikilink")，如果*e* \>
1，则表示[双曲线](../Page/双曲线.md "wikilink")。

  -
    \(r = {ep\over (1 - e\cos \theta)}\)

其中*e*表示[离心率](../Page/离心率.md "wikilink")，*p*表示[焦点到准线的距离](../Page/焦点到准线的距离.md "wikilink")。

### 其他曲线

由于坐标系统是基于圆环的，所以许多有关曲线的方程，极坐标要比直角坐标系（笛卡尔形式）简单得多。比如[伯努利雙紐線](../Page/伯努利雙紐線.md "wikilink")，[蚶線](../Page/蚶線.md "wikilink")（），还有[心臟線](../Page/心臟線.md "wikilink")。

## 复数

[复数的通常](../Page/复数.md "wikilink")[矩形形式为](../Page/矩形.md "wikilink")*a* +
*bi*，在极坐标中也可以表示为两种不同的方式：

1.  \(r(\cos\theta+i\sin\theta)\)，簡寫為\(r \operatorname{cis} \theta\)
2.  \(r e^{i\theta}\)

等同于[欧拉公式](../Page/欧拉公式.md "wikilink")。<ref>

</ref>` `

复数在直角坐标系和极坐标系的變换通过以下公式实现：

\[a = r \cos \theta\]

\[b = r \sin \theta\]

  -
    其中\(r = \sqrt{a^2 + b^2}\)

复数的[乘法](../Page/乘法.md "wikilink")、[除法以及](../Page/除法.md "wikilink")[指数以及开方运算](../Page/指数.md "wikilink")，在极坐标中会比在直角坐标中容易得多，他们分别是：

  - 乘法：\((r \operatorname{cis} \theta) * (R \operatorname{cis} \varphi) = rR \operatorname{cis} (\theta+\varphi)\)
  - 除法：\(\frac{r \operatorname{cis} \theta}{R \operatorname{cis} \varphi} = \frac{r}{R} \operatorname{cis} (\theta-\varphi)\)
  - 指数（[棣莫弗定理](../Page/棣莫弗定理.md "wikilink")，De Moivre's formula）:
    \((r \operatorname{cis} \theta)^n = r^n \operatorname{cis} (n\theta)\)

## 微积分

[微积分可适用于极坐标系下表达的等式](../Page/微积分.md "wikilink")。\[12\]\[13\]

### 矢量微积分

[矢量微积分也可以应用到极坐标](../Page/矢量微积分.md "wikilink")。对于平面运动，令\(\mathbf{r}\)为位置矢量\((r\cos(\theta),r\sin(\theta))\)，由*r*与随时间*t*变化的\(\theta\)表达，\(\hat{\mathbf{r}}\)是\(\mathbf{r}\)方向上的单位矢量，\(\hat{\boldsymbol\theta}\)是以\(\mathbf{r}\)为起始顺时针旋转的角度单位矢量。第一和第二个位置的表达式是：

\[\frac{d\mathbf{r}}{dt} = \dot r\hat{\mathbf{r}} + r\dot\theta\hat{\boldsymbol\theta},\]

\[\frac{d^2\mathbf{r}}{dt^2} = (\ddot r - r\dot\theta^2)\hat{\mathbf{r}} + (r\ddot\theta + 2\dot r \dot\theta)\hat{\boldsymbol\theta}.\]
令\(\mathbf{A}\)为被一条连接焦点与曲线上一点的线所划分出的区域，则\(d\mathbf{A}\)就是由\(\mathbf{r}\)和\(d\mathbf{r}\)所构[平行四边形区域的一半](../Page/平行四边形.md "wikilink")。

\[dA = \begin{matrix}\frac{1}{2}\end{matrix} |\mathbf{r} \times d\mathbf{r}|\],
所以，整个区域就是\(d\mathbf{A}\)关于时间的积分。

## 极坐标与球坐标和圆柱坐标的联系

极坐标系可被扩展到三维空间中，形成[圆柱坐标系和](../Page/圆柱坐标系.md "wikilink")[球坐标系两个不同的坐标系](../Page/球坐标系.md "wikilink")。

### 圆柱坐标系

[CylindricalCoordinates.png](https://zh.wikipedia.org/wiki/File:CylindricalCoordinates.png "fig:CylindricalCoordinates.png")
与将[直角坐标系扩展为三维的方法相似](../Page/直角坐标系.md "wikilink")，*[圆柱坐标系](../Page/圆柱坐标系.md "wikilink")*是在二维极坐标系的基础上增添了第三条用于测量高于平面的点的高度的坐标所构成的。这第三条坐标通常表示为*h*。所以圆柱坐标表示为（*r*,
*θ*, *h*）。

通过以下公式，可以從直角坐标變換為圆柱坐标：

\[{x}={r} \,\cos\theta\]

\[{y}={r} \, \sin\theta\]

\[{z}={h}\]

### 球坐标系

[Spherical_Coordinates.png](https://zh.wikipedia.org/wiki/File:Spherical_Coordinates.png "fig:Spherical_Coordinates.png")
[球坐标系也可以运用坐标](../Page/球坐标系.md "wikilink")（ρ, φ,
θ）扩展为三维，其中ρ是距离球心的距离，φ是距离z轴的角度（称作余纬度或顶角，角度从0到180°），θ是距离x轴的角度（与极坐标中一样）。这个坐标系被称作球坐标系，与用于地球的[经度和](../Page/经度.md "wikilink")[纬度相似](../Page/纬度.md "wikilink")，纬度就是余角φ，取决于δ＝90°－φ，经度可通过*l*=θ-180°算得。\[14\]

通过以下公式，可以從直角坐标變換為球坐标：

\[x =\rho \, \sin\phi \, \cos\theta\]

\[y =\rho \, \sin\phi \, \sin\theta\]

\[z =\rho \, \cos\phi\]

## 应用

### 定位和导航

极坐标通常被用于[导航](../Page/导航.md "wikilink")，作为旅行的目的地或方向可以作为从所考虑的物体的距离和角度。例如，[飞机使用极坐标的一个略加修改的版本进行导航](../Page/飞机.md "wikilink")。这个系统中是一般的用于导航任何种类中的一个系统，在0°射线一般被称为航向360，并且角度是以[顺时针方向继续](../Page/顺时针方向.md "wikilink")，而不是逆时针方向，如同在数学系统那样。航向360对应[地磁北极](../Page/地磁北极.md "wikilink")，而航向90，180，和270分别对应于磁东，南，西。\[15\]因此，一架飞机向正东方向上航行5[海里将是在航向](../Page/海里.md "wikilink")90（[空中交通管制读作](../Page/空中交通管制.md "wikilink")090）上航行5个单位。\[16\]

### 建模

有径向对称的系统提供了极坐标系的自然设置，中心点充当了极点。这种用法的一个典型例子是在适用于径向对称的[水井时候的](../Page/水井.md "wikilink")。有径向力的系统也适合使用极坐标系。这些系统包括了服从[平方反比定律的](../Page/平方反比定律.md "wikilink")[引力场](../Page/引力场.md "wikilink")，以及有点源的系统，如[无线电](../Page/无线电.md "wikilink")[天线](../Page/天线.md "wikilink")。

#### 行星运动的开普勒定律

[kepler-second-law.png](https://zh.wikipedia.org/wiki/File:kepler-second-law.png "fig:kepler-second-law.png")

极坐标提供了一个表达在[引力场中开普勒行星运行定律的自然的方法](../Page/引力场.md "wikilink")。开普勒第一定律表明，环绕一颗恒星运行的行星轨道形成了一个[椭圆](../Page/椭圆.md "wikilink")，这个椭圆的一个焦点在[质心上](../Page/质心.md "wikilink")。上面所给出的二次曲线部分的等式可用于表达这个椭圆。开普勒第二定律表明，连接行星和它所环绕的恒星的线在等时间间隔所划出的区域是面积相等的，即\(d\mathbf{A}\over dt\)是常量。这些等式可由[牛顿运动定律推得](../Page/牛顿运动定律.md "wikilink")。在[开普勒行星运动定律中有相关运用极坐标的详细推导](../Page/开普勒行星运动定律.md "wikilink")。

## 相关内容

  -
  - [圆柱坐标系](../Page/圆柱坐标系.md "wikilink")

  - [球坐标系](../Page/球坐标系.md "wikilink")

## 參考資料

<div class="references-small">

  - 普遍的參考資料

<!-- end list -->

  -
  -
  -
<!-- end list -->

  - 特定的參考資料

</div>

[J](../Category/坐标系.md "wikilink")

1.  [The MacTutor History of Mathematics archive: Coolidge's Origin of
    Polar
    Coordinates](http://www-history.mcs.st-and.ac.uk/Extras/Coolidge_Polars.html)
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14.
15.
16.