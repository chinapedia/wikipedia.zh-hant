**比格爾海峽**（西班牙语：**Canal
Beagle**）位于[南美洲南端](../Page/南美洲.md "wikilink")[智利](../Page/智利.md "wikilink")[火地岛](../Page/火地岛.md "wikilink")[烏斯懷亞的西南方](../Page/烏斯懷亞.md "wikilink")，是一條從東部的[大西洋](../Page/大西洋.md "wikilink")，跨过[阿根廷](../Page/阿根廷.md "wikilink")、[智利兩國到西部](../Page/智利.md "wikilink")[太平洋的水道](../Page/太平洋.md "wikilink")，全長三百二十公里，最寬處約十公里，最狹窄處只有一公里。比格爾海峽景色優美，海浪不大，海峽中凸出的幾個小島，是企鵝、海獅、海狗和各種鳥類的棲息與繁殖之地，智利和阿根廷对此有领土争议。

比格爾海峽是以[小獵犬號帆船的名字命名的](../Page/小獵犬號.md "wikilink")。1826年至1830年间，小猎犬号被派往南美洲进行其第一次水文调查和探险时，发现此海峡\[1\]。生物学家[达尔文参加了小猎犬号的第二次探险](../Page/达尔文.md "wikilink")，也经过此海峡。

## 注释

<references/>

[Category:火地島](../Category/火地島.md "wikilink")
[Category:太平洋海峽](../Category/太平洋海峽.md "wikilink")
[Category:大西洋海峽](../Category/大西洋海峽.md "wikilink")
[Category:智利海峽](../Category/智利海峽.md "wikilink")
[Category:阿根廷海峽](../Category/阿根廷海峽.md "wikilink")
[Category:跨國海峽](../Category/跨國海峽.md "wikilink")
[Category:智利峽灣](../Category/智利峽灣.md "wikilink")
[Category:阿根廷峽灣](../Category/阿根廷峽灣.md "wikilink")
[Category:麥哲倫-智利南極大區](../Category/麥哲倫-智利南極大區.md "wikilink")
[Category:阿根廷-智利邊界](../Category/阿根廷-智利邊界.md "wikilink")

1.