**日本新年**指的是[日本以及世界各地](../Page/日本.md "wikilink")[大和族的](../Page/大和族.md "wikilink")[新春](../Page/新春.md "wikilink")，是大和族一年當中最重要的[节日](../Page/节日.md "wikilink")，又称**正月**（しょうがつ、しょうがち）。正月本来是[農曆的一月](../Page/農曆.md "wikilink")，[明治维新后改用](../Page/明治维新.md "wikilink")[格里历](../Page/格里历.md "wikilink")，則用于称新历的1月，截止1月31日都被稱為正月。農曆的正月則稱為「旧正月」，现在日本人大多數地方不庆祝舊[正月](../Page/正月.md "wikilink")（除[沖繩縣及一些農村地方外](../Page/沖繩縣.md "wikilink")）。

## 簡介

[Meiji_New_Year_Celebration.Wittig_Collection.pntg-20.detail.jpg](https://zh.wikipedia.org/wiki/File:Meiji_New_Year_Celebration.Wittig_Collection.pntg-20.detail.jpg "fig:Meiji_New_Year_Celebration.Wittig_Collection.pntg-20.detail.jpg")至[明治初期日本人在家中過年的情況](../Page/明治.md "wikilink")\]\]
日本新年傳統上在十二月已經開始準備，最早是由十二月初八[臘八節開始就準備過年事務](../Page/臘八節.md "wikilink")，有些則由十二月十三開始，因此這天被称为。而新年由元日至[正月十五才結束](../Page/正月十五.md "wikilink")，稱為“松之內”），現在由於主要節慶活動主要集中在），因此现在“三が日”和“松の内”多混用。自[江戶時代](../Page/江戶時代.md "wikilink")[寬文二年](../Page/寬文.md "wikilink")（[1662年正月初六](../Page/1662年.md "wikilink")），[江户幕府在](../Page/江户幕府.md "wikilink")[江户城下命令人们於正月初七日之前结束賀年装饰的通知](../Page/江户城.md "wikilink")，人们照此仿效，这样的习俗便渐渐以关东为中心扩展开来，把賀年活動從正月十五結束缩短到[人日就結束](../Page/人日.md "wikilink")，而[日本小正月](../Page/日本小正月.md "wikilink")（[元宵節](../Page/元宵節.md "wikilink")）的習俗[左義長也被禁止](../Page/左義長.md "wikilink")。江户幕府末期的考证家[喜田川守貞研究得出此举应该是为了预防火灾](../Page/喜田川守貞.md "wikilink")。此外，有些地區也有将新年截至正月二十（小填倉），把正月二十称为[二十日正月或](../Page/二十日正月.md "wikilink")[骨正月的](../Page/骨正月.md "wikilink")。

相對於正月十五的“小正月”，整個新年又被称为“大正月”、“大年”，“男の正月”，小正月也被称为“小年”或“女の正月”。

## 歷史

日本新年的歷史一般認為始自[飛鳥時代](../Page/飛鳥時代.md "wikilink")，當時[推古天皇從](../Page/推古天皇.md "wikilink")[中國](../Page/中國.md "wikilink")[唐朝引進當時的](../Page/唐朝.md "wikilink")[農曆](../Page/農曆.md "wikilink")[元嘉曆](../Page/元嘉曆.md "wikilink")，並仿效唐人過節的習俗，包括新春習俗，自此日本人開始在農曆正月慶祝新年，但亦有說法指在六世紀前已經有慶祝正月的習俗。日本的新年最初是以[祭祖和向](../Page/祭祖.md "wikilink")[歲神祈求](../Page/歲神.md "wikilink")[豐收的節日](../Page/豐收.md "wikilink")，後來在宮廷和民間一直發展，現在所見的傳統習俗是江戶後期的形式。日本受中國文化影響很深，因此有些傳統與中國非常相似。日本農曆在明治六年（1873年）一月一日起停用，改用西曆，原來的農曆便稱之為「舊曆」，新春日期也隨之改為西曆元旦起的三天國定假日，因此現今日本人的新年是西曆的一月一日。

## 習俗

### 歲前節俗

[Toshinoichi.JPG](https://zh.wikipedia.org/wiki/File:Toshinoichi.JPG "fig:Toshinoichi.JPG")[札幌市](../Page/札幌市.md "wikilink")[中央区](../Page/中央區_\(札幌市\).md "wikilink")附近的年宵市場\]\]
過年前人們會去置辦[年貨](../Page/年貨.md "wikilink")、大掃除、佈置家居。又會舉行[忘年會](../Page/忘年會.md "wikilink")，人們紛紛回家與家人團聚。

#### 辦年貨

在十二月下旬，日本各地都會有稱為「歲之市」的（歳の市）[年宵市場售賣各種賀年用品](../Page/年宵市場.md "wikilink")。傳統上歲之市設於[神社或](../Page/神社.md "wikilink")[寺廟範圍內](../Page/寺廟.md "wikilink")，現代則沒有限制。

#### 忘年会

[忘年会是公司或者朋友们的聚餐](../Page/忘年会.md "wikilink")，是从[室町时代留下来的习惯](../Page/室町时代.md "wikilink")。忘年会的意思是忘掉一年的辛苦的宴会。过去人们一边喝酒一边咏[和歌](../Page/和歌.md "wikilink")，现在多唱卡拉OK。每年的这个时节，街上总会有很多喝得醉醺醺，衣服搭在肩上，领带歪歪斜斜的的中年男人。

#### 大扫除

日本人的传统的大扫除也在十二月十三就開始，由打掃[神龕開始](../Page/神龕.md "wikilink")，一则为了清洁家居，二则也有洗去霉气的意思。只是现在人们生活都繁忙，13日那天顶多就把神棚上的灰尘掸一下，到12月下旬放了假才真正开始大扫除。

#### 佈置年飾

[Kadomatsu_M1181.jpg](https://zh.wikipedia.org/wiki/File:Kadomatsu_M1181.jpg "fig:Kadomatsu_M1181.jpg")
清潔好家居就會佈置，傳統上年飾要在十二月廿九之前佈置好，這是因為日語中「九」與「苦」同音，若這天才完成佈置年飾就不吉利；若果至[除夕才佈置好](../Page/除夕.md "wikilink")，則稱為「一日飾」，對神明欠缺誠意\[1\]。

日本常見的年飾有[門松](../Page/門松.md "wikilink")、[注連繩](../Page/注連繩.md "wikilink")、[破魔矢](../Page/破魔矢.md "wikilink")、[草耙](../Page/草耙.md "wikilink")、[年畫](../Page/年畫.md "wikilink")、[鏡餅等](../Page/鏡餅.md "wikilink")。門松放置於家门口，以求让神灵降臨自家。注連繩則用以阻擋邪靈入侵。破魔矢一般掛在神龕上方，寓意破除妖魔。草耙日語稱為「熊手」，寓意抓住福氣，作用類似朝鮮半島新年的\[2\]。

年畫在日本稱為「正月繪」（），江戶時代以來的傳統年畫多是稱為的[木刻版畫](../Page/木刻版畫.md "wikilink")，題材為傳統吉祥圖案如[七福神](../Page/七福神.md "wikilink")、[松竹梅等](../Page/松竹梅.md "wikilink")。

#### 年夜飯

[Toshikosi-soba20111231.jpg](https://zh.wikipedia.org/wiki/File:Toshikosi-soba20111231.jpg "fig:Toshikosi-soba20111231.jpg")、[天婦羅炸蝦的年越蕎麥](../Page/天婦羅.md "wikilink")\]\]

[大晦日即大除夕](../Page/大晦日.md "wikilink")，这一天大家在中午为止就把所有的过年的准备工作做完，当天傍晚一家人团聚着开始吃[年夜飯过年](../Page/年夜飯.md "wikilink")。年夜飯中有[雜煮](../Page/雜煮.md "wikilink")，此外由江戶時代開始，年夜飯必定包括[麵條](../Page/麵條.md "wikilink")，通常是[蕎麥麵](../Page/蕎麥.md "wikilink")，稱為「」，寓意長壽\[3\]。

#### 守歲

這天人們會[守歲](../Page/守歲.md "wikilink")，迎接年神，要至新年[日出可以去睡](../Page/日出.md "wikilink")。傳說除夕晚早睡的話，就會多生[皺紋和](../Page/皺紋.md "wikilink")[白髮](../Page/白髮.md "wikilink")\[4\]。

#### 除夕敲鐘

除夕子夜，全国各处的大小佛寺庙都会响起108下的钟声，稱為。在子夜时分听到这些远处同时响起的钟声。这108下钟声原来是有特别意思的：第一种说法是这108下钟声代表驱走108个魔鬼，第二种说法是这108下钟声代表了108位神佛，第三种说法是这108下钟声代表代表了一年十二个月、二十四节气、七十二候加起来的总数。无论是哪一种原因，都反映了日本人通过这108下钟声驱走霉运、祈求神佛保佑的愿望。

### 新年习俗

#### 祝贺

[An_Instance_Of_New_Year_Card_In_Japan.JPG](https://zh.wikipedia.org/wiki/File:An_Instance_Of_New_Year_Card_In_Japan.JPG "fig:An_Instance_Of_New_Year_Card_In_Japan.JPG")\]\]
一如[漢字文化圈其他地區的新年](../Page/漢字文化圈.md "wikilink")，人們會互相祝賀，又有向親友或一年关照过自己的人送[年贺状](../Page/年贺状.md "wikilink")（[賀年卡](../Page/賀年卡.md "wikilink")）的习俗原来年初都要进行名为**御年始**（お年始）的串亲访友[拜年活动](../Page/拜年.md "wikilink")。1990年代末以来随着手机的普及，通过电邮，短信等进行拜年的渐渐增多。另外，在新年伊始会面的人一般会说“あけましておめでとう（ございます）”来相互寒暄祝賀。

#### 祭祀

人們在新年時會祭祀先祖，也有迎接年神祈求五谷丰登的活动，与[夏之盆](../Page/日本盂蘭盆節.md "wikilink")（夏の盆[盂兰盆节](../Page/盂兰盆节.md "wikilink")）相对应，也有的习俗。但是，随着佛教影响的加强，祭祀活动融合了佛教的[盂兰盆](../Page/盂兰盆.md "wikilink")，成为人们供养先祖的活动。

#### 御年玉（压岁钱）

新年期間，長輩要给未成年晚輩一些[壓歲钱](../Page/壓歲钱.md "wikilink")，日语叫做御年玉（お年玉）。御年玉傳統上用白色的[封套](../Page/紅包袋.md "wikilink")，近年則有不同圖案的封套。金額視乎晚輩的年齡和與自己的親疏關係而定。

#### 饮食

[Oseti.jpg](https://zh.wikipedia.org/wiki/File:Oseti.jpg "fig:Oseti.jpg")\]\]
新年吃[年糕是日本的传统](../Page/年糕.md "wikilink")，象征着坚韧与希望，日本年糕的食用方法有放在炉火上烤制，也有加進湯裡食用。杂煮则是一种放年糕的汤。在关东用清水煮。关西用白味噌煮，年糕是圆形的。吃杂煮就是把头天供给神吃的东西，撤下来加上山珍海味一起煮，表示得到了神的恩惠。

日本的年菜稱為[御节料理](../Page/御节料理.md "wikilink")，傳統上御节料理先用于供神，后又撤下供人们食用，意味着人们从神那里得到了食物。做御节料理的还有一个原因就是因为年神在家里，所以不便在厨房里弄出很多丁丁当当的嘈杂声，所以三天不开火。繁忙的家庭主妇在这几天也能得到休息。御节料理的摆法很有讲究，专用的饭盒里一共有四层,多半是些图吉利的菜色。例如，红白萝卜丝代表红白至喜。海带卷代表欢乐、愉快。[黑豆代表勤劳工作](../Page/黑豆.md "wikilink")。[鲱鱼籽代表子孙昌盛](../Page/鲱鱼籽.md "wikilink")。

新年的第一天要喝[屠苏酒](../Page/屠苏酒.md "wikilink")，一年内防病消灾的意思。屠苏是一种[中药](../Page/中药.md "wikilink")。配方内包括白术、桔梗、山椒、防风、肉桂、大黄、小豆等。喝屠苏酒起源于宫廷，后来传至民间。

### 初诣

[Kumanonachitaisha8476.jpg](https://zh.wikipedia.org/wiki/File:Kumanonachitaisha8476.jpg "fig:Kumanonachitaisha8476.jpg")

新年大家一般都会去[神社或者](../Page/神社.md "wikilink")[佛寺裡参拜](../Page/佛寺.md "wikilink")，稱為初诣也称初参或初社。钟声过后，信徒就开始到寺庙里拜佛许多妇女去神社的时都会穿上[和服](../Page/和服.md "wikilink")，围上毛皮围巾。人们喝着热乎乎的甜米酒，在有节奏的鼓乐声中或者[神官祓厄](../Page/神官.md "wikilink")，或者亲朋好友聊天谈笑，迎接新春的到来。大部分人选择去的是神社。电视里每年都有各地的着名神社参拜情况的实况转播。

### 初夢

[初夢是指新年做的第一個夢](../Page/初夢.md "wikilink")，如能做一个吉祥美梦，夢到富士山與老鷹與茄子，据说会给人带来全年的好运。

#### 新年開筆

[新年開筆是指新年初次寫](../Page/新年開筆.md "wikilink")[書法](../Page/日本書道.md "wikilink")，傳統上是年初二進行。這習俗最初流行於[貴族階層](../Page/貴族.md "wikilink")，所寫的內容是具有吉祥意義的[漢詩或](../Page/漢詩.md "wikilink")[對聯](../Page/對聯.md "wikilink")，後來流傳至民間則可以書寫任何寓意吉祥的字句，或簡單寫上代表新年的字詞。

#### 新年遊戲

[Kusakabe_Kimbei_152_Girls.JPG](https://zh.wikipedia.org/wiki/File:Kusakabe_Kimbei_152_Girls.JPG "fig:Kusakabe_Kimbei_152_Girls.JPG")
新年親友相聚時會玩遊戲，常見的遊戲有各種傳統[棋類](../Page/棋類.md "wikilink")、[歌留多](../Page/歌留多.md "wikilink")、[花札](../Page/花札.md "wikilink")、、[陀螺](../Page/陀螺.md "wikilink")、[手鞠](../Page/手鞠.md "wikilink")、[羽根突](../Page/羽根突.md "wikilink")、放[風箏等](../Page/風箏.md "wikilink")。其中陀螺和風箏傳統上是男孩玩的遊戲，女孩則玩羽根突\[5\]。

#### 舞獅

新年期間在全國各地皆有[舞獅活動](../Page/舞獅.md "wikilink")，寓意驅邪除厄\[6\]。

## 新年中的節俗

### 人日

年初七為[人日](../Page/人日.md "wikilink")，傳統上這天是所有人類的生日，人們會吃[七草粥](../Page/七草粥.md "wikilink")。日本人相信喝七草粥可以在新的一年中无病息灾。最初的七草粥是由七种谷物（米、麦、粟米、黍米、稗子、芝麻、小豆）煮成的粥，镰仓时代以后慢慢地变成了现在的芹菜、荠菜、繁缕、菘菜、萝卜菜、御形、佛座等七种绿色植物。吃七草粥也有包含着盼望暖和的春天早些到来的心情。

### 鏡開與小正月

[Kagami_mochi_by_midorisyu.jpg](https://zh.wikipedia.org/wiki/File:Kagami_mochi_by_midorisyu.jpg "fig:Kagami_mochi_by_midorisyu.jpg")
[镜开原来是武士社会的风习](../Page/镜开.md "wikilink")，是祈愿延年长命的仪式。将供在神前的[镜饼取下来分成一块块](../Page/镜饼.md "wikilink")，意味着正月的结束和新的一年的开始。因为忌讳用切这个字，所以用了开运的开就叫镜开。傳統上是在小正月或正月二十進行，也有其他如年初七\[7\]，[江戶时代曾經因為要避開](../Page/江戶时代.md "wikilink")[德川家光的](../Page/德川家光.md "wikilink")[死忌而定在正月十一日这天为镜开的日子](../Page/死忌.md "wikilink")\[8\]。[京都及附近地區還有在年初四鏡開的習俗](../Page/京都.md "wikilink")\[9\]。

镜饼是不可以用刀之类的东西切开的，要用手或者槌来掰开、砸开。这天一般的家里，一边祈祷一家的圆圆满满\[10\]，讲用炭火烤熟的年糕放在热腾腾的[红豆沙或加進雜煮里吃](../Page/红豆沙.md "wikilink")。镜开以后，也象征着年过完了，回到平常的日子里，等待着下一个新年的到来。

正月十五是小正月，傳統上是祈求豐收的日子，這天會吃[紅豆沙和拆除年飾的日子](../Page/紅豆沙.md "wikilink")，一些地區有稱為[左義長的火祭](../Page/左義長.md "wikilink")。

## 現況

現時日本除了保留一些傳統習俗外，也有一些近現代新興的習慣。

### 福袋

[Fukubukuro-5000yen-2010.jpg](https://zh.wikipedia.org/wiki/File:Fukubukuro-5000yen-2010.jpg "fig:Fukubukuro-5000yen-2010.jpg")
每逢新年期间，日本各大[百货公司都会推出各种](../Page/百货公司.md "wikilink")[福袋](../Page/福袋.md "wikilink")，价格大多数在一万到五万円之间。这些福袋，象征着好兆头。装在福袋里的商品，价值肯定超过福袋的价格。福袋里面是什么东西大家都不知道，但可以肯定的是里面的商品会比福袋本身的价格高，深受家庭主妇的欢迎。

### 賀年影視節目

日本的電視台會在新年期間播放賀年節目，最著名的是[紅白歌合戰](../Page/紅白歌合戰.md "wikilink")。

此外，各電影院也會上映一些應節的[賀歲片](../Page/賀歲片.md "wikilink")，日語稱為「正月映畫」（）。

## 禁忌

在日本，如過去一年中遇到丧事，一般不去庆祝正月，并会寄出丧中欠礼的卡片，这时收到卡片的亲朋好友也不能向他们寄年贺状。

## 新年休假

[Pachinkokadomatsu-jan6-2011.jpg](https://zh.wikipedia.org/wiki/File:Pachinkokadomatsu-jan6-2011.jpg "fig:Pachinkokadomatsu-jan6-2011.jpg")
1月1日是公共假日。政府部门从12月29日到1月3日为假期，一般企业也多以此为标准。[银行等金融机关多从](../Page/银行.md "wikilink")12月31日到1月3日放假，所以系统维护常需要较长时间。[公共交通机关于此期间一般情况下会照平日时刻表运行](../Page/公共交通.md "wikilink")。

一方面，零售业私人店铺20世纪80年代前半期之前多于1月5-7日休业。直到1980年左右，百货商店和超市等大型商店甚至有在正月前三天休假的。但是，随着24小时营业便利店登场等生活样式的变化，开店时间提前，90年代以后仅1月1日休业，随后两天开始大多数商店都缩短营业的时间。大型商店等店铺另一方面多连1月1日也在营业。总之，大多数店铺在4日左右都会恢复正常营业。

## 參見

  - [華人新年](../Page/華人新年.md "wikilink")
  - [朝鮮新年](../Page/朝鮮新年.md "wikilink")
  - [越南新年](../Page/越南新年.md "wikilink")

## 参考文献

[日本新年](../Category/日本新年.md "wikilink")

1.  [正月飾りの正しい飾り方・時期を知っていますか？](http://u-note.me/note/47487559)

2.
3.  [年越そば](http://www.echizenya.co.jp/mini/colum/tosikosi.htm)

4.
5.  [凧上げと羽根突き--陰陽五行説による正月](http://www.relnet.co.jp/relnet/brief/r18-8.htm)

6.  [獅子舞](http://www.echizenya.co.jp/mini/mini.html)

7.  [allabout
    「鏡開き」をまるごと解明！](http://allabout.co.jp/aa/special/sp_newyear/contents/10050/220640/)


8.  [神社と神道](http://jinja.jp/modules/chishiki/index.php?content_id=8)

9.  [奈良新聞 天理教](http://www.nara-np.co.jp/20100105154257.html)

10. [日本文化いろは辞典](http://iroha-japan.net/iroha/A01_event/02_kagami.html)