**威靈頓維多利亞大學**（[毛利語](../Page/毛利語.md "wikilink")：**Te Whare Wānanga o Te
Ūpoko o Te Ika a Māui**，），是新西兰著名大学。

## 历史

於1897年成立於[新西兰](../Page/新西兰.md "wikilink")[惠灵顿](../Page/惠灵顿.md "wikilink")，當年正值[維多利亞女王登基六十週年紀念](../Page/維多利亞女王.md "wikilink")，因此該大學便以「維多利亞」命名。大学的四位创始教授：Thomas
Easterfield, Hugh MacKenzie, Richard Maclaurin 和John Rankine Brown
于1899抵达惠灵顿并开始教学。与[奥克兰大学](../Page/奥克兰大学.md "wikilink")，[坎特伯雷大学和](../Page/坎特伯雷大学.md "wikilink")[奥塔哥大学一样](../Page/奥塔哥大学.md "wikilink")，学校在1961年以前是[新西兰大学的组成学院之一](../Page/新西兰大学.md "wikilink")。新西兰大学于1961年解散，学位授予权下放给各组成学院。

2004年，新西兰政府承认维多利亚大学为全国前三所研究性大学之一（仅次于奥克兰大学和坎特伯雷大学）。[亚洲周刊](../Page/亚洲周刊.md "wikilink")2001年把维多利亚大学列入新西兰最好的四所大学之一。[上海交通大学](../Page/上海交通大学.md "wikilink")2006年的世界名校排名也把维多利亚大学列入[亚太地区最好的百所大学](../Page/亚太地区.md "wikilink")。同时学校在[QS世界大学排名的评价系统中各項目均获得最高的五星评级](../Page/QS世界大学排名.md "wikilink")。

2017年，维多利亚大学曾计划改名为惠灵顿大学（University of
Wellington），避免在国际上与其他维多利亚大学混淆，在众多校友及政商界人士的反对中，2018年12月18日被时任教育部部长Chris
Hipkins一票否决。

## 組織

[Old_Government_Buildings,_Wellington.JPG](https://zh.wikipedia.org/wiki/File:Old_Government_Buildings,_Wellington.JPG "fig:Old_Government_Buildings,_Wellington.JPG")
惠灵顿维多利亚大学由六个学院组成，分布在惠灵顿地区的4个校区。他们分別是建筑学院、教育学院、人文社会学院、商业与管理学院、法学院及科学院。商业与管理学院位于惠灵顿市中心Pipitea校区，紧接商业区，拥有一座12层的教学办公楼(卢瑟福教学楼Rutherford
House)。法学院在亚太地区享有盛名，被视为新西兰乃至全大洋洲最好的法学院之一。
其早期的毕业生在新西兰和[澳大利亚的法律界和政界皆有出色的表现](../Page/澳大利亚.md "wikilink")。

维多利亚大学现有20380名学生。其中3400名是[国际学生或](../Page/国际学生.md "wikilink")[交换学生](../Page/交换学生.md "wikilink")。很多学生来自[中国](../Page/中国.md "wikilink")，[马来西亚](../Page/马来西亚.md "wikilink")，[韩国](../Page/韩国.md "wikilink")，[德国和](../Page/德国.md "wikilink")[美国](../Page/美国.md "wikilink")。

## 参考文献

## 外部連結

  - [惠灵顿维多利亚大学的网站](http://www.victoria.ac.nz)

[惠灵顿维多利亚大学](../Category/惠灵顿维多利亚大学.md "wikilink")
[Category:惠灵顿](../Category/惠灵顿.md "wikilink")
[Category:1897年創建的教育機構](../Category/1897年創建的教育機構.md "wikilink")