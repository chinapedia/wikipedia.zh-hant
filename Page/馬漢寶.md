**馬漢寶**（），字**襄武**，[安徽省](../Page/安徽省.md "wikilink")[渦陽縣人](../Page/渦陽縣.md "wikilink")，生於[湖北](../Page/湖北.md "wikilink")[漢口](../Page/漢口.md "wikilink")（今湖北省[武漢市](../Page/武漢市.md "wikilink")），[中華民國](../Page/中華民國.md "wikilink")[法律學者](../Page/法律.md "wikilink")，[中華民國前](../Page/中華民國.md "wikilink")[司法院大法官](../Page/司法院大法官.md "wikilink")。其父[馬壽華曾任](../Page/馬壽華.md "wikilink")[司法院秘書長](../Page/司法院.md "wikilink")、[行政法院院長](../Page/中華民國最高行政法院.md "wikilink")、[公務員懲戒委員會委員長及總統府](../Page/公務員懲戒委員會.md "wikilink")[國策顧問等職](../Page/國策顧問.md "wikilink")。

## 生平

馬漢寶[小學在](../Page/小學.md "wikilink")[上海就讀](../Page/上海.md "wikilink")，因家住西方人群居之處，從小就習慣使用[英語](../Page/英語.md "wikilink")。小學畢業後，先後就讀於南洋中學、復旦高中，後尊父意入[復旦大學法律系習法](../Page/復旦大學.md "wikilink")。1947年時，馬漢寶自復旦大學肄業，隨父母來台灣，入[台灣大學法律系](../Page/台灣大學.md "wikilink")。1950年畢業時，因成績為全級最佳，被校長[傅斯年留為助教](../Page/傅斯年.md "wikilink")，後迭升講師、副教授、[教授等職](../Page/教授.md "wikilink")，主要教授[憲法及](../Page/憲法.md "wikilink")[國際私法等課程](../Page/國際私法.md "wikilink")。

1964年，馬漢寶進入[美國](../Page/美國.md "wikilink")[哈佛大學研究](../Page/哈佛大學.md "wikilink")；1971年，受聘為位於[西雅圖華盛頓大學法學院客座教授](../Page/西雅圖華盛頓大學.md "wikilink")；其後在1981年至1982年間，亦曾應聘為[奧地利科學院及](../Page/奧地利科學院.md "wikilink")[維也納大學法學部客座教授](../Page/維也納大學.md "wikilink")，並在西德[慕尼黑大學及位在西柏林的](../Page/慕尼黑大學.md "wikilink")[柏林自由大學講學](../Page/柏林自由大學.md "wikilink")。1991年，應聘為[香港大學法學院客座教授](../Page/香港大學.md "wikilink")；1993年，應聘為[法國學術院講座](../Page/法蘭西學術院.md "wikilink")；1994年，應聘任[加拿大](../Page/加拿大.md "wikilink")[英屬哥倫比亞大學法學院客座教授](../Page/英屬哥倫比亞大學.md "wikilink")；1995年，任[紐約](../Page/紐約.md "wikilink")[哥倫比亞大學法學院客座教授](../Page/哥倫比亞大學.md "wikilink")；1997年，任[聖路易華盛頓大學法學院訪問教授](../Page/聖路易斯華盛頓大學.md "wikilink")；1999年，任[北京大學法學院訪問教授](../Page/北京大學.md "wikilink")。國際私法及中西法哲學或思想比較為其所長。

另外，1972年，馬漢寶曾擔任[考試院](../Page/考試院.md "wikilink")[考試委員一職](../Page/考試委員.md "wikilink")，1982年被補提名為中華民國第四屆大法官，同年就職，1985年連任，1994年退職。1984年至2002年間並曾任[中央研究院評議員](../Page/中央研究院.md "wikilink")。

馬漢寶是虔誠的基督教信徒，是[聖公會教友](../Page/臺灣聖公會.md "wikilink")，目前仍積極參與教會聖工。2015年9月21日，馬漢寶獲聖公會臺灣教區主教賴榮信派立為「臺灣教區終身法政法制顧問」，典禮在[聖約翰座堂舉行](../Page/聖約翰座堂.md "wikilink")。[1](https://web.archive.org/web/20090322110020/http://www.stjohn.org.tw/)。

## 註釋

  -
    1馬漢寶先生在不少資料上習慣以[陰曆](../Page/陰曆.md "wikilink")[十一月二十七日代之](../Page/十一月廿七.md "wikilink")。

## 參考資料

  - 《大法官釋憲史料》，[司法院編](../Page/司法院.md "wikilink")，1998年。
  - 《法律哲理與制度．馬漢寶教授八秩華誕祝壽論文集》，元照出版公司，2006年。
  - [臺灣聖公會聖約翰座堂網站](https://web.archive.org/web/20090322110020/http://www.stjohn.org.tw/)

[Category:中華民國總統府國策顧問](../Category/中華民國總統府國策顧問.md "wikilink")
[Category:司法院大法官](../Category/司法院大法官.md "wikilink")
[Category:中華民國法學家](../Category/中華民國法學家.md "wikilink")
[Category:哥倫比亞大學教師](../Category/哥倫比亞大學教師.md "wikilink")
[Category:香港大學教授](../Category/香港大學教授.md "wikilink")
[Category:柏林自由大學教師](../Category/柏林自由大學教師.md "wikilink")
[Category:慕尼黑大學教師](../Category/慕尼黑大學教師.md "wikilink")
[Category:維也納大學教師](../Category/維也納大學教師.md "wikilink")
[Category:國立臺灣大學教授](../Category/國立臺灣大學教授.md "wikilink")
[Category:哈佛大學校友](../Category/哈佛大學校友.md "wikilink")
[Category:國立臺灣大學法律學院校友](../Category/國立臺灣大學法律學院校友.md "wikilink")
[Category:復旦大學校友](../Category/復旦大學校友.md "wikilink")
[Category:臺灣聖公宗教徒](../Category/臺灣聖公宗教徒.md "wikilink")
[Category:台灣戰後安徽移民](../Category/台灣戰後安徽移民.md "wikilink")
[Category:台灣戰後湖北移民](../Category/台灣戰後湖北移民.md "wikilink")
[Category:亳州人](../Category/亳州人.md "wikilink")
[Category:漢口人](../Category/漢口人.md "wikilink")
[H漢](../Category/馬姓.md "wikilink")
[Category:第5屆中華民國考試委員](../Category/第5屆中華民國考試委員.md "wikilink")
[Category:華盛頓大學教師](../Category/華盛頓大學教師.md "wikilink")
[Category:上海市復旦中學校友](../Category/上海市復旦中學校友.md "wikilink")
[Category:复旦大学校友](../Category/复旦大学校友.md "wikilink")
[Category:第6屆中華民國考試委員](../Category/第6屆中華民國考試委員.md "wikilink")