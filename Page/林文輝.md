[Lam_Man_Fai.png](https://zh.wikipedia.org/wiki/File:Lam_Man_Fai.png "fig:Lam_Man_Fai.png")
**林文輝**（），[香港](../Page/香港.md "wikilink")[黃大仙上邨](../Page/黃大仙上邨.md "wikilink")（龍上）[區議員](../Page/區議員.md "wikilink")。

他在[1999年香港區議會選舉及](../Page/1999年香港區議會選舉.md "wikilink")[2003年香港區議會選舉分別擊敗](../Page/2003年香港區議會選舉.md "wikilink")[陳炎光](../Page/陳炎光.md "wikilink")（1999年）和[前綫的](../Page/前綫_\(1996年\).md "wikilink")[劉山青](../Page/劉山青.md "wikilink")（2003年），[2007年香港區議會選舉自動當選連任](../Page/2007年香港區議會選舉.md "wikilink")，他曾任市政局議員，[黃大仙區議會主席](../Page/黃大仙區議會.md "wikilink")，前[民建聯成員](../Page/民建聯.md "wikilink")。他曾經參與[2004年立法會選舉](../Page/2004年立法會選舉.md "wikilink")，排在[陳婉嫻名單的第二名](../Page/陳婉嫻.md "wikilink")。
\[1\]\[2\]

[2011年香港區議會選舉中](../Page/2011年香港區議會選舉.md "wikilink")，讓出龍上選區由[陳婉嫻出選](../Page/陳婉嫻.md "wikilink")，自己則轉戰東美選區落敗。

[2015年香港區議會選舉參選龍上選區](../Page/2015年香港區議會選舉.md "wikilink")，成功當選重返議會，成功領導工聯會保住陳婉嫻空缺。

## 相關

  - [2004年香港立法會選舉](../Page/2004年香港立法會選舉.md "wikilink")

先後擔任勞工子弟中學教師，主要任教物理及數學。

現在擔任校長室秘書。

## 参考文献

<div class="references-small">

<references />

</div>

[Category:黃大仙區議員](../Category/黃大仙區議員.md "wikilink")
[Category:香港中文大學校友](../Category/香港中文大學校友.md "wikilink")
[Category:前市政局議員](../Category/前市政局議員.md "wikilink")
[Category:民主建港協進聯盟成員](../Category/民主建港協進聯盟成員.md "wikilink")
[Wen文輝](../Category/林姓.md "wikilink")

1.  [香港民建聯
    林文輝](http://www.dab.org.hk/tr/main.jsp?content=category-content.jsp&categoryId=1395)
2.  [黃大仙區議會主席**林文輝**主持造勢大會](http://www.takungpao.com:82/news/2003-11-16/GW-198282.htm)