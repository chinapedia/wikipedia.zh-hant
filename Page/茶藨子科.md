**茶藨子科**\[1\]也叫醋栗科（Ribesiaceae），只有一[属](../Page/属.md "wikilink")—醋栗属（*Ribesa*），或叫茶藨子属，大约有150[种](../Page/种.md "wikilink")，主要分布在北[温带地区和](../Page/温带.md "wikilink")[南美西部温带地区](../Page/南美.md "wikilink")。[中国约](../Page/中国.md "wikilink")45种，主要分布在西部和东北。

本[科植物都是灌木](../Page/科.md "wikilink")，具针刺；单[叶丛生](../Page/叶.md "wikilink")，无托叶；[花常退化为单性](../Page/花.md "wikilink")，总状花序或单花，[花瓣](../Page/花瓣.md "wikilink")4-5，小或鳞片状；[果实为](../Page/果实.md "wikilink")[浆果](../Page/浆果.md "wikilink")，有的品种作为栽培植物果实可食用，有的品种可以作为观赏[植物](../Page/植物.md "wikilink")。

1981年的[克朗奎斯特分类法将其分入](../Page/克朗奎斯特分类法.md "wikilink")[蔷薇目](../Page/蔷薇目.md "wikilink")，1998年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
分类法将其列入新设立的](../Page/APG_分类法.md "wikilink")[虎耳草目](../Page/虎耳草目.md "wikilink")。

## 物種

  - [黑加仑](../Page/黑加仑.md "wikilink")
  - [紅加侖](../Page/紅加侖.md "wikilink")
  - [华中茶藨子](../Page/华中茶藨子.md "wikilink")
  - [紫花茶藨子](../Page/紫花茶藨子.md "wikilink")
  - [英吉利茶藨子](../Page/英吉利茶藨子.md "wikilink")
  - [刺果茶藨子](../Page/刺果茶藨子.md "wikilink")
  - [天山茶藨子](../Page/天山茶藨子.md "wikilink")
  - [长果茶藨子](../Page/长果茶藨子.md "wikilink")
  - [矮醋栗](../Page/矮醋栗.md "wikilink")
  - [阔叶茶藨子](../Page/阔叶茶藨子.md "wikilink")
  - [东北茶藨子](../Page/东北茶藨子.md "wikilink")
  - [水葡萄茶藨子](../Page/水葡萄茶藨子.md "wikilink")
  - [美丽茶藨子](../Page/美丽茶藨子.md "wikilink")
  - [四川茶藨子](../Page/四川茶藨子.md "wikilink")
  - [台湾茶藨子](../Page/台湾茶藨子.md "wikilink")
  - [糖茶藨子](../Page/糖茶藨子.md "wikilink")
  - [桂叶茶藨子](../Page/桂叶茶藨子.md "wikilink")
  - [东方茶藨子](../Page/东方茶藨子.md "wikilink")
  - [四川蔓茶藨子](../Page/四川蔓茶藨子.md "wikilink")
  - [美洲茶藨子](../Page/美洲茶藨子.md "wikilink")
  - [青海茶藨子](../Page/青海茶藨子.md "wikilink")
  - [细枝茶藨子](../Page/细枝茶藨子.md "wikilink")
  - [密刺茶藨子](../Page/密刺茶藨子.md "wikilink")
  - [长白茶藨子](../Page/长白茶藨子.md "wikilink")
  - [簇花茶藨子](../Page/簇花茶藨子.md "wikilink")
  - [康边茶藨子](../Page/康边茶藨子.md "wikilink")
  - [长序茶藨子](../Page/长序茶藨子.md "wikilink")
  - [湖南茶藨子](../Page/湖南茶藨子.md "wikilink")
  - [曲萼茶藨子](../Page/曲萼茶藨子.md "wikilink")
  - [圆叶茶藨子](../Page/圆叶茶藨子.md "wikilink")
  - [滇中茶藨子](../Page/滇中茶藨子.md "wikilink")
  - [矮茶藨子](../Page/矮茶藨子.md "wikilink")
  - [高茶藨子](../Page/高茶藨子.md "wikilink")
  - [陕西茶藨子](../Page/陕西茶藨子.md "wikilink")
  - [光叶茶藨子](../Page/光叶茶藨子.md "wikilink")
  - [天全茶藨子](../Page/天全茶藨子.md "wikilink")
  - [西藏茶藨子](../Page/西藏茶藨子.md "wikilink")
  - [红萼茶藨子](../Page/红萼茶藨子.md "wikilink")
  - [小果茶藨子](../Page/小果茶藨子.md "wikilink")
  - [欧洲醋栗](../Page/欧洲醋栗.md "wikilink")
  - [阿尔泰醋栗](../Page/阿尔泰醋栗.md "wikilink")
  - [花茶藨子](../Page/花茶藨子.md "wikilink")
  - [光萼茶藨子](../Page/光萼茶藨子.md "wikilink")
  - [石生茶藨子](../Page/石生茶藨子.md "wikilink")
  - [多花茶藨子](../Page/多花茶藨子.md "wikilink")
  - [鄂西茶藨子](../Page/鄂西茶藨子.md "wikilink")
  - [富蕴茶藨子](../Page/富蕴茶藨子.md "wikilink")
  - [尖叶茶藨子](../Page/尖叶茶藨子.md "wikilink")
  - [华西茶藨子](../Page/华西茶藨子.md "wikilink")
  - [渐尖茶藨子](../Page/渐尖茶藨子.md "wikilink")
  - [香茶藨子](../Page/香茶藨子.md "wikilink")
  - [绿花茶藨子](../Page/绿花茶藨子.md "wikilink")
  - [冰川茶藨子](../Page/冰川茶藨子.md "wikilink")
  - [毛茶藨子](../Page/毛茶藨子.md "wikilink")
  - [双刺茶藨子](../Page/双刺茶藨子.md "wikilink")
  - [裂叶茶藨子](../Page/裂叶茶藨子.md "wikilink")

## 注释

## 外部链接

  - [浆果产业](http://www.uga.edu/)

[\*](../Category/茶藨子科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")
[Category:水果](../Category/水果.md "wikilink")
[Category:耐旱植物](../Category/耐旱植物.md "wikilink")

1.