**德國商業銀行大廈**為一幢位於[德國](../Page/德國.md "wikilink")[法蘭克福的](../Page/法蘭克福.md "wikilink")[摩天大樓](../Page/摩天大樓.md "wikilink")，建於1994年至1997年，[诺曼·福斯特事务所设计](../Page/诺曼·福斯特.md "wikilink")，曾為[歐盟境內最高的摩天大樓](../Page/歐盟.md "wikilink")，直到2013年被[英國](../Page/英國.md "wikilink")[倫敦的](../Page/倫敦.md "wikilink")[碎片大廈所取代](../Page/碎片大廈.md "wikilink")。至今仍是[德國第一高樓](../Page/德國.md "wikilink")。

## 概况

德國商業銀行大廈高259米，56層，提供121,000[平方公尺的辦公空間為](../Page/平方公尺.md "wikilink")[德國商業銀行總部](../Page/德國商業銀行.md "wikilink")，擁有冬天公園、自然採光設施與空氣流通設施。其中，二樓的員工餐廳在週一至週五中午11點至下午2點30分，開放一般訪客入內用餐。

## 画廊

Image:Commerzbank Tower from Main Tower.jpg
Image:Frankfurt.Commerzbanktower.wmt.jpg Image:Commerzbank-7.jpg
Image:Panorama 46.JPG <File:JMatern_060320_8433-8436_WC.jpg>
Image:Frankfurt.Commerbanktower.innenhof.wmt.jpg

## 外部連結

  - [Der Commerzbank-Tower auf
    www.emporis.de](https://web.archive.org/web/20070311075203/http://www.emporis.com/ge/wm/bu/?id=109691)
  - [Artikel aus Zeitschrift *Intelligente
    Architektur*](https://web.archive.org/web/20070703113247/http://www.architektur.tu-darmstadt.de/powerhouse/db/248,id_122,s_Projects.fb15)

[Category:法蘭克福摩天大樓](../Category/法蘭克福摩天大樓.md "wikilink")
[Category:250米至299米高的摩天大樓](../Category/250米至299米高的摩天大樓.md "wikilink")
[Category:諾曼·福斯特設計的建築](../Category/諾曼·福斯特設計的建築.md "wikilink")
[Category:德国建筑之最](../Category/德国建筑之最.md "wikilink")