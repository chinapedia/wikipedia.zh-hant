**曹节**（），[字](../Page/表字.md "wikilink")**汉丰**，[南阳郡](../Page/南阳郡.md "wikilink")[新野县人](../Page/新野县.md "wikilink")，是[汉灵帝时有权势的](../Page/汉灵帝.md "wikilink")[宦官](../Page/宦官.md "wikilink")，曾参与诛杀[窦武](../Page/窦武.md "wikilink")、[陈蕃](../Page/陈蕃.md "wikilink")。

## 生平

据《[后汉书](../Page/后汉书.md "wikilink")》记载，其本[魏郡人](../Page/魏郡.md "wikilink")，出身于世吏二千石的家族。[顺帝初](../Page/汉顺帝.md "wikilink")，曹节以西园骑迁小黄门，[桓帝时](../Page/汉桓帝.md "wikilink")，为[中常侍](../Page/中常侍.md "wikilink")，[奉车都尉](../Page/奉车都尉.md "wikilink")。

[建宁元年](../Page/建宁_\(东汉\).md "wikilink")（168年），因迎立灵帝有功，被封为长安乡侯。当时[窦太后临朝](../Page/窦妙.md "wikilink")，大将军窦武与太傅陈蕃密谋诛杀宦官，曹节于是和长乐五官史[朱瑀](../Page/朱瑀.md "wikilink")、从官史[共普](../Page/共普.md "wikilink")、张亮等十七人矫诏以长乐食监[王甫为黄门令](../Page/王甫_\(东汉\).md "wikilink")，控制灵帝与窦太后，并派兵诛杀窦武、陈蕃等，事后曹节迁为长乐卫尉，并被封为育阳侯，增邑三千户；王甫迁为中常侍，依旧为[黄门令](../Page/黄门令.md "wikilink")；朱瑀被封为都乡侯，千五百户；共普、张亮等五人各三百户；其他十一人则都被封为关内侯，岁食租二千斛。

[建宁二年](../Page/建宁_\(东汉\).md "wikilink")（169年），曹节病困，诏拜为车骑将军，病愈罢，复为中常侍，位[特进](../Page/特进.md "wikilink")，不久被任命为[大长秋](../Page/大长秋.md "wikilink")。五年（172年）六月初十，在窦武失败后失势被[幽禁的窦太后去世](../Page/幽禁.md "wikilink")，曹节、王甫仇恨窦武，提出以贵人之礼安葬窦太后及改以[冯贵人配享桓帝](../Page/冯贵人_\(汉桓帝\).md "wikilink")，因廷尉[陈球](../Page/陈球_\(东汉\).md "wikilink")、太尉[李咸等力争而未果](../Page/李咸.md "wikilink")。[熹平元年](../Page/熹平.md "wikilink")（172年），曹节与王甫等共同诬陷桓帝弟勃海王[刘悝谋反](../Page/刘悝.md "wikilink")，刘悝被诛杀后，以功受封者有十二人，王甫被封为冠军侯，曹节亦增邑四千六百户，并前七千六百户，父兄子弟皆为公卿列校、牧守令长，布满天下。[光和二年](../Page/光和.md "wikilink")（179年）为[尚书令](../Page/尚书令.md "wikilink")。

光和四年（181年），曹节逝世，死后被追赠为[车骑将军](../Page/车骑将军.md "wikilink")。

女婿[冯芳后来成为](../Page/冯芳.md "wikilink")[西園八校尉之一](../Page/西園八校尉.md "wikilink")。

## 参考文献

  - [《后汉书·卷七十八·宦者列传第六十八》](http://www.guoxue.com/shibu/24shi/hhansu/hhsu_085.htm)

[Category:東漢宦官](../Category/東漢宦官.md "wikilink")
[Category:东汉中常侍](../Category/东汉中常侍.md "wikilink")
[Category:东汉都尉](../Category/东汉都尉.md "wikilink")
[Category:东汉长乐卫尉](../Category/东汉长乐卫尉.md "wikilink")
[Category:东汉车骑将军](../Category/东汉车骑将军.md "wikilink")
[Category:东汉特进](../Category/东汉特进.md "wikilink")
[Category:东汉大长秋](../Category/东汉大长秋.md "wikilink")
[Category:尚书令](../Category/尚书令.md "wikilink")
[Category:东汉追赠车骑将军](../Category/东汉追赠车骑将军.md "wikilink")
[Category:东汉列侯](../Category/东汉列侯.md "wikilink")
[Category:新野人](../Category/新野人.md "wikilink")
[J](../Category/曹姓.md "wikilink")