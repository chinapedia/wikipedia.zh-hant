**内务府**（）為[清朝的一个官署名稱](../Page/清朝.md "wikilink")，是总管皇室宫禁大小事务的机构。

## 歷史

内务府根源于[满族早期社会的](../Page/满族.md "wikilink")[包衣阿哈制度](../Page/包衣.md "wikilink")。包衣是“家的”的[满语音译](../Page/满语.md "wikilink")，阿哈則為奴才、奴僕\[1\]，即為家奴之意。[八旗制度产生时](../Page/八旗制度.md "wikilink")，包衣作为八旗成员的一部分而被编入包衣[牛录](../Page/牛录.md "wikilink")。随着满族社会的发展和清王朝封建君主制的确立，皇属包衣牛录的职责和地位也发生了变化，向具有宫廷服务性质的机构——[内府转化](../Page/内府.md "wikilink")，这就是内务府的雏形。

清入关后，由于清宫服务范围的扩大和历代封建王朝[宦官制度的影响](../Page/宦官.md "wikilink")，内务府一度被[宦官机构](../Page/宦官.md "wikilink")——[十三衙门取代](../Page/十三衙门.md "wikilink")。但是十三衙门这一机构不能适应满族统治集团的政治需要，随着清王朝在全国统治的巩固，十三衙门也就必然重新被内务府所取代。

《钦定总管内务府现行则例》载，“国初设立[内务府](../Page/内务府.md "wikilink")。[顺治十一年](../Page/顺治.md "wikilink")（1654年）裁，置[十三衙门](../Page/十三衙门.md "wikilink")。十八年裁十三衙门，仍置内务府。”\[2\]

## 职责

清代内务府的职责是“奉天子之家事”，管理宫禁事务。其成员由内务府上三旗「[镶黄旗](../Page/镶黄旗.md "wikilink")、[正黄旗](../Page/正黄旗.md "wikilink")、[正白旗](../Page/正白旗.md "wikilink")」的十五个[包衣](../Page/包衣.md "wikilink")[佐领](../Page/佐领.md "wikilink")、十八个[旗鼓佐领](../Page/旗鼓佐领.md "wikilink")、两个[朝鲜佐领](../Page/朝鲜佐领.md "wikilink")、一个[回子佐领和三十个](../Page/回子佐领.md "wikilink")[内管领的包衣人及太监组成](../Page/内管领.md "wikilink")，其机构组织兼容了清初内务府和[十三衙门两种制度的内容和特点](../Page/十三衙门.md "wikilink")，并最终形成了以七司三院为主干兼辖其他四十余衙门的庞大的宫廷服务机构。清代内务府是清代国家机构中职官人数最多、机构组织最为庞大的衙门，在维护清朝统治和专制皇权方面起到了十分重要的作用。

## 机构设置

内务府衙门分内务府堂及所属七司、三院等五十多个部门，总称**总管内务府衙门**，其最高官员为[总管内务府大臣](../Page/总管内务府大臣.md "wikilink")，特任，无定额。內務府總管為內務府之主官，品等為正二品，下設如會計等七個司，功能職務為管理出納，財務收支，祭祀禮儀等。

### 内务府堂

内务府及总管大臣的办公处所。下设[月官处](../Page/月官处.md "wikilink")、[督催所](../Page/督催所.md "wikilink")、[销算房](../Page/销算房.md "wikilink")、[翻译房](../Page/翻译房.md "wikilink")、[档案房及](../Page/档案房.md "wikilink")[本房等办事机构](../Page/本房.md "wikilink")。

## 宮殿監

[宮殿監](../Page/宮殿監.md "wikilink")：設都領侍一員「二品或三品或四品花翎，隨皇帝、太后喜好」掌宫中六十九處太監\[3\]。俗稱敬事房總管。

### 七司

  - [广储司](../Page/广储司.md "wikilink")，职若外朝[户部](../Page/户部.md "wikilink")，掌本司所属六库仓储用度事务，并验收其余诸司所属庄园钱粮地租及其余贡物，下设六库、七作、二房。
      - 六库：[银库](../Page/银库.md "wikilink")、[皮库](../Page/皮库.md "wikilink")、[瓷库](../Page/瓷库.md "wikilink")、[缎库](../Page/缎库.md "wikilink")、[衣库](../Page/衣库.md "wikilink")、[茶库](../Page/茶库.md "wikilink")
      - 七作：[银作](../Page/银作.md "wikilink")、[铜作](../Page/铜作.md "wikilink")、[染作](../Page/染作.md "wikilink")、[衣作](../Page/衣作.md "wikilink")、[绣作](../Page/绣作.md "wikilink")、[花作](../Page/花作.md "wikilink")、[皮作](../Page/皮作.md "wikilink")
      - 二房：[帽房](../Page/帽房.md "wikilink")、[针线房](../Page/针线房.md "wikilink")
  - [都虞司](../Page/都虞司.md "wikilink")，职若外朝[兵部](../Page/兵部.md "wikilink")，掌内务府属武官兵丁考选任用事宜，并掌渔猎采捕等事，下设[东档房](../Page/东档房.md "wikilink")、[西档房等办事机构](../Page/西档房.md "wikilink")。
  - [掌仪司](../Page/掌仪司.md "wikilink")，职若外朝[礼部](../Page/礼部.md "wikilink")，掌内廷祭祀礼仪，并掌国内宗教事务，下设[果房](../Page/果房.md "wikilink")、[神房](../Page/神房.md "wikilink")、[中和乐处](../Page/中和乐处.md "wikilink")、[僧录司](../Page/僧录司.md "wikilink")、[道录司](../Page/道录司.md "wikilink")、[东档房](../Page/东档房.md "wikilink")、[西档房](../Page/西档房.md "wikilink")、[本房等办事机构](../Page/本房.md "wikilink")
  - [会计司](../Page/会计司.md "wikilink")，职若外朝户部、[吏部](../Page/吏部.md "wikilink")，掌内府帑项出纳及所属庄园田亩之事，并掌内廷女官、宦官选用给养事宜。
  - [营造司](../Page/营造司.md "wikilink")，职若外朝[工部](../Page/工部.md "wikilink")，掌宫廷营造修缮事务，下设七库、三作。
      - 七库：[木库](../Page/木库.md "wikilink")、[铁库](../Page/铁库.md "wikilink")、[房库](../Page/房库.md "wikilink")、[器库](../Page/器库.md "wikilink")、[薪库](../Page/薪库.md "wikilink")、[炭库](../Page/炭库.md "wikilink")、[圆明园薪炭库](../Page/圆明园薪炭库.md "wikilink")
      - 三作：[铁作](../Page/铁作.md "wikilink")、[漆作](../Page/漆作.md "wikilink")、[花炮作](../Page/花炮作.md "wikilink")
  - [庆丰司](../Page/庆丰司.md "wikilink")，掌内廷及上三旗所属牛羊畜牧事务。
  - [慎刑司](../Page/慎刑司.md "wikilink")，职若外朝[刑部](../Page/刑部.md "wikilink")，掌审拟上三旗刑狱。

### 三院

  - [上驷院](../Page/上驷院.md "wikilink")，掌御用马匹。
  - [武备院](../Page/武备院.md "wikilink")，掌内廷禁旅所用仪仗武器，下设[北鞍库](../Page/北鞍库.md "wikilink")、[南鞍库](../Page/南鞍库.md "wikilink")、[甲库](../Page/甲库.md "wikilink")、[毡库](../Page/毡库.md "wikilink")。
  - [奉宸苑](../Page/奉宸苑.md "wikilink")，掌园囿离宫及天下驻跸行宫。

### 内务府属其他机构

  - 三织造处：[江宁织造](../Page/江宁织造.md "wikilink")、[苏州织造](../Page/苏州织造.md "wikilink")、[杭州织造](../Page/杭州织造.md "wikilink")
  - [织染局](../Page/织染局.md "wikilink")
  - [绮华馆](../Page/绮华馆.md "wikilink")
  - [打牲乌拉处](../Page/打牲乌拉处.md "wikilink")

### 内三旗参领处及包衣各营

  - [内三旗参领处](../Page/内三旗参领处.md "wikilink")
  - [内三旗包衣营](../Page/内三旗包衣营.md "wikilink")

### 其它

  - [养鹰鹞处及](../Page/养鹰鹞处.md "wikilink")[内外养狗处](../Page/内外养狗处.md "wikilink")
  - [御茶膳房](../Page/御茶膳房.md "wikilink")
  - [雍和宫](../Page/雍和宫.md "wikilink")
  - [中正殿](../Page/中正殿.md "wikilink")
  - [升平署](../Page/升平署.md "wikilink")
  - 陵寝官「[盛京陵寝事务总管大臣](../Page/盛京陵寝事务总管大臣.md "wikilink")、[东陵总管大臣](../Page/东陵总管大臣.md "wikilink")、[西陵总管大臣](../Page/西陵总管大臣.md "wikilink")」
  - [内三旗银两庄头处](../Page/内三旗银两庄头处.md "wikilink")
  - [内管领处](../Page/掌关防管理内管领事务处.md "wikilink")「[掌关防处](../Page/掌关防管理内管领事务处.md "wikilink")」：下设[内饽饽房](../Page/内饽饽房.md "wikilink")、[外饽饽房](../Page/外饽饽房.md "wikilink")、[酒醋房](../Page/酒醋房.md "wikilink")、[菜库](../Page/菜库.md "wikilink")、[器皿库](../Page/器皿库.md "wikilink")、[车库](../Page/车库.md "wikilink")、[管理苏拉车辆处](../Page/管理苏拉车辆处.md "wikilink")
  - [官三仓](../Page/官三仓.md "wikilink")
  - [恩丰仓](../Page/恩丰仓.md "wikilink")
  - [官房租库](../Page/官房租库.md "wikilink")
  - [总理工程处](../Page/总理工程处.md "wikilink")
  - [张家口外群牧处](../Page/张家口外群牧处.md "wikilink")
  - [牺牲所](../Page/牺牲所.md "wikilink")
  - [管辖番役处](../Page/管辖番役处.md "wikilink")
  - [御鸟枪处和](../Page/御鸟枪处.md "wikilink")[内火药库](../Page/内火药库.md "wikilink")
  - [圆明园](../Page/圆明园.md "wikilink")
  - [畅春园](../Page/畅春园.md "wikilink")
  - [清漪园](../Page/清漪园.md "wikilink")
  - [静明园](../Page/静明园.md "wikilink")
  - [静宜园](../Page/静宜园.md "wikilink")
  - [御船处](../Page/御船处.md "wikilink")
  - [热河行宫](../Page/热河行宫.md "wikilink")
  - [汤泉行宫](../Page/汤泉行宫.md "wikilink")
  - [盘山行宫](../Page/盘山行宫.md "wikilink")
  - [黄新庄行宫](../Page/黄新庄行宫.md "wikilink")
  - [三大殿](../Page/故宫三大殿.md "wikilink")「[太和殿](../Page/太和殿.md "wikilink")、[中和殿](../Page/中和殿.md "wikilink")、[保和殿](../Page/保和殿.md "wikilink")」
  - [宁寿宫](../Page/宁寿宫.md "wikilink")
  - 管理[慈宁宫](../Page/慈宁宫.md "wikilink")、[寿康宫处](../Page/寿康宫.md "wikilink")，[慈宁宫花园处](../Page/慈宁宫花园.md "wikilink")
  - [御药房](../Page/御药房.md "wikilink")
  - [文渊阁](../Page/文渊阁.md "wikilink")
  - [武英殿修书处](../Page/武英殿修书处.md "wikilink")
  - [御书处](../Page/御书处.md "wikilink")
  - [养心殿造办处](../Page/养心殿造办处.md "wikilink")
  - [咸安宫官学](../Page/咸安宫官学.md "wikilink")
  - [蒙古官学](../Page/蒙古官学.md "wikilink")
  - [景山官学](../Page/景山官学.md "wikilink")
  - [长房官学](../Page/长房官学.md "wikilink")
  - [办理回人事务佐领处及](../Page/办理回人事务佐领处.md "wikilink")[回缅官学](../Page/回缅官学.md "wikilink")
  - [回子官学](../Page/回子官学.md "wikilink")
  - [敬事房康熙皇帝在位时规定由内务府总管宫廷事务](../Page/敬事房.md "wikilink"),并设立敬事房作为懲處太监的机构，使宦官不敢造次。清代始創。\[4\]

## 经费来源

  - 部库的皇室经费：

<!-- end list -->

  -
    清朝规定，每年[户部都要撥銀六十萬兩以上](../Page/户部.md "wikilink")\[5\]给与内库作为皇室的开支费用，道光十年起，户部指撥粵海關之三十萬兩，光绪十九年上谕令户部按年另筹五十萬兩,合计每年共一百四十萬兩。\[6\]
    ,这是常例拨款；另外，根据皇帝和宫中事务，如帝-{后}-私用的交進銀\[7\]、修缮陵寝園林行宮、皇帝大婚、帝-{后}-万寿圣节等，亦可获得户部等巨額专款，如同治大婚各機關就撥近千萬兩與清皇室\[8\]，慈禧六旬万寿庆典亦花費約五百三十萬兩\[9\]。

<!-- end list -->

  - 来自盐业的收入：

<!-- end list -->

  -
    皇帝通过内务府亲信包衣出任本属户部官差的盐政官，通过各种渠道剥削盐商，索取财物，以充实内库。

<!-- end list -->

  - 来自[榷关的收入](../Page/榷关.md "wikilink")：

<!-- end list -->

  -
    与盐政官无二，榷关官差同样为内务府包衣所垄断，\[10\]使国家税收源源不断进入皇家内库。

<!-- end list -->

  - 贡品：

<!-- end list -->

  -
    包括各省[土贡](../Page/土贡.md "wikilink")、[外藩贡品以及](../Page/外藩贡品.md "wikilink")[采办贡品与官员的](../Page/采办贡品.md "wikilink")[献纳](../Page/献纳.md "wikilink")。

<!-- end list -->

  - 其它：

<!-- end list -->

  -
    包括没收、罚赎、[捐官等收入](../Page/捐官.md "wikilink")。

<!-- end list -->

  - 商业活动：

<!-- end list -->

  -
    如[人参售卖](../Page/人参.md "wikilink")、皇室消费不完的物品如珠玉等的变卖以及恩赏、借贷营运“生息银两”。\[11\]

<!-- end list -->

  - [議罪銀](../Page/議罪銀.md "wikilink")：

<!-- end list -->

  -
    議罪銀與大清律例中的贖刑「納贖、收贖、贖罪」不同。

## 经费支出

1.  帝-{后}-日常膳食和服御物品的消耗；
2.  赏赐；
3.  节日庆典如皇太-{后}-、皇帝万寿、皇帝大婚等；
4.  修缮宫殿、苑囿、陵寝以及祭祀；
5.  出巡；
6.  衙门办公费和官员差役人员的薪资。

## 參考文獻

### 引用

### 来源

  - 孫文良：《中國官制史》，臺北：文津出版社，1993年

  - 祁美琴：《清代内务府》，沈阳：辽宁民族出版社，2009年

  - 〈內務府慎刑司呈稿〉、〈內務府來文〉土地房屋類

  - 〈內務府來文〉刑罰類

  -
## 外部連結

  -
  -
## 参见

  - [內務府公署](../Page/內務府公署.md "wikilink")

{{-}}

[Category:清朝中央官制](../Category/清朝中央官制.md "wikilink")
[内务府](../Category/内务府.md "wikilink")

1.  《滿和辭典》aha，奴僕、召使ひ，頁7
2.  清宫述闻：正续编合编本，紫禁城出版社，1990年，第347-348页
3.  《钦定宫中现行则例》
4.  《阉宦兴衰》 《中国古代宦官传》
5.  昭槤《嘯亭雜錄》卷八 ,
6.  祁美琴 :《清代内务府》, P128 - 129。
7.  徐珂《清稗类钞》戶部歲奉孝欽后十八萬，德宗二十萬，名曰「交進銀」。德宗之二十萬，二月初繳。孝欽后之十八萬，則每節交五萬，年終交八萬。
8.  录副档：同治十一年四月二十五日户部折，案卷号4932。
9.  周育民 : 《晚清财政与社会变迁》, P319 。
10. 《南开学报》1984 年第 3 期，何本方 : 《清代户部诸关初探》。
11. [028](http://www.mh.sinica.edu.tw/PGPublication_Detail.aspx?tmid=3&mid=45&pubid=478&majorTypeCode=2&minorTypeCode=2&major=2&minor=2#1)、[pdf](http://www.mh.sinica.edu.tw/Module/UcSysUserUploadFile_Handler.ashx?filePath=D%3A%5C%5CNew_Web%5C%5CFileUpload%5C%5C96%5C%5C%E4%B9%BE%E9%9A%86%E6%9C%9D%E5%85%A7%E5%8B%99%E5%BA%9C%E7%9A%84%E7%95%B6%E9%8B%AA%E8%88%87%E7%99%BC%E5%95%86%E7%94%9F%E6%81%AF\(1736-1795\).pdf)