**泊村**（）位於[北方四島](../Page/北方四島.md "wikilink")（南千島群島）[國後島南部區域](../Page/國後島.md "wikilink")。過去曾經為日本領土，但在1945年二次大戰後遭[蘇聯佔領](../Page/蘇聯.md "wikilink")，現實際上為[俄羅斯所統治](../Page/俄羅斯.md "wikilink")，行政區劃屬於[遠東聯邦管區](../Page/遠東聯邦管區.md "wikilink")[萨哈林州](../Page/萨哈林州.md "wikilink")。但因日本至今仍然主張擁有主權，故在[北海道](../Page/北海道.md "wikilink")[根室振兴局下仍設有此行政區劃的建制](../Page/根室振兴局.md "wikilink")，但並無任何實際上的運作。

以泊（）為主要聚落，俄文名稱的意義是「哥羅尼的城鎮」，為[哥羅尼事件的發生地點](../Page/哥羅尼事件.md "wikilink")。

## 沿革

[Tomari_Village_in_Kunashiri_Island.JPG](https://zh.wikipedia.org/wiki/File:Tomari_Village_in_Kunashiri_Island.JPG "fig:Tomari_Village_in_Kunashiri_Island.JPG")

  - 1923年：泊村、東沸村、米戶賀村、秩苅別村[合併為泊村](../Page/市町村合併.md "wikilink")，成為北海道二級村。\[1\]
  - 1945年9月：被蘇聯佔領。

## 參考資料

1.