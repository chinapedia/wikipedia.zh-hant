[Arigun.jpg](https://zh.wikipedia.org/wiki/File:Arigun.jpg "fig:Arigun.jpg")

**阿里袞**（；），字**松崖**。為[中國](../Page/中國.md "wikilink")[清朝官員](../Page/清朝.md "wikilink")，[满洲](../Page/满洲.md "wikilink")[镶黄旗人](../Page/鑲黃旗_\(八旗制度\).md "wikilink")，[钮祜禄氏](../Page/钮祜禄氏.md "wikilink")。

## 生平

清初五大臣[額亦都](../Page/額亦都.md "wikilink")、[和碩公主曾孫](../Page/和碩公主.md "wikilink")；大臣[遏必隆孫](../Page/遏必隆.md "wikilink")；大臣[尹德之子](../Page/尹德.md "wikilink")\[1\]。乾隆十五年（1751年）任[兩廣總督](../Page/兩廣總督.md "wikilink")。乾隆二十三年（1758年），任[参赞大臣](../Page/参赞大臣.md "wikilink")，与将军[富德攻讨霍集占](../Page/富德.md "wikilink")，解除将军[兆惠之围](../Page/兆惠.md "wikilink")。乾隆二十八年，加太子太保。乾隆二十九年，授户部尚书、[协办大学士](../Page/协办大学士.md "wikilink")<ref>[民国](../Page/民国.md "wikilink")·[趙爾巽等](../Page/趙爾巽.md "wikilink")，《[清史稿](../Page/清史稿.md "wikilink")》（卷313）：“六月，靖逆将军[雅尔哈善攻](../Page/雅尔哈善.md "wikilink")[库车](../Page/库车.md "wikilink")，[霍集占赴援](../Page/霍集占.md "wikilink")，入城守，已，复走还[叶尔羌](../Page/叶尔羌.md "wikilink")。
上为罢雅尔哈善，而督定边将军[兆惠攻](../Page/兆惠.md "wikilink")[阿克苏](../Page/阿克苏.md "wikilink")，遂进逼叶尔羌。十一月，命阿里衮
选马三千、驼七百益兆惠军。兆惠攻叶尔羌不克，濒[黑水结寨](../Page/黑水.md "wikilink")，霍集占为长围困之。
上闻，授富德定边右将军、阿里衮参赞大臣，援兆惠。是月命袭封二等公。十二月，
授兵部尚书、正红旗蒙古都统。二十四年正月，富德师至[呼尔璊](../Page/呼尔璊.md "wikilink")，霍集占出战，五
日四夜未决。阿里衮以驼马至，乘夜分师为两翼斫阵，斩千馀级。[布拉呢敦中创](../Page/布拉呢敦.md "wikilink")，
与[霍集占并败走](../Page/霍集占.md "wikilink")。援兆惠全师以还。上以阿里衮送马济军，如期集事，且杀贼多，
加[云骑尉](../Page/云骑尉.md "wikilink")[世职](../Page/世职.md "wikilink")，例进一等公。七月，霍集占走[巴达克山部](../Page/巴达克山.md "wikilink")，阿里衮与富德等帅师从
之，降其众万二千有奇。阿里衮以五百人驻[伊西勒库尔淖尔西截隘](../Page/葉什勒池.md "wikilink")，复分兵出其南，
遇敌，夺其家属辎重，降二千有奇。复将选兵二百逾岭逐敌。巴达克山部旋纳款， 以霍集占首献。行赏，赐阿里衮双眼孔雀翎。

二十五年，召还京师。六月，自喀什噶尔行次[叶尔羌](../Page/叶尔羌.md "wikilink")，会[雅木扎尔回酋](../Page/雅木扎尔.md "wikilink")[迈喇木煽讹谓阿睦尔撒纳复入](../Page/迈喇木.md "wikilink")[阿克苏](../Page/阿克苏.md "wikilink")，群起为乱。乃复还[喀什噶尔](../Page/喀什噶尔.md "wikilink")，率八百人以出，至[伯什克勒木](../Page/伯什克勒木.md "wikilink")，迈喇木等以千馀人拒战，阿里衮督所部击破之。贼入城坚守，麾兵合围，
夜四鼓，城人呼号乞降，迈喇木遁去。上奖阿里衮应机立办，授其子[拜唐阿](../Page/拜唐阿.md "wikilink")[丰升额](../Page/丰升额.md "wikilink")[蓝翎侍卫](../Page/蓝翎侍卫.md "wikilink")。阿里衮旋捕迈喇木等送京师，复进丰升额[三等侍卫](../Page/三等侍卫.md "wikilink")，授其次子[倭兴额蓝翎侍卫](../Page/倭兴额.md "wikilink")。十月，阿里衮还京师，授[领侍卫内大臣](../Page/领侍卫内大臣.md "wikilink")，[图形紫光阁](../Page/图形紫光阁.md "wikilink")。二十八年，加太子太保。二十九年，授[户部尚书](../Page/户部尚书.md "wikilink")、[协办大学士](../Page/协办大学士.md "wikilink")。”</ref>。

乾隆三十一年二月，[明瑞战死](../Page/明瑞.md "wikilink")[缅甸](../Page/缅甸.md "wikilink")[猛腊](../Page/猛腊.md "wikilink")，是年二月，朝廷以大学士[傅恒为](../Page/傅恒.md "wikilink")[经略](../Page/经略.md "wikilink")，授阿里衮及[阿桂为副将军](../Page/阿桂.md "wikilink")，领兵参加[清缅战争](../Page/清缅战争.md "wikilink")，途中得疫疾，傅恒令他留[永昌治疾](../Page/永昌.md "wikilink")，阿里袞不肯，乾隆三十四年十二月，卒於軍中。諡襄壯，祀賢良祠。以豐升額金川功，追加封號為果毅繼勇公。

有子[丰升额](../Page/丰升额.md "wikilink")、[倭兴额](../Page/倭兴额.md "wikilink")、[色克精额](../Page/色克精额.md "wikilink")、[布彦达赉](../Page/布彦达赉.md "wikilink")\[2\]。

## 參考文獻

[Category:清朝兩廣總督](../Category/清朝兩廣總督.md "wikilink")
[Category:清朝兵部尚書](../Category/清朝兵部尚書.md "wikilink")
[Category:清朝戶部尚書](../Category/清朝戶部尚書.md "wikilink")
[Category:鑲白旗滿洲都統](../Category/鑲白旗滿洲都統.md "wikilink")
[Category:協辦大學士](../Category/協辦大學士.md "wikilink")
[Category:諡襄壯](../Category/諡襄壯.md "wikilink")

1.  [民国](../Page/民国.md "wikilink")·[趙爾巽等](../Page/趙爾巽.md "wikilink")，《[清史稿](../Page/清史稿.md "wikilink")》（卷313）：“阿里衮，字松崖，[钮祜禄氏](../Page/钮祜禄氏.md "wikilink")，[满洲正白旗人](../Page/满洲正白旗.md "wikilink")，尹德第四子，而讷亲弟也。乾隆初，自[二等侍卫授](../Page/二等侍卫.md "wikilink")[总管内务府大臣](../Page/总管内务府大臣.md "wikilink")。迁[侍郎](../Page/侍郎.md "wikilink")，历兵、户二部。五年，命与[佥都御史朱必堦如](../Page/佥都御史.md "wikilink")[山东勘](../Page/山东.md "wikilink")[巡抚](../Page/巡抚.md "wikilink")[硕色报歉收失实状](../Page/硕色.md "wikilink")。疏言：“[兰山](../Page/兰山.md "wikilink")、[郯城被水最甚](../Page/郯城.md "wikilink")，请缓徵新、
    旧赋，而以官帑市穀补社仓。”复命与[江南河道总督](../Page/江南河道总督.md "wikilink")[高斌如](../Page/高斌.md "wikilink")[江西勘巡抚](../Page/江西.md "wikilink")[岳濬等徇情](../Page/岳濬.md "wikilink")
    纳贿状，鞫实，濬坐黜。”
2.  [民国](../Page/民国.md "wikilink")·[趙爾巽等](../Page/趙爾巽.md "wikilink")，《[清史稿](../Page/清史稿.md "wikilink")》（卷313）：“三十四年二月，上摘[云贵总督](../Page/云贵总督.md "wikilink")[明德疏语](../Page/明瑞.md "wikilink")，以军中马羸责阿里衮等，下[部议夺职](../Page/部议.md "wikilink")，
    命宽之。三月，傅恆至军，与阿里衮等议进兵渡[戛鸠江](../Page/戛鸠江.md "wikilink")，西攻[猛拱](../Page/猛拱.md "wikilink")、[猛养两](../Page/猛养.md "wikilink")[土司](../Page/土司.md "wikilink")，
    向[阿瓦](../Page/阿瓦.md "wikilink")。阿瓦，缅甸都也。偏师至[猛密](../Page/猛密.md "wikilink")，夹江而下，造舟蛮暮通往来。七月，师行。
    初，阿里衮病疡，上遣医就视良愈，至是复大作。傅恆令留永昌治疾，阿里衮坚请行。师进，缅甸兵不出。十月，傅恆还师蛮暮，复进攻老官屯，驻戛鸠江口。缅甸
    兵水陆并至，傅恆、阿桂军江东，阿里衮军江西，迎战。敌结寨自固，阿里衮率兵
    七百攻之，敌百馀弃寨走。把总姚卓杀敌，夺其旗，师锐进，敌四百馀亦遁。复战，
    会日暮，敌不能坚守，皆引去。凡破寨三，杀敌五百馀。傅恆亦遘疾，诸将议毋更
    进兵，阿里衮曰：“老官屯贼寨，前岁额尔登额攻未克。距此仅一舍，不破之何以
    报命？”策马行，傅恆以下皆从之，寨坚，攻不克。阿里衮疾甚，犹强起督攻，视
    枪砲最多处辄身当之。傅恆虑其伤，令将舟师，毋更与攻寨。十二月，卒於军，谥襄壮，祀贤良祠。以丰升额金川功，追加封号为果毅继勇公。子丰升额、倭兴额、
    色克精额、布彦达赉。”