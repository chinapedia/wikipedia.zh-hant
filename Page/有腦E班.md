《**有腦E班**》（）是[香港](../Page/香港.md "wikilink")[亞洲電視製作的一個](../Page/亞洲電視.md "wikilink")[兒童節目](../Page/兒童節目.md "wikilink")，節目每日有不同主題，在[亞洲電視](../Page/亞洲電視.md "wikilink")[本港台播岀](../Page/本港台.md "wikilink")，此節目接替同類型播了近7年的兒童節目《[亞洲電視兒童台](../Page/亞洲電視兒童台.md "wikilink")》。由[亞洲電視和](../Page/亞洲電視.md "wikilink")[紫荊雜誌社主辦的](../Page/紫荊雜誌社.md "wikilink")“中國改革開放30周年知識競賽”也曾在此節目播出。於2009年4月此節目已被《[媽媽不懂的事](../Page/媽媽不懂的事.md "wikilink")》所取代。

## 播岀時間

### 首播

  - 逢星期一至星期五4:00-4:30PM（2007年10月8日－2008年10月31日）
  - 逢星期一至星期五4:10-4:40PM（2008年11月3日－2009年4月）

### 重播

  - 逢星期一至星期五10:15-10:45AM
  - [星期六或星期日](../Page/星期六.md "wikilink")4:00-4:30PM、（逢賽馬日）則為10:30-11:00AM

## 主持

  - [伍偉樂](../Page/伍偉樂.md "wikilink") (2007-2009年)
  - [黃慧敏](../Page/黃慧敏.md "wikilink")（2007－2008年）
  - [翟威廉](../Page/翟鋒.md "wikilink")（2007－2008年）
    轉投[無線電視](../Page/TVB.md "wikilink")
  - [李可瑩](../Page/李可瑩.md "wikilink") (2007－2009年)
  - [何卓瑩](../Page/何卓瑩.md "wikilink") (2008－2009年)
  - [高思哲](../Page/高思哲.md "wikilink") (2008－2009年)
  - [譚芷翎](../Page/譚芷翎.md "wikilink") (2008－2009年)
  - [莊文康](../Page/莊文康.md "wikilink") (2008－2009年)
  - [林雋健](../Page/林雋健.md "wikilink") (2008－2009年)
  - [黃倩婷](../Page/黃倩婷.md "wikilink") (2009年)

## 小主持

  - [林雅雯](../Page/林雅雯.md "wikilink")（2007－2008年）
  - [胡希文](../Page/胡希文.md "wikilink")（2007－2008年）
  - [李用興](../Page/李用興.md "wikilink")（2007－2008年）
  - [陳德強](../Page/陳德強.md "wikilink")（2007－2008年）
  - [勞子諾](../Page/勞子諾.md "wikilink")（2008－2009年）
  - [冼志律](../Page/冼志律.md "wikilink")（2008－2009年）

## 環節

  - [中文加油站](../Page/中文加油站.md "wikilink")
  - [至Hit新聞眼](../Page/至Hit新聞眼.md "wikilink")
  - [英語Happy hour](../Page/英語Happy_hour.md "wikilink")
  - [遊戲基地](../Page/遊戲基地_\(電視節目\).md "wikilink")
  - [好玩自悠行](../Page/好玩自悠行.md "wikilink")
  - [英語爬爬網](../Page/英語爬爬網.md "wikilink")
  - [知趣派](../Page/知趣派.md "wikilink")
  - [今日Good Show](../Page/今日Good_Show.md "wikilink")
  - [魔法再現](../Page/魔法再現.md "wikilink")

## 外部連結

  - [《108 新生
    aTV》](https://web.archive.org/web/20071011191947/http://app.hkatv.com/atvnews/main.php?id=2699)
  - [有腦E班](https://web.archive.org/web/20071028041339/http://www.hkatv.com/v3/infoprogram/07/smartclass/index.html)
  - [中國改革開放30周年知識競賽](https://web.archive.org/web/20090411142750/http://www.tourtaiwan.org/doc/1008/0/8/2/100808222.html?coluid=0&kindid=0&docid=100808222)
  - [有腦E班小遊戲](http://hk.myblog.yahoo.com/acute_lau)

[Category:香港兒童節目](../Category/香港兒童節目.md "wikilink")
[Category:亞洲電視兒童節目](../Category/亞洲電視兒童節目.md "wikilink")