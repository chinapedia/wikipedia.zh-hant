**三百六十行**是指各行各业的行当，也就是社会的工种。

## 名字由來

“三百六十行”這名詞始于[明朝](../Page/明朝.md "wikilink")[嘉靖年间](../Page/嘉靖.md "wikilink")，[文学家](../Page/文学家.md "wikilink")[田汝成](../Page/田汝成.md "wikilink")《[西湖游览志余](../Page/西湖游览志余.md "wikilink")》一书，作者用“三百六十行”来表示明代的各行各业比[宋代时多](../Page/宋代.md "wikilink")。在这之前，[中國人也习惯用](../Page/中國.md "wikilink")“三十六”这个数来表示很多之意。而[清末](../Page/清.md "wikilink")[徐珂撰写的](../Page/徐珂.md "wikilink")《[清稗类钞·农商类](../Page/清稗类钞·农商类.md "wikilink")》里面说：“三十六行者，种种职业也。反其分工而约计之，日三十六行；倍之，则为七十二行；十之，则为三百六十行，皆就成数而言。”依照这样的看法，三百六十行只是广意指各行各业的统称，是一个虚数而不是个实数。

另外一种说法，认为[农历一年有三百六十天](../Page/农历.md "wikilink")，每天学一行或者干一行的話，整年就能学成或者干成三百六十行。或者说，一个人至多也仅可从学或干成三百六十行。另有的认为[圆周共计三百六十度](../Page/圆周.md "wikilink")，这些是[西亚两河流域人发现的](../Page/西亚.md "wikilink")，从古至今世界通用。“三百六十”从时间上跟空间上来看，全是整数。“三百六十行”有天下全部行业的含意，上下左右和古往今来的行业全都包括进去。此种说法是有一定的道理。

## 類別

[農業行](../Page/農業.md "wikilink")，[作坊行](../Page/作坊.md "wikilink")，[商店行](../Page/商店.md "wikilink")，[飲食行](../Page/飲食.md "wikilink")，[瓜果行](../Page/瓜果.md "wikilink")，[小菜行](../Page/小菜.md "wikilink")，[攤販行](../Page/攤販.md "wikilink")，[花鳥行](../Page/花鳥.md "wikilink")，[修補行](../Page/修補.md "wikilink")，[娛樂行](../Page/娛樂.md "wikilink")，[文化行](../Page/文化.md "wikilink")，[服務行](../Page/服務.md "wikilink")，[醫藥行](../Page/醫藥.md "wikilink")，[交通行](../Page/交通.md "wikilink")，[員工行](../Page/員工.md "wikilink")，[苦力行](../Page/苦力.md "wikilink")，[巫術行](../Page/巫術.md "wikilink")，[乞討行](../Page/乞討.md "wikilink")，[煙賭娼行](../Page/煙賭娼.md "wikilink")，[騙搶行](../Page/騙搶.md "wikilink")。

## 實際行業（未完全）

1.  [耕田](../Page/耕田.md "wikilink")
2.  [鋤草](../Page/鋤草.md "wikilink")
3.  [腳踏車水](../Page/腳踏車水.md "wikilink")
4.  [手推車水](../Page/手推車水.md "wikilink")
5.  [牛拉車水](../Page/牛拉車水.md "wikilink")
6.  [割稻](../Page/割稻.md "wikilink")
7.  [板網捕魚](../Page/板網捕魚.md "wikilink")
8.  [鸕鶿捕魚](../Page/鸕鶿捕魚.md "wikilink")
9.  [鋼叉叉魚](../Page/鋼叉叉魚.md "wikilink")
10. [牧牛](../Page/牧牛.md "wikilink")
11. [養鴨](../Page/養鴨.md "wikilink")
12. [叉青蛙](../Page/叉青蛙.md "wikilink")
13. [捉棉花](../Page/捉棉花.md "wikilink")
14. [紮棉花](../Page/紮棉花.md "wikilink")
15. [採桑葉](../Page/採桑葉.md "wikilink")
16. [剝蠶繭](../Page/剝蠶繭.md "wikilink")
17. [紡紗](../Page/紡紗.md "wikilink")
18. [殺豬](../Page/殺豬.md "wikilink")
19. [狩獵](../Page/狩獵.md "wikilink")
20. [樁米](../Page/樁米.md "wikilink")
21. [磨墨](../Page/磨墨.md "wikilink")
22. [繡花](../Page/繡花.md "wikilink")
23. [成衣舖](../Page/成衣舖.md "wikilink")
24. [湖絲阿姐](../Page/湖絲阿姐.md "wikilink")
25. [糊宮扇](../Page/糊宮扇.md "wikilink")
26. [燈籠坊](../Page/燈籠坊.md "wikilink")
27. [草蓆坊](../Page/草蓆坊.md "wikilink")
28. [竹窗坊](../Page/竹窗坊.md "wikilink")
29. [竹編坊](../Page/竹編坊.md "wikilink")
30. [草鞋坊](../Page/草鞋坊.md "wikilink")
31. [刀刺手腕](../Page/刀刺手腕.md "wikilink")
32. [养猪](../Page/养猪.md "wikilink")

## 未確定的行業

  - 賣[老鼠藥](../Page/老鼠藥.md "wikilink")
  - [野雞](../Page/野雞.md "wikilink")
  - 作坊行之中還有“[刻瓷器](../Page/刻瓷器.md "wikilink")”
  - “[絞面](../Page/絞面.md "wikilink")”，北京稱為“[開臉](../Page/開臉.md "wikilink")”。[清代有專門給老太太梳頭的行當](../Page/清代.md "wikilink")，上海人稱為“[梳頭娘姨](../Page/梳頭娘姨.md "wikilink")”，她們是為新娘子絞面的合適人選。
  - 賣[雞毛撣帚](../Page/雞毛撣帚.md "wikilink")

## 参看

  - [行业](../Page/行业.md "wikilink")
  - [三十六行](../Page/三十六行.md "wikilink")
  - [七十二行](../Page/七十二行.md "wikilink")
  - [三教九流](../Page/三教九流.md "wikilink")

## 参考文献

  - 《中國老360行》藍翔 馮懿有 所著

[Category:中國傳統職業](../Category/中國傳統職業.md "wikilink")
[Category:中国思想史](../Category/中国思想史.md "wikilink")
[Category:中国社会史](../Category/中国社会史.md "wikilink")
[Category:中国经济史](../Category/中国经济史.md "wikilink")