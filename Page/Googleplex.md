[Googleplex-SignIn.jpg](https://zh.wikipedia.org/wiki/File:Googleplex-SignIn.jpg "fig:Googleplex-SignIn.jpg")
**Googleplex**，是[Google公司总部的名字](../Page/Google公司.md "wikilink")，位于[美国](../Page/美国.md "wikilink")[加州](../Page/加州.md "wikilink")[圣克拉拉县的](../Page/圣克拉拉县.md "wikilink")[山景城](../Page/山景城_\(加利福尼亞州\).md "wikilink")，离[旧金山很近](../Page/旧金山.md "wikilink")。

“Googleplex”来源于英文单词“googolplex”（[古戈爾普勒克斯](../Page/古戈爾普勒克斯.md "wikilink")），而「Google」转变于单词“googol”（[古戈爾](../Page/古戈爾.md "wikilink")）。
古戈爾普勒克斯指的是10<sup>10<sup>100</sup></sup>，即1後面有古戈爾个0的[自然数](../Page/自然数.md "wikilink")。

## 位置

Googleplex位于[加州](../Page/加州.md "wikilink")[山景城北部的查尔斯路](../Page/山景城_\(加利福尼亞州\).md "wikilink")，露天剧场百汇和海岸大道之间，靠近海岸公园的[湿地](../Page/湿地.md "wikilink")。员工居住在[旧金山或东海湾](../Page/旧金山.md "wikilink")，由带有[Wi-Fi功能的谷歌补贴汽车接送往返工作](../Page/Wi-Fi.md "wikilink")，汽车以由美国生产和加工的[生物柴油作燃料](../Page/生物柴油.md "wikilink")。\[1\]

Googleplex的周围有阿尔扎广场，西面有[Mozilla基金会](../Page/Mozilla基金会.md "wikilink")，北面是海岸剧场，西北是Intuit公司和世纪剧院，以及[微软公司的](../Page/微软.md "wikilink")[硅谷研究所](../Page/硅谷.md "wikilink")，而计算机历史博物馆在南方，莫菲特场位于附近东部。

## 其他意思

**Googleplex**，也是[道格拉斯·亚当斯](../Page/道格拉斯·亚当斯.md "wikilink")（Douglas
Adams）所著的《[-{zh-hans:银河系漫游指南;
zh-hant:銀河便車指南;}-](../Page/银河系漫游指南.md "wikilink")》一书中的超级电脑（并非沉思）的缩写名：

参考译文：

## 参考

## 參見

[Category:Google](../Category/Google.md "wikilink")
[Category:加利福尼亞州建築物](../Category/加利福尼亞州建築物.md "wikilink")

1.