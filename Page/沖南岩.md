**沖南岩**又被稱作**南岩**、**大南小島**或者是**蛇島**，在[日本則稱呼為](../Page/日本.md "wikilink")**沖之南岩**（），是[釣魚臺列嶼中的一座無人](../Page/釣魚臺列嶼.md "wikilink")[荒島](../Page/荒島.md "wikilink")\[1\]\[2\]\[3\]\[4\]。沖南岩本身島嶼地質為火山島結構[火山島](../Page/火山島.md "wikilink")，通常跟和它相應的[沖北岩一同被歸類為](../Page/沖北岩.md "wikilink")[釣魚臺其附屬島嶼中](../Page/釣魚臺.md "wikilink")。其相對位置位在[石垣島北方約](../Page/石垣島.md "wikilink")160公里、釣魚臺列嶼主島釣魚臺東方約7公里處，島嶼整體[面積約](../Page/面積.md "wikilink")0.01平方公里，而其海拔[高度最高僅有](../Page/高度.md "wikilink")13公尺\[5\]。目前包括[日本](../Page/日本.md "wikilink")、[中華人民共和國和](../Page/中華人民共和國.md "wikilink")[中華民國都聲稱擁有沖南岩之主權](../Page/中華民國.md "wikilink")，其中日本認定沖南岩等釣魚臺列嶼皆為其[實際統治的領土](../Page/實際統治.md "wikilink")，但是另外一方面中華人民共和國與中華民國則多次表示自身才合法擁有該土地的主權\[6\]\[7\]\[8\]\[9\]。

## 註釋

<references group = "註"/>

## 參考資料

## 外部連結

  - [釣魚臺列嶼簡介](https://web.archive.org/web/20120614114615/http://maritimeinfo.moi.gov.tw/marineweb/layout_C10.aspx)

  - [](http://watchizu.gsi.go.jp/watchizu.html?b=254519&l=1233359)

[Category:釣魚台列嶼](../Category/釣魚台列嶼.md "wikilink")
[Category:石垣市](../Category/石垣市.md "wikilink")
[Category:頭城鎮](../Category/頭城鎮.md "wikilink")
[Category:日本無人島](../Category/日本無人島.md "wikilink")

1.  關於沖北岩在各地漢語字詞的使用習慣上皆不同處，其中中國大陸多稱呼為「-{南屿}-」，而香港與臺灣地區則常稱呼為「-{沖南岩}-」。

2.

3.

4.

5.
6.

7.

8.

9.