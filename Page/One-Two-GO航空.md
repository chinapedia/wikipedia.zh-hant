**One-Two-GO航空**（），全名「One-Two-GO航空有限公司」\[1\]（），是[泰國一間以經營](../Page/泰國.md "wikilink")[內陸](../Page/內陸.md "wikilink")[航線為主的](../Page/航線.md "wikilink")[航空公司](../Page/航空公司.md "wikilink")，以[曼谷廊曼機場為基地](../Page/曼谷廊曼機場.md "wikilink")，於2003年12月3日啟業，首航飛[曼谷至](../Page/曼谷.md "wikilink")[清邁](../Page/清邁.md "wikilink")；為[泰國東方航空的](../Page/泰國東方航空.md "wikilink")[子公司](../Page/子公司.md "wikilink")。該企業是泰國首批的[廉價航空公司之一](../Page/廉價航空.md "wikilink")，也是目前[亞洲唯一一家採取](../Page/亞洲.md "wikilink")「無差別航線統一票價」政策的航空業者\[2\]；現有22架二手[飛機](../Page/飛機.md "wikilink")，包括於2007年初從[日本航空公司購入的](../Page/日本航空公司.md "wikilink")14架[麥道](../Page/麥道.md "wikilink")[MD-80系列](../Page/MD-80.md "wikilink")[客機](../Page/客機.md "wikilink")。

自2008年7月22日起，該航空及其[母公司](../Page/母公司.md "wikilink")（[泰國東方航空](../Page/泰國東方航空.md "wikilink")）因累計安全記錄不佳之緣由，均被[泰國民航局勒令停飛](../Page/泰國民航局.md "wikilink")56天。

## 航點

  - [曼谷](../Page/曼谷.md "wikilink")（[曼谷廊曼機場](../Page/曼谷廊曼機場.md "wikilink")）-
    主機場
  - [曼谷](../Page/曼谷.md "wikilink")（[新曼谷國際機場](../Page/新曼谷國際機場.md "wikilink")）
  - [清萊](../Page/清萊.md "wikilink")
  - [合艾](../Page/合艾.md "wikilink")
  - [甲米](../Page/甲米.md "wikilink")
  - [洛坤](../Page/洛坤.md "wikilink")
  - [布吉](../Page/布吉.md "wikilink")
  - [萬倫](../Page/萬倫.md "wikilink")

## 機隊

截至2007年為止，**One-Two-GO航空**有下列飛機： \[3\]

  - 2架[波音747-100](../Page/波音747.md "wikilink")
  - 1架[波音747-300](../Page/波音747.md "wikilink")
  - 5架[麥道MD-82](../Page/MD-82.md "wikilink")
  - 1架[麥道MD-83](../Page/MD-83.md "wikilink")
  - 1架[麥道MD-87](../Page/MD-83.md "wikilink")

## 另見

  - [One-Two-GO航空269號班機空難](../Page/One-Two-GO航空269號班機空難.md "wikilink")
  - [廉價航空公司](../Page/廉價航空公司.md "wikilink")

## 備註

## 參考來源

[ja:ワン・トゥー・ゴー航空](../Page/ja:ワン・トゥー・ゴー航空.md "wikilink")

[Category:泰國航空公司](../Category/泰國航空公司.md "wikilink")
[Category:2003年成立的航空公司](../Category/2003年成立的航空公司.md "wikilink")
[Category:2010年結業航空公司](../Category/2010年結業航空公司.md "wikilink")

1.  "[STATUS OF THE INQUIRY INTO THE ACCIDENT OF ONE TWO GO AIRLINES
    FLIGHT
    OG 269](http://www.thaiembassy.sg/press_media/news-highlights/status-of-the-inquiry-into-the-accident-of-one-two-go-airlines-flight-og)."
    ([Archive](http://www.webcitation.org/6Fh18VZot)) Royal Thai Embassy
    of Singapore. Retrieved on 6 April 2013.
2.  <http://www.cnn.com/2007/WORLD/asiapcf/09/17/thailand.crash/index.html>
3.  [Airfleets.com One-Two-GO Airlines fleet
    details](http://www.airfleets.net/flottecie/Orient%20Thai%20Airlines.htm=)