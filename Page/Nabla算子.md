**Del算子**或稱**Nabla算子**，在中文中也叫**向量微分算子**、**劈形算子**、**倒三角算子**，[符号为](../Page/符号.md "wikilink")\(\nabla\)，是一个[向量](../Page/向量算子.md "wikilink")[微分算子](../Page/微分算子.md "wikilink")，但本身並非一個向量\[1\]。

其形式化定义为：

\(\nabla = {d \over dr}\)

在n维空间中，分母dr为含n个分量的向量，因而\(\nabla\)本身就是个n维向量[算子](../Page/算子.md "wikilink")。

三维情况下，\(\nabla = {\frac{\partial }{\partial x}}\mathbf{i}+
{\frac{\partial }{\partial y}}\mathbf{j}+
{\frac{\partial }{\partial z}}\mathbf{k}\)或\(\nabla = ({\frac{\partial }{\partial x}},{\frac{\partial }{\partial y}},
{\frac{\partial }{\partial z}})\)

二维情况下，\(\nabla = {\frac{\partial }{\partial x}}\mathbf{i}+
{\frac{\partial }{\partial y}}\mathbf{j}\)或\(\nabla = ({\frac{\partial }{\partial x}},{\frac{\partial }{\partial y}})\)

\(\nabla\)作用于不同类型的量，得到的就是不同类型的新量：

\[\nabla\]直接作用于函数F(r)（不论F是标量还是向量），意味着求F(r)的[梯度](../Page/梯度.md "wikilink")，表示为：\(\nabla F(r)\)（标量函数的梯度为向量，向量的梯度为二阶张量……）；

\[\nabla\]与非标量函数F(r)由点积符号·连接，意味着求F(r)的[散度](../Page/散度.md "wikilink")，表示为：\(\nabla\)·\(F(r)\)；

\[\nabla\]与非标量（三维）函数F(r)由叉积符号×连接，意味着求F(r)的[旋度](../Page/旋度.md "wikilink")，表示为：\(\nabla\)×\(F(r)\)。

## 名稱

Nabla算子的名字来自[希腊语中一种被称为纳布拉琴的](../Page/希腊语.md "wikilink")[竖琴](../Page/竖琴.md "wikilink")。相关的词汇也存在于[亚拉姆语和](../Page/亚拉姆语.md "wikilink")[希伯来语中](../Page/希伯来语.md "wikilink")。

该符号的另一常见的名称是*atled*，因为它是希腊字母Δ倒过来的形状。除了*atled*外，它还有一个名称是*del*。

Del算子在标准HTML中写为\&nabla，而在[LaTeX中为](../Page/LaTeX.md "wikilink")\\nabla。在[Unicode中](../Page/Unicode.md "wikilink")，它是[十进制](../Page/十进制.md "wikilink")[数](../Page/数.md "wikilink")8711，也即[十六进制数](../Page/十六进制.md "wikilink")0x2207。

Del算子在[数学中用于指代](../Page/数学.md "wikilink")[梯度算符](../Page/梯度.md "wikilink")，並可組成[散度](../Page/散度.md "wikilink")、[旋度和](../Page/旋度.md "wikilink")[拉普拉斯算子](../Page/拉普拉斯算子.md "wikilink")。它也用于指代[微分几何中的](../Page/微分几何.md "wikilink")[联络](../Page/联络.md "wikilink")（可以视为更广意义上的梯度算子）。它由[哈密尔顿引入](../Page/威廉·哈密顿.md "wikilink")。

## 參考

[Category:数学符号](../Category/数学符号.md "wikilink")
[Category:數學表示法](../Category/數學表示法.md "wikilink")
[Category:微分算子](../Category/微分算子.md "wikilink")

1.  David J. Griffiths,*Introduction to electrodynamics*,Fourth
    edition,Pearson Education, Inc.,p.16.