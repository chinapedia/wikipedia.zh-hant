**凡賽堤**（），他是光明之神—[巴德爾](../Page/巴德爾.md "wikilink")（）和[南娜](../Page/南娜.md "wikilink")（Nanna）之子。是真理與正義之神。他的宮殿名為[格利特尼爾](../Page/格利特尼爾.md "wikilink")（），意即「閃耀、光亮」，因為此殿有著銀色的[屋頂和金色的](../Page/屋頂.md "wikilink")[柱子](../Page/柱子.md "wikilink")，散發著光芒，即使在遠處也看得到。

在[北歐諸神中](../Page/北歐諸神.md "wikilink")，這位神祇並沒有在[諸神的黃昏被提及](../Page/諸神的黃昏.md "wikilink")，似可解釋他是一個不喜戰鬥的[和平之神](../Page/和平.md "wikilink")。

## 凡賽堤的出身

他出生之後，就被認為是北歐十二主神之一。他的職務和戰神[提爾](../Page/提爾.md "wikilink")（）是相對的，[提爾主持的是有關流](../Page/提爾.md "wikilink")[血鬥爭的紛爭](../Page/血.md "wikilink")；而凡賽堤則是對需要調解的事務提出仲裁。他是[諸神中最聰明正直](../Page/諸神.md "wikilink")、善雄辯的一位，他的判決也絕對公正，沒有人能違抗他的[判決](../Page/判決.md "wikilink")，違抗者將得到他正直不私的處罰──[死亡](../Page/死亡.md "wikilink")。所以最重要的誓言，常是指著他的名義發下的。

傳說他也是古代的[弗里西亞人](../Page/弗里西亞.md "wikilink")（）最主要的神或是[祖先](../Page/祖先.md "wikilink")。另有說法指出，凡賽堤可能是由[弗里西亞人原先](../Page/弗里西亞.md "wikilink")[信仰的神明演變過來的](../Page/信仰.md "wikilink")。一般認為，[巴德爾是個在仲裁紛爭時沒有決斷力的神](../Page/巴德爾.md "wikilink")，因此借用了原為[會議守護之神的神名](../Page/會議.md "wikilink")，成為[巴德爾的兒子](../Page/巴德爾.md "wikilink")，最後納入北歐神話中，成為[真理與](../Page/真理.md "wikilink")[正義之神](../Page/正義.md "wikilink")。

## 凡賽堤的故事

有故事中描述凡賽堤是[北歐最初](../Page/北歐.md "wikilink")[法律的創造者](../Page/法律.md "wikilink")：古[弗里西亞人中的](../Page/弗里西亞.md "wikilink")12位長老，為了收集各部落間最好的法律遊遍[斯堪的那維亞諸地](../Page/斯堪的那維亞.md "wikilink")，好編輯成一套對所有人都適用的[法律](../Page/法律.md "wikilink")。之後，長老們想找個安靜的地方編撰[法典](../Page/法典.md "wikilink")，卻在海上遇到[暴風](../Page/暴風.md "wikilink")，這時小船上多了一個陌生人，他引領著12位長老抵達一個小島，首先他劈地產生一股泉水，然後緩緩對眾人口述出一部法典。這人據說就是凡賽堤，小[島即是](../Page/島.md "wikilink")[赫里戈蘭島](../Page/赫里戈蘭島.md "wikilink")（），之後這[島和](../Page/島.md "wikilink")[泉就被奉為神聖的地方](../Page/泉.md "wikilink")，相傳此島禁止戰鬥及流血，就是[維京人也不敢侵犯](../Page/維京人.md "wikilink")，因為他們相信，在這島上即使戰死也無法到達[英靈殿](../Page/英靈殿.md "wikilink")（），而是會被丟到[海拉](../Page/海拉.md "wikilink")（）的死亡之國。而重大的審判大都在此島舉行，審判官會先飲島上的泉水，以紀念[真理與](../Page/真理.md "wikilink")[正義之神](../Page/正義.md "wikilink")。另外，[北歐人在](../Page/北歐.md "wikilink")[冬季不進行審判](../Page/冬季.md "wikilink")，他們認為陰沉的冬季不適合正義的判決。

[F](../Category/北歐神祇.md "wikilink")