**.wf**為[法國](../Page/法国.md "wikilink")[海外屬地](../Page/海外屬地.md "wikilink")[瓦利斯和富圖納](../Page/瓦利斯和富圖納.md "wikilink")[國家及地區頂級域](../Page/國家及地區頂級域.md "wikilink")（ccTLD）的[域名](../Page/域名.md "wikilink")\[1\]\[2\]\[3\]。

## 参考

## 外部連結

  - [IANA .wf whois information](http://www.iana.org/root-whois/wf.htm)
  - [.wf official
    website](https://web.archive.org/web/20051224045614/http://www.afnic.fr/doc/autres-nic/dom-tom)

[sv:Toppdomän\#W](../Page/sv:Toppdomän#W.md "wikilink")

[wf](../Category/國家及地區頂級域.md "wikilink")
[Category:瓦利斯和富图纳](../Category/瓦利斯和富图纳.md "wikilink")

1.
2.
3.