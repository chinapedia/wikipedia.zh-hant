**热那亚板球与足球俱乐部**（**Genoa Cricket and Football
Club**）是一家位於[意大利](../Page/意大利.md "wikilink")[热那亚以足球為主的俱乐部](../Page/热那亚.md "wikilink")，成立於1893年，是意大利历史最悠久的足球俱乐部。球會代表色為紅色和藍色，的主場是[費拉里斯球場](../Page/費拉里斯球場.md "wikilink")，和熱那亞另一支球會[森多利亞共用](../Page/桑普多利亚足球俱乐部.md "wikilink")。

## 歷史

热那亚是意大利历史最悠久的足球俱乐部，他们在历史上战绩辉煌，从1898年到1924年共计获得过9次[意甲冠军](../Page/意甲.md "wikilink")，在七年的時間內兩次達成三連冠，意甲夺冠总次数在全国排名第四，仅次于[尤文图斯](../Page/尤文图斯.md "wikilink")、[AC米兰和](../Page/AC米兰.md "wikilink")[国际米兰](../Page/国际米兰.md "wikilink")，然而自1924年后，未能再夺得冠軍。

近年来，熱那亞一直在甲级和乙级之间沉浮。2004-05球季曾奪得意乙冠軍，但可惜在最後一場以2比3敗予[摩德納被判打](../Page/摩德納足球俱樂部.md "wikilink")-{zh-hans:假球;zh-hk:假波;zh-tw:假球;}-罪名成立。不能升班外，並被罰降至丙組作賽。

在2006-07赛季，他们取得了[乙级联赛第三名的成绩](../Page/意乙.md "wikilink")，得以回到意甲联赛。熱那亞在其返回意甲聯賽的首個賽季獲得排名聯賽第10位的成績。而在08-09賽季意甲聯賽中，熱那亞在積分相同的情況下因相互勝負劣勢排名聯賽第五位，因而得以獲得參加下賽季[歐洲聯賽的資格](../Page/歐洲聯賽.md "wikilink")。

## 球會荣誉

<small>更新日期：2007年6月10日</small>

  - **[意甲冠军](../Page/意甲.md "wikilink")：9**
      - 1898, 1899, 1900, 1902, 1903, 1904, 1914/15, 1922/23, 1923/24
  - **[意乙冠军](../Page/意乙.md "wikilink")：6**
      - 1934/1935，1952/1953，1961/1962，1972/1973，1975/1976，1988/1989
  - **[意大利盃](../Page/意大利盃.md "wikilink")：1**
      - 1936-37

## 著名球员

  - [Flag_of_Italy.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Italy.svg "fig:Flag_of_Italy.svg") [帕努奇](../Page/帕努奇.md "wikilink")（Christian
    Panucci，1992年—1993年）
  - [Flag_of_Japan.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Japan.svg "fig:Flag_of_Japan.svg") [三浦知良](../Page/三浦知良.md "wikilink")（1994年—1995年）

## 外部链接

  - [官方主页](http://www.genoacfc.it)

[分類:1893年建立的足球俱樂部](../Page/分類:1893年建立的足球俱樂部.md "wikilink")

[R](../Category/意大利足球俱樂部.md "wikilink")