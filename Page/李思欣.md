**李思欣**（，），，為前香港[無綫電視旗下女演員](../Page/TVB.md "wikilink")。

## 簡歷

李思欣早年就讀[香港嘉諾撒學校和](../Page/香港嘉諾撒學校.md "wikilink")[庇理羅士女子中學](../Page/庇理羅士女子中學.md "wikilink")。於2004年取得[香港演藝學院戲劇文憑](../Page/香港演藝學院.md "wikilink")。同年拍攝首部電視劇《[赤沙印記@四葉草.2](../Page/赤沙印記@四葉草.2.md "wikilink")》於劇中擔任第二女主角，在《[法證先鋒](../Page/法證先鋒.md "wikilink")》初演反派，飾演殺人兇手景天思（Tracy），其後曾主演《[亂世佳人](../Page/亂世佳人_\(電視劇\).md "wikilink")》的顧昭兒、《[凶城計中計](../Page/凶城計中計.md "wikilink")》的霍倩琦等。

2010年，李思欣於《[蒲松齡](../Page/蒲松齡_\(電視劇\).md "wikilink")》飾演仗势欺人的魏虹，《[公主嫁到](../Page/公主嫁到.md "wikilink")》中飾演的晉懷公主以及於2011年在《[萬凰之王](../Page/萬凰之王.md "wikilink")》中飾演失心瘋的祥嬪。2011年底播出的[處境喜劇](../Page/處境喜劇.md "wikilink")《[結·分@謊情式](../Page/結·分@謊情式.md "wikilink")》中飾演的獨立女孩錢愛晨，戲份相當重。2012年於《[名媛望族](../Page/名媛望族.md "wikilink")》飾演的婢女采月，演技精湛獲得好評。於2014年5月底約滿離開[無綫電視](../Page/無綫電視.md "wikilink")，主力經營生意\[1\]，仍以自由身活躍於幕前，及主持新城電台美容節目《美麗秀》。

### 感情生活

2011年4月於[微博被發現與](../Page/微博.md "wikilink")[新城知訊台](../Page/新城知訊台.md "wikilink")[唱片騎師](../Page/唱片騎師.md "wikilink")[范振鋒秘密拍拖四個月](../Page/范振鋒.md "wikilink")，同年9月1日宣佈於2012年1月15日舉行婚禮。

## 演出作品

### 電視劇（[無綫電視](../Page/無綫電視.md "wikilink")）

|                                                    |                                                    |                |
| -------------------------------------------------- | -------------------------------------------------- | -------------- |
| **首播**                                             | **劇名**                                             | **角色**         |
| 2004年                                              | **[赤沙印記@四葉草.2](../Page/赤沙印記@四葉草.2.md "wikilink")** | **葉　蕎**        |
| 2005年                                              | [佛山贊師父](../Page/佛山贊師父.md "wikilink")               | 謝蘋兒            |
| [識法代言人](../Page/識法代言人.md "wikilink")               | Betty                                              |                |
| 2006年                                              | **[法證先鋒](../Page/法證先鋒.md "wikilink")**             | **景天思（Tracy）** |
| [東方之珠](../Page/東方之珠_\(劇集\).md "wikilink")          | 金　燕（青年）                                            |                |
| **[亂世佳人](../Page/亂世佳人_\(電視劇\).md "wikilink")**     | **顧昭兒**                                            |                |
| 2007年                                              | [奸人堅](../Page/奸人堅.md "wikilink")                   | 霍珍妮            |
| 2008年                                              | [最美麗的第七天](../Page/最美麗的第七天.md "wikilink")           | 王　慧（Jessie）    |
| [銀樓金粉](../Page/銀樓金粉.md "wikilink")                 | 尚可怡                                                |                |
| [搜神傳](../Page/搜神傳.md "wikilink")                   | 石敢言                                                |                |
| 2010年                                              | **[蒲松齡](../Page/蒲松齡_\(電視劇\).md "wikilink")**       | **魏　虹**        |
| [公主嫁到](../Page/公主嫁到.md "wikilink")                 | 晉懷公主                                               |                |
| 2011年                                              | [仁心解碼](../Page/仁心解碼.md "wikilink")                 | 李芷恩            |
| [Only You 只有您](../Page/Only_You_只有您.md "wikilink") | 蘭                                                  |                |
| [洪武三十二](../Page/洪武三十二.md "wikilink")               | 甜　兒                                                |                |
| **[萬凰之王](../Page/萬凰之王.md "wikilink")**             | **祥　嬪**                                            |                |
| 2012年                                              | **[結·分@謊情式](../Page/結·分@謊情式.md "wikilink")**       | '''錢愛晨（晨晨）     |
| [當旺爸爸](../Page/當旺爸爸.md "wikilink")                 | Ada                                                |                |
| **[名媛望族](../Page/名媛望族.md "wikilink")**             | **采　月**                                            |                |
| 2013年                                              | [幸福摩天輪](../Page/幸福摩天輪.md "wikilink")               | 鍾彩妮（Cherry）    |
| **[凶城計中計](../Page/凶城計中計.md "wikilink")**（非黃金時段播映）  | **霍倩琦**                                            |                |
| 2014年                                              | [寒山潛龍](../Page/寒山潛龍.md "wikilink")                 | 冬　兒            |

### 電視廣告

  - 2001年︰Clean and Clear
    護膚系列（和當時未出道的歌手[鄧麗欣合作](../Page/鄧麗欣.md "wikilink")）[1](http://www.youtube.com/watch?v=wL7p5YplaPI)
  - VO5廣告
  - 2008年︰早稻田日本語研修中心
  - 2008年︰JEE金瑞典（抽氣扇）

### 電影

  - 2012年︰《[起勢搖滾](../Page/起勢搖滾.md "wikilink")》
  - 2017年︰《[我們的6E班](../Page/我們的6E班.md "wikilink")》
  - 待上映︰《[選擇遊戲](../Page/選擇遊戲.md "wikilink")》
  - 拍摄中︰《[狙擊密令](../Page/狙擊密令.md "wikilink")》

### 音樂錄影帶

  - 關楚耀︰（守護者）
  - 鄧健泓︰（戒不掉你）
  - 郭力行︰（笨過人）
  - 梁靖琪︰（賤人）
  - 張敬軒︰（他的故事）
  - 麥浚龍︰（借火）
  - 王友良︰（不再同哭）
  - 何紫慧︰（I do）

### 節目主持

  - 2005年︰康泰加倍開心日本3S之旅
  - 2005年︰全講風生水起
  - 2008年︰MTR Shops︰車站購物情緣
  - 2009年︰《證監會特約 - 財智達人》- 擔任短劇演員
  - 2012年︰長隆旅遊區特約︰家添歡樂逍遙遊

## 產品代言

  - 2001年︰Sony PlayStation 2
  - 2005年︰[龍發製藥](../Page/龍發製藥.md "wikilink")（[龍潤茶旗下公司](../Page/龍潤茶.md "wikilink")）

## 商業參與

李思欣於銅鑼灣駱克道投資雞煲火鍋生意，現有兩間分店，另亦有經營網上鑽石生意。

## 參考

## 外部連結

  -
  -
  -
[sze](../Category/李姓.md "wikilink")
[Category:前無綫電視女藝員](../Category/前無綫電視女藝員.md "wikilink")
[Category:香港電視演員](../Category/香港電視演員.md "wikilink")
[Category:香港女性模特兒](../Category/香港女性模特兒.md "wikilink")
[Category:香港主持人](../Category/香港主持人.md "wikilink")
[Category:香港演藝學院校友](../Category/香港演藝學院校友.md "wikilink")
[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[Category:庇理羅士女子中學校友](../Category/庇理羅士女子中學校友.md "wikilink")

1.