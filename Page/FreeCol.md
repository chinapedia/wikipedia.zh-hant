**FreeCol**是一款[4X遊戲](../Page/4X遊戲.md "wikilink")，是1994年發行的複製版。經[GNU通用公共許可證發佈](../Page/GNU通用公共許可證.md "wikilink")，是為[自由軟體](../Page/自由軟體.md "wikilink")。

FreeCol極大部分是由[Java寫成的](../Page/Java.md "wikilink")，所以可以[跨平臺執行遊戲](../Page/跨平臺.md "wikilink")。FreeCol可以在[Linux和](../Page/Linux.md "wikilink")[Windows上執行](../Page/Microsoft_Windows.md "wikilink")（[Mac
OS
X也可以](../Page/Mac_OS_X.md "wikilink")，但是會有些限制）。在2007年2月它曾經是[SourceForge.net當月的專案](../Page/SourceForge.net.md "wikilink")。\[1\]

Freecol有兩組規則供玩家選擇：「經典」（Classic）和「Freecol」。「經典」規則在和[遊戲內容按照](../Page/遊戲性.md "wikilink")1994年原作的玩法，而「Freecol」則加入了受原遊戲發行版中刪去內容啟發的玩法和全新的遊戲內容，例如4個新歐洲國家和國家優點。\[2\]

## 遊戲

FreeCol遊戲從1492年開始。玩家必須在[新大陸與一些](../Page/新大陸.md "wikilink")[開拓者建立殖民地與其他來自歐洲的對手競爭](../Page/開拓者.md "wikilink")。玩家剛開始會接受來自歐洲祖國的援助，直到殖民地可以自立為止，屆時殖民地可以宣告[獨立](../Page/獨立.md "wikilink")，並且打敗來自祖國的皇家遠征軍隊後即可獲得勝利。

歐洲祖國從援助，到限制，到成為最後獨立的敵人，是FreeCol遊戲的特色。FreeCol遊戲獲勝不在於要佔領所有的地區或打敗土著與其他殖民者，而是要能夠建立起足以對抗皇家軍的軍力，和背後的生產力與財富，來宣告獨立。

玩家可以用從[城市採集而來或者](../Page/城市.md "wikilink")[土著饋贈的](../Page/原住民.md "wikilink")[自然資源和](../Page/自然資源.md "wikilink")[歐洲進行貿易](../Page/歐洲.md "wikilink")。在城市中建立工業將原料加工在貿易中可以獲益更多，或者可以維持城市的運作，例如樹林可以加工成木材而礦石可以加工成工具。

城市與城市之間也可以透過運輸線，來擴大生產、加工、貿易的規模。

### 地形

此游戏引擎的地图基于正方形格，而不是基于六角砖（像Triple
A那样）。一个聚居地的正方形格可以覆盖九个类似正方形格。此游戏具有各种地形，生产不同的原料。

### 任務分配

玩家可以將人物抓取到城市中的資源地圖上去採集資源。具有專業的人物在採集特定的資源時速度會較快。雖然派遣更多的人力可使城市成長，但是更多的人力也會需要更多的糧食。將人物派遣至工廠中可以將原料加工成貨物；人力愈多加工速度也就愈快。但是必須要有原料的供應；當原料不足時，工人會不事生產而只是空耗糧食。

在人物上裝備工具可以進行工事，而裝備槍枝或馬匹會變成士兵（工具和槍枝或馬匹可以自行生產或是向歐洲購買）。裝備工具的工人可以在野外（城市之外）開墾農田（增加糧食產量）或者建造道路（增加單位移動效率）。

### 運輸貨物

[馬篷車或是船隻可以運送貨物](../Page/馬篷車.md "wikilink")。馬篷車被用來在陸路上運輸，可以有效的在城市間運送原料，或者將貨物運送到港口城市，或者與印地安部落、其他玩家的城市貿易。而船隻可以經由海路將貨物販賣至歐洲，或者從那裡購買原料。移民也必須由船隻從歐洲載運到新大陸。

### 土著關係

玩家可以選擇不同的方針，和土著採和平貿易、相互餽贈、學習的方針（可以選擇是否伴隨和平傳教的文化攻勢），或嘗試以武力征服，這些策略可以在遊戲的不同時期交互為用。選擇不同的文明和開國元老，也有助於不同方針的實行。

### 開國元老

遊戲設計者從歷史人物中選出有代表性的數十位人物，在遊戲中隨著自由意識的提升，將陸續響應玩家的號召加入陣營，成為有特殊助益的開國元老。

## 參見

  - [Freeciv](../Page/Freeciv.md "wikilink")
  - [开源游戏列表](../Page/开源游戏列表.md "wikilink")

## 參考資料

## 外部連結

  -
  - [The Linux Game Tome:
    FreeCol](https://web.archive.org/web/20030212102108/http://happypenguin.org/show?FreeCol)

  - [FreeCol](../Page/translatewiki:FreeCol.md "wikilink") on
    [translatewiki.net](../Page/translatewiki.net.md "wikilink")

  - [FreeCol](../Page/wikia:civilization:FreeCol.md "wikilink") on the
    [Civilization](../Page/wikia:civilization.md "wikilink")
    [Wikia](../Page/Wikia.md "wikilink")

  -
  - [FreeCol on HappyPenguin-The Linux Game
    Tome](http://happypenguin.altervista.org/gameshow.php?t=FreeCol)

[Category:2003年電子遊戲](../Category/2003年電子遊戲.md "wikilink")
[Category:4X電子遊戲](../Category/4X電子遊戲.md "wikilink")
[Category:Linux游戏](../Category/Linux游戏.md "wikilink")
[Category:MacOS遊戲](../Category/MacOS遊戲.md "wikilink")
[Category:Windows游戏](../Category/Windows游戏.md "wikilink")
[Category:自由软件](../Category/自由软件.md "wikilink")
[Category:回合制策略游戏](../Category/回合制策略游戏.md "wikilink")
[Category:地理大發現電子遊戲](../Category/地理大發現電子遊戲.md "wikilink")
[Category:开源游戏](../Category/开源游戏.md "wikilink")
[Category:免费游戏](../Category/免费游戏.md "wikilink")
[Category:用Java編程的自由軟體](../Category/用Java編程的自由軟體.md "wikilink")
[Category:開源策略遊戲](../Category/開源策略遊戲.md "wikilink")
[Category:Java平台遊戲](../Category/Java平台遊戲.md "wikilink")
[Category:等距視角遊戲](../Category/等距視角遊戲.md "wikilink")

1.
2.