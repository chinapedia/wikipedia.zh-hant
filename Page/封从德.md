[Feng_Congde_at_Tiananmen_University_of_Democracy_20140601.jpg](https://zh.wikipedia.org/wiki/File:Feng_Congde_at_Tiananmen_University_of_Democracy_20140601.jpg "fig:Feng_Congde_at_Tiananmen_University_of_Democracy_20140601.jpg")

**封从德**（），出生於[四川](../Page/四川.md "wikilink")，1989年[六四天安門事件被](../Page/六四天安門事件.md "wikilink")[北京市公安局通缉的](../Page/北京市公安局.md "wikilink")21名[学生运动领袖之一](../Page/学生运动.md "wikilink")。原就读于[北京大学](../Page/北京大学.md "wikilink")。1988年5月已与另一学生领袖[柴玲](../Page/柴玲.md "wikilink")[結婚](../Page/結婚.md "wikilink")\[1\]，八九学潮遭到镇压后二人共同前往[法国避难](../Page/法国.md "wikilink")；后柴玲又辗转到了[美国](../Page/美国.md "wikilink")，二人随即[离婚](../Page/离婚.md "wikilink")，封从德则继续留在法国读完了[博士](../Page/博士.md "wikilink")[学位](../Page/学位.md "wikilink")。现居美国[旧金山](../Page/旧金山.md "wikilink")\[2\]。

封从德于1982年入读[北京大学](../Page/北京大学.md "wikilink")，1986年保送北京大学遥感技术应用研究所（今北京大学遥感与地理信息系统研究所）研究[计算机识别](../Page/计算机识别.md "wikilink")[卫星图象](../Page/卫星图象.md "wikilink")[专家系统](../Page/专家系统.md "wikilink")，1988年获[高级程序员证书](../Page/高级程序员证书.md "wikilink")，1989年5月获[波士顿大学五年](../Page/波士顿大学.md "wikilink")[博士学位](../Page/博士学位.md "wikilink")[奖学金](../Page/奖学金.md "wikilink")。六四事件期间当选[北大筹委会常委](../Page/北大筹委会.md "wikilink")、[高自联主席](../Page/高自联.md "wikilink")，及任[天安门广场绝食团和](../Page/天安门广场绝食团.md "wikilink")[保卫天安门广场指挥部副总指挥](../Page/保卫天安门广场指挥部.md "wikilink")。封從德和其主持的廣播站，在1989年6月4日凌晨通過口頭表決的方式，成功地組織了[天安門廣場學生最後的撤離](../Page/天安門廣場.md "wikilink")，避免了更大的伤亡。

六四事件后，封从德在国内逃亡的十个月中感悟[中国传统文化价值](../Page/中国传统文化.md "wikilink")，遂弃理从文，出国后转入法国[高等研究應用學院宗教历史系](../Page/高等研究應用學院.md "wikilink")，1995年获[硕士文凭](../Page/硕士.md "wikilink")，1996年通过[博士候选资格答辩](../Page/博士.md "wikilink")。1997年在[荷兰](../Page/荷兰.md "wikilink")[莱顿大学研究](../Page/莱顿大学.md "wikilink")[儒家学说](../Page/儒家.md "wikilink")，1999年回法国撰写博士论文研究[道教和](../Page/道教.md "wikilink")[中医](../Page/中医.md "wikilink")，2003年7月获得博士学位。获得博士学位后，以设计[网络数据库为业](../Page/网络数据库.md "wikilink")。2013年為[天安门民主大学复校筹备组成員之一](../Page/天安门民主大学.md "wikilink")。现主编《[六四档案](http://www.64memo.com/)》网站，並主持天安门民主大学。

2003年，《[TVBS周刊](../Page/TVBS周刊.md "wikilink")》第318期披露，[王丹經常以](../Page/王丹.md "wikilink")[男同志身分出入台灣及美國的](../Page/男同志.md "wikilink")[同志酒吧或同志俱樂部尋歡作樂](../Page/同志酒吧.md "wikilink")，在[同性性行為中多扮女性身分](../Page/同性性行為.md "wikilink")。2003年12月7日，王丹在《[自由時報](../Page/自由時報.md "wikilink")》發文〈對於TVBS周刊抹黑報導的抗議與聲明〉，要求《TVBS周刊》向他道歉。2003年12月9日，《TVBS周刊》發表[公開信回應](../Page/公開信.md "wikilink")，他們根據「絕對可靠的消息來源」，並得到多位社會知名人士的證實，而絕非僅僅根據網路消息或「一名流亡詩人」所提供的內情。對此，封從德寫了一首[打油詩](../Page/打油詩.md "wikilink")〈贈王丹〉，諷刺王丹「朝求台灣財，暮伴台男睡」。

2014年8月20日，封從德在《[時報周刊](../Page/時報周刊.md "wikilink")》的專訪中說，[中國共產黨實際上明裡暗裡結構性地推波助瀾](../Page/中國共產黨.md "wikilink")，「推[明星以燬民運](../Page/明星.md "wikilink")」，二十幾年屢試不爽，民運組織幾無招架之力，王丹即是此類「明星領袖」之一；愛作秀的明星也愛[篡改歷史](../Page/篡改歷史.md "wikilink")，「[六四屠城之前](../Page/六四清场.md "wikilink")，群情激憤萬眾一心，就比誰都激進和高調；屠城之後，規避責任，就塗改得之前就比誰都溫和」，他的著作《六四日記》與本年7月他在《[開放雜誌](../Page/開放雜誌.md "wikilink")》發表的文章\[3\]即有細述王丹如何在《王丹回憶錄》中纂亂史實，[王有才](../Page/王有才.md "wikilink")、[熊焱](../Page/熊焱.md "wikilink")、[劉剛等等許多當事人的回憶亦多有見證](../Page/劉剛_\(民運人士\).md "wikilink")；若王丹真有高度的防共意識，王丹應該好好規勸其學生如[陳為廷](../Page/陳為廷.md "wikilink")，別在不同場合都穿著共產黨領袖[馬克思](../Page/馬克思.md "wikilink")、[列寧](../Page/列寧.md "wikilink")、[切格瓦拉頭像的](../Page/切格瓦拉.md "wikilink")[T恤](../Page/T恤.md "wikilink")，更不要一方面號稱防止未來的中共滲透、一方面卻現在就全文高歌中共黨歌《[國際歌](../Page/國際歌.md "wikilink")》\[4\]\[5\]。

封從德是堅定的“民國復興派”人士，2011年創設了[孫文學校](http://www.Sun1911.com/)。海內外中國大陸民主人士中，以[辛灝年](../Page/辛灝年.md "wikilink")、封從德為代表的“[民國派](../Page/民國派.md "wikilink")”認同[三民主義](../Page/三民主義.md "wikilink")，主張中國大陸重歸[中華民國](../Page/中華民國.md "wikilink")、重新施行《[中華民國憲法](../Page/中華民國憲法.md "wikilink")》。民國派的[司徒一認為](../Page/司徒一.md "wikilink")，[孫中山晚年著述所定格的三民主義是](../Page/孫中山.md "wikilink")《中華民國憲法》的基礎但不是羈絆，《中華民國憲法》在這一基礎上有創新和發展，孫中山身後各種流派的三民主義詮釋和發展（包括[中國國民黨主流理論](../Page/中國國民黨.md "wikilink")）都不能稱作《中華民國憲法》的基礎。

## 參看

  - [中華人民共和國持不同政見者列表](../Page/中華人民共和國持不同政見者列表.md "wikilink")
  - [新唐人電視台：「封從德」標籤](http://www.ntdtv.com/xtr/gb/articlelistbytag_%E5%B0%81%E4%BB%8E%E5%BE%B7.html)

## 注釋

<references />

[Category:中華人民共和國持不同政見者](../Category/中華人民共和國持不同政見者.md "wikilink")
[Category:八九民运学生领袖](../Category/八九民运学生领袖.md "wikilink")
[Category:萊頓大學校友](../Category/萊頓大學校友.md "wikilink")
[Category:波士顿大学校友](../Category/波士顿大学校友.md "wikilink")
[Category:北京大学校友](../Category/北京大学校友.md "wikilink")
[Category:中華人民共和國脫逃者](../Category/中華人民共和國脫逃者.md "wikilink")
[Category:都江堰人](../Category/都江堰人.md "wikilink")
[C從](../Category/封姓.md "wikilink")

1.  梁淑英，[學運夫妻──柴玲、封從德](http://1989report.hkja.org.hk/site/portal/Site.aspx?id=A27-96)
    ，[人民不會忘記](../Page/人民不會忘記.md "wikilink")
2.
3.
4.
5.