**金城武**（[日語](../Page/日語.md "wikilink")：，），暱稱**Aniki**，[臺灣](../Page/臺灣.md "wikilink")[台北市出生](../Page/台北市.md "wikilink")，台裔日本混血兒，[日本及](../Page/日本.md "wikilink")[臺灣](../Page/臺灣.md "wikilink")[男演員](../Page/男演員.md "wikilink")，由於出生於11日，十一合起來便是士，因此父親取名為武，合起來就是武士。2018年受邀為[奧斯卡評審](../Page/奧斯卡.md "wikilink")。

父親是[日本人](../Page/日本.md "wikilink")，母親為[台灣人](../Page/台灣.md "wikilink")，在台灣長大讀書，先後在日本、台灣、中国大陸及香港發展，接拍電影、電視劇及廣告等工作，住在台北。曾分別就讀[日僑學校與](../Page/臺北市日僑學校.md "wikilink")[台北美國學校](../Page/台北美國學校.md "wikilink")。

## 個人背景

### 經歷

年少時，金城武因想買[摩托車而答應拍攝](../Page/摩托車.md "wikilink")[廣告](../Page/廣告.md "wikilink")，踏入[演藝圈](../Page/演藝圈.md "wikilink")。早期曾和[劉若英一同擔任歌手](../Page/劉若英.md "wikilink")[陳昇的錄音室製作助理](../Page/陳昇.md "wikilink")，並一邊學習音樂製作的相關知識。1991年，華視八點檔第一部台灣的電視劇《[草地狀元](../Page/草地狀元_\(電視劇\).md "wikilink")》配角演出，並陸續拍攝[廣告](../Page/廣告.md "wikilink")、發行[唱片等](../Page/唱片.md "wikilink")，並踏入電影圈拍攝[王家衛與](../Page/王家衛.md "wikilink")[朱延平等多位導演的電影](../Page/朱延平.md "wikilink")。金城武在當時與[林志穎](../Page/林志穎.md "wikilink")、[吳奇隆及](../Page/吳奇隆.md "wikilink")[蘇有朋等人並稱](../Page/蘇有朋.md "wikilink")「台灣四小天王」，展開演歌雙棲之路線。1998年，金城武將演藝重心轉移至戲劇，同年，接演日本[富士電視臺的日劇](../Page/富士電視臺.md "wikilink")《[神啊！請多給我一點時間](../Page/神啊！請多給我一點時間.md "wikilink")》，劇中冷酷的音樂製作人一角，使其獲得日本[第18屆日劇學院賞最佳男主角獎](../Page/第18回日劇學院賞名單.md "wikilink")\[1\]，而此劇亦在日本及其他亞洲地區引起廣泛討論。2000年後，金城武與不少知名導演合作，如[張藝謀](../Page/張藝謀.md "wikilink")、[陳可辛](../Page/陳可辛.md "wikilink")、[劉偉強](../Page/劉偉強.md "wikilink")、[吳宇森等](../Page/吳宇森.md "wikilink")，使其演藝實力獲得廣泛肯定。因金城武會多種語言（[日語](../Page/日語.md "wikilink")、[英語](../Page/英語.md "wikilink")、[華語](../Page/華語.md "wikilink")、[臺語](../Page/臺語.md "wikilink")、[粵語等](../Page/粵語.md "wikilink")），使演藝範圍涵蓋全亞洲。2014年，与[吴宇森第二次合作电影](../Page/吴宇森.md "wikilink")《太平轮》。

### 信仰

1997年，金城武皈依[宗南嘉楚](../Page/宗南嘉楚.md "wikilink")[仁波切门下](../Page/仁波切.md "wikilink")，成为[藏传佛教弟子](../Page/藏传佛教.md "wikilink")。\[2\]

## 作品

### 電影

|                                                                          |                                                               |           |
| ------------------------------------------------------------------------ | ------------------------------------------------------------- | --------- |
| **年份**                                                                   | **電影名稱(港譯)**                                                  | '''角色 ''' |
|                                                                          |                                                               |           |
| 1993年                                                                    | 《[現代豪俠傳](../Page/現代豪俠傳.md "wikilink")》                        | 長空        |
| 1994年                                                                    | 《[沉默的姑娘](../Page/沉默的姑娘.md "wikilink")》                        | 古柏奇       |
| 《[第六感奇緣之人魚傳說](../Page/第六感奇緣之人魚傳說.md "wikilink")》                         | Kenji                                                         |           |
| 《[重慶森林](../Page/重慶森林.md "wikilink")》                                     | 223（警察）/何志武                                                   |           |
| 《[報告班長3](../Page/報告班長3.md "wikilink")》                                   | 金鐵生                                                           |           |
| 1995年                                                                    | 《[校園敢死隊](../Page/校園敢死隊.md "wikilink")》(學校霸王)                  | 老鷹        |
| 《[墮落天使](../Page/墮落天使_\(1995年電影\).md "wikilink")》                         | 何志武                                                           |           |
| 《臭屁王》(蠟筆小小生)                                                             | 阿武                                                            |           |
| 《[中國龍](../Page/中國龍_\(電影\).md "wikilink")》                                | 郝湯姆                                                           |           |
| 《[摩登笑探](../Page/冇面俾.md "wikilink")》                                      | 鄧川石                                                           |           |
| 《逃學戰警》(新紮師兄追女仔)                                                          | 金英俊                                                           |           |
| 1996年                                                                    | 《[冒險王](../Page/冒險王.md "wikilink")》                            | 阿城        |
| 《[天涯海角](../Page/天涯海角\(1996年電影\).md "wikilink")》                          | 那口蟲                                                           |           |
| 《[泡妞專家](../Page/泡妞專家.md "wikilink")》(重慶愛情感覺)                             | 王大利                                                           |           |
| 《[號角響起](../Page/號角響起.md "wikilink")》(四個不平凡的少年)                           | 李大維                                                           |           |
| 1997年                                                                    | 《[初纏戀後之二人世界](../Page/初纏戀後之二人世界.md "wikilink")》                | 林家棟       |
| 《[火燒島之橫行霸道](../Page/火燒島之橫行霸道.md "wikilink")》                             | 楊重                                                            |           |
| 《[馬永貞](../Page/馬永貞_\(電影\).md "wikilink")》                                | [馬永貞](../Page/馬永貞.md "wikilink")                              |           |
| 《[兩個只能活一個](../Page/兩個只能活一個.md "wikilink")》                               | 阿武                                                            |           |
| 《迷霧》(迷離花劫)                                                               | 若侍武弘                                                          |           |
| 《[神偷諜影](../Page/神偷諜影.md "wikilink")》                                     | Jackal                                                        |           |
| 1998年                                                                    | 《[安娜瑪德蓮娜](../Page/安娜馬德蓮娜.md "wikilink")》                      | 陳家富       |
| 《[我的愛人你的死神](../Page/我的愛人你的死神.md "wikilink")》                             | 研二                                                            |           |
| 《[不夜城](../Page/不夜城.md "wikilink")》                                       | 劉健一                                                           |           |
| 1999年                                                                    | 《[心動](../Page/心動.md "wikilink")》                              | 林浩君       |
| 2000年                                                                    | 《[薰衣草](../Page/薰衣草_\(電影\).md "wikilink")》                     | 天使        |
| 《[-{zh-cn:太空游侠; zh-tw:太空遊俠; zh-hk:極盜狂熱份子}-](../Page/太空遊俠.md "wikilink")》 | 西山保                                                           |           |
| 2002年                                                                    | 《[-{zh-hans:武者回歸;zh-hant:回歸者;}-](../Page/武者回歸.md "wikilink")》 | 宮本        |
| 2003年                                                                    | 《[向左走向右走](../Page/向左走向右走_\(電影\).md "wikilink")》               | 劉智康       |
| 2004年                                                                    | 《[十面埋伏](../Page/十面埋伏_\(電影\).md "wikilink")》                   | 金捕頭       |
| 2005年                                                                    | 《[如果·愛](../Page/如果·愛.md "wikilink")》                          | 林見東       |
| 2006年                                                                    | 《[傷城](../Page/傷城.md "wikilink")》                              | 丘健邦       |
| 2007年                                                                    | 《[投名狀](../Page/投名狀.md "wikilink")》                            | 姜午陽       |
| 2008年                                                                    | 《[死神的精準度](../Page/死神的精準度.md "wikilink")》                      | 死神千葉      |
| 《[赤壁](../Page/赤壁_\(電影\).md "wikilink")》                                  | [諸葛亮](../Page/諸葛亮.md "wikilink")                              |           |
| 《[K20：怪人二十面相](../Page/K20：怪人二十面相.md "wikilink")》                         | 遠藤平吉                                                          |           |
| 2009年                                                                    | 《[赤壁：決戰天下](../Page/赤壁：決戰天下.md "wikilink")》                    | 諸葛亮       |
| 2011年                                                                    | 《[武俠](../Page/武俠_\(電影\).md "wikilink")》                       | 徐百九       |
| 2014年                                                                    | 《[太平輪：亂世浮生](../Page/太平輪_\(電影\).md "wikilink")》                | 嚴澤坤       |
| 2015年                                                                    | 《[太平輪：驚濤摯愛](../Page/太平輪_\(電影\).md "wikilink")》                | 嚴澤坤       |
| 2016年                                                                    | 《[擺渡人](../Page/擺渡人_\(2016年電影\).md "wikilink")》                | 管春        |
| 2017年                                                                    | 《[喜歡你](../Page/男人手册.md "wikilink")》                           | 路晉        |
| 2019年                                                                    | 《[風林火山](../Page/風林火山_\(2019年電影\).md "wikilink")》              |           |

### 配音

|        |                                             |                   |         |
| ------ | ------------------------------------------- | ----------------- | ------- |
| **年份** | **電影名稱**                                    | **角色**            | **備註**  |
|        |                                             |                   |         |
|        |                                             |                   |         |
| 1999年  | 《[泰山（動畫）](../Page/泰山_\(電影\).md "wikilink")》 | 泰山（配音臺灣版、香港版、日本版） | 迪士尼動畫電影 |
|        |                                             |                   |         |

### 電視劇

|        |                                      |                                                    |         |
| ------ | ------------------------------------ | -------------------------------------------------- | ------- |
| **日期** | **電視台**                              | **劇名**                                             | **角色**  |
| 1991年  | [華視](../Page/華視.md "wikilink")       | 《[草地狀元](../Page/草地狀元.md "wikilink")》(鄉土劇)          | 陳乃建     |
| 1995年  |                                      | 《[富士彩色顯人生](../Page/富士彩色顯人生.md "wikilink")》（單元劇）    |         |
| 1996年  | [富士電視台](../Page/富士電視台.md "wikilink") | 《[聖誕前夕的奇蹟](../Page/聖誕前夕的奇蹟.md "wikilink")》（單元劇）    | 吳鐘明     |
| 1998年  | 富士電視台                                | 《[神啊！請多給我一點時間](../Page/神啊！請多給我一點時間.md "wikilink")》 | 石川啟吾    |
| 2000年  | 富士電視台                                | 《[二千年之戀](../Page/二千年之戀.md "wikilink")》             | 尤利·馬洛耶夫 |
| 2002年  | [NTV](../Page/NTV.md "wikilink")     | 《[黃金保齡球](../Page/黃金保齡球.md "wikilink")》             | 芥川周     |
|        |                                      |                                                    |         |

### 電玩遊戲配音

|        |                                                                                        |        |
| ------ | -------------------------------------------------------------------------------------- | ------ |
| **年份** | **遊戲**                                                                                 | **角色** |
| 2001年  | 《[鬼武者](../Page/鬼武者.md "wikilink")》（[PS2遊戲](../Page/PlayStation_2.md "wikilink")）       | 明智左馬介  |
| 2003年  | 《[鬼武者 無頼伝](../Page/鬼武者系列.md "wikilink")》（[PS2遊戲](../Page/PlayStation_2.md "wikilink")） |        |
| 2004年  | 《[鬼武者3](../Page/鬼武者3.md "wikilink")》（[PS2遊戲](../Page/PlayStation_2.md "wikilink")）     |        |
| 2019年  | 《[鬼武者 高清移植版](../Page/鬼武者.md "wikilink")》（[PC遊戲](../Page/PC.md "wikilink")）             |        |

### 紀錄片

|        |        |         |        |                         |
| ------ | ------ | ------- | ------ | ----------------------- |
| **年份** | **類型** | **電視台** | **語言** | **名稱**                  |
| 2003年  | 紀錄片    | NHK     | 英語、日語  | 《金城武南極探險之旅 金城武の極地任務》DVD |
|        |        |         |        |                         |

## 音樂

### 專輯

<table>
<thead>
<tr class="header">
<th style="text-align: center;"><p>發行日期</p></th>
<th style="text-align: center;"><p>專輯名稱</p></th>
<th style="text-align: center;"><p>發行公司</p></th>
<th style="text-align: center;"><p>語 言</p></th>
<th><p>color:white" width="200px" align="center"|曲 目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p>1992年（臺灣）</p></td>
<td style="text-align: center;"><p>《<a href="../Page/分手的夜裡.md" title="wikilink">分手的夜裡</a>》</p></td>
<td style="text-align: center;"><p>福隆製作<br />
寶麗金唱片</p></td>
<td style="text-align: center;"><p>國語、臺語</p></td>
<td></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1993年（臺灣）</p></td>
<td style="text-align: center;"><p>《<a href="../Page/只要你和我.md" title="wikilink">只要你和我</a>》</p></td>
<td style="text-align: center;"><p>福隆製作<br />
寶麗金唱片</p></td>
<td style="text-align: center;"><p>國語</p></td>
<td></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>1993年（香港）</p></td>
<td style="text-align: center;"><p>《<a href="../Page/可依靠的好朋友.md" title="wikilink">可依靠的好朋友</a>》</p></td>
<td style="text-align: center;"><p>年代唱片<br />
新藝寶唱片</p></td>
<td style="text-align: center;"><p>國語</p></td>
<td></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1994年（臺灣）</p></td>
<td style="text-align: center;"><p>《<a href="../Page/溫柔超人.md" title="wikilink">溫柔超人</a>》</p></td>
<td style="text-align: center;"><p>福隆製作<br />
寶麗金唱片</p></td>
<td style="text-align: center;"><p>國語</p></td>
<td></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>1994年（臺灣）</p></td>
<td style="text-align: center;"><p>《<a href="../Page/失約.md" title="wikilink">失約</a>》</p></td>
<td style="text-align: center;"><p>年代唱片<br />
EMI唱片</p></td>
<td style="text-align: center;"><p>粵語、國語、日語</p></td>
<td></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1994年（臺灣）</p></td>
<td style="text-align: center;"><p>《<a href="../Page/標準情人.md" title="wikilink">標準情人</a>》</p></td>
<td style="text-align: center;"><p>福隆製作<br />
EMI唱片</p></td>
<td style="text-align: center;"><p>國語</p></td>
<td></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>1995年（香港）</p></td>
<td style="text-align: center;"><p>《<a href="../Page/給我心愛的人.md" title="wikilink">給我心愛的人</a>》</p></td>
<td style="text-align: center;"><p>年代唱片<br />
EMI唱片</p></td>
<td style="text-align: center;"><p>粵語</p></td>
<td></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1995年（臺灣）</p></td>
<td style="text-align: center;"><p>《<a href="../Page/偷偷的醉.md" title="wikilink">偷偷的醉</a>》</p></td>
<td style="text-align: center;"><p>福隆製作<br />
EMI唱片</p></td>
<td style="text-align: center;"><p>國語</p></td>
<td></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>1996年（臺灣）</p></td>
<td style="text-align: center;"><p>《<a href="../Page/多苦都願意.md" title="wikilink">多苦都願意</a>》</p></td>
<td style="text-align: center;"><p>福隆製作<br />
EMI唱片</p></td>
<td style="text-align: center;"><p>國語</p></td>
<td></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1996年（臺灣）</p></td>
<td style="text-align: center;"><p>《<a href="../Page/精選集memories.md" title="wikilink">精選集memories</a>》</p></td>
<td style="text-align: center;"><p>EMI唱片</p></td>
<td style="text-align: center;"><p>國語、粵語</p></td>
<td></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>1998年（臺灣）</p></td>
<td style="text-align: center;"><p>《<a href="../Page/金城武的精選歌集.md" title="wikilink">金城武的精選歌集</a>》</p></td>
<td style="text-align: center;"><p>福隆製作<br />
千禧年代</p></td>
<td style="text-align: center;"><p>國語、粵語、日語</p></td>
<td></td>
</tr>
<tr class="even">
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td></td>
</tr>
</tbody>
</table>

### 單曲

|              |                                                    |              |                                                                        |
| ------------ | -------------------------------------------------- | ------------ | ---------------------------------------------------------------------- |
| **年份**       | **原聲帶名稱**                                          | **歌曲**       | **備註**                                                                 |
| 1993年        | \-                                                 | 心之路          | [台灣](../Page/台灣.md "wikilink")[心路基金會公益歌曲](../Page/心路基金會.md "wikilink") |
| 2005年        | 《[如果愛](../Page/如果愛.md "wikilink")》                 | 忘了我是誰（與周迅合唱） |                                                                        |
| 美麗故事（與池珍熙合唱） |                                                    |              |                                                                        |
| 十字街頭（與周迅合唱）  | 獲[第43屆金馬獎最佳原創電影歌曲獎](../Page/第43屆金馬獎.md "wikilink") |              |                                                                        |
| 假如           |                                                    |              |                                                                        |
|              |                                                    |              |                                                                        |

## 寫真集

  - 『made in HEAVEN』（[角川書店](../Page/角川書店.md "wikilink")、1997年7月）ISBN
    978-4048528023
  - 『金城武寫真集　RYU★KENICHI』（角川書店、1998年5月）　ISBN 978-4048529327
  - 『君のいた永遠(とき)』（角川書店、1999年9月）ISBN 978-4048531313
  - 『金城武寫真集　Returner MIYAMOTO』（角川書店、2002年8月）ISBN 978-4048535465
  - 『LOVERS　PHOTO BOOK』（角川書店、2004年9月）ISBN 978-4048537841
  - 金城武寫真集　Chiba Takeshi Kaneshiro（角川書店、2008年3月）ISBN 978-4048949163

## 獎項紀錄

<table>
<thead>
<tr class="header">
<th style="text-align: center;"><p>年份</p></th>
<th style="text-align: center;"><p>獲提名</p></th>
<th style="text-align: center;"><p>獎項</p></th>
<th style="text-align: center;"><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p>1993年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p><a href="../Page/第十六屆十大中文金曲得獎名單.md" title="wikilink">第16屆香港十大中文金曲頒獎禮最有前途新人獎銀獎</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>1998年</p></td>
<td style="text-align: center;"><p>《<a href="../Page/神啊！請多給我一點時間.md" title="wikilink">神啊！請多給我一點時間</a>》</p></td>
<td style="text-align: center;"><p><a href="../Page/第18回日劇學院賞名單.md" title="wikilink">第18回日劇學院賞主演男優賞</a>（最佳男主角）</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>1999年</p></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"><p>（日本電影電視製作人協會獎）特別賞（特別獎）</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2002年</p></td>
<td style="text-align: center;"><p>《<a href="../Page/黃金保齡球.md" title="wikilink">黃金保齡球</a>》</p></td>
<td style="text-align: center;"><p><a href="../Page/第33回日劇學院賞名單.md" title="wikilink">第33回日劇學院賞主演男優賞</a>（最佳男主角）</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2006年</p></td>
<td style="text-align: center;"><p>《<a href="../Page/如果愛.md" title="wikilink">如果愛</a>》</p></td>
<td style="text-align: center;"><p>第8屆中國<a href="../Page/長春電影節.md" title="wikilink">長春電影節最佳男主角</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>2008年</p></td>
<td style="text-align: center;"><p>《<a href="../Page/投名狀.md" title="wikilink">投名狀</a>》</p></td>
<td style="text-align: center;"><p>第9屆中國<a href="../Page/長春電影節.md" title="wikilink">長春電影節最佳男配角</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>2017年</p></td>
<td style="text-align: center;"><p>《<a href="../Page/擺渡人_(2016年電影).md" title="wikilink">擺渡人</a>》</p></td>
<td style="text-align: center;"><p><a href="../Page/第54屆金馬獎.md" title="wikilink">第54屆金馬獎最佳男主角</a></p></td>
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

### 其他

  - 世上最英俊的臉龐100人
      - 2013年 - 81位
      - 2014年 - 29位
      - 2015年 - 73位
      - 2016年 - 93位

## 參考文獻

## 外部連結

  - [金城武.NET](http://www.takeshikaneshiro.net)

  - [福隆經紀公司金城武官方網頁](http://www.fulongstars.com/takeshi/index.htm)

  - 中文電影資料庫 [金城武](http://www.dianying.com/ft/person/JinChengwu)

  -
  -
  -
  -
  -
[Category:台灣男歌手](../Category/台灣男歌手.md "wikilink")
[Category:臺灣電視男演員](../Category/臺灣電視男演員.md "wikilink")
[Category:臺灣電影男演員](../Category/臺灣電影男演員.md "wikilink")
[Category:台灣男性模特兒](../Category/台灣男性模特兒.md "wikilink")
[Category:台灣佛教徒](../Category/台灣佛教徒.md "wikilink")
[Category:日本男歌手](../Category/日本男歌手.md "wikilink")
[Category:日本電影演員](../Category/日本電影演員.md "wikilink")
[Category:日本男性模特兒](../Category/日本男性模特兒.md "wikilink")
[Category:琉球演員](../Category/琉球演員.md "wikilink")
[Category:琉球電影演員](../Category/琉球電影演員.md "wikilink")
[Category:台北市人](../Category/台北市人.md "wikilink")
[Category:在台灣的琉球人](../Category/在台灣的琉球人.md "wikilink")
[Category:琉球裔混血兒](../Category/琉球裔混血兒.md "wikilink")
[Category:藏傳佛教徒](../Category/藏傳佛教徒.md "wikilink")
[Category:华裔混血儿](../Category/华裔混血儿.md "wikilink")
[Category:在中国的日本人](../Category/在中国的日本人.md "wikilink")
[Category:日劇學院賞最佳男主角得主](../Category/日劇學院賞最佳男主角得主.md "wikilink")
[Category:金馬獎電影音樂獎項獲獎者](../Category/金馬獎電影音樂獎項獲獎者.md "wikilink")
[Category:福隆經紀公司旗下藝人](../Category/福隆經紀公司旗下藝人.md "wikilink")
[Category:華人電影演員](../Category/華人電影演員.md "wikilink")
[Category:台裔混血兒](../Category/台裔混血兒.md "wikilink")
[Category:琉球族歌手](../Category/琉球族歌手.md "wikilink")
[Category:琉球族演員](../Category/琉球族演員.md "wikilink")
[Category:琉球族男性模特兒](../Category/琉球族男性模特兒.md "wikilink")

1.
2.