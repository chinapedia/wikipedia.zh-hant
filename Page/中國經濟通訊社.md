**中國經濟通訊社**（簡稱**中經社**；[英語](../Page/英語.md "wikilink")：**C**hina
**E**conomic **N**ews
**S**ervice，**CENS**）是[王惕吾於](../Page/王惕吾.md "wikilink")1974年6月1日在台灣創辦的一個以[英文發行經貿雜誌的](../Page/英文.md "wikilink")[出版社](../Page/出版社.md "wikilink")，隸屬[聯合報系](../Page/聯合報系.md "wikilink")。2007年由[王必成接任董事長](../Page/王必成.md "wikilink")\[1\]\[2\]。

## 資料來源

[Z中](../Category/台灣出版社.md "wikilink")
[Z中](../Category/台灣通訊社.md "wikilink")
[Category:1974年成立的公司](../Category/1974年成立的公司.md "wikilink")
[Z中](../Category/總部位於新北市的工商業機構.md "wikilink")
[Category:汐止區](../Category/汐止區.md "wikilink")
[Category:1974年台灣建立](../Category/1974年台灣建立.md "wikilink")

1.
2.