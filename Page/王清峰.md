**王清峰**，台南人，中華民國（[台灣](../Page/台灣.md "wikilink")）[律師](../Page/律師.md "wikilink")、前任[中華民國法務部部長](../Page/中華民國法務部.md "wikilink")，曾任[監察委員](../Page/監察委員.md "wikilink")。現任[中華民國紅十字會總會長](../Page/中華民國紅十字會.md "wikilink")。
王清峰曾與[陳履安搭檔參選第九任](../Page/陳履安.md "wikilink")[1996年總統選舉](../Page/1996年中華民國總統選舉.md "wikilink")，是中華民國史上第一位參與總統選舉的女性候選人（副總統），但並未當選。後擔任法務部長，因個人宗教信仰與信念不願執行死刑，自行辭職。

## 簡歷

出生於[臺南市](../Page/臺南市.md "wikilink")，父**王生爨**為[台南市資深](../Page/台南市.md "wikilink")[小兒科醫師](../Page/小兒科.md "wikilink")。高中就讀[臺北市立第一女子高級中學](../Page/臺北市立第一女子高級中學.md "wikilink")、後考取[國立政治大學](../Page/國立政治大學.md "wikilink")。曾獲得十三屆十大傑出女青年\[1\]。

1987年，[媒體報導大量少女遭父母所賣](../Page/台灣媒體.md "wikilink")，被逼從娼，為長期從事雛妓救援及協助工作，於8月成立「[婦女救援基金會](../Page/婦女救援基金會.md "wikilink")」。

1990年，成立「百合中途之家」收容中心。

1992年，成立「慰安婦申訴專線」，開始展開受害人認證訪查及協助工作，致力於台籍[慰安婦真相之揭發](../Page/慰安婦.md "wikilink")。

1997年，開設婚姻受暴婦女專線，投入協助工作。

1998年，訪問台籍[慰安婦](../Page/慰安婦.md "wikilink")，拍攝成「阿媽的秘密——台籍慰安婦的故事」紀錄片，該片獲1998年[金馬獎最佳紀錄片獎](../Page/金馬獎.md "wikilink")。

1999年，推動「強姦罪告訴乃論改非告訴乃論」及「若罰[娼亦罰嫖](../Page/妓女.md "wikilink")」相關修法工作、並出版「台灣慰安婦報告」一書。

1999年，[921地震後即投入災後重建工作](../Page/921地震.md "wikilink")，募款並參與協助災民房屋重建，其後並輔導農民改種台灣本土優良肉桂樹，增加其收益並改善水土保持，迄今仍努力協助農民開發肉桂不同商業用途、增加銷路。

2001年，[景文案爆發後](../Page/景文案.md "wikilink")，受委任接管[景文技術學院](../Page/景文技術學院.md "wikilink")。

2004年，應邀擔任[319槍擊案真相調查委員會委員](../Page/319槍擊案.md "wikilink")。

2005年，[中華民國任務型國代選舉中](../Page/2005年中華民國任務型國代選舉.md "wikilink")，獲[張亞中等](../Page/張亞中.md "wikilink")150人聯盟提名並當選，但未就職即退出該聯盟，放棄代表資格。

2008年4月21日，候任[行政院長](../Page/行政院長.md "wikilink")[劉兆玄公佈出任](../Page/劉兆玄.md "wikilink")[馬英九就任](../Page/馬英九.md "wikilink")[總統後的法務部部長](../Page/中華民國總統.md "wikilink")，並在2008年5月20日宣誓就職。就任後的首件人事案即是駁回前任部長[施茂林的回任退休案](../Page/施茂林.md "wikilink")，王清峰只表示「不宜、不妥」而未表示其他理由。但人事行政局與銓敘部皆隨後表示，回任公務員屬於施茂林法律上權益，最後施茂林以台北地檢署檢察長身分辦理退休，罕見地成為政務官回任一審檢察體系職務的首例\[2\]，隨後施茂林發表公開信表示駁回回任係屬政治清算。\[3\]

2010年3月11日，因公開表態[廢除死刑](../Page/中華民國死刑制度.md "wikilink")，拒絕依法簽署四十餘位死刑犯的死刑執行令，並對檢察官施壓，希望他們不要以死刑起訴。事件引起爭議，被迫下台負責。

2012年5月25日，當選[中華民國紅十字會第](../Page/中華民國紅十字會.md "wikilink")20任總會長。

## 參選副總統

王清峰曾與[陳履安搭檔參選](../Page/陳履安.md "wikilink")[第九任（1996年）總統選舉](../Page/1996年中華民國總統選舉.md "wikilink")，是中華民國史上第一位參與總統選舉的女性候選人（副總統），但並未當選。

<table>
<thead>
<tr class="header">
<th><p>總統候選人</p></th>
<th><p>副總統候選人</p></th>
<th><p>黨派</p></th>
<th><p>總得票數</p></th>
<th><p>得票率</p></th>
<th><p>當選</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/李登輝.md" title="wikilink">李登輝</a></p></td>
<td><p><a href="../Page/連戰.md" title="wikilink">連戰</a></p></td>
<td></td>
<td><p>5,813,699</p></td>
<td><p>54.0%</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/彭明敏.md" title="wikilink">彭明敏</a></p></td>
<td><p><a href="../Page/謝長廷.md" title="wikilink">謝長廷</a></p></td>
<td></td>
<td><p>2,274,586</p></td>
<td><p>21.1%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/林洋港.md" title="wikilink">林洋港</a></p></td>
<td><p><a href="../Page/郝柏村.md" title="wikilink">郝柏村</a></p></td>
<td><p>獨立參選人</p></td>
<td><p>1,603,790</p></td>
<td><p>14.9%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳履安.md" title="wikilink">陳履安</a></p></td>
<td><p><strong>王清峰</strong></p></td>
<td><p>獨立參選人</p></td>
<td><p>1,074,044</p></td>
<td><p>9.98%</p></td>
<td></td>
</tr>
</tbody>
</table>

## 爭議事件

### 廢除死刑爭議

爭議的直接觸發點是，[國民黨](../Page/國民黨.md "wikilink")[立委](../Page/立委.md "wikilink")[吳育昇在](../Page/吳育昇.md "wikilink")2010年2月22日立法院總質詢，質疑行政院
4
年從未執行死刑，要求行政院長[吳敦義表態](../Page/吳敦義.md "wikilink")。王清峰後來反駁吳育昇，反對王清峰的一方指她行政怠惰，爭拗展開。社會大眾普遍認為王為一己之價值觀及宗教信仰，置國家律政維持於不顧，已明確涉及公務員瀆職行為，面對民間反彈聲浪四起，王的固執己見嚴重衝擊行政團隊形象，執政黨內部開始出現要求革去王法務部長職位的呼聲。而直到其卸任，王共積壓四十餘死囚而不執行。

3月9日，親自撰文「理性與寬容─暫停執行死刑」推動廢除死
刑政策\[4\]，更直言寧可下台，也絕不批准任何一件死刑執行，以保護死刑犯的生命權和人權。過往44件已判死刑的案子也絕不簽署執行。此外，王清峰也曾表示願意代替死刑犯執行，就算為死刑犯下地獄都甘心；她表示任內不會執行死刑，即便丟官也在所不惜。\[5\]她強調，「並沒有忘記被害人的痛苦，
也沒有看不到他們的眼淚」，她認為基於對生命權的尊重，在有周延的配套下，才會全面廢除死刑。\[6\]

3月11日，[民進黨籍](../Page/民進黨.md "wikilink")[立法委員](../Page/立法委員.md "wikilink")[邱議瑩表示](../Page/邱議瑩.md "wikilink")，如果王清峰堅持廢除死刑，當初就應該拒絕接任法務部長，王清峰下臺也未嘗不是一種負責任的表現。\[7\]

3月11日，[國民黨籍](../Page/國民黨.md "wikilink")[台北市長](../Page/台北市長.md "wikilink")[郝龍斌亦表示](../Page/郝龍斌.md "wikilink")，他個人不贊成廢除死刑，認為應該待有社會共識再來考量比較妥當。\[8\]

3月20日，[東森電視的政治評論節目](../Page/東森電視.md "wikilink")《關鍵報告》提出質疑：「累積五年，共44件死刑未執行，然而2008年5月20日，王清峰才宣誓就職，究竟真正該負責的是誰？」統計數據中顯示，近20年來國民黨執政時，每年槍決死刑犯均為數十人不等，當年馬英九擔任法務部長時，在野的民進黨堅決反對執行死刑。直到民進黨執政後，每年槍決的死刑犯降到個位數，且逐年降低，民進黨執政時的法務部長[施茂林](../Page/施茂林.md "wikilink")，雖然於2005年12月26日簽署槍決林盟凱和林信宏二名死刑犯、但之後開始全面拒簽。

3月10日，根據[聯合報於晚間所進行的民調顯示](../Page/聯合報.md "wikilink")，僅1成2支持廢除死刑、另不到1成民眾贊成暫緩死刑；有7成4民眾反對廢除死刑、且有4成2民眾認為王清峰應該為廢除死刑下台負責。\[9\]

3月11日，知名藝人[白冰冰表示](../Page/白冰冰.md "wikilink")，王清峰根本就「不懂法律」，不適任法務部長一職，認為「王清峰是在告訴我們，未來在台灣只要犯了罪，你可以找人被執行，因為她可以代替別人被執行。」\[10\]同日晚間王清峰向[行政院院長](../Page/行政院院長.md "wikilink")[吳敦義請辭獲准](../Page/吳敦義.md "wikilink")。\[11\]

## 參照

<div class="references-small">

<references />

</div>

## 外部連結

|- |colspan="3"
style="text-align:center;"|**[ROC_Executive_Yuan_Logo.svg](https://zh.wikipedia.org/wiki/File:ROC_Executive_Yuan_Logo.svg "fig:ROC_Executive_Yuan_Logo.svg")[行政院](../Page/行政院.md "wikilink")**
|-

[Category:台灣律師](../Category/台灣律師.md "wikilink")
[Category:中華民國紅十字會會長](../Category/中華民國紅十字會會長.md "wikilink")
[Category:中華民國副總統參選人](../Category/中華民國副總統參選人.md "wikilink")
[Category:中華民國法務部部長](../Category/中華民國法務部部長.md "wikilink")
[Category:第二屆監察委員](../Category/第二屆監察委員.md "wikilink")
[Category:臺灣廢除死刑運動者](../Category/臺灣廢除死刑運動者.md "wikilink")
[Category:臺灣女性權利運動者](../Category/臺灣女性權利運動者.md "wikilink")
[Category:國立政治大學校友](../Category/國立政治大學校友.md "wikilink")
[Category:臺北市立第一女子高級中學校友](../Category/臺北市立第一女子高級中學校友.md "wikilink")
[Category:女性律師](../Category/女性律師.md "wikilink")
[Category:律師出身的政治人物](../Category/律師出身的政治人物.md "wikilink")
[Category:台灣佛教徒](../Category/台灣佛教徒.md "wikilink")
[Category:閩南裔臺灣人](../Category/閩南裔臺灣人.md "wikilink")
[Category:台南市人](../Category/台南市人.md "wikilink")
[Q清](../Category/王姓.md "wikilink")

1.  [第九任總統副總統選舉選舉公報](http://park.org/Taiwan/Government/Events/Presidential_Election/html/pet0001.htm)，中央選舉委員會
2.
3.
4.  全文內容：[理性與寬容─暫停執行死刑](http://www.moj.gov.tw/public/Attachment/031016413364.pdf)
5.
6.
7.
8.
9.
10.
11.