是一个 [自由](../Page/自由软件.md "wikilink")，[开源](../Page/开源.md "wikilink")
的[媒体播放器](../Page/媒体播放器.md "wikilink")。目前可以在
[Windows](../Page/Windows.md "wikilink")，[Windows
CE](../Page/Windows_CE.md "wikilink")／[Windows
Mobile](../Page/Windows_Mobile.md "wikilink")／[Palm
OS](../Page/Palm_OS.md "wikilink") 操作系统上運作。它可以播放包括
[ASF](../Page/ASF.md "wikilink")，[ASX](../Page/Advanced_Stream_Redirector.md "wikilink")，[AVI](../Page/AVI.md "wikilink")，[DIVX](../Page/DIVX.md "wikilink")，[M2V](../Page/M2V.md "wikilink")，[M3U](../Page/M3U.md "wikilink")，[MKV](../Page/MKV.md "wikilink")，[MP3](../Page/MP3.md "wikilink")，[MP2](../Page/MP2.md "wikilink")，[MPEG](../Page/MPEG.md "wikilink")，[MPG](../Page/MPEG-1.md "wikilink")，[mpeg4](../Page/mpeg4.md "wikilink")，[OGG](../Page/Ogg.md "wikilink")，[OGM](../Page/OGM.md "wikilink")，[WAV](../Page/WAV.md "wikilink")，[WMV在内的众多格式的影片](../Page/WMV.md "wikilink")

## 外部链接

  - [TCPMP
    主页](https://web.archive.org/web/20051216132514/http://tcpmp.corecodec.org/about)
  - [corecodec 主页](http://corecodec.com/)

[Category:媒体播放器](../Category/媒体播放器.md "wikilink")
[Category:开放源代码](../Category/开放源代码.md "wikilink")