**齊湣王**（，約40歲），又作**[齊閔王](../Page/齊閔王.md "wikilink")**，本名**田地**，一名**遂**。在位十七年，屢建武功，破[秦](../Page/秦.md "wikilink")、[燕諸國](../Page/燕.md "wikilink")，制[楚](../Page/楚.md "wikilink")，滅[宋](../Page/宋国.md "wikilink")。齊湣王三年（公元前298年），派[孟嘗君入秦](../Page/孟嘗君.md "wikilink")\[1\]，[秦昭襄王任孟尝君為](../Page/秦昭襄王.md "wikilink")[宰相](../Page/宰相.md "wikilink")，後被扣押，賴雞鳴狗盜[食客之助得脫](../Page/食客.md "wikilink")，後任齊相\[2\]。不久齊湣王整頓[稷下學宮](../Page/稷下學宮.md "wikilink")，孟嘗君憂功高震主，出奔至[魏國](../Page/魏國.md "wikilink")。

[周赧王元年](../Page/周赧王.md "wikilink")（前314年），湣王的父親[齊宣王派軍](../Page/齊宣王.md "wikilink")[攻破燕國](../Page/齊破燕之戰.md "wikilink")，[燕王噲](../Page/燕王噲.md "wikilink")、[太子平及](../Page/太子平.md "wikilink")[宰相](../Page/宰相.md "wikilink")[子之皆死](../Page/子之.md "wikilink")\[3\]。周赧王三年（前312年）燕人立[燕昭王](../Page/燕昭王.md "wikilink")，亟思復仇的燕昭王採納[樂毅及](../Page/樂毅.md "wikilink")[蘇秦建議](../Page/蘇秦.md "wikilink")，爭取[盟國](../Page/盟國.md "wikilink")，孤立[齊國](../Page/齊國.md "wikilink")，蘇秦兩次入齊離間，齊湣王相繼西向攻打秦國。

周赧王二十七年（前288年），[齊秦互帝](../Page/齊秦互帝.md "wikilink")。周赧王二十九年(前286年)，[齊滅宋](../Page/齊滅宋.md "wikilink")。\[4\]

周赧王三十一年（前284年）[濟西之戰](../Page/濟西之戰.md "wikilink")，燕國上將軍樂毅以五國聯軍攻齊，大敗齊軍；燕軍又於[臨淄城西之秦周大敗齊軍](../Page/臨淄.md "wikilink")，[達子戰死](../Page/達子.md "wikilink")，燕軍攻入臨淄，湣王出逃至[莒](../Page/莒.md "wikilink")（今[山東](../Page/山東.md "wikilink")[莒縣](../Page/莒縣.md "wikilink")），但被名義上協防的[楚國將軍](../Page/楚國.md "wikilink")[淖齒所殺](../Page/淖齒.md "wikilink")\[5\]。

## 稗官野史

相传齐湣王战败后，走投无路，被飞来的凤凰所救。\[6\]

## 家庭

  - 父母

<!-- end list -->

  - [齊宣王田辟疆](../Page/齊宣王.md "wikilink")

<!-- end list -->

  - 妻妾

<!-- end list -->

  - [宿瘤女](../Page/宿瘤女.md "wikilink")，齊國東郭采桑之女。早卒。
  - [齊湣太后](../Page/齊湣太后.md "wikilink")（《說苑·奉使》「王(太子法章)與太后奔于莒」）

<!-- end list -->

  - 子女

<!-- end list -->

  - [齊襄王田法章](../Page/齊襄王.md "wikilink")
  - [田關](../Page/田關.md "wikilink")，以田氏遠祖陳胡公諡號為氏，改姓胡，為胡關。

## 图集

[File:齐湣王1.jpg|太和殿顶相传为齐湣王化身的](File:齐湣王1.jpg%7C太和殿顶相传为齐湣王化身的)[骑凤仙人形象](../Page/骑凤仙人.md "wikilink")1
[File:齐湣王2.jpg|太和殿顶相传为齐湣王化身的骑凤仙人形象2](File:齐湣王2.jpg%7C太和殿顶相传为齐湣王化身的骑凤仙人形象2)
[File:齐湣王3.jpg|太和殿顶相传为齐湣王化身的骑凤仙人形象3](File:齐湣王3.jpg%7C太和殿顶相传为齐湣王化身的骑凤仙人形象3)
[File:齐湣王4.jpg|太和殿顶相传为齐湣王化身的骑凤仙人形象4](File:齐湣王4.jpg%7C太和殿顶相传为齐湣王化身的骑凤仙人形象4)
[File:齐湣王5.jpg|太和殿顶相传为齐湣王化身的骑凤仙人形象5](File:齐湣王5.jpg%7C太和殿顶相传为齐湣王化身的骑凤仙人形象5)
[File:齐湣王6.jpg|太和殿顶相传为齐湣王化身的骑凤仙人形象6](File:齐湣王6.jpg%7C太和殿顶相传为齐湣王化身的骑凤仙人形象6)

## 在位年與西曆對照表

<div class="NavFrame" style="clear: both; border: 1px solid #999; margin: 0.5em auto;">

<div class="NavHead" style="background-color: #CCCCFF; font-size: 100%; border-left: 3em soli; text-align: center; font-weight: bold;">

在位年與西曆對照表

</div>

<div class="NavContent" style="padding: 1em 0 0 0; font-size: 100%; text-align: center;">

| 齊湣王                            | 元年                             | 2年                             | 3年                             | 4年                             | 5年                             | 6年                             | 7年                             | 8年                             | 9年                             | 10年                            |
| ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| 西元                             | 前300年                          | 前299年                          | 前298年                          | 前297年                          | 前296年                          | 前295年                          | 前294年                          | 前293年                          | 前292年                          | 前291年                          |
| [干支](../Page/干支.md "wikilink") | [辛酉](../Page/辛酉.md "wikilink") | [壬戌](../Page/壬戌.md "wikilink") | [癸亥](../Page/癸亥.md "wikilink") | [甲子](../Page/甲子.md "wikilink") | [乙丑](../Page/乙丑.md "wikilink") | [丙寅](../Page/丙寅.md "wikilink") | [丁卯](../Page/丁卯.md "wikilink") | [戊辰](../Page/戊辰.md "wikilink") | [己巳](../Page/己巳.md "wikilink") | [庚午](../Page/庚午.md "wikilink") |
| 齊湣王                            | 11年                            | 12年                            | 13年                            | 14年                            | 15年                            | 16年                            | 17年                            |                                |                                |                                |
| 西元                             | 前290年                          | 前289年                          | 前288年                          | 前287年                          | 前286年                          | 前285年                          | 前284年                          |                                |                                |                                |
| [干支](../Page/干支.md "wikilink") | [辛未](../Page/辛未.md "wikilink") | [壬申](../Page/壬申.md "wikilink") | [癸酉](../Page/癸酉.md "wikilink") | [甲戌](../Page/甲戌.md "wikilink") | [乙亥](../Page/乙亥.md "wikilink") | [丙子](../Page/丙子.md "wikilink") | [丁丑](../Page/丁丑.md "wikilink") |                                |                                |                                |

</div>

</div>

## 参考文献

### 引用

### 书目

  - 《先秦诸子系年考辨》：一二八、齐湣王在位十八年非四十年其元年为周赧王十五年非周显王四十六年辨

{{-}}

[Category:齊國君主](../Category/齊國君主.md "wikilink")
[Category:遇刺身亡的周朝诸侯国君主](../Category/遇刺身亡的周朝诸侯国君主.md "wikilink")

1.  《史記·孟嘗君列傳第十五》：齐湣王二十五年，复卒使孟尝君入秦，秦昭襄王即以孟尝君为秦相。
2.  《史記·孟嘗君列傳第十五》：湣王不自得，以其遣孟尝君。孟尝君至，则以为齐相，任政。
3.  《史記·燕召公世家第四》：齊宣王因令章子将五都之兵，以因北地之众以伐燕。士卒不战，城门不闭，燕王哙死，齐大胜。
4.  《史記·田敬仲完世家第十六》
5.  《戰國策·齊策六·齐负郭之民有狐晅者》：王奔莒，淖齿数之......于是杀闵王于鼓-{里}-。
6.