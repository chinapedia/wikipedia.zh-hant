**A-POP**的「A」取自[日本](../Page/日本.md "wikilink")[秋葉原的簡稱](../Page/秋葉原.md "wikilink")「」和[動畫的](../Page/動畫.md "wikilink")[日語羅馬字](../Page/日語羅馬字.md "wikilink")「Anime」，指[動畫歌曲](../Page/動畫歌曲.md "wikilink")、[電子遊戲](../Page/電子遊戲.md "wikilink")[主題曲](../Page/主題曲.md "wikilink")、[聲優歌曲](../Page/聲優.md "wikilink")、[電波歌曲](../Page/電波歌曲.md "wikilink")、[萌曲等](../Page/萌曲.md "wikilink")。

## 概要

「A-POP」此一詞彙早於2000年已經出現\[1\]。但實際正式於2002年左右開始提倡的是[Shall Luck
PLUS的](../Page/Shall_Luck_PLUS.md "wikilink")[代表取締役](../Page/代表取締役.md "wikilink")[金杉肇](../Page/金杉肇.md "wikilink")\[2\]，2005年同公司旗下[F\&C的產品中的音樂使用A](../Page/F&C.md "wikilink")-POP的名義\[3\]。其後電視動畫《[地上最強新娘](../Page/地上最強新娘.md "wikilink")》中的數首樂曲、[Animelo
Summer Live](../Page/Animelo_Summer_Live.md "wikilink")2007
Generation-A\[4\]、2008年[NICONICO動畫的網上節目](../Page/NICONICO動畫.md "wikilink")「A-POP
HIT STUDIO」也是表明以A-POP為主題類型\[5\]。

## 參見

  - [堤健一郎](../Page/堤健一郎.md "wikilink")－2000年時的提倡者
  - [金杉肇](../Page/金杉肇.md "wikilink")
  - [秋葉系](../Page/秋葉系.md "wikilink")

## 参考資料

## 外部連結

  - [Animelo Summer Live](http://animelo.jp/generation-a/)
  - [A pop HitStudio（暫稱）](http://animelo.jp/apophitstudio/)
  - [A-POP★官方網站](https://web.archive.org/web/20080410064449/http://puredol.jp/)

[\*](../Category/動畫歌曲.md "wikilink")

1.  [](http://books.yahoo.co.jp/book_detail/07062629)
2.
3.
4.
5.