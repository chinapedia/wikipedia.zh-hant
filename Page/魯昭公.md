**魯昭公**（），魯国之二十四代君主。前542年即位，前517年，魯昭公伐[季孙氏](../Page/季孙氏.md "wikilink")，但大败，魯昭公逃到[齐国](../Page/齐国.md "wikilink")，前510年，昭公死。在其任內，他嘗試與[季平子政治角力](../Page/季平子.md "wikilink")，演變成「[鬥雞之變](../Page/鬥雞之變.md "wikilink")」，使昭公要逃到齊國。

  - 在位期間執政為[季孫宿](../Page/季孫宿.md "wikilink")、[叔孫婼](../Page/叔孫婼.md "wikilink")、[仲孫貜](../Page/仲孫貜.md "wikilink")。
  - 魯昭公二十三年（前519年），叔孫昭子將魯政讓位給[季孫意如](../Page/季孫意如.md "wikilink")。
  - [鬥雞之變後](../Page/鬥雞之變.md "wikilink")，在位期間執政為[仲孫何忌](../Page/仲孫何忌.md "wikilink")、[叔孫不敢](../Page/叔孫不敢.md "wikilink")。

## 家庭

庶兄

  - [子野](../Page/子野.md "wikilink")

庶弟

  - [魯定公](../Page/魯定公.md "wikilink")

妻妾

  - 孟子，吴国人。鲁哀公十二年五月卒。

### 子

| 通稱                             | 姓 | 氏 | 名    | 字 | 母 | 諡 | 封地                             |
| ------------------------------ | - | - | ---- | - | - | - | ------------------------------ |
| [公衍](../Page/公衍.md "wikilink") | 姬 |   | 衍    |   |   |   | [陽穀](../Page/陽穀.md "wikilink") |
| [公為](../Page/公為.md "wikilink") | 姬 |   | 為或務人 |   |   |   |                                |
| [公果](../Page/公果.md "wikilink") | 姬 |   | 果    |   |   |   |                                |
| [公賁](../Page/公賁.md "wikilink") | 姬 |   | 賁    |   |   |   |                                |

## 在位年與西曆對照表

<div class="NavFrame" style="clear: both; border: 1px solid #999; margin: 0.5em auto;">

<div class="NavHead" style="background-color: #CCCCFF; font-size: 100%; border-left: 3em soli; text-align: center; font-weight: bold;">

在位年與西曆對照表

</div>

<div class="NavContent" style="padding: 1em 0 0 0; font-size: 100%; text-align: center;">

| 魯昭公                            | 元年                             | 二年                             | 三年                             | 四年                             | 五年                             | 六年                             | 七年                             | 八年                             | 九年                             | 十年                             |
| ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| 西元                             | 前541年                          | 前540年                          | 前539年                          | 前538年                          | 前537年                          | 前536年                          | 前535年                          | 前534年                          | 前533年                          | 前532年                          |
| [干支](../Page/干支.md "wikilink") | [庚申](../Page/庚申.md "wikilink") | [辛酉](../Page/辛酉.md "wikilink") | [壬戌](../Page/壬戌.md "wikilink") | [癸亥](../Page/癸亥.md "wikilink") | [甲子](../Page/甲子.md "wikilink") | [乙丑](../Page/乙丑.md "wikilink") | [丙寅](../Page/丙寅.md "wikilink") | [丁卯](../Page/丁卯.md "wikilink") | [戊辰](../Page/戊辰.md "wikilink") | [己巳](../Page/己巳.md "wikilink") |
| 魯昭公                            | 十一年                            | 十二年                            | 十三年                            | 十四年                            | 十五年                            | 十六年                            | 十七年                            | 十八年                            | 十九年                            | 二十年                            |
| 西元                             | 前531年                          | 前530年                          | 前529年                          | 前528年                          | 前527年                          | 前526年                          | 前525年                          | 前524年                          | 前523年                          | 前522年                          |
| [干支](../Page/干支.md "wikilink") | [庚午](../Page/庚午.md "wikilink") | [辛未](../Page/辛未.md "wikilink") | [壬申](../Page/壬申.md "wikilink") | [癸酉](../Page/癸酉.md "wikilink") | [甲戌](../Page/甲戌.md "wikilink") | [乙亥](../Page/乙亥.md "wikilink") | [丙子](../Page/丙子.md "wikilink") | [丁丑](../Page/丁丑.md "wikilink") | [戊寅](../Page/戊寅.md "wikilink") | [己卯](../Page/己卯.md "wikilink") |
| 魯昭公                            | 二十一年                           | 二十二年                           | 二十三年                           | 二十四年                           | 二十五年                           | 二十六年                           | 二十七年                           | 二十八年                           | 二十九年                           | 三十年                            |
| 西元                             | 前521年                          | 前520年                          | 前519年                          | 前518年                          | 前517年                          | 前516年                          | 前515年                          | 前514年                          | 前513年                          | 前512年                          |
| [干支](../Page/干支.md "wikilink") | [庚辰](../Page/庚辰.md "wikilink") | [辛巳](../Page/辛巳.md "wikilink") | [壬午](../Page/壬午.md "wikilink") | [癸未](../Page/癸未.md "wikilink") | [甲申](../Page/甲申.md "wikilink") | [乙酉](../Page/乙酉.md "wikilink") | [丙戌](../Page/丙戌.md "wikilink") | [丁亥](../Page/丁亥.md "wikilink") | [戊子](../Page/戊子.md "wikilink") | [己丑](../Page/己丑.md "wikilink") |
| 魯昭公                            | 三十一年                           | 三十二年                           |                                |                                |                                |                                |                                |                                |                                |                                |
| 西元                             | 前511年                          | 前510年                          |                                |                                |                                |                                |                                |                                |                                |                                |
| [干支](../Page/干支.md "wikilink") | [庚寅](../Page/庚寅.md "wikilink") | [辛卯](../Page/辛卯.md "wikilink") |                                |                                |                                |                                |                                |                                |                                |                                |

</div>

</div>

## 參見

  -
[Category:魯國君主](../Category/魯國君主.md "wikilink")
[Category:春秋人](../Category/春秋人.md "wikilink")
[Category:姬姓](../Category/姬姓.md "wikilink")