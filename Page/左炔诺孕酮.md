**左炔诺孕酮**（Levonorgestrel）是一种用于快速[避孕的短效药物](../Page/避孕.md "wikilink")\[1\]。它可以抑制[排卵](../Page/排卵.md "wikilink")，同时使[宫颈黏液浓度增大](../Page/子宫颈.md "wikilink")，阻止[精子前进](../Page/精子.md "wikilink")。代表商品名称有“毓婷”、“安婷”「B計劃」（Plan
B）等。通常以口服錠劑出售\[2\]。本品通常用作[緊急避孕](../Page/緊急避孕.md "wikilink")，服藥後效果可維持120小時。藥效在性交後會降低，且只有在未受孕的情形有效，無法中止妊娠的進程\[3\]。左炔诺孕酮可與[雌激素配為](../Page/雌激素.md "wikilink")[复合口服避孕药](../Page/复合口服避孕药.md "wikilink")\[4\]。左炔諾孕酮也能配合[子宮環使用](../Page/子宮環.md "wikilink")，此類劑型商品名為「[蜜蕊娜](../Page/蜜蕊娜.md "wikilink")」（Mirena），屬於長效型避孕藥\[5\]。在某些國家可以避孕\[6\]。

常見副作用包含噁心、乳房壓痛、頭痛，以及月經不調（包括經血量改變，以及頻率紊亂）\[7\]。此藥物在孕期作緊急避孕使用時不會中止妊娠的進程，且目前沒有藥物會損害胎兒的證據\[8\]。哺乳期間用藥目前顯示安全\[9\]，服用此藥物並不會改變感染[性感染疾病的風險](../Page/性感染疾病.md "wikilink")\[10\]。左炔诺孕酮屬於[黄体制剂](../Page/黄体制剂.md "wikilink")，和所有的[孕激素藥物一様](../Page/孕激素.md "wikilink")，有類似[孕酮的效果](../Page/孕酮.md "wikilink")\[11\]。其主要作用乃藉由減少[排卵以及關閉](../Page/排卵.md "wikilink")[子宮頸](../Page/子宮頸.md "wikilink")，以達到阻絕精子之效。\[12\]

左炔諾孕酮最初於1960年代首次合成，並於1980年代開始用於避孕\[13\]。本品列名於[世界卫生组织基本药物标准清单之中](../Page/世界卫生组织基本药物标准清单.md "wikilink")，為基礎公衛體系必備藥物之一\[14\]。該藥屬於[通用名藥物](../Page/通用名藥物.md "wikilink")\[15\]，每次避孕劑量在[開發中國家的批發價約位於](../Page/開發中國家.md "wikilink")0.23至1.65美金之間\[16\]。在美國本品屬於[非處方藥](../Page/非處方藥.md "wikilink")，且各年齡都可取得\[17\]。

## 不良反应

服用左炔诺孕酮可能会造成轻微的[呕吐](../Page/呕吐.md "wikilink")、[恶心](../Page/恶心.md "wikilink")。可能会造成女性下次[月经提前或延期](../Page/月经.md "wikilink")。
仍然有很多临床症状表明，左炔诺孕酮和[子宫外孕有关](../Page/子宫外孕.md "wikilink")。

## 參考文獻

## 外部連結

  - [Levonelle](http://www.levonelle.co.uk/) manufacturer's product
    information from [Schering](../Page/Schering.md "wikilink")
  - [Monograph for
    levonorgestrel](https://web.archive.org/web/20060925001403/http://www.ukmi.nhs.uk/NewMaterial/html/docs/levonorg.pdf)
    - Uk Medicines Information
  - [U.S. National Library of Medicine: Drug Information Portal -
    Levonorgestrel](http://druginfo.nlm.nih.gov/drugportal/dpdirect.jsp?name=Levonorgestrel)

[Category:激素避孕](../Category/激素避孕.md "wikilink")
[Category:孕激素类](../Category/孕激素类.md "wikilink")
[Category:光学纯药物](../Category/光学纯药物.md "wikilink")
[Category:炔烃](../Category/炔烃.md "wikilink")
[Category:世界卫生组织基本药物](../Category/世界卫生组织基本药物.md "wikilink")

1.

2.

3.
4.

5.
6.

7.
8.
9.
10.
11.
12.
13.

14.

15.

16.

17.