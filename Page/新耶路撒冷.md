**新耶路撒冷**是指當[耶穌再臨的時候](../Page/耶穌再臨.md "wikilink")，[新天新地的首都所在](../Page/新天新地.md "wikilink")。不同的[基督教](../Page/基督教.md "wikilink")[宗派對新耶路撒冷的準確定義有不同的解釋](../Page/宗派.md "wikilink")：可能在今日的[聖殿山](../Page/聖殿山.md "wikilink")、整個[耶路撒冷](../Page/耶路撒冷.md "wikilink")，甚至任何一個完全和今日的耶路撒冷毫無關係的地方；也可能並不是真的指某個城市。

## 新約聖經裡的新耶路撒冷

在[新约中](../Page/新约.md "wikilink")，“新耶路撒冷”一词仅出现了两次：分别为[启示录第三章第](../Page/启示录.md "wikilink")12节与第二十一章第2节。启示录在第二十一章16节描述新耶路撒冷，長寬高相等，地基周長為12000[斯塔德](../Page/罗马度量衡历史.md "wikilink")。一斯塔德大约等于185.4米，因此新耶路撒冷地基大小为：

(12000/4\*185.4/1000)² ≈ 30.9万平方千米。

若為立方體，其容积为：

(12000/4\*185.4/1000)³ ≈ 1.72亿立方千米。

若為金字塔形，其容积为：

(12000/4\*185.4/1000)³/3 ≈ 0.57亿立方千米。

## 不同基督宗教的新耶路撒冷

一般基督教派對新耶路撒冷的解釋可謂眾說紛紜。認為是指實際地方；認為新耶路撒冷是指[伊甸樂園](../Page/伊甸樂園.md "wikilink")；認為是神的樂園，是所有巳死的圣徒复活的地方，與阴间的乐园相對。一般基督教宗派對新耶路撒冷的描述與[天堂近似](../Page/天堂.md "wikilink")。

### 耶和華見證人的新耶路撒冷

[耶和华见证人相信](../Page/耶和华见证人.md "wikilink")「新耶路撒冷」是上帝羔羊的新娘，由天上的耶路撒冷（即十四萬四千的受膏立的[基督徒](../Page/基督徒.md "wikilink")）所組成。新耶路撒冷不是地上的任何地方。(《[啟示錄](../Page/啟示錄.md "wikilink")》21:2,
9-11; 《[哥林多後書](../Page/哥林多後書.md "wikilink")》1:21;
《[加拉太書](../Page/加拉太書.md "wikilink")》4:26, 31)

### 新耶路撒冷教會

基督教[Swedenborg派经常提及他们在解释新耶路撒冷方面的贡献](../Page/Swedenborg派.md "wikilink")，可见于[伊曼紐·斯威登堡](../Page/伊曼紐·斯威登堡.md "wikilink")（1688年1月29日－1772年3月29日，[瑞典科学家与神学家](../Page/瑞典.md "wikilink")）的书，如《新耶路撒冷及其属天性质》、[启示的显明和](../Page/启示的显明.md "wikilink")[启示的解释](../Page/启示的解释.md "wikilink")。根据这些书，圣经中描述的新耶路撒冷是神要恢复基督教这个新旨意的记号。并且新耶路撒冷开始建立于1757年。

### 李常受对新耶路撒冷的解释

[地方召会的领袖](../Page/地方召会.md "wikilink")[李常受在后期许多著作中详细解释](../Page/李常受.md "wikilink")《[启示录](../Page/启示录.md "wikilink")》中的新耶路撒冷。在[圣经恢復本](../Page/圣经恢复本.md "wikilink")《启示录》21章2节的注1、注2和注3\[1\]中，他认为，由于《启示录》中的重大事物都是用[表号表征](../Page/表号.md "wikilink")、描述的，（因为深奥，用人的明言难以尽述，）如灯台表徵[召会](../Page/教会.md "wikilink")，狮子和羔羊表征得胜救赎的[基督](../Page/基督.md "wikilink")，四馬表徵福音、战争、饥荒和死亡的普及，龙、蛇表征[撒但](../Page/撒但.md "wikilink")；最后，也是最大的一个表号——新耶路撒冷，并不是一座物质、无生命的城，乃是一个团体活的人位（新妇），表征历代以来所有蒙神救赎的圣徒活的组合。对基督，她是新妇，为叫祂得满足；对神，她是一个[會幕](../Page/會幕.md "wikilink")，为着神可有安居之所，并藉以彰显祂自己。

## 参考文献

## 外部链接

  - [新耶路撒冷](https://web.archive.org/web/20050308203019/http://www.lsmchinese.org/big5/07online_reading/lifestudy/read.asp?no=27-59)
  - ["Go and measure"](http://h5t3tt4242.livejournal.com/)

## 参见

  - [耶路撒冷](../Page/耶路撒冷.md "wikilink")
      - [耶路撒冷旧城](../Page/耶路撒冷旧城.md "wikilink")
  - [基督教末世论](../Page/基督教末世论.md "wikilink")
      - [敌基督](../Page/敌基督.md "wikilink")、[第三圣殿](../Page/第三圣殿.md "wikilink")
      - [千年王国](../Page/千年王国.md "wikilink")、[新天新地](../Page/新天新地.md "wikilink")

{{-}}

[Category:启示录中的城市](../Category/启示录中的城市.md "wikilink")
[Category:耶路撒冷基督教](../Category/耶路撒冷基督教.md "wikilink")
[Category:基督教末世论](../Category/基督教末世论.md "wikilink")
[Category:基督教宇宙论](../Category/基督教宇宙论.md "wikilink")
[Category:天堂的概念](../Category/天堂的概念.md "wikilink")
[Category:伊斯兰神秘主义](../Category/伊斯兰神秘主义.md "wikilink")
[Category:伊斯蘭教與其他宗教](../Category/伊斯蘭教與其他宗教.md "wikilink")
[Category:圣经短语](../Category/圣经短语.md "wikilink")
[Category:基督教术语](../Category/基督教术语.md "wikilink")

1.  [圣经恢復本《启示录》21章2节的注1、注2和注3](http://www.recoveryversion.com.tw/)