**舒明天皇**（，），諱田村，[日本第](../Page/日本.md "wikilink")34代[天皇](../Page/天皇.md "wikilink")（629年1月4日至641年10月9日）。

628年[推古天皇死后](../Page/推古天皇.md "wikilink")，朝廷发生皇位继嗣之争。天皇遗言由[圣德太子的儿子](../Page/圣德太子.md "wikilink")、[苏我马子的外孫](../Page/苏我马子.md "wikilink")[山背大兄王继位](../Page/山背大兄王.md "wikilink")，但[苏我虾夷违背遗言](../Page/苏我虾夷.md "wikilink")，拥立[敏达天皇的孫](../Page/敏达天皇.md "wikilink")、[押坂彦人大兄皇子的儿子田村皇子为天皇](../Page/押坂彦人大兄皇子.md "wikilink")。天皇即位后次年派遣第一次遣唐使。在位12年间，[苏我氏实力获得很大发展](../Page/苏我氏.md "wikilink")。他曾经与[高表仁爭礼不欢而散](../Page/高表仁.md "wikilink")。

## 直系

[Emperor_family_tree26-37.png](https://zh.wikipedia.org/wiki/File:Emperor_family_tree26-37.png "fig:Emperor_family_tree26-37.png")

### 飛鳥時代（27代－37代）

### 白鳳時代 (38代－48代)

## 參考

  - [新編《日本書紀》——卷廿三——舒明天皇——息長足日廣額天皇](https://web.archive.org/web/20041028160328/http://applepig.idv.tw/kuon/furu/text/syoki/syoki23.htm)

[Category:飛鳥時代天皇](../Category/飛鳥時代天皇.md "wikilink")
[Category:萬葉歌人](../Category/萬葉歌人.md "wikilink")