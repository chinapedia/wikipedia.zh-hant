**泡泡属**（[学名](../Page/学名.md "wikilink")：**）又名**巴婆果属**，是少數在[溫帶生長的的](../Page/溫帶.md "wikilink")[木兰目](../Page/木兰目.md "wikilink")[蕃荔枝科落葉木](../Page/蕃荔枝科.md "wikilink")，北美原產。其树被称为泡泡树，果實可以食用，通常被叫作**泡泡果**或**寶爪果**，雖說其是溫帶果樹但非常耐寒。

## 种

  - [狭叶泡泡树](../Page/狭叶泡泡树.md "wikilink") *Asimina angustifolia*
    [Raf.](../Page/Raf..md "wikilink")：分布于[佛罗里达州](../Page/佛罗里达州.md "wikilink")、[佐治亚州](../Page/佐治亚州.md "wikilink")、[亚拉巴马州](../Page/亚拉巴马州.md "wikilink")
  - [木泡泡树](../Page/木泡泡树.md "wikilink")  *Asimina incana*
    ([W.Bartram](../Page/W.Bartram.md "wikilink"))
    Exel：分布于佛罗里达州、佐治亚州；异名*Annona
    incana* W. Bartram\[1\]
  - [大花泡泡树](../Page/大花泡泡树.md "wikilink") *Asimina obovata*
    ([Willd.](../Page/Willd..md "wikilink"))
    [Nash](../Page/George_Valentine_Nash.md "wikilink")：分布于佛罗里达州；异名*Annona
    obovata* Willd.\[2\]
  - [小花泡泡树](../Page/小花泡泡树.md "wikilink") *Asimina parviflora*
    ([Michx.](../Page/Michx..md "wikilink"))
    [Dunal](../Page/Dunal.md "wikilink")：[得克萨斯州和](../Page/得克萨斯州.md "wikilink")[弗吉尼亚州南部](../Page/弗吉尼亚州.md "wikilink")
  - [小泡泡树](../Page/小泡泡树.md "wikilink") *Asimina pygmea* (W. Bartram)
    Dunal：佛罗里达州、佐治亚州
  - [网泡泡树](../Page/网泡泡树.md "wikilink") *Asimina reticulata* Shuttlw. ex
    Chapman：佛罗里达州、佐治亚州
  - [四瓣泡泡树](../Page/四瓣泡泡树.md "wikilink") *Asimina tetramera*
    [Small](../Page/John_Kunkel_Small.md "wikilink")：佛罗里达州，濒危
  - [泡泡树](../Page/泡泡树.md "wikilink") *Asimina triloba*
    ([L.](../Page/L..md "wikilink"))
    Dunal：[加拿大](../Page/加拿大.md "wikilink")[安大略省最南端](../Page/安大略省.md "wikilink")、[美国东部的](../Page/美国.md "wikilink")[纽约](../Page/纽约.md "wikilink")、美国西部的[內布拉斯加州东南部](../Page/內布拉斯加州.md "wikilink")、美国南部的佛罗里达州北部和得克萨斯州东部；异名*Annona
    triloba* L.\[3\]

## 參考文獻

[Category:番荔枝科](../Category/番荔枝科.md "wikilink")
[泡泡属](../Category/泡泡属.md "wikilink")
[Category:藥用植物](../Category/藥用植物.md "wikilink")

1.
2.
3.