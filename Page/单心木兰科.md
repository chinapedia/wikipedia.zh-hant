**单心木兰科**又名[单室木兰科](../Page/单室木兰科.md "wikilink")(Degeneriaceae)，只有1[属](../Page/属.md "wikilink")[单心木兰属](../Page/单心木兰属.md "wikilink")（*Degeneria*）或称[单室木兰属](../Page/单室木兰属.md "wikilink")，2[种](../Page/种.md "wikilink")，只生长在[斐济及周围岛屿](../Page/斐济.md "wikilink")。

本[科](../Page/科.md "wikilink")[植物为大型常绿](../Page/植物.md "wikilink")[乔木](../Page/乔木.md "wikilink")，单[叶互生](../Page/叶.md "wikilink")，芳香，含有芳香油。

## 参考文献

## 外部链接

  - 在[L. Watson和M.J. Dallwitz
    (1992年)《有花植物分科》](https://web.archive.org/web/20070103200438/http://delta-intkey.com/angio/)中的[单心木兰科](http://delta-intkey.com/angio/www/degeneri.htm)
  - [单心木兰科花和果实的照片](http://www.ryanphotographic.com/degeneriaceae.html)
  - [NCBI中的单心木兰科](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Tree&id=22298&lvl=3&lin=f&keep=1&srchmode=1&unlock)
  - [CSDL中的单心木兰科](http://www.csdl.tamu.edu/FLORA/cgi/gateway_family?fam=Degeneriaceae)

[category:植物科名](../Page/category:植物科名.md "wikilink")

[\*](../Category/单心木兰科.md "wikilink")