**吴**（222年－280年5月1日，史称**孙吴**或**东吴**）是[中国歷史上](../Page/中国歷史.md "wikilink")[三国时期由](../Page/三国.md "wikilink")[孫策奠基](../Page/孫策.md "wikilink")、[孙权建立的政权](../Page/孙权.md "wikilink")。孙权以其地古为[吴国](../Page/吴国.md "wikilink")，且受封为“吴王”，定國號為“吳”；所统治地区古称[江东](../Page/江东.md "wikilink")，以皇室姓孙，又名“东吴”或“孙吴”\[1\]\[2\]。

在[三国之中](../Page/三国.md "wikilink")，东吴[水軍最強](../Page/水軍.md "wikilink")，佔領[扬州與](../Page/扬州刺史部.md "wikilink")[荊州大部地區及](../Page/荆州刺史部.md "wikilink")[交州全境](../Page/交州刺史部.md "wikilink")。东吴亦是三国中最后一个灭亡的政权，于[280年亡于西晋](../Page/晉滅吳之戰.md "wikilink")。

## 歷史

[東漢末年](../Page/東漢.md "wikilink")，[吳郡](../Page/吳郡.md "wikilink")[富春人](../Page/富春_\(古地名\).md "wikilink")[孫堅任](../Page/孫堅.md "wikilink")[破虜將軍受](../Page/破虜將軍.md "wikilink")[烏程侯](../Page/烏程侯.md "wikilink")、領[豫州刺史](../Page/豫州.md "wikilink")、[長沙太守](../Page/長沙.md "wikilink")，曾經參戰[黃巾之亂](../Page/黃巾之亂.md "wikilink")、**西涼邊章之亂**、**區星之亂**，率兵積極參與[討伐董卓期間建立許多戰功](../Page/董卓討伐戰.md "wikilink")**擔任先鋒**、**斬殺[華雄](../Page/華雄.md "wikilink")**、**擊退[呂布](../Page/呂布.md "wikilink")**、**率先進入[洛陽](../Page/洛陽.md "wikilink")**、**[董卓逼迫求和](../Page/董卓.md "wikilink")**等事蹟。

191年（初平三年），孫堅-{征}-[荊州](../Page/荊州_\(古代\).md "wikilink")[襄陽](../Page/襄陽.md "wikilink")[劉表](../Page/劉表.md "wikilink")、[黃祖時戰死](../Page/黃祖.md "wikilink")，年僅三十七歲。

孫堅子[孫策在孫堅死後繼承領導之位](../Page/孫策.md "wikilink")，而由於孫堅生前屬[袁術派系](../Page/袁術.md "wikilink")，因而投靠袁術，向袁術借兵馬，率兵攻佔[揚州](../Page/扬州_\(古代\).md "wikilink")。在袁術自行稱[帝后](../Page/皇帝.md "wikilink")，與其決裂，自立門戶。先後擊敗[劉繇](../Page/劉繇.md "wikilink")、[許貢](../Page/許貢.md "wikilink")、[嚴白虎](../Page/嚴白虎.md "wikilink")、[王朗等](../Page/王朗.md "wikilink")[揚州割據勢力](../Page/扬州_\(古代\).md "wikilink")，奠定孫吳開國基礎。[曹操控制的](../Page/曹操.md "wikilink")[許昌朝廷拜其為討逆將軍](../Page/許昌.md "wikilink")，封吳[侯](../Page/侯爵.md "wikilink")。[建安五年](../Page/建安_\(汉献帝\).md "wikilink")（200年）夏，孫策被吳郡太守[許貢的門客襲擊](../Page/許貢.md "wikilink")，不久傷重去世，時年二十六歲。

孫策死後，因其子[孫紹年幼](../Page/孫紹.md "wikilink")，而讓其弟[孫權繼位執掌江東](../Page/孫權.md "wikilink")，繼承其兄遺志，在短時間內穩住了江東形勢。其後孫權一方面鎮壓揚州境內的[山越反叛](../Page/山越.md "wikilink")，同時三征黃祖，謀求據有荊州和江夏。

建安十三年（208年），荊州牧劉表死。曹操軍也在這時南下征荊州。繼任荊州牧的[劉琮投降曹操](../Page/劉琮_\(劉表之子\).md "wikilink")。曹操在獲得荊州之後，進一步謀劃攻打孫權佔據的江東。孫權派部下[魯肅到](../Page/魯肅.md "wikilink")\[劉備\]處探索軍情，魯肅得知劉備欲投奔蒼悟太守[吳臣後](../Page/吳臣.md "wikilink")，便勸說他改投孫權。於是劉備就派[諸葛亮前往東吳會合](../Page/諸葛亮.md "wikilink")[孫權](../Page/孫權.md "wikilink")，在[赤壁](../Page/赤壁.md "wikilink")[黃蓋火燒敵船大破曹操八十萬大軍](../Page/黃蓋.md "wikilink")（後世稱[赤壁之戰](../Page/赤壁之戰.md "wikilink")）。此後曹操退守北方，佔據荊州北部的[南陽郡](../Page/南陽郡.md "wikilink")、[江夏郡局部及](../Page/江夏郡.md "wikilink")[南郡局部地區](../Page/南郡.md "wikilink")。孫權派[周瑜](../Page/周瑜.md "wikilink")、[呂蒙](../Page/呂蒙.md "wikilink")、[甘寧](../Page/甘寧.md "wikilink")、[程普](../Page/程普.md "wikilink")、[韓當](../Page/韓當.md "wikilink")、[周泰](../Page/周泰.md "wikilink")、[蔣欽等將領攻占](../Page/蔣欽.md "wikilink")**夷陵**、**南郡**、[江陵](../Page/江陵.md "wikilink")。

這一時期，劉備則去攻打荊州南部的[長沙](../Page/長沙.md "wikilink")、[桂陽](../Page/桂陽.md "wikilink")、[武陵](../Page/武陵.md "wikilink")、[零陵四郡](../Page/零陵.md "wikilink")。周瑜死後，孫權將南郡借給劉備以及確認他分得荊南四郡，督導荊州。同時在建安十五年，孫權取得了[交州](../Page/交州.md "wikilink")。在劉備取蜀之後，孫權謀求奪回荊州，最後雙方同意以[湘江為界](../Page/湘江.md "wikilink")，長沙、桂陽、江夏屬孫權；零陵、武陵、南郡屬劉備。建安二十四年，孫權向曹操稱藩，派[呂蒙趁](../Page/呂蒙.md "wikilink")[關羽攻](../Page/關羽.md "wikilink")[襄陽時襲奪荊州](../Page/襄陽.md "wikilink")，至此劉備勢力退出荊州。

220年曹操死，[曹丕篡位](../Page/曹丕.md "wikilink")，東漢滅亡，次年劉備稱帝，分別建立[魏](../Page/曹魏.md "wikilink")、[蜀漢](../Page/蜀漢.md "wikilink")。劉備稱帝后，即以復仇之名向孫權發動[夷陵之戰](../Page/夷陵之戰.md "wikilink")。孫權先向曹丕稱臣，沿用曹魏[黄初年号](../Page/黄初.md "wikilink")\[3\]，被封為吳王，都[武昌](../Page/鄂州市.md "wikilink")（今[鄂州市](../Page/鄂州市.md "wikilink")），以避免兩線作戰。222年，劉備在[猇亭被吳鎮西將軍](../Page/猇亭.md "wikilink")[陸遜擊敗](../Page/陸遜.md "wikilink")，幾乎全軍覆沒，劉備不久就駕崩於[白帝城](../Page/白帝城.md "wikilink")。劉備死後，掌权的蜀汉丞相[諸葛亮遣](../Page/諸葛亮.md "wikilink")[鄧芝出使吳國](../Page/鄧芝.md "wikilink")，孫权同意吳蜀恢復同盟。

由于孙权拒绝将儿子[孙登派去曹魏做人质](../Page/孙登.md "wikilink")，于是斷絕了與曹魏的短暫同盟，并自立年号[黄武](../Page/黄武.md "wikilink")，導致魏文帝曹丕先後三次對吳用兵，均無功而還。後[曹休又在](../Page/曹休.md "wikilink")228年在[石亭之战對吳用兵](../Page/石亭之战.md "wikilink")，亦遭陸遜、[全琮和](../Page/全琮.md "wikilink")[朱桓等人迎擊而失敗](../Page/朱桓.md "wikilink")。[黃龍元年](../Page/黄龙_\(孙权\).md "wikilink")（229年，魏[太和三年](../Page/太和_\(曹睿\).md "wikilink")，蜀漢[建興七年](../Page/建兴_\(蜀汉\).md "wikilink")）四月丙申，孫權稱帝，國號吳，改元黃龍，是為吳大帝。

[嘉禾二年](../Page/嘉禾_\(年号\).md "wikilink")（233年），孫權封割據遼東的[公孫淵為燕王](../Page/公孫淵.md "wikilink")，派太常[張彌](../Page/張彌.md "wikilink")、執金吾[許晏](../Page/許晏.md "wikilink")、將軍[賀達領兵一萬多人並送去金銀財寶](../Page/賀達.md "wikilink")，欲立渊为燕王，然渊亦恐吳國無法及時支援又覬覦財物，便斬殺使者。同時數次攻打魏國，也削弱魏國兵力收取數城。

嘉禾六年（237年）公孫淵自立燕王，改元[紹漢](../Page/紹漢.md "wikilink")，建立[燕國](../Page/燕国_\(公孙渊\).md "wikilink")。次年正月向東吳稱臣，然而隨即於八月為魏臣[司馬懿率兵平定](../Page/司馬懿.md "wikilink")，公孫淵戰敗身亡，燕國政權覆滅。

[赤烏四年](../Page/赤烏.md "wikilink")（241年），孫權的長子[孫登死亡](../Page/孫登.md "wikilink")。次年孫權立三子[孫和為太子](../Page/孫和.md "wikilink")，不久又封四子[孫霸為魯王](../Page/孫霸.md "wikilink")，引發「[二宮之爭](../Page/二宮之爭.md "wikilink")」，又稱「南魯黨爭」，朝中大臣亦分為兩派。最終孫和被廢，孫霸被賜死，數名大臣亦在二宮之爭中死去，最後七子[孫亮反而被立為太子](../Page/孫亮.md "wikilink")。吳經此事件，非但皇室遭刑，而且舉國分歧，埋下內部鬥爭的禍根。

252年，執掌江東五十二年的孫權駕崩，享年七十一歲。孫亮即位，改元[建興](../Page/建興_\(東吳\).md "wikilink")。由[諸葛恪](../Page/諸葛恪.md "wikilink")、[孫弘](../Page/孫弘.md "wikilink")、[孫峻等輔政](../Page/孫峻.md "wikilink")。253年，諸葛恪征[淮南](../Page/淮南.md "wikilink")，慘敗。諸葛恪被孫峻所殺，大權落入孫峻之手。

256年孫峻病死後，事付從弟[偏將軍](../Page/偏將軍.md "wikilink")[孫綝](../Page/孫綝.md "wikilink")。後孫綝廢[孫亮為](../Page/孫亮.md "wikilink")[會稽王](../Page/會稽.md "wikilink")，改立[孫休為帝](../Page/孫休.md "wikilink")。不久孫綝又為孫休所殺。

[元興元年](../Page/元兴_\(吴\).md "wikilink")（264年），孫休病死。這時蜀漢剛滅亡，吳軍[西進失利](../Page/永安之戰.md "wikilink")，[交趾又](../Page/交趾郡.md "wikilink")[叛吳降魏](../Page/交阯之亂.md "wikilink")，東吳國內形勢不穩，欲立一個較年長的君主。左典軍[萬彧向丞相](../Page/萬彧.md "wikilink")[濮陽興](../Page/濮陽興.md "wikilink")、左將軍[張布推薦孫和的長子](../Page/張布.md "wikilink")[孫皓即位](../Page/孫皓.md "wikilink")。孫皓即位之初曾一度施行仁政，下令撫恤國內人民，又開倉振貧、減省後宮宮女、放生宮內多餘珍禽異獸，一時被譽為明君。然而其殘暴一面逐漸開始顯露，以致民怨不絕，幸賴[陸抗](../Page/陸抗.md "wikilink")、[陸凱等大臣全力支撐國局](../Page/陸凱_\(三國\).md "wikilink")。

274年，陸抗病死。[咸寧五年](../Page/咸寧.md "wikilink")，[晉武帝下令分六路大举伐吳](../Page/晉武帝.md "wikilink")。晉軍勢如破竹，晉將[王渾](../Page/王浑_\(太原\).md "wikilink")、[杜預](../Page/杜預.md "wikilink")、[王濬和](../Page/王濬.md "wikilink")[賈充等人相繼擊破吳將](../Page/賈充.md "wikilink")[張悌](../Page/張悌_\(三國吳\).md "wikilink")、[沈瑩](../Page/沈瑩.md "wikilink")、[孫震](../Page/孙震_\(三国\).md "wikilink")、[張象](../Page/張象.md "wikilink")、[伍延](../Page/伍延.md "wikilink")、[薛瑩](../Page/薛瑩_\(三國\).md "wikilink")、[胡衝和](../Page/胡衝.md "wikilink")[孫歆等人](../Page/孫歆.md "wikilink")，吳國防線快速崩潰。

280年([太康元年](../Page/太康_\(西晋\).md "wikilink"))，晉將杜預率領十萬大軍繼續前次伐吳的攻勢，孫皓緊急下令張悌率領七八千人抵禦，杜預擊潰了張悌大軍，直逼[建業](../Page/建業.md "wikilink")。孫皓手下已無人為他賣命，自知大勢已去。便等西晉大軍攻來時，在[石頭城上宣佈投降](../Page/石頭城.md "wikilink")，至此，東吳滅亡，三國歸[西晉](../Page/西晋.md "wikilink")。

吳國滅亡時，領州四、郡四十三、縣三百一十三、戶五十二萬三千、吏三萬二千、兵二十三萬、男女口二百三十萬、米穀二百八十萬斛，舟船五千餘艘，後宮五千餘人。\[4\]

## 人口

孫權在早期即擊敗江夏太守黄祖，虏掠男女数万口。他建國後為了提昇人口數，平定山越並以其「羸者充戶，強者補兵」，並且騷擾淮南來獲得人口。

依據[三國志](../Page/三國志.md "wikilink")[吳書有傳](../Page/吴书_\(三国志\).md "wikilink")，孫吳於222年改元[黃武年間時](../Page/黃武.md "wikilink")，人口大概約二百八十餘萬，士兵二十四萬人；在吳國強盛時期(252年)人口將近三百萬，士兵約三十六萬。

## 軍事制度

吳軍以舟師為主，步兵次之。孫吳水軍發達，在[濡須口和西陵設有水軍基地](../Page/濡須口.md "wikilink")，在[侯官](../Page/侯官.md "wikilink")（今[福建](../Page/福建.md "wikilink")[閩侯](../Page/閩侯.md "wikilink")）設有造船廠。其所造名為「長安」的戰船，可載士兵千餘人。其精銳軍隊有車下虎士、丹陽青巾軍與交州義士等。孫吳還有設有山越兵、蠻兵、夷兵等少數民族部隊。由于比较特殊的社会政治环境，孫吴除了有世兵制外，还有[世襲領兵制](../Page/世襲領兵制.md "wikilink")。各将领所領軍隊算是其[部曲](../Page/部曲.md "wikilink")，軍隊除了服从中央指挥参与战役，但还要为其将领提供其它耕种杂役等。在將領死后軍隊須繼續聽令將領之子或其弟等繼承者。

## 經濟

[WuJar.jpg](https://zh.wikipedia.org/wiki/File:WuJar.jpg "fig:WuJar.jpg")
由於吳據處江南，而這一帶在三國時還是人煙不稠密之地，孫權一登位後就設置農官，實行[屯田](../Page/屯田制.md "wikilink")。由於地處海邊，吳國在[造船和](../Page/造船业.md "wikilink")[鹽業都相當發達](../Page/鹽業.md "wikilink")，甚至遠超曹魏，同時海上貿易亦有所興起。單是所生產的船隻，高度已達五層，動輒可載數千人。

孫吳所處的[江南](../Page/江南.md "wikilink")，社會經濟起步較晚，在三國時還是人口稀薄之地。然而由於這裡戰亂較少，使得北方人民大量遷居，帶來先進生產技術和勞動力。孫權登位後設置農官，實行[屯田制](../Page/屯田制.md "wikilink")，江南地區的農業生產和社會經濟得到發展。紡織業方面，江南以產[麻布出名](../Page/麻.md "wikilink")，[豫章郡](../Page/豫章郡.md "wikilink")（[治所在今](../Page/治所.md "wikilink")[江西省](../Page/江西省.md "wikilink")[南昌市](../Page/南昌市.md "wikilink")）的[雞鳴布名傳千里](../Page/雞鳴布.md "wikilink")。[三吳出產](../Page/三吳.md "wikilink")「[八蠶之綿](../Page/八蠶之綿.md "wikilink")」，[諸暨](../Page/諸暨.md "wikilink")、[永安一帶所産絲的質量很高](../Page/武康鎮.md "wikilink")。冶鑄業以[武昌](../Page/武昌郡.md "wikilink")（今湖北省鄂州市）爲最發達，孫權曾在此地開採[銅礦](../Page/銅礦.md "wikilink")，打造兵器。孙吴在[海鹽](../Page/海鹽.md "wikilink")（今[浙江](../Page/浙江.md "wikilink")[嘉兴](../Page/嘉兴.md "wikilink")[海鹽](../Page/海鹽.md "wikilink")）、沙中（今[江蘇](../Page/江蘇.md "wikilink")[苏州](../Page/苏州.md "wikilink")[常熟](../Page/常熟.md "wikilink")）設官員，來管理這兩地的鹽業生産；在[建安郡](../Page/建安郡.md "wikilink")（治所在今[福建省](../Page/福建省.md "wikilink")[福州市](../Page/福州市.md "wikilink")）設典船校尉，海船南抵[南海](../Page/南海.md "wikilink")、北達[遼東](../Page/遼東.md "wikilink")。[海上貿易亦有所興起](../Page/海上貿易.md "wikilink")，孫吳的商業都市以[建業](../Page/建業.md "wikilink")（今江苏省[南京市](../Page/南京市.md "wikilink")）、[吳郡](../Page/吳郡.md "wikilink")（今[江苏省](../Page/江苏省.md "wikilink")[苏州市](../Page/苏州市.md "wikilink")）、[番禺](../Page/番禺.md "wikilink")（今[广东省](../Page/广东省.md "wikilink")[广州市](../Page/广州市.md "wikilink")）為主，其中番禺以國外貿易為主。

## 政治

東吳在政治上大體跟東漢相近，地方上仍實行州郡制，中央方面也是同樣。唯一不同者，則是東吳受江南本地豪族影響，單是在朝朝臣，就有不少顧姓人士（如[顧雍](../Page/顧雍.md "wikilink")）、朱姓人士（如[朱桓](../Page/朱桓.md "wikilink")，[朱然为丹扬郡人](../Page/朱然.md "wikilink")）、陸姓人士（如[陸遜](../Page/陸遜.md "wikilink")）和張姓人士（如[張溫](../Page/張溫_\(東吳\).md "wikilink")），就是後世所稱吳四姓，這些[士族都是漢朝時長居江南的望族](../Page/士族_\(中國\).md "wikilink")。

## 行政區劃

|                                         |
| :-------------------------------------: |
|                **东吴州郡**                 |
|                    州                    |
|  [扬州](../Page/扬州_\(古代\).md "wikilink")  |
|     [吴郡](../Page/吴郡.md "wikilink")      |
|    [蕲春郡](../Page/蕲春郡.md "wikilink")     |
|    [会稽郡](../Page/会稽郡.md "wikilink")     |
|    [豫章郡](../Page/豫章郡.md "wikilink")     |
|    [庐江郡](../Page/庐江郡.md "wikilink")     |
|    [庐陵郡](../Page/庐陵郡.md "wikilink")     |
|    [鄱阳郡](../Page/鄱阳郡.md "wikilink")     |
| [新都郡](../Page/新都郡_\(东汉\).md "wikilink") |
|    [临川郡](../Page/临川郡.md "wikilink")     |
|    [临海郡](../Page/临海郡.md "wikilink")     |
|    [建安郡](../Page/建安郡.md "wikilink")     |
|    [吴兴郡](../Page/吴兴郡.md "wikilink")     |
|    [东阳郡](../Page/东阳郡.md "wikilink")     |
| [毗陵典农校尉](../Page/毗陵典农校尉.md "wikilink")  |
| [庐陵南部都尉](../Page/庐陵南部都尉.md "wikilink")  |
|  [荆州](../Page/荆州_\(古代\).md "wikilink")  |
|    [武陵郡](../Page/武陵郡.md "wikilink")     |
|    [零陵郡](../Page/零陵郡.md "wikilink")     |
|    [桂阳郡](../Page/桂阳郡.md "wikilink")     |
|    [长沙郡](../Page/长沙郡.md "wikilink")     |
|  [武昌郡](../Page/武昌郡.md "wikilink")\[5\]  |
|    [安成郡](../Page/安成郡.md "wikilink")     |
|    [彭泽郡](../Page/彭泽郡.md "wikilink")     |
|    [宜都郡](../Page/宜都郡.md "wikilink")     |
|    [临贺郡](../Page/临贺郡.md "wikilink")     |
|    [衡阳郡](../Page/衡阳郡.md "wikilink")     |
|    [湘东郡](../Page/湘东郡.md "wikilink")     |
| [建平郡](../Page/建平郡_\(东吴\).md "wikilink") |
|    [天门郡](../Page/天门郡.md "wikilink")     |
|    [昭陵郡](../Page/昭陵郡.md "wikilink")     |
|    [始安郡](../Page/始安郡.md "wikilink")     |
| [始兴郡](../Page/始兴郡_\(东吴\).md "wikilink") |
|  [广州](../Page/广州_\(古代\).md "wikilink")  |
|    [苍梧郡](../Page/苍梧郡.md "wikilink")     |
|    [郁林郡](../Page/郁林郡.md "wikilink")     |
| [高凉郡](../Page/高凉郡_\(东汉\).md "wikilink") |
|    [高兴郡](../Page/高兴郡.md "wikilink")     |
|    [桂林郡](../Page/桂林郡.md "wikilink")     |
|  [合浦北部尉](../Page/合浦北部尉.md "wikilink")   |
|     [交州](../Page/交州.md "wikilink")      |
|    [日南郡](../Page/日南郡.md "wikilink")     |
|    [九真郡](../Page/九真郡.md "wikilink")     |
|    [合浦郡](../Page/合浦郡.md "wikilink")     |
|    [武平郡](../Page/武平郡.md "wikilink")     |
|    [九德郡](../Page/九德郡.md "wikilink")     |
| [新昌郡](../Page/新昌郡_\(东吴\).md "wikilink") |
|    [朱崖郡](../Page/朱崖郡.md "wikilink")     |
|                                         |

## 文化

### 文學

孫吳作家有[張紘](../Page/張紘.md "wikilink")、[薛綜](../Page/薛綜.md "wikilink")、[華覈](../Page/華覈.md "wikilink")、[韋昭等](../Page/韋昭.md "wikilink")。張紘為孫權長史，與[建安七子中的](../Page/建安七子.md "wikilink")[孔融](../Page/孔融.md "wikilink")、[陳琳等友善](../Page/陳琳_\(文學家\).md "wikilink")。薛綜為江東名儒，居孫權太子師傅之位。華覈則是孫吳末年作家。

韋昭善寫史，著有{{〈}}[吳鼓吹曲十二曲](../Page/吳鼓吹曲十二曲.md "wikilink"){{〉}}，內容為整部孫吳發展史，與[繆襲的](../Page/繆襲.md "wikilink"){{〈}}[魏鼓吹曲十二曲](../Page/魏鼓吹曲十二曲.md "wikilink"){{〉}}南北相對。他又著有《[吳書](../Page/吳書_\(韋昭\).md "wikilink")》55卷等。

近年長沙也出土一批以紀錄東吳史料為主的《[三國吳簡](../Page/三國吳簡.md "wikilink")》，對於深入研究東吳的土地制度、賦稅制度、司法制度及有關的典章制度，具有非常重要的價值。

### 宗教

三國時期的[佛教重鎮](../Page/佛教.md "wikilink")，北方以[洛陽為主](../Page/洛陽.md "wikilink")，南方則為[建業](../Page/建業.md "wikilink")。孫吳方面，當[支謙](../Page/支謙.md "wikilink")、[康僧會先後入吳](../Page/康僧會.md "wikilink")，受[孫權推崇並支持發展](../Page/孫權.md "wikilink")。[孫皓稱帝時](../Page/孫皓.md "wikilink")，本要毀壞佛寺，因康僧會說法感化，終而放棄。在[蜀漢](../Page/蜀漢.md "wikilink")，佛教不是很興盛，規模不大。

### 藝術

[急就章.jpg](https://zh.wikipedia.org/wiki/File:急就章.jpg "fig:急就章.jpg")
孫吳有很多擅長各種藝術的名士，時人稱為[吳國八絕](../Page/吳國八絕.md "wikilink")。有[吳範](../Page/吳範.md "wikilink")、[劉惇](../Page/劉惇.md "wikilink")、[趙達](../Page/趙達.md "wikilink")、[嚴武](../Page/嚴武.md "wikilink")、[皇象](../Page/皇象.md "wikilink")、[曹不興](../Page/曹不興.md "wikilink")、[宋壽和](../Page/宋壽.md "wikilink")[鄭嫗等人](../Page/鄭嫗.md "wikilink")。例如嚴武擅下[圍棋](../Page/圍棋.md "wikilink")，同輩中無人能勝，有「棋聖」之稱。至於曹不興則擅[繪畫](../Page/繪畫.md "wikilink")、皇象則擅[書法](../Page/書法.md "wikilink")。

孫吳曹不興，擅長寫生與繪佛畫，被譽為「佛畫之祖」，作品有《[維摩詰圖](../Page/維摩詰.md "wikilink")》、《[釋迦牟尼說法圖](../Page/釋迦牟尼.md "wikilink")》等等。他曾把五十尺絹連在一起，畫一人像，心明手快，運筆而成。其作品富有立體感，世人有「曹衣出水」之稱，號「曹家樣」。孫吳吳王趙夫人，吳丞相[趙遠之妹](../Page/趙遠.md "wikilink")，善於書法山水繪畫，時人譽為「針絕」。她為孫權繪各國山川地形圖，實開[山水畫之首](../Page/山水畫.md "wikilink")。

孫吳的著名碑刻有《[天發神讖碑](../Page/天發神讖碑.md "wikilink")》、《[禪國山碑](../Page/禪國山碑.md "wikilink")》、《[谷朗碑](../Page/谷朗碑.md "wikilink")》等。其中《天發神讖碑》以圓馭方，勢險局寬，氣勢雄偉奇恣。皇象擅[小篆](../Page/小篆.md "wikilink")、隸書，尤精章草。流傳作品有《[急就章](../Page/急就章.md "wikilink")》、《[文武將隊帖](../Page/文武將隊帖.md "wikilink")》及《天發神讖碑》等。

### 科技

孫吳位於江南地區，水路發達，造船技術發達。其戰船有的上下五層，有的還能容納士兵三千人。

## 外族

孫吳內部還有[山越](../Page/山越.md "wikilink")，其為據守[江南山地各族人的總稱](../Page/江南.md "wikilink")。他們自給自足，且與[曹魏聯繫](../Page/曹魏.md "wikilink")，孫吳屢次征討皆難以根除。234年[諸葛恪使用](../Page/諸葛恪.md "wikilink")[堅壁清野的戰術圍山三年](../Page/堅壁清野.md "wikilink")，降伏山越，並收編其精壯為軍隊。在[嶺南地區還有](../Page/嶺南.md "wikilink")[俚人](../Page/俚人.md "wikilink")，範圍涵蓋孫吳[廣州](../Page/廣州.md "wikilink")、[交州及](../Page/交州.md "wikilink")[蜀漢](../Page/蜀漢.md "wikilink")[益州南部](../Page/益州.md "wikilink")。孫權也展開海上的發展，他派使臣[朱應](../Page/朱應.md "wikilink")、[康泰泛海到](../Page/康泰.md "wikilink")[夷洲](../Page/夷州.md "wikilink")（疑为现在的[台湾](../Page/台湾.md "wikilink")，一说為[琉球](../Page/琉球.md "wikilink")）、[亶洲補充人口](../Page/亶洲.md "wikilink")，到[遼東](../Page/遼東.md "wikilink")、[朝鮮半島](../Page/朝鮮半島.md "wikilink")、[林邑](../Page/林邑.md "wikilink")（今[越南南部](../Page/越南.md "wikilink")）、[扶南](../Page/扶南.md "wikilink")（今[柬埔寨](../Page/柬埔寨.md "wikilink")）和[南洋群島等地溝通聯繫](../Page/南洋群島.md "wikilink")，這些都擴大孫吳在海外的影響力。[大秦商人和](../Page/羅馬帝國.md "wikilink")[林邑使臣也曾到達吳都](../Page/林邑.md "wikilink")[建業](../Page/建業.md "wikilink")。

## 封爵的大臣

受封者：108位

皇族：48位

  - 儲君
      - [孫登](../Page/孫登.md "wikilink")：太子－220年，[孫權長子](../Page/孫權.md "wikilink")
      - [孫和](../Page/孫和.md "wikilink")：太子－241年，[孫權三子](../Page/孫權.md "wikilink")
      - [孫亮](../Page/孫亮.md "wikilink")：太子－250年、会稽王－256年，[孫權七子](../Page/孫權.md "wikilink")
      - [孙𩅦](../Page/孙𩅦.md "wikilink")：太子－262年、豫章王－264年，[孙休長子](../Page/孙休.md "wikilink")
      - [孙瑾](../Page/孙瑾.md "wikilink")：太子－264年，[孫皓長子](../Page/孫皓.md "wikilink")
  - 郡王
      - 孙权
          - [孙霸](../Page/孙霸.md "wikilink")：鲁王－241年，四子
          - [孫奮](../Page/孫奮.md "wikilink")：齊王－252年、章安侯－258年，五子
          - [孫休](../Page/孫休.md "wikilink")：琅琊王－253年，六子
      - 孙休
          - [孙𩃙](../Page/孙𩃙.md "wikilink")：汝南王－264年，次子。
          - [孙壾](../Page/孙壾.md "wikilink")：梁王－264年，三子
          - [孙𠅨](../Page/孙𠅨.md "wikilink")：陈王－264年，四子
      - 孙皓
          - [孙虔](../Page/孙虔.md "wikilink"): 淮阳王－264年、鲁王－273年，次子
          - 东平王－269年、齐王－273年，三子
          - 陈留王-273年
          - 章陵王-273年
          - 成纪王-278年
          - 宣威王-278年
          - 中山王-280年
          - 代王-280年

其余孙皓十一位儿子名与封号均不详

  - 縣侯
      - [孫策](../Page/孫策.md "wikilink")
          - [孙绍](../Page/孙绍.md "wikilink")：吴侯－229年，上虞侯
              - [孙奉](../Page/孙奉.md "wikilink")：上虞侯
      - [孫權](../Page/孫權.md "wikilink")
          - [孫慮](../Page/孫慮.md "wikilink")：建昌侯－228年，二子
              - [孫英](../Page/孫英.md "wikilink")：吴侯，[孫登次子](../Page/孫登.md "wikilink")
              - [孙皓](../Page/孙皓.md "wikilink")：[烏程侯](../Page/烏程侯.md "wikilink")，[孫和長子](../Page/孫和.md "wikilink")
              - [孫德](../Page/孫德.md "wikilink")：钱唐侯，[孫和二子](../Page/孫和.md "wikilink")
              - [孫谦](../Page/孫谦.md "wikilink")：永安侯，[孫和三子](../Page/孫和.md "wikilink")
              - [孙基](../Page/孙基.md "wikilink")：吴侯，[孫霸二子](../Page/孫霸.md "wikilink")
              - [孙壹](../Page/孙壹.md "wikilink")：宛陵侯，[孫霸三子](../Page/孫霸.md "wikilink")
      - [孫靜](../Page/孫靜.md "wikilink")
          - [孙奂](../Page/孙奂.md "wikilink")：沙羡侯－226年，[孫靜四子](../Page/孫靜.md "wikilink")
              - [孙承](../Page/孙承.md "wikilink")：沙羡侯，[孫靜之孫](../Page/孫靜.md "wikilink")
              - [孙胤](../Page/孙胤.md "wikilink")：丹陽侯，[孫靜之孫](../Page/孫靜.md "wikilink")
              - [孙晞](../Page/孙晞.md "wikilink")，丹陽侯，[孫靜之孫](../Page/孫靜.md "wikilink")
                  - [孙峻](../Page/孙峻.md "wikilink")：富春侯－253年，[孫靜曾孫](../Page/孫靜.md "wikilink")
                  - [孙綝](../Page/孙綝.md "wikilink")：永宁侯，[孫靜曾孫](../Page/孫靜.md "wikilink")
      - 孫堅族孫
          - [孙桓](../Page/孙桓.md "wikilink")：丹徒侯，[孫河三子](../Page/孫河.md "wikilink")
          - [孙韶](../Page/孙韶.md "wikilink")：建德侯，[孫河侄子](../Page/孫河.md "wikilink")
              - [孙楷](../Page/孙楷.md "wikilink")：临成侯，[孙韶長子](../Page/孙韶.md "wikilink")
              - [孙越](../Page/孙越.md "wikilink")：建德侯，[孙韶次子](../Page/孙韶.md "wikilink")
  - 鄉侯
      - [孙松](../Page/孙松.md "wikilink")：都乡侯，[孙翊子](../Page/孙翊.md "wikilink")
      - [孙邻](../Page/孙邻.md "wikilink")：都乡侯-214年，[孙贲长子](../Page/孙贲.md "wikilink")
          - [孙苗](../Page/孙苗.md "wikilink")：都乡侯，[孙邻长子](../Page/孙邻.md "wikilink")
  - 亭侯
      - [孙贲](../Page/孙贲.md "wikilink")：都亭侯，[孙羌长子](../Page/孙羌.md "wikilink")

大臣：60位

  - 縣侯（一等侯爵）
      - [徐琨](../Page/徐琨_\(後漢\).md "wikilink")：廣德侯－200年代
          - [徐矯](../Page/徐矯.md "wikilink")：廣德侯
          - [徐祚](../Page/徐祚.md "wikilink")：廣德侯、徐矯之弟
      - [步騭](../Page/步騭.md "wikilink")：广信侯－211年、臨湘侯－229年
          - [步協](../Page/步協.md "wikilink")：臨湘侯－247年
              - 步玑：臨湘侯
      - [贺齐](../Page/贺齐.md "wikilink")：山阴侯－216年
      - [潘璋](../Page/潘璋.md "wikilink")：溧阳侯－219年
      - [陸遜](../Page/陸遜.md "wikilink")：华亭侯－219年、婁侯－220年、江陵侯－222年
          - [陸抗](../Page/陸抗.md "wikilink")：江陵侯－245年
              - [陸晏](../Page/陸晏.md "wikilink")：江陵侯－274年
      - [呂蒙](../Page/呂蒙.md "wikilink")：孱陵侯－220年
          - [呂霸](../Page/呂霸.md "wikilink")：孱陵侯－220年
          - [呂琮](../Page/呂琮.md "wikilink")：孱陵侯，呂霸之兄
          - [呂睦](../Page/呂睦.md "wikilink")：孱陵侯，吕霸之弟
      - [諸葛瑾](../Page/諸葛瑾.md "wikilink")：宣城侯－220年、宛陵侯－222年
          - [诸葛融](../Page/诸葛融.md "wikilink")：宛陵侯－241年
      - [周泰](../Page/周泰.md "wikilink")：陵陽侯－220年
          - 周邵：陵陽侯
          - 周承：陵陽侯，周邵之弟
      - [朱然](../Page/朱然.md "wikilink")：西安乡侯－220年、永安侯－222年、当阳侯－223年
          - [施绩](../Page/施绩.md "wikilink")：当阳侯－249年
      - [吕岱](../Page/吕岱.md "wikilink")：都乡侯－220年、番禺侯－226年
          - [吕凯](../Page/呂凱_\(東吳\).md "wikilink")：番禺侯－256年
      - [全琮](../Page/全琮.md "wikilink")：阳华亭侯－220年、錢塘侯－222年前
          - [全怿](../Page/全怿.md "wikilink")：钱塘侯
      - [張昭](../Page/張昭.md "wikilink")：由拳侯－221年、婁侯－229年
          - [張休](../Page/張休.md "wikilink")：婁侯－236年
      - [呂範](../Page/呂範.md "wikilink")：宛陵侯－221年、南昌侯－228年前
          - [呂據](../Page/呂據.md "wikilink")：南昌侯－228年
      - [滕胤](../Page/滕胤.md "wikilink")：都亭侯－221年、高密侯－256年前
          - [滕牧](../Page/滕牧.md "wikilink")：高密侯－264年，滕胤族弟
      - [朱桓](../Page/朱桓.md "wikilink")：新城亭侯、嘉興侯－222年
          - [朱異](../Page/朱異.md "wikilink")：嘉兴侯－238年
      - [朱治](../Page/朱治.md "wikilink")：毗陵侯－222年
          - [朱才](../Page/朱才.md "wikilink")：毗陵侯－224年
              - [朱琬](../Page/朱琬.md "wikilink")：毗陵侯
      - [孫邵](../Page/孫邵.md "wikilink")：陽羨侯－222年
      - [韩当](../Page/韩当.md "wikilink")：都亭侯－222年、石城侯－223年
          - [韩綜](../Page/韩綜.md "wikilink")：石城侯－226年
      - [蒋壹](../Page/蒋壹.md "wikilink")：宣城侯－222年
      - [徐盛](../Page/徐盛.md "wikilink")：芜湖侯－224年前
          - [徐楷](../Page/徐楷.md "wikilink")：芜湖侯－224年
      - [顧雍](../Page/顧雍.md "wikilink")：醴陵侯－226年
          - [顾济](../Page/顾济.md "wikilink")：醴陵侯－243年
          - [顾裕](../Page/顾裕.md "wikilink")：醴陵侯、顾济之兄
      - [芮玄](../Page/芮玄.md "wikilink")：溧阳侯－226年前
      - [潘濬](../Page/潘濬.md "wikilink")：劉陽侯－229年前
          - 潘翥：劉陽侯－239年
      - [朱據](../Page/朱據.md "wikilink")：雲陽侯－229年
          - [朱宣](../Page/朱據.md "wikilink")：雲陽侯，朱據之孫
      - [諸葛恪](../Page/諸葛恪.md "wikilink")：都乡侯－234年、陽都侯－252年
      - [丁奉](../Page/丁奉.md "wikilink")：都亭侯－252年、都乡侯－252年、安豐侯－255年
      - [全尚](../Page/全尚.md "wikilink")：都亭侯－252年、永平侯－253年
      - [文钦](../Page/文钦.md "wikilink")：谯侯－255年
      - [陸凱](../Page/陆凯_\(三国\).md "wikilink")：都乡侯－255年、嘉興侯－264年
      - [濮阳兴](../Page/濮阳兴.md "wikilink")：外黄侯－256年
      - [何洪](../Page/何洪.md "wikilink")：永平侯－264年
          - [何邈](../Page/何邈.md "wikilink")：永平侯
      - [何蒋](../Page/何蒋.md "wikilink")：溧阳侯－264年
      - [何植](../Page/何植.md "wikilink")：宣城侯－264年
      - [陆景](../Page/陆景.md "wikilink")：毗陵侯－264年后
      - [张悌](../Page/张悌.md "wikilink")：山都侯－279年
      - [虞汜](../Page/虞汜.md "wikilink")：余姚侯
      - [诸葛诞](../Page/诸葛诞.md "wikilink")：寿春侯
  - 鄉侯（二等侯爵）
      - [沈珩](../Page/沈珩.md "wikilink")：永安乡侯
      - [周胤](../Page/周胤.md "wikilink")：都乡侯－229年
      - [是儀](../Page/是儀.md "wikilink")：都亭侯、都乡侯－229年
      - [陈表](../Page/陈表.md "wikilink")：都乡侯－236年
      - [钟离牧](../Page/钟离牧.md "wikilink")：都乡侯－236年
      - [張承](../Page/張承.md "wikilink")：都乡侯
          - [張震](../Page/張震_\(張承之子\).md "wikilink")：都乡侯－245年
      - [全吴](../Page/全吴.md "wikilink")：都乡侯

<!-- end list -->

  - 亭侯（三等侯爵）
      - [步阐](../Page/步阐.md "wikilink")：西亭侯－247年
      - [韦昭](../Page/韦昭.md "wikilink")：高陵亭侯－264年
      - [華覈](../Page/華覈.md "wikilink")：徐陵亭侯－264年
      - [骆统](../Page/骆统.md "wikilink")：新阳亭侯
      - [陈脩](../Page/陈脩.md "wikilink")：都亭侯－220年
      - [陆胤](../Page/陆胤.md "wikilink")：都亭侯－258年
          - 陆式：都亭侯
      - [魯淑](../Page/魯淑.md "wikilink")：都亭侯
          - [鲁睦](../Page/鲁睦.md "wikilink")：都亭侯
      - [程咨](../Page/程咨.md "wikilink")：亭侯－229年
      - [全绪](../Page/全绪.md "wikilink")：亭侯－253年
      - 文雍：亭侯－256年
      - [凌封](../Page/凌封.md "wikilink")：亭侯
          - [凌烈](../Page/凌烈.md "wikilink")：亭侯，凌封之弟
  - 關內侯
      - [黄柄](../Page/黄柄.md "wikilink")：关内侯－229年
      - [周鲂](../Page/周鲂.md "wikilink")：关内侯
  - 列侯
      - [諸葛直](../Page/諸葛直.md "wikilink"):亶洲將軍—231年
      - [陈永](../Page/陈永.md "wikilink")：侯
      - 王氏三人：列侯，[王夫人之弟](../Page/大懿皇后.md "wikilink")

## 追封諡號的大臣

## 宰輔名列

  - 首輔

<!-- end list -->

  - [孫邵](../Page/孫邵.md "wikilink")（王相）
  - [顧雍](../Page/顧雍.md "wikilink")（丞相）
  - [陸遜](../Page/陸遜.md "wikilink")（丞相）
  - [步骘](../Page/步骘.md "wikilink")（丞相）
  - [朱据](../Page/朱据.md "wikilink")（丞相）
  - [諸葛恪](../Page/諸葛恪.md "wikilink")（[太傅兼領大將軍](../Page/太傅.md "wikilink")）
  - [孫峻](../Page/孫峻.md "wikilink")（丞相兼領大將軍）
  - [孫綝](../Page/孫綝.md "wikilink")（丞相兼領大將軍）
  - [濮陽興](../Page/濮陽興.md "wikilink")（丞相）
  - [陸凱](../Page/陆凯_\(三国\).md "wikilink")（左丞相）、[萬彧](../Page/萬彧.md "wikilink")（右丞相）
  - [張悌](../Page/張悌_\(三國吳\).md "wikilink")（丞相）

<!-- end list -->

  - 大司馬

<!-- end list -->

  - [呂範](../Page/呂範.md "wikilink")
  - 朱然、全琮（分領左右大司馬）
  - [呂岱](../Page/呂岱.md "wikilink")
  - [滕胤](../Page/滕胤.md "wikilink")
  - [施績](../Page/施績.md "wikilink")、[丁奉](../Page/丁奉.md "wikilink")（分領左右大司馬）
  - [范慎](../Page/范慎.md "wikilink")（太尉）
  - [陸抗](../Page/陸抗.md "wikilink")
  - [诸葛靓](../Page/诸葛靓.md "wikilink")（《晋书》记载）

## 君主列表

## 世系图

<center>

</center>

## 註釋

## 參考文獻

  - 《[三國志](../Page/三國志.md "wikilink")》，[陳壽著](../Page/陳壽.md "wikilink")，[裴松之注](../Page/裴松之.md "wikilink")
  - 《[後漢書](../Page/後漢書.md "wikilink")》，[范曄著](../Page/范晔_\(史家\).md "wikilink")
  - 柿沼阳平：〈[孙吴货币经济的结构和特点](http://www.nssd.org/articles/article_read.aspx?id=45101585)〉。
  - 「三國吳兵考」，陶元珍著，載《燕京學報》第13期，[上海書店](../Page/上海書店.md "wikilink")，1983年影印本

{{-}}

[孫吳](../Category/孫吳.md "wikilink") [\*3](../Category/三国.md "wikilink")
[Category:222年建立的國家或政權](../Category/222年建立的國家或政權.md "wikilink")
[Category:229年建立的國家或政權](../Category/229年建立的國家或政權.md "wikilink")
[Category:中国历史政权](../Category/中国历史政权.md "wikilink")

1.  《三国志》卷42：群臣或难（谯）周曰：“今（邓）艾以不远，恐不受降，如之何？”周曰：“方今东吴未宾，事势不得不受之，受之之后，不得不礼。”
2.
3.  [罗新：走马楼吴简中的建安纪年简问题](http://www.aisixiang.com/data/22525.html)
4.  《三國志·吳書·三嗣主傳》注引《晉陽秋》
5.  从原[江夏郡中设立](../Page/江夏郡.md "wikilink")，吴占江夏郡是否尚有存留、郡治所在皆不明。