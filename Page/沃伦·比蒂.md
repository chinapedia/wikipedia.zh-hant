**亨利·沃伦·比蒂**（，），[美国](../Page/美国.md "wikilink")[演员](../Page/演员.md "wikilink")、[导演](../Page/导演.md "wikilink")、[编剧和](../Page/编剧.md "wikilink")[制片人](../Page/電影監製.md "wikilink")。他曾经获得[奥斯卡奖和](../Page/奥斯卡奖.md "wikilink")[金球奖](../Page/金球奖.md "wikilink")。2002年，美国电影学会授予比蒂[戈登·E·索耶奖](../Page/戈登·E·索耶奖.md "wikilink")。2004年，比蒂获得[肯尼迪中心荣誉奖](../Page/肯尼迪中心荣誉奖.md "wikilink")，2007年获得[金球奖终身成就奖](../Page/金球奖终身成就奖.md "wikilink")，2008年获得[美国电影学会](../Page/美国电影学会.md "wikilink")[终身成就奖](../Page/AFI终身成就奖.md "wikilink")。

## 獎項

### [奥斯卡最佳導演獎](../Page/奥斯卡最佳導演獎.md "wikilink")

  - 獲獎：《[赤色分子](../Page/赤色分子.md "wikilink")》 （1981年）

### [奥斯卡最佳男主角獎](../Page/奥斯卡最佳男主角獎.md "wikilink")

  - 提名：《[我倆沒有明天](../Page/我倆沒有明天.md "wikilink")》 （1967年）
  - 提名：《[天堂可以等待](../Page/天堂可以等待.md "wikilink")》 （1978年）
  - 提名：《[赤色分子](../Page/赤色分子.md "wikilink")》 （1981年）
  - 提名：《[一代情枭毕斯](../Page/一代情枭毕斯.md "wikilink")》 （1991年）

### [金球獎最佳戲劇類電影男主角](../Page/金球獎最佳戲劇類電影男主角.md "wikilink")

  - 提名：《[天涯何处无芳草](../Page/天涯何处无芳草.md "wikilink")》 （1961年）
  - 提名：《[我倆沒有明天](../Page/我倆沒有明天.md "wikilink")》 （1967年）
  - 提名：《[赤色分子](../Page/赤色分子.md "wikilink")》 （1981年）
  - 提名：《[一代情枭毕斯](../Page/一代情枭毕斯.md "wikilink")》 （1991年）

### [金球獎最佳音樂及喜劇類電影男主角](../Page/金球獎最佳音樂及喜劇類電影男主角.md "wikilink")

  - 提名：《[洗发水](../Page/洗发水_\(电影\).md "wikilink")》 （1975年）
  - 獲獎：《[天堂可以等待](../Page/天堂可以等待.md "wikilink")》 （1978年）
  - 提名：《[选举追击令](../Page/选举追击令.md "wikilink")》 （1998年）

## 主要作品

  - 1961年 《[天涯何处无芳草](../Page/天涯何处无芳草.md "wikilink")》（Splendor in the
    Grass）
  - 1961年 《[罗马之春](../Page/罗马之春.md "wikilink")》（The Roman Spring of Mrs.
    Stone）
  - 1962年 《[情场浪子](../Page/情场浪子.md "wikilink")》（All Fall Down）
  - 1964年 《[莉莉丝](../Page/莉莉丝_\(电影\).md "wikilink")》（Lilith）
  - 1965年 《[心墙魅影](../Page/心墙魅影.md "wikilink")》（Mickey One）
  - 1965年 《Promise Her Anything》（Promise Her Anything）
  - 1966年 《[人海万花筒](../Page/人海万花筒.md "wikilink")》（Kaleidoscope）
  - 1967年 《[我倆沒有明天](../Page/我倆沒有明天.md "wikilink")》（Bonnie and Clyde）
  - 1970年 《人间游戏》（The Only Game in Town ）
  - 1971年 《[花村](../Page/花村.md "wikilink")》（McCabe & Mrs. Miller）
  - 1971年 《$》（$）
  - 1973年 《Year of the Woman》（Year of the Woman）(记录片)
  - 1974年 《[暗杀十三招](../Page/暗杀十三招.md "wikilink")》（The Parallax View）
  - 1975年 《[洗发水](../Page/洗发水_\(电影\).md "wikilink")》（Shampoo）
  - 1975年 《[财富](../Page/财富\(电影\).md "wikilink")》（The Fortune）
  - 1978年 《[天堂可以等待](../Page/天堂可以等待.md "wikilink")》（Heaven Can Wait）
  - 1981年 《[赤色分子](../Page/赤色分子.md "wikilink")》（Reds）
  - 1984年 《乔治·斯蒂芬：一个制片人的生活历程》（George Stevens: A Filmmakers Journey）(记录片)
  - 1987年 《[伊斯达](../Page/伊斯达.md "wikilink")》（Ishtar）(also producer)
  - 1990年 《[至尊神探](../Page/至尊神探.md "wikilink")》（Dick Tracy）
  - 1991年 《[麦当娜：真实与挑战](../Page/麦当娜：真实与挑战.md "wikilink")》（Madonna: Truth
    or Dare）(记录片)
  - 1991年 《[一代情枭毕斯](../Page/一代情枭毕斯.md "wikilink")》（Bugsy）
  - 1992年 《Writing with Light: Vittorio Storaro》（Writing with Light:
    Vittorio Storaro） (记录片)
  - 1994年 《[恋爱事件](../Page/恋爱事件.md "wikilink")》（Love Affair）(also writer)
  - 1998年 《[选举追击令](../Page/选举追击令.md "wikilink")》（Bulworth）
  - 1999年 《The Book That Wrote Itself》（The Book That Wrote Itself）
  - 2001年 《[城里城外](../Page/城里城外.md "wikilink")》（Town & Country）
  - 2003年 《Dean Tavoularis: The Magician of Hollywood》（Dean Tavoularis:
    The Magician of Hollywood） (记录片)
  - 2005年 《One Bright Shining Moment》（One Bright Shining Moment） (记录片)

## 参考资料

  -
## 外部链接

  -
  -
[Category:美國電影演員](../Category/美國電影演員.md "wikilink")
[Category:美國電影導演](../Category/美國電影導演.md "wikilink")
[Category:美國電影監製](../Category/美國電影監製.md "wikilink")
[Category:奧斯卡最佳導演獎獲獎者](../Category/奧斯卡最佳導演獎獲獎者.md "wikilink")
[Category:金球獎最佳導演獲得者](../Category/金球獎最佳導演獲得者.md "wikilink")
[Category:美国男性编剧](../Category/美国男性编剧.md "wikilink")
[Category:美國西北大學校友](../Category/美國西北大學校友.md "wikilink")
[Category:維吉尼亞州人](../Category/維吉尼亞州人.md "wikilink")
[Category:加利福尼亚州民主党人](../Category/加利福尼亚州民主党人.md "wikilink")
[Category:美国编剧工会奖得主](../Category/美国编剧工会奖得主.md "wikilink")
[Category:美國空軍士兵](../Category/美國空軍士兵.md "wikilink")
[Category:肯尼迪中心荣誉奖得主](../Category/肯尼迪中心荣誉奖得主.md "wikilink")
[Category:英語電影導演](../Category/英語電影導演.md "wikilink")
[Category:加拿大裔美國人](../Category/加拿大裔美國人.md "wikilink")
[Category:金球獎終身成就獎獲得者](../Category/金球獎終身成就獎獲得者.md "wikilink")
[Category:金球奖最佳音乐或喜剧男主角奖得主](../Category/金球奖最佳音乐或喜剧男主角奖得主.md "wikilink")
[Category:20世纪美国男演员](../Category/20世纪美国男演员.md "wikilink")
[Category:21世紀美國男演員](../Category/21世紀美國男演員.md "wikilink")