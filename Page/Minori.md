**minori**（）是日本[成人遊戲製作公司](../Page/日本成人遊戲.md "wikilink")。曾是[CoMix
Wave的軟件部](../Page/CoMix_Wave.md "wikilink")，隨著2007年3月CoMix
Wave的分割而在翌月成為正式公司。minori這個公司名是從同公司的口號「We always keep **minori**ty
spirit.」而來。作品中登場的女性外表多為年少但心理上卻很成熟，而開頭影片多為[新海誠製作](../Page/新海誠.md "wikilink")，作品多是[群像劇和](../Page/群像劇.md "wikilink")[互動小說](../Page/互動小說.md "wikilink")。2019年2月28日宣布解散製作團隊停止開發遊戲。\[1\]\[2\]

2009年6月25日起，官方网站（同时包括下属所有相关作品的独立子站点）屏蔽日本以外地区的访问。肇因於[Illusion公司的作品](../Page/Illusion.md "wikilink")[电车之狼在](../Page/电车之狼.md "wikilink")[英国](../Page/英国.md "wikilink")[亚马逊网站遭到总部位于英国](../Page/亚马逊.md "wikilink")[伦敦的国际](../Page/伦敦.md "wikilink")[女权组织](../Page/女权.md "wikilink")[Equality
Now的抗议而下架](../Page/Equality_Now.md "wikilink")。\[3\]随后以minori为代表，众多日本[美少女遊戲公司纷纷跟进效仿](../Page/美少女遊戲.md "wikilink")。目前已有部分公司（如[MOONSTONE](../Page/MOONSTONE.md "wikilink")）解除访问限制。

## 作品列表

  - 2001年8月31日－[BITTERSWEET
    FOOLS](../Page/BITTERSWEET_FOOLS.md "wikilink")
  - 2002年4月19日－[Wind -a breath of
    heart-](../Page/Wind_-a_breath_of_heart-.md "wikilink")
  - 2002年12月27日－
  - 2004年7月23日－[春天的足音](../Page/春天的足音.md "wikilink")（）
  - 2004年11月5日－Wind -a breath of heart- Re:gratitude
  - 2005年3月25日－（以「minori feat. Aeris」的名義發售）
  - 2006年3月31日－
  - [ef - a fairy tale of the
    two.](../Page/ef_-_a_fairy_tale_of_the_two..md "wikilink")
      - ef - First Fan Disc（先行fandisk）
          - 2006年8月11日－13日
            於[C70先行發售](../Page/Comic_Market.md "wikilink")
          - 2006年8月25日 一般發售
      - 2006年12月22日－ef - the first tale.（第1部）
      - 2008年5月30日－ef - the latter tale.（第2部）
      - 2010年9月17日－
  - 2009年9月18日－[eden\*](../Page/eden*.md "wikilink")
  - 2011年12月29日 -  - NICE TO MEET YOU！
  - 2012年5月18日 -  - Alice the magical conductor. STORY\#01 Spring Has
    Come \!
  - 2012年12月21日 - [夏空的英仙座](../Page/夏空的英仙座.md "wikilink")（）
  - 2014年1月31日 -
  - 2015年2月27日 - [永不落幕的前奏詩](../Page/永不落幕的前奏詩.md "wikilink")（）
  - 2016年2月26日 -
  - 2017年3月31日 -
  - 2018年1月26日 -
  - 2019年1月25日 -

## 參考資料

## 外部連結

  - [minori官方網站入口](https://web.archive.org/web/20090505215105/http://www.minori.ph/)（日本以外IP無法直接瀏覽主頁面，會被導向至英文版mangagamer.com的網誌。）

[\*](../Category/Minori.md "wikilink")
[Category:日本電子遊戲公司](../Category/日本電子遊戲公司.md "wikilink")
[Category:日本成人遊戲公司](../Category/日本成人遊戲公司.md "wikilink")
[Category:豐島區公司](../Category/豐島區公司.md "wikilink")

1.
2.
3.