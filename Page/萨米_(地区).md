<table>
<caption><font size=+1><strong>薩米</strong></font><br />
Sápmi, Sameland, Lappland, Lappi, Лапландия</caption>
<tbody>
<tr class="odd">
<td style="text-align: center;"><table>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Sami_flag.svg" title="fig:Sami_flag.svg">Sami_flag.svg</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Norway.svg" title="fig:Flag_of_Norway.svg">Flag_of_Norway.svg</a> <a href="https://zh.wikipedia.org/wiki/File:Flag_of_Sweden.svg" title="fig:Flag_of_Sweden.svg">Flag_of_Sweden.svg</a> <a href="https://zh.wikipedia.org/wiki/File:Flag_of_Finland.svg" title="fig:Flag_of_Finland.svg">Flag_of_Finland.svg</a> <a href="https://zh.wikipedia.org/wiki/File:Flag_of_Russia.svg" title="fig:Flag_of_Russia.svg">Flag_of_Russia.svg</a></p></td>
</tr>
<tr class="even">
<td><p>(<a href="../Page/薩米旗.md" title="wikilink">詳細</a>)</p></td>
<td></td>
</tr>
</tbody>
</table></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="https://zh.wikipedia.org/wiki/File:LocationSapmi.png" title="fig:LocationSapmi.png">LocationSapmi.png</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/區歌.md" title="wikilink">區歌</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/語言.md" title="wikilink">語言</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/面積.md" title="wikilink">面積</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/人口.md" title="wikilink">人口</a><br />
 - 薩米人<br />
 - 全部人口</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/獨立.md" title="wikilink">獨立</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/時區.md" title="wikilink">時區</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><small>¹ 拉普蘭是一個民族區域，此區域橫跨<a href="../Page/挪威.md" title="wikilink">挪威</a>、<a href="../Page/瑞典.md" title="wikilink">瑞典</a>、<a href="../Page/芬蘭.md" title="wikilink">芬蘭及</a><a href="../Page/俄羅斯.md" title="wikilink">俄羅斯的北部</a>，區域內的<a href="../Page/薩米人.md" title="wikilink">薩米人享有不同程度的自治權</a>。</small></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

**薩米**是[薩米人傳統居住的文化區域](../Page/薩米人.md "wikilink")。薩米位於北歐[斯堪地那維亞半島的北部](../Page/斯堪地那維亞半島.md "wikilink")，橫跨[挪威](../Page/挪威.md "wikilink")、[瑞典及](../Page/瑞典.md "wikilink")[芬蘭北部和](../Page/芬蘭.md "wikilink")[俄羅斯](../Page/俄羅斯.md "wikilink")[科拉半島北部](../Page/科拉半島.md "wikilink")，大部分在[北極圈之內](../Page/北極圈.md "wikilink")，號稱“歐洲最後一塊原始保留區”、“[聖誕老人的故鄉](../Page/聖誕老人.md "wikilink")”，是芬蘭重要的觀光點，而每年吸引大批游客的夢幻之地“聖誕老人樹”就在這裏。此地區在[薩米語中有不同稱呼](../Page/薩米語.md "wikilink")，北薩米人稱之為Sápmi，Julev薩米人稱之為Sábme，南薩米人稱之為Saemie；在瑞典和挪威語也稱之為Sameland。薩米經常與瑞典[拉普蘭混淆](../Page/拉普蘭_\(瑞典\).md "wikilink")，但兩者所指的區域並非完全相同。

隨着國際化，跨國境的合作日益重要，對於當地的薩米族原住民和非薩米族居民而言，現存國界越來越不重要。薩米地區的居民中，以俄羅斯人和挪威人佔大多數，而薩米族人只佔小數的5%。雖然現時沒有爭取當地獨立的運動，但有組織提倡地區自治或當地原住民的民族自決。

薩米又稱為「拉普蘭」，意思就是「拉普人之地」，但由於此地的原住民薩米人覺得「拉普人」乃是對他們的侮蔑稱呼，於是此地正式改名為**薩米**了。

Sapmi_Lappland.jpg| Sarek_Skierffe_Laitaure_Rapadalen.jpg|
Sarek_Skierffe_Rapadelta.jpg| Kebnekaise view from
Tuolpagorni.jpg|薩米和瑞典的最高峰──[凱布訥山](../Page/凱布訥山.md "wikilink")

[Category:挪威地理](../Category/挪威地理.md "wikilink")
[Category:芬蘭地理](../Category/芬蘭地理.md "wikilink")
[Category:瑞典地理](../Category/瑞典地理.md "wikilink")
[Category:俄羅斯地理](../Category/俄羅斯地理.md "wikilink")
[Category:薩米](../Category/薩米.md "wikilink")
[Category:北极地区](../Category/北极地区.md "wikilink")