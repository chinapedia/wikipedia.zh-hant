**艾斯頓·古查**（，）是[美國](../Page/美國.md "wikilink")[愛荷華州](../Page/愛荷華州.md "wikilink")[錫達拉皮茲人](../Page/錫達拉皮茲.md "wikilink")，是[演員](../Page/演員.md "wikilink")、[製作人](../Page/製作人.md "wikilink")、前[模特兒](../Page/模特兒.md "wikilink")、[喜劇演員與企業投資家](../Page/喜劇演員.md "wikilink")。

## 生平

### 早年生活

艾希頓·庫奇在美國[愛荷華州的](../Page/愛荷華州.md "wikilink")[錫達拉皮茲出生](../Page/錫達拉皮茲.md "wikilink")，其父母賴瑞·庫奇和黛安·庫奇為工廠的員工\[1\]，有一位姊姊桃莎和一位[腦性麻痺的孿生兄弟麥可](../Page/腦性麻痺.md "wikilink")。庫奇的童年就像一個喜愛戶外活動的美國中西部居民一樣，做過許多奇特的的工作，如木匠工、捆草工和家禽閹割工。他在[錫達拉皮茲的華盛頓高中念了一年書之後](../Page/錫達拉皮茲.md "wikilink")，便隨著家人搬到了
Tiffin 鎮，在 Clear Creek-Amana
高中完成學業，是學校橄欖球隊的一員\[2\]。他的家庭生活壓力相當大，他曾說過：「我不想回家之後發現更多弟弟的壞消息，所以我讓自己很忙就感覺不到了。」\[3\]他也表示曾有輕生的念頭，在十三歲時，曾想過從某醫院陽台跳樓自殺，但他父親最後避免這件事的發生\[4\]。大約這個時候，其父母離婚了。高三時，曾和表哥在半夜闖入學校偷錢，在犯案離開時被抓到了，被判三級[竊盜罪](../Page/竊盜.md "wikilink")，[緩刑三年和](../Page/緩刑.md "wikilink")180小時的[社區服務](../Page/社區服務.md "wikilink")。庫奇表示此事件讓他走回正途，但卻失去了他的女友和即將核准的大學獎學金，在學校和當地社區受到排擠\[5\]。

庫奇之後去念了[愛荷華大學](../Page/愛荷華大學.md "wikilink")，為了想治好其弟弟的病，便主修[生化工程](../Page/生化工程.md "wikilink")（但未畢業），曾因為太吵太瘋狂而被趕出學校宿舍\[6\]，他說：「我以為我無所不知，但實際上並不然，我抽菸抽的兇，也愛到處參加派對，起床後常常不知道自己前晚做了什麼，我真的是玩得太過火了，很驚訝我竟然沒死。」\[7\]庫奇曾為了收入當過地板清潔工，有時也去[捐血賺錢](../Page/捐血.md "wikilink")\[8\]。在大學時期，他在[愛荷華市的一間酒吧被星探挖掘](../Page/愛荷華市.md "wikilink")，參加「Fresh
Faces of
Iowa」模特兒選拔賽贏得第一名，也贏得一趟到[紐約市的](../Page/紐約市.md "wikilink")「國際模特兒專長協會」（International
Modeling and Talent
Association）的旅程。待在紐約市一陣子後，他回到[錫達拉皮茲](../Page/錫達拉皮茲.md "wikilink")，之後又搬到[洛杉磯去追求他的演藝生涯](../Page/洛杉磯.md "wikilink")\[9\]。

### 職業生涯

#### 进入演艺圈

1997年，在參加完IMTA協會的模特兒競賽之後，庫奇與紐約的奈斯特（Next）模特兒經紀公司簽約，接拍了[Calvin
Klein的廣告](../Page/Calvin_Klein.md "wikilink")，在巴黎和米蘭走秀，也拍了[必勝客的廣告](../Page/必勝客.md "wikilink")。

模特兒的生涯初期成功之後，庫奇搬到了[洛杉磯](../Page/洛杉磯.md "wikilink")，在一次面試之後，獲得《That '70s
Show》節目的演出機會，從1998年一直延續到2006年。同時，庫奇也接演了許多電影，他曾參加《[珍珠港](../Page/珍珠港.md "wikilink")》丹尼·沃克（Danny
Walker）的角色的試鏡，但並沒有成功\[10\]。他接演了許多成功的喜劇片，如《[豬頭，我的車咧？](../Page/豬頭，我的車咧？.md "wikilink")》（*Dude,
Where's My Car?*）、《[新婚告急](../Page/新婚告急.md "wikilink")》（*Just
Married*）和《[誰敢來晚餐](../Page/誰敢來晚餐.md "wikilink")》（*Guess
Who*）。他在《[蝴蝶效應](../Page/蝴蝶效應_\(電影\).md "wikilink")》（*The
Butterfly Effect*）的角色對他來說非常引人注目，大眾對此片的評論有好有壞，但票房的成績相當亮眼\[11\]。

2003年，庫奇當起[MTV台](../Page/MTV.md "wikilink")《[明星大整蛊](../Page/明星大整蛊.md "wikilink")》節目的製作人兼演員，此節目用[隱藏式攝影機的手法拍攝名人](../Page/隱藏式攝影機.md "wikilink")。同時，庫奇也是[真人實境秀](../Page/真人實境秀.md "wikilink")《*Beauty
and the Geek*》和《*The Real Wedding
Crashers*》的監製。2006年，他接演了《[海防最前線](../Page/海防最前線.md "wikilink")》（*The
Guardian*），與[凱文·柯斯納共同演出美國海岸防衛隊的救命員](../Page/凱文·柯斯納.md "wikilink")，此片的製片公司[正金石影業曾因為庫奇喜劇演員的定位而猶豫是否找他來演這個動作片](../Page/正金石影業.md "wikilink")，他為了能夠接演此片，被迫不得與《*That
70s
Show*》簽訂第八季和第九季的新合約，不過後來他還是以特別來賓的身分出現在前四集裡以及最後一集\[12\]。庫奇也為動畫電影《[打獵季節](../Page/打獵季節.md "wikilink")》（*Open
Season*）配音，此片還剛好與《海防最前線》同日上映。

#### 踏入IT界

2013年11月1日，库彻受聘为联想最新产品YOGA平板代言，并成为联想特邀产品工程师。

## 個人生活

[Demi_Moore_and_Ashton_Kutcher_TechCrunch50.jpg](https://zh.wikipedia.org/wiki/File:Demi_Moore_and_Ashton_Kutcher_TechCrunch50.jpg "fig:Demi_Moore_and_Ashton_Kutcher_TechCrunch50.jpg")
庫奇曾與女演員[潔紐艾莉·瓊斯](../Page/潔紐艾莉·瓊斯.md "wikilink")（January
Jones；1998-2001）、[艾希莉·史考特](../Page/艾希莉·史考特.md "wikilink")（Ashley
Scott；2001-2002）、[茉奈特·馬祖兒](../Page/茉奈特·馬祖兒.md "wikilink")（Monet
Mazur；2002）和[布蘭妮·墨菲](../Page/布蘭妮·墨菲.md "wikilink")（Brittany
Murphy；2002-2003）交往過，他與莫菲在2003年分手後，即與[黛咪·摩爾交往](../Page/黛咪·摩爾.md "wikilink")，兩人年齡相差15歲的差距常成為媒體的話題焦點，之後兩人在2005年9月24日結婚，舉辦了私人婚禮，約有100親朋好友參加，當中也包含了摩爾的前夫[布魯斯·威利](../Page/布魯斯·威利.md "wikilink")。庫奇表示這場婚禮是一個「不邏輯的決定」\[13\]，他也說道：「布魯斯和我另外也有深厚的交情。」後兩人於2013年離婚。

庫奇之後於2012年4月開始與女星[蜜拉·庫妮絲開始交往](../Page/蜜拉·庫妮絲.md "wikilink")，兩人曾經在[70年代秀合作過](../Page/70年代秀.md "wikilink")。\[14\]\[15\]
在2014年2月底，兩人宣布訂婚的消息。\[16\]\[17\]

庫奇的知心好友有前《That '70s
Show》節目的共同演員[丹尼·曼斯特森](../Page/丹尼·曼斯特森.md "wikilink")（Danny
Masterson）和[威廉·維德拉瑪](../Page/威廉·維德拉瑪.md "wikilink")（Wilmer
Valderrama），還有演出《[豬頭，我的車咧？](../Page/豬頭，我的車咧？.md "wikilink")》的演員[西恩·威廉·史考特](../Page/西恩·威廉·史考特.md "wikilink")。庫奇另外與曼斯特森和維德拉瑪投資合開了一間[義大利餐廳](../Page/義大利菜.md "wikilink")，叫做「多切」（Dolce）\[18\]，還有一間[日式主題餐廳](../Page/日式.md "wikilink")「藝妓屋」（Geisha
House），位在[亞特蘭大和](../Page/亞特蘭大.md "wikilink")[洛杉磯](../Page/洛杉磯.md "wikilink")。同時，庫奇也是[芝加哥熊隊的贊助家](../Page/芝加哥熊.md "wikilink")。

## 戲劇作品

| 年份        | 片名                         | 中文片名                                       | 角色                   |
| --------- | -------------------------- | ------------------------------------------ | -------------------- |
| 1998–2006 | *That '70s Show*           |                                            | Michael Kelso        |
| 1999      | *Coming Soon*              |                                            | Louie 路易             |
| 2000      | *Down to You*              | 煞到你                                        | Jim Morrison 吉姆·莫瑞森  |
| 2000      | *Reindeer Games*           |                                            | College Kid          |
| 2000      | *Dude, Where's My Car?*    | 豬頭，我的車咧？                                   | Jesse Montgomery III |
| 2001      | *Just Shoot Me\!*          |                                            | Dean Cassidy（1集）     |
| 2001      | *Texas Rangers*            | 飆風特警                                       | George Durham        |
| 2002      | *Grounded for Life*        |                                            | Cousin Scott（1集）     |
| 2003–2007 | *Punk'd*                   | [明星大整蛊](../Page/明星大整蛊.md "wikilink")       | 主持人                  |
| 2003      | *Just Married*             | [新婚告急](../Page/新婚告急.md "wikilink")         | Tom Leezak           |
| 2003      | *My Boss's Daughter*       | 頭家千金                                       | Tom Stansfield       |
| 2003      | *Cheaper by the Dozen*     | 十二生笑                                       | Hank 漢克              |
| 2004      | *The Butterfly Effect*     | [蝴蝶效應](../Page/蝴蝶效應_\(電影\).md "wikilink")  | Evan Treborn 伊凡      |
| 2005      | *Guess Who*                | 誰敢來晚餐                                      | Simon Green 賽門·格林    |
| 2005      | *A Lot Like Love*          | [再見鍾情](../Page/再見鍾情.md "wikilink")         | Oliver Martin 奧立佛·馬丁 |
| 2005      | *Robot Chicken*            |                                            | （配音）                 |
| 2006      | *Bobby*                    | [驚爆時刻](../Page/驚爆時刻.md "wikilink")         | Fisher 費雪            |
| 2006      | *The Guardian*             | [海防最前線](../Page/海防最前線.md "wikilink")       | Jake Fischer 傑克·費雪   |
| 2006      | *Open Season*              | [打獵季節](../Page/打獵季節.md "wikilink")         | Elliot 艾略特（配音）       |
| 2008      | *Miss Guided*              |                                            | Beaux                |
| 2008      | *What Happens in Vegas...* | [頭彩冤家](../Page/頭彩冤家.md "wikilink")         | Jack                 |
| 2009      | *Spread*                   | [情圣终结者](../Page/情圣终结者.md "wikilink")       | Nikki Harper         |
| 2009      | *Personal Effects*         | [真愛效應](../Page/真愛效應.md "wikilink")         | Walter Simmons       |
| 2010      | *Valentine's Day*          | [情人節快樂](../Page/情人節快樂.md "wikilink")       | Reed Bennet          |
| 2010      | *Killers*                  | [刺客公敵](../Page/刺客公敵.md "wikilink")         | Spencer 史賓賽          |
| 2011      | *No Strings Attached*      | [飯飯之交](../Page/飯飯之交.md "wikilink")         | Adam                 |
| 2011      | *New Year's Eve*           | [101次新年快樂](../Page/101次新年快樂.md "wikilink") | Randy                |
| 2011–2015 | *Two and a half men*       | [好漢兩個半](../Page/好漢兩個半.md "wikilink")       | Walden Schmidt       |
| 2013      | *Jobs*                     | [賈伯斯](../Page/賈伯斯_\(電影\).md "wikilink")    | Steve Jobs           |

## 製作

| 年份   | 片名                          | 中文片名                                      | 頭銜    |
| ---- | --------------------------- | ----------------------------------------- | ----- |
| 2003 | *Punk'd*                    | [明星大整蛊](../Page/明星大整蛊.md "wikilink")      | 監製    |
| 2003 | *My Boss's Daughter*        | 頭家千金                                      | 共同製作人 |
| 2004 | *The Butterfly Effect*      | [蝴蝶效應](../Page/蝴蝶效應_\(電影\).md "wikilink") | 監製    |
| 2004 | *You've Got a Friend*       |                                           | 監製    |
| 2006 | *Beauty and the Geek*       | 第二季第二集                                    | 監製    |
| 2007 | *Adventures in Hollyhood*   |                                           | 監製    |
| 2007 | *Miss Guided*               |                                           | 監製    |
| 2007 | *Game Show in My Head*      |                                           | 監製    |
| 2007 | *The Real Wedding Crashers* |                                           | 監製    |
| 2007 | *Room 401*                  | [驚悚401號房](../Page/驚悚401號房.md "wikilink")  | 監製    |

## 参考文献

## 外部連結

  -
  -
[Category:美國電影男演員](../Category/美國電影男演員.md "wikilink")
[Category:美國電視男演員](../Category/美國電視男演員.md "wikilink")
[Category:美国男性模特儿](../Category/美国男性模特儿.md "wikilink")
[Category:美国男配音演员](../Category/美国男配音演员.md "wikilink")
[Category:美國電視監製](../Category/美國電視監製.md "wikilink")
[Category:愛荷華大學校友](../Category/愛荷華大學校友.md "wikilink")
[Category:愛荷華州人](../Category/愛荷華州人.md "wikilink")
[Category:美国投资者](../Category/美国投资者.md "wikilink")
[Category:美國金融家](../Category/美國金融家.md "wikilink")
[Category:20世纪美国男演员](../Category/20世纪美国男演员.md "wikilink")
[Category:在美國出生的雙胞胎](../Category/在美國出生的雙胞胎.md "wikilink")
[Category:惡作劇者](../Category/惡作劇者.md "wikilink")
[Category:21世紀美國男演員](../Category/21世紀美國男演員.md "wikilink")
[Category:捷克裔美國人](../Category/捷克裔美國人.md "wikilink")
[Category:德國裔美國人](../Category/德國裔美國人.md "wikilink")
[Category:爱尔兰裔美国人](../Category/爱尔兰裔美国人.md "wikilink")
[Category:美国风险投资家](../Category/美国风险投资家.md "wikilink")

1.  <http://www.filmreference.com/film/49/Ashton-Kutcher.html>

2.

3.

4.

5.
6.
7.

8.

9.  Meers, Erik (2001). ["Dude – He's a
    star."](http://www.papermag.com/?section=article&parid=216&page=1)
    Papermag.com. Retrieved October 6, 2006.

10. <http://www.imdb.com/title/tt0213149/trivia>

11.
12.
13.

14.

15.

16.

17.

18.