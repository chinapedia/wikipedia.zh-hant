本列表包括[旧约和](../Page/旧约.md "wikilink")[新约圣经中提到的](../Page/新约.md "wikilink")[先知](../Page/先知.md "wikilink")。

主表只包括圣经中明确称为[先知者](../Page/先知.md "wikilink")，次表包括曾经看见[异象以及有](../Page/异象.md "wikilink")[预言经历者](../Page/圣经预言.md "wikilink")，第三表所列为[假先知](../Page/假先知.md "wikilink")。

## 主表

### A

  - [亚伦](../Page/亚伦.md "wikilink") ([出埃及记](../Page/出埃及记.md "wikilink")
    7:1)
  - [亚伯](../Page/亚伯.md "wikilink") ([路加福音](../Page/路加福音.md "wikilink")
    11:50-51)
  - [亚伯拉罕](../Page/亚伯拉罕.md "wikilink") ([创世纪](../Page/创世纪.md "wikilink")
    20:7)
  - [亚迦布](../Page/亚迦布.md "wikilink") ([使徒行传](../Page/使徒行传.md "wikilink")
    21:10)
  - [亚古珥](../Page/亚古珥.md "wikilink") ([箴言](../Page/箴言.md "wikilink")
    30:1)
  - [亚希雅](../Page/亚希雅.md "wikilink") ([列王纪上](../Page/列王纪.md "wikilink")
    11:29)
  - [阿摩司](../Page/阿摩司.md "wikilink") ([阿摩司书](../Page/阿摩司书.md "wikilink")
    7:8)
  - [亚拿](../Page/亚拿.md "wikilink") ([路加福音](../Page/路加福音.md "wikilink")
    2:36)
  - [亚撒利雅](../Page/亚撒利雅.md "wikilink")
    ([历代志下](../Page/历代志.md "wikilink") 15:1)

### B

  - [巴拿巴](../Page/巴拿巴.md "wikilink") ([使徒行传](../Page/使徒行传.md "wikilink")
    13:1)

### D

  - [但以理](../Page/但以理.md "wikilink") ([马太福音](../Page/马太福音.md "wikilink")
    24:15)
  - [大卫](../Page/大卫.md "wikilink") ([希伯来书](../Page/希伯来书.md "wikilink")
    11:32)
  - [底波拉](../Page/底波拉.md "wikilink") ([士师记](../Page/士师记.md "wikilink")
    4:4)

### E

  - [以利亚](../Page/以利亚.md "wikilink") ([列王纪上](../Page/列王纪.md "wikilink")
    18:22)
  - [以利沙](../Page/以利沙.md "wikilink") ([列王纪上](../Page/列王纪.md "wikilink")
    19:16)
  - [以诺](../Page/以诺.md "wikilink") ([犹大书](../Page/犹大书.md "wikilink")
    1:14)
  - [以西结](../Page/以西结.md "wikilink") ([以西结书](../Page/以西结书.md "wikilink")
    1:3)

### G

  - [迦得](../Page/迦得.md "wikilink") ([撒母耳记下](../Page/撒母耳记.md "wikilink")
    24:11)
  - [基甸](../Page/基甸.md "wikilink") ([希伯来书](../Page/希伯来书.md "wikilink")
    11:32)

### H

  - [哈巴谷](../Page/哈巴谷书.md "wikilink")
    ([哈巴谷书](../Page/哈巴谷书.md "wikilink") 1:1)
  - [哈该](../Page/哈该.md "wikilink") ([哈该书](../Page/哈该书.md "wikilink")
    1:1)
  - [何西阿](../Page/何西阿.md "wikilink") ([何西阿书](../Page/何西阿书.md "wikilink")
    1:1)
  - [户勒大](../Page/户勒大.md "wikilink") ([列王纪下](../Page/列王纪下.md "wikilink")
    22:14)

### I

  - [易多](../Page/易多.md "wikilink") ([历代志下](../Page/历代志.md "wikilink")
    13:22)
  - [以赛亚](../Page/以赛亚.md "wikilink") ([以赛亚书](../Page/以赛亚书.md "wikilink")
    1:1)

### J

  - [雅各](../Page/雅各.md "wikilink") ([创世纪](../Page/创世纪.md "wikilink")
    28:11 - 16)
  - [耶户](../Page/耶户.md "wikilink") ([列王纪上](../Page/列王纪.md "wikilink")
    16:7)
  - [耶利米](../Page/耶利米.md "wikilink") ([耶利米书](../Page/耶利米书.md "wikilink")
    1:11)
  - [约珥](../Page/约珥.md "wikilink") ([约珥书](../Page/约珥书.md "wikilink")
    1:1)
  - [施洗约翰](../Page/施洗约翰.md "wikilink")
    ([路加福音](../Page/路加福音.md "wikilink") 7:28)
  - [使徒约翰](../Page/使徒约翰.md "wikilink") ([启示录](../Page/启示录.md "wikilink")
    1:1)
  - [约拿](../Page/约拿.md "wikilink") ([约拿书](../Page/约拿书.md "wikilink")
    1:1)
  - [约书亚](../Page/约书亚.md "wikilink") ([约书亚记](../Page/约书亚记.md "wikilink")
    1:1)
  - [猶大](../Page/猶大.md "wikilink") ([使徒行传](../Page/使徒行传.md "wikilink")
    15:32)

### L

  - [古利奈](../Page/古利奈.md "wikilink")（Cyrene,
    Libya）人[路求](../Page/路求.md "wikilink")
    ([使徒行传](../Page/使徒行传.md "wikilink") 13:1)

### M

  - [玛拉基](../Page/玛拉基.md "wikilink") （[玛拉基书](../Page/玛拉基书.md "wikilink")
    1:1)
  - [马念](../Page/马念.md "wikilink") ([使徒行传](../Page/使徒行传.md "wikilink")
    13:1)
  - [弥迦](../Page/弥迦.md "wikilink") ([弥迦书](../Page/弥迦书.md "wikilink")
    1:1)
  - [米该雅](../Page/米该雅.md "wikilink") ([列王纪上](../Page/列王纪上.md "wikilink")
    22:8)
  - [米利暗](../Page/米利暗.md "wikilink") ([出埃及记](../Page/出埃及记.md "wikilink")
    15:20)
  - [摩西](../Page/摩西.md "wikilink") ([申命记](../Page/申命记.md "wikilink")
    34:10)

### N

  - [那鸿](../Page/那鸿书.md "wikilink")（[那鸿书](../Page/那鸿书.md "wikilink")
    1:1）
  - [拿单](../Page/拿单_\(先知\).md "wikilink")（[撒母耳记下](../Page/撒母耳记.md "wikilink")
    7:2）
  - [挪亞](../Page/挪亞.md "wikilink")（[創世記](../Page/創世記.md "wikilink") 7:1）

### O

  - [俄巴底亚](../Page/俄巴底亚.md "wikilink")
    （[俄巴底亚书](../Page/俄巴底亚书.md "wikilink") 1:1)
  - [俄德](../Page/俄德.md "wikilink") ([历代志下](../Page/历代志.md "wikilink")
    15:8)

### P

  - [保罗](../Page/保罗.md "wikilink") （[使徒行传](../Page/使徒行传.md "wikilink")
    16:9）
  - [腓利](../Page/腓利.md "wikilink") ([使徒行传](../Page/使徒行传.md "wikilink")
    8:26） 注：他的四个女儿也说预言（[使徒行传](../Page/使徒行传.md "wikilink") 21:8, 9）

### S

  - [撒母耳](../Page/撒母耳.md "wikilink")
    ([撒母耳记上](../Page/撒母耳记.md "wikilink") 3:20）
  - [示玛雅](../Page/示玛雅.md "wikilink") （[列王纪上](../Page/列王纪上.md "wikilink")
    12:22）
  - [西拉](../Page/西拉.md "wikilink") ([使徒行传](../Page/使徒行传.md "wikilink")
    15:32)
  - 称呼尼结的西面 ([使徒行传](../Page/使徒行传.md "wikilink") 13:1）

### T

  - [两个见证人](../Page/两个见证人.md "wikilink")
    （[启示录](../Page/启示录.md "wikilink") 11:3）

### Z

  - [撒迦利亚](../Page/撒迦利亚.md "wikilink")
    （[撒迦利亚书](../Page/撒迦利亚书.md "wikilink") 1:1）
  - [撒迦利亚 (耶何耶大之子)](../Page/撒迦利亚_\(耶何耶大之子\).md "wikilink")
    （[路加福音](../Page/路加福音.md "wikilink") 11:50-51）
  - [西番雅](../Page/西番雅.md "wikilink") ([西番雅书](../Page/西番雅书.md "wikilink")
    1:1)

## 次表

  - 伊利达 ([民数记](../Page/民数记.md "wikilink") 11:26)
  - 以利以谢 （[历代志下](../Page/历代志.md "wikilink") 20:37)
  - [以利沙伯](../Page/以利沙伯.md "wikilink")，施洗约翰的母亲
    ([路加福音](../Page/路加福音.md "wikilink") 1:41）
  - [夏甲](../Page/夏甲.md "wikilink")（[創世記](../Page/創世記.md "wikilink")
    16:10-11）
  - [雅哈悉](../Page/雅哈悉.md "wikilink") ([历代志下](../Page/历代志.md "wikilink")
    20:14)
  - [约瑟](../Page/约瑟_\(旧约圣经\).md "wikilink")，雅各的儿子（[創世記](../Page/創世記.md "wikilink")
    37:5 - 11）
  - [约瑟](../Page/约瑟_\(新约圣经\).md "wikilink")，马利亚的丈夫（[马太福音](../Page/马太福音.md "wikilink")
    1:20)
  - [圣母玛利亚](../Page/圣母玛利亚.md "wikilink")，耶稣的母亲（[路加福音](../Page/路加福音.md "wikilink")
    1:26-28）
  - 米达 ([民数记](../Page/民数记.md "wikilink") 11:26)
  - [巴比伦国王](../Page/巴比伦.md "wikilink")[尼布甲尼撒](../Page/尼布甲尼撒.md "wikilink")
    （[但以理书](../Page/但以理书.md "wikilink") 2:1)
  - [扫罗王](../Page/扫罗王.md "wikilink")
    ([撒母耳记上](../Page/撒母耳记.md "wikilink") 10:10)
  - [耶路撒冷的](../Page/耶路撒冷.md "wikilink")[西面](../Page/西面.md "wikilink")
    （[路加福音](../Page/路加福音.md "wikilink") 2:25, 26)
  - [所罗门](../Page/所罗门.md "wikilink") （[列王纪上](../Page/列王纪.md "wikilink")
    3:5）
  - 以色列的七十位长老（[民数记](../Page/民数记.md "wikilink") 11:25）
  - [撒迦利亚](../Page/撒迦利亚_\(祭司\).md "wikilink")，施洗约翰的父亲([路加福音](../Page/路加福音.md "wikilink")
    1:67)

## 假先知

  - [亚哈](../Page/亚哈.md "wikilink") ([耶利米书](../Page/耶利米书.md "wikilink")
    29:21)
  - [押朔](../Page/押朔.md "wikilink") ([耶利米书](../Page/耶利米书.md "wikilink")
    28:1)
  - [巴兰](../Page/巴兰.md "wikilink") ([民数记](../Page/民数记.md "wikilink")
    22:9)
  - [巴耶稣](../Page/巴耶稣.md "wikilink") ([使徒行传](../Page/使徒行传.md "wikilink")
    13:6)
  - [哈拿尼雅](../Page/哈拿尼雅.md "wikilink")
    ([耶利米书](../Page/耶利米书.md "wikilink") 28:5)
  - [耶洗别](../Page/耶洗别.md "wikilink") ([启示录](../Page/启示录.md "wikilink")
    2:20)
  - [挪亚底](../Page/挪亚底.md "wikilink") ([尼希米记](../Page/尼希米记.md "wikilink")
    6:14)
  - [启示录](../Page/启示录.md "wikilink")
    16:13的[假先知](../Page/假先知.md "wikilink")
  - [西底家](../Page/西底家.md "wikilink") ([耶利米书](../Page/耶利米书.md "wikilink")
    29:21)

## 外部链接

  - [Prophetic Midrash](http://www.propheticmidrash.com/)

## 参见

  - [先知书](../Page/先知书.md "wikilink")：[大先知书](../Page/大先知书.md "wikilink")、[小先知书](../Page/小先知书.md "wikilink")
  - [圣经人物列表](../Page/圣经人物列表.md "wikilink")
  - [伊斯兰教的先知](../Page/伊斯兰教的先知.md "wikilink")
  - [圣经预言](../Page/圣经预言.md "wikilink")

{{-}}

[\*](../Category/先知.md "wikilink")
[Category:圣经人物列表](../Category/圣经人物列表.md "wikilink")