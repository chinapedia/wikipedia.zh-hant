[Qoo_logo.svg](https://zh.wikipedia.org/wiki/File:Qoo_logo.svg "fig:Qoo_logo.svg")
[Qoocharacter.jpg](https://zh.wikipedia.org/wiki/File:Qoocharacter.jpg "fig:Qoocharacter.jpg")


**酷儿**（；）是由[可口可樂公司生產的一種非碳酸飲料](../Page/可口可樂公司.md "wikilink")。1999年11月15日，可口可樂联合[美汁源](../Page/美汁源.md "wikilink")（Minute
Maid）在[日本推出了这款面向儿童及青少年的饮料](../Page/日本.md "wikilink")，隨後於[台灣](../Page/台灣.md "wikilink")、[韓國](../Page/韓國.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")、[香港和](../Page/香港.md "wikilink")[中國大陆推出](../Page/中國大陆.md "wikilink")\[1\]，现在在亚洲生产包括[葡萄及](../Page/葡萄.md "wikilink")[橙味的多种口味](../Page/柳橙.md "wikilink")。

飲料廣告卡通的形象是由[博報堂旗下的](../Page/博報堂.md "wikilink")設計。在台灣的[電視廣告採用](../Page/電視廣告.md "wikilink")[趙自強主唱的廣告歌](../Page/趙自強.md "wikilink")：「Q-o-o，有種果汁真好喝，喝的時候Qoo，喝完臉紅紅！」

在曾經銷售過此品牌飲料的市場中，臺灣自2005年前後逐漸減少鋪貨而淡出市場，並由同屬可口可樂旗下的[美粒果填補果汁產品市場的空缺](../Page/美粒果.md "wikilink")。

## 參考文獻

<div class="references-small">

<references>

</references>

</div>

## 外部連結

  - [酷儿日本網站](http://www.qoo.jp/)

  - [酷儿新加坡網站](https://web.archive.org/web/20100305210920/http://www.coca-cola.com.sg/home/home.asp)

[de:Getränkemarken der Coca-Cola
GmbH](../Page/de:Getränkemarken_der_Coca-Cola_GmbH.md "wikilink")

[Category:可口可樂品牌](../Category/可口可樂品牌.md "wikilink")
[Category:軟飲料](../Category/軟飲料.md "wikilink")
[Category:吉祥物](../Category/吉祥物.md "wikilink")
[Category:1999年面世的產品](../Category/1999年面世的產品.md "wikilink")

1.