[教宗](../Page/教宗.md "wikilink")**德範九世**（，约1020年—1058年3月29日），本名Frédéric，於1057年8月3日至1058年3月29日岀任教宗。他本人原是貴族，為[下洛林](../Page/洛林王國.md "wikilink")[公爵](../Page/公爵.md "wikilink")[戈弗雷三世之弟和](../Page/戈弗雷三世.md "wikilink")[第一次十字軍將領](../Page/第一次十字軍.md "wikilink")[布永的戈德弗里的舅公](../Page/布永的戈德弗里.md "wikilink")；所以他又被稱為洛林的Frederic。在教宗的順序上，他也可稱爲**德範十世**。請參考[德範二世](../Page/德範二世.md "wikilink")。

## 譯名列表

  - 史蒂芬十世、司提反十世、史蒂溫十世、[司提溫十世](../Page/司提溫十世.md "wikilink")：[智慧藏百科全書網](http://www.wordpedia.com/search/resultx.asp?w=stephen)的各種譯名。
  - 伊什特万十世、伊斯特万十世、[斯特凡十世](../Page/斯特凡十世.md "wikilink")、[斯蒂芬十世](../Page/斯蒂芬十世.md "wikilink")、**司提反十世**：《[世界人名翻譯大辭典](../Page/世界人名翻譯大辭典.md "wikilink")》1993年版的各種譯名。
  - 德範：[香港天主教教區檔案　歷任教宗](https://web.archive.org/web/20051023073732/http://archives.catholic.org.hk/popes/index.htm)作德範。

[S](../Category/教宗.md "wikilink") [S](../Category/1058年逝世.md "wikilink")
[Category:法國出生的教宗](../Category/法國出生的教宗.md "wikilink")