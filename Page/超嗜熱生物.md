[Grand_prismatic_spring.jpg](https://zh.wikipedia.org/wiki/File:Grand_prismatic_spring.jpg "fig:Grand_prismatic_spring.jpg")的[大棱镜温泉](../Page/大棱镜温泉.md "wikilink")，其中的淺顔色由超嗜熱菌所形成。\]\]

**超嗜熱生物**指能在極熱的環境（60°C以上）中生活的生物。其生长最適溫度通常在80\~110°C，而2003年發現的一株[古菌](../Page/古菌.md "wikilink")“[菌株121](../Page/菌株121.md "wikilink")”\[[https://archive.is/20120529153056/http://www.astrobiology.com/news/viewpr.html?pid=12337\]甚至能在和](https://archive.is/20120529153056/http://www.astrobiology.com/news/viewpr.html?pid=12337%5D甚至能在和)[滅菌鍋相同的溫度](../Page/滅菌鍋.md "wikilink")，即121°C下，24個小時内，細胞數目加倍。多數超嗜熱生物屬於[古菌](../Page/古菌.md "wikilink")，但也有一些[細菌](../Page/細菌.md "wikilink")（包括一些[藍藻](../Page/藍藻.md "wikilink")）可以忍耐70到80°C的高溫。很多超嗜熱生物也可以抵抗其它極端環境，如高酸度或輻射強度。

超嗜熱生物最初於1960年代在[美國](../Page/美國.md "wikilink")[懷俄明州](../Page/懷俄明州.md "wikilink")[黃石公園的熱泉中发现](../Page/黃石公園.md "wikilink")。此後，又發現了50種以上。一些超嗜熱生物需要至少90°C的高温才能夠存活。

儘管目前還沒有發現能在122°C或以上正常生活的生物，但它們的存在還是很有可能的（[菌株121在](../Page/菌株121.md "wikilink")130°C下兩個小時仍存活，但在換入103°C的新鮮培養基前不能繁殖）。然而，大概不存在150°C或更高溫度下存活的生物，因爲[DNA和其它對生命活動很重要的分子在此溫度下會分解](../Page/DNA.md "wikilink")。

超嗜熱生物的[蛋白質需要具有很強的熱穩定性](../Page/蛋白質.md "wikilink")，這依賴於它們在高溫中結構的穩定性，從而使功能保持穩定。這些蛋白和在較低溫度下生活的生物的相應蛋白[同源](../Page/同源.md "wikilink")，但其最強的功能卻在于能在高得多的溫度下發揮。超嗜熱生物的蛋白常有以下特點：内部[氨基酸殘基形成更多的](../Page/氨基酸.md "wikilink")[鹽鍵](../Page/鹽鍵.md "wikilink")，有緊密折疊的疏水核心等。此外，一些超嗜熱生物製造很多胞内溶質，如[磷酸二肌醇酯](../Page/磷酸二肌醇酯.md "wikilink")（di-inositol
phosphate）、[磷酸二甘油酯](../Page/磷酸二甘油酯.md "wikilink")（diglycerol
phosphate）、[甘露糖基甘油酸](../Page/甘露糖基甘油酸.md "wikilink")（mannosylglycerate）等，幫助蛋白質抵抗熱降解。多數低溫下的同源蛋白在60°C就會[變性](../Page/變性_\(生物化學\).md "wikilink")，所以這些熱穩的蛋白具有很高的潜在商業價值，比如，用於高溫下的催化反應。

其它超嗜熱生物：

  - [火葉菌屬的](../Page/火葉菌屬.md "wikilink")[延胡索酸火葉菌](../Page/延胡索酸火葉菌.md "wikilink")（*Pyrolobus
    fumarii*），一種生活在113°C大西洋熱液噴口的[古菌](../Page/古菌.md "wikilink")。

## 參見

  - [中温生物](../Page/中温生物.md "wikilink")（Mesophile）
  - [嗜熱生物](../Page/嗜熱生物.md "wikilink")
  - [嗜極生物](../Page/嗜極生物.md "wikilink")

[Category:嗜热生物](../Category/嗜热生物.md "wikilink")
[Category:间歇泉](../Category/间歇泉.md "wikilink")