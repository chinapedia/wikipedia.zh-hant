**香川郡**（）是隸屬香川縣的一[郡](../Page/郡.md "wikilink")。香川縣的名稱「香川」之名，即是因為設置此縣時，縣廳設於此郡，便以「香川」作為縣名。

現轄有以下1町：

  - [直島町](../Page/直島町.md "wikilink")

過去的範圍還包括現在的[高松市西半部地區](../Page/高松市.md "wikilink")。

## 歷史

[江戶時代末期](../Page/江戶時代.md "wikilink")，除了[直島](../Page/直島.md "wikilink")、[男木島](../Page/男木島.md "wikilink")、[女木島為幕府天領以外](../Page/女木島.md "wikilink")，皆為[高松藩領地](../Page/高松藩.md "wikilink")，但直島、男木島、女木島也是由幕府交由高松藩代管。根據《旧高旧领取调帐》中记载，在明治初期轄有：高松城下、西滨村、宫胁村、中之村、上之村、东滨村、福冈村、松绳村、今里村、伏石村、下多肥村、上多肥村、太田村、鹿角村、一宫村、出作村、三名村、百相村、寺井村、大野村、浅野村、川东上村、川东下村、川内原村、东谷村、安原上村、安原下村、冈村、由佐村、吉光村、横井村、池内村、西庄村、乡东村、笠居村、鹤市村、饭田村、檀纸村、御厩村、中间村、山崎村、冈本村、川部村、圆座村、成合村、勅使村、马场村、冲村、坂田村、万藏村。（1町49村）

1871年[廢藩置縣後](../Page/廢藩置縣.md "wikilink")，最初直島、男木島、女木島成為[倉敷縣所屬](../Page/倉敷縣.md "wikilink")，其於原屬高松藩的地區則屬於[高松縣](../Page/高松縣.md "wikilink")，但不到一年內，又先後一同被整併為[丸龜縣](../Page/丸龜縣.md "wikilink")、[香川縣所屬](../Page/香川縣.md "wikilink")。

1873年後，陸續又被整併為[名東縣](../Page/名東縣.md "wikilink")、香川縣、[愛媛縣](../Page/愛媛縣.md "wikilink")，直到1888年才最終確定屬於再次恢復設置的香川縣。

### 變遷表

[Kagawa_Kagawa-gun_1890.png](https://zh.wikipedia.org/wiki/File:Kagawa_Kagawa-gun_1890.png "fig:Kagawa_Kagawa-gun_1890.png")

  - 1872年：直岛、男木岛、女木岛改隶属香川郡。（1町52村）
  - 1881年：（1町57村）
      - 福冈村被分割为上福冈村、下福冈村。
      - 笠居村被分割为上笠居村、中笠居村、下笠居村。
      - 安原上村的部分地區被分割出獨立設置为安原上东村和安原上西村。
  - 1890年2月15日：實施[市制和](../Page/市制_\(日本制度\).md "wikilink")[町村制](../Page/町村制.md "wikilink")，[高松市從香川郡中分出](../Page/高松市.md "wikilink")，獨立設置為市\[1\]，同時其他地區亦整併如下：（25村）
      - **[高松市](../Page/高松市.md "wikilink")** ←
        内町、東滨町、新材木町、通町、本町、上横町、下横町、北滨材木町、鶴屋町、鱼屋町、内磨屋町、工町、浜之町、天神前、南鍛冶屋町、北龟井町、南龟井町、西瓦町、東瓦町、田町、中新町、新瓦町、南新町、旅籠町、西通町、兵库町、丸龟町、古新町、外磨屋町、南紺屋町、一番町、二番町、三番町、四番町、五番町、六番町、七番町、八番町、九番町、十番町、西新町、西滨町、木藏町、盐屋町、片原町、百間町、大工町、桶屋町、野方町、今新町、七十間町、御坊町、新通町、井口町、新盐屋町、筑地町、福田町、北古馬場町、古馬場町（以上高松城下）、東滨村（一部）、中之村（一部）
      - **[宫胁村](../Page/宫胁村.md "wikilink")** ← 宫胁村、西滨村
      - **[中笠居村](../Page/香西町.md "wikilink")**（单独村制）
      - **[栗林村](../Page/栗林村_\(香川县\).md "wikilink")** ←
        藤冢町（高松城下）、中之村（大部分）、上之村
      - **[東滨村](../Page/東滨村.md "wikilink")** ← 東滨村（大部分）、福冈下村
      - **[太田村](../Page/太田村_\(香川县\).md "wikilink")** ←
        太田村、松绳村、伏石村、今里村、福岡上村
      - **[鹭田村](../Page/鹭田村_\(香川县\).md "wikilink")** ←
        坂田村、勅使村、馬場村、沖村、万藏村
      - **[一宫村](../Page/一宫村_\(香川县\).md "wikilink")** ← 一宫村、成合村、鹿角村、三名村
      - **[多肥村](../Page/多肥村.md "wikilink")** ← 出作村（大部分）、上多肥村、下多肥村
      - **[百相村](../Page/佛生山町.md "wikilink")** ← 百相村、出作村（一部）
      - **[大野村](../Page/大野村_\(香川县\).md "wikilink")** ← 大野村、寺井村
      - **[浅野村](../Page/浅野村_\(香川县\).md "wikilink")**（单独村制）
      - **[川東村](../Page/川東村_\(香川县\).md "wikilink")** ← 川東上村、川東下村、川内原村
      - **[安原村](../Page/安原村_\(香川县\).md "wikilink")** ← 安原下村、東谷村
      - **[安原上東村](../Page/盐江村.md "wikilink")** ← 安原上東村、安原上村
      - **[安原上西村](../Page/上西村.md "wikilink")**（单独村制）
      - **[池西村](../Page/池西村.md "wikilink")** ← 池内村、西庄村、横井村
      - **[由佐村](../Page/由佐村.md "wikilink")** ← 由佐村、岡村、吉光村
      - **[川冈村](../Page/川岡村_\(香川县\).md "wikilink")** ← 川部村、冈本村
      - **[圆座村](../Page/圆座村.md "wikilink")** ← 圆座村、山崎村
      - **[檀纸村](../Page/檀纸村.md "wikilink")** ← 檀纸村、御厩村、中間村
      - **[弦打村](../Page/弦打村.md "wikilink")** ← 鶴市村、飯田村、郷東村
      - **[上笠居村](../Page/上笠居村.md "wikilink")**（单独村制）
      - **[下笠居村](../Page/下笠居村.md "wikilink")**（单独村制）
      - **[雌雄島村](../Page/雌雄島村.md "wikilink")** ← 女木島、男木島
      - **[直島村](../Page/直島町.md "wikilink")**（直島单独村制）
  - 1898年2月11日：百相村改制並改名为[佛生山町](../Page/佛生山町.md "wikilink")。（1町24村）
  - 1914年5月1日：宫胁村被[併入高松市](../Page/市町村合併.md "wikilink")。（1町23村）
  - 1915年2月11日：中笠居村改制並改名为[香西町](../Page/香西町.md "wikilink")。（2町22村）
  - 1918年4月3日：安原上东村改名為盐江村。
  - 1921年1月1日：东滨村被併入高松市。（2町21村）
  - 1921年11月1日：栗林村被併入高松市。（2町20村）
  - 1940年2月11日：鹭田村、太田村被併入高松市。（2町18村）
  - 1951年4月1日：安原上西村改名为上西村。
  - 1954年4月1日：直島村改制为[直島町](../Page/直島町.md "wikilink")。\[2\]（3町17村）
  - 1955年4月1日：大野村、川东村、淺野村合并為[香川町](../Page/香川町.md "wikilink")。（4町14村）
  - 1956年9月30日：（4町）
      - 佛生山町、香西町、一宫村、圆座村、下笠居村、弦打村、雌雄岛村、上笠居村、川冈村、多肥村、檀紙村被併入高松市。
      - 由佐村和池西村合并為[香南町](../Page/香南町.md "wikilink")。
      - 盐江村、上西村、安原村合并為[盐江町](../Page/盐江町.md "wikilink")。
  - 1956年10月25日：盐江町的部分地区被併入香川町。
  - 1958年4月1日：香川町的部分地区被併入高松市。
  - 2005年9月26日：盐江町被併入高松市。（3町）
  - 2006年1月10日：香川町和香南町被併入高松市。（1町）

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1890年2月15日</p></th>
<th><p>1890年 - 1926年</p></th>
<th><p>1926年 - 1954年</p></th>
<th><p>1955年 - 1989年</p></th>
<th><p>1989年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>高松市</p></td>
<td><p>高松市</p></td>
<td><p>高松市</p></td>
<td><p>高松市</p></td>
<td><p>高松市</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>宮脇村</p></td>
<td><p>1914年5月1日<br />
併入高松市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>東濱村</p></td>
<td><p>1921年1月1日<br />
併入高松市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>栗林村</p></td>
<td><p>1921年11月1日<br />
併入高松市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>太田村</p></td>
<td><p>1940年2月11日<br />
併入高松市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>鷺田村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>中笠居村</p></td>
<td><p>1915年2月11日<br />
改制並改名為香西町</p></td>
<td><p>1956年9月30日<br />
併入高松市</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>一宮村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>多肥村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>百相村</p></td>
<td><p>1898年2月11日<br />
改制並改名為佛生山町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>川岡村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>圓座村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>檀紙村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>弦打村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>上笠居村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>下笠居村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>雌雄島村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>安原村</p></td>
<td><p>1956年9月30日<br />
合并為鹽江町</p></td>
<td><p>2005年9月26日<br />
併入高松市</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>安原上東村</p></td>
<td><p>1918年4月3日<br />
改名為鹽江村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>安原上西村</p></td>
<td><p>1951年4月1日<br />
改名為上西村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>大野村</p></td>
<td><p>1955年4月1日<br />
合并為香川町</p></td>
<td><p>2006年1月10日<br />
併入高松市</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>淺野村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>川東村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>池西村</p></td>
<td><p>1956年9月30日<br />
合并為香南町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>由佐村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>直島村</p></td>
<td><p>1954年4月1日<br />
直島町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參考資料

[Category:讚岐國](../Category/讚岐國.md "wikilink")

1.
2.