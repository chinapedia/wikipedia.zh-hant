**周欽南**（），[泰國](../Page/泰國.md "wikilink")[道教](../Page/道教.md "wikilink")[法師](../Page/法師.md "wikilink")，[廣東](../Page/廣東.md "wikilink")[潮州](../Page/潮州.md "wikilink")[華僑](../Page/華僑.md "wikilink")，居於[泰國中部](../Page/泰國.md "wikilink")[春武里府](../Page/春武里府.md "wikilink")[是拉差素拉塞村](../Page/是拉差.md "wikilink")\[1\]。白龍王身世來歷有多個傳說，有人說他以前是個電器維修師傅，又有人說他是維修單車的，總之並非出自大富之家。

據稱[龍神](../Page/龍神.md "wikilink")「白[龍王](../Page/龍王.md "wikilink")」[附身其上](../Page/附身.md "wikilink")，因此人稱**白龍王**。他能為信徒指點迷津，做出[神諭](../Page/神諭.md "wikilink")[預言](../Page/預言.md "wikilink")，渡化有緣人，為人[祈福](../Page/祈福.md "wikilink")。

周欽南平日身穿白衣，每周五、六、日接見信眾，一天見100名，按順序排隊進入，不收費，由信徒自行樂捐。信徒前往白龍廟時，也都會身穿白衣、白褲，表示敬意，並排隊等候進入，進廟前要先插17支香。

## 生平

相傳他40歲夢見白龍王，之後開始以白龍王使者的身分為信徒解惑。

香港演藝圈中，在向氏兄弟，如[向華強](../Page/向華強.md "wikilink")、[向華勝等](../Page/向華勝.md "wikilink")，成為他的信徒之後，陸續吸引了許多香港、台灣等地的演藝圈人士前往泰國，向他請益，如[梁朝偉](../Page/梁朝偉.md "wikilink")、[刘德华](../Page/刘德华.md "wikilink")、[成龍](../Page/成龍.md "wikilink")、[王晶](../Page/王晶_\(導演\).md "wikilink")、[羅志祥](../Page/羅志祥.md "wikilink")、[文雋](../Page/文雋.md "wikilink")、[劉偉強](../Page/劉偉強.md "wikilink")、[譚詠麟](../Page/譚詠麟.md "wikilink")、[陳百祥](../Page/陳百祥.md "wikilink")、[曾志偉](../Page/曾志偉.md "wikilink")、[梅艷芳](../Page/梅艷芳.md "wikilink")、[張國榮](../Page/張國榮.md "wikilink")、[謝霆鋒](../Page/謝霆鋒.md "wikilink")、[余文樂](../Page/余文樂.md "wikilink")、[羅文](../Page/羅文.md "wikilink")、[黎明](../Page/黎明.md "wikilink")、[陶大宇](../Page/陶大宇.md "wikilink")、[鄭秀文](../Page/鄭秀文.md "wikilink")、[楊千嬅](../Page/楊千嬅.md "wikilink")、[彭于晏](../Page/彭于晏.md "wikilink")、[舒淇](../Page/舒淇.md "wikilink")、[梁思浩](../Page/梁思浩.md "wikilink")、[任賢齊](../Page/任賢齊.md "wikilink")、[钟欣桐](../Page/钟欣桐.md "wikilink")、[孟瑤及](../Page/孟瑤_\(藝人\).md "wikilink")[李李仁等](../Page/李李仁.md "wikilink")。香港富豪如[林建岳](../Page/林建岳.md "wikilink")、[楊受成等人也是他的信徒](../Page/楊受成.md "wikilink")。

2010年，罹患[肺炎之後](../Page/肺炎.md "wikilink")，身體狀況轉差。2013年8月17日白龍王支氣管病情惡化，在自宅中病逝，享年76歲。\[2\]2013年10月6日是白龍王[尾七](../Page/作七.md "wikilink")，其弟子为他安放纪念雕像，并进行“[八仙](../Page/八仙.md "wikilink")”[扶乩仪式](../Page/扶乩.md "wikilink")，结果让白龙王女儿「Pet」接掌日后庙中开坛傳遞[神諭](../Page/神諭.md "wikilink")，為善信解决疑难，她登上白龙王昔日宝座，正式成为[黄龙王](../Page/黄龙王.md "wikilink")。

## 流行文化

影星[梁朝偉是著名的周欽南信徒](../Page/梁朝偉.md "wikilink")，經常在每部新片開拍前至泰國請教周欽南。但是周欽南以沒緣份為由，數次拒見其妻[劉嘉玲](../Page/劉嘉玲.md "wikilink")。[張國榮在死前數週](../Page/張國榮.md "wikilink")，曾求見白龍王，但被拒絕。

導演[劉偉強也是周欽南的信徒之一](../Page/劉偉強.md "wikilink")，電影《[無間道](../Page/無間道.md "wikilink")》和《[投名状](../Page/投名状.md "wikilink")》的片名是經過周欽南的指點而決定\[3\]\[4\]。

## 相關

香港富豪林建岳及楊受成等捐款，於[泰國白龍王廟旁興建](../Page/泰國.md "wikilink")[欽南廟及](../Page/欽南廟.md "wikilink")[欽樂亭](../Page/欽樂亭.md "wikilink")，用作供奉白龍王之[父親](../Page/父親.md "wikilink")，2007年7月[開幕](../Page/開幕儀式.md "wikilink")。[白龍王廟離](../Page/白龍王廟.md "wikilink")[曼谷約](../Page/曼谷.md "wikilink")100[公里](../Page/公里.md "wikilink")。而農曆五月五日
[龍王誕](../Page/龍王誕.md "wikilink")，八月九日
[師公誕](../Page/師公誕.md "wikilink")。

## 秦偉開運商品事件

[臺灣藝人](../Page/臺灣.md "wikilink")[秦偉在購物台賣白龍王開運商品](../Page/秦偉.md "wikilink")，每組要價上萬元，電視台抽一半傭金，他稱自己是做賠本生意，而因其是正式弟子，已有不少節目邀他談白龍王，在被控是消費白龍王。
白龍王官方網站信徒留言指出，他在台灣購物台賣的開運物全都奉行典型推銷手法，整起買賣是場騙局，白龍王也並未加持、代言過任何商品，秦偉對此回應稱販售的開運商品購自龍王廟，並未說過是白龍王銷售的。\[5\]

## 参考

<references/>

  - [香港《太陽報》:
    凡間活仙泰國白龍王](http://the-sun.on.cc/channels/adult/20060321/20060320222149_0000.html)
  - [白龍王病逝舒淇網上悼念：知道你一定一路好走](http://hk.apple.nextmedia.com/enews/realtime/20130817/51651656)

## 外部連結

  - [泰國春武里府白龍王廟官方網站](http://www.whitedragonking.com/)

[Z](../Category/泰國民間信仰.md "wikilink")
[Z](../Category/泰國潮汕人.md "wikilink")
[Y](../Category/周姓.md "wikilink")

1.
2.  [泰國直擊 本報獨家專訪白龍王
    首度開腔評香港富豪](http://www.hkheadline.com/ent/ent_content.asp?contid=27893&srctype=n)頭條日報
    頭條網 2007年6月18日
3.
4.
5.  [秦偉消費白龍王？　遭控騙子賣開運假貨](http://tw.news.yahoo.com/%E7%A7%A6%E5%81%89%E6%B6%88%E8%B2%BB%E7%99%BD%E9%BE%8D%E7%8E%8B-%E9%81%AD%E6%8E%A7%E9%A8%99%E5%AD%90%E8%B3%A3%E9%96%8B%E9%81%8B%E5%81%87%E8%B2%A8-010127789.html)