**Phone-in節目**，[香港譯做](../Page/香港.md "wikilink")**烽煙節目**，[台灣稱為](../Page/台灣.md "wikilink")**叩應（Call-in）節目**，是一種[即時](../Page/實時.md "wikilink")[現場直播](../Page/現場直播.md "wikilink")，讓觀眾致電與節目主持人或嘉賓直接對話、發表意見的節目。**Phone-in節目**可以是[電台節目](../Page/電台.md "wikilink")，也可以是[電視節目](../Page/電視節目.md "wikilink")。它的作用，主要是作為溝通雙方面的平台，使大家可以平等直接的對話。

## 政治名嘴

**香港**

随着[政论节目的兴起](../Page/政论节目.md "wikilink")，一大批香港的政论家和政论节目主持人，也成为香港的一代[名嘴](../Page/名嘴.md "wikilink")，如[黄毓民](../Page/黄毓民.md "wikilink")、[鄭經翰](../Page/鄭經翰.md "wikilink")、[陶杰](../Page/陶杰.md "wikilink")、[周融](../Page/周融.md "wikilink")、[吳志森等](../Page/吳志森.md "wikilink")，而一批资历深厚、阅历颇丰的文艺圈人士，因参与政论节目，而成为香港炙手可热的名嘴，如[潘小涛](../Page/潘小涛.md "wikilink")、[梁文道](../Page/梁文道.md "wikilink")、[胡恩威](../Page/胡恩威.md "wikilink")、[刘天赐](../Page/刘天赐.md "wikilink")、[马家辉](../Page/马家辉.md "wikilink")、[谭卫儿](../Page/谭卫儿.md "wikilink")、[张翠容](../Page/张翠容.md "wikilink")、[施南生](../Page/施南生.md "wikilink")、[刘锐绍](../Page/刘锐绍.md "wikilink")、[萧若元](../Page/萧若元.md "wikilink")、[岑建勋](../Page/岑建勋.md "wikilink")、[狄娜](../Page/狄娜.md "wikilink")、[卢觅雪等](../Page/卢觅雪.md "wikilink")。

近年来随着[香港无线电视](../Page/香港无线电视.md "wikilink")、香港[亚洲电视](../Page/亚洲电视.md "wikilink")、[香港有线电视](../Page/香港有线电视.md "wikilink")、[NOW宽频电视和](../Page/NOW宽频电视.md "wikilink")[凤凰卫视的不断壮大](../Page/凤凰卫视.md "wikilink")，各大电视台不断培养出别具一格的主持人。

## 製作成本

Phone-in
節目的製作成本最輕，不必像[卡通片般要](../Page/卡通片.md "wikilink")[配音](../Page/配音.md "wikilink")、又不必像[廣播劇般要](../Page/廣播劇.md "wikilink")[劇本](../Page/劇本.md "wikilink")、更不必像[新聞](../Page/新聞.md "wikilink")[採訪般要](../Page/採訪.md "wikilink")[剪接等後期製作](../Page/剪接.md "wikilink")、又不必像[點唱節目般付](../Page/點唱.md "wikilink")[版稅](../Page/版稅.md "wikilink")。
因為成本效益明顯，所以烽煙節目在港台兩地大行其道。

## 香港的著名Phone-in節目

  - [D100的](../Page/D100.md "wikilink")《[風波裡的茶杯](../Page/風波裡的茶杯.md "wikilink")》（主持：[鄭經翰與](../Page/鄭經翰.md "wikilink")[林旭華](../Page/林旭華.md "wikilink")）
  - [商業電台的](../Page/香港商業電台.md "wikilink")《[政事有心人](../Page/政事有心人.md "wikilink")》（主持：[黃毓民](../Page/黃毓民.md "wikilink")）（已結束）
  - [香港電台的](../Page/香港電台.md "wikilink")《[自由風自由Phone](../Page/自由風自由Phone.md "wikilink")》、《[千禧年代](../Page/千禧年代_\(電台節目\).md "wikilink")》
  - [有線電視的](../Page/香港有線電視.md "wikilink")《[肥媽Phone
    Show](../Page/肥媽Phone_Show.md "wikilink")》（已結束）、《[新龍門陣](../Page/新龍門陣.md "wikilink")》（已結束）、[財經資訊台的](../Page/有線財經資訊台.md "wikilink")《[香港刺針](../Page/香港刺針.md "wikilink")》（已結束）及《[Sunday有理講](../Page/Sunday有理講.md "wikilink")》
  - [now寬頻電視](../Page/now寬頻電視.md "wikilink")[now新聞台的](../Page/now新聞台.md "wikilink")《[時事全方位](../Page/時事全方位.md "wikilink")》。
  - [香港電台第二台的](../Page/香港電台第二台.md "wikilink")《[星空奇遇鐵達尼](../Page/星空奇遇鐵達尼.md "wikilink")》(主持：[麥潤壽](../Page/麥潤壽.md "wikilink"))（已結束）
  - [香港數碼廣播](../Page/香港數碼廣播.md "wikilink")[數碼大家台的](../Page/數碼大家台.md "wikilink")《[星空再遇鐵達尼](../Page/星空再遇鐵達尼.md "wikilink")》(主持：[麥潤壽](../Page/麥潤壽.md "wikilink"))由2011年12月13日啟播（已結束）
  - [商業電台](../Page/商業電台.md "wikilink")[叱吒903的](../Page/叱吒903.md "wikilink")《[一八七二遊花園](../Page/一八七二遊花園.md "wikilink")》（主持﹕[余迪偉](../Page/余迪偉.md "wikilink")、[鄒凱光](../Page/鄒凱光.md "wikilink")、[阿Bu](../Page/阿Bu.md "wikilink")）(已結束)
  - [MyRadio的](../Page/MyRadio.md "wikilink")《毓民踩場》（主持：[黃毓民](../Page/黃毓民.md "wikilink")）
  - [新城知訊台的](../Page/新城知訊台.md "wikilink")《[新香蕉俱樂部](../Page/新香蕉俱樂部.md "wikilink")》（主持﹕[杜浚斌](../Page/杜浚斌.md "wikilink")(Ben)、[林澤群](../Page/林澤群.md "wikilink")(阿群)）

## 澳門的著名Phone-in節目

  - [澳門電台的](../Page/澳門電台.md "wikilink")《澳門講場》。
  - [澳門蓮花衛視的](../Page/澳門蓮花衛視.md "wikilink")《[澳門開講](../Page/澳門開講.md "wikilink")》。

## 台灣的著名Call-in節目

  - [中天電視](../Page/中天電視.md "wikilink")《[全民最大黨](../Page/全民最大黨.md "wikilink")》
  - [TVBS](../Page/TVBS.md "wikilink")《[2100全民開講](../Page/2100全民開講.md "wikilink")》
  - [三立新聞台](../Page/三立新聞台.md "wikilink")《[大話新聞](../Page/大話新聞.md "wikilink")》
  - [華視](../Page/華視.md "wikilink")《李濤新聞廣場》是台灣第一個電視[call-in節目](../Page/call-in.md "wikilink")。[趙怡說](../Page/趙怡.md "wikilink")：“1987年回來，正是[國內政治最動盪的時候](../Page/中華民國.md "wikilink")，那時華視幾乎是[軍方的灘頭堡](../Page/中華民國國軍.md "wikilink")。我在華視是第一個非軍方出身一級主管，開了國內第一個電視叩應節目《李濤新聞廣場》；當時每個觀眾打電話進來，電視台要派兩個人去查，確定對方是誰。”（按：趙怡當時是華視新聞部經理。）\[1\]

## 影響

**電視CALL
IN**是流行於[台灣地區](../Page/台灣.md "wikilink")[有線電視及](../Page/有線電視.md "wikilink")[無線電視特殊節目型態](../Page/無線電視.md "wikilink")，慣發[政治鬥爭議題](../Page/政治鬥爭.md "wikilink")，偶能掀起[政治風潮](../Page/政治風潮.md "wikilink")。

## 註釋

## 參考文獻

  - Crisell, A. (2002) *An Introductory History of British
    Broadcasting*. 2nd ed. London Routledge.
  - Hutchby, I. (1996) *Confrontation Talk: Arguments, Asymmetries and
    Power on Talk Radio*. Mahwah, NJ: Lawrence Erlbaum.
  - Talbot, M., Atkinson, K., and Atkinson, D. (2003) *Language and
    Power in the Modern World*. Edinburgh: Edinburgh University Press.

[\*](../Category/政論節目.md "wikilink")
[Category:广播](../Category/广播.md "wikilink")

1.  《[聯合報](../Page/聯合報.md "wikilink")》2006年10月10日A10版。