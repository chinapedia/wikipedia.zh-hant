[Babinski's_sign01.jpg](https://zh.wikipedia.org/wiki/File:Babinski's_sign01.jpg "fig:Babinski's_sign01.jpg")

**巴宾斯基反射**是一种在刺激足底时出现的神经[反射现象](../Page/反射.md "wikilink")，常被用来观察新生儿神经系统发育情况，和诊断成人[脊髓和](../Page/脊髓.md "wikilink")[脑部疾病](../Page/脑部.md "wikilink")。

## 方法

用钝物从脚后跟向前轻轻地摩擦足底的外侧缘，可能会产生以下三种结果：

  - 屈肌反射：脚趾和整个脚向内屈曲（蹠反射）——这是健康的成人正常的反应。
  - 没有反应。
  - 伸肌反射：拇趾朝向头的方向上翘，同时其他脚趾呈扇形张开；这就是*巴宾斯基反射*的标志。对于几个月大的新生儿，这属于正常反应；但对于超过2岁者，通常这就属于异常反应。但在睡眠状态下和长时间的步行之后（例如士兵行军）出现的巴宾斯基反射仍属于正常
    。

## 解释

这种伸肌反射可以解释为[脊髓损伤](../Page/脊髓.md "wikilink")。

几个月大的新生儿通常都会表现出这种伸肌反射。用手指轻划婴儿脚底外侧，他的拇扯会缓缓地上跷，其余各趾则呈扇形张开。这是由于新生儿的中枢神经通路（锥体束及大脑皮层）还未成熟
，因而该反射不受[大脑皮层抑制](../Page/大脑皮层.md "wikilink")。该反射约在6－18个月逐渐消失，但在睡眠或昏迷中仍可出现。2岁后若再出现此反射，一般是锥体束受损害的表现。　

## 名称来源

该反射现象最早由法国波兰裔神经学家[巴宾斯基](../Page/巴宾斯基.md "wikilink")（Joseph Jules
François Félix Babinski，1857年—1932年）所发现，故得名。

## 注释

## 参考文献

## 外部链接

## 参见

  - [神经系统](../Page/神经系统.md "wikilink")
  - [反射](../Page/反射.md "wikilink")
  - [达尔文反射](../Page/达尔文反射.md "wikilink")（Darwinian
    reflex）、[罗曼尼斯反射或](../Page/达尔文反射.md "wikilink")[抓握反射](../Page/达尔文反射.md "wikilink")　
  - [莫罗氏反射](../Page/莫罗氏反射.md "wikilink")（Moro
    reflex）或[惊跳反射](../Page/莫罗氏反射.md "wikilink")
  - [游泳反射](../Page/游泳反射.md "wikilink")（Swimming reflex）
  - [瞳孔反射](../Page/瞳孔反射.md "wikilink")
  - [吮吸反射](../Page/吮吸反射.md "wikilink")
  - [定向反射](../Page/定向反射.md "wikilink")
  - [巴布金反射](../Page/巴布金反射.md "wikilink")
  - [行走反射](../Page/行走反射.md "wikilink")
  - [蜷缩反射](../Page/蜷缩反射.md "wikilink")
  - [击剑反射](../Page/击剑反射.md "wikilink")

[Category:神经系统](../Category/神经系统.md "wikilink")
[Category:婴儿](../Category/婴儿.md "wikilink")
[Category:反射](../Category/反射.md "wikilink")