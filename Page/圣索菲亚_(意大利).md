**圣索菲亚**（****）是[意大利](../Page/意大利.md "wikilink")[艾米利亚-罗马涅大区](../Page/艾米利亚-罗马涅大区.md "wikilink")[弗利-切塞纳省的一个](../Page/弗利-切塞纳省.md "wikilink")[市镇](../Page/市镇_\(意大利\).md "wikilink")，总面积148.56平方公里，人口4,269人，人口密度28.7人/平方公里（2009年）。ISTAT代码为040043。

## 参考

## 外部链接

  - [www.comune.santa-sofia.fo.it/](https://web.archive.org/web/20060808211802/http://www.comune.santa-sofia.fo.it/)

[S](../Category/弗利-切塞納省市鎮.md "wikilink")