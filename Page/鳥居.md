[Torii_of_Temple_Itsukushima.jpg](https://zh.wikipedia.org/wiki/File:Torii_of_Temple_Itsukushima.jpg "fig:Torii_of_Temple_Itsukushima.jpg")管理的海上鸟居\]\]

**鳥居**（）是[日本](../Page/日本.md "wikilink")[神社的建築之一](../Page/神社.md "wikilink")，傳說是連接神明居住的神域與人類居住的俗世之通道，屬「[結界](../Page/結界.md "wikilink")」的一種。鳥居有多種形狀，但大多均以兩根[支柱與一至二根](../Page/柱.md "wikilink")[橫樑構成](../Page/樑.md "wikilink")，部分鳥居在橫樑中央有[牌匾](../Page/匾.md "wikilink")。

## 概要

[ToriiJapaneseGate.png](https://zh.wikipedia.org/wiki/File:ToriiJapaneseGate.png "fig:ToriiJapaneseGate.png")
[Torii_gate_variation.svg](https://zh.wikipedia.org/wiki/File:Torii_gate_variation.svg "fig:Torii_gate_variation.svg")
豎立鳥居的習俗，據說於設立神社前已經存在\[1\]。

關於鳥居的起源眾說紛紜，據[神道教的天岩户傳說](../Page/神道教.md "wikilink")，一次[天照大神因爲討厭她的兄弟](../Page/天照大神.md "wikilink")，找了一個[山洞躲起來](../Page/山洞.md "wikilink")，用石頭將洞口堵上，人間因此沒有了[太陽](../Page/太陽.md "wikilink")。大家想了一個辦法，建立了一個高高的木架，將所有的公雞放到上面，讓所有的公雞一起啼叫，天照大神感到奇怪，推開石頭看看，那些躲在一旁的相撲力士們立刻抓住機會合力將石頭推開，這個世界就重新大放光明了。這個木架就是第一個鳥居。\[2\]由于在[日語中](../Page/日語.md "wikilink")，“鳥”（）這個漢字單用時，在指鳥類之外也可以指[雞](../Page/雞.md "wikilink")，因此鳥居可以被譯為“置放雞的木架”。此外也有说法指鳥居是源於[中國的](../Page/中國.md "wikilink")[牌坊](../Page/牌坊.md "wikilink")、[朝鮮半島](../Page/朝鮮半島.md "wikilink")、[印度的](../Page/印度.md "wikilink")[托拉纳等](../Page/托拉纳.md "wikilink")。

在日本各地的神社之中，位於[京都的](../Page/京都.md "wikilink")[伏見稻荷大社是因為鳥居而聞名的神社之一](../Page/伏見稻荷大社.md "wikilink")。供奉商業之神的該神社由于有人许愿就敬献一座鸟居，經年累月之下神社境內豎立的鸟居排列成一座座長長的甬道，而被稱為「千本鳥居」。除此之外，位於[廣島縣離島](../Page/廣島縣.md "wikilink")[宮島上的](../Page/宮島.md "wikilink")[嚴島神社](../Page/嚴島神社.md "wikilink")，也以其豎立在海中的大鳥居著名，是常被用來象徵日本傳統信仰文化的知名鳥居之一。

## 图片集

<File:Heianjingu> torii.jpg|平安神宮大鳥居
<File:KyotoFushimiInariLarge.jpg>|[京都](../Page/京都.md "wikilink")[伏見稻荷大社境內鳥居甬道](../Page/伏見稻荷大社.md "wikilink")（千本鳥居）
<File:Kehi> Jingu Torii.jpg|[氣比神宮的大鳥居](../Page/氣比神宮.md "wikilink")
<File:Tomb> of Emperor Yosei.jpg|[陽成天皇陵](../Page/陽成天皇.md "wikilink")
<File:Kasuga> shrine
200608022110.JPG|[福冈县](../Page/福冈县.md "wikilink")[春日市](../Page/春日市.md "wikilink")[春日神社](../Page/春日神社_\(春日市\).md "wikilink")（宝永七年奉納）
<File:Kumanonachitaisha3s2048.jpg>|[熊野那智大社](../Page/熊野那智大社.md "wikilink")
二的鳥居
<File:Kogarasu-jinja01s3872.jpg>|[木烏神社的鳥居](../Page/木烏神社.md "wikilink")
<File:Inscriptions> on torii, Fushimi Inari shrine,
Kyoto.jpg|日本京都伏见稻荷神社前鸟居上奉献者的题名 <File:Taoyuan>
Shinto
shrines-1.jpg|[台灣](../Page/台灣.md "wikilink")[桃園神社的鳥居](../Page/桃園神社.md "wikilink")-已被修改過。
<File:Nikko> Toshogu Outer Torii
M3032.jpg|[日光東照宮明神鸟居](../Page/日光東照宮.md "wikilink")
<File:Sakahogi> Jinja2008-2.jpg|神明鸟居 <File:Fujisan> Hongū Sengen Taisha
daitorii.jpg|[富士山本宮淺間大社](../Page/富士山本宮淺間大社.md "wikilink")

## 参考资料

## 參見

  - [神道坊](../Page/神道坊.md "wikilink")：設於寺廟、陵園入口連接[神道
    (道路)](../Page/神道_\(道路\).md "wikilink")（參道）的[牌坊](../Page/牌坊.md "wikilink")

  - ：朝鮮傳統建築中設於寺廟、陵園、宮殿入口，連接神道、[御路的紅色牌坊](../Page/御路.md "wikilink")

  - （[沖繩縣](../Page/沖繩縣.md "wikilink")[讀谷村](../Page/讀谷村.md "wikilink")），[駐日美軍通訊設施](../Page/駐日美軍.md "wikilink")

  - [伏見稻荷大社](../Page/伏見稻荷大社.md "wikilink")

## 外部链接

  - [玄松子・神社知識](http://www.genbu.net/tisiki/index.htm)

[Category:神社](../Category/神社.md "wikilink") [Category:日本傳統的門
(建築物)](../Category/日本傳統的門_\(建築物\).md "wikilink")
[Category:神道](../Category/神道.md "wikilink")

1.
2.  《日本書紀》：“是時天照大神驚動，以梭傷身。由此發慍，乃入于天石窟，閉磐戶而幽居焉。故六合之內常闇，而不知晝夜之相代。于時八十萬神會合於天安河邊，計其可禱之方。故思兼神深謀遠慮，遂聚常世之長鳴鳥，使互長鳴。”