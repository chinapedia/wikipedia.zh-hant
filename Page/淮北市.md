**淮北市**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[安徽省下辖的](../Page/安徽省.md "wikilink")[地级市](../Page/地级市.md "wikilink")，位于安徽省北部，[濉河沿岸](../Page/濉河.md "wikilink")，鲁、苏、豫、皖四省交界处，[符夹铁路线上](../Page/符夹铁路.md "wikilink")。古代这里称**相**、**相县**。素有煤城、酒乡之称。

2017年11月15日，淮北市入选第五届[全国文明城市](../Page/全国文明城市.md "wikilink")。2017年12月28日，淮北市成为[安徽省第](../Page/安徽省.md "wikilink")13座通[高铁的地级市](../Page/高铁.md "wikilink")，可直达[上海](../Page/上海.md "wikilink")、[北京](../Page/北京.md "wikilink")、[合肥等重要城市](../Page/合肥.md "wikilink")。

## 历史沿革

[殷商之前即是部落驻地](../Page/殷商.md "wikilink")。相传[商朝的](../Page/商朝.md "wikilink")[商汤十一世祖](../Page/商汤.md "wikilink")[相土建城于今](../Page/相土.md "wikilink")[相山之南](../Page/相山.md "wikilink")，相山、相城由此得名。《[诗经](../Page/诗经.md "wikilink")》上载有“相土烈烈，海外有载”。[春秋](../Page/春秋.md "wikilink")[宋国](../Page/宋国.md "wikilink")[宋共公在此建都](../Page/宋共公.md "wikilink")。[战国时相属](../Page/战国.md "wikilink")[楚](../Page/楚.md "wikilink")。[秦始皇三十三年](../Page/秦始皇.md "wikilink")（前224年）置[相县](../Page/相县.md "wikilink")，属[泗水郡](../Page/泗水郡.md "wikilink")，为郡治。[汉高祖四年](../Page/汉高祖.md "wikilink")（前203年），改泗水郡为[沛郡](../Page/沛郡.md "wikilink")，相城仍为郡治。

[三国](../Page/三国.md "wikilink")[魏时期](../Page/魏.md "wikilink")，[魏文帝分沛国置](../Page/魏文帝.md "wikilink")[汝阴郡](../Page/汝阴郡.md "wikilink")，相县属汝阴郡。[隋](../Page/隋.md "wikilink")[开皇九年](../Page/开皇.md "wikilink")（589年），相城为[符离县辖地](../Page/符离县.md "wikilink")，属[彭城郡](../Page/彭城郡.md "wikilink")。直至[中华人民共和国建国前](../Page/中华人民共和国.md "wikilink")，相城再无县或以上的行政设置。

1949年6月，市境分属[宿县和](../Page/宿县.md "wikilink")[萧县](../Page/萧县.md "wikilink")。1950年7月1日，析[宿县西境析置](../Page/宿县.md "wikilink")[宿西县](../Page/宿西县.md "wikilink")，同年改名[濉溪县](../Page/濉溪县.md "wikilink")，属[皖北人民行政公署](../Page/皖北人民行政公署.md "wikilink")[宿县行政专区公署](../Page/宿县专区.md "wikilink")。1953年2月，市境分属[濉溪县和](../Page/濉溪县.md "wikilink")[萧县](../Page/萧县.md "wikilink")。1958年5月成立淮北煤矿筹备处，及至1960年4月，设立[濉溪市](../Page/濉溪市.md "wikilink")，为省辖市。1971年4月，濉溪市更名为淮北市。1977年2月，原属[宿县地区的濉溪县划归淮北市](../Page/宿县地区.md "wikilink")。1981年，由萧县[龙城镇](../Page/龙城镇_\(萧县\).md "wikilink")，析出[段园镇划入淮北煤矿](../Page/段园镇.md "wikilink")（今杜集区的飞地）。全市辖[相山区](../Page/相山区.md "wikilink")、[杜集区](../Page/杜集区.md "wikilink")、[烈山区和](../Page/烈山区.md "wikilink")[濉溪县至今](../Page/濉溪县.md "wikilink")。

## 地理

### 地形

[地形以](../Page/地形.md "wikilink")[平原](../Page/平原.md "wikilink")、丘陵为主，东部、北部为[丘陵](../Page/丘陵.md "wikilink")。[濉河流贯全境](../Page/濉河.md "wikilink")。

### 气候

[温带季风气候](../Page/温带季风气候.md "wikilink")，年均[气温](../Page/气温.md "wikilink")14.5℃，年[降水量](../Page/降水量.md "wikilink")892.8毫米，[四季分明](../Page/四季.md "wikilink")。

## 政治

### 现任领导

<table>
<caption>淮北市四大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党淮北市委员会.md" title="wikilink">中国共产党<br />
淮北市委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/淮北市人民代表大会.md" title="wikilink">淮北市人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/淮北市人民政府.md" title="wikilink">淮北市人民政府</a><br />
<br />
市长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议淮北市委员会.md" title="wikilink">中国人民政治协商会议<br />
淮北市委员会</a><br />
主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/黄晓武.md" title="wikilink">黄晓武</a>[1]</p></td>
<td><p><a href="../Page/戴启远.md" title="wikilink">戴启远</a>[2]</p></td>
<td><p><a href="../Page/谌伟.md" title="wikilink">谌伟</a>[3]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p><a href="../Page/安徽省.md" title="wikilink">安徽省</a><a href="../Page/怀宁县.md" title="wikilink">怀宁县</a></p></td>
<td><p>安徽省<a href="../Page/五河县.md" title="wikilink">五河县</a></p></td>
<td><p>安徽省<a href="../Page/寿县.md" title="wikilink">寿县</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2016年8月</p></td>
<td><p>2016年9月</p></td>
<td><p>2016年9月</p></td>
<td><p>2018年1月</p></td>
</tr>
</tbody>
</table>

### 行政区划

现辖3个[市辖区](../Page/市辖区.md "wikilink")、1个[县](../Page/县_\(中华人民共和国\).md "wikilink")。

  - 市辖区：[杜集区](../Page/杜集区.md "wikilink")、[相山区](../Page/相山区.md "wikilink")、[烈山区](../Page/烈山区.md "wikilink")
  - 县：[濉溪县](../Page/濉溪县.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p><strong>淮北市行政区划图</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>区划代码[4]</p></td>
</tr>
<tr class="odd">
<td><p>街道</p></td>
</tr>
<tr class="even">
<td><p>340600</p></td>
</tr>
<tr class="odd">
<td><p>340602</p></td>
</tr>
<tr class="even">
<td><p>340603</p></td>
</tr>
<tr class="odd">
<td><p>340604</p></td>
</tr>
<tr class="even">
<td><p>340621</p></td>
</tr>
</tbody>
</table>

## 人口

<table>
<caption><strong>淮北市各区（县）人口数据</strong></caption>
<thead>
<tr class="header">
<th><p>区划名称</p></th>
<th><p>常住人口[5]（2010年11月）</p></th>
<th><p>户籍人口[6]<br />
（2010年末）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>总计</p></td>
<td><p>比重<br />
（%）</p></td>
<td><p>每平方公里<br />
人口密度</p></td>
</tr>
<tr class="even">
<td><p>淮北市</p></td>
<td><p>2114276</p></td>
<td><p>100</p></td>
</tr>
<tr class="odd">
<td><p>杜集区</p></td>
<td><p>324398</p></td>
<td><p>15.34</p></td>
</tr>
<tr class="even">
<td><p>相山区</p></td>
<td><p>467358</p></td>
<td><p>22.10</p></td>
</tr>
<tr class="odd">
<td><p>烈山区</p></td>
<td><p>321565</p></td>
<td><p>15.21</p></td>
</tr>
<tr class="even">
<td><p>濉溪县</p></td>
<td><p>1000955</p></td>
<td><p>47.34</p></td>
</tr>
</tbody>
</table>

根据2010年[第六次全国人口普查](../Page/第六次全国人口普查.md "wikilink")，全市常住人口2114276人\[7\]，同[第五次全国人口普查相比](../Page/第五次全国人口普查.md "wikilink")，十年共增加239676人，增长12.79%，年平均增长1.21%。其中，男性人口1070553人，占50.63%；女性人口1043723人，占49.37%。总人口性别比（以女性100）为102.57。0－14岁人口357928人，占16.93%
；15－64岁人口1562229人，占73.89%；65岁及以上人口194119人，占9.18%。

## 经济

  - 酿酒工业历史悠久。
  - 工业以煤炭、电力、纺织、酿酒、建材为支柱产业。
  - 郊区农产小麦、豆类、棉花、花生等。
  - 国民生产总值(GDP):799亿元（2017年）。
  - 人均国民生产总值(GNP):36427元（2017年）。

[huaibeiview.jpg](https://zh.wikipedia.org/wiki/File:huaibeiview.jpg "fig:huaibeiview.jpg")

## 交通

  - 青阜线、符夹线貫通全市。
  - [高速铁路方面](../Page/中华人民共和国高速铁路.md "wikilink")，[淮萧联络线已开通运营](../Page/淮北至萧县北客车联络线.md "wikilink")，通过与[郑徐客运专线相连](../Page/郑徐客运专线.md "wikilink")，沟通[京沪高铁](../Page/京沪高铁.md "wikilink")。

## 资源

  - 煤炭产量居中国第五位，为华东最大的煤炭生产与洗选加工基地。
  - 特产有[口子酒](../Page/口子酒.md "wikilink")、煤等。

## 名胜

  - 周[宋共姬墓](../Page/宋共姬.md "wikilink")、[薛广德传诗处](../Page/薛广德.md "wikilink")、[刘贞简故里](../Page/刘贞简.md "wikilink")、东汉[桓谭藏书处](../Page/桓谭.md "wikilink")、[藕花墅等遗址](../Page/藕花墅.md "wikilink")。
  - 北山峪中有[显通寺](../Page/显通寺.md "wikilink")，习称相山庙，周围有奏鸣台、渗水崖、[宋襄王饮马池](../Page/宋襄王.md "wikilink")、钓鱼台、出云台等名胜。
  - 境内的双堆集，是[淮海战役全歼](../Page/淮海战役.md "wikilink")[黄维兵团之处](../Page/黄维.md "wikilink")，建有烈士陵园和纪念塔。
  - [相山公园](../Page/相山公园.md "wikilink")（中国国家4A级旅游景区，对公众免票开放。公园中显通寺内有[乾隆皇帝题字](../Page/乾隆.md "wikilink")“惠我南黎”。）
  - 龙脊山自然风景区（最高峰海拔363米，为淮北海拔之最，相传为八仙中[张果老升仙处](../Page/张果老.md "wikilink")。）

## 教育

  - 高校：[淮北师范大学](../Page/淮北师范大学.md "wikilink")，[淮北职业技术学院](../Page/淮北职业技术学院.md "wikilink")，[安徽矿业职业技术学院](../Page/安徽矿业职业技术学院.md "wikilink")，[淮北煤电技师学院等](../Page/淮北煤电技师学院.md "wikilink")。
  - 中等学校：[淮北师范学校](../Page/淮北师范学校.md "wikilink")，[淮北艺术学校](../Page/淮北艺术学校.md "wikilink")，[淮北卫生学校](../Page/淮北卫生学校.md "wikilink")，
    [淮北市工业学校](../Page/淮北市工业学校.md "wikilink")，淮北市第二高级职业中学，淮北市职业技术学院中专部，淮北煤电技师学院，濉溪县职教中心，烈山区职教中心，杜集区职教中心，淮北市西园中学，淮北市第二中学等
  - 安徽省省级示范高中：[淮北市第一中学](../Page/淮北市第一中学.md "wikilink")，[淮北市实验高级中学](../Page/淮北市实验高级中学.md "wikilink")，淮北市第十二中学，淮北师范大学附属实验中学，[濉溪中学](../Page/濉溪中学.md "wikilink")，濉溪二中，[临涣中学](../Page/临涣中学.md "wikilink")。

## 市花市树

  - [梅花](../Page/梅花.md "wikilink")、[月季为淮北市市花](../Page/月季.md "wikilink")，[银杏](../Page/银杏.md "wikilink")、[国槐为淮北市市树](../Page/国槐.md "wikilink")。

## 现任政府成员

  - 戴启远市长
  - 李明常务副市长
  - 朱浩东副市长
  - 孙劲飚副市长
  - 王莉莉副市长
  - 胡百平副市长
  - 宋伟秘书长

## 文物保护单位

### 国家级

  - [柳孜隋唐大运河码头遗址](../Page/柳孜隋唐大运河码头遗址.md "wikilink")
  - [临涣淮海战役总前委旧址](../Page/临涣淮海战役总前委旧址.md "wikilink")（临涣文昌宫）
  - [临涣古城墙遗址](../Page/临涣古城墙遗址.md "wikilink")
  - [石山孜遗址](../Page/石山孜遗址.md "wikilink")
  - [古城汉墓](../Page/古城汉墓.md "wikilink")

### 省级

  - 共姬墓
  - [显通寺](../Page/显通寺.md "wikilink")
  - 汉阚遗址及水牛墓
  - 小李庄淮海战役总前委旧址
  - 化家湖遗址
  - 徐集徐氏祠堂
  - 赵集二级扬水站旧址
  - 濉溪老街古建筑群
  - [口子酒古窖池及中西式建筑群](../Page/口子酒.md "wikilink")
  - 宋疃青龙寺
  - 相山秘霞洞石刻

## 参考文献

## 外部链接

  - [淮北在线](http://www.huaibei.ccoo.cn/)
  - [淮北市政府](https://web.archive.org/web/20051013071818/http://www.huaibei.gov.cn/)

{{-}}

[Category:安徽地级市](../Category/安徽地级市.md "wikilink")
[淮北](../Category/淮北.md "wikilink")
[皖](../Category/中国大城市.md "wikilink")
[Category:中华人民共和国第二批资源枯竭型城市](../Category/中华人民共和国第二批资源枯竭型城市.md "wikilink")
[Category:中原经济区城市](../Category/中原经济区城市.md "wikilink")
[Category:国家园林城市](../Category/国家园林城市.md "wikilink")
[5](../Category/全国文明城市.md "wikilink")

1.
2.
3.
4.
5.
6.
7.