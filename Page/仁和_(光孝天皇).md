**仁和**（885年二月廿一至889年四月廿七）是[日本的](../Page/日本.md "wikilink")[年號](../Page/年號.md "wikilink")，在[元慶之後](../Page/元慶.md "wikilink")、[寬平之前](../Page/寬平.md "wikilink")。這時代的[天皇是](../Page/天皇.md "wikilink")[平安時代之](../Page/平安時代.md "wikilink")[光孝天皇和](../Page/光孝天皇.md "wikilink")[宇多天皇](../Page/宇多天皇.md "wikilink")。

## 改元

  - 元慶九年二月廿一（885年3月11日） 改元。
  - 仁和五年四月廿七（889年5月30日） 改元寬平。

## 出處

## 大事記

  - 仁和四年（888年） 興建[仁和寺](../Page/仁和寺.md "wikilink")

## 出生

## 逝世

## 紀年

|                                |                                |                                |                                |                                |                                |
| ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| 仁和                             | 元年                             | 二年                             | 三年                             | 四年                             | 五年                             |
| [公元](../Page/公元.md "wikilink") | 885年                           | 886年                           | 887年                           | 888年                           | 889年                           |
| [干支](../Page/干支.md "wikilink") | [乙巳](../Page/乙巳.md "wikilink") | [丙午](../Page/丙午.md "wikilink") | [丁未](../Page/丁未.md "wikilink") | [戊申](../Page/戊申.md "wikilink") | [己酉](../Page/己酉.md "wikilink") |

## 參考

  - [日本年號索引](../Page/日本年號索引.md "wikilink")
  - 同期存在的其他政權之紀年
      - [中和](../Page/中和_\(唐僖宗\).md "wikilink")（881年七月至885年三月）：[唐朝唐僖宗之年號](../Page/唐朝.md "wikilink")
      - [光啟](../Page/光啟.md "wikilink")（885年三月至888年正月）：唐朝唐僖宗之年號
      - [建貞](../Page/建貞.md "wikilink")（886年十月至十二月）：唐朝時期貴族領袖襄王[李熅之年號](../Page/李熅.md "wikilink")
      - [文德](../Page/文德_\(唐僖宗\).md "wikilink")（888年二月至十二月）：唐朝唐僖宗、[唐昭宗之年號](../Page/唐昭宗.md "wikilink")
      - [龍紀](../Page/龍紀.md "wikilink")（889年正月至十二月）：唐朝唐昭宗之年號
      - [貞明](../Page/貞明_\(隆舜\).md "wikilink")（878年起）：[南詔領袖](../Page/南詔.md "wikilink")[隆舜之年號](../Page/隆舜.md "wikilink")
      - [承智](../Page/承智.md "wikilink")：南詔領袖隆舜之年號
      - [大同](../Page/大同_\(隆舜\).md "wikilink")（至888年）：南詔領袖隆舜之年號
      - [嵯耶](../Page/嵯耶.md "wikilink")（889年至897年）：南詔領袖隆舜之年號

## 參考文獻

  - 松橋達良，《元号はやわかり—東亜歷代建元考》，砂書房，1994年7月，ISBN 4-915818-27-6
  - 李崇智，《中国历代年号考》，中华书局，2001年1月ISBN 7-101-02512-9

[Category:9世纪日本年号](../Category/9世纪日本年号.md "wikilink")
[Category:880年代日本](../Category/880年代日本.md "wikilink")