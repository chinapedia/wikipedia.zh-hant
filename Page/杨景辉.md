**楊景輝**，出生于[中国](../Page/中国.md "wikilink")[广东省](../Page/广东省.md "wikilink")[广州市](../Page/广州市.md "wikilink")，[中國](../Page/中國.md "wikilink")[跳水](../Page/跳水.md "wikilink")[運動員](../Page/運動員.md "wikilink")。

## 生涯

楊景輝最先在广州市[越秀区跳水队接受训练](../Page/越秀区.md "wikilink")，其教练是[王友光](../Page/王友光.md "wikilink")，3年之後入讀广东省运动学院，當時楊景輝有兩個教练，分別是[谭良德以及](../Page/谭良德.md "wikilink")[李青](../Page/李青_\(跳水运动员\).md "wikilink")。經過3年的培訓後，楊景輝入选国家跳水集训队，那時楊景輝的教练是谭良德。4年後楊景輝入選国家队，教练是[鍾少珍](../Page/鍾少珍.md "wikilink")。

在入選国家跳水集训队的第4年，楊景輝奪得了[九运会中的男子团体](../Page/九运会.md "wikilink")[金牌](../Page/金牌.md "wikilink")。兩年後的[世界大学生运动会男子双人跳台](../Page/世界大学生运动会.md "wikilink")[金牌](../Page/金牌.md "wikilink")。

2004年，楊景輝更上一層樓，分別奪得了[国际游联跳水大奖赛](../Page/国际游联跳水大奖赛.md "wikilink")[加拿大站男子](../Page/加拿大.md "wikilink")10米跳台冠军以及與[田亮聯手奪得了](../Page/田亮.md "wikilink")[雅典奧運的男子雙人](../Page/2004年夏季奧林匹克運動會.md "wikilink")10米高臺[金牌](../Page/金牌.md "wikilink")。\[1\]

2005年，楊景輝分別奪得了[国际泳联跳水大奖赛](../Page/国际泳联.md "wikilink")[美国站以及](../Page/美国.md "wikilink")[珠海站的男子](../Page/珠海.md "wikilink")10米台双人冠军及[2005年世界游泳錦標賽亞軍](../Page/世錦賽.md "wikilink")（與[胡佳合作](../Page/胡佳_\(1983年\).md "wikilink")）。

2005年后，楊景輝退出了國家跳水隊主力陣容，渐渐淡出人们的视线。之後，楊景輝入讀于[中山大学行政管理系](../Page/中山大学.md "wikilink")，碩士畢業後于[广东体育职业技术学院任职团委副书记](../Page/广东体育职业技术学院.md "wikilink")。\[2\]

## 參考資料

## 外部連結

[Category:广州籍运动员](../Category/广州籍运动员.md "wikilink")
[Category:中国奥运跳水运动员](../Category/中国奥运跳水运动员.md "wikilink")
[Category:中国跳水运动员](../Category/中国跳水运动员.md "wikilink")
[Category:中國奧林匹克運動會金牌得主](../Category/中國奧林匹克運動會金牌得主.md "wikilink")
[Category:杨姓](../Category/杨姓.md "wikilink")
[Category:2004年夏季奧林匹克運動會跳水運動員](../Category/2004年夏季奧林匹克運動會跳水運動員.md "wikilink")
[Category:2004年夏季奧林匹克運動會金牌得主](../Category/2004年夏季奧林匹克運動會金牌得主.md "wikilink")
[Category:中山大学校友](../Category/中山大学校友.md "wikilink")
[Category:奧林匹克運動會跳水獎牌得主](../Category/奧林匹克運動會跳水獎牌得主.md "wikilink")

1.  [我的奥林匹克](http://space.tv.cctv.com/act/video.jsp?videoId=VIDE1208273743051188)
2.  [杨景辉：不跳水就回学校吧](http://roll.sohu.com/20120725/n348951944.shtml)