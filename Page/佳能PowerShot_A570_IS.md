**佳能 PowerShot A570
IS**是一款[佳能](../Page/佳能.md "wikilink")[数码相机](../Page/数码相机.md "wikilink")，于[2007年3月推出](../Page/2007年3月.md "wikilink")。

A570 IS是目前[PowerShot
A系列里头装备了图像稳定器](../Page/Canon_PowerShot.md "wikilink")（IS）的最低端产品，2007年上市之后价格迅速调整到1500附近，成为[佳能用来抢占低端市场的有力产品](../Page/佳能.md "wikilink")。

随着[A650
IS召回](../Page/Canon_PowerShot_A650_IS.md "wikilink")，短期内佳能无法推出合适的填补空档的机型，A570
IS与[Canon PowerShot A720
IS成为仅存的市面在售的两款带防抖功能的](../Page/Canon_PowerShot_A720_IS.md "wikilink")[佳能PowerShot数码相机](../Page/Canon_PowerShot.md "wikilink")。

2008年1月，继任者[Canon PowerShot A590
IS推出](../Page/Canon_PowerShot_A590_IS.md "wikilink")。保留了A570
IS的所有特性，但在像素上有升级，颜色也变为黑色。

## 主要参数

  - 710万 有效[象素](../Page/象素.md "wikilink")
  - 4倍光学[变焦](../Page/变焦.md "wikilink")
  - 1/2.5 英寸 [CCD](../Page/CCD.md "wikilink")
  - 2.5寸TFT液晶屏，分辨率11.5万象素
  - 快门：15～1/2000秒
  - [ISO](../Page/ISO.md "wikilink")：80/100/200/400/800/1600
  - 面部优先／9点智能AiAF／中央单点
  - 有声短片记录（[Motion JPEG编码与单声道音频](../Page/Motion_JPEG.md "wikilink")）
  - 使用[SDHC卡](../Page/SDHC卡.md "wikilink")／[SD卡](../Page/SD卡.md "wikilink")／[MMC卡作为存储介质](../Page/MMC卡.md "wikilink")
  - 使用[DIGIC III数字处理芯片](../Page/DIGIC.md "wikilink")
  - 使用2节[AA电池](../Page/AA电池.md "wikilink")
  - [Exif 2.2](../Page/EXIF.md "wikilink")
  - 不带电池175克

## 参见

  - [Canon PowerShot](../Page/Canon_PowerShot.md "wikilink")
  - [Canon PowerShot A650
    IS](../Page/Canon_PowerShot_A650_IS.md "wikilink")
  - [Canon PowerShot A710
    IS](../Page/Canon_PowerShot_A710_IS.md "wikilink")
  - [Canon PowerShot A720
    IS](../Page/Canon_PowerShot_A720_IS.md "wikilink")
  - [Canon PowerShot A590
    IS](../Page/Canon_PowerShot_A590_IS.md "wikilink")

## 外部链接

  - [PowerShot A570
    IS](https://web.archive.org/web/20070630235255/http://www.canon.com.cn/front/product/product_main.jsp?id=2355&content=spec_attache_consume&type=spec)－佳能（中国）

[en:Canon PowerShot A570
IS](../Page/en:Canon_PowerShot_A570_IS.md "wikilink")

[Category:佳能相機](../Category/佳能相機.md "wikilink")