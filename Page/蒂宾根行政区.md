| 行政区的位置                                                                                                                                              |
| --------------------------------------------------------------------------------------------------------------------------------------------------- |
| [Locator_map_RB_TÜ_in_Germany.svg](https://zh.wikipedia.org/wiki/File:Locator_map_RB_TÜ_in_Germany.svg "fig:Locator_map_RB_TÜ_in_Germany.svg") |
| 基本数据                                                                                                                                                |
| 所属联邦州:                                                                                                                                              |
| 首府:                                                                                                                                                 |
| 面积:                                                                                                                                                 |
| 人口:                                                                                                                                                 |
| 人口密度:                                                                                                                                               |
| 下设行政区划:                                                                                                                                             |
| 行政区政府                                                                                                                                               |
| 政府地址:                                                                                                                                               |
| 政府网站:                                                                                                                                               |
| 行政区地图                                                                                                                                               |
| [BW-RB-Tuebingen.png](https://zh.wikipedia.org/wiki/File:BW-RB-Tuebingen.png "fig:BW-RB-Tuebingen.png")                                             |

**蒂宾根行政区**（Regierungsbezirk
Tübingen）是[德国](../Page/德国.md "wikilink")[巴登-符腾堡州的](../Page/巴登-符腾堡州.md "wikilink")4个[行政区之一](../Page/行政区_\(德国\).md "wikilink")，首府[蒂宾根](../Page/蒂宾根.md "wikilink")。蒂宾根行政区的行政级别隶属于巴登-符腾堡州政府，行政区下设8个县和1个直辖市。

## 行政区划

图宾根行政区下设8个县：

1.  [山地-多瑙县](../Page/山地-多瑙县.md "wikilink")（Alb-Donau-Kreis），首府[乌尔姆](../Page/乌尔姆.md "wikilink")（Ulm）
2.  [比伯拉赫县](../Page/比伯拉赫县.md "wikilink")（Biberach），首府[比伯拉赫](../Page/比伯拉赫.md "wikilink")（Biberach）
3.  [博登湖县](../Page/博登湖县.md "wikilink")（Bodenseekreis），首府[腓特烈港](../Page/腓特烈港.md "wikilink")（Friedrichshafen）
4.  [拉芬斯堡县](../Page/拉芬斯堡县.md "wikilink")（Ravensburg），首府[拉芬斯堡](../Page/拉芬斯堡.md "wikilink")（Ravensburg）
5.  [罗伊特林根县](../Page/罗伊特林根县.md "wikilink")（Reutlingen），首府[罗伊特林根](../Page/罗伊特林根.md "wikilink")（Reutlingen）
6.  [锡格马林根县](../Page/锡格马林根县.md "wikilink")（Sigmaringen），首府[锡格马林根](../Page/锡格马林根.md "wikilink")（Sigmaringen）
7.  [蒂宾根县](../Page/蒂宾根县.md "wikilink")（Tübingen），首府[蒂宾根](../Page/蒂宾根.md "wikilink")（Tübingen）
8.  [措伦阿尔布县](../Page/措伦阿尔布县.md "wikilink")（Zollernalbkreis），首府[巴林根](../Page/巴林根.md "wikilink")（Balingen）

图宾根行政区下设1个直辖市：

1.  [乌尔姆市](../Page/乌尔姆.md "wikilink")（Ulm）

[巴登-符腾堡州](../Category/德国行政区.md "wikilink")
[行政区](../Category/巴登-符腾堡州行政区划.md "wikilink")