[Uhrzeigersinn.png](https://zh.wikipedia.org/wiki/File:Uhrzeigersinn.png "fig:Uhrzeigersinn.png")

以**順時針方向**運行指依從時針移動的方向運行（如右上圖），即可視為由右上方向下，然後轉向左，再回到上。[數學上](../Page/數學.md "wikilink")，在[直角坐标系以方程式](../Page/直角坐标系.md "wikilink")*x*
= [sin](../Page/正弦.md "wikilink") *t* 和 *y* =
[cos](../Page/餘弦.md "wikilink") *t*
定義的[圓形](../Page/圓形.md "wikilink")，隨着t值減少所代表的曲線就是依**順時針方向**繪畫。換一種說法，當一個物體不斷向右方移動所形成的軌跡，也是循着**順時針方向**移動。順時針的相反方向是[逆時針方向](../Page/逆時針方向.md "wikilink")。

[Gegenuhrzeigersinn.png](https://zh.wikipedia.org/wiki/File:Gegenuhrzeigersinn.png "fig:Gegenuhrzeigersinn.png")\]\]

一旋轉運動屬於**順時針**抑或[逆時針](../Page/逆時針.md "wikilink")，視乎觀察面的設定。例如，由[北極正上方看下去](../Page/北極.md "wikilink")，[地球自轉屬於](../Page/地球自轉.md "wikilink")[逆時針](../Page/逆時針.md "wikilink")，但由[南極正上方看下去](../Page/南極.md "wikilink")，地球自轉則是順時針。

## 起源

時針之所以「順時針」轉動，是源自其前身[日晷](../Page/日晷.md "wikilink")。時針首先出現於[北半球](../Page/北半球.md "wikilink")，其作用與日晷投射在地上的「影子指針」相似。[太陽由東邊升起西邊落下](../Page/太陽.md "wikilink")，它投射在日晷上的影子之運行方向相反，是由西向東，所以日晷上代表時間的數字排列也是如此，現代[時鐘錶面數字沿用了這種排列方式](../Page/時鐘.md "wikilink")。

現代，有部分時鐘以[逆時針方向運行以作創新](../Page/逆時針方向.md "wikilink")。但歷史上，曾有[猶太人將時鐘做成](../Page/猶太人.md "wikilink")[逆時針運行](../Page/逆時針.md "wikilink")，以配合[希伯來文由右至左閱讀的傳統](../Page/希伯來文.md "wikilink")，如[歐洲部分](../Page/歐洲.md "wikilink")[猶太教會堂的時鐘](../Page/猶太教會堂.md "wikilink")
\[1\]。

## 用途

一般[螺絲](../Page/螺絲.md "wikilink")、容器的蓋都是以[逆時針方向鬆開](../Page/逆時針方向.md "wikilink")、**順時針方向**上緊為原則，符合[右手定則](../Page/右手定則.md "wikilink")。西方有短句「*righty-tighty,
lefty-loosey*」（右緊左鬆）以[助憶](../Page/助憶.md "wikilink")。 （R H Daniel,
1980）但這只適用於由上而下觀察旋轉的情況。另一記憶方法是利用[拇指](../Page/拇指.md "wikilink")；豎起[右手](../Page/右手.md "wikilink")[拇指](../Page/拇指.md "wikilink")，其餘的[手指之捲曲方向代表扭鬆的方向](../Page/手指.md "wikilink")，[左手則代表扭緊](../Page/左手.md "wikilink")。

螺絲之所以以順時針方向上緊，其中一個原因是用右手的人是以手的[旋後肌將螺絲以](../Page/旋後肌.md "wikilink")**順時針旋轉**，而一般來說旋後肌是較[旋前肌強壯](../Page/旋前肌.md "wikilink")\[2\]。

[西藏寺廟的](../Page/西藏.md "wikilink")[轉經筒是以順時針方向轉動的](../Page/轉經筒.md "wikilink")。

## 心理

大部分[左撇子以順時針的方向畫圓圈和繞過建築物](../Page/左撇子.md "wikilink")，研究認為這與哪邊大腦主導有關。\[3\]

## 參考

## 參閱

  - [右手定則](../Page/右手定則.md "wikilink")
  - [利手](../Page/利手.md "wikilink")
  - [光學異構物](../Page/光學異構物.md "wikilink")
  - [相对方位](../Page/相对方位.md "wikilink")
  - [逆時針方向](../Page/逆時針方向.md "wikilink")

[ja:CCW](../Page/ja:CCW.md "wikilink")

[Category:基本物理概念](../Category/基本物理概念.md "wikilink")
[Category:方位](../Category/方位.md "wikilink")

1.  <http://www.scrapbookpages.com/CzechRepublic/Prague/Josefov/JosefovHistory.html>.
2.
3.  Theodore H. Blau, *The torque test: A measurement of cerebral
    dominance.* 1974, American Psychological Association.