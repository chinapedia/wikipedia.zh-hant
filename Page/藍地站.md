**藍地站**（英文：**Lam Tei
Stop**）是[香港輕鐵的車站之一](../Page/香港輕鐵.md "wikilink")，代號350，屬單程車票[第3收費區](../Page/輕鐵第3收費區.md "wikilink")，共有2個月台。藍地站位於[青山公路（藍地段）旁近](../Page/青山公路#藍地段.md "wikilink")[藍地](../Page/藍地.md "wikilink")。

## 車站結構

### 車站樓層

<table>
<tbody>
<tr class="odd">
<td><p>-</p></td>
<td><p>行人天橋</p></td>
<td><p>往返<a href="../Page/藍地.md" title="wikilink">藍地</a></p></td>
</tr>
<tr class="even">
<td><p><strong>地面</strong></p></td>
<td></td>
<td><p><a href="../Page/詠柏苑.md" title="wikilink">詠柏苑</a></p></td>
</tr>
<tr class="odd">
<td><p>月台</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p> 往<a href="../Page/天逸站.md" title="wikilink">天逸</a>（<a href="../Page/泥圍站.md" title="wikilink">泥圍</a>）|}}</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p> 往<a href="../Page/屯門碼頭站.md" title="wikilink">屯門碼頭</a> {{!}}  往<a href="../Page/友愛站.md" title="wikilink">友愛</a>（<a href="../Page/兆康站_(輕鐵).md" title="wikilink">兆康</a>）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>月台</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/青山公路.md" title="wikilink">青山公路</a>－藍地段</p></td>
<td></td>
</tr>
</tbody>
</table>

### 車站周邊

  - [藍地](../Page/藍地.md "wikilink")
  - [藍地菜站](../Page/藍地菜站.md "wikilink")
  - [妙法寺](../Page/妙法寺.md "wikilink")
  - [新慶村](../Page/新慶村.md "wikilink")
  - [屯子圍](../Page/屯子圍.md "wikilink")
  - [富澤花園](../Page/富澤花園.md "wikilink")
  - [詠柏苑](../Page/詠柏苑.md "wikilink")
  - [綠怡居](../Page/綠怡居.md "wikilink")
  - [豫豐花園](../Page/豫豐花園.md "wikilink")

## 鄰接車站

## 接駁交通

<div class="NavFrame collapsed" style="background-color: #FFFF00;clear: no; border: 1px solid #999; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FF00FF; margin: 0 auto; padding: 0 10px; text-align: center; font-weight: bold;">

接駁交通列表

</div>

<div class="NavContent" style="margin: 0 auto; padding: 0 10px;">

  - [巴士](../Page/香港巴士.md "wikilink")

</div>

</div>

  - 紅色公共小巴，來往元朗至佐敦道

## 外部連結

  - [港鐵公司 - 輕鐵及巴士服務 -
    輕鐵街道圖](http://www.mtr.com.hk/chi/lr_bus/stmap_index.html)
  - [港鐵公司 - 輕鐵及巴士服務 -
    輕鐵行車時間表](http://www.mtr.com.hk/chi/lr_bus/schedule/schedule_index.html)

[Category:藍地](../Category/藍地.md "wikilink")
[Category:1988年启用的铁路车站](../Category/1988年启用的铁路车站.md "wikilink")
[Category:屯門區鐵路車站](../Category/屯門區鐵路車站.md "wikilink")