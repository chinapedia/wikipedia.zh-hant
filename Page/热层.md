[AtmosphereTerrestre_HD_zh.jpg](https://zh.wikipedia.org/wiki/File:AtmosphereTerrestre_HD_zh.jpg "fig:AtmosphereTerrestre_HD_zh.jpg")

**热层**（英文：****），亦稱**熱成層**、**熱氣層**或**增溫層**，是[地球](../Page/地球.md "wikilink")[大氣層的一層](../Page/大氣層.md "wikilink")。它位於[中间层之上及](../Page/中间层.md "wikilink")[散逸層之下](../Page/散逸層.md "wikilink")，其顶部離地面约800km。热层的空气受[太阳短波辐射而处于高度电离的状态](../Page/太阳辐射.md "wikilink")，[电离层便存在于在本层之中](../Page/电离层.md "wikilink")，而[極光也是在热层顶部发生的](../Page/極光.md "wikilink")\[1\]。热层的空气密度极小，[俄羅斯已退役的](../Page/俄羅斯.md "wikilink")[和平號太空站和现役的](../Page/和平號太空站.md "wikilink")[國際太空站均在距离地表](../Page/國際太空站.md "wikilink")320至380km的热层顶部运行。

## 词源

英文裡热层一字「Thermosphere」是從[希臘語θερμός](../Page/希臘語.md "wikilink")（[拉丁化](../Page/拉丁化.md "wikilink")=thermos，意思『熱』）一字引申而來。

## 温度变化

热层氣溫主要依靠太陽活動來決定，這是因為[氧](../Page/氧.md "wikilink")[原子強烈吸收波長小於](../Page/原子.md "wikilink")0.175[微米的太陽短波辐射而升温的缘故](../Page/微米.md "wikilink")。热层的氣溫會因高度而迅速上升，有時甚至可以高達2,000[℃](../Page/攝氏.md "wikilink")。熱成層的上方是從距離地面500至1000公里開始的[散逸層](../Page/散逸層.md "wikilink")，那裡大氣層會與外太空接壤。縱然少數的空氣粒子甚至可以於白晝時間到達2,500℃（4500[℉](../Page/華氏.md "wikilink")）高溫，但因為分子之間的距離太疏，它不會令人感到任何溫暖，就連一台普通的溫度計也只會量度到零攝氏度以下。

## 大气密度

热层存在于離地表85公里以上的高空。在這樣的高度，剩餘的大氣氣體會根據分子量而分層。热层的空氣極為稀薄，本层[质量仅占](../Page/质量.md "wikilink")[大气总质量的](../Page/大气.md "wikilink")0.5%。在120公里高度以上的空间，空气[密度已小到](../Page/密度.md "wikilink")[声波难以传播的程度](../Page/声波.md "wikilink")，在270公里高度上，空气密度约为地面空气的百亿分之一，在300公里的高度上，空气密度只及地面密度的千亿分之一，再向上空气就更稀薄了。

## 電離層

熱層的大氣分子吸收了因太陽的短波輻射及磁場後其電子能量增加，當中一部份進行[電離](../Page/電離.md "wikilink")。這些電離過的離子與電子形成了[電離層](../Page/電離層.md "wikilink")。在熱成層的電離層，存在著E層（離地面100-120公里）、F1層（離地面170-230公里）、F2層（離地面200-500公里，夜間融合為F層，約離地面300-500公里）三層。而因季節變化更會出現[突發性E層](../Page/突發性E層.md "wikilink")（Es層，約離地面100公里）。電離層可以反射[無線電波](../Page/無線電波.md "wikilink")，因此它又被人類利用進行遠距離無線電通信。

## 极光现象

热层在高緯度地區因磁場而被加速的電子會順勢流入，与热层中的大氣分子衝突繼而受到[激發及](../Page/激發.md "wikilink")[電離](../Page/電離.md "wikilink")。當那些分子復回原來狀態的時候，就會產生發光現象。此稱為[極光](../Page/極光.md "wikilink")。

## 参考资料

<references/>

[Category:地球科学](../Category/地球科学.md "wikilink")
[Category:大气](../Category/大气.md "wikilink")

1.