**Team Desire - Discipline - Dedication**，也就是**Team
3D**或者**3D.NY**。隊伍名稱即取自渴望 (**Desire**)、紀律
(**Discipline**)以及奉獻 (**Dedication**)英文的第一個字母集結而成，也就是Team
3D，也因為隊伍位於美國紐約市，故稱3D.NY。3D.NY是一個著名的[美國](../Page/美國.md "wikilink")[絕對武力電子競技職業隊伍](../Page/絕對武力.md "wikilink")，目前包括有[絕對武力：次世代](../Page/絕對武力：次世代.md "wikilink")、[魔獸爭霸3以及](../Page/魔獸爭霸3.md "wikilink")[生死格鬥4的隊伍](../Page/生死格鬥4.md "wikilink")，舊有的隊伍包括[絕對武力](../Page/絕對武力.md "wikilink")、[決勝時刻以及獵魔者](../Page/使命召喚.md "wikilink")。隊伍最初是由克雷格·雷賓於2002年創辦，不過目前則是由大衛·傑風管里，與Team
3D合作的贊助商包括[nVidia](../Page/nVidia.md "wikilink")、[Intel](../Page/Intel.md "wikilink")、[森海塞爾](../Page/森海塞爾.md "wikilink")、Xfire、PNY、SteelSeries、電子競技娛樂協會以及Velocity
Servers。
目前這支隊伍也是美國G7隊伍中的其中一支。

## 隊員

<table>
<caption><strong><a href="../Page/絕對武力.md" title="wikilink">絕對武力現役名單</a></strong></caption>
<thead>
<tr class="header">
<th></th>
<th><p>名字</p></th>
<th><p>遊戲名稱</p></th>
<th><p>加入日期</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>Kyle Miller</p></td>
<td><p><em>Ksharp</em></p></td>
<td><p>2002年4月9日</p></td>
<td><p>非現役時期：2006年3月30日－2007年1月13日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Jimmy Lin</p></td>
<td><p><em>LiN</em></p></td>
<td><p>2007年1月31日</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Salvatore Garozzo</p></td>
<td><p><em>Volcano</em></p></td>
<td><p>2004年9月6日</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>Mikey So</p></td>
<td><p><em>Method</em></p></td>
<td><p>2005年8月16日</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Andrew Rector</p></td>
<td><p><em>Rector</em></p></td>
<td><p>2007年1月13日</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

<table>
<caption><strong><a href="../Page/絕對武力.md" title="wikilink">絕對武力非現役名單</a></strong></caption>
<thead>
<tr class="header">
<th></th>
<th><p>名字</p></th>
<th><p>遊戲名稱</p></th>
<th><p>加入日期</p></th>
<th><p>釋出日期</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>Ron Kim</p></td>
<td><p><em>rambo</em></p></td>
<td><p>2002年4月9日</p></td>
<td><p>2007年1月12日</p></td>
<td><p>轉隊至 <strong><a href="../Page/Complexity.md" title="wikilink">Complexity</a></strong>[1]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Eric Stromberg</p></td>
<td><p><em>da_bears</em></p></td>
<td><p>2002年4月9日</p></td>
<td><p>2002年7月23日</p></td>
<td><p>轉隊至<strong>Chicago Chimera</strong></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Sean Morgan</p></td>
<td><p><em>Bullseye</em></p></td>
<td><p>2002年4月9日</p></td>
<td><p>2004年4月29日</p></td>
<td><p>轉隊至<strong><a href="../Page/Complexity.md" title="wikilink">Complexity</a></strong>[2]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Josh Sievers</p></td>
<td><p><em>Dominator</em></p></td>
<td><p>2005年5月19日</p></td>
<td><p>2007年1月13日</p></td>
<td><p>轉隊至<strong>Dallas Venom</strong></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Griffin Benger</p></td>
<td><p><em>shaGuar</em></p></td>
<td><p>2005年4月16日</p></td>
<td><p>2007年1月13日</p></td>
<td><p>轉隊至<strong>Evil Geniuses</strong></p></td>
</tr>
</tbody>
</table>

<table>
<caption><strong>其他遊戲現役名單</strong></caption>
<thead>
<tr class="header">
<th></th>
<th><p>遊戲</p></th>
<th><p>名字</p></th>
<th><p>遊戲名稱</p></th>
<th><p>加入日期</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p><a href="../Page/生死格鬥4.md" title="wikilink">生死格鬥4</a></p></td>
<td><p>Manny Rodriguez</p></td>
<td><p>Master</p></td>
<td></td>
<td><p>被Dallas Venom徵招參加冠軍遊戲系列賽</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/生死格鬥4.md" title="wikilink">生死格鬥4</a></p></td>
<td><p>Jared Reyes</p></td>
<td><p>Cali Jared</p></td>
<td><p>2007年6月12日</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/生死格鬥4.md" title="wikilink">生死格鬥4</a></p></td>
<td><p>Tanya Underwood</p></td>
<td><p>coolsvilla</p></td>
<td><p>2007年6月12日</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/世界街頭賽車.md" title="wikilink">世界街頭賽車</a></p></td>
<td><p>Chris Bjorkman</p></td>
<td><p>BadaXX</p></td>
<td><p>2007年6月12日</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/世界街頭賽車.md" title="wikilink">世界街頭賽車</a></p></td>
<td><p>Barry Brady</p></td>
<td><p>DoubleB</p></td>
<td><p>2007年6月12日</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/FIFA_2007.md" title="wikilink">FIFA 2007</a></p></td>
<td><p>Matt Wood</p></td>
<td><p>Wizakor</p></td>
<td><p>2007年6月12日</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 得獎事蹟

| 現役排名 | 最高排名 |
| ---- | ---- |
| 第11名 | 第5名  |

**Zonerank排名**

| 年度    | 賽事                                   | 排名 |
| ----- | ------------------------------------ | -- |
| 2002年 | *[CPL](../Page/CPL.md "wikilink")*   | 冠軍 |
| 2003年 | *Cyber X Games*                      | 冠軍 |
| 2003年 | *[CPL](../Page/CPL.md "wikilink")*   | 季軍 |
| 2004年 | *[WCG](../Page/WCG.md "wikilink")*   | 冠軍 |
| 2005年 | *[WCG](../Page/WCG.md "wikilink")*   | 冠軍 |
| 2006年 | *World Series of Video Games*        | 亞軍 |
| 2006年 | *[ESWC](../Page/ESWC.md "wikilink")* | 殿軍 |
|       |                                      |    |

**[絕對武力得獎事蹟](../Page/絕對武力.md "wikilink")**

| 年度    | 地點                                                                                | 賽事                                 | 排名 |
| ----- | --------------------------------------------------------------------------------- | ---------------------------------- | -- |
| 2005年 | [佛羅里達州](../Page/佛羅里達州.md "wikilink")[奧蘭多](../Page/奧蘭多_\(佛羅里達州\).md "wikilink")    | *美國遊戲大聯盟*                          | 冠軍 |
| 2005年 | [密蘇里州](../Page/密蘇里州.md "wikilink")[聖路易](../Page/聖路易.md "wikilink")                | *美國遊戲大聯盟*                          | 冠軍 |
| 2005年 | [賓夕法尼亞州](../Page/賓夕法尼亞州.md "wikilink")[費城](../Page/費城.md "wikilink")              | *美國遊戲大聯盟*                          | 亞軍 |
| 2005年 | [加利福尼亞州](../Page/加利福尼亞州.md "wikilink")[洛杉磯](../Page/洛杉磯_\(加利福尼亞州\).md "wikilink") | *美國遊戲大聯盟*                          | 冠軍 |
| 2005年 | [喬治亞州](../Page/喬治亞州.md "wikilink")[亞特蘭大](../Page/亞特蘭大_\(喬治亞州\).md "wikilink")     | *美國遊戲大聯盟*                          | 冠軍 |
| 2005年 | [伊利諾州](../Page/伊利諾州.md "wikilink")[芝加哥](../Page/芝加哥.md "wikilink")                | *美國遊戲大聯盟*                          | 冠軍 |
| 2005年 | [紐約市](../Page/紐約市.md "wikilink")                                                  | *美國遊戲大聯盟*決賽                        | 冠軍 |
| 2005年 |                                                                                   | *[CPL](../Page/CPL.md "wikilink")* | 冠軍 |
| 2005年 | [新加坡](../Page/新加坡.md "wikilink")[新達城](../Page/新達城廣場.md "wikilink")                | *[WCG](../Page/WCG.md "wikilink")* | 冠軍 |
|       |                                                                                   |                                    |    |

**[最後一戰2得獎事蹟](../Page/最後一戰2.md "wikilink")**

| 年度    | 賽事                            | 排名 |
| ----- | ----------------------------- | -- |
| 2006年 | *World Series of Video Games* | 殿軍 |
|       |                               |    |

**[魔獸爭霸3得獎事蹟](../Page/魔獸爭霸3.md "wikilink")**

| 年度    | 賽事                                 | 排名 |
| ----- | ---------------------------------- | -- |
| 2002年 | *[CPL](../Page/CPL.md "wikilink")* | 冠軍 |
|       |                                    |    |

**[生死格鬥4得獎事蹟](../Page/生死格鬥4.md "wikilink")**

| 年度    | 賽事                                 | 排名 |
| ----- | ---------------------------------- | -- |
| 2004年 | *[CPL](../Page/CPL.md "wikilink")* | 亞軍 |
|       |                                    |    |

**[決勝時刻得獎事蹟](../Page/使命召喚.md "wikilink")**

## 連結

  - [Team 3D官方網站](https://web.archive.org/web/20020802140715/http://www.team3d.net/)

## 參見

<div class="references-small">

<references />

</div>

[Category:美国电子竞技团队](../Category/美国电子竞技团队.md "wikilink")
[Category:2002年紐約州建立](../Category/2002年紐約州建立.md "wikilink")

1.
2.