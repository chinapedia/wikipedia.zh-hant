《**新九品芝麻官**》，香港剧名《**栋笃状王**》，是一部2005年首播的中国大陆[古装公堂](../Page/古装剧.md "wikilink")[喜剧](../Page/喜剧.md "wikilink")。该剧由浙江华新影视有限责任公司制作，香港导演[温伟基](../Page/温伟基.md "wikilink")、[邓伟恩联合执导](../Page/邓伟恩.md "wikilink")，并由[黄子华](../Page/黄子华.md "wikilink")、[张默](../Page/张默.md "wikilink")、[黄圣依](../Page/黄圣依.md "wikilink")、[苏永康](../Page/苏永康.md "wikilink")、[罗家英](../Page/罗家英.md "wikilink")、[韩雨芹等领衔主演](../Page/韩雨芹.md "wikilink")。剧名虽借用自1994年上映的[周星驰知名电影](../Page/周星驰.md "wikilink")《[九品芝麻官](../Page/九品芝麻官.md "wikilink")》，但仅背景相同，情节方面则毫无关联。全剧以[清朝](../Page/清朝.md "wikilink")[同治年间为背景](../Page/同治.md "wikilink")，讲述了男主人公立志除暴安良、为民伸冤，在成为广东一名九品知县以后，智斗[广东四大状师](../Page/广东四大状师.md "wikilink")，并周旋于官场之间，最终在四大状师之一[宋世杰的帮助下](../Page/宋士杰.md "wikilink")，成功揭发当朝大太监的罪证，赢得圣上垂青。

本剧于2004年9月在浙江[横店影视城开机拍摄](../Page/横店影视城.md "wikilink")\[1\]，同年12月初杀青关机\[2\]。2005年9月26日，本剧在[广东电视台珠江频道全国首播](../Page/广东电视台珠江频道.md "wikilink")\[3\]\[4\]，2007年3月28日在[江西卫视](../Page/江西卫视.md "wikilink")[黄金档](../Page/黄金时段.md "wikilink")[上星播出](../Page/中国省级卫星电视频道.md "wikilink")\[5\]（不过由于不明原因，该剧于4月5日遭到撤播）。香港则于2006年2月27日在[翡翠台首播该剧](../Page/翡翠台.md "wikilink")。

## 演員

**括弧內爲香港版配音員**

  - **[黃子華](../Page/黃子華.md "wikilink")** 飾
    **[宋世傑](../Page/宋世傑.md "wikilink")**
  - **[張　默](../Page/張默.md "wikilink")** 飾
    **陸小鳳**（[陳欣](../Page/陳欣_\(配音員\).md "wikilink")）
  - **[黃聖依](../Page/黃聖依.md "wikilink")** 飾
    **展隨風**（[曾秀清](../Page/曾秀清.md "wikilink")）
  - **[蘇永康](../Page/蘇永康.md "wikilink")** 飾 **公孫安份**
  - **[羅家英](../Page/羅家英.md "wikilink")** 飾 **陸一儒**
  - **[韓雨芹](../Page/韓雨芹.md "wikilink")** 飾
    **宋菲菲**（[鄭麗麗](../Page/鄭麗麗.md "wikilink")）
  - **[黃一飛](../Page/黃一飛.md "wikilink")** 飾 **段大人**
  - [牟鳳彬](../Page/牟鳳彬.md "wikilink") 飾
    [陳夢吉](../Page/陳夢吉.md "wikilink")（[潘文柏](../Page/潘文柏.md "wikilink")）
  - [鍾　亮](../Page/鍾亮.md "wikilink") 飾 [方唐鏡](../Page/方唐鏡.md "wikilink")
  - [Tae](../Page/Tae.md "wikilink") 飾
    劉華東（[蘇強文](../Page/蘇強文.md "wikilink")）
  - [楊　昇](../Page/楊昇.md "wikilink") 飾
    駱山（[張炳強](../Page/張炳強.md "wikilink")）
  - [潘　結](../Page/潘結.md "wikilink") 飾
    李鬢（[袁淑珍](../Page/袁淑珍.md "wikilink")）
  - [馬宗德](../Page/馬宗德.md "wikilink") 飾
    洪立本（[林保全](../Page/林保全.md "wikilink")）
  - [呂　行](../Page/呂行.md "wikilink") 飾 皇帝（[雷霆](../Page/雷霆.md "wikilink")）
  - [鮑逸琳](../Page/鮑逸琳.md "wikilink") 飾
    巧兒（[朱妙蘭](../Page/朱妙蘭.md "wikilink")）
  - [李建義](../Page/李建義.md "wikilink") 飾
    張玉奇（[梁志達](../Page/梁志達.md "wikilink")）
  - [李丹霞](../Page/李丹霞.md "wikilink") 飾
    自在（[林雅婷](../Page/林雅婷.md "wikilink")）
  - [張韾文](../Page/張韾文.md "wikilink") 飾
    飛花（[張頌欣](../Page/張頌欣.md "wikilink")）

## 其他地區播放

### 香港

無綫電視翡翠台從2006年2月27日至4月7日逢週一至週五晚上22:05至23:05播出。

## 歌曲

### 主題曲(香港)

《知彼不知己》
主唱：[黃子華](../Page/黃子華.md "wikilink")
作曲：[鄧志偉](../Page/鄧志偉.md "wikilink")、[譚天樂](../Page/:en:譚天樂.md "wikilink")
作詞：[陳詩慧](../Page/陳詩慧.md "wikilink")、[黃子華](../Page/黃子華.md "wikilink")

### 片尾曲(內地)

《我願等》
主唱：[蘇永康](../Page/蘇永康.md "wikilink")
作曲：[林慕德](../Page/林慕德.md "wikilink")
作詞：[姚謙](../Page/姚謙.md "wikilink")

## 收视

在[香港](../Page/香港.md "wikilink")[無綫電視](../Page/無綫電視.md "wikilink")[翡翠台播出時的收視](../Page/翡翠台.md "wikilink")：

| 週次 | 日期               | 香港平均收視                           | 广州AGB省网 | 广州AGB市网 | 广州AGB合计 |
| -- | ---------------- | -------------------------------- | ------- | ------- | ------- |
| 1  | 2006年2月27日-3月3日  | 24[點](../Page/收視點.md "wikilink") |         |         |         |
| 2  | 2006年3月6日-3月10日  | 22[點](../Page/收視點.md "wikilink") | 3.6     | 4.2     | 7.8     |
| 3  | 2006年3月13日-3月17日 | 20[點](../Page/收視點.md "wikilink") |         | 4.6     |         |
| 4  | 2006年3月20日-3月24日 | 21[點](../Page/收視點.md "wikilink") |         | 4.4     |         |
| 5  | 2006年3月27日-3月31日 | 20[點](../Page/收視點.md "wikilink") | 3.5     | 4.5     | 8       |
| 6  | 2006年4月3日-4月7日   | 20[點](../Page/收視點.md "wikilink") | 3.7     | 4.4     | 8.1     |

## 參考文獻

## 外部鍵接

  - [無綫電視官方網頁 -
    棟篤狀王](https://web.archive.org/web/20060408195614/http://tvcity.tvb.com/drama/hail_the_judge/)

## 作品的變遷

[Category:2005年中國電視劇集](../Category/2005年中國電視劇集.md "wikilink")
[Category:無綫電視外購劇集](../Category/無綫電視外購劇集.md "wikilink")
[Category:清朝晚期背景電視劇](../Category/清朝晚期背景電視劇.md "wikilink")
[Category:華策影視製作電視劇](../Category/華策影視製作電視劇.md "wikilink")

1.
2.
3.
4.
5.