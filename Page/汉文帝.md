**漢文帝劉恆**（），[劉邦第四子](../Page/劉邦.md "wikilink")，母[薄姬](../Page/薄姬.md "wikilink")，[漢惠帝之庶弟](../Page/漢惠帝.md "wikilink")。[西漢第五位](../Page/西漢.md "wikilink")[皇帝](../Page/皇帝.md "wikilink")（前180年11月14日—前157年7月6日在位），在位23年，享年47歲，其正式諡號為「孝文皇帝」，後世省略「孝」字稱「漢文帝」。葬於**[霸陵](../Page/漢霸陵.md "wikilink")**（在今[陝西省](../Page/陝西省.md "wikilink")[西安市](../Page/西安市.md "wikilink")[灞桥区](../Page/灞桥区.md "wikilink")[白鹿原东北角](../Page/白鹿原_\(地名\).md "wikilink")）。其[廟號](../Page/廟號.md "wikilink")**[太宗](../Page/太宗.md "wikilink")**，[謚號孝](../Page/謚號.md "wikilink")**文皇帝**。也是《[二十四孝](../Page/二十四孝.md "wikilink")》中親嘗湯藥的主角。

## 生平

漢王四年（前203年）漢王[劉邦於](../Page/劉邦.md "wikilink")[成臯召幸](../Page/成臯.md "wikilink")[薄夫人](../Page/薄夫人.md "wikilink")，有身孕，当年就生下文帝，高祖十一年春（前196年）年八岁封為**代王**，其為人寬容平和，在政治上保持低調。

[呂-{后}-在殺害](../Page/呂雉.md "wikilink")[劉邦愛姬](../Page/劉邦.md "wikilink")[戚夫人和其子趙王](../Page/戚夫人.md "wikilink")[劉如意後](../Page/劉如意.md "wikilink")，提議代王劉恆改封[趙王](../Page/趙王.md "wikilink")，然而劉恆巧妙地謙讓了，故而才能夠在呂-{后}-專權時期得以保命。

呂后專權，大封呂姓子弟為[異姓王](../Page/異姓王.md "wikilink")，[呂產](../Page/呂產.md "wikilink")、[呂祿等呂姓外戚強勢](../Page/呂祿.md "wikilink")，高祖长孙齐王[刘襄实力强大](../Page/刘襄_\(齐王\).md "wikilink")，率先起兵；[陳平](../Page/陈平_\(汉朝\).md "wikilink")、[周勃等](../Page/周勃.md "wikilink")[元老](../Page/元老.md "wikilink")、[列侯和](../Page/列侯.md "wikilink")[宗室](../Page/宗室.md "wikilink")[劉章等人以計謀騙來](../Page/刘章_\(城阳王\).md "wikilink")[呂氏](../Page/呂.md "wikilink")[外戚的兵权](../Page/外戚.md "wikilink")，[夷滅諸呂](../Page/夷滅諸呂.md "wikilink")，也打算把呂氏血緣掃清，要廢除[漢後少帝](../Page/漢後少帝.md "wikilink")，擁立新[皇帝](../Page/皇帝.md "wikilink")。

琅琊王[刘泽等指刘襄的舅舅驷钧为恶人](../Page/刘泽_\(燕王\).md "wikilink")，立刘襄等于复立一个吕氏。因為劉恆時年二十四歲，是劉邦當時現存年紀最長之子，又寬厚孝順，而且劉恆之母薄氏的家族比較不強勢。\[1\]陳平、周勃等大臣隨即請劉恆入長安即位，劉恆遍問代國眾臣意見，[郎中令](../Page/郎中令.md "wikilink")[張武認為此事有詐](../Page/張武.md "wikilink")，[中尉](../Page/中尉_\(古代官制\).md "wikilink")[宋昌卻覺得此為天賜良機](../Page/宋昌.md "wikilink")，劉恆只好與母親[薄姬商議此事](../Page/薄姬.md "wikilink")，薄姬也不知如何是好，遣其弟[薄昭前往](../Page/薄昭.md "wikilink")[長安與朝廷大臣們協商](../Page/長安.md "wikilink")，在九月的最後一日，劉恆奔赴[長安](../Page/長安.md "wikilink")，群臣三呼萬歲，繼天子位，[太僕](../Page/太僕.md "wikilink")[夏侯嬰親自拘捕漢後少帝](../Page/夏侯嬰.md "wikilink")[劉弘](../Page/劉弘.md "wikilink")，迎接劉恆。大臣們宣稱劉弘、[梁王](../Page/梁王.md "wikilink")[劉泰](../Page/劉泰.md "wikilink")、淮阳王[刘武](../Page/劉武_\(淮陽王\).md "wikilink")、恆山王[刘朝等非惠帝子](../Page/刘朝.md "wikilink")，废黜刘弘并将四人杀害。

漢文帝[即位後](../Page/即位.md "wikilink")，勵精圖治，興修[水利](../Page/水利.md "wikilink")，衣著樸素，廢除[肉刑](../Page/肉刑.md "wikilink")，使[漢朝進入強盛安定的時期](../Page/漢朝.md "wikilink")。當時百姓富裕，天下小康。漢文帝與其子[漢景帝統治時期被合稱為](../Page/漢景帝.md "wikilink")[文景之治](../Page/文景之治.md "wikilink")。

汉文帝十年，汉文帝舅[薄昭因故杀死了朝廷使者](../Page/薄昭.md "wikilink")，被赐自尽。

前157年7月6日（[六月己亥](../Page/六月.md "wikilink")），漢文帝崩於[長安](../Page/長安.md "wikilink")[未央宮](../Page/未央宮.md "wikilink")，死後葬[霸陵](../Page/漢霸陵.md "wikilink")。

## 評價

[Refusing_the_Seat_-_Anonymous_painter_during_the_Song_dynasty.jpg](https://zh.wikipedia.org/wiki/File:Refusing_the_Seat_-_Anonymous_painter_during_the_Song_dynasty.jpg "fig:Refusing_the_Seat_-_Anonymous_painter_during_the_Song_dynasty.jpg")
漢文帝與其子[漢景帝統治時期合稱為](../Page/漢景帝.md "wikilink")[文景之治](../Page/文景之治.md "wikilink")，奉為賢明[帝王的典範](../Page/帝王.md "wikilink")。此外，漢文帝在位時，存在[諸侯](../Page/諸侯.md "wikilink")[王國勢力過大及](../Page/王國.md "wikilink")[匈奴入侵](../Page/匈奴.md "wikilink")[中原等問題](../Page/中原.md "wikilink")。汉文帝对待这些问题采取的是异常谨慎而且又有效的手法。对待诸侯王，文帝采取以德服人的态度，小错不纠，在中央弱势的时候成功的安抚住了各地蠢蠢欲动的诸侯，为后来景帝处理[七国之乱造就了一批忠心耿耿的诸侯王和大臣](../Page/七国之乱.md "wikilink")。最重要的两个大动作是：安抚吴王，使得吴王在最年富力强的时候没有假借丧子之仇反叛；在齐王死后将齐国一分为七，既满足了齐王的儿子们称王的需求，为自己赢得了贤德之名，又消除了最大的一个诸侯国齐国。假如文帝的谨慎稳重的做法被一直持续下去，汉朝也就不会发生后来的七国之乱，诸侯王问题亦有希望能夠和平解决。

文帝與其妻子竇皇后，其子景帝都愛好[黃老之術](../Page/黃老之術.md "wikilink")，假托黃帝老子思想，以道家的清靜無為為治世方法，文景兩世與民休息，輕刑罰減賦帑，文帝本身亦恭行仁孝，生活質樸簡約，是[中國歷史上少有的賢君之一](../Page/中國歷史.md "wikilink")。

西漢末年的儒家学者[劉向曾經回答](../Page/劉向.md "wikilink")[成帝詢問](../Page/漢成帝.md "wikilink")，評價文帝“本修黄、老之言，不甚好儒术，其治尚清净无为，以故礼乐庠序未修，民俗未能大化，苟温饱完给，所谓治安之国也”、“（訟獄）治理不能過中宗（[漢宣帝](../Page/漢宣帝.md "wikilink")）之世”、“似不及中宗之世，不可以為昇平”，但认为文帝能礼待劝谏者，“文帝礼言事者，不伤其意，群臣无小大，至卽便从容言，上止辇听之，其言可者称善，不可者喜笑而已”\[2\]。

道德方面，文帝亦曾經親自為母親[薄氏嘗藥](../Page/薄姬.md "wikilink")，深具孝心，在《[二十四孝](../Page/二十四孝.md "wikilink")》裡被排在第二位。

## 家族

<center>

</center>

### 兄弟

  - 大哥 齊悼惠王[劉肥](../Page/劉肥.md "wikilink")
  - 二哥 [漢惠帝劉盈](../Page/漢惠帝.md "wikilink")
  - 三哥 趙隱王[劉如意](../Page/劉如意.md "wikilink")
  - 五弟 趙共王[劉恢](../Page/劉恢.md "wikilink")
  - 六弟 趙幽王[劉友](../Page/劉友_\(趙王\).md "wikilink")
  - 七弟 淮南厲王[劉長](../Page/刘长_\(淮南王\).md "wikilink")
  - 八弟 燕靈王[劉建](../Page/刘建_\(燕王\).md "wikilink")

### 后妃

  - [代王王后](../Page/代王王后.md "wikilink")，刘恒为代王时的[王后](../Page/王后.md "wikilink")，早逝，所生四子皆早夭。
  - [皇后窦氏](../Page/窦皇后_\(汉文帝\).md "wikilink")，生[刘嫖](../Page/刘嫖.md "wikilink")、[刘启](../Page/汉景帝.md "wikilink")、[刘武](../Page/劉武_\(梁王\).md "wikilink")
  - [慎夫人](../Page/慎夫人.md "wikilink")，有宠无子
  - 尹姬，文帝宠幸邯郸慎夫人和尹姬，有宠无子\[3\]

### 皇子

  - 嫡長子[劉啟](../Page/汉景帝.md "wikilink")，漢景帝
  - 嫡次子[劉武](../Page/劉武_\(漢文帝子\).md "wikilink")，梁孝王
  - 三子[劉參](../Page/劉參.md "wikilink")，代王
  - 四子[劉揖](../Page/劉揖.md "wikilink")，梁怀王

### 皇女

  - 長女[劉嫖](../Page/劉嫖.md "wikilink")，馆陶长公主
  - 刘氏，嫁[周勃之子](../Page/周勃.md "wikilink")[周勝之](../Page/周勝之.md "wikilink")，《史记\[4\]》仅称公主，元代《[文献通考](../Page/文献通考.md "wikilink")\[5\]》称昌平公主

## 纪年

| 汉太宗孝文皇帝                          | 元年                              | 二年                              | 三年                              | 四年                             | 五年                              | 六年                              | 七年                             | 八年                              | 九年                              | 十年                              |
| -------------------------------- | ------------------------------- | ------------------------------- | ------------------------------- | ------------------------------ | ------------------------------- | ------------------------------- | ------------------------------ | ------------------------------- | ------------------------------- | ------------------------------- |
| [公元](../Page/公元纪年.md "wikilink") | <small>前180年十月—前179年九月</small>  | <small>前179年十月—前178年后九月</small> | <small>前178年十月—前177年九月</small>  | <small>前177年十月—前176年九月</small> | <small>前176年十月—前175年后九月</small> | <small>前175年十月—前174年九月</small>  | <small>前174年十月—前173年九月</small> | <small>前173年十月—前172年后九月</small> | <small>前172年十月—前171年九月</small>  | <small>前171年十月—前170年后九月</small> |
| 汉太宗孝文皇帝                          | 十一年                             | 十二年                             | 十三年                             | 十四年                            | 十五年                             | 十六年                             | 后元年                            | 后二年                             | 后三年                             | 后四年                             |
| [公元](../Page/公元纪年.md "wikilink") | <small>前170年十月—前169年九月</small>  | <small>前169年十月—前168年九月</small>  | <small>前168年十月—前167年后九月</small> | <small>前167年十月—前166年九月</small> | <small>前166年十月—前165年九月</small>  | <small>前165年十月—前164年后九月</small> | <small>前164年十月—前163年九月</small> | <small>前163年十月—前162年九月</small>  | <small>前162年十月—前161年后九月</small> | <small>前161年十月—前160年九月</small>  |
| 汉太宗孝文皇帝                          | 后五年                             | 后六年                             | 后七年                             |                                |                                 |                                 |                                |                                 |                                 |                                 |
| [公元](../Page/公元纪年.md "wikilink") | <small>前160年十月—前159年后九月</small> | <small>前159年十月—前158年九月</small>  | <small>前158年十月—前157年九月</small>  |                                |                                 |                                 |                                |                                 |                                 |                                 |

## 軼事

漢文帝重用[鄧通](../Page/鄧通.md "wikilink")，“賞賜通巨萬以十數”，又將[蜀地的銅山賜給鄧通](../Page/蜀.md "wikilink")，准許他任意鑄[銅錢](../Page/銅錢.md "wikilink")，史稱「鄧氏錢布天下，其富如此」。因为邓通曾經在文帝在位时得罪过太子刘启，所以文帝驾崩之后，即位的汉景帝刘启罢免邓通，并且没收其全部家产，最后邓通因为穷困而饿死在蜀地（传说是雅安）。

## 影視形象

  - 1986年[香港電視劇](../Page/香港電視劇.md "wikilink")《[真命天子](../Page/真命天子.md "wikilink")》[廖啟智飾漢文帝劉恆](../Page/廖啟智.md "wikilink")
  - 2010年《[美人心計](../Page/美人心計.md "wikilink")》由[陳鍵鋒飾演漢文帝劉恆](../Page/陳鍵鋒.md "wikilink")
  - 2011年《[大風歌](../Page/大風歌.md "wikilink")》由[劉牧飾演漢文帝劉恆](../Page/劉牧.md "wikilink"),蘇彥錚飾劉恆（少年）,王亮飾劉恆（童年）

## 參考文獻

  -
  -
## 注釋

<div class="references-small">

<references />

</div>

|-  |-

[5](../Category/西漢皇帝.md "wikilink") [L劉](../Category/二十四孝.md "wikilink")
[4](../Category/汉高帝皇子.md "wikilink")
[汉](../Category/入祀历代帝王庙景德崇圣殿.md "wikilink")
[Category:劉姓](../Category/劉姓.md "wikilink")

1.  《[史記](../Page/史記.md "wikilink")·呂后本紀》：“大臣...乃曰：「代王，方今高帝見子最長，仁孝寬厚。太后家薄氏，謹良。且立長故順；以仁孝聞於天下，便。」乃相與共陰使人召代王。”
2.  [應劭](../Page/應劭.md "wikilink")，[風俗通義卷二](../Page/風俗通義.md "wikilink")
3.  《史记·卷四十九·外戚世家第十九》
    《[汉书](../Page/汉书.md "wikilink")·卷九十七上·外戚传第六十七上》
4.  《[史记](../Page/史记.md "wikilink")·卷五十七　绛侯周勃世家第二十七》
5.  《[文献通考](../Page/文献通考.md "wikilink")·卷二百五十八·帝系考九》：文帝二女　馆陶长公主（窦后生女。文帝即位为馆陶长公。师古曰："年最长，故谓长公主。"堂邑侯陈于尚之。见《窦后传》）、昌平公主（周勃太子胜之尚之。见《周勃传》）。