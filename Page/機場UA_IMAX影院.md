[Airport_UA_IMAX_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Airport_UA_IMAX_\(Hong_Kong\).jpg "fig:Airport_UA_IMAX_(Hong_Kong).jpg")
[AviationDiscoveryCentre_01.JPG](https://zh.wikipedia.org/wiki/File:AviationDiscoveryCentre_01.JPG "fig:AviationDiscoveryCentre_01.JPG")及4D超立體巨幕影館入口\]\]
[HKairport_t2_04.JPG](https://zh.wikipedia.org/wiki/File:HKairport_t2_04.JPG "fig:HKairport_t2_04.JPG")

**機場UA
IMAX影院**位於[香港國際機場二號客運大樓](../Page/香港國際機場.md "wikilink")[翔天廊](../Page/翔天廊.md "wikilink")，於2012年7月5日開業，是香港第三間IMAX戲院，設有全港最大的IMAX銀幕。前身為[洲立影藝經營的](../Page/洲立影藝.md "wikilink")**4D超立體巨幕影館**（4D
Extreme
Screen），於2007年3月30日開幕，2012年1月31日結業，2月起改由[娛藝接手經營](../Page/娛藝.md "wikilink")\[1\]。

## 影院設計

機場UA
IMAX影院設有高約13.89米、闊約22.42米的全港最大IMAX巨型銀幕，新影院繼承IMAX專利影院設計的特色，包括頂天立地巨幕、清晰明亮的畫面、多聲道數碼環繞音響系統及享譽全球的影院幾何設計，讓觀眾完全置身電影之中，感受最震撼逼真的視聽體驗。影院播放的電影包括IMAX
2D、3D電影、IMAX科教電影及非IMAX電影。

## 歷史

### 4D超立體巨幕影館時代

4D超立體巨幕影館（4D Extreme
Screen）由[香港機場管理局](../Page/香港機場管理局.md "wikilink")、[洲立影藝有限公司和影片技術供應商](../Page/洲立影藝.md "wikilink")[SimEx-Iwerks聯手創辦](../Page/SimEx-Iwerks.md "wikilink")，耗資約1,000萬[美元](../Page/美元.md "wikilink")。影館設有64[呎高](../Page/呎.md "wikilink")、44呎闊，即約5層樓高的巨型[銀幕](../Page/銀幕.md "wikilink")，開幕當時是全[亞洲最大的](../Page/亞洲.md "wikilink")4D銀幕。全院座位358個，採取劇院式階梯設計，避免觀眾視野被遮擋，配備先進的特技效果裝置和環迴[立體聲音響](../Page/立體聲.md "wikilink")，以特製的菲林和放映機播放影片，使畫面大出數倍，清晰度、立體感和層次感也有所增加。影館是全球具備最多種類官能[刺激效果的](../Page/刺激.md "wikilink")4D特效電影館，觀眾戴上特製眼鏡觀看4D特效電影時，除了可以在銀幕上欣賞立體影像，亦能夠感受視覺和聽覺以外與劇情同步發展的7種感官效果，包括[風吹](../Page/風.md "wikilink")、[雪飄](../Page/雪.md "wikilink")、[水濺](../Page/水.md "wikilink")、[泡沫](../Page/泡沫.md "wikilink")、[煙霧](../Page/煙霧.md "wikilink")、[閃光和](../Page/閃光.md "wikilink")[氣味](../Page/氣味.md "wikilink")。另有舞台設備、視聽系統及數碼投影器，同時具備可供舉行大型會議的配套設施。

  - 影片種類

影館能夠播放不同類型的電影，包括二維、三維及四維的35毫米及70毫米影片。每天播放約8至10場的三維電影或者四維電影，包括「高空飛行歷奇」、「外太空探索」以及「科幻歷險」等題材，曾播放電影包括《[羅拔神奇家族](../Page/羅拔神奇家族.md "wikilink")》及《[迷你魔界大冒險](../Page/迷你魔界大冒險.md "wikilink")》等。

  - 開幕儀式

影館於2007年3月30日正式開幕，當日由[香港旅遊發展局主席](../Page/香港旅遊發展局.md "wikilink")[周梁淑怡和](../Page/周梁淑怡.md "wikilink")[演員](../Page/演員.md "wikilink")[莫文蔚等嘉賓戴上特製眼鏡觀看立體電影](../Page/莫文蔚.md "wikilink")。

當日香港旅遊發展局主席周梁淑怡認為該影院是繼[香港迪士尼樂園和](../Page/香港迪士尼樂園.md "wikilink")[東涌](../Page/東涌.md "wikilink")[纜車](../Page/纜車.md "wikilink")[昂坪360後](../Page/昂坪360.md "wikilink")，另一個[大嶼山的旅遊新景點](../Page/大嶼山.md "wikilink")，相信能夠吸引更多市民及旅客來訪。香港國際機場機場地產業務總經理楊嘉倫認為，影館可吸引旅客在轉機和候機途中花上15分鐘入場。同時認為現時通往機場的交通十分方便，影館亦可吸引市民到翔天廊。

## 參見

  -
## 注釋

<references/>

## 參考資料

  - [UA院線官方網頁](https://web.archive.org/web/20131222023456/http://www.uacinemas.com.hk/chi/cinema/CinemaList?id=22)
  - [機場4D影院開幕
    體驗光影色香味](http://makescentstudio.blogspot.hk/2007/03/4d-4d-cinema.html)，《明報》，2007年3月31日
  - [嘉宾体验4D超立体巨幕电影](http://www.chinanews.com.cn/tp/hwcz/news/2007/03-30/904714.shtml)，中國新聞網，2007年3月30日
  - [4D超立體巨幕影館官方網頁](https://web.archive.org/web/20091231004828/http://www.mclcinema.com/mclCinemaInfo_AA_hk.aspx?visLang=1)

[Category:香港戲院](../Category/香港戲院.md "wikilink")
[Category:香港國際機場](../Category/香港國際機場.md "wikilink")

1.  [「UA將在機場營運IMAX影院」](http://hongkongfilms.mysinablog.com/index.php?op=ViewArticle&articleId=3436388)《講。鏟。片》，2012年2月12日