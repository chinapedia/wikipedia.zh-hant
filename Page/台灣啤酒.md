[金牌台灣啤酒_Gold_Medal_Taiwan_Beer.jpg](https://zh.wikipedia.org/wiki/File:金牌台灣啤酒_Gold_Medal_Taiwan_Beer.jpg "fig:金牌台灣啤酒_Gold_Medal_Taiwan_Beer.jpg")
[Taiwan_Beer_aluminium_cans_20071112.jpg](https://zh.wikipedia.org/wiki/File:Taiwan_Beer_aluminium_cans_20071112.jpg "fig:Taiwan_Beer_aluminium_cans_20071112.jpg")
**台灣啤酒**，簡稱**台啤**，是由[台灣菸酒股份有限公司所發售的](../Page/台灣菸酒股份有限公司.md "wikilink")[啤酒](../Page/啤酒.md "wikilink")[品牌](../Page/品牌.md "wikilink")，原名「高砂麥酒」，1945年改名為「台灣啤酒」，是[台灣最受歡迎的啤酒品牌之一](../Page/台灣.md "wikilink")。

## 歷史

[Beer_raw_material_in_Product_Extension_Center,_TTL_Jhunan_Brewery_20151017.jpg](https://zh.wikipedia.org/wiki/File:Beer_raw_material_in_Product_Extension_Center,_TTL_Jhunan_Brewery_20151017.jpg "fig:Beer_raw_material_in_Product_Extension_Center,_TTL_Jhunan_Brewery_20151017.jpg")
台啤最早是在[台灣日治時期](../Page/台灣日治時期.md "wikilink")1919年由[高砂麥酒株式會社首釀](../Page/高砂麥酒株式會社.md "wikilink")\[1\]，當時稱為「高砂生啤酒」（高砂生ビール）。[日本投降後](../Page/日本投降.md "wikilink")，中華民國政府接收台灣，由當時新成立的[台灣省菸酒公賣局接收並改為](../Page/台灣省菸酒公賣局.md "wikilink")「台北第二酒廠」，並將「高砂生啤酒」改名為「台灣啤酒」。1975年，台北第二酒廠更名為「[建國啤酒廠](../Page/建國啤酒廠.md "wikilink")」。

其配方最早來自德國技師，是一種[拉格啤酒](../Page/拉格啤酒.md "wikilink")，獨特的風味來自釀造過程添加的[蓬萊米](../Page/蓬萊米.md "wikilink")。飲用方式通常是冰過後搭配[台灣小吃](../Page/台灣小吃.md "wikilink")。此外台啤贏得過數個國際獎項，包括了1977年的the
International Monde Selection、2002年的the Brewing Industry International
Awards。

雖然積極於外銷，但是除了台灣和海外台灣人社群外，台啤的能見度不高。2000年後，因洋酒攻占市場，台啤形象又過於老氣，故進行一連串更新運動：例如旗下的[籃球隊命名為](../Page/籃球隊.md "wikilink")[台灣啤酒隊](../Page/台灣啤酒籃球隊.md "wikilink")，[電視廣告由年輕一輩的歌手如](../Page/電視廣告.md "wikilink")[張惠妹](../Page/張惠妹.md "wikilink")、[伍佰](../Page/伍佰.md "wikilink")、[蔡依林等代言與演唱](../Page/蔡依林.md "wikilink")[主題曲](../Page/主題曲.md "wikilink")。

## 啤酒廠

  - [台北啤酒工廠](../Page/台北啤酒工廠.md "wikilink")：台灣最早的啤酒廠，前身為[建國啤酒廠](../Page/建國啤酒廠.md "wikilink")。目前朝向「台北啤酒文化園區」規劃。
  - [苗栗](../Page/苗栗縣.md "wikilink")[竹南啤酒廠](../Page/竹南啤酒廠.md "wikilink")：面積、產能最大，管理台北啤酒工廠。專門生產台灣啤酒、台灣生啤酒。
  - [台中](../Page/臺中市.md "wikilink")[烏日啤酒廠](../Page/烏日啤酒廠.md "wikilink")：2003年，研發出金牌啤酒相關系列。每年地方皆舉辦烏日啤酒文化觀光節。
  - [台南](../Page/臺南市.md "wikilink")[善化啤酒廠](../Page/善化啤酒廠.md "wikilink")：銷售啤酒相關產品，常搭配台南特產清燙牛肉舉辦行銷活動。

## 衍生商品

[Gold_Medal_Taiwan_Beer_paper_boxes_20150206.jpg](https://zh.wikipedia.org/wiki/File:Gold_Medal_Taiwan_Beer_paper_boxes_20150206.jpg "fig:Gold_Medal_Taiwan_Beer_paper_boxes_20150206.jpg")

  - 經典台灣啤酒（台灣啤酒）
  - 金牌台灣啤酒
  - 18天台灣生啤酒
  - 台灣啤酒 M\!NE（全麥啤酒）
  - 台灣啤酒 黑M\!NE（黑麥啤酒）
  - 台灣啤酒果微醺 (荔枝、白葡萄、青蘋果)
  - 台灣啤酒水果系列 (鳳梨、芒果、柳丁、紅葡萄、青梅)
  - 台灣啤酒 蜂蜜啤酒 2015年4月上市
  - 台灣啤酒 小麥啤酒
  - 台灣啤酒 PREMIUM

## 代言人

  - [伍佰](../Page/伍佰.md "wikilink")
  - [六甲樂團](../Page/六甲樂團.md "wikilink")
  - [張惠妹](../Page/張惠妹.md "wikilink")
  - [楊培安](../Page/楊培安.md "wikilink")
  - [羅志祥](../Page/羅志祥.md "wikilink")
  - [蕭敬騰](../Page/蕭敬騰.md "wikilink")
  - [蘇打綠](../Page/蘇打綠.md "wikilink")
  - [蔡依林](../Page/蔡依林.md "wikilink")
  - [陳意涵](../Page/陳意涵.md "wikilink")
  - [陳柏霖](../Page/陳柏霖.md "wikilink")
  - [藍正龍](../Page/藍正龍.md "wikilink")
  - [宥勝](../Page/宥勝.md "wikilink")
  - [周渝民](../Page/周渝民.md "wikilink")
  - [林依晨](../Page/林依晨.md "wikilink")
  - [王力宏](../Page/王力宏.md "wikilink")
  - [趙又廷](../Page/趙又廷.md "wikilink")
  - [劉以豪](../Page/劉以豪.md "wikilink")
  - [五月天](../Page/五月天.md "wikilink")
  - [陳嘉樺](../Page/陳嘉樺.md "wikilink")
  - [韋禮安](../Page/韋禮安.md "wikilink")

## 參考來源

## 外部連結

  - [台灣啤酒](http://www.twbeer.com.tw/)

  - [經典台灣啤](https://web.archive.org/web/20120928041846/http://www.twbeer.com.tw/classic/)

  - [金牌台灣啤酒](https://web.archive.org/web/20120922081336/http://www.twbeer.com.tw/gold)

  - [18天台灣生啤酒](https://www.twbeer.com.tw/beer_Only18Days.html)

  - [台灣菸酒股份有限公司](http://www.ttl.com.tw/)

  -
[Category:啤酒品牌](../Category/啤酒品牌.md "wikilink")
[Category:台灣的啤酒](../Category/台灣的啤酒.md "wikilink")
[Category:台灣菸酒公司](../Category/台灣菸酒公司.md "wikilink")
[Category:台灣品牌](../Category/台灣品牌.md "wikilink")
[Category:台灣啤酒及酒廠](../Category/台灣啤酒及酒廠.md "wikilink")

1.