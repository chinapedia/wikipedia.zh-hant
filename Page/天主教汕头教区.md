**天主教汕头教区（）**是[罗马天主教在](../Page/罗马天主教.md "wikilink")[中国](../Page/中国.md "wikilink")[广东省东部设立的一个](../Page/广东省.md "wikilink")[教区](../Page/教区.md "wikilink")，屬[廣州總教區](../Page/天主教廣州總教區.md "wikilink")。

## 历史

1914年4月6日，广东代牧区分为[广州代牧区和潮州代牧区](../Page/天主教廣州總教區.md "wikilink")，均由法国[巴黎外方传教会负责](../Page/巴黎外方传教会.md "wikilink")。1915年8月18日，潮州代牧区更名为汕头代牧区。

1929年，划出大埔、梅县、兴宁、平远、蕉岭、五华6县，成立[嘉应教区](../Page/嘉应教区.md "wikilink")，归美国[玛利诺外方传教会管理](../Page/玛利诺外方传教会.md "wikilink")。1946年4月11日，汕头代牧区升为汕头教区。

## 教堂

  - [汕头圣若瑟主教座堂](../Page/若瑟堂_\(汕头\).md "wikilink")

## 主教

  - [实茂芳](../Page/实茂芳.md "wikilink")（Bishop Adolphe Rayssac,
    M.E.P.）1914年－1935年
  - [荷敬谦](../Page/荷敬谦.md "wikilink")（Bishop Charles Vogel,
    M.E.P.）1935年－1958年
  - [庄建坚](../Page/庄建坚.md "wikilink")（Bishop Peter Zhuang
    Jian-jian）2006年－2018年
  - [黃炳章](../Page/黃炳章.md "wikilink") 2018年－現在

## 信徒

1949年前后，汕头教区有外籍神父20人，华籍神父38人，教徒约30000人，其中市区教徒约2000人。至1987年，汕头教区有神职人员11人（主教1人，神父10人），教徒43312人，其中汕头市区2050人，潮州市1920人，揭西县10979人，揭阳县7656人，潮阳县7500人，普宁县4715人，惠来县4302人，饶平县2930人，澄海县1001人，南澳县259人\[1\]。

目前汕头教区有神职人员23人，主教1人，神父22人，修士4人，修女30人。全教区教友约13万人，教堂约150座。

## 参考文献

<div class="references-small">

<references />

</div>

{{-}}

[Category:中国天主教教区](../Category/中国天主教教区.md "wikilink")
[Category:汕头天主教](../Category/汕头天主教.md "wikilink")
[Category:1914年中國建立](../Category/1914年中國建立.md "wikilink")

1.  《汕头市志》2000版