**G-四聯體**（英語：**G-quadruplex**、**G-tetrads**或**G<sub>4</sub>-DNA**）是一種由富含[鳥嘌呤的核酸序列所構成的四股型態](../Page/鳥嘌呤.md "wikilink")。含有經由[胡斯坦(Hoogsteen)](../Page/胡斯坦碱基对.md "wikilink")[氫鍵維持穩定](../Page/氫鍵.md "wikilink")，並由鳥嘌呤以正方形方式排列而成的結構。在方形中央有一個[單價](../Page/單價.md "wikilink")[陽離子](../Page/陽離子.md "wikilink")（通常是[鉀](../Page/鉀.md "wikilink")）\[1\]。這種結構可見於[DNA](../Page/DNA.md "wikilink")、[RNA](../Page/RNA.md "wikilink")、[LNA](../Page/鎖核酸.md "wikilink")（鎖核酸）與[PNA](../Page/PNA.md "wikilink")（肽核酸）；根據形成四聯體的各股的方向，可能有[分子內](../Page/分子内作用力.md "wikilink")（intramolecular）、[雙分子](../Page/反应分子数.md "wikilink")（bimolecular）或四分子（tetramolecular）等不同類型\[2\]。

在四聯體的結構中，我們可以根據四股的方向，將結構分為[平行和](../Page/平行_\(幾何\).md "wikilink")。

[G-quadruplex.svg](https://zh.wikipedia.org/wiki/File:G-quadruplex.svg "fig:G-quadruplex.svg")
[Telomer-structure.gif](https://zh.wikipedia.org/wiki/File:Telomer-structure.gif "fig:Telomer-structure.gif")

## 註釋

## 參考文獻

  -
  -
  -
  -
  -
  -
  -
  -
  -
## 外部連結

  - [Nanopore and Aptamer Biosensor
    group](http://nanopore.weebly.com/){NAB group}

### 四聯體網站

  - [COST MP0802 Quadruplex
    Network](https://web.archive.org/web/20150619072314/http://www.g4net.org/)
    – the European network of G-quadruplex community: From first
    principles to applications
  - [Quadruplex.org](http://www.quadruplex.org/) – a website to serve
    the quadruplex community
  - [G-Quadruplex World](http://gquadruplex.wordpress.com/) – a website
    to discuss publications and other information of interest to those
    working in the field of G-quadruplexes
  - [Quadbase](http://www.quadruplex.org/?view=quadbase) – downloadable
    data on predicted G-quadruplexes
  - [Greglist](https://www.webcitation.org/6DYII2TVw?url=http://tubic.tju.edu.cn/greglist/)
    – a database listing potential G-quadruplex regulated genes
  - \[<http://quadbase.igib.res.in>: Database on Quadruplex information:
    QuadBase from IGIB\]
  - [GRSDB](http://bioinformatics.ramapo.edu/GRSDB2/)- a database of
    G-quadruplexes near RNA processing sites.
  - [GRS_UTRdb](http://bioinformatics.ramapo.edu/GRS_UTRDB/)- a
    database of G-quadruplexes in the UTRs.
  - [G-quadruplex Resource Site](http://bioinformatics.ramapo.edu/GQRS/)
  - [non-B Motif Search Tool at non-B
    DB](https://web.archive.org/web/20101230025145/http://nonb.abcc.ncifcrf.gov/apps/nBMST/default)-
    a web server to predict G-quadruplex forming motifs and other non-B
    DNA forming motifs from users' DNA sequences.

### 預測G-四聯體的工具

  - [Quadparser: Downloadable program for finding putative
    quadruplex-forming
    sequences](http://www.quadruplex.org/?view=quadparser_web) from
    Balasubramanian's group.
  - [QGRS Mapper: a web-based application for predicting G-quadruplexes
    in nucleotide sequences and NCBI
    genes](http://bioinformatics.ramapo.edu/QGRS/) from Bagga's group.
  - [Quadfinder: Tool for Prediction and Analysis of G Quadruplex Motifs
    in DNA/RNA Sequences from Maiti's group, IGIB, Delhi,
    India](http://202.54.26.221/quadfinder/)

[Category:DNA](../Category/DNA.md "wikilink")

1.
2.