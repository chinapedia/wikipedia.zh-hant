**Duce**（[意大利语](../Page/意大利语.md "wikilink")[IPA](../Page/国际音标.md "wikilink")：）是意大利语表示“领袖”或者“元首”意义的一个特殊名词，Duce一词源于[拉丁语](../Page/拉丁语.md "wikilink")*Dux*。并因[意大利](../Page/意大利.md "wikilink")[法西斯主义独裁者](../Page/法西斯主义.md "wikilink")[贝尼托·墨索里尼在](../Page/贝尼托·墨索里尼.md "wikilink")1925年至1945年间使用此称谓而著称。Duce
的淵源及迷思來自於法國大革命及18世紀理想主義和浪漫主義所提到的“特質（genius）”。

## 词源

“Duce”一词源于[拉丁语](../Page/拉丁语.md "wikilink")*Dux*，表示“将军”或对应的[军衔](../Page/军衔.md "wikilink")，Dux在[古罗马可指代军团](../Page/古罗马.md "wikilink")[统帅或](../Page/统帅.md "wikilink")[罗马执政官乃至](../Page/罗马执政官.md "wikilink")[罗马皇帝](../Page/罗马皇帝.md "wikilink")\[1\]\[2\]。

## 使用者

类似于[德语的](../Page/德语.md "wikilink")“[Führer](../Page/Führer.md "wikilink")”\[3\]，它同样也是一个头衔和[代名词](../Page/代名词.md "wikilink")。Duce的称号始用于19世纪，被用作[意大利统一运动的政治](../Page/意大利统一.md "wikilink")[宣传中](../Page/宣传.md "wikilink")，曾被用于[朱塞佩·加里波底与之后的](../Page/朱塞佩·加里波底.md "wikilink")[加布里埃尔·邓南遮](../Page/加布里埃尔·邓南遮.md "wikilink")。[第一次世界大战期间](../Page/第一次世界大战.md "wikilink")，“*Duce
Supremo*（最高统帅）”曾被用来指代[意大利王国](../Page/意大利王国.md "wikilink")[国王](../Page/意大利统治者列表.md "wikilink")[维托里奥·埃马努埃莱三世作为意大利最高军事统帅](../Page/维托里奥·埃马努埃莱三世.md "wikilink")。这个头衔此后的使用者，同时也是最为著名的使用者是[意大利王國首相](../Page/法西斯意大利.md "wikilink")[贝尼托·墨索里尼](../Page/贝尼托·墨索里尼.md "wikilink")。

[国家法西斯党成立后](../Page/国家法西斯党.md "wikilink")，Duce被明确引入党章的第一条：“国家法西斯党是一个执行*领袖*的命令、为法西斯国家服务的志愿民兵组织”\[4\]。由于“Führer”[中文一般被译为](../Page/中文.md "wikilink")“元首”，为避免混淆，“Duce”的中文译名通常为“领袖”。

## 同类称谓

“*Der Führer*”常用来直接指代[阿道夫·希特勒](../Page/阿道夫·希特勒.md "wikilink")，同样“*Il
Duce*”也会用来直接指代墨索里尼，这些特定称谓前通常会被加上一个[冠词](../Page/冠词.md "wikilink")。同类直接指代的称谓还有*[Caudillo](../Page/考迪羅.md "wikilink")*指代[西班牙](../Page/西班牙.md "wikilink")[独裁者](../Page/独裁者.md "wikilink")[弗朗西斯科·佛朗哥及部分前西班牙](../Page/弗朗西斯科·佛朗哥.md "wikilink")[殖民地国家的军事独裁者](../Page/殖民地.md "wikilink")；*[Poglavnik](../Page/Poglavnik.md "wikilink")*指代[克罗地亚法西斯组织](../Page/克罗地亚.md "wikilink")[乌斯塔沙及](../Page/乌斯塔沙.md "wikilink")[克罗地亚独立国领袖](../Page/克罗地亚独立国.md "wikilink")[安特·帕韦利奇](../Page/安特·帕韦利奇.md "wikilink")；*[Conducător](../Page/Conducător.md "wikilink")*指代[罗马尼亚的一些](../Page/罗马尼亚.md "wikilink")[寡头政治家](../Page/寡头政治.md "wikilink")，如[扬·安东内斯库和](../Page/扬·安东内斯库.md "wikilink")[尼古拉·齐奥塞斯库](../Page/尼古拉·齐奥塞斯库.md "wikilink")；*[主席](../Page/中国共产党中央委员会主席.md "wikilink")*指代[中国的](../Page/中国.md "wikilink")[毛泽东](../Page/毛泽东.md "wikilink")\[5\]及*[委员长](../Page/國民政府軍事委員會.md "wikilink")*指代[蒋中正](../Page/蒋中正.md "wikilink")\[6\]。

## 参考文献

<references />

  - [De Imperatoribus Romanis](http://www.roman-emperors.org/)

[Category:法西斯主义](../Category/法西斯主义.md "wikilink")
[Category:意大利语](../Category/意大利语.md "wikilink")
[Category:贝尼托·墨索里尼](../Category/贝尼托·墨索里尼.md "wikilink")
[Category:国家法西斯党](../Category/国家法西斯党.md "wikilink")

1.
2.
3.
4.
5.
6.