**中国共产党中央纪律检查委员会**，简称**中共中央纪委**、**中央纪委**或**中纪委**，是[中国共产党的最高](../Page/中国共产党.md "wikilink")[纪律检查机关](../Page/中国共产党纪律检查机关.md "wikilink")。中国共产党中央纪律检查委员会与[中华人民共和国国家监察委员会合署办公](../Page/中华人民共和国国家监察委员会.md "wikilink")。

中国共产党中央纪律检查委员会负责维护党的章程和其他党内法规，检查党的路线方针政策和决议执行情况，对党员领导干部行使权力进行监督，维护宪法法律，对公职人员依法履职、秉公用权、廉洁从政以及道德操守情况进行监督检查，对涉嫌职务违法和职务犯罪的行为进行调查并作出政务处分决定，对履行职责不力、失职失责的领导人员进行问责，负责组织协调党风廉政建设和反腐败宣传等。中央纪委实行[书记负责制](../Page/中国共产党中央纪律检查委员会书记.md "wikilink")。

## 历史

1927年5月，中共中央决定成立**[中国共产党中央监察委员会](../Page/中国共产党中央监察委员会.md "wikilink")**，1928年7月撤销。1949年11月，决定设立**中国共产党中央纪律检查委员会**，1955年再改称为**[中国共产党中央监察委员会](../Page/中国共产党中央监察委员会.md "wikilink")**，“[文化大革命](../Page/文化大革命.md "wikilink")”爆發後中国共产党纪律检查机关基本瘫痪，1978年12月[中共十一届三中全会后重设](../Page/中国共产党第十一届中央委员会第三次全体会议.md "wikilink")**中国共产党中央纪律检查委员会**。

中央纪律检查委员会重新组建以来，六任[中央纪律检查委员会书记中](../Page/中国共产党中央纪律检查委员会书记.md "wikilink")，有五位一直是以[中共中央政治局常委身份兼任](../Page/中国共产党中央政治局常务委员会.md "wikilink")。1978年《[中国共产党章程](../Page/中国共产党章程.md "wikilink")》中规定，中央纪律检查委员会设[第一书记一名](../Page/第一书记_\(中国\).md "wikilink")，而第一书记必须由中共中央政治局常委中产生，这是中共党章首次对第一书记职务设置有所规定。在1987年[中共十三大上通过](../Page/中国共产党第十三次全国代表大会.md "wikilink")《中国共产党章程部分条文修正案》，取消中央纪委第一书记须从中央政治局常委从产生一项。1992年，尉健行出任中纪委书记，但不是中央政治局常委，而是以中央政治局委员、[中央書記處書記兼任](../Page/中国共产党中央书记处.md "wikilink")。1997年，[中国共产党第十五次全国代表大会上](../Page/中国共产党第十五次全国代表大会.md "wikilink")，尉健行当选中央政治局常委、中纪委书记。自2002年[中共十六大起](../Page/中国共产党第十六次全国代表大会.md "wikilink")，中纪委书记均由中央政治局常委兼任。

1993年2月，根据[中国共产党中央委员会和](../Page/中国共产党中央委员会.md "wikilink")[中华人民共和国国务院的决定](../Page/中华人民共和国国务院.md "wikilink")，[中华人民共和国监察部与中共中央纪律检查委员会机关](../Page/中华人民共和国监察部.md "wikilink")[合署办公](../Page/合署办公.md "wikilink")，机构列入[国务院序列](../Page/国务院序列.md "wikilink")，编制列入[中共中央直属机构](../Page/中共中央直属机构.md "wikilink")。[国家预防腐败局列入](../Page/国家预防腐败局.md "wikilink")[国务院直属机构序列](../Page/国务院直属机构.md "wikilink")，在监察部加挂牌子。1993年起，地方各级行政监察机关与同级[中国共产党纪律检查机关](../Page/中国共产党纪律检查机关.md "wikilink")[合署办公](../Page/合署办公.md "wikilink")。

2018年3月，中共中央印发《[深化党和国家机构改革方案](../Page/深化党和国家机构改革.md "wikilink")》。方案指出，“组建国家监察委员会。将监察部、国家预防腐败局的职责，最高人民检察院查处贪污贿赂、失职渎职以及预防职务犯罪等反腐败相关职责整合，组建国家监察委员会，同中央纪律检查委员会[合署办公](../Page/合署办公.md "wikilink")，履行纪检、监察两项职责，实行一套工作机构、两个机关名称。”“国家监察委员会由全国人民代表大会产生，接受全国人民代表大会及其常务委员会的监督。”“不再保留监察部、国家预防腐败局\[1\]。”

## 职责

根据《》，中国共产党纪律检查机关承担下列职责和任务\[2\]：

## 机构设置

<onlyinclude> 根据《》，中央纪委国家监委内设机构、派出机构为副部级。中央纪委国家监委设置下列机构\[3\]：

### 内设机构

### 直属事业单位

### 直属企业单位

  - [中国方正出版社](../Page/中国方正出版社.md "wikilink")

### 派出机构

  - [中央纪律检查委员会国家监察委员会中央和国家机关纪检监察工作委员会](../Page/中央纪律检查委员会国家监察委员会中央和国家机关纪检监察工作委员会.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻国务院办公厅纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻国务院办公厅纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻外交部纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻外交部纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻国家发展和改革委员会纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻国家发展和改革委员会纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻教育部纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻教育部纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻科学技术部纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻科学技术部纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻工业和信息化部纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻工业和信息化部纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻公安部纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻公安部纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻国家安全部纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻国家安全部纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻民政部纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻民政部纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻司法部纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻司法部纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻财政部纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻财政部纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻人力资源和社会保障部纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻人力资源和社会保障部纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻自然资源部纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻自然资源部纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻生态环境部纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻生态环境部纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻住房和城乡建设部纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻住房和城乡建设部纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻交通运输部纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻交通运输部纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻水利部纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻水利部纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻农业农村部纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻农业农村部纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻商务部纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻商务部纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻文化和旅游部纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻文化和旅游部纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻国家卫生健康委员会纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻国家卫生健康委员会纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻退役军人事务部纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻退役军人事务部纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻应急管理部纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻应急管理部纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻中国人民银行纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻中国人民银行纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻审计署纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻审计署纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻国务院国有资产监督管理委员会纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻国务院国有资产监督管理委员会纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻海关总署纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻海关总署纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻国家税务总局纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻国家税务总局纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻国家市场监督管理总局纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻国家市场监督管理总局纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻国家体育总局纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻国家体育总局纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻人民日报社纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻人民日报社纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻中国科学院纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻中国科学院纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻中国社会科学院纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻中国社会科学院纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻中国银行保险监督管理委员会纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻中国银行保险监督管理委员会纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻中国证券监督管理委员会纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻中国证券监督管理委员会纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻最高人民法院纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻最高人民法院纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻最高人民检察院纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻最高人民检察院纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻中央办公厅纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻中央办公厅纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻中央组织部纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻中央组织部纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻中央宣传部纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻中央宣传部纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻中央统一战线工作部纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻中央统一战线工作部纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻中央外事工作委员会办公室纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻中央外事工作委员会办公室纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻全国人大机关纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻全国人大机关纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻全国政协机关纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻全国政协机关纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻中华全国总工会机关纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻中华全国总工会机关纪检监察组.md "wikilink")
  - [中央纪律检查委员会国家监察委员会驻国家电网公司纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻国家电网公司纪检监察组.md "wikilink")（正司局级）
  - [中央纪律检查委员会国家监察委员会驻中国投资有限责任公司纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻中国投资有限责任公司纪检监察组.md "wikilink")（正司局级）
  - [中央纪律检查委员会国家监察委员会驻国家开发银行纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻国家开发银行纪检监察组.md "wikilink")（正司局级）
  - [中央纪律检查委员会国家监察委员会驻中国进出口银行纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻中国进出口银行纪检监察组.md "wikilink")（正司局级）
  - [中央纪律检查委员会国家监察委员会驻中国农业发展银行纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻中国农业发展银行纪检监察组.md "wikilink")（正司局级）
  - [中央纪律检查委员会国家监察委员会驻中国工商银行纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻中国工商银行纪检监察组.md "wikilink")（正司局级）
  - [中央纪律检查委员会国家监察委员会驻中国农业银行纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻中国农业银行纪检监察组.md "wikilink")（正司局级）
  - [中央纪律检查委员会国家监察委员会驻中国银行纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻中国银行纪检监察组.md "wikilink")（正司局级）
  - [中央纪律检查委员会国家监察委员会驻中国建设银行纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻中国建设银行纪检监察组.md "wikilink")（正司局级）
  - [中央纪律检查委员会国家监察委员会驻交通银行纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻交通银行纪检监察组.md "wikilink")（正司局级）
  - [中央纪律检查委员会国家监察委员会驻中国中信集团有限公司纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻中国中信集团有限公司纪检监察组.md "wikilink")（正司局级）
  - [中央纪律检查委员会国家监察委员会驻中国光大（集团）总公司纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻中国光大（集团）总公司纪检监察组.md "wikilink")（正司局级）
  - [中央纪律检查委员会国家监察委员会驻中国人民保险集团股份有限公司纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻中国人民保险集团股份有限公司纪检监察组.md "wikilink")（正司局级）
  - [中央纪律检查委员会国家监察委员会驻中国人寿保险（集团）公司纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻中国人寿保险（集团）公司纪检监察组.md "wikilink")（正司局级）
  - [中央纪律检查委员会国家监察委员会驻中国太平保险集团公司纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻中国太平保险集团公司纪检监察组.md "wikilink")（正司局级）
  - [中央纪律检查委员会国家监察委员会驻中国出口信用保险公司纪检监察组](../Page/中央纪律检查委员会国家监察委员会驻中国出口信用保险公司纪检监察组.md "wikilink")（正司局级）

</onlyinclude>

## 历届组成人员

  - 第五届中央监察委员会

1927年，中共五届一中全会选出中央监察委员会。

  - 中央监察委员会书记：[王荷波](../Page/王荷波.md "wikilink")
  - 中央监察委员会委员（五届一中全会选出）：[王荷波](../Page/王荷波.md "wikilink")、[许白昊](../Page/许白昊.md "wikilink")、[杨匏安](../Page/杨匏安.md "wikilink")、[张佐臣](../Page/张佐臣.md "wikilink")、[刘峻山](../Page/刘峻山.md "wikilink")、[周振声](../Page/周振声.md "wikilink")、[蔡以忱](../Page/蔡以忱.md "wikilink")
  - 中央监察委员会候补委员（五届一中全会选出）：[杨培森](../Page/杨培森.md "wikilink")、[萧石月](../Page/萧石月.md "wikilink")、[阮啸仙](../Page/阮啸仙.md "wikilink")

<!-- end list -->

  - 第七届中央监察委员会

1955年4月，中共七届五中全会通过第七届中央监察委员会成员名单。

  - 书记：[董必武](../Page/董必武.md "wikilink")
  - 副书记：[刘澜涛](../Page/刘澜涛.md "wikilink")、[谭政](../Page/谭政.md "wikilink")、[王从吾](../Page/王从吾.md "wikilink")、[钱瑛](../Page/钱瑛.md "wikilink")、[刘锡五](../Page/刘锡五.md "wikilink")
  - 委员：[王从吾](../Page/王从吾.md "wikilink")、[王维舟](../Page/王维舟.md "wikilink")、[吴溉之](../Page/吴溉之.md "wikilink")、[李士英](../Page/李士英.md "wikilink")、[帅孟奇](../Page/帅孟奇.md "wikilink")、[徐立清](../Page/徐立清.md "wikilink")、[马明方](../Page/马明方.md "wikilink")、[高克林](../Page/高克林.md "wikilink")、[高扬](../Page/高扬.md "wikilink")、[张鼎丞](../Page/张鼎丞.md "wikilink")、[董必武](../Page/董必武.md "wikilink")、[刘锡五](../Page/刘锡五.md "wikilink")、[刘澜涛](../Page/刘澜涛.md "wikilink")、[钱瑛](../Page/钱瑛.md "wikilink")、[谭政](../Page/谭政.md "wikilink")
  - 候补委员：[王维纲](../Page/王维纲.md "wikilink")、[王翰](../Page/王翰.md "wikilink")、[朱明](../Page/朱明_\(中将\).md "wikilink")、[李景膺](../Page/李景膺.md "wikilink")、[梁国斌](../Page/梁国斌.md "wikilink")、[龚子荣](../Page/龚子荣.md "wikilink")

<!-- end list -->

  - 第八届中央监察委员会

1956年9月，中共八届一中全会通过第八届中央监察委员会成员名单。

  - 中央监察委员会书记：[董必武](../Page/董必武.md "wikilink")
  - 中央监察委员会副书记：[刘澜涛](../Page/刘澜涛.md "wikilink")、[萧华](../Page/萧华_\(上将\).md "wikilink")、[王从吾](../Page/王从吾.md "wikilink")、[钱瑛](../Page/钱瑛.md "wikilink")、[刘锡五](../Page/刘锡五.md "wikilink")
  - 中央监察委员会委员：[王从吾](../Page/王从吾.md "wikilink")、[王维舟](../Page/王维舟.md "wikilink")、[王维纲](../Page/王维纲.md "wikilink")、[帅孟奇](../Page/帅孟奇.md "wikilink")、[刘格平](../Page/刘格平.md "wikilink")、[刘锡五](../Page/刘锡五.md "wikilink")、[刘澜涛](../Page/刘澜涛.md "wikilink")、[李士英](../Page/李士英.md "wikilink")、[李楚离](../Page/李楚离.md "wikilink")、[萧华](../Page/萧华_\(上将\).md "wikilink")、[吴溉之](../Page/吴溉之.md "wikilink")、[高克林](../Page/高克林.md "wikilink")、[高扬](../Page/高扬.md "wikilink")、[马明方](../Page/马明方.md "wikilink")、[张鼎丞](../Page/张鼎丞.md "wikilink")、[董必武](../Page/董必武.md "wikilink")、[钱瑛](../Page/钱瑛.md "wikilink")
  - 中央监察委员会候补委员：[王翰](../Page/王翰.md "wikilink")、[刘其人](../Page/刘其人.md "wikilink")、[李景膺](../Page/李景膺.md "wikilink")、[龚子荣](../Page/龚子荣.md "wikilink")

<!-- end list -->

  - 第十一届中央纪律检查委员会

1978年12月，中共第十一届中央委员会第三次全体会议选举产生了由一百人组成的中央纪律检查委员会。

  - 第一书记：[陈云](../Page/陈云.md "wikilink")
  - 第二书记：[邓颖超](../Page/邓颖超.md "wikilink")
  - 第三书记：[胡耀邦](../Page/胡耀邦.md "wikilink")
  - 常务书记：[黄克诚](../Page/黄克诚.md "wikilink")
  - 副书记（11人）：[王鹤寿](../Page/王鹤寿.md "wikilink")、[马国瑞](../Page/马国瑞.md "wikilink")、[王从吾](../Page/王从吾.md "wikilink")、[刘顺元](../Page/刘顺元.md "wikilink")、[张启龙](../Page/张启龙.md "wikilink")、[袁任远](../Page/袁任远.md "wikilink")、[章蕴](../Page/章蕴.md "wikilink")、[郭述申](../Page/郭述申.md "wikilink")、[李一氓](../Page/李一氓.md "wikilink")、[魏文伯](../Page/魏文伯.md "wikilink")、[张策](../Page/张策.md "wikilink")
  - 常务委员会委员（24人）：[马辉之](../Page/马辉之.md "wikilink")、[王建安](../Page/王建安.md "wikilink")、[王维纲](../Page/王维纲.md "wikilink")、[王鹤峰](../Page/王鹤峰.md "wikilink")、[方志纯](../Page/方志纯.md "wikilink")、[孔祥祯](../Page/孔祥祯.md "wikilink")、[帅孟奇](../Page/帅孟奇.md "wikilink")、[吕剑人](../Page/吕剑人.md "wikilink")、[刘型](../Page/刘型.md "wikilink")、[刘建章](../Page/刘建章.md "wikilink")、[刘澜波](../Page/刘澜波.md "wikilink")、[李士英](../Page/李士英.md "wikilink")、[李楚离](../Page/李楚离.md "wikilink")、[张子意](../Page/张子意.md "wikilink")、[武新宇](../Page/武新宇.md "wikilink")、[周扬](../Page/周扬.md "wikilink")、[周仲英](../Page/周仲英.md "wikilink")、[唐天际](../Page/唐天际.md "wikilink")、[曹瑛](../Page/曹瑛.md "wikilink")、[曹广化](../Page/曹广化.md "wikilink")、[阎秀峰](../Page/阎秀峰.md "wikilink")、[韩光](../Page/韩光.md "wikilink")、[傅秋涛](../Page/傅秋涛.md "wikilink")、[曾涌泉](../Page/曾涌泉.md "wikilink")
  - 委员(61人)：[马信](../Page/马信.md "wikilink")、[王大中](../Page/王大中.md "wikilink")、[王文轩](../Page/王文轩.md "wikilink")、[王若水](../Page/王若水.md "wikilink")、[王苏民](../Page/王苏民.md "wikilink")、[王朝文](../Page/王朝文.md "wikilink")、[王直哲](../Page/王直哲.md "wikilink")、[毛铎](../Page/毛铎.md "wikilink")、[文正一](../Page/文正一.md "wikilink")、[平杰三](../Page/平杰三.md "wikilink")、[朱云谦](../Page/朱云谦.md "wikilink")、[朱穆之](../Page/朱穆之.md "wikilink")、[卢仁灿](../Page/卢仁灿.md "wikilink")、[刘英](../Page/刘英.md "wikilink")(女)、[刘丽英](../Page/刘丽英.md "wikilink")(女)、[刘敬之](../Page/刘敬之.md "wikilink")、[刘鸣九](../Page/刘鸣九.md "wikilink")、[安建平](../Page/安建平.md "wikilink")、[严东生](../Page/严东生.md "wikilink")、[多吉才让](../Page/多吉才让.md "wikilink")、[李坚](../Page/李坚.md "wikilink")、[李之琏](../Page/李之琏.md "wikilink")、[李立功](../Page/李立功.md "wikilink")、[李华生](../Page/李华生.md "wikilink")、[李振海](../Page/李振海.md "wikilink")、[杰尔格勒](../Page/杰尔格勒.md "wikilink")、[杨心培](../Page/杨心培.md "wikilink")、[杨长春](../Page/杨长春.md "wikilink")、[杨秀山](../Page/杨秀山.md "wikilink")、[何东昌](../Page/何东昌.md "wikilink")、[何廷一](../Page/何廷一.md "wikilink")、[何善远](../Page/何善远.md "wikilink")、[阿木冬·尼牙孜](../Page/阿木冬·尼牙孜.md "wikilink")、[吴波](../Page/吴波.md "wikilink")、[汪文风](../Page/汪文风.md "wikilink")、[宋诚](../Page/宋诚.md "wikilink")、[张中](../Page/张中.md "wikilink")、[张凯](../Page/张凯.md "wikilink")、[张祺](../Page/张祺.md "wikilink")、[张兆美](../Page/张兆美.md "wikilink")、[张承先](../Page/张承先.md "wikilink")、[张瑞华](../Page/张瑞华.md "wikilink")、[陈林](../Page/陈林.md "wikilink")、[段云](../Page/段云.md "wikilink")、[范儒生](../Page/范儒生.md "wikilink")、[周太和](../Page/周太和.md "wikilink")、[周凤鸣](../Page/周凤鸣.md "wikilink")、[郑爱平](../Page/郑爱平.md "wikilink")、[胡德华](../Page/胡德华.md "wikilink")、[饶正锡](../Page/饶正锡.md "wikilink")、[侯维煜](../Page/侯维煜.md "wikilink")、[徐少甫](../Page/徐少甫.md "wikilink")、[徐深吉](../Page/徐深吉.md "wikilink")、[浦安修](../Page/浦安修.md "wikilink")(女)、[殷继昌](../Page/殷继昌.md "wikilink")、[黄荣](../Page/黄荣.md "wikilink")、[黄甘英](../Page/黄甘英.md "wikilink")、[黄民伟](../Page/黄民伟.md "wikilink")、[彭儒](../Page/彭儒.md "wikilink")(女)、[曾三](../Page/曾三.md "wikilink")(女)、[蹇先任](../Page/蹇先任.md "wikilink")(女)

1980年2月，增补[赵毅敏为副书记](../Page/赵毅敏.md "wikilink")。

  - 第十二届中央纪律检查委员会

1982年9月，中共第十二次全国代表大会选举出第十二届中央纪律检查委员会。

  - 第一书记：陈云
  - 第二书记：黄克诚
  - 常务书记：王鹤寿
  - 书记：王从吾、[韩光](../Page/韩光_\(中共\).md "wikilink")、[李昌](../Page/李昌.md "wikilink")、马国瑞、[韩天石](../Page/韩天石.md "wikilink")
  - 常务委员会委员：马国瑞、王从吾、王鹤寿、李昌、[李之琏](../Page/李之琏.md "wikilink")、[李正亭](../Page/李正亭.md "wikilink")、陈云、黄克诚、韩光、韩天石、[蔡顺礼](../Page/蔡顺礼.md "wikilink")
  - 委员：马国瑞、[王凌](../Page/王凌.md "wikilink")、[王铁](../Page/王铁.md "wikilink")、[王焰](../Page/王焰.md "wikilink")、[王又新](../Page/王又新.md "wikilink")、王从吾、[王尧山](../Page/王尧山.md "wikilink")、[王众音](../Page/王众音.md "wikilink")、[王秉祥](../Page/王秉祥.md "wikilink")、[王宗槐](../Page/王宗槐.md "wikilink")、[王战平](../Page/王战平.md "wikilink")、[王晓光](../Page/王晓光.md "wikilink")、[王福庆](../Page/王福庆.md "wikilink")、[王鹤寿](../Page/王鹤寿.md "wikilink")、[王鹤峰](../Page/王鹤峰.md "wikilink")、[云世英](../Page/云世英.md "wikilink")、毛铎、[文力](../Page/文力.md "wikilink")、文正一、[石生荣](../Page/石生荣.md "wikilink")、[石新山](../Page/石新山.md "wikilink")、[史敏](../Page/史敏.md "wikilink")、[白治民](../Page/白治民.md "wikilink")、[包玉山](../Page/包玉山.md "wikilink")、[朱绍清](../Page/朱绍清.md "wikilink")、[乔青](../Page/乔青.md "wikilink")、[任志恒](../Page/任志恒.md "wikilink")、多吉才让、刘\]、[刘昆](../Page/刘昆.md "wikilink")、[刘汉生](../Page/刘汉生.md "wikilink")、[刘自德](../Page/刘自德.md "wikilink")、刘丽英、刘鸣九、[刘家栋](../Page/刘家栋.md "wikilink")、[刘茹影](../Page/刘茹影.md "wikilink")、[刘新权](../Page/刘新权.md "wikilink")、[刘鹤孔](../Page/刘鹤孔.md "wikilink")、[许梦侠](../Page/许梦侠.md "wikilink")、[孙彤辉(女)](../Page/孙彤辉\(女\).md "wikilink")、[严佑民](../Page/严佑民.md "wikilink")、[严克伦](../Page/严克伦.md "wikilink")、[李庄](../Page/李庄.md "wikilink")、李坚、[李昌](../Page/李昌.md "wikilink")、[李涛](../Page/李涛_\(解放军\).md "wikilink")、[李耀](../Page/李耀.md "wikilink")、李之琏、[李正亭](../Page/李正亭.md "wikilink")、[李兴旺](../Page/李兴旺.md "wikilink")、[李君彦](../Page/李君彦.md "wikilink")、[李哲夫](../Page/李哲夫.md "wikilink")、[李健民](../Page/李健民.md "wikilink")、李振海、[杨克](../Page/杨克.md "wikilink")、[杨珏](../Page/杨珏.md "wikilink")、[杨子谦](../Page/杨子谦.md "wikilink")、[杨西林](../Page/杨西林.md "wikilink")、[杨攸箴](../Page/杨攸箴.md "wikilink")、[杨蕴玉](../Page/杨蕴玉.md "wikilink")、[吴信泉](../Page/吴信泉.md "wikilink")、[余达佳](../Page/余达佳.md "wikilink")、[余述生](../Page/余述生.md "wikilink")、[余建亭](../Page/余建亭.md "wikilink")、[狄子才](../Page/狄子才.md "wikilink")、[邹衍](../Page/邹衍.md "wikilink")、宋诚、[宋洁涵](../Page/宋洁涵.md "wikilink")、[张矛](../Page/张矛.md "wikilink")、[张凯](../Page/张凯.md "wikilink")、[张顺](../Page/张顺.md "wikilink")、[张力行](../Page/张力行.md "wikilink")、[张传栋](../Page/张传栋.md "wikilink")、[张海峰](../Page/张海峰.md "wikilink")、阿木冬·尼牙孜、陈云、[陈坦](../Page/陈坦.md "wikilink")、[陈达之](../Page/陈达之.md "wikilink")、[陈如龙](../Page/陈如龙.md "wikilink")、[邵井蛙](../Page/邵井蛙.md "wikilink")、[范希贤](../Page/范希贤.md "wikilink")、[范朝利](../Page/范朝利.md "wikilink")、[林晓](../Page/林晓.md "wikilink")、[林一心](../Page/林一心.md "wikilink")、[林维先](../Page/林维先.md "wikilink")、[金风](../Page/金风.md "wikilink")、[金石](../Page/金石.md "wikilink")、[金昭典](../Page/金昭典.md "wikilink")、[庞然](../Page/庞然.md "wikilink")、[赵重德](../Page/赵重德.md "wikilink")、[赵起扬](../Page/赵起扬.md "wikilink")、段云、[饶正锡](../Page/饶正锡.md "wikilink")、[徐其孝](../Page/徐其孝.md "wikilink")、徐深吉、[高峻](../Page/高峻.md "wikilink")、[高新华](../Page/高新华.md "wikilink")、[郭建](../Page/郭建.md "wikilink")、[郭春原](../Page/郭春原.md "wikilink")、[唐延杰](../Page/唐延杰.md "wikilink")、[浦安修](../Page/浦安修.md "wikilink")、[晏福生](../Page/晏福生.md "wikilink")、[黄中](../Page/黄中.md "wikilink")、[黄凯](../Page/黄凯_\(政治人物\).md "wikilink")、[黄乃一](../Page/黄乃一.md "wikilink")、[黄民伟](../Page/黄民伟.md "wikilink")、[黄立清](../Page/黄立清.md "wikilink")、黄克诚、[曹广化](../Page/曹广化.md "wikilink")、[曹幼民](../Page/曹幼民.md "wikilink")、[曹冠群](../Page/曹冠群.md "wikilink")、[曹达诺夫](../Page/曹达诺夫.md "wikilink")、[戚元靖](../Page/戚元靖.md "wikilink")、[崔健](../Page/崔健.md "wikilink")、[康迪](../Page/康迪.md "wikilink")、[章林](../Page/章林.md "wikilink")、[梁茂成](../Page/梁茂成.md "wikilink")、彭儒、[彭清云](../Page/彭清云.md "wikilink")、韩光、韩天石、[焦若愚](../Page/焦若愚.md "wikilink")、[焦善民](../Page/焦善民.md "wikilink")、[谢邦治](../Page/谢邦治.md "wikilink")、[詹大南](../Page/詹大南.md "wikilink")、[蔡顺礼](../Page/蔡顺礼.md "wikilink")、[谭开云](../Page/谭开云.md "wikilink")、[谭申平](../Page/谭申平.md "wikilink")、[薛坦](../Page/薛坦.md "wikilink")、[薛凤霄](../Page/薛凤霄.md "wikilink")、蹇先任、[蹇先佛](../Page/蹇先佛.md "wikilink")

<!-- end list -->

  - 第十三届中央纪律检查委员会

1987年11月，中共第十三次全国代表大会选举出第十三届中央纪律检查委员会。

  - 书记：[乔石](../Page/乔石.md "wikilink")
  - 副书记：[陈作霖](../Page/陈作霖.md "wikilink")、[李正亭](../Page/李正亭.md "wikilink")、[肖洪达](../Page/肖洪达.md "wikilink")
  - 常务委员会委员：[王德瑛](../Page/王德瑛.md "wikilink")、[乔石](../Page/乔石.md "wikilink")、[刘丽英](../Page/刘丽英.md "wikilink")、李正亭、[肖洪达](../Page/肖洪达.md "wikilink")、[陈作霖](../Page/陈作霖.md "wikilink")、[郭林祥](../Page/郭林祥.md "wikilink")、[傅杰](../Page/傅杰.md "wikilink")
  - 委员：[丁凤英](../Page/丁凤英.md "wikilink")、[马启新](../Page/马启新.md "wikilink")、[马铁军](../Page/马铁军.md "wikilink")、[王占昌](../Page/王占昌.md "wikilink")、[王言昌](../Page/王言昌.md "wikilink")、[王宗春](../Page/王宗春.md "wikilink")、[王晓光](../Page/王晓光.md "wikilink")、[王维澄](../Page/王维澄.md "wikilink")、[王德瑛](../Page/王德瑛.md "wikilink")、[韦成栋](../Page/韦成栋.md "wikilink")、[云世英](../Page/云世英.md "wikilink")、[巴桑](../Page/巴桑.md "wikilink")、[石庚](../Page/石庚.md "wikilink")、[白石](../Page/白石.md "wikilink")、[冯芝茂](../Page/冯芝茂.md "wikilink")、[吕枫](../Page/吕枫.md "wikilink")、[吕绍堂](../Page/吕绍堂.md "wikilink")、[朱治宏](../Page/朱治宏.md "wikilink")、[乔石](../Page/乔石.md "wikilink")、[多巴](../Page/多巴.md "wikilink")、[刘友法](../Page/刘友法.md "wikilink")、[刘汉桢](../Page/刘汉桢.md "wikilink")、刘丽英、[齐中堂](../Page/齐中堂.md "wikilink")、[许鸣真](../Page/许鸣真.md "wikilink")、[孙彤辉(女)](../Page/孙彤辉\(女\).md "wikilink")、李正亭、[李发荣](../Page/李发荣.md "wikilink")、[李春亭](../Page/李春亭.md "wikilink")、[李焕政](../Page/李焕政.md "wikilink")、[李德明](../Page/李德明.md "wikilink")、[杨敏之](../Page/杨敏之.md "wikilink")、[肖洪达](../Page/肖洪达.md "wikilink")、[汪文风](../Page/汪文风.md "wikilink")、[张明](../Page/张明.md "wikilink")、[张丁华](../Page/张丁华.md "wikilink")、[张伯祥](../Page/张伯祥.md "wikilink")、[张序登](../Page/张序登.md "wikilink")、[张定鸿](../Page/张定鸿.md "wikilink")、陈达之、[陈作霖](../Page/陈作霖.md "wikilink")、[陈法文](../Page/陈法文.md "wikilink")、[武长友](../Page/武长友.md "wikilink")、[林开钦](../Page/林开钦.md "wikilink")、[林英海](../Page/林英海.md "wikilink")、[罗进新](../Page/罗进新.md "wikilink")、[罗运通](../Page/罗运通.md "wikilink")、[孟志元](../Page/孟志元.md "wikilink")、[项华](../Page/项华.md "wikilink")、[赵兴元](../Page/赵兴元.md "wikilink")、[赵保星](../Page/赵保星.md "wikilink")、[荀友明](../Page/荀友明.md "wikilink")、[侯颖](../Page/侯颖.md "wikilink")、[格日勒图](../Page/格日勒图.md "wikilink")、[贾军](../Page/贾军.md "wikilink")、[顾云飞](../Page/顾云飞.md "wikilink")、[徐青](../Page/徐青.md "wikilink")、[徐文伯](../Page/徐文伯.md "wikilink")、[高姿](../Page/高姿.md "wikilink")、[郭林祥](../Page/郭林祥.md "wikilink")、[黄继述](../Page/黄继述.md "wikilink")、[曹芃生](../Page/曹芃生.md "wikilink")、[曹庆泽](../Page/曹庆泽.md "wikilink")、[曹克明](../Page/曹克明.md "wikilink")、[彭钢](../Page/彭钢.md "wikilink")、[彭珮云](../Page/彭珮云.md "wikilink")、[董范园](../Page/董范园.md "wikilink")、[傅杰](../Page/傅杰.md "wikilink")、[谢勇](../Page/谢勇.md "wikilink")

<!-- end list -->

  - 第十四届中央纪律检查委员会

1992年10月中共第十四次全国代表大会选举出第十四届中央纪律检查委员会。

  - 书记：[尉健行](../Page/尉健行.md "wikilink")
  - 副书记：[侯宗宾](../Page/侯宗宾.md "wikilink")、陈作霖、[曹庆泽](../Page/曹庆泽.md "wikilink")、[王德瑛](../Page/王德瑛.md "wikilink")、[徐青](../Page/徐青.md "wikilink")
  - 常务委员会委员：[王光](../Page/王光.md "wikilink")、王德瑛、刘丽英、[安启元](../Page/安启元.md "wikilink")、[李至伦](../Page/李至伦.md "wikilink")、[何勇](../Page/何勇_\(政治人物\).md "wikilink")、陈作霖、[侯宗宾](../Page/侯宗宾.md "wikilink")、[徐青](../Page/徐青.md "wikilink")、[曹庆泽](../Page/曹庆泽.md "wikilink")、[尉健行](../Page/尉健行.md "wikilink")、[彭钢](../Page/彭钢.md "wikilink")、[傅杰](../Page/傅杰.md "wikilink")
  - 委员：[丁凤英](../Page/丁凤英.md "wikilink")、[万绍芬](../Page/万绍芬.md "wikilink")、[马世昌](../Page/马世昌.md "wikilink")、[王光](../Page/王光.md "wikilink")、[王其超](../Page/王其超.md "wikilink")、[王茂润](../Page/王茂润.md "wikilink")、[王宗春](../Page/王宗春.md "wikilink")、[王富中](../Page/王富中.md "wikilink")、[王福义](../Page/王福义.md "wikilink")、[王德顺](../Page/王德顺_\(1939年\).md "wikilink")、[王德瑛](../Page/王德瑛.md "wikilink")、[乌兰木伦](../Page/乌兰木伦.md "wikilink")、[巴桑](../Page/巴桑.md "wikilink")、[甘子玉](../Page/甘子玉.md "wikilink")、[艾维仁](../Page/艾维仁.md "wikilink")、[田聪明](../Page/田聪明.md "wikilink")、[冯少武](../Page/冯少武.md "wikilink")、[冯芝茂](../Page/冯芝茂.md "wikilink")、[冯锡铭](../Page/冯锡铭.md "wikilink")、[朱育理](../Page/朱育理.md "wikilink")、[多巴](../Page/多巴.md "wikilink")、[刘崑](../Page/刘崑.md "wikilink")、[刘锷](../Page/刘锷.md "wikilink")、刘丽英、[刘明仁](../Page/刘明仁.md "wikilink")、[刘贵岭](../Page/刘贵岭.md "wikilink")、[刘峰岩](../Page/刘峰岩.md "wikilink")、[刘积斌](../Page/刘积斌.md "wikilink")、[刘善祥](../Page/刘善祥.md "wikilink")、[安启元](../Page/安启元.md "wikilink")、[祁培文](../Page/祁培文.md "wikilink")、[孙祖梅](../Page/孙祖梅.md "wikilink")、[孙隆椿](../Page/孙隆椿.md "wikilink")、[李钊](../Page/李钊.md "wikilink")、[李文海](../Page/李文海.md "wikilink")、[李成仁](../Page/李成仁.md "wikilink")、[李至伦](../Page/李至伦.md "wikilink")、[李金华](../Page/李金华.md "wikilink")、[李俊杰](../Page/李俊杰.md "wikilink")、[李振东](../Page/李振东.md "wikilink")、[李恩潮](../Page/李恩潮.md "wikilink")、李焕政、[李清林](../Page/李清林.md "wikilink")、[李惠仁](../Page/李惠仁_\(官員\).md "wikilink")、[杨兴富](../Page/杨兴富.md "wikilink")、[杨昌](../Page/杨昌.md "wikilink")、[杨贤足](../Page/杨贤足.md "wikilink")、[杨昌基](../Page/杨昌基.md "wikilink")、[杨崇汇](../Page/杨崇汇.md "wikilink")、[杨敏之](../Page/杨敏之.md "wikilink")、[杨德清](../Page/杨德清.md "wikilink")、[杨德福](../Page/杨德福.md "wikilink")、[吴景春](../Page/吴景春.md "wikilink")、[何勇](../Page/何勇.md "wikilink")、[佟国荣](../Page/佟国荣.md "wikilink")、[闵耀中](../Page/闵耀中.md "wikilink")、[汪文风](../Page/汪文风.md "wikilink")、[沈茂成](../Page/沈茂成.md "wikilink")、[宋国臣](../Page/宋国臣.md "wikilink")、[张轰](../Page/张轰.md "wikilink")、[张文岳](../Page/张文岳.md "wikilink")、[张华林](../Page/张华林.md "wikilink")、[张均法](../Page/张均法.md "wikilink")、[张宝顺](../Page/张宝顺.md "wikilink")、[张惠新](../Page/张惠新.md "wikilink")、[陈为松](../Page/陈为松.md "wikilink")、[陈光琳](../Page/陈光琳.md "wikilink")、陈作霖、[陈明枢](../Page/陈明枢.md "wikilink")、[范新德](../Page/范新德.md "wikilink")、[林兆枢](../Page/林兆枢.md "wikilink")、[林殷才](../Page/林殷才.md "wikilink")、[尚文](../Page/尚文.md "wikilink")、[周声涛](../Page/周声涛.md "wikilink")、[郑国雄](../Page/郑国雄.md "wikilink")、[赵丛](../Page/赵丛.md "wikilink")、[赵地](../Page/赵地.md "wikilink")、[赵宗鼐](../Page/赵宗鼐.md "wikilink")、[胡之光](../Page/胡之光.md "wikilink")、[柳斌](../Page/柳斌.md "wikilink")、[侯颖](../Page/侯颖.md "wikilink")、[侯宗宾](../Page/侯宗宾.md "wikilink")、[饶凤翥](../Page/饶凤翥.md "wikilink")、[洪虎](../Page/洪虎.md "wikilink")、[贺邦靖](../Page/贺邦靖.md "wikilink")、[袁守芳](../Page/袁守芳.md "wikilink")、[格日勒图](../Page/格日勒图.md "wikilink")、[贾军](../Page/贾军.md "wikilink")、[夏国华](../Page/夏国华.md "wikilink")、[顾云飞](../Page/顾云飞.md "wikilink")、[钱冠林](../Page/钱冠林.md "wikilink")、[徐青](../Page/徐青.md "wikilink")、[朗大忠](../Page/朗大忠.md "wikilink")、[曹庆泽](../Page/曹庆泽.md "wikilink")、[曹克明](../Page/曹克明.md "wikilink")、[崔毅](../Page/崔毅.md "wikilink")、[尉健行](../Page/尉健行.md "wikilink")、[隋永举](../Page/隋永举.md "wikilink")、[彭钢](../Page/彭钢.md "wikilink")、[董范园](../Page/董范园.md "wikilink")、[蒋冠庄](../Page/蒋冠庄.md "wikilink")、[韩德乾](../Page/韩德乾.md "wikilink")、[傅杰](../Page/傅杰.md "wikilink")、[傅志寰](../Page/傅志寰.md "wikilink")、[谢安山](../Page/谢安山.md "wikilink")、[靳玉德](../Page/靳玉德.md "wikilink")、[谭福德](../Page/谭福德.md "wikilink")、[翟泰丰](../Page/翟泰丰.md "wikilink")

<!-- end list -->

  - 第十五届中央纪律检查委员会

1997年9月，中共第十五次全国代表大会选举出第十五届中央纪律检查委员会。

  - 书记：尉健行
  - 副书记：韩杼滨、曹庆泽、[何勇](../Page/何勇_\(政治人物\).md "wikilink")、周子玉、[夏赞忠](../Page/夏赞忠.md "wikilink")、[刘丽英](../Page/刘丽英.md "wikilink")（女）、刘锡荣、傅杰、张惠新
  - 常务委员会委员：尉健行、韩杼滨、曹庆泽、何勇、周子玉、[夏赞忠](../Page/夏赞忠.md "wikilink")、刘丽英、傅杰、李至伦、[马馼](../Page/马馼.md "wikilink")、[祁培文](../Page/祁培文.md "wikilink")、[李登柱](../Page/李登柱.md "wikilink")、[赵洪祝](../Page/赵洪祝.md "wikilink")、彭钢（女）、[刘锡荣](../Page/刘锡荣.md "wikilink")、[袁纯清](../Page/袁纯清.md "wikilink")、吴定富、张惠新
  - 秘书长：[袁纯清](../Page/袁纯清.md "wikilink")
  - 委员：[于友先](../Page/于友先.md "wikilink")、[于洪葆](../Page/于洪葆.md "wikilink")、[马馼](../Page/马馼.md "wikilink")、[马凯](../Page/马凯_\(官员\).md "wikilink")、[马世昌](../Page/马世昌.md "wikilink")、[王成铭](../Page/王成铭.md "wikilink")、[王同琢](../Page/王同琢.md "wikilink")、[王华元](../Page/王华元.md "wikilink")、[王众孚](../Page/王众孚.md "wikilink")、[王国章](../Page/王国章.md "wikilink")、[王胜俊](../Page/王胜俊.md "wikilink")、[王莉莉](../Page/王莉莉.md "wikilink")、[王唯众](../Page/王唯众.md "wikilink")、王德顺、[尤仁](../Page/尤仁.md "wikilink")、[乌兰木伦](../Page/乌兰木伦.md "wikilink")、[方嘉德](../Page/方嘉德.md "wikilink")、[布穷](../Page/布穷.md "wikilink")、[田淑兰](../Page/田淑兰.md "wikilink")、[田聪明](../Page/田聪明.md "wikilink")、[白志健](../Page/白志健.md "wikilink")、[冯芝茂](../Page/冯芝茂.md "wikilink")、[朱增泉](../Page/朱增泉.md "wikilink")、[刘锷](../Page/刘锷.md "wikilink")、[刘丰富](../Page/刘丰富.md "wikilink")、[刘文杰](../Page/刘文杰.md "wikilink")、刘丽英、[刘学斌](../Page/刘学斌.md "wikilink")、[刘峰岩](../Page/刘峰岩.md "wikilink")、[刘积斌](../Page/刘积斌.md "wikilink")、[刘雅芝](../Page/刘雅芝.md "wikilink")、[刘锡荣](../Page/刘锡荣.md "wikilink")、[祁培文](../Page/祁培文.md "wikilink")、[许中田](../Page/许中田.md "wikilink")、[孙淦](../Page/孙淦.md "wikilink")、[孙载夫](../Page/孙载夫.md "wikilink")、[李有慰](../Page/李有慰.md "wikilink")、[李成仁](../Page/李成仁.md "wikilink")、李至伦、[李英唐](../Page/李英唐.md "wikilink")、[李虎林](../Page/李虎林.md "wikilink")、[李国光](../Page/李国光.md "wikilink")、[李宝祥](../Page/李宝祥.md "wikilink")、[李建玉](../Page/李建玉.md "wikilink")、[李铁林](../Page/李铁林.md "wikilink")、[李继松](../Page/李继松.md "wikilink")、[李雪莹](../Page/李雪莹.md "wikilink")、[李焕政](../Page/李焕政.md "wikilink")、[李惠仁](../Page/李惠仁_\(官員\).md "wikilink")、[李登柱](../Page/李登柱.md "wikilink")、[杨贤足](../Page/杨贤足.md "wikilink")、[杨惠川](../Page/杨惠川.md "wikilink")、[杨德清](../Page/杨德清.md "wikilink")、[吴广才](../Page/吴广才.md "wikilink")、[吴定富](../Page/吴定富.md "wikilink")、[吴润忠](../Page/吴润忠.md "wikilink")、[吴野渡](../Page/吴野渡.md "wikilink")、[何勇](../Page/何勇.md "wikilink")、[冷宽](../Page/冷宽.md "wikilink")、[闵耀中](../Page/闵耀中.md "wikilink")、[沈国俊](../Page/沈国俊.md "wikilink")、[张轰](../Page/张轰.md "wikilink")、[张黎](../Page/张黎.md "wikilink")、[张毅](../Page/张毅.md "wikilink")、[张天保](../Page/张天保.md "wikilink")、[张凤楼](../Page/张凤楼.md "wikilink")、[张玉芹](../Page/张玉芹.md "wikilink")、[张左己](../Page/张左己.md "wikilink")、[张华林](../Page/张华林.md "wikilink")、[张吾乐](../Page/张吾乐.md "wikilink")、[张柏林](../Page/张柏林.md "wikilink")、[张钰钟](../Page/张钰钟.md "wikilink")、张惠新、[陈光林](../Page/陈光林.md "wikilink")、[陈福今](../Page/陈福今.md "wikilink")、[范新德](../Page/范新德.md "wikilink")、[罗锋](../Page/罗锋.md "wikilink")、[罗清泉](../Page/罗清泉.md "wikilink")、[金道铭](../Page/金道铭.md "wikilink")、[周子玉](../Page/周子玉.md "wikilink")、[周可仁](../Page/周可仁.md "wikilink")、[郑万通](../Page/郑万通.md "wikilink")、[郑坤生](../Page/郑坤生.md "wikilink")、[赵荣](../Page/赵荣.md "wikilink")、[赵虹](../Page/赵虹.md "wikilink")、[赵春兰](../Page/赵春兰.md "wikilink")、[赵洪祝](../Page/赵洪祝.md "wikilink")、[赵海渔](../Page/赵海渔.md "wikilink")、[胡家燕](../Page/胡家燕.md "wikilink")、[贺邦靖](../Page/贺邦靖.md "wikilink")、[贺美英](../Page/贺美英.md "wikilink")、[袁纯清](../Page/袁纯清.md "wikilink")、[夏国华](../Page/夏国华.md "wikilink")、[夏赞忠](../Page/夏赞忠.md "wikilink")、[徐光春](../Page/徐光春.md "wikilink")、[徐承栋](../Page/徐承栋.md "wikilink")、曹庆泽、[曹克明](../Page/曹克明.md "wikilink")、[曹和庆](../Page/曹和庆.md "wikilink")、[曹洪兴](../Page/曹洪兴.md "wikilink")、[曹惠臣](../Page/曹惠臣.md "wikilink")、[康成元](../Page/康成元.md "wikilink")、[梁绮萍](../Page/梁绮萍.md "wikilink")、尉健行、彭钢、[董雷](../Page/董雷.md "wikilink")、[董宜胜](../Page/董宜胜.md "wikilink")、[韩灵](../Page/韩灵.md "wikilink")、[韩杼滨](../Page/韩杼滨.md "wikilink")、[程世峨](../Page/程世峨.md "wikilink")、傅杰、[解振华](../Page/解振华.md "wikilink")、[谭乃达](../Page/谭乃达.md "wikilink")、[翟月卿](../Page/翟月卿.md "wikilink")、[濮洪九](../Page/濮洪九.md "wikilink")

<!-- end list -->

  - 第十六届中央纪律检查委员会

2002年11月，中共第十六次全国代表大会选举出第十六届中央纪律检查委员会。

  - 书记：[吴官正](../Page/吴官正.md "wikilink")
  - 副书记：何勇、夏赞忠、李至伦、[张树田](../Page/张树田.md "wikilink")、刘锡荣、张惠新、[刘峰岩](../Page/刘峰岩.md "wikilink")、[马馼](../Page/马馼.md "wikilink")、[干以胜](../Page/干以胜.md "wikilink")
  - 常务委员会委员：[干以胜](../Page/干以胜.md "wikilink")、马馼、[马志鹏](../Page/马志鹏.md "wikilink")、[王振川](../Page/王振川.md "wikilink")、[刘峰岩](../Page/刘峰岩.md "wikilink")、[刘家义](../Page/刘家义.md "wikilink")、刘锡荣、[吴官正](../Page/吴官正.md "wikilink")、[吴毓萍](../Page/吴毓萍.md "wikilink")、何勇、[沈德咏](../Page/沈德咏.md "wikilink")、[张树田](../Page/张树田.md "wikilink")、张惠新、赵洪祝、夏赞忠、[黄树贤](../Page/黄树贤.md "wikilink")、[解厚铨](../Page/解厚铨.md "wikilink")、[吴玉良](../Page/吴玉良_\(中纪委\).md "wikilink")、李至伦
  - 秘书长兼新闻发言人：[干以胜](../Page/干以胜.md "wikilink")
  - 委员：干以胜、马馼、[马子龙](../Page/马子龙.md "wikilink")、[马志鹏](../Page/马志鹏.md "wikilink")、[马铁山](../Page/马铁山.md "wikilink")、[王成铭](../Page/王成铭.md "wikilink")、[王同琢](../Page/王同琢.md "wikilink")、[王华元](../Page/王华元.md "wikilink")、[王寿亭](../Page/王寿亭.md "wikilink")、[王志刚](../Page/王志刚.md "wikilink")、[王建宙](../Page/王建宙.md "wikilink")、[王显政](../Page/王显政.md "wikilink")、[王振川](../Page/王振川.md "wikilink")、王莉莉、[王唯众](../Page/王唯众.md "wikilink")、王德顺、[尹凤岐](../Page/尹凤岐_\(将军\).md "wikilink")、[巴特尔](../Page/巴特尔_\(中国官员\).md "wikilink")、[布穷](../Page/布穷.md "wikilink")、[田淑兰](../Page/田淑兰.md "wikilink")、[白玛](../Page/白玛.md "wikilink")、[冯永生](../Page/冯永生.md "wikilink")、[冯健身](../Page/冯健身.md "wikilink")、[邢元敏](../Page/邢元敏.md "wikilink")、[朱维群](../Page/朱维群.md "wikilink")、[乔宗淮](../Page/乔宗淮.md "wikilink")、[任泽民](../Page/任泽民.md "wikilink")、[刘江](../Page/刘江.md "wikilink")、[刘丰富](../Page/刘丰富.md "wikilink")、[刘志峰](../Page/刘志峰.md "wikilink")、[刘晓江](../Page/刘晓江.md "wikilink")、刘峰岩、[刘家义](../Page/刘家义.md "wikilink")、刘锡荣、[安立敏](../Page/安立敏.md "wikilink")、[孙文盛](../Page/孙文盛.md "wikilink")、[孙宝树](../Page/孙宝树.md "wikilink")、[孙载夫](../Page/孙载夫.md "wikilink")、[阳安江](../Page/阳安江.md "wikilink")、[李文华](../Page/李文华.md "wikilink")、[李玉赋](../Page/李玉赋.md "wikilink")、[李东生](../Page/李东生.md "wikilink")、[李有慰](../Page/李有慰.md "wikilink")、李至伦、[李传卿](../Page/李传卿.md "wikilink")、[李运之](../Page/李运之.md "wikilink")、[李金明](../Page/李金明.md "wikilink")、[李继松](../Page/李继松.md "wikilink")、[李雪莹](../Page/李雪莹.md "wikilink")、[李崇禧](../Page/李崇禧.md "wikilink")、[李清林](../Page/李清林.md "wikilink")、[杨光洪](../Page/杨光洪.md "wikilink")、[杨多良](../Page/杨多良.md "wikilink")、[吴广才](../Page/吴广才.md "wikilink")、[吴玉良](../Page/吴玉良_\(中纪委\).md "wikilink")、[吴官正](../Page/吴官正.md "wikilink")、[吴毓萍](../Page/吴毓萍.md "wikilink")、[何勇](../Page/何勇.md "wikilink")、[沈淑济](../Page/沈淑济.md "wikilink")、[沈德咏](../Page/沈德咏.md "wikilink")、[张毅](../Page/张毅_\(中共官员\).md "wikilink")、[张凤楼](../Page/张凤楼.md "wikilink")、张树田、[张恩照](../Page/张恩照.md "wikilink")、[张钰钟](../Page/张钰钟.md "wikilink")、张惠新、[陈希](../Page/陈希.md "wikilink")、[陈培忠](../Page/陈培忠.md "wikilink")、[陈冀平](../Page/陈冀平.md "wikilink")、[范新德](../Page/范新德.md "wikilink")、[林文肯](../Page/林文肯.md "wikilink")、[罗世谦](../Page/罗世谦.md "wikilink")、[岳宣义](../Page/岳宣义.md "wikilink")、[金银焕](../Page/金银焕.md "wikilink")、[金道铭](../Page/金道铭.md "wikilink")、[周占顺](../Page/周占顺.md "wikilink")、[郑坤生](../Page/郑坤生.md "wikilink")、[赵荣](../Page/赵荣.md "wikilink")、[赵春兰](../Page/赵春兰.md "wikilink")、赵洪祝、[胡家燕](../Page/胡家燕.md "wikilink")、[祝光耀](../Page/祝光耀.md "wikilink")、[祝春林](../Page/祝春林.md "wikilink")、[贺邦靖](../Page/贺邦靖.md "wikilink")、[秦绍德](../Page/秦绍德.md "wikilink")、[聂成根](../Page/聂成根.md "wikilink")、[贾文先](../Page/贾文先.md "wikilink")、[夏赞忠](../Page/夏赞忠.md "wikilink")、[徐承栋](../Page/徐承栋.md "wikilink")、[徐敬业](../Page/徐敬业_\(共和国\).md "wikilink")、[高俊良](../Page/高俊良.md "wikilink")、[陶方桂](../Page/陶方桂.md "wikilink")、[黄丹华](../Page/黄丹华.md "wikilink")、[黄远志](../Page/黄远志.md "wikilink")、[黄树贤](../Page/黄树贤.md "wikilink")、[黄淑和](../Page/黄淑和.md "wikilink")、[黄献中](../Page/黄献中.md "wikilink")、[曹洪兴](../Page/曹洪兴.md "wikilink")、[曹康泰](../Page/曹康泰.md "wikilink")、[常小兵](../Page/常小兵.md "wikilink")、[崔会烈](../Page/崔会烈.md "wikilink")、[康日新](../Page/康日新.md "wikilink")、[梁绮萍](../Page/梁绮萍.md "wikilink")、[彭小枫](../Page/彭小枫.md "wikilink")、[董雷](../Page/董雷.md "wikilink")、[董万才](../Page/董万才.md "wikilink")、[董宜胜](../Page/董宜胜.md "wikilink")、[韩长赋](../Page/韩长赋.md "wikilink")、[韩忠信](../Page/韩忠信.md "wikilink")、[傅克诚](../Page/傅克诚.md "wikilink")、[焦焕成](../Page/焦焕成.md "wikilink")、[谢作炎](../Page/谢作炎.md "wikilink")、[楼继伟](../Page/楼继伟.md "wikilink")、[解厚铨](../Page/解厚铨.md "wikilink")、[蔡长松](../Page/蔡长松.md "wikilink")、[翟小衡](../Page/翟小衡.md "wikilink")、[樊守志](../Page/樊守志.md "wikilink")、[滕久明](../Page/滕久明.md "wikilink")、[薛利](../Page/薛利.md "wikilink")、[魏建国](../Page/魏建国.md "wikilink")、[魏家福](../Page/魏家福.md "wikilink")

<!-- end list -->

  - 第十七届中央纪律检查委员会

<!-- end list -->

  - 第十八届中央纪律检查委员会

<!-- end list -->

  - 第十九届中央纪律检查委员会

## 注释

## 参考文献

## 外部链接

  - [中央纪委国家监委网站](http://www.ccdi.gov.cn)
      - [中央纪委国家监委网站反腐败国际追逃追赃专栏](http://www.ccdi.gov.cn/special/ztzz/)
  - [全国纪检监察机关统一举报网站](http://www.12388.gov.cn)

## 参见

  - [中国共产党纪律检查机关](../Page/中国共产党纪律检查机关.md "wikilink")
      - [中国共产党中央监察委员会](../Page/中国共产党中央监察委员会.md "wikilink")
      - [中国共产党中央纪律检查委员会书记](../Page/中国共产党中央纪律检查委员会书记.md "wikilink")
  - [中央巡视工作领导小组](../Page/中央巡视工作领导小组.md "wikilink")
  - [中國古代行政監察制度](../Page/中國古代行政監察制度.md "wikilink")：[御史臺](../Page/御史臺.md "wikilink")、[都察院](../Page/都察院.md "wikilink")
  - [中华人民共和国各级监察委员会](../Page/中华人民共和国各级监察委员会.md "wikilink")
      - [中华人民共和国国家监察委员会](../Page/中华人民共和国国家监察委员会.md "wikilink")
      - [留置 (监察措施)](../Page/留置_\(监察措施\).md "wikilink")
  - [中华人民共和国监察部](../Page/中华人民共和国监察部.md "wikilink")、[国家预防腐败局](../Page/国家预防腐败局.md "wikilink")
  - [中华人民共和国最高人民检察院反贪污贿赂总局](../Page/中华人民共和国最高人民检察院反贪污贿赂总局.md "wikilink")

{{-}}

[中国共产党中央纪律检查委员会](../Category/中国共产党中央纪律检查委员会.md "wikilink")
[Category:中华人民共和国反腐败机构](../Category/中华人民共和国反腐败机构.md "wikilink")

1.
2.
3.