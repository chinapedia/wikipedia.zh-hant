**千陽號**為[日本漫畫及](../Page/日本漫畫.md "wikilink")[動畫](../Page/日本動畫.md "wikilink")《[ONE
PIECE](../Page/ONE_PIECE.md "wikilink")》中虛構的海賊船。屬Brig
Sloop型的船，以**獅子頭**造形的船頭以及「**士兵船塢系統**」為其主要特色，是**[草帽海賊團](../Page/ONE_PIECE海賊列表#草帽海賊團.md "wikilink")**的**第2艘海賊船**。

## 海賊船簡介

[Thousand_Sunny.JPG](https://zh.wikipedia.org/wiki/File:Thousand_Sunny.JPG "fig:Thousand_Sunny.JPG")
千陽號的取名者為[艾斯巴古](../Page/水之七島#艾斯巴古.md "wikilink")，意義為「要像[太陽一樣](../Page/太陽.md "wikilink")，從容快樂的，穿越殘酷的萬里大海」。

設計者為[佛朗基](../Page/佛朗基.md "wikilink")，使用價值不斐、世界上擁有最高強度的寶樹「亞當」為材料，並由[佛朗基](../Page/佛朗基.md "wikilink")、[艾斯巴古](../Page/水之七島#艾斯巴古.md "wikilink")、[包利](../Page/水之七島#包利.md "wikilink")、[畢普利·露露](../Page/水之七島#畢普利·露露.md "wikilink")、[戴魯斯通合作趕工建造](../Page/水之七島#戴魯斯通.md "wikilink")。

主要特色是獅子造形的船頭及「**士兵船塢系統**」，此外還有草皮作成的[甲板](../Page/甲板.md "wikilink")，附有大型[水族館的](../Page/水族館.md "wikilink")[房間](../Page/房間.md "wikilink")、[圖書館等許多方便的](../Page/圖書館.md "wikilink")[設備](../Page/設備.md "wikilink")。

「大事件」發生兩年後，佛朗基回到[夏波帝諸島為千陽號保養與安裝新兵器](../Page/夏波帝諸島.md "wikilink")，並由[雷利上膜](../Page/席爾巴斯·雷利.md "wikilink")，向魚人島出發。

### 具備自我意識

和[前進梅利號一樣](../Page/前進梅利號.md "wikilink")，具備自我意識，艦首的獅子頭和梅利號的羊頭一樣依照狀況不同而出現各種表情。當草帽海賊團從海底通過急速的海流浮上水面時，千陽號亦曾露出雙眼昏花的表情。

[劇場版Z中在遭到](../Page/ONE_PIECE_FILM_Z.md "wikilink")「新海軍」圍剿時，千陽號疑似自行發動了「風來爆發」協助一行人死裡逃生。

## 船內設備

### 1樓

| 設施名稱      | 簡介                                                                                                                                                                                                                                                                                                                                           |
| --------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **男生房間**  | 比過去寬大很多，使用上下舖，入口旁有[梳洗台](../Page/梳洗台.md "wikilink")、[牙具和叫醒大家的](../Page/牙具.md "wikilink")[鬧鐘](../Page/鬧鐘.md "wikilink")，內部有溝渠式[暖爐](../Page/暖爐.md "wikilink")，洗衣工具組，床為木製[吊床](../Page/吊床.md "wikilink")7張，壁櫥7個，外加兩門大砲，牆上貼有6張[懸賞單](../Page/ONE_PIECE用語列表#懸賞金額.md "wikilink")。原本[賓什莫克·香吉士的沒貼](../Page/賓什莫克·香吉士.md "wikilink")，多雷斯羅薩事件落幕後增加香吉士的懸賞單。 |
| **水族館**   | 裡頭整面牆都是[魚缸](../Page/魚缸.md "wikilink")，設有沙發座椅，可以觀賞各種[魚類](../Page/魚類.md "wikilink")，其實是為了保持魚類鮮度，所用升降設施和廚房相連接，入口處的旁邊放著很多酒，[水槽內側有一條通路](../Page/水槽.md "wikilink")，通路裡有兩個大砲，裡頭連[天梯也是魚缸](../Page/天梯.md "wikilink")。                                                                                                                                 |
| **能量供應室** | 以3桶[可樂為能量](../Page/可樂.md "wikilink")，讓船後方的引擎發射「風來爆發」，能飛行一公里以上。                                                                                                                                                                                                                                                                               |
|           |                                                                                                                                                                                                                                                                                                                                              |

### 2樓

| 設施名稱                               | 簡介                                                                                                                                                                                                                                                                                                                                                                                                     |
| ---------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| **保健室**                            | [多尼多尼·喬巴的房間](../Page/多尼多尼·喬巴.md "wikilink")，裡頭有他嚮往會轉的[椅子](../Page/椅子.md "wikilink")。一旦沒病人此處就會變成走廊。                                                                                                                                                                                                                                                                                                     |
| **[廚房](../Page/廚房.md "wikilink")** | 位在船的中心，裡頭有巨大[烤箱](../Page/烤箱.md "wikilink")、[煙囪](../Page/煙囪.md "wikilink")、[換氣扇](../Page/換氣扇.md "wikilink")、電傳蟲一隻和上鎖的[冰箱](../Page/冰箱.md "wikilink")，密碼是「7326」（同時包含[娜美](../Page/娜美.md "wikilink")、[賓什莫克·香吉士和](../Page/賓什莫克·香吉士.md "wikilink")[妮可·羅賓的生日數字](../Page/妮可·羅賓.md "wikilink")），目的是阻止香吉士以外的男生偷吃（主要是[魯夫](../Page/魯夫.md "wikilink")），廚房巨大灶台前的[桅杆有傳送食物的小型升降梯](../Page/桅杆.md "wikilink")，可以把食物送往水族館。 |
| **女生房間**                           | [娜美和](../Page/娜美.md "wikilink")[妮可·羅賓的房間](../Page/妮可·羅賓.md "wikilink")，比男生房間豪華，入口旁牆壁中央有梳洗台，擺放紅酒和茶具，內部有梳妝台、床兩張、沙發兩張、壁櫥兩個和[娜美的寶箱](../Page/娜美.md "wikilink")，外加兩門大砲。[香吉士和](../Page/香吉士_\(ONE_PIECE\).md "wikilink")[布魯克暱稱此處為](../Page/布魯克.md "wikilink")「秘密花園」\[1\]。通常其他男性都不會在這間房間休息，前往多雷斯羅薩的途中，娜美和羅賓則顧慮到當時同船的桃之助還是個小孩，所以特地讓桃之助和她們睡在同張床鋪上。                                                                |
| **糧倉**                             | 因為放置的是沒有加工過的[食物](../Page/食物.md "wikilink")（如乳酪、乾糧等），所以[魯夫也沒輒](../Page/魯夫.md "wikilink")。                                                                                                                                                                                                                                                                                                               |
| **魚缸口**                            | 將抓到的魚從這個入口丟進水族館，想吃魚就從這裡撈，採完全防雨密閉式。                                                                                                                                                                                                                                                                                                                                                                     |
|                                    |                                                                                                                                                                                                                                                                                                                                                                                                        |

### 3樓與室外

<table>
<thead>
<tr class="header">
<th><p>設施名稱</p></th>
<th><p>簡介</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>圖書館、測量室</strong></p></td>
<td><p>除<a href="../Page/魯夫.md" title="wikilink">魯夫之外一行人的書都放在這裡</a>，測量室則屬於<a href="../Page/娜美.md" title="wikilink">娜美的</a>，放置<a href="../Page/航海日誌.md" title="wikilink">航海日誌</a>，設有<a href="../Page/測量機.md" title="wikilink">測量機</a>。</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/浴室.md" title="wikilink">浴室</a></strong></p></td>
<td><p>位在圖書館、測量室的上頭，爬<a href="../Page/梯子.md" title="wikilink">梯子就可以上去</a>，是個洋式的<a href="../Page/大浴場.md" title="wikilink">大浴場</a>。「大事件」結束兩年後，娜美利用從小空島維薩利亞學到的氣象知識，在浴室裡製作沐浴氣候（會從中撒出洗澡熱水的小雲朵），以方便沐浴時使用。</p></td>
</tr>
<tr class="odd">
<td><p><strong>船頭</strong></p></td>
<td><p>:* <strong>螺旋雄獅</strong></p>
<p>::緊急迴避系統，將獅頭上的鬃毛順時針旋轉，船就能夠倒退。（此功能被<a href="../Page/佛朗基.md" title="wikilink">佛朗基命名為</a><strong>膽小旅程</strong>[2]）</p>
<p>:* <strong>獅吼炮</strong></p>
<p>::千陽號的艦首砲，從艦首獅嘴發射出威力超強的<a href="../Page/加農砲.md" title="wikilink">加農砲</a>，可利用獅頭內部操控室的「狙擊圈」自由網羅目標，需要花費3桶可樂。</p>
<p>::要發動獅吼砲必須同時使用花費兩桶<a href="../Page/可樂.md" title="wikilink">可樂的</a>「風來爆發」才可以避免<a href="../Page/後座力.md" title="wikilink">後座力讓船飛退</a>，因此佛朗基表示總計需要花費5桶可樂，除非陷入緊急狀態，要不然此招不能常用。</p>
<p>:* <strong>千陽眼力</strong></p>
<dl>
<dt></dt>
<dd><dl>
<dt></dt>
<dd>於第二部中登場的功能，從獅頭眼部發出的探照燈。
</dd>
</dl>
</dd>
</dl></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/錨.md" title="wikilink">錨</a></strong></p></td>
<td><p>造型是獅爪，可以改變方向</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/舵輪.md" title="wikilink">舵輪</a></strong></p></td>
<td><p>依上頭的數字和旁邊的拉桿選擇士兵船塢系統的頻道，附有乘坐椅子。</p></td>
</tr>
<tr class="even">
<td><p><strong>甲板</strong></p></td>
<td><p>使用草皮做成。</p></td>
</tr>
<tr class="odd">
<td><p><strong>瞭望台、<a href="../Page/健身房.md" title="wikilink">健身房</a></strong></p></td>
<td><p>設有天體<a href="../Page/望遠鏡.md" title="wikilink">望遠鏡</a>，下方有擴音器和麥克風相連接，裡頭擺放了<a href="../Page/啞鈴.md" title="wikilink">啞鈴</a>、鐵棒等<a href="../Page/羅羅亞·索隆.md" title="wikilink">羅羅亞·索隆</a><a href="../Page/健身.md" title="wikilink">健身用的</a><a href="../Page/器具.md" title="wikilink">器具</a>。</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/娜美.md" title="wikilink">娜美的橘子樹</a></strong></p></td>
<td><p>從<a href="../Page/娜美.md" title="wikilink">娜美的故鄉可可亞西村貝爾梅爾家的橘子園移植過來的</a>3棵橘子樹（原本栽種在<a href="../Page/前進梅利號.md" title="wikilink">前進梅利號上</a>）。由於橘子非常美味，因此瞄住橘子的外敵（主要是<a href="../Page/魯夫.md" title="wikilink">魯夫</a>）及保護橘子是<a href="../Page/賓什莫克·香吉士.md" title="wikilink">賓什莫克·香吉士的任務</a>。</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/妮可·羅賓.md" title="wikilink">妮可·羅賓的花圃</a></strong></p></td>
<td><p>由磚塊鋪呈，設在3樓最容易曬到太陽的地方，因為羅賓非常喜歡花，因此常在這種植許多形形色色的花。</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/騙人布.md" title="wikilink">騙人布的小花園</a></strong></p></td>
<td><p>第二部中登場，騙人布用來栽種「綠星」的植物園，設在羅賓的花圃旁邊，劇場版中有替綠星灑除蟲農藥的劇照。</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 地下1樓

| 設施名稱                                                                     | 簡介                                                        |
| ------------------------------------------------------------------------ | --------------------------------------------------------- |
| **[騙人布工廠](../Page/騙人布.md "wikilink")**                                   | [騙人布開發戰鬥用兵器及道具的所在](../Page/騙人布.md "wikilink")。            |
| **木材庫**                                                                  | 補強用木材的倉庫（建造千陽號的木材取自寶樹「亞當」）。                               |
| **軍火庫**                                                                  | 放置彈藥等武器的地方。                                               |
| **兵器開發室**                                                                | [佛朗基的房間](../Page/佛朗基.md "wikilink")，裡頭放置了大量的鐵等製作武器時必要的材料。 |
| **[士兵船塢系統](../Page/千陽號#特殊機能#士兵船塢系統（SOLDIER_DOCK_SYSTEM）.md "wikilink")** | 千陽號特有的輔助船塢系統。                                             |
|                                                                          |                                                           |

## 特殊機能

### 緊急逃脫加速系統「風來爆發」

利用可樂作為動力的超大型空氣砲。拜構成船體的素材「寶樹亞當」所擁有的高強度所賜，千陽號得以利用設置於艦尾的該裝備實現長達一公里的跳躍飛行，藉此從緊急狀態下脫身，使用一次要消耗3桶可樂。

### 士兵船塢系統（SOLDIER DOCK SYSTEM）

由佛朗基開發，用於輔助母船巡航的子船系統（也可說是士兵）。圓形的船塢分成6個小房間，依舵輪上頭的數字和旁邊的拉桿選擇和頻道對應的艦載機，乘坐者可以先到位於船中心的「乘組員待機室」待命。
[Thousand_Sunny_-_Soldier_Dock_System_en.svg](https://zh.wikipedia.org/wiki/File:Thousand_Sunny_-_Soldier_Dock_System_en.svg "fig:Thousand_Sunny_-_Soldier_Dock_System_en.svg")

:\* **零號頻道－外輪**

  -

      -
        以[可樂為動力發動船兩側的外輪](../Page/可樂.md "wikilink")，能夠輕鬆應付帆船最怕的兩種窘境：龍捲風或暴風雨。

:\* **一號頻道－白木馬一號**

  -

      -
        原本是[娜美的威霸](../Page/娜美.md "wikilink")，後來前端被[佛朗基改成了可愛的馬頭造型](../Page/佛朗基.md "wikilink")，鼻子部份附有照明燈，由於沒經過娜美同意而被她臭罵一頓\[3\]。其操作因為要解暸海水的流動，所以十分困難，所有成員當中只有娜美會操作。威霸是[空島特有的單人用交通工具](../Page/空島_\(ONE_PIECE\).md "wikilink")，能在海面行駛，就像[水上摩托車一樣](../Page/水上摩托車.md "wikilink")；在[草帽海賊團裡的威霸裝有已絕種的](../Page/草帽海賊團.md "wikilink")「噴風貝」兩枚，馬力非常強大。

:\* **二號頻道－迷你梅利二號**

  -

      -
        四人座蒸汽[外輪船](../Page/外輪船.md "wikilink")，佛朗基參考了梅利號的造型打造而成。後來加入的[布魯克從未看過前進梅利號的模樣](../Page/布魯克_\(ONE_PIECE\).md "wikilink")，因此他都將迷你梅利二號暱稱為「小羊羊」。

:\* **三號頻道－鯊魚三號**

  -

      -
        3人座偵查[潛水艇](../Page/潛水艇.md "wikilink")，裡頭附有[潛望鏡和電話蟲](../Page/潛望鏡.md "wikilink")（一種在偉大航路作為通訊工具使用、外形像蝸牛的生物），此潛水艇最深限度能潛到5,000公尺。

:\* **四號頻道－黑犀FR-U四號**

  -

      -
        於第二部中登場，佛朗基藉由導入[形狀記憶合金](../Page/形狀記憶合金.md "wikilink")「瓦波金屬」製成的新兵器，前端有著[犀牛頭型的重型](../Page/犀牛.md "wikilink")[機車](../Page/機車.md "wikilink")。

:\* **五號頻道－腕龍戰車五號**

  -

      -
        於第二部中登場，佛朗基藉由導入形狀記憶合金「瓦波金屬」製成的新兵器，砲口為[腕龍頭型的](../Page/腕龍.md "wikilink")[坦克車](../Page/坦克車.md "wikilink")。

::\* **「鋼鐵海賊」佛朗基將軍**

  -

      -
        於第二部中登場，由**四號頻道－黑犀FR-U四號**與**五號頻道－腕龍戰車五號**合體而成的大型機械人。被佛朗基形容是貝卡帕庫博士昔日夢想的新兵器，身上標示著代表草帽海賊團的圖案以及「BF
        38」的字樣，就連頭部也都採用佛朗基的飛機頭髮型而加裝大型鑽子，如同大鐵球般的肩膀個別標上星星的圖案，雙臂前段寫著「FR-
        U」，手部則是由坦克車的車輪組裝而成，背後則附有一把名為掠奪者之劍的「佛朗劍」。操控者主要待在機械人的體內（胸腔），負責操控機械人的戰鬥動作，此機械人的堅固程度，甚至足以擋下火箭砲的攻擊。其作戰技能詳參[佛朗基](../Page/佛朗基.md "wikilink")。

## 參考資料

<div class="references-small">

<references />

## 相關項目

[Category:草帽海賊團](../Category/草帽海賊團.md "wikilink") [Category:ONE
PIECE](../Category/ONE_PIECE.md "wikilink")
[Category:虛構船艦](../Category/虛構船艦.md "wikilink")

1.  《ONE PIECE》漫畫第 699 話
2.  《ONE PIECE》漫畫第495話。
3.  《ONE PIECE》動畫第 337 話