[NewTux.svg](https://zh.wikipedia.org/wiki/File:NewTux.svg "fig:NewTux.svg")

**Tux**（一只企鹅）是[Linux的标志](../Page/Linux.md "wikilink")。将[企鹅作为Linux标志是由](../Page/企鹅.md "wikilink")[林納斯·托瓦茲提出的](../Page/林納斯·托瓦茲.md "wikilink")。

大多数人相信，“Tux”这个名字来源于**T**orvalds
**U**ni**X**，而不是因为它看起来像是穿着一件黑色小礼服（**tux**edo）。

这个企鹅图案在最佳Linux图标竞赛中被选中。其他一些图案可以在[Linux图标大赛网站](http://www.cs.earlham.edu/~jeremiah/linux-pix/linux-logo.html)中找到。Tux的设计者是[Larry
Ewing](../Page/Larry_Ewing.md "wikilink")，他于1996年，利用[GIMP软件设计出了这个企鹅](../Page/GIMP.md "wikilink")，并在以下条例下发布：

  -
    Permission to use and/or modify this image is granted provided you
    acknowledge me lewing@isc.tamu.edu and The GIMP if someone asks.

Tux已经成为Linux和[开源社群的象徵](../Page/开放源代码.md "wikilink")。英国Linux用户组（British
LUG）甚至在当地的动物园認养了几只企鹅。

## Tuz

[Tuz-logo.png](https://zh.wikipedia.org/wiki/File:Tuz-logo.png "fig:Tuz-logo.png")

在 2.6.29 系列的Linux核心中，为支持**拯救[袋獾运动](../Page/袋獾.md "wikilink")**Tux
被临时地替换成 [Tuz](../Page/Tuz.md "wikilink")。

## 其他版本

Image:Crystal_128_penguin.png|Tux
[Crystal](../Page/Commons:Category:Crystal_icons.md "wikilink") 第1版
Image:TUX-G2-SVG.svg|Tux
[Crystal](../Page/Commons:Category:Crystal_icons.md "wikilink") 第2版
Image:Pax tux.png|[PaX版本的Tux](../Page/PaX.md "wikilink")
Image:Tux.svg|高質矢量的Tux Image:Tux-simple.svg|普通矢量的Tux
Image:NewTux.svg|晶莹的Tux

## 参见

  - [Linux](../Page/Linux.md "wikilink")
  - [超级企鹅（游戏）](../Page/超级企鹅.md "wikilink")

[Category:Linux](../Category/Linux.md "wikilink")
[Category:電腦吉祥物](../Category/電腦吉祥物.md "wikilink")
[Category:鳥類吉祥物](../Category/鳥類吉祥物.md "wikilink")
[Category:虛構企鵝](../Category/虛構企鵝.md "wikilink")