**谷精草科**（拉丁語：*Eriocaulaceae*）包括10[属约](../Page/属.md "wikilink")1,150-1,200[种](../Page/种.md "wikilink")，分布在[热带和](../Page/热带.md "wikilink")[亚热带地区](../Page/亚热带.md "wikilink")，也有部分种类生长在[温带地区](../Page/温带.md "wikilink")，以[美洲种类最多](../Page/美洲.md "wikilink")。[中国只有](../Page/中国.md "wikilink")1属约40种，分布在除西北外的全国各地，以南方最多。

本科[植物多为多年生](../Page/植物.md "wikilink")[草本](../Page/草本.md "wikilink")，也有部分为一年生，[叶狭窄](../Page/叶.md "wikilink")，丛生；小[花](../Page/花.md "wikilink")，单性，辐射对称，雄花和雌花混生，风媒；[果实为膜质蒴果](../Page/果实.md "wikilink")。喜湿，一般生长在[水分充足的](../Page/水.md "wikilink")[土壤中](../Page/土壤.md "wikilink")，有部分种类为水草。

1981年的[克朗奎斯特分类法单独分出一个单科](../Page/克朗奎斯特分类法.md "wikilink")[谷精草目](../Page/谷精草目.md "wikilink")，1998年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
分类法将本](../Page/APG_分类法.md "wikilink")[科列入](../Page/科.md "wikilink")[禾本目](../Page/禾本目.md "wikilink")。

  - 属

<!-- end list -->

  - *[Blastocaulon](../Page/Blastocaulon.md "wikilink")*
  - [谷精草属](../Page/谷精草属.md "wikilink") *Eriocaulon*
  - *[Lachnocaulon](../Page/Lachnocaulon.md "wikilink")*
  - *[Leiothrix](../Page/Leiothrix.md "wikilink")*
  - [非洲谷精属](../Page/非洲谷精属.md "wikilink") *Mesanthemum*
  - [食蟲榖精屬](../Page/食蟲榖精屬.md "wikilink") *Paepalanthus*
  - *[Philodice](../Page/Philodice.md "wikilink")*
  - *[Rhodonanthus](../Page/Rhodonanthus.md "wikilink")*
  - [南美谷精属](../Page/南美谷精属.md "wikilink") *Syngonanthus*
  - [太阳草属](../Page/太阳草属.md "wikilink") *Tonina*

## 外部链接

  - 在[L. Watson和M.J. Dallwitz
    (1992年)《有花植物分科》](http://delta-intkey.com/angio/)中的[谷精草科](http://delta-intkey.com/angio/www/eriocaul.htm)

[\*](../Category/谷精草科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")