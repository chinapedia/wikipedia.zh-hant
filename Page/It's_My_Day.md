《**It's My
Day**》是[薛凱琪的首張](../Page/薛凱琪.md "wikilink")[國語專輯](../Page/國語.md "wikilink")，於2008年4月18日推出，主打歌為《愛麗絲的第一次》，以及第二主打《我不需要Tiffany》。此外，碟內其中一曲《It's
My
Day》更由[Zarahn成員](../Page/Zarahn.md "wikilink")[Goro作曲](../Page/Goro.md "wikilink")。而《復刻回憶》則是改編自樂壇巨星[張學友的經典粵語流行歌曲](../Page/張學友.md "wikilink")[每天愛你多一些](../Page/每天愛你多一些.md "wikilink")，由[薛凱琪與](../Page/薛凱琪.md "wikilink")[方大同合唱](../Page/方大同.md "wikilink")。

## 曲目

| 次序   | 歌名          | 作曲                                 | 填詞                               | 編曲          | 監製                               |
| ---- | ----------- | ---------------------------------- | -------------------------------- | ----------- | -------------------------------- |
| 1\.  | It's My Day | Goro Wong                          | 劉偉恩／范逸敏                          | 郭偉聰         | 陳偉                               |
| 2\.  | 黑色淚滴        | 王維皓(COLOR)                         | 范逸敏                              | 陳偉          | 陳偉                               |
| 3\.  | 一個人失憶       | [李偲菘](../Page/李偲菘.md "wikilink")   | 劉偉恩                              | Terence Teo | [李偲菘](../Page/李偲菘.md "wikilink") |
| 4\.  | 復刻回憶        | [桑田佳祐](../Page/桑田佳祐.md "wikilink") | 易家揚                              | 阿滾          | 陳偉                               |
| 5\.  | 愛麗絲的第一次     | 陳科妤                                | 徐世珍                              | 陳科妤         | 馬毓芬                              |
| 6\.  | 我不需要Tiffany | [方大同](../Page/方大同.md "wikilink")   | [周耀輝](../Page/周耀輝.md "wikilink") | Terence Teo | [李偲菘](../Page/李偲菘.md "wikilink") |
| 7\.  | 半路          | 阿火                                 | 易家揚                              | Terence Teo | [李偲菘](../Page/李偲菘.md "wikilink") |
| 8\.  | 找到天使了       | 劉永輝                                | 姚若龍                              | Randy       | 馬毓芬                              |
| 9\.  | Red is...   | Jung Yeon Tae / Michy              | [游思行](../Page/游思行.md "wikilink") | Derek Chua  | [李偲菘](../Page/李偲菘.md "wikilink") |
| 10\. | 新不了情        | 梁翹柏                                | [李焯雄](../Page/李焯雄.md "wikilink") | 梁翹柏         | 梁翹柏                              |

## 曲目資料

  - 《新不了情》為[無線電視劇](../Page/無線.md "wikilink")[新不了情插曲](../Page/新不了情_\(2008年電視劇\).md "wikilink")，非翻唱[萬芳的同名歌曲](../Page/萬芳.md "wikilink")，只是同名異曲。
  - 《復刻回憶》由[薛凱琪與](../Page/薛凱琪.md "wikilink")[方大同合唱](../Page/方大同.md "wikilink")，原曲為[桑田佳祐的](../Page/桑田佳祐.md "wikilink")《[真夏的果實](../Page/真夏的果實.md "wikilink")》（[南方之星歌曲](../Page/南方之星.md "wikilink")）。
  - 《我不需要Tiffany》為2008年冠軍歌曲、及香港第二主打歌曲。
  - 《It's my day》為台灣第一主打歌曲。
  - 《愛麗斯的第一次》為香港第一主打歌曲。

[Category:薛凱琪音樂專輯](../Category/薛凱琪音樂專輯.md "wikilink")