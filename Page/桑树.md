[桑叶.jpg](https://zh.wikipedia.org/wiki/File:桑叶.jpg "fig:桑叶.jpg")
**桑树**，又稱**娘仔樹**（）\[1\]。有许多种，有[乔木也有灌木](../Page/乔木.md "wikilink")，有“[华桑](../Page/华桑.md "wikilink")”、“[白桑](../Page/白桑.md "wikilink")”、“[鸡桑](../Page/鸡桑.md "wikilink")”等。其果实名为[桑葚](../Page/桑葚.md "wikilink")。

## 主要用途

  - 樹皮、樹根：可入中藥，稱作**桑白皮**、**蜜桑白皮**，亦可當成食用菇菌菌絲載體，早期用以製紙。（如[桑寄生](../Page/桑寄生.md "wikilink")）
  - 葉：可入藥，但主用途是餵養[蠶](../Page/蠶.md "wikilink")（與[柞樹一樣](../Page/柞樹.md "wikilink")。）
  - 果實（[桑葚](../Page/桑葚.md "wikilink")）：可食用、榨汁用、釀酒用，亦可乾燥後搗末入藥。

[_Morus_alba_MHNT.BOT.2006.0.1270.JPG](https://zh.wikipedia.org/wiki/File:_Morus_alba_MHNT.BOT.2006.0.1270.JPG "fig:_Morus_alba_MHNT.BOT.2006.0.1270.JPG")

## 物種

桑屬物種分類的爭議比較大，現時已出版的文獻裡，劃為桑屬的植物超過150種，儘管當中有不少不同的命名很可能是指同一個物種植物。在這百多個物種的命名當中，只有10到16個是得到普遍認同的。以下為現時普遍認同屬於桑屬的物種：

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><em><a href="../Page/Morus_alba.md" title="wikilink">Morus alba</a></em> <small><a href="../Page/Carl_Linnaeus.md" title="wikilink">L.</a></small> – White Mulberry <font size=1>(E Asia)</font></li>
<li><em><a href="../Page/Morus_australis.md" title="wikilink">Morus australis</a></em> <small><a href="../Page/Jean_Louis_Marie_Poiret.md" title="wikilink">Poir.</a></small> – Chinese Mulberry <font size=1>(SE Asia)</font></li>
<li><em><a href="../Page/Morus_celtidifolia.md" title="wikilink">Morus celtidifolia</a></em> <small><a href="../Page/Carl_Sigismund_Kunth.md" title="wikilink">Kunth</a></small> <font size=1>(Mexico)</font></li>
<li><em><a href="../Page/Morus_insignis.md" title="wikilink">Morus insignis</a></em> <font size=1>(S America)</font></li>
<li><em><a href="../Page/Morus_mesozygia.md" title="wikilink">Morus mesozygia</a></em> <small><a href="../Page/Otto_Stapf.md" title="wikilink">Stapf</a></small> – African Mulberry <font size=1>(S and C Africa)</font></li>
<li><em><a href="../Page/Morus_macroura.md" title="wikilink">Morus macroura</a></em> - Long mulberry</li>
<li><em><a href="../Page/Morus_microphylla.md" title="wikilink">Morus microphylla</a></em> – Texas Mulberry <font size=1>(Mexico, Texas (USA))</font></li>
<li><em><a href="../Page/Morus_nigra.md" title="wikilink">Morus nigra</a></em> <small>L.</small> – Black Mulberry <font size=1>(SW Asia)</font></li>
<li><em><a href="../Page/Morus_rubra.md" title="wikilink">Morus rubra</a></em> <small>L.</small> – Red Mulberry <font size=1>(E N America)</font></li>
</ul></td>
<td></td>
</tr>
</tbody>
</table>

The following, all from eastern and southern Asia, are additionally
accepted by one or more taxonomic lists or studies;
[synonymy](../Page/synonymy.md "wikilink"), as given by other lists or
studies, is indicated in square brackets:

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><em>Morus atropurpurea</em></li>
<li><em>Morus bombycis</em> [<em>M. australis</em>]</li>
<li><em>Morus cathayana</em></li>
<li><em>Morus indica</em> [<em>M. alba</em>]</li>
<li><em>Morus japonica</em> [<em>M. alba</em>]</li>
<li><em>Morus kagayamae</em> [<em>M. australis</em>]</li>
<li><em>Morus laevigata</em> [<em>M. alba</em> var. <em>laevigata; M. macroura</em>]</li>
<li><em>Morus latifolia</em> [<em>M. alba</em>]</li>
<li><em>Morus liboensis</em></li>
</ul></td>
<td><ul>
<li><em>Morus macroura</em> [<em>M. alba</em> var. <em>laevigata</em>]</li>
<li><em>Morus mongolica</em> [<em>M. alba</em> var. <em>mongolica</em>]</li>
<li><em>Morus multicaulis</em> [<em>M. alba</em>]</li>
<li><em>Morus notabilis</em></li>
<li><em>Morus rotundiloba</em></li>
<li><em><a href="../Page/Morus_serrata.md" title="wikilink">Morus serrata</a></em> [<em>M. alba</em> var. <em>serrata</em>], Himalayan mulberry</li>
<li><em>Morus tillaefolia</em></li>
<li><em>Morus trilobata</em> [<em>M. australis</em> var. trilobata'']</li>
<li><em>Morus wittiorum</em></li>
</ul></td>
<td></td>
</tr>
</tbody>
</table>

## 參考資料

## 外部連結

  - [桑
    Sang](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D00547)
    藥用植物圖像數據庫 (香港浸會大學中醫藥學院)
  - [桑枝
    Sangzhi](http://libproject.hkbu.edu.hk/was40/detail?channelid=47953&searchword=pid=B00146)
    中藥材圖像數據庫 (香港浸會大學中醫藥學院)
  - [桑葉
    Sangye](http://libproject.hkbu.edu.hk/was40/detail?channelid=47953&searchword=pid=B00334)
    中藥材圖像數據庫 (香港浸會大學中醫藥學院)
  - [桑白皮
    Sangbaipi](http://libproject.hkbu.edu.hk/was40/detail?channelid=47953&searchword=pid=B00352)
    中藥材圖像數據庫 (香港浸會大學中醫藥學院)
  - [桑葉 Sang
    Ye](http://libproject.hkbu.edu.hk/was40/detail?channelid=44273&searchword=herb_id=D00395)
    中藥標本數據庫 (香港浸會大學中醫藥學院)
  - [桑白皮 Sang Bai
    Pi](http://libproject.hkbu.edu.hk/was40/detail?channelid=44273&searchword=herb_id=D00592)
    中藥標本數據庫 (香港浸會大學中醫藥學院)
  - [葉綠醇
    Phytol](http://libproject.hkbu.edu.hk/was40/detail?channelid=22253&lang=cht&searchword=code=P00619)
    中草藥化學圖像數據庫 (香港浸會大學中醫藥學院)

[樹](../Category/藥用植物.md "wikilink")
[Category:桑属](../Category/桑属.md "wikilink")
[Category:卡尔·林奈命名的生物分类](../Category/卡尔·林奈命名的生物分类.md "wikilink")

1.  [娘仔樹－臺灣閩南語常用詞辭典](http://twblg.dict.edu.tw/holodict_new/result_detail.jsp?n_no=5889&source=9&level1=4&level2=15&level3=0&curpage=0&sample=0&radiobutton=0&querytarget=0&limit=1&pagenum=0&rowcount=0&cattype=1)