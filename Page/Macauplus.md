**Macauplus（簡稱MPLUS）**是[澳門的一個](../Page/澳門.md "wikilink")[互聯網站](../Page/網上討論區.md "wikilink")，於2002年由幾個澳門中學生成立；現在停止運作。
[Mplus_logo.jpg](https://zh.wikipedia.org/wiki/File:Mplus_logo.jpg "fig:Mplus_logo.jpg")

## 網站歷史

2002年初，論壇程式[phpBB十分流行](../Page/phpBB.md "wikilink")。創辦人之一的CCUBE製作了一個可以整合phpBB的網站管理程式，取名SITEngine。在當年這個程式是除[PHP-Nuke和某些phpBB插件外唯一可以整合phpBB系統的中文網站程式](../Page/PHP-Nuke.md "wikilink")。2003年，Macauplus的前身Macauweb.net成立，Macauweb.net的中文名稱是「。咩」，是受當年很流行的[微軟](../Page/微軟.md "wikilink")「[.NET](../Page/.NET.md "wikilink")」和[蘋果電腦](../Page/蘋果電腦.md "wikilink")「[.mac](../Page/.mac.md "wikilink")」影響而得名的。不久網站迅速發展，原有的伺服器不勝負荷而故障，令網站被迫暫停近一個月。其後，Macauweb.net改名為Macauplus，也租用了新主機，連結速度有所提升。2004年，網站推出多項會員服務，包括網站登錄、免費短域名和個人新聞台等服務，其中個人新聞台服務大受好評，不久於[澳門日報電腦版上刊登](../Page/澳門日報.md "wikilink")\[1\]。2005年中旬，網站的討論區遭[黑客入侵](../Page/黑客.md "wikilink")，最後轉用安全性較高的商業版[Invision
Power Board取代原來的phpBB](../Page/Invision_Power_Board.md "wikilink")。

2007年4月，Macauplus的域名由「macauplus.com」改成「macauplus.net」。

## 網站特色

### 節日特別版面

Macauplus是澳門首個在不同節日推出特別版設計的網站，如[聖誕節和](../Page/聖誕節.md "wikilink")[農曆新年甚至](../Page/農曆新年.md "wikilink")[黑色星期五也會有不同的設計](../Page/黑色星期五.md "wikilink")。

### 網站設計

Macauplus也是澳門首個使用[PNG透明技術設計版面的網站](../Page/PNG.md "wikilink")。在沒有這項技術前，網站設計師要使用透明底色的效果，非使用GIF不可，但[GIF只有](../Page/GIF.md "wikilink")256色，不能製造出華麗的效果。現在PNG透明技術令設計師解決這個問題，他們可以自由地在網頁使用陰影和光暈，達到理想的效果。但使用這項技術有一缺點，因為微軟的[Internet
Explorer不支援這項技術](../Page/Internet_Explorer.md "wikilink")，故使用IE瀏覽時有機會發生錯誤。

### CCOUNT人數計

CCOUNT是Macauplus自行研發的網站人數計程式，其功能除了包括每日每月的人流外，還附有一整頁的詳細人流資料，大受港澳及[台灣人士的歡迎](../Page/台灣.md "wikilink")。

### 學術性教學

Macauplus間中也會推出一些學術性的教學。如[攝影教學和](../Page/攝影.md "wikilink")[Photoshop教學等](../Page/Photoshop.md "wikilink")。

### 新聞台

Macauplus是澳門首個自創新聞台的網站，曾一度吸引大量會員申請。新聞台多以日記使用，或是用來寫下心情小記。可惜由於Yahoo\!Blog、[WordPress等網誌的興起](../Page/WordPress.md "wikilink")，以及Macauplus新聞台的漏洞相繼被發現，結果現在只剩下小部分的會員仍在使用。

## 參與社會活動

[Web_contest.jpg](https://zh.wikipedia.org/wiki/File:Web_contest.jpg "fig:Web_contest.jpg")
2005年，Macauplus協辦「穗澳青委網頁設計比賽」。是次活動由[澳門廣州地區聯誼會青年委員會主辦](../Page/澳門廣州地區聯誼會青年委員會.md "wikilink")，[澳門教育暨青年局](../Page/澳門教育暨青年局.md "wikilink")、[澳門民政總署及Dreamsky](../Page/澳門民政總署.md "wikilink")
Network Services贊助\[2\]。

2006年，Macauplus贊助[澳門旅遊學院攝影比賽](../Page/澳門旅遊學院.md "wikilink")。是次由澳門旅遊學院學生會資訊科技社主辦，[澳門攝影學會協辦](../Page/澳門攝影學會.md "wikilink")，[澳門教育暨青年局](../Page/澳門教育暨青年局.md "wikilink")、[澳門霍英東基金會](../Page/澳門霍英東基金會.md "wikilink")、MACAUPLUS、怡和科技有限公司和澳門旅遊學院贊助\[3\]。

## 管理問題

Macauplus雖然人流量不大，但一直存在管理上的問題。管理員不常處理網站事務，令討論區出現大量的廣告文章，影響用戶瀏覽。

## 參看

  - [澳門媒體](../Page/澳門媒體.md "wikilink")
  - [澳門互聯網站](../Page/Qoos.md "wikilink")
  - [Invision Power Board](../Page/Invision_Power_Board.md "wikilink")
  - [phpBB](../Page/phpBB.md "wikilink")

## 外部連結

  - [MPLUS首頁](http://www.macauplus.net)

## 参考文献

<div class="references-small">

<references />

</div>

[Category:澳門網站](../Category/澳門網站.md "wikilink")

1.  [馬交網賞曲](http://www.macauplus.net/images/macauplus/newspaper2.jpg) -
    [澳門日報剪報](../Page/澳門日報.md "wikilink") 2004-02
2.  [穗澳青委舉辦網頁設計比賽](http://www.macauart.net/News/ContentC.asp?id=9110)  -
    澳門藝術網 2005-04-23
3.  [IFT Newsletter 39th
    Issue](http://www.ift.edu.mo/publication/letters/no39.pdf)  -
    Instituto de Formação Turística
    [澳門旅遊學院](../Page/澳門旅遊學院.md "wikilink")
    2006-09