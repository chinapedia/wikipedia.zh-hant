**菱刈町**（）是位於[日本](../Page/日本.md "wikilink")[鹿兒島縣北部的一個](../Page/鹿兒島縣.md "wikilink")[城鎮](../Page/城鎮.md "wikilink")。已於2008年11月1日與[大口市合併成](../Page/大口市.md "wikilink")[伊佐市](../Page/伊佐市.md "wikilink")。

日本最大的金礦[菱刈礦山位於轄區內](../Page/菱刈礦山.md "wikilink")。

## 歷史

### 年表

  - 1889年4月1日：實施[町村制](../Page/町村制.md "wikilink")，現在的轄區在當時分屬菱刈郡菱刈村（原[菱刈郡前目村](../Page/菱刈郡.md "wikilink")、田中村、川北村、德邊村、重留村、下手村、[北伊佐郡市山村](../Page/北伊佐郡.md "wikilink")、花北村合併而成）和太良村。
  - 1891年8月：太良村分割成[東太良村和西太良村](../Page/東太良村.md "wikilink")（現已合併為[大口市](../Page/大口市.md "wikilink")）。
  - 1896年3月29日：改隸屬伊佐郡。
  - 1925年2月11日：東太良村改名為[本城村](../Page/本城村.md "wikilink")。
  - 1940年4月29日：改制為菱刈町。
  - 1954年7月15日：菱刈村與本城村合併為新設置的**菱刈町**。
  - 2008年11月1日：預定與[大口市合併為](../Page/大口市.md "wikilink")[伊佐市](../Page/伊佐市.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>1889年4月1日</p></th>
<th><p>1889年-1910年</p></th>
<th><p>1910年-1930年</p></th>
<th><p>1930年-1950年</p></th>
<th><p>1950年-1970年</p></th>
<th><p>1970年-1990年</p></th>
<th><p>1990年-現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>北伊佐郡<br />
大口村</p></td>
<td><p>1896年3月29<br />
伊佐郡大口村</p></td>
<td><p>1918年4月1日<br />
改制為大口町</p></td>
<td><p>1954年4月1日<br />
合併為大口市</p></td>
<td><p>2008年11月1日<br />
合併為伊佐市</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>北伊佐郡<br />
山野村</p></td>
<td><p>1896年3月29<br />
伊佐郡山野村</p></td>
<td><p>1940年11月10日<br />
改制為山野町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>北伊佐郡<br />
羽月村</p></td>
<td><p>1896年3月29<br />
伊佐郡羽月村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>菱刈郡<br />
太良村</p></td>
<td><p>1896年3月29<br />
伊佐郡西太良村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1896年3月29<br />
伊佐郡東太良村</p></td>
<td><p>1925年2月11日<br />
改名為本城村</p></td>
<td><p>1954年7月15日<br />
合併為菱刈町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>菱刈郡<br />
菱刈村</p></td>
<td><p>1896年3月29<br />
伊佐郡菱刈村</p></td>
<td><p>1940年4月29日<br />
改制為菱刈町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 交通

### 鐵路

目前轄區內無鐵路通過，過去曾有[國鐵](../Page/國鐵.md "wikilink")[山野線通過](../Page/山野線.md "wikilink")，但已於1988年停駛。

  - 國鐵
      - 山野線：[西菱刈車站](../Page/西菱刈車站.md "wikilink") -
        [菱刈車站](../Page/菱刈車站.md "wikilink") -
        [前目車站](../Page/前目車站.md "wikilink") -
        [湯之尾車站](../Page/湯之尾車站.md "wikilink")

## 觀光資源

  - 湯之尾溫泉
  - 湯之尾瀑布
  - 菱刈鐵道記念公園（舊國鐵山野線菱刈車站）
  - 箱崎神社

## 姊妹、友好都市

### 日本

  - [西之表市](../Page/西之表市.md "wikilink")（鹿兒島縣）：於1962年11月10日締結姊妹都市

## 本地出身之名人

  - [榎木孝明](../Page/榎木孝明.md "wikilink")：[俳優](../Page/俳優.md "wikilink")

## 外部連結

  - [菱刈觀光特產協會](http://seitengai.com/hishikari/)

  - [菱刈町商工會](http://hishikari.kashoren.or.jp/)

  - [伊佐地區合併協議會](http://isa-gappei.jp/)

## 參考資料

<div class="references-small">

<references />

</div>

[Category:伊佐郡](../Category/伊佐郡.md "wikilink")
[Category:伊佐市](../Category/伊佐市.md "wikilink")