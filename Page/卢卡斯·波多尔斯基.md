**卢卡斯·約瑟夫·波多尔斯基**（，，），暱稱為**波弟**（），是一名[德國職業足球員](../Page/德國.md "wikilink")，現在是[日本甲組職業足球聯賽球隊](../Page/日本甲組職業足球聯賽.md "wikilink")[神戶勝利船的隊長](../Page/神戶勝利船.md "wikilink")。他在球員生涯中主要司職[前鋒或](../Page/前鋒.md "wikilink")[左翼鋒](../Page/中场.md "wikilink")。普多斯基被視為德國其中一位最佳的球員，終結能力高。他的左腳射門既準且強，在禁區邊緣即有進攻能力，在球場左路的威脅大\[1\]。

1995年，他加入[科隆的青年軍](../Page/科隆足球俱乐部.md "wikilink")，並於2003年晉升至一線隊，為球隊上陣81場。2006年，他轉會至[拜仁慕尼黑](../Page/拜仁慕尼黑足球俱乐部.md "wikilink")，於2007-08賽季贏得[德國甲組足球聯賽和](../Page/德國甲組足球聯賽.md "wikilink")[德國足協盃冠軍](../Page/德國足協盃.md "wikilink")。2009年，普多斯基重返科隆，於3年後，他轉會至[英格蘭超級足球聯賽球會](../Page/英格蘭超級足球聯賽.md "wikilink")[阿仙奴](../Page/阿仙奴足球會.md "wikilink")，於[2013-14賽季贏得](../Page/2013/14球季英格蘭足總盃.md "wikilink")[英格蘭足總盃](../Page/英格蘭足總盃.md "wikilink")。2015年1月，他被外借至[意大利甲組足球聯賽球會](../Page/意大利甲組足球聯賽.md "wikilink")[國際米蘭](../Page/國際米蘭.md "wikilink")，半年後他轉會至[土耳其超級足球聯賽球會](../Page/土耳其超級足球聯賽.md "wikilink")[加拉塔沙雷](../Page/加拉塔沙雷體育會.md "wikilink")\[2\]。

在國家隊方面，普多斯基有資格代表[德國國家足球隊或](../Page/德國國家足球隊.md "wikilink")[波蘭國家足球隊上陣](../Page/波蘭國家足球隊.md "wikilink")，他最初在2003年曾試圖自薦加入波蘭隊，但被當時的波蘭隊教練拒絕，他最終代表德國隊出戰。普多斯基的國家隊生涯成功，並成為其中一位國家英雄。2004年，他首次為國家隊上陣。自此，普多斯基參與所有大賽直到退出國家隊，並贏得[2014年世界盃](../Page/2014年世界盃.md "wikilink")。他為國家隊上陣了130場，成為代表德國隊上陣第3多的球員；他亦為國家隊攻入49球，亦是為德國隊進球第3多的球員。

2013年5月29日，普多斯基於對陣[厄瓜多爾國家足球隊時](../Page/厄瓜多爾國家足球隊.md "wikilink")，開賽9秒後為德國隊攻入1球。此入球為德國隊歷史上最快的入球，於世界紀錄方面，僅次於[聖馬力諾國家足球隊球員](../Page/聖馬力諾國家足球隊.md "wikilink")對陣[英格蘭國家足球隊時創造的](../Page/英格蘭國家足球隊.md "wikilink")8.3秒。2017年3月22日，普多斯基復出在友誼賽對陣英格蘭隊後宣佈結束國家隊生涯\[3\]\[4\]。

## 早年

1987年，波多尔斯基隨其父母遷居德國。他擁有波蘭和德國雙重公民權，但沒有申请过波蘭護照\[5\]，2003年試圖加入波蘭隊被拒之後長期為德國隊效力。

## 球會生涯

### 科隆

2003年，18歲的波多尔斯基仍然是球會青年隊的一分子。当时，球會正掙扎在由[德甲降班到](../Page/德甲.md "wikilink")[德乙的邊緣](../Page/德乙.md "wikilink")。一隊的領隊马塞尔·科勒尔在缺少資金的情况下，決定給予這位年輕前鋒一次機會，看他的表现是否值得升上一隊。普多斯基被邀請參與一隊球員的練習。

儘管科隆降級至德乙，波多尔斯基仍繼續留在球會，并射入24球協助球隊再次返回甲組。而他亦是在1975年後第一位獲選入國家隊的德乙球員，並於2004年6月6日在對[匈牙利的比賽中以後備入替的首次上陣](../Page/匈牙利國家足球隊.md "wikilink")。他在[2004年歐洲國家盃中為](../Page/2004年歐洲國家盃.md "wikilink")[德國效力](../Page/德國國家足球隊.md "wikilink")，亦是队中最年輕的球員，他亦參加了[FIFA](../Page/FIFA.md "wikilink")2005年的[洲際國家盃](../Page/洲際國家盃.md "wikilink")，並射入了3球。

### 拜仁慕尼黑

德國教練[克林斯曼高度稱讚他对对手致命的頭鎚和射門](../Page/尤尔根·克林斯曼.md "wikilink")，并且他还十分年輕。正因如此，德國班霸拜仁慕尼黑於2006年世界盃开幕前正式收購波多斯基，雖然他跟科隆的合約直至2007年才完結。

在2006年6月1日，拜仁正式收購波多尔斯基。他代表拜仁出場的首場德甲比賽是在2006年8月11日，拜仁以2-0擊敗[多蒙特](../Page/多蒙特.md "wikilink")。然而隨著剛拿下世界杯冠軍的義大利明星前鋒[托尼和身為該屆世界盃金靴得主的國家隊前輩](../Page/卢卡·托尼.md "wikilink")[克洛澤相繼加入拜仁](../Page/米羅斯拉夫·克洛澤.md "wikilink")，波多尔斯基在拜仁未能坐上主力位置，因而萌生去意。

### 重返科隆

在2009年1月，波多尔斯基確定將回到科隆，并將在2009年7月1日正式加盟科隆\[6\]。

### 阿仙奴

#### 2012/13赛季

2012年4月30日，[科隆足球俱乐部和](../Page/科隆足球俱乐部.md "wikilink")[阿仙奴官网宣布](../Page/阿仙奴.md "wikilink")，波多尔斯基将于2012/2013赛季正式加盟阿仙奴，这是阿仙奴主教练[雲加提前敲定的普多斯基加盟阿仙奴的转会](../Page/雲加.md "wikilink")，波多尔斯基披上9號球衣，但是转会费和合约期均未透露\[7\]，轉會費為1080萬鎊。

#### 2014/15赛季

自2012年從科隆加盟阿仙奴以來，普多斯基一直未能夠成為球隊常規正選，2014/15赛季開季至今，普多斯基未有得到足夠上陣機會，他亦多次對此公開表示不滿。

2015年1月5日，普多斯基被阿仙奴外借至[意甲球會](../Page/意甲.md "wikilink")[國際米蘭](../Page/國際米蘭.md "wikilink")，借用期至球季結束\[8\]。

### 加拉塔萨雷

### 神户胜利船

## 國家隊生涯

后波多尔斯基被列入德國的名單內，並跟[克洛澤成為拍檔](../Page/米罗斯拉夫·克洛泽.md "wikilink")，在分組中對抗[哥斯達黎加](../Page/哥斯達黎加國家足球隊.md "wikilink")、[厄瓜多爾和](../Page/厄瓜多爾國家足球隊.md "wikilink")[波蘭](../Page/波蘭國家足球隊.md "wikilink")，最後紛紛擊敗一眾勁旅，但可惜四強階段不敵[意大利而止步四强](../Page/意大利國家足球隊.md "wikilink")，但憑著他的精湛射術以及個人取得三個入球的优秀成绩獲得賽事首次舉辦的最佳年輕球員称号，而德國則成為第三季軍東道主，僅次於法國和意大利。

[2008年歐洲國家盃首場比賽中](../Page/2008年歐洲國家盃.md "wikilink")，波多尔斯基面對自己的出生國波蘭獨進兩球，幫助德國隊2：0戰勝對手。但是他在進球後卻沒有任何慶祝動作，只是將右手放在胸口。

### 國際賽入球

<table>
<tbody>
<tr class="odd">
<td><p><strong>#</strong></p></td>
<td><p><strong>日期</strong></p></td>
<td><p><strong>地点</strong></p></td>
<td><p><strong>对手</strong></p></td>
<td><p><strong>进球</strong></p></td>
<td><p><strong>比分结果</strong></p></td>
<td><p><strong>比赛性质</strong></p></td>
</tr>
<tr class="even">
<td><p>1.</p></td>
<td><p>2004年12月21日</p></td>
<td><p><a href="../Page/拉加曼加拉體育場.md" title="wikilink">拉加曼加拉體育場</a>，<a href="../Page/曼谷.md" title="wikilink">曼谷</a>，<a href="../Page/泰國.md" title="wikilink">泰國</a></p></td>
<td></td>
<td><p>3–1</p></td>
<td><p>5–1</p></td>
<td><p><a href="../Page/:en:Exhibition_game.md" title="wikilink">友誼賽</a></p></td>
</tr>
<tr class="odd">
<td><p>2.</p></td>
<td><p>2004年12月21日</p></td>
<td><p>拉加曼加拉體育場，曼谷，泰國</p></td>
<td></td>
<td><p>5–1</p></td>
<td><p>5–1</p></td>
<td><p>友誼賽</p></td>
</tr>
<tr class="even">
<td><p>3.</p></td>
<td><p>2005年3月26日</p></td>
<td><p><a href="../Page/:en:Bežigrad_Stadium.md" title="wikilink">Bežigrad Stadium</a>，<a href="../Page/盧布爾雅那.md" title="wikilink">盧布爾雅那</a>，<a href="../Page/斯洛文尼亞.md" title="wikilink">斯洛文尼亞</a></p></td>
<td></td>
<td><p>1–0</p></td>
<td><p>1–0</p></td>
<td><p>友誼賽</p></td>
</tr>
<tr class="odd">
<td><p>4.</p></td>
<td><p>2005年6月4日</p></td>
<td><p><a href="../Page/:en:Windsor_Park.md" title="wikilink">Windsor Park</a>，<a href="../Page/貝爾法斯特.md" title="wikilink">貝爾法斯特</a>，<a href="../Page/北愛爾蘭.md" title="wikilink">北愛爾蘭</a></p></td>
<td><p><a href="../Page/北愛爾蘭.md" title="wikilink">北愛爾蘭</a></p></td>
<td><p>4–1</p></td>
<td><p>4–1</p></td>
<td><p>友誼賽</p></td>
</tr>
<tr class="even">
<td><p>5.</p></td>
<td><p>2005年6月15日</p></td>
<td><p><a href="../Page/商業銀行球場.md" title="wikilink">商業銀行球場</a>，<a href="../Page/法蘭克福.md" title="wikilink">法蘭克福</a>，<a href="../Page/德國.md" title="wikilink">德國</a></p></td>
<td></td>
<td><p>4–2</p></td>
<td><p>4–3</p></td>
<td><p><a href="../Page/FIFA.md" title="wikilink">FIFA</a><a href="../Page/2005年洲際國家盃.md" title="wikilink">2005年洲際國家盃</a></p></td>
</tr>
<tr class="odd">
<td><p>6.</p></td>
<td><p>2005年6月25日</p></td>
<td><p><a href="../Page/法蘭克人體育場.md" title="wikilink">法蘭克人體育場</a>，<a href="../Page/紐倫堡.md" title="wikilink">紐倫堡</a>，德國</p></td>
<td></td>
<td><p>1–1</p></td>
<td><p>2–3</p></td>
<td><p>FIFA2005年洲際國家盃</p></td>
</tr>
<tr class="even">
<td><p>7.</p></td>
<td><p>2005年6月29日</p></td>
<td><p><a href="../Page/中央體育場.md" title="wikilink">中央體育場</a>，<a href="../Page/萊比錫.md" title="wikilink">萊比錫</a>，德國</p></td>
<td></td>
<td><p>1–0</p></td>
<td><p>4–3</p></td>
<td><p>FIFA2005年洲際國家盃</p></td>
</tr>
<tr class="odd">
<td><p>8.</p></td>
<td><p>2005年9月7日</p></td>
<td><p><a href="../Page/:en:Olympiastadion_Berlin.md" title="wikilink">奧林匹克體育場</a>，<a href="../Page/柏林.md" title="wikilink">柏林</a>，德國</p></td>
<td></td>
<td><p>1–0</p></td>
<td><p>4–2</p></td>
<td><p>友誼賽</p></td>
</tr>
<tr class="even">
<td><p>9.</p></td>
<td><p>2005年9月7日</p></td>
<td><p>奧林匹克體育場，柏林，德國</p></td>
<td></td>
<td><p>3–1</p></td>
<td><p>4–2</p></td>
<td><p>友誼賽</p></td>
</tr>
<tr class="odd">
<td><p>10.</p></td>
<td><p>2005年9月7日</p></td>
<td><p>奧林匹克體育場，柏林，德國</p></td>
<td></td>
<td><p>4–2</p></td>
<td><p>4–2</p></td>
<td><p>友誼賽</p></td>
</tr>
<tr class="even">
<td><p>11.</p></td>
<td><p>2006年5月27日</p></td>
<td><p><a href="../Page/:en:Badenova_Stadion.md" title="wikilink">Badenova Stadion</a>, <a href="../Page/弗賴堡.md" title="wikilink">弗賴堡</a>, 德國</p></td>
<td></td>
<td><p>3–0</p></td>
<td><p>7–0</p></td>
<td><p>友誼賽</p></td>
</tr>
<tr class="odd">
<td><p>12.</p></td>
<td><p>2006年5月27日</p></td>
<td><p>Badenova Stadion, 弗賴堡, 德國</p></td>
<td></td>
<td><p>5–0</p></td>
<td><p>7–0</p></td>
<td><p>友誼賽</p></td>
</tr>
<tr class="even">
<td><p>13.</p></td>
<td><p>2006年6月20日</p></td>
<td><p>奧林匹克體育場, 柏林, 德國</p></td>
<td></td>
<td><p>3–0</p></td>
<td><p>3–0</p></td>
<td><p><a href="../Page/2006年世界盃.md" title="wikilink">2006年世界盃分組賽</a></p></td>
</tr>
<tr class="odd">
<td><p>14.</p></td>
<td><p>2006年6月24日</p></td>
<td><p><a href="../Page/安联球场_(慕尼黑).md" title="wikilink">安联球场</a>, <a href="../Page/慕尼黑.md" title="wikilink">慕尼黑</a>, 德國</p></td>
<td></td>
<td><p>1–0</p></td>
<td><p>2–0</p></td>
<td><p>2006年世界盃十六强賽</p></td>
</tr>
<tr class="even">
<td><p>15.</p></td>
<td><p>2006年6月24日</p></td>
<td><p>安联球场, 慕尼黑, 德國</p></td>
<td></td>
<td><p>2–0</p></td>
<td><p>2–0</p></td>
<td><p>2006年世界盃十六强賽</p></td>
</tr>
<tr class="odd">
<td><p>16.</p></td>
<td><p>2006年9月2日</p></td>
<td><p><a href="../Page/:en:Gottlieb_Daimler_Stadion.md" title="wikilink">戈特利布·戴姆勒競技場</a>, <a href="../Page/斯圖加特.md" title="wikilink">斯圖加特</a>, 德國</p></td>
<td></td>
<td><p>1–0</p></td>
<td><p>1–0</p></td>
<td><p><a href="../Page/2008年歐洲國家盃外圍賽.md" title="wikilink">2008年歐洲國家盃外圍賽</a></p></td>
</tr>
<tr class="even">
<td><p>17.</p></td>
<td><p>2006年9月6日</p></td>
<td><p><a href="../Page/:en:Stadio_Olimpico_(Serravalle).md" title="wikilink">奧林匹克體育場</a>, <a href="../Page/塞拉瓦萊.md" title="wikilink">塞拉瓦萊</a>, <a href="../Page/聖馬力諾.md" title="wikilink">聖馬力諾</a></p></td>
<td></td>
<td><p>1–0</p></td>
<td><p>13–0</p></td>
<td><p>2008年歐洲國家盃外圍賽</p></td>
</tr>
<tr class="odd">
<td><p>18.</p></td>
<td><p>2006年9月6日</p></td>
<td><p>奧林匹克體育場, 塞拉瓦萊, 聖馬力諾</p></td>
<td></td>
<td><p>5–0</p></td>
<td><p>13–0</p></td>
<td><p>2008年歐洲國家盃外圍賽</p></td>
</tr>
<tr class="even">
<td><p>19.</p></td>
<td><p>2006年9月6日</p></td>
<td><p>奧林匹克體育場, 塞拉瓦萊, 聖馬力諾</p></td>
<td></td>
<td><p>8–0</p></td>
<td><p>13–0</p></td>
<td><p>2008年歐洲國家盃外圍賽</p></td>
</tr>
<tr class="odd">
<td><p>20.</p></td>
<td><p>2006年9月6日</p></td>
<td><p>奧林匹克體育場, 塞拉瓦萊, 聖馬力諾</p></td>
<td></td>
<td><p>10–0</p></td>
<td><p>13–0</p></td>
<td><p>2008年歐洲國家盃外圍賽</p></td>
</tr>
<tr class="even">
<td><p>21.</p></td>
<td><p>2006年10月11日</p></td>
<td><p><a href="../Page/:en:Tehelné_Pole_Stadion.md" title="wikilink">Tehelné Pole Stadion</a>, <a href="../Page/布拉迪斯拉發.md" title="wikilink">布拉迪斯拉發</a>, <a href="../Page/斯洛伐克.md" title="wikilink">斯洛伐克</a></p></td>
<td></td>
<td><p>1–0</p></td>
<td><p>4–1</p></td>
<td><p>2008年歐洲國家盃外圍賽</p></td>
</tr>
<tr class="odd">
<td><p>22.</p></td>
<td><p>2006年10月11日</p></td>
<td><p>Tehelné Pole Stadion, 布拉迪斯拉發, 斯洛伐克</p></td>
<td></td>
<td><p>4–1</p></td>
<td><p>4–1</p></td>
<td><p>2008年歐洲國家盃外圍賽</p></td>
</tr>
<tr class="even">
<td><p>23.</p></td>
<td><p>2007年9月12日</p></td>
<td><p><a href="../Page/萊茵能源球場.md" title="wikilink">萊茵能源球場</a>, <a href="../Page/科隆.md" title="wikilink">科隆</a>, 德國</p></td>
<td></td>
<td><p>3–1</p></td>
<td><p>3–1</p></td>
<td><p>友誼賽</p></td>
</tr>
<tr class="odd">
<td><p>24.</p></td>
<td><p>2007年11月17日</p></td>
<td><p><a href="../Page/AWD球場.md" title="wikilink">AWD球場</a>, <a href="../Page/漢諾威.md" title="wikilink">漢諾威</a>, 德國</p></td>
<td></td>
<td><p>3–0</p></td>
<td><p>4–0</p></td>
<td><p>2008年歐洲國家盃外圍賽</p></td>
</tr>
<tr class="even">
<td><p>25.</p></td>
<td><p>2008年3月26日</p></td>
<td><p><a href="../Page/聖雅各公園球場.md" title="wikilink">聖雅各公園球場</a>, <a href="../Page/巴塞爾.md" title="wikilink">巴塞爾</a>, <a href="../Page/瑞士.md" title="wikilink">瑞士</a></p></td>
<td></td>
<td><p>4–0</p></td>
<td><p>4–0</p></td>
<td><p>友誼賽</p></td>
</tr>
<tr class="odd">
<td><p>26.</p></td>
<td><p>2008年6月8日</p></td>
<td><p><a href="../Page/:en:Wörthersee_Stadion.md" title="wikilink">Wörthersee Stadion</a>, <a href="../Page/克拉根福.md" title="wikilink">克拉根福</a>, <a href="../Page/奧地利.md" title="wikilink">奧地利</a></p></td>
<td></td>
<td><p>1–0</p></td>
<td><p>2–0</p></td>
<td><p><a href="../Page/2008年歐洲國家盃.md" title="wikilink">2008年歐洲國家盃分組賽</a></p></td>
</tr>
<tr class="even">
<td><p>27.</p></td>
<td><p>2008年6月8日</p></td>
<td><p>Wörthersee Stadion, 克拉根福, 奧地利</p></td>
<td></td>
<td><p>2–0</p></td>
<td><p>2–0</p></td>
<td><p>2008年歐洲國家盃分組賽</p></td>
</tr>
<tr class="odd">
<td><p>28.</p></td>
<td><p>2008年6月12日</p></td>
<td><p>Wörthersee Stadion, 克拉根福, 奧地利</p></td>
<td></td>
<td><p>1–2</p></td>
<td><p>1–2</p></td>
<td><p>2008年歐洲國家盃分組賽</p></td>
</tr>
<tr class="even">
<td><p>29.</p></td>
<td><p>2008年9月6日</p></td>
<td><p><a href="../Page/:en:Rheinpark_Stadion.md" title="wikilink">萊茵公園球場</a>, <a href="../Page/:en:Vaduz.md" title="wikilink">華杜茲</a>, <a href="../Page/列支敦士登.md" title="wikilink">列支敦士登</a></p></td>
<td></td>
<td><p>1–0</p></td>
<td><p>6–0</p></td>
<td><p><a href="../Page/2010年世界盃外圍賽.md" title="wikilink">2010年世界盃外圍賽</a></p></td>
</tr>
<tr class="odd">
<td><p>30.</p></td>
<td><p>2008年9月6日</p></td>
<td><p>萊茵公園球場, 華杜茲, 列支敦士登</p></td>
<td></td>
<td><p>2–0</p></td>
<td><p>6–0</p></td>
<td><p>2010年世界盃外圍賽</p></td>
</tr>
<tr class="even">
<td><p>31.</p></td>
<td><p>2008年10月11日</p></td>
<td><p><a href="../Page/西格納伊度納公園.md" title="wikilink">西格納伊度納公園</a>, <a href="../Page/多蒙特.md" title="wikilink">多蒙特</a>, 德國</p></td>
<td></td>
<td><p>1–0</p></td>
<td><p>2–1</p></td>
<td><p>2010年世界盃外圍賽</p></td>
</tr>
<tr class="odd">
<td><p>32.</p></td>
<td><p>2009年3月28日</p></td>
<td><p>中央體育場, 萊比錫, 德國</p></td>
<td></td>
<td><p>4–0</p></td>
<td><p>4–0</p></td>
<td><p>2010年世界盃外圍賽</p></td>
</tr>
<tr class="even">
<td><p>33.</p></td>
<td><p>2009年5月29日</p></td>
<td><p><a href="../Page/上海體育場.md" title="wikilink">上海體育場</a>, <a href="../Page/上海.md" title="wikilink">上海</a>, <a href="../Page/中國.md" title="wikilink">中國</a></p></td>
<td></td>
<td><p>1–1</p></td>
<td><p>1–1</p></td>
<td><p>友誼賽</p></td>
</tr>
<tr class="odd">
<td><p>34.</p></td>
<td><p>2009年9月9日</p></td>
<td><p>AWD球場, 漢諾威, 德國</p></td>
<td></td>
<td><p>4–0</p></td>
<td><p>4–0</p></td>
<td><p>2010年世界盃外圍賽</p></td>
</tr>
<tr class="even">
<td><p>35.</p></td>
<td><p>2009年10月14日</p></td>
<td><p><a href="../Page/北方銀行競技場.md" title="wikilink">北方銀行競技場</a>, <a href="../Page/漢堡.md" title="wikilink">漢堡</a>, 德國</p></td>
<td></td>
<td><p>1–1</p></td>
<td><p>1–1</p></td>
<td><p>2010年世界盃外圍賽</p></td>
</tr>
<tr class="odd">
<td><p>36.</p></td>
<td><p>2009年11月18日</p></td>
<td><p><a href="../Page/維爾廷斯球場.md" title="wikilink">維爾廷斯球場</a>, <a href="../Page/蓋爾森基興.md" title="wikilink">蓋爾森基興</a>, 德國</p></td>
<td></td>
<td><p>1–0</p></td>
<td><p>2–2</p></td>
<td><p>友誼賽</p></td>
</tr>
<tr class="even">
<td><p>37.</p></td>
<td><p>2009年11月18日</p></td>
<td><p>維爾廷斯球場, 蓋爾森基興, 德國</p></td>
<td></td>
<td><p>2–2</p></td>
<td><p>2–2</p></td>
<td><p>友誼賽</p></td>
</tr>
<tr class="odd">
<td><p>38.</p></td>
<td><p>2010年5月29日</p></td>
<td><p><a href="../Page/普斯卡什球場.md" title="wikilink">普斯卡什球場</a>, <a href="../Page/布達佩斯.md" title="wikilink">布達佩斯</a>, <a href="../Page/匈牙利.md" title="wikilink">匈牙利</a></p></td>
<td></td>
<td><p>1–0</p></td>
<td><p>3–0</p></td>
<td><p>友誼賽</p></td>
</tr>
<tr class="even">
<td><p>39.</p></td>
<td><p>2010年6月13日</p></td>
<td><p><a href="../Page/德班球場.md" title="wikilink">德班球場</a>, <a href="../Page/德班.md" title="wikilink">德班</a>, <a href="../Page/南非.md" title="wikilink">南非</a></p></td>
<td></td>
<td><p>1–0</p></td>
<td><p>4–0</p></td>
<td><p><a href="../Page/2010年世界盃.md" title="wikilink">2010年世界盃分組賽</a></p></td>
</tr>
<tr class="odd">
<td><p>40.</p></td>
<td><p>2010年6月27日</p></td>
<td><p><a href="../Page/自由州球場.md" title="wikilink">自由州球場</a>, <a href="../Page/布隆方丹.md" title="wikilink">布隆方丹</a>, 南非</p></td>
<td></td>
<td><p>2–0</p></td>
<td><p>4–1</p></td>
<td><p>2010年世界盃十六强賽</p></td>
</tr>
<tr class="even">
<td><p>41</p></td>
<td><p>2010年9月7日</p></td>
<td><p>萊茵能源球場, 科隆, 德國</p></td>
<td></td>
<td><p><strong>2</strong>–0</p></td>
<td><p>6–1</p></td>
<td><p><a href="../Page/2012年歐洲國家盃外圍賽.md" title="wikilink">2012年歐洲國家盃外圍賽</a></p></td>
</tr>
<tr class="odd">
<td><p>42</p></td>
<td><p>2010年10月12日</p></td>
<td><p><a href="../Page/:en:Astana_Arena.md" title="wikilink">阿斯塔納競技場</a>, <a href="../Page/阿斯塔納.md" title="wikilink">阿斯塔納</a>, <a href="../Page/哈薩克.md" title="wikilink">哈薩克</a></p></td>
<td></td>
<td><p><strong>3</strong>–0</p></td>
<td><p>3–0</p></td>
<td><p>2012年歐洲國家盃外圍賽</p></td>
</tr>
<tr class="even">
<td><p>43</p></td>
<td><p>2011年9月2日</p></td>
<td><p>維爾廷斯球場, 蓋爾森基興, 德國</p></td>
<td></td>
<td><p><strong>3</strong>–0</p></td>
<td><p>6–2</p></td>
<td><p>2012年歐洲國家盃外圍賽</p></td>
</tr>
<tr class="odd">
<td><p>44</p></td>
<td><p>2012年6月17日</p></td>
<td><p><a href="../Page/利維夫球場.md" title="wikilink">利維夫球場</a>, <a href="../Page/利維夫.md" title="wikilink">利維夫</a>, <a href="../Page/烏克蘭.md" title="wikilink">烏克蘭</a></p></td>
<td></td>
<td><p><strong>1</strong>–0</p></td>
<td><p>2–1</p></td>
<td><p><a href="../Page/2012年歐洲國家盃.md" title="wikilink">2012年歐洲國家盃</a></p></td>
</tr>
</tbody>
</table>

## 榮譽

### 球會

  -  [科隆](../Page/科隆足球會.md "wikilink")

<!-- end list -->

  - [德國足球乙級聯賽](../Page/德國足球乙級聯賽.md "wikilink")：[2004-05賽季](../Page/2004年至2005年德國足球乙級聯賽.md "wikilink")

<!-- end list -->

  -  [拜仁慕尼黑](../Page/拜仁慕尼黑.md "wikilink")

<!-- end list -->

  - [德国足球甲级联赛](../Page/德国足球甲级联赛.md "wikilink")：[2007-08賽季](../Page/2007年至2008年德国足球甲级联赛.md "wikilink")
  - [德國聯賽盃](../Page/德國足球協會聯賽盃.md "wikilink")：[2007年](../Page/2007年德國聯賽盃.md "wikilink")
  - [德国足协杯](../Page/德国足协杯.md "wikilink")：2007年

<!-- end list -->

  -  [阿仙奴](../Page/阿仙奴足球會.md "wikilink")

<!-- end list -->

  - [英格蘭足總盃冠軍](../Page/英格蘭足總盃.md "wikilink")﹕[2014年](../Page/2013/14球季英格蘭足總盃.md "wikilink")

### 國家隊

  - [世界杯足球赛冠军](../Page/世界杯足球赛.md "wikilink")：[2014](../Page/2014年世界杯足球赛.md "wikilink")
  - [世界盃足球賽季軍](../Page/世界盃足球賽.md "wikilink")：[2006](../Page/2006年世界盃足球賽.md "wikilink")、[2010](../Page/2010年世界杯足球赛.md "wikilink")
  - [國際足協洲際國家盃季軍](../Page/國際足協洲際國家盃.md "wikilink")：[2005](../Page/2005年洲際國家盃.md "wikilink")
  - [歐洲國家杯亞軍](../Page/歐洲國家杯.md "wikilink")：[2008](../Page/2008年歐洲國家盃.md "wikilink")

### 個人

  - 世界盃最佳年青球員：[2006](../Page/2006年世界盃足球賽.md "wikilink")
  - 歐洲國家杯賽事最佳陣容：[2008](../Page/2008年歐洲國家盃.md "wikilink")

## 参考資料

## 外部連結

  -
  -

  -
  -
[Category:2014年世界盃足球賽球員](../Category/2014年世界盃足球賽球員.md "wikilink")
[Category:2012年歐洲國家盃球員](../Category/2012年歐洲國家盃球員.md "wikilink")
[Category:2010年世界盃足球賽球員](../Category/2010年世界盃足球賽球員.md "wikilink")
[Category:2008年歐洲國家盃球員](../Category/2008年歐洲國家盃球員.md "wikilink")
[Category:2006年世界盃足球賽球員](../Category/2006年世界盃足球賽球員.md "wikilink")
[Category:2005年洲際國家盃球員](../Category/2005年洲際國家盃球員.md "wikilink")
[Category:2004年歐洲國家盃球員](../Category/2004年歐洲國家盃球員.md "wikilink")
[Category:德國國家足球隊球員](../Category/德國國家足球隊球員.md "wikilink")
[Category:德国足球运动员](../Category/德国足球运动员.md "wikilink")
[Category:足球前鋒](../Category/足球前鋒.md "wikilink")
[Category:足球中場](../Category/足球中場.md "wikilink")
[Category:科隆球員](../Category/科隆球員.md "wikilink")
[Category:拜仁慕尼黑球員](../Category/拜仁慕尼黑球員.md "wikilink")
[Category:阿仙奴球員](../Category/阿仙奴球員.md "wikilink")
[Category:國際米蘭球員](../Category/國際米蘭球員.md "wikilink")
[Category:神戶勝利船球員](../Category/神戶勝利船球員.md "wikilink")
[Category:德甲球員](../Category/德甲球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:意甲球員](../Category/意甲球員.md "wikilink")
[Category:英格蘭外籍足球運動員](../Category/英格蘭外籍足球運動員.md "wikilink")
[Category:意大利外籍足球運動員](../Category/意大利外籍足球運動員.md "wikilink")
[Category:日本外籍足球運動員](../Category/日本外籍足球運動員.md "wikilink")
[Category:德國旅外足球運動員](../Category/德國旅外足球運動員.md "wikilink")
[Category:波蘭裔德國人](../Category/波蘭裔德國人.md "wikilink")
[Category:西里西亞人](../Category/西里西亞人.md "wikilink")
[Category:银月桂叶获得者](../Category/银月桂叶获得者.md "wikilink")
[Category:歸化德國公民](../Category/歸化德國公民.md "wikilink")
[Category:FIFA世纪俱乐部](../Category/FIFA世纪俱乐部.md "wikilink")
[Category:世界盃足球賽冠軍隊球員](../Category/世界盃足球賽冠軍隊球員.md "wikilink")

1.

2.

3.
4.

5.

6.

7.

8.