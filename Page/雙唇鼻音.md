**雙唇鼻音**是[輔音的一種](../Page/輔音.md "wikilink")，用於一些[口語中](../Page/口語.md "wikilink")。雙唇鼻音在[國際音標的符號是](../Page/國際音標.md "wikilink")，[X-SAMPA音標的符號則是](../Page/X-SAMPA.md "wikilink")
`m`。世界上大部分[語言均有使用雙唇鼻音](../Page/語言.md "wikilink")。[漢語中](../Page/漢語.md "wikilink")“摸”、“麼”等字均是以雙唇鼻音開首，漢語拼音中以[m表示](../Page/m.md "wikilink")。

## 特徵

雙唇鼻音特點:

  - 發音方法是閉塞，也就是說通過阻礙空氣在聲腔流動來發音。

## 該音見於漢語中

  - 在[現代標準漢語裡](../Page/現代標準漢語.md "wikilink")，雙唇鼻音見於*媽* ()、*摸* ()、*麼*
    ()的音首。雙唇鼻音韻尾皆化為n。
  - 於[粵語](../Page/粵語.md "wikilink")[廣州話](../Page/廣州話.md "wikilink")，此音可以在音首和音末中出現，如*無*
    (mou4)、*甘* (gam1)；亦可單獨出現，如*唔* (m4)。
  - 在[闽南语裡](../Page/闽南语.md "wikilink")，此音可在音首和音末出現，如*麵* (mi)、*甘*
    (gamf)；亦可單獨出現，如*姆* (mr)。(發音採[MLT](../Page/MLT.md "wikilink"))

## 該音見於英語中

[英語中雙唇鼻音以字母m表示](../Page/英語.md "wikilink")。包括[美式英語在內的某些英語口音](../Page/美式英語.md "wikilink")，雙唇鼻音可以單獨成一音節，如*bottom*。
在英語中唇齒鼻音是雙唇鼻音的[同位異音](../Page/同位異音.md "wikilink")。見條目[唇齒鼻音](../Page/唇齒鼻音.md "wikilink")。

## 该音出现语言及单词

<table>
<thead>
<tr class="header">
<th><p>语言名</p></th>
<th><p>例词</p></th>
<th><p><a href="../Page/国际音标.md" title="wikilink">IPA</a></p></th>
<th><p>意义</p></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/阿迪格语.md" title="wikilink">阿迪格语</a></p></td>
<td></td>
<td></td>
<td><p>梳子</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿拉伯语.md" title="wikilink">阿拉伯语</a></p></td>
<td><p><a href="../Page/现代标准阿拉伯语.md" title="wikilink">现代标准</a>[1]</p></td>
<td></td>
<td></td>
<td><p>厨房</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/亚美尼亚语.md" title="wikilink">亚美尼亚语</a></p></td>
<td><p><a href="../Page/东亚美尼亚语.md" title="wikilink">东部</a>[2]</p></td>
<td></td>
<td></td>
<td><p>母亲</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/巴斯克语.md" title="wikilink">巴斯克语</a></p></td>
<td></td>
<td></td>
<td><p>去爱</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/保加利亚语.md" title="wikilink">保加利亚语</a></p></td>
<td></td>
<td></td>
<td><p>雾</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/加泰罗尼亚语.md" title="wikilink">加泰罗尼亚语</a>[3]</p></td>
<td></td>
<td></td>
<td><p>母亲</p></td>
<td><p>见<a href="../Page/加泰罗尼亚语音系.md" title="wikilink">加泰罗尼亚语音系</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/切罗基语.md" title="wikilink">切罗基语</a></p></td>
<td><p><a href="../Page/切罗基字母表.md" title="wikilink">Ꭰ<strong>Ꮉ</strong></a></p></td>
<td></td>
<td><p>水</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/汉语.md" title="wikilink">汉语</a></p></td>
<td><p><a href="../Page/粵語.md" title="wikilink">粵語</a><a href="../Page/广州话.md" title="wikilink">广州话</a></p></td>
<td><p></p></td>
<td></td>
<td><p>晚</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/現代標準漢語.md" title="wikilink">現代標準</a></p></td>
<td><p></p></td>
<td></td>
<td><p>妈</p></td>
<td><p>见<a href="../Page/维基百科:普通话国际音标.md" title="wikilink">汉语音系</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/捷克语.md" title="wikilink">捷克语</a></p></td>
<td></td>
<td></td>
<td><p>男人</p></td>
<td><p>见<a href="../Page/捷克语音系.md" title="wikilink">捷克语音系</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/荷兰语.md" title="wikilink">荷兰语</a>[4]</p></td>
<td></td>
<td></td>
<td><p>嘴巴</p></td>
<td><p>见<a href="../Page/荷兰语音系.md" title="wikilink">荷兰语音系</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/英语.md" title="wikilink">英语</a></p></td>
<td></td>
<td></td>
<td><p>他（宾格）</p></td>
<td><p>见<a href="../Page/英语音系.md" title="wikilink">英语音系</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/菲律宾语.md" title="wikilink">菲律宾语</a></p></td>
<td></td>
<td><p>/ma'nok/</p></td>
<td><p>公鸡</p></td>
<td><p>见<a href="../Page/菲律宾音系.md" title="wikilink">菲律宾音系</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/芬兰语.md" title="wikilink">芬兰语</a></p></td>
<td></td>
<td></td>
<td><p>我</p></td>
<td><p>见<a href="../Page/芬兰语音系.md" title="wikilink">芬兰语音系</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/法语.md" title="wikilink">法语</a>[5]</p></td>
<td></td>
<td></td>
<td><p>吃</p></td>
<td><p>见<a href="../Page/法语音系.md" title="wikilink">法语音系</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/格鲁吉亚语.md" title="wikilink">格鲁吉亚语</a>[6]</p></td>
<td></td>
<td></td>
<td><p>三</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/德语.md" title="wikilink">德语</a></p></td>
<td></td>
<td></td>
<td><p>嘴巴</p></td>
<td><p>见<a href="../Page/德语音系.md" title="wikilink">德语音系</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/希腊语.md" title="wikilink">希腊语</a>[7]</p></td>
<td></td>
<td><p>/'maza/</p></td>
<td><p>土块</p></td>
<td><p>见<a href="../Page/现代希腊语音系.md" title="wikilink">现代希腊语音系</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/古吉拉特语.md" title="wikilink">古吉拉特语</a></p></td>
<td></td>
<td></td>
<td><p>雄孔雀</p></td>
<td><p>见<a href="../Page/古吉拉特语音系.md" title="wikilink">古吉拉特语音系</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/夏威夷语.md" title="wikilink">夏威夷语</a>[8]</p></td>
<td></td>
<td><p>/maka/</p></td>
<td><p>眼睛</p></td>
<td><p>见<a href="../Page/夏威夷语音系.md" title="wikilink">夏威夷语音系</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/印地语.md" title="wikilink">印地语</a></p></td>
<td></td>
<td></td>
<td><p>房子</p></td>
<td><p>见<a href="../Page/印地语-乌尔都语音系.md" title="wikilink">印地语-乌尔都语音系</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/希伯来语.md" title="wikilink">希伯来语</a></p></td>
<td></td>
<td></td>
<td><p>母亲</p></td>
<td><p>见<a href="../Page/现代希伯来语音系.md" title="wikilink">现代希伯来语音系</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/匈牙利语.md" title="wikilink">匈牙利语</a></p></td>
<td></td>
<td></td>
<td><p>今天</p></td>
<td><p>见<a href="../Page/匈牙利语音系.md" title="wikilink">匈牙利语音系</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/印度尼西亚语.md" title="wikilink">印度尼西亚语</a>[9]</p></td>
<td></td>
<td></td>
<td><p>进入</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/意大利语.md" title="wikilink">意大利语</a>[10]</p></td>
<td></td>
<td><p>/'mamma/</p></td>
<td><p>妈妈</p></td>
<td><p>见<a href="../Page/意大利语音系.md" title="wikilink">意大利语音系</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/日语.md" title="wikilink">日语</a>[11]</p></td>
<td><p><a href="../Page/日本汉字.md" title="wikilink">乾杯</a> </p></td>
<td></td>
<td><p>干杯</p></td>
<td><p>见<a href="../Page/日語音韻學.md" title="wikilink">日語音韻學</a></p></td>
</tr>
<tr class="odd">
<td><p>[12]</p></td>
<td></td>
<td></td>
<td><p>姐姐</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/韩语.md" title="wikilink">韩语</a></p></td>
<td><p></p></td>
<td></td>
<td><p>妈妈</p></td>
<td><p>见<a href="../Page/韩语音系.md" title="wikilink">韩语音系</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/马其顿语.md" title="wikilink">马其顿语</a></p></td>
<td></td>
<td></td>
<td><p>母亲</p></td>
<td><p>见<a href="../Page/马其顿语音系.md" title="wikilink">马其顿语音系</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/马来语.md" title="wikilink">马来语</a></p></td>
<td></td>
<td></td>
<td><p>晚上</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/马拉雅拉姆语.md" title="wikilink">马拉雅拉姆语</a>[13]</p></td>
<td></td>
<td></td>
<td><p>缺少</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/马耳他语.md" title="wikilink">马耳他语</a></p></td>
<td></td>
<td><p>/ilma/</p></td>
<td><p>水</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/马拉地语.md" title="wikilink">马拉地语</a></p></td>
<td></td>
<td></td>
<td><p>意识</p></td>
<td><p>见<a href="../Page/马拉地语音系.md" title="wikilink">马拉地语音系</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/挪威语.md" title="wikilink">挪威语</a></p></td>
<td></td>
<td></td>
<td><p>妈妈</p></td>
<td><p>见<a href="../Page/挪威语语音音系.md" title="wikilink">挪威语语音音系</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/波斯语.md" title="wikilink">波斯语</a></p></td>
<td></td>
<td></td>
<td><p>母亲</p></td>
<td><p>见<a href="../Page/波斯语音系.md" title="wikilink">波斯语音系</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/皮拉罕语.md" title="wikilink">皮拉罕语</a></p></td>
<td></td>
<td></td>
<td><p>父母</p></td>
<td><p>同字异音</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/波兰语.md" title="wikilink">波兰语</a>[14]</p></td>
<td></td>
<td></td>
<td><p>质量</p></td>
<td><p>见<a href="../Page/波兰音系.md" title="wikilink">波兰音系</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/葡萄牙语.md" title="wikilink">葡萄牙语</a>[15]</p></td>
<td></td>
<td><p>/'matu/</p></td>
<td><p>灌木</p></td>
<td><p>见<a href="../Page/葡萄牙语音系.md" title="wikilink">葡萄牙语音系</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/旁遮普语.md" title="wikilink">旁遮普语</a></p></td>
<td><p><a href="../Page/古木基文.md" title="wikilink">ਮੈਂ</a></p></td>
<td></td>
<td><p>我</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/俄罗斯语.md" title="wikilink">俄罗斯语</a>[16]</p></td>
<td></td>
<td></td>
<td><p>丈夫</p></td>
<td><p>与音形成对比。见<a href="../Page/俄罗斯语音系.md" title="wikilink">俄罗斯语音系</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/塞尔维亚-克罗地亚语.md" title="wikilink">塞尔维亚-克罗地亚语</a></p></td>
<td><p>/ </p></td>
<td><p>/milina/</p></td>
<td><p>享受</p></td>
<td><p>见<a href="../Page/塞尔维亚-克罗地亚语音系.md" title="wikilink">塞尔维亚-克罗地亚语音系</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/斯洛伐克语.md" title="wikilink">斯洛伐克语</a></p></td>
<td></td>
<td></td>
<td><p>男人</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/西班牙语.md" title="wikilink">西班牙语</a>[17]</p></td>
<td></td>
<td></td>
<td><p>客舱服务员</p></td>
<td><p>见<a href="../Page/西班牙语音系.md" title="wikilink">西班牙语音系</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/斯瓦西里语.md" title="wikilink">斯瓦西里语</a></p></td>
<td></td>
<td><p>/'miti/</p></td>
<td><p>树</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/瑞典语.md" title="wikilink">瑞典语</a></p></td>
<td></td>
<td><p>/mask/</p></td>
<td><p>蛆虫</p></td>
<td><p>见<a href="../Page/瑞典语音系.md" title="wikilink">瑞典语音系</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td><p>语言；舌头</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/土耳其语.md" title="wikilink">土耳其语</a></p></td>
<td></td>
<td></td>
<td><p>我的</p></td>
<td><p>见<a href="../Page/土耳其语音系.md" title="wikilink">土耳其语音系</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/乌克兰语.md" title="wikilink">乌克兰语</a></p></td>
<td></td>
<td></td>
<td><p>牛奶</p></td>
<td><p>见<a href="../Page/乌克兰语音系.md" title="wikilink">乌克兰语音系</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/乌尔都语.md" title="wikilink">乌尔都语</a></p></td>
<td></td>
<td></td>
<td><p>房子</p></td>
<td><p>见<a href="../Page/印地语-乌尔都语音系.md" title="wikilink">印地语-乌尔都语音系</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/维吾尔语.md" title="wikilink">维吾尔语</a></p></td>
<td></td>
<td></td>
<td><p>我</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/越南语.md" title="wikilink">越南语</a>[18]</p></td>
<td></td>
<td></td>
<td><p>盐</p></td>
<td><p>见<a href="../Page/越南语音系.md" title="wikilink">越南语音系</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/威尔士语.md" title="wikilink">威尔士语</a></p></td>
<td></td>
<td><p>/mam/</p></td>
<td><p>母亲</p></td>
<td><p>见<a href="../Page/威尔士语音系.md" title="wikilink">威尔士语音系</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/西弗里斯语.md" title="wikilink">西弗里斯语</a></p></td>
<td></td>
<td><p>/mar/</p></td>
<td><p>湖</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/彝语北部方言.md" title="wikilink">彝语北部方言</a></p></td>
<td><p></p></td>
<td><p>{国际音标 |/ma˧/</p></td>
<td><p>竹子</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/萨波特克语.md" title="wikilink">萨波特克语</a></p></td>
<td><p><a href="../Page/提尔魁亚潘萨波特克语.md" title="wikilink">提尔魁亚潘萨波特克语</a>[19]</p></td>
<td><p><strong>m</strong>an</p></td>
<td></td>
<td><p>动物</p></td>
</tr>
</tbody>
</table>

## 另請參閱

## 参考资料

[Category:雙唇音](../Category/雙唇音.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14.
15.
16.
17.
18.
19.