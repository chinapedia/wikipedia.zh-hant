**道格拉斯縣**（**Douglas County,
Colorado**）是[美國](../Page/美國.md "wikilink")[科羅拉多州中部的一個縣](../Page/科羅拉多州.md "wikilink")。面積2,183平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口175,766。縣治[城堡石](../Page/城堡石_\(科羅拉多州\).md "wikilink")
(Castle Rock)。

成立於1861年11月1日，以紀念第十六任總統[阿伯拉罕·林肯在](../Page/阿伯拉罕·林肯.md "wikilink")1860年的競選對手之一、[民主黨的](../Page/民主黨_\(美國\).md "wikilink")[史提芬·阿諾德·道格拉斯](../Page/史提芬·阿諾德·道格拉斯.md "wikilink")。是該州最早的十七個縣之一。

[D](../Category/科罗拉多州行政区划.md "wikilink")