[Bank_Run_on_American_Union_Bank.jpeg](https://zh.wikipedia.org/wiki/File:Bank_Run_on_American_Union_Bank.jpeg "fig:Bank_Run_on_American_Union_Bank.jpeg")
[Response_of_malicious_rumours_of_BEA_on_24_Sept_2008.jpg](https://zh.wikipedia.org/wiki/File:Response_of_malicious_rumours_of_BEA_on_24_Sept_2008.jpg "fig:Response_of_malicious_rumours_of_BEA_on_24_Sept_2008.jpg")分行出現擠提情況\]\]
**擠兌**（也稱為**擠提**；兌是兌現，提則是指提款）指的是[銀行或](../Page/銀行.md "wikilink")[金融機構被大批的](../Page/金融機構.md "wikilink")[存款客戶要求提款領回自有的儲金](../Page/存款.md "wikilink")，通常發生在銀行的營運上有重大負面傳聞之時，銀行遭遇集中且密集的提款，很可能支應不及而宣告營運困難，倘若未得到其他援助或讓擠兌情事減緩，則該銀行極有可能因此宣告倒閉以及[信用](../Page/信用.md "wikilink")[破產](../Page/破產.md "wikilink")。

## 原理

銀行接受客戶存款時，實際上是銀行向客戶借錢，屬於債務的一種。
而銀行接受存款之後，並不是單純地把客戶的錢放置保管，而是會把這些錢發作貸款、或者投資，以賺取當中的利息。
所以銀行內所擁有可動支的金錢並不等於所接受的存款總額。 因此，當大量存款客戶在短時間內要求提款時，銀行並沒有足夠的可動支金額來支付這些提款金額。
此時銀行等於[无力偿付債務](../Page/无力偿付.md "wikilink")，也就是[破產](../Page/破產.md "wikilink")，這樣單純由存款引發的破產事件即為**擠兌**。\[1\]

## 各地重要擠兌事件

### 香港

[香港最近的大型擠兌事件為](../Page/香港.md "wikilink")[東亞銀行因市場消息問題及](../Page/東亞銀行.md "wikilink")[國際商業信貸銀行因母公司問題而破產](../Page/國際商業信貸銀行.md "wikilink")，引起擠提；而最為人記得的事件，則是香港1965年銀行擠提潮。

### 澳門

滙業銀行的北韓帳戶及擠提事件}}
2005年9月15日，[美國財政部指稱](../Page/美國財政部.md "wikilink")[滙業銀行協助](../Page/滙業銀行.md "wikilink")[北韓客戶](../Page/北韓.md "wikilink")[洗黑錢](../Page/洗黑錢.md "wikilink")、協助[偽鈔流通等支持](../Page/偽鈔.md "wikilink")[恐怖主義活動](../Page/恐怖主義.md "wikilink")，建議美國公司斷絕與該銀行的任何聯繫。澳門市面的滙業銀行出現擠提，在短短兩天被提走了三億[澳門元](../Page/澳門元.md "wikilink")，佔滙業總存款額十分之一，不過銀行稱有足夠現金應付。及後[澳門特別行政區行政長官](../Page/澳門特別行政區行政長官.md "wikilink")[何厚鏵以](../Page/何厚鏵.md "wikilink")“滙業銀行的客戶提款出現嚴重不正常情況”為理由，引用《金融體系法律制度》，委派[大西洋銀行澳門分行總經理蘇鈺龍及](../Page/大西洋銀行.md "wikilink")[澳門金融管理局內部審計辦公室副總監李展程](../Page/澳門金融管理局.md "wikilink")，參與滙業銀行的管理，以鞏固公眾對金融體系的信心。2007年9月28日，兩年的管理期結束，澳門特區政府決定把匯業銀行的管理權交回原股東。

### 英國

[Birmingham_Northern_Rock_bank_run_2007.jpg](https://zh.wikipedia.org/wiki/File:Birmingham_Northern_Rock_bank_run_2007.jpg "fig:Birmingham_Northern_Rock_bank_run_2007.jpg")分行門口的**擠兌**人潮\]\]

2007年9月13日，[英國](../Page/英國.md "wikilink")[北岩銀行聲稱基於融資問題](../Page/北岩銀行.md "wikilink")，接受[英倫銀行一項緊急資助](../Page/英倫銀行.md "wikilink")。一般人認為是次事件是因為[美國次按風暴引起](../Page/美國次按風暴.md "wikilink")。

### 台灣

1985年2月9日，台北市第十信用合作社因放款總額占存款總額之比率高達102%，被財政部勒令停業三天，之後各分社發生擠兌，此次事件被稱為「[十信案](../Page/十信案.md "wikilink")」。

## 相關條目

  - [銀行存款準備金比率](../Page/銀行存款準備金比率.md "wikilink")
  - [金融危機](../Page/金融危機.md "wikilink")

[Category:银行业](../Category/银行业.md "wikilink")
[Category:金融危机](../Category/金融危机.md "wikilink")
[Category:景氣循環](../Category/景氣循環.md "wikilink")

1.   Reprinted (2000) *Fed Res Bank Mn Q Rev* **24** (1), 14–23.