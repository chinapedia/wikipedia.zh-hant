[Military_flag_of_the_Roman_Republic_(19th_century).svg](https://zh.wikipedia.org/wiki/File:Military_flag_of_the_Roman_Republic_\(19th_century\).svg "fig:Military_flag_of_the_Roman_Republic_(19th_century).svg")
**羅馬共和國**是一個僅存在四個月的國家，建立於1849年2月，當時的[教皇国被](../Page/教皇国.md "wikilink")、[朱塞佩·马志尼和](../Page/朱塞佩·马志尼.md "wikilink")推翻。

先前在[教皇国的統治下](../Page/教皇国.md "wikilink")，[猶太教雖具合法地位](../Page/猶太教.md "wikilink")，同時[猶太人可實行猶太教的傳統](../Page/猶太人.md "wikilink")，但在眾多方面卻受[歧視和反對](../Page/歧視.md "wikilink")；而除[天主教和猶太教外之宗教則遭禁止](../Page/天主教.md "wikilink")。於羅馬共和國成立後，其[憲法保障](../Page/憲法.md "wikilink")[宗教自由](../Page/宗教自由.md "wikilink")，而[教皇有治理](../Page/教皇.md "wikilink")[天主教教會的權力](../Page/天主教教會.md "wikilink")；此外，羅馬共和國憲法也是世界上第一部廢除[死刑的憲法](../Page/死刑.md "wikilink")。

## 歷史

1848年11月15日，[教皇国的司法](../Page/教皇国.md "wikilink")[大臣](../Page/大臣.md "wikilink")[沛黎洛·羅西被刺殺了](../Page/沛黎洛·羅西.md "wikilink")。第二天[羅馬的居民湧滿了街道](../Page/羅馬.md "wikilink")，各小型的組織要求組織一個[民主](../Page/民主.md "wikilink")[政府](../Page/政府.md "wikilink")、實施[社會改革並向](../Page/社會改革.md "wikilink")[奧地利帝國宣戰](../Page/奧地利帝國.md "wikilink")。[教皇](../Page/教皇.md "wikilink")[庇护九世趕緊化妝為一位普通的](../Page/庇护九世.md "wikilink")[教士離開羅馬](../Page/教士.md "wikilink")，直到其抵達[加埃塔](../Page/加埃塔.md "wikilink")——[兩西西里王國的堡壘](../Page/兩西西里王國.md "wikilink")。

由於羅馬突然失去政府，人民決定在1849年1月21日選舉組成新政府。因教皇禁止[天主教徒參與這場選舉](../Page/天主教徒.md "wikilink")，故競選的結果傾向於[共和黨](../Page/共和黨.md "wikilink")；到了2月8日，憲法大會宣告羅馬共和國正式成立。

[皮埃蒙特人在](../Page/皮埃蒙特.md "wikilink")[諾瓦拉戰役中失敗的新聞傳來後](../Page/諾瓦拉戰役.md "wikilink")，大會宣告了三人執政，三人分別是查尔斯·阿梅尔，马蒂亚蒙泰基和奥雷利奥萨利班，由穆扎热力带领并由奥雷利奥沙菲进行組織。

在羅馬共和國的統治下，教皇失去了國家統治者的角色，僅作[羅馬教會的領導者](../Page/羅馬教會.md "wikilink")。「三人執政」還通過取消重稅和新增工作職位給[失業者](../Page/失業.md "wikilink")。

[義大利](../Page/義大利.md "wikilink")[愛國者及軍人](../Page/愛國者.md "wikilink")[朱塞佩·加里波第組織](../Page/朱塞佩·加里波第.md "wikilink")「義大利軍隊」，當中有許多來自[山麓和](../Page/山麓.md "wikilink")[奧地利疆土的](../Page/奧地利.md "wikilink")[威尼吉亞話和](../Page/威尼吉亞話.md "wikilink")[倫巴第的新兵](../Page/倫巴第.md "wikilink")／[志願軍](../Page/志願軍.md "wikilink")。他聚集了約一千名的軍人，5月11日，登陸義大利[西西里島西部的](../Page/西西里島.md "wikilink")[馬沙拉](../Page/馬沙拉.md "wikilink")。糾澤佩·加里波底在5月13日擊敗了一支敵軍。次日，他以[维托里奥·艾曼努尔二世的名義自稱為西西里島的](../Page/维托里奥·艾曼努尔二世.md "wikilink")[獨裁者](../Page/獨裁者.md "wikilink")。

5月27日，糾澤佩·加里波底圍困西西里島的[首府](../Page/首府.md "wikilink")[巴勒莫市](../Page/巴勒莫市.md "wikilink")。

糾澤佩·加里波底有許多西西里島的人民支持，所以當地居民都反抗守城的西西里島軍。

## 参见

  - [罗马共和国 (18世纪)](../Page/罗马共和国_\(18世纪\).md "wikilink")

[category:義大利歷史](../Page/category:義大利歷史.md "wikilink")

[Category:已不存在的歐洲國家](../Category/已不存在的歐洲國家.md "wikilink")
[Category:已不存在的共和國](../Category/已不存在的共和國.md "wikilink")
[Category:1849年建立](../Category/1849年建立.md "wikilink")
[Category:1849年廢除](../Category/1849年廢除.md "wikilink")
[Category:短命國家](../Category/短命國家.md "wikilink")