[Corazón.svg](https://zh.wikipedia.org/wiki/File:Corazón.svg "fig:Corazón.svg")

**心形符號**（<font size=+2 color="red">♥</font>）是[愛的象徵](../Page/愛.md "wikilink")，一般認為這個符號源自[心臟](../Page/心臟.md "wikilink")。

## 历史

[Gold_ivy_wreath,_Mid-4th_c_BC_(8725620438).jpg](https://zh.wikipedia.org/wiki/File:Gold_ivy_wreath,_Mid-4th_c_BC_\(8725620438\).jpg "fig:Gold_ivy_wreath,_Mid-4th_c_BC_(8725620438).jpg")叶子的[花环](../Page/花环.md "wikilink")，象征酒神[狄俄尼索斯](../Page/狄俄尼索斯.md "wikilink")\]\]
[SwansHeart.jpg](https://zh.wikipedia.org/wiki/File:SwansHeart.jpg "fig:SwansHeart.jpg")组成的爱心，很多人视这为两人[相爱的象征](../Page/爱情.md "wikilink")\]\]

早在古希腊时期，就使用心形来表示一些植物的叶子。公元前4世纪[伊特鲁里亚文明用心形的](../Page/伊特鲁里亚文明.md "wikilink")[常春藤叶子象征生育](../Page/常春藤.md "wikilink")、忠诚和重生。[佛教用心形符號表示](../Page/佛教.md "wikilink")[遮羞布](../Page/遮羞布.md "wikilink")，具有启蒙的含义。2世纪时，这个符号在[昔兰尼象征用于](../Page/昔兰尼.md "wikilink")[避孕的](../Page/避孕.md "wikilink")[串叶松香草](../Page/串叶松香草.md "wikilink")。\[1\]

虽然在现代，心形符號被普遍認為是源自[心臟](../Page/心臟.md "wikilink")，但何时开始用这个符号表示心脏却并不清楚。一种说法是认为出自古希腊医生[盖伦的著作中对于人类心脏的描绘](../Page/盖伦.md "wikilink")。盖伦通过对[角斗士的解剖研究人体](../Page/角斗士.md "wikilink")，他把心脏画成了像[松果或是倒置的叶子的样子](../Page/松果.md "wikilink")。而之后一直到[中世纪](../Page/中世纪.md "wikilink")，由于宗教上禁止解剖，盖伦对心脏的描绘也就一直作为权威被普遍接受。\[2\]

[Roman_de_la_poire_heart_metaphor.jpg](https://zh.wikipedia.org/wiki/File:Roman_de_la_poire_heart_metaphor.jpg "fig:Roman_de_la_poire_heart_metaphor.jpg")》\]\]
13世纪之后，心形图案逐渐出现在艺术作品中。已知最早表示心脏的使用，是在1250年的一个法国爱情故事《[浪漫的梨](../Page/浪漫的梨.md "wikilink")》（*Roman
de la
poire*）中\[3\]。文艺复兴后，解剖学有了相当大的进步。之前描述的倒置的心也变正，成为了现在见到的心形符号。\[4\]而随着扑克牌的流行，代表红桃的心形符号也开始更为流行。

[Batoni_sacred_heart.jpg](https://zh.wikipedia.org/wiki/File:Batoni_sacred_heart.jpg "fig:Batoni_sacred_heart.jpg")画像\]\]
在1673年12月27日，修女[玛加利大声称遇到了](../Page/玛加利大.md "wikilink")[耶稣](../Page/耶稣.md "wikilink")，耶稣告诉她“她在他心头安息”，这个故事最终塑造了[圣心的典故](../Page/圣心.md "wikilink")，宣告了耶稣的心代表了他对人类的神圣之爱。\[5\]

[I_Love_New_York.svg](https://zh.wikipedia.org/wiki/File:I_Love_New_York.svg "fig:I_Love_New_York.svg")标志中的心形符号\]\]
到了19世纪，心形符号已经完全作为了心脏的符号，同时也象征着爱情。1977年，纽约市宣传的“[I ♥
NY](../Page/I_♥_NY.md "wikilink")”活动，是第一次把心形符号作为[动词](../Page/动词.md "wikilink")“爱”去使用。此后，“I
♥ \[X\]”成为了一种表达感情的文字方式。\[6\]

## 形狀

心形符號是由兩個半圓形突出部分拼在一起而成，上凹下尖。通常心形符號會以[紅色作為表示](../Page/紅色.md "wikilink")。

## Unicode

用來表示愛心的的一種常見方法是\<3。除此之外，[Unicode也收錄了許多的心形符號](../Page/Unicode.md "wikilink")：

<table>
<thead>
<tr class="header">
<th><p>符號</p></th>
<th><p>說明</p></th>
<th><p><a href="../Page/HTML.md" title="wikilink">HTML代碼</a></p></th>
<th><p><a href="../Page/Alt碼.md" title="wikilink">Alt碼</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>U+2661 红桃<a href="../Page/花色.md" title="wikilink">花色</a></p></td>
<td><p><code>♡</code> or <code>♡</code></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>♥</p></td>
<td><p>U+2665 黑桃花色</p></td>
<td><p><code>♥</code> or <code>♥</code> or <code>♥</code></p></td>
<td><p>Alt + 3</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>U+2764 粗实心心形</p></td>
<td><p><code>❤</code></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>U+2765 旋转粗实心心形加重号</p></td>
<td><p><code>❥</code></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>U+2763 粗心形叹号装饰</p></td>
<td><p><code>❣</code></p></td>
<td></td>
</tr>
</tbody>
</table>

另外，在[雜項符號和象形文字中也包含著以下的](../Page/雜項符號和象形文字.md "wikilink")[繪文字](../Page/繪文字.md "wikilink")：

| 符號 | 說明              | [HTML代碼](../Page/HTML.md "wikilink") |
| -- | --------------- | ------------------------------------ |
| 🎔  | U+1F394 尖端向左的心形 | `🎔`                                  |
| 💑  | U+1F491 情侶與愛心   | `💑`                                  |
| 💓  | U+1F493 跳動的愛心   | `💓`                                  |
| 💔  | U+1F494 破碎的愛心   | `💔`                                  |
| 💕  | U+1F495 兩個愛心    | `💕`                                  |
| 💖  | U+1F496 閃亮的愛心   | `💖`                                  |
| 💗  | U+1F497 增大的愛心   | `💗`                                  |
| 💘  | U+1F498 帶著箭的心形  | `💘`                                  |
| 💙  | U+1F499 藍色心形    | `💙`                                  |
| 💚  | U+1F49A 綠色心形    | `💚`                                  |
| 💛  | U+1F49B 黃色心形    | `💛`                                  |
| 💜  | U+1F49C 紫色心形    | `💜`                                  |
| 💝  | U+1F49D 帶著緞帶的心形 | `💝`                                  |
| 💞  | U+1F49E 互相旋轉的心形 | `💞`                                  |
| 💟  | U+1F49F 心形裝飾    | `💟`                                  |

## 用途

[Chocolate-dipped_heart-shaped_cookies.jpg](https://zh.wikipedia.org/wiki/File:Chocolate-dipped_heart-shaped_cookies.jpg "fig:Chocolate-dipped_heart-shaped_cookies.jpg")常以心形[巧克力](../Page/巧克力.md "wikilink")[餅乾向情人傳達甜蜜愛意](../Page/曲奇.md "wikilink")\]\]
[Heart-shaped_bowl.jpg](https://zh.wikipedia.org/wiki/File:Heart-shaped_bowl.jpg "fig:Heart-shaped_bowl.jpg")
心形符號被做為「愛」的代称，例如「我♥你」代表「我愛你」。

紅色的心形符號（紅心）也是[撲克牌的花色之一](../Page/撲克牌.md "wikilink")。

## 参考文献

## 參見

  - [心形手勢](../Page/心形手勢.md "wikilink")
  - [我愛你手勢](../Page/我愛你手勢.md "wikilink")

[category:愛](../Page/category:愛.md "wikilink")

[Category:符號](../Category/符號.md "wikilink")

1.

2.
3.

4.
5.
6.