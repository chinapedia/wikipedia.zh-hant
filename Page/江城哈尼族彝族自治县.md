**江城哈尼族彝族自治县**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[云南省](../Page/云南省.md "wikilink")[普洱市下属的一个](../Page/普洱市.md "wikilink")[自治县](../Page/自治县.md "wikilink")。在云南省南部，[李仙江南岸](../Page/李仙江.md "wikilink")。面积3476平方公里，境内多山、河和峡谷。

[元属元江路](../Page/元.md "wikilink")，[明属元江府](../Page/明.md "wikilink")，[清属普洱府](../Page/清.md "wikilink")，1929年设江城县，1954年5月18日建立自治区，1955年改自治县。

## 行政区划

下辖：\[1\] 。

## 参考资料

[江城哈尼族彝族自治县](../Category/江城哈尼族彝族自治县.md "wikilink")
[Category:普洱区县](../Category/普洱区县.md "wikilink")
[Category:云南省民族自治县](../Category/云南省民族自治县.md "wikilink")
[云](../Category/中国哈尼族自治县.md "wikilink")
[云](../Category/中国彝族自治县.md "wikilink")
[Category:国家级贫困县](../Category/国家级贫困县.md "wikilink")

1.