**宁洱哈尼族彝族自治县**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[云南省](../Page/云南省.md "wikilink")[普洱市下属的一个](../Page/普洱市.md "wikilink")[自治县](../Page/自治县.md "wikilink")。原名**普洱哈尼族彝族自治县**，2007年4月8日，更名为宁洱哈尼族彝族自治县。\[1\][面积](../Page/面积.md "wikilink")3,670-{[平方公里](../Page/平方公里.md "wikilink")}-，2003年[人口](../Page/人口.md "wikilink")19万人。县政府驻[宁洱镇](../Page/宁洱镇.md "wikilink")。

## 行政区划

下辖：\[2\] 。

## 参考文献

[宁洱哈尼族彝族自治县](../Category/宁洱哈尼族彝族自治县.md "wikilink")
[Category:普洱区县](../Category/普洱区县.md "wikilink")
[云](../Category/中国哈尼族自治县.md "wikilink")
[云](../Category/中国彝族自治县.md "wikilink")
[Category:国家级贫困县](../Category/国家级贫困县.md "wikilink")

1.  王江，赵宇思，[云南省思茅市2007年4月8日正式更名为普洱市](http://news.xinhuanet.com/local/2007-04/07/content_5947029.htm)，[新华网](../Page/新华网.md "wikilink")

2.