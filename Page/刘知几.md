**劉知幾**（），字**子玄**，出于[彭城刘氏](../Page/彭城刘氏.md "wikilink")，[唐代官员](../Page/唐代.md "wikilink")、史學家。

## 生平

[永隆年間](../Page/永隆.md "wikilink")（680年）以弱冠舉[進士](../Page/進士.md "wikilink")，官至左[散騎常侍](../Page/散騎常侍.md "wikilink")。他歷任著作佐郎、[中書舍人](../Page/中書舍人.md "wikilink")、著作郎，又撰起居注，兼修国史二十餘年。期間深感宰相大臣監修，多所干預，不能秉筆直書其言。

[景龍二年](../Page/景龍.md "wikilink")（708年）辭去史職，從事私人修史工作。劉知幾一生從事史書編輯工作，著有《刘氏家乘》十五卷、《刘氏谱考》三卷、《[史通](../Page/史通.md "wikilink")》二十卷、《[睿宗](../Page/唐睿宗.md "wikilink")[实录](../Page/实录.md "wikilink")》十卷、《刘子玄集》三十卷；合著有《[三教珠英](../Page/三教珠英.md "wikilink")》一千三百一十三卷、《姓族系录》二百卷、《唐书》八十卷、《[高宗实录](../Page/唐高宗.md "wikilink")》二十卷、《[中宗实录](../Page/唐中宗.md "wikilink")》二十卷、《[则天皇后实录](../Page/武则天.md "wikilink")》三十卷。仅有《[史通](../Page/史通.md "wikilink")》传世，對中國唐朝以前的史籍作了全面的分析和批評，是中國第一部史學理論專著。他在史通中提出史家須有三長：史才、史学、史识；对著史强调直笔，提倡“不掩恶、不属善”、“爱而知其丑、憎而知其善”的态度。

[開元九年](../Page/開元.md "wikilink")（721年），六十歲的他時因營救長子犯罪流配一事而被[唐玄宗貶為](../Page/唐玄宗.md "wikilink")[安州都督府](../Page/安州.md "wikilink")[別駕](../Page/別駕.md "wikilink")，不久便去世，享年六十一歲。有六子：[劉貺](../Page/劉貺.md "wikilink")、[劉餗](../Page/劉餗.md "wikilink")、[劉匯](../Page/劉匯.md "wikilink")、[劉秩](../Page/劉秩.md "wikilink")、[劉迅](../Page/劉迅.md "wikilink")、[劉迥](../Page/劉迥.md "wikilink")。有一女劉氏（714年—771年三月）嫁[李神通玄孙](../Page/李神通.md "wikilink")[洺州](../Page/洺州.md "wikilink")[清漳县尉](../Page/清漳.md "wikilink")[李衡](../Page/李衡.md "wikilink")。

## 关于刘氏源流的考证

刘知几考证刘氏不是[尧的后裔](../Page/尧.md "wikilink")，而是[陆终的苗裔](../Page/陆终.md "wikilink")，彭城丛亭里刘氏也不是楚元王[刘交的后裔](../Page/刘交.md "wikilink")，而是[汉宣帝之子楚孝王](../Page/汉宣帝.md "wikilink")[刘嚣曾孙居巢侯](../Page/刘嚣.md "wikilink")[刘恺的后裔](../Page/刘恺.md "wikilink")\[1\]\[2\]。

## 参考文献

## 參考書目

  - 《[舊唐書](../Page/舊唐書.md "wikilink")》[卷一百二．列傳第五十二：馬懷素 褚無量 **劉子玄** 徐堅
    元行沖 吳兢 韋述](../Page/Wikisource:zh:舊唐書/卷102.md "wikilink")
  - 《[新唐書](../Page/新唐書.md "wikilink")》[卷一百三十二．列傳第五十七：**劉**吳韋蔣柳沈](../Page/Wikisource:zh:新唐書/卷132.md "wikilink")
  - 賴瑞和：〈[劉知幾與唐代的書和手抄本：一個物質文化的觀點](http://www.his.ntnu.edu.tw/files/publish/801_2848ab5e.pdf)〉。

## 外部連接

[category:唐朝進士](../Page/category:唐朝進士.md "wikilink")
[category:唐朝官员](../Page/category:唐朝官员.md "wikilink")
[category:唐朝歷史學家](../Page/category:唐朝歷史學家.md "wikilink")
[category:唐朝县子](../Page/category:唐朝县子.md "wikilink")

[Z](../Category/彭城丛亭里刘氏.md "wikilink")

1.  《唐会要·卷三十六》：长安四年，凤阁舍人刘知几。撰《刘氏》三卷，推汉氏为陆终苗裔，非尧之后。彭城丛亭里诸刘，出自宣帝子楚孝王嚣曾孙司徒居巢侯刘恺之后，不承楚元王交。皆按据明白前代所误，虽为流俗所讥，学者服其该博。
2.  《新唐书·卷一百三十二·列传第五十七》：子玄内负有所未尽，乃委国史于吴兢，别撰《刘氏家史》及《谱考》。上推汉为陆终苗裔，非尧后；彭城丛亭里诸刘，出楚孝王嚣曾孙居巢侯般，不承元王。按据明审，议者高其博。