**韓國聯合通訊社**，又譯**聯合通訊社**，簡稱**韓聯社**、**YNA**，是[韓國的官方](../Page/韓國.md "wikilink")[通訊社](../Page/通訊社.md "wikilink")，也是韓國最大的通訊社。韓聯社的總部在[首爾](../Page/首爾.md "wikilink")，為韓國的[報紙](../Page/報紙.md "wikilink")、[廣播](../Page/廣播.md "wikilink")、[電視和其他媒體提供新聞](../Page/電視.md "wikilink")。

## 歷史

韓聯社建立於1980年12月19日，前身是原合同新聞社（Hapdong News Agency）和東洋通信社（Orient
Press）。韓聯社与65家非朝鮮半島的通訊社簽訂了新聞交換協定；2002年与[朝鮮中央通訊社簽訂了新聞交換協定](../Page/朝鮮中央通訊社.md "wikilink")。\[1\]韓聯社在其網站上以朝鮮語、漢語、英語、日語、西班牙語、阿拉伯語和法語提供免費獲取的新聞摘要。

## 外部連結

  - [韓國聯合通訊社](http://www.yonhapnews.co.kr/)官方網站

  -
  -
  -
  -
  - [韓國聯合通訊社](https://plus.google.com/114892275910835696049)的[Google+官方帳號](../Page/Google+.md "wikilink")

  - [韓國聯合通訊社](https://story.kakao.com/ch/yonhapmedia)的Kakao Story官方部落格

## 參考文獻

<references/>

{{-}}

[Category:韓國媒體](../Category/韓國媒體.md "wikilink")
[Category:YTN集團](../Category/YTN集團.md "wikilink")
[Category:国家通讯社](../Category/国家通讯社.md "wikilink")
[Category:韩国文化之最](../Category/韩国文化之最.md "wikilink")

1.  <http://english.yonhapnews.co.kr/AboutUs/index.html> About Us