**荒木经惟**（），[日本](../Page/日本.md "wikilink")[攝影師](../Page/攝影師.md "wikilink")、[當代藝術家](../Page/當代藝術.md "wikilink")，出生於[東京府](../Page/東京府.md "wikilink")[東京市](../Page/東京市.md "wikilink")[下谷区](../Page/下谷区.md "wikilink")[三之輪](../Page/三之輪.md "wikilink")（今
[東京都](../Page/東京都.md "wikilink")[台東區三之輪](../Page/台東區.md "wikilink")）。

## 生平

荒木经惟在上大學的時候學過[攝影](../Page/攝影.md "wikilink")，1963年於[千葉大学攝影印刷工学科畢業後曾在日本公司](../Page/千葉大学.md "wikilink")[電通](../Page/電通.md "wikilink")(Dentsu)廣告代理工作過，在那裡他遇見了他的未來妻子、日本[隨筆作家](../Page/隨筆作家.md "wikilink")[荒木陽子](../Page/荒木陽子.md "wikilink")（舊姓：青木，1947年－1990年）。他們於1971年結婚之後,
荒木出版了一本他為妻子在[蜜月旅行期間拍攝的畫冊](../Page/蜜月旅行.md "wikilink")《[多愁之旅](../Page/多愁之旅.md "wikilink")》（）,
1990年陽子去世, 荒木又出版了一本為其妻子在[弥留之际拍攝的畫冊](../Page/弥留.md "wikilink")《冬之旅》（）。

荒木经惟出道以來出版了超過350本出版物，且數量每年仍在增長，因此他被認為是日本乃至全世界最多產的藝術家之一。在他的作品中有不少是性愛題材的，甚至有些被稱為[色情圖片](../Page/色情圖片.md "wikilink")。荒木经惟最受歡迎的攝影作品出版物有《多愁之旅》、《[東京幸運洞](../Page/東京幸運洞.md "wikilink")》（*Tokyo
Lucky Hole*）和《Shino》。

[冰島音樂人](../Page/冰島.md "wikilink")[碧玉](../Page/碧玉_\(歌手\).md "wikilink")（Björk
Guðmundsdóttir）是荒木经惟作品的仰慕者，也曾為他做過[模特](../Page/模特.md "wikilink")。

2005年，[美國](../Page/美國.md "wikilink")[導演](../Page/導演.md "wikilink")[特拉維斯·克洛澤](../Page/特拉維斯·克洛澤.md "wikilink")（Travis
Klose）為荒木经惟拍攝記錄片《[Arakimentari](../Page/Arakimentari.md "wikilink")》。

## 外部链接

  - [官方網站](http://www.arakinobuyoshi.com/index.html)

[Category:日本当代艺术家](../Category/日本当代艺术家.md "wikilink")
[Category:日本攝影師](../Category/日本攝影師.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")
[Category:千葉大學校友](../Category/千葉大學校友.md "wikilink")