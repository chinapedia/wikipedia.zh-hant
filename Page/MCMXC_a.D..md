**MCMXC
a.D.**是音乐团体[Enigma](../Page/謎_\(音樂團體\).md "wikilink")（亦翻译成[謎](../Page/謎_\(音樂團體\).md "wikilink")）的首张[专辑](../Page/音樂專輯.md "wikilink")，出版于1990年。专辑名的意思是[罗马数字](../Page/罗马数字.md "wikilink")1990。该专辑是音乐工业史上最受欢迎的CD之一，销量达到了1400万张。

[Category:1990年音樂專輯](../Category/1990年音樂專輯.md "wikilink")