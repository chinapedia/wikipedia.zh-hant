**日本龍屬**（屬名：*Nipponosaurus*）屬於[鴨嘴龍科的](../Page/鴨嘴龍科.md "wikilink")[賴氏龍亞科](../Page/賴氏龍亞科.md "wikilink")，生存於[白堊紀晚期的](../Page/白堊紀.md "wikilink")[亞洲](../Page/亞洲.md "wikilink")。

[正模標本](../Page/正模標本.md "wikilink")（編號UHR
6590，登記於[北海道大學名內](../Page/北海道大學.md "wikilink")）是在1934年11月發現於[樺太廳](../Page/樺太廳.md "wikilink")（現在的[庫頁島](../Page/庫頁島.md "wikilink")），川上炭鑛在建設一家醫院時發現這些化石，而在1937年夏季發現了另外的化石。在1936年，北海道大學的長尾巧教授將這些化石進行敘述、命名，[模式種是](../Page/模式種.md "wikilink")**薩哈林日本龍**（*N.
sachalinensis*，又譯**庫頁島日本龍**）。日本龍是第一個發現於日本領土的恐龍，但在[第二次世界大戰後](../Page/第二次世界大戰.md "wikilink")，[蘇聯占領庫頁島南部](../Page/蘇聯.md "wikilink")。
[Nipponosaurus.jpg](https://zh.wikipedia.org/wiki/File:Nipponosaurus.jpg "fig:Nipponosaurus.jpg")
日本龍的[正模標本](../Page/正模標本.md "wikilink")（編號UHR
6590）包含：左[上頜骨與左](../Page/上頜骨.md "wikilink")[齒骨](../Page/齒骨.md "wikilink")、[頂骨](../Page/頂骨.md "wikilink")、其他部分的零散頭顱骨、13節[頸椎](../Page/頸椎.md "wikilink")、六節[背椎](../Page/背椎.md "wikilink")、兩節[薦椎](../Page/薦椎.md "wikilink")、以及35節[尾椎](../Page/尾椎.md "wikilink")、左[肩胛骨](../Page/肩胛骨.md "wikilink")、左右[肱骨的末端](../Page/肱骨.md "wikilink")、前肢的其他部位、一個[坐骨](../Page/坐骨.md "wikilink")、左[腸骨](../Page/腸骨.md "wikilink")、以及大部分後肢。這些標本被發現於[上蝦夷組](../Page/上蝦夷組.md "wikilink")，年代為晚[三冬階到早](../Page/三冬階.md "wikilink")[坎潘階](../Page/坎潘階.md "wikilink")。雖然這些骨頭的保存狀態很差，但已保存了60%的身體部份。日本龍是研究最缺乏的[賴氏龍亞科之一](../Page/賴氏龍亞科.md "wikilink")。

在2004年，日本龍被Suzuki等人重新敘述，他們將日本龍鑑定為：[上隅骨的結實冠突](../Page/上隅骨.md "wikilink")、脊軸的[神經棘只有稍微發展](../Page/神經棘.md "wikilink")、第四指的第一節掌骨後緣有大幅彎曲。

Suzuki等人認為該正模標本來自於一個接近成年個體，身長約7.6公尺。他們也發表[親緣分支分類法分析](../Page/親緣分支分類法.md "wikilink")，發現薩哈林日本龍的親緣關係非常接近[北美洲的](../Page/北美洲.md "wikilink")[亞冠龍](../Page/亞冠龍.md "wikilink")。他們認為日本龍的狀態不是[疑名](../Page/疑名.md "wikilink")。

## 參考資料

  - Nagao, T. 1936. *Nipponosaurus sachalinensis* - A new genus and
    species of trachodont dinosaur from Japanese Saghalien. *J. Faculty
    Sci. Hokkaido Imperial Univ. Ser.* IV (2): 187-220
  - Nagao, T. 1938. On the limb bones of *Nipponosaurus sachaliensis*
    Nagao, a Japanese hadrosaurian dinosaur. *Annot. Zool. Japan*
    17(3/4):312-318.
  - Suzuki D., Weishampel D. B., and Minoura N. 2004. *Nippponosaurus
    sachalinensis* (Dinosauria; Ornithopoda): anatomy and systematic
    position within Hadrosauridae. *Journal of Vertebrate Paleoentology*
    24(1):145-164.

## 外部链接

  - [恐龙.NET](https://web.archive.org/web/20170527222928/http://dinosaur.net.cn/)——中国古生物门户网站。

[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")
[Category:賴氏龍亞科](../Category/賴氏龍亞科.md "wikilink")
[Category:亞洲恐龍](../Category/亞洲恐龍.md "wikilink")