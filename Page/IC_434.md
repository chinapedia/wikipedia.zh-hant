{{ Emission nebula | | image = | name = IC 434 | type =
[發射星雲](../Page/發射星雲.md "wikilink") | epoch = J2000.0 | ra =
| dec =  | dist_ly = | appmag_v = | size_v = | constellation =
[獵戶座](../Page/獵戶座.md "wikilink") | radius_ly = | absmag_v = |
notes = | names =}}

**IC
434**是位於[獵戶座的一個明亮](../Page/獵戶座.md "wikilink")[發射星雲](../Page/發射星雲.md "wikilink")，它於1786年2月1日被[威廉·赫歇爾發現](../Page/威廉·赫歇爾.md "wikilink")。它位於[獵戶腰帶最東邊的](../Page/獵戶腰帶.md "wikilink")[參宿一旁邊](../Page/參宿一.md "wikilink")，是一片細長且模糊不清的地區。IC
434因為襯托出著名的[馬頭星雲](../Page/馬頭星雲.md "wikilink")，因此它比[IC
星表中的其它天體更為著名](../Page/索引星表.md "wikilink")。

## 馬頭星雲

**马头星云**（亦称**巴纳德33**，是明亮的**[IC](../Page/IC天体.md "wikilink")
434**內的一個暗星雲）是位于[猎户座的](../Page/猎户座.md "wikilink")[暗星云](../Page/暗星云.md "wikilink")，馬頭星雲離地球1,500[光年](../Page/光年.md "wikilink")，从[地球看它位于](../Page/地球.md "wikilink")[猎户座ζ下方](../Page/猎户座ζ.md "wikilink")，視星等8.3等，肉眼不能見。因形狀十分像馬頭的[剪影](../Page/剪影.md "wikilink")，故有馬頭星雲的稱號。1888年[哈佛大學天文台拍下的照片首次發現這個不同尋常形狀的星雲](../Page/哈佛大學.md "wikilink")。

该星云後正在增长的红色部分是由被附近[猎户座σ电离的](../Page/猎户座σ.md "wikilink")[氢气所产生](../Page/氢.md "wikilink")。马头星云黑暗部分由厚厚的暗星雲造成，头颈部下方对左边撒下阴影。离开星云的气流因为[磁场原因而呈现出漏斗状](../Page/磁场.md "wikilink")。马头星云基部的亮点是正在生成的新恆星。

## 外部連結

  - [The Horsehead Nebula @ The Electronic
    Sky](http://www.glyphweb.com/esky/nebulae/ic434.html)
  - [哈勃太空望远镜观测IC 434](http://hubblesite.org/newscenter/newsdesk/archive/releases/2001/12/image/a)

[Category:發射星雲](../Category/發射星雲.md "wikilink")
[0434](../Category/IC天體.md "wikilink")
[Category:暗星云](../Category/暗星云.md "wikilink")
[Category:猎户座](../Category/猎户座.md "wikilink")
[Category:獵戶座分子雲複合體](../Category/獵戶座分子雲複合體.md "wikilink")