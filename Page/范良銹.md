**范良銹**，桃園新屋人，曾任[行政院公共工程委員會主任委員](../Page/行政院公共工程委員會.md "wikilink")，總統府國策顧問。

## 學歷

  - 國立交通大學運輸工程研究所碩士
  - 中國文化大學市政學系學士

## 經歷

  - [台北捷運公司副總經理](../Page/台北捷運公司.md "wikilink")
  - [台北市政府捷運工程局副局長](../Page/台北市政府捷運工程局.md "wikilink")
  - 台北市政府捷運工程局代理局長
  - 台北市政府捷運工程局局長
  - [台北市政府副秘書長](../Page/台北市政府.md "wikilink")
  - 桃園縣副縣長
  - [公共工程委員會主任委員](../Page/公共工程委員會.md "wikilink")
  - [八八水災中央防災中心指揮官](../Page/八八水災.md "wikilink")
  - 總統府國策顧問

## 爭議

  - 2008年9月8日：政務委員兼公共工程委員會主委范良銹主持「[台北縣特二號快速道路三之二標](../Page/台北縣特二號快速道路.md "wikilink")」公聽會，與會人士表示應保持[湳仔當地河岸景觀](../Page/湳仔.md "wikilink")，范良銹回答
    :「台北板橋以前是個湖，叫做[康熙湖](../Page/康熙台北湖.md "wikilink")，不然你回到（[清朝](../Page/台灣清治時期.md "wikilink")）那個時代好了。」「那麼喜歡[冬山河](../Page/冬山河.md "wikilink")，那你搬過去住好了。」
  - 范良銹接受媒體訪問，強調和范佐憲可能是宗親，但從來不認識范佐憲；范良銹表示
    :「良字輩的下一代是佐字輩啦！我們來台是17代啦！但是我從來不知道有一個范佐憲這個人，也從來沒有碰過這人。」\[1\]

|- |colspan="3"
style="text-align:center;"|**[ROC_Executive_Yuan_Logo.svg](https://zh.wikipedia.org/wiki/File:ROC_Executive_Yuan_Logo.svg "fig:ROC_Executive_Yuan_Logo.svg")[行政院](../Page/行政院.md "wikilink")**
|-

## 外部連結

  - [公共工程委員會 -
    主任委員](https://web.archive.org/web/20080503235509/http://www.pcc.gov.tw/cht/index.php)

[Category:中華民國總統府國策顧問](../Category/中華民國總統府國策顧問.md "wikilink")
[Category:中華民國行政院公共工程委員會主任委員](../Category/中華民國行政院公共工程委員會主任委員.md "wikilink")
[離](../Category/臺北市工務局局長.md "wikilink")
[D](../Category/臺北市工務局.md "wikilink")
[離](../Category/臺北市捷運工程局局長.md "wikilink")
[D](../Category/臺北市捷運工程局.md "wikilink")
[Category:桃園縣副縣長](../Category/桃園縣副縣長.md "wikilink")
[Category:中國文化大學校友](../Category/中國文化大學校友.md "wikilink")
[Category:國立交通大學校友](../Category/國立交通大學校友.md "wikilink")
[Category:客家裔臺灣人](../Category/客家裔臺灣人.md "wikilink")
[Category:新屋人](../Category/新屋人.md "wikilink")
[L良](../Category/范姓.md "wikilink")

1.