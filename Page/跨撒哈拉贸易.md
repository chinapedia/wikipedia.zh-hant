[Great_Mosque_of_Djenné_1.jpg](https://zh.wikipedia.org/wiki/File:Great_Mosque_of_Djenné_1.jpg "fig:Great_Mosque_of_Djenné_1.jpg")，建立於公元800年,當時是一個重要的貿易中心，現在成為[世界文化遺產](../Page/世界文化遺產.md "wikilink")\]\]
**跨撒哈拉貿易**界于北非地中海沿岸國家及西非國家之間，是條從[第八世紀到](../Page/八世紀.md "wikilink")[十六世紀末間重要的](../Page/十六世紀.md "wikilink")[貿易路線](../Page/貿易路線.md "wikilink")。7世纪以后，阿拉伯人来到北非并控制了撒哈拉商道贸易。8\~11世纪为商道贸易的发展时期，11世纪中叶\~16世纪末为全盛时期，此后走向低潮。我們在探討商隊路線的位置以及貿易流量的興衰前必須先知道所謂跨撒哈拉貿易到底是如何存在的。危險又空曠的[撒哈拉沙漠隔開了地中海世界的貿易和尼日尔的貿易](../Page/撒哈拉沙漠.md "wikilink")。[費爾南·布勞岱爾在](../Page/費爾南·布勞岱爾.md "wikilink")*The
Perspective of the
World*一書指出：“在這種地區，像是跨大西洋一樣，除非說有特殊狀況，如有一定的预期利润，才值得穿越。但与跨大西洋贸易不同的是，撒哈拉沙漠一直都有一群人以當地為基地做區域性的貿易。”

跨撒哈拉貿易是靠由[阿拉伯](../Page/阿拉伯.md "wikilink")[駱駝组成的](../Page/駱駝.md "wikilink")[商隊進行](../Page/商隊.md "wikilink")。這些駱駝在编入商隊前會事先在北非[馬格裏布或西非](../Page/馬格裏布.md "wikilink")[薩赫勒平原上以數個月的時間養肥](../Page/薩赫勒.md "wikilink")。根據使用過商隊的阿拉伯商人[-{zh-hans:伊本·白图泰;
zh-hant:伊本·巴圖塔;}-的說法](../Page/伊本·白图泰.md "wikilink")，每個商隊平均有一千隻駱駝，有時甚至用到12,000隻。商隊的嚮導是高薪請來的[柏柏爾族人](../Page/柏柏爾.md "wikilink")，因為他們清楚撒哈拉沙漠的情況，而且在通过其他[遊牧部落时可以確保商隊的安全](../Page/遊牧部落.md "wikilink")。商隊的生存危机重重，需要倚靠團隊的和諧運作。通常因為商隊無法一次攜帶全程所需的水，擅跑的人會先被派往前方的[綠洲](../Page/綠洲.md "wikilink")，如此可以把水在商隊抵達綠洲前數天就運給商隊。

## 早期的跨撒哈拉贸易

尼罗河谷附近的小型贸易路线已经存在了数千年，但是在驯化骆驼前，横穿撒哈拉沙漠是非常困难的。一些物品和原材料在远离它们原产地的地方被发现，这在考古学上证明了特别是在最西端,沙漠最狭窄的地方有貿易的行為。在古典文学中，也有一些貿易接触的例子,
好比說奥达哥斯特(Aoudaghost)城的形成發展可能來自於于這些有限的貿易，但這不能證明所有在這區域的都市化都是由於這小型貿易而產生的。

在南部撒哈拉当时的[石窟壁画描绘了](../Page/石窟壁画.md "wikilink")[马拉](../Page/马.md "wikilink")[马车的场景](../Page/马车.md "wikilink")，引发了[马车在当时被应用的猜测](../Page/马车.md "wikilink")。但是，在这一区域并没有发现同时代的马的骸骨，并且由于负载较小，马车也不是合适的贸易运载方式。

这个地区最早的驯养骆驼的证据可以追溯至[第三世紀](../Page/三世纪.md "wikilink")。因[柏柏爾人使用骆驼使得撒哈拉范围内的贸易能變得更频繁](../Page/柏柏爾.md "wikilink")，但日常的贸易路线直到[第七世纪](../Page/七世纪.md "wikilink")
[伊斯蘭教開始傳到西非才形成](../Page/伊斯蘭教.md "wikilink")。发展出的贸易路线主要有两条:
第一条路线穿越沙漠西部，从[摩洛哥至](../Page/摩洛哥.md "wikilink")[-{zh-hans:尼日尔;
zh-hant:尼日;}-河湾](../Page/尼日爾河.md "wikilink")；第二条由現代[突尼西亞至](../Page/突尼西亞.md "wikilink")[乍得湖区域](../Page/乍得湖.md "wikilink")。这些路线相对较短，而且被路途上經過的少數绿洲网络限制而難以更動。在这个区域以东，利比亚以南则是無法通過,没有绿洲且充满强烈沙漠风暴的區域。从尼日尔河湾通往[埃及的路线在](../Page/埃及.md "wikilink")[第十世纪因危险而被废弃](../Page/十世纪.md "wikilink")。

## 中世纪的跨撒哈拉贸易

[Niger_saharan_medieval_trade_routes.PNG](https://zh.wikipedia.org/wiki/File:Niger_saharan_medieval_trade_routes.PNG "fig:Niger_saharan_medieval_trade_routes.PNG")
位于今日[毛利塔尼亚南部的加纳帝国伴随着撒哈拉贸易的發展而崛起](../Page/毛利塔尼亚.md "wikilink")。撒哈拉貿易中主要的有金鹽貿易,
地中海贸易多鹽缺金, 而西非國家則是多金而缺盐。奴隶贸易也很重要，因为很多黑人被送到北面做家庭奴隸,
而西非国家买进训练有素的奴隶军。因這些貿易,
几条商路被开辟出来了，最重要的可能就要数终止于[西吉尔马萨和位于現在](../Page/西吉尔马萨.md "wikilink")[摩洛哥的东部的](../Page/摩洛哥.md "wikilink")[Ifriqua的商路了](../Page/Ifriqua.md "wikilink")。和其他北非城市一样，在那里柏柏尔商人因為和伊斯兰教的接触增多而促進傳教。
在[第八世紀的時候](../Page/八世紀.md "wikilink"), 穆斯林到了迦納, 使很多迦納很多人改信伊斯蘭教,
這很可能也造成對迦納帝国的貿易有優惠. 在約1050年, 迦納佔領了奥达哥斯特城,
但在Bure附近的新金礦降低了在城內的貿易,
反而使[索索人受益](../Page/索索.md "wikilink").
索索人後來成立了[-{zh-hans:馬里;
zh-hant:馬利;}-王國](../Page/馬利王國.md "wikilink")。

像加纳一样，马里也是个穆斯林王国。在它的统治下，金盐贸易继续进行着。
另外，相对次要的贸易货物是奴隶，来自南方的可乐果，玻璃珠，和来自北方作为流通货币的[寶螺](../Page/寶螺.md "wikilink")。
在馬里王國的統治下, 在[-{zh-hans:尼日爾;
zh-hant:尼日;}-河岸的大城](../Page/尼日爾河.md "wikilink")
—包括 [加奧和](../Page/加奧.md "wikilink")[-{zh-hans:杰內;
zh-hant:傑內;}-](../Page/杰內.md "wikilink")—
都興盛起來了,特別像[廷巴克图的財富還在](../Page/廷巴克图.md "wikilink")[歐洲出名](../Page/歐洲.md "wikilink")。
在西南非大草原及森林的交界區還發展了重要的貿易中心, 像是[贝格霍和](../Page/贝格霍.md "wikilink")[Bono
Manso](../Page/Bono_Manso.md "wikilink") (現在的加納)
還有[邦杜庫](../Page/邦杜庫.md "wikilink")
(現在的象牙海岸)。在西方的交易路線持續擁有其重要的地位,
像在現在茅利塔尼亞境內的[瓦丹](../Page/瓦丹.md "wikilink"),
[瓦拉塔](../Page/瓦拉塔.md "wikilink") 和
[欣蓋提成為主要的貿易中心](../Page/欣蓋提.md "wikilink"),而在[Assodé](../Page/Assodé.md "wikilink")(現在[阿加德茲](../Page/阿加德茲.md "wikilink"))境內的[圖瓦雷克城市則是沿著東方的交易路線發展](../Page/圖瓦雷克.md "wikilink"),
而成為現在的[-{zh-hans:尼日爾; zh-hant:尼日;}-](../Page/尼日爾.md "wikilink")。

东部跨撒哈拉贸易路線使得地处-{zh-hans:乍得;
zh-hant:查德;}-湖地区长久的[卡奈姆－博尔努帝国兴起](../Page/卡奈姆－博尔努.md "wikilink")。這條商路并不便捷，只有当西边有動乱的时候（比如[阿蒙哈德占领期间](../Page/阿蒙哈德.md "wikilink")）才发挥作用。

## 跨撒哈拉贸易的衰落

非洲西海岸的[葡萄牙旅行者们为欧洲和西非的贸易开拓了一条新的道路](../Page/葡萄牙.md "wikilink")。
在[十六世紀初期](../Page/十六世紀.md "wikilink")，欧洲人已经在海岸地区牢牢扎根，对西非来说，与富裕的欧洲人做生意成为头等重要的大事，北非在政治与经济方面的重要性均在下降。虽然穿越撒哈拉沙漠的旅程仍然是漫长而充满危险的，但对跨撒哈拉贸易的主要打击是1591-1592年的[摩洛哥战争](../Page/摩洛哥战争.md "wikilink")。摩洛哥军队穿越撒哈拉沙漠袭击廷巴克图(Timbuktu)，加奥(Gao)以及其他一些重要的贸易中心。他们破坏建筑和财物，并且流放市民。这场对贸易的破坏致使这些城市的重要性戏剧般的下降，最终由于仇恨导致贸易减少了相当的幅度。

雖然貿易金額大幅地減少,貫通撒哈拉的貿易仍然繼續。可是通往西非海岸的貿易路線變得越來越簡易,特別是經過[法國](../Page/法國.md "wikilink")
在1890年向西非荒漠草原的侵略及後來非洲內陸[鐵路的建築](../Page/鐵路.md "wikilink")。一条计划中的从[达喀尔经由尼日尔到](../Page/达喀尔.md "wikilink")[阿尔及尔的铁路线最终没有建造](../Page/阿尔及尔.md "wikilink")。随着二十世纪六十年代此地区国家的纷纷独立，众多的国境线造成北-南之间的道路难以畅通。这些国家的政府对西撒哈拉和中撒哈拉的柏柏尔人的民族独立运动怀有敌意。因此，这些政府几乎没有做出任何努力去保持或支持跨撒哈拉贸易。二十世纪九十年代的“柏柏尔人大叛乱”和[阿尔及利亚内战使得很多道路被关闭](../Page/阿尔及利亚内战.md "wikilink")，这进一步中断了这条跨撒哈拉的贸易之路。

如今，穿越撒哈拉的少数柏油碎石路和同样少数的[卡车仍然进行着跨撒哈拉贸易](../Page/卡车.md "wikilink")，特别是[燃料和盐](../Page/燃料.md "wikilink")。在传统的商路上基本上已经见不到骆驼了，但从[阿加德兹到](../Page/阿加德兹.md "wikilink")[比尔马和](../Page/比尔马.md "wikilink")[廷巴克图到Taoudenni的短途路线上](../Page/廷巴克图.md "wikilink")，骆驼还是经常被使用（雖然數量較少）。图阿雷格人中的一些仍然延用着这些传统的商路，经常每年骑骆驼6个月跋涉1500英里穿越撒哈拉，从沙漠中心向边缘贩盐。

## 參考資料

  - M'hammad Sabour and Knut S. Vikør (eds), *[Ethnic Encounter
    和文化变迁](http://www.hf-fak.uib.no/institutter/smi/paj/Masonen.html)*,
    Bergen, 1997年,
    [1](http://www.google.co.uk/search?q=cache:5XWSfLiHmtMJ:www.uta.fi/~hipema/trans.htm+trans-Saharan-trade&hl=en)
    Google页面缓存，更新日期2005年一月。
  - [7-14世纪跨撒哈拉黄金贸易](http://www.metmuseum.org/toah/hd/gold/hd_gold.htm)
    来源：[纽约现代艺术博物馆](../Page/纽约现代艺术博物馆.md "wikilink")
  - Kevin Shillington (eds), ["Tuareg: Takedda
    和跨撒哈拉贸易"](http://www.routledge-ny.com/ref/africanhist/tuareg.html)
    来源：*非洲历史百科全书*, 菲茨罗伊 迪尔伯恩, 2004年, ISBN 1-57958-245-1
  - Lewicki T., "撒哈拉和撒哈拉人在南北关系中的角色", 来源*联合国教科文组织非洲历史第三卷*, 加利福尼亚大学出版社，
    1994, ISBN 92-3-601709-6
  - [費爾南布·羅代爾](../Page/費爾南布·羅代爾.md "wikilink"), *The Perspective of the
    World,* 第三卷*Civilization and Capitalism* 1984 (法语版1979)

## 註解

請觀看 [國家地理頻道](../Page/國家地理頻道.md "wikilink") 系列 *非洲* (2001), Episode 2
"Desert Odyssey"。此集跟随一个图阿雷格人的部落经6个月乘骆驼穿越撒哈拉沙漠。

[Category:非洲经济史](../Category/非洲经济史.md "wikilink")
[Category:中世纪社会经济](../Category/中世纪社会经济.md "wikilink")
[Category:国际贸易](../Category/国际贸易.md "wikilink")