**萨克拉门托縣**（）是[美國](../Page/美國.md "wikilink")[加利福尼亞州的一個縣](../Page/加利福尼亞州.md "wikilink")，位於[中央谷地](../Page/中央谷地.md "wikilink")。面積2,578平方公里，根據[美國2000年人口普查數字](../Page/美國2000年人口普查.md "wikilink")，共有人口1,223,499人。縣治[沙加緬度](../Page/沙加緬度_\(加利福尼亞州\).md "wikilink")（Sacramento）也是州的首府。

成立於1850年，是加州建州時的縣之一。縣名來自[薩克拉門托河](../Page/薩克拉門托河.md "wikilink")（薩克拉門托是[西班牙語](../Page/西班牙語.md "wikilink")[聖體聖事的意思](../Page/聖體聖事.md "wikilink")）。

## 人口集居地

### 建制市 (Incorporated cities)

  - [柑橘高地](../Page/柑橘高地_\(加利福尼亚州\).md "wikilink") (Citrus
    Heights，建制於1997年)
  - [麋鹿林](../Page/麋鹿林_\(加利福尼亞州\).md "wikilink") (Elk Grove，建制於2000年)
  - [佛森](../Page/佛森_\(加利福尼亞州\).md "wikilink") (Folsom，建制於1946年)
  - [高特](../Page/高特_\(加利福尼亞州\).md "wikilink") (Galt，建制於1946年)
  - [艾列頓](../Page/艾列頓_\(加利福尼亞州\).md "wikilink") (Isleton，建制於1923年)
  - [科多瓦牧場](../Page/科多瓦牧場_\(加利福尼亞州\).md "wikilink") (Rancho
    Cordova，建制於2003年)
  - [沙加緬度](../Page/沙加緬度_\(加利福尼亞州\).md "wikilink") (Sacramento，建制於1850年)

[Sacramento](../Category/加利福尼亞州行政區劃.md "wikilink")
[Category:萨克拉门托谷](../Category/萨克拉门托谷.md "wikilink")
[Category:加利福尼亚州中央谷地](../Category/加利福尼亚州中央谷地.md "wikilink")
[Category:萨克拉门托都市区](../Category/萨克拉门托都市区.md "wikilink")
[Category:加利福尼亚州萨克拉门托县](../Category/加利福尼亚州萨克拉门托县.md "wikilink")