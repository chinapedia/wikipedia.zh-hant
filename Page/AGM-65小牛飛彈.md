**AGM-65“小牛”**（），是[美国研制的一种為](../Page/美国.md "wikilink")[近距離空中支援而开发的空對地戰術](../Page/近距離空中支援.md "wikilink")[導彈](../Page/導彈.md "wikilink")（AGM），射程48公里。它能有效地打擊各種戰術目標，包括裝甲單位、防空設備，艦艇，地面運輸部隊及燃料儲存設施等。

AGM-65設計時納入模組化設計，可以讓不同尋標器、戰鬥部結合在相同的固體燃料火箭發動機彈體上；不同型的AGM-65具有不同的導引系統，包括[光電](../Page/光電.md "wikilink")、[激光](../Page/激光.md "wikilink")、[红外线](../Page/红外线.md "wikilink")、[感光耦合元件](../Page/感光耦合元件.md "wikilink")、[極高頻導引等](../Page/極高頻.md "wikilink")。AGM-65亦有兩類彈頭，一種重57公斤、於頭部有[接觸引信](../Page/接觸引信.md "wikilink")，另一種彈頭較重為136公斤、配備[延遲引信](../Page/延遲引信.md "wikilink")，後者增加的動能穿透目標后再引爆；後者對大型、堅固的目標最為有效。

## 使用者

[AGM-65_operators.png](https://zh.wikipedia.org/wiki/File:AGM-65_operators.png "fig:AGM-65_operators.png")
大部份美軍戰鬥機、攻擊機，甚至是巡邏機都有掛載小牛飛彈的能力，但導引飛彈用的雷射指示器等設備並非每種機型皆有內建，因此在配備此武器時也可以藉由外掛導引莢艙的做法增加武裝配備彈性，以下是一些例子：

  - [F-4E幽靈II](../Page/F-4幽靈II戰鬥機.md "wikilink")
  - [配備了雷射指示器的F-5E/F C構型機](../Page/F-5自由鬥士戰鬥機.md "wikilink")
  - [F-15E戰鬥機](../Page/F-15E打擊鷹式戰鬥轟炸機.md "wikilink")
  - [F-16戰隼](../Page/F-16戰隼戰鬥機.md "wikilink")
  - [F-18E/F黃蜂](../Page/F/A-18E/F超級大黃蜂式打擊戰鬥機.md "wikilink")
  - [JAS 39獅鷲](../Page/JAS_39獅鷲戰鬥機.md "wikilink")
  - [AH-1W攻擊直升機](../Page/AH-1W攻擊直升機.md "wikilink")
  - [AH-64E攻擊直升機](../Page/AH-64阿帕契直升機.md "wikilink")
  - [A-10C雷霆II](../Page/A-10雷霆二式攻擊機.md "wikilink")
  - [AV-8B鷂II](../Page/AV-8攻擊機.md "wikilink")
  - [P-3C獵戶座](../Page/P-3獵戶座海上巡邏機.md "wikilink")

## 實戰紀錄

  - 兩伊戰爭
    [伊朗持有的美軍戰機在](../Page/伊朗.md "wikilink")[伊朗革命後勤斷援後仍勉強運作](../Page/伊朗革命.md "wikilink")，[伊朗空軍的F](../Page/伊朗空軍.md "wikilink")-4也曾使用過小牛飛彈攻擊波斯灣油輪和[伊拉克地面部隊](../Page/伊拉克.md "wikilink")；由於伊朗和美國交惡致使美方中斷後勤支援，雖然在[伊朗門事件中伊朗方得到少量的美規原廠零件](../Page/伊朗門事件.md "wikilink")，但後來[兩伊戰爭停火後即無法繼續運作](../Page/兩伊戰爭.md "wikilink")，伊朗方面則決定複製各種美製武器維持戰力，現今伊朗方面服役中的小牛飛彈多為1990年代後的仿製版，而伊朗的空對地飛彈主要使用來自中俄兩國的製品。

<!-- end list -->

  - 波斯灣戰爭
    美軍在1991年的波灣戰爭陸地攻勢發動前，對伊拉克發動近2個月的空中掃蕩任務；據統計在該段時間美軍至少使用小牛飛彈執行5000次對地面裝甲目標攻擊任務\[1\]，運用型號為紅外線影像導引之AGM-65D，美國空軍統計認為小牛飛彈的命中率有80%以上，但美國海軍陸戰隊則認為只有60%。在2003年的伊拉克作戰行動中美軍共發射918枚小牛飛彈。

## 参见

  - [AGM-114地獄火飛彈](../Page/AGM-114地獄火飛彈.md "wikilink")
  - [德立拉导弹](../Page/德立拉导弹.md "wikilink")

[Category:美國飛彈](../Category/美國飛彈.md "wikilink")
[Category:空對地飛彈](../Category/空對地飛彈.md "wikilink")

1.