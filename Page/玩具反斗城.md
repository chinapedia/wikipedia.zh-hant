[ToysRus_New_York_Flagship_store_interior_2011.jpg](https://zh.wikipedia.org/wiki/File:ToysRus_New_York_Flagship_store_interior_2011.jpg "fig:ToysRus_New_York_Flagship_store_interior_2011.jpg")[紐約時代廣場的玩具反斗城旗艦店](../Page/紐約時代廣場.md "wikilink")（已於2011年結業）\]\]
[Toysrus_Us.jpg](https://zh.wikipedia.org/wiki/File:Toysrus_Us.jpg "fig:Toysrus_Us.jpg")的玩具反斗城區域型分店\]\]
[Ocean_Terminal_ToysRus_201409.jpg](https://zh.wikipedia.org/wiki/File:Ocean_Terminal_ToysRus_201409.jpg "fig:Ocean_Terminal_ToysRus_201409.jpg")[海運大廈的玩具反斗城旗艦店](../Page/海運大廈.md "wikilink")\]\]
[ToysRus_in_Chadstone_Shopping_Centre_2017.JPG](https://zh.wikipedia.org/wiki/File:ToysRus_in_Chadstone_Shopping_Centre_2017.JPG "fig:ToysRus_in_Chadstone_Shopping_Centre_2017.JPG")[墨爾本](../Page/墨爾本.md "wikilink")[查斯頓購物中心的玩具反斗城与宝宝反斗城](../Page/查斯頓購物中心.md "wikilink")\]\]
[ToysRUscHENGDU.jpg](https://zh.wikipedia.org/wiki/File:ToysRUscHENGDU.jpg "fig:ToysRUscHENGDU.jpg")的玩具反斗城与宝宝反斗城\]\]
[Sundan_and_Toysrus-Suzhou_Times_Square_(2013).jpg](https://zh.wikipedia.org/wiki/File:Sundan_and_Toysrus-Suzhou_Times_Square_\(2013\).jpg "fig:Sundan_and_Toysrus-Suzhou_Times_Square_(2013).jpg")[圆融时代广场的玩具反斗城](../Page/圆融时代广场.md "wikilink")\]\]
[Toys_R_Us_Ravensburg_2005.jpg](https://zh.wikipedia.org/wiki/File:Toys_R_Us_Ravensburg_2005.jpg "fig:Toys_R_Us_Ravensburg_2005.jpg")
[Toysrus_Japan_02.jpg](https://zh.wikipedia.org/wiki/File:Toysrus_Japan_02.jpg "fig:Toysrus_Japan_02.jpg")
[ToysRUsOntario4.jpg](https://zh.wikipedia.org/wiki/File:ToysRUsOntario4.jpg "fig:ToysRUsOntario4.jpg")

**玩具反斗城**（，在其標誌中的寫法是）是[美國一家跨國大型](../Page/美國.md "wikilink")[玩具](../Page/玩具.md "wikilink")[連鎖店](../Page/連鎖店.md "wikilink")，成立於1948年，總部位於[紐約都會區內的](../Page/紐約都會區.md "wikilink")[新澤西州](../Page/新澤西州.md "wikilink")。1967年由[查爾斯·拉扎勒斯](../Page/查爾斯·拉扎勒斯.md "wikilink")（Charles
Lazarus）將其現代化，玩具反斗城追溯其起源於1948年由他創立的Lazarus兒童傢具店。他為他的產品添加了玩具，並最終轉移了他的重點。該公司從事玩具業已超過65年，在美國有大約800家門店，而美國以外的地區大約有800家門店。該公司目前已聲請[破產](../Page/破產.md "wikilink")。

玩具反斗城集团还包括宝宝反斗城（BABIES R US）、KIDS R
US等[连锁商店](../Page/连锁店.md "wikilink")，其目前是一上市公司，总部设在美国[新泽西州的韋恩](../Page/新泽西州.md "wikilink")。

2017年9月18日，於[美國](../Page/美國.md "wikilink")[維吉尼亞州聲請](../Page/維吉尼亞州.md "wikilink")[破產保護](../Page/破產保護.md "wikilink")。\[1\]

2018年3月15日，玩具反斗城提交動議，尋求破產法院批准其開始[清算美國業務](../Page/清算.md "wikilink")。該公司還尋求重組和出售加拿大業務以及在亞洲和中歐地區的國際業務。玩具反斗城美國約700間分店在6月29日為營業的最後一天。直到10月15日，[Geoffrey
LLC繼承其資產後](../Page/Geoffrey_LLC.md "wikilink")，計劃重新營業的地點。\[2\]

## 全球據點

**Toys "R" Us, International**是玩具反斗城公司的一個分支，主要經營管理美國以外的據點。Toys "R" Us,
International最初於1986年下半年在[香港設置首家海外分店據點](../Page/香港.md "wikilink")，今日已在全球（美國之外）設置大約1400間\[3\]分店。

### 亞洲

玩具反斗城在1986年下半年登陸[香港](../Page/香港.md "wikilink")，由[利豐集團經營](../Page/利豐.md "wikilink")，總店位於尖沙咀[海運大廈](../Page/海運大廈.md "wikilink")。現時香港共有14家分店（11間主店加2間Toys"R"Us
Express/Toy
Box，1間Baby"R"Us）。而[台灣有](../Page/台灣.md "wikilink")21家分店，也是由利豐集團經營。至2008年2月1日，Toys
Li Fung (Asia) Ltd.在亚洲8个市场（台湾、中国、香港、新加坡、马来西亚、泰国、菲律宾及澳门）开设106间玩具"反"斗城分店。

2011年11月2日玩具反斗城與利豐集團旗下的利豐零售達成協議，將買回雙方合資公司部份股權，使玩具反斗城在合資公司控股權增加至70%，以提高對大中華區及東南亞等店舖的掌握。

2012年8月18日，玩具反斗城宣布将首次进驻北京。公司在中国首都东部揭幕两家门店，进军华北市场。

2017年5月3日馮氏零售集團與玩具反斗城亞洲簽訂一項協議，將合併[玩具反斗城日本與大中華區及東南亞的業務](../Page/玩具反斗城.md "wikilink")。在加入玩具反斗城日本後，馮氏零售集團將擁有亞洲合資企業玩具反斗城亞洲
（Toys"R"Us Asia Ltd）約15%股權。

### 現有據點

  - －22間分店

  - －13間分店

  - －1間分店

  - －1984年成立，65間分店

  - －2006年12月成立，141間分店

  - －1995年成立，13間分店

  - －4間分店

  - －2006年成立，4間分店

  - －1987年成立，38間分店

  - －1987年成立，78間分店

  - －1986年成立，13間分店，1間寶寶反斗城

  - －2007年10月成立，3間分店

  - －1995年成立，23間分店

  - －1989年成立，160間分店

  - －2009年7月5日成立，1間分店

  - －2間分店

  - －18間分店

  - －8間分店

  - －2006年成立，24間分店

  - －8間分店

  - －1間分店

  - －1996年成立，10間分店

  - －1984年成立，6間分店

  - －1987年成立，23間分店

  - －2007年成立，8間分店

  - －45間分店

  - －1994年成立，13間分店

  - －6間分店

  - －2005年成立，9間分店

  - －1995年成立，13間分店

### 其他據點

  - －1985年成立，100間分店，2018年4月24日全線分店關閉

  - －1948年成立，735間分店，2018年6月29日全線關閉，即將重新開業

  - －1993年成立，43間分店，2018年8月5日全線關閉

  - －1996年成立，2000年代全線分店關閉

  - －1990年代成立，不久關閉

  - －1993年成立，17間分店，至1997年已經受經營問題困擾，現全線分店已於2009年3月3日外判轉售，以「Toys XL」名義繼續經營

  - －全線分店更名為「Tokiyi」繼續經營

## 注釋

<references/>

## 外部連結

  - [玩具反-{斗}-城美國官網](http://www.toysrus.com/)

  - [玩具反-{斗}-城日本官網](http://www.toysrus.co.jp/)

  - [玩具反-{斗}-城台灣官網](http://www.toysrus.com.tw/)

  - [玩具反-{斗}-城法國官網](http://www.toysrus.fr/)

  - [玩具反-{斗}-城中國官網](http://www.toysrus.com.cn/)

  - [玩具反-{斗}-城香港官網](http://www.toysrus.com.hk/)

  -
  -
  -
  -
  -
  -
[Category:玩具](../Category/玩具.md "wikilink")
[Category:跨国公司](../Category/跨国公司.md "wikilink")
[Category:美国零售商](../Category/美国零售商.md "wikilink")
[W](../Category/台灣零售商.md "wikilink")
[Category:香港零售商](../Category/香港零售商.md "wikilink")
[Category:利豐](../Category/利豐.md "wikilink")
[Category:贝恩资本公司](../Category/贝恩资本公司.md "wikilink")
[Category:1948年成立的公司](../Category/1948年成立的公司.md "wikilink")

1.
2.
3.  [美國・玩具反斗城的公司簡介](http://www3.toysrus.com/about/)