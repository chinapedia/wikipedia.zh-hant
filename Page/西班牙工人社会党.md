**西班牙工人社会党**（），简称**工社党**（），是[西班牙的一个](../Page/西班牙.md "wikilink")[中間偏左的](../Page/中間偏左.md "wikilink")[社會民主主義政黨](../Page/社會民主主義.md "wikilink")。工人社會黨是西班牙現時歷史最悠久的政黨。

## 歷史

西班牙工人社會黨早年是[共和主義及](../Page/共和主義.md "wikilink")[社會主義的政黨](../Page/社會主義.md "wikilink")，反對[西班牙王室](../Page/西班牙.md "wikilink")，1931年[西班牙波旁王朝第二次結束](../Page/西班牙波旁王朝.md "wikilink")，同年成立[西班牙第二共和國](../Page/西班牙第二共和國.md "wikilink")，同年工人社會黨首次獲勝，組建首個左派聯合政府。共和左派政府實行的國有化政策引發右派保王派及軍方保守派勢力反彈，1936年再次獲勝後引發[西班牙內戰](../Page/西班牙內戰.md "wikilink")，內戰結束後[佛朗哥實行法西斯獨裁統治](../Page/佛朗哥.md "wikilink")，工人社會黨遭到封禁。

西班牙在1977年恢復民主政治後，工人社會党實行[改良主義路線](../Page/改良主義.md "wikilink")，最終在1982年成功在選舉中獲勝，組建46年首個左派政府，同年領導西班牙加入[北約](../Page/北約.md "wikilink")。在1982－1996年间，工人社會黨四次蝉联执政。1996年大選失利後，1996－2004年成为在野党。

2004年4月，工人社会党在大选中获胜，重新上台。

2008年，工人社会党在大选中获胜，再次上台执政。

2011年11月20日，工人社会党在大选慘败，成为在野党。

2015年12月20日，工人社会党在大选再次慘败，雖然維持第二大黨的地位，但該黨的得票率及議席是西班牙恢復民主政治以來最低。2016年6月26日工人社会党在大选再次慘败，席次數比上一次低。

2018年6月1日，由該黨領導[佩德羅·桑切斯所發動的不信任案在眾議院中獲得通過](../Page/佩德羅·桑切斯.md "wikilink")，該黨再次成為執政黨。

## 歷屆選舉結果

### 初始議會

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>投票結果</p></th>
<th><p>獲得席次</p></th>
<th><p>狀態</p></th>
<th><p>席次排名</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>#</p></td>
<td><p>±</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Arrow_Blue_Right_001.svg" title="fig:Arrow_Blue_Right_001.svg">Arrow_Blue_Right_001.svg</a>0</p></td>
<td><p>N/A</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>*</p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Green_Arrow_Up_Darker.svg" title="fig:Green_Arrow_Up_Darker.svg">Green_Arrow_Up_Darker.svg</a>1</p></td>
<td><p>反對黨</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>*</p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Arrow_Blue_Right_001.svg" title="fig:Arrow_Blue_Right_001.svg">Arrow_Blue_Right_001.svg</a>0</p></td>
<td><p>反對黨</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>*</p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Arrow_Blue_Right_001.svg" title="fig:Arrow_Blue_Right_001.svg">Arrow_Blue_Right_001.svg</a>0</p></td>
<td><p>反對黨</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>**</p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Green_Arrow_Up_Darker.svg" title="fig:Green_Arrow_Up_Darker.svg">Green_Arrow_Up_Darker.svg</a>5</p></td>
<td><p>反對黨</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>*</p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Arrow_Blue_Right_001.svg" title="fig:Arrow_Blue_Right_001.svg">Arrow_Blue_Right_001.svg</a>0</p></td>
<td><p>反對黨</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Red_Arrow_Down.svg" title="fig:Red_Arrow_Down.svg">Red_Arrow_Down.svg</a>2</p></td>
<td><p>反對黨</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Green_Arrow_Up_Darker.svg" title="fig:Green_Arrow_Up_Darker.svg">Green_Arrow_Up_Darker.svg</a>3</p></td>
<td><p>反對黨</p></td>
</tr>
</tbody>
</table>

  - \* 代表與結盟。
  - \*\* 代表與結盟。

### 第二共和國議會

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>投票結果</p></th>
<th><p>獲得席次</p></th>
<th><p>狀態</p></th>
<th><p>席次排名</p></th>
<th><p>附註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>#</p></td>
<td><p>%</p></td>
<td><p>±<a href="../Page/百分點.md" title="wikilink">pp</a></p></td>
<td><p>#</p></td>
<td><p>±</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>21.4%</p></td>
<td><p>—</p></td>
<td></td>
<td><p>—</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>19.4%</p></td>
<td><p>–2.0</p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Red_Arrow_Down.svg" title="fig:Red_Arrow_Down.svg">Red_Arrow_Down.svg</a>57</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1936年西班牙大選.md" title="wikilink">1936</a></p></td>
<td></td>
<td><p>16.4%*</p></td>
<td><p>–3.0</p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Green_Arrow_Up_Darker.svg" title="fig:Green_Arrow_Up_Darker.svg">Green_Arrow_Up_Darker.svg</a>40</p></td>
</tr>
</tbody>
</table>

### 議會

<table>
<tbody>
<tr class="odd">
<td><table>
<caption><a href="../Page/眾議院_(西班牙).md" title="wikilink">眾議院</a></caption>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>投票結果</p></th>
<th><p>獲得席次</p></th>
<th><p>狀態</p></th>
<th><p>席次排名</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>#</p></td>
<td><p>%</p></td>
<td><p>±<a href="../Page/百分點.md" title="wikilink">pp</a></p></td>
<td><p>#</p></td>
<td><p>±</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>5,371,866</p></td>
<td><p>29.3%</p></td>
<td><p>—</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>5,469,813</p></td>
<td><p>30.4%</p></td>
<td><p>1.1</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>10,127,392</p></td>
<td><p>48.1%</p></td>
<td><p>17.7</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>8,901,718</p></td>
<td><p>44.1%</p></td>
<td><p>4.0</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>8,115,568</p></td>
<td><p>39.6%</p></td>
<td><p>4.5</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>9,150,083</p></td>
<td><p>38.8%</p></td>
<td><p>0.8</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>9,425,678</p></td>
<td><p>37.6%</p></td>
<td><p>1.2</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>7,918,752</p></td>
<td><p>34.2%</p></td>
<td><p>3.4</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>11,026,163</p></td>
<td><p>42.6%</p></td>
<td><p>8.4</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>11,289,335</p></td>
<td><p>43.9%</p></td>
<td><p>1.3</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2011年西班牙大選#眾議院.md" title="wikilink">2011</a></p></td>
<td><p>7,003,511</p></td>
<td><p>28.8%</p></td>
<td><p>15.1</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2015年西班牙大選.md" title="wikilink">2015</a></p></td>
<td><p>5,545,315</p></td>
<td><p>22.0%</p></td>
<td><p>6.8</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2016年西班牙大選#眾議院.md" title="wikilink">2016</a></p></td>
<td><p>5,443,846</p></td>
<td><p>22.6%</p></td>
<td><p>0.6</p></td>
<td></td>
</tr>
</tbody>
</table></td>
<td><p> </p></td>
<td><table>
<caption><a href="../Page/參議院_(西班牙).md" title="wikilink">參議院</a></caption>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>獲得席次</p></th>
<th><p>席次排名</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>#</p></td>
<td><p>±</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>—</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Green_Arrow_Up_Darker.svg" title="fig:Green_Arrow_Up_Darker.svg">Green_Arrow_Up_Darker.svg</a>15</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Green_Arrow_Up_Darker.svg" title="fig:Green_Arrow_Up_Darker.svg">Green_Arrow_Up_Darker.svg</a>65</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Red_Arrow_Down.svg" title="fig:Red_Arrow_Down.svg">Red_Arrow_Down.svg</a>10</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Red_Arrow_Down.svg" title="fig:Red_Arrow_Down.svg">Red_Arrow_Down.svg</a>17</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Red_Arrow_Down.svg" title="fig:Red_Arrow_Down.svg">Red_Arrow_Down.svg</a>11</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Red_Arrow_Down.svg" title="fig:Red_Arrow_Down.svg">Red_Arrow_Down.svg</a>15</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Red_Arrow_Down.svg" title="fig:Red_Arrow_Down.svg">Red_Arrow_Down.svg</a>21</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Green_Arrow_Up_Darker.svg" title="fig:Green_Arrow_Up_Darker.svg">Green_Arrow_Up_Darker.svg</a>29</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Green_Arrow_Up_Darker.svg" title="fig:Green_Arrow_Up_Darker.svg">Green_Arrow_Up_Darker.svg</a>7</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2011年西班牙大選#參議院.md" title="wikilink">2011</a></p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Red_Arrow_Down.svg" title="fig:Red_Arrow_Down.svg">Red_Arrow_Down.svg</a>42</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2015年西班牙大選.md" title="wikilink">2015</a></p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Red_Arrow_Down.svg" title="fig:Red_Arrow_Down.svg">Red_Arrow_Down.svg</a>7</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2016年西班牙大選#參議院.md" title="wikilink">2016</a></p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Red_Arrow_Down.svg" title="fig:Red_Arrow_Down.svg">Red_Arrow_Down.svg</a>4</p></td>
</tr>
</tbody>
</table></td>
</tr>
</tbody>
</table>

### 歐洲議會

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>投票結果</p></th>
<th><p>獲得席次</p></th>
<th><p>席次排名</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>#</p></td>
<td><p>%</p></td>
<td><p>±<a href="../Page/百分點.md" title="wikilink">pp</a></p></td>
<td><p>#</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>7,522,706</p></td>
<td><p>39.1%</p></td>
<td><p>—</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>6,275,552</p></td>
<td><p>39.6%</p></td>
<td><p>+0.5</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>5,719,707</p></td>
<td><p>30.8%</p></td>
<td><p>–8.8</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>7,477,823</p></td>
<td><p>35.3%</p></td>
<td><p>+4.5</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>6,741,112</p></td>
<td><p>43.5%</p></td>
<td><p>+8.2</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>6,141,784</p></td>
<td><p>38.8%</p></td>
<td><p>–4.7</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>3,614,232</p></td>
<td><p>23.0%</p></td>
<td><p>–15.8</p></td>
</tr>
</tbody>
</table>

## 著名成员

  - [帕布洛·伊格莱西亚斯·波塞](../Page/帕布洛·伊格莱西亚斯.md "wikilink")
  - [弗朗西斯科·拉尔戈·卡巴列罗](../Page/弗朗西斯科·拉尔戈·卡巴列罗.md "wikilink")
  - [罗多尔弗·略皮斯](../Page/罗多尔弗·略皮斯.md "wikilink")
  - [英达莱西奥·普莱托](../Page/英达莱西奥·普莱托.md "wikilink")
  - [朱利安·贝斯特伊罗](../Page/朱利安·贝斯特伊罗.md "wikilink")
  - [托马斯·梅亚贝](../Page/托马斯·梅亚贝.md "wikilink")
  - [卡耶塔诺·雷东多·阿塞尼亚](../Page/卡耶塔诺·雷东多·阿塞尼亚.md "wikilink")
  - [拉蒙·鲁维亚尔](../Page/拉蒙·鲁维亚尔.md "wikilink")
  - [费利佩·冈萨雷斯](../Page/费利佩·冈萨雷斯.md "wikilink")
  - [阿方索·格拉](../Page/阿方索·格拉.md "wikilink")
  - [何塞·路易斯·罗德里格斯·萨帕特罗](../Page/何塞·路易斯·罗德里格斯·萨帕特罗.md "wikilink")
  - [阿尔弗雷多·佩雷斯·鲁瓦尔卡瓦](../Page/阿尔弗雷多·佩雷斯·鲁瓦尔卡瓦.md "wikilink")
  - [佩德罗·桑切斯](../Page/佩德罗·桑切斯.md "wikilink")

## 另見

  - [西班牙政治](../Page/西班牙政治.md "wikilink")
  - [西班牙政黨列表](../Page/西班牙政黨列表.md "wikilink")

## 參考資料

## 參考書目

  -
  -
  -
  -
  -
  -
## 延伸閱讀

  - Graham, Helen. "The Spanish Socialist Party in Power and the
    Government of Juan Negrín, 1937-9," *European History Quarterly*
    (1988) 18\#2 pp 175–206.
    [online](http://ehq.sagepub.com/content/18/2/175.citation)

[\*](../Category/西班牙工人社会党.md "wikilink")
[Category:西班牙政党](../Category/西班牙政党.md "wikilink")
[Category:歐洲社會黨成員](../Category/歐洲社會黨成員.md "wikilink")
[Category:社會民主主義政黨](../Category/社會民主主義政黨.md "wikilink")