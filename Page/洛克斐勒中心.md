[NBC_News_Rockefeller_Center.JPG](https://zh.wikipedia.org/wiki/File:NBC_News_Rockefeller_Center.JPG "fig:NBC_News_Rockefeller_Center.JPG")》的攝影現場。\]\]

**洛克菲勒中心**（**Rockefeller
Center**）是座落于[美國](../Page/美國.md "wikilink")[紐約州](../Page/紐約州.md "wikilink")[紐約市](../Page/紐約市.md "wikilink")[第五大道的一個由數個](../Page/第五大道.md "wikilink")[摩天大樓組成的城中城](../Page/摩天大樓.md "wikilink")，由[洛克斐勒家族出資建造](../Page/洛克斐勒家族.md "wikilink")，設計者為Raymond
M.
Hood。由十九棟大樓組成，各大樓底層相通。1987年被美國政府認定為[國家歷史地標](../Page/國家歷史地標.md "wikilink")。

## 建築

洛克斐勒中心是歷史上最浩大的[都市計劃之一](../Page/都市計劃.md "wikilink")，這塊區域佔地二十二英畝，由十九棟建築圍塑出來的活動區域，對於公共空間的運用開啟了城市規劃的新風貌，完整的商場與辦公大樓讓中城區繼[華爾街之後](../Page/華爾街.md "wikilink")，成為紐約第二個市中心。第五大道旁較為低矮的國際大樓緩緩起伏到第六大道旁最高的GE大樓（通用电气-69層高樓），交錯橫貫之間的是供市民使用的廣場〈海峽花園“Channel
Garden”、下層廣場“Lower Plaza”等〉，這座城中城每天容納上班、觀光、消費的流量達25萬人。

嚴格來說，洛克斐勒中心區域涵蓋第五大道至第七大道，介於47街至52街之間，區內涵括餐廳、辦公大樓、服飾店、銀行、郵局、書店…，甚至還有地下鐵通道貫穿連結，建築師聰明地利用大樓間的廣場、空地與樓梯間製造人行流動的方向，讓一天超過25萬的人潮在此穿梭無虞。

洛克斐勒中心在建築史上最大的衝擊是提供公共領域的使用，這種為普羅大眾設計的空間概念引發後來對於「市民空間」（Civic
Space）的重視，巧妙地利用大樓的大廳、廣場、樓梯間、路衝設計成行人的休息區、消費區，徹底落實為廣大中產階級服務的计划，建築物不再是取悅上帝及皇帝的工具。

## 歷史

1928年，世界首富[约翰·戴维森·洛克菲勒之子](../Page/约翰·戴维森·洛克菲勒.md "wikilink")[小约翰·戴维森·洛克菲勒](../Page/小约翰·戴维森·洛克菲勒.md "wikilink")，與[大都會歌劇院合作開始了此建築計畫](../Page/大都會歌劇院.md "wikilink")。

然而，第二年的1929年的[大蕭條](../Page/大蕭條.md "wikilink")，對[美國經濟造成了毀滅性的災难](../Page/美國經濟.md "wikilink")，也使大都會歌劇院撤出了投資。這時，小洛克菲勒面臨兩個選擇，放棄或繼續。他說：「我明白現在的我只有兩條路，一是放棄整個計畫，二是完全由我一個人的資金繼續完成他。」他選擇了繼續，獨自承擔起這個巨大的計畫，限期24年。（可選擇延期另外三期各21年，總共87年）他當年堅持繼續計畫的原因，很大部分是為了創造建築的就業機會並提振美國人民的信心。

這可能是人類歷史上最浩大的私人建築計畫。在[紐約](../Page/紐約.md "wikilink")[曼哈頓](../Page/曼哈頓.md "wikilink")，[裝飾風藝術風格的十四棟摩天大樓](../Page/裝飾風藝術.md "wikilink")，佔地22英畝，於1930年5月17日正式動工，於九年後的1939年11月1日完成。建築名稱是家族幕僚[Ivy
Lee所建議的](../Page/Ivy_Lee.md "wikilink")。小洛克菲勒原先並不想以自己的姓氏命名，但考量到這名字能吸引到更多商家與人潮，接受了提案。

整個中心分為兩部分，第一部分為1930年代建造的十四棟大樓，為古典風格。第二部分是1960年代和1970年代新建的四棟大樓，為現代風格。

1989年10月，[日本泡沫經濟達到頂點的這一年](../Page/日本泡沫經濟.md "wikilink")，[三菱地所以](../Page/三菱地所.md "wikilink")8.46億[美圓](../Page/美圓.md "wikilink")（以當時的匯率，約合1100多億[日圓](../Page/日圓.md "wikilink")）的價格購買了洛克菲勒中心的擁有者[洛克菲勒集團](../Page/洛克菲勒集團.md "wikilink")（[Rockefeller
Group](../Page/Rockefeller_Group.md "wikilink")）51%的股權\[1\]，從而取得了洛克菲勒中心的控制權，成為日本當年海外投資的經典案例。此後，三菱地所又分批購買了洛克菲勒集團剩下的所有股權\[2\]。

1996年7月因爲無法償還貸款，洛克菲勒集團將最初建造的14棟大樓轉讓\[3\]。2000年，房地產業者[Jerry
Speyer](../Page/Jerry_Speyer.md "wikilink")（洛克菲勒第三代領導人[David
Rockefeller的好友](../Page/David_Rockefeller.md "wikilink")）以18.5億美元買下了這14棟大樓。

而較新的4棟大樓則仍然為[三菱地所的全資子公司](../Page/三菱地所.md "wikilink")[洛克菲勒集團所擁有](../Page/洛克菲勒集團.md "wikilink")。

## 相关条目

  - [罗斯洛克国际金融中心](../Page/罗斯洛克国际金融中心.md "wikilink")

## 参考文献

## 外部連接

  -
  - [Guide to Rockefeller
    Center](http://manhattan.about.com/od/historyandlandmarks/a/rockefellercent.htm)

  - [The Rockefeller Group](http://www.rockefellergroup.com/)

  - [Rockefeller Center
    Webcam](http://www.wnbc.com/wxcam/1210190/detail.html)

  - [in-Arch.net: Rockefeller Center
    introduction](http://www.in-arch.net/NYC/nycintro.html#rockefeller)

[Category:曼哈頓摩天大樓](../Category/曼哈頓摩天大樓.md "wikilink")
[Category:1939年完工建築物](../Category/1939年完工建築物.md "wikilink")
[Category:冠以人名的建築物](../Category/冠以人名的建築物.md "wikilink")
[Category:曼哈顿旅游景点](../Category/曼哈顿旅游景点.md "wikilink")

1.  [1](http://www.nytimes.com/1989/10/31/business/japanese-buy-new-york-cachet-with-deal-for-rockefeller-center.html)，紐約時報：Japanese
    Buy New York Cachet With Deal for Rockefeller Center

2.  [洛克菲勒集團歷史](http://www.rockefellergroup.com/about-us/history/)

3.