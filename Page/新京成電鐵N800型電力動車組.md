**新京成N800型電力動車組**（）是[新京成電鐵的](../Page/新京成電鐵.md "wikilink")[通勤型電力動車組](../Page/通勤型電力動車組.md "wikilink")，於2005年投入服務。

首組列車於2005年4月出廠。最初編組的試乘會在5月28日舉行，在29日開始服役。

## 概要

[日本車輛製造製造的電聯車](../Page/日本車輛製造.md "wikilink")。[新京成電鐵預計在](../Page/新京成電鐵.md "wikilink")2007年直入[京成千葉線](../Page/京成千葉線.md "wikilink")。此外[800型以服務超過](../Page/新京成電鐵800型電力動車組.md "wikilink")30年，所以有關方面就決定製造新車輛取代之。

為減低製造成本，所以N800型便以[京成3000型作藍本](../Page/京成3000型電力動車組_\(二代\).md "wikilink")，再配以新京成的塗色。

### 和京成3000型的不同

  - 車身顏色
  - 貫通門
  - 車内扶手環(3000型為圓型，N800型是三角型）
  - 關門鈴聲

## 諸元

  - 製造年：2005年
  - 車輛數：6-{zh-tw:節;zh-hk:卡;zh-cn:节}-編成
  - 編組：4M2T
  - 保安裝置：[新京成電鐵](../Page/新京成電鐵.md "wikilink")、[京成電鐵](../Page/京成電鐵.md "wikilink")、[北總鐵道](../Page/北總鐵道.md "wikilink")、[京濱急行電鐵](../Page/京濱急行電鐵.md "wikilink")、[芝山鐵道](../Page/芝山鐵道.md "wikilink")、[都營地下鐵淺草線使用的](../Page/都營地下鐵淺草線.md "wikilink")[1號型ATS及](../Page/1號型ATS.md "wikilink")[C-ATS](../Page/C-ATS.md "wikilink")。

## 連結

  - [新京成電鐵N800型的官方網站](http://www.shinkeisei.co.jp/welove/train_museum/n800)

[Category:日本電力動車組](../Category/日本電力動車組.md "wikilink")
[Category:新京成電鐵车辆](../Category/新京成電鐵车辆.md "wikilink")
[Category:日本車輛製鐵路車輛](../Category/日本車輛製鐵路車輛.md "wikilink")
[Category:1500伏直流电力动车组](../Category/1500伏直流电力动车组.md "wikilink")