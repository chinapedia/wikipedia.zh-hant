**金敏喜**（，），[韓國](../Page/韓國.md "wikilink")[女演員](../Page/女演員.md "wikilink")，2017年[柏林影展最佳女演员奖得主](../Page/柏林影展.md "wikilink")。\[1\]

## 感情生活

2003年與[李政宰拍攝廣告認識](../Page/李政宰.md "wikilink")，同年10月開始熱戀，但戀愛關係維持了3年之後分手。

2008年7月與小她6歲的模特兒[李洙赫交往](../Page/李洙赫.md "wikilink")，兩年後分手。

2013年初與[趙寅成交往](../Page/趙寅成.md "wikilink")\[2\]，2014年9月，透過經紀公司發表已與[趙寅成和平分手的聲明](../Page/趙寅成.md "wikilink")。\[3\]

2016年6月21日，爆出疑似介入已婚導演[洪尚秀家庭的傳聞](../Page/洪尚秀.md "wikilink")，而媒體大都為洪尚秀及其妻單方面出面回應。2017年3月13日，她公開承認與洪尚秀是相愛關係。2018年初有報導指二人已分手。

## 演出作品

### 電視劇

  - 1999年：[KBS](../Page/韓國放送公社.md "wikilink")《[學校2](../Page/學校2.md "wikilink")》
  - 2000年：[SBS](../Page/SBS_\(韓國\).md "wikilink")《[茱麗葉的男朋友](../Page/茱麗葉的男朋友.md "wikilink")》
  - 2002年：SBS《[純真年代](../Page/純真年代_\(韓國電視劇\).md "wikilink")》飾演 洪芝微
  - 2004年：SBS《[嫂嫂19歲](../Page/嫂嫂19歲.md "wikilink")》飾演 崔秀芝
  - 2006年：KBS《[再見獨奏](../Page/再見獨奏.md "wikilink")》飾演 崔薇莉
  - 2008年：KBS《[戀愛結婚](../Page/戀愛結婚_\(電視劇\).md "wikilink")》飾演 李康萱

### 電影

  - 2000年：《[純愛譜](../Page/純愛譜.md "wikilink")》
  - 2002年：《[Surprise Party](../Page/Surprise_Party.md "wikilink")》
  - 2007年：《[熱情似火](../Page/熱情似火_\(韓影\).md "wikilink")》
  - 2009年：《[女演員們](../Page/女演員們.md "wikilink")》
  - 2011年：《[白鯨](../Page/白鯨_\(韓影\).md "wikilink")》
  - 2012年：《[火車](../Page/火車_\(電影\).md "wikilink")》
  - 2013年：《[說閒話：導演瘋了](../Page/說閒話：導演瘋了.md "wikilink")》
  - 2013年：《[戀愛的溫度](../Page/戀愛的溫度.md "wikilink")》
  - 2014年：《[哭泣的男子](../Page/哭泣的男子.md "wikilink")》
  - 2015年：《[這時對，那時錯](../Page/這時對，那時錯.md "wikilink")》
  - 2016年：《[下女的誘惑](../Page/下女的誘惑.md "wikilink")》飾演 秀子
  - 2017年：《[獨自在夜晚的海邊](../Page/獨自在夜晚的海邊.md "wikilink")》
  - 2017年：《[克莱尔的相机](../Page/克莱尔的相机.md "wikilink")》
  - 2017年：《[之後](../Page/之後.md "wikilink")》
  - 2018年：《[草葉集](../Page/Grass_\(2018_film\).md "wikilink")》
  - 2018年：《[河畔酒店](../Page/河畔酒店.md "wikilink")》

## 獲獎

  - 1999年：[KBS演技大賞](../Page/KBS演技大賞.md "wikilink")－青少年演技獎
  - 2000年：[SBS演技大賞](../Page/SBS演技大賞.md "wikilink")－女新人獎
  - 2008年：第44屆[百想藝術大賞](../Page/百想藝術大賞.md "wikilink")－電影部門 女子最優秀演技獎（熱情似火）
  - 2008年：第9屆釜山電影評論家協會獎－女主角獎（熱情似火）
  - 2012年：第21屆釜日電影獎－女主角獎（火車）
  - 2013年：[第49屆百想藝術大賞](../Page/第49屆百想藝術大賞.md "wikilink")－電影部門
    女子最優秀演技獎（戀愛的溫度）
  - 2013年：第34屆[青龍電影獎](../Page/青龍電影獎.md "wikilink")－人氣明星獎
  - 2016年：Director's Cut Awards
    －女演員獎（[下女的誘惑](../Page/下女的誘惑.md "wikilink"))
  - 2016年：[第37屆青龍電影獎](../Page/第37屆青龍電影獎.md "wikilink")－最佳女主角（下女的誘惑）
  - 2017年:
    [第67屆柏林影展](../Page/第67屆柏林影展.md "wikilink")－[最佳女演員銀熊獎](../Page/最佳女演員銀熊獎.md "wikilink")（獨自在海邊的夜晚）

## 腳註

## 外部連結

  - [NAVER搜索](http://search.naver.com/search.naver?where=nexearch&query=%EA%B9%80%EB%AF%BC%ED%9D%AC&sm=top_hty&fbm=1&ie=utf8)

  - [微博金敏喜吧](http://weibo.com/u/5995491832)

[K](../Page/category:韓國電影演員.md "wikilink")
[K](../Page/category:韓國電視演員.md "wikilink")
[K](../Page/category:檀國大學校友.md "wikilink")
[K](../Page/category:首爾特別市出身人物.md "wikilink")

[M](../Category/金姓.md "wikilink")
[Category:韩国女演员](../Category/韩国女演员.md "wikilink")

1.
2.  [趙寅成-金敏喜證實交往ing 1個月前密切約會](http://tw.enewsworld.interest.me/enews/contents.asp?idx=34669)enews
    World
3.  [演員趙寅成與金敏喜分手](https://tw.news.yahoo.com/%E6%BC%94%E5%93%A1%E8%B6%99%E5%AF%85%E6%88%90%E8%88%87%E9%87%91%E6%95%8F%E5%96%9C%E5%88%86%E6%89%8B-074833250.html)
    韓國中央日報