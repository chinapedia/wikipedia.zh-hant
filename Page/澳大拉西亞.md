[LocationAustralasia.png](https://zh.wikipedia.org/wiki/File:LocationAustralasia.png "fig:LocationAustralasia.png")
**澳大拉西亚**（）一般指[大洋洲的一个地区](../Page/大洋洲.md "wikilink")，如[澳大利亚](../Page/澳大利亚.md "wikilink")、[新西兰和邻近的](../Page/新西兰.md "wikilink")[太平洋](../Page/太平洋.md "wikilink")[岛屿](../Page/岛屿.md "wikilink")。

**Australasia**一詞是由[法国學者](../Page/法国.md "wikilink")[布羅塞](../Page/布羅塞.md "wikilink")（）於1756年出版的《Histoire
des navigations aux terres
australes》一書中提出的\[1\]，取自[拉丁文](../Page/拉丁文.md "wikilink")，意思是「[亚洲南部](../Page/亚洲.md "wikilink")」，並將其区別自[玻里尼西亞](../Page/玻里尼西亞.md "wikilink")（至东面）和东南太平洋（*Magellanica*）地区。也不包括[密克罗尼西亚](../Page/密克罗尼西亚.md "wikilink")（至东北面）。

## 自然地理

地形來說，澳大拉西亚包括[澳大利亚大陆](../Page/澳大利亞洲.md "wikilink")（包括[塔斯马尼亚岛](../Page/塔斯马尼亚州.md "wikilink")），[新西兰和](../Page/新西兰.md "wikilink")[美拉尼西亞](../Page/美拉尼西亞.md "wikilink")：[新畿內亞和太平洋澳洲东北部的邻近岛屿](../Page/新畿內亞.md "wikilink")。有時会指在[赤道和](../Page/赤道.md "wikilink")[纬度](../Page/纬度.md "wikilink")47°以南之间的所有太平洋的[陆地和](../Page/陆地.md "wikilink")[岛屿](../Page/岛屿.md "wikilink")。

澳大拉西亚的大部分领土位于[印度尼西亚地处](../Page/印度尼西亚.md "wikilink")[澳洲板块的南面](../Page/澳大利亞洲.md "wikilink")，並侧至[印度洋的西面及](../Page/印度洋.md "wikilink")[南冰洋的南面](../Page/南冰洋.md "wikilink")。周边领土至[欧亚大陆板块的西北](../Page/欧亚大陆.md "wikilink")、[菲律宾板块的北面](../Page/菲律宾板块.md "wikilink")、和在太平洋的无数[边缘海](../Page/边缘海.md "wikilink")，上至[太平洋板块的北面和东面](../Page/太平洋板块.md "wikilink")。

## 人文地理

[人类学家虽然不太同意其细节](../Page/人类学家.md "wikilink")，但也支持澳大拉西亚和鄰近亚区的原居民來自[东南亚的說法](../Page/东南亚.md "wikilink")。

### 政治地理

#### 普遍认为是澳大拉西亚地区的国家

  -
  -
[Flag_of_Australasian_team_for_Olympic_games.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Australasian_team_for_Olympic_games.svg "fig:Flag_of_Australasian_team_for_Olympic_games.svg")
[澳大利亚和](../Page/澳大利亚.md "wikilink")[新西兰在](../Page/新西兰.md "wikilink")20世纪之前都曾是[大英帝国的殖民地](../Page/大英帝国.md "wikilink")，之後成為[自治領](../Page/自治領.md "wikilink")，[1931年西敏法令並逐步脫離英國獨立后](../Page/1931年西敏法令.md "wikilink")，两国均成為为高度[发达国家](../Page/发达国家.md "wikilink")，并且拥有极高的[人类发展指数](../Page/人类发展指数.md "wikilink")。两国在人文以及经济层面在20世纪后期亦基本实行了一体化，並推行[多元文化主義](../Page/多元文化主義.md "wikilink")。“紐澳”（ANZ，或譯“澳新”）这个词汇亦被广泛的应用到该地区的经济，政治，人文以及体育等领域当中。

[地缘政治学來說](../Page/地缘政治学.md "wikilink")，澳大拉西亚有時用作形容澳大利亚和新西兰的词语。有很多澳大利亚和新西兰机构的名称都附有「（Royal）Australasian
Society」（皇家澳大拉西亞協會）。而在以前，「澳大拉西亞」一詞也曾用來作為澳大利亚和新西兰运动团体參加比賽時的名称。包括1905年至1913年期間的[网球比賽](../Page/网球.md "wikilink")，當時澳大利亚和新西兰合併其最佳運動員去參加[戴维斯杯](../Page/戴维斯杯.md "wikilink")（並於1907年、1908年、1909年、1911年獲勝）。還有1908年和1912年，兩国也曾使用澳大拉西亚參加[奥运会](../Page/奥运会.md "wikilink")。

#### 有时也被认为是澳大拉西亚地区的国家

  -
1914-1975年之間巴布亞新幾内亞由[大英帝國及](../Page/大英帝國.md "wikilink")[澳大利亞統治](../Page/澳大利亞.md "wikilink")，1975年獨立。

## 生態地理

從[生態學的角度來看](../Page/生態學.md "wikilink")，[澳新界](../Page/澳新界.md "wikilink")（Australasia
ecozone）是一個跟鄰近地區有顯著差別的地區：這區內有很多獨特的[植物和](../Page/植物.md "wikilink")[動物](../Page/動物.md "wikilink")，而且都有共同的演化史。依照這個範疇來說，澳大拉西亞只包括澳大利亚、新畿內亞及鄰近島嶼，還有從印尼[龙目岛至](../Page/龙目岛.md "wikilink")[蘇拉威西島向東的](../Page/蘇拉威西島.md "wikilink")[印度尼西亞島嶼](../Page/印度尼西亞.md "wikilink")。而用來分出[亚洲的分界線稱](../Page/亚洲.md "wikilink")[華理士分界線](../Page/華理士分界線.md "wikilink")：[婆羅洲和](../Page/婆羅洲.md "wikilink")[峇里島位於線的西面](../Page/峇里島.md "wikilink")，屬於亞洲的一侧。

## 参考文献

### 引用

### 来源

  - 书籍

<!-- end list -->

  - [Australasia](https://web.archive.org/web/20070221033758/http://www.1911encyclopedia.org/Australasia)
    [大英百科全書第十一版](../Page/大英百科全書第十一版.md "wikilink")

## 參見

  - [大洋洲](../Page/大洋洲.md "wikilink")
  - [澳大利亞洲](../Page/澳大利亞洲.md "wikilink")
  - [澳大利亚](../Page/澳大利亚.md "wikilink")
  - [新西兰](../Page/新西兰.md "wikilink")

{{-}}

[Category:大洋洲](../Category/大洋洲.md "wikilink")
[Category:分裂地區](../Category/分裂地區.md "wikilink")

1.