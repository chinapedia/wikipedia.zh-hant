**顾拜旦男爵皮埃尔·德·弗雷迪**（，），[法蘭西](../Page/法蘭西.md "wikilink")[巴黎人](../Page/巴黎.md "wikilink")，现代[奥林匹克运动会的发起人](../Page/奥林匹克运动会.md "wikilink")，1896年至1925年任[国际奥林匹克委员会主席](../Page/国际奥林匹克委员会.md "wikilink")，[奥林匹克会徽](../Page/奥林匹克五环.md "wikilink")、[奥林匹克会旗设计者](../Page/奥林匹克会旗.md "wikilink")。他终生倡导奥林匹克精神，被誉为“现代奥林匹克之父”。顾拜旦不仅是国际[体育活动家](../Page/体育.md "wikilink")，同时也是卓有成就的[教育家和](../Page/教育家.md "wikilink")[历史学家](../Page/历史学家.md "wikilink")。

## 生平

顾拜旦出身于法国[贵族家庭](../Page/贵族.md "wikilink")，在家中排行第四，父亲夏尔·德·弗雷迪（）是画家。\[1\]\[2\]拥有顾拜旦[男爵](../Page/男爵.md "wikilink")[頭銜](../Page/頭銜.md "wikilink")，从小喜欢贵族运动，如[击剑](../Page/击剑.md "wikilink")、[赛艇](../Page/赛艇.md "wikilink")、[骑马](../Page/骑马.md "wikilink")，也喜欢[拳击](../Page/拳击.md "wikilink")。

从1875年开始，一直到1881年，[考古学家在](../Page/考古学家.md "wikilink")[希腊连续发掘出古代奥运会的文物遗址](../Page/希腊.md "wikilink")，这引起了顾拜旦的兴趣和关注。在[1890年](../Page/1890年.md "wikilink")，他终于有机会访问[希腊的](../Page/希腊.md "wikilink")[奥林匹亚](../Page/奥林匹亚.md "wikilink")，古代奥林匹克运动的发源地。他认为宏扬古代奥林匹克精神可以促进国际体育运动的发展。

在1892年12月25日，皮埃尔·德·顾拜旦发表演讲，在演讲中首次提出“复兴奥林匹克运动”。

1894年在[巴黎举办了国际体育会议](../Page/巴黎.md "wikilink")，决定在希腊创办第一届现代奥运会，并规定每4年举行一次。

1894年6月23日，[国际奥林匹克委员会正式成立](../Page/国际奥林匹克委员会.md "wikilink")，当时希腊文学家[泽麦特里乌斯·维凯拉斯担任](../Page/泽麦特里乌斯·维凯拉斯.md "wikilink")，而顾拜旦则担任国际奥委会[秘书长](../Page/秘书长.md "wikilink")。

顾拜旦在1896年至1925年期间担任[国际奥委会主席](../Page/国际奥委会.md "wikilink")。

顾拜旦参与组织了[1900年夏季奥林匹克运动会](../Page/1900年夏季奥林匹克运动会.md "wikilink")、1924年[夏季和](../Page/1924年夏季奥林匹克运动会.md "wikilink")[冬季奥林匹克运动会](../Page/1924年冬季奥林匹克运动会.md "wikilink")。

## 参考文献

  -
  -
  -
## 外部链接

  - [The International Pierre De Coubertin Committee
    (CIPC)](http://www.coubertin.ch/) – Lausanne
  - [Coubertin reader of
    Flaubert](https://web.archive.org/web/20071128113734/http://www.sporteslettres.net/archives/coubertin/Coubertin-reader-of-Flaubert.pdf)
  - [The Wenlock Olympian
    Society](https://web.archive.org/web/20090410111109/http://www.wenlock-olympian-society.org.uk/archives/index.shtml)
  - [Discourse of Pierre de Coubertin at Sorbonne announcing the
    restoring of the Olympic
    games](https://web.archive.org/web/20080817053852/http://www.litteratureaudio.com/index.php/2008/07/26/coubertin-pierre-de-la-renovation-des-jeux-olympiques/)

[Category:法国教育家](../Category/法国教育家.md "wikilink")
[Category:法国历史学家](../Category/法国历史学家.md "wikilink")
[Category:巴黎政治大学校友](../Category/巴黎政治大学校友.md "wikilink")
[Category:国际奥林匹克委员会主席](../Category/国际奥林匹克委员会主席.md "wikilink")
[Category:法國男爵](../Category/法國男爵.md "wikilink")

1.  Hill, p. 5
2.