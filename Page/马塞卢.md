**馬賽魯**（Maseru/Masero）是[莱索托的首都](../Page/莱索托.md "wikilink")，是全球少有設置於邊境線的首都之一，临近[南非边境](../Page/南非.md "wikilink")，也是该国唯一成型的城市，人口180,000（2004年）。馬賽魯位于29°20'
S, 27°30' E。该城仅有的工业是地毯和蜡烛的制造。

城内只有一条比较繁华的街区国王大道（Kingsway）。城市很多建筑为别国援建，其中[中华人民共和国援建了莱索托的会议中心](../Page/中华人民共和国.md "wikilink")、[图书馆](../Page/图书馆.md "wikilink")，[北朝鲜援建了体育场和一些政府办公楼](../Page/北朝鲜.md "wikilink")。

<File:Maseru> Leotho main south.jpg|郊區貧民窟
[File:BasothoHatShop.jpg|Hat超商](File:BasothoHatShop.jpg%7CHat超商)
[File:KingswayMaseru.jpg|商業區](File:KingswayMaseru.jpg%7C商業區)
[File:GantsiHighStreet.jpg|Gantsi高速公路](File:GantsiHighStreet.jpg%7CGantsi高速公路)

[Category:非洲首都](../Category/非洲首都.md "wikilink")
[M](../Category/莱索托城市.md "wikilink")
[Category:1869年建立的聚居地](../Category/1869年建立的聚居地.md "wikilink")
[Category:1869年非洲建立](../Category/1869年非洲建立.md "wikilink")