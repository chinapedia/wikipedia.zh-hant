**INFOCOMP電腦科學期刊**為一份國際性的科學[期刊](../Page/期刊.md "wikilink")（索引指標、印刷出版、[季刊](../Page/季刊.md "wikilink")、連續流通）。所注重的領域為（但不限於此）：

  - [人工智能](../Page/人工智能.md "wikilink")
  - [組合最佳化與](../Page/組合最佳化.md "wikilink")[啟發式演算法](../Page/啟發式演算法.md "wikilink")
  - [计算机图形学](../Page/计算机图形学.md "wikilink")、[图像处理與](../Page/图像处理.md "wikilink")[虚拟现实](../Page/虚拟现实.md "wikilink")
  - [資料庫](../Page/数据库.md "wikilink")
  - [圖形理論](../Page/圖形理論.md "wikilink")、[应用数学與](../Page/应用数学.md "wikilink")[計算理論](../Page/計算理論.md "wikilink")
  - [超媒體與](../Page/超媒體.md "wikilink")[多媒体](../Page/多媒体.md "wikilink")
  - [資訊系統](../Page/資訊系統.md "wikilink")
  - [教育資訊技術](../Page/教育資訊技術.md "wikilink")
  - [软件工程](../Page/软件工程.md "wikilink")

## 出版

**INFOCOMP電腦科學期刊**是由位在[巴西](../Page/巴西.md "wikilink")[米纳斯吉拉斯之拉弗哈斯聯邦大學](../Page/米纳斯吉拉斯.md "wikilink")（[UFLA](../Page/UFLA.md "wikilink")）電子計算機科學系所出版的。本期刊索引指標也列在[DOAJ](../Page/DOAJ.md "wikilink")、[INSPEC](../Page/INSPEC.md "wikilink")-[IEE](../Page/IEE.md "wikilink")、[DEST](../Page/DEST.md "wikilink")、
[EBSCO](../Page/EBSCO.md "wikilink")、[ULRICHS](../Page/ULRICHS.md "wikilink")、[IS
Journals](../Page/IS_Journals.md "wikilink")、[IS
World](../Page/IS_World.md "wikilink")，及其他協會上。

本期刊是屬於季刊形式，出版時間在3月、6月、9月、及12月，有網路版及印刷版。網路版是採[开放获取形式](../Page/开放获取.md "wikilink")，不僅不須要訂閱也不須要註冊。所有上述的出版期數均可在網路上獲取。所有投稿文章均可在網路上經由SBC（[巴西電腦協會](../Page/巴西電腦協會.md "wikilink")）之[JEMS系統](../Page/JEMS.md "wikilink")（[EDAS之子系統](../Page/EDAS.md "wikilink")）來提交。

## 歷史沿革

*INFOCOMP電腦科學期刊*在1999年第一次出版。

## 參見

  - [開放獲取期刊列表](../Page/開放獲取期刊列表.md "wikilink")

## 參考文獻

  -
## 外部連結

  - [INFOCOMP Journal of Computer
    Science](https://web.archive.org/web/20060524065911/http://www.dcc.ufla.br/infocomp/index.eng.html)

[Category:计算机科学期刊](../Category/计算机科学期刊.md "wikilink")
[Category:教育技術](../Category/教育技術.md "wikilink")
[Category:開放獲取期刊](../Category/開放獲取期刊.md "wikilink")
[Category:1999年建立的出版物](../Category/1999年建立的出版物.md "wikilink")
[Category:季期刊](../Category/季期刊.md "wikilink")