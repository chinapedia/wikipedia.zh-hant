**福坦莫大学**（Fordham
University），又译作**福特汉姆大学**、**福德汉姆大学**或**復旦大學**，是一所位于美国[纽约市的歷史悠久的私立大学](../Page/纽约市.md "wikilink")。它在纽约市有三个校区，其中最著名的是[曼哈顿中城的](../Page/曼哈顿.md "wikilink")[林肯中心校区](../Page/林肯中心.md "wikilink")，其法学院和商学院均位于此。大學目前在校人數約為15,000人，在校師生比為1：14，48%的校內課程實行小班教學。2017年美國新聞創業學位列第13位，金融學位列第14位，市場營銷學位列第12位，數據分析學位列第11位和全美第1名的劇院表演學程。福坦莫法学院在纽约市大型律师事务所中拥有强大的校友力量。福坦莫法学院的校友人数在美国前五、前25和前50的律师事务所中均列第六位。该学院毕业生在大型律所中就职的优异表现主要得益于其地理位置的优势。

該校與達特茅斯學院，聖母大學，聖十字學院等一樣，都是小班制大學，學校人數很少，另外教會大學的性質也使得其在以總體論文發表數量，年均畢業生數量等為重要參數的國際排名中不佔優勢。2009年國立巴黎高等礦業學校的在檢視了逾3000所世界大學後，依據其畢業生在世界500強企業就業的情況挑選了700所世界大學進行排名，大學位列全世界第63名。之後更新的排名中，大學位列全球第16名。\[1\]薪酬基準網站Emolument.com通過分析來自10,900名持有MBA學位的畢業生的薪酬數據，公布2018年產生薪酬最高的MBA畢業生的前20名世界商學院排名第17名，僅次於第16名倫敦商學院,高於第18名牛津大學商學院。\[2\]

福特翰大學的知名校友包括了前美國中央情報局局長William J. Casey，美前司法部長John Mitchell、美國智財局局長John
Brennan、美聯儲備紐約分行第七任行長，奧斯卡金像獎與金球獎影帝丹佐華盛頓，美國榮譽勳章得主Thomas J. Kelly、Martin
Thomas McMahon及Robert Charles Murray，普立茲獎得主Jim Dwyer與Loretta
Tofani，諾貝爾獎得主Victor
Hess及兩屆奧運會金牌得主賽道運動員湯姆考特尼。多位美國參議員、眾議員、市長、州長、法官、兩位國家元首：納米比亞總統哈吉格林布和美國總統唐納德川普。還有多位校友擔任財星500大企業執行長包含：美國排名第二的Cravath律師事務所合夥人Michael
S. Goldman ，Burberry 執行長Rose Marie Bravo，可口可樂和摩根大通等公司的首席執行官兼董事Maria
Elena Lagomasino，Xerox 施樂董事長兼首席執行官Anne M. Mulcahy，Airbus空中客車首席運營官John
Leahy，美聯社（AP）總裁兼首席執行官Louis D. Boccardi，美國最大的私人能源公司Consolidated
Edison執行長，Countrywide Financial Corp美國最大的房地產抵押貸款公司創辦人，Don
Valentine 紅杉資本的風險投資家Apple和Google的原始投資者，惠靈頓馬拉（紐約巨人隊老闆）以及億萬富翁企業家Eugene
Shvidler和Lorenzo Mendoza都是校友。

福坦莫大学授予学士、硕士和博士学位。福坦莫在[中国和](../Page/中国.md "wikilink")[英国都有合作项目](../Page/英国.md "wikilink")。

## 历史

[Fordham's_campuses-fr.svg](https://zh.wikipedia.org/wiki/File:Fordham's_campuses-fr.svg "fig:Fordham's_campuses-fr.svg")
福坦莫大学由[天主教纽约总教区于](../Page/天主教纽约总教区.md "wikilink")1841年创办，初名为圣约翰学院（St.
Johns
College）（与纽约市[皇后区的](../Page/皇后区.md "wikilink")[圣约翰大学](../Page/圣若望大学.md "wikilink")
St. Johns
University无关），随后由[耶稣会接手管理](../Page/耶稣会.md "wikilink")。1905年建立法学院，1907年更名为Fordham
University，现由非教会人士管理。学校曾经有医学院，但是因为有学生毕业后从事堕胎，跟天主教精神不符评级下降，最终于1919年关闭了该学院。\[3\]\[4\]

## 译名

因英语“Fordham”里的h不发音，所以将校名音译成“福特汉姆”或“福德汉姆”都是错误的；\[5\]但也有根据正确发音音译为福德姆大学、福尔德姆大学的。\[6\]因为发音与著名的位于上海的[复旦大学相近](../Page/复旦大学.md "wikilink")，也有譯作“复旦大学”\[7\]\[8\]\[9\]\[10\]。

## 校园

福坦莫大学有三个校区，分别是位于[布朗克斯的](../Page/布朗克斯.md "wikilink")[玫瑰山](../Page/玫瑰山.md "wikilink")（1841-），位于[曼哈顿的](../Page/曼哈顿.md "wikilink")[林肯中心](../Page/林肯中心.md "wikilink")（1961-），和位于[威斯特徹斯特县](../Page/威斯特徹斯特县.md "wikilink")的一个校区。2010年美国电影[华尔街2：金钱永不眠中](../Page/华尔街2：金钱永不眠.md "wikilink")[迈克尔·道格拉斯的几场戏即在福坦莫大学校园中摄制](../Page/迈克尔·道格拉斯.md "wikilink")。

### 玫瑰山校园

玫瑰山校园目前是福坦莫的主校区，有除了法学院以外的所有专业。福坦莫没有工学院，但是通过与[哥伦比亚大学和](../Page/哥伦比亚大学.md "wikilink")[凯斯西储大学合作](../Page/凯斯西储大学.md "wikilink")，可以让学生通过特定项目获得工学教育和合作方学位。\[11\]福坦莫有新建理学院的计划，但是因为经费不足而暂时搁浅。\[12\]

<File:Fordham> University 02.JPG|Keating Hall <File:Fordham> University
03.JPG|本科商学院 <file:Fordham> University 05.JPG|食堂 <file:Fordham>
University 06.JPG|校长办公室，最早的一栋建筑 <file:Fordham> University 08.JPG|学校教堂
<file:Fordham> University 07.JPG|教堂内部 <file:Fordham> University
11.JPG|神学院 <file:Fordham> University 12.JPG|老宿舍 <file:Fordham>
University New Dorm.JPG|新宿舍 <file:Fordham> University 13.JPG|草坪
<file:Fordham> University 14.JPG|图书馆 <file:Fordham> University
15.JPG|火车站

### 林肯中心校园

林肯中心校园目前有整个法学院，部分商学院、社会服务学院、和教育学院在此。\[13\]本科生在这里也有一个校区，被称为福坦莫林肯中心学院（Fordham
College at Lincoln
Center）。\[14\]学生可通过校内巴士来往玫瑰山和林肯中心，也可以坐[地铁或](../Page/纽约地铁.md "wikilink")[大都会北方通勤铁路](../Page/大都会北方通勤铁路.md "wikilink")。\[15\]校园中央偏西有一块不属于学校的住宅楼“阿尔弗雷德公寓”\[16\]，西北和西南以后也会为了筹集资金建起新的住宅楼。\[17\]
林肯中心校园起源于1926年法学院为了应对[美国律师协会增加律师资格的要求](../Page/美国律师协会.md "wikilink")，而专门为没有本科学历的法学院录取者准备的短期大学速成班。该学院最早位于纽约下城区，后与法学院一起迁往现址。\[18\]

因为布朗克斯校园周边治安不佳，在考虑过将校园搬到郊区的可能性\[19\]以及发现纽约市区的吸引力加强后\[20\]，福坦莫准备大举扩建其林肯中心校园。\[21\]作为该计划的第一步，福坦莫通过出售校园西北角的一地块，获得了资金来建设新的法学院大楼和宿舍，虽然此举遭到了周边居民对于当初福坦莫获得此地条件和与纽约市政府所签合同的质疑。\[22\]阿尔弗雷德公寓的居民甚至诉诸法律希望来阻止福坦莫扩建，但官司已被法院驳回。\[23\]

在新法学院和宿舍在2014年投入使用后，福坦莫准备在2032年以前继续在林肯中心建设新的社会服务学院和教育学院大楼、新的商学院大楼、新的图书馆。学校西南角的一地块也有可能被出售以换取建设资金。\[24\]

<File:Fordham> University Lincoln Center
Campus.JPG|林肯中心校园，包括法学院老楼，法学院新楼及新宿舍，一栋宿舍楼，和除法学院外其他专业本科生与研究生共用的
Lowenstein 大楼 <File:Fordham> Law new building 2014.jpg|法学院

## 排名

该大学在2012年《[美国新闻与世界报道](../Page/美国新闻与世界报道.md "wikilink")》之本科综合排名为全美第53名，2013为58名。\[25\]其法学院排名为全美第29\[26\]，商学院的金融专业为全美第14、营销专业为第23\[27\]。2012年，该校住宿费被评为全美最高\[28\]，校园伙食被评为全美最差\[29\]。2013年，该校被评为全美最贵高校第十二名。\[30\]
Collegechoice千禧一代排名前50位的大學全美第19 \[31\]

## 院系设置

仅提供大学部教育的学院有：玫瑰山学院、林肯中心学院、加贝利商学院、继续教育学院。
仅提供研究生教育的学院有：法学院、工商管理学院、文理学院、社会服务学院、教育学院、宗教学院。\[32\]
两个商学院将合并，同时兼顾本科生和研究生教育。\[33\]

### 法学院

福坦莫大学法学院创建于1905年，在纽约市仅次于[哥伦比亚大学和](../Page/哥伦比亚大学.md "wikilink")[纽约大学的法学院](../Page/纽约大学.md "wikilink")。福坦莫法学院授予[法律博士](../Page/法律博士.md "wikilink")（J.D.）、[法学硕士](../Page/法学硕士.md "wikilink")（LL.M.）\[34\]、[法学博士](../Page/法学博士.md "wikilink")（S.J.D.）学位\[35\]。

福坦莫法学院在2007年度[美国新闻与世界报道中排第二十五名](../Page/美国新闻与世界报道.md "wikilink")；按照入学的LSAT成绩排第十五名。为福坦莫法学院输送学生人数最多的十所大学包括五所[常春藤盟校](../Page/常春藤盟校.md "wikilink")：[哥伦比亚大学](../Page/哥伦比亚大学.md "wikilink")、[康奈尔大学](../Page/康奈尔大学.md "wikilink")、[宾夕法尼亚大学](../Page/宾夕法尼亚大学.md "wikilink")、[布朗大学和](../Page/布朗大学.md "wikilink")[普林斯顿大学](../Page/普林斯顿大学.md "wikilink")；还包括[纽约大学](../Page/纽约大学.md "wikilink")、[乔治城大学](../Page/乔治城大学.md "wikilink")、[密歇根大学](../Page/密歇根大学.md "wikilink")、[波士顿大学和](../Page/波士顿大学.md "wikilink")[波士顿学院](../Page/波士顿学院.md "wikilink")。

福坦莫法学院在[纽约市大型律师事务所中拥有强大的校友力量](../Page/纽约市.md "wikilink")。福坦莫法学院的校友人数在美国前五、前25和前50的律师事务所中均列第六位。
根据 2006 年的美国 100 大律所的数据，福坦莫法学院的学生在全美 50
名的律所中找到工作的数目是全国第9，前25名的律所里是第8，前十的律所里是第9。

福坦莫法学院在美国流行文化中也留下了印迹：2007年的第80届奥斯卡金像奖法律惊悚片《[全面反击](../Page/全面反击.md "wikilink")》中，[乔治·克鲁尼所扮演的律师Michael](../Page/乔治·克鲁尼.md "wikilink")
Clayton就毕业于福坦莫法学院。

法学院和[巴黎一大](../Page/巴黎一大.md "wikilink")、[二大](../Page/巴黎二大.md "wikilink")、[复旦](../Page/复旦大学.md "wikilink")、[中国政法](../Page/中国政法大学.md "wikilink")、[华东政法](../Page/华东政法大学.md "wikilink")、[国立台湾大学](../Page/国立台湾大学.md "wikilink")、[早稻田大学等学校签订了交流协议](../Page/早稻田大学.md "wikilink")。\[36\]\[37\]

### 加贝利商学院

福坦莫原有两个商学院。加贝利商学院（Gabelli School of
Business，简称GSB）提供本科生教学。\[38\]工商管理学院（Graduate
School of Business
Administration，简称GSBA）提供本科以上的学位教育。2014年5月学校宣布两个商学院将合并，同时兼顾本科生和研究生教育，并首次在林肯中心校区开设本科教育。\[39\]2015年2月两个学院完成合并，合并后的新商学院仍然继承了加贝利商学院的名称。\[40\]

GSBA的EMBA于2013年在世界排名中位居第66名。\[41\]其创建于1969年，于1998年和[北京大学](../Page/北京大学.md "wikilink")[中国经济研究中心](../Page/中国经济研究中心.md "wikilink")（CCER）合作创办的北大国际MBA（BiMBA,Beijing
International
MBA）是中国最优秀的商学院项目之一，其中方教授包括[林毅夫](../Page/林毅夫.md "wikilink")、[周其仁](../Page/周其仁.md "wikilink")、[海闻](../Page/海闻.md "wikilink")、[杨壮等](../Page/杨壮.md "wikilink")。校友包括[财新传媒总发行人兼总编辑](../Page/财新传媒.md "wikilink")、前《财经》杂志主编[胡舒立女士](../Page/胡舒立.md "wikilink")。2009年5月16日，福坦莫在第164届毕业典礼上授予[世界银行副行长及首席经济学家](../Page/世界银行.md "wikilink")[林毅夫教授](../Page/林毅夫.md "wikilink")[荣誉博士学位](../Page/荣誉博士.md "wikilink")。一同获此荣誉的有纽约市长[彭博等](../Page/彭博.md "wikilink")。\[42\]

2011年3月中旬，北大国际（BiMBA）与福坦莫大学在北京香格里拉饭店举办了盛大的百名校友鸡尾酒会。北京大学[国家发展研究院常务副院长](../Page/国家发展研究院.md "wikilink")[巫和懋教授](../Page/巫和懋.md "wikilink")、北大国际（BiMBA）院长杨壮教授、福坦莫大学副校长Stephen
Freedman和各届校友、嘉宾共百余人出席了此次活动。\[43\]

## 学生活动

### 体育竞技

[thumb](../Page/file:Fordham_University_04.JPG.md "wikilink")
福坦莫大学拥有23支体育竞技项目。福坦莫的吉祥物是[大角公羊](../Page/大角羊.md "wikilink")（ram），代表色是褐红色和白色。福坦莫的竞技队大部分都在[国家大学体育协会第一分区中比赛](../Page/国家大学体育协会.md "wikilink")，也是的成员之一。福坦莫的美式足球项目参加了[爱国者联盟](../Page/爱国者联盟.md "wikilink")，其是[国家大学体育协会中学术实力仅次于](../Page/国家大学体育协会.md "wikilink")[常春藤联盟的组织](../Page/常春藤联盟.md "wikilink")。\[44\]\[45\]

福坦莫美式足球队在1929年获得过全国冠军，1941及1942两次进入决赛，两次取得爱国者联盟冠军（2002级2007），并参加了次年的[国家大学体育协会第一分区足球比赛](../Page/国家大学体育协会.md "wikilink")。球队是美国大学校队里获胜次数排名第15名。\[46\]福坦莫美式足球队的强大更促成了“常春藤联盟”一词的出现。《纽约先驱论坛报》的体育记者
Caswell Adams
在将福坦莫球队的表现与[耶鲁大学及](../Page/耶鲁大学.md "wikilink")[普林斯顿大学校队进行比较后](../Page/普林斯顿大学.md "wikilink")，称后两者只是“（柔嫩的）常春藤队伍”。\[47\]\[48\]

## 知名教授

  - Victor Francis Hess- 奧地利美國物理學家，諾貝爾物理學獎獲得者，他發現了宇宙射線\[1\]。

<!-- end list -->

  - Anne Anastasi-美國國家科學獎得主，美國心理學家，因其開創性的心理測量學開發而聞名。

<!-- end list -->

  - Marshall McLuhan (1911-1980) - 英国文学教授
    (1967-68)，“[地球村](../Page/地球村.md "wikilink")”一词的创造者。

<!-- end list -->

  - Ivo Banac - 耶魯大學歷史教授

<!-- end list -->

  - Matt Gold - 美國福坦莫大學（Fordham University）法律兼任教授、前美國貿易代表副助理高德

<!-- end list -->

  - Paul D. McNelis - 多次到台灣的國立政治大學經濟系訪問。

<!-- end list -->

  - 杨壮 - 商学院管理学教授，北大国际（Beijing International MBA at Peking University,
    BiMBA）创始人、美方院长。

<!-- end list -->

  - John D. Finnerty - 金融系教授，2011年入选固定收益分析师学会（Fixed Income Analysts
    Society）名人堂。\[49\]

<!-- end list -->

  - James R. Lothian - 特聘教授（Distiguished
    Professor），前《》编辑，曾任职于[美国国家经济研究所](../Page/美国国家经济研究所.md "wikilink")，也曾是[国际货币基金](../Page/国际货币基金.md "wikilink")、[亚特兰大美国联邦储备银行](../Page/亚特兰大美国联邦储备银行.md "wikilink")、荷兰[马斯特里赫特大学访问学者](../Page/马斯特里赫特大学.md "wikilink")。\[50\]

## 知名校友

### 商界

  - Rose Marie Bravo- Burberry
    執行長紐約華爾街日報將她列入2004年度50大名人堂，\[5\]“財富”雜誌在2004年和2005年在美國以外的“50強商界女性”榜單中名列第13位。
  - Kevin Burke- Con Edison董事長，總裁兼首席執行官 聯合愛迪生（Consolidated
    Edison，常簡稱為Con Edison）是美國最大的私人能源公司
  - Maria Elena Lagomasino- 擔任可口可樂和摩根大通等公司的首席執行官兼董事(2001–2005)。
    2007年西班牙裔年度商業女性。
  - Eileen Howard Boone - CVS藥局（CVS pharmacy）美國藥妝店連鎖企業高級副總裁，CVS健康基金會主席
  - Don Valentine - 紅杉資本的風險投資家 ;[Apple
    Computer](../Page/Apple_Computer.md "wikilink")，[Oracle
    Corporation](../Page/Oracle_Corporation.md "wikilink")，思科，[Google和](../Page/Google.md "wikilink")[YouTube的原始投資者](../Page/YouTube.md "wikilink")
  - Ann M. Mulcahy - 執行長 CEO of [Xerox](../Page/Xerox.md "wikilink")
    Corporation 財星500大企業全錄公司（Xerox，NYSE：XRX）
  - John Leahy - 欧洲民航飞机制造公司
    [Airbus空中客車首席運營官](../Page/Airbus.md "wikilink")
  - Stephen J. Hemsley - 執行長 CEO of
    [UnitedHealth](../Page/UnitedHealth.md "wikilink") Group
    美國全球營業額最高健康保險及衛生資訊科技公司
  - Lorenzo Mendoza- 委內瑞拉億萬富翁，Polar企業首席執行官
  - Robert E. Campbell- Johnson & Johnson 強生公司副董事長
  - Robert B. McKeon- Chairman of Veritas Capital 管理的資產約為88億美元。
  - Angelo Mozilo- 美國國家金融服務公司聯合創始人兼首席執行官（英語：Countrywide Financial
    Corp）美國最大的房地產抵押貸款公司，財富500強中第146名
  - John Mara- 主席, COO, and co-owner of the New York Giants
    紐約巨人隊國家橄欖球聯盟（NFL）
  - Eugene Shvidler - 俄裔美國億萬富翁，國際石油大亨
  - Rosemary Vrablic- Deutsche Bank
    [德意志銀行美國私人財富管理業務的董事總經理兼高級私人銀行家](../Page/德意志銀行.md "wikilink")
  - Ellen Alemany- 蘇格蘭皇家銀行 RBS Citizens Financial Group和RBS
    Americas前任董事長兼首席執行官
  - Raul Alarcon- 西班牙廣播系統首席執行官
  - Louis Boccardi - [美聯社美國乃至於世界最大的通讯社](../Page/美聯社.md "wikilink")
    CEO（1985-2003）; 1994 - 2003年[普立茲獎會成員](../Page/普立茲獎.md "wikilink")
  - Maxxie Goldstein - MEYVYN 聯合創始人，2018 福布斯 30 Under 30 每年收入達到200萬美元

### 法律、政治

  - [唐納·約翰·川普](../Page/特朗普.md "wikilink")（Donald John
    Trump）美國第45任總統，曾就读於福坦莫大學两年，后转学至[宾夕法尼亚大学](../Page/宾夕法尼亚大学.md "wikilink")。\[51\]

<!-- end list -->

  - 傑羅丁·安妮·費拉羅（Geraldine Anne Ferraro，1935年8月25日－2011年3月26日）
    美國律師和民主黨政治家，曾在美國眾議院任職。
    1984年，她是代表美國主要政黨的第一位女性副總統候選人。

<!-- end list -->

  - John O. Brennan- 國土安全部副國家安全顧問（2009-2013）和總統奧巴馬總統的中情局局長 (CIA)

<!-- end list -->

  - [安德鲁·库默](../Page/安德鲁·库默.md "wikilink") (Andrew Mark Cuomo) -
    第56任[纽约州州长](../Page/纽约州.md "wikilink")。

<!-- end list -->

  - Martin H. Glynn - 第40 任[紐約州州長](../Page/紐約州.md "wikilink")

<!-- end list -->

  - William J. Casey- CIA 中央情報局局長 1981-1987

<!-- end list -->

  - John N. Mitchell -
    [尼克松总统任内的](../Page/尼克松.md "wikilink")[美国司法部长](../Page/美国司法部长.md "wikilink")(United
    States Attorney General)。

<!-- end list -->

  - Bernard Michael Shanley
    他與美國總統德懷特·D·艾森豪威爾合作。他曾在艾森豪威爾總統任職白宮副參謀長，任命秘書（1955-1957）和特別顧問（1953-1955）

<!-- end list -->

  - Jerrold Nadler - [美国众议院议员](../Page/美国众议院.md "wikilink")(1992-
    )。毕业于福坦莫大学法学院和[哥伦比亚大学](../Page/哥伦比亚大学.md "wikilink")。

<!-- end list -->

  - Ann McLaughlin Korologos- 美國勞工部長

<!-- end list -->

  - Stuart M. Bernstein - 美国纽约南区破产法院 (US Bankruptcy Court, Southern
    District of New York) 法官，福坦莫大学法学院1975年毕业生。

<!-- end list -->

  - [陈卓光](../Page/陈卓光.md "wikilink")(Denny Chin) -
    [美国联邦第二巡回上诉法院大法官](../Page/美国联邦第二巡回上诉法院.md "wikilink")，1978年毕业于福坦莫大学法学院，1975年毕业于[普林斯顿大学](../Page/普林斯顿大学.md "wikilink")。陈卓光在1994年8月到2009年4月间担任[美国纽约南区联邦法院法官](../Page/美国纽约南区联邦法院.md "wikilink")，并主审[麦道夫案](../Page/麦道夫.md "wikilink")
    (Bernard Madoff) 等著名大案。

<!-- end list -->

  - Loretta A. Preska -
    [美国纽约南区联邦法院大法官](../Page/美国纽约南区联邦法院.md "wikilink")(Chief
    Judge)，福坦莫大学法学院1973年毕业生。

<!-- end list -->

  - Denis F. Cronin - 美国Vinson &
    Elkins律师事务所资深合伙人，福坦莫大学法学院1972年毕业生。Cronin先生曾在美国排名第一的律师事务所Wachtell
    Lipton工作21年，其中16年担任合伙人，6年担任管理合伙人(Managing Partner)。

<!-- end list -->

  - Michael S. Goldman - 美国排名第二的Cravath律师事务所合伙人。

<!-- end list -->

  - Neil Breslin - 紐約州參議員紐約第42區（1997-2002），第46區（2003-12），第44區（2013-）

<!-- end list -->

  - Michael Gianaris -紐約州參議員，紐約第12區（2011-）

<!-- end list -->

  - Clyde Mitchell - 美国White &
    Case律师事务所荣休合伙人，曾在该所服务三十年。福坦莫大学法学院1959年毕业生。

<!-- end list -->

  - Harold F. Moore -
    美国Skadden律师事务所资深合伙人，获福坦莫大学学士(1968)、硕士(1970)和博士(1971)学位。

<!-- end list -->

  - [司徒文](../Page/司徒文.md "wikilink")（William A. "Bill" Stanton） -
    前美國在台協會台北辦事處處長，福坦莫大学学士。

<!-- end list -->

  - Hage Geingob - Namibia納米比亞第一任總理。第三任和納米比亞現任總統。

<!-- end list -->

  - Ray McGovern- 前中情局分析師，政治活動家

<!-- end list -->

  - Daniel Ragsdale-美國移民和海關執法局副局長

<!-- end list -->

  - John Granville-美國國際開發署外交官

<!-- end list -->

  - Gen. Jack Keane-美國陸軍退役的四星將軍和前副參謀長

<!-- end list -->

  - Martin T. McMahon - 美國陸軍少將，美國榮譽勳章獲得者，美國巴拉圭大使，紐約州參議員，紐約州議會議員

<!-- end list -->

  - William R. Meagher - 美国著名律师事务所Skadden, Arps, Slate, Meagher &
    Flom创始合伙人之一。

### 财经

  - E. Gerald Corrigan -
    [美联储纽约分行第七任行长](../Page/美联储.md "wikilink")(1985-1993)，[巴塞尔银行监理委员会主席](../Page/巴塞尔银行监理委员会.md "wikilink")(1991-1993)，中国[银监会国际咨询委员会委员](../Page/银监会.md "wikilink")。福坦莫大学经济学博士(1971年)。

<!-- end list -->

  - Mario GabellI - GSB ‘65, GAMCO Investor 的創始人，董事長，CEO

<!-- end list -->

  - Joe Moglia - [多倫多道明銀行](../Page/多倫多道明銀行.md "wikilink")
    Toronto-Dominion Bank 北美第六大銀行 TD Ameritrade Holding
    Corporation董事長兼前首席執行官

<!-- end list -->

  - 胡舒立 -
    北大国际EMBA校友，[财新传媒总发行人兼总编辑](../Page/财新传媒.md "wikilink")、[中山大学传播与设计学院院长](../Page/中山大学.md "wikilink")，前[《财经》杂志主编](../Page/《财经》.md "wikilink")。

<!-- end list -->

  - Karl Kilb - [彭博社](../Page/彭博社.md "wikilink")(Bloomberg)法律总监(General
    Counsel)

<!-- end list -->

  - Patricia David - [摩根大通JPMorgan](../Page/摩根大通.md "wikilink")
    Chase董事總經理兼全球負責人

<!-- end list -->

  - Ludwig D’Angelo - 執行主任 - [摩根大通](../Page/摩根大通.md "wikilink") JP
    Morgan，CCB Technology Production Management

<!-- end list -->

  - Kathleen Brown - [高盛](../Page/高盛.md "wikilink") (Goldman Sachs)
    顧問，高盛公共財經（西部地區）負責人

### 学界

  - William J. McGill - 哥倫比亞大學校長
  - Dennis Di Lorenzo - 紐約大學專業研究School of Professional Studies學院院長
  - John Sexton - [纽约大学](../Page/纽约大学.md "wikilink")(NYU)第十五任校长(2002-
    )，曾任[纽约大学法学院院长](../Page/纽约大学法学院.md "wikilink")(1988-2002)，[美联储纽约分行董事会主席](../Page/美联储.md "wikilink")(2003-2007)。获福坦莫大学学士(1963)、硕士(1965)、博士(1978)学位。
  - George
    Casella-(1952-2012-06-18)，[佛罗里达大学统计系著名教授](../Page/佛罗里达大学.md "wikilink")。
  - 王巍 - [长江商学院金融学教授](../Page/长江商学院.md "wikilink")，福坦莫大学经济学博士(1992年)。

### 藝術,演藝跟體育

  - Denzel Washington ([丹泽尔·华盛顿](../Page/丹泽尔·华盛顿.md "wikilink"), 1954- )
    - 畢業於福坦莫,美国著名影星，凭《训练日》中的演出获2002年奥斯卡最佳男主角奖。1977年获福坦莫大学戏剧与传媒学学士。

<!-- end list -->

  - Alan Alda - 美國演員，導演，編劇和作者。曾七次獲得艾美獎和金球獎得主

<!-- end list -->

  - Don DeLillo - 國家圖書獎和筆/福克納獎獲獎作者

<!-- end list -->

  - Mike Breen- 美國廣播公司的NBA美國體育評論員，也是MSG網絡上紐約尼克斯隊比賽的首席播音員。

<!-- end list -->

  - Vin Scully- 艾美獎獲獎播報員 洛杉磯道奇隊運動員;棒球名人堂;無線電名人堂

<!-- end list -->

  - Loretta Tofani-普利策獎得主記者

<!-- end list -->

  - Alice Smith- 格萊美獎提名歌手

<!-- end list -->

  - Tom Courtney - 在1956年夏季奧運會上獲得兩枚金牌，在880碼比賽創造了世界紀錄

<!-- end list -->

  - Robert Sean Leonard- 金球獎獲獎電視節目演員

<!-- end list -->

  - Chrissy Costanza ([克丽希•克斯丹萨](../Page/克丽希•克斯丹萨.md "wikilink"), 1995-
    ) - 畢業於福坦莫,美國著名網絡歌手,在2011年加入 Against The
    Current(ATC)。2012年在YouTube大受好評。

<!-- end list -->

  - Charles Osgood - 三次艾美獎和兩次Peabody獲獎記者

<!-- end list -->

  - Mary Higgins Clark- 國際和紐約時報暢銷作家超過50個懸念小說，40多年的懸疑小說女王

<!-- end list -->

  - Patricia Clarkson - 提名奧斯卡最佳女配角獎和金球獎

<!-- end list -->

  - Taylor Schilling- Netflix原創喜劇劇集“橙色是新黑”（2013年至今）的角色扮演Piper Chapman
  - Olivia Nuzzi - 政治記者，擔任紐約雜誌的華盛頓記者。

<!-- end list -->

  - Amanda Randolph Hearst-
    [阿曼達·藍道夫·赫茲](../Page/阿曼達·藍道夫·赫茲.md "wikilink")
    美國社交名媛、威廉·藍道夫·赫茲的媒體企業集團赫茲國際集團的繼承人《美麗佳人》的副市場編輯

<!-- end list -->

  - Archduchess Charlotte of Austria- 奧地利皇帝查理一世的女兒

<!-- end list -->

  - Jazmin Grace Grimaldi 摩納哥王子阿爾貝二世和塔瑪拉羅托洛的女兒

<File:Alan> Alda MASH 1972.JPG|[Alan
Alda](../Page/Alan_Alda.md "wikilink"), [艾美獎](../Page/艾美獎.md "wikilink")
and [全球獎](../Page/全球獎.md "wikilink")-提名演員 <File:Esteban>
Bellan.jpg|[Steve
Bellán](../Page/Steve_Bellán.md "wikilink"),第一古巴人和第一拉丁美洲人打大聯盟棒球
<File:John> Brennan CIA official portrait.jpg|[John O.
Brennan](../Page/John_O._Brennan.md "wikilink"),
[中央情報局局長](../Page/中央情報局局長.md "wikilink")
<File:William-Casey.jpg>|[William J.
Casey](../Page/William_J._Casey.md "wikilink"),
[中央情報局局長](../Page/中央情報局局長.md "wikilink")
<File:Mary> Higgins Clark at the Mazza Museum.jpg|[Mary Higgins
Clark](../Page/Mary_Higgins_Clark.md "wikilink"), 小說家 <File:Patricia>
Clarkson 2009 Whatever Works portrait.jpg|[Patricia
Clarkson](../Page/Patricia_Clarkson.md "wikilink"),
[奧斯卡金像獎](../Page/奧斯卡金像獎.md "wikilink")-提名演員
<File:Andrew> Cuomo by Pat Arnow cropped.jpeg|[Andrew
Cuomo](../Page/Andrew_Cuomo.md "wikilink"), 56th
[紐約州州長](../Page/紐約州州長.md "wikilink") <File:Lana> Del
Rey Cannes 2012.jpg|[Lana Del Rey](../Page/Lana_Del_Rey.md "wikilink"),
創作歌手 <File:Portrait> of John LaFarge.jpg|[John La
Farge](../Page/John_La_Farge.md "wikilink"), 視覺藝術家
<File:GeraldineFerraro.jpg>|[Geraldine
Ferraro](../Page/Geraldine_Ferraro.md "wikilink"),
[美國眾議院成員](../Page/美國眾議院.md "wikilink") <File:Hage>
Geingob.jpg|[Hage Geingob](../Page/Hage_Geingob.md "wikilink"),
[納米比亞總統](../Page/納米比亞總統.md "wikilink") <File:Martin> H.
Glynn.jpg|[Martin H. Glynn](../Page/Martin_H._Glynn.md "wikilink"), 40th
[紐約州州長](../Page/紐約州州長.md "wikilink") <File:Michael> Kay.jpg|[Michael
Kay](../Page/Michael_Kay_\(sports_broadcaster\).md "wikilink"),
[紐約洋基隊體育廣播員](../Page/紐約洋基隊.md "wikilink") <File:Jack>
Keane.jpg|[Jack Keane](../Page/Jack_Keane.md "wikilink"),
[美國陸軍副參謀長](../Page/美國陸軍副參謀長.md "wikilink") <File:Vince>
lombardi bart starr.jpg|[Vince
Lombardi](../Page/Vince_Lombardi.md "wikilink"),
[名人堂](../Page/職業足球名人堂.md "wikilink") <File:Theodore>
Cardinal McCarrick.jpg|[世界經濟論壇成員](../Page/世界經濟論壇.md "wikilink")
<File:Scully> GM.JPG|[Vin Scully](../Page/Vin_Scully.md "wikilink"),
體育播報員 <File:Robert> Gould Shaw.jpg|[Robert Gould
Shaw](../Page/Robert_Gould_Shaw.md "wikilink"),
\[\[美國內戰\]中的\[第54馬薩諸塞步兵團|美國內戰\]中的\[第54馬薩諸塞步兵團\]\]指揮官
<File:Official> Portrait of President Donald Trump (cropped).jpg|[Donald
Trump](../Page/Donald_Trump.md "wikilink"),
[美國總統](../Page/美國總統.md "wikilink")，出席了2年，畢業於沃頓商學院
<File:Denzel> Washington cropped.jpg|[Denzel
Washington](../Page/Denzel_Washington.md "wikilink"),奧斯卡獲獎演員

## 参考资料

[Category:纽约市大学](../Category/纽约市大学.md "wikilink")
[Category:1841年創建的教育機構](../Category/1841年創建的教育機構.md "wikilink")
[\*](../Category/福坦莫大學.md "wikilink")

1.

2.

3.

4.

5.

6.

7.  [周樑勳](http://www.phys.ncku.edu.tw/db/pweb/teacher.php?user_id=100145)，國立成功大學物理學系

8.  [黃永勝](https://research.asia.edu.tw/TchEportfolio/index_1/100102565)，亞洲大學

9.  [林福來](http://www.sdime.ntnu.edu.tw/intro/super_pages.php?ID=intro4&Sn=26&print=friendly)，國立臺灣師範大學數學教育中心

10.

11.

12.

13.

14.

15.

16.

17.
18.

19.

20.

21.

22.

23.

24.
25.

26.

27.

28.

29.

30.

31.

32. [Schools and Colleges](http://www.fordham.edu/academics/index.asp)

33. [Graduate, Undergraduate Business to
    Unify](http://www.fordham.edu/Campus_Resources/eNewsroom/topstories_3190.asp)

34.

35.

36.

37.

38.
39.
40. [Donna Rapaccioli Named Dean of Fordham’s Unified Gabelli School of
    Business](http://news.fordham.edu/business-and-economics/donna-rapaccioli-named-dean-of-fordhams-unified-gabelli-school-of-business/)

41. [Executive MBA
    Ranking 2013](http://rankings.ft.com/businessschoolrankings/emba-ranking-2013)

42.

43. [高瞻远瞩 战略合作 ——记福坦莫大学(Fordham University)
    与北大国际（BiMBA）校友会](http://www.bimba.edu.cn/article.asp?articleid=10982)

44.

45.

46.

47.
48.

49.

50.

51. [聊聊特朗普那些事｜从小“熊”到大，能有今天得感谢他的虎爸](http://news.163.com/16/0720/14/BSE4MQ5400014SEH.html)