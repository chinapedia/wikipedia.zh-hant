**田成平**（），[河北](../Page/河北.md "wikilink")[大名人](../Page/大名.md "wikilink")。1964年4月加入[中國共產黨](../Page/中國共產黨.md "wikilink")，1968年2月參加工作，大學學歷。歷任[青海省省長](../Page/青海省.md "wikilink")、省委書記，[山西省委書記](../Page/山西省.md "wikilink")、[勞動和社會保障部部長](../Page/勞動和社會保障部.md "wikilink")。为原[湖北省副](../Page/湖北省.md "wikilink")[省长](../Page/省长.md "wikilink")[田英之子](../Page/田英.md "wikilink")。

## 生平

1962年，考入[清华大学土木建筑工程系给排水专业](../Page/清华大学土木建筑工程系.md "wikilink")。1968年，在安徽省霍邱城西湖军垦农场锻炼。1970年5月，任[北京石油化工总厂胜利化工厂宣传科干事](../Page/北京石油化工总厂.md "wikilink")。1973年1月，任北京石油化工总厂团委书记。1974年10月，任[北京石油化工总厂前进化工厂党委副书记](../Page/北京石油化工总厂.md "wikilink")。1983年9月，任燕山石油化工总公司党委副书记。1984年10月，任中共北京市西城区委书记\[1\]。

1988年2月，任中共青海省委副书记。1992年11月，任中共青海省委副书记、[青海省人民政府副省长](../Page/青海省人民政府.md "wikilink")、代省长。1993年1月，任中共青海省委副书记、青海省人民政府省长。1997年2月，任中共青海省委书记。1998年1月，升任中共青海省委书记、青海省人大常委会主任\[2\]\[3\]。

1999年6月，任[中共山西省委书记](../Page/中共山西省委.md "wikilink")。2000年1月，任中共山西省委书记、[山西省政协主席](../Page/山西省政协.md "wikilink")。2001年2月，任中共山西省委书记。2003年1月，任中共山西省委书记、[山西省人大常委会主任](../Page/山西省人大常委会.md "wikilink")。2005年6月，任[中华人民共和国劳动和社会保障部部长](../Page/中华人民共和国劳动和社会保障部.md "wikilink")\[4\]。2008年3月至2014年10月，任中央农村工作领导小组副组长\[5\]。

此外担任[中共第十四屆候補中央委員](../Page/中共第十四屆候補中央委員.md "wikilink")，第十五、十六、十七屆中央委員\[6\]。

## 参考文献

{{-}}

[Category:中华人民共和国劳动和社会保障部部长](../Category/中华人民共和国劳动和社会保障部部长.md "wikilink")
[Category:山西省政協主席](../Category/山西省政協主席.md "wikilink")
[Category:山西省人大常委会主任](../Category/山西省人大常委会主任.md "wikilink")
[Category:青海省人大常委会主任](../Category/青海省人大常委会主任.md "wikilink")
[Category:中国共产党第十七届中央委员会委员](../Category/中国共产党第十七届中央委员会委员.md "wikilink")
[Category:中国共产党第十六届中央委员会委员](../Category/中国共产党第十六届中央委员会委员.md "wikilink")
[Category:中国共产党第十五届中央委员会委员](../Category/中国共产党第十五届中央委员会委员.md "wikilink")
[Category:中國共產黨第十四屆中央委員會候補委員](../Category/中國共產黨第十四屆中央委員會候補委員.md "wikilink")
[Category:中共山西省委书记](../Category/中共山西省委书记.md "wikilink")
[Category:中共青海省委书记](../Category/中共青海省委书记.md "wikilink")
[Category:中共青海省委副書記](../Category/中共青海省委副書記.md "wikilink")
[Category:中共西城區委書記](../Category/中共西城區委書記.md "wikilink")
[Category:大名人](../Category/大名人.md "wikilink")
[C](../Category/田姓.md "wikilink")
[Category:清华大学校友](../Category/清华大学校友.md "wikilink")

1.
2.
3.
4.
5.
6.