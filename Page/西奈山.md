**西奈山**（）是[埃及](../Page/埃及.md "wikilink")[西奈半岛南端的一座山](../Page/西奈半岛.md "wikilink")。

## 地理

西乃山海拔高度为2,285米，在西奈半島仅次於海拔2,637米的[凯瑟琳山](../Page/凯瑟琳山.md "wikilink")（Mount
St. Catherine）,\[1\]後者是西奈半岛和埃及的最高峰。周围被山脉高峰环绕。

## 修道院

[圣凯瑟琳修道院位于山脚](../Page/圣凯瑟琳修道院.md "wikilink")，海拔约为1,200米。

## 宗教聖地

在《[希伯來聖經](../Page/希伯來聖經.md "wikilink")》中，西奈山是耶和華的使者從荊棘裡火焰中向摩西顯現之處。領以色列人出埃及後，[摩西在這座山代表](../Page/摩西.md "wikilink")[以色列人領受](../Page/以色列人.md "wikilink")[耶和華的律法](../Page/耶和華.md "wikilink")[十诫](../Page/十诫.md "wikilink")。當摩西在山上時，[亞倫制造了](../Page/亞倫.md "wikilink")[金牛犢给以色列人敬拜](../Page/金牛犢.md "wikilink")。摩西下山看见，摔碎了法版。经过懲罰和认罪后，摩西回到西奈山，要求上帝制造新的石碑，並放在[约柜內](../Page/约柜.md "wikilink")，当中放置很多上帝口述的內容\[2\]

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  - [Wikivoyage: Mount
    Sinai](https://en.wikivoyage.org/wiki/Mount_Sinai)
  - [Photo album from Mount Sinai and St
    Catherine's](https://web.archive.org/web/20051120093409/http://www.ianandwendy.com/OtherTrips/Egypt/StCatherines/slideshow2.htm)
  - [Contains many photos of both claimed sites, and some
    research.](http://www.baseinstitute.org/)

[Category:圣经地名](../Category/圣经地名.md "wikilink")
[Category:埃及地理](../Category/埃及地理.md "wikilink")
[Category:希伯来圣经中的山](../Category/希伯来圣经中的山.md "wikilink")

1.
2.