**Emesene**
是一個用[PyGTK寫的](../Page/PyGTK.md "wikilink")[MSN用戶端](../Page/MSN.md "wikilink")，其目的是要建立一個和官方的
Live Messenger 介面相似的軟體而又維持著清爽的介面。

細心看一下
E<span style="color: red;">m</span>e<span style="color: red;">s</span>e<span style="color: red;">n</span>e
就會發現它是由英文字母 "e" 分隔開 msn。

## 支援功能

[emesene_2.jpg](https://zh.wikipedia.org/wiki/File:emesene_2.jpg "fig:emesene_2.jpg")
[Linux](../Page/Linux.md "wikilink")）\]\]
[emesene_3.jpg](https://zh.wikipedia.org/wiki/File:emesene_3.jpg "fig:emesene_3.jpg")
[Linux](../Page/Linux.md "wikilink")）\]\]
最新版本的emeseme使用MSN的通訊協定MSNP18，並支援大部分MSN功能，包括：

  - 來電振動
  - 表情符號
  - 自訂表情符號
  - 文字格式與顏色
  - 個人頭像
  - 編輯顯示暱稱與個人訊息
  - Tag聊天視窗
  - 外掛模組

## 外部連結

  - [emesene官網](https://web.archive.org/web/20110505143322/http://blog.emesene.org/)
  - [emesene翻譯說明](http://emesene.org/smf/index.php/topic,1012.0.html)
  - [emesene開發計劃首頁](https://launchpad.net/emesene) -
    emesene官方使用[Launchpad提供的開發平台](../Page/Launchpad.md "wikilink")
  - [emesene翻譯列表](https://translations.launchpad.net/emesene) -
    emesene官方使用Launchpad提供的翻譯平台

[Category:SourceForge专案](../Category/SourceForge专案.md "wikilink")
[Category:自由的即时通讯软件](../Category/自由的即时通讯软件.md "wikilink")
[Category:自由軟體](../Category/自由軟體.md "wikilink")
[Category:跨平台軟體](../Category/跨平台軟體.md "wikilink")
[Category:自由跨平台軟體](../Category/自由跨平台軟體.md "wikilink")