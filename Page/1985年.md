## 大事记

### 1月

  - [1月12日](../Page/1月12日.md "wikilink")——[1985年婆羅浮屠爆炸案](../Page/1985年婆羅浮屠爆炸案.md "wikilink")
  - [1月20日](../Page/1月20日.md "wikilink")——[美國總統](../Page/美國總統.md "wikilink")[朗奴·列根宣誓開始第二個總統任期](../Page/朗奴·列根.md "wikilink")。

### 2月

  - [2月8日](../Page/2月8日.md "wikilink")——[台灣爆發](../Page/台灣.md "wikilink")[十信案](../Page/十信案.md "wikilink")，為台灣戰後最嚴重的經濟犯罪事件。
  - [2月20日](../Page/2月20日.md "wikilink")——中国第一个[南极考察站](../Page/南极.md "wikilink")——[长城站在](../Page/长城站.md "wikilink")[南极](../Page/南极.md "wikilink")[乔治岛建成](../Page/乔治岛.md "wikilink")。

### 3月

  - [3月7日](../Page/3月7日.md "wikilink")——香港举行[区议会选举](../Page/1985年香港区议会选举.md "wikilink")，本届选举是[香港区议会的第二届选举](../Page/香港区议会.md "wikilink")

<!-- end list -->

  - [3月11日](../Page/3月11日.md "wikilink") -- 54歲的戈爾巴喬夫繼任蘇聯總書記 ,
    成為蘇聯最高領導階層中最年輕的一員。

### 4月

  - [4月20日](../Page/4月20日.md "wikilink")——[香港](../Page/香港.md "wikilink")[寶馬山雙屍案](../Page/寶馬山雙屍案.md "wikilink")。
  - [4月21日](../Page/4月21日.md "wikilink")——[艾爾頓·冼拿首次贏得](../Page/艾爾頓·冼拿.md "wikilink")[一級方程式分站冠軍](../Page/一級方程式.md "wikilink")。

### 5月

  - [5月19日](../Page/5月19日.md "wikilink")——[香港隊在](../Page/香港足球代表隊.md "wikilink")[北京工人體育場舉行的](../Page/北京工人體育場.md "wikilink")[1986年世界盃外圍賽亞洲區小組賽中以](../Page/1986年世界盃足球賽.md "wikilink")2:1擊敗[中國隊](../Page/中國國家足球隊.md "wikilink")，賽後支持中國隊的球迷不滿賽果而引發騷亂，史稱[五一九事件](../Page/五一九事件.md "wikilink")。

### 6月

  - [6月4日](../Page/6月4日.md "wikilink")——中央軍委主席[鄧小平在](../Page/鄧小平.md "wikilink")[中央軍委擴大會議上宣佈](../Page/中央軍委.md "wikilink")[中國人民解放軍進行](../Page/中國人民解放軍.md "wikilink")[百萬大裁軍](../Page/百萬大裁軍.md "wikilink")。
  - [6月6日](../Page/6月6日.md "wikilink")——[香港政府指](../Page/港英政府.md "wikilink")[海外信託銀行無力償還債務](../Page/海外信託銀行.md "wikilink")，翌日正式接管該行。
  - [6月17日](../Page/6月17日.md "wikilink")——[探索頻道開始播放](../Page/探索頻道.md "wikilink")。\[1\]
  - [6月23日](../Page/6月23日.md "wikilink")——[印度航空182號班機在](../Page/印度航空182號班機空難.md "wikilink")[愛爾蘭附近的](../Page/愛爾蘭.md "wikilink")[大西洋上空被](../Page/大西洋.md "wikilink")[卡利斯坦运动組織放置的炸彈炸毀](../Page/卡利斯坦运动.md "wikilink")，機上329人全部遇難。
  - [6月27日](../Page/6月27日.md "wikilink")——[台灣](../Page/台灣.md "wikilink")[金蘭醬油董事長鍾秋桂夫婦](../Page/金蘭醬油.md "wikilink")，因生食[非洲大蝸牛感染嗜伊紅性](../Page/非洲大蝸牛.md "wikilink")[腦膜炎](../Page/腦膜炎.md "wikilink")，不治身亡。\[2\]

### 7月

  - [7月3日](../Page/7月3日.md "wikilink")——[回到未來於](../Page/回到未來.md "wikilink")[美國各戲院上映](../Page/美國.md "wikilink")，成為1985年[票房最高的](../Page/票房.md "wikilink")[電影及](../Page/電影.md "wikilink")[回到未來系列裏](../Page/回到未來系列.md "wikilink")[票房最高的](../Page/票房.md "wikilink")[電影](../Page/電影.md "wikilink")。
  - [7月5日](../Page/7月5日.md "wikilink")——中国[国务院常务会议批准](../Page/国务院常务会议.md "wikilink")[国家科委](../Page/国家科委.md "wikilink")、原[教育部](../Page/中華人民共和國教育部.md "wikilink")、[中国科学院](../Page/中国科学院.md "wikilink")《关于试办博士后流动站的报告》。中国[博士后制度正式诞生](../Page/博士后.md "wikilink")。

### 8月

  - [8月12日](../Page/8月12日.md "wikilink")——[日本航空123號班機](../Page/日本航空123號班機空難.md "wikilink")，從[東京](../Page/東京.md "wikilink")[羽田飛往](../Page/羽田機場.md "wikilink")[大阪途中](../Page/大阪市.md "wikilink")，因嚴重機械故障墜毀於[群馬縣山區](../Page/群馬縣.md "wikilink")，520人罹難，4人生還。
  - [8月18日](../Page/8月18日.md "wikilink")——[松花江](../Page/松花江.md "wikilink")[哈尔滨段发生](../Page/哈尔滨.md "wikilink")[恶性沉船事故](../Page/八·一八松花江沉船事故.md "wikilink")，造成171人溺水死亡。

### 9月

  - [9月19日](../Page/9月19日.md "wikilink")——在[墨西哥城發生芮氏](../Page/墨西哥城.md "wikilink")7.8地震。
  - [9月22日](../Page/9月22日.md "wikilink")——美英德法日世界國在紐約廣場酒店簽訂《[广场协议](../Page/广场协议.md "wikilink")》。
  - [9月26日](../Page/9月26日.md "wikilink")——[香港立法局首次舉行間接選舉](../Page/香港立法局.md "wikilink")，間選[功能界別議席](../Page/功能界別.md "wikilink")。

### 10月

  - [10月1日](../Page/10月1日.md "wikilink")——[中華人民共和國与](../Page/中華人民共和國.md "wikilink")[格林纳达建立外交关系](../Page/格林纳达.md "wikilink")。但由於之後格林纳达与[中華民國](../Page/中華民國.md "wikilink")（[台灣](../Page/台灣.md "wikilink")）建交，中華人民共和國於[1989年](../Page/1989年.md "wikilink")[8月7日中止與格林纳达的外交关系](../Page/8月7日.md "wikilink")。[2005年中格复交](../Page/2005年.md "wikilink")。
  - [10月30日](../Page/10月30日.md "wikilink")——[香港立法局大樓由](../Page/香港立法會大樓.md "wikilink")[香港總督](../Page/香港總督.md "wikilink")[尤德主持揭幕儀式](../Page/尤德.md "wikilink")，並在該大樓舉行首次會議，新當選的議員宣誓時無須硬性讀出效忠英女王[伊利沙伯二世的誓詞](../Page/伊利沙伯二世.md "wikilink")。
  - 10月30日——[美國](../Page/美國.md "wikilink")[亞特蘭提斯號太空梭首次發射升空執行任務](../Page/亞特蘭提斯號太空梭.md "wikilink")。

### 11月

  - [11月2日](../Page/11月2日.md "wikilink")——[日本職棒](../Page/日本職棒.md "wikilink")[阪神虎隊奪得成軍](../Page/阪神虎隊.md "wikilink")51年以來首次日本大賽優勝。

### 12月

  - [12月18日](../Page/12月18日.md "wikilink")——[香港](../Page/香港.md "wikilink")[基本法諮詢委員會成立](../Page/基本法諮詢委員會.md "wikilink")，由中國[國務院港澳辦公室主任](../Page/國務院港澳辦公室.md "wikilink")[姬鵬飛主持](../Page/姬鵬飛.md "wikilink")，共有180名委員，包括8名[立法局議員](../Page/立法局.md "wikilink")、3名香港政府高官及5名[市政局議員](../Page/市政局.md "wikilink")。

## 出生

### 1月

  - [1月1日](../Page/1月1日.md "wikilink")——[蒂亚戈·斯普利特](../Page/蒂亚戈·斯普利特.md "wikilink")，巴西著名篮球运动员。
  - [1月4日](../Page/1月4日.md "wikilink")——[艾尔·杰弗森](../Page/艾尔·杰弗森.md "wikilink")，美国著名篮球运动员，夏洛特山猫队史首位月最佳球员。
  - [1月6日](../Page/1月6日.md "wikilink")——[傅嘉莉](../Page/傅嘉莉.md "wikilink")，[香港演員](../Page/香港.md "wikilink")、模特兒。
  - [1月6日](../Page/1月6日.md "wikilink")——[何啟明](../Page/何啟明_\(工聯會\).md "wikilink")，[香港](../Page/香港.md "wikilink")[勞工界立法會議員](../Page/勞工界功能界別.md "wikilink")。
  - [1月10日](../Page/1月10日.md "wikilink")——[柯佳嬿](../Page/柯佳嬿.md "wikilink")，台灣女演員。
  - [1月14日](../Page/1月14日.md "wikilink")——[阿隆·布鲁克斯](../Page/阿隆·布鲁克斯.md "wikilink")，美国著名篮球运动员，2010年进步最快球员。
  - [1月17日](../Page/1月17日.md "wikilink")——[金永雲](../Page/強仁.md "wikilink")，[韩国歌手](../Page/韩国.md "wikilink")，[韓國男子團體](../Page/韓國.md "wikilink")[Super
    Junior成员](../Page/Super_Junior.md "wikilink")**強仁**
  - [1月19日](../Page/1月19日.md "wikilink")——[石川梨華](../Page/石川梨華.md "wikilink")，[日本歌手](../Page/日本.md "wikilink")。
  - [1月23日](../Page/1月23日.md "wikilink")——[董芳卓](../Page/董芳卓.md "wikilink")，[中国足球运动员](../Page/中国.md "wikilink")。
  - [1月29日](../Page/1月29日.md "wikilink")——[马克·加索尔](../Page/马克·加索尔.md "wikilink")，西班牙著名篮球运动员，2013年最佳防守球员，西班牙球星-保罗·加索尔的弟弟。
  - [1月31日](../Page/1月31日.md "wikilink")——[藍又時](../Page/藍又時.md "wikilink")，[台灣歌手](../Page/台灣歌手.md "wikilink")。

### 2月

  - [2月2日](../Page/2月2日.md "wikilink")——[桐山漣](../Page/桐山漣.md "wikilink")，[日本演员](../Page/日本.md "wikilink")。
  - [2月5日](../Page/2月5日.md "wikilink")——[基斯坦奴·朗拿度](../Page/基斯坦奴·朗拿度.md "wikilink")，[葡萄牙足球員](../Page/葡萄牙.md "wikilink")。
  - [2月7日](../Page/2月7日.md "wikilink")——[吳岱豪](../Page/吳岱豪.md "wikilink")，台灣[籃球運動員](../Page/籃球.md "wikilink")。
  - [2月8日](../Page/2月8日.md "wikilink")——[王宇佐](../Page/王宇佐.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[網球好手](../Page/網球.md "wikilink")。
  - [2月10日](../Page/2月10日.md "wikilink")——[保罗·米尔萨普](../Page/保罗·米尔萨普.md "wikilink")，美国著名篮球明星。
  - [2月14日](../Page/2月14日.md "wikilink")——[楊愛瑾](../Page/楊愛瑾.md "wikilink")，[香港](../Page/香港.md "wikilink")[歌手](../Page/歌手.md "wikilink")。
  - [2月26日](../Page/2月26日.md "wikilink")——[藤本美貴](../Page/藤本美貴.md "wikilink")，[日本歌手](../Page/日本.md "wikilink")。
  - [2月27日](../Page/2月27日.md "wikilink")——[李賢璞](../Page/李賢璞.md "wikilink")，台灣搖滾樂團[八三夭主唱](../Page/八三夭.md "wikilink")、UP品牌主理人。
  - [2月28日](../Page/2月28日.md "wikilink")——[莊鵑瑛](../Page/莊鵑瑛.md "wikilink")，[台灣歌手](../Page/台灣.md "wikilink")。

### 3月

  - [3月3日](../Page/3月3日.md "wikilink")——[李柏誼](../Page/李柏誼.md "wikilink")，[台灣樂團](../Page/台灣.md "wikilink")[MP魔幻力量吉他手](../Page/MP魔幻力量.md "wikilink")。
  - [3月20日](../Page/3月20日.md "wikilink")——[Mandy
    Lieu](../Page/Mandy_Lieu.md "wikilink")，[香港](../Page/香港.md "wikilink")[模特兒](../Page/模特兒.md "wikilink")。
  - [3月22日](../Page/3月22日.md "wikilink")——[王麗坤](../Page/王麗坤.md "wikilink")，[中國演員](../Page/中國.md "wikilink")。
  - [3月24日](../Page/3月24日.md "wikilink")——[綾瀨遙](../Page/綾瀨遙.md "wikilink")，[日本演員](../Page/日本.md "wikilink")。
  - [3月24日](../Page/3月24日.md "wikilink")——[解婕翎](../Page/解婕翎.md "wikilink")，[台灣女藝人](../Page/台灣.md "wikilink")
  - [3月26日](../Page/3月26日.md "wikilink")——，[韓國男子搖滾樂團](../Page/韓國.md "wikilink")[TRAX吉他手](../Page/TRAX.md "wikilink")**政模**
  - [3月29日](../Page/3月29日.md "wikilink")——[翟威廉](../Page/翟威廉.md "wikilink")，[香港藝人](../Page/香港.md "wikilink")
  - [3月29日](../Page/3月29日.md "wikilink")——[陳康健](../Page/陳康健.md "wikilink")，[香港藝人](../Page/香港.md "wikilink")
  - [3月29日](../Page/3月29日.md "wikilink")——[克里斯蒂安·福斯特](../Page/克里斯蒂安·福斯特.md "wikilink")，[德国女子](../Page/德国.md "wikilink")[排球運動員](../Page/排球.md "wikilink")。

### 4月

  - [4月1日](../Page/4月1日.md "wikilink")——[古斯塔沃·阿永](../Page/古斯塔沃·阿永.md "wikilink")，[墨西哥篮球运动员](../Page/墨西哥.md "wikilink")。
  - [4月9日](../Page/4月9日.md "wikilink")——[山下智久](../Page/山下智久.md "wikilink")，[日本藝人](../Page/日本.md "wikilink")。
  - [4月10日](../Page/4月10日.md "wikilink")——[許銘倢](../Page/許銘倢.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[棒球選手](../Page/棒球.md "wikilink")。
  - [4月12日](../Page/4月12日.md "wikilink")——[吉澤瞳](../Page/吉澤瞳.md "wikilink")，[日本歌手](../Page/日本.md "wikilink")。
  - [4月16日](../Page/4月16日.md "wikilink")——[罗尔·邓](../Page/罗尔·邓.md "wikilink")，[英国籍苏丹著名篮球明星](../Page/英国.md "wikilink")。
  - [4月16日](../Page/4月16日.md "wikilink")——[陳樂基](../Page/陳樂基.md "wikilink")，[香港](../Page/香港.md "wikilink")[創作男歌手](../Page/創作.md "wikilink")。
  - [4月22日](../Page/4月22日.md "wikilink")--[孙骁骁](../Page/孙骁骁.md "wikilink")，[中国大陆电视剧演员](../Page/中国大陆.md "wikilink"),
    主持人。
  - [4月26日](../Page/4月26日.md "wikilink")——[南奎里](../Page/南奎里.md "wikilink")，韓國女歌手、演員

### 5月

  - [5月2日](../Page/5月2日.md "wikilink")——[莉莉·艾倫](../Page/莉莉·艾倫.md "wikilink")，[英格蘭女歌手](../Page/英格蘭.md "wikilink")、主持人。
  - [5月5日](../Page/5月5日.md "wikilink")——[中川翔子](../Page/中川翔子.md "wikilink")，日本女藝人
  - [5月7日](../Page/5月7日.md "wikilink")——[J·巴拉文](../Page/J·巴拉文.md "wikilink")(J.
    Balvin)，[哥倫比亞歌手](../Page/哥倫比亞.md "wikilink")。
  - [5月8日](../Page/5月8日.md "wikilink")——[劉文杰](../Page/劉文杰_\(歌手\).md "wikilink")，[中國男歌手](../Page/中國.md "wikilink")，[信樂團主唱](../Page/信樂團.md "wikilink")
  - [5月16日](../Page/5月16日.md "wikilink")——[大倉忠義](../Page/大倉忠義.md "wikilink")，[日本演員](../Page/日本.md "wikilink")、歌手，[關西傑尼斯8](../Page/關西傑尼斯8.md "wikilink")（関ジャニ∞）成員。
  - [5月19日](../Page/5月19日.md "wikilink")——[梁子駿](../Page/梁子駿.md "wikilink")，[香港足球運動員](../Page/香港.md "wikilink")。
  - [5月22日](../Page/5月22日.md "wikilink")——[鄧穎芝](../Page/鄧穎芝.md "wikilink")，[香港](../Page/香港.md "wikilink")[歌手](../Page/歌手.md "wikilink")。
  - [5月22日](../Page/5月22日.md "wikilink")——[周秀娜](../Page/周秀娜.md "wikilink")，[香港](../Page/香港.md "wikilink")[模特兒](../Page/模特兒.md "wikilink")。
  - [5月27日](../Page/5月27日.md "wikilink")——[姜建銘](../Page/姜建銘.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[旅日棒球選手](../Page/旅日棒球選手.md "wikilink")。
  - [5月29日](../Page/5月29日.md "wikilink")——[徐卓](../Page/徐卓.md "wikilink")，[中国](../Page/中国.md "wikilink")[摄影师](../Page/摄影师.md "wikilink")。

### 6月

  - [6月4日](../Page/6月4日.md "wikilink")——[盧卡斯·波多爾斯基](../Page/盧卡斯·波多爾斯基.md "wikilink")，[德國足球選手](../Page/德國.md "wikilink")。
  - [6月12日](../Page/6月12日.md "wikilink")——[布雷克·羅斯](../Page/布雷克·羅斯.md "wikilink")，软件開發者。
  - [6月12日](../Page/6月12日.md "wikilink")——[林奕匡](../Page/林奕匡.md "wikilink")，香港著名男歌手。
  - [6月19日](../Page/6月19日.md "wikilink")——[噶瑪巴·伍金赤列多吉](../Page/噶瑪巴·伍金赤列多吉.md "wikilink")，第十七世[大寶法王](../Page/大寶法王.md "wikilink")。
  - [6月20日](../Page/6月20日.md "wikilink")——[陶嫚曼](../Page/陶嫚曼.md "wikilink")，台灣女演員，歌手。
  - [6月20日](../Page/6月20日.md "wikilink")——[鄭錡鴻](../Page/鄭錡鴻.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[旅美棒球選手](../Page/旅美棒球選手.md "wikilink")。
  - [6月25日](../Page/6月25日.md "wikilink")——[辛猛](../Page/辛猛.md "wikilink")，[中国](../Page/中国.md "wikilink")
    [维京（中国）公司](../Page/维京（中国）公司.md "wikilink") 创办人。
  - [6月29日](../Page/6月29日.md "wikilink")——[西野翔](../Page/西野翔.md "wikilink")，[日本](../Page/日本.md "wikilink")[AV女優](../Page/AV女優.md "wikilink")。
  - [6月30日](../Page/6月30日.md "wikilink")——[春田菜菜](../Page/春田菜菜.md "wikilink")，日本漫畫家。
  - 6月30日——[美國](../Page/美國.md "wikilink")，[麥可·菲爾普斯](../Page/麥可·菲爾普斯.md "wikilink")，[美國游泳選手](../Page/美國游泳選手.md "wikilink")。

### 7月

  - [7月2日](../Page/7月2日.md "wikilink")——[艾希莉·提斯代爾](../Page/艾希莉·提斯代爾.md "wikilink")，美國演員。
  - [7月5日](../Page/7月5日.md "wikilink")——[楊奇煜](../Page/楊奇煜.md "wikilink")，[台灣歌手](../Page/台灣.md "wikilink")、演員，男子組合[Lollipop棒棒堂成員](../Page/Lollipop棒棒堂.md "wikilink")**小煜**。
  - [7月11日](../Page/7月11日.md "wikilink")——[前田亞季](../Page/前田亞季.md "wikilink")，日本女演員。
  - [7月14日](../Page/7月14日.md "wikilink")——[伊澤千夏](../Page/伊澤千夏.md "wikilink")，[日本](../Page/日本.md "wikilink")[AV女優](../Page/AV女優.md "wikilink")。
  - [7月14日](../Page/7月14日.md "wikilink")——[陳肇麒](../Page/陳肇麒.md "wikilink")，[香港足球運動員](../Page/香港.md "wikilink")。
  - [7月14日](../Page/7月14日.md "wikilink")——[李光洙](../Page/李光洙_\(演員\).md "wikilink")，[南韓演員](../Page/南韓.md "wikilink")、主持人。
  - [7月15日](../Page/7月15日.md "wikilink")——[盧廣仲](../Page/盧廣仲.md "wikilink")，[歌手](../Page/歌手.md "wikilink")。
  - [7月16日](../Page/7月16日.md "wikilink")——[日笠陽子](../Page/日笠陽子.md "wikilink")，[日本](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")。
  - [7月18日](../Page/7月18日.md "wikilink")——[鄧有宗](../Page/鄧有宗.md "wikilink")，台灣搖滾樂團[八三夭貝斯手](../Page/八三夭.md "wikilink")。
  - [7月19日](../Page/7月19日.md "wikilink")——[拉马库斯·阿尔德里奇](../Page/拉马库斯·阿尔德里奇.md "wikilink")，美国著名篮球明星。
  - [7月21日](../Page/7月21日.md "wikilink")——[陳偉殷](../Page/陳偉殷.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[旅日棒球選手](../Page/旅日棒球選手.md "wikilink")。
  - [7月21日](../Page/7月21日.md "wikilink")——[張紋嘉](../Page/張紋嘉.md "wikilink")，[香港歌手及演員](../Page/香港.md "wikilink")。
  - [7月26日](../Page/7月26日.md "wikilink")——[中土居宏宜](../Page/中土居宏宜.md "wikilink")，日本男歌手（[lead團員](../Page/lead.md "wikilink")）。
  - [7月26日](../Page/7月26日.md "wikilink")——[加藤夏希](../Page/加藤夏希.md "wikilink")，日本女演員。
  - [7月26日](../Page/7月26日.md "wikilink")——[周笔畅](../Page/周笔畅.md "wikilink")，中国歌手。
  - [7月27日](../Page/7月27日.md "wikilink")——[許孟哲](../Page/許孟哲.md "wikilink")，[台灣主持](../Page/台灣.md "wikilink")、歌手、演員。

### 8月

  - [8月8日](../Page/8月8日.md "wikilink")——[林宗彥](../Page/林宗彥.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[兒童節目](../Page/兒童節目.md "wikilink")[MOMO歡樂谷主持人](../Page/MOMO歡樂谷.md "wikilink")。
  - [8月9日](../Page/8月9日.md "wikilink")——[洪真英](../Page/洪真英.md "wikilink")，[韓國女歌手](../Page/韓國.md "wikilink")、演員。
  - [8月9日](../Page/8月9日.md "wikilink")——[宋熙年](../Page/宋熙年.md "wikilink")，[香港主持人](../Page/香港.md "wikilink")。
  - [8月16日](../Page/8月16日.md "wikilink")——[MeiMei](../Page/郭婕祈.md "wikilink")，[台灣歌手](../Page/台灣.md "wikilink")、藝人。
  - [8月17日](../Page/8月17日.md "wikilink")——[蒼井優](../Page/蒼井優.md "wikilink")，日本女演員。
  - [8月19日](../Page/8月19日.md "wikilink")——[陳蔚而](../Page/陳蔚而.md "wikilink")，[香港演員](../Page/香港.md "wikilink")。
  - [8月30日](../Page/8月30日.md "wikilink")——[許仁杰](../Page/許仁杰.md "wikilink")，臺灣歌手演員。

### 9月

  - [9月3日](../Page/9月3日.md "wikilink")——[梶裕貴](../Page/梶裕貴.md "wikilink")，日本男[聲優](../Page/聲優.md "wikilink")。
  - [9月9日](../Page/9月9日.md "wikilink")——[J.R.史密斯](../Page/J.R.史密斯.md "wikilink")，[美国著名籃球運動員](../Page/美国.md "wikilink")。
  - [9月9日](../Page/9月9日.md "wikilink")——[卢卡·莫德里奇](../Page/卢卡·莫德里奇.md "wikilink")，[克罗地亚足球員](../Page/克罗地亚.md "wikilink")。
  - [9月10日](../Page/9月10日.md "wikilink")——[王子寧](../Page/王子寧.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[主持人](../Page/主持人.md "wikilink")，藝名**王子若**。
  - [9月10日](../Page/9月10日.md "wikilink")——[松田翔太](../Page/松田翔太.md "wikilink")，日本男演員。
  - [9月14日](../Page/9月14日.md "wikilink")——[上戶彩](../Page/上戶彩.md "wikilink")，日本女歌手、演員。
  - [9月18日](../Page/9月18日.md "wikilink")——[张健忠](../Page/张健忠.md "wikilink")，[中國足球運動員](../Page/中國.md "wikilink")。
  - [9月19日](../Page/9月19日.md "wikilink")——[宋仲基](../Page/宋仲基.md "wikilink")，[韓國男演員](../Page/韓國.md "wikilink")。
  - [9月19日](../Page/9月19日.md "wikilink")——[鄧芷茵](../Page/鄧芷茵.md "wikilink")，[香港歌手](../Page/香港歌手.md "wikilink")。
  - [9月23日](../Page/9月23日.md "wikilink")——[後藤真希](../Page/後藤真希.md "wikilink")，日本女歌手。
  - [9月26日](../Page/9月26日.md "wikilink")——[高國輝](../Page/高國輝.md "wikilink")，曾效力於[美國職棒](../Page/美國職棒.md "wikilink")[西雅圖水手隊](../Page/西雅圖水手.md "wikilink")[小聯盟體系](../Page/小聯盟.md "wikilink")，現為[富邦悍將](../Page/富邦悍將.md "wikilink")[外野手](../Page/外野手.md "wikilink")，於2002年至2003年間將姓氏改為[羅](../Page/羅.md "wikilink")，2012年10月改回父姓。
  - [9月28日](../Page/9月28日.md "wikilink")——[申東熙](../Page/神童_\(藝人\).md "wikilink")，[韩国歌手](../Page/韩国.md "wikilink")，[韓國男子團體](../Page/韓國.md "wikilink")[Super
    Junior成员](../Page/Super_Junior.md "wikilink")**神童**。

### 10月

  - [10月7日](../Page/10月7日.md "wikilink")——[廖亦崟](../Page/廖亦崟.md "wikilink")，[台灣主持](../Page/台灣.md "wikilink")、歌手、演員，男子組合[Lollipop棒棒堂成員](../Page/Lollipop棒棒堂.md "wikilink")**威廉**。
  - [10月8日](../Page/10月8日.md "wikilink")——[瑛士](../Page/瑛士.md "wikilink")，[日本](../Page/日本.md "wikilink")[歌手](../Page/歌手.md "wikilink")、[演員](../Page/演員.md "wikilink")。
  - [10月8日](../Page/10月8日.md "wikilink")——[布魯諾·馬爾斯](../Page/布魯諾·馬爾斯.md "wikilink")，[美國](../Page/美國.md "wikilink")、[創作型歌手](../Page/創作型歌手.md "wikilink")。
  - [10月12日](../Page/10月12日.md "wikilink")——[戴小奕](../Page/戴小奕.md "wikilink")，[香港及](../Page/香港.md "wikilink")[中國](../Page/中國.md "wikilink")[模特兒](../Page/模特兒.md "wikilink")。
  - [10月15日](../Page/10月15日.md "wikilink")——[劉峻緯](../Page/劉峻緯.md "wikilink")，[台灣主持](../Page/台灣.md "wikilink")、歌手、演員，男子組合[Lollipop棒棒堂成員](../Page/Lollipop棒棒堂.md "wikilink")**阿緯**，原名**劉俊緯**。
  - [10月24日](../Page/10月24日.md "wikilink")——[朗尼](../Page/朗尼.md "wikilink")，[英國足球員](../Page/英國.md "wikilink")。

### 11月

  - [11月2日](../Page/11月2日.md "wikilink")——[潘杰寧](../Page/潘杰寧.md "wikilink")，[香港主持人](../Page/香港.md "wikilink")、演員。
  - [11月5日](../Page/11月5日.md "wikilink")——[JR](../Page/JR_\(藝人\).md "wikilink")（藝人），[台灣主持](../Page/台灣.md "wikilink")、歌手、演員。
  - [11月5日](../Page/11月5日.md "wikilink")——[林逸欣](../Page/林逸欣.md "wikilink")，[台灣主持](../Page/台灣.md "wikilink")、歌手、演員。
  - [11月5日](../Page/11月5日.md "wikilink")——[可嵐](../Page/可嵐.md "wikilink")，[香港歌手](../Page/香港.md "wikilink")。
  - [11月5日](../Page/11月5日.md "wikilink")——[田中聖](../Page/田中聖.md "wikilink")，日本傑尼斯KAT-TUN成員之一。
  - [11月9日](../Page/11月9日.md "wikilink")----[陈赫](../Page/陈赫.md "wikilink"),
    [中国大陆人](../Page/中国大陆.md "wikilink")。
  - [11月11日](../Page/11月11日.md "wikilink")——[林益全](../Page/林益全.md "wikilink")，[臺灣](../Page/臺灣.md "wikilink")[棒球選手](../Page/棒球.md "wikilink")，現效力於[中華職棒](../Page/中華職棒.md "wikilink")[義大犀牛](../Page/義大犀牛.md "wikilink")。
  - [11月20日](../Page/11月20日.md "wikilink")——[炎亞綸](../Page/炎亞綸.md "wikilink")，[台灣歌手](../Page/台灣.md "wikilink")、演員。
  - [11月21日](../Page/11月21日.md "wikilink")——[陳偉霆](../Page/陳偉霆.md "wikilink")，[香港歌手](../Page/香港.md "wikilink")。
  - [11月21日](../Page/11月21日.md "wikilink")——[卡莉·蕾·傑普森](../Page/卡莉·蕾·傑普森.md "wikilink")，[加拿大歌手](../Page/加拿大.md "wikilink")。
  - [11月25日](../Page/11月25日.md "wikilink")——[邵庭](../Page/邵庭.md "wikilink")，台灣女藝人。
  - [11月29日](../Page/11月29日.md "wikilink")——[田口淳之介](../Page/田口淳之介.md "wikilink")，日本傑尼斯KAT-TUN成員之一。
  - [11月30日](../Page/11月30日.md "wikilink")——[宮崎葵](../Page/宮崎葵.md "wikilink")，日本女演員。
  - [11月30日](../Page/11月30日.md "wikilink")——[滿島光](../Page/滿島光.md "wikilink")，日本女演員。

### 12月

  - [12月5日](../Page/12月5日.md "wikilink")——[窪田啓子](../Page/窪田啓子.md "wikilink")，日本女性歌手。
  - [12月12日](../Page/12月12日.md "wikilink")——[貫地谷栞](../Page/貫地谷栞.md "wikilink")，日本女演員。
  - [12月14日](../Page/12月14日.md "wikilink")——[劉湘怡](../Page/劉湘怡.md "wikilink")，[伊林娛樂女](../Page/伊林娛樂.md "wikilink")[模特兒](../Page/模特兒.md "wikilink")。
  - [12月16日](../Page/12月16日.md "wikilink")——[橘慶太](../Page/橘慶太.md "wikilink")，日本歌手。
  - [12月17日](../Page/12月17日.md "wikilink")——[緒方龍一](../Page/緒方龍一.md "wikilink")，日本歌手。
  - [12月24日](../Page/12月24日.md "wikilink")——[蔡芷紜](../Page/蔡芷紜.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[演員](../Page/演員.md "wikilink")。
  - [12月26日](../Page/12月26日.md "wikilink")——[城田優](../Page/城田優.md "wikilink")，日本藝人。
  - [12月31日](../Page/12月31日.md "wikilink")——[菁瑋](../Page/菁瑋.md "wikilink")，[香港女演員](../Page/香港.md "wikilink")。

## 逝世

  - [徐柳仙](../Page/徐柳仙.md "wikilink")，著名[粵劇演員](../Page/粵劇.md "wikilink")。
  - [1月8日](../Page/1月8日.md "wikilink")——[王世中](../Page/王世中.md "wikilink")(S.C.
    Wang)，中華民國[中央研究院第三屆](../Page/中央研究院.md "wikilink")(生命科學組)院士（生於[1913年](../Page/1913年.md "wikilink"))
  - [3月10日](../Page/3月10日.md "wikilink")，[康斯坦丁·乌斯季诺维奇·契尔年科](../Page/康斯坦丁·乌斯季诺维奇·契尔年科.md "wikilink")，73歲，蘇聯後期總書記，主席。
  - [4月3日](../Page/4月3日.md "wikilink")——[伍獻文](../Page/伍獻文.md "wikilink")(Hsien-wen
    Wu)，86歲，中華民國[中央研究院第一屆](../Page/中央研究院.md "wikilink")(生命科學組)院士（生於[1900年](../Page/1900年.md "wikilink"))
  - [5月14日](../Page/5月14日.md "wikilink")——[翁美玲](../Page/翁美玲.md "wikilink")，香港[無綫電視女演員](../Page/無綫電視.md "wikilink")。
  - [7月4日](../Page/7月4日.md "wikilink")——[許不了](../Page/許不了.md "wikilink")，[台灣喜劇演員](../Page/台灣.md "wikilink")。
  - [7月16日](../Page/7月16日.md "wikilink")——[海因里希·伯爾](../Page/海因里希·伯爾.md "wikilink")，[德国](../Page/德国.md "wikilink")[作家](../Page/作家.md "wikilink")，[1972年](../Page/1972年.md "wikilink")[诺贝尔文学奖得獎者](../Page/诺贝尔文学奖.md "wikilink")
  - [7月24日](../Page/7月24日.md "wikilink")——[堯茂書](../Page/堯茂書.md "wikilink")，[中國第一位漂流](../Page/中國.md "wikilink")[長江的人](../Page/長江.md "wikilink")。
  - [8月12日](../Page/8月12日.md "wikilink")——[坂本九](../Page/坂本九.md "wikilink")，[日本演員](../Page/日本.md "wikilink")、歌手。
  - [8月17日](../Page/8月17日.md "wikilink")——[尼古拉·艾登](../Page/尼古拉·艾登.md "wikilink")，[英國](../Page/英國.md "wikilink")[政治家](../Page/政治家.md "wikilink")。
  - [9月6日](../Page/9月6日.md "wikilink")——[史良](../Page/史良.md "wikilink")，中國女[律師](../Page/律師.md "wikilink")，[中國民主同盟領導人](../Page/中國民主同盟.md "wikilink")。
  - [9月19日](../Page/9月19日.md "wikilink")——[伊塔羅·卡爾維諾](../Page/伊塔羅·卡爾維諾.md "wikilink")，[義大利](../Page/義大利.md "wikilink")[作家](../Page/作家.md "wikilink")。
  - [9月21日](../Page/9月21日.md "wikilink")——[古龙](../Page/古龙.md "wikilink")，[台湾](../Page/台湾.md "wikilink")[武侠小说家](../Page/武侠.md "wikilink")。
  - [10月1日](../Page/10月1日.md "wikilink")——[埃尔文·布鲁克斯·怀特](../Page/埃尔文·布鲁克斯·怀特.md "wikilink")，[美国作家](../Page/美国.md "wikilink")。
  - [10月22日](../Page/10月22日.md "wikilink")——[许世友](../Page/许世友.md "wikilink")，[中國人民解放軍將令](../Page/中國人民解放軍.md "wikilink")。
  - [11月14日](../Page/11月14日.md "wikilink")——[顧維鈞](../Page/顧維鈞.md "wikilink")，中國近代外交家與法學家。

## [诺贝尔奖](../Page/诺贝尔奖.md "wikilink")

  - [物理](../Page/诺贝尔物理学奖.md "wikilink")：[克勞斯·馮·克利青](../Page/克勞斯·馮·克利青.md "wikilink")，現量子霍爾效應
  - [化学](../Page/诺贝尔化学奖.md "wikilink")：
    [赫伯特·豪普特曼](../Page/赫伯特·豪普特曼.md "wikilink")（Herbert
    Aaron Hauptman）、[傑爾姆·卡爾](../Page/傑爾姆·卡爾.md "wikilink")，(Jerome
    Karle)，发展测定分子和晶体结构的方法
  - [生理和医学](../Page/诺贝尔医学奖.md "wikilink")：[约瑟夫·古德斯坦](../Page/约瑟夫·古德斯坦.md "wikilink")
    (Joseph L.Goldstein)
    、[麥可·布朗](../Page/麥可·斯圖亞特·布朗.md "wikilink")（Michael
    S. Brown），关于[胆固醇代谢调控的研究](../Page/胆固醇.md "wikilink")
  - [文学](../Page/诺贝尔文学奖.md "wikilink")：[克洛德·西蒙](../Page/克洛德·西蒙.md "wikilink")（Claude
    Simon），由于他善于把诗人和画家的丰富想象与深刻的时间意识融为一体，对人类的生存状况进行了深入的描写
  - [和平](../Page/诺贝尔和平奖.md "wikilink")：International Physicians for the
    Prevention of Nuclear
    War（[国际医师预防核战组织](../Page/国际医师预防核战组织.md "wikilink")）
  - [经济](../Page/诺贝尔经济学奖.md "wikilink")：[弗兰科·莫迪利安尼](../Page/弗兰科·莫迪利安尼.md "wikilink")
    (Franco Modigliani)，第一个提出储蓄的生命周期假设。这一假设在研究家庭和企业储蓄中得到了广泛应用。

## [奥斯卡金像奖](../Page/奥斯卡金像奖.md "wikilink")

（第58届，[1986年颁发](../Page/1986年.md "wikilink")）

  - [奥斯卡最佳影片奖](../Page/奥斯卡最佳影片奖.md "wikilink")——《[走出非洲](../Page/走出非洲.md "wikilink")》（Out
    of Africa）
  - [奥斯卡最佳导演奖](../Page/奥斯卡最佳导演奖.md "wikilink")——[西德尼·波拉克](../Page/西德尼·波拉克.md "wikilink")（Sydney
    Pollack）《走出非洲》
  - [奥斯卡最佳男主角奖](../Page/奥斯卡最佳男主角奖.md "wikilink")——[威廉·赫特](../Page/威廉·赫特.md "wikilink")（William
    Hurt）《[蜘蛛女之吻](../Page/蜘蛛女之吻.md "wikilink")》
  - [奥斯卡最佳女主角奖](../Page/奥斯卡最佳女主角奖.md "wikilink")——[吉拉汀·佩姬](../Page/吉拉汀·佩姬.md "wikilink")（Geraldine
    Page）《[邦蒂富尔之行](../Page/邦蒂富尔之行.md "wikilink")》
  - [奥斯卡最佳男配角奖](../Page/奥斯卡最佳男配角奖.md "wikilink")——[唐·阿米奇](../Page/唐·阿米奇.md "wikilink")（Don
    Ameche）《[蚕茧](../Page/蚕茧.md "wikilink")》
  - [奥斯卡最佳女配角奖](../Page/奥斯卡最佳女配角奖.md "wikilink")——[安吉丽卡·休斯顿](../Page/安吉丽卡·休斯顿.md "wikilink")（Anjelica
    Huston）《[普里齐的荣誉](../Page/普里齐的荣誉.md "wikilink")》

（其他奖项参见[奥斯卡金像奖获奖名单](../Page/奥斯卡金像奖获奖名单.md "wikilink")）

## 參見

  - [1985年台灣之最](../Page/1985年台灣之最.md "wikilink")

## 參考文獻

[\*](../Category/1985年.md "wikilink")
[5年](../Category/1980年代.md "wikilink")
[8](../Category/20世纪各年.md "wikilink")

1.  "[The 59th Academy Awards (1987) Nominees and
    Winners"](http://www.oscars.org/awards/academyawards/legacy/ceremony/59th-winners.html).
    oscars.org.Retrieved 2012-10-06.
2.