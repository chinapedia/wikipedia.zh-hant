[Kowloon_Hotel_lobby_2018.jpg](https://zh.wikipedia.org/wiki/File:Kowloon_Hotel_lobby_2018.jpg "fig:Kowloon_Hotel_lobby_2018.jpg")
**九龍酒店**（**The Kowloon
Hotel**），是[香港一間](../Page/香港.md "wikilink")[乙級高價酒店](../Page/乙級高價酒店.md "wikilink")，位於[九龍](../Page/九龍.md "wikilink")[尖沙咀](../Page/尖沙咀.md "wikilink")[彌敦道與](../Page/彌敦道.md "wikilink")[漢口道交界](../Page/漢口道.md "wikilink")，於1985年落成\[1\]，1986年2月16日開幕\[2\]，原址為[美輪大酒店](../Page/美輪大酒店.md "wikilink")，酒店地庫直通[港鐵](../Page/港鐵.md "wikilink")[尖沙咀站](../Page/尖沙咀站.md "wikilink")／[尖東站L](../Page/尖東站.md "wikilink")4出口。九龍酒店是[海逸國際酒店集團的成員之一](../Page/海逸國際酒店集團.md "wikilink")，位於尖沙咀海傍，部分房間可眺望醉人維港景緻。

九龍酒店共設有736間客房，包括分為標準及高級（低層），以及豪華及海景（高層）客房。酒店大堂設有咖啡廳，二樓設有中菜廳、西餐廳及酒吧。其中中菜廳部分可劃為舉辦宴會之用。

## 歷史

九龍酒店原為[香港上海大酒店擁有](../Page/香港上海大酒店.md "wikilink")\[3\]，於2004年12月24日以19.3億元出售予長江實業及和記黃埔各持60%的合資公司，並成為[海逸國際酒店集團的成員酒店](../Page/海逸國際酒店集團.md "wikilink")，原本的九龍商標則被[海逸國際酒店集團的商標所取代](../Page/海逸國際酒店集團.md "wikilink")。\[4\]

## 鄰近

  - [半島酒店](../Page/半島酒店.md "wikilink")
  - [香港喜來登酒店](../Page/香港喜來登酒店.md "wikilink")
  - [重慶大廈](../Page/重慶大廈.md "wikilink")
  - [香港太空館](../Page/香港太空館.md "wikilink")
  - [香港文化中心](../Page/香港文化中心.md "wikilink")

## 途經之公共交通服務

<table>
<thead>
<tr class="header">
<th><p>交通路線列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><dl>
<dt><a href="../Page/港鐵.md" title="wikilink">港鐵</a></dt>

</dl>
<ul>
<li><font color="{{荃灣綫色彩}}">█</font><a href="../Page/荃灣綫.md" title="wikilink">荃灣綫</a>：<a href="../Page/尖沙咀站.md" title="wikilink">尖沙咀站</a></li>
<li><font color="{{西鐵綫色彩}}">█</font><a href="../Page/西鐵綫.md" title="wikilink">西鐵綫</a>：<a href="../Page/尖東站.md" title="wikilink">尖東站</a></li>
</ul>
<dl>
<dt><a href="../Page/香港巴士.md" title="wikilink">巴士</a></dt>

</dl>
<dl>
<dt><a href="../Page/機場快綫穿梭巴士.md" title="wikilink">機場快綫穿梭巴士</a><br />
<a href="../Page/的士站.md" title="wikilink">的士站</a></dt>

</dl></td>
</tr>
</tbody>
</table>

## 九龍酒店商場

[Kowloon_Hotel_Shopping_Arcade_2013.JPG](https://zh.wikipedia.org/wiki/File:Kowloon_Hotel_Shopping_Arcade_2013.JPG "fig:Kowloon_Hotel_Shopping_Arcade_2013.JPG")
九龍酒店商場設於地庫部分。地庫一層及二層原為中菜廳環龍閣及宴會廳，其後位於地庫二層的環龍閣易名為海逸軒，並於2006年4月結業（2008年7月已遷至二樓並以龍逸軒名義重新開業）。中菜廳及宴會廳遷出後，地庫部分於2006年至2011年期間曾經為[西武百貨的尖沙咀分店](../Page/西武百貨.md "wikilink")，為香港最後一家西武分店。西武尖沙咀店於2011年9月結業後，原址再次進行改建為九龍酒店商場，於2013年開業\[5\]，地面商鋪部分及B1層租予莎莎\[6\]、B2層則分拆成多個舖位，直通[尖東站L](../Page/尖東站.md "wikilink")4出入口，B3層則租予日本餐廳[千両](../Page/千両.md "wikilink")，樓層之間有扶手電梯連接。

## 參見

  - [都會海逸酒店](../Page/都會海逸酒店.md "wikilink")
  - [北角海逸酒店](../Page/北角海逸酒店.md "wikilink")
  - [嘉湖海逸酒店](../Page/嘉湖海逸酒店.md "wikilink")
  - [海逸國際酒店集團](../Page/海逸國際酒店集團.md "wikilink")

## 參考資料

<div style="font-size: 85%">

<references />

</div>

## 外部連結

  - [九龍酒店官方網址](http://www.harbour-plaza.com/tc/home.aspx?hotel_id=klnh&section_id=home&subsection_id=overview)

[Category:尖沙咀酒店](../Category/尖沙咀酒店.md "wikilink")
[Category:彌敦道](../Category/彌敦道.md "wikilink")

1.  [Kowloon Hotel, Hong
    Kong](http://www.emporis.com/en/wm/bu/?id=kowloonhotel-hongkong-china)(Emporis.com)
2.  [九龍酒店30周年珍珠禧誌慶](http://www.harbour-plaza.com/kowloon/SpecialOffer-KLNH_30th_Anniversary-tc.htm)
3.  [1985年施工期間的九龍酒店（Flickr圖片）](https://www.flickr.com/photos/aadvanderdrift/9523105923/in/album-72157635100911818/)
4.  [須予披露交易
    出售九龍酒店](http://www.hkexnews.hk/listedco/listconews/sehk/20041223/LTN20041223058_C.pdf)(香港上海大酒店有限公司公告，2004年12月23日)
5.  [九龍酒店商場改建](http://www.fujitec-hk.com.hk/tc/news/n12060003.html)(富士達香港有限公司，2012年6月)
6.  [](http://jetsoguide.com/jetso/63621)