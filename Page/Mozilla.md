**Mozilla**是一个[自由軟體社群](../Page/自由軟體社群.md "wikilink")，由[網景通訊公司的成員於](../Page/網景通訊公司.md "wikilink")1998年創立\[1\]\[2\]。在非正式的場合下，「Mozilla」這個名字常用於不同的事物上。這些事物大都與現已歇業的網景通訊公司及其旗下的應用軟體相關。許多其它「Mozilla」名詞的用法請見下方敘述。

## 開發代號

最初，「Mozilla」這個名字被用作[網景領航員的開發代號](../Page/网景导航者.md "wikilink")。網景通訊公司希望網景領航員能夠取代當時世界第一的[Mosaic](../Page/Mosaic.md "wikilink")，而這個名字由「Mosaic
Killa」（Killa是俚語中Killer的拼法）变化而来\[3\]，并与经典的虚拟怪物[哥吉拉谐趣](../Page/哥吉拉.md "wikilink")：「Godzilla
eat the
Mosaic」，即Mosaic+Godzilla+Killa=Mozilla\[4\]\[5\]，Netscape工程師[傑米·加文斯基說他是在一次Netscape員工會議上想到這個名字的](../Page/傑米·加文斯基.md "wikilink")\[6\]\[7\]。

## 开放原始代码计划

1998年3月31日，Netscape在源碼開放許可證的安排下，公開了[網景通訊家的大部分](../Page/網景通訊家.md "wikilink")[原始碼](../Page/源代码.md "wikilink")。這個項目沿用了Mozilla的名稱，並且新成立了Mozilla開發社群，及其專門網站Mozilla.org\[8\]。不過由於通訊家的原始碼老舊難以維護，通訊家的原始碼差不多被全部拋棄，Mozilla社群開發出更穩定、更多功能的新一代[互聯網應用套件](../Page/網路套件.md "wikilink")。經過一段長時間的開發之後，應用套件終於在2002年6月5日誕生。Mozilla當時成為該應用套件的名字。

Mozilla的程式碼成為了[Netscape 6和](../Page/Netscape_6.md "wikilink")[Netscape
7的基礎](../Page/Netscape_7.md "wikilink")。而由基金會推出的[Mozilla
Firefox](../Page/Mozilla_Firefox.md "wikilink")、[Mozilla
Thunderbird也是基於Mozilla最底層的程式碼](../Page/Mozilla_Thunderbird.md "wikilink")。為了幫助一般用戶分辨基金會旗下的多個軟件，基金會開始把應用套件稱呼為「[Mozilla
Application
Suite](../Page/Mozilla_Application_Suite.md "wikilink")」，簡稱「Mozilla
Suite」。

Mozilla基金會已不維護Mozilla Application
Suite，以便集中開發人員的精神在Firefox和Thunderbird上。現時Mozilla
Application
Suite已由非正式的[SeaMonkey所取代](../Page/SeaMonkey.md "wikilink")\[9\]。

## 社群

Mozilla社群由來自全球的積極貢獻者支持，部分志工参与運作，包括軟體[在地化](../Page/在地化.md "wikilink")、[程式設計等](../Page/程式設計.md "wikilink")。

Mozilla社群也指一个非嚴密組織。其成员使用、開發、推廣並支持Mozilla相關計畫及Mozilla宣言開放網路理念\[10\]，名為「**Mozillians**」（）\[11\]。
具體的行動包括：

  - 翻譯在地化Mozilla的軟體及網站成為其他語言。
  - 在[部落格或各相關活動進行](../Page/部落格.md "wikilink")[網頁標準傳教](../Page/網頁標準.md "wikilink")，有時是獨立行動、有時以更具組織的形式諸如“Mozilla
    Reps”專案呈現\[12\]\[13\]。
  - 組織在地及國際相關Mozilla會議，例如：Mozilla Camp、Mozilla Summit及Drumbeat。
  - 透過[網路論壇及](../Page/網路論壇.md "wikilink")[IRC等管道提供Mozilla產品使用者支援](../Page/Internet_relay_chat.md "wikilink")。
  - 為學童組織教育活動，透過如“Hackasaurus”\[14\]等專案傳授全球資訊網及如何製作網路內容等知識。
  - 測試Mozilla各產品的未正式發行版本並回報問題\[15\]。

許多此類活動為志願工作，而部份則由[Mozilla基金會贊助](../Page/Mozilla基金會.md "wikilink")\[16\]。

## 參考文獻

## 外部連結

  -
  - [Mozilla宣言](https://www.mozilla.org/en-US/about/manifesto/)

  - [Mozilla
    Wiki](https://wiki.mozilla.org/)（[Mozilla社群的重要里程碑\[英文](../Page/mozillawiki:Timeline.md "wikilink")\]）

  - \[//hg.mozilla.org/ Mozilla Mercurial Repository\]

## 參見

  - [Mozilla之書](../Page/Mozilla之書.md "wikilink")

  - [Mozilla (吉祥物)](../Page/Mozilla_\(吉祥物\).md "wikilink")

  -
{{-}}

[Mozilla](../Category/Mozilla.md "wikilink")
[Category:Netscape](../Category/Netscape.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14.
15.
16.