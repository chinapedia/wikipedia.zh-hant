[Lightning.inflight.arp.750pix.jpg](https://zh.wikipedia.org/wiki/File:Lightning.inflight.arp.750pix.jpg "fig:Lightning.inflight.arp.750pix.jpg")的[閃電式戰鬥機](../Page/閃電式戰鬥機.md "wikilink")\]\]
**超音速巡航**(英語:Supercruise)是指[飛機在不開啟](../Page/飛機.md "wikilink")[後燃器之情況下持續進行](../Page/後燃器.md "wikilink")[超音速飛行](../Page/超音速.md "wikilink")。第一架能夠維持水平超音速巡航飛行的是[英國航太的](../Page/英國航太.md "wikilink")[閃電式戰鬥機](../Page/閃電式戰鬥機.md "wikilink")\[1\]。

早期在定義上，只要能在平飛狀態下維持超音速飛行，就算是具備超音速巡航能力。但由於必須開啟後燃器才能維持超音速，燃料耗損極為巨大，可達一般狀態的數十倍之多，非常不經濟。一般戰鬥機在後燃器全開下，大多數不到十分鐘就會耗盡所有燃料。

由於現代引擎技術的提昇，使得不開後燃器進行超音速巡航已經成為可能，在[美國空軍於](../Page/美國空軍.md "wikilink")1987年提出的先進戰術戰鬥機（ATF）計劃中，即將此一能力列為必備要求之一，因此現代的超音速巡航定義必須能不開啟後燃器，否則就只能算是具備超音速飛行能力。

目前具備超音速巡航能力的飞机有前[苏联的](../Page/苏联.md "wikilink")**[Mig-31](../Page/Mig-31.md "wikilink")[截击机](../Page/截击机.md "wikilink")**、**[Tu-160战略](../Page/Tu-160.md "wikilink")[轰炸机](../Page/轰炸机.md "wikilink")**，在[美国ATF計劃中勝出的](../Page/美国.md "wikilink")**[F-22猛禽戰鬥機](../Page/F-22猛禽戰鬥機.md "wikilink")**和落敗的**[YF-23戰鬥機](../Page/YF-23戰鬥機.md "wikilink")**，[英國](../Page/英國.md "wikilink")、[德國](../Page/德國.md "wikilink")、[義大利](../Page/義大利.md "wikilink")、[西班牙等國合作研發的](../Page/西班牙.md "wikilink")**[颱風戰鬥機](../Page/颱風戰鬥機.md "wikilink")**，法國的[陣風戰鬥機](../Page/陣風戰鬥機.md "wikilink")，以及[瑞典的](../Page/瑞典.md "wikilink")**[JAS-39NG(E/F)
鹰狮战斗机](../Page/JAS_39獅鷲戰鬥機.md "wikilink")**，[英国](../Page/英国.md "wikilink")、[法国联合研制的](../Page/法国.md "wikilink")2倍音速[协和式客机等等](../Page/协和式客机.md "wikilink")。

另外，也有一些較為特殊的例子，不能直接以「是否使用後燃器」做為超音速巡航能力的標準。例如[SR-71黑鳥式偵察機](../Page/SR-71黑鳥式偵察機.md "wikilink")，是設計成在高速下會關閉燃燒室的通道，而將所有氣流都送往後燃器，也就是直接以後燃器做為動力來源的[衝壓引擎模式](../Page/衝壓引擎.md "wikilink")。亦有部份機型例如JSF計劃的**[F-35閃電II攻擊戰鬥機](../Page/F-35閃電II攻擊戰鬥機.md "wikilink")**\[2\]的超音速巡航，是必須先開啟後燃器以突破穿音速階段的推力瓶頸，或是利用特殊的飛行路徑，待完全進入超音速後即可關閉後燃器。

至於大型的[民航機和](../Page/民航機.md "wikilink")[轟炸機要超音速巡航較易](../Page/轟炸機.md "wikilink")，因為其較大的機體可以載更多燃料，而且發動機的冷卻較易解決，早如[XB-70和](../Page/XB-70.md "wikilink")[協和式客機都做到的](../Page/協和式客機.md "wikilink")，但所引起的噪音問題卻較難解決，而實際應用較少。

## 参考文献

[Category:航空術語](../Category/航空術語.md "wikilink")

1.  English Electric Aircraft and their Predecessors, Stephen Ransom &
    Robert Fairclough, Putnam, London, 1987, (p.227)
2.