**戈爾登瓦利縣**（**Golden Valley County,
Montana**）是[美國](../Page/美國.md "wikilink")[蒙大拿州中南部的一個縣](../Page/蒙大拿州.md "wikilink")。面積3,047平方公里。根据[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口1,042人。治所[拉伊蓋特](../Page/拉伊蓋特_\(蒙大拿州\).md "wikilink")（Ryegate）。

成立於1920年10月4日。\[1\]縣名的意思是「黃金谷地」，是用來吸引拓荒者之用。\[2\]

## 参考文献

<div class="references-small">

<references />

</div>

[G](../Category/蒙大拿州行政區劃.md "wikilink")

1.  [About Golden Valley
    County](http://www.co.golden-valley.mt.us/html/about.html)
2.  [Montana Railroad History - MONTANA PLACE
    NAMES](http://www.montanarailroadhistory.info/MontanaPlaceNames.htm)