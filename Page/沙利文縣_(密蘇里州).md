**沙利文縣**（**Sullivan County,
Missouri**）是[美國](../Page/美國.md "wikilink")[密蘇里州北部的一個縣](../Page/密蘇里州.md "wikilink")。面積1,687平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口7,219人。縣治[米蘭](../Page/米蘭_\(密蘇里州\).md "wikilink")
(Milan)。

成立於1843年2月17日，原名**高地縣** (**Highland
County**)，1845年2月14日建立縣政府時改用現名。縣名紀念[美國獨立戰爭英雄](../Page/美國獨立戰爭.md "wikilink")[約翰·沙利文](../Page/約翰·沙利文.md "wikilink")[少將](../Page/少將.md "wikilink")。

[S](../Category/密苏里州行政区划.md "wikilink")