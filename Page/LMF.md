**LMF**，全名**Lazy Mutha
Fucka**，中文名為**大懶堂**，是於1999年成立的一隊[香港](../Page/香港.md "wikilink")[新金屬](../Page/新金屬.md "wikilink")、[硬核嘻哈](../Page/硬核嘻哈.md "wikilink")、[饒舌搖滾樂隊組合](../Page/饒舌搖滾.md "wikilink")\[1\]，歌詞內容對於[政治](../Page/政治.md "wikilink")、社會現象等題材有一定的涉獵和探討，有別於其他香港主流樂隊。

## 簡介

**LMF**早期不過是一群[獨立樂隊在](../Page/地下樂隊.md "wikilink")「Dark
Entry」音樂會尾聲的時候以「大雜燴」形式出現，而後隨著「Dark
Entry」的停辦，LMF也停擺了一段時間，1996年曾為香港地下音樂合輯「自主音樂圖鑑
」寫過他們第一首的原創中文歌「[同床](../Page/同床.md "wikilink")」。直到1999年以[Anodize](../Page/Anodize.md "wikilink")、[Screw和](../Page/Screw.md "wikilink")[N.T.三個獨立](../Page/N.T..md "wikilink")[樂隊為骨幹](../Page/樂隊.md "wikilink")（然而後期，原屬N.T.的MC
Yan跟其他原屬N.T.的成員意見不合，最後只有[MC仁留在LMF](../Page/MC仁.md "wikilink")；其他原屬N.T.的成員便跟個別樂隊另組“Family”），加上在本地[Hip-hop界享負盛名的DJ](../Page/Hip-hop.md "wikilink")
tommy組成了現時人所共知的LMF。

LMF自1999年推出首張EP後，因歌詞有不少[粗口內容](../Page/粗口.md "wikilink")，其歌曲即引起社會頗大爭議，因此部分歌曲也推出無粗口版本。但他們和不少專門研究社會問題和流行文化的學者和人士皆認為，那是時下年青人的文化和態度，以及對現實社會的不滿。後來他們漸漸為社會所接受，並晉身主流樂壇。

2000年他們順勢推出\<<大懶堂>\>，因為沒有列明指引條例，令華納唱片公司賠償款項，不過做就在樂壇的意外的成績，令他們進身三甲位置，叱吒樂壇我最喜愛組合獎金獎和至獎歌曲第五位。

2001年，他們作為[生力啤的代言人](../Page/生力啤.md "wikilink")，並拍攝多輯廣告。然而，被商業化的LMF使他們的支持度下降，同時亦不斷備受主流媒體和保守人士的長期批評。

最後於2003年解散，但部份成員至今仍活躍於樂壇，並經常與香港及其他地區的音樂界人士合作交流音樂。

2009年4月，[香港電影金像獎頒獎典禮](../Page/香港電影金像獎頒獎典禮.md "wikilink")，[李小龍剛好](../Page/李小龍.md "wikilink")60週年，他們被邀請唱
\<\<1127\>\> 紀念李小龍，而 \<\<1127\>\>
本身意義，是[李小龍出生月份](../Page/李小龍.md "wikilink")，11月27日。
11月，LMF正式復出並發表新曲《揸緊中指》，其後再推出新曲《數治時代》。

2014年發表新曲《惡世紀》。

## 獎項

  - 2000年度[叱咤樂壇流行榜頒獎典禮專業推介](../Page/叱咤樂壇流行榜頒獎典禮.md "wikilink")．叱咤十大第五位
    \<\<[大懶堂](../Page/大懶堂.md "wikilink")\>\>
  - 2000年度[叱咤樂壇流行榜頒獎典禮組合獎](../Page/叱咤樂壇流行榜頒獎典禮.md "wikilink")：金獎
  - 2001年度[十大中文金曲](../Page/十大中文金曲.md "wikilink") 最愛歡迎卡拉OK歌曲獎：愛是 ....
    feat [鄭秀文](../Page/鄭秀文.md "wikilink")

## 成員

### 主要成員

  - [陳偉雄](../Page/陳偉雄.md "wikilink")（Phat，阿肥）（Rap）（現亦為組合[廿四味](../Page/廿四味_\(組合\).md "wikilink")（主音））
  - [梁永傑](../Page/梁永傑.md "wikilink")（Kit，阿傑）（Rap）（現亦為組合[廿四味](../Page/廿四味_\(組合\).md "wikilink")（主音））
  - [陳廣仁](../Page/陳廣仁.md "wikilink")（MC
    Yan，MC仁）（Rap）（原屬[N.T.的成員](../Page/N.T..md "wikilink")，現成立了自己的音樂廠牌（Fu©Kin
    Music福建音樂）及服裝品牌（寧死不屈 NSBQ） 是香港街頭塗鴉教父）（現亦為獨立音樂人、塗鴉藝術家，修西藏密宗，研究神秘學）
  - [孫國華](../Page/孫國華.md "wikilink")（華哥）（Rap，唱）(前重金屬樂隊[Anodize主音](../Page/Anodize.md "wikilink")）
  - [陳匡榮](../Page/陳匡榮.md "wikilink")（Davy，大飛）（結他）(前重金屬樂隊[Anodize鼓手](../Page/Anodize.md "wikilink")，現亦為主流歌手作曲及A.ROOM
    監製)
  - [麥文威](../Page/麥文威.md "wikilink")（Jimmy，老占）（低音結他）(前重金屬樂隊[Anodize](../Page/Anodize.md "wikilink")
    低音結他手，現亦為[ThePostman和](../Page/ThePostman.md "wikilink")[COOPER樂隊低音結他手](../Page/COOPER樂隊.md "wikilink")，DJ
    BMX 專門店創辦人)
  - [李健宏](../Page/李健宏.md "wikilink")（Kevin，Kevin仔）（鼓）（[TVB勁歌金曲鼓手](../Page/TVB.md "wikilink"),Sun
    entertainment artist,品牌The Baddies主理人）
  - [梁偉庭](../Page/梁偉庭.md "wikilink")（Prodip，阿庭）（結他）（現亦為唱片封套設計師及[UFO研究員](../Page/UFO.md "wikilink")，曾策劃香港首部有關不明飛行物體的紀錄片《Hong
    Kong UFO Documentary》）

### 其他

  - [李燦森](../Page/李璨琛.md "wikilink")（Sam）（為客席成員，Monkey Men主音）
  - [鄭華勝](../Page/鄭華勝.md "wikilink")（Gary，勝哥）(結他 / 結他)已退出
  - [張進偉](../Page/張進偉.md "wikilink")（DJ
    Tommy，烏蠅）(打碟机和混音台)（唱片公司[CMD前CEO](../Page/CMD.md "wikilink")）已退出
  - [洪柏琪](../Page/洪柏琪.md "wikilink")（Kee，阿琪）(結他)已退出

## 唱片列表

  - 1996年：《V.A. 自主音樂圖鑑》—「同床」
  - 1999年：《Lazy Mutha Fucka》（EP）
  - 2000年：《大懶堂》
  - 2001年：《LMFamiglia》（EP）
  - 2002年：《Crazy Children》
  - 2002年：《嘻武門》（精選+新曲）
  - 2002年：《LMF VIDEOPHILE》（影碟）
  - 2003年：《Finalazy》（CD + Bonus CD）
  - 2003年：《LMFsHits Greatest Hits》（精選，2CD）
  - 2009年：《揸緊中指》（白版 Promotion CD）
  - 2009年：《The Ultimate_s...Hits》（精選，3CD）

### 個別成員作品

  - 1999年： [DJ TOMMY](../Page/DJ_TOMMY.md "wikilink") 《[Scratch
    Rider](../Page/Scratch_Rider.md "wikilink")》
  - 2001年： [DJ TOMMY](../Page/DJ_TOMMY.md "wikilink") 《[Respect 4 Da
    Chopstick
    HipHop](../Page/Respect_4_Da_Chopstick_HipHop.md "wikilink")》
  - 2001年：[大飛DAVY CHAN](../Page/大飛DAVY_CHAN.md "wikilink")《[Aroom
    Represents The Realazy
    Mofo.DBF](../Page/Aroom_Represents_The_Realazy_Mofo.DBF.md "wikilink")》
  - 2008年：[MC仁](../Page/MC仁.md "wikilink")《[福建音樂](../Page/福建音樂.md "wikilink")》
  - 2009年： [DJ TOMMY](../Page/DJ_TOMMY.md "wikilink")
    《[Vinyl-CD-MP3](../Page/Vinyl-CD-MP3.md "wikilink")》

## 演唱會

  - 2001年：[2001 Amiglia 演唱會](../Page/2001_Amiglia_演唱會.md "wikilink")
  - 2003年：[2003 Finalazy 演唱會](../Page/2003_Finalazy_演唱會.md "wikilink")
  - 2009年12月6日：[The Wild Lazy
    Tour(Singapore)](../Page/The_Wild_Lazy_Tour\(Singapore\).md "wikilink")
  - 2010年1月2-3日：[The Wild Lazy Tour(Hong
    Kong)](../Page/The_Wild_Lazy_Tour\(Hong_Kong\).md "wikilink")
  - 2010年4月24日：[The Wild Lazy
    Tour(Macao)](../Page/The_Wild_Lazy_Tour\(Macao\).md "wikilink")
  - 2010年9月4日：[The Wild Lazy Tour(KUALA LUMPUR,
    MALAYSIA)](../Page/The_Wild_Lazy_Tour\(KUALA_LUMPUR,_MALAYSIA\).md "wikilink")
  - 2014年3月31日：[Neo Tribes Tour(Hong
    Kong)](../Page/Neo_Tribes_Tour\(Hong_Kong\).md "wikilink")
  - 2015年5月31日：澳門站

## 電影

  - 2002年：《[大你](../Page/大你.md "wikilink")》（紀錄片）

## 評價

以[粗口](../Page/粗口.md "wikilink")、[通俗手法寫成的](../Page/通俗.md "wikilink")[粵語歌](../Page/粵語歌.md "wikilink")，早見於[二戰後的](../Page/二戰.md "wikilink")[香港](../Page/香港.md "wikilink")。[黃秋生從](../Page/黃秋生.md "wikilink")1990年代起曾推出三張含有粗口歌的唱碟。LMF的歌詞，大多數以身邊之見聞而有感而發。在《WTF》中聽出LMF對[官商勾結及政府的控訴和不滿](../Page/官商勾結.md "wikilink")\[2\]。

有人認為這是唱出了「港人心聲」，又有人指出這是創作自由的成果。

但由於有很多人對這類歌詞難以接受，加上各種原因，令唱[粗口](../Page/粗口.md "wikilink")[歌的人變得具爭議性](../Page/歌.md "wikilink")：

  - 港人以聽情歌、流行曲為主，一時難以接受另類的音樂文化。
  - 有[保守派人士為了樹立衛道之士的形象](../Page/保守派.md "wikilink")，而打擊唱粗口歌的人，並抹黑其形象。
  - 狂野派對流行年代，填上粗俗歌詞的舞曲大行其道。而繼LMF的歌曲流行後，陸續有不少Hip
    Hop歌手進身樂壇。到現時仍有很多人分不清誰唱甚麼曲的，做成資訊混淆。例如有人把樂隊[MP4](../Page/MP4.md "wikilink")（4PM）所唱的「老竇咪索茄」，當成為LMF所唱。
  - 「[潮童](../Page/潮童.md "wikilink")」為了聽粗口而聽粗口歌，曲解歌詞原意。令人誤解為「粗口歌就是教壞人」，因而只要是粗口歌就會被[歧視](../Page/歧視.md "wikilink")。

## 有關書籍

  - 《出賣LMF：粗口音樂檔案》（伍詠詩等著）\[3\]
  - 《地下狂野分子》（[馬傑偉著](../Page/馬傑偉.md "wikilink")）\[4\]

## 外部連結

  - [成員梁偉庭（Prodip）訪問聲軌及短片](http://pinkmessage.com/talk/viewtopic.php?t=186)
  - [《香港不明飛行物記錄》創作者梁偉庭訪問](http://hkfilex.rthk.org.hk/tags/listing.php?tid=607)
  - [LMF-"The Wild Lazy Tour Finale" official facebook
    page](http://www.facebook.com/pages/LMF-The-Wild-Lazy-Tour-Finale/121200484616806)
  - [LMF:THE WILD LAZY TOUR official web
    site](http://www.wildlazymf.com/)
  - [" www.ourradio.hk Broad-Band Music "
    嘉賓：LMF全體成員](https://web.archive.org/web/20100511153803/http://www.ourradio.hk/web/index.php?option=com_content&view=article&id=875%3Abroad-band-music-20100101&catid=77%3Abroad-band-music-&Itemid=97)

## 參考文獻

<div class="references-small">

<references>

</references>

</div>

[Category:香港男子演唱團體](../Category/香港男子演唱團體.md "wikilink")
[Category:粤语Hip-Hop](../Category/粤语Hip-Hop.md "wikilink")
[Category:香港搖滾樂團](../Category/香港搖滾樂團.md "wikilink")

1.  [LMF成員介紹自己及LMF](http://www.youtube.com/watch?v=5celcs5udAk)

2.  [LMF - WTF](http://www.youtube.com/watch?v=O82Y59EIFU8)

3.  [頭條日報 頭條網 -
    粗口歌Vs.主流文化？](http://www.hkheadline.com/culture/culture_content.asp?contid=53428&srctype=g)

4.