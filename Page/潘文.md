**潘文**（英文名：**John Pomfret**），是一位美國记者。

## 生平

潘文生於美国[威斯康辛州](../Page/威斯康辛州.md "wikilink")[密爾沃基](../Page/密爾沃基.md "wikilink")，在[纽约长大](../Page/纽约.md "wikilink")。1977年入[斯坦福大学学习](../Page/斯坦福大学.md "wikilink")。1980年代初作为交换留学生到[中国](../Page/中国.md "wikilink")，在[南京大学历史系学习](../Page/南京大学.md "wikilink")。之后返回美国，在斯坦福大学完成学业。他於1983-1984年作為[福布莱特學者到](../Page/福布莱特计划.md "wikilink")[新加坡的](../Page/新加坡.md "wikilink")[東南亞研究所研究](../Page/東南亞研究所.md "wikilink")[柬埔寨內戰](../Page/柬埔寨內戰.md "wikilink")\[1\]。他曾在[法国](../Page/法国.md "wikilink")[巴黎做酒吧调酒服务生](../Page/巴黎.md "wikilink")，也曾在[日本学习柔道](../Page/日本.md "wikilink")。1986年起任[美联社驻](../Page/美联社.md "wikilink")[中国大陆](../Page/中国大陆.md "wikilink")、[香港](../Page/香港.md "wikilink")、[越南](../Page/越南.md "wikilink")、[印度](../Page/印度.md "wikilink")、[阿富汗](../Page/阿富汗.md "wikilink")、[南斯拉夫新闻记者](../Page/南斯拉夫.md "wikilink")，1992年[海湾战争期间在](../Page/海湾战争.md "wikilink")[沙特阿拉伯](../Page/沙特阿拉伯.md "wikilink")、[伊拉克](../Page/伊拉克.md "wikilink")、[伊朗](../Page/伊朗.md "wikilink")、[科威特担任战地记者](../Page/科威特.md "wikilink")。1992年担任《[华盛顿邮报](../Page/华盛顿邮报.md "wikilink")》驻[巴尔干半岛首席记者](../Page/巴尔干半岛.md "wikilink")。1998年任华盛顿邮报[北京分社社长](../Page/北京.md "wikilink")、首席记者。
2004年，由于对亚洲的最佳报道被美国[亞洲協會授予Osborn](../Page/亞洲協會.md "wikilink") Elliott
Prize。\[2\] 现任华盛顿邮报[洛杉矶分社社长](../Page/洛杉矶.md "wikilink")。

潘文是第一批到中国大陆学习的美国大学生，[南京大学是当时唯一一所让外国学生和本国学生同住的重点大学](../Page/南京大学.md "wikilink")，在南大留学时和七名中国学生合住一个宿舍。他2006年出版的第一本著书，《Chinese
Lessons: Five Classmates, and the Story of the New China 》（直译 -
《中国教训：五位同学及新中国的故事》），讲述了五位南大历史系八二级同学的个人经历，透过他们迥然不同的命运见证中国社会的变化。\[3\]

## 参见

  - [潘公凯](../Page/潘公凯_\(记者\).md "wikilink")

## 參考資料

## 外部链接

  - [John Pomfret](http://www.johnpomfret.net/)
  - [老外书写南大82级同学经历侧面展现中国二十年变迁
    （美国《侨报》）](http://www.chinapressusa.com/shequ/200608070006.htm)

[category:美國记者](../Page/category:美國记者.md "wikilink")
[category:紐約市人](../Page/category:紐約市人.md "wikilink")

[Category:史丹佛大學校友](../Category/史丹佛大學校友.md "wikilink")
[Category:南京大學校友](../Category/南京大學校友.md "wikilink")
[Category:威斯康辛州人](../Category/威斯康辛州人.md "wikilink")
[Category:华盛顿邮报人物](../Category/华盛顿邮报人物.md "wikilink")
[Category:傅爾布萊特學者](../Category/傅爾布萊特學者.md "wikilink")

1.
2.  [John Pomfret of the Washington Post Receives $10,000 Award for His
    Inside Look at the Historical Changes Taking Place in
    China](http://asiasociety.org/media/press-releases/john-pomfret-washington-post-receives-10000-award-his-inside-look-historical-ch).亞洲協會.

3.