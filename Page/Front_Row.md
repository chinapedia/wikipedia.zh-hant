**Front
Row**是由[苹果电脑公司为其](../Page/苹果电脑公司.md "wikilink")[麦金塔电脑开发的一个](../Page/麦金塔电脑.md "wikilink")[软件](../Page/软件.md "wikilink")[应用程序](../Page/应用程序.md "wikilink")。它由2005年10月12日[苹果电脑公司](../Page/苹果电脑公司.md "wikilink")[CEO](../Page/CEO.md "wikilink")[斯蒂夫·乔布斯在活动上发布](../Page/斯蒂夫·乔布斯.md "wikilink")。可以让用户通过[苹果遥控器](../Page/苹果遥控器.md "wikilink")（Apple
Remote，形状和第一代[iPod
shuffle类似](../Page/iPod_shuffle.md "wikilink")，只有六个按钮）直接访问[QuickTime](../Page/QuickTime.md "wikilink")，[DVD
Player](../Page/DVD_Player.md "wikilink")、[iTunes和](../Page/iTunes.md "wikilink")[iPhoto浏览电脑中的各种媒体](../Page/iPhoto.md "wikilink")。该软件已附在[iMac](../Page/iMac.md "wikilink")、[MacBook](../Page/MacBook.md "wikilink")、[MacBook
Pro](../Page/MacBook_Pro.md "wikilink")，[Intel Mac
mini中](../Page/Mac_mini.md "wikilink")。

苹果已经宣布下一代Front
Row将支援目前没被支持的[麦金塔电脑](../Page/麦金塔电脑.md "wikilink")，（和[Photo
Booth一样](../Page/Photo_Booth.md "wikilink")，新版本将与[Mac OS X
v10.5](../Page/Mac_OS_X_v10.5.md "wikilink")
Leopard花豹）一起发布。舆论一直关注其如何在没有遥控器内置支持的[麦金塔电脑使用](../Page/麦金塔电脑.md "wikilink")。

已发布的[Apple TV的界面和操作方式和Front](../Page/Apple_TV.md "wikilink")
Row十分相似，皆是使用Apple Remote Control來操控。

Mac OSX 10.7已取消Front Row軟體應用程序。

## 性能

Front Row主要功能包括：

  - 类似iPod的大画面用户界面
  - 从播放列表或iTunes音乐库中浏览播放音乐
  - 从iPhoto中浏览相册和观看幻灯片播放
  - 观看从iTunes购买的电视剧和音乐录影带
  - 观看电影预告片
  - 可以观看[DVD](../Page/DVD.md "wikilink")
  - 观看使用[iMovie所制作的影片](../Page/iMovie.md "wikilink")
  - 用QuickTime观看媒体库中，或者用户电影文件夹中的非[iTMS视频](../Page/iTMS.md "wikilink")
  - 观看“[Podcasts](../Page/Podcasts.md "wikilink")”（包含音樂Podcast或是視訊Podcast，[Vodcast](../Page/Vodcast.md "wikilink")。）
  - 通过[Bonjour观看](../Page/Bonjour.md "wikilink")、浏览、收听同樣網路環境中电脑上的所有媒體類型格式

[Category:蘋果公司軟體](../Category/蘋果公司軟體.md "wikilink")
[Category:MacOS軟體](../Category/MacOS軟體.md "wikilink")
[Category:iTunes](../Category/iTunes.md "wikilink")
[Category:媒体播放器](../Category/媒体播放器.md "wikilink")
[Category:已停止開發的軟體](../Category/已停止開發的軟體.md "wikilink")