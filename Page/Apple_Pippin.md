**Apple
Pippin**是一种由[苹果公司在](../Page/苹果公司.md "wikilink")1990年代中期开发的游戏机／多媒体播放机平台。它采用66[MHz的](../Page/MHz.md "wikilink")
[PowerPC
603e处理器](../Page/PowerPC_600#PowerPC_603e_.2F_603ev.md "wikilink")，14.4
kbit/s调制解调器并运行一精简版之[Mac
OS](../Page/Mac_OS.md "wikilink")。其目的是希望创造出主要面向基于[CD之多媒体播放](../Page/CD.md "wikilink")（特别是游戏）之低价电脑，并附带网络功能。它有一4倍速[光驱及可以连接至](../Page/光驱.md "wikilink")[电视的视讯输出](../Page/电视.md "wikilink")。其名字的由来是[翠玉苹果](../Page/翠玉苹果.md "wikilink")（Newtown
Pippin）——一个[苹果](../Page/苹果.md "wikilink")[栽培品种](../Page/栽培品种.md "wikilink")，类似于[旭苹果](../Page/旭苹果.md "wikilink")（McIntosh）之于[麦金塔](../Page/麦金塔.md "wikilink")（Macintosh）。

苹果公司从未企图自行推出Pippin，而是希望将其授权给第三方厂商－与不幸的[3DO相同的商业模式](../Page/3DO.md "wikilink")。然而，只有[Bandai被授权制造贩售Pippin](../Page/Bandai.md "wikilink")。

在Bandai的Pippin上市之时（1995年于[日本](../Page/日本.md "wikilink")，1996年于[美国](../Page/美国.md "wikilink")），市场已被功能更强大的[任天堂64与](../Page/任天堂64.md "wikilink")[PlayStation占据](../Page/PlayStation_\(遊戲機\).md "wikilink")。此外，当时Pippin可用的软件亦少得可怜－唯一的供应商就是Bandai自己。美金599元的初始价格、被官方定位为低价电脑的Pippin，实际上其系统更常被认为是[电视游乐器](../Page/电视游乐器.md "wikilink")。因此，它的价位被认为高出其同类商品太多。

仅有数千台Pippin被制造出来，因此，甚至连键盘与调制解调器附件的产量都比主机多。\[1\]

最终，Pippin因为太晚切入3D游戏机场、且不论是作为游戏机或电脑来说功能都太弱，因此遭受沉重打击。Bandai的版本很快就消亡，只在美国和日本取得了有限的销量。

在2006年5月，Pippin被[PC
World杂志选为史上](../Page/PC_World杂志.md "wikilink")25大科技烂货。\[2\]

2011年时，[李开复在其微博上透露他在苹果工作时正是Pippin这个项目的负责人](../Page/李开复.md "wikilink")。\[3\]

## 技术资讯

### 硬件

  - 66MHz PowerPC 603 RISC 微处理器
      - Superscalar, three instructions per clock cycle
      - 8KB data and 8KB instruction caches
      - IEEE标准单／双精确浮点运算器(FPU-Floating Point Unit)

<!-- end list -->

  - 6MB系统与视频共用内存，advanced architecture
      - 2、4及8MB内存扩充卡

<!-- end list -->

  - 128K SRAM store/restore backup
  - 4倍速光驱
  - 两个高速[序列埠](../Page/序列埠.md "wikilink")，其中一个可支持[GeoPort](../Page/GeoPort.md "wikilink")
  - PCI兼容扩充槽

<!-- end list -->

  - Two ruggedized ADB inputs
      - Supports up to four simultaneous players over Apple Desktop Bus
        (ADB)
      - Supports standard ADB keyboards and mice with connector adapters

### 视频

  - 8位元与16位元视频支持
  - Dual frame buffers for superior frame-to-frame animation
  - 支持NTSC与PAL复合视频、S-Video与VGA(640x480)屏幕
  - 水平／垂直视频回旋

### 音频

  - 双声道16位元44kHz范例输出
  - 双声道16位元44kHz范例输入
  - 耳机输出孔与单独的音量调整器
  - 可播放音频CD

### 软件

  - Runtime environment derived from Mac OS
  - PPC native version of QuickDraw
  - Reduced system memory footprint (computer specific features removed)
  - Disk-resident System Software stamped on CD-ROM with title
  - System boots off of CD-ROM
  - Pippin System Software upgrades released through CD-ROM stamping
    operations
  - 68k emulator
  - Macintosh Toolbox intact

## 参见

  - [苹果电脑公司](../Page/苹果电脑公司.md "wikilink")

## 参考来源

## 外部链接

  - [Complete List of Apple Pippin Titles Released in the US and
    Japan](http://www.gooddealgames.com/Release_Lists/RL_Pippin.html)
  - [Bandai Pippin Museum and
    Archive](http://www.macgeek.org/museum/pippin/)
  - [ASSEMbler's Bandai Pippin
    Page](https://web.archive.org/web/20080321013705/http://assembler.roarvgm.com/Apple_Bandai_pippin/apple_bandai_pippin.html)
  - [Apple's Original Pippin
    Site](https://web.archive.org/web/19970129095612/http://www.pippin.apple.com/)
  - [Hacking the Pippin](http://www.vintagemacworld.com/pip1.html)
  - [Apple's Pippin and Bandai's @World: Missing the
    Mark(et)](http://lowendmac.com/coventry/06/0922.html)
  - [Pippin Software and games in
    action](http://www.mac-collection.co.uk)
  - [Pippin World (via
    Archive)](https://web.archive.org/web/20050308044555/http://www.pippinworld.co.uk/)

[Category:萬代遊戲機](../Category/萬代遊戲機.md "wikilink")
[Category:第五世代游戏机](../Category/第五世代游戏机.md "wikilink")
[Pinpin](../Category/蘋果公司硬體.md "wikilink")
[Category:1995年面世的產品](../Category/1995年面世的產品.md "wikilink")
[Category:失敗的游戏机](../Category/失敗的游戏机.md "wikilink")
[Category:網路電腦](../Category/網路電腦.md "wikilink")

1.  Assembler. *[Apple Bandai
    Pippin](http://assembler.roarvgm.com/Apple_Bandai_pippin/apple_bandai_pippin.html)
    *. Retrieved Nov. 30, 2006
2.  [1](http://www.pcworld.com/article/125772/worst_products_ever.html?page=6)
3.  [@李开复：我真正负责的产品是Pippin。这个产品是个迷你Mac游戏机，只有CD，没有硬盘，连接电视。但是：1）市场已有强大对手：Sony、Sega、任天堂，2）苹果资金不足，只给40人的薪水，其他经费求合作伙伴Bandai出，3）Pippin的特点是有很多益智教育的游戏，但是这不是市场想要的。从这个产品的失败，我学到很多。](http://weibo.com/1197161814/xldJCnqRU?mod=weibotime).李开复.新浪微博.2011-08-26.\[2014-07-25\].