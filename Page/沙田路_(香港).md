[Sha_Tin_Road_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Sha_Tin_Road_\(Hong_Kong\).jpg "fig:Sha_Tin_Road_(Hong_Kong).jpg")西面的高架道路路段。\]\]
**沙田路（英文：Sha Tin
Road）**，是[香港](../Page/香港.md "wikilink")[1號幹線的末段](../Page/香港1號幹線.md "wikilink")，位於[新界](../Page/新界.md "wikilink")[沙田東部](../Page/沙田區.md "wikilink")，全長3.6公里。連接[獅子山隧道](../Page/獅子山隧道.md "wikilink")[紅梅谷出口的](../Page/紅梅谷.md "wikilink")[獅子山隧道公路及位於](../Page/獅子山隧道公路.md "wikilink")[沙田馬場附近的](../Page/沙田馬場.md "wikilink")[大埔公路沙田段](../Page/大埔公路沙田段.md "wikilink")。全線為兩線雙程分隔[快速公路](../Page/高速公路.md "wikilink")\[1\]，部份路段為[高架道路](../Page/高架道路.md "wikilink")。車速限制每小時80公里。

沙田路繞過[沙田新市鎮中心及跨越](../Page/沙田新市鎮.md "wikilink")[沙田第一城附近的](../Page/沙田第一城.md "wikilink")[城門河](../Page/城門河.md "wikilink")，連接[獅子山隧道及](../Page/獅子山隧道.md "wikilink")[9號幹線](../Page/9號幹線.md "wikilink")。此外，沙田路有支路（沙瀝公路）接駁[2號幹線的](../Page/2號幹線.md "wikilink")[大老山公路](../Page/大老山公路.md "wikilink")，通往[大老山隧道或](../Page/大老山隧道.md "wikilink")[馬鞍山](../Page/馬鞍山_\(香港市鎮\).md "wikilink")。

## 出口位置

<table>
<thead>
<tr class="header">
<th><p><a href="../Page/沙田路_(香港).md" title="wikilink">沙田路</a> <a href="https://zh.wikipedia.org/wiki/File:HK_Route1.svg" title="fig:链接=https://zh.wikipedia.org/wiki/File:HK_Route1.svg">链接=<a href="https://zh.wikipedia.org/wiki/File:HK_Route1.svg">https://zh.wikipedia.org/wiki/File:HK_Route1.svg</a></a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>北行</p></td>
</tr>
<tr class="even">
<td><p>連接大埔公路－沙田段 <a href="https://zh.wikipedia.org/wiki/File:HK_Route1.svg" title="fig:链接=https://zh.wikipedia.org/wiki/File:HK_Route1.svg">链接=<a href="https://zh.wikipedia.org/wiki/File:HK_Route1.svg">https://zh.wikipedia.org/wiki/File:HK_Route1.svg</a></a><a href="https://zh.wikipedia.org/wiki/File:HK_Route9.svg" title="fig:链接=https://zh.wikipedia.org/wiki/File:HK_Route9.svg">链接=<a href="https://zh.wikipedia.org/wiki/File:HK_Route9.svg">https://zh.wikipedia.org/wiki/File:HK_Route9.svg</a></a></p></td>
</tr>
<tr class="odd">
<td><p><em>沙田路<strong>起點</strong></em> <a href="https://zh.wikipedia.org/wiki/File:HK_Route1.svg" title="fig:链接=https://zh.wikipedia.org/wiki/File:HK_Route1.svg">链接=<a href="https://zh.wikipedia.org/wiki/File:HK_Route1.svg">https://zh.wikipedia.org/wiki/File:HK_Route1.svg</a></a></p></td>
</tr>
<tr class="even">
<td><p>不適用</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/火炭.md" title="wikilink">火炭</a> <a href="../Page/大涌橋路.md" title="wikilink">大涌橋路</a>、<a href="../Page/火炭路.md" title="wikilink">火炭路</a>（<a href="../Page/翠榕橋.md" title="wikilink">翠榕橋</a>）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/圓洲角.md" title="wikilink">圓洲角</a> <a href="../Page/沙田圍路.md" title="wikilink">沙田圍路</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/小瀝源.md" title="wikilink">小瀝源</a>、<a href="../Page/馬鞍山_(香港市鎮).md" title="wikilink">馬鞍山</a>、<a href="../Page/九龍東選區.md" title="wikilink">九龍（東）</a><a href="https://zh.wikipedia.org/wiki/File:HK_Route2.svg" title="fig:20x20像素">20x20像素</a></p>
<p><a href="../Page/沙瀝公路.md" title="wikilink">沙瀝公路</a></p></td>
</tr>
<tr class="even">
<td><p><em>沙田路<strong>起點</strong></em> <a href="https://zh.wikipedia.org/wiki/File:HK_Route1.svg" title="fig:链接=https://zh.wikipedia.org/wiki/File:HK_Route1.svg">链接=<a href="https://zh.wikipedia.org/wiki/File:HK_Route1.svg">https://zh.wikipedia.org/wiki/File:HK_Route1.svg</a></a></p></td>
</tr>
<tr class="odd">
<td><p>連接<a href="../Page/獅子山隧道公路.md" title="wikilink">獅子山隧道公路</a><a href="https://zh.wikipedia.org/wiki/File:HK_Route1.svg" title="fig:链接=https://zh.wikipedia.org/wiki/File:HK_Route1.svg">链接=<a href="https://zh.wikipedia.org/wiki/File:HK_Route1.svg">https://zh.wikipedia.org/wiki/File:HK_Route1.svg</a></a></p></td>
</tr>
</tbody>
</table>

## 途經的公共交通服務

<div class="NavFrame collapsed" style="background-color: #FFFF00; clear: no; border: 1px solid #999; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: center; font-weight: bold;">

交通路線列表

</div>

<div class="NavContent" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px;">

  - [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - [紅色小巴](../Page/紅色小巴.md "wikilink")

<!-- end list -->

  - 荃灣至沙田馬場線\[2\]
  - 觀塘至沙田馬場線\[3\]
  - 大埔至旺角線\[4\]
  - 上水至旺角線\[5\]
  - 觀塘至落馬洲線\[6\]
  - 上水至銅鑼灣線\[7\]

</div>

</div>

## 参考文献

[Category:香港高速公路](../Category/香港高速公路.md "wikilink")
[Category:沙田區街道](../Category/沙田區街道.md "wikilink")
[Category:沙田區](../Category/沙田區.md "wikilink")
[Category:紅色公共小巴可行駛的幹線街道](../Category/紅色公共小巴可行駛的幹線街道.md "wikilink")

1.
2.  [荃灣川龍街—沙田馬場](http://www.16seats.net/chi/rmb/r_n48.html)
3.  [觀塘協和街—沙田馬場](http://www.16seats.net/chi/rmb/r_kn89.html)
4.  [旺角西洋菜南街—大埔太和邨](http://www.16seats.net/chi/rmb/r_kn72.html)
5.  [旺角亞皆老街—大埔及上水](http://www.16seats.net/chi/rmb/r_kn73.html)
6.  [觀塘同仁街—落馬洲](http://www.16seats.net/chi/rmb/r_kn77.html)
7.  [銅鑼灣鵝頸橋—大埔及上水](http://www.16seats.net/chi/rmb/r_hn73.html)