[1900._Няньхуа.jpg](https://zh.wikipedia.org/wiki/File:1900._Няньхуа.jpg "fig:1900._Няньхуа.jpg")
[Toyouni_I-New-Year.jpg](https://zh.wikipedia.org/wiki/File:Toyouni_I-New-Year.jpg "fig:Toyouni_I-New-Year.jpg")
[Minhwa-Tiger_and_magpie-043.jpg](https://zh.wikipedia.org/wiki/File:Minhwa-Tiger_and_magpie-043.jpg "fig:Minhwa-Tiger_and_magpie-043.jpg")
[Phú_quý.JPG](https://zh.wikipedia.org/wiki/File:Phú_quý.JPG "fig:Phú_quý.JPG")木板年畫\]\]

**年画**是東亞的一种民间美术形式。因伴随着[新春](../Page/新春.md "wikilink")[趋吉避凶的活动也产生](../Page/趋吉避凶.md "wikilink")，故名**年画**。在朝鮮則稱為**歲畫**（[諺文](../Page/諺文.md "wikilink")：****，[朝鮮漢字](../Page/朝鮮漢字.md "wikilink")：****），越南稱為**幀節**（，[漢喃](../Page/漢喃.md "wikilink")：****）。

## 歷史

年畫起源於[中國](../Page/中國.md "wikilink")，作为一种正式的民间画种，年画大约始于[五代](../Page/五代.md "wikilink")[北宋](../Page/北宋.md "wikilink")，其渊源却可以上推至[秦](../Page/秦.md "wikilink")[汉或更早的](../Page/汉.md "wikilink")[驱鬼](../Page/驱鬼.md "wikilink")、[避邪之类的](../Page/避邪.md "wikilink")[守护神门画](../Page/门神.md "wikilink")，旧称“纸画”、“纸片”与“画张”等。

[宋代](../Page/宋代.md "wikilink")[镂版雕印的发展以及春节庆祝活动的丰富促使年画得到发展](../Page/镂版雕印.md "wikilink")，题材丰富起来，有[风俗](../Page/风俗.md "wikilink")、[仕女](../Page/仕女.md "wikilink")、娃娃、[戏曲等](../Page/戏曲.md "wikilink")。

随着[明代中后期](../Page/明.md "wikilink")[版画的兴盛](../Page/版画.md "wikilink")，彩色套印技术的成熟大大促进了木版年画的绘制与销行。明清时期可谓是木版年画的鼎盛期。

[清代末叶开始流行石印及胶版印刷的年画](../Page/清.md "wikilink")，盛行“月份牌年画”，主要集中在[上海](../Page/上海.md "wikilink")、[天津等城市](../Page/天津.md "wikilink")。

如今传统雕版年画比较少见，更为常见的是使用电脑技术绘制印刷的年画，题材也不仅限于传统的锦鲤、娃娃、财神等，例如在中国大陆的市场上经常可以见到[十大元帅和](../Page/十大元帅.md "wikilink")[中共领导人题材的年画](../Page/党和国家领导人.md "wikilink")。

## 現代年畫

現在，年画更加成了一种为群众所喜闻乐见的艺术，无论在题材上，还是在表现手法上都日趋丰富。

## 各地年畫

### 中國的年畫

作为年节装饰绘画，年画的内容多为「表吉祥、志喜庆」一类，形式上则是红火活泼，单纯明快，色彩亦鲜艳明亮，多用单色平涂，题材上则包罗一切具有喜庆色彩的风俗生活，新闻轶事、传统戏曲[小说](../Page/小说.md "wikilink")、民间故事等，其创作与群众生活密切相关，地区特色显著。其中，以[天津的](../Page/天津.md "wikilink")[杨柳青年画与](../Page/杨柳青年画.md "wikilink")[江苏](../Page/江苏.md "wikilink")[苏州](../Page/苏州.md "wikilink")[桃花坊年画以及](../Page/桃花坊年画.md "wikilink")[山东](../Page/山东.md "wikilink")[潍坊的](../Page/潍坊.md "wikilink")[杨家埠年画著称于世](../Page/杨家埠年画.md "wikilink")。

中国现存最早的年画是[宋朝版](../Page/宋朝.md "wikilink")《隋朝窈窕呈倾国之芳容图》，画的是[王昭君](../Page/王昭君.md "wikilink")、[赵飞燕](../Page/赵飞燕.md "wikilink")、[班姬](../Page/班姬.md "wikilink")、[绿珠](../Page/绿珠.md "wikilink")，俗称《四美图》\[1\]。

### 朝鮮的年畫

稱為**歲畫**，有印於白紙上的黑白版畫，也有彩色顏料繪畫者，常見有龍、虎、鹿、喜鵲等吉祥動物圖案。

### 越南的年畫

以兒童、風俗生活為主，色彩鮮豔，有些繪在[木板上](../Page/木.md "wikilink")。

### 日本的年畫

有[浮世繪](../Page/浮世繪.md "wikilink")、[水墨畫等形式](../Page/水墨畫.md "wikilink")，浮世繪多以[日本](../Page/日本.md "wikilink")[神祇如](../Page/神祇.md "wikilink")[七福神為題材](../Page/七福神.md "wikilink")，水墨畫則常見[歲寒三友](../Page/歲寒三友.md "wikilink")（[松](../Page/松.md "wikilink")、[竹](../Page/竹.md "wikilink")、[梅](../Page/梅.md "wikilink")）等。

## 参见

[Wuqiang3.JPG](https://zh.wikipedia.org/wiki/File:Wuqiang3.JPG "fig:Wuqiang3.JPG")

  - [木版年画](../Page/木版年画.md "wikilink")
  - [天津](../Page/天津.md "wikilink")[杨柳青年画](../Page/杨柳青年画.md "wikilink")
  - [江苏](../Page/江苏.md "wikilink")[苏州](../Page/苏州.md "wikilink")[桃花塢年畫](../Page/桃花塢年畫.md "wikilink")
  - [山东](../Page/山东.md "wikilink")[潍坊的](../Page/潍坊.md "wikilink")[杨家埠年画](../Page/杨家埠年画.md "wikilink")
  - [陕西](../Page/陕西.md "wikilink")[凤翔年画](../Page/凤翔年画.md "wikilink")
  - [陕西](../Page/陕西.md "wikilink")[汉中年画](../Page/汉中年画.md "wikilink")
  - [湖南](../Page/湖南.md "wikilink")[邵阳](../Page/邵阳.md "wikilink")[滩头年画](../Page/滩头年画.md "wikilink")
  - [山西](../Page/山西.md "wikilink")[平阳年画](../Page/平阳年画.md "wikilink")
  - [河北](../Page/河北.md "wikilink")[武强年画](../Page/武强年画.md "wikilink")
  - [河南](../Page/河南.md "wikilink")[朱仙镇年画](../Page/朱仙镇.md "wikilink")
  - [广东](../Page/广东.md "wikilink")[佛山年画](../Page/佛山年画.md "wikilink")

## 参考文献

  - 洪长泰：〈[重绘中国：新中国成立初期的年画运动与农民的抵制](http://www.nssd.org/articles/article_read.aspx?id=669061776)〉。

{{-}}

[年画](../Category/年画.md "wikilink")
[Category:新春習俗](../Category/新春習俗.md "wikilink")

1.  [年画收藏
    缘何墙里开花墙外香](http://news.163.com/11/0121/04/6QT4L48B00014AED.html)2011-01-21