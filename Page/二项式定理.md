[Pascal's_triangle_5.svg](https://zh.wikipedia.org/wiki/File:Pascal's_triangle_5.svg "fig:Pascal's_triangle_5.svg")出现在[杨辉三角](../Page/杨辉三角.md "wikilink")（帕斯卡三角）中。除边缘的数字外，其他每一个数都为其上方两数之和。\]\]
在[初等代數中](../Page/初等代數.md "wikilink")，**二项式定理**（）描述了[二项式的](../Page/二项式.md "wikilink")[幂的代数展开](../Page/幂.md "wikilink")。根据该定理，可以将两个数之和的整数次幂诸如(*x* + *y*)<sup>*n*</sup>
展开为类似 *ax*<sup>*b*</sup>*y*<sup>*c*</sup> 项之和的恒等式，其中*b*、*c*均为非负整数且
*n*}}。系数*a*是依赖于 \(n\) 和*b*的正整数。当某项的指数为1时，通常略去不写。例如：\[1\]

\((x+y)^4 \;=\; x^4 \,+\, 4 x^3y \,+\, 6 x^2 y^2 \,+\, 4 x y^3 \,+\, y^4.\)

*ax*<sup>*b*</sup>*y*<sup>*c*</sup>
中的系数*a*被称为[二项式系数](../Page/二项式系数.md "wikilink")，记作
\(\tbinom nb\) 或
\(\tbinom nc\)（二者值相等）。二项式定理可以推广到任意实数次幂，即**广义二项式定理**\[2\]。

## 历史

二项式系数的三角形排列通常被认为是法国数学家[布莱兹·帕斯卡的贡献](../Page/布莱兹·帕斯卡.md "wikilink")，他在17世纪描述了这一现象\[3\]。但早在他之前，就曾有数学家进行类似的研究。例如，古希腊数学家[欧几里得于公元前](../Page/欧几里得.md "wikilink")4世纪提到了指数为2的情况\[4\]\[5\]。公元前三世纪，印度数学家[青目探讨了更高阶的情况](../Page/青目.md "wikilink")。[帕斯卡三角形的雏形于](../Page/帕斯卡三角形.md "wikilink")10世纪由印度数学家[大力羅摩发现](../Page/大力羅摩.md "wikilink")。在同一时期，波斯数学家\[6\]和数学家兼诗人[歐瑪爾·海亞姆得到了更为普遍的二项式定理的形式](../Page/歐瑪爾·海亞姆.md "wikilink")。13世纪，中国数学家[杨辉也得到了类似的结果](../Page/杨辉.md "wikilink")\[7\]。用[数学归纳法的原始形式给出了二项式定理和](../Page/数学归纳法.md "wikilink")[帕斯卡三角形](../Page/帕斯卡三角形.md "wikilink")([巴斯卡三角形](../Page/巴斯卡三角形.md "wikilink"))的有关证明\[8\]。[艾萨克·牛顿勋爵将二项式定理的系数推广到有理数](../Page/艾萨克·牛顿.md "wikilink")\[9\]。

## 定理的陈述

根据此定理，可以将 *x* + *y* 的任意次幂展开成和的形式

\[(x+y)^n = {n \choose 0}x^n y^0 + {n \choose 1}x^{n-1}y^1 + {n \choose 2}x^{n-2}y^2 + \cdots + {n \choose n-1}x^1 y^{n-1} + {n \choose n}x^0 y^n,\]
其中每个 \(\tbinom nk\)
为一个称作[二项式系数的特定正整数](../Page/二项式系数.md "wikilink")，其等於\(\frac{n!}{k!(n-k)!}\)。这个公式也称**二项式公式**或**二项恒等式**。使用[求和符号](../Page/求和符号.md "wikilink")，可以把它写作

\[(x+y)^n = \sum_{k=0}^n {n \choose k}x^{n-k}y^k = \sum_{k=0}^n {n \choose k}x^{k}y^{n-k}.\]
后面的表达式只是将根据 *x* 与 *y* 的对称性得出的，通过比较发现公式中的二项式系数也是对称的。 二项式定理的一个变形是用 1 来代换
*y* 得到的，所以它只涉及一个[变量](../Page/变量.md "wikilink")。在这种形式中，公式写作

\[(1+x)^n = {n \choose 0}x^0 + {n \choose 1}x^1 + {n \choose 2}x^2 + \cdots + {n \choose {n-1}}x^{n-1} + {n \choose n}x^n,\]
或者等价地

\[(1+x)^n = \sum_{k=0}^n {n \choose k}x^k.\]

{{-}}

## 几何释义

[binomial_theorem_visualisation.svg](https://zh.wikipedia.org/wiki/File:binomial_theorem_visualisation.svg "fig:binomial_theorem_visualisation.svg")
对于正值a和b，二项式定理，在n = 2时是在几何上的明显事实，边为的正方形，可以切割成1个边为a的正方形，1个边为b的正方形，和2个边为a和b的长方形。对于n = 3，定理陈述了边为的立方体，可以切割成1个边为a的立方体，1个边为b的立方体，3个a×a×b长方体，和3个a×b×b长方体。

在[微积分中](../Page/微积分.md "wikilink")，此图解也给出[导数](../Page/导数.md "wikilink")
\((x^n)'=nx^{n-1}\)的几何证明\[10\]。设\(a=x\)且\(b=\Delta x\)，将b解释为a的[无穷小量改变](../Page/无穷小量.md "wikilink")，则此图解将无穷小量改变，显示为n维[超立方体](../Page/超立方体.md "wikilink")
\((x+\Delta x)^n\)：

\[(x+\Delta x)^n = x^n + nx^{n-1}\Delta x + \tbinom{n}{2}x^{n-2}(\Delta x)^2 + \cdots.\]
其中（针对\(\Delta x\)的）线性项的系数是\(nx^{n-1}\)，将公式代入采用[差商的](../Page/差商.md "wikilink")[导数定义并取极限](../Page/导数定义.md "wikilink")，意味着忽略高阶项\((\Delta x)^2\)和更高者，产生公式：\((x^n)'=nx^{n-1}\)。若再进行积分，这对应于应用[微积分基本定理](../Page/微积分基本定理.md "wikilink")，则得到[卡瓦列里求积公式](../Page/卡瓦列里求积公式.md "wikilink")：\(\textstyle{\int x^{n-1}\,dx = \tfrac{1}{n} x^n}\)。

## 證明

### 數學歸納法

當\(n=1\)，

\[(a+b)^1 = \sum_{k=0}^1 { 1 \choose k } a^{1-k}b^k = { 1 \choose 0 }a^1b^0+{ 1 \choose 1 }a^0b^1 = a+b\]

假設二项展开式在 \(n=m\) 時成立。若\(n=m+1\)，

\[(a+b)^{m+1}\] \(=\) \(a(a+b)^m + b(a+b)^m\)

\[=\]
\(a \sum_{k=0}^m { m \choose k } a^{m-k} b^k + b \sum_{j=0}^m { m \choose j } a^{m-j} b^j\)

\[=\]
\(\sum_{k=0}^m { m \choose k } a^{m-k+1} b^k + \sum_{j=0}^m { m \choose j } a^{m-j} b^{j+1}\)
將\(a\)、\(b\)

\[=\]
\(a^{m+1} + \sum_{k=1}^m { m \choose k } a^{m-k+1} b^k + \sum_{j=0}^m { m \choose j } a^{m-j} b^{j+1}\)
取出\(k=0\)的項

\[=\]
\(a^{m+1} + \sum_{k=1}^m { m \choose k } a^{m-k+1} b^k + \sum_{k=1}^{m+1} { m \choose k-1 }a^{m-k+1}b^{k}\)
設\(j = k-1\)

\[=\]
\(a^{m+1} + \sum_{k=1}^m { m \choose k } a^{m-k+1}b^k + \sum_{k=1}^{m} { m \choose k-1 }a^{m+1-k}b^{k} + b^{m+1}\)
取出\(k=m+1\)項

\[=\]
\(a^{m+1} + b^{m+1} + \sum_{k=1}^m \left[ { m \choose k } + { m \choose k-1 } \right] a^{m+1-k}b^k\)
兩者加起

\[=\]
\(a^{m+1} + b^{m+1} + \sum_{k=1}^m { m+1 \choose k } a^{m+1-k}b^k\)
套用[帕斯卡法則](../Page/帕斯卡法則.md "wikilink")

\[=\] \(\sum_{k=0}^{m+1} { m+1 \choose k } a^{m+1-k}b^k\)

### 組合方法

考慮\((a+b)^7=(a+b)(a+b)(a+b)(a+b)(a+b)(a+b)(a+b)\)，共7個括號相乘，從7個括號選出其中的4個括號中的*a*，再從剩餘的3個括號中選出3個*b*相乘，便得一組\(a^4b^3\)，而這樣的選法共有\(\tbinom 74\)種，故總共有\(\tbinom 74\)個\(a^4b^3\)；其他各項同理。

同理，\((a+b)^n=(a+b)(a+b)....(a+b)(a+b)\)，共*n*個括號相乘，從*n*個括號選出其中的*k*個括號中的*a*，再從剩餘的(*n*-*k*)個括號中選出(*n*-*k*)個*b*相乘，便得一組\(a^kb^{n-k}\)，而這樣的選法共有\(\tbinom nk\)種，故總共有\(\tbinom nk\)個\(a^kb^{n-k}\)；其他各項同理。

### 不盡相異物排列方法

考慮\((a+b)^7=(a+b)(a+b)(a+b)(a+b)(a+b)(a+b)(a+b)\)，每一個括號可以出*a*或出*b*，而最後要有4個*a*、3個*b*相乘，這形同*aaaabbb*的「不盡相異物排列」，其方法數為\(\frac{7!}{4! \times3!}\)，恰好等於\(\tbinom 74\)；其他各項同理。

同理，\((a+b)^n=(a+b)(a+b)....(a+b)(a+b)\)，每一個括號可以出*a*或出*b*，而最後要有*k*個*a*、(*n*-*k*)個*b*相乘，這形同*aa....aabb....bb*的「不盡相異物排列」，其方法數為\(\frac{n!}{k! \times(n-k)!}\)，恰好等於\(\tbinom nk\)；其他各項同理。

### 一般形式的证明

通常二项式定理可以直接使用[泰勒公式进行证明](../Page/泰勒公式.md "wikilink"). 下面的方法不使用泰勒公式

> 设
>
> \(f(x)=(1+x)^a\), \(g(x)=\sum_{k=0}^{\infty}{a \choose k}x^k\). 注意只有当
> \(|x|<1\)时上述两个函数才收敛
>
>   - 首先证明 \(f(x)\)收敛于1. 这里省略
>
> <!-- end list -->
>
>   - 之后, 易得\(f(x)\)满足微分方程: \((1+x)f'(x) = a f(x)\). 用求导的一般方法就能得到这个结论,
>     这里省略
>
> <!-- end list -->
>
>   - 再证明 \(g(x)\)亦满足上述微分方程:
>
> \(g(x)=1+ \sum_{k=1}^{\infty}{a \choose k}x^k\)
>
> > \(\begin{align}
> > g'(x) & = \sum_{k=1}^{\infty}{a \choose k}k x^{k-1} \\
> >       & = \sum_{k=0}^{\infty}{a\choose {k+1}}(k+1) x^{k} \\
> >       & = \sum_{k=0}^{\infty}{a \choose k}(a-k) x^k \\
> > \end{align}\)
> >
> > 因为
> >
> > \(\begin{align}
> > {a \choose {k+1} }(k+1) & = \frac{(a)(a-1)\cdots(a - k + 1)(a - k)}{(k+1)!}(k+1) \\
> >                    & = \frac{(a)(a-1)\cdots(a - k + 1)(a - k)}{k!} \\
> >                    & = {a \choose k}(a-k)
> > \end{align}\)
>
> 于是
> \((1+x)g'(x) = g'(x) + x \sum_{k=1}^{\infty}{a \choose k}k x^{k-1}\)
>
> \[=\sum_{k=0}^{\infty}{a \choose k}(a-k) x^k + \sum_{k=1}^{\infty}{a \choose k}k x^{k}\]
>
> \[=\sum_{k=0}^{\infty}{a \choose k}(a-k) x^k + \sum_{k=0}^{\infty}{a \choose k}k x^{k}\]
>
> \[=\sum_{k=0}^{\infty}{a \choose k}x^k (a-k+k)\]
>
> \[=a \sum_{k=0}^{\infty}{a \choose k}x^k\]
>
> \[=a \cdot g(x)\]

因为
\(\sum_{k=0}^{\infty}{a \choose k}k x^{k}={a \choose 0} \cdot 0x^0+ \sum_{k=1}^{\infty}{a \choose k}k x^{k}=\sum_{k=1}^{\infty}{a \choose k}k x^{k}\)

\[\frac{g'(x)}{g(x)}=\frac{a}{1+x}\]

\[\because \frac{f'(x)}{f(x)}=\frac{a}{1+x}\]

\[\therefore \frac{f'(x)}{f(x)}=\frac{g'(x)}{g(x)}\]

\[g'(x)f(x)=f'(x)g(x)\]

  - 根据[除法定则](../Page/除法定则.md "wikilink")，\(\frac{d}{dx} \left(\frac{g(x)}{f(x)}\right)=\frac{g'(x)f(x)-f'(x)g(x)}{(f(x))^2}=0\)

<!-- end list -->

  - 根据[拉格朗日中值定理](../Page/拉格朗日中值定理.md "wikilink")，\(\frac{g(x)}{f(x)}\)是[常数函數](../Page/常数函數.md "wikilink").

\[\frac{g(x)}{f(x)}=\frac{g(0)}{f(0)}=1\]

\[f(x)=g(x)\]

## 应用

牛顿以二项式定理作为基石发明出了[微积分](../Page/微积分.md "wikilink")\[11\]
。其在初等数学中应用主要在于一些粗略的分析和估计以及证明恒等式等。

### 证明组合恒等式

二项式定理给出的系数可以视为组合数 \({n \choose k}\) 的另一种定义。 因此二项式展开与组合数的关系十分密切。
它常常用来证明一些组合恒等式。比如证明
\(\sum _{k=0} ^n {n \choose k}^2 = {2n \choose n}\)

可以考虑恒等式 \((1+x)^n (1+x)^n = (1+x)^{2n}\)。 展开等式左边得到：
\(\sum_{i=0}^n \sum_{j=0}^n { n \choose i} {n \choose j} x^i x^j\)。
注意这一步使用了有限求和与乘积可以交换的性质。 同时如果展开等式右边可以得到
\(\sum_{k=0}^{2n}  { 2n \choose k} x^k\)。 比较两边幂次为 \(k\) 的项的系数可以得到:
\(\sum_{i=0} ^k { n \choose i} {n \choose k - i} = {2n \choose k}\)。 令
\(k=n\)，并注意到 \({ n \choose i} = {n \choose n - i}\) 即可得到所要证明的结论。

### 多倍角恒等式

在[复数中](../Page/复数_\(数学\).md "wikilink")，二项式定理可以與[棣莫弗公式結合](../Page/棣莫弗公式.md "wikilink")，成為[多倍角公式](../Page/三角恒等式#多倍角公式.md "wikilink")\[12\]。根據棣莫弗公式：

\[\cos\left(nx\right)+i\sin\left(nx\right) = \left(\cos x+i\sin x\right)^n.\,\]
通過使用二项式定理，右邊的表達式可以擴展為

\[\left(\cos x+i\sin x\right)^2 = \cos^2 x + 2i \cos x \sin x - \sin^2 x,\]
由棣莫弗公式，实部与虚部对应，能夠得出

\[\cos(2x) = \cos^2 x - \sin^2 x \quad\text{and}\quad\sin(2x) = 2 \cos x \sin x,\]
即二倍角公式。同樣，因為

\[\left(\cos x+i\sin x\right)^3 = \cos^3 x + 3i \cos^2 x \sin x - 3 \cos x \sin^2 x - i \sin^3 x,\]
所以藉棣莫弗公式，能夠得出

\[\cos(3x) = \cos^3 x - 3 \cos x \sin^2 x \quad\text{and}\quad \sin(3x) = 3\cos^2 x \sin x - \sin^3 x.\]
整體而言，多倍角恒等式可以寫作

\[\cos(nx) = \sum_{k\text{ even}} (-1)^{\frac{k}{2}} {n \choose k}\cos^{n-k} x \sin^k x\]
和

\[\sin(nx) = \sum_{k\text{ odd}} (-1)^{\frac{k-1}{2}} {n \choose k}\cos^{n-k} x \sin^k x.\]

### e级数

數學常數[e的定義爲下列極限值](../Page/e_\(數學常數\).md "wikilink")：\[13\]

\[e = \lim_{n\to\infty} \left(1 + \frac{1}{n}\right)^n.\]

使用二项式定理能得出

\[\left(1 + \frac{1}{n}\right)^n = 1 + {n \choose 1}\frac{1}{n} + {n \choose 2}\frac{1}{n^2} + {n \choose 3}\frac{1}{n^3} + \cdots + {n \choose n}\frac{1}{n^n}.\]

第*k*项之總和為

\[{n \choose k}\frac{1}{n^k} \;=\; \frac{1}{k!}\cdot\frac{n(n-1)(n-2)\cdots (n-k+1)}{n^k}\]

因為*n* → ∞，右邊的表达式趋近1。因此

\[\lim_{n\to\infty} {n \choose k}\frac{1}{n^k} = \frac{1}{k!}.\]

這表明e可以表示为\[14\]\[15\]

\[e = \sum_{k=0}^\infty\frac{1}{k!}=\frac{1}{0!} + \frac{1}{1!} + \frac{1}{2!} + \frac{1}{3!} + \cdots.\]

## 推广

该定理可以推广到对任意实数次幂的展开，即所谓的牛顿**广义二项式定理**：

\((x + y)^\alpha = \sum _{k=0}^\infty {\alpha \choose k} x^{\alpha - k} y^k\)。其中\({\alpha \choose k} = \frac{\alpha (\alpha-1) ... (\alpha - k +1)}{k!} = \frac{(\alpha)_k}{k!}\)。

### 多项式展开

对于多元形式的多项式展开，可以看做二项式定理的推广：\[16\]\[17\]
\(\left ( x_1+x_2+...+x_n \right )^{k}=\sum_{\alpha_1+\alpha_2+...+\alpha _n=k}\frac{k!}{\alpha _1!...\alpha _n!}x_1^{\alpha _1}...x_n^{\alpha _n}\).

证明：


[数学归纳法](../Page/数学归纳法.md "wikilink")。对元数n做归纳：
当n=2时，原式为二项式定理，成立。
假设对n-1元成立，则：

\[\left ( x_1+x_2+...+x_n \right )^{k}\]

\[=\] \(((x_1+x_2+...+x_{n-1})+x_n)^{k}\)

\[=\]
\(\sum_{\alpha _n=0}^{k}\frac{k!}{\alpha _n!\left ( k-\alpha_n \right )!}\left ( x_1+x_2+...+x_{n-1} \right )^{k-\alpha _n}x_n^{\alpha _n}\)

\[=\]
\(\sum_{\alpha _n=0}^{k}\frac{k!}{\alpha _n!\left ( k-\alpha_n \right )!}\sum_{\alpha_1+\alpha_2+...+\alpha _{n-1}=k-\alpha _n}\frac{\left ( k-\alpha _n \right )!}{\alpha _1!...\alpha _{n-1}!}x_1^{\alpha _1}...x_{n-1}^{\alpha _{n-1}}x_n^{\alpha _n}\)

\[=\]
\(\sum_{\alpha_1+\alpha_2+...+\alpha _n=k}\frac{k!}{\alpha _1!...\alpha _n!}x_1^{\alpha _1}...x_n^{\alpha _n}\)证毕

## 参见

  - [二項分佈](../Page/二項分佈.md "wikilink")
  - [組合](../Page/組合.md "wikilink")
  - [立方根](../Page/立方根.md "wikilink")
  - [平方根](../Page/平方根.md "wikilink")
  - [牛顿法](../Page/牛顿法.md "wikilink")
  - [多项式定理](../Page/多项式定理.md "wikilink")
  - [负二项分布](../Page/负二项分布.md "wikilink")
  - [杨辉三角形](../Page/杨辉三角形.md "wikilink")
  - [斯特靈公式](../Page/斯特靈公式.md "wikilink")

## 参考文獻

## 參考書目

  -
  - Barth, Nils R. (November 2004). "Computing Cavalieri's Quadrature
    Formula by a Symmetry of the n-Cube". The American Mathematical
    Monthly (Mathematical Association of America) 111 (9): 811–813.
    <doi:10.2307/4145193>. ISSN 0002-9890. JSTOR 4145193, [author's
    copy](https://web.archive.org/web/20120320075331/http://nbarth.net/math/papers/barth-01-cavalieri.pdf),
    [further remarks and
    resources](https://web.archive.org/web/20120320075335/http://nbarth.net/math/papers/)

  -
## 外部链接

  - [Binomial
    Theorem](http://demonstrations.wolfram.com/BinomialTheorem/) -
    [史蒂芬·沃尔夫勒姆](../Page/史蒂芬·沃尔夫勒姆.md "wikilink")
  - ["Binomial Theorem
    (Step-by-Step)"](http://demonstrations.wolfram.com/BinomialTheoremStepByStep/)
    by Bruce Colletti and Jeff Bryant, [Wolfram
    演示项目](../Page/Wolfram_演示项目.md "wikilink"), 2007.
  - [The Binomial Theorem - Interactive
    Mathematics](http://hyperphysics.phy-astr.gsu.edu/hbase/alg3.html)
  - [Binomial Expansion -
    HyperPhysics](https://web.archive.org/web/20150416041521/http://www.themathpage.com/aPreCalc/binomial-theorem.htm)

[E](../Category/代数定理.md "wikilink") [E](../Category/组合数学.md "wikilink")
[Category:阶乘与二项式主题](../Category/阶乘与二项式主题.md "wikilink")

1.  [Binomial Expansions - leeds
    uk](http://www1.maths.leeds.ac.uk/~rathjen/CIVE1619ch4.pdf)

2.  Roman, Steven "The Umbral Calculus", Dover Publications, 2005, ISBN
    0-486-44129-3

3.  Devlin, Keith, *The Unfinished Game: Pascal, Fermat, and the
    Seventeenth-Century Letter that Made the World Modern*, Basic Books;
    1 edition (2008), ISBN 978-0-465-00910-7, p. 24.

4.  [Binomial Theorem - wolfram
    mathworld](http://mathworld.wolfram.com/BinomialTheorem.html)

5.  [The Story of the Binomial Theorem by J. L.
    Coolidge](http://www.jstor.org/pss/2305028), *The American
    Mathematical Monthly* **56**:3 (1949), pp. 147–157

6.

7.

8.
9.  Bourbaki: *History of mathematics*

10.

11.

12. [Multiple-Angle Formulas -
    MathWorld](http://mathworld.wolfram.com/Multiple-AngleFormulas.html)

13. [The Constant e - NDE/NDT Resource
    Center](https://www.nde-ed.org/EducationResources/Math/Math-e.htm)

14. [Series - NTEC](http://www.ntec.ac.uk/Maths/PDFs/s6_series.pdf)

15. [Encyclopedic Dictionary of
    Mathematics](../Page/Encyclopedic_Dictionary_of_Mathematics.md "wikilink")
    142.D

16. [多項式定理的新證明及其展開 -
    佛山科学技术学院信息科学与数学系](http://www.cnki.com.cn/Article/CJFDTotal-FSDX201206006.htm)

17.