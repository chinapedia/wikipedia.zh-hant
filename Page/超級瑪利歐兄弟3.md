[SUPER_MARIO_BROS._3.png](https://zh.wikipedia.org/wiki/File:SUPER_MARIO_BROS._3.png "fig:SUPER_MARIO_BROS._3.png")
，是[任天堂於](../Page/任天堂.md "wikilink")1988年10月23日首次在[FC遊戲機](../Page/FC遊戲機.md "wikilink")（俗稱「紅白機」）上推出的[電子遊戲](../Page/電子遊戲.md "wikilink")。此遊戲為「超級瑪利歐兄弟」系列的第三部作品，在[日本國內熱銷了](../Page/日本.md "wikilink")384萬套，是日本紅白機遊戲史上銷售量排名第二的作品，僅次於系列作的第一作《[超級瑪利歐兄弟](../Page/超級瑪利歐兄弟.md "wikilink")》。全世界的總銷售量為1728萬套，2003年在[GBA推出重製版本](../Page/GBA.md "wikilink")。

## 玩法

《超级马里奥兄弟3》是一款2D，横向卷轴的平台游戏。玩家可以控制屏幕的角色—马里奥或者路易吉。本游戏和系列前作《[超级马里奥兄弟](../Page/超级马里奥兄弟.md "wikilink")》、《[超级马里奥兄弟2](../Page/超级马里奥兄弟2.md "wikilink")》和《[超级马里奥兄弟USA](../Page/超级马里奥兄弟USA.md "wikilink")》相类似，并且增加了新的元素。除了在前几作中的跑步和跳跃之外，玩家可以从斜坡滑行、捡起和投掷特殊石块和自由地爬上和爬下藤蔓。此外，马里奥在提升能力之后，能够飞行和飘浮。\[1\]游戏世界包括8个“王国”，每个可以分成多个关卡。八个世界具有各自明显的视觉主题。

## USA/日本版的分別

  - USA版修正了日本版一部份的Bug。
  - USA版火之花、超級葉子、狸貓裝、青蛙裝、鐵鎚兄弟裝、靴子裝狀態時受傷只會變回超級磨菇狀態，日本版則變回最初狀態。
  - 在SFC版「超級瑪利歐兄弟合輯」以後，不論日本版以及USA版，火之花、超級葉子、狸貓裝、青蛙裝、鐵鎚兄弟裝、靴子裝狀態時受傷都只會變回超級磨菇狀態。

## 外部链接

  - [スーパーマリオブラザーズ3](http://www.nintendo.co.jp/ngc/sms/history/sm3/index.html)任天堂

[Category:超级马里奥系列电子游戏](../Category/超级马里奥系列电子游戏.md "wikilink")
[Category:1988年电子游戏](../Category/1988年电子游戏.md "wikilink")
[Category:任天堂遊戲](../Category/任天堂遊戲.md "wikilink")
[Category:红白机游戏](../Category/红白机游戏.md "wikilink") [Category:Wii
Virtual Console游戏](../Category/Wii_Virtual_Console游戏.md "wikilink")
[Category:任天堂3DS Virtual
Console游戏](../Category/任天堂3DS_Virtual_Console游戏.md "wikilink")
[Category:動作遊戲](../Category/動作遊戲.md "wikilink")

1.