**延岡市**（）是位于[日本](../Page/日本.md "wikilink")[宮崎縣北部的](../Page/宮崎縣.md "wikilink")[城市](../Page/城市.md "wikilink")，[人口約](../Page/人口.md "wikilink")13萬人，為宮崎縣內人口數量第三多的城市（次于[宮崎市和](../Page/宮崎市.md "wikilink")[都城市](../Page/都城市.md "wikilink")），也是宮崎縣北部的主要城市。

日本的[化學材料](../Page/化學材料.md "wikilink")[公司](../Page/公司.md "wikilink")[旭化成創業初期的工廠設於此](../Page/旭化成.md "wikilink")，也帶動了本地的發展，因此延岡也被稱為[企業城下町](../Page/企業城下町.md "wikilink")。

## 历史

在[平安時代](../Page/平安時代.md "wikilink")，行政區劃為[臼杵郡](../Page/臼杵郡.md "wikilink")[英多鄉](../Page/英多鄉.md "wikilink")，原由[土持氏治理](../Page/土持氏.md "wikilink")，直到1578年4月10日，[土持親成在](../Page/土持親成.md "wikilink")[松尾城被](../Page/松尾城.md "wikilink")[豐後國的](../Page/豐後國.md "wikilink")[大友宗麟擊敗才中止](../Page/大友宗麟.md "wikilink")。\[1\]

1587年[豐臣秀吉在](../Page/豐臣秀吉.md "wikilink")[九州征伐後](../Page/九州征伐.md "wikilink")，成為[日向國下的](../Page/日向國.md "wikilink")[縣藩](../Page/縣藩.md "wikilink")（後改名為延岡藩），並築縣城（後來的[延岡城](../Page/延岡城.md "wikilink")），因而開始以城下町的形式發展。在[幕府末期](../Page/幕府.md "wikilink")，屬於內藤家的封地，[石高為七萬石](../Page/石高.md "wikilink")。\[2\]

[明治時期設置宮崎縣後](../Page/明治時期.md "wikilink")，因縣政府設於[宮崎市](../Page/宮崎市.md "wikilink")，失去政治中心地位的延岡，發展速度便不如宮崎市，因此過去曾為延岡藩主的[內藤氏及藩士將許多自有的特權釋放出來](../Page/內藤氏.md "wikilink")，促成延岡的工業發展。

1923年[野口遵所成立的](../Page/野口遵.md "wikilink")[日本窒素肥料將工廠設於此](../Page/日本窒素肥料.md "wikilink")，使延岡成為宮崎縣內的工業城市，並帶動快速發展。[二次大戰後](../Page/二次大戰.md "wikilink")，由於[財閥解體](../Page/財閥解體.md "wikilink")，延岡的工廠改組後由[旭化成繼續經營](../Page/旭化成.md "wikilink")；至1951年時，轄區內一半的人口、市議會三分之一的議員都與旭化成有關係，而市府三分之二的稅收也都來與旭化成有關的活動，因此被稱為「企業城下町」。但近年來旭化成在延岡的經營比重逐漸縮小之後，人口也開始跟著減少。

### 年表

  - 1889年4月1日：實施[町村制](../Page/町村制.md "wikilink")，現在的轄區分屬[東臼杵郡延岡町](../Page/東臼杵郡.md "wikilink")、[恒富村](../Page/恒富村.md "wikilink")、[岡富村](../Page/岡富村.md "wikilink")、[伊形村](../Page/伊形村.md "wikilink")、[東海村](../Page/東海村_\(宮崎縣\).md "wikilink")、[南方村](../Page/南方村.md "wikilink")、[南浦村](../Page/南浦村.md "wikilink")、北方村、北浦村、北川村。
  - 1930年4月1日：延岡町、恒富村、岡富村合併為新設置的延岡町。
  - 1933年2月11日：改制為**延岡市**。\[3\]
  - 1936年10月25日：伊形村和東海村被併入。
  - 1955年4月1日：南方村和南浦村被併入。
  - 1970年11月3日：北方村改制為[北方町](../Page/北方町_\(宮崎縣\).md "wikilink")。
  - 1972年11月1日：北浦村和北川村同時改制為[北浦町和](../Page/北浦町_\(宮崎縣\).md "wikilink")[北川町](../Page/北川町.md "wikilink")。
  - 2006年2月20日：北浦町、北方町被併入。
  - 2007年3月31日：北川町被併入。

<table>
<thead>
<tr class="header">
<th><p>1889年以前</p></th>
<th><p>1889年5月1日</p></th>
<th><p>1889年 - 1944年</p></th>
<th><p>1945年 - 1954年</p></th>
<th><p>1955年 - 1988年</p></th>
<th><p>19891年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>延岡町</p></td>
<td><p>1930年4月1日<br />
合併為延岡町</p></td>
<td><p>1933年2月11日<br />
改制為延岡市</p></td>
<td><p>延岡市</p></td>
<td><p>延岡市</p></td>
<td><p>延岡市</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>岡富村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>恒富村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>伊形村</p></td>
<td><p>1936年10月25日<br />
併入延岡市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>東海村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>南方村</p></td>
<td><p>1955年4月1日<br />
併入延岡市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>南浦村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>北方村</p></td>
<td><p>1970年11月3日<br />
改制為北方町</p></td>
<td><p>2006年2月20日<br />
併入延岡市</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>北浦村</p></td>
<td><p>1972年11月1日<br />
改制為北浦町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>北川村</p></td>
<td><p>1972年11月1日<br />
改制為北川町</p></td>
<td><p>2007年3月31日<br />
併入延岡市</p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 交通

### 鐵路

  - [九州旅客鐵道](../Page/九州旅客鐵道.md "wikilink")
      - [日豐本線](../Page/日豐本線.md "wikilink")：[市棚車站](../Page/市棚車站.md "wikilink")
        - [北川車站](../Page/北川車站.md "wikilink") -
        [日向長井車站](../Page/日向長井車站.md "wikilink") -
        [北延岡車站](../Page/北延岡車站.md "wikilink") -
        [延岡車站](../Page/延岡車站.md "wikilink") -
        [南延岡車站](../Page/南延岡車站.md "wikilink") -
        [旭丘車站](../Page/旭丘車站_\(宮崎縣\).md "wikilink") -
        [土土呂車站](../Page/土土呂車站.md "wikilink")

### 道路

  - [高速道路](../Page/高速道路.md "wikilink")

<!-- end list -->

  - [東九州自動車道](../Page/東九州自動車道.md "wikilink")：[北浦交流道](../Page/北浦交流道.md "wikilink")
    - [須美江交流道](../Page/須美江交流道.md "wikilink") -
    [北川交流道](../Page/北川交流道.md "wikilink") -
    [延岡系統交流道](../Page/延岡系統交流道.md "wikilink") -
    [延岡南交流道](../Page/延岡南交流道.md "wikilink")
  - [九州中央自動車道](../Page/九州中央自動車道.md "wikilink")：[北方交流道](../Page/北方交流道.md "wikilink")
    - [舞野交流道](../Page/舞野交流道.md "wikilink") - 延岡系統交流道

## 觀光資源

### 景點

[Central_Nobeoka_from_MtAtago_200807.jpg](https://zh.wikipedia.org/wiki/File:Central_Nobeoka_from_MtAtago_200807.jpg "fig:Central_Nobeoka_from_MtAtago_200807.jpg")

  - [今山惠比壽神社](../Page/今山惠比壽神社.md "wikilink")
  - [延岡城遺跡](../Page/延岡城.md "wikilink")
  - [南方古墳群](../Page/南方古墳群.md "wikilink")
  - [和田越古戰場](../Page/和田越古戰場.md "wikilink")
  - [行縢山](../Page/行縢山.md "wikilink")・[行縢瀑布](../Page/行縢瀑布.md "wikilink")：[日本瀑布百選之一](../Page/日本瀑布百選.md "wikilink")
  - [愛宕山](../Page/愛宕山.md "wikilink")：[日本夜景100選之一](../Page/日本夜景100選.md "wikilink")、[日本夜景遺産](../Page/日本夜景遺産.md "wikilink")

### 祭典、活動

  - 延岡大師祭
  - 城山鐘祭（毎年6月10日）
  - 熊野神社的歲頂火（離毎年舊曆1月15日最近的星期六）

## 教育

### 大學

  - [九州保健福祉大學](../Page/九州保健福祉大學.md "wikilink")
  - [聖心烏蘇拉學園短期大學](../Page/聖心烏蘇拉學園短期大學.md "wikilink")

### 高等學校

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><dl>
<dt>公立</dt>

</dl>
<ul>
<li><a href="../Page/宮崎縣立延岡高等學校.md" title="wikilink">宮崎縣立延岡高等學校</a></li>
<li><a href="../Page/宮崎縣立延岡星雲高等學校.md" title="wikilink">宮崎縣立延岡星雲高等學校</a></li>
<li><a href="../Page/宮崎縣立延岡青朋高等學校.md" title="wikilink">宮崎縣立延岡青朋高等學校</a></li>
<li><a href="../Page/宮崎縣立延岡工業高等學校.md" title="wikilink">宮崎縣立延岡工業高等學校</a></li>
<li><a href="../Page/宮崎縣立延岡商業高等學校.md" title="wikilink">宮崎縣立延岡商業高等學校</a></li>
</ul></td>
<td><dl>
<dt>私立</dt>

</dl>
<ul>
<li><a href="../Page/尚學館中學校、高等部.md" title="wikilink">尚學館中學校、高等部</a></li>
<li><a href="../Page/延岡學園高等學校.md" title="wikilink">延岡學園高等學校</a></li>
<li><a href="../Page/聖心烏蘇拉学園高等學校.md" title="wikilink">聖心烏蘇拉学園高等學校</a></li>
</ul></td>
</tr>
</tbody>
</table>

## 姊妹、友好都市

### 日本

  - [坂井市](../Page/坂井市.md "wikilink")（[福井縣](../Page/福井縣.md "wikilink")）：由於[延岡藩與](../Page/延岡藩.md "wikilink")[丸岡藩過去曾經先後由](../Page/丸岡藩.md "wikilink")[有馬氏治理](../Page/有馬氏.md "wikilink")，因此延岡市與[丸岡町在](../Page/丸岡町.md "wikilink")1979年10月27日締結為姊妹都市；2006年3月20日丸岡町合併為坂井市之後，兩地於2006年11月22日繼續締結為姊妹市。\[4\]
  - [磐城市](../Page/磐城市.md "wikilink")（[福島縣](../Page/福島縣.md "wikilink")）：由於延岡藩與[磐城藩過去曾經先後由](../Page/磐城藩.md "wikilink")[內藤氏治理](../Page/內藤氏.md "wikilink")，因此延岡市與[磐城市於](../Page/磐城市.md "wikilink")1997年5月3日締結兄弟都市。\[5\]

### 海外

  - [麥德福](../Page/麥德福.md "wikilink")（[美國](../Page/美國.md "wikilink")[麻薩諸塞州](../Page/麻薩諸塞州.md "wikilink")）：兩地於1980年8月29日締結姊妹都市。\[6\]

## 本地出身之名人

  - [福島瑞穂](../Page/福島瑞穂.md "wikilink")：日本[社會民主黨領導人](../Page/社會民主黨_\(日本\).md "wikilink")、[日本參議院](../Page/日本參議院.md "wikilink")[議員](../Page/議員.md "wikilink")、[律師](../Page/律師.md "wikilink")
  - [後藤勇吉](../Page/後藤勇吉.md "wikilink")：日本第一個民間航空器駕駛
  - [NOISY](../Page/NOISY.md "wikilink")：[貝斯手](../Page/貝斯手.md "wikilink")、[歌手](../Page/歌手.md "wikilink")
  - [黒木純司](../Page/黒木純司.md "wikilink")：前職業棒球選手
  - [柳田聖人](../Page/柳田聖人.md "wikilink")：前職業棒球選手
  - [濱田隆士](../Page/濱田隆士.md "wikilink")：古生物學者
  - [松田丈志](../Page/松田丈志.md "wikilink")：[游泳選手](../Page/游泳.md "wikilink")
  - [金子裕則](../Page/金子裕則.md "wikilink")：[創作歌手](../Page/創作歌手.md "wikilink")
  - [谷克二](../Page/谷克二.md "wikilink")：[作家](../Page/作家.md "wikilink")
  - [藤江監物](../Page/藤江監物.md "wikilink")：[延岡藩家老](../Page/延岡藩.md "wikilink")
  - [江尻喜多右衛門](../Page/江尻喜多右衛門.md "wikilink")：延岡藩郡[奉行](../Page/奉行.md "wikilink")
  - [長野誠](../Page/長野誠.md "wikilink")：[極限體能王SASUKE節目的](../Page/極限體能王SASUKE.md "wikilink")[極限體能王全明星](../Page/極限體能王全明星.md "wikilink")、[漁夫](../Page/漁夫.md "wikilink")、「第50金比羅丸」漁船的船長
  - [37代式守伊之助](../Page/式守伊之助_\(37代\).md "wikilink")：[大相撲](../Page/大相撲.md "wikilink")[立行司](../Page/立行司.md "wikilink")
  - [南克幸](../Page/南克幸.md "wikilink")：[排球選手](../Page/排球.md "wikilink")
  - [琴柏谷充隆](../Page/琴柏谷充隆.md "wikilink")：[大相撲](../Page/大相撲.md "wikilink")[力士](../Page/力士.md "wikilink")
  - [酒井瞳](../Page/酒井瞳.md "wikilink")：[藝人](../Page/藝人.md "wikilink")

## 參考資料

## 外部連結

  - [延岡商工會議所](http://www.miyazaki-cci.or.jp/nobeoka/)

  - [延岡觀光協會](http://www.nobekan.jp/)

  - [延岡情報站](https://web.archive.org/web/20080917182238/http://www.nobeokan.com/)

  - [延岡.com](http://www.nobeoka-dotto.com/)

  -
<!-- end list -->

1.
2.
3.
4.
5.
6.