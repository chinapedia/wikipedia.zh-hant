**司馬倫**（），字**子彝**，西晋的第三位皇帝，也是[西晉](../Page/西晉.md "wikilink")[八王之亂中其中一王](../Page/八王之亂.md "wikilink")。[司馬懿第九子](../Page/司馬懿.md "wikilink")，母[柏夫人](../Page/柏夫人.md "wikilink")。

## 生平

### 早年

司马伦出生于240年，[曹魏](../Page/曹魏.md "wikilink")[嘉平二年](../Page/嘉平_\(曹芳\).md "wikilink")（250年）封安樂亭侯\[1\]，後改封東安子，拜[諫議大夫](../Page/諫議大夫.md "wikilink")。

[晉武帝](../Page/晉武帝.md "wikilink")[司馬炎建國後](../Page/司馬炎.md "wikilink")，封[琅邪王](../Page/琅邪王.md "wikilink")。時坐使散騎將劉緝買工所將盜御裘，[廷尉杜友正緝棄市](../Page/廷尉.md "wikilink")，倫當與緝同罪。晉武帝以倫為血親，下詔[赦免](../Page/赦免.md "wikilink")。歷任行東[中郎將](../Page/中郎將.md "wikilink")、宣威將軍。[咸寧三年](../Page/咸寧_\(司馬炎\).md "wikilink")（277年），改封[趙王](../Page/趙王.md "wikilink")，遷[平北將軍](../Page/平北將軍.md "wikilink")、督[鄴城守事](../Page/鄴城.md "wikilink")，後進[安北將軍](../Page/安北將軍.md "wikilink")。

### 惠帝時期

[元康初年](../Page/元康_\(晋惠帝\).md "wikilink")，改任[征西將軍](../Page/征西將軍.md "wikilink")、開府儀同三司，守鎮[關中](../Page/關中.md "wikilink")。因他刑賞不公引致[氐](../Page/氐.md "wikilink")[羌反叛](../Page/羌.md "wikilink")，於是被召回京，拜[車騎將軍](../Page/車騎將軍.md "wikilink")、[太子太傅](../Page/太子太傅.md "wikilink")。深交皇后[賈南風](../Page/賈南風.md "wikilink")（[晉惠帝皇后](../Page/晉惠帝.md "wikilink")），為賈-{后}-所親信。求錄[尚書](../Page/尚书_\(官职\).md "wikilink")、[尚書令等職](../Page/尚書令.md "wikilink")，但在大臣[張華](../Page/張華.md "wikilink")、[裴頠反對下不可](../Page/裴頠.md "wikilink")。

賈-{后}-謀害愍懷太子[司馬遹](../Page/司馬遹.md "wikilink")，使他失去太子之位。時倫領右軍將軍。有人企圖策動政變廢賈-{后}-復立太子，但司馬倫怕太子復立後對付曾是賈-{后}-一派的他，他於是泄露此事，使政變失敗，而更力勸賈-{后}-儘早謀害太子，以絕眾望。

太子遇害後，司马倫發動政變，以為太子復仇為名，廢賈-{后}-為庶人，幽禁於建始殿更把她運去[金墉城](../Page/金墉城.md "wikilink")。倫更[矯詔自為使持節](../Page/矯詔.md "wikilink")、[大都督](../Page/大都督.md "wikilink")、督中外諸軍事、相國，侍中、王如故，體制一如[司馬懿](../Page/司馬懿.md "wikilink")、[司馬昭輔魏之故事](../Page/司馬昭.md "wikilink")。例如其子[司馬馥封為前將軍](../Page/司馬馥.md "wikilink")，封濟陽王。另一子[司馬虔為](../Page/司馬虔.md "wikilink")[黃門郎](../Page/黃門郎.md "wikilink")，封汝陰王。

淮南王[司馬允起兵討倫失敗後](../Page/司馬允.md "wikilink")，司马倫加[九錫](../Page/九錫.md "wikilink")，增封五萬戶。

### 篡位時期

司馬倫矯詔[晉惠帝禪位](../Page/晉惠帝.md "wikilink")，自稱皇帝，專惠帝為太上皇，改元建始\[2\]。由於濫封爵位過甚，時人為之諺曰：「貂不足，狗尾續。」

三月，齊王[司馬冏起兵反司馬倫](../Page/司馬冏.md "wikilink")，成都王[司馬穎](../Page/司馬穎.md "wikilink")、河間王[司馬顒](../Page/司馬顒.md "wikilink")、常山王[司馬乂等支持](../Page/司馬乂.md "wikilink")。司馬倫兵敗。初被困於金墉城，後被賜死。\[3\]\[4\]

## 子

  - [司馬荂](../Page/司馬荂.md "wikilink")，[皇太子](../Page/皇太子.md "wikilink")
  - 濟陽王[司馬馥](../Page/司馬馥.md "wikilink")
  - 汝陰王[司馬虔](../Page/司馬虔.md "wikilink")
  - 霸城王[司馬詡](../Page/司馬詡.md "wikilink")

均于301年与其一同被赐死。

## 年号

赵王司马伦篡位后所用的[年号是](../Page/年号.md "wikilink")**建始**（[301年正月](../Page/301年.md "wikilink")—四月），共计四个月。《[晋书](../Page/晋书.md "wikilink")·惠帝纪》记载：[永宁元年正月赵王伦篡帝位](../Page/永宁.md "wikilink")。《赵王伦传》说篡帝位，大赦，改元建始。

  - 纪年

| 建始                               | 元年                                 |
| -------------------------------- | ---------------------------------- |
| [公元](../Page/公元纪年.md "wikilink") | [301年](../Page/301年.md "wikilink") |
| [干支](../Page/干支纪年.md "wikilink") | [辛酉](../Page/辛酉.md "wikilink")     |

  - [中国年号列表\#西晉](../Page/中国年号列表#西晉.md "wikilink")
  - 其他时期使用[建始的年号](../Page/建始.md "wikilink")
  - 同一时期存在的其他政权的年号
      - [永康](../Page/永康_\(晋\).md "wikilink")（[300年](../Page/300年.md "wikilink")-[301年四月](../Page/301年.md "wikilink")）：[西晋](../Page/西晋.md "wikilink")[晋惠帝司马衷年号](../Page/晋惠帝.md "wikilink")

## 評價

  - [房玄龄](../Page/房玄龄.md "wikilink")：“伦实庸琐，见欺[孙秀](../Page/孙秀_\(西晉\).md "wikilink")，潜构异图，煽成奸慝。乃使元良遘怨酷，上宰陷诛夷，乾耀以之暂倾，皇纲于焉中圮。遂裂冠毁冕，幸百六之会；绾玺扬纛，窥九五之尊。夫神器焉可偷安，鸿名岂容妄假！而欲托兹淫祀，享彼天年，凶暗之极，未之有也。”“伦实下愚，敢窃龙图，乱常奸位，遄及严诛。”（《晋书·卷五十九·列传第二十九》）

## 相关影視

  - 中國大陸電視劇《[軍師聯盟](../Page/軍師聯盟.md "wikilink")》（2017年）：[劉奇飾演司馬倫](../Page/劉奇_\(演員\).md "wikilink")

## 參考文獻

  - 李崇智，《中国历代年号考》，中华书局，2004年12月 ISBN 7101025129
  - 唐[房玄齡等人合著](../Page/房玄齡.md "wikilink")，《[晉書](../Page/晉書.md "wikilink")·[趙王倫傳](../Page/s:晉書/卷059#趙王倫.md "wikilink")》
  - 北宋[司馬光著](../Page/司馬光.md "wikilink")，元[胡三省音注](../Page/胡三省.md "wikilink")，《[資治通鑑](../Page/資治通鑑.md "wikilink")·[晉紀六](../Page/s:資治通鑑/卷084.md "wikilink")》

[Category:西晋宗室](../Category/西晋宗室.md "wikilink")
[S司M馬L倫](../Category/西晉被賜死人物.md "wikilink")
[Category:晋朝皇帝](../Category/晋朝皇帝.md "wikilink")

1.  《晉書·宣帝紀》：嘉平二年春正月，天子命帝立廟于洛陽,
    置左右長史，增掾屬、舍人滿十人，歲舉掾屬任御史、秀才各一人，增官騎百人，鼓吹十四人，封子肜平樂亭侯，倫安樂亭侯。
2.  《晉書·惠帝紀》：永寧元年春正月乙丑，趙王倫篡帝位。
3.  《晉書·惠帝紀》：夏四月……癸亥……於是大赦，改元，孤寡賜穀五斛，大酺五日。誅趙王倫、義陽王威、九門侯質等及倫之黨與。
4.  《資治通鑑》：丁卯，遣尚書袁敞持節賜倫死