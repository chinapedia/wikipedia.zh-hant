**孟静**，[河南](../Page/河南.md "wikilink")[洛阳人](../Page/洛阳.md "wikilink")，中国知名娱乐记者，曾工作于《[三联生活周刊](../Page/三联生活周刊.md "wikilink")》，现为《[Vista看天下](../Page/Vista看天下.md "wikilink")》杂志副主编。

## 经历

孟静曾在河南地方媒体工作数年，后短暂就职于《中国新闻周刊》，2003年成为《[三联生活周刊](../Page/三联生活周刊.md "wikilink")》文化部记者，主要从事电视文化方面的报道和评论，与《[南方周末](../Page/南方周末.md "wikilink")》文化娱乐记者袁蕾被业界人士戏称为“南袁北孟”。\[1\]
2013年起任《[Vista看天下](../Page/Vista看天下.md "wikilink")》杂志副主编。

她曾用笔名「凌霜华」、「孟久旱」、「孟十六」、「孟美静」，其中「凌霜华」为[金庸小说](../Page/金庸.md "wikilink")《[连城诀](../Page/连城诀.md "wikilink")》中人物，被部分网友评价为“凌霜华是金庸笔下最悲情的女子”。

## 著作

1.  《[八卦多一点](../Page/八卦多一点.md "wikilink")》，新星出版社，2008年2月1日。
2.  《秀场后台》——三联生活周刊文丛（第二辑），生活·读书·新知三联书店，2010年1月\[2\]。

## 参考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - [孟静搜狐博客](http://mj6655.blog.sohu.com/)

[M孟](../Category/洛阳人.md "wikilink") [M孟](../Category/中國記者.md "wikilink")
[J静](../Category/孟姓.md "wikilink")

1.
2.