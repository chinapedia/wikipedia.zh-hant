**李鉉雨**（，），美籍韓裔[男演员及](../Page/男演员.md "wikilink")[歌手](../Page/歌手.md "wikilink")，本名，**李尚源**。早期為歌手，剛出道第一張專輯即獲得KBS、MBC十大歌手獎；後曾主持MBC電視節目《星期三藝術舞台》；自2001年開始往戲劇界發展。

## 经历

李鉉雨在[美国完成高中和大学教育](../Page/美国.md "wikilink")；毕业于纽约帕森设计学院的奥提斯美术学院；后回到[韩国](../Page/韩国.md "wikilink")，进入暻园大学读研究生。1991年，以首张个人专辑《Black
Rainbow》进入歌坛，并以一曲《梦》走红。因充满感情色彩的歌曲、以及给人以安全感的温暖嗓音，李鉉雨被女歌迷称为“不是‘想交往’的男人，而是‘想一起生活’的男人”。\[1\]

此外，他还曾担任MBC《周三艺术舞台》的主持人，《人间故事－女人》的故事解说员以及SBS《李鉉雨的音乐生活》的广播主持。自2001年，李开始向演藝界发展。首次在影片《中毒》，客串了一名歌手的角色；后又在多部影视剧中出演配角。2006年，李鉉雨首次担当影片主角，在《夏天结束之前》中，突破性地扮演了一位自私但是又令人不得不爱的散发着性感魅力的离婚男子。

2009年2月21日，李鉉雨和小自己13岁的恋人结婚。同年9月，两人生下一名男婴。\[2\]

## 演出作品

### 電視劇

  - 2003年：[MBC](../Page/文化廣播_\(韓國\).md "wikilink")《[屋塔房小貓](../Page/屋塔房小貓.md "wikilink")》飾
    劉東俊
  - 2004年：MBC《[想結婚的女子](../Page/想結婚的女子.md "wikilink")》飾 金智勳
  - 2005年：[KBS](../Page/韓國放送公社.md "wikilink")《[結婚](../Page/結婚_\(電視劇\).md "wikilink")》飾
    徐鎮熙
  - 2005年：MBC《[悲傷戀歌](../Page/悲傷戀歌.md "wikilink")》
  - 2006年：MBC《[愛情無法擋](../Page/愛情無法擋.md "wikilink")》
  - 2006年：[SBS](../Page/SBS_\(韓國\).md "wikilink")《[獨身天下](../Page/獨身天下.md "wikilink")》
  - 2007年：KBS《[達子的春天](../Page/達子的春天.md "wikilink")》飾 嚴基中
  - 2010年：KBS《[Oh My Lady](../Page/Oh_My_Lady.md "wikilink")》飾 柳始俊
  - 2012年：SBS《[蠑螈道士和影子操作團](../Page/蠑螈道士和影子操作團.md "wikilink")》
  - 2013年：MBC《[百年的遺產](../Page/百年的遺產.md "wikilink")》飾 姜奉秀
  - 2013年：KBS《[紅寶石戒指](../Page/紅寶石戒指_\(電視劇\).md "wikilink")》飾 盧東八
  - 2016年：SBS《[戲子](../Page/戲子_\(電視劇\).md "wikilink")》飾 崔俊夏

### 電影

  - 1998年：《[Saturday，2:00
    pm](../Page/Saturday，2:00_pm.md "wikilink")》（客串）
  - 2004年：《[S日記](../Page/S日記.md "wikilink")》
  - 2005年：《[我的B型男友](../Page/我的B型男友.md "wikilink")》（客串）
  - 2005年：《[Before The Summer Passes
    Away](../Page/Before_The_Summer_Passes_Away.md "wikilink")》
  - 2006年：《[Sunday Seoul](../Page/Sunday_Seoul.md "wikilink")》（網路電影+客串）
  - 2010年：《遺失的背包》

### 綜藝節目

  - 2015年12月3日：《[Happy Together](../Page/Happy_Together.md "wikilink")》
    Ep426 (與尹尚、[John Park](../Page/John_Park.md "wikilink")、[Eric
    Nam](../Page/Eric_Nam.md "wikilink")、Stephanie Lee)

## 获奖荣誉

  - 1991年：MBC第10届歌手歌谣祭新人奖
  - 1991年：KBS歌谣大赏新人奖

## 外部連結

  - [EPG](https://web.archive.org/web/20070903145306/http://epg.epg.co.kr/Star/Profile/index.asp?actor_id=2861)

  - [Yahoo
    Korea](http://kr.search.yahoo.com/search?p=%B0%A1%BC%F6+%C0%CC%C7%F6%BF%EC+&fr=fr_iy)


## 參考文獻

<div class="references-small">

<references>

</references>

</div>

[L](../Category/韓國男歌手.md "wikilink")
[L](../Category/韩国電視演员.md "wikilink")
[L](../Category/韓國電影演員.md "wikilink")
[L](../Category/韓國音樂劇演員.md "wikilink")
[H](../Category/李姓.md "wikilink")

1.  [中国音乐库：李贤宇](http://www.mdbchina.net/singer/%E6%9D%8E%E8%B4%A4%E5%AE%87-13122/)

2.  [李贤宇结婚7个月后当爸爸](http://ent.sina.com.cn/s/j/2009-09-27/14442715216.shtml)