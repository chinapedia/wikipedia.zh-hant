2006年[世界棒球經典賽](../Page/世界棒球經典賽.md "wikilink")，中華台北隊在A組預賽中連續敗給[南韓隊](../Page/韓國棒球代表隊.md "wikilink")、[日本隊](../Page/日本棒球代表隊.md "wikilink")，僅贏得對[中國隊的勝利](../Page/中國棒球代表隊.md "wikilink")，位居分組第三而無法晉級複賽。

## 選手名單

<table>
<tbody>
<tr class="odd">
<td><ul>
<li>投手
<ul>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
</ul></li>
</ul>
<ul>
<li>捕手
<ul>
<li></li>
<li></li>
<li></li>
</ul></li>
</ul></td>
<td><p> </p></td>
<td><ul>
<li>內野手
<ul>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
</ul></li>
</ul>
<ul>
<li>外野手
<ul>
<li></li>
<li></li>
<li></li>
<li></li>
</ul></li>
</ul></td>
</tr>
</tbody>
</table>

<small>備註：

1.  原先被規劃遞補王建民空缺的[林岳平](../Page/林岳平.md "wikilink")，因未被列入經典賽六十人名單，且已超過六十人名單繳交期限，遭[大聯盟拒絕](../Page/大聯盟.md "wikilink")，而由[黃俊中遞補](../Page/黃俊中.md "wikilink")。
2.  原在三十人名單中的[鄭錡鴻在澳洲移訓後被教練團以近況不佳移出](../Page/鄭錡鴻.md "wikilink")，改由[陳偉殷遞補](../Page/陳偉殷.md "wikilink")，但遭[中日龍隊拒絕](../Page/中日龍隊.md "wikilink")，教練團因此補徵召[張家浩進入陣容](../Page/張家浩.md "wikilink")。

</small>

[Category:台灣棒球國家代表隊](../Category/台灣棒球國家代表隊.md "wikilink")