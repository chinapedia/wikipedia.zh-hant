[Arclength.svg](https://zh.wikipedia.org/wiki/File:Arclength.svg "fig:Arclength.svg")
functional has as its domain the vector space of [rectifiable
curves](../Page/rectifiable_curve.md "wikilink") (a subspace of
\(C([0,1],\mathbb{R}^3)\)), and outputs a real scalar. This is an
example of a non-linear functional.\]\]
[Integral_as_region_under_curve.svg](https://zh.wikipedia.org/wiki/File:Integral_as_region_under_curve.svg "fig:Integral_as_region_under_curve.svg")
is a [linear functional](../Page/linear_functional.md "wikilink") on the
vector space of Riemann-integrable functions from \(\mathbb{R}\) to
\(\mathbb{R}\).\]\]
传统上，**泛函**（functional）通常是指一種[定義域為](../Page/定義域.md "wikilink")[函數](../Page/函數.md "wikilink")，而值域为实数的「函數」。换句话说，就是从函数组成的一个[向量空间到](../Page/向量空间.md "wikilink")[实数的一个](../Page/实数.md "wikilink")[映射](../Page/映射.md "wikilink")。也就是说它的输入为函数，而输出为实数。泛函的应用可以追溯到[变分法](../Page/变分法.md "wikilink")，那里通常需要寻找一个函数用来最小化某个特定泛函。在物理学上，寻找某个能量泛函的最小系统状态是泛函的一个重要应用。

在[泛函分析中](../Page/泛函分析.md "wikilink")，泛函也用来指一个从任意向量空间到标量域的映射。泛函中的一类特例[线性泛函引发了对](../Page/线性泛函.md "wikilink")[对偶空间的研究](../Page/对偶空间.md "wikilink")。

设\(S\\)是由一些[函数構成的](../Page/函数.md "wikilink")[集合](../Page/集合.md "wikilink")。所谓\(S\\)上的泛函就是\(S\\)上的一个实值函数。\(S\\)称为该泛函的[容许函数集](../Page/容许函数集.md "wikilink")。

函数的变换某种程度上是更一般的概念，参见[算子](../Page/算子.md "wikilink")。

## 例子

### 對偶性

觀察映射

\[x_0 \mapsto f(x_0)\] 是一個函數，在這裡，\(x_0\)是函數f的自变量。

同時，將函數映射至一個點的函數值

\[f \mapsto f(x_0)\] 是一個泛函，在此\(x_0\)是一個參數

只要 \(f\)
是一個從[向量空間至一個佈於實數的](../Page/向量空間.md "wikilink")[體的線性轉換](../Page/體.md "wikilink")，上述的線性映射彼此[對偶](../Page/對偶.md "wikilink")，那麼在[泛函分析上](../Page/泛函分析.md "wikilink")，這兩者都稱作線性泛函。

## 参见

  - [线性泛函](../Page/线性泛函.md "wikilink")
  - [最优化](../Page/最优化.md "wikilink")
  - [张量](../Page/张量.md "wikilink")

## 参考资料

  -
  -
[F](../Category/函数.md "wikilink") [F](../Category/泛函分析.md "wikilink")