**台灣全民廣播電台股份有限公司**（英語：News98 Radio
Station，簡稱：**九八新聞台**、英語：**News98**），是[台灣](../Page/台灣.md "wikilink")[台北市旗下唯一的](../Page/台北市.md "wikilink")[廣播電台](../Page/廣播電台.md "wikilink")，收聽範圍為[桃園以北](../Page/桃園市.md "wikilink")。[台呼是](../Page/Jingle.md "wikilink")「時時刻刻報新聞，聽見全世界」。

## 歷史

台灣全民廣播電台原來是前[民主進步黨](../Page/民主進步黨.md "wikilink")[立法委員](../Page/立法委員.md "wikilink")[張俊宏成立於](../Page/張俊宏.md "wikilink")1993年7月22日，後來被張俊宏出售、由[趙少康擔任總顧問](../Page/趙少康.md "wikilink")；因為頻率在調頻98.1
MHz，因此命名為News98，而開播時間則改為1999年。總台位於在台北市中正區羅斯福路二段「大都市國際中心」。News98的標誌（日與月）是[霍榮齡所設計的](../Page/霍榮齡.md "wikilink")。

News98以新聞與財經理財節目為主，新聞評論節目為輔。2012年，News98中文名稱定為「九八新聞台」。

2016年2月28日起，News98改版，《非耀不可》、《一點照新聞》、《愛你22小時》、《
阿貓阿狗逛大街》等節目停播，《九點強強滾》改為週一到週五19:00播出，節目改名為《19點強強滾》；平日的《整點新聞》改為7點到21點播出，週末《整點新聞》改為週六、週日12點到18點，每天22點、23點、24點的整點新聞取消播出。

2017年3月4日起，停播週末的《整點新聞》，同月6日起，停播整點新聞後的「即時路況」。8月起平日的《整點新聞》改為7點到19點播出。

2017年9月9日起，裁撤新聞部資遣全部11位記者和多數編輯與主播；目前只剩4位主播播報平日整點新聞。

2018年7月2日起，調整部分節目內容；平日16點改播《金融曼哈頓》、《股市羅賓漢》；23點播出《士大夫談知識》。

2019年3月13日起，自中華電信hinet網頁下架。

## 間隙節目

  - 交通安全一定強：週一至週五07:54、18:54播出（該單元與交通部公路總局共同製播）。

## 節目表

|             |                                                                                                                                                                                                                                                         |                                          |                                                  |
| ----------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------- | ------------------------------------------------ |
| 播出時間        | 週一至週五                                                                                                                                                                                                                                                   | 週六                                       | 週日                                               |
| 06:00-07:00 | 98愛報報 <font color=orange>詩瑋                                                                                                                                                                                                                             | 張大春泡新聞 (重播)                              | 世界一把抓 (重播)                                       |
| 07:00-08:00 | 新聞．財經起床號 <font color=orange>陳鳳馨                                                                                                                                                                                                                         | 健康總動員 <font color=orange>安安              | 健康總動員 <font color=orange>安安                      |
| 08:00-09:00 | 新聞．財經起床號 <font color=orange>陳鳳馨                                                                                                                                                                                                                         | 全民On Call：鄧惠文時間 (重播)                     | Evergreen 好聽的歌 <font color=orange>Jackson & Katy |
| 09:00-10:00 | 新聞．世界一把抓 <font color=orange>鄭麗文                                                                                                                                                                                                                         | 健康情報讚<font color=orange>安安               | Evergreen 好聽的歌 <font color=orange>Jackson & Katy |
| 10:00-11:00 | 新聞．愛吃愛生活 <font color=orange>許心怡                                                                                                                                                                                                                         | 民意新連線 <font color=orange>賴祥蔚             | 民意新連線 (重播)                                       |
| 11:00-12:00 | 新聞．名醫On Call <font color=orange>名醫群                                                                                                                                                                                                                     | 名醫On Call (重播)                           | 名醫On Call (重播)                                   |
| 12:00-13:00 | 新聞．小董真心話 <font color=orange>董智森                                                                                                                                                                                                                         | 全民On Call：全能狗S 寵物當家 (重播)                 | 愛吃愛生活 (重播)                                       |
| 13:00-14:00 | 新聞．張大春泡新聞 <font color=orange>張大春</font>／週五 幸福號列車 <font color=orange>張曼娟                                                                                                                                                                                 | 健康補給站<font color=orange>安安               | 健康補給站<font color=orange>安安                       |
| 14:00-15:00 | 新聞．張大春泡新聞 <font color=orange>張大春</font>／週五 幸福號列車 <font color=orange>張曼娟                                                                                                                                                                                 | 全民on Call：名醫時間 (長虹診所共同製播) (重播)           | 張大春泡新聞 (重播)                                      |
| 15:00-16:00 | 新聞．投資理財王 <font color=orange>張琦蓁、楊天迪、林和彥                                                                                                                                                                                                                 | 台股大玩家<font color=orange> 張琦蓁、魏明裕、周致偉     | 超級玩樂大帝國 <font color=orange>姚舜                    |
| 16:00-17:00 | 新聞．金融曼哈頓 <font color=orange>阮蕙慈</font>股市羅賓漢<font color=orange> 羅文彬                                                                                                                                                                                      | 財運紅不讓 <font color=orange>林洳萱、林和彥、陳學進     | 超級玩樂大帝國 <font color=orange>姚舜                    |
| 17:00-18:00 | 新聞．財經一路發 <font color=orange>阮慕驊                                                                                                                                                                                                                         | 金融曼哈頓 ／股市羅賓漢(重播)                         | 財經起床號 (重播)                                       |
| 18:00-19:00 | 新聞．財經一路發 <font color=orange>阮慕驊                                                                                                                                                                                                                         | 財經起床號 (重播)                               | 全民On Call：呂秋遠時間(重播)                              |
| 19:00-20:00 | 新聞．王牌紅不讓 <font color=orange>李德瑜                                                                                                                                                                                                                         | 民意新連線 (重播)                               | 民意新連線 (重播)                                       |
| 20:00-21:00 | 週一 全民On Call：全能狗S 寵物當家 <font color=orange>楊靜宇</font> ／週二 全民On Call：鄧惠文時間 <font color=orange>鄧惠文</font>／週三 全民On Call：名醫時間 <font color=orange>名醫群</font>／週四 全民On Call：名醫時間 <font color=orange>名醫群</font>／週五 全民On Call：呂秋遠時間 <font color=orange>呂秋遠</font> | 幸福號列車 (重播)                               | Evergreen好聽的歌 (重播)                               |
| 21:00-22:00 | 鑽石線上 <font color=orange>丁呈臻</font>／股市幸運星<font color=orange> 丁珵臻、 林幸蓉                                                                                                                                                                                    | 幸福號列車 (重播)                               | Evergreen好聽的歌 (重播)                               |
| 22:00-23:00 | 98行家品味 <font color=orange>劉家安</font>／股市羅賓漢 <font color=orange>費平</font>(重播)                                                                                                                                                                             | 士大夫談知識 <font color=orange>王思迅</font>(重播) | 98音樂風：<font color=orange>小風</font> (重播)          |
| 23:00-24:00 | 士大夫談知識 <font color=orange>謝文憲、謝閔瑜、白瑜、王思迅、 許景泰                                                                                                                                                                                                           | 士大夫談知識 <font color=orange>王琄             | 98音樂風：<font color=orange>小風</font> (重播)          |
| 00:00-01:00 | 真情會客室 、今夜不設限 <font color=orange>林玉輝                                                                                                                                                                                                                     | 今夜不設限 <font color=orange>林玉輝             | 真情會客室 <font color=orange>林玉輝                     |
| 01:00-02:00 | 張大春泡新聞：大書場 (重播)．週六 幸福號列車 (重播)                                                                                                                                                                                                                           | 98音樂風 <font color=orange>宜珣              | 98音樂風 <font color=orange>嘉美                      |
| 02:00-03:00 | 小董真心話（重播）．週六 幸福號列車 (重播)                                                                                                                                                                                                                                 | 98音樂風 <font color=orange>嘉美              | 98音樂風 <font color=orange>98DJ                    |
| 03:00-04:00 | 世界一把抓 (重播)                                                                                                                                                                                                                                              | 98音樂風 <font color=orange>祐堂              | 98音樂風 <font color=orange>98DJ                    |
| 04:00-05:00 | 財經起床號 (重播)                                                                                                                                                                                                                                              | 98音樂風 <font color=orange>小魚              | 98音樂風 <font color=orange>宜珣                      |
| 05:00-06:00 | 財經一路發 (重播)                                                                                                                                                                                                                                              | 98音樂風 <font color=orange>明真              | 98音樂風 <font color=orange>SaSa                    |

## 主播及節目主持人

### 新聞主播

|   **新聞主播**    |
| :-----------: |
| 劉佳娜 0700-1200 |
|               |

### 主持人

|             **主持人**              |
| :------------------------------: |
| [陳鳳馨](../Page/陳鳳馨.md "wikilink") |
|               張曼娟                |
|             Jackson              |
| [賴祥蔚](../Page/賴祥蔚.md "wikilink") |

## 已停播節目

  - MTV音樂瘋（和[MTV合作](../Page/音樂電視網.md "wikilink")）
  - 午安台灣
  - 李慶安 News Box
  - Goodmorning Taiwan
  - 張牙舞爪過週末 / 張景為
  - 美食報報報
  - 新鮮晚報
  - Movie Goer
  - Ruby's Talk
  - 遊走閒逛
  - 生活ＤＮＡ / 黃薇
  - 說書人張大春
  - 政經龍捲風
  - 政經12點 / 鄭弘儀
  - Goodevening
  - 好健康星期六
  - 銀色 Spotlight / 袁永興
  - 日月神叫 / 趙自強 & 郎祖筠
  - Do Re Mi 亮范店 / 劉亮佐 & 范瑞君
  - 嘻哈喬治 / 張兆志
  - 九八夜線
  - 拂曉時刻
  - News98 12點新聞（週一至週五播出）
  - 吹吹音樂風 / Tracy
  - 音樂大街 / Tracy→詩瑋
  - [愛你22小時](../Page/愛你22小時.md "wikilink") / 王文華
  - 週日薇薇笑 / 徐薇
  - 運動痛快報 / 李雲翔
  - 科技98 / 林之晨
  - 阿貓阿狗逛大街 / 林清盛
  - 一點照新聞 / 楊照
  - 非耀不可 / 劉駿耀
  - 音樂didadi / [李欣芸](../Page/李欣芸.md "wikilink")
  - 音樂五四三 / 馬世芳
  - 爵士Taxi / 程港輝
  - 兩岸新連線 / 魯智淺
  - 將你放出來 (Johnny Fun出來) / Johnny
  - [九點強強滾](../Page/九點強強滾.md "wikilink") (19點強強滾) / 趙自強

## 參見

  - [飛碟聯播網](../Page/飛碟聯播網.md "wikilink")
  - [臺灣廣播電台列表](../Page/臺灣廣播電台列表.md "wikilink")

## 參考來源

## 外部連結

  - [九八新聞台](http://www.news98.com.tw)

  - (官方)

  -
[九](../Category/台灣廣播電台.md "wikilink")