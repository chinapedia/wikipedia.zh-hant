**里夏德·阿道夫·席格蒙迪**（，），[奥地利](../Page/奥地利.md "wikilink")、[德国籍的](../Page/德国.md "wikilink")[匈牙利裔](../Page/匈牙利.md "wikilink")[化学家](../Page/化学家.md "wikilink")，1925年[诺贝尔化学奖获得者](../Page/诺贝尔化学奖.md "wikilink")\[1\]，主要研究领域为[胶体化学](../Page/胶体化学.md "wikilink")。[月球上有以其名字命名的](../Page/月球.md "wikilink")“[席格蒙迪环形山](../Page/席格蒙迪环形山.md "wikilink")”。

## 早期经历

席格蒙迪1865年4月1日出生于[奥地利帝国的](../Page/奥地利帝国.md "wikilink")[维也纳](../Page/维也纳.md "wikilink")。其父阿道夫·席格蒙迪为[牙科在奥地利的发展做了很大贡献](../Page/牙科.md "wikilink")，还发明了几种牙科医疗器材。阿道夫·席格蒙迪在1880年早逝，席格蒙迪由母亲抚养长大，接受了全面的教育，另一方面也保持[攀岩](../Page/攀岩.md "wikilink")、[登山和](../Page/登山.md "wikilink")[游泳等爱好](../Page/游泳.md "wikilink")。\[2\]高中阶段，席格蒙迪培养了对[自然科学](../Page/自然科学.md "wikilink")，尤其是[化学和](../Page/化学.md "wikilink")[物理的兴趣](../Page/物理.md "wikilink")，并开始在家中自己做实验。

## 学术生涯

[ColloidalGold_aq.png](https://zh.wikipedia.org/wiki/File:ColloidalGold_aq.png "fig:ColloidalGold_aq.png")，其金粒子大小決定顏色\[3\]\]\]

席格蒙迪在[维也纳大学医学院开始大学阶段的学习](../Page/维也纳大学.md "wikilink")，但为了学习化学很快就转学到[維也納工業大學](../Page/維也納科技大學.md "wikilink")，后来又转到[慕尼黑大学](../Page/慕尼黑大学.md "wikilink")。在慕尼黑大学期间，他在威廉·冯·米勒教授的指导下开始科学研究，重点研究[茚](../Page/茚.md "wikilink")，并于1889年获得[博士学位](../Page/博士学位.md "wikilink")。\[4\]

席格蒙迪后来离开[有机化学领域](../Page/有机化学.md "wikilink")，加入[柏林大学](../Page/柏林大学.md "wikilink")的物理研究组。1893年在[格拉茨大学完成](../Page/格拉茨大学.md "wikilink")[德语国家教授资格考试](../Page/德语国家教授资格考试.md "wikilink")（Habilitation）。1897年，由于他在[玻璃及其着色方面的知识](../Page/玻璃.md "wikilink")，位于[耶拿的](../Page/耶拿.md "wikilink")[肖特玻璃制造厂向他提供一份工作](../Page/肖特集团.md "wikilink")。席格蒙迪接受了这一工作，在该厂期间，他对茶色玻璃进行研究，还发明了一种玻璃，命名为“Jenaer
Milchglas”。1900年，席格蒙迪从肖特玻璃制造厂离职，但仍留在耶拿，独立开展研究。他与[光学器材厂商](../Page/光学.md "wikilink")[蔡司公司合作](../Page/蔡司公司.md "wikilink")，研制了狭缝超显微镜。1907年，他加入[格丁根大学](../Page/格丁根大学.md "wikilink")，成为[无机化学研究所教授和所长](../Page/无机化学.md "wikilink")，直到1929年2月退休。\[5\]

[Vintage_cranberry_glass.jpg](https://zh.wikipedia.org/wiki/File:Vintage_cranberry_glass.jpg "fig:Vintage_cranberry_glass.jpg")

1925年，席格蒙迪因“证明了[胶体](../Page/胶体.md "wikilink")[溶液的异相性质](../Page/溶液.md "wikilink")，以及确立了现代[胶体化学的基础](../Page/胶体化学.md "wikilink")”，被授予诺贝尔化学奖。\[6\]这些成果出自他在[格拉茨和耶拿的时期](../Page/格拉茨.md "wikilink")。甚至在完成博士论文之前，他已经对使用银盐对玻璃着色进行了研究，并发表相关论文。在格拉茨时期，他完成了自己最著名的关于胶体的研究工作。其中一项成果就是产生茶色玻璃的精确机制。\[7\]其后几年，他研究胶態金（或称金溶胶）及其在蛋白质标记上的用途。

## 个人生活

席格蒙迪于1903年与劳拉·路易丝·穆勒结婚。他们拥有两个女儿。从格丁根大学退休后不久，席格蒙迪于1929年9月24日在格丁根去世。\[8\]

## 参考文献

## 延伸阅读

  -
  -
  -
  -
  -
## 外部链接

  - [诺贝尔基金会网站上的里夏德·阿道夫·席格蒙迪传记](http://nobelprize.org/nobel_prizes/chemistry/laureates/1925/zsigmondy-bio.html)

[Category:诺贝尔化学奖获得者](../Category/诺贝尔化学奖获得者.md "wikilink")
[Category:匈牙利諾貝爾獎獲得者](../Category/匈牙利諾貝爾獎獲得者.md "wikilink")
[Category:匈牙利化学家](../Category/匈牙利化学家.md "wikilink")
[Category:奥地利化学家](../Category/奥地利化学家.md "wikilink")
[Category:德国化学家](../Category/德国化学家.md "wikilink")
[Category:哥廷根大學教師](../Category/哥廷根大學教師.md "wikilink")
[Category:格拉茨大學教授](../Category/格拉茨大學教授.md "wikilink")
[Category:柏林洪堡大學教師](../Category/柏林洪堡大學教師.md "wikilink")
[Category:慕尼黑大學校友](../Category/慕尼黑大學校友.md "wikilink")
[Category:維也納大學校友](../Category/維也納大學校友.md "wikilink")
[Category:維也納工業大學校友](../Category/維也納工業大學校友.md "wikilink")
[Category:維也納科學家](../Category/維也納科學家.md "wikilink")

1.

2.

3.  [Different sizes of colloidal gold
    particles.](http://www.ansci.wisc.edu/facstaff/Faculty/pages/albrecht/albrecht_web/Programs/microscopy/colloid.html)


4.

5.
6.
7.

8.