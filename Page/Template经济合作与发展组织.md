<includeonly>{{\#ifeq:|nocat||}}</includeonly><noinclude>

## 使用方法

本模板会自动把引用它的页面加入分类。引用本模板时使用下列语法，可以避免加入分类：<small>（注意nocat必须为英文小写）</small>
{{|nocat}}

</noinclude>

[Category:OECD成員經濟體](../Category/OECD成員經濟體.md "wikilink")
[\*](../Category/經濟合作與發展組織.md "wikilink")
[经](../Category/国际组织导航模板.md "wikilink")