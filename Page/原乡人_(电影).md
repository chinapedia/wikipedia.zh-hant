是一部1980年出品的[台湾电影](../Page/台湾电影.md "wikilink")，根据[台湾](../Page/台湾.md "wikilink")[客家作家](../Page/客家.md "wikilink")[鍾理和的短篇小说](../Page/鍾理和.md "wikilink")《[原鄉人](../Page/原乡人_\(小说\).md "wikilink")》及其本人经历改编。导演[李行](../Page/李行.md "wikilink")，主演[秦漢](../Page/秦漢_\(演員\).md "wikilink")、[林凤娇](../Page/林凤娇.md "wikilink")。

影片获得1981年[第18屆金馬獎的最佳原创电影歌曲](../Page/第18屆金馬獎.md "wikilink")（[翁清溪作曲](../Page/翁清溪.md "wikilink")、[邓丽君演唱](../Page/邓丽君.md "wikilink")）、童星（[鄭傳文](../Page/鄭傳文.md "wikilink")），并获最佳音乐、音效的提名。

## 外部連結

  - {{@movies|fMcmb3031875|原鄉人}}

  -
  -
  -
  -
[0](../Category/1980年代台灣電影作品.md "wikilink")
[Category:台灣劇情片](../Category/台灣劇情片.md "wikilink")
[Y原](../Category/台灣小說改編電影.md "wikilink")
[Category:李行電影](../Category/李行電影.md "wikilink")
[Category:張永祥編劇作品](../Category/張永祥編劇作品.md "wikilink")