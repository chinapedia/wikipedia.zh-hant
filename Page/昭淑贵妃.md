**昭淑貴妃**，[周姓](../Page/周姓.md "wikilink")，為[北宋](../Page/北宋.md "wikilink")[宋仁宗](../Page/宋仁宗.md "wikilink")[妃](../Page/妃.md "wikilink")，[秦國魯國賢穆明懿大長公主及](../Page/秦國魯國大長公主.md "wikilink")[燕國舒國大長公主生母](../Page/寶壽公主.md "wikilink")。

## 生平

周氏出身[開封](../Page/开封府.md "wikilink")，四歲時，隨其姑母入宮為女侍。后因侍奉宠妃张贵妃（后来的[溫成皇后](../Page/溫成皇后.md "wikilink")），被年纪比她还要小的张贵妃认做养女，封为[安定](../Page/安定郡.md "wikilink")[郡君](../Page/郡君.md "wikilink")，得以為宋仁宗侍寢，並生下燕國舒國大長公主等兩位公主，册封为[美人](../Page/美人.md "wikilink")，[婕妤](../Page/婕妤.md "wikilink")，[婉容](../Page/婉容.md "wikilink")。

周氏所生的兩位公主紛紛下嫁後，被晉封為[賢妃](../Page/賢妃.md "wikilink")。仁宗駕崩後，周氏一直活到[宋徽宗即位後](../Page/宋徽宗.md "wikilink")，並逐渐被加封為[德妃](../Page/德妃.md "wikilink")，[淑妃](../Page/淑妃.md "wikilink")，[貴妃](../Page/貴妃.md "wikilink")，這其中的40多年，周氏一直過著儉樸的禮佛生活。

後來，[燕國舒國大長公主過世](../Page/寶壽公主.md "wikilink")，徽宗准許周氏離開後宮，並在宮外營建私宅與親友往來。周氏相當地長壽，一直活到九十三歲逝世，[諡號](../Page/諡號.md "wikilink")**昭淑**。

[Category:宋仁宗妃嬪](../Category/宋仁宗妃嬪.md "wikilink")
[Z](../Category/开封人.md "wikilink")
[\~](../Category/周姓.md "wikilink")