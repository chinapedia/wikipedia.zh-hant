[Lohas_Park_Wellcome_Supermarket_2013.jpg](https://zh.wikipedia.org/wiki/File:Lohas_Park_Wellcome_Supermarket_2013.jpg "fig:Lohas_Park_Wellcome_Supermarket_2013.jpg")惠康\]\]
[Wellcome_Superstore_in_Liberte_2011.jpg](https://zh.wikipedia.org/wiki/File:Wellcome_Superstore_in_Liberte_2011.jpg "fig:Wellcome_Superstore_in_Liberte_2011.jpg")惠康超級廣場\]\]
[Old_Stanley_Police_Station_Level_2_2010.jpg](https://zh.wikipedia.org/wiki/File:Old_Stanley_Police_Station_Level_2_2010.jpg "fig:Old_Stanley_Police_Station_Level_2_2010.jpg")改建的惠康超級市場\]\]
[Wellcome,_Cheung_Chau_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Wellcome,_Cheung_Chau_\(Hong_Kong\).jpg "fig:Wellcome,_Cheung_Chau_(Hong_Kong).jpg")分店\]\]
[Jasons_ichiba_in_Park_Central_2018.jpg](https://zh.wikipedia.org/wiki/File:Jasons_ichiba_in_Park_Central_2018.jpg "fig:Jasons_ichiba_in_Park_Central_2018.jpg")原惠康超級廣場升級為Jasons
ichiba\]\]
[Wellcome_Supermarket_in_Taipei_Beitou_2015.JPG](https://zh.wikipedia.org/wiki/File:Wellcome_Supermarket_in_Taipei_Beitou_2015.JPG "fig:Wellcome_Supermarket_in_Taipei_Beitou_2015.JPG")[北投區的頂好](../Page/北投區.md "wikilink")\]\]

**惠-{}-康**（），在[台灣稱](../Page/台灣.md "wikilink")**頂-{}-好**，是[香港最大型及歷史最悠久的](../Page/香港.md "wikilink")[連鎖](../Page/連鎖店.md "wikilink")[超級市場](../Page/超級市場.md "wikilink")，於1945年成立，1964年起由[怡和集團旗下](../Page/怡和集團.md "wikilink")[牛奶國際全資擁有](../Page/牛奶國際.md "wikilink")，至今有超過301間分店，其中33間為超級廣場（Superstore），遍佈全港，其主要對手是另一連鎖超級市場集團[百佳](../Page/百佳.md "wikilink")。

惠-{}-康與百佳兩大連鎖超級市場集團共佔據香港超過7成[市場佔有率](../Page/市場佔有率.md "wikilink")。按市場調查機構EuroMonitor估計，2012年排名第一的惠-{}-康在香港市場佔有率約為39.8%；[百佳在香港市場佔有率約為](../Page/百佳.md "wikilink")33.1%；而華潤創業旗下的[華潤萬家超市在香港的市佔率則為](../Page/華潤萬家超市.md "wikilink")7.8%。
\[1\]

除香港外，牛奶國際亦在1987年於[台灣收購](../Page/台灣.md "wikilink")[-{頂好企業}-旗下的頂](../Page/頂好企業.md "wikilink")-{}-好超市（今「惠康百貨股份有限公司」，[英](../Page/英语.md "wikilink")：Wellcome
Taiwan Co. Ltd），於當地經營「**頂-{}-好
Wellcome**」連鎖超級市場，首家分店於1987年12月在[臺北市](../Page/臺北市.md "wikilink")[大安區](../Page/大安區.md "wikilink")[忠孝東路開幕](../Page/忠孝東路.md "wikilink")，至今已成為台灣大型連鎖超市之一，目前於全台共開設逾300家分店。

## 歷史

  - 1945年，惠康辦館由吳宗偉先生、劉濂先生及高燕如先生創立。第一間商店位於[香港](../Page/香港.md "wikilink")[中環](../Page/中環.md "wikilink")[雪廠街](../Page/雪廠街.md "wikilink")，專售洋酒、餅乾、罐頭、飲品等進口貨。
  - 1957年，辦館開設家居送貨部。成立之初，每日平均有600名顧客到各分店訂購貨品。
  - 1960年，[牛奶公司和](../Page/牛奶公司.md "wikilink")[連卡佛合作創辦的子公司](../Page/連卡佛.md "wikilink")[大利連](../Page/大利連.md "wikilink")（Dairy
    Lane）有限公司在[香港](../Page/香港.md "wikilink")[中環開設大利連超級市場](../Page/中環.md "wikilink")，是香港第一間超級市場。
  - 1964年，惠康正式成為牛奶公司的附屬公司，並引進美國「自助購物」超市經營概念：顧客無須向服務員索取貨品，可自行於貨架選購貨品。
  - 1972年，怡和洋行旗下香港置地收購牛奶公司
  - 1973年，惠康率先推出袋裝米，打破散裝米銷售傳統，為顧客提供方便衛生的選擇。
  - 1980年，大利連超級市場和惠康辦館合併為惠康超級市場，並開始在各區設立分店。
  - 1982年，邀得「肥姐」[沈殿霞拍攝廣告](../Page/沈殿霞.md "wikilink")
  - 1985年，推出自家品牌「特惠牌（No Frills）」，貨品種類超過300款。同年首間超市採用電子數據聯通（Electronic
    Digital Internet）網絡，提升營運。
  - 1986年，牛奶公司從香港置地分拆，在香港上市
  - 1987年，[台灣首家分店在](../Page/台灣.md "wikilink")[台北市](../Page/台北市.md "wikilink")[大安區](../Page/大安區.md "wikilink")[忠孝東路開幕](../Page/忠孝東路.md "wikilink")。
  - 1998年，[銅鑼灣](../Page/銅鑼灣.md "wikilink")[記利佐治街分店帶頭引進超級市場](../Page/記利佐治街.md "wikilink")24小時營業概念。同年投資港幣四億元興建「惠康新鮮食品中心」。佔地16萬1千平方呎，為全亞洲最先進的食品處理中心，確保整個供應鍊既安全又具效率。又創建自家國際品牌「首選（First
    Choice）」，提供超過1,000款貨品，價格相宜；現為其中一個廣為香港人熟悉的品牌。
  - 2000年，推出「十大超市品牌選舉」，成為全港一年一度業界盛事，廣邀全民投票加許民選品牌。
  - 2001年，邀得「DoDo姐」[鄭裕玲出任代言人](../Page/鄭裕玲.md "wikilink")
  - 2001年，於[屯門市廣場開設首間超級廣場](../Page/屯門市廣場.md "wikilink")，結集傳統街市及雜貨店功能於一體，為顧客提供「一站式」購物服務。
  - 2001年，於[新都城中心](../Page/新都城中心.md "wikilink")3期開設首間部份(收銀處位置)無天花設計的超級廣場，邀得「DoDo姐」[鄭裕玲在該分店拍攝電視廣告](../Page/鄭裕玲.md "wikilink")。
  - 2003年，[赤柱新店開幕](../Page/赤柱.md "wikilink")，成為香港唯一設於歷史古蹟「百年[赤柱警署](../Page/赤柱警署.md "wikilink")」內的超級市場。

<!-- end list -->

  - 成為香港首間超市委任演藝界名人為代言人，為品牌推廣。
  - 創立另一自家品牌「御品皇（Yu Pin King）」，提供約200款高品質中式食材。
  - 成為香港首間參與[八達通日日賞計劃之超市](../Page/八達通日日賞.md "wikilink")，讓顧客於購物時不論以任何付款方式均可賺取「日日賞$」，並於下次購物時作現金使用。
  - 2009年於[Facebook開設專頁](../Page/Facebook.md "wikilink")，開拓出一個與顧客互動的嶄新平台。
  - 創新推出互動電視遊戲廣告「撳價」，顧客於指定時間收看惠康電視廣告，用手機把電視廣告內顯示之答案發送至指定號碼，即有機會獲取惠康現金券。
  - 連續第3年榮獲環境保護委員會及環境保護署頒發之「香港環保卓越計劃 - 批發及零售業金獎」，肯定了惠康在環保方面的領導地位。
  - 2013年把「惠康為您送」網站全面升級，為顧客帶來更方便的網上購物體驗。
  - 2017年，[將軍澳中心原惠康超級廣場升級為Jasons](../Page/將軍澳中心.md "wikilink") Ichiba。
  - 2018年，[西九龍中心惠康超級市場倒閉](../Page/西九龍中心.md "wikilink")。

## 分店

### （惠-{}-康）

| 品牌                                                                           | [港島](../Page/香港島.md "wikilink") | [九龍](../Page/九龍.md "wikilink") | [新界](../Page/新界.md "wikilink") | [離島](../Page/離島區.md "wikilink") |
| ---------------------------------------------------------------------------- | ------------------------------- | ------------------------------ | ------------------------------ | ------------------------------- |
| 惠-{}-康超級市場                                                                   | 59                              | 67                             | 115                            | 6                               |
| 惠-{}-康超級廣場                                                                   | 6                               | 9                              | 16                             | 2                               |
| [Market Place by Jasons](../Page/Market_Place_by_Jasons.md "wikilink")       | 9                               | 12                             | 5                              | 0                               |
| [Oliver's the Delicatessen](../Page/Oliver's_the_Delicatessen.md "wikilink") | 1                               | 0                              | 0                              | 0                               |
| [3hreeSixty](../Page/3hreeSixty.md "wikilink")                               | 1                               | 1                              | 0                              | 0                               |
| [Jasons·Food & Living](../Page/Jasons_Food_&_Living.md "wikilink")           | 1                               | 0                              | 0                              | 0                               |
| Jasons Ichiba                                                                | 0                               | 0                              | 1                              | 0                               |
| 總數                                                                           | 77                              | 89                             | 137                            | 8                               |

  - 截至2018年9月，惠-{}-康於香港共經營280間分店。\[2\]
  - 截至2014年3月，Market Place by Jasons於香港共經營26間分店。\[3\]

<!-- end list -->

  - 小知識

<!-- end list -->

  - 惠康在上水新豐路相隔約二百公尺處建下了兩間惠康超級市場。相信為相隔最近的惠康超市。
  - 西環堅尼地城卑路乍街隆基大樓設有一間惠康超級市場，於120公尺外的北街海怡花園地下已有另一間分店。

### （頂-{}-好）

| 地區分店                             | 頂-{}-好 | [Jasons Market Place](../Page/Market_Place_by_Jasons.md "wikilink") |
| -------------------------------- | ------ | ------------------------------------------------------------------- |
| [基隆市](../Page/基隆市.md "wikilink") | 7      | 0                                                                   |
| [臺北市](../Page/臺北市.md "wikilink") | 54     | 10                                                                  |
| [新北市](../Page/新北市.md "wikilink") | 78     | 4                                                                   |
| [桃園市](../Page/桃園市.md "wikilink") | 33     | 1                                                                   |
| [新竹市](../Page/新竹市.md "wikilink") | 9      | 1                                                                   |
| [新竹縣](../Page/新竹縣.md "wikilink") | 12     | 1                                                                   |
| [苗栗縣](../Page/苗栗縣.md "wikilink") | 5      | 0                                                                   |
| [臺中市](../Page/臺中市.md "wikilink") | 6      | 2                                                                   |
| [彰化縣](../Page/彰化縣.md "wikilink") | 1      | 0                                                                   |
| [嘉義市](../Page/嘉義市.md "wikilink") | 3      | 0                                                                   |
| [臺南市](../Page/臺南市.md "wikilink") | 5      | 1                                                                   |
| [高雄市](../Page/高雄市.md "wikilink") | 5      | 4                                                                   |
| [屏東縣](../Page/屏東縣.md "wikilink") | 0      | 1                                                                   |
| [宜蘭縣](../Page/宜蘭縣.md "wikilink") | 1      | 0                                                                   |
| 總數                               | 219    | 25                                                                  |

  - 截至2018年12月，頂-{}-好於台灣共經營219間分店 \[4\]，Jasons Market
    Place於台灣共經營25間分店。\[5\]
  - 2016年，台灣頂好超市自1986年成立創業30週年。

### （惠-{}-康）

  - 惠康超級市場1間分店

### （惠-{}-康）

  - 惠康超級市場1間分店

### （惠-{}-康）

  - 惠康超級市場11間分店

## 爭議及事件

###

  - 售賣過期衛生巾

2006年4月，一名顧客從惠康超級市場購入衛生巾後，正當打算使用其中一包時，發現有關衛生巾護翼完全失去黏力，仔細察看更驚覺該棉層邊上有多處黑色小點。其後該顧客再仔細觀察，發現該衛生巾已過期三個多月，但仍在惠康的貨架上出售。惠康發言人則堅稱超市政策不容許售賣過期的衛生巾，又指是個別店舖經理疏忽檢測貨品而未有發現店內有過期衛生巾。\[6\]

  - 店內出現老鼠

2011年1月，一名YouTube用戶上載短片，短片中清楚可見惠康超級市場一個薯仔攤架內有老鼠，片中一名市民手持麪包夾，在薯仔堆中搜索。一隻薯仔般大小的小鼠赫然竄出，再逃跑到其他食物堆中。惠-{}-康發言人表示對問題表示關注，會追查片段是否屬屯門區內惠康並跟進，及後事件則不了了之。\[7\]\[8\]

  - 被海關檢控

2014年2月，[香港海關發現惠康出售一品牌的薯片時](../Page/香港海關.md "wikilink")，標價一包售5.5元，但卻聲稱以「兩包特價」的價錢標價11.5元出售，比正常購買兩包的11元貴0.5元，被法庭裁定違反新修訂的《商品說明條例》，被判罰款1萬元，是修例後首宗成功檢控個案。\[9\]\[10\]

###

  - 瘦肉精爭議

2012年3月，[國民黨立委](../Page/國民黨.md "wikilink")[蔡正元展示實驗報告指出](../Page/蔡正元.md "wikilink")，多項購自頂-{}-好超市的豬肉產品，含有[瘦肉精](../Page/瘦肉精.md "wikilink")[沙丁胺醇](../Page/沙丁胺醇.md "wikilink")（Salbuterol），此物質毒性更遠高於[萊克多巴胺](../Page/萊克多巴胺.md "wikilink")。其後頂-{}-好超市隨即發表聲明稿，宣示店內發售的澳洲牛肉都不含瘦肉精，但蔡正元隨即公布民眾委託[義美食品檢驗室檢驗資訊](../Page/義美食品.md "wikilink")，顯示有5項驗出瘦肉精的肉品與內臟來自頂-{}-好超市。\[11\]及後基隆市衛生局食品衛生科稽查員更加在一月隨機抽驗牛肉中發現，一件在頂-{}-好超市售賣的牛肉對瘦肉精快篩呈陽性反應，送檢確認肉品含瘦肉精。\[12\]

  - 食品過期

2013年6月10日 [中天新聞，桃園頂好超市
食品過期4個月](https://web.archive.org/web/20141214024944/http://www.chinatimes.com/realtimenews/20130610004785-260401)

## 參考資料和註腳

## 外部連結

### （惠-{}-康-{}-超-{}-市）

  - [惠-{}-康為您送
    (香港大型網上超級市場)](https://www.wellcome.com.hk/wd2shop/zh/html/index.html?utm_source=Wikipedia&utm_medium=referral&utm_campaign=Wikipedia_Wellcome&utm_term=Wikipedia&utm_content=HK-chinese)
      - [:YouTube上的](../Page/:YouTube.md "wikilink")[惠-{}-康Wellcome（香港店）頻道](https://www.youtube.com/user/Wellcomesupermarket)

      -
### （頂-{}-好-{}-超-{}-市）

  - [頂-{}-好-{}-超-{}-市-Wellcome（台灣店）](http://www.wellcome.com.tw/)
      -
      -
[Category:1945年成立的公司](../Category/1945年成立的公司.md "wikilink")
[Category:牛奶公司](../Category/牛奶公司.md "wikilink")
[Category:香港超級市場](../Category/香港超級市場.md "wikilink")
[Category:台灣零售商](../Category/台灣零售商.md "wikilink")

1.  [香港文匯報
    市場調查機構EuroMonitor-香港超市的市佔率](http://paper.wenweipo.com/2013/07/20/YO1307200013.htm)
2.  [惠-{}-康分店位置（香港）](http://www.wellcome.com.hk/wd2shop/zh/html/customer-services/store-locator.html)
3.  [Market Place by
    Jasons分店位置（香港）](http://www.marketplacebyjasons.com/mpjshop/ch/html/others/store-locator.html)

4.  [頂好門市據點](http://www.wellcome.com.tw/CHT/HOME/Store)(台灣)
5.  [JASONS Market Place門市據點](http://www.jasons.com.tw/#index_store)(台灣)
6.  [超市特攻隊：惠康過期衛生巾現黑點或致發炎](http://hk.apple.nextmedia.com/news/art/20060410/5817016)
7.
8.
9.  [特價薯片貴過正價
    惠康違商品例罰1萬](http://www.mpfinance.com/htm/finance/20140213/news/ec_gbb1.htm)
10. [惠康認罪罰萬元網民叫好](http://www.singpao.com/xw/gat/201402/t20140213_489019.html)

11. [立委爆豬肉含瘦肉精　{頂好}：不應公佈中央未確認資訊](http://www.nownews.com/2012/03/13/11490-2794286.htm)
12. [追查瘦肉精美牛/下游查緝
    抽檢知名餐廳](http://www.libertytimes.com.tw/2012/new/mar/4/today-fo6.htm)