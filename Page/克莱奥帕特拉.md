**克婁-{巴特拉}-**（[希腊语](../Page/希腊语.md "wikilink")：****，[拉丁化](../Page/拉丁化.md "wikilink")：），或译为「-{克麗奧佩脫拉}-」、「-{克利奥帕特拉}-」或「-{克莉奧佩特拉}-」等，是一个起源于[希腊语的](../Page/希腊语.md "wikilink")[女性](../Page/女性.md "wikilink")[名字](../Page/名字.md "wikilink")，意为“父亲的荣耀”，为[希腊化时代一些著名女性所常用](../Page/希腊化时代.md "wikilink")，主要集中在[古埃及的](../Page/古埃及.md "wikilink")[托勒密王朝](../Page/托勒密王朝.md "wikilink")。相对的，其[男性形式为](../Page/男性.md "wikilink")**克莱奥帕特罗斯**（希腊语：****，拉丁化：）。在希腊语中，意为“荣耀”，是（父亲）的[属格](../Page/属格.md "wikilink")，意为“父亲的”，两词拼在一起即为“父亲的荣耀”。

## 起源

“克莱奥帕特拉”最早出现于[希腊神话中](../Page/希腊神话.md "wikilink")，是几个英雄的妻子们的名字。关于希腊神话中的克莱奥帕特拉，请参见主题条目：

  - [克勒俄帕特拉](../Page/克勒俄帕特拉.md "wikilink")

## 著名人士

名叫克利奥帕特拉的著名人物有：

  - [克娄巴特拉·欧律狄刻](../Page/克娄巴特拉·欧律狄刻.md "wikilink")：[马其顿王国](../Page/马其顿王国.md "wikilink")[贵妇](../Page/贵族.md "wikilink")，[马其顿国王](../Page/马其顿国王.md "wikilink")[腓力二世最后一位](../Page/腓力二世_\(马其顿\).md "wikilink")[妻子和](../Page/妻子.md "wikilink")[王妃](../Page/王妃.md "wikilink")。
  - [马其顿的克娄巴特拉](../Page/馬其頓的克麗奧佩脫拉.md "wikilink")：马其顿王国[公主](../Page/公主.md "wikilink")，腓力二世与[奧林匹亞絲的女儿](../Page/奧林匹亞絲.md "wikilink")，[亚历山大大帝的姊妹](../Page/亚历山大大帝.md "wikilink")。
  - [克娄巴特拉一世](../Page/克娄巴特拉一世.md "wikilink")：[埃及托勒密王朝](../Page/埃及.md "wikilink")[女王](../Page/女王.md "wikilink")，[叙利亚](../Page/叙利亚.md "wikilink")[塞琉古王朝国王](../Page/塞琉古王朝.md "wikilink")[安条克大帝之女](../Page/安条克大帝.md "wikilink")，托勒密王朝的第一位克利奥帕特拉。
  - [克娄巴特拉二世](../Page/克娄巴特拉二世.md "wikilink")：埃及托勒密王朝女王。
  - [克娄巴特拉三世](../Page/克娄巴特拉三世.md "wikilink")：埃及托勒密王朝女王。
  - [克娄巴特拉四世](../Page/克娄巴特拉四世.md "wikilink")：埃及托勒密王朝女王。
  - [贝勒尼基三世·克娄巴特拉](../Page/贝勒尼基三世.md "wikilink")：埃及托勒密王朝女王。
  - [克娄巴特拉五世](../Page/克娄巴特拉五世.md "wikilink")：埃及托勒密王朝女王。
  - [克娄巴特拉六世](../Page/克娄巴特拉六世.md "wikilink")：埃及托勒密王朝一位身份不明的女王。
  - [贝勒尼基四世·克娄巴特拉](../Page/贝勒尼基四世.md "wikilink")：埃及托勒密王朝女王。
  - [克娄巴特拉七世](../Page/克娄巴特拉七世.md "wikilink")：埃及托勒密王朝末代女王，最著名的一位克娄巴特拉，即通常所说的“[埃及艳后](../Page/埃及艳后.md "wikilink")”。
  - [克娄巴特拉·忒娅](../Page/克娄巴特拉·忒娅.md "wikilink")：埃及托勒密王朝公主、叙利亚塞琉古王朝王后。
  - [克娄巴特拉·塞勒涅一世](../Page/克娄巴特拉·塞勒涅一世.md "wikilink")：埃及托勒密王朝公主、[王后](../Page/王后.md "wikilink")，叙利亚塞琉古王朝王后、[摄政](../Page/摄政.md "wikilink")“[太后](../Page/太后.md "wikilink")”，[托勒密八世与克利奥帕特拉三世的女儿](../Page/托勒密八世.md "wikilink")。
  - [克娄巴特拉·塞勒涅二世](../Page/克娄巴特拉·塞勒涅二世.md "wikilink")：或[克娄巴特拉八世](../Page/克娄巴特拉八世.md "wikilink")，埃及托勒密王朝末代公主，[努米底亚和](../Page/努米底亚.md "wikilink")[毛里塔尼亚女王](../Page/毛里塔尼亚.md "wikilink")，克娄巴特拉七世与[马克·安东尼的女儿](../Page/马克·安东尼.md "wikilink")。
  - [毛里塔尼亚的克娄巴特拉](../Page/毛里塔尼亚的克娄巴特拉.md "wikilink")：或[克娄巴特拉九世](../Page/克娄巴特拉九世.md "wikilink")，可能存在的毛里塔尼亚公主。
  - [本都的克娄巴特拉](../Page/本都的克娄巴特拉.md "wikilink")：[本都王国公主](../Page/本都王国.md "wikilink")，[亚美尼亚国王](../Page/亚美尼亚.md "wikilink")[提格兰大帝的妻子](../Page/提格兰大帝.md "wikilink")。
  - [炼金术士克丽奥佩脱拉](../Page/炼金术士克丽奥佩脱拉.md "wikilink")：埃及女性炼金术士和哲学家。

## 参见

  - [克娄巴特拉·塞勒涅](../Page/克娄巴特拉·塞勒涅.md "wikilink")
  - [埃及艳后 (消歧义)](../Page/埃及艳后_\(消歧义\).md "wikilink")

[Category:各语言人名](../Category/各语言人名.md "wikilink")