**最後的審判**（[希伯来语](../Page/希伯来语.md "wikilink")：****；[阿拉伯语](../Page/阿拉伯语.md "wikilink")：****；[英语](../Page/英语.md "wikilink")：****），或者稱為**大審判**、**末日審判**,是一種宗教思想，在[世界末日之時](../Page/世界末日.md "wikilink")[神会出現](../Page/神.md "wikilink")，將死者復生并對他們進行裁決，分為[永生者和打入](../Page/永生.md "wikilink")[地獄者](../Page/地獄.md "wikilink")。這種觀點最初起源於[瑣羅亞斯德教的教義](../Page/琐罗亚斯德教.md "wikilink")：创世的阿胡拉·玛兹达将会在末日时审判世界。此概念后来深深影响了[犹太教](../Page/犹太教.md "wikilink")，后来更为后起之[基督教及](../Page/基督教.md "wikilink")[伊斯兰教所继承](../Page/伊斯兰教.md "wikilink")。

## 埃及神話中的大審判

对古埃及人来说，[心脏记录了一个人一生中的所有善行和恶行](../Page/心脏.md "wikilink")。一个人死后，在[审判厅](../Page/审判厅.md "wikilink")（Hall
of
Judgement）中会对他进行一场审判仪式，而他的心脏将被作为审判的主要依据。死者被狼头死神[阿努比斯](../Page/阿努比斯.md "wikilink")（Anubis）引入这个大厅，心脏被放在天平上，与[玛特](../Page/玛特.md "wikilink")（Maat）神的[真实之羽作重量方面的对比](../Page/真实之羽.md "wikilink")。接著阿努比斯调整天平的[铅垂](../Page/铅垂.md "wikilink")，[图特](../Page/图特.md "wikilink")（Thoth）神记录下裁决的结果（而根據[三一律](../Page/三一律.md "wikilink")，所稱量得出的結果只有三種：“真實之羽的重量
= 死者心臟的重量”或“死者心臟的重量 \< 真實之羽的重量”或“死者心臟的重量 \> 真實之羽的重量”）。经过裁决，如果在“真實之羽的重量 ≥
死者心臟的重量”的情況下，死者就會受到庇护——死者會被[阿努比斯引到](../Page/阿努比斯.md "wikilink")[欧西里斯](../Page/欧西里斯.md "wikilink")（Osiris）的面前，從而得到永生；如果死者心臟的重量
\>
真實之羽的重量（因為罪惡作祟的緣故），死者就會受到制裁——死者的心脏会被一只长着鳄鱼头、狮子的上身和河马后腿的恶魔吞噬者[阿米特](../Page/阿米特.md "wikilink")（Amit）所吞食。

## 亞伯拉罕諸教的大審判

### 犹太教中记载的大審判

### 基督教中记载的大審判

### 伊斯兰教中记载的大審判

#### 古兰经中的经文

我嘱告你们和我自己要敬畏[安拉](../Page/安拉.md "wikilink")，为那众生将长时间肃立见主之日（复生日）做准备，至尊主说：“信士们啊！你们应当敬畏[安拉](../Page/安拉.md "wikilink")，每个人都要考虑自己为明天准备了什么。要敬畏安拉！安拉彻知你们的一切行为。”（《古兰经》59：18）

世界末日和后世复生是极其严重的大事，在天经和圣训中对此有大篇幅的预警，名称繁多描述各异，都形容其恐怖至极，令信士听后胆战心惊。所以，你们要虔诚敬意地为此多做准备：“那些因担心去见主而施该舍之物的人，是争先行善而获胜之人。”（《古兰经》23：60－61）

伪信者和不信者否定后世，他们的下场必然是受辱和遭殃：“那些否定会见[安拉的人确实亏本了](../Page/安拉.md "wikilink")！等到那一时刻突然降临时，他们才会说：‘哎呀！可惜我们对此麻痹大意了。’他们背负着自己的罪孽，多么可恶的重负啊！”（《古兰经》6：31）

#### 圣训录

东方出现[马赫迪](../Page/马赫迪.md "wikilink")（救世者）。他在[天房前得到人民的宣誓效忠](../Page/克尔白.md "wikilink")，使人间久违的公正和正义充满大地，他在世期间全世界一直享受主的福佑和慈悯。圣使说：“我的人民末期，将出现迈赫迪，安拉给他降下及时雨，大地物产丰富，财富分配公平，牲畜繁多，人民强盛，他将生活七八年。”（艾卜赛义德·呼德瑞传述《哈克目圣训录》）

[伪基督](../Page/敌基督.md "wikilink")[麦西哈](../Page/弥赛亚.md "wikilink")[丹扎里出现](../Page/丹扎里.md "wikilink")。他妖言惑众并能显示许多奇异现象，是人们叛主和迷误的罪魁祸首，他是个独眼龙，眉间写着“叛主”一词。圣使在描述其异象时曾说：“……然后他来蛊惑一群民众，人们都相信并响应他，他便呼风唤雨命地产物，牲畜丰产，膘肥体壮奶足；然后他去蛊惑另一群民众，遭拒绝后愤然离去，那些不听他话的人却变得一贫如洗。他在经过一处废墟时对它说：‘现出你的宝藏来！’于是宝藏就像蜂群围着蜂王一样随他而去；他唤来一个朝气蓬勃的青年，一剑将他劈成两半，然后召唤一声，那青年便又满面笑容地应声而来。就在他胡作非为之际，[玛利亚之子](../Page/圣母玛利亚.md "wikilink")[耶稣奉](../Page/耶稣.md "wikilink")[安拉之命而来](../Page/安拉.md "wikilink")。……”（南瓦斯·本塞目安传述《穆斯林圣训录》）

先知对伪[基督的危害性做了详细的描述](../Page/基督.md "wikilink")，并对人们进行了严厉的警告，他要求我们在礼拜时求主护佑免遭此魔头的伤害，他说：“你们在拜中念完作证词后，求主保佑免遭四项灾难之害，说：‘主啊！求您保佑我免遭火狱之灾、免遭坟墓之灾、免遭生死痛苦之灾、免遭伪[基督之灾](../Page/基督.md "wikilink")。’”（艾卜胡莱赖传述《穆斯林圣训录》）

又说：“谁背诵了《山洞章》中的前十节经文，谁就能免遭伪[基督之害](../Page/基督.md "wikilink")。”（艾卜代尔达尔传述《穆斯林圣训录》）

[玛利亚之子降生](../Page/圣母玛利亚.md "wikilink")。他奉命从天上灵魂与肉体一同降生，在世间主持公道、灭猪、打碎[十字架](../Page/十字架.md "wikilink")、杀死伪[基督](../Page/基督.md "wikilink")，凡是信奉天经的人都追随他，全都皈依[伊斯兰](../Page/伊斯兰.md "wikilink")，[安拉将给所有的时间和食物降福](../Page/安拉.md "wikilink")。

耶尔朱哲和迈尔朱哲出世。这是一群人数众多势力强大的民众，无人能够打败他们，他们的面孔像盾牌，宽脸塌鼻，他们每经过一地，水源必被吸干，建筑被毁殆尽。最后，女先知祈求安拉消灭耶尔朱哲和迈尔朱哲，于是[安拉用缠绕其脖子的寄生虫消灭他们](../Page/安拉.md "wikilink")，然后又派飞鸟将其衔到至尊主意欲之地。

东方沉陷，西方沉陷。大面积地陷是主对人类的警告和惩罚，人类对此惊恐万分，纷纷向主讨饶。

[阿拉伯半岛沉陷](../Page/阿拉伯半岛.md "wikilink")。圣伴侯载法·本吴塞德·给法瑞说：“先知看到我们讨论问题，便问：‘你们在讨论什么？’我们说在讨论世界末日，先知说：‘世界末日不发生，除非先看见十大征兆。’他提到了烟雾、伪基督、怪兽、太阳从西方升起、玛丽亚之子降生、耶尔朱哲和迈尔朱哲、东方沉陷、西方沉陷、阿拉伯半岛沉陷，最后是也门喷出烈火，将人们赶到一个集合的地方。”（《穆斯林圣训录》）

非太阳光芒从東方升起。淡忘信仰的人们以为这是忏悔的最佳时机，其实此时忏悔已经来不及了，圣使说：“末日不发生，直到太阳从東边升起，当世人目睹火燒烈焰在太阳下从升起时全都皈依了，而此时的忏悔却于事无补，正所谓：‘以前不信或虽信而未做好事之人，此时其信仰已无济于事。（6：158）’”（艾卜胡莱赖传述《两大圣训录》）

怪兽出现。至尊主说：“当对他们的预言发生时，我从地上生出一种怪兽对他们讲话，因为世人已不信我的启示。”（《古兰经》27章82节）那是一个会说话的怪兽，在世界末日前夕前来谴责世人，它擦亮信士的脸以标上信主的标志，在不信者的鼻子上印上不信主的标志。

《[穆斯林聖訓實錄](../Page/穆斯林聖訓實錄.md "wikilink")》41:6985：「[復生日不會來臨](../Page/復生日.md "wikilink")，直到穆斯林殺戮[猶太人](../Page/猶太人.md "wikilink")。當穆斯林追殺猶太人，他們藏於石頭和樹木後時，石頭和樹木就會喊：『穆斯林！安拉的僕民！我後面的就是猶太人，快來殺他！』但[厄爾蓋德樹不會這樣](../Page/厄爾蓋德樹.md "wikilink")，因為它是猶太人的樹。」\[1\]

## 艺术作品

  - 16世纪的[米开朗基罗为罗马](../Page/米开朗基罗.md "wikilink")[西斯廷礼拜堂创作的](../Page/西斯廷礼拜堂.md "wikilink")[壁画](../Page/壁画.md "wikilink")《[最后的审判](../Page/最后的审判_\(米开朗琪罗\).md "wikilink")》
  - Jean Cousin 《最后的审判》

<File:The> Last Judgement. Jean Cousin..jpg|Jean Cousin的作品

## 大众文化

  - 好莱坞系列电影《[终结者](../Page/终结者.md "wikilink")》中，[天网对人类发动的](../Page/天网.md "wikilink")[核战争被称为](../Page/核战争.md "wikilink")“审判日”。
  - [布萊德彼特演的電影](../Page/布萊德彼特.md "wikilink")《[末日之戰](../Page/末日之戰_\(電影\).md "wikilink")》中，全地球被病毒感染，似乎暗諷或影攝「最後的審判」。

## 参考文献

## 外部連結

  - [*Catholic Encyclopedia* "General
    Judgment"](http://www.newadvent.org/cathen/08552a.htm)
  - [Judgment Day Past and
    Future](http://www.life.com/gallery/60451/judgment-day-past-and-future#index/0)
    – slideshow by *[《生活》](../Page/生活_\(雜誌\).md "wikilink")*
  - Swedenborg, E. [*The Last Judgment and Babylon Destroyed. All the
    Predictions in the Apocalypse are at This Day
    Fulfilled*](http://swedenborgdigitallibrary.org/contets/LJ.html)
    (Swedenborg Foundation 1951)

## 参见

  - [神义论](../Page/神义论.md "wikilink")、[状告上帝](../Page/状告上帝.md "wikilink")
  - [末世论](../Page/末世论.md "wikilink")
  - [世界末日](../Page/世界末日.md "wikilink")
  - [救世主](../Page/救世主.md "wikilink")、[弥赛亚](../Page/弥赛亚.md "wikilink")、[千年王国](../Page/千年王国.md "wikilink")
  - [犹太教末世论](../Page/犹太教末世论.md "wikilink")
  - [基督教末世论](../Page/基督教末世论.md "wikilink")：[橄榄山讲论](../Page/橄榄山讲论.md "wikilink")、[分羊的比喻](../Page/分羊的比喻.md "wikilink")、[大灾难](../Page/大灾难.md "wikilink")
  - [伊斯兰教末世论](../Page/伊斯兰教末世论.md "wikilink")

{{-}}

[Category:审判](../Category/审判.md "wikilink")
[Category:启示录中的事件](../Category/启示录中的事件.md "wikilink")
[最后的审判](../Category/最后的审判.md "wikilink")
[Category:圣经短语](../Category/圣经短语.md "wikilink")
[Category:耶稣的教义和教诲](../Category/耶稣的教义和教诲.md "wikilink")
[Category:犹太教末世论](../Category/犹太教末世论.md "wikilink")
[Category:基督教末世论](../Category/基督教末世论.md "wikilink")
[Category:伊斯兰教末世论](../Category/伊斯兰教末世论.md "wikilink")
[Category:伊斯蘭教與其他宗教](../Category/伊斯蘭教與其他宗教.md "wikilink")
[Category:犹太教和基督教话题](../Category/犹太教和基督教话题.md "wikilink")
[Category:基督教术语](../Category/基督教术语.md "wikilink")

1.  [The Book Pertaining to the Turmoil and Portents of the Last Hour
    (Kitab Al-Fitan wa Ashrat
    As-Sa\`ah)](http://www.usc.edu/org/cmje/religious-texts/hadith/muslim/041-smt.php)
    , Translation of Sahih Muslim, Book 41.