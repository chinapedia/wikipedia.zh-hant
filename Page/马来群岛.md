[Malayarch.jpg](https://zh.wikipedia.org/wiki/File:Malayarch.jpg "fig:Malayarch.jpg")

**馬來群島**（）\[1\]是一组散布于[印度洋和](../Page/印度洋.md "wikilink")[太平洋上](../Page/太平洋.md "wikilink")，位於[东南亚](../Page/东南亚.md "wikilink")[大陸和](../Page/大陸.md "wikilink")[澳大利亚之间的群島](../Page/澳大利亚.md "wikilink")。該群島由2万多个岛屿组成，是世界上[面积最大的](../Page/面积.md "wikilink")[群岛](../Page/群岛.md "wikilink")。群岛上的[国家有](../Page/国家.md "wikilink")[印度尼西亚](../Page/印度尼西亚.md "wikilink")、[菲律宾](../Page/菲律宾.md "wikilink")、[马来西亚](../Page/马来西亚.md "wikilink")（[东马](../Page/东马.md "wikilink")）
、[文莱](../Page/文莱.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")、[东帝汶](../Page/东帝汶.md "wikilink")\[2\]。[新幾內亞島](../Page/新幾內亞.md "wikilink")（包含[巴布亚新几内亚](../Page/巴布亚新几内亚.md "wikilink")）一般不納入馬來群島之範疇\[3\]\[4\]。

## 詞源及用法

「馬來群島」此常用名源於[馬來人種](../Page/馬來人種.md "wikilink")（Malay
race）概念\[5\]，該種族涵蓋今日的[印尼](../Page/印尼.md "wikilink")、[馬來西亞和](../Page/馬來西亞.md "wikilink")[菲律賓民族](../Page/菲律賓.md "wikilink")。[歐洲](../Page/歐洲.md "wikilink")[殖民主義者從對](../Page/殖民主義.md "wikilink")[馬來族](../Page/馬來族.md "wikilink")[帝國](../Page/帝國.md "wikilink")[三佛齊影響的觀察](../Page/三佛齊.md "wikilink")，提出了這個種族概念\[6\]。

19世紀博物學家[阿尔弗雷德·拉塞尔·华莱士](../Page/阿尔弗雷德·拉塞尔·华莱士.md "wikilink")（Alfred
Wallace）把「馬來群島」一詞用作其重要著作《[馬來群島](../Page/馬來群島_\(書籍\).md "wikilink")》的名稱。該書記載了他在該地區的研究。華萊士以「印度群島」和「印度-澳大利亞」群島指稱當地\[7\]。他基於地形的相似性，把[所羅門群島和](../Page/所羅門群島.md "wikilink")[馬來半島納入該地區](../Page/馬來半島.md "wikilink")\[8\]正。如華萊士所指\[9\]，應否以文化和地理因素撇除巴布新幾內亞仍有爭論：巴布新幾內亞的文化與該地區其他國家相當不同，而[新幾內亞島地理上不是](../Page/新幾內亞.md "wikilink")[亞洲大陸一部分](../Page/亞洲.md "wikilink")；[巽他陸架](../Page/巽他陸架.md "wikilink")（Sunda
Shelf）上的島嶼才是（另見[澳大利亞大陸](../Page/澳大利亞大陸.md "wikilink")）。

馬來群島在歐洲殖民時代曾稱為「[東印度](../Page/東印度.md "wikilink")」\[10\]，現在偶會這樣指稱\[11\]，但历史上，「东印度」的[地域概念比较松散](../Page/地域.md "wikilink")，既适用于现在的[印度尼西亚共和国](../Page/印度尼西亚共和国.md "wikilink")（前[荷属东印度](../Page/荷属东印度.md "wikilink")），也可包括马来群岛（[菲律宾属于本](../Page/菲律宾.md "wikilink")[群岛](../Page/群岛.md "wikilink")），广义的「東印度」還包括[中南半島](../Page/中南半島.md "wikilink")（又稱[印度支那](../Page/印度支那.md "wikilink")）和[印度次大陸](../Page/印度次大陸.md "wikilink")，以至伸延至整个[东南亚和](../Page/东南亚.md "wikilink")[印度](../Page/印度.md "wikilink")。印尼人則以「[努沙登加拉](../Page/马来世界.md "wikilink")」（Nusantara）一詞表示「馬來群島」\[12\]。該地區也有「印度尼西亞群島」一稱\[13\]\[14\]。

## 區劃

马来群岛的大部分[岛屿](../Page/岛屿.md "wikilink")，介于[亚洲大陆](../Page/亚洲大陆.md "wikilink")（[西北](../Page/西北.md "wikilink")）和[澳大利亚](../Page/澳大利亚.md "wikilink")（[东南](../Page/东南.md "wikilink")）之间，沿[赤道伸展六千一百里](../Page/赤道.md "wikilink")（3,800哩）以上的宽阔地带。包括[婆罗洲](../Page/婆罗洲.md "wikilink")、[西里伯斯](../Page/西里伯斯.md "wikilink")、[爪哇及](../Page/爪哇.md "wikilink")[苏门答腊几座主要](../Page/苏门答腊.md "wikilink")[岛屿](../Page/岛屿.md "wikilink")。[政治上包括](../Page/政治.md "wikilink")[新加坡](../Page/新加坡.md "wikilink")、[汶莱](../Page/汶莱.md "wikilink")、[印度尼西亚](../Page/印度尼西亚.md "wikilink")、[马来西亚](../Page/马来西亚.md "wikilink")、[菲律宾和](../Page/菲律宾.md "wikilink")[东帝汶六国](../Page/东帝汶.md "wikilink")。有时也会把[美拉尼西亚群岛的](../Page/美拉尼西亚群岛.md "wikilink")[巴布亚新几内亚包括进来](../Page/巴布亚新几内亚.md "wikilink")。

## 地理及地質

馬來群島的水陸面積逾200萬[平方公里](../Page/平方公里.md "wikilink")\[15\]，将[印度洋](../Page/印度洋.md "wikilink")（南）和[太平洋](../Page/太平洋.md "wikilink")（北、东）分隔开，[地形可分不同的两部分](../Page/地形.md "wikilink")︰涵盖[苏门答腊](../Page/苏门答腊.md "wikilink")、[婆罗洲](../Page/婆罗洲.md "wikilink")、[爪哇和部分](../Page/爪哇.md "wikilink")[新几内亚岛的](../Page/新几内亚岛.md "wikilink")[大陆棚](../Page/大陆棚.md "wikilink")；而[西部的](../Page/西部.md "wikilink")[巴里与](../Page/巴里.md "wikilink")[婆罗洲之间](../Page/婆罗洲.md "wikilink")，东部的[阿鲁](../Page/阿鲁.md "wikilink")（Aru）与[新几内亚](../Page/新几内亚.md "wikilink")[东部之间的](../Page/东部.md "wikilink")[岛屿则为新](../Page/岛屿.md "wikilink")[褶曲](../Page/褶曲.md "wikilink")[海底](../Page/海底.md "wikilink")[山脉](../Page/山脉.md "wikilink")。

本[地区的](../Page/地区.md "wikilink")[核心是](../Page/核心.md "wikilink")[巽他台地](../Page/巽他台地.md "wikilink")，[台地上面是浅海](../Page/台地.md "wikilink")，[台地亦称](../Page/台地.md "wikilink")[巽他陆棚](../Page/巽他陆棚.md "wikilink")（Sunda
Shelf），[巽他陆棚是](../Page/巽他陆棚.md "wikilink")[东南亚](../Page/东南亚.md "wikilink")[大陆延伸的稳定地块](../Page/大陆.md "wikilink")。邻近的[莎湖陆棚](../Page/莎湖陆棚.md "wikilink")，[新几内亚及其附属](../Page/新几内亚.md "wikilink")[岛屿是](../Page/岛屿.md "wikilink")[莎湖陆棚的一部分](../Page/莎湖陆棚.md "wikilink")，同样也是[澳大利亚大陆的延伸](../Page/澳大利亚大陆.md "wikilink")。在这[核心周围](../Page/核心.md "wikilink")，本[地区的其馀部分则是](../Page/地区.md "wikilink")[陆棚外缘一系列](../Page/陆棚.md "wikilink")[连续](../Page/连续.md "wikilink")[褶皱运动逐步形成的](../Page/褶皱运动.md "wikilink")，更强烈和广泛的造山幕发生于[中生代](../Page/中生代.md "wikilink")[时期](../Page/时期.md "wikilink")。位于许多[板块的会合处](../Page/板块.md "wikilink")，故而地质运动颇为剧烈，火山活动相当的活跃，整個[地区各处散布著二百多座](../Page/地区.md "wikilink")[火山](../Page/火山.md "wikilink")，其中约七十座在[印度尼西亚](../Page/印度尼西亚.md "wikilink")、过去一百五十年间曾喷发过。在四个主要[冰期](../Page/冰期.md "wikilink")，当时[海平面比现在低过一百](../Page/海平面.md "wikilink")[公尺](../Page/公尺.md "wikilink")（300呎），巽他与莎湖两[陆棚露出海面](../Page/陆棚.md "wikilink")，[陆棚地面大河奔流](../Page/陆棚.md "wikilink")，当时的[动物和](../Page/动物.md "wikilink")[古人类可藉陆桥穿越此处](../Page/古人类.md "wikilink")。当一萬七千年前[冰盖融解後](../Page/冰盖.md "wikilink")，[巽他陆棚一部分被淹没](../Page/巽他陆棚.md "wikilink")，[山脉和较高的](../Page/山脉.md "wikilink")[地方成了浅海浸入的](../Page/地方.md "wikilink")[岛屿或](../Page/岛屿.md "wikilink")[半岛](../Page/半岛.md "wikilink")。

马来群岛地处热带，赤道横穿整个群岛，群岛大部为热带雨林所覆盖。群島的25,000個島嶼包括了許多較小的群島\[16\]。其中最大的群島如下：

  - [巽他群島](../Page/巽他群島.md "wikilink")
      - [大巽他群島](../Page/大巽他群島.md "wikilink")
      - [小巽他群島](../Page/小巽他群島.md "wikilink")
  - [摩鹿加群島](../Page/摩鹿加群島.md "wikilink")
  - [菲律賓群島](../Page/菲律賓群島.md "wikilink")

五大島嶼包別是[蘇門達臘島](../Page/蘇門達臘島.md "wikilink")、[婆羅洲](../Page/婆羅洲.md "wikilink")、[蘇拉威西島](../Page/蘇拉威西島.md "wikilink")、[爪哇島](../Page/爪哇島.md "wikilink")、[呂宋島和](../Page/呂宋島.md "wikilink")[棉蘭老島](../Page/棉蘭老島.md "wikilink")。另外一个有时会被包含进来的[新幾內亞一般被归属于](../Page/新幾內亞.md "wikilink")[大洋洲](../Page/大洋洲.md "wikilink")[美拉尼西亚群岛](../Page/美拉尼西亚群岛.md "wikilink")。

馬來群島地理上是世界上最多活[火山的地區之一](../Page/火山.md "wikilink")。[構造抬升產生了高山](../Page/構造抬升.md "wikilink")，最高者有[沙巴的](../Page/沙巴.md "wikilink")[京那巴魯山](../Page/京那巴魯山.md "wikilink")（海拔4,095.2米）；如果计入新几内亚，则是[巴布亞的](../Page/巴布亞省.md "wikilink")[查亞峰](../Page/查亞峰.md "wikilink")（[海拔](../Page/海拔.md "wikilink")4,884[米](../Page/米.md "wikilink")）最高。群島地處[赤道之上](../Page/赤道.md "wikilink")，屬於[熱帶](../Page/熱帶.md "wikilink")[氣候](../Page/氣候.md "wikilink")。

[Línea_de_Wallace.jpg](https://zh.wikipedia.org/wiki/File:Línea_de_Wallace.jpg "fig:Línea_de_Wallace.jpg")和[龍目島之間的龍目海峽的深海形成了一道水障](../Page/龍目島.md "wikilink")，這景象在低海平面連接現時分隔兩邊的島嶼和陸地時仍可看到。\]\]

### 生物地理

華萊士以「馬來群島」一詞作為其記錄當地研究的重要著作的名稱。他注意到似乎有一條隱形的界線將亞洲和澳洲的動植物分開。為紀念他的發現，科學界將劃分這兩區的界線稱為[華萊士線](../Page/華萊士線.md "wikilink")。[冰河時期的分界線由](../Page/冰河時期.md "wikilink")[婆羅洲和](../Page/婆羅洲.md "wikilink")[蘇拉威西島之間的深海海峽形成](../Page/蘇拉威西島.md "wikilink")，並穿過[峇里島和](../Page/峇里島.md "wikilink")[龍目島之間的](../Page/龍目島.md "wikilink")[龍目海峽](../Page/龍目海峽.md "wikilink")。過渡區現稱為[華萊士區](../Page/華萊士區.md "wikilink")（Wallacea
Zone），擁有源於[亞洲和](../Page/亞洲.md "wikilink")[澳洲的混合種](../Page/澳洲.md "wikilink")，以及當地的特有種。

## 物种

### 动物

**马来群岛**的[动物区系也颇丰富](../Page/动物区系.md "wikilink")，但大型[动物不多](../Page/动物.md "wikilink")，[象](../Page/象.md "wikilink")、[虎](../Page/虎.md "wikilink")、[犀牛](../Page/犀牛.md "wikilink")、[野牛](../Page/野牛.md "wikilink")、[貘及](../Page/貘.md "wikilink")[猿都属于亚洲种](../Page/猿.md "wikilink")。区系[动物在巽他](../Page/动物.md "wikilink")[陆棚](../Page/陆棚.md "wikilink")[岛屿的分布不均匀](../Page/岛屿.md "wikilink")，[标志著这些](../Page/标志.md "wikilink")[岛屿在不同地质时期的联接或分离](../Page/岛屿.md "wikilink")。[澳大利亚的区系动物](../Page/澳大利亚.md "wikilink")，[袋鼠在](../Page/袋鼠.md "wikilink")[新几内亚有](../Page/新几内亚.md "wikilink")[发现](../Page/发现.md "wikilink")，其他[有袋类分布越过](../Page/有袋类.md "wikilink")[莎湖陆棚远达](../Page/莎湖陆棚.md "wikilink")[帝汶和](../Page/帝汶.md "wikilink")[西里伯斯岛](../Page/西里伯斯岛.md "wikilink")，两岛之间的某些[岛屿则有风土种如矮水牛以及](../Page/岛屿.md "wikilink")[小巽他群岛的](../Page/小巽他群岛.md "wikilink")[科莫多岛](../Page/科莫多岛.md "wikilink")（Komodo
Island，弗洛里斯岛〔Flores〕附近）上的[巨蜥](../Page/巨蜥.md "wikilink")。[群岛也许拥有全世界最丰富的](../Page/群岛.md "wikilink")[昆虫和](../Page/昆虫.md "wikilink")[鸟类](../Page/鸟类.md "wikilink")，它们分布范围更广泛，特别引人注目的是大螯甲虫、巨蝶、羽色绚丽的[极乐鸟](../Page/极乐鸟.md "wikilink")、俊美的[孔雀雉和斑斓的美冠](../Page/孔雀.md "wikilink")[鹦鹉](../Page/鹦鹉.md "wikilink")。

### 植物

在大部分馬來群岛上，大雨和高温导致[植物全年快速生长](../Page/植物.md "wikilink")，[种群丰富](../Page/种群.md "wikilink")。[雨水充沛](../Page/雨水.md "wikilink")[地区覆被著](../Page/地区.md "wikilink")[热带长绿](../Page/热带.md "wikilink")[雨林](../Page/雨林.md "wikilink")，比较乾爽的[地区有半落叶季风林](../Page/地区.md "wikilink")，包括[柚木和](../Page/柚木.md "wikilink")[桉树](../Page/桉树.md "wikilink")，这类[森林主要呈季节性生长](../Page/森林.md "wikilink")。其他[森林](../Page/森林.md "wikilink")[类型有海滨林](../Page/类型.md "wikilink")（[红树林](../Page/红树林.md "wikilink")、[水椰子](../Page/水椰子.md "wikilink")、露兜）、沼泽林及山地林。有一百五十多种[棕榈](../Page/棕榈.md "wikilink")，包括[椰子](../Page/椰子.md "wikilink")、糖棕、西谷棕、[槟榔等](../Page/槟榔.md "wikilink")。[竹子分布于全区](../Page/竹子.md "wikilink")。大片[自然林被](../Page/自然林.md "wikilink")[人类](../Page/人类.md "wikilink")[农耕反覆清除](../Page/农耕.md "wikilink")，现在主要孳生著白茅（Imperata
cylindrica）。

馬來群岛的[植物区系是世界上最丰富的](../Page/植物区系.md "wikilink")，[乔木](../Page/乔木.md "wikilink")、[灌木和草类多达三萬种以上](../Page/灌木.md "wikilink")，属于二千五百多个已鉴定的[科](../Page/科.md "wikilink")。[种群丰富是](../Page/种群.md "wikilink")[雨水充沛](../Page/雨水.md "wikilink")、生长期长、[地形差异与](../Page/地形.md "wikilink")[环境](../Page/环境.md "wikilink")[复杂的结果](../Page/复杂.md "wikilink")，也是馬來群岛作为[亚](../Page/亚.md "wikilink")、[澳两](../Page/澳.md "wikilink")[大陆之间的陆桥](../Page/大陆.md "wikilink")[位置等作用综合形成的](../Page/位置.md "wikilink")。两[大陆](../Page/大陆.md "wikilink")[动植物在](../Page/动植物.md "wikilink")[群岛上发生](../Page/群岛.md "wikilink")[混合](../Page/混合.md "wikilink")。

## 地貌

本地区[群岛](../Page/群岛.md "wikilink")[地形以崎岖的](../Page/地形.md "wikilink")[山地为骨架](../Page/山地.md "wikilink")，上覆著[火山带](../Page/火山带.md "wikilink")，许多[火山锥高出三千公尺](../Page/火山锥.md "wikilink")（一萬呎）。在稳定的巽他[陆棚各地](../Page/陆棚.md "wikilink")，山坡长期遭受[风化](../Page/风化.md "wikilink")，轮廓平缓；而不稳定地区[山脉](../Page/山脉.md "wikilink")[地形很不规则](../Page/地形.md "wikilink")。有些[岛屿有广阔的](../Page/岛屿.md "wikilink")[河口湾](../Page/河口湾.md "wikilink")，而许多浅海沿岸，如[苏门答腊东岸](../Page/苏门答腊.md "wikilink")、[婆罗洲西岸和南岸](../Page/婆罗洲.md "wikilink")、[新几内亚西南岸](../Page/新几内亚.md "wikilink")，拥有大片沼泽带，终年积水，不适人居；而[爪哇中部和东部](../Page/爪哇.md "wikilink")、[苏门答腊北部以及](../Page/苏门答腊.md "wikilink")[小巽他群岛部分地方则因地处](../Page/小巽他群岛.md "wikilink")[冲积平原且排水良好](../Page/冲积平原.md "wikilink")，故人口众多。

## 水文

[群岛的](../Page/群岛.md "wikilink")[多山](../Page/多山.md "wikilink")[地形](../Page/地形.md "wikilink")，加上充沛的年[降雨量](../Page/降雨量.md "wikilink")，使短小、坡降陡的[河流侵蚀力强](../Page/河流.md "wikilink")。[大陆棚](../Page/大陆棚.md "wikilink")[岛屿的](../Page/岛屿.md "wikilink")[河流将其侵蚀物沉积于周围浅海](../Page/河流.md "wikilink")，迅速生成[广大的](../Page/广大.md "wikilink")[氾滥平原与](../Page/氾滥平原.md "wikilink")[三角洲](../Page/三角洲.md "wikilink")，这类低地上的[河流漫长](../Page/河流.md "wikilink")，如[苏门答腊的](../Page/苏门答腊.md "wikilink")[穆西河](../Page/穆西河.md "wikilink")（Musi）、[婆罗洲的](../Page/婆罗洲.md "wikilink")[卡普阿斯河](../Page/卡普阿斯河.md "wikilink")（Kapuas）与巴里托河（Barito），以及[新几内亚的](../Page/新几内亚.md "wikilink")[塞皮克河](../Page/塞皮克河.md "wikilink")（Sepik）与弗莱河（Fly）。介于两个[陆棚之间](../Page/陆棚.md "wikilink")，从深海里陡峭[升起的](../Page/升起.md "wikilink")[岛屿](../Page/岛屿.md "wikilink")，没有机会在深海周围产生[沿海](../Page/沿海.md "wikilink")[平原和](../Page/平原.md "wikilink")[三角洲](../Page/三角洲.md "wikilink")，平坦的低地很有限，[河流一般也都短小](../Page/河流.md "wikilink")，坡降陡峻。

## 土質

本地区[土壤相当](../Page/土壤.md "wikilink")[复杂](../Page/复杂.md "wikilink")。[火山带的](../Page/火山带.md "wikilink")[土壤及其衍生的](../Page/土壤.md "wikilink")[冲积层很肥](../Page/冲积层.md "wikilink")，如[中爪哇和](../Page/中爪哇.md "wikilink")[东爪哇](../Page/东爪哇.md "wikilink")、[苏门答腊部分](../Page/苏门答腊.md "wikilink")[地区以及](../Page/地区.md "wikilink")[西里伯斯的零散](../Page/西里伯斯.md "wikilink")[地方](../Page/地方.md "wikilink")。砖红壤产生于[马来亚](../Page/马来亚.md "wikilink")、[婆罗洲及稳定的巽他](../Page/婆罗洲.md "wikilink")[陆棚其他](../Page/陆棚.md "wikilink")[岛屿](../Page/岛屿.md "wikilink")，对[农业而言](../Page/农业.md "wikilink")，[土壤贫瘠](../Page/土壤.md "wikilink")，有时简直无法利用。在巽他[陆棚](../Page/陆棚.md "wikilink")[边缘的](../Page/边缘.md "wikilink")[印度尼西亚](../Page/印度尼西亚.md "wikilink")[大岛上](../Page/大岛.md "wikilink")，有广阔的[沿海](../Page/沿海.md "wikilink")[沼泽](../Page/沼泽.md "wikilink")，[沼泽表面积聚著枯死的](../Page/沼泽.md "wikilink")[植物](../Page/植物.md "wikilink")，使原本没有[利用](../Page/利用.md "wikilink")[价值的](../Page/价值.md "wikilink")[土地](../Page/土地.md "wikilink")，具有潜在的肥力。

## 氣候

马来群岛全部位于[热带](../Page/热带.md "wikilink")，在[赤道两侧各](../Page/赤道.md "wikilink")10个[纬度之间](../Page/纬度.md "wikilink")，[气温高](../Page/气温.md "wikilink")，年[平均](../Page/平均.md "wikilink")27℃（80℉），屬於[熱帶雨林氣候](../Page/熱帶雨林氣候.md "wikilink")。年温差在[赤道上只有几度](../Page/赤道.md "wikilink")，离[赤道越远](../Page/赤道.md "wikilink")，温差逐渐增大。[温度变化也受到附近](../Page/温度.md "wikilink")[海洋的影响和调节](../Page/海洋.md "wikilink")。

影响[气候变化的因素是](../Page/气候变化.md "wikilink")[降水](../Page/降水.md "wikilink")，年[降雨量从](../Page/降雨量.md "wikilink")[苏门答腊和](../Page/苏门答腊.md "wikilink")[爪哇](../Page/爪哇.md "wikilink")[山地迎风坡的八千一百三十公釐](../Page/山地.md "wikilink")（320吋）以上，到[苏拉威西](../Page/苏拉威西.md "wikilink")[西部与](../Page/西部.md "wikilink")[小巽他群岛的雨影带不足五百公釐](../Page/小巽他群岛.md "wikilink")（20吋）。[群岛大部分年平均兩千公釐](../Page/群岛.md "wikilink")（80吋）以上，全年[均匀分布](../Page/均匀分布.md "wikilink")，但从[中爪哇往东到全部](../Page/中爪哇.md "wikilink")[小巽他群岛](../Page/小巽他群岛.md "wikilink")，年[降雨量逐步减少而](../Page/降雨量.md "wikilink")[旱季拉长](../Page/旱季.md "wikilink")。另一个[气候变化因素是](../Page/气候变化.md "wikilink")[台风](../Page/台风.md "wikilink")，每年7∼11月份，从西南[太平洋上向西和向北吹来二十多起](../Page/太平洋.md "wikilink")[台风](../Page/台风.md "wikilink")。

## 人口

馬來群島的[人口逾](../Page/人口.md "wikilink")3億，人口最稠密的島嶼為[爪哇島](../Page/爪哇島.md "wikilink")。另外，印尼約17,500個島嶼中，6,000個有人居住\[17\]。馬來群島人以[南島語族](../Page/南島語族.md "wikilink")\[18\]亞族居多，說相應的西[馬來-波里尼西亞語](../Page/馬來-波里尼西亞語.md "wikilink")。相比[東南亞大陸民族](../Page/東南亞大陸.md "wikilink")，東南亞地區與其他太平洋的南島語族有較多社會和文化聯繫。

。

該地區的主要宗教有[伊斯蘭教](../Page/伊斯蘭教.md "wikilink")、[基督宗教](../Page/基督宗教.md "wikilink")（新教、天主教）、[佛教](../Page/佛教.md "wikilink")、[中國民間信仰](../Page/中國民間信仰.md "wikilink")、[印度教](../Page/印度教.md "wikilink")，以及傳統的[自然宗教](../Page/自然宗教.md "wikilink")。

## 居民

馬來群岛的主要居民似乎是从[东南亚及南亚大陆移来的](../Page/东南亚.md "wikilink")。在本地区的原始居民中有[矮黑人](../Page/矮黑人.md "wikilink")（Negroid），亦称[尼格利陀人](../Page/尼格利陀人.md "wikilink")（Negritos）。他们被约在四千年前开始从[亚洲大陆入境的](../Page/亚洲大陆.md "wikilink")所[取代或](../Page/取代.md "wikilink")[吸收](../Page/吸收.md "wikilink")，原古马来人带来[水稻种植与](../Page/水稻.md "wikilink")[家畜饲养](../Page/家畜.md "wikilink")[技能](../Page/技能.md "wikilink")。约在西元前3世纪从[东南亚来的](../Page/东南亚.md "wikilink")[马来人接踵而至](../Page/马来人.md "wikilink")，散布到[印度尼西亚和](../Page/印度尼西亚.md "wikilink")[菲律宾各地](../Page/菲律宾.md "wikilink")，他们然後又被继来的[民族如孟](../Page/民族.md "wikilink")-高棉、[泰](../Page/泰.md "wikilink")、缅人挤出[亚洲大陆](../Page/亚洲大陆.md "wikilink")（[马来半岛除外](../Page/马来半岛.md "wikilink")）或吸收掉。馬來群岛的马来族系後来又同[阿拉伯人](../Page/阿拉伯人.md "wikilink")、[印度人及](../Page/印度人.md "wikilink")[華人有小规模的](../Page/華人.md "wikilink")[混合](../Page/混合.md "wikilink")。

馬來群岛居民讲多种[语言](../Page/语言.md "wikilink")，属于马来-波里尼西亚语系，亦称[澳斯特罗尼西亚诸语言](../Page/澳斯特罗尼西亚诸语言.md "wikilink")（Austronesian
languages）。现代居民的[多元文化主要是](../Page/多元文化.md "wikilink")[印度教](../Page/印度教.md "wikilink")、[伊斯兰教](../Page/伊斯兰教.md "wikilink")、[基督宗教与](../Page/基督宗教.md "wikilink")[佛教](../Page/佛教.md "wikilink")[文化作用的结果](../Page/文化.md "wikilink")。

## 经济

### 农业

[农业为本地区的首要](../Page/农业.md "wikilink")[经济活动](../Page/经济活动.md "wikilink")。大部分居民是[农民](../Page/农民.md "wikilink")，直接或间接以农为生，[农业](../Page/农业.md "wikilink")[经济多种多样](../Page/经济.md "wikilink")，大多数[农民是自给性的稻农](../Page/农民.md "wikilink")，种植[水稻](../Page/水稻.md "wikilink")，有时也以[玉米](../Page/玉米.md "wikilink")、[芋类或](../Page/芋.md "wikilink")[木薯作主粮](../Page/木薯.md "wikilink")。本区域的集约稻作区出现在[河流](../Page/河流.md "wikilink")[谷地](../Page/谷地.md "wikilink")、[三角洲及](../Page/三角洲.md "wikilink")[沿海沼泽带的肥沃冲积土上](../Page/沿海.md "wikilink")，而[甘蔗](../Page/甘蔗.md "wikilink")、[椰乾](../Page/椰.md "wikilink")、[香料](../Page/香料.md "wikilink")、[橡胶](../Page/橡胶.md "wikilink")、[烟草及](../Page/烟草.md "wikilink")[植物](../Page/植物.md "wikilink")[纤维等](../Page/纤维.md "wikilink")[经济作物也有小农种植](../Page/经济作物.md "wikilink")。种植园[生产](../Page/生产.md "wikilink")[价值高的出口品](../Page/价值.md "wikilink")[橡胶](../Page/橡胶.md "wikilink")、[棕油](../Page/棕油.md "wikilink")、[剑麻](../Page/剑麻.md "wikilink")、[金鸡纳霜](../Page/金鸡纳霜.md "wikilink")（[奎宁](../Page/奎宁.md "wikilink")）和[茶叶](../Page/茶叶.md "wikilink")，还有[咖啡](../Page/咖啡.md "wikilink")、[烟草和](../Page/烟草.md "wikilink")[椰乾](../Page/椰.md "wikilink")。山区游耕农在高坡上种植山稻和[玉米](../Page/玉米.md "wikilink")。[畜牧业规模小](../Page/畜牧业.md "wikilink")，除[印度尼西亚](../Page/印度尼西亚.md "wikilink")[东部](../Page/东部.md "wikilink")[群岛外](../Page/群岛.md "wikilink")，很少大群饲养。一般经常饲养的大[牲畜是](../Page/牲畜.md "wikilink")[水牛和](../Page/水牛.md "wikilink")[黄牛](../Page/黄牛.md "wikilink")，用[动物拉](../Page/动物.md "wikilink")[犁是本地区](../Page/犁.md "wikilink")[农业的基础](../Page/农业.md "wikilink")。

### 天然資源

[森林提供有价值的](../Page/森林.md "wikilink")[资源](../Page/资源.md "wikilink")，如[木材](../Page/木材.md "wikilink")、[树脂](../Page/树脂.md "wikilink")、[藤条及其他林产品](../Page/藤.md "wikilink")。[木材](../Page/木材.md "wikilink")[出口对于](../Page/出口.md "wikilink")[婆罗洲及](../Page/婆罗洲.md "wikilink")[苏门答腊欠](../Page/苏门答腊.md "wikilink")[发达的](../Page/发达.md "wikilink")[经济特别重要](../Page/经济.md "wikilink")。

#### 礦業

本地区大部分所在的巽他[陆棚是世界上高度成矿带之一](../Page/陆棚.md "wikilink")，富含[锡](../Page/锡.md "wikilink")、[金](../Page/金.md "wikilink")、[铜](../Page/铜.md "wikilink")、[锌](../Page/锌.md "wikilink")、[银](../Page/银.md "wikilink")、[铅](../Page/铅.md "wikilink")、[铁](../Page/铁.md "wikilink")、[锰](../Page/锰.md "wikilink")、[铬](../Page/铬.md "wikilink")、[钴](../Page/钴.md "wikilink")、[钒](../Page/钒.md "wikilink")、[钼和](../Page/钼.md "wikilink")[钨](../Page/钨.md "wikilink")，还有广泛分布的[石灰岩](../Page/石灰岩.md "wikilink")、[萤石](../Page/萤石.md "wikilink")、[高岭土](../Page/高岭土.md "wikilink")、[石膏](../Page/石膏.md "wikilink")、泥灰岩、[矽砂和](../Page/矽.md "wikilink")[盐](../Page/食盐.md "wikilink")。[第二次世界大战以後](../Page/第二次世界大战.md "wikilink")，各国相继[独立](../Page/独立.md "wikilink")，不仅加强[传统](../Page/传统.md "wikilink")[采矿业](../Page/采矿业.md "wikilink")，并[开发过去未曾普遍开采的各种](../Page/开发.md "wikilink")[矿产](../Page/矿产.md "wikilink")，如[石油](../Page/石油.md "wikilink")、[天然气](../Page/天然气.md "wikilink")、[铁矿石](../Page/铁矿石.md "wikilink")、铝土、[镍和](../Page/镍.md "wikilink")[铜](../Page/铜.md "wikilink")。[石油](../Page/石油.md "wikilink")[矿区分布在](../Page/矿区.md "wikilink")[汶莱](../Page/汶莱.md "wikilink")、[婆罗洲](../Page/婆罗洲.md "wikilink")[东部等处](../Page/东部.md "wikilink")，[铝土和](../Page/鋁.md "wikilink")[镍则产于](../Page/镍.md "wikilink")[印度尼西亚](../Page/印度尼西亚.md "wikilink")。[矿产](../Page/矿产.md "wikilink")[开发影响当地](../Page/开发.md "wikilink")[景观](../Page/景观.md "wikilink")，也促进[都市和](../Page/都市.md "wikilink")[交通](../Page/交通.md "wikilink")[发展](../Page/发展.md "wikilink")。

### 工業

虽然本地区不是世界大[贸易集团之一](../Page/贸易集团.md "wikilink")；但这里佔世界[天然橡胶出口量的一半以上和](../Page/天然橡胶.md "wikilink")[锡的出口量](../Page/锡.md "wikilink")1/3左右。[印尼只有](../Page/印尼.md "wikilink")1/7的[国内生产总值来自](../Page/国内生产总值.md "wikilink")[制造业](../Page/制造业.md "wikilink")，其馀[国家则佔到](../Page/国家.md "wikilink")10∼25％。[乡村](../Page/乡村.md "wikilink")[工业有](../Page/工业.md "wikilink")[橡胶和](../Page/橡胶.md "wikilink")[棕油的初加工](../Page/棕油.md "wikilink")、小规模[碾米](../Page/碾米.md "wikilink")、砖瓦业。[城市](../Page/城市.md "wikilink")[工业有大规模](../Page/工业.md "wikilink")[碾米](../Page/碾米.md "wikilink")、[橡胶二次加工](../Page/橡胶.md "wikilink")、炼锡和[纺织](../Page/纺织.md "wikilink")，还有[玻璃](../Page/玻璃.md "wikilink")、[肥皂](../Page/肥皂.md "wikilink")、[香烟及其他](../Page/香烟.md "wikilink")[消费品的](../Page/消费品.md "wikilink")[制造业](../Page/制造业.md "wikilink")。

## 历史

公元1至15世纪，[苏门答腊](../Page/苏门答腊.md "wikilink")、[爪哇和](../Page/爪哇.md "wikilink")[婆罗洲出现一些](../Page/婆罗洲.md "wikilink")[印度文化影响的](../Page/印度文化.md "wikilink")[王国](../Page/王国.md "wikilink")，相续兴起和衰亡。

14至15世纪，[伊斯兰教从](../Page/伊斯兰教.md "wikilink")[西亞和](../Page/西亞.md "wikilink")[印度半島传入](../Page/印度半島.md "wikilink")，[马来亚的](../Page/马来亚.md "wikilink")[马六甲既是本地区](../Page/马六甲.md "wikilink")[转口贸易中心](../Page/转口贸易.md "wikilink")，也是伊斯兰的传播中心。

16世纪，[欧洲人开始](../Page/欧洲人.md "wikilink")[殖民统治](../Page/殖民统治.md "wikilink")，1511年[葡萄牙人佔领马六甲](../Page/葡萄牙.md "wikilink")，然後被[荷兰人驱逐](../Page/荷兰人.md "wikilink")。虽然[英国人企图介入](../Page/英国人.md "wikilink")[东印度群岛纷争](../Page/东印度群岛.md "wikilink")，但终于落入荷兰人控制，葡萄牙人最後只剩下[帝汶岛](../Page/帝汶岛.md "wikilink")。[太平洋戰爭初期](../Page/太平洋戰爭.md "wikilink")，[日本人擊敗欧洲殖民者](../Page/日本人.md "wikilink")，控制馬來群島大部分地區，这个时期激发了[民族主义的成长](../Page/民族主义.md "wikilink")，战後英國及荷蘭曾經嘗試繼續維持殖民統治，但該區的民族都爭取獨立，誕生新的国家──[印度尼西亚](../Page/印度尼西亚.md "wikilink")（1949）、[马来西亚](../Page/马来西亚.md "wikilink")（1963）、[巴布亚纽几内亚](../Page/巴布亚纽几内亚.md "wikilink")（1975）和[汶莱](../Page/汶莱.md "wikilink")（1984）。

## 註釋

## 參考文獻

## 外部連結

  - [Wallace, Alfred
    Russel](../Page/Alfred_Russel_Wallace.md "wikilink"). *The Malay
    Archipelago*, [Volume
    I](http://www.authorama.com/book/malay-archipelago-1.html),
    \[<http://www.authorama.com/book/malay-archipelago-2.html>, Volume
    II\].

## 參見

  - [馬來半島](../Page/馬來半島.md "wikilink")
  - [馬來西亞](../Page/馬來西亞.md "wikilink")
  - [華萊士線](../Page/華萊士線.md "wikilink")

{{-}}

[Category:亚洲群岛](../Category/亚洲群岛.md "wikilink")
[马来群岛](../Category/马来群岛.md "wikilink")
[Category:东南亚](../Category/东南亚.md "wikilink")
[Category:亚洲地理的世界之最](../Category/亚洲地理的世界之最.md "wikilink")

1.  過去曾有「東印度」、「印度群島」、「印度尼亞西群島」等名稱。

2.  "[Malay Archipelago](http://www.britannica.com/ebc/article-9371061)
    ." *[Encyclopedia
    Britannica](../Page/Encyclopedia_Britannica.md "wikilink")*. 2006.
    Chicago: Encyclopedia Britannica, Inc.

3.
4.  "[Maritime Southeast
    Asia](http://www.worldworx.tv/regional-information/asia/maritime-southeast-asia/index.htm)
    ." *Worldworx Travel*. Accessed 26 May 2009.

5.

6.  Reid, Anthony. [Understanding melayu (Malay) as a source of diverse
    modern identities. Origins of
    Malayness](http://www.accessmylibrary.com/coms2/summary_0286-2067345_ITM)
    2001 Cambridge University Press. Retrieved on March 2, 2009.

7.  ;

8.
9.  [Papua Web: The Malay Archipelago (Chapter XL - Races of Man in the
    Malay
    Archipelago)](http://www.papuaweb.org/dlib/bk/wallace/race.html).
    Quote:
    "If we draw a line ... commencing to the east of the Philippine
    Islands, thence along the western coast of Gilolo, through the
    island of Bouru, and curving round the west end of Mores, then
    bending back by Sandalwood Island to take in Rotti, we shall divide
    the Archipelago into two portions, the races of which have strongly
    marked distinctive peculiarities. This line will separate the
    Malayan and all the Asiatic races, from the Papuans and all that
    inhabit the Pacific; and though along the line of junction
    intermigration and commixture have taken place, yet the division is
    on the whole almost as well defined and strongly contrasted, as is
    the corresponding zoological division of Archipelago, into an
    Indo-Malayan and Austro-Malayan region."

10. [OED first edition](../Page/Oxford_English_Dictionary.md "wikilink")
    *A geographical term, including Hindostan, Further India, and the
    islands beyond* with first found usage 1598

11.
12. ;

13. Friedhelm Göltenboth (2006) *Ecology of insular Southeast Asia: the
    Indonesian Archipelago* Elsevier, ISBN 978-0-444-52739-4, ISBN
    978-0-444-52739-4

14. [Modern Quaternary Research in Southeast Asia,
    Volume 1](http://books.google.com/books?id=ikSQw_-8gboC&pg=PA227&dq=maritime+southeast+asia&ei=q9olS730OZOkkATG58nOCw&cd=1#v=onepage&q=malay%20archipelago&f=false)

15.
16. [Philippines : General
    Information](https://web.archive.org/web/20071022221129/http://www.gov.ph/aboutphil/general.asp).
    Government of the Philippines. Retrieved 2009-11-06; ;

17. ;

18. 「南島語族」（[南島語系](../Page/南島語系.md "wikilink")）一詞原為[語言學術語](../Page/語言學.md "wikilink")，[人類學用以指稱該使用同一語系的民族](../Page/人類學.md "wikilink")。