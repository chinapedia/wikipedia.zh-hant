**Lucida**是一个西文[字体家族名](../Page/字体.md "wikilink")，由查尔斯·毕格罗（Charles
Bigelow）和克里斯·霍尔姆斯（Kris Holmes）于1985年设计。

这个家族有很多变体，包括黑体（Blackletter），艺术体（Calligraphy），手写体（Handwriting），[衬线](../Page/衬线.md "wikilink")（Fax,
Bright）和[无衬线体](../Page/无衬线体.md "wikilink")（Sans, [Lucida Sans
Unicode](../Page/Lucida_Sans_Unicode.md "wikilink")，[Lucida
Grande](../Page/Lucida_Grande.md "wikilink")，Sans Typewriter等等）。

Bigelow\&Holmes公司与[TeX的厂商Y](../Page/TeX.md "wikilink")\&Y合作制作了一套完整的TeX用数学符号，因此这个字体也是少数TeX数学公式排版能使用的字体之一。

Lucida Console也是英文版[Windows
XP和](../Page/Windows_XP.md "wikilink")[Windows
CE中](../Page/Windows_CE.md "wikilink")[蓝屏死机和](../Page/蓝屏死机.md "wikilink")[Windows記事本的默认字体](../Page/Windows記事本.md "wikilink")。Lucida
Sans Demibold（轮廓和Lucida Grande
Bold体完全一样，但是数字间距较紧）被用于[苹果公司的](../Page/苹果公司.md "wikilink")[Mac
OS
X](../Page/Mac_OS_X.md "wikilink")[操作系统](../Page/操作系统.md "wikilink")，以及其他众多[程序](../Page/程序.md "wikilink")，包括[Front
Row](../Page/Front_Row.md "wikilink")。

## 字体样本

<File:Lucida> Sans sample.svg|Lucida Sans <File:Lucida> Sans Typewriter
sample.svg|Lucida Sans Typewriter <File:Lucida> Console
sample.svg|Lucida Console <File:Lucida> Handwriting sample.svg|Lucida
Handwriting <File:Lucida> Grande sample.svg|[Lucida
Grande](../Page/Lucida_Grande.md "wikilink") <File:Lucida> Calligraphy
sample.svg|Lucida Calligraphy <File:Lucida> Blackletter
sample.svg|Lucida Blackletter <File:Lucida> Fax sample.svg|Lucida Fax
<File:Lucida> Bright sample.svg|Lucida Bright <File:Lucida> Serif
sample.svg|Lucida Serif

## 外部链接

  - [clagnut The new typography](http://www.clagnut.com/blog/266/)（blog
    entry with some information on variation of Lucida typeface names
    across systems）
  - [*Lucida* fonts](http://www.ascendercorp.com/lucida.html)（Ascender
    corporation）
  - [*Lucida* and TeX](http://tug.org/store/lucida/)（TeX Users Group）
  - [*Lucida*
    fonts](http://www.linotype.com/112289/lucidafontfamilygroup-clan.html)（Linotype
    corporation）
  - [Notes on
    Lucida](http://www.tug.org/store/lucida/designnotes.html)，by
    [Charles
    Bigelow](../Page/Charles_Bigelow_\(type_designer\).md "wikilink")

[Category:字体](../Category/字体.md "wikilink")