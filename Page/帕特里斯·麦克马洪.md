**玛利·埃德姆·帕特里斯·莫里斯·德·麦克马洪，馬真塔公爵**（**Marie Edme Patrice Maurice de
Mac-Mahon, Duc de
Magenta**，），[法國軍人](../Page/法國.md "wikilink")，[法兰西第三共和国第二任總統](../Page/法兰西第三共和国.md "wikilink")（1873年—1879年）。在[克里米亞戰爭及](../Page/克里米亞戰爭.md "wikilink")[義大利](../Page/義大利.md "wikilink")[馬真塔戰役](../Page/馬真塔戰役.md "wikilink")（1859）中揚名，被升為[法國元帥](../Page/法國元帥.md "wikilink")，并受封為[馬真塔公爵](../Page/馬真塔.md "wikilink")（duc
de Magenta）。

他是[詹姆斯黨的](../Page/詹姆斯黨.md "wikilink")[愛爾蘭家族後裔](../Page/愛爾蘭.md "wikilink")，於1827年展開軍旅生涯，1864～1870年間擔任[阿爾及利亞總督](../Page/阿爾及利亞.md "wikilink")，後來又在[普法戰爭中擔任指揮官](../Page/普法戰爭.md "wikilink")。他也是[凡爾賽軍司令](../Page/凡爾賽.md "wikilink")，這支軍隊於1871年鎮壓[巴黎公社起義](../Page/巴黎公社.md "wikilink")。

1873年，[阿道夫·梯也尔辭去總統職務之後](../Page/阿道夫·梯也尔.md "wikilink")，他被選為總統。作為保皇派，他在總統任內頒布了1875年憲法。之後發生了憲法危機，麥克馬洪與共和派不合，被迫於1879年下台辭職，由共和派控制的議會接管政府。

## 另見

  - [亨利·麥克馬洪](../Page/亨利·麥克馬洪.md "wikilink")

[M](../Category/法國總統.md "wikilink") [M](../Category/法國公爵.md "wikilink")
[Category:愛爾蘭裔法國人](../Category/愛爾蘭裔法國人.md "wikilink")
[Category:圣西尔军校生](../Category/圣西尔军校生.md "wikilink")
[Category:巴黎公社](../Category/巴黎公社.md "wikilink")