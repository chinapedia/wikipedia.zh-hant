[NHK_logo.svg](https://zh.wikipedia.org/wiki/File:NHK_logo.svg "fig:NHK_logo.svg")使用NHK標誌的白色半透明版本，与[NHK
World-Japan有别](../Page/NHK_World-Japan_\(頻道\).md "wikilink")\[1\]\]\]

**NHK World
Premium**（）是[日本廣播協會](../Page/日本廣播協會.md "wikilink")（NHK）使用[日语进行](../Page/日语.md "wikilink")[国际广播的](../Page/国际广播.md "wikilink")[收费电视](../Page/收费电视.md "wikilink")[綜合頻道](../Page/綜合頻道.md "wikilink")。

## 概要

  - [NHK
    World-Japan的同系頻道](../Page/NHK_World-Japan_\(頻道\).md "wikilink")，以播放NHK精選（）新聞、資訊、綜合節目、劇集、音樂、體育及兒童節目為主。除部分时段外，其余时段全部锁码播出。每日卫星电视开锁时间如下（[日本时间](../Page/日本时间.md "wikilink")，仅限国际19号、国际20号、国际21号卫星）\[2\]：

<!-- end list -->

  -

      -
        周一至周四：6:30-8:00、12:00-12:43、19:00-19:30、21:00-22:25、23:40-0:30
        周五：6:30-8:00、12:00-12:20、19:00-20:00、21:00-22:00、23:40-0:30
        周六：6:30-8:00、9:00-9:30、12:00-12:15、19:00-19:30、20:45-21:00
        周日：6:00-11:00、12:00-13:05、19:00-19:30、20:45-21:00

<!-- end list -->

  - NHK在1995年4月於[北美洲](../Page/北美洲.md "wikilink")、[歐洲進行](../Page/歐洲.md "wikilink")「國際電視廣播」，衛星訊號也遍及[亞洲](../Page/亞洲.md "wikilink")\[3\]。同年11月24日，[三商企業旗下](../Page/三商企業.md "wikilink")[三商多媒體取得](../Page/三商多媒體.md "wikilink")[台灣總代理權](../Page/台灣.md "wikilink")，NHK海外企畫室室長[入澤邦雄等八位高級主管出席授權儀式](../Page/入澤邦雄.md "wikilink")，三商多媒體並於1996年2月1日起向台灣各家[有線電視系統業者收取授權費](../Page/有線電視.md "wikilink")\[4\]。亞洲播出的版本，當時台灣稱「NHK亞洲台」\[5\]，在台灣各家有線電視系統業者的頻道表中取代僅向日本播出的[NHK衛星第1頻道與](../Page/NHK衛星第1頻道.md "wikilink")[NHK衛星第2頻道](../Page/NHK衛星第2頻道.md "wikilink")。
  - 1998年4月1日與NHK World
    TV開始向[亞太地區播出](../Page/亞太地區.md "wikilink")，三商多媒體仍是台灣總代理，中文名稱定為「**NHK世界台**」，[國家通訊傳播委員會](../Page/國家通訊傳播委員會.md "wikilink")（NCC）备案之频道名称仍沿用英文名。
  - 在[中国大陆地区稱作](../Page/中国大陆.md "wikilink")「**NHK世界收费电视频道**」，可通过[亚太六号卫星上的中国境外卫星电视统一平台接收](../Page/亚太六号.md "wikilink")，该服务一般只提供给三星级以上的[酒店和政府指定的居住区](../Page/酒店.md "wikilink")。\[6\]

## 註釋

## 相關條目

  - [NHK環球廣播網](../Page/NHK環球廣播網.md "wikilink")
  - [TV Japan](../Page/TV_Japan.md "wikilink")
  - [JSTV](../Page/日本语卫星放送.md "wikilink")

## 參考資料

<div class="references-small">

  - [NW世界台簡介](http://nhk.mercuries.com.tw/NW.htm)
  - [广电总局关于2007年度三星级以上涉外宾馆等单位可申请接收的境外卫星电视频道范围的通知](https://web.archive.org/web/20140503152732/http://www.sarft.gov.cn/articles/2007/07/26/20070914162110700760.html)
  - [《日本NHK卫星电视数码频道》](https://web.archive.org/web/20160304135553/http://www.docin.com/p-226131591.html)

</div>

## 外部連結

  - [NHK World Premium](http://nhkworldpremium.com/)
  - [NHK World Premium Australia](http://www.japantv.com.au/)
  - [三商多媒體股份有限公司（NHK世界台）](http://nhk.mercuries.com.tw/)
  - [中视卫星电视节目有限责任公司（NHK世界收费频道）](http://www.stvp.com.cn/content/details21_455.html)

[Category:香港電視播放頻道](../Category/香港電視播放頻道.md "wikilink")
[Category:台灣電視播放頻道](../Category/台灣電視播放頻道.md "wikilink")
[Category:卫星电视频道](../Category/卫星电视频道.md "wikilink")
[Category:NHK电视频道](../Category/NHK电视频道.md "wikilink")

1.  [FAQ: Reception through Local Operators -
    Q2](http://nhkworldpremium.com/faq/index3_e.aspx?ssl=false#02). NHK
    World Premium. \[2016-09-19\] .
2.  [NHKワールドプレミアム](https://www3.nhk.or.jp/nhkworld/ja/premium/)，NHK
3.
4.  [我國衛星電視產業92-94年度營運調查期末報告](http://neverland.km.nccu.edu.tw/xms/read_attach.php?id=1953).
    [國立政治大學新聞學系](../Page/國立政治大學新聞學系.md "wikilink").
    [國家通訊傳播委員會九十四年度委託研究計畫](../Page/國家通訊傳播委員會.md "wikilink").
    2006-10. 第127頁. \[2016-02-06\] .
5.  . 廣播與電視. 1996-01. \[2012-08-27\] . 海外稱「NHK
    International」，向亞洲發送訊號未見於NHK官方記載。另見[討論](../Page/Talk:NHK_World_Premium#NHK亞洲台.md "wikilink")。
6.  [频道专区 -
    频道介绍：NHK世界收费电视频道](http://www.stvp.com.cn/content/details21_455.html)
    .[中视卫星电视节目有限责任公司](../Page/中视卫星电视节目有限责任公司.md "wikilink").
    \[2016-09-19\] .