[CH_Landwasser_2.jpg](https://zh.wikipedia.org/wiki/File:CH_Landwasser_2.jpg "fig:CH_Landwasser_2.jpg")（Landwasserviadukt）\]\]

**冰川特快**（德語：**Glacier-Express**）是一條從[策馬特行駛到](../Page/策馬特.md "wikilink")[聖模里茲](../Page/聖模里茲.md "wikilink")（夏季可達[達沃斯](../Page/達沃斯.md "wikilink")）之間的[鐵路線](../Page/鐵路.md "wikilink")，是著名的觀光路線，由[勞蒂亞鐵路](../Page/勞蒂亞鐵路.md "wikilink")（Rhätische
Bahn）及[馬特洪哥達鐵路](../Page/馬特洪哥達鐵路.md "wikilink")（Matterhorn-Gotthard-Bahn，本身原是兩家公司）共同經營。

冰川特快的行駛速度並不快，但是中途即使行經不同鐵路路線，乘客也不必換車。在路程中，冰川特快一共會經過291條[橋樑](../Page/橋樑.md "wikilink")、91個[隧道](../Page/隧道.md "wikilink")，並通過海拔2033公尺的[歐伯拉普隘口](../Page/歐伯拉普隘口.md "wikilink")
（Oberalp
Pass），總時程約7個半鐘頭。冰川特快全線採用[窄軌鐵道](../Page/窄軌.md "wikilink")，並有部分路段利用[齒軌以利在陡峭的坡度上行進](../Page/齒軌鐵路.md "wikilink")。

持有[歐鐵](../Page/歐鐵.md "wikilink")（Eurail）乘車證的旅客，若無其他車票，則只能從聖模里茲搭往[迪森提斯](../Page/迪森提斯.md "wikilink")（Disentis/Mustér）。若要以迪森提斯作為起點，則必須預先另購車票，這種車票可分兩級，從迪森提斯到采爾馬特的一級票在2006年的票價為130[瑞士法郎](../Page/瑞士法郎.md "wikilink")；二級票則為75瑞士法郎。

## 外部連結

  - [Glacier Express official website](http://www.glacierexpress.ch/)
  - [Glacier Express](http://www.ddavid.com/alongdesire/swiss1.htm) A
    personal trip on the Glacier Express with photographs

[Category:瑞士命名列车](../Category/瑞士命名列车.md "wikilink")