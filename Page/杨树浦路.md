**杨树浦路**是[上海市](../Page/上海市.md "wikilink")[虹口区与](../Page/虹口区.md "wikilink")[杨浦区南部](../Page/杨浦区.md "wikilink")、滨临[黄浦江的一条东西向干道](../Page/黄浦江.md "wikilink")，西起惠民路，东至黎平路，全长5586米。杨树浦路西段設有[上海軌道交通四号线](../Page/上海軌道交通四号线.md "wikilink")[杨树浦路站](../Page/杨树浦路站.md "wikilink")。\[1\]

## 历史

1871年，[上海公共租界工部局自下海浦](../Page/上海公共租界工部局.md "wikilink")[提篮桥向东辟筑杨树浦路](../Page/提篮桥.md "wikilink")，直至杨树浦港。1884年，又越过杨树浦港，继续向东延筑至[黎平路](../Page/黎平路.md "wikilink")。

1895年[中日马关条约签订后](../Page/中日马关条约.md "wikilink")，外商大举在华开设工厂，濒临黄浦江的杨树浦路南侧一带，迅速建成了毛条、毛纺、丝织、造船、发电、煤气、自来水、化工、制皂等40多家大中型工厂，形成中国规模最大最具规模的工业区之一。其中[杨树浦水厂为英国古典城堡式造型](../Page/杨树浦水厂.md "wikilink")，1883年建成供水，是中国最早的现代化地面水厂。

杨树浦路的北侧，则形成大片工人住宅区，以旧式里弄和大量棚户区为主，包括[鼎和里](../Page/鼎和里.md "wikilink")、[新康里](../Page/新康里.md "wikilink")、[华忻坊等](../Page/华忻坊.md "wikilink")26条里弄。杨树浦路目前大部分路段店铺稀少，只在[宁国南路至](../Page/宁国南路.md "wikilink")[临青路之间](../Page/临青路.md "wikilink")，集中了数十家中小型商店。

## 公交线路

杨树浦路沿线公交有8、28、60、70、79、80、124、134、135、137、577、813、853等。

## 参考文献

<div class="references-small">

<references />

</div>

[Y](../Category/杨浦区.md "wikilink")
[Category:上海公共租界时期辟筑道路](../Category/上海公共租界时期辟筑道路.md "wikilink")
[Y](../Category/上海道路.md "wikilink")

1.  [专业志 \>\> 上海市政工程志 \>\> 第一篇市区道路 \>\> 第二章主要干道 \>\>
    第一节　东西向主要干道](http://shtong.gov.cn/node2/node2245/node68289/node68294/node68312/node68325/userobject1ai65725.html)上海地方志办公室