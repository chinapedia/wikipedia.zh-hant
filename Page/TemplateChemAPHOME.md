<table cellpadding="2" cellspacing="0" style="margin:5px; border:3px solid;">

<tr>

<td style="border-bottom:3px solid; background:#efefef;">

<center>

**[新‧化學提升計畫佈告欄](Wikipedia:條目質量提升計劃/科技提升計畫#化學系列.md "wikilink")**

</center>

</td>

</tr>

<tr>

<td colspan="0">

![Science-symbol-2.png](Science-symbol-2.png "Science-symbol-2.png")
<small>此列表作為提升參與和引導，並所有參與者共同維護與添增，若有計畫提升哪些條目，請在該條目旁邊標註**\[\[:User:XXX|↑\]\]**提醒眾人，若認為完成請從模板中移除，並至個人貢獻處提報。</small>

<center>

<big>本期关注的条目是</big>

</center>

  - 待擴充條目：
    [化學小作品與](:../Category/化學小作品.md "wikilink")[元素小作品](:../Category/元素小作品.md "wikilink")：
    [生物化學小作品](:../Category/生物化學小作品.md "wikilink")：[寡醣](寡醣.md "wikilink")
    [有機化合物與](:../Category/有機化合物小作品.md "wikilink")[無機化合物小作品](:../Category/無機化合物小作品.md "wikilink")：
    [有機化學](有機化學.md "wikilink")：[醯化反應](醯化反應.md "wikilink")、[傅-克醯基化反應](傅-克醯基化反應.md "wikilink")

<!-- end list -->

  - 待創建條目：

{{.w}}{{.w}}{{.w}}

  - 內容欠缺（![[File:QSicon_in_Arbeit.svg](File:QSicon_in_Arbeit.svg.md)](QSicon_in_Arbeit.svg
    "File:QSicon_in_Arbeit.svg")3 - 10 KB）：

{{.w}}{{.w}}{{.w}}{{.w}}{{.w}}{{.w}}{{.w}}{{.w}}{{.w}}{{.w}}{{.w}}{{.w}}

  - 內容極為欠缺（![[File:Qsicon_Ueberarbeiten.svg](File:Qsicon_Ueberarbeiten.svg.md)](Qsicon_Ueberarbeiten.svg
    "File:Qsicon_Ueberarbeiten.svg")3 KB以下）：

{{.w}}{{.w}}{{.w}}{{.w}}{{.w}}{{.w}}{{.w}}

  - [新化學提升計畫提案](Wikipedia:條目質量提升計劃/科技提升計畫#化學系列.md "wikilink")：腈、環氧化合物、斯文氧化反應、傅－克反應、化學專題創建

<!-- end list -->

  - 完善缺少信息框的条目：参见[条目列表](https://petscan.wmflabs.org/?psid=3297536.md)。

<div style="text-align: right;">

<small></small>

</div>

</td>

</tr>

</table>

<noinclude>

## 過去存檔

  - 扩充（或重写）所有[化学元素条目完成](:分类:化学元素.md "wikilink")：

` `` `` `` `

### 化學

以下是[User:Merphisto所建議的的](User:Merphisto.md "wikilink")[基礎條目列表](Wikipedia:基礎條目/科學、數學與技術.md "wikilink")，擴充元維基版本不少欠缺卻有極高重要度之條目。

</noinclude>

[](../Category/化学模板.md "wikilink")