**紙模型**，有時也可以稱作**卡片模型**，是一種由[紙](../Page/紙.md "wikilink")（通常是厚紙或卡片）製成的模型。這是一種消閑物品，有時也是小孩的手工之一。它在[日本及](../Page/日本.md "wikilink")[歐洲](../Page/歐洲.md "wikilink")（尤以[東歐地區](../Page/東歐.md "wikilink")）較為流行。紙模型根據其圖紙的來源，主要可分為兩類：其一是市面上有售的印刷品及書籍；其二則是可以從網上下載免費的圖紙，再自行列印及剪貼
打印纸膜、一般打印在卡纸 相片纸 、根据不同的模型也有分类

## 商業紙模型

[商業紙模型在](../Page/商業.md "wikilink")[二十世紀早期的雜誌中已經出現](../Page/二十世紀.md "wikilink")，而在[二次大戰中最為盛行](../Page/二次大戰.md "wikilink")，因為當時紙的供應相比起其它物品較為充裕。而在[英國](../Page/英國.md "wikilink")[設計及出版的Micromodels](../Page/設計.md "wikilink")，在1941年開始曾盛行一時；其題材也是多元化的，有[建築物](../Page/建築物.md "wikilink")、船艦、[飛機](../Page/飛機.md "wikilink")、[坦克等](../Page/坦克.md "wikilink")，而[波蘭的GPM](../Page/波蘭.md "wikilink")
在出版紙模型上也是十分有名的。但隨著[塑膠模型的普及](../Page/塑膠模型.md "wikilink")，商業紙模型則較為少見了。

不過，由於紙模型的相對廉宜及拼砌的容易（紙模型不用自行上色，但大多塑膠模型則需要；而且前者在粘貼上也就更為容易，只用普通膠水便可），所以在二十一世紀的今天，紙模型仍有其市場價值，而且在網路上仍能購買到不少的紙模型，大多都是以建築物及戰艦為主。在[台灣](../Page/台灣.md "wikilink")，更有人設計了不少的紙模型明信片，如\[[http://monkey-design.com.tw/\]作為紀念品出售，也有專門製作](http://monkey-design.com.tw/%5D作為紀念品出售，也有專門製作)[F1賽車的業餘玩家](../Page/F1.md "wikilink")，如[F1紙模型](http://blog.yimg.com/2/rGQGbRx7s5.3gV0nluB35RtBVfLS_kJZCuqtv49X.w3FgsLzrbrBHw--/66/o/4GN2Hi647Hb8aGGDHJtT_A.jpg)；此外，在台灣，有出版社更推出含有著名建築（如[台灣總統府](../Page/台灣總統府.md "wikilink")）等圖紙的書籍。

## 免費紙模型

免費紙模型則是隨著網路普及而興起的，而且因為它們大多是免費下載及使用[噴墨打印機的成本低廉](../Page/噴墨打印機.md "wikilink")，更可以隨著自己的喜好而調整大小，自由度極高；故愈來愈多人喜歡在網上下載紙模型來拼砌。自2000年，（一說是1999年，[山葉發動機株式會社機車模型開始](../Page/山葉發動機株式會社.md "wikilink")）左右開始，
世界各地不少紙模型的喜好者把自己設計的圖紙公佈於網路上，供其他愛好者下載。而模型的種類，因為紙的可塑性，模型的造型因而可以變得極為多元化，再加上現時日本[動漫的流行](../Page/動漫.md "wikilink")，故紙模型除了有傳統的建築物、船艦、汽車之外，還有不少與動漫相關的角色，如[GUNDAM](../Page/GUNDAM.md "wikilink")，
[keroro軍曹等的紙模型圖樣也相繼推出](../Page/keroro軍曹.md "wikilink")。而不少的商業機構有見及此，便把它們的產品設計成紙模型，例如[山葉發動機株式會社](../Page/山葉發動機株式會社.md "wikilink")；或是自行設計紙模型（大多以著名的事物為藍本），
這在不少[打印機品牌的網頁上可以見到](../Page/打印機.md "wikilink")，[Canon的Creative](../Page/Canon.md "wikilink")
Park正是一個例子；更有不少官方網站利用紙模型作為其推廣手段之一，如日本電影[沒有出路的海](../Page/沒有出路的海.md "wikilink")（），則以多期連載潛艇模型來引起大眾注意。\[1\]

隨了從網上下載圖紙外，有經驗的紙模玩家通常會在紙上構作，或使用繪圖軟件（如 Adobe Illustrator
等）創作圖紙，而且現在有軟件（如[Pepakura
Designer](../Page/Pepakura_Designer.md "wikilink")）可以引入原有的立體圖像轉變為紙模圖紙，所以有更多的人把它們的立體創作上載上網以供大家分享，而紙模的類型則更為多元化了。

由於紙模型的風氣盛行，有一些原本把自己設計的圖紙上傳的人也出版了在市場上販賣的版本，故這間接地把逐漸衰退的商業紙模行業重回軌道，這現象在台灣及日本頗為常見。\[2\]\[3\]

[Albatros_B.I_paper_model.JPG](https://zh.wikipedia.org/wiki/File:Albatros_B.I_paper_model.JPG "fig:Albatros_B.I_paper_model.JPG")紙模型\]\]
[SVB73wiki.jpg](https://zh.wikipedia.org/wiki/File:SVB73wiki.jpg "fig:SVB73wiki.jpg")紙模型\]\]
[Himeji_Castle_paper_model_20061121.jpg](https://zh.wikipedia.org/wiki/File:Himeji_Castle_paper_model_20061121.jpg "fig:Himeji_Castle_paper_model_20061121.jpg")紙模型\]\]

## 紙模型的拼砌方法

有些商業紙模型是預切的刀模，即是不需要剪裁便可以把部件拆出來的；可是仍有不少的是要用剪刀把部件一件件的剪出來，才能進行拼砌。有經驗的玩家建議在用刀具剪裁前用一些鈍的器具壓出折邊，可以令效果更好；而在粘貼時則應該用掃子把[漿糊塗的平均更佳](../Page/漿糊.md "wikilink")；在製造圓柱時，使用筆等物件輔助則較為理想\[4\]；而紙是容易彎曲的材料，所以在必要時也應該加上較硬的物件，如木條等以加固模型。紙模型大多是預先塗色，所以在拼砌後即可以完成；但不少人也會為自己的紙模加工及上色。

## 紙模型的普及

在過去，紙模型只是小眾的興趣，並沒受到太大的重視；但到了現在，由於網絡的發展，紙模變得更為盛行了；在世界上不同的地區，如德國及台灣，有不少紙模愛好者成立了關於紙模型創作的網路社群及網頁，藉此與其他人分享製作紙模型的心得；而且更定期在不同地方舉行展覽，使紙模型變得更為普及，並成為不少雜誌的專題，如香港的Metropop等。

## 紙巴士

顧名思義，是用紙製的巴士模型。現時紙製巴士模型大部份比例都是1/76的。

在香港，紙巴士暫時只有[80M巴士專門店才有得賣](../Page/80M巴士專門店.md "wikilink")，但是價錢昂貴(1張HK$10)，且選擇也較少。不過坊間有一些紙製巴士模型可以免費列印，且選擇也較多。

## 參見

  - [比例模型](../Page/比例模型.md "wikilink")
  - [鐵道模型](../Page/鐵道模型.md "wikilink")

## 參考資料

## 外部連結

  - [Yee's Job](http://www.yeesjob.com/)
  - [uhu02 ペーパークラフト](http://uhu02.way-nifty.com/die_eule_der_minerva/)
  - [www.cardmodel.cn](http://%5Bhttp://www.cardmodel.cn)／中文最专业的纸模型站点 \]
  - [1:24
    娃娃屋紙模型](https://web.archive.org/web/20150519025317/http://www.ifunwoo.com/p/feature.html)

[Category:手工艺](../Category/手工艺.md "wikilink")
[Category:紙張藝術](../Category/紙張藝術.md "wikilink")

1.  見[電影《》的官方網站](http://deguchi-movie.jp)
2.  見http://www.d1.dion.ne.jp/\~hamanaga/ThumbLink/Thumb55.html
    一免費紙模的連結網站；部分是被刪去的
3.  [紙工房](http://www.zuko.to/kobo/works-f/c4-f/c4-set.html)
    看日文，同樣的紙模將會在市場上發售
4.  [Skill 組合技巧](http://3dpapermodel.com.tw/how/skill.html)