**辅公祏**（），[中国](../Page/中国.md "wikilink")[隋朝末年民變军领袖之一](../Page/隋朝.md "wikilink")，齐郡临济（今[山东](../Page/山东.md "wikilink")[章丘](../Page/章丘市.md "wikilink")）人。

## 生平

辅公袥與[杜伏威交好](../Page/杜伏威.md "wikilink")，姑家以牧羊為業，常从姑家偷羊给杜伏威吃。613年随从杜伏威率众据[长白山](../Page/长白山.md "wikilink")（今章丘、[邹平交界处](../Page/邹平.md "wikilink")）起兵，杜伏威自称将军。后转战[淮河以南](../Page/淮河.md "wikilink")，隋將[宋颢前来镇压](../Page/宋颢.md "wikilink")，杜伏威将隋军引入芦苇荡中，放火燒死其步兵、骑兵。先后占领[高邮和历阳](../Page/高邮.md "wikilink")（今[安徽](../Page/安徽.md "wikilink")[和县](../Page/和县.md "wikilink")）。杜伏威自称总管，以辅公祏为[长史](../Page/长史.md "wikilink")。

[唐高祖](../Page/唐高祖.md "wikilink")[武德二年](../Page/武德.md "wikilink")（619年）隨杜伏威投降[唐朝](../Page/唐朝.md "wikilink")，獲任命为[淮南道](../Page/淮南道.md "wikilink")[行台](../Page/行台.md "wikilink")[仆射](../Page/仆射.md "wikilink")，受封舒国公。武德三年（620年），奉命率军攻打[李子通](../Page/李子通.md "wikilink")，夺取丹阳（今[江苏](../Page/江苏.md "wikilink")[南京](../Page/南京.md "wikilink")），乃以此为基地发展实力。武德五年（622年），杜伏威入[长安](../Page/长安.md "wikilink")，以辅公祏留守丹阳，将兵权交给右将军[王雄诞](../Page/王雄诞.md "wikilink")，此舉讓辅公祏心生不满。

武德六年（623年）八月，辅公祏杀死王雄诞，在丹阳起兵反唐，自称宋帝，年号[天明](../Page/天明_\(輔公祏\).md "wikilink")，攻占海州（今江苏[连云港](../Page/连云港.md "wikilink")）等地。時北方既定，[八月廿一](../Page/八月廿一.md "wikilink")，唐高祖派[李孝恭率水军自江州](../Page/李孝恭.md "wikilink")（今[江西](../Page/江西.md "wikilink")[九江](../Page/九江.md "wikilink")）、[李靖自宣州](../Page/李靖.md "wikilink")（今[安徽](../Page/安徽.md "wikilink")[宣城](../Page/宣城.md "wikilink")），[黃君漢自譙](../Page/黃君漢.md "wikilink")（今安徽[亳州](../Page/亳州.md "wikilink")）以及[李世勣自](../Page/李世勣.md "wikilink")[泗水四路皆禀李孝恭节度进军征讨](../Page/泗水縣.md "wikilink")。

武德七年（624年）李孝恭指揮各軍在[当涂大破辅公祏军队](../Page/当涂.md "wikilink")，乘胜追击。[三月廿八](../Page/三月廿八.md "wikilink")，辅公祏弃丹阳而逃，在投奔[会稽](../Page/会稽.md "wikilink")[左游仙的途中于](../Page/左游仙.md "wikilink")[常州](../Page/常州.md "wikilink")[武康](../Page/武康.md "wikilink")（今[浙江武康](../Page/浙江.md "wikilink")）被俘，辅公祏偽稱是接受杜伏威命令才造反，不久被杀。武德七年二月，杜伏威亦暴薨于长安。

## 參考書目

  - 《[舊唐書](../Page/舊唐書.md "wikilink")》列傳第六

[Category:隋朝民變領袖](../Category/隋朝民變領袖.md "wikilink")
[Category:唐朝民变政权君主](../Category/唐朝民变政权君主.md "wikilink")
[Category:唐朝官员](../Category/唐朝官员.md "wikilink")
[Category:唐朝國公](../Category/唐朝國公.md "wikilink")
[Category:唐朝被處決者](../Category/唐朝被處決者.md "wikilink")
[Category:中國被毒死人物](../Category/中國被毒死人物.md "wikilink")
[Category:章丘人](../Category/章丘人.md "wikilink")
[G](../Category/辅姓.md "wikilink")
[Category:大唐雙龍傳登場人物](../Category/大唐雙龍傳登場人物.md "wikilink")