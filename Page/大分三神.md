**大分三神**（**大分トリニータ**,Oita
Trinita）是一支[日本足球隊](../Page/日本.md "wikilink")，屬於[日本職業足球聯賽的球隊之一](../Page/日本職業足球聯賽.md "wikilink")。目前在乙級作賽。球隊的根據地位於[大分縣](../Page/大分縣.md "wikilink")[大分市](../Page/大分市.md "wikilink")、[別府市](../Page/別府市.md "wikilink")、[佐伯市以及整個大分縣](../Page/佐伯市.md "wikilink")，主球場為[大分體育公園綜合競技場](../Page/大分體育公園綜合競技場.md "wikilink")。

## 簡介

球隊的名字是來自英語的trinity或意大利語的trinità，代表縣民、企業、行政三種架構。球隊設立於1994年，創立兩年，首先稱霸縣內聯賽，及後奪得[九州聯賽冠軍](../Page/九州聯賽.md "wikilink")。1996年，升上日本足球聯賽（JSL）。1999年日職聯乙級成立時，加盟日職聯。2002年奪得乙級冠軍，升往甲級作賽，2008年夺得日本联赛杯冠军并取得联赛第四名成绩，2009泛太平洋杯邀请赛中2—1胜山东鲁能夺得第三名，2009年因俱乐部经营资金拮据问题导致戰績不佳，降到乙級。

## 現役球員名單

## 著名前球员

  - [洛伦佐·史泰倫斯](../Page/洛伦佐·史泰倫斯.md "wikilink")

  - [马格诺·阿尔维斯](../Page/马格诺·阿尔维斯.md "wikilink")

  -
  -
  - [理查德·维茨格](../Page/理查德·维茨格.md "wikilink")

## 外部連結

  - [大分三神官方網站](http://www.oita-trinita.co.jp/)

[Category:日本足球俱樂部](../Category/日本足球俱樂部.md "wikilink")
[Category:大分市](../Category/大分市.md "wikilink")
[Category:別府市](../Category/別府市.md "wikilink")
[Category:佐伯市](../Category/佐伯市.md "wikilink")
[Category:1994年建立的足球俱樂部](../Category/1994年建立的足球俱樂部.md "wikilink")
[Category:1994年日本建立](../Category/1994年日本建立.md "wikilink")