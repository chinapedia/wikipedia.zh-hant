**-{余}-文樂**（，），[香港](../Page/香港.md "wikilink")[男歌手](../Page/男歌手.md "wikilink")、[男演員](../Page/男演員.md "wikilink")。因主演[香港電台青春劇](../Page/香港電台.md "wikilink")《[青春@Y2K](../Page/青春@Y2K.md "wikilink")》、《[Y2K+01](../Page/Y2K+01.md "wikilink")》而為人熟識。後與[環球唱片簽約](../Page/環球唱片公司.md "wikilink")，發展歌影事業。他早年與父母、一名胞兄與一名胞妹於[元朗的村屋和](../Page/元朗.md "wikilink")[錦繡花園生活](../Page/錦繡花園.md "wikilink")。在1993年畢業於港澳信義會黃陳淑英紀念學校，及後入讀[天主教培聖中學](../Page/天主教培聖中學.md "wikilink")，讀至1998年中五畢業後\[1\]，再於[基督教香港信義會心誠中學](../Page/基督教香港信義會心誠中學.md "wikilink")\[2\]重讀一年中五，並於1990年至2000年期間就讀[基督教香港信義會元朗信義中學](../Page/基督教香港信義會元朗信義中學.md "wikilink")\[3\]中六年級。但未完成預科課程便發展演藝事業。初年先在街上被星探發掘，才任職兼職模特兒。

## 演藝生涯

余文樂投入全職[模特兒行列並開始其演藝事業](../Page/模特兒.md "wikilink")。由於他富有時代感的外形，他很快受到廣告商的青睞。及後得到導演和唱片公司的賞識，正式開始投入電影、電視劇及歌唱的演藝事業。

出道不久便拍攝港台節目[青春@Y2K](../Page/青春@Y2K.md "wikilink")，合作藝人包括[蔡卓妍和](../Page/蔡卓妍.md "wikilink")[唐詩詠](../Page/唐詩詠.md "wikilink")。2002年正式加入樂壇，同年新人包括[麥浚龍](../Page/麥浚龍.md "wikilink")、[蕭正楠](../Page/蕭正楠.md "wikilink")，因三人是非不斷，被香港傳媒冠以「是非三人組」的稱號。最初“觸電”的他還是個青澀的新人。《[憂憂愁愁的走了](../Page/憂憂愁愁的走了.md "wikilink")》是一部Dogma風格的作品，導演在影片中做了破舊立新的嘗試。余文樂也藉這部充滿實驗意味的電影登上了熒幕。

2003年，因爭獎事件激烈，據稱沒有後台支持的余文樂因而失意香港四大音樂頒獎禮的新人獎。同時，有報道指因余與[Twins成員](../Page/Twins.md "wikilink")[鍾欣桐拍攝電影](../Page/鍾欣桐.md "wikilink")《[一碌蔗](../Page/一碌蔗.md "wikilink")》而傳出緋聞，因此與[鍾欣桐的另一緋聞男友](../Page/鍾欣桐.md "wikilink")[陳冠希交惡](../Page/陳冠希.md "wikilink")，繼而發生「撞膊事件」，令余文樂、陳冠希二人形象直插谷底。

他曾經主演過多部香港電影，包括《[江湖](../Page/江湖_\(電影\).md "wikilink")》以及《[無間道](../Page/無間道.md "wikilink")》，現已成為[東南亞著名的年青偶像派電影演員](../Page/東南亞.md "wikilink")。在《[無間道](../Page/無間道.md "wikilink")》系列中，他飾演年輕版[梁朝偉](../Page/梁朝偉.md "wikilink")，即劇中角色「陳永仁」，後又在電影《[江湖](../Page/江湖_\(電影\).md "wikilink")》中飾演年輕版的[劉德華](../Page/劉德華.md "wikilink")，然後於《[無間道II](../Page/無間道II.md "wikilink")》中擔正成為主角與其他影帝如[曾志偉](../Page/曾志偉.md "wikilink")、[黃秋生](../Page/黃秋生.md "wikilink")、[吳鎮宇合作](../Page/吳鎮宇.md "wikilink")，演技備受肯定。

2004年，[鄧小平百年壽辰](../Page/鄧小平.md "wikilink")，[上影集團投拍了獻禮巨片](../Page/上影集團.md "wikilink")《鄧小平·1928》。他在電影中扮演鄧小平的助手，革命青年[薛浦生](../Page/薛浦生.md "wikilink")。在電影《[頭文字D](../Page/頭文字D.md "wikilink")》中，他飾演「中里毅」，該片亦由[周杰倫](../Page/周杰倫.md "wikilink")、[陳冠希](../Page/陳冠希.md "wikilink")、[杜汶澤主演](../Page/杜汶澤.md "wikilink")。他的演出令人留下深刻印象，憑此電影打入[日本市場](../Page/日本.md "wikilink")，簽約[日本](../Page/日本.md "wikilink")[經理人公司](../Page/經理人.md "wikilink")。

2007年应邀担任日本天后級歌手[滨崎步的](../Page/滨崎步.md "wikilink")《Glitter》以及《Fated》两個PV的男主角。

2008年，余文樂憑電影《[第一誡](../Page/第一誡.md "wikilink")》榮獲韓國[富川國際電影節最佳男主角](../Page/富川國際電影節.md "wikilink")，與[鄭伊健共享影帝殊榮](../Page/鄭伊健.md "wikilink")。

2010年，余文樂出演《[志明與春嬌](../Page/志明與春嬌_\(電影\).md "wikilink")》與[楊千嬅飾演姊弟戀情侶](../Page/楊千嬅.md "wikilink")。由於口碑佳於2012年再出演《[春嬌與志明](../Page/春嬌與志明.md "wikilink")》。

2016年演出電影《[一念無明](../Page/一念無明_\(電影\).md "wikilink")》獲得[第53屆金馬獎評審讚揚](../Page/第53屆金馬獎.md "wikilink")，為男主角獎項之遺珠\[4\]。

2017年，余文樂出演《[春嬌救志明](../Page/春嬌救志明.md "wikilink")》，該電影成為「2017年香港華語票房冠軍電影」。

2017年11月，余文樂出演電影《[狂獸](../Page/狂獸.md "wikilink")》，飾演漁民走私集團頭子，接受訪問時笑言導演李子俊像變態，表示自己和張晉都覺得愈拍愈瘋，導演像跟他們有仇，每天特技化妝四小時，落妝一小時，在水深十呎對打，真的累死了。在水底打鬥有很多不穩定因素，日日不見天日，落到海要自己照顧自己，心情好像跌入黑洞，海水不停衝入鼻，心想：「拍完未？我就死了！」\[5\]。

2018年1月，余文樂接受[楊千嬅邀請擔任](../Page/楊千嬅.md "wikilink")《[楊千嬅三二一GO！演唱會](../Page/楊千嬅三二一GO！演唱會.md "wikilink")》的尾場嘉賓\[6\]。

## 感情生活

2016年12月，余文樂與[台灣皮帶大王千金](../Page/台灣.md "wikilink")[王棠云戀情公開](../Page/王棠云.md "wikilink")，更拍拖到深圳欣賞籃球賽。其間，余文樂介紹身旁的籃球明星易建聯給女友認識\[7\]。

2017年12月5日，余文樂突然於社交網站宣佈結婚，並貼上婚禮照片，配上愛妻宣言\[8\]。

2018年5月10日，妻子[王棠云在](../Page/王棠云.md "wikilink")[香港港安醫院剖腹產下](../Page/香港港安醫院.md "wikilink")7磅兒子余初见Cody
Yue\[9\]\[10\]。

而婚後亦不時於網上公開稱讚妻子，如稱讚她幫忙把行李箱的東西放得十分整齊等\[11\]。

## 軼事

2013年，余文樂在互聯網上載一張抱着幼童駕車的照片，照片其後於討論區廣泛流傳。余因發覺照片可能會誤導他人，所以於上載照片約40多分鐘後便將之刪除。而[警察公共關係科的發言人就表示未曾接獲有關的投訴](../Page/警察公共關係科.md "wikilink")。\[12\]

## 演藝以外事業

### 潮流品牌

  - 2010年，余文樂創立個人潮流品牌[COMMON
    SENSE](../Page/COMMON_SENSE.md "wikilink")(CMSS)
  - 2014年，創立[MADNESS服飾品牌](../Page/MADNESS.md "wikilink")，一個讓余文樂分享個人時尚與生活喜好的平台。余文樂期望“MADNESS
    BREEDS
    MADNESS”，以madness這種每個人都應該擁有的「神經」與「瘋狂」特質，孕育出像他一樣能夠創造藝術的同道中人。產品貫徹余文樂的簡約風格，配合細節與舒適的物料，以網購形式讓世界各地更多人都有機會分享，並於2016年10月於[北京市三里屯開設實體店鋪](../Page/北京市.md "wikilink")。\[13\]

### [電子競技](../Page/電子競技.md "wikilink")

2017年11月，余文樂於台灣成立瘋狂電競有限公司，並向[ahq電子競技俱樂部收購](../Page/ahq電子競技俱樂部.md "wikilink")[英雄聯盟](../Page/英雄聯盟.md "wikilink")2017年夏季GCS菁英挑戰聯賽冠軍ahq-fighter以及[傳說對決ahq](../Page/傳說對決.md "wikilink")
White兩支戰隊。並更名為[MAD Team](../Page/MAD_Team.md "wikilink")\[14\]

## 演出

### 電視劇

| 年度                                   | 播出頻道                                                                | 劇名                                              | 飾演                               |
| ------------------------------------ | ------------------------------------------------------------------- | ----------------------------------------------- | -------------------------------- |
| 2000                                 | [香港電台](../Page/香港電台.md "wikilink")                                  | [青春@Y2K](../Page/青春@Y2K.md "wikilink")          | 陳永樂                              |
| 2001                                 | [香港電台](../Page/香港電台.md "wikilink")                                  | [Y2K+01](../Page/Y2K+01.md "wikilink")          | 余文樂                              |
| 2002                                 | 台灣[中視](../Page/中視.md "wikilink")、[亞洲電視](../Page/亞洲電視.md "wikilink") | [愛情白皮書](../Page/愛情白皮書.md "wikilink")            | 歐陽掛居                             |
| 2006                                 | [亞洲電視](../Page/亞洲電視.md "wikilink")                                  | [浴火鳳凰](../Page/浴火鳳凰_\(2006年電視劇\).md "wikilink") | 向隨想                              |
| 2007                                 |                                                                     | [純白之戀](../Page/純白之戀.md "wikilink")              | 王志浩                              |
| | [特警出擊](../Page/特警出擊.md "wikilink") |                                                                     |                                                 |                                  |
| 2010                                 | 台灣[華視](../Page/華視.md "wikilink")                                    | [熊貓人](../Page/熊貓人_\(電視劇\).md "wikilink")        | 羅漢                               |
|                                      |                                                                     |                                                 |                                  |
| 2014                                 | [福建厦门影视频道](../Page/厦门电视台.md "wikilink")                             | [玲瓏局](../Page/玲瓏局.md "wikilink")                | 方翰文                              |
| [騰訊視頻](../Page/騰訊視頻.md "wikilink")   | [微時代](../Page/微時代.md "wikilink")                                    | 華三毛                                             |                                  |
| 2016                                 |                                                                     | [我的愛對你說](../Page/我的愛對你說.md "wikilink")          | 丁福乐                              |
| 2016-2017                            | [騰訊視頻](../Page/騰訊視頻.md "wikilink")、啊蒙寬頻                             | [特務](../Page/特務.md "wikilink")                  | 葉光仙，第二男主角                        |
| 2018                                 | 網絡劇                                                                 | [冒險王衛斯理](../Page/冒險王衛斯理.md "wikilink")          | [衛斯理](../Page/衛斯理.md "wikilink") |
| 待播                                   |                                                                     | [失憶之城](../Page/失憶之城.md "wikilink")              | 冰                                |
|                                      |                                                                     |                                                 |                                  |

### 電影

| 年度                                                                 | 片名                                           | 飾演           |
| ------------------------------------------------------------------ | -------------------------------------------- | ------------ |
| 2001年                                                              | [憂憂愁愁的走了](../Page/憂憂愁愁的走了.md "wikilink")     | \--          |
| 2002年                                                              | [一碌蔗](../Page/一碌蔗.md "wikilink")             | 阿凡           |
| [飛虎雄獅](../Page/飛虎雄獅.md "wikilink")                                 | 耶穌                                           |              |
| [無間道](../Page/無間道.md "wikilink")                                   | 青年陳永仁                                        |              |
| 2003年                                                              | [行運超人](../Page/行運超人.md "wikilink")           | 警察           |
| [下一站…天后](../Page/下一站…天后.md "wikilink")                             | 阿榮                                           |              |
| [百分百感覺2003](../Page/百分百感覺2003.md "wikilink")                       | Jerry                                        |              |
| [無間道II](../Page/無間道II.md "wikilink")                               | 青年陳永仁                                        |              |
| [無間道III終極無間](../Page/無間道III終極無間.md "wikilink")                     |                                              |              |
| [尋找周杰倫](../Page/尋找周杰倫.md "wikilink")                               | 宇仔                                           |              |
| 2004年                                                              | [我的大嚿父母](../Page/我的大嚿父母.md "wikilink")       | 康祈祖          |
| [江湖](../Page/江湖_\(電影\).md "wikilink")                              | 翼仔                                           |              |
| [精裝追女仔2004](../Page/精裝追女仔2004.md "wikilink")                       | 傻強                                           |              |
| [鄧小平·1928](../Page/邓小平·1928.md "wikilink")                         | 薛浦生                                          |              |
| 2005年                                                              | [黑白戰場](../Page/黑白戰場.md "wikilink")           | 發仔           |
| [頭文字D](../Page/頭文字D_\(電影\).md "wikilink")                          | 中里毅                                          |              |
| [猛龍](../Page/猛龍.md "wikilink")                                     | 阿樂                                           |              |
| 2006年                                                              | [春田花花同學會](../Page/春田花花同學會.md "wikilink")     | 警察通訊員        |
| [龍虎門](../Page/龍虎門.md "wikilink")                                   | 石黑龍                                          |              |
| [妄想](../Page/妄想_\(电影\).md "wikilink")                              | 劉成坤                                          |              |
| [臥虎](../Page/臥虎.md "wikilink")                                     | 刀手                                           |              |
| 2007年                                                              | [男兒本色](../Page/男兒本色.md "wikilink")           | 方弈威          |
| [危險人物](../Page/危險人物_\(香港電影\).md "wikilink")                        | 譚健鋒                                          |              |
| [男才女貌](../Page/男才女貌.md "wikilink")                                 | 楊樂                                           |              |
| [塚愛](../Page/塚愛.md "wikilink")                                     | 段恩明                                          |              |
| [破事兒](../Page/破事兒.md "wikilink")                                   | 初級殺手                                         |              |
| 2008年                                                              | [花花型警](../Page/花花型警.md "wikilink")           | 麥克民（Michael） |
| [軍雞](../Page/軍雞_\(電影\).md "wikilink")                              | 成島亮                                          |              |
| 青苔\[15\]                                                           | 仗角                                           |              |
| [第一诫](../Page/第一诫.md "wikilink")                                   | 李國強                                          |              |
| 2009年                                                              | [同門](../Page/同門.md "wikilink")               | 伍寶           |
| 2010年                                                              | [志明與春嬌](../Page/志明與春嬌_\(電影\).md "wikilink")  | 張志明          |
| [童眼](../Page/童眼.md "wikilink")                                     | 阿樂                                           |              |
| [精武風雲·陳真](../Page/精武風雲.md "wikilink")                              |                                              |              |
| [荒村公寓](../Page/荒村公寓.md "wikilink")                                 | 郭徑                                           |              |
| [劍雨](../Page/劍雨.md "wikilink")                                     | 雷彬                                           |              |
| [I Come With The Rain](../Page/I_Come_With_The_Rain.md "wikilink") |                                              |              |
| 2012年                                                              | [情謎](../Page/情迷_\(2012年电影\).md "wikilink")   | 方亦楠          |
| [春嬌與志明](../Page/春嬌與志明.md "wikilink")                               | 張志明                                          |              |
| [醉後一夜](../Page/醉後一夜.md "wikilink")                                 | 沈偉                                           |              |
| [車手](../Page/車手.md "wikilink")                                     | 陳翔                                           |              |
| [血滴子](../Page/血滴子.md "wikilink")                                   | 海都                                           |              |
| 2013年                                                              | [救火英雄](../Page/救火英雄.md "wikilink")           | 游邦潮          |
| [飛虎出征](../Page/飛虎出征.md "wikilink")                                 | 陈铭富                                          |              |
| 2014年                                                              | [香港仔](../Page/香港仔_\(電影\).md "wikilink")      | Dan          |
| [閨密](../Page/閨密_\(電影\).md "wikilink")                              | 喬立                                           |              |
| [金雞SSS](../Page/金雞SSS.md "wikilink")                               | 地铁嫖客                                         |              |
| 2015年                                                              | [賭城風雲II](../Page/賭城風雲II.md "wikilink")       | 阿樂           |
| [赤道](../Page/赤道_\(電影\).md "wikilink")                              | 范家明                                          |              |
| [迷城](../Page/迷城.md "wikilink")                                     | 郭少聰                                          |              |
| [陀地驅魔人](../Page/陀地驅魔人.md "wikilink")                               | 劉江                                           |              |
| 2016年                                                              | [賭城風雲III](../Page/賭城風雲III.md "wikilink")     | 阿樂           |
| [一念無明](../Page/一念無明_\(電影\).md "wikilink")                          | 黃世東                                          |              |
| 2017年                                                              | [春嬌救志明](../Page/春嬌救志明.md "wikilink")         | 張志明          |
| [悟空傳](../Page/悟空傳.md "wikilink")                                   | [楊戩 (二郎神)](../Page/楊戩_\(二郎神\).md "wikilink") |              |
| [狂獸](../Page/狂獸.md "wikilink")                                     | 江貴成                                          |              |

### 配音

  - 2010年：《[魔髮奇緣](../Page/魔髮奇緣.md "wikilink")》

### 廣告

#### 平面廣告

  - Sony（2000）
  - Extra（2000）
  - Instant-Dict快譯通（2000）
  - Timberland（2001）
  - Adidas（2001）
  - [香港貿易發展局](../Page/香港貿易發展局.md "wikilink")2000/2001場刊
  - Giodano《Blue Stars》2000 Winter
  - Giodano Taiwan2001 Spring/Summer
  - IDD 0060（2001）
  - Giodano Hong Kong 2001 Winter
  - Giodano Spring/Summer 2002
  - Diamond Line Valentine Print Adv（2002）
  - Gillette鬚刨（2002）
  - Meko 薄荷糖
  - Shiseido Pureness Product
  - levi's 路.我主导
  - Qoros, Qoros 5 SUV (2017)
  - Tommy Hilfiger

#### 電視廣告

  - Sony Memory Stick Viewcam（2001）
  - 7-up soft drink（2001）
  - Heng Sang Bank Credit Card恒生信用卡（2001）
  - 東方紅纖體清穢丸（2001）
  - 安東時間運動休閑鞋
  - 代言碧柔男士洗面乳
  - 可口可樂「龍鳳檸樂」
  - Pizza Hut「N發現」（2008）
  - Tommy Hilfiger

### 曾參與表演

  - 2000年：Timberland
  - 2000年：Armani Exchange
  - 2001年：Agnes b
  - 2001年：Starz People Graduation Ceremony
  - 2001年：IT fashion show
  - 2001年：Armani Exchange
  - 2001年：Puma Hit Wave DJ Nite
  - 2001年：Heelys Shoes Ribbon Cut
  - 2001年：Colombia SportWear Presentation
  - 2001年：Diamond Line Girl Contest
  - 2001年；新港城中心『冰天雪地夢幻遊蹤』
  - 2001年：黃金海岸商場『勁唱熱跳Count Down夜』

## 音樂

### 唱片

### 個人唱片

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>專輯 #</p></th>
<th style="text-align: left;"><p>專輯名稱</p></th>
<th style="text-align: left;"><p>專輯類型</p></th>
<th style="text-align: left;"><p>發行公司</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>1st</p></td>
<td style="text-align: left;"><p><a href="../Page/Private_Room.md" title="wikilink">Private Room</a></p></td>
<td style="text-align: left;"><p>EP</p></td>
<td style="text-align: left;"><p><a href="../Page/環球唱片_(香港).md" title="wikilink">環球唱片</a></p></td>
<td style="text-align: left;"><p>2002年9月24日</p></td>
<td style="text-align: left;"><ol>
<li>Computer Data</li>
<li>全面收購（Music Video）</li>
<li>全面收購</li>
<li>還你門匙</li>
<li>我哋</li>
<li>運動太好</li>
<li>紙巾</li>
<li>全面收購（160mph Remix）</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>2nd</p></td>
<td style="text-align: left;"><p><a href="../Page/Lost_And_Found_(余文樂專輯).md" title="wikilink">Lost And Found</a></p></td>
<td style="text-align: left;"><p>EP</p></td>
<td style="text-align: left;"><p><a href="../Page/環球唱片_(香港).md" title="wikilink">環球唱片</a></p></td>
<td style="text-align: left;"><p>2003年9月5日</p></td>
<td style="text-align: left;"><p><strong>CD</strong></p>
<ol>
<li>失戀博物館</li>
<li>最愛指數</li>
<li>放心放手</li>
<li>兩湯一麵</li>
<li>愛快羅密歐</li>
<li>司機</li>
<li>一直等待（國語）</li>
</ol>
<p><strong>VCD</strong></p>
<ol>
<li>最愛指數 Music Video</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>3rd</p></td>
<td style="text-align: left;"><p><a href="../Page/生還者_(余文樂專輯).md" title="wikilink">生還者</a></p></td>
<td style="text-align: left;"><p>EP</p></td>
<td style="text-align: left;"><p><a href="../Page/環球唱片_(香港).md" title="wikilink">環球唱片</a></p></td>
<td style="text-align: left;"><p>2004年11月17日</p></td>
<td style="text-align: left;"><p><strong>CD</strong></p>
<ol>
<li>生還者</li>
<li>南拳</li>
<li>雷霆傘兵</li>
<li>護衛</li>
<li>人民英雄</li>
<li>生還者（Acoustic Mix）</li>
<li>堅強（國語）</li>
</ol>
<p><strong>VCD</strong></p>
<ol>
<li>生還者MV</li>
<li>余文樂 '生還者' 之三日兩夜外展訓練營</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><a href="../Page/生還者_(余文樂專輯).md" title="wikilink">生還者（第二版）</a></p></td>
<td style="text-align: left;"><p>EP</p></td>
<td style="text-align: left;"><p><a href="../Page/環球唱片_(香港).md" title="wikilink">環球唱片</a></p></td>
<td style="text-align: left;"><p>2005年2月4日</p></td>
<td style="text-align: left;"><p><strong>CD</strong></p>
<ol>
<li>生還者</li>
<li>南拳</li>
<li>雷霆傘兵</li>
<li>護衛</li>
<li>人民英雄</li>
<li>生還者（Acoustic Mix）</li>
<li>堅強（國語）</li>
</ol>
<p><strong>VCD 1</strong></p>
<ol>
<li>生還者MV</li>
<li>余文樂 '生還者' 之三日兩夜外展訓練營</li>
</ol>
<p><strong>VCD 2</strong></p>
<ol>
<li>護衛MV</li>
<li>南拳MV</li>
<li>堅強MV</li>
</ol></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>4th</p></td>
<td style="text-align: left;"><p><a href="../Page/Whether_Or_Not.md" title="wikilink">Whether Or Not</a></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/環球唱片_(香港).md" title="wikilink">環球唱片</a></p></td>
<td style="text-align: left;"><p>2005年10月21日</p></td>
<td style="text-align: left;"><ol>
<li>晴天行雷</li>
<li>識笑識走</li>
<li>一二三紅綠燈</li>
<li>名牌</li>
<li>下一次真愛（國語）</li>
<li>南拳（國語）</li>
<li>風雨（國語）</li>
<li>交換（國語）</li>
<li>原因（國語）</li>
<li>角落（國語）</li>
<li>堅強（國語）</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>5th</p></td>
<td style="text-align: left;"><p><a href="../Page/不是明星.md" title="wikilink">不是明星</a><small><strong>（國語專輯）</strong></small></p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/東亞唱片_(集團).md" title="wikilink">東亞唱片</a></p></td>
<td style="text-align: left;"><p>2011年4月13日</p></td>
<td style="text-align: left;"><p><strong>CD</strong></p>
<ol>
<li>不是明星</li>
<li>給我一杯</li>
<li>喂！喂！</li>
<li>其實我們很熟</li>
<li>默背妳的心碎</li>
<li>男人愛麻煩（余文樂/陳奐仁）</li>
<li>快瘋了</li>
<li>你說我太帥</li>
<li>有骨氣</li>
<li>給我一杯（NIGO® REMIX）</li>
</ol>
<p><strong>DVD</strong></p>
<ol>
<li>不是明星MV</li>
<li>給我一杯MV</li>
<li>其實我們很熟MV</li>
<li>默背妳的心碎MV</li>
</ol></td>
</tr>
</tbody>
</table>

### 單曲

  - 《明知故犯》（與[張燊悅合唱](../Page/張燊悅.md "wikilink")）
  - 《純白之戀》主題曲（與[張娜拉合唱](../Page/張娜拉.md "wikilink")）
  - 《無言以愛》（《男才女貌》主題曲）

## 派台歌曲成績

| **四台派台歌曲最高位置**                                                   |
| ---------------------------------------------------------------- |
| 唱片                                                               |
| **2002年**                                                        |
| [Angel of Mercy](../Page/Angel_of_Mercy.md "wikilink")           |
| [Private Room](../Page/Private_Room.md "wikilink")               |
| [Private Room](../Page/Private_Room.md "wikilink")               |
| [Private Room](../Page/Private_Room.md "wikilink")               |
| **2003年**                                                        |
| [Lost And Found](../Page/Lost_And_Found_\(余文樂專輯\).md "wikilink") |
| [Lost And Found](../Page/Lost_And_Found_\(余文樂專輯\).md "wikilink") |
| [Lost And Found](../Page/Lost_And_Found_\(余文樂專輯\).md "wikilink") |
| **2004年**                                                        |
| [生還者](../Page/生還者_\(余文樂專輯\).md "wikilink")                       |
| [生還者](../Page/生還者_\(余文樂專輯\).md "wikilink")                       |
| **2005年**                                                        |
| [生還者](../Page/生還者_\(余文樂專輯\).md "wikilink")                       |
| [Whether Or Not](../Page/Whether_Or_Not.md "wikilink")           |
|                                                                  |
| [Whether Or Not](../Page/Whether_Or_Not.md "wikilink")           |
| [Whether Or Not](../Page/Whether_Or_Not.md "wikilink")           |
| **2011年**                                                        |
| [不是明星](../Page/不是明星.md "wikilink")                               |
| [不是明星](../Page/不是明星.md "wikilink")                               |
| [不是明星](../Page/不是明星.md "wikilink")                               |
| **2012年**                                                        |
| [春嬌與志明Single](../Page/春嬌與志明_\(單曲\).md "wikilink")                |
| [寰亞時代](../Page/寰亞時代.md "wikilink")                               |

| **各台冠軍歌總數** |
| ----------- |
| 903         |
| **1**       |

## MTV拍攝

  - [梁詠琪](../Page/梁詠琪.md "wikilink") - 《繼續愛》
  - [游鴻明](../Page/游鴻明.md "wikilink") - 《樓下那個女人》
  - [香港電台](../Page/香港電台.md "wikilink") - 完全學生手冊之記錄17歲
  - 香港電台 - Music Appreciation
  - [有線電視](../Page/有線電視.md "wikilink") - 音樂台WE WE Wet Wet
  - [新城電台Young](../Page/新城電台.md "wikilink")\!我們造得更好夏令行動 -
    《Young\!我們造得更型》
  - [商業電台](../Page/商業電台.md "wikilink")[雷霆881](../Page/雷霆881.md "wikilink")
    - [戀上你的床廣播劇](../Page/戀上你的床.md "wikilink")《好趁青春留倩影》
  - [張韶涵](../Page/張韶涵.md "wikilink") - 《靜不下來》
  - [鄭　融](../Page/鄭融.md "wikilink") - 《失蹤主角》
  - 鄭　融 - 《紅綠燈》
  - [濱崎步](../Page/濱崎步.md "wikilink") - 《距愛～Distance Love～》
  - [吳雨霏](../Page/吳雨霏.md "wikilink") - 《人非草木》（導演）
  - [Super Girls](../Page/Super_Girls.md "wikilink") - 《有事發生》

## 獎項

  - 2002年：PM 新世紀最具人氣男新星
  - 2002年：2002年度[叱咤樂壇流行榜頒獎典禮](../Page/叱咤樂壇流行榜頒獎典禮.md "wikilink")
    叱-{咤}-樂壇生力軍男歌手銀獎
  - 2002年：2002年度[十大勁歌金曲頒獎典禮](../Page/十大勁歌金曲頒獎典禮.md "wikilink")
    新星試打金曲獎（還你門匙）
  - 2008年：韓國[富川國際電影節](../Page/富川國際電影節.md "wikilink") 最佳男主角
  - 2017年: 第五屆十大華語表彰典禮 年度男演員［一念無明］

## 軼聞

  - 余文樂2007年[視網膜脫落](../Page/視網膜脫落.md "wikilink")，之後明顯大小眼，有傳他是裝了假眼\[16\]。
  - 余文樂為愛車之人，並擁有多輛名車，例如他於2012年用了超過80萬元買入黑色的[Range
    Rover](../Page/Range_Rover.md "wikilink") Evoque越野車。\[17\]

## 參考來源

## 外部連結

  -
  -
  -
  - 官方

  -
  -
  -
  -
  -
  -
[WEN](../Category/余姓.md "wikilink")
[Category:台山人](../Category/台山人.md "wikilink")
[Category:广东人](../Category/广东人.md "wikilink")
[Category:21世紀歌手](../Category/21世紀歌手.md "wikilink")
[Category:21世紀男演員](../Category/21世紀男演員.md "wikilink")
[Category:香港男歌手](../Category/香港男歌手.md "wikilink")
[Category:粵語流行音樂歌手](../Category/粵語流行音樂歌手.md "wikilink")
[Category:香港男演員](../Category/香港男演員.md "wikilink")
[Category:香港電影男演員](../Category/香港電影男演員.md "wikilink")
[Category:香港電視演員](../Category/香港電視演員.md "wikilink")
[Category:香港男性模特兒](../Category/香港男性模特兒.md "wikilink")
[Category:天主教培聖中學校友](../Category/天主教培聖中學校友.md "wikilink")
[Category:基督教香港信義會心誠中學校友](../Category/基督教香港信義會心誠中學校友.md "wikilink")
[Category:基督教香港信義會元朗信義中學校友](../Category/基督教香港信義會元朗信義中學校友.md "wikilink")

1.

2.

3.

4.

5.  [【狂獸】張晉搏殺拍戲也要陪家中三個女狂人　余文樂水底硬食重拳](http://bka.mpweekly.com/interview/%E5%A8%9B%E6%A8%82123/20171118-91107)

6.  [【找數真漢子】講明要升降台出場
    志明撐春嬌合唱〈當我想起你〉](https://bka.mpweekly.com/focus/local/20180103-96732)

7.  [余文樂被逮約會皮帶千金　醉眼洩愛意](https://tw.appledaily.com/new/realtime/20161219/1015473/)

8.  [【男神封盤】余文樂閃婚 發表愛妻宣言](http://bka.mpweekly.com/focus/20171205-93127)

9.

10.

11. [【晒透明喼】余文樂曲線讚老婆](https://bka.mpweekly.com/focus/20180626-120319)

12.

13.

14. [余文樂創瘋狂電競，成立《傳說對決》《英雄聯盟》MAD TEAM
    戰隊](https://www.4gamers.com.tw/news/detail/33795/shawn-sue-establish-mad-crew-esport)

15. [青苔官方網站](http://moss.big.com.hk/)

16.

17.