**陆军第十九镇**，[清朝末年军队现代化改革之后的](../Page/清朝.md "wikilink")[新军编制之一](../Page/新军.md "wikilink")，相当于现在的师的规模。第十九镇驻[云南](../Page/云南.md "wikilink")，共有步、马、炮等21个营。

[光绪三十四年](../Page/光绪三十四年.md "wikilink")（1908年），清廷派陆军统制[崔祥奎来云南](../Page/崔祥奎.md "wikilink")，将云南陆军混成协扩编为一镇。[云贵总督](../Page/云贵总督.md "wikilink")[锡良把](../Page/锡良.md "wikilink")“督练处”改为“督练公所”，按全国陆军编制序列，番号为“陆军第十九镇”，统制崔祥奎，下辖两协，每协两标，每标三营，每营官兵536人，全镇官兵1.09万人。

[光绪三十五年](../Page/光绪三十五年.md "wikilink")（1909年），[锡良调任东三省总督](../Page/锡良.md "wikilink")，[沈秉堃护理](../Page/沈秉堃.md "wikilink")[云贵总督](../Page/云贵总督.md "wikilink")，购买德国克虏伯最新式步枪8000支、山炮54门、重机枪50挺，全部用来装备陆军第十九镇。

辛亥革命前夕，其统制为[钟麟同](../Page/钟麟同.md "wikilink")，总参议为[靳云鹏](../Page/靳云鹏.md "wikilink")，兵备处总办为[王振畿](../Page/王振畿.md "wikilink")。第三十七协统领[蔡锷](../Page/蔡锷.md "wikilink")。第三十八协协统[曲同丰](../Page/曲同丰.md "wikilink")。

[辛亥革命云南](../Page/辛亥革命.md "wikilink")[重九起义参加者](../Page/重九起义.md "wikilink")，主要为该镇中下层青年官兵。

## 部隊隸屬組織

  - 第三十七协，[统领](../Page/统领.md "wikilink")[蔡锷](../Page/蔡锷.md "wikilink")
      - 第七十三标，[标统丁锦](../Page/标统.md "wikilink")，反对革命。
          - 七十三标第三营，[管带](../Page/管带.md "wikilink")[李鸿祥](../Page/李鸿祥.md "wikilink")
      - 第七十四标，标统[罗佩金](../Page/罗佩金.md "wikilink")
          - 七十四标第一营，管带[唐继尧](../Page/唐继尧.md "wikilink")
          - 七十四标第二营，管带[刘存厚](../Page/刘存厚.md "wikilink")
              - 七十四标二营左队任[司务长](../Page/司务长.md "wikilink")(相当于[排长](../Page/排长.md "wikilink"))[朱德](../Page/朱德.md "wikilink")
              - 七十四标二营[哨官](../Page/哨官.md "wikilink")（[连长](../Page/连长.md "wikilink")）[武士英](../Page/武士英.md "wikilink")（原名吴福铭，山西平阳人，暗杀[宋教仁的凶手](../Page/宋教仁.md "wikilink")）
          - 第七十四标第三营，管带[雷飚](../Page/雷飚.md "wikilink")
  - 第三十八协，统领[曲同丰](../Page/曲同丰.md "wikilink")
      - 第七十五标
      - 第七十六标，标统[涂孝烈](../Page/涂孝烈.md "wikilink")
          -   - 士兵，[杨希闵](../Page/杨希闵.md "wikilink")
  - 炮兵第十九标
      - 第三营，管带[谢汝翼](../Page/谢汝翼.md "wikilink")
  - 马軍第十九标
  - 工程第十九营
  - 辎重第十九营
  - 机关枪第十九营
  - 陆军警察第十九营
  - 军乐队

## 外部連結

  - [陸軍第十九鎮及雲南講武堂對雲南辛亥革命的關係](http://tci.ncl.edu.tw/cgi-bin/gs32/gsweb.cgi?o=dnclret&s=id=%22TCI0000607138%22.&searchmode=basic&tcihsspage=tcisearch_opt2_search)
  - [清朝云南驻军从绿营到新军第十九镇](http://history.kunming.cn/index/content/2009-09/08/content_1963407.htm)

[Category:新建陆军](../Category/新建陆军.md "wikilink")
[Category:云南军事史](../Category/云南军事史.md "wikilink")