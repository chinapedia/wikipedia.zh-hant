[ChaseBankChinatownManhattan.jpg](https://zh.wikipedia.org/wiki/File:ChaseBankChinatownManhattan.jpg "fig:ChaseBankChinatownManhattan.jpg")[曼哈頓](../Page/曼哈頓.md "wikilink")[華埠](../Page/華埠_\(曼哈頓\).md "wikilink")\]\]
**大通银行**（，[商業名稱](../Page/商業名稱.md "wikilink")：**Chase
Bank**，[商標](../Page/商標.md "wikilink")：大通、JP摩根、JP摩根大通\[1\]）是一家[美國的國家銀行](../Page/美國.md "wikilink")，是[摩根大通的子公司](../Page/摩根大通.md "wikilink")，專注于[商業和](../Page/商業銀行.md "wikilink")[零售银行業務](../Page/零售银行.md "wikilink")。大通银行與JP摩根公司在2000年合併，該銀行前稱為**大通曼哈頓銀行**（）\[2\]。大通曼哈頓銀行是由**大通國家銀行**（）和**曼哈頓公司銀行**（）於1955年合併而成立\[3\]。自2004年與**[芝加哥第一銀行](../Page/芝加哥第一銀行.md "wikilink")**合併以來，該銀行總部位於[俄亥俄州哥倫布市](../Page/哥伦布_\(俄亥俄州\).md "wikilink")
\[4\]。該銀行收購了[華盛頓互惠的存款和大部分資產](../Page/華盛頓互惠.md "wikilink")。

大通在全美設有5,100多家分行和16,100台[自動櫃員機](../Page/自動櫃員機.md "wikilink")。摩根大通擁有235,678名員工（截至2015年），並在100多個國家開展業務。
摩根大通目前擁有約2.6萬億美元的資產。

摩根大通透過其子公司**大通**成為[美國四大銀行之一](../Page/四大銀行#美國.md "wikilink")。\[5\]\[6\]

[缩略图](https://zh.wikipedia.org/wiki/File:Chase_logo_pre_merger.png "fig:缩略图")

## 歷史

### 曼哈顿公司

美國大通銀行可追溯於1799年創立的曼哈顿银行。\[7\]

阿龙·伯尔與亞歷山大·咸美頓，除了作為商業競爭對手，亦是政治上競爭對手。他們在1804年7月11日進行[決鬥](../Page/伯尔-汉密尔顿决斗.md "wikilink")，最後亞歷山大·咸美頓於翌日重傷死亡。當時決鬥所用的手槍由美國大通銀行持有，現時於[摩根大通總部展出](../Page/摩根大通.md "wikilink")。[John_D._Rockefeller,_Jr._(1915).jpg](https://zh.wikipedia.org/wiki/File:John_D._Rockefeller,_Jr._\(1915\).jpg "fig:John_D._Rockefeller,_Jr._(1915).jpg")是美國大通國家銀行大股東|替代=|无\]\]

### 大通國民银行

[AaronBurr.jpg](https://zh.wikipedia.org/wiki/File:AaronBurr.jpg "fig:AaronBurr.jpg"),
曼哈顿公司创办者\]\]
[ChaseBellaireBeltwayHouston.JPG](https://zh.wikipedia.org/wiki/File:ChaseBellaireBeltwayHouston.JPG "fig:ChaseBellaireBeltwayHouston.JPG")[中國城](../Page/中国城_\(休斯敦\).md "wikilink")\]\]

大通國民银行於1877年由[約翰·湯普生創立](../Page/約翰·湯普生.md "wikilink"),\[8\]
名稱由美國財政部長兼首席法官[萨蒙·波特兰·蔡斯而來](../Page/萨蒙·波特兰·蔡斯.md "wikilink"),\[9\]
雖然蔡斯與銀行業無關。\[10\]
大通國民银行於1920年代透過旗下的[證券公司](../Page/證券.md "wikilink")，收購了幾家規模細小的銀行。其最著名的收購是於1930年代收購了大股東是小約翰.洛克斐勒的纽约公平信托公司。\[11\]
這使它成美國乃至世界的大型銀行。大通国民银行也积极参与[石油工业的投资中](../Page/石油工业.md "wikilink")，董事会管理层与[标准石油的公司继承者长期保持联系](../Page/标准石油.md "wikilink")，其后改为[埃克森美孚](../Page/埃克森美孚.md "wikilink")。

### 合併

1955年，大通国民银行与曼哈顿银行合并为大通曼哈顿银行。\[12\]
因为大通国民银行是相对一个较大的银行，所以大通国民银行最初拥有“曼哈顿银行”作为它的外号，但这样就泄露了伯尔最初以曼哈顿公司作为银行的意图，这不单包括需要条款来允许它用盈余资金来启动银行，还需要股份持有者的一致同意来完成银行的收购。最后交易是因为调整为大通国民曼哈顿公司银行而获得成功，而[John
J.McCloy成为合并公司的](../Page/John_J.McCloy.md "wikilink")[董事长](../Page/董事长.md "wikilink")。这样就不需要股份持有者的一致同意。在McCloy的后继者[George
Champion带领下](../Page/George_Champion.md "wikilink"),
共1799间过时的州际分行被合并为一间现代银行。1969年，在[戴卫.洛克菲勒的带领下](../Page/戴卫.洛克菲勒.md "wikilink")，银行成为[银行控股公司](../Page/银行控股公司.md "wikilink")[大通曼哈顿集团的旗下一份子](../Page/大通曼哈顿集团.md "wikilink")。\[13\]

### 最近購併

1996年7月，刚收购的[美华银行成功兼并了美国大通银行](../Page/纽约化学银行.md "wikilink")。尽管在国家登记的名字依然是[美华银行](../Page/纽约化学银行.md "wikilink")，但因为大通银行更为世人所知，所以依旧以大通曼哈顿银行命名。
随后，JP摩根集团与大通曼哈顿银行在2000年12月正式合并。合并后，公司更名为[JP摩根大通](../Page/JP摩根大通.md "wikilink")。在2004年，其继而收购了[芝加哥第一银行](../Page/芝加哥第一银行.md "wikilink")，使得美国大通银行成为美国最大的信用卡发行商。2008年3月24日，[JP摩根大通成功收购了曾为](../Page/JP摩根大通.md "wikilink")2007年全美第五大[投资银行的](../Page/投资银行.md "wikilink")[贝尔斯登](../Page/贝尔斯登.md "wikilink")。
摩根大通集团於2008年9月25日以19億美元收购[華盛頓互惠銀行](../Page/華盛頓互惠銀行.md "wikilink")。
[華盛頓互惠銀行在西海岸常年来对顾客的各种免费政策也随着被Chase的兼并而改变](../Page/華盛頓互惠銀行.md "wikilink")。

### 境外的信用卡公司

[Chase在加拿大为BestBuy](../Page/Chase.md "wikilink")（[百思买](../Page/百思买.md "wikilink")）和Futureshop，Sears（[西尔斯](../Page/西尔斯.md "wikilink")）等公司签发store
credit card。但是不在加拿大从事其他金融业务。

## 腳註參考

## 進階閱讀

  - *The Chase: The Chase Manhattan Bank, N.A., 1945-1985*, John Donald
    Wilson, Boston: Harvard Business School Press, 1986.
  - *Memoirs.* David Rockefeller, New York: Random House, 2002.
  - *The Chairman: John J. McCloy - The Making of the American
    Establishment,* Kai Bird, New York: Simon & Schuster, 1992.
  - *Water for Gotham: A History,* Gerard T. Koeppel, Princeton:
    Princeton University Press, 2000.

## 外部連結

  - [Chase website](http://www.chase.com/)
  - [An Evolutionary View of Internationalization: Chase Manhattan
    Bank, 1917
    to 1996.](https://web.archive.org/web/20060830205559/http://fic.wharton.upenn.edu/fic/papers/02/0237.pdf)
    A Financial Institutions Center study (pdf) completed in 2002.

[Category:洛克菲勒家族](../Category/洛克菲勒家族.md "wikilink")
[Category:摩根大通](../Category/摩根大通.md "wikilink")
[Category:1799年成立的公司](../Category/1799年成立的公司.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.
9.

10.
11. David Rockefeller, *Memoirs*, New York: Random House, 2002.
    (pp.124-25)

12.
13.