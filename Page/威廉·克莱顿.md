[William_L._Clayton_arrives_for_Potsdam_conference.jpg](https://zh.wikipedia.org/wiki/File:William_L._Clayton_arrives_for_Potsdam_conference.jpg "fig:William_L._Clayton_arrives_for_Potsdam_conference.jpg")
**威廉·洛克哈特·克莱顿**（[英文](../Page/英文.md "wikilink")：**William Lockhart
Clayton**，），[美国著名商界领袖](../Page/美国.md "wikilink")、政府官员。

## 生平

克莱顿出生于[密西西比州](../Page/密西西比州.md "wikilink")，成长于[田纳西州](../Page/田纳西州.md "wikilink")。他13岁即辍学，成为[圣路易斯一名](../Page/圣路易斯_\(密苏里州\).md "wikilink")[棉花商人的助手](../Page/棉花.md "wikilink")。1896年，克莱顿在[纽约的](../Page/纽约.md "wikilink")[美国棉花公司找到一份工作](../Page/美国棉花公司.md "wikilink")，1904年即升任该公司助理经理，同年他辞职，与两名合作伙伴在[俄克拉何马城开办了自己的棉花贸易公司](../Page/俄克拉何马城.md "wikilink")。1916年，该公司迁至[休斯敦](../Page/休斯敦.md "wikilink")，成为当时世界上规模最大的棉花贸易公司。

[一战结束后](../Page/一战.md "wikilink")，克莱顿进入政府的棉花分配委员会服务。虽然他是忠诚的[民主党支持者](../Page/美国民主党.md "wikilink")，但是一开始却并不支持[富兰克林·罗斯福的](../Page/富兰克林·罗斯福.md "wikilink")[新政](../Page/新政.md "wikilink")，不过新政的良好效果使其于1936年改变看法，成为罗斯福的支持者。

1940年，克莱顿进入[Reconstruction Finance
Corporation服务](../Page/Reconstruction_Finance_Corporation.md "wikilink")，后又转去[进出口银行](../Page/进出口银行.md "wikilink")。在此期间，他限制向[纳粹](../Page/纳粹.md "wikilink")[德国出口战略物资](../Page/德国.md "wikilink")。1944年1月，由于同副总统[亨利·华莱士产生矛盾](../Page/亨利·华莱士.md "wikilink")，克莱顿辞去了政府职务，但不久即重返政府。

1944年底，克莱顿出任负责经济事务的[助理国务卿](../Page/助理国务卿.md "wikilink")，任内他大力推行自由贸易，他是[哈里·杜鲁门总统参加](../Page/哈里·杜鲁门.md "wikilink")[波茨坦会议时的经济顾问](../Page/波茨坦会议.md "wikilink")，也负责向[亨利·史汀生和杜鲁门就](../Page/亨利·史汀生.md "wikilink")[原子弹引起的问题提供对策](../Page/原子弹.md "wikilink")。克莱顿强烈支持对[欧洲提供经济援助](../Page/欧洲.md "wikilink")，他是1947年[马歇尔计划的主要推动者之一](../Page/马歇尔计划.md "wikilink")。

1948年，克莱顿退出公众服务。

[Category:美國國務次卿](../Category/美國國務次卿.md "wikilink")
[Category:美國助理國務卿](../Category/美國助理國務卿.md "wikilink")
[Category:美國民主黨員](../Category/美國民主黨員.md "wikilink")
[Category:美國第二次世界大戰人物](../Category/美國第二次世界大戰人物.md "wikilink")
[Category:美國企業家](../Category/美國企業家.md "wikilink")
[Category:密西西比州人](../Category/密西西比州人.md "wikilink")
[Category:田納西州人](../Category/田納西州人.md "wikilink")