**什羅普郡**（，\[1\]；[國際音標](../Page/國際音標.md "wikilink")：／，英文簡稱：Salop\[2\]／Shrops\[3\]），[英國](../Page/英國.md "wikilink")[英格蘭](../Page/英格蘭.md "wikilink")[西米德蘭茲的](../Page/西米德蘭茲.md "wikilink")[單一管理區](../Page/英格蘭的郡.md "wikilink")，西接[威爾斯的邊界](../Page/威爾斯.md "wikilink")。以人口計算，[什魯斯伯里是第](../Page/什魯斯伯里.md "wikilink")1大鎮，[特爾福德是第](../Page/特爾福德.md "wikilink")1大[新市鎮](../Page/英國的新市鎮.md "wikilink")（包含多個鎮：第2大鎮[惠靈頓](../Page/惠靈頓_\(什羅普郡\).md "wikilink")、第3大鎮[梅德利等](../Page/梅德利_\(什羅普郡\).md "wikilink")），第4大鎮[奧斯沃斯特里](../Page/奧斯沃斯特里.md "wikilink")；第1大[自治市鎮](../Page/英國的自治市鎮.md "wikilink")（Borough）是[什魯斯伯里-阿查姆](../Page/什魯斯伯里-阿查姆.md "wikilink")。

2009年4月之前，什羅普郡是34個[非都市郡之一](../Page/非都市郡.md "wikilink")，實際管轄5個[非都市區](../Page/非都市區.md "wikilink")，[佔地](../Page/面積.md "wikilink")3,197[平方公里](../Page/平方公里.md "wikilink")（[第14](../Page/英格蘭的非都市郡列表_\(以面積排列\).md "wikilink")），有289,200[人口](../Page/人口.md "wikilink")（[第34](../Page/英格蘭的非都市郡列表_\(以人口排列\).md "wikilink")）；如看待成48個[名譽郡之一](../Page/名譽郡.md "wikilink")，它名義上包含多1個[單一管理區](../Page/單一管理區.md "wikilink")─[特爾福德和雷金](../Page/特爾福德和雷金.md "wikilink")，[佔地增至](../Page/面積.md "wikilink")3,487[平方公里](../Page/平方公里.md "wikilink")（[第13](../Page/英格蘭的名譽郡列表_\(以面積排列\).md "wikilink")），[人口增至](../Page/人口.md "wikilink")451,100（[第42](../Page/英格蘭的名譽郡列表_\(以人口排列\).md "wikilink")）。

2009年4月，什羅普廢郡改制為2個[單一管理區](../Page/單一管理區.md "wikilink")

## 行政區劃

### 1972-2009

[ 1. [什羅普郡北](../Page/什羅普郡北.md "wikilink")
2\. [奧斯沃斯特里](../Page/奧斯沃斯特里區.md "wikilink")
3\. [什魯斯伯里-阿查姆](../Page/什魯斯伯里-阿查姆.md "wikilink")
4\. [什羅普郡南](../Page/什羅普郡南.md "wikilink")
5\. [布里奇諾斯](../Page/布里奇諾斯區.md "wikilink")
6\.
[特爾福德-雷金](../Page/特爾福德-雷金.md "wikilink")<small>（[單一管理區](../Page/單一管理區.md "wikilink")）</small>
](https://zh.wikipedia.org/wiki/File:Shropshire_Ceremonial_Numbered.png "fig: 1. 什羅普郡北 2. 奧斯沃斯特里 3. 什魯斯伯里-阿查姆 4. 什羅普郡南 5. 布里奇諾斯 6. 特爾福德-雷金（單一管理區） ")
什羅普郡是[非都市郡](../Page/非都市郡.md "wikilink")，實際管轄5個[非都市區](../Page/非都市區.md "wikilink")：[什羅普郡北](../Page/什羅普郡北.md "wikilink")（North
Shropshire）、[奧斯沃斯特里](../Page/奧斯沃斯特里區.md "wikilink")（Oswestry）、[什魯斯伯里-阿查姆](../Page/什魯斯伯里-阿查姆.md "wikilink")（Shrewsbury
and Atcham）、[什羅普郡南](../Page/什羅普郡南.md "wikilink")（South
Shropshire）、[布里奇諾斯](../Page/布里奇諾斯區.md "wikilink")（Bridgnorth）；如看待成[名譽郡](../Page/名譽郡.md "wikilink")，它名義上包含多1個[單一管理區](../Page/單一管理區.md "wikilink")：[特爾福德-雷金](../Page/特爾福德-雷金.md "wikilink")（Telford
and Wrekin）。

### 2009至今

2009年4月1日，英格蘭地方政府結構變化正式實施，7個[英格蘭的郡改革行政區劃](../Page/英格蘭的郡.md "wikilink")，產生9個新的[單一管理區](../Page/單一管理區.md "wikilink")。什羅普由[非都市郡改制為](../Page/非都市郡.md "wikilink")[單一管理區](../Page/單一管理區.md "wikilink")。
[ 1.
[什羅普](../Page/什羅普.md "wikilink")（[單一管理區](../Page/單一管理區.md "wikilink")）
2\.
[特爾福德-雷金](../Page/特爾福德-雷金.md "wikilink")（[單一管理區](../Page/單一管理區.md "wikilink")）</small>
](https://zh.wikipedia.org/wiki/File:New_Shropshire_Ceremonial_Numbered.png "fig: 1. 什羅普（單一管理區） 2. 特爾福德-雷金（單一管理區） ")

## 旅遊

著名的[鐵橋](../Page/鐵橋.md "wikilink")（The Iron
Bridge）位於[艾恩布里奇](../Page/艾恩布里奇.md "wikilink")（Ironbridge）。

## 地理

下圖顯示英格蘭48個名譽郡的分佈情況。什羅普郡東北至南與[英格蘭的郡相鄰](../Page/英格蘭的郡.md "wikilink")：東北與[柴郡相鄰](../Page/柴郡.md "wikilink")，東與[斯塔福德郡相鄰](../Page/斯塔福德郡.md "wikilink")，東南與[伍斯特郡相鄰](../Page/伍斯特郡.md "wikilink")，南與[萊斯特郡相鄰](../Page/萊斯特郡.md "wikilink")；西南至西北接[威爾斯邊界](../Page/威爾斯.md "wikilink")：西南、西與[波伊斯郡相鄰](../Page/波伊斯郡.md "wikilink")，西北與[雷克瑟姆郡相鄰](../Page/雷克瑟姆郡.md "wikilink")。

什羅普郡是英格蘭人口最稀疏的鄉間地區之一。

## 注釋

## 外部連結

  - 什羅普郡[旅遊局](http://www.shropshiretourism.info/)
  - 1911年版本《[大英百科全書](../Page/大英百科全書.md "wikilink")》[什羅普郡](https://web.archive.org/web/20070102151346/http://www.1911encyclopedia.org/Shropshire)
  - 《[什羅普郡雜誌](http://www.shropshiremagazine.com/)》介紹什羅普郡而獲獎的雜誌

[Category:英格蘭的郡](../Category/英格蘭的郡.md "wikilink")
[施洛普郡](../Category/施洛普郡.md "wikilink")

1.  見[香港](../Page/香港.md "wikilink")[大帽山](../Page/大帽山.md "wikilink")[石崗村街道](../Page/石崗村.md "wikilink")[史樂信路](../Page/史樂信路.md "wikilink")
    *Shropshire Road*
2.  [Blandings: English
    Counties](http://www.blandings.org.uk/what/Counties.htm)
3.  [SHROPS - What does SHROPS stand
    for?](http://acronyms.thefreedictionary.com/SHROPS)