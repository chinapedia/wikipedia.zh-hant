**高山狸藻**（[學名](../Page/學名.md "wikilink")：***Utricularia
alpina***）為[狸藻屬中型的陸生或](../Page/狸藻屬.md "wikilink")[附生](../Page/附生植物.md "wikilink")[多年生](../Page/多年生植物.md "wikilink")[食虫植物](../Page/食虫植物.md "wikilink")。其[种加词](../Page/种加词.md "wikilink")“*alpina*”来源于[拉丁文](../Page/拉丁文.md "wikilink")“*alpinus*”，意为“高山”，指其原生地环境。其分布于[安的列斯群島和](../Page/安的列斯群島.md "wikilink")[南美洲北部](../Page/南美洲.md "wikilink")。在安的列斯群島其存在于[多米尼克](../Page/多米尼克.md "wikilink")、[格瑞納達](../Page/格瑞納達.md "wikilink")、[瓜德羅普](../Page/瓜德羅普.md "wikilink")、[牙買加](../Page/牙買加.md "wikilink")、[馬提尼克](../Page/馬提尼克.md "wikilink")、[蒙特塞拉特](../Page/蒙特塞拉特.md "wikilink")、[薩巴](../Page/薩巴_\(荷蘭\).md "wikilink")、[聖基茨和尼維斯](../Page/聖基茨和尼維斯.md "wikilink")、[聖露西亞](../Page/聖露西亞.md "wikilink")、[聖文森島以及](../Page/聖文森島.md "wikilink")[千里達島](../Page/千里達島.md "wikilink")。在[南美洲北部其存在于](../Page/南美洲.md "wikilink")[巴西](../Page/巴西.md "wikilink")、[哥倫比亞](../Page/哥倫比亞.md "wikilink")、[蓋亞那以及](../Page/蓋亞那.md "wikilink")[委內瑞拉](../Page/委內瑞拉.md "wikilink")。\[1\]高山狸藻附生于长满青苔的树干上，海拔分布范围为1500米至2000米。\[2\]

高山狸藻葉片寬長，花白色，具球莖，栽培介質須透氣良好，以免走莖腐爛。生長環境需低溫高濕。

## 分类学

2002年，一项基于[ISSR技术的研究对狸藻属的](../Page/ISSR.md "wikilink")10个陆生物种的种间关系进行了分析。ISSR扩展共使用了6个引物，共产生了236个片段。高山狸藻被确定与[长叶狸藻](../Page/长叶狸藻.md "wikilink")之间存在着最为密切的种间关系。在[形态学特征上](../Page/形态学.md "wikilink")，也符合这一结论。这两者都具有椭圆形或倒卵形的叶片，花丝弯曲，子房卵形，萼片为卵形至卵状三角形。\[3\]但[彼得·泰勒在其](../Page/彼得·泰勒_\(植物学家\).md "wikilink")1989年的专著《[狸藻属——分类学专著](../Page/狸藻属——分类学专著.md "wikilink")》将这两个物种分别置于[兰花狸藻组](../Page/兰花狸藻组.md "wikilink")和[叶状狸藻组](../Page/叶状狸藻组.md "wikilink")内。\[4\]

## 参考文献

  - Rutishauser, Rolf and Jeannette Brugger and Lorenz Butschi. (1992).
    [Structural and developmental diversity of *Utricularia*
    traps](http://www.carnivorousplants.org/cpn/articles/CPNv21n3p68_74.pdf).
    *[Carnivorous Plant Newsletter](../Page/食虫植物通讯.md "wikilink")*.
    **21**(3): 68–74.

## 外部連結

  - 食虫植物照片搜寻引擎中[高山狸藻的照片](http://www.cpphotofinder.com/utricularia-alpina-550.html)

[Utricularia adpressa](../Category/南美洲食虫植物.md "wikilink") [Utricularia
adpressa](../Category/中美洲食虫植物.md "wikilink")
[Category:巴西植物](../Category/巴西植物.md "wikilink")
[Category:哥伦比亚植物](../Category/哥伦比亚植物.md "wikilink")
[Category:多尼米克植物](../Category/多尼米克植物.md "wikilink")
[Category:瓜德罗普植物](../Category/瓜德罗普植物.md "wikilink")
[Category:圭亚那植物](../Category/圭亚那植物.md "wikilink")
[Category:牙买加植物](../Category/牙买加植物.md "wikilink")
[Category:马提尼克植物](../Category/马提尼克植物.md "wikilink")
[Category:特里尼达和多巴哥植物](../Category/特里尼达和多巴哥植物.md "wikilink")
[Category:委内瑞拉植物](../Category/委内瑞拉植物.md "wikilink")
[alpina](../Category/狸藻屬.md "wikilink")
[alpina](../Category/兰花狸藻组.md "wikilink")

1.  Taylor, Peter. (1989). *[The genus Utricularia - a taxonomic
    monograph](../Page/狸藻属——分类学专著.md "wikilink")*. Kew Bulletin
    Additional Series XIV: London.

2.  Butschi, Lorenz. Translation by Dorothea Huber and Klaus Ammann.
    (1989). [Carnivorous plants of Auyantepui in
    Venezuela](http://www.carnivorousplants.org/cpn/articles/CPNv18n1p15_18.pdf).
    *[Carnivorous Plant Newsletter](../Page/食虫植物通讯.md "wikilink")*.
    **18**(1): 15–18.

3.  Rahman, M. Oliur and Katsuhiko Kondo. (2002). [Evaluation of
    inter-simple sequence repeat (ISSR) for systematic relationship of
    some terresetrial species of *Utricularia* L.
    (Lentibulariaceae)](http://www.carnivorousplants.org/cpn/articles/ICPS2002confp175_188.pdf).
    Proc. 4th Intl. Carniv. Pl. Conf. 175–188.

4.