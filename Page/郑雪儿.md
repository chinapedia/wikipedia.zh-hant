**鄭雪兒**（[英文](../Page/英文.md "wikilink")：**Michelle
Saram**，），藝名「**雪兒**」（曾用藝名「**莫雅伦**」），[新加坡](../Page/新加坡.md "wikilink")[演員及](../Page/演員.md "wikilink")[歌手](../Page/歌手.md "wikilink")。本身是[中印混血兒](../Page/中印混血兒.md "wikilink")，[父親是](../Page/父親.md "wikilink")[印度人](../Page/印度.md "wikilink")，[母親是](../Page/母親.md "wikilink")[華人](../Page/華人.md "wikilink")。

## 簡歷

鄭雪兒於1997年6月與[郭富城合作拍攝](../Page/郭富城.md "wikilink")[自由易廣告而加入](../Page/自由易.md "wikilink")[娛樂圈](../Page/娛樂圈.md "wikilink")(與[蒙嘉慧合演](../Page/蒙嘉慧.md "wikilink"))，然後加入香港[無綫電視](../Page/無綫電視.md "wikilink")。2000年代，前往[台灣發展](../Page/台灣.md "wikilink")，2002年同2003年获得新加坡红星大奖「最受欢迎女艺人」。

2017年，她為報[古天樂和](../Page/古天樂.md "wikilink")[葉偉信往日提攜之恩而復出客串電影](../Page/葉偉信.md "wikilink")《[杀破狼贪狼](../Page/杀破狼贪狼.md "wikilink")》\[1\]\[2\]。

同年5月，她接拍由[邵氏兄弟及](../Page/邵氏兄弟.md "wikilink")[优酷制作的网剧及电视剧](../Page/优酷.md "wikilink")《[飞虎之潜行极战](../Page/飞虎之潜行极战.md "wikilink")》，饰演[CIA情报组主管](../Page/CIA.md "wikilink")。之后在访问中说因为该剧的[监制](../Page/监制.md "wikilink")[乐易玲邀请她](../Page/乐易玲.md "wikilink")，才促成这次的合作。她表示以为饰演的[CIA有动作戏份](../Page/CIA.md "wikilink")，到临开拍时，才知道有大量[办公室讲对白的戏份](../Page/办公室.md "wikilink")，还特意开拍前准备功课死记对白，不过她说在剧里只有一场揸枪戏份。

## 感情生活

[鄭雪兒於](../Page/鄭雪兒.md "wikilink")2010年與[新加坡GHM酒店集團大股東](../Page/新加坡.md "wikilink")
Zecha 的兒子 Ajai Zecha 結婚\[3\]\[4\]。其丈夫是英籍人士。

## 音樂作品

  - 2000年5月：[国语专辑](../Page/国语.md "wikilink")《别装傻》

## 電視劇

  - 2000年：香港[無綫電視](../Page/無綫電視.md "wikilink")《[創世紀II之天地有情](../Page/創世紀_\(電視劇\).md "wikilink")》
    飾 秦淑芬
  - 2001年：香港[無綫電視](../Page/無綫電視.md "wikilink")《[尋秦記](../Page/尋秦記_\(電視劇\).md "wikilink")》
    飾 趙倩
  - 2002年：台灣[華視](../Page/華視.md "wikilink")《[流星花園2](../Page/流星花園_\(台灣電視劇\).md "wikilink")》
    飾 葉莎\[5\]
  - 2002年：新加坡[新传媒电视8频道](../Page/新传媒电视8频道.md "wikilink")《星梦情真》
  - 2003年：新加坡[新传媒电视8频道](../Page/新传媒电视8频道.md "wikilink")《[我家四个宝](../Page/我家四个宝.md "wikilink")》
  - 2004年：新加坡[新传媒电视8频道](../Page/新传媒电视8频道.md "wikilink")《[非一般妈妈](../Page/非一般妈妈.md "wikilink")》
  - 2005年：《[天外飞仙](../Page/天外飞仙.md "wikilink")》飾 馬娜娜
  - 2005年：中國大陸[中央电视台](../Page/中央电视台.md "wikilink")《[天空之城](../Page/天空之城_\(台灣電視劇\).md "wikilink")》
  - 2005年：中國大陸[中央电视台](../Page/中央电视台.md "wikilink")《[男男女女](../Page/男男女女.md "wikilink")》
  - 2006年：新加坡[新传媒电视8频道](../Page/新传媒电视8频道.md "wikilink")《爱情零度C》
  - 2018年：香港[邵氏兄弟](../Page/邵氏兄弟.md "wikilink")《[飛虎之潛行極戰](../Page/飛虎之潛行極戰.md "wikilink")》
    飾 雪兒

## 电影

|        |                                             |          |
| ------ | ------------------------------------------- | -------- |
| **首播** | **片名**                                      | **角色**   |
| 1999年  | 《[爆裂刑警](../Page/爆裂刑警.md "wikilink")》        | 阿Yen     |
| 2000年  | 《[神偷次世代](../Page/神偷次世代.md "wikilink")》      | Michelle |
| 2004年  | 《[追蹤眼前人](../Page/追蹤眼前人.md "wikilink")》      |          |
| 2007年  | 《[莫失莫忘](../Page/莫失莫忘.md "wikilink")》        |          |
| 2017年  | 《[殺破狼·貪狼](../Page/貪狼_\(電影\).md "wikilink")》 |          |
| 待定     | 《[尋秦記](../Page/尋秦記_\(電影\).md "wikilink")》   | 趙倩       |

## 广告

  - 台湾--【Miller Beer】、【光陽機車】、【和信电讯】、【水平衡护肤品】、【拍立得】
  - 香港--【One 2 Free电信公司】、【Sarelle手表】、【苗侨伟眼镜店】、【XS服饰】、【曼秀雷敦香体止汗露】
  - 新加坡--【Slimming Sanctuary瘦身中心】
  - 中国大陸--【丹芭比润发乳】、【曼秀雷敦香体止汗露】、【触觉休闲运动服饰】
  - 印尼--【Eillips洗面奶】

## 參考資料

<references/>

[Suet](../Category/鄭姓.md "wikilink")
[Z](../Category/新加坡女演員.md "wikilink")
[Z](../Category/新加坡電視演員.md "wikilink")
[Z](../Category/新加坡女歌手.md "wikilink")
[Z](../Category/前新傳媒電視藝人.md "wikilink")
[Category:前無綫電視女藝員](../Category/前無綫電視女藝員.md "wikilink")
[Category:印度裔混血兒](../Category/印度裔混血兒.md "wikilink")
[Category:华裔混血儿](../Category/华裔混血儿.md "wikilink")
[Z](../Category/印度裔新加坡人.md "wikilink")
[Z](../Category/南洋理工大學校友.md "wikilink")

1.
2.
3.
4.
5.