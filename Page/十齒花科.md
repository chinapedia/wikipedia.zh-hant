**十齿花科**（学名：Dipentodontaceae），又名**十萼花科**，只有2[属十餘](../Page/属.md "wikilink")[种](../Page/种.md "wikilink")，，只生长在[中国西南的](../Page/中国.md "wikilink")[广西](../Page/广西.md "wikilink")、[云南](../Page/云南.md "wikilink")、[贵州和相邻的](../Page/贵州.md "wikilink")[缅甸北部及](../Page/缅甸.md "wikilink")[印度东北](../Page/印度.md "wikilink")。

本科[植物是](../Page/植物.md "wikilink")[乔木](../Page/乔木.md "wikilink")，高达11[米](../Page/米.md "wikilink")；单[叶互生](../Page/叶.md "wikilink")，有托叶；[花小](../Page/花.md "wikilink")，[白色](../Page/白色.md "wikilink")，[花瓣](../Page/花瓣.md "wikilink")5-7，花盘与[花被贴合成杯状](../Page/花被.md "wikilink")；[果实为](../Page/果实.md "wikilink")[蒴果](../Page/蒴果.md "wikilink")，革质，被灰棕色长柔毛，内含[黑色](../Page/黑色.md "wikilink")[种子](../Page/种子.md "wikilink")。

1981年的[克朗奎斯特分类法将其列在](../Page/克朗奎斯特分类法.md "wikilink")[檀香目中](../Page/檀香目.md "wikilink")，1998年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
分类法和](../Page/APG_分类法.md "wikilink")2003年经过修订的[APG II
分类法尚无法确定其分类](../Page/APG_II_分类法.md "wikilink")。

## 外部链接

  - 在[L. Watson和M.J. Dallwitz
    (1992年)《有花植物分科》](https://web.archive.org/web/20070103200438/http://delta-intkey.com/angio/)中的[十齿花科](http://delta-intkey.com/angio/www/dipentod.htm)
  - [APG网站中的十齿花科](http://www.mobot.org/MOBOT/Research/APWeb/orders/huertealesweb.htm#Dipentodontaceae)
  - [NCBI中的十齿花科](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?lin=s&p=has_linkout&id=179841)

[\*](../Category/十齿花科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")