**李多熙**（，，），[韓國](../Page/韓國.md "wikilink")[模特兒](../Page/模特兒.md "wikilink")、[女演員](../Page/女演員.md "wikilink")
\[1\]\[2\]。2016年與華誼兄弟 (H.BROTHERS)簽約 \[3\]

## 演出

### 電視劇

| |年份                                 | 電視台                                                          | 劇名                                                 | 角色           | 備註 |
| ----------------------------------- | ------------------------------------------------------------ | -------------------------------------------------- | ------------ | -- |
| 2003年                               | [SBS](../Page/SBS_\(韓國\).md "wikilink")                      | [千年之愛](../Page/千年之愛.md "wikilink")                 | \-           |    |
| 2004年                               | SBS                                                          | [走向暴風](../Page/走向暴風.md "wikilink")                 | \-           |    |
| 2005年                               | [MBC](../Page/文化廣播_\(韓國\).md "wikilink")                     | [悲傷戀歌](../Page/悲傷戀歌.md "wikilink")                 | 江信熙          |    |
| 2007年                               | MBC                                                          | [太王四神記](../Page/太王四神記.md "wikilink")               | 閣丹           |    |
| MBC                                 | [Air City](../Page/空港城市.md "wikilink")                       | 韓伊京                                                |              |    |
| 2008年                               | MBC                                                          | [可可島的秘密](../Page/可可島的秘密.md "wikilink")             | 李多熙          |    |
| 2011年                               | MBC                                                          | [Royal Family](../Page/Royal_Family.md "wikilink") | 李有善          |    |
| [tvN](../Page/tvN.md "wikilink")    | [Birdie Buddy](../Page/Birdie_Buddy.md "wikilink")           | 閔惠玲                                                |              |    |
| tvN                                 | [需要浪漫](../Page/需要浪漫.md "wikilink")                           |                                                    | 特別出演         |    |
| [TV朝鮮](../Page/TV朝鮮.md "wikilink")  | [拯救高奉實歐巴桑](../Page/拯救高奉實歐巴桑.md "wikilink")                   |                                                    | 特別出演         |    |
| 2012年                               | SBS                                                          | [我人生的甘霖](../Page/我人生的甘霖.md "wikilink")             | **韓甘霖**      |    |
| 2013年                               | SBS                                                          | [聽見你的聲音](../Page/聽見你的聲音.md "wikilink")             | 黃佳弦/徐度妍\[4\] |    |
| [KBS](../Page/韓國放送公社.md "wikilink") | [秘密](../Page/秘密_\(2013年電視劇\).md "wikilink")                  | 申世妍\[5\]\[6\]                                      |              |    |
| 2014年                               | KBS                                                          | [Big Man](../Page/Big_Man.md "wikilink")           | **蘇美羅**\[7\] |    |
| 2015年                               | SBS                                                          | [Mrs. Cop](../Page/Mrs._Cop.md "wikilink")         | 閔度英 \[8\]    |    |
| 2018年                               | KBS                                                          | [推理的女王2](../Page/推理的女王2.md "wikilink")             | 鄭熙妍/徐賢秀\[9\] |    |
| [JTBC](../Page/JTBC.md "wikilink")  | [Beauty Inside](../Page/Beauty_Inside_\(電視劇\).md "wikilink") | 姜社羅                                                |              |    |

### 電影

| 年份    | style= align="center"| 作品        | 角色 | style= align="center"|備註 |
| ----- | -------------------------------- | -- | ------------------------ |
| 2008年 | 《黑心母女》                           | \- |                          |
| 2010年 | 《合聲                              | \- |                          |
| 2015年 | 《[五月](../Page/五月.md "wikilink")》 | \- |                          |

### 綜藝節目

| 年份         | style= align="center"| 電視台                                                  | style= align="center"| 節目                     | style= align="center"|備註 |
| ---------- | --------------------------------------------------------------------------- | --------------------------------------------- | ------------------------ |
| 2015年      | MBC                                                                         | 《[真正的男人](../Page/真正的男人.md "wikilink")》女軍特輯2   | \[10\]                   |
| 2017-2018年 | SBS                                                                         | 《[叢林的法則](../Page/叢林的法則.md "wikilink")》293-301 | 庫克群島篇\[11\]              |
| 2018年      | JTBC                                                                        | 《[認識的哥哥](../Page/認識的哥哥.md "wikilink")》第112回   |                          |
| SBS        | 《[Running Man](../Page/Running_Man.md "wikilink")》第388-389集                 | 夥伴競賽\[12\]                                    |                          |
| SBS        | 《[Running Man](../Page/Running_Man.md "wikilink")》第393至396集、第398集、第399至400集 | 固定出演嘉賓                                        |                          |
| SBS        | 《[叢林的法則](../Page/叢林的法則.md "wikilink")》                                      | 沙巴篇 \[13\]                                    |                          |
|            |                                                                             |                                               |                          |

### MV

| style= align="center"| 年份 | style= align="center"| 歌手                | 歌曲          | 備註                                      |
| ------------------------- | ---------------------------------------- | ----------- | --------------------------------------- |
| 2007年                     | Tim                                      | 《因為愛》       |                                         |
| 2012年                     | [C-CLOWN](../Page/C-CLOWN.md "wikilink") | 《漸行漸遠》      | 與[權賢尚](../Page/權賢尚.md "wikilink")       |
| 2013年                     | [Davichi](../Page/Davichi.md "wikilink") | 《因為今天特別地想你》 | 與[吳知恩](../Page/吳知恩.md "wikilink")       |
| 2014年                     | [Davichi](../Page/Davichi.md "wikilink") | 《胳膊枕》       | 與[孫浩俊](../Page/孫浩俊.md "wikilink")\[14\] |
| 2015年                     | [Tei](../Page/Tei.md "wikilink")         | 《在想念的日子裡》   | \[15\]                                  |

## 獎項

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>名稱</p></th>
<th><p>獎項</p></th>
<th><p>作品</p></th>
<th><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2013年</p></td>
<td><p><a href="../Page/SBS演技大賞.md" title="wikilink">SBS演技大賞</a></p></td>
<td><p>新星獎</p></td>
<td><p><a href="../Page/聽見你的聲音.md" title="wikilink">聽見你的聲音</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/KBS演技大賞.md" title="wikilink">KBS演技大賞</a></p></td>
<td><p>女配角獎</p></td>
<td><p><a href="../Page/秘密_(2013年電視劇).md" title="wikilink">秘密</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2014年</p></td>
<td><p><a href="../Page/KBS演技大賞.md" title="wikilink">KBS演技大賞</a></p></td>
<td><p>女子人氣賞</p></td>
<td><p><a href="../Page/Big_Man.md" title="wikilink">Big Man</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2015年</p></td>
<td><p><a href="../Page/SBS演技大賞.md" title="wikilink">SBS演技大賞</a></p></td>
<td><p>迷你電視劇 女子特別演技賞</p></td>
<td><p><a href="../Page/Mrs._Cop.md" title="wikilink">Mrs. Cop</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參考資料

## 外部連結

  - [李多熙官方網站](http://huayibrothersent.com/portfolio_page/lee-da-hee/)

  -
  - [EPG](https://web.archive.org/web/20071010143514/http://epg.epg.co.kr/star/profile/index.asp?actor_id=7788)


[Category:韓國電視演員](../Category/韓國電視演員.md "wikilink")
[Category:世宗大學校友](../Category/世宗大學校友.md "wikilink")
[L](../Category/京畿道出身人物.md "wikilink")
[L](../Category/龍仁市出身人物.md "wikilink")
[Category:李姓](../Category/李姓.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14.
15.