**Netscape
Browser**是Netscape[網頁瀏覽器的第](../Page/網頁瀏覽器.md "wikilink")8版，改寫自[Mozilla基金會的](../Page/Mozilla基金會.md "wikilink")[Mozilla
Firefox瀏覽器](../Page/Mozilla_Firefox.md "wikilink")，不同於以往導航者（Navigator）與郵件軟體（Netscape
Mail &
Newsgroups）一起推出的套裝形式。原本的HTML編輯器也被獨立出來，成為由[Linspire公司研發的](../Page/Linspire.md "wikilink")[Nvu](../Page/Nvu.md "wikilink")。

該軟體僅有[Windows](../Page/Microsoft_Windows.md "wikilink")[作業系統執行的版本](../Page/作業系統.md "wikilink")。由[美國線上](../Page/美國線上.md "wikilink")（AOL）發行，開發方面則委託[加拿大公司Mercurial](../Page/加拿大.md "wikilink")
Communications負責。該款瀏覽器是自[Netscape結束瀏覽器開發業務後](../Page/Netscape.md "wikilink")，首款以Netscape名義推出的瀏覽器，也是Netscape瀏覽器系列的延續。

特徵是並內建了兩種網頁[排版引擎](../Page/排版引擎.md "wikilink")，包含Firefox原本的[Gecko排版引擎](../Page/Gecko.md "wikilink")，與微軟[Internet
Explorer的](../Page/Internet_Explorer.md "wikilink")[Trident
（MSHTML）排版引擎](../Page/Trident.md "wikilink")。預設有兩種佈景主題，其中一種不跟隨微軟介面稱為Fusion的主題，或是原本Netscape使用的傳統主題設計Winscape。正式版於2005年3月19日推出，並且在發佈的同日旋即推出更新版。

## 版本歷史

| 版號                  | 發布日期        | 基於Mozilla Firefox版本 | 說明                     |
| ------------------- | ----------- | ------------------- | ---------------------- |
| 0.5.6+              | 2004年11月30日 | 0.9.3               |                        |
| 0.6.4               | 2005年1月7日   | 1.0                 |                        |
| 0.9.4（8.0 pre-beta） | 2005年2月17日  | 1.0                 |                        |
| 0.9.5（8.0 pre-beta） | 2005年2月23日  | 1.0                 |                        |
| 0.9.6（8.0 beta）     | 2005年3月3日   | 1.0                 |                        |
| 8.0                 | 2005年5月19日  | 1.0.3               | 正式上市                   |
| 8.0.1               | 2005年5月19日  | 1.0.4               | 隨Firefox安全性修正更新        |
| 8.0.2               | 2005年6月17日  | 1.0.4               | 修正IE的XML相關錯誤           |
| 8.0.3.1             | 2005年7月25日  | 1.0.6               | 隨Firefox安全性修正更新        |
| 8.0.3.3             | 2005年8月8日   | 1.0.6               | 修正左鍵無法正常下載等臭蟲          |
| 8.0.4               | 2005年10月19日 | 1.0.7               | 隨Firefox安全性修正更新        |
| 8.1.2               | 2006年9月27日  | 1.0.8               | 強化RSS功能                |
| 8.1.3               | 2007年4月2日   | 1.0.8               | 隨Firefox1.5.0.8安全性修正更新 |

## 中文化

Netscape Browser並未有中文化的版本或是語言套件。

## 外部連結

  - [下載各舊版本的Netscape瀏覽器和網路套件](http://sillydog.org/narchive/full123.php)

## 相關文章

  - [Netscape](../Page/Netscape.md "wikilink")
  - [網景 (瀏覽器)](../Page/網景_\(瀏覽器\).md "wikilink")
  - [Mozilla Firefox](../Page/Mozilla_Firefox.md "wikilink")
  - [网页浏览器列表](../Page/网页浏览器列表.md "wikilink")
  - [网页浏览器比较](../Page/网页浏览器比较.md "wikilink")

[Category:基於Mozilla
Firefox的網頁瀏覽器](../Category/基於Mozilla_Firefox的網頁瀏覽器.md "wikilink")
[Category:Netscape](../Category/Netscape.md "wikilink")
[Category:2005年軟體](../Category/2005年軟體.md "wikilink")
[Category:網頁瀏覽器](../Category/網頁瀏覽器.md "wikilink")