[Average_earnings_of_workers_by_education_and_sex_-_2006.png](https://zh.wikipedia.org/wiki/File:Average_earnings_of_workers_by_education_and_sex_-_2006.png "fig:Average_earnings_of_workers_by_education_and_sex_-_2006.png")
**玻璃天花板**（）是指在[公司](../Page/公司.md "wikilink")、[企业和](../Page/企业.md "wikilink")[机关](../Page/机关.md "wikilink")、[团体中对某些群体](../Page/社會團體.md "wikilink")（如[女性](../Page/女性.md "wikilink")、[少数族裔](../Page/少数族裔.md "wikilink")）晋升到高级职位或決策層的潜在限制或障碍。它指正如[玻璃一样](../Page/玻璃.md "wikilink")，这个障碍虽然不会明文规定，但却是实实在在的存在。

另外，也有**玻璃地板**（glass
floor）一词，是指某些群体在地位降低过程中的障碍。比如因为对女性的[刻板印象](../Page/刻板印象.md "wikilink")，在减少她们相对男性升任公司[总裁及高階主管机会的同时](../Page/总裁.md "wikilink")，也减少了她们[失業](../Page/失業.md "wikilink")、[过度劳动](../Page/过度劳动.md "wikilink")、落入地位低下的[矿工](../Page/矿工.md "wikilink")、[囚犯等处境的几率](../Page/囚犯.md "wikilink")。一些女權主義者認為，男性缺乏玻璃地板是推動女權主義及移除玻璃天花板的障礙，多數男性無法達到玻璃天花板的高度、但明確感受到缺乏玻璃地板保護，因此會把女權主義視為得了便宜還賣乖。

一些人認為玻璃天花板在某種程度上只是假像，男性能力的個體差異比女性大很多，因此頂尖者及失敗者會有高比例的男性。

玻璃天花板難以移除的原因還有：

  - 「女王蜂效應」，女性也受不了女性主管，她們給女性部屬的壓力比男主管高，嚴重者會造成不孕
  - 「[婚姻斜坡](../Page/婚姻斜坡.md "wikilink")」，多數女性對丈夫的要求仍高、年輕及中年男性擔任全職家庭主夫的社會觀感仍差，面臨失業及低薪、男性的承受力就是比較女性低很多，這使得政經高層基於社會責任感（避免單身率太高及[生育率太低](../Page/生育率.md "wikilink")、也是為了減少失敗者的總數；既然女性對男性的職場失敗者很不友善，那職場自然會打壓女性），而將工作及薪資留給男性；許多女權人士輕忽低[生育率及高男性無後率的危害](../Page/生育率.md "wikilink")，使得女權的正當性遭到質疑。

## 玻璃天花板指數

經濟學人針對[經濟合作暨發展組織](../Page/经济合作与发展组织.md "wikilink")([OECD](../Page/经济合作与发展组织.md "wikilink"))成員國家，調查公佈了各國「透明天花板指數」(英語：Glass-Ceiling
index)。指出女性在各國工作與生育生涯中，受到無形屏障或福利的狀況\[1\]。

### 參考指標：

  - 男女的高等教育受教比例
  - 女性勞動力投入職場程度
  - 男女的薪資差距比例
  - 一般育兒成本
  - 生育福利：支全薪的母親育嬰假
  - 生育福利：支全薪的父親陪產假
  - 女性申請商業管理學位的比例
  - 女性升遷至資深經理的比例
  - 女性進入董事會的比例
  - 女性成為國會議員的比例

其中，經濟學人數據指出，最適合女人工作的國家前三名為：挪威、瑞士、芬蘭\[2\]。

## 變種及相關術語

  - [賽璐珞天花](../Page/賽璐珞天花.md "wikilink")，指女性佔[荷里活高級職位的少數](../Page/荷里活.md "wikilink")，由Lauzen（2002年）和其他人證明。
  - 黃銅天花板：特指[军队](../Page/军队.md "wikilink")、[警察部門中的玻璃天花板](../Page/警察.md "wikilink")。

## 相關條目

  - [同酬日](../Page/同酬日.md "wikilink")

## 參考資料

<references />

[Category:潛規則](../Category/潛規則.md "wikilink")
[Category:性別研究](../Category/性別研究.md "wikilink")
[Category:经济问题](../Category/经济问题.md "wikilink")
[Category:種族主義](../Category/種族主義.md "wikilink")
[Category:僱傭](../Category/僱傭.md "wikilink")

1.
2.