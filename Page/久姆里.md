**久姆里**（[亚美尼亚语](../Page/亚美尼亚语.md "wikilink")：**Գյումրի**）位于[亚美尼亚西北部](../Page/亚美尼亚.md "wikilink")，距离首都[埃里温](../Page/埃里温.md "wikilink")120公里，是[希拉克省的首府和最大城市](../Page/希拉克省.md "wikilink")，人口150,917人，是全国第二大城市。

城市的名字在其历史上变换了多次。最初称为Kumayri或Gyumri（Կումայրի），然后改为“亚历山德罗波尔”（Ալեքսանդրապոլ，1840年－1924年），再后来是列宁纳坎（Լենինական，1924年－1990年），最后是久姆里。

## 历史

最早到达久姆里的人据说是公元前五世纪的[希腊人](../Page/希腊.md "wikilink")。但也有另一种说法认为久姆里是由[辛梅里安人建立的](../Page/辛梅里安人.md "wikilink")，根据是辛梅里安人在公元前720年占领了此地。在随后的近两千年内，久姆里只有零星的移民，直到1837年此地建立起了一个重要的[俄罗斯要塞](../Page/俄罗斯.md "wikilink")。

1840年，久姆里围绕着要塞迅速成长，并被命名为“亚历山德罗波尔”。这个名字是为了向沙皇[尼古拉一世的妻子](../Page/尼古拉一世_\(俄国\).md "wikilink")[普鲁士的夏洛特公主表示敬意](../Page/亚历山德拉·费奥多罗芙娜皇后_\(尼古拉一世\).md "wikilink")，因为她在皈依了[东正教后改名为](../Page/东正教.md "wikilink")“亚历山德拉·费奥多罗芙娜”。久姆里成为[俄罗斯帝国军队在](../Page/俄罗斯帝国.md "wikilink")[南高加索地区的重要据点并修建了许多军营](../Page/南高加索.md "wikilink")。

当时，亚历山德罗波尔的地位被认为比[埃里温重要](../Page/埃里温.md "wikilink")，而埃里温只是一个看起来无关紧要的小村庄。在[俄罗斯-土耳其战争中](../Page/俄土战争_\(1828年-1829年\).md "wikilink")，大量[亚美尼亚人从](../Page/亚美尼亚人.md "wikilink")[卡尔斯](../Page/卡尔斯.md "wikilink")、[埃尔祖鲁姆等](../Page/埃尔祖鲁姆.md "wikilink")[奥斯曼帝国领土内的城市来到亚历山德罗波尔](../Page/奥斯曼帝国.md "wikilink")。埃里温仍然无足轻重，直到被宣布为1918年独立的[亚美尼亚共和国和](../Page/亚美尼亚共和国.md "wikilink")1920年[亚美尼亚苏维埃社会主义共和国的首都](../Page/亚美尼亚苏维埃社会主义共和国.md "wikilink")，这一状况才得到改变。

[Gyumrigeneralview.jpg](https://zh.wikipedia.org/wiki/File:Gyumrigeneralview.jpg "fig:Gyumrigeneralview.jpg")
[Surb_Amenaprkitch_pre88.jpeg](https://zh.wikipedia.org/wiki/File:Surb_Amenaprkitch_pre88.jpeg "fig:Surb_Amenaprkitch_pre88.jpeg")
1924年，为了纪念已故的[苏联领导人](../Page/苏联.md "wikilink")[弗拉基米尔·列宁](../Page/列宁.md "wikilink")，城市名字改为了“列宁纳坎”。列宁纳坎是亚美尼亚苏维埃社会主义共和国的主要工业中心，也是仅次于埃里温的全国第二大城市。在1988年影响全国大部分地区的[斯皮塔克地震中](../Page/亚美尼亚大地震.md "wikilink")，列宁纳坎受到了严重的损毁。目前使用的这个城市名字是在1990年[苏联解体后采用的](../Page/苏联解体.md "wikilink")。如今，久姆里仍然是亚美尼亚共和国的第二大城市。

## 宗教建筑

在久姆里，共有5座[教堂](../Page/教堂.md "wikilink")、1座[女子修道院和](../Page/女子修道院.md "wikilink")1座俄罗斯[礼拜堂](../Page/礼拜堂.md "wikilink")。其中最重要、最具有历史意义的是神圣救世主教堂（Church
of the Holy
Saviour）。教堂始建于1859年，于1873年完工。但在1988年[斯皮塔克地震中](../Page/亚美尼亚大地震.md "wikilink")，这座教堂受到了严重的破坏，目前正在重建过程中。

## 友好城市

  - [Flag_of_Bulgaria.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Bulgaria.svg "fig:Flag_of_Bulgaria.svg")
    [保加利亚](../Page/保加利亚.md "wikilink")[普罗夫迪夫](../Page/普罗夫迪夫.md "wikilink")

  - [亞歷山大](../Page/亞歷山大_\(維珍尼亞州\).md "wikilink")

  - [亞歷山大港](../Page/亞歷山大港.md "wikilink")

  - [塞薩洛尼基](../Page/塞薩洛尼基.md "wikilink")

  - [奧薩斯庫](../Page/奧薩斯庫.md "wikilink")

  - [克雷泰伊](../Page/克雷泰伊.md "wikilink")

  - [莫茲多克](../Page/莫茲多克.md "wikilink")

  - [納爾德奧](../Page/納爾德奧.md "wikilink")

  -
  - [庫塔伊西](../Page/庫塔伊西.md "wikilink")

  - [烏里揚諾夫斯克](../Page/烏里揚諾夫斯克.md "wikilink")

  - [塔爾圖](../Page/塔爾圖.md "wikilink")

  - [皮特什蒂](../Page/皮特什蒂.md "wikilink")

  - [西安](../Page/西安.md "wikilink")

  - [科爾多瓦](../Page/科爾多瓦_\(阿根廷\).md "wikilink")

  - [比亞維斯托克](../Page/比亞維斯托克.md "wikilink")

  - [恰德爾倫加](../Page/恰德爾倫加.md "wikilink")

  - [多莫傑多沃](../Page/多莫傑多沃.md "wikilink")

  - [格倫代爾](../Page/格倫代爾_\(加利福尼亞州\).md "wikilink")

## 外部链接

  - [www.gyumricity.am](http://www.gyumricity.am/)

[Category:亚美尼亚城市](../Category/亚美尼亚城市.md "wikilink")