**奧斯汀县**（**Austin County,
Texas**）是位於[美國](../Page/美國.md "wikilink")[德克薩斯州東南部的一個縣](../Page/德克薩斯州.md "wikilink")。面積1,700平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口23,590人。縣治[貝爾維爾](../Page/贝尔维尔_\(得克萨斯州\).md "wikilink")（Bellville）。注意[同名的城市位於](../Page/奧斯汀_\(德克薩斯州\).md "wikilink")[特拉維斯縣](../Page/特拉維斯縣_\(德克薩斯州\).md "wikilink")。

成立於1836年3月17日，縣政府成立於1837年，是最早的二十三個縣之一。縣名以有「德克薩斯之父」之稱的[史提芬·F·奧斯汀](../Page/史蒂芬·奧斯丁.md "wikilink")（Stephen
Fuller Austin）命名。\[1\]。

## 参考文献

[A](../Category/得克萨斯州行政区划.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.