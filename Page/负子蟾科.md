**负子蟾**是多種分布於[南美洲和](../Page/南美洲.md "wikilink")[非洲的原始蛙類](../Page/非洲.md "wikilink")。

完全水棲性。背、腹部平坦、身體流線形。後足[蹼發達](../Page/蹼.md "wikilink")。皮膚光滑。有[側線系統](../Page/側線.md "wikilink")。頭小、無[舌](../Page/舌.md "wikilink")。[眼瞼無法自由移動](../Page/眼瞼.md "wikilink")。後足趾上有爪。

## 分類

  - Dactylethrinae <small>Hogg, 1839</small>
      - [膜蟾属](../Page/膜蟾属.md "wikilink") *Hymenochirus*
        <small>Boulenger, 1896</small>
      - [睑蟾属](../Page/睑蟾属.md "wikilink") *Pseudhymenochirus*
        <small>Chabanaud, 1920</small>
      - *Silurana* <small>Gray, 1864</small>
      - [爪蟾属](../Page/爪蟾属.md "wikilink") *Xenopus* <small>Wagler,
        1827</small>
  - 负子蟾亚科 Pipinae <small>Gray, 1825</small>
      - [负子蟾属](../Page/负子蟾属.md "wikilink") *Pipa* <small>Laurenti,
        1768</small>

## 參考

  -
[Category:无尾目](../Category/无尾目.md "wikilink")
[Category:中蛙亞目](../Category/中蛙亞目.md "wikilink")
[\*](../Category/負子蟾科.md "wikilink")