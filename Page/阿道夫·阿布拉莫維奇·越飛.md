**阿道夫·阿布拉莫维奇·越飛**\[1\]（，[拉丁化](../Page/拉丁化.md "wikilink")：Adolf
Abramovich
Joffe；）是[俄國](../Page/俄國.md "wikilink")[革命家](../Page/革命家.md "wikilink")、[布爾什維克](../Page/布爾什維克.md "wikilink")[政治家](../Page/政治家.md "wikilink")、[蘇聯](../Page/蘇聯.md "wikilink")[外交家](../Page/外交.md "wikilink")，曾任蘇聯特使與[中國國民黨創黨領袖](../Page/中國國民黨.md "wikilink")[孫中山會談](../Page/孫中山.md "wikilink")。

## 生平

[Joffe.jpg](https://zh.wikipedia.org/wiki/File:Joffe.jpg "fig:Joffe.jpg")

### 求學時熱衷政治

阿道夫出生於俄羅斯[辛菲羅波爾一個家境](../Page/辛菲羅波爾.md "wikilink")[富裕的](../Page/富裕.md "wikilink")[犹太](../Page/犹太.md "wikilink")[家庭](../Page/家庭.md "wikilink")，[中學時即熱中參加](../Page/中學.md "wikilink")[政治活動](../Page/政治.md "wikilink")。1903年，加入[俄國社會民主工黨](../Page/俄國社會民主工黨.md "wikilink")，之後在俄國各地活動。1906年被迫[流亡](../Page/流亡.md "wikilink")，先搬到[德國](../Page/德國.md "wikilink")[柏林](../Page/柏林.md "wikilink")，之後被逐，轉至[維也納](../Page/維也納.md "wikilink")。於1908年至1912年於維也納學習[醫學](../Page/醫學.md "wikilink")，同時與[托洛茨基一同編輯](../Page/托洛茨基.md "wikilink")《[真理報](../Page/真理報.md "wikilink")》。1912年，29歲時返回俄國，但馬上被捕並被[流放到](../Page/流放.md "wikilink")[西伯利亞](../Page/西伯利亞.md "wikilink")。

### 布爾什維克

1917年[二月革命後](../Page/二月革命.md "wikilink")，回歐洲，於第六屆[布爾什維克大會被選為中央委員](../Page/布爾什維克.md "wikilink")，與托洛茨基合編《[前進報](../Page/前進報.md "wikilink")》。同年開始[軍事領導工作](../Page/軍事.md "wikilink")。[十月革命後](../Page/十月革命.md "wikilink")，從事外交工作，為外交部副部長，領導參與對[德國的停火談判](../Page/德國.md "wikilink")。

1922年7月，越飛被任為苏联駐華全權代表。1922年8月，越飞抵達[北京後](../Page/北京.md "wikilink")，与當時[中華民國北洋政府谈判](../Page/中華民國.md "wikilink")，对手为剛上任不久的外交总长[顾维钧](../Page/顾维钧.md "wikilink")。越飞想先达成建交目的，顾维钧仍要求苏联先把红军从[外蒙撤退](../Page/外蒙.md "wikilink")，然而谈判因此沒有結果。之後，越飛又致函當時駐在[洛阳且军事实力最强的](../Page/洛阳.md "wikilink")[吴佩孚将军](../Page/吴佩孚.md "wikilink")，希望建立合作关系，但是吴佩孚拒绝了苏联人的[游说](../Page/游说.md "wikilink")。然後，越飛又前往[上海](../Page/上海.md "wikilink")，與[孫中山會面](../Page/孫中山.md "wikilink")。與孫中山會面後一起發表“[孫文越飛聯合宣言](../Page/孫文越飛聯合宣言.md "wikilink")”（簡稱“[孫越宣言](../Page/孫越宣言.md "wikilink")”），孙中山竟然同意苏军暫時留驻外蒙，同[中國國民黨開展了與](../Page/中國國民黨.md "wikilink")[蘇聯及共產黨的合作關係](../Page/蘇聯.md "wikilink")。同年，越飛再赴[日本](../Page/日本.md "wikilink")、[英國等地從事外交活動](../Page/英國.md "wikilink")。1926年返回俄羅斯。

### 自殺

1927年，托洛茨基被[史達林開除出](../Page/史達林.md "wikilink")[蘇聯共產黨](../Page/蘇聯共產黨.md "wikilink")；1927年11月16日，越飛在[莫斯科的](../Page/莫斯科.md "wikilink")[醫院內](../Page/醫院.md "wikilink")[自殺](../Page/自殺.md "wikilink")。

## 註釋

[Category:蘇聯外交官](../Category/蘇聯外交官.md "wikilink")
[Category:蘇聯駐德國大使](../Category/蘇聯駐德國大使.md "wikilink")
[Category:蘇聯駐奧地利大使](../Category/蘇聯駐奧地利大使.md "wikilink")
[Category:蘇聯共產黨人物](../Category/蘇聯共產黨人物.md "wikilink")
[Category:俄国革命家](../Category/俄国革命家.md "wikilink")
[Category:猶太政治人物](../Category/猶太政治人物.md "wikilink")
[Category:蘇聯自殺者](../Category/蘇聯自殺者.md "wikilink")
[Category:俄羅斯自殺者](../Category/俄羅斯自殺者.md "wikilink")
[Category:自殺政治人物](../Category/自殺政治人物.md "wikilink")
[Category:俄羅斯共產主義者](../Category/俄羅斯共產主義者.md "wikilink")
[Category:俄羅斯無神論者](../Category/俄羅斯無神論者.md "wikilink")
[Category:猶太無神論者](../Category/猶太無神論者.md "wikilink")
[Category:蘇聯猶太人](../Category/蘇聯猶太人.md "wikilink")
[Category:烏克蘭猶太人](../Category/烏克蘭猶太人.md "wikilink")
[Category:俄國猶太人](../Category/俄國猶太人.md "wikilink")
[Category:犹太人社会主义者](../Category/犹太人社会主义者.md "wikilink")
[Category:克里米亞人](../Category/克里米亞人.md "wikilink")
[Category:維也納大學校友](../Category/維也納大學校友.md "wikilink")
[Category:左翼反对派](../Category/左翼反对派.md "wikilink")
[Category:苏联驻日本大使](../Category/苏联驻日本大使.md "wikilink")
[Category:共产国际人物‎](../Category/共产国际人物‎.md "wikilink")

1.  根據《[世界人名翻譯大辭典](../Page/世界人名翻譯大辭典.md "wikilink")》p.1405，只有Abram
    Adolph Abramovich Joffe譯「越飛」，其他以Joffe為名的人一律譯「若費」。