**司徒**，[中国](../Page/中国.md "wikilink")、朝鮮、越南古代职官。[西周始置](../Page/西周.md "wikilink")，位次[三公](../Page/三公.md "wikilink")，与[六卿相当](../Page/六卿.md "wikilink")，与[司马](../Page/司马.md "wikilink")、[司空](../Page/司空.md "wikilink")、[司士](../Page/司士.md "wikilink")、[司寇并称](../Page/司寇.md "wikilink")“[五官](../Page/五官.md "wikilink")”，[金文多作](../Page/金文.md "wikilink")“司土”，与司马、司工（即司空）合称“[三有司](../Page/三有司.md "wikilink")”。是管理土地、人民的官，与后世的[户部尚书相当](../Page/户部尚书.md "wikilink")。[春秋时沿置](../Page/春秋.md "wikilink")。

[汉哀帝时](../Page/汉哀帝.md "wikilink")[丞相改称](../Page/丞相.md "wikilink")[大司徒](../Page/大司徒.md "wikilink")，[东汉时改称司徒](../Page/东汉.md "wikilink")，成为三公之一。

## 历代司徒

### 上古

  - 舜

<!-- end list -->

  - [契](../Page/契.md "wikilink")

### 周

  - 魯

<!-- end list -->

  - [季孙氏](../Page/季孙氏.md "wikilink")

<!-- end list -->

  - 宋

<!-- end list -->

  - [皇父充石](../Page/皇父充石.md "wikilink")、[鳞矔](../Page/鳞矔.md "wikilink")、[华喜](../Page/华喜.md "wikilink")、[边卬](../Page/边卬.md "wikilink")、[皇怀](../Page/皇怀.md "wikilink")

<!-- end list -->

  - 郑

<!-- end list -->

  - [子孔](../Page/子孔.md "wikilink")

<!-- end list -->

  - 陈

<!-- end list -->

  - [辕颇](../Page/辕颇.md "wikilink")

### 汉

  - [西漢](../Page/西漢.md "wikilink")

<!-- end list -->

  - [新朝](../Page/新朝.md "wikilink")

<!-- end list -->

  - [王寻](../Page/王寻.md "wikilink") 9-23
  - [张邯](../Page/张邯.md "wikilink") 23

<!-- end list -->

  - [更始](../Page/更始.md "wikilink")

<!-- end list -->

  - [刘縯](../Page/刘縯.md "wikilink") 23
  - [刘赐](../Page/刘赐_\(东汉\).md "wikilink") 23

<!-- end list -->

  - [东漢](../Page/东漢.md "wikilink")

### 三国

### 晋

### 十六国

  - 成汉

[李云](../Page/李雲_\(成漢\).md "wikilink")
[王达](../Page/王达_\(成汉\).md "wikilink")
[何点](../Page/何点.md "wikilink") [王瓌](../Page/王瓌.md "wikilink")

  - 后赵

[裴宪](../Page/裴宪.md "wikilink") [石韬](../Page/石韜_\(後趙\).md "wikilink")
[申钟](../Page/申钟.md "wikilink")

  - 冉魏

[郎闿](../Page/郎闿.md "wikilink") [刘茂](../Page/刘茂_\(十六国\).md "wikilink")

  - 前燕

[慕容评](../Page/慕容评.md "wikilink")

  - 后燕

[慕容德](../Page/慕容德.md "wikilink") [慕容元](../Page/慕容元.md "wikilink")

  - 南燕

[慕容钟](../Page/慕容钟.md "wikilink") [鞠仲](../Page/鞠仲.md "wikilink")
[慕容惠](../Page/慕容惠.md "wikilink")

  - 前秦

[王永](../Page/王永_\(前秦\).md "wikilink") [苻同成](../Page/苻同成.md "wikilink")
[苻广](../Page/苻广.md "wikilink")

  - 夏

[赫连定](../Page/赫连定.md "wikilink")

  - 后凉

[吕弘](../Page/吕弘.md "wikilink")

### 南北朝

  - 宋

[刘义真](../Page/刘义真.md "wikilink") [徐羡之](../Page/徐羡之.md "wikilink")
[王弘](../Page/王弘_\(刘宋\).md "wikilink")
[刘义康](../Page/刘义康.md "wikilink")
[刘义恭](../Page/刘义恭.md "wikilink")
[刘义宣](../Page/刘义宣.md "wikilink")
[刘子鸾](../Page/刘子鸾.md "wikilink")
[刘子尚](../Page/刘子尚.md "wikilink")
[刘休仁](../Page/刘休仁.md "wikilink")
[袁粲](../Page/袁粲.md "wikilink") [刘燮](../Page/刘燮.md "wikilink")

  - 齐

[褚渊](../Page/褚渊.md "wikilink") [萧子良](../Page/萧子良.md "wikilink")
[萧子卿](../Page/萧子卿.md "wikilink") [萧锵](../Page/萧锵.md "wikilink")
[萧宝义](../Page/萧宝义.md "wikilink")

  - 梁

[谢朏](../Page/谢朏.md "wikilink") [萧伟](../Page/萧伟.md "wikilink")
[萧宏](../Page/萧宏.md "wikilink") [萧纶](../Page/萧纶.md "wikilink")
[萧绎](../Page/萧绎.md "wikilink") [王僧辩](../Page/王僧辩.md "wikilink")
[陆法和](../Page/陆法和.md "wikilink") [萧勃](../Page/萧勃.md "wikilink")
[陈霸先](../Page/陈霸先.md "wikilink")

  - 陈

[陈頊](../Page/陈頊.md "wikilink") [陈叔英](../Page/陈叔英.md "wikilink")

  - 北魏

[长孙嵩](../Page/长孙嵩.md "wikilink") [长孙翰](../Page/长孙翰.md "wikilink")
[崔浩](../Page/崔浩.md "wikilink") [古弼](../Page/古弼.md "wikilink")
[陆丽](../Page/陆丽.md "wikilink") [刘尼](../Page/刘尼.md "wikilink")
[拓跋目辰](../Page/拓跋目辰.md "wikilink")
[拓跋丕](../Page/拓跋丕.md "wikilink")
[陈建](../Page/陈建.md "wikilink") [元他](../Page/元他.md "wikilink")
[尉元](../Page/尉元.md "wikilink") [冯诞](../Page/冯诞.md "wikilink")
[元勰](../Page/元勰.md "wikilink") [元详](../Page/元详.md "wikilink")
[元嘉](../Page/元嘉.md "wikilink") [高肇](../Page/高肇.md "wikilink")
[元怿](../Page/元怿.md "wikilink") [元怀](../Page/元怀.md "wikilink")
[胡国珍](../Page/胡国珍.md "wikilink") [元澄](../Page/元澄.md "wikilink")
[元继](../Page/元继.md "wikilink") [崔光](../Page/崔光.md "wikilink")
[萧综](../Page/萧综.md "wikilink") [皇甫度](../Page/皇甫度.md "wikilink")
[杨椿](../Page/杨椿.md "wikilink") [元徽](../Page/元徽.md "wikilink")
[李延实](../Page/李延实.md "wikilink") [萧赞](../Page/萧赞.md "wikilink")
[长孙稚](../Page/长孙稚.md "wikilink") [元彧](../Page/元彧.md "wikilink")
[尔朱彦伯](../Page/尔朱彦伯.md "wikilink")
[元亶](../Page/元亶.md "wikilink")

  - 东魏

[高盛](../Page/高盛.md "wikilink") [高昂](../Page/高昂.md "wikilink")
[孙腾](../Page/孙腾.md "wikilink") [高隆之](../Page/高隆之.md "wikilink")
[娄昭](../Page/娄昭.md "wikilink") [侯景](../Page/侯景.md "wikilink")
[韩轨](../Page/韩轨.md "wikilink") [彭乐](../Page/彭乐.md "wikilink")

  - 北齐

[潘相乐](../Page/潘相乐.md "wikilink") [尉粲](../Page/尉粲.md "wikilink")
[高湛](../Page/高湛.md "wikilink") [段韶](../Page/段韶.md "wikilink")
[高归彦](../Page/高归彦.md "wikilink") [娄睿](../Page/娄睿.md "wikilink")
[斛律光](../Page/斛律光.md "wikilink") [高润](../Page/高润.md "wikilink")
[韩祖念](../Page/韩祖念.md "wikilink") [高俨](../Page/高俨.md "wikilink")
[高绰](../Page/高绰.md "wikilink") [高孝珩](../Page/高孝珩.md "wikilink")
[高延宗](../Page/高延宗.md "wikilink")
[高阿那肱](../Page/高阿那肱.md "wikilink")
[高普](../Page/高普.md "wikilink") [赵彦深](../Page/赵彦深.md "wikilink")
[莫多婁敬顯](../Page/莫多婁敬顯.md "wikilink")

  - 西魏

[斛斯椿](../Page/斛斯椿.md "wikilink") [元赞](../Page/元赞_\(广平公\).md "wikilink")
[万俟洛](../Page/万俟洛.md "wikilink") [李叔仁](../Page/李叔仁.md "wikilink")
[王盟](../Page/王盟.md "wikilink") [高仲密](../Page/高仲密.md "wikilink")
[元欣](../Page/元欣.md "wikilink")
[李弼](../Page/李弼_\(赵国公\).md "wikilink")

  - 北周

[李弼](../Page/李弼_\(赵国公\).md "wikilink")
[侯莫陈崇](../Page/侯莫陈崇.md "wikilink")
[杨荐](../Page/杨荐.md "wikilink") [宇文贵](../Page/宇文贵.md "wikilink")
[宇文直](../Page/宇文直.md "wikilink") [宇文亮](../Page/宇文亮.md "wikilink")
[长孙览](../Page/长孙览.md "wikilink") [于翼](../Page/于翼.md "wikilink")
[宇文椿](../Page/宇文椿.md "wikilink") [王谊](../Page/王谊.md "wikilink")

### 隋

  - [楊素](../Page/楊素.md "wikilink") 606

### 唐

  - [李世民](../Page/李世民.md "wikilink") 621-626
  - [李元吉](../Page/李元吉.md "wikilink") 626
  - [長孫無忌](../Page/長孫無忌.md "wikilink") 642-649
  - [李元景](../Page/李元景.md "wikilink") 649-653
  - [李元礼](../Page/李元礼.md "wikilink") 653-672
  - [李元轨](../Page/李元轨.md "wikilink") 683-685
  - [李元名](../Page/李元名.md "wikilink") 689-690
  - [李旦](../Page/李旦.md "wikilink") 702-703
  - [武攸暨](../Page/武攸暨.md "wikilink") 705
  - [李憲](../Page/李成器.md "wikilink") 713
  - [李成义](../Page/李成义.md "wikilink") 713-724
  - [李浚](../Page/唐肃宗.md "wikilink") 732-738
  - [李业](../Page/李业.md "wikilink") 733-734
  - [李琮](../Page/李琮_\(奉天皇帝\).md "wikilink") 736-752
  - [郭子仪](../Page/郭子仪.md "wikilink") 757-764
  - [李抱玉](../Page/李抱玉.md "wikilink") 764-765
  - [郭子仪](../Page/郭子仪.md "wikilink") 768-779
  - [李正己](../Page/李正己.md "wikilink") 779-781
  - [李晟](../Page/李晟.md "wikilink") 784-787
  - [马燧](../Page/马燧.md "wikilink") 787-795
  - [李抱真](../Page/李抱真.md "wikilink") 793
  - [杜佑](../Page/杜佑.md "wikilink") 806-812
  - [韩弘](../Page/韩弘.md "wikilink") 815-822
  - [李光颜](../Page/李光颜.md "wikilink") 824-826
  - [乌重胤](../Page/乌重胤.md "wikilink") 827
  - [王智兴](../Page/王智兴.md "wikilink") 828-829
  - [裴度](../Page/裴度.md "wikilink") 830-839
  - [李德裕](../Page/李德裕.md "wikilink") 843-844
  - [白敏中](../Page/白敏中.md "wikilink") 852-861
  - [令狐綯](../Page/令狐綯.md "wikilink") 859-860
  - [李悰](../Page/李悰.md "wikilink") 862-863
  - [李纮](../Page/李纮.md "wikilink") 864-872
  - [王鐸](../Page/王鐸_\(唐朝\).md "wikilink") 872-873，877-879，881-883
  - [韦保衡](../Page/韦保衡.md "wikilink") 873
  - [郑畋](../Page/郑畋.md "wikilink") 883
  - [萧遘](../Page/萧遘.md "wikilink") 885-887
  - [韦昭度](../Page/韦昭度.md "wikilink") 887-888，893
  - [孔纬](../Page/孔纬.md "wikilink") 889
  - [杜让能](../Page/杜让能.md "wikilink") 889-892
  - [徐彦若](../Page/徐彦若.md "wikilink") 898-899
  - [崔胤](../Page/崔胤.md "wikilink") 903-904

### 五代十国

  - 后梁

[韩建](../Page/韩建.md "wikilink") [赵光逢](../Page/赵光逢.md "wikilink")

  - 后唐

[郭承丰](../Page/郭承丰.md "wikilink")（检校） [朱洪实](../Page/朱洪实.md "wikilink")

  - 后晋

[冯道](../Page/冯道.md "wikilink")

  - 后汉

[李守贞](../Page/李守贞.md "wikilink")

  - 后周

[窦贞固](../Page/窦贞固.md "wikilink") [范质](../Page/范质.md "wikilink")

  - 前蜀

[毛文锡](../Page/毛文锡.md "wikilink") [周庠](../Page/周庠.md "wikilink")

  - 楚

[李铎](../Page/李铎.md "wikilink")

  - 南唐

[李景通](../Page/李景通.md "wikilink") [王令谋](../Page/王令谋.md "wikilink")
[宋齐丘](../Page/宋齐丘.md "wikilink") [李建勋](../Page/李建勋.md "wikilink")
[李从善](../Page/李从善.md "wikilink") [李从镒](../Page/李从镒.md "wikilink")

  - 闽

[杨沂丰](../Page/杨沂丰.md "wikilink")

  - 北汉

[赵弘](../Page/赵弘_\(北汉\).md "wikilink")

### 宋

[范质](../Page/范质.md "wikilink") [赵普](../Page/赵普.md "wikilink")
[王旦](../Page/王旦.md "wikilink") [王曾](../Page/王曾.md "wikilink")
[丁谓](../Page/丁谓.md "wikilink") [冯拯](../Page/冯拯.md "wikilink")
[王钦若](../Page/王钦若.md "wikilink") [吕夷简](../Page/吕夷简.md "wikilink")
[陈执中](../Page/陈执中.md "wikilink") [韩琦](../Page/韩琦.md "wikilink")
[文彦博](../Page/文彦博.md "wikilink") [曹佾](../Page/曹佾.md "wikilink")
[富弼](../Page/富弼.md "wikilink") [韩忠彦](../Page/韩忠彦.md "wikilink")
[王岩叟](../Page/王岩叟.md "wikilink") [许将](../Page/许将.md "wikilink")
[赵似](../Page/赵似.md "wikilink") [赵偲](../Page/赵偲.md "wikilink")
[赵宗汉](../Page/赵宗汉.md "wikilink") [吕惠卿](../Page/吕惠卿.md "wikilink")

### 辽

[划设](../Page/划设.md "wikilink") [韩德让](../Page/韩德让.md "wikilink")
[张俭](../Page/张俭.md "wikilink")

### 金

[完颜衮](../Page/完颜衮.md "wikilink") [徒单恭](../Page/徒单恭.md "wikilink")
[张通古](../Page/张通古.md "wikilink") [萧玉](../Page/萧玉.md "wikilink")
[李石](../Page/李石.md "wikilink") [徒单克宁](../Page/徒单克宁.md "wikilink")

### 元

  - [絳曲堅贊](../Page/絳曲堅贊.md "wikilink")

### 明

  - [丘吉嘉称](../Page/丘吉嘉称.md "wikilink")（[大司徒仁波切](../Page/大司徒仁波切.md "wikilink")）

## 参考文献

## 外部链接

## 参见

  - [三公](../Page/三公.md "wikilink")
  - [丞相](../Page/丞相.md "wikilink")
  - [宰相](../Page/宰相.md "wikilink")
  - [户部尚书](../Page/户部尚书.md "wikilink")

{{-}}

[司徒](../Category/司徒.md "wikilink")