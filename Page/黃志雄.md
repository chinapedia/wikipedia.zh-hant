**黃志雄**（），[中華民國](../Page/中華民國.md "wikilink")[中國國民黨](../Page/中國國民黨.md "wikilink")[新北市政治人物](../Page/新北市.md "wikilink")\[1\]，[國立華僑實驗高級中學體育班畢業](../Page/國立華僑實驗高級中學.md "wikilink")，[中國文化大學運動教練研究所碩士](../Page/中國文化大學.md "wikilink")，中國文化大學體育系國術組學士，[國立體育大學教練研究所博士班修讀中](../Page/國立體育大學.md "wikilink")，[跆拳道選手](../Page/跆拳道.md "wikilink")。代表台灣參加2000年[雪梨奧運男子組第一量級比賽](../Page/2000年夏季奧林匹克運動會.md "wikilink")，獲得銅牌。2004年參加[雅典奧運男子組第二量級](../Page/2004年夏季奧林匹克運動會.md "wikilink")（68公斤級）比賽，將第一量級禮讓給[朱木炎](../Page/朱木炎.md "wikilink")，他在決賽中以3:4敗給[伊朗隊的哈迪](../Page/伊朗.md "wikilink")，為台灣拿下該屆奧運會的第二面銀牌。

2004年，黃志雄在[第六屆立法委員選舉中](../Page/第六屆立法委員選舉.md "wikilink")，被[中國國民黨提名為不分區立法委員第三順位](../Page/中國國民黨.md "wikilink")，並當選。[2008年中華民國立法委員選舉黃志雄被](../Page/2008年中華民國立法委員選舉.md "wikilink")[中國國民黨提名參選臺北縣第五選區](../Page/中國國民黨.md "wikilink")（臺北縣[鶯歌鎮](../Page/鶯歌區.md "wikilink")、[樹林市及](../Page/樹林區.md "wikilink")[新莊市九里](../Page/新莊區.md "wikilink")），並成功由不分區轉戰區域立委，擊敗甫由台聯加入民進黨的時任立委、前樹林市長[廖本煙](../Page/廖本煙.md "wikilink")，後於[2012年中華民國立法委員選舉中遭逢相同對手](../Page/2012年中華民國立法委員選舉.md "wikilink")，不過仍當選連任，得票率甚至小幅上升。2016年挑戰四連霸失利。

## 政治

### [2008年中華民國立法委員選舉](../Page/2008年中華民國立法委員選舉.md "wikilink")

| 2008年[臺北縣第五選舉區](../Page/新北市第五選舉區.md "wikilink")[立法委員選舉結果](../Page/立法委員.md "wikilink") |
| ------------------------------------------------------------------------------------- |
| 號次                                                                                    |
| 1                                                                                     |
| 2                                                                                     |
| 3                                                                                     |
| 4                                                                                     |
| 5                                                                                     |
| **選舉人數**                                                                              |
| **投票數**                                                                               |
| **有效票**                                                                               |
| **無效票**                                                                               |
| **投票率**                                                                               |

### [2008年奧林匹克運動員委員會委員選舉](../Page/國際奧林匹克委員會.md "wikilink")

[黃志雄在](../Page/黃志雄.md "wikilink")[北京奧運期間參選](../Page/北京奧運.md "wikilink")[奧林匹克運動員委員會委員](../Page/國際奧林匹克委員會.md "wikilink")，爭取入選[奧林匹克運動員委員會四名委員名額](../Page/國際奧林匹克委員會.md "wikilink")，最後落選。結果由[南韓跆拳道金牌選手](../Page/南韓.md "wikilink")[文大成](../Page/文大成.md "wikilink")、前[俄國](../Page/俄國.md "wikilink")[游泳名將](../Page/游泳.md "wikilink")[波波夫](../Page/波波夫.md "wikilink")、[德國女子](../Page/德國.md "wikilink")[鈍劍選手](../Page/鈍劍.md "wikilink")[博克爾和](../Page/博克爾.md "wikilink")[古巴女子](../Page/古巴.md "wikilink")[排球選手](../Page/排球.md "wikilink")[魯伊茲當選為該屆](../Page/魯伊茲.md "wikilink")[奧林匹克運動員委員會四名委員](../Page/國際奧林匹克委員會.md "wikilink")。

### [2012年中華民國立法委員選舉](../Page/2012年中華民國立法委員選舉.md "wikilink")

| 2012年[新北市第五選舉區](../Page/新北市第五選舉區.md "wikilink")[立法委員選舉結果](../Page/立法委員.md "wikilink") |
| ------------------------------------------------------------------------------------- |
| 號次                                                                                    |
| 1                                                                                     |
| 2                                                                                     |
| 3                                                                                     |
| 4                                                                                     |
| **選舉人數**                                                                              |
| **投票數**                                                                               |
| **有效票**                                                                               |
| **無效票**                                                                               |
| **投票率**                                                                               |

### [2016年中華民國立法委員選舉](../Page/2016年中華民國立法委員選舉.md "wikilink")

| 2016年[新北市第五選舉區](../Page/新北市第五選舉區.md "wikilink")[立法委員選舉結果](../Page/立法委員.md "wikilink") |
| ------------------------------------------------------------------------------------- |
| 號次                                                                                    |
| 1                                                                                     |
| 2                                                                                     |
| 3                                                                                     |
| **選舉人數**                                                                              |
| **投票數**                                                                               |
| **有效票**                                                                               |
| **無效票**                                                                               |
| **投票率**                                                                               |

## 重大議題表決立場

### 贊成

  - 刪除西寶水力發電計畫預算

<!-- end list -->

  - 罷免陳水扁總統

<!-- end list -->

  - 設置長照服務發展基金

### 反對

  - 2008年度大學不得調漲學雜費

<!-- end list -->

  - 停建核四

<!-- end list -->

  - 刪除紅十字總會為特定獎補助對象

<!-- end list -->

  - 刪除軍公教18%、總統國務機要費等預算

<!-- end list -->

  - 反媒體壟斷

<!-- end list -->

  - 如失業率未降低，首長減薪50%

<!-- end list -->

  - 暫緩電價調漲

<!-- end list -->

  - 每月330度以下用戶電價不調漲

<!-- end list -->

  - 調高基本工資

<!-- end list -->

  - 通過顏清標條款

### 沒投票

  - 停建核四

<!-- end list -->

  - 將「刪除核四預算」列入院會議程討論

\[2\]

## 國際賽得獎紀錄

  - 1995年[菲律賓世界錦標賽](../Page/菲律賓.md "wikilink") \<span
    style="background-color: "CC9966"\>銅牌</span>
  - 1997年[香港世界錦標賽](../Page/香港.md "wikilink")
    <span style="background-color: gold">金牌</span>
  - 1999年[加拿大世界錦標賽](../Page/加拿大.md "wikilink")
    <span style="background-color: gold">金牌</span>
  - 2000年[雪梨奧運](../Page/2000年夏季奧林匹克運動會.md "wikilink") \<span
    style="background-color: "CC9966"\>銅牌</span>
  - 2002年[釜山](../Page/釜山.md "wikilink")[亞洲運動會](../Page/2002年亞洲運動會.md "wikilink")
    <span style="background-color: gold">金牌</span>
  - 2003年[德國世界錦標賽](../Page/德國.md "wikilink")
    <span style="background-color: gold">金牌</span>
  - 2004年[泰國雅典奧運亞洲區資格賽](../Page/泰國.md "wikilink") 銅牌
  - 2004年[雅典奧運](../Page/2004年夏季奧林匹克運動會.md "wikilink")
    <span style="background-color: silver">銀牌</span>

## 相關條目

  - [2008年中華民國立法委員選舉區域暨原住民選舉區投票結果列表](../Page/2008年中華民國立法委員選舉區域暨原住民選舉區投票結果列表.md "wikilink")

## 著作

  - 黃志雄、[朱木炎](../Page/朱木炎.md "wikilink")/口述，《贏在我不認輸》（台北市：[國語日報](../Page/國語日報.md "wikilink")，2005）
  - 黃志雄/口述，《黃志雄加碼人生》（台北縣：雅書堂，2010）
  - 黃志雄、[洪佳君](../Page/洪佳君.md "wikilink")/口述，《看見生命之光》（台北市：[幼獅文化](../Page/幼獅文化.md "wikilink")，2010）

## 參考資料

## 外部連結

[新北05](../Category/中華民國區域立法委員選區.md "wikilink")
[Category:新北市政治](../Category/新北市政治.md "wikilink")
[Category:2004年夏季奧林匹克運動會獎牌得主](../Category/2004年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:2000年夏季奥林匹克运动会奖牌得主](../Category/2000年夏季奥林匹克运动会奖牌得主.md "wikilink")
[Category:2002年亞洲運動會金牌得主](../Category/2002年亞洲運動會金牌得主.md "wikilink")
[Category:台灣奧林匹克運動會銀牌得主](../Category/台灣奧林匹克運動會銀牌得主.md "wikilink")
[Category:台灣奧林匹克運動會銅牌得主](../Category/台灣奧林匹克運動會銅牌得主.md "wikilink")
[Category:中華民國亞洲運動會金牌得主](../Category/中華民國亞洲運動會金牌得主.md "wikilink")
[Category:台灣跆拳道選手](../Category/台灣跆拳道選手.md "wikilink")
[Category:台灣奧運跆拳道運動員](../Category/台灣奧運跆拳道運動員.md "wikilink")
[Category:第6屆中華民國立法委員](../Category/第6屆中華民國立法委員.md "wikilink")
[Category:第7屆中華民國立法委員](../Category/第7屆中華民國立法委員.md "wikilink")
[Category:運動員出身的政治人物](../Category/運動員出身的政治人物.md "wikilink")
[Category:中國國民黨黨員](../Category/中國國民黨黨員.md "wikilink")
[Category:國立體育大學校友](../Category/國立體育大學校友.md "wikilink")
[Category:中國文化大學校友](../Category/中國文化大學校友.md "wikilink")
[Category:國立華僑實驗高級中學校友](../Category/國立華僑實驗高級中學校友.md "wikilink")
[Category:新北市立樹林國民中學校友](../Category/新北市立樹林國民中學校友.md "wikilink")
[Category:新北市立文林國民小學校友](../Category/新北市立文林國民小學校友.md "wikilink")
[Category:樹林人](../Category/樹林人.md "wikilink")
[Zhixiong](../Category/黃姓.md "wikilink")
[Category:亞洲運動會跆拳道獎牌得主](../Category/亞洲運動會跆拳道獎牌得主.md "wikilink")
[Category:2000年夏季奧林匹克運動會跆拳道運動員](../Category/2000年夏季奧林匹克運動會跆拳道運動員.md "wikilink")
[Category:2004年夏季奧林匹克運動會跆拳道運動員](../Category/2004年夏季奧林匹克運動會跆拳道運動員.md "wikilink")

1.
2.  [立委投票指南](http://vote.ly.g0v.tw/legislator/voter_sp/1366/8/)。