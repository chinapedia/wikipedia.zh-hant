**九龍**（），與[香港島及](../Page/香港島.md "wikilink")[新界同為](../Page/新界.md "wikilink")[香港三大地域之一](../Page/香港.md "wikilink")。九龍位於香港境內的地理中心，三面環海，與南面的香港島隔[維多利亞港相望](../Page/維多利亞港.md "wikilink")。[香港十八區中的五區屬於九龍](../Page/香港十八區.md "wikilink")，分別是[油尖旺區](../Page/油尖旺區.md "wikilink")、[深水埗區](../Page/深水埗區.md "wikilink")、[九龍城區](../Page/九龍城區.md "wikilink")、[黃大仙區](../Page/黃大仙區.md "wikilink")、[觀塘區](../Page/觀塘區.md "wikilink")。九龍連同新界在中國古藉稱為**九龍半島**。

九龍原來只是[界限街以南](../Page/界限街.md "wikilink")，但在1937年[九龍群山以南成為](../Page/九龍群山.md "wikilink")「[新九龍](../Page/新九龍.md "wikilink")」，通稱九龍。歷年來的填海工程，使九龍的面積不斷擴展，但仍是香港城市規劃之三大主要部份中最小的，截至2010年，九龍的面積約47平方公里\[1\]，2011年人口普查結果顯示，全港約有30%人口居於九龍。

## 歷史

[南宋以前](../Page/南宋.md "wikilink")，史書關於九龍的記載甚少，不過，從[深水埗區](../Page/深水埗區.md "wikilink")[李鄭屋漢墓和](../Page/李鄭屋漢墓.md "wikilink")[旺角](../Page/旺角.md "wikilink")[通菜街出土的](../Page/通菜街.md "wikilink")[晉代](../Page/晉代.md "wikilink")[陶器](../Page/陶.md "wikilink")，證明最晚在[漢代時期九龍已有人居住](../Page/漢代.md "wikilink")，並且受到[中原文化的影響](../Page/中原.md "wikilink")。[秦朝平定](../Page/秦朝.md "wikilink")[百越之時](../Page/百越.md "wikilink")，今天的九龍被納入爲[番禺縣管轄](../Page/番禺縣.md "wikilink")。由[晉朝至](../Page/晉朝.md "wikilink")[明朝期間](../Page/明朝.md "wikilink")，今天的九龍之地曾屬[寶安縣](../Page/寶安縣.md "wikilink")、[東官郡](../Page/東官郡.md "wikilink")、[廣州府等行政區管轄](../Page/廣州府.md "wikilink")。至明朝，[寶安縣改名爲](../Page/寶安縣.md "wikilink")[新安縣](../Page/寶安縣.md "wikilink")，並屬之管轄，直至清朝與英國簽訂《[北京條約](../Page/北京条约.md "wikilink")》，九龍（不包括[新九龍](../Page/新九龍.md "wikilink")）才屬於英屬香港。另相傳中國[宋朝末年](../Page/宋朝.md "wikilink")，1276年6月14日即位的皇帝[宋端宗](../Page/宋端宗.md "wikilink")[趙昰被](../Page/趙昰.md "wikilink")[元朝軍隊相逼南逃避難](../Page/元朝.md "wikilink")，流亡逃至[官富場](../Page/官富場.md "wikilink")，即今日的[九龍城一帶](../Page/九龍城.md "wikilink")，後人在海岸旁的一塊大石刻上「宋王臺」三個字以作紀念，即現存[宋王臺花園](../Page/宋王臺.md "wikilink")。

[Map_of_Hong_Kong_in_First_Convention_of_Peking_in_1860.jpg](https://zh.wikipedia.org/wiki/File:Map_of_Hong_Kong_in_First_Convention_of_Peking_in_1860.jpg "fig:Map_of_Hong_Kong_in_First_Convention_of_Peking_in_1860.jpg")》中國割讓予英國的[九龍半島部份](../Page/九龍半島.md "wikilink")\]\]

### 割讓九龍半島

[第二次鴉片戰爭之役](../Page/第二次鴉片戰爭.md "wikilink")，[清朝再敗給英法聯軍](../Page/清朝.md "wikilink")，於1860年簽下《[北京條約](../Page/北京條約.md "wikilink")》，把九龍半島南部連同鄰近的[昂船洲一同割讓給英國](../Page/昂船洲.md "wikilink")。當時在九龍半島上的新邊界只用矮矮的鐵絲網分割，位置就在今天的[界限街](../Page/界限街.md "wikilink")，這便是界限街的英文名稱「」的由來。1898年，英國通過與清廷簽訂《[中英展拓香港界址專條](../Page/中英展拓香港界址專條.md "wikilink")》及其他一系列租借條約，租借九龍半島北部、新界和鄰近的兩百多個離島，但[九龍寨城除外](../Page/九龍寨城.md "wikilink")，租期99年。自英國於1860年取得九龍半島後到1898租借九龍半島北部和新界，這30多年沒有重點開發九龍，因為九龍半島的控制權在軍部，軍部主要保留九龍半島作為軍事用途保護港島。軍部與英商一直爭議九龍半島的用途，直到1898年《展拓香港界址專條》簽訂後，九龍半島的用途才改為商業與旅遊，而開始大力發展。\[2\][Kowloon_City,_Mainland,_opposite_Hong_Kong.jpg](https://zh.wikipedia.org/wiki/File:Kowloon_City,_Mainland,_opposite_Hong_Kong.jpg "fig:Kowloon_City,_Mainland,_opposite_Hong_Kong.jpg")

#### 發展九龍半島

1904年，[彌敦](../Page/彌敦.md "wikilink")[爵士出任](../Page/爵士.md "wikilink")[香港總督](../Page/香港總督.md "wikilink")，他大力拓展九龍半島，如擴闊主要大道，包括後來因他而更名的[彌敦道](../Page/彌敦道.md "wikilink")，當時九龍半島的人流仍然稀少，被不少人指為蠢事，但時間證明了並非如是，現時彌敦道一帶是九龍區最人煙稠密的地區。\[3\]1905年9月，香港立法局通過興建[九廣鐵路的計劃](../Page/九廣鐵路.md "wikilink")，到1910年正式通車。[Victoria_City_and_Kowloon_1915.jpg](https://zh.wikipedia.org/wiki/File:Victoria_City_and_Kowloon_1915.jpg "fig:Victoria_City_and_Kowloon_1915.jpg")

#### 新九龍

1937年，政府開始將[界限街以北及](../Page/界限街.md "wikilink")[九龍群山以南的新界劃作新九龍](../Page/九龍群山.md "wikilink")。1984年，[中英談判使香港這個歷史遺留問題得以解決](../Page/中英談判.md "wikilink")，「新九龍」這種劃分已經不再有意義，所以現時兩地都一併稱為「九龍」。但位於新九龍的物業跟[新界物業一樣](../Page/新界.md "wikilink")，須向[香港政府繳交](../Page/香港政府.md "wikilink")[地租](../Page/地租.md "wikilink")，而界限街以南的九龍的物業則只需繳交極低的象徵式地租。但根據香港法例第1章釋義及通則條例附表5，新界的定義仍包括新九龍，地域區分有別於實際應用。

#### 居民職業

根據1841年5月《[轅門報](../Page/轅門報.md "wikilink")》（即後來的《[香港政府憲報](../Page/香港政府憲報.md "wikilink")》）所載，[香港島上居民可分為三大類](../Page/香港島.md "wikilink")，即漁民、農民和打石工人。與港島一水之隔的九龍半島，此三大分類自然亦無分別，一體並存。兩者稍有不同的，是[港島的城市化步伐較快](../Page/港島.md "wikilink")，起步較早，而九龍的則起步較晚，要到1920年代之後才稍具規模。\[4\]

### 地名由來

相傳九龍地名由來最常見的解釋是九龍北部的八條山脈亦即為龍脈（[鴉巢山](../Page/鴉巢山.md "wikilink")、[筆架山](../Page/筆架山_\(香港\).md "wikilink")、[獅子山](../Page/獅子山_\(香港\).md "wikilink")、[雞胸山](../Page/雞胸山.md "wikilink")、[慈雲山](../Page/慈雲山.md "wikilink")、[大老山](../Page/大老山.md "wikilink")、[東山](../Page/東山_\(香港\).md "wikilink")、[飛鵝山](../Page/飛鵝山.md "wikilink")），加上皇帝自己（[宋帝昺](../Page/宋帝昺.md "wikilink")），便是九條龍脈；另一說法是九只是代表多的意思。\[5\]

## 城市景觀

## 地理及行政區劃分

[1_hong_kong_aerial_panorama_night_2011.JPG](https://zh.wikipedia.org/wiki/File:1_hong_kong_aerial_panorama_night_2011.JPG "fig:1_hong_kong_aerial_panorama_night_2011.JPG")至[旺角的夜景](../Page/旺角.md "wikilink")，遠方是[香港島](../Page/香港島.md "wikilink")（2011年6月）\]\]
[Kowloon_Peninsula.jpg](https://zh.wikipedia.org/wiki/File:Kowloon_Peninsula.jpg "fig:Kowloon_Peninsula.jpg")至[土瓜灣的霞色](../Page/土瓜灣.md "wikilink")（2008年12月）\]\]
[HK_Kowloon_View_2006.jpg](https://zh.wikipedia.org/wiki/File:HK_Kowloon_View_2006.jpg "fig:HK_Kowloon_View_2006.jpg")至[觀塘一帶](../Page/觀塘.md "wikilink")（2006年12月）\]\]
[East_Tsim_Sha_Tsui_Hung_Hom.jpg](https://zh.wikipedia.org/wiki/File:East_Tsim_Sha_Tsui_Hung_Hom.jpg "fig:East_Tsim_Sha_Tsui_Hung_Hom.jpg")至[紅磡一帶](../Page/紅磡.md "wikilink")，圖中水域為[維多利亞港](../Page/維多利亞港.md "wikilink")\]\]
[HK_East_Kowloon_View2007.jpg](https://zh.wikipedia.org/wiki/File:HK_East_Kowloon_View2007.jpg "fig:HK_East_Kowloon_View2007.jpg")[九龍灣至](../Page/九龍灣.md "wikilink")[觀塘一帶](../Page/觀塘.md "wikilink")\]\]
[West_Kowloon_Cultural_District_201810.jpg](https://zh.wikipedia.org/wiki/File:West_Kowloon_Cultural_District_201810.jpg "fig:West_Kowloon_Cultural_District_201810.jpg")一帶\]\]
[HK_EdinburghPlace_West-Kln_Foster-n-Partners.jpg](https://zh.wikipedia.org/wiki/File:HK_EdinburghPlace_West-Kln_Foster-n-Partners.jpg "fig:HK_EdinburghPlace_West-Kln_Foster-n-Partners.jpg")
[Kai_Tak_Airport_Site_2009.jpg](https://zh.wikipedia.org/wiki/File:Kai_Tak_Airport_Site_2009.jpg "fig:Kai_Tak_Airport_Site_2009.jpg")
[Buildings_in_Kowloon.jpg](https://zh.wikipedia.org/wiki/File:Buildings_in_Kowloon.jpg "fig:Buildings_in_Kowloon.jpg")
根據香港[地政總署測繪處於](../Page/地政總署.md "wikilink")2010年2月文件顯示，九龍總面積為46.94[平方公里](../Page/平方公里.md "wikilink")\[6\]。轄下共有5個分區，每區均有其各自的[區議會](../Page/區議會.md "wikilink")：

  - [九龍城區](../Page/九龍城區.md "wikilink")：包括[九龍城](../Page/九龍城.md "wikilink")、[九龍塘](../Page/九龍塘.md "wikilink")、[何文田](../Page/何文田.md "wikilink")、[土瓜灣](../Page/土瓜灣.md "wikilink")、[馬頭圍](../Page/馬頭圍.md "wikilink")、[紅磡](../Page/紅磡.md "wikilink")、[啟德等地方](../Page/啟德.md "wikilink")。
  - [觀塘區](../Page/觀塘區.md "wikilink")：包括[觀塘](../Page/觀塘.md "wikilink")、[牛頭角](../Page/牛頭角.md "wikilink")、[佐敦谷](../Page/佐敦谷.md "wikilink")、[九龍灣](../Page/九龍灣.md "wikilink")、[四順](../Page/四順.md "wikilink")、[秀茂坪](../Page/秀茂坪.md "wikilink")、[藍田](../Page/藍田_\(香港\).md "wikilink")、[茶果嶺](../Page/茶果嶺.md "wikilink")、[鯉魚門](../Page/鯉魚門.md "wikilink")、[油塘等地方](../Page/油塘.md "wikilink")。
  - [深水埗區](../Page/深水埗區.md "wikilink")：包括[深水埗](../Page/深水埗.md "wikilink")、[長沙灣](../Page/長沙灣.md "wikilink")、[荔枝角](../Page/荔枝角.md "wikilink")、[石硤尾](../Page/石硤尾.md "wikilink")、[又一村](../Page/又一村.md "wikilink")、[昂船洲等地方](../Page/昂船洲.md "wikilink")。
  - [黃大仙區](../Page/黃大仙區.md "wikilink")：包括[黃大仙](../Page/黃大仙_\(香港地方\).md "wikilink")、[鑽石山](../Page/鑽石山.md "wikilink")、[慈雲山](../Page/慈雲山.md "wikilink")、[樂富](../Page/樂富.md "wikilink")、[橫頭磡](../Page/橫頭磡.md "wikilink")、[牛池灣](../Page/牛池灣.md "wikilink")、[新蒲崗等地方](../Page/新蒲崗.md "wikilink")。
  - [油尖旺區](../Page/油尖旺區.md "wikilink")：包括[油麻地](../Page/油麻地.md "wikilink")、[尖沙咀](../Page/尖沙咀.md "wikilink")、[旺角](../Page/旺角.md "wikilink")、[佐敦](../Page/佐敦.md "wikilink")、[大角咀等地方](../Page/大角咀.md "wikilink")。

1968年5月起，九龍、新九龍與香港島一起重新分為十區。過去是先有九龍後有分區\[7\]，但現在已經反過來，「九龍」是指以上五個區的總和。而隨着區議會區界修訂，九龍之範圍亦有變更。

自1998年起，[立法會選舉選區在九龍有](../Page/香港立法會.md "wikilink")2個：

  - [九龍東](../Page/九龍東.md "wikilink")：包括黃大仙區及觀塘區
  - [九龍西](../Page/九龍西.md "wikilink")：包括油尖旺區、深水埗區及九龍城區

選舉選區的議席數目根據人口而決定，個別選區的議席數目會因人口增減而或有更改。

[1995年香港立法局選舉亦曾使用](../Page/1995年香港立法局選舉.md "wikilink")「九龍中」、「九龍東」、「九龍南」、「九龍西」、「九龍東北」、「九龍西北」及「九龍西南」等劃分。

## 人口

根據2011年[香港人口普查](../Page/香港人口普查.md "wikilink")，九龍的總人口為2,108,419人，佔全港人口29.82%。

2001年時，九龍的人口總數有2,023,979人，佔全港30.2%。而歷史記載，1860年九龍人口不足1千人\[8\]。

## 衛星城市

原屬新九龍的[觀塘區一帶由於距離九龍半島較遠](../Page/觀塘區.md "wikilink")，過去曾和[荃灣一起發展為](../Page/荃灣.md "wikilink")[衛星城市](../Page/香港新市鎮#衛星城市.md "wikilink")。

## 填海工程

九龍的大部份土地是由[填海所得](../Page/填海.md "wikilink")，包括早期的[啟德機場等](../Page/啟德機場.md "wikilink")，而近年的[西九龍填海計劃是歷來在市區進行的最大規模填海工程](../Page/西九龍填海計劃.md "wikilink")，它把九龍半島的面積擴展了三分之一，並將海岸線向海港伸展，延展最多的部分達一公里闊，[佐敦道碼頭](../Page/佐敦道碼頭.md "wikilink")、[大角咀碼頭更因填海而消失](../Page/大角咀碼頭.md "wikilink")，而來往佐敦道的航線亦取消。昂船洲原本是和九龍半島分開的，在[西九龍填海工程中](../Page/西九龍.md "wikilink")，[昂船洲正式和九龍半島連接](../Page/昂船洲.md "wikilink")，截至2010年，九龍半島的總面積擴展至約47平方公里，比毗鄰的城市[澳門面積約](../Page/澳門.md "wikilink")30平方公里大出約63%。

## 名稱誤用

雖然官方對九龍的定義僅限於[九龍城區](../Page/九龍城區.md "wikilink")、[觀塘區](../Page/觀塘區.md "wikilink")、[深水埗區](../Page/深水埗區.md "wikilink")、[黃大仙區及](../Page/黃大仙區.md "wikilink")[油尖旺區五區](../Page/油尖旺區.md "wikilink")，由於九龍位於整個香港的中心、佔有地利等因素，因此有商家喜將之以外的地方亦稱位於九龍，如連接九龍地區的[新界地域](../Page/新界.md "wikilink")，尤其新界東的[將軍澳及新界西的](../Page/將軍澳.md "wikilink")[葵涌等地](../Page/葵涌.md "wikilink")。

較早期的例子包括[無綫電視昔日的總部](../Page/無綫電視.md "wikilink")[電視城以及](../Page/電視城.md "wikilink")[香港科技大學](../Page/香港科技大學.md "wikilink")，同樣位於[新界](../Page/新界.md "wikilink")[西貢區](../Page/西貢區.md "wikilink")，但地址均使用「九龍清水灣」；另外[合和集團位於新界](../Page/合和集團.md "wikilink")[荃灣的酒店](../Page/荃灣.md "wikilink")，過往曾一度名為「九龍[悅來酒店](../Page/悅來酒店.md "wikilink")」；甚至遠至[新界](../Page/新界.md "wikilink")[屯門的](../Page/屯門.md "wikilink")[香港黃金海岸](../Page/香港黃金海岸.md "wikilink")，地址被稱為「九龍[青山灣](../Page/青山灣.md "wikilink")[青山公路](../Page/青山公路.md "wikilink")1號」。即使[無綫電視在](../Page/無綫電視.md "wikilink")2003年將總部遷至位於[將軍澳工業邨的](../Page/將軍澳工業邨.md "wikilink")[電視廣播城](../Page/電視廣播城.md "wikilink")，其官方地址仍使用「九龍將軍澳工業邨駿才街七十七號」\[9\]。

2006年，[長實集團位於新界](../Page/長實集團.md "wikilink")[葵涌的樓盤](../Page/葵涌.md "wikilink")[雍雅軒](../Page/雍雅軒.md "wikilink")，由於在[電視廣告宣傳其處於九龍區](../Page/電視廣告.md "wikilink")，因而被[廣播事務管理局發出勸喻](../Page/廣播事務管理局.md "wikilink")，指廣告誤導。於2008年落成、同樣位於[葵涌的](../Page/葵涌.md "wikilink")[新鴻基地產商廈項目](../Page/新鴻基地產.md "wikilink")，卻被命名為[九龍貿易中心](../Page/九龍貿易中心.md "wikilink")，更以「九龍西商業新據點」自居\[10\]。

2009年，位於新界西貢區[將軍澳](../Page/將軍澳.md "wikilink")[寶林的大型商場](../Page/寶林.md "wikilink")[新都城在宣傳板上以](../Page/新都城.md "wikilink")「東九龍最大購物中心」自居，由於寶琳實際上是位於新界西貢區將軍澳北，因此遭到真正為東九龍區最大的購物商場——位於[九龍灣的](../Page/九龍灣.md "wikilink")[MegaBox去信表示不滿](../Page/MegaBox.md "wikilink")，後來[新都城刪去有關字眼](../Page/新都城.md "wikilink")，事件不了了之。

2011年，[將軍澳市中心](../Page/將軍澳市中心.md "wikilink")[天晉旁的酒店項目命名](../Page/天晉.md "wikilink")[香港九龍東皇冠假日酒店及](../Page/香港九龍東皇冠假日酒店.md "wikilink")[香港九龍東智選假日酒店](../Page/香港九龍東智選假日酒店.md "wikilink")，它們均位於新界西貢區將軍澳，不過以九龍東命名。

2015年，[卓能集團位於新界](../Page/卓能集團.md "wikilink")[荃灣](../Page/荃灣.md "wikilink")[油柑頭的樓盤命名](../Page/油柑頭.md "wikilink")[壹號九龍山頂](../Page/壹號九龍山頂.md "wikilink")，但荃灣屬新界，非九龍。

## 未來發展

  - 沙田至中環綫途經九龍

[沙田至中環綫是](../Page/沙田至中環綫.md "wikilink")\[11\]正在興建中的大型鐵路擴建項目，路綫分為兩大部份，包括大圍至紅磡段及金鐘至紅磡段，整個擴建部份長約17公里，設有10座[車站](../Page/車站.md "wikilink")。大圍至紅磡段部份由現[馬鞍山綫](../Page/馬鞍山綫.md "wikilink")[大圍開始伸延](../Page/大圍.md "wikilink")，穿過[大老山到達](../Page/大老山.md "wikilink")[鑽石山](../Page/鑽石山.md "wikilink")，再前往[啟德新發展區](../Page/啟德.md "wikilink")、[土瓜灣到達](../Page/土瓜灣.md "wikilink")[紅磡接現](../Page/紅磡.md "wikilink")[西鐵綫](../Page/西鐵綫.md "wikilink")，2019年中通車時該路段將與馬鞍山綫及西鐵綫一起合稱[屯馬綫](../Page/屯馬綫.md "wikilink")。金鐘至紅磡段則由現[東鐵綫](../Page/東鐵綫.md "wikilink")[紅磡站經](../Page/紅磡站.md "wikilink")[第四條過海鐵路到達](../Page/第四條過海鐵路.md "wikilink")[香港島的](../Page/香港島.md "wikilink")[會展](../Page/會展站.md "wikilink")，最終以[金鐘為終點站](../Page/金鐘站.md "wikilink")。計劃包括興建6個新車站（大圍至紅磡段：[顯徑](../Page/顯徑站.md "wikilink")、[啟德](../Page/啟德站.md "wikilink")、[宋皇臺](../Page/宋皇臺站.md "wikilink")、[土瓜灣](../Page/土瓜灣站.md "wikilink")、[何文田](../Page/何文田站.md "wikilink")；金鐘至紅磡段：[會展](../Page/會展站.md "wikilink")）、重置[紅磡站及擴建](../Page/紅磡站.md "wikilink")[鑽石山站與](../Page/鑽石山站.md "wikilink")[金鐘站](../Page/金鐘站.md "wikilink")。

  - 廣深港高速鐵路設香港段西九龍站

<!-- end list -->

  - 中九龍幹線

[中九龍幹線是](../Page/中九龍幹線.md "wikilink")[6號幹線的一部份](../Page/6號幹線.md "wikilink")，貫通[油麻地](../Page/油麻地.md "wikilink")、[京士柏](../Page/京士柏.md "wikilink")、[十二號山](../Page/十二號山.md "wikilink")、[採石山](../Page/採石山.md "wikilink")、[馬頭圍](../Page/馬頭圍.md "wikilink")、[馬頭角](../Page/馬頭角.md "wikilink")、[啟德至](../Page/啟德.md "wikilink")[九龍灣](../Page/九龍灣.md "wikilink")，全線均為三線雙程分隔[公路](../Page/公路.md "wikilink")，全長4.7公里，包括長3.9公里的[隧道路道](../Page/隧道.md "wikilink")，預計於2020年竣工。

  - 西九文化區

[西九文化區為集結了一系列世界級](../Page/西九文化區.md "wikilink")[文化](../Page/文化.md "wikilink")、[藝術](../Page/藝術.md "wikilink")、[潮流](../Page/潮流.md "wikilink")、[消費及大眾](../Page/消費.md "wikilink")[娛樂為一體的綜合文化主要場地](../Page/娛樂.md "wikilink")，核心設施包括[劇院](../Page/劇院.md "wikilink")、[博物館](../Page/博物館.md "wikilink")、演藝場館、[劇場及](../Page/劇場.md "wikilink")[廣場等](../Page/廣場.md "wikilink")，旨意提高[香港文化的水平及世界地位](../Page/香港文化.md "wikilink")\[12\]。2012年3月，城市規劃委員會公布發展圖則草圖，於2013年起分階段展開工程，預計第一階段建設於2020年或者以前竣工，第二階段建設於2030年或者以前竣工\[13\]\[14\]。

  - 啟德發展計劃

《[啟德發展計劃](../Page/啟德發展計劃.md "wikilink")》\[15\]是基於[香港國際機場由](../Page/香港國際機場.md "wikilink")[九龍城搬遷至](../Page/九龍城.md "wikilink")[大嶼山](../Page/大嶼山.md "wikilink")[赤鱲角後](../Page/赤鱲角.md "wikilink")，[香港政府在](../Page/香港政府.md "wikilink")[啟德機場舊址進行的大型市區發展計劃](../Page/啟德機場.md "wikilink")，包括興建[啟德體育園區](../Page/啟德體育園區.md "wikilink")、[都會公園](../Page/都會公園_\(香港\).md "wikilink")、[郵輪碼頭](../Page/啟德郵輪碼頭.md "wikilink")、[酒店](../Page/酒店.md "wikilink")、[住宅](../Page/住宅.md "wikilink")、[商業及](../Page/商業.md "wikilink")[娛樂等核心](../Page/娛樂.md "wikilink")[建築項目](../Page/建築.md "wikilink")，總規劃面積達328[公頃](../Page/公頃.md "wikilink")。首階段項目將會於2013年或以前陸續地落成、次階段項目將會於2016年或以前陸續地落成，終階段項目將會於2021年或以前陸續地落成，即預計於2021年全面竣工。

## 九龍區法定古蹟

  - [九龍寨城南門遺蹟](../Page/九龍城寨.md "wikilink")
  - [前九龍寨城衙門](../Page/九龍城寨.md "wikilink")
  - [瑪利諾修院學校](../Page/瑪利諾修院學校.md "wikilink")
  - [李鄭屋漢墓](../Page/李鄭屋漢墓.md "wikilink")
  - [尖沙咀前水警總部](../Page/尖沙咀前水警總部.md "wikilink")
  - [前九龍英童學校](../Page/前九龍英童學校.md "wikilink")
  - [前九廣鐵路鐘樓](../Page/前九廣鐵路鐘樓.md "wikilink")
  - [香港天文台總部](../Page/香港天文台總部.md "wikilink")
  - [東華三院文物館](../Page/東華三院文物館.md "wikilink")

## 建築物變遷

## 注释

## 参考文献

## 外部链接

## 參見

  - [香港填海工程](../Page/香港填海工程.md "wikilink")
  - [十大建設計劃](../Page/十大建設計劃.md "wikilink")

{{-}}

[Category:香港半島](../Category/香港半島.md "wikilink")
[九龍](../Category/九龍.md "wikilink")
[Category:香港地方](../Category/香港地方.md "wikilink")

1.
2.  [《香港掌故》造地編 三之二](http://hk.epochtimes.com/7/9/7/51234.htm) 香港掌故
3.  [彌敦道
    見證彌敦爵士的遠見](http://news.hkheadline.com/figure/index.asp?id=287)頭條日報
    頭條網 - 圖說往昔
4.  [《九龍城區風物志》](http://www.districtcouncils.gov.hk/klc_d/chinese/doc/KCD_Heritage_Txt.pdf)

5.  饒玖才：《香港地名探索》，ISBN 978-962-950-261-4
6.  [地政總署測繪處：香港地理資料](http://www.landsd.gov.hk/mapping/en/publications/hk_geographic_data_sheet.pdf)
7.  1967年《[華僑日報](../Page/華僑日報.md "wikilink")》新界市區分圖：葵涌街道詳圖
8.  [亞洲電視《解密百年香港》](http://www.hkatv.com/infoprogram/07/hkdecode/content05b.html)
     第五集 Part 2
9.
10. [《九龍貿易中心－九龍西商業新據點》](http://www.shkp.com/data/publication/quarterly/33/33/33_486.pdf)
    《新鴻基地產季刊》 2009年第二季
11.
12. [立法會CB(1)161/03-04號文件](http://www.legco.gov.hk/yr03-04/chinese/panels/ha/papers/haplw1118cb1-161-c.pdf)立法會秘書處
    議會事務部1 2003年10月27日
13. [西九文化區發展圖則草圖](http://hk.news.yahoo.com/%E8%A5%BF%E4%B9%9D%E6%96%87%E5%8C%96%E5%8D%80%E7%99%BC%E5%B1%95%E5%9C%96%E5%89%87%E8%8D%89%E5%9C%96-085600845.html)
    《星島日報》 2012年3月30日
14. [西九文化區發展草圖公布](http://hk.news.yahoo.com/%E8%A5%BF%E4%B9%9D%E6%96%87%E5%8C%96%E5%8D%80%E7%99%BC%E5%B1%95%E8%8D%89%E5%9C%96%E5%85%AC%E5%B8%83-093219808.html)
     《明報》 2012年3月30日
15. [立法會規劃地政及工程事務委員會啟德規劃檢討](http://www.legco.gov.hk/yr04-05/chinese/panels/plw/papers/plwcb1-99-1c.pdf)