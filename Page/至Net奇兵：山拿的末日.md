《**至Net奇兵：山拿的末日**》（''，）是[至Net奇兵系列的第三款遊戲](../Page/至Net奇兵.md "wikilink")，為《[至Net奇兵](../Page/至Net奇兵_\(遊戲\).md "wikilink")》的續作，由[The
Game
Factory製作](../Page/The_Game_Factory.md "wikilink")，在2008年6月20日於美國推出在[任天堂DS平臺的](../Page/任天堂DS.md "wikilink")[角色扮演遊戲](../Page/電子角色扮演遊戲.md "wikilink")。此遊戲的故事情節和《[至Net奇兵：追求無限](../Page/至Net奇兵：追求無限.md "wikilink")》一樣概括《[至NET奇兵](../Page/至NET奇兵.md "wikilink")》[動畫版的第四輯](../Page/動畫.md "wikilink")。

## 故事

和《[至Net奇兵：追求無限](../Page/至Net奇兵：追求無限.md "wikilink")》一樣，威廉因為被山拿控制而成為山拿的手下，為了令威廉回復清醒，所以Lyoko小組的五名Lyoko戰士——亞烈達、傑理明、治狼、尤美及阿奇便繼續和邪惡的山拿戰鬥，希望能夠令威廉擺脫山拿控制回復清醒以及消滅山拿。
[《至NET奇兵：山拿的末日》遊戲截圖.jpg](https://zh.wikipedia.org/wiki/File:《至NET奇兵：山拿的末日》遊戲截圖.jpg "fig:《至NET奇兵：山拿的末日》遊戲截圖.jpg")

## 遊戲方式

  - 遊戲方式：全用觸摸筆（不用按鍵）
  - 人物走動：觸摸筆持續點擊觸控螢幕，人物將會向觸摸筆的方向移動。觸摸筆距離人物越是遠，人物移動速度越快。
  - 對戰方式：走向一隻怪獸/威廉，或一定距離內怪獸/威廉向人物移動時，就可進入對戰模式。

[C](../Category/2008年电子游戏.md "wikilink")
[C](../Category/任天堂DS遊戲.md "wikilink")
[C](../Category/電子角色扮演遊戲.md "wikilink")
[Category:至Net奇兵](../Category/至Net奇兵.md "wikilink")