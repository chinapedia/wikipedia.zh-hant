[VLBA_St_Croix-04.jpg](https://zh.wikipedia.org/wiki/File:VLBA_St_Croix-04.jpg "fig:VLBA_St_Croix-04.jpg")[聖克羅伊是超長基線位於最東邊的接受機](../Page/聖克羅伊.md "wikilink")。\]\]
**甚長基線陣列**（，簡寫：）是由位於[美国](../Page/美国.md "wikilink")[新墨西哥州](../Page/新墨西哥州.md "wikilink")[索科洛的](../Page/索科洛.md "wikilink")[美国国家射电天文台](../Page/美国国家射电天文台.md "wikilink")[陣列操作中心遙控的](../Page/陣列操作中心.md "wikilink")10架[電波望遠鏡組成的陣列](../Page/無線電望遠鏡.md "wikilink")。這個阵列是全世界最大的天文[超長基線干涉測量仪器](../Page/超長基線干涉測量.md "wikilink")。阵列於1986年2月開始建造，至1993年5月才全部完成，並在1993年5月29日首度使用全部的10架天線，全部的建設費用為8500萬美元。

每個甚長基線站都有一架直徑25公尺的[碟型天線和一棟作為操控室的建築](../Page/碟型天線.md "wikilink")，用來放置電腦、紙帶記錄器和其他必須的儀器，並用來蒐集和儲存天線所獲得的訊號。每個天線的重量是240噸，當直立的指向上方時高度相當於10層樓。基線的最大長度為8611公里。

## 观测站

超长基线阵列通常所做的无线电观测在[波长从](../Page/波长.md "wikilink")3[毫米到](../Page/毫米.md "wikilink")28[厘米](../Page/厘米.md "wikilink")，或换言之，在从0.3GHz至96GHz的频率。在这个频率范围内，VLBA观察对射电天文有用的八个不同频带。VLBA也观测在1Ghz以下的两个窄的无线频带，包括明亮的微波激射器排放产生的谱线。

甚長基線陣列的電波望遠鏡位於下列地點：

  - [夏威夷的](../Page/夏威夷.md "wikilink")[毛納基山天文台](../Page/毛納基山天文台.md "wikilink")（）
  - [加利福尼亞州的](../Page/加利福尼亞州.md "wikilink")[歐文谷](../Page/歐文谷.md "wikilink")（）
  - [亞利桑那州的](../Page/亞利桑那州.md "wikilink")[基特峰國立天文台](../Page/基特峰國立天文台.md "wikilink")（）
  - [新墨西哥州的](../Page/新墨西哥州.md "wikilink")[Pie
    Town](../Page/Pie_Town.md "wikilink")（）
  - [新墨西哥州的](../Page/新墨西哥州.md "wikilink")[洛斯阿拉莫斯](../Page/洛斯阿拉莫斯.md "wikilink")（）
  - [德克薩斯州的](../Page/德克薩斯州.md "wikilink")[大衛斯堡](../Page/大衛斯堡.md "wikilink")（）
  - [華盛頓區的](../Page/華盛頓區.md "wikilink")[布魯斯特](../Page/布魯斯特.md "wikilink")（）
  - [愛荷華州的](../Page/愛荷華州.md "wikilink")[North
    Liberty](../Page/North_Liberty.md "wikilink")（）
  - [新罕布夏州的](../Page/新罕布夏州.md "wikilink")[漢考克](../Page/漢考克.md "wikilink")（）
  - [美屬維爾京群島的](../Page/美屬維爾京群島.md "wikilink")[聖克羅伊](../Page/聖克羅伊.md "wikilink")（）

## 高灵敏度阵列（HSA）

使用VLBA可以被动态地调度，可通过纳入其他的射电望远镜，例如波多黎各[阿雷西博射电望远镜](../Page/阿雷西博天文台.md "wikilink")，西弗吉尼亚州的，新墨西哥州的[甚大天线阵](../Page/甚大天线阵.md "wikilink")（VLA），和德国的，其灵敏度得到改善五倍。

<table>
<thead>
<tr class="header">
<th><p><a href="../Page/地名学.md" title="wikilink">地名学</a></p></th>
<th><p><a href="../Page/美国州份.md" title="wikilink">州</a></p></th>
<th><p><a href="../Page/经纬度.md" title="wikilink">经纬度</a></p></th>
<th></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/阿雷西沃.md" title="wikilink">阿雷西沃</a></p></td>
<td><p><a href="../Page/波多黎各.md" title="wikilink">波多黎各</a></p></td>
<td></td>
<td><p>AR</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Green_Bank,_West_Virginia.md" title="wikilink">Green Bank</a></p></td>
<td><p><a href="../Page/西弗吉尼亚州.md" title="wikilink">西弗吉尼亚州</a></p></td>
<td></td>
<td><p>GB</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/甚大天线阵.md" title="wikilink">甚大天线阵</a></p></td>
<td><p><a href="../Page/新墨西哥州.md" title="wikilink">新墨西哥州</a></p></td>
<td></td>
<td><p>Y27</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Effelsberg.md" title="wikilink">Effelsberg</a></p></td>
<td><p><a href="../Page/德国.md" title="wikilink">德国</a></p></td>
<td></td>
<td><p>EB</p></td>
</tr>
</tbody>
</table>

## 外部連結

  - [超長基線陣列網站](http://www.vlba.nrao.edu/)

[Category:射电望远镜](../Category/射电望远镜.md "wikilink")
[Category:干涉儀望遠鏡](../Category/干涉儀望遠鏡.md "wikilink")