**中华人民共和国水利部**，简称**水利部**，是[中华人民共和国国务院主管](../Page/中华人民共和国国务院.md "wikilink")[水行政的](../Page/水.md "wikilink")[组成部门](../Page/国务院组成部门.md "wikilink")\[1\]。

水利部成立于1949年10月，是中华人民共和国成立最早的政府部门之一，历经多次改革。

## 沿革

1949年10月，成立[中央人民政府水利部](../Page/中央人民政府水利部.md "wikilink")。1954年9月，改为**中华人民共和国水利部**。

1958年2月11日，水利部与[电力工业部合并](../Page/中华人民共和国电力工业部.md "wikilink")，成立[中华人民共和国水利电力部](../Page/中华人民共和国水利电力部.md "wikilink")。1979年2月23日，又决定将**中华人民共和国水利部**和[电力工业部分开](../Page/中华人民共和国电力工业部.md "wikilink")。

1982年国务院机构改革方案，再次将水利部和[电力工业部合并](../Page/中华人民共和国电力工业部.md "wikilink")，第二次设立中华人民共和国水利电力部。1988年4月，恢复**中华人民共和国水利部**。

2018年3月17日[第十三届全国人民代表大会第一次会议通过](../Page/第十三届全国人民代表大会第一次会议.md "wikilink")《第十三届全国人民代表大会第一次会议
关于国务院机构改革方案的决定》，批准《国务院机构改革方案》。方案规定：“优化水利部职责。将[国务院三峡工程建设委员会及其办公室](../Page/国务院三峡工程建设委员会.md "wikilink")、[国务院南水北调工程建设委员会及其办公室并入水利部](../Page/国务院南水北调工程建设委员会.md "wikilink")。不再保留国务院三峡工程建设委员会及其办公室、国务院南水北调工程建设委员会及其办公室。”\[2\]\[3\]\[4\]

## 职责

根据《》，水利部承担下列职能\[5\]：

## 机构设置

根据《》，水利部设置下列机构\[6\]：

### 内设机构

### 派出机构

### 直属事业单位

### 直属企业单位

  - [中国水利水电出版社](../Page/中国水利水电出版社.md "wikilink")
  - [南水北调东线总公司](../Page/南水北调东线总公司.md "wikilink")

### 主管社会团体

  - [中国水利学会](../Page/中国水利学会.md "wikilink")

## 历任负责人

<table>
<thead>
<tr class="header">
<th><p>任次</p></th>
<th><p>肖像<br />
姓名</p></th>
<th><p><a href="../Page/籍贯.md" title="wikilink">籍贯</a></p></th>
<th><p>在任时期<br />
（职位）</p></th>
<th><p>最高任职</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><a href="../Page/傅作義.md" title="wikilink">傅作義</a></p></td>
<td><p><a href="../Page/山西.md" title="wikilink">山西</a><a href="../Page/万荣县.md" title="wikilink">荣河县</a><br />
（今<a href="../Page/万荣.md" title="wikilink">万荣</a>）</p></td>
<td><p>1949年10月19日－1954年9月<br />
（<a href="../Page/中央人民政府水利部.md" title="wikilink">中央人民政府水利部部长</a>）</p>
<hr />
<p>1954年9月－1958年2月11日<br />
（<strong>中华人民共和国水利部</strong>部长）</p>
<hr />
<p>1958年2月11日－1972年10月<br />
（<a href="../Page/中华人民共和国水利电力部.md" title="wikilink">中华人民共和国水利电力部部长</a>）</p></td>
<td><p><a href="../Page/中华人民共和国国防委员会.md" title="wikilink">中华人民共和国国防委员会副主席</a><br />
<a href="../Page/中国人民政治协商会议全国委员会副主席.md" title="wikilink">中国人民政治协商会议全国委员会副主席</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><a href="../Page/张文碧.md" title="wikilink">张文碧</a></p></td>
<td></td>
<td><p>1972年10月－1975年1月17日<br />
（水利电力部<a href="../Page/革委会.md" title="wikilink">革命委员会主任</a>）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p><a href="../Page/钱正英.md" title="wikilink">钱正英</a><br />
（女）</p></td>
<td><p><a href="../Page/浙江.md" title="wikilink">浙江</a><a href="../Page/嘉兴.md" title="wikilink">嘉兴</a></p></td>
<td><p>1975年－1979年<br />
（中华人民共和国水利电力部部长）</p>
<hr />
<p>1979年2月23日－1982年3月8日<br />
（<strong>中华人民共和国水利部</strong>部长）</p>
<hr />
<p>1982年－1988年<br />
（中华人民共和国水利电力部部长）</p></td>
<td><p>中国人民政治协商会议全国委员会副主席</p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p><a href="../Page/杨振怀.md" title="wikilink">杨振怀</a></p></td>
<td><p><a href="../Page/安徽.md" title="wikilink">安徽</a><a href="../Page/合肥.md" title="wikilink">合肥</a></p></td>
<td><p>1988年4月12日－1993年3月29日<br />
（中华人民共和国水利部部长）</p></td>
<td><p><a href="../Page/全国人民代表大会农业与农村委员会.md" title="wikilink">全国人民代表大会农业与农村委员会副主任委员</a></p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p><a href="../Page/钮茂生.md" title="wikilink">钮茂生</a></p></td>
<td><p><a href="../Page/北京.md" title="wikilink">北京</a></p></td>
<td><p>1993年3月29日－1998年11月4日</p></td>
<td><p><a href="../Page/河北省省长.md" title="wikilink">河北省省长</a><br />
<a href="../Page/中国人民政治协商会议全国委员会.md" title="wikilink">中国人民政治协商会议全国委员会民族和宗教委员会主任</a></p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p><a href="../Page/汪恕诚.md" title="wikilink">汪恕诚</a></p></td>
<td><p><a href="../Page/江苏.md" title="wikilink">江苏</a><a href="../Page/溧阳.md" title="wikilink">溧阳</a></p></td>
<td><p>1998年11月4日－2007年4月27日</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p><a href="../Page/陳雷_(水利部部長).md" title="wikilink">陈雷</a></p></td>
<td><p><a href="../Page/北京.md" title="wikilink">北京</a></p></td>
<td><p>2007年4月27日－2018年4月27日</p></td>
<td><p>第十三届全国人民代表大会<a href="../Page/全国人民代表大会农业与农村委员会.md" title="wikilink">全国人民代表大会农业与农村委员会副主任委员</a></p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p><a href="../Page/鄂竟平.md" title="wikilink">鄂竟平</a></p></td>
<td><p><a href="../Page/河北.md" title="wikilink">河北</a><a href="../Page/乐亭.md" title="wikilink">乐亭</a></p></td>
<td><p>2018年4月27日－</p></td>
<td></td>
</tr>
</tbody>
</table>

## 参考文献

## 外部链接

  - [中华人民共和国水利部](http://www.mwr.gov.cn)（官方网站）
  - [水利部历史沿革与职能](http://news.xinhuanet.com/zhengfu/2003-02/25/content_744870.htm)
    新华网

{{-}}        |-          |-

[中华人民共和国水利部](../Category/中华人民共和国水利部.md "wikilink")
[Category:第1届国务院组成部门](../Category/第1届国务院组成部门.md "wikilink")
[Category:第7届国务院组成部门](../Category/第7届国务院组成部门.md "wikilink")
[Category:第8届国务院组成部门](../Category/第8届国务院组成部门.md "wikilink")
[Category:第9届国务院组成部门](../Category/第9届国务院组成部门.md "wikilink")
[Category:第10届国务院组成部门](../Category/第10届国务院组成部门.md "wikilink")
[Category:第11届国务院组成部门](../Category/第11届国务院组成部门.md "wikilink")
[Category:第12届国务院组成部门](../Category/第12届国务院组成部门.md "wikilink")

1.

2.

3.

4.

5.
6.