**苯並\[*a*\]芘**（），化學式：C<sub>20</sub>H<sub>12</sub>，是一種五環[多環芳香烃類](../Page/多環芳香烃.md "wikilink")，是一個高活性的間接致癌物質、[誘變劑和致畸的物質](../Page/誘變劑.md "wikilink")，結晶為黃色固體。這種物質是在300到600°[C之間的不完全燃燒狀態下產生](../Page/攝氏.md "wikilink")。苯並芘存在於[煤焦油中](../Page/煤焦油.md "wikilink")，而煤焦油可見於汽車廢氣（尤其是[柴油引擎](../Page/柴油.md "wikilink")）、[菸草與](../Page/菸草.md "wikilink")[木材燃燒產生的煙](../Page/木材.md "wikilink")，以及[炭烤食物中](../Page/炭烤食物.md "wikilink")。苯並芘為一種間接的[致癌物质](../Page/致癌物质.md "wikilink")，從18世紀以來，便發現與許多[癌症有關](../Page/癌症.md "wikilink")。其在体内的[代谢物](../Page/代谢物.md "wikilink")[二羟環氧苯並芘](../Page/二羟環氧苯並芘.md "wikilink")，是产生致癌性的物質。

## 用途

BAP並無[工業用途](../Page/工業.md "wikilink")。
[Benzopyrene_diol_epoxide_chemical_structure.png](https://zh.wikipedia.org/wiki/File:Benzopyrene_diol_epoxide_chemical_structure.png "fig:Benzopyrene_diol_epoxide_chemical_structure.png")的化學結構。\]\]

## 参考文献

[Category:IARC第1类致癌物质](../Category/IARC第1类致癌物质.md "wikilink")
[Category:多環芳香烴](../Category/多環芳香烴.md "wikilink")
[Category:突變原](../Category/突變原.md "wikilink")