**佳能 EOS 450D**，又称为**Digital Rebel
XSi**（[北美市场](../Page/北美.md "wikilink")）或**EOS Kiss Digital
X2**（日本市场）\[1\]，是由佳能公司于2008年1月23日推出，3月\[2\]上市的一款消费级别单镜头反光相机。在北美市场，EOS
450D于同年4月\[3\] 上市。作为成功的[佳能 EOS
400D的继任者](../Page/佳能_EOS_400D.md "wikilink")，佳能EOS
450D具有1220万有效像素CMOS图像感应器以及拥有更大的连拍缓存，并且使用3英寸23万像素的宽视角LCD显示屏。

## 参见

  - [佳能 EOS 300D](../Page/佳能_EOS_300D.md "wikilink")
  - [佳能 EOS 350D](../Page/佳能_EOS_350D.md "wikilink")
  - [佳能 EOS 400D](../Page/佳能_EOS_400D.md "wikilink")
  - [尼康 D80](../Page/尼康_D80.md "wikilink")

## 参考文献

<small></small>

[Category:数码单反相机](../Category/数码单反相机.md "wikilink")
[Category:佳能相機](../Category/佳能相機.md "wikilink")
[Category:2008年面世的相機](../Category/2008年面世的相機.md "wikilink")

1.
2.
3.