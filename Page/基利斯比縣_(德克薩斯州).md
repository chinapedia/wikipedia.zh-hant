**基利斯比縣**（英文：**Gillespie County,
Texas**）是位於[美國](../Page/美國.md "wikilink")[德克薩斯州中部的一個縣](../Page/德克薩斯州.md "wikilink")。面積2,749平方公里。根據[美國人口調查局](../Page/美國人口調查局.md "wikilink")2000年統計，共有人口20,814人。縣城是[弗雷德里克斯堡](../Page/弗雷德里克斯堡_\(德克薩斯州\).md "wikilink")。

成立於1848年2月23日，縣名紀念[美墨戰爭將領羅伯特](../Page/美墨戰爭.md "wikilink")·A·基利斯比（Robert
A. Gillespie）。

美國前總統[林登·約翰遜就是在這個縣出生](../Page/林登·約翰遜.md "wikilink")。

[美國海軍](../Page/美國海軍.md "wikilink")[五星上將](../Page/五星上將.md "wikilink")[切斯特·威廉·尼米茲也是在弗雷德里克斯堡的大街](../Page/切斯特·威廉·尼米茲.md "wikilink")（Main
Street）出生，現時那棟建築仍在大街。

## 參考

  - [The Handbook of Texas
    Online](http://www.tshaonline.org/handbook/online/articles/hcg04)

[G](../Category/得克萨斯州行政区划.md "wikilink")
[Category:1848年建立的聚居地](../Category/1848年建立的聚居地.md "wikilink")