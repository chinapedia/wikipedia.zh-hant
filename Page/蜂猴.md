**蜂猴**（學名*Loris
tardigradus*），是一種主要產于[印度和](../Page/印度.md "wikilink")[斯里蘭卡雨林地區的小型](../Page/斯里蘭卡.md "wikilink")[原猴](../Page/原猴.md "wikilink")，屬於[懒猴科](../Page/懒猴科.md "wikilink")[蜂猴屬](../Page/蜂猴屬.md "wikilink")。

蜂猴平均體長在17.5-26釐米之間，平均體重只有85-350克。它們有一雙面向前方的大眼睛，耳朵也很大，大拇腳趾與其他四趾相對，形成鑷子的形狀，可以拿握東西，沒有尾巴。蜂猴的身體為深灰色，毛髮為銀白色。

群居動物，在晚上分頭覓食，白天則聚集到巢穴裏睡覺。蜂猴主要以[昆蟲](../Page/昆蟲.md "wikilink")、[蜥蜴和](../Page/蜥蜴.md "wikilink")[卵為食](../Page/卵.md "wikilink")，有時也吃[漿果](../Page/漿果.md "wikilink")、[樹葉和](../Page/樹葉.md "wikilink")[花蕾](../Page/花蕾.md "wikilink")。

母蜂猴在10個月時就達到性成熟期，每年可以和公蜂猴交配兩次。孕期為166-169天，每胎1-2仔，斷奶期為6-7個月。

由於獵殺和棲息地的減少，蜂猴目前被列爲瀕危動物。

共有兩個亞種：

  - 指名亞種 *L. t. tardigradus*
  - *L. t. nycticeboides*

## 參考

  - Database entry includes justification for why this species is
    endangered

[Category:懒猴科](../Category/懒猴科.md "wikilink")
[Category:中国国家一级保护动物](../Category/中国国家一级保护动物.md "wikilink")
[Category:卡尔·林奈命名的生物分类](../Category/卡尔·林奈命名的生物分类.md "wikilink")