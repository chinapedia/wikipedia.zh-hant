**托德·朗德格伦**（[英语](../Page/英语.md "wikilink")：****，）是[美国最出色的](../Page/美国.md "wikilink")[流行乐和](../Page/流行乐.md "wikilink")[摇滚乐的](../Page/摇滚乐.md "wikilink")[歌手](../Page/歌手.md "wikilink")、词曲作者、乐器演奏家、[吉他手](../Page/吉他手.md "wikilink")、音乐制作人和录音工程师之一。

朗德格伦出生于[美国](../Page/美国.md "wikilink")[费城](../Page/费城.md "wikilink")。他最初于1968年至1970年间在[纳兹乐队](../Page/纳兹乐队.md "wikilink")()效力，1970年离开乐队后开始个人音乐生涯并且发行了一系列堪称摇滚经典的专辑。如[Runt:
The Ballad of Todd
Rundgren](../Page/Runt:_The_Ballad_of_Todd_Rundgren.md "wikilink")、[Something/Anything?](../Page/Something/Anything?.md "wikilink")、[A
Wizard, a True
Star等等](../Page/A_Wizard,_a_True_Star.md "wikilink")。此外他还组建了[艺术摇滚风格的](../Page/艺术摇滚.md "wikilink")[乌托邦乐队](../Page/乌托邦乐队.md "wikilink")()。

1970年代末，他开始为其他著名乐手、乐队制作专辑，成为世界顶级的音乐制作人。并在期间先后加入[Ringo
Starr](../Page/林格·斯塔.md "wikilink")、[汽車合唱團](../Page/汽車合唱團.md "wikilink")()乐队后身[新汽车](../Page/新汽车.md "wikilink")()、[金发女郎](../Page/金发女郎.md "wikilink")()等乐队。

## 外部連結

  - [Hello,It's Me - Todd
    Rundgren音乐传奇的第一个十年](http://blueslyrist.blogcn.com/articles/helloits-me-todd-rundgren%E9%9F%B3%E4%B9%90%E4%BC%A0%E5%A5%87%E7%9A%84%E7%AC%AC%E4%B8%80%E4%B8%AA%E5%8D%81%E5%B9%B4.html)
    原作者Lyrist Lee

[Category:美国吉他手](../Category/美国吉他手.md "wikilink")
[Category:美国钢琴家](../Category/美国钢琴家.md "wikilink")
[Category:美国音乐制作人](../Category/美国音乐制作人.md "wikilink")
[Category:美國摇滚歌手](../Category/美國摇滚歌手.md "wikilink")
[Category:美國男歌手](../Category/美國男歌手.md "wikilink")
[Category:美國创作歌手](../Category/美國创作歌手.md "wikilink")
[Category:宾夕法尼亚州人](../Category/宾夕法尼亚州人.md "wikilink")
[Category:瑞典裔美国人](../Category/瑞典裔美国人.md "wikilink")
[Category:奥地利裔美国人](../Category/奥地利裔美国人.md "wikilink")
[Category:日本富士搖滾音樂祭參加歌手](../Category/日本富士搖滾音樂祭參加歌手.md "wikilink")