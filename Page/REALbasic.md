[Realbasic2005macide.jpg](https://zh.wikipedia.org/wiki/File:Realbasic2005macide.jpg "fig:Realbasic2005macide.jpg")中使用Realbasic的畫面\]\]
[Tour-linux-1-windoweditor.jpg](https://zh.wikipedia.org/wiki/File:Tour-linux-1-windoweditor.jpg "fig:Tour-linux-1-windoweditor.jpg")中使用REALbasic的畫面\]\]
[Tour-win-1-windoweditor.jpg](https://zh.wikipedia.org/wiki/File:Tour-win-1-windoweditor.jpg "fig:Tour-win-1-windoweditor.jpg")中使用REALbasic的畫面\]\]
**REALbasic**又名[Real
Studio是一個基於](../Page/Real_Studio.md "wikilink")[BASIC語言的編程語言及](../Page/BASIC.md "wikilink")[跨平臺開發工具](../Page/跨平臺.md "wikilink")，由[REAL
Software, Inc.的Andrew](../Page/REAL_Software,_Inc..md "wikilink")
Barry于1997年6月12日于官方網站公佈，原名「CrossBASIC」\[1\]。REALbasic能够在[Windows](../Page/Windows.md "wikilink")、[Mac及](../Page/Mac.md "wikilink")[Linux這三种](../Page/Linux.md "wikilink")[操作系統上运行](../Page/操作系統.md "wikilink")\[2\]，并能生成本机代码，即原生的应用程序。此外，还能生成Web程序，并将支持iOS等移动平台。由于除了兼容部分BASIC语言的语法之外，这款开发工具已经与过气的老式BASIC语言渐行渐远，因此于2013年的第一个发行版开始产品名称、企业名称均更名为[Xojo](../Page/Xojo.md "wikilink")（音近"啁啾"）。

## REALbasic的特徵

以下是REALbasic的一些特徵。

### 正面

  - 是一款快速开发环境，提供了对多平台API和界面功能的包装
  - 为所有平台编译CPU可直接执行的指令代码，不需要任何解释器、虚拟机或运行时，执行速度较慢
  - REALbasic是一個完全根據[事件驅動程式設計設計的](../Page/事件驅動程式設計.md "wikilink")[面向对象程序设计式](../Page/面向对象程序设计.md "wikilink")[編程語言](../Page/編程語言.md "wikilink")。
  - REALbasic是一個不必[運行庫而可製作](../Page/運行庫.md "wikilink")[軟件的](../Page/軟件.md "wikilink")[编译器](../Page/编译器.md "wikilink")。
  - 和[Perl一樣的方式](../Page/Perl.md "wikilink")——[正则表达式的](../Page/正则表达式.md "wikilink")[方法尋找文字列](../Page/方法_\(電腦科學\).md "wikilink")。
  - 不必學會[Macintosh
    Toolbox也可操作](../Page/Macintosh_Toolbox.md "wikilink")。
  - 比較適合／擅長于[多媒體軟件的開發](../Page/多媒體.md "wikilink")。
  - 參照計數器方式的[垃圾回收](../Page/垃圾回收_\(計算機科學\).md "wikilink")。
  - 完全支持[Unicode](../Page/Unicode.md "wikilink")。
  - 備有通訊簿（只限Mac）。
  - 支持[QuickTime](../Page/QuickTime.md "wikilink")（只限Windows及Mac）。
  - 有聲。
  - 支持[3D設計](../Page/3D.md "wikilink")。
  - 支持[HTTP](../Page/HTTP.md "wikilink")、[POP3](../Page/POP3.md "wikilink")、[SMTP](../Page/SMTP.md "wikilink")、[XML及](../Page/XML.md "wikilink")[SOAP](../Page/SOAP.md "wikilink")。
  - [SSL](../Page/SSL.md "wikilink")（只限專業版）
  - 備有[字典](../Page/字典.md "wikilink")。
  - 備有[Visual Basic的數據兼容性](../Page/Visual_Basic.md "wikilink")。
  - 可以製造有[插件的軟件](../Page/插件.md "wikilink")。

### 反面

  - 所生成的应用程序由于包含了支持库，尺寸较大，需要很多容量，在90年代和21实际初曾是一个较大的弊端，不过随着网络速度和磁盘容量都迅速增加，随便一个软件都要上10M的今天，此问题并不明显。
  - 由于付费开发工具行业目前已经衰落，该公司的资源相对有限，有些Bug的修复需要较长时间，添加了较大新功能或做了大改动的一些版本不是特别稳定。

## 技能

  - 雖然可以不必學通常在[Mac中學習](../Page/Mac.md "wikilink")[編程語言時需要學的](../Page/編程語言.md "wikilink")[Macintosh
    Toolbox也可以製造軟件](../Page/Macintosh_Toolbox.md "wikilink")，但是不能製造比較複雜的軟件。因此，爲了滿足這些需要開發較爲複雜的軟件的人而可以製造有插件及有3D圖像／動畫的軟件。

## 評價

被稱爲「[Mac中的](../Page/Mac.md "wikilink")[Visual
Basic](../Page/Visual_Basic.md "wikilink")」。\[3\]

## 參考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - [Xojo, Inc.](http://www.xojo.com/?lang=chs)，Xojo官方中文网站。
  - [Xojo中文用户事务部门](https://web.archive.org/web/20121206110051/http://www.realcenter.cn/products/price.htm)，Xojo中文事务页面
  - [Really Basic
    REALbasic](https://web.archive.org/web/20070703043413/http://ttpsoftware.com/jpsite/index.php)，用來對初學者介紹學習Real
    Basic的簡易度。
  - [REALbasic的開發者雜誌官方網站](http://www.rbdeveloper.com/)
  - [RB圖書館](http://www.rblibrary.com/)，公開一些代原碼等。
  - [REALDev](https://web.archive.org/web/20070629080535/http://www.classicteck.com/rbdn.php)，一個Wiki型的開發／討論及資料庫。
  - [RBDevZone](http://www.rbdevzone.com)，給高級／專業的編程員的討論。
  - [declareSub.com](http://www.declareSub.com)，公開一些代源碼等資料。
  - [召苏博客 -
    Xojo频道](http://www.3exware.com/home/default.asp?cateID=5)一些Xojo相关咨询

[Category:集成开发环境](../Category/集成开发环境.md "wikilink")
[Category:程序设计语言](../Category/程序设计语言.md "wikilink")

1.  [Cross Basic is now
    REALBasic](https://www.webcitation.org/query?id=1256589685602607&url=www.geocities.com/SoHo/Lofts/5483/CrossBasic.html)
2.  [RealBasic 2005 for Mac, Windows and
    Linux（英文），於2005年9月13日發表的評論性文章](http://www.theregister.co.uk/2005/09/13/review_realbasic_2005/)
3.  [想不到REALbasic這麽厲害！](http://www.homebanker.net/blog/post/87.html)