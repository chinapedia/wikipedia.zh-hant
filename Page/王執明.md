**王執明**，[台灣女](../Page/台灣.md "wikilink")[地質學家](../Page/地質學家.md "wikilink")，也是台灣第首位女地質學家\[1\]，曾親身深入中央山脈、花東縱谷和太魯閣實地勘查\[2\]，為台灣的地質學建立第一手資料。曾任中國地質學會理事長，台大地質科學系教授\[3\]。

## 生平

1920
年，王執明生於[中國東北](../Page/中國.md "wikilink")[黑龍江](../Page/黑龍江.md "wikilink")，1930
年和母親從[北京逃難到](../Page/北京.md "wikilink")[四川](../Page/四川.md "wikilink")，一路雖然顛沛流離，但王執明日後受訪時總是提起這段經歷，認為那段回憶種下了她日後對山和石頭熱愛的根源。\[4\]\[5\]

高二那年和家人來到台灣，在台中二中讀書時對[地質學發生了興趣](../Page/地質學.md "wikilink")，後考取[臺灣大學地質系](../Page/臺灣大學.md "wikilink")。畢業後與[歷史學家](../Page/史學家.md "wikilink")[李國祁結婚](../Page/李國祁.md "wikilink")，1959年到[美國](../Page/美國.md "wikilink")[佛羅里達州立大學讀地質研究所](../Page/佛罗里达州立大学.md "wikilink")，1961年取得碩士學位後，到[德國陪伴在當地唸書的丈夫](../Page/德國.md "wikilink")，期間於德國[波鴻魯爾大學礦物研究所攻讀博士學位](../Page/波鴻魯爾大學.md "wikilink")，為配合夫婿的返台時間，王執明僅花兩年半就取得了博士學位。\[6\]\[7\]

## 研究與貢獻

返台後，王執明進入台大地質系任教，曾任系主任及所長，1969 年到 1990
年執教鞭期間，親自引領學生上山下谷，取得岩石及田野調查的第一手資料，尤其多次深入[中央山脈](../Page/中央山脈.md "wikilink")、[花東縱谷實地勘查岩層結構](../Page/花東縱谷.md "wikilink")。王執明也潛心推廣地質科學，除編譯國高中地球科學教科書外，還成立地球科學文教基金會，興辦活動，推廣地質知識。\[8\]\[9\]

1987 年，王執明為紀念父親王漢倬，捐贈設立「王漢倬」論文獎，以表彰地質科學論文表現優異者\[10\]。

## 著作

[太魯閣國家公園立霧溪峽谷岩性及岩石成因研究](https://www.taroko.gov.tw/zh-tw/Ecology/ResearchDetail?id=27)

## 參考文獻

[category:國立臺灣大學理學院校友](../Page/category:國立臺灣大學理學院校友.md "wikilink")

[Category:國立臺中第二高級中學校友](../Category/國立臺中第二高級中學校友.md "wikilink")
[Category:佛羅里達州立大學校友](../Category/佛羅里達州立大學校友.md "wikilink")
[Category:波鴻魯爾大學校友](../Category/波鴻魯爾大學校友.md "wikilink")
[Category:台灣地質學家](../Category/台灣地質學家.md "wikilink")
[Category:女性地球科學家](../Category/女性地球科學家.md "wikilink")
[Category:國立臺灣大學教授](../Category/國立臺灣大學教授.md "wikilink")
[Category:第7屆中華民國考試委員](../Category/第7屆中華民國考試委員.md "wikilink")
[Category:台灣戰後東北移民](../Category/台灣戰後東北移民.md "wikilink")
[Category:王姓](../Category/王姓.md "wikilink")
[Category:第8屆中華民國考試委員](../Category/第8屆中華民國考試委員.md "wikilink")
[Category:台中市人](../Category/台中市人.md "wikilink")

1.

2.

3.
4.
5.

6.
7.
8.
9.
10.