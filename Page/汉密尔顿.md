**Hamilton**，音譯**哈密尔顿** 、**漢彌爾頓** 、**漢米爾頓**、**咸美頓**、**漢默頓**。可以指：

## 人物

  - [理查德·汉密尔顿](../Page/理查德·汉密尔顿.md "wikilink")：[美国](../Page/美国.md "wikilink")[NBA](../Page/NBA.md "wikilink")[篮球](../Page/篮球.md "wikilink")[明星](../Page/明星.md "wikilink")
  - [亚历山大·汉密尔顿](../Page/亚历山大·汉密尔顿.md "wikilink")：[美國金融家](../Page/美國.md "wikilink")、軍官、政治家，美國開國元勛之一，曾任[美國財政部長](../Page/美國財政部長.md "wikilink")。
  - [琳达·汉密尔顿](../Page/琳达·汉密尔顿.md "wikilink")：美國[好莱坞](../Page/好莱坞.md "wikilink")[演员](../Page/演员.md "wikilink")
  - [泰勒·汉密尔顿](../Page/泰勒·汉密尔顿.md "wikilink")：美国职业公路[自行车](../Page/自行车.md "wikilink")[运动员](../Page/运动员.md "wikilink")
  - [刘易斯·汉密尔顿](../Page/刘易斯·汉密尔顿.md "wikilink")：英国[F1赛车手](../Page/F1.md "wikilink")，2008年、2014年、2015年、2017年及2018年F1年度车手冠军。
  - [威廉·盧雲·哈密頓](../Page/威廉·盧雲·哈密頓.md "wikilink")：[愛爾蘭](../Page/愛爾蘭.md "wikilink")[數學家](../Page/數學家.md "wikilink")、[物理學家及](../Page/物理學家.md "wikilink")[天文學家](../Page/天文學家.md "wikilink")。
  - [韓美洵](../Page/韓美洵.md "wikilink")：前[香港](../Page/香港.md "wikilink")[副輔政司](../Page/香港輔政司.md "wikilink")。

## 地名

### 澳洲

  - [咸美頓
    (新南威爾士州)](../Page/咸美頓_\(新南威爾士州\).md "wikilink")，[紐卡斯爾市郊](../Page/纽卡斯尔_\(新南威尔士州\).md "wikilink")
      - [咸美頓火車站 (新南威爾士州)](../Page/咸美頓火車站_\(新南威爾士州\).md "wikilink")
  - [咸美頓 (昆士蘭州)布里斯班首府](../Page/咸美頓_\(昆士蘭州\).md "wikilink")
  - [咸美頓 (南澳大利亚州)](../Page/咸美頓_\(南澳大利亚州\).md "wikilink")
  - [咸美頓 (塔斯曼尼亞州)](../Page/咸美頓_\(塔斯曼尼亞州\).md "wikilink")
  - [咸美頓 (維多利亞州)](../Page/咸美頓_\(維多利亞州\).md "wikilink")
  - [咸美頓島 (昆士蘭州)](../Page/咸美頓島_\(昆士蘭州\).md "wikilink")

### 百慕達

  - [汉密尔顿](../Page/汉密尔顿_\(百慕大\).md "wikilink")：[百慕大的首府](../Page/百慕大.md "wikilink")
  - [咸美頓教區 (百慕達)](../Page/咸美頓教區_\(百慕達\).md "wikilink")

### 加拿大

  - [哈密尔顿 (安大略省)](../Page/哈密尔顿_\(安大略省\).md "wikilink")
      - [咸美頓GO中心一個車站](../Page/咸美頓GO中心.md "wikilink")
      - [John C. Munro Hamilton International
        Airport](../Page/John_C._Munro_Hamilton_International_Airport.md "wikilink")：[咸美頓的機場](../Page/哈密尔顿_\(安大略省\).md "wikilink")
      - 咸美頓港：又名[伯靈頓灣](../Page/伯靈頓灣.md "wikilink")（[Burlington
        Bay](../Page/Burlington_Bay.md "wikilink")）[安大略湖的支流](../Page/安大略湖.md "wikilink")
  - [咸美頓鎮(安大略省)](../Page/咸美頓鎮\(安大略省\).md "wikilink")
  - [哈密爾頓島 (努纳武特地區)](../Page/哈密爾頓島_\(努纳武特地區\).md "wikilink")

### 韓國

  - [巨文島](../Page/巨文島.md "wikilink")，英語名稱為哈密尔顿港（Port
    Hamilton）[朝鮮半島南部沿岸](../Page/朝鮮半島.md "wikilink")[濟州海峽的島嶼](../Page/濟州海峽.md "wikilink")。

### 新西蘭

  - [汉密尔顿](../Page/汉密尔顿_\(新西兰\).md "wikilink")：新西兰[北岛北部](../Page/北島_\(新西兰\).md "wikilink")，是[新西兰第四大城市](../Page/新西兰.md "wikilink")，也是新西兰最大的内陆城市。

### 英國

  - [汉密尔顿
    (南拉纳克郡)](../Page/汉密尔顿_\(南拉纳克郡\).md "wikilink")：蘇格蘭[南拉纳克郡行政中心](../Page/南拉纳克郡.md "wikilink")
      - [咸美頓宮](../Page/咸美頓宮.md "wikilink")
  - [汉密尔顿
    (莱斯特)](../Page/汉密尔顿_\(莱斯特\).md "wikilink")：英格蘭[莱斯特的區域](../Page/莱斯特.md "wikilink")

### 美國

  - [咸美頓 (阿拉巴馬州)](../Page/咸美頓_\(阿拉巴馬州\).md "wikilink")
  - [咸美頓 (科羅拉多州)](../Page/咸美頓_\(科羅拉多州\).md "wikilink")
  - [咸美頓 (佐治亞州)](../Page/咸美頓_\(佐治亞州\).md "wikilink")
  - [咸美頓 (伊利諾伊州)](../Page/咸美頓_\(伊利諾伊州\).md "wikilink")
  - [咸美頓鎮 (印第安納州)](../Page/咸美頓鎮_\(印第安納州\).md "wikilink")：位於[斯托本縣
    (印第安納州)和](../Page/斯托本縣_\(印第安納州\).md "wikilink")[迪卡爾布縣
    (印第安納州)](../Page/迪卡爾布縣_\(印第安納州\).md "wikilink")
  - [咸美頓
    (克林頓縣)](../Page/咸美頓_\(克林頓縣\).md "wikilink")：位於[印第安納州](../Page/印第安納州.md "wikilink")[克林頓縣](../Page/克林頓縣_\(印地安納州\).md "wikilink")
  - [咸美頓
    (麥迪遜郡)](../Page/咸美頓_\(麥迪遜郡\).md "wikilink")：位於[印第安納州](../Page/印第安納州.md "wikilink")[麥迪遜縣](../Page/麥迪遜縣_\(印地安納州\).md "wikilink")
  - [咸美頓 (艾奧瓦州)](../Page/咸美頓_\(艾奧瓦州\).md "wikilink")
  - [咸美頓 (堪薩斯州)](../Page/咸美頓_\(堪薩斯州\).md "wikilink")
  - [咸美頓 (馬里蘭州)](../Page/咸美頓_\(馬里蘭州\).md "wikilink")
  - [咸美頓 (麻薩諸塞州)](../Page/咸美頓_\(麻薩諸塞州\).md "wikilink")
  - [咸美頓 (密歇根州)](../Page/咸美頓_\(密歇根州\).md "wikilink")
  - [咸美頓 (密西西比州)](../Page/咸美頓_\(密西西比州\).md "wikilink")
  - [咸美頓 (密蘇里州)](../Page/咸美頓_\(密蘇里州\).md "wikilink")
  - [咸美頓 (蒙大拿州)](../Page/咸美頓_\(蒙大拿州\).md "wikilink")
  - [咸美頓 (內華達州)](../Page/咸美頓_\(內華達州\).md "wikilink")
  - [咸美頓鎮
    (大西洋郡)](../Page/咸美頓鎮_\(大西洋郡\).md "wikilink")：位於[新澤西州](../Page/新澤西州.md "wikilink")[大西洋縣](../Page/大西洋縣.md "wikilink")
      - [咸美頓商場](../Page/咸美頓商場.md "wikilink")：位於咸美頓鎮的商場
  - [咸美頓鎮
    (默瑟縣)](../Page/咸美頓鎮_\(默瑟縣\).md "wikilink")：位於[新澤西州](../Page/新澤西州.md "wikilink")[默瑟縣](../Page/默瑟縣.md "wikilink")
      - [咸美頓站 (NJT車站)](../Page/咸美頓站_\(NJT車站\).md "wikilink")：NJT的車站
  - [咸美頓鎮 (紐約州)](../Page/咸美頓鎮_\(紐約州\).md "wikilink")
      - [咸美頓村 (紐約州)](../Page/咸美頓村_\(紐約州\).md "wikilink")
  - [咸美頓 (北卡羅萊納州)](../Page/咸美頓_\(北卡羅萊納州\).md "wikilink")
  - [咸美頓 (北達科他州)](../Page/咸美頓_\(北達科他州\).md "wikilink")
  - [咸美頓 (俄亥俄州)](../Page/咸美頓_\(俄亥俄州\).md "wikilink")
  - [咸美頓 (德克薩斯州)](../Page/咸美頓_\(德克薩斯州\).md "wikilink")
  - [咸美頓 (弗吉尼亞州)](../Page/咸美頓_\(弗吉尼亞州\).md "wikilink")
  - [咸美頓 (華盛頓州)](../Page/咸美頓_\(華盛頓州\).md "wikilink")
  - [咸美頓 (威斯康辛州)](../Page/咸美頓_\(威斯康辛州\).md "wikilink")
  - [咸美頓城 (加利福尼亞州)](../Page/咸美頓城_\(加利福尼亞州\).md "wikilink")

### 香港

  - \-{[咸美頓街](../Page/咸美頓街.md "wikilink")}- 一條位於油麻地的街道

參見：

  - [漢密爾頓縣](../Page/漢密爾頓縣.md "wikilink")
  - [漢密爾頓鎮](../Page/漢密爾頓鎮.md "wikilink")

## 教育

部份學校以咸美頓為名，包括

  - [咸美頓學院](../Page/咸美頓學院.md "wikilink")：美國[紐約](../Page/紐約.md "wikilink")
  - [咸美頓學院
    (艾奧瓦州)](../Page/咸美頓學院_\(艾奧瓦州\).md "wikilink")：美國[艾奧瓦州](../Page/艾奧瓦州.md "wikilink")
  - [咸美頓學院
    (肯塔基州)](../Page/咸美頓學院_\(肯塔基州\).md "wikilink")：美國[肯塔基州已關閉的女子中學](../Page/肯塔基州.md "wikilink")
  - [咸美頓男子高中](../Page/咸美頓男子高中.md "wikilink")：紐西蘭[汉密尔顿](../Page/汉密尔顿_\(新西兰\).md "wikilink")
  - [咸美頓學院
    (蘇格蘭)](../Page/咸美頓學院_\(蘇格蘭\).md "wikilink")：[蘇格蘭](../Page/蘇格蘭.md "wikilink")[汉密尔顿](../Page/汉密尔顿_\(南拉纳克郡\).md "wikilink")
  - [咸美頓和阿歷山大學院](../Page/咸美頓和阿歷山大學院.md "wikilink")：澳洲[汉密尔顿](../Page/咸美頓_\(維多利亞州\).md "wikilink")

## 其他

  - [咸美頓 (電影)](../Page/咸美頓_\(電影\).md "wikilink"), 一奪瑞典制電影，主角為卡爾咸美頓
  - [咸美頓 (schooner)](../Page/咸美頓_\(schooner\).md "wikilink"), an
    American schooner class ship during the War of 1812
  - [咸美頓学院足球俱乐部](../Page/咸美頓学院足球俱乐部.md "wikilink")（Hamilton Academical
    F.C.）：[蘇超足球會](../Page/蘇超.md "wikilink")
  - [咸美頓屋 (dance)](../Page/咸美頓屋_\(dance\).md "wikilink"), a Scottish
    country dance
  - [咸美頓鐘錶公司瑞士錶行](../Page/咸美頓鐘錶公司.md "wikilink")
  - [咸美頓紀錄](../Page/咸美頓紀錄.md "wikilink"), a record label
  - [咸美頓方程](../Page/咸美頓方程.md "wikilink"), A set of physical equations on
    Hamiltonian mechanics.
  - [咸美頓徑](../Page/咸美頓徑.md "wikilink"), a certain type of graph in
    mathematical graph theory
  - [美國十元](../Page/美國十元.md "wikilink"), U.S. currency featuring a
    portrait of Alexander Hamilton
  - [漢密爾頓 (音樂劇)](../Page/漢密爾頓_\(音樂劇\).md "wikilink")
    是一齣關於美國開國元勳[亞歷山大·漢密爾頓的音樂劇](../Page/亞歷山大·漢密爾頓.md "wikilink")