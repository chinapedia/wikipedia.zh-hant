**NSV**12.7毫米[重機槍由](../Page/重機槍.md "wikilink")[蘇聯制造](../Page/蘇聯.md "wikilink")，名字取自其三位設計師G.I.
Nikitin、J.S. Sokolov及V.I. Volkov。

## 歷史

NSV在1971年推出，用於取代[蘇聯紅軍的](../Page/蘇聯紅軍.md "wikilink")[DShK重機槍](../Page/DShK重機槍.md "wikilink")，1972年正式裝備。NSV獲很多國家特許生產，如[波蘭](../Page/波蘭.md "wikilink")、[南斯拉夫](../Page/南斯拉夫.md "wikilink")、[印度](../Page/印度.md "wikilink")、[保加利亞等](../Page/保加利亞.md "wikilink")。

蘇聯解體後[俄羅斯以](../Page/俄羅斯.md "wikilink")[Kord重機槍取代NSV](../Page/Kord重機槍.md "wikilink")，而原本特許生產NSV的[哈薩克亦停止生產](../Page/哈薩克.md "wikilink")。

## 衍生型

  - NSVT

NSVT是裝在車輛射架上的NSV改裝版本，如[主戰坦克及](../Page/主戰坦克.md "wikilink")[裝甲運兵車](../Page/裝甲運兵車.md "wikilink")，[塞爾維亞的](../Page/塞爾維亞.md "wikilink")亦特許生產NSVT，名為M87，並裝備當時的[南斯拉夫軍隊](../Page/南斯拉夫.md "wikilink")。

  - WKM-B

[波蘭的NSV重機槍改用](../Page/波蘭.md "wikilink")[12.7×99毫米NATO彈藥的版本](../Page/12.7×99mm_NATO.md "wikilink")。

## 使用國

[12,7_NSV_Turku_3.JPG](https://zh.wikipedia.org/wiki/File:12,7_NSV_Turku_3.JPG "fig:12,7_NSV_Turku_3.JPG")的NSV\]\]
[Warsaw_Hummer_03.JPG](https://zh.wikipedia.org/wiki/File:Warsaw_Hummer_03.JPG "fig:Warsaw_Hummer_03.JPG")軍隊，裝在[悍馬上的NSV](../Page/悍馬.md "wikilink")\]\]
[Постоянная_группировка_ВМФ_России_в_Средиземном_море_обеспечивает_противовоздушную_оборону_над_территории_Сирии_(29).jpg](https://zh.wikipedia.org/wiki/File:Постоянная_группировка_ВМФ_России_в_Средиземном_море_обеспечивает_противовоздушную_оборону_над_территории_Сирии_\(29\).jpg "fig:Постоянная_группировка_ВМФ_России_в_Средиземном_море_обеспечивает_противовоздушную_оборону_над_территории_Сирии_(29).jpg")士兵使用裝在[舰艇上的NSV](../Page/舰艇.md "wikilink")\]\]

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  - ：名為**12.7 ItKK 96**\[1\]

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  - ：名為**Zastava M87**\[2\]

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
### 前使用國

  -
  -
## 參考

  - [DShK重機槍](../Page/DShK重機槍.md "wikilink")
  - [白朗寧M2重機槍](../Page/白朗寧M2重機槍.md "wikilink")
  - [KPV重機槍](../Page/KPV重機槍.md "wikilink")

## 参考文献

## 外部連結

  - —[NSV 12.7 Modern
    Firearms—NSV](https://web.archive.org/web/20071024112952/http://world.guns.ru/machine/mg02-e.htm)

[Category:重機槍](../Category/重機槍.md "wikilink")
[Category:12.7×108毫米槍械](../Category/12.7×108毫米槍械.md "wikilink")
[Category:俄羅斯槍械](../Category/俄羅斯槍械.md "wikilink")
[Category:蘇聯槍械](../Category/蘇聯槍械.md "wikilink")
[Category:運動及狩獵武器中央設計研究局](../Category/運動及狩獵武器中央設計研究局.md "wikilink")

1.  <http://www.mil.fi/maavoimat/kalustoesittely/index.dsp?level=54&equipment=73>
2.  [zastava官方頁面-zastava
    M87](http://www.zastava-arms.co.yu/images/vojni/m87/m87_engleski.htm)