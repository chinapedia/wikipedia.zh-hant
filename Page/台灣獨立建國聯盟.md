**台灣獨立建國聯盟**（[英文](../Page/英文.md "wikilink")：**World United Formosans for
Independence**，**WUFI**；[白話字](../Page/白話字.md "wikilink")：Tâi-oân
To̍k-li̍p Kiàn-kok
Liân-bêng），簡稱**台獨聯盟**或**獨盟**，是一個推動[台灣獨立運動的組織](../Page/台灣獨立運動.md "wikilink")。1970年由四個海外獨立運動團體：日本[台灣青年獨立聯盟](../Page/台灣青年獨立聯盟.md "wikilink")（1960年成立，委員長[辜寬敏](../Page/辜寬敏.md "wikilink")）、加拿大[台灣人權委員會](../Page/台灣人權委員會.md "wikilink")（1964年成立）、美國[全美台灣獨立聯盟](../Page/全美台灣獨立聯盟.md "wikilink")（1966年成立，當時主席為[蔡同榮](../Page/蔡同榮.md "wikilink")\[1\]）和[歐洲台灣獨立聯盟](../Page/歐洲台灣獨立聯盟.md "wikilink")（1967年成立）與國內[台灣自由聯盟共同組成世界性台灣獨立聯盟](../Page/台灣自由聯盟.md "wikilink")。成為在海外勢力最大的[台灣獨立運動組織](../Page/台灣獨立運動.md "wikilink")。

## 簡介

臺灣獨立建國聯盟以建立自由、民主、平等、福祉、公義之[台灣共和國為宗旨](../Page/台灣共和國.md "wikilink")。台獨聯盟的主張是正名、制憲、以台灣名義加入聯合國。\[2\]

## 歷史

早期台獨組織多在[日本組織](../Page/日本.md "wikilink")，但隨著前往[美國的台灣留學生增加](../Page/美國.md "wikilink")，台獨組織在美國也與日俱增，加上[聯合國總部位於美國](../Page/聯合國總部.md "wikilink")[紐約](../Page/紐約.md "wikilink")，因此連日本[台灣青年獨立聯盟委員長](../Page/台灣青年獨立聯盟.md "wikilink")[辜寬敏也極力主張世界性的台獨聯盟總本部應設於紐約](../Page/辜寬敏.md "wikilink")。因此自1969年9月20日起，全球性的台灣獨立聯盟會議於紐約召開，1970年1月1日宣布成立台灣獨立建國聯盟。第一任總本部負責人由[蔡同榮及](../Page/蔡同榮.md "wikilink")[張燦鍙擔任正副主席](../Page/張燦鍙.md "wikilink")\[3\]。

1970年4月，台灣獨立建國聯盟盟員[黃文雄](../Page/黃文雄_\(人權活動家\).md "wikilink")、[鄭自財](../Page/鄭自財.md "wikilink")「[槍擊蔣經國事件](../Page/四二四刺蔣案.md "wikilink")（四二四刺蔣案）」，導致台灣獨立建國聯盟內部對走暴力路線及和平路線引起爭執，更導致首任主席蔡同榮宣佈不再連任。四二四刺蔣案後，黃文雄與鄭自才都離開台灣獨立建國聯盟；雖然台獨聯盟沒有分裂，鄭自才仍然不看好台獨聯盟後勢發展，因為「蔡同榮、張燦鍙、[陳隆志這些領頭都太保守了](../Page/陳隆志.md "wikilink")」\[4\]。

1973年3月29日，台灣獨立建國聯盟盟員[黃昭夫](../Page/黃昭夫.md "wikilink")（亦有被記錄為「黃照夫」者）在[法國](../Page/法國.md "wikilink")[巴黎](../Page/巴黎.md "wikilink")[巴士底廣場持刀割喉殺傷](../Page/巴士底廣場.md "wikilink")[中國國民黨駐法國總書記](../Page/中國國民黨.md "wikilink")[滕永康](../Page/滕永康.md "wikilink")（[滕傑子](../Page/滕傑.md "wikilink")），被捕後入獄五年。\[5\]\[6\]

1976年10月，台灣獨立建國聯盟盟員[王幸男以郵包炸傷](../Page/王幸男.md "wikilink")[台灣省主席](../Page/台灣省主席.md "wikilink")[謝東閔等事件](../Page/謝東閔.md "wikilink")（[王幸男郵包炸彈事件](../Page/王幸男郵包炸彈事件.md "wikilink")）。由於暴力路線不得同情台獨運動的[美國政府及](../Page/美國政府.md "wikilink")[美國國會議員支持](../Page/美國國會.md "wikilink")，台獨聯盟改走向美國國會[遊說的溫和路線](../Page/遊說.md "wikilink")。

1979年1月，[中華人民共和國最高領導人](../Page/中華人民共和國最高領導人.md "wikilink")[鄧小平訪問美國](../Page/鄧小平.md "wikilink")，台灣獨立建國聯盟宣傳部部長[張金策擔任](../Page/張金策.md "wikilink")[白宮前](../Page/白宮.md "wikilink")「海外台灣人大示威」總指揮；其後，張金策質疑台灣獨立建國聯盟支持[美國帝國主義立場](../Page/美國帝國主義.md "wikilink")，退出台獨聯盟。

隨著臺灣內部[黨外運動的發展](../Page/黨外運動.md "wikilink")，與政治環境的開放與轉變，臺灣獨立建國聯盟的領導人亦突破[黑名單的限制](../Page/黑名單_\(台灣\).md "wikilink")，發起遷台運動，紛紛[偷渡返台](../Page/偷渡.md "wikilink")，並於1992年將總部遷回臺灣，聯盟領導人[張燦鍙](../Page/張燦鍙.md "wikilink")、[郭倍宏](../Page/郭倍宏.md "wikilink")、[李應元等人亦在此一行動中被捕下獄](../Page/李應元.md "wikilink")。

2016年11月20日，《[台灣蘋果日報](../Page/台灣蘋果日報.md "wikilink")》披露，《[民報](../Page/民報_\(2014年\).md "wikilink")》創辦人[陳永興與](../Page/陳永興.md "wikilink")[台灣國辦公室創辦人](../Page/台灣國辦公室.md "wikilink")[王獻極近期廣邀獨派與本土派人士籌備成立](../Page/王獻極.md "wikilink")「獨派協議平台」，陳永興擔任召集人，預定本年12月10日[國際人權日宣布推動](../Page/國際人權日.md "wikilink")「台灣獨立公投」\[7\]。同月21日，台獨聯盟主席[陳南天表示](../Page/陳南天.md "wikilink")，本日《[台北時報](../Page/台北時報.md "wikilink")》報導稱台獨聯盟為獨派協議平台發起團體之一，此言並非事實，台獨聯盟「僅參與11月6日會前會，並未參與該平台創建」\[8\]。

2018年10月18日，[台灣中社](../Page/台灣中社.md "wikilink")、台獨聯盟與[台灣基督長老教會不滿](../Page/台灣基督長老教會.md "wikilink")[民主進步黨禁止黨職](../Page/民主進步黨.md "wikilink")、公職人員參加[喜樂島聯盟活動](../Page/喜樂島聯盟.md "wikilink")，舉辦記者會，公開支持喜樂島聯盟\[9\]。但2019年2月20日，[台灣憲法學會副秘書長](../Page/台灣憲法學會.md "wikilink")[林青昭批評](../Page/林青昭.md "wikilink")，台獨聯盟與許多台獨支持者以[2020年東京奧運台灣正名公投案未過關](../Page/2020年東京奧運台灣正名公投案.md "wikilink")、「避免親中勢力復辟，方能進一步推展獨立建國工程」等理由，宣布將與喜樂島聯盟劃清界線、公開支持已在2018年10月24日「對台灣獨立建國陣營做出過河拆橋作為」的民進黨\[10\]。

## 現況

目前臺灣獨立建國聯盟總部設在臺灣，另外設有[美國本部](../Page/美國.md "wikilink")、[日本本部](../Page/日本.md "wikilink")、[加拿大本部](../Page/加拿大.md "wikilink")、[歐洲本部及](../Page/歐洲.md "wikilink")[南美本部](../Page/南美.md "wikilink")。現任主席為[陳南天](../Page/陳南天.md "wikilink")。目前較著名的台獨聯盟盟員有前[國安會秘書長](../Page/國安會.md "wikilink")[陳唐山](../Page/陳唐山.md "wikilink")、前[環保署長](../Page/環保署.md "wikilink")[李應元等人](../Page/李應元.md "wikilink")。

## 歷任主席

### 總部主席

  - [蔡同榮](../Page/蔡同榮.md "wikilink")（1970年～1971年）
  - [鄭紹良](../Page/鄭紹良.md "wikilink")（1971年）
  - [彭明敏](../Page/彭明敏.md "wikilink")（1972年）
  - [張燦鍙](../Page/張燦鍙.md "wikilink")（1973年～1987年）
  - [許世楷](../Page/許世楷.md "wikilink")（1987年～1991年）
  - [張燦鍙](../Page/張燦鍙.md "wikilink")（1991年～1995年）
  - [黃昭堂](../Page/黃昭堂.md "wikilink")（1995年～2011年11月17日）
  - [許世楷](../Page/許世楷.md "wikilink")（代理，2011年11月17日～2012年1月）
  - [吳庭和](../Page/吳庭和.md "wikilink")（代理，2012年1月～2012年6月7日）
  - [陳南天](../Page/陳南天.md "wikilink")（2012年6月7日～至今）

### 海外各本部主席

  - 美國本部主席：[吳明基](../Page/吳明基.md "wikilink")
  - 日本本部委員長：[王明理](../Page/王明理.md "wikilink")
  - 加拿大本部主席：張理邦
  - 歐洲本部主席：盧榮-{杰}-
  - 南美本部主席：蕭健次

## 參考文獻

<div class="references-small">

<references>

</references>

</div>

## 和台獨聯盟相關的史料以及研究書目

（按照作者姓氏漢語拼音順序排列）

  - [陳佳宏](../Page/陳佳宏.md "wikilink")，1998，海外台獨運動史：美國「台獨」團體之發展與挑戰，50年代中至90年代中。台北：前衛。
  - 陳銘城，1992，海外台獨運動四十年。台北：[自立晚報](../Page/自立晚報.md "wikilink")。
  - Geoffroy, Claude，1997，台灣獨立運動：起源及1945年以後的發展，黃發典譯。台北：前衛。
  - 黃嘉光、王康陸、陳正修編，1991a，海外台獨運動三十年：[張燦鍙選集](../Page/張燦鍙.md "wikilink")(上)。台北：前衛。
  - 黃嘉光、王康陸、陳正修編，1991b，海外台獨運動三十年：[張燦鍙選集](../Page/張燦鍙.md "wikilink")(下)。台北：前衛。
  - 藍適齊，2002，再討論戰後海外台獨運動相關刊物及「海外台灣人史」。台灣史料研究 18：99-109。
  - 李逢春等，1985，風起雲湧：北美洲台灣獨立運動之發展。Kearny，N.J.：世界台灣獨立聯盟。
  - 李維菁，1993，眾神歸鄉：台獨聯盟遷台後的發展與困境。國立台灣大學新聞學研究所碩士論文。
  - 施正鋒編，2000，台灣獨立建國聯盟的故事。台北：前衛。
  - Shu, Wei-Der. 2002. Who Joined the Clandestine Political
    Organization? Some Preliminary Evidence from the Overseas Taiwan
    Independence Movement. In *Memories of the Future: National Identity
    Issues and the Search for a New Taiwan*, edited by Stephane Corcuff,
    47-69. Armonk, N.Y.: M. E. Sharpe.
  - 宋重陽，1996，台灣獨立運動私記：三十五年之夢。台北：前衛。
  - Wang, Howard. 1997. The Taiwan Independence Movement in the United
    States: A New Identity Revisited. Honors thesis, Department of
    History, University of California at Berkeley.
  - Wang, Mei-ling T. 1999. *The Dust that Never Settles: The Taiwan
    Independence Campaign and U.S.-China Relations*. Lanham, Md.:
    University Press of America.
  - 吳錦暄，1986，「世界台灣獨立聯盟」組織發展與實力分析。中央警官學校警政研究所碩士論文。
  - 許維德， 2001，發自異域的另類聲響：戰後海外台獨運動相關刊物初探。台灣史料研究 17：99-155。
  - [莊秋雄](../Page/莊秋雄.md "wikilink")，1994，海外遊子台獨夢，再版。台北：前衛。
  - 莊秋雄，2002，海外遊子台獨夢續。台北：前衛。

## 参见

  - [台灣獨立運動](../Page/台灣獨立運動.md "wikilink")
  - [共和國 (雜誌)](../Page/共和國_\(雜誌\).md "wikilink")

## 外部連結

  - [台灣獨立建國聯盟網站/World United Formosans for
    Independence](http://www.wufi.org.tw/)

  - [台灣演義](../Page/台灣演義.md "wikilink"), 2010/03/14 -
    FAPA與[台獨聯盟](../Page/台獨聯盟.md "wikilink")：[(1/5)](http://www.youtube.com/watch?v=1nJuidiL6r8)、[(2/5)](http://www.youtube.com/watch?v=Zs7ijKD54Y4)、[(3/5)](http://www.youtube.com/watch?v=ylvIp6Y07dg)、[(4/5)](http://www.youtube.com/watch?v=gXCLAlJKFZ0)、[(5/5)](http://www.youtube.com/watch?v=jyiNtzm9NNo)

  - [台灣演義](../Page/台灣演義.md "wikilink"), 2011/05/15 -
    台灣獨立建國聯盟：[(1/3)](http://www.youtube.com/watch?v=qcnxO1biGg8)、[(2/3)](http://www.youtube.com/watch?v=kTIW5wS6zKU)、[(3/3)](http://www.youtube.com/watch?v=KL7GQM_HKd4)

  -
{{-}}

[Category:台灣獨立運動組織](../Category/台灣獨立運動組織.md "wikilink")
[Category:臺灣反共組織](../Category/臺灣反共組織.md "wikilink")

1.  [全美台灣獨立聯盟的結盟](http://www.wufi.org.tw/%E5%85%A8%E7%BE%8E%E5%8F%B0%E7%81%A3%E7%8D%A8%E7%AB%8B%E8%81%AF%E7%9B%9F%E7%9A%84%E7%B5%90%E7%9B%9F/)
    台灣獨立建國聯盟
2.
3.  《台灣獨立建國聯盟的故事》，[施正鋒編](../Page/施正鋒.md "wikilink")，[前衛出版社出版](../Page/前衛出版社.md "wikilink")，2000年，頁47
4.
5.  [鄭欣先生訪問紀錄](http://www.mh.sinica.edu.tw/MHDocument/Publication/Publication_674.pdf),
    《海外台獨運動相關人物口述史 續篇》,
    [中央研究院近代史研究所](../Page/中央研究院近代史研究所.md "wikilink"),
    2012年
6.  [海外運動紀事](http://da.lib.nccu.edu.tw/tdm/?m=2302&wsn=0501),
    [國立政治大學](../Page/國立政治大學.md "wikilink")
7.
8.
9.
10.