**區域鐵路**（Regional
Rail），在中华人民共和国也称为**城际轨道交通**\[1\]，指一个区域内中等距离的铁路，包括客运及货运。\[2\]与[城际列车相比](../Page/城际列车.md "wikilink")，区域铁路停靠站較多、速度較低、有区域限制；但与[通勤铁路相比](../Page/通勤铁路.md "wikilink")，区域铁路覆盖的面积往往更大，也不会每站必停，是介于城际列车与通勤铁路之间、中等距离的铁路运输形式。有时也被当作**[通勤鐵路](../Page/通勤鐵路.md "wikilink")**的同近义词使用。

## 列車

區域鐵路的列車多數都會設法增加載客量，但因為行走距離有時會比[地下鐵路等長](../Page/地鐵.md "wikilink")，所以一般都不會令乘客感覺太過擠迫。大部份的區域鐵路列車都不會有餐車等屬於長途車的裝備。區域鐵路一般的行走距離為15至180[公里](../Page/公里.md "wikilink")，運行速度由55至175km/h不等。[車廂可能是單層或雙層](../Page/鐵路車輛#客車.md "wikilink")，常見的單層車廂的載客量約80至110人，雙層的約145至170人。

## 特點

區域鐵路與[城際鐵路不同的是](../Page/城际铁路.md "wikilink")，前者會停在絕大多數，甚至所有的沿途車站。區域鐵路提供沿線的較小社區之間的服務，以及與長途交通的接軌服務。类似的服务有如英国铁路的[Regional
Railways](../Page/Regional_Railways.md "wikilink")，法国的[法国省际列车](../Page/法国省际列车.md "wikilink")（*Transport
express
régional*），德国的[德铁区域运输](../Page/德铁区域运输.md "wikilink")，以及韩国的[統一號](../Page/統一號.md "wikilink")。

美国的铁路客运并不发达，所以缺少此类服务，在使用该词时往往将之与通勤铁路混用。

大部分的區域鐵路都是[重鐵](../Page/重型鐵路系統.md "wikilink")，使用跟[国铁相同或相近的标准](../Page/国铁.md "wikilink")\[3\]。

跟[輕軌運輸或](../Page/輕軌運輸.md "wikilink")[城市軌道交通系統](../Page/城市軌道交通系統.md "wikilink")（通常是地下鐵路及其延伸）相比，區域鐵路主要分別是：\[4\]

  - 使用的車輛體積較大
  - 多數班次較稀疏
  - 有特定的抵發時間表（列車在指定的時刻開車，而不是每隔一段時間開出）
  - 服務地區的人口密度一般較低
  - 跟城市間的市際列車，或貨車使用同一[路軌](../Page/鐵軌.md "wikilink")

因為區域鐵路可以使用原有的市際列車或貨運鐵路的路軌，建造成本得以大為降低。但亦有部分區域鐵路為避免與其它列車爭路而造成誤點，而特意建造獨立的路軌。

區域鐵路通常採用當地的既有铁路线的[軌距](../Page/軌距.md "wikilink")。例如在英、美為 1435 毫米（4 呎 8½
吋）[標準軌距](../Page/標準軌距.md "wikilink")，在[日本及](../Page/日本.md "wikilink")[瑞士為](../Page/瑞士.md "wikilink")
1067 毫米[窄軌](../Page/窄軌.md "wikilink")。

某些[歐洲國家由於城市間的距離短](../Page/歐洲.md "wikilink")，使到市際列車服務及區域鐵路的分野亦變得模糊。例如[荷蘭及](../Page/荷蘭.md "wikilink")[比利時的市際列車運載有很多通勤上班人士](../Page/比利時.md "wikilink")，它們使用的車輛、行車距離及班次亦接近其它國家的區域鐵路。

在[北美洲](../Page/北美洲.md "wikilink")，區域鐵路鐵路通常是由政府或半政府機構營運。

## 車輛

區域鐵路最常見使用的是[柴油動車組](../Page/柴聯車.md "wikilink")、或[電力動車組](../Page/電力動車組.md "wikilink")。有些地方亦會使用[機車牽引](../Page/鐵路機車.md "wikilink")[車廂行走](../Page/鐵路客車.md "wikilink")。使用機車牽引時，區域鐵路差不多全都會使用[推拉操作](../Page/推拉操作.md "wikilink")，往一方向行駛時由機車在前拉；往另一方向則由機車在後推，由司機在另一端附有駕駛室的客車廂遙控機車。日本則以[動車組為主流](../Page/動車組.md "wikilink")，与[通勤列车相对](../Page/通勤列车.md "wikilink")，往往称之为“近郊型列车”。

## 各地的区域铁路

### 中國大陸

中国的[城际轨道交通属于区域铁路](../Page/城际轨道交通.md "wikilink")，是[城市群内各城市间的客运轨道交通系统](../Page/城市群.md "wikilink")。\[5\]

2005年3月16日，中国国务院审议并原则通过《环渤海京津冀地区、长江三角洲地区、珠江三角洲地区城际轨道交通网规划》\[6\]，以此为起点，中国的城际轨道交通发展进入了一个极好的时期。此后，中国国家发改委先后批准了《珠江三角洲地区城际轨道交通网规划(2009年修订)》\[7\]、《中原城市群城际轨道交通网规划(2009-2020)》\[8\]、《武汉城市圈城际轨道交通网规划(2009-2020年)》\[9\]、《长株潭城市群城际轨道交通网规划（2009-2020年）》\[10\]、《环渤海地区山东半岛城市群城际轨道交通网规划（2011-2020年）》\[11\]，为中国城市群城际轨道交通的发展绘制了蓝图。

#### 已开通

<table>
<thead>
<tr class="header">
<th><p><strong>服务中心城市</strong></p></th>
<th><p><strong>线路</strong></p></th>
<th><p><strong>运营长度（含在建）<small>（km）</small></strong></p></th>
<th><p><strong>运营车站数（含在建车站）</strong></p></th>
<th><p><strong>站间距<small>（km）</small></strong></p></th>
<th><p><strong>设计速度<small>（km/h）</small></strong></p></th>
<th><p><strong>铁路线</strong></p></th>
<th><p><strong>（预计）通车日期</strong><br />
<small>（年/月/日）</small></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>广州市、深圳市</p></td>
<td><p><a href="../Page/广深动车组列车.md" title="wikilink">广深线</a></p></td>
<td><p>147</p></td>
<td><p>6</p></td>
<td><p>24.5</p></td>
<td><p>200</p></td>
<td><p><a href="../Page/廣深鐵路.md" title="wikilink">廣深鐵路</a></p></td>
<td><p>1959; 2007/4/18</p></td>
</tr>
<tr class="even">
<td><p>北京市、天津市</p></td>
<td><p><a href="../Page/京津城际铁路.md" title="wikilink">京津城际铁路</a></p></td>
<td><p>117</p></td>
<td><p>5</p></td>
<td><p>23.39</p></td>
<td><p>350</p></td>
<td><p>--</p></td>
<td><p>2008/8/1</p></td>
</tr>
<tr class="odd">
<td><p>沈阳市、抚顺市</p></td>
<td><p><a href="../Page/沈抚城际铁路.md" title="wikilink">沈抚城际铁路</a></p></td>
<td><p>61</p></td>
<td><p>8</p></td>
<td><p>8</p></td>
<td><p>120</p></td>
<td><p>--</p></td>
<td><p>2009/7/30</p></td>
</tr>
<tr class="even">
<td><p>上海市、南京市</p></td>
<td><p><a href="../Page/沪宁城际铁路.md" title="wikilink">沪宁城际铁路</a></p></td>
<td><p>301</p></td>
<td><p>31</p></td>
<td><p>9.71</p></td>
<td><p>300</p></td>
<td><p>--</p></td>
<td><p>2010/7/1</p></td>
</tr>
<tr class="odd">
<td><p>长春市、吉林市</p></td>
<td><p><a href="../Page/长吉城际铁路.md" title="wikilink">长吉城际铁路</a></p></td>
<td><p>111</p></td>
<td><p>7</p></td>
<td><p>15.56</p></td>
<td><p>250</p></td>
<td><p>--</p></td>
<td><p>2011/1/11</p></td>
</tr>
<tr class="even">
<td><p>成都市、重庆市</p></td>
<td><p><a href="../Page/成渝客运专线.md" title="wikilink">成渝客运专线</a></p></td>
<td><p>307</p></td>
<td><p>12</p></td>
<td><p>25.58</p></td>
<td><p>300</p></td>
<td><p>--</p></td>
<td><p>2014</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### 兴建中

<table>
<thead>
<tr class="header">
<th><p><strong>服务中心城市</strong></p></th>
<th><p><strong>线路</strong></p></th>
<th><p><strong>运营长度（含在建）<small>（km）</small></strong></p></th>
<th><p><strong>运营车站数（含在建车站）</strong></p></th>
<th><p><strong>设计速度<small>（km/h）</small></strong></p></th>
<th><p><strong>铁路线</strong></p></th>
<th><p><strong>（预计）通车日期</strong><br />
<span style="font-size:smaller;">（年/月/日）</span></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>郑州市</p></td>
<td><p><a href="../Page/郑许城际铁路.md" title="wikilink">郑州-新郑机场-许昌城际铁路</a></p></td>
<td><p>115.5</p></td>
<td><p>16</p></td>
<td><p>200</p></td>
<td><p><a href="../Page/郑许城际铁路.md" title="wikilink">郑许城际铁路</a></p></td>
<td><p>不详</p></td>
</tr>
<tr class="even">
<td><p>广州市等</p></td>
<td><p><a href="../Page/广佛环线.md" title="wikilink">广佛环线</a></p></td>
<td><p>34.971</p></td>
<td><p>6</p></td>
<td><p>200</p></td>
<td><p>--</p></td>
<td><p>2016</p></td>
</tr>
<tr class="odd">
<td><p>广州市等</p></td>
<td><p><a href="../Page/穗莞深城际轨道交通.md" title="wikilink">穗莞深城际轨道交通</a></p></td>
<td><p>待定</p></td>
<td><p>待定</p></td>
<td><p>140</p></td>
<td><p>--</p></td>
<td><p>2016</p></td>
</tr>
<tr class="even">
<td><p>东莞市、惠州市</p></td>
<td><p><a href="../Page/莞惠城际轨道交通.md" title="wikilink">莞惠城际轨道交通</a></p></td>
<td><p>99.841</p></td>
<td><p>18</p></td>
<td><p>200</p></td>
<td><p>莞惠城际铁路 /<br />
莞惠城际动车</p></td>
<td><p>2016年3月30日<br />
（部分開通：其中的10個站台）</p></td>
</tr>
<tr class="odd">
<td><p>广州市</p></td>
<td><p><a href="../Page/广佛肇城际轨道交通.md" title="wikilink">广佛肇城际轨道交通</a></p></td>
<td><p>86.82</p></td>
<td><p>16</p></td>
<td><p>200</p></td>
<td><p>--</p></td>
<td><p>2016</p></td>
</tr>
<tr class="even">
<td><p>天津市</p></td>
<td><p><a href="../Page/津滨城际铁路.md" title="wikilink">津滨城际铁路</a></p></td>
<td><p>44.815</p></td>
<td><p>6</p></td>
<td><p>350</p></td>
<td><p>--</p></td>
<td><p>2014</p></td>
</tr>
<tr class="odd">
<td><p>南京市、杭州市</p></td>
<td><p><a href="../Page/宁杭客运专线.md" title="wikilink">宁杭客运专线</a></p></td>
<td><p>249</p></td>
<td><p>13</p></td>
<td><p>350</p></td>
<td><p>--</p></td>
<td><p>2013</p></td>
</tr>
<tr class="even">
<td><p>张家口市、大同市</p></td>
<td><p><a href="../Page/张大城际铁路.md" title="wikilink">张大城际铁路</a></p></td>
<td><p>161</p></td>
<td><p>6</p></td>
<td><p>250</p></td>
<td><p>--</p></td>
<td><p>未定</p></td>
</tr>
<tr class="odd">
<td><p>沈阳市、丹东市</p></td>
<td><p><a href="../Page/沈丹客运专线.md" title="wikilink">沈丹客运专线</a></p></td>
<td><p>224</p></td>
<td><p>8</p></td>
<td><p>250</p></td>
<td><p>--</p></td>
<td><p>2015</p></td>
</tr>
<tr class="even">
<td><p>柳州市、来宾市、南宁市</p></td>
<td><p><a href="../Page/柳南城际铁路.md" title="wikilink">柳南城际铁路</a></p></td>
<td><p>226</p></td>
<td><p>9</p></td>
<td><p>250</p></td>
<td><p>--</p></td>
<td><p>2013</p></td>
</tr>
<tr class="odd">
<td><p>北京市、张家口市</p></td>
<td><p><a href="../Page/京张城际铁路.md" title="wikilink">京张城际铁路</a></p></td>
<td><p>173.947</p></td>
<td><p>7</p></td>
<td><p>250</p></td>
<td><p>--</p></td>
<td><p>未定</p></td>
</tr>
<tr class="even">
<td><p>天津市、保定市</p></td>
<td><p><a href="../Page/津保铁路.md" title="wikilink">津保铁路</a></p></td>
<td><p>158</p></td>
<td><p>7</p></td>
<td><p>250</p></td>
<td><p>--</p></td>
<td><p>2013</p></td>
</tr>
<tr class="odd">
<td><p>丹东市、大连市</p></td>
<td><p><a href="../Page/丹大城际铁路.md" title="wikilink">丹大城际铁路</a></p></td>
<td><p>289</p></td>
<td><p>18</p></td>
<td><p>200</p></td>
<td><p>--</p></td>
<td><p>未定</p></td>
</tr>
<tr class="even">
<td><p>成都市等</p></td>
<td><p><a href="../Page/成绵乐客运专线.md" title="wikilink">成绵乐客运专线</a></p></td>
<td><p>318.4</p></td>
<td><p>22</p></td>
<td><p>250</p></td>
<td><p>--</p></td>
<td><p>2013</p></td>
</tr>
<tr class="odd">
<td><p>长沙市等</p></td>
<td><p><a href="../Page/长益常城际铁路.md" title="wikilink">长益常城际铁路</a></p></td>
<td><p>168.7</p></td>
<td><p>14</p></td>
<td><p>不详</p></td>
<td><p>--</p></td>
<td><p>未定</p></td>
</tr>
</tbody>
</table>

### 中華民國

[TRA_EMU707_at_Xike_Station_20071231.jpg](https://zh.wikipedia.org/wiki/File:TRA_EMU707_at_Xike_Station_20071231.jpg "fig:TRA_EMU707_at_Xike_Station_20071231.jpg")

  - [台鐵](../Page/台鐵.md "wikilink")\[12\]
      - [西部幹線各](../Page/西部幹線.md "wikilink")[捷運化區間](../Page/臺鐵捷運化.md "wikilink")：
          - [基隆](../Page/基隆車站.md "wikilink")—[中壢](../Page/中壢車站.md "wikilink")
          - [豐原](../Page/豐原車站.md "wikilink")—[二水](../Page/二水車站.md "wikilink")
          - [斗六](../Page/斗六車站.md "wikilink")—[潮州](../Page/潮州車站.md "wikilink")
  - [桃園機場捷運](../Page/桃園機場捷運.md "wikilink")

### 日本

在日本，区域铁路被称作「中距離電車」。

#### [JR線](../Page/JR.md "wikilink")

##### [近畿圈](../Page/近畿地方.md "wikilink")（京阪神）

  - 東海道本線、山陽本線（JR京都線、JR神戶線）
  - JR東西線、片町線（學研都市線）
  - 福知山線（JR寶塚線）
  - 阪和線

##### [首都圈區域](../Page/關東地方.md "wikilink")

  - [東海道](../Page/東海道本線.md "wikilink")、[東北本線](../Page/東北本線.md "wikilink")（[京濱東北線](../Page/京濱東北線.md "wikilink")、[根岸線](../Page/根岸線.md "wikilink")、[上野東京線](../Page/上野東京線.md "wikilink")）
  - [横濱線](../Page/横濱線.md "wikilink")
  - [南武線](../Page/南武線.md "wikilink")
  - [武藏野線](../Page/武藏野線.md "wikilink")
  - [赤羽線](../Page/赤羽線.md "wikilink")（[埼京線](../Page/埼京線.md "wikilink")）
  - [中央本線](../Page/中央本線.md "wikilink")（[中央線（快速）](../Page/中央快速線.md "wikilink")、[中央、總武線（各站停車）](../Page/中央、總武緩行線.md "wikilink")、直通東京地下鐵東西線）
  - [總武線（快速）](../Page/總武快速線.md "wikilink")、[橫須賀線](../Page/橫須賀線.md "wikilink")
  - [京葉線](../Page/京葉線.md "wikilink")
  - [鶴見線](../Page/鶴見線.md "wikilink")
  - [常磐線](../Page/常磐線.md "wikilink")（快速線、緩行線、直通東京地下鐵千代田線）、[水戶線](../Page/水戶線.md "wikilink")
    （部份班次為近郊型電車）
  - [成田線](../Page/成田線.md "wikilink")（常磐快速線直通）
  - [相模線](../Page/相模線.md "wikilink")
  - [湘南新宿線](../Page/湘南新宿線.md "wikilink")
      - 湘南新宿線，常磐線部分區間和橫須賀線雖使用近郊型電車，但路線卻有中距離通勤功用。

#### 非JR業者

  - 日本所有地下鐵線 (部分線與JR或私鐵直通運轉)
  - 16所[大手私鐵中](../Page/大手私鐵.md "wikilink")，大部份公司的全線區間。（除近鐵的 762 毫米軌距區間）
      - 當中[近畿日本鐵道](../Page/近畿日本鐵道.md "wikilink")、[京濱急行電鐵](../Page/京濱急行電鐵.md "wikilink")、[京成電鐵](../Page/京成電鐵.md "wikilink")、[京阪電氣鐵道](../Page/京阪電氣鐵道.md "wikilink")、[南海電氣鐵道](../Page/南海電氣鐵道.md "wikilink")、[小田急電鐵](../Page/小田急電鐵.md "wikilink")、[東武鐵道](../Page/東武鐵道.md "wikilink")、[西日本鐵道](../Page/西日本鐵道.md "wikilink")、[名古屋鐵道有](../Page/名古屋鐵道.md "wikilink")[優等列車於通勤線運行](../Page/優等列車.md "wikilink")。

其他私鐵及[第三部門鐵道](../Page/第三部門.md "wikilink") :

  - [東京臨海高速鐵道臨海線](../Page/東京臨海高速鐵道.md "wikilink") (與JR埼京線直通運轉)
  - [筑波快線](../Page/首都圈新都市鐵道筑波快線.md "wikilink")
  - [東葉](../Page/東葉高速線.md "wikilink")、泉北、[埼玉高速鐵道](../Page/埼玉高速鐵道.md "wikilink")
  - [山陽電鐵](../Page/山陽電鐵.md "wikilink")
  - [新京成電鐵](../Page/新京成電鐵.md "wikilink")
  - [北总铁道](../Page/北总铁道.md "wikilink")
  - [IGR岩手銀河鐵道](../Page/IGR岩手銀河鐵道.md "wikilink")、[青森鐵道](../Page/青森鐵道.md "wikilink")
    (兩者時常直通運轉)

### 韓國

  - [首爾](../Page/首爾.md "wikilink")[首都圈電鐵全區間](../Page/韓國首都圈電鐵.md "wikilink")，包括[KORAIL直通區間](../Page/韓國鐵道公社.md "wikilink")。

### 法国

[Gare_de_Lyon_zCRW_1256.jpg](https://zh.wikipedia.org/wiki/File:Gare_de_Lyon_zCRW_1256.jpg "fig:Gare_de_Lyon_zCRW_1256.jpg")

  - [巴黎](../Page/巴黎.md "wikilink")[RER](../Page/區域快鐵.md "wikilink")

<!-- end list -->

  - [法兰西岛](../Page/法兰西岛.md "wikilink")[Transilien](../Page/法兰西岛区域铁路.md "wikilink")

<!-- end list -->

  - [法国其它地区](../Page/大区_\(法国\).md "wikilink")[TER](../Page/法国大区列车.md "wikilink")

### 西班牙

  - [西班牙近郊鐵路](../Page/西班牙近郊鐵路.md "wikilink")（包含12個都市營運系統）

## 注释

## 参考文献

## 參見

  - [城际铁路](../Page/城际铁路.md "wikilink")

  - [S-Bahn](../Page/城市快铁.md "wikilink")

  - [城市軌道交通系統](../Page/城市軌道交通系統.md "wikilink")

  - [地鐵](../Page/地鐵.md "wikilink")

  - [捷運](../Page/捷運.md "wikilink")

  - [輕軌運輸](../Page/輕軌運輸.md "wikilink")

  - （[倫敦地鐵](../Page/倫敦地鐵.md "wikilink")[區域線前身](../Page/倫敦地鐵區域線.md "wikilink")）

{{-}}

[Category:鐵路](../Category/鐵路.md "wikilink")
[Category:公共交通](../Category/公共交通.md "wikilink")

1.
2.  [省级区域铁路货运联系的系统研究](http://www.cnki.com.cn/Article/CJFDTotal-DLKX199101002.htm)
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.