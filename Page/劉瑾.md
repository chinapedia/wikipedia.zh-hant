**劉瑾**（），[陝西](../Page/陝西.md "wikilink")[興平人](../Page/興平.md "wikilink")。[中國](../Page/中國.md "wikilink")[明代中期](../Page/明代.md "wikilink")，[明武宗时期的重要](../Page/明武宗.md "wikilink")[宦官](../Page/宦官.md "wikilink")。

## 生平

據史書所記，劉瑾本姓[谈](../Page/谈姓.md "wikilink")，[景泰年間](../Page/景泰_\(年号\).md "wikilink")[自宮](../Page/宮刑.md "wikilink")，並成為劉太監外間的手下，並改[劉姓](../Page/劉姓.md "wikilink")。[成化年間入宮](../Page/成化.md "wikilink")。[弘治年間執掌](../Page/弘治_\(明朝\).md "wikilink")[茂陵司香](../Page/茂陵.md "wikilink")，其後調往東宮。由於經常在太子面前[演戲](../Page/演戲.md "wikilink")，深得太子所寵愛。

弘治十八年（1505年），[明孝宗駕崩](../Page/明孝宗.md "wikilink")，太子朱厚照[即位](../Page/即位.md "wikilink")，是為[明武宗](../Page/明武宗.md "wikilink")（正德皇帝）。[刘瑾和](../Page/刘瑾.md "wikilink")[馬永成](../Page/馬永成_\(明朝\).md "wikilink")、[高鳳](../Page/高鳳.md "wikilink")、[羅祥](../Page/羅祥.md "wikilink")、[魏彬](../Page/魏彬.md "wikilink")、[丘聚](../Page/丘聚.md "wikilink")、[谷大用](../Page/谷大用.md "wikilink")、[張永被稱為](../Page/张永_\(明朝\).md "wikilink")“[八虎](../Page/八虎.md "wikilink")”。

[正德初年设立內行廠](../Page/正德_\(明武宗\).md "wikilink")。正德五年（1510年），安化王[朱寘鐇以討伐劉瑾為名](../Page/朱寘鐇.md "wikilink")，在[寧夏起兵叛亂](../Page/寧夏.md "wikilink")，明武宗用[楊一清平亂](../Page/楊一清.md "wikilink")。楊一清到[燕京獻俘](../Page/燕京.md "wikilink")，趁機拉攏張永，密奏劉瑾謀反\[1\]。武宗派禁軍抄劉瑾家，按[王鏊](../Page/王鏊.md "wikilink")《[震澤長語](../Page/s:震澤長語/雜論.md "wikilink")》僅抄出的金銀就有：黃金一千兩百零五萬兩，白銀兩亿五千九百五十八萬兩\[2\]。2001年《[亞洲華爾街日報](../Page/亞洲華爾街日報.md "wikilink")》據此將劉瑾列入過去1000年來，全球最富有的50人名單\[3\]。另據清[趙翼](../Page/趙翼.md "wikilink")《[二十二史劄記](../Page/二十二史劄記.md "wikilink")》所載，劉瑾有黃金250萬兩，白銀5000餘萬兩。按[張瀚](../Page/張瀚_\(明朝\).md "wikilink")《[皇明疏議輯略](../Page/皇明疏議輯略.md "wikilink")》卷三十二〈議獄〉，在其本家搜出「金銀數百萬」\[4\]。同年西曆八月二十五日，六十歲的劉瑾，依法被判[凌遲](../Page/凌遲.md "wikilink")3357刀處死\[5\]。據野史記載，劉瑾的肉之後還被一文錢一兩售賣，而坊間亦大肆搶購。[邓之诚](../Page/邓之诚.md "wikilink")《骨董续记》卷二“寸磔”条云：“世俗言明代寸磔之刑，刘瑾四千二百刀，[郑鄤三千六百刀](../Page/郑鄤.md "wikilink")」。

## 逸闻

[刘瑾雖貪婪專權](../Page/刘瑾.md "wikilink")，但頗有政治才能，也从未将国事当做儿戏。史载，劉瑾将奏章带回私第后，都与他的妹婿礼部司务[孙聪及华亭人](../Page/孙聪.md "wikilink")[张文冕商量参决](../Page/张文冕.md "wikilink")，再由大学士[焦芳润色](../Page/焦芳.md "wikilink")，内阁[李东阳审核之后颁发](../Page/李东阳.md "wikilink")，还是颇为慎重的。用事期间，他针对时弊，对政治制度作了不少改动，推行过一些新法。即所谓“劉瑾变法”\[6\]\[7\]\[8\]。一說劉瑾於正德三年八月创罚米法，“自是忤瑾者，悉诬以旧事，入之罚米例中，中外文武无宁日矣”\[9\]。

## 影視作品

  - [王偉](../Page/王偉.md "wikilink")：1978年[麗的電視](../Page/麗的電視.md "wikilink")[劇集](../Page/劇集.md "wikilink")《[天龍訣](../Page/天龍訣.md "wikilink")》
  - [羅烈](../Page/羅烈.md "wikilink")：1993年[亞洲電視](../Page/亞洲電視.md "wikilink")[劇集](../Page/劇集.md "wikilink")《[天蠶變之再與天比高](../Page/天蠶變之再與天比高.md "wikilink")》
  - [张会中](../Page/张会中.md "wikilink")：2000年中国大陆、台湾合拍半喜剧电视剧《[绝色双娇](../Page/绝色双娇.md "wikilink")》
  - [秦焰](../Page/秦焰.md "wikilink")：2005年大明帝国之夜来风雨
  - [張國強](../Page/張國強_\(香港\).md "wikilink")：2009年[香港](../Page/香港.md "wikilink")[TVB電視劇](../Page/TVB.md "wikilink")：《[王老虎搶親](../Page/王老虎搶親_\(電視劇\).md "wikilink")》
  - [张野](../Page/张野.md "wikilink"):2018年[优酷视频](../Page/优酷视频.md "wikilink")[古装剧](../Page/古装剧.md "wikilink"):《[回到明朝当王爷之杨凌传](../Page/回到明朝当王爷之杨凌传.md "wikilink")》

## 參考資料

  - [谷應泰](../Page/谷應泰.md "wikilink")，“劉瑾用事”，《[明史紀事本末](../Page/明史紀事本末.md "wikilink")·六》卷四十三。
  - [谷應泰](../Page/谷應泰.md "wikilink")，“宸濠之叛”，《明史紀事本末·七》卷四十七。

## 注釋

<div class="references-small">

<references />

</div>

## 参见

  - [汪直擅政](../Page/汪直擅政.md "wikilink")
  - [王振擅政](../Page/王振擅政.md "wikilink")
  - [魏忠贤](../Page/魏忠贤.md "wikilink")

[Category:1451年出生](../Category/1451年出生.md "wikilink")
[Category:明朝宦官](../Category/明朝宦官.md "wikilink")
[Category:明朝被處決者](../Category/明朝被處決者.md "wikilink")
[Category:兴平人](../Category/兴平人.md "wikilink")
[J瑾](../Category/劉姓.md "wikilink")
[Category:谈姓](../Category/谈姓.md "wikilink")
[Category:死于凌迟](../Category/死于凌迟.md "wikilink")
[Category:明朝死于凌迟者](../Category/明朝死于凌迟者.md "wikilink")

1.  『明史・卷一百二十一・列傳第九、公主』「（英宗八女，第三）淳安公主，成化二年下嫁蔡震。震行醇謹。正德中，劉瑾下獄，詔廷訊。有問者，瑾輒指其人附己，廷臣無敢詰。震厲聲曰：「我皇家至戚，應不附爾」。趣獄卒考掠之，瑾乃服罪，以是知名。嘉靖中卒，贈太保，謚康僖。」
2.  此為時人王鏊所估，多所引用，但後來一般認為誇大。見
3.  [Asian Wall Street Journal article that mentions Liu
    Jin](http://interactive.wsj.com/public/resources/documents/mill-1-timeline.htm)
4.
5.  邓之诚《骨董续记》卷二“寸磔”条云：“世俗言明代寸磔之刑，刘瑾四千二百刀，[郑鄤三千六百刀](../Page/郑鄤.md "wikilink")。李慈铭日记亦言之。”
6.  据《[明史](../Page/明史.md "wikilink")》，“给事中屈铨、祭酒王云凤请编瑾行事，著为律令。”
7.  据《[明通鉴](../Page/明通鉴.md "wikilink")·武宗》“‘辛丑，兵科给事中屈拴，请颁行劉瑾所定《见行事例》，按[六部为序](../Page/六部.md "wikilink")，编集成书，颁布中外，以昭法守。诏‘下廷臣议行。’”
8.  《明史·列传192》，“廷臣奏瑾所变法，吏部二十四事，户部三十余事，兵部十八事，工部十三事，诏悉厘正如旧制。”
9.  [夏燮](../Page/夏燮.md "wikilink")《明通鉴》卷42，但《明通鉴》于“正德二年六月”条下记载：“时（刘）瑾憾（杨）一清不附己，劾其破冒边费，故有是诏。未几，复逮一清下锦衣卫狱，大学士李东阳、王鏊论救，乃得释。未几，仍摭他事，先后罚米六百石。”，早在正德二年即有罰米法了。