[KDevelop.png](https://zh.wikipedia.org/wiki/File:KDevelop.png "fig:KDevelop.png")

**KDevelop**，是一个支持多[程序设计语言的](../Page/程序设计语言.md "wikilink")[集成开发环境](../Page/集成开发环境.md "wikilink")。它运行于[Linux和其它](../Page/Linux.md "wikilink")[类Unix环境](../Page/类Unix.md "wikilink")。它本身不包含[编译器](../Page/编译器.md "wikilink")，而是调用其它编译器来编译程序。

KDevelop
3.5.5已支持[Ada](../Page/Ada.md "wikilink")、[Bash](../Page/Bash.md "wikilink")、[C](../Page/C语言.md "wikilink")、[C++](../Page/C++.md "wikilink")、[Fortran](../Page/Fortran.md "wikilink")、[Haskell](../Page/Haskell.md "wikilink")、[Java](../Page/Java.md "wikilink")、[Pascal](../Page/Pascal_\(程式語言\).md "wikilink")、[Perl](../Page/Perl.md "wikilink")、[Python](../Page/Python.md "wikilink")、[Ruby](../Page/Ruby.md "wikilink")、[SQL等](../Page/SQL.md "wikilink")[程序设计语言](../Page/程序设计语言.md "wikilink")。

## 歷史

## 特性

KDevelop透過[KParts框架使用编辑器组件](../Page/KParts.md "wikilink")。預設使用[Kate](../Page/Kate.md "wikilink")。下面是KDevelop的一些特性。有关编辑器组件的特性，可以查看[Kate相关介绍](../Page/Kate.md "wikilink")。

  - 源代码编辑器具有语法高亮和自动缩进的功能（继承自Kate_Part）。
  - 项目管理器可以管理各种不同的项目类型，例如[Automake](../Page/Automake.md "wikilink")、基于[Qt的](../Page/Qt.md "wikilink")[qmake项目和基于](../Page/qmake.md "wikilink")[Java的](../Page/Java.md "wikilink")[Ant项目](../Page/Ant.md "wikilink")。
  - 类浏览器。
  - GUI设计器。
  - GNU编译器套件的前端。
  - GNU调试器的前端。
  - 生成和升级类定义以及应用程序框架的向导。
  - 自动代码补全（C/C++）。
  - 内置[Doxygen支持](../Page/Doxygen.md "wikilink")。
  - 并行版本控制（也被称作软件配置管理）系统支持。支持[CVS](../Page/CVS.md "wikilink")、[Subversion](../Page/Subversion.md "wikilink")、[Perforce和](../Page/Perforce.md "wikilink")[ClearCase](../Page/ClearCase.md "wikilink")。

KDevelop
3是一个完全基于插件的架构。当一个开发人员做了一个改动，他只需要重新编译这个插件。这样就可以同时保存几个配置文件，每个配置文件中可以指定加载不同的插件。KDevelop是和编程语言以及构建系统无关的，支持[KDE](../Page/KDE.md "wikilink")、[GNOME和例如](../Page/GNOME.md "wikilink")[Qt](../Page/Qt.md "wikilink")、[GTK+和](../Page/GTK+.md "wikilink")[wxWidgets的其它一些技术](../Page/wxWidgets.md "wikilink")。

KDevelop支持多种不同语言，其中包括[C](../Page/C语言.md "wikilink")、[C++](../Page/C++.md "wikilink")、[Perl](../Page/Perl.md "wikilink")、[Python](../Page/Python.md "wikilink")、[PHP](../Page/PHP.md "wikilink")、[Java](../Page/Java.md "wikilink")、[Fortran](../Page/Fortran.md "wikilink")、[Ruby](../Page/Ruby.md "wikilink")、[Ada](../Page/Ada.md "wikilink")、[Pascal](../Page/Pascal.md "wikilink")、[SQL和](../Page/SQL.md "wikilink")[Bash脚本](../Page/Bash.md "wikilink")。支持的构建系统包括GNU（automake）、cmake、qmake和自定义项目的make（如果您想使用自己的Makefile，KDevelop不会破坏它们的）以及不需要Makefile的脚本项目。

对于C和C++，KDevelop提供代码补全功能。符号都被保存到一个Berkeley
DB文件数据库中，这样可以快速查找，而不用重新解析。KDevelop还提供了一个开发人员框架，它可以帮助大家为其它编程语言编写新的解析器。

集成的调试器可以让您图形化地完成所有有关断点和回溯的调试，它甚至可以在您动态加载插件的时候一样工作，这点和命令行的gdb不同。

快速打开可以让您在文件中快速导航。

现在，对于这个集成开发环境已经有50到100个可用的插件。其中主要包括项目范围内的持续代码标签；快速展开代码片段；使用格式化工具按照一种风格重新编排您的代码格式；正则表达式搜索以及在重构代码时允许莫名在项目范围进行搜索／替换。

## 外部链接

  - [KDevelop主页](http://www.kdevelop.org/)
  - [KDev3 KDev4 comparison
    table](https://web.archive.org/web/20101030071010/http://www.kdevelop.org/mediawiki/index.php/KDevelop_4/KDev3_KDev4_comparison_table)
  - [KDevelop介绍（简体中文）](http://www.kdecn.org/kdesoft/kde3/kdevelop.php)

[Category:KDE](../Category/KDE.md "wikilink")
[Category:集成开发环境](../Category/集成开发环境.md "wikilink")
[Category:自由整合開發環境](../Category/自由整合開發環境.md "wikilink")
[Category:Linux集成开发环境](../Category/Linux集成开发环境.md "wikilink")