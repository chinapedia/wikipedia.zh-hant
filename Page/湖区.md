**湖区**（**Lake
District**）是[英格兰西北部](../Page/英格兰.md "wikilink")[坎布里亚郡的一片乡村地区](../Page/坎布里亚郡.md "wikilink")，是受欢迎的度假胜地，以湖泊与群山，并因19世纪初诗人[华兹华斯的作品以及](../Page/华兹华斯.md "wikilink")[湖畔诗人](../Page/湖畔诗人.md "wikilink")（Lake
Poets）而著称。这个区域游客较多的中心地带被辟为[湖区国家公园](../Page/湖区国家公园.md "wikilink")，是英国的14个国家公园之一，也是英国不多的山区之一。英格兰所有海拔高于3000英尺的地方都位于这个国家公园内。2017年列入世界自然遺產。

## 地理

湖区的东西和南北跨度大约都是34英里（55千米），它是冰河时代的产物，在15000年前形成今日的面貌，包括冰蚀形成的宽阔的[U形谷](../Page/U形谷.md "wikilink")，其中有许多今天被水填满，形成了湖泊，这就是湖区名称的由来。这里还有许多冰河时代的盆地谷，通常形成小湖。较高的山多岩石，较低的山毗邻[高沼地](../Page/高沼地.md "wikilink")（moorland），广泛覆盖着[欧洲蕨](../Page/欧洲蕨.md "wikilink")
和[石南花](../Page/石楠_\(石楠属\).md "wikilink")。[林木线](../Page/林木线.md "wikilink")（tree
line）以下是天然的[橡树林地和](../Page/橡树.md "wikilink")19世纪人工栽培的[松树林](../Page/松树.md "wikilink")。由于降雨丰沛，许多地方沼泽遍布。

## 外部链接

[Lakeland_View.jpg](https://zh.wikipedia.org/wiki/File:Lakeland_View.jpg "fig:Lakeland_View.jpg").
[Harter Fell](../Page/Harter_Fell_\(Eskdale\).md "wikilink") and [Hard
Knott](../Page/Hard_Knott.md "wikilink") can be seen, also a small
[tarn](../Page/Tarn_\(lake\).md "wikilink"). --\>\]\]
[Lake_District_Map.PNG](https://zh.wikipedia.org/wiki/File:Lake_District_Map.PNG "fig:Lake_District_Map.PNG")
[Skiddaw.JPG](https://zh.wikipedia.org/wiki/File:Skiddaw.JPG "fig:Skiddaw.JPG")\]\]

  - [Lake District National Park
    Authority](http://www.lake-district.gov.uk/)

  - [Lake District photos](http://www.stridingedge.net/)

  -
  - [Lake District Walks](http://www.lakedistrictwalks.com/)

  - [Illustrated Guide to the Lake
    District](http://www.english-lakes.com)

  - [Lake district photographic guide and Alfred
    Wainwright](http://www.fellwalk.co.uk/eld.htm)

  - [The Lake District Mountains - walks and
    photos](http://www.thelakedistrictmountains.org/)

## 延伸阅读

  - Hollingsworth, S. 'The Geology of the Lake District: a review',
    *Proc. Geologists Assoc.*, 65 (Part 4) 1954
  - Moseley, F. *Geology of the Lake District*, Yorkshire Geologic

## 参考文献

[Category:英国地理](../Category/英国地理.md "wikilink")
[Category:坎布里亞郡](../Category/坎布里亞郡.md "wikilink")