[美國陸軍航空軍](../Page/美國陸軍航空軍.md "wikilink")

{{\*}}[美國空軍預備役司令部](../Page/美國空軍預備役司令部.md "wikilink") |rank =
[US-O6_insignia.svg](https://zh.wikipedia.org/wiki/File:US-O6_insignia.svg "fig:US-O6_insignia.svg")
[陆军上校](../Page/陆军上校.md "wikilink") |battles = [World War
II](../Page/World_War_II.md "wikilink") |serviceyears = 1942–1947
(Active)
1950–1959 (Reserve) |unit = 15th Air Force
{{\*}}449th Bombardment Group |mawards =
[飛行優異十字勳章](../Page/飛行優異十字勳章.md "wikilink")
[空运奖章](../Page/空运奖章.md "wikilink") (4) |}}
**小勞埃德·米拉德·本特森**（，），[美國政治人物](../Page/美國.md "wikilink")，[民主党人](../Page/民主党_\(美国\).md "wikilink")，在1971年至1993年間曾擔任四屆[德州](../Page/德州.md "wikilink")[美國參議員](../Page/美國參議院.md "wikilink")。在1949年至1955年，他曾出任[美國眾議員](../Page/美國眾議院.md "wikilink")。1988年，曾獲得民主黨的副總統競選提名。他曾擔任了美國參議院財經委員會主席。在[比爾·柯林頓執政時期](../Page/比爾·柯林頓.md "wikilink")，更擔任約兩年的[美國財政部長](../Page/美國財政部長.md "wikilink")。

## 早期生平

本特森出生於德州Mission市的一個[丹麥裔移民家庭](../Page/丹麥.md "wikilink")。他是[勞埃德·米拉德·老·本特森所生的第一代](../Page/勞埃德·米拉德·老·本特森.md "wikilink")[美國人](../Page/美國人.md "wikilink")。本特森曾是[美國童軍的](../Page/美國童軍.md "wikilink")[鷹級童軍](../Page/鷹級童軍.md "wikilink")，並且在德州童軍團獲得傑出鷹級童軍獎。1942年，本特森於[奧斯汀的](../Page/奧斯汀.md "wikilink")[德州法律學院畢業](../Page/德州法律學院.md "wikilink")。他在那裡被Sigma
Nu兄弟會封為Upsilon分會的大哥。畢業後，1942年至1945年間到[美國陸軍空戰隊服役](../Page/美國陸軍空戰隊.md "wikilink")。在[巴西負責情報工作之後](../Page/巴西.md "wikilink")，他以飛機師身分參與[第二次世界大戰中第](../Page/第二次世界大戰.md "wikilink")449轟炸機聯隊對[義大利南部的攻擊任務](../Page/義大利.md "wikilink")。他也曾被擊落兩次。其中一次，直接於南斯拉夫的山區墜落。\[1\]23歲，他便晉升為少校管轄為數600人的飛行中隊、15隻轟炸機及其隊員與維修單位。

在15個月的戰鬥裡，本特森出動了35次對付高度設防的目標，譬如[羅馬尼亞的](../Page/羅馬尼亞.md "wikilink")[普拉霍瓦油田](../Page/普拉霍瓦.md "wikilink")，那裡曾是[納粹德軍的重要設施](../Page/納粹.md "wikilink")。當年空中機動司令部的遠征機動第15特遣隊隸屬第449轟炸機聯隊，負責破壞攻擊範圍內所有石油生產設施。這些設施等於[德國在](../Page/德國.md "wikilink")[歐洲大陸一半的能源來源](../Page/歐洲.md "wikilink")。本特森少校的單位也攻擊[德國](../Page/德國.md "wikilink")、[義大利](../Page/義大利.md "wikilink")、[奧地利](../Page/奧地利.md "wikilink")、[捷克](../Page/捷克.md "wikilink")、[匈牙利](../Page/匈牙利.md "wikilink")、[羅馬尼亞及](../Page/羅馬尼亞.md "wikilink")[保加利亞境內的通信中心](../Page/保加利亞.md "wikilink")、飛機工廠及工業目標。他也曾參與突襲[義大利](../Page/義大利.md "wikilink")[安齊歐及為了登陸](../Page/安齊歐.md "wikilink")[法國南部而轟炸一些頑強的目標](../Page/法國.md "wikilink")。

後來，本特森獲得[美國陸軍航空軍其中一個最高榮譽及現今](../Page/美國陸軍航空軍.md "wikilink")[美國空軍最高成就的勳章](../Page/美國.md "wikilink")－[飛行優異十字勳章](../Page/飛行優異十字勳章.md "wikilink")。除此之外，他又獲得[空軍獎章加](../Page/空軍獎章.md "wikilink")3橡葉。這個獎章用來嘉許一些能夠完成一定數量任務的軍人。他在退役之前晉升至[空軍預備隊上校](../Page/空軍預備隊.md "wikilink")。

## 早期政治生涯

[Lloyd_Bentsen,_bw_photo_as_senator.jpg](https://zh.wikipedia.org/wiki/File:Lloyd_Bentsen,_bw_photo_as_senator.jpg "fig:Lloyd_Bentsen,_bw_photo_as_senator.jpg")
戰後，本特森回到他的家鄉－[格蘭德河谷](../Page/格蘭德河谷.md "wikilink")。1946年至1955年期間，他為當地人服務，擔任了連續三屆眾議員及[伊達爾戈縣的](../Page/伊達爾戈縣.md "wikilink")[裁判官](../Page/裁判官.md "wikilink")。在過去的三次普選中，沒有遇到任何反對票。在擔任眾議員的時候，他支持如果北韓不撤出38分界線便要向[北韓使用核子武器](../Page/北韓.md "wikilink")。1954年，他拒絕了再次競選並且投身商界。

## 商業生涯

本特森曾經在[侯斯頓的金融界工作了](../Page/侯斯頓.md "wikilink")16年。他非常成功並且收入豐厚。1970年，他晉身為Lincoln
Consolidated控股公司主席。

## 重返政壇

本特森為了參選而放棄了所有管理及董事職位。因此他的主要競選活動十分成功，讓參議員[拉爾夫·亞伯洛在](../Page/拉爾夫·亞伯洛.md "wikilink")1970年競逐德州議席前的黨內提名時候感到困擾。\[2\]

同年，本特森雖然受到時任眾議員、共和黨的[喬治·赫伯特·沃克·布殊的攻擊](../Page/喬治·赫伯特·沃克·布殊.md "wikilink")，但仍能在選舉勝出。選舉當晚，本特森對布殊的反擊令人信服。本特森的立場被[拉爾夫·亞伯洛](../Page/拉爾夫·亞伯洛.md "wikilink")（Ralph
Yarborough）的支持者及美國突出的自由派人士所排擠。毫無疑問，在1970年競選中，[凱因斯主義經濟學學者](../Page/凱因斯主義.md "wikilink")[約翰·肯尼斯·高伯瑞](../Page/約翰·肯尼斯·高伯瑞.md "wikilink")（John
Kenneth Galbraith）贊同布殊。民主黨的自由派認為如果本特森當選便會代表一個更保守的德州民主黨。

## 1976年總統競選

1974年，本特森為了民主黨1976年的總統選舉提名而舉行競選工作。他探訪了30個州並且從德州某一個籌款人士手中籌得35萬美元。本特森在1975年2月17日正式宣布參選並且籌得超過100萬美元的競選經費。當時只有[阿拉巴馬州的](../Page/阿拉巴馬州.md "wikilink")[喬治·華萊士及](../Page/喬治·華萊士.md "wikilink")[華盛頓州的](../Page/華盛頓州.md "wikilink")[亨利·傑克森的競選經費稍微超越這個總額](../Page/亨利·傑克森.md "wikilink")。本特森未能有效地組織全國性的活動。很多觀察者相信這位新晉參議員在競逐總統提名時沒有勝出的希望，不如轉移角逐副總統提名。

對於溫和派及保守派來說，華萊士和傑克森被認為是兩個主要競爭對手。其實本特森都有能力吸引這些選票。[吉米·卡特都在競選活動吸引到同樣的選票](../Page/吉米·卡特.md "wikilink")。

直到1975年10月，本特森在選舉中表現仍然不甚突出。他把競選活動集中在8到10個州分，希望突破困局。在首次州分角逐，情況已經十分嚴峻，他在[密西西比州僅僅能夠獲得](../Page/密西西比州.md "wikilink")1.6%的選票。兩個星期後，他在[奧克拉荷馬州傾盡全力也只能取得](../Page/奧克拉荷馬州.md "wikilink")12%選票。數日後，他停止了他自己的全國性競選活動，但仍然以[德州之子的身分競選](../Page/德州.md "wikilink")。可是，到了1976年5月1日，[吉米·卡特從](../Page/吉米·卡特.md "wikilink")98名德州代表取得92票。本特森在這次失敗使他的全國性競選希望幻滅。

## 參議員生涯

他後來面對[美國參議院的選舉](../Page/美國參議院.md "wikilink")，連續擊敗了四名共和黨的參議員，1970年擊敗了時任眾議員的[喬治·赫伯特·沃克·布希](../Page/喬治·赫伯特·沃克·布希.md "wikilink")，1976年擊敗了[達拉斯的](../Page/達拉斯.md "wikilink")[艾倫·施鐵民](../Page/艾倫·施鐵民.md "wikilink")，1982年擊敗了[達拉斯的共和黨保守派](../Page/達拉斯.md "wikilink")[詹姆·柯林斯](../Page/詹姆·柯林斯.md "wikilink")，1988年則擊敗了[柏德樂](../Page/柏德樂.md "wikilink")。同年，本特森成為美國民主黨的副總統候選人。

本特森早期被認為是保守派民主黨人。他其後是溫和派民主黨員，支持[墮胎權](../Page/墮胎.md "wikilink")、[平權法案](../Page/平權法案.md "wikilink")，支持公立學校禱告、環境保護、[死刑](../Page/死刑.md "wikilink")、[減稅](../Page/減稅.md "wikilink")、[撤銷管制規定等措施來平衡他認同和支持的](../Page/撤銷管制規定.md "wikilink")[民權](../Page/基本權_\(憲法學\).md "wikilink")。在經濟政策上，他支持商業利益。他曾擔任了美國參議院財經委員會主席。

## 1988年總統競選

1988年，[麻薩諸塞州州長](../Page/麻薩諸塞州.md "wikilink")[迈克尔·杜卡基思放棄了原來考慮的俄亥俄州議員John](../Page/迈克尔·杜卡基思.md "wikilink")
Glenn，選擇了黨內溫和保守派的本特森與他一起競逐[總統大選](../Page/1988年美國總統選舉.md "wikilink")，以平衡民主黨內的保守派及爭取支持，然而自由派的反對聲音導致杜卡基思和本特森競選失敗，同時保守派繼續支持共和黨的布什和奎爾。杜卡基思和本特森名單，再次由[1960年美國總統選舉以來](../Page/1960年美國總統選舉.md "wikilink")，北方自由的麻省和南方保守的德州民主黨候選人名單再次出現。期間副總統選舉辯論中，本特森最著名的是，當共和黨參議員、副總統候選人[丹·奎爾被質疑是否有政治經驗時](../Page/丹·奎爾.md "wikilink")，奎爾時年41歲，他把自己的政治經驗和[約翰·甘迺迪相比](../Page/約翰·甘迺迪.md "wikilink")，因此能勝任總統的職權，本特森隨即打斷他的發言，說：“參議員，我曾和傑克（約翰·甘迺迪昵称）共事。我知道傑克。傑克是我的朋友。參議員，你不是傑克。（Senator,
I served with Jack Kennedy. I knew Jack Kennedy. Jack Kennedy was a
friend of mine. Senator, you're no Jack
Kennedy.）”杜卡基思和本特森代表的民主黨在總統選舉中，德州僅獲得43%的普選票（布什和奎爾則獲得56%的普選票）。儘管如此，本特森以59%得票率當選連任參議員。

## 後期政治生涯

[Portrait_of_Lloyd_Bentsen.jpg](https://zh.wikipedia.org/wiki/File:Portrait_of_Lloyd_Bentsen.jpg "fig:Portrait_of_Lloyd_Bentsen.jpg")
1992年本特森計劃參選總統，但在當時布什總統引發[海灣戰爭後放棄參選](../Page/海灣戰爭.md "wikilink")，布什後來在[總統大選](../Page/1992年美國總統選舉.md "wikilink")，敗給[比爾·克林頓](../Page/比爾·克林頓.md "wikilink")。

1993年1月，他為答應[比爾·克林頓總統的邀請](../Page/比爾·克林頓.md "wikilink")，辭去了參議員職位，出任第69任[美國財政部長](../Page/美國財政部長.md "wikilink")。任內他力主消滅財政赤字以恢復經濟，作為黨內溫和保守派，他的出任受到民主黨一些自由派人物的抨擊。他擔任了約兩年後，於1994年12月[美國中期選舉後辭職](../Page/美國中期選舉.md "wikilink")。

## 去世

本特森退出政壇後，曾經在1998年中風。2006年5月23日，他在[-{zh-hans:休斯敦;zh-hk:侯斯頓;zh-tw:休士頓;}-的家中逝世](../Page/侯斯頓.md "wikilink")，享壽85歲。他的朋友、前總統柯林頓，在他的葬禮上發表悼詞。

## 選舉歷史

  - **1988年競選美國總統／副總統**
      - [喬治·赫伯特·沃克·布殊](../Page/喬治·赫伯特·沃克·布殊.md "wikilink")/[丹·奎爾](../Page/丹·奎爾.md "wikilink")
        (共和黨), 53% (426 選舉人票)
      - [迈克尔·杜卡基思](../Page/迈克尔·杜卡基思.md "wikilink")/勞埃德·本特森 (民主黨), 46%
        (111 選舉人票)
      - 勞埃德·本特森/[迈克尔·杜卡基思](../Page/迈克尔·杜卡基思.md "wikilink") (民主黨), 0% (1
        選舉人票)

<!-- end list -->

  - **1988年競選美國參議員**
      - 勞埃德·本特森(民主黨) , 59%
      - 柏德樂Beau Boulter (共和黨), 40%

<!-- end list -->

  - **1982年競選美國參議員**
      - 勞埃德·本特森 (民主黨), 59%
      - 詹姆．柯林斯(James Collins) (共和黨), 41%

<!-- end list -->

  - **1976年競選美國參議員**
      - 勞埃德·本特森 (民主黨), 57%
      - 艾倫·施鐵民(Alan Steelman) (共和黨), 42%

<!-- end list -->

  - **1970年競選美國參議員**
      - 勞埃德·本特森 (民主黨), 53%
      - [喬治·赫伯特·沃克·布殊](../Page/喬治·赫伯特·沃克·布殊.md "wikilink") (共和黨), 47%

## 參見

  - [Sigma Nu兄弟會](../Page/Sigma_Nu兄弟會.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

[分類:德州大學奧斯汀分校校友](../Page/分類:德州大學奧斯汀分校校友.md "wikilink")

[Category:總統自由勳章獲得者](../Category/總統自由勳章獲得者.md "wikilink")
[Category:航空獎章獲得者](../Category/航空獎章獲得者.md "wikilink")
[Category:1988年美國總統選舉](../Category/1988年美國總統選舉.md "wikilink")
[Category:美國民主黨副總統候選人](../Category/美國民主黨副總統候選人.md "wikilink")
[Category:美國財政部長](../Category/美國財政部長.md "wikilink")
[Category:美国民主党联邦参议员](../Category/美国民主党联邦参议员.md "wikilink")
[B](../Category/美国民主党联邦众议员.md "wikilink")
[Category:美國民主黨員](../Category/美國民主黨員.md "wikilink")
[Category:美國童軍人物](../Category/美國童軍人物.md "wikilink")
[Category:美國共濟會會員](../Category/美國共濟會會員.md "wikilink")
[Category:美國空軍上校](../Category/美國空軍上校.md "wikilink")
[Category:美國陸軍航空軍軍官](../Category/美國陸軍航空軍軍官.md "wikilink")
[Category:美國第二次世界大戰軍事人物](../Category/美國第二次世界大戰軍事人物.md "wikilink")
[Category:美國長老宗教徒](../Category/美國長老宗教徒.md "wikilink")
[Category:丹麥裔美國人](../Category/丹麥裔美國人.md "wikilink")
[Category:德克薩斯州人](../Category/德克薩斯州人.md "wikilink")
[Category:杰出鹰级童军奖得主](../Category/杰出鹰级童军奖得主.md "wikilink")

1.  美國財政部部長傳記辭典
2.  [美國財政部部長簡介](http://www.ustreas.gov/education/history/secretaries/lmbentsen.html)