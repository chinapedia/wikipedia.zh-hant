**敬武公主**，也稱**敬武長公主**。[漢宣帝之女](../Page/汉宣帝.md "wikilink")，生母没有记载
，[漢元帝之妹](../Page/漢元帝.md "wikilink")。

## 生平

敬武公主天生麗質，婀娜窈窕，嫁富平侯[張臨](../Page/張臨.md "wikilink")（[張安世曾孫](../Page/張安世.md "wikilink")）為妻，生了一個兒子[張放](../Page/張放.md "wikilink")\[1\]。張臨死後，又嫁临平侯[赵钦](../Page/赵钦_\(营平侯\).md "wikilink")（[赵充国孙](../Page/赵充国.md "wikilink")），无子，公主指使赵钦良人习氏假孕，将他人之子诈称为自己所生子\[2\]。趙钦死後，诈称为习氏所生子的赵岑嗣侯，习氏为太夫人，后被告发非赵钦子，国除。公主再嫁高阳侯[薛宣](../Page/薛宣.md "wikilink")。薛宣以罪免侯归故郡，公主留京师。後薛宣病逝，薛宣子[薛况回长安与之同居](../Page/薛况.md "wikilink")\[3\]。因阿附丁、傅外戚，为外戚王氏所恨。[漢平帝时](../Page/漢平帝.md "wikilink")，王莽专权，公主非议王莽，王莽遂使人以药逼其自杀\[4\]\[5\]。王莽向太皇太后[王政君宣称公主因暴病而薨](../Page/王政君.md "wikilink")，太后想前往吊丧，被王莽极力阻止\[6\]。

## 家庭

<center>

</center>

## 参考文献

[Category:漢宣帝皇女](../Category/漢宣帝皇女.md "wikilink")
[Category:西漢公主](../Category/西漢公主.md "wikilink")
[Category:西汉自杀人物](../Category/西汉自杀人物.md "wikilink")
[Category:劉姓](../Category/劉姓.md "wikilink")

1.  《[汉书·张汤传](../Page/s:汉书/卷059.md "wikilink")》

2.  《[汉书·赵充国传](../Page/s:汉书/卷069.md "wikilink")》

3.  漢書/卷083，「况私从敦煌归长安，会赦，因留与主私乱。」

4.  《[汉书·薛宣传](../Page/s:汉书/卷083.md "wikilink")》

5.  《[汉书·王莽传上](../Page/s:汉书/卷099上.md "wikilink")》

6.