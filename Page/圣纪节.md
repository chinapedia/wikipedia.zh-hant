[International_Mawlid_Conference_at_Minar-e-Pakistan_Lahore_by_Minhaj-ul-Quran1.jpg](https://zh.wikipedia.org/wiki/File:International_Mawlid_Conference_at_Minar-e-Pakistan_Lahore_by_Minhaj-ul-Quran1.jpg "fig:International_Mawlid_Conference_at_Minar-e-Pakistan_Lahore_by_Minhaj-ul-Quran1.jpg")
[巴基斯坦](../Page/巴基斯坦.md "wikilink")\]\]
**圣纪节**（，）是[伊斯兰教的重要节日](../Page/伊斯兰教.md "wikilink")，为纪念先知[穆罕默德的诞辰日](../Page/穆罕默德.md "wikilink")，但[逊尼派和](../Page/逊尼派.md "wikilink")[什叶派纪念圣纪节的日期不同](../Page/什叶派.md "wikilink")，[中国境内的少数民族穆斯林基本是逊尼派](../Page/中国.md "wikilink")，所以圣纪节也是信仰伊斯兰教的少数民族重要节日。逊尼派的圣纪节是[伊斯兰历](../Page/伊斯兰历.md "wikilink")3月12日，什叶派是3月17日。据说当年穆罕默德经常在自己出生的日子（星期一）进行斋戒，但现在穆斯林过圣纪节并不把斋，而是准备[食品庆祝](../Page/食品.md "wikilink")，讲述穆罕默德生前的事迹等。但事实，由于文化的变迁，在中国，少数民族并不纪念圣纪节。

## 圣纪节与[公历对照表](../Page/公历.md "wikilink")

| 年份                                   | 逊尼派                                  | 什叶派                                  |
| ------------------------------------ | ------------------------------------ | ------------------------------------ |
| [2003年](../Page/2003年.md "wikilink") | [5月14日](../Page/5月14日.md "wikilink") | [5月19日](../Page/5月19日.md "wikilink") |
| [2004年](../Page/2004年.md "wikilink") | [5月2日](../Page/5月2日.md "wikilink")   | [5月7日](../Page/5月7日.md "wikilink")   |
| [2005年](../Page/2005年.md "wikilink") | [4月21日](../Page/4月21日.md "wikilink") | [4月26日](../Page/4月26日.md "wikilink") |
| [2006年](../Page/2006年.md "wikilink") | [4月11日](../Page/4月11日.md "wikilink") | [4月16日](../Page/4月16日.md "wikilink") |
| [2007年](../Page/2007年.md "wikilink") | [3月31日](../Page/3月31日.md "wikilink") | [4月5日](../Page/4月5日.md "wikilink")   |
| [2008年](../Page/2008年.md "wikilink") | [3月20日](../Page/3月20日.md "wikilink") | [3月25日](../Page/3月25日.md "wikilink") |
| [2009年](../Page/2009年.md "wikilink") | [3月9日](../Page/3月9日.md "wikilink")   | [3月14日](../Page/3月14日.md "wikilink") |
| [2010年](../Page/2010年.md "wikilink") | [2月26日](../Page/2月26日.md "wikilink") | [3月3日](../Page/3月3日.md "wikilink")   |
| [2011年](../Page/2011年.md "wikilink") | [2月15日](../Page/2月15日.md "wikilink") | [2月20日](../Page/2月20日.md "wikilink") |
| [2012年](../Page/2012年.md "wikilink") | [2月4日](../Page/2月4日.md "wikilink")   | [2月9日](../Page/2月9日.md "wikilink")   |
| [2013年](../Page/2013年.md "wikilink") | [1月24日](../Page/1月24日.md "wikilink") | [1月29日](../Page/1月29日.md "wikilink") |

**圣纪节**

[Category:伊斯蘭節日](../Category/伊斯蘭節日.md "wikilink")