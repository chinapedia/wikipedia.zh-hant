**郭炳江**（，），[香港](../Page/香港.md "wikilink")[企業家](../Page/企業家.md "wikilink")，籍貫[廣東省](../Page/廣東省.md "wikilink")[中山](../Page/中山市.md "wikilink")，[新鴻基地產聯合創辦人](../Page/新鴻基地產.md "wikilink")[郭得勝次子](../Page/郭得勝.md "wikilink")，[郭炳湘](../Page/郭炳湘.md "wikilink")（已故）之二弟及[郭炳聯之二兄](../Page/郭炳聯.md "wikilink")。

## 生平

郭炳江幼時家族經營雜貨批發等，小一至中五均就讀[慈幼英文學校](../Page/慈幼英文學校.md "wikilink")\[1\]。後到[英國升學](../Page/英國.md "wikilink")，取得英國[倫敦商学院](../Page/倫敦商学院.md "wikilink")[工商管理碩士學位及](../Page/工商管理.md "wikilink")[倫敦大學](../Page/倫敦大學.md "wikilink")[帝國學院](../Page/帝國學院.md "wikilink")[土木工程系學士學位](../Page/土木工程.md "wikilink")。

郭炳江是[新鴻基地產發展有限公司前任](../Page/新鴻基地產發展有限公司.md "wikilink")[董事局聯席主席兼前任](../Page/董事長.md "wikilink")[董事總經理](../Page/董事總經理.md "wikilink")\[2\]，亦是[三號幹線（郊野公園段）有限公司主席](../Page/三號幹線（郊野公園段）有限公司.md "wikilink")\[3\]、[IFC
Development Limited](../Page/IFC_Development_Limited.md "wikilink")
聯席主席\[4\]\[5\]及[東亞銀行有限公司前任獨立非執行董事](../Page/東亞銀行有限公司.md "wikilink")\[6\]。

在地產業界，他是[香港地產建設商會第一副會長](../Page/香港地產建設商會.md "wikilink")。後來獲[香港特區政府委任為](../Page/香港特區政府.md "wikilink")[臨時建造業統籌委員會委員及](../Page/臨時建造業統籌委員會.md "wikilink")[可持續發展委員會成員](../Page/可持續發展委員會.md "wikilink")。及後，他擔任[營商諮詢小組成員](../Page/營商諮詢小組.md "wikilink")、[土地及建設諮詢委員會](../Page/土地及建設諮詢委員會.md "wikilink")、[註冊承建商懲戒處分委員會及](../Page/註冊承建商懲戒處分委員會.md "wikilink")[香港總商會工業事務委員會之委員](../Page/香港總商會工業事務委員會.md "wikilink")、[建造商會物業管理委員會主席及](../Page/建造商會物業管理委員會.md "wikilink")[香港建造商會幹事](../Page/香港建造商會.md "wikilink")。

在社區參與方面，他曾出任[香港公益金董事](../Page/香港公益金.md "wikilink")、[社會福利政策及服務委員會及](../Page/社會福利政策及服務委員會.md "wikilink")[香港公開進修學院校董會之委員](../Page/香港公開大學.md "wikilink")。他曾被選為[廣州市榮譽市民及第九屆](../Page/廣州市.md "wikilink")[中國人民政治協商會議](../Page/中國人民政治協商會議.md "wikilink")[上海市委員會的常務委員](../Page/上海市.md "wikilink")。同時，他亦為[慈幼學校](../Page/慈幼學校.md "wikilink")([小學](../Page/小學.md "wikilink"))及[慈幼英文學校](../Page/慈幼英文學校.md "wikilink")([中學](../Page/中學.md "wikilink"))的校友，為母校辦事，校內的409室亦以其命名。

1991年他獲政府委任為[市政局議員](../Page/市政局_\(香港\).md "wikilink")，至1993年離任。

他於1994年起信奉[基督新教](../Page/基督新教.md "wikilink")，並於同年12月與太太[梁潔芹一同](../Page/梁潔芹.md "wikilink")[洗禮](../Page/洗禮.md "wikilink")。郭炳江出生時患有「小耳症」，雙耳天生沒耳道，要接受手術及戴助聽器植入耳道內。

政府於2018年3月2日刊憲，宣佈撤除郭炳江的銀紫荊星章，並同時褫奪太平紳士身份。

## 家庭成員

[HK_Olympic_Torch_Relay_Shatin_B1.jpg](https://zh.wikipedia.org/wiki/File:HK_Olympic_Torch_Relay_Shatin_B1.jpg "fig:HK_Olympic_Torch_Relay_Shatin_B1.jpg")香港火炬接力\]\]

  - 太太：[梁潔芹](../Page/梁潔芹.md "wikilink")
  - 女兒：[郭曉妍](../Page/郭曉妍.md "wikilink")（Noelle），前女婿：[呂慶耀](../Page/呂慶耀.md "wikilink")\[7\]
  - 兒子：[郭基煇](../Page/郭基煇.md "wikilink")（Adam）（1983年－）

## 涉嫌賄賂被捕入獄

  - 2012年3月29日，[香港廉政公署邀請](../Page/香港廉政公署.md "wikilink")[新鴻基地產董事局聯席主席郭炳江](../Page/新鴻基地產.md "wikilink")、[郭炳聯](../Page/郭炳聯.md "wikilink")，與及前[政務司司長](../Page/政務司司長.md "wikilink")[許仕仁到廉署總部協助調查一宗貪污案](../Page/許仕仁.md "wikilink")，同日傍晚，廉政公署正式宣佈拘捕三人。\[8\]\[9\]7月13日，香港廉政公署正式起诉前政务司司长许仕仁、新鸿基地产董事局聯席主席郭炳江、郭炳联、新鸿基执行董事陈鉅源和港交所前高级副总裁关雄生。
  - 2014年12月19日，郭炳江在[高等法院被陪審團以七比二裁定一條串謀公職人員行為失當罪罪成](../Page/香港高等法院.md "wikilink")，於當日傍晚即時收監，還押[荔枝角收押所](../Page/荔枝角收押所.md "wikilink")。
  - 2014年12月23日，法官判處郭炳江監禁5年、罰款50萬[港元及下令禁止他出任任何公司董事](../Page/港元.md "wikilink")5年。
  - 2014年12月29日，郭炳江代表律師代郭炳江正式向法庭提上訴，指法官引導陪審團出錯，而且陪審團的裁決不合常理。\[10\]
  - 2016年7月12日，終審法院批准郭炳江就貪賄案的上訴許可申請，並且准許他以一千萬港元保釋外出。\[11\]
  - 2017年6月14日，郭炳江終審上訴失敗，須重返監獄服刑。\[12\]
  - 2018年3月2日，郭炳江於2007年獲頒授之[銀紫荊星章被政府褫奪](../Page/銀紫荊星章.md "wikilink")，其[太平紳士委任亦被撤銷](../Page/太平紳士.md "wikilink")。\[13\]
  - 2019年3月21日，郭炳江刑滿，於早上9:30步出[赤柱監獄](../Page/赤柱監獄.md "wikilink")。\[14\]

## 參見

  - [郭得胜家族](../Page/郭得胜家族.md "wikilink")

## 注釋

<references/>

## 外部連結

  - [新鴻基地產發展有限公司:董事及行政架構](https://web.archive.org/web/20070928095415/http://www.shkp.com.hk/data/investors/reports_detail/1/1/1_28_tc.pdf)
  - [郭炳江見證](https://web.archive.org/web/20080730025022/http://www.shanfook.org.hk/shanfook/html/cloud02.html)

[Category:上海市政协委员](../Category/上海市政协委员.md "wikilink")
[Category:全國政協香港委员](../Category/全國政協香港委员.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:香港新教徒](../Category/香港新教徒.md "wikilink")
[Category:新鴻基地產](../Category/新鴻基地產.md "wikilink")
[Category:東亞銀行](../Category/東亞銀行.md "wikilink")
[Category:倫敦帝國學院校友](../Category/倫敦帝國學院校友.md "wikilink")
[Category:香港中山人](../Category/香港中山人.md "wikilink")
[Category:郭得勝家族](../Category/郭得勝家族.md "wikilink")
[Category:香港曾入獄者](../Category/香港曾入獄者.md "wikilink")
[Category:被褫奪香港勳銜者](../Category/被褫奪香港勳銜者.md "wikilink")
[Category:前市政局議員](../Category/前市政局議員.md "wikilink")
[Category:香港理工大學榮譽博士](../Category/香港理工大學榮譽博士.md "wikilink")
[Category:香港公開大學榮譽博士](../Category/香港公開大學榮譽博士.md "wikilink")
[B炳](../Category/郭姓.md "wikilink")

1.

2.   東亞銀行公司資料

3.
4.
5.
6.
7.

8.  [廉署拘捕郭氏兄弟及許仕仁涉嫌貪污行為失當](http://rthk.hk/rthk/news/expressnews/news.htm?expressnews&20120329&55&829767)
    ，[香港電台](../Page/香港電台.md "wikilink")，2012年3月29日

9.  [消息稱廉署循官商勾結方向調查郭氏兄弟案件](http://rthk.hk/rthk/news/expressnews/news.htm?expressnews&20120329&55&829775)
    ，[香港電台](../Page/香港電台.md "wikilink")，2012年3月29日

10. [【新地案】郭炳江正式提上訴](http://hk.apple.appledaily.com/realtime/news/20141229/53278946?top=4h)，[蘋果日報](../Page/蘋果日報_\(香港\).md "wikilink")，2014年12月29日

11. [【新地案】郭炳江步出終院：生活有規律，但最好唔好响裡面](http://hk.apple.nextmedia.com/realtime/news/20160712/55348782)，[蘋果日報](../Page/蘋果日報_\(香港\).md "wikilink")，2016年7月12日

12. [【新地案上訴】郭炳江終極上訴失敗
    重新收押荔枝角再X光「通櫃」](http://hk.apple.nextmedia.com/realtime/news/20170614/56825515)，[蘋果日報](../Page/蘋果日報_\(香港\).md "wikilink")，2017年6月14日

13.

14. [郭炳江刑滿出獄
    「我冇怨恨」](https://hk.news.appledaily.com/breaking/realtime/article/20190321/59391097)，[蘋果日報](../Page/蘋果日報_\(香港\).md "wikilink")，2019年3月21日