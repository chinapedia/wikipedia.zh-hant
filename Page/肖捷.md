**肖捷**（），[辽宁](../Page/辽宁省.md "wikilink")[开原人](../Page/开原市.md "wikilink")，出生于北京。1976年3月参加工作，1985年8月加入[中国共产党](../Page/中国共产党.md "wikilink")。[中国人民大学财政系财政金融专业本科毕业](../Page/中国人民大学.md "wikilink")，财政部财政科学研究所[财政专业](../Page/财政.md "wikilink")[在职](../Page/在职研究生.md "wikilink")[博士](../Page/博士.md "wikilink")[研究生](../Page/研究生.md "wikilink")。[第十七](../Page/中国共产党第十七届中央委员会委员列表.md "wikilink")、[十八届](../Page/中国共产党第十八届中央委员会委员列表.md "wikilink")、[十九届](../Page/中国共产党第十九届中央委员会委员列表.md "wikilink")[中央委员](../Page/中国共产党中央委员会.md "wikilink")。曾任[国务院常务副秘书长](../Page/中华人民共和国国务院副秘书长.md "wikilink")（正部长级，保障李克强总理日常工作）、财政部部长。現任[国务委员兼](../Page/国务委员.md "wikilink")[国务院秘书长](../Page/中华人民共和国国务院秘书长.md "wikilink")、[国务院机关党组书记](../Page/中国共产党国务院机关党组.md "wikilink")。

## 簡歷

1976年3月，进入[北京市机械局机械研究所做工](../Page/北京市.md "wikilink")。1978年10月，考入[中国人民大学财政系财政金融专业学习](../Page/中国人民大学.md "wikilink")。1982年9月，进入[财政部综合计划司长期计划处](../Page/中华人民共和国财政部.md "wikilink")，任干部。1987年1月，任长期计划处副处长，并于1987年12月至1989年4月在[西德](../Page/西德.md "wikilink")（联邦德国）进修一年半。1991年12月，升任处长。1993年11月，任综合计划司副司长(其间：1994年9月至1995年7月，在[中共中央党校一年制中青年干部培训班学习](../Page/中共中央党校.md "wikilink")；1992年9月至1995年7月，在财政部财政科学研究所财政专业读在职博士研究生)。1998年7月，任财政部综合司司长。2000年6月，任财政部国库司司长。2001年9月，任财政部副部长。

2005年7月，任[中共湖南省委常委](../Page/中国共产党湖南省委员会.md "wikilink")、[湖南省副省长](../Page/湖南省.md "wikilink")。

2007年8月，任[国家税务总局局长](../Page/国家税务总局.md "wikilink")\[1\]。2013年3月，任[国务院常务副秘书长](../Page/中华人民共和国国务院副秘书长.md "wikilink")（正部长级），接替[尤权负责](../Page/尤权.md "wikilink")[李克强总理的日常工作](../Page/李克强.md "wikilink")\[2\]。2016年11月7日，任[財政部部長](../Page/中华人民共和国财政部.md "wikilink")\[3\]。2017年10月，任[中央国家机关工委书记](../Page/中共中央国家机关工作委员会.md "wikilink")，国务院机关党组书记、副秘书长\[4\]。

2018年3月19日，[第十三届全国人民代表大会第一次会议第七次全体会议](../Page/第十三届全国人民代表大会#第一次会议.md "wikilink")，肖捷获总理[李克强提名为](../Page/李克强.md "wikilink")[国务委员及](../Page/国务委员.md "wikilink")[国务院秘书长](../Page/中华人民共和国国务院秘书长.md "wikilink")，\[5\]经全国人大投票表决决定任命。\[6\]

## 参考文献

{{-}}

[Category:中华人民共和国领导人](../Category/中华人民共和国领导人.md "wikilink")
[Category:李克强内阁](../Category/李克强内阁.md "wikilink") [Category:国务委员
(第13届国务院)](../Category/国务委员_\(第13届国务院\).md "wikilink")
[Category:中华人民共和国国务院秘书长](../Category/中华人民共和国国务院秘书长.md "wikilink")
[Category:中华人民共和国国务院正部长级副秘书长](../Category/中华人民共和国国务院正部长级副秘书长.md "wikilink")
[Category:中华人民共和国财政部部长](../Category/中华人民共和国财政部部长.md "wikilink")
[Category:中华人民共和国财政部副部长](../Category/中华人民共和国财政部副部长.md "wikilink")
[Category:国家税务总局局长](../Category/国家税务总局局长.md "wikilink")
[Category:中国共产党第十七届中央委员会委员](../Category/中国共产党第十七届中央委员会委员.md "wikilink")
[Category:中国共产党第十八届中央委员会委员](../Category/中国共产党第十八届中央委员会委员.md "wikilink")
[Category:中国共产党第十九届中央委员会委员](../Category/中国共产党第十九届中央委员会委员.md "wikilink")
[Category:中共湖南省委常委](../Category/中共湖南省委常委.md "wikilink")
[Category:中华人民共和国湖南省常务副省长](../Category/中华人民共和国湖南省常务副省长.md "wikilink")
[X](../Category/中国人民大学校友.md "wikilink")
[Category:开原人](../Category/开原人.md "wikilink")
[J](../Category/肖姓.md "wikilink")
[Category:北京人](../Category/北京人.md "wikilink")

1.
2.
3.
4.
5.
6.