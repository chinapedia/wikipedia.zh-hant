**豐臣秀賴**（），[豐臣秀吉之子](../Page/豐臣秀吉.md "wikilink")，側室-{[淀殿](../Page/淀殿.md "wikilink")}-（-{淀}-夫人）所生，幼名拾丸。娶[德川秀忠之女](../Page/德川秀忠.md "wikilink")[千姬为妻](../Page/德川千姬.md "wikilink")，但和側室之間生有[國松與](../Page/豐臣國松.md "wikilink")[天秀尼](../Page/天秀尼.md "wikilink")，官至從一位[右大臣](../Page/右大臣.md "wikilink")，[豐臣政權的第三代家督](../Page/豐臣政權.md "wikilink")。

## 生平

在秀賴出生之前，尚有一兄[鶴松](../Page/豐臣鶴松.md "wikilink")，但很早就夭折了。原本秀吉以為自己年事已高，不會再得子嗣。不料在五十七歲時，竟又得一子，使秀吉對原本收養的繼承人[關白](../Page/關白.md "wikilink")、豐臣家[家督](../Page/家督.md "wikilink")[豐臣秀次開始冷淡](../Page/豐臣秀次.md "wikilink")，奪回賜給秀次的權力，[賜死家督秀次](../Page/賜死.md "wikilink")，將其妻妾子女[滅族](../Page/滅族.md "wikilink")。從此，秀吉便為秀賴的將來鋪路，制定了[五大老與](../Page/五大老.md "wikilink")[五奉行](../Page/五奉行.md "wikilink")，讓他們輔佐秀賴。

太閣秀吉死後，[豐臣秀賴成為豐臣政權第三代家督](../Page/豐臣秀賴.md "wikilink")。五大老以[德川家康最具影響力](../Page/德川家康.md "wikilink")，使豐臣政權內部勢力開始有派系分別。不久，五大老之一的[前田利家也過世](../Page/前田利家.md "wikilink")，五奉行之一的[石田三成開始與家康對立](../Page/石田三成.md "wikilink")，互相攏絡勢力。最後東軍以[德川家康為首](../Page/德川家康.md "wikilink")，西軍以[毛利輝元為首](../Page/毛利輝元.md "wikilink")，於1600年兩軍對決，史稱[關原之戰](../Page/關原之戰.md "wikilink")，後來德川方面贏得戰爭，而石田三成則被處死。1603年，德川家康即[-{征}-夷大將軍之位](../Page/幕府將軍.md "wikilink")，建立[幕府](../Page/幕府.md "wikilink")，豐臣政權結束。

雖然德川與豐臣兩家不和，但仍尊照秀吉生前意願，讓秀賴與表妹千姬結婚。但是雙方婚姻不能阻礙家康的野心，家康一直意圖讓秀賴前去參見他，屢次被-{淀}-夫人阻止，後來雙方於1611年在[二條城見面](../Page/二條城.md "wikilink")，暫時和睦。然而發生[方廣寺鐘銘事件](../Page/方廣寺鐘銘事件.md "wikilink")，[幕府以](../Page/幕府.md "wikilink")[文字獄方式構陷豐臣家](../Page/文字獄.md "wikilink")，雙方正式決裂，德川家開始進攻豐臣家。[大坂冬之陣後](../Page/大坂冬之陣.md "wikilink")，雙方進行協議，暫告停戰。

[Toyotomihideyori_tomb.jpg](https://zh.wikipedia.org/wiki/File:Toyotomihideyori_tomb.jpg "fig:Toyotomihideyori_tomb.jpg")
1615年夏天，因豐臣家不能驅逐浪人，再度與德川家決裂，造成[大坂夏之陣發生](../Page/大坂夏之陣.md "wikilink")。[大坂城被攻落之際](../Page/大坂城.md "wikilink")，千姬逃回德川家，秀賴與-{淀}-夫人及身邊親信如[大野治長](../Page/大野治長.md "wikilink")、[大藏卿局等人一同躲在大坂城的米倉裡](../Page/大藏卿局.md "wikilink")[切腹](../Page/切腹.md "wikilink")，得年僅二十二歲。德川家康将豐臣家男丁不分老幼全部处死，秀賴儿子国松丸被搜出並[斬首](../Page/斬首.md "wikilink")，在[常高院的一再恳求之下](../Page/常高院.md "wikilink")，女兒[千代姬免于一死但被迫](../Page/千代姬.md "wikilink")[出家為尼](../Page/出家.md "wikilink")，法號為天秀法泰尼，通稱[天秀尼](../Page/天秀尼.md "wikilink")。豐臣家自天秀尼於西元1645年過世後，秀吉一脈自此絶後。

秀賴死後，[法名](../Page/法名.md "wikilink")**歸寂山高陽寺殿秀山大居士**，其墓一在京都養源院，一在京都清涼寺。

關於秀賴，由於他從小受的是[公家教育](../Page/公家.md "wikilink")，目前流傳的[書法筆跡有相當高的評價](../Page/書法.md "wikilink")。

## 家系

### 妻妾

  - 正室：[德川千姬](../Page/德川千姬.md "wikilink")
  - 側室：渡邊阿以茶之方（和期之方，渡邊五兵衛之女）
  - 側室：阿石之方（成田直助之女）

### 子女

  - 長子：國松丸（秀勝、[豐臣國松](../Page/豐臣國松.md "wikilink")）
  - 長女：千代姬（[天秀尼](../Page/天秀尼.md "wikilink")）

## 注釋

|-style="text-align: center; background: \#FFE4E1;" |align="center"
colspan="3"|**豐臣秀賴**  |-    |-  |-    |-   |-  |-

[Hideyori](../Category/豐臣氏.md "wikilink")
[Category:織豐政權大名](../Category/織豐政權大名.md "wikilink")
[Category:日本自殺者](../Category/日本自殺者.md "wikilink")
[Category:攝津國出身人物](../Category/攝津國出身人物.md "wikilink")