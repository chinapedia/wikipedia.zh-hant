**西崎崇子**（）是日本[小提琴家](../Page/小提琴家.md "wikilink")。

自幼從父[西崎信二學習](../Page/西崎信二.md "wikilink")[小提琴](../Page/小提琴.md "wikilink")，是[鈴木鎮一所創的](../Page/鈴木鎮一.md "wikilink")[鈴木教學法的首位学员](../Page/鈴木教學法.md "wikilink")。五歲開始在公眾場合演出。

1962年去美國深造并获[弗里茨·克萊斯勒獎學金](../Page/弗里茨·克萊斯勒.md "wikilink")（Fritz Kreisler
Scholarship）。

1967年在[黎文特利特國際比賽中獲亞軍](../Page/黎文特利特國際比賽.md "wikilink")。

1969年在[朱利亞德協奏曲比賽](../Page/朱利亞德學院.md "wikilink")（Juilliard Concerto
Competition）中獲冠軍，當時她與[今井信子](../Page/今井信子.md "wikilink")（[Nobuko
Imai](../Page/:en:Nobuko_Imai.md "wikilink")）一起演奏了莫扎特交響協奏曲。

后与[拿索斯唱片公司老闆](../Page/拿索斯唱片公司.md "wikilink")[克劳斯·海曼](../Page/克劳斯·海曼.md "wikilink")（Klaus
Heymann）结婚，定居[香港](../Page/香港.md "wikilink")。

西崎崇子錄製的《[梁祝小提琴協奏曲](../Page/梁祝小提琴協奏曲.md "wikilink")》总銷量超過三百萬張\[1\]。

由於西崎崇子致力推廣古典音樂及慈善活動，2003年獲[香港特別行政區政府頒授](../Page/香港特別行政區政府.md "wikilink")[銅紫荊星章](../Page/銅紫荊星章.md "wikilink")。

西崎崇子於2013獲[香港科技大學頒授榮譽大學院士](../Page/香港科技大學.md "wikilink")\[2\]\[3\]，並於該校人文學部兼任教授\[4\]。

## 注釋

## 外部链接

  - [biography and discography of Takako Nishizaki at
    Naxos.com](http://www.naxos.com/artistinfo/bio552.htm)

[Category:日本小提琴家](../Category/日本小提琴家.md "wikilink")
[Category:愛知縣出身人物](../Category/愛知縣出身人物.md "wikilink")

1.  [西崎崇子，拿索斯](http://www.naxos.com.hk/Display1.asp?path=takako.htm)
2.  [27/06/2013
    香港科技大學頒授榮譽大學院士予四位傑出人士](http://www.ust.hk/chi/news/press_20130627-1039.html)
3.  [第5頁西崎崇子教授讚辭全文](http://www.ust.hk/wp-content/uploads/2014/07/Full_Citation.pdf)
4.  [HKUST - Division of
    Humanities](http://huma.ust.hk/people/faculty_office_hours.html)