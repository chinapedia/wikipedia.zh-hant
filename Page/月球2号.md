<table>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><font size="+1"><strong>月球2号</strong></font></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><table>
<tbody>
<tr class="odd">
</tr>
</tbody>
</table></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>发射日期</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>发射载具</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>任务结束日期</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>质量</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>稳定方式</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><strong>NSSDC ID</strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><strong>轨道数据</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

**月球2号**（[俄语](../Page/俄语.md "wikilink")：****）是[苏联于](../Page/苏联.md "wikilink")1959年9月12日发射的无人[月球探测器](../Page/月球.md "wikilink")。它是世界上第一个在月球表面[硬着陆的航天器](../Page/硬着陆.md "wikilink")。1959年9月14日，月球2号在月球上墜毀。

月球2号的探测结果表明，月球没有[磁场](../Page/磁场.md "wikilink")，且月球周围没有像[范艾伦辐射带一样的](../Page/范艾伦辐射带.md "wikilink")[辐射带](../Page/辐射带.md "wikilink")。

在月球2号上携带了两枚刻有[苏联国徽的装饰物](../Page/苏联国徽.md "wikilink")。1959年9月15日，苏联领导人[赫鲁晓夫把一枚这种装饰物的复制品送给了](../Page/赫鲁晓夫.md "wikilink")[美国总统](../Page/美国.md "wikilink")[艾森豪威尔](../Page/艾森豪威尔.md "wikilink")。
[缩略图](https://zh.wikipedia.org/wiki/File:Kansas_Cosmosphere_Luna_2_Pennant_2013.JPG "fig:缩略图")
{{-}}

## 參見

  - [太陽系探測器列表](../Page/太陽系探測器列表.md "wikilink")
  - [太陽系探索時間線](../Page/太陽系探索時間線.md "wikilink")

[Category:月球号探测器](../Category/月球号探测器.md "wikilink")
[Category:苏联的世界之最](../Category/苏联的世界之最.md "wikilink")
[Category:1959年蘇聯](../Category/1959年蘇聯.md "wikilink")
[Category:1959年科學](../Category/1959年科學.md "wikilink")
[Category:1959年9月](../Category/1959年9月.md "wikilink")