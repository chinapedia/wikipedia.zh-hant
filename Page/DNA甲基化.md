[DNA_methylation.jpg](https://zh.wikipedia.org/wiki/File:DNA_methylation.jpg "fig:DNA_methylation.jpg")基因调控中起重要作用。\]\]

**DNA甲基化**（）為[DNA化學修飾的一種形式](../Page/DNA.md "wikilink")，能在不改變DNA序列的前提下，改變遺傳表現。為[外遺傳編碼](../Page/外遺傳編碼.md "wikilink")（epigenetic
code）的一部分，是一種[外遺傳機制](../Page/外遺傳學.md "wikilink")。DNA甲基化過程會使[甲基添加到DNA分子上](../Page/甲基.md "wikilink")，例如在[胞嘧啶環的](../Page/胞嘧啶.md "wikilink")5'碳上：這種[5'方向的DNA甲基化方式可見於所有脊椎動物](../Page/方向性.md "wikilink")。

在[人類細胞內](../Page/人類.md "wikilink")，大約有1%的DNA[鹼基受到了](../Page/鹼基.md "wikilink")[甲基化](../Page/甲基化.md "wikilink")。在成熟[體細胞](../Page/體細胞.md "wikilink")[組織中](../Page/组织_\(生物学\).md "wikilink")，DNA甲基化一般發生於[CpG雙核苷酸](../Page/CpG位点.md "wikilink")（CpG
dinucleotide）部位；而非CpG甲基化則於[胚胎幹細胞中較為常見](../Page/胚胎幹細胞.md "wikilink")\[1\]
\[2\]。植物體內胞嘧啶的甲基化則可分為對稱的CpG（或CpNpG），或是不對稱的CpNpNp形式（C與G是鹼基；p是磷酸根；N指的是任意的核苷酸）。

特定胞嘧碇受甲基化的情形，可利用[亞硫酸鹽定序](../Page/亞硫酸鹽定序.md "wikilink")（bisulfite
sequencing）方式測定。DNA甲基化可能使基因沉默化，進而使其失去功能。此外，也有一些生物體內不存在DNA甲基化作用。

## 參考文獻

## 延伸阅读

  - Elias Daura-Oller, Maria Cabre, Miguel A Montero, Jose L Paternain,
    and Antoni Romeu (2009)"Specific gene hypomethylation and cancer:
    New insights into coding region feature trends". Bioinformation.
    2009; 3(8): 340–343.PMID PMC2720671
  - Shen, L. & Waterland, R.A. (2008): *Methods of DNA methylation
    analysis.* In: *Curr. Opin. Clin. Nutr. Metab. Care.* 10(5):576–581.
    PMID 17693740
  - Beck, S. & Rakyan, V.K. (2008): *The methylome: approaches for
    global DNA methylation profiling.* In: *Trends Genet.*
    24(5):231–237. PMID 18325624
  - Shames, D.S. et al.'' (2007): *DNA methylation in health, disease,
    and cancer.* In: *Curr. Mol. Med.* 7(1):85–102. PMID 17311535
    [PDF](https://web.archive.org/web/20100616083035/http://bentham.org/cmm/sample/cmm7-1/0008M.pdf)
  - Patra, S. K. (2008) Ras regulation of DNA-methylation and cancer.
    Exp Cell Res 314(6): 1193-1201.
  - Patra, S.K., Patra, A., Ghosh, T. C. et al. (2008) Demethylation of
    (cytosine-5-C-methyl) DNA and regulation of transcription in the
    epigenetic pathways of cancer development Cancer Metast. Rev. 27(2):
    315-334

## 外部連結

  - [DNA Methylation database](http://www.methdb.net/)

  -
{{-}}

[fr:Méthylation\#La méthylation de
l'ADN](../Page/fr:Méthylation#La_méthylation_de_l'ADN.md "wikilink")

[Category:DNA](../Category/DNA.md "wikilink")
[Category:表觀遺傳學](../Category/表觀遺傳學.md "wikilink")

1.
2.