**-{zh-hans:;zh-tw:;zh-hk:}-山广域市**（），<span>簡稱**蔚山**，</span>是[大韩民国的](../Page/大韩民国.md "wikilink")[广域市之一](../Page/广域市.md "wikilink")，东南部临[朝鮮東海的著名工业城市](../Page/朝鮮東海.md "wikilink")，為韩国第二位企业集团[现代集团的所在地](../Page/现代集团.md "wikilink")，現任市長是[宋哲鎬](../Page/宋哲鎬.md "wikilink")。

蔚山广域市面积為1060.2 km²，人口1,172,925人（2016年）。

过去这里曾是韩国[捕鲸业主要中心](../Page/捕鲸.md "wikilink")。该城拥有[蔚山大学和](../Page/蔚山大学.md "wikilink")[K联赛足球俱乐部](../Page/韩国职业足球联赛.md "wikilink")[蔚山现代](../Page/蔚山现代.md "wikilink")。

## 历史

[三韩时期](../Page/三韩.md "wikilink")，蔚山被称作屈阿火村。757年（[景德王](../Page/景德王.md "wikilink")16年），蔚山成为河曲县，[高丽太祖时升格为兴丽府](../Page/高丽太祖.md "wikilink")，但在991年（[成宗](../Page/高丽成宗.md "wikilink")10年）又被降为恭花县。1018年（[显宗](../Page/高麗顯宗.md "wikilink")9年），高丽在此设防御史。1397年（[太祖](../Page/李成桂.md "wikilink")6年），[朝鲜王朝在蔚山设镇](../Page/朝鲜王朝.md "wikilink")，1413年（太宗13年）改设为蔚山郡。1598年（宣祖31年），因为[义兵在](../Page/義兵_\(朝鮮\).md "wikilink")[壬辰倭乱中的卓越功勋](../Page/壬辰倭乱.md "wikilink")，蔚山被升格为蔚山都护府。1895年，蔚山因地方管制改革，再次被改编为蔚山郡，郡内的部分地区被划归为蔚山面，1931年升格为邑，1962年升格为市。1995年，蔚山郡被并入到蔚山市，1997年升格为目前的蔚山广域市。目前，蔚山广域市下设1郡、4区、2邑、10面、72洞。

## 地理

[태화강,울산.jpg](https://zh.wikipedia.org/wiki/File:태화강,울산.jpg "fig:태화강,울산.jpg")
蔚山广域市位于[朝鲜半岛东南段](../Page/朝鲜半岛.md "wikilink")，东临[韩国东海](../Page/韩国东海.md "wikilink")，西边是[密阳市和](../Page/密阳市.md "wikilink")[庆尚北道](../Page/庆尚北道.md "wikilink")[清道郡](../Page/清道郡.md "wikilink")，南面是[梁山市](../Page/梁山市.md "wikilink")，北面是[庆州市](../Page/庆州市.md "wikilink")。

蔚山东部是由[庆州](../Page/庆州市.md "wikilink")
[吐含山向南延伸出来的东大山脉](../Page/吐含山.md "wikilink")，主要山峰包括东大山（444米）、舞龙山（453米）、天马山（303米）等；西部是由若干1000米以上的高山形成的呈南北走向的天皇山区，主要山峰包括高献山（1003米）、迦智山（1024米）、天皇山（1189米）、载药山（1108米）、鹫栖山（1092米）等。发源于白云山东部溪谷的穿过蔚山市中心在明村洞与东川汇合流入[韩国东海](../Page/韩国东海.md "wikilink")。此外，回夜江也流经蔚山市南部注入东海。蔚山市80%以上的城区都包含在这两大水系之中。蔚山湾内相连有蔚山港、温山港和方鱼津港三个港口。

### 气候

受[韩国东海暖流的影响](../Page/韩国东海.md "wikilink")，以及周围山地对冬季西北风的阻挡，蔚山广域市气候温暖。年均气温在13.5摄氏度左右，1月平均气温0.9摄氏度，8月平均气温25.9摄氏度。年均降水量为1272.4毫米。

## 行政区划

[Ulsan_New_City_Hall.jpg](https://zh.wikipedia.org/wiki/File:Ulsan_New_City_Hall.jpg "fig:Ulsan_New_City_Hall.jpg")
下辖4个区和1个郡，分別是：

  - [北區](../Page/北區_\(蔚山\).md "wikilink")()
  - [東區](../Page/東區_\(蔚山\).md "wikilink")()
  - [中區](../Page/中區_\(蔚山\).md "wikilink")()
  - [南區](../Page/南區_\(蔚山\).md "wikilink")()
  - [蔚州郡](../Page/蔚州郡.md "wikilink") ()

## 工业

该城是蔚山工业区的核心——跨国公司[现代集团的基地](../Page/现代集团.md "wikilink")。1962年以前蔚山只是一个渔港和集散中心。作为韩国的第一个五年经济计划的一部分，蔚山成为开放港口，发展了大型工业，包括[石油精炼](../Page/石油.md "wikilink")、化肥、汽车制造和[重工业](../Page/重工业.md "wikilink")。[造船港口Pangojin於](../Page/造船.md "wikilink")1962年并入该市。

## 教育

  - [蔚山大學](../Page/蔚山大學.md "wikilink")

## 友好城市

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><p><a href="../Page/山口县.md" title="wikilink">山口县</a><a href="../Page/萩市.md" title="wikilink">萩市</a> （1968年）</p></li>
<li><p><a href="../Page/臺灣.md" title="wikilink">臺灣</a><a href="../Page/花蓮縣.md" title="wikilink">花蓮縣</a><a href="../Page/花蓮市.md" title="wikilink">花蓮市</a> （1981年）</p></li>
<li><p><a href="../Page/俄勒岡州.md" title="wikilink">俄勒岡州</a><a href="../Page/波特蘭_(奧勒岡州).md" title="wikilink">波特蘭</a> （1987年）</p></li>
<li><p><a href="../Page/吉林省.md" title="wikilink">吉林省</a><a href="../Page/長春市.md" title="wikilink">長春市</a> （1994年）</p></li>
<li><p><a href="../Page/伊茲密爾.md" title="wikilink">伊茲密爾</a> （2002年）</p></li>
</ul></td>
<td><ul>
<li><p><a href="../Page/桑托斯.md" title="wikilink">桑托斯</a> （2002年）</p></li>
<li><p><a href="../Page/慶和省.md" title="wikilink">慶和省</a> （2002年）</p></li>
<li><p><a href="../Page/托木斯克.md" title="wikilink">托木斯克</a> （2003年）</p></li>
<li><p><a href="../Page/熊本縣.md" title="wikilink">熊本縣</a><a href="../Page/熊本市.md" title="wikilink">熊本市</a> （2010年）</p></li>
<li><p><a href="../Page/曼德勒省.md" title="wikilink">曼德勒省</a><a href="../Page/曼德勒.md" title="wikilink">曼德勒</a> （2017年）</p></li>
</ul></td>
</tr>
</tbody>
</table>

## 参考资料

## 外部链接

  - [官方网站](https://web.archive.org/web/20050703011228/http://chinese.ulsan.go.kr/)

  - <http://blog.daum.net/ulsanlike/11788545> 蔚山大谷里盤 亀台岩刻画

  -
  - [維客旅行上的](../Page/維客旅行.md "wikilink")[蔚山广域市](http://wikitravel.org/en/Ulsan)

  -
  - \[//maps.google.com/maps?q=Ulsan 谷歌地圖\]

[\*](../Category/蔚山廣域市.md "wikilink")
[Category:日本海/朝鮮東海沿海城市](../Category/日本海/朝鮮東海沿海城市.md "wikilink")
[Category:1997年建立的行政區劃](../Category/1997年建立的行政區劃.md "wikilink")