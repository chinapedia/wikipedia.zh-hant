[Yoshitsune_with_benkei.jpg](https://zh.wikipedia.org/wiki/File:Yoshitsune_with_benkei.jpg "fig:Yoshitsune_with_benkei.jpg")\]\]
[A_Fighting_Monk,_Military_Costumes_in_Old_Japan..jpg](https://zh.wikipedia.org/wiki/File:A_Fighting_Monk,_Military_Costumes_in_Old_Japan..jpg "fig:A_Fighting_Monk,_Military_Costumes_in_Old_Japan..jpg")
**僧兵**（そうへい）是[日本](../Page/日本.md "wikilink")[平安時代中期到](../Page/平安時代.md "wikilink")[安土桃山時代存在的武裝的](../Page/安土桃山時代.md "wikilink")[寺院僧徒](../Page/寺院.md "wikilink")。

## 概要

在同時代被稱為法師武者、僧眾、惡僧，[江戶時代以降使用](../Page/江戶時代.md "wikilink")「僧兵」的稱呼。惡僧的「惡」和[中世的](../Page/中世.md "wikilink")[惡黨相同](../Page/惡黨.md "wikilink")，有「兇惡」、「強大」之意味。主要是[寺社勢力所屬的武裝集團](../Page/寺社勢力.md "wikilink")。風貌如同[繪卷物所描繪](../Page/繪卷物.md "wikilink")，有[裹頭](../Page/裹頭.md "wikilink")、[高下駄](../Page/高下駄.md "wikilink")、[薙刀等特徵](../Page/薙刀.md "wikilink")。另外，神社所屬的武裝集團稱作[神人](../Page/神人.md "wikilink")（じにん）。

僧兵、神人出現的時代，[律令制崩壞](../Page/律令制.md "wikilink")，社會混亂，擁有廣大的[寺領](../Page/寺領.md "wikilink")・[神領的寺社因為有富裕的經濟力](../Page/神領.md "wikilink")，有成為盗賊或各種勢力目標的危險性。所以，寺社有保持防衛武力的必要性。在這種時代背景下，造成「寺院・神社的武裝化」。

在[京都](../Page/京都.md "wikilink")・[奈良的大寺院服雜役的大眾](../Page/奈良市.md "wikilink")（[堂眾](../Page/堂眾.md "wikilink")）武裝自衛，[平安時代末期](../Page/平安時代.md "wikilink")，成為強大的武力集團，以[興福寺](../Page/興福寺.md "wikilink")・[延曆寺](../Page/延曆寺.md "wikilink")・[園城寺](../Page/園城寺.md "wikilink")、[東大寺等](../Page/東大寺.md "wikilink")[寺院為據點](../Page/寺院.md "wikilink")，經常互相爭奪勢力或對[朝廷](../Page/朝廷.md "wikilink")、[攝關家進行](../Page/攝家.md "wikilink")[強訴](../Page/強訴.md "wikilink")。[以仁王舉兵之時也參加對](../Page/以仁王舉兵.md "wikilink")[平家的戰爭](../Page/平家.md "wikilink")，在『[平家物語](../Page/平家物語.md "wikilink")』的武藏坊[辨慶可見這樣的描寫](../Page/辨慶.md "wikilink")。其中，興福寺（南都）的眾徒被稱作奈良法師、延曆寺（北嶺）的眾徒被稱作山法師。[白河法皇曾舉出天下之三不如意](../Page/白河天皇.md "wikilink")，就是「[賀茂川之水](../Page/賀茂川.md "wikilink")（[鴨川洪災](../Page/鴨川_\(淀川水系\).md "wikilink")）・双六之賽（[雙陸棋賭博滋生社會問題](../Page/雙陸棋.md "wikilink")）・山法師（比叡山僧兵眾）」，可見僧兵的橫暴是朝廷的不安要素。

遠離中央的地域也有和地方軍事力結合的有力寺社，對當時的權力平衡帶來相當大的影響。[源平爭亂之時](../Page/源平合戰.md "wikilink")，指揮[熊野水軍的](../Page/熊野水軍.md "wikilink")[熊野別當成為雙方爭取的對象是著名的例子](../Page/熊野別當.md "wikilink")。

[室町時代](../Page/室町時代.md "wikilink")，曾經作為[天台座主的](../Page/天台座主.md "wikilink")[足利義教](../Page/足利義教.md "wikilink")，熟知僧兵的軍事力和破壞力，所以，大規模的討伐延曆寺（如同後年的[織田信長](../Page/織田信長.md "wikilink")）。

各地的有力寺社保持軍事力的現象持續到[豐臣秀吉實施](../Page/豐臣秀吉.md "wikilink")[刀狩為止](../Page/刀狩.md "wikilink")。[戰國時代的有力僧兵團有以下之例](../Page/戰國時代_\(日本\).md "wikilink")。

### 南都北嶺

  - [興福寺](../Page/興福寺.md "wikilink")・・・[寶藏院流槍術之開祖](../Page/寶藏院流槍術.md "wikilink")・[胤榮等廣為人知](../Page/胤榮.md "wikilink")。
  - [延曆寺](../Page/延曆寺.md "wikilink")・・・織田信長的[比叡山燒討](../Page/比叡山燒討_\(1571年\).md "wikilink")。

### 其他

都在江戶時代之前喪失軍事力或分割宗教和軍事力。

  - [根來寺](../Page/根來寺.md "wikilink")・・・[津田流砲術之開祖](../Page/津田流砲術.md "wikilink")・[津田算長為首](../Page/津田算長.md "wikilink")，有名的強大[鐵炮隊](../Page/火繩槍.md "wikilink")。[豐臣秀吉在](../Page/豐臣秀吉.md "wikilink")[紀州征伐時制壓](../Page/紀州征伐.md "wikilink")，喪失軍事力。
  - [石山本願寺](../Page/石山本願寺.md "wikilink")・・・除了獨自的僧兵集團，擁有動員各地門徒（多為[國人或庶民](../Page/國人_\(日本\).md "wikilink")）發起[一向一揆的絕大影響力](../Page/一向一揆.md "wikilink")・軍事力，[石山合戰失敗後壞滅](../Page/石山合戰.md "wikilink")，喪失軍事力。
  - [諏訪大社](../Page/諏訪大社.md "wikilink")・・・被[武田信玄制壓](../Page/武田信玄.md "wikilink")，喪失軍事力。
  - [宇佐神宮](../Page/宇佐神宮.md "wikilink")・・・被[大友宗麟制壓](../Page/大友義鎮.md "wikilink")，喪失軍事力。
  - [日光山](../Page/日光山.md "wikilink")[輪王寺](../Page/輪王寺.md "wikilink")・・・擁有多數僧兵，在[下野是有數的武裝勢力](../Page/下野國.md "wikilink")。協助[後北條氏](../Page/後北條氏.md "wikilink")，所以被[豐臣秀吉彈壓](../Page/豐臣秀吉.md "wikilink")，寺領被没收衰退。受移封[關東的](../Page/關東地方.md "wikilink")[德川家康保護](../Page/德川家康.md "wikilink")，回復勢力。
  - [多武峰妙樂寺](../Page/談山神社.md "wikilink")・・・數度和[興福寺發生武力衝突](../Page/興福寺.md "wikilink")。[天正](../Page/天正_\(日本\).md "wikilink")13年（1585年），同意[大和入國的](../Page/大和國.md "wikilink")[豐臣秀長提出的武裝解除要求](../Page/豐臣秀長.md "wikilink")，供出武具。
  - [白山平泉寺](../Page/平泉寺白山神社.md "wikilink")・・・延曆寺的[末寺](../Page/末寺.md "wikilink")，最盛期擁有8千人的僧兵，勢力延伸到[越前](../Page/越前國.md "wikilink")。天正2年（1574年），在一向一揆的抗爭中全山燒失。勢力減弱。和[織田信長](../Page/織田信長.md "wikilink")、[豐臣秀吉合作](../Page/豐臣秀吉.md "wikilink")，回復寺領。
  - [聖眾寺](../Page/聖眾寺_\(桑名市\).md "wikilink")・・・擁有千人僧兵的[真言宗大寺院](../Page/真言宗.md "wikilink")，[天正年間](../Page/天正_\(日本\).md "wikilink")（1573年
    -
    1593年），在[織田信長家臣](../Page/織田信長.md "wikilink")[瀧川一益的北伊勢侵攻之際](../Page/瀧川一益.md "wikilink")，遭到燒討，受到壞滅的打擊。
  - [三岳寺](../Page/三岳寺.md "wikilink")・・・[天台宗](../Page/天台宗.md "wikilink")[山門派的大寺院](../Page/山門派.md "wikilink")，擁有數百人的僧兵，織田信長進行[比叡山燒討的](../Page/比叡山燒討.md "wikilink")3年前，[永祿](../Page/永祿.md "wikilink")11年（1568年），被織田信長家臣瀧川一益攻擊，寺院全數燒失。
  - [英彦山](../Page/英彦山.md "wikilink")・・・隆盛的[修驗道場](../Page/修驗道.md "wikilink")，最盛期擁有數千名僧兵。[天正](../Page/天正_\(日本\).md "wikilink")9年（1581年），受到[大友義統的](../Page/大友義統.md "wikilink")[彦山燒討而戰敗](../Page/彦山燒討.md "wikilink")，勢力減弱。

## 關連項目

  - [寺社勢力](../Page/寺社勢力.md "wikilink")

## 外部連結

  - \[<https://web.archive.org/web/20110928153522/http://www2.town.komono.mie.jp/menu1478.html>　僧兵祭　三重縣菰野町\]

[Category:平安時代](../Category/平安時代.md "wikilink")
[Category:日本僧人](../Category/日本僧人.md "wikilink")
[Category:日本佛教史](../Category/日本佛教史.md "wikilink")
[Category:日本軍事人物](../Category/日本軍事人物.md "wikilink")