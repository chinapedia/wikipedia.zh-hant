**北海福成机场**（）是[中國一個國內](../Page/中國.md "wikilink")[機場](../Page/機場.md "wikilink")，位于[广西壮族自治区](../Page/广西壮族自治区.md "wikilink")[北海市](../Page/北海市.md "wikilink")[福成镇](../Page/福成镇_\(北海市\).md "wikilink")。

## 主航站樓

主航站樓一共有[二層](../Page/二.md "wikilink")，呈長翼形，[一層為國內到達大廳](../Page/一.md "wikilink")、行李提取區、出入口，[二層為國內出發大廳](../Page/二.md "wikilink")、商場、值機櫃檯、安檢口、候機廳、登機口。

## 貨運區

貨運區位於[機場北區](../Page/機場.md "wikilink")，一共有[三層](../Page/三.md "wikilink")，[一層為出入口](../Page/一.md "wikilink")、貨運登記中心，[二層至](../Page/二.md "wikilink")[三層為](../Page/三.md "wikilink")[寫字樓](../Page/辦公樓.md "wikilink")。

## 公務樓

公務樓一共有[二層](../Page/二.md "wikilink")，呈长方形，[一層為公務機到達大廳](../Page/一.md "wikilink")、行李提取區、出入口，[二層為公務機出發大廳](../Page/二.md "wikilink")、商場、公務機專用值機櫃檯、安檢口、候機廳、登機口。

## 航線

2017夏秋航季

## 相关条目

  - [中国大陆机场列表](../Page/中国大陆机场列表.md "wikilink")

[Category:廣西機場](../Category/廣西機場.md "wikilink")
[Category:北海建筑物](../Category/北海建筑物.md "wikilink")
[Category:北海交通](../Category/北海交通.md "wikilink")
[Category:1987年啟用的機場](../Category/1987年啟用的機場.md "wikilink")