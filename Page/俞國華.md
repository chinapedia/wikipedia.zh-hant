<table>
<thead>
<tr class="header">
<th><p>colspan="2" style="background: #CFCFCF; text-align: center" {{!}} 學歷</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><div style="text-align: left">
<ul>
<li><a href="../Page/國立清華大學.md" title="wikilink">國立清華大學畢業</a><br />
<span style="color: blue;">(1934年)</span></li>
<li>美國<a href="../Page/哈佛大學.md" title="wikilink">哈佛大學研究所碩士</a><br />
<span style="color: blue;">(1946年)</span></li>
<li>英國<a href="../Page/倫敦大學.md" title="wikilink">倫敦大學</a><a href="../Page/政治經濟學院.md" title="wikilink">政治經濟學院研究</a><br />
<span style="color: blue;">(1947年)</span></li>
</ul>
</div></td>
</tr>
<tr class="even">
<td><p>colspan="2" style="background: #CFCFCF; text-align: center" {{!}} 經歷</p></td>
</tr>
<tr class="odd">
<td><div style="text-align: left">
<ul>
<li>（國民政府）蔣委員長侍從室秘書<br />
<span style="color: blue;">(1936年-1944年)</span></li>
<li><a href="../Page/國際復興開發銀行.md" title="wikilink">國際復興開發銀行副執行董事</a><br />
<span style="color: blue;">(1947年-1950年)</span></li>
<li><a href="../Page/國際貨幣基金會.md" title="wikilink">國際貨幣基金會副執行董事</a><br />
<span style="color: blue;">(1951年-1955年)</span></li>
<li><a href="../Page/中央信託局.md" title="wikilink">中央信託局局長</a><br />
<span style="color: blue;">(1955年-1961年)</span></li>
<li>中華開發信託公司常務董事<br />
<span style="color: blue;">(1959年-1967年)</span></li>
<li><a href="../Page/中國國際商業銀行.md" title="wikilink">中國國際商業銀行董事長</a><br />
<span style="color: blue;">(1961年-1967年)</span></li>
<li><a href="../Page/中國產物保險公司.md" title="wikilink">中國產物保險公司董事長</a><br />
<span style="color: blue;">(1961年-1967年)</span></li>
<li>國際復興開發銀行副理事<br />
<span style="color: blue;">(1964年-1967年)</span></li>
<li><a href="../Page/財政部.md" title="wikilink">財政部</a>（第十任）部長<br />
<span style="color: blue;">(1967年12月4日-1969年7月4日)</span></li>
<li>國際復興開發銀行中國理事<br />
<span style="color: blue;">(1967年-1969年)</span></li>
<li><a href="../Page/政務委員.md" title="wikilink">行政院政務委員</a><br />
<span style="color: blue;">(1967年12月4日-1984年6月1日)</span></li>
<li><a href="../Page/中央銀行.md" title="wikilink">中央銀行總裁</a><br />
<span style="color: blue;">(1969年6月25日-1984年5月30日)</span></li>
<li><a href="../Page/亞洲開發銀行.md" title="wikilink">亞洲開發銀行中國理事</a><br />
<span style="color: blue;">(1969年-1984年)</span></li>
<li>國際貨幣基金會中國理事<br />
<span style="color: blue;">(1969年-1980年)</span></li>
<li><a href="../Page/行政院經濟建設委員會.md" title="wikilink">行政院經濟建設委員會主任委員</a><br />
<span style="color: blue;">(1977年12月1日-1984年6月1日)</span></li>
<li><a href="../Page/行政院院長.md" title="wikilink">行政院院長</a><br />
<span style="color: blue;">(1984年6月1日-1989年6月1日)</span></li>
<li><a href="../Page/總統府資政.md" title="wikilink">總統府資政</a><br />
<span style="color: blue;">(1989年6月1日-2000年10月4日)</span></li>
<li>中國國民黨中央常務委員<br />
<span style="color: blue;">(1979年)</span></li>
<li>中國國民黨副主席兼黨產管理主任<br />
<span style="color: blue;">(1997年-2000年10月4日)</span></li>
<li><a href="../Page/蔣經國國際學術交流基金會.md" title="wikilink">蔣經國國際學術交流基金會董事長</a><br />
<span style="color: blue;">(1989年-2000年10月4日)</span></li>
</ul>
</div></td>
</tr>
</tbody>
</table>

**俞國華**（），[浙江省](../Page/浙江省.md "wikilink")[奉化縣人](../Page/奉化.md "wikilink")，與[中華民國前總統](../Page/中華民國.md "wikilink")[蔣中正及](../Page/蔣中正.md "wikilink")[蔣經國同鄉](../Page/蔣經國.md "wikilink")，[中華民國政治人物](../Page/中華民國.md "wikilink")。因為與[蔣中正份屬同鄉](../Page/蔣中正.md "wikilink")，進入其侍從室工作，後留學英美深造。1955年返臺從政。日後在財金方面大展長才，與前[行政院長](../Page/行政院長.md "wikilink")[孫運璿](../Page/孫運璿.md "wikilink")、前財政部長[李國鼎](../Page/李國鼎.md "wikilink")、前主計長[周宏濤等](../Page/周宏濤.md "wikilink")，並稱臺灣經濟奇蹟最關鍵的推手。1984年5月，行政院長[孫運璿中風辭去院長職](../Page/孫運璿.md "wikilink")，俞國華以[中央銀行總裁兼](../Page/中央銀行.md "wikilink")[行政院經濟建設委員會主任委員身份](../Page/行政院經濟建設委員會.md "wikilink")，繼任[行政院長](../Page/行政院長.md "wikilink")。卸下院長職後，曾任[總統府資政及](../Page/總統府資政.md "wikilink")[中國國民黨副主席](../Page/中國國民黨.md "wikilink")。俞先生逝世七年後，時任中央銀行總裁的[彭淮南猶於正式場合](../Page/彭淮南.md "wikilink")，詳述俞國華於金融自由化、制度化與國際化方面，對國家貢獻隆崇的事蹟\[1\]。

與[俞飛鵬](../Page/俞飛鵬.md "wikilink")、[俞濟時合稱為](../Page/俞濟時.md "wikilink")「[奉化三俞](../Page/奉化三俞.md "wikilink")」。

## 早年生平

俞父[俞作屏與](../Page/俞作屏.md "wikilink")[蔣中正為中學同學](../Page/蔣中正.md "wikilink")，曾任蔣氏的[秘書](../Page/秘書.md "wikilink")，後於[陳炯明與](../Page/陳炯明.md "wikilink")[廣州方面內戰時殉職](../Page/廣州.md "wikilink")。俞國華先後入錦溪中學、[舟山](../Page/舟山.md "wikilink")[定海中學](../Page/定海.md "wikilink")、[寧波](../Page/寧波.md "wikilink")[省立第四中學高中部](../Page/宁波中学.md "wikilink")、上海[光華大學](../Page/光華大學.md "wikilink")、[北平](../Page/北平.md "wikilink")[清華大學](../Page/清華大學.md "wikilink")；1934年夏畢業於清華大學政治學系，7月任職於[國民政府軍事委員會](../Page/國民政府軍事委員會.md "wikilink")[南昌行營](../Page/南昌行營.md "wikilink")，開啟了俞國華在蔣中正身邊服務的日子。

1935年起，俞國華先後任職於軍委會武昌行營、重慶行營、[軍事委員會委員長侍從室秘書](../Page/軍事委員會委員長侍從室.md "wikilink")。1936年6月發生[兩廣事變](../Page/兩廣事變.md "wikilink")，旋即赴廣州處理相關事宜，未幾發生[西安事變](../Page/西安事變.md "wikilink")，亦被禁足兩週旋即被釋放回[南京](../Page/南京.md "wikilink")。1937年[抗日軍興](../Page/抗日戰爭_\(中國\).md "wikilink")，俞國華隨統帥部西遷，期間並隨同蔣中正與蔣宋美齡外訪[印度](../Page/印度.md "wikilink")[甘地](../Page/甘地.md "wikilink")、見習[開羅會議等事](../Page/開羅會議.md "wikilink")。1944年5月奉派赴美進修；9月入[哈佛大學研究院主修國際財政金融](../Page/哈佛大學.md "wikilink")；1946年轉赴英國[倫敦政經學院](../Page/倫敦政經學院.md "wikilink")；1947年赴美出任[華盛頓](../Page/華盛頓.md "wikilink")[國際復興開發銀行副執行董事](../Page/國際復興開發銀行.md "wikilink")；1951年1月，任[國際貨幣基金會副執行董事](../Page/國際貨幣基金會.md "wikilink")。

## 回國服務

俞國華於1955年自美返國，旋即出任[中央信託局局長](../Page/中央信託局.md "wikilink")，於任期內致力對外貿易，主辦[臺糖](../Page/臺糖.md "wikilink")、[臺鹽](../Page/臺鹽.md "wikilink")、米糧外銷業務，對象是[日本](../Page/日本.md "wikilink")。後業務逐漸擴展至[棉織品](../Page/棉織品.md "wikilink")、[五金等輕工業品](../Page/五金.md "wikilink")，市場亦延伸及[南韓](../Page/南韓.md "wikilink")、[泰國](../Page/泰國.md "wikilink")、[美國](../Page/美國.md "wikilink")、[西歐](../Page/西歐.md "wikilink")，並增設[新加坡代表處](../Page/新加坡.md "wikilink")。

1957年秋，應美政府之邀赴美研究資本形成問題，歸國後建議國府成立開發公司與證券交易所，使民間資金透過證券市場導入生產。1958年初，國府成立中華開發公司籌備處，俞國華奉派兼任主任委員，同年十月赴[印度](../Page/印度.md "wikilink")[新德里](../Page/新德里.md "wikilink")，出席國際復興開發銀行及國際貨幣基金年會；1960年2月赴[越南共和國出席中越經濟合作會議](../Page/越南共和國.md "wikilink")。1961年，俞國華轉任[中國銀行董事長](../Page/中國國際商業銀行.md "wikilink")（2006年已改組為[兆豐國際商業銀行](../Page/兆豐國際商業銀行.md "wikilink")）並兼任[中國產物保險公司](../Page/中國產物保險公司.md "wikilink")(今兆豐產物保險公司)董事長，積極拓展國際金融，過後數年赴南美各國訪問，並在1964年赴[日本](../Page/日本.md "wikilink")[東京出席國際復興開發銀行年會](../Page/東京.md "wikilink")。

## 財稅重臣

1967年12月，奉派為[中華民國財政部長](../Page/中華民國財政部.md "wikilink")，因蔣中正決定實施九年[國民義務教育](../Page/國民義務教育.md "wikilink")，所需經費極為龐大，為籌措財源，乃實施加稅計畫，以達成預算平衡。1969年4月當選中國國民黨第十屆中央委員；5月兼任[中華民國行政院力行小組召集人](../Page/中華民國行政院.md "wikilink")，負責對[中華人民共和國經濟作戰之策劃](../Page/中華人民共和國.md "wikilink")。

當年6月，俞國華接任[中央銀行總裁](../Page/中央銀行.md "wikilink")，兼任國際貨幣基金理事及亞洲開發銀行理事，在央行任內，俞維持貨幣供給穩定增加，並抑制臺灣在1973年、1979年兩次[石油危機所引發的物價狂飆](../Page/石油危機.md "wikilink")；建造央行辦公大樓，更新中央造幣廠、印製廠，並實施[利率自由化](../Page/利率.md "wikilink")，訂定銀行利率調整要點。1970年6月，俞兼任中國國民黨文化經濟事業管理委員會主任委員。

1972年4月，俞國華訪[西德](../Page/西德.md "wikilink")、[瑞士](../Page/瑞士.md "wikilink")、[比利時](../Page/比利時.md "wikilink")，5月任國民黨財務委員會主任委員。7月7日，[中](../Page/中华民国.md "wikilink")[日断交](../Page/日本.md "wikilink")，此局勢逆轉之下，中華民國在[亞洲開發銀行的地位岌岌可危](../Page/亞洲開發銀行.md "wikilink")。以中央銀行總裁出任中華民國亞銀理事的俞國華極為重視日本對亞銀的影響，由於每屆亞銀年會，日本大藏大臣均是年會最舉足輕重的貴賓，俞訪問日本時，每次都設法與大藏大臣[竹下登謀面](../Page/竹下登.md "wikilink")，以聯絡感情；但日本已與北京建交，殊不願與臺北官方接觸。但因中華民國駐日副代表[林金莖與竹下登同為](../Page/林金莖.md "wikilink")[早稻田大學校友](../Page/早稻田大學.md "wikilink")，兩人商議後，暫時不提及北京入會案，中華民國在亞銀的地位得以未受動搖。

1973年10月獲美國[聖若望大學頒榮譽商業博士學位](../Page/聖若望大學.md "wikilink")。1977年12月，中華民國行政院成立[經濟建設委員會](../Page/經濟建設委員會.md "wikilink")，俞奉命兼任主任委員，負責整體經濟決策及規劃各項發展計劃，主掌[臺灣經濟快速轉型的政策導引](../Page/臺灣經濟.md "wikilink")。

1978年7月，[中華民國總統](../Page/中華民國總統.md "wikilink")[蔣經國以俞國華在財政部長任內改革稅制](../Page/蔣經國.md "wikilink")，為九年國教提供財政後盾，卓有勳績；其後任央行總裁兼任經建會主委任內，主持經濟金融政策，協助完成[十大建設](../Page/十大建設.md "wikilink")，使經濟快速成長，兼保有物價穩定，特授於一等[景星勳章](../Page/景星勳章.md "wikilink")，以酬其功績。隨後於8月出訪[哥倫比亞](../Page/哥倫比亞.md "wikilink")，1979年3月又應[馬可仕之邀](../Page/馬可仕.md "wikilink")，出訪[菲律賓](../Page/菲律賓.md "wikilink")，同年12月當選國民黨第11屆中央常務委員；1981年8月又率團前往[史瓦濟蘭和](../Page/史瓦濟蘭.md "wikilink")[南非](../Page/南非.md "wikilink")，洽談經濟合作事宜，該年11月，赴美出席兩國經濟協會聯席年會。

## 行政院長

### 任內

1984年6月，俞國華出任行政院長，任內臺灣政治有偌大變遷：

  - 一、在政治方面：宣佈解除[戒嚴令](../Page/戒嚴令.md "wikilink")，開放[黨禁](../Page/黨禁.md "wikilink")，解除[報禁](../Page/報禁.md "wikilink")，開放[臺澎金馬人民前往](../Page/臺澎金馬.md "wikilink")[中國大陸探親](../Page/中國大陸.md "wikilink")，制訂第一屆中央民意代表自願退職條例，修訂集會遊行法。
  - 二、經濟方面：連續五年平均經濟成長率是9.9%，外匯存底由150億美元增至760億美元，躍居世界第二，僅次於[日本](../Page/日本.md "wikilink")；此期臺灣物價10倍的飛漲，為[二戰結束以來僅見](../Page/二戰結束.md "wikilink")。
  - 三、財政方面：廢除[支票刑罰](../Page/支票.md "wikilink")，實施新制[營業稅](../Page/營業稅.md "wikilink")，解除[外匯管制](../Page/外匯.md "wikilink")，廢除[屠宰稅](../Page/屠宰稅.md "wikilink")。
  - 四、成立[勞工委員會](../Page/勞工委員會.md "wikilink")，處理勞資糾紛，停納[田賦](../Page/田賦.md "wikilink")，開辦[農民健康保險](../Page/農民健康保險.md "wikilink")。
  - 五、於中央設置[環保署](../Page/環保署.md "wikilink")，地方設[環保局](../Page/環保局.md "wikilink")，分層推動環境保護。

1987年6月8日至6月12日，與夫人訪問新加坡。\[2\]1988年，俞率特使團赴[韓國](../Page/韓國.md "wikilink")[漢城](../Page/漢城.md "wikilink")，慶賀[盧泰愚總統就職](../Page/盧泰愚.md "wikilink")；7月當選中國國民黨第13屆中央委員。1989年1月，率團訪中南美[巴哈馬](../Page/巴哈馬.md "wikilink")、[瓜地馬拉](../Page/瓜地馬拉.md "wikilink")、[多明尼加](../Page/多明尼加.md "wikilink")，與巴哈馬總理簽訂建交文件，兩國正式建立外交關係。

### 下台

俞國華內閣組成之初，臺灣爆出連連弊案及重大災難，包括二次大水災、1984年6至12月的三次重大[礦災](../Page/礦難.md "wikilink")（土城[海山煤礦](../Page/海山煤礦.md "wikilink")、三峽[海山一坑](../Page/海山一坑.md "wikilink")、瑞芳[煤山煤礦礦災](../Page/煤山煤礦礦災.md "wikilink")），以及[江南案及](../Page/江南案.md "wikilink")[十信案的爆發](../Page/十信案.md "wikilink")，俞內閣也被批為「災難內閣」。除了影響俞國華在民間聲望外，也給黨務系統如[李煥勢力有了發揮力量](../Page/李煥.md "wikilink")。1987年2月24日，國民黨資深立委[吳春晴在立法院第](../Page/吳春晴.md "wikilink")79會期質詢中，公開指出俞國華在外包[酒家女](../Page/酒家女.md "wikilink")。\[3\]引起外界軒然大波。

[蔣經國逝世後](../Page/蔣經國.md "wikilink")，俞國華在[李登輝代理主席案中捲入](../Page/李登輝.md "wikilink")[政爭](../Page/二月政爭.md "wikilink")，由於俞國華被外界視為在代理主席案遭[蔣宋美齡施壓](../Page/蔣宋美齡.md "wikilink")，導致俞氏在中常會上遭到只有列席資格的[宋楚瑜指責](../Page/宋楚瑜.md "wikilink")。雖然最後通過李登輝代理主席案，然而對俞已造成打擊，成為去職原因之一。

1988年7月，中國國民黨舉行十三全會，會前國民黨內部則形成「擁俞」及「倒俞」的政爭。然而掌握黨機器權力的秘書長[李煥在十三全會期間選舉中進行動員](../Page/李煥.md "wikilink")，以第一高票當選中央委員，而時任[行政院院長俞國華卻只屈居](../Page/行政院.md "wikilink")35名。而俞國華則在[立法院中受到李煥勢力的強烈攻擊](../Page/立法院.md "wikilink")，最後選擇提出辭呈。\[4\]

1989年5月17日，俞國華公佈「辭職三理由」，表示為了黨內團結、為了給李總統重新安排內閣的機會、為了讓年輕人接棒，宣布辭去行政院長職務。最後李登輝於總統府召見李煥，李煥隨即對外放出組閣新聞。最後李煥於1989年6月1日組閣。

在政爭中，俞國華夫人董梅貞更在接受中視夜間新聞訪問時，說出了「政治太可怕了！」此一名言。並表示「早點離開這一是非之地」。\[5\]

俞國華下台後，總統[李登輝特頒以一等](../Page/李登輝.md "wikilink")[卿雲勳章](../Page/卿雲勳章.md "wikilink")，同月受聘為[中華民國總統府資政](../Page/中華民國總統府資政.md "wikilink")。

## 晚年

俞國華受聘總統府資政一年後，在1990年5月率特使團赴哥斯大黎加，代表中華民國總統參加新任總統卡特隆就職典禮。1991年12月，俞擔任中華民國國家統一促進會首任理事長；1993年2月，獲[倫敦政經學院頒授傑出校友獎狀](../Page/倫敦政經學院.md "wikilink")，同年成立「財團法人俞國華文教基金會」以資助財政、金融、經濟等議題的文化教育事業；1994年[國立清華大學成立](../Page/國立清華大學.md "wikilink")「俞國華講座」以鼓勵清大傑出研究人材，並成立「俞國華獎學金」以獎勵清大品學兼優的學子。1996年膺任中國國民黨副主席。2000年9月因肺炎住院，10月4日心肺衰竭病逝，10日13日[陳水扁總統明令褒揚](../Page/陳水扁.md "wikilink")。

褒揚令原文為：

## 其他

  - 曾任中華民國總統的[李登輝曾回憶](../Page/李登輝.md "wikilink")：「[蔣經國過世前跟我說過一句話](../Page/蔣經國.md "wikilink")，他說俞國華這個人要重用。這個人是老實人，絕不會做狠事。但是俞國華做不到一年，就被[李煥用立法委員煽動](../Page/李煥.md "wikilink")，把他拉下來。」\[6\]

## 榮譽

  - [美國](../Page/美國.md "wikilink")[聖若望大學頒榮譽商業博士學位](../Page/聖若望大學.md "wikilink")：1973年10月，獲[美國](../Page/美國.md "wikilink")[聖若望大學頒榮譽商業博士學位](../Page/聖若望大學.md "wikilink")。
  - [一等景星勳章](../Page/一等景星勳章.md "wikilink")：1978年7月，中華民國總統蔣經國以俞國華在財政部長任內勳績，特授於一等景星勳章，以酬其功績。
  - [一等卿雲勳章](../Page/一等卿雲勳章.md "wikilink")：1989年5月，俞辭行政院長職，當時總統李登輝特頒以一等卿雲勳章，同月受聘於中華民國總統府資政。
  - [倫敦政經學院傑出校友](../Page/倫敦政經學院.md "wikilink")：1993年2月，獲倫敦政經學院頒授傑出校友獎狀。

## 家庭

## 注釋

## 相關條目

  - [加工出口工廠](../Page/加工出口工廠.md "wikilink")

## 參考資料及外部連結

  - 〈民國人物小傳－俞國華〉，《傳記文學》第77卷第6期（2000年12月），頁139-141。
  - 喻蓉蓉〈首相外交憶餘——林金莖口述歷史之外〉，《歷史月刊》第191期，頁42-43。
  - [俞國華文教基金會](https://web.archive.org/web/20050308225711/http://www.foundations.org.tw/list/show.asp?OrgID=137)
  - [俞國華獎學金](http://www.nucl.nthu.edu.tw/ipns/scholarship.html)
  - [清大數位校史館：名譽博士俞國華](https://web.archive.org/web/20090401050009/http://archives.lib.nthu.edu.tw/history/about/3-03/3-03-10.htm)
  - [俞國華開啟金融國際化
    彭淮南蕭萬長同追思](http://www.epochtimes.com/b5/7/6/16/n1746024.htm)
  - [俞國華（文建會）](http://taipedia.cca.gov.tw/index.php?title=%E4%BF%9E%E5%9C%8B%E8%8F%AF%EF%BC%88%E6%96%87%E5%BB%BA%E6%9C%83%EF%BC%89)
  - [張心洽先生小傳](https://web.archive.org/web/20090920185629/http://www.felixchang.org/1856.3021_0i00000p0.pdf)
  - [財經巨擘：俞國華生涯行腳](http://library.drnh.gov.tw/bookDetail.jsp?id=51869)
  - [中華民國行政院歷任正副院長介紹](https://web.archive.org/web/20090912184920/http://www.ey.gov.tw/content.asp?CuItem=4237&mp=1)

|- |colspan="3"
style="text-align:center;"|**[Executive_Yuan,ROC_LOGO.svg](https://zh.wikipedia.org/wiki/File:Executive_Yuan,ROC_LOGO.svg "fig:Executive_Yuan,ROC_LOGO.svg")[行政院](../Page/行政院.md "wikilink")**
|-

[Y俞](../Category/行政院院長.md "wikilink")
[Y俞](../Category/中華民國財政部部長.md "wikilink")
[Y俞](../Category/中華民國中央銀行總裁.md "wikilink")
[Y俞](../Category/中華民國總統府資政.md "wikilink")
[Y俞](../Category/獲頒授景星勳章者.md "wikilink")
[Y俞](../Category/中國國民黨副主席.md "wikilink")
[Y俞](../Category/清华大学校友.md "wikilink")
[Y俞](../Category/國立清華大學校友.md "wikilink")
[Y俞](../Category/奉化人.md "wikilink")
[G](../Category/奉化三俞.md "wikilink")
[Y俞](../Category/台灣戰後寧波移民.md "wikilink")
[G國](../Category/俞姓.md "wikilink")
[Category:中華民國行政院經濟建設委員會主任委員](../Category/中華民國行政院經濟建設委員會主任委員.md "wikilink")
[Y俞](../Category/中國國民黨中央委員會委員.md "wikilink")
[Category:冷戰時期領袖](../Category/冷戰時期領袖.md "wikilink")

1.  [俞國華先生在央行行誼](http://www.cbc.gov.tw/public/Attachment/83209391771.pdf)
2.  [俞國華院長訪問新加坡紀實](http://history.ey.gov.tw/Item/VideoDetail/e0f1b5d3-7239-42bb-847e-ada7f5088c81)
3.  陸雁，\<俞國華的祕密情人？\>，《新觀點週刊》48期，1987年3月2日。
4.  [若林正丈](../Page/若林正丈.md "wikilink")，《戰後台灣政治史》，台北：國立台灣大學出版中心，2014年，217頁。
5.  [陳明通](../Page/陳明通.md "wikilink")，《地方派系與臺灣政治變遷》，台北：月旦出版，1995年，197頁。
6.  [台灣民主化之路](https://www.youtube.com/watch?v=Sic3TeRP3Oo)0分30秒處