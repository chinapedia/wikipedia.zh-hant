**太仓直隶州**是[清代](../Page/清朝.md "wikilink")[江苏省的一个](../Page/江蘇省_\(清\).md "wikilink")[直隶州](../Page/直隶州.md "wikilink")，在今[江苏省](../Page/江苏省.md "wikilink")[苏州市](../Page/苏州市.md "wikilink")、[南通市](../Page/南通市.md "wikilink")、及[上海市境](../Page/上海市.md "wikilink")。辛亥革命后，废除。

在[明代](../Page/明代.md "wikilink")，[太仓州隶属于](../Page/太仓州.md "wikilink")[南直隶](../Page/南直隶.md "wikilink")[苏州府](../Page/苏州府.md "wikilink")。[清朝](../Page/清朝.md "wikilink")[雍正二年](../Page/雍正.md "wikilink")（1724年），太仓州升格为直隶州\[1\]。同时因人口、赋税繁多，而增设县份：分太仓州北部设[镇洋县](../Page/镇洋县.md "wikilink")（[同城而治](../Page/治所同城.md "wikilink")）；分[嘉定县设](../Page/嘉定县.md "wikilink")[宝山县](../Page/宝山县.md "wikilink")（次年[县治定为](../Page/县治.md "wikilink")[吴淞所城](../Page/吴淞所城.md "wikilink")），因此太仓直隶州下辖镇洋县、嘉定县、宝山县、[崇明县四县](../Page/崇明县.md "wikilink")。辖区范围基本上相当于今日苏州市辖[太仓市全境](../Page/太仓市.md "wikilink")、上海市[苏州河以北各区县](../Page/苏州河.md "wikilink")（除北岸狭长地带属于[上海县](../Page/上海县.md "wikilink")），及[南通市辖](../Page/南通市.md "wikilink")[启东市南部](../Page/启东市.md "wikilink")（当时属崇明县）。

清代考评[繁，疲，难](../Page/衝繁疲難.md "wikilink")\[2\]。[宣统三年](../Page/宣统.md "wikilink")（1911年），[辛亥革命爆发](../Page/辛亥革命.md "wikilink")。同年11月，[江苏巡抚](../Page/江苏巡抚.md "wikilink")[程德全在苏州宣布江苏独立](../Page/程德全.md "wikilink")。11月17日（九月二十七日），苏军都督府颁布《暂行地方制》，“[同城州县均截并为一](../Page/治所同城.md "wikilink")”。民国元年（1912年）1月，江苏临时省议会议决，江苏都督府通令颁布《江苏暂行地方制》，各地废府、州，并县、厅\[3\]。

## 参考文献

## 参见

  - [太仓卫](../Page/太仓卫.md "wikilink")
  - [太仓县](../Page/太仓县.md "wikilink")
  - [太仓市](../Page/太仓市.md "wikilink")

{{-}}

[蘇](../Category/清朝直隶州.md "wikilink")
[Category:江苏的州](../Category/江苏的州.md "wikilink")
[Category:上海的州](../Category/上海的州.md "wikilink")
[Category:苏州行政区划史](../Category/苏州行政区划史.md "wikilink")
[Category:南通行政区划史](../Category/南通行政区划史.md "wikilink")
[Category:1724年建立的行政区划](../Category/1724年建立的行政区划.md "wikilink")
[Category:1912年废除的行政区划](../Category/1912年废除的行政区划.md "wikilink")

1.
2.  《[清史稿](../Page/清史稿.md "wikilink")·卷五十八·志三十三》◎地理五......太仓直隶州：繁，疲，难。隶苏松太道。顺治初，因明制，属苏州府，县一。雍正二年，升直隶州，析州置镇洋县，又割苏州府之嘉定属之，析其地置宝山，同隶州......领县四。北有穿山......镇洋，繁。倚。雍正二年置......

3.