《**Make
Up**》\[1\]是[香港](../Page/香港.md "wikilink")[天后](../Page/天后.md "wikilink")[楊千嬅於](../Page/楊千嬅.md "wikilink")2003年7月9日發行的唱片，是她由[新藝寶唱片發行的最後一張個人大碟](../Page/新藝寶唱片.md "wikilink")。由於缺乏宣傳的關係，本地銷量僅一萬\[2\]。

## 簡介

經過前三年《[Play It Loud, Kiss Me
Soft](../Page/Play_It_Loud,_Kiss_Me_Soft.md "wikilink")》、《[M VS M
上半場](../Page/M_VS_M_上半場.md "wikilink")》、《[M VS M 下半場及M VS M
總決賽](../Page/M_VS_M_下半場及M_VS_M_總決賽.md "wikilink")》和《[Miriam's
Music
Box](../Page/Miriam's_Music_Box.md "wikilink")》在音樂創新上的鋪墊和開拓，楊千嬅在這張專輯首次在商業和實驗性上取得平衡，推出了以電影命名的概念大碟。亦是她眾多專輯裡面實驗性音樂較多的一張。

大碟製作人依然由[人山人海負責](../Page/人山人海_\(公司\).md "wikilink")，另外也加入了新的合作。楊千嬅大膽起用[香港](../Page/香港.md "wikilink")[女子音樂組合](../Page/女子音樂組合.md "wikilink")[at17成員](../Page/at17.md "wikilink")[盧凱彤首次為他人作曲的歌曲](../Page/盧凱彤.md "wikilink")「龍爭虎鬥」作為其新碟第一主打，配以[黃偉文譜寫的詼諧歌詞](../Page/黃偉文.md "wikilink")。

[蔡德才自](../Page/蔡德才.md "wikilink")1997年與楊千嬅合作，這次的大碟則寫了兩首歌。當中，「飛女正傳」是楊千嬅的經典曲目，在ktv的點擊率非常高，之後在2010年《Ladies\&Gentlemen楊千嬅世界巡迴演唱會》與丈夫[丁子高的合唱更是掀起全場高潮](../Page/丁子高.md "wikilink")。

「稀客」則是[雷頌德走一貫的大路情歌](../Page/雷頌德.md "wikilink")。「歌舞昇平」是懷舊的Jazz曲風。[Eric
Kwok寫下了中國小調色彩的](../Page/郭偉亮.md "wikilink")「民間傳奇」，是楊千嬅的新嘗試。另一首「娃娃夫人」後面的海鷗音效為大碟抹上了一層另類的面紗。[陳綺貞繼](../Page/陳綺貞.md "wikilink")1999年「夏天的故事」後再次合作，寫下了藍調風格的「藍與黑」。假音人第一次為楊千嬅寫下了「忌廉溝鮮奶」。[香港](../Page/香港.md "wikilink")[女子音樂組合](../Page/女子音樂組合.md "wikilink")[at17另一成員](../Page/at17.md "wikilink")[林二汶創作的](../Page/林二汶.md "wikilink")「魂斷威尼斯」則以長達兩分鐘的海浪聲作為結束。

這張大碟亦是楊千嬅在[新藝寶唱片的最後一張大碟](../Page/新藝寶唱片.md "wikilink")，這張專輯的主打歌有「龍爭虎鬥」和「飛女正傳」，但由於缺乏宣傳的關係，加上大碟的實驗性并不能被媒體較為接受，因此銷量亦較差。

## 曲目列表

## 流行榜與獎項

|           歌曲           |          最高位置          |          獎項          |
| :--------------------: | :--------------------: | :------------------: |
| <small>中文歌曲龍虎榜</small> | <small>903專業推介</small> | <small>勁爆本地榜</small> |
|        **龍爭虎鬥**        |           9            |        **1**         |
|        **飛女正傳**        |           2            |        **1**         |

## 參考文獻

[Category:楊千嬅音樂專輯](../Category/楊千嬅音樂專輯.md "wikilink")
[Category:2003年音樂專輯](../Category/2003年音樂專輯.md "wikilink")
[Category:香港音樂專輯](../Category/香港音樂專輯.md "wikilink")
[Category:流行音樂專輯](../Category/流行音樂專輯.md "wikilink")

1.
2.  <http://www.mingpaoweekly.com/htm/1840/Ba01_3.htm>