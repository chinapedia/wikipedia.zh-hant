**溫哥華加人隊**（****）是位於[加拿大](../Page/加拿大.md "wikilink")[卑詩省](../Page/卑詩省.md "wikilink")[溫哥華的](../Page/溫哥華.md "wikilink")[國家冰球聯盟隊伍](../Page/國家冰球聯盟.md "wikilink")，隸屬於[西大區](../Page/西部聯盟_\(NHL\).md "wikilink")[太平洋分區](../Page/太平洋分區.md "wikilink")。他們的主場館為可容納18,860名觀眾的[羅渣士體育館](../Page/羅渣士體育館.md "wikilink")（前稱通用汽車體育館）。

溫哥華加人隊成立於1945年，但在1970年才跟[水牛城軍刀隊一起加入國家冰球聯盟](../Page/水牛城軍刀隊.md "wikilink")。他們曾經三次晉身[史丹利盃總決賽但均未能奪冠](../Page/史丹利盃.md "wikilink")，分別為：1982年負於[紐約島人隊](../Page/紐約島人隊.md "wikilink")、1994年負於[紐約遊騎兵隊以及](../Page/紐約遊騎兵隊.md "wikilink")2011年負於[波士頓棕熊隊](../Page/波士頓棕熊隊.md "wikilink")。加人隊曾三度獲得[卡雲斯·甘保杯](../Page/卡雲斯·甘保杯.md "wikilink")，並曾於2010-11年及2011-12年球季連續贏得總統獎座。

加人隊現有四個退役球衣號碼，包括：[Stan
Smyl的](../Page/:en:Stan_Smyl.md "wikilink")12號、[Trevor
Linden的](../Page/:en:Trevor_Linden.md "wikilink")16號、[Marcus
Naslund的](../Page/:en:Marcus_Naslund.md "wikilink")19號、以及[Pavel
Bure的](../Page/:en:Pavel_Bure.md "wikilink")10號。四人除Bure外均曾擔任加人隊隊長。

## 主場館

加人隊現時的主場館為[羅渣士體育館](../Page/羅渣士體育館.md "wikilink")（2010年7月前稱為通用汽車體育館），球隊自1995-96年球季起便開始使用。之前的25年則以[太平洋體育館為主場館](../Page/太平洋體育館.md "wikilink")。

## 1994年史丹利盃總決賽

加人隊自1981-82年球季後首次打入史丹利盃總決賽，對手為[紐約遊騎兵隊](../Page/紐約遊騎兵隊.md "wikilink")。加人隊先勝第1場，其後連負3場。第5、6場則取得勝利，需要以第7場決勝負，最後騎兵隊主場加時以3-2擊敗加人隊奪得史丹利盃。賽後[溫哥華市中心出現騷亂及搶劫](../Page/溫哥華市中心.md "wikilink")，多人被捕\[1\]。

## 2011年史丹利盃總決賽

加人隊自1993-94年球季後首次打入史丹利盃總決賽，對手為[波士頓棕熊隊](../Page/波士頓棕熊隊.md "wikilink")。雙方完成6場比賽後各勝3場，需要以第7場決勝負，最後棕熊隊作客以4-0擊敗加人隊奪得史丹利盃。賽後溫哥華市中心再次出現騷亂及搶劫，同樣多人被捕\[2\]。

## 現役球員名單

[Blackhawks_vs_Canucks_102010_-_Henrik_Sedin_crop.jpg](https://zh.wikipedia.org/wiki/File:Blackhawks_vs_Canucks_102010_-_Henrik_Sedin_crop.jpg "fig:Blackhawks_vs_Canucks_102010_-_Henrik_Sedin_crop.jpg")
[Henrik_Sedin_Campbell_Bowl.jpg](https://zh.wikipedia.org/wiki/File:Henrik_Sedin_Campbell_Bowl.jpg "fig:Henrik_Sedin_Campbell_Bowl.jpg")

## 参考文獻

<div class="references-small">

<references />

</div>

## 外部連結

  - [溫哥華加人官方網頁](http://canucks.nhl.com/)

[V](../Category/國家冰球聯盟球隊.md "wikilink")
[Category:溫哥華體育](../Category/溫哥華體育.md "wikilink")
[Category:1945年建立](../Category/1945年建立.md "wikilink")

1.
2.