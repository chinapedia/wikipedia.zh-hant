《**CSI犯罪現場：紐約**》（****，簡稱****）是一部[美國](../Page/美國.md "wikilink")[刑事](../Page/刑事.md "wikilink")[電視影集](../Page/電視影集.md "wikilink")，首播於2004年9月22日。它是《[CSI犯罪現場](../Page/CSI犯罪現場.md "wikilink")》的第二個[衍生影集](../Page/衍生影集.md "wikilink")（）。首次出現是在《[CSI犯罪現場:
邁阿密](../Page/CSI犯罪現場:_邁阿密.md "wikilink")－》第二季的23集（），劇中主角何瑞修（）前往[紐約市追捕一名從邁阿密逃逸的謀殺嫌犯](../Page/紐約市.md "wikilink")。有趣的是，影集在美國播映的時段與[國家廣播公司的](../Page/國家廣播公司.md "wikilink")《[法網遊龍](../Page/法律與秩序.md "wikilink")》（**）一樣，而後者是以[紐約市為舞臺的老牌犯罪影集](../Page/紐約市.md "wikilink")。

除了[蓋瑞·辛尼茲](../Page/蓋瑞·辛尼茲.md "wikilink")、[美琳娜·卡娜卡瑞迪斯](../Page/美琳娜·卡娜卡瑞迪斯.md "wikilink")、以及[安娜·貝爾納普以外](../Page/安娜·貝爾納普.md "wikilink")，所有的演員都是來自[紐約](../Page/紐約.md "wikilink")。然而，[蓋瑞·辛尼茲和](../Page/蓋瑞·辛尼茲.md "wikilink")[安娜·貝爾納普都曾在紐約居住過一段時間](../Page/安娜·貝爾納普.md "wikilink")。

2012年5月13日，CBS续订本片第九季\[1\]。

全劇於2013年2月22日結束\[2\]。CBS於2013年5月10日宣布不會播映第十季\[3\]。

## 劇集介紹

  - 主題曲跟其他兩個劇集一樣，選用[何許人合唱團](../Page/何許人合唱團.md "wikilink")（）的曲子，這次選的是《巴巴·歐萊禮》（**）這首歌；它有時候也被誤稱為《青少年荒地》（**），給人速度的快感。

<!-- end list -->

  - 在[犯罪现场调查：邁阿密第](../Page/犯罪现场调查：邁阿密.md "wikilink")2季23集引出紐約版，及後犯罪现场调查：紐約正式播出。紐約版與邁阿密版再次出現交錯的劇情（）：邁阿密11月7日播出的第4季第7集「**」為兩區交錯案件的上集；紐約版11月9日播出的第2季第7集「**」，則是交錯案件的下集。故事由一段墜機的事故展開，描寫一個連續殺人犯。兩組人馬都有參與演出。

<!-- end list -->

  - 在[2004年11月](../Page/2004年11月.md "wikilink")，[Spike
    TV電視網以破紀錄的價格](../Page/Spike_TV.md "wikilink")，一集190萬美金，買下了這個系列的重播權。這家電視網先前已經以一集160萬美金的價格買下《CSI犯罪現場》的重播權。《CSI犯罪現場：紐約》預計於2005年末加入的影集重播行列。

<!-- end list -->

  - 紐約篇和CSI犯罪現場的職業定位有所區別，CSI犯罪現場的角色是屬於刑事鑑證的科学家，而紐約篇和邁阿密篇的角色均隸屬於警察部門，而紐約篇的角色職業是屬於城市刑警與科學家混搭的職業設定，所以與邁阿密篇的角色一樣能夠持有槍械。本作有明確描寫到紐約鑑證科警察的專用車是**雪佛蘭Chevrolet
    Avalanche**，拍攝手法比另外兩個系列較為血腥，畫面風格呈現冷藍，還有陰天的環境和二氧化碳的排放畫面比較多。

<!-- end list -->

  - 在第三季的「Cold Reveal」，出現同為CBS製播的[鐵證懸案](../Page/鐵證懸案.md "wikilink")（Cold
    Case）中的Scotty Valens，這是CSI系列首次與非CSI系列影集合作。

<!-- end list -->

  - 第8季已於2011年9月中旬播出，由於本劇主要內容地點位於紐約，第8季首集以911事件10週年為主，已播映完毕。第9季是最後一季在美國已播映完畢。

## 演員陣容

### 主角

  - **[麥克·泰勒](../Page/麥克·泰勒.md "wikilink")（）：[蓋瑞·辛尼茲飾演](../Page/蓋瑞·辛尼茲.md "wikilink")**，是紐約CSI團隊的領導人，獨身（劇中設定他的妻子死於[911事件](../Page/911事件.md "wikilink")）。曾經在美軍[海軍陸戰隊服役](../Page/海軍陸戰隊.md "wikilink")；他也曾經說過在這世界上最想做的事就是為國家服務。他與波納席拉探員之間有著極其信賴的友誼關係。他是一名[貝斯樂手](../Page/貝斯.md "wikilink")，在一間[爵士樂俱樂部中定期表演](../Page/爵士樂.md "wikilink")。他在第五季二十三集中欣然接受部下丹尼及琳賽請託，擔任他們女兒的[教父](../Page/教父母.md "wikilink")。

<!-- end list -->

  -
    在第三季中，他與新加入的法醫佩登有著感情的發展。但是第四季開始不久，佩登因思念家人辭職回到英國，麥克雖然曾陪同她在英國住了一段時間，但仍因無法適應而回到紐約。最後兩人因為遠距離而分手。

<!-- end list -->

  - **[喬·丹佛](../Page/喬·丹佛.md "wikilink")（）：莎拉·華德（）飾演**，第七季起自華盛頓特區聯邦調查局轉來的新任三級調查員副主管。個性幽默，在第七季第一集中剛到實驗室時，便發現一具屍體，調查時發揮了她驚人的觀察力。

<!-- end list -->

  -
    因揭發和她共事12年的FBI探員法蘭克·華特（Frank
    Waters）在偵辦馬修斯議員女兒被強姦案時銷毀一份由他執行的鑑定結果失誤的DNA文件，使該名強姦犯無罪釋放，她也由華盛頓特區聯邦調查局轉調紐約市犯罪調查實驗室為三級調查員副主管（第七季第三集）。

<!-- end list -->

  - **[丹尼·麥瑟](../Page/丹尼·麥瑟.md "wikilink")（）探員：[卡明·喬文納佐](../Page/卡明·喬文納佐.md "wikilink")（）飾演**，這個角色被認為是影集裡最複雜的一個。他在[史坦頓島的一個](../Page/史坦頓島.md "wikilink")「義大利[黑手黨](../Page/黑手黨.md "wikilink")」家庭中被監視著長大（參見「**」這一集），有著一套自己的混合道德觀（介於違法者與執法者之間）。

<!-- end list -->

  -
    麥克親自挑選丹尼加入這個團隊，這對後者來說是項榮譽與責任，每天都為他帶來不少壓力。他生性多疑，不容易相信他人。唐·弗拉克探員是他的好友，可以說是他唯一願意交付信任的人。丹尼對後加入的組員琳賽有好感，常戲謔地稱她「蒙大拿（Montana）」來取笑她的鄉村背景，隨著劇情發展也衍生出兩人的曖昧關係，其感情支線意外變成影迷們另一個注意的焦點。

<!-- end list -->

  -
    第二季「法網恢恢」中，丹尼被捲入一宗十五年前的兇殺案。他的哥哥路易為得到真正兇手的證言，被痛打癱瘓成植物人，但成功地證明了丹尼的清白。在第五季第九集，丹尼以旁白的方式發展和琳賽的感情。兩人在第五季第十七集公證結婚，由麥克跟史黛拉擔任[證婚人](../Page/證婚人.md "wikilink")。
    於第五季「Pay up」的最後一幕時，和CSI們在小酒吧裡紀念因公殉職的Jessica
    Angell時遭受攻擊不慎被子彈擊中，於第六季起開始漫長的復健之路，但復健期間仍參與CSI辦案。

<!-- end list -->

  - **[薛爾登·霍克](../Page/薛爾登·霍克.md "wikilink")（）法醫與鑑識員：[希爾·哈潑](../Page/希爾·哈潑.md "wikilink")（）飾演**，天才兒童，才18歲就從大學畢業；24歲獲得[外科醫師執業執照](../Page/外科醫師.md "wikilink")。

<!-- end list -->

  -
    然而，因無法救回兩個病人，從此放棄成為一個外科醫生，成為一個法醫。在第2季裡，霍克離開[解剖室](../Page/解剖.md "wikilink")，投入現場調查工作。

<!-- end list -->

  -
    在「Raising
    Shane」，他在一兇殺案中成為主要嫌疑犯，而整個團隊也被要求移交此案件。但最後還是其他團員發現線索，證明了霍克的清白。

<!-- end list -->

  -
    曾有一位女友，已分手。在第七季裡與大學同學Camille交往。

<!-- end list -->

  - **[唐·弗拉克](../Page/唐·弗拉克.md "wikilink")（）探員：[艾迪·卡希爾](../Page/艾迪·卡希爾.md "wikilink")（[Eddie
    Cahill](../Page/埃迪·卡希爾.md "wikilink")）飾演**，來自一個世代從事執法工作的家族。他是紐約警局舊式辦案風格與新時代的之間的橋樑。

<!-- end list -->

  -
    是個風趣的[重案組探員](../Page/重案組.md "wikilink")，但是對嫌犯缺乏耐心。他的辦案技巧經常遊走於法律容許的邊界，也因此造成不少爭議。他與丹尼·麥瑟是好友，總是願意傾聽後者的問題。

<!-- end list -->

  -
    在第二季的最後一集「炸彈客」中，他被兇殺案現場的[炸彈炸成重傷](../Page/炸彈.md "wikilink")，到片終時仍然昏迷不醒。在第三季開始時他完全康復並回到工作崗位。在第五季出現他的妹妹。

<!-- end list -->

  - **[琳賽·夢露](../Page/琳賽·夢露.md "wikilink")：[安娜·貝爾納普飾演](../Page/安娜·貝爾納普.md "wikilink")**，鑑識科學家，在艾婷·波恩被解僱之後從[蒙大拿州調過來](../Page/蒙大拿州.md "wikilink")。

<!-- end list -->

  -
    來自中西部的琳賽是一名性格爽朗的女孩子，身手矯健，加入不久就完全融入了原來的團隊。在第三季第十二集「Silent
    Night」中，她在剛到達兇殺現場後就立即離開，神情激動。及後她向史黛拉說出自己曾經是一件兇殺案中唯一的悻存者，而被害者當中有她的朋友。在第三季第十四集「Lying
    Game」中，她被要求回到蒙大拿為這件兇殺出庭作證，因而短暫地離開了團隊。

<!-- end list -->

  -
    丹尼不時表現出對琳賽的興趣；第三季第三集「Love Run
    Cold」中，當丹尼向琳賽表示他對她有好感時，琳賽承認自己對丹尼也有好感，但因為個人的問題而不希望有進一步的發展。第五季的第九集琳賽懷孕，第十集丹尼求婚，但琳賽拒絕了，因為她認為丹尼還沒準備好要結婚。但丹尼還是在第五季第十七集求婚成功，由麥克跟史黛拉擔任[證婚人](../Page/證婚人.md "wikilink")，並於第二十三集誕下一女。

<!-- end list -->

  - **[席德·漢默貝克](../Page/席德·漢默貝克.md "wikilink")（）法醫：羅伯特·喬依（）飾演**自第2季起加入的新法醫，取代改投入現場鑑識工作的薛爾登成為紐約警局解剖室的主要法醫。

<!-- end list -->

  -
    被形容為一個「半路出家的天才」，最近才從廚師轉職成法醫。為人風趣幽默，曾經因為在驗屍時感染到核輻射而差點喪命。第八季發明了一個新式睡枕，經由麥克測試之後效果良好，被財團買下睡枕的專利權，成為了億萬富翁。但卻患上淋巴癌。

<!-- end list -->

  - **[亞當](../Page/亞當_\(CSI犯罪現場角色\).md "wikilink")（）：[A.J.
    Buckley飾演](../Page/A·J·巴克利.md "wikilink")**，實驗室技師。在第4季虛擬世界殺人案中，他發揮電腦專長。第5季開始到現場勘驗。

### 配角

  - 李納德·吉爾斯（）：[傑·葛蘭特·艾爾布萊區](../Page/傑·葛蘭特·艾爾布萊區.md "wikilink")（）飾演，實驗室主管，以前是個跑者，後因意外造成腰部以下癱瘓。
  - 珍·帕森（）：[桑雅·瓦傑](../Page/桑雅·瓦傑.md "wikilink")（）飾演，實驗室技師。
  - 查德·溫寧漢（）：[查德·林德勃](../Page/查德·林德勃.md "wikilink")（）飾演，實驗室技師。
  - 伊凡·趙（）醫生：[隆·袁](../Page/隆·袁.md "wikilink")（）飾演（第2季起），在霍克醫生改從事現場調查工作之後取代了他的工作。他被形容為一個「英俊、年輕、平易近人的醫生」。
  - 查克·香儂（）：[大衛·朱利安·賀須](../Page/大衛·朱利安·賀須.md "wikilink")（）飾演（第2季起），新來的實驗室技師。
  - 無名制服警察：[葛瑞格·德哥斯丁諾](../Page/葛瑞格·德哥斯丁諾.md "wikilink")（）飾演（第2季起）。

### 離開的角色

  - 艾婷·波恩（）探員：
    飾演，她土生土長於[布魯克林區](../Page/布魯克林區.md "wikilink")，具有可以快速適應新情境的能力。

<!-- end list -->

  -
    在第2季第2集「**」裡被麥克解僱，理由是她的好友被一個兇犯姦殺，但是證據不足以將其繩之以法，艾婷意圖變造證據。雖然她並未真的這麼做，但是她有打開封條，這讓她的誠信度遭到懷疑。麥克決定解僱她，因為她有危害整個實驗室誠信的風險。她同意了這個決定，因為她不知道如果還有下次，她是否能抵抗誘惑。在被解僱後，她獨自繼續搜索強姦犯的證據，結果被兇犯發現，並被殺害，CSI實驗室的所有同僚萬分悲痛，他們知道誰是凶手，但是卻缺少關鍵證據。最後依靠艾婷被害時留下的重要證據，終於將其繩之以法。結案以後，CSI實驗室的所有同僚聚到一個酒吧紀念她。

<!-- end list -->

  - 史黛拉·波納席拉（）探員：
    飾演，把自己完全奉獻給工作。她常常被叫做「[自由女神](../Page/自由女神.md "wikilink")」，因為她強硬的個性、強烈的決心、以及高度的智慧使然。她是一半[希臘](../Page/希臘.md "wikilink")、一半[義大利血統的孤兒](../Page/義大利.md "wikilink")，從小在[寄養家庭中長大](../Page/寄養家庭.md "wikilink")。她跟麥克是非常好的朋友，總是擔心他的生理與心理的狀態。

<!-- end list -->

  -
    第二季「全面通行」中，史黛拉被精神狀態不穩的男友禁錮於她的家中，並且嘗試殺害她。最後史黛拉被迫因自衛而開槍殺死男友，但她卻受了很大的精神傷害。在第三季的「Heart
    of Glass」中，她被兇案現場的染血玻璃割傷，及後發現死者是愛滋病患者，她也有可能會被感染；在第20集「What Schemes
    May Come」中，檢測由Adam Ross進行；在第21集「Past
    Imperfect」，得知檢測結果為HIV陰性。在第四季第十六集中，史黛拉所住的公寓失火，牽扯出失蹤兒童案。在第七季起離開劇組。The
    34th floor中交代了她到紐爾良就任CSI隊長。

<!-- end list -->

  -
    CBS對外表示「我們希望她續約，但是也尊重她選擇離開」，並且肯定她六年來精采的演出；Melina
    Kanakaredes也表示很珍惜六年在CSI:NY的演出與製作單位同仁的友誼。

<!-- end list -->

  - 潔西卡·安琪（）探員： 飾演，她並不是CSI的成員，但與CSI的成員私交甚好。

<!-- end list -->

  -
    安琪與第三季起即有登場，但一開始名字為Jennifer Angell，第四季起改名為Jessica
    Angell。在第五季帶著證人Conner
    Dunbrook吃早餐時遭受攻擊，受到嚴重的槍傷後送醫不治。

## 集名列表

每集的故事大綱請見[CSI犯罪現場：紐約章節列表](../Page/CSI犯罪現場：紐約章節列表.md "wikilink")。

## 參見

  - [CSI犯罪現場](../Page/CSI犯罪現場.md "wikilink")
  - [CSI犯罪現場: 邁阿密](../Page/CSI犯罪現場:_邁阿密.md "wikilink")

## 参考文献

## 外部連結

  -
[CSI犯罪現場](../Category/CSI犯罪現場.md "wikilink")
[Category:2000年代美國電視劇](../Category/2000年代美國電視劇.md "wikilink")
[Category:2004年開播的美國電視影集](../Category/2004年開播的美國電視影集.md "wikilink")
[Category:2010年代美國電視劇](../Category/2010年代美國電視劇.md "wikilink")
[Category:2013年停播的美國電視影集](../Category/2013年停播的美國電視影集.md "wikilink")
[Category:英語電視劇](../Category/英語電視劇.md "wikilink") [CSI:
NY](../Category/美國劇情電視劇.md "wikilink")
[Category:美国犯罪电视剧](../Category/美国犯罪电视剧.md "wikilink")
[Category:警匪電視劇](../Category/警匪電視劇.md "wikilink")
[Category:電視影集衍生作品](../Category/電視影集衍生作品.md "wikilink")
[Category:紐約市背景電視節目](../Category/紐約市背景電視節目.md "wikilink")
[Category:CBS電視節目](../Category/CBS電視節目.md "wikilink")
[Category:CBS電視工作室製作的電視節目](../Category/CBS電視工作室製作的電視節目.md "wikilink")
[Category:亞洲電視外購劇集](../Category/亞洲電視外購劇集.md "wikilink")
[Category:福斯傳媒集團外購電視劇](../Category/福斯傳媒集團外購電視劇.md "wikilink")
[Category:索尼影業電視外購電視劇](../Category/索尼影業電視外購電視劇.md "wikilink")

1.  <http://tvbythenumbers.zap2it.com/2012/05/13/report-csi-miami-canceled-csi-ny-renewed/133850/>
2.  <http://tvbythenumbers.zap2it.com/2012/12/06/cbs-announces-mid-season-lineup-including-premiere-of-golden-boy-and-return-of-rules-of-engagement-survivor-the-amazing-race/160626/>
3.  <http://www.hollywoodreporter.com/live-feed/cbs-cancels-csi-ny-vegas-520885>