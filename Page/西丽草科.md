**西丽草科**只有1[属](../Page/属.md "wikilink")—[西丽草属](../Page/西丽草属.md "wikilink")（*Hesperocallis*）1[种](../Page/种.md "wikilink")—[西丽草](../Page/西丽草.md "wikilink")（*Hesperocallis
undulata*），分布在[北美洲西南部的](../Page/北美洲.md "wikilink")[沙漠地带](../Page/沙漠.md "wikilink")。

以前的分类法一直将这个[植物分在](../Page/植物.md "wikilink")[百合科中](../Page/百合科.md "wikilink")，2003年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
II
分类法将其单独列为一个](../Page/APG_II_分类法.md "wikilink")[科](../Page/科.md "wikilink")，但仍然认为可以选择性地和[天门冬科或](../Page/天门冬科.md "wikilink")[龍舌蘭科合并](../Page/龍舌蘭科.md "wikilink")。

## 外部链接

  - [*Hesperocallis*
    e-flora中的西丽草属](http://www.efloras.org/florataxon.aspx?flora_id=1&taxon_id=115230)
  - [图片和介绍](http://www.calflora.net/bloomingplants/desertlily.html)
  - [7幅图片](http://www.oceanlight.com/lightbox.php?sp=hesperocallis_undulata)
  - [NCBI中的西丽草科](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Tree&id=232742&lvl=3&lin=f&keep=1&srchmode=1&unlock)
  - [CSDL中的西丽草科](http://www.csdl.tamu.edu/FLORA/cgi/gateway_family?fam=Hesperocallidaceae)

[\*](../Category/西丽草科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")