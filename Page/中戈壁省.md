[Map_mn_dundgobi_aimag.png](https://zh.wikipedia.org/wiki/File:Map_mn_dundgobi_aimag.png "fig:Map_mn_dundgobi_aimag.png")
**中戈壁省**（）位於[蒙古國中南部](../Page/蒙古國.md "wikilink")，面積74,690平方公里，人口38,821（2011年）。首府[曼達勒戈壁](../Page/曼達勒戈壁.md "wikilink")。

[中戈壁省](../Category/中戈壁省.md "wikilink")
[Dundgovi](../Category/蒙古国省份.md "wikilink")