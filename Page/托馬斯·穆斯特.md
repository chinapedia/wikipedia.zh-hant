**托馬斯·穆斯特**（，）是前[奧地利職業](../Page/奧地利.md "wikilink")[網球運動員](../Page/網球.md "wikilink")（1985年），前世界排名第一，[法網](../Page/法網.md "wikilink")（1995年）單打冠軍得主，已退役。

## 大滿貫

### 男單冠軍（1）

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>比賽</p></th>
<th><p>場地</p></th>
<th><p>決賽對手</p></th>
<th><p>對手國籍</p></th>
<th><p>勝方比分</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1995</p></td>
<td><p><strong>法网</strong></p></td>
<td><p>-{zh-cn:红土;zh-hk:泥地;zh-tw:紅土;}-</p></td>
<td><p><a href="../Page/張德培.md" title="wikilink">張德培</a></p></td>
<td><p><a href="../Page/美國.md" title="wikilink">美國</a></p></td>
<td><p>7-5, 6-2, 6-4</p></td>
</tr>
</tbody>
</table>

## ATP巡迴賽單打冠軍（44）

  - 1986年 (1) - [荷蘭希爾弗瑟姆](../Page/荷蘭.md "wikilink")
  - 1988年 (4) - 波士頓、波爾多、布拉格、-{巴里}-
  - 1990年 (3) -
    [羅馬](../Page/羅馬大師賽.md "wikilink")、阿德雷德、[摩洛哥卡薩布蘭卡](../Page/摩洛哥.md "wikilink")
  - 1991年 (2) - 佛羅倫斯、日內瓦
  - 1992年 (3) -
    [蒙地卡羅](../Page/蒙地卡羅大師賽.md "wikilink")、佛羅倫斯、[克羅地亞烏馬格](../Page/克羅地亞.md "wikilink")
  - 1993年 (7) -
    墨西哥城、佛羅倫斯、熱那亞、[奧地利基茨比厄爾](../Page/奧地利.md "wikilink")、聖馬利諾、烏馬格、巴勒莫
  - 1994年 (3) - 墨西哥城、馬德里、[奧地利聖皮爾藤](../Page/奧地利.md "wikilink")
  - [1995年](http://www.atpworldtour.com/Tennis/Players/Mu/T/Thomas-Muster.aspx?t=pa&y=1995&m=s&e=0)
    (12) -
    墨西哥城、[葡萄牙埃斯托利爾](../Page/葡萄牙.md "wikilink")、巴塞羅拿、[蒙地卡羅](../Page/蒙地卡羅大師賽.md "wikilink")、[羅馬](../Page/羅馬大師賽.md "wikilink")、**法網**、聖皮爾滕、史圖加特室外賽、聖馬利諾、烏馬格、布加勒斯特、德國[埃森](../Page/馬德里大師賽.md "wikilink")
  - 1996年 (7) -
    墨西哥城、埃斯托利爾、巴塞隆納、[蒙地卡羅](../Page/蒙地卡羅大師賽.md "wikilink")、[羅馬](../Page/羅馬大師賽.md "wikilink")、史圖加特室外賽、波哥大
  - 1997年 (2) - 杜拜、[邁阿密](../Page/邁阿密大師賽.md "wikilink")

為ATP大師賽 （共8項冠軍）

## 外部連結

  -
  -
  -
[M](../Category/奥地利网球运动员.md "wikilink")
[M](../Category/左撇子.md "wikilink")