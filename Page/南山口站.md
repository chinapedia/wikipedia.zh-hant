**南山口站**位于[中国](../Page/中国.md "wikilink")[青海省](../Page/青海省.md "wikilink")[海西州](../Page/海西蒙古族藏族自治州.md "wikilink")[格尔木市](../Page/格尔木市.md "wikilink")，[海拔](../Page/海拔.md "wikilink")3080米，于1984年启用\[1\]。该站为[青藏铁路集团公司直属的车站](../Page/青藏铁路集团公司.md "wikilink")，也是[青藏铁路一期](../Page/青藏铁路.md "wikilink")[西宁至](../Page/西宁站.md "wikilink")[格尔木段和二期格尔木至](../Page/格尔木站.md "wikilink")[拉萨段的连接点](../Page/拉萨站.md "wikilink")\[2\]。

## 车站设施

## 使用情况

南山口站是[青藏铁路的](../Page/青藏铁路.md "wikilink")[有人驻守车站](../Page/有人驻守车站.md "wikilink")，有14列旅客列车经过此站，主要为[拉萨到发的各类旅客列车](../Page/拉萨站.md "wikilink")。

## 始发车次

目前南山口站未有任何始发车次。

## 历史

  - 1984年：南山口站建成启用。

## 相关条目

  - [青藏铁路](../Page/青藏铁路.md "wikilink")

## 参考资料

<div class="references-2column">

<references/>

</div>

[Category:青藏铁路车站](../Category/青藏铁路车站.md "wikilink")
[Category:海西州铁路车站](../Category/海西州铁路车站.md "wikilink")
[Category:格尔木市](../Category/格尔木市.md "wikilink")
[Category:1984年启用的铁路车站](../Category/1984年启用的铁路车站.md "wikilink")

1.  中华建筑资讯网：〈[藏域风格建筑群
    璀璨“新天路”](http://www.rpccn.com/news/33/200673162447.htm)
    〉，于2007年6月25日存取
2.  筑龙网：〈[探访青藏铁路系列一
    铁路建设史上最伟大的穿越](http://news.sinoaec.com/dongtai/read.asp?id=43155)
    〉，于2007年6月25日存取