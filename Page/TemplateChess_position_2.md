<noinclude></noinclude> {| cellspacing="0" cellpadding="0"
style="border-collapse: collapse;" |- | style="text-align: center;
font-family: monospace; border-top: none; padding-right: 2px;" | 8 |
style="border-top: solid thin; border-left: solid thin;"
|![px](chess_{{{1}}}d40.png "px") | style="border-top: solid thin;"
|![px](chess_{{{2}}}l40.png "px") | style="border-top: solid thin;"
|![px](chess_{{{3}}}d40.png "px") | style="border-top: solid thin;"
|![px](chess_{{{4}}}l40.png "px") | style="border-top: solid thin;"
|![px](chess_{{{5}}}d40.png "px") | style="border-top: solid thin;"
|![px](chess_{{{6}}}l40.png "px") | style="border-top: solid thin;"
|![px](chess_{{{7}}}d40.png "px") | style="border-top: solid thin;
border-right: solid thin;" |![px](chess_{{{8}}}l40.png "px") |- |
style="text-align: center; font-family: monospace; padding-right: 2px;"
| 7 | style="border-left: solid thin;" |![px](chess_{{{9}}}l40.png "px")
| ![px](chess_{{{10}}}d40.png "px") | ![px](chess_{{{11}}}l40.png "px")
| ![px](chess_{{{12}}}d40.png "px") | ![px](chess_{{{13}}}l40.png "px")
| ![px](chess_{{{14}}}d40.png "px") | ![px](chess_{{{15}}}l40.png "px")
| style="border-right: solid thin;" |![px](chess_{{{16}}}d40.png "px")
|- | style="text-align: center; font-family: monospace; padding-right:
2px;" | 6 | style="border-left: solid thin;"
|![px](chess_{{{17}}}d40.png "px") | ![px](chess_{{{18}}}l40.png "px") |
![px](chess_{{{19}}}d40.png "px") | ![px](chess_{{{20}}}l40.png "px") |
![px](chess_{{{21}}}d40.png "px") | ![px](chess_{{{22}}}l40.png "px") |
![px](chess_{{{23}}}d40.png "px") | style="border-right: solid thin;"
|![px](chess_{{{24}}}l40.png "px") |- | style="text-align: center;
font-family: monospace; padding-right: 2px;" | 5 | style="border-left:
solid thin;" |![px](chess_{{{25}}}l40.png "px") |
![px](chess_{{{26}}}d40.png "px") | ![px](chess_{{{27}}}l40.png "px") |
![px](chess_{{{28}}}d40.png "px") | ![px](chess_{{{29}}}l40.png "px") |
![px](chess_{{{30}}}d40.png "px") | ![px](chess_{{{31}}}l40.png "px") |
style="border-right: solid thin;" |![px](chess_{{{32}}}d40.png "px") |-
| style="text-align: center; font-family: monospace; padding-right:
2px;" | 4 | style="border-left: solid thin;"
|![px](chess_{{{33}}}d40.png "px") | ![px](chess_{{{34}}}l40.png "px") |
![px](chess_{{{35}}}d40.png "px") | ![px](chess_{{{36}}}l40.png "px") |
![px](chess_{{{37}}}d40.png "px") | ![px](chess_{{{38}}}l40.png "px") |
![px](chess_{{{39}}}d40.png "px") | style="border-right: solid thin;"
|![px](chess_{{{40}}}l40.png "px") |- | style="text-align: center;
font-family: monospace; padding-right: 2px;" | 3 | style="border-left:
solid thin;" |![px](chess_{{{41}}}l40.png "px") |
![px](chess_{{{42}}}d40.png "px") | ![px](chess_{{{43}}}l40.png "px") |
![px](chess_{{{44}}}d40.png "px") | ![px](chess_{{{45}}}l40.png "px") |
![px](chess_{{{46}}}d40.png "px") | ![px](chess_{{{47}}}l40.png "px") |
style="border-right: solid thin;" |![px](chess_{{{48}}}d40.png "px") |-
| style="text-align: center; font-family: monospace; padding-right:
2px;" | 2 | style="border-left: solid thin;"
|![px](chess_{{{49}}}d40.png "px") | ![px](chess_{{{50}}}l40.png "px") |
![px](chess_{{{51}}}d40.png "px") | ![px](chess_{{{52}}}l40.png "px") |
![px](chess_{{{53}}}d40.png "px") | ![px](chess_{{{54}}}l40.png "px") |
![px](chess_{{{55}}}d40.png "px") | style="border-right: solid thin;"
|![px](chess_{{{56}}}l40.png "px") |- | style="text-align: center;
font-family: monospace; padding-right: 2px;" | 1 | style="border-bottom:
solid thin; border-left: solid thin;" |![px](chess_{{{57}}}l40.png "px")
| style="border-bottom: solid thin;" |![px](chess_{{{58}}}d40.png "px")
| style="border-bottom: solid thin;" |![px](chess_{{{59}}}l40.png "px")
| style="border-bottom: solid thin;" |![px](chess_{{{60}}}d40.png "px")
| style="border-bottom: solid thin;" |![px](chess_{{{61}}}l40.png "px")
| style="border-bottom: solid thin;" |![px](chess_{{{62}}}d40.png "px")
| style="border-bottom: solid thin;" |![px](chess_{{{63}}}l40.png "px")
| style="border-right: solid thin; border-bottom: solid thin;"
|![px](chess_{{{64}}}d40.png "px") |- style="text-align: center;
font-family: monospace; font-size: 100%;" | |a |b |c |d |e |f |g |h
|}<noinclude> </noinclude>

[](../Category/体育模板.md "wikilink")