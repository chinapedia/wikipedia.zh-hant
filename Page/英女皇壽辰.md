  -
    *本文「女王」一詞均指[英聯邦王國之君主](../Page/英聯邦王國.md "wikilink")。*

[Government_House,_Jersey,_Queen's_Birthday_reception_2005.jpg](https://zh.wikipedia.org/wiki/File:Government_House,_Jersey,_Queen's_Birthday_reception_2005.jpg "fig:Government_House,_Jersey,_Queen's_Birthday_reception_2005.jpg")的[副總督在英女皇壽辰當日開放](../Page/副總督.md "wikilink")[總督府招待公眾](../Page/總督府.md "wikilink")，並會在當天宣讀壽辰榮譽人士的姓名。\]\]

**英女皇官方壽辰**（[英語](../Page/英語.md "wikilink")：**Queen's Official
Birthday**），又稱**英女皇壽辰**（英語：**Queen's
Birthday**），英國以外的[英聯邦王國也稱](../Page/英聯邦王國.md "wikilink")**女皇壽辰**，是不少[英聯邦國家的](../Page/英聯邦.md "wikilink")[公眾假期](../Page/公眾假期.md "wikilink")，但以英聯邦王國為主。作為[共和國的](../Page/共和國.md "wikilink")[斐濟在](../Page/斐濟.md "wikilink")2013年及以前，仍然有慶祝英女皇壽辰。如果[國君是男性](../Page/國君.md "wikilink")，則節日名稱由「英女皇」改為「英皇」。需要注意的是，[官方壽辰在不同國家訂在不同日子](../Page/官方壽辰.md "wikilink")，但一般而言，所訂的日子都不是女皇的真正[生日](../Page/生日.md "wikilink")。當今在位君主是[伊利沙伯二世](../Page/伊利沙伯二世.md "wikilink")，她的真正生日是4月21日。另外，在官方壽辰當天，各王国政府會公佈授勳名單。

## 英國

在[英國](../Page/英國.md "wikilink")，現今的女皇壽辰一般訂於6月的第一、第二或第三個星期六（以2009年為例，則訂於6月13日），而在壽辰當天，[倫敦市的](../Page/倫敦.md "wikilink")[騎兵衛隊閱兵場會舉行](../Page/騎兵衛隊閱兵場.md "wikilink")[旌旗操演](../Page/旌旗操演.md "wikilink")（又名英女皇壽辰閱兵）。把壽辰訂在6月是[愛德華七世](../Page/愛德華七世.md "wikilink")（1901年—1910年在位）的主意，這是因為那時候英國的天氣較好。

在英國本土的女皇壽辰當日，政府會公佈[壽辰授勳名單](../Page/英國榮譽制度.md "wikilink")（Birthday
Honours
List），宣告新獲勳人士的姓名及所得勳銜。在英國的駐外外交機構，官方壽辰當日會被視作英國的[國慶日](../Page/國慶日.md "wikilink")。由於女皇壽辰訂於星期六，並非[工作日](../Page/工作日.md "wikilink")，因此不屬於公眾假期；出於這個原故，[公務員在每年](../Page/公務員.md "wikilink")5月最後一個星期一可獲得一日「優待假期」作補償（這一日通常也是每年的春天[銀行假期](../Page/銀行假期.md "wikilink")），讓他們可享受一連三天的周末假期。

## 澳洲和紐西蘭

[澳洲的官方壽辰訂在每年](../Page/澳洲.md "wikilink")6月的第二個星期一，是一個法定[公眾假期](../Page/公眾假期.md "wikilink")，但是[西澳大利亚和](../Page/西澳大利亚.md "wikilink")[昆士兰州卻是例外](../Page/昆士兰州.md "wikilink")，[西澳大利亚當地在女官方壽辰的前一個星期另立了](../Page/西澳大利亚.md "wikilink")[建基日](../Page/建基日.md "wikilink")，卻又同時把壽辰定在[珀斯皇家展覽會的開幕日](../Page/珀斯皇家展覽會.md "wikilink")（一般在9月最後一個星期一或者是10月的第一個星期一），並且是屬於學校假期；以2016年为例，昆士兰州将10月的第一个星期一定为女王壽辰。\[1\]。

在澳洲官方壽辰當日，會公佈「女皇壽辰授勳名單」，以公佈新一批[澳洲勳章的獲勳人士](../Page/澳洲勳章.md "wikilink")。此外，當天接續下去的一個星期也同時定作澳洲雪季的開始，但是事實上，通常要待女皇壽辰以後的一段日子，才會真正有足夠的[雪讓遊人](../Page/雪.md "wikilink")[滑雪](../Page/滑雪.md "wikilink")。

一直以來，在澳洲女皇官方壽辰，澳洲各處都有公開的[煙花表演](../Page/煙花.md "wikilink")，但其盛大程度在近年已漸漸被規模更大的[除夕煙花表演比下去](../Page/除夕.md "wikilink")。另外，[澳洲首都特區當局只會在緊接著女皇官方壽辰之前的那個周末](../Page/澳洲首都特區.md "wikilink")，准許民眾販賣煙花\[2\]。

[紐西蘭方面](../Page/紐西蘭.md "wikilink")，女王的官方壽辰則定在每年6月的第一個星期一，由於氣候比澳洲寒冷的緣故，所以當天接續下去的一個星期也同樣定作紐西蘭雪季的開始。

以[紐澳兩地的女皇官方壽辰](../Page/紐澳.md "wikilink")，不能得知英女皇真正的生日。相反，定在4月25日的[-{zh-hans:澳新军团日;
zh-hant:澳紐軍團日;}-和](../Page/澳新軍團日.md "wikilink")[復活節卻更接近](../Page/復活節.md "wikilink")[英女皇的真正生日](../Page/英女皇.md "wikilink")。

## 加拿大

[加拿大以](../Page/加拿大.md "wikilink")[維多利亞日來慶祝女皇壽辰](../Page/維多利亞日.md "wikilink")，並定在每年5月24日之前最近的星期一，但若果5月24日本身是星期一的話，則當天會定為女皇壽辰。不過，普遍的民眾並不清楚維多利亞日是慶祝女皇壽辰，更鮮有人知道，維多利亞日最初是慶祝[維多利亞女皇在](../Page/維多利亞女皇.md "wikilink")1819年5月24日的生日。對加拿大人來說，維多利亞日標誌著[夏季的開始](../Page/夏季.md "wikilink")，因為自當日起，大型[遊樂場和戶外](../Page/遊樂場.md "wikilink")[泳池都會陸續開放](../Page/泳池.md "wikilink")，夏天活動也會相繼展開。

## 其他國家和地區

在[直布羅陀和絕大部份](../Page/直布羅陀.md "wikilink")[英國海外領地都以女皇壽辰為公眾假期](../Page/英國海外領地.md "wikilink")。而當中[福克蘭群島更特別地以](../Page/福克蘭群島.md "wikilink")[伊利沙伯二世的真正生日](../Page/伊利沙伯二世.md "wikilink")——4月21日，作為英女皇壽辰，這是因為福克蘭群島在6月正值寒冷的[冬季](../Page/冬季.md "wikilink")。[百慕達政府在](../Page/百慕達.md "wikilink")2009年設立[國家英雄日以取代英女皇壽辰](../Page/國家英雄日.md "wikilink")，令女皇壽辰不再成為公眾假期，有關決定在當時引起不少反對聲音。\[3\]

[香港在](../Page/香港.md "wikilink")1997年[香港主權移交](../Page/香港主權移交.md "wikilink")[中國後](../Page/中國.md "wikilink")，不再以英女皇壽辰為公眾假期。英女皇壽辰由[香港特別行政區成立紀念日所取代](../Page/香港特別行政區成立紀念日.md "wikilink")。在英國殖民地時期，英女皇壽辰的日子由1960、1970年代安排在4月21日，及至1980年代初更改為每年6月第二或第三個星期六和隨後的星期一。1997年是最後一個在香港慶祝英女皇壽辰假期的年份，當年因為遷就7月1日的[香港特別行政區成立紀念日](../Page/香港特別行政區成立紀念日.md "wikilink")，港府將英女皇壽辰安排在6月28日（星期六）和6月30日（星期一），配合當年主權移交假期（1997年7月1日及2日）和6月29日（星期日）。當年香港市民共享受一個連續5天的公眾假期。

[斐濟雖然在](../Page/斐濟.md "wikilink")1987年起成[共和國](../Page/共和國.md "wikilink")，並停止尊奉女皇為國家元首，但女皇仍然被奉為斐濟的「最高酋長」。直至2003年，當地才停止慶祝[查理斯王子壽辰](../Page/查理斯_\(威爾斯親王\).md "wikilink")；\[4\]而到2012年慶祝[女皇登基鑽禧紀念後](../Page/伊利沙伯二世登基鑽禧紀念.md "wikilink")，當地政府在2012年7月宣佈由2013年起不再慶祝英女皇壽辰。\[5\]

## 參考資料

<div style="font-size:small">

<references />

</div>

## 相關條目

  - [軍旗敬禮分列式](../Page/軍旗敬禮分列式.md "wikilink")
  - [大公官方壽辰](../Page/大公官方壽辰.md "wikilink")（[盧森堡](../Page/盧森堡.md "wikilink")）
  - [國王日](../Page/國王日.md "wikilink")（[荷蘭](../Page/荷蘭.md "wikilink")）
  - [万寿节](../Page/万寿节.md "wikilink")

## 外部連結

  - [BBC在2001年有關英國本土的英女皇壽辰報導](http://news.bbc.co.uk/1/hi/in_depth/uk/2001/birthday_honours_2001/1390951.stm)
  - [一個女皇，兩個生日](http://www.royal.gov.uk/output/page4820.asp)，英國皇室官方網站

[Category:英國節日](../Category/英國節日.md "wikilink")
[Category:英国君主](../Category/英国君主.md "wikilink")
[Category:君主生日](../Category/君主生日.md "wikilink")

1.  <http://www.docep.wa.gov.au/Corporate/Media/statements/2005/September/queens_birthday.html>
     <http://publicholidays.com.au/queens-birthday/>
2.  <http://www.workcover.act.gov.au/docs/fireworks.htm>
3.  "[Bermuda ditches Queen's Birthday public
    holiday](http://www.stuff.co.nz/world/371565)", *stuff.co.nz*, 17
    April 2008.
4.  "[Fiji Removes Prince Charles' Birthday Public
    Holiday](http://www.qppstudio.net/public-holidays-news/2003/fiji_001488.htm)",
    *The Fiji Times*, 11 June 2003.
5.  "[GOVERNMENT REVIEWS PUBLIC
    HOLIDAYS](http://www.fiji.gov.fj/index.php?option=com_content&view=article&id=6606:government-reviews-public-holidays&catid=71:press-releases&Itemid=155)
    ". Fiji: Ministry of Information, 31 July 2012.