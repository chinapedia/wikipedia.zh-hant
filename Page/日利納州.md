[Okresy_kraj_Zilina_Slovakia.png](https://zh.wikipedia.org/wiki/File:Okresy_kraj_Zilina_Slovakia.png "fig:Okresy_kraj_Zilina_Slovakia.png")
**日利納州**（、）是[斯洛伐克北部的一個州](../Page/斯洛伐克.md "wikilink")，與[波蘭和](../Page/波蘭.md "wikilink")[捷克接壤](../Page/捷克.md "wikilink")。面積6,788平方公里，人口692,332
(2001年)。首府[日利納](../Page/日利納.md "wikilink")。

下設11區。

[Category:斯洛伐克行政區劃](../Category/斯洛伐克行政區劃.md "wikilink")