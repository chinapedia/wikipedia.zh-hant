**美洲苦木科**又名[夷苦木科](../Page/夷苦木科.md "wikilink")，为[美洲苦木目下唯一的](../Page/美洲苦木目.md "wikilink")[科](../Page/科.md "wikilink")，共有2[属约](../Page/属.md "wikilink")46[种](../Page/种.md "wikilink")，生长在[中美洲和](../Page/中美洲.md "wikilink")[南美洲的](../Page/南美洲.md "wikilink")[安第斯山区](../Page/安第斯山.md "wikilink")。

本科[植物为](../Page/植物.md "wikilink")[乔木或](../Page/乔木.md "wikilink")[灌木](../Page/灌木.md "wikilink")，单[叶互生](../Page/叶.md "wikilink")，无托叶；[花小](../Page/花.md "wikilink")，[花瓣](../Page/花瓣.md "wikilink")3-5数；[果实为](../Page/果实.md "wikilink")[浆果或](../Page/浆果.md "wikilink")[蒴果](../Page/蒴果.md "wikilink")。

1981年的[克朗奎斯特分类法将其列在](../Page/克朗奎斯特分类法.md "wikilink")[苦木科中](../Page/苦木科.md "wikilink")，属于[无患子目](../Page/无患子目.md "wikilink")，1998年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
分类法认为应该单独分出一个](../Page/APG_分类法.md "wikilink")[科](../Page/科.md "wikilink")，但无法列在任何一个[目中](../Page/目.md "wikilink")，直接放到[蔷薇分支之下](../Page/蔷薇分支.md "wikilink")。2009年发表的[APG
III
分类法将本科置于](../Page/APG_III_分类法.md "wikilink")[美洲苦木目下](../Page/美洲苦木目.md "wikilink")。

## 参考文献

## 外部链接

  - 在[L. Watson和M.J. Dallwitz
    (1992年)《有花植物分科》](http://delta-intkey.com/angio/)中的[美洲苦木科](http://delta-intkey.com/angio/www/picramni.htm)
  - [NCBI中的美洲苦木科](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?lin=s&p=has_linkout&id=85159)

[\*](../Category/美洲苦木科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")