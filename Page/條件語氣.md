**條件語氣**（）是用在[條件句中提及事物的假設狀態](../Page/條件句.md "wikilink")，或視另一組境況而定的不確定事件的動詞形式。這種[語氣因此類似於](../Page/語氣.md "wikilink")[虛擬語氣](../Page/虛擬語氣.md "wikilink")，儘管有這兩種不同動詞形式的語言以不同的方式使用它們。

條件動詞形式還可以有時間用法，經常用來表達“過去將來”[時態](../Page/時態.md "wikilink")。

## 羅曼語言中的條件形式

儘管[拉丁語在條件句中使用直陳語氣和虛擬語氣](../Page/拉丁語.md "wikilink")，多數[羅曼語言發展出了條件變格范例](../Page/羅曼語言.md "wikilink")。這些形式的演化（和羅曼[將來時形式的創新](../Page/將來時.md "wikilink")）是[文法化的周知例子](../Page/文法化.md "wikilink")，語法和意義上獨立的詞成為帶有高度簡化語義功能的[規範詞素](../Page/規範詞素.md "wikilink")。羅曼條件（和將來時）形式起源自跟隨著限定形式的動詞
*habēre*
的拉丁不定式。這個動詞最初意味著在古典拉丁語中的“擁有／所有”，但是在后期拉丁語中被挑選為用作時間／語氣輔助的文法用途。詞序的固定（不定式
+ 助詞）和 *habēre* 的曲折形式的語音簡約最終導致兩個元素融合成一個單一語法形式。

## 參見

  - [條件句](../Page/條件句.md "wikilink")
  - [將來時](../Page/將來時.md "wikilink")
  - [虛擬語氣](../Page/虛擬語氣.md "wikilink")(Subjunctive mood)

## 延伸閱讀

  - Aski, Janice M. 1996. "Lightening the Teacher's Load: Linguistic
    Analysis and Language Instruction". *Italica* 73(4): 473-492.
  - Benveniste, E. 1968. "Mutations of linguistic categories". In Y.
    Malkiel and W.P. Lehmann (eds) *Directions for historical
    linguistics*, pp. 83–94. Austin and London: University of Texas
    Press.
  - Joseph, Brian D. 1983. *The synchrony and diachrony of the Balkan
    infinitive: a study in general, areal, and historical linguistics*.
    Cambridge: Cambridge University Press. .

[category:語氣](../Page/category:語氣.md "wikilink")