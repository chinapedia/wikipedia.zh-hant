**首字母縮略字**（）是將相關詞句的第一個字母[縮寫而組合成一個新字](../Page/縮寫.md "wikilink")。
「首字母縮略字」又稱為「頭字語」\[1\]。
而這個新字的發音則是依據這個新字書寫的方式。例如“”（[激光](../Page/激光.md "wikilink")）一字就是取“”的首字母縮略字。而它的發音就是依據laser的英文发音规则來發音。他們常常是幾個單詞的第一個字母合併而成（比如**[北約](../Page/北約.md "wikilink")**的縮寫詞NATO，**[激光](../Page/激光.md "wikilink")**的縮寫詞laser），也有幾個單詞分別的前幾個字母合併而成（比如**[比荷盧三國關稅同盟](../Page/比荷盧三國關稅同盟.md "wikilink")**的英文Benelux就是由三國的首字母**Be**lgium
**Ne**therlands **Lux**embourg合併而成）。

人們常常可以潛移默化地認識認知這些單詞，卻沒有一個潛在的標準。這些縮寫詞在過去並沒有很廣泛的應用，直到二十世紀才逐漸流行起來。在西方語言中，這些縮寫詞屬於[造詞法的範疇中](../Page/造詞法.md "wikilink")，而且被看作是[混合詞的一類](../Page/混合詞.md "wikilink")。在中文裏這些詞逐漸被人們接受，被收錄到了現代漢語詞典的附錄裏。

## 例子

**Messeger**,來自**ME**rcury **S**urface, **S**pace **E**nvironment,
**GE**ochemistry and
**R**anging的縮寫。是[信使號的全寫](../Page/信使號.md "wikilink")。意譯為「水星表面、太空環境、地球化學與廣泛探索」。

**Lisa** ,來自**L**aser **I**nterferometer **S**pace
**A**ntenna的縮寫，意指[雷射干涉太空天線](../Page/LISA.md "wikilink")，曾是一個由美國太空總署（NASA）和歐洲太空總署（ESA）合作的重力波探測計劃。

## 參見

  - [简称](../Page/简称.md "wikilink")
  - [縮寫](../Page/縮寫.md "wikilink")
  - [新语 (1984文学作品)](../Page/新语_\(1984\).md "wikilink")

## 参考文献

[Category:詞語類型](../Category/詞語類型.md "wikilink")
[字首縮寫](../Category/字首縮寫.md "wikilink")

1.  <https://www.wenku1.com/news/70B5AF7A995F6D39.html>