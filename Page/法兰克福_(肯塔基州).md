**法兰克福**（）是[美国](../Page/美国.md "wikilink")[肯塔基州的州府](../Page/肯塔基州.md "wikilink")、[富蘭克林縣縣治](../Page/富蘭克林縣_\(肯塔基州\).md "wikilink")。[肯塔基河穿城而过](../Page/肯塔基河.md "wikilink")。

## 历史

1780年代印第安人袭击了一群来自近肯塔基[列克星敦的欧洲移民](../Page/列克星敦_\(肯塔基州\).md "wikilink")。这些移民当时在[肯塔基河边的浅滩](../Page/肯塔基河.md "wikilink")（ford）制盐，移民中一个叫史蒂芬·法兰克（Stephen
Frank）人被杀，因此此地被称为**Frank's Ford**（直译为法兰克的浅滩），后来渐渐化作现名。\[1\]

## 姐妹城市

  - [聖佩得羅-德馬科里斯](../Page/聖佩德羅-德馬科里斯.md "wikilink")

## 参考文献

[F](../Category/肯塔基州城市.md "wikilink")
[F](../Category/美国各州首府.md "wikilink")
[Category:肯塔基州法兰克福](../Category/肯塔基州法兰克福.md "wikilink")
[Category:肯塔基州富兰克林县城市](../Category/肯塔基州富兰克林县城市.md "wikilink")

1.