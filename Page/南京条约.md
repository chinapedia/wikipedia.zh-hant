《**南京條約**》（），又稱《**江寧条约**》，大清朝廷称之为“万年和约”\[1\]，是大清首個因對[歐美國家戰敗而簽之](../Page/歐美.md "wikilink")[條約](../Page/條約.md "wikilink")。滿清[道光二十二年](../Page/道光皇帝.md "wikilink")（1842年），[大清在對](../Page/大清.md "wikilink")[英国的](../Page/英国.md "wikilink")[第一次鸦片战争中战败](../Page/第一次鸦片战争.md "wikilink")。雙方代表在南京[静海寺谈判并在泊于](../Page/静海寺.md "wikilink")[南京](../Page/南京.md "wikilink")[下關江面的英军旗舰](../Page/下关區.md "wikilink")[汗華囇號上签署](../Page/汗華囇號.md "wikilink")《南京條約》，以確切文件達成開放通商，並且大清向英國割讓[香港島](../Page/香港島.md "wikilink")。

1943年，[中華民國](../Page/中華民國.md "wikilink")[国民政府分别与英](../Page/国民政府.md "wikilink")、美等國签订协议，廢除一些[不平等條約內容](../Page/不平等條約.md "wikilink")，例如[治外法權](../Page/治外法權.md "wikilink")。

《南京條約》原件之一由[英國政府保存](../Page/英國政府.md "wikilink")；另一份正本由[中華民國外交部寄存於](../Page/中華民國外交部.md "wikilink")[臺北](../Page/臺北.md "wikilink")[外雙溪](../Page/外雙溪.md "wikilink")[國立故宮博物院](../Page/國立故宮博物院.md "wikilink")。

## 背景

[Replica_of_Treaty_of_Nanking.jpg](https://zh.wikipedia.org/wiki/File:Replica_of_Treaty_of_Nanking.jpg "fig:Replica_of_Treaty_of_Nanking.jpg")藏\]\]
1839年3月，大清[欽差大臣](../Page/欽差大臣.md "wikilink")[林則徐奉旨至廣州嚴禁鴉片](../Page/林則徐.md "wikilink")，與英國產生貿易、司法和外交衝突，使得英政府內閣在同年10月1日，決定派遣遠征軍至中國，從而引發[第一次鴉片戰爭](../Page/第一次鴉片戰爭.md "wikilink")。

1840年4月7日，英國反對派[托利黨在](../Page/托利黨.md "wikilink")[下議院動議](../Page/英國下議院.md "wikilink")，譴責政府在對華關係上缺乏遠見及思慮不週，亦忽視了應該給予駐廣州商務總監相關的訓令及權力，用以對付非法鴉片貿易\[2\]。
這動議特意迴避了戰爭及鴉片兩項敏感議題，藉此爭取托利黨內最大的支持以通過議案\[3\]。經過三日激辯，下議院最終以271票對262票否决動議，托利黨因此未能阻止英國艦隊繼續前往中國及戰爭的爆發。[上議院一項類似的動議](../Page/英國上議院.md "wikilink")\[4\]亦未能於5月12日會議中通過。下議院最終在戰事早已爆發後的1840年7月27日通過撥款173,442英鎊作為中國遠征軍的開支\[5\]。

1842年7月，英國軍舰百余艘與官兵约9千人，自[吴淞口溯](../Page/吴淞口.md "wikilink")[长江西上](../Page/长江.md "wikilink")，进攻长江与[京杭大运河交叉点的軍事要地](../Page/京杭大运河.md "wikilink")[镇江](../Page/镇江.md "wikilink")，意图截断南北交通，阻止[漕运](../Page/漕运.md "wikilink")。驻守镇江的1500名[八旗兵与英军展开激烈巷战](../Page/八旗.md "wikilink")，死伤惨重。对岸的[扬州绅商](../Page/扬州.md "wikilink")，惶恐万状，给了英國军隊50万銀元，作为不占领的交换条件。滿清無力對抗英國現代化的軍隊，[道光帝决定接受英国条件](../Page/道光帝.md "wikilink")，進行议和。

8月4日，英国军舰驶抵[南京](../Page/南京.md "wikilink")[下关江面](../Page/下关区.md "wikilink")，清政府代表在[静海寺与英国政府议约](../Page/静海寺.md "wikilink")，双方共在寺内談判约4次。8月12日，[砵甸乍](../Page/砵甸乍.md "wikilink")（又譯璞鼎查）提出：「讓地通商一端，大清必將香港地方讓與大英永遠據守。蓋大英之國體，既被大清之凌辱，理當讓地方以伏其罪，而補所傷之威儀也。」\[6\]

8月29日（[道光二十二年七月二十四日](../Page/道光.md "wikilink")），清廷代表[钦差大臣](../Page/钦差大臣.md "wikilink")[耆英](../Page/耆英.md "wikilink")、[伊-{里}-布和英国代表砵甸乍在英军旗舰](../Page/伊里布.md "wikilink")[汗华丽号上正式签订](../Page/康沃利斯號.md "wikilink")《南京条约》\[7\]。

## 部分内容

  - 大清和英国停战并締結永久和平及對等关系。
  - 清政府向英方开放沿海的[广州](../Page/广州.md "wikilink")、[福州](../Page/福州.md "wikilink")、[厦门](../Page/厦门.md "wikilink")、[宁波](../Page/宁波.md "wikilink")、[上海五处港口](../Page/上海.md "wikilink")，进行贸易通商。
  - 清政府将[香港岛割让予英国治理](../Page/香港岛.md "wikilink")。\[8\]
  - 清政府向英国共赔偿2千1百万[银圓](../Page/银圓.md "wikilink")。
  - 两国各自释放对方军民。
  - 英军撤出南京、[定海等处江面和岛屿](../Page/定海.md "wikilink")。
  - 兩國共同訂立進出口關稅。

英國商人在通商各口「應納進口、出口貨稅、餉費，均宜秉公議定則例」，從而開創了協定關稅之先例。廢除「公行」制度，規定以後「凡有英商等赴各該口岸貿易者，勿論與何商交易，均聽其便」。

## 中英文版本誤譯

《南京条约》的翻译工作由英方人员担任，由於清政府缺乏通曉英語的外交官員，所以主要的翻譯都是由英國的外交官[马儒翰](../Page/马儒翰.md "wikilink")、[郭士立和](../Page/郭士立.md "wikilink")[罗伯聃負責](../Page/罗伯聃.md "wikilink")。由於當時的翻譯條件並不如現在的成熟，因此，大清政府的版本和英國政府的版本在原文原意上出现差异\[9\]，令英人遲遲未能進入廣州城。

條約第二條的中文譯文为：  英文版本为：

條約第三條中文譯文為：

而英文條款为：

英文版本中“in perpetuity”的中文正確翻譯為：永遠、永久；而中文版本中的“常远”的解釋卻有空間討論。

## 影响

南京條約是自清朝于200多年前確立了較穩定的版圖後第一次因戰敗而所簽署的割地條約。随后，[美国和](../Page/美国.md "wikilink")[法国纷纷效仿英國](../Page/法国.md "wikilink")，分别与清政府签定《[中美望厦条约](../Page/中美望厦条约.md "wikilink")》和《[中法黄埔条约](../Page/黄埔条约.md "wikilink")》。

### 香港

香港正式成為英國殖民地，成為英國繼[印度後在](../Page/英屬印度.md "wikilink")[遠東的一個新據點](../Page/遠東.md "wikilink")，在19世紀開始逐漸發展成為轉口和貿易重鎮。往後基於受英國統治，避開了清末的腐敗和近代中國的政治不穩，成功由華南邊陲的一個小漁村蛻變成為國際城市，並在中國[改革開放前擔當中國與世界的連接橋樑](../Page/改革開放.md "wikilink")，影響中國後來推行改革開放發展毗鄰香港的[深圳](../Page/深圳.md "wikilink")，推動中國經濟現代化。

### 開放國門

《南京条约》为中國近代历史上第一个與外國戰敗而需要割讓土地和開放通商的不平等條約。在割让香港岛和开放口岸後，清政府再與英國簽訂《[中英五口通商章程](../Page/中英五口通商章程.md "wikilink")》（1843年7月22日）和《[五口通商附粘善后条款](../Page/五口通商附粘善后条款.md "wikilink")》（虎门条约，1843年10月8日）。雖然英国透過條約相继取得了[协定关税](../Page/协定关税.md "wikilink")、[治外法权](../Page/治外法权.md "wikilink")（仿照中美望厦条约索取了领事裁判权，中外人民诉讼各按本国法律管理）、划定[租界](../Page/租界.md "wikilink")、片面最惠国待遇、军舰停泊口岸等外交權利，在與大清帝國貿易時享有更多利益，清朝亦因英國的壓力而開放國門，加速現代化，亦間接為往後清朝覆亡、中華民國成立埋下伏筆。

### 經濟貿易

1843年至1844年，基於條約厦门、上海、宁波、福州、廣州相继开埠。厦门、福州、宁波因地理限制，商务并不繁盛。而位於长江口的上海因最接近主要出口物资——丝绸和茶叶的产地，又位于江、浙富庶之区，同时是中國南北海运的中间站，原在广州的英美商人及其雇佣的[买办蜂拥而至](../Page/买办.md "wikilink")，开设[洋行](../Page/洋行.md "wikilink")。1853年起，上海开始取代广州，成为全大清最大贸易港口。英、美、法三国相继沿[黄浦江设立租界](../Page/黄浦江.md "wikilink")，并不断扩展，形成[上海公共租界和](../Page/上海公共租界.md "wikilink")[上海法租界](../Page/上海法租界.md "wikilink")。

### 西方文化

鸦片战争时，在清朝约有30万的地下[天主教徒](../Page/天主教.md "wikilink")。[基督教](../Page/基督教.md "wikilink")[新教没有公开传教](../Page/新教.md "wikilink")，只有20名英美传教士在[澳门进行一些准备工作](../Page/澳门.md "wikilink")，例如翻译圣经以及编写字典。1846年，道光皇帝明诏弛禁天主教，归还原有教堂，天主教于是转而公开活动。[耶稣会负责江苏](../Page/耶稣会.md "wikilink")、安徽和直隶东南部的传教工作，[遣使会负责直隶的大部分和浙江](../Page/遣使会.md "wikilink")、江西，[多明我会则专门负责福建](../Page/多明我会.md "wikilink")。来自英、美、德三国十余个新教差会也纷纷在五口设立教堂、学校、医院。其中美国[归正会在厦门兴建了基督教在大清的第一所教堂](../Page/归正会.md "wikilink")——[新街堂](../Page/新街堂.md "wikilink")，[美北长老会的](../Page/美北长老会.md "wikilink")[嘉约翰在广州創办了清朝第一所西医院](../Page/嘉约翰.md "wikilink")[博济医院](../Page/博济医院.md "wikilink")。

## 香港回歸

中英兩國在1980年代開始就[香港前途問題談判](../Page/香港前途問題.md "wikilink")，並簽訂《[中英聯合聲明](../Page/中英聯合聲明.md "wikilink")》，香港[主權由](../Page/主權.md "wikilink")[英國移交至](../Page/英國.md "wikilink")[中華人民共和國](../Page/中華人民共和國.md "wikilink")。

然而當時的[中華民國總統](../Page/中華民國總統.md "wikilink")[李登輝曾表示](../Page/李登輝.md "wikilink")「《南京條約》的正本在[臺灣](../Page/臺灣.md "wikilink")，[香港的主權屬於](../Page/香港.md "wikilink")[中華民國](../Page/中華民國.md "wikilink")」\[10\]。

## 参见

  - [第一次鸦片战争](../Page/第一次鸦片战争.md "wikilink")
  - [穿鼻草約](../Page/穿鼻草約.md "wikilink")
  - [虎门条约](../Page/虎门条约.md "wikilink")
  - [中法黄埔条约](../Page/黄埔条约.md "wikilink")
  - [中美望廈條約](../Page/中美望廈條約.md "wikilink")
  - [第二次鸦片战争](../Page/第二次鸦片战争.md "wikilink")
  - [大安之役](../Page/大安之役.md "wikilink")

## 参考文献

## 外部链接

  -
  - [第七屆香港杯外交知識競賽 -
    中英南京條約](https://web.archive.org/web/20140811081943/http://marketing.mingpao.com/foreign_affairs/content.cfm?Path=p6_26.htm)

[Category:第一次鴉片戰爭](../Category/第一次鴉片戰爭.md "wikilink")
[Category:香港殖民地史](../Category/香港殖民地史.md "wikilink")
[Category:中英条约 (清朝)](../Category/中英条约_\(清朝\).md "wikilink")
[Category:不平等条约](../Category/不平等条约.md "wikilink")
[Category:領土變更相關條約](../Category/領土變更相關條約.md "wikilink")
[條](../Category/国立故宫博物院藏品.md "wikilink")
[Category:19世纪南京](../Category/19世纪南京.md "wikilink")
[Category:1842年中國](../Category/1842年中國.md "wikilink")
[Category:1842年英國](../Category/1842年英國.md "wikilink")
[Category:1842年條約](../Category/1842年條約.md "wikilink")

1.  [s:虎門條約](../Page/s:虎門條約.md "wikilink")
2.  有些人(甚至包括歷史學家)都誤以為英國國會在1840年4月9日以9票之差通過動議對華開戰。其實動議由提出。[1840年4月7日英國國會會議記錄(Hansard)中擇錄原文如下](https://api.parliament.uk/historic-hansard/commons/1840/apr/07/war-with-china#column_704):
    "The right hon. Baronet concluded with moving that— It appears to
    this House, on consideration of the papers relating to China,
    presented to this House, by command of her Majesty, that the
    interruption in our commercial and friendly intercourse with that
    country, and the hostilities which have since taken place, are
    mainly to be attributed to the want of foresight and precaution on
    the part of her Majesty's present advisers, in respect to our
    relations with China, and especially to their neglect to furnish the
    superintendent at Canton with powers and instructions calculated to
    provide against the growing evils connected with the contraband
    traffic in opium, and adapted to the novel and difficult situation
    in which the superintendent was placed."
    <http://hansard.millbanksystems.com/commons/1840/apr/07/war-with-china#column_704>
3.
4.  該動議由提出。[會議記錄中的原文如下](https://api.parliament.uk/historic-hansard/lords/1840/may/12/war-with-china#column_26):
    "That an humble address be presented to her Majesty, to express to
    her Majesty the deep concern of this House in learning that an
    interruption has occurred in the friendly relations and commercial
    intercourse which had so long subsisted with the Chinese empire; and
    to represent to her Majesty that these calamities have, in the
    opinion of this House, been occasioned by British subjects having
    persevered in taking opium to China, in direct and known violation
    of the laws of that empire; and to request that her Majesty will be
    graciously pleased to take immediate measures for the prevention of
    such proceedings, which are so dishonourable to the character, and
    so detrimental to the interests of her subjects; and to assure her
    Majesty, that if any additional powers should be found requisite for
    the purpose, this House will readily concur in granting them to her
    Majesty."
5.  英國國會會議記錄，1840 年5月12日及7月27日
6.
7.  中外旧约辞典 3页 ISBN 978-7-5436-0835-1
8.  南京条约原文：三、因大英商船远路涉洋，往往有损坏须修补者，自应给予沿海一处，以便修船及存守所用物料。今大皇帝准将香港一岛给予大英国君主暨嗣后世袭主位者常远据守主掌，任便立法治理。
9.
10.