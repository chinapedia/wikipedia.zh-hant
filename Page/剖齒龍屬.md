**剖齒龍屬**（[學名](../Page/學名.md "wikilink")：*Koparion*）是[獸腳亞目](../Page/獸腳亞目.md "wikilink")[恐龍的一](../Page/恐龍.md "wikilink")[屬](../Page/屬.md "wikilink")，目前的研究甚少。[模式種是](../Page/模式種.md "wikilink")**道氏剖齒龍**（*K.
douglassi*），是在1994年被描述、命名的。牠的[化石只有](../Page/化石.md "wikilink")[牙齒](../Page/牙齒.md "wikilink")，被認為是屬於[虛骨龍類](../Page/虛骨龍類.md "wikilink")，有可能是[傷齒龍科](../Page/傷齒龍科.md "wikilink")\[1\]。這些化石是發現於[美國](../Page/美國.md "wikilink")[猶他州](../Page/猶他州.md "wikilink")，地質年代被估計可追溯至[侏羅紀晚期的](../Page/侏羅紀.md "wikilink")[啟莫里階](../Page/啟莫里階.md "wikilink")，約1億5500萬至1億5000萬年前。[莫里遜組的剖齒龍化石](../Page/莫里遜組.md "wikilink")，發現於第6地層帶\[2\]。

## 參考文獻

<div class="references-small">

<references>

</references>

</div>

## 外部連結

  - [傷齒龍科](https://web.archive.org/web/20090710170842/http://www.thescelosaurus.com/troodontidae.htm)

[Category:傷齒龍科](../Category/傷齒龍科.md "wikilink")
[Category:北美洲恐龍](../Category/北美洲恐龍.md "wikilink")
[Category:侏羅紀恐龍](../Category/侏羅紀恐龍.md "wikilink")

1.  Chure, D. J. (1994). "*Koparion douglassi*, a new dinosaur from the
    Morrison Formation (Upper Jurassic) of Dinosaur National Monument;
    the oldest troodontid (Theropoda: Maniraptora)." *Brigham Young
    University Geology Studies*, **40**: 11-15.
2.  Foster, J. (2007). "Appendix." *Jurassic West: The Dinosaurs of the
    Morrison Formation and Their World*. Indiana University Press. pp.
    327-329.