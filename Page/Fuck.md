**Fuck**是个著名的[英语](../Page/英语.md "wikilink")[脏话](../Page/脏话.md "wikilink")，常委婉地写成**f××k**、**法克**或**F
word**。意思是[性交](../Page/性交.md "wikilink")，与[北京话的](../Page/北京话.md "wikilink")“[肏](../Page/肏.md "wikilink")”（ㄘㄠˋ/cào）、[河南话的](../Page/河南话.md "wikilink")“[尻](../Page/尻.md "wikilink")”\[1\]、[閩南語泉漳話的](../Page/泉漳話.md "wikilink")“[姦](../Page/干你娘.md "wikilink")”（-{幹}-）、[閩南語潮汕話的](../Page/潮汕话.md "wikilink")“[扑](../Page/扑.md "wikilink")”、[四川话的](../Page/四川话.md "wikilink")“[日](../Page/日.md "wikilink")\[2\]”、[粵语的](../Page/粵语.md "wikilink")“[屌](../Page/屌.md "wikilink")”，[长沙话的](../Page/长沙话.md "wikilink")“[嬲](../Page/嬲.md "wikilink")”，以及[陕西话的](../Page/陕西话.md "wikilink")“[贼](../Page/贼.md "wikilink")”（zéi）意思類似。这个字普遍被认为是極为冒犯的意義。在使用上，它既可作为一个[及物动词](../Page/及物动词.md "wikilink")，亦可作[不及物动词](../Page/不及物动词.md "wikilink")，到了現代甚至演变为[名词](../Page/名词.md "wikilink")、[感嘆词或](../Page/感嘆词.md "wikilink")[形容词](../Page/形容词.md "wikilink")。

## 和Fuck有關的常見詞語

  - Fuck you（另有Screw you）。

<!-- end list -->

  -
    「去你的」。

<!-- end list -->

  - What the Fuck（簡稱WTF）。

<!-- end list -->

  -
    華語：「他媽的怎麼了」、「搞什麼」；[臺灣話](../Page/臺灣閩南語髒話.md "wikilink")：「創啥洨」（國語諧音「衝三小」）、「啥洨」（國語諧音「殺小」）。
    美國英語：「Fucking good」太好了。
    美國英語：「Fucking shit」太糟了。
    英國英語：「just Fuck it」

## 歷史及來源

Fuck的[字源到了今時今日已變得眾說紛紜](../Page/字源.md "wikilink")，而且字義上是否從一開始就已經是具有[冒犯性](../Page/冒犯性.md "wikilink")，還是從某個年期開始才是，亦不能查考，但至少於15世纪的[英语文獻中已經可以看到這個字的蹤影](../Page/英语.md "wikilink")。雖然在英語世界具有權威地位的《[牛津英語詞典](../Page/牛津英語詞典.md "wikilink")》（Oxford
English
Dictionary）也認為Fuck的字源已難考究，但主張此字是源自於[盎格魯-撒克遜](../Page/盎格魯-撒克遜.md "wikilink")。

### 文獻出處

15世紀時有一首[诗](../Page/诗.md "wikilink")“[跳蚤和](../Page/跳蚤.md "wikilink")[苍蝇](../Page/苍蝇.md "wikilink")”，此诗由[拉丁语和](../Page/拉丁语.md "wikilink")[英语混合写成](../Page/英语.md "wikilink")，讽刺了[英国](../Page/英国.md "wikilink")[剑桥的](../Page/剑桥.md "wikilink")[卡尔梅修道士](../Page/卡尔梅修道士.md "wikilink")，其诗名取自第一句“Flen,
flyys, and
freris”，即“跳蚤，苍蝇和[修道士](../Page/修道士.md "wikilink")”。有fuck一词的一行是“Non
sunt in coeli, quia gxddbov xxkxzt pg ifmk.”其中的拉丁文“Non sunt in coeli,
quia”意为“他们（指修道士们）不在[天堂](../Page/天堂.md "wikilink")，因為『gxddbov xxkxzt pg
ifmk』”。這段後面的“gxddbov xxkxzt pg
ifmk”顯然是[隱語](../Page/隱語.md "wikilink")，不過只要透過[字母重映射的方法即可破译](../Page/凱撒密碼.md "wikilink")，其規則為向前回推一個字母（需注意當時的字母沒有J、U和W）：

  - `gxddbov xxkxzt pg ifmk.`

<!-- end list -->

  -

      -
        ↓

<!-- end list -->

  - `fvccant vvivys of heli.`

如此一來全句變为“Non sunt in coeli, quia fvccant vvivys of
heli.”意思是「他们不在天堂，因為他们『[幹](../Page/幹.md "wikilink")』了[伊利的](../Page/伊利_\(劍橋郡\).md "wikilink")[妇女](../Page/妇女.md "wikilink")。」句中的「fvccant」即為故意將「fuck」偽裝成拉丁文形式，即「fvcco」的現在式第三人稱複數。

### 可能的語源

一本於1598年出版的字典\[3\]認為Fuck這個字源自[拉丁文的](../Page/拉丁文.md "wikilink")或[古德語的](../Page/古德語.md "wikilink")(现代德语中仍在使用此词)，原意是「to
strike」（可作攻擊、襲擊、壓鑄等不同意思）或「to penetrate」（穿透），但在俗語（slang）卻可用來暗示性交（to
copulate）。著名的語源學家Eric Partridge亦指出這個德語字源自拉丁文，意思就是「pugilist, puncture,
and
prick」，就是刺穿的意思\[4\]。耐人寻味的是：到了今時今日，「prick」這個英文字除了解作刺穿以外，在現代口語中更常用來指男性的[陰莖](../Page/陰莖.md "wikilink")。

## 参见

  - [富金](../Page/富金.md "wikilink")（Fucking），位于奥地利的一个村庄
  - [七大脏词](../Page/七大脏词.md "wikilink")
  - [屌](../Page/屌.md "wikilink")
  - [幹你娘](../Page/幹你娘.md "wikilink")
  - [幹](../Page/幹.md "wikilink")
  - [Fuck：禁忌词与保护我们的第一修正案自由](../Page/Fuck：禁忌词与保护我们的第一修正案自由.md "wikilink")
  - [自主规制音](../Page/自主规制音.md "wikilink")

## 参考文献

## 延伸閱讀

  -
  -
  - Presents hundreds of uses of *fuck* and related words.

  - [Michael Swan](../Page/Michael_Swan_\(writer\).md "wikilink"),
    *Practical English Usage*, [Oxford University
    Press](../Page/Oxford_University_Press.md "wikilink"), 1995, ISBN
    0-19-431198-8.

  - [Wayland Young](../Page/Wayland_Young.md "wikilink"), *Eros Denied:
    Sex in Western Society*. Grove Press/Zebra Books, New York 1964.

  - [Carl Jung](../Page/Carl_Jung.md "wikilink"), *Psychology of the
    Unconscious: A Study of the Transformations and Symbolisms of the
    Libido*. Moffat, Yard and Company, New York 1916. Translated by
    [Beatrice M. Hinkle](../Page/Beatrice_M._Hinkle.md "wikilink"),
    M.D., Neurological Dept. of [Cornell University Medical
    School](../Page/Cornell_University_Medical_School.md "wikilink") and
    of the New York Post Graduate Medical School.

  - [Richard Dooling](../Page/Richard_Dooling.md "wikilink"), *Blue
    Streak: Swearing, Free Speech & Sexual Harassment,* (1996) ISBN
    0-679-44471-8. Chapters on famous swear words, including the f-word,
    and the laws pertaining to their use.

  - [Fuck](../Page/Fuck_\(film\).md "wikilink") – documentary film by
    Steve Anderson ([ThinkFilm](../Page/ThinkFilm.md "wikilink") 2005)

## 外部連結

  - [英語維基文庫中的「跳蚤和苍蝇」（Flen
    Flyys）一詩](https://en.wikisource.org/wiki/Flen_flyys)
  - [Re: the Cheney-Leahy incident,
    slate.com](https://web.archive.org/web/20080808070253/http://slate.msn.com/id//)
    - [discusses how American newspapers decide whether or not to print
    *fuck*.](http://www.slate.com/articles/news_and_politics/explainer/2004/06/when_do_papers_print_the_fword.html)
  - ["Online Etymology
    Dictionary."](http://www.etymonline.com/index.php?search=fuck&searchmode=none)
    Some etymological research on the word *fuck*.
  - [Fuck](http://ssrn.com/abstract=896790), academic paper exploring
    the legal implications of the word, by [Christopher M.
    Fairman](../Page/Christopher_M._Fairman.md "wikilink"), [Ohio State
    University](../Page/Ohio_State_University.md "wikilink") – [Michael
    E. Moritz College of
    Law](../Page/Moritz_College_of_Law.md "wikilink") March 2006. Ohio
    State Public Law Working Paper No. 59.
  - [Archive.org](https://archive.org/details/JackWagnerattr.MontyPythontheWordFuck)
    – [Jack Wagner](../Page/Jack_Wagner_\(announcer\).md "wikilink"),
    "The Word Fuck".

[Category:英語词语](../Category/英語词语.md "wikilink")
[粗](../Category/俗語.md "wikilink")
[下](../Category/性俗語.md "wikilink")
[X](../Category/髒話.md "wikilink")

1.
2.  即“入”
3.  John Florio's A Worlde of Wordes, London: Arnold Hatfield for Edw.
    Blount
4.  [What is the origin of the 'f'
    word?](http://dictionary.reference.com/help/faq/language/e52.html),
    [dictionary.com](../Page/dictionary.com.md "wikilink")