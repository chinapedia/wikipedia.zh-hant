[Coat_of_Arms_of_Ussuriysk_(Primorsky_kray).png](https://zh.wikipedia.org/wiki/File:Coat_of_Arms_of_Ussuriysk_\(Primorsky_kray\).png "fig:Coat_of_Arms_of_Ussuriysk_(Primorsky_kray).png")

**雙城子**，俄文稱**「烏蘇里斯克」**（），是[俄羅斯](../Page/俄羅斯.md "wikilink")[濱海邊疆區的一個城市](../Page/濱海邊疆區.md "wikilink")，坐落在[兴凯湖平原上](../Page/兴凯湖.md "wikilink")，[科马罗夫卡河和](../Page/科马罗夫卡河.md "wikilink")[拉科夫卡河之间](../Page/拉科夫卡河.md "wikilink")，距离[海参崴](../Page/海参崴.md "wikilink")112千米，双城子面积为3690平方千米。城市的主要工业为食品加工。因为双城子是铁路的一个重要交汇点，双城子又有机车修理工业，其中之一的大型双城子机车工厂建于1895年，是当时的[双城子铁路主要的维修工厂](../Page/双城子铁路.md "wikilink")。

在2015年，双城子人口为168,137人\[1\]，是滨海边疆区第二大城市（次于[海参崴](../Page/海参崴.md "wikilink")），其所在的双城子市在2015年人口总共为194,250人\[2\]。

雙城子最早在1866年被沙俄命名作[尼古拉斯科耶](../Page/尼古拉斯科耶.md "wikilink")（Nikolskoye，Нико́льское）。随着西伯利亚大铁路修建，1898年改名为[尼科利斯克-烏蘇里斯基](../Page/尼科利斯克-烏蘇里斯基.md "wikilink")（Nikolsk-Ussuriysky，
Нико́льск-Уссури́йский）。1934年7月22日，设立尼科利斯克-烏蘇里斯基州，州府驻尼科利斯克-烏蘇里斯基市。\[3\]\[4\]。1935年改称伏罗希洛夫市。1956年改称乌苏里斯克市。

## 歷史

[Ussuriysk-Stone-Tortoise-S-3542.jpg](https://zh.wikipedia.org/wiki/File:Ussuriysk-Stone-Tortoise-S-3542.jpg "fig:Ussuriysk-Stone-Tortoise-S-3542.jpg")，于1868年被發現，可能是[金朝墓葬遺物](../Page/金朝.md "wikilink")\]\]

  - 9世紀中期，雙城子之地是[渤海國的重鎮](../Page/渤海國.md "wikilink")，[虞婁靺鞨世居於此](../Page/虞婁靺鞨.md "wikilink")。
  - [金熙宗](../Page/金熙宗.md "wikilink")[天眷元年](../Page/天眷.md "wikilink")（1138年）置[上京路](../Page/上京路.md "wikilink")，治所在會寧府會寧縣（今[黑龍江省](../Page/黑龍江省.md "wikilink")[哈爾濱市](../Page/哈爾濱市.md "wikilink")[阿城區](../Page/阿城區.md "wikilink")[白城遺址](../Page/白城遺址.md "wikilink")）。轄[恤品路](../Page/恤品路.md "wikilink")，一作速頻路，治所在今[雙城子](../Page/雙城子.md "wikilink")。
  - 雙城子位於[綏芬河下游北岸和](../Page/綏芬河.md "wikilink")[富爾丹河下游西岸](../Page/富爾丹河.md "wikilink")。
  - [清代被稱為](../Page/清代.md "wikilink")**富爾丹城**（），其漢語名字「雙城子」則是根據滿語別名「**雙城堡**」（）意譯而來。
  - 1860年，根據《[中俄北京條約](../Page/中俄北京條約.md "wikilink")》，清朝將[黑龍江以北](../Page/黑龍江.md "wikilink")、[烏蘇里江以東之地劃歸](../Page/烏蘇里江.md "wikilink")[俄國](../Page/俄罗斯帝国.md "wikilink")，[雙城子遂成為](../Page/雙城子.md "wikilink")[俄國領土](../Page/俄罗斯帝国.md "wikilink")。
  - 1866年開埠，俄國人將該地命名為[尼古拉斯科耶](../Page/尼古拉斯科耶.md "wikilink")（Nikolskoye，），以紀念[俄國的](../Page/俄罗斯帝国.md "wikilink")[聖尼古拉](../Page/聖尼古拉.md "wikilink")，有13个来自[阿斯特拉罕和](../Page/阿斯特拉罕.md "wikilink")[沃罗涅什的家庭来到这个地方](../Page/沃罗涅什.md "wikilink")，1898年設[尼古爾斯克-烏蘇里斯基市](../Page/尼古爾斯克-烏蘇里斯基.md "wikilink")（）\[5\]。
  - 19世紀七十年代，[伯力和](../Page/伯力.md "wikilink")[海參崴之間建成鐵路](../Page/海參崴.md "wikilink")（如今該鐵路成為[西伯利亞鐵路的一部分](../Page/西伯利亞鐵路.md "wikilink")），由於[雙城子地處交通要道](../Page/雙城子.md "wikilink")，逐漸繁榮起來。
  - 1898年改名为[尼科利斯克-烏蘇里斯基](../Page/尼科利斯克-烏蘇里斯基.md "wikilink")（Nikolsk-Ussuriysky，
    ）
  - [日俄戰爭之後](../Page/日俄戰爭.md "wikilink")，雙城子成為俄國在遠東的重鎮之一。
  - 1935年被改名為「[伏罗希洛夫市](../Page/伏罗希洛夫市.md "wikilink")」（），以[蘇聯元帥](../Page/蘇聯元帥.md "wikilink")[克利缅特·叶夫列莫维奇·伏罗希洛夫命名](../Page/克利缅特·叶夫列莫维奇·伏罗希洛夫.md "wikilink")。
  - 1956年[苏共二十大反](../Page/苏共二十大.md "wikilink")[个人崇拜](../Page/个人崇拜.md "wikilink")，改名乌苏里斯克。
  - 目前[华人仍普遍称其为](../Page/华人.md "wikilink")“双城子”，[中华人民共和国出版的地图则将之标记为](../Page/中华人民共和国.md "wikilink")“乌苏里斯克（双城子）”。

## 交通

[雙城子鐵路](../Page/雙城子鐵路.md "wikilink")（[濱綏鐵路以東的延伸](../Page/濱綏鐵路.md "wikilink")）和[西伯利亞鐵路的交匯點在此市](../Page/西伯利亞鐵路.md "wikilink")。设有[双城子站](../Page/双城子站.md "wikilink")。南北向的[M60公路可前往](../Page/M60公路_\(俄罗斯\).md "wikilink")[伯力和](../Page/伯力.md "wikilink")[海参崴](../Page/海参崴.md "wikilink")，并在2012年修建了前往距离雙城子13千米的[米哈伊洛夫卡村的支路](../Page/米哈伊洛夫卡村.md "wikilink")。

[Станция_Уссурийск.jpg](https://zh.wikipedia.org/wiki/File:Станция_Уссурийск.jpg "fig:Станция_Уссурийск.jpg")

## 参考资料

[双](../Category/外滿洲城市.md "wikilink")
[双](../Category/滨海边疆区城市.md "wikilink")

1.

2.
3.

4.

5.