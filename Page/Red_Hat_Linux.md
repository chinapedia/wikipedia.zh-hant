**Red Hat Linux**是由[Red
Hat公司發行的一个](../Page/Red_Hat.md "wikilink")[Linux发行套件](../Page/Linux发行套件.md "wikilink")。

Red Hat
Linux可算是一個「中年」的Linux發行套件，其1.0版本於1994年11月3日發行。雖然其歷史不及[Slackware般悠久](../Page/Slackware_Linux.md "wikilink")，但比起很多的Linux發行套件，Red
Hat的歷史悠久得多。

Red Hat
Linux中的[RPM软件包格式可以说是Linux社区的一个事实标准](../Page/RPM套件管理員.md "wikilink")，被廣泛使用於其他Linux发行套件中。

以Red Hat
Linux为基礎派生的Linux發行套件有很多，其中包括以[桌面用戶為目標的](../Page/桌面.md "wikilink")[Mandrake
Linux](../Page/Mandrake_Linux.md "wikilink")（原為包含[KDE的Red](../Page/KDE.md "wikilink")
Hat Linux），[Yellow Dog
Linux](../Page/Yellow_Dog_Linux.md "wikilink")（開始時為支援[PowerPC的Red](../Page/PowerPC.md "wikilink")
Hat Linux）和[ASPLinux](../Page/ASPLinux.md "wikilink")（對非拉丁字元有較好支援的Red
Hat Linux）。

自从Red Hat 9.0版本发布后，Red Hat公司就不再开发桌面版的Linux发行套件，而将全部力量集中在服务器版的开发上，也就是[Red
Hat Enterprise
Linux版](../Page/Red_Hat_Enterprise_Linux.md "wikilink")。2004年4月30日，Red
Hat公司正式停止對Red Hat 9.0版本的支援，標誌著Red Hat Linux的正式完結。原本的桌面版Red Hat
Linux發行套件則與來自民間的Fedora計劃合併，成為[Fedora
Core發行版本](../Page/Fedora_Core.md "wikilink")。

## 特性

Red Hat
Linux擁有一個圖形化的安裝程序[Anaconda](../Page/Anaconda.md "wikilink")，目的是為了令新手更容易使用。同時，它有一個內建的[防火牆設置工具](../Page/防火墙_\(计算机\).md "wikilink")[Lokkit](../Page/Lokkit.md "wikilink")。

由Red Hat Linux
8.0開始，[UTF-8成為了系統預設的字元編碼設定](../Page/UTF-8.md "wikilink")。這對於英語用戶來說無甚影響，但當用到[ISO/IEC
8859-1字元集的較高位置字元時](../Page/ISO/IEC_8859-1.md "wikilink")，編碼方式則完全不同。對於部分[法語或](../Page/法語.md "wikilink")[瑞典語的用户來說](../Page/瑞典語.md "wikilink")，這是一個冒犯性的舉動，這問題可以透過移除"LANG"設定中的".UTF-8"來解決。

8.0版本亦是第一個使用Bluecurve桌面主題的發行版本。

因為可能發生的[版權或](../Page/版權.md "wikilink")[專利權問題](../Page/專利權.md "wikilink")，Red
Hat
Linux不包含很多的功能。例如[Rhythmbox和](../Page/Rhythmbox.md "wikilink")[XMMS中的](../Page/XMMS.md "wikilink")[MP3支持被Red](../Page/MP3.md "wikilink")
Hat方面移除了，並推薦用戶使用沒有版權问题的[Ogg
Vorbis取而代之](../Page/Ogg_Vorbis.md "wikilink")，但用戶可以自行安裝[MP3的支持](../Page/MP3.md "wikilink")。Red
Hat Linux亦缺少[NTFS的支持](../Page/NTFS.md "wikilink")，不過有需要的用戶亦可自行安裝。

## 臺灣抵制

2002年在[臺灣出現自由軟體用戶抵制Red](../Page/臺灣.md "wikilink") Hat Linux的活動。自於Red Hat
Linux 8.0版開始，Red Hat
Linux的國家列表中，將原本的「臺灣」改稱「中國台灣」，並移除[中華民國國旗](../Page/中華民國國旗.md "wikilink")\[1\]，此舉引發臺灣自由軟體界抵制Red
Hat Linux的活動，Red Hat Linux的臺灣社群用戶紛紛改換其他Linux發行版。

## 版本歷史

發行日期資料來源：comp.os.linux.announce（news:comp.os.linux.announce）

  - 1.0（Mother's Day）：1994年11月3日，49.95美元
  - 1.1（Mother's Day+0.1）：1995年8月1日，39.95美元
  - 2.0：1995年9月20日
  - 2.1：1995年11月23日
  - 3.0.3（Picasso）：1996年5月1日－第一個支援[DEC
    Alpha的版本](../Page/DEC_Alpha.md "wikilink")
  - 4.0（Colgate）：1996年8月8日－第一個支援[SPARC的版本](../Page/SPARC.md "wikilink")
  - 4.1（Vanderbilt）：1997年2月3日
  - 4.2（Biltmore）：1997年5月19日
  - 5.0（Hurricane）：1997年12月1日
  - 5.1（Manhattan）：1998年5月22日
  - 5.2（Apollo）：1998年11月2日
  - 6.0（Hedwig）：1999年4月26日
  - 6.1（Cartman）：1999年10月4日
  - 6.2（Zoot）：2000年4月3日
  - 7（Guinness）：2000年9月25日（本版本的編號是"7"，不是"7.0"）
  - 7.1（Seawolf）：2001年4月16日
  - 7.2（Enigma）：2001年10月22日
  - 7.3（Valhalla）：2002年5月6日
  - 8.0（Psyche）：2002年9月30日
  - 9（Shrike）：2003年3月31日（本版本的編號是"9"，不是"9.0"）

Fedora計劃與Red Hat計劃於2003年9月22日合併。

*Fedora Core的發行歷史請參見[Fedora Core條目](../Page/Fedora_Core.md "wikilink")*

## 參考文獻

<references/>

## 外部連結

  - [Red Hat首頁](http://www.redhat.com/)

[Category:Linux](../Category/Linux.md "wikilink") [Category:Red
Hat](../Category/Red_Hat.md "wikilink")

1.