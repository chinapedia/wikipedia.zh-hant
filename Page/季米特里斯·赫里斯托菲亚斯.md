[Dimitris_Christofias_and_Dmitry_Medvedev.jpg](https://zh.wikipedia.org/wiki/File:Dimitris_Christofias_and_Dmitry_Medvedev.jpg "fig:Dimitris_Christofias_and_Dmitry_Medvedev.jpg")

**季米特里斯·赫里斯托菲亚斯**（；），[賽普勒斯](../Page/賽普勒斯.md "wikilink")[共产主义政党](../Page/共产主义.md "wikilink")[勞動人民進步黨的前總書記](../Page/勞動人民進步黨.md "wikilink")，[2008年塞普勒斯總統選舉当選為](../Page/2008年塞普勒斯總統選舉.md "wikilink")[賽普勒斯總統](../Page/賽普勒斯總統.md "wikilink")。

生于基雷尼亚，1969～1974年就读于莫斯科社会科学院，获哲学博士学位。1954年加入劳进党。1976年当选劳进党尼科西亚-基雷尼亚地区委员会委员。1982年当选劳进党第15届中央委员会委员。1986年当选劳进党中央委员会政治局委员。1987年当选劳进学中央委员会秘书长。1988年当选劳进党中央委员会总书记。1991年、1996年、2001年三次当选塞议会议员。2001年6月7日，当选为议会议长。

## 總統

2008年2月24日，塞浦路斯劳动人民进步党中央总书记、议会议长季米特里斯·赫里斯托菲亚斯在第二輪投票大选中，以53.36%的得票战胜对手、前外长卡苏利季斯，当选为[塞浦路斯总统](../Page/塞浦路斯总统.md "wikilink")。俄国《[生意人报](../Page/生意人报.md "wikilink")》称，“他的获胜是一个历史性事件”。法国《[费加罗报](../Page/费加罗报.md "wikilink")》强调说，这是“[欧盟诞生的首位共产党人国家元首](../Page/欧盟.md "wikilink")”。[美国国务院也发表声明](../Page/美国国务院.md "wikilink")，祝贺他当选总统，表示期待与他合作，共同反恐，促进塞浦路斯两个民族的和解与统一。欧盟委员会主席[巴罗佐在贺电中称](../Page/巴罗佐.md "wikilink")，他的当选为打破塞浦路斯问题僵局提供新的机会。赫里斯托菲亚斯雖然是共產黨員，但他當選總統後宣布支持自由市場。

## 參考文獻

[Category:賽普勒斯人](../Category/賽普勒斯人.md "wikilink")
[Category:塞浦路斯总统](../Category/塞浦路斯总统.md "wikilink")
[Category:共产党领导人](../Category/共产党领导人.md "wikilink")