**石蒜科**（[学名](../Page/学名.md "wikilink")：）是[單子葉植物綱](../Page/單子葉植物綱.md "wikilink")，[天門冬目的一個](../Page/天門冬目.md "wikilink")[科](../Page/科.md "wikilink")。石蒜科分为三个亚科，約有60[屬](../Page/屬.md "wikilink")，800多[種](../Page/種.md "wikilink")，主要生长在世界各地的[温带地区](../Page/温带.md "wikilink")，[中国有](../Page/中国.md "wikilink")12属约25种。早期的分類系統（[克朗奎斯特分类法](../Page/克朗奎斯特分类法.md "wikilink")）將石蒜科分類在[百合目內](../Page/百合目.md "wikilink")，2003年经过改进的以[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
II
分类法认为可以和](../Page/APG_II_分类法.md "wikilink")[葱科及](../Page/葱科.md "wikilink")[百子莲科合并](../Page/百子莲科.md "wikilink")，也可以单分。

本科植物为多年生[草本](../Page/草本.md "wikilink")，地下部有一被薄膜的[鳞茎](../Page/鳞茎.md "wikilink")，较少为[根茎](../Page/根茎.md "wikilink")；基生少数细长[叶](../Page/叶.md "wikilink")；[花两性](../Page/花.md "wikilink")，单生或数朵排列成伞形花序，生于花茎顶端，下有一总苞，通常二至多枚膜质苞片构成，被片6枚、2轮，成美丽的花瓣状，下部常和生成长短不同的管，裂片上有附属物（副花冠），子房下位3枚；[蒴果或肉质](../Page/蒴果.md "wikilink")[浆果](../Page/浆果.md "wikilink")。

下列各屬常被栽培做為庭園觀賞植物：

  - [孤挺花屬](../Page/孤挺花屬.md "wikilink")
  - [君子蘭屬](../Page/君子蘭屬.md "wikilink") -
    [君子蘭](../Page/君子蘭.md "wikilink")
  - [雪滴花屬](../Page/雪滴花屬.md "wikilink")
  - [朱頂紅屬](../Page/朱頂紅屬.md "wikilink") -
    [朱頂紅](../Page/朱頂紅.md "wikilink")
  - [蜘蛛百合屬](../Page/蜘蛛百合屬.md "wikilink")
  - [雪片蓮屬](../Page/雪片蓮屬.md "wikilink")
  - [石蒜屬](../Page/石蒜屬.md "wikilink") - [石蒜](../Page/石蒜.md "wikilink")
  - [水仙屬](../Page/水仙屬.md "wikilink") - [水仙](../Page/水仙.md "wikilink")

## 屬

  - [孤挺花屬](../Page/孤挺花屬.md "wikilink") *Amaryllis* - 約有2種。
  - *Ammocharis*
  - *Apodolirion*
  - *Bokkeveldia*
  - *Boophone*
  - *Bravoa*
  - [傘百合屬](../Page/傘百合屬.md "wikilink") *Brunsvigia* - 約有20種。
  - *Caliphruria*
  - *Calostemma*
  - *Carpolyza*
  - *Chlidanthus*
  - *Choananthus*
  - [君子蘭屬](../Page/君子蘭屬.md "wikilink") *Clivia* - 約有4種。
  - *Cooperia*
  - [文殊蘭屬](../Page/文殊蘭屬.md "wikilink") *Crinum* - 約有65種。
  - *Cryptostephanus*
  - *Cybistetes*
  - [垂筒花屬](../Page/垂筒花屬.md "wikilink") *Cyrtanthus* - 約有50種。
  - [亞馬遜百合屬](../Page/亞馬遜百合屬.md "wikilink") *Eucharis*
  - *Eucrosia*
  - *Eustephia*
  - [雪滴花屬](../Page/雪滴花屬.md "wikilink") *Galanthus* - 約有20種。
  - *Gemmaria*
  - *Gethyllis*
  - *Griffinia*
  - [大韭蘭屬](../Page/大韭蘭屬.md "wikilink") *Habranthus*
  - [火球花屬](../Page/火球花屬.md "wikilink") *Haemanthus* - 約有20種。
  - *Hannonia*
  - *Hessea*
  - *Hieronymiella*
  - [朱頂紅屬](../Page/朱頂紅屬.md "wikilink") *Hippeastrum* - 約有55種。
  - [蜘蛛百合屬](../Page/蜘蛛百合屬.md "wikilink") *Hymenocallis* - 約有50種。
  - *Ismene*
  - *Lapiedra*
  - *Leptochiton*
  - [雪片蓮屬](../Page/雪片蓮屬.md "wikilink") *Leucojum* - 約有10種。
  - [石蒜屬](../Page/石蒜屬.md "wikilink") *Lycoris*
  - *Namaquanula*
  - [水仙屬](../Page/水仙屬.md "wikilink") *Narcissus* - 約有50種。
  - [納麗石蒜屬](../Page/納麗石蒜屬.md "wikilink") *Nerine* - 約有30種。
  - [假蔥屬](../Page/假蔥屬.md "wikilink") *Nothoscordum*
  - *Pamianthe*
  - [全能花属](../Page/全能花属.md "wikilink") *Pancratium* - 約有16種。
  - *Paramongaia*
  - *Phaedranassa*
  - *Phycella*
  - *Placea*
  - *Proiphys (Eurycles)*
  - *Pucara*
  - *Pyrolirion*
  - *Rauhia*
  - [細葉孤挺花屬](../Page/細葉孤挺花屬.md "wikilink") *Rhodophiala*
  - [网球花屬](../Page/网球花屬.md "wikilink")*Scadoxus*
  - [燕水仙屬](../Page/燕水仙屬.md "wikilink") *Sprekelia*
  - *Stenomesson*
  - [黃花石蒜屬](../Page/黃花石蒜屬.md "wikilink") *Sternbergia*
  - *Strumaria*
  - *Tapeinanthus*
  - *Tedingea*
  - *Traubia*
  - *Ungernia*
  - *Urceolina*
  - *Vagaria*
  - *Vallota*
  - *Worsleya*
  - [葱莲属](../Page/葱莲属.md "wikilink") *Zephyranthes* - 約有50種。

[\*](../Category/石蒜科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")