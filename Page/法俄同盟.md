[Avenue_Nicholas_II,_looking_towards_the_Dome_of_the_Invalides,_Exposition_Universal,_1900,_Paris,_France.jpg](https://zh.wikipedia.org/wiki/File:Avenue_Nicholas_II,_looking_towards_the_Dome_of_the_Invalides,_Exposition_Universal,_1900,_Paris,_France.jpg "fig:Avenue_Nicholas_II,_looking_towards_the_Dome_of_the_Invalides,_Exposition_Universal,_1900,_Paris,_France.jpg")的[亞歷山大三世橋和](../Page/亞歷山大三世橋.md "wikilink")[聖彼得堡的](../Page/聖彼得堡.md "wikilink")[聖三一橋至今仍是法俄同盟的象徵](../Page/聖三一橋_\(聖彼得堡\).md "wikilink")。|262x262px\]\]**法俄同盟**是1891年至1894年間[法國與](../Page/法國.md "wikilink")[俄國所正式締結的防衛性同盟](../Page/俄國.md "wikilink")，簡稱「**法俄同盟**」。主要內容為：

1.  若[德國](../Page/德意志帝國.md "wikilink")/[意大利進攻](../Page/意大利王國_\(1861年–1946年\).md "wikilink")[法國](../Page/法蘭西第三共和國.md "wikilink")，[俄國會出兵協助](../Page/俄罗斯帝国.md "wikilink")。
2.  若德國/[奧匈帝國進攻俄國](../Page/奧匈帝國.md "wikilink")，法國會出兵協助。
3.  若[三國同盟全體出兵進攻任何一方](../Page/三國同盟.md "wikilink")，須共同作戰。

## 背景

1878年的[柏林會議中](../Page/柏林會議.md "wikilink")，德國迫俄國修約。又於1879年與[奧匈帝國結](../Page/奧匈帝國.md "wikilink")[德奧同盟](../Page/德奧同盟.md "wikilink")。縱使有[三皇同盟](../Page/三皇同盟.md "wikilink")，但德國自柏林會議後
，擴大了奧俄在巴爾幹半島上的衝突，特別以奧匈帝國在[柏林會議中獲得波黑兩地與](../Page/柏林會議.md "wikilink")[俄國積怨更深](../Page/俄國.md "wikilink")，在[德國無法再維持兩國之間的友誼時](../Page/德國.md "wikilink")
,
德國首相[俾斯麥有鑑於奧匈帝國在歐洲形象比俄國良好](../Page/俾斯麥.md "wikilink")，加上兩國亦有[日耳曼民族血統](../Page/日耳曼民族.md "wikilink")，最終使德國放棄俄國而令法國有機可乘。

再者，俾斯麥於1890年退休後[威廉二世接掌外交事務](../Page/威廉二世_\(德國\).md "wikilink")，他深信德俄兩國的皇室姻親必令俄國義無反顧地順從德國，這使德國更漠視俄國，對俄并不再續簽[再保條約而與德正式脫交](../Page/再保條約.md "wikilink")。
加上這時俄國正積極發展[工業](../Page/工業.md "wikilink")，急需外資，德國拒貸而法國則批出貸款，使俄國有意轉投法國。雖然俄皇[亞歷山大三世討厭](../Page/亞歷山大三世_\(俄國\).md "wikilink")[共和主義](../Page/共和主義.md "wikilink")（即以共和國為政體），但也為雙方結盟奠下基礎。

## 參見

  - [三帝同盟](../Page/三帝同盟.md "wikilink")
  - [德奧同盟](../Page/德奧同盟.md "wikilink")
  - [三國同盟](../Page/三國同盟.md "wikilink")
  - [地中海協定](../Page/地中海協定.md "wikilink")
  - [英日同盟](../Page/英日同盟.md "wikilink")
  - [英法協約](../Page/英法協約.md "wikilink")
  - [英俄條約](../Page/英俄條約.md "wikilink")
  - [三國協約](../Page/三國協約.md "wikilink")
  - [同盟國](../Page/同盟國.md "wikilink")
  - [協約國](../Page/協約國.md "wikilink")
  - [第一次世界大戰](../Page/第一次世界大戰.md "wikilink")

[Category:第一次世界大战](../Category/第一次世界大战.md "wikilink")
[Category:法国外交史](../Category/法国外交史.md "wikilink")
[Category:俄罗斯外交史](../Category/俄罗斯外交史.md "wikilink")
[Category:俄羅斯帝國條約](../Category/俄羅斯帝國條約.md "wikilink")
[Category:法國條約](../Category/法國條約.md "wikilink")
[Category:法俄關係](../Category/法俄關係.md "wikilink")
[Category:法国军事同盟](../Category/法国军事同盟.md "wikilink")