**西藏南路站**位于[上海](../Page/上海.md "wikilink")[黃浦區](../Page/黃浦區.md "wikilink")[中山南路](../Page/中山南路.md "wikilink")[西藏南路](../Page/西藏南路.md "wikilink")，为[上海轨道交通4号线和](../Page/上海轨道交通4号线.md "wikilink")[8号线的地下车站和](../Page/上海轨道交通8号线.md "wikilink")[换乘站](../Page/换乘站.md "wikilink")，两线均为地下[岛式站台](../Page/岛式站台.md "wikilink")，其中4号线车站东西走向，位于地下三层，8号线车站南北走向，位于地下二层，两个站台在站台层即互相连通，这也是[上海轨道交通网络中首次出现的无需通过站厅层即可双向换乘其他线路的车站](../Page/上海轨道交通.md "wikilink")。

車站于2007年12月29日启用，同时本站也成为上海首个启用时即成为[换乘站的车站](../Page/换乘站.md "wikilink")，其中4号线站台由于2003年7月1日在[浦西](../Page/浦西.md "wikilink")[董家渡路附近发生的严重事故](../Page/董家渡路.md "wikilink")，令工程延误，而未与4号线其它车站一并启用。

## 公交换乘

23、45、66、66区间、96、96(区间)、109、144、306、318、327、715、780、869、938、969

<File:South> Xizang Road Station.jpg|四号线站台 <File:Xizang> Road (S)
Station Concourse.jpg|四、八号线[十字换乘大厅](../Page/换乘站.md "wikilink")

## 车站出口

4、8号线共用站体，因此出口编号也为统一使用

  - 1号口：中山南路北侧，西藏南路东
  - 2号口：中山南路南侧，西藏南路东
  - 3号口：中山南一路南侧，西藏南路西
  - 5号口：西藏南路西侧，中山南一路北

[Category:黄浦区地铁车站](../Category/黄浦区地铁车站.md "wikilink")
[Category:2007年启用的铁路车站](../Category/2007年启用的铁路车站.md "wikilink")
[Category:以街道命名的中國鐵路車站](../Category/以街道命名的中國鐵路車站.md "wikilink")