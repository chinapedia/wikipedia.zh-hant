**奧斯特里茨戰役**（1805年12月2日），是[拿破崙戰爭中的一場著名](../Page/拿破崙戰爭.md "wikilink")[戰役](../Page/戰役.md "wikilink")。75,000人的[法國軍隊在](../Page/法国.md "wikilink")[拿破崙的指揮下](../Page/拿破仑一世.md "wikilink")，在[波西米亚的](../Page/波西米亚.md "wikilink")[奧斯特里茨村](../Page/奧斯特里茨.md "wikilink")（位於今[捷克境內](../Page/捷克.md "wikilink")）取得了對87,000[俄羅斯](../Page/俄罗斯.md "wikilink")-[奧地利聯軍的決定性勝利](../Page/奥地利.md "wikilink")。[第三次反法同盟隨之瓦解](../Page/第三次反法同盟.md "wikilink")，并直接导致奧地利皇帝于次年被迫取消[神聖羅馬帝國皇帝的封號](../Page/神圣罗马帝国.md "wikilink")。

這場戰役因歐洲三個大國的皇帝奧皇[弗朗茨二世](../Page/弗朗茨二世_\(神圣罗马帝国\).md "wikilink")、沙皇[亞歷山大一世](../Page/亞歷山大一世_\(俄國\).md "wikilink")、法蘭西第一帝國皇帝[拿破崙全部親臨戰場](../Page/拿破仑一世.md "wikilink")，又稱“**三皇會戰**”。

## 背景

[Schlacht_bei_Austerlitz.jpg](https://zh.wikipedia.org/wiki/File:Schlacht_bei_Austerlitz.jpg "fig:Schlacht_bei_Austerlitz.jpg")
1804年的[歐洲](../Page/欧洲.md "wikilink")，戰爭烏云密布。自1802年《[亞眠和約](../Page/亞眠和約.md "wikilink")》以來，[英國和法國在](../Page/英国.md "wikilink")[西印度群島](../Page/西印度群岛.md "wikilink")、[地中海和](../Page/地中海.md "wikilink")[巴爾幹衝突不斷](../Page/巴爾幹.md "wikilink")；[俄羅斯和法國的關係也在惡化](../Page/俄罗斯.md "wikilink")。1804年3月21日，[波旁王室的](../Page/波旁王朝.md "wikilink")被法國政府以叛國罪處決，導致英國和俄羅斯對法國的不滿最終爆發。1805年4月，[英國首相](../Page/英国首相.md "wikilink")[威廉·庇特和俄羅斯](../Page/小威廉·皮特.md "wikilink")[沙皇](../Page/沙皇.md "wikilink")[亞歷山大一世簽訂](../Page/亞歷山大一世_\(俄羅斯\).md "wikilink")《[普雷斯堡和约](../Page/普雷斯堡和约.md "wikilink")》，英國和俄羅斯結成新的[反法同盟](../Page/反法同盟.md "wikilink")。在[第二次反法同盟戰爭中遭受重大損失的](../Page/第二次反法同盟.md "wikilink")[奧地利帝國起初持觀望態度](../Page/奧地利帝國.md "wikilink")。但1805年3月，已經成為法蘭西[皇帝的](../Page/皇帝.md "wikilink")[拿破崙·波拿巴進一步加冕自己為](../Page/拿破仑一世.md "wikilink")[意大利國王](../Page/意大利統治者列表.md "wikilink")。此舉激怒了奧地利皇帝[弗朗茨二世](../Page/弗朗茨二世_\(神圣罗马帝国\).md "wikilink")。1805年7月，奧地利也加入了反法同盟。第三次反法同盟形成。

反法同盟的主攻方向是[巴伐利亞](../Page/巴伐利亚.md "wikilink")。1805年8月底，[卡爾·馬克·馮·萊貝里希將軍和](../Page/卡爾·馬克·馮·萊貝里希.md "wikilink")[米哈伊爾·庫圖佐夫將軍分率奧地利和俄羅斯大軍](../Page/米哈伊尔·库图佐夫.md "wikilink")，向巴伐利亞的[烏爾姆進逼](../Page/烏爾姆.md "wikilink")。拿破崙迅速調遣部隊，在烏爾姆包圍了奧軍，並在庫圖佐夫趕來增援之前，於10月20日迫使馬克將軍投降。庫圖佐夫聞訊倉皇撤退，[繆拉親王指揮法軍乘勝追擊](../Page/若阿尚·缪拉.md "wikilink")，兵不血刃佔領奧地利首都[維也納](../Page/維也納.md "wikilink")。開戰至此，法軍取得了一連串勝利，戰果輝煌。但整個戰局得形勢對於法國來說依然十分嚴峻。[烏爾姆大捷的第二天](../Page/烏爾姆戰役.md "wikilink")，[英國皇家海軍就在](../Page/英國皇家海軍.md "wikilink")[特拉法加海戰中打敗法國和](../Page/特拉法加海戰.md "wikilink")[西班牙的聯合艦隊](../Page/西班牙.md "wikilink")，確保了英國的海上霸權，引起了[巴黎的恐慌](../Page/巴黎.md "wikilink")。庫圖佐夫擺脫了追擊，和[弗雷德里希·威廉·馮·巴克斯霍頓](../Page/弗雷德里希·威廉·馮·巴克斯霍頓.md "wikilink")（Friedrich
Wilhelm von
Buxhoeveden）將軍的增援俄軍以及俄皇奧皇的直屬部隊會合後，兵力超過法軍，隨時可能反撲。法國人此時遠離後方，[後勤供應緊張](../Page/後勤.md "wikilink")。[情報部門還帶來了更壞的消息](../Page/情報.md "wikilink")：[普魯士也即將加入反法同盟](../Page/普魯士.md "wikilink")，一旦普魯士參戰，法軍將腹背受敵。拿破崙需要迅速殲滅俄奧聯軍，一舉確立胜勢。

## 參戰部隊序列和指揮官

[Ulm_capitulation.jpg](https://zh.wikipedia.org/wiki/File:Ulm_capitulation.jpg "fig:Ulm_capitulation.jpg")以及麾下的奧地利軍隊，[查爾斯·瑟芬寧繪](../Page/查爾斯·瑟芬寧.md "wikilink")。\]\]

### 法軍（大兵團）

總司令：拿破崙

  - 法國皇家衛隊（近衛軍）：指揮[貝西埃爾](../Page/让-巴普蒂斯·贝西埃尔.md "wikilink")[法國元帥](../Page/法國元帥.md "wikilink")，人數：5,500人和24門砲
  - 第一軍團：軍長[貝爾納多特法國元帥](../Page/卡尔十四世·约翰.md "wikilink")，人數：13,000人和24門砲
  - 第三軍團：軍長[達武法國元帥](../Page/路易·尼古拉·达武.md "wikilink")，人數：6,300人（包括2500名騎兵）和12門砲
  - 第四軍團：軍長[尼古拉斯·讓·德迪烏·蘇爾特法國元帥](../Page/尼古拉斯·讓·德迪烏·蘇爾特.md "wikilink")，人數：23,600人和35門砲
  - 第五軍團：軍長[讓·拉納法國元帥](../Page/让·拉纳.md "wikilink")，人數：12,900人和20門砲
  - 擲彈兵：軍長[尼古拉·夏爾·烏迪諾法國元帥](../Page/尼古拉·夏尔·乌迪诺.md "wikilink")，人數：5,700人
  - 騎兵預備軍團：軍長[若阿尚·繆拉法國元帥](../Page/若阿尚·缪拉.md "wikilink")、親王，人數：7,400人和36門砲

### 俄奧聯軍

總指揮：[米哈伊爾·庫圖佐夫](../Page/米哈伊尔·库图佐夫.md "wikilink")（實際指揮權在[亞歷山大一世手上](../Page/亞歷山大一世_\(俄國\).md "wikilink")），左翼總指揮：[巴克斯霍頓](../Page/弗雷德里希·威廉·馮·巴克斯霍頓.md "wikilink")

  - 俄羅斯皇家衛隊（近衛軍）：指揮[康斯坦丁大公](../Page/康斯坦丁大公\(俄羅斯\).md "wikilink")，人數：6,730步兵，3,700騎兵，100先驅和40門砲
  - 沙皇右翼前鋒部隊：指揮[彼得·巴格拉基昂中將](../Page/彼得·伊萬諾維奇·巴格拉季昂.md "wikilink")，人數：9,200步兵，4,500騎兵和42門砲
  - [巴克斯霍頓將軍左翼前鋒部隊](../Page/弗雷德里希·威廉·馮·巴克斯霍頓.md "wikilink")：指揮[米迦勒·馮·基恩米亞將軍](../Page/米迦勒·馮·基恩米亞.md "wikilink")，人數：3,440步兵，3,440騎兵和12門輕型火砲
  - 第一縱隊：指揮[迪米特里·謝爾蓋耶維奇·多克托洛夫中將](../Page/迪米特里·謝爾蓋耶維奇·多克托洛夫.md "wikilink")
  - 第二縱隊：指揮[路易·亞歷山大·安德勞爾·德·朗熱隆中將](../Page/路易·亞歷山大·安德勞爾·德·朗熱隆.md "wikilink")
  - 第三縱隊：指揮[普雷斯比斯維斯基中將](../Page/普雷斯比斯維斯基.md "wikilink")
  - 第四縱隊：指揮[米哈伊爾·安德烈維奇·米羅拉多維奇中將和](../Page/米哈伊爾·安德烈維奇·米羅拉多維奇.md "wikilink")[克羅拉瑟](../Page/克羅拉瑟.md "wikilink")
  - 第五縱隊：指揮[約翰·馮·列支敦士登親王中將](../Page/約翰·約瑟夫一世_\(列支敦士登大公\).md "wikilink")

## 戰役進程

### 部署

1805年11月21日，拿破崙率領繆拉、拉納和蘇爾特三個軍進駐奧斯特里茨，他要把俄奧聯軍引進他親自選定的這個[戰場](../Page/戰場.md "wikilink")，以一個漂亮的殲滅戰徹底打破目前所面臨的困境。此時法軍在奧斯特里茨只有53,000人，對面的[奥洛穆茨聚集了](../Page/奥洛穆茨.md "wikilink")85,000俄奧聯軍，他相信聯軍會憑優勢兵力發動進攻。為了確保對手上鉤，拿破崙採取了一系列迷惑手段。先是在談判中故意示弱，接著讓部隊做出準備撤退的假象。最後又走出大膽的一步：主動放棄位於戰場中央的[戰略要地](../Page/战略.md "wikilink")[普拉欽高地](../Page/普拉欽.md "wikilink")，將自己的右翼徹底暴露在聯軍面前。與此同時，波拿道特和達武的兩個軍正在火速趕來增援。他們一旦到達，法軍總兵力將達到73,000人，足以和聯軍匹敵。對此重要消息，聯軍卻一無所知。

[Battle_of_Austerlitz,_Situation_at_1800,_1_December_1805.png](https://zh.wikipedia.org/wiki/File:Battle_of_Austerlitz,_Situation_at_1800,_1_December_1805.png "fig:Battle_of_Austerlitz,_Situation_at_1800,_1_December_1805.png")
12月1日，拿破崙做出了最後的部署。左翼由拉納的第五軍（13,000人）鎮守北面的[桑頓山](../Page/桑頓山.md "wikilink")，繆拉親王的5600名[騎兵預備軍在後支援](../Page/骑兵.md "wikilink")。波拿道特的第一軍也將在這一線發起攻擊。南方的[塔爾尼茲村和](../Page/塔爾尼茲村.md "wikilink")[索科爾尼茲村是拿破崙故意暴露出來的右翼](../Page/索科爾尼茲村.md "wikilink")，吸引聯軍進攻。這一側僅由蘇爾特軍的一個師12,000人把守，師長[克洛德·列格朗少將](../Page/克洛德·列格朗.md "wikilink")。達武的第三軍將在第二天凌晨抵達增援。蘇爾特軍餘下的兩個師（[多米尼克·旺達姆師和](../Page/多米尼克·旺達姆.md "wikilink")[路易·樊尚·勒布隆·德·聖海拉爾師](../Page/路易·樊尚·勒布隆·德·聖海拉爾.md "wikilink")）則潛伏在戰場中央，一旦聯軍主力都被吸引到南方，就一舉攻下普拉欽高地，切斷聯軍兩翼的聯繫。

與此同時，聯軍總部也制定了自己的計劃。老謀深算的庫圖佐夫主張繼續撤退以拖垮勞師遠征的法軍，但雄心勃勃的亞歷山大和急於復仇的奧地利將軍們被拿破崙的誘敵之計所迷惑，認為這是打敗拿破崙的天賜良機。儘管名義上庫圖佐夫是全軍總指揮，但實際的決定權在亞歷山大手中。最後，由奧地利的[洛維特將軍制定的聯軍作戰計劃正中拿破崙下懷](../Page/洛維特.md "wikilink")：巴格拉季昂指揮右翼前鋒部隊進攻桑頓山，牽制法軍。利希頓斯坦因的第五縱隊則負責保障他和中央接合部的安全。聯軍主攻方向在南線，左翼前鋒部隊和第一二三四縱隊在巴克斯霍頓的指揮下，將攻占塔爾尼茲村和索科爾尼茲村，打垮法軍的右翼，然後向北全面-{}-包抄。

### 南線和北線

[Battle_of_Austerlitz_-_Situation_at_0900,_2_December_1805.gif](https://zh.wikipedia.org/wiki/File:Battle_of_Austerlitz_-_Situation_at_0900,_2_December_1805.gif "fig:Battle_of_Austerlitz_-_Situation_at_0900,_2_December_1805.gif")
[Battle_of_Austerlitz_-_Situation_at_1400,_2_December_1805.gif](https://zh.wikipedia.org/wiki/File:Battle_of_Austerlitz_-_Situation_at_1400,_2_December_1805.gif "fig:Battle_of_Austerlitz_-_Situation_at_1400,_2_December_1805.gif")
12月2日凌晨6點，聯軍在瀰漫的大霧中開始移動。激戰首先在南線爆發，基恩米亞率領5,000奧軍組成的左翼前鋒部隊進攻塔爾尼茲村，被法軍擊退。8點，多克托洛夫第一縱隊的14,000俄軍趕到，以優勢兵力攻下了塔爾尼茲村。聯軍在塔爾尼茲村還未及站穩腳跟，達武已經率軍趕到並立即發起反擊，奪回塔爾尼茲村。隨後，聯軍乘混亂又發動了一次反擊，再次攻下塔爾尼茲村。

對塔爾尼茲村的進攻開始後不久，倫格朗指揮的第二縱隊（俄軍12,000人）和普雷斯比斯維斯基指揮的第三縱隊（俄軍10,000人）也先後投入了對索科爾尼茲村的進攻。索科爾尼茲村戰況激烈，俄軍首先攻下索科爾尼茲村後不久，敗退下來的法軍立即重整部隊，發動反擊奪回索科爾尼茲村。俄軍再次攻下索科爾尼茲村後，達武軍的[路易·弗里昂師趕到](../Page/路易·弗里昂.md "wikilink")，又將俄國人趕出索科爾尼茲村。此后索科爾尼茲村反复易手，但聯軍始終無法將其徹底佔據。

在北線，巴格拉季昂指揮的右翼前鋒部隊由13,000名俄軍組成，利希頓斯坦因的4,600名奧地利騎兵在他的側翼。他們負責進攻拉納把守的桑頓山。聯軍發動了數次猛烈的攻勢，但在法國[步兵](../Page/步兵.md "wikilink")，騎兵和[砲兵的協同防守下](../Page/砲兵.md "wikilink")，都被打退，上午10點30分，拉納和繆拉發動反擊，一舉將聯軍趕出北方戰場。

### 普拉欽高地

[Austerlitz-lejeune.jpg](https://zh.wikipedia.org/wiki/File:Austerlitz-lejeune.jpg "fig:Austerlitz-lejeune.jpg")

在南線和北線展開激戰的同時，蘇爾特軍的旺達姆師和聖海拉爾師（共16,000人）乘著濃霧的掩護，推進到普拉欽高地腳下靜靜潛伏，等待著進攻的信號。上午8點半，由24,000名俄奧聯軍組成的第四縱隊在克羅拉瑟的指揮下，也開始離開普拉欽高地，加入對南線的攻擊。至此，南線已吸引了超過5萬人的聯軍主力。中央的普拉欽高地正如拿破崙的設想一樣，變得兵力空虛，法軍將輕而易舉的從此突破。

上午9點，拿破崙對蘇爾特下達了進攻的命令。也就在這時，紅日終於透出雲層，驅散了濃霧。蟄伏已久的法軍範達姆師和聖海拉爾師的精銳士兵敲著鼓點，挺著[刺刀](../Page/刺刀.md "wikilink")，一舉衝上普拉欽高地。

面對法軍猝不及防的中路突破，普拉欽高地原上本洋溢著樂觀氣氛的聯軍總部頓時亂作一團。克羅拉瑟的部隊已經離開，利希頓斯坦因的部隊也在激戰當中。法軍輕易的就粉碎了駐守俄軍的抵抗，佔領了高地。倫格朗率第二縱隊反攻，也被蘇爾特軍擊退。11點30分，法軍已完全控制了普拉欽高地。隨後，拿破崙將大本營移到了普拉欽高地上，同時命令貝爾納多特從北線趕來增援。

為了避免被徹底分割的命運，康斯坦丁大公親自率領作為[預備隊的俄羅斯](../Page/預備隊.md "wikilink")[近衛軍做孤注一擲的反攻](../Page/近衛軍.md "wikilink")。拿破崙也將[貝西埃爾指揮的近衛軍投入戰鬥](../Page/貝西埃爾.md "wikilink")。戰局一時呈膠著狀態，但拿破崙手下還有完整的貝爾納多特第一軍，而聯軍已經沒有一隻預備隊可以投入戰鬥了。隨著貝爾納多特大軍源源不斷開上普拉欽高地，俄羅斯近衛軍終於支持不住，全軍潰退。到了下午2點，普拉欽高地上屍橫遍野，拿破崙完成了他分割聯軍的計劃，聯軍的中路徹底被擊潰。自此大局已定，聯軍的潰敗已成不可逆轉之勢。

### 聯軍的潰敗

下午2點，重整後的聖海拉爾師兵分兩路，從普拉欽高地南面開下。一路前往索科爾尼茲村，會同達武發起反擊；一路切斷南線聯軍的歸路。南線聯軍很快就被這最後的一擊打垮，各支部隊紛紛奪路而逃。混亂中，亞歷山大甚至丟掉了他的參謀部。孤身一人逃出的亞歷山大最後被俄軍在一個小村落中找到。擋在聯軍撤退線路上的是冰封的[扎錢湖](../Page/扎錢湖.md "wikilink")。奧地利和俄羅斯的士兵試圖從結冰的湖面逃離。但此時普拉欽高地上的大砲已經架好。砲彈呼嘯而下，落在湖面。湖冰碎裂，數千人葬身湖底。更多的聯軍士兵則只能在湖邊等待被俘。

下午4點30分，天降小雪。拿破崙策馬巡視戰場，奧斯特里茨戰役以法軍的輝煌勝利告終。

## 結果

[今日的奧斯特里茨古戰場](https://zh.wikipedia.org/wiki/File:Panorama_Austerlitz_Battle_Field_Prace_Czech_Rep.jpg "fig:今日的奧斯特里茨古戰場")
奧斯特里茨戰役中，聯軍損失超過26,000人，其中15,000人戰死，超過10,000人被俘。此外還損失了186門大砲，45面[團旗](../Page/團旗.md "wikilink")。法軍傷亡8,500人，損失1面[團旗](../Page/團旗.md "wikilink")。

1805年12月4日，弗朗茨二世和拿破崙會談，達成停火協議。12月5日（也就是戰後的第四天），\[1\]原本來宣戰的[外交使節見到拿破崙](../Page/外交使節.md "wikilink")，反而祝賀勝利並表示普魯士願意和法國結盟。拿破崙語帶嘲諷的說：「命運女神把你祝賀的對象改變了。」

12月27日，奧地利和法國簽訂[普雷斯堡和約](../Page/普雷斯堡和约.md "wikilink")。奧地利退出反法同盟，弗朗茨二世取消自己「神聖羅馬帝國皇帝」的封號。至此，第三次反法同盟瓦解，神聖羅馬帝國的歷史也告終結。拿破崙成為歐洲的霸主。

## 參見

  - [拿破崙](../Page/拿破仑一世.md "wikilink")
  - [拿破崙戰爭](../Page/拿破崙戰爭.md "wikilink")
  - [世界戰爭列表](../Page/世界戰爭列表.md "wikilink")

## 注释

## 參考文獻

  - *Todd Fisher:《The Napoleonic Wars: The rise of the Emperor
    1805-1807》ISBN 1-57958-357-1*
  - Brooks, Richard (editor). *Atlas of World Military History.* London:
    HarperCollins, 2000. ISBN 0-7607-2025-8
  - Chandler, David G. *The Campaigns of Napoleon.* New York: Simon &
    Schuster, 1995. ISBN 0-02-523660-1
  - Fisher, Todd & Fremont-Barnes, Gregory. *The Napoleonic Wars: The
    Rise and Fall of an Empire.* Oxford: Osprey Publishing Ltd., 2004.
    ISBN 1-84176-831-6
  - [Tolstoy, Leo](../Page/Leo_Tolstoy.md "wikilink"). *War and Peace.*
    London: Penguin Group, 1982. ISBN 0-14-044417-3
  - McLynn, Frank. *Napoleon: A Biography.* New York: Arcade Publishing
    Inc., 1997. ISBN 1-55970-631-7
  - Uffindell, Andrew. *Great Generals of the Napoleonic Wars.* Kent:
    Spellmount Ltd., 2003. ISBN 1-86227-177-1

<div class="references-small">

<references />

</div>

[Category:拿破仑战争战役](../Category/拿破仑战争战役.md "wikilink")
[Category:俄罗斯帝国战役](../Category/俄罗斯帝国战役.md "wikilink")
[Category:捷克战役](../Category/捷克战役.md "wikilink")
[Category:1805年衝突](../Category/1805年衝突.md "wikilink")
[Category:1805年奧地利帝國](../Category/1805年奧地利帝國.md "wikilink")
[Category:1805年法國](../Category/1805年法國.md "wikilink")
[Category:1805年12月](../Category/1805年12月.md "wikilink")

1.  《圖說兵器戰爭史》，作者/陳仲丹，出版/三聯書店（香港）有限公司，出版日期/2004年4月，ISBN 962-04-2348-8