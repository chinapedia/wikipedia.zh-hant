**約翰·肯尼斯·高伯瑞**（，），又譯為**約翰·加爾布雷斯**、蓋布雷斯或蓋布瑞斯，生於[加拿大](../Page/加拿大.md "wikilink")[安大略省](../Page/安大略省.md "wikilink")，[蘇格蘭裔美國](../Page/蘇格蘭.md "wikilink")[經濟學家](../Page/經濟學家.md "wikilink")（[制度經濟學家](../Page/制度經濟學.md "wikilink")、[凱因斯學派](../Page/凱因斯.md "wikilink")、美國[進步主義學者](../Page/進步主義.md "wikilink")）。

## 生平

1908年生於[加拿大](../Page/加拿大.md "wikilink")[安大略省](../Page/安大略省.md "wikilink")。1931年畢業於加國的[貴湖大學](../Page/貴湖大學.md "wikilink")，1933年取得同校[碩士](../Page/碩士.md "wikilink")，1934年獲得[柏克萊加州大學博士學位](../Page/柏克萊加州大學.md "wikilink")，並於同年獲聘於[哈佛大學擔任導師](../Page/哈佛大學.md "wikilink")。他同時也擁有來自全球超過五十個[榮譽博士學位](../Page/榮譽博士.md "wikilink")。

1937年成為[美國公民後](../Page/美國.md "wikilink")，多次擔任歷任[美國總統經濟顧問](../Page/美國總統.md "wikilink")，如曾助[民主黨的](../Page/美國民主黨.md "wikilink")[杜魯門管控工資及物價水準](../Page/杜魯門.md "wikilink")。於1946年和2000年分別由杜鲁门總統和同為民主黨的[柯林頓總統颁发](../Page/柯林頓.md "wikilink")「[总统自由勋章](../Page/总统自由勋章.md "wikilink")」。

加尔布雷斯继承美国制度学派的传统，坚持以制度为经济学的研究对象。认为把资源配置与利用作为研究对象是一种错误，把经济学分为微观宏观是一种不幸。经济学研究要改变重物轻人，只看产值不见福利的倾向。他研究了广泛的课题，使制度经济学成为庞大而复杂的体系。从制度角度分析资本主义社会，提出二元体系理论。认为美国社会由计划体系和市场体系两部分构成，计划体系由1000家大公司组成，权力掌握在技术和管理人员手中，控制着市场和价格，从而也控制着市场体系。市场体系则由1200万小企业、农场、个体经营者组成，完全听任市场支配。由于计划体系占统治和支配地位，美国这种丰裕社会存在各种收入分配不平等、经济发展不平衡、资源配置失调、通货膨胀与失业等问题。为解决这些问题，需进行制度改革，通过国家力量使两种体系的权力与收入平等化。称实现两种体系平等化的社会为新社会主义。

## 著作

憑藉其豐富的經驗，共有三本重要作品：1958年的《富裕的社會》（The affluent Society），1967年出版《新工業國家》（The
New Industrial State）以及1973年的《經濟治國》（Economics and the Public Purpose）。

## 逸事

值得一提的是，他的身高203公分，比[英國](../Page/英國.md "wikilink")[經濟學家](../Page/經濟學家.md "wikilink")[凯恩斯的](../Page/凯恩斯.md "wikilink")196公分還高。

[Category:美国经济学家](../Category/美国经济学家.md "wikilink")
[Category:加拿大經濟學家](../Category/加拿大經濟學家.md "wikilink")
[Category:哈佛大學教師](../Category/哈佛大學教師.md "wikilink")
[Category:美国自由主义](../Category/美国自由主义.md "wikilink")
[Category:波士頓人](../Category/波士頓人.md "wikilink")
[Category:總統自由勳章獲得者](../Category/總統自由勳章獲得者.md "wikilink")
[Category:普林斯頓大學教師](../Category/普林斯頓大學教師.md "wikilink")
[Category:加州大學柏克萊分校校友](../Category/加州大學柏克萊分校校友.md "wikilink")
[Category:貴湖大學校友](../Category/貴湖大學校友.md "wikilink")
[Category:罗蒙诺索夫金质奖章获得者](../Category/罗蒙诺索夫金质奖章获得者.md "wikilink")