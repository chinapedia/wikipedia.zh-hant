[Friedrich_woehler.jpg](https://zh.wikipedia.org/wiki/File:Friedrich_woehler.jpg "fig:Friedrich_woehler.jpg")

**弗里德里希·维勒**（，），[德国](../Page/德国.md "wikilink")[化学家](../Page/化学家.md "wikilink")。他因人工合成了[尿素](../Page/尿素.md "wikilink")，打破了[有机化合物的](../Page/有机化合物.md "wikilink")“生命力”学说而闻名。

## 生平简历

  - 1800年7月31日，出生于[法兰克福附近的Eschersheim](../Page/法兰克福.md "wikilink")，父亲是当地一位有名的医生。
  - 1820年﹣1822年，在[马尔堡大学学习医学](../Page/马尔堡大学.md "wikilink")。
  - 1822年﹣1823年，在[海德堡大学学习医学](../Page/海德堡.md "wikilink")，同时跟随[利奥波德·格麦林学习化学](../Page/利奥波德·格麦林.md "wikilink")。（一说1821年开始）
  - 1823年9月2日，完成了在海德堡大学的学业，获得外科学医学博士学位，被导师格麦林推荐到位于[瑞典](../Page/瑞典.md "wikilink")[斯德哥尔摩的](../Page/斯德哥尔摩.md "wikilink")[永斯·贝采利乌斯的实验室工作](../Page/永斯·贝采利乌斯.md "wikilink")。
  - 1825年﹣1831年，在德国[柏林理工学校教授化学课程](../Page/柏林理工学校.md "wikilink")。
  - 1831年﹣1836年，在德国[卡塞尔高等理工学校教授化学课程](../Page/卡塞尔高等理工学校.md "wikilink")。
  - 1836年﹣1882年，在[哥廷根大学任化学正教授](../Page/哥廷根大学.md "wikilink")。
  - 1882年9月23日，在[哥廷根去世](../Page/哥廷根.md "wikilink")。

## 科学成就

维勒一生發表过化学论文270多篇，获得世界各国给予的荣誉纪念达317种，是一位勤勉的化学家。他的主要成就有以下几方面：

### 人工合成尿素

维勒自1824年起研究氰酸铵的合成，但是他发现在[氰酸中加入](../Page/氰酸.md "wikilink")[氨水后蒸乾得到的白色晶体并不是铵盐](../Page/氨水.md "wikilink")，到了1828年他终于证明出这个实验的产物是尿素。维勒由于偶然的发现了从无机物合成有机物的方法，而被认为是[有机化学研究的先锋](../Page/有机化学.md "wikilink")。在此之前，人们普遍认为：有机物只能依靠一种生命力在[动物或](../Page/动物.md "wikilink")[植物体内产生](../Page/植物.md "wikilink")；人工只能合成无机物而不能合成有机物。维勒的老师[永斯·贝采利乌斯当时也支持生命力学说](../Page/永斯·贝采利乌斯.md "wikilink")，他写信给维勒问他能不能在实验室里“制造出一个小孩来”。

维勒将自己的发现和实验过程写成题目为“论尿素的人工制成”的论文，发表在1828年《[物理学和化学年鉴](../Page/物理学和化学年鉴.md "wikilink")》第12卷上。他的论文详尽记述了如何用[氰酸与](../Page/氰酸.md "wikilink")[氨水或](../Page/氨水.md "wikilink")[氯化铵与](../Page/氯化铵.md "wikilink")[氰酸银来制备纯净的尿素](../Page/氰酸银.md "wikilink")。随着其他化学家对他的实验的重现成功，人们认识到有机物是可以在实验室由人工合成的，这打破了多年来占据有机化学领域的生命力学说。随后，[乙酸](../Page/乙酸.md "wikilink")、[酒石酸等有机物相继被合成出来](../Page/酒石酸.md "wikilink")，支持了维勒的观点。

### 发现同分异构体现象

发现尿素的过程同时说明[氰酸铵和尿素的分子式是相同的](../Page/氰酸铵.md "wikilink")，这是[同分异构的最早的例证](../Page/同分异构.md "wikilink")。接着，维勒又发现氰酸和另一位德国化学家[尤斯图斯·冯·李比希在](../Page/尤斯图斯·冯·李比希.md "wikilink")1824年发现的[雷酸的分子式相同](../Page/雷酸.md "wikilink")。1830年，贝采利乌斯提出了“同分异构”学说：同样的化学成分，可以组成性质不同的化合物，它们的化学成分一样，却是性质不同的化合物。而在此之前，化学界一向认为，同一种成分不可能同时存在于两种不同的化合物之中。

### 发现并制备铝等元素单质

1827年维勒用金属[钾还原熔融的无水氯化铝得到较纯的金属](../Page/钾.md "wikilink")[铝单质](../Page/铝.md "wikilink")。维勒还用同样的方法发现了[铍](../Page/铍.md "wikilink")（1828年）、[钇](../Page/钇.md "wikilink")，并且命名了铍。

另一个关于元素發现的著名的故事是说维勒在分析一种来自墨西哥的矿石的时候虽然猜测到其中可能含有一种新元素，但是由于没有认真研究而放过了，他的同门师兄[尼尔斯·加布里埃尔·塞夫斯特瑞姆认真分析了其中的成分](../Page/尼尔斯·加布里埃尔·塞夫斯特瑞姆.md "wikilink")，找到了元素[钒](../Page/钒.md "wikilink")。一般的科学资料上介绍则是维勒在塞夫斯特瑞姆发现钒后为后者证实了钒的存在。

### 其他贡献

维勒还分离出[硼](../Page/硼.md "wikilink")，研究了[硅烷和](../Page/硅烷.md "wikilink")[钛及其化合物的性质](../Page/钛.md "wikilink")。1842年他制备了[碳化钙](../Page/碳化钙.md "wikilink")，并证明它与水作用，放出[乙炔](../Page/乙炔.md "wikilink")。

维勒和李比希还合作在有机化学方面做出了很多贡献。1832年他和李比希共同发现了[苯甲酸基团](../Page/苯甲酸.md "wikilink")，研究了有机化学的基团反应；1837年又共同发现了[扁桃苷](../Page/扁桃苷.md "wikilink")；1848年维勒发现了[氢醌](../Page/氢醌.md "wikilink")。

## 主要著作

  - Lehrbuch der Chemie/化学课本，德累斯顿，1825年出版，4卷；
  - Grundriss der Anorganischen Chemie/无机化学概论，柏林，1830年出版
  - Grundriss der Organischen Chemie/有机化学概论，柏林，1840年出版
  - Praktischen Uebringen der Chemischen Analyse/分析化学实验教程，柏林，1854年出版

## 参见

  - [有机化学](../Page/有机化学.md "wikilink")
  - [尿素](../Page/尿素.md "wikilink")
  - [尤斯图斯·冯·李比希](../Page/尤斯图斯·冯·李比希.md "wikilink")（Justus Freiherr von
    Liebig）德国化学家，创立了有机化学。
  - [氰酸银](../Page/氰酸银.md "wikilink")
  - [雷酸银](../Page/雷酸银.md "wikilink")
  - [同分異構](../Page/同分異構.md "wikilink")
  - [史丹利·米勒](../Page/史丹利·米勒.md "wikilink")

## 来源

<references/>

[Category:德国化学家](../Category/德国化学家.md "wikilink")
[Category:瑞典皇家科學院院士](../Category/瑞典皇家科學院院士.md "wikilink")
[Category:哥廷根大學教師](../Category/哥廷根大學教師.md "wikilink")
[Category:海德堡大學校友](../Category/海德堡大學校友.md "wikilink")
[Category:馬爾堡大學校友](../Category/馬爾堡大學校友.md "wikilink")
[Category:黑森人](../Category/黑森人.md "wikilink")
[Category:化學元素發現者](../Category/化學元素發現者.md "wikilink")
[Category:法蘭克福人](../Category/法蘭克福人.md "wikilink")
[Category:皇家学会外籍会员](../Category/皇家学会外籍会员.md "wikilink")
[Category:科普利獎章獲得者](../Category/科普利獎章獲得者.md "wikilink")