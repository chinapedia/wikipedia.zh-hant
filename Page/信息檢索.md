**資訊檢索**（）是从[信息资源集合获得与信息需求相关的信息资源的活动](../Page/信息.md "wikilink")。搜索可以基于[全文或其他基于内容的索引](../Page/全文檢索.md "wikilink")。

自动信息检索系统用于减少所谓的“[資訊超載](../Page/資訊超載.md "wikilink")”。许多[大學和](../Page/大學.md "wikilink")[公共图书馆使用IR系统提供图书](../Page/公共图书馆.md "wikilink")、期刊和其他文件的访问。Web搜索引擎是最常见的IR应用程序。

## 概述

当用户向系统输入[查询时](../Page/查询字符串.md "wikilink")，信息检索过程开始。查询是[信息需求的正式声明](../Page/資訊需求.md "wikilink")，例如在Web搜索引擎中的搜索字符串。在信息检索中，查询不会唯一地标识集合中的单个对象。相反可以有不止一个对象匹配查询，它们可能具有不同程度的相关性。

对象是由内容集合或[数据库中的信息表示的实体](../Page/数据库.md "wikilink")。用户查询要与数据库信息进行匹配。然而，与数据库的经典SQL查询相反，在信息检索中，返回的结果可能匹配或不匹配查询，因此结果通常被排名。这种结果排名是信息检索搜索与数据库搜索相比的关键区别。\[1\]

根据应用，数据对象可以是文本文档、图像\[2\]、音频\[3\]、[思维导图](../Page/心智图.md "wikilink")\[4\]或视频等。通常文档本身不保存或直接存储在IR系统中，而是以文献替代或[元数据在系统中表示](../Page/元数据.md "wikilink")。

大多数IR系统对数据库中的每个对象与查询匹配的程度计算数值分数，并根据此值对对象进行排名。然后向用户显示排名靠前的对象。如果用户希望细化查询，则可以重复该过程。\[5\]

## 信息检索的类型

按照检索手段，可分为：

  - 传统信息检索（手工检索）和
  - 现代信息检索（计算机检索）；

按照检索内容，分为：

  - 书目检索、
  - 数据检索、
  - 事实检索、
  - [全文检索](../Page/全文检索.md "wikilink")、
  - [图像检索](../Page/图像检索.md "wikilink")：例如：Google images
  - 多媒体检索：例如：[Soundhound](../Page/Soundhound.md "wikilink")（[聲頻檢索](../Page/聲頻檢索.md "wikilink")）。

## 信息检索的主要技术指标

傳統的指標：

  - 齊全率
  - 準確率
  - 检索速度

常用的指標代號：

  - \(X \cap Y\)：兩個檢索的[交集](../Page/交集.md "wikilink")
  - \(| X |\)：檢索結果的[數量](../Page/势_\(数学\).md "wikilink")
  - \(\int\)：[積分](../Page/積分.md "wikilink")
  - \(\sum\)：[求和](../Page/求和符号.md "wikilink")
  - \(\Delta\)：[对称差](../Page/对称差.md "wikilink")

## 检索系统

運用一定的方法從某種資訊媒介上（包括書、硬碟、光碟等）
的資料中查找所需要情報的系統。一般可區分為手工情報檢索系統（[檢索卡](../Page/檢索卡.md "wikilink")）、機械情報檢索系統（[微縮卷](../Page/微縮卷.md "wikilink")）和計算機情報檢索系統三大類。

[ProQuest是目前最大及歷史最悠久的情報檢索服務供應商](../Page/ProQuest.md "wikilink")，從1938年起就開始為學校把[期刊製成微縮膠卷來儲存](../Page/期刊.md "wikilink")
\[6\]。這些膠卷在數碼以後，繼續以光碟陣及網上服務的形式為學校提供過期期刊內容的存取服務\[7\]。

以下為市面上比較常見的情報系統：

  - [DIALOG](../Page/DIALOG.md "wikilink")
  - [Ovum](../Page/Ovum.md "wikilink")
  - [Emerald](../Page/Emerald.md "wikilink")
  - [ABI](../Page/ABI.md "wikilink")

## 參考文獻

## 參見

  - [搜索引擎](../Page/搜索引擎.md "wikilink")
      - [网络搜索引擎](../Page/网络搜索引擎.md "wikilink")
  - [跨語檢索](../Page/跨語檢索.md "wikilink")
  - [文本信息检索](../Page/文本信息检索.md "wikilink")
  - [图像搜索](../Page/图像搜索.md "wikilink")

{{-}}

[sv:Informationsåtkomst](../Page/sv:Informationsåtkomst.md "wikilink")

[Category:圖書資訊科學](../Category/圖書資訊科學.md "wikilink")
[Category:電腦科學](../Category/電腦科學.md "wikilink")
[信息检索](../Category/信息检索.md "wikilink")

1.  Jansen, B. J. and Rieh, S. (2010) [The Seventeen Theoretical
    Constructs of Information Searching and Information
    Retrieval](https://faculty.ist.psu.edu/jjansen/academic/jansen_theoretical_constructs.pdf).
    Journal of the American Society for Information Sciences and
    Technology. 61(8), 1517-1534.
2.
3.
4.
5.
6.
7.