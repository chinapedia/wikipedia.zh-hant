**天祚**（935年九月-937年十月）是[南吴睿帝](../Page/南吴.md "wikilink")[杨溥的](../Page/杨溥_\(吴\).md "wikilink")[年號](../Page/年號.md "wikilink")，共计3年。

## 紀年

| 天祚                               | 元年                             | 二年                             | 三年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 935年                           | 936年                           | 937年                           |
| [干支](../Page/干支纪年.md "wikilink") | [乙未](../Page/乙未.md "wikilink") | [丙申](../Page/丙申.md "wikilink") | [丁酉](../Page/丁酉.md "wikilink") |

## 參看

  - [中國年號索引](../Page/中國年號索引.md "wikilink")
  - 同期存在的其他政权年号
      - [清泰](../Page/清泰.md "wikilink")（934年四月至936閏十一月）：後唐—[李從珂之年號](../Page/李從珂.md "wikilink")
      - [天福](../Page/天福_\(石敬瑭\).md "wikilink")（936年十一月至944年六月）：[後晉](../Page/後晉.md "wikilink")—[石敬瑭之年號](../Page/石敬瑭.md "wikilink")
      - [永和](../Page/永和_\(王延鈞\).md "wikilink")（935年正月至936年二月）：閩—王延鈞之年號
      - [通文](../Page/通文.md "wikilink")（936年三月至939年七月）：閩—[王繼鵬之年號](../Page/王繼鵬.md "wikilink")
      - [大有](../Page/大有_\(年號\).md "wikilink")（928年三月至942年三月）：[南漢](../Page/南漢.md "wikilink")—[劉龑之年號](../Page/劉龑.md "wikilink")
      - [明德](../Page/明德_\(孟知祥\).md "wikilink")（934年四月至937年）：[後蜀](../Page/後蜀.md "wikilink")—[孟知祥之年號](../Page/孟知祥.md "wikilink")
      - [大明](../Page/大明_\(楊干真\).md "wikilink")（931年至937年）：[大義寧](../Page/大義寧.md "wikilink")—[楊干真之年號](../Page/楊干真.md "wikilink")
      - [天顯](../Page/天顯.md "wikilink")（926年二月至938年）：[契丹](../Page/遼朝.md "wikilink")—[耶律阿保機](../Page/耶律阿保機.md "wikilink")、[耶律德光之年號](../Page/耶律德光.md "wikilink")
      - [甘露](../Page/甘露_\(耶律倍\).md "wikilink")（926年至936年）：[東丹](../Page/東丹.md "wikilink")—[耶律倍之年號](../Page/耶律倍.md "wikilink")
      - [同慶](../Page/同慶_\(尉遲烏僧波\).md "wikilink")（912年至966年）：[于闐](../Page/于闐.md "wikilink")—[尉遲烏僧波之年號](../Page/尉遲烏僧波.md "wikilink")
      - [承平](../Page/承平_\(朱雀天皇\).md "wikilink")（931年四月二十六日至938年五月二十二日）：日本[朱雀天皇之年號](../Page/朱雀天皇.md "wikilink")

## 参考资料

1.  [徐红岚](../Page/徐红岚.md "wikilink")，《中日朝三国历史纪年表》，辽宁教育出版社，1998年5月 ISBN
    7538246193

[Category:南吴年号](../Category/南吴年号.md "wikilink")
[Category:930年代中国政治](../Category/930年代中国政治.md "wikilink")