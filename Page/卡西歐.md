**-{zh-hans:卡西欧计算机株式会社;zh-hant:卡西歐計算機株式會社;}-**（，，）是一家跨國的消費性電子產品和商用電子產品製造公司，總部設在東京澀谷。其產品包括[計算器](../Page/计算器.md "wikilink")，移動電話，[數碼相機](../Page/数码相机.md "wikilink")，電子音樂儀器和[數碼手錶](../Page/手表.md "wikilink")。該公司始建於1946年，並於1957年發布了世界上第一個全電子緊湊型計算器，同時亦是數碼相機的早期創新研發者之一。在20世紀的80和90年代，卡西歐開發出多款實惠的家用電子鍵盤。

2013年，卡西欧开通官方商城，针对[中国大陆用户展开销售](../Page/中国大陆.md "wikilink")。

## 歷史

該公司於1946年4月由[樫尾忠雄創立](../Page/樫尾忠雄.md "wikilink")，他是一名精通裝配的[工程師](../Page/工程師.md "wikilink")。公司的名字來自「樫尾」的日語讀音「Kashio」，首款產品是[香煙指環](../Page/香煙.md "wikilink")，使用者可以把香煙套在指環上，從而可在沒有煙灰缸時，不需用嘴或手指拿煙。當時剛結束[二戰](../Page/二戰.md "wikilink")，香煙對當時的日本來說是一種價值商品，因此該款香煙指環十分成功，為樫尾帶來第一桶金。

樫尾在1949年於東京[銀座舉行的商業展看到電動](../Page/銀座.md "wikilink")[計算機後](../Page/計算機.md "wikilink")，便與他的兄弟合作，用他從指環賺到的錢開發計算機。在當時，不少計算機是手動或以[馬達推動](../Page/馬達.md "wikilink")。樫尾有見於此，便以自己對電子學的知識，以[螺線管製造計算機](../Page/螺線管.md "wikilink")，並於1954年成功研製首台電子計算機，當時的售價高達48.5萬日圓。與同期的機械式計算機比較，他的電子產品只有10個單位數字鍵，而機械產品除了個位數外，還需要另設10、100等多位數字鍵。

1957年，卡西歐推出14-A型計算機，也是全球首款全電子計算機。公司於同年更名称**-{zh-hans:卡西欧计算机株式会社;zh-hant:卡西歐計算機株式會社;}-**，專門研發電子產品。

## 公司沿革

[CASIO_14-A.jpg](https://zh.wikipedia.org/wiki/File:CASIO_14-A.jpg "fig:CASIO_14-A.jpg")[国立科学博物馆](../Page/国立科学博物馆.md "wikilink")\]\]

  - 1946年4月：[樫尾忠雄在](../Page/樫尾忠雄.md "wikilink")[東京都](../Page/東京都.md "wikilink")[三鷹市成立](../Page/三鷹市.md "wikilink")**樫尾製作所**。
  - 1957年6月：開發出世界第一部沒有[齒輪的純電器式計算機](../Page/齒輪.md "wikilink")「14-A型」。\[1\]\[2\]\[3\]樫尾忠雄、俊雄、和雄、幸雄四兄弟共同創設**-{zh-hans:卡西欧计算机株式会社;zh-hant:卡西歐計算機株式會社;}-**（），第一任社長為四兄弟之父樫尾茂。
  - 1960年4月：在東京都[北多摩郡大和町](../Page/北多摩郡.md "wikilink")（現稱[東大和市](../Page/東大和市.md "wikilink")）成立東京工廠（現改成東京事務所Tokyo
    Product Control and Technical Center）。
  - 1965年9月：電子式桌上計算機「001型」問世。
  - 1966年6月：將總部遷移至東京都北多摩郡大和町（現稱東大和市）。
  - 1969年10月：在[山梨縣](../Page/山梨縣.md "wikilink")[中巨摩郡玉穂町建立甲府工廠](../Page/中巨摩郡.md "wikilink")。
  - 1972年8月：發表世界第一部個人計算機「」。
  - 1974年5月：總部搬遷到東京都[新宿區](../Page/新宿區.md "wikilink")。
  - 1974年11月：推出「CASIOTRON」（）電子手錶，這是世界第一隻不須手動調整大小月或[閏年的全自動電子錶](../Page/閏年.md "wikilink")。
  - 1978年1月：發售第一部名片大小的計算機「卡西歐迷你名片LC-78型」（），厚度僅3.9mm。
  - 1978年2月：在臺灣成立海外生產工廠「臺灣刻時豪股份有限公司」。
  - 1979年7月：成立八王子（Hachioji）工廠（現改成八王子研究所），且在東京都[西多摩郡羽村町](../Page/西多摩郡.md "wikilink")（現稱[羽村市](../Page/羽村市.md "wikilink")）成立羽村技術中心。
  - 1979年10月：在[山形縣](../Page/山形縣.md "wikilink")[東根市建立](../Page/東根市.md "wikilink")「山形卡西歐有限公司」。
  - 1980年1月：推出電子樂器「卡西歐Tone」（）。
  - 1980年2月：推出首支結合計算機功能的手錶Data Bank系列C-80-1型。
  - 1983年4月：推出首支具防震功能的手錶G-Shock DW-5000C-1A型。
  - 1989年2月：推出首支具有天氣預測感測器的數位手錶BM-100WJ-3型（PRO TERK系列尚未獨立之前身）。
  - 1998年1月：將總部遷至東京都[澁谷區初台](../Page/澁谷區.md "wikilink")。
  - 2000年：[行動電話事業部成立](../Page/移動電話.md "wikilink")，並發售 C303CA型手機。
  - 2004年4月：旗下行動電話事業部與[日立製作所合併](../Page/日立.md "wikilink")，成為。
  - 2010年6月：卡西歐日立移動通訊有限公司與[NEC旗下行動電話事業部整合](../Page/日本電氣.md "wikilink")。
  - 2018年4月：宣佈退出小型數位相機，終止小型數位相機的生產\[4\]\[5\]

## 產品

  - [電子計算器](../Page/電子計算器.md "wikilink")（[科学计算器](../Page/科学计算器.md "wikilink")、[图形计算器](../Page/图形计算器.md "wikilink")、标准计算器，仅列出主要产品）
      - [Casio fx-3650P](../Page/Casio_fx-3650P.md "wikilink")
      - [Casio fx-3950P](../Page/Casio_fx-3950P.md "wikilink")
      - [Casio fx-50FH](../Page/卡西歐fx-50FH.md "wikilink")
      - [Casio fx-82MS](../Page/Casio_fx-82MS.md "wikilink")
      - [Casio fx-82ES](../Page/卡西欧_fx-82ES.md "wikilink")
      - [Casio fx-991MS](../Page/卡西欧_fx-991MS.md "wikilink")
      - [Casio fx-991ES](../Page/Casio_fx-991ES.md "wikilink")
      - [Casio fx-CG10](../Page/Casio_fx-CG10.md "wikilink")
      - [Casio fx-CG20](../Page/Casio_fx-CG20.md "wikilink")
      - [Casio fx-82ES PLUS](../Page/Casio_fx-82ES_PLUS.md "wikilink")
      - [Casio fx-82ES PLUS
        A](../Page/Casio_fx-82ES_PLUS_A.md "wikilink")
      - [Casio fx-82CN X](../Page/Casio_fx-82CN_X.md "wikilink")
      - [Casio fx-95ES PLUS](../Page/Casio_fx-95ES_PLUS.md "wikilink")
      - [Casio fx-991ES PLUS](../Page/Casio_fx-991ES_PLUS.md "wikilink")
      - [Casio fx-991CN X](../Page/卡西欧_fx-991CN_X.md "wikilink")
  - [收銀機](../Page/收銀機.md "wikilink")
  - [手表](../Page/手表.md "wikilink")
      - [G-Shock](../Page/G-Shock.md "wikilink")
          - [MR-G](../Page/MR-G.md "wikilink")
          - [Master of G](../Page/Master_of_G.md "wikilink")
              - Frogman（蛙人，Shock Resist & ISO 200M Water Resist)
              - Riseman（飛人，Shock Resist & Twin Sensor)
              - Mudman（泥人，Shock Resist & Mud Resist)
              - Gulfman（灣人，Shock Resist & Rust Resist)
          - MT-G
          - GIEZ
          - Sky Cockpit
          - Original
          - Standard
      - Oceanus（ 亞洲地區 ）
          - Manta（Smart Access）
          - Manta
          - Smart Access
          - Classic Line
      - Casio Sport
          - Pro Trek 登山系列（亞洲/歐洲/其他地區）/ Pathfinder（美國/加拿大地區）
              - Prestige Special （Manaslu Special）（ 日本）
              - Analogue Digital Combination
              - Multi Field Line
              - Slim Line
              - Others
          - PHYS 跑步健身系列
          - Sea-Pathfinfer 航海系列
          - Others
      - Edifice

2015年 全新款 EQB-500

  -   - [Baby-G](../Page/Baby-G.md "wikilink")
      - Sheen（亞洲地區）
      - Regular
          - Combination
          - Data Bank
          - e-DataBank
          - Futurist
      - Wave
        Ceptor（[手表](../Page/手表.md "wikilink")/[時鐘](../Page/時鐘.md "wikilink")）

  - [數位相機](../Page/數位相機.md "wikilink")（[標準相機](../Page/標準相機.md "wikilink")、[自拍相機](../Page/自拍相機.md "wikilink")）

      - High Speed EXILIM
          - ZR1500
          - EX-100
          - EX-10
          - ZR1200
          - ZR800
          - ZR700
          - ZR1000
          - ZR200
          - ZR20
      - EXILIM
          -
      - EXILIM TR

该系列也在用户群中被称作“自拍神器”

  -   - EX-TR100
      - EX-TR150
      - EX-TR200
      - EX-TR300
      - EX-TR350/EX-TR350S
      - EX-TR500/ TR50
      - EX-TR550
      - EX-TR600

  -   - LIFE STYLE
          - EX-MR1
          - EX-FR10
          - EX-FR100

  - [筆記型電腦](../Page/筆記型電腦.md "wikilink")（[手提電腦](../Page/手提電腦.md "wikilink")、[筆記型電腦](../Page/筆記型電腦.md "wikilink")）

  - [電子辭典](../Page/電子辭典.md "wikilink")

  - [手提電話](../Page/手提電話.md "wikilink")（[行動電話](../Page/行動電話.md "wikilink")）

  - [音樂鍵盤](../Page/音樂鍵盤.md "wikilink")（[電子琴](../Page/電子琴.md "wikilink")）

  - [PDA](../Page/个人数码助理.md "wikilink")

  - [印表機](../Page/電腦打印機.md "wikilink")

  - [石英鐘](../Page/石英鐘.md "wikilink")

  - [Sub-notebook](../Page/Sub-notebook.md "wikilink")

  - [Wordtank](../Page/Wordtank.md "wikilink")

  - [投影機](../Page/投影機.md "wikilink")

  - [Casio
    Loopy](../Page/Casio_Loopy.md "wikilink")（[游戏机](../Page/游戏机.md "wikilink")）

## 图集

<File:CASIO> electronic dictionary
EV-SP3900.jpg|[电子词典EV](../Page/电子词典.md "wikilink")-SP3900
<File:QV-10.jpg>| [数码相机QV](../Page/数码相机.md "wikilink")-10 <File:Au>
W31CA CASIO gif-anime1.gif|Au W31CA[手机](../Page/手机.md "wikilink")
[File:Casio-fx115ES-5564.jpg|fx-115ES科学计算器](File:Casio-fx115ES-5564.jpg%7Cfx-115ES科学计算器)
<File:Casio> fx-991MS.jpg|fx-991MS科学计算器 <File:Casio>
fx-3650P-v.JPG|获[香港考试及评核局认可的fx](../Page/香港考试及评核局.md "wikilink")-3650P科学计算器
[File:Casio-FA11.jpg|PB-770便携式电脑](File:Casio-FA11.jpg%7CPB-770便携式电脑)
<File:Casio> Digital Diary SF-R20
open.JPG|SF-R20[PDA](../Page/PDA.md "wikilink") <File:Casio> F-91W
5051.jpg|F-91W数字手表 <File:Casio> vl tone.jpg| VL-1电子琴 <File:Casio>
az1.jpg|AZ-1

## 参考资料

## 外部連結

  - [日本卡西歐官方網站](http://casio.jp)

  - [臺灣卡西歐股份有限公司官方網站](http://www.casio.com.tw)

  - [中國卡西歐官方網站](http://www.casio.com.cn)

  - [卡西歐全球官方網站](http://world.casio.com/)

  -
[卡西歐](../Category/卡西歐.md "wikilink")
[Category:日本電子公司](../Category/日本電子公司.md "wikilink")
[Category:行動電話製造商](../Category/行動電話製造商.md "wikilink")
[Category:鐘錶製造商](../Category/鐘錶製造商.md "wikilink")
[Category:日本摄影器材生产商](../Category/日本摄影器材生产商.md "wikilink")
[Category:日本品牌](../Category/日本品牌.md "wikilink")
[Category:澀谷區公司](../Category/澀谷區公司.md "wikilink")
[Category:跨国公司](../Category/跨国公司.md "wikilink")
[Category:1946年成立的公司](../Category/1946年成立的公司.md "wikilink")

1.  [電卓の歴史（カシオ計算機のWebサイト）](http://casio.jp/dentaku/info/history/beginning/)
2.  [電卓博物館](http://www.dentaku-museum.com/calc/calc/2-casio/1-casiod/casiod.html)
3.  [主要产品一览](http://www.casio.com.cn/company/nenpyo.html)
4.
5.