[Vote-Box.jpg](https://zh.wikipedia.org/wiki/File:Vote-Box.jpg "fig:Vote-Box.jpg")

**直接選舉**（），簡稱**直選**，即[政府首腦或](../Page/政府首腦.md "wikilink")[立法機關的議員由民眾直接選舉產生](../Page/立法機關.md "wikilink")。選民可以直接以一人一票的方式，投票予他們喜歡的參選人。至於決定當選者則要依據不同的選舉方式。一般而言以得到較多數選票者勝或以兩局制先行淘汰部份參選者。相對概念為[間接選舉](../Page/間接選舉.md "wikilink")。現時直接選舉主要出現在[總統制國家](../Page/總統制.md "wikilink")，議會制國家多為間接選舉。

##

[美國最具代表性的](../Page/美國.md "wikilink")[美國總統是经由各州普選及](../Page/美國總統.md "wikilink")[美國選舉人團](../Page/美國選舉人團.md "wikilink")[間接選舉產生](../Page/間接選舉.md "wikilink")，但其他各级聯邦、州及地方行政首长皆由地方選舉產生，[美國國會的](../Page/美國國會.md "wikilink")[美國参议院全部议员](../Page/美國参议院.md "wikilink")、[美國众议院全部议员](../Page/美國众议院.md "wikilink")、州长、州议会、部分州政府官员、州長、市议会、縣長及縣议会均由直選產生。

##

[韓國總統](../Page/韓國總統.md "wikilink")、[韓國國會](../Page/韓國國會.md "wikilink")、17个市長及道知事、市道议会、区议会均由直選產生。

##

[中華民國第一位民選首都市長吳三連於1951年勝選後_First_People-elected_Mayor_of_Taipei,_the_Capital_of_TAIWAN.jpg](https://zh.wikipedia.org/wiki/File:中華民國第一位民選首都市長吳三連於1951年勝選後_First_People-elected_Mayor_of_Taipei,_the_Capital_of_TAIWAN.jpg "fig:中華民國第一位民選首都市長吳三連於1951年勝選後_First_People-elected_Mayor_of_Taipei,_the_Capital_of_TAIWAN.jpg")(左二)於1951年1月7日獲悉以65.5%高票當選[台北市第一屆民選市長後在辦事處與支持者舉杯同歡](../Page/台北市.md "wikilink")。\]\]

### 地方議會與首長直選

臺灣自1935年（[昭和十年](../Page/昭和.md "wikilink")）[日治時期即已舉辦州](../Page/臺灣日治時期.md "wikilink")、市、街、庄議員直接民選。

1950年，[中華民國](../Page/中華民國.md "wikilink")[台灣地區開始實施縣市地方自治](../Page/台灣地區.md "wikilink")，[臺灣省首度舉辦第](../Page/臺灣省.md "wikilink")1屆縣市議員選舉，1951年，則首度舉辦第1屆縣市長選舉。[吳三連與](../Page/吳三連.md "wikilink")[高玉樹等](../Page/高玉樹.md "wikilink")[台灣本省非](../Page/台灣人.md "wikilink")[中國國民黨人士亦可多次當選為具實權之](../Page/中國國民黨.md "wikilink")[首都](../Page/首都.md "wikilink")[臺北市市長](../Page/臺北市市長.md "wikilink")。

### 國會直選

[1947年中華民國國民大會代表選舉及](../Page/1947年中華民國國民大會代表選舉.md "wikilink")[1948年中華民國立法委员選舉是全](../Page/1948年中華民國立法委员選舉.md "wikilink")[中華民國](../Page/中華民國.md "wikilink")[大陸地區唯一一次的直接選举](../Page/大陸地區.md "wikilink")。

1969年，台灣地區首次舉辦增補[國會議員的](../Page/國會議員.md "wikilink")[國民大會代表與](../Page/國民大會代表.md "wikilink")[立法委員選舉](../Page/1969年中華民國立法委員選舉.md "wikilink")。

1986年，首个[反對黨](../Page/反對黨.md "wikilink")[民主進步黨首次參加第](../Page/民主進步黨.md "wikilink")3次增額國代選舉和[第5次增額立委選舉](../Page/1986年中華民國立法委員選舉.md "wikilink")。

[台灣民主化在](../Page/台灣民主化.md "wikilink")[中華民國政府決定於](../Page/中華民國政府.md "wikilink")1989年開放組黨結社、組織參加集會遊行、從事政治活動、自由辦報或出版刊物後有了更全面的進展。

中華民國政府凍結部分[憲法內容](../Page/中華民國憲法.md "wikilink")，而另訂《[中華民國憲法增修條文](../Page/中華民國憲法增修條文.md "wikilink")》後的國會全面改選，落實於1991年的[第2屆國民大會代表和](../Page/1991年中華民國國民大會代表選舉.md "wikilink")1992年的[第2屆立法委員選舉](../Page/1992年中華民國立法委員選舉.md "wikilink")，並首次舉行[1994年中華民國省市長暨省市議員選舉及](../Page/1994年中華民國省市長暨省市議員選舉.md "wikilink")[1996年中華民國總統選舉](../Page/1996年中華民國總統選舉.md "wikilink")。

### 總統直選與公民投票

[中華民國總統直接民選則於](../Page/中華民國總統.md "wikilink")1996年首次舉行，這次選舉不但讓[中華民國政府擁有更強的合法性](../Page/中華民國政府.md "wikilink")，也讓中華民國成為真正的民主國家，在政治層面落實[台灣本土化運動](../Page/台灣本土化運動.md "wikilink")。直接民選的總統與國會更是[中華民國在台灣](../Page/中華民國在台灣.md "wikilink")[主權獨立自主](../Page/主權.md "wikilink")、政治[自由](../Page/自由.md "wikilink")[民主](../Page/民主.md "wikilink")、社會開放進步、政府[依法行政](../Page/法治.md "wikilink")、全民監督政府施政與公僕表現的重要象徵與方式。此外，[中華民國於](../Page/中華民國.md "wikilink")2003年底成為東亞第一個立法保障[公民投票權的國家](../Page/公民投票.md "wikilink")，依《[公民投票法](../Page/公民投票法.md "wikilink")》進行的[公民投票亦屬廣義選舉與直接民權之一環](../Page/公民投票.md "wikilink")。

##

### 人大代表大会选举

根据中华人民共和国《中华人民共和国宪法》第九十七条规定，全国人民代表大会的代表，省、自治区、直辖市、设区的市、自治州的人民代表大会的代表，由下一级人民代表大会选举。不设区的市、市辖区、县、自治县、乡、民族乡、镇的人民代表大会的代表，由选民直接选举。

### 浙江省台州市“乡镇（街道）团委书记直选”

#### 基本介绍

2002年6月和11月，中国共青团[浙江省](../Page/浙江省.md "wikilink")[台州市委分别在椒江洪家街道和下陈街道进行了乡镇](../Page/台州市.md "wikilink")（街道）[团委的直选工作](../Page/团委.md "wikilink")。2003年，直选推广到全市。

报道声称“台州市乡镇（街道）团委书记直选是以乡镇（街道）为区块，以团支部海选为基础，以团员代表大会为导向，以竞选演说、区域内所有团员直接无记名差额投票选举为基本方式产生基层团委，并以实行团员代表常任制为监督制度的基层团干部选拔和使用机制。”\[1\]

#### 创新性

台州市团委声称，其创新性表现在：

  - 一是由所有团员直接投票选举产生团委成员，将民主选举权交给团员，扩大了团内民主；
  - 二是行政村团支部采取“海选”方式进行统一换届选举，乡镇一级团委干部的直选机制扩大了基层团干部的选拔范围，打破了乡镇（街道）团干部只在年轻公务员中挑选的身份限制；
  - 三是实行了团员代表常任制、团情恳谈、团情公示和民主评议制度，为评价和监督直选后基层团干部的工作提供了[制度保证](../Page/制度.md "wikilink")。

#### 作用

台州市委认为，乡镇团委书记直选增强了广大团员对团员身份的认同，调动了团员参与团活动的积极性，激发了团员的[民主意识](../Page/民主.md "wikilink")，培养了青年团员的民主热情。直选增强了团干部的责任意识和服务意识，提升了基层团干部的综合素质，提高了基层团组织的号召力和凝聚力。据称，此项措施实行后，可以增强团干部的群众基础，使一批优秀的不受身份限制的团员青年脱颖而出，逐渐形成服务型基层团委。

台州市委认为，乡镇团委班子直选带来的除了团内民主扩大、团组织凝聚力的增强，它对共青团以外的基层民主政治建设也具有示范、启发和推动作用。此外，直选的团委班子不受干部身份限制的探索、竞选演说和差额直选的做法以及对直选后团干部工作的监督制度的建立等等都为基层政权的直接选举有着重要的借鉴和启发意义。

据信，共青团中央组织部、青农部对台州乡镇团委书记直选的做法持肯定态度，团浙江省委已发文在整个浙江省推广台州的做法。中国大陸的几家报纸，如《中国青年报》、《中国青年报内参》、《浙江日报内参》及台州的各类媒体也对此作了报道。

### 四川省雅安市“直选县级党代表”

\[2\]

#### 基本介绍

2002年10—12月，雅安市委组织部在雨城区和荥经县进行直选县级党代表试点，并实行党代会常任制。所有党员都可以报名参选，符合资格的候选人要进行承诺演讲。党员秘密划票。选举结果当场公布，并向当选代表颁发《当选证书》。县、乡镇领导都必须以普通党员的身份参加选举。

#### 突出特点

与其他实行党代会常任制的地方相比，雅安市的做法有三个突出特点：

  - （1）党代表是通过直接[选举产生的](../Page/选举.md "wikilink")。所有党员都可以报名参选，符合资格的候选人要进行承诺演讲。党员秘密划票。选举结果当场公布，并向当选代表颁发《当选证书》。县、乡镇领导都必须以普通党员的身份参加选举。
  - （2）建立制度，保障党代表有效地履行职责和行使权利。党代表有专门的活动经费，并定期举行代表团活动。党代表可以向党代会提出议案，向全委会和常委会提出意见和建议。党代表要对县委委员以及纪委委员的工作进行测评。党代表每年要向选举单位述职。党代表可以列席县委常委会和全委会。
  - （3）探索新方法加强党内民主，提高党内民主的社会影响力。雨城区成立了监督委员会和决策咨询委员会。监督委员会的功能是监督区委委员会和区纪委委员。决策咨询委员会目的是保障党委决策科学化和民主化。实行党代表席位制，以避免党代表调离该选区出现的代表空缺。荥经县邀请政协委员会旁听党代会，加强[中共中央与](../Page/中共中央.md "wikilink")[民主党派和](../Page/民主党派.md "wikilink")[无党派人士的联系](../Page/无党派人士.md "wikilink")。

#### 作用

[四川省](../Page/四川省.md "wikilink")[雅安市委认为](../Page/雅安市.md "wikilink")，“直选县级党代表”可以提高党内凝聚力和认同感，无论是普通党员还是党代表都能通过有效的渠道来参加党内生活。也能加强党员之间，普通党员与领导干部之间的交流。党内权力的合理配置也开始得到实现，提高党内决策的透明度。通过实行党代会常任制，把决策权归还给党代会和全委会，重大事项实行票决，常委会的权力受到了制约，从而有效遏制了党内的“一言堂”现象。还可为人民民主的落实提供了有力的制度支持。雅安在推行党代会常任制的同时，也实行了乡镇、科局领导干部的[公推公选](../Page/公推公选.md "wikilink")。雅安市委认为，经历过党内直接选举的干部更能够接受人民选举的检验。

据信，2003年底以来[北京市](../Page/北京市.md "wikilink")、[天津市](../Page/天津市.md "wikilink")、[吉林省等十多个省市区县党委](../Page/吉林省.md "wikilink")300多人前往雅安市考察，另外，中组部、中纪委等中央高层有关部门领导也前来调研指导。

### 河北省迁西县妇代会直接选举

\[3\]

#### 介绍

[河北省](../Page/河北省.md "wikilink")[迁西县妇代会直接选举是从](../Page/迁西县.md "wikilink")1999年12月份开始的。1999年，在第五届村委会换届之前，在114个村进行妇代会换届直接选举试点，把妇代会干部的委任制变成直接选举制。直接选举制就是不设候选人，自由竞选，由具备选举资格的妇女采取无记名投票的办法直接选举产生。当场宣布选举结果。乡妇联主席向新当选的妇代会成员颁发《当选证书》。

#### 该措施发起的动因

妇代会直接选举制度的产生是为了解决农村妇女参政存在的问题。

  - 迁西农村妇女政治参与热情不高。由于受“男主外，女主内”传统思想影响，有相当部分妇女满足于在家“锅台转”，钟情于相夫教子，不关心村务。
  - 妇女政治参与度低。1996年第四届村委会选举时，村委会成员中妇女只占12％。
  - 妇代会干部终身制问题严重。在1995年前，50岁以上的妇代会主任占30％，年龄老化，而且多数素质低，不能适应新时期的要求。
  - 委任制产生的妇女干部往往只对上负责，不对下负责，缺乏群众基础，没有什么威望。

#### 作用

  - 促进了广大妇女的政治参与热情，提高了她们的民主意识。农村妇女参选率，由以往的70％提高到96％。
  - 提高了妇女干部队伍的素质。选举时，参与竞选者非常踊跃，一般一个村有3－5人上台作竞选讲演，尹庄乡船庄村上台竞选者多达13人。通过竞选，妇代会出现了生机和活力。
  - 解决了妇女干部只对上级组织负责，不对基层妇女群众负责的问题，实现了对上和对下负责的一致性，提高了妇女干部的服务意识。
  - 促使妇女政治参与层次提高。过去，妇女主任很少能进入村委会。现在，直选产生的妇女主任由于群众基础好，大多在村委会选举时又当选，有的还当了村主任，实现了女性村主任职务零的突破，并有一部分妇女当选为村副主任。

据信，迁西县妇代会直选试点工作取得了成功，引起了中国中央电视台、中国妇女报、乡镇论坛、河北日报等[媒体的关注](../Page/媒体.md "wikilink")，多次作了报道。

### 廣東省汕尾市陸豐市東海街道烏坎村

[廣東省](../Page/廣東省.md "wikilink")[汕尾市](../Page/汕尾市.md "wikilink")[陸豐市東海街道](../Page/陸豐市.md "wikilink")[烏坎村在發生風波後實行直接選舉](../Page/烏坎村.md "wikilink")。烏坎事件被視為是中國大陸群體事件的標誌性里程碑之一。

##

[香港立法會議席分為](../Page/香港立法會.md "wikilink")[地區直選及](../Page/地區直選.md "wikilink")[功能界別](../Page/功能界別.md "wikilink")，地區直選的35席實行直接選舉，功能界別的35席分為29個功能界別，其中[勞工界功能界別設](../Page/勞工界功能界別.md "wikilink")3席（由工會團體以團體票全票制選出，不屬於直接選舉），[區議會（第二）功能界別設](../Page/區議會（第二）功能界別.md "wikilink")5席（由不屬於28個功能界別的全港地區直選選民選出，屬於變相的直接選舉），各功能界別的選民比例不同，功能界別的選舉方式包括個人票及團體票，團體票產生的功能界別議席不屬於直接選舉，個人票產生的功能界別議席屬於直接選舉，如[會計界功能界別](../Page/會計界功能界別.md "wikilink")、[醫學界功能界別](../Page/醫學界功能界別.md "wikilink")、[教育界功能界別](../Page/教育界功能界別.md "wikilink")、[法律界功能界別等](../Page/法律界功能界別.md "wikilink")。

[香港區議會議席除](../Page/香港區議會.md "wikilink")27個新界鄉事委員會主席兼任的當然議員外，其餘均為直接選舉，由區議會[單一選區選民選出](../Page/單一選區.md "wikilink")。

##

[澳門立法會議席的](../Page/澳門立法會.md "wikilink")14席實行直接選舉，12席實行間接選舉，7席由[澳門行政長官委任](../Page/澳門行政長官.md "wikilink")。

## 参考文献

` 4.`[`《中华人民共和国宪法》,中国人大网`](http://www.npc.gov.cn/npc/xinwen/2018-03/22/content_2052621.htm)

## 参见

  - [自由民主制](../Page/自由民主制.md "wikilink")
  - [代議民主制](../Page/代議民主制.md "wikilink")
  - [公民投票](../Page/公民投票.md "wikilink")
  - [普選](../Page/普選.md "wikilink")
  - [間接選舉](../Page/間接選舉.md "wikilink")
  - [人權](../Page/人權.md "wikilink")
  - [世界人權宣言](../Page/世界人權宣言.md "wikilink")
  - [公民權利和政治權利國際公約](../Page/公民權利和政治權利國際公約.md "wikilink")

[Category:選舉制度](../Category/選舉制度.md "wikilink")

1.  [浙江省台州市“乡镇（街道）团委书记直选”](http://www.chinainnovations.org/Item/24064.aspx)，中華人民共和國政府创新网。
2.  [四川省雅安市“直选县级党代表”](http://www.chinainnovations.org/Item/24098.aspx)，中華人民共和國政府创新网。
3.  河北省迁西县妇代会直接选举，中国政府创新网。