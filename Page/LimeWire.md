**LimeWire**是一個使用[Gnutella網絡來尋找與傳送檔案的](../Page/Gnutella.md "wikilink")[Java平台](../Page/Java平台.md "wikilink")[點對點](../Page/點對點.md "wikilink")[檔案分享客戶端](../Page/檔案分享.md "wikilink")。Limewire是一個以[GNU通用公共許可證發佈的](../Page/GNU通用公共許可證.md "wikilink")[自由軟體](../Page/自由軟體.md "wikilink")，另外亦鼓勵用戶繳費使用LimeWire
Pro。2010年10月26日LimeWire输掉了与美国唱片业协会的版权官司，被法院命令永久停止运作。\[1\]在LimeWire网站被关闭前，在所有使用P2P网络的网民中，56%使用LimeWire。2010年第四季度LimeWire网站被迫关闭后，许多用户转向[Frostwire](../Page/FrostWire.md "wikilink")。去年第四季度，Frostwire的使用比例从第三季度的10%增至21%，Bittorrent从第三季度的8%增至12%。

## 特點

LimeWire以[Java程式設計語言編寫](../Page/Java.md "wikilink")，可在任何裝有[Java虛擬機的電腦上運行](../Page/Java虛擬機.md "wikilink")，現提供了適用於[Microsoft
Windows](../Page/Microsoft_Windows.md "wikilink")、[Mac OS
X與基於](../Page/Mac_OS_X.md "wikilink")[RPM的](../Page/RPM套件管理員.md "wikilink")[Linux發行版的安裝程式](../Page/Linux套件列表.md "wikilink")。對[Mac
OS 9及其他早期版本的支援隨著LimeWire](../Page/Mac_OS_9.md "wikilink") 4.0.10的發佈而喪失。

LimeWire使用[SHA-1與](../Page/SHA家族.md "wikilink")[Tiger Tree
Hash暗號化](../Page/雜湊樹.md "wikilink")[散列函数來確保已下載資料的完整性](../Page/散列函数.md "wikilink")。儘管硏究員已鑑定SHA-1算法的潛在弱點\[2\]，但由於LimeWire並非只依賴於SHA-1，該些弱點不會對LimeWire驗証已下載檔案有太大的影響。

LimeWire提供透過[Digital Audio Access
Protocol分享其資料庫](../Page/Digital_Audio_Access_Protocol.md "wikilink")，因此當LimeWire運行時任何己分享的檔案將可被區域網絡中使用DAAP的裝置（如[iTunes](../Page/iTunes.md "wikilink")）檢測到。

## 版本

LimeWire的開發商Lime Wire
LLC為該程式分發了兩個版本，一個免費的基本版與一個費用為18.88[美元](../Page/美元.md "wikilink")（9.78歐元）的增強版，開發商聲稱增強版會提供更快的下載速度。這是通過在任何時間推動相同搜查檔案直接連接4個主機來達成，而免費版限制了最多連接2個主機\[3\]。2004年4月前免費版的LimeWire同梱了一個已被電腦安全專家認定為[間諜軟體](../Page/間諜軟體.md "wikilink")，名為**LimeShop**（一個[TopMoxie的變種](../Page/TopMoxie.md "wikilink")）的程式分發。除此之外LimeShop會監控線上採購以將銷售佣金更改到Lime
Wire LLC，而反安裝LimeWire將不會移除LimeShop。2004年4月20日發佈的LimeWire
3.9.4移除了所有同梱軟體，解決了該些缺點\[4\]。

由於LimeWire是一個[自由與](../Page/自由軟體.md "wikilink")[開放原始碼軟體](../Page/開放原始碼.md "wikilink")，因而產生了多個[衍生版本](../Page/衍生版本.md "wikilink")，包括了[LionShare](../Page/LionShare.md "wikilink")，一個[賓夕法尼亞州立大學的實驗軟體開發計劃](../Page/賓夕法尼亞州立大學.md "wikilink")，以及一個擁有專利界面，基於[Mac
OS
X的Gnutella客戶端](../Page/Mac_OS_X.md "wikilink")[Acquisition](../Page/Acquisition.md "wikilink")。[康乃爾大學的硏究員開發了一個名為](../Page/康乃爾大學.md "wikilink")[Credence的名聲管理插件](../Page/Credence.md "wikilink")，讓用戶可在下載前區別"真正（genuine）"與"可疑（suspect）"的檔案。2005年10月12日有報道指出一些LimeWire開放源始碼的參與者衍生了該計劃並將其名為[FrostWire](../Page/FrostWire.md "wikilink")\[5\]。

LimeWire是首個支援防火牆到防火牆傳輸的檔案分享程式，這個功能於2004年11月發佈的4.2版加入。

現時的LimeWire測試版結合了一個重新編寫的Limewire[元數據處理與加入對](../Page/元數據.md "wikilink")[BitTorrent的支援](../Page/BitTorrent.md "wikilink")。

## 爭辯與法律問題

根據2005年6月28日紐約時報報道Lime Wire
LLC因*[米高梅訴格羅斯特案](../Page/米高梅訴格羅斯特案.md "wikilink")*的結果而考慮停止分發LimeWire\[6\]。在2005年9月25日有報道指出Lime
Wire
LLC正製作一個拒絕分享缺乏有效授權資訊檔案版本的程式\[7\]。但兩個改變都未有發生，截至2007年5月22日仍可下載與分享擁有版權的檔案。

在2006年8月4日[美國唱片業協會控告LimeWire](../Page/美國唱片業協會.md "wikilink")，聲稱其自未經授權的下載中獲益\[8\]。2006年9月25日LimeWire以違反[反托拉斯法反控訴RIAA](../Page/反托拉斯法.md "wikilink")\[9\]。

2006年5月12日BBC報道"Limewire"與"Lime
wire"是很可能會自互聯網[搜尋引擎回傳連結到](../Page/搜尋引擎.md "wikilink")[惡意軟體的搜尋字眼之一](../Page/惡意軟體.md "wikilink")\[10\]。

[CA
Anti-Spyware](../Page/CA_Anti-Spyware.md "wikilink")（原為PestPatrol）將LimeWire標記為間諜軟體，如將LimeWire與[Kazaa同時安裝在電腦上亦會將Kazaa偵測為間諜軟體](../Page/Kazaa.md "wikilink")。

2010年10月26日LimeWire输掉了与美国唱片业协会的版权官司，被法院命令永久停止运作。\[11\]

## 商業模式

Lime Wire LLC透過銷售Limewire Pro來獲得收益，一個6個月的Limewire
Pro許可証要價$18.88，許多用戶普遍誤以為需要授權才可以通過LimeWire存取[Gnutella網絡](../Page/Gnutella.md "wikilink")，實際上那僅是該軟體的許可証。

Pro版與Basic版有兩方面的分別：

  - 提供技術支援
  - 應該會提供更多搜尋結果與更快的下載速度

## 参考资料

  -
  -
## 外部連結

  - [LimeWire官方網站](http://www.limewire.com)

<!-- end list -->

  - [LimeWire linked to Identity
    Theft](https://web.archive.org/web/20070702193214/http://www.consumeraffairs.com/news04/2006/10/limewire.html)
  - [LimeWire Bears Brunt of Identity Theft
    Concerns](http://www.slyck.com/story1328.html)

[Category:Java平台軟體](../Category/Java平台軟體.md "wikilink")
[Category:自由軟體](../Category/自由軟體.md "wikilink")
[Category:P2P](../Category/P2P.md "wikilink")
[Category:自由檔案分享軟體](../Category/自由檔案分享軟體.md "wikilink")
[Category:自由跨平台軟體](../Category/自由跨平台軟體.md "wikilink")

1.

2.  [Schneier on Security: SHA-1
    Broken](http://www.schneier.com/blog/archives/2005/02/sha1_broken.html)

3.

4.

5.  ["FrostWire Beta
    Released"](http://www.slyck.com/news.php?story=954)，來自[Slyck.com](../Page/Slyck.com.md "wikilink")

6.  Tom Zeller，2005年6月28日，"Trying to Tame an Unruly
    Technology"，*紐約時報*，C版，專欄2，第一頁

7.  <http://www.slyck.com/news.php?story=927>

8.  <http://recordingindustryvspeople.blogspot.com/2006/08/copy-of-complaint-in-arista-v-lime.html>

9.  <http://recordingindustryvspeople.blogspot.com/2006/09/lime-wire-sues-riaa-for-antitrust.html>
    and
    <http://recordingindustryvspeople.blogspot.com/2006/11/limewire-files-amended-counterclaims.html>

10. [BBC News: Warning on search engine
    safety](http://news.bbc.co.uk/1/hi/technology/4765199.stm)

11.