**林泠**，原名**胡雲裳**，[广东](../Page/广东.md "wikilink")[开平人](../Page/开平.md "wikilink")，出生於[四川](../Page/四川.md "wikilink")。就讀[北一女中](../Page/北一女中.md "wikilink")，1958年[台湾大学化学系毕业](../Page/台湾大学.md "wikilink")，后获得美国[弗吉尼亚大学化學博士学位](../Page/弗吉尼亚大学.md "wikilink")，現定居美國，台灣詩人。

[林泠在黃石公園.jpg](https://zh.wikipedia.org/wiki/File:林泠在黃石公園.jpg "fig:林泠在黃石公園.jpg")

1952年發表第一件作品〈流浪人〉，創作時間甚早，與[白萩被稱為](../Page/白萩.md "wikilink")「天才詩人」。1958年畢業後赴美，創作量大減。完成學業後結婚生子，投入工業研發。1982年集結早年作品由洪範書店出版《林泠詩集》，直到2003年才出版第二部詩集《在植物與幽靈之間》。林泠表示寫作斷層的原因是因為「忙碌」，忙碌也是唯一的原因。\[1\]

林泠具有化學專業，歷任美國化學界資深研發主管，並受聘美國衛生總署為全美醫藥研究方案評審。雖作品量不多，但她是在台湾诗坛上重要的女诗人，其创作手法独特，体裁秀丽自然，诗风婉约优美。其詩作〈不繫之舟〉、〈阡陌〉曾入選台灣中學的中文教科書。

## 主要作品

  - 《林泠诗集》
  - 《在植物與幽靈之間》

## 外部链接

  - [林泠：关于林泠](http://dcc.ndhu.edu.tw/trans/poemroad/lin-leng/archives/2005/12/post.html)
  - [林泠](https://web.archive.org/web/20070416130440/http://staff.whsh.tc.edu.tw/~huanyin/mofa/p/mofa_poet2.php)

## 参见

  - [现代诗歌](../Page/现代诗歌.md "wikilink")
  - [现代诗人](../Page/现代诗人.md "wikilink")
  - 诗人列表

[L](../Page/category:台灣詩人.md "wikilink")

[Category:維吉尼亞大學校友](../Category/維吉尼亞大學校友.md "wikilink")
[L](../Category/國立臺灣大學理學院校友.md "wikilink")
[L](../Category/臺北市立第一女子高級中學校友.md "wikilink")
[Category:胡姓](../Category/胡姓.md "wikilink")

1.   Fanily
    粉絲玩樂|accessdate=2018-08-10|work=www.fanily.tw|language=zh-TW}}