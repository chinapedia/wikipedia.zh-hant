**HP-UX**（取自**H**ewlett **P**ackard **U**ni**X**）是惠普科技公司（HP,
[Hewlett-Packard](../Page/Hewlett-Packard.md "wikilink")）以[System
V為基礎所研發成的類](../Page/UNIX_System_V.md "wikilink")[UNIX](../Page/UNIX.md "wikilink")[作業系統](../Page/作業系統.md "wikilink")。HP-UX可以在HP的[PA-RISC處理器](../Page/PA-RISC.md "wikilink")、[Intel的](../Page/Intel.md "wikilink")[Itanium處理器的電腦上執行](../Page/Itanium.md "wikilink")，另外過去也能用於後期的[阿波羅電腦](../Page/阿波羅電腦.md "wikilink")（[Apollo/Domain](../Page/Apollo/Domain.md "wikilink")）系統上。較早版本的HP-UX也能用於[HP
9000系列](../Page/HP_9000.md "wikilink")200型、300型、400型的電腦系統（使用[Motorola的](../Page/Motorola.md "wikilink")[68000處理器](../Page/68000.md "wikilink")）上，和[HP-9000系列](../Page/HP-9000系列.md "wikilink")500型電腦（使用HP專屬的[FOCUS處理器架構](../Page/FOCUS_\(硬體\).md "wikilink")）。

## 外部連結

  - [Hewlett-Packard HP-UX](http://www.hp.com/go/hpux/)
  - [The HP-UX Porting and Archive
    Center](http://hpux.cs.utah.edu/)：Porting [Open Source
    Software](../Page/Open_Source_Software.md "wikilink") to HP-UX
  - [EnterpriseUNIX.org](http://www.enterpriseunix.org) HP-UX News and
    Information Portal
  - [comp.sys.hp.hpux from Google
    Groups](http://groups.google.com/groups?hl=en&lr=&ie=UTF-8&oe=UTF-8&group=comp.sys.hp.hpux)
  - [HP-UX FAQ](http://www.faqs.org/faqs/hp/hpux-faq/)
  - [Securing
    HP-UX](https://web.archive.org/web/20050311042440/http://www.blacksheepnetworks.com/security/resources/sec_HPUX2.html)
  - [HP and Veritas to Accelerate HP-UX 11i
    Virtualization](http://www.hp.com/hpinfo/newsroom/press/2004/041202a.html)

[Category:System V](../Category/System_V.md "wikilink")
[Category:惠普產品](../Category/惠普產品.md "wikilink")