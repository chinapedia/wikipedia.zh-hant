**中华**，古时亦称**中夏**，意思**[华夏](../Page/华夏.md "wikilink")**相近。“中华”一词最早见于典籍是东晋十六国时期。晋，桓温《请还都洛阳疏》：“自强胡陵暴，中华荡覆，狼狈失据。”

## 辭源

古代[漢族王朝最初多建都於](../Page/漢族.md "wikilink")[河南省及其附近區域](../Page/河南省.md "wikilink")，以其位居[漢地之中部](../Page/漢地.md "wikilink")，文化美盛，故稱其地為「中華」。後各朝疆土漸廣，凡所轄地，皆稱為「中華」。
《[晉書](../Page/晉書.md "wikilink")》 桓溫請還都洛陽疏：「彊胡陵暴，**中華**蕩覆，狼狽失據。」。《资治通鉴
·卷一百·晋纪二十二》：「（荀）羡谓（賈）坚曰：『君父、祖世为晋臣，奈何背本不降？』坚曰：『晋自弃**中华**，非吾叛也。民既无主，强则托命。既已事人，安可改节！吾束修自立，涉赵历燕，未尝易志，君何匆匆相谓降乎！』羡复责之，坚怒曰：『竖子，儿女御乃公！』羡怒，执置雨中，数日，坚愤惋而卒。」《《北齊書》》˙卷二十一˙高乾傳：「于時，鮮卑共輕**中華**朝士，唯憚服於昂。」\[1\]

五胡乱华时期，汉民族的民族意识空前高涨，当时的“中华”主要代指已经沦陷的以[河洛为中心的](../Page/河洛.md "wikilink")[中原地区](../Page/中原.md "wikilink")。

近代以前，“中华”一词与“汉（人）”的意思接近。中华民國建立後，“中华”和“中华民族”渐渐发展为指代整个中国的所有[民族](../Page/民族.md "wikilink")（[族群](../Page/族群.md "wikilink")）。

## 用法

### “大中华”的用法

指包括以華人為主的[中國大陸](../Page/中國大陸.md "wikilink")、[香港](../Page/香港.md "wikilink")、[澳門](../Page/澳門.md "wikilink")、[臺灣](../Page/臺灣.md "wikilink")，[新加坡以及](../Page/新加坡.md "wikilink")[華人較多的](../Page/華人.md "wikilink")[马来西亚的地區](../Page/马来西亚.md "wikilink")\[2\]。许多商业机构和跨国企业都采用“[大中华地区](../Page/大中华地区.md "wikilink")”或“[大中华区](../Page/大中华区.md "wikilink")”一词于自身的商务运作当中，而以此为名所设立的公司业务部门则主要负责处理臺灣及中国大陆、香港、澳门等两岸三地之间业务来往的管理事项，偶尔也兼有负责新加坡和马来西亚之间的“新马”（旧称“星马”）业务。

国际上基本认为[中华圈是最初以漢文化為主的古中國以及受古中國冊封](../Page/中华圈.md "wikilink")、奉中華文化為上的眾多朝貢國，而今為華人以及中華文化為主的[中國大陸](../Page/中國大陸.md "wikilink")、[香港](../Page/香港.md "wikilink")、[澳門](../Page/澳門.md "wikilink")、[臺灣地區](../Page/臺灣.md "wikilink")\[3\]。

### 在日本的用法

在日本，常以“中华”表示源自中国，泛指[中華人民共和國的](../Page/中華人民共和國.md "wikilink")[大陸](../Page/大陸.md "wikilink")、[香港](../Page/香港.md "wikilink")、[澳門及](../Page/澳門.md "wikilink")[中華民國的](../Page/中華民國.md "wikilink")[臺灣](../Page/臺灣.md "wikilink")，例如**中华料理**、**中华民族**。

## 在其他地区的对应词和词源

西方人在探索和认识东方的过程中，对“China”一词的定义一直很模糊，这种状况一直持续到明朝中末期，期间西方基督教传教士（如[圣方济·沙勿略](../Page/圣方济·沙勿略.md "wikilink")、[罗明坚](../Page/罗明坚.md "wikilink")、[利玛窦](../Page/利玛窦.md "wikilink")、钟巴相、[郭居静等等](../Page/郭居静.md "wikilink")）来访，这种传教举动在[中国历史上还是头一次](../Page/中国历史.md "wikilink")，大大加深了西方人对中华的认知。世界通用的“**China**”有来自于秦（Ts'in、Ch'in或Qin）、瓷都[景德镇的古称](../Page/景德镇.md "wikilink")“[昌南](../Page/昌南.md "wikilink")”、[陕西话的](../Page/陕西话.md "wikilink")“长安”、[契丹](../Page/契丹.md "wikilink")(Khitan、Catai或Cathay)等等的说法。

由于[西辽](../Page/西辽.md "wikilink")（即被[金国灭亡后西迁的](../Page/金国.md "wikilink")[契丹](../Page/契丹.md "wikilink")）与西方以及中亚和西亚交流较深，故當今有部分国家称呼中国为“[契丹](../Page/契丹.md "wikilink")”或其演变词。在突厥语系语言中，中国被称为“Kaitay”、“Kathay”、“Hatay”、“Katay”；在斯拉夫语系语言中，中国被称为“Китай”（[俄语](../Page/俄语.md "wikilink")），“Китай”（[乌克兰语](../Page/乌克兰语.md "wikilink")）；在西欧和北欧诸语言中，中国被雅称为“Cathay”（[法语](../Page/法语.md "wikilink")、[英语](../Page/英语.md "wikilink")、[德语](../Page/德语.md "wikilink")、[荷兰语](../Page/荷兰语.md "wikilink")、[北日耳曼语支](../Page/北日耳曼语支.md "wikilink")）。这些称呼皆源自“**[契丹](../Page/契丹_\(中国称号\).md "wikilink")**”一词。

## 参见

  - [华夏](../Page/华夏.md "wikilink")
  - [大中华](../Page/大中华.md "wikilink")
  - [中华文明](../Page/中华文明.md "wikilink")
  - [中国古代文化](../Page/中国古代文化.md "wikilink")
  - [中国行政区划](../Page/中国行政区划.md "wikilink")
  - [中国历史](../Page/中国历史.md "wikilink")
  - [中国地理](../Page/中国地理.md "wikilink")
  - [中国文化](../Page/中国文化.md "wikilink")

## 參考文獻

<div class="references-small">

<references>

</references>

</div>

[Category:中國的稱號](../Category/中國的稱號.md "wikilink")

1.
2.  p.2-3,
3.  p.2-3,