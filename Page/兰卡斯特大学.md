**蘭卡斯特大学**（Lancaster University，正式名稱 University of
Lancaster），或譯為**蘭開斯特大學**，是一所坐落在[英国](../Page/英国.md "wikilink")[蘭卡斯特市郊的知名学院制大学](../Page/蘭卡斯特市.md "wikilink")。

兰卡斯特大学还与[杜倫大学](../Page/杜倫大学.md "wikilink")、[利兹大学](../Page/利兹大学.md "wikilink")、[利物浦大学](../Page/利物浦大学.md "wikilink")、[曼彻斯特大学](../Page/曼彻斯特大学.md "wikilink")、[纽卡斯尔大学](../Page/纽卡斯尔大学.md "wikilink")、[谢菲尔德大学和](../Page/谢菲尔德大学.md "wikilink")[约克大学一同成立N](../Page/约克大学.md "wikilink")8研究联盟。兰卡斯特大学采用[非獨立書院体系](../Page/書院聯邦制.md "wikilink")，由四个学院和9个非獨立書院组成，学院主要进行科研以及教学，而非獨立書院主要是安排校院内的本科生、研究生、博士后研究人员和一些大学的工作人员等的生活，住宿和福利。

## 历史

[二战之后](../Page/二战.md "wikilink")，随着人口的扩张和新技术涌现的挑战，高等教育的发展成为英国政府的重要关注点。兰卡斯特大学就是在这种背景下，于1964年获皇家特许状并成立。在1964年10月接收了它的第一批学生。大学最初有13名教授，
32名教学和科研工作人员， 8名[图书馆工作人员和](../Page/图书馆.md "wikilink")14名学术管理员。

兰卡斯特大学当时的校训为："patet omnibus
veritas"，意为“真理面向所有人”。1965年，大学迎来了首位理学学士学生。兰卡斯特大学是英国[平板玻璃大学中的一所](../Page/平板玻璃大学.md "wikilink")，现校址位于兰卡斯特市以南3英里的[Bailrigg](../Page/Bailrigg.md "wikilink"),
[Ellel和](../Page/Ellel.md "wikilink")[Galgate三个村庄](../Page/Galgate.md "wikilink")
[附近](http://www.multimap.com/map/browse.cgi?pc=LA14YW&scale=25000)。

该校的校徽就是按照学校的标志性建筑[University Chaplaincy
Centre的尖顶形状而设计的](../Page/Lancaster_University_Chaplaincy_Centre.md "wikilink")。[Lancaster_University_chaplaincy_centre_spire_and_logo.jpg](https://zh.wikipedia.org/wiki/File:Lancaster_University_chaplaincy_centre_spire_and_logo.jpg "fig:Lancaster_University_chaplaincy_centre_spire_and_logo.jpg")

## 学术

蘭卡斯特大學於2016-2017在英國境內排名第9名，也是西北部排名第一的學校。

### 學院

大学最初有两个書院, Bowland和Lonsdale。

之后大学迅速扩展，到有现在已经有八个大學生學院和一個研究生學院, 这些書院都是根据学校附近的地名来命名的:

  - [Bowland](../Page/Bowland_College.md "wikilink")，根据附近的Bowland猎场命名。
  - [County](../Page/The_County_College.md "wikilink")，根据大学所在的[兰开夏郡议会命名](../Page/兰开夏郡.md "wikilink")。
  - [Cartmel](../Page/Cartmel_College.md "wikilink")，根据[坎布里亚郡的](../Page/坎布里亚郡.md "wikilink")[Cartmel丘地命名](../Page/Cartmel.md "wikilink")。
  - [Fylde](../Page/Fylde_College.md "wikilink")，根据[Fylde半島命名](../Page/Fylde.md "wikilink")。
  - [Furness](../Page/Furness_College.md "wikilink")，根据历史上[兰开夏郡的](../Page/兰开夏郡.md "wikilink")[Furness地区命名](../Page/Furness.md "wikilink")(现在属于[坎布里亚郡](../Page/坎布里亚郡.md "wikilink"))命名。
  - [Lonsdale](../Page/Lonsdale_College.md "wikilink")，根据[Kirkby
    Lonsdale命名](../Page/Kirkby_Lonsdale.md "wikilink")。
  - [Grizedale
    College](http://grizedale.lusu.co.uk/)，根据[坎布里亚郡的](../Page/坎布里亚郡.md "wikilink")[Grizedale森林命名](../Page/Grizedale.md "wikilink")。
  - [Pendle
    College](http://pendle.lusu.co.uk/)，根据[兰卡斯特的](../Page/兰卡斯特.md "wikilink")[Pendle地区和著名的](../Page/Pendle.md "wikilink")[Pendle巫婆审讯命名](../Page/Pendle巫婆审讯.md "wikilink")。

此外，学校还有一个研究生學院:

  - [Graduate
    College](http://www.lancaster.ac.uk/colleges/graduate/)，于1992年建立。

这些學院都是基于[牛津剑桥的学院制度不同](../Page/牛桥.md "wikilink")，因此每个學院有较弱的独立性；不提供教學僅提供生活，住宿和福利，類似於[香港中文大學](../Page/香港中文大學.md "wikilink")。每个學院有自己的酒吧和学生活动室。

### 系所

兰卡斯特设有三个学院负责提供集中讲授的课程教育及研究：

  - 艺术和社会科学学院：由法学、应用社会科学、教育研究、英文和创作、欧洲语言和文化、历史、语言学、哲学、政治学、国际关系、宗教研究、社会学、文化研究所、卫生研究、当代艺术（美术，设计，音乐及戏剧研究）和拉斯金中心等部门组成。
  - 科学技术学院：由生物科学、通讯系统、计算、工程学、环境科学、地理学、数学和统计、自然科学、物理、卫生与医药学院等组成。
  - 管理学院：是一个独立的单一学院（[兰卡斯特大学管理学院](../Page/兰卡斯特大学管理学院.md "wikilink")），由会计及财务、经济学、管理和领导学、管理科学、市场营销和组织、工作和技术研究所的企业精神与企业发展与中心的电子科学、卓越领导、研究的技术与组织、国际研究中心、会计、兰卡斯特中心预报、兰卡斯特战略管理中心、兰卡斯特中国管理中心、兰卡斯特领导力研究中心、绩效导向人力资源中心等部门组成。
  - 健康醫藥学院

## 校园

蘭卡斯特大學校園佔地在Bailrigg，大學總面積為360英畝，自蘭卡斯特市中心僅三英里。蘭卡斯特的學生享受一個巨大的綠色校園，在校園內既可欣賞山與海的美景。校園鄰近M6高速公路，可直達曼徹斯特，倫敦和蘇格蘭;
蘭卡斯特的火車也有提供直達車至各大城市。此外，蘭開斯特大學校區內擁有全英國最頻繁的大學巴士服務之美名。不僅如此，從校園至市區也設有自行車專屬道。

## 学生活动

在校园有很多学生自行运做的俱乐部和社会活动团体，他们包括：

  - [Bailrigg FM](../Page/Bailrigg_FM.md "wikilink")，学生广播电台
  - [Scan](../Page/Scan_报.md "wikilink")，学生报刊
  - 兰卡斯特大学电影院 (由电影社团负责运营)
  - [兰卡斯特大学徒步旅行俱乐部](../Page/兰卡斯特大学徒步旅行俱乐部.md "wikilink")
  - 兰卡斯特大学登山俱乐部

每年夏天，学校会与另一所北方名校[約克大學联合举办全歐洲最大的校際體育競賽](../Page/英國约克大学.md "wikilink")“玫瑰錦標賽”。比赛地点每年在约克和兰卡斯特之间轮换。

[LUSU](../Page/LUSU.md "wikilink"),
也就是兰卡斯特[学生会还在兰卡斯特城中心拥有一家叫Sugarhouse的](../Page/学生会.md "wikilink")[夜总会](../Page/夜总会.md "wikilink")。Sugarhouse的营业收入是学生会的主要收入来源。

## 学校领导

从2005年一月开始，校长是[Chris
Bonington爵士](../Page/Chris_Bonington.md "wikilink")。在此之前从建校始校长一直是[雅麗珊郡主](../Page/雅麗珊公主_\(奧格威爵士夫人\).md "wikilink")。

## 著名教授

Geoffery Leech, Norman Fairclough, Keith Beven

## 參考文獻

## 外部链接

  - [蘭卡斯特大學臺灣學生會](https://www.facebook.com/Lancaster-University-Taiwanese-Student-Society-%E8%98%AD%E5%8D%A1%E6%96%AF%E7%89%B9%E5%A4%A7%E5%AD%B8%E8%87%BA%E7%81%A3%E5%90%8C%E5%AD%B8%E6%9C%83-1652914744956410/?fref=ts/)
  - [Lancaster University website](http://www.lancaster.ac.uk/)
  - [History of Lancaster
    University](https://web.archive.org/web/20050507180106/http://www.lancs.ac.uk/users/history/universityhistory/index.htm)
  - [Lancaster University Students' Union (LUSU)](http://www.lusu.co.uk)
  - [The Sugarhouse](http://www.thesugarhouse.co.uk)
  - [Scan](https://web.archive.org/web/20050329094006/http://www.lusu.co.uk/scan)
    - the LUSU student newspaper
  - [The Scam](http://www.thescam.org.uk) - alternative student
    newspaper
  - [The Graduate College
    bar](https://web.archive.org/web/20151117210037/http://gradbar.co.uk/)
  - [Virtual campus
    tour](http://www.student360.com/main/index.asp?header=01&uniid=124)
  - [College bar
    reviews](http://www.pubutopia.com/pubs/L/Lancaster/Lancaster%20University/)

[Category:兰卡斯特大学](../Category/兰卡斯特大学.md "wikilink")
[Category:1964年創建的教育機構](../Category/1964年創建的教育機構.md "wikilink")