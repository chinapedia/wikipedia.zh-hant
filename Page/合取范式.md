在[布尔逻辑中](../Page/布尔逻辑.md "wikilink")，如果一个[公式是](../Page/公式_\(数理逻辑\).md "wikilink")[子句的](../Page/子句_\(逻辑\).md "wikilink")[合取](../Page/逻辑合取.md "wikilink")，那么它是**合取范式**(CNF)的。作为[规范形式](../Page/规范形式.md "wikilink")，它在[自动定理证明中有用](../Page/自动定理证明.md "wikilink")。它类似于在电路理论中的[规范和之积形式](../Page/规范和之积形式.md "wikilink")。

所有的文字的合取和所有的文字的析取是 CNF
的，因为可以被分别看作一个文字的子句的合取和一个单一子句的合取。和[析取范式](../Page/析取范式.md "wikilink")(DNF)中一样，在
CNF
公式中可以包含的命题连结词是[与](../Page/逻辑合取.md "wikilink")、[或和](../Page/逻辑析取.md "wikilink")[非](../Page/逻辑否定.md "wikilink")。非算子只能用做文字的一部分，这意味着它只能在命题变量前出现。

例如，下列所有公式都是 CNF:

\[A \wedge B\]

\[\neg A \wedge (B \vee C)\]

\[(A \vee B) \wedge (\neg B \vee C \vee \neg D) \wedge (D \vee \neg E)\]

\[(\neg B \vee C)\]

而下列不是:

\[\neg (B \vee C)\]

\[(A \wedge B) \vee C\]

\[A \wedge (B \vee (D \wedge E))\]

上述三个公式分别等价于合取范式的下列三个公式:

\[\neg B \wedge \neg C\]

\[(A \vee C) \wedge (B \vee C)\]

\[A \wedge (B \vee D) \wedge (B \vee E)\]

所有命题公式都可以转换成 CNF
的[等价公式](../Page/逻辑等价.md "wikilink")。这种变换基于了关于[逻辑等价的规则](../Page/逻辑等价.md "wikilink"):
[双重否定律](../Page/双重否定消去.md "wikilink")、[德·摩根定律和](../Page/德·摩根定律.md "wikilink")[分配律](../Page/分配律.md "wikilink")。

因为所有逻辑公式都可以转换成合取范式的等价公式，证明经常基于所有公式都是 CNF 的假定。但是在某些情况下，这种到 CNF
的转换可能导致公式的指数性爆涨。例如，把下述非-CNF 公式转换成 CNF 生成有 \(2^n\)
个子句的公式:

\[(X_1 \wedge Y_1) \vee (X_2 \wedge Y_2) \vee \dots \vee (X_n \wedge Y_n)\]

## 参见

  - [析取范式](../Page/析取范式.md "wikilink")
  - [代数范式](../Page/代数范式.md "wikilink")
  - [霍恩子句](../Page/霍恩子句.md "wikilink")
  - [奎因－麦克拉斯基算法](../Page/奎因－麦克拉斯基算法.md "wikilink")

## 外部链接

  - [Scheme and Ocaml programs for converting propositional logic
    statements into
    CNF](https://web.archive.org/web/20070312055054/http://www.g615.co.uk/riftor/content.php?view=5&type=1)

[H](../Category/布尔代数.md "wikilink")