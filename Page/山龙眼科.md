**山龙眼科**包括80[属](../Page/属.md "wikilink")，约2000余[种](../Page/种.md "wikilink")，主要分布在南半球，大部分种类在[澳洲和](../Page/澳洲.md "wikilink")[南非](../Page/南非.md "wikilink")，此外在[南美洲和](../Page/南美洲.md "wikilink")[东南亚有少数种类](../Page/东南亚.md "wikilink")\[1\]，[中国原产的有](../Page/中国.md "wikilink")[山龙眼属和](../Page/山龙眼属.md "wikilink")[假山龙眼属](../Page/假山龙眼属.md "wikilink")2属共21种，分布在南方。

本科[植物多为](../Page/植物.md "wikilink")[灌木或](../Page/灌木.md "wikilink")[乔木](../Page/乔木.md "wikilink")，稀为多年生[草本](../Page/草本.md "wikilink")；单[叶互生](../Page/叶.md "wikilink")、稀对生或轮生，常革质，无托叶；[花两性](../Page/花.md "wikilink")，稀单性，通常左右对称，稀为辐射对称，无[花瓣](../Page/花瓣.md "wikilink")；[花萼为](../Page/花萼.md "wikilink")[花瓣状](../Page/花瓣.md "wikilink")，4基数，具有[颜色](../Page/颜色.md "wikilink")，[花小](../Page/花.md "wikilink")，但[花序明显](../Page/花序.md "wikilink")，有特异化的很特殊的传粉机制\[2\]；[果实为](../Page/果实.md "wikilink")[坚果](../Page/坚果.md "wikilink")、[核果](../Page/核果.md "wikilink")、[蒴果或蓇葖果](../Page/蒴果.md "wikilink")。各品种适应分化的非常严重，因此有时很难通过共性确定其[科属](../Page/科.md "wikilink")\[3\]，但各[属之间还是有明显的区别](../Page/属.md "wikilink")。

[ 月桂叶哈克木 (*Hakea
laurina*)](https://zh.wikipedia.org/wiki/File:Pincushion_hakea03.jpg "fig: 月桂叶哈克木 (Hakea laurina)")

[Banksia_coccinea_(Illustrationes_Florae_Novae_Hollandiae_plate_3).jpg](https://zh.wikipedia.org/wiki/File:Banksia_coccinea_\(Illustrationes_Florae_Novae_Hollandiae_plate_3\).jpg "fig:Banksia_coccinea_(Illustrationes_Florae_Novae_Hollandiae_plate_3).jpg")

## 属

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><em><a href="../Page/Acidonia.md" title="wikilink">Acidonia</a></em></li>
<li><em><a href="../Page/Adenanthos.md" title="wikilink">Adenanthos</a></em></li>
<li><em><a href="../Page/Agastachys.md" title="wikilink">Agastachys</a></em></li>
<li><em><a href="../Page/Alloxylon.md" title="wikilink">Alloxylon</a></em></li>
<li><em><a href="../Page/Athertonia.md" title="wikilink">Athertonia</a></em></li>
<li><em><a href="../Page/Aulax.md" title="wikilink">Aulax</a></em></li>
<li><em><a href="../Page/Austromuellera.md" title="wikilink">Austromuellera</a></em></li>
<li><a href="../Page/棒柯属.md" title="wikilink">棒柯属</a> <em>Banksia</em></li>
<li><em><a href="../Page/Beauprea.md" title="wikilink">Beauprea</a></em></li>
<li><em><a href="../Page/Beapreopsis.md" title="wikilink">Beapreopsis</a></em></li>
<li><em><a href="../Page/Bellendena.md" title="wikilink">Bellendena</a></em></li>
<li><em><a href="../Page/Brabejum.md" title="wikilink">Brabejum</a></em></li>
<li><em><a href="../Page/Buckinghamia.md" title="wikilink">Buckinghamia</a></em></li>
<li><em><a href="../Page/Cardwellia.md" title="wikilink">Cardwellia</a></em></li>
<li><em><a href="../Page/Carnarvonia.md" title="wikilink">Carnarvonia</a></em></li>
<li><em><a href="../Page/Cenarrhenes.md" title="wikilink">Cenarrhenes</a></em></li>
<li><em><a href="../Page/Conospermum.md" title="wikilink">Conospermum</a></em></li>
<li><em><a href="../Page/Darlingia.md" title="wikilink">Darlingia</a></em></li>
<li><em><a href="../Page/Diastella.md" title="wikilink">Diastella</a></em></li>
<li><em><a href="../Page/Dilobeia.md" title="wikilink">Dilobeia</a></em></li>
</ul></td>
<td><ul>
<li><em><a href="../Page/Dryandra.md" title="wikilink">Dryandra</a></em></li>
<li><em><a href="../Page/Eidothea.md" title="wikilink">Eidothea</a></em></li>
<li><em><a href="../Page/Embothrium.md" title="wikilink">Embothrium</a></em></li>
<li><em><a href="../Page/Eucarpha.md" title="wikilink">Eucarpha</a></em></li>
<li><em><a href="../Page/Euplassa.md" title="wikilink">Euplassa</a></em></li>
<li><em><a href="../Page/Faurea.md" title="wikilink">Faurea</a></em></li>
<li><em><a href="../Page/Finschia.md" title="wikilink">Finschia</a></em></li>
<li><em><a href="../Page/Floydia.md" title="wikilink">Floydia</a></em></li>
<li><em><a href="../Page/Franklandia.md" title="wikilink">Franklandia</a></em></li>
<li><em><a href="../Page/Garnieria.md" title="wikilink">Garnieria</a></em></li>
<li><em><a href="../Page/Gevuina.md" title="wikilink">Gevuina</a></em></li>
<li><a href="../Page/银桦属.md" title="wikilink">银桦属</a> <em>Grevillea</em></li>
<li><a href="../Page/哈克木属.md" title="wikilink">哈克木属</a> <em>Hakea</em></li>
<li><a href="../Page/山龙眼属.md" title="wikilink">山龙眼属</a> <em>Helicia</em></li>
<li><a href="../Page/假山龙眼属.md" title="wikilink">假山龙眼属</a> <em>Heliciopsis</em></li>
<li><em><a href="../Page/Hicksbeachia.md" title="wikilink">Hicksbeachia</a></em></li>
<li><em><a href="../Page/Hollandaea.md" title="wikilink">Hollandaea</a></em></li>
<li><em><a href="../Page/Isopogon.md" title="wikilink">Isopogon</a></em></li>
<li><em><a href="../Page/Kermadecia.md" title="wikilink">Kermadecia</a></em></li>
</ul></td>
<td><ul>
<li><em><a href="../Page/Knightia.md" title="wikilink">Knightia</a></em></li>
<li><em><a href="../Page/Lambertia.md" title="wikilink">Lambertia</a></em></li>
<li><a href="../Page/木百合屬.md" title="wikilink">木百合屬</a>（<a href="../Page/銀葉樹屬.md" title="wikilink">銀葉樹屬</a>）<em><a href="../Page/Leucadendron.md" title="wikilink">Leucadendron</a></em></li>
<li><a href="../Page/針墊花屬.md" title="wikilink">針墊花屬</a> <em>Leucospermum</em></li>
<li><em><a href="../Page/Lomatia.md" title="wikilink">Lomatia</a></em></li>
<li><a href="../Page/澳洲坚果属.md" title="wikilink">澳洲坚果属</a> <em>Macadamia</em></li>
<li><em><a href="../Page/Malagasia.md" title="wikilink">Malagasia</a></em></li>
<li><em><a href="../Page/Mimetes.md" title="wikilink">Mimetes</a></em></li>
<li><em><a href="../Page/Musgravea.md" title="wikilink">Musgravea</a></em></li>
<li><em><a href="../Page/Neorites.md" title="wikilink">Neorites</a></em></li>
<li><em><a href="../Page/Opisthiolepis.md" title="wikilink">Opisthiolepis</a></em></li>
<li><em><a href="../Page/Oreocallis.md" title="wikilink">Oreocallis</a></em></li>
<li><em><a href="../Page/Orites.md" title="wikilink">Orites</a></em></li>
<li><em><a href="../Page/Orothamnus.md" title="wikilink">Orothamnus</a></em></li>
<li><em><a href="../Page/Panopsis.md" title="wikilink">Panopsis</a></em></li>
<li><em><a href="../Page/Paranomus.md" title="wikilink">Paranomus</a></em></li>
<li><em><a href="../Page/Persoonia.md" title="wikilink">Persoonia</a></em></li>
<li><em><a href="../Page/Petrophile.md" title="wikilink">Petrophile</a></em></li>
<li><em><a href="../Page/Placospermum.md" title="wikilink">Placospermum</a></em></li>
</ul></td>
<td><ul>
<li><a href="../Page/帝王花屬.md" title="wikilink">帝王花屬</a> <em>Protea</em></li>
<li><em><a href="../Page/Roupala.md" title="wikilink">Roupala</a></em></li>
<li><em><a href="../Page/Serruria.md" title="wikilink">Serruria</a></em></li>
<li><em><a href="../Page/Sleumerodendron.md" title="wikilink">Sleumerodendron</a></em></li>
<li><em><a href="../Page/Sorocephalus.md" title="wikilink">Sorocephalus</a></em></li>
<li><em><a href="../Page/Spatalla.md" title="wikilink">Spatalla</a></em></li>
<li><em><a href="../Page/Sphalmium.md" title="wikilink">Sphalmium</a></em></li>
<li><em><a href="../Page/Stenocarpus.md" title="wikilink">Stenocarpus</a></em></li>
<li><em><a href="../Page/Stirlingia.md" title="wikilink">Stirlingia</a></em></li>
<li><em><a href="../Page/Strangea.md" title="wikilink">Strangea</a></em></li>
<li><em><a href="../Page/Symphionema.md" title="wikilink">Symphionema</a></em></li>
<li><em><a href="../Page/Synaphea.md" title="wikilink">Synaphea</a></em></li>
<li><a href="../Page/蒂羅花属.md" title="wikilink">蒂羅花属</a> <em>Telopea</em></li>
<li><em><a href="../Page/Toronia.md" title="wikilink">Toronia</a></em></li>
<li><em><a href="../Page/Triunia.md" title="wikilink">Triunia</a></em></li>
<li><em><a href="../Page/Turrillia.md" title="wikilink">Turrillia</a></em></li>
<li><em><a href="../Page/Vexatorella.md" title="wikilink">Vexatorella</a></em></li>
<li><em><a href="../Page/Virotia.md" title="wikilink">Virotia</a></em></li>
<li><em><a href="../Page/Xylomelum.md" title="wikilink">Xylomelum</a></em></li>
</ul></td>
</tr>
</tbody>
</table>

## 参考文献

<references/>

## 外部链接

  - [澳大利亚国家植物园中的山龙眼科图片](http://www.anbg.gov.au/images/photo_cd/proteaceae/)
  - [《中国植物》中的山龙眼科](http://www.efloras.org/florataxon.aspx?flora_id=2&taxon_id=10730)
  - [NCBI分类中的山龙眼科](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Tree&id=4328&lvl=3&p=mapview&p=has_linkout&p=blast_url&p=genome_blast&lin=f&keep=1&srchmode=1&unlock)

[\*](../Category/山龙眼科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")

1.
2.
3.