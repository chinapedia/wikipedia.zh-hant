《**All The Best
98-01**》是[歌手](../Page/歌手.md "wikilink")[古巨基的流行音樂精選輯](../Page/古巨基.md "wikilink")，於2001年9月7日正式發行。

## 曲目

## 派台歌曲成績

| 歌曲                | 903 專業推介 | RTHK 中文歌曲龍虎榜 | 997 勁爆流行榜 | TVB 勁歌金榜 |
| ----------------- | -------- | ------------ | --------- | -------- |
| 幸運鈴聲              | 9        | \-           | 4         | \-       |
| Happy Birthday 2U | 10       | \-           | \-        | \-       |

[Category:古巨基音樂專輯](../Category/古巨基音樂專輯.md "wikilink")
[Category:香港音樂專輯](../Category/香港音樂專輯.md "wikilink")
[Category:2001年音樂專輯](../Category/2001年音樂專輯.md "wikilink")
[Category:流行音樂專輯](../Category/流行音樂專輯.md "wikilink")