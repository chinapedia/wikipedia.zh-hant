**OpenOffice.org Writer**（又稱**OpenOffice Writer**，**OpenOffice
文書處理器**或**OO.o
文書處理器**）是由[OpenOffice.org所開發的免費文字編輯](../Page/OpenOffice.org.md "wikilink")[軟件](../Page/軟件.md "wikilink")。這一個軟件提供[Microsoft
Word軟件包括的基本功能](../Page/Microsoft_Word.md "wikilink")，可以把文件以[DOC或](../Page/DOC.md "wikilink")[PDF的形式儲存和輸出](../Page/PDF.md "wikilink")。

OpenOffice.org還提供[OpenOffice.org
Impress](../Page/OpenOffice.org_Impress.md "wikilink")、[OpenOffice.org
Math](../Page/OpenOffice.org_Math.md "wikilink")、[OpenOffice.org
Draw](../Page/OpenOffice.org_Draw.md "wikilink")、[OpenOffice.org
Calc和](../Page/OpenOffice.org_Calc.md "wikilink")[OpenOffice.org
Base免費下載](../Page/OpenOffice.org_Base.md "wikilink")。以上的軟件都有48種（尚有其他語言的版本在開發中）的不同[語言版本方便各國人士](../Page/語言.md "wikilink")。

## 參見

  - [文字處理器比較](../Page/文字處理器比較.md "wikilink")
  - [開放美工圖庫](../Page/開放美工圖庫.md "wikilink")
  - [OpenOffice.org](../Page/OpenOffice.org.md "wikilink")
  - [Calc](../Page/OpenOffice.org_Calc.md "wikilink")
  - [Impress](../Page/OpenOffice.org_Impress.md "wikilink")
  - [Math](../Page/OpenOffice.org_Math.md "wikilink")
  - [Draw](../Page/OpenOffice.org_Draw.md "wikilink")

## 參考資料

  - [OpenOffice.org](http://www.openoffice.org)

[de:Apache
OpenOffice\#Writer](../Page/de:Apache_OpenOffice#Writer.md "wikilink")

[Category:开放源代码](../Category/开放源代码.md "wikilink")
[Category:文書處理器](../Category/文書處理器.md "wikilink")