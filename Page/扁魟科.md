**扁魟科**又稱平魟科，是[軟骨魚綱](../Page/軟骨魚綱.md "wikilink")[燕魟目的一](../Page/燕魟目.md "wikilink")[科](../Page/科.md "wikilink")。

## 分布

本科魚類廣泛分布於各大洋。

## 深度

水深3至200公尺以上。

## 分類

**扁魟科**下分3個屬，如下：

### 鷂扁魟屬(*Trygonoptera*)

  -   - [爪哇鷂扁魟](../Page/爪哇鷂扁魟.md "wikilink")(*Trygonoptera javanica*)
      - [凱納鷂扁魟](../Page/凱納鷂扁魟.md "wikilink")(*Trygonoptera kaiana*)
      - [黏鷂扁魟](../Page/黏鷂扁魟.md "wikilink")(*Trygonoptera mucosa*)
      - [卵形鷂扁魟](../Page/卵形鷂扁魟.md "wikilink")(*Trygonoptera ovalis*)
      - [野鷂扁魟](../Page/野鷂扁魟.md "wikilink")(*Trygonoptera personata*)

### 大尾扁魟屬(*Urobatis*)

  -   - [哈氏大尾扁魟](../Page/哈氏大尾扁魟.md "wikilink")(*Urobatis halleri*)

### 扁魟屬(*Urolophus*)

  -   - [盔扁魟](../Page/盔扁魟.md "wikilink")(*Urolophus armatus*)
      - [黃平魟](../Page/黃平魟.md "wikilink")(*Urolophus
        aurantiacus*)：又稱褐黃扁魟。
      - [大扁魟](../Page/大扁魟.md "wikilink")(*Urolophus bucculentus*)
      - [圓盤扁魟](../Page/圓盤扁魟.md "wikilink")(*Urolophus circularis*)
      - [聚扁魟](../Page/聚扁魟.md "wikilink")(*Urolophus concentricus*)
      - [帶紋扁魟](../Page/帶紋扁魟.md "wikilink")(*Urolophus cruciatus*)
      - [南澳扁魟](../Page/南澳扁魟.md "wikilink")(*Urolophus expansus*)
      - [雜色扁魟](../Page/雜色扁魟.md "wikilink")(*Urolophus flavomosaicus*)
      - [巨斑扁魟](../Page/巨斑扁魟.md "wikilink")(*Urolophus gigas*)
      - [牙買加扁魟](../Page/牙買加扁魟.md "wikilink")(*Urolophus jamaicensis*)
      - [葉狀扁魟](../Page/葉狀扁魟.md "wikilink")(*Urolophus lobatus*)
      - [斑扁魟](../Page/斑扁魟.md "wikilink")(*Urolophus maculatus*)
      - [紋背扁魟](../Page/紋背扁魟.md "wikilink")(*Urolophus mitosis*)
      - [黏扁魟](../Page/黏扁魟.md "wikilink")(*Urolophus mucosus*)
      - [海濱扁魟](../Page/海濱扁魟.md "wikilink")(*Urolophus orarius*)
      - [少斑扁魟](../Page/少斑扁魟.md "wikilink")(*Urolophus paucimaculatus*)
      - [醜扁魟](../Page/醜扁魟.md "wikilink")(*Urolophus sufflavus*)
      - [扁魟](../Page/扁魟.md "wikilink")(*Urolophus testaceus*)
      - [淺綠扁魟](../Page/淺綠扁魟.md "wikilink")(*Urolophus viridis*)
      - [西澳扁魟](../Page/西澳扁魟.md "wikilink")(*Urolophus westraliensis*)

## 特徵

本科是所有魟魚中唯一具有尾鳍者，尾鳍上有辐状软骨支撑。尾部較體盤前後徑略長，但亦有少數種類略短。尾部上方有一強棘，棘之兩側具有鋸齒緣並會分泌毒液。

## 生態

本科魚類廣泛分布於全球各大洋，有些具有地域性，大部分為海水魚，有時見於河口，少部分則生活於[南美洲](../Page/南美洲.md "wikilink")、[非洲及](../Page/非洲.md "wikilink")[東南亞淡水水域](../Page/東南亞.md "wikilink")。活動力差，通常將身體埋於沙泥中，僅露出雙眼和呼吸孔，或利用胸鰭做波浪狀的運動而貼游於底層水域。屬肉食性，以小魚、[甲殼類及](../Page/甲殼類.md "wikilink")[軟體動物為食](../Page/軟體動物.md "wikilink")。卵胎生。

## 經濟利用

食用魚，但味道較腥，以[紅燒為宜](../Page/紅燒.md "wikilink")，或當作下雜魚處理。

## 参考文献

  - [FishBase](http://fishbase.sinica.edu.tw/)
  - [台灣魚類資料庫](http://fishdb.sinica.edu.tw/)

## 外部链接

  - [扁魟科介绍和照片](http://www.marinethemes.com/sparspottedray.html)

[\*](../Category/扁魟科.md "wikilink")