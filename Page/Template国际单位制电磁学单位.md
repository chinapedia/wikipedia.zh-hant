<table>
<thead>
<tr class="header">
<th><p><a href="国际单位制.md" title="wikilink">国际单位制</a><a href="电磁学.md" title="wikilink">电磁学</a><a href="国际单位制基本单位.md" title="wikilink">单位</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>符号</p></td>
</tr>
<tr class="even">
<td><p>I</p></td>
</tr>
<tr class="odd">
<td><p>H</p></td>
</tr>
<tr class="even">
<td><p>Q</p></td>
</tr>
<tr class="odd">
<td><p>V</p></td>
</tr>
<tr class="even">
<td><p>E</p></td>
</tr>
<tr class="odd">
<td><p>R; Z; X</p></td>
</tr>
<tr class="even">
<td><p>ρ</p></td>
</tr>
<tr class="odd">
<td><p>P</p></td>
</tr>
<tr class="even">
<td><p>C</p></td>
</tr>
<tr class="odd">
<td><p>ε0</p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td><p>G; Y; B</p></td>
</tr>
<tr class="even">
<td><p>κ, γ, σ</p></td>
</tr>
<tr class="odd">
<td><p>Φ</p></td>
</tr>
<tr class="even">
<td><p>B</p></td>
</tr>
<tr class="odd">
<td><p>R</p></td>
</tr>
<tr class="even">
<td><p>L, M</p></td>
</tr>
<tr class="odd">
<td><p>μ</p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td><div style="float:right;">
<p><small></small></p>
</div></td>
</tr>
</tbody>
</table>

<noinclude> </noinclude>

[Category:物理学模板](../Category/物理学模板.md "wikilink")