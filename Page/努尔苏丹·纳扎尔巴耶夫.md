**努尔苏丹·阿比舍维奇·纳扎尔巴耶夫**，[GCMG](../Page/GCMG.md "wikilink")（，；，；），[哈萨克斯坦政治人物](../Page/哈萨克斯坦.md "wikilink")，[苏联时期担任](../Page/苏联.md "wikilink")[哈萨克共产党第一书记](../Page/哈萨克共产党.md "wikilink")，在1991年12月[独立后即任](../Page/苏联解体.md "wikilink")[哈萨克斯坦总统](../Page/哈萨克斯坦总统.md "wikilink")\[1\]。2019年3月19日纳扎巴耶夫宣布辞去哈萨克斯坦总统的职务\[2\]\[3\]。

## 生平

纳扎尔巴耶夫1940年7月6日生于[哈萨克苏维埃社会主义共和国](../Page/哈萨克苏维埃社会主义共和国.md "wikilink")[阿拉木图州卡斯克连区切莫尔甘村](../Page/阿拉木图州.md "wikilink")。1967年毕业于卡拉干达工学院；1979年—1984年任[哈萨克共产党中央委员会书记](../Page/哈萨克共产党.md "wikilink")；1984年—1989年任[部长会议主席](../Page/哈萨克斯坦总理.md "wikilink")（[总理](../Page/总理.md "wikilink")）；1989年—1991年任共产党中央委员会第一书记（最高领导人）；1990年2月兼任[最高苏维埃主席](../Page/最高苏维埃.md "wikilink")（[国家元首](../Page/国家元首.md "wikilink")）；1990年4月起任[哈萨克苏维埃社会主义共和国总统](../Page/哈萨克苏维埃社会主义共和国.md "wikilink")。

自独立以来，哈萨克斯坦没有举行任何被西方认为是自由或公平的选举\[4\]
\[5\]。1991年后纳扎尔巴耶夫一直担任[哈萨克斯坦总统至](../Page/哈萨克斯坦总统.md "wikilink")2019年。他在、、、[2011年](../Page/2011年哈萨克斯坦总统选举.md "wikilink")、[2015年的哈萨克斯坦总统选举中分别获得了](../Page/2015年哈萨克斯坦总统选举.md "wikilink")98.8%\[6\]、81.00%\[7\]、91.15%、95.55%、97.75%\[8\]的得票率。

2019年3月19日，在全国各城市发生异常持久的抗议活动之后\[9\]\[10\]，纳扎尔巴耶夫宣布辞去哈萨克斯坦总统的职务，理由是需要“新一代领导人”\[11\]。该公告于阿斯塔纳的电视讲话中发出，之后他签署了一项法令，从2019年3月20日起终止了他的权力\[12\]。参议院主席[卡·托卡耶夫被任命为该国总统](../Page/卡瑟姆若马尔特·托卡耶夫.md "wikilink")，直至该总统任期结束\[13\]。

## 评价

### 正面评价

据[英国广播公司报道](../Page/英国广播公司.md "wikilink")，纳扎尔巴耶夫执政生涯中注重经济改革。其支持者称纳扎尔巴耶夫上世纪90年代的改革维持了内部稳定，推动哈萨克斯坦在21世纪前十年间取得惊人经济增长\[14\]。

[中国外交部称纳扎尔巴耶夫是哈萨克斯坦共和国的缔造者](../Page/中华人民共和国外交部.md "wikilink")，是深受全体哈萨克斯坦人民拥戴的民族领袖\[15\]。俄罗斯总统[普京称赞他的领导](../Page/普京.md "wikilink")\[16\]，乌兹别克斯坦总统[沙夫卡特·米尔济约耶夫称他是](../Page/沙夫卡特·米尔济约耶夫.md "wikilink")“伟大的政治家”\[17\]。

### 多项指控

据《[卫报](../Page/卫报.md "wikilink")》报道，纳扎尔巴耶夫已被数个人权组织指控侵犯人权、压制异议以及主持专制政权\[18\]。中亚问题学者指出，在纳扎尔巴耶夫担任总统期间，针对纳扎尔巴耶夫及其圈子的腐败和偏袒指控不断上升。批评者说，该国政府已经变得像一个家族系统\[19\]。

据《[纽约客](../Page/纽约客.md "wikilink")》报道，1999年，瑞士银行业官员在一个显然属于纳扎尔巴耶夫的账户中发现了85亿美元，而这笔钱本属于哈萨克斯坦财政部\[20\]。随后，纳扎尔巴耶夫成功地推动了一项议会法案，授予他法律豁免权，以及另一项旨在使洗钱合法化的议案，进一步激怒了批评者\[21\]。哈萨克斯坦报纸《共和国报》（Respublika）在2002年报道，纳扎尔巴耶夫在20世纪90年代中期，秘密将10亿美元的国家石油收入藏进了他的瑞士银行账户。随后报纸的办公室外面出现了一只被砍掉头的狗，并附有警告：“不会有下一次”。狗的头后来出现在报社编辑——伊琳娜·彼得罗什娃（Irina
Petrushova）的公寓外面，同样附有警告：“不会有最后一次”\[22\]\[23\]\[24\]。这家报纸还遭到了燃烧弹袭击\[25\]。但并无直接证据证明以上行为是纳扎尔巴耶夫所为。

2007年5月，哈萨克斯坦议会批准了一项宪法修正案，允许纳扎尔巴耶夫按自己的意愿寻求多次连任。这项修正案仅适用于纳扎尔巴耶夫，因为它规定该国的第一位总统竞选公职的次数不受限制，但下一位总统的任期被限制在五年内\[26\]
。

## 相关作品

  - 《[我童年的天空](../Page/我童年的天空.md "wikilink")》，努尔苏丹·纳扎尔巴耶夫的传记电影

## 参考文献

{{-}}

[Category:哈萨克斯坦总统](../Category/哈萨克斯坦总统.md "wikilink")
[Category:蘇聯共產黨人物](../Category/蘇聯共產黨人物.md "wikilink")
[Category:哈薩克斯坦穆斯林](../Category/哈薩克斯坦穆斯林.md "wikilink")
[Category:哈萨克族人](../Category/哈萨克族人.md "wikilink")
[Category:比利時利奧波德大綬章勳章持有人](../Category/比利時利奧波德大綬章勳章持有人.md "wikilink")
[Category:法國榮譽軍團大十字勳章持有人](../Category/法國榮譽軍團大十字勳章持有人.md "wikilink")
[Category:聖米迦勒及聖喬治勳章榮譽爵級大十字勳章持有人](../Category/聖米迦勒及聖喬治勳章榮譽爵級大十字勳章持有人.md "wikilink")
[Category:蘇聯共產黨中央政治局委員](../Category/蘇聯共產黨中央政治局委員.md "wikilink")
[Category:聖安德烈勳章持有人](../Category/聖安德烈勳章持有人.md "wikilink")

1.

2.

3.

4.

5.

6.  Nohlen, D, Grotz, F & Hartmann, C (2001) *Elections in Asia: A data
    handbook, Volume I*, p424 ISBN 0-19-924958-X

7.  Nohlen, D, Grotz, F & Hartmann, C (2001) *Elections in Asia: A data
    handbook, Volume I*, p424 ISBN 0-19-924958-X

8.

9.  [Kazakh President Nazarbaev Abruptly Resigns, But Will Retain Key
    Roles](https://www.rferl.org/a/kazakh-president-nursultan-nazarbaev-says-he-is-resigning-/29830123.html)

10. [Обращение Главы государства Нурсултана Назарбаева к народу
    Казахстана](http://www.akorda.kz/ru/speeches/internal_political_affairs/in_speeches_and_addresses/obrashchenie-glavy-gosudarstva-nursultana-nazarbaeva-k-narodu-kazahstana).
    The official web site of the President of Kazakhstan, 19 March 2019.

11.

12.
13.
14.

15.

16. <https://m.youtube.com/watch?v=zJszdTom8VI>

17. <https://www.inform.kz/en/shavkat-mirziyoyev-nursultan-nazarbayev-is-a-great-politician_a3509084>

18.

19.

20.

21.

22.

23.

24.

25.
26.