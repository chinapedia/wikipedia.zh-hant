**相位陣列**（），是由一群[天線組成的陣列](../Page/天線.md "wikilink")。送往各個天線的訊號的相對[相位經過適當調整](../Page/相位.md "wikilink")，最後會強化訊號在指定方向的強度，並且壓抑其他方向的強度。此技術本來是為[射電天文學開發](../Page/射電天文學.md "wikilink")。後來也為[主動電子掃描陣列雷達所用](../Page/主動電子掃描陣列雷達.md "wikilink")。

許多地區的[調幅廣播](../Page/調幅廣播.md "wikilink")[電臺都使用這個技術](../Page/廣播電臺.md "wikilink")，可以限定廣播的範圍，減少對其他地區的[干擾](../Page/干擾.md "wikilink")。
[PAVE_PAWS_Radar_Clear_AFS_Alaska.jpg](https://zh.wikipedia.org/wiki/File:PAVE_PAWS_Radar_Clear_AFS_Alaska.jpg "fig:PAVE_PAWS_Radar_Clear_AFS_Alaska.jpg")就利用了相位陣列原理。\]\]

## 外部連結

  - [Radar Research and Development - Phased Array
    Radar](http://www.nssl.noaa.gov/par/) — [National Severe Storms
    Laboratory](../Page/National_Severe_Storms_Laboratory.md "wikilink")
  - [Shipboard Phased Array
    Radars](http://www.harpoonhq.com/waypoint/articles/Article_044.pdf)
  - [NASA Report: MMICs For Multiple Scanning Beam Antennas for Space
    Applications](http://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/19870018450_1987018450.pdf)
  - [Principle of Phased
    Array](http://www.radartutorial.eu/06.antennas/Phased%20Array%20Antenna.en.html)
    @ www.radartutorial.eu
  - ['Phased Array' microphone system of Tony
    Faulkner](http://www.sengpielaudio.com/TonyFaulknerPhasedArray06.htm)

[category:天线](../Page/category:天线.md "wikilink")

[Category:無線電](../Category/無線電.md "wikilink")