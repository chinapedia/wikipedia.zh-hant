**周文俊**（Wen Tsing
Chow，），1940年交通大學電機工程學士、1942年美國[麻省理工學院電機工程碩士](../Page/麻省理工學院.md "wikilink")。是[中國出生的](../Page/中國.md "wikilink")[美國](../Page/美國.md "wikilink")[导弹制导](../Page/导弹制导.md "wikilink")[科學家與數位化](../Page/科學家.md "wikilink")[電腦](../Page/電腦.md "wikilink")[科技先驅者](../Page/科技.md "wikilink")。

1956年，周文俊在[紐約](../Page/紐約.md "wikilink")[加頓城的美國](../Page/加頓城.md "wikilink")[保殊艾瑪公司工作](../Page/保殊艾瑪公司.md "wikilink")，並發明了[PROM](../Page/PROM.md "wikilink")。這項發明是由[美國空軍所提出以用作提升空軍用電腦以及Atlas](../Page/美國空軍.md "wikilink")
E/F波段導彈的靈活性和保安性的。

2004年美國空軍追贈美國空軍太空與飛彈先驅獎（Air Force Space and Missiles Pioneers
Award）之最高榮譽，成為獲獎的少數平民之一，此前獲獎者也只有約30人。現代電腦創始人之一[范·紐曼](../Page/约翰·冯·诺伊曼.md "wikilink")（John
von Neumann）與周文俊是目前為止獲得該獎的僅有的兩位電腦科學家。

## 參照

  - [PROM](../Page/PROM.md "wikilink")
  - [Saturn V](../Page/Saturn_V.md "wikilink")
  - [AP-101](../Page/AP-101.md "wikilink")

## 外部連結

1.
2.  [U. S. Patent 3,028,659 for key PROM
    technology](http://patimg2.uspto.gov/.piw?Docid=03028659&idkey=NONE)

[Category:美籍华裔科学家](../Category/美籍华裔科学家.md "wikilink")
[Category:美国计算机科学家](../Category/美国计算机科学家.md "wikilink")
[Category:国立交通大學校友](../Category/国立交通大學校友.md "wikilink")
[Category:麻省理工學院校友](../Category/麻省理工學院校友.md "wikilink")
[Category:IBM員工](../Category/IBM員工.md "wikilink")