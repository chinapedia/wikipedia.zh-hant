**[台北樹蛙](../Page/臺北.md "wikilink")**（[學名](../Page/學名.md "wikilink")：）为[树蛙科](../Page/树蛙科.md "wikilink")[树蛙属的](../Page/树蛙属.md "wikilink")[两栖动物](../Page/两栖动物.md "wikilink")，是[台灣的特有物种](../Page/台灣.md "wikilink")\[1\]。该物种的模式产地在台湾台北\[2\]。

## 特徵

身體背部呈翠綠色，腹部則呈黃色，具有隨環境而變更身體顏色的能力。體型屬於中等，體長雄性約，雌性約，比雄性稍長\[3\]。

## 習性

屬[肉食性](../Page/肉食動物.md "wikilink")，其賴以維生的食物是蟲或蟻，在冬天進行[交配](../Page/交配.md "wikilink")。一般聚集於水池、及[溝渠等](../Page/溝渠.md "wikilink")[靜水區域](../Page/靜水區域.md "wikilink")。\[4\]\[5\]\[6\]。

棲息於亞熱帶或熱帶潮濕的低地[森林](../Page/森林.md "wikilink")、亞熱帶或熱帶潮濕[山地森林](../Page/山地森林.md "wikilink")、[沼澤](../Page/沼澤.md "wikilink")、淡水[濕地](../Page/濕地.md "wikilink")、[耕地](../Page/耕地.md "wikilink")、[農園](../Page/農園.md "wikilink")，[池塘和灌溉農地](../Page/池塘.md "wikilink")。儘管族群數量穩定。但仍受[棲息地喪失威脅](../Page/棲息地喪失.md "wikilink")\[7\]。

## 分佈

分佈於[台灣本島](../Page/台灣島.md "wikilink")\[8\]，可在[台灣中部](../Page/台灣.md "wikilink")[南投以北的海拔](../Page/南投縣.md "wikilink")1,500米以下山區丘陵地見到，但主要分佈於[台北盆地地區](../Page/台北盆地.md "wikilink")。北部是台北樹蛙數量第一多的棲息地，基隆則是台灣台北樹蛙密度第一名的縣市。

台北樹蛙過去一直被誤認為為[日本樹蛙](../Page/日本樹蛙.md "wikilink")，直到1979年時才被[台大](../Page/台大.md "wikilink")[動物學系教授](../Page/國立臺灣大學動物學系.md "wikilink")[梁潤生及](../Page/梁潤生.md "wikilink")[王慶讓鑑定為新種](../Page/王慶讓.md "wikilink")，被[中華民國政府宣告為](../Page/中華民國政府.md "wikilink")[保育類動物](../Page/臺灣保育物種列表.md "wikilink")。

## 參見

  - [中強公園](../Page/中強公園.md "wikilink")
  - [內寮濕地](../Page/內寮濕地.md "wikilink")
  - [中央社區](../Page/中央社區.md "wikilink")

## 参考文献

## 外部連結

  -
[台北树蛙](../Category/树蛙属.md "wikilink")
[Category:台灣特有種](../Category/台灣特有種.md "wikilink")
[Category:台灣兩棲動物](../Category/台灣兩棲動物.md "wikilink")

1.
2.
3.

4.
5.

6.

7.
8.