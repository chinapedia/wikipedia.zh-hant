[中壢區](../Page/中壢區.md "wikilink"){{.w}}[平鎮區](../Page/平鎮區.md "wikilink"){{.w}}[八德區](../Page/八德區.md "wikilink"){{.w}}[楊梅區](../Page/楊梅區.md "wikilink"){{.w}}[蘆竹區](../Page/蘆竹區.md "wikilink"){{.w}}[龍潭區](../Page/龍潭區_\(台灣\).md "wikilink"){{.w}}[龜山區](../Page/龜山區.md "wikilink"){{.w}}[大溪區](../Page/大溪區.md "wikilink"){{.w}}[大園區](../Page/大園區.md "wikilink"){{.w}}[觀音區](../Page/觀音區.md "wikilink"){{.w}}[新屋區](../Page/新屋區.md "wikilink")

` |group2 = `[`原住民區`](../Page/直轄市山地原住民區.md "wikilink")
` | list2 = `[`復興區`](../Page/復興區_\(台灣\).md "wikilink")
` |group3 = 歷史沿革`
` | list3 = `[`天興州`](../Page/天興州.md "wikilink")
`   |group2 = 清治`
`   | list2 = `[`諸羅縣`](../Page/諸羅縣.md "wikilink")`{{→}}`[`彰化縣`](../Page/彰化縣_\(清朝\).md "wikilink")`{{→}}`[`淡水廳`](../Page/淡水廳.md "wikilink")`{{→}}`[`臺北府`](../Page/臺北府.md "wikilink")`（`[`淡水縣`](../Page/淡水縣.md "wikilink")`{{.w}}`[`新竹縣`](../Page/新竹縣_\(清朝\).md "wikilink")`{{.w}}`[`南雅廳`](../Page/南雅廳.md "wikilink")`）`
`   |group3 = 日治`
`   | list3 = `[`臺北縣`](../Page/臺北縣_\(日治時期\).md "wikilink")`{{→}}`[`臺北縣`](../Page/臺北縣_\(日治時期\).md "wikilink")`{{.w}}`[`新竹縣`](../Page/新竹縣_\(日治時期\).md "wikilink")`{{→}}`[`臺北縣`](../Page/臺北縣_\(日治時期\).md "wikilink")`{{→}}`[`桃仔園廳`](../Page/桃園廳.md "wikilink")`{{→}}`[`新竹州`](../Page/新竹州.md "wikilink")`（`[`中壢郡`](../Page/中壢郡.md "wikilink")`{{.w}}`[`桃園郡`](../Page/桃園郡.md "wikilink")`{{.w}}`[`大溪郡`](../Page/大溪郡.md "wikilink")`）`
`   |group4 = 民國`
`   | list4 = 新竹縣``（中壢區{{.w}}桃園區{{.w}}大溪區）{{→}}`[`桃園縣`](../Page/桃園縣.md "wikilink")
` }}`
` | below = `[`桃園市地理`](../Category/桃園市地理.md "wikilink")`{{.w}}`[`桃園市交通`](../Category/桃園市交通.md "wikilink")

}}<includeonly>{{\#IFEQ:|no||}}</includeonly><noinclude></noinclude>

[Category:桃園市行政區劃](../Category/桃園市行政區劃.md "wikilink")