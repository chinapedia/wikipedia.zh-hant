**無宗教**是指那些人們沒有固定的[宗教信仰](../Page/宗教信仰.md "wikilink")，不屬於任何一個宗教組織，包括但不等於是[無神論者](../Page/無神論.md "wikilink")，无宗教亦包含[不可知論者或](../Page/不可知論.md "wikilink")[有神論者](../Page/有神論.md "wikilink")。

## 定義

雖然被歸類成無宗教的人們不遵循一特定宗教，但這不表示他們就完全不相信[超自然或](../Page/超自然.md "wikilink")[神](../Page/神.md "wikilink")。他們可能是沒有特定宗教信仰或者不奉行宗教儀式的[有神論者](../Page/有神論.md "wikilink")。特別是那些對宗教有負面評價，或者把宗教定義成「組織性的宗教」的人，他們可能有或可能沒有[精神上的信仰](../Page/精神.md "wikilink")，但會說他們自己是無宗教信仰，不參加及不屬於任何一个宗教组織。

有許多無宗教信仰者，仍然會儀式性地參加許多如[嬰兒洗禮](../Page/嬰兒洗禮.md "wikilink")、[婚禮](../Page/婚禮.md "wikilink")、[喪禮等宗教禮節與祭祀等宗教活動](../Page/喪禮.md "wikilink")，例如[欧洲基督教国家中的无宗教信仰者中不少仍是](../Page/欧洲.md "wikilink")[天主教会](../Page/天主教会.md "wikilink")、[路德宗國教會](../Page/路德宗.md "wikilink")、[聖公宗國教會](../Page/聖公宗.md "wikilink")、[東正教会成员](../Page/東正教会.md "wikilink")，部分仍會在[基督教主要節日如](../Page/基督教.md "wikilink")[復活節和](../Page/復活節.md "wikilink")[聖誕節到](../Page/聖誕節.md "wikilink")[教堂參加](../Page/教堂.md "wikilink")[崇拜](../Page/崇拜.md "wikilink")；[日本的很多所謂](../Page/日本.md "wikilink")[神道教人士也無宗教信仰](../Page/神道教.md "wikilink")，但會按傳統到[神社參拜](../Page/神社.md "wikilink")；許多[中國大陸](../Page/中國大陸.md "wikilink")、[香港和](../Page/香港.md "wikilink")[臺灣等地的](../Page/臺灣.md "wikilink")[華人](../Page/華人.md "wikilink")，心靈上並無宗教信仰，但因為尊重傳統，在不少重要節日和傳統節慶會到[廟宇參拜](../Page/廟宇.md "wikilink")，也會參加[掃墓](../Page/掃墓.md "wikilink")、[祭祖或者](../Page/祭祖.md "wikilink")[中元普渡等活動](../Page/中元普渡.md "wikilink")。

## 各國統計資料

[Irreligion_map.png](https://zh.wikipedia.org/wiki/File:Irreligion_map.png "fig:Irreligion_map.png")

| 國家 | 無宗教人口比例 (多于20%者)（2012年） | 日期及資料                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| -- | ----------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| |  | 70.4                    | \[1\]                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| |  | 67.8                    | <ref name="Czech Statistical Office">{{Cite web|url=[http://www.czso.cz/sldb2011/eng/redakce.nsf/i/tab_7_1_population_by_religious_belief_and_by_municipality_size_groups/$File/PVCR071_ENG.pdf|title=](http://www.czso.cz/sldb2011/eng/redakce.nsf/i/tab_7_1_population_by_religious_belief_and_by_municipality_size_groups/$File/PVCR071_ENG.pdf%7Ctitle=) Population by religious belief and by municipality size groups |
| |  | 63                      | \[2\]\[3\]                                                                                                                                                                                                                                                                                                                                                                                                                              |
| |  | 61                      | \[4\]                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| |  | 59                      | \[5\]\[6\]\[7\]                                                                                                                                                                                                                                                                                                                                                                                                                         |
| |  | 56                      | \[8\]\[9\]                                                                                                                                                                                                                                                                                                                                                                                                                              |
| |  | 54                      | \[10\]                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| |  | 52                      | \[11\]\[12\]\[13\]                                                                                                                                                                                                                                                                                                                                                                                                                      |
| |  | 52                      | \[14\]                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| |  | 51                      | \[15\]                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| |  | 50.5                    | \[16\]\[17\]\[18\]                                                                                                                                                                                                                                                                                                                                                                                                                      |
| |  | 47                      | \[19\]                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| |  | 44                      | \[20\]                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| |  | 43.8                    | \[21\]                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| |  | 43.5                    | \[22\]                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| |  | 43                      | \[23\]\[24\]                                                                                                                                                                                                                                                                                                                                                                                                                            |
| |  | 42.9                    | \[25\]                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| |  | 42.6                    | \[26\]                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| |  | 42.4                    | \[27\]                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| |  | 42                      | \[28\]                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| |  | 41.9                    | \[29\]                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| |  | 40.6                    | \[30\]                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| |  | 39                      | \[31\]                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| |  | 35.4                    | \[32\]                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| |  | 29.9                    | \[33\]                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| |  | 29.9                    | \[34\]                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| |  | 25.0                    | \[35\]                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| |  | 23.9                    | \[36\]                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| |  | 23.3                    | \[37\]                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| |  | 23.1                    | \[38\]                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| |  | 22.8                    | \[39\]                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| |  | 22.3                    | \[40\]                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| |  | 21.4                    | \[41\]                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| |  | 20.6                    | \[42\]                                                                                                                                                                                                                                                                                                                                                                                                                                  |

<table>
<thead>
<tr class="header">
<th><p>國家</p></th>
<th><p>無宗教人口比例 (少于20%者)（2012）</p></th>
<th><p>日期及資料</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>|</p></td>
<td><p>19.4</p></td>
<td><p>[43]</p></td>
</tr>
<tr class="even">
<td><p>|</p></td>
<td><p>18.3</p></td>
<td><p>[44]</p></td>
</tr>
<tr class="odd">
<td><p>|</p></td>
<td><p>17.8</p></td>
<td><p>[45]</p></td>
</tr>
<tr class="even">
<td><p>|</p></td>
<td><p>16.0</p></td>
<td><p>[46]</p></td>
</tr>
<tr class="odd">
<td><p>|</p></td>
<td><p>15.7</p></td>
<td><p>[47]</p></td>
</tr>
<tr class="even">
<td><p>|</p></td>
<td><p>15.6</p></td>
<td><p>[48]</p></td>
</tr>
<tr class="odd">
<td><p>|</p></td>
<td><p>15.1</p></td>
<td><p>[49]</p></td>
</tr>
<tr class="even">
<td><p>|</p></td>
<td><p>14.6</p></td>
<td><p>[50]</p></td>
</tr>
<tr class="odd">
<td><p>|</p></td>
<td><p>13.2</p></td>
<td><p>[51]</p></td>
</tr>
<tr class="even">
<td><p>|</p></td>
<td><p>12.2</p></td>
<td><p>[52]</p></td>
</tr>
<tr class="odd">
<td><p>|</p></td>
<td><p>11.4</p></td>
<td><p>[53]</p></td>
</tr>
<tr class="even">
<td><p>|</p></td>
<td><p>11.3</p></td>
<td><p>[54]</p></td>
</tr>
<tr class="odd">
<td><p>|</p></td>
<td><p>11.1</p></td>
<td><p>[55]</p></td>
</tr>
<tr class="even">
<td><p>|</p></td>
<td><p>11.1</p></td>
<td><p>[56]</p></td>
</tr>
<tr class="odd">
<td><p>|</p></td>
<td><p>10.9</p></td>
<td><p>[57]</p></td>
</tr>
<tr class="even">
<td><p>|</p></td>
<td><p>9.0</p></td>
<td><p>[58]</p></td>
</tr>
<tr class="odd">
<td><p>|</p></td>
<td><p>8.0</p></td>
<td><p>[59]</p></td>
</tr>
<tr class="even">
<td><p>|</p></td>
<td><p>7.9</p></td>
<td><p>[60]</p></td>
</tr>
<tr class="odd">
<td><p>|</p></td>
<td><p>7.0</p></td>
<td><p><ref></p>
<ul>
<li><p></ref></p></li>
</ul></td>
</tr>
<tr class="even">
<td><p>|</p></td>
<td><p>7.0</p></td>
<td><p>[61]</p></td>
</tr>
<tr class="odd">
<td><p>|</p></td>
<td><p>6.6</p></td>
<td><p>[62]</p></td>
</tr>
<tr class="even">
<td><p>|</p></td>
<td><p>6.0</p></td>
<td><p>[63]</p></td>
</tr>
<tr class="odd">
<td><p>|</p></td>
<td><p>5.8</p></td>
<td><p>[64]</p></td>
</tr>
<tr class="even">
<td><p>|</p></td>
<td><p>4.7</p></td>
<td><p>[65]</p></td>
</tr>
<tr class="odd">
<td><p>|</p></td>
<td><p>4.6</p></td>
<td><p>[66]</p></td>
</tr>
<tr class="even">
<td><p>|</p></td>
<td><p>4.0</p></td>
<td><p>[67]</p></td>
</tr>
<tr class="odd">
<td><p>|</p></td>
<td><p>3.0</p></td>
<td><p>[68]</p></td>
</tr>
<tr class="even">
<td><p>|</p></td>
<td><p>2.5</p></td>
<td><p>[69]</p></td>
</tr>
<tr class="odd">
<td><p>|</p></td>
<td><p>2.4</p></td>
<td><p>[70]</p></td>
</tr>
<tr class="even">
<td><p>|</p></td>
<td><p>1.7</p></td>
<td><p>[71]</p></td>
</tr>
<tr class="odd">
<td><p>|</p></td>
<td><p>1.3</p></td>
<td><p>[72]</p></td>
</tr>
<tr class="even">
<td><p>|</p></td>
<td><p>1.1</p></td>
<td><p>[73]</p></td>
</tr>
<tr class="odd">
<td><p>|</p></td>
<td><p>1.1</p></td>
<td><p>[74]</p></td>
</tr>
<tr class="even">
<td><p>|</p></td>
<td><p>0.7</p></td>
<td><p>[75]</p></td>
</tr>
<tr class="odd">
<td><p>|</p></td>
<td><p>0.7</p></td>
<td><p>[76]</p></td>
</tr>
<tr class="even">
<td><p>|</p></td>
<td><p>0.27</p></td>
<td><p>[77]</p></td>
</tr>
<tr class="odd">
<td><p>|</p></td>
<td><p>0.1</p></td>
<td><p>[78]</p></td>
</tr>
</tbody>
</table>

## 参考文献

## 參見

  - [反宗教](../Page/反宗教.md "wikilink")

  - [無神論](../Page/無神論.md "wikilink")

  - [不可知論](../Page/不可知論.md "wikilink")

  -
{{-}}

[無宗教](../Category/無宗教.md "wikilink")

1.
2.
3.  [Dentsu Communication Institute Inc., Research Centre for Japan
    (2006)](http://www2.ttcn.ne.jp/~honkawa/9460.html)

4.
5.

6.   [Religionen in Deutschland:
    Mitgliederzahlen](http://www.remid.de/remid_info_zahlen.htm)
    Religionswissenschaftlicher Medien- und Informationsdienst; October
    31, 2009. Retrieved on November 19, 2009.

7.  [REMID Data of "Religionswissenschaftlicher Medien- und
    Informationsdienst"](http://www.remid.de/remid_info_zahlen.htm)
    Retrieved on January 16, 2015.

8.
9.  Knippenberg, Hans "The Changing Religious Landscape of Europe"
    edited by Knippenberg published by Het Spinhuis, Amsterdam 2005 ISBN
    90-5589-248-3, page 92

10. [Zuckerman, Phil. "Atheism: Contemporary Rates and Patterns", from
    the *Cambridge Companion to Atheism*, edited by Michael Martin,
    University of Cambridge Press,
    2007](http://www.pitzer.edu/academics/faculty/zuckerman/Ath-Chap-under-7000.pdf)


11.  US Department of State - International religious freedom report
    2006

12.

13.  Some publications

14.
15.  Publications are taken from Gallup

16.
17.
18.  Some publications

19.

20.
21.
22.
23.
24. According to figures compiled by the South Korean
    [](../Page/National_Statistical_Office_\(South_Korea\).md "wikilink").


25.
26.
27.
28.

29.

30.
31.

32.
33.
34.
35.

36.  Canada 2011 census

37. [1](http://datos.cis.es/pdf/Es2927mar_A.pdf) Socialogical Research
    Centre, January 2012

38.
39.

40. ["Census shows result of mining boom, with increased cost of housing
    and higher
    wages"](http://www.theaustralian.com.au/national-affairs/jump-in-people-identifying-as-indigenous/story-fn59niix-1226404041200),
    PIA AKERMAN, The Australian, 21 June 2012.

41.

42. ["Pew Research
    Center"](http://www.globalreligiousfutures.org/countries/botswana/religious_demography#/?affiliations_religion_id=0&affiliations_year=2010),
    Accessed 23 March 2016.

43.
44. [The Latin American Socio-Religious Studies Program / Programa
    Latinoamericano de Estudios Sociorreligiosos
    (PROLADES)](http://www.prolades.com/) PROLADES Religion in America
    by country

45.
46.  Gallup-Argentina survey

47.

48.  Gallup-Belize survey

49.  Güney Afrika 2001 census

50.

51.
52.
53.
54. [International Religious Freedom Report 2008: Costa
    Rica](http://www.state.gov/g/drl/rls/irf/2008/108520.htm). United
    States [Bureau of Democracy, Human Rights and
    Labor](../Page/Bureau_of_Democracy,_Human_Rights_and_Labor.md "wikilink")
    (September 14, 2007). *This article incorporates text from this
    source, which is in the [public
    domain](../Page/public_domain.md "wikilink").*

55.
56.
57.
58.
59.

60.  [El 80% de los ecuatorianos afirma ser católico, según el
    INEC](http://www.eluniverso.com/2012/08/15/1/1382/80-ecuatorianos-afirma-ser-catolico-segun-inec.html)

61.
62.
63.
64.
65.
66.
67.
68.

69.
70.
71.
72.
73.
74.
75.
76.
77.

78.