**塞馬湖**（）是[芬蘭最大的湖泊](../Page/芬蘭.md "wikilink")，也是[歐洲第四大自然淡水湖泊](../Page/歐洲.md "wikilink")，位於芬蘭的东南部。它是在[大冰期后期由](../Page/大冰期.md "wikilink")[冰川溶化而形成的](../Page/冰川.md "wikilink")。湖岸边的主要城市有[拉彭蘭塔](../Page/拉彭蘭塔.md "wikilink")、[伊馬特拉](../Page/伊馬特拉.md "wikilink")、[萨翁林纳](../Page/萨翁林纳.md "wikilink")、[米凱利](../Page/米凱利.md "wikilink")、[瓦尔考斯和](../Page/瓦尔考斯.md "wikilink")[約恩蘇](../Page/約恩蘇.md "wikilink")。[武克希河从塞马湖流出并注入到](../Page/武克希河.md "wikilink")[拉多加湖](../Page/拉多加湖.md "wikilink")。湖面上岛屿众多，狭窄的水道链接着湖泊的众多部分，每个部分有着自己的名字。由此，塞马湖体现了芬兰所有主要湖泊类型，并且水体富营养化程度不一。

从[拉彭蘭塔到](../Page/拉彭蘭塔.md "wikilink")[维堡的](../Page/维堡.md "wikilink")连接着塞马湖和[芬兰湾](../Page/芬兰湾.md "wikilink")。其它一些运河把塞马湖和芬兰东部的小型湖泊连在一起，并形成一套水道网络。这些水道主要用于运输木材、矿物、金属原材料、木浆和其它货物，但是游客也会使用这些水道。

塞馬湖也是世界上三種淡水[海豹之一](../Page/海豹.md "wikilink")[塞馬環斑海豹的唯一棲息地](../Page/塞馬環斑海豹.md "wikilink")。生活在塞马湖的另一个濒危物种是塞马[鲑鱼](../Page/鲑鱼.md "wikilink")\[1\]。

[File:GreaterSaimaa.gif|塞馬湖的衛星圖片(左上角處)](File:GreaterSaimaa.gif%7C塞馬湖的衛星圖片\(左上角處\))，[芬蘭灣在照片底部](../Page/芬蘭灣.md "wikilink")，右方則為[拉多加湖](../Page/拉多加湖.md "wikilink")，中間黑線是[俄羅斯與](../Page/俄羅斯.md "wikilink")[芬蘭的邊界](../Page/芬蘭.md "wikilink")
<File:Noctilucent> clouds over saimaa.jpg|在塞马湖上空发生的夜光云

## 参考文献

## 外部链接

  - 这就是芬兰：[湖区居民冬季向濒危海豹伸出援手](https://finland.fi/zh/shenghuoyushehui/huqujumindongjixiangbinweihaibaoshenchuyuanshou/)
  - [Visit Saimaa：塞马湖区官方旅游指南](http://www.visitsaimaa.fi/)
  - [Go Saimaa：另一个湖区旅游指南](https://www.gosaimaa.com/en)

[Category:芬蘭湖泊](../Category/芬蘭湖泊.md "wikilink")

1.