## 大事记

  - **[中国](../Page/中国.md "wikilink")**
      - 二月，[慕容儁命](../Page/慕容儁.md "wikilink")[慕容垂領兵伐](../Page/慕容垂.md "wikilink")[後趙](../Page/後趙.md "wikilink")，三月燕軍攻下[薊城](../Page/薊城.md "wikilink")。
      - [冉閔發動](../Page/冉閔.md "wikilink")[後趙漢人屠殺胡人](../Page/後趙.md "wikilink")，「一日之中，斬首數萬。閔親帥趙人以誅胡、羯，無貴賤、男女、少長皆斬之，死者二十餘萬，屍諸城外，悉爲野犬豺狼所食。其屯戍四方者，閔皆以書命趙人爲將帥者誅之，或高鼻多鬚濫死者半。」\[1\]
      - 冉闵建立[冉魏](../Page/冉魏.md "wikilink")。
      - [石虎之子](../Page/石虎.md "wikilink")[石祗聽說其兄皇帝](../Page/石祗.md "wikilink")[石鑒被](../Page/石鑒.md "wikilink")[冉閔殺死](../Page/冉閔.md "wikilink")，於是在襄國（今河北省邢台市）自立為帝，並起兵討伐[冉閔](../Page/冉閔.md "wikilink")。
      - 投降[東晉的](../Page/東晉.md "wikilink")[苻洪在](../Page/苻洪.md "wikilink")[後趙大亂時試圖謀取中原](../Page/後趙.md "wikilink")。最終雖然遭毒殺，其子[苻健成功奪下關中](../Page/苻健.md "wikilink")，建國[前秦](../Page/前秦.md "wikilink")，與[東晉斷絕](../Page/東晉.md "wikilink")。
      - [東晉朝廷決定乘](../Page/東晉.md "wikilink")[後趙大亂而收復](../Page/後趙.md "wikilink")[中原和](../Page/中原.md "wikilink")[關中地區](../Page/關中.md "wikilink")，任命[殷浩為中軍將軍](../Page/殷浩.md "wikilink")、假節、都督[揚](../Page/揚州.md "wikilink")[豫](../Page/豫州.md "wikilink")[徐](../Page/徐州.md "wikilink")[兗](../Page/兗州.md "wikilink")[青五州諸軍事](../Page/青州.md "wikilink")。
      - 11月，[冉魏武悼天王](../Page/冉魏.md "wikilink")[冉閔率軍圍攻](../Page/冉閔.md "wikilink")[後趙都城](../Page/後趙.md "wikilink")[襄國](../Page/襄國.md "wikilink")，爆發[襄國之戰](../Page/襄國之戰.md "wikilink")。
  - **[羅馬帝國](../Page/羅馬帝國.md "wikilink")**
      - 馬格嫩提烏斯在西部作亂，自己稱帝，並且誅殺[君士坦斯一世](../Page/君士坦斯一世.md "wikilink")，不久以後，[君士坦提烏斯二世揮兵西進](../Page/君士坦提烏斯二世.md "wikilink")，平定了馬格嫩提烏斯的叛亂，暫時結束帝國分裂，成為唯一的[奧古斯都](../Page/奧古斯都.md "wikilink")。

## 出生

  -
## 逝世

  - [石鑒](../Page/石鑒.md "wikilink")，[後趙君主](../Page/後趙.md "wikilink")。
  - [苻洪](../Page/苻洪.md "wikilink")，[前秦開國君主](../Page/前秦.md "wikilink")[苻健之父](../Page/苻健.md "wikilink")。
  - [褚裒](../Page/褚裒.md "wikilink")，[東晉](../Page/東晉.md "wikilink")[外戚](../Page/外戚.md "wikilink")，官至征北大將軍、徐兗二州刺史。
  - [李農](../Page/李農.md "wikilink")，[十六國時](../Page/十六國.md "wikilink")[後趙大臣](../Page/後趙.md "wikilink")。
  - [君士坦斯一世](../Page/君士坦斯一世.md "wikilink")，[羅馬帝國](../Page/羅馬帝國.md "wikilink")[君士坦丁王朝](../Page/君士坦丁王朝.md "wikilink")[皇帝](../Page/皇帝.md "wikilink")（337年至350年在位），也是[君士坦丁一世之子](../Page/君士坦丁一世_\(罗马帝国\).md "wikilink")。

## 參考資料

[\*](../Category/350年.md "wikilink")
[0年](../Category/350年代.md "wikilink")
[5](../Category/4世纪各年.md "wikilink")

1.  《資治通鑑》-{卷}-九十八