[HK_CWB_Hennessy_Road_n_Lee_Garden_Road_n_Hsin_Chong_Construction_Group_HCCG_2008-09_a.jpg](https://zh.wikipedia.org/wiki/File:HK_CWB_Hennessy_Road_n_Lee_Garden_Road_n_Hsin_Chong_Construction_Group_HCCG_2008-09_a.jpg "fig:HK_CWB_Hennessy_Road_n_Lee_Garden_Road_n_Hsin_Chong_Construction_Group_HCCG_2008-09_a.jpg")前[興利中心](../Page/興利中心.md "wikilink")（[希慎廣場地基工程](../Page/希慎廣場.md "wikilink")）地盤\]\]

**新昌集團**（，）是[香港一家建築公司](../Page/香港.md "wikilink")、[上市公司](../Page/上市公司.md "wikilink")，曾名為**新昌營造**，由已故[葉庚年先生於](../Page/葉庚年.md "wikilink")1939年獨資創立，現任主席為林春彪。公司在[英國](../Page/英國.md "wikilink")[百慕達註冊](../Page/百慕達.md "wikilink")，香港總部位於[九龍東](../Page/九龍東.md "wikilink")[牛頭角](../Page/牛頭角.md "wikilink")[偉業街新昌中心](../Page/偉業街.md "wikilink")。\[1\]

## 歷史

  - 2007年，[朱樹豪旗下全資附屬公司Carrick](../Page/朱樹豪.md "wikilink") Worldwide
    Limited，以每股作價1.78元收購新昌營造約61.67%股權，交易總作價約為7.042億元。\[2\]
  - 2010年年底，未完成合約的總額達到100億港元，年度收益為42億港元。\[3\]
  - 2011年7月，新昌營造公佈以32.48億港元向副主席[王英偉收購](../Page/王英偉.md "wikilink")[遼寧](../Page/遼寧.md "wikilink")[鐵嶺及](../Page/鐵嶺.md "wikilink")[江西](../Page/江西.md "wikilink")[青城兩幅土地](../Page/青城.md "wikilink")。\[4\]
  - 2012年，新昌營造公佈 [王英偉](../Page/王英偉.md "wikilink") 荣升为主席。
  - 2014年，新昌採用新標誌。
  - 2014年5月，新昌營造公佈以106.25億港元收購[廣東省](../Page/廣東省.md "wikilink")[佛山市一幅土地](../Page/佛山市.md "wikilink")。\[5\]同月，Carrick
    Worldwide Limited
    向天物投資旗下全資附屬公司瑞安投資出售其持有全部的3.76億股新昌營造股份，相當於新昌營造13.15%股權。天物投資是[天津物產集團之全資附屬公司](../Page/天津物產集團有限公司.md "wikilink")。交易完成後，天津物產集團共持有新昌營造17.98%股權。\[6\]
  - 2016年4月，改名為新昌集團。\[7\]以高達100億元港元，先後收購了內地4個房地産項目，但經過狙擊後，股價一日直插30%。公司由2015年度佔盈利24.46億港元，變成巨額虧損27.34億港元。\[8\]
  - 2017年4月，公佈年度業績前停牌\[9\]，至今仍未復牌。\[10\]又宣佈以7.6億港元出售香港觀塘偉業街的新昌寫字樓辦公室和北京、廣州的發展物業。
  - 2017年陷入財困後，新昌雖奪得多個政府工程項目，但財政狀況再度惡化，在2018年8月，有員工聲稱至今仍未收到7月份工資。公司車輛續牌的兩張共約1萬元的支票被彈票。\[11\]同月，香港[西九管理局解除與新昌集團的工程合約](../Page/西九管理局.md "wikilink")，包括[M+需要停工](../Page/M+.md "wikilink")，工地暫時關閉，當局需重新招標另覓新承建商。\[12\]\[13\]

## 承包工程項目

  - [澳門](../Page/澳門.md "wikilink")[威尼斯人度假村](../Page/威尼斯人度假村.md "wikilink")
  - [香港今旅](../Page/香港今旅.md "wikilink")
  - 尖沙咀[喜來登酒店](../Page/喜來登酒店.md "wikilink")
  - [利景酒店](../Page/利景酒店.md "wikilink")
  - [香港酒店](../Page/香港酒店.md "wikilink")
  - [博愛醫院](../Page/博愛醫院.md "wikilink")
  - [北區醫院](../Page/北區醫院.md "wikilink")
  - [聯合醫院](../Page/聯合醫院.md "wikilink")
  - [廣華醫院](../Page/廣華醫院.md "wikilink")
  - [葛量洪醫院](../Page/葛量洪醫院.md "wikilink")
  - [美孚新邨](../Page/美孚新邨.md "wikilink")
  - [沙田](../Page/沙田.md "wikilink")[美林邨](../Page/美林邨.md "wikilink")
  - [沙田](../Page/沙田.md "wikilink")[穗禾苑](../Page/穗禾苑.md "wikilink")
  - [天水圍](../Page/天水圍.md "wikilink")[天盛苑A](../Page/天盛苑.md "wikilink")、B、G、P、Q、R六座
  - [啟德機場客運大樓及貨運大樓](../Page/啟德機場.md "wikilink")（已拆）
  - [船灣淡水湖供水計劃](../Page/船灣淡水湖.md "wikilink")
  - [昂船洲大橋](../Page/昂船洲大橋.md "wikilink")
  - [九廣鐵路](../Page/九廣鐵路.md "wikilink")[紅磡站重建](../Page/紅磡站.md "wikilink")
  - [海洋公園](../Page/海洋公園.md "wikilink")
  - [香港理工大學](../Page/香港理工大學.md "wikilink")
  - [香港浸會大學大學會堂](../Page/香港浸會大學.md "wikilink")
  - [香港科技大學吳家瑋學術廊第二期及鄭裕彤樓](../Page/香港科技大學.md "wikilink")
  - [職業訓練局總部大樓](../Page/職業訓練局.md "wikilink")
  - [澳門金沙](../Page/澳門金沙.md "wikilink")
  - [數碼港](../Page/數碼港.md "wikilink")
  - [香港中文大學](../Page/香港中文大學.md "wikilink")[新亞書院誠明館](../Page/新亞書院.md "wikilink")、人文館及錢穆圖書館，[善衡書院](../Page/善衡書院.md "wikilink")、[晨興書院及](../Page/晨興書院.md "wikilink")[和聲書院](../Page/和聲書院.md "wikilink")
  - [太古坊](../Page/太古坊.md "wikilink")[康橋大廈](../Page/康橋大廈.md "wikilink")
  - [中環](../Page/中環.md "wikilink")[環球大廈](../Page/環球大廈.md "wikilink")
  - 銅鑼灣[香港世貿中心](../Page/香港世貿中心.md "wikilink")
  - [嘉里中心](../Page/嘉里中心.md "wikilink")
  - 高鐵 [西九龍總站](../Page/西九龍總站.md "wikilink")（南）
  - 澳門 [銀河娛樂渡假村第二期](../Page/銀河娛樂渡假村.md "wikilink")
  - [沙田至中環線合約編號](../Page/沙田至中環線.md "wikilink")1109號兩個港鐵站，及1.6公里鐵路隧道
  - 啟德[德朗邨](../Page/德朗邨.md "wikilink")（與有利建築合營）
  - 沙田[美田邨美全樓](../Page/美田邨.md "wikilink")
  - 前已婚人員宿舍之公共房屋發展項目
  - [軒尼詩道](../Page/軒尼詩道.md "wikilink")28號商業項目
  - [香港科學園第三期](../Page/香港科學園.md "wikilink")（部分）
  - [民航處新總部](../Page/民航處.md "wikilink")
  - [佐敦谷箱形雨水渠污水截流工程](../Page/佐敦谷.md "wikilink")
  - 中環[百子里活化工程](../Page/百子里.md "wikilink")
  - 大家樂新建食品工廠
  - [石門滙豐大廈](../Page/石門_\(香港\).md "wikilink")
  - [NTT將軍澳工業邨金融數據中心](../Page/日本電信電話.md "wikilink")60%
  - 將軍澳匯豐銀行數據中心
  - 中國移動數據中心
  - [九廣鐵路公司大樓](../Page/火炭鐵路大樓.md "wikilink")

## 參考

[CATEGORY:中國房地產開發公司](../Page/CATEGORY:中國房地產開發公司.md "wikilink")

## 外部連結

  - [新昌集團控股有限公司](http://www.hsinchong.com/zh-HK/)

[Category:香港建築公司](../Category/香港建築公司.md "wikilink")
[Category:1939年成立的公司](../Category/1939年成立的公司.md "wikilink")

1.  [新昌營造集團有限公司
    (0404.HK)](http://hk.biz.yahoo.com/p/hk/profile/index.html?s=0404.HK)

2.  [新昌营造获骏豪主席7亿元收购61.67%股权](http://finance.sina.com.cn/stock/hkstock/ggscyd/20071102/10064132126.shtml)
    財華社，2007年11月2日
3.  <http://www.hsinchong.com/Content/Uploads/ar2010-0e5f6bcb-c064-42bb-bf84-0cf3ae37422a.pdf>
4.  [交易爭議 新昌半月瀉逾三成 購副主席王英偉兩地
    地價一年漲兩倍](http://www.mpfinance.com/htm/Finance/20110817/News/ec_ech1.htm)
    明報，2011年8月17日
5.  [新昌106億購佛山土地](http://news.takungpao.com.hk/paper/q/2014/0516/2479473.html)
    大公報，2014年5月16日
6.  [新昌營造：天津物產集團持股增至17.98%成單一最大股東](http://www.quamnet.com/newscontent.action;jsessionid=788F2059ACE8338C97C6FECE8980F354.node2?articleId=3619947)
    匯港通訊，2014年5月27日
7.  [歷史和里程碑](http://www.hsinchong.com/zh-HK/Pages/history-and-milestones/2010-2019)
8.
9.  [港交所上市公司訊息](http://www.hkexnews.hk/listedco/listconews/sehk/2017/0403/LTN20170403104_C.HTM)香港交易所，2017年4月3日
10. [香港交易所
    投資服務中心](https://www.hkex.com.hk/chi/invest/company/quote_page_c.asp?WidCoID=0404)
11.
12.
13.