**布達佩斯戰役**是[第二次世界大戰結束前](../Page/第二次世界大戰.md "wikilink")[蘇軍在](../Page/蘇聯紅軍.md "wikilink")[布達佩斯攻勢中圍攻](../Page/布達佩斯攻勢.md "wikilink")[匈牙利](../Page/匈牙利.md "wikilink")[首都](../Page/首都.md "wikilink")[布達佩斯的戰役](../Page/布達佩斯.md "wikilink")，围困长达50天。

圍攻最早開始於1944年12月26日蘇軍包圍了當時由[德意志國防軍及](../Page/德意志國防軍.md "wikilink")[匈牙利軍防守的布達佩斯](../Page/匈牙利軍.md "wikilink")，守軍在1945年2月13日無條件投降，進攻的蘇軍是由[羅季翁·雅科夫列維奇·馬利諾夫斯基指揮的](../Page/羅季翁·雅科夫列維奇·馬利諾夫斯基.md "wikilink")[烏克蘭第2方面軍](../Page/草原方面軍.md "wikilink")，守軍包括了德意志國防軍（Wehrmacht
Heer）、[武裝親衛隊及](../Page/武裝親衛隊.md "wikilink")[匈牙利軍](../Page/匈牙利.md "wikilink")（Honved*或*Honvédség）。

围城期间约有38,000名平民死于饥饿和军事行动，布達佩斯戰役是第二次世界大戰中最血腥的圍城戰之一，此次围城是盟军向柏林推进过程中的一场策略性的胜利。

## 一般情況

1944年匈牙利變成[納粹德國的](../Page/納粹德國.md "wikilink")“有離心的衛星國”，同年3月匈牙利希望退出戰爭，德國視匈牙利這樣是阻撓他們反對[猶太人的行徑](../Page/猶太人.md "wikilink")，而德國亦極其需要匈牙利在[巴拉頓湖附近的](../Page/巴拉頓湖.md "wikilink")[油田](../Page/油田.md "wikilink")，3月19日德國實施[馬格麗特行動](../Page/馬格麗特行動.md "wikilink")，大批德軍（[德意志國防軍](../Page/德意志國防軍.md "wikilink")）進駐匈牙利，匈牙利[攝政王](../Page/攝政王.md "wikilink")[霍爾蒂·米克洛什](../Page/霍爾蒂·米克洛什.md "wikilink")[海軍上將停止嘗試將匈牙利退出戰爭](../Page/海軍上將.md "wikilink")。

到10月霍爾蒂再次嘗試與[同盟國談判爭取和平](../Page/同盟國.md "wikilink")，但10月16日德國實施[鐵拳行動及迫令霍爾蒂退位](../Page/鐵拳行動.md "wikilink")，霍爾蒂和他的政府由[箭十字黨的](../Page/箭十字黨.md "wikilink")[法西斯領袖](../Page/法西斯.md "wikilink")[薩拉西·費倫茨代替](../Page/萨拉希·费伦茨.md "wikilink")。

## 圍城

### 包圍布達佩斯

1944年10月29日[蘇聯紅軍開始進攻布達佩斯](../Page/蘇聯紅軍.md "wikilink")，超過1百萬人分成兩個機動集團向該城進發，計劃將布達佩斯與餘下的德國及匈牙利軍隊分割，11月7日蘇聯軍隊進入布達佩斯舊城區20英哩的郊區，12月19日經過休整後紅軍恢復進攻，12月26日蘇聯軍隊切斷了從布達佩斯到[維也納的公路及包圍布達佩斯](../Page/維也納.md "wikilink")。

當蘇軍會合時，接近33,000名德國及37,000名匈牙利士兵，還有超過800,000名平民仍然留在城內，在拒絕授權撤退後，[阿道夫·希特勒宣佈布達佩斯是一個要塞城市](../Page/阿道夫·希特勒.md "wikilink")（
Budapest）及被防守至最後一人。

布達佩斯是[約瑟夫·史達林的主要目標](../Page/約瑟夫·史達林.md "wikilink")，當時[雅爾達會議正準備召開](../Page/雅爾達會議.md "wikilink")，史達林希望向[溫斯頓·邱吉爾及](../Page/溫斯頓·邱吉爾.md "wikilink")[羅斯福展示他的強大力量](../Page/富蘭克林·德拉諾·羅斯福.md "wikilink")，因此他命令[羅季翁·雅科夫列維奇·馬利諾夫斯基將軍儘快攻佔該城](../Page/羅季翁·雅科夫列維奇·馬利諾夫斯基.md "wikilink")。

12月29日馬利諾夫斯基派出兩名特使，希望談判城市的投降，但特使沒有回來，這一點在蘇聯國內引起廣泛討論，一些德國及匈牙利的歷史學家爭論特使被故意地槍斃，其他人相信他們是被流彈擊中身亡，無論如何，蘇聯指揮官認為這是拒絕勸降及命令開始圍攻。

### 開始圍攻及德軍首次進攻

蘇軍的進攻從東面的郊區展開，通過[佩斯城](../Page/佩斯_\(匈牙利\).md "wikilink")，利用它中央區域龐大的道路網快速擴展攻勢，德國及匈牙利守軍被壓倒，他們嘗試用空間換取時間，以減緩蘇軍進攻的速度，他們最終後退以縮短戰線，希望利用[布達城的山區地形來防守](../Page/布達.md "wikilink")。

1945年1月開始德軍先後組織了3次代號為，康拉德行動是德國及匈牙利聯軍解救被圍的布達佩斯守軍。

1月1日**康拉德I號行動**被展開，德軍[武裝親衛隊第4裝甲軍從](../Page/武裝親衛隊第4裝甲軍.md "wikilink")[塔塔市通過布達佩斯以突破蘇軍包圍](../Page/塔塔.md "wikilink")，同時[武裝親衛隊部隊從布達佩斯西面攻擊以取得優勢](../Page/武裝親衛隊.md "wikilink")，1月3日蘇軍命令增調4個師以解除威脅，終於在布達佩斯北面20公哩的[比克斯克阻止了德軍的進攻](../Page/比克斯克.md "wikilink")，1月12日德軍被迫撤退。

1月7日德軍實施**康拉德II號行動**，德軍武裝親衛隊第4裝甲軍從[艾斯特根向布達佩斯發起進攻](../Page/艾斯特根.md "wikilink")，試圖攻佔機場以改善城市的空中運輸供應，但反攻在機場附近被阻止。

1月17日最後一部份的康拉德行動****被實施，德軍武裝親衛隊第4裝甲軍及[德軍第3裝甲軍從南面進攻及試圖包圍蘇軍](../Page/德軍第3裝甲軍.md "wikilink")10個師，但最終失敗。

### 戰事加劇

同時布達佩斯戰役巷戰密度大幅增加，物資供應變得十分重要，因為在圍城之前[費里海吉機場已在](../Page/費里海吉.md "wikilink")1944年12月27日失陷，直到1945年1月9日，雖然遭到蘇軍[高射炮攻擊](../Page/高射炮.md "wikilink")，德軍仍然可以使用一些主要街道及[布達城堡一帶的公園作為飛機及](../Page/布達城堡.md "wikilink")[滑翔機的降落場](../Page/滑翔機.md "wikilink")，在[多瑙河封涷前](../Page/多瑙河.md "wikilink")，一些物資能在黑夜及霧的掩護下被[駁船運入城中](../Page/駁船.md "wikilink")。

但是糧食短缺是十分平常及士兵需自己尋找口糧，有時候甚至殺食自己的馬匹，而嚴寒的天氣亦影響德國及匈牙利守軍。

很快蘇軍發現了自己處身於當日德軍在[史達林格勒同樣的情況](../Page/史達林格勒.md "wikilink")，但是他們仍然能在狙擊手及工兵的幫助下推進，戰鬥經常在[下水道發生](../Page/下水道.md "wikilink")，因為無論蘇軍及德軍均以它們來前進的，6個[蘇聯海軍士兵佔據了城堡山及在回到自己在地底的戰線前俘虜了一名德軍軍官](../Page/蘇聯海軍.md "wikilink")，但是這些事件很少發生，因為防禦系統是由軸心國軍建立的，他們在下水道裡有當地居民作為嚮導。

1月中[切貝爾島被攻佔](../Page/切貝爾島.md "wikilink")，島上有多間工廠，當時在蘇軍炮火下仍在生產[反坦克手雷發射器及](../Page/反坦克手雷發射器.md "wikilink")[發射體](../Page/發射體.md "wikilink")，同時在佩斯城，情況開始惡化，[遊擊隊面對防守區被蘇軍一分為二的危險](../Page/遊擊隊.md "wikilink")。

1945年1月17日希特勒接受將所有餘下守軍從佩斯城撤退到布達城的建議，所有5座位於多瑙河上的橋樑塞滿了撤退的民眾及軍隊，1月18日雖然匈牙利軍官反對，德軍最終炸毀了這5座漂亮的橋樑。

### 德軍第二次進攻

1945年1月20日德軍發起第2次主要進攻，這次他們在南面突破包圍圈，在城市南面打開了一個20公里的突破口及向蘇軍的供應基地[德布勒森進攻](../Page/德布勒森.md "wikilink")。

史達林命令其軍隊不惜一切代價堅守，兩個方面軍暫時停止圍攻及移向南面應付德軍的進攻，但德軍因疲勞及物資供應短缺而在城市外20公里被迫停止進攻，布達佩斯的守軍被蘇軍容許可離開城市及包圍圈，但希特勒拒絕。

1月28日德軍不能在維持戰線及被迫撤退，布達佩斯的守軍再度被封鎖。

### 布達戰役

[Russian_AA_Gunners_in_Budapest.jpg](https://zh.wikipedia.org/wiki/File:Russian_AA_Gunners_in_Budapest.jpg "fig:Russian_AA_Gunners_in_Budapest.jpg")
與興建在平原的佩斯城不同，布達城興建在山上，這令守軍能在進攻者頭上架設火炮及防禦工事，令蘇軍推進減慢，主要堡壘，[蓋勒山丘由武裝親衛隊防守](../Page/蓋勒山丘.md "wikilink")，他們數次擊退了蘇軍的進攻，在附近，蘇軍與德軍在市內墓園激戰，在墓碑的戰事持續了數天。

戰鬥在多瑙河中間的[瑪格麗特島特別殘酷](../Page/瑪格麗特島.md "wikilink")，該島與守軍餘下防守區域仍由餘下一半的瑪格麗特橋連接，它被用作傘降區及簡便機場供給小型飛機升降。

2月11日經過6個星期戰鬥，蘇軍從3個不同方向突破守軍防禦及勝利攻佔蓋勒山丘，從此蘇軍炮火範圍最終能覆蓋全城，剩下的軸心國守軍集中在少於2平方公里的範圍，他們大部份都是營養不良及染病，每天的配給口糧只有150克的麵包及宰殺馬匹而來的馬肉，但他們仍然拒絕投降及逐街逐屋防守，與蘇軍士兵及坦克作戰，這時一些匈牙利士兵（）已歸附於蘇軍向德軍及自己國人進攻。

經過兩天血戰後，蘇軍攻佔南部火車站，他們向城堡山進攻，2月10日，經過一輪交響樂式的炮轟，蘇軍在城堡山建立了橋頭堡，同時將守軍切成兩半。

### 德軍第3次進攻（或突圍）及投降

希特勒禁止德軍指揮官，武裝親衛隊上將放棄布達佩斯或嘗試突圍，不過空運物資供應已在數天前停止及空投物資，突圍亦不可能。

維登布魯赫決定不顧一切率領守軍突圍，正常地，德軍指揮官不需徵詢市內匈牙利指揮官的意見，非同尋常地，他與匈牙利指揮官將軍一同突圍。

2月11日晚上有25,000名德軍及匈牙利軍開始從城堡山走下來，他們分成3批，每一批裡有數千名平民，整個家庭推著嬰兒車，蹅著冰雪出來，不幸地，蘇軍正等待這些逃亡者。

軍隊及平民利用霧作掩護，第一批出來時令蘇軍士兵及炮兵目瞪口呆，因此有一些人逃脫，但第2批及第3批就沒有這麼幸運，蘇軍大炮及火箭彈飛向逃走區，造成多人死亡，雖然付出慘重代價，但5,000至10,000人安全逃至布達佩斯西北面的山林地帶及向維也納走去，大約7,000名德軍逃脫了。

[LancHid45.jpg](https://zh.wikipedia.org/wiki/File:LancHid45.jpg "fig:LancHid45.jpg")）橋墩\]\]
大部份試圖逃脫的人被殺、受傷或被蘇軍俘虜，維登布魯赫及欣迪亦被俘虜。

1945年2月13日剩餘的守軍最終投降，布達佩斯成為廢墟，超過80%的建築物損毀或被破壞，著名歷史建築物如[匈牙利國會大廈及城堡已被嚴重破壞](../Page/匈牙利國會大廈.md "wikilink")，所有5座橫跨多瑙河的橋樑亦被破壞。

德軍及匈牙利軍損失慘重，幾乎所有師團被消滅，最少德軍損失了整個或大部份[德軍第13裝甲師](../Page/德軍第13裝甲師.md "wikilink")、[統帥堂裝甲擲彈兵師](../Page/统帅堂装甲掷弹兵师.md "wikilink")、德軍武裝親衛隊[第8弗洛里安·蓋依騎兵師及](../Page/第八弗洛里安·蓋依騎兵師.md "wikilink")[第22瑪麗婭·特蕾西婭志願騎兵師](../Page/第二十二瑪麗婭·特蕾西婭志願騎兵師.md "wikilink")，匈牙利第1軍團包括第10步兵師、第12步兵師及第1裝甲師全部被消滅。

大約40,000名平民死亡，另有數不盡的因飢餓或染病而死，很多10歲至70歲的婦女被強姦\[1\]，在布達佩斯，估計有50,000名婦女被羅馬尼亞或蘇聯紅軍士兵強姦。

## 總結

除這年3月實施的[驚蟄行動](../Page/巴拉頓湖戰役.md "wikilink")（Unternehmen
Frühlingserwachen）外，圍攻布達佩斯是德軍在南部戰線的最後一次大型軍事行動，圍攻耗盡了德意志國防軍特別是武裝親衛隊的資源，對蘇軍來說，布達佩斯戰役是[柏林戰役前最後一次大型採排](../Page/柏林戰役.md "wikilink")，它亦令紅軍實施[維也納戰役](../Page/維也納戰役.md "wikilink")，1945年4月13日布達佩斯投降後剛好兩個月，維也納宣告淪陷。
[Budapest_medal.jpg](https://zh.wikipedia.org/wiki/File:Budapest_medal.jpg "fig:Budapest_medal.jpg")是授與所有曾參與布達佩斯戰役的蘇聯官兵\]\]

### 回憶錄及日記

有關第二次世界大戰的布達佩斯、依格多及Krisztinaváros的部份，是根據居民的回憶錄及日記而成的，László
Deseő，1944年時15歲與家人住在美莎露絲街32號，這地區是戰事最激烈的地區，因為它接近[宰部火車站及戰略上非常重要的山頭據點](../Page/宰部火車站.md "wikilink")，Deseő保留了他的日記記錄了整個圍城時期\[2\]，András
Németh的回憶錄亦描述了整個圍攻及轟炸空置的學校建築物，他和跟隨他的士兵在之前利用此地觀察敵人的動向\[3\]。

## 脚注

## 参考资料

  - John F. Montgomery, *Hungary: The Unwilling Satellite*. Devin-Adair
    Company, New York, 1947. Reprint: Simon Publications, 2002.
    Available online at [Historical Text
    Archive](https://web.archive.org/web/20070216070442/http://historicaltextarchive.com/books.php?op=viewbook&bookid=7&pre=1)
    and at the [Corvinus Library of Hungarian
    History](https://web.archive.org/web/20070701094833/http://www.hungarian-history.hu/lib/montgo/).
  - Gosztony, Peter: Der Kampf um Budapest, 1944/45, München : Schnell &
    Steiner, 1964.
  - Nikolai Shefov, *Russian fights*, Lib. Military History, M. 2002.
  - James Mark. Remembering Rape: Divided Social Memory and the Red Army
    in Hungary 1944–1945. Past and Present 2005: 188: 133-161 (Oxford
    University Press).
  - Krisztian Ungvary, *The Siege of Budapest: One Hundred Days in World
    War II* (trans. Peter Zwack), Yale University Press, 2005, ISBN
    0-300-10468-5
  - Source about soviet casualties, estimated at 80,000, not 160,000:
    <https://archive.is/20121221115318/http://www.victory.mil.ru/war/oper/15.html>

## 外部連結

  - [World War II: Siege of
    Budapest](https://web.archive.org/web/20070930201524/http://www.historynet.com/magazines/mhq/3033336.html)

[Category:第二次世界大戰歐洲戰場戰役](../Category/第二次世界大戰歐洲戰場戰役.md "wikilink")
[Category:德国战役](../Category/德国战役.md "wikilink")
[Category:匈牙利戰役](../Category/匈牙利戰役.md "wikilink")
[Category:蘇聯战役](../Category/蘇聯战役.md "wikilink")
[Category:德國參與的第二次世界大戰戰役和行動](../Category/德國參與的第二次世界大戰戰役和行動.md "wikilink")

1.  "The worst suffering of the Hungarian population is due to the rape
    of women. Rapes - affecting all age groups from ten to seventy are
    so common that very few women in Hungary have been spared." Swiss
    embassy report cited in Ungváry 2005, p.350.
2.  [Deseő László
    naplója](http://www.rev.hu/html/hu/tanulmanyok/1945elott/bpostroma.htm)
    （Hungarian）
3.  [Németh András –
    *Mostohafiak*](http://mek.oszk.hu/02800/02801/02801.htm#7)
    (Hungarian)