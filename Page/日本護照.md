**日本護照**是[日本國發行的](../Page/日本國.md "wikilink")[護照](../Page/護照.md "wikilink")，最早於1878年起開始發行。

## 歷史

### 日本本土

1878年2月20日，日本開始簽發第一套護照，稱為「[日本帝國海外旅券](../Page/日本帝國海外旅券.md "wikilink")」（依據1878年2月20日日本[外務省所製定的](../Page/外務省.md "wikilink")《海外旅券規則》所發行，由於是[日本帝國時期所制定的法令](../Page/日本帝國.md "wikilink")，因此稱為日本帝國海外旅券）。1917年起，護照附有個人[照片](../Page/照片.md "wikilink")。1918年，由於日本與[中國之間交通往來頻繁](../Page/中國.md "wikilink")，雙方決定容許對方國民毋須持護照出入境\[1\]。1926年，日本當局開始簽發現代設計的護照。

1992年，根據國際標準，日本當局發出可以讓機器讀取的「MRP旅券」。1995年11月1日，再根據國際標準，當局發出10年有效的護照給成年人，未成年人士繼續持有5年有效的護照。2004年，當局接受電子申請，由[岡山縣開始](../Page/岡山縣.md "wikilink")。2006年，當局開始簽發內藏[晶片](../Page/晶片.md "wikilink")，有生物特徵的護照。

日本政府於1998年將2月20日定為「旅券之日」，以紀念日本帝國海外旅券的發行。

### 台灣

1897年，[台灣割讓予日本後](../Page/台灣.md "wikilink")，大批日本[浪人經台灣前往中國](../Page/浪人.md "wikilink")，造成治安問題，因此日本當局宣布需要持有加註有台灣籍的日本旅券，才能出入台灣。

1907年，由於申請日本旅券手續過於繁複，[台灣總督府宣布](../Page/台灣總督府.md "wikilink")，前往[中國及](../Page/中國.md "wikilink")[香港的旅客](../Page/香港.md "wikilink")，可以自行決定是否申請日本旅券\[2\]。

另一方面，當時日本向台灣人發出海外旅券，但[清朝政府卻不承認其日本籍](../Page/清朝.md "wikilink")，使中日兩國當時很多紛爭\[3\]。

### 朝鮮半島

1907年，日本頒布《外國旅行規則》，宣布於[韓國人簽發日本旅券的方法](../Page/韓國.md "wikilink")。1910年，日本正式吞併韓國。之後作為日本國籍的朝鮮半島人出國自然亦使用日本護照，直到1945年日本戰敗投降。

## 护照类型

  - **普通护照**：发放对象为普通公民。
      - 普通护照有5年有效期和10年有效期两种版本。19岁以下（含19岁）的日本公民只能持有5年有效期护照，而满20岁的日本公民可选择持有5年有效期（[紺色](../Page/紺色.md "wikilink")）或是10年有效期（[红色](../Page/红色.md "wikilink")）的护照。
  - **公务护照**：发放对象为[议员和](../Page/议员.md "wikilink")[公务员](../Page/公务员.md "wikilink")。
  - **外交护照**：发放对象为[日本皇室](../Page/日本皇室.md "wikilink")、外交官及其家眷或是政府高级官员。
      - 由于二战后对天皇的法律规定，因此[日本天皇和日本](../Page/日本天皇.md "wikilink")[皇后并不持有护照](../Page/皇后.md "wikilink")。

所有在2006年3月20日以后发放的日本护照都是[生物识别护照](../Page/生物识别护照.md "wikilink")。

## 护照外观

日本护照的封面中央印有[日本國徽](../Page/菊花纹章.md "wikilink")「[菊花紋章](../Page/菊花紋章.md "wikilink")」，在其上方用[日文漢字](../Page/日文漢字.md "wikilink")（[篆體](../Page/篆書.md "wikilink")）印有「**」之字樣，同样的在其下方则寫有[英语字樣](../Page/英语.md "wikilink")「**」。5年有效期版本的普通护照为暗蓝色，10年有效期的则为[绯红色](../Page/绯红色.md "wikilink")。另外公务护照是暗绿色，外交护照则是暗褐色。

[Japanese_Passport_Information_page.jpg](https://zh.wikipedia.org/wiki/File:Japanese_Passport_Information_page.jpg "fig:Japanese_Passport_Information_page.jpg")

### 身份信息页

  - 护照持有人照片
  - 护照类型
  - 签发国
  - 护照编号
  - 姓氏*（括号内可加注[化名](../Page/化名.md "wikilink")）*
  - 名字*（括号内可加注[化名](../Page/化名.md "wikilink")）*
  - 国籍
  - 出生日期
  - 性别
  - 本籍
  - 护照签发日
  - 有效日期
  - 签发机构
  - 护照持有人签名

信息页以机读区结束。

### 护照声明

护照包含其签发国的声明，用来知会其它所有国家，证明持有人具有簽發国的公民身份，并且请求允许其过境，同时享有国际法所规定的待遇。日本护照内的声明如下:

[日语版本](../Page/日语.md "wikilink")：

  -

      -

[英语版本](../Page/英语.md "wikilink")：

  -

      -

[中文語意](../Page/中文.md "wikilink")：

  -

      -

### 语言

除了护照最末的注意事项（例如在十年期的生物识别护照的第51页）僅用日语标明外，日本护照完全使用[日语和](../Page/日语.md "wikilink")[英语两种语言](../Page/英语.md "wikilink")。这个声明包括了护照持有人在外国遇到各种情况时，所应该知道的事情。

姓、名及个人情况（例如出生日期）都只以拉丁大写字母标明。日语名字基本上都依据[平文式罗马字系统转换](../Page/平文式罗马字.md "wikilink")，在某些特殊情况下则例外，特别是当该人名是一个外国名字（例如配偶或子女为外国人时）的[片假名转译](../Page/片假名.md "wikilink")，在这种情况下可以使用该姓名的原始拉丁拼法（前提条件是需要递承政府颁发原始拼法的正式文件，例如护照等）。

在持有人签名处则标明了持有人姓名的日文写法。

## 免簽證入境

[Visa_requirements_for_Japanese_citizens.png](https://zh.wikipedia.org/wiki/File:Visa_requirements_for_Japanese_citizens.png "fig:Visa_requirements_for_Japanese_citizens.png")
根据亨利护照指数，截止2018年10月，日本护照获得免签证入境190个国家和地区的权利，登上世界护照指数排行首位。

## 參考資料

<references />

<https://www.henleyglobal.com/press-release-details/japanese-passport-is-the-strongest/>

## 參見

  - [護照](../Page/護照.md "wikilink")
  - [日本帝國海外旅券 (台灣)](../Page/日本帝國海外旅券_\(台灣\).md "wikilink")

[Category:日本外交](../Category/日本外交.md "wikilink")
[Japan](../Category/各國護照.md "wikilink")

1.  [外交史Q\&A，東京：外務省](http://www.mofa.go.jp/mofaj/annai/honsho/shiryo/qa/sonota_01.html)
2.  川島真著、鍾淑敏譯，《日本外務省外交史料館館藏台灣人出國護照相關資料之介紹－1897年至1934年》，1999年，台灣史研究學術專刊
3.  [日據時期的台灣籍民，北京：人民網，2005年](http://news.sina.com.cn/c/2005-10-24/09197246825s.shtml)