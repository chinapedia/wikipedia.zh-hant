[Major_Ground_Force_Units.png](https://zh.wikipedia.org/wiki/File:Major_Ground_Force_Units.png "fig:Major_Ground_Force_Units.png")
[Chengdu_Military_Region.svg](https://zh.wikipedia.org/wiki/File:Chengdu_Military_Region.svg "fig:Chengdu_Military_Region.svg")

**中国人民解放军成都军区**是[中国人民解放军曾有的](../Page/中国人民解放军.md "wikilink")[大軍區之一](../Page/大軍區.md "wikilink")。在中華人民共和國[深化国防和军队改革中](../Page/深化国防和军队改革.md "wikilink")，成都軍區於2016年1月隨其他大軍區一併撤銷。該軍區曾主管[四川](../Page/四川.md "wikilink")、[云南](../Page/云南.md "wikilink")、[贵州](../Page/贵州.md "wikilink")、[西藏](../Page/西藏自治区.md "wikilink")（不含[阿里地区](../Page/阿里地区.md "wikilink")）和[重庆等三省](../Page/重庆.md "wikilink")、一自治区、一直辖市军事事务的大军区，总部位于[成都市](../Page/成都市.md "wikilink")，司令部驻成都市江汉路北较场（原[中央军校旧址](../Page/中央军校.md "wikilink")），后勤部驻成都市通惠门西较场，成都军区空军司令部驻成都市新南门。

成都军区所属的[第13集团军是](../Page/第13集团军.md "wikilink")[中国人民解放军陆军集团军中唯一一个从](../Page/中国人民解放军陆军.md "wikilink")1950年代到21世纪参与军事行动从未间断过的军级单位。曾先后参与了[“解放”西南](../Page/西南战役.md "wikilink")、[西南“剿匪”](../Page/西南剿匪.md "wikilink")、[西藏平叛](../Page/1959年藏区骚乱.md "wikilink")、[中印战争](../Page/中印战争.md "wikilink")、[珍宝岛战役](../Page/珍宝岛战役.md "wikilink")、[中越边境战争以及在](../Page/中越边境战争.md "wikilink")[海地](../Page/海地.md "wikilink")、非洲等地的的[联合国维和行动](../Page/联合国维和行动.md "wikilink")。

成都军区的战略使命是保护[成都](../Page/成都.md "wikilink")、重庆等特大城市、军工核心基地，维护西藏的统一，防卫[印度来自](../Page/印度.md "wikilink")[喜马拉雅方向的进攻](../Page/喜马拉雅.md "wikilink")，并与[广州军区协防](../Page/广州军区.md "wikilink")[越南](../Page/越南.md "wikilink")。

2016年1月16日成都军区被撤销，由新成立的西部战区和南部战区接替。

## 编制

成都军区下辖2个[集团军](../Page/集团军.md "wikilink")（第13、14集团军）和成都军区空军，成都军区空军总部位于[成都](../Page/成都.md "wikilink")。

[第13集团军属下的第](../Page/第13集团军.md "wikilink")37师的前身是抗战爆发由[红四方面军第31军改编而成的八路军](../Page/红四方面军第31军.md "wikilink")129师386旅，[陈赓任旅长](../Page/陈赓.md "wikilink")，[陈再道](../Page/陈再道.md "wikilink")（后为[许世友](../Page/许世友.md "wikilink")）任副旅长，[王新亭任政治部主任](../Page/王新亭.md "wikilink")。第13集团军的第149师原为第18军的52师，其前身为晋冀鲁豫野战军第一纵队主力旅之一。曾进军西藏，并鎮壓[1959年藏區騷亂](../Page/1959年藏區騷亂.md "wikilink")。并参加过[中印边境战争](../Page/中印边境战争.md "wikilink")。1969年第50军149师与西藏军区第52师对调防务并互换番号，52师改称149师调入50军。

1985年第50军撤销番号后，属下的主力第149师编入第13军，进驻四川[乐山](../Page/乐山.md "wikilink")。第13集团军现属“成都军区”，为甲类重装集团军，军部驻重庆鹅岭，该部队山地作战能力极强。

同年以原[武汉军区空军改编为成都军区空军](../Page/武汉军区.md "wikilink")，主力为空33师和空44师。空33师97团装备[苏-27驻地重庆](../Page/苏-27.md "wikilink")[白市驿](../Page/重庆白市驿机场.md "wikilink")。空44师132团装备[歼-10驻地云南](../Page/歼-10.md "wikilink")[陆良机场](../Page/陆良机场.md "wikilink")。

  - [第13集团军下辖](../Page/第13集团军.md "wikilink")：
      - 军直部队：教导大队,司机训练大队,通信团,工兵团,防化团,电子对抗团,新装备实验大队。
      - 摩步第37师
      - 摩步第149师
      - 装甲17旅
      - 炮兵旅
      - 高炮旅
      - 陆航2旅
      - 特种作战旅

<!-- end list -->

  - [第14集团军下辖](../Page/第14集团军.md "wikilink")：
      - 军直部队：通信团，工兵团，防化团，舟桥第86团，司训大队
      - 机步第31旅
      - 机步第40旅
      - 摩步第42旅
      - 山步第32旅
      - 第18装甲旅
      - 第4炮兵旅
      - 高炮旅

<!-- end list -->

  - [西藏军区下辖](../Page/西藏军区.md "wikilink")：
      - 直属部队：通信团，汽车16团，电子对抗团，特种作战团。
      - 山地步兵52旅
      - 山地步兵53旅
      - 装甲54旅
      - 防空旅
      - 工兵旅
      - 炮兵308团

所辖各省级[军区](../Page/军区.md "wikilink")：

  - [四川省军区](../Page/四川省军区.md "wikilink")
  - [西藏军区](../Page/西藏军区.md "wikilink")（[副大军区级单位](../Page/副大军区级.md "wikilink")）
  - [贵州省军区](../Page/贵州省军区.md "wikilink")
  - [云南省军区](../Page/云南省军区.md "wikilink")
  - [重庆警备区](../Page/重庆警备区.md "wikilink")

## 历任主官

### [司令员](../Page/司令员.md "wikilink")

  - [贺炳炎](../Page/贺炳炎.md "wikilink") 1955\~1960
  - [黄新廷](../Page/黄新廷.md "wikilink") 1960.8\~1967.3
  - [梁兴初](../Page/梁兴初.md "wikilink") 1967.3\~1972.
  - [秦基伟](../Page/秦基伟.md "wikilink") 1973. \~1975.10
  - [刘兴元](../Page/刘兴元.md "wikilink") 1975.10\~1977.9
  - [吴克华](../Page/吴克华.md "wikilink") 1977.9\~1979.1
  - [尤太忠](../Page/尤太忠.md "wikilink") 1980.1\~1982.10
  - [王诚汉](../Page/王诚汉.md "wikilink") 1982.10\~1985.6
  - [傅全有](../Page/傅全有.md "wikilink")
  - [张太恒](../Page/张太恒.md "wikilink")
  - [李九龍](../Page/李九龍.md "wikilink")
  - [隗福臨](../Page/隗福臨.md "wikilink")
  - [廖锡龙](../Page/廖锡龙.md "wikilink")
  - [王建民](../Page/王建民_\(上将\).md "wikilink")
  - [李世明](../Page/李世明_\(上将\).md "wikilink")
  - [李作成](../Page/李作成.md "wikilink")2013年7月

### [政治委员](../Page/政治委员.md "wikilink")

  - [李井泉](../Page/李井泉.md "wikilink")
  - [郭林祥](../Page/郭林祥.md "wikilink")（先为第二政委，后为第三政委）
  - [廖志高](../Page/廖志高.md "wikilink")（第二政委）
  - [甘渭汉](../Page/甘渭汉.md "wikilink")（第四政委）
  - [张国华](../Page/张国华.md "wikilink")（第一政委）
  - [陈仁麒](../Page/陈仁麒.md "wikilink")（先为第二政委，后为政委）
  - [谢家祥](../Page/谢家祥.md "wikilink")（先为第三政委，后为政委）
  - [谢　政](../Page/谢政.md "wikilink")
  - [刘兴元](../Page/刘兴元.md "wikilink")（第一政委）1972.3\~1975.10
  - [郭林祥](../Page/郭林祥.md "wikilink")
  - [李大章](../Page/李大章.md "wikilink")（第二政委）
  - [陈先瑞](../Page/陈先瑞.md "wikilink")
  - [赵紫阳](../Page/赵紫阳.md "wikilink")（第一政委）
  - [孔石泉](../Page/孔石泉.md "wikilink")（第二政委）
  - [钟汉华](../Page/钟汉华.md "wikilink")
  - [徐立清](../Page/徐立清.md "wikilink")（先为第二政委，后为第一政委）
  - [谭启龙](../Page/谭启龙.md "wikilink")（第二政委）
  - [万海峰](../Page/万海峰.md "wikilink")
  - [谷善慶](../Page/谷善慶.md "wikilink")
  - [張　工](../Page/张工.md "wikilink")
  - [张志坚](../Page/张志坚_\(上将\).md "wikilink")
  - [杨德清](../Page/杨德清.md "wikilink")
  - [刘书田](../Page/刘书田.md "wikilink")
  - [张海阳](../Page/张海阳.md "wikilink")
  - [田修思](../Page/田修思.md "wikilink")
  - [朱福熙](../Page/朱福熙.md "wikilink")

## 历任[副大军区职军官](../Page/副大军区职.md "wikilink")

### 副司令员

  - [何正文](../Page/何正文.md "wikilink")（1955年5月－1959年3月，第二副司令员）
  - [李文清](../Page/李文清.md "wikilink")（1955年5月－1959年6月）
  - [何正文](../Page/何正文.md "wikilink")（1962年6月－1974年11月）
  - [黄新廷](../Page/黄新廷.md "wikilink")（1957年9月－1960年8月）
  - [韦杰](../Page/韦杰.md "wikilink")（1957年9月－1982年10月）
  - [李文清](../Page/李文清.md "wikilink")（1962年6月－1975年8月）
  - [邓少东](../Page/邓少东.md "wikilink")（1965年5月－1975年8月）
  - [胡继成](../Page/胡继成.md "wikilink")（1967年9月－1977年12月）
  - [胡炳云](../Page/胡炳云.md "wikilink")（1967年9月－1974年9月）
  - [王诚汉](../Page/王诚汉.md "wikilink")（1969年3月－1982年10月）
  - [王东保](../Page/王东保.md "wikilink")（1969年7月－1982年10月）
  - [谢正荣](../Page/谢正荣.md "wikilink")（1969年8月－1977年12月）
  - [温玉成](../Page/温玉成.md "wikilink")（1970年6月－1971年11月，第一副司令员）
  - [郑本炎](../Page/郑本炎.md "wikilink")（1971年3月－1975年8月）
  - [陈宏](../Page/陈宏.md "wikilink")（1971年11月－1975年8月，第一副司令员）
  - [茹夫一](../Page/茹夫一.md "wikilink")（1972年9月－1982年10月）
  - [陈明义](../Page/陈明义.md "wikilink")（1973年11月－1985年6月）
  - [肖永银](../Page/肖永银.md "wikilink")（1975年5月－1977年12月）
  - [梁中玉](../Page/梁中玉.md "wikilink")（1975年8月－1979年1月）
  - [赵文进](../Page/赵文进.md "wikilink")（1977年12月－1982年10月）

<!-- end list -->

  - [李文清](../Page/李文清.md "wikilink")（1979年1月－1982年10月）
  - [徐成功](../Page/徐成功.md "wikilink")（1980年12月－1982年10月）
  - [阎守庆](../Page/阎守庆.md "wikilink")（1980年12月－1985年6月）
  - [王金泉](../Page/王金泉.md "wikilink")（1980年12月－1982年10月）
  - [张志礼](../Page/张志礼.md "wikilink")（1982年10月－1985年6月）
  - [廖锡龙](../Page/廖锡龙.md "wikilink")（1985年6月－1995年7月）
  - [张太恒](../Page/张太恒.md "wikilink")（1985年6月－1990年4月）
  - [马秉臣](../Page/马秉臣.md "wikilink")（1986年5月－1993年12月）
  - [侯书军](../Page/侯书军.md "wikilink")（1987年9月－1991年9月）
  - [张德福](../Page/张德福.md "wikilink")（1990年4月－1991年6月）
  - [姜洪泉](../Page/姜洪泉.md "wikilink")（1991年4月－1992年11月）
  - [谢德财](../Page/谢德财.md "wikilink")（1991年9月－1993年12月）
  - [迟云秀](../Page/迟云秀.md "wikilink")（1992年11月－1994年12月）
  - [朱成友](../Page/朱成友.md "wikilink")（1993年12月－1996年4月）
  - [黄恒美](../Page/黄恒美.md "wikilink")（1993年12月－2001年1月）
  - [陈世俊](../Page/陈世俊.md "wikilink")（1994年12月－2004年12月）
  - [陈显华](../Page/陈显华.md "wikilink")（1996年1月－2001年7月）
  - [刘宝臣](../Page/刘宝臣.md "wikilink")（1996年7月－2002年1月）
  - [傅秉耀](../Page/傅秉耀.md "wikilink")（1997年3月－2003年7月）
  - [金仁燮](../Page/金仁燮.md "wikilink")（2000年6月－2003年1月）

<!-- end list -->

  - [汪超群](../Page/汪超群.md "wikilink")（2001年1月－2005年12月）
  - [蒙进喜](../Page/蒙进喜.md "wikilink")（2001年7月－2007年12月）
  - [桂全智](../Page/桂全智.md "wikilink")（2002年1月－2006年12月）
  - [刘亚红](../Page/刘亚红.md "wikilink")（2003年1月－2003年12月）
  - [方殿荣](../Page/方殿荣.md "wikilink")（2003年7月－）
  - [范晓光](../Page/范晓光.md "wikilink")（2003年12月－2008年7月）
  - [李世明](../Page/李世明.md "wikilink")（2006年12月－2007年9月）
  - [吕登明](../Page/吕登明.md "wikilink")（2007年9月－2009年1月）
  - [李作成](../Page/李作成.md "wikilink")（2007年12月－2013年7月）
  - [董贵山](../Page/董贵山.md "wikilink")（2008年7月－2009年12月）
  - [阮志柏](../Page/阮志柏.md "wikilink")（2009年1月－2012年5月）
  - [舒玉泰](../Page/舒玉泰.md "wikilink")（2009年12月－2014年12月）
  - [艾虎生](../Page/艾虎生.md "wikilink")（2012年7月－2014年12月）
  - [战厚顺](../Page/战厚顺.md "wikilink")（2013年1月－）
  - [楊金山](../Page/楊金山.md "wikilink")（2013年7月－2014年10月）
  - [石香元](../Page/石香元.md "wikilink")（2014年1月－2015年7月）
  - [周小周](../Page/周小周.md "wikilink")（2014年12月－）
  - [李凤彪](../Page/李凤彪.md "wikilink")（2014年12月－）
  - [郑和](../Page/郑和_\(少将\).md "wikilink")（2015年7月-）

### 副政治委员

  - [阎红彦](../Page/阎红彦.md "wikilink")（1955年5月－1959年11月）
  - [郭林祥](../Page/郭林祥.md "wikilink")（1955年5月－1959年11月）
  - [余洪远](../Page/余洪远.md "wikilink")（1962年12月－1970年7月）
  - [余述生](../Page/余述生.md "wikilink")（1964年2月－1973年8月）
  - [刘结挺](../Page/刘结挺.md "wikilink")（1967年7月－1971年1月）
  - [谢家祥](../Page/谢家祥.md "wikilink")（1967年7月－1970年12月）
  - [谢云晖](../Page/谢云晖.md "wikilink")（1970年3月－1975年8月）
  - [何云峰](../Page/何云峰.md "wikilink")（1970年5月－1975年5月）
  - [段思英](../Page/段思英_\(少将\).md "wikilink")（1970年5月－1977年12月）
  - [欧阳平](../Page/欧阳平.md "wikilink")（1970年12月－1975年8月）
  - [任荣](../Page/任荣.md "wikilink")（1971年9月－1980年8月）
  - [余洪远](../Page/余洪远.md "wikilink")（1973年3月－1975年8月）
  - [罗应怀](../Page/罗应怀.md "wikilink")（1975年5月－1977年12月）

<!-- end list -->

  - [魏伯亭](../Page/魏伯亭.md "wikilink")（1975年8月－1977年12月）
  - [余述生](../Page/余述生.md "wikilink")（1977年12月－1982年10月）
  - [胡继成](../Page/胡继成.md "wikilink")（1977年12月－1979年1月）
  - [谢云辉](../Page/谢云辉.md "wikilink")（1979年1月－1982年10月）
  - [欧阳平](../Page/欧阳平.md "wikilink")（1979年5月－1982年10月）
  - [金忠藩](../Page/金忠藩.md "wikilink")（1979年7月－1982年10月）
  - [鲁加汉](../Page/鲁加汉.md "wikilink")（1979年7月－1982年10月）
  - [阴法唐](../Page/阴法唐.md "wikilink")（1980年6月－1985年6月）
  - [王金泉](../Page/王金泉.md "wikilink")（1982年10月－1985年6月）
  - [牛击](../Page/牛击.md "wikilink")（1982年10月－1985年6月）
  - [李硕](../Page/李硕.md "wikilink")（1985年6月－1988年6月）
  - [艾维仁](../Page/艾维仁.md "wikilink")（1988年6月－1990年4月）
  - [王永宁](../Page/王永宁.md "wikilink")（1990年4月－1993年12月）

<!-- end list -->

  - [邵农](../Page/邵农.md "wikilink")（1990年4月－1993年12月）
  - [张少松](../Page/张少松.md "wikilink")（1992年11月－1995年7月）
  - [郑贤斌](../Page/郑贤斌.md "wikilink")（1993年12月－1997年11月）
  - [吴润忠](../Page/吴润忠.md "wikilink")（1993年12月－2000年12月）
  - [姜福堂](../Page/姜福堂.md "wikilink")（1993年12月－1995年9月）
  - [杨德福](../Page/杨德福.md "wikilink")（1997年11月－2000年6月）
  - [屈全绳](../Page/屈全绳.md "wikilink")（2000年6月－2007年6月）
  - [马子龙](../Page/马子龙.md "wikilink")（2000年12月－2006年12月）
  - [胡永柱](../Page/胡永柱.md "wikilink")（2006年12月－2007年12月）
  - [段禄定](../Page/段禄定.md "wikilink")（2007年6月－2010年7月）
  - [赵开增](../Page/赵开增.md "wikilink")（2007年12月－）
  - [刘长银](../Page/刘长银.md "wikilink")（2010年7月－）

### [参谋长](../Page/参谋长.md "wikilink")

  - [何正文](../Page/何正文.md "wikilink")（1955年3月－1955年12月）
  - [茹夫一](../Page/茹夫一.md "wikilink")（1955年12月－1957年8月）
  - [何正文](../Page/何正文.md "wikilink")（1958年3月－1960年8月）
  - [茹夫一](../Page/茹夫一.md "wikilink")（1960年8月－1972年9月）
  - [徐成功](../Page/徐成功.md "wikilink")（1972年9月－1975年8月）
  - [陈明义](../Page/陈明义.md "wikilink")（1975年8月－1978年7月）

<!-- end list -->

  - [徐成功](../Page/徐成功.md "wikilink")（1978年7月－1980年12月）
  - [杨增彤](../Page/杨增彤.md "wikilink")（1980年12月－1985年6月）
  - [陶伯钧](../Page/陶伯钧.md "wikilink")（1985年6月－1992年11月）
  - [陈显华](../Page/陈显华.md "wikilink")（1992年11月－1996年1月）
  - [朱启](../Page/朱启.md "wikilink")（1996年1月－1998年3月）
  - [金仁燮](../Page/金仁燮.md "wikilink")（1998年3月－2000年6月）

<!-- end list -->

  - [桂全智](../Page/桂全智.md "wikilink")（2000年6月－2002年1月）
  - [刘亚红](../Page/刘亚红.md "wikilink")（2002年1月－2003年1月）
  - [吕登明](../Page/吕登明.md "wikilink")（2003年1月－2007年9月）
  - [艾虎生](../Page/艾虎生.md "wikilink")（2007年9月－2012年7月）
  - [周小周](../Page/周小周.md "wikilink")（2012年7月－2014年12月）
  - [戎贵卿](../Page/戎贵卿.md "wikilink")（2014年12月-）

### [政治部主任](../Page/政治部.md "wikilink")

  - [余述生](../Page/余述生.md "wikilink")（1955年5月－1969年3月）
  - [段思英](../Page/段思英_\(少将\).md "wikilink")（1969年3月－1972年9月）
  - [鲍奇辰](../Page/鲍奇辰.md "wikilink")（1972年9月－1975年8月）
  - [王焕如](../Page/王焕如.md "wikilink")（1975年8月－1977年12月）
  - [金忠藩](../Page/金忠藩.md "wikilink")（1977年12月－1979年7月）
  - [牛击](../Page/牛击.md "wikilink")（1979年7月－1982年10月）

<!-- end list -->

  - [乔学亭](../Page/乔学亭.md "wikilink")（1982年10月－1985年6月）
  - [邵农](../Page/邵农.md "wikilink")（1985年6月－1990年4月）
  - [郑贤斌](../Page/郑贤斌.md "wikilink")（1990年4月－1993年12月）
  - [姜福堂](../Page/姜福堂.md "wikilink")（1993年12月－1995年9月，副政治委员兼）
  - [杨德福](../Page/杨德福.md "wikilink")（1996年1月－1997年11月）
  - [马国文](../Page/马国文.md "wikilink")（1997年11月－1998年8月）

<!-- end list -->

  - [屈全绳](../Page/屈全绳.md "wikilink")（1998年8月－2000年6月）
  - [胡永柱](../Page/胡永柱.md "wikilink")（2000年6月－2006年12月）
  - [杜金才](../Page/杜金才.md "wikilink")（2006年12月－2007年6月）
  - [吴昌德](../Page/吴昌德.md "wikilink")（2007年6月－2011年7月）
  - [柴绍良](../Page/柴绍良.md "wikilink")（2011年7月－2014年2月）
  - [刘念光](../Page/刘念光.md "wikilink")（2014年2月－2016年1月）

## 参考文献

  - [解放军、武警部队和公安现役部队现任主要负责人名录](http://club.xilu.com/xinguancha/msgview-950389-104372.html)
  - [解放军各大单位历任主要负责人名录](http://club.xilu.com/xinguancha/msgview-950389-97222.html)

## 外部链接

  - [成都军区|中国军网-成都军区|军报记者-成都](https://web.archive.org/web/20150905202400/http://cd.81.cn/)

{{-}}

[Category:中国人民解放军大军区](../Category/中国人民解放军大军区.md "wikilink")
[成都军区](../Category/成都军区.md "wikilink")
[Category:西南军事](../Category/西南军事.md "wikilink")
[Category:中国人民解放军驻成都单位](../Category/中国人民解放军驻成都单位.md "wikilink")
[Category:中国人民解放军驻重庆单位](../Category/中国人民解放军驻重庆单位.md "wikilink")
[Category:中国人民解放军驻四川单位](../Category/中国人民解放军驻四川单位.md "wikilink")
[Category:中国人民解放军驻贵州单位](../Category/中国人民解放军驻贵州单位.md "wikilink")
[Category:中国人民解放军驻云南单位](../Category/中国人民解放军驻云南单位.md "wikilink")
[Category:中国人民解放军驻西藏单位](../Category/中国人民解放军驻西藏单位.md "wikilink")