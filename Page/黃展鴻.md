**黃展鴻**（**Wong Chin
Hung**，），生於[香港](../Page/香港.md "wikilink")，司職[左後衛](../Page/左後衛.md "wikilink")，現擔任[香港甲組聯賽球會](../Page/香港甲組聯賽.md "wikilink")[標準流浪主教練兼球員](../Page/流浪足球會.md "wikilink")。

## 球會生涯

黃展鴻生於[香港在](../Page/香港.md "wikilink")[觀塘](../Page/觀塘.md "wikilink")[樂華邨長大](../Page/樂華邨.md "wikilink")，自幼已熱愛足球，於16歲時與同學一同投考[快譯通青年軍](../Page/花花足球會.md "wikilink")，並獲取錄成為學徒球員，直到[快譯通退出](../Page/花花足球會.md "wikilink")[甲組聯賽後](../Page/香港甲組足球聯賽.md "wikilink")，黃展鴻曾轉行投身裝修行業，及後獲得[流浪總監](../Page/香港流浪足球會.md "wikilink")[李輝立邀請加盟](../Page/李輝立.md "wikilink")，正式展開[甲組球員生涯](../Page/香港甲組足球聯賽.md "wikilink")。

初期，黃展鴻擔任中場角色，一年後轉投[福建](../Page/香港福建足球隊.md "wikilink")，直到2005年重返[流浪](../Page/香港流浪足球會.md "wikilink")，並轉型左後衛。\[1\]他在2007年夏天加盟本地老牌球會[南華](../Page/南華足球隊.md "wikilink")，首季已成為球隊正選左後衛。

2008/09年球季，由於[香港隊主力左後衛](../Page/香港足球代表隊.md "wikilink")[潘耀焯由](../Page/潘耀焯.md "wikilink")[愉園加盟](../Page/愉園體育會.md "wikilink")[南華](../Page/南華足球隊.md "wikilink")，令黃展鴻失去正選位置。季初黃展鴻外借至[天水圍飛馬](../Page/天水圍飛馬.md "wikilink")，並於2008年10月12日對[屯門普高的聯賽大演帽子戲法](../Page/屯門普高.md "wikilink")。至2009年1月把他收回，[南華於](../Page/南華足球隊.md "wikilink")[2010年亞協盃分組賽首兩輪成績不濟](../Page/2010年亞洲足協盃.md "wikilink")，只錄得一和一負的成績，直到第三輪開始[南華重上正軌](../Page/南華足球隊.md "wikilink")，在第五輪作客[泰國勁旅](../Page/泰國.md "wikilink")[蒙通聯的賽事中](../Page/蒙通聯足球會.md "wikilink")，黃展鴻於下半場未段後備入替[白鶴](../Page/白鶴_\(足球運動員\).md "wikilink")，在上場短短三分鐘便於禁區內接應[歐陽耀冲的橫傳射入奠定勝局的一球](../Page/歐陽耀冲.md "wikilink")，更令[南華升上小組榜首](../Page/南華足球隊.md "wikilink")。

在2012/13年球季，由於隊友[郭建邦表現出色](../Page/郭建邦.md "wikilink")，令黃展鴻長居後備。直到2013年1月與[南華解約重返母會](../Page/南華足球隊.md "wikilink")[流浪](../Page/香港流浪足球會.md "wikilink")，並獲委任為隊長。\[2\]黃展鴻於2013年8月轉會升班馬[東方沙龍](../Page/東方足球隊.md "wikilink")，直至2017/18年球季加入[東方龍獅的教練團](../Page/東方足球隊.md "wikilink")。\[3\]

## 國際賽生涯

在2009年12月，黃展鴻因[翟廷峯受傷](../Page/翟廷峯.md "wikilink")，而以超齡球員身份獲補選入[香港奧運代表隊](../Page/香港奧運足球代表隊.md "wikilink")，參加在[香港舉行的](../Page/香港.md "wikilink")[東亞運動會足球比賽](../Page/2009年東亞運動會足球比賽.md "wikilink")，在決賽對[日本互射十二碼階段時射入關鍵的第五球](../Page/日本23歲以下國家足球隊.md "wikilink")，協助[香港以總比數](../Page/香港奧運足球代表隊.md "wikilink")5–3勝出，歷史性首次贏得大型運動會足球金牌之餘，他更視為[香港](../Page/香港.md "wikilink")[東亞運足球金牌功臣之一](../Page/2009年東亞運動會足球比賽.md "wikilink")。\[4\]

## 個人生活

黃展鴻已婚並育有二子和一女。\[5\]

## 榮譽

  - 南華

<!-- end list -->

  - [香港甲組足球聯賽冠軍](../Page/香港甲組足球聯賽.md "wikilink")（2007–08、2008–09、2009–10、2012–13季度）
  - [香港聯賽盃冠軍](../Page/香港聯賽盃.md "wikilink")（2007–08季度）
  - [香港高級組銀牌冠軍](../Page/香港高級組銀牌.md "wikilink")（2009–10季度）
  - [香港足總盃冠軍](../Page/香港足總盃.md "wikilink")（2007–08、2010–11季度）

<!-- end list -->

  - 東方龍獅

<!-- end list -->

  - [香港足總盃冠軍](../Page/2013–14年香港足總盃.md "wikilink")（2013–14季度）
  - [香港高級組銀牌冠軍](../Page/香港高級組銀牌.md "wikilink")（2014–15、2015–16季度）
  - [香港超級聯賽冠軍](../Page/2015－16年度香港超級聯賽.md "wikilink")（2015–16季度）

<!-- end list -->

  - 香港代表隊

<!-- end list -->

  - [東亞運動會足球項目金牌](../Page/2009年東亞運動會足球比賽.md "wikilink")（2009年）
  - [龍騰盃冠軍](../Page/2011年龍騰盃國際足球邀請賽.md "wikilink")（2011年）
  - [香港傑出運動員選舉](../Page/香港傑出運動員選舉.md "wikilink") 香港最佳運動隊伍（2009年）
  - [省港盃冠軍](../Page/省港盃.md "wikilink")（2009、2013年）

<!-- end list -->

  - 香港聯賽選手隊

<!-- end list -->

  - [香港賀歲盃冠軍](../Page/2009年賀歲盃足球賽.md "wikilink")（2009年）

<!-- end list -->

  - 個人

<!-- end list -->

  - [香港足球明星選舉](../Page/香港足球明星選舉.md "wikilink") 最佳11人（2008–09、2009–10季度）

## 參考資料

## 外部連結

  - [香港足球總會網站球員註冊資料](http://www.hkfa.com/ch/club/12/detail?player=43)

[Category:黃姓](../Category/黃姓.md "wikilink")
[Category:港超球員](../Category/港超球員.md "wikilink")
[Category:港甲球員](../Category/港甲球員.md "wikilink")
[Category:快譯通球員](../Category/快譯通球員.md "wikilink")
[Category:流浪球員](../Category/流浪球員.md "wikilink")
[Category:福建球員](../Category/福建球員.md "wikilink")
[Category:飛馬球員](../Category/飛馬球員.md "wikilink")
[Category:南華球員](../Category/南華球員.md "wikilink")
[Category:足球後衛](../Category/足球後衛.md "wikilink")
[Category:東方球員](../Category/東方球員.md "wikilink")
[Category:香港足球運動員](../Category/香港足球運動員.md "wikilink")
[Category:香港足球代表隊球員](../Category/香港足球代表隊球員.md "wikilink")

1.  [黃展鴻為球迷踢好波](http://news.takungpao.com/paper/q/2013/0121/1397874.html)
    [大公報](../Page/大公報.md "wikilink") 2013年1月21日
2.  [黃展鴻回歸流浪做隊長](http://hk.apple.nextmedia.com/sports/art/20130111/18129980)
    [蘋果日報](../Page/蘋果日報.md "wikilink") 2013年01月11日
3.  [【港超聯】七新兵曬冷開操
    東方劍指所有錦標](http://hk.apple.nextmedia.com/realtime/sports/20170711/56941595)
    [蘋果日報](../Page/蘋果日報.md "wikilink") 2017年07月11日
4.  [東亞運金牌英雄低調轉型
    「小球王」黃展鴻初任教練](https://www.upower.com.hk/article/76932-%E6%9D%B1%E4%BA%9E%E9%81%8B%E9%87%91%E7%89%8C%E8%8B%B1%E9%9B%84%E4%BD%8E%E8%AA%BF%E8%BD%89%E5%9E%8B-%E3%80%8C%E5%B0%8F%E7%90%83%E7%8E%8B%E3%80%8D%E9%BB%83%E5%B1%95%E9%B4%BB%E5%88%9D%E4%BB%BB%E6%95%99)
    Upower 2017年07月11日
5.  [工廈變「綠茵場」　踢出「錢途」](http://hk.apple.nextmedia.com/nextplus/%E5%91%A8%E5%88%8A%E5%A3%B9%E7%9B%A4%E7%94%9F%E6%84%8F/article/20130228/2_16847075/%E5%B7%A5%E5%BB%88%E8%AE%8A-%E7%B6%A0%E8%8C%B5%E5%A0%B4-%E8%B8%A2%E5%87%BA-%E9%8C%A2%E9%80%94-)
    [蘋果日報](../Page/蘋果日報.md "wikilink") 2013年1月19日