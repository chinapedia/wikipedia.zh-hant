**罗伯特·霍夫施塔特**（，），[美国](../Page/美国.md "wikilink")[物理学家](../Page/物理学家.md "wikilink")，因為“其对原子核中的电子散射现象的开创性研究以及随之而来的对核子结构的发现”而和[鲁道夫·穆斯堡尔共同分享了](../Page/鲁道夫·穆斯堡尔.md "wikilink")1961年[诺贝尔物理学奖](../Page/诺贝尔物理学奖.md "wikilink")。\[1\]\[2\]

## 生平

霍夫斯塔特1915年2月5日出生于纽约。父亲路易斯·霍夫斯塔特是一位推销员。霍夫斯塔特是在纽约接受的小学和中学教育之后进入纽约市立学院，并在1935年他20岁时以[极优等的成绩获得理学学士学位](../Page/拉丁文学位荣誉.md "wikilink")。之后接受来自通用电气公司所属Charles
A.
Coffin基金的奖学金资助，来到普林斯顿大学进行研究生学习并先后获得理学硕士学位和哲学博士学位。1938年博士毕业后前往宾夕法尼亚大学进行博士后研究。在[第二次世界大战期间以物理学家的身份帮助美国国家标准局开发了近炸引信和防空武器](../Page/第二次世界大战.md "wikilink")。1950年加入斯坦福大学后在其中任教至1985年退休。\[3\]

1942年他与出生于[巴尔的摩的南希](../Page/巴尔的摩.md "wikilink")·吉万（1920-2007）成婚。\[4\]他们育有三个孩子：劳拉、莫里（患有残疾无法说话）\[5\]和[普利策奖获得者](../Page/普利策奖.md "wikilink")[道格拉斯](../Page/侯世达.md "wikilink")。\[6\]

罗伯特·霍夫斯塔特的[埃尔德什数是](../Page/埃尔德什数.md "wikilink")5。\[7\]

美国[哥伦比亚广播公司播出的电视情景喜剧](../Page/哥伦比亚广播公司.md "wikilink")《[-{zh-tw:宅男行不行;
zh-hk:囧男大爆炸;
zh-cn:生活大爆炸;}-](../Page/生活大爆炸.md "wikilink")》中的角色[伦纳德·霍夫斯塔特就是以他为原型的](../Page/伦纳德·霍夫斯塔特.md "wikilink")。

## 贡献

罗伯特·霍夫斯塔特在普林斯顿大学攻读博士学位期间开发出了以碱金属卤化物为材料制作的[闪烁计数器](../Page/闪烁计数器.md "wikilink")，利用少量铊或钠杂质激活后的NaI和CsI来检测[X射线](../Page/X射线.md "wikilink")。\[8\]

1956年罗伯特·霍夫斯塔特在由他撰写并发表在[现代物理评论的论文中创造出了一个新的长度单位](../Page/现代物理评论.md "wikilink")[飞米](../Page/飞米.md "wikilink")（单位符号**fm**）以纪念核物理学的奠基人之一意大利物理学家[恩里科·费米](../Page/恩里科·费米.md "wikilink")\[9\]，后来这个词被核物理学家和粒子物理学家广泛使用。

## 参考资料

  - [诺贝尔官方网站关于罗伯特·霍夫施塔特简介](http://nobelprize.org/nobel_prizes/physics/laureates/1961/hofstadter-bio.html)

[H](../Category/1915年出生.md "wikilink")
[H](../Category/1990年逝世.md "wikilink")
[H](../Category/美國核物理學家.md "wikilink")
[Category:實驗物理學家](../Category/實驗物理學家.md "wikilink")
[H](../Category/诺贝尔物理学奖获得者.md "wikilink")
[Category:美國諾貝爾獎獲得者](../Category/美國諾貝爾獎獲得者.md "wikilink")
[H](../Category/猶太諾貝爾獎獲得者.md "wikilink")
[Category:美国国家科学奖获奖者](../Category/美国国家科学奖获奖者.md "wikilink")
[Category:紐約市立大學校友](../Category/紐約市立大學校友.md "wikilink")
[Category:普林斯顿大学校友](../Category/普林斯顿大学校友.md "wikilink")
[Category:史丹佛大學教師](../Category/史丹佛大學教師.md "wikilink")
[Category:紐約市人](../Category/紐約市人.md "wikilink")
[Category:UNSW狄拉克奖章和讲座得主](../Category/UNSW狄拉克奖章和讲座得主.md "wikilink")

1.
2.  Robert Hofstadter, "[The Electron Scattering Method & its
    Application to the Structure of Nuclei and
    Nucleons](http://nobelprize.org/nobel_prizes/physics/laureates/1961/hofstadter-lecture.pdf),"
    Nobel Lectures, Physics 1942-1962, pp. 560-581, Elsevier Pub. Co.,
    Amsterdam-London-New York (Dec 1961).
3.
4.  2007 [obituary to Nancy
    Givan](http://news.stanford.edu/pr/2007/pr-nancy-082207.html) from
    Stanford university.
5.  Doug Hofstadter's dedication to 'I am a strange loop'.
6.  [National Academy of Sciences
    biography](http://stills.nap.edu/html/biomems/rhofstadter.html)
7.  [Some Famous People with Finite Erdös
    Numbers](http://www.oakland.edu/enp/erdpaths/)
8.
9.