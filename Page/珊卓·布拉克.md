**珊卓·安妮特·布拉克**（英语：Sandra Annette
Bullock，），生于[美国](../Page/美国.md "wikilink")[維吉尼亞州](../Page/維吉尼亞州.md "wikilink")，[美國知名女](../Page/美國.md "wikilink")[演員](../Page/演員.md "wikilink")、编剧和制片人，[奥斯卡最佳女主角奖得主](../Page/奥斯卡最佳女主角奖.md "wikilink")。

她成名于1990年代，代表作包括《[捍衛戰警](../Page/生死時速.md "wikilink")》和《[二見鍾情](../Page/二見鍾情.md "wikilink")》，自此確立[好萊塢一線女星的地位](../Page/好萊塢.md "wikilink")。2010年憑在電影《[攻其不備](../Page/攻其不備.md "wikilink")》中的演出贏得[第82屆](../Page/第82屆奧斯卡金像獎.md "wikilink")[奧斯卡最佳女主角獎](../Page/奧斯卡最佳女主角獎.md "wikilink")。

## 早年生活

她出生於[美國](../Page/美國.md "wikilink")[維吉尼亞州](../Page/維吉尼亞州.md "wikilink")[阿靈頓縣](../Page/阿靈頓縣.md "wikilink")，父親約翰·W·布拉克為[五角大廈承包商](../Page/五角大廈.md "wikilink")，母親海各·D·麥爾是一名德國歌劇唱家，不過已於2000年4月4日因癌症與世長辭。布拉克外祖父是一名[紐倫堡的火箭科學家](../Page/紐倫堡.md "wikilink")，後來布拉克在紐倫堡住到了12歲之後，開始與她的母親展開歌劇巡迴演出，也因此布拉克的童年除了住在德國外也住在歐洲的其他地方，布拉克自小也從媽媽身上學習[芭蕾舞以及演唱藝術](../Page/芭蕾舞.md "wikilink")。
布拉克高中時就讀華盛頓李高校，並且是一名啦啦隊員，還曾經與足球隊員約會過。1982年畢業後進入東卡羅萊納大學，在這期間曾經做過服務生，不過後來為了投入演藝圈而在讀了大學三年後休學，後來前往[曼哈頓做演員的試聽](../Page/曼哈頓.md "wikilink")，並且讓自己投身多項工作融入社會（酒保、酒吧女招待、衣帽間管理員）。

布拉克後來還是在東卡羅萊納大學完成了學業，拿到了學士學位，布拉克會說一口流利的德語，不過她在德國電視訪談中談到她比較喜歡說英語。

## 演員生涯

當身在紐約之時，珊卓在邻里剧场表演学校（The Neighborhood
Playhouse）學習戲劇課程。她在許多個學生電影中現身，而且稍後也在[外百老匯的戲劇](../Page/外百老匯.md "wikilink")"No
Time Flat"中領銜演出。導演 亞倫·J·雷維對珊卓的演出印象深刻，並且提供她在1989年的電視電影作品《Bionic Showdown:
The Six Million Dollar Man and the Bionic
Woman》中的演出機會。在本片拍攝完畢之後，珊卓留在[洛杉磯](../Page/洛杉磯.md "wikilink")，在幾部獨立電影中演出小角色，也在[NBC電視](../Page/NBC.md "wikilink")1990年的短篇現場影片《Working
Girl》
中演出主要角色。之後她接拍了幾部電影，1992年的《[浪漫女人香](../Page/:en:Love_Potion_No._9_\(film\).md "wikilink")》、1993年的《[愛情這玩意](../Page/:en:The_Thing_Called_Love.md "wikilink")》與《[亞馬遜之火](../Page/:en:Fire_on_the_Amazon.md "wikilink")》。之後珊卓憑藉在1993年的動作片《[超級戰警](../Page/超級戰警.md "wikilink")》（Demolition
Man）的突出表現，獲得了出演次年上映的電影《[捍衛戰警](../Page/捍衛戰警.md "wikilink")》（Speed）女主角安妮一角的機會。《[捍衛戰警](../Page/捍衛戰警.md "wikilink")》在全球範圍內取得巨大成功，票房突破3億500萬美元，成為她演藝生涯中第二賣座的影片。

接著珊卓接拍的《[二見鍾情](../Page/二見鍾情.md "wikilink")》、《網絡上身》、《殺戮時刻》均在票房上取得不俗成績，正式確立好萊塢一線女星地位。同时憑藉在《二見鍾情》中的演出，珊卓還獲得了[金球獎最佳音樂及喜劇類電影女主角的提名](../Page/金球獎最佳音樂及喜劇類電影女主角.md "wikilink")。1997年珊卓拍攝《[捍衛戰警2](../Page/捍衛戰警2.md "wikilink")》時接受了1100萬美金的片酬，而在《[麻辣女王2](../Page/麻辣女王2.md "wikilink")》中則接受了1750萬美金的片酬。\[1\]

在1996年與1999年，珊卓被《[人物](../Page/人物_\(杂志\).md "wikilink")》杂志评選為世界上最美麗的50個人物之一，她在《[帝国](../Page/帝國雜誌.md "wikilink")》杂志所列出的「世界上100名永遠的電影明星」中名列第58名。她出席了2002年表揚她傑出表現的「Raul
Julia Award」\<ref name = Washington Life"\></ref>，身為情境喜劇《The George
Lopez Show》的製作人，她為了拓展事業成立了公司。

在2004年，珊卓在電影《[衝擊效應](../Page/衝擊效應.md "wikilink")》（Crash）中演出配角的角色，並因此片的演出獲得正面評價，也些影評認為這是她演藝生涯中最棒的一次演出。稍後珊卓在電影《[跳越時空的情書](../Page/跳越時空的情書.md "wikilink")》（The
Lake
House）中現身，這部浪漫劇情片中，她再度和同演《[捍衛戰警](../Page/捍衛戰警.md "wikilink")》的男演員[基努·李維攜手演出](../Page/基努·李維.md "wikilink")，本片在2006年6月16日發行，因為在本劇中兩位主人翁的角色因故事所需而分開演出
(本劇情節是關於時空旅行)，珊卓與[基努·李維在演出期間只有兩週是一起共同拍攝的](../Page/基努·李維.md "wikilink")。\[2\]在同一年，珊卓在電影《[柯波帝：冷血污名](../Page/聲名狼藉.md "wikilink")》（Infamous）中現身，此劇中她扮演作家[哈波·李](../Page/哈波·李.md "wikilink")（Harper
Lee）。珊卓也在電影《[亡命感應](../Page/亡命感應.md "wikilink")》（Premonition）中與[朱利安·麥可馬洪一同演出](../Page/朱利安·麥可馬洪.md "wikilink")，本劇於2007年的3月發行。\[3\]

2010年以電影《[攻其不備](../Page/攻其不備.md "wikilink")》获得[奧斯卡最佳女主角獎](../Page/奧斯卡.md "wikilink")，但在奧斯卡典禮的前一天也以另一部電影《[求愛女王](../Page/求愛女王.md "wikilink")》獲得[金酸莓獎最爛女主角](../Page/金酸莓獎.md "wikilink")，創下首位在同一年以來同時獲得的奧斯卡獎和金酸莓獎的记錄。

珊卓在2005年3月24日接受了[好萊塢星光大道上的一顆星](../Page/好萊塢.md "wikilink")，2007年1月份，珊卓以8500萬元的淨值，名列[富比士](../Page/富比士.md "wikilink")（Forbes）雜誌娛樂圈第十四名富有的女性。\[4\]

2013年在墨西哥导演[艾方索·柯朗执导的](../Page/艾方索·柯朗.md "wikilink")3D科幻灾难片《[地心引力](../Page/地心引力_\(电影\).md "wikilink")》中饰演险境求生的[航天员莱恩](../Page/航天员.md "wikilink")·斯通博士，其演出获得广泛好评，并获得[金球奖](../Page/金球奖.md "wikilink")、[美国演员工会奖](../Page/美国演员工会奖.md "wikilink")、[英国电影学院奖和](../Page/英国电影学院奖.md "wikilink")[奥斯卡金像奖最佳女主角提名](../Page/奥斯卡金像奖.md "wikilink")。

## 企業經營者身份

珊卓管理自己的製作公司「Fortis
Films」，她的姊-{}-妹[傑西娜·布拉克·布拉杜是此公司的董事長](../Page/傑西娜·布拉克·布拉杜.md "wikilink")，而她的父親[約翰·布拉克則是這家公司的執行長](../Page/約翰·布拉克.md "wikilink")。\[5\]珊卓是《George
Lopez》的製作人，電視網在五月錄製這齣情境喜劇，即使在企業上並不見得是有實質獲利的盤算，且得使珊卓耗資1000萬美金，珊卓仍嘗試製作一部由作家F.X.吐爾的短篇故事《[登峰造擊](../Page/登峰造擊.md "wikilink")》所改編的電影，雖未能在女性的票房中吸引注意，\[6\]
但這個故事最終在2004年的[奧斯卡金像獎中獲獎項](../Page/奧斯卡金像獎.md "wikilink")。珊卓的公司[Fortis
Films同樣製作了她自己主演的影片](../Page/Fortis_Films.md "wikilink")《[求愛女王](../Page/求愛女王.md "wikilink")》。另外与此同時，她还致力於她的「Austin餐廳」、「Bess
Bistro」與其第一個行銷企畫──一系列的有機蠟燭。

## 私人生活

布拉克曾經與演員[坦特·唐納文訂婚](../Page/坦特·唐納文.md "wikilink")，當時他們是在拍攝Love Potion No.
9而相遇，不過他們的關係只維持了4年。她也曾經與美式足球選手東尼·艾克曼約會過，或是吉他手蓋·佛西斯、史帝夫·巴希米、澳洲音樂家包伯·史奈德、萊恩·高斯林、[馬修·麥克康諾伊交往過](../Page/馬修·麥克康諾伊.md "wikilink")。不過最後都沒有繼續交往下去。
布拉克於2005年7月16日嫁給电视节目主持人傑西·詹姆斯，兩人是在布拉克為十歲教子準備聖誕禮物時認識的。布拉克與先生的婚姻上還曾經註明過：

於2010年3月，傳媒揭發傑西與多名女子的外遇醜聞，傑西於3月18日向布拉克發表公開道歉信，希望挽救婚姻，但兩人最終於6月正式離婚。同時，布拉克宣佈會以單身母親身份，繼續收養她與傑西在四年前開始申請、並於2010年1月正式領養的男嬰Louis
Bardo Bullock。

布拉克曾經在2000年12月20日逃離了Jackson
Hole機場的空難。後來在2001年震驚全世界的[911恐怖攻擊事件時](../Page/911恐怖攻擊事件.md "wikilink")，布拉克當時下榻在Soho
Grand旅館，而這旅館就離世貿中心只有12個街區的距離，當布拉克在旅館房間內目睹此事發生時，她立即到附近的醫院提供救援，並且在全紐約電話線癱瘓時，利用自身的空閒時間使用Palm發送e-mail替受傷的人聯絡到他們的家人。

布拉克曾三度捐贈100萬美元至美國紅十字會，第一次是[911恐怖攻擊事件](../Page/911恐怖攻擊事件.md "wikilink")，第二次則是[南亞大海嘯](../Page/南亞大海嘯.md "wikilink")，第三次是[海地大地震](../Page/海地大地震.md "wikilink")。

## 作品列表

### 電影

<table>
<thead>
<tr class="header">
<th><p>片名</p></th>
<th><p>年份</p></th>
<th><p>角色</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/佔領美國.md" title="wikilink">佔領美國</a> (Hangmen)</p></td>
<td><p>1987</p></td>
<td><p>莉莎·愛德華茲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>異想天開 (<a href="../Page/A_Fool_and_His_Money.md" title="wikilink">A Fool and His Money</a>)</p></td>
<td><p>1989</p></td>
<td><p>黛比</p></td>
<td><p>又名Religion, Inc.</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/情有獨鍾.md" title="wikilink">情有獨鍾</a> (Who Shot Patakango?)</p></td>
<td><p>1989</p></td>
<td><p>黛芙琳·摩倫</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/浪漫女人香.md" title="wikilink">浪漫女人香</a> (Love Potion No. 9)</p></td>
<td><p>1992</p></td>
<td><p>黛安·費蘿</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/神秘失蹤.md" title="wikilink">神秘失蹤</a> (The Vanishing)</p></td>
<td><p>1993</p></td>
<td><p>黛安·薛弗</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/當派對結束.md" title="wikilink">當派對結束</a> (When the Party's Over)</p></td>
<td><p>1993</p></td>
<td><p><a href="../Page/亞曼達.md" title="wikilink">亞曼達</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/愛情有什麼道理.md" title="wikilink">愛情有什麼道理</a>（<a href="../Page/:en:The_Thing_Called_Love.md" title="wikilink">The Thing Called Love</a>）</p></td>
<td><p>1993</p></td>
<td><p><a href="../Page/琳達·琳登.md" title="wikilink">琳達·琳登</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/越空狂龍.md" title="wikilink">越空狂龍</a>（Demolition Man）</p></td>
<td><p>1993</p></td>
<td><p>萊妮那·賀斯利</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蠻荒天使.md" title="wikilink">蠻荒天使</a>（Fire on the Amazon）</p></td>
<td><p>1993</p></td>
<td><p>亞歷莎·洛斯曼</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/老當益壯.md" title="wikilink">老當益壯</a>（Wrestling Ernest Hemingway）</p></td>
<td><p>1993</p></td>
<td><p>亞蘭</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Who_Do_I_Gotta_Kill?.md" title="wikilink">Who Do I Gotta Kill?</a></p></td>
<td><p>1994</p></td>
<td><p>蘿里</p></td>
<td><p>又名Me &amp; The Mob</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/捍衛戰警.md" title="wikilink">捍衛戰警</a>（Speed）</p></td>
<td><p>1994</p></td>
<td><p>安妮·波特</p></td>
<td><p>木星獎最佳國際女演員<br />
MTV電影獎最佳女演員<br />
MTV电影奖最佳银幕情侣<br />
MTV電影獎最性感女演員<br />
<a href="../Page/土星獎.md" title="wikilink">土星獎最佳電影女主角</a><br />
<a href="../Page/en:Jupiter_Award_(film_award).md" title="wikilink">木星獎最佳國際女演員</a><br />
提名—MTV電影獎最佳親吻<br />
提名—<a href="../Page/尼克頻道兒童選擇獎.md" title="wikilink">尼克頻道兒童選擇獎最受喜愛電影女演員</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/二見鍾情.md" title="wikilink">二見鍾情</a>（While You Were Sleeping）</p></td>
<td><p>1995</p></td>
<td><p>露西·亞琳諾·摩德萊茲</p></td>
<td><p>木星獎最佳國際女演員<br />
提名—<a href="../Page/金球獎最佳音樂及喜劇類電影女主角.md" title="wikilink">金球獎最佳音樂及喜劇類電影女主角</a><br />
提名—MTV電影獎最佳女演員<br />
提名—MTV電影獎最性感女演員<br />
提名—美國喜劇獎電影類女演員</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/網路上身.md" title="wikilink">網路上身</a>（The Net）</p></td>
<td><p>1995</p></td>
<td><p>安琪拉·班奈特/露絲·瑪斯</p></td>
<td><p>木星獎最佳國際女演員</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/偷心計劃.md" title="wikilink">偷心計劃</a> (Two If by Sea)</p></td>
<td><p>1996</p></td>
<td><p>蘿茲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/殺戮時刻.md" title="wikilink">殺戮時刻</a>（A Time to Kill）</p></td>
<td><p>1996</p></td>
<td><p>亞倫·蘿亞克</p></td>
<td><p>木星獎最佳國際女演員<br />
大熱門娛樂獎懸疑類最受喜愛女演員<br />
提名—MTV電影獎最佳女演員</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/永遠愛你.md" title="wikilink">永遠愛你</a>（In Love and War）</p></td>
<td><p>1996</p></td>
<td><p>亞妮斯·柯洛斯基</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/捍衛戰警2.md" title="wikilink">捍衛戰警2：喋血巡洋</a>（Speed 2: Cruise Control）</p></td>
<td><p>1997</p></td>
<td><p>安妮·波特</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/真愛告白.md" title="wikilink">真愛告白</a>（Hope Floats）</p></td>
<td><p>1998</p></td>
<td><p>伯蒂·普魯特</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/超異能快感.md" title="wikilink">超異能快感</a>（Practical Magic）</p></td>
<td><p>1998</p></td>
<td><p>莎莉·歐文</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/埃及王子.md" title="wikilink">埃及王子</a>（The Prince of Egypt）</p></td>
<td><p>1998</p></td>
<td><p>米利暗</p></td>
<td><p>配音</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/觸電之旅.md" title="wikilink">觸電之旅</a>（Force of Nature）</p></td>
<td><p>1999</p></td>
<td><p>莎拉·路易斯</p></td>
<td><p>美國青少年選擇獎票選脾氣獎<br />
提名-大熱門娛樂獎喜劇類最受喜愛女演員<br />
提名—尼克頻道兒童選擇獎最受喜愛電影女演員<br />
提名—尼克頻道兒童選擇獎最受喜愛熒幕情侶</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/愛情槍手.md" title="wikilink">愛情槍手</a>（Gun Shy）</p></td>
<td><p>2000</p></td>
<td><p>茱蒂·緹普</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/28天.md" title="wikilink">28天</a>（28 Days）</p></td>
<td><p>2000</p></td>
<td><p>葛雯·康明斯</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/麻辣女王.md" title="wikilink">麻辣女王</a>（Miss Congeniality）</p></td>
<td><p>2000</p></td>
<td><p>葛瑞西·哈特/葛瑞西·陸·弗里布許</p></td>
<td><p>美国喜剧奖电影类最佳女主角<br />
提名—<a href="../Page/金球奖.md" title="wikilink">金球奖音乐喜剧类最佳女主角</a><br />
提名—<a href="../Page/衛星獎.md" title="wikilink">衛星獎音乐喜剧类最佳女主角</a><br />
提名—美國青少年選擇獎票選女演員<br />
提名—美國青少年選擇獎票選脾氣獎</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/拿命線索.md" title="wikilink">拿命線索</a>（<a href="../Page/:en:Murder_by_Numbers.md" title="wikilink">Murder by Numbers</a>）</p></td>
<td><p>2002</p></td>
<td><p>凱西·美薇樂/傑西卡·瑪莉·赫德森</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/YaYa私密日記.md" title="wikilink">YaYa私密日記</a>（Divine Secrets of the Ya-Ya Sisterhood）</p></td>
<td><p>2002</p></td>
<td><p>細達·華克</p></td>
<td><p>提名—美國青少年選擇獎票選戲劇類女演員</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/貼身情人.md" title="wikilink">貼身情人</a>（Two Weeks Notice）</p></td>
<td><p>2002</p></td>
<td><p>露西·凱爾森</p></td>
<td><p>提名—美國青少年選擇獎票選喜劇類女演員<br />
提名—美國青少年選擇獎票選脾氣獎</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/衝擊效應.md" title="wikilink">衝擊效應</a>（Crash）</p></td>
<td><p>2004</p></td>
<td><p>津·凱伯特</p></td>
<td><p><a href="../Page/廣播影評人協會.md" title="wikilink">廣播影評人協會獎最佳群戲</a><br />
好萊塢電影獎年度群戲<br />
<a href="../Page/美國演員工會獎.md" title="wikilink">美國演員工會獎最佳群戲</a><br />
提名—<a href="../Page/哥譚獨立電影獎.md" title="wikilink">哥譚獨立電影獎最佳群戲</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/我的小親親.md" title="wikilink">我的小親親</a> （Loverboy）</p></td>
<td><p>2005</p></td>
<td><p>哈克太太</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/麻辣女王2.md" title="wikilink">麻辣女王2</a>：美麗的要命（Miss Congeniality 2: Armed &amp; Fabulous）</p></td>
<td><p>2005</p></td>
<td><p>葛瑞西·哈特</p></td>
<td><p>美國青少年選擇獎票選喜劇類女演員</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/跳越時空的情書.md" title="wikilink">跳越時空的情書</a>（The Lake House）</p></td>
<td><p>2006</p></td>
<td><p>凱特·福斯特</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/聲名狼藉.md" title="wikilink">柯波帝：冷血污名</a>（Infamous）</p></td>
<td><p>2006</p></td>
<td><p><a href="../Page/哈波·李.md" title="wikilink">奈莉·哈波·李</a></p></td>
<td><p>好萊塢電影獎年度女配角</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/亡命感應.md" title="wikilink">亡命感應</a>（Premonition）</p></td>
<td><p>2007</p></td>
<td><p>琳達·漢森</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/愛情限時簽.md" title="wikilink">愛情限時簽</a>（The Proposal）[7]</p></td>
<td><p>2009</p></td>
<td><p>瑪格麗特·泰特</p></td>
<td><p>伦勃朗奖最佳國際女演員<br />
美國青少年選擇獎票選愛情喜劇類女演員<br />
提名-<a href="../Page/金球奖.md" title="wikilink">金球奖音乐喜剧类最佳女主角</a><br />
提名—衛星獎音乐喜剧类最佳女主角<br />
提名—MTV电影奖最佳喜剧表演<br />
提名—MTV电影奖最佳吻戏</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/求愛女王.md" title="wikilink">求愛女王</a>（All About Steve）</p></td>
<td><p>2009</p></td>
<td><p>瑪麗·霍洛維茲</p></td>
<td><p>金酸梅最差女主角<br />
金酸梅醬最差熒幕情侶</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/弱点_(电影).md" title="wikilink">攻其不備</a> (The Blind Side)</p></td>
<td><p>2009</p></td>
<td><p>李·安妮·杜希</p></td>
<td><p><a href="../Page/奥斯卡金像奖.md" title="wikilink">奥斯卡金像奖最佳女主角</a><br />
<a href="../Page/金球獎最佳戲劇類電影女主角.md" title="wikilink">金球獎最佳戲劇類電影女主角</a><br />
<a href="../Page/美國演員工會獎最佳女主角.md" title="wikilink">美國演員工會獎最佳女主角</a><br />
<a href="../Page/廣播影評人協會.md" title="wikilink">廣播影評人協會最佳女主角</a><br />
美國青少年選擇獎票選戲劇類女演員<br />
提名—達拉斯沃思堡影評人協會獎最佳女主角<br />
提名—丹佛影評人協會獎最佳女主角<br />
提名-休斯頓影評人協會獎最佳女主角<br />
提名—MTV电影奖最佳女演员<br />
提名—尼克頻道兒童選擇獎最受喜愛電影女演員<br />
提名—聖地亞哥影評人協會獎最佳女主角<br />
提名—华盛顿影评人协会最佳女主角</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/心靈鑰匙.md" title="wikilink">心靈鑰匙</a>（Extremely Loud and Incredibly Close）</p></td>
<td><p>2011</p></td>
<td><p>Linda Schell</p></td>
<td><p>提名—喬治亞影評人協會獎最佳女配角<br />
提名—美國青少年選擇獎票選戲劇類女演員</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/麻辣嬌鋒.md" title="wikilink">麻辣嬌鋒</a>（The Heat）</p></td>
<td><p>2013</p></td>
<td><p>莎拉·雅許朋</p></td>
<td><p><a href="../Page/人民選擇獎.md" title="wikilink">人民選擇獎最受喜愛喜劇類電影女主角</a><br />
提名—<a href="../Page/廣播影評人協會.md" title="wikilink">廣播影評人協會喜劇類最佳女主角</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/地心引力_(電影).md" title="wikilink">地心引力</a>（Gravity）</p></td>
<td><p>2013</p></td>
<td><p>蕾恩·史東</p></td>
<td><p>非裔影評人協會獎最佳女主角<br />
女性電影記者聯盟獎最佳動作女演員<br />
女性電影記者聯盟獎超越年齡女演員<br />
<a href="../Page/:en:19th_Critics&#39;_Choice_Awards.md" title="wikilink">廣播影評人協會獎動作電影類最佳女主角</a><br />
好萊塢電影節年度女演員<br />
休斯頓影評人協會獎最佳女主角<br />
木星獎最佳國際女演員<br />
堪薩斯影評人協會獎最佳女主角<br />
<a href="../Page/棕櫚泉國際電影節.md" title="wikilink">棕櫚泉國際電影節年度女演員</a><br />
<a href="../Page/人民選擇獎.md" title="wikilink">人民選擇獎最受喜愛戲劇類電影女主角</a><br />
土星獎最佳電影女主角<br />
提名—<a href="../Page/奥斯卡金像奖.md" title="wikilink">奥斯卡金像奖最佳女主角</a><br />
提名—英國電影學院獎最佳女主角<br />
提名—<a href="../Page/衛星獎.md" title="wikilink">衛星獎電影類最佳女主角</a><br />
提名—<a href="../Page/:en:AACTA_International_Award_for_Best_Actress.md" title="wikilink">澳洲電影學院獎國際類最佳女主角</a><br />
提名—女性電影記者聯盟獎最佳女主角<br />
提名—女性電影記者聯盟獎年度女性偶像<br />
提名—<a href="../Page/廣播影評人協會.md" title="wikilink">廣播影評人協會最佳女主角</a><br />
提名—芝加哥影評人協會獎最佳女主角<br />
提名—達拉斯沃思堡影評人協會獎最佳女主角<br />
提名—丹佛影評人協會獎最佳女主角<br />
提名—都柏林影評人協會獎最佳女主角<br />
提名—喬治亞影評人協會獎最佳女主角<br />
提名—<a href="../Page/金球獎最佳戲劇類電影女主角.md" title="wikilink">金球獎最佳戲劇類電影女主角</a><br />
提名—愛荷華影評人協會獎最佳女主角<br />
提名—北凱洛萊納影評人協會獎最佳女主角<br />
提名—鳳凰城影評人協會獎最佳女主角<br />
提名—聖地亞哥影評人協會獎最佳女主角<br />
提名—舊金山影評人協會獎最佳女主角<br />
提名—<a href="../Page/美國演員工會獎最佳女主角.md" title="wikilink">美國演員工會獎最佳女主角</a><br />
提名—聖路易斯影評人協會獎最佳女主角<br />
提名—猶他影評人協會獎最佳女主角<br />
提名—溫哥華影評人協會獎最佳女主角<br />
提名—華盛頓影評人協會獎最佳女主角</p></td>
</tr>
<tr class="odd">
<td><p>首相：先驅（The Prime Ministers: The Pioneers）</p></td>
<td><p>2013</p></td>
<td><p><a href="../Page/果尔达·梅厄.md" title="wikilink">果尔达·梅厄</a></p></td>
<td><p>紀錄片，配音</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/小小兵_(電影).md" title="wikilink">小小兵</a>（Minions）</p></td>
<td><p>2015</p></td>
<td><p>史嘉蕾·殺很大</p></td>
<td><p>配音</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/危機女王.md" title="wikilink">危機女王</a>（Our Brand is Crises）</p></td>
<td><p>2015</p></td>
<td><p>珍·柏汀</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/瞞天過海：八面玲瓏.md" title="wikilink">瞞天過海：八面玲瓏</a>（Ocean's 8）</p></td>
<td><p>2018</p></td>
<td><p>黛比·奧申</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蒙上你的眼.md" title="wikilink">蒙上你的眼</a>（Bird Box）</p></td>
<td><p>2018</p></td>
<td><p>梅樂莉·海耶斯</p></td>
<td></td>
</tr>
</tbody>
</table>

### 電視劇

| 片名                                                               | 年份        | 角色              | 備註                        |
| ---------------------------------------------------------------- | --------- | --------------- | ------------------------- |
| Bionic Showdown: The Six Million Dollar Man and the Bionic Woman | 1989      | Kate Mason      | 電視電影                      |
| Starting from Scratch                                            | 1989      | Barbara Webster | Episode:"Confidence Game" |
| The Preppie Murder                                               | 1989      | Stacy           | 電視電影                      |
| Working Girl                                                     | 1990      | Tess McGill     |                           |
| [Lucky Chances](../Page/Lucky_Chances.md "wikilink")             | 1990      | 瑪麗亞·聖坦蘿         | 迷你劇                       |
| [乔治·洛佩兹秀](../Page/乔治·洛佩兹.md "wikilink") (George Lopez)           | 2002−2004 | Accident Amy    | 客串，同時擔任本劇製作人              |

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

  -

  -

  - [荷蘭珊卓布拉克後援會](http://www.sandrabullock.nl/)

  - [珊卓布拉克中央影迷社區](https://web.archive.org/web/20050119100314/http://www.sandrabullockcentral.com/)

[Category:奧斯卡最佳女主角獎獲獎演員](../Category/奧斯卡最佳女主角獎獲獎演員.md "wikilink")
[Category:金球獎最佳電影女主角獲得者](../Category/金球獎最佳電影女主角獲得者.md "wikilink")
[Category:金酸莓獎獲獎演員](../Category/金酸莓獎獲獎演員.md "wikilink")
[Category:好萊塢星光大道](../Category/好萊塢星光大道.md "wikilink")
[Category:21世纪美国女演员](../Category/21世纪美国女演员.md "wikilink")
[Category:美國喜剧演员](../Category/美國喜剧演员.md "wikilink")
[Category:美國電視女演員](../Category/美國電視女演員.md "wikilink")
[Category:美國電影女演員](../Category/美國電影女演員.md "wikilink")
[Category:美國電影监制](../Category/美國電影监制.md "wikilink")
[Category:美國女性企業家](../Category/美國女性企業家.md "wikilink")
[Category:东卡罗莱纳大学校友](../Category/东卡罗莱纳大学校友.md "wikilink")
[Category:維吉尼亞州人](../Category/維吉尼亞州人.md "wikilink")
[Category:德國裔美國人](../Category/德國裔美國人.md "wikilink")
[Category:20世纪美国女演员](../Category/20世纪美国女演员.md "wikilink")
[Category:美国演员工会奖最佳电影群体演出奖得主](../Category/美国演员工会奖最佳电影群体演出奖得主.md "wikilink")

1.  Sandra Bullock Revealed, E Channel Special (2000)
2.
3.
4.  [The 20 Richest Women In
    Entertainment](http://www.forbes.com/2007/01/17/richest-women-entertainment-tech-media-cz_lg_richwomen07_0118womenstars_lander.html)
    *Forbes.com*
5.
6.
7.  [Fox 2000 aims to solve
    puzzle](http://variety.com/2006/film/news/fox-2000-aims-to-solve-puzzle-1117951828/)