《**ONE
PIECE**》（，中文譯名：海賊王、航海王）是[日本](../Page/日本.md "wikilink")[漫畫家](../Page/漫畫家.md "wikilink")[尾田榮一郎創作的一部著名的](../Page/尾田榮一郎.md "wikilink")[少年漫畫作品](../Page/少年漫畫.md "wikilink")，至今仍連載中，該作品內容為描述一名少年**[蒙其·D·魯夫](../Page/蒙其·D·魯夫.md "wikilink")**想要得到「**ONE
PIECE**」和成為「**海賊王**」的[故事](../Page/故事.md "wikilink")。

該作於[1997年起在日本](../Page/1997年.md "wikilink")[漫畫](../Page/漫畫.md "wikilink")[雜誌](../Page/雜誌.md "wikilink")《[週刊少年Jump](../Page/週刊少年Jump.md "wikilink")》（[集英社](../Page/集英社.md "wikilink")）定期連載，之後被改編為各種系列的[衍生作品](../Page/ONE_PIECE#衍生作品.md "wikilink")，主要包括同名的[電視動畫](../Page/ONE_PIECE_\(動畫\).md "wikilink")、[電影](../Page/ONE_PIECE_\(動畫\)#電影.md "wikilink")、[小說和](../Page/ONE_PIECE#輕小說.md "wikilink")[電子遊戲等周邊](../Page/ONE_PIECE#電子遊戲.md "wikilink")[媒體產品](../Page/媒體.md "wikilink")。

## 作品概述

財富、名聲、勢力、擁有世界上的一切的[「海賊王」哥爾·D·羅傑在即將遭到處刑前說了一段話](../Page/哥爾·D·羅傑.md "wikilink")：「想要我的財寶嗎？想要的話可以全部給你。去找吧！我把所有的財寶都放在那裡」！後來世界上的人們將這個寶藏稱作「大秘寶」（ONE
PIECE），為了爭奪大秘寶「ONE
PIECE」，而先後爭相出海成為[海賊](../Page/海賊.md "wikilink")，因此造就了「大海賊時代」。主角蒙其·D·魯夫為了要實現與[“紅髮”傑克的約定而出海](../Page/“紅髮”傑克.md "wikilink")，在遙遠的路途上找尋著志同道合的夥伴，進入「偉大的航道」，目標是得到「ONE
PIECE」與成為「海賊王」。

本作品連載多年來以宏偉壯大的世界觀與精心設計的故事為特色，除了擁有熱血的冒險故事以及戰鬥，背後也涵蓋許多反諷元素及現實觀點，包括“[戰爭的殘酷](../Page/戰爭.md "wikilink")、種族[歧視問題](../Page/歧視.md "wikilink")、[世界政府的黑暗](../Page/ONE_PIECE用語列表#世界政府.md "wikilink")、人性善惡”……等；至於故事中龐大豐富的架空世界，含有許多[童話](../Page/童話.md "wikilink")、[神話和](../Page/神話.md "wikilink")[魔幻元素在其中](../Page/魔幻.md "wikilink")：[天空中夢幻的](../Page/天.md "wikilink")[神域](../Page/神域.md "wikilink")“[空島](../Page/空島.md "wikilink")”、充滿[夢魘的](../Page/夢魘.md "wikilink")“[魔鬼三角地帶](../Page/魔鬼三角地帶.md "wikilink")”、魚人及[人魚的](../Page/人魚.md "wikilink")[世界](../Page/世界.md "wikilink")“[魚人島](../Page/魚人島.md "wikilink")”、愛與激情的[玩具之國](../Page/玩具.md "wikilink")“[多雷斯羅薩](../Page/多雷斯羅薩.md "wikilink")”、[動物](../Page/動物.md "wikilink")[王國](../Page/王國.md "wikilink")“[佐烏](../Page/佐烏.md "wikilink")”、滿是甜食的國度“托特蘭王國”、[武士的國度](../Page/武士.md "wikilink")“[和之國](../Page/和之國.md "wikilink")”
等，大部分敵人設定性格殘忍，不擇手段。

在日本漫畫雜誌《[週刊少年Jump](../Page/週刊少年Jump.md "wikilink")》中長期佔據人氣榜的第一名；單行本在日本以外，亦已有30多個[翻譯版本發行](../Page/翻譯.md "wikilink")2012年5月10日《ONE
PIECE》成為日本[Oricon排行榜從](../Page/Oricon.md "wikilink")2008年4月開始統計漫畫銷量以來，在該榜中首個總銷量破億冊的漫畫\[1\]。目前全世界總發行量已經突破**4億3,000萬本**（2017年10月）。

在2015年6月15日，[尾田榮一郎以本作品發行量打破世界紀錄](../Page/尾田榮一郎.md "wikilink")，獲得獲頒「單一作者創作發行量最多的漫畫」的[金氏世界紀錄](../Page/金氏世界紀錄.md "wikilink")\[2\]，尾田榮一郎獲得這獎項因此還畫了張魯夫拿著金氏世界紀錄的證書。

改編的動畫於1999年[富士電視台播出大獲好評](../Page/富士電視台.md "wikilink")，也是[東映動畫製作動畫作品中時間最長的作品](../Page/東映動畫.md "wikilink")。從2010年起，陸續與許多個[公司實施合作](../Page/公司.md "wikilink")，並在日本全國各地舉辦各種活動，也逐漸拓展在[日本海外國家及地區](../Page/日本.md "wikilink")（[美國](../Page/美國.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[香港等](../Page/香港.md "wikilink")）舉辦相關活動。

## 歷史沿革

  - 1996年，在《[週刊少年Jump](../Page/週刊少年Jump.md "wikilink")》1996年41号和《[週刊少年Jump的增刊號](../Page/週刊少年Jump的增刊號.md "wikilink")》刊載本作的原型作品《[ROMANCE
    DAWN](../Page/ONE_PIECE_ROMANCE_DAWN_STORY.md "wikilink")》。
  - 1997年
      - 7月22日，於《週刊少年Jump》1997年34号（8月4日號）正式連載開始。
      - 12月，漫畫單行本第1冊出版发行。
  - 1998年，初次动画作品《[打倒！海賊強薩克！](../Page/ONE_PIECE_打倒！海贼强萨克！.md "wikilink")》於[Jump
    Festa放映](../Page/Jump_Festa.md "wikilink")。
  - 1999年10月，由[東映動畫製作的同名电视动画版開始播放](../Page/東映動畫.md "wikilink")。
  - 2000年
      - 3月，劇場版第1作《[黃金島大冒險](../Page/ONE_PIECE_黃金島大冒險.md "wikilink")》上映。
      - 7月，第1個家用游戏《ONE PIECE 〜めざせ海賊王\!〜》发售。
  - 2001年7月，第1本画集《ONE PIECE 尾田栄一郎画集 COLOR WALK》发售。
  - 2002年7月，單行本第24冊的初版发行量漫畫最高紀錄初版252萬本（更新中）。
  - 2005年2月，單行本累計發行量在日本漫畫歷史中最快超過1億冊（第36冊）。
  - 2006年12月，《週刊少年Jump》2007年4·5合并号，刊載與[鳥山明](../Page/鳥山明.md "wikilink")《[龍珠](../Page/龍珠.md "wikilink")》的合作作品《CROSS
    EPOCH》。
  - 2007年7月，周刊少年JUMP增刊連載十週年舉辦慶祝活动。
  - 2009年12月，劇場版第10作《[ONE PIECE FILM STRONG
    WORLD](../Page/ONE_PIECE_FILM_STRONG_WORLD.md "wikilink")》公映，為当時劇場版系列歴代最高的票房纪录：48億日元。
  - 2010年
      - 3月，單行本第57巻初版發行量日本出版史上最高纪錄初版300萬本（以後更新中）。
      - 8月，《週刊少年Jump》2010年39號刊載的第597話原作第1部結束，到目前為止的故事用《生存之海
        超新星篇》作為副標题。4週的休刊期間後，44號（10月4日發售）新篇章《最后之海
        新世界篇》開始。
      - 11月，單行本累計發行量超過2億冊（60巻）\[3\]。
  - 2011年4月，《週刊少年Jump》2011年17号，与[島袋光年作](../Page/島袋光年.md "wikilink")《[美食的俘虏](../Page/美食的俘虏.md "wikilink")》的合作作品《开吃！恶魔果实！！》刊载。
  - 2012年
      - 3月，為記念連載15周年，初次举办展覧《尾田栄一郎監修 ONE PIECE展 〜原画×映像×体感的海贼王》。
      - 12月，劇場版第12作《[ONE PIECE FILM
        Z](../Page/ONE_PIECE_FILM_Z.md "wikilink")》公映，為劇場版系列歴代最高的票房纪录：68.7億日元。
  - 2013年
      - 11月，單行本累計發行量超過3億冊（第72冊），為歷史上發行量最多的日本漫畫系列。
      - 11月，尾田榮一郎公認的官方公式網站『ONE PIECE.com』正式啟動。
  - 2014年
      - 8月，電視動畫15周年特別篇《[ONE PIECE “3D2Y” 跨越艾斯之死\!
        魯夫與夥伴的誓言](../Page/ONE_PIECE_“3D2Y”_跨越艾斯之死!_魯夫與夥伴的誓言.md "wikilink")》。空白的兩年的故事，魯夫經歷了什麼樣的艱苦霸氣修煉？
      - 12月，單行本累計銷量已突破3億冊，改編成動畫後，木棉花推出『海賊王 航向2015』新年特別企劃。
  - 2015年
      - 8月，電視動畫製作啟動特別篇《[ONE PIECE 薩波特別篇 〜3兄弟的羈絆
        奇跡的再會和被繼承的意志〜](../Page/ONE_PIECE_薩波特別篇_〜3兄弟的羈絆_奇跡的再會和被繼承的意志〜.md "wikilink")》。將從頂點戰爭的兩年後開始講起。
      - 10月，「超級歌舞伎2 ONE PIECE」公演開幕。
  - 2016年
      - 7月，劇場版第13作《[ONE PIECE FILM
        GOLD](../Page/ONE_PIECE_FILM_GOLD.md "wikilink")》公映。
      - 11月，為了支援熊本地震災害，開始啟動熊本復興企劃實施中。
  - 2017年
      - 7月，本作品已達20周年，陸續推出20周年企畫的相關活動。
      - 7月，為記念連載20周年、將連載開始日的7月22日設為「ONE PIECE日」。

## 劇情大綱

[Thousand_Sunny.JPG](https://zh.wikipedia.org/wiki/File:Thousand_Sunny.JPG "fig:Thousand_Sunny.JPG")

### 生存之海 超新星篇

為《ONE PIECE》的第一部故事。收錄卷數為第1～61卷；收錄話數為第1～597話。

  -
    **[蒙其·D·魯夫](../Page/蒙其·D·魯夫.md "wikilink")**童年時受到海賊[「紅髮」傑克的啟蒙](../Page/「紅髮」傑克.md "wikilink")，自小就立志要成為「**海賊王**」。17歲時他出海之後，接續遇見了海賊獵人**[羅羅亞·索隆](../Page/羅羅亞·索隆.md "wikilink")**、航海士**[娜美](../Page/娜美.md "wikilink")**、狙擊手**[騙人布](../Page/騙人布.md "wikilink")**、廚師**[賓什莫克·香吉士](../Page/賓什莫克·香吉士.md "wikilink")**等人並結為夥伴。他們組成了草帽海賊團準備進入偉大的航道來到了羅格鎮。離開羅格鎮後，一行人在偉大航道的入口「顛倒山」之前以各自的夢想立誓，跨越顛倒山之後進入了偉大航道，展開下一段冒險。
  -
    草帽海賊團一行人來到偉大航道、在威士忌山峰遇到臥底在秘密犯罪組織[巴洛克華克的](../Page/巴洛克華克.md "wikilink")[阿拉巴斯坦王國公主](../Page/阿拉巴斯坦王國.md "wikilink")[納菲魯塔莉·薇薇](../Page/納菲魯塔莉·薇薇.md "wikilink")，從公主口中得知阿拉巴斯坦正處於水深火熱之中，以及[王下七武海](../Page/王下七武海.md "wikilink")[『Mr.0』克洛克達爾的陰謀後](../Page/克洛克達爾.md "wikilink")，魯夫決定幫助這位公主。在途中前往島嶼「小花園」與[巴洛克華克的部分成員開戰](../Page/巴洛克華克.md "wikilink")，並在旅途中為了治好在小花園生病的娜美去了[磁鼓島並增添一名船醫夥伴](../Page/ONE_PIECE角色列表#磁鼓島「櫻花王國」.md "wikilink")[多尼多尼·喬巴](../Page/多尼多尼·喬巴.md "wikilink")。
  -
    抵達阿拉巴斯坦後，當地因為克洛克達爾的煽動爆發內戰，魯夫最後擊敗克洛克達爾，內戰亦隨之結束。其後薇薇向魯夫等人道別，而原[巴洛克華克副社長](../Page/巴洛克華克.md "wikilink")**[妮可·羅賓](../Page/妮可·羅賓.md "wikilink")**，因失去人生目標，以考古學家的身份加入了草帽海賊團。
  -
    離開阿拉巴斯坦王國之後，草帽海賊團撞見一艘從天而降的大船，同時紀錄指針竟然向上指向天空。對[空島產生興趣的魯夫靠著上升海流前往空島](../Page/空島.md "wikilink")。誤入了空島統治者[「神」艾涅爾的領地](../Page/空島#艾涅爾.md "wikilink")，草帽海賊團、神的軍團、以及領地原住民「香狄亞」展開激戰，艾涅爾最終被擊敗，而空島居民和香狄亞一族長達400年的戰爭也終止。
  -
    草帽海賊團遇見專門以Davy Back
    Fight遊戲搶奪其它海賊團船員的[「銀狐」弗克西](../Page/ONE_PIECE海賊列表#弗克西.md "wikilink")，船醫喬巴被其搶走，兩個海賊團展開對決。
  -
    為修繕傷痕累累的[前進梅利號](../Page/前進梅利號.md "wikilink")，草帽海賊團來到以造船業聞名的水之七島，卻得知梅莉號已經無法修復的消息，騙人布又慘敗給拆船流氓**[佛朗基](../Page/佛朗基.md "wikilink")**，因而離團。另一方面，羅賓遇見了政府機關[CP9](../Page/世界政府直屬秘密間諜機關#CP9.md "wikilink")，由於害怕過去發生在自己故鄉的悲劇重演在草帽海賊團上，因此自願被CP9帶走。而騙人布又因為不滿魯夫換掉梅利號的決定而決定離開，草帽海賊團陷入分崩離析的危機。
  -
    草帽海賊團前往司法島救回羅賓，與世界政府底下的CP9部隊全面對決，最後救回羅賓，騙人布也回到團裡。魯夫以火葬儀式為大限已至的梅利號送行，而佛朗基用當初偷來的2億貝里建造一艘新船[千陽號](../Page/千陽號.md "wikilink")，繼續出海冒險，而佛朗基也以船匠的身分加入海賊團。
  -
    草帽海賊團踏入魔幻的三桅帆船地帶，遇見神秘的骷髏人**[布魯克](../Page/布魯克_\(ONE_PIECE\).md "wikilink")**，並與王下七武海之一的[月光·摩利亞產生衝突](../Page/月光·摩利亞.md "wikilink")。最後魯夫擊敗了摩利亞，布魯克也以音樂家的身分加入草帽海賊團。
  -
    草帽海賊團進入偉大航道前半段的最末端夏波帝諸島，和世界貴族[天龍人起了衝突](../Page/ONE_PIECE角色列表#世界貴族「天龍人」.md "wikilink")，之後遇到[海軍上將](../Page/海軍_\(ONE_PIECE\).md "wikilink")[黃猿](../Page/黃猿.md "wikilink")、[戰桃丸以及改造人攻擊](../Page/海軍_\(ONE_PIECE\)#戰桃丸.md "wikilink")，到最後[巴索羅謬·大熊利用果實幫助草帽海賊團逃脫](../Page/革命軍_\(ONE_PIECE\)#巴索羅謬·大熊.md "wikilink")，把各個船員擊飛至不同的島嶼。
  -
    被擊飛至女人島的魯夫遇見島上的[「海賊女帝」波雅·漢考克](../Page/波雅·漢考克.md "wikilink")，又得知自己的兄長[「火拳」波特卡斯·D·艾斯要被處決](../Page/白鬍子海賊團#波特卡斯·D·艾斯.md "wikilink")，便請蛇姬將自己偷渡至海底大監獄「推進城」。
  -
    魯夫在推進城遇見過去的敵人（[「小丑」巴其](../Page/巴其_\(ONE_PIECE\)#巴其.md "wikilink")、以克洛克達爾為首的巴洛巴華克部分成員）等人，眾人為達成各自的目的，分別先後集結組成聯盟後展開越獄行動，最後魯夫來到處刑的場所海軍本部[馬林福特](../Page/馬林福特.md "wikilink")。
  -
    魯夫及越獄同伴、以及為了救艾斯而來的[白鬍子海賊團](../Page/白鬍子海賊團.md "wikilink")、[海軍的三上將以及眾多海軍展開了](../Page/海軍_\(ONE_PIECE\).md "wikilink")「頂點戰爭」。戰爭最後由「紅髮」傑克出面調停而收場，但遺憾的是艾斯和白鬍子皆殞命於戰場。戰爭結束後，失去了哥哥艾斯並深感自身實力不足的魯夫，在[「冥王」雷利的建議下給夥伴留下了訊息](../Page/ONE_PIECE海賊列表#席爾巴斯·雷利.md "wikilink")，之後展開了為期兩年的[霸氣修行之旅](../Page/ONE_PIECE用語列表#霸氣.md "wikilink")。

### 最後之海 新世界篇

為《ONE PIECE》的第二部故事，收錄卷數為第61卷起；收錄話數為第598話起。

  -
    歷經兩年修行後重逢的草帽海賊團為了前往新世界，而出發邁向位於海底10,000公尺以下處的「[魚人島](../Page/魚人島.md "wikilink")」。他們遇到[吉貝爾並得知魚人島的悲劇歷史](../Page/吉貝爾.md "wikilink")，原本魚人島繼承已故王妃的想法、漸漸地接近與人類和平共處的理想，但是由荷帝·瓊斯所率領的新魚人海賊團卻粉碎了這個美夢。草帽海賊團挺身阻止荷帝所率領的「[新魚人海賊團](../Page/ONE_PIECE海賊列表#新魚人海賊團.md "wikilink")」，並與[白星公主為首的魚人與人類之間開始建立新的羈絆](../Page/白星.md "wikilink")。
  -
    草帽海賊團與新任王下七武海[托拉法爾加·D·瓦特爾·羅相遇](../Page/托拉法爾加·D·瓦特爾·羅.md "wikilink")，魯夫接受羅的提議，組成了「海賊同盟」，目標是「[四皇](../Page/四皇.md "wikilink")」之一的[「百獸」海道](../Page/ONE_PIECE海賊列表#海道.md "wikilink")。他們靠著這強力的同盟來對抗在龐克哈薩特製造人工惡魔果實的[凱薩·克勞恩](../Page/ONE_PIECE角色列表#凱薩·克勞恩.md "wikilink")。
  -
    草帽海賊團和羅潛入到[唐吉訶德·多佛朗明哥治理下的王國](../Page/唐吉訶德·多佛朗明哥.md "wikilink")「[多雷斯羅薩](../Page/多雷斯羅薩.md "wikilink")」，與多佛朗明哥旗下人馬展開對決，期間與[革命軍參謀總長的義兄](../Page/革命軍.md "wikilink")[薩波重逢](../Page/革命軍_\(ONE_PIECE\)#薩波.md "wikilink")，在擊敗多佛朗明哥後，多雷斯羅薩的悲劇就此結束。而作為檯面下引路人的多佛朗明哥的垮台，也使得世界局勢開始出現大幅變化。
  -
    魯夫等人到佐烏國家與娜美等人會合，但此國家卻已呈現潰滅狀態。一行人追查發生在這個國家的秘密，卻意外發現香吉士出身為殺手家族。魯夫一行人為了阻止其結婚，正在思考救回香吉士的方法時，意外得知了和之國光月一族的秘密。魯夫等人得知這件驚人事實，於是將下一個擊倒的目標鎖定為四皇「百獸」海道。魯夫、索隆與和之國光月一族及佐烏的住民為各自的目的分組團隊。
  -
    魯夫團隊前往四皇[「BIG
    MOM」夏洛特·莉莉所在的](../Page/夏洛特·莉莉.md "wikilink")「萬國」蛋糕島準備奪回要結婚的香吉士。魯夫等人理解香吉士與家族聯姻的真正理由，再度和吉貝爾相會，並與想要暗殺BIG
    MOM的[火戰車海賊團組成暫時聯盟](../Page/ONE_PIECE海賊列表#火戰車海賊團.md "wikilink")，成功破壞雙方聯姻婚禮，救出香吉士和其家族[杰爾馬66](../Page/ONE_PIECE角色列表#杰爾馬66.md "wikilink")。吉貝爾為實踐理念，正式宣布退出BIG
    MOM旗下與辭別[太陽海賊團](../Page/ONE_PIECE海賊列表#太陽海賊團.md "wikilink")，以掌舵手的身分加入草帽海賊團。在一行人達成目的後，逃出BIG
    MOM的領地，期間魯夫與[BIG
    MOM海賊團的](../Page/BIG_MOM海賊團.md "wikilink")[夏洛特·卡塔克利有多次激烈對決](../Page/夏洛特·卡塔克利.md "wikilink")，在最後的對決中獲勝。
  -
    世界政府加盟國裡作為代表的50個國家，再度前往「聖地」馬力喬亞召開每4年一次的世界會議，商討世界大局政策。
  -
    索隆等人按[錦衛門的要求偽裝成](../Page/和之國#錦衛門.md "wikilink")[和之國國民](../Page/和之國.md "wikilink")。另一方面，魯夫等人先後抵達四皇「百獸」海道駐紮的和之國。

## 登場角色

  - [角色列表](../Page/ONE_PIECE角色列表.md "wikilink")（[草帽海賊團](../Page/ONE_PIECE海賊列表#草帽海賊團.md "wikilink")
    - [海賊](../Page/ONE_PIECE海賊列表.md "wikilink") -
    [海軍](../Page/海軍_\(ONE_PIECE\).md "wikilink")）

## 用語與世界觀

本作的世界觀舞台是由世界各地的加盟國與所組成的國際組織「世界政府」所共同管理。然而，由於[「海賊王」哥爾·D·羅傑被執行死刑後迎來了](../Page/哥爾·D·羅傑.md "wikilink")“大海賊時代”，結果海賊們於世界各地擴展權力，並直接與直屬世界政府的海軍作戰。本作是以島上的國家為單位，也有的島嶼只有村子、城鎮存在。

至於生活方式和科學技術，基本上是以現實世界海賊的「黃金時代」（17世紀到18世紀）為藍本，但是與現實世界而言還是擁有很大的差別，以作品中世界固有的獨特設定（例如：偉大航道、紅土大陸和電話蟲等）。[惡魔果實服用後會依不同的果實而對應獲得不可思議的特殊能力](../Page/惡魔果實.md "wikilink")，許多角色因其能力都擁有了超人般的戰鬥力。有著「海王類」這樣的特殊生物，還有像巨人族、魚人族、小人族和毛皮族等這樣的種族，也有超過許多普通人的身高2公尺以上的人類。

以船長[蒙其·D·魯夫為首和他的伙伴組成](../Page/蒙其·D·魯夫.md "wikilink")[草帽海賊團](../Page/ONE_PIECE海賊列表#草帽海賊團.md "wikilink")，但他們並不會對一般民眾進行掠奪（主要是從所有者不明或同為海賊的團隊中掠奪）。它們主要到每座島上遇到問題與事件，在擊敗敵人後，繼續前往下一座島。敵人大多是以海賊為主，但有得海賊會與世界政府勾結或直接隸屬於他們，也因此會與世界政府（主要是海軍）和國家的王的公權力進行挑戰並鬥爭。

## 短期集中刊頭連載

是在航海王各話開頭刊頭（扉頁）上連載的單頁漫畫，常用來交待一些人物的簡單後續發展。

<table>
<thead>
<tr class="header">
<th><p>No</p></th>
<th style="text-align: center;"><p>名稱</p></th>
<th><p>本篇</p></th>
<th><p>連載話數</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td style="text-align: center;"></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td style="text-align: center;"></td>
<td><p>-{小小巴其的大冒險}-</p></td>
<td><p>-{巴基冒險記}-</p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td style="text-align: center;"></td>
<td><p>-{克比和貝魯梅伯的奮鬥日記}-</p></td>
<td><p>-{高比麥普奮鬥日記}-</p></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td style="text-align: center;"></td>
<td><p>-{傑克斯的冒險天國}-</p></td>
<td><p>-{贊高的跳舞天國}-</p></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td style="text-align: center;"></td>
<td><p>-{小八的海底散步}-</p></td>
<td><p>-{八仔的海底散步}-</p></td>
</tr>
<tr class="even">
<td><p>5</p></td>
<td style="text-align: center;"></td>
<td><p>-{瓦波爾的雜食萬歲}-</p></td>
<td><p>-{瓦波爾的雜食萬歲}-</p></td>
</tr>
<tr class="odd">
<td><p>6</p></td>
<td style="text-align: center;"></td>
<td><p>-{艾斯的黑鬍子大搜查線}-</p></td>
<td><p>-{艾斯的黑鬍子大搜查線}-</p></td>
</tr>
<tr class="even">
<td><p>7</p></td>
<td style="text-align: center;"></td>
<td><p>-{涅槃一不留神的藍海生活}-</p></td>
<td><p>-{涅槃在青海的糊塗生活}-</p></td>
</tr>
<tr class="odd">
<td><p>8</p></td>
<td style="text-align: center;"></td>
<td><p>-{Miss黃金週的作戰名「尋找巴洛克」}-</p></td>
<td><p>-{Miss G.W的作戰計劃名叫「Meet巴洛克」}-</p></td>
</tr>
<tr class="even">
<td><p>－</p></td>
<td style="text-align: center;"></td>
<td><p>-{那個人現在}-</p></td>
<td><p>-{那個人現在}-</p></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td style="text-align: center;"></td>
<td><p>-{艾涅爾的宇宙大作戰}-</p></td>
<td><p>-{艾尼路的宇宙大作戰}-</p></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td style="text-align: center;"></td>
<td><p>CP9的任務外報告</p></td>
<td><p>CP9的任務之外報告</p></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td style="text-align: center;"></td>
<td><p>-{香吉士·在地獄中振作}-</p></td>
<td><p>-{山治之身在地獄要保持平常心}-</p></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td style="text-align: center;"></td>
<td><p>-{羅賓·真是過分}-</p></td>
<td><p>-{魯賓之做得好過份啊}-</p></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td style="text-align: center;"></td>
<td><p>-{佛朗基·本週我真爛}-</p></td>
<td><p>-{芬奇之本週的我真不行}-</p></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td style="text-align: center;"></td>
<td><p>-{騙人布·孤單就會死的病}-</p></td>
<td><p>-{烏索普的獨自一人就會死的病}-</p></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td style="text-align: center;"></td>
<td><p>-{喬巴·我不是食物啊，混帳！}-</p></td>
<td><p>-{喬巴之我不是食物呀，混帳}-</p></td>
</tr>
<tr class="even">
<td><p>16</p></td>
<td style="text-align: center;"></td>
<td><p>-{娜美·天氣預報}-</p></td>
<td><p>-{奈美的天氣報告}-</p></td>
</tr>
<tr class="odd">
<td><p>17</p></td>
<td style="text-align: center;"></td>
<td><p>-{布魯克·報一宿一內褲之恩}-</p></td>
<td><p>-{布魯克一宿一短褲的報恩??}-</p></td>
</tr>
<tr class="even">
<td><p>18</p></td>
<td style="text-align: center;"></td>
<td><p>-{索隆·他在哪裡呀？真會添麻煩！}-</p></td>
<td><p>-{卓洛之那班傢伙在哪裡呀，真會添麻煩}-</p></td>
</tr>
<tr class="odd">
<td><p>19</p></td>
<td style="text-align: center;"></td>
<td><p>-{來自全世界的甲板}-</p></td>
<td><p>-{從世界的甲板}-</p></td>
</tr>
<tr class="even">
<td><p>20</p></td>
<td style="text-align: center;"></td>
<td><p>-{卡里布的新世界狂笑曲}-</p></td>
<td><p>-{卡里布的新世界嘿嘻嘻嘻嘻}-</p></td>
</tr>
<tr class="odd">
<td><p>21</p></td>
<td style="text-align: center;"></td>
<td><p>-{海俠吉貝爾單人之旅}-</p></td>
<td><p>-{吉貝爾的海俠孤流}-</p></td>
</tr>
<tr class="even">
<td><p>22</p></td>
<td style="text-align: center;"></td>
<td><p>-{來自全世界的甲板·5億的男人篇}-</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td style="text-align: center;"></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>23</p></td>
<td style="text-align: center;"></td>
<td><p>-{不請自來的草帽大船團小故事}-</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td style="text-align: center;"></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 相關作品

  -
    原作為[尾田榮一郎](../Page/尾田榮一郎.md "wikilink")，在總集篇中刊載的番外篇（數頁的短篇）。
  -
    作畫為[武井宏文](../Page/武井宏文.md "wikilink")，於月刊『最強Jump』2012年1月號～2014年2月號期間刊載。
    起於公式本「ONE PIECE BLUE」中的四格短漫-正義使者《喬巴超人》，《ONE PIECE
    草帽劇場》中的一話。同時電視版也播放出原創劇本的喬巴超人，第336話「喬巴超人出動！保衛海濱的電視台」，故事敘述主人翁喬巴超人帶領其助手娜美菲雅粉碎邪惡組織“騙人達巴達團”陰謀的故事。
  -
    作畫為[安藤英](../Page/安藤英.md "wikilink")（[尾田榮一郎的助手](../Page/尾田榮一郎.md "wikilink")），於「[最強Jump](../Page/最強Jump.md "wikilink")」2015年1月號起刊載。
  -
    原作為[尾田榮一郎](../Page/尾田榮一郎.md "wikilink")，收錄在「ONE PIECE
    magazine」，描述「如果薩波在頂上戰爭中救了艾斯和魯夫，三人再次相聚」以此為展開的特別故事。
  -
    與《[七龍珠](../Page/七龍珠.md "wikilink")》的合作作品，於「週刊少年JUMP」2007年4·5號刊載。
  -
    與《[美食獵人](../Page/美食獵人.md "wikilink")》的合作作品，於「週刊少年JUMP」2011年17號刊載。
  -
    與《[食戟之靈](../Page/食戟之靈.md "wikilink")》的合作作品，於「週刊少年JUMP」2018年34號刊載特別短篇，以[香吉士為主角的短篇故事](../Page/香吉士.md "wikilink")。

## 發行書籍

### 單行本

### 總集篇

| 書名                           | 日版發行日期      | 日版ISBN                 |
| ---------------------------- | ----------- | ---------------------- |
| THE FIRST LOG                | 2005年3月     | ISBN 978-4-08-111020-9 |
| THE 2ND LOG "SANJI"          | 2005年11月    | ISBN 978-4-08-111021-6 |
| THE 3RD LOG "NAMI"           | 2005年12月    | ISBN 978-4-08-111022-3 |
| THE 4TH LOG "GRAND LINE"     | 2006年1月     | ISBN 978-4-08-111023-0 |
| THE 5TH LOG "CHOPPER"        | 2006年2月     | ISBN 978-4-08-111024-7 |
| THE 6TH LOG "ARABASTA"       | 2006年3月     | ISBN 978-4-08-111025-4 |
| THE 7TH LOG "VIVI"           | 2006年4月     | ISBN 978-4-08-111026-1 |
| THE 8TH LOG "SKYPIEA"        | 2008年4月     | ISBN 978-4-08-111027-8 |
| THE 9TH LOG "GOD"            | 2008年5月     | ISBN 978-4-08-111028-5 |
| THE 10TH LOG "BELL"          | 2008年6月     | ISBN 978-4-08-111029-2 |
| THE 11TH LOG "WATER SEVEN"   | 2009年4月     | ISBN 978-4-08-111009-4 |
| THE 12TH LOG "ROCKET MAN"    | 2009年5月     | ISBN 978-4-08-111010-0 |
| THE 13TH LOG "NICO ROBIN"    | 2009年7月     | ISBN 978-4-08-111011-7 |
| THE 14TH LOG "FRANKY"        | 2009年8月     | ISBN 978-4-08-111012-4 |
| THE 15TH LOG "THRILLER BARK" | 2011年2月     | ISBN 978-4-08-111033-9 |
| THE 16TH LOG "BROOK"         | 2011年2月     | ISBN 978-4-08-111035-3 |
| THE 17TH LOG "SABÃODY"       | 2012年9月14日  | ISBN 978-4-08-111052-0 |
| THE 18TH LOG "IMPEL DOWN"    | 2012年10月12日 | ISBN 978-4-08-111053-7 |
| THE 19TH LOG "MARINEFORD"    | 2012年11月9日  | ISBN 978-4-08-111054-4 |
| XTRA LOG 1 "STRONG WORLD"    | 2012年12月4日  | ISBN 978-4-08-111061-2 |
| THE 20TH LOG "ACE"           | 2012年12月14日 | ISBN 978-4-08-111055-1 |
| THE 21ST LOG "2 YEARS LATER" | 2017年6月23日  | ISBN 978-4-08-111183-1 |
| THE 22TH LOG "SHIRAHOSHI"    | 2017年7月28日  | ISBN 978-4-08-111184-8 |
| THE 23RD LOG "PUNK HAZARD"   | 2017年8月25日  | ISBN 978-4-08-111185-5 |
| THE 24TH LOG "SMILE"         | 2017年9月22日  | ISBN 978-4-08-111186-2 |
| THE 25TH LOG "DRESSROSA"     | 2017年10月27日 | ISBN 978-4-08-111187-9 |
| THE 26TH LOG "MEMORIES"      | 2017年11月24日 | ISBN 978-4-08-111188-6 |
| THE 27TH LOG "LAW"           | 2017年12月21日 | ISBN 978-4-08-111189-3 |
| THE 28TH LOG "LUCY"          | 2018年1月26日  | ISBN 978-4-08-111190-9 |

### 公式書

<table>
<thead>
<tr class="header">
<th><p>卷數</p></th>
<th><p>標題</p></th>
<th><p><a href="../Page/集英社.md" title="wikilink">集英社</a></p></th>
<th><p><a href="../Page/東立出版社.md" title="wikilink">東立出版社</a></p></th>
<th><p><a href="../Page/浙江人民美术出版社.md" title="wikilink">浙江人民美术出版社</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>發售日期</p></td>
<td><p>ISBN</p></td>
<td><p>發售日期</p></td>
<td><p>ISBN</p></td>
<td><p>發售日期</p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p>ONE PIECE RED GRAND CHARACTERS<br />
-{ONE PIECE RED-絕讚的人物特寫}-<br />
-{航海王：RED 伟大人物特写}-</p></td>
<td><p>2002年1月5日</p></td>
<td><p>ISBN 978-4-08-873211-4</p></td>
<td><p>2004年7月15日</p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p>ONE PIECE BLUE GRAND DATA FILE<br />
-{ONE PIECE BLUE-絕讚的內幕集錦}-<br />
-{航海王：BLUE 伟大内幕集锦}-</p></td>
<td><p>2002年8月2日</p></td>
<td><p>ISBN 978-4-08-873358-6</p></td>
<td><p>2004年11月29日</p></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td><p>ONE PIECE YELLOW GRAND ELEMENTS<br />
-{ONE PIECE YELLOW-絕讚的元素解析}-<br />
-{航海王：YELLOW 伟大元素解析}-</p></td>
<td><p>2007年4月4日</p></td>
<td><p>ISBN 978-4-08-874098-0</p></td>
<td><p>2008年6月20日</p></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td><p>ONE PIECE GREEN SECRET PIECES<br />
-{ONE PIECE GREEN-絕讚的秘密章節}-<br />
-{航海王：GREEN 秘密奇闻}-</p></td>
<td><p>2010年11月4日</p></td>
<td><p>ISBN 978-4-08-874848-1</p></td>
<td><p>2011年1月29日</p></td>
</tr>
<tr class="even">
<td><p>5</p></td>
<td><p>ONE PIECE BLUE DEEP CHARACTERS WORLD<br />
-{ONE PIECE BLUE DEEP-絕讚的角色世界}-<br />
-{航海王：BLUE DEEP 人物世界}-</p></td>
<td><p>2012年3月2日</p></td>
<td><p>ISBN 978-4-08-870445-6</p></td>
<td><p>2012年8月3日</p></td>
</tr>
</tbody>
</table>

  -

<table>
<thead>
<tr class="header">
<th><p>標題</p></th>
<th><p><a href="../Page/集英社.md" title="wikilink">集英社</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>發售日期</p></td>
<td><p>ISBN</p></td>
</tr>
<tr class="even">
<td><p>STARTER SET Vol.1</p></td>
<td><p>2018年9月4日</p></td>
</tr>
<tr class="odd">
<td><p>BOOSTER SET 「東の海」の猛者達</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>BOOSTER SET 集結! 「超新星」</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>BOOSTER SET アーロン一味とココヤシ村の人々</p></td>
<td><p>2018年11月2日</p></td>
</tr>
<tr class="even">
<td><p>BOOSTER SET インペルダウンの番人VS囚人達</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>BOOSTER SET 砂の王国・アラバスタの精鋭</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>BOOSTER SET “四皇”白ひげ海賊団</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>BOOSTER SET 結集! 秘密結社バロックワークス</p></td>
<td><p>2019年1月4日</p></td>
</tr>
<tr class="even">
<td><p>BOOSTER SET 海底の楽園・魚人島の住人達</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>BOOSTER SET ~空島の強敵達~</p></td>
<td><p>2019年2月4日</p></td>
</tr>
<tr class="even">
<td><p>BOOSTER SET ~魚人島の強敵達~</p></td>
<td><p>ISBN 978-4-08-908330-7</p></td>
</tr>
<tr class="odd">
<td><p>STARTER SET Vol.2</p></td>
<td><p>2019年3月4日</p></td>
</tr>
<tr class="even">
<td><p>BOOSTER SET シャンドラの戦士VS神の軍勢</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>BOOSTER SET パンクハザードの脅威</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>BOOSTER SET 世界一の船大工!ガレーラカンパニー</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>BOOSTER SET 恐怖の支配者! ドンキホーテファミリー</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>BOOSTER SET “闇の正義”の執行人!CP9</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>BOOSTER SET 激突!コロシアムの闘士達</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>BOOSTER SET 悪夢!スリラーバークの怪人達</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>BOOSTER SET 天性の戦士!モコモ公国のミンク族</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>BOOSTER SET “北の海”の戦争屋・ジェルマ66</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>BOOSTER SET “四皇”ビッグ・マム海賊団</p></td>
<td></td>
</tr>
</tbody>
</table>

### 畫集

  -
    原作：尾田榮一郎。目前出版共八卷，日本由集英社發行，台灣由[東立出版社發行](../Page/東立出版社.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>卷數</p></th>
<th><p>標題</p></th>
<th><p><a href="../Page/集英社.md" title="wikilink">集英社</a></p></th>
<th><p><a href="../Page/東立出版社.md" title="wikilink">東立出版社</a></p></th>
<th><p><a href="../Page/浙江人民美术出版社.md" title="wikilink">浙江人民美术出版社</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>發售日期</p></td>
<td><p>ISBN</p></td>
<td><p>發售日期</p></td>
<td><p>ISBN</p></td>
<td><p>發售日期</p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td></td>
<td><p>2001年7月19日</p></td>
<td><p>ISBN 978-4-08-859217-6</p></td>
<td><p>2004年4月30日</p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td></td>
<td><p>2003年11月4日</p></td>
<td><p>ISBN 978-4-08-859376-0</p></td>
<td><p>2004年8月12日</p></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td></td>
<td><p>2006年1月5日</p></td>
<td><p>ISBN 978-4-08-859538-2</p></td>
<td><p>2010年7月31日</p></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td></td>
<td><p>2010年3月4日</p></td>
<td><p>ISBN 978-4-08-782267-0</p></td>
<td><p>2011年7月18日</p></td>
</tr>
<tr class="even">
<td><p>5</p></td>
<td></td>
<td><p>2010年12月3日</p></td>
<td><p>ISBN 978-4-08-782356-1</p></td>
<td><p>2012年12月21日</p></td>
</tr>
<tr class="odd">
<td><p>6</p></td>
<td></td>
<td><p>2014年1月4日</p></td>
<td><p>ISBN 978-4-08-782747-7</p></td>
<td><p>2014年5月23日</p></td>
</tr>
<tr class="even">
<td><p>7</p></td>
<td></td>
<td><p>2016年7月4日</p></td>
<td><p>ISBN 978-4-08-792509-8</p></td>
<td><p>2016年11月4日</p></td>
</tr>
<tr class="odd">
<td><p>8</p></td>
<td></td>
<td><p>2018年3月2日</p></td>
<td><p>ISBN 978-4-08-792523-4</p></td>
<td><p>2018年8月10日</p></td>
</tr>
</tbody>
</table>

  -
    原作：尾田榮一郎。全一卷，日本由集英社發行，台灣由[東立出版社發行](../Page/東立出版社.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>卷數</p></th>
<th><p><a href="../Page/集英社.md" title="wikilink">集英社</a></p></th>
<th><p><a href="../Page/東立出版社.md" title="wikilink">東立出版社</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>發售日期</p></td>
<td><p>ISBN</p></td>
<td><p>發售日期</p></td>
</tr>
<tr class="even">
<td><p>全</p></td>
<td><p>2009年12月18日</p></td>
<td><p>ISBN 978-4-08-782251-9</p></td>
</tr>
</tbody>
</table>

### 小說

  - 小說化系列
    作者為[濱崎達也](../Page/濱崎達也.md "wikilink")，主要改編自尾田榮一郎《ONE
    PIECE》電影版和動畫原創的故事將其小說化，目前已出版16部。日本由集英社（JUMP
    j-BOOKS）發行；台灣目前只有代理5部，由[東立出版社發行](../Page/東立出版社.md "wikilink")；中國由[上海譯文出版社出版](../Page/上海譯文出版社.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>卷數</p></th>
<th><p>標題</p></th>
<th><p>JUMP j-BOOKS</p></th>
<th><p><a href="../Page/東立出版社.md" title="wikilink">東立出版社</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>發售日期</p></td>
<td><p>ISBN</p></td>
<td><p>發售日期</p></td>
<td><p>ISBN</p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p><br />
<small><a href="../Page/ONE_PIECE_打倒！海賊強薩克！.md" title="wikilink">ONE PIECE 航海王 打倒！海賊強薩克</a></small></p></td>
<td><p>1999年</p></td>
<td><p>ISBN 4-08-703084-9</p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p><br />
<small>航海王 羅格鎮篇</small></p></td>
<td><p>2000年</p></td>
<td><p>ISBN 4-08-703096-2</p></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td><p><br />
<small>ONE PIECE 發條島大冒險</small></p></td>
<td><p>2001年</p></td>
<td><p>ISBN 4-08-703102-0</p></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td><p><br />
<small>ONE PIECE 千年龍傳說</small></p></td>
<td><p>2001年</p></td>
<td><p>ISBN 4-08-703107-1</p></td>
</tr>
<tr class="even">
<td><p>5</p></td>
<td><p><br />
<small>ONE PIECE 珍獸島之喬巴王國</small></p></td>
<td><p>2002年</p></td>
<td><p>ISBN 4-08-703110-1</p></td>
</tr>
<tr class="odd">
<td><p>6</p></td>
<td><p><br />
<small>ONE PIECE THE MOVIE 死亡盡頭的冒險</small></p></td>
<td><p>2003年</p></td>
<td><p>ISBN 4-08-703124-1</p></td>
</tr>
<tr class="even">
<td><p>7</p></td>
<td><p><br />
<small>ONE PIECE 被詛咒的聖劍</small></p></td>
<td><p>2004年</p></td>
<td><p>ISBN 4-08-703137-3</p></td>
</tr>
<tr class="odd">
<td><p>8</p></td>
<td><p><br />
<small>ONE PIECE 祭典男爵與神秘島</small></p></td>
<td><p>2005年</p></td>
<td><p>ISBN 4-08-703153-5</p></td>
</tr>
<tr class="even">
<td><p>9</p></td>
<td><p><br />
<small>ONE PIECE 機關城的鋼鐵巨兵</small></p></td>
<td><p>2006年</p></td>
<td><p>ISBN 4-08-703168-3</p></td>
</tr>
<tr class="odd">
<td><p>10</p></td>
<td><p><br />
<small>ONE PIECE 沙漠王女與海賊們</small></p></td>
<td><p>2007年</p></td>
<td><p>ISBN 978-4-08-703178-2</p></td>
</tr>
<tr class="even">
<td><p>11</p></td>
<td><p><br />
<small>ONE PIECE 喬巴身世之謎：冬季綻放、奇跡的櫻花</small></p></td>
<td><p>2008年</p></td>
<td><p>ISBN 978-4-08-703190-4</p></td>
</tr>
<tr class="odd">
<td><p>12</p></td>
<td><p><br />
<small>ONE PIECE FILM STRONG WORLD 強者天下</small></p></td>
<td><p>2009年</p></td>
<td><p>ISBN 978-4-08-703219-2</p></td>
</tr>
<tr class="even">
<td><p>13</p></td>
<td><p><br />
<small>3D劇場版ONE PIECE 追逐草帽大冒險</small></p></td>
<td><p>2011年</p></td>
<td><p>ISBN 978-4-08-703241-3</p></td>
</tr>
<tr class="odd">
<td><p>14</p></td>
<td><p><br />
<small>ONE PIECE FILM Z 航海王電影Z小說</small></p></td>
<td><p>2012年</p></td>
<td><p>ISBN 978-4-08-703285-7</p></td>
</tr>
<tr class="even">
<td><p>15</p></td>
<td><p><br />
<small>航海王劇場版小說 ONE PIECE「3D2Y」跨越艾斯之死！魯夫與夥伴的誓言！</small></p></td>
<td><p>2014年</p></td>
<td><p>ISBN 978-4-08-703331-1</p></td>
</tr>
<tr class="odd">
<td><p>16</p></td>
<td><p><br />
<small>航海王劇場版小說 ONE PIECE FILM GOLD</small></p></td>
<td><p>2016年</p></td>
<td><p>ISBN 978-4-08-703398-4</p></td>
</tr>
</tbody>
</table>

  -
    原作：尾田榮一郎，小說：（第一卷）、浜崎達也（第二卷），插畫：[寺田克也](../Page/寺田克也.md "wikilink")。目前出版兩卷，最初於「ONE
    PIECE magazine」連載，日本由集英社發行書籍版本，台灣目前尚未代理。
    內容是以[波特卡斯·D·艾斯為主軸的外傳小說](../Page/波特卡斯·D·艾斯.md "wikilink")，描述艾斯在原作裡沒出現的劇情、他最初的夥伴以及如何獲得火焰果實的能力的故事。
    {| class=wikitable style="font-size:small;"

\!rowspan="2"|卷數 \!rowspan="2"|標題 \!colspan="2"| JUMP j-BOOKS |-
\!發售日期\!\!ISBN |- \! 1 |  | 2018年4月4日 | ISBN 978-4-08-703445-5
|- \! 2 |  | 2018年6月4日 | ISBN 978-4-08-703449-3 |}

  -
    原作：尾田榮一郎，小說：[大崎知仁](../Page/大崎知仁.md "wikilink")。目前全一卷，最初於「ONE PIECE
    magazine」連載，日本由集英社發行書籍版本，台灣為東立出版代理。
    內容是以草帽海賊團成員的故事的外傳小說，描述一般人的角度來看待草帽海賊團成員們的短篇故事（共9篇）。
    {| class=wikitable style="font-size:small;"

\!rowspan="2"|卷數 \!rowspan="2"|標題 \!colspan="2"| JUMP j-BOOKS
\!colspan="2"| [東立出版社](../Page/東立出版社.md "wikilink") |-
\!發售日期\!\!ISBN\!\!發售日期\!\!ISBN |- \! 全 |  | 2017年11月2日 | ISBN
978-4-08-703434-9 | 2018年9月3日 | ISBN 978-957-261-030-5 |}

### 繪本

  -
    原作：尾田榮一郎，繪本：長田真作。目前全一卷，最初於「ONE PIECE
    magazine」連載，日本由集英社發行書籍版本，台灣目前尚未代理。
    以繪本形式描述魯夫與艾斯與薩波這個永遠三兄弟的組合的故事。
    {| class=wikitable style="font-size:small;"

\!rowspan="2"|卷數 \!colspan="2"| [集英社](../Page/集英社.md "wikilink") |-
\!發售日期\!\!ISBN |- \! 全 | 2018年3月2日 | ISBN 978-4-08-780835-3 |}

### 雜誌特刊

  -
    原作：尾田榮一郎，編輯：V‧JUMP編輯部。全3冊，日本由集英社發行，台灣由東立出版社發行。
    本系列書籍為連載20周年超特別企劃書本，收錄了對魯夫和艾斯的評價，艾斯成名前的經歷小說和海兵觀點的頂點戰爭小說，已及艾斯薩波魯夫三兄弟的繪本，航海王粉絲的日常漫畫，介紹尾田仔的短篇漫畫，現實中的海盜介紹和故事中的惡魔果實介紹，專訪尾田榮一郎以及公開原稿設定圖等。
    {| class=wikitable style="font-size:small;"

\!rowspan="2"|卷數 \!colspan="2"| [集英社](../Page/集英社.md "wikilink") ―
SHUEISHA ― \!colspan="2"| [東立出版社](../Page/東立出版社.md "wikilink") |-
\!發售日期\!\!ISBN\!\!發售日期\!\!ISBN |- \! vol.1 |2017年7月7日 |ISBN
978-4-08-102232-8 |2018年2月12日 |ISBN 201-801-235-350-0（首刷附錄版） |- \! vol.2
|2017年8月4日 |ISBN 978-4-08-102233-5 |2018年6月15日 |ISBN 978-986-486-994-7
|- \! vol.3 |2017年9月1日 |ISBN 978-4-08-102234-2 |2018年8月14日 |ISBN
978-986-486-995-4 |}

### 電影漫畫

<table>
<thead>
<tr class="header">
<th><p>卷數</p></th>
<th><p>標題</p></th>
<th><p><a href="../Page/集英社.md" title="wikilink">集英社</a></p></th>
<th><p><a href="../Page/東立出版社.md" title="wikilink">東立出版社</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>發售日期</p></td>
<td><p>ISBN</p></td>
<td><p>發售日期</p></td>
<td><p>ISBN</p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p><br />
<small>航海王劇場版~死亡盡頭大冒險~（上·下）</small></p></td>
<td><p>2003年10月3日</p></td>
<td><p>ISBN 978-4-08-873547-4</p></td>
</tr>
<tr class="odd">
<td><p>ISBN 978-4-08-873548-1</p></td>
<td><p>2004年12月4日</p></td>
<td><p>ISBN 986-11-4148-0</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p><br />
<small>航海王劇場版 被咀咒的聖劍（上·下）</small></p></td>
<td><p>2004年7月2日</p></td>
<td><p>ISBN 978-4-08-873707-2</p></td>
</tr>
<tr class="odd">
<td><p>ISBN 978-4-08-873708-9</p></td>
<td><p>2008年9月5日</p></td>
<td><p>ISBN 978-986-11-7176-0</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td><p><br />
<small>航海王劇場版 機關城的機械巨兵（全）</small></p></td>
<td><p>2006年11月2日</p></td>
<td><p>ISBN 978-4-08-874172-7</p></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td><p><br />
<small>航海王劇場版 阿拉巴斯坦戰記 沙漠王女與海賊們（全）</small></p></td>
<td><p>2008年3月4日</p></td>
<td><p>ISBN 978-4-08-874236-6</p></td>
</tr>
<tr class="even">
<td><p>5</p></td>
<td><p><br />
<small>航海王劇場版 喬巴身世之謎+冬季綻放的奇蹟之櫻（全）</small></p></td>
<td><p>2009年11月27日</p></td>
<td><p>ISBN 978-4-08-874832-0</p></td>
</tr>
<tr class="odd">
<td><p>6</p></td>
<td><p><br />
<small>航海王電影版 強者天下（上·下）</small></p></td>
<td><p>2010年12月3日</p></td>
<td><p>ISBN 978-4-08-874846-7</p></td>
</tr>
<tr class="even">
<td><p>ISBN 978-4-08-874847-4</p></td>
<td><p>ISBN 978-986-470-258-9</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p><br />
<small>航海王劇場版~死亡盡頭大冒險~（全·新裝版）</small></p></td>
<td><p>2011年2月18日</p></td>
<td><p>ISBN 978-4-08-874850-4</p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p><br />
<small>航海王劇場版 被咀咒的聖劍（全·新裝版）</small></p></td>
<td><p>ISBN 978-4-08-874851-1</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p><br />
<small>ONE PIECE FILM Z 航海王電影：Z（上·下）</small></p></td>
<td><p>2013年7月4日</p></td>
<td><p>ISBN 978-4-08-870874-4</p></td>
</tr>
<tr class="even">
<td><p>ISBN 978-4-08-870875-1</p></td>
<td><p>2014年7月23日</p></td>
<td><p>ISBN 978-986-348-678-7</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>10</p></td>
<td><p><br />
<small>ONE PIECE FILM GOLD 航海王電影：GOLD（上·下）</small></p></td>
<td><p>2017年7月4日</p></td>
<td><p>ISBN 978-4-08-881253-3</p></td>
</tr>
<tr class="even">
<td><p>ISBN 978-4-08-881254-0</p></td>
<td><p>2018年10月19日</p></td>
<td><p>ISBN 978-957-26-0785-5</p></td>
<td></td>
</tr>
</tbody>
</table>

### 外傳漫畫

由為[安藤英著作](../Page/安藤英.md "wikilink")，目前已出版4卷，日本由集英社JUMP
j-BOOKS發行，台灣由東立出版社發行。內容是以航海王外傳漫畫的短篇搞笑故事集，在本書當中可以看到各種IF（平行世界）故事，包括了草帽一行人再戰CP9、薇薇再次與草帽一行人聯手戰鬥、草帽一行人的成立秘辛、以及夢幻般的魯夫艾斯薩波三兄弟齊聚一堂等故事，以歡樂方式呈現給讀者。

  -
    {| class=wikitable style="font-size:small;"

\!rowspan="2"|卷數 \!colspan="2"| [集英社](../Page/集英社.md "wikilink")（JUMP
COMICS） \!colspan="2"| [東立出版社](../Page/東立出版社.md "wikilink") |-
\!發售日期\!\!ISBN\!\!發售日期\!\!ISBN |- \! 1 | 2015年10月3日 | ISBN
978-4-08-880552-8 | 2016年4月22日 | ISBN 978-986-348-313-7 |- \! 2 |
2016年7月4日 | ISBN 978-4-08-880732-4 | 2016年11月3日 | ISBN
978-986-348-533-9 |- \! 3 | 2017年8月4日 | ISBN 978-4-08-881199-4 |
2017年12月22日 | ISBN 978-986-486-725-7 |- \! 4 | 2018年6月4日 | ISBN
978-4-08-881507-7 |2018年11月 日 |ISBN 978-957-261-414-3 |}

### 其他書籍

  -
    原作：尾田榮一郎。全2冊，日本由集英社發行，台灣目前尚未代理。
    {| class=wikitable style="font-size:small;"

\!rowspan="2"|卷數 \!rowspan="2"|標題 \!colspan="2"|
[集英社](../Page/集英社.md "wikilink") (JUMP COMICS) |-
\!發售日期\!\!ISBN |- \! 1 | ONE PIECE RAINBOW\! | 2007年5月1日 | ISBN
978-4-08-874099-7 |- \! 2 | ONE PIECE WHITE\! | 2012年8月3日 | ISBN
978-4-08-870576-7 |}

  -
    原作：尾田榮一郎，解說：[内田樹](../Page/内田樹.md "wikilink")。目前已出版3冊，日本由集英社發行，台灣由東立出版社發行。
    本系列書籍收錄這是一本以ONE
    PIECE故事裡的各個角色們曾經說過的各種經典台詞所組成的名言集。諸如魯夫出發航海第一天所說的第一句話，到艾斯死後，他懊悔萬分的一句話，分為各種主題的各主題包括「冒險」、「英雄」、「枷鎖」、「未來」、「超越」等經典名言都收錄在本系列書籍。
    {| class=wikitable style="font-size:small;"

\!rowspan="2"|卷數 \!colspan="2"| [集英社新書](../Page/集英社.md "wikilink")
\!colspan="2"| [東立出版社](../Page/東立出版社.md "wikilink") |-
\!發售日期\!\!ISBN\!\!發售日期\!\!ISBN |- \! 上 |2011年3月 |ISBN
978-4-08-720582-4 |2013年10月1日 |ISBN 978-986-332-588-8 |- \! 下 |2011年4月
|ISBN 978-4-08-720587-9 |2013年10月1日 |ISBN 978-986-332-589-5 |- \! PART2
|2014年3月 |ISBN 978-4-08-720728-6 |2015年1月7日 |ISBN 978-986-365-668-5 |}

  -
    原作：尾田榮一郎。全1冊，日本由集英社發行，台灣目前尚未代理。
    {| class=wikitable style="font-size:small;"

\!rowspan="2"|卷數 \!colspan="2"| [集英社](../Page/集英社.md "wikilink")
(FLOWER\&BEE BOOK) |- \!發售日期\!\!ISBN |- \! 全 | 2011年9月12日 | ISBN
978-4-08-102129-1 |}

  -
    作者：[SANJI](../Page/香吉士.md "wikilink")。全1冊，日本由集英社發行，台灣由東立出版社發行。
    本書是由大廚香吉士親自撰寫的食譜，裡面包含了帶骨肉、水水白菜、海王類筆管麵等四十道航海王漫畫中出現過的料理，重現了漫畫中美味的名場面！不用擔心沒有航海王世界中的食材可以料理，本書中的食譜已經用我們世界上的食材代替了，但是味道可是一樣的美味！
    {| class=wikitable style="font-size:small;"

\!rowspan="2"|卷數 \!colspan="2"| [集英社](../Page/集英社.md "wikilink")
\!colspan="2"| [東立出版社](../Page/東立出版社.md "wikilink") |-
\!發售日期\!\!ISBN\!\!發售日期\!\!ISBN |- \! 全 |2012年11月28日 |ISBN
978-4-08-780658-8 |2013年8月19日 |ISBN 978-986-332-775-2 |}

  -
    原作：尾田榮一郎，編輯：V‧JUMP編輯部。目前已出版2冊，日本由集英社發行，台灣由東立出版社發行。
    本系列書籍收錄了除了草帽一行九個人之外，還包括七武海、海軍、世界政府、革命軍、四皇、甚至於其他也同樣很有魅力的海賊們，以及故事內出現過的能力、道具、生物等等，許多只有「航海王」專家才知道答案的問題，而且解答旁邊還附有詳細的說明。
    {| class=wikitable style="font-size:small;"

\!rowspan="2"|卷數 \!colspan="2"| [集英社](../Page/集英社.md "wikilink") (JUMP
COMICS) \!colspan="2"| [東立出版社](../Page/東立出版社.md "wikilink") |-
\!發售日期\!\!ISBN\!\!發售日期\!\!ISBN |- \! 1 |2014年3月4日 |ISBN
978-4-08-880099-8 |2014年6月30日 |ISBN 978-986-348-980-1 |- \! 2 |2014年9月4日
|ISBN 978-4-08-880253-4 |2014年12月19日 |ISBN 978-986-365-889-4 |}

  -
    原作：尾田榮一郎。全2冊，日本由集英社發行，台灣目前尚未代理。
    {| class=wikitable style="font-size:small;"

\!rowspan="2"|卷數 \!colspan="2"| [集英社](../Page/集英社.md "wikilink") ―
SHUEISHA ― |- \!發售日期\!\!ISBN |- \! I |2014年12月1日 |ISBN 978-4-08-102188-8
|- \! II |2014年12月15日 |ISBN 978-4-08-102189-5 |}

  -
    原作：尾田榮一郎，編輯：V‧JUMP編輯部。全1冊，日本由集英社發行，台灣由東立出版社發行。
    {| class=wikitable style="font-size:small;"

\!rowspan="2"|卷數 \!colspan="2"| [集英社](../Page/集英社.md "wikilink") (JUMP
COMICS) \!colspan="2"| [東立出版社](../Page/東立出版社.md "wikilink") |-
\!發售日期\!\!ISBN\!\!發售日期\!\!ISBN |- \! 全 |2015年4月3日 |ISBN
978-4-08-908243-0 |2016年2月5日 |ISBN 978-986-431-976-3 |}

  -
    原作：尾田榮一郎，繪畫：藤澤実佳。全1冊平裝書，日本由集英社發行，台灣由東立出版社發行。
    收錄ONE PIECE故事裡面的草帽一行人和許多大人物們的Ｑ版小插圖繪畫步驟，教你如何用一支原子筆就畫出航海王Ｑ版小人物。
    {| class=wikitable style="font-size:small;"

\!rowspan="2"|卷數 \!colspan="2"| [集英社](../Page/集英社.md "wikilink")
\!colspan="2"| [東立出版社](../Page/東立出版社.md "wikilink") |-
\!發售日期\!\!ISBN\!\!發售日期\!\!ISBN |- \! 全 |2015年4月24日 |ISBN
978-4-08-780751-6 |2016年1月11日 |ISBN 978-986-431-977-0 |}

  -
    原作：尾田榮一郎。目前已出版2冊，日本由集英社發行，台灣目前尚未代理。
    {| class="wikitable" style="font-size:small;"

\! rowspan="2" |卷數 \! colspan="2" | [集英社](../Page/集英社.md "wikilink")
(JUMP COMICS) |- \!發售日期\!\!ISBN |- \! 1 | 2018年6月4日 |ISBN
978-4-08-881559-6 |- \! 2 | 2018年7月4日 |ISBN 978-4-08-881560-2 |}

## 衍生作品

### 電視動畫

### 電影

### 電子遊戲

<table>
<thead>
<tr class="header">
<th><p>發售年份</p></th>
<th><p>發售日期</p></th>
<th><p>遊戲名稱</p></th>
<th><p>遊戲類型</p></th>
<th><p>主機平台</p></th>
<th><p>開發廠商</p></th>
<th><p>遊戲人數</p></th>
<th><p>官方網站</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2000年</p></td>
<td><p>7月19日</p></td>
<td><p><br />
<small>航海王 〜目標海賊王！〜</small></p></td>
<td><p><a href="../Page/模擬遊戲.md" title="wikilink">SLG</a></p></td>
<td><p><a href="../Page/WonderSwan.md" title="wikilink">WS</a></p></td>
<td><p><a href="../Page/萬代.md" title="wikilink">萬代</a></p></td>
<td><p>1-2人</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2001年</p></td>
<td><p>3月15日</p></td>
<td><p><br />
<small>航海王 Grand Battle!</small></p></td>
<td><p><a href="../Page/動作遊戲.md" title="wikilink">ACT</a></p></td>
<td><p><a href="../Page/PlayStation.md" title="wikilink">PS</a></p></td>
<td><p>1-2人</p></td>
<td><p><a href="http://www.bandaigames.channel.or.jp/list/onepiece/index.html">1</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>4月27日</p></td>
<td><p><br />
<small>航海王 -海賊團誕生-</small></p></td>
<td><p><a href="../Page/角色扮演遊戲.md" title="wikilink">RPG</a></p></td>
<td><p><a href="../Page/Game_Boy.md" title="wikilink">GB</a></p></td>
<td><p><a href="../Page/萬普.md" title="wikilink">萬普</a></p></td>
<td><p>1人</p></td>
<td><p><a href="https://web.archive.org/web/20070825173834/http://www.banpresto.co.jp/japan/house/soft/s77/s77.htm">2</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>8月2日</p></td>
<td><p><br />
<small>航海王 飛天海賊團</small></p></td>
<td><p>PS</p></td>
<td><p>萬代</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>9月13日</p></td>
<td><p><br />
<small>航海王 虹之島傳說</small></p></td>
<td><p><a href="../Page/動作角色扮演遊戲.md" title="wikilink">A-RPG</a></p></td>
<td><p><a href="../Page/WonderSwan_Color.md" title="wikilink">WSC</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2002年</p></td>
<td><p>1月3日</p></td>
<td><p><br />
<small>航海王 Treasure Wars</small></p></td>
<td><p><a href="../Page/圖版遊戲.md" title="wikilink">BG</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3月20日</p></td>
<td><p><br />
<small>航海王 Grand Battle!2</small></p></td>
<td><p>ACT</p></td>
<td><p>PS</p></td>
<td><p>1-2人</p></td>
<td><p><a href="http://www.bandaigames.channel.or.jp/list/onepiece3/index.html">3</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>6月28日</p></td>
<td><p><br />
<small>航海王 幻之偉大航道冒險記</small></p></td>
<td><p>RPG</p></td>
<td><p>GB</p></td>
<td><p>萬普</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>7月12日</p></td>
<td><p><br />
<small>航海王 Grand Battle: Swan Colosseum</small></p></td>
<td><p>ACT</p></td>
<td><p>WSC</p></td>
<td><p>萬代</p></td>
<td><p>1-2人</p></td>
<td><p><a href="http://www.swan.channel.or.jp/swan/software/line_up/index_FOP04.html">4</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>11月1日</p></td>
<td><p><br />
<small>航海王 Treasure Battle!</small></p></td>
<td><p><a href="../Page/GameCube.md" title="wikilink">GC</a></p></td>
<td><p>1-4人</p></td>
<td><p><a href="http://www.bandaigames.channel.or.jp/list/onepiece_tb/index.html">5</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>11月15日</p></td>
<td></td>
<td><p>A-RPG</p></td>
<td><p><a href="../Page/Game_Boy_Advance.md" title="wikilink">GBA</a></p></td>
<td><p>萬普</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>12月20日</p></td>
<td><p><br />
<small>航海王 Treasure Wars2 歡迎來到巴基樂園</small></p></td>
<td><p>BG</p></td>
<td><p>WSC</p></td>
<td><p>萬代</p></td>
<td><p>1-4人</p></td>
<td><p><a href="http://www.swan.channel.or.jp/swan/software/line_up/index_FOP05.html">6</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2003年</p></td>
<td><p>3月28日</p></td>
<td><p><br />
<small>航海王 目標! 賞金王</small></p></td>
<td><p><a href="../Page/聚會遊戲.md" title="wikilink">PG</a></p></td>
<td><p>GBA</p></td>
<td><p>萬普</p></td>
<td><p>1-4人</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>5月1日</p></td>
<td><p><br />
<small>航海王 Ocean Dream!</small></p></td>
<td><p>RPG</p></td>
<td><p>PS</p></td>
<td><p>萬代</p></td>
<td><p>1人</p></td>
<td><p><a href="http://www.bandaigames.channel.or.jp/list/onepiece_ocean/top.html">7</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>10月16日</p></td>
<td><p><br />
<small>航海王 喬巴的大冒險</small></p></td>
<td><p>A-RPG</p></td>
<td><p>WSC</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>12月11日</p></td>
<td><p><br />
<small>航海王 Grand Battle!3</small></p></td>
<td><p>ACT</p></td>
<td><p><a href="../Page/PlayStation_2.md" title="wikilink">PS2</a>、GC</p></td>
<td><p>1-2人</p></td>
<td><p><a href="http://www.bandaigames.channel.or.jp/list/one_gra3/">8</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2004年</p></td>
<td><p>3月11日</p></td>
<td><p><br />
<small>航海王 Going Baseball</small></p></td>
<td><p><a href="../Page/體育遊戲.md" title="wikilink">SPG</a></p></td>
<td><p>GBA</p></td>
<td><p>1-2人</p></td>
<td><p><a href="http://www.bandaigames.channel.or.jp/list/one_going/">9</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>7月22日</p></td>
<td><p><br />
<small>航海王 Land Land!</small></p></td>
<td><p><a href="../Page/動作遊戲.md" title="wikilink">ACT</a></p></td>
<td><p>PS2</p></td>
<td><p>1人</p></td>
<td><p><a href="http://www.bandaigames.channel.or.jp/list/one_land/">10</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2005年</p></td>
<td><p>3月17日</p></td>
<td><p><br />
<small>航海王 Grand!RUSH</small></p></td>
<td><p>ACT</p></td>
<td><p>PS2、GC</p></td>
<td><p>1-2人</p></td>
<td><p><a href="http://www.bandaigames.channel.or.jp/list/ps2_one_rush/">11</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>4月28日</p></td>
<td><p><br />
<small>航海王 ONE PIECE Dragon Dream!</small></p></td>
<td><p>A-RPG</p></td>
<td><p>GBA</p></td>
<td><p>1人</p></td>
<td><p><a href="http://www.bandaigames.channel.or.jp/list/gba_one_dra/">12</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>9月8日</p></td>
<td><p><br />
<small>Fighting For 航海王</small></p></td>
<td><p>ACT</p></td>
<td><p>PS2</p></td>
<td><p>1-2人</p></td>
<td><p><a href="http://www.bandaigames.channel.or.jp/list/ps2_one_fight/">13</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>11月10日</p></td>
<td><p><br />
<small>航海王 Pirates Carnival</small></p></td>
<td><p>PG</p></td>
<td><p>PS2、GC</p></td>
<td><p>1-4人</p></td>
<td><p><a href="http://www.bandaigames.channel.or.jp/list/one_carnival/">14</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2006年</p></td>
<td><p>10月16日</p></td>
<td><p><br />
<small>航海王 Grand Adventure</small></p></td>
<td><p>RPG</p></td>
<td><p><a href="../Page/i-αppli.md" title="wikilink">i-αppli</a><br />
<small>[FOMA90x系對應]</small></p></td>
<td><p>萬代</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>7月20日</p></td>
<td><p><br />
<small>譯：戰鬥競技場 D.O.N</small></p></td>
<td><p>ACT</p></td>
<td><p>PS2、GC</p></td>
<td><p>1-4人</p></td>
<td><p><a href="http://battlestadium.com/index.html">15</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2007年</p></td>
<td><p>4月26日</p></td>
<td><p><br />
<small>航海王 無限冒險</small></p></td>
<td><p><a href="../Page/動作冒險遊戲.md" title="wikilink">A-AVG</a></p></td>
<td><p><a href="../Page/Wii.md" title="wikilink">Wii</a></p></td>
<td><p><a href="../Page/萬代南夢宮娛樂.md" title="wikilink">BNEI</a></p></td>
<td><p>1-2人</p></td>
<td><p><a href="http://www.bandaigames.channel.or.jp/list/wii_onepiece/">16</a></p></td>
</tr>
<tr class="even">
<td><p>8月30日</p></td>
<td><p><br />
<small>航海王 靈魂檔位</small></p></td>
<td><p>ACT</p></td>
<td><p><a href="../Page/任天堂DS.md" title="wikilink">NDS</a></p></td>
<td><p>1-4人</p></td>
<td><p><a href="http://www.bandaigames.channel.or.jp/list/ds_onepiece/index.html">17</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2008年</p></td>
<td><p>2月26日</p></td>
<td><p><br />
<small>航海王 無限巡航 第1章 波浪中的秘寶</small></p></td>
<td><p>A-AVG</p></td>
<td><p>Wii</p></td>
<td><p>1-2人</p></td>
<td><p><a href="http://www.bandaigames.channel.or.jp/list/wii_onepiece2/">18</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2009年</p></td>
<td><p>2月26日</p></td>
<td><p><br />
<small>航海王 無限巡航 第2章 覺醒的勇者</small></p></td>
<td><p>Wii</p></td>
<td><p>1-2人</p></td>
<td><p><a href="http://www.bandaigames.channel.or.jp/list/wii_onepiece3/">19</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2010年</p></td>
<td><p>9月9日</p></td>
<td><p><br />
<small>航海王 大決戰！</small></p></td>
<td><p>ACT</p></td>
<td><p>NDS</p></td>
<td><p>1-4人</p></td>
<td><p><a href="http://www.bandaigames.channel.or.jp/list/opj/1/">20</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2011年</p></td>
<td><p>5月26日</p></td>
<td><p><br />
<small>譯：<a href="../Page/航海王_無限巡航_SP.md" title="wikilink">航海王 無限巡航 SP</a></small></p></td>
<td><p>A-AVG</p></td>
<td><p>3DS</p></td>
<td><p>1人</p></td>
<td><p><a href="http://www.bandaigames.channel.or.jp/list/3ds_onepiece/">21</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>11月17日</p></td>
<td><p><br />
<small>航海王 大決戰！2 新世界</small></p></td>
<td><p>ACT</p></td>
<td><p>NDS</p></td>
<td><p>1-4人</p></td>
<td><p><a href="http://www.bandaigames.channel.or.jp/list/opj/">22</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2012年</p></td>
<td><p>3月1日<br />
8月8日</p></td>
<td><p><br />
<small><a href="../Page/ONE_PIECE：海賊無雙.md" title="wikilink">航海王 -{海賊}-無雙</a></small></p></td>
<td><p>ACT</p></td>
<td><p><a href="../Page/PS3.md" title="wikilink">PS3</a></p></td>
<td><p>1-2人</p></td>
<td><p><a href="http://www.bandaigames.channel.or.jp/list/opm/1/pc/">23</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>12月20日</p></td>
<td><p><br />
<small>航海王 冒險的黎明</small></p></td>
<td><p>A-RPG</p></td>
<td><p><a href="../Page/PSP.md" title="wikilink">PSP</a>、<a href="../Page/3DS.md" title="wikilink">3DS</a></p></td>
<td><p>1人</p></td>
<td><p><a href="http://www.bandaigames.channel.or.jp/list/psp_op/">24</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2013年</p></td>
<td><p>3月20日</p></td>
<td><p><br />
<small><a href="../Page/ONE_PIECE_海賊無雙2.md" title="wikilink">航海王 -{海賊}-無雙2</a></small></p></td>
<td><p>ACT</p></td>
<td><p>PS3、<a href="../Page/PlayStation_Vita.md" title="wikilink">PSV</a></p></td>
<td><p>1-2人</p></td>
<td><p><a href="http://www.bandaigames.channel.or.jp/list/opm/pc/">25</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>11月21日</p></td>
<td><p><br />
<small><a href="../Page/航海王_無限世界：赤紅.md" title="wikilink">航海王 無限世界：赤紅</a></small></p></td>
<td><p>A-AVG</p></td>
<td><p>3DS、PS3<br />
PS Vita、Wii U</p></td>
<td><p>2人</p></td>
<td><p><a href="http://www.bandaigames.channel.or.jp/list/opuw/">26</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2014年</p></td>
<td><p>11月13日</p></td>
<td><p><br />
<small>航海王 超級偉大航路之爭 X</small></p></td>
<td><p>ACT</p></td>
<td><p>3DS</p></td>
<td><p>4人</p></td>
<td><p>[<a href="http://www.bandaigames.channel.or.jp/list/opj/">http://www.bandaigames.channel.or.jp/list/opj/</a>.]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>11月14日</p></td>
<td><p><br />
<small>航海王 Dance Battle</small></p></td>
<td><p><a href="../Page/音樂遊戲.md" title="wikilink">MUG</a></p></td>
<td><p><a href="../Page/IOS.md" title="wikilink">IOS</a><br />
<a href="../Page/Andriod.md" title="wikilink">Andriod</a></p></td>
<td><p>1人</p></td>
<td><p><a href="http://www.bandaigames.channel.or.jp/list/one_main/dance/?utm_source=youtube&amp;utm_medium=annotation_otvi&amp;utm_campaign=annotation_otvi">27</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2015年</p></td>
<td><p>3月26日<br />
12月21日(NS)</p></td>
<td><p><br />
<small><a href="../Page/航海王_海賊無雙3.md" title="wikilink">航海王 海賊無雙3</a></small></p></td>
<td><p>ACT</p></td>
<td><p>PC、PSV<br />
PS3、PS4<br />
NS(2017年)</p></td>
<td><p>1-2人</p></td>
<td><p><a href="http://www.bandaigames.channel.or.jp/list/opm/">28</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2016年</p></td>
<td><p>9月2日</p></td>
<td><p><br />
<small>航海王 Burning Blood</small></p></td>
<td><p>ACT</p></td>
<td><p>PC、PS4<br />
PSV</p></td>
<td><p><a href="../Page/萬代南夢宮遊戲.md" title="wikilink">（BNEI）</a></p></td>
<td><p>2人</p></td>
<td><p>[<a href="http://opbb.bn-ent.net/pc/">http://opbb.bn-ent.net/pc/</a>?]<br />
<a href="https://store.steampowered.com/app/425220/One_Piece_Burning_Blood/">29</a></p></td>
</tr>
<tr class="even">
<td><p>9月21日</p></td>
<td><p><br />
<small>航海王 大海賊鬥技場</small></p></td>
<td><p>3DS</p></td>
<td></td>
<td><p><a href="http://opc.bn-ent.net/top.html">30</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2017年</p></td>
<td><p>8月24日</p></td>
<td><p><br />
<small>航海王 無限世界：赤紅 豪華版</small></p></td>
<td><p>A-AVG</p></td>
<td><p>PC、PS4<br />
Switch</p></td>
<td><p>2人</p></td>
<td><p><a href="http://opuw.bn-ent.net">31</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>9月21日</p></td>
<td><p><br />
<small>航海王 海賊無雙3 豪華版</small></p></td>
<td><p>ACT</p></td>
<td><p>Switch</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2018年</p></td>
<td><p>5月24日</p></td>
<td><p><br />
<small>航海王 偉大巡航</small></p></td>
<td><p>ACT</p></td>
<td><p>PS4</p></td>
<td><p>1人</p></td>
<td><p><a href="http://onepiecevr.bn-ent.net/">32</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>年內預定</p></td>
<td><p><br />
<small>航海王 尋秘世界</small></p></td>
<td></td>
<td><p><a href="https://opws.bn-ent.net/">33</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 舞台劇

  -
    是於TOKYO ONE PIECE
    TOWER（[東京鐵塔航海王主題樂園](../Page/東京鐵塔航海王主題樂園.md "wikilink")）期間限定舉辦的真人舞台秀，目前總共上演3部。

<!-- end list -->

  -
    公演期間：2015年3月13日～2016年4月10日

  - ＜CAST＞

<!-- end list -->

  - 魯夫：三浦宏規／永田崇人／竹中凌平 （CV：[田中真弓](../Page/田中真弓.md "wikilink")）
  - 索隆：伊万里有／橋本全一 （CV：[中井和哉](../Page/中井和哉.md "wikilink")）
  - 娜美：樋浦結花／樋浦舞花 （CV：[岡村明美](../Page/岡村明美.md "wikilink")）
  - 騙人布：菅野勇城／及川洸 （CV：[山口勝平](../Page/山口勝平.md "wikilink")）
  - 香吉士：越智友己／村上健斗／井澤巧麻 （CV：[平田廣明](../Page/平田廣明.md "wikilink")）
  - 喬巴：國友久志／山本恭之輔 （CV：[大谷育江](../Page/大谷育江.md "wikilink")）
  - 羅賓：海来／田中セシル／谷口莉緒 （CV：[山口由里子](../Page/山口由里子.md "wikilink")）
  - 佛朗基：大見魁冴／永門けん （CV：[矢尾一樹](../Page/矢尾一樹.md "wikilink")）
  - 布魯克：森大成／晴本宏亮 （CV：[長](../Page/長_\(日本配音員\).md "wikilink")）

<!-- end list -->

  - ＜製作團隊＞

<!-- end list -->

  - 演出／共同劇本：ウォーリー木下
  - 劇本：堀裕介
  - 音樂：和田俊輔
  - 編舞：振付稼業air:man
  - 武術指導：富田昌則
  - 美術：BOKETA
  - 燈光：菅橋友紀／藤巻聴
  - 音響：大木裕介
  - 影像：上田大樹／大鹿奈穂
  - 技術指導：関口祐二
  - 服裝：牧野iwao純子
  - 髮型設計：馮啓考
  - 演出助手：相田剛志
  - 舞台監督：堀吉行
  - 雷射燈光：カスト
  - 程序監控：マイルランテック
  - 舞臺布景：C-COM舞台装置
  - 道具：アトリエカオス
  - LED燈光：Namoo＆Partners Inc.
  - 人偶：関聡太郎／スタジオノ－ヴァ
  - 角色分配：野上祥子
  - 製作：NERUKE規劃／KYUBU

<!-- end list -->

  -
    公演期間：2016年4月23日～2017年4月9日

  - ＜CAST＞

<!-- end list -->

  - 魯夫：關修人／前田隆太朗（CV：田中真弓）
  - 索隆：三宅潤／四條真悟（CV：中井和哉）
  - 娜美：樋浦舞花／樋浦結花／佐達ももこ（CV：岡村明美）
  - 騙人布：及川洸／菅野勇城（CV：山口勝平）
  - 香吉士：村上健斗／茅根和哉（CV：平田広明）
  - 喬巴：無，玩偶演出（CV：大谷育江）
  - 羅賓：谷口莉緒／畑井咲耶（CV：山口由里子）
  - 佛朗基：（CV：矢尾一樹）
  - 布魯克：（CV：長）
  - 羅：磯野大／西村信彰／宮元英光（CV：[神谷浩史](../Page/神谷浩史.md "wikilink")）
  - 黃猿：大見魁冴／晴元宏亮（CV：[石塚運昇](../Page/石塚運昇.md "wikilink")）
  - Rigatto：國友久志／片山隼
  - 旁白：岸祐二

<!-- end list -->

  - ＜製作團隊＞

<!-- end list -->

  - 演出：UOORII木下
  - 劇本：ほりゆーすけ
  - 音樂：和田俊輔
  - 編舞：左HIDALI
  - 武術指導：富田昌則
  - 美術：BOKETA
  - 燈光：菅橋友紀（balance,inc.LIGHTING）
  - 音響：中島聡
  - 音效：大鹿奈穂
  - 技術指導：関口裕二（balance,inc.LIGHTING）
  - 服裝：牧野iwao純子／小林菜摘
  - 髮型設計：馮啓孝／松下よし子
  - マジック監修：HIROKI HARA
  - 演出助手：相田剛志
  - 舞台監督：堀吉行
  - 雷射燈光：カスト
  - 程序監控：マイルランテック
  - 舞臺布景：C-COM舞台装置
  - 道具：アトリエ・カオス
  - LED燈光：JWB
  - 人偶：スタジオ・ノーヴァ
  - 宣傳美術：岡垣吏紗（Gene & Fred）
  - 宣傳写真：中村理生（Un.inc）
  - 角色分配：野上祥子
  - 監製：集英社／東映動畫
  - 贊助：KYUBU
  - 製作：NERUKE規劃

<!-- end list -->

  -
    首次公演：2017年4月29日

  - ＜簡介＞
    幻影〟迷惑了〝草帽一行人〟！！？？ 由尾田栄一郎全權監製！！大人氣的現場表演第3彈，「ONE PIECE LIVE
    ATTRACTION〝3〟
    『PHANTOM』（幻影）」！！〝草帽一行人〟在你的面前展示出一個不容錯過的表演。是一個以投影映射和照明技術作輔，巨大壓迫感的演出！

    本作是原作者，尾田栄一郎全權監製，完全原創的故事。而為了本作創作出的「歌姬 安」
    也會出場，是一個完全原創的故事！另外，本作的開場曲和片尾曲，也是交由大人氣歌唱組合「GReeeeN」所創作！令人目不轉睛，是遠遠凌架於前作的感動作品！！

  - ＜CAST＞

<!-- end list -->

  - 魯夫：關修人／芹沢尚哉／志茂星哉／霜田哲朗（CV：田中真弓）
  - 索隆：磯野大／中島礼貴／松田裕（CV：中井和哉）
  - 娜美：樋浦舞花／樋浦結花／尾﨑優子／橘里依／齋藤千尋（CV：岡村明美）
  - 騙人布：菅野勇城／一之瀬嘉仁／金田明秀／三小田芳樹（CV：山口勝平）
  - 香吉士：岩崎良祐／Taiga／太田裕二／林勇輝（CV：平田廣明）
  - 喬巴：無，玩偶演出（CV：大谷育江）
  - 羅賓：谷口莉緒（CV：山口由里子）※影像演出
  - 佛朗基：大見魁冴（CV：矢尾一樹）※影像演出
  - 布魯克：晴本宏亮（CV：長）※影像演出
  - 「歌姬」安／アン：小島瑠奈／橘里依／橋本樹（CV：[早見沙織](../Page/早見沙織.md "wikilink")）
  - DJ 鸚鵡／オウムDJ：無（CV：[MC RYU](../Page/MC_RYU.md "wikilink")）
  - 巴其：金田明秀／勇士武範／勇士武範／稲井大地（CV：[千葉繁](../Page/千葉繁.md "wikilink")）
  - 艾斯：及川洸／田中稔／伊藤澄也／増田智彦（CV：[古川登志夫](../Page/古川登志夫.md "wikilink")）
  - 馮·克雷：麻倉尚太（CV：矢尾一樹）※影像演出
  - 漢考克：畑井咲耶（CV：[三石琴乃](../Page/三石琴乃.md "wikilink")）※影像演出
  - 羅：西村信彰（CV：神谷浩史）※影像演出
  - 密佛格：橋本全一（CV：[掛川裕彥](../Page/掛川裕彥.md "wikilink")）※影像演出
  - 培羅娜：平石亜弓（CV：[西原久美子](../Page/西原久美子.md "wikilink")）※影像演出
  - 斯摩格：田中しげ美（CV：[大場真人](../Page/大場真人.md "wikilink")）※影像演出
  - 達斯琪：川村海乃（CV：[野田順子](../Page/野田順子.md "wikilink")）※影像演出
  - 卡文迪許：松本誠也（CV：[石田彰](../Page/石田彰.md "wikilink")）※影像演出
  - 伊娃柯夫：藤田晋之介（CV：[岩田光央](../Page/岩田光央.md "wikilink")）※影像演出
  - 薩波：宮元英光／村上健斗（CV：[古谷徹](../Page/古谷徹.md "wikilink")）※影像演出

<!-- end list -->

  - ＜製作團隊＞

<!-- end list -->

  - 原作／監修：尾田榮一郎 （集英社「週刊少年JUMP」連載中）
  - 開場曲／結尾曲：[GReeeeN](../Page/GReeeeN.md "wikilink")
  - 演出：UOORII木下
  - 劇本：ほりゆーすけ／入江おろぱ
  - 音樂：和田俊輔
  - 編舞：左HIDALI
  - 武術指導：富田昌則
  - 美術：BOKETA
  - 燈光：菅橋友紀（balance,inc.LIGHTING）
  - 音效：大木裕介
  - 影像：大鹿奈穗
  - 技術指導：関口裕二（balance,inc.DESIGN）
  - 服裝：牧野iwao純子／小林菜摘
  - 髮型設計：馮啓孝／松下よし子
  - 演出助手：相田剛志／岸京子
  - 演出助理：伊藤マサミ
  - 舞臺監督：堀吉行
  - 雷射燈光：カスト
  - 程序監控：マイルランテック
  - 舞臺佈景：C-COM舞台装置
  - 道具：アトリエ・カオス
  - LED燈光：JWB
  - 人偶：スタジオ・ノーヴァ
  - 監製：集英社／東映動畫
  - 贊助：KYUBU
  - 製作：NERUKE規劃

<!-- end list -->

  -
    於每年大阪·[日本環球影城](../Page/日本環球影城.md "wikilink")（USJ）夏季期間限定舉辦。極具壓倒性臨場感的逼真現場表演。原汁原味地將『海賊王』世界演出，場面逼真及震撼力十足的原創表演。可盡情投入逼真的『海賊王』世界，令你大爲興奮達至忘我境界。

<!-- end list -->

  -
    於2016年10月7日起至11月25日在東京的新橋演舞場公演。是由集英社與松竹合作的特別企劃，將以原作中的「頂上戰爭」作為主軸，描繪和海軍們對戰而四散的草帽一行人，魯夫聽聞艾斯被宣判處刑後前往搭救，為了拯救艾斯的魯夫和海賊們、還有準備行刑的海軍們於是展開了大戰。

<!-- end list -->

  -
    **演員表**

:\*：魯夫、漢考克、紅髮傑克

:\*市川右近：白鬍子

:\*坂東巳之助：索隆、馮·克雷、史庫亞德

:\*中村隼人：香吉士、雷電

:\*市川春猿：娜美、桑塔索妮雅

:\*市川弘太郎：小八、戰桃丸

:\*市川寿猿：亞帕羅·比薩羅

:\*市川笑三郎：紐婆婆

:\*市川猿弥：吉貝爾、黑鬍子（汀奇）

:\*市川笑也：羅賓、瑪莉哥德

:\*市川男女蔵：麥哲倫

:\*市川門之助：鶴

:\*[福士誠治](../Page/福士誠治.md "wikilink")：艾斯

:\*嘉島典俊：布魯克、赤犬（盃）

:\*浅野和之：雷利、伊萬科夫、戰國

### 真人版美劇

隨著《ONE
PIECE》漫畫連載20週年的來臨，於2017年7月21日宣布製作成美國電視劇\[4\]，由知名美劇《[越獄風雲](../Page/越獄風雲.md "wikilink")》導演馬蒂·阿德爾斯坦（[Marty
Adelstein](../Page/Marty_Adelstein.md "wikilink")）所主導，製作公司為[好萊塢Tomorrow](../Page/好萊塢.md "wikilink")
studios。

原作者[尾田榮一郎於記者會以親筆信透露](../Page/尾田榮一郎.md "wikilink")，3年前決定將《ONE
PIECE》翻拍成真人版美劇，並保證不會辜負粉絲們的期待，並指出過去20年來一直都打算將《ONE
PIECE》真人化，而當中唯一條件是真人版內容絕對不能背叛《ONE
PIECE》的廣大支持者。《週刊少年JUMP》的中野編集長表示，決定交由[好萊塢的製作公司統籌製作](../Page/好萊塢.md "wikilink")，以保障真人版《ONE
PIECE》的質感。不過這次卻並非真人版電影形式，反而會是更長篇的真人版美劇形式。

此外，製作人馬蒂·阿德爾斯坦（[Marty
Adelstein](../Page/Marty_Adelstein.md "wikilink")）在記者會上表示自己 20
年來一直都是《ONE
PIECE》粉絲，而今次集英社及作者尾田榮一郎將真人化的重任交到其手上，他對此感到相當榮幸，並認為這將會是史上最高製作成本的電視連續劇。亦表示自己會傾盡全力，將原著的氣氛帶到電視螢幕上。

## 活動與企劃

### 紀念館

  -

<table>
<thead>
<tr class="header">
<th><p>舉辦地</p></th>
<th><p>地點</p></th>
<th><p>期間</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>拉古納蒲郡</p></td>
<td><p><a href="../Page/愛知縣.md" title="wikilink">愛知縣</a><a href="../Page/蒲郡市.md" title="wikilink">蒲郡市</a></p></td>
<td><p>2010年3月20日 - 6月27日<br />
2011年3月19日 - 9月4日<br />
2012年9月15日 - 2013年3月3日<br />
2014年3月15日 - 2015年3月1日</p></td>
</tr>
<tr class="even">
<td><p>台場衆国</p></td>
<td><p><a href="../Page/東京都.md" title="wikilink">東京都</a><a href="../Page/港区_(東京都).md" title="wikilink">港区</a></p></td>
<td><p>2010年7月17日 - 8月31日</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/豪斯登堡.md" title="wikilink">豪斯登堡</a></p></td>
<td><p><a href="../Page/長崎縣.md" title="wikilink">長崎縣</a><a href="../Page/佐世保市.md" title="wikilink">佐世保市</a></p></td>
<td><p>2010年7月15日 - 10月15日</p></td>
</tr>
<tr class="even">
<td><p>安比高原</p></td>
<td><p><a href="../Page/岩手縣.md" title="wikilink">岩手縣</a><a href="../Page/八幡平市.md" title="wikilink">八幡平市</a></p></td>
<td><p>2010年9月15日 - 26日</p></td>
</tr>
<tr class="odd">
<td><p>TIC</p></td>
<td><p><a href="../Page/宮城縣.md" title="wikilink">宮城縣</a><a href="../Page/仙台市.md" title="wikilink">仙台市</a></p></td>
<td><p>2010年12月23日 - 2011年1月10日</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/九族文化村.md" title="wikilink">九族文化村</a></p></td>
<td><p>台灣·<a href="../Page/南投縣.md" title="wikilink">南投縣</a><a href="../Page/魚池鄉.md" title="wikilink">魚池鄉</a></p></td>
<td><p>2011年7月 - 2012年4月<br />
2012年8月 - 2013年6月</p></td>
</tr>
<tr class="odd">
<td><p>秋田基地中心艾雲</p></td>
<td><p><a href="../Page/秋田縣.md" title="wikilink">秋田縣</a><a href="../Page/秋田市.md" title="wikilink">秋田市</a></p></td>
<td><p>2011年8月10日 - 18日</p></td>
</tr>
<tr class="even">
<td><p>橫濱地標大廈</p></td>
<td><p><a href="../Page/神奈川縣.md" title="wikilink">神奈川縣</a><a href="../Page/横浜市.md" title="wikilink">横浜市</a></p></td>
<td><p>2011年11月10日 - 12月25日</p></td>
</tr>
<tr class="odd">
<td><p>NTT克雷大廳</p></td>
<td><p><a href="../Page/廣島縣.md" title="wikilink">廣島縣</a><a href="../Page/廣島市.md" title="wikilink">廣島市</a></p></td>
<td><p>2011年12月23日 - 2012年1月9日</p></td>
</tr>
<tr class="even">
<td><p>枚方園區</p></td>
<td><p><a href="../Page/大阪府.md" title="wikilink">大阪府</a><a href="../Page/枚方市.md" title="wikilink">枚方市</a></p></td>
<td><p>2012年3月7日 - 7月8日</p></td>
</tr>
<tr class="odd">
<td><p>留壽都度假村</p></td>
<td><p><a href="../Page/北海道.md" title="wikilink">北海道</a><a href="../Page/虻田郡.md" title="wikilink">虻田郡</a></p></td>
<td><p>2012年7月7日 - 9月9日</p></td>
</tr>
<tr class="even">
<td><p>夏威夷溫泉度假村</p></td>
<td><p><a href="../Page/福島縣.md" title="wikilink">福島縣</a><a href="../Page/磐城市.md" title="wikilink">磐城市</a></p></td>
<td><p>2013年4月27日 - 5月6日</p></td>
</tr>
<tr class="odd">
<td><p>新Reoma世界</p></td>
<td><p><a href="../Page/香川縣.md" title="wikilink">香川縣</a><a href="../Page/丸龜市.md" title="wikilink">丸龜市</a></p></td>
<td><p>2013年6月29日 - 8月27日<br />
2014年7月5日 - 9月15日</p></td>
</tr>
<tr class="even">
<td><p>那須高地公園</p></td>
<td><p><a href="../Page/栃木縣.md" title="wikilink">栃木縣</a><a href="../Page/那須郡.md" title="wikilink">那須郡</a></p></td>
<td><p>2014年7月12日 - 9月24日</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/韓國戰爭紀念館.md" title="wikilink">韓國戰爭紀念館</a></p></td>
<td><p><a href="../Page/大韓民國.md" title="wikilink">韓國</a>·<a href="../Page/首爾市.md" title="wikilink">首爾市</a><a href="../Page/龍山區_(首爾).md" title="wikilink">龍山區</a></p></td>
<td><p>2014年7月26日 - 9月22日</p></td>
</tr>
<tr class="even">
<td><p>會議中心「BEXCO」</p></td>
<td><p>韓國·<a href="../Page/釜山廣域市.md" title="wikilink">釜山市</a><a href="../Page/海雲台區.md" title="wikilink">海雲台區</a></p></td>
<td><p>2014年12月20日 - 2015年3月14日</p></td>
</tr>
<tr class="odd">
<td><p>弘益大學路藝術中心</p></td>
<td><p>韓國·首爾市<a href="../Page/鐘路區.md" title="wikilink">鐘路區</a></p></td>
<td><p>2015年4月4日 - 2015年8月30日</p></td>
</tr>
<tr class="even">
<td><p>芝政世界</p></td>
<td><p><a href="../Page/福井縣.md" title="wikilink">福井縣</a><a href="../Page/坂井市.md" title="wikilink">坂井市</a></p></td>
<td><p>2015年7月4日 - 9月23日</p></td>
</tr>
</tbody>
</table>

### 展覽會

  -
    記念《ONE PIECE》連載2012年已達成15周年、由原作者尾田栄一郎監修的展覽會。以《原畫X映像X體感ONE PIECE》為主題。

| 舉辦地                                       | 地點                                                                     | 期間                       |
| ----------------------------------------- | ---------------------------------------------------------------------- | ------------------------ |
| [六本木新城](../Page/六本木新城.md "wikilink") 森美術館 | [東京都](../Page/東京都.md "wikilink")[港區](../Page/港區_\(東京都\).md "wikilink") | 2012年3月20日 - 6月17日       |
| [天保山特設畫廊](../Page/三得利美術館.md "wikilink")   | [大阪府](../Page/大阪府.md "wikilink")[大阪市](../Page/大阪市.md "wikilink")       | 2012年11月24日 - 2013年2月17日 |
| 華山1914文創園區                                | [台灣](../Page/台灣.md "wikilink") [台北市](../Page/台北市.md "wikilink")        | 2014年7月1日 - 9月22日        |
| 台中文化創意產業園區拾藝術                             | 台灣 [台中市](../Page/台中市.md "wikilink")                                    | 2016年12月24日 - 2017年4月9日  |

### 專賣店

  - :
    為2012年9月開放的官方商品專門店。在2014年3月的台灣台北市[西門町開設姊妹店](../Page/西門町.md "wikilink")，並正式命名為「台灣航海王專賣店」；2015年的大阪（阿倍野藉口商城）·名古屋（近鐵百貨名古屋店）·福岡（福岡Parco）3個城市開設姊妹店。

| 店鋪名稱     | 舉辦會場                                     | 地點                                                                 | 期間                     |
| -------- | ---------------------------------------- | ------------------------------------------------------------------ | ---------------------- |
| 大阪 草帽商店  | 天保山特設畫廊                                  | [大阪府](../Page/大阪府.md "wikilink")[大阪市](../Page/大阪市.md "wikilink")   | 2012年11月24日～2013年2月17日 |
| 小倉 草帽商店  | 正是在那裡City                                | [福岡縣](../Page/福岡縣.md "wikilink")[北九州市](../Page/北九州市.md "wikilink") | 2013年3月23日～5月26日       |
| 福岡 草帽商店  | [福岡塔](../Page/福岡塔.md "wikilink")         | 福岡縣[福岡市](../Page/福岡市.md "wikilink")                                | 2013年7月23日～月28日        |
| 長野 草帽商店  | 納加之東急百貨店                                 | [長野縣](../Page/長野縣.md "wikilink")[長野市](../Page/長野市.md "wikilink")   | 2013年7月26日～8月12日       |
| 川越 草帽商店  | 丸廣百貨店川越店                                 | [埼玉縣](../Page/埼玉縣.md "wikilink")[川越市](../Page/川越市.md "wikilink")   | 2013年12月26日～2014年1月8日  |
| 梅田 草帽商店  | 阪急百貨店梅田本店                                | 大阪府大阪市                                                             | 2014年3月5日～3月18日        |
| 名古屋 草帽商店 | [東急手創館名古屋店](../Page/東急手創館.md "wikilink") | [愛知縣](../Page/愛知縣.md "wikilink")[名古屋市](../Page/名古屋市.md "wikilink") | 2014年7月28日～8月10日       |
| 熊本 草帽商店  | 縣民百貨店                                    | [熊本縣](../Page/熊本縣.md "wikilink")[熊本市](../Page/熊本市.md "wikilink")   | 2014年8月9日～8月20日        |

### 其他

  -
    於2015年3月13日開始在[東京鐵塔開辦的大型主題公園](../Page/東京鐵塔.md "wikilink")。公園內有許多設施，如每個角色主題的景點和商店/餐廳\[5\]。

<!-- end list -->

  -
    活動期間為2015年7月18日至8月31日，在「お台場夢大陸〜 ドリームメガナツマツリ
    〜」在超技師人口隆重舉行與“團隊實驗室”合作。於2016年7月16日至7月31日，在LaLaport
    Fujimi舉行\[6\]。

<!-- end list -->

  -
    活動期間為2016年3月20日，在[沖繩縣](../Page/沖繩縣.md "wikilink")[北谷町舉辦的路跑活動](../Page/北谷町.md "wikilink")。最初是2015年5月在台灣舉行的反向企劃\[7\]。
      - 官方網站：[ONE PIECE RUN in 沖繩](http://onepiece-run.jp/)

<!-- end list -->

  -
    活動期間為2016年4月29日至8月31日，在沖繩縣[恩納村](../Page/恩納村.md "wikilink")「琉球村」舉辦展覽。展示篇章為「推進城〜頂上戰爭篇」\[8\]。

<!-- end list -->

  -
    活動期間為2017年8月26日至10月18日，在馬來西亞·吉隆坡的伊勢丹舉辦的企畫展覽會\[9\]。後來於2018年4月28日至5月13日在日本國內的札幌市舉辦\[10\]。

<!-- end list -->

  -
    活動期間為2017年10月7日至10月22日，本次是《ONE
    PIECE》連載20週年與日本京都市合作的紀念特展\[11\]\[12\]，並推出「ONE
    PIECE ART NUE
    大覺寺《魔獸與公主與誓言之花》」\[13\]\[14\]，近日並公開了將展出的日本畫一部分，由原作漫畫家尾田榮一郎所繪製的角色設定圖\[15\]。
    這是「京都草帽一夥道中記～另一個和之國～」的特別篇故事\[16\]\[17\]，將以大覺寺作為舞台來設計整個展覽的故事。除此之外，還有《ONE
    PIECE》與日本畫的合作，以水墨畫、繪卷等各種創作方式來呈現《航海王》的世界。
      - 官方網站：[航海王20th × KYOTO
        京都草帽一夥道中記～另一個和之國～](https://sp.shonenjump.com/j/op-20th-anniv/article/page_18.html)

<!-- end list -->

  -
    活動期間為2018年1月20日至2月25日（全90場公演），在劇場「DMM VR
    THEATER」上演，以全3D全息投影技術為應用方式來呈現出《ONE
    PIECE》世界\[18\]\[19\]。本次故事為原創的劇本，並有新的惡魔果實能力者登場。
      - 官方網站：[航海王戲劇舞台 THE METAL
        ～追憶的馬林福特～](https://vr-theater.dmm.com/schedule/onepiece-vr)

<!-- end list -->

  -
    活動期間為2018年8月12日至9月2日（全28場公演），在[東京國際論壇大樓上演全球首場的](../Page/東京國際論壇大樓.md "wikilink")「Brass
    Entertainment」，以音樂和表演合為一體的新奇表演藝術體現方式來呈現出《ONE PIECE》世界\[20\]。「Brass
    Entertainment」指以銅管樂器和敲擊樂器為中心，樂隊成員會在台上邊奏邊跳，提供視聽的雙重享受。本次活動表演的是「東海篇」。
      - 官方網站：[航海王音宴 ～東海篇～](https://otoutage.com/)

<table style="width:10%;">
<colgroup>
<col style="width: 3%" />
<col style="width: 3%" />
<col style="width: 3%" />
</colgroup>
<tbody>
<tr class="odd">
<td><dl>
<dt></dt>
<dd><strong>表演者</strong>
</dd>
</dl>
<ul>
<li>魯夫：松浦司</li>
<li>索隆：福地教光</li>
<li>騙人布：森良平</li>
<li>香吉士：高澤礁太</li>
<li>娜美：大北岬</li>
</ul></td>
<td><dl>
<dt></dt>
<dd><strong>音樂師</strong>
</dd>
</dl>
<ul>
<li>打擊樂：石川直</li>
<li>小號：米所裕夢</li>
<li>小號：Amanda Bateman</li>
<li>長號：Lisa Chappell</li>
<li>大號：Graham Roese</li>
<li>打擊樂：中部敬之</li>
</ul></td>
<td><dl>
<dt></dt>
<dd><strong>製作團隊</strong>
</dd>
</dl>
<ul>
<li>地點：東京國際中心 HALL C（東京・有樂町）</li>
<li>原作：尾田榮一郎</li>
<li>導演・編劇・編舞：金谷卡赫利</li>
<li>音樂監督：田中公平</li>
<li>提供：KYODO東京／富士電視台／東映動畫音樂出版</li>
<li>協賛：伊藤園</li>
<li>監修：集英社／東映ANIMETION</li>
<li>企劃製作：KYODO東京</li>
</ul></td>
</tr>
</tbody>
</table>

## 社會反響

### 發行量

在日本漫畫雜誌《[週刊少年Jump](../Page/週刊少年Jump.md "wikilink")》中長期佔據人氣榜的第一名；單行本在日本以外，亦已有30多個翻譯版本發行
2012年5月10日《ONE
PIECE》成為日本[Oricon排行榜從](../Page/Oricon.md "wikilink")2008年4月開始統計漫畫銷量以來，在該榜中首個總銷量破億冊的漫畫\[21\]。

2012年8月漫畫單行本第67卷初版發行量高達405萬冊，第九次刷新日本初版發行量紀錄\[22\]。2014年《ONE
PIECE》漫畫單行本的累計發行量在日本當地超過3億2,000萬冊（第76卷出版後）。第76卷漫畫初版發行量達到了400萬冊。這是自2011年11月發行的第64卷後，連續13卷單卷發行量達到400萬册。

《ONE
PIECE》漫畫的單行本在日本的初版發行數量在第57卷（2010年3月發售）達到了300萬冊，一舉超越了人氣小说《哈利波特和鳳凰社》（靜山社）的290萬冊，刷新了當時的紀錄。之後的第64卷（2011年11月發售）首次突破400萬冊大關，並在第67卷以405萬冊成為了目前為止的最高紀錄。

本作的翻譯出版的版本發表在超過全球35個國家已售出，正在播出的動畫在全球有40多個國家。日本以外的總發行量已經突破6000萬本（2014年12月）。至於目前全世界總發行量已經突破**4億3000萬本**（2017年10月）。

### 評價

## 獲獎殊榮

<table>
<thead>
<tr class="header">
<th><p>典禮</p></th>
<th><p>回數（一年）</p></th>
<th><p>部門和獎品</p></th>
<th><p>受獎對象</p></th>
<th><p>來源</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>日本媒體藝術100的選舉</p></td>
<td><p>（2006年）</p></td>
<td><p>漫畫部門</p></td>
<td><p>漫畫</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>日本漫畫家協會獎</p></td>
<td><p>第41回</p></td>
<td><p>大獎</p></td>
<td><p>[23]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>德國雄達人讀者選擇獎</p></td>
<td><p>（2005年）<br />
（2008年）<br />
（2009年）</p></td>
<td><p>國際漫畫部門最佳漫畫獎</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>動畫 &amp; 漫畫大獎<br />
（Anime &amp; Manga 19th Grand Prix）</p></td>
<td><p>第19回</p></td>
<td><p>漫畫部門最佳少年漫畫獎</p></td>
<td><p>[24]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>動畫部門 最佳經典系列獎</p></td>
<td><p>電視動畫</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>動畫神戶獎</p></td>
<td><p>第5回</p></td>
<td><p>AM神戶獎（主題歌獎）</p></td>
<td><p>ウィーアー!（We Are!）</p></td>
<td><p>[25]</p></td>
</tr>
<tr class="odd">
<td><p>東京動畫片獎</p></td>
<td><p>第1回</p></td>
<td><p>電視節目部門 優秀作品賞</p></td>
<td><p>電視動畫（<a href="../Page/東映動畫.md" title="wikilink">東映動畫</a>）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>日本金碟大獎</p></td>
<td><p>第22回</p></td>
<td><p>年度最佳動畫專輯</p></td>
<td><p><a href="../Page/ONE_PIECE_SUPER_BEST.md" title="wikilink">ONE PIECE SUPER BEST</a></p></td>
<td><p>[26]</p></td>
</tr>
<tr class="odd">
<td><p>第25回</p></td>
<td><p>年度最佳動畫專輯</p></td>
<td><p><a href="../Page/ONE_PIECE_MEMORIAL_BEST.md" title="wikilink">ONE PIECE MEMORIAL BEST</a></p></td>
<td><p>[27]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>GyaO！商店獎</p></td>
<td><p>（2012年）</p></td>
<td><p>全方位的銷售部門<br />
動畫部</p></td>
<td><p>電視動畫</p></td>
<td><p>[28]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/亞洲電視大獎.md" title="wikilink">亞洲電視大獎</a></p></td>
<td><p>（2012年）</p></td>
<td><p>動畫部門 大獎</p></td>
<td><p>[29]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>金總獎</p></td>
<td><p>第18回</p></td>
<td><p>優秀銀獎</p></td>
<td><p><a href="../Page/ONE_PIECE_黃金島大冒險.md" title="wikilink">黃金島大冒險</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第19回</p></td>
<td><p>優秀銀獎</p></td>
<td><p><a href="../Page/ONE_PIECE_發條島的冒險.md" title="wikilink">發條島的冒險</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第28回</p></td>
<td><p>優秀銀獎</p></td>
<td><p><a href="../Page/ONE_PIECE_FILM_強者天下.md" title="wikilink">ONE PIECE FILM STRONG WORLD</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第31回</p></td>
<td><p>優秀銀獎</p></td>
<td><p><a href="../Page/ONE_PIECE_FILM_Z.md" title="wikilink">ONE PIECE FILM Z</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/日本電影金像獎.md" title="wikilink">日本奧斯卡金像獎</a></p></td>
<td><p><a href="../Page/第32回日本電影金像獎.md" title="wikilink">第32回</a></p></td>
<td><p>優秀動畫作品獎</p></td>
<td><p><a href="../Page/ONE_PIECE_THE_MOVIE_喬巴身世之謎：冬季綻放、奇跡的櫻花.md" title="wikilink">喬巴身世之謎：冬季綻放、奇蹟的櫻花</a></p></td>
<td><p>[30]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/第34回日本電影金像獎.md" title="wikilink">第34回</a></p></td>
<td><p>優秀動畫作品獎</p></td>
<td><p>『ONE PIECE FILM STRONG WORLD』</p></td>
<td><p>[31]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/第36回日本電影金像獎.md" title="wikilink">第36回</a></p></td>
<td><p>優秀動畫作品獎</p></td>
<td><p>ONE PIECE FILM Z</p></td>
<td><p>[32]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>巴塞羅那亞洲電影節</p></td>
<td><p>第12回</p></td>
<td><p>觀眾獎（觀眾最喜愛影片）</p></td>
<td><p>ONE PIECE FILM STRONG WORLD</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>影像技術獎勵</p></td>
<td><p>第65回</p></td>
<td><p>動畫部門</p></td>
<td><p><a href="../Page/ONE_PIECE_3D_追逐草帽大冒險.md" title="wikilink">ONE PIECE 3D 追逐草帽大冒險</a></p></td>
<td><p>[33]</p></td>
</tr>
<tr class="odd">
<td><p>PlayStation Awards</p></td>
<td><p>（2002年）</p></td>
<td><p>金質獎</p></td>
<td><p>ONE PIECE 盛大對戰!<br />
ONE PIECE 盛大對戰! 2</p></td>
<td><p>[34]</p></td>
</tr>
<tr class="even">
<td><p>（2012年）</p></td>
<td><p>金質獎</p></td>
<td><p><a href="../Page/ONE_PIECE_海賊無雙.md" title="wikilink">ONE PIECE 海賊無雙</a></p></td>
<td><p>[35]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>（2013年）</p></td>
<td><p>金質獎</p></td>
<td><p><a href="../Page/ONE_PIECE_海賊無雙2.md" title="wikilink">ONE PIECE 海賊無雙2</a></p></td>
<td><p>[36]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/日本遊戲大獎.md" title="wikilink">日本遊戲大獎</a></p></td>
<td><p>（2012年）</p></td>
<td><p>優秀獎</p></td>
<td><p>ONE PIECE 海賊無雙</p></td>
<td><p>[37]</p></td>
</tr>
<tr class="odd">
<td><p>GREE Platform Award</p></td>
<td><p>（2014年）</p></td>
<td><p>優秀賞</p></td>
<td><p>ONE PIECE 冒險日誌</p></td>
<td><p>[38]</p></td>
</tr>
<tr class="even">
<td><p>年度許可</p></td>
<td><p>（2010年）<br />
（2012年）</p></td>
<td><p>大獎</p></td>
<td><p>ONE PIECE</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>（2011年）</p></td>
<td><p>人物角色授權獎</p></td>
<td><p>ONE PIECE</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>促銷許可獎</p></td>
<td><p>ONE PIECE 千雪姬號</p></td>
<td><p>[39]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>日本玩具大獎</p></td>
<td><p>（2011年）</p></td>
<td><p>人物角色·玩具部門 大獎</p></td>
<td><p>ONE PIECE LOGBOX</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>移動式廣告大獎</p></td>
<td><p>第10回</p></td>
<td><p>市場營銷部門 入獎</p></td>
<td><p>羅森ARG 海賊王 收集圖章活動的應用</p></td>
<td><p>[40]</p></td>
</tr>
<tr class="odd">
<td><p>東京互動廣告獎</p></td>
<td><p>第9回</p></td>
<td><p>申請部門 移動式應用 入賞</p></td>
<td><p>[41]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>顯示器行業大獎</p></td>
<td><p>第31回</p></td>
<td><p>顯示器産業激勵獎</p></td>
<td><p>尾田栄一郎監修 ONE PIECE展〜原画×映像×体感的海賊王</p></td>
<td><p>[42]</p></td>
</tr>
<tr class="odd">
<td><p>日本商品化權協會獎</p></td>
<td><p>（2012年）</p></td>
<td></td>
<td><p>『ONE PIECE』（東映動畫）</p></td>
<td><p>[43]</p></td>
</tr>
<tr class="even">
<td><p>讀賣新聞出版廣告獎</p></td>
<td><p>第18回</p></td>
<td><p>金賞</p></td>
<td><p>ONE PIECE 漫畫3億冊突破記念運動<br />
「日本縱向! OPJ47 巡航」</p></td>
<td><p>[44]</p></td>
</tr>
<tr class="odd">
<td><p>新聞廣告獎</p></td>
<td><p>第34回</p></td>
<td><p>廣告主部門 大獎</p></td>
<td><p>[45]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>廣告電通獎</p></td>
<td><p>第67回</p></td>
<td><p>新聞廣告 企畫部門 最優秀獎</p></td>
<td><p>[46]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>銅環獎</p></td>
<td><p>（2014年）</p></td>
<td><p>現場娛樂傑出部門<br />
最佳整體表現製作獎</p></td>
<td><p>海賊王 尊貴展</p></td>
<td><p>[47]</p></td>
</tr>
</tbody>
</table>

## 譯名糾紛

台灣譯名版權糾紛，因大然文化最先標得海賊王漫畫之版權後因經營不善而倒閉，之後由其他公司陸續接手，但大然文化不願釋出譯名版權，導致接手公司只好改名發行，海賊王也受其害，被更改為與內容不符的名稱「航海王」，因此被眾多讀者視為不尊重作者的本意。中國大陆也受此影響，使得凡官方版权要求使用航海王這一名稱。

## 爭議

  - 第89冊日文版的封面摺頁，嘲諷[横井軍曹](../Page/橫井庄一.md "wikilink")，出現爭議\[48\]。台灣中文版因此刪去封面摺頁，摺頁為空白。

## 參考資料

### 備註

### 來源

## 外部連結

**原作漫畫**

  - [ONE PIECE.com](https://one-piece.com/) - 尾田榮一郎公認的官方公式網站

  -
  - [ONE PIECE WEB](http://www.j-onepiece.com/)

  - [ONE PIECE GRAND
    STORY](https://web.archive.org/web/20131211221051/http://grandstory.one-piece.com/)

  - [腾讯动漫连载网址](http://ac.qq.com/Comic/comicInfo/id/505430/)

**動畫**

  - [ONE PIECE 東映官網](http://www.toei-anim.co.jp/tv/onep/)

  - [ONE PIECE -
    富士電視台股份有限公司](https://web.archive.org/web/20050414032935/http://www.fujitv.co.jp/b_hp/onepiece/)

  - [「ONE PIECE」DVD公式網站](http://mv.avex.jp/onepiece/)

  -
  -
  - [台灣台視《航海王》官網](http://www.ttv.com.tw/drama/2005/cartoon/onepeace/01-story.htm)

  - [台視「航海王新視界映畫祭」](http://www.ttv.com.tw/drama14/onepiece/default.asp)

**遊戲**

  - [ONE PIECE GAME PORTAL
    萬代南夢宮遊戲官方網站](http://www.bandaigames.channel.or.jp/list/one_main/)

**舞台秀**

  - [ONE PIECE LIVE ATTRACTION 東京海賊塔](https://onepiecetower.tokyo/)

  -
  - [ONE PIECE PREMIER SUMMER
    海賊USJ城](https://web.archive.org/web/20161019084600/http://www.usj.co.jp/jump/)

**商品專賣店**

  - [ONE PIECE 草帽商店](http://www.mugiwara-store.com/)

  -
  -
  - [台灣航海王專賣店](http://www.mugiwara-store-taiwan.com/)

  -
  - [台灣航海王授權商-成真文創](http://www.chengjen.com.tw/)

[ONE PIECE](../Category/ONE_PIECE.md "wikilink")
[Category:日本漫畫作品](../Category/日本漫畫作品.md "wikilink")
[Category:週刊少年Jump連載作品](../Category/週刊少年Jump連載作品.md "wikilink")
[Category:海盜題材作品](../Category/海盜題材作品.md "wikilink")
[Category:未完結作品](../Category/未完結作品.md "wikilink")

1.  [PIECE』、累計売上1億部突破](http://www.oricon.co.jp/news/ranking/2011350『ONE)

2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14.
15.
16.
17.
18.
19.
20.
21. [PIECE』、累計売上1億部突破](http://www.oricon.co.jp/news/ranking/2011350『ONE)

22.
23. [過去的獲獎者（日本漫畫家協會獎和文化，體育，科學和技術部長獎）](http://www.nihonmangakakyokai.or.jp/award.php)
    日本漫畫家協會
24. FRA
25. [第五屆動畫 神戶獎的決定](http://www.anime-kobe.jp/archive/2000/) 動畫神戶
26. [第二十二屆日本金唱片大獎](http://www.golddisc.jp/award/22/Prize_4.html#gd22_22)
    THE JAPAN GOLD DISC AWARD2008 官方網站
27. [第二十五屆日本金唱片大獎](http://www.golddisc.jp/award/25/Prize_2.html#gd25_12)THE
    JAPAN GOLDDISC AWARD2011　官方網站
28. [GyaO！最薪資輸送有很多的獎項揭曉一年一度的“海賊王”](http://animeanime.jp/article/2012/11/18/12100.html)動畫片！動畫片！2012年11月18日
29. \[ フ大獎得主在節目富士電視台製作的亞洲電視大獎！ ！
    [富士電視台](../Page/富士電視台.md "wikilink")、2012年12月10日
30. [第32回日本電影金像獎](http://www.japan-academy-prize.jp/prizes/?t=32#title27)
     [日本電影金像獎官方網站](../Page/日本電影金像獎.md "wikilink")
31. [第34回日本電影金像獎](http://www.japan-academy-prize.jp/prizes/?t=34#title27)
    [日本電影金像獎官方網站](../Page/日本電影金像獎.md "wikilink")
32. [第36回日本電影金像獎](http://www.japan-academy-prize.jp/prizes/?t=36)
    [日本電影金像獎官方網站](../Page/日本電影金像獎.md "wikilink")
33. [映像技術獎　受獎一覧](http://www.mpte.jp/outline/kennsyou/eg.html) 日本電影電視技術協會
34.
35.
36.
37. [日本遊戲大獎2012](http://awards.cesa.or.jp/prize/prize_01.html)
    [計算機娛樂供應商協會](../Page/計算機娛樂供應商協會.md "wikilink")
38.
39. [日本玩具大賞2011](http://www.toys.or.jp/toyshow/toyshow2011/omocha_taisyou2011.html)
    日本玩具協会
40. [第10回（2011年）
    受獎作品](http://www.mobileadawards.com/archive2011/work03.html#win02)
     移動式廣告大獎
41. [第9回東京互動廣告獎
    受獎作品發表](http://tokyo.interactive.ad.awards.jp/Results11/info/34.html)
    [東京インタラクティブ·アド·アワード](../Page/東京インタラクティブ·アド·アワード.md "wikilink")
42. [第31回顯示器行業大獎（2012）入獎作品](http://www.display.or.jp/zzaward/s2012/index02.html)
    日本顯示器業團體連合會
43. [日本商品化權協會獎　選出3人物角色](http://www.sankeibiz.jp/business/news/130206/bsj1302060503000-n1.htm)
     [SankeiBiz](../Page/SankeiBiz.md "wikilink")、2013年2月6日
44.
45.
46.
47.
48.