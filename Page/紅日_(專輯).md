《**红日**》為[香港歌手](../Page/香港.md "wikilink")[李克勤於](../Page/李克勤.md "wikilink")[寶麗金唱片公司時期的最後一張大碟](../Page/寶麗金唱片.md "wikilink")，大碟推出時間为1992年10月\[1\]。之後他便轉投至[星光唱片](../Page/星光唱片.md "wikilink")。

本大碟內收錄了11首歌曲，當中包括至今仍視為[勵志歌曲經典之作](../Page/勵志.md "wikilink")《紅日》\[2\]\[3\]。另外，與[周慧敏合唱的](../Page/周慧敏.md "wikilink")《萬千寵愛在一身》中場一段獨白最後一句「點解啲女人成日都咁多嘢問」成為經典的合唱歌曲，及翻唱由[譚炳文原唱](../Page/譚炳文.md "wikilink")、重新編曲及演繹的《舊歡如夢》，李克勤所跳的「騎呢」舞步，都為當時佳話，但很多人也視此曲為李克勤的代表作。此外，專輯中的《你是我的太陽》是當時李克勤為追求將來的太太[盧淑儀所創作的歌曲](../Page/盧淑儀_\(香港\).md "wikilink")。

## 曲目

## 參考

[Category:1992年音樂專輯](../Category/1992年音樂專輯.md "wikilink")
[Category:香港音樂專輯](../Category/香港音樂專輯.md "wikilink")
[Category:李克勤音樂專輯](../Category/李克勤音樂專輯.md "wikilink")

1.
2.
3.