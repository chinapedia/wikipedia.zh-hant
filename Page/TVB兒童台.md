**TVB兒童-{台}-**（；前稱****）是[香港](../Page/香港.md "wikilink")[無綫網絡電視](../Page/無綫網絡電視.md "wikilink")（2013年前身為[無綫收費電視](../Page/無綫收費電視.md "wikilink")）擁有的一條以兒童為對象而設的頻道，該頻道會播放家長可與小朋友一起收看最新的合家歡及益智的卡通，加強親子關係，而且亦會舉辦一系列由小朋友參與之選舉活動、讓小朋友發揮天賦才藝及潛質。2006年3月1日更名為“無綫兒童台”。

該頻道的主要競爭對手是[香港有線電視擁有的](../Page/香港有線電視.md "wikilink")[兒童台](../Page/有線兒童台.md "wikilink")。

## 歷史

2004年1月27日，「**銀河衞視**」開始試播，該頻道以「」作為頻道名稱正式開播。

2006年3月1日，為配合「**SuperSun
新電視**」易名「**無綫收費電視**」，其頻道名稱將「」更名為「**無綫兒童-{台}-**」，同年5月10日開始正式透過無綫收費電視使用第11頻道收看和[now寬頻電視的無綫收費電視特選組合第](../Page/now寬頻電視.md "wikilink")811頻道播出。

2013年4月1日，無綫網絡電視部分頻道號碼正式更改，其中該頻道將使用原有的第11頻道更改為現有的第10頻道。\[1\]

2013年4月29日，該頻道將中文名稱的「**無綫兒童-{台}-**」更名為「**兒童-{台}-**」。

2015年4月1日早上6時，該頻道終止播映，唯海外[無綫通識台依然維持播放至今](../Page/無綫通識台.md "wikilink")。

## 播放模式

「tvbQ」開台初期分為兩部份，早上6時正至晚上8時正為兒童節目時段，包括自製節目《小 Teen
地》，外購真人演出之兒童節目《[天線得得B](../Page/天線得得B.md "wikilink")》，另有不少歐美動畫片集及重播的日本動畫片集《[忍者小靈精](../Page/忍者小靈精.md "wikilink")》及《[我係小忌廉](../Page/我係小忌廉.md "wikilink")》；而晚上8時正至翌日早上6時正則會播放
[BBC製作的紀錄片](../Page/英國廣播公司.md "wikilink")，自製節目[中西醫療坊及](../Page/中西醫療坊.md "wikilink")[成長的故事](../Page/成長的故事.md "wikilink")，外購劇集（例如：[我的這一班](../Page/我的這一班_\(台灣電視劇\).md "wikilink")）及資訊節目（例如：[寵物大本營](../Page/寵物當家.md "wikilink")），非以兒童為對象的動畫片集，包括：《[龍珠](../Page/龍珠.md "wikilink")》、《[金田一少年之事件簿](../Page/金田一少年之事件簿_\(動畫\).md "wikilink")》、《[火影忍者](../Page/火影忍者.md "wikilink")》、《[長空浪族](../Page/LAST_EXILE.md "wikilink")》等。

於2005年6月13日晚上開始，停止播放非以兒童為對象的動畫片集，而受到影響之動畫片集《[龍珠](../Page/龍珠.md "wikilink")》及《[火影忍者](../Page/火影忍者.md "wikilink")》則由同年6月18日開始改於[無綫音樂台播放](../Page/TVB音樂台.md "wikilink")，但是《[長空浪族](../Page/LAST_EXILE.md "wikilink")》仍然安排於「tvbQ」的晚間時段播放直至6月26日。

於2005年6月27日開始，「tvbQ」的晚間至凌晨之非兒童節目時段取消，換言之全日24小時均改為播放兒童節目及以兒童為對象之動畫片集。

## 菠蘿家族

  - 菠蘿龍
  - Mr.mI
  - 菠蘿的的
  - 菠蘿識識
  - 菠蘿DogDog
  - 菠蘿WowWow
  - 眼鏡蛇
  - 望望鏡
  - 蔥蔥

## 主持

<table>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><strong>無綫收費電視兒童台(2004-2009)主持</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/傅子欣.md" title="wikilink">傅子欣</a><br />
(首席主持)</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/梁珈詠.md" title="wikilink">梁珈詠</a><br />
(副首席主持兼<a href="../Page/放學ICU.md" title="wikilink">放學ICU主持</a>)</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/霍健邦.md" title="wikilink">霍健邦</a><br />
(高級主持兼<a href="../Page/放學ICU.md" title="wikilink">放學ICU主持</a>)</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

## 小主持

<table>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><strong>無綫收費電視兒童台(2004-2010)小主持</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/許穎童.md" title="wikilink">許穎童</a><br />
(首席小主持兼<a href="../Page/放學ICU.md" title="wikilink">放學ICU小主持</a>)</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/何韻熙.md" title="wikilink">何韻熙</a><br />
(高級小主持兼<a href="../Page/放學ICU.md" title="wikilink">放學ICU小主持</a>)</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/黃樂霖.md" title="wikilink">黃樂霖</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

## 兒童節目

  - [小TEEN地](../Page/小TEEN地.md "wikilink")
  - [Mr.mI遊樂場](../Page/Mr.mI遊樂場.md "wikilink")
  - [夢幻樂園](../Page/夢幻樂園.md "wikilink")
  - [成長的故事](../Page/成長的故事.md "wikilink")
  - [知知JUMP](../Page/知知JUMP.md "wikilink")
  - [SuperKids拔星賽](../Page/SuperKids拔星賽.md "wikilink")
  - [Q仔Q女生日會](../Page/Q仔Q女生日會.md "wikilink")
  - [電視城歷奇生日派對](../Page/電視城歷奇生日派對.md "wikilink")
  - [兒童演藝學院](../Page/兒童演藝學院.md "wikilink")
  - [猩猩同學會](../Page/猩猩同學會.md "wikilink")

## 動畫

  - [寶石寵物系列](../Page/寶石寵物.md "wikilink")

  - [雙子公主](../Page/雙子公主.md "wikilink")

  - [寵物小精靈](../Page/寵物小精靈.md "wikilink")(Gameboy遊戲)

  - [寵物小精靈超世代](../Page/寵物小精靈超世代.md "wikilink")

  - [寵物小精靈DP](../Page/寵物小精靈DP.md "wikilink")

  - [寵物小精靈：超級願望](../Page/寵物小精靈：超級願望.md "wikilink")

  - [動物橫町](../Page/動物橫町.md "wikilink")

  - [唐詩家族](../Page/唐詩家族.md "wikilink")

  - [冒險遊記](../Page/冒險遊記Pluster_World.md "wikilink")

  - [決鬥大師](../Page/決鬥大師.md "wikilink")

  - [小鯉魚歷險記](../Page/小鯉魚歷險記.md "wikilink")

  - 繽紛熊貓國度 (Panshel's World)

  -
  -
  - [我們這一家](../Page/我們這一家.md "wikilink")

  - [小魔女Doremi](../Page/小魔女Doremi.md "wikilink")

  - [爆旋陀螺](../Page/爆旋陀螺.md "wikilink")

  - [爆旋陀螺II](../Page/爆旋陀螺.md "wikilink")

  - [愛探險的Dora](../Page/愛探險的朵拉.md "wikilink")

  - [花漾明星系列](../Page/花漾明星.md "wikilink")

  - [摺紙戰士](../Page/摺紙戰士.md "wikilink")

  - [貓狗寵物街](../Page/貓狗寵物街.md "wikilink")

  - [新宇宙飛龍](../Page/新宇宙飛龍.md "wikilink")

  - [四驅兄弟](../Page/四驅兄弟.md "wikilink")

  - [魔動王](../Page/魔動王.md "wikilink")

  - 經典勇者系列-[太陽勇者](../Page/太陽勇者.md "wikilink")

  - 經典勇者系列-[勇者急先鋒](../Page/勇者特急隊.md "wikilink")

  - 經典勇者系列-[勇者警察](../Page/勇者警察.md "wikilink")

  - 經典勇者系列-[黃金勇者](../Page/黃金勇者.md "wikilink")

  - 經典勇者系列-[勇者指令](../Page/勇者指令.md "wikilink")

  - 經典勇者系列-[勇者王](../Page/勇者王.md "wikilink")

  - [真仙魔大戰](../Page/真仙魔大戰.md "wikilink")

  - [守護蛋精靈](../Page/守護甜心.md "wikilink")

  - [滴骰孖妹](../Page/滴骰孖妹.md "wikilink")

  - [星夢美少女](../Page/星夢美少女.md "wikilink")

  - [我家3姊妹](../Page/我家3姊妹.md "wikilink")

  - [飛天小女警Z](../Page/飛天小女警Z.md "wikilink")

  - [閃電十一人](../Page/閃電十一人.md "wikilink")

  - [我們這一家](../Page/我們這一家.md "wikilink")

  - [喜羊羊與灰太狼](../Page/喜羊羊與灰太狼.md "wikilink")

  - [哪吒傳奇](../Page/哪吒傳奇.md "wikilink")

  - [數碼暴龍系列](../Page/數碼暴龍.md "wikilink")

  - [KERORO軍曹](../Page/KERORO軍曹.md "wikilink")

  - [外星BB撞地球](../Page/外星BB撞地球.md "wikilink")

  - [娛樂金魚眼](../Page/娛樂金魚眼.md "wikilink")

  - [老夫子系列](../Page/老夫子.md "wikilink")

  - [魔界女王候補生](../Page/魔界女王候補生.md "wikilink")

  - [Battle Spirits 少年突破馬神](../Page/Battle_Spirits_少年突破馬神.md "wikilink")

  - [Battle Spirits 少年激霸](../Page/Battle_Spirits_少年激霸彈.md "wikilink")

  - [Battle Spirits 少年勇者](../Page/Battle_Spirits_Brave.md "wikilink")

  - [Battle Spirits 小霸王](../Page/Battle_Spirits_小霸王.md "wikilink")

  -
  - [絕對無敵](../Page/絕對無敵.md "wikilink")

  - [熱血最強](../Page/熱血最強.md "wikilink")

  - [元氣爆發](../Page/元氣爆發.md "wikilink")

  - [四驅小子](../Page/四驅小子.md "wikilink")

  - [奇幻魔法Melody系列](../Page/奇幻魔法Melody.md "wikilink")

  - [哈姆太郎](../Page/哈姆太郎.md "wikilink")

  - [爆丸系列](../Page/爆丸.md "wikilink")

  - [麵包超人](../Page/麵包超人.md "wikilink")

  - [福星大咀鳥](../Page/大嘴鳥_\(動畫\).md "wikilink")

  - [福娃奧運漫遊記](../Page/福娃奧運漫遊記.md "wikilink")

  -
  - [足球小將](../Page/足球小將.md "wikilink")

  - [寵物反斗星](../Page/寵物反斗星.md "wikilink")

  - [網絡小精靈](../Page/光速大冒險PIPOPA.md "wikilink")

  -
  - [丁丁歷險記](../Page/丁丁歷險記.md "wikilink")(歐洲經典漫畫改編，粵語版從未在翡翠台播出)

  - [旋風四驅王](../Page/旋風四驅王.md "wikilink")

  - [我係小忌廉](../Page/我係小忌廉.md "wikilink")

  -
  - [忍者亂太郎](../Page/忍者亂太郎.md "wikilink")

  - [閃電十一人GO](../Page/閃電十一人GO.md "wikilink")

  - [快樂酷寶](../Page/快樂酷寶.md "wikilink")

  - [BB保你大](../Page/BB保你大.md "wikilink")

  - [自古英雄出少年](../Page/自古英雄出少年.md "wikilink")

  - [超級小黑咪](../Page/超級小黑咪.md "wikilink")

  - [幪面貓俠](../Page/幪面貓俠.md "wikilink")

  - [馬達加斯加的企鵝系列](../Page/馬達加斯加的企鵝.md "wikilink")

  - [功夫熊貓至尊傳奇](../Page/功夫熊貓至尊傳奇.md "wikilink")

  - [倒霉熊](../Page/倒霉熊.md "wikilink")

  - [深藍救兵](../Page/深藍救兵.md "wikilink")

## 海外版本

## 參考資料

<references/>

## 外部連結

[音](../Category/電視廣播有限公司電視頻道.md "wikilink")
[Category:2006年成立的電視台或電視頻道](../Category/2006年成立的電視台或電視頻道.md "wikilink")
[\*](../Category/香港已停播的電視頻道.md "wikilink")
[Category:粤语电视频道](../Category/粤语电视频道.md "wikilink")

1.