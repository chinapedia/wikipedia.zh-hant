**雙鉤葉屬**（[拉丁語](../Page/拉丁語.md "wikilink")：*Dioncophyllum*）為[雙鉤葉科單型的](../Page/雙鉤葉科.md "wikilink")[植物](../Page/植物.md "wikilink")[屬](../Page/屬.md "wikilink")，相當於本屬唯一種類──**托倫雙鉤葉**（[學名](../Page/學名.md "wikilink")：*Dioncophyllum
thollonii*）。本種為[藤本植物](../Page/攀緣植物.md "wikilink")，全長可達30 -
40公尺，原產於[西非的熱帶](../Page/西非.md "wikilink")[雨林](../Page/雨林.md "wikilink")，證實可發現於[加彭](../Page/加彭.md "wikilink")、[剛果共和國](../Page/剛果共和國.md "wikilink")、[刚果民主共和国](../Page/刚果民主共和国.md "wikilink")。本種如同其他的熱帶藤本植物，需要高溫與高度的濕氣。

## 語源學

本屬的名稱是取自科名，源自於[希臘語](../Page/希臘語.md "wikilink")（di= 成雙；onco= 爪形；phyllum=
葉片），歸因於葉上的兩個鉤爪狀攀附器官。種名則是對托倫（Thollon）表示敬意，他於1888年發現本種植物。

## 參考文獻

<div class="references-small">

  - Airy Shaw: ''On the *Dioncophyllaceae*, a remarkable new family of
    flowering plants.'' Kew Bulletin 327–347, 1951
  - Messer, K. S.: *Isolierung, Strukturaufklärung und Beiträge zur
    Synthese von Naturstoffen aus tropischen Heilpflanzen sowie
    Etablierung chiraler On-line-Analytik.* Diss. Universität Würzburg,
    2002. [Volltext
    online](http://opus.bibliothek.uni-wuerzburg.de/opus/volltexte/2002/377/)

</div>

## 外部連結

  - [Aluka - Dioncophyllum tholloni
    Baill.](http://www.aluka.org/action/showCompilationPage?doi=10.5555%2FAL.AP.COMPILATION.PLANT-NAME-SPECIES.DIONCOPHYLLUM.THOLLONI)
    托倫雙鉤葉的臘葉標本

[Category:双钩叶科](../Category/双钩叶科.md "wikilink")