**新垣 渚**（あらかき なぎさ、1980年5月9日 -
），[日本職棒原福岡軟體銀行鷹及](../Page/日本職棒.md "wikilink")[東京養樂多燕子隊球員](../Page/東京養樂多燕子.md "wikilink")，2016年引退。現為福岡軟體銀行鷹球團職員。

## 概要

  - 身高・體重：190 cm 83 kg
  - 投打：右投右打
  - 出生地：[沖繩縣](../Page/沖繩縣.md "wikilink")[那霸市](../Page/那霸市.md "wikilink")
  - 血型：O型
  - 球歴：沖繩水產高 - 九共大 - 福岡大榮鷹ー・福岡軟體銀行鷹（2003年 - ）　
  - FA取得：未
  - 選秀年：2002年（自由選擇）
  - 英語表記：**ARAKAKI**
  - 背番号：**18**
  - 推定年薪：1億2000萬日圓（2007年）
  - 守備位置：投手 (四分之三投球)
      - 松坂世代之一，與[和田毅](../Page/和田毅.md "wikilink")、[杉内俊哉互為競爭對手](../Page/杉内俊哉.md "wikilink")。
      - 與[齊藤和巳](../Page/齊藤和巳.md "wikilink")、[和田毅](../Page/和田毅.md "wikilink")、[杉內俊哉並稱為軟銀四本柱](../Page/杉內俊哉.md "wikilink")。
      - 2007年創下日職史上單季最多暴投紀錄(25個)。
      - 暱稱為「**ナギ**」(na gi)。

## 通算成績

<table>
<thead>
<tr class="header">
<th><p>年度|</p></th>
<th><p>球團|</p></th>
<th><p>出場|</p></th>
<th><p>先發|</p></th>
<th><p>完投|</p></th>
<th><p>完封|</p></th>
<th><p>無四球|</p></th>
<th><p>勝利|</p></th>
<th><p>敗戰|</p></th>
<th><p>救援|</p></th>
<th><p>中繼點|</p></th>
<th><p>勝率|</p></th>
<th><p>打者|</p></th>
<th><p>投球<br />
局數|</p></th>
<th><p>被<br />
安<br />
打|</p></th>
<th><p>被全壘打|</p></th>
<th><p>保送|</p></th>
<th><p>敬遠|</p></th>
<th><p>觸身球|</p></th>
<th><p>三<br />
振<br />
數|</p></th>
<th><p>暴投|</p></th>
<th><p>犯規|</p></th>
<th><p>失分|</p></th>
<th><p>自<br />
責<br />
分|</p></th>
<th><p>防<br />
禦<br />
率|</p></th>
<th><p>WHIP</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2003</p></td>
<td><p><a href="../Page/福岡軟體銀行鷹.md" title="wikilink">大榮鷹<br />
軟體銀行鷹</a></p></td>
<td><p>18</p></td>
<td><p>18</p></td>
<td><p>8</p></td>
<td><p>1</p></td>
<td><p><strong>1</strong></p></td>
<td><p>8</p></td>
<td><p>7</p></td>
<td><p>0</p></td>
<td><p>--</p></td>
<td><p>.533</p></td>
<td><p>505</p></td>
<td><p>121.1</p></td>
<td><p>110</p></td>
<td><p>10</p></td>
<td><p>30</p></td>
<td><p>0</p></td>
<td><p>6</p></td>
<td><p>132</p></td>
<td><p>5</p></td>
<td><p>0</p></td>
<td><p>52</p></td>
<td><p>45</p></td>
<td><p>3.34</p></td>
<td><p>1.15</p></td>
</tr>
<tr class="even">
<td><p>2004</p></td>
<td><p>25</p></td>
<td><p>25</p></td>
<td><p>9</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>11</p></td>
<td><p>8</p></td>
<td><p>0</p></td>
<td><p>--</p></td>
<td><p>.579</p></td>
<td><p><strong>807</strong></p></td>
<td><p><strong>192.1</strong></p></td>
<td><p>173</p></td>
<td><p>14</p></td>
<td><p><strong>73</strong></p></td>
<td><p>0</p></td>
<td><p>11</p></td>
<td><p><strong>177</strong></p></td>
<td><p><strong>8</strong></p></td>
<td><p>0</p></td>
<td><p>75</p></td>
<td><p>70</p></td>
<td><p>3.28</p></td>
<td><p>1.28</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2005</p></td>
<td><p>22</p></td>
<td><p>21</p></td>
<td><p>3</p></td>
<td><p>2</p></td>
<td><p>0</p></td>
<td><p>10</p></td>
<td><p>6</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>.625</p></td>
<td><p>600</p></td>
<td><p>136.2</p></td>
<td><p>146</p></td>
<td><p>13</p></td>
<td><p>54</p></td>
<td><p>1</p></td>
<td><p>9</p></td>
<td><p>130</p></td>
<td><p>7</p></td>
<td><p>1</p></td>
<td><p>73</p></td>
<td><p>70</p></td>
<td><p>4.61</p></td>
<td><p>1.46</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2006</p></td>
<td><p>23</p></td>
<td><p>23</p></td>
<td><p>5</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>13</p></td>
<td><p>5</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>.722</p></td>
<td><p>641</p></td>
<td><p>155.1</p></td>
<td><p>132</p></td>
<td><p>10</p></td>
<td><p>46</p></td>
<td><p>0</p></td>
<td><p>7</p></td>
<td><p>151</p></td>
<td><p>10</p></td>
<td><p>0</p></td>
<td><p>60</p></td>
<td><p>52</p></td>
<td><p>3.01</p></td>
<td><p>1.15</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2007</p></td>
<td><p>21</p></td>
<td><p>21</p></td>
<td><p>2</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>7</p></td>
<td><p>10</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>.412</p></td>
<td><p>581</p></td>
<td><p>137.1</p></td>
<td><p>128</p></td>
<td><p>7</p></td>
<td><p>51</p></td>
<td><p>0</p></td>
<td><p>4</p></td>
<td><p>132</p></td>
<td></td>
<td><p>0</p></td>
<td><p>62</p></td>
<td><p>55</p></td>
<td><p>3.60</p></td>
<td><p>1.30</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2008</p></td>
<td><p>15</p></td>
<td><p>15</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>4</p></td>
<td><p>6</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>.400</p></td>
<td><p>411</p></td>
<td><p>92.2</p></td>
<td><p>89</p></td>
<td><p>7</p></td>
<td><p>39</p></td>
<td><p>0</p></td>
<td><p>6</p></td>
<td><p>105</p></td>
<td><p><strong>15</strong></p></td>
<td><p>2</p></td>
<td><p>55</p></td>
<td><p>43</p></td>
<td><p>4.18</p></td>
<td><p>1.38</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2009</p></td>
<td><p>4</p></td>
<td><p>4</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>2</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>.000</p></td>
<td><p>95</p></td>
<td><p>19.1</p></td>
<td><p>31</p></td>
<td><p>5</p></td>
<td><p>10</p></td>
<td><p>0</p></td>
<td><p>1</p></td>
<td><p>15</p></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>17</p></td>
<td><p>17</p></td>
<td><p>7.91</p></td>
<td><p>2.12</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>通算:7年</p></td>
<td><p>128</p></td>
<td><p>127</p></td>
<td><p>27</p></td>
<td><p>8</p></td>
<td><p>3</p></td>
<td><p>53</p></td>
<td><p>44</p></td>
<td><p>0</p></td>
<td><p>0</p></td>
<td><p>.546</p></td>
<td><p>3640</p></td>
<td><p>855.2</p></td>
<td><p>809</p></td>
<td><p>66</p></td>
<td><p>303</p></td>
<td><p>1</p></td>
<td><p>44</p></td>
<td><p>842</p></td>
<td><p>73</p></td>
<td><p>3</p></td>
<td><p>394</p></td>
<td><p>352</p></td>
<td><p>3.71</p></td>
<td><p>1.30</p></td>
<td></td>
</tr>
</tbody>
</table>

  - 2011年度結束時
  - 各年度粗體字為聯盟最高、<span style="color:red;">**粗紅字**</span>是日職歴代最高

## 書籍

  - 渚 NAGISA （2007年5月11日、[講談社](../Page/講談社.md "wikilink")） ISBN
    978-4062139687
      -
        ビジュアルノンフィクション。写真は[スエイシナオヨシ撮影](../Page/スエイシナオヨシ.md "wikilink")。

## 廣告

  - [フタタ](../Page/フタタ.md "wikilink")　『プライス真っ二つ編』
  - [四季工房](../Page/四季工房.md "wikilink")　『ベストを尽くす編』　『子供の頃から編』

## 獎項

  - [三振王](../Page/三振王.md "wikilink")（2004年）
  - [月間MVP](../Page/最優秀選手_\(野球\)#月間MVP.md "wikilink")
    （2003年7月度、2004年7月度、2005年9月度、2006年3・4月度）

<!-- end list -->

  -
    洋聯史上最快以投手取得四次MVP，西武的[西口文也五次為目前洋聯最多紀錄投手](../Page/西口文也.md "wikilink")。

<!-- end list -->

  - [JA全農Go・Go賞](../Page/JA全農Go・Go賞.md "wikilink")
      - 「最多奪三振賞」 （King of K） （2007年5月度）

## 經歷

沖繩水產（甲子園）－九州共立大－大榮鷹ー軟體銀行鷹

## 関連項目

  - [王貞治](../Page/王貞治.md "wikilink")（監督）
  - [秋山信二](../Page/秋山信二.md "wikilink")（監督）
  - [城島健司](../Page/城島健司.md "wikilink")
  - [松中信彦](../Page/松中信彦.md "wikilink")
  - [和田毅](../Page/和田毅.md "wikilink")
  - [杉內俊哉](../Page/杉內俊哉.md "wikilink")
  - [稲嶺誉](../Page/稲嶺誉.md "wikilink")
  - [山村路直](../Page/山村路直.md "wikilink")
  - [田上秀則](../Page/田上秀則.md "wikilink")
  - [馬原孝浩](../Page/馬原孝浩.md "wikilink")
  - [高橋秀聰](../Page/高橋秀聰.md "wikilink")
  - [的場直樹](../Page/的場直樹.md "wikilink")
  - [川崎宗則](../Page/川崎宗則.md "wikilink")
  - [齊藤和巳](../Page/齊藤和巳.md "wikilink")
  - [松坂大輔](../Page/松坂大輔.md "wikilink")
  - [松坂世代](../Page/松坂世代.md "wikilink")
  - [三輪田勝利](../Page/三輪田勝利.md "wikilink")
  - [水産高校](../Page/水産高校.md "wikilink")
  - [おすぎ](../Page/おすぎ.md "wikilink")

## 外部連結

  - [新垣渚官方網站](https://web.archive.org/web/20081006195049/http://nagisa18.com/)

  - [福岡軟體銀行鷹隊—新垣渚基本資料](https://web.archive.org/web/20070407034417/http://www.softbankhawks.co.jp/players/024.html)

[Category:1980年出生](../Category/1980年出生.md "wikilink")
[Category:在世人物](../Category/在世人物.md "wikilink")
[Category:日本棒球選手](../Category/日本棒球選手.md "wikilink")
[Category:沖繩縣出身人物](../Category/沖繩縣出身人物.md "wikilink")
[Category:福岡軟體銀行鷹隊球員](../Category/福岡軟體銀行鷹隊球員.md "wikilink")
[Category:琉球族運動員](../Category/琉球族運動員.md "wikilink")