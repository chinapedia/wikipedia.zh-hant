[AK-74_RPK-74_DA-ST-89-06612.jpg](https://zh.wikipedia.org/wiki/File:AK-74_RPK-74_DA-ST-89-06612.jpg "fig:AK-74_RPK-74_DA-ST-89-06612.jpg")（上方）與RPK-74（下方）的對比圖\]\]

**RPK-74**（[俄文](../Page/俄语.md "wikilink")：）是[苏联在](../Page/苏联.md "wikilink")1974年為蘇軍裝備以[AK-74改進而成](../Page/AK-74突击步枪.md "wikilink")，用於替換[RPK](../Page/RPK輕機槍.md "wikilink")[輕機槍](../Page/輕機槍.md "wikilink")，由[卡拉什尼科夫設計](../Page/米哈伊尔·季莫费耶维奇·卡拉什尼科夫.md "wikilink")。

## 設計

RPK-74是[AK-74的一種變種槍](../Page/AK-74.md "wikilink")，它發射[5.45×39毫米M](../Page/5.45×39mm.md "wikilink")74子彈，配有長、重槍管、[兩腳架](../Page/兩腳架.md "wikilink")、改進型木製固定[槍托](../Page/槍托.md "wikilink")（有些是折叠枪托），通用[AK-74的](../Page/AK-74突击步枪.md "wikilink")30、45發彈匣。但由於槍管固定不能更換，RPK-74不能作長時間壓制射擊，實際上只屬於重槍管[自動步槍](../Page/自動步槍.md "wikilink")。

## 歷史

RPK-74是與AK-74一起研發並同時服役的，研製目的是為了滿足蘇軍的火力支援單位，以取代原有的[7.62×39mm](../Page/7.62×39mm.md "wikilink")
[RPK輕機槍](../Page/RPK輕機槍.md "wikilink")。

目前RPK-74仍然在[俄軍及多個前蘇聯國家的軍隊中服役](../Page/俄軍.md "wikilink")，多個[華沙條約亦曾授權生產RPK](../Page/華沙條約.md "wikilink")-74。

## 型號

  - **RPKS-74**—折叠枪托，裝備[傘兵部队](../Page/傘兵.md "wikilink")
  - **RPK-74N**—装有NSPU夜视瞄准镜
  - **RPK-74N2**—RPK-74N的塑料护木及枪托版本
  - **RPK-74M**—装有塑料护木及塑料折叠枪托的現代化版本
  - **[RPK-201](../Page/RPK-201輕機槍.md "wikilink")**—發射[5.56×45毫米北約子彈的出口版本](../Page/5.56×45mm_NATO.md "wikilink")
  - **[RPK-203](../Page/RPK-203輕機槍.md "wikilink")**—發射[7.62×39毫米中間型威力槍彈的出口版本](../Page/7.62×39mm.md "wikilink")

## 使用國

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
### 前使用國

  -
## 參考

  - [FN Minimi](../Page/FN_Minimi輕機槍.md "wikilink")
  - [M249 SAW](../Page/M249班用自動武器.md "wikilink")
  - [95式班用機槍](../Page/95式班用機槍.md "wikilink")
  - [班用機槍](../Page/轻机枪.md "wikilink")

## 外部連結

  - —[Modern
    Firearms-RPK-74](https://web.archive.org/web/20080913183205/http://world.guns.ru/machine/mg16-e.htm)

  - —[weaponplace.ru-RPK-74](http://www.weaponplace.ru/rpk.php)

  - —[D Boy Gun
    World-RPK-74](http://firearmsworld.net/russain/kalash/ak/rpk74.htm)

[Category:轻机枪](../Category/轻机枪.md "wikilink")
[Category:5.45×39毫米槍械](../Category/5.45×39毫米槍械.md "wikilink")
[Category:俄羅斯槍械](../Category/俄羅斯槍械.md "wikilink")
[Category:蘇聯槍械](../Category/蘇聯槍械.md "wikilink")
[Category:導氣式槍械](../Category/導氣式槍械.md "wikilink")