[新庄仔路.jpg](https://zh.wikipedia.org/wiki/File:新庄仔路.jpg "fig:新庄仔路.jpg")
**新庄仔路（Xinzhuangzi
Rd.）**為[高雄市](../Page/高雄市.md "wikilink")[左營區的東西向主要道路](../Page/左營區.md "wikilink")，本道路為早年[左營區至](../Page/左營區.md "wikilink")[三民區的重要道路](../Page/三民區.md "wikilink")，但因都市計畫及道路重劃等故，現今新庄仔路許多路段已經消失，呈斷斷續續之貌。起端於[蓮池潭西南側](../Page/蓮池潭.md "wikilink")[勝利路口](../Page/勝利路_\(高雄市\).md "wikilink")，末端為新庄仔路515巷口續行接[天祥二路](../Page/天祥路_\(高雄市\).md "wikilink")，[博愛三路口至](../Page/博愛路_\(高雄市\).md "wikilink")[左營火車站段另拓寬出](../Page/左營火車站.md "wikilink")**新莊一路**，與新庄仔路並存。

## 行經行政區域

（由西至東）

  - [左營區](../Page/左營區.md "wikilink")

## 分段

西起於[勝利路口](../Page/勝利路_\(高雄市\).md "wikilink")，東於[翠華路口接](../Page/翠華路_\(高雄市\).md "wikilink")[崇德路](../Page/崇德路_\(高雄市\).md "wikilink")。過崇德路、政德路後，新庄仔路延續[博愛三路口至新庄仔路](../Page/博愛路_\(高雄市\).md "wikilink")181巷。最後新庄仔路於博愛三路口接[新莊一路延續](../Page/新莊一路.md "wikilink")，東止於新庄仔路515巷口接[天祥二路](../Page/天祥路_\(高雄市\).md "wikilink")。

## 歷史

目前至真路300巷為舊新莊仔路111巷

## 沿線設施

（由西至東）

  - [蓮池潭](../Page/蓮池潭.md "wikilink")
  - 新庄仔星顯宮
  - [澳盛銀行北高雄分行](../Page/澳盛銀行.md "wikilink")
  - [全聯福利中心左營店](../Page/全聯福利中心.md "wikilink")
  - [高雄市左營區新莊國小](http://www.sjps.kh.edu.tw/)
  - 新庄仔青雲宮
  - [高雄市農會新庄辦事處](../Page/高雄市農會.md "wikilink")
  - [新吉莊北極殿](../Page/新吉莊北極殿.md "wikilink")
  - [板信商業銀行高新莊分行](../Page/板信商業銀行.md "wikilink")
  - [高雄市政府警察局左營分局新莊派出所](../Page/高雄市政府警察局.md "wikilink")
  - [高雄市公共腳踏車租賃系統](../Page/高雄市公共腳踏車租賃系統.md "wikilink")
      - 蓮池潭站：[左營區環潭路](../Page/左營區.md "wikilink")[蓮池潭風景區管理所後方入口處](../Page/蓮池潭.md "wikilink")

## 公車資訊

  - [漢程客運](../Page/漢程客運.md "wikilink")
      - 環狀168東 [金獅湖站](../Page/金獅湖站.md "wikilink")－金獅湖站
      - 環狀168西 金獅湖站－金獅湖站
  - [南台灣客運](../Page/南台灣客運.md "wikilink")
      - 91
        [金獅湖站](../Page/金獅湖站.md "wikilink")－[歷史博物館](../Page/歷史博物館.md "wikilink")
      - 紅36 先勝路口－
        捷運[巨蛋站](../Page/巨蛋站.md "wikilink")－[文藻外語學院](../Page/文藻外語學院.md "wikilink")
  - [港都客運](../Page/港都客運.md "wikilink")
      - 217 新昌幹線 [加昌站](../Page/加昌站_\(公車\).md "wikilink")－合群社區－鳥松區公所
  - [高雄客運](../Page/高雄客運.md "wikilink")
      - 24A 坔埔圓照寺－ 捷運[巨蛋站](../Page/巨蛋站.md "wikilink")
      - 24B 楠梓－大社－大灣－高雄
      - 8021 鳳山－彌陀

## 相關條目

  - [高雄市主要道路列表](../Page/高雄市主要道路列表.md "wikilink")

[Category:高雄市街道](../Category/高雄市街道.md "wikilink")