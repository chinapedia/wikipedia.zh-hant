[耿再成.jpg](https://zh.wikipedia.org/wiki/File:耿再成.jpg "fig:耿再成.jpg")
**耿再成**（），字**德甫**，[五河](../Page/五河县.md "wikilink")（今屬[安徽省](../Page/安徽省.md "wikilink")）人，[元末](../Page/元朝.md "wikilink")[紅巾軍人物](../Page/紅巾軍.md "wikilink")，[明朝開國功臣](../Page/明朝.md "wikilink")。

元末追隨[朱元璋](../Page/朱元璋.md "wikilink")。治军严厉，士兵於蔬菜瓜果无所取。元[至正十四年](../Page/至正.md "wikilink")（1354年）十月，丞相[脫脫以元軍十萬人合圍](../Page/脫脫.md "wikilink")[六合](../Page/六合縣.md "wikilink")（今[江蘇省](../Page/江蘇省.md "wikilink")[南京市](../Page/南京市.md "wikilink")[六合区](../Page/六合区.md "wikilink")），朱元璋以為六合是[滁州屏障](../Page/滁州_\(古代\).md "wikilink")，派遣耿再成等前往援救，對壘數戰，元軍引去\[1\]。

至正十九年（1359年），朱元璋命[枢密院判耿再成](../Page/枢密院判.md "wikilink")、参军[胡大海率兵取](../Page/胡大海.md "wikilink")[处州](../Page/处州.md "wikilink")。至元二十二年（1362年）二月，处州[苗帅](../Page/苗帅.md "wikilink")[李佑](../Page/李佑.md "wikilink")、[賀仁德等叛變](../Page/賀仁德.md "wikilink")，再成當時正在吃飯，闻变上马迎战，兵卒未滿二十人，再成揮劍連斷數槊，受伤落马，不屈而死。[追封高阳郡公](../Page/追封.md "wikilink")，配享太廟。[洪武十年](../Page/洪武.md "wikilink")（1377年），加赠**泗国公**，谥**武壮**\[2\]。

## 参考文献

<div class="references-small">

<references>

</references>

</div>

[Category:红巾军人物](../Category/红巾军人物.md "wikilink")
[Category:元朝被杀害人物](../Category/元朝被杀害人物.md "wikilink")
[Category:淮西二十四将](../Category/淮西二十四将.md "wikilink")
[Category:明朝追赠国公](../Category/明朝追赠国公.md "wikilink")
[Z](../Category/耿姓.md "wikilink")
[Category:諡武壯](../Category/諡武壯.md "wikilink")

1.  《[明史](../Page/明史.md "wikilink")》（卷133）：“耿再成，字德甫，五河人。從太祖於濠，克泗、滁州。元兵圍六合，太祖救之，與再成軍瓦梁壘。力戰，度不敵，引還。元兵尾至，太祖設伏澗側，令再成誘敵，大敗之。以鎮撫從渡江，下集慶。以元帥守鎮江，以行樞密院判官守長興，再守揚州。從取金華，為前鋒，屯縉雲之黃龍山以遏敵沖。與胡大海破石抹宜孫於處州，克其城，守之。宜孫來攻，又敗之慶元。”
2.  《[明史](../Page/明史.md "wikilink")》（卷133）：“再成持軍嚴，士卒出入民間，蔬果無所捐。金華苗帥蔣英等叛，殺胡大海。處州苗帥李祐之等聞之，亦作亂。再成方對客飯，聞變，上馬，收戰卒不滿二十人，迎賊罵曰：「賊奴！國家何負汝，乃反。」賊攢槊刺再成。再成揮劍連斷數槊，中傷墜馬，大罵不絕口死。胡深等收其屍，藁葬之。後改葬金陵[聚寶山](../Page/聚寶山.md "wikilink")。追封高陽郡公，侑享太廟，肖像功臣廟。洪武十年加贈泗國公，諡武壯。”