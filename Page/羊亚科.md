**羊亚科**（[学名](../Page/学名.md "wikilink")：），是[牛科下的一个亚科](../Page/牛科.md "wikilink")。因为这个亚科内的动物之间的亲属关系还不很清楚，为了显示这个不清楚性，在生物分类学上亚科以下又分族。

高鼻羚羊族和斑羚屬俗称羚羊。

## 主要特征

除了[湖羊两性都无角外](../Page/湖羊.md "wikilink")，两性都有角。雄性角大于雌性角。角多扁平，断面略成三角形，表面有许多环行横棱隆起。

## 分布

除[澳洲无自然分布外遍布世界各地](../Page/澳洲.md "wikilink")。

## 主要食物

[草本植物](../Page/草本植物.md "wikilink")、[灌木的叶子](../Page/灌木.md "wikilink")。

## 主要天敌

[虎](../Page/虎.md "wikilink")、[豹](../Page/豹.md "wikilink")、[狮](../Page/狮.md "wikilink")、[狼](../Page/狼.md "wikilink")、[猞猁等](../Page/猞猁.md "wikilink")。

## 保护现状

[山羊](../Page/山羊.md "wikilink")、[绵羊安全](../Page/绵羊.md "wikilink")，其余均为一、二级保护动物。

## 分类

  - **羊亚科 *Caprinae***
      - [羊羚族](../Page/羊羚族.md "wikilink") Rupicaprini
          - [臆羚属](../Page/臆羚属.md "wikilink") *Rupicapra*
          - [斑羚属](../Page/斑羚属.md "wikilink") *Nemorhaedus*
          - [雪羊属](../Page/雪羊属.md "wikilink") *Oreamnos*
      - [羊牛族](../Page/羊牛族.md "wikilink") Ovibovini
          - [麝牛属](../Page/麝牛属.md "wikilink") *Ovibos*
          - [羚牛属](../Page/羚牛属.md "wikilink") *Budorcas*
      - [羊族](../Page/羊族.md "wikilink") Caprini
          - [羊属](../Page/羊属.md "wikilink") *Ovis*
          - [蛮羊属](../Page/蛮羊属.md "wikilink") *Ammotragus*
          - [岩羊属](../Page/岩羊属.md "wikilink") *Pseudois*
          - [山羊属](../Page/山羊属.md "wikilink") *Capra*
          - [半羊属](../Page/半羊属.md "wikilink")(塔尔羊属) *Hemitragus*

## 外部链接

  - [羊属](../Page/羊属.md "wikilink")

{{-}}

[\*](../Category/羊亚科.md "wikilink")