[Innocent9.jpg](https://zh.wikipedia.org/wiki/File:Innocent9.jpg "fig:Innocent9.jpg")
[教宗](../Page/教宗.md "wikilink")**諾森九世**（，，原名*' Gian Antonio Facchinetti
de
Nuce*'，於1591年10月29日—1591年12月30日岀任教宗），[意大利人](../Page/意大利.md "wikilink")。

## 譯名列表

  - 依諾增爵九世：[天主教香港教區禮儀委員會：禧年專頁](http://catholic-dlc.org.hk/2000.htm)作依諾增爵。
  - 因諾森特九世：《[世界人名翻譯大辭典](../Page/世界人名翻譯大辭典.md "wikilink")》1993年版作因諾森特。
  - 英諾森九世：[《大英簡明百科知識庫》2005年版](http://wordpedia.britannica.com/concise/content.aspx?id=4166&hash=S4qRyNX7zlhlyoEjrdWypA%3D%3D&t=3)作英諾森。
  - 諾森九世：[香港天主教教區檔案　歷任教宗](https://web.archive.org/web/20051023073732/http://archives.catholic.org.hk/popes/index.htm)作諾森。
  - 意諾增爵九世：[天主教天津教区](https://web.archive.org/web/20070311132024/http://www.catholic.tj.cn/p/Article/Jhls/Lsjs/200507/7285.html)作意諾增爵。

{{-}}

[Category:16世纪意大利人](../Category/16世纪意大利人.md "wikilink")
[Category:義大利出生的教宗](../Category/義大利出生的教宗.md "wikilink")
[Category:博洛尼亞大學校友](../Category/博洛尼亞大學校友.md "wikilink")