[Flying_rainbow_flag_at_Taiwan_Pride_20041106.jpg](https://zh.wikipedia.org/wiki/File:Flying_rainbow_flag_at_Taiwan_Pride_20041106.jpg "fig:Flying_rainbow_flag_at_Taiwan_Pride_20041106.jpg")\]\]
[Rainbow_Flag_at_Southorn_Playground_6030.JPG](https://zh.wikipedia.org/wiki/File:Rainbow_Flag_at_Southorn_Playground_6030.JPG "fig:Rainbow_Flag_at_Southorn_Playground_6030.JPG")巴士站之上\]\]
**彩虹旗**是採用[彩虹的主要色調組成的多色](../Page/彩虹.md "wikilink")[旗幟](../Page/旗幟.md "wikilink")。當今有很多种彩虹旗正在被使用，他们之间可能完全沒有關聯。其中目前最著名的可能是用以代表“[同志](../Page/LGBT.md "wikilink")”的[六色彩虹旗](../Page/彩虹旗_\(LGBT\).md "wikilink")。在[意大利](../Page/意大利.md "wikilink")，彩虹旗則普遍用作[和平的標誌](../Page/和平.md "wikilink")。

## 历史

[宗教改革時期](../Page/宗教改革.md "wikilink")，彩紅旗被當作[德國農民戰爭的標幟](../Page/德國農民戰爭.md "wikilink")，其宗教典故來自創世紀中，上帝向諾亞立下的彩虹之約，象徵新天新地的到來。農民戰爭領導人托馬斯·閔采爾的形象經常和彩紅旗同置。

现为[秘鲁](../Page/秘鲁.md "wikilink")[库斯科市的市旗使用七色条纹的彩虹设计](../Page/库斯科.md "wikilink")，这面旗帜曾被认为是[前哥倫布時期](../Page/前哥倫布時期.md "wikilink")[印加帝国](../Page/印加.md "wikilink")（Pre-Columbian
Inca empire）的Tawantinsuyu旗帜，但后来发现这是错误的。

## LGBT

在1970年代，美國曾有人把彩虹旗用以象徵[國際主義以及全人類的聯合](../Page/國際主義.md "wikilink")。但是到了1970年代末，彩虹旗與[LGBT族群之間的聯係開始在美國及西方世界流行](../Page/LGBT.md "wikilink")，現時已成為代表支持LGBT族群的象徵。

## 彩虹旗所可能代表的事物

<File:PACE> flag (without text).svg|
[File:Gay_flag.svg|象征](File:Gay_flag.svg%7C象征)[LGBT社群的](../Page/LGBT.md "wikilink")[彩虹旗](../Page/彩虹旗_\(LGBT\).md "wikilink")（1979年之六色版本）
<File:ICA> flag.svg|[國際合作社聯盟旗幟](../Page/國際合作社聯盟.md "wikilink")
<File:Flag_of_Buddhism.svg>|[国际佛教旗](../Page/国际佛教旗.md "wikilink")
<File:Flag> of
Cusco.svg|[秘鲁](../Page/秘鲁.md "wikilink")[库斯科市市旗](../Page/库斯科市.md "wikilink")
<File:Flag> of the Jewish Autonomous
Oblast.svg|[俄罗斯联邦](../Page/俄罗斯联邦.md "wikilink")[犹太自治州州旗](../Page/犹太自治州.md "wikilink")（1996年制定）

## 参考文献

## 参见

  - [彩虹](../Page/彩虹.md "wikilink")、[光谱](../Page/光谱.md "wikilink")
  - [三色旗](../Page/三色旗.md "wikilink")、[五色旗](../Page/五色旗.md "wikilink")
  - [旗帜学](../Page/旗帜学.md "wikilink")

[Category:旗帜类型](../Category/旗帜类型.md "wikilink")
[和平符号](../Category/和平符号.md "wikilink")