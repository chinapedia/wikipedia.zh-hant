[AkishinoNoMiyaGoFusaiM1089.jpg](https://zh.wikipedia.org/wiki/File:AkishinoNoMiyaGoFusaiM1089.jpg "fig:AkishinoNoMiyaGoFusaiM1089.jpg")
**文仁親王妃紀子**（，），原名**川嶋纪子**（），[秋篠宮文仁親王之妻](../Page/秋篠宮文仁親王.md "wikilink")，即[今上天皇](../Page/今上天皇.md "wikilink")[明仁的二儿媳](../Page/明仁.md "wikilink")，使用的徽印是[檜扇菖蒲](../Page/檜扇菖蒲.md "wikilink")。现任[结核预防会总裁](../Page/结核.md "wikilink")、[日本](../Page/日本.md "wikilink")[红十字社名誉副总裁](../Page/红十字社.md "wikilink")。

## 早年生活

紀子是[學習院大學](../Page/學習院大學.md "wikilink")[經濟學](../Page/經濟學.md "wikilink")[教授](../Page/教授.md "wikilink")[川嶋辰彥與妻子川嶋和代的長女](../Page/川嶋辰彥.md "wikilink")，於[日本](../Page/日本.md "wikilink")[靜岡市](../Page/靜岡市.md "wikilink")[駿河區之賞賜財團濟生會的](../Page/駿河區.md "wikilink")出生。在完成[東京大學經濟學部碩士課程後](../Page/東京大學.md "wikilink")，前往[美國](../Page/美國.md "wikilink")[賓夕法尼亞大學留學](../Page/賓夕法尼亞大學.md "wikilink")，所以紀子出生後到6歲前是在美國度過的。6歲返國後，紀子入讀靜岡市市立小學，隨後轉到[東京的區立目白小學就讀](../Page/東京.md "wikilink")，小學4年級時編入學習院初等科。在她小學5年級時，其父獲邀前往[奧地利進行研究](../Page/奧地利.md "wikilink")，所以她再次隨父母出國，在當地居住了2年，並在[維也納就讀初中](../Page/維也納.md "wikilink")。紀子少年時出國的經歷，使她能說流利的[英語和](../Page/英語.md "wikilink")[德語](../Page/德語.md "wikilink")。

1979年9月再次歸國後，紀子入讀學習院女子中等科，1985年入讀學習院大學文學部心理學科，1989年畢業。

## 婚姻生活

[Kikosamagoseikon.jpg](https://zh.wikipedia.org/wiki/File:Kikosamagoseikon.jpg "fig:Kikosamagoseikon.jpg")
紀子在學習院大學認識比她高一屆的學長秋篠宮文仁親王（當時稱為禮宮文仁親王），文仁親王於1986年6月26日仍是[本科生時已首次向紀子求婚](../Page/本科生.md "wikilink")。然而他們3年後才向外界透露結婚的意向，並於1989年9月12日獲[皇室會議批准訂婚](../Page/皇室會議.md "wikilink")。當時紀子仍居住在父親的大學教職員宿舍，於是被日本傳媒戲稱為「3LDK\[1\]王妃」。

他們的婚禮於1990年（[平成二年](../Page/平成.md "wikilink")）6月29日舉行。文仁親王獲[宮內廳批准創設新](../Page/宮內廳.md "wikilink")[宮家](../Page/宮家.md "wikilink")「[秋篠宮](../Page/秋篠宮.md "wikilink")」，亦於婚禮當日獲天皇明仁授予**秋篠宮文仁親王**稱號，而紀子則成為**文仁親王妃紀子**，俗稱**紀子妃**。

文仁親王與紀子的結合打破日本皇室數個傳統。首先，文仁親王訂婚時仍未正式大學畢業，而且早於其兄長[皇太子德仁親王結婚](../Page/皇太子德仁.md "wikilink")。其次，紀子是第一位嫁入日本皇室的[中產階級女性](../Page/中產階級.md "wikilink")。雖然天皇明仁的皇后[美智子也是平民出身](../Page/美智子.md "wikilink")，但她畢竟生於富裕家庭，是[日清製粉公司總裁之女](../Page/日清製粉公司.md "wikilink")。最後，文仁親王與紀子的愛情故事被傳媒廣泛報導，亦屬日本皇室首次。

婚後紀子除了盡力履行皇室職責外，亦繼續在學習院大學研究院人文科學研究科就讀，修讀[心理學](../Page/心理學.md "wikilink")[博士前期課程](../Page/博士.md "wikilink")，1995年修業（[碩士畢業](../Page/碩士.md "wikilink")）。她亦以熱心於聾人福利事業稱著，而且是位富有經驗的[手語](../Page/手語.md "wikilink")[傳譯員](../Page/傳譯員.md "wikilink")。

## 子女

文仁親王與紀子夫婦自1997年起居於[東京都](../Page/東京都.md "wikilink")[港區](../Page/港區.md "wikilink")[赤阪的寓所](../Page/赤阪.md "wikilink")，育有兩女一子。

  - 長女[眞子内親王在](../Page/眞子内親王.md "wikilink")1991年（平成三年）10月23日生於[東京都](../Page/東京都.md "wikilink")[千代田區](../Page/千代田區.md "wikilink")。
  - 次女[佳子内親王在](../Page/佳子内親王.md "wikilink")1994年（平成六年）12月29日生於[東京都](../Page/東京都.md "wikilink")[千代田區](../Page/千代田區.md "wikilink")。
  - 長子[悠仁親王在](../Page/悠仁親王.md "wikilink")2006年（平成十八年）9月6日生於[東京都](../Page/東京都.md "wikilink")[港區](../Page/港區_\(東京都\).md "wikilink")[愛育醫院](../Page/愛育醫院.md "wikilink")。
      - 日本時間（UTC+9）8時27分剖腹產下，出生時重2.558公斤。是日本皇室自1965年來首名出生的男性後嗣，亦繼伯父[德仁親王](../Page/德仁親王.md "wikilink")、父親[文仁親王後](../Page/秋筱宫文仁亲王.md "wikilink")，成為日本天皇的第三順位繼承人。

## 榮譽

  - 1990年6月29日受勳一等[寶冠章](../Page/寶冠章.md "wikilink")

## 注釋

<div style="font-size: 85%">

<references />

</div>

## 參考資料

1.  *宮內廳，Personal Histories of Their Imperial Highnesses Prince and
    Princess
    Akishino，<https://web.archive.org/web/20061010230225/http://www.kunaicho.go.jp/e02/ed02-06.html>，2006年9月6日更新。*

## 外部連結

  - [宮內廳有關秋篠宮文仁親王與紀子夫婦的介紹](https://web.archive.org/web/20061010223230/http://www.kunaicho.go.jp/e03/ed03-04.html)
  - [菊花皇朝釋重負
    紀子誕男嬰　四十一年首位繼承人](http://hk.news.yahoo.com/fc/news_fc_injapanheir.html)
  - [日本皇室时隔41年诞生首名男性成员](http://news.sina.com.cn/w/2006-09-06/08339947911s.shtml)

[Category:日本親王妃與王妃](../Category/日本親王妃與王妃.md "wikilink")
[Category:秋篠宮](../Category/秋篠宮.md "wikilink")
[Category:學習院大學校友](../Category/學習院大學校友.md "wikilink")
[Category:1966年出生](../Category/1966年出生.md "wikilink")
[Category:靜岡市出身人物](../Category/靜岡市出身人物.md "wikilink")
[Category:御茶水女子大學校友](../Category/御茶水女子大學校友.md "wikilink")

1.  3LDK：日本的“三室一厅加餐厅、厨房的单元”的简称。