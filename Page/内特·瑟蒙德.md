[Nate_Thurmond_1969.jpeg](https://zh.wikipedia.org/wiki/File:Nate_Thurmond_1969.jpeg "fig:Nate_Thurmond_1969.jpeg")
**纳撒尼尔·瑟蒙德**（，），昵称**內特·瑟蒙德**（），綽號**内特大帝**（**Nate the
Great**），[美国退役](../Page/美国.md "wikilink")[篮球运动员](../Page/篮球.md "wikilink")，场上位置[中锋](../Page/中锋.md "wikilink")。

1977年，宣布退役，1985年进入[篮球名人堂](../Page/篮球名人堂.md "wikilink")。是NBA正式記錄中，第一位取得[四雙的的球員](../Page/四雙.md "wikilink")，至今仍保持着NBA单节18个篮板球的纪录。被列入[NBA50大巨星之一](../Page/NBA50大巨星.md "wikilink")。

## 生平

瑟蒙德在[1963年NBA选秀中于第一轮第三顺位被](../Page/1963年NBA选秀.md "wikilink")[旧金山勇士队选中](../Page/金州勇士队.md "wikilink")。由于队内有另一位巨星[威尔特·张伯伦](../Page/威尔特·张伯伦.md "wikilink")（Wilt
Chamberlain），瑟蒙德多数时间作为[大前锋](../Page/大前锋.md "wikilink")，或者是张伯伦的替补上场。不久，张伯伦被交换去[费城76人队](../Page/费城76人队.md "wikilink")，瑟蒙德于是成为队内首发中锋。1974年，瑟蒙德被交换去[芝加哥公牛队](../Page/芝加哥公牛队.md "wikilink")，当年10月18日，瑟蒙德在面对[亚特兰大鹰队时拿下了](../Page/亚特兰大鹰队.md "wikilink")22分、14[篮板球](../Page/篮板球.md "wikilink")、13[助攻和](../Page/助攻.md "wikilink")12[盖帽](../Page/盖帽.md "wikilink")，成为NBA中第一位在正式统计中取得[四双的球员](../Page/四双.md "wikilink")。次年，瑟蒙德又被交换去[克里夫兰骑士队](../Page/克里夫兰骑士队.md "wikilink")，在骑士队，35岁的瑟蒙德一举率队打入东部联盟决赛，只是输给了强大的[波士顿凯尔特人队](../Page/波士顿凯尔特人队.md "wikilink")，人称“里奇菲尔德奇迹”。

2016年7月16日，瑟蒙德因[血癌病逝於](../Page/血癌.md "wikilink")[舊金山](../Page/舊金山.md "wikilink")，享年74歲。\[1\]

## 延伸阅读

  -
## 参考文献

## 外部链接

  - [名人堂资料](https://web.archive.org/web/20080919211653/http://www.hoophall.com/halloffamers/bhof-nate-thurmond.html)
  - [Hoopedia
    bio](https://web.archive.org/web/20080516203111/http://hoopedia.nba.com/index.php/Nate_Thurmond)
  - [NBA档案](http://www.nba.com/history/players/thurmond_summary.html)

[Category:美国男子篮球运动员](../Category/美国男子篮球运动员.md "wikilink")
[Category:金州勇士队球员](../Category/金州勇士队球员.md "wikilink")
[Category:芝加哥公牛队球员](../Category/芝加哥公牛队球员.md "wikilink")
[Category:克里夫兰骑士队球员](../Category/克里夫兰骑士队球员.md "wikilink")
[Category:NBA退休背號球员](../Category/NBA退休背號球员.md "wikilink")
[Category:篮球名人堂成员](../Category/篮球名人堂成员.md "wikilink")

1.  [NBA》勇士傳奇中鋒瑟蒙德辭世
    享年74歲](http://www.chinatimes.com/realtimenews/20160717000732-260403)