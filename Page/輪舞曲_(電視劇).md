{{ Otheruses|subject=一套韓日合拍劇集|other=一種樂曲形式|回旋曲 }}

《**輪舞曲**》（*Rondo*）是[日本](../Page/日本.md "wikilink")[TBS電視台](../Page/TBS電視台.md "wikilink")50週年台慶大戲，也是日本、[韓國恢復](../Page/韓國.md "wikilink")[外交](../Page/外交.md "wikilink")40週年大戲，是TBS電視台與[韓國](../Page/韓國.md "wikilink")[藝堂娛樂](../Page/藝堂娛樂.md "wikilink")（Yedang
Entertainment
Company）共同製作的[日劇](../Page/日劇.md "wikilink")，[竹野內豐](../Page/竹野內豐.md "wikilink")、[崔智友主演](../Page/崔智友.md "wikilink")。首集一推出便獲得20﹪的高[收視率](../Page/收視率.md "wikilink")。

TBS電視台播出時段為《[周日劇場](../Page/周日劇場.md "wikilink")》時段內。臺灣[緯來日本台於](../Page/緯來日本台.md "wikilink")2008年3月14日20點播出。香港[無綫電視購下播映權](../Page/電視廣播有限公司.md "wikilink")，分別安排於2006年5月13日及2007年4月30日起在[無綫劇集台及](../Page/無綫劇集台.md "wikilink")[明珠台播出](../Page/明珠台.md "wikilink")。\[1\]

## 故事大綱

一場命運與過去交錯的動人故事，跨越日韓兩國的終極[愛情](../Page/愛情.md "wikilink")，在《輪舞曲》中緩緩揭開序幕。父親在韓國遭殺害的日本[刑警竹野內豐](../Page/刑警.md "wikilink")，為了追查塵封已久的真相，改名換姓潛入[黑幫](../Page/黑幫.md "wikilink")「神狗」[臥底](../Page/臥底.md "wikilink")。另一方面，為了尋找失蹤的父親，崔智友帶著妹妹從韓國來到日本，經營一間小小的[韓國料理店](../Page/韓國料理.md "wikilink")。他們各自背負著不可告人的秘密，卻在[東京的角落相遇](../Page/東京.md "wikilink")、相戀，捲入悲傷的宿命輪舞……

## 演員

  - [竹野內豐飾金山琢己](../Page/竹野內豐.md "wikilink")（假名西鳩翔）（香港配音：[陳欣](../Page/陳欣_\(配音員\).md "wikilink")）
  - [崔智友飾崔允兒](../Page/崔智友.md "wikilink")（或崔由奈）/ 佐倉愛
    \[2\]（香港配音：[潘寧](../Page/潘寧.md "wikilink")）
  - [橋爪功飾宋圭煥](../Page/橋爪功.md "wikilink")（香港配音：[盧國權](../Page/盧國權.md "wikilink")）
  - [速水直道飾風間龍吾](../Page/速水直道.md "wikilink")（香港配音：[黃啟昌](../Page/黃啟昌.md "wikilink")）
  - [李貞賢飾崔允曦](../Page/李貞賢.md "wikilink")（或崔由理）/ 佐倉唯
    \[3\]（香港配音：[周筠](../Page/周筠.md "wikilink")）
  - [杉浦直樹飾風間龍一郎](../Page/杉浦直樹.md "wikilink")（香港配音：[陳曙光](../Page/陳曙光.md "wikilink")）
  - [木村佳乃飾一之瀨晶](../Page/木村佳乃.md "wikilink")（香港配音：[陸惠玲](../Page/陸惠玲.md "wikilink")）
  - [石橋凌飾伊崎真彥](../Page/石橋凌.md "wikilink")（香港配音：[林保全](../Page/林保全.md "wikilink")）
  - [佐藤龍太飾阿英](../Page/佐藤龍太.md "wikilink")（香港配音：[陳振安](../Page/陳振安.md "wikilink")）
  - [市川由衣飾風間琴美](../Page/市川由衣.md "wikilink")（香港配音：[程文意](../Page/程文意.md "wikilink")）

## 收視率

| 集數          | 播出時間       | 副題                  | 標題 | 收視率                         |
| ----------- | ---------- | ------------------- | -- | --------------------------- |
|             |            |                     |    |                             |
| 1           | 2006年1月15日 | めぐりあい・・・愛と哀しみと宿命の二人 |    | <font color="red">**20.0%** |
| 2           | 2006年1月22日 | 心の言葉                |    | 15.2%                       |
| 3           | 2006年1月29日 | 哀しい約束～はじめての涙・・・     |    | 15.1%                       |
| 4           | 2006年2月5日  | つかの間のデート～揺れる思い      |    | 15.8%                       |
| 5           | 2006年2月12日 | 母の愛                 |    |                             |
| 6           | 2006年2月19日 | キスの朝                |    | 14.6%                       |
| 7           | 2006年2月26日 | 新章突入                |    | 15.5%                       |
| 8           | 2006年3月5日  | 愛するが故の悲劇            |    | <font color=blue>**13.2%**  |
| 9           | 2006年3月12日 | 運命の地、ソウルへ           |    | 14.1%                       |
| 10          | 2006年3月19日 | 償うべき罪守るべき愛          |    | 14.6%                       |
| 最終話         | 2006年3月26日 | さよなら愛しき人            |    | 17.7%                       |
| 平均收視率 15.5% |            |                     |    |                             |

## 雜項

  - 第1集中，[藥局取景地點是](../Page/藥局.md "wikilink")[京濱急行電鐵](../Page/京濱急行電鐵.md "wikilink")[新馬場車站前](../Page/新馬場車站.md "wikilink")[商店街的](../Page/商店街.md "wikilink")「多摩藥局」（トモ薬局）前。
  - 第2集中，崔允兒（或崔由奈）在[橋上讀](../Page/橋.md "wikilink")[信](../Page/書信.md "wikilink")，取景地點是京濱急行電鐵新馬場車站與[目黑川附近的](../Page/目黑川.md "wikilink")[城南信用金庫品川支店附近](../Page/城南信用金庫.md "wikilink")。
  - 第9集中，崔允兒（或崔由奈）在韓國經過的[高級中學是](../Page/高級中學.md "wikilink")[韓國中央高等學校](../Page/韓國中央高等學校.md "wikilink")。[韓劇](../Page/韓劇.md "wikilink")《[冬季戀歌](../Page/冬季戀歌.md "wikilink")》中，鄭有珍（或鄭惟珍）經過的高級中學也是該校。

## 注釋

<div class="references-small">

<references />

</div>

## 外部連結

  - [緯來日本台《輪舞曲》官方網站](http://japan.videoland.com.tw/channel/rondo/)

## 節目的變遷

[Category:無綫電視外購劇集](../Category/無綫電視外購劇集.md "wikilink")
[Category:周日劇場](../Category/周日劇場.md "wikilink")
[Category:日語電視劇](../Category/日語電視劇.md "wikilink")
[Category:韓語電視劇](../Category/韓語電視劇.md "wikilink")
[Category:2006年日本電視劇集](../Category/2006年日本電視劇集.md "wikilink")
[Category:緯來電視外購日劇](../Category/緯來電視外購日劇.md "wikilink")
[Category:日韓合拍劇](../Category/日韓合拍劇.md "wikilink")
[Category:異族戀題材電視劇](../Category/異族戀題材電視劇.md "wikilink")
[Category:日本懸疑劇](../Category/日本懸疑劇.md "wikilink")
[Category:TBS警匪電視劇](../Category/TBS警匪電視劇.md "wikilink")
[Category:亞裔日本人題材作品](../Category/亞裔日本人題材作品.md "wikilink")

1.  [雙語廣播](../Page/麗音廣播.md "wikilink")（日語原版及無綫自行製作的粵語配音版本）。
2.  [日曜劇場『輪舞曲』 － ロンド － ｜
    SPECIAL](https://web.archive.org/web/20060412000106/http://www.tbs.co.jp/rondo2006/special/special11_5.html)
3.  [日曜劇場『輪舞曲』 － ロンド － ｜
    SPECIAL](https://web.archive.org/web/20060412000106/http://www.tbs.co.jp/rondo2006/special/special11_5.html)