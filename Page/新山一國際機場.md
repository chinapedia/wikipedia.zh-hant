[Tsn-sign-1967.jpg](https://zh.wikipedia.org/wiki/File:Tsn-sign-1967.jpg "fig:Tsn-sign-1967.jpg")與[南越空軍總部標誌](../Page/越南共和國空軍.md "wikilink")\]\]
[Tan_Son_Nhat_International_Airport_Level_3_Concourse.jpg](https://zh.wikipedia.org/wiki/File:Tan_Son_Nhat_International_Airport_Level_3_Concourse.jpg "fig:Tan_Son_Nhat_International_Airport_Level_3_Concourse.jpg")
[Tan_Son_Nhat_International_Airport_Level_3.jpg](https://zh.wikipedia.org/wiki/File:Tan_Son_Nhat_International_Airport_Level_3.jpg "fig:Tan_Son_Nhat_International_Airport_Level_3.jpg")
[Tan_Son_Nhat_International_Airport_Level_4.jpg](https://zh.wikipedia.org/wiki/File:Tan_Son_Nhat_International_Airport_Level_4.jpg "fig:Tan_Son_Nhat_International_Airport_Level_4.jpg")
[ILLY_COFFEE_LATTE_IN_VVTS_AIRPORT.jpg](https://zh.wikipedia.org/wiki/File:ILLY_COFFEE_LATTE_IN_VVTS_AIRPORT.jpg "fig:ILLY_COFFEE_LATTE_IN_VVTS_AIRPORT.jpg")
[Vietnam_Airlines_A321_at_Tan_Son_Nhat_airport.jpg](https://zh.wikipedia.org/wiki/File:Vietnam_Airlines_A321_at_Tan_Son_Nhat_airport.jpg "fig:Vietnam_Airlines_A321_at_Tan_Son_Nhat_airport.jpg")\]\]
[Vietnam_Airlines_A330-200_at_Tan_Son_Nhat_airport.jpg](https://zh.wikipedia.org/wiki/File:Vietnam_Airlines_A330-200_at_Tan_Son_Nhat_airport.jpg "fig:Vietnam_Airlines_A330-200_at_Tan_Son_Nhat_airport.jpg")\]\]
[B-18708_(8233398945).jpg](https://zh.wikipedia.org/wiki/File:B-18708_\(8233398945\).jpg "fig:B-18708_(8233398945).jpg")滑行於新山一國際機場\]\]

**新山一國際機場**（；）是一個位於[越南](../Page/越南.md "wikilink")[胡志明市的機場](../Page/胡志明市.md "wikilink")，為目前越南境內最大的機場，但2020年將被[隆城國際機場取代](../Page/隆城國際機場.md "wikilink")。該機場建於[法屬印度支那時期](../Page/法屬印度支那.md "wikilink")，在[越南戰爭時期曾被](../Page/越南戰爭.md "wikilink")[駐越美軍改建為](../Page/駐越美軍.md "wikilink")[新山一空軍基地](../Page/新山一空軍基地.md "wikilink")。该机场设计年旅客吞吐量为2500万人次。

## 歷史

  - 1930年，法屬印度支那政府在新山村興建[機場](../Page/機場.md "wikilink")。
  - 1956年，駐越美軍授助南越政府改建空軍基地。
  - 1975年4月，[北越軍隊砲擊機場](../Page/北越.md "wikilink")、[美國海軍陸戰隊](../Page/美國海軍陸戰隊.md "wikilink")、美僑及越南[難民從機場撤離](../Page/難民.md "wikilink")。
  - 2004年12月9日，越南戰爭結束，直航[美國的航班正式首航](../Page/美國.md "wikilink")。

## 地面交通

由於機場距市中心僅7公里，故交通十分方便，設公巴站及計程車可选择，未来將設有[地鐵](../Page/胡志明市地鐵.md "wikilink")。

  - 巴士：109、152線途經，10,000越南盾
  - 酒店接送：大多高級酒店提供接送服務，要事先預約
  - 地鐵：2015年興建，預計2020年通車

## 航空公司與目的地

（下列航空公司之排列以該公司英語字首由A至Z排列）

### 1號航站樓（國內航線）

### 2號航站樓（國際航線）

## 相关条目

  - [胡志明市](../Page/胡志明市.md "wikilink")
  - [越南航空](../Page/越南航空.md "wikilink")
  - [越捷航空](../Page/越捷航空.md "wikilink")
  - [捷星太平洋航空](../Page/捷星太平洋航空.md "wikilink")

## 外部連結

  -
[Category:胡志明市建築物](../Category/胡志明市建築物.md "wikilink")
[Category:胡志明市交通](../Category/胡志明市交通.md "wikilink")
[Category:越南機場](../Category/越南機場.md "wikilink")