## 大事記

  - [1月1日](../Page/1月1日.md "wikilink")
    [佐敦道一間音樂廳四級火](../Page/佐敦道.md "wikilink")，4人喪生。
  - [1月2日](../Page/1月2日.md "wikilink")
    [鯉魚門附近發生撞船事故](../Page/鯉魚門.md "wikilink")，1死6獲救。
  - [1月5日](../Page/1月5日.md "wikilink") 九龍證劵交易所開業
  - [1月9日](../Page/1月9日.md "wikilink")
    由[伊利沙伯皇后號改建中的海上學府於青衣島附近海面發生](../Page/伊利沙伯皇后號.md "wikilink")[災難級大火](../Page/海上學府災情警報大火.md "wikilink")，14人受傷，焚燒102小時後熄滅。
  - [2月7日](../Page/2月7日.md "wikilink")
    [謝斐道一間音樂廳三級火](../Page/謝斐道.md "wikilink")，3死4傷。
  - [3月15日](../Page/3月15日.md "wikilink")
    [長沙灣](../Page/長沙灣.md "wikilink")[廣隆泰工業大廈發生](../Page/廣隆泰工業大廈.md "wikilink")[五級大火](../Page/廣隆泰工業大廈五級火.md "wikilink")，5人受傷。
  - [3月29日](../Page/3月29日.md "wikilink")
    行走旺角至觀塘線的七十輛小巴，因不滿其中九輛被警察抄牌而全線罷駛抗議。
  - [4月2日](../Page/4月2日.md "wikilink")
    13名[聾啞及](../Page/聾啞.md "wikilink")1名健全青年行劫由[荔枝角開往](../Page/荔枝角.md "wikilink")[尖沙咀的](../Page/尖沙咀.md "wikilink")[九巴](../Page/九巴.md "wikilink")6號綫，劫去3名乘客現金後被警方拘捕。
  - [4月12日](../Page/4月12日.md "wikilink") 耗資1650萬元興建的中文大學新科學館正式啟用。
  - [4月23日](../Page/4月23日.md "wikilink") 大角咀渡輪碼頭啟用。
  - [5月11日](../Page/5月11日.md "wikilink")
    [山道木樓四級火](../Page/山道木樓四級火.md "wikilink")，12死16傷。\[1\]\[2\]
  - [6月4日](../Page/6月4日.md "wikilink")
    光榮號油輪在[青衣以南海域爆炸](../Page/青衣.md "wikilink")，2死16傷。
  - [6月18日](../Page/6月18日.md "wikilink")
    [六一八雨災](../Page/六一八雨災.md "wikilink")\[3\]
      - 半山區寶珊道大量山泥傾瀉，將樓高十二層的旭和大廈撞塌，67死19傷。
      - 暴雨將觀塘雞寮安置區上的山泥冲瀉，壓毀78間房屋，造成71死52傷。
  - [6月23日](../Page/6月23日.md "wikilink") 港府宣佈港幣隨英鎊在國際外匯市場上自由浮動。
  - [6月24日](../Page/6月24日.md "wikilink") 香港金融市場局面混亂，銀行停掛匯價，九龍證劵交易所暫停營業。
  - [7月6日](../Page/7月6日.md "wikilink") 港府宣佈港幣與美元訂立固定匯率，每5.65港元兌1美元。
  - [7月11日](../Page/7月11日.md "wikilink")
    港府撥款150萬，動員105000人進行[清潔香港運動](../Page/清潔香港運動.md "wikilink")。
  - [7月22日](../Page/7月22日.md "wikilink")
    一日內兩宗慘劇，[柴灣道車禍](../Page/柴灣道.md "wikilink")3死7傷，停泊在[昂船洲修理的北江輪發生鍋爐爆炸引致](../Page/昂船洲.md "wikilink")3死9傷。
  - [8月1日](../Page/8月1日.md "wikilink")
    [香港理工學院揭幕](../Page/香港理工學院.md "wikilink")。
  - [8月2日](../Page/8月2日.md "wikilink")
    [香港海底隧道正式通車](../Page/香港海底隧道.md "wikilink")。\[4\]
  - [8月31日](../Page/8月31日.md "wikilink")，[新蒲崗](../Page/新蒲崗.md "wikilink")[利源工業大廈五級火](../Page/利源工業大廈五級火.md "wikilink")，2人喪生。
  - [9月28日](../Page/9月28日.md "wikilink")，[新蒲崗](../Page/新蒲崗.md "wikilink")[華富工業大廈五級火](../Page/華富工業大廈五級火.md "wikilink")，2人受傷。
  - [10月](../Page/10月.md "wikilink")
      - 廣角鏡月刊創刊
  - [10月14日](../Page/10月14日.md "wikilink")
    [香港](../Page/香港.md "wikilink")[銅鑼灣](../Page/銅鑼灣.md "wikilink")[大丸百貨發生強烈煤氣爆炸](../Page/大丸.md "wikilink")，[消防處列為一級火](../Page/香港消防處.md "wikilink")，造成2死211人受傷，其中一名死者為消防員。傷者中72人重傷，139人輕傷。\[5\]
  - [10月30日](../Page/10月30日.md "wikilink")
    [新蒲崗](../Page/新蒲崗.md "wikilink")[爵祿街](../Page/爵祿街.md "wikilink")64號2樓發生兇殺命案。\[6\]
  - [11月29日](../Page/11月29日.md "wikilink")
    香港[置地公司宣佈擁有八成](../Page/置地公司.md "wikilink")[牛奶公司股權](../Page/牛奶公司.md "wikilink")，收購成功。
  - [12月6日](../Page/12月6日.md "wikilink")
    12小時內發生三宗嚴重火警；灣仔[白宮酒店發生](../Page/白宮酒店.md "wikilink")[四級大火](../Page/白宮酒店四級火.md "wikilink")，造成9死21傷。同日中午，有[九龍](../Page/九龍.md "wikilink")[鑽石山](../Page/鑽石山.md "wikilink")[大磡村發生大火](../Page/大磡村.md "wikilink")，百多間木屋被焚。同日晚上，[中環](../Page/中環.md "wikilink")[於仁大廈](../Page/於仁大廈.md "wikilink")（今遮打大廈）發生[五級大火](../Page/於仁大廈五級火.md "wikilink")，一名消防員殉職，另有5人受傷。\[7\]
  - [12月31日](../Page/12月31日.md "wikilink") 星光行除夕命案。\[8\]
  - [萬宜水庫動工](../Page/萬宜水庫.md "wikilink")
  - [美心快餐開業](../Page/美心快餐.md "wikilink")
  - [房屋委員會成立](../Page/房屋委員會.md "wikilink")
  - 香港作家[金庸宣佈封筆](../Page/金庸.md "wikilink")，停止創作武俠小說。

## 出生人物

  - 3月9日 [鄭中基](../Page/鄭中基.md "wikilink")
  - 3月31日 [趙學而](../Page/趙學而.md "wikilink")
  - 7月6日 [戴耀明](../Page/戴耀明.md "wikilink")
  - 7月17日 [霍汶希](../Page/霍汶希.md "wikilink")
  - 8月18日 [古巨基](../Page/古巨基.md "wikilink")
  - 8月19日 [鄭秀文](../Page/鄭秀文.md "wikilink")
  - 9月13日 [陳慧琳](../Page/陳慧琳.md "wikilink")
  - 9月17日 [甄志強](../Page/甄志強.md "wikilink")
  - 12月5日 [郭卓樺](../Page/郭卓樺.md "wikilink")

## 逝世人物

  - 消防員[王傍旺](../Page/王傍旺.md "wikilink")，殉職於[大丸百貨煤氣爆炸一級火](../Page/大丸百貨煤氣爆炸一級火.md "wikilink")。
  - 消防員[伍亮秋](../Page/伍亮秋.md "wikilink")，殉職於[於仁大廈五級火](../Page/於仁大廈五級火.md "wikilink")。

## 參考文獻

## 參考書籍

  - 陳昕 郭志坤：《香港全紀錄》，卷二，香港中華書店，1998。 ISBN 978-962-231-891-5

[Category:1972年香港](../Category/1972年香港.md "wikilink")
[Category:20世紀各年香港](../Category/20世紀各年香港.md "wikilink")
[Category:1972年](../Category/1972年.md "wikilink")

1.
2.
3.
4.
5.  「大丸公司漏煤氣爆炸
    兩死二百餘傷大慘劇」，[工商日報第一頁](../Page/工商日報.md "wikilink")，1972年10月15日
6.  華僑日報, 1972-10-31 第15頁
7.
8.  香港工商日報, 1973-01-01 第11頁