**RPM套件管理員**（簡稱**RPM**，全称为**The RPM Package
Manager**）是在Linux下广泛使用的[软件包管理器](../Page/软件包管理器.md "wikilink")。RPM此名詞可能是指.rpm的檔案格式的[軟體包](../Page/軟體包.md "wikilink")，也可能是指其本身的软件包管理器(RPM
Package Manager)。最早由[Red
Hat研制](../Page/Red_Hat.md "wikilink")，现在也由[开源社区开发](../Page/开源社区.md "wikilink")。RPM通常随附于[Linux发行版](../Page/Linux发行版.md "wikilink")，但也有单独将RPM作为[应用软件发行的发行版](../Page/应用软件.md "wikilink")（例如[Gentoo](../Page/Gentoo.md "wikilink")）。RPM仅适用于安装用RPM来打包的软件，目前是[GNU/Linux下软件包资源最丰富的软件包类型之一](../Page/GNU/Linux.md "wikilink")。

## RPM软件包

RPM软件包分为二进制包（Binary）、源代码包（Source）和Delta包三种。二进制包可以直接安装在计算机中，而源代码包将会由RPM自动编译、安装。源代码包经常以src.rpm作为[后缀名](../Page/后缀名.md "wikilink")。

## 参见

  - [Linux](../Page/Linux.md "wikilink")
  - [Linux套件列表](../Page/Linux套件列表.md "wikilink")

## 参考资料

## 外部链接

  - [RPM网站](http://www.rpm.org)

[Category:归档格式](../Category/归档格式.md "wikilink")
[Category:自由软件包管理系统](../Category/自由软件包管理系统.md "wikilink")
[Category:Linux软件包管理相关软件](../Category/Linux软件包管理相关软件.md "wikilink")
[Category:Red Hat](../Category/Red_Hat.md "wikilink")