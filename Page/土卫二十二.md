**土卫二十二**(S/2000 S 6,
Ijiraq)是环绕[土星运行的一颗正轉不規則](../Page/土星.md "wikilink")[卫星](../Page/卫星.md "wikilink")，在2000年被
[約翰·卡維拉斯和](../Page/約翰·卡維拉斯.md "wikilink")[布萊特·格萊德曼發現](../Page/布萊特·格萊德曼.md "wikilink")，隨後並給它一個暫時性的編號**S/2000
S
6**\[1\]\[2\]。直到2003年才替它取了一個正式的英文名[伊耶拉克](../Page/伊耶拉克.md "wikilink")(Ijiraq)，而伊耶拉克在[因努伊特神話中的一個怪物](../Page/因努伊特神話.md "wikilink")\[3\]。

## 命名

位在[麥克馬斯特大學的天文學家](../Page/麥克馬斯特大學.md "wikilink")[約翰·卡維拉斯](../Page/約翰·卡維拉斯.md "wikilink")，以[因努伊特神話中的](../Page/因努伊特神話.md "wikilink")[伊耶拉克](../Page/伊耶拉克.md "wikilink")(Ijiraq)作為它的英文名，這首次跳脫了以[希臘神話或](../Page/希臘神話.md "wikilink")[羅馬神話中的人物為衛星命名的習慣](../Page/羅馬神話.md "wikilink")。他發現衛星後，就開始為它找一個適當的名稱，他希望一個可以融合多種文化卻又可以代表[加拿大的名字](../Page/加拿大.md "wikilink")，但經過數個月的思考和詢問許多人文學者後，他還是無法找到一個適當的名稱。直到2001年3月，他在為他還孩子讀一本[因努伊特的故事書時](../Page/因努伊特.md "wikilink")，他發現[伊耶拉克是一個喜歡躲躲藏藏的怪物](../Page/伊耶拉克.md "wikilink")，這就和所有[土星的的小](../Page/土星.md "wikilink")[衛星一樣](../Page/衛星.md "wikilink")，喜歡躲躲藏藏的，另外，[因努伊特人住在北極圈內](../Page/因努伊特人.md "wikilink")，非常的冷，也和土衛二十二一樣。因此，他就將土衛二十二取為伊耶拉克。

## 軌道特性

[TheIrregulars_SATURN_PRO_GROUPS.svg](https://zh.wikipedia.org/wiki/File:TheIrregulars_SATURN_PRO_GROUPS.svg "fig:TheIrregulars_SATURN_PRO_GROUPS.svg")，紅色為[高盧衛星群](../Page/高盧衛星群.md "wikilink")。\]\]土衛二十二離土星平均為11100000公里，軌道傾角46°，其軌道與[土衛二十四非常類似](../Page/土衛二十四.md "wikilink")。左圖為土衛二十二和其他軌道類似的[正轉衛星之比較](../Page/正轉衛星.md "wikilink")，圖裡黃色的線條表示每個衛星的[離心率](../Page/離心率.md "wikilink")，線條的兩端分別衛星的[近日點和](../Page/近日點.md "wikilink")[遠日點](../Page/遠日點.md "wikilink")。一般來說，土衛二十二受到[古在共振](../Page/古在共振.md "wikilink")(Kozai
resonance)的影響，他的[離心率和](../Page/離心率.md "wikilink")[軌道傾角會隨時間變化](../Page/軌道傾角.md "wikilink")，而且它的[近心點角十分不穩定](../Page/近心點角.md "wikilink")，它的近心點角大約在90°左右，但有高達60°度的振幅。\[4\]

## 物理特性

一般來說，土衛二十二屬於[因紐特群的一員](../Page/因紐特群.md "wikilink")\[5\]，而有趣的是，它似乎比其他[因紐特群還](../Page/因紐特群.md "wikilink")「紅」的許多([土衛二十](../Page/土衛二十.md "wikilink")、[土衛二十四和](../Page/土衛二十四.md "wikilink")[土衛二十九](../Page/土衛二十九.md "wikilink"))，在紅光的部份它的[光譜斜率](../Page/光譜斜率.md "wikilink")(一個物體的[反射比和它](../Page/反射比.md "wikilink")[反射波長之間](../Page/波.md "wikilink")[函數的](../Page/函數.md "wikilink")[斜率](../Page/斜率.md "wikilink"))比一般衛星還高出許多(每100nm升19.5%)，這表示它對紅光的反射率長強，但對其他顏色的光卻非常弱，換言之他是一個非常紅的行星，這個現象在一般的[外海王星天體](../Page/外海王星天體.md "wikilink")(如[塞德娜](../Page/塞德娜.md "wikilink"))很常見，但對衛星來說卻是十分罕見。另外，土衛二十二的[光譜顯示他的表面很有可能有](../Page/光譜.md "wikilink")[冰的存在](../Page/冰.md "wikilink")。\[6\]

## 参见

  - [土星的卫星](../Page/土星的卫星.md "wikilink")

## 參考資料

[土卫22](../Category/土星的卫星.md "wikilink")

1.  [IAUC 7521: *S/2000 S 5, S/2000 S
    6*](http://cfa-www.harvard.edu/iauc/07500/07521.html)  November 18,
    2000 (discovery)
2.  [MPEC 2000-Y14: *S/2000 S 3, S/2000 S 4, S/2000 S 5, S/2000 S 6,
    S/2000 S 10*](http://cfa-www.harvard.edu/mpec/K00/K00Y14.html)
    December 19, 2000 (discovery and ephemeris)
3.  [IAUC 8177: *Satellites of Jupiter, Saturn,
    Uranus*](http://cfa-www.harvard.edu/iauc/08100/08177.html)  August
    8, 2003 (naming the moon)
4.  [Nesvorný, D.](../Page/David_Nesvorný.md "wikilink"); [Alvarellos,
    Jose L. A.](../Page/Jose_L._A._Alvarellos.md "wikilink"); [Dones,
    L.](../Page/Luke_Dones.md "wikilink"); and [Levison, H.
    F.](../Page/Harold_F._Levison.md "wikilink"); [*Orbital and
    Collisional Evolution of the Irregular
    Satellites*](http://www.journals.uchicago.edu/AJ/journal/issues/v126n1/202528/202528.web.pdf),
    The Astronomical Journal, **126** (2003), pp. 398–429
5.  Gladman, B. J.; [Nicholson, P.
    D.](../Page/Philip_D._Nicholson.md "wikilink"); [Burns, J.
    A.](../Page/Joseph_A._Burns.md "wikilink"); Kavelaars, J. J.;
    [Marsden, B. G.](../Page/Brian_G._Marsden.md "wikilink"); [Holman,
    M. J.](../Page/Matthew_J._Holman.md "wikilink"); Grav, T.;
    [Hergenrother, C. W.](../Page/Carl_W._Hergenrother.md "wikilink");
    [Petit, J.-M.](../Page/Jean-Marc_Petit.md "wikilink"); [Jacobson, R.
    A.](../Page/Robert_A._Jacobson.md "wikilink"); and [Gray, W.
    J.](../Page/William_J._Gray.md "wikilink"); [*Discovery of 12
    satellites of Saturn exhibiting orbital
    clustering*](http://www.nature.com/nature/journal/v412/n6843/abs/412163a0.html),
    Nature, **412** (July 12, 2001), pp. 163–166
6.  [Grav, T.](../Page/Tommy_Grav.md "wikilink"); and [Bauer,
    J.](../Page/James_Bauer.md "wikilink"); [*A deeper look at the
    colors of Saturnian irregular
    satellites*](http://arxiv.org/abs/astro-ph/0611590)