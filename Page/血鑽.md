[Hands_ondiamonds_350.jpg](https://zh.wikipedia.org/wiki/File:Hands_ondiamonds_350.jpg "fig:Hands_ondiamonds_350.jpg")淘鑽石\]\]
**血鑽**（也稱**衝突鑽石**、**战争钻石**或**血鑽石**）是一种在[战争区域开采并销往](../Page/战争.md "wikilink")[市场的](../Page/市场.md "wikilink")[钻石](../Page/钻石.md "wikilink")。依照[聯合國的定義](../Page/联合国.md "wikilink")，假如反政府組織出產鑽石，而利潤被用於推翻該政府，那些鑽石就會被稱爲血鑽\[1\]。血鑽是由于销售钻石所得到的高额[利润和](../Page/利润.md "wikilink")[资金會被投入到反政府或违背](../Page/资金.md "wikilink")[安理会精神的流血武装冲突得名](../Page/安理会.md "wikilink")。这些组织派系秘密地为暴徒提供活动经费，或为组建进行侵略的[军事力量筹措资金以发动战争](../Page/军事力量.md "wikilink")。有些[非政府组织声称](../Page/非政府组织.md "wikilink")，血鑽的利润是2001年9月11日的[九一一袭击事件的资金来源](../Page/九一一袭击事件.md "wikilink")。

2002年11月，为根除非洲血鑽的非法贸易、维护非洲地区的和平与稳定，联合国通过了《[金伯利进程国际證書制度](../Page/金伯利进程国际證書制度.md "wikilink")》，未附有金伯利进程成员所签发证明书的[毛坯钻石进口以及面向非金伯利进程成员出口毛坯钻石都被禁止](../Page/毛坯钻石.md "wikilink")。

有证据显示，位于[安哥拉的](../Page/安哥拉.md "wikilink")“[争取安哥拉彻底独立全国同盟](../Page/争取安哥拉彻底独立全国同盟.md "wikilink")（UNITA）”和位于[塞拉利昂的](../Page/塞拉利昂.md "wikilink")“[革命联合阵线](../Page/革命联合阵线.md "wikilink")（RUF）”的[财政资金来自](../Page/财政.md "wikilink")[利比里亚政府的钻石](../Page/利比里亚.md "wikilink")[贸易](../Page/贸易.md "wikilink")。

## 影視作品

  - 血鑽是2002年電影 *[新鐵金剛之不日殺機](../Page/新鐵金剛之不日殺機.md "wikilink")* 的主題之一。
  - 由[-{zh-tw:李奧納多·狄卡皮歐; zh-hk:里安納度·狄卡比奧;
    zh-cn:莱昂纳多·迪卡普里奥;}-主演的](../Page/莱昂纳多·迪卡普里奥.md "wikilink")2006年[電影](../Page/電影.md "wikilink")《[-{zh-cn:血钻;
    zh-tw:血鑽石;
    zh-hk:血鑽;}-](../Page/血鑽_\(電影\).md "wikilink")》，讲述了因血鑽的[利益而在塞拉利昂所引發的多年](../Page/利益.md "wikilink")[內戰](../Page/塞拉利昂内战.md "wikilink")。
  - 在歌曲中，歌詞描述了塞拉里昂的血鑽交易，並批評西方民眾對於血鑽來源的無知。
  - 血鑽在[極地戰嚎2中是玩家的虛擬貨幣](../Page/極地戰嚎2.md "wikilink")。

## 参考资料

[Category:战争](../Category/战争.md "wikilink")
[Category:非洲经济](../Category/非洲经济.md "wikilink")
[Category:鑽石](../Category/鑽石.md "wikilink")
[Category:全球性议题](../Category/全球性议题.md "wikilink")
[Category:经济全球化](../Category/经济全球化.md "wikilink")

1.