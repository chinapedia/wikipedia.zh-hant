**元謀龍屬**（屬名：*Yuanmousaurus*）是種[蜥腳下目](../Page/蜥腳下目.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，生存於中[侏儸紀的](../Page/侏儸紀.md "wikilink")[中國](../Page/中國.md "wikilink")。元謀龍取名於[雲南省](../Page/雲南省.md "wikilink")[元謀縣](../Page/元謀縣.md "wikilink")，是它的化石的發現所在地。不過，元謀龍的化石是一個不完整骨骼，發現於[張和組](../Page/張和組.md "wikilink")。身長估計可能約17公尺。[模式種是](../Page/模式種.md "wikilink")**姜驛元謀龍**（*Yuanmousaurus
jiangyiensis*），是由[董枝明](../Page/董枝明.md "wikilink")、[吕君昌等人所敘述](../Page/吕君昌.md "wikilink")、命名。元謀龍可能與[巴塔哥尼亞龍是近親](../Page/巴塔哥尼亞龍.md "wikilink")。牠們比[峨嵋龍還衍化](../Page/峨嵋龍.md "wikilink")，但比巴塔哥尼亞龍原始。

## 參考資料

  - Lu Junchang, Li Shaoxue, Ji Qiang, Wang Guofu, Zhang Jiahua & Dong
    Zhiming Acta Geologica Sinica-English Edition, 2006. No. 1. *New
    Eusauropod Dinosaur from Yuanmou of Yunnan Province, China*. pp
    5-14.

## 外部連結

  - [Archives of the Dinosaur Mailing
    List](http://dml.cmnh.org/2006Apr/msg00007.html)

[Category:侏羅紀恐龍](../Category/侏羅紀恐龍.md "wikilink")
[Category:亞洲恐龍](../Category/亞洲恐龍.md "wikilink")
[Category:大鼻龍類](../Category/大鼻龍類.md "wikilink")