**哈薩克蘇維埃社會主義共和國國徽**啟用於1937年3月26日，參照[蘇聯](../Page/蘇聯.md "wikilink")[國徽設計而成](../Page/蘇聯國徽.md "wikilink")。

呈圓形，中為旭日初升之象，上方有紅色五角星和[鎚子與鐮刀](../Page/鎚子與鐮刀.md "wikilink")，象徵[社會主義照亮全球](../Page/社會主義.md "wikilink")。

外圍由麥穗。飾帶以[俄語和](../Page/俄語.md "wikilink")[哈薩克語書有](../Page/哈薩克語.md "wikilink")[蘇聯](../Page/蘇聯.md "wikilink")[國家格言](../Page/國家格言.md "wikilink")「[全世界無產者，聯合起來！](../Page/全世界無產者，聯合起來！.md "wikilink")」。下方以俄語和哈薩克語書有「哈薩克蘇維埃社會主義共和國」的首字母。

1991年12月26日，[哈薩克斯坦國徽成為新國家的國徽](../Page/哈薩克斯坦國徽.md "wikilink")。

[Category:哈萨克苏维埃社会主义共和国](../Category/哈萨克苏维埃社会主义共和国.md "wikilink")
[K](../Category/蘇聯國徽.md "wikilink")