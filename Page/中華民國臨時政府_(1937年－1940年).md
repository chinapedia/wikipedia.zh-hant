[庆祝中华民国临时政府成立.jpg](https://zh.wikipedia.org/wiki/File:庆祝中华民国临时政府成立.jpg "fig:庆祝中华民国临时政府成立.jpg")
[中华民国临时政府成立.jpg](https://zh.wikipedia.org/wiki/File:中华民国临时政府成立.jpg "fig:中华民国临时政府成立.jpg")
[ROC_government_officials_(Provisional_Government_of_the_Republic_of_China).jpg](https://zh.wikipedia.org/wiki/File:ROC_government_officials_\(Provisional_Government_of_the_Republic_of_China\).jpg "fig:ROC_government_officials_(Provisional_Government_of_the_Republic_of_China).jpg")
**中華民國臨時政府**，是[大日本帝国在](../Page/大日本帝国.md "wikilink")[中国抗日战争期間成立的](../Page/中国抗日战争.md "wikilink")[傀儡政權](../Page/傀儡政權.md "wikilink")，於1937年12月14日在[北京成立](../Page/北京.md "wikilink")，統轄[平津和](../Page/平津.md "wikilink")[華北等地區](../Page/華北.md "wikilink")。為與[中華民國維新政府相區別](../Page/中華民國維新政府.md "wikilink")，又稱**华北临时政府**。以[五色旗為](../Page/五色旗.md "wikilink")[國旗](../Page/國旗.md "wikilink")。

## 歷史

「中華民國臨時政府总统」虚设，「行政委員長」為[王克敏](../Page/王克敏.md "wikilink")，模仿[三權分立制度](../Page/三權分立.md "wikilink")，下設「議政」、「[行政](../Page/行政.md "wikilink")」、「[司法](../Page/司法.md "wikilink")」三委員會，「議政委員會委員長兼教育總長」為[湯爾和](../Page/湯爾和.md "wikilink")，「司法委員會委員長」為[董康](../Page/董康.md "wikilink")。曾經於[北洋政府任職的](../Page/北洋政府.md "wikilink")[王揖唐](../Page/王揖唐.md "wikilink")（國會議長）與[齊燮元等人都參與過](../Page/齊燮元.md "wikilink")「中華民國臨時政府」。「行政委員會」下設四部，「行政部」由王克敏主持，「治安部」由齊燮元主持，「法制部」由[朱深主持](../Page/朱深.md "wikilink")，「賑濟部」由王揖唐主持。「中華民國臨時政府」除了組成「[新民會](../Page/中华民国新民会.md "wikilink")」作為「東亞解放新國民運動」的動員機制外，並且設有[华北治安军](../Page/华北治安军.md "wikilink")，总司令是齊燮元。

「中華民國臨時政府」的主導者為[日軍中的](../Page/日軍.md "wikilink")[華北方面軍](../Page/華北方面軍.md "wikilink")，「中華民國臨時政府」的組成者多為[北洋政府的](../Page/北洋政府.md "wikilink")[官僚人物](../Page/官僚.md "wikilink")。1940年[汪精衛成立](../Page/汪精衛.md "wikilink")「[南京國民政府](../Page/汪精卫政权.md "wikilink")」後，「中華民國臨時政府」被解散，改為“[華北政務委員會](../Page/華北政務委員會.md "wikilink")”，王揖唐任委员长，由日本人直接控制，不听命汪精卫政府。

## 歷任

| 人　名                              | 臨時政府官職                                                                                                | 原任[北洋政府官職](../Page/北洋政府.md "wikilink") |
| -------------------------------- | ----------------------------------------------------------------------------------------------------- | -------------------------------------- |
| [王克敏](../Page/王克敏.md "wikilink") | [Wang_Kemin.jpg](https://zh.wikipedia.org/wiki/File:Wang_Kemin.jpg "fig:Wang_Kemin.jpg")             | 行政委員會委員長兼行政部總長                         |
| [湯爾和](../Page/湯爾和.md "wikilink") | [Tang_Erhe.jpg](https://zh.wikipedia.org/wiki/File:Tang_Erhe.jpg "fig:Tang_Erhe.jpg")                | 議政委員会委員長兼教育總長                          |
| [董康](../Page/董康.md "wikilink")   | [Dong_Kang.jpg](https://zh.wikipedia.org/wiki/File:Dong_Kang.jpg "fig:Dong_Kang.jpg")                | 司法委員会委員長                               |
| [王揖唐](../Page/王揖唐.md "wikilink") | [Wang_Yitang.JPG](https://zh.wikipedia.org/wiki/File:Wang_Yitang.JPG "fig:Wang_Yitang.JPG")          | 内政總長                                   |
| [齊燮元](../Page/齊燮元.md "wikilink") | [Qi_Xieyuan.jpg](https://zh.wikipedia.org/wiki/File:Qi_Xieyuan.jpg "fig:Qi_Xieyuan.jpg")             | 治安總長兼華北治安軍司令                           |
| [王蔭泰](../Page/王蔭泰.md "wikilink") | [Wang_Yintai.jpg](https://zh.wikipedia.org/wiki/File:Wang_Yintai.jpg "fig:Wang_Yintai.jpg")          | 實業總長                                   |
| [江朝宗](../Page/江朝宗.md "wikilink") | [Jiang_Chaozong.jpg](https://zh.wikipedia.org/wiki/File:Jiang_Chaozong.jpg "fig:Jiang_Chaozong.jpg") | [北京特別市市長](../Page/北平市.md "wikilink")   |
| [高凌霨](../Page/高凌霨.md "wikilink") | [高凌霨.jpg](https://zh.wikipedia.org/wiki/File:高凌霨.jpg "fig:高凌霨.jpg")                                   | [天津特別市市長](../Page/天津特别市.md "wikilink") |

## 参见

[Z](../Category/中华民国大陆时期中央政府机构.md "wikilink")
[Z](../Category/日伪政权.md "wikilink")
[Z](../Category/北京政治史.md "wikilink")
[Z](../Category/1937年建立的國家或政權.md "wikilink")
[Z](../Category/1940年終結的國家或政權.md "wikilink")
[Z](../Category/短命國家.md "wikilink")