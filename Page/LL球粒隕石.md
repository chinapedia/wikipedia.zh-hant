**LL球粒隕石**（又稱為低鐵低金屬群球粒隕石）是最豐富的[普通球粒隕石中佔的比例最小的一個族群](../Page/普通球粒隕石.md "wikilink")，大約是已發現墬落球粒隕石的10-11%，和所有墬落隕石的8-9%（參見[墬落隕石的統計](../Page/墬落隕石的統計.md "wikilink")）。

**LL**所代表的意義是**低鐵**（總含量）和**低金屬**，它們的鐵總含量約為19-22%，而金屬鐵則只有0.3-3%，這意味著多數的鐵都是以氧化鐵（FeO）的形式存在於矽酸鹽內；橄欖石中包含26-32摩爾百分比的[鐵橄欖石](../Page/鐵橄欖石.md "wikilink")（Fa）。含量最豐富的礦物是[紫蘇輝石](../Page/紫蘇輝石.md "wikilink")（一種[輝石](../Page/輝石.md "wikilink")）和[橄欖石](../Page/橄欖石.md "wikilink")。其它的礦物包括鐵-鎳金屬、[隕硫鐵](../Page/隕硫鐵.md "wikilink")（FeS）、[長石或長石的玻璃](../Page/長石.md "wikilink")、[鉻鐵礦](../Page/鉻鐵礦.md "wikilink")、和磷酸鹽。

LL球粒隕石在普通球粒隕石的族群中擁有最大的[球粒](../Page/球粒.md "wikilink")，平均直徑在1mm。LL群組包括許多主要的普通球粒隕石，包括最著名的[Semarkona](../Page/Semarkona.md "wikilink")（類型3.0）的球粒隕石。但是，大多數的LL球粒隕石都曾經歷過熱變質，在岩時學上的類型是5和6，這表示它們礦物中的成分是均勻的，球粒的邊緣已經擴散而不易分辨。這些，加上金屬含量低，導致19世紀的礦物學家[Tschermak推定他門是從](../Page/Gustav_Tschermak_von_Seysenegg.md "wikilink")[無球粒隕石過渡到](../Page/無球粒隕石.md "wikilink")[球粒隕石的中間產物](../Page/球粒隕石.md "wikilink")，並命名為[古銅橄欖無粒隕石](../Page/古銅橄欖無粒隕石.md "wikilink")\[1\]
或「橄欖石-紫蘇輝石球粒隕石」。我們現在知道LL球粒隕石和無球粒隕石是完全不同的，因此這兩個名詞在LL球粒隕石的分類中早就不再使用了。許多LL球粒隕石視角礫岩。

## 外部連結

  -
  -
## 參考資料

也可參考：:

  -
    F. Heide and F. Wlotzka, *Meteorites: Messengers from Space*.
    Springer-Verlag, 1995.

[fi:LL-kondriitti](../Page/fi:LL-kondriitti.md "wikilink")

[Category:陨石](../Category/陨石.md "wikilink")
[Category:隕石類型](../Category/隕石類型.md "wikilink")

1.  \[<http://adsabs.harvard.edu/abs/1964SCoA>....4..137T "The
    Microscopic Properties of Meteorites" by Gustav Tschermak (Die
    Mikroskopische Beschaffenheit der Meteoriten, translated by John A.
    Wood and K. Mathilde Wood). *Smithsonian Contributions to
    Astrophysics,* vol. 4, p. 137–239\].