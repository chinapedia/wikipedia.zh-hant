[HK_AlliancePrimarySchoolKowloonTong.JPG](https://zh.wikipedia.org/wiki/File:HK_AlliancePrimarySchoolKowloonTong.JPG "fig:HK_AlliancePrimarySchoolKowloonTong.JPG")
[Chinese_Christian_&_Missionary_Alliance_Church.jpg](https://zh.wikipedia.org/wiki/File:Chinese_Christian_&_Missionary_Alliance_Church.jpg "fig:Chinese_Christian_&_Missionary_Alliance_Church.jpg")（圖中前方）\]\]
[Alliance_Primary_School_Kowloon_Tong_Pak_Tin_Estate_2014.jpg](https://zh.wikipedia.org/wiki/File:Alliance_Primary_School_Kowloon_Tong_Pak_Tin_Estate_2014.jpg "fig:Alliance_Primary_School_Kowloon_Tong_Pak_Tin_Estate_2014.jpg")
**九龍塘宣道小學**（**Alliance Primary School, Kowloon
Tong**）是[香港的一所非牟利私立全日制小學](../Page/香港.md "wikilink")，亦是[香港九龍塘基督教中華宣道會屬下的](../Page/香港九龍塘基督教中華宣道會.md "wikilink")[小學](../Page/小學.md "wikilink")，於1955年創辦。

九龍塘宣道小學創辦時，校舍設於[九龍仔蘭開夏道二號](../Page/九龍仔.md "wikilink")。為了配合政府推行的小學全日制政策，校舍於1998年在原址進行重建，校舍重建期間，暫使用[紅磡](../Page/紅磡.md "wikilink")[黃埔花園德豐街](../Page/黃埔花園.md "wikilink")[黃埔宣道小學兩年作為臨時校舍](../Page/黃埔宣道小學.md "wikilink")，後再遷往[石硤尾](../Page/石硤尾.md "wikilink")[白田邨的小學校舍兩年作臨時校舍](../Page/白田邨.md "wikilink")，新校舍已於2002年完成重建。

新校舍為一所符合現代學校校舍標準設計的校舍，面前6243平方米，設有30個課室、禮堂、圖書館、室內運動場、音樂室及多媒體教室等。

九龍塘宣道小學於2002年度獲香港藝術發展局頒發學校組優秀藝術教育銀獎，亦分別於2005年及2006年獲世界管樂協會及北京市教育委員會分別邀請前往[新加坡及](../Page/新加坡.md "wikilink")[北京進行交流及演出](../Page/北京.md "wikilink")。

## 樓層分佈

  - 地庫：[九龍塘宣道會](../Page/九龍塘宣道會.md "wikilink")
  - 地下：停車場
  - 一樓：禮堂
  - 二樓：宣道幼稚園（後梯與二樓不相連）, 活動室 ( 興建中 )
  - 三樓：校務處、圖書館、閱讀室、1A、1B,1C教員休息室
  - 四樓：電腦室、1D,2A,2B,2C,2D,6D
  - 五樓：音樂室、3A、3B, 3C, 3D, 5A, 5B輔導室
  - 六樓：美勞室、4A, 4B, 4C, 4D, 5C, 5D、輔導室
  - 七樓：體育館、視聽室、6A, 6B, 6C
  - 八樓：教員室、教員休息室
  - 九樓：教會重地
  - 十樓（天台）：空調機房

## 其他宣道小學

  - [大坑東宣道小學](http://www.apstht.edu.hk)
  - [黃埔宣道小學](http://www.apsw.edu.hk)
  - [宣道會葉紹蔭紀念小學](http://www.casyymps.edu.hk)
  - [上水宣道小學](http://www.apsss.edu.hk)

## 著名校友

  - [梁偉文](../Page/梁偉文.md "wikilink")（[林夕](../Page/林夕.md "wikilink")） -
    香港著名填詞人、[商業電台創作總監](../Page/商業電台.md "wikilink")\[1\]
  - [梁德輝](../Page/梁德輝.md "wikilink")（Eric） -
    [香港電台DJ](../Page/香港電台.md "wikilink")
  - [盧頌萱](../Page/盧頌萱.md "wikilink")（Circle） -
    前[香港有線電視新聞主播](../Page/香港有線電視新聞.md "wikilink")
  - [潘曉彤](../Page/潘曉彤.md "wikilink")（Rachel） - 香港電視藝員
  - [鄭敬基](../Page/鄭敬基.md "wikilink") - 香港男藝人\[2\]
  - [陳華國](../Page/陳華國.md "wikilink") - 香港著名舞台服裝設計師

## 注釋

## 參考來源

## 外部連結

  - [九龍塘宣道小學](http://www.apskt.edu.hk/)

[Category:香港九龍塘基督教中華宣道會](../Category/香港九龍塘基督教中華宣道會.md "wikilink")
[Category:九龍城區小學](../Category/九龍城區小學.md "wikilink")
[Category:九龍仔](../Category/九龍仔.md "wikilink")

1.
2.  [Joe Cheng's primary
    school, 2018-5-23](https://drive.google.com/file/d/174M3WHh6hQChBqJ-HI8UkCTm9Q1ZvsGO/view?usp=sharing)