**山陰地方**（）是[日本的](../Page/日本.md "wikilink")[地理區劃之一](../Page/日本地理區劃.md "wikilink")，泛指[本州西部面向](../Page/本州.md "wikilink")[日本海一側的地區](../Page/日本海.md "wikilink")，名稱則來自于[五畿七道中的](../Page/五畿七道.md "wikilink")[山陰道](../Page/山陰道_\(日本\).md "wikilink")。涵蓋範圍為[鳥取縣](../Page/鳥取縣.md "wikilink")、[島根縣以及](../Page/島根縣.md "wikilink")[山口縣北部地區](../Page/山口縣.md "wikilink")，但有時[京都府日本海側地區](../Page/京都府.md "wikilink")、[岡山縣北部和](../Page/岡山縣.md "wikilink")[廣島縣北部也會被列入](../Page/廣島縣.md "wikilink")。

## 地理

### 地形

  - [山](../Page/山.md "wikilink")：[大山](../Page/大山_\(鸟取县\).md "wikilink")、[三瓶山](../Page/三瓶山.md "wikilink")、[冰之山](../Page/冰之山.md "wikilink")
  - [川](../Page/川.md "wikilink")：千代川、日野川、[江之川](../Page/江之川.md "wikilink")
  - [湖](../Page/湖.md "wikilink")：[中海](../Page/中海.md "wikilink")、[宍道湖](../Page/宍道湖.md "wikilink")

## 地域

[缩略图](https://zh.wikipedia.org/wiki/File:Daisen_01.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Izumo-taisha_121539276_b25e49382d_o.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Tottori_castle08_1920.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Matsue_arban_area_No,2.JPG "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Kumano_taisha.jpg "fig:缩略图")

### 主要都市圈

1.  米子都市圈（25万2387人）
2.  鳥取都市圈（24万9067人）
3.  松江都市圈（22万5937人）
4.  出雲都市圈（17万3715人）
5.  豐岡都市圈（11万8565人）
6.  倉吉都市圈（11万6650人）
7.  濱田都市圈（9万8888人）
8.  益田都市圈（6万1051人）
9.  萩都市圈（5万6566人）

### 主要都市

#### 人口20万人以上

1.  [松江市](../Page/松江市.md "wikilink")（島根縣廳所在地，特例市）

#### 人口10万人以上

1.  [鳥取市](../Page/鳥取市.md "wikilink")（鳥取縣縣廳所在地，[特例市](../Page/特例市.md "wikilink")）
2.  [米子市](../Page/米子市.md "wikilink")（鳥取縣）
3.  [出雲市](../Page/出雲市.md "wikilink")（島根縣）

#### 人口5万人以上

1.  [豐岡市](../Page/豐岡市.md "wikilink")（兵庫縣）
2.  [萩市](../Page/萩市.md "wikilink")（山口縣）
3.  [濱田市](../Page/濱田市.md "wikilink")（島根縣）
4.  [倉吉市](../Page/倉吉市.md "wikilink")（鳥取縣）
5.  [益田市](../Page/益田市.md "wikilink")（島根縣）

## 交通

### 鐵路

  - [西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/山陰本線.md" title="wikilink">山陰本線</a></li>
<li><a href="../Page/因美線.md" title="wikilink">因美線</a></li>
<li><a href="../Page/伯備線.md" title="wikilink">伯備線</a></li>
<li><a href="../Page/境線.md" title="wikilink">境線</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/木次線.md" title="wikilink">木次線</a></li>
<li><a href="../Page/三江線.md" title="wikilink">三江線</a></li>
<li><a href="../Page/山口線.md" title="wikilink">山口線</a></li>
<li><a href="../Page/美彌線.md" title="wikilink">美彌線</a></li>
</ul></td>
</tr>
</tbody>
</table>

  - [智頭急行](../Page/智頭急行.md "wikilink")

<!-- end list -->

  - [智頭線](../Page/智頭急行智頭線.md "wikilink")

<!-- end list -->

  - [若櫻鐵道](../Page/若櫻鐵道.md "wikilink")

<!-- end list -->

  - [若櫻鐵道若櫻線](../Page/若櫻鐵道若櫻線.md "wikilink")

<!-- end list -->

  - [一畑電氣鐵道](../Page/一畑電氣鐵道.md "wikilink")

<!-- end list -->

  - [一畑電車北松江線](../Page/一畑電車北松江線.md "wikilink")
  - [一畑電車大社線](../Page/一畑電車大社線.md "wikilink")

### 主要道路

<table>
<tbody>
<tr class="odd">
<td><dl>
<dt>高速公路</dt>

</dl>
<ul>
<li><a href="../Page/山陰自動車道.md" title="wikilink">山陰自動車道</a></li>
<li><a href="../Page/米子自動車道.md" title="wikilink">米子自動車道</a></li>
<li><a href="../Page/濱田自動車道.md" title="wikilink">濱田自動車道</a></li>
</ul></td>
<td><dl>
<dt>幹線国道</dt>

</dl>
<ul>
<li><a href="../Page/國道9號_(日本).md" title="wikilink">国道9号</a></li>
<li><a href="../Page/国道29号.md" title="wikilink">国道29号</a></li>
<li><a href="../Page/国道53号.md" title="wikilink">国道53号</a></li>
<li><a href="../Page/国道54号.md" title="wikilink">国道54号</a></li>
</ul></td>
<td><dl>
<dt>其他国道</dt>

</dl>
<ul>
<li><a href="../Page/国道178号.md" title="wikilink">国道178号</a></li>
<li><a href="../Page/国道179号.md" title="wikilink">国道179号</a></li>
<li><a href="../Page/国道180号.md" title="wikilink">国道180号</a></li>
<li><a href="../Page/国道181号.md" title="wikilink">国道181号</a></li>
<li><a href="../Page/国道184号.md" title="wikilink">国道184号</a></li>
<li><a href="../Page/国道186号.md" title="wikilink">国道186号</a></li>
<li><a href="../Page/国道187号.md" title="wikilink">国道187号</a></li>
<li><a href="../Page/国道191号.md" title="wikilink">国道191号</a></li>
<li><a href="../Page/国道262号.md" title="wikilink">国道262号</a></li>
<li><a href="../Page/国道312号.md" title="wikilink">国道312号</a></li>
<li><a href="../Page/国道313号.md" title="wikilink">国道313号</a></li>
</ul></td>
</tr>
</tbody>
</table>

### 機場

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/但馬飛行場.md" title="wikilink">コウノトリ但馬空港</a>（<a href="../Page/豐岡市.md" title="wikilink">豐岡市</a>）</li>
<li><a href="../Page/鸟取机场.md" title="wikilink">鸟取机场</a>（<a href="../Page/鳥取市.md" title="wikilink">鳥取市</a>）</li>
<li><a href="../Page/美保飛行場.md" title="wikilink">米子空港</a>（<a href="../Page/境港市.md" title="wikilink">境港市</a>）</li>
</ul></td>
<td><ul>
<li><a href="../Page/出云机场.md" title="wikilink">出云机场</a>（<a href="../Page/斐川町.md" title="wikilink">斐川町</a>）</li>
<li><a href="../Page/石见机场.md" title="wikilink">萩・石見空港</a>（<a href="../Page/益田市.md" title="wikilink">益田市</a>）</li>
<li><a href="../Page/隐岐机场.md" title="wikilink">隐岐机场</a>（<a href="../Page/隱岐之島町.md" title="wikilink">隱岐之島町</a>）</li>
</ul></td>
</tr>
</tbody>
</table>

## 以山陰命名的企業

  - [山陰合同銀行](../Page/山陰合同銀行.md "wikilink")
  - [山陰放送](../Page/山陰放送.md "wikilink")
  - [山陰中央電視台](../Page/山陰中央電視台.md "wikilink")
  - 山陰中央新報社
  - [山陰調頻](../Page/山陰調頻.md "wikilink")

## 参考文献

## 参见

  - [山陽地方](../Page/山陽地方.md "wikilink")
  - [中國地方](../Page/中國地方.md "wikilink")
  - [四國](../Page/四國.md "wikilink")

{{-}}

[Category:日本地理區劃](../Category/日本地理區劃.md "wikilink")
[Category:鳥取縣地理](../Category/鳥取縣地理.md "wikilink")
[Category:島根縣地理](../Category/島根縣地理.md "wikilink")
[Category:兵庫縣地理](../Category/兵庫縣地理.md "wikilink")
[Category:京都府地理](../Category/京都府地理.md "wikilink")
[Category:山口縣地理](../Category/山口縣地理.md "wikilink")