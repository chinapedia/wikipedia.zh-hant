**NBA进步最快球员奖**是[国家篮球协会自](../Page/国家篮球协会.md "wikilink")[1985-86
NBA赛季起开始颁发的一个年度奖项](../Page/1985-86_NBA赛季.md "wikilink")，奖项在相应常规赛季中进步最多的球员。获奖者由[美国和](../Page/美国.md "wikilink")[加拿大的体育专栏作家投票选出](../Page/加拿大.md "wikilink")，每人可以投出一票，其中包含自己认为进步最多、第二多和第三多球员的姓名。球员每得到一张进步最多的选票支持则可得5分，第二多得3分，第三多得1分，总计得分最高者即可获奖（与实际所得进步最多选票的数量没有直接关系）\[1\]。

自成立以来，该奖项一共有30位得主，並沒有連續得獎者，最新的获奖者是[维克多·歐拉迪波](../Page/维克多·歐拉迪波.md "wikilink")。没有任何一位获奖者效力的球队在该赛季拿下[NBA总冠军](../Page/NBA总冠军列表.md "wikilink")，[伯利斯·迪奧是歷屆獲獎者中第一位以球員身份得過NBA總冠軍](../Page/伯利斯·迪奧.md "wikilink")。卓吉奇、[希达耶特·特克鲁](../Page/希达耶特·特克鲁.md "wikilink")、、[乔治·穆雷桑和](../Page/乔治·穆雷桑.md "wikilink")[伯利斯·迪奥是仅有的五位不是在美国出生的获奖者](../Page/伯利斯·迪奥.md "wikilink")，并且除了塞卡利以外，另外四位运动员也完全不是在美国接受籃球教育，塞卡利曾是[雪城大学男篮隊的成员](../Page/雪城大学.md "wikilink")。

[阿尔文·罗伯逊](../Page/阿尔文·罗伯逊.md "wikilink")、[达纳·巴罗斯](../Page/达纳·巴罗斯.md "wikilink")、[崔西·麥葛雷迪](../Page/崔西·麥葛雷迪.md "wikilink")、[杰梅因·奥尼尔](../Page/杰梅因·奥尼尔.md "wikilink")、[丹尼·格兰杰](../Page/丹尼·格兰杰.md "wikilink")、[凱文·洛夫](../Page/凱文·洛夫.md "wikilink")、[保罗·乔治](../Page/保罗·乔治.md "wikilink")、[揚尼斯·安戴托昆波和](../Page/揚尼斯·安戴托昆波.md "wikilink")[维克多·歐拉迪波都曾在获奖的同一赛季入选](../Page/维克多·歐拉迪波.md "wikilink")[NBA全明星赛](../Page/NBA全明星赛.md "wikilink")。[达勒·艾利斯](../Page/达勒·艾利斯.md "wikilink")、[凯文·达克沃斯](../Page/凯文·达克沃斯.md "wikilink")、[凯文·约翰逊](../Page/凯文·约翰逊.md "wikilink")、[吉尔伯特·阿里纳斯和](../Page/吉尔伯特·阿里纳斯.md "wikilink")[扎克·兰多夫都在之后入选](../Page/扎克·兰多夫.md "wikilink")[全明星赛](../Page/NBA全明星赛.md "wikilink")。只有麥葛雷迪、奥尼尔和乔治在获奖的同一赛季入选[NBA最佳阵容](../Page/NBA最佳阵容.md "wikilink")，其中麥葛雷迪进入第二队，奥尼尔和乔治进入第三队。

## 获奖球员

[TracyMcGrady.jpg](https://zh.wikipedia.org/wiki/File:TracyMcGrady.jpg "fig:TracyMcGrady.jpg")在[2000-01
NBA赛季获奖](../Page/2000-01_NBA赛季.md "wikilink")。\]\]
[Kevin_Love_2.jpg](https://zh.wikipedia.org/wiki/File:Kevin_Love_2.jpg "fig:Kevin_Love_2.jpg")在[2010-11年NBA赛季获奖](../Page/2010-11年NBA赛季.md "wikilink")。\]\]
[RyanAndersonMagic.jpg](https://zh.wikipedia.org/wiki/File:RyanAndersonMagic.jpg "fig:RyanAndersonMagic.jpg")在[2011-12
NBA赛季获奖](../Page/2011-12_NBA赛季.md "wikilink")。\]\]

|  |             |
|  | ----------- |
|  | 现役球员        |
|  | 入选奈史密斯篮球名人堂 |

<table>
<thead>
<tr class="header">
<th><p>赛季</p></th>
<th><p>球员</p></th>
<th><p>位置</p></th>
<th><p>国籍</p></th>
<th><p>球队</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p><a href="../Page/阿尔文·罗伯逊.md" title="wikilink">阿尔文·罗伯逊</a></p></td>
<td><p><a href="../Page/后卫.md" title="wikilink">后卫</a></p></td>
<td></td>
<td><p><a href="../Page/圣安东尼奥马刺.md" title="wikilink">圣安东尼奥马刺</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/达勒·艾利斯.md" title="wikilink">达勒·艾利斯</a></p></td>
<td><p><a href="../Page/后卫.md" title="wikilink">后卫</a>/<a href="../Page/前锋.md" title="wikilink">前锋</a></p></td>
<td></td>
<td><p><a href="../Page/西雅图超音速.md" title="wikilink">西雅图超音速</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/凯文·达克沃斯.md" title="wikilink">凯文·达克沃斯</a></p></td>
<td><p><a href="../Page/中锋_(篮球).md" title="wikilink">中锋</a></p></td>
<td></td>
<td><p><a href="../Page/波特兰开拓者.md" title="wikilink">波特兰开拓者</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/凯文·约翰逊.md" title="wikilink">凯文·约翰逊</a></p></td>
<td><p><a href="../Page/后卫.md" title="wikilink">后卫</a></p></td>
<td></td>
<td><p><a href="../Page/菲尼克斯太阳.md" title="wikilink">菲尼克斯太阳</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/中锋_(篮球).md" title="wikilink">中锋</a></p></td>
<td></td>
<td><p><a href="../Page/迈阿密热火.md" title="wikilink">迈阿密热火</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/斯科特·斯基尔斯.md" title="wikilink">斯科特·斯基尔斯</a></p></td>
<td><p><a href="../Page/后卫.md" title="wikilink">后卫</a></p></td>
<td></td>
<td><p><a href="../Page/奥兰多魔术.md" title="wikilink">奥兰多魔术</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/佩维斯·埃里森.md" title="wikilink">佩维斯·埃里森</a></p></td>
<td><p><a href="../Page/中锋_(篮球).md" title="wikilink">中锋</a>/<a href="../Page/前锋.md" title="wikilink">前锋</a></p></td>
<td></td>
<td><p><a href="../Page/华盛顿奇才.md" title="wikilink">华盛顿奇才</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/马翰默德·劳夫.md" title="wikilink">马翰默德·劳夫</a></p></td>
<td><p><a href="../Page/后卫.md" title="wikilink">后卫</a></p></td>
<td></td>
<td><p><a href="../Page/丹佛掘金.md" title="wikilink">丹佛掘金</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/前锋.md" title="wikilink">前锋</a></p></td>
<td></td>
<td><p><a href="../Page/华盛顿奇才.md" title="wikilink">华盛顿奇才</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/达纳·巴罗斯.md" title="wikilink">达纳·巴罗斯</a></p></td>
<td><p><a href="../Page/后卫.md" title="wikilink">后卫</a></p></td>
<td></td>
<td><p><a href="../Page/费城76人.md" title="wikilink">费城76人</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/乔治·穆雷桑.md" title="wikilink">乔治·穆雷桑</a></p></td>
<td><p><a href="../Page/中锋_(篮球).md" title="wikilink">中锋</a></p></td>
<td></td>
<td><p><a href="../Page/华盛顿奇才.md" title="wikilink">华盛顿奇才</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/中锋_(篮球).md" title="wikilink">中锋</a></p></td>
<td></td>
<td><p><a href="../Page/迈阿密热火.md" title="wikilink">迈阿密热火</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/阿兰·亨德森.md" title="wikilink">阿兰·亨德森</a></p></td>
<td><p><a href="../Page/前锋.md" title="wikilink">前锋</a></p></td>
<td></td>
<td><p><a href="../Page/亚特兰大老鹰.md" title="wikilink">亚特兰大老鹰</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/达雷尔·阿姆斯特朗.md" title="wikilink">达雷尔·阿姆斯特朗</a></p></td>
<td><p><a href="../Page/后卫.md" title="wikilink">后卫</a></p></td>
<td></td>
<td><p><a href="../Page/奥兰多魔术.md" title="wikilink">奥兰多魔术</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/傑倫·羅斯.md" title="wikilink">傑倫·羅斯</a></p></td>
<td><p><a href="../Page/后卫.md" title="wikilink">后卫</a>/<a href="../Page/前锋.md" title="wikilink">前锋</a></p></td>
<td></td>
<td><p><a href="../Page/印第安纳步行者.md" title="wikilink">印第安纳步行者</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/崔西·麥葛雷迪.md" title="wikilink">崔西·麥葛雷迪</a></p></td>
<td><p><a href="../Page/后卫.md" title="wikilink">后卫</a>/<a href="../Page/前锋.md" title="wikilink">前锋</a></p></td>
<td></td>
<td><p><a href="../Page/奥兰多魔术.md" title="wikilink">奥兰多魔术</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/杰梅因·奥尼尔.md" title="wikilink">杰梅因·奥尼尔</a></p></td>
<td><p><a href="../Page/前锋.md" title="wikilink">前锋</a>/<a href="../Page/中锋_(篮球).md" title="wikilink">中锋</a></p></td>
<td></td>
<td><p><a href="../Page/印第安纳步行者.md" title="wikilink">印第安纳步行者</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/吉尔伯特·阿里纳斯.md" title="wikilink">吉尔伯特·阿里纳斯</a></p></td>
<td><p><a href="../Page/后卫.md" title="wikilink">后卫</a></p></td>
<td></td>
<td><p><a href="../Page/金州勇士.md" title="wikilink">金州勇士</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/扎克·兰多夫.md" title="wikilink">扎克·兰多夫</a>*</p></td>
<td><p><a href="../Page/前锋.md" title="wikilink">前锋</a></p></td>
<td></td>
<td><p><a href="../Page/波特兰开拓者.md" title="wikilink">波特兰开拓者</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/鲍比·西蒙斯.md" title="wikilink">鲍比·西蒙斯</a></p></td>
<td><p><a href="../Page/后卫.md" title="wikilink">后卫</a>/<a href="../Page/前锋.md" title="wikilink">前锋</a></p></td>
<td></td>
<td><p><a href="../Page/洛杉矶快船.md" title="wikilink">洛杉矶快船</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/伯利斯·迪奥.md" title="wikilink">伯利斯·迪奥</a></p></td>
<td><p><a href="../Page/前锋.md" title="wikilink">前锋</a></p></td>
<td></td>
<td><p><a href="../Page/菲尼克斯太阳.md" title="wikilink">菲尼克斯太阳</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/蒙塔·艾利斯.md" title="wikilink">蒙塔·艾利斯</a></p></td>
<td><p><a href="../Page/后卫.md" title="wikilink">后卫</a></p></td>
<td></td>
<td><p><a href="../Page/金州勇士.md" title="wikilink">金州勇士</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/希达耶特·特克鲁.md" title="wikilink">希达耶特·特克鲁</a></p></td>
<td><p><a href="../Page/前锋.md" title="wikilink">前锋</a></p></td>
<td></td>
<td><p><a href="../Page/奥兰多魔术.md" title="wikilink">奥兰多魔术</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/丹尼·格兰杰.md" title="wikilink">丹尼·格兰杰</a></p></td>
<td><p><a href="../Page/前锋.md" title="wikilink">前锋</a></p></td>
<td></td>
<td><p><a href="../Page/印第安纳步行者.md" title="wikilink">印第安纳步行者</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/阿伦·布鲁克斯.md" title="wikilink">阿伦·布鲁克斯</a></p></td>
<td><p><a href="../Page/后卫.md" title="wikilink">后卫</a></p></td>
<td></td>
<td><p><a href="../Page/休斯敦火箭.md" title="wikilink">休斯敦火箭</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/凯文·洛夫.md" title="wikilink">凯文·洛夫</a></p></td>
<td><p><a href="../Page/前锋.md" title="wikilink">前锋</a></p></td>
<td></td>
<td><p><a href="../Page/明尼苏达森林狼.md" title="wikilink">明尼苏达森林狼</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/莱恩·安德森.md" title="wikilink">莱恩·安德森</a></p></td>
<td><p><a href="../Page/前锋.md" title="wikilink">前锋</a></p></td>
<td></td>
<td><p><a href="../Page/奥兰多魔术.md" title="wikilink">奥兰多魔术</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/保罗·乔治.md" title="wikilink">保罗·乔治</a></p></td>
<td><p><a href="../Page/前锋.md" title="wikilink">前锋</a></p></td>
<td></td>
<td><p><a href="../Page/印第安纳步行者.md" title="wikilink">印第安纳步行者</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/戈倫·卓吉奇.md" title="wikilink">戈倫·卓吉奇</a></p></td>
<td><p><a href="../Page/后卫.md" title="wikilink">后卫</a></p></td>
<td></td>
<td><p><a href="../Page/菲尼克斯太阳.md" title="wikilink">菲尼克斯太阳</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/吉米·巴特勒.md" title="wikilink">吉米·巴特勒</a></p></td>
<td><p><a href="../Page/后卫.md" title="wikilink">后卫</a></p></td>
<td></td>
<td><p><a href="../Page/芝加哥公牛.md" title="wikilink">芝加哥公牛</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/C·J·麥凱倫.md" title="wikilink">C·J·麥凱倫</a></p></td>
<td><p><a href="../Page/后卫.md" title="wikilink">后卫</a></p></td>
<td></td>
<td><p><a href="../Page/波特蘭開拓者.md" title="wikilink">波特蘭開拓者</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/揚尼斯·安戴托昆波.md" title="wikilink">揚尼斯·安戴托昆波</a></p></td>
<td><p><a href="../Page/前鋒.md" title="wikilink">前鋒</a></p></td>
<td></td>
<td><p><a href="../Page/密爾瓦基公鹿.md" title="wikilink">密爾瓦基公鹿</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/维克多·歐拉迪波.md" title="wikilink">维克多·歐拉迪波</a></p></td>
<td><p><a href="../Page/後衛.md" title="wikilink">後衛</a></p></td>
<td></td>
<td><p><a href="../Page/印第安那溜馬.md" title="wikilink">印第安那溜馬</a></p></td>
</tr>
</tbody>
</table>

## 注释

## 参考资料

  - 整體

<!-- end list -->

  -
  -
<!-- end list -->

  - 細節

[Category:NBA獎項](../Category/NBA獎項.md "wikilink")
[Category:1986年建立的奖项](../Category/1986年建立的奖项.md "wikilink")

1.