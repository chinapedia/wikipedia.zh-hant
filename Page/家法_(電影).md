，是1979年2月22日首影的[香港電影](../Page/香港電影.md "wikilink")、[港產片](../Page/港產片.md "wikilink")，由[鄧光榮自](../Page/鄧光榮.md "wikilink")[編劇自](../Page/編劇.md "wikilink")[導演的](../Page/導演.md "wikilink")[電影](../Page/電影.md "wikilink")，內容屬[故事片](../Page/故事片.md "wikilink")、[動作片](../Page/動作片.md "wikilink")、[愛情片綜合](../Page/愛情片.md "wikilink")。在1979年的[香港票房](../Page/香港票房.md "wikilink")，《家法》位列第14位，當年[收入](../Page/收入.md "wikilink")2,400,299港元。

## 劇情

[富豪洪天蹤](../Page/富豪.md "wikilink")，[年齡](../Page/年齡.md "wikilink")60，育有四名[兒子](../Page/兒子.md "wikilink")，長子是洪英，[父親有意將家業交予](../Page/父親.md "wikilink")[繼承](../Page/繼承.md "wikilink")，特在[祖先牌前定下](../Page/祖先.md "wikilink")[家法](../Page/家規.md "wikilink")，要四子遵守，以和為貴，同心協力，振興[世襲家業](../Page/世襲.md "wikilink")。在洪天蹤大壽[筵席間](../Page/宴會.md "wikilink")，對頭人顧雄武不速來客，重提恩怨舊仇恨。

## 演員

  - [任達華](../Page/任達華.md "wikilink")
  - 鄧光榮
  - [羅文](../Page/羅文.md "wikilink")，[客串](../Page/客串.md "wikilink")
  - [梁小玲](../Page/梁小玲.md "wikilink")

## 外部参考

  -
  -
  -
  -
  - [1979年香港電影《家法》](https://web.archive.org/web/20070328001217/http://bjcnc.megajoy.com/film/vod_detail.aspx?Channel_Id=1&Program_Id=24572)

  - [鄧光榮讚執著、堅持](http://paper.wenweipo.com/2002/10/20/EN0210200008.htm)，香港《[文汇报](../Page/文汇报_\(香港\).md "wikilink")》，2002-10-20。

[Category:香港動作片](../Category/香港動作片.md "wikilink")
[Category:香港劇情片](../Category/香港劇情片.md "wikilink")
[9](../Category/1970年代香港電影作品.md "wikilink")