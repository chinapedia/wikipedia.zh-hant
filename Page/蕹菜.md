**蕹菜**（[學名](../Page/學名.md "wikilink")：），又称**空心菜**、**通菜**、**通心菜**、**壅菜**、**甕菜**、**應菜**、**藤菜**、**瓮菜**及**葛菜**，为一年生[草本植物](../Page/草本植物.md "wikilink")，[茎蔓性](../Page/茎.md "wikilink")，中空，节上能生不定根。

## 別名

應菜，蕹菜(南方草木狀、嘉祐本草、植物名實圖考)，空心菜(福建、廣西、貴州、四川)，通菜蓊、蓊菜(福建)，藤藤菜(江蘇、四川)，通菜(廣東)，甕蔡、無心菜、藤菜、竹葉菜、葛菜、草菜、蕻菜、蓊菜(因有不同區域之品種而有不同)。

## 臺灣分布

台灣本島各地，生長於水田、溝渠、溪畔及沼澤地。

## 特徵

其莖長，中空。葉披針形，長，寬。開白色喇叭狀花，直徑，可用扦插或播種的方式繁殖。\[1\]

蕹菜原产[東南亞](../Page/東南亞.md "wikilink")，但目前主要分布于[中国](../Page/中国.md "wikilink")[长江以南](../Page/长江.md "wikilink")[温热带地区](../Page/温热带.md "wikilink")。在水田、池沼种的称为「**水蕹菜**」，葉、藤管比旱地种植的大。

蕹菜的[叶呈](../Page/叶.md "wikilink")[心形](../Page/心脏.md "wikilink")，叶柄很长；夏秋季开白色或淡紫色喇叭狀[花](../Page/花.md "wikilink")；种子淡褐或黑色；蕹菜性喜温暖湿润，生长势强，最大特点是耐涝耐炎热。在15℃—40℃条件下均能生长，耐连作。对土壤要求不严，适应性广，无论旱地水田，沟边地角都可栽植。夏季炎热高温仍能生长，但不耐寒，遇霜茎叶枯死，高温无霜地区可终年栽培。

蕹菜按照繁殖方式分为[子菜和藤蕹两类](../Page/子菜.md "wikilink")。栽培方式依地势不同分为旱地栽培、水生栽培和浮生栽培（或称深水栽培）三种。

## 利用

### 食用蔬菜

[Ipomoea_aquatica_cooked.jpg](https://zh.wikipedia.org/wiki/File:Ipomoea_aquatica_cooked.jpg "fig:Ipomoea_aquatica_cooked.jpg")
以嫩[莖](../Page/莖.md "wikilink")、綠[葉來](../Page/葉.md "wikilink")[炒食或](../Page/炒.md "wikilink")[煮](../Page/煮.md "wikilink")[湯](../Page/湯.md "wikilink")，作為[夏](../Page/夏天.md "wikilink")[秋高溫季節的主要](../Page/秋天.md "wikilink")[菜餚之一](../Page/菜餚.md "wikilink")。普通家庭或[餐館炒蕹菜](../Page/餐館.md "wikilink")，常常以[蒜頭或](../Page/蒜頭.md "wikilink")[鹽和](../Page/鹽.md "wikilink")[醬油進行調味](../Page/醬油.md "wikilink")，偶爾加入[牛肉](../Page/牛肉.md "wikilink")。當中常見菜色包括盛行於[粵](../Page/廣東.md "wikilink")[港](../Page/香港.md "wikilink")[澳地區的](../Page/澳門.md "wikilink")[椒絲腐乳通菜以及流行於](../Page/椒絲腐乳通菜.md "wikilink")[星](../Page/新加坡.md "wikilink")[馬兩地的](../Page/馬來西亞.md "wikilink")「馬來風光」（即[辣炒通心菜](../Page/辣椒.md "wikilink")）。

## 参考文献

## 外部連結

  - [蕹菜
    Wengcai](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D01175)
    藥用植物圖像數據庫（香港浸會大學中醫藥學院）

  -
[蕹菜](../Category/番薯属.md "wikilink")
[Category:葉菜類](../Category/葉菜類.md "wikilink")
[Category:1775年描述的植物](../Category/1775年描述的植物.md "wikilink")

1.