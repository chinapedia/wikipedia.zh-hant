**天主教曉明女子高級中學**（政府登記**臺中市私立曉明女子高級中學**）（英語：Stella Matutina Girls' High
School），簡稱**曉明女中**、**曉明**，是一所由[聖母聖心修女會於](../Page/聖母聖心傳教修女會.md "wikilink")1963年在[臺灣臺中創立的](../Page/臺灣.md "wikilink")[天主教](../Page/天主教.md "wikilink")[私立](../Page/私立学校.md "wikilink")[完全中學](../Page/完全中學.md "wikilink")。該校現設有[高中部](../Page/高中.md "wikilink")18班、[國中部](../Page/國中.md "wikilink")30班，以其嚴謹校風著稱於[中臺灣地區](../Page/中臺灣.md "wikilink")，並與[臺中女中為臺中現存唯二的](../Page/臺中女中.md "wikilink")[女子中學](../Page/女子中學.md "wikilink")。

該校創校之初多受[衛道中學提供人員與器材設備上的協助](../Page/衛道中學.md "wikilink")，故與[衛道中學在創校上有淵源而往來密切](../Page/衛道中學.md "wikilink")，常並稱其二校為「衛曉」，且衛道以招收男生為主、曉明則是純女校，因而常合稱作兄妹校。

## 歷史

  - 1932年，[天主教聖母聖心修女會成立於](../Page/天主教聖母聖心修女會.md "wikilink")[中國東北](../Page/中國東北.md "wikilink")[瀋陽](../Page/瀋陽.md "wikilink")。
  - 1948年10月，[祁志英修女率領](../Page/祁志英.md "wikilink")33位聖母聖心修女會經由[南京市](../Page/南京市.md "wikilink")、[上海市](../Page/上海市.md "wikilink")、[基隆市](../Page/基隆市.md "wikilink")、[高雄市等地](../Page/高雄市.md "wikilink")，落腳於[臺中市](../Page/台中市.md "wikilink")。
  - 1963年，**曉明女中**成立，招收國中部學生一百多人。「**曉明**」二字取自其[拉丁文名](../Page/拉丁语.md "wikilink")「**Stella
    Matutina**」，意為「曉明之星」。
  - 1966年，增設高中部。
  - 1973年，增設護理助產合訓科。
  - 1974年，增設高、國中音樂實驗班。
  - 1990年，護理科停招。
  - 2011年，音樂班停招。

### 歷任校長

  - [張春榮](../Page/張春榮.md "wikilink")（1963年8月 — 1975年6月）
  - [朱南子](../Page/朱南子.md "wikilink")（1975年7月 — 1990年7月、1991年8月 —
    1995年7月）
  - [孫臺華](../Page/孫台華.md "wikilink")（1990年8月 — 1991年7月）
  - [劉瑞瓊](../Page/劉瑞瓊.md "wikilink")（1995年7月 — 2006年2月）
  - [朱南子](../Page/朱南子.md "wikilink")（2006年2月 —2017年2月 ）
  - [劉瑞瓊](../Page/劉瑞瓊.md "wikilink")（2017年2月 — ）

## 知名校友

  - [朱少麟](../Page/朱少麟.md "wikilink")：文學作家，著有《傷心咖啡店之歌》、《燕子》、《地底三萬呎》等書。
  - [何欣穗](../Page/何欣穗.md "wikilink")：創作型歌手、DJ。
  - [范瑋琪](../Page/范瑋琪.md "wikilink")：初中部三年級時移民美國,現為歌手、主持人。
  - [應曉薇](../Page/應曉薇.md "wikilink")：演員、臺北市中正萬華區議員。
  - 瑪法達：星座作家。
  - [蔡銀娟](../Page/蔡銀娟.md "wikilink")：《夏綠蒂的愛情習題、我的32個臉孔》作者、電影《候鳥來的季節》導演。
  - [林嘉俐](../Page/林嘉俐.md "wikilink")：演員，2004年獲金鐘獎「戲劇類女配角獎」。
  - [安唯綾](../Page/安唯綾.md "wikilink")：三立財經新聞主播。
  - [楊又穎](../Page/楊又穎.md "wikilink")：演員、《愛玩咖》主持人、模特兒，曾是《大學生了沒》班底。
  - [張麗善](../Page/張麗善.md "wikilink")：現任雲林縣縣長，前立法委員。
  - [王加佳：前臺中縣議員](../Page/王加佳.md "wikilink")。
  - [詹璇依](../Page/詹璇依.md "wikilink")：三立iNEWS主播。
  - [房業涵](../Page/房業涵.md "wikilink")：華視新聞主播。
  - [鄭凱云](../Page/鄭凱云.md "wikilink")：民國81年初中部畢業,TVBS台主播及主持人。
  - [黃光芹](../Page/黃光芹.md "wikilink")：政治評論名嘴,廣播節目主持人。
  - [林穎孟](../Page/林穎孟.md "wikilink")：時代力量發言人。

## 交通

  - [臺中市公車](../Page/臺中市公車.md "wikilink")

**曉明女中**(中清路)

  - [臺中客運](../Page/台中客運.md "wikilink")：[6](../Page/台中市公車6路.md "wikilink")、[9](../Page/台中市公車9路.md "wikilink")、[29](../Page/台中市公車29路.md "wikilink")、[33](../Page/台中市公車33路.md "wikilink")、[101](../Page/台中市公車101路.md "wikilink")、[108](../Page/台中市公車108路.md "wikilink")、[154](../Page/台中市公車154路.md "wikilink")、[500](../Page/台中市公車500路.md "wikilink")
  - [統聯客運](../Page/統聯客運.md "wikilink")：[61](../Page/台中市公車61路.md "wikilink")、[500](../Page/台中市公車500路.md "wikilink")

**曉明女中**(漢口路)

  - [東南客運](../Page/東南客運.md "wikilink")：[67](../Page/台中市公車67路.md "wikilink")
  - [豐榮客運](../Page/豐榮客運.md "wikilink")：[127](../Page/台中市公車127路.md "wikilink")

## 參考資料

  - [財團法人曉明文教基金會](http://203.72.96.2/st9/htm/)

## 相關條目

  - [天主教臺中教區](../Page/天主教台中教區.md "wikilink")

## 外部連結

  - [曉明女中](http://www.smgsh.tc.edu.tw/)

[天](../Category/臺中市私立高級中學.md "wikilink")
[中](../Category/台灣天主教會學校.md "wikilink")
[Category:台灣女子學校](../Category/台灣女子學校.md "wikilink")
[Category:北區 (臺中市)](../Category/北區_\(臺中市\).md "wikilink")
[Category:1963年創建的教育機構](../Category/1963年創建的教育機構.md "wikilink")
[私](../Category/台中市完全中學.md "wikilink")