**虛擬系統**，也常被稱為**影子系統**，是一種[電腦軟體](../Page/電腦軟體.md "wikilink")，可以在現有的[作業系統上虛擬出一個相同的環境](../Page/作業系統.md "wikilink")，並在該虛擬環境中執行[應用程式](../Page/應用程式.md "wikilink")，而所有存取與改變系統的活動將會被限制在該環境下，意即虛擬系統與實體系統是隔離的，虛擬系統中的活動不會造成實體系統的改變。

虛擬系統和[虛擬機的不同在於](../Page/虛擬機.md "wikilink")：虛擬系統只能模擬和現有作業系統相同的環境，而虛擬機則可以模擬出其他種類的作業系統。而且虛擬機需要模擬底層的[硬體指令](../Page/硬體.md "wikilink")，所以在應用程式執行速度上比虛擬系統慢得多。

## 虛擬系統列表

<table>
<thead>
<tr class="header">
<th><p>名稱</p></th>
<th><p>官方網站</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/BufferZone.md" title="wikilink">BufferZone</a></p></td>
<td><p><a href="http://www.trustware.com">http://www.trustware.com</a></p></td>
<td><p><a href="../Page/免費軟體.md" title="wikilink">免費軟體</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/PowerShadow.md" title="wikilink">PowerShadow</a><br />
（中文名：影子系統）</p></td>
<td><p><a href="http://www.yingzixitong.cn">http://www.yingzixitong.cn</a></p></td>
<td><p>免費軟體</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Returnil_Virtual_System.md" title="wikilink">Returnil Virtual System</a></p></td>
<td><p><a href="https://web.archive.org/web/20160520043818/http://www.returnilvirtualsystem.com/">https://web.archive.org/web/20160520043818/http://www.returnilvirtualsystem.com/</a></p></td>
<td><p>商業軟體</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/ShadowUser.md" title="wikilink">ShadowUser</a></p></td>
<td><p><a href="http://www.storagecraft.com/products/ShadowUser/">http://www.storagecraft.com/products/ShadowUser/</a></p></td>
<td><p>商業軟體</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Shadow_Defender.md" title="wikilink">Shadow Defender</a></p></td>
<td><p><a href="http://www.shadowdefender.com/">http://www.shadowdefender.com/</a></p></td>
<td><p>商業軟體</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/prayaya_v3.md" title="wikilink">prayaya v3</a></p></td>
<td><p><a href="http://www.prayaya.com/">http://www.prayaya.com/</a></p></td>
<td><p>商業軟體</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Sandboxie.md" title="wikilink">Sandboxie</a></p></td>
<td><p><a href="http://www.sandboxie.com">http://www.sandboxie.com</a></p></td>
<td><p>共享軟件（可以永久免費使用，但相比收費的註冊版會有一些功能限制）</p></td>
</tr>
</tbody>
</table>

## 参见

  - [虚拟化](../Page/虚拟化.md "wikilink")
  - [虚拟机](../Page/虚拟机.md "wikilink")

[\*](../Category/虚拟化软件.md "wikilink")
[Category:電腦安全](../Category/電腦安全.md "wikilink")