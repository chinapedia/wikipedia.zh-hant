[Robert-michels.jpg](https://zh.wikipedia.org/wiki/File:Robert-michels.jpg "fig:Robert-michels.jpg")
**羅伯特·米契爾斯**（，，），生于[德国](../Page/德国.md "wikilink")[科隆](../Page/科隆.md "wikilink")，卒于[意大利](../Page/意大利.md "wikilink")[罗马](../Page/罗马.md "wikilink")，是一位着力于描述精英的政治行为并对精英理论有贡献的德国[社会学家](../Page/社会学.md "wikilink")。他最出名的作品是出版于1911年的《政党政治》，在该书中，米歇尔斯阐述了“[寡頭鐵律](../Page/寡頭鐵律.md "wikilink")”。米歇尔斯是[马克斯·韦伯的一位学生](../Page/马克斯·韦伯.md "wikilink")，同时也是[维尔纳·桑巴特和](../Page/维尔纳·桑巴特.md "wikilink")[阿基利·劳瑞亚的好友及信徒](../Page/阿基利·劳瑞亚.md "wikilink")。在政党政治上，米歇尔斯从[德国社会民主党转向了](../Page/德国社会民主党.md "wikilink")[意大利社会党](../Page/意大利社会党.md "wikilink")，并且属于支持意大利革命工团主义的分支。随后，米歇尔斯认为[法西斯主义代表了一种更加民主的社会主义](../Page/法西斯主义.md "wikilink")，因而转向了意大利法西斯主义。米歇尔斯的思想为当今的缓和理论的基础，这种理论描述了激进政治团体融入既存政治体系的过程。

## 生平

米歇尔斯生于一个富有的德国家庭，先后在[英国](../Page/英国.md "wikilink")，[法国](../Page/法国.md "wikilink")[巴黎](../Page/巴黎.md "wikilink")（[索邦大学](../Page/索邦大学.md "wikilink")）、德国[莱比锡](../Page/莱比锡.md "wikilink")（[慕尼黑大学](../Page/慕尼黑大学.md "wikilink")）、德国[哈雷以及意大利](../Page/哈雷.md "wikilink")[都灵求学](../Page/都灵.md "wikilink")。米歇尔斯在[马尔堡大学执教的过程中变成了一位社会主义者](../Page/马尔堡大学.md "wikilink")，并且在德国社会民主党的激进分支内部活跃积极，但他于1907年离开了德国社会民主党。

今天，米歇尔斯被认为是马克斯·韦伯的一位聪明学生。在二十世纪早期，米歇尔斯就在历史以及社会学研究领域取得了国际声望。1911年，他出版了《论现代民主中政党的社会学：政治团体中寡头倾向的研究》，在这方面研究中，米歇尔斯证明了包括社会主义政党在内的政党是不可能实现民主的，这是因为政党会很快演变成寡头官僚组织。在意大利，他被与“意大利革命工团主义”联系起来，这是一个意大利社会党中的左翼分支。

米歇尔斯批判了[卡尔·马克思的](../Page/卡尔·马克思.md "wikilink")[唯物主义](../Page/唯物主义.md "wikilink")[決定論](../Page/決定論.md "wikilink")。相较于卡尔·马克思，米歇尔斯的社会主义研究借鉴于维尔纳·桑巴特的历史研究方法，因而是更加经验主义的。因为米歇尔斯崇拜意大利文化并且在社会科学领域取得的成就非常突出，他引起了[路易吉·伊諾第和阿基利](../Page/路易吉·伊諾第.md "wikilink")·劳瑞亚的注意。他们帮助米歇尔斯在[都灵大学成功地获取了一个教授席位](../Page/都灵大学.md "wikilink")。直至1914年，米歇尔斯在都灵大学教授经济学、政治科学以及社会经济学。1914年之后，他成为了瑞士[巴塞尔大学的经济学教授](../Page/巴塞尔大学.md "wikilink")，直到1926年。

在[第一次世界大战之后](../Page/第一次世界大战.md "wikilink")，米歇尔斯加入了由[墨索里尼](../Page/贝尼托·墨索里尼.md "wikilink")（意大利社会党的前领导）领导的意大利法西斯党。米歇尔斯相信墨索里尼的个人魅力和劳工阶级之间的直接联系是实现没有官僚阶层的下层阶级掌权的真正途径。他在意大利[佩鲁贾大学担任经济学与历史学教授](../Page/佩鲁贾大学.md "wikilink")，并不时地在罗马讲授课程，直至1936年去世。

## 外部連結

  - [Robert
    Michels生平](http://www.bookrags.com/biography-robert-michels/)
  - [Political
    Parties](http://socserv.mcmaster.ca/econ/ugcm/3ll3/michels/polipart.pdf)，《Political
    Parties》的[PDF版本](../Page/PDF.md "wikilink")
  - [Books and Articles on: Robert
    Michels](http://www.questia.com/library/sociology-and-anthropology/sociologists-and-anthropologists/robert-michels.jsp)，[Questia](../Page/Questia.md "wikilink")

[Category:德国社会学家](../Category/德国社会学家.md "wikilink")
[Category:德國社會主義者](../Category/德國社會主義者.md "wikilink")
[Category:義大利法西斯主義者](../Category/義大利法西斯主義者.md "wikilink")
[Category:科隆人](../Category/科隆人.md "wikilink")
[Category:巴塞爾大學教師](../Category/巴塞爾大學教師.md "wikilink")
[Category:馬爾堡大學教師](../Category/馬爾堡大學教師.md "wikilink")
[Category:哈雷大学校友](../Category/哈雷大学校友.md "wikilink")
[Category:萊比錫大學校友](../Category/萊比錫大學校友.md "wikilink")
[Category:慕尼黑大學校友](../Category/慕尼黑大學校友.md "wikilink")