**露薏絲·布朗**（，）生於[英國](../Page/英國.md "wikilink")[奥海姆](../Page/奧爾德姆.md "wikilink")（Oldham），是全球第一名[試管嬰兒](../Page/試管嬰兒.md "wikilink")。莱斯利·布朗（）和约翰·布朗（）是她的父母。因[輸卵管受阻](../Page/輸卵管.md "wikilink")，萊斯利·布朗試了九年，都無法自然懷孕。1977年11月10日，[帕特里克·斯特普托和](../Page/帕特里克·斯特普托.md "wikilink")[罗伯特·G·爱德华兹兩位醫師為莱斯利](../Page/罗伯特·G·爱德华兹.md "wikilink")·布朗進行人工受精手術。雖然莱斯利·布朗和约翰·布朗知道這項手術仍在實驗階段，但當時無人告訴他們仍未有人接受人工受精手術後誕下嬰兒。露薏絲·布朗在晚上11時47分於Oldham
General
Hospital出生，生產過程採取預定的剖腹生產。出生時的體重5磅12盎司（約2.608公斤），生產過程有完整地記錄。妹妹Natalie也是試管嬰兒。她曾在[布里斯托的幼兒中心任職保育員](../Page/布里斯托.md "wikilink")。現在她在郵局工作。她和Wesley
Mullinder於2004年9月4日結婚。2006年12月20日，她誕下一個男嬰\[1\]，該男嬰是自然受孕的\[2\]。其母莱斯利·布朗在2012年6月6日逝世，享年64歲。\[3\]

## 參考資料

<div class="references-small">

<references/>

</div>

## 外部連結

  - [BBC關於露薏絲布朗的英文人物簡介
    (2003年7月)](http://news.bbc.co.uk/1/hi/health/3091241.stm)

[he:הפריה
חוץ-גופית\#היסטוריה](../Page/he:הפריה_חוץ-גופית#היסטוריה.md "wikilink")

[Category:英国人](../Category/英国人.md "wikilink")
[Category:生物學史](../Category/生物學史.md "wikilink")

1.
2.
3.