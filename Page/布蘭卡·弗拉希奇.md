**布蘭卡·弗拉希奇**（，）是[克羅地亞女](../Page/克羅地亞.md "wikilink")[跳高](../Page/跳高.md "wikilink")[運動員](../Page/運動員.md "wikilink")，曾獲得[2008年奥运会](../Page/2008年奥运会.md "wikilink")[银牌和](../Page/银牌.md "wikilink")[2016年奥运会](../Page/2016年奥运会.md "wikilink")[铜牌](../Page/铜牌.md "wikilink")、2007年和[2009年世界田徑錦標賽](../Page/2009年世界田徑錦標賽.md "wikilink")[金牌两枚](../Page/金牌.md "wikilink")、2008和[2010年世界室內田徑錦標賽金牌两枚](../Page/2010年世界室內田徑錦標賽.md "wikilink")。她在2007年8月7日跳出2.07[米](../Page/公尺.md "wikilink")，是克羅地亞的最高記錄。

目前她以2.08米（2009年8月创造）位列女子跳高历史最佳成绩第二位，仅次于[保加利亚](../Page/保加利亚.md "wikilink")[Stefka
Kostadinova创造的](../Page/Stefka_Kostadinova.md "wikilink")2.09米。\[1\]\[2\]

## 生平

她在1983年於克羅地亞[斯普利特-達爾馬提亞縣](../Page/斯普利特-達爾馬提亞縣.md "wikilink")[斯普利特出生](../Page/斯普利特.md "wikilink")。

## 参考资料

## 外部链接

  - [Blanka Vlašić's official web
    page](https://web.archive.org/web/20090611224908/http://www.blanka-vlasic.hr/index-en.php)

  -
  - [SPIKES Hero profile on
    www.spikesmag.com](https://web.archive.org/web/20080714175204/http://www.spikesmag.com/athletes/Heroes/blankavlasic.aspx)

  - [Blanka Vlašić's
    Pictures](http://atlechic.webcindario.com/blanka_vlasic.htm)

  -
[Category:克羅地亞田径運動員](../Category/克羅地亞田径運動員.md "wikilink")
[Category:跳高運動員](../Category/跳高運動員.md "wikilink")
[Category:世界田徑錦標賽獎牌得主](../Category/世界田徑錦標賽獎牌得主.md "wikilink")
[Category:女子跳高运动员](../Category/女子跳高运动员.md "wikilink")
[Category:克罗地亚奥林匹克运动会银牌得主](../Category/克罗地亚奥林匹克运动会银牌得主.md "wikilink")
[Category:克罗地亚奥林匹克运动会铜牌得主](../Category/克罗地亚奥林匹克运动会铜牌得主.md "wikilink")
[Category:2008年夏季奧林匹克運動會獎牌得主](../Category/2008年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:2016年夏季奥林匹克运动会奖牌得主](../Category/2016年夏季奥林匹克运动会奖牌得主.md "wikilink")
[Category:2000年夏季奧林匹克運動會田徑運動員](../Category/2000年夏季奧林匹克運動會田徑運動員.md "wikilink")
[Category:2004年夏季奧林匹克運動會田徑運動員](../Category/2004年夏季奧林匹克運動會田徑運動員.md "wikilink")
[Category:2008年夏季奧林匹克運動會田徑運動員](../Category/2008年夏季奧林匹克運動會田徑運動員.md "wikilink")
[Category:2016年夏季奧林匹克運動會田徑運動員](../Category/2016年夏季奧林匹克運動會田徑運動員.md "wikilink")
[Category:奥林匹克运动会田径银牌得主](../Category/奥林匹克运动会田径银牌得主.md "wikilink")
[Category:奥林匹克运动会田径铜牌得主](../Category/奥林匹克运动会田径铜牌得主.md "wikilink")

1.  [High Jump All Time
    (indoors)](http://www.iaaf.org/statistics/toplists/inout=i/age=n/season=0/sex=W/all=y/legal=A/disc=HJ/detail.html).
    [IAAF](../Page/IAAF.md "wikilink") (2009-09-01). Retrieved on
    2009-09-01.
2.  [High Jump All Time
    (outdoors)](http://www.iaaf.org/statistics/toplists/inout=o/age=n/season=0/sex=W/all=y/legal=A/disc=HJ/detail.html).
    [IAAF](../Page/IAAF.md "wikilink") (2009-09-01). Retrieved on
    2009-09-01.