**濱名湖**為位于[日本](../Page/日本.md "wikilink")[静岡縣](../Page/静岡縣.md "wikilink")[濱松市](../Page/濱松市.md "wikilink")、[湖西市的](../Page/湖西市.md "wikilink")[潟湖](../Page/潟湖.md "wikilink")。南部與海相通，為日本第十大湖。面積65平方公里，湖周長114公里，位居日本第二。最大水深16.6米，平均水深4.8米。2016年3月，與[台灣](../Page/台灣.md "wikilink")[南投縣](../Page/南投縣.md "wikilink")[魚池鄉的](../Page/魚池鄉.md "wikilink")[日月潭締結為姐妹湖](../Page/日月潭.md "wikilink")\[1\]。

## 參考資料

[Category:日本湖泊](../Category/日本湖泊.md "wikilink") [Category:北區
(濱松市)](../Category/北區_\(濱松市\).md "wikilink")
[Category:湖西市](../Category/湖西市.md "wikilink")
[Category:遠江國](../Category/遠江國.md "wikilink")
[Category:潟湖](../Category/潟湖.md "wikilink")
[Category:靜岡縣地理](../Category/靜岡縣地理.md "wikilink") [Category:西區
(濱松市)](../Category/西區_\(濱松市\).md "wikilink")

1.