**4月29日**是[公历一年中的第](../Page/公历.md "wikilink")119天（[闰年第](../Page/闰年.md "wikilink")120天），离全年的结束还有246天。

## 大事记

### 9世紀

  - [837年](../Page/837年.md "wikilink")：中国《[文献通考](../Page/文献通考.md "wikilink")》客星栏记录到一颗发生在[双子座的](../Page/双子座.md "wikilink")[新星](../Page/新星.md "wikilink")。

### 15世紀

  - [1429年](../Page/1429年.md "wikilink")：[圣女贞德率领军队到达](../Page/圣女贞德.md "wikilink")[奥尔良](../Page/奥尔良.md "wikilink")，开始解除[奥尔良之围](../Page/奥尔良之围.md "wikilink")。

### 17世紀

  - [1672年](../Page/1672年.md "wikilink")：法王[路易十四入侵](../Page/路易十四.md "wikilink")[荷蘭](../Page/荷蘭.md "wikilink")。

### 18世紀

  - [1770年](../Page/1770年.md "wikilink")：[英国航海家](../Page/英国.md "wikilink")[詹姆斯·库克率领](../Page/詹姆斯·库克.md "wikilink")[努力号帆船的船员在](../Page/努力号.md "wikilink")[澳大利亚东海岸登陆](../Page/澳大利亚.md "wikilink")，成为首批到达澳大利亚的[欧洲人](../Page/欧洲.md "wikilink")。

### 19世紀

  - [1882年](../Page/1882年.md "wikilink")：[德国发明家](../Page/德国.md "wikilink")[维尔纳·冯·西门子在](../Page/维尔纳·冯·西门子.md "wikilink")[柏林首次公开展示了他发明的世界首辆](../Page/柏林.md "wikilink")[无轨电车](../Page/无轨电车.md "wikilink")。
  - [1900年](../Page/1900年.md "wikilink")：[賓夕法尼亞州州旗啟用](../Page/賓夕法尼亞州州旗.md "wikilink")。

### 20世紀

  - [1929年](../Page/1929年.md "wikilink")：[蘇聯進行第一次](../Page/蘇聯.md "wikilink")[五年计划](../Page/苏联五年计划.md "wikilink")。
  - [1932年](../Page/1932年.md "wikilink")：[韩国青年](../Page/韩国.md "wikilink")[尹奉吉在](../Page/尹奉吉.md "wikilink")[上海用炸弹刺杀](../Page/上海.md "wikilink")[日本上海占领军总司令](../Page/日本.md "wikilink")[白川义则等人](../Page/白川义则.md "wikilink")，[虹口公园爆炸案发生](../Page/虹口公园爆炸案.md "wikilink")。
  - [1945年](../Page/1945年.md "wikilink")：[二戰](../Page/二戰.md "wikilink")：[意大利的德軍向](../Page/意大利.md "wikilink")[同盟國投降](../Page/同盟國_\(第二次世界大戰\).md "wikilink")。
  - [1946年](../Page/1946年.md "wikilink")：[远东国际军事法庭开庭](../Page/远东国际军事法庭.md "wikilink")。
  - [1975年](../Page/1975年.md "wikilink")：[越戰進入尾聲階段](../Page/越戰.md "wikilink")，美軍在[南越首都](../Page/越南共和国.md "wikilink")[西貢展開名為](../Page/胡志明市.md "wikilink")「[常風行動](../Page/常風行動.md "wikilink")」的大規模[直昇機撤退行動](../Page/直昇機.md "wikilink")，以在[北越及](../Page/北越.md "wikilink")[越共攻入西貢前疏散在越美國人和相關越南籍難民](../Page/越南南方民族解放陣線.md "wikilink")。
  - [1990年](../Page/1990年.md "wikilink")：[香港](../Page/香港.md "wikilink")[沙田白石船民中心有數十名](../Page/沙田_\(香港\).md "wikilink")[越南船民逃走](../Page/越南船民.md "wikilink")。
  - [1992年](../Page/1992年.md "wikilink")：[美国](../Page/美国.md "wikilink")[洛杉矶四名殴打](../Page/洛杉矶.md "wikilink")[黑人](../Page/黑人.md "wikilink")[罗德尼·金的](../Page/罗德尼·金.md "wikilink")[白人警察被判无罪引发了](../Page/白人.md "wikilink")[大规模种族冲突](../Page/1992年洛杉磯暴動.md "wikilink")，导致至少50人死亡。
  - [1997年](../Page/1997年.md "wikilink")：[禁止化學武器公約生效](../Page/禁止化學武器公約.md "wikilink")。
  - 1997年：中国[湖南省](../Page/湖南省.md "wikilink")[岳阳市](../Page/岳阳市.md "wikilink")[荣家湾站发生](../Page/荣家湾站.md "wikilink")[列车相撞事故](../Page/1997年京广铁路荣家湾站列车相撞事故.md "wikilink")，导致126人死亡。

### 21世紀

  - [2004年](../Page/2004年.md "wikilink")：[马其顿](../Page/馬其頓共和國.md "wikilink")[總統选举](../Page/馬其頓總統.md "wikilink")，[茨爾文科夫斯基获胜](../Page/布蘭科·茨爾文科夫斯基.md "wikilink")。
  - [2005年](../Page/2005年.md "wikilink")：[中国共产党中央委员会总书记](../Page/中国共产党.md "wikilink")[胡锦涛与](../Page/胡锦涛.md "wikilink")[中国国民党主席](../Page/中国国民党.md "wikilink")[连战在](../Page/连战.md "wikilink")[北京进行自](../Page/北京.md "wikilink")[国共内战以来两党最高领导人的首次](../Page/国共内战.md "wikilink")[会谈](../Page/2005年中国国民党和平之旅.md "wikilink")。
  - [2009年](../Page/2009年.md "wikilink")：[澳門維基媒體協會刊憲成立](../Page/澳門維基媒體協會.md "wikilink")。
  - [2011年](../Page/2011年.md "wikilink")：[威尔士威廉王子与凯特·米德尔顿的婚礼舉行](../Page/威尔士威廉王子与凯特·米德尔顿的婚礼.md "wikilink")。
  - [2013年](../Page/2013年.md "wikilink")：[赫雪爾太空望遠鏡因為致冷劑耗盡而結束任務](../Page/赫雪爾太空望遠鏡.md "wikilink")。
  - [2016年](../Page/2016年.md "wikilink")：由中资兴建的马来西亚[标志塔](../Page/106交易塔.md "wikilink")（现106交易塔）正式动工，在[吉隆坡118落成之前是马来西亚最高的摩天大楼](../Page/吉隆坡118.md "wikilink")。

## 出生

  - [1137年](../Page/1137年.md "wikilink")：[暲子內親王](../Page/暲子內親王.md "wikilink")，日本[平安時代至](../Page/平安時代.md "wikilink")[鎌倉時代的女性皇族](../Page/鎌倉時代.md "wikilink")（[1211年去世](../Page/1211年.md "wikilink")）
  - [1803年](../Page/1803年.md "wikilink")：[詹姆士·布鲁克](../Page/詹姆士·布鲁克.md "wikilink")，[砂拉越第一位白人](../Page/砂拉越.md "wikilink")[拉者](../Page/拉者.md "wikilink")（[1868年去世](../Page/1868年.md "wikilink")）
  - [1854年](../Page/1854年.md "wikilink")：[昂利·庞加莱](../Page/昂利·庞加莱.md "wikilink")，法国[数学家](../Page/数学家.md "wikilink")，[物理学家和](../Page/物理学家.md "wikilink")[哲学家](../Page/哲学家.md "wikilink")（[1912年去世](../Page/1912年.md "wikilink")）
  - [1876年](../Page/1876年.md "wikilink")：[佐迪圖](../Page/佐迪圖.md "wikilink")，[埃塞俄比亞](../Page/埃塞俄比亞.md "wikilink")[女皇](../Page/女皇.md "wikilink")（[1930年去世](../Page/1930年.md "wikilink")）
  - [1879年](../Page/1879年.md "wikilink")：[汤玛斯·比彻姆](../Page/汤玛斯·比彻姆.md "wikilink")，英国指挥家（[1961年去世](../Page/1961年.md "wikilink")）
  - [1893年](../Page/1893年.md "wikilink")：[哈羅德·尤里](../Page/哈羅德·尤里.md "wikilink")，美國科學家（[1981年去世](../Page/1981年.md "wikilink")）
  - [1901年](../Page/1901年.md "wikilink")：[裕仁](../Page/裕仁.md "wikilink")，[日本天皇](../Page/日本天皇.md "wikilink")（[1989年去世](../Page/1989年.md "wikilink")）
  - [1905年](../Page/1905年.md "wikilink")：[菲利普·史密斯](../Page/菲利普·史密斯.md "wikilink")，美国电子工程师，[史密斯圆图的发明者](../Page/史密斯圆图.md "wikilink")（[1987年去世](../Page/1987年.md "wikilink")）
  - [1922年](../Page/1922年.md "wikilink")：[迪克·苏迪曼](../Page/迪克·苏迪曼.md "wikilink")，[印度尼西亚著名](../Page/印度尼西亚.md "wikilink")[羽毛球运动员](../Page/羽毛球.md "wikilink")，被誉为“印尼羽毛球之父”（[1986年去世](../Page/1986年.md "wikilink")）
  - [1936年](../Page/1936年.md "wikilink")：[祖宾·梅塔](../Page/祖宾·梅塔.md "wikilink")，[印度](../Page/印度.md "wikilink")[犹太人](../Page/犹太人.md "wikilink")[指挥家](../Page/指挥家.md "wikilink")
  - [1944年](../Page/1944年.md "wikilink")：[柳传志](../Page/柳传志.md "wikilink")，中国企业家，[联想集团的创始人](../Page/联想集团.md "wikilink")
  - [1955年](../Page/1955年.md "wikilink")：[蔡惠萍](../Page/蔡惠萍.md "wikilink")，[香港女配音員](../Page/香港.md "wikilink")
  - [1957年](../Page/1957年.md "wikilink")：[-{zh-cn:丹尼尔·戴-刘易斯;
    zh-tw:丹尼爾·戴-路易斯;
    zh-hk:丹尼爾·路易斯;}-](../Page/丹尼爾·戴-劉易斯.md "wikilink")，[英國](../Page/英國.md "wikilink")[演員](../Page/演員.md "wikilink")
  - [1958年](../Page/1958年.md "wikilink")：[-{zh-cn:米歇尔·法伊弗; zh-tw:蜜雪兒·菲佛;
    zh-hk:米雪·菲花;}-](../Page/蜜雪兒·菲佛.md "wikilink")，[美國女](../Page/美國.md "wikilink")[演員](../Page/演員.md "wikilink")
  - [1961年](../Page/1961年.md "wikilink")：[立木文彥](../Page/立木文彥.md "wikilink")，[日本動畫](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")
  - [1963年](../Page/1963年.md "wikilink")：[彭比得](../Page/彭比得.md "wikilink")，[香港演员](../Page/香港.md "wikilink")
  - [1970年](../Page/1970年.md "wikilink")：[奧瑪·花曼](../Page/奧瑪·花曼.md "wikilink")，[美国女电影演员](../Page/美国.md "wikilink")
  - 1970年：[安德烈·阿加西](../Page/安德烈·阿加西.md "wikilink")，美国职业[网球运动员](../Page/网球.md "wikilink")，第一位获得生涯[金满贯的男子單打網球運動員](../Page/金满贯.md "wikilink")
  - [1974年](../Page/1974年.md "wikilink")：[湯美](../Page/湯美.md "wikilink")，[香港](../Page/香港.md "wikilink")[巴西籍足球運動員](../Page/巴西.md "wikilink")
  - [1979年](../Page/1979年.md "wikilink")：[李同國](../Page/李同国.md "wikilink")，[韓國足球運動員](../Page/韓國.md "wikilink")
  - [1980年](../Page/1980年.md "wikilink")：[竹幼婷](../Page/竹幼婷.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[節目主持人](../Page/節目主持人.md "wikilink")
  - [1984年](../Page/1984年.md "wikilink")：[周永恆](../Page/周永恆.md "wikilink")，香港歌手
  - [1985年](../Page/1985年.md "wikilink")：[榎本亜弥子](../Page/榎本亜弥子.md "wikilink")，[日本女演员](../Page/日本.md "wikilink")、[模特兒](../Page/模特兒.md "wikilink")
  - [1986年](../Page/1986年.md "wikilink")：[李倸英](../Page/李倸英.md "wikilink")，[韓國女演員](../Page/韓國.md "wikilink")
  - [1987年](../Page/1987年.md "wikilink")：[江語晨](../Page/江語晨.md "wikilink")，台灣演員
  - [1988年](../Page/1988年.md "wikilink")：[高潤荷](../Page/高潤荷.md "wikilink")，[南韓歌手](../Page/南韓.md "wikilink")
  - 1988年：[許廷鏗](../Page/許廷鏗.md "wikilink")，香港歌手
  - 1988年：[胡桓瑋](../Page/胡桓瑋.md "wikilink")，台灣演員
  - [1992年](../Page/1992年.md "wikilink")：[趙粵](../Page/趙粵.md "wikilink")，中國女子團體[SNH48成員](../Page/SNH48.md "wikilink")
  - [2007年](../Page/2007年.md "wikilink")：[蘇菲亞](../Page/蘇菲亞_\(西班牙公主\).md "wikilink")，[西班牙公主](../Page/西班牙.md "wikilink")

## 逝世

  - [643年](../Page/643年.md "wikilink")：[侯君集](../Page/侯君集.md "wikilink")，[唐朝将领](../Page/唐朝.md "wikilink")。
  - [1931年](../Page/1931年.md "wikilink")：[恽代英](../Page/恽代英.md "wikilink")，[广州起义领导人之一](../Page/广州起义_\(1927年\).md "wikilink")、中国青年的领袖和导师、[中國共產黨早期領導人](../Page/中國共產黨.md "wikilink")。（[1895年出生](../Page/1895年.md "wikilink")）
  - [1950年](../Page/1950年.md "wikilink")：[道格拉斯·麦基尔南](../Page/道格拉斯·麦基尔南.md "wikilink")，美国外交官、间谍。（[1913年出生](../Page/1913年.md "wikilink")）
  - [1951年](../Page/1951年.md "wikilink")：[路德维希·维特根斯坦](../Page/路德维希·维特根斯坦.md "wikilink")，奥地利哲学家、[逻辑学家](../Page/逻辑.md "wikilink")，[分析哲学的代表人物](../Page/分析哲学.md "wikilink")（[1889年出生](../Page/1889年.md "wikilink")）
  - [1968年](../Page/1968年.md "wikilink")：[林昭](../Page/林昭.md "wikilink")，右派分子，民主人士，基督教徒，文革時被處決。（[1932年出生](../Page/1932年.md "wikilink")）
  - [1971年](../Page/1971年.md "wikilink")：[李四光](../Page/李四光.md "wikilink")，中国地质学家（[1889年出生](../Page/1889年.md "wikilink")）
  - [1980年](../Page/1980年.md "wikilink")：[希区柯克](../Page/希区柯克.md "wikilink")，美国著名电影导演，被称为“悬念大师”（[1899年出生](../Page/1899年.md "wikilink")）
  - [2000年](../Page/2000年.md "wikilink")：[范文同](../Page/范文同.md "wikilink")，越南政治家，曾任[越南总理](../Page/越南总理.md "wikilink")（[1906年出生](../Page/1906年.md "wikilink")）
  - 2000年：[葛庭燧](../Page/葛庭燧.md "wikilink")，中国物理学家，[中国科学院院士](../Page/中国科学院.md "wikilink")（[1913年出生](../Page/1913年.md "wikilink")）
  - 2000年：[古耕虞](../Page/古耕虞.md "wikilink")，中国企业家，被称为“猪鬃大王”（[1905年出生](../Page/1905年.md "wikilink")）
  - [2006年](../Page/2006年.md "wikilink")：[約翰·加爾布雷斯](../Page/約翰·加爾布雷斯.md "wikilink")，美國[經濟學家](../Page/經濟學家.md "wikilink")（[1908年出生](../Page/1908年.md "wikilink")）
  - [2008年](../Page/2008年.md "wikilink")：[柏楊](../Page/柏楊.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[作家](../Page/作家.md "wikilink")、[思想家](../Page/思想家.md "wikilink")、[歷史家](../Page/歷史家.md "wikilink")（[1920年出生](../Page/1920年.md "wikilink")）
  - [2012年](../Page/2012年.md "wikilink")：[楊世杰](../Page/楊世杰.md "wikilink")，[中國](../Page/中國.md "wikilink")[天文學家](../Page/天文學家.md "wikilink")（[1933年出生](../Page/1933年.md "wikilink")）

## 节日、风俗习惯

*参见：* [聯合國紀念日](../Category/聯合國紀念日.md "wikilink")

  - [世界舞蹈日](../Page/世界舞蹈日.md "wikilink")

  - ：國定假期（[2007年之前稱為](../Page/2007年.md "wikilink")[綠之日](../Page/綠之日.md "wikilink")，[2007年起改稱](../Page/2007年.md "wikilink")[昭和之日](../Page/昭和之日.md "wikilink")。以前一度稱為「天長節」，以紀念[裕仁](../Page/裕仁.md "wikilink")[天皇壽辰](../Page/天皇.md "wikilink")）