**新井
里美**（），[日本女性](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")。旧[ゆーりんプロ所屬](../Page/ゆーりんプロ.md "wikilink")，現所屬新井聲務所。[埼玉縣出身](../Page/埼玉縣.md "wikilink")。[血型AB型](../Page/血型.md "wikilink")。已婚，丈夫也是聲優。\[1\]

## 簡介

### 經歷

  - [埼玉縣立浦和西高等學校畢業後](../Page/埼玉縣立浦和西高等學校.md "wikilink")。為了成為聲優而前往[專門學校東京放送學院放送聲優科就讀](../Page/專門學校東京放送學院.md "wikilink")。
  - 2010年6月28日在自己的blog裡，表示需要休養。不過在7月裡每週都更新blog（星期日更新），8月宣布回歸工作。而休養期間的作品由其他聲優代演（後述）。
  - 第五回聲優獎上獲頒「女配角獎」\[2\]。

### 人物

  - 就讀埼玉縣立浦和西高等學校二年級（16歲）時與友人一同參加第51回《[超級變變變](../Page/超級變變變.md "wikilink")》，結果只得14分而被淘汰。
  - 因為聲音特别，所以在圈内有個外號“大媽”。但許多人也正因為這獨特的聲線而喜爱她。
  - 非常喜歡[香蕉](../Page/香蕉.md "wikilink")，相反地非常討厭[酸梅](../Page/酸梅.md "wikilink")，和代表作的角色蒼葉梢完全相反，本人說：「酸梅是讓什麼都吃的我感到恐懼的食物」，但最终克服了。\[3\]
  - 曾经和一起演出《[科學超電磁砲](../Page/科學超電磁砲.md "wikilink")》的[佐藤利奈在其他作品中也常常一起演出](../Page/佐藤利奈.md "wikilink")。
  - 作為來賓參加廣播節目《》（第2回），被[生天目仁美說了](../Page/生天目仁美.md "wikilink")：「雖然是這種人，但是站姿非常漂亮」，因而太高興而不小心流出口水。
      - 穿著有根的鞋子和有點短的裙子，「喀喀喀喀」的走路姿態非常漂亮，只看那個身影的話，如同擔任高階主管的女性。在節目中被生天目仁美說了：「妳不要講話比較好吧？」。
      - 同一節目的第11回，被臨時主持人[下屋則子說了](../Page/下屋則子.md "wikilink")：「外表感覺是非常清秀、幹練的[OL](../Page/办公室女职员.md "wikilink")」。
  - 本人說，在收錄電影版《[機動戰士Z鋼彈](../Page/機動戰士Z鋼彈.md "wikilink")》的時候，被[富野由悠季叱責過演技](../Page/富野由悠季.md "wikilink")。「妳到底幾歲啊？憑這種演技，到目前為止是怎麼活過來的！？」被說了這種人生被否定似的話，於是曾經想過，想要就這樣被車輾過（死去）。但是，多虧了富野的嚴格指導，演技也因此大幅提升。2005年後，在許多動畫中，一人分飾多角的工作也變多了。

### 代演

新井在休養期間的角色，由以下聲優代演。

  - [小櫻悅子](../Page/小櫻悅子.md "wikilink")（《[妖怪少爺](../Page/妖怪少爺.md "wikilink")》：納豆小僧（9話
    - 20話））
  - [豐崎愛生](../Page/豐崎愛生.md "wikilink")（《[出包王女](../Page/出包王女.md "wikilink")》：沛凱\[4\]
    ）
  - [中原麻衣](../Page/中原麻衣.md "wikilink")（《[FAIRY
    TAIL](../Page/FAIRY_TAIL.md "wikilink")》：比絲卡·姆蘭（44話））

## 參與作品

主要角色以**粗體**顯示

### 電視動畫

  - 2003年

<!-- end list -->

  - [數碼寶貝最前線](../Page/數碼寶貝最前線.md "wikilink")（机车兽（鼹鼠型））
  - [真珠美人魚](../Page/真珠美人魚.md "wikilink")（可可）

<!-- end list -->

  - 2004年

<!-- end list -->

  - [阿貴](../Page/阿貴.md "wikilink")（****）
  - [麻辣女孩](../Page/麻辣女孩.md "wikilink")（）
  - [蒼穹之戰神](../Page/蒼穹之戰神.md "wikilink")（要咲良）
  - [Hi Hi Puffy AmiYumi](../Page/Hi_Hi_Puffy_AmiYumi.md "wikilink")（）

<!-- end list -->

  - 2005年

<!-- end list -->

  - [阿貴 第2期](../Page/阿貴.md "wikilink")（****、）
  - おちゃめな魔女サブリナ（）
  - [地獄少女](../Page/地獄少女.md "wikilink")（菅野純子）(第一季第4話)
  - [灼眼的夏娜](../Page/灼眼的夏娜.md "wikilink")（燐子）
  - [星艦駕駛員](../Page/星艦駕駛員.md "wikilink")（）
  - [哈姆太郎](../Page/哈姆太郎.md "wikilink")（）
  - [VIEWTIFUL JOE](../Page/VIEWTIFUL_JOE.md "wikilink")（、）
  - [怪俠索羅利](../Page/怪俠索羅利.md "wikilink")（**妮莉**、）
  - [神奇寶貝超世代](../Page/神奇寶貝超世代.md "wikilink")（）
  - [我們的仙境](../Page/我們的仙境.md "wikilink")（**蒼葉梢** / **赤坂早紀** / **金澤魚子** /
    **綠川千百合** / **紺野棗**）
  - [LOVELESS](../Page/LOVELESS.md "wikilink")（女學生、媽媽、）
  - [魯邦三世 天使的策略 ～夢中的裝置是殺人香味～](../Page/魯邦三世.md "wikilink")（）

<!-- end list -->

  - 2006年

<!-- end list -->

  - [金色琴弦～primo passo～](../Page/金色琴弦.md "wikilink")（高遠美緒）
  - [Code Geass
    反叛的魯路修](../Page/Code_Geass_反叛的魯路修.md "wikilink")（篠崎咲世子、、）
  - [COYOTE RAGTIME SHOW](../Page/COYOTE_RAGTIME_SHOW.md "wikilink")（、）
  - [零之使魔](../Page/零之使魔.md "wikilink")（、、、）
  - [戰吼](../Page/戰吼.md "wikilink")（阿古屋真秋）
  - [RAY THE ANIMATION](../Page/RAY_THE_ANIMATION.md "wikilink")（真美）

<!-- end list -->

  - 2007年

<!-- end list -->

  - [銀魂](../Page/銀魂_\(動畫\).md "wikilink")（脅薰、）
  - [灼眼的夏娜II](../Page/灼眼的夏娜.md "wikilink")（巨大燐子）
  - [零之使魔 ～雙月的騎士～](../Page/零之使魔.md "wikilink")（）
  - [東京魔人學園劍風帖 龍龍](../Page/東京魔人學園劍風帖_龍龍.md "wikilink")（**櫻井小蒔**）
  - [東京魔人學園劍風帖 龍龍 第貳](../Page/東京魔人學園劍風帖.md "wikilink")（**櫻井小蒔**）
  - [交響情人夢](../Page/交響情人夢.md "wikilink")（菅沼沙也）
  - [英雄時代](../Page/英雄時代.md "wikilink")（比諾比、）
  - [BLUE DRAGON](../Page/BLUE_DRAGON.md "wikilink")（主人的影子）
  - [神奇寶貝鑽石&珍珠](../Page/神奇寶貝鑽石&珍珠.md "wikilink")（小麥）
  - [魔人偵探腦嚙涅羅](../Page/魔人偵探腦嚙涅羅.md "wikilink")（）
  - [名偵探柯南](../Page/名偵探柯南.md "wikilink")（女侍、大庭茜）

<!-- end list -->

  - 2008年

<!-- end list -->

  - [水晶之焰](../Page/水晶之焰.md "wikilink")（麻奈美）
  - [Code Geass 反叛的魯路修
    R2](../Page/Code_Geass_反叛的魯路修.md "wikilink")（篠崎咲世子、）
  - [死後文](../Page/死後文.md "wikilink")（）
  - [守護甜心](../Page/守護甜心.md "wikilink")（雪貝）
  - [零之使魔～三美姬的輪舞～](../Page/零之使魔.md "wikilink")（、、）
  - [魔法禁書目錄](../Page/魔法禁書目錄.md "wikilink")（[白井黑子](../Page/白井黑子.md "wikilink")）
  - [出包王女](../Page/出包王女.md "wikilink")（**沛凱**）
  - [二十面相少女](../Page/二十面相少女.md "wikilink")（****）
  - [光速大冒險PIPOPA](../Page/光速大冒險PIPOPA.md "wikilink")（**小波**）
  - [伯爵與妖精](../Page/伯爵與妖精.md "wikilink")（）

<!-- end list -->

  - 2009年

<!-- end list -->

  - [金色琴弦～secondo passo～](../Page/金色琴弦.md "wikilink")（高遠美緒）
  - [天國少女](../Page/天國少女.md "wikilink")（娜潔拉）
  - [鬍子小雞](../Page/鬍子小雞.md "wikilink")（媽媽）
  - [大正野球娘。](../Page/大正野球娘。.md "wikilink")（**安娜·卡特蘭德**）
  - [科學超電磁砲](../Page/科學超電磁砲.md "wikilink")（**白井黑子**）

<!-- end list -->

  - 2010年

<!-- end list -->

  - [嬌蠻貓娘大橫行](../Page/嬌蠻貓娘大橫行.md "wikilink")（鈴木）
  - [大神與七位夥伴](../Page/大神與七位夥伴.md "wikilink")（旁白）
  - [妄想學生會](../Page/妄想學生會.md "wikilink")（畑蘭子）
  - [妖怪少爺](../Page/妖怪少爺.md "wikilink")（納豆小僧）
  - 魔法禁書目錄Ⅱ（白井黑子）

<!-- end list -->

  - 2011年

<!-- end list -->

  - [這樣算是殭屍嗎？](../Page/這樣算是殭屍嗎？.md "wikilink")（水母美迦洛）
  - [FAIRY
    TAIL魔導少年](../Page/FAIRY_TAIL魔導少年.md "wikilink")（比絲卡‧姆蘭）（ビスカ・ムーラン）
  - [眾神中的貓神](../Page/眾神中的貓神.md "wikilink")（天仓守灯媛）
  - [花開物語](../Page/花開物語.md "wikilink")（押水佳代子）

<!-- end list -->

  - 2012年

<!-- end list -->

  - [愛殺寶貝](../Page/愛殺寶貝.md "wikilink")（“其他女声”）
  - [襲來！美少女邪神](../Page/襲來！美少女邪神.md "wikilink")（夏塔小弟、伊斯香）
  - [咲-Saki- 阿知賀篇 episode of
    side-A](../Page/咲-Saki-_阿知賀篇_episode_of_side-A.md "wikilink")（花田煌）
  - [名偵探柯南](../Page/名偵探柯南.md "wikilink")（鏡堂妙子）
  - [出包王女Darkness](../Page/出包王女.md "wikilink")（**沛凱**）
  - [櫻花莊的寵物女孩](../Page/櫻花莊的寵物女孩.md "wikilink")（空太母（神田明子））

<!-- end list -->

  - 2013年

<!-- end list -->

  - [問題兒童都來自異世界？](../Page/問題兒童都來自異世界？.md "wikilink")（白夜叉）
  - [科學超電磁砲S](../Page/科學超電磁砲.md "wikilink")（**白井黑子**）
  - [襲來！美少女邪神W](../Page/襲來！美少女邪神.md "wikilink")（夏塔小弟）
  - [我是國王大人](../Page/我是國王大人.md "wikilink")（**旁白**）
  - [戀愛研究所](../Page/戀愛研究所.md "wikilink")（杉原老師）
  - [噬血狂襲](../Page/噬血狂襲.md "wikilink")（碧翠絲·巴斯勒）

<!-- end list -->

  - 2014年

<!-- end list -->

  - [妄想学生会＊](../Page/妄想学生会.md "wikilink")（畑蘭子）
  - [宇宙浪子](../Page/宇宙浪子.md "wikilink")（用務員之妻）
  - [咲-Saki- 全國篇](../Page/咲-Saki-.md "wikilink")（花田煌）
  - [世界征服 謀略之星](../Page/世界征服_謀略之星.md "wikilink")（婆）
  - [蠟筆小新](../Page/蠟筆小新.md "wikilink")（美樹、樂句院蝶子）
  - [selector infected
    WIXOSS](../Page/selector_infected_WIXOSS.md "wikilink")（艾爾德拉）
  - [魔法少女大戰](../Page/魔法少女大戰.md "wikilink")（**摩助**）
  - [NO GAME NO LIFE
    遊戲人生](../Page/NO_GAME_NO_LIFE_遊戲人生.md "wikilink")（女僕）
  - [銀河騎士傳](../Page/銀河騎士傳.md "wikilink")（西山拉拉）
  - [目隱都市的演繹者](../Page/目隱都市的演繹者.md "wikilink")（薊）
  - [白銀的意志 ARGEVOLLEN](../Page/白銀的意志_ARGEVOLLEN.md "wikilink")（莉茲·羅德里克）
  - selector spread WIXOSS（艾爾德拉）

<!-- end list -->

  - 2015年

<!-- end list -->

  - [軍人少女！](../Page/軍人少女！.md "wikilink")（小守、老虎啾）
  - [幸腹塗鴉](../Page/幸腹塗鴉.md "wikilink")（內木母親）
  - [蒼穹之戰神 EXODUS](../Page/蒼穹之戰神.md "wikilink")（要咲良）
  - [魔法少女奈葉ViVid](../Page/魔法少女奈葉ViVid.md "wikilink")（愛爾絲·塔斯敏）
  - [銀魂°](../Page/銀魂_\(動畫\).md "wikilink")（脇薰）
  - [銀河騎士傳 第九行星戰役](../Page/銀河騎士傳.md "wikilink")（西山拉拉）
  - [下流梗不存在的灰暗世界](../Page/下流梗不存在的灰暗世界.md "wikilink")（**早乙女乙女**）
  - [出包王女DARKNESS 2nd](../Page/出包王女.md "wikilink")（**沛凱**）
  - [我家有個魚乾妹](../Page/我家有個魚乾妹.md "wikilink")（旁白）
  - [輕鬆百合 3☆High！](../Page/輕鬆百合.md "wikilink")（北宮初美）
  - [對魔導學園35試驗小隊](../Page/對魔導學園35試驗小隊.md "wikilink")（杉波朱雀）
  - [新妹魔王的契約者 BURST](../Page/新妹魔王的契約者.md "wikilink")（擂台賽的播報員）

<!-- end list -->

  - 2016年

<!-- end list -->

  - [魔法使 光之美少女！](../Page/魔法使_光之美少女！.md "wikilink")（魔法水晶）
  - [重裝武器](../Page/重裝武器.md "wikilink")（薇蒂妮·阿普頓）
  - [Re：從零開始的異世界生活](../Page/Re：從零開始的異世界生活.md "wikilink")（**碧翠絲**）
  - [Anne Happy ♪](../Page/Anne_Happy_♪.md "wikilink")（梅婆）
  - [食戟之靈](../Page/食戟之靈.md "wikilink") 貳之皿（露西·雨果）
  - [熱情傳奇 The X](../Page/熱情傳奇.md "wikilink")（シアリーズ）
  - [天真與閃電](../Page/天真與閃電.md "wikilink")（飯田惠）
  - [Active Raid－機動強襲室第八課－](../Page/Active_Raid－機動強襲室第八係－.md "wikilink")
    2nd（亞碧蓋兒·馬爾奇涅斯）
  - [魔法少女育成計劃](../Page/魔法少女育成計劃.md "wikilink")（**魔法賽璐璐44**）
  - [ViVid Strike\!](../Page/ViVid_Strike!.md "wikilink")（愛爾絲·塔斯敏）
  - [斯特拉的魔法](../Page/斯特拉的魔法.md "wikilink")（椎奈的母親）

<!-- end list -->

  - 2017年

<!-- end list -->

  - [時間飛船24](../Page/時間飛船24.md "wikilink")（侍女）
  - [烙印勇士](../Page/烙印勇士.md "wikilink") 第2期（伊巴蕾拉）
  - [覆面系NOISE](../Page/覆面系NOISE.md "wikilink")（久瀨月果）
  - [咕嚕咕嚕魔法陣](../Page/咕嚕咕嚕魔法陣.md "wikilink")（皮卡比亞）
  - 食戟之靈 餐之皿（露西·雨果）
  - [網路勝利組](../Page/網路勝利組.md "wikilink")（旁白）

<!-- end list -->

  - 2018年

<!-- end list -->

  - [3月的獅子 第2期](../Page/3月的獅子.md "wikilink")（高城惠的母親）

  - [POP TEAM EPIC](../Page/POP_TEAM_EPIC.md "wikilink")（魔法使〈第2話B段〉）-
    本人也實際演出

  - [OVERLORD II](../Page/OVERLORD_\(小說\).md "wikilink")（佩絲特妮·S·汪可）

  - [擁抱！光之美少女](../Page/HUG！光之美少女.md "wikilink")（比辛）

  - [宇宙戰艦提拉米蘇](../Page/宇宙戰艦提拉米蘇.md "wikilink")（本田茂子、小汪〈宇宙吉娃娃〉小型犬）

  - [Hisone與Masotan](../Page/Hisone與Masotan.md "wikilink")（絹番莉莉子）

  - [高分少女](../Page/高分少女.md "wikilink")（**矢口奈美惠**\[5\]）

  - （**六·七**）

  - 魔法禁書目錄III（白井黑子）

### OVA

  - [出包王女](../Page/出包王女.md "wikilink")（沛凱）
  - [名偵探柯南 女高中生偵探鈴木園子的事件簿](../Page/名偵探柯南.md "wikilink")（西崎榮子）
  - [百合星人奈緒子美眉](../Page/百合星人奈緒子美眉.md "wikilink")（奈緒子）
  - [科学超电磁炮EX Someone is watching
    you](../Page/科学超电磁炮.md "wikilink")（白井黑子）
  - [問題兒童都來自異世界？](../Page/問題兒童都來自異世界？.md "wikilink")（白夜叉）
  - [屍體派對](../Page/屍體派對.md "wikilink")（篠原世以子）

**2013年**

  - [人魚又上鉤](../Page/人魚又上鉤.md "wikilink")（若菜千繪里）※漫畫第9集附DVD特裝版

**2016年**

  - [FAIRY TAIL 妖精們的聖誕節](../Page/FAIRY_TAIL_\(動畫\).md "wikilink")（比絲卡）

### 劇場版動畫

  - [阿貴 THE MOVIE 「チェルシーの逆襲」](../Page/阿貴.md "wikilink")（**ジャスミン**）
  - [機動戰士Z鋼彈 星之繼承者](../Page/機動戰士Z鋼彈.md "wikilink") （**花‧園麗**）
  - [機動戰士Z鋼彈 戀人們](../Page/機動戰士Z鋼彈.md "wikilink")（**花‧園麗**）
  - [機動戰士Z鋼彈 星辰鼓動之愛](../Page/機動戰士Z鋼彈.md "wikilink")（**花‧園麗**）
  - [劇場版 灼眼的夏娜](../Page/灼眼的夏娜.md "wikilink")（燐子）
  - [劇場版 魔法禁書目錄
    -安迪米昂的奇蹟-](../Page/劇場版_魔法禁書目錄_-安迪米昂的奇蹟-.md "wikilink")（白井黑子）
  - [機動戰士鋼彈 The Origin
    II-悲傷的阿爾黛西亞](../Page/機動戰士鋼彈The_OriginII-悲傷的阿爾黛西亞.md "wikilink")
    （**哈囉**）

<!-- end list -->

  - 2016年

<!-- end list -->

  - [POP IN Q](../Page/POP_IN_Q.md "wikilink")（**路皮**\[6\]）

### 遊戲

  - [魔法禁书目录](../Page/魔法禁书目录.md "wikilink")&[科学超电磁炮系列](../Page/科学超电磁炮.md "wikilink")（白井黑子）
  - [SD鋼彈 GGENERATION
    SPIRITS](../Page/SD鋼彈G世代.md "wikilink")（ファ・ユイリィ、フローレンス・キリシマ）
  - [SD鋼彈 GGENERATION
    WARS](../Page/SD鋼彈G世代.md "wikilink")（ファ・ユイリィ、フローレンス・キリシマ）
  - [鋼彈激鬥年代記](../Page/GUNDAM激鬥年代記.md "wikilink")（ファ・ユイリィ）
  - [ガンダムバトルユニバース](../Page/ガンダムバトルユニバース.md "wikilink")（ファ・ユイリィ）
  - [鋼彈無雙](../Page/鋼彈無雙.md "wikilink")（ファ・ユイリィ）
  - [Crash Tag Team
    Racing](../Page/Crash_Tag_Team_Racing.md "wikilink")（ココ・バンディクー）
  - [Code Geass 反叛的魯路修 LOST
    COLORS](../Page/Code_Geass_反叛的魯路修.md "wikilink")（ノネット）
  - [這個美妙的世界](../Page/這個美妙的世界.md "wikilink")（八代卯月）
  - [超級機器人大戰A PORTABLE](../Page/超級機器人大戰A_PORTABLE.md "wikilink")（花‧園麗）
  - [超級機器人大戰Z](../Page/超級機器人大戰Z.md "wikilink")（花‧園麗）
  - [超級機器人大戰Z 特別篇圓盤](../Page/超級機器人大戰Z.md "wikilink")（花‧園麗）
  - [蒼穹之戰神](../Page/蒼穹之戰神.md "wikilink")（要咲良）
  - [大正野球娘 〜少女們的青春日記〜](../Page/大正野球娘.md "wikilink")（**安娜・卡特蘭德**）
  - [宵星傳奇](../Page/宵星傳奇.md "wikilink")（ゴーシュ、クローム/クロームドラゴン、シルフ、ミムラ、一般人他）
  - [緋夜傳奇](../Page/緋夜傳奇.md "wikilink")（シアリーズ）
  - [數碼寶貝拯救隊 另一個任務](../Page/數碼寶貝拯救隊_另一個任務.md "wikilink")（兒童）
  - [光速大冒險PIPOPA](../Page/光速大冒險PIPOPA.md "wikilink")（**小波**）
  - [水的旋律](../Page/水的旋律.md "wikilink")（日下部仁美）
  - [英雄傳說VI「空之軌跡」](../Page/英雄傳說VI「空之軌跡」.md "wikilink")（桃樂絲·海婭特）
  - [屍體派對 血腥籠罩之恐懼再現](../Page/屍體派對.md "wikilink")（コープスパーティー ブラッドカバー
    リピーティッドフィアー)（**篠原世以子**）
  - [屍體派對 暗影之書](../Page/屍體派對.md "wikilink")（コープスパーティー Book of
    Shadows）（**篠原世以子**）
  - [屍體派對 - THE ANTHOLOGY- 幸子的戀愛遊戲 Hysteric Birthday
    2U](../Page/屍體派對.md "wikilink")（コープスパーティー -THE ANTHOLOGY-
    サチコの恋愛遊戯Hysteric Birthday 2U）（**篠原世以子**）
  - [屍體派對 BLOOD DRIVE](../Page/屍體派對.md "wikilink")(コープスパーティー BLOOD
    DRIVE）（**篠原世以子**）
  - [屍體派對 血腥籠罩之恐懼再現3DS](../Page/屍體派對.md "wikilink") (コープスパーティー ブラッドカバー
    リピーティッドフィアー）（**篠原世以子**）
  - [白貓Project](../Page/白貓Project.md "wikilink")（索亞拉・帕諾斯）

### 外語片配音

  - [Caitlin's Way](../Page/Caitlin's_Way.md "wikilink")（ケイトリン）
  - [醜女貝蒂](../Page/醜女貝蒂.md "wikilink")
  - [動物屋](../Page/動物屋.md "wikilink")（シェリー）
  - [失蹤現場 第三季](../Page/失蹤現場.md "wikilink")（シャンタル \#14）
  - [史蒂芬一家](../Page/史蒂芬一家.md "wikilink")（露比）
  - [筆仙](../Page/筆仙.md "wikilink")
  - [魔女薩賓娜](../Page/魔女薩賓娜.md "wikilink")（グエン（タラ・ストロング））
  - [白宮風雲 第四季](../Page/白宮風雲.md "wikilink")（ゾーイ・バートレット）
  - [芝加哥希望](../Page/芝加哥希望.md "wikilink")（ダニア）
  - [七月四日誕生](../Page/七月四日誕生.md "wikilink")
  - [小查與寇弟的頂級生活](../Page/小查與寇弟的頂級生活.md "wikilink")（頂上‧蘭）
  - [蜘蛛人](../Page/蜘蛛人.md "wikilink")（サリージョンソン）
  - [誰都有秘密](../Page/誰都有秘密.md "wikilink")
  - [巴爾札克與小裁縫](../Page/巴爾札克與小裁縫.md "wikilink")
  - [不簡單的任務](../Page/不簡單的任務.md "wikilink")
  - [芝加哥打鬼](../Page/芝加哥打鬼.md "wikilink")（ティナ）
  - [逃出法蘭西](../Page/逃出法蘭西.md "wikilink")（莎拉）
  - [執法悍將](../Page/執法悍將.md "wikilink")（キャミィ\#221）
  - [粉紅豹](../Page/粉紅豹.md "wikilink")※2006年版
  - [拳霸](../Page/拳霸.md "wikilink")（ワン）

## 參考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - [官方個人資料](https://archive.is/20130209232304/http://www.yu-rin.com/profile/w_arai.html)（）

  - [新官方網站](http://arai-satomi.com/)

  -
[Category:日本女性配音員](../Category/日本女性配音員.md "wikilink")
[Category:埼玉縣出身人物](../Category/埼玉縣出身人物.md "wikilink")
[Category:聲優獎助演女優獎得主](../Category/聲優獎助演女優獎得主.md "wikilink")

1.
2.
3.  Web廣播《[今井麻美・喜多村英梨的RADIO
    屍體派對](../Page/屍體派對.md "wikilink")》第8回（2010年7月16日配信）
4.  不過新井卻演出是同時期開始的《魔法禁書目錄II》。
5.
6.