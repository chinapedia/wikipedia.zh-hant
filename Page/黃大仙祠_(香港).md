[TWGHs_Wong_Tai_Sin_Fortune_Telling_&_Oblation_Arcade.jpg](https://zh.wikipedia.org/wiki/File:TWGHs_Wong_Tai_Sin_Fortune_Telling_&_Oblation_Arcade.jpg "fig:TWGHs_Wong_Tai_Sin_Fortune_Telling_&_Oblation_Arcade.jpg")
[HK_Wong_Tai_Sin_Temple_Interior1.jpg](https://zh.wikipedia.org/wiki/File:HK_Wong_Tai_Sin_Temple_Interior1.jpg "fig:HK_Wong_Tai_Sin_Temple_Interior1.jpg")
[HK_Good_Wish_Garden.jpg](https://zh.wikipedia.org/wiki/File:HK_Good_Wish_Garden.jpg "fig:HK_Good_Wish_Garden.jpg")

**黃大仙祠**，全稱**赤松黃大仙祠**或**[嗇色園黃大仙祠](../Page/嗇色園.md "wikilink")**，是[香港的一座著名](../Page/香港.md "wikilink")[廟宇](../Page/廟宇.md "wikilink")，位於[九龍東](../Page/九龍東.md "wikilink")[黃大仙](../Page/黃大仙_\(香港地方\).md "wikilink")，佔地約18,000平方米，由非牟利慈善團體[嗇色園管理](../Page/嗇色園.md "wikilink")。廟宇主要供奉[東晉時](../Page/東晉.md "wikilink")[南中國](../Page/南中國.md "wikilink")[道教著名神祇](../Page/道教.md "wikilink")[黃初平](../Page/黃初平.md "wikilink")，另亦有供奉儒、釋兩教的神祇如[孔子](../Page/孔子.md "wikilink")、[觀音等](../Page/觀音.md "wikilink")，故三教融合為香港黃大仙祠一大特色。該建築被列作[香港一級歷史建築](../Page/香港一級歷史建築.md "wikilink")

## 歷史

1897年間，[梁仁庵聯同友好在](../Page/梁仁庵.md "wikilink")[廣東](../Page/廣東.md "wikilink")[番禺扶乩遣興](../Page/番禺.md "wikilink")，偶然得到[黃初平](../Page/黃初平.md "wikilink")（黃大仙）降乩教導「普世勸善」，自此就成為黃大仙的信仰者，奉黃大仙為仙師，入道成為道侶，獲賜道號「傳道」。根據史料，1898年農曆八月廿三日，即黃大仙的寶誕，菩山道侶就開乩請求擇地建觀，普救百姓，在黃大仙「同意及指引」下，廣東第一間供奉黃大仙的祠觀，在翌年農曆的五、六月間，在[廣州花埭](../Page/廣州.md "wikilink")（[花地](../Page/花地.md "wikilink")，位於[芳村](../Page/芳村.md "wikilink")）落成。當時據說，只有梁仁庵道長能得到仙佛的感應，使箕筆移動和辨認沙上的箕字，所以成為普濟壇的主持。清末民初時期，黃大仙祠成為廣州著名宗教勝地，吸引廣州及[珠江三角洲等善信到來參拜](../Page/珠江三角洲.md "wikilink")。

梁仁庵後得黃大仙乩示，指廣州必有動亂，於是在1901年返回故鄉廣東省南海縣[西樵山稔岡村](../Page/西樵山.md "wikilink")，再成立「普慶」壇，並出錢出力，該祠約在1903年竣工。

1911年[辛亥革命](../Page/辛亥革命.md "wikilink")，1912年[宣統退位](../Page/宣統退位.md "wikilink")，在破舊立新、徹底破除封建迷信的浪潮下，廣州市內許多廟宇寺觀都遭受破壞，普濟壇的廣州黃大仙祠亦不例外。由於中國政局動盪，社會穩定受到威脅，梁氏返回南海故鄉，但得黃大仙降乩指示「此地不宜久留，必須向南遷移」
。1915年，梁仁庵道長父子兩人，攜帶黃大仙畫像至香港繼續弘道，並在1921年於[九龍竹園村創立普宜壇](../Page/九龍.md "wikilink")（嗇色園）。黃大仙信仰自此在香港有蓬勃的發展，甚至傳播至東南亞及美加等一帶。而廣州黃大仙祠普濟壇在1919年就被[國民政府充公](../Page/國民政府.md "wikilink")，改建為[孤兒院](../Page/孤兒院.md "wikilink")。

梁仁庵到香港後，先在[乍畏街及大笪地開壇闡教](../Page/乍畏街.md "wikilink")，再於灣仔大道東設壇安奉黃大仙師及開設藥店，兩年後該處被火燒毁，又把壇遷移往灣仔海傍東。其後於1921年黃大仙師降乩啟示，命當時梁仁庵道長
往九龍城一帶相地建殿，道侶到竹園村附近一山，見靈秀獨鍾，便再扶乩請示仙師，得乩示「此乃鳳翼吉地」，適合開壇闡教，遂決定於現址建祠，並於同年7月落成啟用。1921年8月，[嗇色園正式成立](../Page/嗇色園.md "wikilink")，負責管理祠廟。黃大仙祠建成之初，原為私人道場，只供道侶及家屬入內參拜。後來參拜善信漸多，及至1934年，園方礙於當時的廟宇條例及租地批約所限，不能再讓民眾入內，後經當時[華人廟宇委員會委員](../Page/華人廟宇委員會.md "wikilink")[周峻年向](../Page/周峻年.md "wikilink")[華民政務司請准](../Page/華民政務司.md "wikilink")，才特許於每年[正月初一開放讓善信入內參拜](../Page/正月初一.md "wikilink")。至1956年8月21日，黃大仙祠才正式獲政府批准全面開放予善信參拜。

## 祠內建築

黃大仙祠共佔地18,000多平方米，除主殿外、還有三聖堂、從心苑、九龍壁等，各具建築特色，祠內的牌坊亦充分表現中國傳統文化。除個別建築物顯現中國傳統寺廟建築特色外，祠內建築又按左龍右鳳、五行屬性而興建，令整座建築群組更見特色。\[1\]黃大仙祠於2010年5月被確定為[香港一級歷史建築](../Page/香港一級歷史建築.md "wikilink")。

主殿紅柱金頂、藍楣黃格的外觀，可視為中國傳統寺廟建築的典型。主祭壇中央供奉黃大仙師之畫像，殿內亦供奉了[護法神](../Page/護法神.md "wikilink")[齊天大聖及](../Page/齊天大聖.md "wikilink")[地主](../Page/地主.md "wikilink")。主祭壇背後刻有一幅木雕，載述了黃大仙師得道成仙之事蹟。殿內牆壁更裝上儒、釋、道[三教的木刻經文和圖畫](../Page/三教.md "wikilink")，饒富意義。\[2\]

位於大殿旁的三聖堂，供奉呂祖先師、[觀音菩薩及](../Page/觀音菩薩.md "wikilink")[關聖帝君](../Page/關聖帝君.md "wikilink")。呂祖（即[呂洞賓](../Page/呂洞賓.md "wikilink")）是道教的神仙，[觀音是佛教的](../Page/觀音.md "wikilink")[菩薩](../Page/菩薩.md "wikilink")，加上麟閣供奉儒家先師[孔子](../Page/孔子.md "wikilink")，盂香亭供奉[佛教之](../Page/佛教.md "wikilink")[燃燈佛](../Page/燃燈佛.md "wikilink")。集三教神祇，故說[三教合流為香港黃大仙祠一大特色](../Page/三教合流.md "wikilink")。三聖堂內有楹聯以「嗇色」
二字為聯首：「嗇節有餘三教同源承一脈，色空雖幻眾生樂善自千秋」\[3\]。

祠內九龍壁於1981年按照[中國](../Page/中國.md "wikilink")[北京](../Page/北京.md "wikilink")[北海公園的九龍壁實物仿製](../Page/北海公園.md "wikilink")，石刻九龍，戲珠於波濤雲霧之中，蟠踞於花苑之上，栩栩如生。石壁背後刻有由已故[中國佛教協會主席](../Page/中國佛教協會.md "wikilink")[趙樸初先生獻寫的](../Page/趙樸初.md "wikilink")「九龍壁」三字及題詩一首。左龍右鳳，與九龍壁相遙呼應的鳳鳴樓位於整個建築群組最右方，樓高兩層，為園方的行政會議室及活動禮堂，以中國宮殿式設計，屋頂蓋綠琉璃瓦，別具特色。

從心苑則是於1991年園方為慶祝70週年紀慶而建，取名自[孔子曰](../Page/孔子.md "wikilink")「70而從心所欲」。苑內的長廊參照北京[頤和園長廊興建](../Page/頤和園.md "wikilink")；另有小橋、水榭、瀑布流水、人工湖兩個及各式各樣的小亭如方亭、圓亭、八角亭、扇亭等，別有特色。

代表五行屬性的建築群組則分別為飛鸞台「金」形，經堂「木」形，玉液池「水」形，盂香亭「火」形，照壁「土」形，據說是道侶得仙師乩示建築須配合[五行](../Page/五行.md "wikilink")，廟宇方可永垂久遠。

## 大殿擴建

2008年4月，園方宣佈斥資港幣1億4000萬元，進行35年來最大規劃的擴建工程，包括翻新現時供奉黃大仙師的大殿、重整大殿前之參神平台、及在大殿底部興建一座供奉太歲的地下宮殿「元辰殿」。擴建工程歷時近三年，工程於2011年1月完成\[4\]\[5\]。

「太歲元辰殿」於2011年1月12日正式對外開放，全殿分為前廳及主殿，整體設計融合中國傳統道教及現代色彩，主殿橫額由國學大師[饒宗頤親題](../Page/饒宗頤.md "wikilink")。殿內設有眾星之母[斗姆元君](../Page/斗姆元君.md "wikilink")、六十[太歲及六十元辰](../Page/太歲.md "wikilink")，並提供電子感應「上表」祈福系統及LED星象天幕，入場費為100港元，65歲或以上長者及傷殘人士半價。此外，殿內提供「上表」服務，由道士代為「[攝太歲](../Page/安太歲.md "wikilink")」，收費300港元，完成「上表」後，善信所屬的太歲的頭頂上會放出煙霞及射出紅光\[6\]。祠方稍後會再將善信的上表燒予太歲。其他工程包括新建的[藥王殿](../Page/藥王.md "wikilink")、[財神殿](../Page/財神.md "wikilink")、[王靈官殿等三個殿宇及擴建](../Page/王靈官.md "wikilink")[福德祠](../Page/土地神.md "wikilink")。黃大仙祠內亦新置了[月老銅像](../Page/月下老人.md "wikilink")，以應不同善信的需要。

## 祈福

黃大仙祠外設有解簽檔，讓善信占卜，趨吉避凶。

另外每年除夕晚上，都會有大量善信到黃大仙祠，以便在年初一上頭注香\[7\]。

### 網上祈福

2008年12月起園方推出網上祈福及網上直播\[8\]。提供網上祈福是希望讓海外善信不用親臨黃大仙祠仍可禱告祈福，善信登入網頁\[9\]，輸入姓名及祈願内容後，園方定期將祈福人士名字及祈願資料由數據處理庫列印出來，交由道長會舉行儀式，誦經及禱告，代為祈福。網上直播則每天實時直插祠內善信參神情況，希望讓海外善信可透過網絡感受參神的氣氛。

## 取景

[Wong_Tai_Sin_Fortune-telling_and_Oblation_Arcade.jpg](https://zh.wikipedia.org/wiki/File:Wong_Tai_Sin_Fortune-telling_and_Oblation_Arcade.jpg "fig:Wong_Tai_Sin_Fortune-telling_and_Oblation_Arcade.jpg")》曾安排參賽者到本廟來看掌相及臉相。\]\]
美國真人秀節目《[驚險大挑戰](../Page/驚險大挑戰.md "wikilink")》（*The Amazing
Race*）[第2季曾於本廟設快進關卡](../Page/驚險大挑戰2.md "wikilink")；參賽者要到簽品哲理中心第44號檔「上海一枝梅」，並由Amelia
Chow看掌相及臉相以過關。

## 開放時間

  - 黃大仙祠：每日07:00至17:00（年初一將提早開放時間）
  - 從心苑：每日09:00至16:30（星期一休息）
  - 黃大仙簽品哲理中心：每日07:00至18:00

## 交通

<div class="NavFrame" style="background: #FFFFFF; margin: 0 auto; padding: 0 10px">

<div class="NavHead" style="background: #FFFFFF; margin: 0 auto; padding: 0 10px">

**交通路線列表**

</div>

<div class="NavContent" style="background: #FFFFFF; margin: 0 auto; padding: 0 10px">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{觀塘綫色彩}}">█</font>[觀塘綫](../Page/觀塘綫.md "wikilink")：[黃大仙站B](../Page/黃大仙站.md "wikilink")2出口

<!-- end list -->

  - [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - [專線小巴](../Page/香港小巴.md "wikilink")

</div>

</div>

## 圖集

<File:Wong> Tai Sin Temple, Mar
06.JPG|第一洞天牌坊，據說是[乙亥年](../Page/乙亥.md "wikilink")（1935年）赤松仙的乩筆\[10\]。
<File:Wong> Tai Sin Temple 14, Mar 06.JPG|「赤松黃仙祠」正殿，掛有「道 經 師」三個字
<File:Wong> Tai Sin Temple 16, Mar 06.JPG|金華分蹟牌坊 <File:Wong> Tai Sin
Temple 10, Mar 06.JPG|盂香亭 <File:Wong> Tai Sin Temple, Confucian
Veranda.jpg|孔道門

## 參考資料

## 外部連結

[Category:黃大仙](../Category/黃大仙.md "wikilink")
[Category:香港地標](../Category/香港地標.md "wikilink")
[Category:香港道教建築](../Category/香港道教建築.md "wikilink")
[Category:嗇色園](../Category/嗇色園.md "wikilink")
[Category:香港道觀](../Category/香港道觀.md "wikilink")
[Category:香港非物質文化遺產](../Category/香港非物質文化遺產.md "wikilink")
[Category:香港的祠](../Category/香港的祠.md "wikilink")
[Category:竹園](../Category/竹園.md "wikilink")

1.  [黃大仙祠虛擬遊覽](http://www.siksikyuen.org.hk/public/contents/category?lang=big5&cid=804)

2.  [殿內雕刻](http://www.siksikyuen.org.hk/public/virtualtour/desc?place_id=1)

3.
4.  [嗇色園黃大仙祠大殿擴建及元辰殿建設計劃新聞發佈](http://www.siksikyuen.org.hk/public/contents/press?ha=&wc=0&hb=&hc=&revision%5fid=11119&item%5fid=9274)

5.

6.

7.  [嗇色園](../Page/嗇色園.md "wikilink") - [除夕祈福科儀
    (頭炷香)](https://www1.siksikyuen.org.hk/%E5%AE%97%E6%95%99%E4%BA%8B%E5%8B%99/%E5%AE%97%E6%95%99%E4%BB%8B%E7%B4%B9/%E7%A7%91%E5%84%80/%E9%99%A4%E5%A4%95%E7%A5%88%E7%A6%8F\(%E9%A0%AD%E7%82%B7%E9%A6%99\))

8.

9.  [網上祈福](http://services1.siksikyuen.org.hk/pray/movie/PlayMovie.aspx)

10.