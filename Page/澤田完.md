**澤田完**，或译为**沢田完**（），是一位[日本](../Page/日本.md "wikilink")[東京都出身的](../Page/東京都.md "wikilink")[作曲家](../Page/作曲家.md "wikilink")，[東京音樂大學肆业](../Page/東京音樂大學.md "wikilink")。以作曲涉猎种类广泛而闻名，配乐编曲涉及連續劇、電影和舞台劇等艺术形式。他更是自2005年起擔任《[多啦A夢](../Page/多啦A夢.md "wikilink")》的音樂擔當者。

## 電視節目

  - [NHKみんなのうた](../Page/NHKみんなのうた.md "wikilink")（作曲、編曲）
  - [沒有標題的音樂會](../Page/沒有標題的音樂會.md "wikilink")（編曲）

## 電視劇

  - [14歲媽媽](../Page/14歲媽媽.md "wikilink")
  - [傳說中的教師](../Page/傳說中的教師.md "wikilink")
  - [女婿先生](../Page/女婿先生.md "wikilink")
  - [美女與野獸](../Page/美女與野獸.md "wikilink")
  - [成為女主人\!](../Page/成為女主人!.md "wikilink")
  - [薔薇的十字架](../Page/薔薇的十字架.md "wikilink")
  - [再見螢火蟲](../Page/再見螢火蟲.md "wikilink")（真人版）
  - [逃亡者 おりん](../Page/逃亡者_おりん.md "wikilink")
  - [夏日之戀～Summer Time～](../Page/夏日之戀～Summer_Time～.md "wikilink")
  - [ドリーム～90日で1億円～](../Page/ドリーム～90日で1億円～.md "wikilink")
  - [Doctor-X～外科醫·大門未知子～](../Page/Doctor-X～外科醫·大門未知子～.md "wikilink")
  - [養狗這回事](../Page/養狗這回事.md "wikilink")
  - [遺產爭族](../Page/遺產爭族.md "wikilink")

## 舞台劇

  - 紫式部ものがたり
  - エレファントマン
  - 傳說中的女優
  - 浪人街
  - サラ ～追想で綴る女優サラ・ベルナールの生涯～
  - 戀愛三重奏
  - 若き日のゴッホ

## 動畫

  - あじさいの唄（[OVA作品](../Page/OVA.md "wikilink")）
  - [ジバクくん](../Page/ジバクくん.md "wikilink")
  - [Keroro軍曹](../Page/Keroro軍曹_\(動畫\).md "wikilink")（初代OP、3代目ED、挿入歌作曲）
  - [多啦A夢](../Page/多啦A夢.md "wikilink")（水田版本）
  - [スージーちゃんとマービー](../Page/スージーちゃんとマービー.md "wikilink")
  - [KAIKANフレーズ](../Page/KAIKANフレーズ.md "wikilink")
  - [MOONLIGHT MILE](../Page/MOONLIGHT_MILE.md "wikilink")

## 電影

  - [ふみ子の海](http://www.fumikonoumi.com/)
  - [深紅](../Page/深紅.md "wikilink")

## 動畫電影

  - [大雄的南海大冒險](../Page/大雄的南海大冒險.md "wikilink")（音樂編曲）
  - [大雄與機械人王國](../Page/大雄與機械人王國.md "wikilink")（插入歌編曲）
  - [大雄的恐龍2006](../Page/大雄的恐龍2006.md "wikilink")
  - [大雄的新魔界大冒險\~7人魔法使\~](../Page/大雄的新魔界大冒險~7人魔法使~.md "wikilink")
  - [大雄與綠之巨人傳](../Page/大雄與綠之巨人傳.md "wikilink")
  - [新·大雄的宇宙開拓史](../Page/新·大雄的宇宙開拓史.md "wikilink")
  - [大雄的人魚大海戰](../Page/大雄的人魚大海戰.md "wikilink")
  - [新·大雄的日本诞生](../Page/新·大雄的日本诞生.md "wikilink")
  - [大雄的南極冰天雪地大冒險](../Page/大雄的南極冰天雪地大冒險.md "wikilink")（插入歌編曲）
  - [劇場版寵物小精靈 超夢夢的反擊](../Page/劇場版寵物小精靈_超夢夢的反擊.md "wikilink")（片尾主題歌編曲）
  - [劇場版寵物小精靈 比卡超探險隊](../Page/劇場版寵物小精靈_比卡超探險隊.md "wikilink")

## 参见

  - [東京音樂大學](../Page/東京音樂大學.md "wikilink")
  - [作曲家一覽](../Page/作曲家一覽.md "wikilink")

[Category:日本作曲家](../Category/日本作曲家.md "wikilink")
[Category:20世纪作曲家](../Category/20世纪作曲家.md "wikilink")
[Category:21世纪作曲家](../Category/21世纪作曲家.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")