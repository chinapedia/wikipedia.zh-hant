[BhutanPunakha.png](https://zh.wikipedia.org/wiki/File:BhutanPunakha.png "fig:BhutanPunakha.png")
[Punakha_Dzong.jpg](https://zh.wikipedia.org/wiki/File:Punakha_Dzong.jpg "fig:Punakha_Dzong.jpg")與[母河](../Page/母河.md "wikilink")\]\]
**普那卡宗**（སྤུ་ན་ཁ་རྫོང་ཁག）（）是[不丹二十個宗](../Page/不丹.md "wikilink")（dzongkhag）之一，面積達1,016平方公里，人口約23,340人，首府為[普那卡](../Page/普那卡.md "wikilink")。

**普那卡宗**轄下有以下的[格窩](../Page/格窩.md "wikilink")（Gewog）：

  - [Chhubu
    Gewog](https://web.archive.org/web/20050817115231/http://www.dop.gov.bt/fyp/09/pn_Chhubu.pdf)
  - [Dzomo
    Gewog](https://web.archive.org/web/20041022222159/http://www.dop.gov.bt/fyp/09/pn_DZOMO.pdf)
  - [Goenshari
    Gewog](https://web.archive.org/web/20050820060815/http://www.dop.gov.bt/fyp/09/pn_Goenshari.pdf)
  - [Guma
    Gewog](https://web.archive.org/web/20041022215618/http://www.dop.gov.bt/fyp/09/pn_GUMA.pdf)
  - [Kabjisa
    Gewog](https://web.archive.org/web/20050824051431/http://www.dop.gov.bt/fyp/09/pn_KABJISA.pdf)
  - [Lingmukha
    Gewog](https://web.archive.org/web/20050817051329/http://www.dop.gov.bt/fyp/09/pn_Lingmukha.pdf)
  - [Shenga Bjime
    Gewog](https://web.archive.org/web/20050816065624/http://www.dop.gov.bt/fyp/09/pn_SHENGA_BJIME.pdf)
  - [Talo
    Gewog](https://web.archive.org/web/20050817094047/http://www.dop.gov.bt/fyp/09/pn_TALO.pdf)
  - [Toewang
    Gewog](https://web.archive.org/web/20041022224939/http://www.dop.gov.bt/fyp/09/pn_TOEWANG.pdf)

[Category:不丹行政區劃](../Category/不丹行政區劃.md "wikilink")