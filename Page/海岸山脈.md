[Taiwan-Haian_Range.jpg](https://zh.wikipedia.org/wiki/File:Taiwan-Haian_Range.jpg "fig:Taiwan-Haian_Range.jpg")
**海岸山脈**為[台灣五大山脈之一](../Page/台灣五大山脈.md "wikilink")，[日治時期被稱為](../Page/台灣日治時期.md "wikilink")**台東山脈**，位於臺灣本島之東緣，即[臺灣東部](../Page/臺灣東部.md "wikilink")，北起[花蓮縣](../Page/花蓮縣.md "wikilink")[花蓮溪口](../Page/花蓮溪.md "wikilink")，南迄[臺東縣](../Page/臺東縣.md "wikilink")[卑南溪口](../Page/卑南溪.md "wikilink")，其縱長凡150公里，東西之平均寬度為10公里，稜脈呈東北－西南走向\[1\]，是五大山脈之中最低矮者，山嶺高度多在1000公尺左右，北段較低，南段較高，最高峰為高1680公尺的[新港山](../Page/新港山.md "wikilink")\[2\]。

## 地形

海岸山脈一般以[秀姑巒溪切穿而分成南北兩段](../Page/秀姑巒溪.md "wikilink")。北段北自[花蓮溪口的花蓮山起](../Page/花蓮溪.md "wikilink")（地質學家認為[花蓮市的](../Page/花蓮市.md "wikilink")[美崙山](../Page/美崙山.md "wikilink")、花崗山亦係海岸山脈殘餘的山丘），經賀田山、[月眉山](../Page/月眉山_\(花蓮縣\).md "wikilink")、[六階鼻山](../Page/六階鼻山.md "wikilink")、[八里灣山](../Page/八里灣山.md "wikilink")、[大奇山而抵](../Page/大奇山.md "wikilink")[秀姑巒溪](../Page/秀姑巒溪.md "wikilink")，八里灣山是北段最高峰，但高度未及1000公尺。北段為[台11甲線光豐公路切斷](../Page/台11線.md "wikilink")，另有米棧-水璉產業道路、奉公越嶺路等古道\[3\]。

奇美以南是南段，自里牙津山起，經三間屋山、北花東山、成廣澳山、新港山、都蘭山而止於[卑南溪](../Page/卑南溪.md "wikilink")，南段除有[東河到富里的東富公路切穿外](../Page/東河鄉_\(臺灣\).md "wikilink")，新開通的[台30線玉長公路](../Page/台30線.md "wikilink")，從[卓溪鄉的](../Page/卓溪鄉.md "wikilink")[南安瀑布起](../Page/南安瀑布.md "wikilink")，經[玉里](../Page/玉里鎮.md "wikilink")、安通，鑿穿海岸山脈後，直抵[台11線的寧埔](../Page/台11線.md "wikilink")；還有[瑞穗到大港口的](../Page/瑞穗鄉.md "wikilink")[瑞港公路](../Page/瑞港公路.md "wikilink")、[安通越嶺道等古道](../Page/安通越嶺古道.md "wikilink")。

海岸山脈東西兩側有許多平行主稜的小稜脈，學者稱為「雁狀排列」，如同[雪山山脈北段](../Page/雪山山脈.md "wikilink")\[4\]。

受到菲律賓海板塊的擠壓，[綠島和](../Page/綠島.md "wikilink")[蘭嶼正以每年八公分的速度旋轉](../Page/蘭嶼.md "wikilink")、移向台東，約50和120萬年後就會在台東縣[台東市位置連接](../Page/台東市.md "wikilink")，成為海岸山脈的一部份。數百萬年後海岸山脈北段會脫離台灣本島至[琉球海溝一帶隱沒入歐亞大陸板塊底下而消失](../Page/琉球海溝.md "wikilink")。

## 地質

海岸山脈是[菲律賓群島漂移而來的陸地](../Page/菲律賓群島.md "wikilink")，因此地質為[安山岩的火山集塊岩](../Page/安山岩.md "wikilink")，與[綠島](../Page/綠島.md "wikilink")、[蘭嶼相同](../Page/蘭嶼.md "wikilink")，因為與[花東縱谷的板塊不同](../Page/花東縱谷.md "wikilink")，兩相撞擊下，使花東地區成為台灣地震發生最頻繁的地區\[5\]。在海岸山脈擠壓形成的過程中，海底物質底泥及岩塊被推擠上陸地，形成特殊泥岩層，例如分布在海岸山脈南端，位於臺東縣的[利吉惡地](../Page/利吉惡地.md "wikilink")\[6\]。而高熱岩漿噴發地表時混合了其他岩石，形成了以堅硬高大聳立的集塊岩為主體的山體，例如[都蘭山](../Page/都蘭山.md "wikilink")\[7\]。

## 主要山峰

海岸山脈超過一千公尺以上的山峰有十餘座，由北而南詳列如下：

1.  [織羅山](../Page/織羅山.md "wikilink")（1,152公尺）
2.  [麻汝蘭山](../Page/麻汝蘭山.md "wikilink")（1,280公尺）
3.  [三間屋山](../Page/三間屋山.md "wikilink")（1,334公尺）
4.  [北花東山](../Page/北花東山.md "wikilink")（1,242公尺）
5.  [花東山](../Page/花東山.md "wikilink")（1,135公尺）
6.  [烏帽子山](../Page/烏帽子山.md "wikilink")（1,012公尺）
7.  [安通越山](../Page/安通越山.md "wikilink")（1,085公尺）
8.  [大庄越山](../Page/大庄越山.md "wikilink")（1,010公尺）
9.  [成廣澳山](../Page/成廣澳山.md "wikilink")（1,597公尺）
10. [分水崙山](../Page/分水崙山.md "wikilink")（1,441公尺）
11. [白守蓮山](../Page/白守蓮山.md "wikilink")（1,561公尺）
12. [新港山](../Page/新港山.md "wikilink")（1,682公尺，**最高峰**）
13. [麒麟山](../Page/麒麟山.md "wikilink")（1,544公尺）
14. [嘉平山](../Page/嘉平山.md "wikilink")（1,016公尺）
15. [富興山](../Page/富興山.md "wikilink")（1,023公尺）
16. [里東峰](../Page/里東峰.md "wikilink")（1,010公尺）
17. [都蘭山](../Page/都蘭山.md "wikilink")（1,190公尺）

<File:Seashore> Mountain Range，Taiwan.jpg|花蓮的海岸山脈 <File:Haian> Range
03.jpg|花蓮縣豐濱郷的海岸山脈 <File:Haian> Range 02.jpg|花蓮縣豐濱郷的海岸山脈 <File:2010> 07
18550 5233 Chenggong Township, Taiwan.JPG|臺東縣成功鎮的海岸山脈 <File:2010> 07
14760 5399 Amis Folk Center Cobblestones in Chenggong Towship
Taiwan.JPG|臺東縣成功鎮的海岸山脈 <File:2005> 0708 01570 MAM 00384.jpg|臺東縣成功鎮的海岸山脈
<File:2010> 07 14100 6489 Chenggong Chenggong Fishing Harbor
Taiwan.JPG|臺東縣成功鎮[成功漁港看海岸山脈](../Page/成功漁港.md "wikilink") <File:Taiwan>
2009 East Coast Mountain Range FRD 6296 Pano
Extracted.jpg|[玉長公路橫貫海岸山脈](../Page/玉長公路.md "wikilink")

## 參考資料

[Category:台灣山脈](../Category/台灣山脈.md "wikilink")
[Category:花蓮縣地理](../Category/花蓮縣地理.md "wikilink")
[Category:台東縣地理](../Category/台東縣地理.md "wikilink")

1.
2.
3.

4.
5.

6.
7.