**神石高原町**（）是位于[廣島縣東部](../Page/廣島縣.md "wikilink")，與[岡山縣接壤的一](../Page/岡山縣.md "wikilink")
[町](../Page/町.md "wikilink")。

轄區位於[吉備高原](../Page/吉備高原.md "wikilink")，與岡山縣備中地方的[高梁市](../Page/高梁市.md "wikilink")[備中町](../Page/備中町.md "wikilink")、[成羽町](../Page/成羽町.md "wikilink")、[川上町](../Page/川上町.md "wikilink")、[井原市](../Page/井原市.md "wikilink")[芳井町具有共同的歷史與文化](../Page/芳井町.md "wikilink")。

## 歷史

1619年起成為[福山藩](../Page/福山藩.md "wikilink")[水野勝成的領地](../Page/水野勝成.md "wikilink")，直到1698年因[水野勝岑過世後無留下子嗣](../Page/水野勝岑.md "wikilink")，而後部分地區成為德川幕府的[天領](../Page/天領.md "wikilink")，部分地區則成為[中津藩的飛領](../Page/中津藩.md "wikilink")。\[1\]

[明治維新後](../Page/明治維新.md "wikilink")，先後隸屬[倉敷縣](../Page/倉敷縣.md "wikilink")、[深津縣](../Page/深津縣.md "wikilink")、[小田縣](../Page/小田縣.md "wikilink")、[岡山縣](../Page/岡山縣.md "wikilink")，直到1877年才確定歸屬[廣島縣](../Page/廣島縣.md "wikilink")。

### 沿革

  - 1889年4月1日：實施[町村制](../Page/町村制.md "wikilink")，現在轄區在當時分屬：[神石郡阿下村](../Page/神石郡.md "wikilink")、有木村、上野村、小野村、上村、上豐松村、龜石村、木津和村、草木村、來見村、小畠村、笹尾村、下豐松村、新坂村、李村、高蓋村、高光村、田頭村、近田村、父木野村、常光村、中平村、永渡村、花濟村、福永村、古川村、牧村、光末村、光信村、油木村。
  - 1897年7月1日：有木村、上豐松村、笹尾村、下豐松村、中平村[合併為](../Page/市町村合併.md "wikilink")[豐松村](../Page/豐松村.md "wikilink")。
  - 1897年7月11日：上野村、李村、近田村、花濟村合併為仙養村。
  - 1917年8月1日：油木村改制為[油木町](../Page/油木町.md "wikilink")。
  - 1940年11月10日：草木村、田頭村、福永村、牧村合併為新設置的牧村。
  - 1942年4月1日：阿下村、上村、龜石村、小畠村、常光村合併為新設置的小畠村。
  - 1943年4月1日：高光村、古川村合併為新設置的高光村。
  - 1944年1月1日：木津和村、高蓋村、父木野村、光末村、光信村合併為新設置的高蓋村。
  - 1949年7月1日：[蘆品郡大正村的桑木地區被併入高蓋村](../Page/蘆品郡.md "wikilink")。
  - 1954年3月31日：[甲奴郡階見的部分地區被併入高蓋村](../Page/甲奴郡.md "wikilink")。
  - 1954年11月3日：高光村和牧村合併為[神石町](../Page/神石町.md "wikilink")。
  - 1955年3月31日：
      - 來見村、小畠村、高蓋村合併為[三和町](../Page/三和町_\(廣島縣神石郡\).md "wikilink")。
      - 永渡村被併入神石町。
  - 1955年4月1日：油木町、小野村、新坂村合併為新設置的油木町。
  - 1956年3月31日：油木町與仙養村合併為新設置的油木町。
  - 1959年7月1日：蘆品郡藤尾村的部分地區被併入三和町。
  - 2004年11月5日：油木町、神石町、豐松村、三和町合併為**神石高原町**。\[2\]

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1889年4月1日</p></th>
<th><p>1889年 - 1926年</p></th>
<th><p>1926年 - 1954年</p></th>
<th><p>1955年 - 1989年</p></th>
<th><p>1989年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>有木村</p></td>
<td><p>1897年7月1日<br />
合併為豐松村</p></td>
<td><p>2004年11月5日<br />
合併為神石高原町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>上豐松村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>?尾村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>下豐松村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>中平村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>上野村</p></td>
<td><p>1897年7月11日<br />
合併為仙養村</p></td>
<td><p>1956年3月31日<br />
合併為油木町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>李村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>近田村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>花濟村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>油木村</p></td>
<td><p>1917年8月1日<br />
改制為油木町</p></td>
<td><p>1955年4月1日<br />
合併為油木町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>小野村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>新?村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>草木村</p></td>
<td><p>1940年11月10日<br />
合併為牧村</p></td>
<td><p>1954年11月3日<br />
合併為神石町</p></td>
<td><p>神石町</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>田頭村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>福永村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>牧村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>高光村</p></td>
<td><p>1943年4月1日<br />
高光村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>古川村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>永渡村</p></td>
<td><p>1955年3月31日<br />
併入神石町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>阿下村</p></td>
<td><p>1942年4月1日<br />
小畠村</p></td>
<td><p>1955年3月31日<br />
合併為三和町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>上村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>龜石村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>小畠村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>常光村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>木津和村</p></td>
<td><p>1944年1月1日<br />
高蓋村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>高蓋村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>父木野村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>光末村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>光信村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>來見村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 交通

轄區內無鐵路通過，距離近的鐵路車站包括位於[府中市的](../Page/府中市_\(廣島縣\).md "wikilink")[上下車站](../Page/上下車站.md "wikilink")、[福山市的](../Page/福山市.md "wikilink")[道上車站](../Page/道上車站.md "wikilink")、[新市車站](../Page/新市車站.md "wikilink")、[-{庄}-原市的](../Page/庄原市.md "wikilink")[東城車站](../Page/東城車站.md "wikilink")、[高梁市的](../Page/高梁市.md "wikilink")[備中高梁車站](../Page/備中高梁車站.md "wikilink")。

## 觀光資源

轄區北部屬於[比婆道後帝釋國定公園的範圍](../Page/比婆道後帝釋國定公園.md "wikilink")。

  - 星居山森林公園
  - 米見山
  - 塚峠
  - 堂面洞窟遺跡
  - 觀音堂岩陰遺跡

## 教育

### 高等教育

  - [廣島縣立油木高等學校](../Page/廣島縣立油木高等學校.md "wikilink")

## 參考資料

## 外部連結

  - [神石高原町觀光協會](http://jkougen.jp/kankou/)

  - [神石高原商工會](http://jkougen.jp/)

  - [神石高原町人力資源中心](http://akibudou.sakura.ne.jp/)

<!-- end list -->

1.

2.