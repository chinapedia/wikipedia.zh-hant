**章新胜**，现任中国教育部副部长，[中国奥林匹克委员会副主席](../Page/中国奥林匹克委员会.md "wikilink")，[国际自然保护联盟主席](../Page/国际自然保护联盟.md "wikilink")。

1948年11月，生于[江苏省](../Page/江苏省.md "wikilink")[宿迁市](../Page/宿迁市.md "wikilink")[沭阳县](../Page/沭阳县.md "wikilink")。毕业于[杭州大学](../Page/杭州大学.md "wikilink")（今[浙江大学](../Page/浙江大学.md "wikilink")）外语系。曾在[美国科罗拉多大学学习](../Page/美国.md "wikilink")。获得[哈佛大学城市规划](../Page/哈佛大学.md "wikilink")[硕士](../Page/硕士.md "wikilink")[学位](../Page/学位.md "wikilink")。

1986年-1989年，担任中国国家旅游局副局长。1989年-1997年，担任[苏州市市长](../Page/苏州市.md "wikilink")。2001年至今，担任中国教育部副部长。2004年至今，担任中国奥委会副主席。

2008年，当选第十一届全国政协委员\[1\]，代表教育界，分入第四十组。并担任外事委员会专委。\[2\]

2012年，被选为[国际自然保护联盟](../Page/国际自然保护联盟.md "wikilink")（IUCN）主席。\[3\]

## 参考资料

## 外部链接

  - [教育部副部长章新胜简历](https://finance.sina.com.cn/roll/20050621/15011707861.shtml)

[Z章新胜](../Category/沭陽人.md "wikilink")
[Z章新胜](../Category/浙江大学校友.md "wikilink")
[Category:第十一届全国政协委员](../Category/第十一届全国政协委员.md "wikilink")
[Category:章姓](../Category/章姓.md "wikilink")
[Category:香港嶺南大學榮譽博士](../Category/香港嶺南大學榮譽博士.md "wikilink")

1.
2.
3.