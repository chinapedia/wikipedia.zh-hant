[Salman-Rushdie-1.jpg](https://zh.wikipedia.org/wiki/File:Salman-Rushdie-1.jpg "fig:Salman-Rushdie-1.jpg")
**艾哈邁德·薩爾曼·魯西迪**爵士（，，），又译为**萨尔曼·拉什迪**，印度/英国文学家，作家。

## 经历

生於[孟買](../Page/孟買.md "wikilink")，十四歲移居[英國讀書](../Page/英國.md "wikilink")，[劍橋大學國王學院歷史系畢業](../Page/劍橋大學國王學院.md "wikilink")。其作品风格往往被归类为[魔幻写实主义](../Page/魔幻写实主义.md "wikilink")，作品显示出东西方文化的双重影响。他在英國支持工黨，並批評祖國印度的保守派政黨。

魯西迪現居紐約，並曾經在電影《[BJ單身日記](../Page/BJ單身日記_\(電影\).md "wikilink")》客串。

## 作品

### 午夜之子

魯西迪的第二部小說作品《午夜之子》獲得1981年[布克獎](../Page/布克獎.md "wikilink")，內容充滿豐富的想像。

### 撒旦的詩篇

1989年出版，魯西迪的《[撒旦詩篇](../Page/撒旦詩篇.md "wikilink")》（*The Satanic
Verses*，或譯《[魔鬼詩篇](../Page/魔鬼詩篇.md "wikilink")》）因為責駡[伊斯蘭教的不公平](../Page/伊斯蘭教.md "wikilink")，而遭[伊朗精神領袖](../Page/伊朗.md "wikilink")[赛义德·鲁霍拉·霍梅尼下達追殺令](../Page/赛义德·鲁霍拉·霍梅尼.md "wikilink")。追杀令由霍梅尼于1989年2月14日宣布，英国在与伊朗交涉失败后，于3月7日宣布与伊朗断决外交关系。1998年两国恢复外交关系，作为复交的前提，以[穆罕默德·哈塔米总统为首的伊朗政府宣布](../Page/穆罕默德·哈塔米.md "wikilink")“既不支持也不阻止对鲁西迪的刺杀”。

### 其他著作

  - [格里茅斯](../Page/格里茅斯.md "wikilink")（Grimus）1975年出版，是科幻小說。
  - [羞恥 (魯西迪)](../Page/羞恥_\(魯西迪\).md "wikilink") 1983年出版，以巴基斯坦為場景的政治小說。
  - [想像的家園](../Page/想像的家園.md "wikilink")（Imaginary Homelands：Essays and
    Criticism，1981-1991）1991年出版，「這本集結了75篇論文和書評，涵蓋了文學、政治與宗教，被視為一部『世界主義─後殖民批評』的重要宣言。」\[1\]。
  - [摩爾人的最後嘆息](../Page/摩爾人的最後嘆息.md "wikilink")（The Moor's Last
    Sigh）1995年出版，是一部大型家族史詩小說。
  - [她腳下的土地](../Page/她腳下的土地.md "wikilink")（The Ground Beneath Her
    Feet）1999年出版，小說，一位現代流行音樂歌手一生的故事。
  - [憤怒](../Page/憤怒.md "wikilink")（Fury）2001年出版，屬於哲理寓言小說。
  - [小丑薩利瑪](../Page/小丑薩利瑪.md "wikilink")
  - [哈樂與故事之海](../Page/哈樂與故事之海.md "wikilink")
  - [東方，西方](../Page/東方，西方.md "wikilink")
  - [美洲豹的微笑：尼加拉瓜之旅](../Page/美洲豹的微笑：尼加拉瓜之旅.md "wikilink")
  - [綠野仙蹤 (魯西迪)](../Page/綠野仙蹤_\(魯西迪\).md "wikilink")

## 注释

## 参考

[Category:布克獎獲獎者](../Category/布克獎獲獎者.md "wikilink")
[Category:英國皇家學會院士](../Category/英國皇家學會院士.md "wikilink")
[Category:當代文學作家](../Category/當代文學作家.md "wikilink")
[Category:後現代文學](../Category/後現代文學.md "wikilink")
[Category:英國小說家](../Category/英國小說家.md "wikilink")
[Category:無神論者](../Category/無神論者.md "wikilink")
[Category:劍橋大學國王學院校友](../Category/劍橋大學國王學院校友.md "wikilink")
[Category:下級勳位爵士](../Category/下級勳位爵士.md "wikilink")
[Category:归化英国公民的印度人](../Category/归化英国公民的印度人.md "wikilink")
[Category:孟買人](../Category/孟買人.md "wikilink")
[Category:詹姆斯·泰特·布莱克纪念奖获得者](../Category/詹姆斯·泰特·布莱克纪念奖获得者.md "wikilink")

1.