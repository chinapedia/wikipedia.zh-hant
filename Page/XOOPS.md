**XOOPS**是一種開放原始碼的[内容管理系統](../Page/内容管理系統.md "wikilink")，衍生自[PHP-Nuke](../Page/PHP-Nuke.md "wikilink")，採用[PHP語言跟](../Page/PHP.md "wikilink")[MySQL資料庫](../Page/MySQL.md "wikilink")。功能、界面全部模組化設計，可用於建構各種網路社區。

XOOPS的發布採用[GPL協議](../Page/GPL.md "wikilink")，可免費使用和修改；在遵循[GPL相關條款的前提下](../Page/GPL.md "wikilink")，可自由再發布。

XOOPS是 e**X**tensible **O**bject **O**riented **P**ortal **S**ystem
的縮寫，按照英語規則，XOOPS應該讀作“zoo'ps”。

XOOPS的用途很廣泛，對於個人使用者，可以啟用XPress-{zh-hans:模块;
zh-hant:模組;}-（針對XOOPS的[WordPress](../Page/WordPress.md "wikilink")），作為個人的[網誌](../Page/網誌.md "wikilink")。對於較大規模的網站，可以根據需要，安裝更多的-{zh-hans:模块;
zh-hant:模組;}-，如新聞發布、[論壇](../Page/論壇.md "wikilink")、資源下载、友情連結，以及線上購物、廣告管理等，對於[Wiki](../Page/Wiki.md "wikilink")、[RSS](../Page/RSS.md "wikilink")，XOOPS也有相應的-{zh-hans:模块;
zh-hant:模組;}-。

在國際語言支持方面，XOOPS有二十種以上的語言版本，包括英文、繁體中文、簡體中文，編碼可自由選擇採用[GB
2312](../Page/GB_2312.md "wikilink")、[BIG
5或是](../Page/大五碼.md "wikilink")[UTF-8](../Page/UTF-8.md "wikilink")。

## 參見

  - [XOOPS Cube](../Page/XOOPS_Cube.md "wikilink")
  - [物件導向](../Page/物件導向.md "wikilink")
  - [內容管理系統](../Page/內容管理系統.md "wikilink")（CMS）
  - [內容管理系統列表](../Page/內容管理系統列表.md "wikilink")

## 外部連結

  - [XOOPS 官方網站](http://www.xoops.org/)
  - [XOOPS 繁體中文](http://xoops.tn.edu.tw/)
  - [XOOPS SourceForge Project
    Page](../Page/:sourceforge:projects/XOOPS/.md "wikilink")

### [XOOPS Cube](../Page/XOOPS_Cube.md "wikilink")（XOOPS 的分支）：

  - [XOOPS Cube英語官方網站](http://www.xoopscube.org/)
  - [XOOPS
    Cube繁體中文支援站](https://web.archive.org/web/20061228054403/http://www.xoopscube.tw/)
  - [XOOPS Cube日本支援網站](http://www.xoopscube.jp/)
  - [XOOPS Cube SourceForge Project
    Site](../Page/:sourceforge:projects/xoopscube/.md "wikilink")

[Category:內容管理系統](../Category/內容管理系統.md "wikilink")
[Category:PHP](../Category/PHP.md "wikilink")
[Category:SourceForge專案](../Category/SourceForge專案.md "wikilink")
[Category:自由軟體](../Category/自由軟體.md "wikilink")