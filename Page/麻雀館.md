**麻雀館**是一個供客人進行[麻將耍樂的娛樂場所](../Page/麻將.md "wikilink")。

## 香港麻雀館

### 歷史

[HK_WC_Spring_Garden_Lane_春園街_Dragon_MJ.jpg](https://zh.wikipedia.org/wiki/File:HK_WC_Spring_Garden_Lane_春園街_Dragon_MJ.jpg "fig:HK_WC_Spring_Garden_Lane_春園街_Dragon_MJ.jpg")的一間麻雀館\]\]
[Mahjong_school_6218.JPG](https://zh.wikipedia.org/wiki/File:Mahjong_school_6218.JPG "fig:Mahjong_school_6218.JPG")一間麻雀館的霓虹燈牌\]\]
麻將一直以來是[中國人的傳統社交活動](../Page/中國人.md "wikilink")。[香港在](../Page/香港.md "wikilink")1841年由[英國管治](../Page/英國.md "wikilink")，1871年，香港政府下令禁止[賭博活動](../Page/賭博.md "wikilink")，所有賭館禁止營業，但打麻雀是粵人的應酬消閒娛樂，是故麻雀館則獲有限制保留。

[第二次世界大戰後](../Page/第二次世界大戰.md "wikilink")，政府於1956年規定麻雀館必須向[警察牌照課申請才可以經營](../Page/警察牌照課.md "wikilink")，而因為麻將共有144隻牌，政府便發出144個牌照。由於法例上禁止賭博，因此麻雀館牌照的英文名需要易名為「-{[麻雀學校](../Page/麻雀學校.md "wikilink")}-」（Mahjong
School）。

警察牌照課發出的牌照給與個別人士，是個人持有形式，沒有限期，可以承繼。現時改由[影視處管理](../Page/影視處.md "wikilink")，如果轉換持牌人的話，加上期限及其他條例。所以麻雀館牌照將會越來越少。

### 現況

現時，麻雀館的牌照名為「麻將／天九牌照」，由[民政事務局簽發](../Page/民政事務局.md "wikilink")，牌照持有人只可在指定地點及時間（中午12時至晚上12時）經營麻雀館。2006年，香港約有60間麻雀館營業，大部份開設於[旺角](../Page/旺角.md "wikilink")、[油麻地](../Page/油麻地.md "wikilink")、[灣仔](../Page/灣仔.md "wikilink")、及[北角](../Page/北角.md "wikilink")，另外有少部分位於[青山道及](../Page/青山道.md "wikilink")[荃灣舊區等地區](../Page/荃灣.md "wikilink")。

香港政府建議在2009年在香港所有娛樂場所，包括食肆、[酒吧](../Page/酒吧.md "wikilink")、[卡啦OK及麻雀館等禁止](../Page/卡啦OK.md "wikilink")[吸煙](../Page/吸煙.md "wikilink")，一度引起麻雀館業界的反對。

麻雀館主要靠「抽水」獲得收入。現在的麻雀館以服務吸引顧客，包括供應[茶](../Page/茶.md "wikilink")[水](../Page/水.md "wikilink")，甚至送[利市等](../Page/利市.md "wikilink")，亦以[閉路電視監察客人有否進行](../Page/閉路電視.md "wikilink")[出千及其他不法行為](../Page/詐騙.md "wikilink")，保障客人的利益及安全。

麻雀館亦稱「竹館」。在麻雀館內打麻雀通常主要規定：

  - 打法為大陸碰槓牌（跑馬仔），以鬥快糊牌為目標。
  - 槓牌可以馬上收錢。
  - 糊出者由出沖者全包。
  - 自摸收雙倍。
  - 打牌時不能隨便拆搭，否則有人糊出要包賠。
  - 糊出者負責付「抽水」。

## 日本麻雀館

日本的麻雀館稱為「雀莊」（雀荘，じゃんそう）。

## 參考資料

  - 香港[蘋果日報](../Page/蘋果日報.md "wikilink")，2006年4月20日
  - 《留學生入主麻雀館》，[香港商報](../Page/香港商報.md "wikilink")，2005年8月14日
  - 《[香港風華](../Page/香港風華.md "wikilink")》：竹館雀鳴，[亞洲電視](../Page/亞洲電視.md "wikilink")

[Category:麻將](../Category/麻將.md "wikilink")
[Category:娛樂場所](../Category/娛樂場所.md "wikilink")
[Category:香港博彩](../Category/香港博彩.md "wikilink")
[Category:香港文娛設施](../Category/香港文娛設施.md "wikilink")
[Category:中国文化](../Category/中国文化.md "wikilink")