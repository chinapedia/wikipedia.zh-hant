**執行時期**（）在[電腦科學中代表一個](../Page/電腦科學.md "wikilink")[電腦程式從開始執行到終止執行的運作](../Page/電腦程式.md "wikilink")、執行的時期。與執行時期相對的其他時期包括：[設计時期](../Page/設计時期.md "wikilink")（design
time）、[編譯時期](../Page/編譯時期.md "wikilink")（compile
time）、[鏈結時期](../Page/链接期.md "wikilink")（link
time）、與[載入時期](../Page/載入時期.md "wikilink")（load time）。

而[執行環境是一種為正在執行的程序或程式提供軟體服務的](../Page/運行環境.md "wikilink")[虛擬機械環境](../Page/虚拟机.md "wikilink")。它有可能是由作業系統自行提供，或由執行此程式的母程式提供。

通常由[作業系統負責處理程式的載入](../Page/作業系統.md "wikilink")：利用[載入器](../Page/載入器.md "wikilink")（loader）讀入程式碼，接著進行基本的[記憶體配置](../Page/記憶體.md "wikilink")，並視需要[聯結此程式指定的所有動態链接庫](../Page/链接器.md "wikilink")。有些程式語言會由此語言提供的運行環境處理上述工作。

程式碼的某些問題，只能在執行期間進行偵錯動作（或較有效率），例如[邏輯錯誤或](../Page/演算法.md "wikilink")[陣列邊際檢查等便屬此類](../Page/陣列.md "wikilink")。因此不管撰寫與測試得多麼精細，有些[錯誤必須在實際上線並處理真實資料的情況下才能找出](../Page/程序错误.md "wikilink")。因此，程式使用者也許會遇到諸如**執行時期錯誤**之類的訊息。

## 參閱

  - [執行期函式庫](../Page/執行期函式庫.md "wikilink")
  - [編譯時期](../Page/編譯時期.md "wikilink")（）與[编译器](../Page/编译器.md "wikilink")
  - [執行期型態訊息](../Page/執行期型態訊息.md "wikilink")
  - [綁紮](../Page/綁紮.md "wikilink")
  - [直譯器](../Page/直譯器.md "wikilink")
  - [運行環境](../Page/運行環境.md "wikilink")
      - [Apache Portable
        Runtime](../Page/Apache_Portable_Runtime.md "wikilink")、
      - [C標準函式庫](../Page/C標準函式庫.md "wikilink")
      - [C++標準函式庫](../Page/C++標準函式庫.md "wikilink")
      - [Common Language
        Runtime](../Page/Common_Language_Runtime.md "wikilink")
      - [Java Runtime Environment](../Page/JRE.md "wikilink")
      - [XULRunner](../Page/XULRunner.md "wikilink")

[Category:電腦術語](../Category/電腦術語.md "wikilink")