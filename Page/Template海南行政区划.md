[琼山区](../Page/琼山区.md "wikilink"){{.w}}[美兰区](../Page/美兰区.md "wikilink")
|group2 = [三亚市](../Page/三亚市.md "wikilink") |list2 =
[吉阳区](../Page/吉阳区.md "wikilink"){{.w}}[海棠区](../Page/海棠区.md "wikilink"){{.w}}[天涯区](../Page/天涯区.md "wikilink"){{.w}}[崖州区](../Page/崖州区.md "wikilink")
|group3 = [三沙市](../Page/三沙市.md "wikilink") |list3 =
[西沙群岛](../Page/西沙群岛.md "wikilink"){{.w}}[南沙群岛](../Page/南沙群岛.md "wikilink"){{.w}}[中沙群岛的岛礁及其海域](../Page/中沙群岛的岛礁及其海域.md "wikilink")
|group4 = [儋州市](../Page/儋州市.md "wikilink") |list4 =
[*无县级行政区*](../Page/:Template:海南省儋州市行政区划.md "wikilink") }}

|group3 = [省直辖
县级行政区](../Page/省直辖县级行政区.md "wikilink") |list3 =
[琼海市](../Page/琼海市.md "wikilink"){{.w}}[文昌市](../Page/文昌市.md "wikilink"){{.w}}[万宁市](../Page/万宁市.md "wikilink"){{.w}}[东方市](../Page/东方市.md "wikilink")
|group2 = [县](../Page/县.md "wikilink") |list2 =
[定安县](../Page/定安县.md "wikilink"){{.w}}[屯昌县](../Page/屯昌县.md "wikilink"){{.w}}[澄迈县](../Page/澄迈县.md "wikilink"){{.w}}[临高县](../Page/临高县.md "wikilink")
|group3 = [自治县](../Page/自治县.md "wikilink") |list3 =
[白沙黎族自治县](../Page/白沙黎族自治县.md "wikilink"){{.w}}[昌江黎族自治县](../Page/昌江黎族自治县.md "wikilink"){{.w}}[乐东黎族自治县](../Page/乐东黎族自治县.md "wikilink"){{.w}}[陵水黎族自治县](../Page/陵水黎族自治县.md "wikilink"){{.w}}[保亭黎族苗族自治县](../Page/保亭黎族苗族自治县.md "wikilink"){{.w}}[琼中黎族苗族自治县](../Page/琼中黎族苗族自治县.md "wikilink")}}

|group4 = 准地级[乡级行政区](../Page/类似乡级单位.md "wikilink") |list4 =

|belowstyle = text-align: left; font-size: 90%; |below =
注1：[儋州市的行政区划是地级市](../Page/儋州市.md "wikilink")、镇两级编-{制}-，没有县级编-{制}-。
注2：[三沙市存在领土争议](../Page/三沙市.md "wikilink")，部份岛礁分别由[中华民国](../Page/中华民国.md "wikilink")、[越南](../Page/越南.md "wikilink")、[菲律宾](../Page/菲律宾.md "wikilink")、[马来西亚实际控制](../Page/马来西亚.md "wikilink")。其行政区划是地级市独立一级编制，没有县、乡镇行政区划编制。
参见：[中华人民共和国县级以上行政区列表](../Page/中华人民共和国县级以上行政区列表.md "wikilink")、[海南省乡级以上行政区列表](../Page/海南省乡级以上行政区列表.md "wikilink")。
}}<noinclude>

</noinclude>

[Category:中华人民共和国各省级行政区行政区划模板](../Category/中华人民共和国各省级行政区行政区划模板.md "wikilink")
[\*](../Category/海南行政区划.md "wikilink")
[海南行政区划模板](../Category/海南行政区划模板.md "wikilink")