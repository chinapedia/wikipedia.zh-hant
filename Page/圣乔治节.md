**圣佐治日**即[4月23日](../Page/4月23日.md "wikilink")，是一个带有强烈[宗教色彩的纪念日](../Page/宗教.md "wikilink")，主要在一些将[圣佐治作为](../Page/圣佐治.md "wikilink")[守護聖者的](../Page/守護聖者.md "wikilink")[国家和地区间举行](../Page/国家.md "wikilink")，包括[英格兰](../Page/英格兰.md "wikilink")、[德国](../Page/德国.md "wikilink")、[格鲁吉亚](../Page/格鲁吉亚.md "wikilink")、[保加利亚](../Page/保加利亚.md "wikilink")、[葡萄牙以及](../Page/葡萄牙.md "wikilink")[加泰罗尼亚](../Page/加泰罗尼亚.md "wikilink")。在英国，圣佐治日同时也是[英格蘭地區的](../Page/英格蘭.md "wikilink")[国庆日](../Page/国庆日.md "wikilink")。公元[303年的这一天](../Page/303年.md "wikilink")，[圣佐治因试图阻止](../Page/圣佐治.md "wikilink")[罗马皇帝对](../Page/罗马皇帝.md "wikilink")[基督徒的迫害而](../Page/基督徒.md "wikilink")[殉道](../Page/殉道.md "wikilink")。

自[1969年以来](../Page/1969年.md "wikilink")，圣佐治日被[罗马教廷宣布不再必须作为](../Page/罗马教廷.md "wikilink")[公共假日](../Page/公共假日.md "wikilink")，但在[天主教范围内](../Page/天主教.md "wikilink")，圣佐治仍然作为相当重要的[圣徒而被纪念](../Page/圣徒.md "wikilink")。在加泰罗尼亚，圣佐治日为当地第二大的庆祝性公假日。在那一天，根据传统风俗，人们会向爱人送一支[玫瑰](../Page/玫瑰.md "wikilink")，向孩子送一本书。由于这一天根据不同历法，恰好也是[莎士比亚和](../Page/莎士比亚.md "wikilink")[塞万提斯逝世的日子](../Page/塞万提斯.md "wikilink")，因此这一天后来也被[联合国教育科学文化组织](../Page/联合国教育科学文化组织.md "wikilink")（UNESCO）记为[世界图书与版权日](../Page/世界图书与版权日.md "wikilink")。

圣佐治同时也是[童军活动的重要守護聖者](../Page/童军.md "wikilink")，在英国，很多童军團體在每年最接近圣佐治日的周日举办游行活动。

## 俄罗斯

## 格魯吉亞

格魯吉亞人稱聖佐治日為*Giorgoba*，每年11月23日（儒略曆11月10日）舉行慶祝，這一日對格魯吉亞人來說十分重要，學校和大學都會關門，所有人都会吃格魯吉亞傳統美食，并且到到教堂去庆祝这个节日。

## 外部链接

  - [Essay on the tradition of St George's
    Day](http://www.novareinna.com/festive/georgeday.html)
  - [St George's Day Events company
    website](https://web.archive.org/web/20060423165255/http://www.stgeorgesdayevents.org.uk/)
  - [The Royal Society of Saint George
    website](http://www.royalsocietyofstgeorge.com)
  - [St George's Day Events - Official England
    Website](http://www.enjoyengland.com/ideas/features/feature_pages/st-george-landing.aspx)

[Category:基督教节日](../Category/基督教节日.md "wikilink")
[Category:童軍節日](../Category/童軍節日.md "wikilink")