**宫鲁鸣**（），[山东](../Page/山东.md "wikilink")[牟平人](../Page/牟平.md "wikilink")，[中国](../Page/中国.md "wikilink")[篮球运动员](../Page/篮球.md "wikilink")、中国男子籃球隊主教练。

## 生平

宫身高172厘米，启蒙时期进入徐州业余体校。1974年入选[江苏青年篮球队](../Page/江苏.md "wikilink")，司职[组织后卫](../Page/组织后卫.md "wikilink")，不久进入江苏一线队，1981年、1985年和1988年三次入选[中国男篮](../Page/中国男篮.md "wikilink")，曾参加[汉城奥运会](../Page/汉城奥运会.md "wikilink")，为主力后卫[孙凤武的替补](../Page/孙凤武.md "wikilink")。

退役后，宫于1991年任国家男篮助理教练，协助[蒋兴权工作](../Page/蒋兴权.md "wikilink")。1995年任国家男篮主教练，率队在历史上首次打入[奥运会前八名](../Page/奥运会.md "wikilink")；1999年起任[中国女篮主教练](../Page/中国女篮.md "wikilink")。2014年起担任中国男篮主教练。

2015年率领中国男篮获得2015年亚洲篮球锦标赛冠军。

## 参考资料

  - [宫鲁鸣
    卧薪尝胆为明天](http://sports.eastday.com/epublish/gb/paper97/20010813/class009700005/hwz372886.htm)
  - [新浪体育·宫鲁鸣](http://sports.sina.com.cn/k/gongluming/)

[L鲁](../Category/宫姓.md "wikilink")
[Category:牟平人](../Category/牟平人.md "wikilink")
[Category:中国男子篮球运动员](../Category/中国男子篮球运动员.md "wikilink")
[Category:中国奥运篮球运动员](../Category/中国奥运篮球运动员.md "wikilink")
[Category:中国篮球教练](../Category/中国篮球教练.md "wikilink")
[Category:烟台籍运动员](../Category/烟台籍运动员.md "wikilink")
[Category:1988年夏季奥林匹克运动会篮球运动员](../Category/1988年夏季奥林匹克运动会篮球运动员.md "wikilink")
[Category:中国篮球协会副主席](../Category/中国篮球协会副主席.md "wikilink")