**韮川站**（）是一個位於[群馬縣](../Page/群馬縣.md "wikilink")[太田市台之鄉町](../Page/太田市.md "wikilink")，屬於[東武鐵道](../Page/東武鐵道.md "wikilink")[伊勢崎線的](../Page/伊勢崎線.md "wikilink")[鐵路車站](../Page/鐵路車站.md "wikilink")。車站編號是**TI
17**。

1932年（[昭和](../Page/昭和.md "wikilink")7年）8月當時在的車站開設費用是1,500日圓\[1\]，同年10月25日開業\[2\]。

## 歷史

  - 1932年（[昭和](../Page/昭和.md "wikilink")7年）10月25日 - 開業。

## 車站結構

[島式月台](../Page/島式月台.md "wikilink")1面2線的高架車站。

### 月台配置

<table>
<thead>
<tr class="header">
<th><p>月台</p></th>
<th><p>路線</p></th>
<th><p>方向</p></th>
<th><p>目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Tobu_Isesaki_Line_(TI)_symbol.svg" title="fig:Tobu_Isesaki_Line_(TI)_symbol.svg">Tobu_Isesaki_Line_(TI)_symbol.svg</a> 伊勢崎線</p></td>
<td><p>下行</p></td>
<td><p><a href="../Page/太田站_(群馬縣).md" title="wikilink">太田方向</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>上行</p></td>
<td><p><a href="../Page/館林站.md" title="wikilink">館林</a>、<a href="../Page/久喜站.md" title="wikilink">久喜</a>、<span style="font-size:small"><a href="https://zh.wikipedia.org/wiki/File:Tobu_Skytree_Line_(TS)_symbol.svg" title="fig:Tobu_Skytree_Line_(TS)_symbol.svg">Tobu_Skytree_Line_(TS)_symbol.svg</a> <a href="../Page/伊勢崎線.md" title="wikilink">東武晴空塔線</a></span> <a href="../Page/北千住站.md" title="wikilink">北千住</a>、<br />
<a href="../Page/東京晴空塔站.md" title="wikilink">東京晴空塔</a>、<a href="../Page/淺草站.md" title="wikilink">淺草方向</a></p></td>
<td></td>
</tr>
</tbody>
</table>

## 使用情況

2016年度1日平均上下車人次為**2,284人**\[3\]。

近年1日平均上下車人次的推移如下表\[4\]｡

| 年度           | 1日平均上下車人次\[5\] |
| ------------ | -------------- |
| 2003年（平成15年） | 1,963          |
| 2004年（平成16年） | 2,072          |
| 2005年（平成17年） | 2,072          |
| 2006年（平成18年） | 2,014          |
| 2007年（平成19年） | 1,993          |
| 2008年（平成20年） | 2,044          |
| 2009年（平成21年） | 2,016          |
| 2010年（平成22年） | 1,969          |
| 2011年（平成23年） | 1,933          |
| 2012年（平成24年） | 2,064          |
| 2013年（平成25年） | 2,294          |
| 2014年（平成26年） | 2,473\[6\]     |
| 2015年（平成27年） | 2,473          |
| 2016年（平成28年） | 2,284          |

## 相鄰車站

  - [Tōbu_Tetsudō_Logo.svg](https://zh.wikipedia.org/wiki/File:Tōbu_Tetsudō_Logo.svg "fig:Tōbu_Tetsudō_Logo.svg")
    東武鐵道
    [Tobu_Isesaki_Line_(TI)_symbol.svg](https://zh.wikipedia.org/wiki/File:Tobu_Isesaki_Line_\(TI\)_symbol.svg "fig:Tobu_Isesaki_Line_(TI)_symbol.svg")
    伊勢崎線
      -

        區間急行、區間準急（平日只有1班上行列車）、普通

          -
            [野州山邊](../Page/野州山邊站.md "wikilink")（TI-16）－**韮川（TI-17）**－[太田](../Page/太田站_\(群馬縣\).md "wikilink")（TI-18）

## 參考資料

## 外部連結

  - [東武鐵道 韮川站](http://www.tobu.co.jp/station/info/1707.html)

[Ragawa](../Category/日本鐵路車站_Ni.md "wikilink")
[Category:群馬縣鐵路車站](../Category/群馬縣鐵路車站.md "wikilink")
[Category:伊勢崎線車站](../Category/伊勢崎線車站.md "wikilink")
[Category:太田市](../Category/太田市.md "wikilink")
[Category:1932年啟用的鐵路車站](../Category/1932年啟用的鐵路車站.md "wikilink")

1.  現在的紙幣價值相當於1000萬日圓以上

2.
3.
4.  [1](http://web.archive.org/web/*/http://railway.tobu.co.jp/guide/station/info/1707.html)

5.  [各種報告書 - 関東交通広告協議会](http://www.train-media.net/report/index.html)

6.