[405px-WP_Clemens_von_Ketteler.jpg](https://zh.wikipedia.org/wiki/File:405px-WP_Clemens_von_Ketteler.jpg "fig:405px-WP_Clemens_von_Ketteler.jpg")

**克莱门斯·冯·克林德男爵**（，），是1900年[义和团事变期间在](../Page/义和团事变.md "wikilink")[北京街头被清军杀死的](../Page/北京.md "wikilink")[德國駐華公使](../Page/德國駐華公使.md "wikilink")。克林德被殺違反了[國際法](../Page/國際法.md "wikilink")（如清廷熟悉的《[萬國公法](../Page/萬國公法.md "wikilink")》）中「外交人員人身不可受侵犯」的原則。\[1\]这一轰动事件成为[八国联军的導火線之一](../Page/八国联军.md "wikilink")。

## 生平

克林德出生在德国[明斯特](../Page/明斯特.md "wikilink")，接受的是军事教育，1881年，他辞去军职进入外交部门，并被派往[中国工作](../Page/中国.md "wikilink")。

他到中國后曾任驻[广州和](../Page/广州.md "wikilink")[天津等地领事](../Page/天津.md "wikilink")，1889年回德。以后去[美国](../Page/美国.md "wikilink")（1892年－1896年）和[墨西哥](../Page/墨西哥.md "wikilink")（1896年－1899年）任职，1897年他與美国公司總經理的女兒Maude
Ledyard結婚\[2\]。1899年4月他再到中国，升任[德意志帝国驻华](../Page/德意志帝国.md "wikilink")[公使](../Page/公使.md "wikilink")。

1900年5月31日至6月3日，八國警衛約400人進入北京保護使館區及[西什庫教堂](../Page/西什庫教堂.md "wikilink")。

6月6日，[直隸](../Page/直隸.md "wikilink")[總督](../Page/總督.md "wikilink")[裕禄电告总理衙门](../Page/裕禄.md "wikilink")，称[義和团民自焚毁铁路电杆之后愈加猖獗](../Page/義和团.md "wikilink")，明目张膽，不服劝谕，也在天津焚杀教民；虽芦保、津芦铁路有兵分守，仍肆意焚毁，在各处拆毁教堂，各国洋人已甚忧愤，“而天津租界之洋人尤感惊恐，屡以中国办理太鬆，欲派兵助剿为言，此时我军自行剿办，尚可操纵自如，若至外人干预，则事更难措手”，目睹现在情况，拳民断非劝导所能解散，“趁此匪势初起，必须剿抚并用，尚可剋期而定”。

6月10日，义和团大规模开进[北京](../Page/北京.md "wikilink")，而北京與外界的電報通訊因電線桿被義和團破壞而中斷。各國政府因無法與使館聯絡而商組聯軍進京保衛使館。

6月11日，发生[日本使館書記](../Page/日本.md "wikilink")[杉山彬在](../Page/杉山彬.md "wikilink")[永定門外被](../Page/永定門.md "wikilink")[甘軍杀死的事件](../Page/甘軍.md "wikilink")，同日列強在天津組成前往北京，途中遭到清軍及義和團的阻擊。

6月16日，[前門一帶約千家](../Page/前門.md "wikilink")（一說四千家\[3\]）商舖被義和團民燒成廢墟。慈禧召開御前會議後，亦曾發出勒令解散拳民的上諭。不料到了翌日（6月17日）收到洋人出兵的同時還要求她歸政於[光緒帝的虛假情報](../Page/光緒帝.md "wikilink")\[4\]。於是慈禧態度作出一百八十度轉變。

## 遇害

[Aerial_view_of_Beijing_in_1902_at_the_scene_of_Ambassador_Von_Ketteler%27s_murder.jpg](https://zh.wikipedia.org/wiki/File:Aerial_view_of_Beijing_in_1902_at_the_scene_of_Ambassador_Von_Ketteler%27s_murder.jpg "fig:Aerial_view_of_Beijing_in_1902_at_the_scene_of_Ambassador_Von_Ketteler%27s_murder.jpg")
6月19日，清政府[总理衙门照会各国驻华使节](../Page/总理衙门.md "wikilink")「限二十四点鍾內各國一切人等均需离京」。当晚，各国公使联名致函总理衙门，以路途安全无保障为由，要求延缓离京日期，并要求次日上午9时前给予答复。

6月20日上午8时，克林德未能说服各国公使一同行动，遂独自帶同翻译乘轿从[东交民巷使馆](../Page/东交民巷.md "wikilink")，前往[东单牌楼北大街](../Page/东单牌楼北大街.md "wikilink")[东堂子胡同](../Page/东堂子胡同.md "wikilink")[总理衙门](../Page/总理衙门.md "wikilink")（现为[中華人民共和國公安部人民来访接待室](../Page/中華人民共和國公安部.md "wikilink")）交涉。克林德決定不帶四名德國衛兵，兩乘轎子只有兩名沒有武裝的馬伕隨行。柯達士看到克林德腰上沒有掛平常帶的[左輪手槍](../Page/左輪手槍.md "wikilink")，也就沒有帶槍。途中走到[东单牌楼北大街](../Page/东单牌楼北大街.md "wikilink")[西总布胡同西口](../Page/西总布胡同.md "wikilink")（距[东堂子胡同只隔一条](../Page/东堂子胡同.md "wikilink")[石大人胡同](../Page/石大人胡同.md "wikilink")（今[外交部街](../Page/外交部街.md "wikilink")）），為正在巡逻的[神机营](../Page/神机营.md "wikilink")（就在距此不远的[煤渣胡同今](../Page/煤渣胡同.md "wikilink")[基督教青年会](../Page/基督教青年会.md "wikilink")（[中华圣经会旧址](../Page/中华圣经会.md "wikilink")）处）霆字枪队[章京](../Page/章京.md "wikilink")[恩海擊斃](../Page/恩海.md "wikilink")，柯达士受伤，轎伕逃走。\[5\]\[6\]據前導馬伕劉玉成事後對德國政府的供詞，他聽到幾聲幾乎是同時的槍響，回頭看到一隊清軍手持步槍，憤怒的推撞克林德的轎子，而克林德在轎子中一動不動。\[7\]

## 事後

[Grab_Klemens_von_Ketteler.jpg](https://zh.wikipedia.org/wiki/File:Grab_Klemens_von_Ketteler.jpg "fig:Grab_Klemens_von_Ketteler.jpg")中央公墓的克林德墓\]\]
6月21日，清政府以[光绪帝的名義](../Page/光绪帝.md "wikilink")，向[英國](../Page/大不列顛及愛爾蘭聯合王國.md "wikilink")、[美國](../Page/美國.md "wikilink")、[法國](../Page/法蘭西第三共和國.md "wikilink")、[德國](../Page/德意志帝國.md "wikilink")、[義國](../Page/意大利王國.md "wikilink")、[日本](../Page/大日本帝國.md "wikilink")、[俄國](../Page/俄羅斯帝國.md "wikilink")、[奧匈](../Page/奧匈帝國.md "wikilink")、[西班牙](../Page/西班牙王國.md "wikilink")、[比國](../Page/比利時王國.md "wikilink")、[荷蘭十一國同時宣戰](../Page/荷蘭王國.md "wikilink")。在清廷向各國宣戰的同時，也懸賞捕殺洋人，規定“殺一洋人賞五十兩；洋婦四十兩；洋孩三十兩”\[8\]。義和團及朝廷軍隊圍攻各國在北京的使館。

克林德被殺事件发生后，[德国皇帝](../Page/德国皇帝.md "wikilink")[威廉二世决意报复中国](../Page/威廉二世_\(德国\).md "wikilink")，正式派遣2万多人的对华远征军，由[瓦德西元帅指挥](../Page/瓦德西.md "wikilink")。不過這支部隊尚未抵達中國，戰爭已經結束。

[Enhai.jpg](https://zh.wikipedia.org/wiki/File:Enhai.jpg "fig:Enhai.jpg")
擊斃克林德的[神机营](../Page/神机营.md "wikilink")[章京](../Page/章京.md "wikilink")[恩海被德國判處](../Page/恩海.md "wikilink")[死刑](../Page/死刑.md "wikilink")，1900年12月31日，依[中國慣例](../Page/中國.md "wikilink")[斬首](../Page/斬首.md "wikilink")。

[Peking_Kettler_Monument.jpg](https://zh.wikipedia.org/wiki/File:Peking_Kettler_Monument.jpg "fig:Peking_Kettler_Monument.jpg")
1901年清朝战败以后，与11国签订[辛丑条约](../Page/辛丑条约.md "wikilink")，第一款就是：清廷派[醇亲王](../Page/醇亲王.md "wikilink")[载沣](../Page/载沣.md "wikilink")（1883年―1951年，光绪之弟、[溥仪之父](../Page/溥仪.md "wikilink")）赴德国就克林德公使被杀一事向德国皇帝道歉，并在克林德被杀地点修建一座品级相当的石牌坊。1901年9月4日，专使醇亲王载沣在[柏林代表](../Page/柏林.md "wikilink")[光绪皇帝向德国皇帝道歉](../Page/光绪.md "wikilink")。“克林德碑”牌坊横跨在繁华的东单北大街上，于1901年6月25日开工，1903年1月8日竣工，是[汉白玉蓝](../Page/汉白玉.md "wikilink")[琉璃瓦](../Page/琉璃瓦.md "wikilink")[庑殿顶式](../Page/庑殿顶.md "wikilink")，形制是四柱三间七楼，碑文用[拉丁语](../Page/拉丁语.md "wikilink")、[德语](../Page/德语.md "wikilink")、[汉语三种文字](../Page/汉语.md "wikilink")，表达清朝皇帝对克林德被杀的惋惜。1918年11月11日，[第一次世界大战结束](../Page/第一次世界大战.md "wikilink")，德国战败，1918年11月13日，[中華民國北京政府将牌坊迁往中央公园](../Page/中華民國北京政府.md "wikilink")（今[中山公园](../Page/北京中山公園.md "wikilink")），将坊额改为“公理战胜”，但规模缩小为三楼。1920年7月4日，[中華民國北京政府在中央公园内为新落成的纪念坊举行落成典礼](../Page/中華民國北京政府.md "wikilink")。1953年10月，在北京召开期间，再次改名为[保衛和平坊](../Page/保衛和平坊.md "wikilink")。

## 参见

  - [义和团运动](../Page/义和团运动.md "wikilink")
  - [恩海](../Page/恩海.md "wikilink")

## 注释与参考文献

<references />

  - [唐德剛](../Page/唐德剛.md "wikilink")：《晚清七十年：義和團與八國聯軍》 ISBN 957-32-3514-5

  - [袁偉時](../Page/袁偉時.md "wikilink")：[尋求真相:为何、何时、如何「反帝反封建」？](https://web.archive.org/web/20080506030612/http://56cun.blogsome.com/2006/04/07/%E4%B8%BA%E4%BD%95%E3%80%81%E4%BD%95%E6%97%B6%E3%80%81%E5%A6%82%E4%BD%95%E3%80%8C%E5%8F%8D%E5%B8%9D%E5%8F%8D%E5%B0%81%E5%BB%BA%E3%80%8D%EF%B9%96%E8%A2%81%E4%BC%9F%E6%97%B6/)

  -
## 外部链接

  - [克林德之死](http://www.deutsche-schutzgebiete.de/ketteler.htm) （德语）

[Category:八國聯軍人物](../Category/八國聯軍人物.md "wikilink")
[Category:德国外交官](../Category/德国外交官.md "wikilink")
[Category:在國外被謀殺身亡德國人](../Category/在國外被謀殺身亡德國人.md "wikilink")
[Category:波茨坦人](../Category/波茨坦人.md "wikilink")

1.

2.

3.  唐德剛：《晚清七十年：義和團與八國聯軍》 ISBN 957-32-3514-5

4.
5.

6.

7.

8.