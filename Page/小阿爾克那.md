**小阿爾克那**（**Minor
Arcana**），在[塔羅牌中一般俗稱為](../Page/塔羅牌.md "wikilink")「小奧秘」或「小牌」，共包含56張牌，編號由Ace至10、及四張宮廷牌，通常牌義都較[大阿爾克那仔細和實在](../Page/大阿爾克那.md "wikilink")，所以亦有人會只用小牌作占卜。牌義可以從牌的牌組和數字或宮廷身份推斷出來，但只是很有參考作用的資料，雖然對於初學者記憶牌義很有幫助，但每張牌的意義最好還是根據牌本身的圖像推斷。

## 四種花色與涵義

小阿爾克那的花色和[歐洲中古代](../Page/歐洲.md "wikilink")[封建社會的](../Page/封建社會.md "wikilink")[士農工商跟](../Page/士農工商.md "wikilink")[四大元素有著不小的相對關係](../Page/四元素說.md "wikilink")。

  - 權仗：梅花 - 農民（成長與精神）- 火（有形無體）
  - 聖杯：愛心 - 聖職（學問與倫理）- 水（無形有體）
  - 劍　：桃花 - 武士（力量與法律）- 風（無形無體）
  - 金幣：方磚 - 商人（流通與財富）- 土（有形有體）

## 代號與意義

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/Ace_(塔羅牌).md" title="wikilink">Ace (塔羅牌)</a></li>
<li><a href="../Page/2_(塔羅牌).md" title="wikilink">2 (塔羅牌)</a></li>
<li><a href="../Page/3_(塔羅牌).md" title="wikilink">3 (塔羅牌)</a></li>
<li><a href="../Page/4_(塔羅牌).md" title="wikilink">4 (塔羅牌)</a></li>
<li><a href="../Page/5_(塔羅牌).md" title="wikilink">5 (塔羅牌)</a></li>
<li><a href="../Page/6_(塔羅牌).md" title="wikilink">6 (塔羅牌)</a></li>
<li><a href="../Page/7_(塔羅牌).md" title="wikilink">7 (塔羅牌)</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/8_(塔羅牌).md" title="wikilink">8 (塔羅牌)</a></li>
<li><a href="../Page/9_(塔羅牌).md" title="wikilink">9 (塔羅牌)</a></li>
<li><a href="../Page/10_(塔羅牌).md" title="wikilink">10 (塔羅牌)</a></li>
<li>侍從</li>
<li>騎士</li>
<li>皇后</li>
<li>國王</li>
</ul></td>
</tr>
</tbody>
</table>

## 牌卡介紹

最通用的塔羅牌[偉特塔羅牌中的牌組如下](../Page/偉特塔羅牌.md "wikilink")：

<table>
<tbody>
<tr class="odd">
<td><p>　</p>
<ol>
<li>A</li>
<li>　</li>
<li>　</li>
<li>　</li>
<li>　</li>
<li>　</li>
<li>　</li>
<li>　</li>
<li>　</li>
<li>　</li>
<li>侍從</li>
<li>騎士</li>
<li>皇后</li>
<li>國王</li>
</ol></td>
<td><p>權杖</p>
<ul>
<li><a href="../Page/權杖A.md" title="wikilink">權杖A</a></li>
<li><a href="../Page/權杖2.md" title="wikilink">權杖2</a></li>
<li><a href="../Page/權杖3.md" title="wikilink">權杖3</a></li>
<li><a href="../Page/權杖4.md" title="wikilink">權杖4</a></li>
<li><a href="../Page/權杖5.md" title="wikilink">權杖5</a></li>
<li><a href="../Page/權杖6.md" title="wikilink">權杖6</a></li>
<li><a href="../Page/權杖7.md" title="wikilink">權杖7</a></li>
<li><a href="../Page/權杖8.md" title="wikilink">權杖8</a></li>
<li><a href="../Page/權杖9.md" title="wikilink">權杖9</a></li>
<li><a href="../Page/權杖10.md" title="wikilink">權杖10</a></li>
<li><a href="../Page/權杖侍從.md" title="wikilink">權杖侍從</a></li>
<li><a href="../Page/權杖騎士.md" title="wikilink">權杖騎士</a></li>
<li><a href="../Page/權杖皇后.md" title="wikilink">權杖皇后</a></li>
<li><a href="../Page/權杖國王.md" title="wikilink">權杖國王</a></li>
</ul></td>
<td><p>聖杯</p>
<ul>
<li><a href="../Page/聖杯A.md" title="wikilink">聖杯A</a></li>
<li><a href="../Page/聖杯2.md" title="wikilink">聖杯2</a></li>
<li><a href="../Page/聖杯3.md" title="wikilink">聖杯3</a></li>
<li><a href="../Page/聖杯4.md" title="wikilink">聖杯4</a></li>
<li><a href="../Page/聖杯5.md" title="wikilink">聖杯5</a></li>
<li><a href="../Page/聖杯6.md" title="wikilink">聖杯6</a></li>
<li><a href="../Page/聖杯7.md" title="wikilink">聖杯7</a></li>
<li><a href="../Page/聖杯8.md" title="wikilink">聖杯8</a></li>
<li><a href="../Page/聖杯9.md" title="wikilink">聖杯9</a></li>
<li><a href="../Page/聖杯10.md" title="wikilink">聖杯10</a></li>
<li><a href="../Page/聖杯侍從.md" title="wikilink">聖杯侍從</a></li>
<li><a href="../Page/聖杯騎士.md" title="wikilink">聖杯騎士</a></li>
<li><a href="../Page/聖杯皇后.md" title="wikilink">聖杯皇后</a></li>
<li><a href="../Page/聖杯國王.md" title="wikilink">聖杯國王</a></li>
</ul></td>
<td><p>寶劍</p>
<ul>
<li><a href="../Page/寶劍A.md" title="wikilink">寶劍A</a></li>
<li><a href="../Page/寶劍2.md" title="wikilink">寶劍2</a></li>
<li><a href="../Page/寶劍3.md" title="wikilink">寶劍3</a></li>
<li><a href="../Page/寶劍4.md" title="wikilink">寶劍4</a></li>
<li><a href="../Page/寶劍5.md" title="wikilink">寶劍5</a></li>
<li><a href="../Page/寶劍6.md" title="wikilink">寶劍6</a></li>
<li><a href="../Page/寶劍7.md" title="wikilink">寶劍7</a></li>
<li><a href="../Page/寶劍8.md" title="wikilink">寶劍8</a></li>
<li><a href="../Page/寶劍9.md" title="wikilink">寶劍9</a></li>
<li><a href="../Page/寶劍10.md" title="wikilink">寶劍10</a></li>
<li><a href="../Page/寶劍侍從.md" title="wikilink">寶劍侍從</a></li>
<li><a href="../Page/寶劍騎士.md" title="wikilink">寶劍騎士</a></li>
<li><a href="../Page/寶劍皇后.md" title="wikilink">寶劍皇后</a></li>
<li><a href="../Page/寶劍國王.md" title="wikilink">寶劍國王</a></li>
</ul></td>
<td><p>錢幣</p>
<ul>
<li><a href="../Page/錢幣A.md" title="wikilink">錢幣A</a></li>
<li><a href="../Page/錢幣2.md" title="wikilink">錢幣2</a></li>
<li><a href="../Page/錢幣3.md" title="wikilink">錢幣3</a></li>
<li><a href="../Page/錢幣4.md" title="wikilink">錢幣4</a></li>
<li><a href="../Page/錢幣5.md" title="wikilink">錢幣5</a></li>
<li><a href="../Page/錢幣6.md" title="wikilink">錢幣6</a></li>
<li><a href="../Page/錢幣7.md" title="wikilink">錢幣7</a></li>
<li><a href="../Page/錢幣8.md" title="wikilink">錢幣8</a></li>
<li><a href="../Page/錢幣9.md" title="wikilink">錢幣9</a></li>
<li><a href="../Page/錢幣10.md" title="wikilink">錢幣10</a></li>
<li><a href="../Page/錢幣侍從.md" title="wikilink">錢幣侍從</a></li>
<li><a href="../Page/錢幣騎士.md" title="wikilink">錢幣騎士</a></li>
<li><a href="../Page/錢幣皇后.md" title="wikilink">錢幣皇后</a></li>
<li><a href="../Page/錢幣國王.md" title="wikilink">錢幣國王</a></li>
</ul></td>
</tr>
</tbody>
</table>

[CATEGORY:塔羅牌](../Page/CATEGORY:塔羅牌.md "wikilink")