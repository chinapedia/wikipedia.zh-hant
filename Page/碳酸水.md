[Drinking_glass_00118.gif](https://zh.wikipedia.org/wiki/File:Drinking_glass_00118.gif "fig:Drinking_glass_00118.gif")
**碳酸水**（），又稱梳打水、蘇打水，汽泡水，是在壓力下將二氧化碳氣體溶入水中的飲料。有時加入一些添加劑，如鈉。這個過程，被稱為碳酸化，它使水可以起泡。人們喜歡喝飲料，碳酸水則可以提供無熱量和無糖的替代品。碳酸水絕大多數用[飲料瓶出售](../Page/飲料瓶.md "wikilink")，但在家裡它很容易用[蘇打機製造出來](../Page/蘇打機.md "wikilink")。

## 名词辨析

碳酸水或苏打水在英语中有多种称谓，如club soda、soda water、sparkling water、seltzer
water等。这些其实都是一个东西。称之为“soda”只是英语中本身就有此意，和是否添加[小苏打并无直接对应关系](../Page/小苏打.md "wikilink")。近年来多有商家炒作概念试图区分苏打水（添加[碳酸氢钠](../Page/碳酸氢钠.md "wikilink")（即小苏打，烘焙苏打）的）和碳酸水（只加二氧化碳的），属于[望文生义](../Page/望文生义.md "wikilink")。当然事实上，由于碳酸水呈偏酸，确有部分品牌会添加[碱](../Page/碱.md "wikilink")[盐](../Page/盐_\(化学\).md "wikilink")（常见的有小苏打[碳酸氢钠](../Page/碳酸氢钠.md "wikilink")、[碳酸氢钾](../Page/碳酸氢钾.md "wikilink")、[柠檬酸钾等](../Page/柠檬酸钾.md "wikilink")）来改善口感。但是并不能说只有添加了小苏打的才叫“苏打水”。

## 歷史

[Trinkwasserbesprudler.jpg](https://zh.wikipedia.org/wiki/File:Trinkwasserbesprudler.jpg "fig:Trinkwasserbesprudler.jpg")
[Soda_gun.jpg](https://zh.wikipedia.org/wiki/File:Soda_gun.jpg "fig:Soda_gun.jpg")
從前，碳酸水由一家[德國](../Page/德國.md "wikilink")[賽爾脫茲的天然](../Page/賽爾脫茲.md "wikilink")[汽泡](../Page/汽泡.md "wikilink")[礦泉水](../Page/礦泉水.md "wikilink")[公司生產](../Page/公司.md "wikilink")，他們將水放入[瓶中](../Page/瓶.md "wikilink")，然後加入[碳酸氫鈉製成](../Page/碳酸氫鈉.md "wikilink")。根據[蘇打俱樂部出產的碳酸水瓶上標示之製成分量](../Page/蘇打俱樂部.md "wikilink")，也是用小量的[食鹽](../Page/食鹽.md "wikilink")、[檸檬酸鈉](../Page/檸檬酸鈉.md "wikilink")、碳酸氫鈉、[碳酸氫銨](../Page/碳酸氫銨.md "wikilink")、[硫酸鉀或](../Page/硫酸鉀.md "wikilink")[磷酸鹽製成](../Page/磷酸鹽.md "wikilink")，這些添加劑使得碳酸水略帶[鹹味](../Page/鹹.md "wikilink")。

[英國](../Page/英國.md "wikilink")[化學家](../Page/化學家.md "wikilink")[約瑟夫·普利斯特里為發現碳酸水製作方法的第一人](../Page/約瑟夫·普利斯特里.md "wikilink")。他在[利茲的一間](../Page/利茲.md "wikilink")[啤酒廠中](../Page/啤酒.md "wikilink")，將一碗水放在[啤酒桶之上的一個地方](../Page/啤酒桶.md "wikilink")。之後，二氧化碳和[氮兩種](../Page/氮.md "wikilink")[氣體便溶入](../Page/氣體.md "wikilink")[發酵啤酒桶上的水](../Page/發酵.md "wikilink")[碗](../Page/碗.md "wikilink")。當時，人們多是用二氧化碳和氮來滅[鼠](../Page/鼠.md "wikilink")。約瑟夫·普利斯特里意想不到，水加入了氣體之後有更宜人的口感，於是他為[朋友提供碳酸水作一種新的](../Page/朋友.md "wikilink")[飲料](../Page/飲料.md "wikilink")。1772年，約瑟夫·普利斯特里[出版了一本](../Page/出版.md "wikilink")[書](../Page/書.md "wikilink")《水与氣之結合》，描述把[硫酸滴在](../Page/硫酸.md "wikilink")[白堊上會釋放二氧化碳](../Page/白堊.md "wikilink")，並且鼓勵人們用盛滿水的碗收集氣體。

1771年，[瑞典化學家](../Page/瑞典.md "wikilink")[盧本·貝格曼獨立](../Page/盧本·貝格曼.md "wikilink")[發明一種與約瑟夫](../Page/發明.md "wikilink")·普利斯特里發現碳酸水製作方法相似的製造過程。但不太成功，他設法再生產自然地[冒泡的](../Page/碳酸化.md "wikilink")[泉水](../Page/泉水.md "wikilink")，當時認為冒泡的泉水對[健康有益](../Page/健康.md "wikilink")。

現在，碳酸水都是由被[加壓的二氧化碳通過水做成](../Page/加壓.md "wikilink")。加壓之[壓力比](../Page/壓力.md "wikilink")[標準大氣壓力更強](../Page/標準大氣壓力.md "wikilink")，使其增加[溶解度](../Page/溶解度.md "wikilink")，令更多二氧化碳溶入。當[瓶蓋打開](../Page/瓶蓋.md "wikilink")，壓力被釋放，氣體形成[汽泡出來](../Page/汽泡.md "wikilink")，如此形成獨特[泡沫](../Page/泡沫.md "wikilink")。

## 用處

碳酸水最出名的好處是去除[污跡](../Page/污跡.md "wikilink")，例如[咖啡杯中的](../Page/咖啡杯.md "wikilink")[咖啡污跡](../Page/咖啡.md "wikilink")，或[銀的污跡](../Page/銀.md "wikilink")。

今天，碳酸水在雜貨店有售，製作亦非常簡單。

[軟飲料和](../Page/軟飲料.md "wikilink")[碳酸飲料](../Page/碳酸飲料.md "wikilink")（[汽水](../Page/汽水.md "wikilink")、[可樂](../Page/可樂.md "wikilink")、[沙士](../Page/沙士_\(飲料\).md "wikilink")、[雪碧](../Page/雪碧.md "wikilink")）其主要材料就是碳酸水。

## 參見

  - [約瑟夫·普利斯特里](../Page/約瑟夫·普利斯特里.md "wikilink")
  - [盧本·貝格曼](../Page/盧本·貝格曼.md "wikilink")
  - [蘇打俱樂部](../Page/蘇打俱樂部.md "wikilink")
  - [礦泉水](../Page/礦泉水.md "wikilink")
  - [可樂加曼陀珠噴發現象](../Page/可樂加曼陀珠噴發現象.md "wikilink")

## 外部連結

  - [The Priestley Society](http://www.priestleysociety.net)
  - [Joseph Priestley Information
    Website](https://web.archive.org/web/20170604052848/http://josephpriestley.info/)
  - [Priestley's paper *Impregnating Water with Fixed Air*
    1772](https://web.archive.org/web/20060616114754/http://dbhs.wvusd.k12.ca.us/webdocs/Chem-History/Priestley-1772/Priestley-1772-Start.html)
  - [Bartleby](http://www.bartleby.com/65/ca/carbonat-bev.html)
  - [interview with one of New York City's last seltzer delivery
    men](http://www.radiodiaries.org/transcripts/NewYorkWorks/seltzerman.html)

[Category:碳酸飲料](../Category/碳酸飲料.md "wikilink")
[Category:碳酸水](../Category/碳酸水.md "wikilink")
[Category:英格兰发明](../Category/英格兰发明.md "wikilink")
[Category:工业气体](../Category/工业气体.md "wikilink")
[Category:软饮料](../Category/软饮料.md "wikilink")
[Category:飲用水](../Category/飲用水.md "wikilink")