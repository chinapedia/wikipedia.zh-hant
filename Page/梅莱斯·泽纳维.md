**梅莱斯·泽纳维·阿斯拉斯**（[吉茲字母](../Page/吉茲字母.md "wikilink")：，[罗马化](../Page/罗马化.md "wikilink")：Mäläs
Zenawi
Äsräs；），[衣索比亞政治家](../Page/衣索比亞.md "wikilink")，1991年至2012年該國的實際最高領導人。

## 经历

1974年，他被[海尔·塞拉西一世大学开除](../Page/亚的斯亚贝巴大学.md "wikilink")，并于同年与其他学生组织了提格雷民族组织（1975年改组为[提格雷人民解放阵线](../Page/提格雷人民解放阵线.md "wikilink")）。

1980年代中期，他在提格雷人民解放阵线内部创建奉行[霍查主义路线的](../Page/霍查主义.md "wikilink")[提格雷马列主义联盟](../Page/提格雷马列主义联盟.md "wikilink")，并开始成为反对[门格斯图独裁政权的](../Page/门格斯图.md "wikilink")[武装斗争的领袖](../Page/埃塞俄比亚内战.md "wikilink")。

1991年至1995年曾任埃塞俄比亚过渡总统，此后一直擔任总理，直至去世。

2002年，他因和平解決與[厄立特里亞邊界的爭端](../Page/厄立特里亞.md "wikilink")，獲得[世界和平獎元首級的最高榮譽獎](../Page/世界和平獎.md "wikilink")\[1\]。

2012年8月21日，梅莱斯·泽纳维在[布鲁塞尔的一家医院逝世](../Page/布鲁塞尔.md "wikilink")，享年57岁\[2\]。根据埃塞俄比亚宪法，副总理[海尔马里亚姆·德萨莱尼代行总理职务](../Page/海尔马里亚姆·德萨莱尼.md "wikilink")\[3\]\[4\]。9月2日，埃塞俄比亚为泽纳维举行了[国葬](../Page/国葬.md "wikilink")，成为该国80多年来首次为领导人举行的国葬；之后梅莱斯的[遗体将安葬在圣三一教堂](../Page/遗体.md "wikilink")\[5\]。

## 教育与个人生活

1995年获得了[英国公开大学的](../Page/英国公开大学.md "wikilink")[MBA](../Page/MBA.md "wikilink")，2004年取得荷兰[鹿特丹大学的经济学硕士](../Page/鹿特丹大学.md "wikilink")\[6\]。2002年7月荣获[韩国](../Page/韩国.md "wikilink")[韓南大學的名誉政治学博士](../Page/韓南大學.md "wikilink")\[7\]。妻子是[阿泽伯·梅瑟菲纳](../Page/阿泽伯·梅瑟菲纳.md "wikilink")（Azeb
Mesfin），两人育有三个孩子。Azeb
Mesfin目前是国会社会事务委员会主席，2007年1月，她因领导抵抗[艾滋病而被授予](../Page/艾滋病.md "wikilink")
"梦想遗产"
奖\[8\]。此外，她也与政府部门致力于解决该国[儿童高死亡率的问题](../Page/儿童.md "wikilink")。根据[联合国儿童基金会的报告](../Page/联合国儿童基金会.md "wikilink")，自从现任执政党执政以来，该国的儿童死亡率已经下降了40%\[9\]。

## 参考资料

## 外部链接

  - [Biography](http://www.durame.com/2011/09/meles-zenawis-biography.html)

  - [Column archive](http://www.guardian.co.uk/profile/meles-zenawi) at
    *[The Guardian](../Page/The_Guardian.md "wikilink")*

  -
  -
  -
[Category:埃塞俄比亚总理](../Category/埃塞俄比亚总理.md "wikilink")
[Category:埃塞俄比亚总统](../Category/埃塞俄比亚总统.md "wikilink")
[Category:鹿特丹大学校友](../Category/鹿特丹大学校友.md "wikilink")
[Category:英国公开大学校友](../Category/英国公开大学校友.md "wikilink")
[Category:世界和平獎獲得者](../Category/世界和平獎獲得者.md "wikilink")

1.
2.
3.
4.
5.
6.  [More information on Meles
    Zenawi](http://www.commissionforafrica.org/english/commissioners/bios/zenawi.html)

7.
8.  [Azeb Mesfin given Legacy of Dream
    award](http://allafrica.com/stories/200701221034.html)
9.