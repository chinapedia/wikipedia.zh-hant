**紫色**是一種[顏色](../Page/顏色.md "wikilink")，在[科學上有兩種定義](../Page/科學.md "wikilink")：

1.  指[蓝紫色](../Page/蓝紫色.md "wikilink")（380-450nm
    )，英語稱為violet，是[可见光的](../Page/可见光.md "wikilink")[光谱中最边缘的部分](../Page/光谱.md "wikilink")。比其波长更短的称为[紫外线](../Page/紫外线.md "wikilink")。
2.  指由[紅色](../Page/紅色.md "wikilink")（610-760nm）和[藍色](../Page/藍色.md "wikilink")（450-500nm）混合而成的顏色，英語稱為Purple。

## 象徵意義

[Callicarpa_americana.jpg](https://zh.wikipedia.org/wiki/File:Callicarpa_americana.jpg "fig:Callicarpa_americana.jpg")
[Aubergine.jpg](https://zh.wikipedia.org/wiki/File:Aubergine.jpg "fig:Aubergine.jpg")
[Omotesandō_Station_001.JPG](https://zh.wikipedia.org/wiki/File:Omotesandō_Station_001.JPG "fig:Omotesandō_Station_001.JPG")\]\]
[Flag_of_HRH_Princess_Maha_Chakri_Sirindhorn.jpg](https://zh.wikipedia.org/wiki/File:Flag_of_HRH_Princess_Maha_Chakri_Sirindhorn.jpg "fig:Flag_of_HRH_Princess_Maha_Chakri_Sirindhorn.jpg")

  - 中國傳統紫色代表尊貴，如[北京故宮又稱](../Page/北京故宮.md "wikilink")「紫禁城」，亦有所謂「紫氣東來」。受此影響，如今[日本](../Page/日本.md "wikilink")[王室仍尊崇紫色](../Page/王室.md "wikilink")。這源於中國古代對[北極星的崇拜](../Page/北極星.md "wikilink")，如：「觀：北辰紫宮，衣冠立中。含和建德，常受天福。」\[1\]
  - 在西方，紫色亦代表尊貴，常成為貴族所愛用的顏色，這緣於[古羅馬帝國蒂爾人常用的紫色](../Page/古羅馬帝國.md "wikilink")[染料僅供](../Page/染料.md "wikilink")[貴族穿著](../Page/貴族.md "wikilink")，而染成衣物近似[緋紅色](../Page/緋紅色.md "wikilink")，亦甚受當時君主所好。在[拜占庭時代](../Page/拜占庭.md "wikilink")，來自王族嫡系的皇帝會將「紫生」（born
    to the purple）一字加於自己的稱號，表明自己的正統出身，以別於靠其他手段獲得王位的君主。
  - 自古以來紫色就是宗教的顏色。在[基督教中](../Page/基督教.md "wikilink")，紫色代表至高無上和來自[聖靈的力量](../Page/聖靈.md "wikilink")。[猶太教大](../Page/猶太教.md "wikilink")[祭司的服裝或窗簾](../Page/祭司.md "wikilink")、聖器，常常使用紫色。[天主教稱紫色為主教色](../Page/天主教.md "wikilink")。主教穿紫色；[樞機穿朱紅色](../Page/樞機.md "wikilink")。大齋期（Lent，預示[耶穌的受難和復活](../Page/耶穌.md "wikilink")）的主要顏色是紫色。紫色代表神聖、尊貴、慈愛，在高禮儀教會（如[天主教](../Page/天主教.md "wikilink")、[聖公會](../Page/聖公會.md "wikilink")、[信義宗](../Page/信義宗.md "wikilink")），會換上紫色的桌巾和紫色[蠟燭](../Page/蠟燭.md "wikilink")。
  - 紫色（purple）還代表着很多含義。在中国古代传统文化里，正色是五行的青、赤、黃、白、黑，紫色是青與赤组合而成，不是正色，可以表達“不正統”。此類應用首见于《[论语](../Page/论语.md "wikilink")》，《论语·阳货》中有“恶紫之夺朱也，恶郑声之乱雅也，恶利口之覆邦也”。由于《论语》这一典故，人们往往把以邪犯正、以下乱上比作以紫夺朱。例如：王莽篡汉，《汉书·王莽传赞》书为“紫色蛙声，余分闰位”，把王莽篡汉说成是以紫夺朱、蛙声打鸣。清时文人曾骂满清王朝为“夺朱非正色，异姓尽称王”。
  - 紫色在[电阻值中代表](../Page/电阻.md "wikilink")7。
  - 紫色在[泰国是](../Page/泰国.md "wikilink")[星期六的代表色](../Page/星期六.md "wikilink")。比如[诗琳通公主是星期六出生](../Page/诗琳通公主.md "wikilink")，在泰国每年的[4月2日](../Page/4月2日.md "wikilink")（例如[2015年的60岁寿辰](../Page/2015年诗琳通公主60寿辰.md "wikilink")）或公主出席时人民都会使用大量紫色以示庆祝。\[2\]\[3\]
  - 紫色在[京剧脸谱中代表沉着稳重的角色](../Page/京剧.md "wikilink")，如[常遇春](../Page/常遇春.md "wikilink")。
  - 紫色比[黑色更容易表達感情](../Page/黑色.md "wikilink")，近年來影視作品、[ACG作品常用紫色代替黑色或與黑色一起表達邪惡](../Page/ACG.md "wikilink")、詛咒的含義，用於表現[反派及](../Page/反派.md "wikilink")[反英雄](../Page/反英雄.md "wikilink")。
  - [雙魚宮代表色](../Page/雙魚宮.md "wikilink")。
  - 紫色在[日本工業規格的標記色彩標準中被用作標示](../Page/日本工業規格.md "wikilink")[放射性事項](../Page/放射性.md "wikilink")。
  - 紫色在有[性感之意](../Page/性感.md "wikilink")。

## 參考資料

[Category:颜色](../Category/颜色.md "wikilink")
[Category:可見光](../Category/可見光.md "wikilink")
[Category:网页颜色](../Category/网页颜色.md "wikilink")

1.  [焦氏易林 -\>坤之](http://ctext.org/jiaoshi-yilin/kun-zhi/zh)
2.  [認識泰國7彩文化 7天7色不可不知](https://www.hopetrip.com.hk/news/201612/311892.html)
3.  [泰国诗琳通公主60大寿
    曼谷变身“紫色之都”](http://new.qq.com/cmsn/20150402/20150402048360)