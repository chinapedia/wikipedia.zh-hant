《**瞧！這個人**》（），[德国](../Page/德国.md "wikilink")[哲學家](../Page/哲學家.md "wikilink")[尼采的自傳](../Page/尼采.md "wikilink")，完成於1888年，并在尼采死後的1908年由其妹出版。本書其實在出版後並未受到重視，一直到1970年才廣為人知。

在《瞧！這個人》這本書裡面，是尼采詮釋自己哲學著作，同時也是他展現自己的一部作品。在本書裡面，已經出現他晚期作品的主題，特別是在批判基督教和「重估一切價值」。

現在對尼采創作的可信度，以及其作品受到精神疾病影響的程度，有各種不同的解釋。不過尼采經《瞧！這個人》這本書裡面的自我解釋遠比其他作品多。

## 典故

書名的典故源自於《[新約聖經](../Page/新約聖經.md "wikilink")·[約翰福音](../Page/約翰福音.md "wikilink")》中[耶穌被捕后](../Page/耶穌被捕.md "wikilink")，[羅馬帝國的](../Page/羅馬帝國.md "wikilink")[猶太行省](../Page/猶太行省.md "wikilink")[行政長官](../Page/行政長官.md "wikilink")[彼拉多說的話](../Page/彼拉多.md "wikilink")[试观此人](../Page/试观此人.md "wikilink")（），要眾人看[耶穌](../Page/耶穌.md "wikilink")。

## 簡介

這是本獨特的自傳，尼采没有採用傳統的方法敘述生平所發生的事。全書有一個序及15章，其中第4章到第13章是他對其同名書籍的回顧，它們的標題分別是：

  - 序

<!-- end list -->

1.  為什麼我這麼有智慧
2.  為什麼我這麼聰明
3.  為什麼我寫出這麼好的書
4.  悲劇的誕生
5.  不合時宜的考察
6.  人性、太人性的
7.  朝霞：道德思想乃是成見
8.  歡愉的智慧：快樂的科學
9.  查拉圖斯特拉如是說：一本給所有人看也是無人能看的書
10. 善惡的彼岸：未來哲學的序曲
11. 道德譜系學：一個論戰
12. 偶像的黃昏
13. 華格納事件：一個音樂家的問題
14. 為什麼我是一個災禍
15. 一個自我批評的企圖

在題目中，尼采充分表現出他那近乎瘋狂的自大，這有可能跟他的精神不穩有關。

尼采还在该書中这样评价自己：「第一個[悲劇哲學家](../Page/悲劇.md "wikilink")」、「第一個非道德主義者」、「
[歐洲第一個完美的](../Page/歐洲.md "wikilink")[虛無主義者](../Page/虛無主義.md "wikilink")」、「最後一個反政治的德國人」、「最後一個[斯多噶主義者](../Page/斯多噶主義.md "wikilink")」。這些雖然不免流於自誇，但仍有相當的真實性。

## 內容

## 参考文献

## 参见

  - [试观此人](../Page/试观此人.md "wikilink")
  - [弗里德里希·威廉·尼采](../Page/弗里德里希·威廉·尼采.md "wikilink")

{{-}}

[Category:哲学书籍](../Category/哲学书籍.md "wikilink")
[Category:1908年書籍](../Category/1908年書籍.md "wikilink")
[Category:弗里德里希·尼采](../Category/弗里德里希·尼采.md "wikilink")
[Category:自传](../Category/自传.md "wikilink")