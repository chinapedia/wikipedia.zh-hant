**犹他大学**（University of
Utah），位于[美國](../Page/美國.md "wikilink")[猶他州的](../Page/猶他州.md "wikilink")[鹽湖城市](../Page/鹽湖城.md "wikilink")，是一所综合性公立大学，由[耶穌基督後期聖徒教會领袖](../Page/耶穌基督後期聖徒教會.md "wikilink")[杨百翰于](../Page/杨百翰.md "wikilink")1850年建立。学校约有30,000学生及4,000教职员工。

根据卡内基教育基金会2005年公布的大学分类，犹他大学被归类为特高研究型大学（very high research
activity）。在美国4,383所高等学府中，只有96所获得此项分类。

## 参考资料

## 外部链接

  - [犹他大学官方网站](http://www.utah.edu/)

Muqian Tian, Applied Economics, University of Minnesota, Twin Cities

[Category:猶他州大學](../Category/猶他州大學.md "wikilink")
[\*](../Category/猶他大學.md "wikilink")
[Category:1850年建立](../Category/1850年建立.md "wikilink")
[Category:鹽湖城](../Category/鹽湖城.md "wikilink")