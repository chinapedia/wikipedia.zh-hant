[PAL-NTSC-SECAM.svg](https://zh.wikipedia.org/wiki/File:PAL-NTSC-SECAM.svg "fig:PAL-NTSC-SECAM.svg")

**PAL制式**是[電視廣播中色彩調頻的一種方法](../Page/電視廣播.md "wikilink")，全名為**逐行倒相**（Phase
Alternating
Line）。除了[北美](../Page/北美.md "wikilink")、[東亞部分地區使用](../Page/東亞.md "wikilink")[NTSC制式](../Page/NTSC制式.md "wikilink")，[中東](../Page/中東.md "wikilink")、[法國及](../Page/法國.md "wikilink")[東歐採用](../Page/東歐.md "wikilink")[SECAM制式以外](../Page/SECAM制式.md "wikilink")，世界上大部份地區都是採用PAL制式。PAL于1963年由[德國人](../Page/德國.md "wikilink")提出，當時他為[德律風根](../Page/德律風根.md "wikilink")（Telefunken）工作。

## 歷史

1950年代，[西欧正計劃](../Page/西欧.md "wikilink")[彩色電視](../Page/彩色電視.md "wikilink")[廣播](../Page/廣播.md "wikilink")；不過当時NTSC制式本身已有不少缺陷，包括当接收条件差時容易發生[色相轉移](../Page/色相.md "wikilink")（color
tone shifting）現象，所以有人戲稱NTSC制式為**N**ever **T**he **S**ame
**C**olor（不會重現一樣的[色彩](../Page/色彩.md "wikilink")）。為了克服NTSC制式本身的缺点，[欧洲有需要自行研發適合欧洲本土的彩色電視制式](../Page/欧洲.md "wikilink")，就是後来的PAL制式及SECAM制式。而兩者圖像[頻率同為](../Page/頻率.md "wikilink")50Hz，不同於NTSC的60Hz，適合欧洲本身的50Hz[交流電源](../Page/交流電.md "wikilink")[頻率](../Page/頻率.md "wikilink")。

PAL制式由沃尔特·布鲁赫設計，1963年面世。[英国廣播公司是最早使用PAL制式的電視台](../Page/英国廣播公司.md "wikilink")，於1964年在[BBC2試播](../Page/BBC2.md "wikilink")，1967年正式開始全彩廣播。[西德在](../Page/西德.md "wikilink")1967年開始PAL制式廣播。[國際電信聯盟在](../Page/國際電信聯盟.md "wikilink")1998年正式在其出版物將PAL制式正式定義為*Recommendation
ITU-R BT.470-6, Conventional Television Systems*。

「PAL」有時亦被用來指625線、每秒25格、隔行掃描、PAL色彩調頻的電視制式（[576i](../Page/576i.md "wikilink")），它與525線、每秒29.97格、隔行掃描的NTSC制式（[480i](../Page/480i.md "wikilink")）不同。市售的[DVD-Video一般都会標示NTSC或PAL制](../Page/DVD-Video.md "wikilink")。

## 原理

PAL發明的原意是要在兼容原有[黑白電視廣播格式的情況下加入彩色訊號](../Page/黑白電視.md "wikilink")。PAL的原理與NTSC接近。「逐行倒相」的意思是每行掃瞄線的彩色訊號會跟上一行倒相，作用是自動改正在傳播中可能出現的錯相。早期的PAL電視機沒有特別的組件改正錯相，有時嚴重的錯相仍然會被肉眼明顯看到。近年的PAL電視機會把上行的色彩訊號跟下一行的平均起來才顯示，這樣PAL的垂直色彩解像度會低於NTSC；但人眼對色彩的靈敏不及對光暗，因此這並不是明顯問題。

NTSC電視機需要色彩控制（tint control）來手動調節顏色，這亦是NTSC的最大缺陷之一。

## 不同的PAL

PAL本身是指色彩系統，經常被配以625線、每秒25格畫面、隔行掃瞄的電視廣播格式：如B、G、H、I、N。亦有PAL是配以其他解像度的格式：例如[巴西使用的M廣播格式為](../Page/巴西.md "wikilink")525線、29.97格（與NTSC格式一樣），用NTSC彩色，但巴西是使用PAL彩色調頻的。現在大部分的PAL電視機都能收看以上所有不同系統格式的PAL。很多PAL電視機更能同時收看[基頻的NTSC](../Page/基頻.md "wikilink")-M，例如電視遊戲機、錄影機等等的NTSC訊號，但是它們不一定可以接收NTSC廣播。

當影像訊號是以基頻傳送時（例如電視遊戲機、錄影機等等），便再沒有以上所說各種以"字母"區分廣播格式的分別了。這情況下，PAL的意思是指：625條掃瞄線，每秒25格畫面，隔行掃瞄，PAL色彩調頻。對數碼影像如DVD或數碼廣播，制式亦沒有分別，這情況下PAL是指：625條掃瞄線，每秒25格畫面，隔行掃瞄；即是跟SECAM一模一樣。

[英國](../Page/英國.md "wikilink")、[香港](../Page/香港.md "wikilink")、[澳門使用的是PAL](../Page/澳門.md "wikilink")-I，[中國大陸使用的是PAL](../Page/中國大陸.md "wikilink")-D，[新加坡使用的是PAL](../Page/新加坡.md "wikilink")
B/G或D/K。

## PAL放送的電影

[電影一般是以每秒](../Page/電影.md "wikilink")24格拍攝。電影在PAL制式電視播影時會以每秒25格播放，播放的速度因而比電影院內或NTSC電視廣播加快了4%。這種差別不太明顯，但電影內的音樂會因而變得高了一個[半音](../Page/半音.md "wikilink")。如果電視台在廣播時沒有加以調校補償，仔細聆聽便會發現。

## 使用PAL制式的国家和地区

目前使用或曾经使用PAL系统的国家和地区列表。
其中许多已经转换或正在将PAL转换为[DVB-T](../Page/DVB-T.md "wikilink")（大多数国家）、[DVB-T2](../Page/DVB-T2.md "wikilink")（大多数国家）、[DTMB](../Page/DTMB.md "wikilink")（中国、香港和澳门）和[ISDB](../Page/ISDB.md "wikilink")（斯里兰卡、马尔代夫、博茨瓦纳和南美洲部分国家）。

### PAL B, D, G, H, K 或 I

  - （曾使用SECAM制式）\[1\]

  - \[2\]

  - （目前DVB-T正在评估中）\[3\]

  - \[4\]

  - \[5\]

  - \[6\]

  - \[7\]

  - \[8\]

  - \[9\]

  - \[10\]

  - \[11\]

  - （使用[DMB-T / H的数字广播](../Page/DTMB.md "wikilink")）\[12\]

  - （参见新西兰）

  - \[13\]

  - （1990-1992间从SECAM上迁移）\[14\]

  - \[15\]

  - \[16\]

  - \[17\]

  - （只使用UHF）\[18\]

  - \[19\]

  - \[20\]

  - \[21\]

  - （自2007年12月31日起开始施行PAL-I，DMB-T / H，计划在2020年放弃PAL-I广播）

  - \[22\]（将于2018年放弃PAL广播；自2008年至2012年，逐步迁移讲DVB-T至DVB-T2，政府计划在2014年4月免费赠送700万个STB
    DVB-T2）

  - \[23\]（目前DVB-T正在评估中）

  - \[24\]（目前DVB-T正在评估中）

  - \[25\]

  - （曾经在PAL-M中进行实验）\[26\]

  - （对黎巴嫩频道使用PAL制式，来自欧美的频道不采用广播模拟）

  - \[27\]

  -
  - \[28\]

<!-- end list -->

  - \[29\]（逐步从模拟信号迁移至DVB-T2数字信号，从2015年开始至2017年成功完成迁移）

  - \[30\]

  - \[31\]

  - \[32\]

  - \[33\]

  - \[34\]

  - （参加澳大利亚）

  - （曾使用SECAM制式）\[35\]

  - （目前DVB-T正在评估中）\[36\]

  - \[37\]

  - （目前DVB-T正在评估中）

  - \[38\]

  - （由BFBS运营的两种PAL-I模拟信号电视服务）

  - \[39\]

  - \[40\]

  - \[41\]

  - \[42\]

  - \[43\]

  -
  - \[44\]

  - \[45\]

  - \[46\]

  - （同时使用SECAM）\[47\]

  - （将于2020年放弃PAL广播将在2020年被弃。[联播DVB](../Page/联播.md "wikilink")-T2。）

  -
  - \[48\]（PAL广播被弃。定于2015年6月15日关闭模拟信号。联播DVB-T。）

  - \[49\]

  - \[50\]

  - \[51\]

  - \[52\]（DVB-T introduction）

  - \[53\]

### PAL-M

  - \[54\]在中[联播数字格式](../Page/联播.md "wikilink")，也称为SBTVD，于2007年12月更新至[ISDB-T](../Page/ISDB-T.md "wikilink")。PAL将一直持续到2018年。）

### PAL-N

  - \[55\]（在ISDB-T基础上的H264视频，at 576i@50 Hz (SD) or 1080i@50 Hz (HD)）

  - \[56\]（同时使用 ISDB-T）

  - \[57\]（将使用 ISDB-T）

### 已经停止使用PAL制式的国家

以下国家不再将PAL制式用于地面广播，并正在从PAL（有线）迁移至。

<table>
<thead>
<tr class="header">
<th><p>国家</p></th>
<th><p>迁移至</p></th>
<th><p>迁移完成日</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2015年6月17日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2007年9月25日</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2013年12月10日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>DVB-T 和 DVB-T2</p></td>
<td><p>2011年6月7日</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2015年6月17日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2010年3月1日</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2015年1月1日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2013年9月30日</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>DVB-T2</p></td>
<td><p>2015年1月1日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2010年10月20日</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2011年7月1日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2012年6月30日</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>DVB-T 和 DVB-T2</p></td>
<td><p>2009年11月1日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2010年7月1日</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2002年12月</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2007年9月1日</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2015年7月1日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>DVB-T 和 DVB-T2</p></td>
<td><p>2009年6月4日</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>DVB-T2</p></td>
<td><p>2015年6月</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2015年2月6日</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2012年12月31日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2010年11月17日</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>DVB-T 和 DVB-T2</p></td>
<td><p>2013年10月31日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>DVB-T 和 DVB-T2</p></td>
<td><p>2015年2月2日</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2015年3月31日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2014年12月19日</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2012年10月24日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2012年10月24日</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2011年6月13日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2012年7月4日</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2010年11月17日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2015年3月</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2010年6月1日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2012年10月29日</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2006年9月1日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2013年5月31日</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2011年10月31日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2011年5月24日</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2015年6月17日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2014年9月13日</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2006年12月14日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2013年12月1日</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>DVB-T 和 DVB-T2</p></td>
<td><p>2009年12月1日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2013年7月23日</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2012年4月26日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>DVB-T 和 DVB-T2</p></td>
<td><p>2012年2月13日</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2016年</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2014年3月</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>DVB-T 和 DVB-T2</p></td>
<td><p>2012年2月13日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>DVB-T2</p></td>
<td><p>2015年6月7日</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2015年[58]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2010年12月2日</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2010年12月1日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2012年12月31日</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2010年4月3日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2007年10月29日</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2007年11月26日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2014年7月</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>DVB-T</p></td>
<td><p>2015年3月3日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>DVB-T 和 DVB-T2</p></td>
<td><p>2012年2月13日</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/DVB-T.md" title="wikilink">DVB-T</a> 和 <a href="../Page/DVB-T2.md" title="wikilink">DVB-T2</a></p></td>
<td><p>2016年12月31日</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>DVB-T (SD) 和 DVB-T2 (HD)</p></td>
<td><p>2012年10月24日</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>DVB-T2</p></td>
<td><p>2014年12月31日</p></td>
</tr>
</tbody>
</table>

## 参见

  -
  - [彩色电视广播标准列表](../Page/彩色电视广播标准列表.md "wikilink")

      - [ATSC](../Page/ATSC.md "wikilink")

      -
      - [NTSC制式](../Page/NTSC制式.md "wikilink")

          -
      - [SECAM制式](../Page/SECAM制式.md "wikilink")

  - [DVB](../Page/DVB.md "wikilink")

  -
  -
  - [数字电视](../Page/数字电视.md "wikilink")

  -
  - [PAL区](../Page/PAL区.md "wikilink")

  -
  - [YUV](../Page/YUV.md "wikilink")

## 参考文献

## 外部链接

  - [回顾PAL，NTSC和电影胶片刷新率的不同](http://www.paradiso-design.net/videostandards_en.html)
  - [世界电视制式标准](http://www.radios-tv.co.uk/Pembers/World-TV-Standards/Colour-Standards.html#Top)

[Category:電視技術](../Category/電視技術.md "wikilink")
[Category:德国发明](../Category/德国发明.md "wikilink")

1.

2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14.
15.
16.
17.
18.
19.
20.
21.
22.
23.
24.

25.
26.
27.
28.
29.
30.
31.
32.
33.
34.
35.
36.
37.
38.
39.
40.
41.
42.
43.
44.
45.
46.
47.
48.
49.
50.
51.
52.
53.
54.
55.
56.
57.
58.