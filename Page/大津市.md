**大津市**（）是[日本](../Page/日本.md "wikilink")[滋賀縣的](../Page/滋賀縣.md "wikilink")[縣廳所在地](../Page/縣廳所在地.md "wikilink")
(首府)，位於[琵琶湖西南岸](../Page/琵琶湖.md "wikilink")。

## 歷史

[Otsu-BiwakoSosui-M1999.jpg](https://zh.wikipedia.org/wiki/File:Otsu-BiwakoSosui-M1999.jpg "fig:Otsu-BiwakoSosui-M1999.jpg")

  - 667年：[天智天皇建](../Page/天智天皇.md "wikilink")**大津京**。
  - 1571年：[織田信長焚燬](../Page/織田信長.md "wikilink")[延暦寺](../Page/延暦寺.md "wikilink")。
  - 1586年：[豐臣秀吉建](../Page/豐臣秀吉.md "wikilink")**大津城**。
  - 1600年：[關原之戰](../Page/關原之戰.md "wikilink")，大津城被焚。
  - 1868年：置**大津縣**。
  - 1869年：連接海津的蒸汽船業務開通。
  - 1872年：成為滋賀縣縣廳所在地。
  - 1880年：連接[京都市的鐵路開通](../Page/京都市.md "wikilink")。
  - 1890年：[琵琶湖疏水完成](../Page/琵琶湖疏水.md "wikilink")。
  - 1891年5月11日：[大津事件](../Page/大津事件.md "wikilink")。
  - 1898年10月1日：建市。

## 出身人物

  - [川崎恭治](../Page/川崎恭治.md "wikilink")：物理學家，[波茲曼獎得主](../Page/波茲曼獎.md "wikilink")。
  - [登勢](../Page/登勢.md "wikilink")：[幕末時期寺田屋的女老闆](../Page/幕末.md "wikilink")。
  - [小野妹子](../Page/小野妹子.md "wikilink")：[飛鳥時代的政治家](../Page/飛鳥時代.md "wikilink")、外交家，[遣隋使](../Page/遣隋使.md "wikilink")。
  - [木內石亭](../Page/木內石亭.md "wikilink")：[江戶時代的考古學家](../Page/江戶時代.md "wikilink")。
  - [最澄](../Page/最澄.md "wikilink")：[平安時代僧人](../Page/平安時代.md "wikilink")、日本[天台宗的開創者](../Page/天台宗.md "wikilink")。

## 姐妹城市

  - [牡丹江市](../Page/牡丹江市.md "wikilink")

  - [蘭辛](../Page/蘭辛.md "wikilink")

  - [龜尾](../Page/龜尾.md "wikilink")

  - [因特拉肯](../Page/因特拉肯.md "wikilink")

  - [维尔茨堡](../Page/维尔茨堡.md "wikilink")

## 外部链接

  - [市政府網頁](http://www.city.otsu.lg.jp/)

[Category:滋賀縣的市町村](../Category/滋賀縣的市町村.md "wikilink")
[Category:中核市](../Category/中核市.md "wikilink")