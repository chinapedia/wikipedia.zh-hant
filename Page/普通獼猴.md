**普通獼猴**（[學名](../Page/學名.md "wikilink")：**），即**恒河猴**，是[猴科動物中最爲有名的一種猴](../Page/猴科.md "wikilink")。它是典型的[獼猴屬動物](../Page/獼猴屬.md "wikilink")，分佈于[日本](../Page/日本.md "wikilink")[千葉縣](../Page/千葉縣.md "wikilink")[房總半島](../Page/房總半島.md "wikilink")\[1\]、[阿富汗](../Page/阿富汗.md "wikilink")、[巴基斯坦](../Page/巴基斯坦.md "wikilink")、[印度北部和](../Page/印度.md "wikilink")[中國大陸南方](../Page/中國大陸.md "wikilink")。

成年公猴的身體要比母猴大，平均體長度在，體重，母猴則只有，體重。體毛顔色為褐色或灰色，臉部為粉紅色，尾巴不是很長，約在之間。普通獼猴的平均壽命約為25年。

普通獼猴是用於科學試驗的重要品種。

## [分類學](../Page/分類學.md "wikilink")

普通獼猴目前被認為是[單型種](../Page/單型.md "wikilink")，未能區分出[亞種](../Page/亞種.md "wikilink")。
\[2\]

## 保護狀況

普通獼猴已列入[CITES附錄II中](../Page/CITES.md "wikilink")。\[3\]

### 中國大陸

[中國大陸將普通獼猴列入](../Page/中國大陸.md "wikilink")[國家二級保護動物](../Page/國家二級保護動物.md "wikilink")
，《中國國家重點保護野生動物名錄》\[4\] 和《中國瀕危動物紅皮書》[易危物種](../Page/易危.md "wikilink")。
\[5\]

### 南亞

普通獼猴亦受[孟加拉的野生生物](../Page/孟加拉.md "wikilink")(保護)法案附表III，[印度的野生生物](../Page/印度.md "wikilink")(保護)法案附表I，[尼泊爾的國家公園及野生生物保護法案所保護](../Page/尼泊爾.md "wikilink")。\[6\]

### 香港

普通獼猴是香港唯一一種原生[猴子](../Page/猴子.md "wikilink")，但至約1940年代時已在[香港](../Page/香港.md "wikilink")[九龍和](../Page/九龍.md "wikilink")[新界絕種](../Page/新界.md "wikilink")，而在[香港亦已十分罕見](../Page/香港.md "wikilink")。\[7\]

而目前在香港[金山郊野公園](../Page/金山郊野公園.md "wikilink")（因有不少猴子在此棲息而被市民稱為[馬騮山](../Page/金山_\(香港\).md "wikilink")）一帶棲息的野生普通獼猴並不是香港原生獼猴的後代，而是在[第一次世界大戰期間引入的](../Page/第一次世界大戰.md "wikilink")，目的是要獼猴將有毒的[牛眼馬錢](../Page/牛眼馬錢.md "wikilink")（）吃掉以保障[九龍水塘的食水安全](../Page/九龍水塘.md "wikilink")。\[8\]

普通獼猴現受《野生動物保護條例（第170章）》以及《動植物（瀕危物種保護）條例（第586章）》保護。 \[9\]

## 日本純化

2017年2月20日，日本千葉縣[富津市公告](../Page/富津市.md "wikilink")，的164隻[日本獼猴](../Page/日本獼猴.md "wikilink")，經專家確認[DNA有](../Page/DNA.md "wikilink")57隻與野生的普通獼猴（恆河獼猴）雜交，獲得縣府許可後依據《外來生物法》實施撲殺，並已於2月15日舉行慰靈儀式（）\[10\]\[11\]。

## 參考來源

## 外部連結

  - Eudey, A. & Members of the Primate Specialist Group 2000. [*Macaca
    mulatta*](http://www.iucnredlist.org/search/details.php/12554/summ)。In:
    IUCN 2006. 2006 IUCN Red List of Threatened Species.

[Category:獼猴屬](../Category/獼猴屬.md "wikilink")
[Category:中國哺乳動物](../Category/中國哺乳動物.md "wikilink")
[Category:印度哺乳動物](../Category/印度哺乳動物.md "wikilink")
[Category:巴基斯坦動物](../Category/巴基斯坦動物.md "wikilink")
[Category:阿富汗動物](../Category/阿富汗動物.md "wikilink")
[Category:香港動物](../Category/香港動物.md "wikilink")
[Category:香港原生物種](../Category/香港原生物種.md "wikilink")
[Category:孟加拉動物](../Category/孟加拉動物.md "wikilink")
[Category:不丹動物](../Category/不丹動物.md "wikilink")
[Category:寮國動物](../Category/寮國動物.md "wikilink")
[Category:緬甸動物](../Category/緬甸動物.md "wikilink")
[Category:尼泊爾動物](../Category/尼泊爾動物.md "wikilink")
[Category:泰國動物](../Category/泰國動物.md "wikilink")
[Category:越南動物](../Category/越南動物.md "wikilink")
[Category:中國國家二級保護動物](../Category/中國國家二級保護動物.md "wikilink")

1.

2.
3.
4.
5.
6.
7.

8.

9.

10.

11.