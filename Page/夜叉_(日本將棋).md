**夜叉**（）是[日本將棋的棋子之一](../Page/日本將棋.md "wikilink")。只在[大大將棋](../Page/大大將棋.md "wikilink")、[摩訶大大將棋](../Page/摩訶大大將棋.md "wikilink")、[泰將棋](../Page/泰將棋.md "wikilink")、[大局將棋出現](../Page/大局將棋.md "wikilink")。

在多篇手抄本上，日本將棋上的「夜叉」都被寫成「**夜刃**」，不過「夜刃」的版本並不能得到確認，而且，這種棋子的名稱基本上存在著「夜叉」的意義，所以在這裡，兩種版本皆被確認以「夜叉」表記。再者，我們都無從判斷在日本將棋上將「夜叉」寫成「夜刃」的場合、事物。

## 夜叉在大大將棋

不得升級。

<table>
<thead>
<tr class="header">
<th><p>前身棋</p></th>
<th><p>步法</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>夜叉</p></td>
<td><table>
<tbody>
<tr class="odd">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>○</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
<tr class="even">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>○</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
<tr class="odd">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>○</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
<tr class="even">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>○</p></td>
<td><p>　</p></td>
<td><p>○</p></td>
<td><p>　</p></td>
<td><p>○</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
<tr class="odd">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
<tr class="even">
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p><strong>捷</strong></p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
</tr>
<tr class="odd">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
<tr class="even">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>○</p></td>
<td><p>　</p></td>
<td><p>○</p></td>
<td><p>　</p></td>
<td><p>○</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
<tr class="odd">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>○</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
<tr class="even">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>○</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
<tr class="odd">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>○</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
</tbody>
</table></td>
</tr>
</tbody>
</table>

## 夜叉在摩訶大大將棋與泰將棋

可以升級成為[金將](../Page/金將.md "wikilink")（）。

<table>
<thead>
<tr class="header">
<th><p>前身棋</p></th>
<th><p>步法</p></th>
<th><p>升級棋</p></th>
<th><p>步法</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>夜叉</p></td>
<td><table>
<tbody>
<tr class="odd">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>○</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
<tr class="even">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>○</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
<tr class="odd">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>○</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
<tr class="even">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>○</p></td>
<td><p>　</p></td>
<td><p>○</p></td>
<td><p>　</p></td>
<td><p>○</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
<tr class="odd">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
<tr class="even">
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p><strong>捷</strong></p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
</tr>
<tr class="odd">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
<tr class="even">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>○</p></td>
<td><p>　</p></td>
<td><p>○</p></td>
<td><p>　</p></td>
<td><p>○</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
<tr class="odd">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>○</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
<tr class="even">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>○</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
<tr class="odd">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>○</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
</tbody>
</table></td>
<td><p>可循縱向及橫向走五格，或循斜向走兩格。不得越過其他棋子。</p></td>
<td><p>金將</p></td>
</tr>
</tbody>
</table>

## 夜叉在大局將棋

可以升級成為[四天](../Page/四天.md "wikilink")（）。

<table>
<thead>
<tr class="header">
<th><p>前身棋</p></th>
<th><p>步法</p></th>
<th><p>升級棋</p></th>
<th><p>步法</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>夜叉</p></td>
<td><table>
<tbody>
<tr class="odd">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
<tr class="even">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
<tr class="odd">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>○</p></td>
<td><p>　</p></td>
<td><p>○</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
<tr class="even">
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p><strong>捷</strong></p></td>
<td><p>○</p></td>
<td><p>○</p></td>
<td><p>○</p></td>
</tr>
<tr class="odd">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>○</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
<tr class="even">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
<tr class="odd">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
</tbody>
</table></td>
<td><p>可循後方及前斜向走一格，或循橫向走三格。不得越過其他棋子。</p></td>
<td><p>四天</p></td>
</tr>
</tbody>
</table>

## 參見

  - [日本將棋](../Page/日本將棋.md "wikilink")
  - [日本將棋變體](../Page/日本將棋變體.md "wikilink")
  - [大大將棋](../Page/大大將棋.md "wikilink")
  - [摩訶大大將棋](../Page/摩訶大大將棋.md "wikilink")
  - [泰將棋](../Page/泰將棋.md "wikilink")
  - [大局將棋](../Page/大局將棋.md "wikilink")

[Category:日本將棋棋子](../Category/日本將棋棋子.md "wikilink")
[Category:大大將棋棋子](../Category/大大將棋棋子.md "wikilink")
[Category:摩訶大大將棋棋子](../Category/摩訶大大將棋棋子.md "wikilink")
[Category:泰將棋棋子](../Category/泰將棋棋子.md "wikilink")
[Category:大局將棋棋子](../Category/大局將棋棋子.md "wikilink")