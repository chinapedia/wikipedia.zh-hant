**北美黃林鶯**，學名***Dendroica
petechia***，是一種[新世界鶯](../Page/新世界鶯.md "wikilink")。是全世界分佈最廣的[林鶯](../Page/林鶯.md "wikilink")（*Dendroica*），幾乎從[北美到](../Page/北美.md "wikilink")[南美都有廣泛分佈](../Page/南美.md "wikilink")。

## 外部連結

  - [Yellow breasted Warbler by John
    Audubon](https://web.archive.org/web/20070927025738/http://audubon-print.com/yellow-breasted-warbler.html)
  - [Yellow Warbler Information and
    Photos](http://www.sdakotabirds.com/species/yellow_warbler_info.htm)
    - South Dakota Birds and Birding
  - [Yellow Warbler Species
    Account](http://www.birds.cornell.edu/AllAboutBirds/BirdGuide/Yellow_Warbler.html)
    - Cornell Lab of Ornithology
  - [Yellow Warbler - *Dendroia
    petechia*](http://www.mbr-pwrc.usgs.gov/id/framlst/i6520id.html) -
    USGS Patuxent Bird Identification InfoCenter
  - [Grizzlyrun Yellow Warbler general information and
    photos](http://www.grizzlyrun.com/Pets/Yellow_warbler_Dendroica_petechia/Default.htm)
  - [Stamps](http://www.bird-stamps.org/cspecies/19901400.htm)
  - [Yellow Warbler
    videos](http://ibc.hbw.com/ibc/phtml/especie.phtml?idEspecie=8957)
    on the Internet Bird Collection-([Cozumel
    Island](../Page/Cozumel_Island.md "wikilink"), Mexico)

[Category:森莺科](../Category/森莺科.md "wikilink")