**木賊目**（[學名](../Page/學名.md "wikilink")：）是[木賊綱下的一個](../Page/木賊綱.md "wikilink")[目](../Page/目_\(生物\).md "wikilink")，其下包含四個[科](../Page/科_\(生物\).md "wikilink")\[1\]\[2\]
，其中僅[木賊科](../Page/木賊科.md "wikilink")[木賊屬的](../Page/木賊屬.md "wikilink")[物種仍生存在現今的地球上](../Page/物種.md "wikilink")，如[木賊](../Page/木賊.md "wikilink")、[問荊等](../Page/問荊.md "wikilink")。此外生存於[古生代](../Page/古生代.md "wikilink")[石炭紀的高大](../Page/石炭紀.md "wikilink")[樹狀](../Page/樹.md "wikilink")[蕨類如](../Page/蕨類.md "wikilink")[蘆木等也屬於本目物種](../Page/蘆木.md "wikilink")。

## 分類

  - †[古蘆木科](../Page/古蘆木科.md "wikilink") Archaeocalamitaceae
      - †[古蘆木屬](../Page/古蘆木屬.md "wikilink") *Archaeocalamites*
  - †[蘆木科](../Page/蘆木科.md "wikilink") Calamitaceae
      - †[輪葉屬](../Page/輪葉屬.md "wikilink") *Annularia*
      - †[節髓木屬](../Page/節髓木屬.md "wikilink") *Arthropitys*
      - †[星葉屬](../Page/星葉屬.md "wikilink") *Asterophyllites*
      - †[星髓根屬](../Page/星髓根屬.md "wikilink") *Astromyelon*
      - †[蘆木屬](../Page/蘆木屬.md "wikilink") *Calamites*
      - †*Calamocarpon*
      - †[蘆孢穗屬](../Page/蘆孢穗屬.md "wikilink") *Calamostachys*
      - †[環孢穗屬](../Page/環孢穗屬.md "wikilink") *Cingularia*
      - †*Mazostachys*
      - †*Paleostachya*
  - [木賊科](../Page/木賊科.md "wikilink") Equisetaceae
      - †[似木賊屬](../Page/似木賊屬.md "wikilink") *Equisetites*
      - [木賊屬](../Page/木賊屬.md "wikilink") *Equisetum*
  - †[杯葉科](../Page/杯葉科.md "wikilink") Phyllothecaceae
      - †[杯葉屬](../Page/杯葉屬.md "wikilink") *Phyllotheca*

## 參考文獻

## 外部連結

  -
[分類:楔葉亞門](../Page/分類:楔葉亞門.md "wikilink")
[木賊目](../Page/分類:木賊目.md "wikilink")

1.
2.  [Uma nova espécie de Phyllotheca
    Brongniart](https://www.academia.edu/1097073/Uma_nova_especie_de_Phyllotheca_Brongniart_Townrow_no_Permiano_Inferior_da_Bacia_do_Parana_RS)