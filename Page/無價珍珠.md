《**無價珍珠**》（英文：**Pearl of Great
Price**）是摩門教先知[小斯密約瑟留下的作品的選錄](../Page/小斯密約瑟.md "wikilink")，是[耶穌基督後期聖徒教會](../Page/耶穌基督後期聖徒教會.md "wikilink")（俗稱[摩門教](../Page/摩門教.md "wikilink")）的[標準經文四部經典中的其中之一](../Page/標準經文.md "wikilink")，也是一些[後期聖徒運動分支宗派的教會經典](../Page/後期聖徒運動.md "wikilink")。

耶穌基督後期聖徒教會的《無價珍珠》的[Introductory Note
（引言）（英文版本）](http://scriptures.lds.org/en/pgp/introduction)的第一段這麼說：
『《無價珍珠》是一些從深觸許多信仰的重要觀點和耶穌基督後期聖徒教會的教義的材料中精選出來的。這些項目是先知[斯密約瑟留下的](../Page/斯密約瑟.md "wikilink")，並在當時出版於教會期刊。』\[1\]

## 命名

取自聖經典故：

## 內容

《無價珍珠》分成五大段落：[摩西書](../Page/摩西書.md "wikilink")、[亞伯拉罕書](../Page/亞伯拉罕書.md "wikilink")、[聖經譯文摘錄](../Page/聖經譯文摘錄.md "wikilink")、[先知斯密約瑟歷史的摘錄和](../Page/先知斯密約瑟歷史的摘錄.md "wikilink")[耶穌基督後期聖徒教會之信條](../Page/耶穌基督後期聖徒教會之信條.md "wikilink")。摩西書和聖經譯文摘錄分別是[斯密約瑟譯本的](../Page/斯密約瑟譯本.md "wikilink")《[聖經](../Page/聖經.md "wikilink")》中[創世紀和](../Page/創世紀.md "wikilink")[馬太福音的部份段落](../Page/馬太福音.md "wikilink")。先知斯密約瑟歷史的摘錄是以第一人稱寫法呈現小約瑟斯密在教會成立以前的歷史。[耶穌基督後期聖徒教會之信條是由](../Page/耶穌基督後期聖徒教會之信條.md "wikilink")13條簡短的句子組成，是斯密約瑟在1842年寫給[美國](../Page/美國.md "wikilink")[芝加哥民主黨人報編輯](../Page/芝加哥.md "wikilink")
John Wentworth 關於解釋[摩門主義的基本信仰](../Page/摩門主義.md "wikilink")。

[亞伯拉罕書是約瑟斯密買下的](../Page/亞伯拉罕書.md "wikilink")[埃及](../Page/埃及.md "wikilink")[紙葦紙上所宣稱是翻譯出來的作品](../Page/紙葦紙.md "wikilink").
這個翻譯講述亞伯拉罕到埃及的旅程，此文包含了許多[摩門信仰中獨特的教義](../Page/摩門信仰.md "wikilink")，例如[高陞的教義](../Page/高陞.md "wikilink")。對於約瑟斯密是否真是由這埃及紙草紙翻譯出亞伯拉罕書有許多的爭議，因為包括摩門教護教者在內的埃及學專家們都同意這紙草紙翻譯出的內容和亞伯拉罕完全無關.

摩西書內的內容已經與[亞當和](../Page/亞當.md "wikilink")[夏娃的故事以及其他](../Page/夏娃.md "wikilink")[猶太偽經比較過](../Page/猶太偽經.md "wikilink")，有些部份相當雷同，包括關於亞當和夏娃受誘惑和離開[伊甸園的生活](../Page/伊甸園.md "wikilink")。

原來出版的《無價珍珠》的內容和現今版本有些不同。早期版本中一些材料是從《[教義和聖約](../Page/教義和聖約.md "wikilink")》中而來另外還有一首標題為"Oh
Say What is
Truth?"的詩（現在可以在後期聖徒的聖詩中找到）。在1878年加入了一些[摩西書的內容](../Page/摩西書.md "wikilink")。《無價珍珠》在1888年被正典化為耶穌基督後期聖徒教會正典之一.
在1902年從《教義和聖約》來的內容被刪除。耶穌基督後期聖徒教會現行版本的《教義和聖約》的第137章和第138章在1976年曾經被加入在《無價珍珠》中，後來於1979年被移到《教義和聖約》。

## 注释与参考文献

<div class="references-small" style="-moz-column-count:2; column-count:2;" >

<references />

</div>

## 外部連結

  - [无价珍珠​](https://www.lds.org/scriptures/pgp?lang=zhs)
  - [無價珍珠​](https://www.lds.org/scriptures/pgp?lang=zho)
  - [耶穌基督後期聖徒教會的《無價珍珠》的 Introductory Note
    （引言）（英文）](http://scriptures.lds.org/en/pgp/introduction)

[Category:摩爾門教文獻](../Category/摩爾門教文獻.md "wikilink")

1.  原文：The Pearl of Great Price is a selection of choice materials
    touching many significant aspects of the faith and doctrine of The
    Church of Jesus Christ of Latter-day Saints. These items were
    produced by the Prophet Joseph Smith and were published in the
    Church periodicals of his day.