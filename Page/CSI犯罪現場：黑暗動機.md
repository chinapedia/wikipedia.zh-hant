是一套基於電視劇《[CSI犯罪現場](../Page/CSI犯罪現場.md "wikilink")》的[電子遊戲](../Page/電子遊戲.md "wikilink")。該遊戲由[369
Interactive開發](../Page/369_Interactive.md "wikilink")，[Ubisoft發行](../Page/Ubisoft.md "wikilink")，[PC以及](../Page/PC.md "wikilink")[任天堂DS平台](../Page/任天堂DS.md "wikilink")，2004年發表。

这款遊戲就像其前作《[CSI犯罪現場](../Page/CSI犯罪現場_\(電腦遊戲\).md "wikilink")》、續作《[CSI犯罪現場：謀殺的三維](../Page/CSI犯罪現場：謀殺的三維.md "wikilink")》與《[CSI犯罪現場：邁阿密](../Page/CSI犯罪現場：邁阿密\(遊戲\).md "wikilink")》，跟隨五個佈局截然不同的案件，然後在第五個案件中連接之前的四個案件。

## 案件

### 案件1：Daredevil Disaster

World's Wildest Stunts的專業特技人員Ace
Dillinger在一次特技表演中撞毀他的摩托車，CSI組員接到通知。玩家扮演一個新成員並在本案中與[凱薩琳·韋羅斯一起工作](../Page/凱薩琳·韋羅斯.md "wikilink")。

### 案件2：Prints and Pauper

一個表面上是流浪漢被發現死在一個已棄置精神病中。但其胃內的畄西表明了其他情況。玩家在本案中與[華瑞克·布朗一起工作](../Page/華瑞克·布朗.md "wikilink")。

### 案件3：Diggin' It

在一個新賭場的建築工場中挖出人類的骨頭。這地點被認為是一個遠古的印第安公墳。玩家在本案中與[莎拉·賽德爾一起工作](../Page/莎拉·賽德爾.md "wikilink")。

### 案件4: Miss Direction

一個女人在一次排練演出中被槍殺，明顯是由一支道具槍造成。本任務中玩家與[尼克·史多克斯一起工作](../Page/尼克·史多克斯.md "wikilink")。

### 案件5：Dragon and Dropping

一條科摩多龍被殺並找到一隻腳趾。到底是誰被這隻平和的食肉動物攻擊？玩家與[吉爾·葛瑞森一起工作](../Page/吉爾·葛瑞森.md "wikilink")。

## 外部連結

  - [GameSpot上](../Page/GameSpot.md "wikilink")[CSI犯罪現場：黑暗動機的評論](https://web.archive.org/web/20070129042214/http://www.gamespot.com/pc/adventure/csidarkmotives/review.html)

[Category:2004年电子游戏](../Category/2004年电子游戏.md "wikilink")
[Category:冒險遊戲](../Category/冒險遊戲.md "wikilink")
[Category:第一人称冒险游戏](../Category/第一人称冒险游戏.md "wikilink")
[Category:电视改编电子游戏](../Category/电视改编电子游戏.md "wikilink")
[Category:育碧游戏](../Category/育碧游戏.md "wikilink")
[Category:任天堂DS遊戲](../Category/任天堂DS遊戲.md "wikilink")
[Category:Windows遊戲](../Category/Windows遊戲.md "wikilink")
[Category:CSI犯罪現場](../Category/CSI犯罪現場.md "wikilink")
[Category:加拿大開發電子遊戲](../Category/加拿大開發電子遊戲.md "wikilink")
[Category:美國開發電子遊戲](../Category/美國開發電子遊戲.md "wikilink")