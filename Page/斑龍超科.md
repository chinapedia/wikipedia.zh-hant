**斑龍超科**（Megalosauroidea）又譯**巨龍超科**，是[獸腳亞目](../Page/獸腳亞目.md "wikilink")[堅尾龍類的一個超科](../Page/堅尾龍類.md "wikilink")，牠們生存於中[侏儸紀到中](../Page/侏儸紀.md "wikilink")[白堊紀](../Page/白堊紀.md "wikilink")。[棘龍科可能屬於斑龍超科](../Page/棘龍科.md "wikilink")。斑龙超科的定义为：和巴氏[斑龙有比](../Page/斑龙.md "wikilink")[脆弱异特龙和](../Page/脆弱异特龙.md "wikilink")[家麻雀较远的最近共同祖先的物种](../Page/家麻雀.md "wikilink")。\[1\]
该演化支包含了[棘龙属](../Page/棘龙属.md "wikilink")、[斑龙属和](../Page/斑龙属.md "wikilink")[蛮龙属](../Page/蛮龙属.md "wikilink")。

## 分类

**棘龙超科（）**有时被用在斑龙超科内，由Ernst
Stromer于1915年命名。在现代的系统发生学研究内，它是斑龙超科的异名，因此棘龙超科是多余的。后来Paul
Sereno于1998年将棘龙超科定义为节点演化支，范围为包含[棘龙和](../Page/棘龙.md "wikilink")[蛮龙及其所有后代](../Page/蛮龙.md "wikilink")。Thomas
Holtz又于2004年将其定义为分支演化支，范围为比家麻雀更接近棘龙的物种。国际动物命名法委员会（还未有任何管理机构）甚至认为演化支名称如果有传统分类学后缀和暂时排在超科比庇超科更下一级的异名的话，棘龙超科应该被重用。斑龙超科的分类等级在1990年代和21世纪初时未跟随大部分古生物学文献。一系列论文支持斑龙为一个属，属于斑龙超科，并将其安置在棘龙超科内，2008年和2010年间发表认为斑龙超科为有效名称。\[2\]

斑龙超科的分类后来于2010年由Benson的研究跟进。值得注意的是，可能属于斑龙超科的几个“通配符”物种在演化树末端被排除在外，包括了[大龙属](../Page/大龙属.md "wikilink")、[皮尔逖龙属和](../Page/皮尔逖龙属.md "wikilink")[扭椎龙属](../Page/扭椎龙属.md "wikilink")。\[3\]之后，大龙属和扭椎龙属被加进演化树末端，\[4\]而这两个属的化石过于破碎。\[5\]有些“通配符”物种，如[杂肋龙属和](../Page/杂肋龙属.md "wikilink")[大盗龙属曾被认为可能是斑龙超科](../Page/大盗龙属.md "wikilink")，但之后的分析认为它们更可能接近[异特龙超科](../Page/异特龙超科.md "wikilink")\[6\]

Carrano，Benson和Sampson于2012年完成了增加更多物种的[坚尾龙类大型](../Page/坚尾龙类.md "wikilink")[系统发生学分析](../Page/系统发生学.md "wikilink")。他们在分析里使用了一个演化支名称——斑龙类（{{lang|en|Megalosauria}\]，[何塞·波拿巴于](../Page/何塞·波拿巴.md "wikilink")1850年命名），他们将其定义为一个包含斑龙属、棘龙属，它们的最近共同祖先和所有后代的节点演化支。此外，新的一科——[皮亚尼兹基龙科被命名了](../Page/皮亚尼兹基龙科.md "wikilink")，该科包含了比棘龙属或斑龙属更接近[皮亚尼兹基龙属的斑龙超科](../Page/皮亚尼兹基龙属.md "wikilink")。斑龙科也有新的亚科——[非洲猎龙亚科](../Page/斑龙科.md "wikilink")，这个亚科包含了比斑龙属更接近非洲猎龙属的物种。不像Benson的等人于2010年，他们将[杂肋龙属恢复为属于非洲猎龙亚科](../Page/杂肋龙属.md "wikilink")，而[宣汉龙属恢复为属于最原始的](../Page/宣汉龙属.md "wikilink")[中棘龙科](../Page/中棘龙科.md "wikilink")。然而，这两个属的分类位置并不稳定，且它们从分析的排除给予更多的解决方案和稳定的演化树。[扭椎龙属也比排除在外以解决斑龙科和非洲猎龙亚科](../Page/扭椎龙属.md "wikilink")。中国坚尾龙类[乐山龙属第一次被系统发生学分析包含在内](../Page/乐山龙属.md "wikilink")，但被归类为非洲猎龙亚科。而[川东虚骨龙属和](../Page/川东虚骨龙属.md "wikilink")[单脊龙属被分类在](../Page/单脊龙属.md "wikilink")[俄里翁龙类以外](../Page/俄里翁龙类.md "wikilink")。以下演化树显示了这一研究结果。\[7\]

## 參考文獻

<div class="references-small">

<references>

</references>

</div>

  -

[Category:斑龍超科](../Category/斑龍超科.md "wikilink")

1.
2.

3.
4.   [Supporting
    Information](http://www.springerlink.com/content/l496325vp2x32617/MediaObjects/114_2009_614_MOESM1_ESM.pdf)

5.
6.
7.