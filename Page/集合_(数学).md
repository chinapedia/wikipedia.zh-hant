[链接=<https://en.wikipedia.org/wiki/File:Example_of_a_set.svg>](https://zh.wikipedia.org/wiki/File:Example_of_a_set.svg "fig:链接=https://en.wikipedia.org/wiki/File:Example_of_a_set.svg")
**集合**（，或簡稱**集**）是基本的数学概念，它是[集合论的研究对象](../Page/集合论.md "wikilink")，指具有某种特定性质的事物的总体，（在最原始的集合論─[樸素集合論](../Page/樸素集合論.md "wikilink")─中的定義，集合就是“一堆東西”。）集合裡的事物（“东西”），叫作**[元素](../Page/元素_\(數學\).md "wikilink")**。若然\(x\)是集合\(A\)的元素，記作\(x\in A\)。

集合是现代[数学中一个重要的基本概念](../Page/数学.md "wikilink")，而[集合论的基本理论是在](../Page/集合论.md "wikilink")[十九世纪末被创立的](../Page/十九世纪.md "wikilink")。这里对被数学家们称为“直观的”或“朴素的”集合论进行一个简短而基本的介绍
，另外可參见[朴素集合论](../Page/朴素集合论.md "wikilink")；關於对集合作[公理化的理論](../Page/公理系统.md "wikilink")，可见[公理化集合论](../Page/公理化集合论.md "wikilink")。

## 导言

### 定义

简单来说，所谓的一个*集合*，就是将数个对象归类而分成为一个或数个形态各异的大小整体。
一般来讲，集合是具有某种特性的事物的整体，或是一些确认对象的汇集。构成集合的事物或对象称作**[元素](../Page/元素_\(數學\).md "wikilink")**或是**成员**。集合的元素可以是任何事物，可以是人，可以是物，也可以是字母或数字等。

在數學交流當中為了方便，集合會有一些別名。比如：

  - 族、系　通常指它的元素也是一些集合。

### 符号

元素通常用\(a,\ b,\ c,\ d,\ x\)等小写字母來表示；而集合通常用\(\mathbf{A,\ B,\ C,\ D,\ X}\)等大寫字母來表示。

當元素\(a\)属于集合\(\mathbf{A}\)時，记作\(a\in\mathbf{A}\)。

当元素\(a\)不属于集合\(\mathbf{A}\)时，记作\(a\not \in\mathbf{A}\)。

如果\(\mathbf{A ,\ B}\)两个集合所包含的元素完全一样，则二者相等，写作\(\mathbf{A = B}\)。

### 集合的特性

**无序性**：一个集合中，每个元素的地位都是相同的，元素之间是无序的。

  - 集合上可以定义序关系，定义了序关系后，元素之间就可以按照序关系排序。但就集合本身的特性而言，元素之间没有必然的序。（参见[序理论](../Page/序理论.md "wikilink")）

**互异性**：一个集合中，任何两个元素都认为是不相同的，即每个元素只能出现一次。

  - 有时需要对同一元素出现多次的情形进行刻画，可以使用[多重集](../Page/多重集.md "wikilink")，其中的元素允许出现多次。

**确定性**：给定一个集合，任给一个元素，该元素或者属于或者不属于该集合，二者必居其一，不允许有模棱两可的情况出现。

## 集合的表示

  - 集合可以用文字或数学符号描述，称为**描述法**，比如：

\[A=\]大于零的前三个[自然数](../Page/自然数.md "wikilink")

\[B=\]光的三原色和白色

  - 集合的另一种表示方法是在大括号中列出其元素，称为**列举法**，比如：

\[C=\left \{ 1,2,3 \right \}\]

\[D=\{\]红色\(,\)蓝色\(,\)绿色\(,\)白色\(\}\)

尽管两个集合有不同的表示，它们仍可能是相同的。比如：上述集合中，\(A=C\)而\(B=D\)，因为它们正好有相同的元素。

元素列出的顺序不同，或者元素列表中有重复，都没有关系。比如：这三个集合\(\left \{ 2,4 \right \}\)，\(\left \{ 4,2 \right \}\)和\(\left \{ 2,2,4,2 \right \}\)是相同的，同样因为它们有相同的元素。

  - 集合在不严格的意义下也可以通过草图来表示，更多信息，请见[文氏图](../Page/文氏图.md "wikilink")。

## 集合间的关系

### 子集与包含关系

[链接=<https://en.wikipedia.org/wiki/File:Venn_A_subset_B.svg>](https://zh.wikipedia.org/wiki/File:Venn_A_subset_B.svg "fig:链接=https://en.wikipedia.org/wiki/File:Venn_A_subset_B.svg")

#### 定义

集合\(A\)、\(B\)，若\(\forall a\in A\)，有\(a\in B\therefore A\subseteq B\)。则称\(A\)是\(B\)的**子集**，亦称\(A\)**包含于**\(B\)，或\(B\)**包含**\(A\)，记作\(A\subseteq B\)。

若\(A\subseteq B\)，且\(A\neq B\)，则称\(A\)是\(B\)的**真子集**，亦称\(A\)**真包含于**\(B\)，或\(B\)**真包含**\(A\)，记作\(A\subset B\)。

#### 基本性质

  - 包含关系“\(\subseteq\)”是集合间的一个**[非严格偏序关系](../Page/偏序关系#非严格偏序，自反偏序.md "wikilink")**，因为它有如下性质：
      - **自反性**：\(\forall\)集合\(S\)，\(S\subseteq S\)；（任何集合都是其本身的子集）
      - **反对称性**：\(A\subseteq B\)且\(B\subseteq A \Leftrightarrow A=B\)；（这是证明两集合相等的常用手段之一）
      - **传递性**：\(A\subseteq B\)且\(B\subseteq C\Rightarrow A\subseteq C\)；
  - 真包含关系“\(\subset\)”是集合间的一个**[严格偏序关系](../Page/偏序关系#严格偏序，反自反偏序.md "wikilink")**，因为它有如下性质：
      - **反自反性**：\(\forall\)集合\(S\)，\(S\subset S\)都不成立；
      - **非对称性**：\(A\subset B\Rightarrow B\subset A\)不成立；反之亦然；
      - **传递性**：\(A\subset B\)且\(B\subset C\Rightarrow A\subset C\)；
  - 显然，包含关系，真包含关系定义了集合间的[偏序关系](../Page/偏序关系.md "wikilink")。而\(\varnothing\)是这个偏序关系的**最小元素**，即：\(\forall\)集合\(S\)，\(\varnothing \subseteq S\)；且若\(S\neq \varnothing\)，则\(\varnothing\subset S\)，（空集是任何集合的子集，是任何非空集合的真子集）

#### 举例

:\* 所有男人的集合是所有人的集合的[真子集](../Page/真子集.md "wikilink")。

:\*
所有自然数的集合是所有[整数的集合的](../Page/整数.md "wikilink")[真子集](../Page/真子集.md "wikilink")。

:\*\(\left \{ 1,3 \right \}\subset\left \{ 1,2,3,4 \right \}\)

:\*\(\left \{ 1,2,3,4 \right \}\subseteq\left \{ 1,2,3,4 \right \}\)

## 集合的运算

### 併

两个集合可以相"加"。\(A\)和\(B\)的**聯集**是将\(A\)和\(B\)的元素放到一起构成的新集合。

#### 定义

给定集合\(A\)，\(B\)，定义运算\(\cup\)如下：\(A\cup B=\{e|e\in A\)或\(e\in B\}\)。\(A\cup B\)称为\(A\)和\(B\)的**聯集**。

[Set_union.png](https://zh.wikipedia.org/wiki/File:Set_union.png "fig:Set_union.png")

#### 示例

:\*\(\{1,2\}\cup \{\)红色\(,\)白色\(\}=\{1,2,\)红色\(,\)白色\(\}\)

:\*\(\{1,2,\)绿色\(\}\cup \{\)红色\(,\)白色\(,\)绿色\(\}=\{1,2,\)红色\(,\)白色\(,\)绿色\(\}\)

:\*\(\left \{ 1,2 \right \}\cup \left \{ 1,2 \right \}=\left \{ 1,2 \right \}\)

#### 基本性质

作为集合间的二元运算，\(\cup\)运算具有以下性质。

  - **[交换律](../Page/交换律.md "wikilink")**：\(A\cup B=B\cup A\)；
  - **[结合律](../Page/结合律.md "wikilink")**：\(\left ( A\cup B \right )\cup C=A\cup\left ( B\cup C \right )\)；
  - **[幂等律](../Page/幂等律.md "wikilink")**：\(A\cup A=A\)；
  - **[幺元](../Page/幺元.md "wikilink")**：\(\forall\)集合\(A\)，\(A\cup\varnothing=A\)；（\(\varnothing\)是\(\cup\)运算的幺元）。

### 交

一个新的集合也可以通过两个集合"共"有的元素来构造。\(A\)和\(B\)的**交集**，写作\(A\cap B\)，是既属于\(A\)的、又属于\(B\)的所有元素组成的集合。

若\(A\cap B=\varnothing\)，则\(A\)和\(B\)称作**不相交**。

[Set_intersection.png](https://zh.wikipedia.org/wiki/File:Set_intersection.png "fig:Set_intersection.png")

#### 定义

给定集合\(A\)、\(B\)，定义运算\(\cap\)如下：\(A\cap B=\{e|\in A\)且\(e\in B\}\)。\(A\cap B\)称为\(A\)和\(B\)的**交集**。

#### 基本性质

作为集合间的二元运算，\(\cap\)运算具有以下性质。

  - **[交换律](../Page/交换律.md "wikilink")**：\(A\cap B=B\cap A\)；
  - **[结合律](../Page/结合律.md "wikilink")**：\((A\cap B)\cap C=A\cap B(B\cap C)\)；
  - **[幂等律](../Page/幂等律.md "wikilink")**：\(A\cap A=A\)；
  - **[空集合](../Page/空集合.md "wikilink")**：\(\forall\)集合\(A\)，\(A\cap\varnothing=\varnothing\)；（\(\varnothing\)是\(\cap\)运算的空集合）。

其它性质还有：

  - \(A\subseteq B\Rightarrow A\cap B=A\)

#### 示例

:\*\(\{1,2\}\cap\{\)红色\(,\)白色\(\}=\varnothing\)

:\*\(\{1,2,\)绿色\(\}\cap\{\)红色\(,\)白色\(,\)绿色\(\}=\{\)绿色\(\}\)

:\*\(\{1,2\}\cap\{1,2\}=\{1,2\}\)

### 差

两个集合也可以相"减"。\(A\)在\(B\)中的**相对补集**，国际上通常写作
\(B\setminus A\)，中文教材中有时也会写作\(B-A\)。表示属于\(B\)的、但不属于\(A\)的所有元素组成的集合。

在特定情况下，所讨论的所有集合是一个给定的[全集](../Page/全集.md "wikilink")\(U\)的子集。这样，
\(U-A\)称作\(A\)的**绝对补集**，或简称**补集**（餘集），写作\(A'\)或\(C_UA\)。

[Set_difference.svg](https://zh.wikipedia.org/wiki/File:Set_difference.svg "fig:Set_difference.svg")

补集可以看作两个集合相减，有时也称作**差集**。

#### 定义

给定集合\(A\)，\(B\)，定义运算-如下：\(A-B=\{e|e\in A\)且\(e\not\in B\}\)。\(A-B\)称为\(B\)对于\(A\)的**差集**，**相对补集**或**相对餘集**。

在上下文确定了**全集**\(U\)时，对于\(U\)的某个子集\(A\)，一般称\(U-A\)为\(A\)（对于\(U\)）的**补集**或**余集**，通常记为\(A'\)或\(\bar{A}\)，也有记为\(A^\text{c}\),
\(A'\), \(\complement_U A\)，以及\(\complement A\)的。

#### 基本性质

作为集合间的二元运算，- 运算有如下基本性质：

  - \(A-A=\varnothing\)；
  - **右[幺元](../Page/幺元.md "wikilink")**：\(\forall\)集合\(A\)，\(A-\varnothing=A\)；（\(\varnothing\)是\(-\)运算的右幺元）。
  - **左**：\(\forall\)集合\(A\)，\(\varnothing-A=\varnothing\)；（\(\varnothing\)是\(-\)运算的左零元）。

#### 示例

:\*\(\{1,2\}-\{\)红色\(,\)白色\(\}=\{1,2\}\)

:\*\(\{1,2,\)绿色\(\}-\{\)红色\(,\)白色\(,\)绿色\(\}=\{1,2\}\)

:\*\(\{1,2\}-\{1,2\}=\varnothing\)

:\* 若\(U\)是整数集，则奇数的补集是偶数

### 對稱差

#### 定义

给定集合\(A\)，\(B\)，定义**对称差**运算\(\vartriangle\)如下：\(A\vartriangle B=(A-B)\cup(B-A)\)。

#### 基本性质

作为集合间的二元运算，\(\vartriangle\)运算具有如下基本性质：

  - **[交换律](../Page/交换律.md "wikilink")**：\(A\vartriangle B=B\vartriangle A\)；
  - **[结合律](../Page/结合律.md "wikilink")**：\((A\vartriangle B)\vartriangle C=A\vartriangle (B\vartriangle C)\)；
  - **[幺元](../Page/幺元.md "wikilink")**：\(\forall\)集合\(A\)，\(A\vartriangle\varnothing =A\)；（\(\varnothing\)是\(\vartriangle\)运算的幺元）。
  - **[逆元](../Page/逆元.md "wikilink")**：\(A\vartriangle A=\varnothing\)；

## 运算性质

集合的运算除了以上情况之外，集合间还具有以下运算性质：

  - **[分配律](../Page/分配律.md "wikilink")**:

\[A\cup (B\cap C)=(A\cup B)\cap(A\cup C)\]

\[A\cap (B\cup C)=(A\cap B)\cup(A\cap C)\]

  - **[对偶律](../Page/对偶律.md "wikilink")**:

\[\overline{A\cup B}=\overline{A}\cap\overline{B}\]

\[\overline{A\cap B}=\overline{A}\cup\overline{B}\]

## 集合的元素个数

上述每一个集合都有确定的元素个数；比如：集合 *A* 有三个元素、而集合 *B*
有四个。一个集合中元素的数目称为该集合的[基数](../Page/基数_\(数学\).md "wikilink")。[數學寫法有很多種](../Page/數學寫法.md "wikilink")，不同作者及不同書本用不同的寫法:
\(\operatorname{Card}(A),\ \# A,\ |A|,\ \bar{A},\ \bar{\bar{A}}\)。

集合可以没有元素。这样的集合叫做**空集**，用 \(\{\}\)
或符号\(\varnothing\)表示。比如：在2004年，集合\(A\)是所有住在月球上的人，它没有元素，则\(A=\varnothing\)。在数学上，空集非常重要。更多信息请看[空集](../Page/空集.md "wikilink")。

如果集合只含有限个元素，那么这个集合可以称为**[有限集合](../Page/有限集合.md "wikilink")**。

集合也可以有无穷多个元素，這樣的集合称为**[无限集合](../Page/无限集合.md "wikilink")**。比如：自然数集便是无限集合。关于无穷大和集合的大小的更多信息请见集合的[势](../Page/势_\(数学\).md "wikilink")。

## 公理化集合论

若把集合看作“符合任意特定性質的一堆東西”，會得出所謂[罗素悖论](../Page/罗素悖论.md "wikilink")。为解决[罗素悖论](../Page/罗素悖论.md "wikilink")，數學家提出[公理化集合论](../Page/公理化集合论.md "wikilink")。在公理集合论中，集合是一个不加定义的概念。

## 類

在更深層的[公理化数学中](../Page/公理化数学.md "wikilink")，**集合**仅仅是一种特殊的[**类**](../Page/类_\(数学\).md "wikilink")，是“良性类”，是能够成为其它类的元素的类。

类区分为两种：一种是可以顺利进行类运算的“**良性类**”，我们把这种“良性类”称为**集合**；另一种是要限制运算的“**本性类**”，对于本性类，类运算并不是都能进行的。

**定义**
类A如果满足条件“\(\exists B(A\in B)\)”，则称类A为一个**<u>集合</u>**(简称为**集**)，记为\(\operatorname{Set}(A)\)。否则称为**本性类**。

这说明，一个集合可以作为其它类的元素，但一个本性类却不能成为其它类的元素。因此可以理解为“本性类是最高层次的类”。

## 参考文献

  - Dauben, Joseph W., *Georg Cantor: His Mathematics and Philosophy of
    the Infinite*, Boston: [Harvard University
    Press](../Page/Harvard_University_Press.md "wikilink") (1979) ISBN
    978-0-691-02447-9.
  - [Halmos, Paul R.](../Page/Paul_Halmos.md "wikilink"), *Naive Set
    Theory*, Princeton, N.J.: Van Nostrand (1960) ISBN 0-387-90092-6.
  - Stoll, Robert R., *Set Theory and Logic*, Mineola, N.Y.:  (1979)
    ISBN 0-486-63829-4.

## 参见

  - [公理化数学](../Page/公理化数学.md "wikilink")
  - [类的理论](../Page/类的理论.md "wikilink")
  - [罗素公理体系](../Page/罗素公理体系.md "wikilink")
  - [集合代数](../Page/集合代数.md "wikilink")

[J](../Category/集合论.md "wikilink") [J](../Category/朴素集合论.md "wikilink")
[J](../Category/公理化集合论.md "wikilink")