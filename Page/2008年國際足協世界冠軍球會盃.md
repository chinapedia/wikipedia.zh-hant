**2008年國際足協世界冠軍球會盃**（），即第 5
届[俱乐部世界杯](../Page/俱乐部世界杯.md "wikilink")，於2008年12月12日至21日分別在日本的東京、橫濱及豐田市舉行，決賽於橫濱國際綜合競技場進行。

今屆賽事新增了**第五名比賽**，獎金亦會加至16,500,000[美元](../Page/美元.md "wikilink")。\[1\]

## 參賽球隊

<table style="width:65%;">
<colgroup>
<col style="width: 13%" />
<col style="width: 22%" />
<col style="width: 29%" />
</colgroup>
<thead>
<tr class="header">
<th><p>球隊</p></th>
<th><p>協會</p></th>
<th><p>資格</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>準決賽</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/基多利加大學體育會.md" title="wikilink">利加大學</a></p></td>
<td><p><a href="../Page/南美洲足球協會.md" title="wikilink">南美洲足球協會</a></p></td>
<td><p><a href="../Page/2008年南美自由盃.md" title="wikilink">2008年南美自由盃冠軍</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/曼徹斯特聯足球俱樂部.md" title="wikilink">曼聯</a></p></td>
<td><p><a href="../Page/歐洲足球協會.md" title="wikilink">歐洲足球協會</a></p></td>
<td><p><a href="../Page/2007–08年歐洲冠軍聯賽.md" title="wikilink">2007–08年歐洲冠軍聯賽冠軍</a></p></td>
</tr>
<tr class="even">
<td><p><strong>半準決賽</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/帕丘卡足球俱樂部.md" title="wikilink">帕丘卡</a></p></td>
<td><p><a href="../Page/中北美洲及加勒比海足球協會.md" title="wikilink">中北美洲及加勒比海足球協會</a></p></td>
<td><p><a href="../Page/2008年中北美洲及加勒比海冠軍盃.md" title="wikilink">2008年中北美洲及加勒比海冠軍盃冠軍</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/大阪飛腳.md" title="wikilink">大阪飛腳</a></p></td>
<td><p><a href="../Page/亞洲足球協會.md" title="wikilink">亞洲足球協會</a>（<a href="../Page/日本足球協會.md" title="wikilink">主辦國足協</a>）</p></td>
<td><p><a href="../Page/2008年亞足聯冠軍聯賽.md" title="wikilink">2008年亞足聯冠軍聯賽冠軍</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/阿赫利體育俱樂部.md" title="wikilink">阿爾阿赫利</a></p></td>
<td><p><a href="../Page/非洲足球協會.md" title="wikilink">非洲足球協會</a></p></td>
<td><p>2008年<a href="../Page/非洲冠軍聯賽.md" title="wikilink">非洲冠軍聯賽冠軍</a></p></td>
</tr>
<tr class="even">
<td><p><strong>資格賽</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/懷塔科拉聯足球俱樂部.md" title="wikilink">韋達基利聯</a></p></td>
<td><p><a href="../Page/大洋洲足球協會.md" title="wikilink">大洋洲足球協會</a></p></td>
<td><p><a href="../Page/2007–08年大洋洲聯賽冠軍盃.md" title="wikilink">2007–08年大洋洲聯賽冠軍盃冠軍</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿德萊德聯足球俱樂部.md" title="wikilink">阿德萊德聯</a></p></td>
<td><p><a href="../Page/亞洲足球協會.md" title="wikilink">亞洲足球協會</a></p></td>
<td><p><a href="../Page/2008年亞洲冠軍聯賽.md" title="wikilink">2008年亞洲冠軍聯賽亞軍</a></p></td>
</tr>
</tbody>
</table>

<small>\*
由於[大阪飛腳以日本球隊身分奪得](../Page/大阪飛腳.md "wikilink")[亞洲冠軍聯賽冠軍](../Page/2008年亞洲冠軍聯賽.md "wikilink")，主辦國的位置由亞洲冠軍聯賽非日本最高名次球隊澳洲[阿德萊德聯頂替](../Page/阿德萊德聯足球俱樂部.md "wikilink")。</small>

## 比賽

*以[日本標準時間為準](../Page/日本標準時間.md "wikilink")（[UTC+9](../Page/UTC+9.md "wikilink")）*

### 資格賽

-----

### 半準決賽

-----

### 準決賽

-----

### 第五名名次賽

-----

### 季軍戰

-----

### 決賽

## 最終排名

<table style="width:45%;">
<colgroup>
<col style="width: 6%" />
<col style="width: 15%" />
<col style="width: 22%" />
</colgroup>
<thead>
<tr class="header">
<th><p>名次</p></th>
<th><p>球隊</p></th>
<th><p>協會</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>冠軍</p></td>
<td><p><strong><a href="../Page/曼徹斯特聯足球俱樂部.md" title="wikilink">曼聯</a></strong></p></td>
<td><p><a href="../Page/歐洲足球協會.md" title="wikilink">歐洲足球協會</a></p></td>
</tr>
<tr class="even">
<td><p>亞軍</p></td>
<td><p><a href="../Page/基多利加大學體育會.md" title="wikilink">利加大學</a></p></td>
<td><p><a href="../Page/南美洲足球協會.md" title="wikilink">南美洲足球協會</a></p></td>
</tr>
<tr class="odd">
<td><p>季軍</p></td>
<td><p><a href="../Page/大阪飛腳.md" title="wikilink">大阪飛腳</a></p></td>
<td><p><a href="../Page/亞洲足球協會.md" title="wikilink">亞洲足球協會</a></p></td>
</tr>
<tr class="even">
<td><p>第 4 名</p></td>
<td><p><a href="../Page/帕丘卡足球俱樂部.md" title="wikilink">帕丘卡</a></p></td>
<td><p><a href="../Page/中北美洲及加勒比海足球協會.md" title="wikilink">中北美洲及加勒比海足球協會</a></p></td>
</tr>
<tr class="odd">
<td><p>第 5 名</p></td>
<td><p><a href="../Page/阿德萊德聯足球俱樂部.md" title="wikilink">阿德萊德聯</a></p></td>
<td><p><a href="../Page/亞洲足球協會.md" title="wikilink">亞洲足球協會</a></p></td>
</tr>
<tr class="even">
<td><p>第 6 名</p></td>
<td><p><a href="../Page/阿赫利體育俱樂部.md" title="wikilink">阿赫利</a></p></td>
<td><p><a href="../Page/非洲足球協會.md" title="wikilink">非洲足球協會</a></p></td>
</tr>
<tr class="odd">
<td><p>第 7 名</p></td>
<td><p><a href="../Page/懷塔科拉聯足球俱樂部.md" title="wikilink">韋達基利聯</a></p></td>
<td><p><a href="../Page/大洋洲足球協會.md" title="wikilink">大洋洲足球協會</a></p></td>
</tr>
</tbody>
</table>

## 獎項

<table style="width:50%;">
<colgroup>
<col style="width: 12%" />
<col style="width: 22%" />
<col style="width: 15%" />
</colgroup>
<thead>
<tr class="header">
<th><p>獎項</p></th>
<th><p>得主</p></th>
<th><p>所屬球隊</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>金球獎</p></td>
<td><p><a href="../Page/韋恩·魯尼.md" title="wikilink">-{zh-cn:魯尼; zh-hk:朗尼}-</a></p></td>
<td><p><a href="../Page/曼徹斯特聯足球俱樂部.md" title="wikilink">曼聯</a></p></td>
</tr>
<tr class="even">
<td><p>銀球獎</p></td>
<td><p><a href="../Page/基斯坦奴·朗拿度.md" title="wikilink">-{zh-cn:克里斯蒂亞諾·羅納度; zh-hk:基斯坦奴·朗拿度}-</a></p></td>
<td><p><a href="../Page/曼徹斯特聯足球俱樂部.md" title="wikilink">曼聯</a></p></td>
</tr>
<tr class="odd">
<td><p>銅球獎</p></td>
<td><p></p></td>
<td><p><a href="../Page/基多利加大學體育會.md" title="wikilink">利加大學</a></p></td>
</tr>
<tr class="even">
<td><p>金靴獎</p></td>
<td><p><a href="../Page/韋恩·魯尼.md" title="wikilink">-{zh-cn:魯尼; zh-hk:朗尼}-</a></p></td>
<td><p><a href="../Page/曼徹斯特聯足球俱樂部.md" title="wikilink">曼聯</a></p></td>
</tr>
<tr class="odd">
<td><p>公平競技獎</p></td>
<td><p>–</p></td>
<td><p><a href="../Page/阿德萊德聯足球俱樂部.md" title="wikilink">阿德萊德聯</a></p></td>
</tr>
</tbody>
</table>

## 參考資料

## 外部連結

  - [FIFA Club World Cup official
    site](http://www.fifa.com/clubworldcup)

[Category:国际足联俱乐部世界杯](../Category/国际足联俱乐部世界杯.md "wikilink")
[Category:2008年日本體育](../Category/2008年日本體育.md "wikilink")
[Category:日本主辦的國際足球賽事](../Category/日本主辦的國際足球賽事.md "wikilink")

1.  [Organising committee approves tournament format with reintroduction
    of match for fifth
    place](http://www.fifa.com/clubworldcup/organisation/media/newsid=711059.html)