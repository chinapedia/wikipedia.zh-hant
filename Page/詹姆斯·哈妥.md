[JimHartle1.jpg](https://zh.wikipedia.org/wiki/File:JimHartle1.jpg "fig:JimHartle1.jpg")\]\]

**詹姆斯·伯克特·哈妥**（，），美國[物理學家](../Page/物理學家.md "wikilink")，自1966年起任教於[加州大學聖塔芭巴拉分校](../Page/加州大學聖塔芭巴拉分校.md "wikilink")(UCSB)物理系。他著名的成就包括有[廣義相對論](../Page/廣義相對論.md "wikilink")、[天文物理學與](../Page/天文物理學.md "wikilink")[量子力學詮釋等領域](../Page/量子力學詮釋.md "wikilink")。在與[史蒂芬·霍金合作之下](../Page/史蒂芬·霍金.md "wikilink")，他提出了「哈妥-霍金宇宙波函數」——其為[惠勒-得衛特方程式的一個特殊解](../Page/惠勒-得衛特方程式.md "wikilink")，用以解釋[大霹靂](../Page/大霹靂.md "wikilink")[宇宙學的初始條件](../Page/宇宙學.md "wikilink")。

與[默里·盖尔曼等人合作](../Page/默里·盖尔曼.md "wikilink")，哈妥協助架構現代版的[哥本哈根詮釋](../Page/哥本哈根詮釋.md "wikilink")，其基礎為[一致性歷史](../Page/一致性歷史.md "wikilink")(consistent
histories)。與[Dieter
Brill合作](../Page/Dieter_Brill.md "wikilink")，他發現了[布里爾-哈妥幾何子](../Page/布里爾-哈妥幾何子.md "wikilink")(Brill-Hartle
geon)，其為一項近似解，實現了[惠勒所提議的一個假設現象](../Page/惠勒.md "wikilink")——一個[重力波](../Page/重力波.md "wikilink")[波包](../Page/波包.md "wikilink")(gravitational
wave
packet)被侷限在時空中的一塊緊緻區域，所憑藉的是自身場能量的重力吸引。他也是近期一本[廣義相對論教科書的作者](../Page/廣義相對論.md "wikilink")。

## 相關條目

  - [幾何子(geon)](../Page/幾何子.md "wikilink")

## 參考文獻

  - [1](http://www.physics.ucsb.edu/~hartle/gravity.html)

  -
## 外部連結

  - [詹姆斯·哈妥個人網頁](http://www.physics.ucsb.edu/~hartle/)

  - [哈妥之學校教員資料](http://www.catalog.ucsb.edu/2003cat/profiles/hartle.htm)

  - ["The Future of
    Gravity"](http://online.itp.ucsb.edu/online/plecture/hartle/)—2000年4月線上演講([Realaudio](../Page/Realaudio.md "wikilink")
    plus slides)

  - ["Spacetime Quantum
    Mechanics"](https://web.archive.org/web/20060901071230/http://pauli.physics.lsa.umich.edu/w/arch/som/sto2001/Hartle/real/n001.htm)—線上演講(Realaudio)

  - ["The Classical Behavior of Quantum
    Universes"](https://web.archive.org/web/20060522202439/http://www.aei.mpg.de/english/contemporaryIssues/seminarsEvents/hostedConferences/conference/invitedSpeakers/hartle/index.html)—線上演講(Realaudio)

  - [Index to more Hartle
    lectures](http://www.mediasite.com/?q=hartle)—線上演講

[H](../Category/美國物理學家.md "wikilink")
[H](../Category/加州理工學院校友.md "wikilink")
[B](../Category/聖塔芭芭拉加州大學教師.md "wikilink")
[H](../Category/量子重力研究者.md "wikilink")
[Category:爱因斯坦奖获得者](../Category/爱因斯坦奖获得者.md "wikilink")