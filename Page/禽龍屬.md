**禽龍屬**（[学名](../Page/学名.md "wikilink")：**''**''，意為“[鬣蜥的牙齒](../Page/鬣蜥.md "wikilink")”）、又名**擒龍**，是屬於[蜥形綱](../Page/蜥形綱.md "wikilink")[鳥臀目](../Page/鳥臀目.md "wikilink")[鳥腳下目的](../Page/鳥腳下目.md "wikilink")[禽龍類](../Page/禽龍類.md "wikilink")，為大型[草食性動物](../Page/草食性.md "wikilink")，身長約10公尺、高3到4公尺，前手[拇指有一尖爪](../Page/拇指.md "wikilink")，可能用來抵抗掠食動物，或是協助進食。

禽龍的化石多數發現於[歐洲的](../Page/歐洲.md "wikilink")[比利時](../Page/比利時.md "wikilink")、[英國](../Page/英國.md "wikilink")、[德國](../Page/德國.md "wikilink")。牠們主要生存於[白堊紀早期的](../Page/白堊紀.md "wikilink")[巴列姆階到早](../Page/巴列姆階.md "wikilink")[阿普第階](../Page/阿普第階.md "wikilink")，約1億2600萬年前到1億2500萬年前。禽龍的演化位置大約位於行動敏捷的[稜齒龍類首次出現](../Page/稜齒龍類.md "wikilink")，演化至鳥腳下目中最繁盛的[鴨嘴龍類](../Page/鴨嘴龍類.md "wikilink")，這段演化過程的中間位置。禽龍與年代更晚的[鴨嘴龍類](../Page/鴨嘴龍類.md "wikilink")，共同屬於[禽龍類演化支](../Page/禽龍類.md "wikilink")。

禽龍的化石在1822年首次發現，並在1825年由[英國地理學家](../Page/英國.md "wikilink")[吉迪恩·曼特爾進行描述與命名](../Page/吉迪恩·曼特爾.md "wikilink")。在過去的研究歷史中，有許多化石被歸類於禽龍，年代橫跨[侏儸紀](../Page/侏儸紀.md "wikilink")[啟莫里階到](../Page/啟莫里階.md "wikilink")[白堊紀](../Page/白堊紀.md "wikilink")[森諾曼階](../Page/森諾曼階.md "wikilink")，範圍廣達[歐洲](../Page/歐洲.md "wikilink")、[北美洲](../Page/北美洲.md "wikilink")、[亞洲](../Page/亞洲.md "wikilink")、以及[北非](../Page/北非.md "wikilink")。但根據近年研究，這些化石多被歸類於其他屬，或是建立為新屬。目前有兩個有效種，分別是**貝尼薩爾禽龍**（*I.
bernissartensis*）\[1\]與**加爾韋禽龍**（*I. galvensis*）\[2\]。

禽龍是繼[斑龍之後](../Page/斑龍.md "wikilink")，世界上第二種正式命名的恐龍。斑龍、禽龍、以及[林龍是最初用來定義](../Page/林龍.md "wikilink")[恐龍總目的三個屬](../Page/恐龍總目.md "wikilink")。古生物學界對於禽龍的了解，因為新發現的化石而隨著時間不斷改變。禽龍的大量標本，包括從兩個著名[屍骨層發現的接近完整骨骸](../Page/屍骨層.md "wikilink")，使得研究人員可提出許多禽龍生活方面的假設，包括進食、移動方式、以及社會行為。禽龍的重建圖也隨著標本的新發現而改變。

## 描述

[Iguanodon_NT.jpg](https://zh.wikipedia.org/wiki/File:Iguanodon_NT.jpg "fig:Iguanodon_NT.jpg")
[Iguanodon_scale.png](https://zh.wikipedia.org/wiki/File:Iguanodon_scale.png "fig:Iguanodon_scale.png")
禽龍是種體型龐大的[草食性恐龍](../Page/草食性.md "wikilink")，可採二足或四足方式行進\[3\]。最著名的種是**貝尼薩爾禽龍**（*I.
bernissartensis*），平均重達3.08公噸\[4\]，成年個體的身長約10公尺，有些標本可能長達13公尺\[5\]。其他種的體型並沒有那麼大；外形類似、較粗壯的道氏禽龍身長8公尺，而同時代的菲頓禽龍體格則較為輕型，身長為6公尺，這兩個種已被建立為新屬\[6\]。禽龍有高大但狹窄的頭顱骨，喙狀嘴缺乏牙齒、可能覆蓋者[角質](../Page/角質.md "wikilink")，牙齒類似[鬣蜥的牙齒](../Page/鬣蜥.md "wikilink")，但更大、排列更緊密\[7\]。

禽龍的手臂長而粗壯，前肢大約是後肢的75%長度\[8\]，而手掌相當不易彎曲，所以中間三個手指可以承受重量\[9\]。[拇指是圓錐尖狀](../Page/拇指.md "wikilink")，與中間三根主要的手指垂直。在早期重建圖裡，尖狀拇指被放置在禽龍的鼻子上；稍晚的化石則透露出拇指尖爪的正確位置\[10\]。但拇指尖爪的真實作用仍處於爭論中，它們可能用於防禦、或者協助進食。[小指修長](../Page/小指.md "wikilink")、靈活，可能用來操作物體。後肢強壯，但並非適合奔跑，每個腳掌有三個腳趾。[脊柱與尾巴由骨化肌腱支撐](../Page/脊柱.md "wikilink")、堅挺（這些棒狀骨頭經常在模型或繪畫中省略）\[11\]。禽龍與較晚期的近親[鴨嘴龍類](../Page/鴨嘴龍類.md "wikilink")，在身體結構上相異不大。

## 分類及起源

[禽龍類](../Page/禽龍類.md "wikilink")[演化支的名稱來自於禽龍](../Page/演化支.md "wikilink")，禽龍類是[鳥腳下目中非常繁盛的一群](../Page/鳥腳下目.md "wikilink")，生存於中[侏儸紀到晚](../Page/侏儸紀.md "wikilink")[白堊紀](../Page/白堊紀.md "wikilink")。禽龍類除了禽龍以外，最著名的屬包括：[橡樹龍](../Page/橡樹龍.md "wikilink")、[彎龍](../Page/彎龍.md "wikilink")、[豪勇龍](../Page/豪勇龍.md "wikilink")、以及[鴨嘴龍類](../Page/鴨嘴龍類.md "wikilink")。在較早的資料中，[禽龍科是個獨立的科](../Page/禽龍科.md "wikilink")\[12\]\[13\]。在早期分類歷史，禽龍科常被當成「未分類物種集中地」，包括鳥腳下目之中，不屬於[稜齒龍科](../Page/稜齒龍科.md "wikilink")、也不是[鴨嘴龍科的許多物種](../Page/鴨嘴龍科.md "wikilink")。事實上，[卡洛夫龍](../Page/卡洛夫龍.md "wikilink")、彎龍、[刃齒龍](../Page/刃齒龍.md "wikilink")、[康納龍](../Page/康納龍.md "wikilink")、[柵齒龍](../Page/柵齒龍.md "wikilink")、[木他龍](../Page/木他龍.md "wikilink")、豪勇龍、[原巴克龍](../Page/原巴克龍.md "wikilink")，以上恐龍經常被歸類於禽龍科\[14\]。隨者[親緣分支分類法研究的盛行](../Page/親緣分支分類法.md "wikilink")，禽龍科被認為是[並系群](../Page/並系群.md "wikilink")，而上述這些恐龍成為往[鴨嘴龍類演化的演化支上的不同發展支系](../Page/鴨嘴龍類.md "wikilink")，而非單一演化支\[15\]\[16\]。類似禽龍超科的名詞，仍在科學文獻中被當成未定位演化支使用，但許多過去被歸類於禽龍超科的恐龍，現在被改分類於[鴨嘴龍超科](../Page/鴨嘴龍超科.md "wikilink")。在演化樹上，禽龍位在彎龍與豪勇龍之間，禽龍可能是演化自類似彎龍的恐龍\[17\]。[傑克·霍納](../Page/傑克·霍納.md "wikilink")（Jack
Horner）曾經提出一個理論，根據大部分的頭顱骨特徵，鴨嘴龍科可分為兩個關係相遠的演化支，禽龍屬於頭部平坦的[鴨嘴龍亞科支系](../Page/鴨嘴龍亞科.md "wikilink")，而豪勇龍則位在有冠飾的[賴氏龍亞科支系](../Page/賴氏龍亞科.md "wikilink")\[18\]，但這個觀點已被否定\[19\]\[20\]。

## 發現史

[Mantell's_Iguanodon_teeth.jpg](https://zh.wikipedia.org/wiki/File:Mantell's_Iguanodon_teeth.jpg "fig:Mantell's_Iguanodon_teeth.jpg")的牙齒圖解，來自於曼特爾在1825年的禽龍研究\]\]

### 吉迪恩·曼特爾、理查·歐文、以及恐龍的發現

長久以來，禽龍的發現過程被視為傳奇故事。在1822年，[吉迪恩·曼特爾](../Page/吉迪恩·曼特爾.md "wikilink")（Gideon
Mantell）與妻子瑪麗·安（Mary
Ann）在[英格蘭](../Page/英格蘭.md "wikilink")[薩塞克斯郡的卡克費耳德村拜訪一個病人時](../Page/薩塞克斯郡.md "wikilink")，瑪麗·安在蒂爾蓋特森林的地層中發現了禽龍的牙齒\[21\]。然而，沒有證據可以顯示曼特爾帶了他妻子一起探訪病人。而且，曼特爾在1851年宣稱是他自己發現了這些禽龍牙齒\[22\]。但並非每個人都認為這故事是虛構的\[23\]。

若根據曼特爾的筆記，他在1820年於蒂爾蓋特森林發現大型動物化石，其中帶有[獸腳類恐龍的牙齒](../Page/獸腳類.md "wikilink")。曼特爾當時認為這些化石是[肉食性動物](../Page/肉食性.md "wikilink")，可能是種巨大的史前[鱷魚](../Page/鱷魚.md "wikilink")。隔年，他發現其中帶有[草食性動物的牙齒](../Page/草食性.md "wikilink")，開始思考部分化石可能來自於大型[草食性動物](../Page/草食性.md "wikilink")。曼特爾在1822年發表的文獻《*Fossils
of the South Downs*》，提到這些化石來自於史前大型肉食性、草食性動物，但他沒有將不完整的身體化石歸類於任何一種。

不管當初事實的真相是如何，曼特爾前往[倫敦皇家學會請教當時的化石專家這些牙齒屬於哪些動物](../Page/倫敦皇家學會.md "wikilink")。當時大部分科學家，例如[威廉·巴克蘭](../Page/威廉·巴克蘭.md "wikilink")（）與[喬治·居維葉](../Page/喬治·居維葉.md "wikilink")（），最初認為這些牙齒來自於[魚類或](../Page/魚類.md "wikilink")[哺乳類](../Page/哺乳類.md "wikilink")（例如[第四紀的](../Page/第四紀.md "wikilink")[犀牛](../Page/犀牛.md "wikilink")）。在1824年，威廉·巴克蘭敘述、命名了[斑龍](../Page/斑龍.md "wikilink")，之後再度檢視曼特爾的化石，認定這些牙齒屬於史前大型爬行動物。之後喬治·居維葉也修正他的看法，這些牙齒屬於大型爬行動物，而且可能是草食性動物。喬治·居維葉在1824年再版的《*Recherches
sur les Ossemens
Fossiles*》，提到曼特爾發現的化石，並使古生物學界開始瞭解這些新發現。曼特爾開始著手寫論文，企圖找出這些牙齒與現在爬行動物的相似處，並拜訪多位學者與學術單位\[24\]。[皇家外科醫學院的一位博物學家](../Page/皇家外科醫學院.md "wikilink")[山繆·斯塔奇伯里](../Page/山繆·斯塔奇伯里.md "wikilink")（Samuel
Stutchbury），認為這些牙齒類似[鬣蜥的牙齒](../Page/鬣蜥.md "wikilink")，儘管體積是鬣蜥牙齒的兩倍大小\[25\]。在1825年，曼特爾完成他的史前爬行動物牙齒研究，並將研究結果提交給[倫敦皇家學會](../Page/倫敦皇家學會.md "wikilink")\[26\]\[27\]。
[Mantell's_Iguanodon_restoration.jpg](https://zh.wikipedia.org/wiki/File:Mantell's_Iguanodon_restoration.jpg "fig:Mantell's_Iguanodon_restoration.jpg")[梅德斯通的化石所繪製的重建圖](../Page/梅德斯通.md "wikilink")\]\]
[Maidstone_fossil_Iguanodon_1840.jpg](https://zh.wikipedia.org/wiki/File:Maidstone_fossil_Iguanodon_1840.jpg "fig:Maidstone_fossil_Iguanodon_1840.jpg")[梅德斯通發現的禽龍化石](../Page/梅德斯通.md "wikilink")\]\]
在確認過這些牙齒與[鬣蜥牙齒的相似處後](../Page/鬣蜥.md "wikilink")，曼特爾將牠們命名為**禽龍**（*Iguanodon*）；在[希臘文裡](../Page/希臘文.md "wikilink")，***iguana***意為「[鬣蜥](../Page/鬣蜥.md "wikilink")」，***odontos***意為「牙齒」\[28\]。曼特爾基於[異速成長理論](../Page/異速成長.md "wikilink")，而估計這動物的身長接近18公尺，超過當時估計的斑龍身長（12公尺）\[29\]。曼特爾最初是想將牠命名為*Iguanasaurus*（鬣蜥龍），但他的朋友[威廉·丹尼尔·科尼比尔](../Page/威廉·丹尼尔·科尼比尔.md "wikilink")（）建議這個名字比較適合鬣蜥本身，而*Iguanoides*（似鬣蜥）或*Iguanodon*（鬣蜥牙齒）是更好的選擇\[30\]\[31\]。曼特爾當時忘記取種名，所以在1829年，[弗里德里希·霍尔](../Page/弗里德里希·霍尔.md "wikilink")（Friedrich
Holl）將禽龍命名為*I. anglicum*，後來修改為安格理克斯禽龍（*I. anglicus*）\[32\]。

1834年，[肯特郡](../Page/肯特郡.md "wikilink")[梅德斯通發現了一個更好的標本](../Page/梅德斯通.md "wikilink")，屬於[下海绿石砂組地層](../Page/下海绿石砂組.md "wikilink")。曼特爾馬上便取得了該標本，他從牙齒辨認出它屬於禽龍。曼特爾嘗試根據梅德斯通化石，而製作禽龍的重建骨骼。但他犯了一個著名的錯誤，將一個角狀物置於鼻部之上\[33\]。後來所發現的保存狀態更好標本，透露出該角狀物應該是[拇指上的尖指爪](../Page/拇指.md "wikilink")。在梅德斯通所發現的標本，現在與岩石一起在[倫敦](../Page/倫敦.md "wikilink")[自然史博物館展示中](../Page/自然史博物館_\(倫敦\).md "wikilink")。在1949年，梅德斯通當地為了紀念這個發現，將一隻禽龍放在當地的[紋章上](../Page/紋章.md "wikilink")\[34\]。這個標本被認為與[克里斯蒂安·埃里希·赫尔曼·冯·迈尔](../Page/克里斯蒂安·埃里希·赫尔曼·冯·迈尔.md "wikilink")（Christian
Erich Hermann von Meyer）在1832年所命名的曼氏禽龍（*I.
mantelli*）有關聯，汪邁爾命名曼氏禽龍來取代安格理克斯禽龍；但事實上，發現安格理克斯禽龍化石的岩層，與曼氏禽龍所處岩層並不相同\[35\]。

同時，曼特爾與[理查德·歐文](../Page/理查德·歐文.md "wikilink")（）之間產生了緊張關係，歐文是個著名科學家，發現許多更好的化石，並且當時[英國位於混亂的改革時代中](../Page/英國.md "wikilink")，理查德·歐文跟政治界與科學界有良好的社交關係。歐文是個堅定的[創造論者](../Page/創造論.md "wikilink")，他反對當時引起爭論的早期版本[演化論](../Page/演化論.md "wikilink")，並將恐龍作為反擊的論點。在一項對於恐龍的研究中，歐文將恐龍的長度估計為超過61公尺，並提出恐龍並不是巨型蜥蜴，而大膽提出恐龍是先進、類似[哺乳類的動物](../Page/哺乳類.md "wikilink")；根據那個時代的理解，這些特徵是[上帝所賦予的](../Page/上帝.md "wikilink")，而非從蜥蜴轉變為類似哺乳類的動物\[36\]\[37\]。
[Crystal_palace_iguanodon.jpg](https://zh.wikipedia.org/wiki/File:Crystal_palace_iguanodon.jpg "fig:Crystal_palace_iguanodon.jpg")恐龍宴會，舉辦於禽龍模型的體內。\]\]
在1849年，曼特爾瞭解到禽龍並非類似[厚皮動物的笨重動物](../Page/厚皮動物.md "wikilink")\[38\]，而且具有長前肢，也就是說禽龍的外型並不如同歐文所認為的。但在1852年，曼特爾去世，讓他無法參與、製造[水晶宮的恐龍雕塑](../Page/水晶宮.md "wikilink")，改由歐文參與製作水晶宮恐龍雕塑；因此之後數十年，歐文版本的恐龍雕塑成為大眾所熟知的恐龍形象\[39\]。[本杰明·沃特豪斯·霍金斯](../Page/本杰明·沃特豪斯·霍金斯.md "wikilink")（）建造接近20多個不同史前動物的重建雕塑，這些混凝土雕塑由鋼與磚瓦做成骨架；其中有兩個禽龍雕塑，一個呈四肢站立姿勢，另一個以腹部躺著。在四肢站立姿勢的禽龍完成之前，霍金斯曾邀請20個客人在骨架中舉辦宴會\[40\]\[41\]\[42\]。

### 貝尼沙特

最大規模的禽龍化石是在1878年發現於[比利時](../Page/比利時.md "wikilink")[貝尼沙特的煤礦坑](../Page/貝尼沙特.md "wikilink")，位在地表322公尺（1056呎）之下，當地煤礦工人以為他們挖到[矽化木](../Page/矽化木.md "wikilink")\[43\]。在煤礦的管理者[阿方斯·布里亚尔](../Page/阿方斯·布里亚尔.md "wikilink")（）促進之下，[路易斯·道羅](../Page/路易斯·道羅.md "wikilink")（）與[路易·德波夫](../Page/路易·德波夫.md "wikilink")（）開始挖掘這些骨骸，並花數年重建牠們。當地至少挖掘出38個禽龍個體\[44\]，其中大部分為成年個體\[45\]。從1883年開始，這群化石中大部分被公開展覽，直到現在；其中11個是以骨架模型方式展出，而另外20個是以牠們被挖掘出土的型態展出\[46\]。這些禽龍化石是[布魯塞爾](../Page/布魯塞爾.md "wikilink")[比利時皇家自然科學博物館的重要展覽品](../Page/比利時皇家自然科學博物館.md "wikilink")，其中一個的複製品在[牛津大學自然歷史博物館展出](../Page/牛津大學自然歷史博物館.md "wikilink")。這些化石中，大部分被歸類於新種，貝尼薩爾禽龍（*I.
bernissartensis*），比[英國所發現的禽龍還要大型](../Page/英國.md "wikilink")、粗壯；但其中一個化石，被歸類於不明確、體型纖細的曼氏禽龍（*I.
mantelli*）。這些禽龍化石是已知第一群完整恐龍化石之一。除了這些禽龍之外，另外還發現了[植物](../Page/植物.md "wikilink")、[魚類](../Page/魚類.md "wikilink")、以及其他[爬行動物的化石](../Page/爬行動物.md "wikilink")\[47\]，例如[鱷形類的](../Page/鱷形類.md "wikilink")[伯尼斯鱷](../Page/伯尼斯鱷.md "wikilink")。
[Bernissart_Iguanodon_mounted_skeleton.jpg](https://zh.wikipedia.org/wiki/File:Bernissart_Iguanodon_mounted_skeleton.jpg "fig:Bernissart_Iguanodon_mounted_skeleton.jpg")
在當時，化石保存技術才剛起步。而且，骨頭中的[黃鐵礦會變化成](../Page/黃鐵礦.md "wikilink")[硫酸鐵](../Page/硫酸鐵.md "wikilink")，破壞這些化石，使它們破碎、粉碎。當位在地表下時，化石保存於缺氧、濕氣環境，可避免化學變化發生；但一旦接觸到乾燥的空氣時，化學轉化便開始發生。路易·德波夫等人在礦坑挖掘時，用濕黏土包覆剛挖出的化石，外層再用紙張、石膏包覆、以鐵絲環繞固定外層，產生約600個可搬運的石膏塊，總重量約130噸。石膏塊運送到布魯塞爾博物館後，他們除去外層包覆，塗抹混有[丁香油的](../Page/丁香油.md "wikilink")[明膠](../Page/明膠.md "wikilink")，以防止化石質變。他們除去骨頭表面的黃鐵礦部分，以[動物膠填補](../Page/動物膠.md "wikilink")，表面鋪上[錫箔](../Page/錫箔.md "wikilink")\[48\]。這些保護、處理方式擁有意外的功效，可將溼氣保存在化石中，並延長保護年限。在1930年代，布魯塞爾博物館決定實行更好的保護方式，以延長化石的保存狀況。布魯塞爾博物館的工作人員用結合[酒精](../Page/酒精.md "wikilink")、[砒霜](../Page/砒霜.md "wikilink")、以及[蟲膠的物質來處理](../Page/蟲膠.md "wikilink")。這個綜合物質將同時滲透入化石（酒精）、消滅所有生物媒介（砒霜）、並使化石堅固（蟲膠）。這些綜合物質擁有意外的功效，可將溼氣保存在化石中，並延長保護年限。在2003到2007年，博物館進行第三次保護工程，工作人員除去原本的明膠、動物膠、蟲膠，以[聚醋酸乙烯酯](../Page/聚醋酸乙烯酯.md "wikilink")、[氰基丙烯酸酯](../Page/氰基丙烯酸酯.md "wikilink")、[環氧樹脂等物質保護這些化石](../Page/環氧樹脂.md "wikilink")\[49\]。現代的處理方式已不採用監視化石儲藏室的溼度，而是用[聚乙二醇包覆化石](../Page/聚乙二醇.md "wikilink")，並在真空幫浦中加熱，而溼氣將立即地被移除，而小型空間被聚乙二醇填滿，可密封並補強化石\[50\]。

[Iguanodon4.jpg](https://zh.wikipedia.org/wiki/File:Iguanodon4.jpg "fig:Iguanodon4.jpg")
路易斯·道羅利用這些貝尼沙特標本，證明歐文將禽龍形容為厚皮動物是不正確的。道羅將這些身體骨骼以二足姿勢架設起來，類似[鴯鶓與](../Page/鴯鶓.md "wikilink")[沙袋鼠](../Page/沙袋鼠.md "wikilink")，並將原本放在鼻部的尖刺，重新置於[拇指上](../Page/拇指.md "wikilink")\[51\]\[52\]。然而，道羅並非完全正確，但他當時是面對第一群完整恐龍化石，具有知識與經驗上的劣勢。道羅的最大問題是他將禽龍尾巴彎曲。禽龍的尾巴因為骨化肌腱的原因，實際上將近筆直，如同這些化石剛出土的狀態。如果禽龍的尾巴以類似沙袋鼠或[袋鼠的姿勢彎曲](../Page/袋鼠.md "wikilink")，牠們的尾巴將會斷裂。如果禽龍的背部與尾巴是筆直狀態，當禽龍行走時，牠們的身體將會與地面平行，而手臂則處於隨時支撐身體的狀態\[53\]。

貝尼沙特地區的挖掘活動在1881年停止，但當地的化石並沒完全挖出土，最近仍有發現挖掘活動\[54\]。在[第一次世界大戰期間](../Page/第一次世界大戰.md "wikilink")，當地被[德意志帝國所佔據](../Page/德意志帝國.md "wikilink")，[德國古生物學家](../Page/德國.md "wikilink")[奥托·耶克尔](../Page/奥托·耶克尔.md "wikilink")（）來到[比利時](../Page/比利時.md "wikilink")，準備監督貝尼沙特的重新挖掘活動。在即將發現第一個含有化石的地層時，[協約國重新佔領了貝尼沙特](../Page/協約國.md "wikilink")。該地區的挖掘活動因為財務問題而停歇了，並因為1921年的淹水而停止\[55\]。

### 到目前為止的全球發現

20世紀早期的禽龍研究，因為[世界大戰與](../Page/世界大戰.md "wikilink")[經濟大蕭條破壞](../Page/經濟大蕭條.md "wikilink")[歐洲而減少](../Page/歐洲.md "wikilink")。在1925年，[雷金纳德·胡利](../Page/雷金纳德·胡利.md "wikilink")（）將一個發現於[威特島阿瑟菲爾德村的標本](../Page/威特島.md "wikilink")，命名為阿瑟菲爾德禽龍（*I.
atherfieldensis*），這個新種引起了許多研究與分類爭論\[56\]。然而在其他洲所發現的化石，使得原本只在[歐洲發現的禽龍](../Page/歐洲.md "wikilink")，開始在全世界都有發現。這些化石包含在[非洲](../Page/非洲.md "wikilink")[突尼西亞](../Page/突尼西亞.md "wikilink")\[57\]與[撒哈拉沙漠某處所發現的牙齒](../Page/撒哈拉沙漠.md "wikilink")\[58\]、在[蒙古發現的東方禽龍](../Page/蒙古.md "wikilink")（*I.
orientalis*）\[59\]、以及在[北美洲](../Page/北美洲.md "wikilink")[美國](../Page/美國.md "wikilink")[猶他州發現的奧廷格禽龍](../Page/猶他州.md "wikilink")（*I.
ottingeri*）\[60\]，與在[南達科他州發現的拉科塔禽龍](../Page/南達科他州.md "wikilink")（*I.
lakotaensis*）\[61\]。拉科塔禽龍現在是獨立的屬，[拉科塔達科塔齒龍](../Page/達科塔齒龍.md "wikilink")（*D.
lakotaensis*）\[62\]。

[恐龍文藝復興開始於](../Page/恐龍文藝復興.md "wikilink")1969年對於[恐爪龍的重新描述](../Page/恐爪龍.md "wikilink")，但禽龍並非恐龍文藝復興的初期研究對象之一，不過並沒有被忽視太久。[大衛·威顯穆沛](../Page/大衛·威顯穆沛.md "wikilink")（David
B.
Weishampel）對於[鳥腳下目恐龍進食方法的研究](../Page/鳥腳下目.md "wikilink")，提供了禽龍進食方式的相關資訊\[63\]；而[大衛·諾曼](../Page/大衛·諾曼.md "wikilink")（David
B.
Norman）針對禽龍屬的多層面研究，使得禽龍成為最研究最深入的恐龍之一\[64\]\[65\]\[66\]\[67\]。此外，在[德國](../Page/德國.md "wikilink")[北萊茵-威斯伐倫州](../Page/北萊茵-威斯伐倫州.md "wikilink")[布里隆鎮一個村中發現的眾多禽龍骨骸](../Page/布里隆.md "wikilink")，則提供禽龍是群居動物的證據。這群禽龍包含至少15個個體，身長範圍從2到8公尺之間，牠們可能是被洪水所淹死的；而其中某些修長個體屬於相近的[曼特爾龍或](../Page/曼特爾龍.md "wikilink")[道羅齒龍](../Page/道羅齒龍.md "wikilink")（當時被認為是禽龍的一種）\[68\]\[69\]。

禽龍的化石也用在搜尋恐龍[DNA等生物分子的研究中](../Page/DNA.md "wikilink")。[格雷厄姆·恩布里](../Page/格雷厄姆·恩布里.md "wikilink")（）等人對於搜尋禽龍化石裡的殘餘[蛋白質](../Page/蛋白質.md "wikilink")，在禽龍的一個[肋骨中發現了可辨認的典型骨頭蛋白質](../Page/肋骨.md "wikilink")，例如[磷蛋白質與](../Page/磷蛋白質.md "wikilink")[蛋白多醣](../Page/蛋白多醣.md "wikilink")\[70\]。

## 物種

[Iguanodon_de_Bernissart_IRSNB_01.JPG](https://zh.wikipedia.org/wiki/File:Iguanodon_de_Bernissart_IRSNB_01.JPG "fig:Iguanodon_de_Bernissart_IRSNB_01.JPG")\]\]
因為禽龍是最早被命名的恐龍之一，過去曾有許多的種被歸類於禽龍屬。不同於其他早期恐龍（例如[斑龍與](../Page/斑龍.md "wikilink")[畸形龍](../Page/畸形龍.md "wikilink")），禽龍並沒有成為「未分類物種集中地」，但禽龍仍擁有複雜的分類歷史，而牠們的分類目前仍持續進行重新研究\[71\]\[72\]\[73\]\[74\]。其中最著名種是貝尼薩爾禽龍，化石已發現於[比利時](../Page/比利時.md "wikilink")、[英格蘭](../Page/英格蘭.md "wikilink")，可能還有[德國](../Page/德國.md "wikilink")、[西班牙](../Page/西班牙.md "wikilink")、以及[法國](../Page/法國.md "wikilink")；而發現於[美國](../Page/美國.md "wikilink")[猶他州與](../Page/猶他州.md "wikilink")[南達科他州的化石已建立為新屬](../Page/南達科他州.md "wikilink")；發現於[突尼西亞](../Page/突尼西亞.md "wikilink")、[蒙古的化石](../Page/蒙古.md "wikilink")，可能屬於禽龍屬。在2008年，葛瑞格利·保羅（Gregory
S.
Paul）重新研究禽龍的各種，將貝尼薩爾禽龍的範圍限定在比利時貝尼沙特附近發現的化石；而歐洲其他地區發現的禽龍化石，年代為[巴列姆階](../Page/巴列姆階.md "wikilink")，體型較為粗壯，另外標明為*Iguanodon*
sp.，意為未確定名稱的種\[75\]。貝尼薩爾禽龍過去被視為典型的[英國恐龍](../Page/英國.md "wikilink")，但現在與英國關聯不大。

最初的[模式種是安格理克斯禽龍](../Page/模式種.md "wikilink")（*I.
anglicus*），但該種的[正模標本只有一個牙齒](../Page/正模標本.md "wikilink")；而且該種自從被發現以來，只有發現一個部份化石。因此在2000年3月，[國際動物命名法委員會將禽龍的模式種改為較著名的](../Page/國際動物命名法委員會.md "wikilink")**貝尼薩爾禽龍**（*I.
bernissartensis*），編號IRSNB
1534標本被選為新正模標本。禽龍的最初牙齒目前位在[紐西蘭](../Page/紐西蘭.md "wikilink")[威靈頓的](../Page/威靈頓.md "wikilink")[紐西蘭國家博物館](../Page/紐西蘭國家博物館.md "wikilink")，但並沒有被展出。禽龍的最初牙齒原本為[吉迪恩·曼特爾](../Page/吉迪恩·曼特爾.md "wikilink")（Gideon
Mantell）所有，當曼特爾死後，他的化石被交給住在紐西蘭的兒子沃尔特·曼特尔（）\[76\]。

### 目前的有效種

在眾多被歸類於禽龍屬的種中，目前只有少數被認為是有效種，其中只有一種仍歸類於禽龍屬\[77\]\[78\]。

  - 貝尼薩爾禽龍（*I.
    bernissartensis*）：由[喬治·亞伯特·布蘭潔](../Page/喬治·亞伯特·布蘭潔.md "wikilink")（George
    Albert
    Boulenger）在1881年命名，目前是禽龍的[新模式種](../Page/模式種.md "wikilink")，貝尼薩爾禽龍是因為[貝尼沙特所發現的眾多骨骸而著名](../Page/貝尼沙特.md "wikilink")，[歐洲許多地區也有發現貝尼薩爾禽龍的化石](../Page/歐洲.md "wikilink")。[大衛·諾曼](../Page/大衛·諾曼.md "wikilink")（David
    Norman）認為貝尼薩爾禽龍也包含了在[蒙古發現的東方禽龍](../Page/蒙古.md "wikilink")（*I.
    orientalis*），東方禽龍的目前狀態為[疑名](../Page/疑名.md "wikilink")\[79\]。但根據最新的研究，貝尼薩爾禽龍不包含東方禽龍\[80\]。

### 被重新歸類種

在禽龍的研究歷史中，計有約20個種被重新歸類於其他屬。

  - 霍格氏禽龍（*I.
    hoggi*）：由[理查·歐文在](../Page/理查·歐文.md "wikilink")1874年命名。化石是一個下頜，發現於[多塞特郡的](../Page/多塞特郡.md "wikilink")[波白克層](../Page/波白克層.md "wikilink")，年代為上[侏儸紀](../Page/侏儸紀.md "wikilink")[提通階到下](../Page/提通階.md "wikilink")[白堊紀](../Page/白堊紀.md "wikilink")[巴列姆階](../Page/巴列姆階.md "wikilink")。在2002年，[大衛·諾曼](../Page/大衛·諾曼.md "wikilink")（David
    Norman）與[保羅·巴雷特](../Page/保羅·巴雷特.md "wikilink")（Paul
    Barrett）將這個種重新歸類於[彎龍](../Page/彎龍.md "wikilink")，成為霍格氏彎龍（*C.
    hoggii*）\[81\]。之後有其他科學家對這個歸類提出反對意見\[82\]\[83\]。在2009年，[彼得·加爾東](../Page/彼得·加爾東.md "wikilink")（Peter
    Galton）將這個種建立為新屬，[歐文齒龍](../Page/歐文齒龍.md "wikilink")（*Owenodon*）\[84\]。
  - 大禽龍（*I.
    major*）：也是由理查·歐文命名。化石是一個發現於[威特島的](../Page/威特島.md "wikilink")[脊椎骨](../Page/脊椎骨.md "wikilink")，由歐文在1842年歸類為[扭椎龍的一個種](../Page/扭椎龍.md "wikilink")，狀態為[疑名](../Page/疑名.md "wikilink")。在1966年，Justin
    Delair將其改歸類於禽龍屬的一種。現在則被認為是安格里可斯禽龍的一個[異名](../Page/異名.md "wikilink")\[85\]，或可能需要成立為獨立種\[86\]。
  - 阿爾比斯禽龍（*Iguanodon
    albinus*）：是由[捷克](../Page/捷克.md "wikilink")[古生物學家](../Page/古生物學家.md "wikilink")[安东宁·弗里奇](../Page/安东宁·弗里奇.md "wikilink")（）在1893年命名，被懷疑是非恐龍的[爬行動物](../Page/爬行動物.md "wikilink")，目前為[主龍類的](../Page/主龍類.md "wikilink")[阿爾比斯龍](../Page/阿爾比斯龍.md "wikilink")\[87\]。
  - 阿瑟菲爾德禽龍（*I.
    atherfieldensis*）：由[雷金纳德·胡利](../Page/雷金纳德·胡利.md "wikilink")（）在1925年命名\[88\]。體型比貝尼薩爾禽龍較小、較修長，而有較長的[神經棘](../Page/神經棘.md "wikilink")。在2007年重新命名為[曼特爾龍](../Page/曼特爾龍.md "wikilink")\[89\]。
  - *I. exogyrarum*：由弗里奇在1878年命名，有時也拼成*I. exogirarum*或*I.
    exogirarus*。化石材料非常少，因此狀態為[疑名](../Page/疑名.md "wikilink")，目前被[乔治·奥利舍夫斯基](../Page/乔治·奥利舍夫斯基.md "wikilink")（）建立為新屬，[壞骨龍](../Page/壞骨龍.md "wikilink")（*Ponerosteus*）\[90\]。
  - 凡登禽龍（*I.
    valdensis*）：是由[約翰·赫克](../Page/約翰·赫克.md "wikilink")（）在1879年命名，最初被命名為[威特島龍](../Page/威特島龍.md "wikilink")，化石包含脊椎與[骨盆](../Page/骨盆.md "wikilink")，發現於[威特島的](../Page/威特島.md "wikilink")[巴列姆階地層](../Page/巴列姆階.md "wikilink")\[91\]。在1900年，Ernst
    van den
    Broeck將其改歸類於禽龍屬的一個種\[92\]。目前可能是[曼特爾龍的一個成長中個體](../Page/曼特爾龍.md "wikilink")\[93\]，或是曼特爾龍的一個未確定種\[94\]。
  - 纖細禽龍（*I.
    gracilis*）：是由萊德克在1888年命名，當時為[楔椎龍](../Page/楔椎龍.md "wikilink")（*Sphenospondylus*）的模式種。在1969年被洛尼·史提爾（Rodney
    Steel）改歸類於禽龍。目前看法是可能屬於曼特爾龍\[95\]。
  - 道氏禽龍（*I.
    dawsoni*）：是由[理查德·萊德克](../Page/理查德·萊德克.md "wikilink")（Richard
    Lydekker）在1889年命名\[96\]。是萊德克在1880年代晚期命名的兩個種之一，這兩個種皆為有效種，但很少受到討論。道氏禽龍的化石包含兩個部份身體骨骼，發現於[英格蘭](../Page/英格蘭.md "wikilink")[東薩塞克斯郡的喜士定](../Page/東薩塞克斯郡.md "wikilink")（Hastings）地層\[97\]，地質年代為下[白堊紀的](../Page/白堊紀.md "wikilink")[凡藍今階中期](../Page/凡藍今階.md "wikilink")\[98\]。在2010年，被建立為獨立屬，[重骨龍](../Page/重骨龍.md "wikilink")（*Barilium*）\[99\]。
  - 菲頓禽龍（*I.
    fittoni*）：也由萊德克在1889年命名\[100\]，也是發現於英格蘭東薩塞克斯郡的喜士定地層\[101\]。在2004年，大衛·諾曼宣稱在[西班牙發現三個部分骨骸](../Page/西班牙.md "wikilink")\[102\]，現被證實有誤\[103\]。道氏禽龍與菲頓禽龍的差異在於[脊椎與骨盆的特徵](../Page/脊椎.md "wikilink")、形狀，以及體格大小\[104\]。道氏禽龍的體格較菲頓禽龍健壯，擁有類似[彎龍的大型脊椎骨](../Page/彎龍.md "wikilink")，上有短[神經棘](../Page/神經棘.md "wikilink")；而菲頓禽龍的神經棘長而狹窄，角度陡峭\[105\]。在2010年，也被建立為獨立屬，[高刺龍](../Page/高刺龍.md "wikilink")（*Hypselospinus*）\[106\]。
  - 福氏禽龍（*I.
    foxii*）：最初是在1869年由[湯瑪斯·亨利·赫胥黎](../Page/湯瑪斯·亨利·赫胥黎.md "wikilink")（Thomas
    Henry
    Huxley）命名為[稜齒龍的](../Page/稜齒龍.md "wikilink")[模式種](../Page/模式種.md "wikilink")，歐文在1873年或1874年將它重新歸類於禽龍，但不久後被回覆成稜齒龍\[107\]。
  - 荷林頓禽龍（*I.
    hollingtoniensis*）：由萊德克在1889年所命名，目前是菲頓禽龍的[異名](../Page/異名.md "wikilink")\[108\]\[109\]，或者是*Huxleysaurus*的一個種\[110\]。一個標本曾先後被歸類於荷林頓禽龍、曼氏禽龍，但沒有深入的研究。這個標本有類似[鴨嘴龍科的下頜](../Page/鴨嘴龍科.md "wikilink")、非常粗壯的前肢，可能代表一個還沒命名的物種\[111\]。
  - 普萊斯特維奇禽龍（*I.
    prestwichii*）：是由約翰·赫克在1880年命名，已被重新歸類於[彎龍](../Page/彎龍.md "wikilink")\[112\]，也可能是獨立屬，[庫姆納龍](../Page/庫姆納龍.md "wikilink")（*Cumnoria*）。
  - 絲利禽龍（*I.
    seeleyi*）：由約翰·赫克在1882年命名，曾被認為與貝尼薩爾禽龍是同一種動物\[113\]，現在則是[道羅齒龍的](../Page/道羅齒龍.md "wikilink")[異名](../Page/異名.md "wikilink")，但仍有爭議\[114\]\[115\]。
  - 休斯禽龍（*I.
    suessii*）：由[埃曼努埃爾·邦澤爾](../Page/埃曼努埃爾·邦澤爾.md "wikilink")（Emanuel
    Bunzel）在1871年命名，目前被重新歸類於[柵齒龍](../Page/柵齒龍.md "wikilink")\[116\]。

[Dollodon.jpg](https://zh.wikipedia.org/wiki/File:Dollodon.jpg "fig:Dollodon.jpg")的標本，原先後屬於曼氏禽龍、[曼特爾龍](../Page/曼特爾龍.md "wikilink")。位於[布魯塞爾](../Page/布魯塞爾.md "wikilink")\]\]

  - 拉科塔禽龍（*I.
    lakotaensis*）：是由大衛·威顯穆沛與[菲利浦·比約克](../Page/菲利浦·比約克.md "wikilink")（Philip
    R.
    Bjork）在1989年所命名\[117\]。拉科塔禽龍是[北美洲發現的唯一禽龍有效種](../Page/北美洲.md "wikilink")，化石包含一個部份頭顱骨，發現於[美國](../Page/美國.md "wikilink")[南達科他州的](../Page/南達科他州.md "wikilink")[拉科塔組](../Page/拉科塔組.md "wikilink")，地質年代為下[白堊紀的](../Page/白堊紀.md "wikilink")[巴列姆階](../Page/巴列姆階.md "wikilink")。拉科塔禽龍的歸類是有爭議的，有些研究人員認為牠們比貝尼薩爾禽龍原始，並與[眾神花園龍](../Page/眾神花園龍.md "wikilink")（*Theiophytalia*）有接近親緣關係\[118\]，而大衛·諾曼則認為牠們是貝尼薩爾禽龍的一個[異名](../Page/異名.md "wikilink")\[119\]。在2008年，[葛瑞格利·保羅](../Page/葛瑞格利·保羅.md "wikilink")（Gregory
    S.
    Paul）重新研究禽龍的各種，將拉科塔禽龍獨立為新屬，[拉科塔達科塔齒龍](../Page/達科塔齒龍.md "wikilink")（*D.
    lakotaensis*）\[120\]。
  - 曼氏禽龍（*I.
    mantelli*）：由汪邁爾在1832年命名，但汪邁爾當初所研究的化石，與用來命名安格理克斯禽龍的化石相同\[121\]；除此之外，數個梅特斯通的標本（之後成為[選模標本](../Page/選模標本.md "wikilink")），以及一個貝尼沙特的化石，長期以來被歸類於曼氏禽龍。體型修長的貝尼沙特化石，已被重新歸類於[曼特爾龍](../Page/曼特爾龍.md "wikilink")\[122\]。在2008年，葛瑞格利·保羅將這個貝尼沙特化石獨立為新屬，[道羅齒龍](../Page/道羅齒龍.md "wikilink")（*D.
    bampingi*）\[123\]。某些研究人員主張，道羅齒龍是曼特爾龍的異名\[124\]\[125\]。
  - 東方禽龍（*I.
    orientalis*）：是由[蘇聯](../Page/蘇聯.md "wikilink")[古生物學家](../Page/古生物學家.md "wikilink")[阿纳托利·康斯坦丁诺维奇·罗特杰斯特文斯基](../Page/阿纳托利·康斯坦丁诺维奇·罗特杰斯特文斯基.md "wikilink")（Anatoly
    Konstantinovich
    Rozhdestvensky）在1952年命名\[126\]，化石材料不多。其中一個頭顱骨具有明顯拱起的口鼻部，因此該頭顱骨在1998年被重新命名為[高吻龍](../Page/高吻龍.md "wikilink")\[127\]。而東方禽龍因為無法與找出類似貝尼薩爾禽龍的特徵，因此目前的狀態為[疑名](../Page/疑名.md "wikilink")\[128\]\[129\]。
  - 菲利浦禽龍（*I. phillipsi*）：由[哈利·絲利](../Page/哈利·絲利.md "wikilink")（Harry
    Seeley）在1869年命名\[130\]，後被重新歸類於[頜鋸齒龍](../Page/頜鋸齒龍.md "wikilink")\[131\]。

除了上數物種，一個曾長期編入於貝尼薩爾禽龍的標本，在2011年被建立為獨立屬，[德拉帕倫特龍](../Page/德拉帕倫特龍.md "wikilink")（*Delapparentia*）\[132\]。

### 可疑種

目前有5個禽龍物種是[疑名或未經描述的](../Page/疑名.md "wikilink")[無資格名稱](../Page/無資格名稱.md "wikilink")。

  - 安格理克斯禽龍（*I.
    anglicus*）：在1829年由[弗里德里希·霍尔](../Page/弗里德里希·霍尔.md "wikilink")（）命名\[133\]，是禽龍最初的[模式種](../Page/模式種.md "wikilink")，後來被貝尼沙爾禽龍所取代。安格里可斯禽龍有時也被拼為*I.
    angelicus*與*I.
    anglicum*。該種的化石只有牙齒，發現於[英格蘭的](../Page/英格蘭.md "wikilink")[東薩塞克斯郡](../Page/東薩塞克斯郡.md "wikilink")，年代為下[白堊紀的](../Page/白堊紀.md "wikilink")[凡蘭吟階與](../Page/凡蘭吟階.md "wikilink")[巴列姆階](../Page/巴列姆階.md "wikilink")。
  - 希氏禽龍（*I.
    hillii*）：由[埃德温·塔利·牛顿](../Page/埃德温·塔利·牛顿.md "wikilink")（）在1892年命名，化石只有一颗牙齒，發現於[赫福郡的下白堊層](../Page/赫福郡.md "wikilink")，年代為上[白堊紀早](../Page/白堊紀.md "wikilink")[森諾曼階](../Page/森諾曼階.md "wikilink")，是某種早期[鴨嘴龍科恐龍](../Page/鴨嘴龍科.md "wikilink")\[134\]。
  - 奧廷格禽龍（*I.
    ottingeri*）：由[彼得·高尔顿](../Page/彼得·高尔顿.md "wikilink")（）與[詹姆斯·A·詹森](../Page/詹姆斯·A·詹森.md "wikilink")（）在1979年命名，化石只有牙齒，發現於[猶他州的](../Page/猶他州.md "wikilink")[雪松山組下層](../Page/雪松山組.md "wikilink")，地質年代可能為[阿普第階](../Page/阿普第階.md "wikilink")，狀態也為[疑名](../Page/疑名.md "wikilink")\[135\]。
  - 先驅禽龍（*I.
    praecursor*）：由[E·绍瓦热](../Page/E·绍瓦热.md "wikilink")（）在1876年命名，化石只有牙齒，發現於[法國](../Page/法國.md "wikilink")[加萊海峽省](../Page/加萊海峽省.md "wikilink")，年代為晚[侏儸紀](../Page/侏儸紀.md "wikilink")[啟莫里階](../Page/啟莫里階.md "wikilink")，其實是種[蜥腳下目恐龍](../Page/蜥腳下目.md "wikilink")，有時被歸類於[新牙龍](../Page/新牙龍.md "wikilink")\[136\]，但先驅禽龍與新牙龍來自於不同的地層組\[137\]。
  - 蒙古禽龍（"I.
    mongolensis"）：狀態為[無資格名稱](../Page/無資格名稱.md "wikilink")，是根據書籍上的照片來命名，化石後來被正式命名為[高吻龍](../Page/高吻龍.md "wikilink")\[138\]。

還有其他化石有限的屬與種，曾被歸類於禽龍屬的種，但相較於狀態為[疑名的阿瑟菲爾德禽龍](../Page/疑名.md "wikilink")，這些種的歸類更不確定。這些不確定種包含：*Heterosaurus*、*Hikanodon*\[139\]\[140\]、*Iguanosaurus*\[141\]、*Therosaurus*\[142\]、[扭椎龍的一個種](../Page/扭椎龍.md "wikilink")（*Streptospondylus
recentior*）、[短尾鯨龍](../Page/鯨龍.md "wikilink")（*Cetiosaurus
brachyurus*）\[143\]、以及[短體鯨龍](../Page/鯨龍.md "wikilink")（*C.
brevis*，[嵌合體](../Page/嵌合體.md "wikilink")）的部分化石\[144\]。[原禽龍是個](../Page/原禽龍.md "wikilink")[無資格名稱](../Page/無資格名稱.md "wikilink")（Nomen
nudum）\[145\]，可能是[扭椎龍屬的大扭椎龍](../Page/扭椎龍.md "wikilink")（*S.
grandis*）與邁氏扭椎龍（*S. meyeri*）兩個種，都是有效性存疑的種\[146\]。

## 古生物學

[Iguanodon_skull.JPG](https://zh.wikipedia.org/wiki/File:Iguanodon_skull.JPG "fig:Iguanodon_skull.JPG")自然歷史博物館\]\]

### 進食方式與食性

禽龍最先被注意到的特徵之一，是牠們具有[草食性爬行動物的牙齒](../Page/草食性.md "wikilink")\[147\]，但科學家對於牠們如何進食，則沒有共識。如同曼特爾所注意到的，禽龍並不像任何現存的爬行動物，牠們的下頜聯合處缺乏牙齒，形狀為勺狀，他發現禽龍的牙齒最類似[二趾樹獺與已滅絕的地懶](../Page/二趾樹獺.md "wikilink")[磨齒獸](../Page/磨齒獸.md "wikilink")。曼特爾提出假設，認為禽龍擁有可抓握的[舌頭](../Page/舌頭.md "wikilink")，可用來勾取食物\[148\]，如同[長頸鹿](../Page/長頸鹿.md "wikilink")。更完整的化石則否定了曼特爾的假設，例如：用來支撐舌頭的[舌骨很大](../Page/舌骨.md "wikilink")，顯示牠們的舌頭肌肉發達，可推動嘴部的食物，而不能抓取食物\[149\]。道羅藉由一個斷裂的下頜，指出禽龍的舌頭並非類似長頸鹿\[150\]。

禽龍的牙齒類似[鬣蜥的牙齒](../Page/鬣蜥.md "wikilink")，但較大。[鴨嘴龍科擁有多排不斷替換的牙齒](../Page/鴨嘴龍科.md "wikilink")，而禽龍在同一時間只有一副準備替換用的牙齒。[上頜骨左右兩側最多各有](../Page/上頜骨.md "wikilink")29顆牙齒，[前上頜骨則沒有牙齒](../Page/前上頜骨.md "wikilink")；下頜[齒骨左右兩側則各有](../Page/齒骨.md "wikilink")25顆牙齒；上下頜牙齒數量不一致的原因，是因為下頜的牙齒較寬\[151\]。因為這些牙齒位於頜部兩側，以及其他的生理特徵，禽龍被認為具有某種頰部，可能由肌肉所構成，禽龍可以將食物置於兩頰咀嚼，如同大部分其他[鳥臀目恐龍](../Page/鳥臀目.md "wikilink")\[152\]\[153\]。
[Iguanodon_manus_1_NHM.jpg](https://zh.wikipedia.org/wiki/File:Iguanodon_manus_1_NHM.jpg "fig:Iguanodon_manus_1_NHM.jpg")自然歷史博物館\]\]
當禽龍的嘴部閉合時，上頜骨頭會往兩側撐開，上下頜的頰齒表面會互相磨合，可磨碎中間的食物，形成類似[哺乳類的咀嚼動作](../Page/哺乳類.md "wikilink")\[154\]。因為禽龍的牙齒是不斷替換的，所以牠們能夠終生以堅硬的植物為食\[155\]。另外，禽龍的上下頜前端缺乏牙齒\[156\]，形成鈍狀的邊緣，可能覆蓋者[角質](../Page/角質.md "wikilink")，可以咬斷[樹枝](../Page/樹枝.md "wikilink")\[157\]。禽龍的[小指纖細而靈活](../Page/小指.md "wikilink")，可協助勾取食物\[158\]。

目前仍不清楚禽龍平常以何種植物為食。貝尼薩爾禽龍體型較大，可能以離地面4.5公尺以內的樹葉為食\[159\]。大衛·諾曼認為禽龍可能以[木賊](../Page/木賊.md "wikilink")、[蘇鐵](../Page/蘇鐵.md "wikilink")、以及[針葉樹為食](../Page/針葉樹.md "wikilink")\[160\]。一般認為，[白堊紀](../Page/白堊紀.md "wikilink")[開花植物的出現與](../Page/開花植物.md "wikilink")[禽龍類有關](../Page/禽龍類.md "wikilink")，導因於這些恐龍以低高度[植被為食](../Page/植被.md "wikilink")。根據假設，由於禽龍類以[裸子植物為食](../Page/裸子植物.md "wikilink")，使得類似[草的早期](../Page/草.md "wikilink")[被子植物有空間成長](../Page/被子植物.md "wikilink")\[161\]，但目前還沒有證據可以證明這個理論\[162\]\[163\]。無論禽龍以何種植物為食，根據牠們的體型與繁盛程度，牠們應該佔據者體型中到大型[草食性動物的](../Page/草食性.md "wikilink")[生態位](../Page/生態位.md "wikilink")\[164\]。禽龍在[英格蘭與以下恐龍共同生存](../Page/英格蘭.md "wikilink")：小型掠食動物[極鱷龍](../Page/極鱷龍.md "wikilink")、大型掠食動物[始暴龍](../Page/始暴龍.md "wikilink")、[重爪龍](../Page/重爪龍.md "wikilink")、[新獵龍](../Page/新獵龍.md "wikilink")、小型草食性恐龍[稜齒龍與](../Page/稜齒龍.md "wikilink")[荒漠龍](../Page/荒漠龍.md "wikilink")、[禽龍科的](../Page/禽龍科.md "wikilink")[曼特爾龍](../Page/曼特爾龍.md "wikilink")、[甲龍類的](../Page/甲龍類.md "wikilink")[多刺甲龍](../Page/多刺甲龍.md "wikilink")、以及[蜥腳下目的](../Page/蜥腳下目.md "wikilink")[畸形龍](../Page/畸形龍.md "wikilink")\[165\]。

### 姿態與移動方式

[Goodrich_Iguanodon.jpg](https://zh.wikipedia.org/wiki/File:Goodrich_Iguanodon.jpg "fig:Goodrich_Iguanodon.jpg")
早期發現的禽龍化石很破碎，使得科學家們對於禽龍的步態產生不同的看法。禽龍最初被描述成鼻上有角的四足動物。隨者更多的化石被發現，曼特爾發現禽龍的前肢遠短於後肢。他的對手理查·歐文，則認為禽龍是種具有柱狀四肢的矮胖動物。恐龍的第一個原始比例重建工作被託付給曼特爾，但他因為健康不佳的原因而拒絕了，這些禽龍雕塑後來以歐文的版本作為原型。在[貝尼沙特發現了大量禽龍化石後](../Page/貝尼沙特.md "wikilink")，科學家們發現禽龍是種二足動物。但禽龍在當時被塑造成筆直站立的步態，尾巴拖曳在地面上，充當[三腳架的第三支點](../Page/三腳架.md "wikilink")。

大衛·諾曼後來重新研究禽龍化石，他認為禽龍不可能採取筆直站立的三腳架步態，因為牠們的尾巴擁有骨化的肌腱\[166\]。如果採取三腳架步態，禽龍的硬挺尾巴將會斷裂\[167\]。若禽龍採取水平的姿勢，則更能理解牠們的手臂與[肩帶特徵](../Page/胸帶.md "wikilink")。舉例而言，禽龍的手掌相當不靈活，中間三指聚集、靠攏，上有[蹄狀指爪](../Page/蹄.md "wikilink")。這種手部結構能夠承受更多的重量。禽龍的[腕部動物相當不靈活](../Page/腕部.md "wikilink")，手臂與肩膀骨頭結實。這些特徵使得禽龍較常採取四足步態\[168\]。
[Iguanodon_feeding.jpg](https://zh.wikipedia.org/wiki/File:Iguanodon_feeding.jpg "fig:Iguanodon_feeding.jpg")
隨者禽龍的年齡增長，以及體重的增加，牠們將更常採取四足步態；幼年貝尼薩爾禽龍的手臂較成年體的短，約是後肢長度的60%；成年個體的前肢長度則為後肢的70%\[169\]。根據[禽龍類的](../Page/禽龍類.md "wikilink")[足跡化石](../Page/足跡化石.md "wikilink")，以及禽龍的手掌、手臂結構，可推論禽龍採取四足步態時，中間三根蹄狀手指可支撐重量\[170\]\[171\]。禽龍的後腳掌相當長，上有三根腳趾，牠們會採取[趾行動物的方式](../Page/趾行動物.md "wikilink")，行走時以手指與趾爪接觸地面\[172\]。根據估計，禽龍以二足奔跑的最高速度為每小時24公里\[173\]，但牠們無法使用四足步態快速的奔跑\[174\]。
[Iguanodon_Svalbard.JPG](https://zh.wikipedia.org/wiki/File:Iguanodon_Svalbard.JPG "fig:Iguanodon_Svalbard.JPG")[司瓦爾巴特群島的禽龍](../Page/司瓦爾巴特群島.md "wikilink")[足跡化石](../Page/足跡化石.md "wikilink")\]\]
在[英格蘭的早](../Page/英格蘭.md "wikilink")[白堊紀地層發現許多大型三趾](../Page/白堊紀.md "wikilink")[足跡化石](../Page/足跡化石.md "wikilink")，尤其是在[威特島的](../Page/威特島.md "wikilink")[維耳德](../Page/維耳德.md "wikilink")。在早期研究歷史，這些足跡化石很難以敘述、解釋。有些早期的研究人員認為它們與恐龍有關係。在1846年，[愛德華·泰戈特](../Page/愛德華·泰戈特.md "wikilink")（Edward
Tagart）將這些足跡化石歸類於[生跡分類單元中的禽龍](../Page/生跡分類單元.md "wikilink")[痕跡屬](../Page/痕跡屬.md "wikilink")\[175\]；而[塞缪尔·比克尔斯](../Page/塞缪尔·比克尔斯.md "wikilink")（）則在1854年發現這些足跡類似[鳥類的足跡](../Page/鳥類.md "wikilink")，但可能來自於恐龍\[176\]。在1857年，發現一個年輕禽龍的後肢化石，腳掌擁有三根腳趾，顯示這些足跡可能來自於禽龍\[177\]\[178\]。儘管缺乏直接證據，這些足跡化石常被歸類於禽龍\[179\]。在[英格蘭的一個足跡化石顯示禽龍可能以四足方式行進](../Page/英格蘭.md "wikilink")，但足跡本身保存狀態不佳，因此很難作為直接證據\[180\]。歐洲[挪威的](../Page/挪威.md "wikilink")[卑爾根群島與](../Page/卑爾根群島.md "wikilink")[司瓦爾巴特群島](../Page/司瓦爾巴特群島.md "wikilink")，也發現被歸類於禽龍痕跡屬的足跡化石，也是發現禽龍化石的地方\[181\]\[182\]。

### 拇指尖爪

[Iguanodon_thumb_spike.jpg](https://zh.wikipedia.org/wiki/File:Iguanodon_thumb_spike.jpg "fig:Iguanodon_thumb_spike.jpg")[梅德斯通](../Page/梅德斯通.md "wikilink")\]\]
[拇指尖爪是禽龍的最著名特徵之一](../Page/拇指.md "wikilink")。雖然曼特爾最初將拇指尖爪放置在禽龍的鼻部上，但道羅根據在[貝尼沙特發現的完整標本](../Page/貝尼沙特.md "wikilink")，將拇指尖爪放置於手部的正確位置上。但自從80年代以來，仍有許多恐龍的大型拇指尖爪被錯置在足部，類似[馳龍科](../Page/馳龍科.md "wikilink")，例如[西北阿根廷龍](../Page/西北阿根廷龍.md "wikilink")、[拜倫龍](../Page/拜倫龍.md "wikilink")、以及[大盜龍](../Page/大盜龍.md "wikilink")。

禽龍的拇指尖爪被認為是種對付掠食者動物近身武器，類似[短劍](../Page/短劍.md "wikilink")\[183\]\[184\]，但也可能用來挖開[水果與](../Page/水果.md "wikilink")[種子](../Page/種子.md "wikilink")\[185\]，甚至用來與其它禽龍打鬥\[186\]。一位科學家認為這個拇指尖爪連接者毒腺\[187\]，但這觀點並沒有被接受，因為尖爪的內部並非中空\[188\]，表層也沒有溝槽可使毒液流動\[189\]。

### 可能的社會行為

貝尼沙特的大量禽龍化石，有時被認為是單一災害而造成的，現在則被認為是多種原因而造成的。根據最新的理論，該地的禽龍化石來自於至少三次群體死亡事件，有大量個體在很短的地質時間內死亡（約10到100年內）\[190\]，但這並不代表禽龍是種群居動物\[191\]。貝尼沙特的幼年禽龍化石非常普遍，不同於現代群體動物的死亡模式。可能是週期性的洪水將大量的屍體沖積到湖泊或沼澤\[192\]。

德國[北萊茵-威斯伐倫州的禽龍化石](../Page/北萊茵-威斯伐倫州.md "wikilink")，則具有較大的年齡範圍，當地也發現[道羅齒龍](../Page/道羅齒龍.md "wikilink")（或[曼特爾龍](../Page/曼特爾龍.md "wikilink")）與貝尼薩爾禽龍的化石；根據地理特性，顯示可能有群居動物曾遷徙經過河邊\[193\]。

不像其他理論中的的群居動物，例如[鴨嘴龍類與](../Page/鴨嘴龍類.md "wikilink")[角龍科](../Page/角龍科.md "wikilink")，目前沒有證據顯示禽龍為[兩性異形動物](../Page/兩性異形.md "wikilink")。過去一度有理論認為，貝尼沙特的曼氏禽龍或阿瑟菲爾德禽龍（目前是道羅齒龍、曼特爾龍）代表禽龍的某種性別，可能是[雌性個體](../Page/雌性.md "wikilink")；而較大、較結實的貝尼薩爾禽龍，則可能為[雄性個體](../Page/雄性.md "wikilink")\[194\]。然而，這個理論沒有得到任何支持\[195\]\[196\]\[197\]。

[Iguanodon_Crystal_Palace.jpg](https://zh.wikipedia.org/wiki/File:Iguanodon_Crystal_Palace.jpg "fig:Iguanodon_Crystal_Palace.jpg")[水晶宮公園的早期禽龍模型](../Page/水晶宮公園.md "wikilink")\]\]
[Iguanodon_Dinopark.jpg](https://zh.wikipedia.org/wiki/File:Iguanodon_Dinopark.jpg "fig:Iguanodon_Dinopark.jpg")

## 大眾文化

自從在1825年被命名以來，禽龍已成為大眾文化的常見主題之一。在1852年，[倫敦的](../Page/倫敦.md "wikilink")[水晶宮豎立了兩個禽龍雕像](../Page/水晶宮.md "wikilink")\[198\]。在當時，牠們的[拇指尖爪被誤認為鼻角](../Page/拇指.md "wikilink")，而且被誤認為類似[大象的四足動物](../Page/大象.md "wikilink")，但這兩座雕像是最早的完全比例恐龍模型。

禽龍已出現在數個[電影中](../Page/電影.md "wikilink")；例如在[迪士尼的](../Page/華特迪士尼影業.md "wikilink")[動畫電影](../Page/動畫電影.md "wikilink")《恐龍》（**），主角為一隻名為「Aladar」的禽龍，以及牠的三個禽龍同伴。禽龍也是[哥吉拉的三個形象來源之一](../Page/哥吉拉.md "wikilink")，其他兩個分別為[暴龍與](../Page/暴龍.md "wikilink")[劍龍](../Page/劍龍.md "wikilink")\[199\]。禽龍也出現在[動畫](../Page/動畫.md "wikilink")《[歷險小恐龍](../Page/歷險小恐龍.md "wikilink")》（*The
Land Before Time*）與其系列作品。

除了電影以外，禽龍也出現在[英国广播公司的電視節目](../Page/英国广播公司.md "wikilink")《[與恐龍共舞](../Page/與恐龍共舞.md "wikilink")》（**）、[探索頻道的電視節目](../Page/探索頻道.md "wikilink")《[恐龍星球](../Page/恐龍星球.md "wikilink")》（**）、以及[阿瑟·柯南·道爾](../Page/阿瑟·柯南·道爾.md "wikilink")（Arthur
Conan
Doyle）的小說《[失落的世界](../Page/失落的世界.md "wikilink")》（**）。在[羅伯特·巴克](../Page/羅伯特·巴克.md "wikilink")（）的小說《*Raptor
Red*》中，一隻[猶他盜龍獵食一隻禽龍](../Page/猶他盜龍.md "wikilink")。另外，一個[小行星帶的](../Page/小行星帶.md "wikilink")[小行星](../Page/小行星.md "wikilink")，，則是以禽龍為名\[200\]\[201\]。

因為禽龍是最早被敘述的恐龍之一，也是最著名的恐龍之一，牠們常被當作不同時代科學界與一般大眾對於恐龍看法差異的基準點之一。禽龍的想像圖經歷過三個階段：[維多利亞時代](../Page/維多利亞時代.md "wikilink")，禽龍為類似[大象的四足動物](../Page/大象.md "wikilink")，口鼻部上有角；在20世紀60年代以前，禽龍變成二足動物，但身體為接近直立，尾巴拖曳在地面上，呈三腳架步態；從60年代到現代，禽龍的形象更為靈活，能夠以二足與四足方式行走\[202\]。

## 參考資料

## 外部連結

  - [曼特爾在1925年所描述的禽龍牙齒](https://web.archive.org/web/20070929094641/http://www.lhl.lib.mo.us/events_exhib/exhibit/exhibits/dino/man1825.htm)
    Linda Hall Library網站

  - [貝尼薩爾禽龍](https://web.archive.org/web/20060113142619/http://www.dinohunters.com/Iguanodon/bernissart_page.htm)

  - [禽龍](http://www.dinodata.org/index.php?option=com_content&task=view&id=6718&Itemid=67)
    DinoData網站（需要註冊）

  - [禽龍的介紹](http://www.enchantedlearning.com/subjects/dinosaurs/dinos/Iguanodon.shtml)
    Enchanted Learning網站

  - [禽龍基本數據](http://www.kidsdinos.com/dinosaurs-for-children.php?dinosaur=Iguanodon)
    The Dinosaur Iguanodon網站

[Category:禽龍類](../Category/禽龍類.md "wikilink")
[Category:下白堊紀恐龍](../Category/下白堊紀恐龍.md "wikilink")
[Category:歐洲恐龍](../Category/歐洲恐龍.md "wikilink")

1.

2.

3.

4.

5.

6.

7.
8.
9.
10.

11.
12.

13. Norman, David B. "Iguanodontidae". *The Illustrated Encyclopedia of
    Dinosaurs*, 110–115.

14.
15.
16.
17.
18.

19.
20.

21. [Fossil Iguanodon Tooth – Collections Online – Museum of New Zealand
    Te Papa
    Tongarewa](http://collections.tepapa.govt.nz/objectdetails.aspx?oid=212194&coltype=history&regno=gh004839)


22.

23.

24.
25.
26.
27.

28.
29.
30. Cadbury, D. (2000). *The Dinosaur Hunters*. Fourth Estate:London,
    384 p. ISBN 978-1-85702-959-8.

31.

32.

33.

34.

35.
36. Torrens, Hugh. "Politics and Paleontology". *The Complete Dinosaur,*
    175–190.

37.

38.

39.
40.

41.

42. Norman, David B. *The Illustrated Encyclopedia of Dinosaurs.* p. 11.

43.
44.
45.

46.
47.
48. De Pauw, L.F., 1902, *Notes sur les fouilles du charbonnage de
    Bernissart, Découverte, solidification et montage des Iguanodons*,
    Imprim. photo-litho, JH. & P. Jumpertz, 150 av.d’Auderghem. 25 pp

49. Pascal Godefroit & Thierry Leduc, 2008, "La conservation des
    ossements fossiles : le cas des Iguanodons de Bernissart",
    *Conservation, Exposition, Restauration d'Objets d'Art* **2** (2008)

50.
51.

52.

53.
54.  Abstracts of Papers, Sixty-Third Annual Meeting.

55.
56.

57.

58.

59.

60.

61.

62.

63.

64.
65.
66.
67.

68.
69.
70.

71.
72.

73.

74.

75.
76.

77.
78.
79.

80.
81.

82.
83.

84.

85.
86.
87.

88.
89.
90.

91.

92. Van den Broeck, Ernst, 1900, "Les dépôts à iguanodons de Bernissart
    et leur transfert dans l'étage purbeckien ou aquilonien du
    Jurassique Supérieur" Bulletin de la Société Belge Géologique'' XIV
    Mem., 39-112

93. Norman, David B. "A review of *Vectisaurus valdensis*, with comments
    on the family Iguanodontidae". *Dinosaur Systematics*, 147–161.

94.
95.
96.

97.
98.
99.

100.

101.
102.
103.

104.
105.
106.
107.

108.
109.
110. Paul, G.S. (2012). "Notes on the rising diversity of Iguanodont
     taxa, and Iguanodonts named after Darwin, Huxley, and evolutionary
     science." *Actas de V Jornadas Internacionales sobre Paleontología
     de Dinosaurios y su Entorno, Salas de los Infantes, Burgos*.
     p123-133.

111.
112.
113.
114.
115.

116.
117.

118. Brill, Kathleen and Kenneth Carpenter. "A description of a new
     ornithopod from the Lytle Member of the Purgatoire Formation (Lower
     Cretaceous) and a reassessment of the skull of *Camptosaurus*."
     *Horns and Beaks*, 49–67.

119.
120.
121.
122.
123.
124.
125.

126.
127.
128.
129.
130.

131.

132. J. I. Ruiz-Omeñaca. (2011) "[*Delapparentia turolensis* nov. gen et
     sp., un nuevo dinosaurio iguanodontoideo (Ornithischia:
     Ornithopoda) en el Cretácico Inferior de
     Galve](http://estudiosgeol.revistas.csic.es/index.php/estudiosgeol/article/view/818/850)
     ." *Estudios Geológicos* (advance online publication)

133.
134. Horner, John R., David B. Weishampel and Catherine A. Forster.
     "Hadrosauridae". *The Dinosauria*, pp 438–463.

135.
136.

137. Upchurch, Paul, Paul M. Barret, and Peter Dodson. "Sauropoda". *The
     Dinosauria*

138.

139. Christian Keferstein, 1834, *Die Naturgeschichte des Erdkörpers in
     ihren ersten Grundzügen. Zweiter Theil: Die Geologie und
     Paläonthologie*, Friedrich Fleischer, Leipzig, p. 259

140. Muncke, Georg Wilhelm, 1830, *Handbuch der Naturkunde Band 2*,
     Heidelberg 1830

141.

142.

143.
144.

145.

146.
147.
148.

149.

150. Norman, D.B. (1985). *The Illustrated Encyclopedia of Dinosaurs*,
     115.

151.
152.

153. Fastovsky, D.E., and Smith, J.B. "Dinosaur paleoecology." *The
     Dinosauria*, 614–626.

154.
155.

156.
157.
158.
159.
160.
161. Bakker, R.T. "When Dinosaurs Invented Flowers". *The Dinosaur
     Heresies*, 179–198

162.
163.

164.
165. Weishampel, D.B., Barrett, P.M., Coria, R.A., Le Loeuff, J., Xu
     Xing, Zhao Xijin, Sahni, A., Gomani, E.M.P., and Noto, C.R.
     "Dinosaur Distribution". *The Dinosauria*, 517–606.

166.
167.
168.
169.
170.

171.

172.
173.

174.
175.

176.

177.

178.

179.
180.
181.

182.

183.
184.
185.
186.
187.

188.
189.

190.
191.
192.
193.
194.

195.
196.
197.
198.

199.

200.

201.

202.