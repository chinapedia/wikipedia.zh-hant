**魏**（220年12月10日－266年2月8日\[1\]，史称**曹魏**、**魏朝**）。是中國歷史上[漢朝末期](../Page/漢朝.md "wikilink")[三國之中据有北方及](../Page/三國.md "wikilink")[中原的政權](../Page/中原.md "wikilink")。始於220年[曹丕逼迫](../Page/曹丕.md "wikilink")[漢獻帝劉協禪讓帝位](../Page/漢獻帝.md "wikilink")，篡漢為**魏**，因承繼漢朝，故具[法統地位](../Page/法統.md "wikilink")。至266年魏又被[司馬炎篡奪](../Page/司馬炎.md "wikilink")，改號為**[晉](../Page/西晉.md "wikilink")**。

[曹操受封魏公時](../Page/曹操.md "wikilink")，治所在[東漢時期](../Page/東漢.md "wikilink")[魏郡所在地的](../Page/魏郡.md "wikilink")[鄴](../Page/鄴.md "wikilink")，因此漢獻帝封他為「魏公」建立诸侯国——魏国，且如同汉朝初期诸侯王制度可以设置丞相以下百官\[2\]\[3\]，之后又进封「魏王」并以卞氏为魏国王妃\[4\]\[5\]，以曹操之女为魏国公主\[6\]，后来曹操之子曹丕篡漢时便以「魏」為國號。又因为是曹氏政权，故史稱「曹魏」，以區別於其他名「[魏](../Page/魏.md "wikilink")」的政權\[7\]。

魏是三國時期最為強大，領土最遼闊的政權，灭[蜀汉前疆域達到近](../Page/蜀汉.md "wikilink")300萬平方千米。263年，魏軍攻滅蜀汉，同年佔領[廣州](../Page/广州_\(古代\).md "wikilink")，至此曹魏疆域達到全盛，约400萬平方千米。由於曹魏盤踞[中原](../Page/中原.md "wikilink")，所以這區人口也是最多。期間最重要的政治改革有[陳群的](../Page/陳群_\(三國\).md "wikilink")[九品中正制](../Page/九品中正制.md "wikilink")，對魏晉時代之政治產生深遠影響。

## 歷史

[Shinju-kyo_Bronze_Mirror.JPG](https://zh.wikipedia.org/wiki/File:Shinju-kyo_Bronze_Mirror.JPG "fig:Shinju-kyo_Bronze_Mirror.JPG")。\]\]
曹魏未立國前，[東漢已進入群雄割據的時代](../Page/東漢.md "wikilink")。建安元年（196年）[曹操迎](../Page/曹操.md "wikilink")[漢獻帝到許昌](../Page/漢獻帝.md "wikilink")，[挾天子以令諸侯](../Page/挾天子以令諸侯.md "wikilink")，迫令各地割據勢力必須遵奉曹操，而軍事方面則選編精銳，組成一批強大的騎兵勁旅——[虎豹騎](../Page/虎豹騎.md "wikilink")，在平定中原的重要戰役中屢次建功，在掌握軍政的發展下開啟魏晉南北朝[霸府政治的先河](../Page/霸府政治.md "wikilink")，為後來的篡漢立魏打下了基礎。200年的[官渡之戰後控制了](../Page/官渡之戰.md "wikilink")[中原北方地區大部分](../Page/中原.md "wikilink")，並準備南下一舉攻佔全國。

但由於曹軍未適應到南方的地理環境，在水土不服，天氣陡轉等因素下，於在208年的[赤壁之戰中敗於](../Page/赤壁之戰.md "wikilink")[孫權與](../Page/孫權.md "wikilink")[劉備的南方聯軍](../Page/劉備.md "wikilink")，退守北方。但由於曹軍所傷多為原[劉表麾下水軍與降軍](../Page/劉表.md "wikilink")，因此孫劉勢力亦無法撼動曹操政權。由於曹操年事已高，於是終其一生只控制了中原一帶。其後漢獻帝封曹操為魏王，打破了漢高祖所訂的[白馬之盟](../Page/白馬之盟.md "wikilink")，220年，曹操去世後由次子曹丕襲位。

曹丕逼迫漢獻帝禪讓，篡漢為帝，定國號魏，並定都[洛陽](../Page/洛陽.md "wikilink")（或称[曹魏五都](../Page/曹魏五都.md "wikilink")），曹魏正式建立。曹操雖未稱帝，但曹丕稱帝后追尊他為魏武帝（廟號太祖）。漢中王[劉備於](../Page/劉備.md "wikilink")221年在[成都自立為帝](../Page/成都.md "wikilink")，國號漢，史稱蜀漢。最後[孫權也於](../Page/孫權.md "wikilink")229年在[建業稱帝國號吳](../Page/建業.md "wikilink")，至此三國正式形成。

魏文帝曹丕稱帝七年就去世。曹丕死後，長子曹叡即位，是為魏明帝。曹魏朝廷此時分為兩大派，一是以[曹真和](../Page/曹真.md "wikilink")[曹休為主的曹氏一族](../Page/曹休.md "wikilink")、二是以[司馬懿世家和](../Page/司馬懿.md "wikilink")[賈逵世家為主的新勢力](../Page/贾逵_\(三国\).md "wikilink")，日漸形成嚴重的對立，埋下日後的[高平陵之變的種子](../Page/高平陵之變.md "wikilink")。而[夏侯惇和](../Page/夏侯惇.md "wikilink")[夏侯渊死後](../Page/夏侯渊.md "wikilink")，夏侯世家人才能力沒落，漸漸遠離權勢。導致司馬世家勢力龐大，夏侯世家無法抗衡，日後[高平陵之變的發生後](../Page/高平陵之變.md "wikilink")，夏侯世家遭到流放邊境、投降蜀漢與孫吳。

[曹操出身寒族](../Page/曹操.md "wikilink")，且與閹宦有關，不以儒學為務，與當時的豪族、士大夫不同。曹操曾下「求才三令」，強調重才不重德，並以[法家之術為治](../Page/法家.md "wikilink")，要摧破豪族的儒學。曹操為一代梟雄，不僅得到眾多寒族人才支持，也得到部份豪族士大夫支持，如[荀彧](../Page/荀彧.md "wikilink")、[荀攸](../Page/荀攸.md "wikilink")。荀彧更為曹操引進不少[士大夫階層的人才](../Page/士大夫.md "wikilink")。官渡之戰，曹勝袁敗，士大夫豪族不得不暫時忍耐屈服，卻嗣機恢復。終於他們支持出身士族的[司馬懿](../Page/司馬懿.md "wikilink")，向曹氏奪回政權。\[8\]

曹魏都是在與[蜀漢](../Page/蜀漢.md "wikilink")、[孫吳的戰事中度過](../Page/孫吳.md "wikilink")，如蜀漢的[諸葛亮發動多次](../Page/諸葛亮.md "wikilink")[北伐攻打魏國](../Page/諸葛亮北伐.md "wikilink")，[曹叡多次力拒來犯守護國土](../Page/曹叡.md "wikilink")。太傅[司馬懿在](../Page/司馬懿.md "wikilink")[諸葛亮北伐戰事中立下不少戰功](../Page/諸葛亮北伐.md "wikilink")，在曹魏的政治地位漸漸提升，直至[高平陵之變](../Page/高平陵之變.md "wikilink")，司馬懿利用[兵變](../Page/兵變.md "wikilink")，剷除了曹家[宗室的](../Page/宗室.md "wikilink")[曹爽](../Page/曹爽.md "wikilink")，后其子司马师在政治斗争中又铲除了夏侯家的[夏侯玄](../Page/夏侯玄.md "wikilink")，之後[夏侯霸投靠](../Page/夏侯霸.md "wikilink")[蜀漢](../Page/蜀漢.md "wikilink")，司馬氏家族權傾全朝成為新的霸府，[司马师](../Page/司马师.md "wikilink")、[司馬昭兄弟成為朝廷中最有權勢的朝臣](../Page/司馬昭.md "wikilink")，能擅自廢立皇帝。魏帝[曹髦不甘司馬氏威脅自己帝位](../Page/曹髦.md "wikilink")，亲自攻打司馬昭，司马昭命令親信[賈充派](../Page/賈充.md "wikilink")[成濟弒害曹髦](../Page/成濟.md "wikilink")，事後僅成濟被處死，而[司馬氏家族則沒受牽連](../Page/司馬氏.md "wikilink")，因此曹魏於此時名存實亡。

隨著[蜀漢國力日下](../Page/蜀漢.md "wikilink")，263年魏國司馬氏展開攻漢計劃，派遣[鍾會](../Page/鍾會.md "wikilink")、[鄧艾](../Page/鄧艾.md "wikilink")、[諸葛緒等等攻漢](../Page/諸葛緒.md "wikilink")，結果漢後主[劉禪出降](../Page/劉禪.md "wikilink")，蜀漢亡國，隨後司馬昭便平定由[鍾會](../Page/鍾會.md "wikilink")、[姜維](../Page/姜維.md "wikilink")、[劉璿等等蜀漢殘黨與部份鍾會勢力所發起的](../Page/劉璿.md "wikilink")[鍾會之亂](../Page/鍾會之亂.md "wikilink")。後司馬昭死，其子[司馬炎襲晉王](../Page/司馬炎.md "wikilink")、相國位，於265年篡魏自立，國號晉，曹魏滅亡。

[曹奂及后人被封为陈留王](../Page/曹奂.md "wikilink")，在[晋朝受到很高的待遇](../Page/晋朝.md "wikilink")，[陈留王国历经](../Page/陈留王国.md "wikilink")4朝，经[西晋](../Page/西晋.md "wikilink")、[东晋](../Page/东晋.md "wikilink")、[刘宋](../Page/刘宋.md "wikilink")，一直传至[南朝](../Page/南北朝.md "wikilink")[南齐](../Page/南齐.md "wikilink")，国祚之长在历史上实为罕见。

## 军事

曹魏於曹操死前，兵力約30-45萬，曹操死後，曹丕濫兵用戰，興建修房，死了不少的精兵，據當時的史書記載，兵力約13-16萬，為曹魏的低落時期，到了[曹叡統治](../Page/曹叡.md "wikilink")，由於推動多子政策，兵力大增到40-47萬，到司馬家族主政後，約到60-65萬，為最高峰，司馬家族統治後的第23年（滅蜀一年），兵力破百萬。

## 文化

曹魏雖然是以軍事起家，但曹魏一族在文學上具有相當成就，如曹操和其子[曹丕和](../Page/曹丕.md "wikilink")[曹植都擅於寫詩](../Page/曹植.md "wikilink")，時稱三曹，後世又稱[建安文學](../Page/建安文學.md "wikilink")。後期君主也頗有藝術造詣，如曹操其孫[曹叡擅長詩賦](../Page/曹叡.md "wikilink")；[曹髦擅長詩文](../Page/曹髦.md "wikilink")、繪畫，被譽為才子。

## 經濟

魏、漢、吳三國中以曹操最重視[農業](../Page/農業.md "wikilink")（用[毛玠兵農合一之策](../Page/毛玠.md "wikilink")），其中以曹魏人口最多，[屯田](../Page/屯田制.md "wikilink")[墾荒的面積最廣](../Page/墾荒.md "wikilink")。

曹魏重視農業的另一實證，是其大力興修[水利](../Page/水利.md "wikilink")，其工程的規模和數量在三國中首屈一指。如233年，[關中一帶闢建渠道](../Page/關中.md "wikilink")，興修水庫，一舉改造三千多頃[鹽鹼地](../Page/鹽鹼地.md "wikilink")，所獲使國庫大為充實。再如曹魏在[河南的水利工程](../Page/河南.md "wikilink")，其成果使糧食產量倍增。

[諸葛亮北伐時期對曹魏的經濟影響甚是巨大](../Page/諸葛亮北伐.md "wikilink")，\[9\]\[10\]從[辛毗](../Page/辛毗.md "wikilink")、[楊阜的奏章中](../Page/楊阜.md "wikilink")，不止一次提到諸葛亮北伐造成的經濟困難。\[11\]\[12\]\[13\]

## 藩王

## 追封[諡號的大臣](../Page/諡號.md "wikilink")

## 行政區域

下表為262年曹魏的州郡設置數目。共領州十二，郡國八十七，縣國

### 幽州

|                                  |                                |                                  |                                    |                                  |                                  |
| -------------------------------- | ------------------------------ | -------------------------------- | ---------------------------------- | -------------------------------- | -------------------------------- |
| [范陽郡](../Page/范陽郡.md "wikilink") | [代郡](../Page/代郡.md "wikilink") | [漁陽郡](../Page/漁陽郡.md "wikilink") | [右北平郡](../Page/右北平郡.md "wikilink") | [遼西郡](../Page/遼西郡.md "wikilink") | [樂浪郡](../Page/樂浪郡.md "wikilink") |
| [上谷郡](../Page/上谷郡.md "wikilink") | [燕國](../Page/燕國.md "wikilink") | [昌黎郡](../Page/昌黎郡.md "wikilink") | [玄菟郡](../Page/玄菟郡.md "wikilink")   | [遼東郡](../Page/遼東郡.md "wikilink") | [帶方郡](../Page/帶方郡.md "wikilink") |

### 冀州

|                                                                                                                                                                                                                                                                                                                                                              |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| [魏郡](../Page/魏郡.md "wikilink")，西漢置郡。領縣九 （[鄴縣](../Page/鄴縣.md "wikilink")（郡治。北京）、[魏縣](../Page/魏縣.md "wikilink")、[斥丘縣](../Page/斥丘縣.md "wikilink")、[安陽縣](../Page/安陽縣.md "wikilink")、[蕩陰縣](../Page/蕩陰縣.md "wikilink")、[內黃縣](../Page/內黃縣.md "wikilink")、[黎陽縣](../Page/黎阳县_\(东汉\).md "wikilink")、[繁陽縣](../Page/繁陽縣.md "wikilink")、[陰安縣](../Page/陰安縣.md "wikilink")） |
| [陽平郡](../Page/阳平郡_\(曹魏\).md "wikilink")                                                                                                                                                                                                                                                                                                                      |
| [平原郡](../Page/平原郡.md "wikilink")                                                                                                                                                                                                                                                                                                                             |

### 青州

|                                  |                                  |                                  |                                |                                  |                                  |
| -------------------------------- | -------------------------------- | -------------------------------- | ------------------------------ | -------------------------------- | -------------------------------- |
| [城陽郡](../Page/城陽郡.md "wikilink") | [東萊郡](../Page/東萊郡.md "wikilink") | [北海國](../Page/北海國.md "wikilink") | [齊國](../Page/齊國.md "wikilink") | [樂安郡](../Page/樂安郡.md "wikilink") | [濟南國](../Page/濟南國.md "wikilink") |

### 并州

|                                  |                                  |                                  |                                  |                                         |                                  |
| -------------------------------- | -------------------------------- | -------------------------------- | -------------------------------- | --------------------------------------- | -------------------------------- |
| [上黨郡](../Page/上黨郡.md "wikilink") | [西河郡](../Page/西河郡.md "wikilink") | [太原郡](../Page/太原郡.md "wikilink") | [樂平郡](../Page/樂平郡.md "wikilink") | [新興郡](../Page/新兴郡_\(建安\).md "wikilink") | [雁門郡](../Page/雁門郡.md "wikilink") |

### 兗州

|                                  |                                  |                                  |                                |                                         |                                  |                                  |                                  |
| -------------------------------- | -------------------------------- | -------------------------------- | ------------------------------ | --------------------------------------- | -------------------------------- | -------------------------------- | -------------------------------- |
| [泰山郡](../Page/泰山郡.md "wikilink") | [濟北國](../Page/濟北國.md "wikilink") | [東平國](../Page/東平國.md "wikilink") | [東郡](../Page/東郡.md "wikilink") | [任城郡](../Page/任城郡_\(曹魏\).md "wikilink") | [山陽郡](../Page/山陽郡.md "wikilink") | [濟陰郡](../Page/濟陰郡.md "wikilink") | [陳留國](../Page/陳留國.md "wikilink") |

### 徐州

|                                  |                                  |                                  |                                  |                                  |                                  |
| -------------------------------- | -------------------------------- | -------------------------------- | -------------------------------- | -------------------------------- | -------------------------------- |
| [東莞郡](../Page/東莞郡.md "wikilink") | [琅琊國](../Page/琅琊國.md "wikilink") | [東海國](../Page/東海國.md "wikilink") | [廣陵郡](../Page/廣陵郡.md "wikilink") | [下邳郡](../Page/下邳郡.md "wikilink") | [彭城國](../Page/彭城國.md "wikilink") |

### 司州

原漢[司隸校尉部](../Page/司隸校尉.md "wikilink")，曹魏黃初元年改為[司州](../Page/司州.md "wikilink")。領郡國五、縣國七十

[河南尹](../Page/河南尹.md "wikilink")，西漢置郡。領縣二十四

[洛陽縣](../Page/洛陽縣.md "wikilink")（州治。郡治。中京）、[河南縣](../Page/河南县_\(汉朝\).md "wikilink")、[鞏縣](../Page/鞏縣.md "wikilink")、[河陰縣](../Page/河陰縣.md "wikilink")、[成皋縣](../Page/成皋縣.md "wikilink")、[緱氏縣](../Page/緱氏縣.md "wikilink")、[陽城縣](../Page/陽城縣.md "wikilink")、[新城縣](../Page/新城縣_\(古代\).md "wikilink")、[陸渾縣](../Page/陸渾縣.md "wikilink")、[梁縣](../Page/梁縣.md "wikilink")、[陽翟縣](../Page/陽翟縣.md "wikilink")、[滎陽縣](../Page/滎陽縣.md "wikilink")、[京縣](../Page/京縣.md "wikilink")、[密縣](../Page/密縣.md "wikilink")、[卷縣](../Page/卷縣.md "wikilink")、[陽武縣](../Page/陽武縣.md "wikilink")、[苑陵縣](../Page/苑陵縣.md "wikilink")、[中牟縣](../Page/中牟縣.md "wikilink")、[開封縣](../Page/開封縣.md "wikilink")、[原武縣](../Page/原武縣.md "wikilink")、[新鄭縣](../Page/新鄭縣.md "wikilink")、[偃師縣](../Page/偃師縣.md "wikilink")、[平縣](../Page/平縣.md "wikilink")、[穀城縣](../Page/穀城縣.md "wikilink")

[弘農郡](../Page/弘農郡.md "wikilink")，西漢置郡。領縣八

[弘農縣](../Page/弘農縣.md "wikilink")（郡治）、[湖縣](../Page/湖縣.md "wikilink")、[陝縣](../Page/陝縣.md "wikilink")、[宜陽縣](../Page/宜陽縣.md "wikilink")、[黽池縣](../Page/黽池縣.md "wikilink")、[華陰縣](../Page/華陰縣.md "wikilink")、[盧氏縣](../Page/盧氏縣.md "wikilink")、[新安縣](../Page/新安縣.md "wikilink")

[河內郡](../Page/河內郡_\(中國\).md "wikilink")，西漢置郡。領縣十六

[汲縣](../Page/汲縣.md "wikilink")、[共縣](../Page/共縣.md "wikilink")、[林慮縣](../Page/林慮縣.md "wikilink")、[獲嘉縣](../Page/獲嘉縣.md "wikilink")、[修武縣](../Page/修武縣.md "wikilink")、[野王縣](../Page/野王縣.md "wikilink")、[州縣](../Page/州縣.md "wikilink")、[懷縣](../Page/懷縣.md "wikilink")（郡治）、[平皋縣](../Page/平皋縣.md "wikilink")、[河陽縣](../Page/河陽縣_\(河南\).md "wikilink")、[沁水縣](../Page/沁水縣.md "wikilink")、[軹縣](../Page/軹縣.md "wikilink")、[山陽縣](../Page/山陽縣.md "wikilink")、[溫縣](../Page/溫縣.md "wikilink")、[朝歌縣](../Page/朝歌縣.md "wikilink")、[武德縣](../Page/武德縣.md "wikilink")

[河東郡](../Page/河東郡_\(中國\).md "wikilink")，西漢置郡。領縣十一

[安邑縣](../Page/安邑縣.md "wikilink")（郡治）、[東垣縣](../Page/東垣縣.md "wikilink")、[汾陰縣](../Page/汾陰縣.md "wikilink")、[大陽縣](../Page/大陽縣.md "wikilink")、[解縣](../Page/解縣.md "wikilink")、[蒲板縣](../Page/蒲板縣.md "wikilink")、[河北縣](../Page/河北縣.md "wikilink")、[濩澤縣](../Page/濩澤縣.md "wikilink")、[端氏縣](../Page/端氏縣.md "wikilink")、[聞喜縣](../Page/聞喜縣.md "wikilink")、[猗氏縣](../Page/猗氏縣.md "wikilink")

[平陽郡](../Page/平陽郡.md "wikilink")，曹魏黃初元年分河東郡置。領縣十一

[平陽縣](../Page/平陽縣.md "wikilink")（郡治）、[楊縣](../Page/楊縣.md "wikilink")、[永安縣](../Page/永安縣.md "wikilink")、[浦子縣](../Page/浦子縣.md "wikilink")、[狐讘縣](../Page/狐讘縣.md "wikilink")、[襄陵縣](../Page/襄陵縣.md "wikilink")、[絳邑縣](../Page/絳邑縣.md "wikilink")、[臨汾縣](../Page/臨汾縣.md "wikilink")、[北屈縣](../Page/北屈縣.md "wikilink")、[皮氏縣](../Page/皮氏縣.md "wikilink")

### 雍州

[京兆郡長安縣](../Page/京兆尹.md "wikilink")（州治。郡治。西京）、[馮翊郡](../Page/左馮翊.md "wikilink")、[扶風郡](../Page/右扶風.md "wikilink")、[北地郡](../Page/北地郡.md "wikilink")、[新平郡](../Page/新平郡.md "wikilink")、[安定郡](../Page/安定郡.md "wikilink")、[廣魏郡](../Page/廣魏郡.md "wikilink")、[天水郡](../Page/天水郡.md "wikilink")、[南安郡](../Page/南安郡_\(东汉\).md "wikilink")、[隴西郡](../Page/隴西郡.md "wikilink")

### 涼州

[武威郡](../Page/武威郡.md "wikilink")、[金城郡](../Page/金城郡.md "wikilink")、[西平郡](../Page/西平郡.md "wikilink")、[張掖郡](../Page/張掖郡.md "wikilink")、[酒泉郡](../Page/酒泉郡.md "wikilink")、[西海郡](../Page/西海郡_\(東漢\).md "wikilink")、[敦煌郡](../Page/敦煌郡.md "wikilink")

### 豫州

[缩略图](https://zh.wikipedia.org/wiki/File:合肥三国新城遗址公园.jpg "fig:缩略图")
領郡國九、縣國八十八

[陳郡](../Page/陳郡.md "wikilink")，西漢置郡。領縣六

[陳縣](../Page/陳縣.md "wikilink")（郡治）、[武平縣](../Page/武平縣.md "wikilink")、[柘縣](../Page/柘縣.md "wikilink")、[陽夏縣](../Page/陽夏縣.md "wikilink")、[扶樂縣](../Page/扶樂縣.md "wikilink")、[長平縣](../Page/長平縣.md "wikilink")

[潁川郡](../Page/潁川郡.md "wikilink")，秦置郡。領縣十五

[許昌縣](../Page/許昌縣.md "wikilink")（郡治。漢許縣。南京）、[長社縣](../Page/長社縣.md "wikilink")、[潁陰縣](../Page/潁陰縣.md "wikilink")、[潁陽縣](../Page/潁陽縣.md "wikilink")、[臨潁縣](../Page/臨潁縣.md "wikilink")、[郾縣](../Page/郾縣.md "wikilink")、[鄢陵縣](../Page/鄢陵縣.md "wikilink")、[新汲縣](../Page/新汲縣.md "wikilink")、[襄城縣](../Page/襄城縣.md "wikilink")、[繁昌縣](../Page/繁昌縣.md "wikilink")、[郟縣](../Page/郟縣.md "wikilink")、[定陵縣](../Page/定陵縣.md "wikilink")、[昆陽縣](../Page/昆陽縣.md "wikilink")、[舞陽縣](../Page/舞陽縣.md "wikilink")、[父城縣](../Page/父城縣.md "wikilink")

[汝南郡](../Page/汝南郡.md "wikilink")，西漢置郡。領縣二十九

[新息縣](../Page/新息縣.md "wikilink")（郡治）、[新陽縣](../Page/新陽縣_\(曹魏\).md "wikilink")、[安城縣](../Page/安城縣.md "wikilink")（州治）、[慎縣](../Page/慎縣.md "wikilink")、[朗陵縣](../Page/朗陵縣.md "wikilink")、[陽安縣](../Page/陽安縣.md "wikilink")、[上蔡縣](../Page/上蔡縣.md "wikilink")、[平輿縣](../Page/平輿縣.md "wikilink")、[定潁縣](../Page/定潁縣.md "wikilink")、[灈陽縣](../Page/灈陽縣.md "wikilink")、[南頓縣](../Page/南頓縣.md "wikilink")、[汝陽縣](../Page/汝陽縣.md "wikilink")、[吳房縣](../Page/吳房縣.md "wikilink")、[西平縣](../Page/西平縣.md "wikilink")、[□強縣](../Page/□強縣.md "wikilink")、[召陵縣](../Page/召陵縣.md "wikilink")、[西華縣](../Page/西華縣.md "wikilink")、[宜春縣](../Page/宜春縣.md "wikilink")、[新蔡縣](../Page/新蔡縣.md "wikilink")、[褒信縣](../Page/褒信縣.md "wikilink")、[原鹿縣](../Page/原鹿縣.md "wikilink")、[富波縣](../Page/富波縣.md "wikilink")、[固始縣](../Page/固始縣.md "wikilink")、[汝陰縣](../Page/汝陰縣.md "wikilink")、[項縣](../Page/項縣.md "wikilink")、[汝南縣](../Page/汝南縣.md "wikilink")、[安陽縣](../Page/安陽縣.md "wikilink")、[鮦陽縣](../Page/鮦陽縣.md "wikilink")、[慎陽縣](../Page/慎陽縣.md "wikilink")

[梁國](../Page/梁國_\(漢朝\).md "wikilink")，西漢置郡。領縣七

[睢陽縣](../Page/睢陽縣.md "wikilink")（郡治）、[蒙縣](../Page/蒙縣.md "wikilink")、[虞縣](../Page/虞縣.md "wikilink")、[下邑縣](../Page/下邑縣.md "wikilink")、[寧陵縣](../Page/寧陵縣.md "wikilink")、[鄢縣](../Page/鄢縣.md "wikilink")、[碭縣](../Page/碭縣.md "wikilink")

[沛國](../Page/沛國.md "wikilink")，西漢置郡。領縣四

[沛縣](../Page/沛縣.md "wikilink")（郡治）、[豐國](../Page/豐國.md "wikilink")、[杼秋縣](../Page/杼秋縣.md "wikilink")、[公丘縣](../Page/公丘縣.md "wikilink")

[譙郡](../Page/譙郡.md "wikilink")，曹魏時分沛國置。領縣十四

[譙縣](../Page/譙縣.md "wikilink")（郡治，東京）、[相縣](../Page/相縣.md "wikilink")、[蕭縣](../Page/蕭縣.md "wikilink")、[酇縣](../Page/酇縣.md "wikilink")、[山桑縣](../Page/山桑縣.md "wikilink")、[龍亢縣](../Page/龍亢縣.md "wikilink")、[蘄縣](../Page/蘄縣.md "wikilink")、[苦縣](../Page/苦縣.md "wikilink")、[宋縣](../Page/宋縣.md "wikilink")、[符離縣](../Page/符離縣.md "wikilink")、[汶縣](../Page/汶縣.md "wikilink")、[虹縣](../Page/虹縣.md "wikilink")、[輊縣](../Page/輊縣.md "wikilink")、[竹邑縣](../Page/竹邑縣.md "wikilink")

[魯郡](../Page/魯郡.md "wikilink")，西漢置郡。領縣五

[魯縣](../Page/魯縣.md "wikilink")（郡治）、[卞縣](../Page/卞縣.md "wikilink")、[鄒縣](../Page/鄒縣.md "wikilink")、[番縣](../Page/番縣.md "wikilink")、[薛縣](../Page/薛縣.md "wikilink")

[弋陽郡](../Page/弋陽郡.md "wikilink")，曹魏置郡。領縣四

[西陽縣](../Page/西陽縣.md "wikilink")、[軑縣](../Page/軑縣.md "wikilink")、[期思縣](../Page/期思縣.md "wikilink")、[弋陽縣](../Page/弋陽縣.md "wikilink")（郡治）

[安豐郡](../Page/安豐郡.md "wikilink")，曹魏置郡。領縣四

[安風縣](../Page/安風縣.md "wikilink")（郡治）、[雩婁縣](../Page/雩婁縣.md "wikilink")、[安豐縣](../Page/安豐縣.md "wikilink")、[蓼縣](../Page/蓼縣.md "wikilink")

### 揚州

領郡國二、縣國十二

[淮南郡](../Page/淮南郡.md "wikilink")，秦置郡。領縣七

[壽春縣](../Page/壽春縣.md "wikilink")（州治）、[成德縣](../Page/成德縣.md "wikilink")、[下蔡縣](../Page/下蔡縣.md "wikilink")、[義城縣](../Page/義城縣.md "wikilink")、[西曲陽縣](../Page/西曲陽縣.md "wikilink")、[平阿縣](../Page/平阿縣.md "wikilink")、[合肥縣](../Page/合肥縣.md "wikilink")（郡治）

[廬江郡](../Page/廬江郡.md "wikilink")，西漢置郡。統縣三

[六安縣](../Page/六安縣.md "wikilink")（郡治）、[陽泉縣](../Page/陽泉縣.md "wikilink")、[博安縣](../Page/博安縣.md "wikilink")、[神木縣](../Page/神木縣.md "wikilink")

### 荊州

[江夏郡](../Page/江夏郡.md "wikilink")，[上昶縣](../Page/上昶縣.md "wikilink")、[六黃縣](../Page/六黃縣.md "wikilink")，

[襄陽郡](../Page/襄陽郡.md "wikilink")，
[襄陽縣](../Page/襄陽縣.md "wikilink")（郡治、州治）、[當陽縣](../Page/當陽縣.md "wikilink")、[漢津縣](../Page/漢津縣.md "wikilink")

[新城郡](../Page/新城郡_\(曹魏\).md "wikilink")，[房陵縣](../Page/房陵縣.md "wikilink")（郡治）

[南陽郡](../Page/南陽郡.md "wikilink")，[宛縣](../Page/宛縣.md "wikilink")（郡治）、[穰縣](../Page/穰縣.md "wikilink")、[魯陽縣](../Page/魯陽縣.md "wikilink")、[鄧縣](../Page/鄧縣.md "wikilink")

[南鄉郡](../Page/南鄉郡.md "wikilink")，[順陽縣](../Page/順陽縣.md "wikilink")（郡治）

[上庸郡](../Page/上庸郡.md "wikilink")，[上庸縣](../Page/上庸縣.md "wikilink")（郡治）

[魏興郡](../Page/魏興郡.md "wikilink")，[西城縣](../Page/西城縣.md "wikilink")（郡治）

[章陵郡](../Page/章陵郡.md "wikilink")，[新野縣](../Page/新野縣.md "wikilink")、[義陽縣](../Page/義陽縣.md "wikilink")

## 君主列表及年號

## 世系圖

<center>

</center>

## 參考文獻

{{-}}

[\*1](../Category/三國.md "wikilink") [曹魏](../Category/曹魏.md "wikilink")
[Category:中国历史政权](../Category/中国历史政权.md "wikilink")
[Category:220年建立的國家或政權](../Category/220年建立的國家或政權.md "wikilink")

1.
2.  《三国志·魏志·武帝纪》：今以冀州之河东、河内、魏郡、赵国、中山、常山、钜鹿、安平、甘陵、平原凡十郡，封君为魏公。……魏国置丞相已下群卿百寮，皆如汉初诸侯王之制。

3.  《三国志·魏志·荀攸传》：魏国初建，为尚书令。

4.  《三国志·魏志·后妃传》：太祖建国，始命王后。

5.  《三国志·魏志·后妃传·武宣卞皇后》：（建安）二十四年，拜为王后，策曰："夫人卞氏，抚养诸子，有母仪之德。今进位王后，太子诸侯陪位，群卿上寿，减国内死罪一等。"

6.  《三国志·魏志·武帝纪》：夏五月，天子进公爵为魏王。……天子命王女为公主，食汤沐邑。

7.

8.  萬繩楠：《陳寅恪魏晉南北朝史演講錄》（合肥：黃山書社，1987），頁9-13。

9.  《晉書·宗室傳》：每諸葛亮入寇關中，邊兵不能制敵，中軍奔赴，輒不及事機。

10. 《三國志·魏書·郭淮傳》：蜀出鹵城。是時，隴右無谷

11. 《三國志·魏書·楊阜傳》：是時大司馬曹真伐蜀，遇雨不進。阜上疏曰：「……今年兇民饑，宜發明詔損膳減服，技巧珍玩之物，皆可罷之。……今者軍用不足，益宜節度。」

12. 《三國志·魏書·楊阜傳》：阜上疏曰：「……方今二虜合從，謀危宗廟，十萬之軍，東西奔赴，邊境無一日之娛；農夫廢業，民有饑色。……」

13. 《三國志·魏書·辛毗傳》：毗上疏曰：「竊聞諸葛亮講武治兵，而孫權市馬遼東，量其意指，似欲相左右。備豫不虞，古之善政，而今者宮室大興，加連年谷麥不收。……」