**臺中市立足球場**，又名**臺中市專用足球場**或**朝馬足球場**，位於[臺中市](../Page/臺中市.md "wikilink")[朝馬路](../Page/朝馬路.md "wikilink")、[環中路口](../Page/環中路.md "wikilink")，近[中山高速公路](../Page/中山高速公路.md "wikilink")[台中交流道與](../Page/台中交流道.md "wikilink")[中彰快速道路](../Page/中彰快速道路.md "wikilink")，隸屬於[臺中市政府](../Page/臺中市政府.md "wikilink")[教育局](../Page/臺中市政府教育局.md "wikilink")，目前委由臺中市體育會足球委員會經營管理，為一座足球專用園區。

## 設施

包括二面11人制正式[足球場](../Page/足球場.md "wikilink")、一面7人制足球場、一面5人制足球場，另加三面籃球場及停車場。交通方便，管理得當，2007年全年共有19項次足球活動，參加隊數568隊，舉辦696場球賽\[1\]，堪稱全台灣使用率最高的足球場。

## 參考資料與注釋

<div class="references-small">

<references />

</div>

## 外部連結

  - [臺中市體育會足球委員會](http://www.tcfc.org.tw/)

[Category:台灣足球場](../Category/台灣足球場.md "wikilink")
[Category:台中市體育場地](../Category/台中市體育場地.md "wikilink")
[Category:西屯區](../Category/西屯區.md "wikilink")

1.  「朝馬球場 足球場經營典範」，李弘斌，中時電子報，2007-12-29 03:38