**波氏翼龍**（屬名：*Bogolubovia*）是[翼龍目](../Page/翼龍目.md "wikilink")[神龍翼龍科的一屬](../Page/神龍翼龍科.md "wikilink")，發現於[俄羅斯的Petreowsk](../Page/俄羅斯.md "wikilink")，年代為上[白堊紀](../Page/白堊紀.md "wikilink")。波氏翼龍是在1914年被[古生物學家N](../Page/古生物學家.md "wikilink").
Nikolaevich
Bogolubov所發現、敘述，當時為[東方鳥嘴翼龍](../Page/鳥嘴翼龍.md "wikilink")（*Ornithostoma
orientalis*）。後來一度被歸類於[東方無齒翼龍](../Page/無齒翼龍.md "wikilink")（*Pteranodon
orientalis*）。在1989年，Nessov與Yarkov建立為新屬，以發現化石的科學家N. Nikolaevich
Bogolubov為名。波氏翼龍曾被歸類於[神龍翼龍科](../Page/神龍翼龍科.md "wikilink")。在1991年，Wellnofer將波氏翼龍歸類於[無齒翼龍科](../Page/無齒翼龍科.md "wikilink")。\[1\]\[2\]

目前大部分古動物學家認為波氏翼龍可能屬於神龍翼龍科。但波氏翼龍的翼展只有3到4公尺，在神龍翼龍科中屬於中等的體型。\[3\]

## 參考資料

[Category:神龍翼龍超科](../Category/神龍翼龍超科.md "wikilink")
[Category:白堊紀翼龍類](../Category/白堊紀翼龍類.md "wikilink")

1.  Bogolubov, N.N. (1914). "O pozvonk’ pterodaktilya uz’
    vyerkhnyem’lovyikh’ otlozhyenii Saratovskoi gubyernii (A propos
    d'une vertebre de Pterodactyle des depots cretaces superieurs du
    gouvernment de Sartoff). \[On a pterodactyle vertebra from Upper
    Cretaceous deposits of the Government of Saratoff\]." *Annuaire
    geologique et mineralogique de la Russie*, **16**(1): 1-7.
2.  Nessov, L.A. and Yarkov, A.A. (1989). “New Birds from the
    Cretaceous–Paleogene of the USSR and Some Remarks on the History
    of Origin and Evolution of the Class.” *Tr. Zool. Inst. Akad. Nauk
    SSSR*, **197**: 78–97.
3.  Averianov, A.O., Arkhangelsky, M.S., Pervushov, E.M., and Ivanov,
    A.V. (2005). "A New Record of an Azhdarchid (Pterosauria:
    Azhdarchidae) from the Upper Cretaceous of the Volga Region."
    *Paleontological Journal*, **39**(4): 433-439.