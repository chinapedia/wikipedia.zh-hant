**氯化铁**（FeCl<sub>3</sub>）又称**三氯化铁**，是[三价铁的](../Page/三价铁.md "wikilink")[氯化物](../Page/氯化物.md "wikilink")。它易[潮解](../Page/潮解.md "wikilink")，在潮湿的空气会[水解](../Page/水解.md "wikilink")，溶于水时会释放大量热，并产生[啡色的](../Page/啡色.md "wikilink")[酸性](../Page/酸.md "wikilink")[溶液](../Page/溶液.md "wikilink")。这个溶液可[蚀刻铜制的](../Page/蚀刻.md "wikilink")[金属](../Page/金属.md "wikilink")，甚至[不锈钢](../Page/不锈钢.md "wikilink")。

无水的氯化铁是很强的[路易斯酸](../Page/路易斯酸.md "wikilink")，可用作[有机合成的](../Page/有机合成.md "wikilink")[催化剂](../Page/催化剂.md "wikilink")。啡[黄色的六水物是氯化铁常见的商业制品](../Page/黄色.md "wikilink")，其结构为\[FeCl<sub>2</sub>(H<sub>2</sub>O)<sub>4</sub>\]Cl·2H<sub>2</sub>O（参见[三氯化铬](../Page/三氯化铬.md "wikilink")）。

加热至约315℃，氯化铁便熔化，然后变成气态。气体内含有一些Fe<sub>2</sub>Cl<sub>6</sub>（参见[氯化铝](../Page/氯化铝.md "wikilink")），会渐渐分解成[氯化亚铁](../Page/氯化亚铁.md "wikilink")（FeCl<sub>2</sub>）和氯气（Cl<sub>2</sub>）。

## 化学性质

氯化铁是颇强的[路易斯酸](../Page/路易斯酸.md "wikilink")，跟[路易斯碱](../Page/路易斯碱.md "wikilink")，例如[氧化三苯基膦](../Page/氧化三苯基膦.md "wikilink")，可组成稳定的FeCl<sub>3</sub>(OP**-Ph**<sub>3</sub>)<sub>2</sub>（**Ph**
= [苯基](../Page/苯基.md "wikilink")）。

氯化铁在[盐酸中和氯又可组成FeCl](../Page/盐酸.md "wikilink")<sub>4</sub><sup>−</sup>[配离子](../Page/配离子.md "wikilink")，它是黄色的，这就是工业浓[盐酸显黄色的原因](../Page/盐酸.md "wikilink")，可用[乙醚萃取出来](../Page/乙醚.md "wikilink")。

[草酸根离子和氯化铁反应](../Page/草酸根离子.md "wikilink")，可得到稳定的[配位化合物如](../Page/配位化合物.md "wikilink")\[Fe(C<sub>2</sub>O<sub>4</sub>)<sub>3</sub>\]<sup>3−</sup>。

氯化铁是一般强度的[氧化剂](../Page/氧化剂.md "wikilink")，可氧化[氯化亚铜为](../Page/氯化亚铜.md "wikilink")[氯化铜](../Page/氯化铜.md "wikilink")。[还原剂如](../Page/还原剂.md "wikilink")[联胺](../Page/联胺.md "wikilink")，可以使氯化铁成为铁的配位化合物。

## 制取

无水物可以用元素之间化合制取：

  -
    2 Fe + 3 Cl<sub>2</sub> → 2 FeCl<sub>3</sub>

溶液在工业上有三个制法：

1.  将[磁铁矿溶入盐酸](../Page/磁铁矿.md "wikilink")：
      -
        Fe<sub>3</sub>O<sub>4</sub> + 8 HCl → FeCl<sub>2</sub> + 2
        FeCl<sub>3</sub> + 4 H<sub>2</sub>O
2.  再加入氯气：
      -
        2 FeCl<sub>2</sub> + Cl<sub>2</sub> → 2 FeCl<sub>3</sub>

<!-- end list -->

  -
    32 FeCl<sub>2</sub> + 8 SO<sub>2</sub> + 32 HCl →
    32 FeCl<sub>3</sub> + S<sub>8</sub> + 16 H<sub>2</sub>O

将水合物和[亚硫酰氯一起加热](../Page/亚硫酰氯.md "wikilink")，可得到无水物。

## 用途

最常见的用途莫过于[蚀刻](../Page/蚀刻.md "wikilink")，特别是[印刷电路板和](../Page/印刷电路板.md "wikilink")[不锈钢](../Page/不锈钢.md "wikilink")。不过，由于它回收困难，易污染环境\[1\]，现多改用[氯化铜](../Page/氯化铜.md "wikilink")。

它用于污水处理，以除去水中的重金属和[磷酸盐](../Page/磷酸盐.md "wikilink")。\[2\]

在实验室，可用[三氯化铁测试来测试](../Page/三氯化铁测试.md "wikilink")[酚](../Page/酚.md "wikilink")，方法如下：准备1%氯化铁溶液，跟[氢氧化钠混合](../Page/氢氧化钠.md "wikilink")，直到有少量FeO(OH)[沉淀形成](../Page/沉淀.md "wikilink")。过滤该混合液。将试料溶于[水](../Page/水.md "wikilink")、[甲醇或](../Page/甲醇.md "wikilink")[乙醇](../Page/乙醇.md "wikilink")。加入混合液，若有明显的颜色转变，即酚或[烯醇在试料内](../Page/烯醇.md "wikilink")。

另外我们可以用它[催化](../Page/催化.md "wikilink")[双氧水使其分解成](../Page/双氧水.md "wikilink")[氧气和](../Page/氧气.md "wikilink")[水](../Page/水.md "wikilink")。

## 参见

  - [氯化亚铁](../Page/氯化亚铁.md "wikilink")

## 参考资料

[Category:三价铁化合物](../Category/三价铁化合物.md "wikilink")
[Category:氯化物](../Category/氯化物.md "wikilink")
[Category:铁系元素卤化物](../Category/铁系元素卤化物.md "wikilink")
[Category:脱水剂](../Category/脱水剂.md "wikilink")
[Category:有机化学试剂](../Category/有机化学试剂.md "wikilink")
[Category:潮解物质](../Category/潮解物质.md "wikilink")

1.  <http://www.cgan.net/book/books/print/g-history/big5_12/22_1.htm>
2.  <http://www.iw-recycling.org.tw/import/monthly31_m4.pdf>