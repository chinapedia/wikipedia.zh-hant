二代目**竹中組**位於[兵庫縣](../Page/兵庫縣.md "wikilink")[姫路市本部的設置](../Page/姫路市.md "wikilink")\[1\]，[日本的暴力團](../Page/日本.md "wikilink")（犯罪組織）之一・六代目[山口組的](../Page/山口組.md "wikilink")2次團體；曾在1989年退出山口組（本部遷至[岡山市](../Page/岡山市.md "wikilink")），成為一支獨立的團體。正式成員數在1980年約1100人、在1989年約2000人。2007年約280人。

## 略年表

  - 1960年8月[竹中正久在](../Page/竹中正久.md "wikilink")[兵庫縣](../Page/兵庫縣.md "wikilink")[姫路市一帶組成有地緣關係的愚連隊](../Page/姫路市.md "wikilink")。
  - 1961年12月 竹中正久 加入三代目山口組（組長・[田岡一雄授盃予](../Page/田岡一雄.md "wikilink")）。
  - 1971年竹中正久 昇任山口組若頭補佐。
  - 1972年旗下組員遭[白龍會](../Page/白龍會.md "wikilink")（姫路、會長・山田忠一）的幹部殺害棄屍。
  - 1974年9月[神戶](../Page/神戶.md "wikilink")[地方法院姫路支部判決竹中正久懲役](../Page/地方法院.md "wikilink")2年。
  - 1979年9月 竹中在[神戶刑務所服刑期滿出獄](../Page/神戶.md "wikilink")。
  - 1980年1月10日
    津山支部幹部（[杉本組組員](../Page/杉本組.md "wikilink")）在[岡山縣津山遭二代目](../Page/岡山縣.md "wikilink")[木下會系](../Page/木下會.md "wikilink")[平岡組組員槍殺的](../Page/平岡組.md "wikilink")「津山事件」（6日後、在山口組舍弟・湊
    芳治（[湊組組長](../Page/湊組.md "wikilink")）、[白龍會會長](../Page/白龍會.md "wikilink")・山田忠一、[大崎組組長](../Page/大崎組.md "wikilink")・大崎英良的居間仲裁下達成「和解」）。
  - 1980年5月13日 幹部在姫路槍殺[木下會會長](../Page/木下會.md "wikilink")・高山雅裕（姫路事件）。
  - 1981年3月6日 山口組舍弟・湊
    芳治（[湊組組長](../Page/湊組.md "wikilink")）、[白龍會會長](../Page/白龍會.md "wikilink")・山田忠一、[大崎組組長](../Page/大崎組.md "wikilink")・大崎英良的居間仲裁下與第三代[木下會會長](../Page/木下會.md "wikilink")・大崎圭二達成「和解」。
  - 1981年竹中正久 因賭博案遭到逮捕。
  - 1982年6月15日 竹中正久 昇格山口組若頭。
  - 1982年8月26日 竹中正久 涉嫌逃稅遭到逮捕、隔年6月 被拘留。
  - 1984年5月 竹中在山口組三代目的大家姐「文子」（二代目山口登的女兒、三代目田岡一雄的妻子）介入指名，當選為4代目山口組組長、6月5日
    就任四代目山口組組長。同時、原竹中組副組長・[竹中
    武](../Page/竹中武.md "wikilink")（現在為岡山竹中組組長）繼承組長、並昇格為山口組直參。
  - 1984年10月 舍弟・[須藤
    潤](../Page/須藤潤.md "wikilink")（[須藤組組長](../Page/須藤組.md "wikilink")）昇格為山口組直參。
  - 1984年11月
    舍弟・[佐藤邦彦](../Page/佐藤邦彦.md "wikilink")（[佐藤組組長](../Page/佐藤組.md "wikilink")）昇格為山口組直參。
  - 1985年1月26日
    竹中正久在[大阪吹田遭](../Page/大阪.md "wikilink")[一和會](../Page/一和會.md "wikilink")（會長・[山本
    廣](../Page/山本廣.md "wikilink")）旗下的暗殺部隊槍殺、27日
    在[大阪](../Page/大阪.md "wikilink")[警察病院逝世](../Page/警察.md "wikilink")，享年51歲。
  - 1985年10月27日
    旗下的[杉本組系組員在](../Page/杉本組.md "wikilink")[鳥取倉吉](../Page/鳥取.md "wikilink")
    槍殺[一和會佐々木組舍弟](../Page/一和會.md "wikilink")・赤坂
    進（[赤坂組組長](../Page/赤坂組.md "wikilink")）。
  - 1986年2月27日
    旗下[柴田會](../Page/柴田會.md "wikilink")（會長・[柴田健吾](../Page/柴田健吾.md "wikilink")）的組員在先代・竹中正久位於姫路的墓前槍殺[一和會](../Page/一和會.md "wikilink")[加茂田組二代目](../Page/加茂田組.md "wikilink")[花田組](../Page/花田組.md "wikilink")（組長・丹羽勝治）的組員。
  - 1986年5月21日
    旗下[生島組](../Page/生島組.md "wikilink")（組長・生島仁吉）的幹部槍殺[一和會副本部長](../Page/一和會.md "wikilink")・中川宜治（[中川連合會會長](../Page/中川連合會.md "wikilink")）。
  - 1988年5月14日 旗下的2名組員車載爆裂物襲擊[一和會會長](../Page/一和會.md "wikilink")・山本 廣的住宅。
  - 1989年2月 竹中 武就任山口組若頭補佐。
  - 1989年5月 進入五代目山口組（組長・[渡邊芳則](../Page/渡邊芳則.md "wikilink")）時期、同年6月5日
    竹中組宣布退出山口組，並將本部事務所遷移至岡山市。
  - 1989年8月31日 山口組高層幹部勸告竹中武 引退江湖並解散組織，竹中 武拒絕山口組的要求；事後遭山口組襲擊事務所，竹中組一度反擊。
  - 1989年若頭・[大西康雄](../Page/大西康雄.md "wikilink")（[大西組組長](../Page/大西組.md "wikilink")）退出竹中組，加入山口組[後藤組任副組長](../Page/後藤組.md "wikilink")。
  - 2008年3月 二代目竹中組組長・竹中武去世；17日，在岡山縣岡山市田町1丁目4-12的蓮昌寺舉行葬儀式。
  - 2015年
    原山口組二代目[柴田會組長](../Page/柴田會.md "wikilink")・安東美樹（竹中組出身）繼承竹中組二代目名跡\[2\]。

## 初代（兵庫姬路）

  - 組長・[竹中正久](../Page/竹中正久.md "wikilink")

### 最高幹部

  - 副組長・[竹中 武](../Page/竹中武.md "wikilink")
  - 副組長・[竹中 正](../Page/竹中正.md "wikilink")
  - 若頭・[杉本明政](../Page/杉本明政.md "wikilink")（[杉本組組長](../Page/杉本組.md "wikilink")）
  - 若頭補佐・[笹部靜男](../Page/笹部靜男.md "wikilink")（[笹部組組長](../Page/笹部組.md "wikilink")）
  - 若頭補佐・[松浦敏夫](../Page/松浦敏夫.md "wikilink")
  - 若頭補佐・[後藤明義](../Page/後藤明義.md "wikilink")
  - 若頭補佐・[吉村義則](../Page/吉村義則.md "wikilink")
  - 若頭補佐・[萩原公明](../Page/萩原公明.md "wikilink")
  - 若頭補佐・[平尾
    光](../Page/平尾光.md "wikilink")（[平尾組組長](../Page/平尾組.md "wikilink")）

### 其他組員

  - 舍弟・[安田一郎](../Page/安田一郎.md "wikilink")
  - 舍弟・[別所正昭](../Page/別所正昭.md "wikilink")
  - 舍弟・[坂本義一](../Page/坂本義一.md "wikilink")（[坂本會會長](../Page/坂本會.md "wikilink")）
  - 舍弟・[佐藤邦彦](../Page/佐藤邦彦.md "wikilink")（[佐藤組組長](../Page/佐藤組.md "wikilink")）
  - 舍弟・[須藤
    潤](../Page/須藤潤.md "wikilink")（[須藤組組長](../Page/須藤組.md "wikilink")）

## 二代目（岡山市）

  - 組長・[竹中 武](../Page/竹中武.md "wikilink")（原第四代山口組若頭補佐。第四代山口組組長・竹中正久的親弟）

### 最高幹部

  - 相談役・[竹中 正](../Page/竹中正.md "wikilink")（第四代山口組組長・竹中正久的親弟、竹中 武的親兄）
  - 副組長・[坂本義一](../Page/坂本義一.md "wikilink")（[坂本會會長](../Page/坂本會.md "wikilink")）；原：青木恵一郎（二代目西岡組組長）
  - 若頭・不明（[大野連合會會長](../Page/大野連合會.md "wikilink")）；原：大西康男（[大西組組長](../Page/大西組.md "wikilink")）
  - 舍弟頭・不明（[武龍會會長](../Page/武龍會.md "wikilink")）；原：坂本義一（[坂本會會長](../Page/坂本會.md "wikilink")）
  - 舍弟頭補佐・[杉本明政](../Page/杉本明政.md "wikilink")（[杉本組組長](../Page/杉本組.md "wikilink")、之後的第二代[宅見組代理組長](../Page/宅見組.md "wikilink")）
  - 若頭補佐・[生島仁吉](../Page/生島仁吉.md "wikilink")（[生島組組長](../Page/生島組.md "wikilink")）
  - 若頭補佐・[宮本鄉弘](../Page/宮本鄉弘.md "wikilink")（[宮本興業組長](../Page/宮本興業.md "wikilink")）

### 其他組員

  - 舍弟・[笹部靜男](../Page/笹部靜男.md "wikilink")（[笹部組組長](../Page/笹部組.md "wikilink")、原總本部長）
  - 舍弟・[林田誠一](../Page/林田誠一.md "wikilink")（[林田組組長](../Page/林田組.md "wikilink")）
  - [貝崎忠義](../Page/貝崎忠義.md "wikilink")（[貝崎組組長](../Page/貝崎組.md "wikilink")）
  - [安東美樹](../Page/安東美樹.md "wikilink")（[安東會會長](../Page/安東會.md "wikilink")）
  - [山本浩司](../Page/山本浩司.md "wikilink")（[山本組組長](../Page/山本組.md "wikilink")）

## （岡山市）

  - 組長・從缺
  - 組長代行・[竹中正](../Page/竹中正.md "wikilink")
  - 副組長・[坂本義一](../Page/坂本義一.md "wikilink")（[坂本會會長](../Page/坂本會.md "wikilink")）
  - 若頭・山下道夫

## 六代目山口組二代目竹中組（姫路市）

  - 組長・安東美樹
  - 若頭・善岡 仁（善岡興業組長）
  - 舍弟頭・金川隆次（三代目[柴田會會長](../Page/柴田會.md "wikilink"))
  - 統括長・小田 貢（二代目藤正會會長）
  - 本部長・渡邊 智（二代目青木組組長）

## 資料來源

[Category:中國地區暴力團](../Category/中國地區暴力團.md "wikilink")
[Category:山口組消失的組織](../Category/山口組消失的組織.md "wikilink")
[Category:姬路市](../Category/姬路市.md "wikilink")

1.  [山一抗争時の“ヒットマン”逮捕　銃６丁所持容疑](https://www.kobe-np.co.jp/news/shakai/201609/0009590453.shtml)
    神戶新聞 2016/9/29
2.  [竹中組“伝説の組長”逮捕で6代目山口組に四分五裂の危機](https://www.nikkan-gendai.com/articles/view/news/190866)
    日刊ゲンダイDIGITAL 2016年10月1日