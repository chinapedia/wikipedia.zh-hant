**關穎**（，）本名**陈品颖**，[臺灣](../Page/臺灣.md "wikilink")[女演員](../Page/女演員.md "wikilink")、[女歌手](../Page/女歌手.md "wikilink")、[模特兒](../Page/模特兒.md "wikilink")，[籍貫](../Page/籍貫.md "wikilink")[臺灣](../Page/臺灣.md "wikilink")[新竹](../Page/新竹.md "wikilink")，出生於[日本](../Page/日本.md "wikilink")[東京](../Page/東京.md "wikilink")，於5歲時回流臺灣分別在3所學校完成小學學程，後來留學[美國](../Page/美國.md "wikilink")，在大學主修[舞蹈](../Page/舞蹈.md "wikilink")，後來取得[紐約大學藝術行政研究所碩士](../Page/紐約大學.md "wikilink")。關穎以廣告演出崛起，參與了多部電視偶像劇的演出，電影作品以《[向左走向右走](../Page/向左走向右走.md "wikilink")》入圍第40屆[金馬獎最佳女配角](../Page/金馬獎.md "wikilink")。\[1\]\[2\]

關穎表示自己剛出道時被唱片公司簽約為歌手，惟始終未發片，出道8年才有機會發行唱片，所以戲稱首張專輯《[關不住的秘密](../Page/關不住的秘密.md "wikilink")》耗時8年籌備。\[3\]

## 家族背景

關穎的父親[陳國和為](../Page/陳國和.md "wikilink")[日盛金控榮譽董事長](../Page/日盛金控.md "wikilink")，母親[蔡淑媛為](../Page/蔡淑媛.md "wikilink")[國泰集團創辦人](../Page/國泰集團.md "wikilink")[蔡萬春之女](../Page/蔡萬春.md "wikilink")\[4\]。

## 作品

### 電視劇（台灣）

| 年份                                   | 頻道                                         | 劇名                                     | 飾演      |
| ------------------------------------ | ------------------------------------------ | -------------------------------------- | ------- |
| 2003年                                | [華視](../Page/華視.md "wikilink")             | 《[候鳥e人](../Page/候鳥e人.md "wikilink")》   | 李詠湘(湘子) |
| [八大綜合台](../Page/八大綜合台.md "wikilink") | 《[又見橘花香](../Page/又見橘花香.md "wikilink")》     | 駱心蕾                                    |         |
| 華視                                   | 《[吐司男之吻Ⅱ](../Page/吐司男之吻Ⅱ.md "wikilink")》   |                                        |         |
| [中視](../Page/中視.md "wikilink")       | 《[Hi 上班女郎](../Page/Hi_上班女郎.md "wikilink")》 | 蘇曉雲（14,15集）                            |         |
| 2004年                                | 華視、[三立都會台](../Page/三立都會台.md "wikilink")    | 《[天國的嫁衣](../Page/天國的嫁衣.md "wikilink")》 | Katrina |
| 2005年                                | [民視](../Page/民視.md "wikilink")             | 《[偷天換日](../Page/偷天換日.md "wikilink")》   | 余慕緹     |
| 2008年                                | 華視、[緯來綜合台](../Page/緯來綜合台.md "wikilink")    | 《[這裡發現愛](../Page/這裡發現愛.md "wikilink")》 | 陸怡      |
| 2009年                                | [公視](../Page/公視.md "wikilink")             | 《[心星的淚光](../Page/心星的淚光.md "wikilink")》 | 董小鹿     |
|                                      |                                            |                                        |         |

### 電視劇（中國大陸）

  - 2003年《[蒲公英](../Page/蒲公英_\(電視劇\).md "wikilink")》

### 電影

| 年份                                         | 片名                                                   | 飾演                                                                                                                                | 合作演員                                                              |
| ------------------------------------------ | ---------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------- |
| 2003年                                      | 《[小雨之歌](../Page/小雨之歌.md "wikilink")》                 |                                                                                                                                   |                                                                   |
| 《[給我一隻貓](../Page/給我一隻貓.md "wikilink")》     |                                                      |                                                                                                                                   |                                                                   |
| 2004年                                      | 《[向左走，向右走](../Page/向左走向右走_\(電影\).md "wikilink")》     | 小紅(送外賣的小妹)                                                                                                                        | [金城武](../Page/金城武.md "wikilink")、[梁咏琪](../Page/梁咏琪.md "wikilink") |
| 2005年                                      | 《[宅變](../Page/宅變.md "wikilink")》                     |                                                                                                                                   |                                                                   |
| 2007年                                      | 《[神選者](../Page/神選者.md "wikilink")》（未上映）              |                                                                                                                                   |                                                                   |
| 《[基因決定我愛你](../Page/基因決定我愛你.md "wikilink")》 |                                                      | [彭于晏](../Page/彭于晏.md "wikilink")、[余男](../Page/余男.md "wikilink")、[何润东](../Page/何润东.md "wikilink")、[林依轮](../Page/林依轮.md "wikilink") |                                                                   |
| 2008年                                      | 《[兇魅](../Page/兇魅.md "wikilink")》                     |                                                                                                                                   |                                                                   |
| 《[鈕扣人](../Page/鈕扣人.md "wikilink")》         |                                                      |                                                                                                                                   |                                                                   |
| 《[軍雞](../Page/軍雞_\(電影\).md "wikilink")》    | 萌美                                                   |                                                                                                                                   |                                                                   |
| 2009年                                      | 《[淚王子](../Page/淚王子.md "wikilink")》                   | 歐陽千君                                                                                                                              |                                                                   |
| 2010年                                      | 《[第四張畫](../Page/第四張畫.md "wikilink")》                 |                                                                                                                                   |                                                                   |
| 2011年                                      | 《[痞子英雄首部曲:全面開戰](../Page/痞子英雄首部曲:全面開戰.md "wikilink")》 | 杜小晴                                                                                                                               |                                                                   |
| 2013年                                      | 《[瑪德2號](../Page/瑪德2號.md "wikilink")》                 | 小鐵阿姨                                                                                                                              |                                                                   |
| 《[非常幸运](../Page/非常幸运.md "wikilink")》       | 黑寡婦                                                  |                                                                                                                                   |                                                                   |
| 2014年                                      | 《[痞子英雄2：黎明再起](../Page/痞子英雄2：黎明再起.md "wikilink")》     | 杜小晴                                                                                                                               |                                                                   |
|                                            |                                                      |                                                                                                                                   |                                                                   |

### 配音

| 年份    | 片名                                                | 飾演                        |
| ----- | ------------------------------------------------- | ------------------------- |
| 2004年 | [鯊魚黑幫中文版](../Page/鯊魚黑幫.md "wikilink")（Shark Tale） | 安琪(Angie)(中文)             |
| 2007年 | [蜂電影中文版](../Page/蜂電影.md "wikilink")(Bee Movie)    | 凡妮莎(Vanessa Bloom)(花店老闆娘) |
|       |                                                   |                           |

### 音樂專輯

| 專輯名稱                                           | 語言 | 發行公司                               | 發行日期       |
| :--------------------------------------------- | :- | :--------------------------------- | :--------- |
| 《[關不住的秘密](../Page/關不住的秘密.md "wikilink")》發行首張專輯 | 國語 | [金牌大風](../Page/金牌大風.md "wikilink") | 2010年7月23日 |

### 音樂電影

| 年份    | 演唱                               | 歌名           | 地區 |
| ----- | -------------------------------- | ------------ | -- |
| 2003年 | [羅志祥](../Page/羅志祥.md "wikilink") | 音樂愛情故事       | 臺灣 |
| 2004年 | [梁詠琪](../Page/梁詠琪.md "wikilink") | 「沉迷、歸屬感」音樂電影 |    |
|       |                                  |              |    |

### 電視原聲帶

| 年份    | 曲名    | 專輯名稱  |
| ----- | ----- | ----- |
| 2009年 | 星星的淚光 | 心星的淚光 |
|       |       |       |

### 著作

|       |       |    |        |                    |
| ----- | ----- | -- | ------ | ------------------ |
| 年份    | 書名    | 作者 | 出版社    | ISBN               |
| 2009年 | 《上流感》 | 關穎 | 平裝本出版社 | ISBN 9789578037410 |
|       |       |    |        |                    |

## 音樂MV

| 年份    | 演唱                                           | 曲名               | 地區 |
| ----- | -------------------------------------------- | ---------------- | -- |
| 1994年 | [郭富城](../Page/郭富城.md "wikilink")             | 說聲我愛你            | 臺灣 |
| 2002年 | [張學友](../Page/張學友.md "wikilink")             | 禮物               | 臺灣 |
| 2003年 | [羅志祥](../Page/羅志祥.md "wikilink")             | Show time 沒有你    | 臺灣 |
| 2003年 | [羅志祥](../Page/羅志祥.md "wikilink")             | Show time 這一秒我哭了 | 臺灣 |
| 2004年 | [元衛覺醒](../Page/元衛覺醒.md "wikilink")           | 牛仔               | 臺灣 |
| 2004年 | [梁詠琪](../Page/梁詠琪.md "wikilink")             | 沉迷               | 臺灣 |
| 2007年 | [黄立行](../Page/黄立行.md "wikilink")             | 打分数              | 臺灣 |
| 2008年 | [楊宗緯](../Page/楊宗緯.md "wikilink")             | 讓                | 臺灣 |
| 2012年 | [MC HotDog](../Page/MC_HotDog.md "wikilink") | 嗨嗨人生             | 臺灣 |
|       |                                              |                  |    |

[Terri-Kwan-OSIM.jpg](https://zh.wikipedia.org/wiki/File:Terri-Kwan-OSIM.jpg "fig:Terri-Kwan-OSIM.jpg")

## 廣告代言

  - 2009年 凡士林潤膚露
  - 2012年 瑪登瑪朵
  - 萊翠美
  - OSIM

## 獎項與提名

### 金馬獎

|- | align="1"|2003 |《[向左走，向右走](../Page/向左走，向右走.md "wikilink")》
|第40屆金馬獎最佳女配角 | |-

## 參考資料

## 外部連結

  -
  -
  -
  -
  -
  -
  -
[Category:台灣電视女演員](../Category/台灣電视女演員.md "wikilink")
[G關](../Category/台灣電影女演員.md "wikilink")
[G關](../Category/台灣廣告演員.md "wikilink")
[G關](../Category/陳姓.md "wikilink")
[G](../Category/紐約大學校友.md "wikilink")

1.  [第四十屆金馬獎入圍名單今晚揭曉](http://www.people.com.cn/BIG5/yule/1080/2161525.html)
2.  [電影工作者 關穎](http://www.taiwancinema.com/ct.asp?xItem=12547&ctNode=39)
3.  [8月封面人物 關穎
    忠於自己最性感](http://beauty.pchome.com.tw/magazine/report/sh/cittabella/2337/128162880049091048004.htm)2010-08-13
4.  [關穎 叛逆千金男人心
    激怒貴婦媽扔拖鞋](http://tw.nextmedia.com/applenews/article/art_id/32904719/IssueID/20101022)
    2010-10-22