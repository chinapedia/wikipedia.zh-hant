《**小飛俠**》（[英語](../Page/英語.md "wikilink")：****）是[迪士尼第](../Page/迪士尼.md "wikilink")14部[经典动画长片](../Page/经典动画长片.md "wikilink")，1953年2月5日上映。改編自[蘇格蘭作家](../Page/蘇格蘭.md "wikilink")[詹姆斯·馬修·巴利的小說](../Page/詹姆斯·馬修·巴利.md "wikilink")[彼得潘](../Page/彼得潘.md "wikilink")（Peter
Pan）。彼得潘是個永遠長不大的小孩，他和其他孩子們在神奇夢幻島（Neverland）上對抗[-{A及](../Page/鐵鈎船長_\(角色\).md "wikilink")[海盜](../Page/海盜.md "wikilink")。DVD由[博伟在台湾发行](../Page/博伟.md "wikilink")，VCD由[中录德加拉在中国大陆发行](../Page/中录德加拉.md "wikilink")。小飞侠的续集《[梦不落帝国](../Page/梦不落帝国.md "wikilink")》（Return
to Never Land）由[迪士尼在](../Page/迪士尼.md "wikilink")2002年推出。

## 配音員

<table>
<thead>
<tr class="header">
<th><p>配音</p></th>
<th><p>角色</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>英語</p></td>
<td><p>國語</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/李勇_(配音員).md" title="wikilink">李勇</a></p></td>
</tr>
<tr class="odd">
<td><p>無</p></td>
<td><p>小叮噹（Tinker Bell）</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/林芳雪.md" title="wikilink">林芳雪</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>湯米·魯斯克</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/漢斯·康里德.md" title="wikilink">漢斯·康里德</a></p></td>
<td><p><a href="../Page/徐健春.md" title="wikilink">徐健春</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/漢斯·康里德.md" title="wikilink">漢斯·康里德</a></p></td>
<td><p><a href="../Page/胡大衛.md" title="wikilink">胡大衛</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/李映淑.md" title="wikilink">李映淑</a></p></td>
</tr>
<tr class="even">
<td><p>無</p></td>
<td><p>娜娜（Nana）</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/胡立成.md" title="wikilink">胡立成</a></p></td>
</tr>
<tr class="even">
<td><p>無</p></td>
<td><p>鱷魚（The Crocodile）</p></td>
</tr>
<tr class="odd">
<td><p><br />
傑弗瑞·希伯<br />
強尼·馬克格文<br />
史達菲·辛格<br />
（唱）</p></td>
<td><p><a href="../Page/許淑嬪.md" title="wikilink">許淑嬪</a><br />
<a href="../Page/楊小黎.md" title="wikilink">楊小黎</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/瓊·福雷.md" title="wikilink">裘恩·弗瑞</a><br />
<br />
康妮·希爾頓<br />
凱倫·凱斯特</p></td>
<td><p><a href="../Page/李映淑.md" title="wikilink">李映淑</a><br />
<a href="../Page/林芳雪.md" title="wikilink">林芳雪</a><br />
<a href="../Page/許淑嬪.md" title="wikilink">許淑嬪</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/瓊·福雷.md" title="wikilink">裘恩·弗瑞</a></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/姜先誠.md" title="wikilink">姜先誠</a><br />
<a href="../Page/李勇_(配音員).md" title="wikilink">李勇</a><br />
<a href="../Page/沈光平.md" title="wikilink">沈光平</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/沈光平.md" title="wikilink">沈光平</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 另見

  - 《[鐵鈎船長](../Page/鐵鈎船長.md "wikilink")》

## 外部链接

  -
  -
  -
[Category:1953年電影](../Category/1953年電影.md "wikilink")
[Category:1953年動畫電影](../Category/1953年動畫電影.md "wikilink")
[Category:1950年代奇幻片](../Category/1950年代奇幻片.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:迪士尼經典動畫長片電影](../Category/迪士尼經典動畫長片電影.md "wikilink")
[Category:奇幻冒險電影](../Category/奇幻冒險電影.md "wikilink")
[Category:戲劇改編電影](../Category/戲劇改編電影.md "wikilink")
[Category:美國奇幻電影](../Category/美國奇幻電影.md "wikilink")
[Category:動畫奇幻電影](../Category/動畫奇幻電影.md "wikilink")
[Category:海盜電影](../Category/海盜電影.md "wikilink")
[Category:倫敦背景電影](../Category/倫敦背景電影.md "wikilink")
[Category:1900年代背景電影](../Category/1900年代背景電影.md "wikilink")
[Category:華特迪士尼影業電影](../Category/華特迪士尼影業電影.md "wikilink")
[Category:童話故事改編電影](../Category/童話故事改編電影.md "wikilink")
[Category:人鱼题材电影](../Category/人鱼题材电影.md "wikilink")