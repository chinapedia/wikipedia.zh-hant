**非編碼核糖核酸**（），縮寫**ncRNA**，是指各種不[轉譯成](../Page/轉譯.md "wikilink")[蛋白質的](../Page/蛋白質.md "wikilink")[RNA分子](../Page/RNA.md "wikilink")。過去也稱此類RNA為**小核糖核酸**（sRNA）。不過有些ncRNA分子其實相當大。其他較少使用的同義詞還有**非信使核糖核酸**（nmRNA）、**小非信使核糖核酸**（snmRNA）或**功能性核糖核酸**（fRNA）等。

[DNA序列中專門轉錄成非編碼RNA的部分稱為](../Page/DNA.md "wikilink")**RNA基因**或是**非編碼RNA基因**。非編碼RNA基因用來生產[tRNA](../Page/tRNA.md "wikilink")（轉移RNA）與[rRNA](../Page/rRNA.md "wikilink")（核糖體RNA），以及一些小RNA，如snoRNAs、microRNAs、siRNAs與piRNAs；較大的則有Xist、Evf、Air、CTN與PINK等。

基因組中可生產非編碼RNA的DNA比例目前仍未明瞭，最近的轉錄組及[微陣列研究顯示](../Page/DNA微陣列.md "wikilink")，在老鼠基因組中，可能有超過3萬個長ncRNA。[mRNA](../Page/mRNA.md "wikilink")（信使RNA）末端也含有一些非編碼區域（non-coding
regions；UTRs），雖然這些部分並非蛋白質編碼，但mRNA並不歸類於非編碼RNA。

## 外部連結

  - [The Rfam Database](http://www.sanger.ac.uk/Software/Rfam/)
  - [Non-coding RNA database](http://biobases.ibch.poznan.pl/ncRNA/)
  - [EumiR](https://web.archive.org/web/20070928004502/http://miracle.igib.res.in/eumir/)
    prediction of microRNA precursors based on Support Vector Machine.
  - [HairpinFetcher](https://web.archive.org/web/20070928004412/http://miracle.igib.res.in/hfinder/)
    Prediction and analysis of hairpin forming potential in nucleic
    acids.
  - [RNA@IGIB](https://web.archive.org/web/20070711101411/http://miracle.igib.res.in/)
    group at the Institute of Genomics and Integrative Biology working
    on functional noncoding RNA biology.
  - [Wikiomics/RNA](https://web.archive.org/web/20070707152120/http://wikiomics.org/wiki/List_of_articles#RNA)
    Provides links to a variety of ncRNA analysis tools for structure
    prediction, sequence alignment and homology search.
  - [miRacle](https://web.archive.org/web/20070711101355/http://miracle.igib.res.in/miracle/)
    Provides target search for microRNAs and other small non-coding RNAs
    based on an algorithm which incorporates RNA secondary structure.

[Category:RNA](../Category/RNA.md "wikilink")
[Category:分子遺傳學](../Category/分子遺傳學.md "wikilink")
[非編碼RNA](../Category/非編碼RNA.md "wikilink")