[Heckert_GNU_white.svg](https://zh.wikipedia.org/wiki/File:Heckert_GNU_white.svg "fig:Heckert_GNU_white.svg")的頭像\]\]

**GNU计划**（），又譯為**革奴计划**，是一個[自由軟體](../Page/自由軟體.md "wikilink")[集體協作计划](../Page/集體協作.md "wikilink")，1983年9月27日由[理查德·斯托曼在](../Page/理查德·斯托曼.md "wikilink")[麻省理工學院公开发起](../Page/麻省理工學院.md "wikilink")。它的目標是创建一套完全[自由的](../Page/自由软件.md "wikilink")[操作系统](../Page/操作系统.md "wikilink")，稱為[GNU](../Page/GNU.md "wikilink")。理查德·斯托曼最早在net.unix-wizards[新闻组上公布该消息](../Page/新闻组.md "wikilink")，并附带一份《[GNU宣言](../Page/GNU宣言.md "wikilink")》等解释为何发起该计划的文章，其中一个理由就是要“重现当年软件界合作互助的团结精神”。

GNU是“**G**NU is **N**ot
**U**nix”的[递归缩写](../Page/递归缩写.md "wikilink")。為避免与单词gnu（[非洲](../Page/非洲.md "wikilink")[牛羚](../Page/牛羚.md "wikilink")，发音与「new」相同）混淆，斯托曼宣布GNU发音应为「Guh-NOO」（），与「canoe」发音相似。其中，[Emacs就是由这个计划孵化而出](../Page/Emacs.md "wikilink")。

[UNIX是一种广泛使用的商业操作系统的名称](../Page/UNIX.md "wikilink")。由于GNU将要实现UNIX系统的接口标准，因此GNU计划可以分别开发不同的操作系统。GNU计划采用了部分当时已经可自由使用的软件，例如[TeX排版系统和](../Page/TeX.md "wikilink")[X
Window视窗系统等](../Page/X_Window.md "wikilink")。不过GNU计划也开发了大批其他的自由软件，这些软件也被移植到其他操作系统平台上，例如[Microsoft
Windows](../Page/Microsoft_Windows.md "wikilink")、[BSD家族](../Page/BSD.md "wikilink")、[Solaris及](../Page/Solaris.md "wikilink")[Mac
OS](../Page/Mac_OS.md "wikilink")。

为保证GNU软件可以自由地“使用、复制、修改和发布”，所有GNU软件都包含一份在禁止其他人添加任何限制的情况下，授权所有权利给任何人的协议条款，[GNU通用公共许可证](../Page/GNU通用公共许可证.md "wikilink")（GNU
General Public
License，GPL）。这个就是被称为“[公共版權](../Page/Copyleft.md "wikilink")”的概念。GNU也针对不同场合，提供[GNU宽通用公共许可证与](../Page/GNU宽通用公共许可证.md "wikilink")[GNU自由文档许可证这两种协议条款](../Page/GNU自由文档许可证.md "wikilink")。

## 源起

1985年，[理查德·斯托曼创立了](../Page/理查德·斯托曼.md "wikilink")[自由软件基金会来为GNU计划提供](../Page/自由软件基金会.md "wikilink")[技术](../Page/技术.md "wikilink")、[法律以及](../Page/法律.md "wikilink")[财政支持](../Page/财政.md "wikilink")。尽管GNU计划大部分时候是由个人自愿无偿贡献，但[FSF有时还是会聘请](../Page/FSF.md "wikilink")[程序员帮助编写](../Page/程序员.md "wikilink")。当GNU计划开始逐渐获得成功时，一些商业公司开始介入开发和技术-{zh-hans:支持;
zh-hant:支援;}-。当中最著名的就是之后被[Red
Hat兼并的](../Page/Red_Hat.md "wikilink")[Cygnus
Solutions](../Page/Cygnus_Solutions.md "wikilink")。

到了1990年，GNU计划已经开发出的软件包括了一个功能强大的文字编辑器[Emacs](../Page/Emacs.md "wikilink")、[C语言编译器](../Page/C语言.md "wikilink")[GCC以及大部分UNIX系统的程序库和工具](../Page/GCC.md "wikilink")。唯一依然没有完成的重要组件，就是操作系统的[内核](../Page/内核.md "wikilink")（称为[HURD](../Page/HURD.md "wikilink")）。

1991年，[林納斯·托瓦茲编写出了与UNIX兼容的](../Page/林納斯·托瓦茲.md "wikilink")[Linux操作系统内核並在](../Page/Linux内核.md "wikilink")[GPL下发布](../Page/GPL.md "wikilink")。Linux之后在网上广泛流传。1992年，Linux与其他GNU软件结合，完全自由的操作系统正式诞生。许多程序员参与了Linux的开发与修改，也經常將Linux當成開發GNU計劃軟體的平台。该操作系统往往被称为“[GNU/Linux](../Page/GNU/Linux.md "wikilink")”或简称[Linux](../Page/Linux.md "wikilink")。但Linux本身不屬於GNU計劃的一部份，GNU计划自己的内核[Hurd依然在开发中](../Page/Hurd内核.md "wikilink")，目前已经发布[Beta版本](../Page/Beta版本.md "wikilink")。

许多UNIX系统上也安装了GNU软件，因为GNU软件的质量比之前UNIX的软件还要好。GNU工具还被广泛地移植到[Windows和](../Page/Windows.md "wikilink")[Mac
OS上](../Page/Mac_OS.md "wikilink")。

GNU工程十几年以来已经成为一个对软件开发主要的影响力量，创造了无数的重要的工具，例如：强健的[编译器](../Page/编译器.md "wikilink")，有力的文本编辑器，甚至一个全功能的操作系统。这个工程是从1984年[麻省理工学院的程序员理查德](../Page/麻省理工学院.md "wikilink")·斯托曼的想法得来的，他想要建立一个自由的、和UNIX类似的操作环境。从那时开始，许多程序员聚集起来开始开发一个自由的、高质量、易理解的软件。

## 宣言

## 发言人

以下是GNU计划的正式发言人：

  - [Robert J. Chassell](../Page/Robert_J._Chassell.md "wikilink")

  - [Loïc Dachary](../Page/Loïc_Dachary.md "wikilink")

  -
  - [Georg Greve](../Page/Georg_Greve.md "wikilink")

  - [Federico Heinz](../Page/Federico_Heinz.md "wikilink")

  - [Bradley Kuhn](../Page/Bradley_Kuhn.md "wikilink")

  - [Eben Moglen](../Page/Eben_Moglen.md "wikilink")

  - [理查德·斯托曼](../Page/理查德·斯托曼.md "wikilink")

  - [David Sugar](../Page/David_Sugar.md "wikilink")

## 参考文献

## 外部链接

  -
  - [斯托曼1983年宣告GNU计划开始的宣言](https://groups.google.com/groups?hl=en&selm=771%40mit-eddie.UUCP)

  - [GNU哲學的網頁](https://web.archive.org/web/20080202012825/http://www.gnu.org/philosophy/philosophy.zh.html)

  - [GNU宣言](https://gnu.org/gnu/manifesto.html)

  - [GNU專案的細節記錄](https://gnu.org/gnu/the-gnu-project.html)

  - [GNU專案的簡要歷史](https://gnu.org/gnu/gnu-history.html)

  - [原碼3路_3/5 :用革命存活社群-GNU
    (1/2)](https://www.youtube.com/watch?v=ma5IJNROU3g)

## 参见

  - [GNU/Linux](../Page/GNU/Linux.md "wikilink")
  - [GNU通用公共许可证](../Page/GNU通用公共许可证.md "wikilink")
  - [理查德·斯托曼](../Page/理查德·斯托曼.md "wikilink")
  - [自由软件基金会](../Page/自由软件基金会.md "wikilink")
  - [自由软件](../Page/自由软件.md "wikilink")
  - [自由软件运动](../Page/自由软件运动.md "wikilink")

{{-}}

[GNU計畫](../Category/GNU計畫.md "wikilink")
[Category:Unix](../Category/Unix.md "wikilink")