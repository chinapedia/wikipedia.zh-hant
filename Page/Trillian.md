**Trillian**是一個由[美國](../Page/美國.md "wikilink")[軟件公司](../Page/軟件.md "wikilink")[Cerulean
Studios所創造](../Page/Cerulean_Studios.md "wikilink")[即時通訊軟件](../Page/即時通訊軟件.md "wikilink")。它兼容[ICQ](../Page/ICQ.md "wikilink")、[MSN
Messenger](../Page/MSN_Messenger.md "wikilink")、[雅虎通](../Page/雅虎通.md "wikilink")、[IRC](../Page/IRC.md "wikilink")、[AOL
Instant
Messenger等各大即時通訊軟件](../Page/AOL_Instant_Messenger.md "wikilink")，使得用戶可以只使用一個軟件同時連接不同即時通訊服務的聯繫人。其商業版的插件更兼容許多其他的即時通訊服務。

2000年7月1日，最初公佈時的Trillian是一個免費的IRC使用者端。其第一個商業版本（Trillian Pro
1.0）在2002年9月10日公佈銷售。現時有兩種版本，Basic基本版和Pro商業版，分別以免費和收費方式發放。收費版有更多功能。2006年11月21日，Cerulean
Studios宣布即將發放Trillian Astra（4.0），目前正在Alpha測試階段。

Cerulean Studios總部設在美國[康乃狄克州](../Page/康乃狄克州.md "wikilink")，由[Kevin
Kurtz和](../Page/Kevin_Kurtz.md "wikilink")[Scott
Werndorfer以其](../Page/Scott_Werndorfer.md "wikilink")1萬美元積蓄於1998年5月創立。\[1\]其後，他們聘請了[華人](../Page/華人.md "wikilink")[設計師](../Page/設計師.md "wikilink")[麥珀騹擔任軟件的設計](../Page/麥珀騹.md "wikilink")。

## 版本歷史

  - July 1、2000: Version 0.50
  - August 11、2000: Version 0.52
  - November 29、2000: Version 0.60
  - December 23、2000: Version 0.61
  - January 28、2001: Version 0.62
  - March 23、2001: Version 0.63
  - June 04、2001: Version 0.635
  - June 20、2001: Version 0.6351
  - December 05、2001: Version 0.70
  - December 18、2001: Version 0.71
  - February 20、2002: Version 0.72
  - June 7、2002: Version 0.73
  - September 9、2002: Version 0.74 and Pro Version 1.0
  - September 9、2003: Pro Version 2.0
  - December 17、2004: Basic and Pro Version 3.0
  - February 24、2005: Basic and Pro Version 3.1
  - January 5、2007: Astra Alpha Build 24 and for Web Alpha 1\<
  - August 3、2007: Astra for iPhone Alpha and for Web Alpha 2
  - October 5、2007: Astra Mac Alpha Build 1
  - October 31、2008: Astra entered Beta testing
  - August 14、2009: Trillian Basic and Pro Version 4.0
  - October 1、2009: Trillian Astra 4.1 Build 11 Early Access For Pro
    users
  - October 9、2009: Trillian Astra 4.1 Build 12
  - October 16、2009: Trillian Astra 4.1 Build 13

## 參考文獻

## 外部連結

  - [Trillian官方網站](http://www.trillian.im/)

[Category:Windows即时通讯客户端](../Category/Windows即时通讯客户端.md "wikilink")
[Category:免費軟件](../Category/免費軟件.md "wikilink")
[Category:付費軟件](../Category/付費軟件.md "wikilink")
[Category:IOS软件](../Category/IOS软件.md "wikilink")
[Category:Android软件](../Category/Android软件.md "wikilink")

1.  <http://inventors.about.com/od/tstartinventions/a/trillian.htm>