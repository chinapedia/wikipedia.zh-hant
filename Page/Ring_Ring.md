<table>
<thead>
<tr class="header">
<th><p>align="center" bgcolor="orange" style="color:black"; colspan="3"|<em>Ring Ring</em></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/ABBA.md" title="wikilink">ABBA</a> 的 <a href="../Page/唱片.md" title="wikilink">唱片</a></p></td>
</tr>
<tr class="odd">
<td><p>发行时间</p></td>
</tr>
<tr class="even">
<td><p>录制时间</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/音乐流派.md" title="wikilink">流派</a></p></td>
</tr>
<tr class="even">
<td><p>时长</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/发行公司.md" title="wikilink">发行公司</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/制作人.md" title="wikilink">制作人</a></p></td>
</tr>
<tr class="odd">
<td><p>乐评</p></td>
</tr>
<tr class="even">
<td><p><small><a href="../Page/All_Music_Guide.md" title="wikilink">AMG</a></small></p></td>
</tr>
<tr class="odd">
<td><p>ABBA年表</p></td>
</tr>
<tr class="even">
<td><p><em>Ring Ring</em><br />
（1973年）</p></td>
</tr>
</tbody>
</table>

***Ring Ring***
是[瑞典乐队](../Page/瑞典.md "wikilink")[ABBA的首张](../Page/ABBA.md "wikilink")[专辑](../Page/音樂專輯.md "wikilink")，于1973年发行（参看
[1973年音乐史](../Page/1973年音乐史.md "wikilink")）。该专辑直到1995年才在[美国发行](../Page/美国.md "wikilink")。再版的专辑封面（右图所示）使用了乐队名称**ABBA**，尽管当年的原版专辑发行的时候，乐队还没有使用**ABBA**这个名字。原版专辑笨拙的署名为“Björn
& Benny, Agnetha & Anni-Frid”。

## 歌曲列表

1.  "Ring Ring" ([Stig
    Anderson](../Page/Stig_Anderson.md "wikilink")，Andersson, Cody,
    Sedaka, Ulvaeus) - 3:03
2.  "Another Town, Another Train" (Andersson, Ulvaeus) - 3:10
3.  "Disillusion" (Faltskog, Ulvaeus) - 3:05
4.  "People Need Love" (Andersson, Ulvaeus) - 2:43
5.  "I Saw It in the Mirror" (Andersson, Ulvaeus) - 2:33
6.  "Nina Pretty Ballerina" (Andersson, Ulvaeus) - 2:52
7.  "Love Isn't Easy (But It Sure Is Hard Enough)" (Andersson, Ulvaeus)
    - 2:52
8.  "Me and Bobby and Bobby's Brother" (Andersson, Ulvaeus) - 2:49
9.  "He Is Your Brother" (Andersson, Ulvaeus) - 3:17
10. "She's My Kind of Girl" (Andersson, Ulvaeus) - 2:39
11. "I Am Just a Girl" (Andersson, Anderson, Ulvaeus) - 3:02
12. "Rock & Roll Band" (Andersson, Ulvaeus) - 3:07

## 创作人员

**ABBA**

  - [Benny Andersson](../Page/Benny_Andersson.md "wikilink") -
    [钢琴](../Page/钢琴.md "wikilink")，[键盘](../Page/键盘.md "wikilink")，[演唱](../Page/演唱.md "wikilink")，[电子琴](../Page/电子琴.md "wikilink")
  - [Agnetha Fältskog](../Page/Agnetha_Fältskog.md "wikilink") - 演唱
  - [Anni-Frid Lyngstad](../Page/Anni-Frid_Lyngstad.md "wikilink") - 演唱
  - [Björn Ulvaeus](../Page/Björn_Ulvaeus.md "wikilink") -
    [木吉他](../Page/木吉他.md "wikilink")，[吉他](../Page/吉他.md "wikilink")，演唱

**另外的人员**

  - Ola Brunkert - [鼓](../Page/鼓.md "wikilink")
  - Rutger Gunnarsson - [电贝司](../Page/电贝司.md "wikilink")
  - Roger Palm - 鼓
  - Janne Schaffer - acoustic guitar, [电吉他](../Page/电吉他.md "wikilink")
  - Mike Watson - 电贝司

## 制作人员

  - 制作人：Benny Andersson, Björn Ulvaeus
  - 摄影：Rolf Adlercreutz
  - 技师：Michael B. Tretow
  - 技师助理：Björn Almstedt, Lennart Karlsmyr, Rune Persson
  - 重新灌录：Jon Astley, Michael B. Tretow
  - 歌曲排列（String arrangements）: Sven-Olof Walldoff
  - 照片：Bengt H. Malmqvist

[Category:瑞典音樂專輯](../Category/瑞典音樂專輯.md "wikilink")
[Category:流行音樂專輯](../Category/流行音樂專輯.md "wikilink")
[Category:ABBA音樂專輯](../Category/ABBA音樂專輯.md "wikilink")
[Category:1973年音樂專輯](../Category/1973年音樂專輯.md "wikilink")