**HTC Touch Cruise**，原廠型號**HTC P3650，HTC P3651**以及2009年的新版**HTC
T424X**，是台灣[宏達電公司所推出的](../Page/宏達電.md "wikilink")[智慧型手機](../Page/智慧型手機.md "wikilink")，搭載微軟
[Windows Mobile](../Page/Windows_Mobile.md "wikilink")
6，首次採用鏡面設計，2008年1月於歐洲首度發表。時隔一年后推出新版HTC Touch
Cruise，增加了專為輔助[GPS的](../Page/GPS.md "wikilink")[HTC
Footprints足跡地理標記功能](../Page/HTC_Footprints.md "wikilink")。HTC
Polaris的客製版本有HTC P3650，HTC P3651，HTC Touch Cruise，HTC Touch Cruise
P3651，SFR＆HTC Touch Cruise，HTC Touch Find，Dopod P860，O2 Xda Orbit
2。HTC Iolite的客製版本有HTC T424X，HTC Touch Cruise，Dopod Touch Cruise，O2 Xda
Guide等。

## HTC Polaris技術規格

  - [處理器](../Page/處理器.md "wikilink")：Qualcomm MSM7200 400 MHz
  - [作業系統](../Page/作業系統.md "wikilink")：Windows Mobile 6.0 Professional
  - [記憶體](../Page/記憶體.md "wikilink")：ROM：256MB，RAM：128MB
  - 尺寸：110mm X 58mm X 15.5mm
  - 重量：130g（含電池）
  - [螢幕](../Page/螢幕.md "wikilink")：QVGA 解析度、2.8 吋 TFT-LCD 平面式觸控感應螢幕
  - [網路](../Page/網路.md "wikilink")：HSDPA/WCDMA
  - [GPS](../Page/GPS.md "wikilink")：配備GPS及A-GPS
  - [藍牙](../Page/藍牙.md "wikilink")：2.0 with EDR
  - [Wi-Fi](../Page/Wi-Fi.md "wikilink")：IEEE 802.11 b/g
  - [相機](../Page/相機.md "wikilink")：300萬畫素相機，支援自動對焦功能
  - [電池](../Page/電池.md "wikilink")：1350mAh充電式鋰或鋰聚合物電池

## HTC Iolite技術規格

  - [處理器](../Page/處理器.md "wikilink")：Qualcomm MSM7225 528 MHz
  - [作業系統](../Page/作業系統.md "wikilink")：Windows Mobile 6.1 Professional
  - [記憶體](../Page/記憶體.md "wikilink")：ROM：512MB，RAM：256MB
  - 尺寸：102mm X 53.5mm X 14.5mm
  - 重量：103g（含電池）
  - [螢幕](../Page/螢幕.md "wikilink")：QVGA 解析度、2.8 吋 TFT-LCD 平面式觸控感應螢幕
  - [網路](../Page/網路.md "wikilink")：HSDPA/WCDMA
  - [GPS](../Page/GPS.md "wikilink")：配備GPS及A-GPS
  - [藍牙](../Page/藍牙.md "wikilink")：2.0 with EDR
  - [Wi-Fi](../Page/Wi-Fi.md "wikilink")：IEEE 802.11 b/g
  - [相機](../Page/相機.md "wikilink")：320萬畫素相機，不支援自動對焦功能
  - [電池](../Page/電池.md "wikilink")：1100mAh充電式鋰或鋰聚合物電池

## 參見

  - [HTC](../Page/HTC.md "wikilink")
  - [Qtek](../Page/Qtek.md "wikilink")
  - [Dopod](../Page/Dopod.md "wikilink")
  - [TouchFLO](../Page/TouchFLO.md "wikilink")

## 外部連結

  - [HTC P3650，HTC P3651
    概觀](http://www.htc.com/tw/product.aspx?id=22772)
  - [HTC P3650，HTC P3651
    技術規格](http://www.htc.com/tw/product.aspx?id=22776)
  - [HTC T4242 概觀](http://www.htc.com/www/product.aspx?id=81882)
  - [HTC T4242 技術規格](http://www.htc.com/www/product.aspx?id=81888)

[H](../Category/智能手機.md "wikilink") [H](../Category/觸控手機.md "wikilink")
[Touch Cruise](../Category/宏達電手機.md "wikilink")