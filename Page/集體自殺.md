**集體自殺**指一群人為了同一目的而自殺或互相殺害，而這通常與真實或意識到的迫害有關。

## 例子

集體自殺有時具有[宗教背景](../Page/宗教.md "wikilink")。[自殺攻擊](../Page/自殺攻擊.md "wikilink")、[自殺性炸彈和](../Page/自殺性炸彈.md "wikilink")[神風特攻隊是](../Page/神風特攻隊.md "wikilink")[軍事或準軍事形式的自殺](../Page/軍事.md "wikilink")。戰敗的一組人有時情願訴諸於集體自殺多於被俘虜。自殺協議（suicide
pact）是一種不涉及宗教崇拜或[戰爭的集體自殺](../Page/戰爭.md "wikilink")。這種[自殺模式有時是由一小組受挫折的人](../Page/自殺.md "wikilink")，尤其是情人，計劃或實行。集體自殺也曾被用作為一種[政治](../Page/政治.md "wikilink")[抗議的形式](../Page/抗議.md "wikilink")。

## 著名的集體自殺

  - 約前3世紀末，齊王[田橫部下](../Page/田橫.md "wikilink")500人知道田橫死訊後，在[山東海島上自殺](../Page/田橫島.md "wikilink")。
  - 在公元前2世纪後期，[條頓人與其鄰邦](../Page/條頓人.md "wikilink")[辛布里人向南行軍](../Page/辛布里人.md "wikilink")，經[高盧攻打](../Page/高盧.md "wikilink")[羅馬](../Page/羅馬.md "wikilink")。侵略軍經過幾次勝利後，卻在公元前102年的Aquae
    Sextiae戰爭（Battle of Aquae Sextiae，位於今日
    Aix-en-Provence）中被[盖乌斯·马略所敗](../Page/盖乌斯·马略.md "wikilink")。他們的君主条顿伯德(Teutobod)被捉，而被俘的女人進行集體自殺，成為了[日耳曼英雄的故事](../Page/日耳曼.md "wikilink")：根據投降的條件，他們的300個已婚女人會被送給[羅馬人](../Page/羅馬.md "wikilink")。當[條頓的婦女聽到了這個規定](../Page/條頓.md "wikilink")，她們懇求被安排至[刻瑞斯](../Page/刻瑞斯.md "wikilink")[维纳斯神庙服務](../Page/维纳斯.md "wikilink")；當她們的請求被否決，她們殺死了自己的孩子及於當晚一同上吊。翌日朝早，她們被發現死在大家的臂彎中。
  - 在公元1世紀，960名[猶太人不想被](../Page/猶太人.md "wikilink")[羅馬帝國統治及奴役](../Page/羅馬帝國.md "wikilink")，於[马萨达進行集體自殺](../Page/马萨达.md "wikilink")。
  - 中世紀，在北印度一些城市，要塞被[穆斯林圍攻](../Page/穆斯林.md "wikilink")。根據 Jauhar
    規條，男人會抗戰到底，女人會自殺作為祭品以免被俘虜及強姦。
  - 在[奧斯曼帝國佔領](../Page/奧斯曼帝國.md "wikilink")[希臘及至](../Page/希臘.md "wikilink")[希臘獨立戰爭前](../Page/希臘獨立戰爭.md "wikilink")，來自
    Souli 的女人們由於被奧斯曼人壓迫，登上了 Zalongo 山並把她們的孩子拋下懸崖，接著自己一同跳崖，以免被捉。
  - 1930年[霧社事件](../Page/霧社事件.md "wikilink")。
  - 在1945年5月，約900個住在[德國Demmin的住民因害怕](../Page/德國.md "wikilink")[紅軍的到來而集體自殺](../Page/紅軍.md "wikilink")。
  - [日本幾世紀以來的自殺傳統一直為人所知](../Page/日本.md "wikilink")，由[切腹至](../Page/切腹.md "wikilink")[第二次世界大戰的](../Page/第二次世界大戰.md "wikilink")[神風特攻隊撞向](../Page/神風特攻隊.md "wikilink")[美軍戰艦](../Page/美軍.md "wikilink")。在同一場戰事中，數百名被圍的[日本軍寧願集體自殺也不向美軍投降](../Page/日本軍.md "wikilink")，例如1944年7月[塞班島戰役](../Page/塞班島戰役.md "wikilink")，日本軍下達[玉碎攻擊](../Page/玉碎.md "wikilink")，當地數百餘居民則選擇跳崖自殺。
  - 1949年的[太原五百完人事件](../Page/太原五百完人.md "wikilink")。
  - 1978年，在[傳教士及](../Page/傳教士.md "wikilink")[人民圣殿教教主](../Page/人民圣殿教.md "wikilink")[吉姆·琼斯的指示下](../Page/吉姆·琼斯.md "wikilink")，913人在[蓋亞那的](../Page/蓋亞那.md "wikilink")[瓊斯鎮集體自殺](../Page/瓊斯鎮.md "wikilink")。（但相信913人中大部分是被[謀殺及在違反本人的意願下注射](../Page/謀殺.md "wikilink")[毒藥](../Page/毒藥.md "wikilink")。）
  - 1997年，[天堂之門在](../Page/天堂之門.md "wikilink")[加州](../Page/加州.md "wikilink")[聖迭哥進行集體自殺](../Page/聖迭哥.md "wikilink")。他們相信外星人太空船藏在[海爾·博普彗星](../Page/海爾·博普彗星.md "wikilink")。為了到達[太空船而進行自殺](../Page/太空船.md "wikilink")。受害者自行服藥然後被其他同伴以塑膠袋綁頭、最終窒息而死。為期3日一連串的自殺。39人死亡，多為40餘歲及來自多重背景。

[Category:集体自杀](../Category/集体自杀.md "wikilink")
[Category:自殺](../Category/自殺.md "wikilink")