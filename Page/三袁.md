**三袁**（又稱**公安三袁**）指[明代晚期三位](../Page/明代.md "wikilink")[袁姓的](../Page/袁姓.md "wikilink")[散文家兄弟](../Page/散文家.md "wikilink")，他們分別是**[袁宗道](../Page/袁宗道.md "wikilink")**、**[袁宏道](../Page/袁宏道.md "wikilink")**、**[袁中道](../Page/袁中道.md "wikilink")**。由於三袁是[湖廣](../Page/湖廣.md "wikilink")[公安縣](../Page/公安縣.md "wikilink")（今[中國](../Page/中華人民共和國.md "wikilink")[湖北省](../Page/湖北省.md "wikilink")[荊州市](../Page/荊州市.md "wikilink")[公安縣](../Page/公安縣.md "wikilink")）人，其[文學流派世稱](../Page/中國文學流派.md "wikilink")「**[公安派](../Page/公安派.md "wikilink")**」或「**公安體**」。

## 背景

[明代自](../Page/明代.md "wikilink")[弘治到](../Page/弘治.md "wikilink")[萬曆中期](../Page/萬曆.md "wikilink")，「[前後七子](../Page/前後七子.md "wikilink")」相繼統治文壇達百年之久，他們提倡“文必[秦](../Page/秦朝.md "wikilink")[漢](../Page/漢朝.md "wikilink")”、“詩必[盛唐](../Page/盛唐.md "wikilink")”，主張摹擬剽竊。許多有遠見卓識的文人學者，都曾不滿“七子”派的文學主張和做法，但反對力度不足。

三袁均吸取了[李贄](../Page/李贄.md "wikilink")、[徐渭的見解](../Page/徐渭.md "wikilink")，反對後七子首領──[王（世貞）](../Page/王世貞.md "wikilink")、[李（攀龍）等人之擬古](../Page/李攀龍.md "wikilink")、復古，剽竊成風，主張文學重性靈、貴獨創，所作一清新清俊、情趣盎然，不受任何規條束縛，是為「性靈說」。他們另外亦重視了[小說](../Page/小說.md "wikilink")、[戲曲的文學價值](../Page/戲曲.md "wikilink")，認為有可取之處，這是前人所不認同的。

## 貢獻

### 袁宗道

[袁宗道](../Page/袁宗道.md "wikilink")（1560年─1600年），字**伯修**，[明朝](../Page/明朝.md "wikilink")[萬曆十四年](../Page/萬曆.md "wikilink")[進士](../Page/進士.md "wikilink")，任[翰林院編修](../Page/翰林院.md "wikilink")、春坊右庶子等職。他是反復古思維的發起人，他主張即使研究古文，也不可全部模仿，而是要「學其意，不必泥其字句也」。他除了具有「[公安派](../Page/公安派.md "wikilink")」相同的性靈學說外，也對復古模擬的文章加以批判。著有《[白蘇齋集](../Page/白蘇齋集.md "wikilink")》。

### 袁宏道

[袁宏道](../Page/袁宏道.md "wikilink")（1568年─1610年），字**中郎**，號**石公**，萬曆二十年進士，歷任[吳縣知縣](../Page/吳縣.md "wikilink")、[禮部主事](../Page/禮部.md "wikilink")、吏部驗封司主事、稽勳郎中等職、國子博士等職，世以為是三兄弟中成就最高者。少敏慧，善詩文，年十六為諸生，結社城南，自為社長。

他是[明代文學反對](../Page/明代.md "wikilink")[復古運動主將](../Page/復古運動.md "wikilink")，他既反對[前](../Page/前七子.md "wikilink")[後七子摹擬](../Page/後七子.md "wikilink")[秦](../Page/秦朝.md "wikilink")[漢](../Page/漢朝.md "wikilink")[古文](../Page/古文.md "wikilink")，亦反對[唐宋派](../Page/唐宋派.md "wikilink")[唐順之](../Page/唐順之.md "wikilink")、[歸有光摹擬](../Page/歸有光.md "wikilink")[唐](../Page/唐朝.md "wikilink")[宋古文](../Page/宋朝.md "wikilink")，認為[文章與](../Page/文章.md "wikilink")[時代有密切關係](../Page/時代.md "wikilink")。他說過：「世道既變，文亦因之。今之不必摹古者，亦勢也。」\[1\]他亦相信寫文要真，認為「古有古之時，今有今之時」（今人稱為文學進代論），說：「我面不能同君面，而況古人之面貌乎？」\[2\]著有《[袁中郎集](../Page/袁中郎集.md "wikilink")》。

### 袁中道

[袁中道](../Page/袁中道.md "wikilink")（1575年─1630年），字**小修**，萬曆四十四年進士，歷任[徽州教授](../Page/徽州.md "wikilink")、南京吏部郎中等職。他亦主張即使研究古文，也不可全部模仿，而是要有其變化。他不但發揚「[公安派](../Page/公安派.md "wikilink")」主力[袁宏道的反復古理論](../Page/袁宏道.md "wikilink")，也將其理論詳細補充論述。著有《[珂雪齋集](../Page/珂雪齋集.md "wikilink")》、《[游居柿录](../Page/游居柿录.md "wikilink")》。

## 評價

明代後期文壇沉寂，三袁勇於革新文學，糾正過往「[前後七子](../Page/前後七子.md "wikilink")」盲目摹古之風，為文壇帶來了生機，地位舉足輕重。

三袁是同胞兄弟，並能同時在各學術領域，如[哲學思想](../Page/哲學.md "wikilink")、[政治傾向](../Page/政治.md "wikilink")、[文學觀點](../Page/文學.md "wikilink")、創作風格，以至性情、氣質等方面，發揮相互影響。這是[中國文學史上絕無僅有的](../Page/中國文學.md "wikilink")，甚至可謂文壇奇蹟，因此有「一母三進士，南北兩天官」一說。

值得一提的是，三人對以後幾百年的歷史乃至近代的[五四文化運動都產生了積極的影響](../Page/五四文化運動.md "wikilink")。

袁氏兄弟不僅文名卓著，為官也清正廉潔，三人為官到頭，只落得兩袖清風。

## 故里

  - 三袁故里位於[中國](../Page/中華人民共和國.md "wikilink")[湖北省](../Page/湖北省.md "wikilink")[荊州市](../Page/荊州市.md "wikilink")[公安縣](../Page/公安縣.md "wikilink")[孟家溪鎮東](../Page/孟家溪鎮.md "wikilink")2公里的[桂花臺](../Page/桂花臺.md "wikilink")。
  - 桂花臺東南100米有一座[荷葉山](../Page/荷葉山.md "wikilink")，是宗道和中道的合葬[墓](../Page/墓.md "wikilink")，墓前300米處立有一塊高大[墓碑](../Page/墓碑.md "wikilink")。
  - 袁宏道另有一處故里，即現在的[柳浪遺址](../Page/柳浪遺址.md "wikilink")。

## 注釋

<div class="references-small">

<references />

</div>

## 參考

  - [中國歷代文人並稱](../Page/中國歷代文人並稱.md "wikilink")
  - [鄧中龍](../Page/鄧中龍.md "wikilink")《中國文學發展簡史·下卷》
  - 「古雅臺語人」網站

## 外部連結

  - [公安三袁的科考之路](http://www.bjkaoshi.com.cn/pdfimages/20051231141800.Pdf)（《北京考試報》）
  - [袁宏道](https://web.archive.org/web/20010423152808/http://staff.whsh.tc.edu.tw/~huanyin/anfa_yuan1.htm)（古雅臺語人，兼述三袁）

[Y袁](../Category/明朝作家.md "wikilink")
[Y袁](../Category/明朝诗人.md "wikilink")
[Y袁](../Category/公安人.md "wikilink")
[H宏](../Category/袁姓.md "wikilink")

1.  《袁中郎全集·與江進之尺牘》
2.  《袁中郎全集·與丘長孺尺牘》