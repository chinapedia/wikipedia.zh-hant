[P1080515-1.JPG](https://zh.wikipedia.org/wiki/File:P1080515-1.JPG "fig:P1080515-1.JPG")和[北京金融街金角位置的](../Page/北京金融街.md "wikilink")[复兴门的](../Page/复兴门_\(北京\).md "wikilink")[北京百盛购物中心](../Page/北京百盛购物中心.md "wikilink")\]\]
**百盛商業集团**，簡稱**百盛集团**，或者**百盛**，（），是[馬來西亞](../Page/馬來西亞.md "wikilink")[金狮集团旗下的一家連鎖型](../Page/金狮集团.md "wikilink")[百貨公司](../Page/百貨公司.md "wikilink")，1987年从[香港吉利市百货董事长林道良](../Page/吉利市.md "wikilink")（1925—2012）收购旗下英保良（Emporium）百货并改为现名。\[1\]\[2\]

截至2013年11月，百盛百貨在[馬來西亞](../Page/馬來西亞.md "wikilink")、[中國大陸](../Page/中國大陸.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[越南和](../Page/越南.md "wikilink")[緬甸共設有超過](../Page/緬甸.md "wikilink")132間分店，[柬埔寨分店預計在](../Page/柬埔寨.md "wikilink")2015年開幕。百盛也是[斯里蘭卡百貨巨頭](../Page/斯里蘭卡.md "wikilink")——的最大股東。\[3\]目前該集團分別在[吉隆坡](../Page/吉隆坡.md "wikilink")（）、[新加坡](../Page/新加坡.md "wikilink")（）和[香港](../Page/香港.md "wikilink")（）分別上市，也是[馬來西亞唯一一家在三地上市的百貨集團](../Page/馬來西亞.md "wikilink")。
\[4\]\[5\]

## 營運狀況

[Parkson_Pavilion_KL.jpg](https://zh.wikipedia.org/wiki/File:Parkson_Pavilion_KL.jpg "fig:Parkson_Pavilion_KL.jpg")的旗舰店（Parkson
Elite）\]\]

### 馬來西亞

1987年，百盛的第一家分店開設在[吉隆坡](../Page/吉隆坡.md "wikilink")[武吉免登](../Page/武吉免登.md "wikilink")（Bukit
Bintang）的[金河廣場](../Page/金河廣場.md "wikilink")（Sungei Wang
Plaza），接下來平均每年開設2間新分店，發展至今，目前在[馬來西亞共開設](../Page/馬來西亞.md "wikilink")48間分店，在[馬來西亞除](../Page/馬來西亞.md "wikilink")[登嘉樓和](../Page/登嘉樓.md "wikilink")[玻璃市都可找尋其踪跡](../Page/玻璃市.md "wikilink")\[6\]。2007年，在吉隆坡[柏威年廣場](../Page/柏威年廣場.md "wikilink")（Pavilion
KL）的旗艦店開幕後，百盛開始更改其定位，為主要分店進行改裝，展開二代店計劃，在其商標的英文名稱旁加上中文名，將位在主要[購物中心的門市改為旗艦店](../Page/購物中心.md "wikilink")，大量引進國際精品品牌進駐，擺脫以往平民化及經常舉辦大促銷的形象。

除了[百貨公司外](../Page/百貨公司.md "wikilink")，目前百盛也積極在[馬來西亞各城市建設](../Page/馬來西亞.md "wikilink")[購物中心](../Page/購物中心.md "wikilink")，以增加多元收入和佔據當地百貨的主導位置，第一間開設的購物中心為位在[吉隆坡市郊文良港的吉隆坡嘉年华城](../Page/吉隆坡.md "wikilink")（现百乐广场）\[7\]。

百盛在[馬來西亞各州的分佈](../Page/馬來西亞.md "wikilink")：\[8\]\[9\]

<table>
<thead>
<tr class="header">
<th><p>旗帜</p></th>
<th><p>州属</p></th>
<th><p>現有分店</p></th>
<th><p>2016年州属人口<ref>{{cite web|url=<a href="https://www.dosm.gov.my/v1/index.php?r=column/pdfPrev&amp;id=OWlxdEVoYlJCS0hUZzJyRUcvZEYxZz09%7Ctitle=PRESS">https://www.dosm.gov.my/v1/index.php?r=column/pdfPrev&amp;id=OWlxdEVoYlJCS0hUZzJyRUcvZEYxZz09|title=PRESS</a> RELEASE CURRENT POPULATION ESTIMATES, MALAYSIA, 2014-2016</p></th>
<th><p>website=DEPARTMENT OF STATISTICS MALAYSIA|date=2016年7月22日|accessdate=2018年9月20日}}</ref></p></th>
<th><p>店均州民（万人）</p></th>
<th><p><a href="../Page/马来西亚各州国内生产总值列表.md" title="wikilink">2017年人均收入</a>（令吉）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Selangor.svg" title="fig:Flag_of_Selangor.svg">Flag_of_Selangor.svg</a></p></td>
<td><p><a href="../Page/雪蘭莪.md" title="wikilink">雪蘭莪</a>（12）</p></td>
<td><ul>
<li><a href="../Page/莎亞南.md" title="wikilink">莎亞南</a>：实达城市广场</li>
<li><a href="../Page/八打靈再也.md" title="wikilink">八打靈再也</a>：<a href="../Page/万达广场_(雪兰莪).md" title="wikilink">万达广场</a></li>
<li><a href="../Page/八打靈縣.md" title="wikilink">八打靈縣</a>：、梳邦百利、大門（DA MEN Mall）、M广场（M Square Mall）、IOI城市商场</li>
<li><a href="../Page/乌鲁冷岳县.md" title="wikilink">乌鲁冷岳县</a>：美景广场（Plaza Metro）、EVO广场</li>
<li><a href="../Page/鹅唛县.md" title="wikilink">鹅唛县</a>：士拉央广场（Selayang Mall）、Etonic大厦</li>
<li><a href="../Page/巴生縣.md" title="wikilink">巴生縣</a>：巴生百利</li>
</ul></td>
<td><p>613万</p></td>
<td><p>51</p></td>
<td><p>48,091</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Kuala_Lumpur_Malaysia.svg" title="fig:Flag_of_Kuala_Lumpur_Malaysia.svg">Flag_of_Kuala_Lumpur_Malaysia.svg</a></p></td>
<td><p><a href="../Page/吉隆坡.md" title="wikilink">吉隆坡</a><a href="../Page/聯邦直轄區.md" title="wikilink">聯邦直轄區</a>（8）</p></td>
<td><ul>
<li><a href="../Page/武吉免登.md" title="wikilink">武吉免登</a>：<a href="../Page/柏威年廣場.md" title="wikilink">柏威年廣場</a>（Pavilion KL）、<a href="../Page/华氏88广场.md" title="wikilink">华氏88广场</a>（鞋子專賣店）</li>
<li><a href="../Page/吉隆坡城中城.md" title="wikilink">吉隆坡城中城</a>：<a href="../Page/Avenue_K商場.md" title="wikilink">Avenue K商場</a>（鞋子專賣店）</li>
<li><a href="../Page/蕉赖.md" title="wikilink">蕉赖</a>：、MyTown购物中心</li>
<li><a href="../Page/文良港.md" title="wikilink">文良港</a>：百乐广场（Setapak Central）</li>
<li><p>：中环综合广场（NU Sentral）</p></li>
<li><p>：华联广场（OUG Plaza）</p></li>
</ul></td>
<td><p>178万</p></td>
<td><p>22</p></td>
<td><p>111,321</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Sarawak.svg" title="fig:Flag_of_Sarawak.svg">Flag_of_Sarawak.svg</a></p></td>
<td><p><a href="../Page/砂拉越.md" title="wikilink">砂拉越</a>（7）</p></td>
<td><ul>
<li><a href="../Page/古晉.md" title="wikilink">古晉</a>：独立广场（Plaza Merdeka）、新欣（tHe Spring）、Vivacity霸级购物广场、Riverside Complex</li>
<li><a href="../Page/美里.md" title="wikilink">美里</a>：星城广场（Bintang Megamall）</li>
<li><a href="../Page/詩巫.md" title="wikilink">詩巫</a>：</li>
<li><a href="../Page/民都鲁.md" title="wikilink">民都鲁</a>：新欣广场（tHe Spring Bintulu）[10]</li>
</ul></td>
<td><p>276万</p></td>
<td><p>39</p></td>
<td><p>49,327</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Johor.svg" title="fig:Flag_of_Johor.svg">Flag_of_Johor.svg</a></p></td>
<td><p><a href="../Page/柔佛.md" title="wikilink">柔佛</a>（4）</p></td>
<td><ul>
<li><a href="../Page/新山.md" title="wikilink">新山</a>：、假日广场（Holiday Plaza）</li>
<li><a href="../Page/居銮.md" title="wikilink">居銮</a>：居銮百利</li>
<li><a href="../Page/峇株巴辖.md" title="wikilink">峇株巴辖</a>：Square One</li>
</ul></td>
<td><p>365万</p></td>
<td><p>91</p></td>
<td><p>34,362</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Penang_(Malaysia).svg" title="fig:Flag_of_Penang_(Malaysia).svg">Flag_of_Penang_(Malaysia).svg</a></p></td>
<td><p><a href="../Page/檳城.md" title="wikilink">檳城</a>（3）</p></td>
<td><ul>
<li><a href="../Page/喬治市.md" title="wikilink">喬治市</a>：<a href="../Page/合您廣場.md" title="wikilink">合您廣場</a>（Gurney Plaza）、</li>
<li><a href="../Page/北海_(马来西亚).md" title="wikilink">北海</a>：<a href="../Page/双威嘉年华购物中心.md" title="wikilink">双威嘉年华购物中心</a></li>
</ul></td>
<td><p>171万</p></td>
<td><p>57</p></td>
<td><p>49,873</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Putrajaya.svg" title="fig:Flag_of_Putrajaya.svg">Flag_of_Putrajaya.svg</a></p></td>
<td><p><a href="../Page/布城.md" title="wikilink">布城</a><a href="../Page/聯邦直轄區.md" title="wikilink">聯邦直轄區</a>（2）</p></td>
<td><p>阿拉曼大商场、IOI城市商场</p></td>
<td><p>10万</p></td>
<td><p>5</p></td>
<td><p>111,321（與吉隆坡合計）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Negeri_Sembilan.svg" title="fig:Flag_of_Negeri_Sembilan.svg">Flag_of_Negeri_Sembilan.svg</a></p></td>
<td><p><a href="../Page/森美蘭.md" title="wikilink">森美蘭</a>（2）</p></td>
<td><p><a href="../Page/芙蓉市.md" title="wikilink">芙蓉市</a>：Terminal 1、Seremban Prima</p></td>
<td><p>111万</p></td>
<td><p>56</p></td>
<td><p>41,615</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Pahang.svg" title="fig:Flag_of_Pahang.svg">Flag_of_Pahang.svg</a></p></td>
<td><p><a href="../Page/彭亨.md" title="wikilink">彭亨</a>（2）</p></td>
<td><p><a href="../Page/關丹.md" title="wikilink">關丹</a>：东海岸广场（East Coast Mall）、城市广场（City Mall）</p></td>
<td><p>162万</p></td>
<td><p>81</p></td>
<td><p>35,352</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Kedah.svg" title="fig:Flag_of_Kedah.svg">Flag_of_Kedah.svg</a></p></td>
<td><p><a href="../Page/吉打.md" title="wikilink">吉打</a>（2）</p></td>
<td><ul>
<li><a href="../Page/亚罗士打.md" title="wikilink">亚罗士打</a>：<a href="../Page/阿曼中环广场.md" title="wikilink">阿曼中环广场</a></li>
<li><a href="../Page/雙溪大年.md" title="wikilink">雙溪大年</a>：双溪大年百利（Petani Parade）</li>
</ul></td>
<td><p>212万</p></td>
<td><p>106</p></td>
<td><p>20,327</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Sabah.svg" title="fig:Flag_of_Sabah.svg">Flag_of_Sabah.svg</a></p></td>
<td><p><a href="../Page/沙巴.md" title="wikilink">沙巴</a>（2）</p></td>
<td><p><a href="../Page/亞庇.md" title="wikilink">亞庇</a>：<a href="../Page/1Borneo.md" title="wikilink">1Borneo</a>、</p></td>
<td><p>380万</p></td>
<td><p>190</p></td>
<td><p>23,979</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Labuan.svg" title="fig:Flag_of_Labuan.svg">Flag_of_Labuan.svg</a></p></td>
<td><p><a href="../Page/納閩.md" title="wikilink">納閩</a><a href="../Page/聯邦直轄區.md" title="wikilink">聯邦直轄區</a>（1）</p></td>
<td><p>納閩金融中心廣場（Financial Park Labuan Complex）</p></td>
<td><p>10万</p></td>
<td><p>10</p></td>
<td><p>65,949</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Malacca.svg" title="fig:Flag_of_Malacca.svg">Flag_of_Malacca.svg</a></p></td>
<td><p><a href="../Page/馬六甲.md" title="wikilink">馬六甲</a>（1）</p></td>
<td><p><a href="../Page/馬六甲市.md" title="wikilink">馬六甲市</a>：皇冠百利（Mahkota Parade）</p></td>
<td><p>89万</p></td>
<td><p>89</p></td>
<td><p>46,015</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Kelantan.svg" title="fig:Flag_of_Kelantan.svg">Flag_of_Kelantan.svg</a></p></td>
<td><p><a href="../Page/吉蘭丹.md" title="wikilink">吉蘭丹</a>（1）</p></td>
<td><p><a href="../Page/哥打峇魯.md" title="wikilink">哥打峇魯</a>：Kota Bharu Trade Centre</p></td>
<td><p>181万</p></td>
<td><p>181</p></td>
<td><p>13,593</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Perak.svg" title="fig:Flag_of_Perak.svg">Flag_of_Perak.svg</a></p></td>
<td><p><a href="../Page/霹靂州.md" title="wikilink">霹雳</a>（1）</p></td>
<td><p><a href="../Page/怡保.md" title="wikilink">怡保</a>：怡保百利</p></td>
<td><p>247万</p></td>
<td><p>247</p></td>
<td><p>29,226</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Perlis.svg" title="fig:Flag_of_Perlis.svg">Flag_of_Perlis.svg</a></p></td>
<td><p><a href="../Page/玻璃市.md" title="wikilink">玻璃市</a>（0）</p></td>
<td><p>未有</p></td>
<td><p>25万</p></td>
<td><p>/</p></td>
<td><p>23,372</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Terengganu.svg" title="fig:Flag_of_Terengganu.svg">Flag_of_Terengganu.svg</a></p></td>
<td><p><a href="../Page/丁加奴.md" title="wikilink">丁加奴</a>（0）</p></td>
<td><p>117万</p></td>
<td><p>29,347</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>（48）</strong></p></td>
<td><p>3,170万</p></td>
<td><p>66（全国平均）</p></td>
<td><p>42,228</p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

#### 未来计划

| 旗帜                                                                                                                                                    | 州属                                                                      | 将有分店                                                                                                                                                                                                                                                                                                                                                                                   |
| ----------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [Flag_of_Selangor.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Selangor.svg "fig:Flag_of_Selangor.svg")                                          | [雪兰莪](../Page/雪兰莪.md "wikilink")（2）                                     | [白沙罗](../Page/白沙罗_\(雪兰莪\).md "wikilink")<ref>{{cite web|url=[https://www.thestar.com.my/business/business-news/2018/02/02/parksons-exit-a-sign-of-tough-times-for-retail-stores/|title=Parkson’s](https://www.thestar.com.my/business/business-news/2018/02/02/parksons-exit-a-sign-of-tough-times-for-retail-stores/%7Ctitle=Parkson’s) exit a sign of tough times for retail stores? |
| 巴生\[11\]                                                                                                                                              |                                                                         |                                                                                                                                                                                                                                                                                                                                                                                        |
| [Flag_of_Kuala_Lumpur_Malaysia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Kuala_Lumpur_Malaysia.svg "fig:Flag_of_Kuala_Lumpur_Malaysia.svg") | [吉隆坡](../Page/吉隆坡.md "wikilink")[聯邦直轄區](../Page/聯邦直轄區.md "wikilink")（1） | [武吉加里尔](../Page/武吉加里尔.md "wikilink")\[12\]                                                                                                                                                                                                                                                                                                                                             |
| [Flag_of_Malacca.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Malacca.svg "fig:Flag_of_Malacca.svg")                                             | [馬六甲](../Page/馬六甲.md "wikilink")（1）                                     | 马六甲\[13\]                                                                                                                                                                                                                                                                                                                                                                              |

#### 已经关闭

| 旗帜                                                                                                                                                    | 州属                                                                      | 关闭分店                                           | 关闭日期        |
| ----------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------- | ---------------------------------------------- | ----------- |
| [Flag_of_Kuala_Lumpur_Malaysia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Kuala_Lumpur_Malaysia.svg "fig:Flag_of_Kuala_Lumpur_Malaysia.svg") | [吉隆坡](../Page/吉隆坡.md "wikilink")[聯邦直轄區](../Page/聯邦直轄區.md "wikilink")（3） | [阳光广场](../Page/阳光广场.md "wikilink")（Suria KLCC） | 2019年\[14\] |
| [金河广场](../Page/金河广场.md "wikilink")                                                                                                                    | 2018年\[15\]                                                             |                                                |             |
| Maju Junction Mall                                                                                                                                    |                                                                         |                                                |             |
| [Flag_of_Penang_(Malaysia).svg](https://zh.wikipedia.org/wiki/File:Flag_of_Penang_\(Malaysia\).svg "fig:Flag_of_Penang_(Malaysia).svg")            | [檳城](../Page/檳城.md "wikilink")（2）                                       | [槟岛](../Page/槟岛.md "wikilink")                 | 2016年\[16\] |
| [大山脚柏达镇购物广场](../Page/大山脚.md "wikilink")（Perda City Mall）                                                                                              |                                                                         |                                                |             |
| [Flag_of_Malacca.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Malacca.svg "fig:Flag_of_Malacca.svg")                                             | [馬六甲](../Page/馬六甲.md "wikilink")（1）                                     | 马六甲广场（Melaka Mall）                             | 2017年\[17\] |

### 中國大陸

1994年，百盛開始進入[中國市場](../Page/中國.md "wikilink")，是第一批進駐當地的外資[百貨公司](../Page/百貨公司.md "wikilink")，當時金狮百科集团与[中国工艺美术（集团）公司合资开设](../Page/中国工艺美术（集团）公司.md "wikilink")[北京百盛购物中心](../Page/北京百盛购物中心.md "wikilink")（公司名为百盛商业发展有限公司），第一家店的店址设于原[中国工艺美术馆](../Page/中国工艺美术馆.md "wikilink")，占据[长安街与二环的金角](../Page/长安街.md "wikilink")——复兴门。2014年11月百盛在[中国的](../Page/中国.md "wikilink")[北京](../Page/北京.md "wikilink")、[重庆](../Page/重庆.md "wikilink")、[青岛](../Page/青岛.md "wikilink")、[太原](../Page/太原.md "wikilink")、[上海](../Page/上海.md "wikilink")、[大连](../Page/大连.md "wikilink")、[天津等](../Page/天津.md "wikilink")37个城市合资或独资开设了57家以百盛命名的购物中心以及两家以爱客家命名的购物中心，截至2017年12月31日在30個城鎮剩下48間分店\[18\]
。

但隨著百貨市場的激烈經濟及網絡購物的興起，百盛百貨在[中國的營收開始減少](../Page/中國.md "wikilink")，部分門市也陸續倒閉。從2012年至2016年，共有14間門市關閉\[19\]，其中2012年2间，即上海虹橋店及貴陽金鳳凰店；2013年2间，即貴陽鮮花店及石家莊店；2014年3间，即濟南店、常州新北店及北京東四環店；\[20\]2015年为天津店\[21\]。2016年关闭6间，即重庆的两间店（大坪店及万象城店）、西安东大街店、南昌恒荗店、北京太阳宫店及合肥天鹅湖店。\[22\]

百盛於2017年关闭6间，即重庆财富中心店（3月）、郑州东太康路店（5月）\[23\]、合肥瑶海店（元一店，6月）\[24\]、北京长楹天街店（6月，至此百盛在中国内地的门店仅剩48家，在北京只剩下復興店\[25\]）、南昌绿地店（7月）\[26\]、大连长春路店（12月）\[27\]。2018年6月关闭重庆万州店\[28\]、12月底關閉西安西大街店\[29\]，据官网12月初概况44间店仍在运作\[30\]。2019年3月底關閉重慶南坪店，4月初於10天內關閉崑山店\[31\]。

外界揣測百盛近期在中國市場的不利，除了網購興起，大部分的百盛門市都是屬於面積小於3萬平方米，在大型[購物中心及](../Page/購物中心.md "wikilink")[百貨公司充斥的](../Page/百貨公司.md "wikilink")[中國](../Page/中國.md "wikilink")，中小型購物中心已不能滿足當地人的需求之餘；品牌過少及時尚度不高也是其致命傷\[32\]。百盛在中國快速的擴展，沒有做好市場調查盲目入駐各城市，也是導致分店倒閉的因素之一。\[33\]\[34\]

### 越南

百盛在2005年開始進駐[越南市場](../Page/越南.md "wikilink")，目前亦是當地市佔率最高的[百貨公司](../Page/百貨公司.md "wikilink")，目前在越南的[胡志明市](../Page/胡志明市.md "wikilink")、[河内市及](../Page/河内市.md "wikilink")[海防市皆有分店](../Page/海防市.md "wikilink")\[35\]，据2018年10月官网显示越南有7间店\[36\]。

### 印尼

2011年，百盛購下印尼百貨—，目前該百貨在[印尼共有](../Page/印尼.md "wikilink")8間分店，除了印尼的一線及二線城市外，該百貨預計2015年登陸[蘇拉威西](../Page/蘇拉威西.md "wikilink")、[蘇門答臘及](../Page/蘇門答臘.md "wikilink")[加里曼丹的都市](../Page/加里曼丹.md "wikilink")。除了併購當地百貨公司外，在2013年9月，百盛在[棉蘭](../Page/棉蘭.md "wikilink")（Medan）也開設第一家分店，位在[雅加達的第二家分店則在](../Page/雅加達.md "wikilink")2014年6月開幕\[37\]，至2018年3月印尼已有15间店，较2017年减少两间\[38\]。

### 緬甸

2013年10月，百盛在[仰光開始第一家分店](../Page/仰光.md "wikilink")，是第一家外資零售集團進駐[緬甸市場](../Page/緬甸.md "wikilink")\[39\]。

### 老挝

百盛在[老挝的第一家购物中心将在](../Page/老挝.md "wikilink")2019年第四季開幕，是第一家进入老挝市场的外资零售集团。\[40\]

## 相關

  - [金獅集團](../Page/金獅集團.md "wikilink")

## 参考文献

## 外部链接

  - [馬來西亞百盛百貨官網](http://www.parkson.com.my/)
  - [中國百盛百貨官網](http://www.parksongroup.com.cn/)
  - [越南百盛百貨官網](http://www.parkson.com.vn/)
  - [印尼百盛百貨官網](http://www.parkson.co.id/)
  - [金獅集團官網](http://www.liongroup.com.my/)
  - [印尼Centro百貨官網](http://www.centro.co.id/)

[Category:香港交易所上市公司](../Category/香港交易所上市公司.md "wikilink")
[Category:香港上市綜合企業公司](../Category/香港上市綜合企業公司.md "wikilink")
[Category:中國百貨公司](../Category/中國百貨公司.md "wikilink")
[Category:中國民營企業](../Category/中國民營企業.md "wikilink")
[Category:馬來西亞公司](../Category/馬來西亞公司.md "wikilink")
[Category:馬來西亞百貨公司](../Category/馬來西亞百貨公司.md "wikilink")
[Category:越南百貨公司](../Category/越南百貨公司.md "wikilink")
[Category:印尼百貨公司](../Category/印尼百貨公司.md "wikilink")
[Category:百貨公司](../Category/百貨公司.md "wikilink")
[Category:香港交易所上市認股證](../Category/香港交易所上市認股證.md "wikilink")
[Category:1987年成立的公司](../Category/1987年成立的公司.md "wikilink")

1.  [英保良集团创办人病逝](http://www.orientaldaily.com.my/s/17592)东方Online，2015年1月1日

2.  [The Emporium Legend Lim Tow Yong
    (1925-2012)](https://remembersingapore.org/2012/04/09/the-emporium-legend-lim-tow-yong/)Remember
    Singapore，2012年4月9日

3.  [Parkson Retail to buy 42% of ODEL for Rs.1.4
    billion](http://www.dailymirror.lk/mirror-stock-watch-live/20643-parkson-retail-to-buy-42-of-odel-for-rs14-billion-.html)

4.  [百盛商業集團 2006年5月26日通告](http://www.parkson.com.cn/upload/200701/116960191264635400.pdf)

5.  [百盛集團主腦](http://www.singtaonet.com:82/commerce/200704/t20070430_525661.html)
    是[鍾廷森](../Page/鍾廷森.md "wikilink")

6.
7.  [購地發展正面‧百盛財測不變](http://biz.sinchew.com.my/node/64786?tid=18)

8.  [Store Location](http://www.parkson.com.my/store-location)Parkson

9.  [Parkson Malaysia Store Outlet
    Locations](http://trailsshoppers.blogspot.my/2016/04/parkson-malaysia-store-outlet-locations.html?m=1)Trails
    Shoppers，2016年4月6日

10. [Parkson Bintulu opens in time for
    X’mas](http://www.theborneopost.com/2018/12/22/parkson-bintulu-opens-in-time-for-xmas/)The
    Borneo Post，2018年12月22日

11. [通过重组商店位置
    百盛冀获较大贡献](http://www.kwongwah.com.my/?p=494032)光华日报，2018年4月2日

12.
13.
14. [百盛不續約·
    KLCC產托多元分租](https://www.sinchew.com.my/pad/con/2019-03/21/content_2025887.html)星洲網，2019年3月21日

15. [巴生河流域·商场冷
    工业热](http://www.sinchew.com.my/node/1800205)星洲网，2018年10月5日

16. [【独家】经济放缓 供过于求
    槟购物中心吹倒风](http://www.enanyang.my/news/20160808/【独家】经济放缓-供过于求br-槟购物中心吹倒风/amp/)e南洋，2016年8月8日

17. [從輝煌走入歷史-甲廣場百盛12月10日關閉](http://www.chinapress.com.my/20171101/)中国报，2017年11月1日

18.

19.
20. [百盛百货北京一门店悄然歇业
    两年来已关7家店](https://finance.sina.cn/chanjing/gsxw/2014-07-25/detail-icczmvun0589568.d.html?from=wap)新浪新聞，2014年7月25日

21. [百盛天津店宣布4月1日停业
    天津百货业陷寒冬](http://m.linkshop.com/article/news/318824)联商网，2015年3月4日

22. [百盛又关店
    离退出北京市场只有“一店之遥”](http://www.jiemian.com/article/1375309.html)界面新聞，2017年6月6日

23. [盘点2017郑州商场，德化新街异军突起，百盛黯然落幕](https://m.sohu.com/a/220694613_636169/?pvid=000115_3w_a)搜狐网，2017年2月3日

24. [又一家实体店阵亡，合肥元一百盛下月正式关店，半年连关两家店！](http://www.lingshouw.com/article-14522-1.html)中国零售网，2017年5月1日

25.
26. [“一阳指”直击百货痛点
    南昌绿地缤纷城迎来跃变](http://m.winshang.com/news635909.html)赢商网，2018年3月23日

27. [又一家知名百货将要退出大连](http://m.kanshangjie.com/Index/Show?catid=3714&id=118886&type=news)商界，2017年11月10日

28. [百盛重庆南坪店将转型为“百盛 YOUNG
    MALL”](http://m.linkshop.com/article/news/403085?from=web)联商网，2018年5月28日

29. [又一家百盛年底即将关停，传统百货怎样度过瓶颈期？](https://m.ef360.com/news/377425.html)華衣網，2018年11月6日

30. [百盛概况](http://www.parksongroup.com.cn/html_cn/about_parkson/Profile.php)百盛，2018年10月

31. [崑山百盛將於4月8日停業苦撐2年後還是倒下！](http://m.winshang.com/news654980.html)嬴商網，2019年3月20日

32. [百盛再关常营店
    关店成传统百货唯一出路？](http://house.people.com.cn/n1/2017/0608/c164220-29325178.html)人民網，2017年6月8日

33. [百盛关店，正确的选择？](http://content.businessvalue.com.cn/post/31690.html)鈦媒體，2014年8月29日

34. [谁杀死了“百货公司”？](https://36kr.com/p/5106992.html)36氪，2017年12月

35. [department store parkson taps Vietnam experience for Myanmar
    push](http://tuoitrenews.vn/business/1549/department-store-parkson-taps-vietnam-experience-for-myanmar-push)

36.
37. [Malaysia’s Parkson looks to Indonesia for
    expansion](http://www.thejakartapost.com/news/2012/06/28/malaysia-s-parkson-looks-indonesia-expansion.html)

38. [中国业务转型·百盛可返正轨](http://www.sinchew.com.my/node/1737500)星洲网，2018年3月19日

39. [Parkson to debut in
    Myanmar](http://insideretail.asia/2013/05/06/parkson-to-debut-in-myanmar/)

40. [Parkson to open Cambodia
    Mall](http://insideretail.asia/2014/10/06/parkson-open-cambodia-mall/)