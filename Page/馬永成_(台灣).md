**馬永成**（），[中華民國政治人物](../Page/中華民國.md "wikilink")，[臺灣外省人第二代](../Page/臺灣外省人.md "wikilink")，生於臺北市，祖籍[福州](../Page/福州.md "wikilink")。前任[中華民國總統府副秘書長](../Page/中華民國總統府.md "wikilink")。從政後長期擔任[陳水扁](../Page/陳水扁.md "wikilink")[幕僚](../Page/幕僚.md "wikilink")，與同受陳水扁重用的[臺灣客家人](../Page/臺灣客家人.md "wikilink")[羅文嘉齊名](../Page/羅文嘉.md "wikilink")，人稱「羅馬」拍檔。

因[台開案而請辭](../Page/台開案.md "wikilink")，隨後被控涉及[國務機要費案](../Page/國務機要費案.md "wikilink")，[國務機要費案一審判刑](../Page/國務機要費案.md "wikilink")33年、二審撤銷改判無罪；偽造文書案則判8個月、[緩刑](../Page/緩刑.md "wikilink")3年。

## 早年簡介

馬永成於[建國中學畢業後](../Page/建國中學.md "wikilink")，進入[國立臺灣大學政治系就讀](../Page/國立臺灣大學.md "wikilink")，在學期間與[羅文嘉等人參與](../Page/羅文嘉.md "wikilink")[學生運動](../Page/學生運動.md "wikilink")；之後馬永成與羅文嘉都出任時任[立法委員的](../Page/立法委員.md "wikilink")[陳水扁國會辦公室助理](../Page/陳水扁.md "wikilink")。1993年馬永成出任[福爾摩沙基金會執行長](../Page/福爾摩沙基金會.md "wikilink")，1994年擔任陳水扁國會辦公室主任，1994年[台北市長選舉中出任陳水扁競選總部執行總幹事](../Page/台北市長.md "wikilink")；陳水扁當選台北市長後，馬永成出任[台北市政府市長室參事](../Page/台北市政府.md "wikilink")。1997年，時任[台北市政府新聞處處長的羅文嘉因](../Page/台北市政府新聞處.md "wikilink")[臺北市拔河斷臂事件辭職](../Page/臺北市拔河斷臂事件.md "wikilink")，馬永成接任。1998年，馬永成轉任台北市政府新聞處處長與[台北市政府發言人](../Page/台北市政府發言人.md "wikilink")。

## 羅馬拍檔

陳水扁於1998年台北市長連任失利後，馬永成仍出任其助理。2000年陳水扁就任[中華民國總統後](../Page/中華民國總統.md "wikilink")，馬永成出任[總統府機要秘書](../Page/中華民國總統府.md "wikilink")；2005年，馬永成改任總統府副秘書長。馬永成與羅文嘉長期跟隨陳水扁，而被稱為陳水扁的「羅馬拍檔」，被認為是陳水扁最為重要的親信和幕僚。

## 弊案纏身

2006年以來，馬永成受到[在野黨](../Page/泛藍.md "wikilink")[立法委員的強烈批評](../Page/立法委員.md "wikilink")，被指控涉及多宗[弊案](../Page/陳水扁家庭密帳案.md "wikilink")。2006年6月1日，受[台開案陳水扁](../Page/台開案.md "wikilink")[權力下放影響](../Page/權力下放.md "wikilink")，辭去總統府副秘書長一職。2006年11月3日，因[國務機要費案被起訴](../Page/國務機要費案.md "wikilink")。2008年11月4日，因國務機要費案列為被告，並遭到收押。

2008年12月9日，[最高法院檢察署特別偵查組以馬永成已經充分說明相關問題](../Page/最高法院檢察署特別偵查組.md "wikilink")，向法院聲請撤銷[羈押](../Page/羈押.md "wikilink")；合議庭裁准以新台幣五十萬元交保，並限制住居（[聯合報](../Page/聯合報.md "wikilink")97.12.10）。

2009年9月11日，因國務機要費、洗錢、南港展覽館、龍潭購地四大弊案以及追加起訴部分，[台北地方法院](../Page/台北地方法院.md "wikilink")[一審判馬永成](../Page/一審.md "wikilink")20年[有期徒刑](../Page/有期徒刑.md "wikilink")，罪名包括《[貪汙治罪條例](../Page/貪汙治罪條例.md "wikilink")》中的「利用職務詐取財物」、「侵佔公有財物」、[偽造文書等](../Page/偽造文書.md "wikilink")\[1\]。

2014年8月29日，特偵組表示，總統府公文佚失案中，馬永成無具體犯罪事證，因此予以簽結\[2\]。

2015年1月5日貪汙部分，台北地方法院二審改判馬永成無罪，僅依偽造文書罪判刑8個月、緩刑3年\[3\]。

2016年6月2日，馬永成在[TVBS網路新聞中心的專訪中說](../Page/TVBS.md "wikilink")，他確實很難撇清自己與綠營的關係，他自認無論做什麼事都會有爭議，外界對他的印象「都留在過去」，但他已離開政治圈10年，他經商沒能耐、教書沒資格、更沒打算重返政壇\[4\]。

2016年7月15日，網路媒體《[上報](../Page/上報.md "wikilink")》正式上線，馬永成為首任副社長\[5\]。

## 家庭

妻子[范恬甄](../Page/范恬甄.md "wikilink")。

## 參考來源

[Category:中華民國總統府副秘書長](../Category/中華民國總統府副秘書長.md "wikilink")
[Category:臺北市政府發言人](../Category/臺北市政府發言人.md "wikilink")
[Category:臺北市政府新聞處處長](../Category/臺北市政府新聞處處長.md "wikilink")
[Category:台北市政治人物](../Category/台北市政治人物.md "wikilink")
[Category:前民主進步黨黨員](../Category/前民主進步黨黨員.md "wikilink")
[Category:國立臺灣大學社會科學院校友](../Category/國立臺灣大學社會科學院校友.md "wikilink")
[Category:臺北市立建國高級中學校友](../Category/臺北市立建國高級中學校友.md "wikilink")
[Category:台灣罪犯](../Category/台灣罪犯.md "wikilink")
[Category:福州裔台灣人](../Category/福州裔台灣人.md "wikilink")
[Category:台北市人](../Category/台北市人.md "wikilink")
[Yong永](../Category/馬姓.md "wikilink")
[Category:野百合學運參與者](../Category/野百合學運參與者.md "wikilink")
[Category:臺灣媒體工作者](../Category/臺灣媒體工作者.md "wikilink")

1.
2.
3.
4.
5.