## 大事记

<onlyinclude>

### 一月 至 十二月

  - [一月四日](../Page/一月四日.md "wikilink")：[布列塔尼的安妮宣布](../Page/布列塔尼的安妮.md "wikilink")，所有与法国国王结盟的人都将被视为犯下[大不敬](../Page/大不敬.md "wikilink")。
  - [三月十三日](../Page/三月十三日.md "wikilink"):
    刚满周岁的[查理二世成为](../Page/查理·约翰·阿玛迪斯_\(萨伏伊\).md "wikilink")[萨伏伊公爵](../Page/萨伏伊公爵.md "wikilink")，其母[蒙费拉的布兰切摄政](../Page/蒙费拉的布兰切.md "wikilink")。
  - [七月十三日](../Page/七月十三日.md "wikilink")：[卡斯塔夫的约翰完成了](../Page/卡斯塔夫的约翰.md "wikilink")[圣三一教堂](../Page/圣三一教堂（Hrastovlje）.md "wikilink")（今位于[斯洛文尼亚西南部](../Page/斯洛文尼亚.md "wikilink")）壁画的一个绘画周期。
  - [十二月十九日](../Page/十二月十九日.md "wikilink")：布列塔尼的安妮委派代表与[马克西米利安一世联姻](../Page/馬克西米利安一世_\(神聖羅馬帝國\).md "wikilink")。

### 未知日期

  - [足利义稙成为](../Page/足利义稙.md "wikilink")[日本](../Page/日本.md "wikilink")
    [室町幕府的](../Page/室町幕府.md "wikilink")[大将军](../Page/征夷大將軍.md "wikilink")。
  - [珀金·沃贝克于](../Page/珀金·沃贝克.md "wikilink")[勃艮第法庭宣称自己为英格兰国王](../Page/勃艮第公國.md "wikilink")[爱德华四世之子](../Page/爱德华四世.md "wikilink")。
  - 传统上[格伦代尔之战的爆发时间](../Page/格伦代尔之战（斯凯）.md "wikilink")。
  - [天主教](../Page/天主教会.md "wikilink")[传教士抵达](../Page/传教士.md "wikilink")[非洲的](../Page/非洲.md "wikilink")[刚果王国](../Page/刚果王国.md "wikilink")。
  - [佩罗·达·科维良抵达](../Page/佩罗·达·科维良.md "wikilink")[埃塞俄比亚](../Page/埃塞俄比亚.md "wikilink")。
  - [德国首次在](../Page/德国.md "wikilink")[哈布斯堡](../Page/哈布斯堡.md "wikilink")、[梅赫伦和](../Page/梅赫伦.md "wikilink")[因斯布鲁克之间建立常规邮政服务](../Page/因斯布鲁克.md "wikilink")。
  - [列奥纳多·达·芬奇通过细管发现](../Page/列奥纳多·达·芬奇.md "wikilink")[毛细现象](../Page/毛细现象.md "wikilink")。
  - 列奥纳多·达·芬奇发明了一种油灯：用玻璃管护住灯芯，置于装满水的球形玻璃罩内。
  - [诸圣堂于](../Page/诸圣堂.md "wikilink")[维滕贝格开工](../Page/维滕贝格.md "wikilink")。
  - [中国学者](../Page/中国.md "wikilink")[华燧发明](../Page/华燧.md "wikilink")[青铜板](../Page/青铜.md "wikilink")[活字印刷术](../Page/活字印刷术.md "wikilink")，虽然早在[1298年](../Page/1298年.md "wikilink")[王祯就已经用](../Page/王祯.md "wikilink")[锡试验过活字印刷](../Page/锡.md "wikilink")，且[朝鲜人也独立发明了铜板活字印刷术](../Page/朝鲜.md "wikilink")，活字印刷术仍被认为是中国华燧发明的。
  - [朱安诺·马托雷尔和](../Page/朱安诺·马托雷尔.md "wikilink")[马蒂·琼·德·加尔巴的](../Page/马蒂·琼·德·加尔巴.md "wikilink")《[白骑士](../Page/白骑士（Tirant_Lo_Blanc）.md "wikilink")》出版。
  - [阿尔杜斯·马努提乌斯迁往](../Page/阿尔杜斯·马努提乌斯.md "wikilink")[威尼斯](../Page/威尼斯.md "wikilink")。
  - [约翰·柯雷特获得](../Page/约翰·柯雷特.md "wikilink")[牛津大学莫德林学院文学硕士](../Page/牛津大学莫德林学院.md "wikilink")。
  - [约翰内斯·罗伊希林与](../Page/约翰内斯·罗伊希林.md "wikilink")[若望·皮科·德拉·米蘭多拉见面](../Page/若望·皮科·德拉·米蘭多拉.md "wikilink")。
  - 商人将[咖啡从](../Page/咖啡.md "wikilink")[也门带入](../Page/也门.md "wikilink")[麦加和](../Page/麦加.md "wikilink")[阿拉伯](../Page/阿拉伯半岛.md "wikilink")。*（近似日期）*

<onlyinclude>

## 出生

  - [二月十七日](../Page/二月十七日.md "wikilink")：[夏爾三世·德·波旁](../Page/夏爾三世·德·波旁.md "wikilink")，法兰西总管（[1527年卒](../Page/1527年.md "wikilink")）
  - [三月二十四日](../Page/三月二十四日.md "wikilink")：[格奥尔格·阿格里科拉](../Page/格奥尔格·阿格里科拉.md "wikilink")，德国学者与科学家（[1555年卒](../Page/1555年.md "wikilink")）
  - [四月](../Page/四月.md "wikilink")：[维托丽娅·科隆纳](../Page/维托丽娅·科隆纳.md "wikilink")，意大利诗人（[1547年卒](../Page/1547年.md "wikilink")）
  - [五月十六日](../Page/五月十六日.md "wikilink")：[普鲁士](../Page/普鲁士.md "wikilink")[公爵](../Page/公爵.md "wikilink")[阿尔布雷希特](../Page/阿尔布雷希特_\(普鲁士\).md "wikilink")（[1568年卒](../Page/1568年.md "wikilink")）
  - [六月二十八日](../Page/六月二十八日.md "wikilink")：[勃兰登堡的阿尔布雷希特](../Page/阿尔布雷希特_\(勃兰登堡\).md "wikilink")，德国选举人和大主教（[1545年卒](../Page/1545年.md "wikilink")）
  - [十月](../Page/十月.md "wikilink")：[欧拉乌斯·马格努斯](../Page/欧拉乌斯·马格努斯.md "wikilink")，瑞典神职者和作家（[1557年卒](../Page/1557年.md "wikilink")）
  - [十月十二日](../Page/十月十二日.md "wikilink")：[伯纳多·皮萨诺](../Page/伯纳多·皮萨诺.md "wikilink")，意大利作曲家（[1548年卒](../Page/1548年.md "wikilink")）
  - [十一月十日](../Page/十一月十日.md "wikilink")：[约翰三世](../Page/约翰三世_（克莱沃）.md "wikilink")，克莱沃公爵（[1539卒](../Page/1539.md "wikilink")）
  - *未知日期*
      - [阿奇博尔德·道格拉斯](../Page/阿奇博尔德·道格拉斯（阿格努斯）.md "wikilink")，第六任[阿格努斯伯爵](../Page/阿格努斯伯爵.md "wikilink")（[1556年卒](../Page/1556年.md "wikilink")）
      - [卢卡·吉尼](../Page/卢卡·吉尼.md "wikilink")，意大利医生和植物学家（[1566年卒](../Page/1566年.md "wikilink")）
      - [让·萨尔蒙·马克林](../Page/让·萨尔蒙·马克林.md "wikilink")，法国诗人（[1557年卒](../Page/1557年.md "wikilink")）
      - [卡斯帕·史文克菲德](../Page/卡斯帕·史文克菲德.md "wikilink")，德国神学家（[1561年卒](../Page/1561年.md "wikilink")）
  - *可能出生于此年的*
      - [Wijerd
        Jelckama](../Page/Wijerd_Jelckama.md "wikilink")，弗里西亚叛军首领（[1523年卒](../Page/1523年.md "wikilink")）
      - [阿德里安·伊森布兰特](../Page/阿德里安·伊森布兰特.md "wikilink")，弗兰德画家（[1551年卒](../Page/1551年.md "wikilink")）
      - [理查德·瑞奇（首位瑞奇男爵）](../Page/理查德·瑞奇（首位瑞奇男爵）.md "wikilink")，英格兰大法官（[1567年卒](../Page/1567年.md "wikilink")）
      - [约翰·塔弗纳](../Page/约翰·塔弗纳.md "wikilink")，英国作曲家和管风琴手（[1545年卒](../Page/1545年.md "wikilink")）
      - [萨林纳斯的玛利亚](../Page/玛利亚（萨林纳斯）.md "wikilink")，[威洛比夫人](../Page/威洛比.md "wikilink")，[皇家侍女](../Page/皇家侍女.md "wikilink")，[阿拉贡的凯瑟琳的朋友](../Page/阿拉贡的凯瑟琳.md "wikilink")

## 逝世

  - [一月二十七日](../Page/一月二十七日.md "wikilink")：[足利义政](../Page/足利义政.md "wikilink")，日本[幕府将军](../Page/幕府将军.md "wikilink")（[1435年生](../Page/1435年.md "wikilink")）
  - [三月六日](../Page/三月六日.md "wikilink")：[年轻的伊凡](../Page/年轻的伊凡.md "wikilink")，[特维尔统治者](../Page/特维尔.md "wikilink")（[1458年生](../Page/1458年.md "wikilink")）
  - [三月十三日](../Page/三月十三日.md "wikilink")：[查理一世](../Page/查理一世（萨伏伊）.md "wikilink")，萨伏伊公爵（[1468年生](../Page/1468年.md "wikilink")）
  - [四月六日](../Page/四月六日.md "wikilink")：[马加什一世](../Page/马加什一世.md "wikilink")，[匈牙利国王](../Page/匈牙利.md "wikilink")（[1443年生](../Page/1443年.md "wikilink")）
  - [五月十二日](../Page/五月十二日.md "wikilink")：[乔安娜](../Page/乔安娜（葡萄牙公主）.md "wikilink")，葡萄牙执政公主（[1452年生](../Page/1452年.md "wikilink")）
  - [五月二十二日](../Page/五月二十二日.md "wikilink")：[埃德蒙·格雷](../Page/埃德蒙·格雷（肯特郡伯爵）.md "wikilink")，[肯特郡第一任伯爵](../Page/肯特郡.md "wikilink")（[1416年生](../Page/1416年.md "wikilink")）
  - [八月十一日](../Page/八月十一日.md "wikilink")：[弗兰斯•波里得罗德](../Page/弗兰斯•波里得罗德.md "wikilink")，荷兰叛军首领（[1465年生](../Page/1465年.md "wikilink")）
  - [九月一日](../Page/九月一日.md "wikilink")：[席尔瓦的比阿特丽丝](../Page/席尔瓦的比阿特丽丝.md "wikilink")，[多米尼加修女](../Page/多米尼加共和国.md "wikilink")
  - *位置日期*
      - [马蒂·琼·德·加尔巴](../Page/马蒂·琼·德·加尔巴.md "wikilink")，加泰罗尼亚小说家
      - [Aonghas
        Óg](../Page/Aonghas_Óg.md "wikilink")，最后一位独立的[群岛之主](../Page/群岛之主.md "wikilink")

## 参考文献

[\*](../Category/1490年.md "wikilink")
[0年](../Category/1490年代.md "wikilink")
[9](../Category/15世纪各年.md "wikilink")