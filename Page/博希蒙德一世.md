[Gustave_dore_crusades_bohemond_alone_mounts_the_rampart_of_antioch.jpg](https://zh.wikipedia.org/wiki/File:Gustave_dore_crusades_bohemond_alone_mounts_the_rampart_of_antioch.jpg "fig:Gustave_dore_crusades_bohemond_alone_mounts_the_rampart_of_antioch.jpg")
**（塔兰托的）博希蒙德一世，塔兰托亲王和安条克亲王**（**Bohemond I，Prince of Taranto and Prince
of
Antioch**，约）[第一次十字军东征的主要将领](../Page/第一次十字军东征.md "wikilink")。他是[西西里的](../Page/西西里.md "wikilink")[诺曼统治者](../Page/诺曼人.md "wikilink")[卡拉布里亚公爵](../Page/卡拉布里亚.md "wikilink")的长子，吉斯卡尔出身雇佣兵，后成为阿普利亚和[卡拉布里亚公爵](../Page/卡拉布里亚.md "wikilink")。

博希蒙德1079年开始领兵，1081年攻占[阿夫洛纳](../Page/阿夫洛纳.md "wikilink")，同年拜占庭皇帝[阿历克塞一世登基](../Page/阿历克塞一世.md "wikilink")，与诺曼人为敌，成为博希蒙德的对手达30余年。1083年阿历克塞将他们逐出色萨利的拉里萨。1085年博希蒙德丧父，他只得到巴里的[塔兰托伯爵一个封邑](../Page/塔兰托.md "wikilink")。

1095年11月，教皇[乌尔班二世发起](../Page/乌尔班二世.md "wikilink")[第一次十字军东征](../Page/第一次十字军东征.md "wikilink")，博希蒙德立即响应，率领一支小小的诺曼军队参加东征，1096年冬经希腊半岛到达[君士坦丁堡](../Page/君士坦丁堡.md "wikilink")。十字军渡过[博斯普鲁斯海峡](../Page/博斯普鲁斯海峡.md "wikilink")，与土耳其人作战，在尼凯亚、多里莱乌姆和安条克的战斗中，博希蒙德战绩辉煌。

1098年第一次十字军攻陷[萨拉森人在](../Page/萨拉森人.md "wikilink")[叙利亚的防卫重镇](../Page/叙利亚.md "wikilink")[安条克后](../Page/安条克.md "wikilink")，在该城建立了完全照搬[欧洲](../Page/欧洲.md "wikilink")[封建制度的十字军国家](../Page/封建制度.md "wikilink")[安条克公国](../Page/安条克公国.md "wikilink")。塔兰托的博希蒙德被封为[安条克亲王](../Page/安条克亲王.md "wikilink")（称博希蒙德一世），作为这个公国的世袭统治者，并奉命驻守在那里。他屡次进犯领邦[阿勒颇](../Page/阿勒颇.md "wikilink")，扩展领土，但他在与北方的锡瓦斯[突厥人的战斗中失败被俘](../Page/突厥人.md "wikilink")，直到1103年获释。

1105年9月他前往罗马觐见教皇，然后游历法国，1106年与法王[腓力一世之女结婚](../Page/腓力一世.md "wikilink")。1107年再次进攻希腊，率大军在阿夫洛纳登陆，阿历克塞一世急于结束战争，让他统治安条克和几个希腊城邦。博希蒙德也无力再战，只得和阿历克塞一世签订[迪沃尔条约](../Page/迪沃尔条约.md "wikilink")，接受臣服拜占庭的屈辱条件。

## 资料来源

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
## 延伸阅读

  - Ghisalberti, Albert M. (ed) *Dizionario Biografico degli Italiani*.
    Rome.

## 外部链接

  - [第一次十字军东征](../Page/第一次十字军东征.md "wikilink")

|-    |- |-

[Category:安条克亲王](../Category/安条克亲王.md "wikilink")
[Category:意大利歷史](../Category/意大利歷史.md "wikilink")
[Category:的黎波里伯爵](../Category/的黎波里伯爵.md "wikilink")