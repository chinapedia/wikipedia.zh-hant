**音別町**（）為過去位於[北海道](../Page/北海道.md "wikilink")[釧路支廳西部的行政區](../Page/釧路支廳.md "wikilink")，已於2005年10月11日與舊釧路市、[阿寒町](../Page/阿寒町.md "wikilink")[合併為新設立的](../Page/市町村合併.md "wikilink")[釧路市](../Page/釧路市.md "wikilink")，但因此三地中間的[白糠町未加入合併](../Page/白糠町.md "wikilink")，使得原音別町地區成為釧路市的[飛地](../Page/飛地.md "wikilink")；原本的町公所現在則成為釧路市公所的分所。

音別的名稱來自[阿伊努語的](../Page/阿伊努語.md "wikilink")「o-mu-pet」，意思為河口堵塞的河流。

## 地理

位於釧路市市中心西方約45[公里](../Page/公里.md "wikilink")，[帶廣市東方約](../Page/帶廣市.md "wikilink")75公里處；南側面向[太平洋](../Page/太平洋.md "wikilink")，北部多為高山及丘陵地形，人口主要分布於沿海地區。

## 歷史

  - 1915年：尺別村戶長役場從白糠村（現在的白糠町）戶長役場分離，單獨設置。
  - 1919年：成立為北海道二級村。\[1\]
  - 1922年4月1日：改名為音別村。
  - 1959年1月1日：改制為音別町。
  - 2005年10月11日：與舊釧路市、阿寒町合併為新設立的釧路市。

## 交通

### 機場

  - [釧路機場](../Page/釧路機場.md "wikilink")（位於舊釧路市，距離約35公里）

### 鐵路

  - [北海道旅客鐵道](../Page/北海道旅客鐵道.md "wikilink")
      - [根室本線](../Page/根室本線.md "wikilink")：[直別車站](../Page/直別車站.md "wikilink")
        - [尺別車站](../Page/尺別車站.md "wikilink") -
        [音別車站](../Page/音別車站.md "wikilink")
  - 雄別煤礦鐵路尺別線：尺別車債～尺別煤礦山車站，10.8公里，已於1970年4月廢止
      - 尺別車站 - 社尺別車站 - 八幡臨時站 - 新尺別車站 - 旭町車站 - 尺別炭山車站

### 道路

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><dl>
<dt>一般國道</dt>

</dl>
<ul>
<li>國道38號</li>
</ul></td>
<td><dl>
<dt><a href="../Page/都道府縣道.md" title="wikilink">道道</a></dt>

</dl>
<ul>
<li>北海道道241號本流音別停車場線</li>
<li>北海道道361號尺別尺別停車場線</li>
<li>北海道道500號音別浦幌線</li>
</ul></td>
</tr>
</tbody>
</table>

## 教育

### 大學

  - 東日本學園大學教養部（已於1986年遷移至[當別町](../Page/當別町.md "wikilink")）

### 中學校

  - 音別町立音別中學校（現在的釧路市立音別中學校）

### 小學校

  - 音別町立音別小學校（現在的釧路市立音別小學校）

## 本地出身的名人

  - [本城慎之介](../Page/本城慎之介.md "wikilink")（[樂天株式會社](../Page/樂天株式會社.md "wikilink")
    現任[董事](../Page/董事.md "wikilink")、前副社長、樂天創始成員）

## 參考資料

[Category:釧路市](../Category/釧路市.md "wikilink")
[Category:釧路管內](../Category/釧路管內.md "wikilink")

1.