**阿尔阿尔**（[阿拉伯语](../Page/阿拉伯语.md "wikilink")：****）是[沙特阿拉伯北部的一座城市](../Page/沙特阿拉伯.md "wikilink")，靠近与伊拉克的边境。为[北部边疆省首府](../Page/北部边疆省.md "wikilink")，是沙特阿拉伯85号公路上重要的补给和中转站。人口145,237（2004年普查）。\[1\]

## 参考资料

[Category:沙特阿拉伯城市](../Category/沙特阿拉伯城市.md "wikilink")

1.  [Arar, Saudi Arabia Video | Tourism and
    Events](http://www.ovguide.com/arar-9202a8c04000641f80000000004aaebf)