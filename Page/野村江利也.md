**野村江利也**（，），是[日本](../Page/日本.md "wikilink")[CAROTTE事務所所屬的](../Page/CAROTTE.md "wikilink")[演員](../Page/演員.md "wikilink")。身高167厘米，[血型為B型](../Page/血型.md "wikilink")。出身於[東京市](../Page/東京市.md "wikilink")。

## 人物

  - 興趣：[足球](../Page/足球.md "wikilink")
  - 特長：每次[舉重可達](../Page/舉重.md "wikilink")1000下或以上
  - 將來的夢想：[歌手](../Page/歌手.md "wikilink")

## 演出作品

### 電視劇

  - [女人與愛與Mystery](../Page/女人與愛與Mystery.md "wikilink")
    [迷離大藏的事件簿2](../Page/迷離大藏的事件簿2.md "wikilink")（[東京電視台](../Page/東京電視台.md "wikilink")、2003年）。
  - [女王的教室](../Page/女王的教室.md "wikilink")（[日本電視台](../Page/日本電視台.md "wikilink")、2005年）飾演6年3班學生不破翔太。
  - [演歌女王](../Page/演歌女王.md "wikilink")（第3集）（日本電視台、2007年）。

### 其他電視節目

  - [USO?\!Japan](../Page/USO?!Japan.md "wikilink")（[東京廣播公司](../Page/東京廣播公司.md "wikilink")、2003年）
  - [體感！實感！機械人的未來](../Page/體感！實感！機械人的未來.md "wikilink")（[日本放送協會](../Page/日本放送協會.md "wikilink")、2004年）

### 電影

  - [功夫棒球](../Page/功夫棒球.md "wikilink")（2005年7月公映）飾演足球少年。

### 廣告

  - [Misawa Homes](../Page/Misawa_Homes.md "wikilink")「Misawa
    Homing」（2005年）
  - [倍樂生](../Page/倍樂生.md "wikilink")「Pocket Challenge V2」（2006年）
  - [Konami](../Page/Konami.md "wikilink")「」（2006年）

### 硬照

  - [Belluna郵購](../Page/Belluna郵購.md "wikilink")（目錄冊）（2000年）
  - [公文式](../Page/公文式.md "wikilink")（2000年4月開始，為期3年）
  - [雄雞社](../Page/雄雞社.md "wikilink")「」（解說如何縫紉童裝的工具書、ISBN
    4-277-72189-3）（2001年）
  - [法務省](../Page/法務省.md "wikilink")（海報）（2001年）
  - [西武](../Page/西武百貨.md "wikilink")（宣傳單張）（2001年）
  - [青葉圖書](../Page/青葉圖書.md "wikilink")「科學測驗」（2002年）
  - [NTT DoCoMo](../Page/NTT_DoCoMo.md "wikilink")「F66i/GPS
    手提電話」（小冊子）（2003年）
  - [伊藤洋華堂](../Page/伊藤洋華堂.md "wikilink")（宣傳單張）（2003、2004年）
  - [Child社](../Page/Child社.md "wikilink")（目錄冊）（2004年）
  - [須磨學園](../Page/須磨學園.md "wikilink")「中學入學資料」（小冊子、海報、官方網站）（2005年）

### 宣傳錄影帶

  - [ENIX](../Page/ENIX.md "wikilink")（用於宣傳項目）（2002年）

## 站外連結

  - [官方個人詳細資料](https://web.archive.org/web/20080617223848/http://www.carotte-t.com/nomuraeriya.html)

[Category:日本男演員](../Category/日本男演員.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")
[Category:日本兒童演員](../Category/日本兒童演員.md "wikilink")