**薩尼拉克县**（**Sanilac County,
Michigan**）是[美國](../Page/美國.md "wikilink")[密西根州](../Page/密西根州.md "wikilink")[下半島東部的一個縣](../Page/下半島.md "wikilink")，東臨[休倫湖](../Page/休倫湖.md "wikilink")。面積4,119
平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口44,547人。縣治[桑德斯基](../Page/桑德斯基_\(密西根州\).md "wikilink")
(Sandusky)。

成立於1822年9月10日，縣政府成立於1849年12月31日。縣名[懷恩多特族傳說中的一位戰士的名字](../Page/懷恩多特族.md "wikilink")。\[1\]。

## 参考文献

<div class="references-small">

<references />

</div>

[S](../Category/密歇根州行政区划.md "wikilink")
[Category:1848年建立的聚居地](../Category/1848年建立的聚居地.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.