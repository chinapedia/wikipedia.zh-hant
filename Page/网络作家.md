**網絡作家**，又稱**網上作家**，是一種將各种[文學形式](../Page/文學.md "wikilink")，包括[作品](../Page/網路小說.md "wikilink")、[新闻](../Page/新闻.md "wikilink")、[漫画](../Page/漫画.md "wikilink")、[剧本](../Page/剧本.md "wikilink")、[散文](../Page/散文.md "wikilink")、[杂文](../Page/杂文.md "wikilink")、[诗歌](../Page/诗歌.md "wikilink")、[评论等發表在](../Page/评论.md "wikilink")[網絡上的人](../Page/網絡.md "wikilink")，通過[电子书](../Page/电子书.md "wikilink")、[网络杂志](../Page/网络杂志.md "wikilink")、[网络社区](../Page/网络社区.md "wikilink")、[网络日志](../Page/网络日志.md "wikilink")、[博客](../Page/博客.md "wikilink")、[論壇](../Page/論壇.md "wikilink")、[同人誌](../Page/同人誌.md "wikilink")、[文學](../Page/文學.md "wikilink")[網站](../Page/網站.md "wikilink")、[電子郵件](../Page/電子郵件.md "wikilink")，以及建立個人作品網站等方式與[讀者交流](../Page/讀者.md "wikilink")。一般而言，這些作品在[網路上是供人免費閱讀的](../Page/網路.md "wikilink")，但[作者本身仍享有](../Page/作者.md "wikilink")[著作權](../Page/著作權.md "wikilink")。

## 作品發表

如果網絡作家的作品有一定的知名度時，有些[出版社會願意](../Page/出版社.md "wikilink")[出版](../Page/出版.md "wikilink")，有時也會加入一些沒有放在[網路上的章節](../Page/網路.md "wikilink")，或等[出版後再放到](../Page/出版.md "wikilink")[網路上供閱讀](../Page/網路.md "wikilink")。

在[部落格未流行的時候](../Page/部落格.md "wikilink")，有些[網站以統一的發表平台來聚集網路作家](../Page/網站.md "wikilink")，而在故事達到一定的規模及人氣時，該網站則有優先出版的權利。香港網絡作家[直折劍更成立了](../Page/直折劍.md "wikilink")「網上作家協會」，致力推廣寫作文化及提拔新手，。

由於大多數網络作家的作品在[網路上供人](../Page/網路.md "wikilink")[免費](../Page/免費.md "wikilink")[閱讀](../Page/閱讀.md "wikilink")，因此絕大多數以「網上作家」身分自居者均未能以其作品取得版權收入和稿費，未能以「網路作家」作為[全職](../Page/職業.md "wikilink")[工作](../Page/工作.md "wikilink")。

## 資格與質素

網路作家在網上發表作品，並不需要經過[出版社的](../Page/出版社.md "wikilink")[編輯](../Page/編輯.md "wikilink")、[校讎](../Page/校讎.md "wikilink")，因此文筆、[學歷水平等因素](../Page/學歷.md "wikilink")，對一個人能否成為網络作家並沒有影響，幾乎所有的網民都可以參與寫作，各種網路刊載平臺確保了網路作品能順利地發表，大量的自由撰稿人和業餘網路寫手以「網絡作家」身分自居，因此「網絡作家」的數量非常龐大（[部分文學愛好者會把網絡作家跟傳統作家予以區分](../Page/Wikipedia_talk:文學興趣小組維基人列表.md "wikilink")），這同時也出現了魚目混珠的現象，文學作品的質素不再得以保證。\[1\]

## 工作環境

在[中國大陸](../Page/中國大陸.md "wikilink")，網絡作家為了作品更新經常[通宵寫作](../Page/通宵.md "wikilink")，嚴重影響到身體健康。2012年[红袖添香网的網站作家](../Page/红袖添香网.md "wikilink")[青鋆因為過度寫作](../Page/青鋆.md "wikilink")，25歲病逝\[2\]。2013年6月[盛大文學旗下的網絡作家](../Page/盛大文學.md "wikilink")[十年雪落因為過度疲勞而猝死](../Page/十年雪落.md "wikilink")。網絡作家的身體健康問題也可能導致作品中斷更新\[3\]。

應對網絡作家的健康問題，[盛大文學在](../Page/盛大文學.md "wikilink")2013年5月開始為旗下簽約的網絡作家繳付醫療保險\[4\]。

## 作品題材

網路作家的作品題材並無限制，僅受限於當地對網路言論的審查以及作者張貼作品的網站限制。

## 參見

  - [網絡文學](../Page/網絡文學.md "wikilink")
  - [網路小說](../Page/網路小說.md "wikilink")

## 注釋

<references/>

[\*](../Category/網絡作家.md "wikilink")

1.  [袁瓊瓊](../Page/袁瓊瓊.md "wikilink")《九十一年小說選》的[序中提出](../Page/序.md "wikilink")[台灣網路作者水準普遍不足](../Page/台灣.md "wikilink")，有「劣幣驅逐良幣」的[趨勢](../Page/趨勢.md "wikilink")
2.  [网络写手青鋆过劳早逝
    扛着病卖命没晒几天太阳](http://www.chinanews.com/cul/2012/04-13/3816085.shtml)
3.  [網路寫手“十年雪落”猝死出租房內
    因過度勞累](http://big5.huaxia.com/zhwh/whrw/rd/2013/07/3409004.html)
4.  [盛大文学：网络写手将有医疗保险
    保障作者收入](http://www.idcps.com/News/20130515/54469.html)