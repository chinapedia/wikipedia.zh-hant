[LDAutoXRFPic.jpg](https://zh.wikipedia.org/wiki/File:LDAutoXRFPic.jpg "fig:LDAutoXRFPic.jpg")
**X射線荧光光谱儀**（X-ray Fluorescence
Spectrometer，簡稱：**XRF**光谱儀），是一種快速的、非破壞式的物質測量方法。X射线荧光（X-ray
fluorescence，XRF）是用高能量X射线或[伽玛射线轰击材料时激发出的次级X射线](../Page/伽玛射线.md "wikilink")。这种现象被广泛用于[元素分析和](../Page/元素分析.md "wikilink")[化学分析](../Page/分析化学.md "wikilink")，特别是在[金属](../Page/金属.md "wikilink")，[玻璃](../Page/玻璃.md "wikilink")，[陶瓷和建材的调查和研究](../Page/陶瓷.md "wikilink")，[地球化学](../Page/地球化学.md "wikilink")，[法医学](../Page/法医学.md "wikilink")，[考古学和艺术品](../Page/考古学.md "wikilink")\[1\]，例如[油画](../Page/油画.md "wikilink")\[2\]和壁画。

## 使用型態

XRF 用
[X光或其他激發源照射待分析樣品](../Page/X光.md "wikilink")，樣品中的[元素之內層電子被擊出後](../Page/元素.md "wikilink")，造成核外電子的躍遷，在被激發的電子返回[基態的時候](../Page/基態.md "wikilink")，會放射出特徵
X 光；不同的元素會放射出各自的特徵 X 光，具有不同的能量或波長特性。檢測器(Detector)接受這些 X
光，儀器軟體系統將其轉為對應的信號。這一現象廣泛用於元素分析和化學分析，特別是在研究[金屬](../Page/金屬.md "wikilink")，[玻璃](../Page/玻璃.md "wikilink")，[陶瓷和建築材料](../Page/陶瓷.md "wikilink")，以及在地球化學研究、法醫學、電子產品進料品管（EU
RoHS）和考古學等領域，在某種程度上與原子吸收光譜儀互補，減少工廠附設的品管實驗室之分析人力投入。

## X射線荧光的物理原理

當材料暴露在短波長 X
光檢查，或[伽瑪射線](../Page/伽瑪射線.md "wikilink")，其組成原子可能發生電離，如果原子是暴露於輻射與能源大於它的電離勢，足以驅逐內層軌道的[電子](../Page/電子.md "wikilink")，然而這使原子的電子結構不穩定，在外軌道的電子會“回補”進入低軌道，以填補遺留下來的洞。在“回補”的過程會釋出多餘的能源，光子能量是相等兩個軌道的能量差異的。因此，物質放射出的輻射，這是原子的能量特性。

## X射線荧光光譜法在化學分析

主要使用 X 射線束激發荧光輻射，第一次是在 [1928
年由格洛克爾和施雷伯提出的](../Page/1928_年.md "wikilink")\[3\]。到了現在，該方法作為非破壞性分析技術，並作為過程控制的工具，广泛应用于採掘和加工工業。原則上，最輕的元素，可分析出[鈹](../Page/鈹.md "wikilink")（z
= 4），但由於儀器的局限性和輕元素的低 X
射線產量，往往難以量化，所以針對能量分散式的X射線荧光光谱儀，可以分析從輕元素的[鈉](../Page/鈉.md "wikilink")（z
=
11）到鈾，而波長分散式則為從輕元素的[硼到](../Page/硼.md "wikilink")[鈾](../Page/鈾.md "wikilink")。

## 参阅

  - [发射光谱](../Page/发射光谱.md "wikilink")
  - [穆斯堡尔效应](../Page/穆斯堡尔效应.md "wikilink")，γ射线的共振荧光

## 参考资料

[Category:原子物理学](../Category/原子物理学.md "wikilink")
[Category:分子物理学](../Category/分子物理学.md "wikilink")
[Category:光谱学](../Category/光谱学.md "wikilink")
[Category:X射线](../Category/X射线.md "wikilink")
[Category:科学技术](../Category/科学技术.md "wikilink")

1.  De Viguerie L, Sole VA, Walter P, [Multilayers quantitative X-ray
    fluorescence analysis applied to easel
    paintings](http://www.ncbi.nlm.nih.gov/pubmed/19688344), Anal
    Bioanal Chem. 2009 Dec; 395(7): 2015-20. doi:
    10.1007/s00216-009-2997-0
2.  [X-Ray Fluorescence at
    ColourLex](http://colourlex.com/project/x-ray-fluorescence/)
3.  Glocker, R., and Schreiber, H., *Annalen der Physik.*, 85, (1928),
    p. 1089