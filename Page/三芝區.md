**三芝區**位於[台灣](../Page/台灣.md "wikilink")[新北市西北部](../Page/新北市.md "wikilink")，北鄰[石門區](../Page/石門區.md "wikilink")，西北濱[台灣海峽](../Page/台灣海峽.md "wikilink")，西南連[淡水區](../Page/淡水區.md "wikilink")，東南連[金山區及](../Page/金山區_\(台灣\).md "wikilink")[臺北市](../Page/臺北市.md "wikilink")[北投區](../Page/北投區.md "wikilink")。是第7、8、9屆[中華民國總統](../Page/中華民國總統.md "wikilink")[李登輝的故鄉](../Page/李登輝.md "wikilink")。

本區除[埔坪里](../Page/埔坪里.md "wikilink")、[埔頭里](../Page/埔頭里.md "wikilink")[人口密度大於](../Page/人口密度.md "wikilink")2,000人/[平方公里之外](../Page/平方公里.md "wikilink")，其餘11里的人口密度均未滿500人/平方公里，顯示本區[人口分布有過度集中之現象](../Page/人口.md "wikilink")。

## 歷史

清代以來屬「[芝蘭三堡](../Page/芝蘭三堡.md "wikilink")」，日治1920年在當地設-{庄}-（相當於今日的[鄉](../Page/鄉_\(中華民國\).md "wikilink")）時，取前者之名設立[三芝-{庄}-](../Page/三芝庄.md "wikilink")，隸屬[臺北州](../Page/臺北州.md "wikilink")[淡水郡](../Page/淡水郡.md "wikilink")，戰後改為[臺北縣轄鄉](../Page/新北市.md "wikilink")。2010年因臺北縣[改制直轄市](../Page/2010年中華民國縣市改制直轄市.md "wikilink")，改為三芝區至今。

## 地理

三芝區位大屯火山群西北斜面，地勢由西北部向東南升高，昔日火山噴發之熔岩流向三芝石門交界之麟山鼻，形成特殊的岬角海岸地形。三芝的階狀台地有圓山頂、二坪頂(三層台地)、車埕等，特殊的梯田景觀，被登錄為臺灣北部地景保育景點。\[1\]

三芝海岸長度約7.3公里，麟山鼻至沙崙湖為岩砂混合岸砂岸，潮間帶寬廣，俗稱「咾咕」的藻礁為三芝的特色之一。\[2\]

## 人口

## 文化

### 重要文化財

**《三芝新-{庄里}-石滬》**

  -
    位於三芝區新-{庄里}-蕃社路31之1號八仙宮附近沿海 ，乃北臺灣稀有保存尚屬完整之石滬。
    由卵石堆砌而成，形狀如兩半圓形結構，右側部分較為完整，2013年公告為新北市歷史建築
    。\[3\]

'''《三芝三板橋》 '''
[三芝三板橋FUJI2351.jpg](https://zh.wikipedia.org/wiki/File:三芝三板橋FUJI2351.jpg "fig:三芝三板橋FUJI2351.jpg")

  -
    三板橋乃清道光初年同安人林永出資所建之大屯山區過溪橋樑，砥溪中巨石分三段石板架設而成，名三板橋。於圓山村與店子村之間，為昔日居民往來重要路橋。由當地大屯山安山岩石條為材料，三個橋墩四個橋孔，橋墩可見井字排列之構造，具建築史價值。\[4\]

'''《淡水三芝八莊大道公輪祀》 '''

  -
    村落庄頭聚集之民俗信仰活動，九年一輪無廟有像，歷史超過二百年不曾間斷，1892年馬偕博士著作中曾記載與此活動相遇。活動方式異於他處，地域性特色濃厚，深具文化性。\[5\]

**《亦宛然掌中劇團布袋戲》**

  -
    [李天祿之父許金木遂拜許金水為師](../Page/李天祿.md "wikilink")，許金水乃福建布袋戲老師父陳婆的得意弟子。許金木後自組戲團名為「華陽台」，李天祿9歲從父習藝，1931年組團，以掌中戲偶「宛然若真」之意而取名「亦宛然」。民國73年李天祿開全臺首例，帶領「亦宛然」的師父，投入板橋莒光國小的布袋戲薪傳工作。亦宛然掌中劇團包含南管布袋戲、北管布袋戲，其手工藝雕刻，偶衣刺繡精緻，在李天祿與李傳燦父子一脈薪傳下，成為臺灣代表性技藝優良的布袋戲團。\[6\]

## 行政區劃與政治

**行政區域** 轄13個-{里}-級次行政區：\[7\]

  - 八賢-{里}-
  - 埔頭-{里}-
  - 古-{庄里}-
  - 新-{庄里}-
  - 埔坪-{里}-
  - 茂長-{里}-
  - 橫山-{里}-
  - 錫板-{里}-
  - 後厝-{里}-
  - 福德-{里}-
  - 圓山-{里}-
  - 店子-{里}-
  - 興華里:由田心村和車埕村合併而成，其**百拉卡公路**為通往陽明山之幹道。

**日治時期阿石門庄-{庄}-長**\[8\]

|      |       |     |              |         |
| ---- | ----- | --- | ------------ | ------- |
| 就任年  | 日本紀年  | 姓名  | 單位名稱         | 官職名     |
| 1920 | 大正九年  | 戴賢挺 | 淡水郡役所三芝-{庄}- | \-{庄}-長 |
| 1925 | 大正十四年 | 曾石岳 | 淡水郡役所三芝-{庄}- | \-{庄}-長 |
| 1929 | 昭和四年  | 盧根德 | 淡水郡役所石門-{庄}- | \-{庄}-長 |

## 交通

### 公路

  - [TW_CHW101.svg](https://zh.wikipedia.org/wiki/File:TW_CHW101.svg "fig:TW_CHW101.svg")[市道101號](../Page/市道101號.md "wikilink")
      - [TW_CHW101a.svg](https://zh.wikipedia.org/wiki/File:TW_CHW101a.svg "fig:TW_CHW101a.svg")[市道101甲](../Page/市道101號#甲線.md "wikilink")：百拉卡公路
  - [TW_PHW2.svg](https://zh.wikipedia.org/wiki/File:TW_PHW2.svg "fig:TW_PHW2.svg")[台2線](../Page/台2線.md "wikilink")：淡金公路
  - [TW_THWtp5.png](https://zh.wikipedia.org/wiki/File:TW_THWtp5.png "fig:TW_THWtp5.png")[北5線](../Page/北5線.md "wikilink")
  - [TW_THWtp6.png](https://zh.wikipedia.org/wiki/File:TW_THWtp6.png "fig:TW_THWtp6.png")[北6線](../Page/北6線.md "wikilink")
  - [TW_THWtp7.png](https://zh.wikipedia.org/wiki/File:TW_THWtp7.png "fig:TW_THWtp7.png")[北7線](../Page/北7線.md "wikilink")
  - [TW_THWtp7-1.png](https://zh.wikipedia.org/wiki/File:TW_THWtp7-1.png "fig:TW_THWtp7-1.png")[北7-1線](../Page/北7線#支線.md "wikilink")
  - [TW_THWtp8.png](https://zh.wikipedia.org/wiki/File:TW_THWtp8.png "fig:TW_THWtp8.png")[北8線](../Page/北8線.md "wikilink")
  - [TW_THWtp9.png](https://zh.wikipedia.org/wiki/File:TW_THWtp9.png "fig:TW_THWtp9.png")[北9線](../Page/北9線.md "wikilink")
  - [TW_THWtp10.png](https://zh.wikipedia.org/wiki/File:TW_THWtp10.png "fig:TW_THWtp10.png")[北10線](../Page/北10線.md "wikilink")
  - [TW_THWtp11.png](https://zh.wikipedia.org/wiki/File:TW_THWtp11.png "fig:TW_THWtp11.png")[北11線](../Page/北11線.md "wikilink")
  - [TW_THWtp12.png](https://zh.wikipedia.org/wiki/File:TW_THWtp12.png "fig:TW_THWtp12.png")[北12線](../Page/北12線.md "wikilink")
  - [TW_THWtp13.png](https://zh.wikipedia.org/wiki/File:TW_THWtp13.png "fig:TW_THWtp13.png")[北13線](../Page/北13線.md "wikilink")
  - [TW_THWtp14.png](https://zh.wikipedia.org/wiki/File:TW_THWtp14.png "fig:TW_THWtp14.png")[北14線](../Page/北14線.md "wikilink")
  - [TW_THWtp15.png](https://zh.wikipedia.org/wiki/File:TW_THWtp15.png "fig:TW_THWtp15.png")[北15線](../Page/北15線.md "wikilink")
  - [TW_THWtp18.png](https://zh.wikipedia.org/wiki/File:TW_THWtp18.png "fig:TW_THWtp18.png")[北18線](../Page/北18線.md "wikilink")

### 公車

  - [淡水客運](../Page/淡水客運.md "wikilink")
  - [中興巴士](../Page/中興巴士.md "wikilink")
  - [指南客運](../Page/指南客運.md "wikilink")

### 未來

  - [淡海輕軌三芝線](../Page/淡海輕軌.md "wikilink")（規劃中）\[9\]

## 公共設施

  - 警政

<!-- end list -->

  - [新北市政府警察局淡水分局](../Page/新北市政府警察局.md "wikilink")
      - 淡水分局交通分隊
      - 三芝分駐所
      - 后厝派出所
      - 興華派出所

<!-- end list -->

  - 消防

<!-- end list -->

  - [新北市政府消防局第三大隊](../Page/新北市政府消防局.md "wikilink")
      - 淡水分隊
      - 竹圍分隊

## 教育

  - 大專院校

<!-- end list -->

  - [馬偕醫學院](../Page/馬偕醫學院.md "wikilink")
  - [馬偕醫護管理專科學校](../Page/馬偕醫護管理專科學校.md "wikilink")

<!-- end list -->

  - 國民中學

<!-- end list -->

  - \[<http://www.szjh.ntpc.edu.tw/>　新北市立三芝國民中學\]

<!-- end list -->

  - 國民小學

<!-- end list -->

  - \[<http://www.sces.ntpc.edu.tw/>　新北市三芝區三芝國民小學\]
  - \[<http://www.hses.ntpc.edu.tw/>　新北市三芝區橫山國民小學\]
  - 新北市三芝區興華國民小學

<!-- end list -->

  - 圖書館

<!-- end list -->

  - [新北市立圖書館三芝分館](../Page/新北市立圖書館.md "wikilink")：位於新北市三芝區淡金路一段37號

## 旅遊

[三芝貝殼廟FUJI2467.jpg](https://zh.wikipedia.org/wiki/File:三芝貝殼廟FUJI2467.jpg "fig:三芝貝殼廟FUJI2467.jpg")
[三芝源興居.JPG](https://zh.wikipedia.org/wiki/File:三芝源興居.JPG "fig:三芝源興居.JPG")三芝遊客中心暨名人文物館

  - [淺水灣海濱公園](../Page/淺水灣_\(台灣\).md "wikilink")
  - 八連溪紅葉谷
  - [大屯山](../Page/大屯山.md "wikilink")
  - 水車公園7座
  - 三芝冷泉
  - [三芝福成宮](../Page/三芝福成宮.md "wikilink")
  - [三芝貝殼廟](../Page/三芝貝殼廟.md "wikilink")
  - [李天祿布袋戲文物館](../Page/李天祿.md "wikilink")
  - 源興居
  - 櫻木花道
  - 普洱茶博物館
  - [內柑宅古道](../Page/內柑宅古道.md "wikilink")
  - [圓柳古道](../Page/圓柳古道.md "wikilink")
  - [竿尾崙古道](../Page/竿尾崙古道.md "wikilink")
  - [八連古道](../Page/八連古道.md "wikilink")
  - [新北市立圖書館三芝分館](../Page/新北市立圖書館.md "wikilink")

## 特產

## 特色

## 區域立法委員

  - 新北市
    [第一選區](../Page/新北市第一選舉區_\(2008年立法委員\).md "wikilink")：[呂孫綾](../Page/呂孫綾.md "wikilink")

## 出身人物 (名人)

  - [杜聰明](../Page/杜聰明.md "wikilink")：臺灣第一位[醫學博士](../Page/醫學博士.md "wikilink")
  - [李登輝](../Page/李登輝.md "wikilink")：前[中華民國總統](../Page/中華民國總統.md "wikilink")
  - [江文也](../Page/江文也.md "wikilink")：作曲家，有「臺灣的蕭邦」之稱。
  - [盧修一](../Page/盧修一.md "wikilink")：已故[立法委員](../Page/立法委員.md "wikilink")
  - [連俞涵](../Page/連俞涵.md "wikilink")：新生代演員，出色的演技獲得評審的肯定，為第51屆金鐘獎戲劇節目新進演員獎得主。
  - [羅嘉翎](../Page/羅嘉翎.md "wikilink")：世界太跆拳道冠軍。

## 圖片集

[File:Sanjhih-Beach.jpg|三芝區的海灣夕陽](File:Sanjhih-Beach.jpg%7C三芝區的海灣夕陽)
<File:Waterwheel> in Sanjhih.JPG|三芝區的水車夜景

## 參考文獻

## 外部連結

  - [三芝區公所](http://www.sanzhi.ntpc.gov.tw)
  - [《認識三芝-三芝的地形》](http://kaggilach.blogspot.tw/2014/03/blog-post_12.html)

[S](../Category/新北市行政區劃.md "wikilink")
[三芝區](../Category/三芝區.md "wikilink")

1.
2.  [臺灣農委會林務局《臺灣北部地景保育景點登錄點列表》](http://conservation.forest.gov.tw/public/Attachment/410301715871.pdf)
3.  [三芝新-{庄里}-石滬](https://nchdb.boch.gov.tw/assets/advanceSearch/historicalBuilding/20130812000002)，文化部文化資產局
4.  [三芝三板橋](https://nchdb.boch.gov.tw/assets/advanceSearch/monument/20080116000001)
    ，文化部文化資產局
5.  [淡水三芝八庄大道公輪祀](https://nchdb.boch.gov.tw/assets/advanceSearch/folklore/20101012000003)，文化部文化資產局
6.  [亦宛然掌中劇團](https://nchdb.boch.gov.tw/assets/advanceSearch/traditionalPerformingart/20091012000001)，文化部文化資產局
7.  [鄰里資訊](https://www.sanzhi.ntpc.gov.tw/content/?parent_id=10096&type_id=10008)，三芝區公所
8.  [中研院台史所《臺灣總督府職員錄》](http://who.ith.sinica.edu.tw/mpView.action)
9.  營運單位:[高雄捷運公司](../Page/高雄捷運公司.md "wikilink")。羅倩宜，[中鋼承建淡海輕軌案 107年完工](http://news.ltn.com.tw/news/business/paper/837892)，自由時報，2014-12-10
    \[2015-02-15\].