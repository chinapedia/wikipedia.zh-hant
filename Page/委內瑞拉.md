**委內瑞拉玻利瓦爾共和國**（），通稱**委內瑞拉**，是一個位於[南美洲北部的](../Page/南美洲.md "wikilink")[熱帶國家](../Page/热带.md "wikilink")，為[南美洲國家聯盟的成員國](../Page/南美洲國家聯盟.md "wikilink")。首都為[卡拉卡斯](../Page/卡拉卡斯.md "wikilink")。西與[哥倫比亞接壤](../Page/哥倫比亞.md "wikilink")，東與[蓋亞那毗鄰](../Page/蓋亞那.md "wikilink")，南與[巴西交界](../Page/巴西.md "wikilink")。海岸線包括北部[加勒比海及東北部](../Page/加勒比海.md "wikilink")[大西洋島嶼總長約](../Page/大西洋.md "wikilink")2,800公里。委內瑞拉生物多樣性豐富，為西至[安地斯山脈](../Page/安地斯山脈.md "wikilink")、南至[亞馬遜盆地](../Page/亞馬遜盆地.md "wikilink")、東至[奥里诺科河三角洲物種的生物棲息地](../Page/奥里诺科河.md "wikilink")。

## 名称

委內瑞拉的國名「Venezuela」其實源自[義大利文](../Page/義大利文.md "wikilink")「小威尼斯」之義，其典故出於[義大利探險家](../Page/義大利.md "wikilink")[阿美利哥·維斯普西當初在](../Page/阿美利哥·維斯普西.md "wikilink")[馬拉開波湖見到](../Page/馬拉開波湖.md "wikilink")[美洲原住民所居住的水上高腳屋村落](../Page/美洲原住民.md "wikilink")，用意聯想到歐洲的水都[威尼斯而如此命名](../Page/威尼斯.md "wikilink")。由於南美經濟價值用途很高，為了吸引商人參與開發的興趣，便在歐洲推廣“小威尼斯”這個名稱，最後成為該地的代名詞<ref>Venezuela:
A Petro-State Using Renewable Energies Massabié  Achim 2008
ISSN: 2626-2827

</ref>`。至於委內瑞拉名稱後綴的「`[`玻利瓦爾`](../Page/玻利瓦爾.md "wikilink")`」，是1999年委國重修`[`憲法時才加入`](../Page/憲法.md "wikilink")`，用以紀念被視為獨立英雄的`[`西蒙·玻利瓦爾`](../Page/西蒙·玻利瓦爾.md "wikilink")`。`

## 历史

在古代，委內瑞拉是[印第安人](../Page/印第安人.md "wikilink")[阿拉瓦克族和加勒比族的居住地](../Page/阿拉瓦克族.md "wikilink")。1498年，[哥倫布發現了委內瑞拉](../Page/哥倫布.md "wikilink")。1522年時，[西班牙在南美洲的第一個](../Page/西班牙.md "wikilink")[殖民地新卡地茲就在今日的委內瑞拉境內建立](../Page/殖民地.md "wikilink")。1717年新格瑞那達副王轄區成立時，委內瑞拉被規劃為其下屬的一個省。
[缩略图](https://zh.wikipedia.org/wiki/File:SanCristobal.jpg "fig:缩略图")夜景\]\]
1810年4月19日的一場政變中，委內瑞拉脫離了西班牙的控制，並在7月5日正式宣告獨立，但是独立戰爭并未因此而结束；在經過了多年战斗之后，委國終於在南美洲著名的獨立英雄[西蒙·玻利瓦爾率領下](../Page/西蒙·玻利瓦爾.md "wikilink")，於1821年正式得到西班牙的承认而完全独立。直到1830年為止，委內瑞拉與哥倫比亞、[巴拿馬與](../Page/巴拿馬.md "wikilink")[厄瓜多等鄰國同樣](../Page/厄瓜多.md "wikilink")，都是[大哥倫比亞共和國的一部分](../Page/大哥倫比亞共和國.md "wikilink")。爾後委內瑞拉在1830年獨立建國，大哥倫比亞也宣告瓦解。

[缩略图](https://zh.wikipedia.org/wiki/File:Jose_Antonio_Páez.jpg "fig:缩略图")

[缩略图](https://zh.wikipedia.org/wiki/File:Flag_of_Venezuela_\(1830-1836\).svg "fig:缩略图")

1830年，玻利瓦爾逝世後，掌握軍政大權的[何塞·安東尼奥·派斯宣布委內瑞拉脫離大哥倫比亞獨立](../Page/何塞·安東尼奥·派斯.md "wikilink")。1831年派斯就任委內瑞拉國總統，實施強人政治，來自內部的威脅使得幾乎沒有機會處理委內瑞拉與其他國家之間與經濟，後來被政變推翻。[佩德羅·夸爾·埃斯坎東](../Page/佩德羅·夸爾·埃斯坎東.md "wikilink")
(Pedro Gual Escandón)以臨時總統的姿態上台，同時建立“委內瑞拉共和國（República de
Venezuela）”\[1\] 。
[缩略图](https://zh.wikipedia.org/wiki/File:Flag_of_Venezuela_\(1836–1859\).svg "fig:缩略图")

委內瑞拉共和國（República de
Venezuela）雖然成立，但實際仍然是寡頭政治，國家權利掌握在執政者，混亂的經濟更成為致命傷，在兩年後被[朱利安·卡斯楚·康特雷拉斯發動政變拉下台](../Page/朱利安·卡斯楚·康特雷拉斯.md "wikilink")，康特雷拉斯組成軍政府，以高壓控制統治，引起保守黨發動政變，重新奪取政權。最終在1859年爆發長達五年的戰爭。這場戰爭最終由[法爾孔率領的軍隊獲勝](../Page/胡安·克里索斯托莫·法爾孔·伊·薩瓦爾塞.md "wikilink")，共和國被推翻，[法爾孔宣佈建立](../Page/胡安·克里索斯托莫·法爾孔·伊·薩瓦爾塞.md "wikilink")“委內瑞拉合眾國(Estados
Unidos de Venezuela)”，並制定新憲法，採用聯邦制度。 \[2\]

[缩略图](https://zh.wikipedia.org/wiki/File:Combate_de_Maiquetia,_el_2_de_septiembre_de_1859.jpg "fig:缩略图")

[缩略图](https://zh.wikipedia.org/wiki/File:Flag_of_Venezuela_\(1863–1905\).svg "fig:缩略图")

委內瑞拉在19世纪和20世纪前半期，長期为政治的不稳定、政治斗争和[独裁政治统治所籠罩](../Page/独裁政治.md "wikilink")。1952年時任國防部[希門內斯以軍隊自任臨時總統](../Page/馬科斯·佩雷斯·希門內斯.md "wikilink")，推翻委內瑞拉合眾國，建立了現在的政權。1958年，民主運動最终迫使军方停止干预国政。从那年起，由投票產生的民主政府取代了軍事執政。
[缩略图](https://zh.wikipedia.org/wiki/File:Girl_selling_for_Palm_Sunday.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Bvl.png "fig:缩略图")\]\]

[缩略图](https://zh.wikipedia.org/wiki/File:Metroteques.JPG "fig:缩略图")
1998年[乌戈·查维斯第一次當選為委內瑞拉總統](../Page/乌戈·查维斯.md "wikilink")。他翌年就任總統後通過修改憲法，把總統的任期由五年延長至六年，可以連任一次。之後成立新的国民议会，國會由兩院制改為一院制，藉以打擊由反對派所控制的舊國會。2000年乌戈·查维斯經修憲後的總統選舉以約六成票數第二次當選委內瑞拉總統。
[缩略图](https://zh.wikipedia.org/wiki/File:Planta_Termoeléctrica_en_Maracaibo_cuted_version.jpg "fig:缩略图")
2006年3月7日，委内瑞拉国民议会通过了对国旗和国徽的修改方案，决定把国旗上7颗星增加为8颗，并将国徽上骏马飞奔的方向由向右改为向左。\[3\]12月3日，委内瑞拉举行总统大选，[乌戈·查维斯以](../Page/乌戈·查维斯.md "wikilink")63%的選票成功連任，亦是他第三次當選委內瑞拉總統。\[4\]

2009年2月15日，經由公民投票通過修憲（投票率67％；贊成票佔其中的54％，反對票佔46％），取消民選官員的連任限制，允許乌戈·查维斯自2012年起得以連選連任。\[5\]
\[6\]

2012年10月7日，委内瑞拉举行总统大选，乌戈·查维斯以54.42%的選票成功連任，亦是他第四次當選委內瑞拉總統，\[7\]惟五個月後，查维斯便因癌病去世，結束十四年的統治。

2013年3月5日，副总统[尼古拉斯·马杜罗出任临时总统](../Page/尼古拉斯·马杜罗.md "wikilink")。2013年3月8日，尼古拉斯·马杜罗正式宣誓就任代总统。4月14日，尼古拉斯·马杜罗以50.66%的得票率在总统大选中胜出，当选[委内瑞拉总统](../Page/委内瑞拉总统.md "wikilink")。

2014年2月起[反對派發動示威](../Page/2014-2016年委內瑞拉示威.md "wikilink")，要求總統馬杜羅下台，政府下令軍方鎮壓反對派，造成多人死傷，衝突至今仍未解決。

2019年1月，反对派控制的[委内瑞拉全国代表大会宣布任命议长](../Page/委内瑞拉全国代表大会.md "wikilink")[胡安·瓜伊多出任代总统](../Page/胡安·瓜伊多.md "wikilink")，引发[政治危机](../Page/2019年委內瑞拉總統地位危機.md "wikilink")。

## 政治

委內瑞拉实行[联邦](../Page/联邦.md "wikilink")[总统制](../Page/总统制.md "wikilink")，[总统是](../Page/委内瑞拉总统.md "wikilink")[国家元首和](../Page/国家元首.md "wikilink")[政府首脑](../Page/政府首脑.md "wikilink")，设副总统作为副手。[委内瑞拉全国代表大会即国会是由](../Page/委内瑞拉全国代表大会.md "wikilink")165席组成的[一院制立法机关](../Page/一院制.md "wikilink")，目前國會中最大政党是[民主團結圓桌會議](../Page/民主團結圓桌會議.md "wikilink")，是执政党[委內瑞拉統一社會主義黨最主要的反对党派](../Page/委內瑞拉統一社會主義黨.md "wikilink")。现任[委内瑞拉总统是](../Page/委内瑞拉总统.md "wikilink")[尼古拉斯·马杜罗](../Page/尼古拉斯·马杜罗.md "wikilink")。

## 軍事

委内瑞拉军約有現役12萬人，採義務役2年以上兵制。委內瑞拉軍事上早期與[美國親近並購買了](../Page/美國.md "wikilink")[F-16戰機等裝備](../Page/F-16戰機.md "wikilink")，但目前與[俄羅斯交好對美國採取對抗姿態](../Page/俄羅斯.md "wikilink")，而與[歐洲國家的軍事貿易來往並未中斷](../Page/歐洲.md "wikilink")。

## 地理

[SaltoAngel1.jpg](https://zh.wikipedia.org/wiki/File:SaltoAngel1.jpg "fig:SaltoAngel1.jpg"),
世界最高瀑布\]\]
該國位于[北半球](../Page/北半球.md "wikilink")，[赤道以北](../Page/赤道.md "wikilink")，全境屬[熱帶氣候](../Page/熱帶氣候.md "wikilink")，北臨[加勒比海與](../Page/加勒比海.md "wikilink")[大西洋](../Page/大西洋.md "wikilink")，東有[蓋亞那](../Page/蓋亞那.md "wikilink")，南為[巴西](../Page/巴西.md "wikilink")，西邊則與[哥倫比亞接壤](../Page/哥倫比亞.md "wikilink")，而在委內瑞拉外海則有[阿魯巴](../Page/阿魯巴.md "wikilink")、[荷屬安地列斯](../Page/荷屬安地列斯.md "wikilink")（皆為[荷蘭的](../Page/荷蘭.md "wikilink")[海外自治領土](../Page/境外領.md "wikilink")）與[千里達及托巴哥等島嶼邦國](../Page/千里達及托巴哥.md "wikilink")。首都[-{zh-hans:加拉加斯;
zh-hant:卡拉卡斯;}-是個濱海的](../Page/卡拉卡斯.md "wikilink")[港口](../Page/港口.md "wikilink")，也是該國最大城。

委內瑞拉是南美洲最重要的產油國，境內主要的[石油蘊藏區是國境西北角的南美洲第一大湖](../Page/石油.md "wikilink")——[馬拉開波湖](../Page/馬拉開波湖.md "wikilink")。馬拉開波湖是一个[潟湖](../Page/潟湖.md "wikilink")，北端是以一條34英里長的[海峽與](../Page/海峽.md "wikilink")[委內瑞拉灣相連](../Page/委內瑞拉灣.md "wikilink")，後者是加勒比海的一部分。

2007年12月9日開始，委內瑞拉的時區從原來的[UTC-4改為](../Page/UTC-4.md "wikilink")[UTC-4.5](../Page/UTC-4.5.md "wikilink")，比原來的時區晚了半小時。总统查韦斯表示，「提早破曉時間可提昇國家的生產力」，这可让委内瑞拉更多民众醒来时天色已亮，享有更多天亮时间，可提升全民表现。\[8\]

根据2016年4月18日发布的第6224号特别政府文告，委内瑞拉将从2016年5月1日凌晨2：30起修改国家法定时区，以更好利用自然光照，缓解电力危机。届时委内瑞拉将把现有时间向前调半小时，委內瑞拉的時區從原來的UTC-4.5改為UTC-4，与格林威治时间相差4小时。委内瑞拉国土主要位于西4区，曾于1965年和2007年调整过法定时区。目前没有[夏令时](../Page/夏令时.md "wikilink")。

### 行政區劃

[Venezuela_Division_Politica_Territorial.svg](https://zh.wikipedia.org/wiki/File:Venezuela_Division_Politica_Territorial.svg "fig:Venezuela_Division_Politica_Territorial.svg")
 委內瑞拉全國的一級行政區包括了23個州，一個首都特別區，與一个由72个加勒比海岛屿组成的联邦属地。它們分別是：

  - [聯邦區](../Page/聯邦區_\(委內瑞拉\).md "wikilink")（西班牙文：distrito capital，ISO
    3166-2代碼為VE-A）
  - 州（estados）
      - [亚马孙州](../Page/亚马孙州_\(委内瑞拉\).md "wikilink")（Amazonas，ISO
        3166-2代碼為VE-Z）
      - [安索阿特吉州](../Page/安索阿特吉州.md "wikilink")（Anzoátegui，ISO
        3166-2代碼為VE-B）
      - [阿普雷州](../Page/阿普雷州.md "wikilink")（Apure，ISO 3166-2代碼為VE-C）
      - [阿拉瓜州](../Page/阿拉瓜州.md "wikilink")（Aragua，ISO 3166-2代碼為VE-D）
      - [巴里纳斯州](../Page/巴里纳斯州.md "wikilink")（Barinas，ISO 3166-2代碼為VE-E）
      - [玻利瓦爾州](../Page/玻利瓦爾州.md "wikilink")（Bolivar，ISO 3166-2代碼為VE-F）
      - [卡拉沃沃州](../Page/卡拉沃沃州.md "wikilink")（Carabobo，ISO 3166-2代碼為VE-G）
      - [科赫德斯州](../Page/科赫德斯州.md "wikilink")（Cojedes，ISO 3166-2代碼為VE-H）
      - [阿马库罗三角洲州](../Page/阿马库罗三角洲州.md "wikilink")（Delta Amacuro，ISO
        3166-2代碼為VE-Y）
      - [法尔孔州](../Page/法尔孔州.md "wikilink")（Falcón，ISO 3166-2代碼為VE-I）
      - [瓜里科州](../Page/瓜里科州.md "wikilink")（Guárico，ISO 3166-2代碼為VE-J）
      - [拉腊州](../Page/拉腊州.md "wikilink")（Lara，ISO 3166-2代碼為VE-K）
      - [梅里达州](../Page/梅里达州.md "wikilink")（Mérida，ISO 3166-2代碼為VE-L）
      - [米兰达州](../Page/米兰达州.md "wikilink")（Miranda，ISO 3166-2代碼為VE-M）
      - [莫纳加斯州](../Page/莫纳加斯州.md "wikilink")（Monagas，ISO 3166-2代碼為VE-N）
      - [新埃斯帕塔州](../Page/新埃斯帕塔州.md "wikilink")（Nueva Esparta，ISO
        3166-2代碼為VE-O）
      - [波图格萨州](../Page/波图格萨州.md "wikilink")（Portuguesa，ISO
        3166-2代碼為VE-P）
      - [苏克雷州](../Page/苏克雷州.md "wikilink")（Sucre，ISO 3166-2代碼為VE-R）
      - [塔奇拉州](../Page/塔奇拉州.md "wikilink")（Táchira，ISO 3166-2代碼為VE-S）
      - [特鲁希略州](../Page/特鲁希略州.md "wikilink")（Trujillo，ISO 3166-2代碼為VE-T）
      - [瓦尔加斯州](../Page/瓦尔加斯州.md "wikilink")（Vargas，ISO 3166-2代碼為VE-X）
      - [亚拉奎州](../Page/亚拉奎州.md "wikilink")（Yaracuy，ISO 3166-2代碼為VE-U）
      - [苏利亚州](../Page/苏利亚州.md "wikilink")（Zulia，ISO 3166-2代碼為VE-V）
  - 聯邦屬地（dependencia federal，ISO 3166-2代碼為VE-W）
      - [聯邦屬地](../Page/聯邦屬地_\(委內瑞拉\).md "wikilink")（Dependencias
        Federales de Ultramar）

### 主要城市

[AutopistadecaracasUCV.JPG](https://zh.wikipedia.org/wiki/File:AutopistadecaracasUCV.JPG "fig:AutopistadecaracasUCV.JPG")
[Plaza_Bolívar_of_Ciudad_Bolívar.jpg](https://zh.wikipedia.org/wiki/File:Plaza_Bolívar_of_Ciudad_Bolívar.jpg "fig:Plaza_Bolívar_of_Ciudad_Bolívar.jpg")

  - [加拉加斯](../Page/加拉加斯.md "wikilink")（Caracas）
  - [马拉开波](../Page/马拉开波.md "wikilink")（Maracaibo）
  - [巴伦西亚](../Page/巴伦西亚_\(委内瑞拉\).md "wikilink")（Valencia）
  - [巴塞罗那](../Page/巴塞罗那_\(安索阿特吉州\).md "wikilink")（Barcelona）
  - [拉瓜伊拉](../Page/拉瓜伊拉.md "wikilink")（La Guaira）
  - [拉克鲁斯港](../Page/拉克鲁斯港.md "wikilink")（Puerto La Cruz）
  - [库马纳](../Page/库马纳.md "wikilink")（Cumana）
  - [梅里达](../Page/梅里達市.md "wikilink")（Merida）
  - [科罗](../Page/科罗.md "wikilink")（Coro）
  - [巴基西梅托](../Page/巴基西梅托.md "wikilink")（Barquisimeto）
  - [圣克里斯托瓦尔](../Page/圣克里斯托瓦尔.md "wikilink")（San Cristobal）
  - [梅塞德斯](../Page/梅塞德斯.md "wikilink")（La Mercedes）
  - [埃尔蒂格雷](../Page/埃尔蒂格雷.md "wikilink")（El Tigre）
  - [马图林](../Page/马图林.md "wikilink")（Maturin）
  - [图库皮塔](../Page/图库皮塔.md "wikilink")（Tucupita）
  - [玻利瓦尔城](../Page/玻利瓦尔城.md "wikilink")（Ciudad Bolivar）
  - [阿亞庫喬港](../Page/阿亞庫喬港.md "wikilink")（Puerto Ayacucho）
  - [阿塔瓦波河畔圣费尔南多](../Page/阿塔瓦波河畔圣费尔南多.md "wikilink")（San Fernando de
    Atabapo）

## 经济

委内瑞拉主要的经济活动是针对該國国内以及[出口需求而進行的石油开采及提炼](../Page/出口.md "wikilink")。不过，近年來制造业对委内瑞拉[国内生产总值的贡献已经超过了石油工业](../Page/国内生产总值.md "wikilink")。石油是这个国家最为丰富的资源，由[委内瑞拉石油公司](../Page/委内瑞拉石油公司.md "wikilink")（PDVSA）负责加工。官方的开采开始于1875年，由[委内瑞拉塔奇拉石油公司在](../Page/委内瑞拉塔奇拉石油公司.md "wikilink")[塔奇拉州的](../Page/塔奇拉州.md "wikilink")[卢比奥附近的拉阿尔吉特拉那庄园进行](../Page/卢比奥.md "wikilink")。不久后，委内瑞拉的第一家[炼油厂成立](../Page/炼油厂.md "wikilink")，生产[汽油](../Page/汽油.md "wikilink")、[煤油和](../Page/煤油.md "wikilink")[柴油](../Page/柴油.md "wikilink")。

从1922年起，委内瑞拉开始大量出口石油，並發生數次足以改变这个国家发展方向的事件。1961年，根据委内瑞拉人[胡安·巴勃罗·佩雷斯·阿方索的提议](../Page/胡安·巴勃罗·佩雷斯·阿方索.md "wikilink")，成立了[石油输出国组织](../Page/石油输出国组织.md "wikilink")（OPEC）。

在2000年代，在经历了众多的政治和社会的冲突之后，委内瑞拉经济显示出了强劲的复苏。2004年委内瑞拉的经济增长达到了17％（根据[世界银行统计是世界上最高的增长率之一](../Page/世界银行.md "wikilink")），到2005年末，委内瑞拉的[国内生产总值](../Page/国内生产总值.md "wikilink")（PIB）达到了9.4％的增长，而其2006年的经济增长也达到了9.4％之多。[国家风险的基本指数为](../Page/国家风险.md "wikilink")216。2005年12月的[失业率](../Page/失业率.md "wikilink")（8.9％，2006年曾经达到过8.5％）与2004年的数值（10.9％）相比降低了2个百分点。[外汇储备达到了](../Page/外汇储备.md "wikilink")372.99亿[美元](../Page/美元.md "wikilink")。

石油是委内瑞拉的经济命脉，石油收入占委内瑞拉出口收入的约80％。委内瑞拉为石油输出国组织成员，世界主要的产油国之一。这里各个种类的原油丰富，2004年出口852.928.510桶原油，大多出口到[美国](../Page/美国.md "wikilink")、[欧洲和其他](../Page/欧洲.md "wikilink")[拉丁美洲国家](../Page/拉丁美洲.md "wikilink")。这些石油大多产自委内瑞拉的马拉开波湖流域和巴里纳斯－阿布莱流域及委国东部流域。委内瑞拉政府于1976年将其石油工业[国有化](../Page/国有化.md "wikilink")。[委内瑞拉石油公司](../Page/委内瑞拉石油公司.md "wikilink")（PDVSA）开始掌控委石油资源，其下的分公司在委内瑞拉操控了六家规模不一的炼油厂和海外（[库拉骚](../Page/库拉骚.md "wikilink")、美国、[德国](../Page/德国.md "wikilink")、[瑞典和](../Page/瑞典.md "wikilink")[比利时](../Page/比利时.md "wikilink")）的炼油厂。委内瑞拉也是世界主要的天然气生产国之一，2003年的产量达到了297亿立方米；其产品为[天然气](../Page/天然气.md "wikilink")、[乙烷和](../Page/乙烷.md "wikilink")[丙烷气体](../Page/丙烷.md "wikilink")。

### 委內瑞拉危機

自2012年查韦斯时代末期到马杜罗上台以来，委内瑞拉的经济[急转直下](../Page/委內瑞拉危機.md "wikilink")。部分原因由于国际原油价格下跌，导致委内瑞拉出口所得锐减，国家无力负担对于国民的福利支出和进口商品的财政补贴，只能通过不断印钞票来填补财政赤字。另外，长期以来的对于进口商品的依赖和盲目的企业国有化，削弱了经济的抗冲击能力，而国内企业面临虚高的官方汇率和无视市场规律的官方产品定价，生产积极性大幅下降，很多企业陷入停产。面对恶性循环下的通货膨胀和财政[危机](../Page/委內瑞拉危機.md "wikilink")，委内瑞拉政府无力挽回局势，超市甚至陷入了[无货可卖的境地](../Page/委內瑞拉物資短缺.md "wikilink")。\[9\]\[10\]社会失业率高导致了治安的不稳定，整体经济环境于2015年开始急剧恶化。

## 人口

| 委內瑞拉人口數量\[11\]              |
| --------------------------- |
| 年份                          |
| 1971                        |
| 1990                        |
| 2009                        |
| <small>來源：OECD/世界銀行</small> |

### 種族結構

委內瑞拉人口為許多種族的後裔，其中[麥士蒂索人占大多數](../Page/麥士蒂索人.md "wikilink")，種族結構大致為麥士蒂索人68%、白人21%及非洲人10%\[12\]，而美洲原住民僅約1%\[13\]。

## 教育

委内瑞拉的教育機構分爲[學前教育](../Page/學前教育.md "wikilink")、[基礎教育](../Page/基礎教育.md "wikilink")、[中等教育和](../Page/中等教育.md "wikilink")[高等教育](../Page/高等教育.md "wikilink")。根據《教育組織法》規定，委内瑞拉從學齡前到中等教育階段實行[義務教育](../Page/義務教育.md "wikilink")，從6歲到15歲的學齡段人群免費進入由國家直接管理的學校，參加[本科教育](../Page/本科教育.md "wikilink")\[14\]。國家有權提供相關服務以促進和維持教育機會。根據官方數據，2005至2006學年共有1,010,946名兒童參加學前教育\[15\]，基礎教育入學人數為4,885,779人，中等教育入學人數為671,140人\[16\]。

1998年，委内瑞拉的成人人口的識字率達91.1%\[17\]。而截至2008年，識字率上升至95.2%。截至2005年，小學淨入學率為91％\[18\]，中學淨入學率為63％\[19\]。

由於目前委内瑞拉處於經濟困難狀態且犯罪率較高，委內瑞拉的大批畢業生轉向其他地區尋求機遇。一項名為“委内瑞拉國外社區”的研究（由委內瑞拉中央大學的托馬斯·派斯（Thomas
Paez）、梅賽德斯·維瓦斯（Mercedes
Vivas）和胡安·拉斐爾·普利多主導）表明，自玻利瓦爾革命以來，已有超過135萬大學畢業生離開委內瑞拉前往其他國家\[20\]\[21\]。據信近12％的委內瑞拉人居住在國外，而[愛爾蘭成為委内瑞拉學生們的熱門目的地](../Page/愛爾蘭.md "wikilink")\[22\]。根據委內瑞拉物理、數學和自然科學研究院院長克勞迪奧·比法諾（Claudio
Bifano）的說法，2013年中超過一半的醫學畢業生離開委內瑞拉\[23\]。

### 高級學術機構

  - 委內瑞拉中央大學（；簡稱：UCV）
  - 蘇利亞大學（，簡稱：LUZ）
  - 卡拉沃沃大學（；簡稱：UC）
  - [西蒙·玻利瓦爾大學](../Page/西蒙·玻利瓦爾大學.md "wikilink")
  - 新埃斯帕达大學（；簡稱：UNE）
  - 高級管理研究學院（；簡稱：IESA）
  - 安德烈斯·貝洛天主教大學（；簡稱：UCAB）
  - 利桑德羅阿爾瓦拉多大學（；簡稱：UCLA）
  - 安第斯大學（；簡稱：ULA）
  - 瑪格麗塔大學（；簡稱：UNIMAR）
  - 都會大學（；簡稱：UNIMET）
  - 國立塔奇拉實驗大學（；簡稱：UNET）
  - 國立玻利瓦爾武裝力量實驗理工大學（；簡稱：UNEFA）
  - 解放者實驗教育大學（；簡稱：UPEL）
  - “[安東尼奧·何塞·蘇克雷](../Page/安東尼奧·何塞·蘇克雷.md "wikilink")”國立實驗理工大學（；簡稱：UNEXPO）

## 外交

### 與關係

委內瑞拉曾在1941年與中華民國建立外交關係，但在1974年6月29日斷交。

### 與關係

委內瑞拉與中華人民共和國於1974年6月28日建交至今。

### 與關係

2019年2月23日，委內瑞拉總統[尼古拉斯·馬杜洛·莫羅斯不滿哥倫比亞政府支持臨時總統](../Page/尼古拉斯·馬杜洛·莫羅斯.md "wikilink")[瓜伊多與其反對派之陣營且對委內瑞拉提供人道救援](../Page/瓜伊多.md "wikilink")，宣布與其斷交。

### 與關係

2019年1月10日，巴拉圭總統[馬里奧·阿布鐸·貝尼特斯不滿其總統](../Page/馬里奧·阿布鐸·貝尼特斯.md "wikilink")[尼古拉斯·馬杜洛·莫羅斯違法連任](../Page/尼古拉斯·馬杜洛·莫羅斯.md "wikilink")，宣布與其斷交。

### 與關係

[美國與委內瑞拉之間曾經有過緊密的關係](../Page/美國.md "wikilink")，然而在1999年當社會黨[烏戈·查韋斯當選總統之後](../Page/烏戈·查韋斯.md "wikilink")，雙方關係趨於緊張。2019年1月23日，委內瑞拉總統馬杜羅宣佈斷絕與美國的關係，原因是美國承認委內瑞拉反對黨領導人、議會主席[瓜伊多的代總統地位](../Page/胡安·瓜伊多.md "wikilink")。美國對此回應說馬杜羅無權做出這一決定，美國將繼續與瓜伊多領導的臨時政府保持外交關係。\[24\]

## 體育

[棒球是委內瑞拉最流行的](../Page/棒球.md "wikilink")[體育運動項目之一](../Page/體育運動.md "wikilink")，當地有不少人前往[美國職棒大聯盟發展](../Page/美國職棒大聯盟.md "wikilink")，為美國職棒大聯盟第二大的海外球員來源地（僅次於[多明尼加](../Page/多明尼加.md "wikilink")）。委內瑞拉也擁有[職業棒球聯盟](../Page/職業棒球.md "wikilink")。2006年，以六勝零敗的佳績獲得[加勒比海大賽的冠軍](../Page/加勒比海大賽.md "wikilink")。2010年加勒比海大賽將在委內瑞拉的瑪格麗塔島舉行。\[25\]
相較於其他[南美洲國家](../Page/南美洲.md "wikilink")，[足球在委內瑞拉較不盛行](../Page/足球.md "wikilink")，是[南美洲足球協會中最弱的一國](../Page/南美洲足球協會.md "wikilink")，從未踢進[世界盃決賽週](../Page/世界杯足球赛.md "wikilink")，但最近进步神速，其明星球员[胡安·阿朗戈曾效力于](../Page/胡安·阿朗戈.md "wikilink")[德甲](../Page/德甲.md "wikilink")[门兴格拉德巴赫俱乐部](../Page/门兴格拉德巴赫.md "wikilink")。

[2007年美洲國家盃在委内瑞拉舉行](../Page/2007年美洲國家盃.md "wikilink")，委內瑞拉隊也踢進了半準決賽。\[26\]2011年在阿根廷舉行第44屆比賽首度晉級四強，創下史上最佳成績。

[帕斯托·马尔多纳多在](../Page/帕斯托·马尔多纳多.md "wikilink")[2012年世界一级方程式锦标赛](../Page/2012年世界一级方程式锦标赛.md "wikilink")[西班牙大奖赛上取得冠军](../Page/2012年西班牙大奖赛.md "wikilink")，成为委内瑞拉历史上第一位一级方程式分站赛冠军。

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

  - [Presidencia de la República de
    Venezuela](https://web.archive.org/web/20090319201106/http://www.venezuela.gov.ve/)备注:Presidencia
    de la República de Venezuela\>Venezuela,the Bolivarian Republic
    of（VEN,ve,862）,58

  - [.::. Centro de Informacion de Red de Venezuela - Venezuelan
    ...](http://www.nic.ve)备注:.::. Centro de Informacion de Red de
    Venezuela - Venezuelan ...\>Venezuela,the Bolivarian Republic
    of（VEN,ve,862）,58

  -
  -

[委内瑞拉](../Category/委内瑞拉.md "wikilink")
[Category:南美洲國家](../Category/南美洲國家.md "wikilink")
[Category:聯邦制國家](../Category/聯邦制國家.md "wikilink")
[Category:石油輸出國組織成員國](../Category/石油輸出國組織成員國.md "wikilink")
[Category:南美洲國家聯盟成員國](../Category/南美洲國家聯盟成員國.md "wikilink")

1.

2.

3.  [1](http://news.xinhuanet.com/photo/2006-03/13/content_4299334.htm)

4.  [2](http://news.xinhuanet.com/world/2006-12/06/content_5443927.htm)

5.  [修憲過關
    查維茲萬年總統有望](http://www.libertytimes.com.tw/2009/new/feb/17/today-int4.htm)


6.  [終身總統 查維茲當定了](http://udn.com/NEWS/WORLD/WOR3/4740255.shtml)

7.  [3](http://hk.news.yahoo.com/%E5%A7%94%E5%85%A7%E7%91%9E%E6%8B%89%E5%A4%A7%E9%81%B8-%E6%9F%A5%E7%B6%AD%E6%96%AF%E9%80%A3%E4%BB%BB%E6%88%90%E5%8A%9F-041500515.html)

8.  <http://www.chinanews.com.cn/gj/lmfz/news/2007/08-24/1009660.shtml>

9.  [4](http://mt.sohu.com/20160430/n447026447.shtml)

10. [5](http://www.economist.com/blogs/americasview/2013/02/venezuela%E2%80%99s-currency)

11. [CO2 Emissions from Fuel
    Combustion](http://www.iea.org/co2highlights/co2Highlights.XLS)
    Population 1971–2008
    IEA（[pdf](http://iea.org/co2highlights/co2highlights.pdf) ）pp.
    83–85

12. "[Venezuela – Ethnic
    groups](http://countrystudies.us/venezuela/17.htm)". [Library of
    Congress Country
    Studies](../Page/Library_of_Congress_Country_Studies.md "wikilink")

13.

14. [Ley Orgánica de
    Educación](http://ulaweb.adm.ula.ve/ula_sea/Documentos/Normativa/Nacional.PDF)


15. Instituto Nacional de Estadística (2006) [Cifras definitivas para la
    educación](http://www.ine.gob.ve/condiciones/educacion.asp)

16.
17.

18.

19.
20.

21.

22.

23.

24.

25. 祝立康，[加勒比海大賽／等了17年　委內瑞拉全勝封王](http://www.ettoday.com/2006/02/09/341-1903248.htm)，ETtoday

26. 葉慧，[現在進入美洲杯時間！](http://news.sina.com.tw/sports/ycwb/cn/2007-06-26/090912575553.shtml)，sina