**劉炳偉**（），中華民國[政治人物](../Page/政治人物.md "wikilink")，曾任[板橋市民代表會主席](../Page/板橋區_\(台灣\).md "wikilink")、[立法委員](../Page/立法委員.md "wikilink")、[臺灣省議員](../Page/臺灣省議員.md "wikilink")、[臺灣省議會議長](../Page/臺灣省議會.md "wikilink")、[臺灣省議會副議長](../Page/臺灣省議會.md "wikilink")\[1\]\[2\]。

## 生平

劉炳偉出身於[板橋](../Page/板橋區_\(台灣\).md "wikilink")[海山劉家](../Page/海山劉家.md "wikilink")，其父[劉順天投資房地產致富後便熱衷於政治](../Page/劉順天.md "wikilink")，曾擔任[臺北縣議員](../Page/臺北縣議員.md "wikilink")、[板橋鎮長及升格後的](../Page/板橋區_\(台灣\).md "wikilink")[板橋市長](../Page/板橋區_\(台灣\).md "wikilink")。

劉炳偉27歲即代表家族參選板橋市民代表，當選後又獲選為市代會主席，從此在政壇叱吒了20多年。

1992年底其弟[劉炳華當選](../Page/劉炳華.md "wikilink")[立法委員](../Page/立法委員.md "wikilink")，當時劉炳偉擔任[臺灣省議會副議長](../Page/臺灣省議會.md "wikilink")。

1994年更上層樓擔任[臺灣省議會議長](../Page/臺灣省議會.md "wikilink")，1998年12月20日伴隨著[精省而卸任](../Page/精省.md "wikilink")，成為末代省議長。

1999年初劉家爆發財務危機。

2001年劉炳偉競選立委連任又失利，自此劉家在政壇開始走下坡\[3\]。

## 爭議

劉炳偉在2000年間，涉嫌利用議長權勢，關說[第一銀行貸款給前台北縣議員](../Page/第一銀行.md "wikilink")[黃忠信四億八千多萬元](../Page/黃忠信.md "wikilink")，事後向黃忠信借得一億元，但這筆帳黃忠信只繳一年利息，並因案逃亡中國，該筆貸款即列入呆帳。[調查局台北縣調查站約談劉炳偉後](../Page/調查局.md "wikilink")，劉炳偉坦承為了順利獲得黃忠信的借款，幫忙向一銀關說，由總行指示華江分行核貸給黃忠信。檢方於2008年10月14日當庭以涉嫌圖利及背信罪逮捕劉炳偉\[4\]，但隨即在翌日地院召開合議庭審理後，以罪證不足駁回，獲判無罪當庭獲釋\[5\]。

## 家族

  - 父親:[劉順天](../Page/劉順天.md "wikilink")。
  - 大弟:[劉炳煌](../Page/劉炳煌.md "wikilink")。
  - 二弟:[劉炳華](../Page/劉炳華.md "wikilink")，曾任[立法委員](../Page/立法委員.md "wikilink")。
  - 三弟:[劉炳中](../Page/劉炳中.md "wikilink")。
  - 女兒:[劉美芳](../Page/劉美芳.md "wikilink")，現任[新北市議員](../Page/新北市議員.md "wikilink")。

## 參選記錄

### [1994年臺灣省議員選舉](../Page/1994年中華民國省市長暨省市議員選舉.md "wikilink")

| 1994年[臺灣省議員選舉](../Page/臺灣省議員.md "wikilink") |
| ------------------------------------------- |
|                                             |
| 號次                                          |
| 1                                           |
| 2                                           |
| 3                                           |
| 4                                           |
| 5                                           |
| 6                                           |
| 7                                           |
| 8                                           |
| 9                                           |
| 10                                          |
| 11                                          |
| 12                                          |
| 13                                          |
| 14                                          |
| 15                                          |
| 16                                          |
| 17                                          |
| 18                                          |
| 19                                          |
| 20                                          |
| 21                                          |
| 22                                          |
| **選區**                                      |

### [1998年中華民國立法委員選舉](../Page/1998年中華民國立法委員選舉.md "wikilink")

| 1998年[立法委員選舉](../Page/立法委員.md "wikilink") |
| ----------------------------------------- |
|                                           |
| 號次                                        |
| 1                                         |
| 2                                         |
| 3                                         |
| 4                                         |
| 5                                         |
| 6                                         |
| 7                                         |
| 8                                         |
| 9                                         |
| 10                                        |
| 11                                        |
| 12                                        |
| 13                                        |
| 14                                        |
| 15                                        |
| 16                                        |
| 17                                        |
| 18                                        |
| 19                                        |
| 20                                        |
| 21                                        |
| 22                                        |
| 23                                        |
| **選區**                                    |

### [2001年中華民國立法委員選舉](../Page/2001年中華民國立法委員選舉.md "wikilink")

| 2001年[立法委員選舉](../Page/立法委員.md "wikilink") |
| ----------------------------------------- |
|                                           |
| 號次                                        |
| 1                                         |
| 2                                         |
| 3                                         |
| 4                                         |
| 5                                         |
| 6                                         |
| 7                                         |
| 8                                         |
| 9                                         |
| 10                                        |
| 11                                        |
| 12                                        |
| 13                                        |
| 14                                        |
| 15                                        |
| 16                                        |
| 17                                        |
| 18                                        |
| 19                                        |
| 20                                        |
| 21                                        |
| 22                                        |
| 23                                        |
| **選區**                                    |

## 相關條目

  - [海山劉家](../Page/海山劉家.md "wikilink")
  - [劉炳華](../Page/劉炳華.md "wikilink")
  - [劉美芳](../Page/劉美芳.md "wikilink")

## 注釋

## 外部連結

  - [立法院全球資訊網－立法委員－劉炳偉委員簡介](http://www.ly.gov.tw/03_leg/0301_main/legIntro.action?lgno=00196&stage=4)
  - [臺灣省諮議會－劉炳偉](http://www.tpa.gov.tw/opencms/digital/area/past/past01/member0058.html)

|- |colspan="3"
style="text-align:center;"|**[臺灣省議會](../Page/臺灣省議會.md "wikilink")**
|-    |-    |-

[Category:第4屆中華民國立法委員](../Category/第4屆中華民國立法委員.md "wikilink")
[Category:台灣省議長](../Category/台灣省議長.md "wikilink")
[Category:臺灣省副議長](../Category/臺灣省副議長.md "wikilink")
[Category:第10屆臺灣省議員](../Category/第10屆臺灣省議員.md "wikilink")
[Category:第9屆臺灣省議員](../Category/第9屆臺灣省議員.md "wikilink")
[Category:第8屆臺灣省議員](../Category/第8屆臺灣省議員.md "wikilink")
[Category:第7屆臺灣省議員](../Category/第7屆臺灣省議員.md "wikilink")
[Category:中國國民黨黨員](../Category/中國國民黨黨員.md "wikilink")
[Category:醒吾技術學院校友](../Category/醒吾技術學院校友.md "wikilink")
[Category:板橋人](../Category/板橋人.md "wikilink")
[Bing炳](../Category/板橋劉家.md "wikilink")
[Bing炳](../Category/劉姓.md "wikilink")

1.  原副議長[黃鎮岳當選](../Page/黃鎮岳.md "wikilink")[監察委員因而留下的](../Page/監察委員.md "wikilink")[副議長缺經改選由](../Page/臺灣省議會.md "wikilink")**劉炳偉**替補
2.  [立法院全球資訊網
    劉炳偉委員簡介](http://www.ly.gov.tw/ly/01_introduce/0103_leg/leg_main/leg_ver01_01.jsp?ItemNO=01030100&lgno=00196&stage=4)
3.  [政金兩衰板橋劉家式微　劉炳偉：我不是深喉嚨](http://www.nownews.com/2005/11/05/91-1865905.htm)
    2005/11/05
4.  [關說貸款4.8億變呆帳
    劉炳偉聲押](http://www.libertytimes.com.tw/2008/new/oct/15/today-p5.htm)

5.  [罪證不足
    劉炳偉當庭獲釋](http://www.libertytimes.com.tw/2008/new/oct/16/today-p4.htm)