**松代藩**（），[日本](../Page/日本.md "wikilink")[江户时代的一个](../Page/江户时代.md "wikilink")[藩](../Page/藩.md "wikilink")，位于[信浓国](../Page/信浓国.md "wikilink")[埴科郡](../Page/埴科郡.md "wikilink")[松代町](../Page/松代町.md "wikilink")（现属[长野县](../Page/长野县.md "wikilink")[长野市](../Page/长野市.md "wikilink")），是信浓境内最大的藩，藩主居于[松代城](../Page/松代城.md "wikilink")，统治[川中岛地区的四个](../Page/川中岛.md "wikilink")[町](../Page/町.md "wikilink")。[酒井家](../Page/酒井家.md "wikilink")、[松平家和](../Page/松平家.md "wikilink")[真田家先后被封于此](../Page/真田家.md "wikilink")。该藩曾名**川中岛藩**，为[森忠政的领地](../Page/森忠政.md "wikilink")。[佐久间象山即出身于该藩](../Page/佐久间象山.md "wikilink")。其藩主后代在[明治维新中被封为](../Page/明治维新.md "wikilink")[子爵](../Page/子爵.md "wikilink")，后升为[伯爵](../Page/伯爵.md "wikilink")。

## 历代藩主

### 川中島藩

#### 森家

外様　137,500石　

1.  [忠政](../Page/森忠政.md "wikilink")（ただまさ）〔從四位下、右近大夫・侍從〕　1600年（[慶長](../Page/慶長.md "wikilink")5年：[美濃国金山移来](../Page/美濃国.md "wikilink")）－1603年（慶長8年：移去[美作国](../Page/美作国.md "wikilink")[津山藩](../Page/津山藩.md "wikilink")）

#### 松平（まつだいら）家

[親藩](../Page/親藩.md "wikilink")　120,000石

1.  [忠輝](../Page/松平忠輝.md "wikilink")（ただてる）〔從四位下、左近衛少将〕　1603年（慶長8年：[下總国](../Page/下總国.md "wikilink")[佐倉藩移来](../Page/佐倉藩.md "wikilink")）－1610年（慶長15年：移去[越後国](../Page/越後国.md "wikilink")[高田藩](../Page/高田藩.md "wikilink")）

### 松代藩

#### 松平〔越前〕（まつだいら〔えちぜん〕）家

親藩　120,000石

1.  [忠昌](../Page/松平忠昌.md "wikilink")（ただまさ）〔從五位下、伊予守〕　1616年（[元和](../Page/元和.md "wikilink")2年：[常陸国](../Page/常陸国.md "wikilink")[下妻藩移来](../Page/下妻藩.md "wikilink")）－1619年（元和5年：移去越後国高田藩）

#### 酒井（さかい）家

[譜代](../Page/譜代.md "wikilink")　100,000石

1.  [忠勝](../Page/酒井忠勝_\(出羽国庄内藩主\).md "wikilink")（ただかつ）〔從四位下、宮内大輔〕　1619年（元和5年：越後国高田藩移来）－1622年（元和8年：移去[出羽国](../Page/出羽国.md "wikilink")[庄内藩](../Page/庄内藩.md "wikilink")）

#### 真田（さなだ）家

外様（譜代格）100,000石

1.  [信之](../Page/真田信之.md "wikilink")（のぶゆき）〔從五位下、伊豆守〕　1622年（元和8年：[信濃国](../Page/信濃国.md "wikilink")[上田藩移来](../Page/上田藩.md "wikilink")）－1656年（[明历](../Page/明历.md "wikilink")2年）
2.  [信政](../Page/真田信政.md "wikilink")（のぶまさ）〔從五位下、大内記〕　1656年（明历2年）－1658年（[万治元年](../Page/万治.md "wikilink")）
3.  [幸道](../Page/真田幸道.md "wikilink")（ゆきみち）〔從四位下、伊豆守〕　1658年（万治元年）－1727年（[享保](../Page/享保.md "wikilink")12年）
4.  [信弘](../Page/真田信弘.md "wikilink")（のぶひろ）〔從五位下、伊豆守〕　1727年（享保12年）－1736年（[元文元年](../Page/元文.md "wikilink")）
5.  [信安](../Page/真田信安.md "wikilink")（のぶやす）〔從五位下、伊豆守〕　1737年（元文2年）－1752年（[宝历](../Page/宝历.md "wikilink")2年）
6.  [幸弘](../Page/真田幸弘.md "wikilink")（ゆきひろ）〔從四位下、右京大夫〕　1752年（宝历2年）－1798年（[宽政](../Page/宽政.md "wikilink")10年）
7.  [幸専](../Page/真田幸専.md "wikilink")（ゆきたか）〔從四位下、弾正大弼〕　1798年（寛政10年）－1823年（[文政](../Page/文政.md "wikilink")6年）
8.  [幸貫](../Page/真田幸貫.md "wikilink")（ゆきつら）〔從四位下、右京大夫〕　1823年（文政6年）－1852年（[嘉永](../Page/嘉永.md "wikilink")5年），1841年（[天保](../Page/天保.md "wikilink")12年）－1844年（[弘化元年](../Page/弘化.md "wikilink")）曾担任[老中](../Page/老中.md "wikilink")
9.  [幸教](../Page/真田幸教.md "wikilink")（ゆきのり）〔從四位下、右京大夫〕　1852年（[嘉永](../Page/嘉永.md "wikilink")5年）－1866年（[庆应](../Page/庆应.md "wikilink")2年）
10. [幸民](../Page/真田幸民.md "wikilink")（ゆきたみ）〔從二位、信濃守〕　1866年（庆应2年）－1869年（[明治](../Page/明治.md "wikilink")2年：[版籍奉還](../Page/版籍奉還.md "wikilink")）

[Category:松代藩](../Category/松代藩.md "wikilink")
[Category:森氏](../Category/森氏.md "wikilink")
[Category:長澤松平氏](../Category/長澤松平氏.md "wikilink")
[Category:福井松平氏](../Category/福井松平氏.md "wikilink")
[Category:酒井氏](../Category/酒井氏.md "wikilink")
[Category:真田氏](../Category/真田氏.md "wikilink")
[Category:藩](../Category/藩.md "wikilink")