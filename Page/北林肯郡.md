**北林肯郡**（North
Lincolnshire），[英國](../Page/英國.md "wikilink")[英格蘭](../Page/英格蘭.md "wikilink")[約克郡-亨伯](../Page/約克郡-亨伯.md "wikilink")[林肯郡的](../Page/林肯郡.md "wikilink")[Borough](../Page/英國的自治市鎮.md "wikilink")，英格蘭的[單一管理區](../Page/單一管理區.md "wikilink")、[人口](../Page/人口.md "wikilink")159,000，[面積](../Page/面積.md "wikilink")846.31平方公里。行政總部位於[斯肯索普](../Page/斯肯索普.md "wikilink")（Scunthorpe），以南234公里（145英里）達英國首都[倫敦](../Page/倫敦.md "wikilink")（[西敏宮](../Page/西敏宮.md "wikilink")），以南39公里（24英里）達林肯郡[郡治](../Page/郡治.md "wikilink")[林肯](../Page/林肯_\(林肯郡\).md "wikilink")。

北林肯郡東臨[北海](../Page/北海_\(大西洋\).md "wikilink")，南與[林肯郡的](../Page/林肯郡.md "wikilink")[西林齊區相鄰](../Page/西林齊區.md "wikilink")，東南與[東北林肯郡相鄰](../Page/東北林肯郡.md "wikilink")，西與[南約克郡的](../Page/南約克郡.md "wikilink")[羅瑟漢姆區相鄰](../Page/羅瑟漢姆區.md "wikilink")，北與[赫爾河畔京士頓和](../Page/赫爾河畔京士頓.md "wikilink")[東約克郡相鄰](../Page/東約克郡.md "wikilink")。

## 地方沿革

1996年4月1日前，「北林肯郡」這名稱並不存在，以下以「北林肯地區」稱之。

《[1888年地方政府法案](../Page/1888年地方政府法案.md "wikilink")》在1889年4月1日生效，北林肯地區納入[林齊郡](../Page/林齊郡.md "wikilink")（Lindsey）最北部，北鄰[約克郡](../Page/約克郡.md "wikilink")。《[1972年地方政府法案](../Page/1972年地方政府法案.md "wikilink")》在1974年4月1日生效，林齊郡被廢除，北林肯地區改歸[亨伯賽德郡管理](../Page/亨伯賽德郡.md "wikilink")。亨伯賽德郡下轄9個[非都市區](../Page/非都市區.md "wikilink")（non-metropolitan
district）。1992年成立的[英格蘭地方政府委員會](../Page/英格蘭地方政府委員會_\(1992年\).md "wikilink")（Local
Government Commission for
England）建議廢除亨伯賽德郡，把其中3個非都市區─[格蘭福德區](../Page/格蘭福德區.md "wikilink")（Glanford）、[斯肯索普區](../Page/斯肯索普.md "wikilink")、[布斯弗里區](../Page/布斯弗里區.md "wikilink")（Boothferry）合併成全新的[單一管理區](../Page/單一管理區.md "wikilink")「北林肯郡」，在1996年4月1日生效至今。1998年，北林肯郡獲[Borough地位](../Page/英國的自治市鎮.md "wikilink")。

[林肯郡既是](../Page/林肯郡.md "wikilink")[非都市郡](../Page/非都市郡.md "wikilink")，又是[名譽郡](../Page/名譽郡.md "wikilink")，而北林肯郡是[單一管理區](../Page/單一管理區.md "wikilink")。如將林肯郡看待為[非都市郡](../Page/非都市郡.md "wikilink")，北林肯郡不屬它的一部分；如將林肯郡看待為[名譽郡](../Page/名譽郡.md "wikilink")，北林肯郡行政上仍屬它的一部分。

## 政治

2005年5月5日舉行[2005年英國大選後](../Page/2005年英國大選.md "wikilink")，[Ian
Cawsey](../Page/Ian_Cawsey.md "wikilink")、[Elliot
Morley分別當選為北林肯郡](../Page/Elliot_Morley.md "wikilink")[議員](../Page/議員.md "wikilink")，兩人同屬[工黨](../Page/工黨_\(英國\).md "wikilink")。

## 交通

北林肯郡的公路共計長800英里，行人道共計長1,000英里，街燈有22,000枝\[1\]。

## 地理

[Lincolnshire_Ceremonial_Numbered.png](https://zh.wikipedia.org/wiki/File:Lincolnshire_Ceremonial_Numbered.png "fig:Lincolnshire_Ceremonial_Numbered.png")行政區劃圖：
1\. [林肯](../Page/林肯_\(林肯郡\).md "wikilink")
2\. [北凱斯蒂文區](../Page/北凱斯蒂文區.md "wikilink")
3\. [南凱斯蒂文區](../Page/南凱斯蒂文區.md "wikilink")
4\. [南霍蘭區](../Page/南霍蘭區.md "wikilink")
5\. [波士頓區](../Page/波士頓區.md "wikilink")
6\. [東林齊區](../Page/東林齊區.md "wikilink")
7\. [西林齊區](../Page/西林齊區.md "wikilink")
'''8. **北林肯郡**
9\. [東北林肯郡](../Page/東北林肯郡.md "wikilink") \]\]
北林肯郡東臨[北海](../Page/北海_\(大西洋\).md "wikilink")，南與[林肯郡的](../Page/林肯郡.md "wikilink")[西林齊區相鄰](../Page/西林齊區.md "wikilink")，東南與[東北林肯郡相鄰](../Page/東北林肯郡.md "wikilink")，西與[南約克郡的](../Page/南約克郡.md "wikilink")[羅瑟漢姆區相鄰](../Page/羅瑟漢姆區.md "wikilink")，北與[赫爾河畔京士頓和](../Page/赫爾河畔京士頓.md "wikilink")[東約克郡相鄰](../Page/東約克郡.md "wikilink")。

### 附近城市

以下列出北林肯郡行政總部[斯肯索普附近的大城市](../Page/斯肯索普.md "wikilink")（[方位以斯肯索普為量度點](../Page/方位.md "wikilink")）：

| 城市                                                               | 方位   | 距離（公里 / 英里） | 備註     |
| ---------------------------------------------------------------- | ---- | ----------- | ------ |
| [倫敦](../Page/倫敦.md "wikilink")（[西敏宮](../Page/西敏宮.md "wikilink")） | 南    | 234 km      | 145 mi |
| [林肯](../Page/林肯_\(林肯郡\).md "wikilink")                           | 南    | 39 km       | 24 mi  |
| [約克](../Page/約克.md "wikilink")                                   | 西北   | 51 km       | 32 mi  |
| [列斯](../Page/列斯.md "wikilink")                                   | 西北   | 64 km       | 40 mi  |
| [設菲爾德](../Page/設菲爾德.md "wikilink")                               | 西南   | 58 km       | 36 mi  |
| [唐卡斯特](../Page/唐卡斯特.md "wikilink")                               | 西南偏西 | 32 km       | 20 mi  |
| [羅瑟代姆](../Page/羅瑟代姆.md "wikilink")                               | 西南   | 49 km       | 31 mi  |
| [格里姆斯比](../Page/格里姆斯比.md "wikilink")                             | 東    | 38 km       | 24 mi  |

## 圖片集

|                                                                                                                                                |                                                                                                                                                                                                            |
| ---------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
|                                                                                                                                                | [Mount_Pleasant_Mill,_Kirton_in_Lindsey.jpg](https://zh.wikipedia.org/wiki/File:Mount_Pleasant_Mill,_Kirton_in_Lindsey.jpg "fig:Mount_Pleasant_Mill,_Kirton_in_Lindsey.jpg")（Mount Pleasant Mill）\]\] |
| [Thornton_Abbey_Ruins.jpg](https://zh.wikipedia.org/wiki/File:Thornton_Abbey_Ruins.jpg "fig:Thornton_Abbey_Ruins.jpg")（Thornton Abbey）遺蹟\]\] | [Humber_Bridge.png](https://zh.wikipedia.org/wiki/File:Humber_Bridge.png "fig:Humber_Bridge.png")\]\]                                                                                                     |

## 註釋

[Category:英格蘭的單一管理區](../Category/英格蘭的單一管理區.md "wikilink")
[Category:林肯郡](../Category/林肯郡.md "wikilink")

1.  北林肯郡議會：[交通和街道](http://www.northlincs.gov.uk/NorthLincs/Transportandstreets/)