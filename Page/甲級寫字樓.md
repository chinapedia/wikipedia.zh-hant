[Hong_Kong_Island_Skyline_2009.jpg](https://zh.wikipedia.org/wiki/File:Hong_Kong_Island_Skyline_2009.jpg "fig:Hong_Kong_Island_Skyline_2009.jpg")的頂級商辦大樓群之一的[中環](../Page/中環.md "wikilink")\]\]

**頂級商辦大樓**又稱為**A級商辦大樓**，是[辦公大樓之中級數最高的](../Page/辦公大樓.md "wikilink")[商業辦公建築](../Page/商業.md "wikilink")，甚至有些為「智能辦公室」（智慧型辦公大樓）。頂級商辦大樓在結構[建築學的](../Page/建築學.md "wikilink")[設計和](../Page/設計.md "wikilink")[視覺上都屬優質](../Page/視覺.md "wikilink")，多數都成為當地的[地標](../Page/地標.md "wikilink")，租戶置身其中富有成功感和力量。

[深圳“京基100”摩天大楼.jpg](https://zh.wikipedia.org/wiki/File:深圳“京基100”摩天大楼.jpg "fig:深圳“京基100”摩天大楼.jpg")的[京基100](../Page/京基100.md "wikilink")\]\]
[Taipei101.portrait.altonthompson.jpg](https://zh.wikipedia.org/wiki/File:Taipei101.portrait.altonthompson.jpg "fig:Taipei101.portrait.altonthompson.jpg")\]\]
[Kaohsiung_85_Sky_Tower_01.jpg](https://zh.wikipedia.org/wiki/File:Kaohsiung_85_Sky_Tower_01.jpg "fig:Kaohsiung_85_Sky_Tower_01.jpg")\]\]

## 評定標準

[香港](../Page/香港.md "wikilink")[差餉物業估價署將私人寫字樓分為甲](../Page/差餉物業估價署.md "wikilink")、乙、丙三級，只會按物業質素評級，不會考慮其所在地點，而甲級寫字樓具備以下元素\[1\]：

  - 新型及裝修上乘
  - 間隔具彈性
  - 整層樓面面積廣闊
  - 大堂與通道裝潢講究及寬敞
  - 中央[空氣調節系統完善](../Page/空氣調節.md "wikilink")
  - 設有良好的載客及載貨[升降機設備](../Page/升降機.md "wikilink")
  - 設有電梯操作員
  - 專業管理
  - 有停車設施

[戴德梁行認為](../Page/戴德梁行.md "wikilink")，甲級寫字樓的內涵，包括有如下元素：

  - 位置於[商業中心區](../Page/商業中心區.md "wikilink")（[CBD](../Page/CBD.md "wikilink")）內
  - 有完善的[中央空調](../Page/中央空調.md "wikilink")
  - 充足數量及合理分布的升降機和[電梯服務](../Page/電梯.md "wikilink")
  - 甲級專業[物業管理](../Page/物業管理.md "wikilink")
  - 樓層間格等恰當
  - [樓底淨高在](../Page/樓底.md "wikilink")2.8米
  - 每層[建築面積最少](../Page/建築面積.md "wikilink")1千平方米
  - 有零煩惱的[停車場](../Page/停車場.md "wikilink")

## 基本設施

  - [自來水](../Page/自來水.md "wikilink")
  - [洗手間](../Page/洗手間.md "wikilink")
  - [電力](../Page/電力.md "wikilink")（必須通過整間辦公室，並且有許多分散的插座）
  - 照明系統
  - [私用自動交換分機](../Page/私用自動交換分機.md "wikilink")（PABX）
  - 連結至當地電信公司的[光纖連線](../Page/光纖.md "wikilink")
  - 用於[寬頻](../Page/寬頻.md "wikilink")[網路和](../Page/網路.md "wikilink")[電話等通訊的](../Page/電話.md "wikilink")[同軸電纜](../Page/同軸電纜.md "wikilink")
  - [停車場](../Page/停車場.md "wikilink")

## 著名建物

  - [香港國際金融中心](../Page/香港國際金融中心.md "wikilink")
  - [澳門財富中心](../Page/澳門財富中心.md "wikilink") Finance and IT Center of
    Macau
  - [廣州西塔](../Page/廣州國際金融中心.md "wikilink")
  - [上海中心](../Page/上海中心.md "wikilink")
  - [北京國貿](../Page/中國國際貿易中心.md "wikilink")
  - [香港環球貿易廣場](../Page/香港環球貿易廣場.md "wikilink")
  - [台北101](../Page/台北101.md "wikilink")
  - [新光人壽保險摩天大樓](../Page/新光人壽保險摩天大樓.md "wikilink")
  - [高雄85大樓](../Page/高雄85大樓.md "wikilink")
  - [帝國大廈](../Page/帝國大廈.md "wikilink")
  - [雙峰塔](../Page/雙峰塔.md "wikilink")

## 参考文献

### 引用

### 来源

  - [差餉物業估價署](../Page/差餉物業估價署.md "wikilink")[《香港物業報告2008》甲級寫字樓的定義](https://web.archive.org/web/20090320054251/http://www.rvd.gov.hk/tc/doc/hkpr08/04.pdf)

## 参见

  - [不動產投資信託](../Page/不動產投資信託.md "wikilink")

[Category:建築物类型](../Category/建築物类型.md "wikilink")
[Category:香港写字楼](../Category/香港写字楼.md "wikilink")

1.  香港差餉物業估價署：[技術附註](http://www.rvd.gov.hk/tc/doc/statistics/15_technotes.pdf)