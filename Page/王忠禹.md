**王忠禹**（），[吉林](../Page/吉林.md "wikilink")[长春人](../Page/长春.md "wikilink")；[中共中央党校大专学历](../Page/中共中央党校.md "wikilink")，高级工程师。1956年5月加入[中国共产党](../Page/中国共产党.md "wikilink")，曾是中共第十三届中央候补委员，第十四、十五届[中央委员会委员](../Page/中央委员会.md "wikilink")，第十届[全国政协副主席](../Page/全国政协.md "wikilink")，[国务委员兼](../Page/国务委员.md "wikilink")[国务院秘书长](../Page/国务院秘书长.md "wikilink")。现任[中国企业联合会会长](../Page/中国企业联合会.md "wikilink")。

## 简历

1953年，王忠禹毕业于[沈阳轻工业高级职业学校制浆](../Page/沈阳.md "wikilink")（现今青岛科技大学）[造纸专业](../Page/造纸.md "wikilink")；随后进入吉林[造纸厂](../Page/造纸厂.md "wikilink")。在这家工厂，王忠禹曾前后工作了近三十年；并历任吉林造纸厂车间技术员、[车间副主任](../Page/车间.md "wikilink")、厂长室秘书、厂工程师室工程师、厂计划科副科长等职。“[文化大革命](../Page/文化大革命.md "wikilink")”期间，王忠禹曾受冲击，后下放车间和农场劳动。1970年恢复工作后，他回到吉林造纸厂，任生产调度组副组长；而后，又历任厂党委办公室副主任、主任，厂革委会副主任、副厂长、总工程师等职。1980年，王忠禹调任[吉林省第一](../Page/吉林省.md "wikilink")[轻工业局](../Page/轻工业.md "wikilink")，出任副局长。不久，他就被安排到[中共中央党校培训部学习](../Page/中共中央党校.md "wikilink")。学习结束后，王忠禹即被提升为吉林省第一轻工业厅厅长。1983年后，他又历任[中共吉林省委常委](../Page/中共吉林省委.md "wikilink")、副书记，[吉林省人民政府副省长](../Page/吉林省人民政府.md "wikilink")、省长等职。

1992年，王忠禹被调入中央工作。先后担任过国务院生产办公室副主任，国务院经济贸易办公室副主任，国家经济贸易委员会主任职务。1998年3月，在[朱镕基](../Page/朱镕基.md "wikilink")“内阁”中，王忠禹被任命为[国务委员兼](../Page/国务委员.md "wikilink")[国务院秘书长](../Page/国务院秘书长.md "wikilink")、[国家行政学院院长等职](../Page/国家行政学院.md "wikilink")。

2003年3月，王忠禹届满离职。随即，他又当选为第十届[全国政协副主席](../Page/全国政协副主席.md "wikilink")（排名第一位）。2008年，退休后的王忠禹，又当选[中国企业联合会会长](../Page/中国企业联合会.md "wikilink")。

## 外部链接

  - [王忠禹简历](http://news.xinhuanet.com/ziliao/2002-01/21/content_246385.htm)
    [新华网](../Page/新华网.md "wikilink")

{{-}}

[Z](../Category/王姓.md "wikilink") [W王](../Category/长春人.md "wikilink")
[Category:国务委员
(1998-2003)](../Category/国务委员_\(1998-2003\).md "wikilink")
[Category:中华人民共和国国务院秘书长](../Category/中华人民共和国国务院秘书长.md "wikilink")
[Category:国家行政学院院长](../Category/国家行政学院院长.md "wikilink")
[Category:第十届全国政协副主席](../Category/第十届全国政协副主席.md "wikilink")
[Category:中华人民共和国吉林省省长](../Category/中华人民共和国吉林省省长.md "wikilink")
[Category:中华人民共和国国家经济贸易委员会主任](../Category/中华人民共和国国家经济贸易委员会主任.md "wikilink")