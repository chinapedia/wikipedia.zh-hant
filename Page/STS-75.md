****是历史上第七十四次航天飞机任务，也是[哥伦比亚号航天飞机的第十六次太空飞行](../Page/哥倫比亞號太空梭.md "wikilink")。

## 任务成员

  - **[安德鲁·艾伦](../Page/安德鲁·艾伦.md "wikilink")**（，曾执行、以及任务），指令长
  - **[斯科特·赫罗威兹](../Page/斯科特·赫罗威兹.md "wikilink")**（，曾执行、、以及任务），飞行员
  - **[杰弗利·霍夫曼](../Page/杰弗利·霍夫曼.md "wikilink")**（，曾执行、、、以及任务），任务专家
  - **[毛里齐奥·切利](../Page/毛里齐奥·切利.md "wikilink")**（，[意大利宇航员](../Page/意大利.md "wikilink")，曾执行任务），任务专家
  - **[克劳德·尼克列](../Page/克劳德·尼克列.md "wikilink")**（，[瑞士宇航员](../Page/瑞士.md "wikilink")，曾执行、、以及任务），任务专家
  - **[张福林](../Page/張福林.md "wikilink")**（，曾执行、、、、、以及任务），任务专家／有效载荷指令长
  - **[恩贝托·圭多尼](../Page/恩贝托·圭多尼.md "wikilink")**（，[意大利宇航员](../Page/意大利.md "wikilink")，曾执行以及任务），有效载荷专家

[Category:1996年佛罗里达州](../Category/1996年佛罗里达州.md "wikilink")
[Category:1996年科学](../Category/1996年科学.md "wikilink")
[Category:哥伦比亚号航天飞机任务](../Category/哥伦比亚号航天飞机任务.md "wikilink")
[Category:1996年2月](../Category/1996年2月.md "wikilink")
[Category:1996年3月](../Category/1996年3月.md "wikilink")