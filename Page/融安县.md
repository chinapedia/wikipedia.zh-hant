**融安县**位于[中国](../Page/中国.md "wikilink")[广西壮族自治区北部](../Page/广西壮族自治区.md "wikilink")，是[柳州市所辖的一个](../Page/柳州市.md "wikilink")[县](../Page/县.md "wikilink")。

## 行政区划

辖6个镇，6个乡:

  - 镇：长安镇、泗顶镇、大良镇、浮石镇、板榄镇、大将镇
  - 乡：雅瑶乡、大坡乡、桥板乡、沙子乡、东起乡、潭头乡

## 交通

融安县有[209国道](../Page/209国道.md "wikilink")、[融江贯穿城区](../Page/融江.md "wikilink")，除此之外还有[焦柳铁路经过](../Page/焦柳铁路.md "wikilink")。

## 旅游

融安县的主要旅游景点有：长安骑楼街，红茶沟国家森林公园，石门仙湖景区，大洲旅游休闲岛，鹭鹚洲休闲旅游岛等。

## 特产

融安县有金桔，滤粉等特产。

## 参考文献

<references/>

## 外部链接

  - [柳州市融安县简介](http://www.gx.xinhua.org/dtzx/rongan/index.htm)

[融安县](../Category/融安县.md "wikilink") [县](../Category/柳州区县.md "wikilink")
[柳州](../Category/广西县份.md "wikilink")