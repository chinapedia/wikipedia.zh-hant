**IF弗格萊夫喬德**（**ÍF
Fuglafjørður**）是[法罗群岛的足球俱乐部](../Page/法罗群岛.md "wikilink")，位于[弗格莱夫乔德](../Page/弗格莱夫乔德.md "wikilink")，球队的主体育场为弗格莱夫乔德体育场（Fuglafjørdur
Stadium）。

球队在2006赛季中降入了法罗群岛的第二级联赛。

## 球队荣誉

  - **[法罗群岛足球甲级联赛](../Page/法罗群岛足球甲级联赛.md "wikilink")：1次**
      - 1979年

## 现有阵容

2006//07年

## 外部链接

  - [Official Website (Faroes)](http://www.if.fo/)

[Category:法罗群岛足球俱乐部](../Category/法罗群岛足球俱乐部.md "wikilink")