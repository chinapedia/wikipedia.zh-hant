《**幽靈終結者2007**》（**The
Hitcher**）是2007年重新拍攝1986年同樣名字的恐怖片。影片設定為[R級血腥暴力](../Page/R.md "wikilink")、恐怖和語言。電影原本計劃在2007年4月13日上影，但由於早期製作關係提早在2007年2月9日，再提前在2007年1月19日上影。

影片預定在[英國](../Page/英國.md "wikilink")2007年4月9日上影，但被延遲到2007年6月22日。

[T](../Category/2007年電影.md "wikilink")
[T](../Category/新墨西哥州取景電影.md "wikilink")
[T](../Category/美國驚悚片.md "wikilink") [T](../Category/英語電影.md "wikilink")
[T](../Category/2000年代驚悚片.md "wikilink")