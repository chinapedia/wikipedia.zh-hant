## 大事记

  - [2月16日](../Page/2月16日.md "wikilink")，[袁世凯批准凌福彭关于开办工艺学堂的暂行章程及申请经费的禀文](../Page/袁世凯.md "wikilink")，设立北洋工艺学堂（今[河北工业大学前身](../Page/河北工业大学.md "wikilink")）。
  - [6月16日](../Page/6月16日.md "wikilink")，[亨利·福特成立汽车公司](../Page/亨利·福特.md "wikilink")。
  - [6月25日](../Page/6月25日.md "wikilink")，[物理学家](../Page/物理学家.md "wikilink")[居里夫人发现了](../Page/玛丽亚·居里.md "wikilink")[镭](../Page/镭.md "wikilink")。
  - [7月4日](../Page/7月4日.md "wikilink")，[太平洋](../Page/太平洋.md "wikilink")[海底电缆投入使用](../Page/海底电缆.md "wikilink")。
  - [11月18日](../Page/11月18日.md "wikilink")，[美国与](../Page/美國.md "wikilink")[巴拿马签订](../Page/巴拿马.md "wikilink")[美巴条约](../Page/美巴条约.md "wikilink")（Hay-Herran
    Treaty）。
  - [12月17日](../Page/12月17日.md "wikilink")，[美国的](../Page/美國.md "wikilink")[莱特兄弟乘Flyer](../Page/莱特兄弟.md "wikilink")[飞机完成](../Page/飞机.md "wikilink")[人类首次](../Page/人.md "wikilink")[飞行](../Page/飞行.md "wikilink")。
  - [康斯坦丁·齐奥尔科夫斯基最著名的作品](../Page/康斯坦丁·齐奥尔科夫斯基.md "wikilink")《利用反作用力设施探索宇宙空间》出版，是第一部从理论上论证火箭作用的论文。

## 出生

  - [1月2日](../Page/1月2日.md "wikilink")——[田中加子](../Page/田中加子.md "wikilink")，日本最長壽女性人瑞
  - [2月4日](../Page/2月4日.md "wikilink")——[亞歷山大·伊米奇](../Page/亞歷山大·伊米奇.md "wikilink")，美國最長壽男性人瑞（[2014年逝世](../Page/2014年.md "wikilink")）
  - [2月5日](../Page/2月5日.md "wikilink")——[百井盛](../Page/百井盛.md "wikilink")，世界男性最年長者（[2015年逝世](../Page/2015年.md "wikilink")）
  - [2月6日](../Page/2月6日.md "wikilink")——[克劳迪奥·阿劳](../Page/克劳迪奥·阿劳.md "wikilink")，[智利](../Page/智利.md "wikilink")[鋼琴家](../Page/钢琴家.md "wikilink")（[1991年逝世](../Page/1991年.md "wikilink")）
  - [2月8日](../Page/2月8日.md "wikilink")——[東姑阿都拉曼](../Page/東姑阿都拉曼.md "wikilink")，[马来西亚的国父](../Page/马来西亚.md "wikilink")（[1990年逝世](../Page/1990年.md "wikilink")）
  - [2月27日](../Page/2月27日.md "wikilink")——[蔣法賢](../Page/蔣法賢.md "wikilink")，[香港](../Page/香港.md "wikilink")[醫生](../Page/醫生.md "wikilink")（逝世[1974年](../Page/1974年.md "wikilink")）
  - [6月25日](../Page/6月25日.md "wikilink")——[乔治·奥威尔](../Page/乔治·奥威尔.md "wikilink")，[英属印度](../Page/英属印度.md "wikilink")[比哈尔邦](../Page/比哈尔邦.md "wikilink")[作家](../Page/作家.md "wikilink")、[小说家](../Page/小說家_\(職業\).md "wikilink")、[记者](../Page/记者.md "wikilink")、[社会评论家](../Page/社会评论家.md "wikilink")（[1950年逝世](../Page/1950年.md "wikilink")）
  - [7月2日](../Page/7月2日.md "wikilink")——[杜嘉菱](../Page/亚历克·道格拉斯-休姆.md "wikilink")，前[英國首相](../Page/英国首相.md "wikilink")（[1995年逝世](../Page/1995年.md "wikilink")）
  - [7月2日](../Page/7月2日.md "wikilink")——[奧拉夫五世](../Page/奧拉夫五世.md "wikilink")，[挪威國王](../Page/挪威.md "wikilink")（[1991年逝世](../Page/1991年.md "wikilink")）
  - [7月10日](../Page/7月10日.md "wikilink")——[鄧律敦治](../Page/鄧律敦治.md "wikilink")，[香港商人](../Page/香港.md "wikilink")、慈善家及政治家（[1974年逝世](../Page/1974年.md "wikilink")）
  - [8月17日](../Page/8月17日.md "wikilink")——，美國在世最長壽者（[2018年逝世](../Page/2018年.md "wikilink")）
  - [8月21日](../Page/8月21日.md "wikilink")——[王少清](../Page/王少清.md "wikilink")，[香港商人及慈善家](../Page/香港.md "wikilink")（[1984年逝世](../Page/1984年.md "wikilink")）
  - [10月13日](../Page/10月13日.md "wikilink")——[小林多喜二](../Page/小林多喜二.md "wikilink")，[日本](../Page/日本.md "wikilink")[作家](../Page/作家.md "wikilink")（[1933年逝世](../Page/1933年.md "wikilink")）
  - [11月4日](../Page/11月4日.md "wikilink")——[倪柝声](../Page/倪柝声.md "wikilink")，[中国](../Page/中国.md "wikilink")[基督徒领袖](../Page/基督徒.md "wikilink")（[1972年逝世](../Page/1972年.md "wikilink")）
  - [12月4日](../Page/12月4日.md "wikilink")——[亞瑟·馬歇爾爵士](../Page/亞瑟·馬歇爾.md "wikilink")，[英國飛機工程師](../Page/英国.md "wikilink")（逝世[2007年](../Page/2007年.md "wikilink")）
  - [12月12日](../Page/12月12日.md "wikilink")——[小津安二郎](../Page/小津安二郎.md "wikilink")，日本[電影導演](../Page/電影導演.md "wikilink")（[2007年逝世](../Page/2007年.md "wikilink")）
  - [12月28日](../Page/12月28日.md "wikilink")——[馮·諾伊曼](../Page/馮·諾伊曼.md "wikilink")，電腦之父，[匈牙利裔](../Page/匈牙利.md "wikilink")[美國數學家](../Page/美國.md "wikilink")（逝世[1957年](../Page/1957年.md "wikilink")）

## 逝世

  - [4月11日](../Page/4月11日.md "wikilink")──[榮祿](../Page/荣禄.md "wikilink")，晚[清政治家](../Page/清朝.md "wikilink")。
  - [5月9日](../Page/5月9日.md "wikilink")——[保羅·高更](../Page/保羅·高更.md "wikilink")。法國[印象派畫家](../Page/印象派.md "wikilink")（[1848年出生](../Page/1848年.md "wikilink")）
  - [11月13日](../Page/11月13日.md "wikilink")——[卡米耶·畢沙羅](../Page/卡米耶·畢沙羅.md "wikilink")，[法國](../Page/法国.md "wikilink")[印象派](../Page/印象派.md "wikilink")[畫家](../Page/畫家.md "wikilink")。（[1830年出生](../Page/1830年.md "wikilink")）
  - [清軍將領](../Page/清軍.md "wikilink")[馮子才](../Page/馮子才.md "wikilink")。

## 诺贝尔奖

  - [物理](../Page/诺贝尔物理学奖.md "wikilink")：[亨利·貝克勒](../Page/亨利·貝克勒.md "wikilink")、[皮埃爾·居里](../Page/皮埃爾·居里.md "wikilink")、[瑪麗·居禮](../Page/瑪麗·居禮.md "wikilink")
  - [化学](../Page/诺贝尔化学奖.md "wikilink")：[阿伦尼乌斯](../Page/阿伦尼乌斯.md "wikilink")
  - [生理和医学](../Page/诺贝尔生理学或医学奖.md "wikilink")：[尼爾斯·呂貝里·芬森](../Page/尼爾斯·呂貝里·芬森.md "wikilink")
  - [文学](../Page/诺贝尔文学奖.md "wikilink")：[比約恩斯徹納·比昂松](../Page/比約恩斯徹納·比昂松.md "wikilink")
  - [和平](../Page/诺贝尔和平奖.md "wikilink")：[威廉·蘭德爾·克里默](../Page/威廉·蘭德爾·克里默.md "wikilink")

[\*](../Category/1903年.md "wikilink")
[3年](../Category/1900年代.md "wikilink")
[0](../Category/20世纪各年.md "wikilink")