**氟磺酸甲酯**，谑称为**魔幻甲基**（Methyl
magic），化学式为F-SO<sub>2</sub>-OCH<sub>3</sub>，是很强的[甲基化试剂](../Page/甲基化.md "wikilink")，比[碘甲烷](../Page/碘甲烷.md "wikilink")、[硫酸二甲酯要强约](../Page/硫酸二甲酯.md "wikilink")10<sup>4</sup>倍。

氟磺酸甲酯可由等物质的量的[氟磺酸与](../Page/氟磺酸.md "wikilink")[硫酸二甲酯反应蒸馏制得](../Page/硫酸二甲酯.md "wikilink")。它与[三氟甲磺酸甲酯都具有非常高的毒性](../Page/三氟甲磺酸甲酯.md "wikilink")（LD50(鼠)
\~ 5
ppm），会对呼吸系统造成刺激，引起[肺水肿](../Page/肺水肿.md "wikilink")。原理可能是对细胞膜中[脂类的烷基化作用](../Page/脂类.md "wikilink")。\[1\]

## 参见

  - [氟磺酸](../Page/氟磺酸.md "wikilink")
  - [三氟甲磺酸甲酯](../Page/三氟甲磺酸甲酯.md "wikilink")

## 参考资料

[Category:甲基化试剂](../Category/甲基化试剂.md "wikilink")
[Category:甲酯](../Category/甲酯.md "wikilink")
[Category:磺酸酯](../Category/磺酸酯.md "wikilink")

1.