**鼻肌**（**鼻孔壓肌**）是人體[鼻部一塊](../Page/鼻部.md "wikilink")[括約肌狀的肌肉](../Page/括約肌.md "wikilink")，作用為壓下[鼻軟骨](../Page/鼻軟骨.md "wikilink")。

鼻肌包含兩部分：“橫部”（transverse）和“翼部”（alar）。

  - “橫部”

<!-- end list -->

  - “翼部”

某些資料列其為“[鼻孔壓肌](../Page/鼻孔壓肌.md "wikilink")”和“[鼻孔開大肌](../Page/鼻孔開大肌.md "wikilink")”兩塊肌肉。\[1\]

## 圖像

[`File:Gray157.png|Left`](File:Gray157.png%7CLeft)` maxilla. Outer surface.`

## 參考資料

<references/>

## 外部連結

  - [解剖學(Anatomy)-肌肉(muscle)-Muscles of Facial
    Expression(顏面表情肌)](http://smallcollation.blogspot.com/2013/02/anatomy-muscle-muscles-of-facial.html)

  -
  - [Interactive diagram at
    ivy-rose.co.uk](http://www.ivy-rose.co.uk/References/glossary_entry144.htm)

[Category:头颈肌肉](../Category/头颈肌肉.md "wikilink")

1.