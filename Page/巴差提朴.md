[Rama_7_in_stamp.jpg](https://zh.wikipedia.org/wiki/File:Rama_7_in_stamp.jpg "fig:Rama_7_in_stamp.jpg")\]\]

**巴差提朴**\[1\]（；）即**帕·巴·頌德·帕·博拉明·瑪哈·巴差提朴·帕·博告·昭育霍**（）是[暹罗](../Page/暹罗.md "wikilink")（[泰國](../Page/泰國.md "wikilink")）[扎克里王朝第七位国王](../Page/扎克里王朝.md "wikilink")，1925年至1935年在位，亦稱**[拉玛七世](../Page/拉玛_\(泰国国王\).md "wikilink")**（Rama
VII）。

1932年夏天發生[革命](../Page/暹羅立憲革命.md "wikilink")，實行君主立憲制，他成為名義上的君主，這次事件宣告了[拉達那哥欣時期的終結](../Page/拉達那哥欣王國.md "wikilink")。1935年退位，由其姪[阿南塔·瑪希敦繼位](../Page/阿南塔·瑪希敦.md "wikilink")。後移居[英國](../Page/英國.md "wikilink")。

## 簡介

[Royal_portraits_-_004_02.jpg](https://zh.wikipedia.org/wiki/File:Royal_portraits_-_004_02.jpg "fig:Royal_portraits_-_004_02.jpg")

巴差提朴是[瓦栖拉兀的弟弟](../Page/瓦栖拉兀.md "wikilink")，在当时的王位继承顺序中本来其实排第二位。由于[王储](../Page/王储.md "wikilink")在1925年2月意外早逝，他于1925年11月在并未准备充分的情况下登上了王位。即位不久就遇上世界经济危机（[大萧条](../Page/大萧条.md "wikilink")），暹罗亦未能幸免。这位从小在欧洲受教育长大的年轻国王出于节省开支的初衷，解雇了大量宫中劳力。这却引起了普遍的不满，再加上当时糟糕的经济形势，最终导致了[1932年夏天的革命](../Page/暹羅立憲革命.md "wikilink")，成立了[君主立宪制政府](../Page/君主立宪制.md "wikilink")。新政府给了国王两个选择：把权力交给[议会继续作国王](../Page/议会.md "wikilink")（君主立宪）或者直接退位。巴差提朴选择了保留王位。他当时做了以下著名的答复：“为了使成立君主立宪政府的过程能够尽可能柔和地进行，我同意成为一个傀儡。”新宪法最终于1932年12月10日颁布。

1935年3月2日，新政府几乎完全违反了民主化的初衷，而他则感到自己对此已几乎无能为力。同意成為傀儡的巴差提朴還是退位了，他最终选择[禪位給他的侄子](../Page/禪位.md "wikilink")[阿南塔瑪希敦](../Page/阿南塔瑪希敦.md "wikilink")。後來他以養病为由，移居[英国](../Page/英国.md "wikilink")，并于[第二次世界大战期间卒於該地](../Page/第二次世界大战.md "wikilink")。

## 參考

[Category:泰国国王](../Category/泰国国王.md "wikilink")
[Category:扎克里王朝](../Category/扎克里王朝.md "wikilink")
[Category:伊頓公學校友](../Category/伊頓公學校友.md "wikilink")

1.