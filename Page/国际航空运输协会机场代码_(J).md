| 代碼  | 機場                                                     | 城市                                             | 国家和地区                                                    |
| --- | ------------------------------------------------------ | ---------------------------------------------- | -------------------------------------------------------- |
| JAA | [賈拉拉巴德機場](../Page/賈拉拉巴德機場.md "wikilink")               | [賈拉拉巴德](../Page/賈拉拉巴德_\(阿富汗\).md "wikilink")   | [阿富汗](../Page/阿富汗.md "wikilink")                         |
| JAI | [齋浦爾機場](../Page/齋浦爾機場.md "wikilink")                   | [齋浦爾](../Page/齋浦爾.md "wikilink")               | [印度](../Page/印度.md "wikilink")                           |
| JAX | [傑克遜維爾國際機場](../Page/傑克遜維爾國際機場.md "wikilink")           | [傑克遜維爾](../Page/傑克遜維爾_\(佛羅里達州\).md "wikilink") | [美國](../Page/美國.md "wikilink")                           |
| JBB | [诺托哈丁格罗机场](../Page/诺托哈丁格罗机场.md "wikilink")             | [任抹](../Page/任抹.md "wikilink")                 | [印尼](../Page/印尼.md "wikilink")                           |
| JED | [阿卜杜勒-阿齊茲國王國際機場](../Page/阿卜杜勒-阿齊茲國王國際機場.md "wikilink") | [吉達](../Page/吉達.md "wikilink")                 | [沙烏地阿拉伯](../Page/沙烏地阿拉伯.md "wikilink")                   |
| JER | [澤西機場](../Page/澤西機場.md "wikilink")                     | [澤西島](../Page/澤西島.md "wikilink")               | [英國](../Page/英國.md "wikilink")                           |
| JFK | [甘迺迪國際機場](../Page/甘迺迪國際機場.md "wikilink")               | [纽约市](../Page/纽约市.md "wikilink")               | [美國](../Page/美國.md "wikilink")                           |
| JHG | [西雙版納嘎洒機場](../Page/西雙版納嘎洒機場.md "wikilink")             | [景洪市](../Page/景洪市.md "wikilink")               | [中國](../Page/中華人民共和國.md "wikilink")                      |
| JJN | [泉州晋江机场](../Page/泉州晋江机场.md "wikilink")                 | [泉州市](../Page/泉州市.md "wikilink")               | [中国](../Page/中華人民共和國.md "wikilink")                      |
| JNB | [O·R·坦博國際機場](../Page/O·R·坦博國際機場.md "wikilink")         | [約翰尼斯堡](../Page/約翰尼斯堡.md "wikilink")           | [南非](../Page/南非.md "wikilink")                           |
| JNU | [朱諾國際機場](../Page/朱諾國際機場.md "wikilink")                 | [朱諾](../Page/朱諾_\(阿拉斯加州\).md "wikilink")       | [美國](../Page/美國.md "wikilink")                           |
| JRO | [吉力馬扎羅國際機場](../Page/吉力馬扎羅國際機場.md "wikilink")           | [阿魯沙](../Page/阿魯沙.md "wikilink")               | [坦尚尼亞](../Page/坦尚尼亞.md "wikilink")                       |
| JRS | [耶路撒冷國際機場](../Page/耶路撒冷國際機場.md "wikilink")             | [耶路撒冷](../Page/耶路撒冷.md "wikilink")             | [以色列](../Page/以色列.md "wikilink")                         |
| JAC | Jackson Hole Airport                                   | Jackson Hole                                   | [美國](../Page/美國.md "wikilink")                           |
| JAN | Allen C Thompson Field                                 | 傑克森                                            | [美國](../Page/美國.md "wikilink")                           |
| JBR | Municipal                                              | Jonesboro                                      | [美國](../Page/美國.md "wikilink")                           |
| JDH |                                                        | Jodhpur                                        | [印度](../Page/印度.md "wikilink")                           |
| JGA |                                                        | Jamnagar                                       | [印度](../Page/印度.md "wikilink")                           |
| JHB | Sultan Ismail International                            | Johor Bahru                                    | [馬來西亞](../Page/馬來西亞.md "wikilink")                       |
| JHW | Chautauqua County Airport                              | Jamestown                                      | [美國](../Page/美國.md "wikilink")                           |
| JKG | Axamo                                                  | Jonkoping                                      | [瑞典](../Page/瑞典.md "wikilink")                           |
| JLN | Municipal Airport                                      | Joplin                                         | [美國](../Page/美國.md "wikilink")                           |
| JMO |                                                        | Jomsom                                         | [尼泊爾](../Page/尼泊爾.md "wikilink")                         |
| JMS | Jamestown Municipal Airport                            | Jamestown                                      | [美國](../Page/美國.md "wikilink")                           |
| JOI | Federal                                                | Joinville                                      | [巴西](../Page/巴西.md "wikilink")                           |
| JON |                                                        | Johnston Island                                | [OutlyingIslands](../Page/OutlyingIslands.md "wikilink") |
| JST | Johnstown Cambria                                      | Johnstown                                      | [美國](../Page/美國.md "wikilink")                           |
| JUJ | El Cadillal                                            | Jujuy                                          | [阿根廷](../Page/阿根廷.md "wikilink")                         |
| JUL | Juliaca                                                | Juliaca                                        | [秘魯](../Page/秘魯.md "wikilink")                           |

[Category:航空代码列表](../Category/航空代码列表.md "wikilink")