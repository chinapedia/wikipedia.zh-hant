**美國職棒大聯盟年度最佳救援投手獎**（****）是[美國職棒大聯盟從](../Page/美國職棒大聯盟.md "wikilink")1976年起，每年頒給[美聯與](../Page/美國聯盟.md "wikilink")[國聯裡表現最為傑出的](../Page/國家聯盟.md "wikilink")[救援投手的獎項](../Page/救援投手.md "wikilink")。跟其他的獎項（例如[賽揚獎或是](../Page/賽揚獎.md "wikilink")[MVP](../Page/美國職棒大聯盟最有價值球員獎.md "wikilink")）不一樣的地方是，年度最佳救援投手獎純粹只看客觀的統計數據表現，而不是看少數幾個人的主觀意見。

每個救援投手的得分（救援點）計算方式如下：[勝投一場得到兩分](../Page/勝投.md "wikilink")；[救援成功一場得到三分](../Page/救援成功.md "wikilink")；[敗投一場則是扣兩分](../Page/敗投.md "wikilink")。從1987年開始引入[救援失敗的數據](../Page/救援失敗.md "wikilink")，一場也是扣兩分。從2000年開始引入所謂的「高難度救援」（）的紀錄，也就是指當救援投手登場的時候，追平分已經在壘包上了。如果救援成功，則可以得到四分。

在球季結束的時候，各聯盟得分最高的救援投手可以獲得這個獎。

得獎最多次的是和[馬里安諾·李維拉](../Page/馬里安諾·李維拉.md "wikilink")，兩位都得了五次。緊追在後的是以及，這兩位球員各都得了四次。

由名字來看，這個獎項是由[輝瑞](../Page/辉瑞制药有限公司.md "wikilink")（）旗下的一個牌子所贊助的。主要的產品是中和胃酸的抑酸劑，它在1970年代最著名的廣告詞是「Rolaids帶來症狀的舒緩」（）。這個獎也是唯一官方認可由商業贊助的[棒球獎項](../Page/棒球.md "wikilink")。
__NOTOC__

## 歷年最佳救援投手獎得主

### [美國聯盟](../Page/美國聯盟.md "wikilink")（1976年至今）

| 年份    | 球員                                                                                | 球隊                                                                            | 戰績          | 救援        | 防禦率           |
| :---- | :-------------------------------------------------------------------------------- | :---------------------------------------------------------------------------- | ----------- | --------- | ------------- |
| 1976年 | [Bill Campbell](../Page/Bill_Campbell.md "wikilink")                              | [明尼蘇達雙城](../Page/明尼蘇達雙城.md "wikilink")                                        | 17-5        | 20        | 3.01          |
| 1977年 | [Bill Campbell](../Page/Bill_Campbell.md "wikilink")                              | [波士頓紅襪](../Page/波士頓紅襪.md "wikilink")                                          | 13-9        | 31        | 2.96          |
| 1978年 | [Rich Gossage](../Page/Rich_Gossage.md "wikilink")                                | [紐約洋基](../Page/紐約洋基.md "wikilink")                                            | 10-11       | 27        | 2.01          |
| 1979年 | [Jim Kern](../Page/Jim_Kern.md "wikilink")                                        | [德州遊騎兵](../Page/德州遊騎兵.md "wikilink")                                          | 13-5        | 29        | 1.57          |
| 1980年 | [Dan Quisenberry](../Page/Dan_Quisenberry.md "wikilink")                          | [堪薩斯市皇家](../Page/堪薩斯市皇家.md "wikilink")                                        | 12-7        | 33        | 3.09          |
| 1981年 | [Rollie Fingers](../Page/Rollie_Fingers.md "wikilink")                            | [密爾瓦基釀酒人](../Page/密爾瓦基釀酒人.md "wikilink")                                      | 6-3         | 28        | 1.04          |
| 1982年 | [Dan Quisenberry](../Page/Dan_Quisenberry.md "wikilink")                          | [堪薩斯市皇家](../Page/堪薩斯市皇家.md "wikilink")                                        | 9-7         | 35        | 2.57          |
| 1983年 | [Dan Quisenberry](../Page/Dan_Quisenberry.md "wikilink")                          | [堪薩斯市皇家](../Page/堪薩斯市皇家.md "wikilink")                                        | 5-3         | 45        | 1.94          |
| 1984年 | [Dan Quisenberry](../Page/Dan_Quisenberry.md "wikilink")                          | [堪薩斯市皇家](../Page/堪薩斯市皇家.md "wikilink")                                        | 6-3         | 44        | 2.65          |
| 1985年 | [Dan Quisenberry](../Page/Dan_Quisenberry.md "wikilink")                          | [堪薩斯市皇家](../Page/堪薩斯市皇家.md "wikilink")                                        | 8-9         | 37        | 2.37          |
| 1986年 | [Dave Righetti](../Page/Dave_Righetti.md "wikilink")                              | [紐約洋基](../Page/紐約洋基.md "wikilink")                                            | 8-8         | 46        | 2.45          |
| 1987年 | [Dave Righetti](../Page/Dave_Righetti.md "wikilink")                              | [紐約洋基](../Page/紐約洋基.md "wikilink")                                            | 8-6         | 31        | 3.51          |
| 1988年 | [丹尼斯·艾克斯利](../Page/丹尼斯·艾克斯利.md "wikilink")                                        | [奧克蘭運動家](../Page/奧克蘭運動家.md "wikilink")                                        | 4-2         | 45        | 2.35          |
| 1989年 | [Jeff Russell](../Page/Jeff_Russell.md "wikilink")                                | [德州遊騎兵](../Page/德州遊騎兵.md "wikilink")                                          | 6-4         | 38        | 1.98          |
| 1990年 | [Bobby Thigpen](../Page/Bobby_Thigpen.md "wikilink")                              | [芝加哥白襪](../Page/芝加哥白襪.md "wikilink")                                          | 4-6         | 57        | 1.83          |
| 1991年 | [Bryan Harvey](../Page/Bryan_Harvey.md "wikilink")                                | [加州天使](../Page/洛杉磯安那罕天使.md "wikilink")                                        | 2-4         | 46        | 1.60          |
| 1992年 | [丹尼斯·艾克斯利](../Page/丹尼斯·艾克斯利.md "wikilink")                                        | [奧克蘭運動家](../Page/奧克蘭運動家.md "wikilink")                                        | 7-1         | 51        | 1.91          |
| 1993年 | [Jeff Montgomery](../Page/Jeff_Montgomery.md "wikilink")                          | [堪薩斯市皇家](../Page/堪薩斯市皇家.md "wikilink")                                        | 7-5         | 45        | 2.27          |
| 1994年 | [李·史密斯](../Page/李·史密斯.md "wikilink")                                              | [巴爾的摩金鶯](../Page/巴爾的摩金鶯.md "wikilink")                                        | 1-4         | 33        | 3.29          |
| 1995年 | [José Mesa](../Page/José_Mesa.md "wikilink")                                      | [克-{里}-夫蘭印地安人](../Page/克里夫蘭印地安人.md "wikilink")                                | 3-0         | 46        | 1.12          |
| 1996年 | [John Wetteland](../Page/John_Wetteland.md "wikilink")                            | [紐約洋基](../Page/紐約洋基.md "wikilink")                                            | 2-3         | 43        | 2.83          |
| 1997年 | [Randy Myers](../Page/Randy_Myers.md "wikilink")                                  | [巴爾的摩金鶯](../Page/巴爾的摩金鶯.md "wikilink")                                        | 2-3         | 45        | 1.51          |
| 1998年 | [Tom Gordon](../Page/Tom_Gordon.md "wikilink")                                    | [波士頓紅襪](../Page/波士頓紅襪.md "wikilink")                                          | 7-4         | 46        | 2.72          |
| 1999年 | [馬里安諾·李維拉](../Page/馬里安諾·李維拉.md "wikilink")                                        | [紐約洋基](../Page/紐約洋基.md "wikilink")                                            | 4-3         | 45        | 1.83          |
| 2000年 | [Todd Jones](../Page/Todd_Jones.md "wikilink")                                    | [底特律老虎](../Page/底特律老虎.md "wikilink")                                          | 2-4         | 42        | 3.52          |
| 2001年 | [馬里安諾·李維拉](../Page/馬里安諾·李維拉.md "wikilink")                                        | [紐約洋基](../Page/紐約洋基.md "wikilink")                                            | 4-6         | 50        | 2.34          |
| 2002年 | [Billy Koch](../Page/Billy_Koch.md "wikilink")                                    | [奧克蘭運動家](../Page/奧克蘭運動家.md "wikilink")                                        | 11-4        | 44        | 3.27          |
| 2003年 | [Keith Foulke](../Page/Keith_Foulke.md "wikilink")                                | [奧克蘭運動家](../Page/奧克蘭運動家.md "wikilink")                                        | 9-1         | 43        | 2.08          |
| 2004年 | [馬里安諾·李維拉](../Page/馬里安諾·李維拉.md "wikilink")                                        | [紐約洋基](../Page/紐約洋基.md "wikilink")                                            | 4-2         | 53        | 1.94          |
| 2005年 | [馬里安諾·李維拉](../Page/馬里安諾·李維拉.md "wikilink")                                        | [紐約洋基](../Page/紐約洋基.md "wikilink")                                            | 7-4         | 43        | 1.38          |
| 2006年 | [法蘭西斯科·羅德里奎茲](../Page/法蘭西斯科·羅德里奎茲.md "wikilink")                                  | [洛杉磯安那罕天使](../Page/洛杉磯安那罕天使.md "wikilink")                                    | 2-3         | 47        | 1.73          |
| 2007年 | [J. J. Putz](../Page/J._J._Putz.md "wikilink")                                    | [西雅圖水手](../Page/西雅圖水手.md "wikilink")                                          | 6-1         | 40        | 1.38          |
| 2008年 | [法蘭西斯科·羅德里奎茲](../Page/法蘭西斯科·羅德里奎茲.md "wikilink")                                  | [洛杉磯安那罕天使](../Page/洛杉磯安那罕天使.md "wikilink")                                    | 2-3         | 62        | 2.24          |
| 2009年 | [馬里安諾·李維拉](../Page/馬里安諾·李維拉.md "wikilink")</br>[喬·內森](../Page/喬·內森.md "wikilink") | [紐約洋基](../Page/紐約洋基.md "wikilink")</br>[明尼蘇達雙城](../Page/明尼蘇達雙城.md "wikilink") | 3-3</br>2-2 | 44</br>47 | 1.76</br>2.10 |
| 2010年 | [拉斐爾·索利安諾](../Page/拉斐爾·索利安諾.md "wikilink")                                        | [坦帕灣光芒](../Page/坦帕灣光芒.md "wikilink")                                          | 3-2         | 45        | 1.73          |
| 2011年 | [荷西·瓦爾韋德](../Page/荷西·瓦爾韋德.md "wikilink")                                          | [底特律老虎](../Page/底特律老虎.md "wikilink")                                          | 2-4         | 49        | 2.24          |
| 2012年 | [吉姆·詹森](../Page/吉姆·詹森.md "wikilink")                                              | [巴爾的摩金鶯](../Page/巴爾的摩金鶯.md "wikilink")                                        | 2-1         | 51        | 2.49          |

### [國家聯盟](../Page/國家聯盟.md "wikilink")（1976年至今）

| 年份    | 球員                                                           | 球隊                                       | 戰績   | 救援 | 防禦率  |
| :---- | :----------------------------------------------------------- | :--------------------------------------- | ---- | -- | ---- |
| 1976年 | [Rawly Eastwick](../Page/Rawly_Eastwick.md "wikilink")       | [辛辛那提紅人](../Page/辛辛那提紅人.md "wikilink")   | 11-5 | 26 | 2.09 |
| 1977年 | [Rollie Fingers](../Page/Rollie_Fingers.md "wikilink")       | [聖地牙哥教士](../Page/聖地牙哥教士.md "wikilink")   | 8-9  | 35 | 2.99 |
| 1978年 | [Rollie Fingers](../Page/Rollie_Fingers.md "wikilink")       | [聖地牙哥教士](../Page/聖地牙哥教士.md "wikilink")   | 6-13 | 37 | 2.52 |
| 1979年 | [Bruce Sutter](../Page/Bruce_Sutter.md "wikilink")           | [芝加哥小熊](../Page/芝加哥小熊.md "wikilink")     | 6-6  | 37 | 2.22 |
| 1980年 | [Rollie Fingers](../Page/Rollie_Fingers.md "wikilink")       | [聖地牙哥教士](../Page/聖地牙哥教士.md "wikilink")   | 11-9 | 23 | 2.80 |
| 1981年 | [Bruce Sutter](../Page/Bruce_Sutter.md "wikilink")           | [聖路易紅雀](../Page/聖路易紅雀.md "wikilink")     | 3-5  | 25 | 2.62 |
| 1982年 | [Bruce Sutter](../Page/Bruce_Sutter.md "wikilink")           | [聖路易紅雀](../Page/聖路易紅雀.md "wikilink")     | 9-8  | 36 | 2.90 |
| 1983年 | [Al Holland](../Page/Al_Holland.md "wikilink")               | [費城費城人](../Page/費城費城人.md "wikilink")     | 8-4  | 25 | 2.26 |
| 1984年 | [Bruce Sutter](../Page/Bruce_Sutter.md "wikilink")           | [聖路易紅雀](../Page/聖路易紅雀.md "wikilink")     | 5-7  | 45 | 1.54 |
| 1985年 | [Jeff Reardon](../Page/Jeff_Reardon.md "wikilink")           | [蒙特婁博覽會](../Page/華盛頓國民.md "wikilink")    | 2-8  | 41 | 3.18 |
| 1986年 | [Todd Worrell](../Page/Todd_Worrell.md "wikilink")           | [聖路易紅雀](../Page/聖路易紅雀.md "wikilink")     | 9-10 | 36 | 2.08 |
| 1987年 | [Steve Bedrosian](../Page/Steve_Bedrosian.md "wikilink")     | [費城費城人](../Page/費城費城人.md "wikilink")     | 5-3  | 40 | 2.83 |
| 1988年 | [約翰·法蘭柯](../Page/約翰·法蘭柯.md "wikilink")                       | [辛辛那提紅人](../Page/辛辛那提紅人.md "wikilink")   | 6-6  | 39 | 1.57 |
| 1989年 | [馬克·戴維斯](../Page/馬克·戴維斯.md "wikilink")                       | [聖地牙哥教士](../Page/聖地牙哥教士.md "wikilink")   | 4-3  | 44 | 1.84 |
| 1990年 | [約翰·法蘭柯](../Page/約翰·法蘭柯.md "wikilink")                       | [紐約大都會](../Page/紐約大都會.md "wikilink")     | 5-3  | 33 | 2.53 |
| 1991年 | [李·史密斯](../Page/李·史密斯.md "wikilink")                         | [聖路易紅雀](../Page/聖路易紅雀.md "wikilink")     | 6-3  | 47 | 2.34 |
| 1992年 | [李·史密斯](../Page/李·史密斯.md "wikilink")                         | [聖路易紅雀](../Page/聖路易紅雀.md "wikilink")     | 4-9  | 43 | 3.12 |
| 1993年 | [Randy Myers](../Page/Randy_Myers.md "wikilink")             | [芝加哥小熊](../Page/芝加哥小熊.md "wikilink")     | 2-4  | 53 | 3.11 |
| 1994年 | [羅德·貝克](../Page/羅德·貝克.md "wikilink")                         | [舊金山巨人](../Page/舊金山巨人.md "wikilink")     | 2-4  | 28 | 2.77 |
| 1995年 | [Tom Henke](../Page/Tom_Henke.md "wikilink")                 | [聖路易紅雀](../Page/聖路易紅雀.md "wikilink")     | 1-1  | 36 | 1.82 |
| 1996年 | [Jeff Brantley](../Page/Jeff_Brantley.md "wikilink")         | [辛辛那提紅人](../Page/辛辛那提紅人.md "wikilink")   | 1-2  | 44 | 2.41 |
| 1997年 | [Jeff Shaw](../Page/Jeff_Shaw.md "wikilink")                 | [辛辛那提紅人](../Page/辛辛那提紅人.md "wikilink")   | 4-2  | 42 | 2.38 |
| 1998年 | [崔佛·赫夫曼](../Page/崔佛·赫夫曼.md "wikilink")                       | [聖地牙哥教士](../Page/聖地牙哥教士.md "wikilink")   | 4-2  | 53 | 1.48 |
| 1999年 | [Billy Wagner](../Page/Billy_Wagner.md "wikilink")           | [休士頓太空人](../Page/休士頓太空人.md "wikilink")   | 4-1  | 39 | 1.57 |
| 2000年 | [Antonio Alfonseca](../Page/Antonio_Alfonseca.md "wikilink") | [佛羅里達馬林魚](../Page/佛羅里達馬林魚.md "wikilink") | 5-6  | 45 | 4.24 |
| 2001年 | [Armando Benitez](../Page/Armando_Benitez.md "wikilink")     | [紐約大都會](../Page/紐約大都會.md "wikilink")     | 6-4  | 43 | 3.77 |
| 2002年 | [約翰·史摩茲](../Page/約翰·史摩茲.md "wikilink")                       | [亞特蘭大勇士](../Page/亞特蘭大勇士.md "wikilink")   | 3-2  | 55 | 3.25 |
| 2003年 | [艾瑞克·蓋格尼](../Page/艾瑞克·蓋格尼.md "wikilink")                     | [洛杉磯道奇](../Page/洛杉磯道奇.md "wikilink")     | 2-3  | 55 | 1.20 |
| 2004年 | [艾瑞克·蓋格尼](../Page/艾瑞克·蓋格尼.md "wikilink")                     | [洛杉磯道奇](../Page/洛杉磯道奇.md "wikilink")     | 7-3  | 45 | 2.19 |
| 2005年 | [Chad Cordero](../Page/Chad_Cordero.md "wikilink")           | [華盛頓國民](../Page/華盛頓國民.md "wikilink")     | 2-4  | 47 | 1.82 |
| 2006年 | [崔佛·赫夫曼](../Page/崔佛·赫夫曼.md "wikilink")                       | [聖地牙哥教士](../Page/聖地牙哥教士.md "wikilink")   | 0-2  | 46 | 2.14 |
| 2007年 | [荷西·瓦爾韋德](../Page/荷西·瓦爾韋德.md "wikilink")                     | [亞利桑那響尾蛇](../Page/亞利桑那響尾蛇.md "wikilink") | 1-4  | 47 | 2.66 |
| 2008年 | [布萊德·李吉](../Page/布萊德·李吉.md "wikilink")                       | [費城費城人](../Page/費城費城人.md "wikilink")     | 2-0  | 41 | 1.95 |
| 2009年 | [希斯·貝爾](../Page/希斯·貝爾.md "wikilink")                         | [聖地牙哥教士](../Page/聖地牙哥教士.md "wikilink")   | 6-4  | 42 | 2.71 |
| 2010年 | [希斯·貝爾](../Page/希斯·貝爾.md "wikilink")                         | [聖地牙哥教士](../Page/聖地牙哥教士.md "wikilink")   | 2-0  | 47 | 1.93 |
| 2011年 | [約翰·阿克斯福德](../Page/約翰·阿克斯福德.md "wikilink")                   | [密爾瓦基釀酒人](../Page/密爾瓦基釀酒人.md "wikilink") | 2-2  | 46 | 1.93 |
| 2012年 | [克雷格·金布雷爾](../Page/克雷格·金布雷爾.md "wikilink")                   | [亞特蘭大勇士](../Page/亞特蘭大勇士.md "wikilink")   | 3-1  | 42 | 1.01 |

## 參見

  - [救援成功](../Page/救援成功.md "wikilink")

## 外部連結

  - [大聯盟官網關於這個獎項的歷史](https://web.archive.org/web/20041013213647/http://www.mlb.com/NASApp/mlb/mlb/news/mlb_news.jsp?ymd=20020515&content_id=25864&vkey=news_mlb&fext=.jsp)
  - [ESPN歷年救援投手獎候選人排行](http://sports.espn.go.com/mlb/stats/rolaids)

[Relief Man of the Year Award](../Category/美國職棒獎項.md "wikilink")