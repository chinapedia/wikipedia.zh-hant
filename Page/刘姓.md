**劉姓**是[中文姓氏之一](../Page/中文姓氏.md "wikilink")，2013年有人口近7,000萬，佔[中國全國人口的](../Page/中國.md "wikilink")5.34%，在王、李、張之後排第四位。根據中國戶籍管理部門的全國公民身份信息系統（NCIIS），劉是第四大姓。歷史上，劉姓建立了[汉朝](../Page/汉朝.md "wikilink")、[季漢](../Page/季漢.md "wikilink")、[後漢](../Page/後漢.md "wikilink")、[劉宋](../Page/劉宋.md "wikilink")、[南漢等政權](../Page/南漢.md "wikilink")。最早一支劉姓源自[堯的後裔](../Page/堯.md "wikilink")[劉累](../Page/劉累.md "wikilink")，故劉累為劉姓得姓始祖。刘姓的[郡望有二十多个](../Page/郡望.md "wikilink")，以[彭城刘氏最为知名](../Page/彭城刘氏.md "wikilink")。劉（）姓在[韓國亦有分佈](../Page/韓國.md "wikilink")，在當地排名第15，有約22萬人。

## 渊源

  - [帝尧后裔](../Page/帝尧.md "wikilink")[祁姓刘氏](../Page/祁姓.md "wikilink")。尧的后裔[刘累为](../Page/刘累.md "wikilink")[孔甲养龙](../Page/孔甲.md "wikilink")，后来有一条龙病死，刘累携家眷逃亡鲁阳（今河南[鲁山县](../Page/鲁山县.md "wikilink")），这是刘姓的起源（另一说法认为刘累故地在河南[偃师](../Page/偃师.md "wikilink")）。
  - [周成王曾封曾祖父](../Page/周成王.md "wikilink")[王季后人于刘](../Page/王季.md "wikilink")（河南偃师），后失考。
  - [周定王八年](../Page/周定王.md "wikilink")（公元前592年）又封刘邑与王弟季子，[王季子即](../Page/王季子.md "wikilink")[刘康公](../Page/刘康公.md "wikilink")，建立了[刘国](../Page/刘国.md "wikilink")。
  - [劉邦感念](../Page/劉邦.md "wikilink")[項伯當年在鴻門宴時的解救之恩](../Page/項伯.md "wikilink")，賜予項伯劉姓。
  - [婁敬](../Page/婁敬.md "wikilink")，力陳定都長安，獲賜姓劉，拜為郎中，號奉春君。婁敬建議與[匈奴和親](../Page/匈奴.md "wikilink")，並且徙六國後裔和強宗豪族十餘萬人至關中，成為漢初之基本國策。
  - [西漢初年](../Page/西漢.md "wikilink")，漢朝派遣宗室女與匈奴王室聯姻，匈奴贵族之後多改劉姓。
  - [刘知几考证刘氏不是尧的后裔](../Page/刘知几.md "wikilink")，而是[陆终的苗裔](../Page/陆终.md "wikilink")\[1\]\[2\]。

## 郡望

据有关史料记载，刘姓郡望多达27个。

  - [彭城郡](../Page/彭城郡.md "wikilink")（[彭城刘氏](../Page/彭城刘氏.md "wikilink")）
  - [沛郡](../Page/沛郡.md "wikilink")
  - [弘农郡](../Page/弘农郡.md "wikilink")
  - [河间郡](../Page/河间郡.md "wikilink")
  - [中山郡](../Page/中山郡.md "wikilink")
  - [梁郡](../Page/梁郡.md "wikilink")
  - [顿丘郡](../Page/顿丘郡.md "wikilink")
  - [南阳郡](../Page/南阳郡.md "wikilink")
  - [东平郡](../Page/东平郡.md "wikilink")
  - [高密郡](../Page/高密郡.md "wikilink")
  - [竟陵郡](../Page/竟陵郡.md "wikilink")
  - [河南郡](../Page/河南郡.md "wikilink")
  - [尉氏郡](../Page/尉氏郡.md "wikilink")
  - [广平郡](../Page/广平郡.md "wikilink")

<!-- end list -->

  - [丹阳郡](../Page/丹阳郡.md "wikilink")
  - [广陵郡](../Page/广陵郡.md "wikilink")
  - [长沙郡](../Page/长沙郡.md "wikilink")
  - [临淮郡](../Page/临淮郡.md "wikilink")
  - [高平郡](../Page/高平郡.md "wikilink")
  - [东莞郡](../Page/东莞郡.md "wikilink")
  - [平原郡](../Page/平原郡.md "wikilink")
  - [琅邪郡](../Page/琅邪郡.md "wikilink")
  - [兰陵郡](../Page/兰陵郡.md "wikilink")
  - [东海郡](../Page/东海郡.md "wikilink")
  - [宣城郡](../Page/宣城郡.md "wikilink")
  - [南郡](../Page/南郡.md "wikilink")
  - [高阳郡](../Page/高阳郡.md "wikilink")

## 堂號

  - [彭城堂](../Page/彭城堂.md "wikilink")
  - [藜照堂](../Page/藜照堂.md "wikilink")
  - [中山堂](../Page/中山堂.md "wikilink")
  - [蒲編堂](../Page/蒲編堂.md "wikilink")
  - [五忠堂](../Page/五忠堂.md "wikilink")
  - [墨莊堂](../Page/墨莊堂.md "wikilink")
  - [漢里堂](../Page/漢里堂.md "wikilink")

<!-- end list -->

  - [豢龍堂](../Page/豢龍堂.md "wikilink")
  - [七業堂](../Page/七業堂.md "wikilink")
  - [傳經堂](../Page/傳經堂.md "wikilink")
  - [鐵漢堂](../Page/鐵漢堂.md "wikilink")
  - [清愛堂](../Page/清愛堂.md "wikilink")
  - [磐宗堂](../Page/磐宗堂.md "wikilink")

## 當代傑出人物

<table>
<tbody>
<tr class="odd">
<td><p>区域</p></td>
<td><p>政治</p></td>
<td><p>經濟</p></td>
<td><p>文化</p></td>
<td><p>教育</p></td>
<td><p>娛樂</p></td>
</tr>
<tr class="even">
<td><p>中國大陸</p></td>
<td><p><a href="../Page/劉鶴.md" title="wikilink">劉鶴</a><sup><a href="../Page/中华人民共和国国务院副总理.md" title="wikilink">国务院副总理</a>，2018—</sup>、<a href="../Page/劉結一.md" title="wikilink">劉結一</a><sup><a href="../Page/国务院台湾事务办公室.md" title="wikilink">國台辦主任</a>，2018—</sup></p></td>
<td><p><a href="../Page/劉強東.md" title="wikilink">劉強東</a><sup><a href="../Page/京东_(网站).md" title="wikilink">京東商城創始人</a></sup></p></td>
<td><p><a href="../Page/劉翔.md" title="wikilink">劉翔</a><sup><a href="../Page/2004年夏季奥林匹克运动会田径比赛.md" title="wikilink">奧運男子跨欄金牌，2004</a></sup><br />
刘殿中<sup><a href="../Page/象棋特級大師.md" title="wikilink">象棋特級大師</a></sup></p></td>
<td><p><a href="../Page/刘伟_(1957年).md" title="wikilink">刘伟</a><sup><a href="../Page/中國人民大學.md" title="wikilink">中國人民大學校長</a>，2015—</sup><br />
<a href="../Page/刘道玉.md" title="wikilink">刘道玉</a><sup><a href="../Page/武漢大學.md" title="wikilink">武漢大學校長</a>，1981—1988</sup></p></td>
<td><p><a href="../Page/刘烨_(演员).md" title="wikilink">刘烨</a><sup><a href="../Page/金馬獎.md" title="wikilink">金馬獎</a>2001年最佳男演員</sup><br />
<a href="../Page/劉曉慶.md" title="wikilink">劉曉慶</a><sup><a href="../Page/金雞獎.md" title="wikilink">金雞獎</a>1987年最佳女演員</sup><br />
<a href="../Page/劉詩詩.md" title="wikilink">劉詩詩</a><sup>演員</sup><br />
<a href="../Page/劉濤.md" title="wikilink">劉濤</a><sup>演員</sup></p></td>
</tr>
<tr class="odd">
<td><p>港澳</p></td>
<td><p><a href="../Page/劉迺強.md" title="wikilink">劉迺強</a><sup><a href="../Page/全国人民代表大会常务委员会香港特别行政区基本法委员会.md" title="wikilink">香港基本法委員會委員</a></sup></p></td>
<td><p><a href="../Page/劉鑾雄.md" title="wikilink">劉鑾雄</a><sup>個人資產為香港第5</sup></p></td>
<td><p><a href="../Page/劉以鬯.md" title="wikilink">劉以鬯</a><sup>作家</sup></p></td>
<td><p><a href="../Page/劉遵義.md" title="wikilink">劉遵義</a><sup><a href="../Page/香港中文大學.md" title="wikilink">香港中文大學校長</a>，2004—2010</sup></p></td>
<td><p><a href="../Page/劉德華.md" title="wikilink">劉德華</a><sup><a href="../Page/金馬獎.md" title="wikilink">金馬獎</a>2004、2011年最佳男演員</sup><br />
<a href="../Page/劉青雲.md" title="wikilink">劉青雲</a><sup><a href="../Page/金馬獎.md" title="wikilink">金馬獎</a>2012年最佳男演員</sup></p></td>
</tr>
<tr class="even">
<td><p>台灣</p></td>
<td><p><a href="../Page/劉兆玄.md" title="wikilink">劉兆玄</a><sup><a href="../Page/行政院院長.md" title="wikilink">行政院院長</a>，2008—2009</sup><br />
<a href="../Page/劉政鸿.md" title="wikilink">劉政鸿</a><sup>前<a href="../Page/苗栗县县长.md" title="wikilink">苗栗县县长</a>，2005—2014</sup></p></td>
<td><p>劉克振<sup><a href="../Page/研華科技.md" title="wikilink">研華科技創辦人</a>，個人資產為台灣第27</sup></p></td>
<td><p><a href="../Page/劉兆漢.md" title="wikilink">劉兆漢</a><sup><a href="../Page/中央研究院.md" title="wikilink">中研院副院長</a>，2006—2011</sup><br />
<a href="../Page/刘墉_(作家).md" title="wikilink">刘墉</a><sup>作家</sup></p></td>
<td><p><a href="../Page/國立清華大學.md" title="wikilink">國立清華大學校長</a></p>
<ul>
<li><a href="../Page/劉炯朗.md" title="wikilink">劉炯朗</a><sup>1998—2002</sup></li>
<li><a href="../Page/劉兆玄.md" title="wikilink">劉兆玄</a><sup>1987—1993</sup><br />
</li>
</ul></td>
<td><p><a href="../Page/劉家昌.md" title="wikilink">劉家昌</a><sup>作曲家</sup><br />
<a href="../Page/劉若英.md" title="wikilink">劉若英</a><sup>歌手</sup><br />
<a href="../Page/刘瑞琪.md" title="wikilink">刘瑞琪</a><sup><a href="../Page/金鐘獎.md" title="wikilink">金鐘獎</a>2009年最佳女演員</sup></p></td>
</tr>
<tr class="odd">
<td><p>馬來西亞</p></td>
<td><p><a href="../Page/马来西亚内阁.md" title="wikilink">马来西亚内阁</a><sup>2018—</sup></p>
<ul>
<li><a href="../Page/劉偉強_(馬來西亞).md" title="wikilink">劉偉強</a><sup><a href="../Page/马来西亚首相署.md" title="wikilink">首相署部長</a></sup></li>
<li><a href="../Page/劉鎮東.md" title="wikilink">劉鎮東</a><sup>國防部副部長</sup></li>
</ul>
<p>刘佑义<sup>柔佛保皇党主席</sup>[3]</p></td>
<td><p>劉子君<sup>個人資產為國內第8</sup></p></td>
<td><p><a href="../Page/劉國倫.md" title="wikilink">劉國倫</a><sup>羽球員</sup><br />
<a href="../Page/刘元元.md" title="wikilink">刘元元</a><sup>主播</sup></p></td>
<td><p>劉利民<sup><a href="../Page/馬來西亞華校董事聯合會總會.md" title="wikilink">董總主席</a>，2015—2018</sup></p></td>
<td><p><a href="../Page/劉子絢.md" title="wikilink">劉子絢</a><sup>新加坡演員</sup><br />
<a href="../Page/劉永輝.md" title="wikilink">劉永輝</a><sup>音樂製作人</sup></p></td>
</tr>
<tr class="even">
<td><p>亚洲</p></td>
<td><p>（韩国）<a href="../Page/刘彰顺.md" title="wikilink">刘彰顺</a><sup><a href="../Page/韩国总理列表.md" title="wikilink">国务总理</a>，1982</sup><br />
（韩国）<a href="../Page/劉承旼.md" title="wikilink">劉承旼</a><sup><a href="../Page/大邱.md" title="wikilink">大邱</a><a href="../Page/廣域市.md" title="wikilink">廣域市</a><a href="../Page/南区_(大邱).md" title="wikilink">南区国会议员</a></sup> （汶萊）劉光明<sup>財政部副部長</sup><br />
（新加坡）<a href="../Page/劉程強.md" title="wikilink">劉程強</a><sup>國會議員、<a href="../Page/工人党_(新加坡).md" title="wikilink">工人黨秘書長</a></sup><br />
</p></td>
<td></td>
<td><p>（新加坡）劉太格<sup>規劃之父</sup></p></td>
<td><p>（新加坡）<a href="../Page/劉立方.md" title="wikilink">劉立方</a><sup><a href="../Page/新加坡国立大学.md" title="wikilink">新加坡国立大学行政副校長</a>，2016—</sup><br />
（韩国）<a href="../Page/刘龙_(韩国).md" title="wikilink">刘龙</a><sup><a href="../Page/韩国科学技术院.md" title="wikilink">韩国科学技术院特别教授</a></sup></p></td>
<td><p><strong>韩国</strong></p>
<ul>
<li><a href="../Page/刘智泰.md" title="wikilink">刘智泰</a><sup>导演、演員</sup></li>
<li><a href="../Page/劉建_(演員).md" title="wikilink">劉建</a><sup>演員</sup></li>
<li><a href="../Page/劉俊相.md" title="wikilink">劉俊相</a><sup>演員</sup><br />
</li>
<li><a href="../Page/刘在锡.md" title="wikilink">刘在锡</a><sup>主持人</sup></li>
</ul></td>
</tr>
<tr class="odd">
<td><p>世界</p></td>
<td><p>（美國）<a href="../Page/劉雲平.md" title="wikilink">劉雲平</a><sup><a href="../Page/美國眾議院.md" title="wikilink">美國眾議院議員</a>，2015—</sup></p></td>
<td></td>
<td></td>
<td><p>（美國）劉駿<sup><a href="../Page/石溪大學.md" title="wikilink">石溪大學副校長</a>，2016—</sup></p></td>
<td><p><strong>美國</strong></p>
<ul>
<li><a href="../Page/劉文正.md" title="wikilink">劉文正</a><sup>台灣歌手、演員</sup></li>
<li><a href="../Page/劉亦菲.md" title="wikilink">劉亦菲</a><sup>大陸演員</sup></li>
<li><a href="../Page/劉玉玲.md" title="wikilink">劉玉玲</a><sup>演員</sup><br />
</li>
</ul>
<p><strong>加拿大</strong></p>
<ul>
<li><a href="../Page/刘恺威.md" title="wikilink">刘恺威</a><sup>香港演員</sup></li>
<li><a href="../Page/劉嘉玲.md" title="wikilink">劉嘉玲</a><sup><a href="../Page/金雞獎.md" title="wikilink">金雞獎</a>2007年最佳女演員，現居香港</sup><br />
</li>
</ul>
<p><strong>德国</strong></p>
<ul>
<li><a href="../Page/劉香萍.md" title="wikilink">劉香萍</a><sup>香港演員</sup></li>
</ul></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 其他名人

### 皇帝

  - [西汉](../Page/西汉.md "wikilink")
      - [太祖高皇帝](../Page/太祖.md "wikilink")[劉邦](../Page/劉邦.md "wikilink")
      - 孝惠皇帝[劉盈](../Page/劉盈.md "wikilink")
      - [劉恭](../Page/劉恭.md "wikilink")
      - [劉弘](../Page/劉弘.md "wikilink")
      - [太宗孝文皇帝](../Page/太宗.md "wikilink")[劉恒](../Page/劉恒.md "wikilink")
      - 孝景皇帝[劉启](../Page/劉启.md "wikilink")
      - [世宗孝武皇帝](../Page/世宗.md "wikilink")[劉彻](../Page/劉彻.md "wikilink")
      - 孝昭皇帝[劉弗陵](../Page/劉弗陵.md "wikilink")
      - [劉贺](../Page/劉贺.md "wikilink")
      - [中宗孝宣皇帝](../Page/中宗.md "wikilink")[劉询](../Page/劉询.md "wikilink")
      - [高宗孝元皇帝](../Page/高宗.md "wikilink")[劉奭](../Page/劉奭.md "wikilink")
      - [统宗孝成皇帝](../Page/统宗.md "wikilink")[劉骜](../Page/劉骜.md "wikilink")
      - 孝哀皇帝[劉欣](../Page/劉欣.md "wikilink")
      - [元宗孝平皇帝](../Page/元宗.md "wikilink")[劉衎](../Page/劉衎.md "wikilink")
      - [劉婴](../Page/劉婴.md "wikilink")
  - [玄汉](../Page/玄汉.md "wikilink")
      - [劉玄](../Page/劉玄.md "wikilink")
  - [赤眉汉](../Page/赤眉汉.md "wikilink")
      - [刘盆子](../Page/刘盆子.md "wikilink")
  - [东汉](../Page/东汉.md "wikilink")
      - [世祖光武皇帝](../Page/世祖.md "wikilink")[劉秀](../Page/劉秀.md "wikilink")
      - [显宗孝明皇帝](../Page/显宗.md "wikilink")[劉庄](../Page/劉庄.md "wikilink")
      - [肃宗孝章皇帝](../Page/肃宗.md "wikilink")[劉炟](../Page/劉炟.md "wikilink")
      - [穆宗孝和皇帝](../Page/穆宗.md "wikilink")[劉肇](../Page/劉肇.md "wikilink")
      - 孝殇皇帝[劉隆](../Page/劉隆.md "wikilink")
      - [恭宗孝安皇帝](../Page/恭宗.md "wikilink")[劉祜](../Page/劉祜.md "wikilink")
      - [劉懿](../Page/劉懿.md "wikilink")
      - [敬宗孝顺皇帝](../Page/敬宗.md "wikilink")[劉保](../Page/劉保.md "wikilink")
      - 孝冲皇帝[劉炳](../Page/劉炳.md "wikilink")
      - 孝质皇帝[劉缵](../Page/劉缵.md "wikilink")
      - [威宗孝桓皇帝](../Page/威宗.md "wikilink")[劉志](../Page/劉志.md "wikilink")
      - 孝灵皇帝[劉宏](../Page/劉宏.md "wikilink")
      - [劉辩](../Page/劉辩.md "wikilink")
      - 孝献皇帝[劉协](../Page/劉协.md "wikilink")
  - [季汉](../Page/季汉.md "wikilink")
      - [烈祖昭烈皇帝](../Page/烈祖.md "wikilink")[劉备](../Page/劉备.md "wikilink")
      - [仁宗孝怀皇帝](../Page/仁宗.md "wikilink")[劉禅](../Page/劉禅.md "wikilink")
  - [汉赵](../Page/汉赵.md "wikilink")
      - 太祖（初谥高祖）光文皇帝[劉渊](../Page/劉渊.md "wikilink")
      - [劉和](../Page/劉和.md "wikilink")
      - [烈宗昭武皇帝](../Page/烈宗.md "wikilink")[劉聰](../Page/劉聰.md "wikilink")
      - 隐帝[劉粲](../Page/劉粲.md "wikilink")
      - [襄宗昭文皇帝](../Page/襄宗.md "wikilink")[劉曜](../Page/劉曜.md "wikilink")

<!-- end list -->

  - [刘宋](../Page/刘宋.md "wikilink")
      - 高祖武皇帝[劉裕](../Page/劉裕.md "wikilink")
      - [劉義符](../Page/劉義符.md "wikilink")
      - 太祖文皇帝（初谥中宗景皇帝）[劉義隆](../Page/劉義隆.md "wikilink")
      - [劉劭](../Page/劉劭.md "wikilink")
      - 世祖孝武皇帝[劉駿](../Page/劉駿.md "wikilink")
      - [劉子業](../Page/劉子業.md "wikilink")
      - 太宗明皇帝[劉彧](../Page/劉彧.md "wikilink")
      - [劉昱](../Page/劉昱.md "wikilink")
      - 顺帝[劉準](../Page/劉準.md "wikilink")
  - [南漢](../Page/南漢.md "wikilink")
      - 高祖天皇大帝[劉龑](../Page/劉龑.md "wikilink")
      - 殇帝[劉玢](../Page/劉玢.md "wikilink")
      - 中宗文武光圣明孝皇帝[劉晟](../Page/劉晟.md "wikilink")
      - [劉鋹](../Page/劉鋹.md "wikilink")
  - [後漢](../Page/後漢.md "wikilink")
      - 高祖睿文圣武昭肃孝皇帝[劉知遠](../Page/劉知遠.md "wikilink")
      - 隐帝[劉承祐](../Page/劉承祐.md "wikilink")
  - [北漢](../Page/北漢.md "wikilink")
      - 世祖神武皇帝[劉崇](../Page/劉崇.md "wikilink")
      - [睿宗孝和皇帝](../Page/睿宗.md "wikilink")[劉鈞](../Page/劉鈞.md "wikilink")
      - [劉繼恩](../Page/劉繼恩.md "wikilink")
      - [劉繼元](../Page/劉繼元.md "wikilink")
  - [燕](../Page/燕.md "wikilink")
      - [劉守光](../Page/劉守光.md "wikilink")
  - [齊](../Page/齊.md "wikilink")
      - [劉豫](../Page/劉豫.md "wikilink")
  - 其他
      - [刘信](../Page/刘信.md "wikilink")
      - [刘望](../Page/刘望.md "wikilink")
      - [刘永](../Page/刘永.md "wikilink")
      - [刘芒荡](../Page/刘芒荡.md "wikilink")
      - [刘显](../Page/刘显.md "wikilink")
      - [刘黎](../Page/刘黎.md "wikilink")
      - [刘义宣](../Page/刘义宣.md "wikilink")
      - [刘子勋](../Page/刘子勋.md "wikilink")
      - [刘苗王](../Page/刘苗王.md "wikilink")
      - [刘蠡升](../Page/刘蠡升.md "wikilink")
      - [刘没铎](../Page/刘没铎.md "wikilink")
      - [刘武周](../Page/刘武周.md "wikilink")
      - [刘敬躬](../Page/刘敬躬.md "wikilink")
      - [刘元进](../Page/刘元进.md "wikilink")
      - [刘迦论](../Page/刘迦论.md "wikilink")

### 宰相

#### 周朝

  - [刘康公](../Page/刘康公.md "wikilink")
  - [刘定公](../Page/刘定公.md "wikilink")
  - [刘献公](../Page/刘献公.md "wikilink")
  - [刘文公](../Page/刘文公.md "wikilink")
  - [刘桓公](../Page/刘桓公.md "wikilink")

#### 汉朝

  - [刘舍](../Page/刘舍.md "wikilink")
  - [刘屈氂](../Page/刘屈氂.md "wikilink")
  - [刘縯](../Page/刘縯.md "wikilink")
  - [刘赐](../Page/刘赐.md "wikilink")
  - [劉隆](../Page/劉隆.md "wikilink")
  - [劉方](../Page/劉方.md "wikilink")
  - [劉恺](../Page/劉恺.md "wikilink")
  - [劉授](../Page/劉授.md "wikilink")
  - [劉憙](../Page/劉憙.md "wikilink")
  - [劉光](../Page/劉光.md "wikilink")
  - [刘崎](../Page/刘崎.md "wikilink")
  - [劉寿](../Page/劉寿.md "wikilink")
  - [刘矩](../Page/刘矩.md "wikilink")
  - [刘宠](../Page/刘宠.md "wikilink")
  - [劉茂](../Page/劉茂.md "wikilink")
  - [劉嚣](../Page/劉嚣.md "wikilink")
  - [劉逸](../Page/劉逸.md "wikilink")
  - [劉宽](../Page/劉宽.md "wikilink")
  - [劉郃](../Page/劉郃.md "wikilink")
  - [劉弘](../Page/劉弘.md "wikilink")
  - [刘虞](../Page/刘虞.md "wikilink")

#### 三国

  - [刘放](../Page/刘放.md "wikilink")
  - [劉曄](../Page/劉曄.md "wikilink")

#### 晋朝

  - [刘毅](../Page/刘毅.md "wikilink")
  - [刘暾](../Page/刘暾.md "wikilink")
  - [刘柳](../Page/刘柳.md "wikilink")
  - [刘耽](../Page/刘耽.md "wikilink")
  - [刘道怜](../Page/刘道怜.md "wikilink")
  - [刘穆之](../Page/刘穆之.md "wikilink")
  - [刘裕](../Page/刘裕.md "wikilink")

#### 五胡十六国

  - [刘宣](../Page/刘宣.md "wikilink")
  - [刘欢乐](../Page/刘欢乐.md "wikilink")
  - [刘聪](../Page/刘聪.md "wikilink")
  - [刘裕](../Page/刘裕.md "wikilink")
  - [刘殷](../Page/刘殷.md "wikilink")
  - [刘义](../Page/刘义.md "wikilink")
  - [刘隆](../Page/刘隆.md "wikilink")
  - [刘述](../Page/刘述.md "wikilink")
  - [刘均](../Page/刘均.md "wikilink")
  - [刘雅](../Page/刘雅.md "wikilink")
  - [刘群](../Page/刘群.md "wikilink")
  - [刘琦](../Page/刘琦.md "wikilink")

#### 南北朝

  - [刘义康](../Page/刘义康.md "wikilink")
  - [刘义庆](../Page/刘义庆.md "wikilink")
  - [刘义宣](../Page/刘义宣.md "wikilink")
  - [刘义恭](../Page/刘义恭.md "wikilink")
  - [刘宏](../Page/刘宏.md "wikilink")
  - [刘恢](../Page/刘恢.md "wikilink")
  - [刘延孙](../Page/刘延孙.md "wikilink")
  - [刘秀之](../Page/刘秀之.md "wikilink")
  - [刘遵考](../Page/刘遵考.md "wikilink")
  - [刘袆](../Page/刘袆.md "wikilink")
  - [刘昶](../Page/刘昶.md "wikilink")
  - [刘休仁](../Page/刘休仁.md "wikilink")
  - [刘休范](../Page/刘休范.md "wikilink")
  - [刘秉](../Page/刘秉.md "wikilink")
  - [刘勔](../Page/刘勔.md "wikilink")
  - [刘絜](../Page/刘絜.md "wikilink")
  - [刘泥](../Page/刘泥.md "wikilink")
  - [刘洪徽](../Page/刘洪徽.md "wikilink")
  - [刘昉](../Page/刘昉.md "wikilink")

#### 唐朝

  - [刘文静](../Page/刘文静.md "wikilink")
  - [刘洎](../Page/刘洎.md "wikilink")
  - [刘祥道](../Page/刘祥道.md "wikilink")
  - [刘仁轨](../Page/刘仁轨.md "wikilink")
  - [刘景先](../Page/刘景先.md "wikilink")
  - [刘袆之](../Page/刘袆之.md "wikilink")
  - [刘幽求](../Page/刘幽求.md "wikilink")
  - [刘晏](../Page/刘晏.md "wikilink")
  - [刘从一](../Page/刘从一.md "wikilink")
  - [刘滋](../Page/刘滋.md "wikilink")
  - [刘瑑](../Page/刘瑑.md "wikilink")
  - [刘邺](../Page/刘邺.md "wikilink")
  - [刘瞻](../Page/刘瞻.md "wikilink")
  - [刘崇望](../Page/刘崇望.md "wikilink")

#### 五代十国

  - [刘昫](../Page/刘昫.md "wikilink")
  - [刘濬](../Page/刘濬.md "wikilink")
  - [刘弘昌](../Page/刘弘昌.md "wikilink")
  - [刘弘杲](../Page/刘弘杲.md "wikilink")
  - [刘继文](../Page/刘继文.md "wikilink")
  - [刘继颙](../Page/刘继颙.md "wikilink")

#### 宋朝、辽国、西夏国、金国

  - [刘沆](../Page/刘沆.md "wikilink")
  - [刘挚](../Page/刘挚.md "wikilink")
  - [刘正夫](../Page/刘正夫.md "wikilink")
  - [刘居言](../Page/刘居言.md "wikilink")
  - [刘晟](../Page/刘晟.md "wikilink")
  - [刘慎行](../Page/刘慎行.md "wikilink")
  - [刘京](../Page/刘京.md "wikilink")
  - [刘彦宗](../Page/刘彦宗.md "wikilink")
  - [刘筈](../Page/刘筈.md "wikilink")

### 元朝

  - [刘秉忠](../Page/刘秉忠.md "wikilink")

#### 明朝

  - [刘定之](../Page/刘定之.md "wikilink")
  - [刘珝](../Page/刘珝.md "wikilink")
  - [刘吉](../Page/刘吉.md "wikilink")
  - [刘健](../Page/刘健.md "wikilink")
  - [刘宇](../Page/刘宇.md "wikilink")
  - [刘忠](../Page/刘忠.md "wikilink")
  - [刘一燝](../Page/刘一燝.md "wikilink")
  - [刘鸿训](../Page/刘鸿训.md "wikilink")
  - [刘宇亮](../Page/刘宇亮.md "wikilink")
  - [刘中藻](../Page/刘中藻.md "wikilink")
  - [刘沂春](../Page/刘沂春.md "wikilink")
  - [刘麟长](../Page/刘麟长.md "wikilink")

#### 清朝

  - [刘正宗](../Page/刘正宗.md "wikilink")
  - [刘统勋](../Page/刘统勋.md "wikilink")
  - [刘纶](../Page/刘纶.md "wikilink")
  - [刘墉](../Page/刘墉.md "wikilink")
  - [刘权之](../Page/刘权之.md "wikilink")

### 中共领导人

  - [刘少奇](../Page/刘少奇.md "wikilink")
  - [刘伯承](../Page/刘伯承.md "wikilink")
  - [刘华清](../Page/刘华清.md "wikilink")
  - [刘云山](../Page/刘云山.md "wikilink")
  - [刘淇](../Page/刘淇.md "wikilink")
  - [刘延东](../Page/刘延东.md "wikilink")
  - [刘奇葆](../Page/刘奇葆.md "wikilink")

<!-- end list -->

  - [刘家语](../Page/刘家语.md "wikilink")
  - [刘复之](../Page/刘复之.md "wikilink")
  - [刘宁一](../Page/刘宁一.md "wikilink")
  - [刘澜涛](../Page/刘澜涛.md "wikilink")
  - [刘斐](../Page/刘斐.md "wikilink")
  - [刘靖基](../Page/刘靖基.md "wikilink")
  - [刘晓峰](../Page/刘晓峰.md "wikilink")

### 其他名人

  - [刘交](../Page/刘交.md "wikilink")
  - [刘濞](../Page/刘濞.md "wikilink")
  - [刘安](../Page/刘安.md "wikilink")
  - [刘武](../Page/刘武.md "wikilink")
  - [刘细君](../Page/刘细君.md "wikilink")
  - [刘解忧](../Page/刘解忧.md "wikilink")
  - [刘向](../Page/刘向.md "wikilink")
  - [刘歆](../Page/刘歆.md "wikilink")
  - [刘宠](../Page/刘宠.md "wikilink")
  - [刘洪](../Page/刘洪.md "wikilink")
  - [刘徽](../Page/刘徽.md "wikilink")
  - [刘德升](../Page/刘德升.md "wikilink")
  - [刘熙](../Page/刘熙.md "wikilink")
  - [劉昭文](../Page/劉昭文.md "wikilink")：香港配音員
  - [劉奕希](../Page/劉奕希.md "wikilink")：香港配音員
  - [劉惠雲](../Page/劉惠雲.md "wikilink")：香港配音員
  - [刘繇](../Page/刘繇.md "wikilink")
  - [刘表](../Page/刘表.md "wikilink")
  - [刘焉](../Page/刘焉.md "wikilink")
  - [刘璋](../Page/刘璋.md "wikilink")
  - [刘晔](../Page/刘晔.md "wikilink")
  - [刘桢](../Page/刘桢.md "wikilink")
  - [刘伶](../Page/刘伶.md "wikilink")
  - [刘琨](../Page/刘琨.md "wikilink")
  - [刘牢之](../Page/刘牢之.md "wikilink")

<!-- end list -->

  - 劉周勝： 劉周勝牧師 （ Reverend Paul Lau chow sing )
    曾經在香港和紐西蘭傳道，二零零一年，劉牧師出任英國倫敦saint
    Martin in the field 聖公會牧師

`   劉牧師已婚，妻子蔡鳯鳴師母，現時是Saint Martin in the field 聖公會助理牧師，兩人育有壹女壹子. `

  - [刘穆之](../Page/刘穆之.md "wikilink")
  - [刘义庆](../Page/刘义庆.md "wikilink")
  - [刘孝绰](../Page/刘孝绰.md "wikilink")
  - [刘孝威](../Page/刘孝威.md "wikilink")
  - [刘令娴](../Page/刘令娴.md "wikilink")
  - [刘勰](../Page/刘勰.md "wikilink")
  - [刘焯](../Page/刘焯.md "wikilink")
  - [刘炫](../Page/刘炫.md "wikilink")
  - [刘弘基](../Page/刘弘基.md "wikilink")
  - [刘三姐](../Page/刘三姐.md "wikilink")
  - [刘知几](../Page/刘知几.md "wikilink")
  - [刘希夷](../Page/刘希夷.md "wikilink")
  - [刘长卿](../Page/刘长卿.md "wikilink")
  - [刘禹锡](../Page/刘禹锡.md "wikilink")
  - [刘海蟾](../Page/刘海蟾.md "wikilink")
  - [刘昫](../Page/刘昫.md "wikilink")
  - [刘攽](../Page/刘攽.md "wikilink")
  - [刘敞](../Page/刘敞.md "wikilink")
  - [刘恕](../Page/刘恕.md "wikilink")
  - [刘金定](../Page/刘金定.md "wikilink")
  - [刘娥](../Page/刘娥.md "wikilink")
  - [刘锜](../Page/刘锜.md "wikilink")
  - [刘唐](../Page/刘唐.md "wikilink")
  - [刘过](../Page/刘过.md "wikilink")
  - [刘松年](../Page/刘松年.md "wikilink")
  - [刘仙伦](../Page/刘仙伦.md "wikilink")
  - [刘克庄](../Page/刘克庄.md "wikilink")
  - [刘辰翁](../Page/刘辰翁.md "wikilink")
  - [刘完素](../Page/刘完素.md "wikilink")
  - [刘元](../Page/刘元.md "wikilink")
  - [刘福通](../Page/刘福通.md "wikilink")
  - [刘基](../Page/刘基.md "wikilink")
  - [刘纯](../Page/刘纯.md "wikilink")
  - [刘天和](../Page/刘天和.md "wikilink")
  - [刘应节](../Page/刘应节.md "wikilink")
  - [刘显](../Page/刘显.md "wikilink")
  - [刘宗周](../Page/刘宗周.md "wikilink")
  - [刘永福](../Page/刘永福.md "wikilink")
  - [刘铭传](../Page/刘铭传.md "wikilink")
  - [刘锦棠](../Page/刘锦棠.md "wikilink")
  - [刘坤一](../Page/刘坤一.md "wikilink")
  - [刘秉璋](../Page/刘秉璋.md "wikilink")
  - [刘光才](../Page/刘光才.md "wikilink")
  - [刘光第](../Page/刘光第.md "wikilink")
  - [刘步蟾](../Page/刘步蟾.md "wikilink")
  - [刘鹗](../Page/刘鹗.md "wikilink")
  - [刘湘](../Page/刘湘.md "wikilink")
  - [劉濤](../Page/劉濤.md "wikilink")
  - [刘文辉](../Page/刘文辉.md "wikilink")
  - [刘放吾](../Page/刘放吾.md "wikilink")
  - [刘志丹](../Page/刘志丹.md "wikilink")
  - [刘胡兰](../Page/刘胡兰.md "wikilink")
  - [刘师培](../Page/刘师培.md "wikilink")
  - [刘半农](../Page/刘半农.md "wikilink")
  - [刘天华](../Page/刘天华.md "wikilink")
  - [刘大年](../Page/刘大年.md "wikilink")
  - [刘长春](../Page/刘长春.md "wikilink")
  - [劉悅](../Page/劉悅.md "wikilink")
  - [刘欢](../Page/刘欢.md "wikilink")
  - [刘德华](../Page/刘德华.md "wikilink")
  - [刘青云](../Page/刘青云.md "wikilink")
  - [刘晓庆](../Page/刘晓庆.md "wikilink")
  - [劉曉波](../Page/劉曉波.md "wikilink")
  - [劉國庚](../Page/劉國庚.md "wikilink")
  - [劉明峰](../Page/劉明峰.md "wikilink")
  - [劉明湘](../Page/劉明湘.md "wikilink")
  - [刘嘉玲](../Page/刘嘉玲.md "wikilink")
  - [刘若英](../Page/刘若英.md "wikilink")
  - [刘亦菲](../Page/刘亦菲.md "wikilink")
  - [刘诗诗](../Page/刘诗诗.md "wikilink")
  - [劉艾立](../Page/劉艾立.md "wikilink")
  - [刘谦](../Page/刘谦.md "wikilink")
  - [刘永好](../Page/刘永好.md "wikilink")
  - [刘长乐](../Page/刘长乐.md "wikilink")
  - [刘强东](../Page/刘强东.md "wikilink")
  - [刘镇伟](../Page/刘镇伟.md "wikilink")
  - [刘伟强](../Page/刘伟强.md "wikilink")
  - [刘猛](../Page/刘猛.md "wikilink")
  - [刘大白](../Page/刘大白.md "wikilink")
  - [刘丹](../Page/刘丹.md "wikilink")
  - [刘恺威](../Page/刘恺威.md "wikilink")
  - [刘彰顺](../Page/刘彰顺.md "wikilink")
  - [刘兆玄](../Page/刘兆玄.md "wikilink")
  - [劉坤銘](../Page/劉坤銘.md "wikilink")
  - [劉兆佳](../Page/劉兆佳.md "wikilink")
  - [刘焯华](../Page/刘焯华.md "wikilink")
  - [刘永坦](../Page/刘永坦.md "wikilink")
  - [刘克峰](../Page/刘克峰.md "wikilink")
  - [刘威](../Page/刘威.md "wikilink")
  - [刘佩琦](../Page/刘佩琦.md "wikilink")
  - [刘雪华](../Page/刘雪华.md "wikilink")
  - [刘家良](../Page/刘家良.md "wikilink")
  - [刘家辉](../Page/刘家辉.md "wikilink")
  - [刘文正](../Page/刘文正.md "wikilink")
  - [刘芳菲](../Page/刘芳菲.md "wikilink")
  - [刘粤军](../Page/刘粤军.md "wikilink")
  - [刘永清](../Page/刘永清.md "wikilink")
  - [劉峻緯](../Page/劉峻緯.md "wikilink")
  - [劉祿存](../Page/劉祿存.md "wikilink")
  - [劉容嘉](../Page/劉容嘉.md "wikilink")
  - [劉書宏](../Page/劉書宏.md "wikilink")
  - [刘烨](../Page/刘烨.md "wikilink")
  - [刘惜君](../Page/刘惜君.md "wikilink")
  - [刘力扬](../Page/刘力扬.md "wikilink")
  - [刘铁男](../Page/刘铁男.md "wikilink")
  - [刘志军](../Page/刘志军.md "wikilink")
  - [刘玉玲](../Page/刘玉玲.md "wikilink")
  - [刘海粟](../Page/刘海粟.md "wikilink")
  - [刘青山](../Page/刘青山.md "wikilink")
  - [刘仁静](../Page/刘仁静.md "wikilink")
  - [刘小锋](../Page/刘小锋.md "wikilink")
  - [刘伯坚](../Page/刘伯坚.md "wikilink")
  - [刘同](../Page/刘同.md "wikilink")
  - [刘墉](../Page/刘墉.md "wikilink")
  - [刘汉](../Page/刘汉.md "wikilink")
  - [刘建宏](../Page/刘建宏.md "wikilink")
  - [刘佳](../Page/刘佳.md "wikilink")
  - [刘松仁](../Page/刘松仁.md "wikilink")
  - [刘仪伟](../Page/刘仪伟.md "wikilink")
  - [刘江](../Page/刘江.md "wikilink")
  - [刘劲](../Page/刘劲.md "wikilink")
  - [刘銮雄](../Page/刘銮雄.md "wikilink")
  - [劉鑾鴻](../Page/劉鑾鴻.md "wikilink")
  - [刘亚津](../Page/刘亚津.md "wikilink")
  - [刘宝瑞](../Page/刘宝瑞.md "wikilink")
  - [刘家昌](../Page/刘家昌.md "wikilink")
  - [刘在石](../Page/刘在石.md "wikilink")
  - [刘荷娜](../Page/刘荷娜.md "wikilink")
  - [刘之冰](../Page/刘之冰.md "wikilink")
  - [刘美君](../Page/刘美君.md "wikilink")
  - [刘美含](../Page/刘美含.md "wikilink")
  - [刘威葳](../Page/刘威葳.md "wikilink")
  - [刘蓓](../Page/刘蓓.md "wikilink")
  - [刘忻](../Page/刘忻.md "wikilink")
  - [刘诗昆](../Page/刘诗昆.md "wikilink")
  - [刘慈欣](../Page/刘慈欣.md "wikilink")
  - [刘子枫](../Page/刘子枫.md "wikilink")
  - [刘洵](../Page/刘洵.md "wikilink")
  - [刘索拉](../Page/刘索拉.md "wikilink")
  - [刘德凯](../Page/刘德凯.md "wikilink")
  - [刘鸿生](../Page/刘鸿生.md "wikilink")
  - [刘继卣](../Page/刘继卣.md "wikilink")
  - [刘耕宏](../Page/刘耕宏.md "wikilink")
  - [刘鹏](../Page/刘鹏.md "wikilink")
  - [刘慧](../Page/刘慧.md "wikilink")
  - [刘德一](../Page/刘德一.md "wikilink")
  - [刘云天](../Page/刘云天.md "wikilink")
  - [刘能](../Page/刘能.md "wikilink")
  - [劉永才](../Page/永才.md "wikilink")
  - [劉千石](../Page/劉千石.md "wikilink")
  - [劉江華](../Page/劉江華.md "wikilink")
  - [劉皇發](../Page/劉皇發.md "wikilink")
  - [劉健儀](../Page/劉健儀.md "wikilink")
  - [劉慧卿](../Page/劉慧卿.md "wikilink")
  - [葉劉淑儀](../Page/葉劉淑儀.md "wikilink")
  - [劉小麗](../Page/劉小麗.md "wikilink")
  - [刘伟](../Page/刘伟.md "wikilink")
  - [刘伯明](../Page/刘伯明.md "wikilink")
  - [刘旺](../Page/刘旺.md "wikilink")
  - [刘洋](../Page/刘洋.md "wikilink")
  - [刘杰](../Page/刘杰.md "wikilink")
  - [刘敏](../Page/刘敏.md "wikilink")
  - [刘芳](../Page/刘芳.md "wikilink")
  - [刘刚](../Page/刘刚.md "wikilink")
  - [刘波](../Page/刘波.md "wikilink")
  - [刘强](../Page/刘强.md "wikilink")
  - [刘勇](../Page/刘勇.md "wikilink")
  - [刘勇威](../Page/刘勇威.md "wikilink")
  - [刘超](../Page/刘超.md "wikilink")
  - [刘丽](../Page/刘丽.md "wikilink")
  - [刘军](../Page/刘军.md "wikilink")
  - [刘丽英](../Page/刘丽英.md "wikilink")
  - [刘景范](../Page/刘景范.md "wikilink")
  - [刘锡五](../Page/刘锡五.md "wikilink")
  - [刘格平](../Page/刘格平.md "wikilink")
  - [刘其人](../Page/刘其人.md "wikilink")
  - [刘顺元](../Page/刘顺元.md "wikilink")
  - [刘型](../Page/刘型.md "wikilink")
  - [刘建章](../Page/刘建章.md "wikilink")
  - [刘澜波](../Page/刘澜波.md "wikilink")
  - [刘锡荣](../Page/刘锡荣.md "wikilink")
  - [刘峰岩](../Page/刘峰岩.md "wikilink")
  - [刘家义](../Page/刘家义.md "wikilink")
  - [刘滨](../Page/刘滨.md "wikilink")
  - [刘金国](../Page/刘金国.md "wikilink")
  - [刘建国](../Page/刘建国.md "wikilink")
  - [刘在石](../Page/刘在石.md "wikilink")
  - [刘小锋](../Page/刘小锋.md "wikilink")
  - [刘晓峰](../Page/刘晓峰.md "wikilink")
  - [劉佩玥](../Page/劉佩玥.md "wikilink")
  - [劉逸軒](../Page/劉逸軒.md "wikilink")
  - [刘婧荦](../Page/刘婧荦.md "wikilink")
    旅日[中國籍](../Page/中國.md "wikilink")[日本](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")，日本[青二事務所所屬](../Page/青二事務所.md "wikilink")，出生於[中國](../Page/中國.md "wikilink")[北京市](../Page/北京市.md "wikilink")，畢業於[北京外國語大學](../Page/北京外國語大學.md "wikilink")，曾就讀於[日本理工學院專門學校](../Page/日本理工學院專門學校.md "wikilink")，日本聲優界中唯二的[中國人聲優](../Page/中國人.md "wikilink")，另一人是[アプトプロ株式會社所屬的](../Page/アプトプロ株式會社.md "wikilink")[王怜馨現活躍於日本](../Page/王怜馨.md "wikilink")、中國兩地配音圈
  - [劉凡藝](../Page/劉凡藝.md "wikilink")
  - [劉聖雪](../Page/劉聖雪.md "wikilink")
  - [劉淯善](../Page/劉淯善.md "wikilink")
  - [刘少雄](../Page/刘少雄.md "wikilink") 洋名vincent lau,
    一九六零年在香港出生，一九八零年代至一九九零年代初是亚洲电视甘草演员，在綜合節目「綜合86」節目中扮演冒牌錶經纪徐仁富，在電視劇《
    夜琉璃》扮演黃仔，在電視劇《 司機大佬》扮演旅行社經理等等，劉少雄現時淡出娛樂圈，但參加樂團演出. 此外，刘少雄是一名香港消防队教练。

( 相關短片: <https://www.youtube.com/watch?v=0P5O14cbOQE&t=300s>,
<https://www.youtube.com/watch?v=8Z__TY7DeXk&t=30s> )

## 富豪

据2017年福布斯中国400富豪榜，全國前200名共7名次（其中4名次為夫婦或家族）刘姓富豪（排名、居住地、公司）如下：\[4\]

  -
    [刘强东](../Page/刘强东.md "wikilink")（18、[北京](../Page/北京.md "wikilink")、[京东](../Page/京东.md "wikilink")）
    [刘永行](../Page/刘永行.md "wikilink")（28、[上海](../Page/上海.md "wikilink")、东方希望集团）
    [刘永好家族](../Page/刘永好.md "wikilink")，刘永行的兄弟\[5\]（46、[四川](../Page/四川.md "wikilink")[成都](../Page/成都.md "wikilink")、[新希望集团](../Page/新希望集团.md "wikilink")）
    [刘忠田家族](../Page/刘忠田.md "wikilink")（67、[辽宁](../Page/辽宁.md "wikilink")[辽阳](../Page/辽阳.md "wikilink")、[中国忠旺](../Page/中国忠旺.md "wikilink")）

<!-- end list -->

  -
    [刘明辉](../Page/刘明辉\(香港\).md "wikilink")（147、[香港](../Page/香港.md "wikilink")、[中国燃气](../Page/中国燃气.md "wikilink")）
    [刘志强夫妇](../Page/刘志强_\(广东\).md "wikilink")（184、[广东](../Page/广东.md "wikilink")[广州](../Page/广州.md "wikilink")、广东香江集团）
    [刘汉元家族](../Page/刘汉元.md "wikilink")（199、[四川](../Page/四川.md "wikilink")[成都](../Page/成都.md "wikilink")、通威集团）

## 参见

  - [中国姓氏排名](../Page/中国姓氏排名.md "wikilink")

## 参考资料

## 外部連結

  - [黃帝祠-百家姓](https://web.archive.org/web/20100122161522/http://www.chinaemperorhall.com/index.asp)

  - [劉氏家園](https://web.archive.org/web/20070114160111/http://www.htliu.net/)

  - [漢家劉氏網](https://web.archive.org/web/20090521211734/http://www.liu-home.com/)

  - [中国姓氏排行(2007年4月24日)](http://news.sina.com.cn/c/2007-04-24/161812855907.shtml)

  - [中国姓氏排行榜
    (2006年11月)](http://www.lajiaocity.com/publishhtml/33/2006-11-04/20061104072334.html)

[\*](../Category/刘姓.md "wikilink") [L](../Category/漢字姓氏.md "wikilink")
[Category:中國姓氏](../Category/中國姓氏.md "wikilink")
[Category:朝鮮語姓氏](../Category/朝鮮語姓氏.md "wikilink")

1.  《唐会要·卷三十六》：长安四年，凤阁舍人刘知几。撰《刘氏》三卷，推汉氏为陆终苗裔，非尧之后。彭城丛亭里诸刘，出自宣帝子楚孝王嚣曾孙司徒居巢侯刘恺之后，不承楚元王交。皆按据明白前代所误，虽为流俗所讥，学者服其该博。
2.  《新唐书·卷一百三十二·列传第五十七》：子玄内负有所未尽，乃委国史于吴兢，别撰《刘氏家史》及《谱考》。上推汉为陆终苗裔，非尧后；彭城丛亭里诸刘，出楚孝王嚣曾孙居巢侯般，不承元王。按据明审，议者高其博。
3.
4.
5.  [1999年富豪榜上的大佬
    如今还剩这3家在榜](https://m.sohu.com/a/260091147_157078?_f=m-index_important_news_16&spm=smwp.home.fd-important.18.1539888914521FVXqUXa)搜狐新闻，2018年