[EugeneMcCarthy.jpg](https://zh.wikipedia.org/wiki/File:EugeneMcCarthy.jpg "fig:EugeneMcCarthy.jpg")

**尤金·约瑟夫·“吉恩”·麥卡錫**（[英文名](../Page/英文.md "wikilink")：，），[美国政治家](../Page/美国.md "wikilink")，来自[明尼苏达州的](../Page/明尼苏达.md "wikilink")[国会议员](../Page/美国国会.md "wikilink")。1949年至1959年任[联邦众议员](../Page/美国众议院.md "wikilink")，1959年至1971年任[联邦参议员](../Page/美国参议院.md "wikilink")。

在[1968年美国总统选舉中](../Page/1968年美国总统选舉.md "wikilink")，麦卡锡试图获得[民主党候选人提名未果](../Page/民主党_\(美國\).md "wikilink")，他的竞选纲领是反对[越南战争](../Page/越南战争.md "wikilink")。他总共五次参加[美国总统大选](../Page/美国总统.md "wikilink")，然而未获得民主党提名，也不是民主黨具競爭性的總統候選人。由於反對[吉米·卡特加上認為其政策差勁](../Page/吉米·卡特.md "wikilink")，在[1980年美國總統选舉中](../Page/1980年美國總統选舉.md "wikilink")，他決定支持[里根](../Page/罗纳德·里根.md "wikilink")。\[1\]在1982年再度參與參議員選舉，但再度敗選。

## 参考文献

<div class="references-small">

<references />

</div>

[Category:美國男作家](../Category/美國男作家.md "wikilink")
[Category:美国回忆录撰写人](../Category/美国回忆录撰写人.md "wikilink")
[Category:德國裔美國人](../Category/德國裔美國人.md "wikilink")
[Category:爱尔兰裔美国人](../Category/爱尔兰裔美国人.md "wikilink")
[Category:美国政治作家](../Category/美国政治作家.md "wikilink")
[Category:死於帕金森氏症的人](../Category/死於帕金森氏症的人.md "wikilink")
[Category:美国民主党联邦参议员](../Category/美国民主党联邦参议员.md "wikilink")
[Category:美国民主党联邦众议员](../Category/美国民主党联邦众议员.md "wikilink")
[Category:美国自由主义](../Category/美国自由主义.md "wikilink")
[Category:明尼蘇達州聯邦參議員](../Category/明尼蘇達州聯邦參議員.md "wikilink")
[Category:明尼蘇達大學校友](../Category/明尼蘇達大學校友.md "wikilink")

1.