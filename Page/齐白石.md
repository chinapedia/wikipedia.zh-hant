**齐白石**（），[湖南](../Page/湖南.md "wikilink")[湘潭人](../Page/湘潭.md "wikilink")，是当代[中国的](../Page/中国.md "wikilink")[國畫](../Page/中國畫.md "wikilink")[画家](../Page/画家.md "wikilink")。原[名](../Page/名.md "wikilink")**齐纯芝**，字**渭清**，[祖父取号](../Page/祖父.md "wikilink")**兰亭**，老师取名**齐璜**，号**濒生**，[别号](../Page/别号.md "wikilink")**寄萍老人**、**白石山人**，後人常将“山人”二字略去，故后常号“白石”。齐白石也和[张大千并称](../Page/张大千.md "wikilink")“**南张北齐**”。

## 生平

齊白石於1864年[元旦](../Page/元旦.md "wikilink")（同治二年十一月廿二日）出生於[湖南省](../Page/湖南省.md "wikilink")[长沙府湘潭县](../Page/长沙府.md "wikilink")（今湖南省[湘潭县](../Page/湘潭县.md "wikilink")）[白石铺](../Page/白石铺.md "wikilink")[杏子坞农民家庭](../Page/杏子坞.md "wikilink")，原名**齊純芝**。自幼體弱多病。由於家族以務農為生，祖父齊萬秉認為農耕不能賺錢，於齊白石4歲時起教授寫字。於8歲時在[公公所開設之蒙館學習](../Page/公公.md "wikilink")，半年後輟學，輟學後，協助家中務農。14岁起作木匠，學習雕花木工。後來兼習繪畫，並拜蕭薌陔為師。25歲時起拜名士[胡沁園](../Page/胡沁園.md "wikilink")、[陳少蕃等為師](../Page/陳少蕃.md "wikilink")\[1\]，由胡沁園替之取名為璜，號瀕生，因家中靠近白石鋪，故取別號白石山人。學習詩、書、畫、篆刻。並開始兼以賣畫為生，不再以雕花木工賺錢。
[Ci-Byshi_cikada.jpg](https://zh.wikipedia.org/wiki/File:Ci-Byshi_cikada.jpg "fig:Ci-Byshi_cikada.jpg")
32歲時起對刻印產生濃厚興趣，開始向名家學習刻印。35歲時拜學者[王湘綺為師](../Page/王湘綺.md "wikilink")。1903年39岁时第一次到北京。40岁時，開始周遊天下，以后到南北各地游历，饱览名山大川，使他開闊了眼界，師法大自然，充實了作品「造化」內容。1905年41歲時在当时广西首府[桂林卖画为生](../Page/桂林.md "wikilink")，認識了[蔡锷和張中正和尚](../Page/蔡锷.md "wikilink")（即[黃興](../Page/黃興.md "wikilink")）。42歲時起遊覽[廣州](../Page/廣州.md "wikilink")、[廣西](../Page/廣西.md "wikilink")、[重慶](../Page/重慶.md "wikilink")、[越南](../Page/越南.md "wikilink")、[上海](../Page/上海.md "wikilink")、[南京](../Page/南京.md "wikilink")，期間，寫畫風格由工筆畫轉為[寫意畫](../Page/寫意畫.md "wikilink")。

1917年起決定於[北京發展](../Page/北京.md "wikilink")，以卖画刻印为生，並結識了[陳師曾](../Page/陳師曾.md "wikilink")（[陈寅恪之兄](../Page/陈寅恪.md "wikilink")），陈师曾曾对[胡佩衡說齐白石的作品](../Page/胡佩衡.md "wikilink")“思想新奇，不是一般画家能画得出来的……我们应该特别帮助这位乡下老农，为他的绘画宣传”。1922年，陈师曾携齐白石的画作前往日本，在东京的中日联合绘画展览会上展出，使得齐白石在日本名噪一时，画价亦爆增。齐白石在其自传中写道：“二尺长的纸，卖到二百五十银币……还听说法国人在东京，选了师曾和我两人的画，加入巴黎艺术展览会……我做了一首诗，作为纪念：‘曾点胭脂作杏花，百金尺纸众争夸；平生羞杀传名姓，海国都知老画家。’……从此以后，我卖画生涯，一天比一天兴盛起来。这都是师曾提拔我的一番厚意”。陈病故后，齐白石对[张次溪說](../Page/张次溪.md "wikilink")：“我如没有师曾的提携，我的画名，不会有今天”\[2\]。受陳師曾影響，他創造出自成一家的畫畫風格，亦即紅花墨葉的大膽風格，以原本不協調的純紅色和深墨黑用在一起，形成畫面鮮明的對比，表現了畫面上清新、樸實的感覺，实现了“衰年变法”。並學習[徐渭](../Page/徐渭.md "wikilink")、[石濤](../Page/石濤.md "wikilink")，在学习石涛、[朱耷](../Page/朱耷.md "wikilink")、徐渭的基础上，吸收[吴昌硕的技法](../Page/吴昌硕.md "wikilink")，終於自成一家。後結識[梅蘭芳](../Page/梅蘭芳.md "wikilink")、[艾青](../Page/艾青.md "wikilink")、[徐悲鴻等人](../Page/徐悲鴻.md "wikilink")。1922年，在陳師曾協助下，其作品起於外國展出，受到重視。後受[林風眠邀請](../Page/林風眠.md "wikilink")，起到[北京藝術專門學校教授中國畫](../Page/北京藝術專門學校.md "wikilink")。也曾在[张恨水的](../Page/张恨水.md "wikilink")[北平私立北华美术专科学校教授国画](../Page/北平私立北华美术专科学校.md "wikilink")。

据说，侨居海外的[李铁夫曾致友人信中曾认为当代国内真正的画家只有](../Page/李鐵夫.md "wikilink")「齐白石、[吴昌硕二人而已](../Page/吳昌碩.md "wikilink")」，可見其對齊白石十分推崇。

1937年，[日本入侵中國](../Page/日本.md "wikilink")，齊白石因在外國享譽，受日本人拉攏，堅拒屈服，更遭扣押。後日本當局懼怕負上迫害藝術家罪名，扣押三天後釋放。

1949年，[中華人民共和國成立](../Page/中華人民共和國.md "wikilink")，齊白石因是木匠出身，以及与[毛泽东有同乡之谊受到中共表揚](../Page/毛泽东.md "wikilink")。並被聘為[中國美術學院名譽教授](../Page/中國美術學院.md "wikilink")。1953年他被选为中国美术家协会[主席](../Page/主席.md "wikilink")。1954年被选为[第一届全国人民代表大会代表](../Page/第一届全国人民代表大会.md "wikilink")。1955年，[東德總理訪問中華人民共和國](../Page/德意志民主共和国.md "wikilink")，代表德國藝術科學院，授予齊白石共和国艺术科学院通讯院士的荣誉状。1956年，世界和平理事会决定他为1955年度国际和平奖金的获得者。1957年出任北京中國畫院名譽院長，同年9月16日在[北京逝世](../Page/北京.md "wikilink")。享年93岁。

## 畫風

[墨蝦圖.jpg](https://zh.wikipedia.org/wiki/File:墨蝦圖.jpg "fig:墨蝦圖.jpg")\]\]

齐白石强调“作画妙在似与不似之间，太似为媚俗，不似为欺世。”，在篆刻方面自称“三百石印富翁”。晚年仍然细心观察虾、蟹、青蛙的游泳姿态，他的画自成章法，人物、山水、花鸟、草虫、蔬果、玩具都可入画，和20世纪世界[绘画](../Page/绘画.md "wikilink")[艺术的发展方向相符](../Page/艺术.md "wikilink")。

## 纪念

[The_Soviet_Union_1958_CPA_2116_stamp_(Qi_Baishi).jpg](https://zh.wikipedia.org/wiki/File:The_Soviet_Union_1958_CPA_2116_stamp_\(Qi_Baishi\).jpg "fig:The_Soviet_Union_1958_CPA_2116_stamp_(Qi_Baishi).jpg")
[Qibaishi_gongyuan.jpg](https://zh.wikipedia.org/wiki/File:Qibaishi_gongyuan.jpg "fig:Qibaishi_gongyuan.jpg")
齐白石的出生地湖南湘潭和晚年的居住地分别开辟了[湖南湘潭齐白石故居和](../Page/齐白石故居_\(湖南湘潭\).md "wikilink")[北京西城齐白石故居](../Page/齐白石故居_\(北京西城\).md "wikilink")，前者是[全国重点文物保护单位](../Page/全国重点文物保护单位.md "wikilink")，后者是[北京市文物保护单位](../Page/北京市文物保护单位.md "wikilink")。“[齐白石墓](../Page/齐白石墓.md "wikilink")”位于北京市[海淀区金山陵园](../Page/海淀区.md "wikilink")，墓碑碑文为[李苦禅所书](../Page/李苦禅.md "wikilink")。

另外，水星上位於4.2°S,
196.0°W的撞擊坑命名為[齊白石撞擊坑](../Page/齊白石撞擊坑.md "wikilink")\[3\]。

## 弟子

齐白石的弟子很多，其中著名的有李苦禅、李可染、王雪涛、王漱石、王铸九、许麟庐、陈大羽、李立、娄师白、董长青、张德文、陈国刚、秦慕儒等，其三子齐子如、戏剧大师梅兰芳、评剧名家新凤霞也是齐白石的弟子。

## 家庭

齐白石的第一位夫人是12岁时家里娶的童养媳陈春君，第二任夫人是齐白石57岁来北京后陈春君给物色的侧室胡宝珠，陈春君病故后扶正。胡宝珠因难产过世后，齐白石又找了一位夏文珠小姐，但子女反对，于是以护士的名义留在身边，7年后离开。

齐白石的子女有\[4\]：

  - 长子[齐良元](../Page/齐良元.md "wikilink")
  - 次子[齐良黼](../Page/齐良黼.md "wikilink")
  - 三子[齐良琨](../Page/齐良琨.md "wikilink")
  - 四子[齐良迟](../Page/齐良迟.md "wikilink")
  - 五子[齐良已](../Page/齐良已.md "wikilink")
  - 六子[齐良年](../Page/齐良年.md "wikilink")
  - 七子[齐良末](../Page/齐良末.md "wikilink")
  - 长女[齐菊如](../Page/齐菊如.md "wikilink")
  - 次女[齐阿梅](../Page/齐阿梅.md "wikilink")
  - 三女[齐良憐](../Page/齐良憐.md "wikilink")
  - 四女[齐良欢](../Page/齐良欢.md "wikilink")
  - 五女[齐良芷](../Page/齐良芷.md "wikilink")

## 参考文献

  -
  - 胡小花，《[齐白石的三位夫人](http://cswb.changsha.cn/cswb/20111024/cont_1_20_187725.htm)》，《[长沙晚报](../Page/长沙晚报.md "wikilink")》
    2011年10月24日 2012年12月22日查阅。

## 外部链接

  - [齐白石及其作品集](http://www.chinaonlinemuseum.com/painting-qi-baishi.php)
    - 中国在线博物馆
  - [齐白石著名作品中英文介紹](http://orionwebmuseum.blogspot.com)
  - [2011年列名世界拍賣收入記錄前15名的中國畫家名單已公佈 - 第2名為齊白石 Qi Baishi
    齐白石](https://web.archive.org/web/20120525204824/http://tw.knowledge.yahoo.com/question/article?qid=1712040607099)

[Category:湘潭县人](../Category/湘潭县人.md "wikilink")
[Category:中華民國畫家](../Category/中華民國畫家.md "wikilink")
[Category:中国篆刻家](../Category/中国篆刻家.md "wikilink")
[Category:中華民國書法家](../Category/中華民國書法家.md "wikilink")
[Category:中華人民共和國畫家](../Category/中華人民共和國畫家.md "wikilink")
[Category:中華人民共和國書法家](../Category/中華人民共和國書法家.md "wikilink")
[Category:中央文史研究馆馆员](../Category/中央文史研究馆馆员.md "wikilink")
[Category:第一届全国人大代表](../Category/第一届全国人大代表.md "wikilink")
[齊白石](../Category/齊白石.md "wikilink")
[Category:中华民国大陆时期教师](../Category/中华民国大陆时期教师.md "wikilink")
[Category:中华民国大陆时期艺术家](../Category/中华民国大陆时期艺术家.md "wikilink")
[Category:中国木雕](../Category/中国木雕.md "wikilink")
[Category:中国美术家协会主席](../Category/中国美术家协会主席.md "wikilink")

1.  齐白石，《白石老人自述》，第四章，第66-67页。
2.
3.  [Low-angle Impacts: A Look at Qi Baishi and
    Hovnatanian](http://messenger.jhuapl.edu/gallery/sciencePhotos/image.php?page=1&gallery_id=2&image_id=295)

4.