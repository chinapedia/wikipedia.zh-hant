**新民市**是[中国](../Page/中国.md "wikilink")[辽宁省](../Page/辽宁省.md "wikilink")[沈阳市下辖的一个](../Page/沈阳市.md "wikilink")[县级市](../Page/县级市.md "wikilink")，位于沈阳市西北部。

## 历史沿革

[嘉庆十八年](../Page/嘉庆.md "wikilink")(1813年)分承德(今沈阳)西部，广宁东部地区；设新民厅，(治所在新民屯)属奉天府。[光绪二十八年](../Page/光绪.md "wikilink")(1902年)改新民厅为府，辖镇安(今黑山)，彰武两县。

[民国二年](../Page/民国.md "wikilink")(1913年)撤销新民府、建立新民县，属奉天省辽沈道。

## 行政区划

下辖5个街道、20个镇、4个乡。

  - 街道：[东城街道](../Page/东城街道.md "wikilink")、[西城街道](../Page/西城街道.md "wikilink")、[辽滨街道](../Page/辽滨街道.md "wikilink")、[新柳街道](../Page/新柳街道.md "wikilink")、[新城街道](../Page/新城街道.md "wikilink")。
  - 镇：[大红旗镇](../Page/大红旗镇.md "wikilink")、[梁山镇](../Page/梁山镇.md "wikilink")、[大柳屯镇](../Page/大柳屯镇.md "wikilink")、[公主屯镇](../Page/公主屯镇.md "wikilink")、[兴隆镇](../Page/兴隆镇.md "wikilink")、[兴隆堡镇](../Page/兴隆堡镇.md "wikilink")、[胡台镇](../Page/胡台镇.md "wikilink")、[法哈牛镇](../Page/法哈牛镇.md "wikilink")、[前当堡镇](../Page/前当堡镇.md "wikilink")、[大民屯镇](../Page/大民屯镇.md "wikilink")、[柳河沟镇](../Page/柳河沟镇.md "wikilink")、[高台子镇](../Page/高台子镇.md "wikilink")、[张家屯镇](../Page/张家屯镇.md "wikilink")、[罗家房镇](../Page/罗家房镇.md "wikilink")、[三道岗子镇](../Page/三道岗子镇.md "wikilink")、[陶家屯镇](../Page/陶家屯镇.md "wikilink")、[东蛇山子镇](../Page/东蛇山子镇.md "wikilink")、[周坨子镇](../Page/周坨子镇.md "wikilink")、[金五台子镇](../Page/金五台子镇.md "wikilink")、[新农村镇](../Page/新农村镇.md "wikilink")。
  - 乡：[红旗乡](../Page/红旗乡.md "wikilink")、[卢屯乡](../Page/卢屯乡.md "wikilink")、[姚堡乡](../Page/姚堡乡.md "wikilink")、[于家窝堡乡](../Page/于家窝堡乡.md "wikilink")。

## 名人

[:Category: 新民人](../Category/_新民人.md "wikilink")

## 外部链接

  - [行政区划网：新民市](http://www.xzqh.org/quhua/21ln/0181xm.htm)

[新民市](../Category/新民市.md "wikilink")
[沈阳](../Category/辽宁县级市.md "wikilink")
[市](../Category/沈阳区县市.md "wikilink")