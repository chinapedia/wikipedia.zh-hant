**IE7Pro**是一個為[Internet
Explorer而設計的](../Page/Internet_Explorer.md "wikilink")-{[插件](../Page/插件.md "wikilink")}-（Plug-in或稱Add-on），目的是為了讓IE更好用也更加自訂化。最初是專為IE
7而開發的，所以命名為「IE7Pro」，但自0.9.14版起也開始支援舊版的IE 6。IE7Pro的作者是四位華裔的程式設計師：Daniel
Fang、Chris Li、Sean Chen 和Shaka Yu，他們同時也是第一代Orbit
Downloader的創始人。其項目經理為George Woo。

IE7Pro為IE增強了許多原有的功能，例如可以雙擊關閉Tab、從網址欄打開新Tab、管理Tab的網頁瀏覽歷史、記錄並恢復最後瀏覽的網頁、將選單提升到頂部、隱藏快速搜索欄、[Proxy快速切換](../Page/Proxy.md "wikilink")、增加同時連線的數量以提高網頁傳輸速度……等等。另外，IE7Pro也新增了一些IE所沒有的功能，例如[游標手勢](../Page/游標手勢.md "wikilink")、超級拖曳、網頁快速截圖及類似[Greasemonkey的用戶](../Page/Greasemonkey.md "wikilink")[腳本](../Page/腳本.md "wikilink")（Script）和用戶-{[插件](../Page/插件.md "wikilink")}-，而到了1.0及以上版本亦增加了簡易首頁、文字保留及書籤等特別的功能，這些設計都是在IE的主要競爭對手[Firefox上廣受好評的功能](../Page/Firefox.md "wikilink")。

## 外部連結

  - [IE7Pro官方網站](https://web.archive.org/web/20061206201124/http://www.ie7pro.com/)

[Category:Internet
Explorer](../Category/Internet_Explorer.md "wikilink")