**滨州市**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[山东省下辖的](../Page/山东省.md "wikilink")[地级市](../Page/地级市.md "wikilink")，位于山东省北部，[渤海西岸](../Page/渤海.md "wikilink")，[黄河贯穿全境](../Page/黄河.md "wikilink")。东接[东营市](../Page/东营市.md "wikilink")，南邻[淄博市](../Page/淄博市.md "wikilink")，西南与[济南市接壤](../Page/济南市.md "wikilink")，西邻[德州市](../Page/德州市.md "wikilink")，北临渤海。全市总面积9,172平方公里，人口385.9万。滨州相传为中国神话剧《[天仙配](../Page/天仙配.md "wikilink")》中男主角[董永的故乡](../Page/董永.md "wikilink")。[冬枣是当地的知名特产](../Page/冬枣.md "wikilink")。

## 历史

[秦朝今市境属](../Page/秦朝.md "wikilink")[齐郡](../Page/齐郡.md "wikilink")。[两汉时期](../Page/两汉.md "wikilink")，市境属[渤海](../Page/渤海郡.md "wikilink")、[平原](../Page/平原郡.md "wikilink")、[齐三郡](../Page/齐郡.md "wikilink")。[魏晋](../Page/魏晋.md "wikilink")[南北朝](../Page/南北朝.md "wikilink")，市境以[黄河为界分属](../Page/黄河.md "wikilink")[乐陵郡](../Page/乐陵郡.md "wikilink")、[乐安郡](../Page/乐安郡.md "wikilink")。[隋](../Page/隋朝.md "wikilink")[开皇六年](../Page/开皇.md "wikilink")（586年）置[棣州](../Page/棣州.md "wikilink")，治[阳信县](../Page/阳信县.md "wikilink")。[大业二年](../Page/大业.md "wikilink")（606年）改置[沧州](../Page/沧州.md "wikilink")，次年改为[渤海郡](../Page/渤海郡.md "wikilink")。[唐](../Page/唐朝.md "wikilink")[武德四年](../Page/武德.md "wikilink")（621年）复置棣州，不久废；[贞观十七年](../Page/贞观.md "wikilink")（643年）复置，治[厌次县](../Page/厌次县.md "wikilink")（今[惠民县东南](../Page/惠民县.md "wikilink")）。[垂拱四年](../Page/垂拱.md "wikilink")（688年）析[蒲台](../Page/蒲台.md "wikilink")、厌次两县地置[渤海县](../Page/渤海县.md "wikilink")，治今滨州市东。《太平寰宇记》曰：“以在渤海之滨为名。”属棣州。[天宝五年](../Page/天宝.md "wikilink")（746年）以其地咸卤，治所西迁至今滨州城区北。

[五代](../Page/五代.md "wikilink")[后汉置赡国军](../Page/后汉.md "wikilink")，[后周](../Page/后周.md "wikilink")[显德三年](../Page/显德.md "wikilink")（956年）改置[滨州](../Page/滨州.md "wikilink")，治渤海县。[元](../Page/元朝.md "wikilink")[至元二年](../Page/至元.md "wikilink")（1265年）移治今滨州城区。[明初](../Page/明朝.md "wikilink")，棣州改置为[乐安州](../Page/乐安州.md "wikilink")，后改[武定州](../Page/武定州.md "wikilink")，[洪武元年](../Page/洪武.md "wikilink")（1368年）废渤海县入滨州。武定州、滨州均属[济南府](../Page/济南府.md "wikilink")。[清](../Page/清朝.md "wikilink")[雍正二年](../Page/雍正.md "wikilink")（1724年），武定州、滨州升为[直隶州](../Page/直隶州.md "wikilink")。雍正十二年（1734年），武定州升为[武定府](../Page/武定府.md "wikilink")，滨州降为[散州](../Page/散州.md "wikilink")，隶属武定府，并置惠民县，为武定府[附郭县](../Page/附郭县.md "wikilink")。[民国二年](../Page/民国.md "wikilink")（1913年）废武定府，改滨州为滨县，属[岱北道](../Page/岱北道.md "wikilink")，次年改称[济南道](../Page/济南道.md "wikilink")。1925年属[武定道](../Page/武定道.md "wikilink")。1928年废道制，各县直属于省。1936年属山东省第五行政督察区，专署驻惠民县。1944年属渤海行署区。

[中华人民共和国成立后](../Page/中华人民共和国.md "wikilink")，市境分属[清河专区](../Page/清河专区.md "wikilink")、[垦利专区](../Page/垦利专区.md "wikilink")。1950年置[惠民专区](../Page/惠民专区.md "wikilink")，专署驻惠民县。1958年，惠民专区与[淄博市合并为](../Page/淄博市.md "wikilink")[淄博专区](../Page/淄博专区.md "wikilink")，专署驻张店。1961年复置惠民专区。1967年改称[惠民地区](../Page/惠民地区.md "wikilink")。1982年析滨县、博兴县置滨州市。1983年10月，析沾化、博兴县组建地级[东营市](../Page/东营市.md "wikilink")，并将[利津](../Page/利津.md "wikilink")、[垦利](../Page/垦利.md "wikilink")、[广饶](../Page/广饶.md "wikilink")3县划归东营市管辖，将[桓台县划归淄博市](../Page/桓台县.md "wikilink")。1987年2月，撤销滨县，并入滨州市。1989年12月，将[高青县划归淄博市](../Page/高青县.md "wikilink")。1992年3月，惠民地区更名为[滨州地区](../Page/滨州地区.md "wikilink")。2000年6月，撤销滨州地区，改设地级滨州市，原县级滨州市改设[滨城区](../Page/滨城区.md "wikilink")。2014年9月，撤销沾化县，改设[沾化区](../Page/沾化区.md "wikilink")。

## 气候

## 政治

### 现任领导

<table>
<caption>滨州市四大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党滨州市委员会.md" title="wikilink">中国共产党<br />
滨州市委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/滨州市人民代表大会.md" title="wikilink">滨州市人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/滨州市人民政府.md" title="wikilink">滨州市人民政府</a><br />
<br />
市长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议滨州市委员会.md" title="wikilink">中国人民政治协商会议<br />
滨州市委员会</a><br />
主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/佘春明.md" title="wikilink">佘春明</a>[1]</p></td>
<td><p><a href="../Page/崔洪刚.md" title="wikilink">崔洪刚</a>[2]</p></td>
<td><p><a href="../Page/张兆宏.md" title="wikilink">张兆宏</a>[3]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p>汉族</p></td>
<td><p>汉族</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p><a href="../Page/山东省.md" title="wikilink">山东省</a><a href="../Page/潍坊市.md" title="wikilink">潍坊市</a></p></td>
<td><p>山东省<a href="../Page/淄博市.md" title="wikilink">淄博市</a></p></td>
<td><p>山东省淄博市</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2019年1月</p></td>
<td><p>2013年3月</p></td>
<td><p>2017年2月</p></td>
<td></td>
</tr>
</tbody>
</table>

### 行政区划

现辖2个[市辖区](../Page/市辖区.md "wikilink")、4个[县](../Page/县_\(中华人民共和国\).md "wikilink")，代管1个[县级市](../Page/县级市.md "wikilink")。

  - 市辖区：[滨城区](../Page/滨城区.md "wikilink")、[沾化区](../Page/沾化区.md "wikilink")
  - 县级市：[邹平市](../Page/邹平市.md "wikilink")
  - 县：[惠民县](../Page/惠民县.md "wikilink")、[阳信县](../Page/阳信县.md "wikilink")、[无棣县](../Page/无棣县.md "wikilink")、[博兴县](../Page/博兴县.md "wikilink")

除正式行政区划外，滨州市还设立以下经济管理区：[滨州经济开发区](../Page/滨州经济开发区.md "wikilink")、[滨州高新技术产业开发区](../Page/滨州高新技术产业开发区.md "wikilink")、[北海新区](../Page/北海新区.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p><strong>滨州市行政区划图</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>区划代码[4]</p></td>
</tr>
<tr class="odd">
<td><p>街道</p></td>
</tr>
<tr class="even">
<td><p>371600</p></td>
</tr>
<tr class="odd">
<td><p>371602</p></td>
</tr>
<tr class="even">
<td><p>371603</p></td>
</tr>
<tr class="odd">
<td><p>371621</p></td>
</tr>
<tr class="even">
<td><p>371622</p></td>
</tr>
<tr class="odd">
<td><p>371623</p></td>
</tr>
<tr class="even">
<td><p>371625</p></td>
</tr>
<tr class="odd">
<td><p>371681</p></td>
</tr>
<tr class="even">
<td><p>注：滨城区数字包含滨州经济开发区所辖3街道及滨州高新技术产业开发区所辖2街道；无棣县数字包含北海新区所辖马山子镇。</p></td>
</tr>
</tbody>
</table>

## 人口

<table>
<caption><strong>滨州市各区（县）人口数据</strong></caption>
<thead>
<tr class="header">
<th><p>区划名称</p></th>
<th><p>常住人口[5]（2010年11月）</p></th>
<th><p>户籍人口[6]<br />
（2010年末）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>总计</p></td>
<td><p>比重<br />
（%）</p></td>
<td><p>每平方公里<br />
人口密度</p></td>
</tr>
<tr class="even">
<td><p>滨州市</p></td>
<td><p>3748474</p></td>
<td><p>100</p></td>
</tr>
<tr class="odd">
<td><p>滨城区</p></td>
<td><p>682717</p></td>
<td><p>18.21</p></td>
</tr>
<tr class="even">
<td><p>惠民县</p></td>
<td><p>602491</p></td>
<td><p>16.07</p></td>
</tr>
<tr class="odd">
<td><p>阳信县</p></td>
<td><p>427014</p></td>
<td><p>11.39</p></td>
</tr>
<tr class="even">
<td><p>无棣县</p></td>
<td><p>418687</p></td>
<td><p>11.17</p></td>
</tr>
<tr class="odd">
<td><p>沾化区</p></td>
<td><p>351672</p></td>
<td><p>9.38</p></td>
</tr>
<tr class="even">
<td><p>博兴县</p></td>
<td><p>487116</p></td>
<td><p>13.00</p></td>
</tr>
<tr class="odd">
<td><p>邹平市</p></td>
<td><p>778777</p></td>
<td><p>20.78</p></td>
</tr>
<tr class="even">
<td><p>注：滨城区的常住人口数据中包含滨州经济开发区116581人及滨州高新技术产业开发区62231人。</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

根据2010年[第六次全国人口普查](../Page/第六次全国人口普查.md "wikilink")，全市[常住人口为](../Page/常住人口.md "wikilink")374.85万人\[7\]，同[第五次全国人口普查相比](../Page/第五次全国人口普查.md "wikilink")，十年共增加了18.44万人。增长5.17%，年平均增长0.51%。其中，男性为188.40万人，占总人口的50.26%；女性为186.45万人，占总人口的49.74%，总人口性别比（以女性为100）为101.05。0－14岁的人口为60.15万人，占16.05
%；15－64岁的人口为277.95万人，占74.15%；65岁及以上的人口为36.75万人，占9.80%。

## 教育

### 高校

  - [滨州医学院](../Page/滨州医学院.md "wikilink")
  - [滨州学院](../Page/滨州学院.md "wikilink")
  - [滨州职业学院](../Page/滨州职业学院.md "wikilink")
  - [滨州技师学院](../Page/滨州技师学院.md "wikilink")
  - [滨州技术学院](../Page/滨州技术学院.md "wikilink")

### 中学

  - [北镇中学](../Page/北镇中学.md "wikilink")
  - [滨州市第一中学](../Page/滨州市第一中学.md "wikilink")
  - [滨州行知中学](../Page/滨州行知中学.md "wikilink")

## 名人

  - [孙武](../Page/孙武.md "wikilink")
  - [伏生](../Page/伏生.md "wikilink")
  - [东方朔](../Page/东方朔.md "wikilink")
  - [董永](../Page/董永.md "wikilink")
  - [王薄](../Page/王薄.md "wikilink")
  - [范仲淹](../Page/范仲淹.md "wikilink")
  - [李之仪](../Page/李之仪.md "wikilink")
  - [唐赛儿](../Page/唐赛儿.md "wikilink")
  - [明](../Page/明.md "wikilink")[孝恭章皇后](../Page/孫皇后_\(明宣宗\).md "wikilink")
  - [杜受田](../Page/杜受田.md "wikilink")
  - [李广田](../Page/李广田.md "wikilink")

## 参考文献

## 外部链接

  - [滨州市政府网站](http://www.binzhou.net)
  - [滨州市第一中学网站](http://www.bzyz.cn/)

[Category:山东地级市](../Category/山东地级市.md "wikilink")
[Category:滨州](../Category/滨州.md "wikilink")
[鲁](../Category/中国大城市.md "wikilink")
[鲁](../Category/国家卫生城市.md "wikilink")

1.
2.
3.
4.
5.
6.
7.