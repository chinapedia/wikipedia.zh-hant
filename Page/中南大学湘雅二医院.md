**中南大学湘雅二医院**，位于[中國](../Page/中國.md "wikilink")[湖南省](../Page/湖南省.md "wikilink")[长沙市人民路](../Page/长沙市.md "wikilink")139号，是一所[三级甲等医院](../Page/三级甲等医院.md "wikilink")，创建于1958年。目前是长沙规模最大的医院，目前开放病床2200余张，2005年门急诊量达到120万人次，出院病人5.4
万人（次），各类手术 3.6
万台（次）。拥有三个[国家重点学科](../Page/国家重点学科.md "wikilink")：[外科学](../Page/外科学.md "wikilink")（[胸心外科](../Page/胸心外科.md "wikilink")）、[精神病与精神卫生学](../Page/精神病与精神卫生学.md "wikilink")、[内科学](../Page/内科学.md "wikilink")（[内分泌与代谢病](../Page/内分泌与代谢病.md "wikilink")）。

[Category:长沙三级甲等医院](../Category/长沙三级甲等医院.md "wikilink")
[Category:中南大学附属医院](../Category/中南大学附属医院.md "wikilink")