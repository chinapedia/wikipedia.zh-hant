**澄清醫院**是[中華民國](../Page/中華民國.md "wikilink")[臺中市的一間](../Page/臺中市.md "wikilink")[私立](../Page/私立.md "wikilink")[醫院集團](../Page/醫院.md "wikilink")，始創於1932年（[昭和七年](../Page/昭和.md "wikilink")）。澄清醫院院區主要分為平等院區和中港院區兩處，此外尚有不少同名結盟醫療機構。目前被衛生署（今[衛生福利部](../Page/中華民國衛生福利部.md "wikilink")）評定為區域[教學醫院](../Page/教學醫院.md "wikilink")，共有醫護人員一千多人，1,052個病床位。

## 沿革

  - 1932年（昭和七年），[東京大學醫學博士](../Page/東京大學.md "wikilink")[林澄清從](../Page/林澄清.md "wikilink")[日本返台在](../Page/日本.md "wikilink")[臺中市](../Page/台中市_\(州轄市\).md "wikilink")[錦町成立](../Page/錦町_\(台中市\).md "wikilink")「**澄清外科醫院**」（今
    平等院區）。
  - 1948年（昭和23年），[林敬義接任澄清外科醫院院長](../Page/林敬義.md "wikilink")，
  - 1967年（民國56年）9月，更名為「**澄清綜合醫院**」。
  - 1978年（民國67年），經行政院衛生署評鑑核准為三級教學醫院，是臺中市最早的教學醫院之一。
  - 1991年（民國80年）6月，升格準區域教學醫院。
  - 1993年（民國83年）2月19日，[林高德接任院長](../Page/林高德.md "wikilink")。
  - 1994年（民國83年）4月，升格區域教學醫院。

## 院區

| **分區名稱** | **地址**                                                                      | **備註**                                                 | 外部連結                                            |
| -------- | --------------------------------------------------------------------------- | ------------------------------------------------------ | ----------------------------------------------- |
| 平等院區     | 臺中市[中區平等街](../Page/中區_\(臺中市\).md "wikilink")139號                            | 近[臺中公園](../Page/臺中公園.md "wikilink")                    | [網頁](http://www.ccgh.com.tw/PT2/html/home.aspx) |
| 中港院區     | 臺中市[西屯區](../Page/西屯區.md "wikilink")[臺灣大道四段](../Page/臺灣大道.md "wikilink")966號 | 近[台中工業區對面](../Page/台中工業區.md "wikilink")                | [網頁](http://www.ccgh.com.tw/html/home.aspx)     |
| 敬德園區     | 臺中市[西屯區敬德街](../Page/西屯區.md "wikilink")8號                                    |                                                        | [網頁](http://jingde.com.tw/)                     |
| 復健醫院     | 臺中市[北屯區](../Page/北屯區.md "wikilink")[太原路三段](../Page/太原路.md "wikilink")1142號  | 原[中山醫學大學附設復健醫院](../Page/中山醫學大學附設醫院.md "wikilink")\[1\] |                                                 |

## 相關醫療體系

  - [澄清復健醫院](../Page/澄清復健醫院.md "wikilink")：政府委外，位於北屯區
  - [霧峰澄清醫院](../Page/霧峰澄清醫院.md "wikilink")：位於大里區
  - [本堂澄清醫院](../Page/本堂澄清醫院.md "wikilink")：位於霧峰區
  - [烏日澄清醫院](../Page/烏日澄清醫院.md "wikilink")：位於烏日區
  - [太平澄清醫院](../Page/太平澄清醫院.md "wikilink")：位於太平區
  - 理澄診所：農會委外，位於大里區
  - 台中工業區診所：政府委外，位於西屯區

## 交通

### [臺中市公車](../Page/臺中市公車.md "wikilink")

  - 中港院區

<!-- end list -->

  - 台灣大道幹線
    [澄清醫院站](../Page/澄清醫院站.md "wikilink")：[300](../Page/台中市公車300路.md "wikilink")、[301](../Page/台中市公車301路.md "wikilink")、[302](../Page/台中市公車302路.md "wikilink")、[303](../Page/台中市公車303路.md "wikilink")、[304](../Page/台中市公車304路.md "wikilink")、[305](../Page/台中市公車305路.md "wikilink")、[306](../Page/台中市公車306路.md "wikilink")、[307](../Page/台中市公車307路.md "wikilink")、[308](../Page/台中市公車308路.md "wikilink")
  - [台中客運](../Page/台中客運.md "wikilink")：[28](../Page/台中市公車28路.md "wikilink")、[49](../Page/台中市公車49路.md "wikilink")、[57](../Page/台中市公車57路.md "wikilink")、[60](../Page/台中市公車60路.md "wikilink")、[69](../Page/台中市公車69路.md "wikilink")、[88](../Page/台中市公車88路.md "wikilink")、藍9（福科福康路口）、[藍10](../Page/台中市公車146路.md "wikilink")、[藍11](../Page/台中市公車147路.md "wikilink")（福科福康路口）
  - [統聯客運](../Page/統聯客運.md "wikilink")：[75](../Page/台中市公車75路.md "wikilink")、[83](../Page/台中市公車83路.md "wikilink")、[86](../Page/台中市公車86路.md "wikilink")、[藍13](../Page/台中市公車87路.md "wikilink")（福科福康路口）
  - [中台灣客運](../Page/中台灣客運.md "wikilink")：[150](../Page/台中市公車150路.md "wikilink")
  - [豐榮客運](../Page/豐榮客運.md "wikilink")：[48](../Page/台中市公車48路.md "wikilink")
  - [和欣客運](../Page/和欣客運.md "wikilink")：[161](../Page/臺中市公車161路.md "wikilink")
  - [巨業交通](../Page/巨業交通.md "wikilink")：[67](../Page/台中市公車67路.md "wikilink")、[68](../Page/台中市公車68路.md "wikilink")、[168](../Page/台中市公車168路.md "wikilink")、[169](../Page/台中市公車169路.md "wikilink")
  - 聯營路線：[63](../Page/台中市公車63路.md "wikilink")（[豐原](../Page/豐原客運.md "wikilink")、[統聯](../Page/統聯客運.md "wikilink")）

<!-- end list -->

  - 平等院區

<!-- end list -->

  - 第二市場（三民路）、第二市場（臺灣大道）或光復國小（三民路）：[5](../Page/台中市公車5路.md "wikilink")、[6](../Page/台中市公車6路.md "wikilink")、[8](../Page/台中市公車8路.md "wikilink")、[9](../Page/台中市公車9路.md "wikilink")、[14](../Page/台中市公車14路.md "wikilink")、[14副](../Page/台中市公車14路.md "wikilink")、[15](../Page/台中市公車15路.md "wikilink")、[16](../Page/台中市公車16路.md "wikilink")、[29](../Page/台中市公車29路.md "wikilink")、[30](../Page/台中市公車30路.md "wikilink")、[37](../Page/台中市公車37路.md "wikilink")、[40](../Page/台中市公車40路.md "wikilink")、[41](../Page/台中市公車41路.md "wikilink")、[48](../Page/台中市公車48路.md "wikilink")、[70](../Page/台中市公車70路.md "wikilink")、[83](../Page/台中市公車83路.md "wikilink")、[88](../Page/台中市公車88路.md "wikilink")、[100](../Page/台中市公車100路.md "wikilink")、[100副](../Page/台中市公車100路.md "wikilink")、[107](../Page/台中市公車107路.md "wikilink")、[108](../Page/台中市公車108路.md "wikilink")、[125](../Page/台中市公車125路.md "wikilink")、[132](../Page/台中市公車132路.md "wikilink")、[142](../Page/台中市公車142路.md "wikilink")、[163](../Page/台中市公車163路.md "wikilink")、[168](../Page/台中市公車168路.md "wikilink")、[169](../Page/台中市公車169路.md "wikilink")
  - 臺中公園（公園路）：[105](../Page/台中市公車105路.md "wikilink")、[158](../Page/台中市公車158路.md "wikilink")
  - 公園自由路口：[29](../Page/台中市公車29路.md "wikilink")、[41](../Page/台中市公車41路.md "wikilink")、[105](../Page/台中市公車105路.md "wikilink")、[142](../Page/台中市公車142路.md "wikilink")、[158](../Page/台中市公車158路.md "wikilink")、[163](../Page/台中市公車163路.md "wikilink")、[168](../Page/台中市公車168路.md "wikilink")、[169](../Page/台中市公車169路.md "wikilink")
  - 成功平等街口：[168](../Page/台中市公車168路.md "wikilink")、[169](../Page/台中市公車169路.md "wikilink")
  - 電力公司：[30](../Page/台中市公車30路.md "wikilink")、[37](../Page/台中市公車37路.md "wikilink")、[40](../Page/台中市公車40路.md "wikilink")、[45](../Page/台中市公車45路.md "wikilink")、[48](../Page/台中市公車48路.md "wikilink")、[61](../Page/台中市公車61路.md "wikilink")、[81](../Page/台中市公車81路.md "wikilink")、[125](../Page/台中市公車125路.md "wikilink")、[200](../Page/台中市公車200路.md "wikilink")、[202](../Page/台中市公車202路.md "wikilink")、[203](../Page/台中市公車203路.md "wikilink")、[270](../Page/台中市公車270路.md "wikilink")、[271](../Page/台中市公車271路.md "wikilink")、[276](../Page/台中市公車276路.md "wikilink")、[277](../Page/台中市公車277路.md "wikilink")

## 參見

  - [臺灣醫院列表](../Page/臺灣醫院列表.md "wikilink")
  - [澄清基金會](../Page/澄清基金會.md "wikilink")

## 註釋

## 外部連結

  - [澄清醫院官方網站](http://www.ccgh.com.tw)

[Category:臺中市醫院](../Category/臺中市醫院.md "wikilink")
[C](../Category/霧峰區.md "wikilink")
[C](../Category/大雅區.md "wikilink")
[C](../Category/烏日區.md "wikilink")
[C](../Category/太平區_\(臺灣\).md "wikilink")
[B](../Category/台灣區域醫院.md "wikilink")
[Category:臺灣教學醫院](../Category/臺灣教學醫院.md "wikilink")
[醫](../Category/西屯區.md "wikilink") [Category:中區
(臺中市)](../Category/中區_\(臺中市\).md "wikilink")
[Category:北屯區](../Category/北屯區.md "wikilink")

1.  原為中山醫學大學附設復健醫院，為[臺中市政府辦理之復健醫院](../Page/臺中市政府.md "wikilink")，委託[中山醫學大學經營](../Page/中山醫學大學.md "wikilink")。2011年1月1日起移交由澄清醫院經營。