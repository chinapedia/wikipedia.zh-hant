**元德**（1329年八月二十九日至1332年四月二十八日）是[日本的](../Page/日本.md "wikilink")[年號之一](../Page/年號.md "wikilink")。這個時代的[天皇是](../Page/天皇.md "wikilink")[後醍醐天皇](../Page/後醍醐天皇.md "wikilink")、[鎌倉幕府](../Page/鎌倉幕府.md "wikilink")[征夷大將軍為](../Page/征夷大將軍.md "wikilink")[守邦親王](../Page/守邦親王.md "wikilink")、[執權為](../Page/執權.md "wikilink")[北條守時](../Page/北條守時.md "wikilink")。

## 改元

  - 嘉曆四年八月二十九日（1329年[9月22日](../Page/9月22日.md "wikilink")）改元元德
  - 元德三年八月九日（1331年[9月1日](../Page/9月1日.md "wikilink")）[南朝改元元弘](../Page/南北朝时代_\(日本\).md "wikilink")，北朝继续使用元德年号
  - 北朝元德四年/南朝元弘二年四月二十八日（1332年[5月23日](../Page/5月23日.md "wikilink")）北朝改元正慶

## 出處

  - 《[易經](../Page/易經.md "wikilink")》

## 出生

## 逝世

## 大事記

## 紀年、西曆、干支對照表

| 元德                                 | 元年                                   | 二年                                   | 三年                                   | 四年                                   |
| ---------------------------------- | ------------------------------------ | ------------------------------------ | ------------------------------------ | ------------------------------------ |
| 元弘年號                               |                                      |                                      | 元年                                   | 二年                                   |
| [儒略曆日期](../Page/儒略曆.md "wikilink") | [1329年](../Page/1329年.md "wikilink") | [1330年](../Page/1330年.md "wikilink") | [1331年](../Page/1331年.md "wikilink") | [1332年](../Page/1332年.md "wikilink") |
| [干支](../Page/干支.md "wikilink")     | [己巳](../Page/己巳.md "wikilink")       | [庚午](../Page/庚午.md "wikilink")       | [辛未](../Page/辛未.md "wikilink")       | [壬申](../Page/壬申.md "wikilink")       |

## 參考

  - [日本年號索引](../Page/日本年號索引.md "wikilink")
  - 同期存在的其他政權之紀年
      - [元弘](../Page/元弘.md "wikilink")（1331年八月九日至1334年一月二十九日）：[後醍醐天皇之年號](../Page/後醍醐天皇.md "wikilink")
      - [天曆](../Page/天历_\(元朝\).md "wikilink")（1328年九月—1330年五月）：[元](../Page/元朝.md "wikilink")—[元文宗圖帖睦爾](../Page/元文宗.md "wikilink")、[元明宗和世梀之年號](../Page/元明宗.md "wikilink")
      - [至順](../Page/至順.md "wikilink")（1330年五月—1333年十月）：元—元文宗圖帖睦爾、元寧宗懿璘質班、元順帝妥歡貼睦爾之年號
      - [開泰](../Page/開泰_\(陳明宗\).md "wikilink")（1324年—1329年）：[陳朝](../Page/陳朝_\(越南\).md "wikilink")—[陳明宗陳奣之年號](../Page/陳明宗.md "wikilink")
      - [開祐](../Page/開祐.md "wikilink")（1329年—1341年）：[陳朝](../Page/陳朝_\(越南\).md "wikilink")—[陳憲宗陳旺之年號](../Page/陳憲宗.md "wikilink")

## 參考文獻

  - 松橋達良，《元号はやわかり—東亜歷代建元考》，砂書房，1994年7月，ISBN 4915818276
  - 李崇智，《中国历代年号考》，中华书局，2001年1月ISBN 7101025129

[Category:14世纪日本年号](../Category/14世纪日本年号.md "wikilink")
[Category:1320年代日本](../Category/1320年代日本.md "wikilink")
[Category:1330年代日本](../Category/1330年代日本.md "wikilink")