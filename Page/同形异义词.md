__NOTOC__

**同形異義詞**（false
friend）即在[語言學中](../Page/語言學.md "wikilink")，假如甲語言的某詞與乙語言的某詞書寫形式相同或非常相似，但涵義不一样，足以導致誤解，這對詞屬於**同形异义词**。對應的西方名稱源自[法文](../Page/法文.md "wikilink")**faux-ami**，意為“假朋友”。跨語言的同形异义词可分兩類：

1.  本為[同源詞](../Page/同源詞.md "wikilink")，但其中一個在自己的語言裡改變了原意，或兩個均在各自的語言中改變了意思。
2.  詞源不同，只是剛巧拼法相似或相同。

## 例子

### 拼音文字

例如英文的preservative（防腐劑）及其散布於多種[歐洲語言的同形异义词](../Page/歐洲.md "wikilink")。這個字源自法文的préservatif，從préservatif還產生了[德文的Präservativ](../Page/德文.md "wikilink")、[羅馬尼亞文的prezervativ](../Page/羅馬尼亞文.md "wikilink")、[意大利文](../Page/意大利文.md "wikilink")、[西班牙文](../Page/西班牙文.md "wikilink")、[葡萄牙文的preservativo](../Page/葡萄牙文.md "wikilink")、[波蘭文的prezerwatywa](../Page/波蘭文.md "wikilink")。不過，除了英文的preservative還保留「防腐劑」的意思，préservatif及其他各字已轉作「[保險套](../Page/保險套.md "wikilink")」解（法文已改用conservateur來指防腐劑）。

從前[香港的新聞界](../Page/香港.md "wikilink")，將[皇家馬德里](../Page/皇家馬德里.md "wikilink")（Real
Madrid）譯作「真馬德里」，\[1\]就是因為把西班牙文的real等同於英文的real，不知道西班牙文有兩個real，一個來自[原始印歐語的詞根](../Page/原始印歐語.md "wikilink")\*reg-（「統治」），解「皇家」，和英語regal是同源詞；一個來自[拉丁文的res](../Page/拉丁文.md "wikilink")（「事物」），解「真」，和英語real是同源詞。兩詞只是拼法相同，語源、意義皆不相同。

### 日文汉字

[日語從漢語大量借詞](../Page/日語.md "wikilink")，這些詞語在兩種語言中各自發展，有些保留了古漢語用法，例如「湯」解作「熱水」；部分則成為了同形异义词，意義各有偏向，例如「大丈夫」（[平假名](../Page/平假名.md "wikilink")：），日文中這個詞的意思已由「大丈夫般堅強、壯健」引申為「安全、可靠、不打緊、沒關係」。同時，日本人也假借漢字的音、意來表日本本土的詞（參看[萬葉假名](../Page/萬葉假名.md "wikilink")），這種用法日語稱為「」，在這種情況下更不能望文生義。

<table>
<caption>日文漢字與漢語的同形異議詞列舉</caption>
<thead>
<tr class="header">
<th><p>日文漢字</p></th>
<th><p>平假名</p></th>
<th><p>意思</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/情婦.md" title="wikilink">情婦</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>壞心眼</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/心算.md" title="wikilink">心算</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>安靜地<a href="../Page/休息.md" title="wikilink">休息</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>如<a href="../Page/石.md" title="wikilink">石般堅硬的</a><a href="../Page/頭部.md" title="wikilink">頭部</a>、頑固不化的人</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>委託</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/駕駛.md" title="wikilink">駕駛</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>來歷</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>客氣</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/房東.md" title="wikilink">房東</a></p></td>
</tr>
</tbody>
</table>

### 朝鲜汉字

[朝鲜半岛的](../Page/朝鲜半岛.md "wikilink")[朝鮮語依然使用漢字](../Page/朝鮮語.md "wikilink")，部份跟漢語是同形异义词：

<table>
<caption>朝鮮漢字與漢語的同形異議詞列舉</caption>
<thead>
<tr class="header">
<th><p><a href="../Page/諺文.md" title="wikilink">諺文</a></p></th>
<th><p>朝鲜汉字</p></th>
<th><p>意思</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/導演.md" title="wikilink">導演</a>、<a href="../Page/主教練.md" title="wikilink">主教練</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/馬鈴薯.md" title="wikilink">馬鈴薯</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/交易.md" title="wikilink">交易</a>、<a href="../Page/交涉.md" title="wikilink">交涉</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/研究.md" title="wikilink">研究</a>、<a href="../Page/審查.md" title="wikilink">審查</a>、探討</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/團結.md" title="wikilink">團結</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/財務會計.md" title="wikilink">財務會計</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/學習.md" title="wikilink">學習</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/工序.md" title="wikilink">工序</a>、<a href="../Page/工藝.md" title="wikilink">工藝</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>停職反省</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/蒸汽火車.md" title="wikilink">蒸汽火車</a></p></td>
</tr>
</tbody>
</table>

### 越南汉字

[越南語雖然已經不太使用漢字](../Page/越南語.md "wikilink")，但如果寫成漢字，就會出現同形异义词：

<table>
<thead>
<tr class="header">
<th><p><a href="../Page/國語字.md" title="wikilink">國語字</a></p></th>
<th><p>越南汉字</p></th>
<th><p>意思</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/地圖.md" title="wikilink">地圖</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/示威.md" title="wikilink">示威</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/中南半島.md" title="wikilink">中南半島</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/經理.md" title="wikilink">經理</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/教授.md" title="wikilink">教授</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/富有.md" title="wikilink">富有</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/豐滿.md" title="wikilink">豐滿</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/手段.md" title="wikilink">手段</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/書本.md" title="wikilink">書本</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/事故.md" title="wikilink">事故</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/新聞.md" title="wikilink">新聞</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/天氣.md" title="wikilink">天氣</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/辦公室.md" title="wikilink">辦公室</a></p></td>
</tr>
</tbody>
</table>

### 台語漢字

雖然[台語或](../Page/台語.md "wikilink")[閩南語的正確文書系統在流傳區域並不甚普及](../Page/閩南語.md "wikilink")，但如果以正確的[台語漢字書寫表意](../Page/台閩字.md "wikilink")，就會與標準漢語有同形異義詞的產生：

<table>
<caption>台語漢字與漢語的同形異議詞列舉</caption>
<thead>
<tr class="header">
<th><p>台語漢字</p></th>
<th><p><a href="../Page/白話字.md" title="wikilink">白話字</a></p></th>
<th><p>意思</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>（雀鳥）</p></td>
<td></td>
<td><p>麻將（遊戲）</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>奔跑</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>逃亡</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>前天</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>書本</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>車站</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>親戚</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>戒指</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>月亮</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>兒子</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>爭吵</p></td>
</tr>
</tbody>
</table>

### 漢語諸方言

典型同形異義詞如“[婆婆](../Page/祖父母.md "wikilink")”，有的地方指外祖母，有的地方指丈夫的母親。“太太”，有的地方指妻子，有的地方指曾祖母。

閩南語（廣義）中，[泉漳語和](../Page/泉漳語.md "wikilink")[潮州話之間也有許多同形異義詞](../Page/潮州話.md "wikilink")，而且若互對話的時候亦產生很多誤會。

<table>
<thead>
<tr class="header">
<th><p><a href="../Page/漢字.md" title="wikilink">漢字</a></p></th>
<th><p>泉漳語意思</p></th>
<th><p>潮州話意思</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/年輕.md" title="wikilink">年輕</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/年輕女子.md" title="wikilink">年輕女子</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/父親.md" title="wikilink">父親</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/母親.md" title="wikilink">母親</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/教師.md" title="wikilink">教師</a></p></td>
</tr>
</tbody>
</table>

### 各地現代標準漢語

儘管各地[現代標準漢語可互通](../Page/現代標準漢語.md "wikilink")，但之間仍有同形異義詞。

<table>
<thead>
<tr class="header">
<th><p><a href="../Page/漢字.md" title="wikilink">漢字</a></p></th>
<th><p><a href="../Page/中華民國國語.md" title="wikilink">國語意思</a></p></th>
<th><p><a href="../Page/新馬華語.md" title="wikilink">華語意思</a></p></th>
<th><p><a href="../Page/普通話.md" title="wikilink">普通話意思</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p><a href="../Page/花生.md" title="wikilink">花生</a></p></td>
<td><p><a href="../Page/馬鈴薯.md" title="wikilink">馬鈴薯</a></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/房屋.md" title="wikilink">房屋</a></p></td>
<td></td>
<td><p><a href="../Page/房間.md" title="wikilink">房間</a><small>（1980年代以後）</small></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>嚴厲懲處</p></td>
<td><p>對人對事採取措施</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>計算器</p></td>
<td><p>電腦</p></td>
<td></td>
</tr>
</tbody>
</table>

## 其他問題

有時候，某些同型異義詞於某種語言中的意思帶有貶義，甚至是髒話（或者不宜於普通社交場合中使用的詞彙）。這在普通談話中會構成很大的問題。

例子包括：

<table>
<thead>
<tr class="header">
<th><p>詞</p></th>
<th><p>意思（語言）</p></th>
<th><p>意思（語言）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Fag</p></td>
<td><p><a href="../Page/香煙.md" title="wikilink">香煙</a>（英式英文）</p></td>
<td><p>對<a href="../Page/同性戀.md" title="wikilink">同性戀者的藐稱</a>。含冒犯之意（美式英文）</p></td>
</tr>
<tr class="even">
<td><p>Neger</p></td>
<td><p>黑人（荷蘭文）</p></td>
<td><p>(Nigger) 對黑人的藐稱。含冒犯之意（美式英文）</p></td>
</tr>
<tr class="odd">
<td><p>Spaz</p></td>
<td><p>笨拙的人（美式英文）</p></td>
<td><p>對傷殘人士的藐稱（英式英文）</p></td>
</tr>
<tr class="even">
<td><p>Root</p></td>
<td><p>為某人打氣（美式英文）</p></td>
<td><p>與某人做愛（澳洲式英文）</p></td>
</tr>
<tr class="odd">
<td><p>Coger</p></td>
<td><p>拿取（西班牙式<a href="../Page/西班牙文.md" title="wikilink">西班牙文</a>）</p></td>
<td><p>與某人做愛（墨西哥式和阿根廷式西班牙文）</p></td>
</tr>
<tr class="even">
<td><p>Banci</p></td>
<td><p><a href="../Page/人口普查.md" title="wikilink">人口普查</a>（<a href="../Page/馬來語.md" title="wikilink">馬來文</a>）</p></td>
<td><p>女性化的男同性戀者、變性人。含冒犯之意（<a href="../Page/印尼語.md" title="wikilink">印尼文</a>）</p></td>
</tr>
<tr class="odd">
<td><p>Gampang</p></td>
<td><p>容易（印尼文）</p></td>
<td><p>私生子。含冒犯之意（馬來文）</p></td>
</tr>
<tr class="even">
<td><p>Koneksi</p></td>
<td><p>連結、關係（印尼文）</p></td>
<td><p><a href="../Page/陽具.md" title="wikilink">陽具</a>（馬來文）</p></td>
</tr>
<tr class="odd">
<td><p>Seronok</p></td>
<td><p>好，可欣賞的（馬來文）</p></td>
<td><p>色情（印尼文）</p></td>
</tr>
<tr class="even">
<td><p>Budak</p></td>
<td><p>小孩（馬來文）</p></td>
<td><p>奴隸（印尼文）</p></td>
</tr>
<tr class="odd">
<td><p>Pantat</p></td>
<td><p><a href="../Page/臀.md" title="wikilink">臀部</a>（印尼文）</p></td>
<td><p>女性陰道（馬來文）</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>免費（韓語、日語）</p></td>
<td><p>無用，沒本事（粵語）</p></td>
</tr>
</tbody>
</table>

## 偽同源詞

跟同形异义词相關的是「偽同源詞」（），指兩個外表相似，**涵意也相同**，但詞源不同的詞。例如各語言中對父母的稱呼，單以母親來說，可拿中文（[漢藏語系](../Page/漢藏語系.md "wikilink")）的「媽媽」比較意大利語（[印歐語系](../Page/印歐語系.md "wikilink")）的mamma、[格魯吉亞語](../Page/格魯吉亞語.md "wikilink")（[高加索語系](../Page/高加索語系.md "wikilink")）的მამა(mama)。互不相干的語言均以\[m\]、\[b\]、\[a\]（還有\[p\]、\[h\]）這幾個音組成的字來作父母的稱呼，只是因為這幾個是最容易發的音，呀呀學語的嬰兒無意發出，而為人父母的總以為嬰孩最初說的「話」是用來稱呼父母親，因此以這幾個音構成的字，容易成為父母的指稱。

英語daddy和漢語爹爹音近意同，但是各有獨立起源，就是偽同源詞。

## 註釋

## 參考資料

## 参见

  - [臺、華語同形異義詞](../Page/臺、華語同形異義詞.md "wikilink")
  - [粵語用辭](../Page/粵語用辭.md "wikilink")
  - [漢語地區用詞差異列表](../Page/漢語地區用詞差異列表.md "wikilink")

## 外部連結

  - [An on-line hypertext bibliography on false
    friends](http://www.lipczuk.buncic.de/)

  - [French-English False Cognates - Faux
    amis](http://french.about.com/library/fauxamis/blfauxam_a.htm)

<!-- end list -->

  - [False Friends of the
    Slavist](../Page/wikibooks:en:False_Friends_of_the_Slavist.md "wikilink")

[Category:语言学](../Category/语言学.md "wikilink")

1.  參看「從前有隊真馬德里」，刊於《[信報](../Page/信報.md "wikilink")》，2006年8月25日，P29版。