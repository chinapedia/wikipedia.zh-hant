## 摘要

<table style="width:10%;">
<colgroup>
<col style="width: 1%" />
<col style="width: 8%" />
</colgroup>
<thead>
<tr class="header">
<th><p>描述摘要</p></th>
<th><p>Cover of Porgy And Bess.</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>來源</p></td>
<td><p><a href="http://www.vervemusicgroup.com/artist/releases/default.aspx?pid=9849&amp;aid=2685">http://www.vervemusicgroup.com/artist/releases/default.aspx?pid=9849&amp;aid=2685</a></p></td>
</tr>
<tr class="even">
<td><p>日期</p></td>
<td><p>1957</p></td>
</tr>
<tr class="odd">
<td><p>作者</p></td>
<td><p>Verve Records</p></td>
</tr>
<tr class="even">
<td><p>許可</p></td>
<td><p>版權許可資料(Use of the cover art in the article complies with Wikipedia non-free content policy and fair use under United States copyright law as described above.) {{#switch: 檔案其他版本（可留空） Use of the cover art in the article complies with Wikipedia non-free content policy and fair use under United States copyright law as described above.</p></td>
</tr>
</tbody>
</table>