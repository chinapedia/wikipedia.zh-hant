[德阳话](德阳话.md "wikilink"){{-w}} [绵阳话](绵阳话.md "wikilink"){{-w}}
[资阳话](资阳话.md "wikilink"){{-w}}
[南充话](南充话.md "wikilink"){{-w}}
[广元话](广元话.md "wikilink"){{-w}}
[遂宁话](遂宁话.md "wikilink"){{-w}}
[达州话](达州话.md "wikilink"){{-w}}
[巴中话](巴中话.md "wikilink"){{-w}}
[广安话](广安话.md "wikilink"){{-w}}
[万州话](万州话.md "wikilink"){{-w}}
[涪陵话](涪陵话.md "wikilink"){{-w}}
[攀枝花话](攀枝花话.md "wikilink"){{-w}}[恩施话](恩施话.md "wikilink"){{-w}}[宜昌话](宜昌话.md "wikilink")
|group2 = [灌赤片](灌赤片.md "wikilink") |list2 =
[崇州话](崇州话.md "wikilink"){{-w}}
[眉山话](眉山话.md "wikilink"){{-w}}
[乐山话](乐山话.md "wikilink"){{-w}}
[夹江话](夹江话.md "wikilink"){{-w}}
[峨嵋话](峨嵋话.md "wikilink"){{-w}}
[宜宾话](宜宾话.md "wikilink"){{-w}}
[泸州话](泸州话.md "wikilink"){{-w}}
[江津话](江津话.md "wikilink"){{-w}}
[綦江话](綦江话.md "wikilink"){{-w}}
[西充话](西充话.md "wikilink"){{-w}} [西昌话](西昌话.md "wikilink")

` |group2 = `[`雅棉小片`](雅棉小片.md "wikilink")
` |list2 = `[`雅安话`](雅安话.md "wikilink")
` |group3 = `[`仁富小片`](仁富小片.md "wikilink")
` |list3 = `[`仁寿话`](仁寿话.md "wikilink")`{{-w}} `[`荣县话`](荣县话.md "wikilink")`{{-w}} `[`自贡话`](自贡话.md "wikilink")`{{-w}} `[`内江话`](内江话.md "wikilink")
` }}`

}} |group2 = **历史** |list2 = [巴语](巴语.md "wikilink") {{·}}
[蜀语](古蜀语.md "wikilink") {{·}} [巴蜀图语](巴蜀图语.md "wikilink") {{·}}
[巴蜀语](巴蜀语.md "wikilink") |group3 = **书写** |list3 =
[四川话文学](四川话文学.md "wikilink") {{·}}
[四川方言字](四川方言字.md "wikilink") {{·}}
[四川话拼音](四川话拼音.md "wikilink") {{·}}
[四川话拉丁化新文字](四川话拉丁化新文字.md "wikilink")
|group4 = **相关语言** |list4 = [土广东话](土广东话.md "wikilink") {{·}}
[老湖广话](湘语.md "wikilink") {{·}} [四川普通话](四川普通话.md "wikilink")
|below=<small>参见：[西南官话](西南官话.md "wikilink") - [巴蜀](巴蜀.md "wikilink") -
[巴蜀人](巴蜀人.md "wikilink")</small> }} <noinclude> </noinclude>

[Category:四川话](../Category/四川话.md "wikilink")
[\*](../Category/四川话.md "wikilink")
[Category:漢語方言模板](../Category/漢語方言模板.md "wikilink")
[Category:漢語導航模板](../Category/漢語導航模板.md "wikilink")