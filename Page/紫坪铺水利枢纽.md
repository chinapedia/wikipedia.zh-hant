**紫坪铺水利枢纽**位于[中国](../Page/中国.md "wikilink")[四川省](../Page/四川省.md "wikilink")[都江堰市](../Page/都江堰市.md "wikilink")[麻溪乡](../Page/麻溪乡.md "wikilink")，[岷江](../Page/岷江.md "wikilink")[上游](../Page/上游.md "wikilink")[干流处](../Page/干流.md "wikilink")。工程以[灌溉](../Page/灌溉.md "wikilink")、城市[供水为主](../Page/供水.md "wikilink")，兼顾[防洪](../Page/防洪.md "wikilink")、[发电](../Page/发电.md "wikilink")、[环保用水](../Page/环保.md "wikilink")、[旅游等综合效益的](../Page/旅游.md "wikilink")[水利工程](../Page/水利工程.md "wikilink")。

## 建设介绍

[Zipingpu_Dam.JPG](https://zh.wikipedia.org/wiki/File:Zipingpu_Dam.JPG "fig:Zipingpu_Dam.JPG")
紫坪铺水利枢纽为[西部大开发十大工程之一](../Page/西部大开发.md "wikilink")，被列入四川省一号工程。工程于1950年代开始筹建，因其坝基在[紫坪铺镇](../Page/紫坪铺镇.md "wikilink")（前称白沙）紫坪村而得名。于2001年3月29日动工，2002年11月截流，2004年12月1日蓄水，2005年5月第一台机组发电，2006年12月竣工，由[四川省紫坪铺开发有限责任公司负责建设和运营](../Page/四川省紫坪铺开发有限责任公司.md "wikilink")。

## 工程数据

  - 坝顶[高程](../Page/高程.md "wikilink")：884[米](../Page/米.md "wikilink")
  - 趾板基础高程：728米
  - 最大坝高：156米
  - 坝顶长度：663.77米
  - 坝顶宽：12米
  - 正常蓄水位：877米
  - 相应库容：9.98亿立方米
  - 汛期限制水位：850米
  - 总库容：11.12亿立方米
  - 调节库容：7.74亿立方米
  - 调洪库容：5.38亿立方米
  - 防洪库容：1.66亿立方米
  - 库区面积：18.16平方公里
  - 电站尾水长：26.5千米
  - 校核洪水位：883.1米
  - 1000年一遇洪峰流量:12700[立方米](../Page/立方米.md "wikilink")／秒
  - 装机容量：4台19万千瓦机组，总装机76万千瓦
  - 多年平均发电量：34.17亿[度](../Page/度.md "wikilink")
  - 坝址以上[流域面积](../Page/流域面积.md "wikilink")：22662[平方公里](../Page/平方公里.md "wikilink")
  - 坝址以上多年平均流量：469立方／秒
  - 坝址以上年径流量总量：148亿立方米
  - 动态投资:72亿元[人民币](../Page/人民币.md "wikilink")
  - 静态投资:62亿元人民币

## 参见

  - [中国最高水坝列表](../Page/中国最高水坝列表.md "wikilink")

## 参考资料

## 外部链接

  - [四川省紫坪铺水利枢纽工程简介](http://www.cws.net.cn/CWR_Journal/200109/21.html)

[Category:长江流域水利工程](../Category/长江流域水利工程.md "wikilink")
[Category:岷江水系](../Category/岷江水系.md "wikilink")
[Category:四川水利](../Category/四川水利.md "wikilink")
[Category:成都地理](../Category/成都地理.md "wikilink")
[Category:都江堰市](../Category/都江堰市.md "wikilink")