**鼠鯊屬**（[學名](../Page/學名.md "wikilink")*Lamna*）是[鼠鯊科下的一](../Page/鼠鯊科.md "wikilink")[屬](../Page/屬.md "wikilink")[鯊魚](../Page/鯊魚.md "wikilink")。

## 吸熱能力

鼠鯊屬可以維持高於周邊海水[溫度的體溫](../Page/溫度.md "wikilink")，比起其他的[軟骨魚更高](../Page/軟骨魚.md "wikilink")，最高的溫差可達15.6℃。\[1\]\[2\]
在[魚類中](../Page/魚類.md "wikilink")，可以控制體溫的只有較大及敏捷的[物種](../Page/物種.md "wikilink")，如[藍鰭金槍魚及](../Page/藍鰭金槍魚.md "wikilink")[劍魚等](../Page/劍魚.md "wikilink")。

## 物種

鼠鯊屬包含兩個[物種](../Page/物種.md "wikilink")：

  - [鼠鯊](../Page/鼠鯊.md "wikilink")（*L.
    nasus*）：分佈於[大西洋及南](../Page/大西洋.md "wikilink")[太平洋的海岸區域及遠洋](../Page/太平洋.md "wikilink")。
  - [太平洋鼠鯊](../Page/太平洋鼠鯊.md "wikilink")（*L. ditropis*）：分佈於北太平洋海岸。

## 參考

[\*](../Category/鼠鯊屬.md "wikilink")

1.
2.