**汪姓**，中文姓氏之一，在《[百家姓](../Page/百家姓.md "wikilink")》中排名第104。汪姓族人自得姓以來，經過2000多年的發展，如今已繁衍到了全國各地，而且是人口眾多。中國科學院遺傳研究所研究人員杜若甫、袁義達據中國國家統計局提供的1982年全國人口0.5Z，隨機抽樣資料，以及1970年在[中華民國](../Page/中華民國.md "wikilink")[台灣出版的](../Page/台灣.md "wikilink")《[台灣地區人口之姓氏分佈](../Page/台灣地區.md "wikilink")》一書進行統計顯示，汪姓人口約佔全國漢族人口0.38％，據此推算全國當有近500萬人姓汪，按人口多少排列為第57大姓。尤以安徽多汪姓，約佔全國漢族汪姓人口的44％；其次為湖北、四川、江蘇、湖南、浙江等省。上述6省汪姓約佔全國漢族汪姓人口的72％。對於洋人而言，“汪”与“[王](../Page/王姓.md "wikilink")”的发音相近。

## 起源

  - [魯成公](../Page/魯成公.md "wikilink")，以汪為姓。
  - [秦憲公攻](../Page/秦憲公.md "wikilink")[亳戎](../Page/亳戎.md "wikilink")，克「小汪」（今[陝西](../Page/陝西.md "wikilink")[澄城](../Page/澄城.md "wikilink")），以[宗室領兵鎮守](../Page/宗室.md "wikilink")，後人以汪為姓。
  - [夏朝](../Page/夏朝.md "wikilink")[防風氏的後裔定居於](../Page/防風氏.md "wikilink")「汪芒」（今[浙江](../Page/浙江.md "wikilink")[德清](../Page/德清.md "wikilink")）。後為楚所滅，改為汪氏。

## 著名人物

  - 文學

<!-- end list -->

  - [汪倫](../Page/汪倫.md "wikilink")：唐代文人，李白好友
  - [汪元量](../Page/汪元量.md "wikilink")：宋代詩人
  - [汪伯彥](../Page/汪伯彥.md "wikilink")：南宋宰相
  - [汪藻](../Page/汪藻.md "wikilink")：宋代文學家
  - [汪道昆](../Page/汪道昆.md "wikilink")：明代文學家
  - [汪琬](../Page/汪琬.md "wikilink")：明末清初散文家
  - [汪士慎](../Page/汪士慎.md "wikilink")：好茶、善詩、工篆刻與隸書，於揚州賣畫為生。清朝「揚州八怪」之一
  - [汪中](../Page/汪中.md "wikilink")：清代學者。曾於鎮江文宗閣檢校《四庫全書》
  - [汪仁壽](../Page/汪仁壽.md "wikilink")：清代篆刻名家
  - [汪端光](../Page/汪端光.md "wikilink")：清代文人
  - [汪國真](../Page/汪國真.md "wikilink")：中國大陸當代著名詩人
  - [汪明欣](../Page/汪明欣.md "wikilink")：香港失明作家
  - [汪曾祺](../Page/汪曾祺.md "wikilink")：當代著名作家、散文家和劇作家

<!-- end list -->

  - 政軍

<!-- end list -->

  - [汪鳴鑾](../Page/汪鳴鑾.md "wikilink")：清末總理各國事務大臣，著名的藏書家，碑帖名家
  - [汪由敦](../Page/汪由敦.md "wikilink")：清代軍機大臣
  - [汪精衛](../Page/汪精衛.md "wikilink")：清末民初革命家，同盟會及國民黨重要領導人，抗日戰爭爆發後任南京國民政府主席
  - [汪道涵](../Page/汪道涵.md "wikilink")：海峽兩岸關係協會創會會長
  - [汪道淵](../Page/汪道淵.md "wikilink")：曾任中華民國國防部部長
  - [汪東興](../Page/汪東興.md "wikilink")：中國共產黨領導人，曾任中共中央政治局常委、中央委員會副主席
  - [汪洋](../Page/汪洋_\(政治人物\).md "wikilink")：中國共產黨、中華人民共和國政治人物
  - [汪希苓](../Page/汪希苓.md "wikilink")：中華民國中將退役，曾任駐外武官及情報工作
  - [汪希凌](../Page/汪希凌.md "wikilink")：前台灣國防部委員

<!-- end list -->

  - 科技

<!-- end list -->

  - [汪大淵](../Page/汪大淵.md "wikilink")：元代民間航海家，著有《島夷志略》
  - [汪昂](../Page/汪昂.md "wikilink")：明末清初著名醫學家

<!-- end list -->

  - 商界

<!-- end list -->

  - [汪小菲](../Page/汪小菲.md "wikilink")：俏江南集團創始人、執行董事张蘭獨子

<!-- end list -->

  - 演藝

<!-- end list -->

  - [汪笑儂](../Page/汪笑儂.md "wikilink")：中國近代京劇演員、劇作家，戲劇改良的代表人物之一
  - [汪涵](../Page/汪涵.md "wikilink")：湖南衛視著名主持人
  - [汪東城](../Page/汪東城.md "wikilink")：台灣藝人組合飛輪海成員之一
  - [汪建民](../Page/汪建民.md "wikilink")：台灣男藝人
  - [汪琳](../Page/汪琳.md "wikilink")：前香港演員
  - [汪明荃](../Page/汪明荃.md "wikilink")：香港知名電視及粵劇演員
  - [汪用和](../Page/汪用和.md "wikilink")：臺灣電視新聞女主播
  - [汪苏泷](../Page/汪苏泷.md "wikilink")：流行音乐原创小才子，中国大陆最受欢迎男歌手之一
  - [汪峰](../Page/汪峰.md "wikilink")：中國大陸搖滾歌手、音樂創作人、實力派男歌手，鮑家街43號樂隊發起人

<!-- end list -->

  - 體育

<!-- end list -->

  - [汪俊良](../Page/汪俊良.md "wikilink")：前台灣職業棒球選手，曾經效力於中華職棒統一獅隊，中華職棒第一支全壘打的締造者
  - [汪竣泰](../Page/汪竣泰.md "wikilink")：台灣前職業棒球選手，曾效力於中華職棒兄弟象隊
  - [汪明輝](../Page/汪明輝.md "wikilink")：台灣男子划船運動員

## 參見

  - [王姓](../Page/王姓.md "wikilink")

[\*](../Category/汪姓.md "wikilink") [W](../Category/漢字姓氏.md "wikilink")