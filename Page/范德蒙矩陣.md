在[線性代數中](../Page/線性代數.md "wikilink")，**范德蒙矩陣**的命名來自[Alexandre-Théophile
Vandermonde的名字](../Page/Alexandre-Théophile_Vandermonde.md "wikilink")，范德蒙矩陣是一個各列呈現出[幾何級數關係的](../Page/幾何級數.md "wikilink")[矩陣](../Page/矩陣.md "wikilink")，例如：

\[V=\begin{bmatrix}
1 & \alpha_1 & \alpha_1^2 & \dots & \alpha_1^{n-1}\\
1 & \alpha_2 & \alpha_2^2 & \dots & \alpha_2^{n-1}\\
1 & \alpha_3 & \alpha_3^2 & \dots & \alpha_3^{n-1}\\
\vdots & \vdots & \vdots & \ddots &\vdots \\
1 & \alpha_m & \alpha_m^2 & \dots & \alpha_m^{n-1}\\
\end{bmatrix}\] 或以第 *i* 行第 *j* 列的關係寫作：

\[V_{i,j} = \alpha_i^{j-1}\]
（部分作者將上述矩陣寫成[轉置後的形式](../Page/轉置.md "wikilink")，也就是一整排的
1 不列在左邊，而是列在上面。）

*n*階范德蒙矩陣的[行列式可以表示為](../Page/行列式.md "wikilink")：

\[\det(V) = \prod_{1\le i<j\le n} (\alpha_j-\alpha_i)\]

當\(\alpha_i\)各不相同时，\(\det(V)\)不为零。

上述的行列式又稱作[判別式](../Page/判別式.md "wikilink")。

給行列式使用[萊布尼玆公式](../Page/行列式#严格的定义##向量组的行列式.md "wikilink")

\[\det(V) = \sum_{\sigma \in S_n} \sgn(\sigma) \, \alpha_1^{\sigma(1)-1} \cdots \alpha_n^{\sigma(n)-1},\]

可以把公式改寫為

\[\prod_{1\le i<j\le n} (\alpha_j-\alpha_i) = \sum_{\sigma \in S_n} \sgn(\sigma) \, \alpha_1^{\sigma(1)-1} \cdots \alpha_n^{\sigma(n)-1},\]

*S*<sub>*n*</sub> 指的是 {1, 2, ..., *n*}
的[排列集](../Page/排列.md "wikilink")，sgn(σ) 指的是排列 σ 的奇偶性。

若 *m*≤*n*，則矩陣 *V* 有最大的[秩](../Page/矩陣的秩.md "wikilink") rank (*m*)。

## 參閱

  - [拉格朗日多項式](../Page/拉格朗日多項式.md "wikilink")
  - [朗斯基行列式](../Page/朗斯基行列式.md "wikilink")

[F](../Category/矩陣.md "wikilink") [F](../Category/行列式.md "wikilink")
[F](../Category/數值線性代數.md "wikilink")