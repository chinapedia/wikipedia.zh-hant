**iPhoto**是一款由[蘋果公司为](../Page/蘋果公司.md "wikilink")[Mac OS
X操作系统和](../Page/Mac_OS_X.md "wikilink")[iLife软件套装编写](../Page/iLife.md "wikilink")，用于管理[数码照片的应用软件](../Page/数码照片.md "wikilink")。iPhoto只能运行在[Mac
OS X](../Page/Mac_OS_X.md "wikilink")，不能在早期的Classic版[Mac
OS或其它操作系统上运行](../Page/Mac_OS.md "wikilink")。然而在2012年初，蘋果公司也发布了能在iOS系統上運行iPhoto的版本。2015年，iPhoto正式由新軟體「照片」取代。

iPhoto提供以下的服务：

  - [柯达提供的专业照片冲印](../Page/柯达.md "wikilink")
  - 精装书制作：$29.95[美元能制作](../Page/美元.md "wikilink")10页双面的11"x8.5"内页。同时提供其它格式。
  - 与[.Mac整合](../Page/.Mac.md "wikilink")：在线发布照片

以上服务只在[美国](../Page/美国.md "wikilink")、[日本和一些欧洲国家提供](../Page/日本.md "wikilink")。

## 版本特色

iPhoto 4.0的特色：

  - 最高支持25,000张照片
  - [Rendezvous图片共享](../Page/Bonjour_\(协议\).md "wikilink")：不需要设置便能允许[Mac用户通过](../Page/Mac.md "wikilink")[本地网络观看其它电脑上的照片](../Page/本地网络.md "wikilink")
  - 用星星为图片评分
  - 智能相本：与[iTunes智能播放列表类似](../Page/iTunes.md "wikilink")，通过拍摄日期、评分等条件创建相册。
  - 支持大部分数码相机而不需要驱动程序
  - 编辑功能（在前文列出）
  - 幻灯片模式：在幻灯片模式时能同时播放[iTunes中的音乐](../Page/iTunes.md "wikilink")；四种转场模式包括苹果著名的*cube*效果
  - 为.Mac成员提供的屏幕保护程序制作：可以将制作成的屏幕保护程序发布到iDisk上，让其它Mac OS X使用
  - 把照片库备份到CD或DVD中

iPhoto 5.0於2005年一月釋出，增加了以下的功能：

  - 支援[RAW格式](../Page/RAW.md "wikilink")
  - 直接支援由數位相機產生的電影。
  - 更多編輯能力，包含使用dashboard來編輯顏色、曝光、亮度以及影像強度。
  - 支援個別的幻燈片和儲存相簿設計
  - three book sizes, and double-sided pages in books
  - 月曆的標籤，使用日期來尋找相片
  - "關鍵字"標籤可以用來群組相片，而不用使用相簿；這些關鍵字可以使用[Spotlight來搜尋](../Page/Spotlight.md "wikilink")。
  - 在美國4x6 [柯達沖洗價格降低到](../Page/柯達.md "wikilink")19¢

## 參見

  - [ACDSee](../Page/ACDSee.md "wikilink")

  - [iLife](../Page/iLife.md "wikilink")

  - [數位攝影](../Page/數位攝影.md "wikilink")

  -
  - [F-Spot](../Page/F-Spot.md "wikilink")

## 外部链接

  - [iPhoto產品頁at Apple.com](http://www.apple.com/iphoto/)
  - [iPhoto支援at Apple.com](http://www.apple.com/support/iphoto)
  - [I-photo](https://web.archive.org/web/20141217165450/http://i-photo.eu/)

[Category:蘋果公司軟體](../Category/蘋果公司軟體.md "wikilink")
[Category:MacOS軟體](../Category/MacOS軟體.md "wikilink")
[Category:IOS软件](../Category/IOS软件.md "wikilink")
[iPhoto](../Category/圖像檢視器.md "wikilink")
[iPhoto](../Category/照片軟體.md "wikilink")
[Category:2002年面世的產品](../Category/2002年面世的產品.md "wikilink")