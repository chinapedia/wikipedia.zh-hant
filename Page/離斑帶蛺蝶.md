**離斑帶蛺蝶**是[蛺蝶科蝴蝶的一種](../Page/蛺蝶科.md "wikilink")，出沒於[印度西南部和部份](../Page/印度.md "wikilink")[東南西地區](../Page/東南西.md "wikilink")。

## 描述

[Athyma_ranga_reconstr.jpg](https://zh.wikipedia.org/wiki/File:Athyma_ranga_reconstr.jpg "fig:Athyma_ranga_reconstr.jpg")
上部：雄蝶為柔和的黑色，雌蝶則顯現著暗沈的褐色，在某種光線下，會滿佈著藍色的光澤。

## 分佈

[印度的](../Page/印度.md "wikilink")[錫金邦和](../Page/錫金邦.md "wikilink")[阿薩姆邦山區](../Page/阿薩姆邦.md "wikilink")、[不丹](../Page/不丹.md "wikilink")、[緬甸的](../Page/緬甸.md "wikilink")[德林達依](../Page/德林達依.md "wikilink")。亦發現於印度南部、[西高止山脈和](../Page/西高止山脈.md "wikilink")[尼爾吉里丘陵](../Page/尼爾吉里丘陵.md "wikilink")。

## 生命史

### 幼蟲

### 蛹

### 食用植物

[異株木犀欖](../Page/異株木犀欖.md "wikilink")（*Olea
dioica*）和[馬拉巴李欖](../Page/馬拉巴李欖.md "wikilink")（*Linociera
malabarica*）。

[Category:帶蛺蝶屬](../Category/帶蛺蝶屬.md "wikilink")