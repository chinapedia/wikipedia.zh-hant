**Microsoft Exchange
Server**是[微软公司的一套](../Page/微软公司.md "wikilink")[电子邮件服务组件](../Page/电子邮件.md "wikilink")。除传统的电子邮件的存取、储存、转发作用外，在新版本的产品中亦加入了一系列辅助功能，如语音邮件、邮件过滤筛选和OWA（基于[Web的电子邮件存取](../Page/Web.md "wikilink")）。Exchange
Server支持多种电子邮件[网络协议](../Page/网络协议.md "wikilink")，如[SMTP](../Page/SMTP.md "wikilink")、[NNTP](../Page/NNTP.md "wikilink")、[POP3和](../Page/POP3.md "wikilink")[IMAP4](../Page/IMAP4.md "wikilink")。Exchange
Server能够与微软公司的[活动目录完美结合](../Page/活动目录.md "wikilink")。

Exchange Server是个消息与协作系统，Exchange
server可以被用来构架应用于企业、学校的邮件系统甚至于免费邮件系统。也可以用于开发[工作流](../Page/工作流.md "wikilink")、[知识管理系统](../Page/知识管理系统.md "wikilink")、Web系统或者是其他消息系统。

Microsoft Exchange Server ==版本==

  - Exchange 1.0
  - Exchange Server 4.0
  - Exchange Server 5.0
  - Exchange Server 5.5
  - Exchange 2000 Server
  - Exchange Server 2003 6.5.6944
  - Microsoft Exchange Server 2003 SP1 6.5.7226
  - Microsoft Exchange Server 2003 SP2 6.5.7638
  - Microsoft Exchange Server 2003 SP2 （2008 年 3 月更新） 6.5.7653.33
  - Microsoft Exchange Server 2003 SP2 （2008 年 8 月更新） 6.5.7654.4
  - Exchange Server 2007 8.0.685.24 或 8.0.685.25
  - Exchange Server 2010 14.0.639.21
  - Exchange Server 2013
  - Exchange Server 2016
  - Exchange Server 2018

## 競爭對手

  - [IBM Lotus Domino](../Page/IBM_Lotus_Domino.md "wikilink")

## 外部链接

  - [Microsoft Exchange
    Server](https://products.office.com/zh-cn/exchange/microsoft-exchange-server-2016)

[Exchange Server](../Category/微软软件.md "wikilink")
[Category:郵件傳送代理](../Category/郵件傳送代理.md "wikilink")