**查尔斯·珀西·斯诺**，[CBE](../Page/CBE.md "wikilink")（，），英国[科学家](../Page/科学家.md "wikilink")，[小说家](../Page/小说家.md "wikilink")。

## 生平

斯诺在[英国](../Page/英国.md "wikilink")[英格兰](../Page/英格兰.md "wikilink")[东米德兰兹](../Page/东米德兰兹.md "wikilink")[莱斯特郡](../Page/莱斯特郡.md "wikilink")[莱斯特出生](../Page/莱斯特.md "wikilink")，在[莱斯特大学和](../Page/莱斯特大学.md "wikilink")[剑桥大学受到教育](../Page/剑桥大学.md "wikilink")，并于1930年在后者成为[剑桥大学基督学院的一个研究员](../Page/剑桥大学基督学院.md "wikilink")。在1957年他被授予[爵士稱號](../Page/爵士.md "wikilink")，1960年成为[终身貴族](../Page/终身貴族.md "wikilink")——莱斯特的男爵斯诺。他曾在[哈罗德·威尔逊的](../Page/哈罗德·威尔逊.md "wikilink")[英国工党政府担任助手](../Page/英国工党.md "wikilink")。他的朋友包括[數學家](../Page/數學家.md "wikilink")[高德菲·哈罗德·哈代](../Page/高德菲·哈罗德·哈代.md "wikilink")（曾为哈代的《[一個數學家的辯白](../Page/一個數學家的辯白.md "wikilink")》（*A
Mathematician's
Apology*）写序），[物理学家](../Page/物理学家.md "wikilink")[帕特里克·布莱克特与](../Page/帕特里克·布莱克特.md "wikilink")[X射线晶体学家](../Page/X射线晶体学.md "wikilink")[约翰·德斯蒙德·贝尔纳](../Page/约翰·德斯蒙德·贝尔纳.md "wikilink").\[1\]

斯诺与小说家[帕米拉·汉斯福德·约翰逊结婚](../Page/帕米拉·汉斯福德·约翰逊.md "wikilink")。

## 事業

斯诺的首部小说是[侦探小说](../Page/侦探小说.md "wikilink")《[船帆下的死亡](../Page/船帆下的死亡.md "wikilink")》（*[Death
under
Sail](../Page/:en:Death_under_Sail.md "wikilink")*，1932年出版）。他也为[安东尼·特罗洛普](../Page/安东尼·特罗洛普.md "wikilink")（[Anthony
Trollope](../Page/:en:Anthony_Trollope.md "wikilink")）写了一部传记。

不管怎样，他更为人知的是他是题为《[陌生人与亲兄弟](../Page/陌生人与亲兄弟.md "wikilink")》（*[Stangers
and
Brothers](../Page/:en:Stangers_and_Brothers.md "wikilink")*）这一系列描述现代学术与政治背景下的聪明人（intellectuals）的政治小说的作者。《[院长](../Page/院长.md "wikilink")》（*[The
Masters](../Page/:en:The_Masters.md "wikilink")*）是这一系列小说中最为著名的，它通过描写一个剑桥的学院在选举院长之前的一系列事情，讨论了其内部的政治问题。它表达了作者作为一个局内人的观点，也揭露出了学术之外的事物对被认为是客观的学者做出决定时的影响。《院长》与《[新人](../Page/新人.md "wikilink")》（*[The
New
Men](../Page/:en:The_New_Men.md "wikilink")*）一同获得了1954年的[詹姆斯·泰特·布莱克纪念奖](../Page/詹姆斯·泰特·布莱克纪念奖.md "wikilink")。\[2\]《[权力走廊](../Page/权力走廊.md "wikilink")》（*[The
Corridors of
Power](../Page/:en:The_Corridors_of_Power.md "wikilink")*）在今天的英语字典中添加了一个习语。

《[写实主义者](../Page/写实主义者.md "wikilink")》（*[The
Realists](../Page/:en:The_Realists.md "wikilink")*）是对这八位作家作品的一个检验：[司汤达](../Page/司汤达.md "wikilink")，[巴尔扎克](../Page/巴尔扎克.md "wikilink")，[查尔斯·狄更斯](../Page/查尔斯·狄更斯.md "wikilink")，[费奥多尔·陀思妥耶夫斯基](../Page/费奥多尔·陀思妥耶夫斯基.md "wikilink")，[列夫·托尔斯泰](../Page/列夫·托尔斯泰.md "wikilink")，[贝尼托·佩雷斯·加尔多斯](../Page/贝尼托·佩雷斯·加尔多斯.md "wikilink")（Benito
Pérez
Galdós），[亨利·詹姆斯和](../Page/亨利·詹姆斯.md "wikilink")[马塞尔·普鲁斯特](../Page/马塞尔·普鲁斯特.md "wikilink")，斯诺为写实主义小说做出了一个坚实的辩护。

## 值得注意的观点与见解

斯诺最值得人们注意的是他关于他“[兩種文化](../Page/兩種文化.md "wikilink")”这一概念的讲演与书籍。这一概念在他的《[两种文化与科学变革](../Page/两种文化与科学变革.md "wikilink")》（*[The
Two Cultures and the Scientific
Revolution](../Page/:en:The_Two_Cultures_and_the_Scientific_Revolution.md "wikilink")*，1959年出版）。在这本书中，斯诺注意到[科学与](../Page/科学.md "wikilink")[人文中联系的中断对解决](../Page/人文.md "wikilink")[世界上的问题是一个主要障碍](../Page/世界.md "wikilink")。

斯诺特别提到如今世界上[教育的质量正在逐步地降低](../Page/教育.md "wikilink")。比如说，很多[科学家从未读过](../Page/科学家.md "wikilink")[查尔斯·狄更斯的作品](../Page/查尔斯·狄更斯.md "wikilink")，同样，[艺术工作者对科学也同样的不熟悉](../Page/艺术工作者.md "wikilink")。他写道：

  -
    曾经有很多次，我与一些人在一起聚会，根据传统的标准来说，他们是受过高等教育的，并一直对科学家的无知而表现出兴致勃勃的难以置信的样子。有一两次我被激怒了，于是质问他们之中有多少个能够解释清楚[热力学第二定律](../Page/热力学第二定律.md "wikilink")，即[熵的定律](../Page/熵.md "wikilink")。令人沮丧的回答同样是否定的。然而我问的这一问题大概就是将“你读过[莎士比亚吗](../Page/莎士比亚.md "wikilink")？”这一问题转为科学语言描述的一样。

<!-- end list -->

  -
    现在我相信如果我当初问的是一个更简单的问题——例如，你认为[质量](../Page/质量.md "wikilink")，[加速度是什么意思](../Page/加速度.md "wikilink")？，即与‘你能阅读吗？’这一问题转换为科学语言后是等价的——他们这些受过高等教育的人当中不会有超过十分之一的人会认为我在表达同一意思。所以说，当现代[物理学的大厦不断增高时](../Page/物理学.md "wikilink")，如今西方世界中大部分最聪明的人对其的洞察也正如他们[新石器时代的祖先一样](../Page/新石器时代.md "wikilink")。

斯诺的演讲在发表之时引起了很多的骚动，一部分原因是他在陈述观点时不愿妥协的态度。他被文学评论家[F·R·利维斯](../Page/F·R·利维斯.md "wikilink")（[F.
R.
Leavis](../Page/:en:F._R._Leavis.md "wikilink")）强烈地抨击。这一激烈的争辩甚至使[夫兰达斯与史旺创作了一首主题是热力学第一与第二定律的喜剧歌曲](../Page/夫兰达斯与史旺.md "wikilink")，并起名为《第一与第二定律》（*First
and Second Law*）。

斯诺写到：

  -
    当你想起人类悠久而又黑暗的[历史之时](../Page/历史.md "wikilink")，你会发现可怕的[犯罪出于](../Page/犯罪.md "wikilink")[服从之名远远多于出于](../Page/服从.md "wikilink")[背叛之名的犯罪](../Page/背叛.md "wikilink")。

斯诺同时注意到了另一个分化，即富国与穷国之间的分化。

## 另见

  - [约翰·布罗克曼](../Page/约翰·布罗克曼.md "wikilink")（[John
    Brockman](../Page/:en:John_Brockman_\(literary_agent\).md "wikilink")）
  - [卡尔·萨根](../Page/卡尔·萨根.md "wikilink")
  - 《[第三种文化](../Page/第三种文化.md "wikilink")》（*[The Third
    Culture](../Page/:en:The_Third_Culture.md "wikilink")*）
  - [阿尔文·托夫勒](../Page/阿尔文·托夫勒.md "wikilink")
  - [艾德华·威尔森](../Page/艾德华·威尔森.md "wikilink")

## 作品

### 小说

#### 《[陌生人与亲兄弟](../Page/陌生人与亲兄弟.md "wikilink")》系列

  - 《[希望时光](../Page/希望时光.md "wikilink")》（*[Time of
    Hope](../Page/:en:Time_of_Hope.md "wikilink")*，1949年出版）
  - 《[乔治·帕桑特](../Page/乔治·帕桑特.md "wikilink")》（*[George
    Passant](../Page/:en:George_Passant.md "wikilink")*，初次出版时作《陌生人与亲兄弟》，1940年出版）
  - 《富者之仁》（*The Conscience of the Rich*，1958年出版）
  - 《[亮与暗](../Page/亮与暗.md "wikilink")》（*[The Light and the
    Dark](../Page/:en:The_Light_and_the_Dark.md "wikilink")*，1947年出版）
  - 《院长》（*The Masters*，1951年出版）
  - 《新人》（*The New Men*，1954年出版）
  - 《回家》（*Homecomings*，1956年出版）
  - 《丑闻》（*The Affair*，1959年出版）
  - 《[权力走廊](../Page/权力走廊.md "wikilink")》（*[The Corridors of
    Power](../Page/:en:The_Corridors_of_Power.md "wikilink")*，1963年出版）
  - 《沉睡的理性》（*The Sleep of Reason*，1968年出版）
  - 《最后的事》（*Last Things*，1970年出版）

#### 其它小说

  - 《船帆下的死亡》（*Death Under Sail*，1932年出版）
  - 《搜》（*The Search*，1934年出版）
  - 《反叛者》（*The Malcontents*，1972年出版）
  - 《他们的智慧之中》（*In Their Wisdom*，1974年出版）
  - 《遮掩之外衣》（*A Coat of Varnish*，1979年出版）

### 非小说

  - 《科学与政府》（*Science and Government*，1961年出版）
  - 《两种文化与第二种见解》（*The two cultures and a second look*，1963年出版）
  - 《人的种类》（*Variety of men*，1967年出版）
  - 《围攻下的国家》（*The State of Siege*，1968年出版）
  - 《公共事务》（*Public Affairs*，1971年出版）
  - 《特罗洛普》（*Trollope*，1975年出版）
  - 《写实主义者》（*The Realists*，1978年出版）
  - 《物理学家》（*The Physicists*，1981年出版）

## 参考资料

<references />

## 外部链接

  - [关于斯诺-利维斯争论的网页（英）](https://web.archive.org/web/20070708072104/http://academics.vmi.edu/gen_ed/Two_Cultures.html)

[Category:CBE勳銜](../Category/CBE勳銜.md "wikilink")
[Category:獲授終身貴族者](../Category/獲授終身貴族者.md "wikilink")
[Category:英國工黨黨員](../Category/英國工黨黨員.md "wikilink")
[Category:英格蘭小說家](../Category/英格蘭小說家.md "wikilink")
[Category:英国科学家](../Category/英国科学家.md "wikilink")
[Category:劍橋大學基督學院校友](../Category/劍橋大學基督學院校友.md "wikilink")
[Category:萊斯特大學校友](../Category/萊斯特大學校友.md "wikilink")
[Category:萊斯特郡人](../Category/萊斯特郡人.md "wikilink")
[Category:詹姆斯·泰特·布莱克纪念奖获得者](../Category/詹姆斯·泰特·布莱克纪念奖获得者.md "wikilink")

1.  Snow P (2006) C. P. Snow *Christ's College Magazine* 231, 67–9
2.  [詹姆斯·泰特·布莱克纪念奖：获奖者（英）](http://www.englit.ed.ac.uk/jtbwins.htm)