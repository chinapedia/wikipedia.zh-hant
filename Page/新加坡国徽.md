**新加坡国徽**的中间是一个红色[盾牌](../Page/盾牌.md "wikilink")，盾牌上有与[国旗上相似的白色](../Page/新加坡国旗.md "wikilink")[新月及五颗](../Page/新月.md "wikilink")[五角星](../Page/五角星.md "wikilink")。盾牌左右分别为脚踩着金叶、同时站立着的[狮子与](../Page/狮子.md "wikilink")[老虎](../Page/老虎.md "wikilink")。盾牌下为写着国家格言“”（“前进吧，新加坡”）的[马来文的蓝底金字](../Page/马来文.md "wikilink")[绶带](../Page/绶带.md "wikilink")。

## 历史

1959年新加坡取得自治时，时任总理[李光耀组成一个委员会](../Page/李光耀.md "wikilink")，并委任[杜进才副总理带领设计自治邦邦旗与邦徽](../Page/杜进才.md "wikilink")。\[1\]以反映新加坡团结、多元种族的社会为要素，委员会花了两个月的时间设计了邦旗与邦徽。\[2\]时任文化部长[拉惹勒南向](../Page/拉惹勒南.md "wikilink")[立法议会呈上了自治邦象征的相关法案](../Page/新加坡立法议会.md "wikilink")，在1959年11月获得通过，并于1959年12月3日与邦歌首次揭幕。\[3\]\[4\]

杜进才在1989年接受一次采访时指出，当他获委任创造自治邦象征的时候，“有了邦歌，（他们）还需要设计邦旗和邦徽”。杜进才进一步指出：

## 意涵

盾牌上的红色代表了[平等与](../Page/平等.md "wikilink")[友谊](../Page/友谊.md "wikilink")，白色象征着[纯洁与](../Page/纯洁.md "wikilink")[美德](../Page/美德.md "wikilink")。新月表示新加坡是一个新建立的国家，而五颗五角星代表了国家的五大理想：[民主](../Page/民主.md "wikilink")、[和平](../Page/和平.md "wikilink")、[进步](../Page/进步.md "wikilink")、[公正](../Page/公正.md "wikilink")、平等。狮子象征[新加坡](../Page/新加坡.md "wikilink")，老虎象征[马来西亚](../Page/马来西亚.md "wikilink")，两者共同象征着新、马两国之间的历史渊源。

## 使用

1985年，[新加坡金融管理局推出第二系列硬币](../Page/新加坡金融管理局.md "wikilink")，这些硬币的反面展示着新加坡国徽，围绕着国徽的是“新加坡”国名的四种官方语言，以及铸币年份。\[5\]

根据金融管理局，自从新加坡于1967年推行“胡姬花”系列纸币以来，国徽都出现在所有钞票上。\[6\]

## 使用指引

根据《新加坡国徽、国旗及国歌法》，只有政府可以无限制使用国徽。所有人不得在未获得[通讯及新闻部的明确许可之下](../Page/通讯及新闻部.md "wikilink")，打印、制造、展示或售卖任何包含国徽的物品，或允许这样的事发生。人们也不得使用任何可以轻易误认为国徽的标志。有意在文学创作使用国徽者，也需事先获得通讯及新闻部准许。不过，政府官员以及政府部门可在建筑物外墙和印刷文件上使用国徽。\[7\]

## 轶事

2011年，网上流传一封电邮“新加坡1元硬币的故事”，声称新加坡1元硬币原本是“苦脸”，但到[1997年金融危机时](../Page/亞洲金融危機.md "wikilink")，有[风水师建议把它改为](../Page/风水师.md "wikilink")“笑脸”，以便转运，但新加坡当局否认此说法。

新加坡金融管理局受询时说，1元硬币背面最初确实是采用横幅往上拱起的“苦脸”设计，之后才改为“笑脸”设计，但这项改变与风水无关。发言人解释，国徽于1969年首次出现在新加坡钱币上。由于当时雕刻技术的限制，原本的国徽设计无法以三维的效果铸在钱币上，因此便采用较时髦的版本（横幅向上拱）。后来随着雕刻技术的进步，可以铸出原本的国徽设计（横幅向下凹），从1992年起，这个设计便开始用在硬币上。\[8\]

## 参考文献

## 外部链接

  - [新加坡纹章历史](http://www.hubert-herald.nl/Singapore.htm)

{{-}}

[徽](../Category/新加坡国家象征.md "wikilink") [S](../Category/国徽.md "wikilink")

1.  Zaubidah Mohamed，〈[National coat of arms (State
    crest)](http://eresources.nlb.gov.sg/infopedia/articles/SIP_54_2005-01-25.html)〉（国徽），，最后访问于2014年9月5日。

2.
3.  〈[State
    Symbols](http://www.nas.gov.sg/archivesonline/article/singapores-state-symbols)
    〉（国家象征），，最后访问于2014年9月5日。

4.
5.  〈[Circulation Currency \>
    Coins](http://www.mas.gov.sg/Currency/Circulation-Currency/Coins.aspx)〉（流通中货币
    \>
    硬币），[新加坡金融管理局](../Page/新加坡金融管理局.md "wikilink")，（最后修订于2013年10月3日），最后访问于2014年9月5日。

6.  〈[Circulation Currency \>
    Notes](http://www.mas.gov.sg/Currency/Circulation-Currency/Notes.aspx)〉（流通中货币
    \> 纸币），新加坡金融管理局，（最后修订于2014年8月6日），最后访问于2014年9月5日。

7.  新加坡国徽、国旗及国歌法（[2004年修订版](http://statutes.agc.gov.sg/aol/search/display/view.w3p;page=0;query=DocId%3A%223100849d-3555-4b06-ba59-bf4998ae34f2%22%20Status%3Ainforce%20Depth%3A0;rec=0)）

8.  [光明日报：新加坡1元硬幣“苦脸”变“笑脸”‧当局否认与风水有关](http://www.guangming.com.my/node/122421)，
    2011年6月14日