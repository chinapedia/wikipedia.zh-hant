[Qing_Dynasty_Min-Zhe_map_1911.svg](https://zh.wikipedia.org/wiki/File:Qing_Dynasty_Min-Zhe_map_1911.svg "fig:Qing_Dynasty_Min-Zhe_map_1911.svg")

**閩浙[總督](../Page/總督.md "wikilink")**（），正式官銜為**總督閩浙等處地方提督軍務、糧饟兼巡撫事**，是[清朝九位最高級的](../Page/清朝.md "wikilink")[總督](../Page/總督.md "wikilink")[疆臣之一](../Page/封疆大臣.md "wikilink")，總管[閩浙](../Page/閩浙.md "wikilink")（[浙江省](../Page/浙江省_\(清\).md "wikilink")、[福建省與](../Page/福建省_\(清\).md "wikilink")[福建臺灣省](../Page/福建臺灣省.md "wikilink")，臺灣於1895年被割讓給日本）的[軍政](../Page/軍政.md "wikilink")、民務。

## 官制沿革

  - 順治二年（1645年），設立浙閩總督，總督府駐[福州](../Page/福州.md "wikilink")，兼管[浙江](../Page/浙江.md "wikilink")。\[1\]
  - 順治五年（1648年），總督府遷往[衢州](../Page/衢州.md "wikilink")，仍兼管[福建](../Page/福建.md "wikilink")。
  - 順治十五年（1658年），兩省各設總督，福建總督府駐[漳州](../Page/漳州.md "wikilink")，浙江總督府駐[溫州](../Page/溫州.md "wikilink")。
  - 康熙十一年（1672年），將福建總督府迁往[福州](../Page/福州.md "wikilink")。<ref>同上，佟佳江《清史稿订误》称“查《疆臣年

表一》，康熙十二年没有省浙江总督一事。是年浙江总督为[刘兆麒和](../Page/刘兆麒.md "wikilink")[李之芳](../Page/李之芳.md "wikilink")。”</ref>

  - 康熙二十六年（1687年），[福建總督更名](../Page/福建總督.md "wikilink")**閩浙總督**。
  - 雍正五年（1727年），特授[李卫总督浙江](../Page/李卫.md "wikilink")，整饬军政吏治，并兼巡抚事；闽浙总督专辖福建。
  - 雍正十二年（1734年），撤销浙江总督，仍合为一。
  - 乾隆元年（1736年），诏依[李卫例](../Page/李卫.md "wikilink")，特授[嵇曾筠为浙江总督](../Page/嵇曾筠.md "wikilink")，闽浙总督[郝玉麟仍专辖福建](../Page/郝玉麟.md "wikilink")。
  - 乾隆三年（1738年），嵇曾筠入阁，郝玉麟仍总督闽浙如故。
  - 光緒十一年（1885年）起，取消[福建巡撫](../Page/福建巡撫.md "wikilink")，由閩浙總督兼任。
  - 宣統三年（1911年），[辛亥革命爆發](../Page/辛亥革命.md "wikilink")，末任總督因閩軍舉事，而兵敗，吞金殉國。

### 历任总督

<table>
<tbody>
<tr class="odd">
<td><p>width="6%" align ="center"|序次</p></td>
<td><p>width="14%" align ="center"|姓名</p></td>
<td><p>width="14%" align ="center"|籍贯/旗籍</p></td>
<td><p>width="24%" align ="center"|任职时间</p></td>
<td><p>width="24%" align ="center"|卸职时间</p></td>
<td><p>width="18%" align ="center"|卸職原因/备注</p></td>
</tr>
<tr class="even">
<td><p>align ="center" colspan="6" bgcolor="#EFECD1"| 閩浙總督（乾隆三年～光緒十一年）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>align ="center"|35</p></td>
<td><p><a href="../Page/孫爾準.md" title="wikilink">孫爾準</a></p></td>
<td><p><a href="../Page/江蘇省_(清).md" title="wikilink">江蘇省</a><a href="../Page/常鎮通海道.md" title="wikilink">常鎮通海道</a><a href="../Page/常州府.md" title="wikilink">常州府</a><a href="../Page/金匱縣.md" title="wikilink">金匱縣</a></p></td>
<td><p>道光五年九月乙酉<br />
1825年10月12日</p></td>
<td><p>道光十二年二月乙未<br />
1832年3月19日</p></td>
<td><p>去世</p></td>
</tr>
<tr class="even">
<td><p>align ="center"|36</p></td>
<td><p><a href="../Page/程祖洛.md" title="wikilink">程祖洛</a></p></td>
<td><p><a href="../Page/安徽省_(清).md" title="wikilink">安徽省</a><a href="../Page/安徽寧池太廣道.md" title="wikilink">安徽寧池太廣道</a><a href="../Page/徽州府.md" title="wikilink">徽州府</a><a href="../Page/歙縣.md" title="wikilink">歙縣</a></p></td>
<td><p>道光十二年二月乙未<br />
1832年3月19日</p></td>
<td><p>道光十六年七月癸未<br />
1836年8月13日</p></td>
<td><p>丁父憂</p></td>
</tr>
<tr class="odd">
<td><p>align ="center"|44</p></td>
<td><p><a href="../Page/劉韻珂.md" title="wikilink">劉韻珂</a></p></td>
<td><p><a href="../Page/山東省_(清).md" title="wikilink">山東省</a><a href="../Page/兗沂曹濟道.md" title="wikilink">兗沂曹濟道</a><a href="../Page/兗州府.md" title="wikilink">兗州府</a><a href="../Page/汶上縣.md" title="wikilink">汶上縣</a></p></td>
<td><p>道光二十三年五月戊辰<br />
1843年6月23日</p></td>
<td><p>道光三十年十一月丙午<br />
1850年12月21日</p></td>
<td><p>病卸</p></td>
</tr>
<tr class="even">
<td><p>align ="center"|46</p></td>
<td><p><a href="../Page/季芝昌.md" title="wikilink">季芝昌</a></p></td>
<td><p>江蘇省<a href="../Page/常鎮通海道.md" title="wikilink">常鎮通海道</a><a href="../Page/常州府.md" title="wikilink">常州府</a><a href="../Page/江陰縣.md" title="wikilink">江陰縣</a></p></td>
<td><p>咸豐元年五月乙巳<br />
1851年6月18日</p></td>
<td><p>咸豐二年十月壬辰<br />
1852年11月26日</p></td>
<td><p>免</p></td>
</tr>
<tr class="odd">
<td><p>align ="center"|*</p></td>
<td><p><a href="../Page/裕瑞.md" title="wikilink">裕瑞</a></p></td>
<td></td>
<td><p>咸豐元年九月乙丑<br />
1851年11月5日</p></td>
<td><p>咸豐二年七月己酉<br />
1852年8月15日</p></td>
<td><p>兼署</p></td>
</tr>
<tr class="even">
<td><p>align ="center"|*</p></td>
<td><p><a href="../Page/王懿德_(道光進士).md" title="wikilink">王懿德</a></p></td>
<td><p><a href="../Page/河南省_(清).md" title="wikilink">河南省</a><a href="../Page/開歸陳許鄭道.md" title="wikilink">開歸陳許鄭道</a><a href="../Page/開封府.md" title="wikilink">開封府</a><a href="../Page/祥符縣.md" title="wikilink">祥符縣</a></p></td>
<td><p>咸豐二年七月己酉<br />
1852年8月15日</p></td>
<td><p>咸豐二年十月壬辰<br />
1852年11月26日</p></td>
<td><p>兼署</p></td>
</tr>
<tr class="odd">
<td><p>align ="center"|47</p></td>
<td><p><a href="../Page/吳文鎔.md" title="wikilink">吳文鎔</a></p></td>
<td><p>江蘇省<a href="../Page/淮揚海道.md" title="wikilink">淮揚海道</a><a href="../Page/揚州府.md" title="wikilink">揚州府</a><a href="../Page/儀征縣.md" title="wikilink">儀征縣</a></p></td>
<td><p>咸豐二年十月壬辰<br />
1852年11月26日</p></td>
<td><p>咸豐三年八月己卯<br />
1853年9月9日</p></td>
<td><p>改授<a href="../Page/湖廣總督.md" title="wikilink">湖廣總督</a></p></td>
</tr>
<tr class="even">
<td><p>align ="center"|*</p></td>
<td><p>王懿德</p></td>
<td><p>－</p></td>
<td><p>咸豐三年二月己亥<br />
1853年4月2日</p></td>
<td><p>咸豐三年六月辛巳<br />
1853年7月13日</p></td>
<td><p>兼署</p></td>
</tr>
<tr class="odd">
<td><p>align ="center"|*</p></td>
<td><p><a href="../Page/有鳳.md" title="wikilink">有鳳</a></p></td>
<td></td>
<td><p>咸豐三年六月辛巳<br />
1853年7月13日</p></td>
<td><p>咸豐三年八月己卯<br />
1853年9月9日</p></td>
<td><p>兼署</p></td>
</tr>
<tr class="even">
<td><p>align ="center"|48</p></td>
<td><p><a href="../Page/慧成.md" title="wikilink">慧成</a></p></td>
<td></td>
<td><p>咸豐三年八月己卯<br />
1853年9月9日</p></td>
<td><p>咸豐四年正月戊午<br />
1854年2月15日</p></td>
<td><p>差</p></td>
</tr>
<tr class="odd">
<td><p>align ="center"|49</p></td>
<td><p>王懿德</p></td>
<td><p>－</p></td>
<td><p>咸豐四年正月戊午<br />
1854年2月15日</p></td>
<td><p>咸豐八年六月戊辰<br />
1858年8月3日</p></td>
<td><p>以病乞罷</p></td>
</tr>
<tr class="even">
<td><p>align ="center"|58</p></td>
<td><p><a href="../Page/何璟.md" title="wikilink">何璟</a></p></td>
<td><p><a href="../Page/廣東省_(清).md" title="wikilink">廣東省</a><a href="../Page/廣肇羅道.md" title="wikilink">廣肇羅道</a><a href="../Page/廣州府.md" title="wikilink">廣州府</a><a href="../Page/香山縣.md" title="wikilink">香山縣</a></p></td>
<td><p>光緒二年八月丁酉<br />
1876年9月26日</p></td>
<td><p>光緒十年七月己巳<br />
1884年9月16日</p></td>
<td><p>召</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

  - [张存仁](../Page/张存仁.md "wikilink")（浙闽1645年—1647年）
  - [陈锦](../Page/陈锦.md "wikilink")（浙闽1647年—1652年）
  - [刘清泰](../Page/刘清泰.md "wikilink")（浙闽1652年—1654年）
  - [屯泰](../Page/屯泰.md "wikilink")（浙闽1654年—1656年）
  - [李率泰](../Page/李率泰.md "wikilink")（浙闽1656年—1658年，福建1658年—1664年）
  - [赵国祚](../Page/赵国祚.md "wikilink")（浙江1658年—1661年）
  - [赵廷臣](../Page/赵廷臣.md "wikilink")（浙江1661年—1669年）
  - [朱昌祚](../Page/朱昌祚.md "wikilink")（福建1664年—1665年）
  - [张朝璘](../Page/张朝璘.md "wikilink")（福建1666年—1667年）
  - [祖泽溥](../Page/祖泽溥.md "wikilink")（福建1667年—1669年）
  - [刘兆麒](../Page/刘兆麒.md "wikilink")
    [顺天府](../Page/顺天府.md "wikilink")[宝坻县](../Page/宝坻县.md "wikilink")(今[天津市](../Page/天津市.md "wikilink")[宁河县](../Page/宁河县.md "wikilink"))隶汉军
    康熙九年（1669年—1673年）
  - [刘斗](../Page/刘斗.md "wikilink")（福建1670年—1672年）
  - [范承谟](../Page/范承谟.md "wikilink")
    [盛京](../Page/盛京.md "wikilink")(今[辽宁省](../Page/辽宁省.md "wikilink")[沈阳市](../Page/沈阳市.md "wikilink"))
    隶[汉军镶黄旗](../Page/汉军镶黄旗.md "wikilink") 康熙十一年（福建1672年—1674年）
  - [李之芳](../Page/李之芳.md "wikilink")
    [山东](../Page/山东.md "wikilink")[武定](../Page/武定.md "wikilink")
    康熙十二年（浙江1673年—1682年）
  - [郎廷佐](../Page/郎廷佐.md "wikilink")（福建1674年—1676年）
  - [郎廷相](../Page/郎廷相.md "wikilink")（福建1676年—1678年）
  - [姚启圣](../Page/姚启圣.md "wikilink")（福建1678年—1683年）
  - [施维翰](../Page/施维翰.md "wikilink")（浙江1682年—1683年，福建1683年—1684年）
  - [王国安](../Page/王国安.md "wikilink")（浙江1684年，福建1684年—1687年）
  - [王新命](../Page/王新命.md "wikilink")（1687年—1688年）改为**闽浙总督**
  - [王骘](../Page/王骘.md "wikilink") 山东[福山](../Page/福山.md "wikilink")
    康熙二十七年（1688年—1689年）
  - [兴永朝](../Page/兴永朝.md "wikilink")（1689年—1692年）
  - [朱宏祚](../Page/朱宏祚.md "wikilink")（1692年—1695年）
  - [郭世隆](../Page/郭世隆.md "wikilink")（1695年—1702年）
  - [金世荣](../Page/金世荣.md "wikilink")（1702年—1706年）
  - [梁鼐](../Page/梁鼐.md "wikilink")（1706年—1710年）
  - [范时崇](../Page/范时崇.md "wikilink")（1710年—1715年）
  - [觉罗满保](../Page/觉罗满保.md "wikilink")（1715年—1725年）
  - [高其倬](../Page/高其倬.md "wikilink") 辽宁[铁岭](../Page/铁岭.md "wikilink")
    雍正三年（1725年—1729年，1730年）
  - [史贻直](../Page/史贻直.md "wikilink")（1729年—1730年）
  - [刘世明](../Page/刘世明.md "wikilink")（1730年—1732年）
  - [李卫](../Page/李卫.md "wikilink")
    [江苏省](../Page/江苏省.md "wikilink")[砀山](../Page/砀山.md "wikilink")(今属[安徽省](../Page/安徽省.md "wikilink"))
    雍正五年（浙江总督，1727年—1732年）
  - [性桂](../Page/性桂.md "wikilink")（浙江总督，1729年）
  - [程元章](../Page/程元章.md "wikilink")
    [河南](../Page/河南.md "wikilink")[上蔡](../Page/上蔡.md "wikilink")
    雍正十年（浙江总督，1732年—1734年）
  - [郝玉麟](../Page/郝玉麟.md "wikilink") 汉军[镶白旗](../Page/镶白旗.md "wikilink")
    雍正十二年（1732年—1739年）
  - [嵇曾筠](../Page/嵇曾筠.md "wikilink") 江苏[无锡](../Page/无锡.md "wikilink")
    乾隆元年（浙江总督，1736年—1738年）
  - [德沛](../Page/德沛.md "wikilink") 乾隆四年（1739年—1744年）
  - [那苏图](../Page/那苏图.md "wikilink")（1742年—1744年）
  - [马尔泰](../Page/马尔泰.md "wikilink")（1744年—1746年）
  - [喀尔吉善](../Page/喀尔吉善.md "wikilink")
    [满洲正黄旗](../Page/满洲正黄旗.md "wikilink")
    乾隆十一年（1746年—1757年）
  - [杨应琚](../Page/杨应琚.md "wikilink")（1757年—1759年）
  - [杨廷璋](../Page/杨廷璋.md "wikilink") 汉军镶黄旗 乾隆二十四年（1759年—1764年）
  - [苏昌](../Page/苏昌.md "wikilink")
    [满洲](../Page/满洲.md "wikilink")[正蓝旗](../Page/正蓝旗.md "wikilink")
    乾隆二十九年（1764年—1768年）
  - [崔应阶](../Page/崔应阶.md "wikilink")
    [湖北](../Page/湖北.md "wikilink")[江夏](../Page/江夏.md "wikilink")(今[武汉](../Page/武汉.md "wikilink"))
    乾隆三十三年（1768年—1770年）
  - [钟音](../Page/钟音.md "wikilink")
    [顺天府](../Page/顺天府.md "wikilink")[大兴县](../Page/大兴县.md "wikilink")(今[北京市](../Page/北京市.md "wikilink"))
    隶满州[镶蓝旗](../Page/镶蓝旗.md "wikilink") 乾隆三十五年（1770年—1778年）
  - [富明安](../Page/富明安.md "wikilink")（1771年）
  - [杨景素](../Page/杨景素.md "wikilink")（1778年—1779年）
  - [三宝](../Page/三寶_\(乾隆繙譯\).md "wikilink")，[满洲正红旗](../Page/满洲正红旗.md "wikilink")，（1779年—1780年）
  - [富明安](../Page/富明安.md "wikilink")
    [满洲镶红旗](../Page/满洲镶红旗.md "wikilink")
    乾隆三十八年（1780年—1781年）
  - [陈辉祖](../Page/陈辉祖.md "wikilink")（1781年—1782年）
  - [富勒浑](../Page/富勒浑.md "wikilink")
    [蒙古镶白旗](../Page/蒙古镶白旗.md "wikilink")
    乾隆四十七年（1782年—1785年）
  - [雅德](../Page/雅德.md "wikilink")（1785年—1786年）
  - [富纲](../Page/富纲.md "wikilink")（1786年）
  - [常青](../Page/常青.md "wikilink") 满洲正蓝旗 乾隆五十一年（1786年—1787年）
  - [李侍尧](../Page/李侍尧.md "wikilink") 汉军镶黄旗 乾隆五十二年（1787年—1788年）
  - [福康安](../Page/福康安.md "wikilink")（1788年—1789年）
  - [魁伦](../Page/魁伦.md "wikilink")（1796年—1799年）
  - [伍拉纳](../Page/伍拉纳.md "wikilink")（1789年—1795年）
  - [福康安](../Page/福康安.md "wikilink")（1795年—1796年）
  - [魁伦](../Page/魁伦.md "wikilink")（1796年—1799年）
  - [书麟](../Page/书麟.md "wikilink")（1799年）
  - [觉罗长麟](../Page/觉罗长麟.md "wikilink") 满洲正蓝旗 乾隆六十年（1799年—1800年）
  - [玉德](../Page/玉德.md "wikilink")（1800年—1806年）
  - [阿林保](../Page/阿林保.md "wikilink")（1806年—1809年）
  - [方维甸](../Page/方维甸.md "wikilink") 安徽[桐城](../Page/桐城.md "wikilink")
    嘉庆十四年（1809年—1810年）
  - [汪志伊](../Page/汪志伊.md "wikilink") 安徽桐城 嘉庆十六年（1810年—1817年）
  - [董教增](../Page/董教增.md "wikilink")（1817年—1820年）
  - [庆保](../Page/庆保.md "wikilink")（1820年—1822年）
  - [赵慎畛](../Page/赵慎畛.md "wikilink")（1822年—1825年）
  - [孙尔準](../Page/孙尔準.md "wikilink")（1825年—1832年）
  - [程祖洛](../Page/程祖洛.md "wikilink")
    [安徽](../Page/安徽.md "wikilink")[歙县](../Page/歙县.md "wikilink")
    道光十二年（1832年—1836年）
  - [钟祥](../Page/钟祥.md "wikilink")（1836年—1839年）
  - [周天爵](../Page/周天爵.md "wikilink")（1839年）
  - [桂良](../Page/桂良.md "wikilink")（1839年）
  - [邓廷桢](../Page/邓廷桢.md "wikilink")（1839年—1840年）
  - [颜伯焘](../Page/颜伯焘.md "wikilink")（1840年—1841年）
  - [杨国桢](../Page/杨国桢_\(清朝\).md "wikilink")，四川省[崇庆州](../Page/崇庆州.md "wikilink")（1841年—1842年）
  - [怡良](../Page/怡良.md "wikilink")，满洲正红旗（1842年—1843年）
  - [刘韻珂](../Page/刘韻珂.md "wikilink")（1843年—1850年）
  - [裕泰](../Page/裕泰.md "wikilink")（1850年—1851年）
  - [季芝昌](../Page/季芝昌.md "wikilink")（1851年—1852年）
  - [吴文镕](../Page/吴文镕.md "wikilink")（1852年—1853年）
  - [慧成](../Page/慧成.md "wikilink")（1853年）
  - [王懿德](../Page/王懿德.md "wikilink")
    河南[祥符](../Page/祥符.md "wikilink")(今[开封](../Page/开封.md "wikilink"))
    咸丰二年（1854年—1859年）
  - [庆端](../Page/庆端.md "wikilink") 咸丰八年（1859年—1862年）
  - [耆龄](../Page/耆龄.md "wikilink") 满洲正黄旗 同治元年（1862年—1863年）
  - [左宗棠](../Page/左宗棠.md "wikilink")
    [湖南](../Page/湖南.md "wikilink")[湘阴](../Page/湘阴.md "wikilink")
    同治二年（1863年—1866年）
  - [吴棠](../Page/吴棠.md "wikilink")
    安徽[盱眙](../Page/盱眙.md "wikilink")(今安徽[明光](../Page/明光.md "wikilink"))
    同治五年（1866年—1867年）
  - [马新贻](../Page/马新贻.md "wikilink") 山东[菏泽](../Page/菏泽.md "wikilink")
    同治六年（1867年—1868年）
  - [英桂](../Page/英桂.md "wikilink")（1868年—1871年）
  - [张之万](../Page/张之万.md "wikilink")（1871年）
  - [李鹤年](../Page/李鹤年.md "wikilink")
    [奉天义州](../Page/奉天.md "wikilink")(今辽宁[义县](../Page/义县.md "wikilink"))
    同治十年（1871年—1876年）
  - [文煜](../Page/文煜.md "wikilink") 满洲正蓝旗 光绪二年（1876年）
  - [何璟](../Page/何璟.md "wikilink")
    [广东香山](../Page/广东.md "wikilink")(今[中山](../Page/中山市.md "wikilink"))
    光绪二年（1876年—1884年）
  - [杨昌濬](../Page/杨昌濬.md "wikilink") 湖南湘乡 光绪十年（1884年—1888年）
  - [卞宝第](../Page/卞宝第.md "wikilink") 江苏[仪征](../Page/仪征.md "wikilink")
    光绪十四年（1888年—1892年）
  - [谭钟麟](../Page/谭钟麟.md "wikilink")
    [湖南](../Page/湖南.md "wikilink")[茶陵](../Page/茶陵.md "wikilink")
    光绪十八年（1892年—1894年）
  - [边宝泉](../Page/边宝泉.md "wikilink")
    奉天[辽阳](../Page/辽阳.md "wikilink")(今属辽宁) 隶汉军镶红旗
    光绪二十年（1894年—1898年）
  - [许应骙](../Page/许应骙.md "wikilink")
    广东[番禺](../Page/番禺.md "wikilink")(今[广州](../Page/广州.md "wikilink"))
    光绪二十四年（1898年—1903年）
  - [锡良](../Page/锡良.md "wikilink") 蒙古镶蓝旗 光绪二十九年（1903年）
  - [李兴锐](../Page/李兴锐.md "wikilink") 湖南[浏阳](../Page/浏阳.md "wikilink")
    光绪二十九年（1903年—1904年）
  - [魏光焘](../Page/魏光焘.md "wikilink") 湖南[隆回](../Page/隆回.md "wikilink")
    光绪三十年（1904年—1905年）
  - [升允](../Page/升允.md "wikilink") 蒙古镶蓝旗 光绪三十一年（1905年）
  - [嵩蕃](../Page/嵩蕃.md "wikilink") 满洲镶蓝旗 光绪三十一年（1905年）
  - [端方](../Page/端方.md "wikilink") 满洲正白旗 光绪三十一年（1905年—1906年）
  - [周馥](../Page/周馥.md "wikilink")
    安徽[建德](../Page/建德.md "wikilink")(今[东至](../Page/东至.md "wikilink"))
    光绪三十二年 （1906年）
  - [丁振铎](../Page/丁振铎.md "wikilink") 河南[罗山](../Page/罗山.md "wikilink")
    光绪三十二年（1906年—1907年）
  - [松寿](../Page/松壽_\(閩浙總督\).md "wikilink") 满洲正白旗 光绪三十三年（1907年—1911年）

## 參考文献

### 引用

### 来源

  - 书籍

<!-- end list -->

  - 《[清史稿](../Page/清史稿.md "wikilink")》，卷二百七十四，列传六十一，杨雍建 姚缔虞 朱弘祚（子纲） 王骘
    宋荦 陈诜

{{-}}

[Category:清朝總督](../Category/清朝總督.md "wikilink")
[清朝閩浙總督](../Category/清朝閩浙總督.md "wikilink")
[清朝浙閩總督](../Category/清朝浙閩總督.md "wikilink")
[清朝福建總督](../Category/清朝福建總督.md "wikilink")
[清朝浙江總督](../Category/清朝浙江總督.md "wikilink")
[Category:清朝浙江省](../Category/清朝浙江省.md "wikilink")
[Category:清朝福建省](../Category/清朝福建省.md "wikilink")

1.  佟佳江《清史稿订误》称“顺治二年所设为浙闽总督，不是福建总督，更不曾于顺治五年改名浙闽总督。”见
    佟佳江，清史稿订误，长春：吉林大学出版社，1991年，第222页