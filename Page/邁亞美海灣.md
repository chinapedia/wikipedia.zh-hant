**邁亞美海灣**（）由[信和集團發展興建](../Page/信和集團.md "wikilink")，屋苑大部份單位景觀開揚，然而樓齡卻較高，傳媒曾報導屋苑有滲水問題，目前尚未進行大維修。

## 簡介

[邁亞美海灣位於屯門湖翠路](../Page/邁亞美海灣.md "wikilink")268號，鄰近[輕鐵](../Page/香港輕鐵.md "wikilink")[屯門碼頭站](../Page/屯門碼頭站.md "wikilink")，東西兩面分別為[慧豐園及](../Page/慧豐園.md "wikilink")[海翠花園](../Page/海翠花園.md "wikilink")，北面為[悅湖山莊及](../Page/悅湖山莊.md "wikilink")3所中小學，該屋苑的校網編號是70\[1\]。屋苑臨海而建，大部分單位均享海景，景觀包括[黃金海岸](../Page/香港黃金海岸.md "wikilink")、[屯門](../Page/屯門.md "wikilink")[避風塘](../Page/避風塘.md "wikilink")，亦可遠眺[大嶼山](../Page/大嶼山.md "wikilink")[東涌及](../Page/東涌.md "wikilink")[香港國際機場](../Page/香港國際機場.md "wikilink")。屋苑分為6座，間隔為兩房及三房單位，[建築面積從](../Page/建築面積.md "wikilink")500[平方呎到](../Page/平方呎.md "wikilink")1100[平方呎](../Page/平方呎.md "wikilink")，合共提供1272個單位。

## 屋苑滲水問題

2014年，[有線電視的節目](../Page/有線電視.md "wikilink")[樓盤傳真曾報導邁亞美海灣的屋苑公家走廊天花批盪粉化](../Page/樓盤傳真.md "wikilink")，嚴重剝落，令鋼根外露，當中漏水問題更是日積月累，天花甚至出現「鐘乳石」。\[2\]

## 交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - [輕鐵](../Page/香港輕鐵.md "wikilink")[屯門碼頭站](../Page/屯門碼頭站.md "wikilink")

<!-- end list -->

  - [屯門碼頭](../Page/屯門碼頭.md "wikilink")
    渡輪

<!-- end list -->

  - 屯門至[東涌](../Page/東涌新發展碼頭.md "wikilink")/[沙螺灣](../Page/沙螺灣.md "wikilink")/[大澳線](../Page/大澳.md "wikilink")\[3\]
  - 屯門至[澳門](../Page/澳門.md "wikilink")[氹仔線](../Page/氹仔.md "wikilink")
  - 屯門至澳門[外港線](../Page/外港.md "wikilink")\[4\]
  - 屯門至珠海線\[5\]

<!-- end list -->

  - [屯門碼頭巴士總站](../Page/屯門碼頭巴士總站.md "wikilink")

<!-- end list -->

  - [湖翠路](../Page/湖翠路.md "wikilink")

<!-- end list -->

  - [湖景路](../Page/湖景路.md "wikilink")

<!-- end list -->

  - [湖秀街](../Page/湖秀街.md "wikilink")

</div>

</div>

## 周邊

  - [屯門碼頭站](../Page/屯門碼頭站.md "wikilink")
  - [屯門碼頭](../Page/屯門碼頭.md "wikilink")

## 參考資料

<div class="references-small">

<references />

</div>

[category:屯門區私人屋苑](../Page/category:屯門區私人屋苑.md "wikilink")

[Category:信和集團物業](../Category/信和集團物業.md "wikilink")
[Category:屯門區](../Category/屯門區.md "wikilink")
[Category:屯門碼頭區](../Category/屯門碼頭區.md "wikilink")
[Category:蝴蝶灣](../Category/蝴蝶灣.md "wikilink")

1.  [香港教育局](http://www.edb.gov.hk/index.aspx?langno=2&nodeID=1505) 。
2.  [邁亞美海灣天花漏水造出鐘乳石](http://cablenews.i-cable.com/ci/videopage/program/12180759/%E6%A8%93%E7%9B%A4%E5%82%B3%E7%9C%9F/%E9%82%81%E4%BA%9E%E7%BE%8E%E6%B5%B7%E7%81%A3%E5%A4%A9%E8%8A%B1%E6%BC%8F%E6%B0%B4%E9%80%A0%E5%87%BA%E9%90%98%E4%B9%B3%E7%9F%B3)
    [有線電視](../Page/有線電視.md "wikilink")
3.  [屯門至東涌/沙螺灣/大澳線](http://www.fortuneferry.com.hk/timetable.php)
4.  [屯門 \<=\>
    澳門](https://www.turbojet.com.hk/tc/routing-sailing-schedule/tuen-mun-macau/sailing-schedule-fares.aspx)
5.  [屯門 =\>
    珠海](https://www.turbojet.com.hk/tc/routing-sailing-schedule/tuen-mun-zhuhai/sailing-schedule-fares.aspx)