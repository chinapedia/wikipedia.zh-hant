**白沙屯車站**位於[臺灣](../Page/臺灣.md "wikilink")[苗栗縣](../Page/苗栗縣.md "wikilink")[通霄鎮](../Page/通霄鎮.md "wikilink")[白沙屯](../Page/白沙屯_\(苗栗縣\).md "wikilink")，為[臺灣鐵路管理局](../Page/臺灣鐵路管理局.md "wikilink")[海岸線的](../Page/海岸線_\(臺鐵\).md "wikilink")[鐵路車站](../Page/鐵路車站.md "wikilink")。

## 車站構造

  - [島式月台兩座](../Page/島式月台.md "wikilink")。(只有使用第1月台，A側南下、B側北上)
  - 跨線[天橋](../Page/天橋.md "wikilink")。
  - [公共廁所](../Page/公共廁所.md "wikilink")。
  - 大山=白沙屯，雙軌區間南端。

## 利用狀況

  - 目前為[三等站](../Page/臺灣鐵路管理局車站等級#三等站.md "wikilink")，僅停靠[區間車](../Page/臺鐵區間車.md "wikilink")、[區間快車及少數](../Page/台鐵區間快車.md "wikilink")[莒光號有停靠南下](../Page/莒光號列車.md "wikilink")(逆行)507、511次，北上(順行)522次。
  - 站內設有「鐵路之旅 - 小站巡禮紀念章」之戳章，可供旅客蓋戳留念。
  - 本站開放使用[悠遊卡](../Page/悠遊卡.md "wikilink")、[一卡通及](../Page/一卡通_\(台灣\).md "wikilink")[icash
    2.0付費](../Page/icash.md "wikilink")。

| 年度   | 日均進站旅次（人） |
| ---- | --------- |
| 1998 | 147       |
| 1999 | 145       |
| 2000 | 138       |
| 2001 | 141       |
| 2002 | 133       |
| 2003 | 120       |
| 2004 | 130       |
| 2005 | 128       |
| 2006 | 122       |
| 2007 | 125       |
| 2008 | 130       |
| 2009 | 138       |
| 2010 | 155       |
| 2011 | 181       |
| 2012 | 199       |
| 2013 | 211       |
| 2014 | 213       |
| 2015 | 223       |
| 2016 | 244       |
| 2017 | 288       |

歷年旅客人次紀錄

## 車站週邊

  -
  - [台鹽](../Page/臺鹽實業公司.md "wikilink")[通霄精鹽廠](../Page/通霄精鹽廠.md "wikilink")：台鹽公司經營的觀光化製鹽廠，是全台灣唯一還在運作中的[製鹽廠](../Page/製鹽廠.md "wikilink")。

  - [苗栗縣立啟新國民中學](../Page/苗栗縣立啟新國民中學.md "wikilink")[](http://web.csjh.mlc.edu.tw)

  - [白沙屯拱天宮媽祖廟](../Page/白沙屯拱天宮.md "wikilink")

  - 白沙屯隧道遺址

  - 石蓮園餐廳

## 公車資訊

### 公路客運

  - [新竹客運](../Page/新竹客運.md "wikilink")：
      - **<font color="red">5668</font>** 苗栗－後龍－通霄鎮公所
  - [苗栗客運](../Page/苗栗客運.md "wikilink")：
      - **<font color="red">5808</font>** 高鐵苗栗站 - 後龍－通霄 - 苑裡 - 大甲

## 歷史

  - 1922年10月11日：「白沙屯驛」開業。
  - 2015年6月30日：啟用多卡通刷卡機。

## 鄰近車站

## 參考文獻

## 外部連結

  - [白沙屯車站（臺灣鐵路管理局）](http://www.railway.gov.tw/Taichung-Transportation/cp.aspx?sn=11795)

[Category:苗栗縣鐵路車站](../Category/苗栗縣鐵路車站.md "wikilink")
[Category:通霄鎮](../Category/通霄鎮.md "wikilink")
[Category:1922年启用的铁路车站](../Category/1922年启用的铁路车站.md "wikilink")
[Category:海岸線車站](../Category/海岸線車站.md "wikilink")