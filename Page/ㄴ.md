[ㄴ_(nieun)_stroke_order.png](https://zh.wikipedia.org/wiki/File:ㄴ_\(nieun\)_stroke_order.png "fig:ㄴ_(nieun)_stroke_order.png")
****是[朝鮮語](../Page/朝鮮語.md "wikilink")[諺文的一个辅音字母](../Page/諺文.md "wikilink")，被稱為「****」，在[韓國及](../Page/韓國.md "wikilink")[朝鮮的谚文字母表上的排序分別為第三及第二](../Page/朝鮮.md "wikilink")。

ㄴ可作[初声和](../Page/初声.md "wikilink")[终声](../Page/终声.md "wikilink")，均读作[齿龈鼻音](../Page/齿龈鼻音.md "wikilink")。在[馬-賴轉寫中](../Page/馬科恩-賴肖爾轉寫系統.md "wikilink")，初声与终声均转写为n；在[文观部式中](../Page/文化观光部2000年式.md "wikilink")，作初声与终声均转写为n。

ㄴ在[朝鲜语盲文中](../Page/朝鲜语盲文.md "wikilink")，作初声时为[Braille_C3.svg](https://zh.wikipedia.org/wiki/File:Braille_C3.svg "fig:Braille_C3.svg")，作终声时为[Braille_Colon.svg](https://zh.wikipedia.org/wiki/File:Braille_Colon.svg "fig:Braille_Colon.svg")。其[EUC-KR编码为A](../Page/EUC.md "wikilink")4A4，[摩尔斯电码为](../Page/摩尔斯电码.md "wikilink")“··-·”。

## 复合字母

  - [ᄓ](../Page/ᄓ.md "wikilink")
  - [ㅥ](../Page/ㅥ.md "wikilink")
  - [ㅦ](../Page/ㅦ.md "wikilink")
  - [ퟋ](../Page/ퟋ.md "wikilink")
  - [ᄖ](../Page/ᄖ.md "wikilink")
  - [ㅧ](../Page/ㅧ.md "wikilink")
  - [ㅨ](../Page/ㅨ.md "wikilink")
  - [ㄵ](../Page/ㄵ.md "wikilink")
  - [ퟌ](../Page/ퟌ.md "wikilink")
  - [ᇉ](../Page/ᇉ.md "wikilink")
  - [ㄶ](../Page/ㄶ.md "wikilink")

## 字符编码

<table>
<thead>
<tr class="header">
<th><p>种类</p></th>
<th><p>用途</p></th>
<th><p><a href="../Page/Unicode.md" title="wikilink">Unicode编码</a></p></th>
<th><p><a href="../Page/HTML.md" title="wikilink">HTML编码</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>朝鲜语互换字母</strong></p></td>
<td><p>ㄴ</p></td>
<td><p>U+3131</p></td>
<td><p><code>ㄴ</code></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/古諺文#現代的古諺文.md" title="wikilink">朝鲜语字母应用</a></strong></p></td>
<td><p>初声</p></td>
<td></td>
<td><p>U+3134</p></td>
</tr>
<tr class="odd">
<td><p>终声</p></td>
<td></td>
<td><p>U+11AB</p></td>
<td><p><code>ᆫ</code></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/古諺文#古諺文在電腦中的顯示.md" title="wikilink">汉阳私人使用区应用</a></strong></p></td>
<td><p>初声</p></td>
<td></td>
<td><p>U+F788</p></td>
</tr>
<tr class="odd">
<td><p>终声</p></td>
<td></td>
<td><p>U+F875</p></td>
<td><p><code></code></p></td>
</tr>
<tr class="even">
<td><p><strong>半角</strong></p></td>
<td></td>
<td></td>
<td><p>U+FFA4</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

[Category:諺文字母](../Category/諺文字母.md "wikilink")