**無綫電視動畫**，是由[香港](../Page/香港.md "wikilink")[電視廣播有限公司所屬的](../Page/電視廣播有限公司.md "wikilink")[無綫電視配音組](../Page/無綫電視配音組.md "wikilink")[配音並在旗下頻道播出的](../Page/配音.md "wikilink")[動畫](../Page/動畫.md "wikilink")，絕大部份為[日本及](../Page/日本.md "wikilink")[歐美的外購動畫](../Page/歐美.md "wikilink")（不含[迪士尼動畫及](../Page/迪士尼動畫.md "wikilink")[動畫電影](../Page/動畫電影.md "wikilink")），僅有少部份是無綫電視自行製作或其[子公司](../Page/子公司.md "wikilink")[翡翠動畫製作](../Page/翡翠動畫.md "wikilink")。播出頻道主要有[翡翠台](../Page/翡翠台.md "wikilink")、[明珠台](../Page/明珠台.md "wikilink")、[無綫兒童台](../Page/無綫兒童台.md "wikilink")、[高清翡翠台](../Page/高清翡翠台.md "wikilink")、[J2](../Page/J2.md "wikilink")，發售版本有[VCD](../Page/VCD.md "wikilink")、[DVD-Video](../Page/DVD-Video.md "wikilink")。

## 類型

### 自製動畫

<table>
<thead>
<tr class="header">
<th><p>首播日期</p></th>
<th><p>頻道</p></th>
<th><p>中文名稱</p></th>
<th><p>集數</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2003年7月12日<br />
2004年11月13日<br />
2007年12月12日</p></td>
<td><p><a href="../Page/翡翠台.md" title="wikilink">翡翠台</a></p></td>
<td><p><a href="../Page/神鵰俠侶_(動畫).md" title="wikilink">神鵰俠侶</a><br />
<a href="../Page/神鵰俠侶_(動畫).md" title="wikilink">神鵰俠侶II 襄陽風雲</a><br />
<a href="../Page/神鵰俠侶_(動畫).md" title="wikilink">神鵰俠侶III 十六年之約</a></p></td>
<td><p>26<br />
26<br />
26</p></td>
<td><p>第1輯 (港日合作)<br />
第2輯<br />
第3輯</p></td>
</tr>
<tr class="even">
<td><p>2004年6月4日</p></td>
<td><p><a href="../Page/翡翠台.md" title="wikilink">翡翠台</a></p></td>
<td><p><a href="../Page/大頭狗仔隊.md" title="wikilink">大頭狗仔隊</a></p></td>
<td><p>39</p></td>
<td><p>港日合作</p></td>
</tr>
<tr class="odd">
<td><p>約1994年</p></td>
<td><p><a href="../Page/翡翠台.md" title="wikilink">翡翠台</a></p></td>
<td><p><a href="../Page/成語動畫廊.md" title="wikilink">成語動畫廊</a></p></td>
<td><p>180</p></td>
<td><p>自製動畫</p></td>
</tr>
</tbody>
</table>

### 外購動畫

主要有三大類別，為日本動畫、美國動畫、中國動畫，現在還有韓國動畫以及其他香港公司製作的動畫。日本動畫播放量最高，幾乎每日達三小時以上（所有頻道合計）。近年播放中國動畫明顯增多，估計是因為中國動畫價格較低。

## 特色

### 與美日之區別

片頭或標題處，美日地區播出都不會讀標題。而無綫動畫因應香港觀眾不懂外语，配音员會讀標題，而且現時已經成為習慣。

### 主题曲

早年无线对大多数外购动画的主题曲重新填词演唱，或重新作曲。使得整套动画深入人心，动画主题曲更成为当时经典儿歌。但2010年後粵版主题曲數量大幅減少。

### 禁忌

2000年代中，無線動畫中不允許講「死」、「吸煙」、「飲酒」，因此被批評破壞原著；此禁忌當時也適用於無綫所有配音節目。但在J2台啟播後，此禁忌被打破。

### 刪剪

無綫的外購動畫有大部分都經過刪剪。然而早期，大多數觀眾只能通過無綫的免費電視來看動畫，而不清楚無綫這個動作。直至近年，日本動畫能方便地觀看，甚至是先看日語原版再看無綫配音版。這種改變下，觀眾近年發現無綫外購的動畫內容被刪得十分嚴重。外購動畫中的片頭[回顧或下集](../Page/回顧.md "wikilink")[預告都會被刪除](../Page/預告.md "wikilink")，中國大陸或台灣經過後期製作後仍會保留。

播放內容方面，受到香港《廣播法例》的限制，需要對部分涉及色情、暴力、血腥的畫面刪除。某些畫面，如暴露女性胸部、暴露女性內衣褲、男女性[全裸](../Page/全裸.md "wikilink")（但不涉及[性器官](../Page/性器官.md "wikilink")）、以及非禮動作的鏡頭或一些呻吟聲，若在日間（兒童動畫時段）播出時必然刪除，但在夜間播出時亦未必保留。此大幅修改的行為使部分年青觀眾無法接受，絕大多數的動畫投訴則是源於無綫大量刪減趣味性高的內容或原作中不可忽略的重要劇情。另外一個原因是無綫通過刪減內容以保證其廣告時間，在OVA（30分鐘）上更刪至僅有一般TV動畫一集的時間（衹剩20分鐘）。

2010年12月，[J2實行首播動畫](../Page/J2.md "wikilink")，全部足本播映，即不刪剪原版預告、過場；有些動畫沒有預告，無綫更會自製預告；不過，仍會有少許刪剪，大致上會保持足本。第一套實行足本動畫是《[WORKING\!\!
無聊西餐廳](../Page/WORKING!!_無聊西餐廳.md "wikilink")》。遇上一些片長達27分鐘的OVA或動畫，J2更會縮短廣告時間以足本播映。

[翡翠台方面](../Page/翡翠台.md "wikilink")，則除預告、過場之外，全部都會完整播放；在時間不足的情況下所作出的刪剪，翡翠台會於重播時播回。一些緊貼日本播映的動畫、或於J2播映的動畫，翡翠台則會保留足本播放，如《[機動戰士AGE](../Page/機動戰士AGE.md "wikilink")》、《[FAIRY
TAIL魔導少年](../Page/FAIRY_TAIL魔導少年.md "wikilink")》。

## 播出時間

### TV動畫

一般播出時間為星期一至星期五下午15:50至17:50（公眾假期為18:00），學校假期也有在早上10:30至11:30播出（重播動畫）。

[星期六或星期日上午](../Page/星期六.md "wikilink")9:30至12:00及下午16:00至18:00。

星期日凌晨0:15後播出長篇動畫，一般為兩套（每套一小時），有時為一套。由於J2開台，此安排已經於2010年取消。
[星期六一旦遇到籌款節目](../Page/星期六.md "wikilink")、重大節日或特別新聞報道，晚間動畫則多被取消。

### OVA、劇場版

有時在[星期六或星期日下午](../Page/星期六.md "wikilink")13:15（《[午間新聞](../Page/無綫電視午間新聞.md "wikilink")》）後播出。節日或重大日子，有時在上午10:00後播出劇場版。也有在公眾假期上午播放劇場版動畫，一般時間為10:00開始，最遲不超過12:45。

## 評價

無綫電視的配音動畫經常出現更換配音員的情況，某些動畫角色不明原因的情況下被臨時替換，有時在其中一、兩集中臨時替換，有時是永久更換。每當有配音員離職時，總會使播放中的動畫角色被迫換人，而最受非議的是主角配音員被更換。近年[粵語正音運動更波及動畫](../Page/粵語正音運動.md "wikilink")，令觀眾不滿增多。

## 影响

翡翠台是香港首个使用無線信號的免费电视台。互聯網及盜版仍未出現時，外国动画的录像带、VCD、DVD等媒介非常昂贵，可供出租或銷售的類型又太少，大多数小孩衹能靠免费的电视台来收看动画，动画收视率在香港最高。

## 外部連結

  - [無綫電視節目表](http://schedule.tvb.com/home/)
  - [香港動畫資訊網](http://kouji.no-ip.org/hkaiw/index.htm)

[無綫電視動畫](../Category/無綫電視動畫.md "wikilink")