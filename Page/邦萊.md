**邦萊**（**Bunlap**）是位在南[太平洋群島國](../Page/太平洋.md "wikilink")[萬那杜的](../Page/萬那杜.md "wikilink")[五旬節島](../Page/五旬節島.md "wikilink")（Pentecost
Island）上，[美拉尼西亞](../Page/美拉尼西亞.md "wikilink")（Melanesian）[原住民的部落](../Page/原住民.md "wikilink")。他們的祖先大約在2000年前乘[獨木舟來了到此島](../Page/獨木舟.md "wikilink")。當海島上的其它部落基本上全盤西化的同時，邦萊人仍然保留著傳統的生活方式。

## 傳統服飾

邦萊人仍然穿著傳統服飾，女性穿著由纖維長條所做成，長及膝蓋的[裙子](../Page/裙子.md "wikilink")，在進行儀式時，則穿著長及腳踝的裙子。男性只在[腰部綁一條寬布帶](../Page/腰.md "wikilink")，布帶前繫著布料或葉片做成的管狀護套，套住[陰莖](../Page/陰莖.md "wikilink")，外生殖器的剩餘部份（陰囊）和屁股則暴露在外。

## 割禮

每年六月，邦萊人要為五歲的男童進行割[包皮的儀式](../Page/包皮.md "wikilink")，這是部落年度最大的慶典，象徵男童將成為男子漢，並開始在部落中成為新的階級，一生會不斷晉升。

[割禮前先要建造芋頭儲藏室](../Page/割禮.md "wikilink")，採收[椰子](../Page/椰子.md "wikilink")，用樹幹製造架設割禮聖鼓，並由專人用[竹子製作專用的銳利竹刀供割包皮使用](../Page/竹子.md "wikilink")。而進行割禮男童的[舅舅則必須捐贈小](../Page/舅舅.md "wikilink")[豬給男童](../Page/豬.md "wikilink")，以便進行割禮結束後的殺豬儀式。

進行割禮時，先遮住男童[眼睛](../Page/眼睛.md "wikilink")，以竹刀割下包皮，隨後用傳統草藥汁滴在傷口。男童需在男人屋住七個星期，直到傷口癒合，這段期間男童禁止與[母親見面](../Page/母親.md "wikilink")，且男童父母只喝[椰奶以祈求傷口順利癒合及天氣晴朗](../Page/椰奶.md "wikilink")。而其他族人則大量採收[芋頭及](../Page/芋頭.md "wikilink")[薯蕷](../Page/薯蕷.md "wikilink")，為接下來的儀式準備，男童們的家族需共同填滿芋頭儲藏室。第六週時，傷口已痊癒，大人們用芋頭、薯蕷、椰奶烤製拉普拉普派（lap
lap
pie），讓男孩們食用，象徵男童將重回部落。第七週，慶典開始，當割禮聖鼓敲響，男童便可離開男人屋，所有人圍繞著聖鼓吶喊、唱歌、跳割禮舞慶祝，並貢獻巨大的薯蕷、芋頭及椰子。慶典結束後參加者共同分享所有的食物。

## 慶典

邦萊人每年要舉行古老的慶典，稱為Gkol，慶典的重頭戲是陸上彈跳〔land
diving〕。Gkol源自一個發生在西元500年前後的傳奇故事，邦萊部落有個叫Tamalie的[男人與他的](../Page/男人.md "wikilink")[妻子發生爭吵](../Page/妻子.md "wikilink")，妻子受不了[丈夫的虐待而逃跑到樹林裏](../Page/丈夫.md "wikilink")，攀爬上[印度榕樹](../Page/印度榕樹.md "wikilink")（banyan）,
她將藤蔓纏繞在腳踝。當Tamalie
追上來，妻子匆忙從樹上跳下，丈夫不知道妻子做了防護，也跟著縱身一跳。結果Tamalie命喪黃泉，但藤蔓救了妻子一命。這樣的結果使邦萊男人銘記在心，他們開始訓練這種跳躍，以防萬一有一天他們遇到相似的情況。這種習俗後來轉變成慶典的儀式，以歡慶[薯蕷的豐收和證明男人步入成熟](../Page/薯蕷.md "wikilink")。

慶典開始之前需要搭建邦萊高塔，高塔是[女人的形象](../Page/女人.md "wikilink")，象徵對女性的尊崇。塔的設計是由部落中年長有經驗的建築師負責，選擇在具有柔軟沙土的斜坡地，以足夠高的[椰子樹為主幹](../Page/椰子樹.md "wikilink")，並在樹林中砍伐堅固的樹幹及樹枝為支柱，由部落的男人共同搭建，至少要花費一週的時間。築塔期間，男人們必須住在男人屋，不能與女人同房，但可以喝卡法酒助興。女人則負責準備食物、薯蕷派及照顧小孩。部落[巫師會唸頌](../Page/巫師.md "wikilink")[咒語](../Page/咒語.md "wikilink")，並燒煙祈求祖靈庇祐參賽者的安全，及驅雨。

高塔上會搭建許多不同高度的平台，最高的甚至有八層樓高。參賽者各自尋找長度恰當、乾濕適中的藤蔓，這點攸關性命，馬虎不得。未成年的男孩們也可以參加，會選擇從較低的平台跳下，以訓練勇氣。而年輕男人們則向高度挑戰。成功地自最高平台躍下的冠軍男人，可獲得一把樹葉作為獎賞，並會得到許多女人的芳心，可在部落中擁有更多的妻妾。所有人在比賽結束後會一起享用薯蕷派。

慶典在[晴天開始](../Page/晴天.md "wikilink")，女人圍著高塔[唱歌](../Page/唱歌.md "wikilink")[跳舞](../Page/跳舞.md "wikilink")，參賽男人爬上高塔，在腳踝綁上自己的藤蔓，走向突出的平台，雙臂高舉，伸向天空拍掌，感受與塔靈合一之後，兩手握拳置於胸前，在塔靈的指引下，頭朝下的往下跳。

參賽者的下墜之勢被另一端栓在塔上的藤蔓所中止。在藤蔓斷裂、藤蔓長度不正確，或拙劣的跳躍方式的情況下，位於高塔底部，由柔軟的土壤構成的斜坡提供一些保護，使參賽者免受傷害。

雖然使用藤蔓的彈性遠不如蹦極繩（bungee
cords），並且參賽者在下墜結束時並不彈起，但陸上彈跳激發了[高空彈跳或稱](../Page/高空彈跳.md "wikilink")[蹦極](../Page/蹦極.md "wikilink")（bungee
jumping）這種現代極限運動的誕生。

## 飲食

在海島內陸的斜坡地，村民植[芋作為主食](../Page/芋.md "wikilink")。

邦萊男人常在日暮後喝**卡法酒**，為近似[酒精](../Page/酒精.md "wikilink")，具有令人興奮及陶醉的作用的[飲料](../Page/飲料.md "wikilink")，係萃取自[卡法樹](../Page/卡法.md "wikilink")（Kava）的根。而婦女不得飲用卡法酒。

## 資料來源

Bunlap. (2006, July 30). In Wikipedia, The Free Encyclopedia. Retrieved
16:30, July 30, 2006, from
<http://en.wikipedia.org/w/index.php?title=Bunlap&oldid=66701127>.

[Category:大洋洲原住民](../Category/大洋洲原住民.md "wikilink")
[Category:部落習俗](../Category/部落習俗.md "wikilink")