**[巴基斯坦](../Page/巴基斯坦.md "wikilink")**位于[南亚西北部](../Page/南亚.md "wikilink")，南濒[阿拉伯海](../Page/阿拉伯海.md "wikilink")，[国土面积](../Page/国家面积列表.md "wikilink")803,940平方公里；毗邻国家有[印度](../Page/印度.md "wikilink")、[中国](../Page/中国.md "wikilink")、[阿富汗和](../Page/阿富汗.md "wikilink")[伊朗](../Page/伊朗.md "wikilink")。巴处在北纬23度30分至36度45分，东经61度至75度31分之间，南北长1,600多公里\[1\]，主要山脉有[喀喇昆仑山脉](../Page/喀喇昆仑山脉.md "wikilink")、[喜马拉雅山脉](../Page/喜马拉雅山脉.md "wikilink")、[兴都库什山脉和](../Page/兴都库什山脉.md "wikilink")[苏莱曼山脉](../Page/苏莱曼山脉.md "wikilink")；最长河流[印度河自北向南几乎穿贯巴全境](../Page/印度河.md "wikilink")，和其四条主要支流杰卢姆河、奇纳布河、拉维河和萨特勒季河一起被称为“五水”；西部边境的[开伯尔山口和](../Page/开伯尔山口.md "wikilink")[波伦山口是传统上连接中亚和南亚的必经要道](../Page/波伦山口.md "wikilink")。印度河平原为巴最富庶的农业区，有世界上最发达的灌溉网络。接近[海平面的](../Page/海平面.md "wikilink")[喀奇湿地为陆地最低点](../Page/喀奇湿地.md "wikilink")，位于中、巴边界的[乔戈里峰海拔](../Page/乔戈里峰.md "wikilink")8,611米为至高点；由于[地势高低差异](../Page/地形.md "wikilink")，造成[气候的多样化](../Page/气候.md "wikilink")。受[欧亚与](../Page/歐亞大陸板塊.md "wikilink")[印度二大](../Page/印度板块.md "wikilink")[板块撞击的影响](../Page/板块构造论.md "wikilink")，使得巴基斯坦成为[地震多发区](../Page/地震.md "wikilink")。

## 地域范围

[Bajisitan-bianjie-zh.png](https://zh.wikipedia.org/wiki/File:Bajisitan-bianjie-zh.png "fig:Bajisitan-bianjie-zh.png")
巴基斯坦西和伊朗交界，西北和[阿富汗接邻](../Page/阿富汗.md "wikilink")，东北和[中国接壤](../Page/中国.md "wikilink")，东和[印度相连](../Page/印度.md "wikilink")，南濱临[阿拉伯海](../Page/阿拉伯海.md "wikilink")。由於位處地理要衝，因此與週邊鄰國有許多領土及邊界爭議。

### 巴伊邊界線

巴基斯坦独立以前，原属[英属印度的一部分](../Page/英属印度.md "wikilink")。1893年，英属印度政府与[伊朗兩国在](../Page/伊朗.md "wikilink")[俾路支地区劃定长](../Page/俾路支地区.md "wikilink")909公里的边界线，并于1957年正式签署边界协议，至此不再有边界纠纷。

### 巴阿边界线

在巴基斯坦西北側与阿富汗有长达2,640公里的边境线。其中北邊延[兴都库什山脉和](../Page/兴都库什山脉.md "wikilink")[帕米尔高原山脊](../Page/帕米尔高原.md "wikilink")，在地处阿富汗國境內為一处狭长地带称为[瓦罕走廊](../Page/瓦罕走廊.md "wikilink")，此通連[中國](../Page/中國.md "wikilink")[新疆的東西向戰略要道同時區分了巴基斯坦與未接壤的](../Page/新疆.md "wikilink")[塔吉克斯坦](../Page/塔吉克斯坦.md "wikilink")。传统上兴都库什山脉被认为是[印度教徒抵御中亚](../Page/印度教.md "wikilink")[入侵的天然](../Page/入侵.md "wikilink")[屏障](../Page/屏障.md "wikilink")。目前巴阿实际边境线称为[杜兰德线](../Page/杜兰德线.md "wikilink")，该线于1893年由当时的[英属印度政府外务大臣](../Page/英属印度.md "wikilink")[莫蒂默·杜兰德](../Page/莫蒂默·杜兰德.md "wikilink")([Mortimer
Durand](../Page/:en:Mortimer_Durand.md "wikilink"))主持划定，1947年[印巴分治以前未遭到什么困扰](../Page/印巴分治.md "wikilink")。该线的合法性后来遭到了阿富汗政府和跨越巴阿边境的[普什图族质疑](../Page/普什图族.md "wikilink")。阿富汗声称杜兰德线是强者强加给弱者的，而这条线对重建一个国家——[普什图尼斯坦有利](../Page/普什图尼斯坦.md "wikilink")；巴基斯坦則声称自己是英属印度在该地区的继承者，重申该线的永久合法性，故杜兰德线现仍为巴阿兩国的实际分界线。

### 巴中边境线

巴基斯坦与阿富汗东部的边界终点也就是[中国](../Page/中国.md "wikilink")、巴基斯坦边境的起点，向东南延伸至[锡亚琴冰川山口为止](../Page/锡亚琴冰川.md "wikilink")，全长约500公里。从1961年至1965年，中、阿兩国签署一系列关于解决边界问题的协议，最终並达成新的边界条约。

### 巴控克什米尔

巴基斯坦实际控制原[查谟克什米尔土邦](../Page/查谟-克什米尔_\(土邦\).md "wikilink")84,159平方公里的面积，包括[阿扎德克什米尔和](../Page/自由克什米爾.md "wikilink")[北部地区](../Page/北部地区_\(巴控克什米尔\).md "wikilink")。阿扎德克什米尔面积11,639平方公里；北部地区包括[吉尔吉特和](../Page/吉尔吉特县.md "wikilink")[巴尔蒂斯坦兩地](../Page/巴尔蒂斯坦.md "wikilink")，总面积72,520平方公里。北部地区具有极大的特点，世界上14座8千米以上的山峰在该地区有4座，同时因其拥有众多的[冰川而被誉为](../Page/冰川.md "wikilink")“地球第三极”（[北極和](../Page/北極.md "wikilink")[南極之外的第三極](../Page/南極.md "wikilink")）。1947年[印巴分治以后](../Page/印巴分治.md "wikilink")，这一地区的边界纠纷也是印、巴兩国纠纷源头。自1984年以来，[锡亚琴冰川一带成为印](../Page/锡亚琴冰川.md "wikilink")、巴兩国解决边界纠纷的竞技场，冲突双方有很多军人就长眠于此。

### 巴印边境线

巴印停火线自[锡亚琴冰川附近開始向西再轉向西南延伸至](../Page/锡亚琴冰川.md "wikilink")[拉合尔东北约](../Page/拉合尔.md "wikilink")130公里處，全长770公里，于1947年至1948年印巴战争结束期間由联合国主持划定。该停火协议于1949年1月1日正式生效，之后又经历长达18个月的交战，于1972年7月再签署《[西姆拉条约](../Page/西姆拉条约.md "wikilink")》，因此停火线有所调整。自此以后，印巴停火线也被称为人们熟知的兩国“实际控制线”。

巴印边界總共长1,280公里。印度独立和[印巴分治时](../Page/印巴分治.md "wikilink")，[旁遮普與](../Page/旁遮普地区.md "wikilink")[孟加拉地区边界由](../Page/孟加拉.md "wikilink")[英国边界委员会](../Page/英国边界委员会.md "wikilink")([Boundary
Commission](../Page/:en:Boundary_Commission_\(United_Kingdom\).md "wikilink"))主席[西里尔·拉德克利夫于](../Page/西里尔·约翰·拉德克利夫.md "wikilink")1947年主持划定，最终方案以其名字冠称，为“拉德克利夫裁决”。尽管边界划分没有遭到原则上的争议，但边界兩边群情亢奋。印度人一直期望原有的边界进一步西移，因此放弃了[拉合尔](../Page/拉合尔.md "wikilink")；巴基斯坦则希望边界进一步朝东移动，进而得到原[莫卧儿帝国](../Page/莫卧儿帝国.md "wikilink")[首都](../Page/首都.md "wikilink")[德里的控制权](../Page/德里.md "wikilink")。

南部边界不像北部边界有許多爭議。位于[信德省的](../Page/信德省.md "wikilink")[塔尔沙漠与](../Page/塔尔沙漠.md "wikilink")[喀奇濕地南侧交界处的边界问题在](../Page/喀奇濕地.md "wikilink")1923年至1924年間首度釐清。[印巴分治以后](../Page/印巴分治.md "wikilink")，巴基斯坦對信德省南部边界有所爭議並向東取得較大的領土。和1965年爆发于[克什米尔的](../Page/克什米尔.md "wikilink")[印巴战争相比](../Page/印巴战争.md "wikilink")，这一带边界爭議不多，涉及的范围不广。因为南部边界导致的敌意问题在[英国的斡旋下](../Page/英国.md "wikilink")，由[联合国主持于](../Page/联合国.md "wikilink")1968年2月19日签署条约。涉及的边界线总长度为403公里，最终由印、巴兩国共同勘探。巴基斯坦开始申明的领土争议面积9100平方公里，最终获得780平方公里。边界南端长80公里，呈東至西南走向，至[阿拉伯海的爵士灣](../Page/阿拉伯海.md "wikilink")(Sir
Creek)入海口，具体位置双方至今仍在争议之中。

## 地理環境

[Bajisitan-ditu-2002-zh.jpg](https://zh.wikipedia.org/wiki/File:Bajisitan-ditu-2002-zh.jpg "fig:Bajisitan-ditu-2002-zh.jpg")
[巴基斯坦大致分为三大地理区](../Page/巴基斯坦.md "wikilink")：北部高地、[印度河平原](../Page/印度河平原.md "wikilink")（包括[旁遮普省和](../Page/旁遮普省.md "wikilink")[信德省](../Page/信德省.md "wikilink")）、俾路支高原。另外有些地理学家指出还有另外的地区，如与阿富汗交界的西部山区不属于俾路支高原；与印度的交界地区，诸如[萨特累季河以南有個](../Page/薩特萊傑河.md "wikilink")[塔尔沙漠将印度河平原分隔开来](../Page/塔尔沙漠.md "wikilink")。不过用一条自[开伯尔山口向东延伸的线](../Page/开伯尔山口.md "wikilink")、一条从[伊斯兰堡朝西南沿该国中部走向的线可以简单勾勒出三大地理區](../Page/伊斯兰堡.md "wikilink")。

### 北部高地

北部高地属于[兴都库什山](../Page/兴都库什山脉.md "wikilink")、[喀喇昆仑山和](../Page/喀喇昆仑山脉.md "wikilink")[喜马拉雅山地区](../Page/喜马拉雅山脉.md "wikilink")。这一带有世界第二大[山峰](../Page/山峰.md "wikilink")——8,611米的[乔戈里峰](../Page/喬戈里峰.md "wikilink")、世界第九大峰——8,215米的[南迦帕尔巴特峰](../Page/南迦帕尔巴特峰.md "wikilink")，超过半数的山峰標高[海拔](../Page/海拔.md "wikilink")4,500米以上，50座山峰海拔超过6,500米。在这一带[旅游和](../Page/旅游.md "wikilink")[探险困难重重且充满危险](../Page/探险.md "wikilink")，当局一直企图将部分地方开辟为旅游区卻徒勞無功。由于地形崎岖和气候恶劣，作为一道难以逾越的天然屏障，使巴基斯坦免於北部的动荡和入侵。

### 俾路支高原

北部高地以南和印度河平原以西，是沿阿富汗边境的[沙非德山脉](../Page/沙非德山脉.md "wikilink")([Safed
Koh](../Page/:en:Safed_Koh.md "wikilink"))、[苏来曼山脉和](../Page/苏来曼山脉.md "wikilink")[凯撒尔山脉](../Page/凯撒尔山脉.md "wikilink")([Kirthar
Mountains](../Page/:en:Kirthar_Mountains.md "wikilink"))，这些山脈构成信德省的西邊界，而且几乎向南一直延伸到阿拉伯海。海拔较低的地区和北部相比天气多[旱](../Page/干旱.md "wikilink")，其山脉大体上向西南延伸横跨俾路支省。信德省、俾路支省的南部到北部山谷构成了天然屏障，阻礙了来自阿拉伯海以西的民族经[莫克兰](../Page/莫克兰.md "wikilink")([Makran](../Page/:en:Makran.md "wikilink"))海岸向印度河平原迁徙的过程。

沿阿富汗边境有数个[山口将](../Page/山口.md "wikilink")[山脊切断](../Page/山脊.md "wikilink")。科加山口（Khojak）位于俾路支省[基达西北约](../Page/基达.md "wikilink")80公里处；[开伯尔山口位于](../Page/开伯尔山口.md "wikilink")[白沙瓦以西](../Page/白沙瓦.md "wikilink")40公里，是通往阿富汗首都[喀布尔的要道](../Page/喀布尔.md "wikilink")；更北一些的[布罗葛尔山口](../Page/布罗葛尔山口.md "wikilink")([Broghol](../Page/:en:Broghol.md "wikilink"))是通向[瓦罕走廊的必经之路](../Page/瓦罕走廊.md "wikilink")。

### 印度河平原

巴基斯坦只有不到五分之一的陆地适合[农业开发](../Page/农业.md "wikilink")，几乎所有的可耕地都用于耕作，但是其产能卻低于世界平均。北方高寒山区、南部沙漠和西部高原农业耕种稀少；在[旁遮普和](../Page/旁遮普省.md "wikilink")[信德北部](../Page/信德省.md "wikilink")，其肥沃的土地几乎养活了巴基斯坦全国庞大的[人口](../Page/人口.md "wikilink")。

“[印度河](../Page/印度河.md "wikilink")”(Indus)之名源自[梵语](../Page/梵语.md "wikilink")“sindhu”，“sindhu”梵语即“海洋”之意，由
“sindhu”派生的词语有Sindh（信德地区，信德省）、Hindu（[印度教](../Page/印度教.md "wikilink")，印度教徒）和India（印度）等。作为世界主要大河和巴基斯坦的重要河流，印度河的源自于中国[西藏](../Page/西藏.md "wikilink")，另一條同樣源自於西藏流入巴基斯坦的河为[萨特累季河](../Page/薩特萊傑河.md "wikilink")，萨特累季河于[旁遮普地区汇入印度河](../Page/旁遮普地区.md "wikilink")。印度河在巴基斯坦境内的主要支流有[喀布尔河](../Page/喀布尔河.md "wikilink")、[杰赫勒姆河](../Page/杰赫勒姆河.md "wikilink")（Jhelum）、[奇纳布河](../Page/奇纳布河.md "wikilink")（Chenab）、[拉维河](../Page/拉维河.md "wikilink")（Ravi）与[萨特累季河](../Page/薩特萊傑河.md "wikilink")（Sutlej）。印度河[集水区超过](../Page/集水区.md "wikilink")1百万平方公里，由淤泥形成的冲积平原——印度河[盆地因而土壤肥沃](../Page/盆地.md "wikilink")，據此發展的[农业文明至少超过五千年](../Page/农业文明.md "wikilink")。

### 水利建設

印度河上游盆地包括旁遮普地区，下游盆地自东部支流的汇合处——[潘克赫斯特河](../Page/潘克赫斯特河.md "wikilink")([Panjnad](../Page/:en:Panjnad_River.md "wikilink"))开始，向南延伸至阿拉伯海沿岸。旁遮普(Punjab)原意就是“五水之地”之意即印度河、杰赫勒、奇纳布、拉维和萨特累季河匯流的地区，而萨特累季河主要位于印度境内。[英属印度时期](../Page/英属印度.md "wikilink")，英国人在[旁遮普省南部建立了](../Page/旁遮普省.md "wikilink")[运河系统](../Page/运河.md "wikilink")，使原本干旱的地区出现了精耕细作的农业。当时的英国人企图用水利灌溉來控制这一地区，終使这地区的社会和政治受到深远的影响。

巴基斯坦有兩大拦河[大坝](../Page/大坝.md "wikilink")：邻近著名佛教遗址[塔克西拉](../Page/塔克西拉.md "wikilink")([Taxila](../Page/:en:Taxila.md "wikilink"))的[塔贝拉大坝](../Page/塔贝拉大坝.md "wikilink")([Tarbela](../Page/:en:Tarbela_Dam.md "wikilink"))以及[旁遮普省与](../Page/旁遮普省.md "wikilink")[阿扎德克什米尔兩省分界河](../Page/自由克什米爾.md "wikilink")——杰赫勒河上的[曼格拉大坝](../Page/曼格拉大坝.md "wikilink")([Mangla](../Page/:en:Mangla_Dam.md "wikilink"))。另一個瓦萨克大坝(Warsak)规模较小，位在邻近[白沙瓦的喀布尔河上](../Page/白沙瓦.md "wikilink")。这些水利工程在英属印度时期由英国人主持修造，巴国独立以后进行了扩建改造，至今对巴国的国民经济发挥着不可替代的作用；尽管1992年洪水对旁遮普和北方高原造成毁灭性的影响，但这些工程仍能承受这场[洪水的考验](../Page/洪水.md "wikilink")。

### 板块运动与地震

巴地处[欧亚板块和](../Page/歐亞大陸板塊.md "wikilink")[印度板块的结合部](../Page/印度板块.md "wikilink")。受二大[板块撞击的影响](../Page/板块构造论.md "wikilink")，[地震频繁](../Page/地震.md "wikilink")。东南部的[旁遮普省和](../Page/旁遮普省.md "wikilink")[信德省位于印度板块的西北角](../Page/信德省.md "wikilink")；西部[俾路支省和](../Page/俾路支省.md "wikilink")[西北边境省地处](../Page/西北邊境省.md "wikilink")[欧亚板块](../Page/欧亚板块.md "wikilink")，即位于地跨[中东与](../Page/中东.md "wikilink")[中亚的](../Page/中亚.md "wikilink")[伊朗高原东缘](../Page/伊朗高原.md "wikilink")；东北部的[北部地区和](../Page/北部地区_\(巴控克什米尔\).md "wikilink")[阿扎德克什米尔处在印度板块边缘](../Page/自由克什米爾.md "wikilink")，地理上属于中亚，这一带因两大板块的碰撞和挤压成为[地震多发区](../Page/地震.md "wikilink")。

印度板块和欧亚板块的撞击使[喜马拉雅山脉抬升](../Page/喜马拉雅山脉.md "wikilink")，并因此造成巴国境内地震频发，[基达为地震多发区](../Page/基达.md "wikilink")。1931年爆发了强烈地震，随后于1935年发生的地震震级更高，小城基达几乎全毁，邻近的军事驻地伤亡惨重，至少有2万人遇难。另外，1965年一次震中位于西北边境省[科伊斯坦县](../Page/科伊斯坦县.md "wikilink")([Kohistan](../Page/:en:Kohistan_District_\(Pakistan\).md "wikilink"))的地震也造成了严重的伤亡。发生于1991年的地震，尽管位于[西北边境省内的村庄几乎毁损殆尽](../Page/西北边境省.md "wikilink")，但这次地震伤亡人数却比1935年地震少得多。最近的一次地震即发生于2005年10月8日的[克什米尔大地震](../Page/2005年克什米尔大地震.md "wikilink")，巴控区内死亡人数达73,000人。

### 领土概况一覽

面积：

  - 总面积：803,940平方公里
  - 陆地面积：778,720平方公里
  - 水域面积：25,220平方公里

`　`
`面积比较：相當於`[`稍多于兩个`](../Page/中华人民共和国行政区面积表.md "wikilink")[`云南省`](../Page/云南省.md "wikilink")`，`[`或略少于四个`](../Page/中华人民共和国行政区面积表.md "wikilink")[`陕西省的面积`](../Page/陕西省.md "wikilink")`。`
`　`

陆地边界：

  - 陆地边界总长度：6,961公里
  - 相邻国家：[阿富汗](../Page/阿富汗.md "wikilink")：2,640[公里](../Page/公里.md "wikilink")；[中国](../Page/中国.md "wikilink")：500公里；[印度](../Page/印度.md "wikilink")：2,912公里；[伊朗](../Page/伊朗.md "wikilink")：909公里
  - 陆地[海岸线长度](../Page/海岸线.md "wikilink")：1,046公里。

领海权声明：

  - 邻接区：24[海里](../Page/海里.md "wikilink")(44公里)
  - [大陆礁层](../Page/大陆礁层.md "wikilink")：200[海里](../Page/海里.md "wikilink")
  - [海洋专属经济区](../Page/海洋专属经济区.md "wikilink")：200海里
  - [领海](../Page/领海.md "wikilink")：12海里

海拔高度之最：

  - 最低点：位于[印度洋](../Page/印度洋.md "wikilink")，海拔高度0米
  - 最高点：位于[乔戈里峰](../Page/喬戈里峰.md "wikilink")，海拔8,611米

## 气候

[Pakistan.A2005097.0615.1km.jpg](https://zh.wikipedia.org/wiki/File:Pakistan.A2005097.0615.1km.jpg "fig:Pakistan.A2005097.0615.1km.jpg")及邻近地区的[沙尘暴](../Page/沙尘暴.md "wikilink")\]\]

巴基斯坦地处[温带](../Page/温带.md "wikilink")，气候干燥，夏季炎热，而冬季因各地地理位置的不同温差极大，或凉爽或寒冷。降雨量少，而且各地差异很大。如[阿拉伯海沿岸地区通常气候温暖](../Page/阿拉伯海.md "wikilink")，而冰封雪地的[喀喇昆仑山脉和北方高地四季寒冷](../Page/喀喇昆仑山脉.md "wikilink")，只有每年的五月、六月才能吸引来自世界各地的登山爱好者。

巴基斯坦的一年分为四季，[冬季凉爽](../Page/冬季.md "wikilink")，为12月至次年2月；[春季炎热](../Page/春季.md "wikilink")、干燥，为3月至5月；[夏季多雨](../Page/夏季.md "wikilink")，6月至9月，西北属[季风气候](../Page/季风.md "wikilink")；10月至12月为季风减退季节。各地季节有差异，甚至有的地区可能季节不太分明。

首都[伊斯兰堡的](../Page/伊斯兰堡.md "wikilink")1月平均最低气温2°C，6月平均最高气温40°C。每年一半的降雨集中在7至8月，这兩个月月均降雨255毫米。其余月份降雨偏少，月均降雨量50毫米，春季多[冰雹](../Page/冰雹.md "wikilink")。

巴国最大城市[卡拉奇为全国的工商业中心](../Page/卡拉奇.md "wikilink")，和首都伊斯兰堡相比气候湿润而少雨，只有7月、8月的平均降雨量超过25毫米，其余月份极为干燥。和伊斯兰堡相比，温差也不大，1月平均最低温度13°C，夏季平均最高温度34°C。和旁遮普其他地方相比，尽管卡拉奇气温不算高，但由于湿度大，舒适感相对较低。

在旁遮普大部分地区可以感受凉爽的冬季。这个季节女性围丝巾，男性则穿着较厚实，但极少有家庭生暖。2月中旬气温开始回升，春季持续到4月中旬，之后步入夏季。反常的西南季风到5月份到达旁遮普。自1970年代早期以来，季风引发的洪灾频频发生。6月和7月是炎热的天气，尽管官方监测到少数地区出现46°C极端高温，也有过51°C高温的极端天气出现[中暑身亡的](../Page/中暑.md "wikilink")[媒体报道](../Page/媒体.md "wikilink")。曾出现的局部高温记录达到54°C，于1993年发生在[木尔坦](../Page/木尔坦.md "wikilink")([Multan](../Page/:en:Multan.md "wikilink"))。8月份的炎热因雨季的到来，舒缓了人们的不适，之后夏季的炎热慢慢结束，到了10月天气渐渐凉爽起来。

## 自然资源

巴国主要[自然资源为](../Page/自然资源.md "wikilink")[耕地](../Page/耕地.md "wikilink")，分布广泛的[天然气和](../Page/天然气.md "wikilink")[石油储藏](../Page/石油.md "wikilink")。[能源蕴藏藏量大](../Page/能源.md "wikilink")，包括天然气、石油和[煤炭资源](../Page/煤炭.md "wikilink")；[旁遮普省](../Page/旁遮普省.md "wikilink")[岩盐矿带](../Page/岩盐矿带.md "wikilink")([Salt
Range](../Page/:en:Salt_Range.md "wikilink"))为世界著名的纯[盐沉积矿](../Page/盐.md "wikilink")。全国约28％陆地宜于耕作，有世界级的灌溉系统。主要农作物有[棉花](../Page/棉花.md "wikilink")、[小麦](../Page/小麦.md "wikilink")、[大麦](../Page/大麦.md "wikilink")、[水稻](../Page/水稻.md "wikilink")、[玉米](../Page/玉米.md "wikilink")、[高梁](../Page/高梁.md "wikilink")、[粟](../Page/粟.md "wikilink")、[豆类](../Page/豆类.md "wikilink")、[甘蔗和](../Page/甘蔗.md "wikilink")[油类作物](../Page/油类作物.md "wikilink")，包括各类水果和蔬菜种植，这些作物占全国作物产出的75％。

出口的天然资源和半成品主要有[木材](../Page/木材.md "wikilink")、棉纺织品、[皮革](../Page/皮革.md "wikilink")、[水泥和](../Page/水泥.md "wikilink")[大理石等](../Page/大理石.md "wikilink")，其他出口产品包括[地毯](../Page/地毯.md "wikilink")、皮革制品和[瓷砖等](../Page/瓷砖.md "wikilink")。[铬铁矿](../Page/铬铁矿.md "wikilink")、[铁矿石](../Page/铁矿石.md "wikilink")、[铜矿](../Page/铜矿.md "wikilink")、[金和](../Page/金.md "wikilink")[银等矿藏丰富](../Page/银.md "wikilink")，其他珍贵矿石资源有[宝石和大理石](../Page/宝石.md "wikilink")，[非金属矿藏有](../Page/非金属.md "wikilink")[石灰石](../Page/石灰石.md "wikilink")、[石膏](../Page/石膏.md "wikilink")、[硫磺](../Page/硫磺.md "wikilink")、[耐火土和](../Page/耐火土.md "wikilink")[硅石等](../Page/硅石.md "wikilink")。有巨大的[水利发电蕴藏量](../Page/水利发电.md "wikilink")，但由于受到国际国内政治因素的制约致使能源开发速度缓慢，至今仍旧面临能源短缺局面。

土地利用：

  - 可耕地：27％
  - 常年作物：1%
  - 牧场：6%
  - 林地和森林：5%
  - 其他：61% (1993年)
  - 灌溉地：171,100平方公里(1993年)

## 可持续发展

[MultanEvening.jpg](https://zh.wikipedia.org/wiki/File:MultanEvening.jpg "fig:MultanEvening.jpg")
巴基斯坦国家可持续发展报告提出了三大目标，即自然资源保护、促进稳定发展和提高资源利用率。报告看起来象对中央和各省政府、商业领域、[非政府组织](../Page/非政府组织.md "wikilink")、地方团体和每个公民的行动号召。农业生产造成的[非点源污染](http://ecolife.epa.gov.tw/blog/post/873274)(<font size="2">[Nonpoint
source
pollution](../Page/:en:Nonpoint_source_pollution.md "wikilink")</font>)包括[富营养物](../Page/富营养物.md "wikilink")、沉积物、动物废料、杀虫剂和化肥等污染，农业非点污染源经过直接渗透、[地表水的冲刷造成污染](../Page/地表水.md "wikilink")，各种农事活动也造成[土壤团粒的破坏](../Page/土壤团粒.md "wikilink")；因侵蚀造成的沉积物直接影响鱼类栖息地和湿地。另外，消耗过剩的农业化学物质导致水和其他流体物质的污染。流体物质直接影响[水生物的栖息环境](../Page/水生物.md "wikilink")，诸如[温度升高](../Page/温度.md "wikilink")、水中[氧气减少](../Page/氧气.md "wikilink")。非点源污染造成地表水中绝大多数剩余营养物质为化肥、动物排泄物，结果直接导致地表水富营养化。[农业生产中控制](../Page/农业生产.md "wikilink")[病虫害的](../Page/病虫害.md "wikilink")[杀虫剂同样污染地表水资源](../Page/杀虫剂.md "wikilink")，这种污染在农田浇灌后，经过水的冲刷，经过流体物质也造成水的污染和富营养化；另外就是邻近河岸的放牧可能直接造成环境破坏。

为了争取公众和社会组织的改变习惯和尝试，国家可持续发展报告承认社会在价值观上需要有兩大改变，即恢复传统[穆斯林使用](../Page/穆斯林.md "wikilink")[坎儿井的习惯](../Page/坎儿井.md "wikilink")，恢复团体精神与责任感。

国家可持续发展报告提出了十四大优先发展的领域，即农田[水土保持](../Page/水土保持.md "wikilink")、改进[灌溉效率](../Page/灌溉.md "wikilink")、保护河流源头(分水岭)、支持[森林垦复](../Page/森林.md "wikilink")、[牧场恢复与改善牲畜饲养](../Page/牧场.md "wikilink")、保护[水体和保持](../Page/水体.md "wikilink")[水产养殖业持续稳定](../Page/水产养殖业.md "wikilink")、保护[生物多样性](../Page/生物多样性.md "wikilink")、增加[能源使用效率](../Page/能源.md "wikilink")、开发[可再生资源](../Page/可再生资源.md "wikilink")、增强[污染防治](../Page/污染.md "wikilink")、提倡节约型城市、支持机构组织节约公共资源、整合治污和环保计划、保护[文化遗产](../Page/文化遗产.md "wikilink")。并在这些领域提出了68个具体可实施项目，每个项目有长期目标和预期效果，并测算十年内的具体投入。基于环境保护的潜在影响，这些优先发展领域已经获得巴国[非政府组织](../Page/非政府组织.md "wikilink")、妇女组织以及国际非政府组织的关注，这些组织与巴国政府一道致力于推进这项工作，国家可持续发展报告建议将优先发展领域纳入巴国(1993-98)第八个五年计划。

### 环保

1990年代以前，巴基斯坦对[污染和](../Page/污染.md "wikilink")[环境保护问题并未引起重视](../Page/环境保护.md "wikilink")，但对诸如[卫生设施和](../Page/卫生.md "wikilink")[饮用水问题进行了基本的政府审查](../Page/饮用水.md "wikilink")。1987年，大约6％的[农村居民和](../Page/农村.md "wikilink")51％的城市居民具备基本的卫生设施，1990年，总计9,760万约占巴国人口80％的居民还没有[抽水馬桶](../Page/抽水馬桶.md "wikilink")，但1990年在改善饮用水供应方面获得巨大成功，约占全国人口一半的人喝上清洁的饮用水。巴国议会医药委员会(Pakistan
Medical Research
Council)的研究人员承认，全国患病者中大量因喝不洁饮用水致病，他们一直对水的安全分级问题存在置疑。全国有38％的人口喝上了因[管道输送的遭受严重污染的水](../Page/管道.md "wikilink")，但各地情况存在差异。如旁遮普地区约90％饮用水为[地下水](../Page/地下水.md "wikilink")，而信德省只有9％的居民饮用地下水。

《巴基斯坦1988至2003年中期发展规划》和目前五年国家发展计划没有提到[可持续发展战略](../Page/可持续发展.md "wikilink")。进而，对可持续发展问题没有一个完整政策。国家重点集中在粮食与食品生产、能源需求方面，对较高的人口增长没有控制，忽视了人口增长和[环境问题](../Page/环境.md "wikilink")。

《1992年巴基斯坦可持续发展报告》试图纠正国家面临的环境问题，这个报告由来自各行各业的专家起草，政府就当前因环境带来的[健康问题](../Page/健康.md "wikilink")、可持续发展的目标和适宜[人口发布了白皮书](../Page/人口.md "wikilink")。

[环境保护主义者关注的是北部高地山脊地区](../Page/环境保护主义.md "wikilink")[森林覆盖面积缩小问题](../Page/森林.md "wikilink")，这个问题最近最近在研究。1989至1990年，每年的[森林覆盖率下降千分之四](../Page/森林覆盖率.md "wikilink")，结果直接导致1990年代初期的[洪水泛滥](../Page/洪水.md "wikilink")。

[工业的扩张](../Page/工业.md "wikilink")，[工厂将](../Page/工廠.md "wikilink")[有毒物质直接排放至](../Page/毒物.md "wikilink")[大气和](../Page/大气.md "wikilink")[水中](../Page/水.md "wikilink")。自1970年代中期以来，在[旁遮普省](../Page/旁遮普省.md "wikilink")[乡村地区涌现出很多](../Page/乡村.md "wikilink")[纺织](../Page/纺织.md "wikilink")、[食品加工企业](../Page/食品.md "wikilink")，其结果导致河流、水渠被污染。为了提高[农作物产量](../Page/农作物.md "wikilink")，[化肥和](../Page/化肥.md "wikilink")[杀虫剂广泛使用](../Page/杀虫剂.md "wikilink")，全国[地下水资源面临威胁](../Page/地下水.md "wikilink")。

国家可持续发展报告记指出，固态和液态[排泄物为](../Page/排泄物.md "wikilink")[污染主要原因](../Page/污染.md "wikilink")，因不洁饮水导致各种[疾病](../Page/疾病.md "wikilink")。由于仅有城市人口的一半具备基本的卫生设施，农村地区大量的排泄物堆放在路旁和排入水道，或与固体废弃物混在一起。巴全国仅有三处[污水处理厂](../Page/污水处理厂.md "wikilink")，其中兩处只是维持间歇性生产作业，由此导致大部分污水直接排入溪水和河流，而且这些污水流动性很差，用这些污水浇灌[蔬菜](../Page/蔬菜.md "wikilink")，再次导致蔬菜的污染。在巴基斯坦，医学界认为[肠胃炎致病致死](../Page/肠胃炎.md "wikilink")，就是间接受到[水污染所致](../Page/水污染.md "wikilink")。

[Sea_of_motorcycles_scooters.jpeg](https://zh.wikipedia.org/wiki/File:Sea_of_motorcycles_scooters.jpeg "fig:Sea_of_motorcycles_scooters.jpeg")已成为[城市的主要污染源之一](../Page/城市.md "wikilink")\]\]
位置较低的地方用于填埋[固体垃圾](../Page/固体垃圾.md "wikilink")，没有采取对健康有益的[垃圾填埋方式](../Page/垃圾.md "wikilink")。国家可持续发展报告对工业有毒废物倾倒在城市[垃圾堆放区表示关注](../Page/垃圾堆填區.md "wikilink")，那些倾倒的有毒工业废物没有位置标记，没有载明倾倒的数量和[有毒物质的成份](../Page/毒物.md "wikilink")。还有需要关注的，就是城市工业区直接将废物堆放于地面，导致了浅表层地下水的污染。

[卡拉奇的水污染如此严重以至于居民只有煮沸才可以使用](../Page/卡拉奇.md "wikilink")。在这个城市，由于大多数地区排污设施和[供水系統互相交错](../Page/供水系統.md "wikilink")，水网的泄漏成为[污染的主要原因](../Page/污染.md "wikilink")。诸如此类的污染问题在[伊斯兰堡和](../Page/伊斯兰堡.md "wikilink")[拉瓦尔品第也开始出现](../Page/拉瓦尔品第.md "wikilink")。

在绝大多数城市，[空气污染渐渐成为重要问题](../Page/空气污染.md "wikilink")。[汽车](../Page/汽车.md "wikilink")[污染没有得到有效控制](../Page/污染.md "wikilink")，总计90％的空气污染来自汽车。国家可持续发展报告指出，有25％的[一氧化碳来自汽车排放](../Page/一氧化碳.md "wikilink")，是其他碳氧化物的20倍；和[美国相比](../Page/美国.md "wikilink")，汽车每公里[氮氧化物的排放量高出](../Page/氮氧化物.md "wikilink")3.5倍。

另一个问题即[噪声污染在国家可持续发展报告没有提及](../Page/噪声污染.md "wikilink")。自1960年代开始，巴基斯坦广泛的城市化，在人口高密度地区放松了对大型设备的管制，如造成街道拥挤的[汽车](../Page/汽车.md "wikilink")、[卡车和](../Page/卡车.md "wikilink")[摩托车](../Page/摩托车.md "wikilink")，还有就是此起彼伏的汽车鸣笛声和各种[马拉畜力车混杂在一起构成的喧嚣和](../Page/馬車.md "wikilink")[噪声](../Page/噪声.md "wikilink")。

巴基斯坦最主要的自然灾害為频发的[地震](../Page/地震.md "wikilink")，尤其是北部和西部的偶发的强烈地震。另外，7月和8月[暴雨过后](../Page/暴雨.md "wikilink")，[印度河沿岸常發生](../Page/印度河.md "wikilink")[洪涝](../Page/洪涝.md "wikilink")[灾害](../Page/灾害.md "wikilink")。

在环境方面，当今面临的问题有：未经污水处理导致的水污染，工业废水污染和农业[水土流失](../Page/水土流失.md "wikilink")；有限的自然水资源；多数人口没有用上经处理的水；森林砍伐、[土壤侵蚀和](../Page/土壤侵蚀.md "wikilink")[土地荒漠化](../Page/土地荒漠化.md "wikilink")。

### 气候变化与温室效应

地处巴北部属于[印度河上游地区的](../Page/印度河.md "wikilink")[喜马拉雅山区](../Page/喜马拉雅山脉.md "wikilink")，研究人员收集到了树木年轮的[标本](../Page/标本.md "wikilink")，树龄可以追溯到前828年
\[2\]。通过对[树龄千年的树木分析表明](../Page/年轮.md "wikilink")，巴基斯坦北部山区最近一百年和过去的一千年相比，[湿度明显升高](../Page/湿度.md "wikilink")，很可能是由于人类诱发的[全球变暖所致](../Page/全球变暖.md "wikilink")。

## 国际多边合作

巴基斯坦参与环境保护、气候与大气相关的国际公约主要如下：

|                     [条约与](../Page/条约.md "wikilink")[协议](../Page/协议.md "wikilink")                      |
| :----------------------------------------------------------------------------------------------------: |
|                                  [海洋和特定领域](../Page/海洋.md "wikilink")                                   |
|                     [大气与](../Page/大气.md "wikilink")[气候](../Page/气候.md "wikilink")                      |
| [生物多样性](../Page/生物多样性.md "wikilink")、[自然环境与](../Page/自然环境.md "wikilink")[森林](../Page/森林.md "wikilink") |
|                                    [废物处理](../Page/废物.md "wikilink")                                    |
|                                     [河流](../Page/河流.md "wikilink")                                     |

## 相关列表

  - [巴基斯坦山峰列表](../Page/巴基斯坦山峰列表.md "wikilink") ([List of mountains in
    Pakistan](../Page/:en:List_of_mountains_in_Pakistan.md "wikilink"))
  - [巴基斯坦山脉](../Page/巴基斯坦山脉.md "wikilink") ([Mountain ranges of
    Pakistan](../Page/:en:Mountain_ranges_of_Pakistan.md "wikilink"))

## 参考文献

### 引用

### 来源

  -
{{-}}

[bn:পাকিস্তানের ভূগোল](../Page/bn:পাকিস্তানের_ভূগোল.md "wikilink")
[simple:Geography of
Pakistan](../Page/simple:Geography_of_Pakistan.md "wikilink")

[Pakistan](../Category/亚洲各国地理.md "wikilink")
[巴基斯坦地理](../Category/巴基斯坦地理.md "wikilink")
[Category:南亚地理](../Category/南亚地理.md "wikilink")

1.  [中国旅行社协会在线](http://www.cats.org.cn/BJST/mddjj/mddjj.htm) ，巴基斯坦目的地指南
2.  [Geotimes](http://www.geotimes.org/current/WebExtra042806.html)