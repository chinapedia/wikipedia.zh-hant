[Avenue_Apartments_on_West_Beijing_Road_Shanghai.JPG](https://zh.wikipedia.org/wiki/File:Avenue_Apartments_on_West_Beijing_Road_Shanghai.JPG "fig:Avenue_Apartments_on_West_Beijing_Road_Shanghai.JPG")
**铜仁路**（**Tongren
Road**）是中国[上海市](../Page/上海市.md "wikilink")[静安区的一条南北走向的道路](../Page/静安区.md "wikilink")，1914年—1943年间名为**哈同路**（**Hardoon
Road**），南起[福煦路](../Page/福煦路.md "wikilink")（延安中路），北至[爱文义路](../Page/爱文义路.md "wikilink")（北京西路），长659米，沿路为住宅区。铜仁路南京西路以南段多为商业区，如[静安嘉里中心等](../Page/静安嘉里中心.md "wikilink")。

## 历史

1914年，[上海公共租界工部局修筑此路](../Page/上海公共租界工部局.md "wikilink")，因其东侧为犹太富商[哈同的](../Page/哈同.md "wikilink")[爱俪园而命名为哈同路](../Page/爱俪园.md "wikilink")。1943年，[汪精卫政府接收](../Page/汪精卫政府.md "wikilink")[上海公共租界](../Page/上海公共租界.md "wikilink")，将其以[贵州省地名改称铜仁路](../Page/贵州省.md "wikilink")。

## 交汇道路

  - 爱文义路（[北京西路](../Page/北京西路_\(上海\).md "wikilink")）
  - 南阳路
  - [静安寺路](../Page/静安寺路.md "wikilink")（[南京西路](../Page/南京西路_（上海）.md "wikilink")）
  - 安南路（安义路）
  - [福煦路](../Page/福煦路.md "wikilink")（延安中路）

## 沿路近代建筑

  - 南京西路1418号（南京西路铜仁路口）——郭宅（今[上海市人民政府外事办公室](../Page/上海市人民政府.md "wikilink")）
  - 铜仁路240弄（[文德-{里}-](../Page/文德里_\(上海\).md "wikilink")）——1928年—1948年[上海教会聚会所](../Page/上海教会.md "wikilink")（已拆除）
  - 铜仁路257号——[史量才故居](../Page/史量才.md "wikilink")
  - 铜仁路278号——[皮裘公寓](../Page/皮裘公寓.md "wikilink")
  - 铜仁路280号——住宅
  - 铜仁路304-330号——[联华公寓](../Page/联华公寓.md "wikilink")（原名爱文义公寓）
  - 铜仁路333号——[吴同文住宅](../Page/吴同文住宅.md "wikilink")（绿房子）
  - 铜仁路121号——吉士烟厂

[Category:上海租界](../Category/上海租界.md "wikilink")
[H](../Category/上海道路.md "wikilink")
[Category:静安区](../Category/静安区.md "wikilink")