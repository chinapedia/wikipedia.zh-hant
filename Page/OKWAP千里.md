**OKWAP千里**是[英華達的子公司](../Page/英華達.md "wikilink")，成立於2000年，基地分佈於[台北](../Page/台北.md "wikilink")、[中國](../Page/中國.md "wikilink")[上海和](../Page/上海.md "wikilink")[南京](../Page/南京.md "wikilink")，負責各種軟體及硬體的技術研發及製造。2014年，開始有穿戴式電子設備以及網路家電的產品，專注於打造以健康為核心的社交網路平台。\[1\]\[2\]\[3\]
手機代言由[Twins](../Page/Twins.md "wikilink")、[S.H.E作代言](../Page/S.H.E.md "wikilink")。

## 參考文獻

## 外部連結

  - [OKWAP千里](http://www.okwap.com/)

[Category:台灣電子公司](../Category/台灣電子公司.md "wikilink")
[Category:行動電話製造商](../Category/行動電話製造商.md "wikilink")
[Category:總部位於新北市的工商業機構](../Category/總部位於新北市的工商業機構.md "wikilink")

1.  [《電腦設備》何代水：英華達年出貨看增5成](http://www.chinatimes.com/realtimenews/20140612004045-260410)
2.  [流行風／時尚手機 精品風格搶鏡-UDNnews](http://udn.com/news/story/7244/1829852)
3.  [appledaily-和碩
    傳與高通合作無人機](http://www.appledaily.com.tw/appledaily/article/finance/20160201/37043214/)