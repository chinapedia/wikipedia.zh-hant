下列是討論各種辦公室套裝軟體的一般功能與專業功能。如果要有更進一步的資訊請參考各軟體的條目。本表僅提供較廣泛籠統的說明。

## 一般信息

<table>
<thead>
<tr class="header">
<th><p>软件名称</p></th>
<th><p>開發者/公司</p></th>
<th><p>首次公開發佈日期</p></th>
<th><p>前身</p></th>
<th><p>最新穩定版本</p></th>
<th><p>作業系統</p></th>
<th><p>價格（<a href="../Page/美元.md" title="wikilink">美元</a>）</p></th>
<th><p><a href="../Page/開放文檔格式.md" title="wikilink">開放文檔格式支持</a></p></th>
<th><p><a href="../Page/軟件許可證.md" title="wikilink">軟件許可證</a></p></th>
<th><p>在线支持</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>| <a href="../Page/Ability_Office.md" title="wikilink">Ability Office</a></p></td>
<td><p><a href="../Page/Ability_Plus_Software.md" title="wikilink">Ability Plus Software</a></p></td>
<td><p>1985年</p></td>
<td><p>—</p></td>
<td><p>5.00</p></td>
<td><p><a href="../Page/Microsoft_Windows.md" title="wikilink">Windows</a></p></td>
<td><p>$59.99</p></td>
<td><p>計劃於Ability Office 6中支持</p></td>
<td><p><a href="../Page/專有軟件.md" title="wikilink">專有</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>| <a href="../Page/Apache_OpenOffice.md" title="wikilink">Apache OpenOffice</a></p></td>
<td><p><a href="../Page/Apache软件基金会.md" title="wikilink">Apache软件基金会</a></p></td>
<td><p>2012年5月</p></td>
<td><p><a href="../Page/OpenOffice.org.md" title="wikilink">OpenOffice.org</a></p></td>
<td></td>
<td><p><a href="../Page/Microsoft_Windows.md" title="wikilink">Windows</a>, <a href="../Page/macOS.md" title="wikilink">macOS</a>, <a href="../Page/Linux.md" title="wikilink">Linux</a>, <a href="../Page/BSD.md" title="wikilink">BSD</a>, <a href="../Page/Unix.md" title="wikilink">Unix</a>, <a href="../Page/Solaris.md" title="wikilink">Solaris</a>/<a href="../Page/Illumos.md" title="wikilink">Illumos</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/Apache许可证.md" title="wikilink">Apache第二版许可证</a></p></td>
<td><p>可使用插件与<a href="../Page/Google文档.md" title="wikilink">Google文档</a>、<a href="../Page/Zoho.md" title="wikilink">Zoho等同步</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Calligra_Suite.md" title="wikilink">Calligra Suite</a></p></td>
<td><p><a href="../Page/KDE.md" title="wikilink">KDE</a></p></td>
<td><p>2011年</p></td>
<td><p><a href="../Page/KOffice.md" title="wikilink">KOffice</a></p></td>
<td><p>3.0.1</p></td>
<td><p><a href="../Page/Microsoft_Windows.md" title="wikilink">Windows</a>, <a href="../Page/macOS.md" title="wikilink">macOS</a>, <a href="../Page/Linux.md" title="wikilink">Linux</a>, <a href="../Page/BSD.md" title="wikilink">BSD</a>, <a href="../Page/Solaris.md" title="wikilink">Solaris</a>/<a href="../Page/Illumos.md" title="wikilink">Illumos</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/LGPL.md" title="wikilink">LGPL及</a><a href="../Page/GNU_General_Public_License.md" title="wikilink">GPL</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>| <a href="../Page/Corel_Office.md" title="wikilink">Corel Office</a></p></td>
<td><p><a href="../Page/Corel.md" title="wikilink">Corel</a></p></td>
<td><p>1991年</p></td>
<td><p><a href="../Page/WordPerfect.md" title="wikilink">WordPerfect</a>（1982）</p></td>
<td><p>X3（or 13）</p></td>
<td><p><a href="../Page/Microsoft_Windows.md" title="wikilink">Windows</a></p></td>
<td><p>$300</p></td>
<td><p>{{?}}</p></td>
<td><p><a href="../Page/专有软件.md" title="wikilink">專有</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>| <a href="../Page/GNOME_Office.md" title="wikilink">GNOME Office</a></p></td>
<td><p><a href="../Page/GNOME_Foundation.md" title="wikilink">GNOME Foundation</a><br />
<a href="../Page/AbiSource.md" title="wikilink">AbiSource</a></p></td>
<td><p>?</p></td>
<td><p>—</p></td>
<td><p><a href="../Page/AbiWord.md" title="wikilink">2.4.5</a>/<a href="../Page/Gnumeric.md" title="wikilink">1.6.3</a>/<a href="../Page/GNOME-DB.md" title="wikilink">1.9 &amp; 0.62</a></p></td>
<td><p><a href="../Page/跨平臺.md" title="wikilink">跨平臺</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/GNU_General_Public_License.md" title="wikilink">GPL</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>| <a href="../Page/GobeProductive.md" title="wikilink">GobeProductive</a></p></td>
<td><p><a href="../Page/Gobe_Software.md" title="wikilink">Gobe Software</a></p></td>
<td><p>1998年</p></td>
<td><p>—</p></td>
<td><p>3.04 / 2.01</p></td>
<td><p><a href="../Page/Microsoft_Windows.md" title="wikilink">Windows及</a><a href="../Page/BeOS.md" title="wikilink">BeOS</a></p></td>
<td><p>$49.95</p></td>
<td></td>
<td><p><a href="../Page/專有軟件.md" title="wikilink">專有</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>| <a href="../Page/Google_Apps.md" title="wikilink">Google Apps</a><br />
<a href="../Page/Google_Docs.md" title="wikilink">Google Docs</a></p></td>
<td><p><a href="../Page/Google.md" title="wikilink">Google</a></p></td>
<td><p>2006年</p></td>
<td><p>—</p></td>
<td><p>—</p></td>
<td><p><a href="../Page/跨平臺.md" title="wikilink">跨平臺</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/專有軟件.md" title="wikilink">專有</a></p></td>
<td><p>在线办公</p></td>
</tr>
<tr class="even">
<td><p>| <a href="../Page/iWork.md" title="wikilink">iWork</a></p></td>
<td><p><a href="../Page/Apple.md" title="wikilink">Apple</a></p></td>
<td><p>2005年</p></td>
<td><p><a href="../Page/AppleWorks.md" title="wikilink">AppleWorks</a>[1]</p></td>
<td><p>'13</p></td>
<td><p><a href="../Page/Mac_OS_X.md" title="wikilink">Mac OS X</a></p></td>
<td><p>（随新Mac）</p></td>
<td></td>
<td><p><a href="../Page/專有軟件.md" title="wikilink">專有</a></p></td>
<td><p>支持<a href="../Page/iCloud.md" title="wikilink">iCloud</a></p></td>
</tr>
<tr class="odd">
<td><p>| <a href="../Page/Lotus_SmartSuite.md" title="wikilink">Lotus SmartSuite</a></p></td>
<td><p><a href="../Page/IBM.md" title="wikilink">IBM</a></p></td>
<td><p>1992年[2]</p></td>
<td><p>—</p></td>
<td><p>9.8</p></td>
<td><p><a href="../Page/Microsoft_Windows.md" title="wikilink">Windows及</a><a href="../Page/OS/2.md" title="wikilink">OS/2</a></p></td>
<td><p>$190</p></td>
<td></td>
<td><p><a href="../Page/專有軟件.md" title="wikilink">專有</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>| <a href="../Page/Lotus_Symphony.md" title="wikilink">Lotus Symphony</a></p></td>
<td><p><a href="../Page/IBM.md" title="wikilink">IBM</a></p></td>
<td><p>2007年</p></td>
<td><p><a href="../Page/OpenOffice.org.md" title="wikilink">OpenOffice.org</a></p></td>
<td><p>1.2</p></td>
<td><p><a href="../Page/跨平臺.md" title="wikilink">跨平臺</a></p></td>
<td></td>
<td></td>
<td><p>{{?}}</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>| <a href="../Page/LibreOffice.md" title="wikilink">LibreOffice</a></p></td>
<td><p><a href="../Page/The_Document_Foundation.md" title="wikilink">The Document Foundation</a></p></td>
<td><p>2011年1月</p></td>
<td><p><a href="../Page/OpenOffice.org.md" title="wikilink">OpenOffice.org</a></p></td>
<td></td>
<td><p><a href="../Page/Microsoft_Windows.md" title="wikilink">Windows</a>, <a href="../Page/macOS.md" title="wikilink">macOS</a>, <a href="../Page/Linux.md" title="wikilink">Linux</a>, <a href="../Page/BSD.md" title="wikilink">BSD</a>, <a href="../Page/Unix.md" title="wikilink">Unix</a>, <a href="../Page/Solaris.md" title="wikilink">Solaris</a>/<a href="../Page/Illumos.md" title="wikilink">Illumos</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/LGPL.md" title="wikilink">LGPL</a></p></td>
<td><p>可使用插件与<a href="../Page/Google文档.md" title="wikilink">Google文档</a>、<a href="../Page/Zoho.md" title="wikilink">Zoho等同步</a></p></td>
</tr>
<tr class="even">
<td><p>| <a href="../Page/Mariner_Software.md" title="wikilink">MarinerPak</a></p></td>
<td><p><a href="../Page/Mariner_Software.md" title="wikilink">Mariner Software</a></p></td>
<td><p>1996年</p></td>
<td><p>—</p></td>
<td><p>10.0</p></td>
<td><p><a href="../Page/Mac_OS.md" title="wikilink">Mac OS及</a><a href="../Page/Mac_OS_X.md" title="wikilink">Mac OS X</a></p></td>
<td><p>$79.95</p></td>
<td></td>
<td><p><a href="../Page/專有軟件.md" title="wikilink">專有</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>| <a href="../Page/Microsoft_Office.md" title="wikilink">Microsoft Office</a></p></td>
<td><p><a href="../Page/Microsoft.md" title="wikilink">Microsoft</a></p></td>
<td><p><a href="../Page/Microsoft_Office#Versions.md" title="wikilink">1990</a>（Office 1, for Macintosh） <a href="../Page/Microsoft_Office#Versions.md" title="wikilink">1992</a>（Office 3, first Windows version）</p></td>
<td><p><a href="../Page/Microsoft_Word.md" title="wikilink">Microsoft Word</a> <a href="../Page/Microsoft_Excel.md" title="wikilink">Microsoft Excel</a> <a href="../Page/PowerPoint.md" title="wikilink">PowerPoint</a></p></td>
<td><p>2013 (for Windows), 2011（for Mac OS X）</p></td>
<td><p><a href="../Page/Microsoft_Windows.md" title="wikilink">Windows及</a><a href="../Page/Apple_Macintosh.md" title="wikilink">Macintosh</a></p></td>
<td><p><a href="../Page/Microsoft_Office#Editions.md" title="wikilink">视乎地区和版本而定</a></p></td>
<td><p>（诸多免费插件，无原生支持）</p></td>
<td><p><a href="../Page/專有軟件.md" title="wikilink">專有</a></p></td>
<td><p><a href="../Page/Microsoft_Office_365.md" title="wikilink">Microsoft Office 365</a></p></td>
</tr>
<tr class="even">
<td><p>| <a href="../Page/NeoOffice.md" title="wikilink">NeoOffice</a></p></td>
<td><p>Planamesa Inc.</p></td>
<td><p>2005年6月2日</p></td>
<td><p>OpenOffice.org 1.1 for Mac OS X</p></td>
<td><p>2.2.2</p></td>
<td><p><a href="../Page/Mac_OS_X.md" title="wikilink">Mac OS X</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/GNU_General_Public_License.md" title="wikilink">GPL</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>| <a href="../Page/ShareOffice.md" title="wikilink">ShareOffice</a></p></td>
<td><p><a href="../Page/ShareMethods.md" title="wikilink">ShareMethods</a></p></td>
<td><p><a href="../Page/2007年5月.md" title="wikilink">2007年5月</a></p></td>
<td><p>—</p></td>
<td></td>
<td><p><a href="../Page/跨平臺.md" title="wikilink">跨平臺</a></p></td>
<td><p>$15 per user per month</p></td>
<td></td>
<td><p><a href="../Page/專有軟件.md" title="wikilink">專有</a></p></td>
<td><p>在线办公</p></td>
</tr>
<tr class="even">
<td><p>| <a href="../Page/SoftMaker_Office.md" title="wikilink">SoftMaker Office</a></p></td>
<td><p><a href="../Page/SoftMaker.md" title="wikilink">SoftMaker</a></p></td>
<td><p>1989年</p></td>
<td><p>—</p></td>
<td><p>2008 (Windows),<br />
2006（Linux, FreeBSD, PocketPC）</p></td>
<td><p><a href="../Page/FreeBSD.md" title="wikilink">FreeBSD</a>, <a href="../Page/Linux.md" title="wikilink">Linux</a>, <a href="../Page/PocketPC.md" title="wikilink">PocketPC及</a><a href="../Page/Microsoft_Windows.md" title="wikilink">Windows</a></p></td>
<td><p>$69.95</p></td>
<td><p>TextMaker</p></td>
<td><p><a href="../Page/专有软件.md" title="wikilink">專有</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>| <a href="../Page/ThinkFree_Office.md" title="wikilink">ThinkFree Office</a></p></td>
<td><p><a href="../Page/Haansun.md" title="wikilink">Haansun</a></p></td>
<td><p>1994年</p></td>
<td><p>—</p></td>
<td><p>3.5</p></td>
<td><p><a href="../Page/跨平臺.md" title="wikilink">跨平臺</a></p></td>
<td><p>在线版免費，线下版$49.95</p></td>
<td></td>
<td><p><a href="../Page/專有軟件.md" title="wikilink">專有</a></p></td>
<td><p>在线办公、同步</p></td>
</tr>
<tr class="even">
<td><p>| <a href="../Page/WPS_Office.md" title="wikilink">WPS Office</a></p></td>
<td><p><a href="../Page/金山軟件.md" title="wikilink">金山軟件</a></p></td>
<td><p>1988年</p></td>
<td><p>—</p></td>
<td><p>2016</p></td>
<td><p><a href="../Page/跨平臺.md" title="wikilink">跨平臺</a></p></td>
<td><p>视乎版本和购买量而定（简体中文个人版与校园版免费）</p></td>
<td></td>
<td><p><a href="../Page/專有軟件.md" title="wikilink">專有</a></p></td>
<td><p>仅模板和存储</p></td>
</tr>
<tr class="odd">
<td><p>| <a href="../Page/永中Office.md" title="wikilink">永中Office</a></p></td>
<td><p><a href="../Page/永中科技.md" title="wikilink">永中科技</a></p></td>
<td><p>2001年</p></td>
<td><p>—</p></td>
<td><p>2010</p></td>
<td><p><a href="../Page/Microsoft_Windows.md" title="wikilink">Windows及</a><a href="../Page/Linux.md" title="wikilink">Linux</a></p></td>
<td><p>视乎版本和购买量而定</p></td>
<td></td>
<td><p><a href="../Page/专有软件.md" title="wikilink">專有</a></p></td>
<td><p>在线办公</p></td>
</tr>
<tr class="even">
<td><p>| <a href="../Page/ZCubes.md" title="wikilink">ZCubes</a></p></td>
<td><p><a href="../Page/ZCubes_Inc..md" title="wikilink">ZCubes Inc.</a></p></td>
<td><p>2006年</p></td>
<td><p>—</p></td>
<td><p>2007</p></td>
<td><p><a href="../Page/跨平臺.md" title="wikilink">跨平臺</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/專有軟件.md" title="wikilink">專有</a></p></td>
<td><p>在线办公</p></td>
</tr>
<tr class="odd">
<td><p>| <a href="../Page/Zoho_Office_Suite.md" title="wikilink">Zoho Office Suite</a></p></td>
<td><p><a href="../Page/AdventNet.md" title="wikilink">AdventNet</a></p></td>
<td><p>?</p></td>
<td><p>—</p></td>
<td><p>1.0</p></td>
<td><p><a href="../Page/跨平臺.md" title="wikilink">跨平臺</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/專有軟件.md" title="wikilink">專有</a></p></td>
<td><p>在线办公、和<a href="../Page/Microsoft_Office.md" title="wikilink">Microsoft Office同步</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>開發者/公司</p></td>
<td><p>首次公開發佈日期</p></td>
<td><p>基於</p></td>
<td><p>最新穩定版本</p></td>
<td><p>作業系統</p></td>
<td><p>價格（<a href="../Page/美元.md" title="wikilink">美元</a>）</p></td>
<td><p><a href="../Page/開放文檔格式.md" title="wikilink">開放文檔格式支持</a></p></td>
<td><p><a href="../Page/軟件許可證.md" title="wikilink">軟件許可證</a></p></td>
<td><p>在线支持</p></td>
</tr>
</tbody>
</table>

## 作業系統的支持

指办公室套装软体在无[虛擬機器的](../Page/虛擬機器.md "wikilink")[操作系统中运行的情况](../Page/操作系统.md "wikilink")；在给定的套装软体中，有以下六種可能：

  - *否* - 表示不存在，或不曾釋出該版本。
  - *部分* - 表示該辦公室套裝軟體可以運作，不過，與其他作業系統下運作相比，缺少重要的功能，可能還在研發中。
  - *Beta* - 又稱測試版，具備所有的功能，並且釋出，可能還在研發中（如：穩定性不夠）。
  - *是* - 表示已經正式釋出，全功能的穩定版。
  - *Dropped* indicates that while the office suite works, new versions
    are no longer being released for the indicated OS; the number in
    parentheses is the last known stable version which was officially
    released for that OS.
  - *Included* indicates that the office suite comes pre-packaged as
    part of or has been integrated into the operating system.

請注意，以下清單並未將所有的辦公室套裝軟體都包含在內，僅限於現今常見的軟體而已。

<table>
<thead>
<tr class="header">
<th><p>辦公室套裝</p></th>
<th><p><a href="../Page/Microsoft_Windows.md" title="wikilink">Windows</a></p></th>
<th><p><a href="../Page/Mac_OS_X.md" title="wikilink">Mac OS X</a></p></th>
<th><p><a href="../Page/Linux.md" title="wikilink">Linux</a></p></th>
<th><p><a href="../Page/Berkeley_Software_Distribution.md" title="wikilink">BSD</a></p></th>
<th><p><a href="../Page/Unix.md" title="wikilink">Unix</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>| <a href="../Page/Ability_Office.md" title="wikilink">Ability Office</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>| <a href="../Page/Apache_OpenOffice.md" title="wikilink">Apache OpenOffice</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>| <a href="../Page/Calligra_Suite.md" title="wikilink">Calligra Suite</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>| <a href="../Page/GNOME_Office.md" title="wikilink">GNOME Office</a></p></td>
<td><p>[3]</p></td>
<td><p>[4]</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>| <a href="../Page/Gobe_Software.md" title="wikilink">Gobe Productive</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>| <a href="../Page/Google_Apps.md" title="wikilink">Google Apps</a></p></td>
<td><p>是（完全線上）</p></td>
<td><p>是（完全線上）</p></td>
<td><p>是（完全線上）</p></td>
<td><p>是（完全線上）</p></td>
<td><p>是（完全線上）</p></td>
</tr>
<tr class="odd">
<td><p>| <a href="../Page/iWork.md" title="wikilink">iWork</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>| <a href="../Page/Lotus_SmartSuite.md" title="wikilink">Lotus SmartSuite</a></p></td>
<td></td>
<td></td>
<td><p>(with <a href="../Page/Wine_software.md" title="wikilink">Wine</a>)</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>| <a href="../Page/Lotus_Symphony.md" title="wikilink">Lotus Symphony</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>| <a href="../Page/Mariner_Software.md" title="wikilink">MarinerPak</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>| <a href="../Page/Microsoft_Office.md" title="wikilink">Microsoft Office</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>| <a href="../Page/NeoOffice.md" title="wikilink">NeoOffice</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>| <a href="../Page/LibreOffice.md" title="wikilink">LibreOffice</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>| <a href="../Page/ShareOffice.md" title="wikilink">ShareOffice</a></p></td>
<td><p>是（完全線上）</p></td>
<td><p>是（完全線上）</p></td>
<td><p>是（完全線上）</p></td>
<td><p>是（完全線上）</p></td>
<td><p>是（完全線上）</p></td>
</tr>
<tr class="odd">
<td><p>| <a href="../Page/SoftMaker_Office.md" title="wikilink">SoftMaker Office</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>| <a href="../Page/StarOffice.md" title="wikilink">StarOffice</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>| <a href="../Page/ThinkFree_Office.md" title="wikilink">ThinkFree Office</a></p></td>
<td><p>是</p></td>
<td><p>是</p></td>
<td><p>是</p></td>
<td><p>線上</p></td>
<td><p>線上</p></td>
</tr>
<tr class="even">
<td><p>| <a href="../Page/WordPerfect_Office.md" title="wikilink">WordPerfect Office</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>| <a href="../Page/WPS_Office.md" title="wikilink">WPS Office</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>| <a href="../Page/永中Office.md" title="wikilink">永中Office</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>| <a href="../Page/ZCubes.md" title="wikilink">ZCubes</a></p></td>
<td><p>是（完全線上）</p></td>
<td><p>是（完全線上）</p></td>
<td><p>是（完全線上）</p></td>
<td><p>是（完全線上）</p></td>
<td><p>是（完全線上）</p></td>
</tr>
<tr class="even">
<td><p>| <a href="../Page/Zoho.md" title="wikilink">Zoho</a></p></td>
<td><p>是（完全線上）</p></td>
<td><p>是（完全線上）</p></td>
<td><p>是（完全線上）</p></td>
<td><p>是（完全線上）</p></td>
<td><p>是（完全線上）</p></td>
</tr>
<tr class="odd">
<td><p>辦公室套裝</p></td>
<td><p><a href="../Page/Microsoft_Windows.md" title="wikilink">Windows</a></p></td>
<td><p><a href="../Page/Mac_OS_X.md" title="wikilink">Mac OS X</a></p></td>
<td><p><a href="../Page/Linux.md" title="wikilink">Linux</a></p></td>
<td><p><a href="../Page/BSD.md" title="wikilink">BSD</a></p></td>
<td><p><a href="../Page/Unix.md" title="wikilink">Unix</a></p></td>
</tr>
</tbody>
</table>

## 組件

<table>
<thead>
<tr class="header">
<th></th>
<th><p><a href="../Page/文書處理.md" title="wikilink">文書處理</a></p></th>
<th><p><a href="../Page/表格.md" title="wikilink">表格</a></p></th>
<th><p><a href="../Page/簡報.md" title="wikilink">簡報</a></p></th>
<th><p><a href="../Page/電郵.md" title="wikilink">電郵</a></p></th>
<th><p><a href="../Page/流程圖.md" title="wikilink">流程圖</a></p></th>
<th><p><a href="../Page/位图编辑.md" title="wikilink">位图编辑</a></p></th>
<th><p><a href="../Page/矢量图编辑.md" title="wikilink">矢量图编辑</a></p></th>
<th><p><a href="../Page/公式.md" title="wikilink">公式</a></p></th>
<th><p><a href="../Page/数据管理.md" title="wikilink">数据管理</a></p></th>
<th><p><a href="../Page/项目管理.md" title="wikilink">项目管理</a></p></th>
<th><p><a href="../Page/桌面出版.md" title="wikilink">桌面出版</a></p></th>
<th><p><a href="../Page/網頁設計.md" title="wikilink">網頁設計</a></p></th>
<th><p><a href="../Page/共同作業.md" title="wikilink">共同作業</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>| <a href="../Page/Ability_Office.md" title="wikilink">Ability Office</a></p></td>
<td><p><a href="../Page/Ability_Write.md" title="wikilink">Ability Write</a></p></td>
<td><p><a href="../Page/Ability_Spreadsheet.md" title="wikilink">Ability Spreadsheet</a></p></td>
<td><p><a href="../Page/Ability_Presentation.md" title="wikilink">Ability Presentation</a></p></td>
<td></td>
<td><p><a href="../Page/Ability_Draw.md" title="wikilink">Ability Draw</a></p></td>
<td><p><a href="../Page/Ability_Photopaint.md" title="wikilink">Ability Photopaint</a></p></td>
<td><p><a href="../Page/Ability_Draw.md" title="wikilink">Ability Draw</a></p></td>
<td></td>
<td><p><a href="../Page/Ability_Database.md" title="wikilink">Ability Database</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>| <a href="../Page/Apache_OpenOffice.md" title="wikilink">Apache OpenOffice</a></p></td>
<td><p><a href="../Page/Apache_OpenOffice.md" title="wikilink">OpenOffice Writer</a></p></td>
<td><p><a href="../Page/Apache_OpenOffice.md" title="wikilink">OpenOffice Calc</a></p></td>
<td><p><a href="../Page/Apache_OpenOffice.md" title="wikilink">OpenOffice Impress</a></p></td>
<td></td>
<td><p><a href="../Page/Apache_OpenOffice.md" title="wikilink">OpenOffice Draw</a></p></td>
<td></td>
<td><p><a href="../Page/Apache_OpenOffice.md" title="wikilink">OpenOffice Draw</a></p></td>
<td><p><a href="../Page/Apache_OpenOffice.md" title="wikilink">OpenOffice Math</a></p></td>
<td><p><a href="../Page/Apache_OpenOffice.md" title="wikilink">OpenOffice Base</a></p></td>
<td></td>
<td><p><a href="../Page/Apache_OpenOffice.md" title="wikilink">OpenOffice Draw</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Calligra_Suite.md" title="wikilink">Calligra Suite</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="../Page/Kontact.md" title="wikilink">Kontact</a> </p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>| <a href="../Page/Corel_Office.md" title="wikilink">Corel Office</a></p></td>
<td><p><a href="../Page/WordPerfect.md" title="wikilink">WordPerfect</a></p></td>
<td><p><a href="../Page/Quattro_Pro.md" title="wikilink">Quattro Pro</a></p></td>
<td><p><a href="../Page/Corel_Presentations.md" title="wikilink">Corel Presentations</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="../Page/CorelDRAW.md" title="wikilink">CorelDRAW</a></p></td>
<td></td>
<td><p><a href="../Page/Corel_Paradox.md" title="wikilink">Paradox</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>| <a href="../Page/GNOME_Office.md" title="wikilink">GNOME Office</a></p></td>
<td><p><a href="../Page/Abiword.md" title="wikilink">Abiword</a></p></td>
<td><p><a href="../Page/Gnumeric.md" title="wikilink">Gnumeric</a></p></td>
<td></td>
<td><p><a href="../Page/Evolution_(Software).md" title="wikilink">Evolution</a></p></td>
<td><p><a href="../Page/Dia.md" title="wikilink">Dia</a></p></td>
<td><p><a href="../Page/GIMP.md" title="wikilink">GIMP</a></p></td>
<td><p><a href="../Page/Inkscape.md" title="wikilink">Inkscape</a></p></td>
<td></td>
<td><p><a href="../Page/Mergeant.md" title="wikilink">Mergeant</a></p></td>
<td><p><a href="../Page/Planner_(project_management).md" title="wikilink">Planner</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>| <a href="../Page/GobeProductive.md" title="wikilink">GobeProductive</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>| <a href="../Page/Google_Docs.md" title="wikilink">Google Docs</a></p></td>
<td><p>Google文件</p></td>
<td><p>Google試算表</p></td>
<td><p>Google簡報</p></td>
<td></td>
<td></td>
<td></td>
<td><p>Google繪圖</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>| <a href="../Page/iWork.md" title="wikilink">iWork</a></p></td>
<td><p><a href="../Page/Pages.md" title="wikilink">Pages</a></p></td>
<td><p><a href="../Page/Numbers.md" title="wikilink">Numbers</a></p></td>
<td><p><a href="../Page/Keynote.md" title="wikilink">Keynote</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>| <a href="../Page/Lotus_SmartSuite.md" title="wikilink">Lotus SmartSuite</a></p></td>
<td><p><a href="../Page/Lotus_Word_Pro.md" title="wikilink">Lotus Word Pro</a></p></td>
<td><p><a href="../Page/Lotus_1-2-3.md" title="wikilink">Lotus 1-2-3</a></p></td>
<td><p><a href="../Page/Lotus_Freelance_Graphics.md" title="wikilink">Lotus Freelance Graphics</a></p></td>
<td></td>
<td><p><a href="../Page/Lotus_Freelance_Graphics.md" title="wikilink">Lotus Freelance Graphics</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/Equation_Editor.md" title="wikilink">Equation Editor</a></p></td>
<td><p><a href="../Page/Lotus_Approach.md" title="wikilink">Lotus Approach</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>| <a href="../Page/Microsoft_Office.md" title="wikilink">Microsoft Office</a></p></td>
<td><p><a href="../Page/Microsoft_Word.md" title="wikilink">Microsoft Word</a></p></td>
<td><p><a href="../Page/Microsoft_Excel.md" title="wikilink">Microsoft Excel</a></p></td>
<td><p><a href="../Page/Microsoft_PowerPoint.md" title="wikilink">Microsoft PowerPoint</a></p></td>
<td><p><a href="../Page/Microsoft_Outlook.md" title="wikilink">Microsoft Outlook</a></p></td>
<td><p><a href="../Page/Microsoft_Visio.md" title="wikilink">Microsoft Visio</a></p></td>
<td></td>
<td><p><a href="../Page/Microsoft_Visio.md" title="wikilink">Microsoft Visio</a></p></td>
<td><p><a href="../Page/Equation_Editor.md" title="wikilink">Equation Editor</a></p></td>
<td><p><a href="../Page/Microsoft_Access.md" title="wikilink">Microsoft Access</a></p></td>
<td><p><a href="../Page/Microsoft_Project.md" title="wikilink">Microsoft Project</a></p></td>
<td><p><a href="../Page/Microsoft_Publisher.md" title="wikilink">Microsoft Publisher</a></p></td>
<td><p><a href="../Page/Microsoft_SharePoint_Designer.md" title="wikilink">Microsoft SharePoint Designer</a></p></td>
<td><p><a href="../Page/Microsoft_Office_Groove.md" title="wikilink">Microsoft Office Groove</a></p></td>
</tr>
<tr class="odd">
<td><p>| <a href="../Page/NeoOffice.md" title="wikilink">NeoOffice</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>| <a href="../Page/LibreOffice.md" title="wikilink">LibreOffice</a></p></td>
<td><p><a href="../Page/LibreOffice_Writer.md" title="wikilink">LibreOffice Writer</a></p></td>
<td><p><a href="../Page/LibreOffice_Calc.md" title="wikilink">LibreOffice Calc</a></p></td>
<td><p><a href="../Page/LibreOffice_Impress.md" title="wikilink">LibreOffice Impress</a></p></td>
<td></td>
<td><p><a href="../Page/LibreOffice_Draw.md" title="wikilink">LibreOffice Draw</a></p></td>
<td></td>
<td><p><a href="../Page/LibreOffice_Draw.md" title="wikilink">LibreOffice Draw</a></p></td>
<td><p><a href="../Page/LibreOffice_Math.md" title="wikilink">LibreOffice Math</a></p></td>
<td><p><a href="../Page/LibreOffice_Base.md" title="wikilink">LibreOffice Base</a></p></td>
<td></td>
<td><p><a href="../Page/LibreOffice_Draw.md" title="wikilink">LibreOffice Draw</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>| <a href="../Page/SoftMaker_Office.md" title="wikilink">SoftMaker Office</a></p></td>
<td><p><a href="../Page/Textmaker.md" title="wikilink">Textmaker</a></p></td>
<td><p>PlanMaker</p></td>
<td><p>Presentations</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>是</p></td>
<td><p>DataMaker</p></td>
<td></td>
<td><p>是</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>| <a href="../Page/StarOffice.md" title="wikilink">StarOffice</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>| <a href="../Page/WPS_Office.md" title="wikilink">WPS Office</a></p></td>
<td><p>文件</p></td>
<td><p>表格</p></td>
<td><p>演示</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="../Page/Equation_Editor.md" title="wikilink">Equation Editor</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="../Page/轻办公.md" title="wikilink">轻办公</a></p></td>
</tr>
<tr class="even">
<td><p>| <a href="../Page/永中Office.md" title="wikilink">永中Office</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>| <a href="../Page/Zoho.md" title="wikilink">Zoho</a></p></td>
<td><p>Zoho Writer</p></td>
<td><p>Zoho Sheet</p></td>
<td><p>Zoho Show</p></td>
<td><p>Zoho Mail</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>Zoho Reports</p></td>
<td><p>Zoho Projects</p></td>
<td></td>
<td></td>
<td><p>Zoho Wiki</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/文書處理.md" title="wikilink">文書處理</a></p></td>
<td><p><a href="../Page/表格.md" title="wikilink">表格</a></p></td>
<td><p><a href="../Page/簡報.md" title="wikilink">簡報</a></p></td>
<td><p><a href="../Page/電郵.md" title="wikilink">電郵</a></p></td>
<td><p><a href="../Page/流程圖.md" title="wikilink">流程圖</a></p></td>
<td><p><a href="../Page/位图编辑.md" title="wikilink">位图编辑</a></p></td>
<td><p><a href="../Page/矢量图编辑.md" title="wikilink">矢量图编辑</a></p></td>
<td><p><a href="../Page/公式.md" title="wikilink">公式</a></p></td>
<td><p><a href="../Page/数据管理.md" title="wikilink">数据管理</a></p></td>
<td><p><a href="../Page/项目管理.md" title="wikilink">项目管理</a></p></td>
<td><p><a href="../Page/桌面出版.md" title="wikilink">桌面出版</a></p></td>
<td><p><a href="../Page/網頁設計.md" title="wikilink">網頁設計</a></p></td>
<td><p><a href="../Page/共同作業.md" title="wikilink">共同作業</a></p></td>
</tr>
</tbody>
</table>

## 註解

Lotus
SmartSuite的釋出時間來自http://www.findarticles.com/p/articles/mi_m3563/is_n7_v9/ai_14346979

[Kexi](../Page/Kexi.md "wikilink") is available for Windows as a
commercial product. A [Cygwin](../Page/Cygwin.md "wikilink") port of KDE
[1](http://kde-cygwin.sourceforge.net/) (which includes KOffice) is in
progress although the status of that port is poor. KDE4 may see a new
port to windows.

只有[Abiword](../Page/Abiword.md "wikilink")、[Gnumeric](../Page/Gnumeric.md "wikilink")、[GIMP和](../Page/GIMP.md "wikilink")[Inkscape能支援MS](../Page/Inkscape.md "wikilink")
Windows。

只有Abiword是原生應用程式（native application）。

未包含于软件套装中，而是由桌面环境提供

不支持odp。

## 請參看

  - [可支援開放文件軟件列表](../Page/可支援開放文件軟件列表.md "wikilink")
  - [文字處理器比較](../Page/文字處理器比較.md "wikilink")

## 參考文獻

[Category:软件比较](../Category/软件比较.md "wikilink")
[\*](../Category/办公室自动化软件.md "wikilink")

1.
2.  Release date of Lotus SmartSuite from
    <http://www.findarticles.com/p/articles/mi_m3563/is_n7_v9/ai_14346979>
3.  Only [Abiword](../Page/Abiword.md "wikilink"),
    [Gnumeric](../Page/Gnumeric.md "wikilink"),
    [GIMP](../Page/GIMP.md "wikilink") and
    [Inkscape](../Page/Inkscape.md "wikilink") are available for MS
    Windows.
4.  Only Abiword is available as a native application. Using
    [X11](../Page/X11.md "wikilink") one can run
    [GIMP](../Page/GIMP.md "wikilink") and
    [Inkscape](../Page/Inkscape.md "wikilink"). With
    [X11](../Page/X11.md "wikilink") and
    [DarwinPorts](../Page/DarwinPorts.md "wikilink") - also Gnumeric.