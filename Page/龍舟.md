[Dragon_boat_-_Cantonese.JPG](https://zh.wikipedia.org/wiki/File:Dragon_boat_-_Cantonese.JPG "fig:Dragon_boat_-_Cantonese.JPG")的龙舟\]\]
[Eastern_District_Dragon_Boat_Race_-_2008-06-01_09h52m48s_SN201360.jpg](https://zh.wikipedia.org/wiki/File:Eastern_District_Dragon_Boat_Race_-_2008-06-01_09h52m48s_SN201360.jpg "fig:Eastern_District_Dragon_Boat_Race_-_2008-06-01_09h52m48s_SN201360.jpg")）。\]\]
**龍舟**（或稱**龍船**）是指[漢字文化圈的](../Page/漢字文化圈.md "wikilink")[龍形舟](../Page/龍.md "wikilink")，也是中國和琉球[端午節競賽活動時使用的](../Page/端午節.md "wikilink")[人力船隻](../Page/人力運輸.md "wikilink")。
(一)大型龍舟：每隊舵手1人、槳手20人、鑼或鼓手1人（指揮）、奪標手1人、預備員4人。

(二)小型龍舟：每隊舵手1人、槳手12人、鑼或鼓手1人（指揮）、奪標手1人、預備員4人。

龍舟不單是傳統[中華文化的一部分](../Page/中華文化.md "wikilink")，從其引申發展出來的龍舟競賽現已發展為[體育活動](../Page/體育.md "wikilink")。由於[賽龍舟被推廣成為體育項目](../Page/賽龍舟.md "wikilink")，因此龍舟同時成為此體育項目的主要用品。

## 沿革

一般人可能認為龍舟是隨[端午節而出現](../Page/端午節.md "wikilink")，但其實許多端午節習俗早在[楚國前已流傳](../Page/楚國.md "wikilink")。其中的例證有《[穆天子传](../Page/穆天子传.md "wikilink")》曾載：“曾有周天子（指[周穆王](../Page/周穆王.md "wikilink")）乘龍舟浮-{于}-大沼。”\[1\]而《[述異記](../Page/述異記_\(祖沖之\).md "wikilink")》則敘述，[吳王](../Page/吳國.md "wikilink")[夫差作天池](../Page/夫差.md "wikilink")，池中有龍舟，日與[西施戲水](../Page/西施.md "wikilink")。由此看來，龍舟的出現可能是比楚國時期更早。

這推斷與[考古學的發現一致](../Page/考古學.md "wikilink")，其中一個來自[春秋战国时期的](../Page/春秋战国.md "wikilink")[青铜](../Page/青铜.md "wikilink")[钺](../Page/钺.md "wikilink")，便刻有龙舟競渡图案。有學者更指龍舟演变自[百越的](../Page/百越.md "wikilink")[独木舟](../Page/独木舟.md "wikilink")，百越独木舟是以[蛟龙为](../Page/蛟龙.md "wikilink")[图腾](../Page/图腾.md "wikilink")。對於龍舟的起源和原因，今天仍在考究。無論如何，既有龍舟競渡，龍舟肯定已存在。按[史料記錄的龍舟競渡之習俗早已盛行於](../Page/史料.md "wikilink")[吳越和楚國](../Page/吴越地区.md "wikilink")，及至[明朝之後已盛行於宮廷和民間各地](../Page/明朝.md "wikilink")，此外[隋炀帝也曾造过](../Page/隋炀帝.md "wikilink")[龙舟](../Page/龙舟_\(隋\).md "wikilink")。

## 種類

[Haryusen.jpg](https://zh.wikipedia.org/wiki/File:Haryusen.jpg "fig:Haryusen.jpg")時期的爬龍舟\]\]
[Naha_Hari.jpg](https://zh.wikipedia.org/wiki/File:Naha_Hari.jpg "fig:Naha_Hari.jpg")
[Itoman_Hare.jpg](https://zh.wikipedia.org/wiki/File:Itoman_Hare.jpg "fig:Itoman_Hare.jpg")

### 廣東觀賞龍舟

整個粵港澳地區用的觀賞龍舟船身较长略宽，龙头与尾部高翘，龙头可以上下左右摇动。 [廣州](../Page/廣州.md "wikilink")
龍舟競渡在廣州至少有上成千年的歷史。每年端午節，都要舉辦端午賽龍舟的活動。古時廣州是「舟大人多」，屈大均當年形容：「舟龍長十餘丈，高六七尺，龍鬚去水二尺，龍額與項坐六七人。」
廣東劃龍舟又可叫扒龍船，叫做「趁景」。兄弟村、老表村成百條龍舟一起相聚聯絡感情，不論名次，從農曆五月初一到五月二十，幾乎每天有「景」——初一珠村景，初二海珠大塘景，初三車陂景，初四廟頭景，初五石牌、獵德景……這廣州已傳承多年的習俗。其實整個珠三角地區都有相似的趁景活動。另外廣州市和香港都分別有國際龍舟邀請賽。
在初四晚的龍舟大賽前會有龍船飯招呼橈手。龍舟起水打整之後，初三初四到各地遊大比試；並去拜訪關係較好的鄰村，主家村必以禮相迎，放鞭炮、敲鑼打鼓，中午用「龍橋餅」（類似核桃酥）招呼人客，晚餐就用燒豬肉、雞等宴客，互祝大賽成功。初五大賽後，初七初八就去市郊黃竹岐去拜龍母，五月初八是龍母誕，故此廣州俗諺云：「正月生菜會，五月龍母誕」。

### 苗族子母舟

龙舟由三只[独木舟組合而成的](../Page/独木舟.md "wikilink")，中间较长的一只称**母舟**；两边的**子舟**比母舟稍短。龙舟約長20米、寬1米。船身由完整[杉木挖空而成](../Page/杉木.md "wikilink")；龍頭至頸則有[水柳木雕刻](../Page/水柳木.md "wikilink")。苗族龍舟常用於贵州苗族的龙舟节，不是比速度而是比禮物的多寡。

### 越南龍舟

[越南](../Page/越南.md "wikilink")[廣寧省](../Page/廣寧省.md "wikilink")[拜子龍灣的](../Page/拜子龍灣.md "wikilink")[關蘭島](../Page/關蘭島.md "wikilink")（越南語：Đảo
Quan
Lạn）在每年農曆[六月十八舉行](../Page/六月十八.md "wikilink")[關蘭島龍舟競渡](../Page/關蘭島龍舟競渡.md "wikilink")，以紀念[陳朝大將](../Page/越南陳朝.md "wikilink")[陳慶餘抵禦外侮](../Page/陳慶餘.md "wikilink")、保家衛國的功績。

此外，越南傳統歌唱藝術[官賀傳統上是站在龍舟上唱歌](../Page/官賀.md "wikilink")。

### 琉球龍舟

琉球爬龍舟已有六百多年歷史，相傳[南山王](../Page/南山王國.md "wikilink")[汪應祖曾留學](../Page/汪應祖.md "wikilink")[明朝的](../Page/明朝.md "wikilink")[南京](../Page/南京.md "wikilink")，歸國後在[豐見城舉行琉球歷史上的第一場龍舟賽](../Page/豐見城.md "wikilink")（但汪應祖留學南京一事不見於史料記載，另有[閩人三十六姓將龍舟傳入琉球的說法](../Page/閩人三十六姓.md "wikilink")）。

在[琉球國都城](../Page/琉球國.md "wikilink")[首里和附近地區](../Page/首里.md "wikilink")，每年[端午節都會有龍舟競渡](../Page/端午節.md "wikilink")，首里現時歸入[日本](../Page/日本.md "wikilink")[沖繩縣](../Page/沖繩縣.md "wikilink")[那霸市](../Page/那霸市.md "wikilink")，那霸市隨著日本改用公曆，原來在農曆五月初五舉行的龍舟競渡也改為新曆的5月3日至5日舉行；鄰近的[糸滿市仍然保持以農曆計算一些傳統節日](../Page/糸滿市.md "wikilink")，在端午節前夕（[五月初四](../Page/五月初四.md "wikilink")）和[中秋節也會舉行龍舟競渡](../Page/中秋節.md "wikilink")。而端午節前夕[豐見城市也會在](../Page/豐見城市.md "wikilink")[饒波川舉行龍舟競渡活動](../Page/饒波川.md "wikilink")。龍舟頭部有棱角，舌長、往上翹起。

### 日本龍舟

由[江戶時代初開始](../Page/江戶時代.md "wikilink")，划龍舟的習俗從中國傳到日本，自此[長崎每年](../Page/长崎县.md "wikilink")[夏季都會舉行龍舟競渡](../Page/夏季.md "wikilink")。

### 台灣龍舟

[端午節是台湾的重要民俗節日](../Page/台灣端午節.md "wikilink")，近年來文化發展，[台灣各鄉鎮都自行舉辦龍舟競賽](../Page/台灣.md "wikilink")。台灣的龍舟以木造及[玻璃纖維為主](../Page/玻璃纖維.md "wikilink")，並且被視為藝術品，較有名的是[基隆的](../Page/基隆市.md "wikilink")[劉清正所製的木造龍舟和](../Page/劉清正.md "wikilink")[屏東的](../Page/屏東縣.md "wikilink")[洪全瑞所製作的玻璃纖維龍舟及木造龍舟](../Page/洪全瑞.md "wikilink")，有的鄉鎮使用大型及小型龍舟，以應付不同隊伍的大小，一般來說，木造龍舟較重，而玻璃纖維龍舟較為輕，由於沒有統一的規格，台灣各鄉鎮的龍舟規格也不一。

木造龍舟與玻璃纖維之龍舟各有不同之特點，木造龍舟可直接製作不同尺寸，玻璃纖維則需要開模才能製作，但若已有模具製作時間會較短，近年來台灣的木造龍舟及玻璃纖維龍舟皆只有洪全瑞同時有進行製作。

台灣玻璃纖維龍舟，以[洪全瑞所製為例](../Page/洪全瑞.md "wikilink")，龍頭、龍身、龍尾皆為玻璃纖維所製，大型龍舟不含頭尾長約11.3公尺，寬1.5公尺，可乘坐23人，小型龍舟不含頭尾長約9公尺，寬1.4公尺左右，可乘坐15人。

木造龍舟則依需求製作，尺寸、價格皆依照不同規格而有所不同。

### 朝鮮龍舟

一般為國王專用，也常見於佛教繪畫，佛教繪畫中的為[般若龍船](../Page/般若龍船.md "wikilink")。

### 旱龙舟

[旱龙舟](../Page/旱龙舟.md "wikilink")（或稱[陆上龙舟](../Page/陆上龙舟.md "wikilink")）用竹子或木製成，再以五色[绫缎为鳞甲](../Page/绫缎.md "wikilink")。船身比較大，因為龙脊设层楼、飞阁以及两旁有屈原、水神和桡手的[繪畫和](../Page/繪畫.md "wikilink")[彩扎](../Page/彩扎.md "wikilink")。旱龍舟的比賽不是比較速度，著重表演水平。參賽者都以濃妝打扮，邊划船邊表演歌舞。

### 夜龙舟

龙舟两旁垂下无数小灯，在晚上出行。夜龙舟的灯光和水波互相辉映，猶如银白色的[蛟龙在滑行](../Page/蛟龙.md "wikilink")。

### 皇室貴族龍舟

為皇室貴族專用船隻。高大寬敞，舟上有雄偉樓閣，舟身則有精雕彩繪，外表非常奢華。

### 現代競賽龍舟

龙舟注重轻盈，船身多以[柚木](../Page/柚木.md "wikilink")(主要由[印尼](../Page/印尼.md "wikilink")[坤甸進口](../Page/坤甸.md "wikilink"))或玻璃纤维為材料，頭與尾則多用[樟木](../Page/樟木.md "wikilink")。一般只用一、兩支[旗帜作简单装饰](../Page/旗帜.md "wikilink")。競賽龍舟分有标准龙舟、小龙舟等，但規格因地而異，故以競賽規定為準。龍舟內有鼓手在前、舵手在後，槳手則按龍舟競賽則而定。至於競賽使用之槳、尾舵、鼓等都有規定的長度和重量。

### 冰上龍舟

2017年2月，世界上首次冰上龍舟比賽在加拿大首都渥太華舉行，以作為當地慶祝華人農曆新年及認識中國文化的活動。冰上龍舟為塗上龍舟花紋的比賽用長艇，再在底下安裝上雪橇。參賽隊伍選手坐在龍舟內以雙手撐動滑雪杖，令龍舟在結了冰的湖面上滑行。中國駐加拿大領事館職員亦有出席這項比賽。

### 其他

由於龍舟已成為一種國際化的運動，一些西方國家的龍舟產生了不少變化，與傳統的龍舟造型相異。如會將龍頭的造型由傳統的[中國龍改為](../Page/龍.md "wikilink")[西方龍](../Page/龍_\(西方\).md "wikilink")，或是減去鼓手等。

## 競賽龍舟規格

[Wellington_Dragon_Boat_Festival_2005_3.jpg](https://zh.wikipedia.org/wiki/File:Wellington_Dragon_Boat_Festival_2005_3.jpg "fig:Wellington_Dragon_Boat_Festival_2005_3.jpg")[威靈頓的龍舟節](../Page/威靈頓.md "wikilink")，其龍舟外形與獨木舟接近\]\]
[Drachenbootregatta.jpg](https://zh.wikipedia.org/wiki/File:Drachenbootregatta.jpg "fig:Drachenbootregatta.jpg")[杜伊斯堡的龍舟競賽](../Page/杜伊斯堡.md "wikilink")\]\]
[Gdansk_dragon_boats2.jpg](https://zh.wikipedia.org/wiki/File:Gdansk_dragon_boats2.jpg "fig:Gdansk_dragon_boats2.jpg")[格但斯克的龍舟賽](../Page/格但斯克.md "wikilink")\]\]
[Three_Dragon_Boats_in_Last_Race_of_the_Day.jpg](https://zh.wikipedia.org/wiki/File:Three_Dragon_Boats_in_Last_Race_of_the_Day.jpg "fig:Three_Dragon_Boats_in_Last_Race_of_the_Day.jpg")[俄亥俄州](../Page/俄亥俄州.md "wikilink")[克里夫蘭的龍舟競賽](../Page/克里夫蘭_\(俄亥俄州\).md "wikilink")\]\]

### 小龍舟

根据中国龙舟协会竞赛规则：（暫沒統一標準，一般比賽要求）

  - 龍舟長度：12米（由龍頭至龍尾）
  - 船身長度：9.6米
  - 龍舟宽度：96厘米（中舱最宽处）
  - 龍舟重量：不設統一標準。（唯要求同一赛事所用之最重龍舟與最輕龍舟差距不超過5公斤）

## 奧運聖火傳遞

[Olympic_Torch_Relay_in_HK_Shing_Mun_River_Channel_2.jpg](https://zh.wikipedia.org/wiki/File:Olympic_Torch_Relay_in_HK_Shing_Mun_River_Channel_2.jpg "fig:Olympic_Torch_Relay_in_HK_Shing_Mun_River_Channel_2.jpg")
在[2008年夏季奧林匹克運動會的](../Page/2008年夏季奧林匹克運動會.md "wikilink")[香港區火炬接力](../Page/2008年夏季奧林匹克運動會香港區火炬接力.md "wikilink")（2008年5月2日）於[沙田](../Page/沙田.md "wikilink")[城門河的一段路程曾動用](../Page/城門河.md "wikilink")**龍舟**傳遞[聖火](../Page/奧林匹克聖火.md "wikilink")，是[奧運史上首次](../Page/奧運.md "wikilink")。\[2\]一天後（2008年5月3日）的[澳門區火炬接力於](../Page/2008年夏季奧林匹克運動會澳門區火炬接力.md "wikilink")[澳門](../Page/澳門.md "wikilink")[西灣湖上也有用](../Page/西灣湖.md "wikilink")**龍舟**傳遞[聖火](../Page/奧林匹克聖火.md "wikilink")。\[3\]

## 參看

  - [龙舟 (隋朝)](../Page/龙舟_\(隋朝\).md "wikilink")
  - [龍舟競賽](../Page/龍舟競賽.md "wikilink")

## 參考資料

<references/>

## 外部链接

### 龍舟組織

  - [香港赤柱龍舟協會](http://www.dragonboat.org.hk/)
  - [中國香港龍舟總會](http://www.hkcdba.org/)
  - [香港獨木舟總會：龍舟（傳統艇）](http://www.hkcucanoe.com.hk/notice/dragon_boat)
  - [中國龍舟協會](http://dragonboat.sport.org.cn/)

<!-- end list -->

  - [英國龍舟協會](http://www.dragonboat.org.uk/)
  - [澳洲龍舟協會](http://www.ausdbf.com.au/)
  - [日本龍舟協會](http://www.jdba-dragonboat.com/)
  - [香港自由龍龍舟會](http://www.hkfreedomdragon.org/)
  - [香港業餘龍舟總會](http://www.dragonboat-hk.org/)

### 其他

  - [龙舟、端午节和屈原](http://agri-history.net/scholars/yxl/yxl95.htm)
  - [舟大工洪全瑞工作室-競賽龍舟](http://www.shipskill.com/f3-2.html)
  - [台灣龍舟](https://web.archive.org/web/20090612043855/http://music.kh.edu.tw/project/dragonboat/index.htm)

[龍舟](../Category/龍舟.md "wikilink")
[Category:中華民俗](../Category/中華民俗.md "wikilink")
[Category:琉球民俗](../Category/琉球民俗.md "wikilink")
[Category:越南傳統](../Category/越南傳統.md "wikilink")
[Category:台灣傳統](../Category/台灣傳統.md "wikilink")
[Category:端午節](../Category/端午節.md "wikilink")
[Category:中秋節](../Category/中秋節.md "wikilink")
[Category:中国非物质文化遗产](../Category/中国非物质文化遗产.md "wikilink")
[Category:日本民俗](../Category/日本民俗.md "wikilink")
[Category:東亞傳統船艦](../Category/東亞傳統船艦.md "wikilink")

1.
2.
3.