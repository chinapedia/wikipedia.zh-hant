《**投奔怒海**》（）是一部以[越南共和國](../Page/越南共和國.md "wikilink")（南越）滅亡後為題材的[香港電影](../Page/香港.md "wikilink")，由[許鞍華導演](../Page/許鞍華.md "wikilink")，在1982年上映。主要演員有[林子祥](../Page/林子祥.md "wikilink")、[繆騫人](../Page/繆騫人.md "wikilink")、[馬斯晨](../Page/馬斯晨.md "wikilink")、[劉德華等](../Page/劉德華.md "wikilink")。《投奔怒海》是許鞍華的「越南三部曲」之最後一部。

此片在第二屆的[香港電影金像獎中得到最佳電影](../Page/香港電影金像獎.md "wikilink")、最佳導演、最佳編劇（[邱戴安平](../Page/邱戴安平.md "wikilink")）、最佳美術指導（[區丁平](../Page/區丁平.md "wikilink")）及最佳新人（馬斯晨）等五項大奖。\[1\]2005年在香港舉行的「[中國電影一百年最佳一百部華語電影](../Page/最佳華語電影一百部.md "wikilink")」活動中，《投奔怒海》被選為第八。

## 簡介

《投奔怒海》的片名是由[金庸所取](../Page/金庸.md "wikilink")。電影的投資者為[左派電影明星](../Page/左派.md "wikilink")[夏梦所創辦的青鳥公司](../Page/夏梦.md "wikilink")，外景是在中國的[海南島拍攝](../Page/海南島.md "wikilink")。當時[台灣當局仍然禁止有大陸工作人員參與製作](../Page/台灣.md "wikilink")，和在大陸拍攝的電影在台灣上映，所以當時這片在台灣被禁映，後來才被解禁。電影拍攝時正值中國與越南交惡，電影中亦有出現描述[越南排華的場面](../Page/排華#越南.md "wikilink")。然而電影的內容對[共產主義亦有很深刻的批判](../Page/共產主義.md "wikilink")，因此亦在[中國大陸禁止上映](../Page/中國大陸.md "wikilink")。當時[九七問題剛開始在香港浮現](../Page/香港回歸.md "wikilink")，《投奔怒海》對共產黨統治下政治[迫害](../Page/迫害.md "wikilink")、[人性扭曲的描寫產生很大震撼](../Page/人性.md "wikilink")。在當時仍然是左、右兩派壁壘分明的香港電影圈中，《投奔怒海》在政治上顯得特別弔詭；而同時被台灣、中國以泛政治理由禁映，在電影圈中也極為少見。

## 劇情

電影以[日本](../Page/日本.md "wikilink")[左派記者芥川汐見前往越共統一越南後的越南](../Page/左派.md "wikilink")[峴港採訪為主線](../Page/峴港.md "wikilink")。由越南政府安排行程，讓芥川見到越南人幸福的生活。但隨著芥川發現「幸福生活」場面中的不和諧處，以及認識當地人阮琴娘及其一家後，逐漸看見一般越南人在共產革命後的真實悲慘生活，以及革命幹部的不同面目。最後芥川不忍看見阮琴娘被送往[集體農場](../Page/蘇聯農業集體化.md "wikilink")，安排她乘上難民船離開。最後，阮琴娘看著芥川被燒死，與弟弟坐船離開越南。\[2\]

## 主要角色

|        |                                  |                                                                                                                                                                                                                                                                                                        |
| ------ | -------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| **角色** | **演員**                           | **備註**                                                                                                                                                                                                                                                                                                 |
| 芥川汐見   | [林子祥](../Page/林子祥.md "wikilink") | 日本左派記者。曾搶先報導過越共統一越南的新聞，往統一後的越南峴港採訪，最初從安排的行程見到幸福的越南人。後來認識峴港當地人阮琴娘及其弟，發現一般人經濟生活非常困苦，卻仍不欲往集體農場（新經濟區）。而很多與前南越政府有關係的人，都生活在恐懼之中。芥川在“鷄場”（刑场）拍摄琴娘收尸时認識從集體農場回蜆港的祖明，後來到他的農場，看見實際的情況。最後阮琴娘的母親因賣淫被共產黨幹部迫死，芥川安排阮琴娘及其幼弟乘難民船離開越南。在幫助阮琴娘上船的時候被警察發現，逃跑的時候攜帶的[柴油](../Page/柴油.md "wikilink")（因難民船要求乘客攜帶一桶柴油）被警察的子彈擊中起火燒死。 |
| 夫人     | [繆騫人](../Page/繆騫人.md "wikilink") | 峴港唯一剩下酒吧的過氣[老鴇](../Page/老鴇.md "wikilink")，得到當地某些人保護仍然生存營業。夫人的父親是日本人，母親是中國人，做過法國人的情婦，開酒吧時做美國人的生意。在統一以後在共產黨的鼻息下生存。祖明是夫人的男友。                                                                                                                                                                              |
| 阮琴娘    | [馬斯晨](../Page/馬斯晨.md "wikilink") | 峴港當地少女。父親是陣亡的[南越政府軍軍人](../Page/南越政府軍.md "wikilink")，有兩個弟弟。母親被迫賣淫養活一家。二弟拾荒時撿到[手榴彈被炸死](../Page/手榴彈.md "wikilink")。其母被共產黨幹部迫死後，幹部表示會把琴娘「優先送到新經濟區」。最後與三弟成功乘坐難民船離開越南。                                                                                                                                       |
| 祖明     | [劉德華](../Page/劉德華.md "wikilink") | 原來是[美軍的翻譯](../Page/美軍.md "wikilink")，統一後被送到集體農場。因為想籌錢與其友乘船離開越南，短暫回到峴港。與芥川一起回到農場後，被派往[地雷區掘地雷](../Page/地雷.md "wikilink")，等待機會離開。上船後，難民船被軍方截停，被打死。                                                                                                                                                        |
| 阮主任    | [奇夢石](../Page/奇夢石.md "wikilink") | 負責接待芥川的幹部。曾經是留法學生，參加共產革命大半生，也曾為革命坐牢，年老時卻發現自己原來喜歡美食、美酒、美女，俱與共產主義不符。說：「越南人的革命成功了，我自己的革命失敗了」，亦批評一些年輕的幹部「急功近利」。最後被任命為新經濟區某小隊隊長，被立即送走。當時文化局的幹部（阮主任口中的年輕幹部）認為阮主任「不是一個真正的革命者，無法放棄[小資產階級的濫情](../Page/小資產階級.md "wikilink")」。                                                                                    |

## 破绽

劇本最後一幕芥川因所攜的柴油被追兵的子彈擊中而著火。實際上柴油[燃點很高](../Page/燃點.md "wikilink")，一般不會因中槍而起火。

## 榮譽

<table>
<thead>
<tr class="header">
<th><p>頒獎典禮</p></th>
<th><p>獎項</p></th>
<th><p>名字</p></th>
<th><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong><a href="../Page/1983年香港電影金像獎.md" title="wikilink">1983年香港電影金像獎</a></strong></p></td>
<td><p>最佳電影</p></td>
<td><p>投奔怒海</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳導演</p></td>
<td><p><a href="../Page/許鞍華.md" title="wikilink">許鞍華</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳編劇</p></td>
<td><p><a href="../Page/戴安平.md" title="wikilink">戴安平</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳新演員</p></td>
<td><p><a href="../Page/馬斯晨.md" title="wikilink">馬斯晨</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳美術指導</p></td>
<td><p><a href="../Page/區丁平.md" title="wikilink">區丁平</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳男主角</p></td>
<td><p><a href="../Page/林子祥.md" title="wikilink">林子祥</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳女主角</p></td>
<td><p><a href="../Page/繆騫人.md" title="wikilink">繆騫人</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳女主角</p></td>
<td><p><a href="../Page/馬斯晨.md" title="wikilink">馬斯晨</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳新演員</p></td>
<td><p><a href="../Page/劉德華.md" title="wikilink">劉德華</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳攝影</p></td>
<td><p><a href="../Page/黃宗基.md" title="wikilink">黃宗基</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳剪接</p></td>
<td><p><a href="../Page/健健.md" title="wikilink">健健</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳電影配樂</p></td>
<td><p><a href="../Page/羅永暉.md" title="wikilink">羅永暉</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2005年<a href="../Page/香港電影金像獎.md" title="wikilink">香港電影金像獎</a></p></td>
<td><p><strong><a href="../Page/最佳華語電影一百部.md" title="wikilink">最佳華語電影一百部</a></strong></p></td>
<td><p>投奔怒海</p></td>
<td><p>第八名</p></td>
</tr>
</tbody>
</table>

## 其他

  - 影片英文名稱「Boat
    People」直譯就是「船民」，是香港1988年實行[越南船民甄別政策](../Page/香港越南船民問題.md "wikilink")，對因經濟而非政治原因逃離者的稱呼，以和[難民區別](../Page/難民.md "wikilink")。

## 参考文献

## 外部連結

  - {{@movies|fBone0411291}}

  -
  -
  -
  -
  -
  -
{{-}}

[Category:許鞍華電影](../Category/許鞍華電影.md "wikilink")
[2](../Category/1980年代香港電影作品.md "wikilink")
[Category:香港剧情片](../Category/香港剧情片.md "wikilink")
[Category:1980年代剧情片](../Category/1980年代剧情片.md "wikilink")
[Category:越南背景电影](../Category/越南背景电影.md "wikilink")
[Category:海南取景电影](../Category/海南取景电影.md "wikilink")
[Category:粵語電影](../Category/粵語電影.md "wikilink")
[Category:日语电影](../Category/日语电影.md "wikilink")
[Category:越南语电影](../Category/越南语电影.md "wikilink")
[Category:香港電影金像獎最佳電影](../Category/香港電影金像獎最佳電影.md "wikilink")
[Category:香港電影金像獎最佳導演獲獎電影](../Category/香港電影金像獎最佳導演獲獎電影.md "wikilink")
[Category:香港電影金像獎最佳編劇獲獎電影](../Category/香港電影金像獎最佳編劇獲獎電影.md "wikilink")
[Category:香港電影金像獎最佳美術指導獲獎電影](../Category/香港電影金像獎最佳美術指導獲獎電影.md "wikilink")
[Category:香港電影金像獎最佳新演員獲獎電影](../Category/香港電影金像獎最佳新演員獲獎電影.md "wikilink")

1.
2.