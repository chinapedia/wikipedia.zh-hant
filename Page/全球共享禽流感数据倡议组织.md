**全球共享禽流感数据倡议组织**（****，簡稱**GISAID**），建立于2008年5月，\[1\]是由全世界一组权威的[医学科学家组建](../Page/医学.md "wikilink")，该组织致力于改善[流感数据的共享](../Page/流感.md "wikilink")\[2\]。共有包括7位[诺贝尔奖得主在内的超过](../Page/诺贝尔奖得主列表.md "wikilink")70位顶尖科学家签署了这份协议。

GISAID提供一个包含可以让公众访问的数据库平台。GISAID的用户须注册，并且同意共享他们自己的数据，尊重他人的数据，共同分析数据并合作发表文章，并且保持从数据获得的新技术可以让所有人使用从而使新技术不仅可以用来进行研究工作，而且可以用来开发诊断试剂或者疫苗，并且致力于同数据提供者共同工作的用户获得数据\[3\]。

## 参考文献

<div class="references-small">

<references>

</references>

</div>

## 外部链接

  - [GISAID官方主页](http://gisaid.org)

[Category:禽流感](../Category/禽流感.md "wikilink")
[Category:國際醫療與健康組織](../Category/國際醫療與健康組織.md "wikilink")
[Category:醫學組織](../Category/醫學組織.md "wikilink")
[Category:非营利组织](../Category/非营利组织.md "wikilink")

1.
2.  [1](http://www.nature.com/news/2006/060102/full/439006a.html)[自然杂志将来自世界卫生组织和联合国粮食农业组织的2006年初禽流感暴发的数据整合](http://www.nature.com/nature/googleearth/avianflu1.kml)
3.  [国际生物多样性公约有关安全信息共享的部分，特别是关于发展中国家对于生物技术从来自这些国家的生物提取物中所取得的收益如疫苗、诊断工具的权益](http://www.biodiv.org/convention/articles.shtml?a=cbd-19)