**双林寺**位于[山西省](../Page/山西省.md "wikilink")[平遥县城西南](../Page/平遥县.md "wikilink")6公里的桥头村，是一座历史悠久的佛寺。双林寺也是[世界文化遗产](../Page/世界文化遗产.md "wikilink")“[平遥古城](../Page/平遥古城.md "wikilink")”的一部分。

双林寺初名中都寺，建寺年代没有确切记载。寺内现存[北宋](../Page/北宋.md "wikilink")[大中祥符四年](../Page/大中祥符.md "wikilink")（1011年）《姑姑之碑》记记载：“中都寺重修于[北齐](../Page/北齐.md "wikilink")[武平二年](../Page/武平_\(北齐\).md "wikilink")（571年）”。宋代取佛经“双林入灭”之说，更名双林寺。

双林寺占地约15000平方米，坐北朝南，禅院在东，寺院居西，中轴线自南至北依次为堡门、天王殿、释迦殿、[大雄宝殿](../Page/大雄宝殿.md "wikilink")、娘娘殿，两侧有地藏殿和土地殿、罗汉殿和珈蓝殿、钟楼、鼓楼、千佛殿、菩萨殿、贞义祠。寺内现存[彩塑](../Page/彩塑.md "wikilink")2056尊，大小各异，艺术价值极高，是元明彩塑的精华。

## 参考文献

[S](../Page/category:晋中佛寺.md "wikilink")

[S](../Category/平遥建筑.md "wikilink")
[Category:国家4A级旅游景区](../Category/国家4A级旅游景区.md "wikilink")
[Category:6世紀建立](../Category/6世紀建立.md "wikilink")
[Category:山西全国重点文物保护单位](../Category/山西全国重点文物保护单位.md "wikilink")