[HK_Funeral_Home_North_Point_蕭明_1.JPG](https://zh.wikipedia.org/wiki/File:HK_Funeral_Home_North_Point_蕭明_1.JPG "fig:HK_Funeral_Home_North_Point_蕭明_1.JPG")4樓寫字樓\]\]
 **蕭明**（[英語](../Page/英語.md "wikilink")：**Siu
Ming**，），籍貫[廣東](../Page/廣東.md "wikilink")[東莞](../Page/東莞.md "wikilink")[石排鎮](../Page/石排鎮.md "wikilink")，[香港的](../Page/香港.md "wikilink")[殯儀館經營者](../Page/殯儀館.md "wikilink")，有「香港殯儀大王」之稱。祖父最早在香港開設福壽[長生店](../Page/長生店.md "wikilink")（即[棺材舖](../Page/棺材.md "wikilink")），其父負責經營支店。1932年其父病故，後由他繼任福壽支店經理。1950年他收購[香港殯儀館](../Page/香港殯儀館.md "wikilink")\[1\]，並於1966年建成新館\[2\]，又與人在1959年合股創辦[九龍殯儀館](../Page/九龍殯儀館.md "wikilink")\[3\]。他亦於1970年收購[萬國殯儀館](../Page/萬國殯儀館.md "wikilink")\[4\]，從而壟斷了香港的[殯儀業](../Page/殯儀.md "wikilink")。1971年，他將萬國殯儀館5樓租予[東華三院經營](../Page/東華三院.md "wikilink")[非牟利的](../Page/非牟利.md "wikilink")[東華三院殯儀館](../Page/東華三院殯儀館.md "wikilink")\[5\]。蕭亦曾經擔任[鐘聲慈善社社長及](../Page/鐘聲慈善社.md "wikilink")[東華三院總理](../Page/東華三院.md "wikilink")。1979年2月1日下午4時45分\[6\]，蕭明於離開九龍殯儀館時遭2人挾持上車綁架，半小時後警方於[荃錦公路發現目標座駕](../Page/荃錦公路.md "wikilink")\[7\]，疑犯逃脫，救回蕭明，當晚8時在[北角拘捕幕後主腦](../Page/北角.md "wikilink")，主謀為蕭明胞弟蕭炳耀\[8\]，疑為勒索金錢而綁架胞兄，同年7月25日[最高法院](../Page/香港高等法院.md "wikilink")[陪審團以](../Page/陪審團.md "wikilink")5比2票裁定蕭炳耀罪名不成立\[9\]，當庭釋放。蕭明知悉主腦為胞弟後，大受打擊\[10\]。1986年，蕭明因血管膨脹\[11\]，遠赴[美國](../Page/美國.md "wikilink")[三藩市進行手術](../Page/三藩市.md "wikilink")，惟手術失敗，未幾因[心臟病去世](../Page/心臟病.md "wikilink")\[12\]，終年72歲。

## 家庭

  - [曾鳳群](../Page/曾鳳群.md "wikilink")，蕭明的妻子
  - [蕭百成](../Page/蕭百成.md "wikilink")，蕭明的長子，[何鸿燊](../Page/何鸿燊.md "wikilink")[元配](../Page/元配.md "wikilink")[黎婉華的](../Page/黎婉華.md "wikilink")[女婿](../Page/女婿.md "wikilink")（[何超英前夫](../Page/何超英.md "wikilink")，1981年離婚）。2011年病逝。
  - [蕭志成](../Page/蕭志成.md "wikilink")，蕭明的次子。

[1](http://www.tianjindaily.com.cn/business/content/2006-11/06/content_46803.htm)

## 以其命名事物

  - 位於新界葵涌的[女校](../Page/女校.md "wikilink")[天主教母佑會蕭明中學由蕭明捐款創辦](../Page/天主教母佑會蕭明中學.md "wikilink")。
  - [天主教慈幼會伍少梅中學之內的](../Page/天主教慈幼會伍少梅中學.md "wikilink")「蕭明科學館」

## 資料來源

[蕭明](../Category/香港東莞人.md "wikilink")
[S](../Category/香港商人.md "wikilink")
[S](../Category/香港殯儀.md "wikilink")
[S](../Category/香港慈善家.md "wikilink")
[M](../Category/蕭姓.md "wikilink")

1.

2.

3.

4.
5.

6.

7.
8.
9.
10.

11.
12.