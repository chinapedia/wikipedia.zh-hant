[乐府诗集一百卷.jpg](https://zh.wikipedia.org/wiki/File:乐府诗集一百卷.jpg "fig:乐府诗集一百卷.jpg")

《**乐府诗集**》是一部专门录辑[汉代以迄](../Page/汉代.md "wikilink")[唐五代乐府诗的诗歌总集](../Page/唐五代.md "wikilink")，也编入了部分汉以前流传的古歌辞，\[1\]为[宋代](../Page/宋代.md "wikilink")[郭茂倩编纂](../Page/郭茂倩.md "wikilink")。\[2\]《樂府詩集》是一部较為完備的樂府詩總集，收集漢至唐五代時期的樂府民歌、文人作品和先秦歌謠，按其曲調分為十二類，成為早期研究樂府的重要文獻。《樂府詩集》曾被清初藏书大家[徐乾学](../Page/徐乾学.md "wikilink")、[季振宜等收藏](../Page/季振宜.md "wikilink")，1918年被近代学者[傅增湘所得](../Page/傅增湘.md "wikilink")。20世纪三四十年代，傅增湘将书重新装裱。\[3\]

## 诗集简介

《乐府诗集》集录了[先秦歌谣](../Page/先秦.md "wikilink")，[汉朝至](../Page/两汉.md "wikilink")[唐](../Page/唐朝.md "wikilink")[五代的](../Page/五代.md "wikilink")[乐府诗](../Page/乐府诗.md "wikilink")，全书共一百卷。把乐府诗分为郊庙歌辞（12卷）、燕射歌辞（3卷）、鼓吹曲辞（5卷）、横吹曲辞（5卷）、相和歌辞（18卷）、清商曲辞（8卷）、舞曲歌辞（5卷）、琴曲歌辞（4卷，5曲、9引、12操）、杂曲歌辞（18卷）、近代曲辞（4卷）、杂歌谣辞（7卷）以及新乐府辞（12卷）等12大类。\[4\]

## 著名诗篇

《[上邪](../Page/上邪.md "wikilink")》、《[长歌行](../Page/长歌行_\(乐府诗集\).md "wikilink")》、《[艳歌行](../Page/艳歌行.md "wikilink")》、《[敕勒歌](../Page/敕勒歌.md "wikilink")》、《[陌上桑](../Page/陌上桑.md "wikilink")》、《[木兰诗](../Page/木兰诗.md "wikilink")》、《[昭君怨](../Page/昭君怨.md "wikilink")》、[项羽的](../Page/项羽.md "wikilink")《[垓下歌](../Page/垓下歌.md "wikilink")》、《[十五从军征](../Page/十五从军征.md "wikilink")》等等。\[5\]

## 相关评介

《[四库全书总目](../Page/四库全书总目.md "wikilink")》称《乐府诗集》「是集總括歷代樂府，上起陶唐，下迄五代。其解題徵引浩博，援據精審」。\[6\]

## 诗集版本

  - 原始版本：《乐府诗集》以北宋末、南宋初的浙江刻本为初始版本，现藏于[中国国家图书馆](../Page/中国国家图书馆.md "wikilink")，今存79卷目录2卷，其它缺卷则用元刻本以及清抄本补配而成。\[7\]
  - [人民文学出版社版本](../Page/人民文学出版社.md "wikilink")：《乐府诗集（1-4）》（傅增湘藏宋本）（ISBN
    978-7-0200-7776-2）

## 注釋

## 外部链接

  - [《乐府诗集》全文在线阅读](http://www.zggdwx.com/yuefu.html)

  -
[Category:集部總集類](../Category/集部總集類.md "wikilink")
[Category:宋朝诗集](../Category/宋朝诗集.md "wikilink")
[Category:中国各朝代诗歌作品](../Category/中国各朝代诗歌作品.md "wikilink")

1.

2.

3.

4.

5.

6.

7.