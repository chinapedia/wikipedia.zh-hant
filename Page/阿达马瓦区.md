**阿达马瓦区**（、）是[喀麦隆的一个](../Page/喀麦隆.md "wikilink")[区](../Page/喀麦隆行政区划.md "wikilink")，首府[恩冈代雷](../Page/恩冈代雷.md "wikilink")，2008年以前稱**阿达马瓦省**。阿达马瓦区南接[中部区和](../Page/中部区_\(喀麦隆\).md "wikilink")[东部区](../Page/东部区_\(喀麦隆\).md "wikilink")，北接[北部区](../Page/北部区_\(喀麦隆\).md "wikilink")，西南与[西北区和](../Page/西北区_\(喀麦隆\).md "wikilink")[西部区相邻](../Page/西部区_\(喀麦隆\).md "wikilink")，西与[尼日利亚接壤](../Page/尼日利亚.md "wikilink")，东与[中非共和国毗连](../Page/中非共和国.md "wikilink")。

阿达马瓦区陆地面积约为64,000平方千米，是喀麦隆面积第三大的省份。该省地势多山，人迹稀少，其山区分隔喀麦隆南部的森林和北部的草原。省内主要族群为[富尔贝族](../Page/富尔贝族.md "wikilink")，大部分人口从事牧牛业。

## 参考文献

  - Fanso, V.G. (1989) *Cameroon History for Secondary Schools and
    Colleges, Vol. 1: From Prehistoric Times to the Nineteenth Century.*
    Hong Kong: Macmillan Education Ltd, 1989.
  - Neba, Aaron, Ph.D. (1999) *Modern Geography of the Republic of
    Cameroon,* 3rd ed. Bamenda: Neba Publishers.
  - Ngoh, Victor Julius (1996) *History of Cameroon Since 1800.* Limbé:
    Presbook, 1996.

[A](../Category/喀麦隆省份.md "wikilink")