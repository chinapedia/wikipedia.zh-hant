**愛倫·坡獎**（Edgar Allan Poe Awards，簡稱Edgar
Awards），名字取自美國著名[推理小說作家](../Page/推理小說.md "wikilink")[愛倫·坡](../Page/愛倫·坡.md "wikilink")。自1946年起開始頒發，由[美國推理作家協會創辦](../Page/美國推理作家協會.md "wikilink")，獎項範圍擴及小說、電視、電影、戲劇及廣播領域。

## 獎項

自2003年以來，愛倫·坡獎底下的獎項包括：

  - 最佳小說
  - 最佳新作
  - 最佳平裝本小說
  - 最佳評論/傳記纇
  - 最佳犯罪實錄
  - 最佳短篇小說
  - 最佳青少年讀物
  - 最佳兒童讀物
  - 最佳電視影集
  - 最佳電影劇本
  - 最佳戲劇
  - 愛倫坡特別獎
  - 羅伯·費許紀念獎
  - 烏鴉獎 (Raven Awards)
  - 終身大師獎
  - 瑪麗·希金斯·克拉克獎

## 获奖名单

### 最佳小說

| 年度    | 作者                                                | 作者                   | 作品名稱                                   | 作品英文名稱                            |
| ----- | ------------------------------------------------- | -------------------- | -------------------------------------- | --------------------------------- |
| 1955年 | [雷蒙·錢德勒](../Page/雷蒙·錢德勒.md "wikilink")            |                      | [漫長的告別](../Page/漫長的告別.md "wikilink")   | The Long Goodbye                  |
| 1956年 | [瑪格麗特·米勒](../Page/瑪格麗特·米勒.md "wikilink")          | Margaret Millar      | [无解之心](../Page/无解之心.md "wikilink")     | Beast in View                     |
| 1959年 | [史丹利·艾林](../Page/史丹利·艾林.md "wikilink")            | Stanley Ellin        |                                        | The Eighth Circle                 |
| 1963年 | [狄絲·帕爾吉特](../Page/狄絲·帕爾吉特.md "wikilink")          | Edith Pargeter       |                                        | Death and the Joyful Woman        |
| 1964年 | [艾瑞克·安卜勒](../Page/艾瑞克·安卜勒.md "wikilink")          | Eric Ambler          |                                        | The Light of Day                  |
| 1965年 | [約翰·勒卡雷](../Page/約翰·勒卡雷.md "wikilink")            |                      | [冷戰諜魂](../Page/冷戰諜魂.md "wikilink")     | The Spy Who Came in from the Cold |
| 1968年 | [唐納德·維斯雷克](../Page/唐納德·維斯雷克.md "wikilink")        | Donald E. Westlake   |                                        | God Save the Mark                 |
| 1969年 | [迈克尔·克莱顿](../Page/迈克尔·克莱顿.md "wikilink")          | Jeffery Hudson       | [死亡手術室](../Page/死亡手術室.md "wikilink")   | A Case of Need                    |
| 1972年 | [弗雷德里克·福賽斯](../Page/弗雷德里克·福賽斯.md "wikilink")      | Frederick Forsyth    | [豺狼之日](../Page/豺狼之日.md "wikilink")     | The Day of the Jackal             |
| 1974年 | [東尼·席勒曼](../Page/東尼·席勒曼.md "wikilink")            | Tony Hillerman       |                                        | Dance Hall of the Dead            |
| 1977年 | [羅伯·派克](../Page/羅伯·派克.md "wikilink")              | Robert B. Parker     | [允諾之地](../Page/允諾之地.md "wikilink")     | Promised Land                     |
| 1979年 | [肯·福莱特](../Page/肯·福莱特.md "wikilink")              | Ken Follett          | [風暴島](../Page/風暴島.md "wikilink")       | Eye of the Needle                 |
| 1984年 | [埃爾莫·倫納德](../Page/埃爾莫·倫納德.md "wikilink")          | Elmore Leonard       |                                        | LaBrava                           |
| 1987年 | [露絲·藍黛爾](../Page/露絲·藍黛爾.md "wikilink")            | Ruth Rendell         |                                        | A Dark-Adapted Eye                |
| 1992年 | [劳伦斯·卜洛克](../Page/劳伦斯·卜洛克.md "wikilink")          |                      | [屠宰场之舞](../Page/屠宰场之舞.md "wikilink")   | A Dance at the Slaughterhouse     |
| 1993年 | [玛格丽特·马容](../Page/玛格丽特·马容.md "wikilink")          | Margaret Maron       |                                        | Bootlegger's Daughter             |
| 1994年 | [米涅·渥特絲](../Page/米涅·渥特絲.md "wikilink")            |                      | 女雕刻家                                   | The Sculptress                    |
| 1996年 | [迪克·弗朗西斯](../Page/迪克·弗朗西斯.md "wikilink")          | Dick Francis         |                                        | Come to Grief                     |
| 1997年 | [湯瑪斯·H·庫克](../Page/湯瑪斯·H·庫克.md "wikilink")        | Thomas H. Cook       |                                        | The Chatham School Affair         |
| 2004年 | [伊恩·藍欽](../Page/伊恩·藍欽.md "wikilink")              |                      | [死後復生的人](../Page/死後復生的人.md "wikilink") | Resurrection Men                  |
| 2007年 | [賈森·古德溫](../Page/賈森·古德溫.md "wikilink")            |                      | [禁衛軍之樹](../Page/禁衛軍之樹.md "wikilink")   | The Janissary Tree                |
| 2008年 | [约翰·哈特](../Page/约翰·哈特.md "wikilink")              |                      | [順流而下](../Page/順流而下.md "wikilink")     | Down River                        |
| 2009年 | [查爾斯·詹姆斯·巴克斯](../Page/查爾斯·詹姆斯·巴克斯.md "wikilink")  | C. J. Box            | 忘記正義的小鎮                                | Blue Heaven                       |
| 2010年 | [约翰·哈特](../Page/约翰·哈特.md "wikilink")              |                      | [最后的守護人](../Page/最后的守護人.md "wikilink") | The Last Child                    |
| 2011年 | [史蒂夫·汉密尔顿](../Page/史蒂夫·汉密尔顿_\(作家\).md "wikilink") |                      | [天才锁匠](../Page/天才锁匠.md "wikilink")     | The Lock Artist                   |
| 2012年 | [莫·海德](../Page/莫·海德.md "wikilink")                | Mo Hayder            | 走失                                     | gone                              |
| 2014年 |                                                   | William Kent Krueger |                                        | Ordinary Grace                    |
| 2015年 | [史蒂芬·金](../Page/史蒂芬·金.md "wikilink")              | Stephen Edwin King   | [賓士先生](../Page/賓士先生.md "wikilink")     | Mr. Mercedes                      |
|       |                                                   |                      |                                        |                                   |

### 最佳新作

| 年度    | 作者                                   | 作者          | 作品名稱                             | 作品英文名稱        |
| ----- | ------------------------------------ | ----------- | -------------------------------- | ------------- |
| 2009年 | [佛蘭西·林](../Page/佛蘭西·林.md "wikilink") | Francie Lin | [外國人](../Page/外國人.md "wikilink") | The Foreigner |
|       |                                      |             |                                  |               |

### 最佳平裝本小說

| 年度    | 作者                                     | 作者           | 作品名稱                             | 作品英文名稱     |
| ----- | -------------------------------------- | ------------ | -------------------------------- | ---------- |
| 2009年 | [梅格·嘉狄納](../Page/梅格·嘉狄納.md "wikilink") | Meg Gardiner | [中國湖](../Page/中國湖.md "wikilink") | China Lake |
|       |                                        |              |                                  |            |

### 終身大師獎

<table>
<thead>
<tr class="header">
<th><p>年度</p></th>
<th><p>作者</p></th>
<th><p>作者</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1973年</p></td>
<td><p><a href="../Page/亞弗列·希治閣.md" title="wikilink">亞弗列·希治閣</a><br />
<a href="../Page/朱蒂森·菲利浦.md" title="wikilink">朱蒂森·菲利浦</a></p></td>
<td><p>Alfred Hitchcock<br />
Judson Philips</p></td>
</tr>
<tr class="even">
<td><p>1978年</p></td>
<td><p><br />
<a href="../Page/桃樂西·休斯.md" title="wikilink">桃樂西·休斯</a><br />
<a href="../Page/奈歐·馬許.md" title="wikilink">奈歐·馬許</a></p></td>
<td><p>Daphne du Maurier<br />
Dorothy B. Hughes<br />
Ngaio Marsh</p></td>
</tr>
<tr class="odd">
<td><p>2009年</p></td>
<td><p><a href="../Page/詹姆斯·李·柏克.md" title="wikilink">詹姆斯·李·柏克</a><br />
<a href="../Page/蘇·葛拉芙頓.md" title="wikilink">蘇·葛拉芙頓</a></p></td>
<td><p>James Lee Burke<br />
Sue Grafton</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 外部連結

  - [美國推理作家協會官方網站](http://www.mysterywriters.org/)

[Category:美國文學獎](../Category/美國文學獎.md "wikilink")
[Category:推理小說獎](../Category/推理小說獎.md "wikilink")
[Category:以人名命名的奖项](../Category/以人名命名的奖项.md "wikilink")
[Category:1946年建立的奖项](../Category/1946年建立的奖项.md "wikilink")