《**Begin...**》是臺灣歌手[王心凌的首張錄音室專輯](../Page/王心凌.md "wikilink")，由艾迴唱片於2003年2月24日發行。該專輯於同年5月15日發行改版「超值回饋版」。

## 曲目

## 音樂錄影帶

| 歌名        | 導演                               |
| --------- | -------------------------------- |
| 灰姑娘的眼淚    | [鄺盛](../Page/鄺盛.md "wikilink")   |
| Beauty    | [賴偉康](../Page/賴偉康.md "wikilink") |
| 當你        | [黃中平](../Page/黃中平.md "wikilink") |
| On My Way | 鄺盛                               |
| 我以為       |                                  |
|           |                                  |

## 版本

  - 2003年2月24日：平裝版（1CD）
  - 2003年5月15日：超值回饋版（1CD、1VCD）

[Category:流行音樂專輯](../Category/流行音樂專輯.md "wikilink")
[Category:台灣音樂專輯](../Category/台灣音樂專輯.md "wikilink")
[Category:王心凌音樂專輯](../Category/王心凌音樂專輯.md "wikilink")
[Category:2003年音樂專輯](../Category/2003年音樂專輯.md "wikilink")