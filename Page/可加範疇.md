在[範疇論中](../Page/範疇論.md "wikilink")，一個**可加範疇**是一個存在有限[雙積的](../Page/雙積.md "wikilink")[預加法範疇](../Page/預加法範疇.md "wikilink")。舊文獻所謂的「可加範疇」有時指[預可加範疇](../Page/預可加範疇.md "wikilink")，在當代理論中則傾向於區別兩者。

一如[預可加範疇](../Page/預可加範疇.md "wikilink")，對一[交換環](../Page/交換環.md "wikilink")\(k\)也能定義**\(k\)-可加範疇**，可加範疇是\(k=\Z\)的情形。

## 例子

最直接的例子是[交換群範疇](../Page/交換群.md "wikilink")**Ab**，此時的有限[雙積即群的有限](../Page/雙積.md "wikilink")[直積](../Page/直積.md "wikilink")。其它常見例子包括：

  - 一個[環上的左](../Page/環.md "wikilink")[模範疇](../Page/模.md "wikilink")，包括[域或](../Page/域.md "wikilink")[除環上的](../Page/除環.md "wikilink")[向量空間範疇](../Page/向量空間.md "wikilink")。
  - 環上的[矩陣](../Page/矩陣.md "wikilink")[代數](../Page/交換環上的代數.md "wikilink")。

## 基本性質

加法範疇是[預可加範疇的特例](../Page/預可加範疇.md "wikilink")，因此具有預可加範疇的性質，在此僅考慮可加範疇對雙積的特性：

首先注意到空雙積存在，稱為[零對象](../Page/零對象.md "wikilink")，記作\(0\)；它同時是範疇中的[始對象與](../Page/始對象.md "wikilink")[終對象](../Page/終對象.md "wikilink")。

給定加法範疇中的對象\(A, B\)，考慮與自身的雙積\(A^n\)與\(B^m\)；透過雙積的射影與內射態射，能夠以[矩陣表示從](../Page/矩陣.md "wikilink")\(A^n\)至\(B^m\)的態射；若取\(A=B\)、\(n=m\)，則態射的合成對應於方陣乘法。

## 可加函子

一個[預加法範疇間的函子](../Page/預加法範疇.md "wikilink")\(F: \mathcal{C} \to \mathcal{D}\)若在同態集上給出群同態，則稱作**可加函子**。如果\(\mathcal{C}, \mathcal{D}\)還是可加範疇，而且\(F\)保存[雙積的交換圖](../Page/雙積.md "wikilink")，則稱之為（可加範疇間的）**可加函子**。換言之：

若\(B\)是\(A_1, \ldots, A_n\)在\(\mathcal{C}\)中的雙積，設\(p_j\)為相應的投影而\(i_j\)為相應的內射，則\(F(B)\)是\(F(A_1), \ldots, F(A_n)\)的雙積，使得\(F(p_j)\)為相應的投影而\(F(i_j)\)為相應的內射。

可加範疇間常見的函子都是可加函子。事實上，可以證明加法範疇間的[伴隨函子都是可加函子](../Page/伴隨函子.md "wikilink")，而範疇論中的重要函子多以伴隨函子的面貌出現。

## 特殊例子

  - 一個[預阿貝爾範疇是使每個態射都有核與上核的可加範疇](../Page/預阿貝爾範疇.md "wikilink")。
  - 一個[阿貝爾範疇是一個使態射均為](../Page/阿貝爾範疇.md "wikilink")[嚴格態射的預阿貝爾範疇](../Page/嚴格態射.md "wikilink")。

應用最廣的可加範疇通常都是阿貝爾範疇。

## 文獻

  - Nicolae Popescu, 1973, *Abelian Categories with Applications to
    Rings and Modules*, Academic Press, Inc.（已絕版） 該書對此主題有仔細介紹

[Category:加法范畴](../Category/加法范畴.md "wikilink")