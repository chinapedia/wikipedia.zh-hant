《**若水**》為[香港歌手](../Page/香港.md "wikilink")[黃耀明的](../Page/黃耀明.md "wikilink")[粵語專輯](../Page/粤语流行音乐.md "wikilink")，於2006年11月30日正式發行。是黃耀明繼2005年向[顧家輝的致敬大碟](../Page/顧家輝.md "wikilink")[明日之歌後的全新錄音室專輯](../Page/明日之歌.md "wikilink")。

## 專輯簡介

黃耀明與人山人海於2006年3月9日至12日與[香港管弦樂團舉行了一連三場](../Page/香港管弦樂團.md "wikilink")，名為[電幻狂想曲的音樂會](../Page/電幻狂想曲.md "wikilink")，他特別改編了[巴洛克時期](../Page/巴洛克時期.md "wikilink")[作曲家Tomaso](../Page/作曲家.md "wikilink")
Albinoni最著名的作品【[Adagio in G
minor](../Page/Adagio_in_G_minor.md "wikilink")】，作為開場曲目【[維納斯](../Page/維納斯.md "wikilink")】，又為香港電影資料館的回顧活動翻唱了[Edith
Piaf的](../Page/Edith_Piaf.md "wikilink")【[Hymn To
Love](../Page/Hymn_To_Love.md "wikilink")】，即【[給你](../Page/給你.md "wikilink")】。這兩首甚具古典風格的歌曲成為了黃耀明創作【若水】的其中一個契機。他又邀得[周迅及](../Page/周迅.md "wikilink")[容祖兒合唱了兩曲](../Page/容祖兒.md "wikilink")：【[流浪者之歌](../Page/流浪者之歌.md "wikilink")】及【[眼淚贊](../Page/眼淚贊.md "wikilink")】。大碟的主要填詞人為[林夕及](../Page/林夕.md "wikilink")[周耀輝](../Page/周耀輝.md "wikilink")。

大碟名稱【若水】來自[道德經中一句](../Page/道德經.md "wikilink")「上善若水，水善利萬物而不爭。」概念來自近年偏好[佛家和](../Page/佛家.md "wikilink")[道家](../Page/道家.md "wikilink")[哲學的林夕](../Page/哲學.md "wikilink")。

## 音樂風格

黃耀明自言**若水**將是他對華麗風格音樂的最後嘗試，這張大碟充滿了濃烈的古典風格及黃耀明向來鍾情的電子音樂。古典風格的歌曲如【維納斯】,【給你】及【眼淚贊】（此曲與[容祖兒合唱](../Page/容祖兒.md "wikilink")）等充滿了自[愈夜愈美麗開始的華麗編曲](../Page/愈夜愈美麗.md "wikilink")，而大碟中電子風格的歌曲亦甚為極端，如【阿姆斯特丹】及【詠嘆調】，強烈的假音和電子節拍標誌著黃耀明已進入了他的「後電子時代」。碟末一曲【[早餐派](../Page/早餐派.md "wikilink")】清新的民謠音樂則預示著他未來的音樂風格。

## 參與單位

  - 演奏

<!-- end list -->

  - 主音：[黃耀明](../Page/黃耀明.md "wikilink")
  - 和音：[胡詠絲](../Page/胡詠絲.md "wikilink")、黃耀明
  - 吉他：[at17](../Page/at17.md "wikilink")
  - 鍵盤：[梁基爵](../Page/梁基爵.md "wikilink")、[亞里安](../Page/亞里安.md "wikilink")
  - 琵琶：[于逸堯](../Page/于逸堯.md "wikilink")
  - 揚琴：[揚文慧](../Page/揚文慧.md "wikilink")

<!-- end list -->

  - 製作

<!-- end list -->

  - 監製：黃耀明、梁基爵、李端嫻、亞里安、蔡德才
  - 混音：李端嫻、[Simon Li](../Page/Simon_Li.md "wikilink")
  - 美術及形象指導：[Tomas Chan](../Page/Tomas_Chan.md "wikilink")

## 曲目

| 次序   | 歌名      | 作曲                                                                                                                        | 填詞                               | 編曲                               | 監製      | 備註                                  |
| ---- | ------- | ------------------------------------------------------------------------------------------------------------------------- | -------------------------------- | -------------------------------- | ------- | ----------------------------------- |
| 1\.  | 維納斯     | [Tomaso Albinoni（Public Domain）](../Page/Tomaso_Albinoni（Public_Domain）.md "wikilink")                                    | [周耀輝](../Page/周耀輝.md "wikilink") | [梁基爵](../Page/梁基爵.md "wikilink") | 黃耀明、梁基爵 |                                     |
| 2\.  | 阿姆斯特丹   | 黃耀明、[蔡德才](../Page/蔡德才.md "wikilink")                                                                                      | 周耀輝                              | 梁基爵                              | 黃耀明、梁基爵 |                                     |
| 3\.  | 流浪者之歌   | 黃耀明、[蔡德才](../Page/蔡德才.md "wikilink")                                                                                      | [林夕](../Page/林夕.md "wikilink")   | 蔡德才                              | 黃耀明、蔡德才 | 與[周迅合唱](../Page/周迅.md "wikilink")   |
| 4\.  | 詠歎調     | [何山](../Page/何山.md "wikilink")                                                                                            | 林夕                               | 何山                               |         |                                     |
| 5\.  | 永恆      | 梁基爵                                                                                                                       | 林夕                               | 梁基爵                              | 黃耀明、梁基爵 |                                     |
| 6\.  | 給你      | [Marquerite Monnot](../Page/Marquerite_Monnot.md "wikilink")、[Eddie Constantine](../Page/Eddie_Constantine.md "wikilink") | 周耀輝                              | 梁基爵                              | 黃耀明、梁基爵 |                                     |
| 7\.  | 眼淚贊     | 亞里安                                                                                                                       | 周耀輝                              | 亞里安                              |         | 與[容祖兒合唱](../Page/容祖兒.md "wikilink") |
| 8\.  | 四大皆空    | 梁基爵                                                                                                                       | 林夕                               | 梁基爵                              |         |                                     |
| 9\.  | 早餐派     | [四方果](../Page/四方果.md "wikilink")                                                                                          | 周耀輝                              | 梁基爵                              |         |                                     |
| 10\. | 如歌的行板   | 梁基爵                                                                                                                       | 林夕                               | 梁基爵                              |         | 永恆國語版                               |
| 11\. | 給你（國語）  | Marquerite Monnot、Eddie Constantine                                                                                       | 周耀輝                              | 梁基爵                              |         |                                     |
| 12\. | 維納斯（國語） | Tomaso Albinoni（Public Domain）                                                                                            | 周耀輝                              | 梁基爵                              |         |                                     |

## 音樂錄像

| 曲序 | 歌名  | 導演 | 推出年份  | MV連結                                                         |
| -- | --- | -- | ----- | ------------------------------------------------------------ |
| 01 | 維納斯 | －  | 2006年 | [官方MV Youtube連結](http://www.youtube.com/watch?v=yldd92k4qmA) |

  - (-) 為未能提供相關資料

## 派台歌曲及成績

### 香港四台成績

| 派台次序      | 歌曲      | 903 專業推介 | RTHK 中文歌曲龍虎榜 | 997 勁爆流行榜 | TVB 勁歌金榜 |
| --------- | ------- | -------- | ------------ | --------- | -------- |
| **2005年** |         |          |              |           |          |
| 01        | 給你      | －        | －            | 10        | 4        |
| **2006年** |         |          |              |           |          |
| 02        | **維納斯** | **1**    | 7            | 8         | 3        |
| 03        | 阿姆斯特丹   | 8        | 3            | 4         | 2        |
| 04        | **永恆**  | 10       | 14           | 5         | **1**    |
| 05        | 早餐派     | －        | －            | 5         | －        |

  - **粗體**為冠軍歌

## 相關獎項及提名

### 歌曲《維納斯》

  - 香港：[2006
    CASH金帆音樂獎](../Page/2006_CASH金帆音樂獎.md "wikilink")－最佳另類作品（入圍五強）

## 爭議

### 唱片封面疑似抄襲

  - 《若水》在推出後，被指唱片封面抄襲[冰島女歌手](../Page/冰島.md "wikilink")[Björk音樂錄影帶](../Page/碧玉.md "wikilink")《Oceania
    Olympics》的造型。

## 參考文獻

## 外部連結

  - [人山人海官方網頁](http://www.peoplemountainpeoplesea.com)
  - [港樂VS黃耀明Live
    電幻音樂會](http://www.flickr.com/photos/samsonso_photography/sets/72057594078516313/)

[Category:香港音樂專輯](../Category/香港音樂專輯.md "wikilink")
[Category:2006年音樂專輯](../Category/2006年音樂專輯.md "wikilink")
[Category:黃耀明音樂專輯](../Category/黃耀明音樂專輯.md "wikilink")