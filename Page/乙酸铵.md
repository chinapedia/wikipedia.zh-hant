**乙酸銨**是一个[有机盐](../Page/有机盐.md "wikilink")，分子式為CH<sub>3</sub>COONH<sub>4</sub>，白色粉末，水溶液呈中性。可通过[乙酸和](../Page/乙酸.md "wikilink")[氨](../Page/氨.md "wikilink")[反应得到](../Page/化學反應.md "wikilink")。可以用作分析试剂、肉类防腐剂，或者制药等。

[Category:有机酸铵盐](../Category/有机酸铵盐.md "wikilink")
[Category:乙酸盐](../Category/乙酸盐.md "wikilink")