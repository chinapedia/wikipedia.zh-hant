**李東健**（，），韓國男演員、歌手。

## 個人生活

2003年考入[漢陽大學戲劇電影學系](../Page/漢陽大學.md "wikilink")，2008年因為休學次數過多，被[漢陽大學開除學籍](../Page/漢陽大學.md "wikilink")，後通過[慶熙CYBER大學插大資格](../Page/慶熙大學.md "wikilink")，準備3月入學。2010年六月入伍服役、2012年3月28日退伍。

2004年與《[新娘18歲](../Page/新娘18歲.md "wikilink")》同劇女主角[韓智慧相戀](../Page/韓智慧.md "wikilink")，直至2007年末分手。
2015年6月因拍攝電影《[邂逅](../Page/邂逅_\(電影\).md "wikilink")》與[T-ara成員](../Page/T-ara.md "wikilink")[朴芝妍相識進而交往](../Page/朴芝妍.md "wikilink")，2016年12月分手。

2017年2月28日公開與KBS週末劇《[月桂樹西裝店的紳士們](../Page/月桂樹西裝店的紳士們.md "wikilink")》女主角[趙胤熙的戀情](../Page/趙胤熙.md "wikilink")，兩人合作中互生好感並於近期開始戀愛。\[1\]
5月2日，李在自己的官方cafe宣布已和[趙胤熙完成婚姻登記](../Page/趙胤熙.md "wikilink")，並在準備結婚的過程中擁有了新生命，正以感恩的心情期待孩子的降臨。\[2\]
9月29日二人在首爾舉行了非公開婚禮，並於12月14日凌晨喜得千金，母女平安。

## 演出作品

### 電視劇

  - 1998年：[KBS](../Page/韓國放送公社.md "wikilink")《[愛我好不好](../Page/愛我好不好.md "wikilink")》飾
    李東旭
  - 2000年：[MBC](../Page/文化廣播_\(韓國\).md "wikilink")《[三個好友](../Page/三個好友.md "wikilink")》飾
    李東健
  - 2001年：[TBS](../Page/TBS.md "wikilink")/MBC《[Friends](../Page/命運之戀.md "wikilink")》飾
    朴京周
  - 2001年：KBS《[表裡不一](../Page/表裡不一.md "wikilink")》飾 李東健
  - 2002年：MBC《[改變她主意的隨想](../Page/改變她主意的隨想.md "wikilink")》 飾 金錫恩
  - 2002年：MBC《[預約愛情](../Page/預約愛情_\(電視劇\).md "wikilink")》飾 韓東鎮
  - 2002年：MBC《[玲玲](../Page/玲玲_\(電視劇\).md "wikilink")》飾 河泰勛
  - 2003年：MBC《[NO春香vs非夢龍](../Page/NO春香vs非夢龍.md "wikilink")》飾 李才勛
  - 2003年：KBS《[尚道呀，上學去](../Page/尚道呀，上學去.md "wikilink")》飾 姜民碩
  - 2003年：MBC《[用生命去愛你](../Page/用生命去愛你.md "wikilink")》飾 李光植
  - 2003年：[SBS](../Page/SBS_\(韓國\).md "wikilink")《[興夫家的葫蘆開了](../Page/興夫家的葫蘆開了.md "wikilink")》飾
    朴尚久
  - 2004年：KBS《[新娘18歲](../Page/新娘18歲.md "wikilink")》飾 權赫俊
  - 2004年：SBS《[巴黎戀人](../Page/巴黎戀人.md "wikilink")》飾 尹修赫
  - 2004年：SBS《[玻璃畫](../Page/玻璃畫_\(電視劇\).md "wikilink")》飾 韓東柱／山本裕一
  - 2006年：SBS《[再次微笑](../Page/再次微笑.md "wikilink")》飾 潘河振
  - 2007年：SBS《[如果愛，就像他們](../Page/如果愛，就像他們.md "wikilink")》飾 朴鄭泰
  - 2008年：MBC《[每天夜晚](../Page/每天夜晚.md "wikilink")》飾 金範相
  - 2009年：SBS《[明星的戀人](../Page/明星的戀人.md "wikilink")》飾 賢俊（客串第20集）
  - 2013年：KBS《[未來的選擇](../Page/未來的選擇.md "wikilink")》飾 金信
  - 2015年：[tvN](../Page/TVN.md "wikilink")《[Super Daddy
    烈](../Page/Super_Daddy_烈.md "wikilink")》飾 韓烈
  - 2016年：KBS《[月桂樹西裝店的紳士們](../Page/月桂樹西裝店的紳士們.md "wikilink")》飾 李東振
  - 2017年：KBS《[七日的王妃](../Page/七日的王妃.md "wikilink")》飾演
    [李㦕](../Page/燕山君.md "wikilink")
  - 2018年：[JTBC](../Page/JTBC.md "wikilink")《[Sketch](../Page/Sketch_\(韓國電視劇\).md "wikilink")》飾演
    金道振
  - 2018年：SBS《[狐狸新娘星](../Page/狐狸新娘星.md "wikilink")》飾演 徐仁宇
  - 2019年：KBS《[僅此一次的愛情](../Page/僅此一次的愛情.md "wikilink")》飾 池康宇

### 網路劇

  - 2016年：[愛奇藝](../Page/愛奇藝.md "wikilink")《[老师晚上好](../Page/老师晚上好.md "wikilink")》飾
    周宇騰

### 電影

  - 2002年：《[命運之戀](../Page/命運之戀.md "wikilink")》與韓國籍演員[元斌及日本籍演員](../Page/元斌.md "wikilink")[深田恭子](../Page/深田恭子.md "wikilink")、[矢田亞希子](../Page/矢田亞希子.md "wikilink")、[小澤征悅等人聯合演出](../Page/小澤征悅.md "wikilink")
  - 2002年：《[家庭](../Page/家庭_\(電影\).md "wikilink")》
  - 2005年：《[我的B型男友](../Page/我的B型男友.md "wikilink")》飾 邱榮彬
  - 2007年：《[Scud](../Page/Scud.md "wikilink")》（未播出）
  - 2007年：《[現在與相愛的人一起生活嗎？](../Page/現在與相愛的人一起生活嗎？.md "wikilink")》
  - 2007年：《[愛歌](../Page/愛歌_\(電影\).md "wikilink")》
  - 2015年：《[邂逅](../Page/邂逅_\(電影\).md "wikilink")》

### MV

  - 2005年：[李秀英](../Page/李秀英_\(韓國藝人\).md "wikilink")《Forever You》
  - 2007年：[BIGBANG](../Page/BIGBANG.md "wikilink")《最後的問候》
  - 2009年：4Tomorrow《心跳Tomorrow》

## 專輯

  - 1998年：《[Time to Fly](../Page/Time_to_Fly.md "wikilink")》
  - 2000年：《[Much More](../Page/Much_More.md "wikilink")》
  - 2008年：《[月光](../Page/月光.md "wikilink")》（日本、單曲）
  - 2008年：《[My Biography](../Page/My_Biography.md "wikilink")》（日本）

## 其他歌曲

  - 1998年：愛我好不好
  - 2002年：MBC （音樂劇）最佳劇場
  - 2003年：用生命愛你
  - 2004年：[玻璃畫主題曲](../Page/玻璃畫.md "wikilink") - 《朋友》
  - 2005年：[我的B型男友主題曲](../Page/我的B型男友.md "wikilink") - 《And I Love You
    So》
  - 2007年：現在和相愛的人一起生活嗎
  - 2008年：日本電視劇Swindler Lirico - 《Believer》
  - 2008年：《Salad Song》（與[尹恩惠](../Page/尹恩惠.md "wikilink")）

## 獲獎

  - 2004年：[SBS演技大賞](../Page/SBS演技大賞.md "wikilink")－男子特別企劃劇優秀演技賞（[巴黎戀人](../Page/巴黎戀人.md "wikilink")）、十大明星賞（[巴黎戀人](../Page/巴黎戀人.md "wikilink")）（[玻璃畫](../Page/玻璃畫.md "wikilink")）
  - 2005年：第41屆百想藝術大賞－電視劇部門人氣賞（[巴黎戀人](../Page/巴黎戀人.md "wikilink")）
  - 2008年：[MBC演技大賞](../Page/MBC演技大賞.md "wikilink")－男子優秀演技獎（[每天夜晚](../Page/每天夜晚.md "wikilink")）
  - 2015年：第4屆[APAN Star Awards](../Page/大田電視劇節.md "wikilink")－韓流明星賞
  - 2016年：[KBS演技大賞](../Page/2016_KBS演技大賞.md "wikilink")－男子長篇劇優秀演技賞（[月桂樹西裝店的紳士們](../Page/月桂樹西裝店的紳士們.md "wikilink")）
  - 2017年：第22屆消費者日頒獎禮（[月桂樹西裝店的紳士們](../Page/月桂樹西裝店的紳士們.md "wikilink")）
  - 2017年：[KBS演技大賞](../Page/2017_KBS演技大賞.md "wikilink")－男子中篇劇優秀演技賞（[七日的王妃](../Page/七日的王妃.md "wikilink")）

## 外部連結

  - [官方影迷網站](http://cafe.daum.net/leedonggun)
  - [EPG](https://web.archive.org/web/20080410034435/http://epg.epg.co.kr/star/profile/index.asp?actor_id=1190)

  - [Yahoo
    Korea](http://kr.search.yahoo.com/search?p=%EC%9D%B4%EB%8F%99%EA%B1%B4&ret=1&fr=kr-search_top&x=40&y=14)


[L](../Category/韓國電視演員.md "wikilink")
[L](../Category/韓國電影演員.md "wikilink")
[L](../Category/韓國男歌手.md "wikilink")
[L](../Category/韓國天主教徒.md "wikilink")
[L](../Category/漢陽大學校友.md "wikilink")
[L](../Category/首爾藝術大學校友.md "wikilink")
[L](../Category/首爾特別市出身人物.md "wikilink")
[L](../Category/李姓.md "wikilink")
[L](../Category/慶熙CYBER大學校友.md "wikilink")

1.  [「月桂樹CP」誕生！
    李東健趙允熙熱戀ing，韓星網，2017-02-28](http://www.koreastardaily.com/tc/news/91619)
2.  [雙喜臨門\!
    李東健與趙胤熙結婚兼做爸媽，中時電子報，2017-05-02](http://www.chinatimes.com/realtimenews/20170502004827-260404)