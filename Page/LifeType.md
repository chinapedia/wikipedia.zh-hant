**LifeType**是一個[開源的](../Page/開源.md "wikilink")[網誌平台系統](../Page/網誌.md "wikilink")，支援在同一次安裝底下，網站架設多個網誌，並同時作用戶管理。LifeType是一個基於[PHP與](../Page/PHP.md "wikilink")[MySQL技術的多用戶與多](../Page/MySQL.md "wikilink")[網誌系統](../Page/Blog.md "wikilink")，以[GPL授權發佈](../Page/GNU_General_Public_License.md "wikilink")。

## 概說

**LifeType**是在2003年2月由專案創始人**Oscar
Renalias**所建立的，他原來建立這個專案的目的只是因為他需要一個簡單的環境能來管理他自己的網站。在那時候其實他並不知道什麼是[網誌](../Page/Blog.md "wikilink")，但是初期的專案其實已經具備有[網誌的雛形了](../Page/Blog.md "wikilink")。幾個禮拜後，另一個共同專案創始人**Francesc**，建議乾脆以這樣的基礎來建立一個多用戶與多[網誌的平台](../Page/Blog.md "wikilink")。看起來似乎是個不錯的想法，所以幾個月後就在2003年9月2日釋出了**pLog**
0.1版。當初的一些功能，目前都還是留在**LifeType**專案的核心當中。

## 功能

  - 支援多[網誌與多用戶環境](../Page/Blog.md "wikilink")，而且只需要一次安裝
  - 支援[次網域](../Page/次網域.md "wikilink")
  - 集中的社群頁面，容許同一個網誌有多一個用戶參與
  - 整合的多媒體檔案管理（支援[Podcasting](../Page/Podcasting.md "wikilink")、自動圖片縮圖、檔案大量上傳、編寫檔案描述）
  - 多樣化的模版
  - 眾多的[外掛／插件程式](../Page/Plugin.md "wikilink")
  - 超強的[外掛／插件程式架構](../Page/Plugin.md "wikilink")
  - 垃圾防制機制（[貝氏過濾](../Page/貝氏過濾.md "wikilink")、迴響確認、迴響驗證 Captcha、引用網址檢查）
  - 支援簡潔與自訂網址
  - 支援多國語言
  - 支援[Smarty](../Page/Smarty.md "wikilink")[模版引擎](../Page/模版引擎.md "wikilink")
  - 支援[XMLRPC](../Page/XMLRPC.md "wikilink")
  - 支援[流動網誌](../Page/流動網誌.md "wikilink")

## 歷史

**LifeType**原名**pLog**，但[亞馬遜公司早於](../Page/Amazon.md "wikilink")2003年初已經在美國註冊了**Plog**這個商標，甚至比[SourceForge上的](../Page/SourceForge.md "wikilink")**pLog**專案出現的早。所以在2005年時[亞馬遜公司希望](../Page/Amazon.md "wikilink")**pLog**能改名字，並且把這個名字歸還給[亞馬遜公司來使用](../Page/Amazon.md "wikilink")。於是**pLog**於2005年更名為
**LifeType**。

## 參看

  -
## 使用者

＝＝使用者請直接編輯此段落加入即可（機關學校先按層級順序(請注意直轄/省轄縣市)，後而筆劃順序）＝＝

  - [臺灣臺北市立百齡高中](http://blog.blsh.tp.edu.tw/lifetype/summary.php)
  - [臺灣高雄市立五福國中](http://blog.wfjh.kh.edu.tw)
  - [臺灣高雄市立東光國小](http://163.32.170.6/blog/)
  - [臺灣台中市立華龍國小](https://web.archive.org/web/20051125161457/http://plog.hlps.tcc.edu.tw/plog/)
  - [臺灣澎湖縣政府教育局暨國民中小學教育業務及教學交流部落格系統](https://web.archive.org/web/20100117110327/http://lifetype.phc.edu.tw/)

## 外部連結

  - [Lifetype官方網站（英）](http://www.lifetype.net)
  - [Lifetype官方網站（中）](http://www.lifetype.org.tw)
  - [Lifetype官方論壇（英）](http://forums.lifetype.net)
  - [Lifetype官方論壇（中）](http://forum.lifetype.org.tw)
  - [LifeType示範版本](http://www.opensourcecms.com/index.php?option=content&task=view&id=330&Itemid=159)
  - [coldtobi's
    blog](https://web.archive.org/web/20080914064843/http://blog.coldtobi.de/1_coldtobis_blog/archive/128_recaptcha_plugin.html)
    Accessible [ReCaptcha](../Page/ReCaptcha.md "wikilink") Plugin

[Category:PHP](../Category/PHP.md "wikilink")
[Category:網誌軟件](../Category/網誌軟件.md "wikilink")
[Category:網際網路](../Category/網際網路.md "wikilink")
[Category:開源內容管理系統](../Category/開源內容管理系統.md "wikilink")