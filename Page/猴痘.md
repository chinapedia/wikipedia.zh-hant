**猴痘**（**Variole du singe** /
**Monkeypox**）是一种由[猴痘病毒引起的](../Page/猴痘病毒.md "wikilink")，主要在[灵长类](../Page/灵长目.md "wikilink")、[啮齿类动物中传播的疾病](../Page/啮齿目.md "wikilink")，有时偶尔人也可以感染猴痘。

猴痘病毒与[天花病毒同属于](../Page/天花病毒.md "wikilink")[正痘病毒](../Page/正痘病毒.md "wikilink")，因此使用对[天花的](../Page/天花.md "wikilink")[免疫也可以部分抵抗猴痘的爆发](../Page/免疫.md "wikilink")。防御猴痘最好的方法是避免与带病动物的接触。

猴痘最早是1958年在[丹麦](../Page/丹麦.md "wikilink")[哥本哈根從事小兒麻痺疫苗製造的一个实验室里被发现的](../Page/哥本哈根.md "wikilink")。

## 病徵

猴痘的症状是[发高烧](../Page/发热.md "wikilink")、發冷、头痛、背痛、不适、咳嗽，身上（一般聚集在眼帘、面孔、躯干和生殖器）会发类似天花的疱疹，疱疹通常於十天後結痂並留下疤痕。約五成患者會出現[淋巴结肿大](../Page/淋巴结.md "wikilink")（由於天花不會出現此病徵，可藉此作為兩者的區別）。

一般过约四到六周后病会自然好转，但严重时也可以致死，死亡率在1%到10%。潜伏期在5到21天。

## 人類感染

1970年於[非洲](../Page/非洲.md "wikilink")[薩伊首次出現人類感染個案](../Page/薩伊.md "wikilink")。其後於[中非及](../Page/中非.md "wikilink")[西非等國家](../Page/西非.md "wikilink")[熱帶雨林區出現散發性流行](../Page/熱帶雨林.md "wikilink")。

據[世界衛生組織報告指](../Page/世界衛生組織.md "wikilink")1981年-1986年間共出現338名患者，1996年[剛果共和國出現](../Page/剛果共和國.md "wikilink")71名患者；2003年[美國亦曾爆發流行](../Page/美國.md "wikilink")，共出現71名患者，感染來源懷疑從來自於帶病的[岡比亞大鼠傳染](../Page/岡比亞大鼠.md "wikilink")[草原犬鼠](../Page/草原犬鼠.md "wikilink")〈prairie
dogs〉再經草原犬鼠傳染人類。

患者分佈任何年齡層，但主要集中於15歲以下（佔感染人數九成）。

2018年9月，英国出现第一起猴痘确诊病例，被怀疑至少有50人遭受感染\[1\]。

## 参考文献

  -
  - [人畜共通傳染病臨床指引:猴痘](http://www.cdc.gov.tw/CDCzoo/Internet/decl/detail.aspx?uid=46&pid=5&af_id=44)臺灣行政院衛生署疾病管制局

  - [“猴痘”病毒襲擊美國　罪魁疑為寵物土撥鼠](http://news.xinhuanet.com/world/2003-06/09/content_909298.htm)新華網
    2003年6月9日

  - [“猴痘”罪魁：草原土撥鼠](http://www.southcn.com/news/international/zhuanti/pox/pic/200306110975.htm)南方網
    2003年6月11日

[Category:病毒性疾病](../Category/病毒性疾病.md "wikilink")
[Category:兒科疾病](../Category/兒科疾病.md "wikilink")
[Category:核質巨DNA病毒](../Category/核質巨DNA病毒.md "wikilink")

1.  中國報|newspaper=中國報 China Press|accessdate=2018-09-10|language=en-US}}