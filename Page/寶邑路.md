[Po_Yap_Road_2011.jpg](https://zh.wikipedia.org/wiki/File:Po_Yap_Road_2011.jpg "fig:Po_Yap_Road_2011.jpg")
**寶邑路**（）是[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")[將軍澳新市鎮的街道](../Page/將軍澳新市鎮.md "wikilink")，穿越[將軍澳市中心](../Page/將軍澳市中心.md "wikilink")，連接[環保大道和](../Page/環保大道.md "wikilink")[寶順路與](../Page/寶順路.md "wikilink")[翠嶺路交界](../Page/翠嶺路.md "wikilink")，貫通[坑口和](../Page/坑口.md "wikilink")[調景嶺](../Page/調景嶺.md "wikilink")。街道建於填海地之上，兩旁均為住宅和休憩用地，亦是市鎮的交通總匯，設有鐵路車站，往返市區瞬間即可到達。

## 事件

  - 2007年12月14日深夜，[寶順路及寶邑路交界發生致命交通意外](../Page/寶順路.md "wikilink")，一輛[新巴與](../Page/新巴.md "wikilink")[城巴相撞](../Page/城巴.md "wikilink")，2死17傷\[1\]。該路口本來設有[交通燈](../Page/交通燈.md "wikilink")，但事發時則停用，引起爭議\[2\]。
  - 2010年1月15日，寶邑路以南的將軍澳66B區（將軍澳市地段第76號）地皮\[3\]以20億元被勾出，可建住宅單位不多於880伙。
  - 2010年2月22日，上述66B區地皮在[荃灣大會堂拍賣](../Page/荃灣大會堂.md "wikilink")，最終由[新鴻基地產以](../Page/新鴻基地產.md "wikilink")33.7億元投得，比勾地價高出逾68%，預料每呎樓面地價約為4,628元\[4\]。

## 交匯道路

[Bauhinia_Garden_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Bauhinia_Garden_\(Hong_Kong\).jpg "fig:Bauhinia_Garden_(Hong_Kong).jpg")交界，後方為寶盈花園。\]\]

  - [環保大道](../Page/環保大道.md "wikilink")
  - [寶康路](../Page/寶康路.md "wikilink")
  - [唐俊街](../Page/唐俊街.md "wikilink")
  - [唐賢街](../Page/唐賢街.md "wikilink")
  - [寶順路](../Page/寶順路.md "wikilink")

## 沿線建築物

  - [將軍澳運動場](../Page/將軍澳運動場.md "wikilink")
  - [清水灣半島](../Page/清水灣半島.md "wikilink")
  - [將軍澳廣場](../Page/將軍澳廣場.md "wikilink")
  - [君傲灣](../Page/君傲灣.md "wikilink")
  - [怡明邨](../Page/怡明邨.md "wikilink")
  - [寶盈花園](../Page/寶盈花園.md "wikilink")
  - [天晉](../Page/天晉.md "wikilink")
  - [港鐵](../Page/港鐵.md "wikilink")[將軍澳站](../Page/將軍澳站.md "wikilink")
  - [將軍澳中心](../Page/將軍澳中心.md "wikilink")

## 相關

  - [將軍澳市中心](../Page/將軍澳市中心.md "wikilink")
  - [調景嶺](../Page/調景嶺.md "wikilink")
  - [將軍澳](../Page/將軍澳.md "wikilink")

## 途經的公共交通服務

<div class="NavFrame collapsed" style="clear: no; border: 1px solid #999; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; text-align: center; font-weight: bold;">

交通路線列表

</div>

<div class="NavContent" style="margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{將軍澳綫色彩}}">█</font>[將軍澳綫](../Page/將軍澳綫.md "wikilink")：[將軍澳站](../Page/將軍澳站.md "wikilink")

<!-- end list -->

  - [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - [專線小巴](../Page/香港小巴.md "wikilink")

</div>

</div>

## 圖片集

## 參考資料

<div class="references-small">

<references />

</div>

[Category:將軍澳街道](../Category/將軍澳街道.md "wikilink")

1.  〈[將軍澳兩巴士相撞2死17傷](http://hk.news.yahoo.com/071214/12/2le5b.html)〉，《明報》，2007年12月16日
2.  〈[停線代燈位惹爭議 有人嫌阻車流
    有人憂增意外](http://hk.news.yahoo.com/071215/12/2lefe.html)〉，《明報》，2007年12月16日
3.  〈[將軍澳地20億勾出下月拍賣](http://property.mpfinance.com/cfm/pa3.cfm?File=20100115/pab01/p52301a.txt)〉，《明報置業網》，2010年1月15日
4.  〈[新地33.7億奪地樓面地價每呎4628](http://property.mpfinance.com/cfm/pa3.cfm?File=20100223/pab01/p40940.txt)〉，《明報置業網》，2010年2月22日