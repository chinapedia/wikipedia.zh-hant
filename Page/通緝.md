通緝令是由[法庭或者官方發出](../Page/法庭.md "wikilink")，急欲尋找某人有關某案件，例如[艾曼·扎瓦希里及](../Page/艾曼·扎瓦希里.md "wikilink")[奧薩瑪·賓拉登](../Page/奧薩瑪·賓拉登.md "wikilink")，[刑事或](../Page/刑事.md "wikilink")[民事](../Page/民事.md "wikilink")，該人可能稱[通緝犯或者重要](../Page/通緝犯.md "wikilink")[目擊證人](../Page/目擊證人.md "wikilink")。通緝令內可能會提及案情、被通緝人士個人資料及賞金。

在中國古代，有江湖人物以尋找通緝犯，交官府為事業。在[美國](../Page/美國.md "wikilink")[牛仔](../Page/牛仔.md "wikilink")[電影和日本動漫](../Page/電影.md "wikilink")[航海王中](../Page/航海王.md "wikilink")，也有以捉通緝犯為業的人物角色，常稱為「[賞金獵人](../Page/賞金獵人.md "wikilink")」。

## 個案

  - [呂樂](../Page/呂樂.md "wikilink")：[香港著名的](../Page/香港.md "wikilink")「華探長」，2010年在加拿大温哥華病逝，終身未被[香港政府逮捕](../Page/香港政府.md "wikilink")。
  - [馬惜珍](../Page/馬惜珍.md "wikilink")：[棄保潛逃](../Page/棄保潛逃.md "wikilink")[臺灣](../Page/臺灣.md "wikilink")，2015年在臺病逝，終身未被香港政府逮捕。
  - [周正毅](../Page/周正毅.md "wikilink")：被[香港廉政公署通緝之人物](../Page/香港廉政公署.md "wikilink")。控罪包括[串謀詐騙](../Page/串謀詐騙.md "wikilink")、[虛假陳述](../Page/虛假陳述.md "wikilink")。因经济犯罪已在中国内地被判处有期徒刑，目前服刑中。
  - [奧薩瑪·賓拉登](../Page/奧薩瑪·賓拉登.md "wikilink")：[聯邦調查局十大通緝人士之一](../Page/聯邦調查局.md "wikilink")。美國總統[奧巴馬](../Page/奧巴馬.md "wikilink")2011年5月1日在白宮宣布，[蓋達組織領導人賓拉登已經被美國軍方擊斃](../Page/蓋達組織.md "wikilink")。
  - [艾曼·扎瓦希里](../Page/艾曼·扎瓦希里.md "wikilink")：目前是蓋達組織的首腦，懸賞金額2,500萬美元（世界最高），該組織策劃了重大恐怖攻擊[911事件](../Page/911事件.md "wikilink")。
  - [賴昌星](../Page/賴昌星.md "wikilink")：[遠華案主犯](../Page/遠華案.md "wikilink")，與家人潛逃[加拿大](../Page/加拿大.md "wikilink")，被[中華人民共和國政府通緝](../Page/中華人民共和國政府.md "wikilink")，2011年被[加拿大政府](../Page/加拿大.md "wikilink")[遣返回中国](../Page/遣返.md "wikilink")。
  - [林毅夫](../Page/林毅夫.md "wikilink")：[中華民國陸軍叛逃軍官](../Page/中華民國陸軍.md "wikilink")，經[中華民國國防部通緝在案](../Page/中華民國國防部.md "wikilink")。2013年5月28日中華民國國防部表示，林毅夫是“敵前叛逃的罪犯”，若返鄉即以軍法審理。
  - [王又曾](../Page/王又曾.md "wikilink")：前[力霸東森企業集團負責人](../Page/力霸東森企業集團.md "wikilink")，因涉嫌掏空集團資產遭[台北地方法院通緝在案](../Page/台北地方法院.md "wikilink")，2016年5月29日在洛杉磯車禍死亡。
  - [張五常](../Page/張五常.md "wikilink")：香港[經濟學家](../Page/經濟學家.md "wikilink")，因涉嫌在[美國逃稅](../Page/美國.md "wikilink")，2003年起被美國通緝在案。

## 傳票

  - 是[票券之一](../Page/票券.md "wikilink")，由[法庭發出](../Page/法庭.md "wikilink")，用意提醒[罰款已經過期](../Page/罰款.md "wikilink")，或通知出席聆訊。

## 中華民國通緝名單

## 中华人民共和国通缉令

## 相關

  - [鄭南榕](../Page/鄭南榕.md "wikilink")：[中華民國政治評論人](../Page/中華民國.md "wikilink")，1989年1月21日收到「涉嫌[叛亂](../Page/叛亂.md "wikilink")」[傳票](../Page/傳票.md "wikilink")，因此[自焚](../Page/自焚.md "wikilink")。
  - [奇茲米勒對多佛學區案](../Page/奇茲米勒對多佛學區案.md "wikilink")
  - [東方日報藐視法庭案](../Page/東方日報藐視法庭案.md "wikilink")
  - [刑事訴訟](../Page/刑事訴訟.md "wikilink")
  - [美國唱片業協會](../Page/美國唱片業協會.md "wikilink")
  - [远华案](../Page/远华案.md "wikilink")
  - [金田一少年之事件簿](../Page/金田一少年之事件簿.md "wikilink")
  - [攝理教會](../Page/攝理教會.md "wikilink")
  - [鄭明析](../Page/鄭明析.md "wikilink")
  - [劉家昌](../Page/劉家昌.md "wikilink")
  - [呂樂](../Page/呂樂.md "wikilink")
  - [曾启荣](../Page/曾启荣.md "wikilink")
  - [游月霞](../Page/游月霞.md "wikilink")
  - [他信](../Page/他信.md "wikilink")

## 外部参考

  - [廉政公署：主要被**通緝**人士（香港）列表](http://www.icac.org.hk/eng/0/1/2/3.html)
  - [美國聯邦調查局十大**通緝**人士列表](http://www.fbi.gov/wanted.htm)
  - [香港警務處通緝人士](https://web.archive.org/web/20080820003550/http://www.police.gov.hk/hkp-home/chinese/wanted/index.htm)

[Category:法律术语](../Category/法律术语.md "wikilink")
[Category:刑法](../Category/刑法.md "wikilink")
[Category:程序法学](../Category/程序法学.md "wikilink")
[Category:通缉犯](../Category/通缉犯.md "wikilink")
[Category:海報](../Category/海報.md "wikilink")
[Category:执法技巧](../Category/执法技巧.md "wikilink")