**劉悟**（），[范陽](../Page/范陽.md "wikilink")（今[北京](../Page/北京.md "wikilink")、[保定一帶](../Page/保定.md "wikilink")）人。[唐朝](../Page/唐朝.md "wikilink")[平盧節度使](../Page/平盧節度使.md "wikilink")[李師道部將](../Page/李師道.md "wikilink")，後發動[兵變殺師道](../Page/兵變.md "wikilink")，投降[朝廷](../Page/朝廷.md "wikilink")，封為[昭義節度使](../Page/昭義節度使.md "wikilink")。

## 簡介

祖父刘正臣，原名刘客奴。刘悟少有勇力，效力[宣武节度使](../Page/宣武节度使.md "wikilink")[刘逸准麾下](../Page/刘逸准.md "wikilink")，因偷錢致罪，投奔[平卢节度使](../Page/平卢节度使.md "wikilink")[李师古](../Page/李师古.md "wikilink")，師古見劉悟，曰：“後必貴，然敗吾家者此人也。”授为都知兵马使兼监察御史。

師古死後，弟[李師道繼為節度使](../Page/李師道.md "wikilink")。元和十三年（818年）秋，朝廷“下制罪狀李師道，令[宣武](../Page/宣武.md "wikilink")、[魏博](../Page/魏博.md "wikilink")、[義成](../Page/義成.md "wikilink")、[武寧](../Page/武寧.md "wikilink")、[橫海兵共討之](../Page/滄景.md "wikilink")”，官軍挾討平淮西之聲威，發五道兵討淄青，一路勢如破竹。劉悟在潭趙駐營，抵抗[田弘正](../Page/田弘正.md "wikilink")，大将[夏侯澄等戰死](../Page/夏侯澄.md "wikilink")（《[資治通鑑](../Page/資治通鑑.md "wikilink")》中則說被[田弘正所擒](../Page/田弘正.md "wikilink")），軍中謠傳劉悟當為帥，李師道疑其背叛。刘悟召集大將曰：“魏博田弘正兵强，出战必败，不出则死。今天子所诛者，司空（指李師道）一人而已，悟与公等皆为所驱迫，使就其死。何如杀其来使，整戈以取郓，立大功，转危亡为富贵耶！”

元和十四年（819年）二月，劉悟深夜發動[兵變](../Page/兵變.md "wikilink")，率兵趨至[鄆城西門](../Page/鄆城.md "wikilink")，守門士兵開城門，眾兵進城，四處放火。李師道對李師古之妻說：“劉悟反矣”，與子李弘方躲入[廁所](../Page/廁所.md "wikilink")，被搜出。李師道求見劉悟，兵士不許。劉悟命士兵砍下李師道父子首級，向[田弘正投降](../Page/田弘正.md "wikilink")，朝廷封刘悟为检校[工部尚书](../Page/工部尚书.md "wikilink")、兼[御史大夫](../Page/御史大夫.md "wikilink")。

轉任[昭義節度使移镇](../Page/昭義節度使.md "wikilink")[潞州](../Page/潞州.md "wikilink")（今山西[長治](../Page/長治市.md "wikilink")），[穆宗時](../Page/唐穆宗.md "wikilink")，[监军](../Page/监军.md "wikilink")[宦官](../Page/宦官.md "wikilink")[劉承偕多次污辱劉悟](../Page/劉承偕.md "wikilink")，又縱其屬下亂法，暗與[磁州刺史](../Page/磁州.md "wikilink")[張汶](../Page/張汶.md "wikilink")[綁架劉悟](../Page/綁架.md "wikilink")，劉悟窺破陰謀，令士兵殺張汶，擅囚劉承偕，後由朝廷出面，下詔流放承偕至遠州，劉悟才釋出承偕，上表謝恩。事後劉悟加[檢校司徒及](../Page/司徒.md "wikilink")[同平章事](../Page/同平章事.md "wikilink")。

後期劉悟纵恣，割據一方。[敬宗](../Page/唐敬宗.md "wikilink")[寶曆元年](../Page/寶曆.md "wikilink")（825年）九月病卒，被追赠为[太尉](../Page/太尉.md "wikilink")。其子[劉從諫被擁立](../Page/劉從諫.md "wikilink")。

## 參考書目

  - 《資治通鑑》卷第二百四十二

[L](../Category/唐朝軍事人物.md "wikilink")
[L](../Category/昭義節度使.md "wikilink")
[W](../Category/刘姓.md "wikilink")
[L](../Category/唐朝同平章事.md "wikilink")