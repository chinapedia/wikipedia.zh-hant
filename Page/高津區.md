{{ 日本區

`| 自治體名=高津區`

|圖像=[Kuji_Entoubunsui.JPG](https://zh.wikipedia.org/wiki/File:Kuji_Entoubunsui.JPG "fig:Kuji_Entoubunsui.JPG")
|圖像說明=二領[水渠久地圓筒分水設施](../Page/水渠.md "wikilink")（2009年）

`| 區章=`
`| 日文原名=高津区`
`| 平假名=たかつく`
`| 羅馬字拼音=Takatsu-ku`
`| 都道府縣=神奈川縣`
`| 支廳=`
`| 市=川崎市`
`| 編號=14134-8`
`| 面積=17.10`
`| 邊界未定=`
`| 人口=211,893`
`| 統計時間=2008年5月1日`
`| 自治體=`[`川崎市`](../Page/川崎市.md "wikilink")`（`[`中原區`](../Page/中原區_\(日本\).md "wikilink")`、`[`多摩區`](../Page/多摩區.md "wikilink")`、`[`宮前區`](../Page/宮前區.md "wikilink")`）、`[`橫濱市`](../Page/橫濱市.md "wikilink")`（`[`港北區`](../Page/港北區_\(日本\).md "wikilink")`、`[`都築區`](../Page/都築區.md "wikilink")`）`
[`東京都`](../Page/東京都.md "wikilink")`：`[`世田谷區`](../Page/世田谷區.md "wikilink")
`| 樹=`
`| 花=`
`| 其他象徵物=`
`| 其他象徵=`
`| 郵遞區號=213-8570`
`| 所在地=高津區下作延274番地2`
`| 電話號碼=44-861-3113`
`| 外部連結=`<http://www.city.kawasaki.jp/67/67soumu/home/takatu/index.htm>
`| 經度=`
`| 緯度=`
`| 地圖=`

}}

**高津區**（）是[川崎市的](../Page/川崎市.md "wikilink")7區之一。

## 交通

### 鐵路

  - [東日本旅客鐵道](../Page/東日本旅客鐵道.md "wikilink")

<!-- end list -->

  - [JR_JN_line_symbol.svg](https://zh.wikipedia.org/wiki/File:JR_JN_line_symbol.svg "fig:JR_JN_line_symbol.svg")
    [南武線](../Page/南武線.md "wikilink")：武藏溝之口站 -
    [津田山站](../Page/津田山站.md "wikilink") -
    [久地站](../Page/久地站.md "wikilink")

<!-- end list -->

  - [東京急行電鐵](../Page/東京急行電鐵.md "wikilink")

<!-- end list -->

  - [Tokyu_DT_line_symbol.svg](https://zh.wikipedia.org/wiki/File:Tokyu_DT_line_symbol.svg "fig:Tokyu_DT_line_symbol.svg")
    [田園都市線](../Page/田園都市線.md "wikilink")：[二子新地站](../Page/二子新地站.md "wikilink")
    - [高津站](../Page/高津站_\(神奈川縣\).md "wikilink") - 溝之口站 -
    [梶谷站](../Page/梶谷站.md "wikilink")
  - [Tokyu_OM_line_symbol.svg](https://zh.wikipedia.org/wiki/File:Tokyu_OM_line_symbol.svg "fig:Tokyu_OM_line_symbol.svg")
    [大井町線](../Page/大井町線.md "wikilink")：二子新地站 - 高津站 - 溝之口站

<!-- end list -->

  - [日本貨物鐵道](../Page/日本貨物鐵道.md "wikilink")

<!-- end list -->

  - [武藏野線](../Page/武藏野線.md "wikilink")： - 間的小杉隧道路段

## 外部連結