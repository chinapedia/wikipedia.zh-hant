**承久**（1219年四月十二日至1222年四月十三日）是[日本的](../Page/日本.md "wikilink")[年號之一](../Page/年號.md "wikilink")。[後鳥羽上皇實施](../Page/後鳥羽上皇.md "wikilink")[院政](../Page/院政.md "wikilink")。這個時代的[天皇是](../Page/天皇.md "wikilink")[順德天皇](../Page/順德天皇.md "wikilink")、[仲恭天皇與](../Page/仲恭天皇.md "wikilink")[後堀河天皇](../Page/後堀河天皇.md "wikilink")。[鎌倉幕府之](../Page/鎌倉幕府.md "wikilink")[執權為](../Page/執權.md "wikilink")[北條義時](../Page/北條義時.md "wikilink")。

## 改元

  - 建保七年四月十二日（1219年5月27日）改元承久
  - 承久四年四月十三日（1222年5月25日）改元貞應

## 出處

## 出生

## 逝世

  - 建保七年正月：[源實朝](../Page/源實朝.md "wikilink")，鎌倉幕府第三代征夷大將軍。

## 大事記

  - 建保七年正月：[源賴家之二子](../Page/源賴家.md "wikilink")[源公曉在](../Page/源公曉.md "wikilink")[鶴岡八幡宮](../Page/鶴岡八幡宮.md "wikilink")[暗殺](../Page/暗殺.md "wikilink")[源實朝](../Page/源實朝.md "wikilink")。

## 紀年、西曆、干支對照表

|                                    |                                |                                |                                |                                |
| ---------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| 承久                                 | 元年                             | 二年                             | 三年                             | 四年                             |
| [儒略曆日期](../Page/儒略曆.md "wikilink") | 1219年                          | 1220年                          | 1221年                          | 1222年                          |
| [干支](../Page/干支.md "wikilink")     | [己卯](../Page/己卯.md "wikilink") | [庚辰](../Page/庚辰.md "wikilink") | [辛巳](../Page/辛巳.md "wikilink") | [壬午](../Page/壬午.md "wikilink") |

## 參考

  - [日本年號索引](../Page/日本年號索引.md "wikilink")
  - 同期存在的其他政權之紀年
      - [嘉定](../Page/嘉定_\(宋寧宗\).md "wikilink")（1208年正月至1224年十二月）：[宋](../Page/南宋.md "wikilink")—[宋寧宗趙擴之年號](../Page/宋寧宗.md "wikilink")
      - [天開](../Page/天開.md "wikilink")（1205年至1225年）：[大理](../Page/大理國.md "wikilink")—段智祥之年號
      - [建嘉](../Page/建嘉.md "wikilink")（1211年至1224年）：[李朝](../Page/李朝_\(越南\).md "wikilink")—[李旵之年號](../Page/李旵.md "wikilink")
      - [興定](../Page/興定.md "wikilink")（1217年九月至1222年八月）：[金](../Page/金朝.md "wikilink")——[金宣宗完顏珣之年號](../Page/金宣宗.md "wikilink")
      - [天泰](../Page/天泰.md "wikilink")（1215年十月至1223年十二月）：金時期—[蒲鮮萬奴之年號](../Page/蒲鮮萬奴.md "wikilink")
      - [光定](../Page/光定.md "wikilink")（1211年八月至1223年十二月）：[西夏](../Page/西夏.md "wikilink")—夏神宗[李遵頊之年號](../Page/李遵頊.md "wikilink")

## 參考文獻

  - 松橋達良，《元号はやわかり—東亜歷代建元考》，砂書房，1994年7月，ISBN 4915818276
  - 李崇智，《中国历代年号考》，中华书局，2001年1月ISBN 7101025129
  - 所功，《年号の歴史―元号制度の史的研究》，雄山閣，1988年3月，ISBN 4639007116

[Category:13世纪日本年号](../Category/13世纪日本年号.md "wikilink")
[Category:1210年代日本](../Category/1210年代日本.md "wikilink")
[Category:1220年代日本](../Category/1220年代日本.md "wikilink")