**郎平**
（英文名：，），[中国](../Page/中华人民共和国.md "wikilink")[天津人](../Page/天津.md "wikilink")，著名女子[排球运动员及教练](../Page/排球.md "wikilink")，凭藉强劲而精确的扣杀而赢得“铁榔头”的绰号，而在教练时代，郎平也被称为“郎教头”\[1\]。現时為[中国女排总教练](../Page/中国女排.md "wikilink")。

1980年至1985年连续当选中国[全国十佳运动员](../Page/全国十佳运动员.md "wikilink")，1979年当选为“新中国成立30年来杰出运动员”，1984年当选为“新中国成立35年来杰出运动员”，曾五次获得原[国家体委颁发的体育运动荣誉奖章](../Page/国家体委.md "wikilink")。郎平也是[中国共青团第](../Page/中国共青团.md "wikilink")11届中央委员会委员、[中共十二大代表及全国](../Page/中共十二大.md "wikilink")[三八红旗手](../Page/三八红旗手.md "wikilink")\[2\]。1987年与原国家手球队队员[白帆结婚](../Page/白帆.md "wikilink")，后于1995年离婚，1992年其女儿白浪在美国出生。2016年1月16日与中国社科院教授、博士生导师[王育成在北京举行了婚礼](../Page/王育成.md "wikilink")。\[3\]

## 早年经历

1960年12月10日，郎平出生于天津。因为“大跃进”等诸多因素，所以童年时代的她身体虚弱，母亲常用小米粥来补充她的营养。郎平的父亲是个体育迷，他一直带着郎平到[北京工人体育馆去看比赛](../Page/北京工人体育馆.md "wikilink")，这也使得郎平对体育充满了兴趣，尤其是排球\[4\]。

## 球员生涯

郎平1973年被选入[北京](../Page/北京.md "wikilink")[工人体育馆体校排球班](../Page/工人体育馆.md "wikilink")，1976年进入北京市女排，1978年入选[中国女排](../Page/中国女排.md "wikilink")。1981年，中国女排夺得第3届[世界盃冠军](../Page/世界盃排球赛.md "wikilink")，郎平获“优秀运动员奖”，这是中国女排首次获得[世界冠军](../Page/世界冠军_\(女排\).md "wikilink")。郎平后又随队先-{zh-hans:后;
zh-hk:后;}-获得第9届[世界女排锦标赛冠军和](../Page/世界排球锦标赛.md "wikilink")[洛杉矶奥运会金牌](../Page/1984年夏季奥林匹克运动会.md "wikilink")，协助中国女排取得了「三连冠」。

1985年代表中国成功卫冕世界女排锦标赛，达成「四连冠」后，郎平于同年退役，不久毕业于[北京师范大学](../Page/北京师范大学.md "wikilink")[英语专业](../Page/英语.md "wikilink")。1987年，郎平赴[美国](../Page/美国.md "wikilink")[新墨西哥大学留学](../Page/新墨西哥大学.md "wikilink")，取得体育管理现代化专业[硕士学位](../Page/硕士.md "wikilink")。
其后，郎平赴[意大利](../Page/意大利.md "wikilink")[摩德纳俱乐部女子排球队打球](../Page/摩德纳俱乐部.md "wikilink")，1989年获意大利盃赛冠军。1990年，郎平一度回队参加第11届世界女子排球锦标赛，在决赛中败于[苏联女排](../Page/俄罗斯国家女子排球队.md "wikilink")，获得亚军。此后再次退役，回到新墨西哥大学执教女排。

郎平身高1米84，摸高3米17，与[美国名将](../Page/美利坚合众国.md "wikilink")[弗罗拉·海曼](../Page/弗罗拉·海曼.md "wikilink")、[古巴名将](../Page/古巴.md "wikilink")[米雷亚·路易斯并称为](../Page/米雷亚·路易斯.md "wikilink")1980年代世界女排“三大主攻手”。

## 家庭

  - 前夫[白帆](../Page/白帆.md "wikilink")：前手球队队员。1986年10月30日郎平和白帆结婚。1985年郎平退役之后，1987年和白帆一起到美国自费留学。1995年，两人离婚，原因未对外透露。两人育有一女莉迪亚，中文名白浪，小名浪浪；
  - 女儿[白浪](../Page/白浪.md "wikilink")（莉迪亚）：1992年5月27日出生，2014年6月毕业于[斯坦福大学](../Page/斯坦福大学.md "wikilink")，曾为斯坦福大学女子排球队队员。2015年1月起供职于[杰富瑞](../Page/杰富瑞.md "wikilink")，任投资银行分析师\[5\]\[6\]。
  - 丈夫[王育成](../Page/王育成.md "wikilink")：生于北京，籍贯山东[淄博](../Page/淄博.md "wikilink")，1975年[北京大学历史系中国史专业毕业](../Page/北京大学.md "wikilink")，现为博宝艺术网铜器鉴定专家、中国社会科学院博士生导师。2016年1月16日王育成与中国女排主教练郎平在京举行了婚礼。

## 有趣数据

郎平曾带领中国女排和美国女排多次参加三大赛。
而21世纪之前美国女排和巴西女排亦从未赢得过三大赛冠军（按：1952年-2000年女排三大赛由苏联、日本、中国、古巴四队垄断），21世纪以来，这两队女排首次夺得三大赛冠军，均是在三大赛决赛中击败郎平执教的球队：

2008年北京奥运会，巴西女排在女排决赛击败郞平执教的美国女排，首夺奥运金牌，也是巴西女排首个世界冠军（三大赛）。

2014年世界女排锦标赛，美国女排在决赛击败郞平执教的中国女排，首夺世锦赛冠军，也是美国女排首个世界冠军（三大赛）。

## 教练生涯

1995年，郎平被聘为中国女排主教练，率队走出历史低谷，获得[亚特兰大奥运会银牌和](../Page/1996年夏季奥林匹克运动会.md "wikilink")[第13届世界女排锦标赛亚军](../Page/世界排球锦标赛.md "wikilink")，1997年更被[国际排联评为年度女排最佳教练](../Page/国际排联.md "wikilink")。于1998年辞职。

1999年，郎平赴意大利执教摩德纳俱乐部女排，次年即带队夺得[意大利女排联赛冠军](../Page/意大利.md "wikilink")，2001年夺得[欧洲女排冠军联赛冠军](../Page/欧洲.md "wikilink")、2002年夺得意大利联赛和杯赛双料冠军。2002年下半年，郎平执教意大利诺瓦腊俱乐部女排，率队夺得2004年意大利联赛冠军。同年年底，郎平以全票入选[世界排球名人堂](../Page/世界排球名人堂.md "wikilink")，成为[亚洲排球运动员中获此殊荣的第一人](../Page/亚洲.md "wikilink")\[7\]。

2005年，郎平被聘为[美国女排主教练](../Page/美国女排.md "wikilink")。就职之前，郎平在网上询问了中国民众对于她当美国教练的看法，很多人对此感到骄傲，极力地支持她。\[8\]

2008年，在[北京奥运会排球预赛中](../Page/2008年夏季奥林匹克运动会.md "wikilink")，带领美国队在局数落后的情况下，以3:2反胜[中国队](../Page/中国国家女子排球队.md "wikilink")，其后更於半决赛以3:0打败[古巴队](../Page/古巴国家女子排球队.md "wikilink")，进入决赛。最终美国队以1:3不敌巴西队，获得[银牌](../Page/银牌.md "wikilink")。回顾05年签约时美国排协给她定下的指标“率队打进北京奥运会”，郎平已经远远地超额完成了这一任务。赛后郎平表示，下一个奥运周期自己将续约留任美国队。

2008年，北京时间11月26日凌晨（美国当地时间25日），美国女排宣报主教练郎平不再与球队续约，郎平并表示“想用更多的时间和家人待在一起”\[9\]。11月30日，郎平与[土耳其电信女排队签约执教](../Page/土耳其.md "wikilink")，在随后五个月时间里，郎平带球球队跻身欧洲冠军联赛六强、土耳其联赛常规赛第二和土耳其联赛季后赛四强以及土耳其杯赛四强的球队历史最好成绩\[10\]。

2009年8月，郎平11年后再回国执教，出任[广东恒大女排主教练](../Page/广东恒大女子排球俱乐部.md "wikilink")。\[11\]

2013年4月25日，郎平接任[俞觉敏](../Page/俞觉敏.md "wikilink")，再度担任中国国家女子排球队主教练。\[12\]

2014年10月12日，带领中国女排在意大利举行的[世界排球锦标赛中](../Page/2014年世界女子排球锦标赛.md "wikilink")，四强以局分3比1击败东道主意大利女排，但在决赛以局分1比3败於前东家美国女排，夺得银牌。\[13\]

2015年9月6日，中国女排在[第12届世界盃中以](../Page/2015年世界盃女子排球赛.md "wikilink")10胜1负的成绩夺冠，时隔11年再获世界冠军头衔，这也是郞平作為主教练所得到的第一个世界冠军。\[14\]

2016年1月24日，於[CCTV体坛风云人物评选颁奬典礼中](../Page/CCTV体坛风云人物评选.md "wikilink")，荣获最佳主教练和年度评委会大奬，而中国女排获得最佳团队殊荣。\[15\]
值得一提的是，在2014至2016年间，郎平已三度荣获CCTV体坛风云人物评选「最佳主教练」殊荣，是该评选成立以来首位获得同一个奖项两次或以上的人士。

2016年8月21日，在巴西[里约奥运会上](../Page/2016年夏季奥林匹克运动会.md "wikilink")，郎平率领中国女排击败[塞尔维亚队](../Page/塞尔维亚国家女子排球队.md "wikilink")，这是郎平执教生涯中第一次率队赢得奥运会冠军。\[16\]她也成为世界上分别以球员和教练身份都获得奥运会女排金牌的第一人。\[17\]

2017年，里約奧運週期後，郎平選擇繼續留任中國女排主教練一職。但基於身體健康的問題，這一年以治療身體及康復為主，暫退居幕後。2018年，手術後的郎平重回主教練位置，在8月帶領主力陣容重奪亞運會金牌。

## 奖项

### 国家队（球员）

  - 1981年[世界盃排球赛](../Page/世界盃排球赛.md "wikilink")：[Image:Med
    1.png](https://zh.wikipedia.org/wiki/File:Med_1.png "fig:Image:Med 1.png")
    - **金牌**
  - 1982年[世界排球锦标赛](../Page/世界排球锦标赛.md "wikilink")：[Image:Med
    1.png](https://zh.wikipedia.org/wiki/File:Med_1.png "fig:Image:Med 1.png")
    - **金牌**
  - 1984年[1984年夏季奥林匹克运动会女子排球比赛](../Page/1984年夏季奥林匹克运动会.md "wikilink")：[Image:Med
    1.png](https://zh.wikipedia.org/wiki/File:Med_1.png "fig:Image:Med 1.png")
    - **金牌**
  - 1985年[世界盃排球赛](../Page/世界盃排球赛.md "wikilink")：[Image:Med
    1.png](https://zh.wikipedia.org/wiki/File:Med_1.png "fig:Image:Med 1.png")
    - **金牌**
  - 1990年[世界排球锦标赛](../Page/世界排球锦标赛.md "wikilink")：[Image:Med
    2.png](https://zh.wikipedia.org/wiki/File:Med_2.png "fig:Image:Med 2.png")
    - **银牌**

### 国家队（教练）

  - 1995年[世界盃排球赛](../Page/世界盃排球赛.md "wikilink")：[Image:Med
    3.png](https://zh.wikipedia.org/wiki/File:Med_3.png "fig:Image:Med 3.png")
    - **铜牌**
  - [1996年夏季奥林匹克运动会排球比赛](../Page/1996年夏季奥林匹克运动会排球比赛.md "wikilink")：[Image:Med
    2.png](https://zh.wikipedia.org/wiki/File:Med_2.png "fig:Image:Med 2.png")
    - **银牌**
  - 1998年[世界盃排球赛](../Page/世界盃排球赛.md "wikilink")：[Image:Med
    2.png](https://zh.wikipedia.org/wiki/File:Med_2.png "fig:Image:Med 2.png")
    - **银牌**
  - 2007年[世界盃排球赛](../Page/世界盃排球赛.md "wikilink")：[Image:Med
    3.png](https://zh.wikipedia.org/wiki/File:Med_3.png "fig:Image:Med 3.png")
    - **铜牌**
  - [2008年夏季奥林匹克运动会排球比赛](../Page/2008年夏季奥林匹克运动会排球比赛.md "wikilink")：[Image:Med
    2.png](https://zh.wikipedia.org/wiki/File:Med_2.png "fig:Image:Med 2.png")
    - **银牌**
  - [2014年世界女子排球锦标赛](../Page/2014年世界女子排球锦标赛.md "wikilink")：[Image:Med
    2.png](https://zh.wikipedia.org/wiki/File:Med_2.png "fig:Image:Med 2.png")
    - **银牌**
  - [2015年世界盃女子排球赛](../Page/2015年世界盃女子排球赛.md "wikilink") ：[Image:Med
    1.png](https://zh.wikipedia.org/wiki/File:Med_1.png "fig:Image:Med 1.png")
    - **金牌**
  - [2016年夏季奥林匹克运动会排球比赛](../Page/2016年夏季奥林匹克运动会排球比赛.md "wikilink")：[Image:Med
    1.png](https://zh.wikipedia.org/wiki/File:Med_1.png "fig:Image:Med 1.png")
    - **金牌**
  - [2018年世界女子排球锦标赛](../Page/2018年世界女子排球锦标赛.md "wikilink")：[Image:Med
    3.png](https://zh.wikipedia.org/wiki/File:Med_3.png "fig:Image:Med 3.png")
    - **铜牌**

## 参考资料

## 外部链接

  - [郎平新浪微博](http://weibo.com/langping)

[Category:中国排球教练](../Category/中国排球教练.md "wikilink")
[Category:中国女子排球运动员](../Category/中国女子排球运动员.md "wikilink")
[Category:中国奥运排球运动员](../Category/中国奥运排球运动员.md "wikilink")
[Category:中国奥林匹克运动会金牌得主](../Category/中国奥林匹克运动会金牌得主.md "wikilink")
[Category:天津籍排球运动员](../Category/天津籍排球运动员.md "wikilink")
[Ping](../Category/郎姓.md "wikilink")
[Category:新墨西哥大学校友](../Category/新墨西哥大学校友.md "wikilink")
[Category:北京师范大学校友](../Category/北京师范大学校友.md "wikilink")
[Category:北京师范大学名誉教授](../Category/北京师范大学名誉教授.md "wikilink")
[Category:1984年夏季奥林匹克运动会奖牌得主](../Category/1984年夏季奥林匹克运动会奖牌得主.md "wikilink")
[Category:1984年夏季奥林匹克运动会排球运动员](../Category/1984年夏季奥林匹克运动会排球运动员.md "wikilink")
[Category:奥林匹克运动会排球奖牌得主](../Category/奥林匹克运动会排球奖牌得主.md "wikilink")
[Category:1982年亚洲运动会金牌得主](../Category/1982年亚洲运动会金牌得主.md "wikilink")
[Category:中共十二大代表](../Category/中共十二大代表.md "wikilink")
[Category:改革先锋称号获得者](../Category/改革先锋称号获得者.md "wikilink")
[Category:中国排球协会副主席](../Category/中国排球协会副主席.md "wikilink")

1.
2.
3.  [郎平再婚
    与社科院教授共谐连理](http://hk.apple.nextmedia.com/realtime/china/20160116/54653339).
    苹果日报. 2016-01-16.
4.
5.
6.
7.  [中国网](http://www.china.com.cn/chinese/SPORT-c/227104.htm)
8.
9.
10.
11.
12. [新浪体育](http://sports.sina.cn/?sa=d5121318t24v4&cid=804&sid=200396&vt=4)
13.
14.
15.
16.
17.