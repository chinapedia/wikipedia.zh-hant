**明武宗朱厚照**（），或稱**正德帝**，[明朝第](../Page/明朝.md "wikilink")11代[皇帝](../Page/皇帝.md "wikilink")（1505年－1521年在位），[年号](../Page/年号.md "wikilink")「**[正德](../Page/正德_\(明朝\).md "wikilink")**」。十六年（1521）崩，享年30歲，葬于[康陵](../Page/康陵.md "wikilink")，庙号「**武宗**」，谥号「承天达道英肃睿哲昭德显功弘文思孝**毅皇帝**」。

武宗是有明一朝中极具争议性的统治者。他任情恣性，為人嬉乐胡鬧，荒淫无度，寵信[宦官](../Page/宦官.md "wikilink")、建立[豹房](../Page/豹房.md "wikilink")，強徵[處女](../Page/處女.md "wikilink")、[娈童入宮](../Page/娈童.md "wikilink")，有時也搶奪有夫之婦，逸遊無度，還化名為**朱壽**，自封為「鎮國公、總督軍務威武大將軍、總兵官」，又信仰[密宗](../Page/密宗.md "wikilink")、[回教等](../Page/回教.md "wikilink")，自稱**[忽必烈](../Page/忽必烈.md "wikilink")**（[蒙古名](../Page/蒙古.md "wikilink")，[元世祖之名](../Page/元世祖.md "wikilink")）；**[沙吉熬爛](../Page/沙吉熬爛.md "wikilink")**（[波斯語](../Page/波斯語.md "wikilink")，[回教](../Page/回教.md "wikilink")[蘇菲派的](../Page/蘇菲派.md "wikilink")[蘇菲師](../Page/謝赫_\(稱號\).md "wikilink")）、**[大寶法王](../Page/大寶法王.md "wikilink")**（[藏密名](../Page/藏密.md "wikilink")，[白教](../Page/白教.md "wikilink")[首領](../Page/首領.md "wikilink")），施政荒誕不經，朝廷乱象四起。而另一方面，他為人刚毅果断，任内诛灭[刘瑾](../Page/刘瑾.md "wikilink")，平定[安化王](../Page/安化王之乱.md "wikilink")、[寧王之亂](../Page/寧王之亂.md "wikilink")，在[应州之役中击败](../Page/应州大捷.md "wikilink")[達延汗](../Page/達延汗.md "wikilink")，令鞑靼多年不敢深入，并积极学习他国文化，促进中外交流，体现出有为之君的素质，是一位功过参半的皇帝。

## 人物生平

### 即位之初

明武宗朱厚照为[明孝宗嫡长子](../Page/明孝宗.md "wikilink")，生于1491年10月26日（[弘治四年九月二十四日申时](../Page/弘治_\(明朝\).md "wikilink")）。两岁被立为[皇太子](../Page/皇太子.md "wikilink")。唯一的弟弟[朱厚炜又早夭](../Page/朱厚煒_\(明蔚王\).md "wikilink")，是孝宗唯一长大成人的儿子。弘治十一年春，皇太子出阁读书。他天性聪颖，[讲筵时极为认真](../Page/讲筵.md "wikilink")，面对讲师则恭敬对待。几个月后，便已知晓[翰林院与左](../Page/翰林院.md "wikilink")[春坊所有](../Page/春坊.md "wikilink")[讲师的姓名](../Page/讲师.md "wikilink")，以致有讲师缺席便会问询左右“某先生今日安在邪？”這讓孝宗极为喜爱，出游必带上皇太子。同时孝宗听闻皇太子闲暇时喜好兵戎事，认为他安不忘危，所以也不予以干涉。

弘治十八年五月初八日，孝宗皇帝驾崩。在完成文武百官军民耆老劝进的固定程序后，五月十八日，皇太子朱厚照即位，是为明武宗。

明[正德九年正月](../Page/正德_\(明朝\).md "wikilink")，後來反叛的寧王[朱宸濠獻新樣](../Page/朱宸濠.md "wikilink")[元宵四時花燈數百](../Page/元宵.md "wikilink")，窮極奇巧，內附火藥，明武宗命獻者入懸。时值冬季，宫中按例在檐下设有毡幕御寒。以致火星觸及氊幕，引發大火，自二鼓时分一直烧至天明。火势最大时，武宗正在前往豹房的途中，望见乾清宫的火灾，武宗向左右开玩笑称这是「好一棚大烟火也」兩天後壬午日，武宗以乾清宫灾御[奉天門視朝](../Page/奉天門.md "wikilink")，撤[寶座不設](../Page/寶座.md "wikilink")，遂下詔[罪己](../Page/罪己詔.md "wikilink")，並諭文武百官，同加修省。後又常常离开[帝都](../Page/帝都.md "wikilink")[燕京四处巡游](../Page/燕京.md "wikilink")。

### 宦官与豹房

住在[京師期间](../Page/京師.md "wikilink")，又不愿住在[紫禁城](../Page/紫禁城.md "wikilink")，在宫外建了一座“[豹房](../Page/豹房.md "wikilink")”居住，並甄選大量美女於其中供其淫樂。其[男宠也不计其数](../Page/男宠.md "wikilink")，名曰“老儿当”，但也有學者稱，因為正德帝喜歡各地宗教，這些人主要是通曉[漢文](../Page/漢文.md "wikilink")、[蒙文](../Page/蒙文.md "wikilink")、[藏文或](../Page/藏文.md "wikilink")[波斯文](../Page/波斯文.md "wikilink")，作為宗教人士的翻譯官。

正德帝不喜上朝，起初宠信[刘瑾](../Page/刘瑾.md "wikilink")、[張永](../Page/张永_\(明朝\).md "wikilink")、[丘聚](../Page/丘聚.md "wikilink")、[谷大用等号称](../Page/谷大用.md "wikilink")“[八虎](../Page/八虎.md "wikilink")”的[宦官](../Page/宦官.md "wikilink")，1510年平定[安化王之乱](../Page/安化王之乱.md "wikilink")[朱寘鐇后](../Page/朱寘鐇.md "wikilink")，下令将刘瑾[凌迟](../Page/凌迟.md "wikilink")[处死](../Page/处死.md "wikilink")，后又宠信武士[江彬等人](../Page/江彬_\(明朝\).md "wikilink")。

正德帝喜好[宗教靈異](../Page/宗教.md "wikilink")、怪力亂神，终日与来自[西域](../Page/西域.md "wikilink")、[回回](../Page/回回.md "wikilink")、[蒙古](../Page/蒙古.md "wikilink")、乌斯藏（[西藏](../Page/西藏.md "wikilink")）、[朝鲜半島的](../Page/朝鲜半島.md "wikilink")[异域](../Page/异域.md "wikilink")[法師](../Page/法師.md "wikilink")、番僧相伴。正德帝曾学习[蒙古语](../Page/蒙古语.md "wikilink")，自称[忽必烈](../Page/忽必烈.md "wikilink")，也学[藏传佛教](../Page/藏传佛教.md "wikilink")，自称[大宝法王](../Page/大宝法王.md "wikilink")。\[1\]正德帝還曾亲自接见第一位来华的[葡萄牙使者](../Page/葡萄牙.md "wikilink")[皮莱资](../Page/皮莱资.md "wikilink")。正德帝並因為自己[生肖屬豬](../Page/生肖.md "wikilink")，曾一度[敕令全国禁食](../Page/敕令.md "wikilink")[猪肉](../Page/猪肉.md "wikilink")，但他自己仍食用猪肉「内批仍用豕」；旋即在[大學士](../Page/大學士.md "wikilink")[杨廷和的反對下](../Page/杨廷和.md "wikilink")，降[敕廢除](../Page/敕.md "wikilink")。\[2\]

### 应州之役

[Zhengde.jpg](https://zh.wikipedia.org/wiki/File:Zhengde.jpg "fig:Zhengde.jpg")
正德帝“奋然欲以武功自雄”。正德十二年（1517年）10月，在江彬的怂恿下，自封为“[镇国公總督軍務威武大將軍總兵官朱寿](../Page/镇国公.md "wikilink")”，到边地[宣府](../Page/宣府.md "wikilink")（今[张家口](../Page/张家口.md "wikilink")[宣化区](../Page/宣化区.md "wikilink")）亲征，击溃[蒙古](../Page/蒙古.md "wikilink")[鞑靼小王子](../Page/鞑靼.md "wikilink")（即[达延汗巴图蒙克](../Page/达延汗.md "wikilink")），回去后又给自己加封[太师](../Page/太师.md "wikilink")。史称“[应州大捷](../Page/应州大捷.md "wikilink")”。

### 平宁王乱及驾幸南京

正德十四年（1519年）六月十四日，[宁王朱宸濠在封藩](../Page/朱宸濠.md "wikilink")[江西](../Page/江西.md "wikilink")[南昌叛乱](../Page/南昌.md "wikilink")，是為[宁王之乱](../Page/宁王之乱.md "wikilink")，不過四十三天，就被[贛南](../Page/贛南.md "wikilink")[巡撫](../Page/巡撫.md "wikilink")[王陽明及](../Page/王陽明.md "wikilink")[吉安](../Page/吉安.md "wikilink")[知府](../Page/知府.md "wikilink")[伍文定募集散兵游勇平定](../Page/伍文定.md "wikilink")，斬殺三萬餘人，朱宸濠被擒。八月二十二日，武宗离开[北京亲征](../Page/北京.md "wikilink")。二十六日，武宗抵达[涿州](../Page/涿州.md "wikilink")，此時王陽明平定叛乱的奏报送达，但武宗仍决定继续南幸。十二月十一日，武宗传谕[内阁](../Page/内阁.md "wikilink")，以正德十五年（1520年）元旦於南京朝贺、祭祀天地。十二月二十六日，武宗御驾抵[应天府](../Page/应天府.md "wikilink")。次日，祭祀南京太庙，武宗成为自永乐以后重新驾临南京的皇帝。正德十五年闰八月初八日，武宗於南京受宁王降。八月十二日，武宗离京返回北京。

### 賦稅改革

正德八年（1513年）起在江南全面推行的賦稅改革，既減輕了江南當地百姓的負擔，更使從弘治晚期開始，江南地區拖欠中央累積十年之久的賦稅，僅經兩年時間就全部還清。

### 市舶司改革

增加外貿收入，擴大了朝廷稅源。

### 驾崩

武宗御驾南征返回北京途中，於[淮安](../Page/淮安.md "wikilink")[清江浦上学](../Page/清江浦.md "wikilink")[渔夫撒网](../Page/渔夫.md "wikilink")，作為遊戲，卻失足落入水中，并因此患病「燥熱難退」\[3\]。正德十五年十二月初十，大驾回到北京，文武百官出至正阳桥外迎接。十三日，皇帝於[南郊祭祀天地](../Page/北京天坛.md "wikilink")，祭拜过程中突然呕血，随即送入[斋宫休养](../Page/斋宫.md "wikilink")。次日，返回大内，仅在[奉天殿举行庆成礼](../Page/奉天殿.md "wikilink")。此后，立春日的朝贺一同免去。正德十六年（1521年）正月初九日，监察御史郑本公鉴于武宗身体状况不乐观，上奏武宗，望能於[宗室间](../Page/宗室.md "wikilink")[過繼一人主掌](../Page/過繼.md "wikilink")[东宫](../Page/东宫.md "wikilink")，但后来武宗身体略有好转。三月十三日晚间，武宗突然向身边的太监陈敬和苏进表示自己可能無法痊癒，让其召[司礼监并禀告皇太后](../Page/司礼监.md "wikilink")，由[太后与](../Page/太后.md "wikilink")[内阁议处天下事](../Page/内阁.md "wikilink")，并表示自己耽误子嗣。十四日，武宗於[豹房驾崩](../Page/豹房.md "wikilink")，得年29歲。\[4\]

由于武宗無子嗣，因此遵照《[皇明祖训](../Page/皇明祖训.md "wikilink")》，由武宗堂弟、孝宗弟兴献王[朱祐杬之子兴王朱厚熜入嗣大统](../Page/朱祐杬.md "wikilink")。正德十六年五月，[朱厚熜抵达京师](../Page/朱厚熜.md "wikilink")，上谥号为**承天达道英肃睿哲昭德显功弘文思孝毅皇帝**，上庙号为武宗。九月，武宗入葬天寿山陵区的[康陵](../Page/明康陵.md "wikilink")。

## 诏书

### 罪己诏

（原诏文中其有缺损处，以“□”代借）

### 遺詔

## 轶闻

[山西应县木塔明万历匾额.JPG](https://zh.wikipedia.org/wiki/File:山西应县木塔明万历匾额.JPG "fig:山西应县木塔明万历匾额.JPG")
明武宗的生辰为弘治四年九月二十四日，八字为辛亥年，戊戌月，丁酉日，戊申时出生。其中，八字地支分别为申酉戌亥，这种排列方法被称为连如贯珠。在此以前仅太祖[朱元璋的](../Page/朱元璋.md "wikilink")[八字与此类似](../Page/八字.md "wikilink")。\[5\]

賜自己的[替僧為漢地](../Page/替僧.md "wikilink")[噶瑪巴](../Page/噶瑪巴.md "wikilink")，正德五年封大慶法王，鑄大慶法王西天覺道圆明自在大定慧佛金印，兼给誥命，藏名為「領占班丹」，並曾邀請藏地八代噶瑪巴至北京（七代噶瑪巴曾說：「將現身兩位噶瑪巴」）；蒙古名為忽必烈；波斯名為沙吉熬爛，即[蘇菲師](../Page/謝赫_\(稱號\).md "wikilink")（Shaykh，回教蘇菲派長者、教長），並擁有一群伊斯蘭[火者](../Page/火者.md "wikilink")，稱為老兒當。對[道教亦多有了解](../Page/道教.md "wikilink")，可能曾號錦堂老人。

正德十五年（1520年）闰八月，武宗御驾自南京返回时，途径[镇江](../Page/镇江.md "wikilink")，适逢退休居家的原[内阁大臣](../Page/内阁.md "wikilink")[靳贵病逝](../Page/靳贵.md "wikilink")，于是亲临靳贵家中吊唁。但是随行大臣代皇帝撰写的祭文皆不能称意，明武宗遂亲自写道：“朕居东宫，先生为傅。朕登大宝，先生为辅。朕今南游，先生已矣。呜呼哀哉！”左右的侍从文学之臣看后都敛手称服。\[6\]

山西[应县木塔顶层有一方明武宗皇帝](../Page/应县木塔.md "wikilink")[御匾](../Page/御匾.md "wikilink")“天下奇观”。

2004年，在[美國](../Page/美國.md "wikilink")[德州一位](../Page/德州.md "wikilink")[華僑手中發現由明朝正德皇帝親筆所書的](../Page/華僑.md "wikilink")[聖旨](../Page/聖旨.md "wikilink")，內容敘述做人應如何有進取心以及如何為忠[君之臣與正人](../Page/君.md "wikilink")[君子](../Page/君子.md "wikilink")。此文物的發現造成了史學家對歷史記載正德皇帝人格的爭議。\[7\]

## 历史評價

史学界对正德帝的评价不一，\[8\]
有人认为正德帝雖荒淫無行，行徑胡鬧，不理國政，造成叛變日起，且自身壯年即因為逸樂而死；但是亦有人认为他頗能容忍大臣，不罪勸諫之人。君臣之間，相安無事，知错能改，诛灭奸佞。

  - [张廷玉等](../Page/张廷玉.md "wikilink")《[明史](../Page/明史.md "wikilink")》贊曰：「明自正統以來，國勢浸弱。毅皇手除逆瑾，躬禦邊寇，奮然欲以武功自雄。然耽樂嬉遊，暱近群小，至自署官號，冠履之分蕩然矣。猶幸用人之柄躬自操持，而秉鈞諸臣補苴匡救，是以朝綱紊亂，而不底於危亡。假使承孝宗之遺澤，制節謹度，有中主之操，則國泰而名完，豈至重後人之訾議哉！」

<!-- end list -->

  - [談遷](../Page/談遷.md "wikilink")《[國榷](../Page/國榷.md "wikilink")》論曰：「武宗少即警敏，好佚樂。……而武宗又不罪一諫臣，元相呵護，群吏奉法。……夜半出片紙縛（劉）瑾，……錢寧俛首受罪。」

<!-- end list -->

  - [吳熾昌](../Page/吳熾昌.md "wikilink")《[續客窗閒話](../Page/續客窗閒話.md "wikilink")》論曰：「……遊戲中確有主裁，但好行小慧，為儒尚且不可，況九五之尊耶？今之讀史者直以帝比之桀紂，無乃過甚。當初諡曰武宗毅皇帝，毅者果決之謂，可見遇事實能決斷，非盡阿諛可知矣。」

## 任用官员

### 宰輔

  - [劉健](../Page/劉健.md "wikilink")
  - [謝遷](../Page/謝遷.md "wikilink")
  - [李東陽](../Page/李東陽.md "wikilink")
  - [焦芳](../Page/焦芳.md "wikilink")

<!-- end list -->

  - [王鏊](../Page/王鏊.md "wikilink")
  - [楊廷和](../Page/楊廷和.md "wikilink")
  - [劉宇](../Page/劉宇.md "wikilink")
  - [曹元](../Page/曹元.md "wikilink")

<!-- end list -->

  - [梁儲](../Page/梁儲.md "wikilink")
  - [劉忠](../Page/劉忠.md "wikilink")
  - [費宏](../Page/費宏.md "wikilink")
  - [楊一清](../Page/楊一清.md "wikilink")

<!-- end list -->

  - [靳貴](../Page/靳貴.md "wikilink")
  - [蔣冕](../Page/蔣冕.md "wikilink")
  - [毛紀](../Page/毛紀.md "wikilink")

### 寵臣

  - [錢寧](../Page/錢寧.md "wikilink")
  - [江彬](../Page/江彬_\(明朝\).md "wikilink")

### 太監

  - [劉瑾](../Page/劉瑾.md "wikilink")：八虎之首，掌[司禮監](../Page/司禮監.md "wikilink")[秉筆太監](../Page/秉筆太監.md "wikilink")。
  - [張永](../Page/张永_\(明朝\).md "wikilink")：八虎之一，總[神機營](../Page/神機營.md "wikilink")，與劉瑾為黨。後與劉瑾不合。並且為後來倒劉瑾的重要人物。且與[王守仁交善](../Page/王守仁.md "wikilink")。
  - [谷大用](../Page/谷大用.md "wikilink")：八虎之一，提督[西廠](../Page/西廠.md "wikilink")。
  - [馬永成](../Page/馬永成_\(明朝\).md "wikilink")：八虎之一，提督[東廠](../Page/東廠.md "wikilink")。
  - [丘聚](../Page/丘聚.md "wikilink")：八虎之一，提督[東廠](../Page/東廠.md "wikilink")。
  - [羅祥](../Page/羅祥.md "wikilink")：八虎之一。
  - [魏彬](../Page/魏彬.md "wikilink")：八虎之一，總三千營。劉瑾誅後，代掌司禮監。
  - [高鳳](../Page/高鳳.md "wikilink")：八虎之一。司禮監太監，掌管機密。
  - [張忠](../Page/张忠_\(霸州\).md "wikilink")：御馬太監，與司禮[張雄](../Page/張雄.md "wikilink")、[東廠](../Page/東廠.md "wikilink")[張銳並侍豹房用事](../Page/張銳.md "wikilink")，時號三張。性皆兇悖。並與大盜[張茂財](../Page/張茂財.md "wikilink")，結為兄弟。並且後與寧王朱宸濠勾結，收受賄絡，協助其叛變；而後還遮藏王守仁之捷，騙出武宗親自出征。
  - [吳經](../Page/吳經.md "wikilink")：太監，隨武宗南征，就先到[揚州](../Page/揚州.md "wikilink")，強奪寡婦、處女，要求拿金錢來換取。

## 家庭成员

<center>

</center>

### 母亲

  - [孝康敬皇后張氏](../Page/孝康敬皇后.md "wikilink")

### 兄弟姊妹

  - 蔚悼王[朱厚炜](../Page/朱厚炜.md "wikilink")
  - [太康公主朱秀榮](../Page/太康公主_\(明孝宗\).md "wikilink")

### 妻妾

  - 后妃
      - [孝静毅皇后夏氏](../Page/夏皇后_\(明武宗\).md "wikilink")
      - [淑惠德妃吳氏](../Page/吳德妃_\(明武宗\).md "wikilink")\[9\]
      - [榮淑賢妃沈氏](../Page/沈賢妃_\(明武宗\).md "wikilink")\[10\]
      - [王妃](../Page/王妃_\(明武宗\).md "wikilink")\[11\]
      - [刘美人](../Page/刘美人_\(明武宗\).md "wikilink")，又称刘夫人\[12\]，太原民刘良之女
  - 无妃嫔位号者
      - [王满堂](../Page/王满堂.md "wikilink")，女官，職位[浣衣](../Page/浣衣.md "wikilink")\[13\]
      - [马氏](../Page/马姬_\(明武宗\).md "wikilink")，马昂之妹\[14\]
  - 位号不详
      - 戴氏，总兵官戴钦女\[15\]
      - 杜氏，马昂妾\[16\]

## 艺术形象

  - 民间有关于武宗[微服出巡与民间女子相爱的故事](../Page/微服出巡.md "wikilink")，即[游龍戲鳳](../Page/游龍戲鳳.md "wikilink")。

### 电影和电视剧

  - 1959年[香港邵氏兄弟电影公司出品电影](../Page/香港.md "wikilink")《[江山美人](../Page/江山美人.md "wikilink")》，[赵雷飾演明武宗](../Page/赵雷.md "wikilink")。
  - 1961年[香港立達影業公司](../Page/香港.md "wikilink")《[刁蠻女俠](../Page/刁蠻女俠.md "wikilink")》。
  - 1976年[香港电视剧](../Page/香港.md "wikilink")《[民间传奇之江山美人](../Page/民间传奇之江山美人.md "wikilink")》，[郑少秋飾演明武宗](../Page/郑少秋.md "wikilink")。
  - 1979年[香港邵氏兄弟电影公司出品电影](../Page/香港.md "wikilink")《[北地胭脂](../Page/北地胭脂.md "wikilink")》，[岳华飾演明武宗](../Page/岳华.md "wikilink")。
  - 1979年[香港丽的电视武侠剧](../Page/香港.md "wikilink")《[天龙诀](../Page/天龙诀.md "wikilink")》，[万梓良飾演明武宗](../Page/万梓良.md "wikilink")。
  - 1979年[香港亚洲电视古装武侠剧](../Page/香港.md "wikilink")《[天蚕变之再与天比高](../Page/天蚕变之再与天比高.md "wikilink")》，[宗扬飾演明武宗](../Page/宗扬.md "wikilink")。
  - 1994年[香港古装爱情剧](../Page/香港.md "wikilink")《[俠女游龍](../Page/俠女游龍.md "wikilink")》，[罗嘉良飾演明武宗](../Page/罗嘉良.md "wikilink")。
  - 1994年[香港古装剧](../Page/香港.md "wikilink")《[天师钟馗之江山美人](../Page/天师钟馗之江山美人.md "wikilink")》，[陈泰鸣飾演明武宗](../Page/陈泰鸣.md "wikilink")。
  - 1998年[台湾古装剧](../Page/台湾.md "wikilink")《[江山美人](../Page/江山美人_\(1998年電視劇\).md "wikilink")》，[崔浩然飾演明武宗](../Page/崔浩然.md "wikilink")。
  - 1998年中国大陆电视剧《[保镖之天之娇女](../Page/保镖之天之娇女.md "wikilink")》，[王卫国饰演明武宗](../Page/王卫国.md "wikilink")。
  - 2000年中国大陆电视剧《[机灵小不懂](../Page/机灵小不懂.md "wikilink")》，[聂远饰演明武宗](../Page/聂远.md "wikilink")。
  - 2000年中国大陆、台湾合拍半喜剧电视剧《[绝色双娇](../Page/绝色双娇.md "wikilink")》，[焦恩俊饰演明武宗](../Page/焦恩俊.md "wikilink")。
  - 2000年香港无线电视翡翠台古装电视剧《[金装四大才子](../Page/金装四大才子.md "wikilink")》，[蔡子健饰演明武宗](../Page/蔡子健.md "wikilink")。
  - 2002年香港电影《[天下无双](../Page/天下無雙_\(電影\).md "wikilink")》，[张震饰演明武宗](../Page/張震_\(演員\).md "wikilink")。
  - 2004年中国大陆电视剧《[皇后进宫](../Page/皇后进宫.md "wikilink")》，[吴大维饰演明武宗](../Page/吴大维.md "wikilink")。
  - 2004年中国大陆电视剧《[无忧公主](../Page/无忧公主.md "wikilink")》，[冯绍峰饰演明武宗](../Page/冯绍峰.md "wikilink")。
  - 2004年中国大陆电视剧《[凤临阁](../Page/凤临阁.md "wikilink")》，[贾一平饰演明武宗](../Page/贾一平.md "wikilink")。
  - 2004年中国大陆电视剧《[长剑相思](../Page/长剑相思.md "wikilink")》，[高梓淇饰演明武宗](../Page/高梓淇.md "wikilink")。
  - 2005年中国大陆古装历史颠覆剧《[正德演义](../Page/正德演义.md "wikilink")》，[何炅饰演明武宗](../Page/何炅.md "wikilink")。
  - 2005年中国大陆电视剧《[天下第一](../Page/天下第一_\(電視劇\).md "wikilink")》，[邓超飾演明武宗](../Page/邓超.md "wikilink")。
  - 2005年中国大陆电视剧《[天下第一媒](../Page/天下第一媒.md "wikilink")》，[李楠飾演明武宗](../Page/李楠.md "wikilink")。
  - 2005年中国大陆电视剧《[剑出江南](../Page/剑出江南.md "wikilink")》，[剑出江南飾演明武宗](../Page/剑出江南.md "wikilink")。
  - 2007年中国大陆电视剧《[夜来风雨](../Page/夜来风雨.md "wikilink")》，[任东霖饰演明武宗](../Page/任东霖.md "wikilink")。
  - 2009年[香港古装武打电视剧](../Page/香港.md "wikilink")《[王老虎搶親](../Page/王老虎搶親_\(電視劇\).md "wikilink")》，[罗贯峰饰演明武宗](../Page/罗贯峰.md "wikilink")。
  - 2010年[香港古装电视剧](../Page/香港.md "wikilink")《[秋香怒点唐伯虎](../Page/秋香怒点唐伯虎.md "wikilink")》，[黎诺懿饰演明武宗](../Page/黎诺懿.md "wikilink")。
  - 2010年[香港电影](../Page/香港.md "wikilink")《[龙凤店](../Page/龙凤店.md "wikilink")》，[任贤齐饰演明武宗](../Page/任贤齐.md "wikilink")。
  - 2010年中国大陆电视剧《[苏三传奇](../Page/苏三传奇.md "wikilink")》，[李彦明饰演明武宗](../Page/李彦明.md "wikilink")。
  - 2011年中国大陆电视剧《[王陽明
    (電視劇)](../Page/王陽明_\(電視劇\).md "wikilink")》，[朱一龍饰演明武宗](../Page/朱一龍.md "wikilink")。
  - 2018年中国大陆古装剧《[回到明朝当王爷之杨凌传](../Page/回到明朝当王爷之杨凌传.md "wikilink")》，[刘芮麟饰演明武宗](../Page/刘芮麟.md "wikilink")。

### 戏曲

  - 京剧《[游龙戏凤](../Page/游龙戏凤.md "wikilink")》、《[梅龙镇](../Page/梅龙镇.md "wikilink")》

### 歌仔戲

  - 電視歌仔戲：《[正德皇帝遊江南](../Page/正德皇帝遊江南.md "wikilink")》（黃香蓮主演）
  - 電視歌仔戲：《[花月正春风](../Page/花月正春风.md "wikilink")》（杨丽花主演）
  - 電視歌仔戲：《[巡按與大盜](../Page/巡按與大盜.md "wikilink")》（洪秀玉主演）
  - 電視歌仔戲：《[三戏正德皇帝](../Page/三戏正德皇帝.md "wikilink")》（陈小咪主演）

### 文学作品

  - 小說：月關《[回到明朝當王爺](../Page/回到明朝當王爺.md "wikilink")》高寶國際出版

## 参考文献

{{-}}

[Category:明朝皇帝](../Category/明朝皇帝.md "wikilink")
[明武宗](../Category/明武宗.md "wikilink")
[1](../Category/明孝宗皇子.md "wikilink")
[自封](../Category/明朝大庆法王.md "wikilink")
[自封](../Category/明朝太师.md "wikilink")
[自封](../Category/明朝镇国公.md "wikilink")
[自封威武大将军](../Category/明朝大将军.md "wikilink")
[自封](../Category/明朝总兵官.md "wikilink")
[H](../Category/朱姓.md "wikilink")
[明](../Category/入祀历代帝王庙景德崇圣殿.md "wikilink")

1.  《[尧山堂外纪](../Page/尧山堂外纪.md "wikilink")》：“武庙乐以异域事为戏，又更名以从其习。学鞑靼言，则自名曰忽必列；习回回食，则自名曰沙吉熬烂；学西番刺麻僧教，则自名为太宝法王领占班丹。”

2.  《明武宗实录》：“时上巡幸所至，禁民间蓄猪，远近屠杀殆尽；田家有产者悉投诸水。”

3.  [李敖](../Page/李敖.md "wikilink")《笑傲》表示，失足落水後肺炎死亡，或者感染到水中的細菌或寄生蟲，「燥熱難退」，即高燒不退而死亡都是有可能的。

4.  《明实录·武宗毅皇帝实录》卷197·丙寅条：上崩于豹房先一夕……以朕意达
    皇太后，天下事重其与内阁辅臣议处。之前此事皆由朕而误，非汝众人所能与也……

5.  《明实录·武宗毅皇帝实录》卷1：“其生所值支辰为申酉戌亥，连如贯珠，又与 圣祖高皇帝类，莫不欣欣相贺。”

6.  [钱谦益](../Page/钱谦益.md "wikilink")：《列朝诗集·-{乾}-集·武宗毅皇帝》

7.  [美發現明朝皇帝親筆所書聖旨
    被一華人收藏多年](http://news.big5.enorth.com.cn/system/2004/12/21/000928824.shtml)

8.

9.  《[明書](../Page/明書.md "wikilink")》

10.
11. 《[胜朝彤史拾遗记](../Page/胜朝彤史拾遗记.md "wikilink")·卷四》[毛奇龄撰](../Page/毛奇龄.md "wikilink")

12.
13.
14.
15. 《明史·列传一百九十五》

16.