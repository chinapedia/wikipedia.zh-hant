**Herpa**（Herpa Miniaturmodelle GmbH）是一家德國的玩具製造商，以品牌[Herpa
Wings賣出](../Page/Herpa_Wings.md "wikilink")[合金模型](../Page/合金模型.md "wikilink")（Die-cast
models）飛機，以品牌[Herpa Cars &
Trucks賣出汽車模型](../Page/Herpa_Cars_&_Trucks.md "wikilink")。Herpa
Wings產品主要是以1/500的比例製造，但是也有1/400、1/200與1/1000等比例的。Herpa Cars &
Trucks產品主要是以1/87的比例製造，但是也有1/220與1/160等比例的。

Herpa也為愛好汽車模型與[合金模型飛機的人發行了相關的雜誌](../Page/合金模型.md "wikilink")，分別是《Der
Maßstab》與《WingsWorld》。

## 外部链接

  - [Herpa Miniaturmodelle GMBH](http://www.herpa.de/)

[Category:德國玩具製造商](../Category/德國玩具製造商.md "wikilink")
[Category:模型製造商](../Category/模型製造商.md "wikilink")