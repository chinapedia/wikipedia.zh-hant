[TBC_logo.svg](https://zh.wikipedia.org/wiki/File:TBC_logo.svg "fig:TBC_logo.svg")
[Kbs-annex.jpg](https://zh.wikipedia.org/wiki/File:Kbs-annex.jpg "fig:Kbs-annex.jpg")
**東洋廣播公司**（），簡稱**[TBC](../Page/TBC.md "wikilink")**，是[大韓民國一個已不存在的民營](../Page/大韓民國.md "wikilink")[廣播電台兼](../Page/廣播電台.md "wikilink")[電視台](../Page/電視台.md "wikilink")。設立于1964年5月9日，其电台频道于同年5月27日开播，電視頻道於同年12月7日開播，由韓國大財閥[三星集團出資設立](../Page/三星集團.md "wikilink")。因其電視頻道位于第7頻道，而被漢城（現[首爾](../Page/首爾.md "wikilink")）市民暱稱為「七番」。

## 历史

1964年5月27日，东洋广播公司的前身“汉城广播电台”开播，频率1380kc。1965年6月25日，汉城电台开播调频广播，频率89.1MHz；同年12月7日，“东洋电视台”开播。

1965年9月22日，同为三星集團旗下的《[中央日报](../Page/中央日报_\(韩国\).md "wikilink")》创刊；稍早前的8月16日，汉城电台更名为“中央广播电台”，东洋电视台更名为“中央电视台”（JBS
TV）。后因国营的[韩国放送公社](../Page/韩国放送公社.md "wikilink")（KBS）抗议，1966年8月15日，中央广播电台和中央电视台分别更名为“东洋广播电台”和“东洋电视台”，两者后合并为东洋广播公司。之后东洋广播公司又与《中央日报》合并，成立“株式会社中央日報・東洋放送”。

東洋廣播公司營運期間，與[文化廣播](../Page/文化廣播_\(韓國\).md "wikilink")（MBC）并為韓國很受歡迎的民營電視台。當時，文化廣播播出大量[美國動畫](../Page/美國動畫.md "wikilink")，東洋廣播公司就播出[日本動畫予以回擊](../Page/日本動畫.md "wikilink")。然而，由于[國軍保安司令](../Page/大韓民國國軍機務司令部.md "wikilink")[全斗焕](../Page/全斗焕.md "wikilink")（後任[韓國總統](../Page/韓國總統.md "wikilink")）為鉗制[言論自由而實施](../Page/言論自由.md "wikilink")[言論統廢合](../Page/言論統廢合.md "wikilink")，韓國的廣電[媒體生態也發生巨大改變](../Page/媒體.md "wikilink")。1980年11月30日，東洋廣播公司被關閉，其電視及廣播頻道合并入[韓國廣播公社](../Page/韓國廣播公社.md "wikilink")（KBS）。

2008年，[李明博政府通過廣播相關法令增修](../Page/李明博.md "wikilink")，開放各大報業集團經營電視業務。在《中央日報》支持下，TBC得以「復播」，新的電視台將代表《中央日報》的「J」加入名稱，成立[綜合編成頻道](../Page/綜合編成頻道.md "wikilink")[JTBC](../Page/JTBC.md "wikilink")。

2011年12月1日，JTBC開播，以東洋廣播公司繼承者自居，開播廣告詞為「復活TBC，誕生JTBC」。

## 關聯條目

  - [JTBC](../Page/JTBC.md "wikilink")
  - [東亞廣播公司](../Page/東亞廣播公司.md "wikilink")（[東亞日報系列](../Page/東亞日報.md "wikilink")，被合并入KBS）

[T東](../Category/韓國電視台.md "wikilink")
[T東](../Category/韓國廣播電台.md "wikilink")
[T東](../Category/韩国已结业公司.md "wikilink")
[T東](../Category/1964年成立的公司.md "wikilink")
[T東](../Category/1980年廢除.md "wikilink")