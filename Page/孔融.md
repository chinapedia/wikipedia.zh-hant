[Kong_Rong_Qing_Portrait.jpg](https://zh.wikipedia.org/wiki/File:Kong_Rong_Qing_Portrait.jpg "fig:Kong_Rong_Qing_Portrait.jpg")
**孔融**（），[字](../Page/表字.md "wikilink")**文举**，[东汉末](../Page/东汉.md "wikilink")[文學家](../Page/文學.md "wikilink")，[鲁国](../Page/鲁国.md "wikilink")[曲阜人](../Page/曲阜.md "wikilink")，[建安七子之一](../Page/建安七子.md "wikilink")，[孔子二十代孙](../Page/孔子.md "wikilink")，高祖父[孔尚](../Page/孔尚_\(東漢\).md "wikilink")，[鉅鹿](../Page/鉅鹿.md "wikilink")[太守](../Page/太守.md "wikilink")。父[孔宙](../Page/孔宙.md "wikilink")，[泰山](../Page/泰山.md "wikilink")[都尉](../Page/都尉.md "wikilink")。

由于曾任[北海](../Page/北海郡.md "wikilink")[相](../Page/國相_\(郡國\).md "wikilink")，亦称**孔北海**，後因恃才放曠而經常激怒[相國](../Page/相國.md "wikilink")[曹操](../Page/曹操.md "wikilink")，遭[處死和](../Page/處死.md "wikilink")[株連一家](../Page/族滅.md "wikilink")。

## 生平

### 天生神童

孔融十岁那年随父亲到达首都[洛阳](../Page/洛阳.md "wikilink")。当时，-{著名的[士大夫](../Page/士大夫.md "wikilink")}-[李膺也在京城](../Page/李膺_\(東漢\).md "wikilink")，如果不是名士或他的亲戚，守门的人一般是不通报的。孔融只有十岁，想看看李膺是个什么样的人，就登门拜访。他对守门人说：“我是李膺的亲戚。”守门人通报后，李膺接见了他。李膺问他说：“请问你和我有什么亲戚关系呢？”孔融回答道：“从前我的祖先[孔子和你家的祖先老子](../Page/孔子.md "wikilink")[李耳有师资之尊](../Page/李耳.md "wikilink")（孔子曾向老子请教过关于[周礼的问题](../Page/周礼.md "wikilink")），因此，我和你也是世交呀！”当时很多宾客都在场，对孔融的回答十分惊奇。后来[太中大夫](../Page/太中大夫.md "wikilink")[陈炜来到李膺家](../Page/陈炜_\(汉朝\).md "wikilink")，宾客把这件事情告诉他，他却不以为然地说：“小時候聰明，長大了可不一定好。”（小时了了，大未必佳。）孔融立即反驳道：“从您讲的话可以看出，想必您小时候一定是很聪明吧？”（想君小時，必當了了。）\[1\]陈韪无话可说。李膺大笑，说：“銳氣盡出，目無尊長，这么聪明的孩子将来肯定能成大器。”

### 收留張儉

[東漢末期](../Page/東漢.md "wikilink")，發生「[黨錮之禍](../Page/黨錮之禍.md "wikilink")」。在這次事件中，[張儉被通緝](../Page/張儉.md "wikilink")。孔融的哥哥[孔褒是張儉的好友](../Page/孔褒.md "wikilink")，於是張儉便投奔孔褒。不巧孔褒不在家，應門的孔融當時只有十六歲，張儉因孔融年紀太小，沒有把實情告訴他。孔融見張儉神色慌張，於是便把張儉留下。後來事情敗露，張儉逃走，孔融、孔褒卻被逮捕下獄。孔融說張儉是他留下的，他該負責；孔褒說：“彼來求我，非弟之過。”他該負責；孔母說她是家長，她該負責，鬧得“一門爭死”。後來皇帝決定由孔褒負責，將孔褒處決，孔融因此事名聲大噪。

### 京中為官

孔融由此名震遠近，與平原[陶丘洪](../Page/陶丘洪.md "wikilink")、陳留[邊讓](../Page/邊讓.md "wikilink")，並以俊秀，為後進冠蓋。孔融持論經理不及[邊讓等](../Page/邊讓.md "wikilink")，而逸才宏博過之。

辟舉[司徒](../Page/司徒.md "wikilink")[楊賜府](../Page/楊賜.md "wikilink")，彈劾貪污官僚。楊賜派孔融祝賀[何進升為](../Page/何進.md "wikilink")[大將軍](../Page/大将军_\(东亚\).md "wikilink")，不能通過，即時留下對自己的[彈劾狀辭職](../Page/彈劾.md "wikilink")\[2\]。何進忌憚其名聲，闢舉為[侍御史](../Page/侍御史.md "wikilink")，與中丞[趙捨不和](../Page/趙捨.md "wikilink")，辭官。何進再闢舉為[司空掾](../Page/司空掾.md "wikilink")，[北軍中侯](../Page/北軍.md "wikilink")。在職三日，升遷為[虎賁](../Page/虎賁.md "wikilink")[中郎將](../Page/中郎將.md "wikilink")。後[董卓廢](../Page/董卓.md "wikilink")[漢少帝](../Page/刘辩.md "wikilink")，孔融與之對答，常有匡正之言。[董卓懷恨在心](../Page/董卓.md "wikilink")，派孔融到[黃巾軍最為猖獗的](../Page/黃巾軍.md "wikilink")[北海為北海相](../Page/北海郡.md "wikilink")。時年三十八。

### 管理北海

孔融到北海後起兵講武，討伐[黃巾軍](../Page/黃巾軍.md "wikilink")[張饒](../Page/張饒.md "wikilink")，戰敗，轉移保[朱虛縣](../Page/朱虛縣.md "wikilink")。後置城邑，立學校，表顯儒術薦舉賢良[鄭玄](../Page/鄭玄.md "wikilink")、[彭璆](../Page/彭璆.md "wikilink")、[邴原等](../Page/邴原.md "wikilink")；期間為郡吏[是儀改姓](../Page/是儀.md "wikilink")\[3\]。後被[管亥所圍](../Page/管亥.md "wikilink")，遣[太史慈求救於](../Page/太史慈.md "wikilink")[平原相](../Page/平原.md "wikilink")[劉備](../Page/劉備.md "wikilink")。劉備受寵若驚，立即發兵解圍。

孔融治理北海六年，雖然有匡扶朝廷的想法，但實際上沒有甚麼政治才能，因此沒有突出的政績。[建安元年](../Page/建安_\(汉献帝\).md "wikilink")（196年），為[袁譚攻擊](../Page/袁譚.md "wikilink")，城被攻陷後逃往[東山](../Page/蒙山_\(山東\).md "wikilink")，妻兒為袁譚所虜獲。袁譚和孔融從春天打到夏天，打到孔融只剩數百名軍人，當袁譚攻打孔融的期間，孔融依舊坐在書桌旁讀書，並且和人有說有笑，好像甚麼事都沒發生一樣，直到袁譚攻陷城池當天晚上城被攻下時，孔融才逃到東山一帶。\[4\]

### 杀身之祸

[汉献帝遷都](../Page/汉献帝.md "wikilink")[许昌后](../Page/许昌.md "wikilink")，孔融任[将作大匠](../Page/将作大匠.md "wikilink")、迁少府，曾被封为[太中大夫](../Page/太中大夫.md "wikilink")。为人恃才负气，時常頂撞[曹操](../Page/曹操.md "wikilink")，如反對恢复[肉刑](../Page/肉刑.md "wikilink")、反對[曹丕私納](../Page/曹丕.md "wikilink")[袁紹兒媳婦](../Page/袁紹.md "wikilink")[甄氏](../Page/甄宓.md "wikilink")\[5\]、嘲征[乌桓](../Page/乌桓.md "wikilink")\[6\]、反對曹操[禁酒](../Page/禁酒.md "wikilink")\[7\]。再加上他忠于汉室，上奏主张“宜准古[王畿之制](../Page/王畿.md "wikilink")，千里寰内，不以封建诸侯”来增强汉室实权，此举更是严重激怒了曹操。因此，曹操使军谋祭酒[路粹为奏](../Page/路粹.md "wikilink")：“孔融昔在北海，见王室不宁，招合徒众，欲图不轨，言‘我大圣之后也，而灭於宋。有天下者，何必卯金刀？’融为九卿，不遵朝仪，秃巾微行，唐突宫掖。又与白衣祢衡言论放荡，衡与融更相赞扬。祢衡谓孔融曰：‘仲尼不死也。’孔融答曰：‘颜渊复生。’”，承指数致融罪。在建安十三年八月壬子日（公元208年9月26日）\[8\]，孔融被曹操以“招合徒众”，“欲图不轨”、“谤讪朝廷”、“不遵朝仪”等罪名杀之，株连全家。

[魏文帝曹丕深爱孔融文辞](../Page/魏文帝.md "wikilink")，把孔融与[王粲](../Page/王粲.md "wikilink")、[陈琳](../Page/陈琳_\(文学家\).md "wikilink")、[徐幹](../Page/徐幹.md "wikilink")、[阮瑀](../Page/阮瑀.md "wikilink")、[应瑒](../Page/应瑒.md "wikilink")、[刘桢六位文学家相提并论](../Page/刘桢.md "wikilink")，列为“[建安七子](../Page/建安七子.md "wikilink")”。

## 地位与影响

孔融与大学问家[蔡邕交往密切](../Page/蔡邕.md "wikilink")，有詩文互通，有名於一時。

武帝獨尊儒術後使孔家成為名門世家、而孔融本人更是當代名滿天下的名士，天之驕子般的出身讓孔融心高氣傲，常說出不得體或的話觸怒曹操，尤其是他有较大的影响力却公然反对曹操的政治举措，最终招来杀身之祸，無怪乎物以類聚而與禰衡交好。由于曹操杀孔融所加的罪名极重，甚至直到[西晋时](../Page/西晋.md "wikilink")[陈寿著写](../Page/陈寿.md "wikilink")《[三国志](../Page/三国志.md "wikilink")》时，都不敢为孔融立传，成为该史书中缺少的最著名人物之一。

由于全家被杀和曹操所加的严重罪名，孔融的作品大部分流失，后世整理的《孔北海集》残缺不全。但是孔融的言论仍然对当时和后世产生了影响。杀孔融被视为曹操殺名士的污点之一，而孔融反对[肉刑等的作品甚至到](../Page/肉刑.md "wikilink")[隋唐时期仍然被用作朝廷政策讨论的论据](../Page/隋唐.md "wikilink")。

[文化大革命期间](../Page/文化大革命.md "wikilink")[批林批孔运动中](../Page/批林批孔运动.md "wikilink")，孔融作为[孔子的二十世孙](../Page/孔子.md "wikilink")，和反对被[毛泽东欣赏的](../Page/毛泽东.md "wikilink")[曹操的典型](../Page/曹操.md "wikilink")，被“打倒”、“批判”，时隔一千七百多年又成为了政治敏感话题之一。直到文革结束后，中国大陆对于孔融的评价才恢复到对于其文学才能和政治品格的正面欣赏与评价上。

## 家庭

  - 七世祖 [孔霸](../Page/孔霸.md "wikilink")，汉元帝的老师，位至侍中
  - 高祖父 [孔尚](../Page/孔尚_\(东汉\).md "wikilink")，钜鹿太守
  - 父亲 [孔宙](../Page/孔宙.md "wikilink")，泰山都尉
  - 兄长，[孔褒](../Page/孔褒.md "wikilink")、[孔谦](../Page/孔谦_\(东汉\).md "wikilink")
  - 子女，据《后汉书·郑孔荀列传》，孔融有一子一女（兄九歲妹七歲）在孔融被捕后被曹操所杀\[9\]，而在《世说新语·言语第二》中则记载是两个儿子。孔融另有一女嫁给了[羊衜](../Page/羊衜_\(曹魏\).md "wikilink")，有子[羊发](../Page/羊发.md "wikilink")，羊发的异母弟是[羊祜](../Page/羊祜.md "wikilink")。

## 部下

  - [王修](../Page/王修.md "wikilink")，孔融推舉為[孝廉](../Page/孝廉.md "wikilink")，做過孔融[主簿](../Page/主簿.md "wikilink")，屢屢救援被賊人所攻的孔融。
  - [邴原](../Page/邴原.md "wikilink")，孔融推舉為有道，做為孔融[計佐](../Page/計佐.md "wikilink")，因為黃巾賊亂而避難到遼東。
  - [是儀](../Page/是儀.md "wikilink")，早年在孔融下任官，本姓「氏」，因為被孔融嘲笑「氏」字是「民」字無上，所以改姓「是」，後來因賊亂避難到江東。
  - [太史慈](../Page/太史慈.md "wikilink")，為孔融提拔，曾被派往向劉備請求救兵，後來跟隨[劉繇](../Page/劉繇.md "wikilink")。後投靠[孫策](../Page/孫策.md "wikilink")。於[赤壁之戰前病逝](../Page/赤壁之戰.md "wikilink")，死時四十一歲。
  - [王子法](../Page/王子法.md "wikilink")、[劉孔慈](../Page/劉孔慈.md "wikilink")，孔融心腹，《[九州春秋](../Page/九州春秋.md "wikilink")》評其為“凶辨小才”。
  - [左丞祖](../Page/左丞祖.md "wikilink")，孔融幕僚，建議孔融在北方曹操，袁紹實力強大下選擇歸附一方，孔融認為曹袁等人並非真心匡扶漢室，而殺了左丞祖。
  - [劉義遜](../Page/劉義遜.md "wikilink")，孔融幕僚，與左丞祖同為當時清雋之士，卻備在坐席不受重視，見融殺害左丞祖，棄而離去。
  - [禰衡](../Page/禰衡.md "wikilink")，孔融向曹操提拔，但為曹操所不喜。
  - [孫邵](../Page/孫邵.md "wikilink")，字长绪，孔融手下任功曹，後來跟隨[劉繇](../Page/劉繇.md "wikilink")，之後成為[孫策](../Page/孫策.md "wikilink")、[孫權的重要謀士](../Page/孫權.md "wikilink")，任吳國首任丞相。
  - [武安國](../Page/武安國.md "wikilink")，[三國演義虛構人物](../Page/三國演義.md "wikilink")，孔融部下，曾向[呂布挑戰](../Page/呂布.md "wikilink")，被呂布砍斷一條手臂后被救回，之後下落不明。
  - [宗寶](../Page/宗寶.md "wikilink")，三國演義虛構人物，孔融部下武將，管亥攻擊北海時出戰，被管亥數回合斬殺。

## 相关典故

### 孔融讓梨

根據一些後世流傳的文獻紀載，孔融在四歲時就懂得讓梨，這說法目前可見的最早出處，出自唐朝[李賢註解](../Page/李賢.md "wikilink")《[後漢書](../Page/後漢書.md "wikilink")·孔融傳》時，自《融家傳》當中所引用的一段話。\[10\]這故事亦為後世道德教材所引用，像是《[三字经](../Page/三字经.md "wikilink")》中就有“融四岁，能让梨”，指孔融小时候曾把大个的梨让给哥哥吃的故事，教育小孩要懂得禮讓。

### 以今度之，想當然耳

曹操滅袁紹，以袁紹次子袁熙之妻甄宓賜予曹丕，孔融便赴書曹操云：「武討伐紂，以妲己賜周公。」曹操看過一驚，便問出於何典故，孔融遂應答：「以今度之，想當然耳。」以諷刺曹操。

**衍生：**後北宋文豪[蘇軾曾於科考上](../Page/蘇軾.md "wikilink")〈刑賞忠厚之至論〉一文，其中內容：「皋陶曰殺之三，堯曰宥之三」，當時考官皆不知其典故，歐陽脩問蘇軾出於何典。蘇軾回答在《三國志·孔融傳》中。歐陽脩翻查後仍找不到，蘇軾答：「曹操滅袁紹，以紹子袁熙妻甄宓賜子曹丕。孔融云：『即周武王伐紂以妲己賜周公』。操驚，問出於何典，融答：『以今度之，想當然耳』。」歐陽脩聽畢恍然大悟。

### 覆巢之下無完卵

曹操派人捉捕孔融全家时，孔融問使者，希望能夠保全兩個幼子，但是其中一个说了一句名言：“岂见覆巢之下，复有完卵乎？”最后孔融全家被杀。（世說新語／言語第二）

### 歲月不居，時節如流

形容時間過得很快。\[11\]

## 艺术形象

### 三国演义

形象与正史相似。为孔子二十世孙，最初任北海太守，十八路诸侯起兵讨董卓，孔融为第十镇。后黄巾贼管亥率兵数万围北海，孔融与之战不利，幸得太史慈求救于平原相刘备。刘备亲自引兵三千来救，管亥退走。曹操攻陶谦，孔融与刘备等引兵援救。后召还为太中大夫，曹操征荆州时，孔融劝阻，被曹操叱退。孔融口出怨言，为郗虑宾客所听闻。郗虑素来与孔融不睦，于是将此事告诉曹操，孔融被下狱弃市。妻子都被诛杀。好友脂习冒险为其收尸。

### 影视形象

  - 1994年電視劇《[三國演義](../Page/三国演义_\(电视剧\).md "wikilink")》：[郑榕](../Page/郑榕.md "wikilink")
  - 1994年電視劇《[东方小故事](../Page/东方小故事.md "wikilink")》：[杨斌妮](../Page/杨斌妮.md "wikilink")
  - 2002年電視劇《[洛神](../Page/洛神_\(2002年電視劇\).md "wikilink")》：[李鸿杰](../Page/李鸿杰.md "wikilink")
  - 2004年電視劇《[武圣关公](../Page/武圣关公.md "wikilink")》：[李元彪](../Page/李元彪.md "wikilink")
  - 2008年、2009年電影《[赤壁](../Page/赤壁_\(電影\).md "wikilink")》、《[赤壁：決戰天下](../Page/赤壁_\(電影\).md "wikilink")》：[王庆祥](../Page/王庆祥.md "wikilink")
  - 2014年電視劇《[曹操](../Page/曹操_\(电视剧\).md "wikilink")》：[古巨基](../Page/古巨基.md "wikilink")

### 漫畫形象

  - 《[蒼天航路](../Page/蒼天航路.md "wikilink")》（[王欣太](../Page/王欣太.md "wikilink")）
  - 《[火鳳燎原](../Page/火鳳燎原.md "wikilink")》（[陳某](../Page/陳某.md "wikilink")）

## 评价

  - [李膺](../Page/李膺.md "wikilink")：“高明必为伟器。”（《后汉书·卷七十·郑孔荀列传第六十》）
  - [陈登](../Page/陈登.md "wikilink")：“博闻强记，奇逸卓荦，吾敬孔文举。”（《三国志·卷二十二·魏书二十二·桓二陈徐卫卢传第二十二》）
  - [祢衡](../Page/祢衡.md "wikilink")：“大儿孔文举，小儿[杨德祖](../Page/杨德祖.md "wikilink")，余子碌碌，莫足数也。”
  - [曹操](../Page/曹操.md "wikilink")：“世人多采其虚名，少於核实，见融浮艳，好作变异，眩其诳诈，不复察其乱俗也。”
  - [曹丕](../Page/曹丕.md "wikilink")：“今之文人，鲁国孔融文举，广陵陈琳孔璋，山阳王粲仲宣，北海徐干伟长，陈留阮瑀元瑜，东平刘桢公干，斯七子者，于学无所遗，于辞无所假，咸以自骋骥騄于千里，仰齐足而并驰。以此相服，亦良难矣。”
  - [司马彪](../Page/司马彪.md "wikilink")：“融在北海，自以智能优赡，溢才命世，当时豪俊皆不能及。亦自许大志，且欲举军曜甲，与群贤要功，自於海岱结殖根本，不肯碌碌如平居郡守，事方伯、赴期会而已。然其所任用，好奇取异，皆轻剽之才。至于稽古之士，谬为恭敬，礼之虽备，不与论国事也。高密郑玄，称之郑公，执子孙礼。及高谈教令，盈溢官曹，辞气温雅，可玩而诵。论事考实，难可悉行。但能张磔网罗，其自理甚疏。租赋少稽，一朝杀五部督邮。奸民污吏，猾乱朝市，亦不能治。”（《三国志·魏书十二》引自《九州春秋》）
  - [张璠](../Page/张璠.md "wikilink")：“融在郡八年，仅以身免。帝初都许，融以为宜略依旧制，定王畿，正司隶所部为千里之封，乃引公卿上书言其义。是时天下草创，曹、袁之权未分，融所建明，不识时务。又天性气爽，颇推平生之意，狎侮太祖。”
  - [葛洪](../Page/葛洪.md "wikilink")：“孔融、边让文学邈俗，而并不达治务，所在败绩。”
  - [范晔昔谏大夫郑是有言](../Page/范晔.md "wikilink")：“山有猛兽者，藜藿为之不采。是以孔父正色，不容弑虐之谋；平仲立朝，有纾盗齐之望。若夫文举之高志直情，其足以动义概而忤雄心。故使移鼎之迹，事隔于人存；代终之规，启机于身后也。夫严气正性，覆折而己。岂有员园委屈，可以每其生哉！懔懔焉，皓皓焉，其与琨玉秋霜比质可也。”“北海天逸，音情顿挫。越俗易惊，孤音少和。直辔安归，高谋谁佐？”（《后汉书·卷七十·郑孔荀列传第六十》）
  - [刘勰](../Page/刘勰.md "wikilink")：“孔融守北海，文教丽而罕于理。”（《[文心雕龙](../Page/文心雕龙.md "wikilink")》评论孔融和王粲的得与失）
  - [房玄龄](../Page/房玄龄.md "wikilink")：“逮乎当涂基命，文宗蔚起，三祖叶其高韵，七子分其丽则，《翰林》总其菁华，《典论》详其藻绚，彬蔚之美，竞爽当年。独彼陈王，思风遒举，备乎典奥，悬诸日月。”
  - [司马光](../Page/司马光.md "wikilink")：“岁其高气，志在靖难，而才疏意广，高谈清教，盈溢官曹，辞气清雅，可玩可诵，论事考实，难要悉行。但能张桀网罗，而目理甚疏；造次能得人心，久久亦不愿附也。其所任用，好奇取异，多剽轻小才。”（《资治通鉴·汉纪·献帝建安元年》）
  - [苏轼](../Page/苏轼.md "wikilink")：“文举以英伟冠世之资，师表海内，意所予夺，天下从之，此人中龙也。”“晋有羯奴，盗贼之靡。欺孤如操，又羯所耻。我书《春秋》，与齐豹齿。文举在天，虽亡不死。我宗若人，尚友千祀。视公如龙，视操如鬼。”（《苏东坡全集·卷九十四》）“[汉景帝以鞅鞅而杀](../Page/汉景帝.md "wikilink")[周亚夫](../Page/周亚夫.md "wikilink")，曹操以名重而杀孔融，[晋文帝以卧龙而杀](../Page/晋文帝.md "wikilink")[嵇康](../Page/嵇康.md "wikilink")，[晋景帝亦以名重而杀](../Page/晋景帝.md "wikilink")[夏侯玄](../Page/夏侯玄.md "wikilink")，[宋明帝以族大而杀](../Page/宋明帝.md "wikilink")[王彧](../Page/王彧.md "wikilink")，[齐后主以谣言而杀](../Page/齐后主.md "wikilink")[斛律光](../Page/斛律光.md "wikilink")，[唐太宗以谶而杀](../Page/唐太宗.md "wikilink")[李君羡](../Page/李君羡.md "wikilink")，[武后以谣言而杀](../Page/武后.md "wikilink")[裴炎](../Page/裴炎.md "wikilink")，世皆以为非也。”（《东坡志林·卷五》）
  - [陈普](../Page/陈普.md "wikilink")：“一身撑拄汉乾坤，无那危时喜放言。不受祢衡轻薄误，未容曹操驾金根。”（《诗·陈普诗选》）
  - [徐钧](../Page/徐钧.md "wikilink")：“客满尊中酒不空，眼高四海眇奸雄。才疏意广终无就，已兆清虚西晋风。”（《诗·徐钧诗选》）
  - [杨维桢](../Page/杨维桢.md "wikilink")：“孔北海，辟一隅。座客日高谈，论未闻，谈庙谟。弥生本独士，罪书空累予。在生承祖亦清俊，献策未为疏。言不听，何取诛？孔北海，儒亦迂。郑玄何取尊乡闾，妻孥已属青州牧，流矢几前犹读书。”（《铁崖乐府》）
  - [罗贯中](../Page/罗贯中.md "wikilink")：“孔融居北海，豪气贯长虹：坐上客长满，樽中酒不空；文章惊世俗，谈笑侮王公。史笔褒忠直，存官纪太中。”
  - [张溥](../Page/张溥.md "wikilink")：“汉末名人，文有孔融，武有吕布，孟德实兼其长。”
  - [李贽](../Page/李贽.md "wikilink")：“北海大志直节，东汉名流，而与“[建安七子](../Page/建安七子.md "wikilink")”并称；[骆宾王劲辞忠愤](../Page/骆宾王.md "wikilink")，唐之义士，而与“垂拱四杰”为列。以文章之末技而掩其立身之大闲，可惜也！”

## 参考资料

## 參考文獻

  - 《[後漢書](../Page/後漢書.md "wikilink")·[孔融傳](../Page/s:後漢書/卷70.md "wikilink")》
  - 《[三國志](../Page/三國志.md "wikilink")·[崔琰傳](../Page/s:三國志/卷12.md "wikilink")》[裴注](../Page/裴注.md "wikilink")
  - 《[世说新语](../Page/世说新语.md "wikilink")》

{{-}}   [K](../Page/category:三國被處決者.md "wikilink")

[K](../Category/党锢之祸.md "wikilink") [K](../Category/东汉官员.md "wikilink")
[K](../Category/建安七子.md "wikilink") [K](../Category/曲阜人.md "wikilink")
[R融](../Category/孔姓.md "wikilink") [R](../Category/孔子二十代孫.md "wikilink")

1.  《[世說新語](../Page/世說新語.md "wikilink")》言語第二
2.  《後漢書》：「辟司徒楊賜府。時隱核官僚之貪濁者，將加貶黜，融多舉中官親族。尚書畏迫內寵，召掾屬詰責之。融陳對罪惡，言無阿撓。河南尹何進當遷為大將軍，楊賜遣融奉謁賀進，不時通，融即奪謁還府，投劾而去。」注意「投劾而去」指的是留下對自己的彈劾狀籍此辭官，而非彈劾其他的官員
3.  《三國志·是儀傳》：是儀字子羽，北海營陵人也。本姓氏，初為縣吏，後仕郡。**郡相孔融**嘲儀，言"氏"字"民"無上，可改為"是"，乃遂改焉。
4.  《後漢書·卷七十》：「融負其高氣，志在靖難，而才疏意廣，迄無成功。在郡六年，劉備表領青州刺史。建安元年，為袁譚所攻，自春至夏，戰士所餘裁數百人，流矢雨集，戈矛內接。融隱幾讀書，談笑自若。城夜陷，乃奔東山，妻子為譚所虜。」
5.  《后汉书·孔融传》：曹操攻屠鄴城，袁氏妇子多见侵略，而操子丕私纳袁熙妻甄氏。融乃与操书，称“**武王伐纣，以妲己赐周公**”。操不悟，后问出何经典。对曰：“**以今度之，想当然耳。**”
6.  《后汉书·孔融传》：后操讨乌桓，又嘲之曰：“**大将军远征，萧条海外。昔肃慎不贡楛矢，丁零盗苏武牛羊，可并案也。**”
7.  孔融《难曹公表制禁酒书》：“**天有酒旗之星，地列酒泉之郡，人有旨酒之德，故尧不饮千钟，无以成其圣。……夏、商亦以妇人失天下，今令不断婚姻。**”翻译成白话文是说：“天上有酒旗星，地下有酒泉郡，人有海量称酒德，帝尧饮千锺酒被称为圣人。……况且夏桀和商纣王是因为女色亡的国，那应该下令把婚姻也禁止。”
8.  袁宏《後漢紀·卷30》
9.  女年七歲，男年九歲，以其幼弱得全，寄它舍。二子方弈棋，融被收而不動。左右曰：“父執而不起，何也？”答曰：“安有巢毀而卵不破乎！”主人有遺肉汁，男渴而飲之。女曰：“今日之禍，豈得久活，何賴知肉味乎？”兄號泣而止。或言於曹操，遂盡殺之。及收至，謂兄曰：“若死者有知，得見父母，豈非至願！”乃延頸就刑，顏色不變，莫不傷之。（《後漢書·孔融傳》）
10. 《世說新語箋疏》引《續漢書》曰：“孔融，字文舉，魯國人，孔子二十四世孫也。高祖父尚，鉅鹿太守。父宙，泰山都尉。”《融別傳》曰：融四歲，與兄食梨，輒引小者。人問其故。答曰：“小兒，法當取小者。”《[後漢書](../Page/後漢書.md "wikilink")·孔融傳》[李賢注引](../Page/李賢.md "wikilink")“《融家傳》曰：‘年四歲時，與諸兄共食梨，融輒引小者。’大人問其故，答曰：‘我小兒，法當取小者。’由是宗族奇之。”
11. [孔融](../Page/孔融.md "wikilink")《[論盛孝章書](../Page/s:論盛孝章書.md "wikilink")》，收錄於《文選》第四十一卷中。