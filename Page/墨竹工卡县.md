**墨竹工卡县**（）是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[西藏自治区](../Page/西藏自治区.md "wikilink")[拉萨市下属的一个](../Page/拉萨市.md "wikilink")[县](../Page/县级行政区.md "wikilink")。[藏语中意思是](../Page/藏语.md "wikilink")“墨竹色青龙王居住的中间白地”。位于[拉萨河中上游](../Page/拉萨河.md "wikilink")。县政府驻工卡镇（）。

## 行政区划

下辖：\[1\] 。

## 参考文献

{{-}}

[Category:拉萨县份](../Category/拉萨县份.md "wikilink")
[墨竹工卡县](../Category/墨竹工卡县.md "wikilink")
[Category:国家级贫困县](../Category/国家级贫困县.md "wikilink")

1.