**汉斯·阿尔布雷希特·贝特**（，），[德国和](../Page/德国.md "wikilink")[美国](../Page/美国.md "wikilink")[犹太裔核](../Page/犹太裔.md "wikilink")[物理学家](../Page/物理学家.md "wikilink")，对于[天体物理学](../Page/天体物理学.md "wikilink")，[量子电动力学和](../Page/量子电动力学.md "wikilink")[固体物理学有很重要的贡献](../Page/固体物理学.md "wikilink")。由于[恆星核合成理论研究成果](../Page/恆星核合成.md "wikilink")，他荣获了1967年[诺贝尔物理学奖](../Page/诺贝尔物理学奖.md "wikilink")\[1\]\[2\]。

## 早年

贝特于1906年出生于[德国的](../Page/德国.md "wikilink")[斯特拉斯堡](../Page/斯特拉斯堡.md "wikilink")（二战后划归法国至今）。1924年，他进入[法兰克福大学接受教育](../Page/法兰克福大学.md "wikilink")。他后在[慕尼黑大学师从物理学家](../Page/慕尼黑大学.md "wikilink")[阿諾·索末菲学习](../Page/阿諾·索末菲.md "wikilink")[理论物理学](../Page/理论物理学.md "wikilink")。

## 工作与成就

1935年，贝特为逃避[纳粹迫害](../Page/纳粹.md "wikilink")，离开欧洲来到[美国](../Page/美国.md "wikilink")。1938年，贝特因凭借其[恆星核合成理论解释了为什么](../Page/恆星核合成.md "wikilink")[恒星能够在长时间里持续向外释放大量能量而获得](../Page/恒星.md "wikilink")[诺贝尔物理学奖](../Page/诺贝尔物理学奖.md "wikilink")。

[二战期间](../Page/二战.md "wikilink")，贝特受[罗伯特·奥本海默之命负责](../Page/罗伯特·奥本海默.md "wikilink")[原子弹研发的理论物理研究](../Page/原子弹.md "wikilink")，作出了重要贡献。期间他还提拔了年轻人[理查德·费曼](../Page/理查德·费曼.md "wikilink")。有一个计算[核武器效率的公式后就叫做](../Page/核武器.md "wikilink")。贝特晚年还研究了[太阳](../Page/太阳.md "wikilink")[中微子失踪问题](../Page/中微子.md "wikilink")。他因对问题采取刨根问底的研究方法，有“战舰”的昵称。

## 逸闻

  - 《[费曼物理学讲义](../Page/费曼物理学讲义.md "wikilink")》曾提到最早发现太阳的能量来自于核聚变的人(此君就是汉斯·贝特\[3\])，在取得这一发现的当天晚上和女朋友一起出去散步。女生说：“天上的星星好美呀！”贝特说：“是的，然而现在我是世上唯一知道它们为什么发光的人。”女生只是嘲笑了他，很显然并没有因为和世界上唯一知道星星为什么发光的人外出散步而激动。[费曼没有直接说出这位老朋友的名字](../Page/理查德·费曼.md "wikilink")，只是在讲完之后点评说：“的确，孤单是可悲的，不过在这个世界上就是这个样子。”\[4\]

## 传记

  -
  -
## 参考资料

## 外部链接

  -
[Category:诺贝尔物理学奖获得者](../Category/诺贝尔物理学奖获得者.md "wikilink")
[Category:美國諾貝爾獎獲得者](../Category/美國諾貝爾獎獲得者.md "wikilink")
[Category:猶太諾貝爾獎獲得者](../Category/猶太諾貝爾獎獲得者.md "wikilink")
[Category:美國國家科學獎獲獎者](../Category/美國國家科學獎獲獎者.md "wikilink")
[Category:恩里科·費米獎獲獎者](../Category/恩里科·費米獎獲獎者.md "wikilink")
[Category:英國皇家學會院士](../Category/英國皇家學會院士.md "wikilink")
[Category:曼哈頓計畫人物](../Category/曼哈頓計畫人物.md "wikilink")
[Category:美国物理学家](../Category/美国物理学家.md "wikilink")
[Category:美國核物理學家](../Category/美國核物理學家.md "wikilink")
[Category:德國物理學家](../Category/德國物理學家.md "wikilink")
[Category:德國核物理學家](../Category/德國核物理學家.md "wikilink")
[Category:理論物理學家](../Category/理論物理學家.md "wikilink")
[Category:猶太科學家](../Category/猶太科學家.md "wikilink")
[Category:康乃爾大學教師](../Category/康乃爾大學教師.md "wikilink")
[Category:杜克大学教师](../Category/杜克大学教师.md "wikilink")
[Category:蒂賓根大學教師](../Category/蒂賓根大學教師.md "wikilink")
[Category:慕尼黑大學校友](../Category/慕尼黑大學校友.md "wikilink")
[Category:法蘭克福大學校友](../Category/法蘭克福大學校友.md "wikilink")
[Category:德國裔美國人](../Category/德國裔美國人.md "wikilink")
[Category:歸化美國公民的德國人](../Category/歸化美國公民的德國人.md "wikilink")
[Category:移民美國的德國人](../Category/移民美國的德國人.md "wikilink")
[Category:美國猶太人](../Category/美國猶太人.md "wikilink")
[Category:德國猶太人](../Category/德國猶太人.md "wikilink")
[Category:斯特拉斯堡人](../Category/斯特拉斯堡人.md "wikilink")
[Category:爱丁顿奖章获得者](../Category/爱丁顿奖章获得者.md "wikilink")
[Category:亨利·德雷伯奖章获得者](../Category/亨利·德雷伯奖章获得者.md "wikilink")
[Category:马克斯·普朗克奖章获得者](../Category/马克斯·普朗克奖章获得者.md "wikilink")
[Category:万尼瓦尔·布什奖获得者](../Category/万尼瓦尔·布什奖获得者.md "wikilink")
[Category:阿尔伯特·爱因斯坦和平奖获得者](../Category/阿尔伯特·爱因斯坦和平奖获得者.md "wikilink")
[Category:奥斯特奖章获得者](../Category/奥斯特奖章获得者.md "wikilink")
[Category:富兰克林奖章获得者](../Category/富兰克林奖章获得者.md "wikilink")
[Category:罗蒙诺索夫金质奖章获得者](../Category/罗蒙诺索夫金质奖章获得者.md "wikilink")
[Category:奥斯卡·克莱因纪念讲座](../Category/奥斯卡·克莱因纪念讲座.md "wikilink")
[Category:尼尔斯·玻尔奖章获得者](../Category/尼尔斯·玻尔奖章获得者.md "wikilink")
[Category:拉姆福德奖获得者](../Category/拉姆福德奖获得者.md "wikilink")

1.
2.
3.
4.