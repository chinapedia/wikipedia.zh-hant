**赫伯特·乔治·威尔斯**（，），通称**H·G·威尔斯**（）\[1\]，[英国著名](../Page/英国.md "wikilink")[小说家](../Page/小说家.md "wikilink")，新闻[记者](../Page/记者.md "wikilink")、[政治家](../Page/政治家.md "wikilink")、[社会学家和](../Page/社会学家.md "wikilink")[历史学家](../Page/历史学家.md "wikilink")。他创作的[科幻小说对该领域影响深远](../Page/科幻小说.md "wikilink")，如“[时间旅行](../Page/时间旅行.md "wikilink")”、“[外星人入侵](../Page/外星人入侵.md "wikilink")”、“[反乌托邦](../Page/反乌托邦.md "wikilink")”等都是20世纪科幻小说中的主流话题。

## 早年生活

威尔斯1866年出生于[肯特郡的](../Page/肯特郡.md "wikilink")[布朗利一个贫寒的家庭](../Page/布朗利.md "wikilink")。父母都做过[仆人](../Page/仆人.md "wikilink")。父亲约瑟夫曾经是职业[板球运动员](../Page/板球.md "wikilink")，后来经营一家[五金店铺](../Page/五金.md "wikilink")。母亲尼尔则一直给有钱人家做仆人。

1880年，由于父亲的店铺倒闭，威尔斯只好辍学到[温泽的一家布店做学徒](../Page/温泽.md "wikilink")。但是他在这里的工作没有获得店主的满意，1个月以后他就不得已而离开，到[薩默塞特郡当了很短一段时间的小学教师](../Page/薩默塞特郡.md "wikilink")。后来还在[苏塞克斯郡的一个小镇上给一个](../Page/苏塞克斯郡.md "wikilink")[药剂师当助手](../Page/药剂师.md "wikilink")。1881年4月，他又来到[朴次茅斯的一个布店作了两年学徒](../Page/朴次茅斯.md "wikilink")。令人无法忍受的学徒生活迫使他最终离去，在苏塞克斯郡的一家文法学校得到一个助教职位。

1884年他得到助学金（每星期一个[基尼](../Page/基尼.md "wikilink")），进入了[英国皇家科学院的前身堪津顿科学师范学校](../Page/英国皇家科学院.md "wikilink")。他在这里学习[物理学](../Page/物理学.md "wikilink")、[化学](../Page/化学.md "wikilink")、[地质学](../Page/地质学.md "wikilink")、[天文学和](../Page/天文学.md "wikilink")[生物学](../Page/生物学.md "wikilink")。其中他的生物学老师是著名的[进化论科学家](../Page/进化论.md "wikilink")[托马斯·赫胥黎](../Page/托马斯·赫胥黎.md "wikilink")，他后来的科幻小说写作受赫胥黎的进化论思想影响很大。1890年他以[动物学的优异成绩获得了](../Page/动物学.md "wikilink")[伦敦大学](../Page/伦敦大学.md "wikilink")[帝国理工学院的](../Page/帝国理工学院.md "wikilink")[理学学士学位](../Page/理学学士.md "wikilink")。从1891年到1893年在伦敦大学函授学院教授生物学。

## 创作生涯

从1891年开始，威尔斯为一些报刊撰写文章。1893年患上了[肺出血](../Page/肺出血.md "wikilink")，休养期间，开始写作[短篇小说](../Page/短篇小说.md "wikilink")、[散文和](../Page/散文.md "wikilink")[评论](../Page/评论.md "wikilink")，同时也开始了科普创作，例如在《百万年的人》（The
Man of the Year
Million）中他大胆设想在[自然选择影响下未来人类的形象](../Page/自然选择.md "wikilink")，巨大的眼睛，细长的手。

随后《[全国观察家](../Page/全国观察家.md "wikilink")》发表了威尔斯关于[时间旅行的设想的连载文章](../Page/时间旅行.md "wikilink")，后来在1895年把这些文章改为《[時間機器](../Page/时间机器_\(小說\).md "wikilink")》的[小说发行](../Page/小说.md "wikilink")。此书的出版引起轰动，也奠定了他作为科幻小说作家的声誉。

此后，他又陆续发表了《[莫洛博士岛](../Page/莫洛博士岛.md "wikilink")》、《[隐身人](../Page/隱形人_\(小說\).md "wikilink")》、《[世界大战](../Page/世界大战_\(小说\).md "wikilink")》、《神的食物》等科幻小说，还写了大量的[论文和](../Page/论文.md "wikilink")[长篇小说](../Page/长篇小说.md "wikilink")。

20世纪以后，除了科幻小说以外，威尔斯还从幽默小说《[爱情和鲁雅轩](../Page/爱情和鲁雅轩.md "wikilink")》开始，创作了一系列以《》为代表的反映英国中下层社会的写实小说，但是知名度不如他所写的科幻小说。

## 社会活动

少年时学徒的经历，使威尔斯形成了一种批判[资本主义社会的意识](../Page/资本主义.md "wikilink")，并且始终贯穿于他的一生。

他接受了[空想社会主义的思想](../Page/空想社会主义.md "wikilink")，他自称“从学生时代起就是一个社会主义者”。他的科幻小说创作，也是他试图通过教育和科学技术改造社会的一种尝试和努力。但是他并不信仰[马克思主义](../Page/马克思主义.md "wikilink")，而是热衷于[改良主义](../Page/改良主义.md "wikilink")，他称自己是一个“保守的社会主义者”。他不赞成[阶级斗争和暴力](../Page/阶级斗争.md "wikilink")[革命](../Page/革命.md "wikilink")，但是认为有必要消灭资本主义社会里的无政府状态。

1903年，威尔斯成为标榜改良主义的社会主义团体[费边社社员](../Page/费边社.md "wikilink")。对于费边社温和的、改良主义的社会主义思想,他仍然认为有些过于激进。而他对年轻成员的影响和个人领袖欲的膨胀，使他和费边社的领导成员[肖伯纳等发生不合](../Page/肖伯纳.md "wikilink")，最后退出了这个组织。他的长篇小说《》和《》反映的就是他在这段时期的生活经验。

[第一次世界大战后](../Page/第一次世界大战.md "wikilink")，他用了1年时间完成了100多万字的《[世界史纲](../Page/世界史纲.md "wikilink")》，这本著作展现了他作为历史学家的一面。

威尔斯还在1920年和1934年访问[苏联](../Page/苏联.md "wikilink")，受到了[列宁和](../Page/列宁.md "wikilink")[斯大林的接见](../Page/斯大林.md "wikilink")。他虽然不大理解苏联的社会主义制度，但是仍然作了比较真实的报道。这在当时是很少见的。

## 晚年

[缩略图](https://zh.wikipedia.org/wiki/File:Herbert_George_Wells_in_1943.jpg "fig:缩略图")
1920年代以后，威尔斯转向政论性小说创作，借科幻小说的形式，来宣传他的改革理想，但整体上被认为缺乏艺术特色。他晚年的作品转向了[灵魂](../Page/灵魂.md "wikilink")、[宗教](../Page/宗教.md "wikilink")、[道德等方面](../Page/道德.md "wikilink")。

1946年威尔斯在[伦敦去世](../Page/伦敦.md "wikilink")。

## 作品风格

威尔斯善于把科学知识通俗化，并通过小说将其突出出来，正是这种才能使他的科幻小说深受读者欢迎。

他的科幻小说常常具有讽刺性，而显现威尔斯一贯的对资本主义的批判意识，而且这也成为了威尔斯独特的写作风格。

威尔斯的科幻小说以[軟科幻为主](../Page/軟科幻.md "wikilink")，主要描写各种先进的科学技术对未来世界的影响，以及这些科学技术所带来的社会问题，政治冲突也就成为了他的小说中的一个重要方面。他的作品也展现了未来科技发展的各种可能性，在他的作品中科技不仅给人类带来了便利，也同时产生反作用，他认为科学并不一定是人类的伙伴。在他的作品中充满了科学技术给人类带来的威胁：外星人入侵，社会暴政、[战争](../Page/战争.md "wikilink")、人种变异、[太阳消亡](../Page/太阳.md "wikilink")。

威尔斯的创作方法对当时及后世英国和世界科学幻想小说的发展有重要影响。他的《》开创了科幻小说中重要的一支血脉：“[反乌托邦](../Page/反乌托邦.md "wikilink")”小说。后来俄罗斯（苏联）作家[扎米亚京的](../Page/扎米亚京.md "wikilink")《[我们](../Page/我们_\(小说\).md "wikilink")》、英国[赫胥黎的](../Page/赫胥黎.md "wikilink")《[美丽新世界](../Page/美丽新世界.md "wikilink")》，还有[乔治·奥威尔的](../Page/乔治·奥威尔.md "wikilink")《[一九八四](../Page/一九八四.md "wikilink")》都继承了这一传统。他在许多小说中对“大脑袋”的外星人的描述成了科幻小说中历来对外星人的“标准形象”。

他的科幻小说也遭到了一些科幻小说家的批评，他们认为应该把更多的信念放在人类的灵魂和精神之上。

## 主要作品

[缩略图](https://zh.wikipedia.org/wiki/File:Time_Machine_title_page.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Amazing_stories_192708.jpg "fig:缩略图")

  - 《[时间机器](../Page/時間機器_\(小說\).md "wikilink")》（ 1895年）
  - 《[莫洛博士岛](../Page/莫洛博士岛.md "wikilink")》（1896年）
  - 《[隐形人](../Page/隐形人.md "wikilink")》（1897年）
  - 《[星際戰爭](../Page/星際戰爭.md "wikilink")》（1898年）
  - 《》（1899年）
  - 《》（1900年）
  - 《[最早登上月球的人](../Page/最早登上月球的人.md "wikilink")》（1901年）
  - 《》（1904年）
  - 《》（1908年）
  - 《》（1909年）
  - 《》（1909年）
  - 《》（1910年）
  - 《》（1911年）
  - 《》（1914年）
  - 《[世界史纲](../Page/世界史纲.md "wikilink")》（1920年）
  - 《》 （1933年）

## 影響

威尔斯曾被提名1921年、1932年、1935年和1946年的[諾貝爾文學獎](../Page/諾貝爾文學獎.md "wikilink")\[2\]。

2009年9月，[Google網站特意在其首頁標誌換上](../Page/Google.md "wikilink")[飛碟圖案及一組指向](../Page/不明飞行物.md "wikilink")《世界大戰》中外星人登陸地點的[經緯度座標](../Page/經緯度.md "wikilink")，以紀念威爾斯143歲冥誕\[3\]。

## 參考文獻

1.  *艾德译，赫伯特·乔治·威尔斯，《[世界大战](../Page/世界大战_\(小说\).md "wikilink")》，中国戏剧出版社，1999年，ISBN
    7-104-00977-9*

## 外部链接

  -
  - [威尔斯传记](http://library.thinkquest.org/27864/data/wells/hgwbio.html?tqskip1=1&tqtime=0806)

  - [非常详尽的关于威尔斯的生活和作品](https://web.archive.org/web/20021127115354/http://www.geocities.com/originalroman/)

  - [搜狐分类目录中的关于赫伯特·乔治·威尔斯的中文网站](https://web.archive.org/web/20050418193612/http://dir.sogou.com/c006/c006010002037010.html)

  -
  -
  -
  -
  -
  -
  -
  -
  -
[W](../Category/英国作家.md "wikilink")
[W](../Category/科幻小說作家.md "wikilink")
[W](../Category/英国记者.md "wikilink")
[W](../Category/英國政治人物.md "wikilink")
[W](../Category/英国社会学家.md "wikilink")
[W](../Category/英国历史学家.md "wikilink")
[W](../Category/素食主義者.md "wikilink")
[Category:倫敦帝國學院校友](../Category/倫敦帝國學院校友.md "wikilink")
[Category:费边社成员](../Category/费边社成员.md "wikilink")

1.  ["Wells, H. G."](http://www.sf-encyclopedia.com/entry/wells_h_g).
    Revised 20 May 2015. *[The Encyclopedia of Science
    Fiction](../Page/The_Encyclopedia_of_Science_Fiction.md "wikilink")*
    (sf-encyclopedia.com). Retrieved 2015-08-22. Entry by 'JC/BS', [John
    Clute](../Page/John_Clute.md "wikilink") and [Brian
    Stableford](../Page/Brian_Stableford.md "wikilink").
2.
3.