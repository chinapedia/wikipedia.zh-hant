**通用拼音**是一種帶有強烈[台灣國語的拼音系統](../Page/台灣國語.md "wikilink")。該拼音系統以[拉丁字母作](../Page/拉丁字母.md "wikilink")[漢字](../Page/漢字.md "wikilink")[標音的方案](../Page/標音.md "wikilink")，包含了「華語通用拼音」、「[臺語通用拼音](../Page/臺語通用拼音.md "wikilink")」、「[客語通用拼音](../Page/客語通用拼音.md "wikilink")」三種拼音方案。

一般情況下，**通用拼音**通常用做「華語通用拼音」的簡稱，是[臺灣使用的一種](../Page/臺灣.md "wikilink")[中文](../Page/中文.md "wikilink")[拉丁化拼寫系統](../Page/拉丁化.md "wikilink")，也是[中華民國](../Page/中華民國.md "wikilink")[政府在](../Page/中華民國政府.md "wikilink")2002年至2008年採用的中文拉丁化拼音法。

[中華民國教育部於](../Page/中華民國教育部.md "wikilink")2008年12月18日修正「中文譯音使用原則」，改採用[漢語拼音](../Page/漢語拼音.md "wikilink")，但是包括[高雄市在內的民進黨長期執政縣市](../Page/高雄市.md "wikilink")，仍堅持使用通用拼音。

## 演進歷程

  - 1997年秋天，由時任[台北市長的](../Page/臺北市市長.md "wikilink")[陳水扁邀請](../Page/陳水扁.md "wikilink")[中央研究院民族研究所助研究員](../Page/中央研究院.md "wikilink")[-{余}-伯泉副教授主持](../Page/余伯泉.md "wikilink")、中華民國翻譯學學會執行研究（該學會於1999年改稱臺灣翻譯學學會），1998年1月發表，同年4月臺北市政府市政會議通過街道路牌拼音系統採用通用拼音(1998年版)，當時局部採用zh、q、x拼寫ㄓ、ㄑ、ㄒ。
  - 1999年，通用拼音定案，不再局部使用zh、q、x。
  - 2002年7月10日由[中華民國教育部國語推行委員會第](../Page/中華民國教育部.md "wikilink")11次全體委員會議通過「(臺灣)華語拼音系統為通用拼音」、編訂《[中文譯音使用原則](../Page/s:中文譯音使用原則.md "wikilink")》，同年8月22日行政院准予備查。
  - 2003年內政部頒布[地名譯寫原則](../Page/s:地名譯寫原則.md "wikilink")，以行政院備查的通用拼音為地名標準。
  - 2008年12月18日，中華民國教育部修正「中文譯音使用原則」，改為採用漢語拼音，[高雄市等之縣市](../Page/高雄市.md "wikilink")，仍舊使用通用拼音。

## 拼音符號

### 聲母

<table>
<thead>
<tr class="header">
<th></th>
<th><p><a href="../Page/雙唇音.md" title="wikilink">雙唇音</a></p></th>
<th><p><a href="../Page/唇齒音.md" title="wikilink">唇齒音</a></p></th>
<th><p><a href="../Page/齒齦音.md" title="wikilink">齒齦音</a></p></th>
<th><p><a href="../Page/捲舌音.md" title="wikilink">捲舌音</a></p></th>
<th><p><a href="../Page/齦顎音.md" title="wikilink">齦顎音</a></p></th>
<th><p><a href="../Page/軟顎音.md" title="wikilink">軟顎音</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/清濁音.md" title="wikilink">清音</a></p></td>
<td><p><a href="../Page/清濁音.md" title="wikilink">濁音</a></p></td>
<td><p><a href="../Page/清濁音.md" title="wikilink">清音</a></p></td>
<td><p><a href="../Page/清濁音.md" title="wikilink">清音</a></p></td>
<td><p><a href="../Page/清濁音.md" title="wikilink">濁音</a></p></td>
<td><p><a href="../Page/清濁音.md" title="wikilink">清音</a></p></td>
<td><p><a href="../Page/清濁音.md" title="wikilink">濁音</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鼻音_(辅音).md" title="wikilink">鼻音</a></p></td>
<td></td>
<td><p><a href="../Page/雙唇鼻音.md" title="wikilink"><big><strong>m</strong></big> [m</a>]<br />
ㄇ m</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/齒齦鼻音.md" title="wikilink"><big><strong>n</strong></big> [n</a>]<br />
ㄋ n</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/塞音.md" title="wikilink">塞音</a></p></td>
<td><p><a href="../Page/送氣.md" title="wikilink">不送氣</a></p></td>
<td><p><a href="../Page/清雙唇塞音.md" title="wikilink"><big><strong>b</strong></big> [p</a>]<br />
ㄅ b</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/清齒齦塞音.md" title="wikilink"><big><strong>d</strong></big> [t</a>]<br />
ㄉ d</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/送氣.md" title="wikilink">送氣</a></p></td>
<td><p><a href="../Page/清雙唇塞音.md" title="wikilink"><big><strong>p</strong></big> [pʰ</a>]<br />
ㄆ p</p></td>
<td></td>
<td></td>
<td><p><a href="../Page/清齒齦塞音.md" title="wikilink"><big><strong>t</strong></big> [tʰ</a>]<br />
ㄊ t</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/塞擦音.md" title="wikilink">塞擦音</a></p></td>
<td><p><a href="../Page/送氣.md" title="wikilink">不送氣</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="../Page/清齒齦塞擦音.md" title="wikilink"><big><strong>z</strong></big> [ʦ</a>]<br />
ㄗ z</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/送氣.md" title="wikilink">送氣</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="../Page/清齒齦塞擦音.md" title="wikilink"><big><strong>c</strong></big> [ʦʰ</a>]<br />
ㄘ c</p></td>
<td></td>
<td><p><a href="../Page/清捲舌塞擦音.md" title="wikilink"><big><strong>ch</strong></big> [ʈʂʰ</a>]<br />
ㄔ ch</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/擦音.md" title="wikilink">擦音</a></p></td>
<td></td>
<td></td>
<td><p><a href="../Page/清唇齒擦音.md" title="wikilink"><big><strong>f</strong></big> [f</a>]<br />
ㄈ f</p></td>
<td><p><a href="../Page/清齒齦擦音.md" title="wikilink"><big><strong>s</strong></big> [s</a>]<br />
ㄙ s</p></td>
<td></td>
<td><p><a href="../Page/清捲舌擦音.md" title="wikilink"><big><strong>sh</strong></big> [ʂ</a>]<br />
ㄕ sh</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/邊音.md" title="wikilink">邊音</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="../Page/齒齦邊音.md" title="wikilink"><big><strong>l</strong></big> [l</a>]<br />
ㄌ l</p></td>
<td></td>
</tr>
</tbody>
</table>

### 韻母

<table>
<tbody>
<tr class="odd">
<td><table>
<caption>基本母音</caption>
<thead>
<tr class="header">
<th></th>
<th><p><a href="../Page/前元音.md" title="wikilink">前元音</a></p></th>
<th><p><a href="../Page/央元音.md" title="wikilink">央元音</a></p></th>
<th><p><a href="../Page/後元音.md" title="wikilink">後元音</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>不圓唇</p></td>
<td><p>圓唇</p></td>
<td><p>舌尖</p></td>
<td><p>捲舌</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/閉元音.md" title="wikilink">閉元音</a></p></td>
<td><p><a href="../Page/閉前不圓唇元音.md" title="wikilink"><big><strong>i</strong></big> [i</a>]<br />
ㄧ i</p></td>
<td><p><a href="../Page/閉前圓唇元音.md" title="wikilink"><big><strong>yu</strong></big> [y</a>]<br />
ㄩ ü</p></td>
<td><p><big><strong>ih</strong></big> <br />
ㄭ<a href="https://zh.wikipedia.org/wiki/File:U+312D.svg" title="fig:U+312D.svg">U+312D.svg</a> -i</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/中元音.md" title="wikilink">中元音</a></p></td>
<td><p><a href="../Page/半開前不圓唇元音.md" title="wikilink"><big><strong>e</strong></big> [ɛ</a>]<br />
ㄝ ê</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/開元音.md" title="wikilink">開元音</a></p></td>
<td><p><a href="../Page/開央不圓唇元音.md" title="wikilink"><big><strong>a</strong></big> [ä</a>]<br />
ㄚ a</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table></td>
<td><table>
<caption>複合韻母</caption>
<thead>
<tr class="header">
<th><p>通用拼音</p></th>
<th><p>ai</p></th>
<th><p>ei</p></th>
<th><p>ao</p></th>
<th><p>ou</p></th>
<th><p>an</p></th>
<th><p>en</p></th>
<th><p>ang</p></th>
<th><p>eng</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/國際音標.md" title="wikilink">國際音標</a></p></td>
<td><p>[aɪ]</p></td>
<td><p>[eɪ]</p></td>
<td><p>[ɑʊ]</p></td>
<td><p>[ɤʊ]</p></td>
<td><p>[än]</p></td>
<td><p>[ən]</p></td>
<td><p>[ɑŋ]</p></td>
<td><p>[ɤŋ]</p></td>
</tr>
<tr class="even">
<td><p>注音符號</p></td>
<td><p>ㄞ</p></td>
<td><p>ㄟ</p></td>
<td><p>ㄠ</p></td>
<td><p>ㄡ</p></td>
<td><p>ㄢ</p></td>
<td><p>ㄣ</p></td>
<td><p>ㄤ</p></td>
<td><p>ㄥ</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>通用拼音</p></td>
<td><p>ia<br />
ya</p></td>
<td><p>ie<br />
ye</p></td>
<td><p>iao<br />
yao</p></td>
<td><p>iou<br />
you</p></td>
<td><p>ian<br />
yan</p></td>
<td><p>in<br />
yin</p></td>
<td><p>iang<br />
yang</p></td>
<td><p>ing<br />
ying</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/國際音標.md" title="wikilink">國際音標</a></p></td>
<td><p>[iä]</p></td>
<td><p>[iɛ]</p></td>
<td><p>[iɑʊ]</p></td>
<td><p>[iɤʊ]</p></td>
<td><p>[iɛn]</p></td>
<td><p>[in]</p></td>
<td><p>[iɑŋ]</p></td>
<td><p>[iŋ]</p></td>
</tr>
<tr class="even">
<td><p>注音符號</p></td>
<td><p>ㄧㄚ</p></td>
<td><p>ㄧㄝ</p></td>
<td><p>ㄧㄠ</p></td>
<td><p>ㄧㄡ</p></td>
<td><p>ㄧㄢ</p></td>
<td><p>ㄧㄣ</p></td>
<td><p>ㄧㄤ</p></td>
<td><p>ㄧㄥ</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>通用拼音</p></td>
<td><p>ua<br />
wa</p></td>
<td><p>uo<br />
wo</p></td>
<td><p>uai<br />
wai</p></td>
<td><p>uei<br />
wei</p></td>
<td><p>uan<br />
wan</p></td>
<td><p>un<br />
wun</p></td>
<td><p>uang<br />
wang</p></td>
<td><p>ong<br />
wong</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/國際音標.md" title="wikilink">國際音標</a></p></td>
<td><p>[uä]</p></td>
<td><p>[uɔ]</p></td>
<td><p>[uaɪ]</p></td>
<td><p>[ueɪ]</p></td>
<td><p>[uän]</p></td>
<td><p>[un]</p></td>
<td><p>[uɑŋ]</p></td>
<td><p>[uɤŋ]</p></td>
</tr>
<tr class="even">
<td><p>注音符號</p></td>
<td><p>ㄨㄚ</p></td>
<td><p>ㄨㄛ</p></td>
<td><p>ㄨㄞ</p></td>
<td><p>ㄨㄟ</p></td>
<td><p>ㄨㄢ</p></td>
<td><p>ㄨㄣ</p></td>
<td><p>ㄨㄤ</p></td>
<td><p>ㄨㄥ</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>通用拼音</p></td>
<td><p>yue</p></td>
<td><p>yuan</p></td>
<td><p>yun</p></td>
<td><p>yong</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/國際音標.md" title="wikilink">國際音標</a></p></td>
<td><p>[yœ]</p></td>
<td><p>[yɛn]</p></td>
<td><p>[yn]</p></td>
<td><p>[iʊŋ]</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>注音符號</p></td>
<td><p>ㄩㄝ</p></td>
<td><p>ㄩㄢ</p></td>
<td><p>ㄩㄣ</p></td>
<td><p>ㄩㄥ</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table></td>
</tr>
</tbody>
</table>

  - y,w,yu作為**結合音**—**y**an、**w**an、**yu**an的開頭使用。

## 特徵

  - 通用拼音和漢語拼音的「音位歸併」不同，在「符號選擇」有部分相似性。

### 拼寫方式差異

  - 通用拼音的音位歸併不採ㄐㄑㄒ獨立，因此漢語拼音的**j**、**q**、**x**
    為通用拼音的**ji**、**ci**、**si**。

<!-- end list -->

  - 通用拼音的空韻音節比照威妥瑪式採ih，以避免漢語拼音的音節si成為空韻的困擾。在漢語拼音中zhi、chi、shi、ri、zi、ci、si后的**i**
    ，在通用拼音中使用**ih**
    。所以漢語拼音的zhi、chi、shi、ri、zi、ci、si，在通用拼音中以jhih、chih、shih、rih、zih、cih、sih表記。

<!-- end list -->

  - 通用拼音有fong與wun音節，漢語拼音以feng與wen表記，這一點視為通用拼音以台北華語的語音為主體，而非以普通話語音為主體。漢語拼音的en、eng，在通用拼音中，於以下情形拼法有所差異：
      - 連韻**e**ng接於韻母f- 、w-（風、翁）之後時，改拼成**o**ng。
      - w**e**n（文）改拼成w**u**n。

<!-- end list -->

  - 通用拼音以yu貫串所有ㄩ類音節。yu及以其開頭的結合音，接於聲母ji- 、ci- 、si-
    之後，仍使用yu銜接韻母，完全不使用ü（在漢語拼音中音同「淤」），但韻母皆需將i去除。例如j**yu**（居）、s**yu**e（雪）、c**yu**an（全）

<!-- end list -->

  - 聲母ji- 、ci- 、si-
    與連韻yong結合時，去除韻母的i，但保留連韻的y。例如漢語拼音的yong（永）和j**iong**（窘），在通用拼音中即拼為yong（永）和j**yong**（窘）。

<!-- end list -->

  - 通用拼音公布的標準為不漏音的**iou** 和**uei**。而非**iu**（「六」的尾音）和**ui**（「灰」的尾音）

<!-- end list -->

  - 一般如果單詞的音節可能不明，在通用拼音用**短劃**而不用**撇號**分開音節。例「Jian」難以看出是「[吉安](../Page/吉安鄉.md "wikilink")」或「簡」，在漢語拼音用「Ji'an」表記「[吉安](../Page/吉安鄉.md "wikilink")」、在通用拼音用「Ji-an」表記「[吉安](../Page/吉安鄉.md "wikilink")」。

<!-- end list -->

  - 通用拼音的聲調符號與[注音符號相同](../Page/注音符號.md "wikilink")：一聲可以不標，也可以標寫為一橫，輕聲有一點；漢語拼音的一聲為一橫、輕聲沒有符號。

<!-- end list -->

  - 地名第二個漢字音節以a、o、e為首時，通用拼音前面以短劃連接、漢語拼音以撇號連接，如「大安」的通用拼音是「Da-an」、漢語拼音是「Da'an」。除了地名，通用拼音同個字中的音節也可以用短劃分開。例：漢語拼音的
    ，通用拼音可拼為 或。

### 優缺點分析

  - 當初設計通用拼音的主要目的之一是貼近台灣人的發音習慣，並於稍後改版時去除漢語拼音中不符合英文讀寫習慣的聲母（x、q），對於完全沒有學習過漢語拼音的外籍人士而言，可能較容易閱讀。
  - ㄓㄔㄕ三聲母由ㄐㄑㄒ加上h轉寫而來。ㄓ拼寫為jh，在英文中沒有此種拼法，只有接近的John。而汉语拼音使用的zh在英语中被用于转写某些语言的/ʒ/，如[朱可夫](../Page/朱可夫.md "wikilink")：Zhukov。
  - 表示空韻的ih的寫法不見於其他語言。漢語拼音是把空韻當做i的音位變體進行處理，使用i來記錄，歷史上，空韻字的韻母很多本來就是i，這麼寫是有一定歷史依據的。

## 與政治的關係

  - 通用拼音的誕生，本是為了在「語言工具論」（漢語拼音是全世界最廣泛使用的中文拼音標準）與「認同論」（在使用拉丁字母表記時，可與中華人民共和國明顯區分）間取得平衡，1997年由[國民黨市議員](../Page/國民黨.md "wikilink")[陳學聖舉辦拼音座談會](../Page/陳學聖.md "wikilink")，向民進黨籍[陳水扁市長建議採用通用拼音](../Page/陳水扁.md "wikilink")。
  - 1998年4月，由[民主進步黨主政的](../Page/民主進步黨.md "wikilink")[台北市政府](../Page/台北市政府.md "wikilink")，公布採用通用拼音，強調「語言工具論」與「認同論」兩者均衡才是穩健的國家拼音政策，並陸續啟用許多帶有「漢語拼音特徵」的路名標示，例如「忠孝東路五段
    **ZhongXiao** E. Rd. Sec.5」、「許昌街 **XuChang** St.」、「館前路 **GuanQian**
    Rd.」及「市府路 **ShiFu** Rd.」\[1\]
  - [2000年台灣政黨輪替](../Page/2000年中華民國總統選舉.md "wikilink")，國民黨立法委員陳學聖等多人與民進黨立法委員等共同連署支持中央政府公布通用拼音。民進黨任命的教育部長[曾志朗反對通用拼音](../Page/曾志朗.md "wikilink")，這時通用拼音已經改版，以「去除**q**、**x**、**zh**
    」做為與漢語拼音最大區隔，加上通用拼音發明人余伯泉等五位學者，在《語言工具論的五大錯誤：敬答[鄭錦全等四位院士](../Page/鄭錦全.md "wikilink")》的公開文章中述明：「……國際慣例所以會在台灣混淆不清，主要是牽涉到一個更基本的問題，也就是根本的『國家認同混淆』問題」，導致「拼音系統」爭議爆發，藍軍追擊，激起藍綠意識型態對立抗爭，「通用拼音」從此被貼上綠色標籤、失去政治中立的可能性。
  - 2009年通用拼音失去官方光環後，政治色彩更鮮明，雖然台灣華語拼音聯盟一再號稱所有抗爭活動「不分藍綠」，但不論是2009年2月21日[北捷](../Page/北捷.md "wikilink")[西門站](../Page/西門站.md "wikilink")、2009年11月20日國道客運[台北轉運站](../Page/台北轉運站.md "wikilink")、2010年1月10日[中壢火車站](../Page/中壢火車站.md "wikilink")、2010年6月24日[台北縣政府](../Page/台北縣政府.md "wikilink")（現[新北市政府](../Page/新北市政府.md "wikilink")）等地的抗議活動，均只見親綠社團及綠營民代出席。
  - 部分[國民黨和](../Page/國民黨.md "wikilink")[新黨的台北市議員](../Page/新黨.md "wikilink")，曾在陳水扁市長任內表示支持通用拼音，認為通用拼音是台灣唯一能夠兼顧尊重多種族語言，以及中文譯音方便性的譯音系統（此時通用拼音可使用**q**、**x**、**zh**拼寫**ㄑ**、**ㄒ**、**ㄓ**），但是在通用拼音被標明政治立場、藍綠對立激化後反對通用拼音（此時通用拼音已捨棄**q**、**x**、**zh**，失去原有的包容性）。

## 批評與反省

  - 按照台灣華語拼音聯盟網站所載《「台灣地名台灣拼音」基本資訊與說帖》：「源於1996年行政院[教育改革總諮議報告書建議的通用標音系統](../Page/教育改革總諮議報告書.md "wikilink")」，有人會誤會1996年的「教育改革總諮議報告書」主張「通用拼音」。依[洪惟仁於](../Page/洪惟仁.md "wikilink")1999年4月20日[自由時報](../Page/自由時報.md "wikilink")「自由廣場」投書之《請誠實對待「通用拼音」》文中記載：「李([遠哲](../Page/李遠哲.md "wikilink"))院長在4月6日給我們的回信中做出非常明理的澄清：……（[教改會](../Page/教改會.md "wikilink")）基本上同意推動用英文拼音符號代替現有的注音符號（第一式），至於採用何種形式的英文拼音，教改會同仁並無定見，理當由語言學家與教育主管當局審慎研究。」、「通用拼音乃余伯泉先生個人之志業，非遠哲或中研院所委託之工作。」。不過教改總諮議報告書確實建議發展通用標音系統，當時任職於中央研究院的余伯泉也是源於這個建議，發展出通用拼音，李遠哲院長也主持了通用拼音的第一次推動會議，但是基於中央研究院另一批專家有不同的拼音主張，李遠哲態度變為更審慎。2002年教改會同仁黃榮村任教育部長時，承行政院的核示，公布通用拼音政策。
  - 2009年11月20日台北市國道客運轉運站抗議活動中，台北市議員[莊瑞雄宣稱](../Page/莊瑞雄.md "wikilink")「[學甲使用S](../Page/學甲區.md "wikilink")
    (Syuejia)
    已幾百年」\[2\]，然而此非事實。[日治時期學甲譯作](../Page/台灣日治時期.md "wikilink")**Gakkō**、[戰後至](../Page/台灣戰後時期.md "wikilink")2002年則譯作**Hsuechia**。
  - 台灣華語拼音聯盟以羅馬拼音「名從主人」慣例，強調台灣使用通用拼音的正當性。有人批評2003年余伯泉協助陳水扁政府頒布[地名譯寫原則](../Page/s:地名譯寫原則.md "wikilink")，違反「名從主人」慣例、變更全國鄉鎮市區沿用數十年的[威妥瑪拼音英譯名稱](../Page/威妥瑪拼音.md "wikilink")，改採通用拼音英譯，尤其是[淡水](../Page/淡水區.md "wikilink")、[鹿港兩地具歷史意義的英譯名稱](../Page/鹿港鎮.md "wikilink")---閩南語白話字的Tamsui及威妥瑪拼音的Lukang，目前已由內政部於2011年6月16日核定回復。不過這並非事實，2002年通用拼音公布時，縣市以上大地名均維持舊拼音，嘉義與宜蘭拼音的改變也獲得地方政府的同意；通用拼音使用原則的地名譯寫原則，清楚標示可考慮傳統與歷史因素，例如墾丁公園一直維持原有kenting標示而非kending。余伯泉提給台北捷運公司的建議，亦曾建議淡水採Tamsui。1998年余伯泉主導的台北市路名拼音，已將總統府前的凱達格蘭大道拼寫為ketagalan。
  - 面對台灣多元的族群文化，通用拼音包含了「華語通用拼音」、「[臺語通用拼音](../Page/臺語通用拼音.md "wikilink")」、「[客語通用拼音](../Page/客語通用拼音.md "wikilink")」三種拼音方案，以英文自然拼音為基礎，試圖整合、溝通台灣各種語言的拼音平台，提高學生的學習成效，減輕台灣學生因多語言、多拼音的學習困擾，通用拼音在台灣語文的發展史仍是值得注目的。
  - 台灣教育部「97年國民中小學九年一貫課程閩南語綱要(100學年度實施)」特別標示：「標音符號原則於三年級教授，惟學校得視實際需要及學生程度提前於二年級實施。『配合多元語言環境，注意符號系統間的共通性。』」通用拼音無疑為「配合多元語言環境，標音符號系統間的共通性」提供一個可能的方案。

## 參見

  - [中文拼音對照表](../Page/中文拼音對照表.md "wikilink")
  - [中文譯音使用原則](../Page/s:中文譯音使用原則.md "wikilink")
  - [地名譯寫原則](../Page/s:地名譯寫原則.md "wikilink")
  - [威妥瑪拼音](../Page/威妥瑪拼音.md "wikilink")
  - [注音二式](../Page/國語注音符號第二式.md "wikilink")
  - [注音符號](../Page/注音符號.md "wikilink")
  - [汉语拼音](../Page/汉语拼音.md "wikilink")
  - [國際音標](../Page/國際音標.md "wikilink")
  - [臺語通用拼音](../Page/臺語通用拼音.md "wikilink")
  - [客語通用拼音](../Page/客語通用拼音.md "wikilink")
  - [現代標準漢語\#語音系統歷史](../Page/現代標準漢語#历史.md "wikilink")
  - [台灣的中文羅馬拼音](../Page/台灣的中文羅馬拼音.md "wikilink")
  - [台灣本土化運動](../Page/台灣本土化運動.md "wikilink")

## 参考文献

## 外部連結

  - [台灣地名用台灣拼音| 母語教學](http://www.dayutaiwan.org/)
  - [台灣通用拼音資源](http://www.creativeschools.org/romanization/index.htm)
  - [台灣地名用台灣拼音 | 大家一起學母語](http://www.skytaiwan.org/)
  - [積丹尼-推漢語拼音， 摧通用拼音](http://jidanni.org/lang/pinyin/older.html)
  - [通用拼音與漢語拼音的相似性](http://research.chtsai.org/papers/pinyin-comparison-big5.html)
  - [Pinyin4j（支援中文字符转换到通用拼音的Java库；开放源代码）](http://pinyin4j.sourceforge.net/)
  - [拼音對照表與音節表](http://www.casub.taipei.gov.tw/civil/system/road-3.htm)
  - [鯤島本土文化（臺語通用拼音字典PDF檔）](http://www.dang.idv.tw/)
  - [注音轉換工具](http://ctext.org/pinyin.pl?if=gb) - 各種注音方式之間的轉換
  - [20091120三立新聞\~劉建國.莊瑞雄交九抗議](http://www.youtube.com/watch?v=gF5Rag5yN6U)
  - [余伯泉在**ShiFu
    Rd.**前比「讚」](http://jidanni.org/lang/pinyin/images/19980718shifu_rd.jpg)
  - [2009/11/20台灣地名
    台灣拼音，勿通匪類！](http://www.youtube.com/watch?feature=player_embedded&v=nIBHwBa8ndE)
  - [新北市英譯名畫X 民間不滿 2010/06/24
    民視新聞](http://www.youtube.com/watch?v=T5qvnGa-JH0)
  - [2010年1月10日中壢火車站抗議亂改地名拼音](http://www.youtube.com/watch?v=W2pYkneQL3g)
  - [20090312
    搶救台灣地名拼音.反對勞民傷財用中國拼音記者會](http://www.youtube.com/watch?v=OfLxbYLEbTg&feature=related)

[Category:台灣語言拉丁化](../Category/台灣語言拉丁化.md "wikilink")
[Category:现代标准汉语拼音系统](../Category/现代标准汉语拼音系统.md "wikilink")

1.  台北市街路譯名系統專案研究報告。台北市政府民政局。1998年。
2.