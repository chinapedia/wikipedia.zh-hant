**PP视频**是[中国的](../Page/中国.md "wikilink")[流媒体](../Page/流媒体.md "wikilink")[免费软件](../Page/免费软件.md "wikilink")，创始人是[姚欣](../Page/姚欣.md "wikilink")，其作为[点对点技术的程序](../Page/点对点技术.md "wikilink")，结合了[P2P和](../Page/P2P.md "wikilink")[IPTV的技术](../Page/IPTV.md "wikilink")，因此称为P2PTV。\[1\]

2006年至2010年，PPTV连续三年荣登由“清科中国Zero2IPO”评选的“中国最具投资价值企业50强”；2009年，PPLive成为中国唯一一家全球100强的网络视频企业，获得“最具投资价值企业”称号，荣登最具影响力的科技风险投资杂志《Redherring》。

## 历史

[2002年世界杯足球赛](../Page/2002年世界杯足球赛.md "wikilink")，身为球迷的[姚欣苦于学校宿舍没有电视](../Page/姚欣.md "wikilink")，在学校电脑上用[校内网看球赛](../Page/校内网.md "wikilink")，服务器因承载能力有限而经常崩溃。之后，姚欣便萌发了制作电视共享软件的念头。2003年[SARS横行](../Page/SARS.md "wikilink")，姚欣趁着那段空闲时间，开始尝试将點對點技術技术和流媒体技术进行整合，以技术手段来解决在互联网上观看电视节目的问题。\[2\]

2004年9月，[暑假返校后](../Page/暑假.md "wikilink")，姚欣向学校申请休学创业。创业之初，姚欣先后拜访了近百个投资商，却一次又一次地被拒绝。然而，姚欣却得到了家乡[河南一家公司老板](../Page/河南.md "wikilink")[李翀的支持](../Page/李翀.md "wikilink")。李翀具有10几年的高科技企业运作经验。正是在李翀的支持下，PPLive项目得以启动。最初以其母校[华中科技大学的韵苑](../Page/华中科技大学.md "wikilink")26栋寝室为工作室，华中科技大学计算机中心为[服务器托管](../Page/服务器.md "wikilink")，姚欣和团队成员们开始了封闭式开发。2004年年底，PPLive
1.0正式在韵苑26栋诞生。

2005年初，PPLive获得了[软银中国](../Page/软银中国.md "wikilink")（SoftBank China
Venture
Capital）20万[美金天使性质的](../Page/美金.md "wikilink")[种子投资](../Page/种子投资.md "wikilink")。2月，PPLive软件正式上线，在华中科技大学校园内进行小范围的测试。之后，PPLive便火速传开。当时，仅在华中科技大学，PPLive用户就已经达到1700多人。5月，[上海聚力传媒技术有限公司](../Page/上海聚力传媒技术有限公司.md "wikilink")（PPLive）在[上海注册成立](../Page/上海.md "wikilink")，姚欣出任[CEO](../Page/CEO.md "wikilink")，李翀出任[执行董事](../Page/执行董事.md "wikilink")，但是主要运营活动仍然集中在武汉进行。至2005年底，PPLive开始第一轮正式[融资](../Page/融资.md "wikilink")。

2006年3月，刚刚进入中国大陆不久的[蓝驰创投](../Page/蓝驰创投.md "wikilink")（Bluerun）向PPLive注资。同时，PPLive所在的聚力传媒由纯技术型企业转型为[传媒企业](../Page/传媒.md "wikilink")。企业名称改为上海聚力传媒有限公司。同年7月，PPLive开始小规模试验“贴片”广告。9月，PPLive正式进驻上海[张江高科技园区](../Page/张江高科技园区.md "wikilink")，实现了注册地和运营地的统一，同时员工规模超过80人。10月，PPLive正式和[好耶广告网络达成合作](../Page/好耶广告网络.md "wikilink")，开始系统地尝试视频广告业务。

2007年6月，来自[德丰杰投资等总计超过](../Page/德丰杰投资.md "wikilink")2100万美金的第二轮[融资完全到位后](../Page/融资.md "wikilink")，PPLive开始加速整合视频产业链上下游资源。

2008年，PPLive成为第一批获得中国广播电影电视总局颁发《信息网络传播视听节目许可证》的互联网企业，并与300多家电视台、影视制作机构及传媒公司合作。

2009年6月，[中共中央政治局常委](../Page/中共中央政治局.md "wikilink")[李长春与](../Page/李长春.md "wikilink")[上海市委书记](../Page/上海市.md "wikilink")[俞正声](../Page/俞正声.md "wikilink")，[文化部部长](../Page/文化部.md "wikilink")[蔡武](../Page/蔡武.md "wikilink")，[广电总局局长王太华和新闻出版总署署长柳斌杰等人到PPlive聚力传媒考察](../Page/广电总局.md "wikilink")。李长春向PPLive创始人姚欣表示“希望你成为中国的[比尔盖茨](../Page/比尔盖茨.md "wikilink")”，并充分肯定PPLive依托自主创新技术所取得的成就。\[3\]

2010年，更名为PPTV（聚力视频）。\[4\]

2011年2月，PPTV在保持中国国内网络电视行业第一的同时，姚欣还将公司的发展触角伸向了国际化网络电视新媒体，PPTV和软银对外宣布，PPTV得到软银数亿美金的巨额注资，共同开拓互联网视频市场，全面打造PPTV网络电视媒体平台。\[5\]

2013年10月，[苏宁云商与](../Page/苏宁云商.md "wikilink")[联想集团旗下弘毅资本联手](../Page/联想集团.md "wikilink")，斥资4.2亿美元（约33亿港元）入股PPTV，苏宁将成为PPTV单一最大股东。根据交易条款，苏宁云商及弘毅资本将分别出资2.5亿及1.7亿美元，交易完成后，苏宁将持有PPTV约44%股权，成为大股东，弘毅则占29.9%，相当于PPTV整体估值约5.68亿美元（约44.3亿港元）。\[6\]之后由于乐视深陷财务危机，PPTV接手了原乐视体育的部分资源。

2018年，更名为PP视频。这一年，PP视频首次获得了[欧洲五大联赛以及](../Page/欧洲五大联赛.md "wikilink")[欧冠联赛全部比赛的转播权](../Page/欧冠联赛.md "wikilink")（苏宁体育为德甲、意甲及欧冠在中国大陆的全媒体持权转播商）。

## 合作方式

PP视频跟内容提供商的合作关系遵循了从渠道到厂商的路径，最初的内容提供商都是像[东方宽频](../Page/东方宽频.md "wikilink")、[九州梦网](../Page/九州梦网.md "wikilink")、[新传体育](../Page/新传体育.md "wikilink")、[优度宽频](../Page/优度宽频.md "wikilink")（买断《[夜宴](../Page/夜宴.md "wikilink")》网络发行权）、[鸿波宽频](../Page/鸿波宽频.md "wikilink")、[中凯文化等一类的频道](../Page/中凯文化.md "wikilink")，这些企业大都早就是合作伙伴，[光线传媒](../Page/光线传媒.md "wikilink")、[欢乐传媒](../Page/欢乐传媒.md "wikilink")、[太合传媒等传统内容提供商也相续成为了合作伙伴](../Page/太合传媒.md "wikilink")，已经和[央视国际](../Page/中国中央电视台.md "wikilink")、[湖南卫视](../Page/湖南卫视.md "wikilink")、[东方卫视](../Page/东方卫视.md "wikilink")、[凤凰卫视](../Page/凤凰卫视.md "wikilink")、[河南卫视等超过](../Page/河南卫视.md "wikilink")20家[电视台达成了不同层次的合作关系](../Page/电视台.md "wikilink")。PP视频与内容提供商的合作基本上采取保底分成的模式，分成比例大致在30%-50%之间。

## 播放内容

PP视频播放的内容很多。包括[电影](../Page/电影.md "wikilink")，[电视](../Page/电视.md "wikilink")，[音乐](../Page/音乐.md "wikilink")，[新闻](../Page/新闻.md "wikilink")，[体育](../Page/体育.md "wikilink")，[游戏等众多类别](../Page/游戏.md "wikilink")。PP视频的网站还提供节目预报。绝大多数节目都是[粤语或](../Page/粤语.md "wikilink")[北京官話](../Page/北京官話.md "wikilink")，即便是[英语](../Page/英语.md "wikilink")，[日语](../Page/日语.md "wikilink")，[韩语的节目](../Page/韩语.md "wikilink")，也会有中文的字幕。因为P2P技术受到网络连接和用户数量的影响，经常会出现短暂的中断。当用户数量严重不足或文件源发生错误时，甚至会发生永久停止的情况，这时用户必须采取手动重新连接。以及节目内容更新较慢。

PP视频所播放的某些节目由第三方提供，但有一部分节目没有得到[版权所有者的允许](../Page/版权.md "wikilink")，曾遭到上海版权商的起诉。\[7\]

## 参考文献

## 外部链接

  - [PP视频](https://www.pptv.com/)

[Category:中国网站](../Category/中国网站.md "wikilink")
[Category:网络电视](../Category/网络电视.md "wikilink")
[Category:视频网站](../Category/视频网站.md "wikilink")
[Category:视频分享网站](../Category/视频分享网站.md "wikilink")
[Category:P2P](../Category/P2P.md "wikilink")

1.
2.
3.
4.
5.
6.
7.