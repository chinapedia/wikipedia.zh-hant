**悉尼**（），又稱**-{zh-cn:雪梨;zh-hant:雪梨;zh-hk:雪梨;zh-tw:悉尼}-**，位於[澳大利亚东南沿海地带](../Page/澳大利亚.md "wikilink")，是[新南威爾斯州的](../Page/新南威爾斯州.md "wikilink")[首府](../Page/首府.md "wikilink")，是澳大利亚人口最多也是人口最稠密的城市。[都會區人口](../Page/都會區.md "wikilink")5,131,326人（2017）。在[英語裏](../Page/英語.md "wikilink")，市民俗稱雪梨人為「Sydneysider」。

悉尼是[歐洲首個澳洲](../Page/歐洲.md "wikilink")[殖民聚落以及充当罪犯的流放地](../Page/殖民.md "wikilink")，1788年由[英國](../Page/英國.md "wikilink")[第一艦隊的船長](../Page/第一艦隊.md "wikilink")[阿瑟·菲利普於](../Page/阿瑟·菲利普.md "wikilink")[雪梨湾建立](../Page/雪梨湾.md "wikilink")\[1\]\[2\]。悉尼環[傑克遜港](../Page/傑克遜港.md "wikilink")（包括[悉尼港](../Page/悉尼港.md "wikilink")）而建，曾被稱為「海港城市」。它是澳洲最大的[金融中心](../Page/金融.md "wikilink")，也是國際主要[旅遊勝地](../Page/旅遊.md "wikilink")，以[海灘](../Page/邦迪海灘.md "wikilink")、[歌劇院和](../Page/悉尼歌劇院.md "wikilink")[港灣大橋等聞名](../Page/悉尼港灣大橋.md "wikilink")。雪梨长期以来都是生活品質极高的世界都市，曾獲全球化與世界級城市研究小組與網絡（Globalization
and World Cities Study Group and
Network，GaWC）列為[第一級世界都市+](../Page/全球城市.md "wikilink")\[3\]。悉尼举办过多項重要国际[體育赛事](../Page/體育.md "wikilink")，包括1938年[英联邦运动会](../Page/英联邦运动会.md "wikilink")、[2000年悉尼奧運會及](../Page/2000年悉尼奧運會.md "wikilink")2003年[世界盃橄欖球賽](../Page/世界盃橄欖球賽.md "wikilink")。

主要机场为[悉尼机场](../Page/悉尼机场.md "wikilink")，主要港口为[植物学湾](../Page/植物学湾.md "wikilink")。

## 中文名稱

在澳大利亚当地，「雪-{}-梨」和「悉-{}-尼」两种中文名称均有使用，其中[悉尼市政府将](../Page/悉尼市.md "wikilink")「雪-{}-梨」作为繁体中文标准译名，而将「悉-{}-尼」作为简体中文标准译名\[4\]。[中华人民共和国驻悉尼机构使用](../Page/中华人民共和国.md "wikilink")“悉-{}-尼”\[5\]，如中华人民共和国驻悉-{}-尼总领事馆。而[台灣驻悉尼机构则使用](../Page/台灣.md "wikilink")「雪-{}-梨」，如駐雪-{}-梨台北經濟文化辦事處
\[6\]。悉尼当地的华人社团组织名称大多冠以“雪-{}-梨”，澳大利亚的华文媒体传统上使用的也是“雪-{}-梨”。随着来自中国大陆的移民和留学生增多，近年来澳大利亚各级政府及当地华人社区使用“悉-{}-尼”的也越来越多。

## 歷史

[Very_early_map_of_sydney_from_1789.jpg](https://zh.wikipedia.org/wiki/File:Very_early_map_of_sydney_from_1789.jpg "fig:Very_early_map_of_sydney_from_1789.jpg")，圖中的“Cove”即今日之[悉尼湾](../Page/悉尼湾.md "wikilink")\]\]
[澳洲原住民在悉尼地區居住已至少有三萬年](../Page/澳洲原住民.md "wikilink")。1788年[第一艦隊抵達時](../Page/第一艦隊_\(英國殖民艦隊\).md "wikilink")，本地尚住有4000-8000個原住民。\[7\]
悉尼地區有三種不同的語系，它們可推敲為較小部落的方言。主要的語言為[塔魯爾語](../Page/塔魯爾語.md "wikilink")（Darug）；[悉尼市的原住民](../Page/悉尼市.md "wikilink")──卡地哥（Cadigal），講塔魯爾語的一種海岸方言還有塔爾瓦斯語（Dharawal）和顧林凱語（Guringai）。部落有各自的領土，其領土的選址決定了資源的多寡。雖然在[都市化的過程中摧毀了大部分的部落遺址](../Page/都市化.md "wikilink")（如[貝塚](../Page/貝塚.md "wikilink")），幾個地點仍存有石刻。

自從1770年英國海軍上尉[詹姆斯·庫克發現了](../Page/詹姆斯·庫克.md "wikilink")[植物灣](../Page/植物灣.md "wikilink")（Botany
Bay）後，[歐洲隨之對](../Page/歐洲.md "wikilink")[澳洲產生了興趣](../Page/澳洲.md "wikilink")。在[英國政府的命令下](../Page/英國.md "wikilink")，[亚瑟·菲利普](../Page/亚瑟·菲利普.md "wikilink")（Arthur
Phillip）1788年於[傑克遜港的](../Page/傑克遜港.md "wikilink")[悉尼灣建立了罪犯流放地](../Page/悉尼灣.md "wikilink")。他以當時的英國[內政大臣悉尼子爵](../Page/內政大臣.md "wikilink")[湯馬斯·湯森命名該地](../Page/托马斯·汤森，悉尼子爵.md "wikilink")，以頌揚悉尼發佈憲章，批准他建立流放地的貢獻。

1789年4月，一場疫症（據說是[天花](../Page/天花.md "wikilink")），奪去悉尼不少原住民的性命。保守估計，500-1000個原住民死於[布羅肯灣](../Page/布羅肯灣.md "wikilink")（Broken
Bay）和植物灣之間的地區，顧林凱和Darug兩族受感染。\[8\]
英國的殖民遇到頑強阻力，尤其是以勇士領袖Pemulwuy為首，在植物學灣附近的地區發動抗爭。同時，[霍克斯伯里河附近也時常爆發衝突](../Page/霍克斯伯里河.md "wikilink")。因此至1820年為止，悉尼地區只剩下數百個原住民。[麥覺理總督則進一步把原住民](../Page/拉克倫·麥覺理.md "wikilink")「開化、基督教化及教化」，使他們離開部落。\[9\]

[Sydney1796.jpg](https://zh.wikipedia.org/wiki/File:Sydney1796.jpg "fig:Sydney1796.jpg")
麥覺理任新南威爾斯州總督期間，悉尼有初步的發展。囚犯建築了道路、橋樑、碼頭和公共建築。至1822年以前，城內已有銀行、市場、完善的大道及制度化警察機構。

1830-40年代是城市發展（如首批城區的發展）的階段。船隻開始從[不列顛群島接載希望在新國家開展新生活的移民](../Page/不列顛群島.md "wikilink")，悉尼因此進入高度發展的黃金時代。

首次的[淘金熱始於](../Page/澳洲淘金熱.md "wikilink")1851年，悉尼的港口湧入來自世界各地的人潮。19世紀末，隨著蒸氣動力電車和鐵路系統的問世，城區的發展更加迅速。由於工業化所帶來的便捷，使悉尼人口迅速膨脹，在20世紀前夕，悉尼的人口已經超過100萬。但[大恐慌重創悉尼](../Page/大蕭條.md "wikilink")，1932年[悉尼港灣大橋的落成](../Page/悉尼港灣大橋.md "wikilink")，可說是該時期的重大紀事之一。

1942年，[第二次世界大戰期間](../Page/第二次世界大戰.md "wikilink")，日本海軍利用小型潛水艇偷襲悉尼港失敗。

20世紀，悉尼繼續擴展，湧入了[歐洲與](../Page/歐洲.md "wikilink")（後期的）[亞洲的新移民](../Page/亞洲.md "wikilink")，增添國際魅力。悉尼人的祖先大多來自[英國和](../Page/英國.md "wikilink")[愛爾蘭](../Page/愛爾蘭.md "wikilink")。其他移民有[意大利人](../Page/意大利.md "wikilink")、[希臘人](../Page/希臘.md "wikilink")、[猶太人](../Page/猶太.md "wikilink")、[黎巴嫩人](../Page/黎巴嫩.md "wikilink")、[南非人](../Page/南非.md "wikilink")、[南亞人](../Page/南亞.md "wikilink")（包括[印度人](../Page/印度.md "wikilink")、[斯里蘭卡人和](../Page/斯里蘭卡.md "wikilink")[巴基斯坦人](../Page/巴基斯坦.md "wikilink")）、[蘇丹人](../Page/蘇丹.md "wikilink")、[土耳其人](../Page/土耳其.md "wikilink")、[馬其頓人](../Page/馬其頓.md "wikilink")、[克羅埃西亞人](../Page/克羅埃西亞.md "wikilink")、[塞爾維亞人](../Page/塞爾維亞.md "wikilink")、[南美人](../Page/南美.md "wikilink")（[巴西人](../Page/巴西.md "wikilink")、[智利人和](../Page/智利.md "wikilink")[阿根廷人](../Page/阿根廷.md "wikilink")）、[美國人](../Page/美國.md "wikilink")、[東歐人](../Page/東歐.md "wikilink")（[捷克人](../Page/捷克.md "wikilink")、[波蘭人](../Page/波蘭.md "wikilink")、[俄羅斯人](../Page/俄羅斯.md "wikilink")、[烏克蘭人和](../Page/烏克蘭.md "wikilink")[匈牙利人](../Page/匈牙利.md "wikilink")）、[東亞人](../Page/東亞.md "wikilink")（包括[華人和](../Page/華人.md "wikilink")[韓國人](../Page/韓國.md "wikilink")）以及[東南亞人](../Page/東南亞.md "wikilink")（包括[越南人](../Page/越南.md "wikilink")、[泰國人和](../Page/泰國.md "wikilink")[菲律賓人](../Page/菲律賓.md "wikilink")）。

### 紀事年表

[Map_of_Sydney_central_bus_district.PNG](https://zh.wikipedia.org/wiki/File:Map_of_Sydney_central_bus_district.PNG "fig:Map_of_Sydney_central_bus_district.PNG")

  - 1788年：建立首個歐洲殖民地
  - 1852年：悉尼合組為[城市](../Page/城市.md "wikilink")
  - 1855年：[新南威爾斯州首條](../Page/新南威爾斯州.md "wikilink")[鐵路路線接通悉尼與](../Page/鐵路.md "wikilink")[帕拉瑪塔](../Page/帕拉瑪塔市.md "wikilink")
  - 1870年：
  - 1879年：
  - 1883年：[帕拉瑪塔](../Page/帕拉瑪塔.md "wikilink") -
  - 1901年1月1日：[澳洲聯邦在悉尼宣告成立](../Page/澳洲聯邦.md "wikilink")
  - c. 1903年：人口超越[墨爾本](../Page/墨爾本.md "wikilink")，成為澳洲最大的城市
  - 1932年：[悉尼港灣大橋落成](../Page/悉尼港灣大橋.md "wikilink")
  - 1942年：悉尼被[日本海軍](../Page/日本帝國海軍.md "wikilink")[潛艇](../Page/潛艇.md "wikilink")
  - 1973年：[悉尼歌劇院落成](../Page/悉尼歌劇院.md "wikilink")
  - 2000年：[2000年夏季奧運會](../Page/2000年夏季奧運會.md "wikilink")

## 地理

[Sydney_ASTER_2001_oct_12.jpg](https://zh.wikipedia.org/wiki/File:Sydney_ASTER_2001_oct_12.jpg "fig:Sydney_ASTER_2001_oct_12.jpg")的RS[衛星拍攝](../Page/衛星.md "wikilink")。悉尼的中心有三分一是位於上游小灣的南岸。\]\]
悉尼位於東面的[塔斯曼海與西面的](../Page/塔斯曼海.md "wikilink")[藍山之間的沿岸](../Page/藍山_\(澳大利亞\).md "wikilink")[盆地](../Page/沉積盆地.md "wikilink")。悉尼擁有全球最大的天然海港──傑克森港（Port
Jackson，即悉尼港），以及超過70個[海港和](../Page/海港.md "wikilink")[海灘](../Page/海灘.md "wikilink")，包括著名的[邦迪海灘](../Page/邦迪海灘.md "wikilink")。悉尼的市區佔地1,687平方公里（651平方英里），面積跟大[倫敦相若](../Page/倫敦.md "wikilink")。悉尼都會區（[悉尼統計局](../Page/悉尼統計局.md "wikilink")）佔地12,145平方公里（4,689平方英里），其有效範圍是[國家公園和其他未開發的土地](../Page/國家公園.md "wikilink")。

悉尼佔據了2個地理區域──[坎伯蘭峽谷](../Page/坎伯蘭峽谷.md "wikilink")（Cumberland
Plain）和[宏士比高原](../Page/宏士比高原.md "wikilink")（Hornsby
Plateau）。坎伯蘭峽谷是一個比較平坦，有些起伏的地域，橫臥於[傑克遜港以西和以南](../Page/傑克遜港.md "wikilink")。康士比高原是海港以北的[高原](../Page/高原.md "wikilink")，海拔200公尺（656英尺），被草木叢生的溪谷切割開。悉尼最舊的區域位於平坦的地區。宏士比高原，稱為[北岸](../Page/悉尼北岸.md "wikilink")，由於地勢陡峭，發展得較慢，一直以來是人跡罕至。後來1932年[悉尼港灣大橋啟用](../Page/悉尼港灣大橋.md "wikilink")，將高原與城市連接起來，才發展迅速。

### 氣候

悉尼屬於[副熱帶濕潤氣候](../Page/副熱帶濕潤氣候.md "wikilink")，全年平均气温变化较为和缓，降水无明显季节变化，由於東風調節，上半年的雨量稍微高一點。冬季温和湿润，气温很少降至5°C以下，降雪极为罕见（悉尼地区上一次降雪出现在1836年）。夏季相对炎热，多雨，平均温度虽然不高，高温天气也不多见，但在[热浪来袭时也常出现极端高温](../Page/热浪.md "wikilink")。最热月为1月，平均气温22.3°C，日最高气温超过30°C的天数为14.9天，极端最高气温45.8°C（2013年1月18日）。\[10\]。最冷月为7月，平均气温12.2°C，极端最低气温2.1°C（1932年6月22日）。年平均降水量约1214.3毫米。

雖然悉尼不會遭遇[氣旋或大](../Page/熱帶氣旋.md "wikilink")[地震的破坏](../Page/地震.md "wikilink")，但厄尔尼诺現象和[厄爾尼諾南方濤動正愈发深地影响着悉尼的天氣狀況](../Page/厄爾尼諾現象.md "wikilink")：乾旱和林區大火；風暴和洪水，都变得越来越频繁。悉尼有許多毗鄰森林的地區曾發生林區大火，尤其是1994年和2002年──林區大火傾向於春夏兩季發生。悉尼容易遭受罕見的雹暴和暴風的侵襲。

### 市區概況

[Sydney_opera_house_and_skyline.jpg](https://zh.wikipedia.org/wiki/File:Sydney_opera_house_and_skyline.jpg "fig:Sydney_opera_house_and_skyline.jpg")[天際線上的](../Page/天際線.md "wikilink")[悉尼歌劇院](../Page/悉尼歌劇院.md "wikilink")\]\]
[City_of_sydney_from_the_balmain_wharf_dusk_cropped2.jpg](https://zh.wikipedia.org/wiki/File:City_of_sydney_from_the_balmain_wharf_dusk_cropped2.jpg "fig:City_of_sydney_from_the_balmain_wharf_dusk_cropped2.jpg")
被悉尼市區覆蓋的大片地區，以往曾劃分為超過300個域區（郵政用途），現由約38個獨立的地方政府區域管轄（[新南威爾士州州政府及其機構的也有大量的職能](../Page/新南威爾士州.md "wikilink")）。現在的[悉尼市面積不大](../Page/悉尼市.md "wikilink")，佔地僅包括悉尼**中央商業區**及鄰近的**內城**城區。另外，悉尼有許多用於市區的區域非正式的劃分。但是，值得注意的是，悉尼有不少城區是不納入下列的非正式區劃的：[東郊](../Page/悉尼東郊.md "wikilink")、[山區](../Page/悉尼山區.md "wikilink")、[內西城](../Page/悉尼內西城.md "wikilink")、[下北岸](../Page/悉尼下北岸.md "wikilink")、[北部海灘](../Page/悉尼北部海灘.md "wikilink")、[北岸](../Page/悉尼北岸.md "wikilink")、[南悉尼](../Page/南悉尼.md "wikilink")、[東南悉尼](../Page/東南悉尼.md "wikilink")、[西南悉尼及](../Page/西南悉尼.md "wikilink")[西悉尼](../Page/西悉尼.md "wikilink")。

悉尼的[中央商業區從首個](../Page/悉尼中心商務區.md "wikilink")[歐洲殖民聚落](../Page/歐洲.md "wikilink")──[悉尼灣向南面延伸約](../Page/悉尼灣.md "wikilink")2[公里](../Page/公里.md "wikilink")（1.25[英里](../Page/英里.md "wikilink")）。[摩天大樓林立](../Page/摩天大樓.md "wikilink")，古跡包括[悉尼市政廳和](../Page/悉尼市政廳.md "wikilink")[维多利亚女王大厦等](../Page/维多利亚女王大厦.md "wikilink")[砂岩建筑](../Page/砂岩.md "wikilink")，及[悉尼议会大厦等公共建筑](../Page/悉尼议会大厦.md "wikilink")，設有公園如[溫亞公園](../Page/溫亞公園.md "wikilink")（Wynyard
Park）和[海德公園](../Page/海德公園_\(悉尼\).md "wikilink")（Hyde
Park）點綴。中央商業區東面鄰接不少公園──海德公園、[禁苑](../Page/悉尼禁苑.md "wikilink")（The
Domain）、[皇家植物園及](../Page/雪梨皇家植物園.md "wikilink")[悉尼港的](../Page/悉尼港.md "wikilink")[農場灣](../Page/農場灣_\(新南威爾士\).md "wikilink")。中央商業區西臨旅遊勝地[達令港](../Page/達令港.md "wikilink")。而[中央站正是中央商業區的盡頭](../Page/悉尼中央車站.md "wikilink")。[喬治街是悉尼中央商業區的南北大街](../Page/喬治街.md "wikilink")。在中央商業區南部，街道稍呈格狀走向，井然有序；相反，在較古舊的中央商業區北部，街道則較雜亂，這反映出悉尼早期的犍子小道的特殊發展。悉尼的街道多比澳洲其他城市的狹窄，也反映出其澳洲第一古城的特色。

雖然悉尼中央商業區早期是悉尼的商業與文化支柱，但自[第二次世界大戰後](../Page/第二次世界大戰.md "wikilink")，其他商業/文化區不斷向外發展。因此，二戰結束後，中央商業區的白領職位比例從60%以上下跌至2004年的30%以下。悉尼的5個最重要的外環商業區，包括南部的[好市圍](../Page/好市圍市.md "wikilink")、中西面的[帕拉瑪塔](../Page/帕拉瑪塔市.md "wikilink")，西面的[黑鎮](../Page/黑鎮市.md "wikilink")、西南面的[利物浦](../Page/利物浦市_\(新南威爾斯\).md "wikilink")、北面的[車士活及位於悉尼港以北的](../Page/车士活_\(新南威尔士州\).md "wikilink")[北悉尼](../Page/北悉尼議會黑鎮.md "wikilink")。

## 管轄

[Martinsplace.jpg](https://zh.wikipedia.org/wiki/File:Martinsplace.jpg "fig:Martinsplace.jpg")是悉尼市中心的广场，被称为悉尼的“城市心脏”。\]\]
悉尼曾於1945-1964年由[坎伯蘭郡管轄](../Page/坎伯蘭郡_\(新南威爾士州\).md "wikilink")。現時悉尼都會區沒有總管市政的行政機關，其事務已改為由都會區內各個[地方政府區域管理](../Page/地方政府區域.md "wikilink")。地方政府區域均設有選舉議會，負責[新南威爾士州委派的各項職責](../Page/新南威爾士州.md "wikilink")。

[悉尼市的範圍包括](../Page/悉尼市.md "wikilink")[商業中心區和一些鄰接的內城區](../Page/悉尼商業中心區.md "wikilink")，近年通過與鄰接的地方政府區域（如南悉尼）合併擴大面積。悉尼市由[悉尼市長和議會管轄](../Page/悉尼市長.md "wikilink")。悉尼市長往往被視為市的代表。

以下是悉尼的38個地方政府區域：

  - [艾士菲自治市](../Page/艾士菲自治市.md "wikilink")
  - [奧本議會](../Page/奧本議會.md "wikilink")
  - [賓士鎮市](../Page/賓士鎮市.md "wikilink")
  - [希爾斯郡](../Page/希爾斯郡.md "wikilink")
  - [黑鎮市](../Page/黑鎮市.md "wikilink")
  - [植物灣市](../Page/植物灣市.md "wikilink")
  - [寶活議會](../Page/寶活議會.md "wikilink")

<!-- end list -->

  - [康頓議會](../Page/康頓議會.md "wikilink")
  - [金寶鎮市](../Page/金寶鎮市.md "wikilink")
  - [加拿大灣市](../Page/加拿大灣市.md "wikilink")
  - [坎特伯雷市](../Page/坎特伯雷市.md "wikilink")
  - [費菲市](../Page/費菲市.md "wikilink")
  - [好萊德市](../Page/好萊德市.md "wikilink")
  - [康士比郡](../Page/康士比郡.md "wikilink")

<!-- end list -->

  - [獵人山自治市](../Page/獵人山自治市.md "wikilink")
  - [好市圍市](../Page/好市圍市.md "wikilink")
  - [高嘉華自治市](../Page/高嘉華自治市.md "wikilink")
  - [顧林凱議會](../Page/顧林凱議會.md "wikilink")
  - [蘭科夫自治市](../Page/蘭科夫自治市.md "wikilink")
  - [萊卡特自治市](../Page/萊卡特自治市.md "wikilink")
  - [利物浦市](../Page/利物浦市_\(新南威爾斯\).md "wikilink")

<!-- end list -->

  - [曼利議會](../Page/曼利議會.md "wikilink")
  - [馬利維議會](../Page/馬利維議會.md "wikilink")
  - [摩士曼自治市](../Page/摩士曼自治市.md "wikilink")
  - [北悉尼議會](../Page/北悉尼議會.md "wikilink")
  - [帕拉瑪塔市](../Page/帕拉瑪塔市.md "wikilink")
  - [彭里斯市](../Page/彭里斯市.md "wikilink")
  - [碧水議會](../Page/碧水議會.md "wikilink")

<!-- end list -->

  - [蘭域市](../Page/蘭域市.md "wikilink")
  - [石谷市](../Page/石谷市.md "wikilink")
  - [賴德市](../Page/賴德市.md "wikilink")
  - [史卓菲自治市](../Page/史卓菲自治市.md "wikilink")
  - [薩瑟蘭郡](../Page/薩瑟蘭郡.md "wikilink")
  - [悉尼市](../Page/悉尼市.md "wikilink")
  - [華令加議會](../Page/華令加議會.md "wikilink")

<!-- end list -->

  - [韋佛利議會](../Page/韋佛利議會.md "wikilink")
  - [威樂比市](../Page/威樂比市.md "wikilink")
  - [胡拉勒自治市](../Page/胡拉勒自治市.md "wikilink")

悉尼大多數的市府活動是由州政府指導的。這些活動包括公共[交通](../Page/交通.md "wikilink")、主要[道路](../Page/道路.md "wikilink")、交通管制、[警務](../Page/警察.md "wikilink")、[幼稚園](../Page/幼稚園.md "wikilink")（幼兒園）以上的[教育](../Page/教育.md "wikilink")，以及重大的基礎建設項目。由於悉尼擁有新南威爾士州的大部分[人口](../Page/人口.md "wikilink")，州政府傳統上不願意發展悉尼全市的政府機構，避免雙方因而競爭。正因如此，悉尼經常成為州議會與[聯邦議會的](../Page/澳洲政府.md "wikilink")[政治焦點](../Page/政治.md "wikilink")。舉例，自1945年起，悉尼市的選舉範圍在至少4個時刻，被州政府大力改變，以為當時的新南威爾士州議會的執政黨製造優勢。

随着大悉尼区近年的扩张，新增合并几个地方议会是：

  - [蓝山市](../Page/蓝山市.md "wikilink")
  - [霍克斯伯里市](../Page/霍克斯伯里市.md "wikilink")
  - [臥龍迪利郡](../Page/臥龍迪利郡.md "wikilink")
  - [怀昂郡](../Page/怀昂郡.md "wikilink")
  - [戈斯福德](../Page/戈斯福德.md "wikilink")

这样大悉尼区的地方议会已经增加到43个。按照最新的大悉尼规划分区合并划分为悉尼12个内城区为：

内城区：

  - [悉尼市](../Page/悉尼市.md "wikilink")
  - [史卓菲自治市](../Page/史卓菲自治市.md "wikilink") \*
    [寶活議會](../Page/寶活議會.md "wikilink") \*
    [加拿大灣市](../Page/加拿大灣市.md "wikilink") (拟合并)
  - [艾士菲自治市](../Page/艾士菲自治市.md "wikilink") \*
    [萊卡特自治市](../Page/萊卡特自治市.md "wikilink") \*
    [馬利維議會](../Page/馬利維議會.md "wikilink") (拟合并)
  - [植物灣市](../Page/植物灣市.md "wikilink") \*
    [石谷市](../Page/石谷市.md "wikilink") (拟合并)
  - [蘭域市](../Page/蘭域市.md "wikilink") \*
    [韋佛利議會](../Page/韋佛利議會.md "wikilink") \*
    [胡拉勒自治市](../Page/胡拉勒自治市.md "wikilink") (拟合并)
  - [曼利議會](../Page/曼利議會.md "wikilink") \*
    [摩士曼自治市](../Page/摩士曼自治市.md "wikilink") \*
    [華令加議會](../Page/華令加議會.md "wikilink")(南部) (拟合并)
  - [北悉尼議會](../Page/北悉尼議會.md "wikilink") \*
    [威樂比市](../Page/威樂比市.md "wikilink") (拟合并)
  - [賴德市](../Page/賴德市.md "wikilink") \*
    [獵人山自治市](../Page/獵人山自治市.md "wikilink") \*
    [蘭科夫自治市](../Page/蘭科夫自治市.md "wikilink") (拟合并)
  - [坎特伯雷市](../Page/坎特伯雷市.md "wikilink") \*
    [賓士鎮市](../Page/賓士鎮市.md "wikilink") (拟合并)
  - [好市圍市](../Page/好市圍市.md "wikilink") \*
    [高嘉華自治市](../Page/高嘉華自治市.md "wikilink") (拟合并)
  - [帕拉瑪塔市](../Page/帕拉瑪塔市.md "wikilink") \*
    [奧本議會](../Page/奧本議會.md "wikilink")(北部) (拟合并)
  - [好萊德市](../Page/好萊德市.md "wikilink") \*
    [奧本議會](../Page/奧本議會.md "wikilink")(南部) (拟合并)

共12个议会(COUNCIL).

## 經濟

2003年9月，悉尼的失業率是5.3%。\[11\]2005年12月，在澳洲眾多首府中，悉尼以485,000[澳元錄得最高的樓價中位數](../Page/澳元.md "wikilink")。\[12\]根據《[經濟學人](../Page/經濟學人.md "wikilink")》全球訊息部的[生活指數調查](../Page/生活指數.md "wikilink")，悉尼是全球第6大最昂貴的城市。而[瑞銀集團的調查則以收入淨值將悉尼列為全球第](../Page/瑞銀集團.md "wikilink")20位。\[13\]

根據[经济合作与发展组织於](../Page/经济合作与发展组织.md "wikilink")2005年11月出版的報告，在西方世界中，[澳洲擁有最多估價過高的樓宇](../Page/澳洲.md "wikilink")，樓價比證明租值的高出52%。\[14\]

悉尼經濟體系裏最大型、擁有最大部分在職人士的行業包括財產及商業服務業、零售業、製造業、健康及社會服務業。\[15\]1980年代起，職位從製造業遷移至服務及資訊行業。

現在，悉尼發展成[澳洲最大的](../Page/澳洲.md "wikilink")[集團與](../Page/集團.md "wikilink")[金融中心](../Page/金融.md "wikilink")，也是[亞太地區的重要金融中心](../Page/亞太地區.md "wikilink")。\[16\]
悉尼是**[澳洲證券交易所](../Page/澳洲證券交易所.md "wikilink")**、**[澳洲儲備銀行](../Page/澳洲儲備銀行.md "wikilink")**以及許多本國銀行與澳洲集團的全國總部。悉尼也是不少跨國集團的地區總部。[二十世紀福斯在悉尼設有大型製片廠](../Page/二十世紀福斯.md "wikilink")。[悉尼期貨交易所是](../Page/澳洲证券交易所.md "wikilink")[亞太地區最大的金融期貨與期權交易所之一](../Page/亞太地區.md "wikilink")，2005年有總值6,430萬[澳元的合同交易](../Page/澳元.md "wikilink")。悉尼期交所是全球第12大期貨市場，以及第19大期貨與期權市場。

悉尼是著名的旅遊勝地，2004年有780萬名本地旅客和250萬名國際旅客。\[17\]
[Sydney_Harbour_Bridge_night.jpg](https://zh.wikipedia.org/wiki/File:Sydney_Harbour_Bridge_night.jpg "fig:Sydney_Harbour_Bridge_night.jpg")

## 人口統計

2003年，悉尼住有4,198,543人。\[18\]，[人口密度為每平方公里](../Page/人口.md "wikilink")345.7人。\[19\]
**內悉尼**是澳洲人口最稠密的地方，每平方公里有4,023人。\[20\]

[Chinatownsyd.jpg](https://zh.wikipedia.org/wiki/File:Chinatownsyd.jpg "fig:Chinatownsyd.jpg")\]\]
[2001年的人口普查中](../Page/2001年澳大利亞人口普查.md "wikilink")，悉尼市民最普遍把自己的血統形容為[澳洲人](../Page/澳洲人.md "wikilink")、[英國人及](../Page/英國人.md "wikilink")[愛爾蘭人](../Page/愛爾蘭人.md "wikilink")。人口普查亦顯示，1%的悉尼人口擁有[澳洲原住民血統](../Page/澳洲原住民.md "wikilink")，31.2%的人口生於海外。三大移民來源地為[英國](../Page/英國.md "wikilink")、[大中華地區及](../Page/大中華地區.md "wikilink")[紐西蘭](../Page/紐西蘭.md "wikilink")。另外，不少移民來自[越南](../Page/越南.md "wikilink")、[黎巴嫩](../Page/黎巴嫩.md "wikilink")、[意大利](../Page/意大利.md "wikilink")、[印度及](../Page/印度.md "wikilink")[菲律賓](../Page/菲律賓.md "wikilink")。大多數悉尼人是以[英語為母語的](../Page/澳洲英語.md "wikilink")；不少會說第二語言，最普遍的是[漢語](../Page/漢語.md "wikilink")（[粵語為主](../Page/粵語.md "wikilink")，以及[普通話](../Page/普通話.md "wikilink")）、[阿拉伯語](../Page/阿拉伯語.md "wikilink")（包括[黎巴嫩語](../Page/黎巴嫩語.md "wikilink")）及[希臘語](../Page/希臘語.md "wikilink")。\[21\]

一些族群與最初聚居的域區結合起來：[意大利人與](../Page/意大利.md "wikilink")[萊卡特](../Page/萊卡特_\(新南威爾士\).md "wikilink")（Leichhardt）結合，[希臘人與](../Page/希臘人.md "wikilink")[石谷](../Page/石谷_\(新南威爾士\).md "wikilink")（Rockdale）和[好士維](../Page/赫斯特维尔_\(新南威尔士州\).md "wikilink")（Hurstville）結合，[黎巴嫩人與](../Page/黎巴嫩人.md "wikilink")[湖吾巴](../Page/湖吾巴_\(新南威爾士\).md "wikilink")（Lakemba）和[賓士鎮](../Page/班克斯_\(新南威爾士\).md "wikilink")（Bankstown）結合，[韓國人與](../Page/韓國人.md "wikilink")[墾思](../Page/墾思_\(新南威爾士\).md "wikilink")（Campsie）、[愛蘋](../Page/艾坪.md "wikilink")（Epping）結合，[猶太人與](../Page/猶太人.md "wikilink")[邦迪](../Page/邦迪.md "wikilink")（Bondi）和[玫瑰灣](../Page/玫瑰灣.md "wikilink")（Rose
Bay）結合，[印度人與](../Page/印度人.md "wikilink")和[帕拉瑪塔](../Page/帕拉瑪塔_\(新南威爾士\).md "wikilink")（Parramatta）結合，[華人與](../Page/華人.md "wikilink")[乾草市場](../Page/乾草市場_\(新南威爾士\).md "wikilink")（Haymarket）（悉尼的[唐人街在這裏出現](../Page/悉尼唐人街.md "wikilink")）結合，而[越南人則與](../Page/越南人.md "wikilink")[卡巴瑪塔](../Page/卡巴瑪塔_\(新南威爾士\).md "wikilink")（Cabramatta）結合。[紅蕨](../Page/紅蕨_\(新南威爾士\).md "wikilink")（Redfern）則住有大量的澳洲原住民。

2003年，悉尼市民的年齡中位數是34歲，12%的[人口是超過](../Page/人口.md "wikilink")65歲。\[22\]2006年，15.2%的悉尼市民的教育程度至少達至[學士學位](../Page/學士.md "wikilink")。\[23\]，比全國的平均值19%為低。大約55%的悉尼市民信奉基督教，最普遍的教派為[天主教和](../Page/天主教.md "wikilink")[澳洲圣公会](../Page/澳洲圣公会.md "wikilink")。大約9%的人口信奉[佛教](../Page/佛教.md "wikilink")，大約有32%沒有[宗教信仰](../Page/宗教.md "wikilink")。\[24\]

## 教育

[University_of_Sydney_Main_Quadrangle.jpg](https://zh.wikipedia.org/wiki/File:University_of_Sydney_Main_Quadrangle.jpg "fig:University_of_Sydney_Main_Quadrangle.jpg")是[澳洲歷史最悠久的](../Page/澳洲.md "wikilink")[大學](../Page/大學.md "wikilink")。\]\]
悉尼拥有完善的初、中、高等教育设施。按照[新南威尔士州的教育体系](../Page/新南威尔士州#教育.md "wikilink")，小学（包括[幼稚園](../Page/幼稚園.md "wikilink")）和中学可以分为由州政府管理的[公立學校](../Page/公立學校.md "wikilink")、由[天主教会管理的天主教](../Page/天主教会.md "wikilink")[教會學校](../Page/教會學校.md "wikilink")，以及独立的和由其他宗教组织管理的[私立學校](../Page/私立學校.md "wikilink")。行政上，悉尼分为4個州轄学區，下辖919所各类學校。一部分中学需要考试入学，在当地称为“精英中学”。

悉尼是不少[澳洲著名大學的所在地](../Page/澳洲.md "wikilink")，包括於1850年創辦的澳洲第一所[大學](../Page/大學.md "wikilink")──**[悉尼大學](../Page/悉尼大學.md "wikilink")**。\[25\]
澳洲還有五所其它公立大學的主要校區在悉尼：**[新南威爾士大學](../Page/新南威爾士大學.md "wikilink")**、**[麥覺理大學](../Page/麥覺理大學.md "wikilink")**、**[悉尼科技大学](../Page/悉尼科技大学.md "wikilink")**、**[西悉尼大学](../Page/西悉尼大学.md "wikilink")**及[澳洲天主教大學](../Page/澳洲天主教大學.md "wikilink")（6個校區，2個設於悉尼）。主要校區不在悉尼、但在悉尼開辦分校區的大學有[澳洲聖母大學及](../Page/澳洲聖母大學.md "wikilink")**[臥龍崗大學](../Page/臥龍崗大學.md "wikilink")**。

另外悉尼亦有四所多校園的公立技術及進修學院，提供高等職業培训：[悉尼技術學院](../Page/悉尼技術學院.md "wikilink")、[北悉尼技術及進修學院](../Page/北悉尼技術及進修學院.md "wikilink")、[西悉尼技術及進修學院及](../Page/西悉尼技術及進修學院.md "wikilink")[西南悉尼技術及進修學院](../Page/西南悉尼技術及進修學院.md "wikilink")。

## 文化

[Map_of_Sydney_central_bus_district.PNG](https://zh.wikipedia.org/wiki/File:Map_of_Sydney_central_bus_district.PNG "fig:Map_of_Sydney_central_bus_district.PNG")

### 音樂和歌劇

悉尼擁有本地的[音樂與](../Page/音樂.md "wikilink")[劇場團體](../Page/劇場.md "wikilink")，包括[悉尼交響樂團](../Page/悉尼交響樂團.md "wikilink")、[悉尼戲劇團及](../Page/悉尼戲劇團.md "wikilink")[悉尼舞蹈團](../Page/悉尼舞蹈團.md "wikilink")。一年一度的文化盛事，包括[新南威尔士美术馆舉辦的比賽](../Page/新南威尔士美术馆.md "wikilink")──[阿契博爾德獎](../Page/阿契博爾德獎.md "wikilink")（Archibald
Prize），以及在1月舉辦的音樂、劇場與視覺藝術慶典──**[悉尼節](../Page/悉尼節.md "wikilink")**。

[悉尼歌剧院有](../Page/悉尼歌剧院.md "wikilink")5個[劇場](../Page/劇場.md "wikilink")，有能力主辦一系列的表演模式。2006年7個月裏，悉尼歌劇院是全球第三大熱鬧的劇團──[澳洲歌劇團的所在地](../Page/澳洲歌劇團.md "wikilink")。\[26\]
其他場地包括[悉尼市政廳](../Page/悉尼市政廳.md "wikilink")、[城市演奏廳](../Page/城市演奏廳.md "wikilink")、[新南威爾士州劇場及碼頭劇院](../Page/新南威爾士州.md "wikilink")。

[悉尼交響樂團於](../Page/悉尼交響樂團.md "wikilink")[悉尼歌剧院表演](../Page/悉尼歌剧院.md "wikilink")。[悉尼舞蹈團](../Page/悉尼舞蹈團.md "wikilink")20世紀末在編舞家[葛倫·墨菲](../Page/葛倫·墨菲.md "wikilink")（）的領導下也備受推崇。在[澳洲成立的](../Page/澳洲.md "wikilink")[搖滾樂隊包括](../Page/搖滾樂隊.md "wikilink")、[INXS](../Page/INXS.md "wikilink")、[电波鸟人](../Page/电波鸟人乐队.md "wikilink")、[獨立搖滾歌手](../Page/獨立搖滾.md "wikilink")和,
[電子音樂](../Page/電子音樂.md "wikilink") 的開拓者有, 單槍原理（Single Gun
Theory）和。爵士樂隊則有在地下室和海邊進行演奏的[頸](../Page/頸.md "wikilink")。

悉尼培育出多位藝術家，例如、、[Brett
Whiteley](../Page/Brett_Whiteley.md "wikilink")、[約翰·奧森和澳洲著名詩人](../Page/John_Olsen_\(artist\).md "wikilink")。

悉尼擁有多間戲劇機構，位於新鎮的[新劇院在](../Page/新劇院_\(悉尼\).md "wikilink")2007年慶祝了75歲生日。另外還有其它戲劇團，例如和。由1940年代至1970年代，一個群作家結合組織名為[悉尼推](../Page/悉尼推.md "wikilink")，主要是推動城市的文化，主要的成員包括作家[哲米·格瑞亞](../Page/哲米·格瑞亞.md "wikilink")（Germaine
Greer）。

在[電影方面](../Page/悉尼電影.md "wikilink")，在1998年建設了[霍士電影城](../Page/霍士電影城.md "wikilink")。位於[京士頓的](../Page/京士頓_\(悉尼\).md "wikilink")[国家戏剧艺术学院以培育著名藝人為榮](../Page/国家戏剧艺术学院.md "wikilink")，例如[米路·吉遜和](../Page/米路·吉遜.md "wikilink")[巴茲·雷曼](../Page/巴茲·雷曼.md "wikilink")（Baz
Luhrmann）。另外很多電影取景於悉尼，例如《[不可能的任務2](../Page/不可能的任務2.md "wikilink")》、星際大戰電影的《[第二輯](../Page/星際大戰二部曲：複製人全面進攻.md "wikilink")》和《[第三輯](../Page/星際大戰三部曲：西斯大帝的復仇.md "wikilink")》和《[駭客任務](../Page/駭客任務.md "wikilink")》。另外《[海底總動員](../Page/海底總動員.md "wikilink")（海度奇兵）》和《[不可能的任務2](../Page/不可能的任務2.md "wikilink")》都是以悉尼作為藍本。

### 娛樂

[Luna_Park-Sydney-Australia.JPG](https://zh.wikipedia.org/wiki/File:Luna_Park-Sydney-Australia.JPG "fig:Luna_Park-Sydney-Australia.JPG")\]\]
悉尼擁有很多娛樂性的節日，[悉尼節就是其中的佼佼者](../Page/悉尼節.md "wikilink")。它是一個集合室內和室外表演的節日，是在1月舉行節日。另外有一個名為[大日子不在](../Page/大日子不在.md "wikilink")（Big
Day Out）的節日，這是一個搖滾樂手
表演的節日，是一個始於悉尼的節日。[悉尼同志遊行](../Page/悉尼同志遊行.md "wikilink")（Sydney
Gay and Lesbian Mardi
Gras）是一個位於牛津道的節日。悉尼亦包括一些小型節日例如[悉尼電影節](../Page/悉尼電影節.md "wikilink")、位於[悉尼奧林匹克公園舉行的](../Page/悉尼奧林匹克公園.md "wikilink")[皇家復活節展覽會](../Page/皇家復活節展覽會.md "wikilink")、在四/五月舉行的[澳洲時裝周](../Page/澳洲時裝周.md "wikilink")。另外悉尼亦經常大規模地慶祝[元旦和](../Page/元旦.md "wikilink")[澳洲日](../Page/澳洲日.md "wikilink")。

### 博物館

悉尼擁有許多博物館，大多位於都會區內。最大的要數[澳洲博物館了](../Page/澳洲博物館.md "wikilink")（Australian
Museum，主要為自然史與人類學），另外有[發電廠博物館](../Page/發電廠博物館.md "wikilink")（Powerhouse
Museum，科學、科技與設計）、[新南威尔士美术馆](../Page/新南威尔士美术馆.md "wikilink")（Art Gallery
of New South Wales）、[悉尼當代藝術館](../Page/悉尼當代藝術館.md "wikilink") （Museum of
Contemporary
Art）、[澳洲國家海事博物館](../Page/澳洲國家海事博物館.md "wikilink")，另外還有一些較小型的博物館，如：[悉尼博物館](../Page/悉尼博物館.md "wikilink")（Museum
of Sydney, MoS，地方史）。

### 公園與花園

[Chinese_Garden_of_Friendship.jpg](https://zh.wikipedia.org/wiki/File:Chinese_Garden_of_Friendship.jpg "fig:Chinese_Garden_of_Friendship.jpg")\]\]
悉尼的露天場所得天獨厚，市區內也有許多自然空間。位在市中心的有[中國友誼花園](../Page/谊园.md "wikilink")、[海德公園與](../Page/海德公園_\(悉尼\).md "wikilink")[皇家植物園](../Page/皇家植物園.md "wikilink")。大悉尼區還有數個[國家公園](../Page/國家公園.md "wikilink")，其中包括全球第二個成立的國家公園—[皇家國家公園](../Page/皇家國家公園.md "wikilink")（第一位是美國[黃石國家公園](../Page/黃石國家公園.md "wikilink")），佔地132[平方公里](../Page/平方公里.md "wikilink")。\[27\]

### 媒體

[Sydney_New_Year's_Eve_2.jpg](https://zh.wikipedia.org/wiki/File:Sydney_New_Year's_Eve_2.jpg "fig:Sydney_New_Year's_Eve_2.jpg")時皆會發放世界聞名的煙火表演。\]\]
悉尼有兩份主要日報。《[悉尼晨鋒報](../Page/悉尼晨鋒報.md "wikilink")》（*The Sydney Morning
Herald*）是一份中左派大報，也是悉尼涵蓋大量國內外新聞，[文化和商務的記事報紙](../Page/文化.md "wikilink")；它也是澳洲現存歷史最久的報紙，從1831年起就開始被定期發行。它的競爭者是[新聞集團旗下的](../Page/新聞集團.md "wikilink")[每日電訊報](../Page/每日電訊報.md "wikilink")，一份中右翼主流報紙。[\[\[每日電訊報|每日電訊報\<nowiki\>\]\]發行量是\[\[悉尼晨鋒報|悉尼晨鋒報\<nowiki\>\]\]的三倍左右（2017年）](https://en.wikipedia.org/wiki/List_of_newspapers_in_Australia_by_circulation)。兩份報紙都有相應的小型報紙于週日發行，分別為*太陽日報*（Sun-Herald）和
*星期天電訊報*（Sunday Telegraph）。

三個商業[電視網路](../Page/電視.md "wikilink")（[第7電視網](../Page/七號電視網.md "wikilink"),
[第9電視網和](../Page/九號電視網.md "wikilink")[第10電視網](../Page/十號電視網.md "wikilink")）以及政府擁有的全國廣播電視（[澳大利亚广播公司](../Page/澳大利亚广播公司.md "wikilink")（**A**ustralian
**B**roadcasting **C**orporation）和[SBS](../Page/特殊廣播服務.md "wikilink")）
都在悉尼設有總部。以往這些電視網路的本部都處于北岸，但在進十年已經有數家搬入內城區。第9電視網繼續保有他們在悉尼港北岸，位于
的總部。第10電視網則擁有位于內城區
[派蒙的一個重建區域的演播室](../Page/派蒙.md "wikilink")。第7電視網總部也設在派蒙，並在愛蘋有一個演播室，以及一個新計劃建造在中央商業區[馬丁廣場的新聞演播室](../Page/馬丁廣場.md "wikilink")。ABC在相鄰的勾恬冒有一個大型的總部和製作設備。SBS的演播室位于[亞它門](../Page/亞它門.md "wikilink")
。Foxtel和[Optus](../Page/Optus.md "wikilink")
都通過它們的電纜對大部分的市區提供付費電視服務。2001年1月5個免費電視台完成數碼化工程。額外的電視服務包括有ABC第2台（頻道21）、SBS國際新聞台
— SBS2（頻道4）、ABC新聞，體育和天氣台（頻度41）、政府管有的
[新南威爾斯第1台](https://web.archive.org/web/20091030054918/http://www.digitaltv.nsw.gov.au/)（頻度45）,
澳洲基督台（頻度46）、麥格理電視台（頻度47）、TAB體育台（頻度48）、家中購物博覽台（頻度 49）和國會聯邦電台。

### 電台

悉尼擁有很多公營和私營的[AM和](../Page/振幅調變.md "wikilink")[FM電台](../Page/FM.md "wikilink")，包括本地的[702ABC電台](../Page/702ABC電台.md "wikilink")（前2BL電台）；另外有一些以互動為主的電台，包括[2GB](../Page/2GB.md "wikilink")、和[2UE](../Page/2UE.md "wikilink")，屬於FM電台的[織女星電台亦是其中的一分子](../Page/織女星電台.md "wikilink")；悉尼亦包括一些音樂電台，包括[3個M電台](../Page/3個M電台.md "wikilink")、[2日FM電台和](../Page/2日FM電台.md "wikilink")[新星96.9電台](../Page/新星96.9電台.md "wikilink")；當然亦包括一些本地化的電台，例如[3個J電台](../Page/3個J電台.md "wikilink")、[2SER電台和](../Page/2SER電台.md "wikilink")[FBi電台](../Page/FBi電台.md "wikilink")。除此之外亦有一些以其它種族主要聽眾的電台，詳情請見[澳洲電台列表](../Page/澳洲電台列表.md "wikilink")。

## 體育

悉尼是**[澳洲欖球聯盟](../Page/澳洲欖球聯盟.md "wikilink")**的總部、16支[全國橄欖球聯賽](../Page/全國橄欖球聯賽.md "wikilink")（NRL）球隊其中8支（、、、、、、及）的根據地，以及的北部基地（這球隊同時以[臥龍崗為基地](../Page/臥龍崗.md "wikilink")）。悉尼的[澳大利亞體育場](../Page/澳大利亞體育場.md "wikilink")（Telstra
Stadium）是NRL的[總決賽場地](../Page/總決賽.md "wikilink")。
[Sydney_olympic_stadium_track_and_field.jpg](https://zh.wikipedia.org/wiki/File:Sydney_olympic_stadium_track_and_field.jpg "fig:Sydney_olympic_stadium_track_and_field.jpg")期間，澳洲體育場（現稱「澳新銀行體育場」，前稱「澳洲電信體育場」）場內的[田徑項目](../Page/田徑.md "wikilink")\]\]

悉尼有些球隊是一些體育聯盟的成員。包括[悉尼天鵝](../Page/悉尼天鵝澳式足球會.md "wikilink")（[澳大利亞澳式足球聯盟](../Page/澳大利亞澳式足球聯盟.md "wikilink")）、[悉尼足球俱樂部](../Page/悉尼足球俱樂部.md "wikilink")
（[澳洲甲組足球聯賽](../Page/澳洲甲組足球聯賽.md "wikilink")）、[悉尼皇帝](../Page/悉尼皇帝.md "wikilink")
和[西悉尼尖背](../Page/西悉尼尖背.md "wikilink")
（[國家籃球聯盟](../Page/National_Basketball_League_\(Australia\).md "wikilink")）、[悉尼大學火焰](../Page/悉尼大學火焰.md "wikilink")
（[國家女子籃球聯盟](../Page/Women's_National_Basketball_League.md "wikilink")）,
[悉尼藍色](../Page/悉尼藍色.md "wikilink")
（[澳洲主籃球盟](../Page/澳洲主籃球盟.md "wikilink")）、[悉尼褐雨燕](../Page/悉尼褐雨燕.md "wikilink")（[澳洲聯邦銀行大獎賽](../Page/澳洲聯邦銀行大獎賽.md "wikilink")）和[新南威爾斯藍色](../Page/新南威爾斯藍色.md "wikilink")
（[頭等板球賽](../Page/頭等板球賽.md "wikilink")）。

1864年，澳洲的首個[欖球會在](../Page/欖球會.md "wikilink")[悉尼大學成立](../Page/悉尼大學.md "wikilink")。1882年，首屆跨殖民地運動會揭幕，悉尼迎戰。現在，國家隊在[超級橄欖球聯賽中以](../Page/超級橄欖球聯賽.md "wikilink")應戰。悉尼會派出兩支隊伍參加[國家欖球比賽](../Page/國家欖球比賽.md "wikilink")。傳統的球會賽事有。悉尼亦會定期舉辦[澳洲国家橄榄球队國際賽](../Page/澳洲国家橄榄球队.md "wikilink")。

悉尼曾經舉辦[1938年的英聯邦運動會和](../Page/英聯邦運動會.md "wikilink")[2000年夏季奧運會](../Page/2000年夏季奧林匹克運動會.md "wikilink")，並建設了[悉尼奧林匹克公園和](../Page/悉尼奧林匹克公園.md "wikilink")[悉尼足球體育館等地方](../Page/悉尼足球體育館.md "wikilink")。[悉尼奧林匹克公園為多個大型體育活動](../Page/悉尼奧林匹克公園.md "wikilink")（例如國家[澳式足球聯賽的總決賽](../Page/澳式足球聯賽.md "wikilink")、[起源洲賽](../Page/起源洲賽.md "wikilink")（Rugby
League State of
Origin）和2006年世界盃預賽等等）提供了場地。[悉尼足球場是](../Page/悉尼足球場.md "wikilink")[悉尼公雞](../Page/悉尼公雞.md "wikilink")、[悉尼FC和](../Page/悉尼FC.md "wikilink")[新南威爾斯瓦蛙它的主場](../Page/新南威爾斯瓦蛙它.md "wikilink")，鄰近的[悉尼板球體育場是悉尼板球和](../Page/悉尼板球體育場.md "wikilink")[澳式足球的中心](../Page/澳式足球.md "wikilink")，曾舉行多項體育賽事。

水上運動方面，悉尼經常舉辦很多遊艇比賽，例如在聖誕節的次日會舉辦[悉尼至霍巴特遊艇比賽](../Page/悉尼至霍巴特遊艇比賽.md "wikilink")。悉尼的海港是一個適合進行[遊艇](../Page/遊艇.md "wikilink")、小型賽艇、[龍舟和釣魚活動的好地方](../Page/龍舟.md "wikilink")。另外悉尼擁有很多遊客值得遊覽和適合進行陸上和水上運動的海灘，例如[邦迪海灘](../Page/邦迪海灘.md "wikilink")、[曼利海灘](../Page/曼利海灘.md "wikilink")、[棕櫚樹海灘和](../Page/棕櫚樹海灘.md "wikilink")[克羅努拉海灘等等](../Page/克羅努拉海灘.md "wikilink")。

在賽馬運動方面，悉尼當地亦有唐加士打一哩賽、女皇伊利沙伯錦標、葉森讓賽等多項一級賽在蘭域馬場上演。

## 基礎建設

### 醫療

新南威爾士州政府經營許多[公營醫院](../Page/公營醫院.md "wikilink")，由四個類似公共醫療衛生服務機搆來管理這些醫院。包括西南悉尼衛生
（Sydney South West Health）、西悉尼地區（Sydney West Area）、北悉尼（Northern
Sydney）、[中海岸](../Page/中海岸.md "wikilink")（Central Coast）、東南悉尼（Sydney
South East）和伊拉瓦拉（Illawarra Area）各自管理自己屬下的醫院和特別醫療保健中心。另外悉尼也有一些私營醫院。

### 交通

[Sydney-Monorail.jpg](https://zh.wikipedia.org/wiki/File:Sydney-Monorail.jpg "fig:Sydney-Monorail.jpg")\]\]

#### 鐵路

悉尼配備龐大的[火車](../Page/火車.md "wikilink")、[巴士和](../Page/巴士.md "wikilink")[渡輪網絡](../Page/渡輪.md "wikilink")。悉尼的火車由[新南威爾士州州政府的集團](../Page/新南威爾士州.md "wikilink")──[新南威爾士州鐵路局](../Page/新南威爾士州鐵路局.md "wikilink")（RailCorp）營運。悉尼的鐵路網全部集中在都會區的地底，即是[市環線](../Page/市環線.md "wikilink")。然後經過[中央站或是](../Page/悉尼中央車站.md "wikilink")[悉尼海港大橋到達各個](../Page/悉尼海港大橋.md "wikilink")[市郊](../Page/市郊.md "wikilink")，這種服務是由[城市鐵路](../Page/城市鐵路_\(悉尼\).md "wikilink")（Sydney
Trains）提供。另外火車亦提供往返新南威爾斯各城鎮的區域鐵路服務，這就是由[新州鐵路](../Page/新州鐵路.md "wikilink")（NSW
TrainLink）所提供。不過在2000年夏季奧运會接下來的幾年，城市鐵路的性能显著下降。大眾的憤怒，以致鐵路當局引入新時間表、雇用更多司機，並定下大基礎建設計劃，尤其是[城市鐵路路線清理計畫](../Page/城市鐵路路線清理計畫.md "wikilink")。新時間表旨在削减服務，以達至更可靠。在一些路線，如內西區線路段，火車服務在高峰時刻才運作。除了自2004年的改進措施外，基礎建設的改進措施不預期會峻工，直至2010年。

悉尼有一條[輕軌鐵路綫](../Page/輕軌.md "wikilink")──[悉尼城市輕軌](../Page/悉尼城市輕軌.md "wikilink")（Sydney
Light
Rail），沿舊時的貨運路線，貫通[中央火車站經](../Page/悉尼中央車站.md "wikilink")[莉莉田到](../Page/莉莉田.md "wikilink")[達利奇山](../Page/達利奇山.md "wikilink")（Dulwich
Hill），另有第二條輕軌綫在建，將南北貫通市中心，並經過中央火車站往東南郊，連接新南威爾士大學、[蘭域跑馬廳等](../Page/蘭域市.md "wikilink")。

悉尼過去還有[悉尼單軌電車](../Page/悉尼單軌電車.md "wikilink")（*Sydney
Monorail*）循環行駛主要的購物區域和[情人港](../Page/情人港.md "wikilink")（*Darling
Harbour*），該服務主要服務遊客，但因路軌覆蓋區域太小，並不便於往返，並於2013年停運拆除。悉尼以前配備龐大的[電車網絡](../Page/電車網絡.md "wikilink")，但在1950和1960年代逐漸停辦。

都會地區的大部分地區都配備巴士，大多依照1963年前的電車路線。在市區與[市郊的巴土路線都是由](../Page/市郊.md "wikilink")[悉尼巴士公司管有](../Page/悉尼巴士公司.md "wikilink")，班次較為頻密。至於較遠的地區就有不同的私人公共汽車公司服務，不過這些巴士服務被批評為沒有安全保障和非繁忙時間時經常缺少服務。

#### 船運服務

船運服務方面，悉尼主要有政府管有的提供服務。[環形碼頭位於悉尼都會區的北面](../Page/環形碼頭.md "wikilink")，是悉尼主要渡輸服務的集匯處，能運行往悉尼港和附近的碼頭（包括東郊、帕拉馬搭、曼力等）。

#### 公路

雪梨配備龐大的高速公路和收費公路網絡，但主要都在市郊。這是由於不少社區的居民都不希望高速公路影響到他們日常寧靜的生活。

新建的[雪梨過海隧道設在悉尼海港下](../Page/雪梨過海隧道.md "wikilink")，連接悉尼市和東郊。不過隧道經常被批評收費太高和阻礙附近道路的運行。另外[太平洋高速公路](../Page/太平洋高速公路.md "wikilink")（Pacific
Freeway）是澳洲的主要公路，行走悉尼的南面。

#### 機場

[京斯福特·史密斯機場](../Page/京斯福特·史密斯機場.md "wikilink")（*Kingsford Smith
International
Airport*）位於[馬斯覺](../Page/馬斯覺.md "wikilink")（*Mascot*）區，是悉尼的主要機場，也是全球悠歷悠久的仍在運作的商用機場。比較小的[賓士鎮機場](../Page/賓士鎮機場.md "wikilink")（*Bankstown
Airport*） 主要為私人和[通用航空](../Page/通用航空.md "wikilink")（*general
aviation*）提供服務。[何斯頓園](../Page/何斯頓園.md "wikilink")（Hoxton Park） 和
[康頓](../Page/卡姆登_\(新南威尔士州\).md "wikilink")（Camden）設有航空訓練機場。[列治文澳大利亞皇家空軍基地](../Page/列治文澳大利亞皇家空軍基地.md "wikilink")（*RAAF
Base Richmond*） 一直座落在市中心的西北面。

### 公用事業

雪梨市區的供水主要由[雪梨水務](../Page/雪梨水務.md "wikilink")（Sydney
Water）提供，但雪梨供水及其他社區的飲用水供應商其實都是向[雪梨引水局](../Page/雪梨引水局.md "wikilink")（Sydney
Catchment Authority）大量購買從河道及蓄水庫收集得來的飲用水。雪梨集水主要靠水壩儲水，在上納平（Upper
Nepean）水壩系列、藍山區六壩、Woronora水壩、Warragamba水壩和Shoalhaven水壩系列。歷史上集水區的最低水位引發了供水限制，[新南威爾斯州政府也因此展開供水替代方案的調查](../Page/新南威爾斯州.md "wikilink")，包含廢水回收與在Kurnell的海水淡化廠。
2000年時，雪梨曾因供應的飲用水含有過量微生物而使許多市民受感染送院。另外，雪梨水務亦負責城市的廢水及污水收集工作。

2006年2月8日，根據《[悉尼晨鋒報](../Page/悉尼晨鋒報.md "wikilink")》（Sydney Morning
Herald）報導，由於[社區迴響太大](../Page/社區.md "wikilink")，可能令[新南威爾士州政府](../Page/新南威爾士州.md "wikilink")[執政黨失去](../Page/澳洲工黨.md "wikilink")2007年州政府[選舉票源](../Page/選舉.md "wikilink")，[內閣決定放棄興建](../Page/內閣.md "wikilink")[海水淡化廠](../Page/海水淡化廠.md "wikilink")，並著手研究開發[地下水的可行性](../Page/地下水.md "wikilink")。當局在[南部高地](../Page/南部高地.md "wikilink")（Southern
Highlands）和西悉尼（靠近[霍克斯伯里河](../Page/霍克斯伯里河.md "wikilink")（Hawkesbury
River））發現新的[蓄水層](../Page/蓄水層.md "wikilink")。

悉尼的天然氣和電力是由三個公司來供給的，分別是[澳大利亞能源](../Page/澳大利亞能源.md "wikilink")（Energy
Australia）、[澳大利亞天然氣與光公司](../Page/澳大利亞天然氣與光公司.md "wikilink")（Australian
Gas Light Company, AGL）及[整體能源](../Page/整體能源.md "wikilink")（Integral
Energy）。市話與行動通訊服務則有許多公司提供。

## 参考文献

## 外部連結

  - [澳洲悉尼](https://news.china.com.au/?s=%E6%82%89%E5%B0%BC)

  - [悉尼市官方網站](http://www.cityofsydney.nsw.gov.au/)

  - [悉尼市媒體中心](http://www.sydneymedia.com.au/)

  - [海港城市悉尼](http://www.sydney.com.au/)

  - [悉尼大都會策略](https://web.archive.org/web/20080704190128/http://www.metrostrategy.nsw.gov.au/)
    - 新南威爾士州政府主導大悉尼都會區將來30年的發展、變革

  - [遷移到悉尼](http://www.newinoz.com/zh/?sydney)

  - [悉尼天氣預報](http://www.bom.gov.au/cgi-bin/wrap_fwo.pl?IDN10064.txt)

  -
  - \[//maps.google.com/maps?q=Sydney Google地图\]

## 參見

  -
  -
  -
{{-}}

[Category:新南威尔士州城市](../Category/新南威尔士州城市.md "wikilink")
[悉尼](../Category/悉尼.md "wikilink")
[S](../Category/太平洋沿海城市.md "wikilink")
[Category:夏季奥林匹克运动会主办城市](../Category/夏季奥林匹克运动会主办城市.md "wikilink")
[Category:1788年建立](../Category/1788年建立.md "wikilink")

1.

2.

3.  Beaverstock, J.V. et al
    [世界級城市名冊](http://www.lboro.ac.uk/gawc/rb/rb5.html)

4.  例如：[雪梨市政府网站繁体中文网页](http://www.cityofsydney.nsw.gov.au/community-languages/chinese/visiting-sydney)、[悉尼市政府网站简体中文网页](http://www.cityofsydney.nsw.gov.au/community-languages/chinese-simplified/visiting-sydney)

5.  [中华人民共和国驻悉尼总领事馆](http://sydney.chineseconsulate.org/chn/)

6.  [中華民國外交部駐雪梨辦事處
    (駐雪梨台北經濟文化辦事處)](http://www.mofa.gov.tw/moverseasOffice_Detail.aspx?s=0FCF4B85F20FA9F4)

7.  Kohen, J. L. 2000. First and last people: Aboriginal Sydney. In J.
    Connell （Ed.）. *Sydney the emergence of a global city.* pp 76-95.
    Oxford University Press ISBN 0-19-550748-7, pp 76-78

8.  Ibid, pp 81-82

9.  Ibid, pp 83

10. 澳洲氣象局 2006年
    [悉尼氣候摘要，2006年1月](http://www.bom.gov.au/announcements/media_releases/nsw/20060201.shtml)

11. 悉尼統計局 2005年
    [悉尼統計處](http://www.abs.gov.au/Ausstats/abs@.nsf/0/CDD66A1F353FD6A5CA256E5C00193185?Open)

12. 澳洲房地產學會 2005年12月14曰 [Still strong confidence in the housing
    market](http://www.reiaustralia.com.au/media/releases.asp) , Press
    Release

13. <http://www.citymayors.com/economics/richest_cities.html>

14. Boilling, M. February 2 2006.
    [最昂貴的城市中的城市](http://www.heraldsun.news.com.au/common/story_page/0,5478,18010915%5E2862,00.html),
    *Herald Sun*

15. Australian Bureau of Statistics. 2002. [Sydney - Basic Community
    Profile and Snapshot - 2001
    Census](http://www.abs.gov.au/ausstats/abs@cpp.nsf/Lookup/105Snapshot12001)


16. Daly, M. T. and Pritchard, B. 2000. 悉尼：澳洲的金融與商務首都 In J. Connell
    （Ed.）. 《悉尼，全球都市的浮現》 pp 76-95. 牛津大學出版社，ISBN
    0-19-550748-7，167-188頁

17. 新南威爾斯州旅遊 2004年
    \[<http://corporate.tourism.nsw.gov.au/corporatelive/downloads/research/datacard%202004%20forecasts%20（external%20use>）.pdf
    Tourism Data Card - Forecasts, Economic Impacts and selected
    Regional Data - 2004\]

18. about.com, 「老澳」及澳洲人俗語（Strine and Aussie Slang） [Sanger to
    Sydneysider](http://goaustralia.about.com/cs/language/a/strines.htm)

19. 澳洲統計局 2006年
    [國家地區概況：悉尼](http://www.abs.gov.au/AUSSTATS/abs@.nsf/DetailsPage/1052003?OpenDocument&tabname=Details&prodno=105&issue=2003&num=&view=&)

20. 澳洲統計局 2006年
    [國家地區概況：內悉尼](http://www.abs.gov.au/AUSSTATS/abs@.nsf/DetailsPage/105052003?OpenDocument&tabname=Details&prodno=10505&issue=2003&num=&view=&)

21.
22.
23. 悉尼市社區概況 - 悉尼統計局 2006年
    [我們的資格是甚麼？](http://www.id.com.au/profile/default.asp?id=148&gid=350&pg=8),
    profile.id

24.
25. 澳洲教育網絡大學及學院指南 2005年
    [澳洲大學排名](http://www.australian-universities.com/rankings/)

26.

27. 國家公園及野生動植物管理處網站
    [皇家國家公園](http://www.nationalparks.nsw.gov.au/parks.nsf/parkContent/N0030?OpenDocument&ParkKey=N0030&Type=Xo)