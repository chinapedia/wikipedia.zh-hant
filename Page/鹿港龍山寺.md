**鹿港龍山寺**，為[臺灣](../Page/臺灣.md "wikilink")[彰化縣](../Page/彰化縣.md "wikilink")[鹿港鎮的廟宇](../Page/鹿港鎮.md "wikilink")，主祀[觀世音菩薩](../Page/觀世音菩薩.md "wikilink")，本廟與[臺灣另外幾座以龍山寺命名的寺廟](../Page/臺灣.md "wikilink")──[淡水龍山寺](../Page/淡水龍山寺.md "wikilink")、[艋舺龍山寺](../Page/艋舺龍山寺.md "wikilink")、[臺南龍山寺](../Page/臺南龍山寺.md "wikilink")、[鳳山龍山寺等](../Page/鳳山龍山寺.md "wikilink")，都是[分靈自](../Page/分靈.md "wikilink")[福建省](../Page/福建省.md "wikilink")[泉州府](../Page/泉州府.md "wikilink")[晉江縣的](../Page/晉江縣.md "wikilink")[泉安龍山寺](../Page/泉安龍山寺.md "wikilink")，本廟也是臺灣所有[龍山寺中保存最為完整的建築](../Page/龍山寺.md "wikilink")，目前為[國定古蹟](../Page/臺灣古蹟列表.md "wikilink")。

## 歷史

1647年，[泉州三邑移民來台](../Page/泉州三邑.md "wikilink")，自[泉安龍山寺](../Page/泉安龍山寺.md "wikilink")[分香](../Page/分香.md "wikilink")[觀音大士佛像](../Page/觀音大士.md "wikilink")，於鹿港暗街仔（大有街）結庵奉祀。1786年\[1\]，純真璞禪師遷建於現址。1831年，大修並重建多次。

1897年（[明治](../Page/明治.md "wikilink")30年），[日軍的從軍佈教使](../Page/日軍.md "wikilink")[佐佐木一道租用鹿港龍山寺後殿](../Page/佐佐木一道.md "wikilink")，成立[淨土真宗本願寺派布教所](../Page/淨土真宗本願寺派.md "wikilink")，並創立敬愛學校，親自教授[日文](../Page/日文.md "wikilink")，另聘請[秀才](../Page/秀才.md "wikilink")[鄭鴻猷](../Page/鄭鴻猷.md "wikilink")、鄭玉田教導[漢文](../Page/漢文.md "wikilink")。1898年（明治31年）改由[光明智曉](../Page/光明智曉.md "wikilink")[和尚擔任校長](../Page/和尚.md "wikilink")，智曉將學校改為[公學校制度](../Page/公學校.md "wikilink")，並且向龍山寺[主持林文蓮與](../Page/主持.md "wikilink")[檀越代表提議將鹿港龍山寺改為](../Page/檀越.md "wikilink")[本願寺派的分寺](../Page/本願寺.md "wikilink")，1904年鹿港龍山寺加入本願寺派，正殿改奉[阿彌陀佛](../Page/阿彌陀佛.md "wikilink")，原[泉安分靈的觀音大士神像遷至後殿](../Page/安海鎮.md "wikilink")，但是仍以林文蓮為主持。1907年住持林文蓮逝世後，由智曉接管龍山寺，1921年（大正10年）後殿失火，觀音寶像毀損。

1945年，[日本二戰投降](../Page/日本二戰投降.md "wikilink")，[中國國民政府代表](../Page/中國國民政府.md "wikilink")[盟軍](../Page/盟軍.md "wikilink")[接收臺灣](../Page/國民政府接收臺灣.md "wikilink")，鹿港龍山寺改由檀越組織管理委員會主持。1961年，重塑觀音菩薩寶像安座正殿，原日本阿彌陀佛寶像移祀後殿。1983年12月，[內政部將鹿港龍山寺登錄為國家第一級古蹟](../Page/中華民國內政部.md "wikilink")（今[國定古蹟](../Page/國定古蹟.md "wikilink")）\[2\]。

## 建築

鹿港龍山寺是一個大型建築物，被認為是現在在臺灣保存最完整的[台灣清治時期建築物](../Page/台灣清治時期.md "wikilink")。整個廟宇最重要的是其建築和雕刻。

鹿港龍山寺佔地一千六百多[坪](../Page/坪.md "wikilink")，為三進二院七開間的建築格局，分為[山門](../Page/山門.md "wikilink")、[五門殿](../Page/五門殿.md "wikilink")（含[戲台](../Page/戲台.md "wikilink")）、[正殿](../Page/正殿.md "wikilink")（含[拜殿](../Page/拜殿.md "wikilink")）、後殿。鹿港龍山寺戲台上方[藻井結構](../Page/藻井.md "wikilink")，是台灣保存年代最早且最大的作品。八卦藻井設立於戲台上方，具有演戲時共鳴的效果。

## 圖輯

<File:Lukang> Lung-shan Temple 2004.jpg|鹿港龍山寺山門
[File:鹿港龍山寺五門殿.jpg|鹿港龍山寺五門殿](File:鹿港龍山寺五門殿.jpg%7C鹿港龍山寺五門殿)
[File:鹿港龍山寺.jpg|鹿港龍山寺](File:鹿港龍山寺.jpg%7C鹿港龍山寺)[八卦](../Page/八卦.md "wikilink")[藻井](../Page/藻井.md "wikilink")
[File:鹿港龍山寺前殿.jpg|鹿港龍山寺五門殿](File:鹿港龍山寺前殿.jpg%7C鹿港龍山寺五門殿)
[File:鹿港龍山寺五門殿八卦窗.jpg|鹿港龍山寺五門殿](File:鹿港龍山寺五門殿八卦窗.jpg%7C鹿港龍山寺五門殿)[八卦窗](../Page/八卦窗.md "wikilink")
[File:鹿港龍山寺拜殿01.jpg|鹿港龍山寺拜殿](File:鹿港龍山寺拜殿01.jpg%7C鹿港龍山寺拜殿)
[File:鹿港龍山寺正殿.jpg|鹿港龍山寺正殿](File:鹿港龍山寺正殿.jpg%7C鹿港龍山寺正殿)
<File:Lukang> Longshan Temple (Taiwan).jpg|鹿港龍山寺後殿 <File:Lukang>
Longshan Temple, Lukang
(Taiwan).jpg|鹿港龍山寺内[黃杰將軍題字的匾額](../Page/黃杰.md "wikilink")

## 祭祀

### 神祇

  - 正殿：[觀音菩薩](../Page/觀音菩薩.md "wikilink")、[十八羅漢](../Page/十八羅漢.md "wikilink")、[境主尊王](../Page/境主.md "wikilink")、[註生娘娘](../Page/註生娘娘.md "wikilink")、[齊天大聖](../Page/齊天大聖.md "wikilink")、[龍王尊神](../Page/龍王尊神.md "wikilink")
  - 後殿：[三寶佛](../Page/三世佛#橫三世佛.md "wikilink")（[釋迦牟尼佛](../Page/釋迦牟尼佛.md "wikilink")、[藥師佛](../Page/藥師佛.md "wikilink")、[阿彌陀佛](../Page/阿彌陀佛.md "wikilink")）、[文殊菩薩](../Page/文殊菩薩.md "wikilink")、[普賢菩薩](../Page/普賢菩薩.md "wikilink")、[地藏菩薩](../Page/地藏菩薩.md "wikilink")、[玄天上帝](../Page/玄天上帝.md "wikilink")、[風神](../Page/風伯.md "wikilink")、[雨神](../Page/雨神.md "wikilink")、[文昌帝君](../Page/文昌帝君.md "wikilink")、[太歲星君](../Page/太歲星君.md "wikilink")

### 祭典活動

  - [元宵節躦燈腳](../Page/台灣元宵節.md "wikilink")：[農曆正月十五日](../Page/農曆.md "wikilink")
  - [釋迦牟尼佛出家日](../Page/釋迦牟尼佛.md "wikilink")：農曆二月初八
  - 釋迦牟尼佛[涅槃日](../Page/涅槃.md "wikilink")：農曆二月十五日
  - [觀音菩薩誕辰日](../Page/觀音菩薩.md "wikilink")：農曆二月十九日
  - [普賢菩薩誕辰日](../Page/普賢菩薩.md "wikilink")：農曆二月二十一日
  - [龍王尊神誕辰日](../Page/龍王尊神.md "wikilink")：農曆二月二十四日
  - [文殊菩薩誕辰日](../Page/文殊菩薩.md "wikilink")：農曆四 月初四
  - [佛誕節](../Page/佛誕節.md "wikilink")（浴佛節）：農曆四月八日
  - 觀音菩薩成道日：農曆六月十九日
  - [中元普渡](../Page/台灣中元法會.md "wikilink")[盂蘭盆節](../Page/中元節與盂蘭盆節_\(華人\).md "wikilink")：農曆七月十二日
  - [地藏菩薩誕辰日](../Page/地藏菩薩.md "wikilink")：農曆七月三十日
  - 觀音菩薩出家紀念日：農曆九月十九日
  - [藥師佛誕辰日](../Page/藥師佛.md "wikilink")：農曆九月三十日
  - [阿彌陀佛誕辰日](../Page/阿彌陀佛.md "wikilink")：農曆十一月十七日
  - [釋迦牟尼佛成道日](../Page/釋迦牟尼佛.md "wikilink")：農曆十二月八日

## 參見

  - [龍山寺](../Page/龍山寺.md "wikilink")
  - [台灣佛教](../Page/台灣佛教.md "wikilink")
  - [台灣禪宗](../Page/台灣禪宗.md "wikilink")
  - [臨濟正宗](../Page/臨濟正宗.md "wikilink")
  - [拱範宮](../Page/拱範宮.md "wikilink")
  - [艋舺龍山寺](../Page/艋舺龍山寺.md "wikilink")
  - [澎湖觀音亭](../Page/澎湖觀音亭.md "wikilink")

## 註釋

<references />

## 外部連結

  - [鹿港龍山寺官網](http://www.lungshan-temple.org.tw/)

[Category:台灣觀音寺](../Category/台灣觀音寺.md "wikilink")
[Category:彰化縣佛寺](../Category/彰化縣佛寺.md "wikilink")
[Category:鹿港閤港廟](../Category/鹿港閤港廟.md "wikilink")
[Category:台灣清治時期建築](../Category/台灣清治時期建築.md "wikilink")
[Category:1650年代建立](../Category/1650年代建立.md "wikilink")
[Category:1786年完成的建築物](../Category/1786年完成的建築物.md "wikilink")

1.
2.  [鹿港龍山寺](https://nchdb.boch.gov.tw/assets/advanceSearch/monument/19831228000005)
    ，[文化部文化資產局](../Page/文化部文化資產局.md "wikilink")