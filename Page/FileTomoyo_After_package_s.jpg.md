## 摘要

| 描述摘要 | 《[智代After ～It's a Wonderful Life～](../Page/智代After_～It's_a_Wonderful_Life～.md "wikilink")》PC遊戲封面 |
| ---- | ------------------------------------------------------------------------------------------------ |
| 來源   | <http://www.getchu.com/soft.phtml?id=170541>                                                     |
| 日期   | 2005年11月25日                                                                                      |
| 作者   | [Key](../Page/Key_\(公司\).md "wikilink")                                                          |
| 許可   | Gamecover {{\#switch: }}}                                                                        |

## 许可协议

[Category:日本成人游戏封面](../Category/日本成人游戏封面.md "wikilink") [Category:Key
(遊戲品牌)](../Category/Key_\(遊戲品牌\).md "wikilink")