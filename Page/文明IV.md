《**文明IV**》（全称“**席德·梅尔之文明IV**”，）是一个[回合制策略](../Page/回合制策略游戏.md "wikilink")[电脑游戏](../Page/电脑游戏.md "wikilink")，它是[游戏设计师](../Page/游戏设计师.md "wikilink")[席德·梅尔的](../Page/席德·梅尔.md "wikilink")[文明系列作品](../Page/文明系列.md "wikilink")。由主设计师[索伦·约翰逊在梅尔的指导下](../Page/w:Soren_Johnson.md "wikilink")，由梅尔的[工作室](../Page/游戏开发商.md "wikilink")[Firaxis
Games开发](../Page/Firaxis_Games.md "wikilink")。文明IV于2005年10月25日和11月4日之间，在[北美](../Page/北美.md "wikilink")、[欧洲和](../Page/欧洲.md "wikilink")[澳大利亚发行](../Page/澳大利亚.md "wikilink")。[麦金塔版于](../Page/麦金塔.md "wikilink")2006年初发行。游戏的第一个[资料片](../Page/资料片.md "wikilink")——《战神》（*Warlord*）和第二个资料片——《超越刀锋》（*Beyond
the
Sword*）分别于2006年7月和2007年7月在美国和欧洲上市。采用相同引擎而玩法差异很大的“独立资料片”《殖民统治》（*Colonization*）于2008年9月上市。到2007年初，文明IV已经售出了超过150万套。\[1\]

与其他文明系列的游戏相似，文明IV游戏是由玩家从一个小城市开始来建立一个帝国的过程。一个标准的游戏过程通常是玩家选定一个文明（每个文明对应一个帝国）后，游戏从[公元前4000年开始](../Page/前4千年.md "wikilink")，玩家拥有一个“定居者”（settler），可用于建立一个城市；然后玩家通过内政、外交和战争来为自己的文明（即帝国）获取资源和土地，与多个由电脑（单人游戏中）或其他玩家（多人游戏中）控制的文明竞争，以争取最终的胜利。

《文明IV》目前有[英语](../Page/英语.md "wikilink")、[德语](../Page/德语.md "wikilink")、[法语](../Page/法语.md "wikilink")、[西班牙语](../Page/西班牙语.md "wikilink")、[意大利语](../Page/意大利语.md "wikilink")、[日语](../Page/日语.md "wikilink")、[简体中文](../Page/简体中文.md "wikilink")、[繁体中文和](../Page/繁体中文.md "wikilink")[波兰语等多个语言版本](../Page/波兰语.md "wikilink")。而其他一些语言也有非官方翻译版，如[俄语](../Page/俄语.md "wikilink")、[芬兰语](../Page/芬兰语.md "wikilink")、[捷克语和](../Page/捷克语.md "wikilink")[匈牙利语](../Page/匈牙利语.md "wikilink")
。\[2\]\[3\]

## 游戏设定

### 文明和领袖

每个文明都有自己的领袖，玩家在选择文明的同时，也要选择该文明对应的其中一个领袖（如果该文明有两个领袖）。由于不同的领袖具有不同的性格和偏好，因此在游戏过程中要注意根据其性格和偏好来设计文明的发展路线。

| 文明                                           | 全称                                          | 起始科技                                                               | 特色单位                                                                      | 特色建筑                                                                    | 领袖                                       | 领袖特性  | 内政偏好                                | 首都                                     |
| -------------------------------------------- | ------------------------------------------- | ------------------------------------------------------------------ | ------------------------------------------------------------------------- | ----------------------------------------------------------------------- | ---------------------------------------- | ----- | ----------------------------------- | -------------------------------------- |
| [美国](../Page/美国.md "wikilink")               | [美利坚帝国](../Page/美利坚帝国.md "wikilink")        | [渔业](../Page/渔业.md "wikilink")、[农业](../Page/农业.md "wikilink")      | [海豹部隊](../Page/海豹部隊.md "wikilink")，替代[海军陆战队](../Page/海军陆战队.md "wikilink") | [购物中心](../Page/购物中心.md "wikilink")，替代[超级市场](../Page/超级市场.md "wikilink") | [喬治·華盛頓](../Page/喬治·華盛頓.md "wikilink")   | 理财、组织 | [普选制度](../Page/普选权.md "wikilink")   | [华盛顿](../Page/华盛顿特区.md "wikilink")     |
| [富蘭克林·羅斯福](../Page/富蘭克林·羅斯福.md "wikilink")   | 勤奋、组织                                       |                                                                    |                                                                           |                                                                         |                                          |       |                                     |                                        |
| [阿拉伯](../Page/阿拉伯.md "wikilink")             | [阿拉伯帝国](../Page/阿拉伯帝国.md "wikilink")        | [神秘主义](../Page/神秘主义.md "wikilink")、[车轮技术](../Page/轮.md "wikilink") | [駱駝弓箭手](../Page/駱駝弓箭手.md "wikilink")，替代[骑士](../Page/骑士.md "wikilink")     | [伊斯兰学校](../Page/伊斯兰学校.md "wikilink")，替代[图书馆](../Page/图书馆.md "wikilink") | [薩拉丁](../Page/薩拉丁.md "wikilink")         | 思辨、虔信 | [神权政治](../Page/神权政治.md "wikilink")  | [麦加](../Page/麦加.md "wikilink")         |
| [阿茲特克](../Page/阿茲特克.md "wikilink")           | [阿茲特克帝国](../Page/阿茲特克帝国.md "wikilink")      | 神秘主义、[狩獵技术](../Page/狩獵.md "wikilink")                              | [美洲虎战士](../Page/美洲虎战士.md "wikilink")，替代[剑士](../Page/剑术.md "wikilink")     | [人牲祭坛](../Page/人牲祭坛.md "wikilink")，替代[法庭](../Page/法庭.md "wikilink")     | [蒙特祖馬](../Page/蒙特祖馬二世.md "wikilink")     | 好斗、虔信 | [警察制度](../Page/警察制度.md "wikilink")  | [特諾奇提特蘭](../Page/特諾奇提特蘭.md "wikilink") |
| [中国](../Page/中国.md "wikilink")               | [中华帝国](../Page/中国.md "wikilink")            | [农业](../Page/农业.md "wikilink")、[采矿业](../Page/采矿业.md "wikilink")    | [諸葛弩](../Page/諸葛弩.md "wikilink")，替代[弩兵](../Page/弩.md "wikilink")          | [舞榭](../Page/舞榭.md "wikilink")，替代[剧场](../Page/剧场.md "wikilink")         | [毛泽东](../Page/毛泽东.md "wikilink")         | 思辨、组织 | [国有经济](../Page/公有制.md "wikilink")   | [北京](../Page/北京.md "wikilink")         |
| [秦始皇](../Page/秦始皇.md "wikilink")             | 勤奋、理财                                       | 警察制度                                                               |                                                                           |                                                                         |                                          |       |                                     |                                        |
| [埃及](../Page/埃及.md "wikilink")               | [埃及帝国](../Page/埃及帝国.md "wikilink")          | [农业](../Page/农业.md "wikilink")、[车轮技术](../Page/轮.md "wikilink")     | [强战车](../Page/战车.md "wikilink")，替代战车                                      | [方尖塔](../Page/方尖塔.md "wikilink")，替代[纪念碑](../Page/纪念碑.md "wikilink")     | [哈特谢普苏特](../Page/哈特谢普苏特.md "wikilink")   | 虔信、创新 | [世袭制度](../Page/君主制.md "wikilink")   | [底比斯](../Page/底比斯.md "wikilink")       |
| [英格兰](../Page/英格兰.md "wikilink")             | [大英帝国](../Page/大英帝国.md "wikilink")          | 渔业、采矿业                                                             | 英国[來复槍兵](../Page/來复槍.md "wikilink")，替代來复槍兵                                | [证券交易所](../Page/证券交易所.md "wikilink")，替代[银行](../Page/银行.md "wikilink")   | [维多利亚](../Page/维多利亚女王.md "wikilink")     | 霸权、理财 | [代议制度](../Page/代議民主制.md "wikilink") | [伦敦](../Page/伦敦.md "wikilink")         |
| [伊丽莎白](../Page/伊丽莎白一世_\(英格兰\).md "wikilink") | 思辨、理财                                       | [信仰自由](../Page/信仰自由.md "wikilink")                                 |                                                                           |                                                                         |                                          |       |                                     |                                        |
| [法国](../Page/法国.md "wikilink")               | [法兰西帝国](../Page/法兰西帝国.md "wikilink")        | 农业、车轮技术                                                            | 法国[火枪手](../Page/火枪手.md "wikilink")，替代[火枪兵](../Page/火枪.md "wikilink")      | [沙龙](../Page/沙龙.md "wikilink")，替代[天文台](../Page/天文台.md "wikilink")       | [路易十四](../Page/路易十四.md "wikilink")       | 創新、勤奋 | 世袭制度                                | [巴黎](../Page/巴黎.md "wikilink")         |
| [拿破仑](../Page/拿破仑·波拿巴.md "wikilink")         | 组织、魅力                                       | 代议制度                                                               |                                                                           |                                                                         |                                          |       |                                     |                                        |
| [德国](../Page/德国.md "wikilink")               | [德意志帝国](../Page/德意志帝国.md "wikilink")（普鲁士帝国） | 狩猎技术、采矿业                                                           | [豹式坦克](../Page/豹式坦克.md "wikilink")，替代[坦克](../Page/坦克.md "wikilink")       | 装配[工厂](../Page/工厂.md "wikilink")，替代工厂                                   | [腓特烈](../Page/腓特烈大帝.md "wikilink")       | 創新、思辨 | 普选制度                                | [柏林](../Page/柏林.md "wikilink")         |
| [俾斯麥](../Page/俾斯麥.md "wikilink")             | 开拓、勤奋                                       | 代议制度                                                               |                                                                           |                                                                         |                                          |       |                                     |                                        |
| [希腊](../Page/希腊.md "wikilink")               | [希腊帝国](../Page/希腊帝国.md "wikilink")（马其顿王国）   | 渔业、狩猎技术                                                            | [重装步兵](../Page/重装步兵.md "wikilink")，替代[矛兵](../Page/矛兵.md "wikilink")       | [露天剧场](../Page/露天剧场.md "wikilink")，替代[竞技场](../Page/竞技场.md "wikilink")   | [亚历山大](../Page/亚历山大大帝.md "wikilink")     | 好斗、思辨 | 世袭制度                                | [雅典](../Page/雅典.md "wikilink")         |
| [印加](../Page/印加帝国.md "wikilink")             | [印加帝国](../Page/印加帝国.md "wikilink")          | 农业、神秘主义                                                            | [克丘亞战士](../Page/克丘亞战士.md "wikilink")，替代战士                                 | [梯田](../Page/梯田.md "wikilink")，替代[粮仓](../Page/粮仓.md "wikilink")         | [瓦伊纳·卡帕克](../Page/瓦伊纳·卡帕克.md "wikilink") | 好斗、理财 | 世袭制度                                | [庫斯科](../Page/庫斯科.md "wikilink")       |
| [印度](../Page/印度.md "wikilink")               | [印度帝国](../Page/莫卧儿帝国.md "wikilink")（莫卧儿帝国）  | 神秘主义、采矿业                                                           | [快速工人](../Page/人力劳动.md "wikilink")，替代工人                                   | [陵墓](../Page/陵墓.md "wikilink")，替代[监狱](../Page/监狱.md "wikilink")         | [甘地](../Page/圣雄甘地.md "wikilink")         | 勤奋、虔信 | 普选制度                                | [德里](../Page/德里.md "wikilink")         |
| [阿育王](../Page/阿育王.md "wikilink")             | 组织、虔信                                       |                                                                    |                                                                           |                                                                         |                                          |       |                                     |                                        |
| [日本](../Page/日本.md "wikilink")               | [日本帝国](../Page/日本帝国.md "wikilink")          | 渔业、车轮技术                                                            | [武士](../Page/武士.md "wikilink")，替代[锤矛手](../Page/锤矛.md "wikilink")          | [油页岩电站](../Page/油页岩电站.md "wikilink")，替代[火电站](../Page/火电站.md "wikilink") | [德川家康](../Page/德川家康.md "wikilink")       | 好斗、组织 | [重商主义](../Page/重商主义.md "wikilink")  | [京都](../Page/京都.md "wikilink")         |
| [马里](../Page/马里.md "wikilink")               | [马里帝国](../Page/马里帝国.md "wikilink")          | 采矿业、车轮技术                                                           | [Skirmisher](../Page/散兵.md "wikilink")，替代[弓箭兵](../Page/箭术.md "wikilink")  | [铸币厂](../Page/铸币厂.md "wikilink")，替代[煅造场](../Page/煅造场.md "wikilink")     | [曼萨·穆萨](../Page/曼萨·穆萨.md "wikilink")     | 理财、虔信 | [自由市场](../Page/自由市场.md "wikilink")  | [廷巴克圖](../Page/廷巴克圖.md "wikilink")     |
| [蒙古](../Page/蒙古.md "wikilink")               | [蒙古帝国](../Page/蒙古帝国.md "wikilink")          | 狩猎技术、车轮技术                                                          | [怯薜](../Page/怯薜.md "wikilink")，替代[弓騎兵](../Page/弓騎兵.md "wikilink")         | [毡帐](../Page/毡帐.md "wikilink")，替代[马厩](../Page/马厩.md "wikilink")         | [成吉思汗](../Page/成吉思汗.md "wikilink")       | 好斗、开拓 | 警察制度                                | [哈拉和林](../Page/哈拉和林.md "wikilink")     |
| [忽必烈](../Page/忽必烈.md "wikilink")             | 好斗、創新                                       | 世袭制度                                                               |                                                                           |                                                                         |                                          |       |                                     |                                        |
| [波斯](../Page/波斯.md "wikilink")               | [波斯帝国](../Page/波斯帝国.md "wikilink")          | 农业、渔业                                                              | [长生军](../Page/长生军.md "wikilink")，替代战车                                     | [药剂店](../Page/药剂店.md "wikilink")，替代[杂货店](../Page/杂货店.md "wikilink")     | [居魯士](../Page/居鲁士大帝.md "wikilink")       | 开拓、創新 | 代议制度                                | [波斯波利斯](../Page/波斯波利斯.md "wikilink")   |
| [罗马](../Page/罗马.md "wikilink")               | [罗马帝国](../Page/罗马帝国.md "wikilink")          | 渔业、采矿业                                                             | [罗马禁卫军](../Page/罗马禁卫军.md "wikilink")，替代剑士                                 | [中心广场](../Page/中心广场.md "wikilink")，替代[市场](../Page/市场.md "wikilink")     | [恺撒](../Page/恺撒.md "wikilink")           | 开拓、组织 | 代议制度                                | [罗马](../Page/罗马.md "wikilink")         |
| [俄国](../Page/俄国.md "wikilink")               | [俄罗斯帝国](../Page/俄罗斯帝国.md "wikilink")（沙俄）    | 狩猎技术、采矿业                                                           | [哥萨克](../Page/哥萨克.md "wikilink")，替代[骑兵](../Page/骑兵.md "wikilink")         | [研究所](../Page/研究所.md "wikilink")，替代[实验室](../Page/实验室.md "wikilink")     | [叶卡捷琳娜](../Page/叶卡捷琳娜大帝.md "wikilink")   | 創新、理财 | 世袭制度                                | [莫斯科](../Page/莫斯科.md "wikilink")       |
| [彼得](../Page/彼得大帝.md "wikilink")             | 开拓、思辨                                       | 警察制度                                                               |                                                                           |                                                                         |                                          |       |                                     |                                        |
| [西班牙](../Page/西班牙.md "wikilink")             | [西班牙帝国](../Page/西班牙帝国.md "wikilink")        | 渔业、神秘主义                                                            | [西班牙征服者](../Page/西班牙征服者.md "wikilink")，替代[骑士](../Page/骑士.md "wikilink")   | [城防要塞](../Page/城防要塞.md "wikilink")，替代[城堡](../Page/城堡.md "wikilink")     | [伊莎貝拉](../Page/伊莎貝拉一世.md "wikilink")     | 开拓、虔信 | 警察制度                                | [马德里](../Page/马德里.md "wikilink")       |

（在正式发行的简体中文版中，对中国和蒙古的势力名称和领袖名称全部作了修改，毛泽东的人物模型也被替换为[李世民](../Page/李世民.md "wikilink")。）

#### 领袖特性

每位领袖都有两种性格，为对应文明提供加成。所有的特性如下：

  - 好斗：自动晋升格斗与火器部队至“战斗I”。[兵营与](../Page/兵营.md "wikilink")[船坞的建造速度加倍](../Page/船坞.md "wikilink")。
  - 创新：每个城市每回合+2点文化。[剧院与](../Page/剧院.md "wikilink")[竞技场的建造速度加倍](../Page/竞技场.md "wikilink")。
  - 开拓：每个城市+2健康值。[谷仓与](../Page/谷仓.md "wikilink")[海港的建造速度加倍](../Page/海港.md "wikilink")。
  - 理财：城市范围内有2点商业点的地方会产生1点商业点或更多。[银行的建造速度加倍](../Page/银行.md "wikilink")。
  - 勤奋：奇迹建造速度增加50%。[铸造厂的建造速度加倍](../Page/铸造厂.md "wikilink")。
  - 组织：政体维护费用减少50%。[灯塔与](../Page/灯塔.md "wikilink")[法院的建造速度加倍](../Page/法院.md "wikilink")。
  - 思辩：伟人出生率增加100%。[大学的建造速度加倍](../Page/大学.md "wikilink")。
  - 虔信：政府类型转换时不会出现无政府主义。[神庙的建造速度加倍](../Page/寺庙.md "wikilink")。

在《战神》等后续资料片中又增加了防御、霸权、魅力3种性格。

  - 保国:
    自动晋升投射与火器部队至“操练I”和“城防I”。[城墙与](../Page/城墙.md "wikilink")[城堡的建造速度加倍](../Page/城堡.md "wikilink")。
  - 霸权: 大军事家出现几率增加100%。移民的训练速度增加50%。
  - 魅力:
    每座城市+1点快乐。单位晋升所需经验点数减少25%。[纪念碑和](../Page/纪念碑.md "wikilink")[广播塔](../Page/广播.md "wikilink")+1点快乐。

### 科技

《文明IV》中总共出现了85种不同的科技。

  - 遠古時代
      - [採礦](../Page/採礦.md "wikilink")
      - [神秘主義](../Page/神秘主義.md "wikilink")
      - [漁獲](../Page/漁業.md "wikilink")
      - [車輪](../Page/車輪.md "wikilink")
      - [農耕](../Page/農業.md "wikilink")
      - [狩獵](../Page/狩獵.md "wikilink")
      - [青铜冶炼](../Page/青铜.md "wikilink")
      - [石工术](../Page/石工术.md "wikilink")
      - [多神論](../Page/多神教.md "wikilink")
      - [冥想](../Page/冥想.md "wikilink")
      - [航海](../Page/航海.md "wikilink")
      - [製陶術](../Page/陶.md "wikilink")
      - [畜牧業](../Page/畜牧.md "wikilink")
      - [箭術](../Page/箭.md "wikilink")
      - [一神論](../Page/一神論.md "wikilink")
      - [祭司制度](../Page/祭司.md "wikilink")
      - [書寫](../Page/書寫.md "wikilink")
      - [騎術](../Page/馬術.md "wikilink")
  - 古典時代
      - [锻铁术](../Page/锻铁术.md "wikilink")
      - [冶金](../Page/冶金.md "wikilink")
      - [君主制](../Page/君主制.md "wikilink")
      - [文学](../Page/文学.md "wikilink")
      - [字母](../Page/字母.md "wikilink")
      - [戏剧](../Page/戏剧.md "wikilink")
      - [数学](../Page/数学.md "wikilink")
      - [罗盘](../Page/罗盘.md "wikilink")
      - [建筑学](../Page/建筑学.md "wikilink")
      - [法典](../Page/法典.md "wikilink")
      - [货币](../Page/货币.md "wikilink")
      - [历法](../Page/历法.md "wikilink")

<!-- end list -->

  - 中世纪
      - [机械](../Page/机械.md "wikilink")
      - [神学](../Page/神学.md "wikilink")
      - [文官制度](../Page/文官制度.md "wikilink")
      - [封建主义](../Page/封建主义.md "wikilink")
      - [哲学](../Page/哲学.md "wikilink")
      - [音乐](../Page/音乐.md "wikilink")
      - [光学](../Page/光学.md "wikilink")
      - [工程学](../Page/工程学.md "wikilink")
      - [君权神授](../Page/君权神授.md "wikilink")
      - [造纸](../Page/造纸.md "wikilink")
      - [行会](../Page/行会.md "wikilink")
      - [银行业](../Page/银行.md "wikilink")
  - 启蒙时代
      - [印刷术](../Page/印刷术.md "wikilink")
      - [火药](../Page/火药.md "wikilink")
      - [教育](../Page/教育.md "wikilink")
      - [自由主义](../Page/自由主义.md "wikilink")
      - [民族主义](../Page/民族主义.md "wikilink")
      - [军事传统](../Page/军事传统.md "wikilink")
      - [天文学](../Page/天文学.md "wikilink")
      - [民主制度](../Page/民主制度.md "wikilink")
      - [宪法](../Page/宪法.md "wikilink")
      - [经济学](../Page/经济学.md "wikilink")
      - [化学](../Page/化学.md "wikilink")
      - [来复线](../Page/来复线.md "wikilink")
      - [可替换部件](../Page/可替换部件.md "wikilink")
      - [公司制](../Page/公司.md "wikilink")

<!-- end list -->

  - 工业时代
      - [炼钢](../Page/钢.md "wikilink")
      - [科学方法](../Page/科学方法.md "wikilink")
      - [共产主义](../Page/共产主义.md "wikilink")
      - [蒸汽动力](../Page/蒸汽动力.md "wikilink")
      - [流水线](../Page/流水线.md "wikilink")
      - [法西斯主义](../Page/法西斯主义.md "wikilink")
      - [医学](../Page/医学.md "wikilink")
      - [生物学](../Page/生物学.md "wikilink")
      - [物理学](../Page/物理学.md "wikilink")
      - [炮兵](../Page/炮兵.md "wikilink")
      - [铁路](../Page/铁路.md "wikilink")
      - [内燃机](../Page/内燃机.md "wikilink")
      - [核裂变](../Page/核裂变.md "wikilink")
      - [电学](../Page/电学.md "wikilink")
      - [产业主义](../Page/重产业主义.md "wikilink")
  - 现代
      - [制冷](../Page/制冷.md "wikilink")
      - [大众传媒](../Page/大众传媒.md "wikilink")
      - [无线电](../Page/无线电.md "wikilink")
      - [火箭学](../Page/火箭.md "wikilink")
      - [飞行](../Page/飞行.md "wikilink")
      - [塑料](../Page/塑料.md "wikilink")
      - [生态学](../Page/生态学.md "wikilink")
      - [遗传学](../Page/遗传学.md "wikilink")
      - [计算机](../Page/计算机.md "wikilink")
      - [卫星](../Page/卫星.md "wikilink")
      - [复合材料](../Page/复合材料.md "wikilink")
      - [核聚变](../Page/核聚变.md "wikilink")
      - [光纤](../Page/光纤.md "wikilink")
      - [机器人](../Page/机器人.md "wikilink")
  - 未来科技

### 内政

每个文明都要设定自己的内政政策，这些政策由以下五个方面组成，每一个方面都需要选择一种制度。在任何一个方面更改内政政策都会导致“革命”，使该文明的所有城市进入“无政府主义”状态（表现为生产停顿、无科技发展、人口增长率为零等），此状态维持一至多回合。

<table>
<thead>
<tr class="header">
<th><p>内政政策</p></th>
<th><p>政策选项</p></th>
<th><p>维护费用</p></th>
<th><p>效果</p></th>
<th><p>所需科技</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>政府运作</strong></p></td>
<td><p><a href="../Page/专制制度.md" title="wikilink">专制制度</a></p></td>
<td><p>低</p></td>
<td><p>无</p></td>
<td><p>无</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/世袭制度.md" title="wikilink">世袭制度</a></p></td>
<td><p>中</p></td>
<td><p>城市中每一个军事单位都可以为城市＋1点快乐度</p></td>
<td><p>“<a href="../Page/君主制.md" title="wikilink">君主制</a>”</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/代议制度.md" title="wikilink">代议制度</a></p></td>
<td><p>低</p></td>
<td><p>城市中每一个专业人员的＋3科技点，前5座最大城市的快乐度＋3</p></td>
<td><p>“<a href="../Page/宪法.md" title="wikilink">宪法</a>”</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/警察國家.md" title="wikilink">警察國家</a></p></td>
<td><p>高</p></td>
<td><p>战斗单位的生产速度＋25％，战争所引起的不快乐度－50％</p></td>
<td><p>“<a href="../Page/法西斯主义.md" title="wikilink">法西斯主义</a>”</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/普选权.md" title="wikilink">普选制度</a></p></td>
<td><p>中</p></td>
<td><p>城镇（Town）＋1生产，在城市中可以用钱来加速建造</p></td>
<td><p>“<a href="../Page/民主制.md" title="wikilink">民主制</a>”</p></td>
<td></td>
</tr>
<tr class="even">
<td><hr /></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>法律制度</strong></p></td>
<td><p><a href="../Page/野蛮制度.md" title="wikilink">野蛮制度</a></p></td>
<td><p>低</p></td>
<td><p>无</p></td>
<td><p>无</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/分封制度.md" title="wikilink">分封制度</a></p></td>
<td><p>高</p></td>
<td><p>新生产的军事单位＋2点经验，并增加一定数量的免费军事单位</p></td>
<td><p>“<a href="../Page/封建制度.md" title="wikilink">封建制度</a>”</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/官僚制度.md" title="wikilink">官僚制度</a></p></td>
<td><p>中</p></td>
<td><p>首都（即<a href="../Page/皇宫.md" title="wikilink">皇宫所在城市</a>）的生产力＋50％，商业＋50％</p></td>
<td><p>“<a href="../Page/官僚制度.md" title="wikilink">官僚制度</a>”</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/獨立国家.md" title="wikilink">獨立国家</a></p></td>
<td><p>无</p></td>
<td><p>每回合可强制征召3个基本步兵（会减少快乐度），兵营为所在城市提供2点快乐度</p></td>
<td><p>“<a href="../Page/民族主义.md" title="wikilink">民族主义</a>”</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/言论自由.md" title="wikilink">言论自由</a></p></td>
<td><p>低</p></td>
<td><p>所有城市文化度＋100%，城镇＋2商业点</p></td>
<td><p>“<a href="../Page/自由主义.md" title="wikilink">自由主义</a>”</p></td>
<td></td>
</tr>
<tr class="even">
<td><hr /></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>劳工制度</strong></p></td>
<td><p><a href="../Page/部落制度.md" title="wikilink">部落制度</a></p></td>
<td><p>低</p></td>
<td><p>无</p></td>
<td><p>无</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/奴隶制度.md" title="wikilink">奴隶制度</a></p></td>
<td><p>低</p></td>
<td><p>可以在城市中牺牲人口来加速建造</p></td>
<td><p>“鑄銅術”</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/农奴制度.md" title="wikilink">农奴制度</a></p></td>
<td><p>低</p></td>
<td><p>工人的工作速度加快50%</p></td>
<td><p>“<a href="../Page/封建制度.md" title="wikilink">封建制度</a>”</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/等级制度.md" title="wikilink">等级制度</a></p></td>
<td><p>中</p></td>
<td><p>艺术家、科学家和商人的数量不受限制</p></td>
<td><p>“<a href="../Page/法典.md" title="wikilink">法典</a>”</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/人身自由.md" title="wikilink">人身自由</a></p></td>
<td><p>无</p></td>
<td><p>农舍、小村、村庄的成长速度＋100%，增加其它没有采取该制度的文明的市民不快乐度</p></td>
<td><p>“民主制”</p></td>
<td></td>
</tr>
<tr class="even">
<td><hr /></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>经济政策</strong></p></td>
<td><p><a href="../Page/地方分權.md" title="wikilink">地方分權</a></p></td>
<td><p>低</p></td>
<td><p>无</p></td>
<td><p>无</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/重商主义.md" title="wikilink">重商主义</a></p></td>
<td><p>高</p></td>
<td><p>每个城市＋1专业，无国际贸易路线</p></td>
<td><p>“<a href="../Page/:銀行.md" title="wikilink">银行业</a>”</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/自由市场.md" title="wikilink">自由市场</a></p></td>
<td><p>中</p></td>
<td><p>每个城市贸易路线＋1</p></td>
<td><p>“<a href="../Page/经济学.md" title="wikilink">经济学</a>”</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/公有制.md" title="wikilink">国有经济</a></p></td>
<td><p>低</p></td>
<td><p>取消与首都的距离所产生的维护费用，风车<a href="../Page/磨坊.md" title="wikilink">磨坊和水磨坊的粮食产量</a>＋1。</p></td>
<td><p>“<a href="../Page/共产主义.md" title="wikilink">共产主义</a>”</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/环境保护主义.md" title="wikilink">环保主义</a></p></td>
<td><p>中</p></td>
<td><p>该文明的所有城市健康度＋6，城市范围内如果存在的<a href="../Page/森林.md" title="wikilink">森林和</a><a href="../Page/丛林.md" title="wikilink">丛林可以提高快乐度</a>。</p></td>
<td><p>“<a href="../Page/医学.md" title="wikilink">医学</a>”</p></td>
<td></td>
</tr>
<tr class="even">
<td><hr /></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>宗教信仰</strong></p></td>
<td><p><a href="../Page/原始信仰.md" title="wikilink">原始信仰</a></p></td>
<td><p>低</p></td>
<td><p>无</p></td>
<td><p>无　</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/组织信仰.md" title="wikilink">组织信仰</a></p></td>
<td><p>高</p></td>
<td><p>不需要宗教学院就可以生产传教士，信仰“<a href="../Page/国教.md" title="wikilink">国教</a>”的城市的建造速度＋25％</p></td>
<td><p>“<a href="../Page/一神论.md" title="wikilink">一神论</a>”</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/神权政治.md" title="wikilink">神权政体</a></p></td>
<td><p>中</p></td>
<td><p>信奉国教的城市新建的军事单位＋2经验点，禁止国教外的其他宗教在该文明的城市中传播</p></td>
<td><p>“<a href="../Page/神学.md" title="wikilink">神学</a>”</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/和平主义.md" title="wikilink">和平主义</a></p></td>
<td><p>无</p></td>
<td><p>信奉国教的城市中“伟人”诞生率＋100％，每一个军事单位的维护费用+1</p></td>
<td><p>“<a href="../Page/哲学.md" title="wikilink">哲学</a>”　</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/信仰自由.md" title="wikilink">信仰自由</a></p></td>
<td><p>低</p></td>
<td><p>无国教，城市中每种宗教信仰都可以使快乐度+1，该文明所属的所有城市研发+10％。</p></td>
<td><p>“<a href="../Page/自由主义.md" title="wikilink">自由主义</a>”</p></td>
<td></td>
</tr>
</tbody>
</table>

每一个文明都可以选择一种宗教作为“[国教](../Page/国教.md "wikilink")”。

### 外交

《文明IV》中的外交主要是进行物品（包括“资源”、“金钱”、“地图”、甚至“科技”、“城市”和“内政政策的转变”）交换。而某些物品交换需要获得相应科技才可以进行，如金钱交换需要“[貨幣](../Page/貨幣.md "wikilink")”，而地图交换需要“[造纸术](../Page/造纸术.md "wikilink")”。

外交中的操作还包括宣战、签订外交协定（包括“开放边境”协定、“共同防御”协定、“停火”协定和“和平”协定等）、向其他文明赠送物品或要求物品。

在外交的操作面板中，不仅能够查看玩家所属文明与其他各个文明之间的关系，还可以了解是什么原因导致其他文明的领导人与玩家之间的关系良好或恶化。良好的外交关系有利于外交交易的进行，反之则不利。

### 战斗

### 生产与贸易

在「文明III」中，某些天然資源（比如[石油](../Page/石油.md "wikilink")，[煤等等](../Page/煤.md "wikilink")）在被開發後會有機會殆盡。「文明IV」中的天然資源則沒有機會殆盡，但它為城市提供的生產力會因為多種由電腦製做出來的事故而提高或降低。

既有的天然資源可以在新地點被隨機發現。此制度在「文明III」已經出現，在「文明IV」維持不變。

### 宗教

游戏中出现的七大宗教如下，括号中为相应科技：

  - [佛教](../Page/佛教.md "wikilink")（[冥想](../Page/冥想.md "wikilink")）
  - [印度教](../Page/印度教.md "wikilink")（[多神论](../Page/多神论.md "wikilink")）
  - [犹太教](../Page/犹太教.md "wikilink")（[一神论](../Page/一神论.md "wikilink")）
  - [基督教](../Page/基督教.md "wikilink")（[神学](../Page/神学.md "wikilink")）
  - [儒教](../Page/儒教.md "wikilink")（[法典](../Page/法典.md "wikilink")）
  - [道教](../Page/道教.md "wikilink")（[哲学](../Page/哲学.md "wikilink")）
  - [伊斯兰教](../Page/伊斯兰教.md "wikilink")（[君权神授](../Page/君权神授.md "wikilink")）

第一个发现某一宗教的相应科技的文明将有一座城市成为此宗教的“圣城”。例如，如果“中国”是第一个研发出“法典”的文明，那么“中国”的一座城市就成为“儒教”的圣城。圣城的可以获得文化和伟人指数的加成，而最大的好处是获得更多的收入，这就需要在圣城建造“宗教圣坛”。宗教圣坛的建造只有通过大预言家来完成。

### 伟人

《文明IV》中加入了“伟人”系统，游戏中所有的“伟人”都代入了歷史人物的名字。这些“伟人”都是历史人物。游戏中，“伟人”分为七类:
大预言家、大商业家、大艺术家、大科学家、大工程师、大军事家和大间谍；其中后两类是在该游戏的[资料片](../Page/资料片.md "wikilink")《战神》和《超越刀锋》中分别添加。每一类“伟人”都可以加速所属文明的发展进程，并有各自的特殊作用，为玩家提供更多的文明发展途径；\[4\]并且由于所有出现的“伟人”都是历史上的名人，从而提高了玩家在游戏中的沉浸感，给游戏带来了更多的乐趣。\[5\]

### 评分系统

### 胜利方式

游戏常规的取胜方法有如下五种：

  - 征服胜利：征服所有文明，包括消灭或强迫成为自己的[附属国](../Page/附属国.md "wikilink")；
  - 支配胜利：拥有世界大多数的领土和人口，根据选择的游戏难度的不同，要求程度也不同；
  - 外交胜利：在所有文明都参与的[联合国选举中](../Page/联合国.md "wikilink")，成功当选“世界领袖”；
  - 文化胜利：本文明的三个城市达到“传奇”等级；
  - 太空竞赛胜利：成为第一个造好太空船并抵达[半人馬座α星系进行殖民的文明](../Page/南门二.md "wikilink")。

如果到达预先设定的游戏轮数（一般为2050年），而没有任何文明能够取得上述五种取胜方法中的任意一种，则以得分最高者为胜利者。

### 多人游戏

## 新特色

相比《[文明III](../Page/文明III.md "wikilink")》，《文明IV》有很多全新的游戏特点，包括:

### 设定

  - 《文明IV》采用《[席德·梅尔的海盗](../Page/席德·梅尔的海盗.md "wikilink")》中所使用的[三维](../Page/三维图形.md "wikilink")[引擎](../Page/游戏引擎.md "wikilink")（[Gamebryo](../Page/Gamebryo.md "wikilink")）,它使得玩家能够从世界地图级别平滑的过渡到个别城市中的建筑。
  - 18个文明中的13个有两个领袖。每个领袖提供两种特性，基于该领袖历史上统治时期的突出的成就，而每个领袖做为不同的文明会有不同的行为和个性。
  - 有些前作中未使用的历史人物在《文明IV》中作为可供玩家选择的领袖，包括：[阿育王](../Page/阿育王.md "wikilink")、[居魯士二世](../Page/居魯士二世.md "wikilink")、[富兰克林·罗斯福](../Page/富兰克林·罗斯福.md "wikilink")、[哈特謝普蘇特](../Page/哈特謝普蘇特.md "wikilink")、[忽必烈](../Page/忽必烈.md "wikilink")、[高麗太祖王建](../Page/高麗太祖.md "wikilink")、[秦始皇](../Page/秦始皇.md "wikilink")、[萨拉丁和](../Page/萨拉丁.md "wikilink")[维多利亚女王](../Page/维多利亚女王.md "wikilink")。
  - 有五类“伟人”:大预言家、大商业家、大艺术家、大科学家和大工程师。每个伟人给予一些奖励的能力。伟人中包括[柏拉图](../Page/柏拉图.md "wikilink")，[威廉·莎士比亚](../Page/威廉·莎士比亚.md "wikilink")，[米开朗基罗](../Page/米开朗基罗.md "wikilink")，[牛顿](../Page/艾萨克·牛顿.md "wikilink")，和[爱因斯坦](../Page/爱因斯坦.md "wikilink")。
  - 游戏中出现了31类资源，例如[金子](../Page/金.md "wikilink")，[燃料](../Page/燃料.md "wikilink")，[鹿](../Page/鹿.md "wikilink")，等等，所有类型都可用于贸易。
  - 同一条河上或岸边的城市自动有贸易通路产生。
  - 玩家不能从一个建造计划中的生产转换到另一个计划，但某个特定建造计划中的产量会保持下去。例如，如果玩家在建造[神庙](../Page/神庙.md "wikilink")，但是决定切换到[港口](../Page/港口.md "wikilink")，新建筑的生产会从头开始。但是如果玩家换回生产神庙，则以前在神庙上的投入不会消失，但会随着时间有所减少。而且前一个建造计划中多余的产量会转移到下一个建造计划中。
  - 前作中所见的游戏初期的快速扩张变得更困难了。玩家的城市越多，他们就必须为他们的城市付出更多的维护费。所以游戏初期庞大的帝国会夺走玩家可支配的金钱。但是，法院可以减少维护开支，而不是减少腐败。
  - 一个文明的军队不能穿越其他文明的领地，除非他们开战，或者有“通行权协议”（）。
  - 特定城市有特定的特色，例如[工业或](../Page/工业.md "wikilink")[农业](../Page/农业.md "wikilink")。
  - 前作中的污染，城市动乱，和类似的乏味的方面被集成到“城市健康”系统中。某些资源或者建筑，例如麦子和医院，可以改善健康状况，而过多的工业和人口恶化健康状况。
  - 独立的军队可以获得不同类型的作战经验，例如对付特定种类的敌人的奖励或者森林中快速移动的能力。总共有41种不同类型的战斗提升。
  - 军队不再有分别的攻防值。他们有唯一的基准战力，随着情况升高或降低。部队的战力也影响到它能产生的破坏。
  - 更灵活的技术开发。特定的技术可以通过不只一种方法发现，而技术树不再分为各种时代。总共有85项科技。
  - 政府被更灵活的“民事政策”模型所取代，玩家可以设定公民的自由的程度（[奴隶制](../Page/奴隶制.md "wikilink")，[言论自由](../Page/言论自由.md "wikilink")，等等）。玩家的政府通过五类政策的选择定义。
  - 宗教的新概念被引入。游戏中有七大现实宗教，而文明的宗教影响其外交。而且，如果玩家是第一个发现科技树中的特定宗教，他们可以"建立"一个新宗教并设立一个[圣地](../Page/圣地.md "wikilink")。该宗教会逐渐传播。如果玩家控制某个宗教的圣地，他们可以拥有该宗教影响到的城市的视野。
      - 所有七大宗教是泛指的，没有任何宗教有更多的奖励。但是，两个同样宗教的文明（也就是说玩家和一个AI文明）会在贸易和外交上更为友好。
      - 拥有某一宗教圣地城市的玩家，随着该宗教传播到更多的城市，玩家将获得更多的金钱收入——这对于游戏的初期发展尤为重要。
  - AI文明不再拥有整个地图的知识；他们现在和玩家平等。
  - 外交系统作了改进，选项更多了。
  - 有些操作元素被引入，例如可以同时选中多个部队并对他们下达指令。
  - 更多的重点也被放在音乐上；大量使用了巴洛克时期（[巴赫](../Page/巴赫.md "wikilink")），文艺复兴时期（[德普瑞和](../Page/德普瑞.md "wikilink")[帕莱斯特里纳等](../Page/帕莱斯特里纳.md "wikilink")），古典时期（[勃拉姆斯和](../Page/勃拉姆斯.md "wikilink")[贝多芬](../Page/贝多芬.md "wikilink")）和自创作的（主要由[傑夫·布里吉完成](../Page/傑夫·布里吉.md "wikilink")）音乐，使得音乐在游戏中使用的程度超越了所有前作。每个领袖（除了忽必烈）都有自己的外交音乐主题。

### 音乐与视觉效果

  - 《文明IV》的主题曲《[我们的天父](../Page/我们的天父.md "wikilink")》采用了一种非洲使用人数最多的语言[斯瓦西里语](../Page/斯瓦西里语.md "wikilink")，该主题曲备受好评。在第53屆葛萊美獎，《我們的天父》被提名並最終獲得最佳伴唱器樂編曲獎。成為第一首獲得葛萊美獎的電玩遊戲音樂。\[6\]
  - 自定义游戏中广泛采用了贝多芬交响曲作为背景音乐，如[贝多芬第一号交响曲的第二乐章](../Page/第1號交響曲_\(貝多芬\).md "wikilink")、[第五号交响曲的第二乐章](../Page/第5號交響曲_\(貝多芬\).md "wikilink")、[第六交响曲的第一](../Page/贝多芬第六交响曲.md "wikilink")、二乐章，等等。

## 资料片

### 战神

“战神”又译为“战争之王”，是《文明IV》的第一个资料片。在原版基础上增加了6个新文明、10位新领袖、3座新奇观、外交附庸条约、文明特色建筑，伟人系统中添加“大军事家”。

### 超越刀锋

“超越刀锋”是《文明IV》的第二个资料片。在“战神”基础上增加了10个新文明、16位新领袖、7座新奇观、6个新科技，以及随机事件、商业公司、间谍系统等新概念，伟人系统中添加“大间谍家”。

### 殖民统治

“殖民统治”实际上是94年老游戏《席德梅尔的殖民帝国》的复刻版，安装时无需《文明IV》原版支持，可以当做一款独立的游戏。尽管使用了与《文明IV》相同的绘图引擎与技术，但游戏规则、单位、特色等截然不同。玩家可以扮演英国、法国、西班牙、荷兰等势力，在新大陆建立殖民地，最终目标是使殖民地独立。

## 参考文献

## 外部链接

  - [文明4的官方网站](http://www.2kgames.com/civ4/home.htm)

  - [文明联盟俱乐部──在线文明百科](http://www.civclub.net/html_c4/civdoc/index.htm)，含有非常详细的关于文明4的信息。

## 參見

  - [Freeciv](../Page/Freeciv.md "wikilink") -
    与《文明》类似的[開源游戏](../Page/開源.md "wikilink")。
  - [文明III](../Page/文明III.md "wikilink")
  - [文明V](../Page/文明V.md "wikilink")
  - [殖民帝國 (遊戲)](../Page/殖民帝國_\(遊戲\).md "wikilink")
  - [文明 (游戏)](../Page/文明_\(游戏\).md "wikilink")

{{-}}

[de:Sid Meier’s Civilization\#Civilization
IV](../Page/de:Sid_Meier’s_Civilization#Civilization_IV.md "wikilink")

[Category:Windows游戏](../Category/Windows游戏.md "wikilink") [Category:Mac
OS遊戲](../Category/Mac_OS遊戲.md "wikilink")
[4](../Category/文明系列电子游戏.md "wikilink")
[Category:2005年电子游戏](../Category/2005年电子游戏.md "wikilink")
[Category:回合制策略遊戲](../Category/回合制策略遊戲.md "wikilink")
[Category:多人及單人電子遊戲](../Category/多人及單人電子遊戲.md "wikilink")
[Category:多人在线游戏](../Category/多人在线游戏.md "wikilink")
[Category:美國開發電子遊戲](../Category/美國開發電子遊戲.md "wikilink")
[Category:有资料片的游戏](../Category/有资料片的游戏.md "wikilink")
[Category:合作模式遊戲](../Category/合作模式遊戲.md "wikilink")
[Category:熱座模式遊戲](../Category/熱座模式遊戲.md "wikilink")
[Category:Gamebryo引擎游戏](../Category/Gamebryo引擎游戏.md "wikilink")
[Category:4X電子遊戲](../Category/4X電子遊戲.md "wikilink")
[Category:官方简体中文化游戏](../Category/官方简体中文化游戏.md "wikilink")
[Category:Steam商店遊戲](../Category/Steam商店遊戲.md "wikilink")
[Category:中电博亚游戏](../Category/中电博亚游戏.md "wikilink")

1.  <http://ir.take2games.com/ReleaseDetail.cfm?ReleaseID=235572>
2.
3.
4.
5.
6.  <http://venturebeat.com/2011/02/13/baba-yetu-video-game-grammy/>