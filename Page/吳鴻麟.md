**吳鴻麟**（），[中國國民黨政治人物](../Page/中國國民黨.md "wikilink")，[臺灣](../Page/臺灣.md "wikilink")[桃園縣](../Page/桃園市.md "wikilink")（今桃園市）[客家人](../Page/客家人.md "wikilink")、醫師。曾歷任桃園縣議員、議長、[縣長](../Page/縣長.md "wikilink")。設有吳鴻麟獎學金。

## 生平

吳鴻麟於1899年（明治32年）9月28日在[中壢出生](../Page/中壢區.md "wikilink")，1925年3月於[臺北醫學專門學校以優異成績畢業](../Page/臺北醫學專門學校.md "wikilink")，之後進入日本赤十字社台北支部醫院擔任醫員，1930年時與兄長[吳鴻森一同開業](../Page/吳鴻森.md "wikilink")，為私立中壢醫院副院長\[1\]。後擔任皇民奉公會中壢郡支會衛生保健班長。

## 家族

  - 父[吳榮隸](../Page/吳榮隸.md "wikilink")
  - 長兄[吳鴻森曾任](../Page/吳鴻森.md "wikilink")[臺灣省參議會參議員](../Page/臺灣省參議會.md "wikilink")、[國民參政員](../Page/國民參政員.md "wikilink")。
  - [孿生兄弟](../Page/孿生.md "wikilink")[法官](../Page/法官.md "wikilink")[吳鴻麒](../Page/吳鴻麒.md "wikilink")，在1947年的[二二八事件中遭](../Page/二二八事件.md "wikilink")[國軍部隊殺害](../Page/國軍.md "wikilink")。
  - 子[吳伯雄曾任國民黨籍桃園縣第七屆縣長](../Page/吳伯雄.md "wikilink")，現為國民黨榮譽主席。
  - 孫[吳志揚現任](../Page/吳志揚.md "wikilink")[中華職棒聯盟會長](../Page/中華職棒聯盟.md "wikilink")、吳志剛為國民黨籍台北市議員。

|- |colspan="3"
style="text-align:center;"|**[桃園縣](../Page/桃園市.md "wikilink")[政府](../Page/桃園縣政府.md "wikilink")**


## 參考文獻

  - 劉寧顏編，《重修台灣省通志》，台北市，台灣省文獻委員會，1994年。

[category:中國國民黨黨員](../Page/category:中國國民黨黨員.md "wikilink")

[Category:中壢吳氏](../Category/中壢吳氏.md "wikilink")
[Category:桃園縣縣長](../Category/桃園縣縣長.md "wikilink")
[Category:桃園縣議長](../Category/桃園縣議長.md "wikilink")
[Category:第4屆桃園縣議員](../Category/第4屆桃園縣議員.md "wikilink")
[Category:第3屆桃園縣議員](../Category/第3屆桃園縣議員.md "wikilink")
[Category:第2屆桃園縣議員](../Category/第2屆桃園縣議員.md "wikilink")
[Category:臺灣醫院院長](../Category/臺灣醫院院長.md "wikilink")
[Category:九州大學校友](../Category/九州大學校友.md "wikilink")
[Category:國立臺灣大學醫學院校友](../Category/國立臺灣大學醫學院校友.md "wikilink")
[Category:客家裔臺灣人](../Category/客家裔臺灣人.md "wikilink")
[Hong鴻](../Category/中壢吳氏.md "wikilink")
[Category:臺灣雙胞胎](../Category/臺灣雙胞胎.md "wikilink")
[Category:醫生出身的政治人物](../Category/醫生出身的政治人物.md "wikilink")

1.