[Ikeda_Shuichi_"The_World_of_Gundam"_at_Opening_Ceremony_of_the_28th_Tokyo_International_Film_Festival_(22405153956).jpg](https://zh.wikipedia.org/wiki/File:Ikeda_Shuichi_"The_World_of_Gundam"_at_Opening_Ceremony_of_the_28th_Tokyo_International_Film_Festival_\(22405153956\).jpg "fig:Ikeda_Shuichi_\"The_World_of_Gundam\"_at_Opening_Ceremony_of_the_28th_Tokyo_International_Film_Festival_(22405153956).jpg")（2015年）\]\]

**池田秀一**（）是[東京都出身的男性](../Page/東京都.md "wikilink")[聲優](../Page/聲優.md "wikilink")。隸屬於[俳協](../Page/東京俳優生活協同組合.md "wikilink")。

前妻是演員的[戶田惠子](../Page/戶田惠子.md "wikilink")。現任妻子是配音員[玉川紗己子](../Page/玉川紗己子.md "wikilink")。

代表作是『[機動戰士GUNDAM](../Page/機動戰士GUNDAM.md "wikilink")』的[夏亞·阿茲納布爾](../Page/夏亞·阿茲納布爾.md "wikilink")、『[神劍闖江湖](../Page/神劍闖江湖.md "wikilink")』的比古清十郎、『[ONE
PIECE](../Page/ONE_PIECE.md "wikilink")』的紅髮傑克、『[名偵探柯南](../Page/名偵探柯南.md "wikilink")』的『[赤井秀一](../Page/赤井秀一.md "wikilink")』等。

## 略歷

出道極早，少年劇團出身，少年時代都是演員（[童星](../Page/童星.md "wikilink")），大多演出電視連續劇那種父母雙亡、孩子過繼到親戚家、被繼母虐待努力打工向上的小孩。在1977年曾經在日本金田一耕助系列電影獄門島出演警察了沢一角色。長大後開始接配音工作，主要是幫外國電影重新配上日語，一開始受到排擠「露臉的演員幹麼來幹這不用露臉的配音工作」而有隔閡。經由當時[哆啦A夢](../Page/哆啦A夢.md "wikilink")（[機器貓小叮噹](../Page/機器貓小叮噹.md "wikilink")）的[剛田武](../Page/剛田武.md "wikilink")（[技安](../Page/技安.md "wikilink")）聲優[立壁和也的介紹](../Page/立壁和也.md "wikilink")，認識了[松浦典良](../Page/松浦典良.md "wikilink")，也就是後來「[機動戰士GUNDAM](../Page/機動戰士GUNDAM.md "wikilink")」的[音響監督](../Page/音響監督.md "wikilink")。松浦典良介紹池田秀一進入動畫聲優業界，並接了第一份工作「[無敵鋼人泰坦3](../Page/無敵鋼人泰坦3.md "wikilink")」第21話「[音楽は万丈を征す](../Page/音楽は万丈を征す.md "wikilink")」的反派，結識[鈴置洋孝](../Page/鈴置洋孝.md "wikilink")、[井上瑤](../Page/井上瑤.md "wikilink")、[白石冬美等人](../Page/白石冬美.md "wikilink")。由於這次配音工作並不順利，池田秀一曾認為這是他最初也是最後的動畫聲優演出。但由於松浦典良盛情難卻，礙於人情壓力，再度在松浦典良的推薦下前往新作「機動戰士GUNDAM」的試音。（舊說1970年代中期由富野喜幸(後更名為[富野由悠季](../Page/富野由悠季.md "wikilink"))勸進加入《[無敵鋼人泰坦3](../Page/無敵鋼人泰坦3.md "wikilink")》的客串聲優演出。此說已由池田秀一在2006年出版的自傳否定。）
因為同台演出機動戰士GUNDAM的緣故與[戶田惠子結婚](../Page/戶田惠子.md "wikilink")，後來離婚，現任妻子是聲優[玉川紗己子](../Page/玉川紗己子.md "wikilink")。

## 軼事、名言

池田秀一在接到夏亞的配音工作前，這種「好孩子印象」一直強加在他身上揮之不去。少年時代去買東西的時候，商店老闆娘都會跟他說「要多吃一點」、「要加油」然後多送點東西給他。池田秀一當時心想：「我真的沒有繼母在虐待我啊…」

池田秀一在自傳中說道：「我已經演夏亞演了30年了，夏亞已經成為我身體的一部分了，我和夏亞有種一心同體的感覺，演出夏亞的時候有種感覺就像是跟好友把酒言歡般的安心感。夏亞對我來說不僅僅是個好夥伴，也是教導我作為一個聲優的老師。」

## 演出作品

### 電視動畫

  - [悲慘世界](../Page/悲慘世界_\(動畫\).md "wikilink")（馬留斯·彭梅西）
  - [羅德斯島戰記](../Page/羅德斯島戰記.md "wikilink")（卡修）
  - [銀河英雄傳說](../Page/銀河英雄傳說.md "wikilink")（烏里希·克斯拉）

<!-- end list -->

  - 1979年

<!-- end list -->

  - [機動戰士GUNDAM](../Page/機動戰士GUNDAM.md "wikilink")（[夏亞·阿茲納布爾](../Page/夏亞·阿茲納布爾.md "wikilink")）
  - [銀河鐵道999](../Page/銀河鐵道999.md "wikilink")（ナンミ）

<!-- end list -->

  - 1981年

<!-- end list -->

  - [千年女王](../Page/千年女王.md "wikilink")（ハンニバル）
  - [戰國魔神豪將軍](../Page/戰國魔神豪將軍.md "wikilink")（フランシス・ルグラン）

<!-- end list -->

  - 1985年

<!-- end list -->

  - [機動戰士Z
    GUNDAM](../Page/機動戰士Z_GUNDAM.md "wikilink")（[夏亞·阿茲納布爾](../Page/夏亞·阿茲納布爾.md "wikilink")/[克瓦多羅·巴吉納](../Page/克瓦多羅·巴吉納.md "wikilink")）
  - [火焰的阿爾卑斯玫瑰 茱蒂&藍迪](../Page/阿爾卑斯玫瑰_\(漫畫\).md "wikilink")（喬治·德·古爾蒙）

<!-- end list -->

  - 1986年

<!-- end list -->

  - [機動戰士鋼彈 ZZ](../Page/機動戰士鋼彈_ZZ.md "wikilink")（クワトロ・バジーナ）
  - [聖鬥士星矢](../Page/聖鬥士星矢.md "wikilink")（天蠍座米羅）
  - [機器勇士](../Page/機器勇士.md "wikilink")（ケンポーロボ、デビルロック・クレイ）

<!-- end list -->

  - 1988年

<!-- end list -->

  - [森林好小子](../Page/森林好小子.md "wikilink")（火堂害）

<!-- end list -->

  - 1991年

<!-- end list -->

  - [城市獵人](../Page/城市獵人.md "wikilink")（桑田達華）

<!-- end list -->

  - 1994年

<!-- end list -->

  - [七海的堤可](../Page/七海的堤可.md "wikilink")（史考特‧辛普森）

<!-- end list -->

  - 1996年

<!-- end list -->

  - [神劍闖江湖](../Page/神劍闖江湖.md "wikilink")（比古清十郎）

<!-- end list -->

  - 1997年

<!-- end list -->

  - [金田一少年之事件簿](../Page/金田一少年之事件簿.md "wikilink")（雪室憂一、聖正秋人）

<!-- end list -->

  - 1999年

<!-- end list -->

  - [亞歷山大戰記](../Page/亞歷山大戰記.md "wikilink")（托勒密）
  - [ONE PIECE](../Page/ONE_PIECE.md "wikilink")（紅髮傑克）

<!-- end list -->

  - 2001年

<!-- end list -->

  - [ARMS神臂](../Page/ARMS神臂.md "wikilink")（ジャバウォック）
  - [名偵探柯南](../Page/名偵探柯南.md "wikilink")（赤井秀一）

<!-- end list -->

  - 2003年

<!-- end list -->

  - [神槍少女](../Page/神槍少女.md "wikilink")（フェルミ）

<!-- end list -->

  - 2004年

<!-- end list -->

  - [SD鋼彈FORCE](../Page/SD鋼彈FORCE.md "wikilink")（レッドザコ/コマンダーサザビー）
  - [機動戰士GUNDAM SEED
    DESTINY](../Page/機動戰士GUNDAM_SEED_DESTINY.md "wikilink")（吉伯特·杜蘭朵）
  - [Keroro軍曹](../Page/Keroro軍曹.md "wikilink")（西澤梅雄）
  - [天上天下](../Page/天上天下.md "wikilink")（棗慎）
  - [MONSTER](../Page/MONSTER.md "wikilink")(馬汀)

<!-- end list -->

  - 2005年

<!-- end list -->

  - [植木的法则](../Page/植木的法则.md "wikilink")（馬格雷特）
  - [火影忍者](../Page/火影忍者.md "wikilink")（サザナミ/漣漪 159集）

<!-- end list -->

  - 2006年

<!-- end list -->

  - [傳頌之物](../Page/傳頌之物.md "wikilink")（德伊）
  - [天使心](../Page/天使心.md "wikilink") (信)
  - [彩雲國物語](../Page/彩雲國物語.md "wikilink")（紅邵可／旁白）

<!-- end list -->

  - 2007年

<!-- end list -->

  - [金田一少年之事件簿](../Page/金田一少年之事件簿.md "wikilink")（緋色景介）
  - [彩雲國物語第](../Page/彩雲國物語.md "wikilink")2季（紅邵可／旁白）
  - [瀨戶的新娘](../Page/瀨戶的新娘_\(漫畫\).md "wikilink")（不知火明乃之兄）
  - [名偵探柯南](../Page/名偵探柯南.md "wikilink")（黑羽盜一）

<!-- end list -->

  - 2008年

<!-- end list -->

  - [秘密 〜The
    Revelation〜](../Page/秘密_〜The_Revelation〜.md "wikilink")（旁白，非角色）
  - [波菲的漫長旅程](../Page/波菲的漫長旅程.md "wikilink")（朱利安尼神父）
  - [十字架與吸血鬼](../Page/十字架與吸血鬼.md "wikilink")（九曜）
  - [毀滅世界的六人](../Page/毀滅世界的六人.md "wikilink") （雅比，第6、12集）

<!-- end list -->

  - 2010年

<!-- end list -->

  - [魔術快斗](../Page/魔術快斗.md "wikilink")（黑羽盜一）

<!-- end list -->

  - 2011年

<!-- end list -->

  - [C³ -魔幻三次方-](../Page/C³_-魔幻三次方-.md "wikilink")（世界橋加百列）
  - [銀魂'](../Page/銀魂'.md "wikilink")（米墮卿／SAGI）

<!-- end list -->

  - 2013年

<!-- end list -->

  - [Hunter × Hunter](../Page/Hunter_×_Hunter.md "wikilink")（凱特）

<!-- end list -->

  - 2014年

<!-- end list -->

  - [即使如此世界依然美麗](../Page/即使如此世界依然美麗.md "wikilink")（元老·沃登）
  - [魔术快斗1412](../Page/魔术快斗.md "wikilink")（黑羽盜一）
  - [我，要成為雙馬尾](../Page/我，要成為雙馬尾.md "wikilink")（雙馬尾）

<!-- end list -->

  - 2015年

<!-- end list -->

  - （サンタ）

  - [卡片鬥爭\!\! 先導者G GIRS危機篇](../Page/卡片戰鬥先導者.md "wikilink")（明神龍頭）

<!-- end list -->

  - 2016年

<!-- end list -->

  - [機動戰士高達UC RE:0096](../Page/機動戰士鋼彈UC.md "wikilink")（**弗爾·伏朗托**、旁白）
  - [卡片鬥爭\!\! 先導者G 超越之門篇](../Page/卡片戰鬥先導者.md "wikilink")（明神龍頭）
  - [名侦探柯南 章节*ONE*
    缩小的名侦探](../Page/名侦探柯南_章节''ONE''_缩小的名侦探.md "wikilink")（赤井秀一）

<!-- end list -->

  - 2017年

<!-- end list -->

  - [ONE PIECE 東海特別篇
    ～魯夫與4名伙伴的大冒險～](../Page/ONE_PIECE特別篇#東海特別篇_～魯夫與4名伙伴的大冒險～.md "wikilink")（傑克）

<!-- end list -->

  - 2018年

<!-- end list -->

  - [PERSONA5 the Animation](../Page/女神異聞錄5.md "wikilink")（酒醉男／獅童正義）
  - [宇宙戰艦提拉米蘇II](../Page/宇宙戰艦提拉米蘇.md "wikilink")（凱雷德·卡迪拉克）

### OVA

  - [亞爾斯蘭戰記](../Page/亞爾斯蘭戰記.md "wikilink")（ヒルメス）
  - [亞爾斯蘭戰記2](../Page/亞爾斯蘭戰記.md "wikilink")（ヒルメス）
  - [亞爾斯蘭戰記 汗血公路](../Page/亞爾斯蘭戰記.md "wikilink")（ヒルメス）
  - [亞爾斯蘭戰記 征馬孤影](../Page/亞爾斯蘭戰記.md "wikilink")（ヒルメス）
  - [星艦戰將](../Page/星艦戰將.md "wikilink")（アズマ）
  - 雲界迷宮ZEGUY（諸葛亮孔明）
      - [SD戰國傳 武者七人眾篇](../Page/SD戰國傳_武者七人眾篇.md "wikilink")（隱密頑馱無/農丸頑馱無）
  - 風を抜け\!（ジェフ・アネモス）
  - 機動戰士SD GUNDAM 夢のマロン社「宇宙の旅」（アッガイ）
  - [SD高達外傳
    捷古自護篇](../Page/SD高達外傳_捷古自護篇.md "wikilink")（騎士[馬沙](../Page/馬沙.md "wikilink")/黄金騎士[百式](../Page/MSN-100系列機動戰士.md "wikilink")）
  - [SD高達外傳 聖機兵物語](../Page/SD高達外傳_聖機兵物語.md "wikilink")
    (軍師[古華多羅](../Page/古華多羅.md "wikilink")/騎士[馬沙](../Page/馬沙.md "wikilink"))
  - [銀河英雄傳說](../Page/銀河英雄傳說.md "wikilink")（烏里希·克斯拉）
  - 幻夢戰記レダ（ゼル）
  - 縣立地球防衛軍（木曽屋・チルソニアン・文左衛門Jr.）
  - [閃電霹靂車 SAGA](../Page/閃電霹靂車.md "wikilink")（名雲京志郎）
  - [閃電霹靂車 SIN](../Page/閃電霹靂車.md "wikilink")（名雲京志郎）
  - 超人洛克 ロードレオン（ロードレオン）
  - ドリームハンター麗夢（鬼童海丸）
  - 麻雀飛翔傳 哭きの竜（竜）
  - [羅德島戰記](../Page/羅德島戰記.md "wikilink")（カシュー王）
  - [机动战士GUNDAM SEED C.E.73
    STARGAZER](../Page/机动战士GUNDAM_SEED_C.E.73_STARGAZER.md "wikilink")（吉伯特·杜蘭朵）
  - [機動戰士GUNDAM UC](../Page/機動戰士GUNDAM_UC.md "wikilink")（弗爾·伏朗托）
  - [ONE PIECE FILM
    強者天下](../Page/ONE_PIECE_FILM_強者天下.md "wikilink")（紅髮傑克）

**1983年**

  - （**アレックス・ライガー**）

### 劇場版動畫

  - [亞爾斯蘭戰記](../Page/亞爾斯蘭戰記.md "wikilink")（ヒルメス）
  - [SD GUNDAM戰國傳](../Page/SD_GUNDAM戰國傳.md "wikilink")（武者農丸）
  - [機動戰士GUNDAM](../Page/機動戰士GUNDAM.md "wikilink")（夏亞·阿茲納布爾）
  - [機動戰士GUNDAMⅡ哀·戰士篇](../Page/機動戰士GUNDAMⅡ哀·戰士篇.md "wikilink")（夏亞·阿茲納布爾）
  - [機動戰士GUNDAMⅢ](../Page/機動戰士GUNDAMⅢ.md "wikilink")（夏亞·阿茲納布爾）
  - [機動戰士GUNDAM 逆襲的夏亞](../Page/機動戰士GUNDAM_逆襲的夏亞.md "wikilink")（夏亞·阿茲納布爾）
  - [機動戰士Ζ
    GUNDAMⅠ－星之繼承者－](../Page/機動戰士Ζ_GUNDAMⅠ－星之繼承者－.md "wikilink")（夏亞·阿茲納布爾）
  - [機動戰士Ζ
    GUNDAMⅡ－戀人們－](../Page/機動戰士Ζ_GUNDAMⅡ－戀人們－.md "wikilink")（夏亞·阿茲納布爾）
  - [機動戰士Ζ
    GUNDAMⅢ－星之鼓動就是愛－](../Page/機動戰士Ζ_GUNDAMⅢ－星之鼓動就是愛－.md "wikilink")（夏亞·阿茲納布爾）
  - [機動戰士GUNDAM UC](../Page/機動戰士GUNDAM_UC.md "wikilink")（弗爾·伏朗托）
  - [蠟筆小新 伝説を呼ぶ 踊れ\!アミーゴ\!](../Page/蠟筆小新.md "wikilink")（アミーゴスズキ）
  - 幻夢戰記レダ（ゼル）
  - Pa-Pa-Pa ザ★ムービー パーマン（魔土災炎）
  - ヴィナス戰記（カーツ）
  - 火の鳥2772 愛のコスモゾーン（ロック）
  - プロジェクトA子（キャプテン）
  - わが青春のアルカディア（ゾル）
  - [ONE PIECE THE MOVIE
    デッドエンドの冒険](../Page/ONE_PIECE.md "wikilink")（紅髮傑克）
  - [名偵探柯南：異次元的狙擊手](../Page/名偵探柯南：異次元的狙擊手.md "wikilink")（赤井秀一）
  - [名偵探柯南：純黑的惡夢](../Page/名偵探柯南：純黑的惡夢.md "wikilink")（赤井秀一）

### 遊戲

  - [妖精戰士2](../Page/妖精戰士.md "wikilink")（シュウ）
  - [妖精戰士3](../Page/妖精戰士.md "wikilink")（シュウ）
  - [安琪莉可系列](../Page/安琪莉可系列.md "wikilink")（前・緑の守護聖カティス）
  - [伊蘇IV -The Dawn of Ys-](../Page/伊蘇.md "wikilink")（エルディール）
  - [EVE The Lost One](../Page/EVE_The_Lost_One.md "wikilink")（見城陽一）
  - [女神戰記](../Page/女神戰記.md "wikilink")（奧丁、巴巴羅薩）
  - [女神戰記2](../Page/女神戰記2.md "wikilink")（奧丁）
  - [傳頌之物 散りゆく者への子守唄](../Page/傳頌之物.md "wikilink")（ディー）
  - 改造町人シュビビンマン3（魔族隊長）
  - [槍神O.D.](../Page/槍神O.D..md "wikilink")（ガリーノ・クレアーレ・コルシオネ）
  - [機動戰士GUNDAM 連邦 vs.
    Zeon](../Page/機動戰士GUNDAM.md "wikilink")（[夏亞·阿茲納布爾](../Page/夏亞·阿茲納布爾.md "wikilink")）
  - [機動戰士GUNDAM GUNDAM vs. Z
    GUNDAM](../Page/機動戰士GUNDAM.md "wikilink")（[夏亞·阿茲納布爾](../Page/夏亞·阿茲納布爾.md "wikilink")、[克瓦多羅·巴吉納](../Page/克瓦多羅·巴吉納.md "wikilink")）
  - [機動戰士ΖGUNDAM A.E.U.G. vs.
    Titans](../Page/機動戰士GUNDAM.md "wikilink")（[克瓦多羅·巴吉納](../Page/克瓦多羅·巴吉納.md "wikilink")）
  - [機動戰士GUNDAM Climax U.C.](../Page/機動戰士GUNDAM.md "wikilink")
    （[夏亞·阿茲納布爾](../Page/夏亞·阿茲納布爾.md "wikilink")、[克瓦多羅·巴吉納](../Page/克瓦多羅·巴吉納.md "wikilink")）
  - [機動戰士GUNDAM SEED DESTINY 連合 vs.
    Z.A.F.T.II](../Page/機動戰士GUNDAM.md "wikilink") （吉伯特·杜蘭朵）
  - [櫻花大戰2 ～君、死にたもうことなかれ～](../Page/櫻花大戰.md "wikilink")（ロベール・ド・シャトーブリアン）
  - [閃電霹靂車 Road to the Evolution](../Page/閃電霹靂車.md "wikilink")（名雲京志郎）
  - [閃電霹靂車 Road to the Infinity](../Page/閃電霹靂車.md "wikilink")（名雲京志郎）
  - [閃電霹靂車 Road to the Infinity 2](../Page/閃電霹靂車.md "wikilink")（名雲京志郎）
  - [閃電霹靂車 Road to the Infinity 3](../Page/閃電霹靂車.md "wikilink")（名雲京志郎）
  - [閃電霹靂車 Road to the Infinity 4](../Page/閃電霹靂車.md "wikilink")（名雲京志郎）
  - [THE狙擊手](../Page/SIMPLE_2000系列.md "wikilink")（ハリー・C・スペンサー）
  - [THE狙擊手2～惡夢的子彈～](../Page/SIMPLE_2000系列.md "wikilink")（ハリー・C・スペンサー）
  - [闇影之心2](../Page/闇影之心2.md "wikilink")（ブランカ）
  - [獸王記](../Page/獸王記.md "wikilink")（PCE CD-ROM2版，旁白）
  - 新紀幻想スペクトラルソウルズII（マックス）
  - [超級機器人大戰系列](../Page/超級機器人大戰.md "wikilink")（[夏亞·阿茲納布爾](../Page/夏亞·阿茲納布爾.md "wikilink")、[克瓦多羅·巴吉納](../Page/克瓦多羅·巴吉納.md "wikilink")）
  - ダンジョンズ&ドラゴンズ オンライン（日文版，迷宮Master）
  - [超剛戰紀 機界王](../Page/超剛戰紀_機界王.md "wikilink")（シャドーレッド）
  - [天外魔境II 卍MARU](../Page/天外魔境.md "wikilink")（ヨミ、紅丸、磯花法師）
  - [天外魔境III NAMIDA](../Page/天外魔境.md "wikilink")（ニギ）
  - [天下人](../Page/天下人.md "wikilink")（[織田信長](../Page/織田信長.md "wikilink")）
  - [戰國BASARA4](../Page/戰國BASARA.md "wikilink")（[足利義輝](../Page/足利義輝.md "wikilink")）
  - [NOT TREASURE HUNTER](../Page/NOT_TREASURE_HUNTER.md "wikilink")
    （ジェームス・アークライト）
  - [古大陸物語～四個封印～](../Page/古大陸物語.md "wikilink")（ランティア）
  - [幽靈王國](../Page/幽靈王國.md "wikilink")（冥王シードル）
  - [波波洛克洛伊斯物語](../Page/波波洛克洛伊斯物語.md "wikilink")（白騎士）
  - [全民高爾夫3](../Page/全民高爾夫.md "wikilink")・[全民高爾夫4](../Page/全民高爾夫.md "wikilink")（クーガー）
  - [銀河少女警察 The 3rd Planet](../Page/銀河少女警察.md "wikilink")（マクシミリアン・クロエ）
  - [王國之心 Re:chain of memories](../Page/王國之心.md "wikilink")（マールーシャ）
  - [真‧女神轉生4 Final Re:SHIN・MEGAMITENSEI IV
    Final](../Page/真‧女神轉生4_Final.md "wikilink")（ダグサ）
  - [女神異聞錄5](../Page/女神異聞錄5.md "wikilink") ( 獅童正義 )
  - [最终幻想14 final fantasy
    xiv](../Page/最终幻想14_final_fantasy_xiv.md "wikilink") ( 拉哈布雷亚
    )
  - [Granblue Fantasy](../Page/碧藍幻想.md "wikilink") ( 碧騎士華爾佛里德 )

### 日語配音

### 電視演出

  - [龍馬來了](../Page/龍馬來了.md "wikilink")（忠吉（友情客串））
  - [黑貓亭事件](../Page/黑貓亭事件.md "wikilink")（日兆）
  - NHK 2015年 大河劇 [花燃](../Page/花燃.md "wikilink") (旁白)

### 特攝

  - [超人力霸王系列](../Page/超人力霸王.md "wikilink")
  - [迪迦·奥特曼第七集](../Page/迪迦奥特曼.md "wikilink")
    降落到地球的外星人（アメミヤ・シゲキ技官）※以演員身份演出
  - 超級戰隊系列
  - 高速戰隊ターボレンジャー（氷魔的聲音）
  - 忍風戰隊ハリケンジャー（サンダール的聲音）

### 廣播劇CD

  - [妖精戰士II 炎のエルク](../Page/妖精戰士.md "wikilink")（シュウ）
  - [阿爾斯朗戰記 -妖雲群行-](../Page/阿爾斯朗戰記.md "wikilink")（ヒルメス）
  - [植木的法則 The Law Of Drama\!](../Page/植木的法則.md "wikilink")（マーガレット）
  - ガイア・ギア（夏亞·阿茲那布爾）
  - 逆境ナイン（サカキバラ・ゴウ）
  - [彩雲國物語～はじまりの風は紅く～](../Page/彩雲國物語.md "wikilink")（紅邵可）
  - [彩雲國物語～物思う君に愛の手を](../Page/彩雲國物語.md "wikilink")（紅邵可）
  - CD Theater Dragon Quest（IV・ピサロ）
  - 四龍島系列「龍はまどろむ」
  - [羅德島戰記 風と炎の魔神](../Page/羅德島戰記.md "wikilink") CD Cinema
  - ナイトウィザードファンブック「パワー・オブ・ラブ」（サイモン・マーガス）
  - 機工魔術士-enchanter-前編・後編（パラケルスス）

### 書籍

  - 「シャアへの鎮魂歌 わが青春の赤い彗星」，池田秀一，出版社: ワニブックス (2006/12/21)，ISBN
    978-4847017001，ASIN: 4847017005。

### 其他

## 來源

## 外部連結

  - [東京俳優生活協同組合官方網站簡歷](https://haikyo.co.jp/profile/profile.php?ActorID=10587)

  - [「シャアへの鎮魂歌
    わが青春の赤い彗星」的讀後感想](https://web.archive.org/web/20070210160408/http://aqery.org/blog/?p=251)

  - [池田秀一的聲優道](http://seigura.com/senior/road/20160223_16706/1.html)，全3回完結，2016年3月31日最後查閱。

  - [池田秀一的聲優道，完整中文翻譯](http://ccsx.tw/2016/03/01/shuichi-ikeda-road1/)，全3回完結，2016年3月31日最後查閱。

[Category:日本男性配音員](../Category/日本男性配音員.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")
[Category:旁白](../Category/旁白.md "wikilink")