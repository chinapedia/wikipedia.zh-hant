**張乾琦**（）是[台灣攝影師](../Page/台灣攝影.md "wikilink")，英文名為*Chien-Chi
Chang*，也是台灣唯一[馬格蘭攝影通訊社的會員](../Page/馬格蘭攝影通訊社.md "wikilink")，已加入[美國國籍](../Page/美國.md "wikilink")。

他最具代表性的作品是名為《The
Chain》的攝影畫冊，內容是在一家[台灣傳統宗教形式的精神疾病收容機構](../Page/台灣.md "wikilink")「[龍發堂](../Page/龍發堂.md "wikilink")」拍攝的系列影像，反映[精神病人在此機構的生活狀態](../Page/精神病人.md "wikilink")。

## 學歷

  - [豐原高中](../Page/豐原高中.md "wikilink")
  - [東吳大學英文系學士](../Page/東吳大學_\(台灣\).md "wikilink")
  - [美國](../Page/美國.md "wikilink")[印第安那大學教育碩士](../Page/印第安那大學.md "wikilink")

## 經歷

  - [西雅圖時報](../Page/西雅圖時報.md "wikilink")(Seattle Times)攝影記者
  - [巴爾的摩太陽報](../Page/巴爾的摩太陽報.md "wikilink")(Baltimore Sun)攝影記者

## 獲獎

  - 美國年度大專院校傑出攝影家首獎(1990年)
  - [美國年度新聞攝影獎](../Page/美國年度新聞攝影獎.md "wikilink")（）
  - 荷蘭[世界新聞攝影比賽](../Page/世界新聞攝影比賽.md "wikilink")(World Press
    Photo)日常生活類首獎
  - [尤金‧史密斯獎](../Page/尤金‧史密斯獎.md "wikilink") （）

[Category:台灣攝影師](../Category/台灣攝影師.md "wikilink")
[Category:台中市人](../Category/台中市人.md "wikilink")
[C](../Category/東吳大學校友.md "wikilink")
[C](../Category/印第安那大學校友.md "wikilink")
[Category:馬格蘭攝影師](../Category/馬格蘭攝影師.md "wikilink")