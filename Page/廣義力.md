**廣義力**是[拉格朗日力學裏面的一個基本概念](../Page/拉格朗日力學.md "wikilink")。在一個物理系統裏，因為力\(\mathbf{F}\,\!\)，一個粒子經過[虛位移](../Page/虛位移.md "wikilink")\(\delta \mathbf{r}\,\!\)，所作的[虛功](../Page/虛功_\(物理\).md "wikilink")\(\delta W\,\!\)是

\[\delta W = \mathbf{F} \cdot \delta \mathbf{r}\,\!\]。

轉換至[廣義坐標](../Page/廣義坐標.md "wikilink")\(q_1,\ q_2,\ q_3,\ \dots\ q_N\,\!\)，

\[\delta W = \sum_{j=1}^N\ \mathbf{F} \cdot  \frac {\partial \mathbf{r}}{\partial q_j} \delta q_j\,\!\]。

在上面这个方程的右端，位于虚位移前面的这两项的整体即为廣義力，用\(\boldsymbol{\mathcal{F}}\,\!\)表示为：

\[\mathcal{F}_j=  \mathbf{F} \cdot  \frac {\partial \mathbf{r}}{\partial q_j}\,\!\]。

虛功與廣義力的關係是

\[\delta W = \sum_{j=1}^N\ \mathcal{F}_j\delta q_j\,\!\]。

稱\(\mathcal{F}_j\,\!\)為關於廣義坐標\(q_j\,\!\)的廣義力。因為\(\mathcal{F}_j q_j\,\!\)的[量綱是功](../Page/量綱.md "wikilink")，如果\(q_j\,\!\)是距離，則\(\mathcal{F}_j\,\!\)與力的量綱相同；如果\(q_j\,\!\)是角，則它與[力矩的量綱相同](../Page/力矩.md "wikilink")。

## 參閱

  - [拉格朗日力學](../Page/拉格朗日力學.md "wikilink")
  - [哈密頓力學](../Page/哈密頓力學.md "wikilink")
  - [自由度](../Page/自由度.md "wikilink")
  - [虛功](../Page/虛功_\(物理\).md "wikilink")

[G](../Category/力學.md "wikilink") [G](../Category/經典力學.md "wikilink")
[G](../Category/拉格朗日力學.md "wikilink")
[G](../Category/哈密頓力學.md "wikilink")