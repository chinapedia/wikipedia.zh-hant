**BSD小惡魔**（BSD
Daemon）是[BSD](../Page/BSD.md "wikilink")[作業系統的](../Page/作業系統.md "wikilink")[吉祥物](../Page/吉祥物.md "wikilink")。許多人叫他「Beastie」，因為發音跟BSD類似。他的形象是隻[惡魔](../Page/惡魔.md "wikilink")，通常帶支[三叉戟](../Page/三叉戟.md "wikilink")，代表[行程的](../Page/行程.md "wikilink")[分岔](../Page/分岔_\(電腦科學\).md "wikilink")。BSD小惡魔的[版權是由](../Page/版權.md "wikilink")[馬紹爾·克爾克·麥庫錫克持有](../Page/馬紹爾·克爾克·麥庫錫克.md "wikilink")。目前最流行的BSD小惡魔版本是由[動畫導演](../Page/動畫.md "wikilink")[约翰·雷斯特在](../Page/约翰·雷斯特.md "wikilink")1988年3月22日所畫的。而最早的BSD小惡魔版本則是由[漫畫](../Page/漫畫.md "wikilink")[畫家](../Page/畫家.md "wikilink")[費爾·弗格利歐畫的](../Page/費爾·弗格利歐.md "wikilink")。

## ASCII 图片

<div class="tright thumb">

<div class="thumbinner">

<span style="background:#000;font:14px bold;font-family:fixedsys,terminal,courier,monospace;display:block"><span style="color:#f00">`                ,        ,         `
``               /(        )`        ``
`               \ ___   / |        `
`               /- `<span style="color:#fff">`_`</span>``  `-/  '        ``
`              (`<span style="color:#fff">`/\/ \`</span>` \   /\        `
`              `<span style="color:#fff">`/ /   |`</span>`` `    \       ``
`              `<span style="color:#00f">`O O`</span>`   `<span style="color:#fff">`)`</span>` /    |       `
`              `<span style="color:#fff">`` `-^--' ``</span>`` `<     '        ``
`             (_.)  _  )   /        `
``              `.___/`    /         ``
``                `-----' /          ``
<span style="color:#ff0">`   <----.`</span>`     __ / __   \          `
<span style="color:#ff0">`   <----|====`</span>`O)))`<span style="color:#ff0">`==`</span>`) \) /`<span style="color:#ff0">`====`</span>`      `
<span style="color:#ff0">`   <----'`</span>``    `--' `.__,' \         ``
`                |        |         `
`                 \       /       /\`
<span style="color:#0ff">`            ______`</span>`( (_  / ______/`</span>` `
<span style="color:#0ff">`          ,'  ,-----'   |          `
``          `--{__________)          ``</span></span>

</div>

</div>

## 参见

  - [Puffy](../Page/OpenBSD.md "wikilink")
    —[OpenBSD的吉祥物](../Page/OpenBSD.md "wikilink")
  - [Tux](../Page/Tux.md "wikilink")
    —[Linux的吉祥物](../Page/Linux.md "wikilink")
  - [Hexley](../Page/Hexley.md "wikilink") —[Apple
    Darwin的吉祥物](../Page/Apple_Darwin.md "wikilink")

[Category:電腦吉祥物](../Category/電腦吉祥物.md "wikilink")
[Category:BSD](../Category/BSD.md "wikilink")