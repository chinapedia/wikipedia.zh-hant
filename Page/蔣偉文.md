**蔣緯承**（，)，原名**蔣偉文**，2008年改本名為**蔣緯承**，目前以原名蔣偉文作為藝名在演藝圈發展，出生於[台灣](../Page/台灣.md "wikilink")[台北市](../Page/台北市.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[男演員](../Page/男演員.md "wikilink")、[主持人](../Page/主持人.md "wikilink")。[美國](../Page/美國.md "wikilink")[南加州大學畢業](../Page/南加州大學.md "wikilink")。

## 經歷

蔣偉文出生於[台北市](../Page/台北市.md "wikilink")，祖父為[中華民國空軍](../Page/中華民國空軍.md "wikilink")[上校](../Page/上校.md "wikilink")。在[中國大陸被](../Page/中國大陸.md "wikilink")[中國共產黨統治前](../Page/中國共產黨.md "wikilink")，祖父帶全家成員來到台灣。5歲前在[南投縣成長](../Page/南投縣.md "wikilink")。國中一年級時，全家[移民](../Page/移民.md "wikilink")[美國](../Page/美國.md "wikilink")。為前男子團體「[三片吐司](../Page/三片吐司.md "wikilink")」成員之一，目前主要活躍於主持、演戲。與[小鐘](../Page/鐘昀呈.md "wikilink")、[屈中恆](../Page/屈中恆.md "wikilink")、[艾力克斯夫婦為好友](../Page/艾力克斯_\(藝人\).md "wikilink")。

從單身變成人夫，到升格當爸爸，變成三人家庭中的一家之主。蔣偉文事業也轉成以美食和烹飪為重心。曾經在TVBS的美食節目《[吃飯皇帝大](../Page/吃飯皇帝大.md "wikilink")》中，以幽默逗趣的「幸福料理鐵人」小粉紅腳色贏得許多婆婆媽媽及人妻的支持喜愛。代言過許多知名商品，受廠商歡迎。由於常和許多知名大廚合作，廚藝日益精進。目前為[中廣流行網節目](../Page/中廣流行網.md "wikilink")《蔣公廚房》的主持人。蔣另外也擅長繪畫。著有《美味．好味．幸福味：蔣偉文's手繪廚房》、《新手下廚101道不失敗料理：30年廚藝大廚親授美味料理＋312個好吃烹調秘訣》、《111道小家庭幸福料理》、《蔣偉文的幸福廚記》等書。

## 主持

  - 電視

| 年份 | 播出頻道                                                             | 節目                                                                         | 備註 |
| -- | ---------------------------------------------------------------- | -------------------------------------------------------------------------- | -- |
|    | [台灣電視公司](../Page/台灣電視公司.md "wikilink")                           | 《[大野遊龍](../Page/大野遊龍.md "wikilink")》                                       |    |
|    | 《[爆米花週記](../Page/爆米花週記.md "wikilink")》                           |                                                                            |    |
|    | 《[青春樂園 Young Young 紅](../Page/青春樂園_Young_Young_紅.md "wikilink")》 |                                                                            |    |
|    | [中國電視公司](../Page/中國電視公司.md "wikilink")                           | 《[MIT台灣誌](../Page/MIT台灣誌.md "wikilink")》                                   |    |
|    | 《[走在台灣的脊樑上](../Page/走在台灣的脊樑上.md "wikilink")》                     |                                                                            |    |
|    | 《[魅力台灣樂活誌](../Page/魅力台灣樂活誌.md "wikilink")》                       |                                                                            |    |
|    | 《[筆記台灣](../Page/筆記台灣.md "wikilink")》                             | 僅主持外景                                                                      |    |
|    | 《[青春異想廚房](../Page/青春異想廚房.md "wikilink")》                         |                                                                            |    |
|    | [超級電視台](../Page/超級電視台.md "wikilink")                             | 《[九點一級棒](../Page/九點一級棒.md "wikilink")》                                     |    |
|    | 《[野蠻遊戲](../Page/野蠻遊戲_\(電視節目\).md "wikilink")》                    |                                                                            |    |
|    | [三立都會台](../Page/三立都會台.md "wikilink")                             | 《[十分都會](../Page/十分都會.md "wikilink")》                                       |    |
|    | 《[超級挑戰者](../Page/超級挑戰者.md "wikilink")》                           |                                                                            |    |
|    | [中天娛樂台](../Page/中天娛樂台.md "wikilink")                             | 《[電玩暴走](../Page/電玩暴走.md "wikilink")》                                       |    |
|    | 《[麻辣體驗營](../Page/麻辣體驗營.md "wikilink")》                           |                                                                            |    |
|    | [東森綜合台](../Page/東森綜合台.md "wikilink")                             | 《[幸福好家在](../Page/幸福好家在.md "wikilink")》                                     |    |
|    | [東風衛視](../Page/東風衛視.md "wikilink")                               | 《[明星遊戲王](../Page/明星遊戲王.md "wikilink")》                                     |    |
|    | 《[ALL IN 俱樂部](../Page/ALL_IN_俱樂部.md "wikilink")》                 |                                                                            |    |
|    | [霹靂衛星電視台](../Page/霹靂衛星電視台.md "wikilink")                         | 《[電玩帝國](../Page/電玩帝國.md "wikilink")》                                       |    |
|    | [新傳媒電視](../Page/新傳媒電視.md "wikilink")                             | 《[吃遍天下](../Page/吃遍天下.md "wikilink")》                                       |    |
|    | [TVBS歡樂臺](../Page/TVBS歡樂臺.md "wikilink")                         | 《第53屆[亞太影展](../Page/亞太影展.md "wikilink")[星光大道](../Page/星光大道.md "wikilink")》 |    |
|    | [TVBS歡樂臺](../Page/TVBS歡樂臺.md "wikilink")                         | 《[吃飯皇帝大](../Page/吃飯皇帝大.md "wikilink")》                                     |    |
|    | [大愛電視](../Page/大愛電視.md "wikilink")                               | 《在台灣站起》                                                                    |    |

  - 廣播

| 年份                                    | 播出頻道                                   | 節目               | 備註 |
| ------------------------------------- | -------------------------------------- | ---------------- | -- |
|                                       | [環宇廣播電台](../Page/環宇廣播電台.md "wikilink") | 《Uni Jacko Show》 |    |
| |[中廣流行網](../Page/中廣流行網.md "wikilink") | 《蔣公廚房》                                 |                  |    |

## 影視作品

### 電視劇

| 年份    | 播出頻道                                                                | 劇名                                                                      | 飾演         |
| ----- | ------------------------------------------------------------------- | ----------------------------------------------------------------------- | ---------- |
| 2001年 | 華視                                                                  | [醜女大翻身](../Page/醜女大翻身.md "wikilink")                                    |            |
| 2002年 | 台視                                                                  | [搖滾莎士](../Page/搖滾莎士.md "wikilink")                                      | 士 強        |
| 2003年 | [東森綜合台](../Page/東森綜合台.md "wikilink")                                | [老婆大人](../Page/老婆大人_\(台灣\).md "wikilink")                               |            |
| 2003年 | 華視                                                                  | [無河天](../Page/無河天.md "wikilink")                                        |            |
| 2004年 | 東森綜合台                                                               | [婆媳過招千百回](../Page/婆媳過招千百回.md "wikilink")                                |            |
| 2005年 | [東森綜合台](../Page/東森綜合台.md "wikilink")                                | [艾曼紐要怎樣](../Page/艾曼紐要怎樣.md "wikilink")                                  |            |
| 2006年 | 緯來綜合台                                                               | [Q狼特勤組](../Page/Q狼特勤組.md "wikilink")                                    |            |
| 2006年 | 東森綜合台                                                               | [愛情機器](../Page/愛情機器.md "wikilink")                                      |            |
| 2006年 | [衛視中文台](../Page/衛視中文台.md "wikilink")                                | [驚異傳奇之](../Page/驚異傳奇.md "wikilink")[惡魔茱麗葉](../Page/惡魔茱麗葉.md "wikilink") | 陳浩         |
| 2006年 | [緯來綜合台](../Page/緯來綜合台.md "wikilink")                                | [我的兒子是老大](../Page/我的兒子是老大.md "wikilink")                                |            |
| 2006年 | [台視](../Page/台視.md "wikilink")                                      | [愛情經紀約](../Page/愛情經紀約.md "wikilink")                                    |            |
| 2006年 | [華視](../Page/華視.md "wikilink")                                      | [剪刀石頭布](../Page/剪刀石頭布_\(電視劇\).md "wikilink")                            |            |
| 2007年 | [華視](../Page/華視.md "wikilink")                                      | [親親小爸](../Page/親親小爸.md "wikilink")                                      | 大衛         |
| 2007年 | [民視](../Page/民視.md "wikilink")                                      | [愛情兩好三壞](../Page/愛情兩好三壞.md "wikilink")                                  | 魚博士        |
| 2008年 | [中視](../Page/中視.md "wikilink")                                      | [籃球火](../Page/籃球火.md "wikilink")                                        | 紀恨 (特別演出)  |
| 2008年 | [台視](../Page/台視.md "wikilink")                                      | [幸福捕手](../Page/幸福捕手.md "wikilink")                                      | 財哥         |
| 2009年 | [大愛電視](../Page/大愛電視.md "wikilink")                                  | [芳草碧連天](../Page/芳草碧連天_\(電視劇\).md "wikilink")                            | 胡大石        |
| 2012年 | [大愛電視](../Page/大愛電視.md "wikilink")                                  | [團圓飯](../Page/團圓飯_\(大愛劇場\).md "wikilink")                               | 孟慶進(蔣彤)    |
| 2013年 | [台視](../Page/台視.md "wikilink")                                      | [結婚好嗎？](../Page/結婚好嗎？.md "wikilink")                                    | 李皓權        |
| 2013年 | [公視](../Page/公視.md "wikilink")                                      | [沒有名字的甜點店](../Page/沒有名字的甜點店.md "wikilink")                              | 秦野         |
| 2013年 | [台視](../Page/台視.md "wikilink")                                      | [大紅帽與小野狼](../Page/大紅帽與小野狼.md "wikilink")                                | David (客串) |
| 2014年 | [TVBS歡樂台](../Page/TVBS歡樂台.md "wikilink")                            | [A咖的路](../Page/A咖的路.md "wikilink")                                      | 主持人 (客串)   |
| 2014年 | [三立都會台](../Page/三立都會台.md "wikilink")                                | [喜歡·一個人](../Page/喜歡·一個人.md "wikilink")                                  | Enzo (客串)  |
| 2014年 | [大愛電視](../Page/大愛電視.md "wikilink")                                  | [又見春風化雨](../Page/又見春風化雨.md "wikilink")                                  | 張經昆        |
| 2016年 | [三立都會台](../Page/三立都會台.md "wikilink")                                | [1989一念間](../Page/1989一念間.md "wikilink")                                | 簡振華        |
| 2016年 | [三立都會台](../Page/三立都會台.md "wikilink")                                | [獨家保鑣](../Page/獨家保鑣.md "wikilink")                                      | 賀成章        |
| 2018年 | [民視](../Page/民視.md "wikilink")                                      | [實習醫師鬥格](../Page/實習醫師鬥格.md "wikilink")                                  | 陳志明        |
|       | |[幸福學堂:戀愛講義](../Page/幸福學堂:戀愛講義.md "wikilink")                       |                                                                         |            |
|       | |[青春記事簿](../Page/青春記事簿.md "wikilink")                               |                                                                         |            |
|       | |[我的右腿](../Page/我的右腿.md "wikilink")                                 |                                                                         |            |
| 2018年 | [TVBS歡樂台](../Page/TVBS歡樂台.md "wikilink")                            | [初戀的情人](../Page/初戀的情人.md "wikilink")                                    | 劉勇進        |
| 2019年 | [華視](../Page/華視.md "wikilink")、[中天綜合台](../Page/中天綜合台.md "wikilink") | [最佳利益](../Page/最佳利益.md "wikilink")                                      | 李安邦        |
|       |                                                                     |                                                                         |            |

### 電影

| 年份                                              | 片名 | 飾演 |
| ----------------------------------------------- | -- | -- |
| |《[國道封閉](../Page/國道封閉.md "wikilink")》           |    |    |
| |《[報告班長](../Page/報告班長.md "wikilink")4：拂曉-{出}-擊》 |    |    |
| |《[激爆21點](../Page/激爆21點.md "wikilink")》         |    |    |
| |《[鈕扣人](../Page/鈕扣人.md "wikilink")》             |    |    |
| |《[驚天動地](../Page/驚天動地.md "wikilink")》           |    |    |
| |《[地下秩序](../Page/地下秩序.md "wikilink")》           |    |    |
| |《[指間的重量](../Page/指間的重量.md "wikilink")》         |    |    |
| |《[招魂](../Page/招魂.md "wikilink")》 (新加坡)         |    |    |

## 唱片

  - 三片吐司《[早餐要吃](../Page/早餐要吃.md "wikilink")》

## 著作

| 年份       | 書名                                                                            | 出版社    | ISBN               |
| -------- | ----------------------------------------------------------------------------- | ------ | ------------------ |
| 2011年9月  | 《美味．好味．幸福味：蔣偉文's手繪廚房》                                                         | 幸福文化   | ISBN 9789868712232 |
| 2012年11月 | 《新手下廚101道不失敗料理：30年廚藝大廚親授美味料理+312個好吃烹調秘訣》（與[梁振業合著](../Page/梁振業.md "wikilink")） | 幸福文化   | ISBN 9789868889606 |
| 2014年1月  | 《111道小家庭幸福料理：給上班族、新手爸媽與小家庭的美味食譜》（與[溫國智合著](../Page/溫國智.md "wikilink")）         | 日日幸福事業 | ISBN 9789868969148 |
| 2014年9月  | 《蔣偉文的幸福廚記：72道超人氣家常料理，享受美味好食光》                                                 | 橘子     | ISBN 9789863640189 |
| 2015年11月 | 《蔣偉文幸福全家餐：快速又營養的美味食譜，讓吃飯變成親子最快樂的時光！》                                          | 日日幸福   | ISBN 9789869207812 |

## 寫真

  - 《Deja vu蔣偉文似曾相識IN NEW
    YORK》：[杜達雄攝影](../Page/杜達雄.md "wikilink")，也渡視工坊2002年11月出版
  - 《陽光．沙灘の塞班島》：[唐林](../Page/唐林.md "wikilink")、蔣偉文[塞班島攝影集](../Page/塞班島.md "wikilink")，艾迪昇傳播出版

## 廣告

  - [金車公司](../Page/金車公司.md "wikilink")「波爾[礦泉水](../Page/礦泉水.md "wikilink")」
  - [寶僑家品](../Page/寶潔公司.md "wikilink")「歐蕾潤膚沐浴乳」（小阿姨篇）
  - [聯合利華](../Page/聯合利華.md "wikilink")「麗仕[洗髮乳](../Page/洗髮乳.md "wikilink")」
  - [花旗（台灣）商業銀行](../Page/花旗（台灣）商業銀行.md "wikilink")「花旗[信用卡](../Page/信用卡.md "wikilink")」

## 参考资料

## 外部連結

  - [Jacko 蔣偉文 | 蔣公廚房](http://www.jacko.com.tw/)

  -
  -
  -
  -
  -
  -
[J蔣](../Category/臺灣電影男演員.md "wikilink")
[J蔣](../Category/臺灣電視男演員.md "wikilink")
[J蔣](../Category/台灣綜藝節目主持人.md "wikilink")
[W](../Category/蔣姓.md "wikilink")
[C](../Category/南加州大學校友.md "wikilink")
[Category:台北人](../Category/台北人.md "wikilink")