**香港語言學學會粵語拼音方案**，簡稱**粵拼**（[粵拼](../Page/粵拼.md "wikilink")：[jyut6
ping3](http://yue.forvo.com/word/%E7%B2%B5%E6%8B%BC/#yue)，粤拼：Jyutping），是由[香港語言學學會於](../Page/香港語言學學會.md "wikilink")1993年製訂的[粵語羅馬化拼音方案](../Page/粵語.md "wikilink")。其製訂的目的在於以一套簡單、合理、易學、易用的粵語語音轉寫方案來統一社會各界在粵語[拼音使用上的混亂情況](../Page/拼音.md "wikilink")。

## 認同和應用

這套方案目前得到了[香港教育](../Page/香港教育.md "wikilink")、[電腦](../Page/電腦.md "wikilink")[中文](../Page/中文.md "wikilink")[資訊處理等多方面的支持](../Page/資訊.md "wikilink")。[基於粵拼的拼音輸入法也發展得相當成熟](../Page/粵語拼音輸入法.md "wikilink")。除了[香港](../Page/香港.md "wikilink")，[台灣和](../Page/台灣.md "wikilink")[日本也有使用此方案的輸入系統出售](../Page/日本.md "wikilink")。方案得到政府、商界、學界等廣泛認同。\[1\]

  - 香港特區政府的中文界面諮詢委員會從1999年成立開始便一直以這方案為其工作上唯一粵音拼式。\[2\]
  - 《商務新詞典》的2009版以這方案為粵音注音系統。\[3\]
  - [Windows Vista](../Page/Windows_Vista.md "wikilink")
    將「粵拼」預設為其中一種輸入法。\[4\]
  - [香港中文大學人文電算研究中心的](../Page/香港中文大學.md "wikilink")《粵語審音配詞字庫》\[5\]、《粵語音韻集成》\[6\]及《黃錫淩粵音韻彙電子版》\[7\]都是以這方案為預設的注音系統。
  - [香港中文大學中國語言及文學系的](../Page/香港中文大學.md "wikilink")《現代標準漢語與粵語對照資料庫》是以這方案為注音系統。\[8\]
  - [香港中文大學自學中心的粵語學習工作坊](../Page/香港中文大學.md "wikilink")，是以這方案為注音系統。\[9\]
  - [香港大學中文學院漢語中心](../Page/香港大學中文學院.md "wikilink")，是以這方案為注音系統教授粵語發音。\[10\]
  - [香港大學教育學院非華語學生中文學習支援計劃](../Page/香港大學.md "wikilink")，是以這方案為注音系統教導非華語學生學習粵語。\[11\]
  - [香港大學中文學院漢語中心粵語組講師劉擇明發起的](../Page/香港大學中文學院.md "wikilink")「《粵典》計畫」，是以這方案為注音系統。\[12\]\[13\]
  - [香港理工大學中文及雙語學系的網上漢字輸入系統](../Page/香港理工大學.md "wikilink")，是以這方案為注音系統。\[14\]
  - [香港城市大學翻譯及語言學系設立](../Page/香港城市大學.md "wikilink")「粵語拼音資源站」以推廣這方案。\[15\]並推行這方案以糾正懶音。\[16\]
  - 香港教育學院（今[香港教育大學](../Page/香港教育大學.md "wikilink")）語言學及現代語言系的《香港二十世紀中期粵語語料庫》是以這方案為注音系統。\[17\]

## 字母表

粵拼的拼音字母是由[拉丁字母](../Page/拉丁字母.md "wikilink") a 至 z 組成，其中 q、r、v、x
不用。字調則以[阿拉伯數字](../Page/阿拉伯數字.md "wikilink") 1 至 6 標示。

### 聲母

共19個，如下表所示：

<table>
<caption>style="font-size:105%"|<strong>粵拼聲母表</strong></caption>
<thead>
<tr class="header">
<th></th>
<th><p><a href="../Page/雙脣音.md" title="wikilink">雙脣音</a>、<a href="../Page/脣齒音.md" title="wikilink">脣齒音</a></p></th>
<th><p><a href="../Page/舌尖音.md" title="wikilink">舌尖音</a></p></th>
<th><p><a href="../Page/軟顎音.md" title="wikilink">軟顎音</a>、<a href="../Page/聲門音.md" title="wikilink">聲門音</a></p></th>
<th><p><a href="../Page/脣化.md" title="wikilink">脣化軟顎音</a></p></th>
<th><p><a href="../Page/噝音.md" title="wikilink">噝音</a>、<a href="../Page/硬顎音.md" title="wikilink">硬顎音</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>粵拼聲母</p></td>
<td><p>b</p></td>
<td><p>p</p></td>
<td><p>m</p></td>
<td><p>f</p></td>
<td><p>d</p></td>
</tr>
<tr class="even">
<td><p>國際音標</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>漢字舉例</p></td>
<td><p>巴<br />
baa</p></td>
<td><p>怕<br />
paa</p></td>
<td><p>媽<br />
maa</p></td>
<td><p>花<br />
faa</p></td>
<td><p>打<br />
daa</p></td>
</tr>
</tbody>
</table>

### 韻母

#### 韻腹

共9個，如下表所示：

<table>
<caption>style="font-size:105%"|<strong>粵拼韻腹表</strong></caption>
<thead>
<tr class="header">
<th></th>
<th><p><a href="../Page/不圓唇.md" title="wikilink">不圓唇</a><a href="../Page/韻腹.md" title="wikilink">韻腹</a></p></th>
<th><p><a href="../Page/圓唇.md" title="wikilink">圓唇</a><a href="../Page/韻腹.md" title="wikilink">韻腹</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>粵拼韻腹</p></td>
<td><p>aa</p></td>
<td><p>a</p></td>
</tr>
<tr class="odd">
<td><p>國際音標</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>漢字舉例</p></td>
<td><p>沙 saa</p></td>
<td><p>新 san</p></td>
</tr>
</tbody>
</table>

#### 韻尾

共8個，如下表所示：

<table>
<caption>style="font-size:105%"|<strong>粵拼韻尾表</strong></caption>
<thead>
<tr class="header">
<th></th>
<th><p><a href="../Page/元音.md" title="wikilink">元音</a><a href="../Page/韻尾.md" title="wikilink">韻尾</a></p></th>
<th><p><a href="../Page/鼻音_(辅音).md" title="wikilink">鼻音</a><a href="../Page/韻尾.md" title="wikilink">韻尾</a></p></th>
<th><p><a href="../Page/塞音.md" title="wikilink">塞音</a><a href="../Page/韻尾.md" title="wikilink">韻尾</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>粵拼韻尾</p></td>
<td><p>-i</p></td>
<td><p>-u</p></td>
<td><p>-m</p></td>
</tr>
<tr class="even">
<td><p>國際音標</p></td>
<td><p>/</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>漢字舉例</p></td>
<td><p>西 sai<br />
需 seoi</p></td>
<td><p>收 sau</p></td>
<td><p>心 sam</p></td>
</tr>
</tbody>
</table>

#### 韻母表

**韻腹**加**韻尾**組成粵拼的**韻母**，共56個\[18\]：

（方括號內為國際音標，所有例字均只取其韻母）

<table>
<caption>style="font-size:105%"|<strong>韻母表</strong></caption>
<thead>
<tr class="header">
<th><p>行列</p></th>
<th><p>-</p></th>
<th><p>i 列</p></th>
<th><p>u 列</p></th>
<th><p>m 列</p></th>
<th><p>n 列</p></th>
<th><p>ng 列</p></th>
<th><p>p 列</p></th>
<th><p>t 列</p></th>
<th><p>k 列</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>aa 行</p></td>
<td><p>aa 渣<br />
</p></td>
<td><p>aai 齋<br />
</p></td>
<td><p>aau 嘲<br />
</p></td>
<td><p>aam 站<br />
</p></td>
<td><p>aan 讚<br />
</p></td>
<td><p>aang 掙<br />
</p></td>
<td><p>aap 集<br />
</p></td>
<td><p>aat 扎<br />
</p></td>
<td><p>aak 責<br />
</p></td>
</tr>
<tr class="even">
<td><p>a 行</p></td>
<td><p>-</p></td>
<td><p>ai 擠<br />
</p></td>
<td><p>au 周<br />
</p></td>
<td><p>am 斟<br />
</p></td>
<td><p>an 珍<br />
</p></td>
<td><p>ang 增<br />
</p></td>
<td><p>ap 汁<br />
</p></td>
<td><p>at 姪<br />
</p></td>
<td><p>ak 則<br />
</p></td>
</tr>
<tr class="odd">
<td><p>e 行</p></td>
<td><p>e 些<br />
</p></td>
<td><p>ei 四<br />
</p></td>
<td><p>eu 掉*<br />
</p></td>
<td><p>em 舐*<br />
</p></td>
<td><p>-</p></td>
<td><p>eng 鄭<br />
</p></td>
<td><p>ep 夾*<br />
</p></td>
<td><p>-</p></td>
<td><p>ek 石<br />
</p></td>
</tr>
<tr class="even">
<td><p>i 行</p></td>
<td><p>i 思<br />
</p></td>
<td><p>-</p></td>
<td><p>iu 消<br />
</p></td>
<td><p>im 閃<br />
</p></td>
<td><p>in 先<br />
</p></td>
<td><p>ing 升<br />
</p></td>
<td><p>ip 攝<br />
</p></td>
<td><p>it 泄<br />
</p></td>
<td><p>ik 識<br />
</p></td>
</tr>
<tr class="odd">
<td><p>o 行</p></td>
<td><p>o 可<br />
</p></td>
<td><p>oi 開<br />
</p></td>
<td><p>ou 好<br />
</p></td>
<td><p>-<br />
</p></td>
<td><p>on 看<br />
</p></td>
<td><p>ong 康<br />
</p></td>
<td><p>-<br />
</p></td>
<td><p>ot 喝<br />
</p></td>
<td><p>ok 學<br />
</p></td>
</tr>
<tr class="even">
<td><p>u 行</p></td>
<td><p>u 夫<br />
</p></td>
<td><p>ui 灰<br />
</p></td>
<td><p>-</p></td>
<td><p>|-</p></td>
<td><p>un 歡<br />
</p></td>
<td><p>ung 風<br />
</p></td>
<td><p>-</p></td>
<td><p>ut 闊<br />
</p></td>
<td><p>uk 福<br />
</p></td>
</tr>
<tr class="odd">
<td><p>oe 行</p></td>
<td><p>oe 靴<br />
</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>oeng 雙<br />
</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>oek 腳<br />
</p></td>
</tr>
<tr class="even">
<td><p>eo 行</p></td>
<td><p>-</p></td>
<td><p>eoi 需<br />
</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>eon 詢<br />
</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>eot摔<br />
</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p>yu 行</p></td>
<td><p>yu 書<br />
</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>yun 孫<br />
</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>yut 雪<br />
</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p>鼻音韻</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>m 唔<br />
</p></td>
<td><p>-</p></td>
<td><p>ng 吳<br />
</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
</tbody>
</table>

  - 掉、舐、夾為口頭語音，不存在於傳統讀書音中。

### 鼻音獨立韻

共兩個，為：m（唔）、ng（吳、五）（可寫作ŋ）

## 聲調

聲調以阿拉伯數字 1 至 6
標示在每個音節的拼音之後，聲調的數字**不需**以[上標標示](../Page/上標.md "wikilink")：

| 聲調   | 陰平       | 陰上       | 陰去       | 陽平       | 陽上       | 陽去       | 高陰入      | 低陰入       | 陽入       |
| ---- | -------- | -------- | -------- | -------- | -------- | -------- | -------- | --------- | -------- |
| 調值   | 55/53    | 35       | 33       | 21/11    | 23       | 22       | 5        | 3         | 2        |
| 漢字舉例 | 分        | 粉        | 訓        | 焚        | 奮        | 份        | 忽        | 發         | 佛        |
| 代表數字 | 1        | 2        | 3        | 4        | 5        | 6        | 1        | 3         | 6        |
| 拼音   | fan**1** | fan**2** | fan**3** | fan**4** | fan**5** | fan**6** | fat**1** | faat**3** | fat**6** |

style="font-size:105%"|**聲調表**

實際上高陰入、低陰入、陽入聲調的音高，與陰平、陰去、陽去是一樣的，不過是用-p、-t、-k
韻尾的入聲字用以區分。由於[聲調的定義](../Page/聲調.md "wikilink")，是包括抑揚性(即實際音高)和頓挫性。而入聲韻尾-p、-t、-k正是影響了其頓挫性。因此，即使只以1至6標示，我們仍然要說是有九個聲調，或者說有「九聲六調」，不能稱作只有六個聲調。

值得一提的是：粵語中的九聲，正好與[數字中的](../Page/數字.md "wikilink")「[零](../Page/零.md "wikilink")」至「[九](../Page/九.md "wikilink")」（除「[一](../Page/一.md "wikilink")」和「[七](../Page/七.md "wikilink")」同屬高陰入聲外）對應，若依次序由「陰平、陰上、陰去、陽平、陽上、陽去、高陰入、低陰入
、陽入」排列的話，順序就是「[三](../Page/三.md "wikilink")、[九](../Page/九.md "wikilink")、[四](../Page/四.md "wikilink")、[零](../Page/零.md "wikilink")、[五](../Page/五.md "wikilink")、[二](../Page/二.md "wikilink")、[七](../Page/七.md "wikilink")、[八](../Page/八.md "wikilink")、[六](../Page/六.md "wikilink")」；因著粵音存在這個特質，以至能夠造就一些直接填入數字作為歌詞的經典創作，例子有[林子祥的](../Page/林子祥.md "wikilink")《數字人生》。

## 拼寫舉例

|                                                                                     |                                                |
| ----------------------------------------------------------------------------------- | ---------------------------------------------- |
| <big>**[春曉](../Page/春曉.md "wikilink")**</big>　　**[孟浩然](../Page/孟浩然.md "wikilink")** | <big>**Ceon1 Hiu2**</big>　　**Maang6 Hou6jin4** |
| <big>春眠不覺曉，</big>                                                                   | <big>Ceon1 min4 bat1 gok3 hiu2,</big>          |
| <big>處處聞啼鳥。</big>                                                                   | <big>Cyu3 cyu3 man4 tai4 niu5.</big>           |
| <big>夜來風雨聲，</big>                                                                   | <big>Je6 loi4 fung1 jyu5 sing1,</big>          |
| <big>花落知多少？</big>                                                                   | <big>Faa1 lok6 zi1 do1 siu2?</big>             |

## 與[教院式拼音之差別](../Page/教育學院拼音方案.md "wikilink")

| 粵拼  | 例       | [教院](../Page/教育學院拼音方案.md "wikilink") | 例                  |
| --- | ------- | ------------------------------------ | ------------------ |
| yu  | 魚 jyu4  | y                                    | 魚 jy<sup>4</sup>   |
| yun | 圓 jyun4 | yn                                   | 圓 jyn<sup>4</sup>  |
| yut | 粵 jyut6 | yt                                   | 粵 jyt<sup>9</sup>  |
| eon | 詢 seon1 | oen                                  | 詢 soen<sup>1</sup> |
| eot | 摔 seot1 | oet                                  | 摔 soet<sup>7</sup> |
| eoi | 需 seoi1 | oey                                  | 需 soey<sup>1</sup> |
| z   | 制 zai3  | dz                                   | 制 dzai<sup>3</sup> |
| c   | 菜 coi3  | ts                                   | 菜 tsoi<sup>3</sup> |

還有，高陰入、低陰入、陽入三個聲調，教院式順序標記作第7、8、9聲調，粵拼標記作1、3、6聲調(只限入聲)。

## 參考文獻

## 外部連結

  - [粵拼](https://www.lshk.org/jyutping) 香港語言學學會
  - [粵拼網上教室](http://www.iso10646hk.net/jp/learning/index.jsp) 香港理工大學
  - [粵語拼音資源站](http://jyutping.lt.cityu.edu.hk/) 香港城市大學
  - [香港語言學學會粵拼詞表](http://corpus.ied.edu.hk/JPwordlist/index.php)
  - [粵語音節表](http://humanum.arts.cuhk.edu.hk/Lexis/lexi-mf/syllables.php)
    漢語多功能字庫
  - [粤语学习网(学说广东话)](http://www.fyan8.com/yuepin.htm)

[Category:粵語拼音](../Category/粵語拼音.md "wikilink")
[Category:香港粵語](../Category/香港粵語.md "wikilink")
[Category:粵拼使用者](../Category/粵拼使用者.md "wikilink")

1.  {{ cite web
    |url=<https://life.mingpao.com/chi/article?issue=20100422&nodeid=1507946278273>
    |title=粵語的粵拼和IPA |date=2010-04-22 |publisher=明報 |author= }}

2.

3.
4.
5.  {{ cite web |url=<http://humanum.arts.cuhk.edu.hk/Lexis/lexi-can/>
    |title=粵語審音配詞字庫 |date= |publisher=人文電算研究中心|author= }}

6.  {{ cite web |url=<http://humanum.arts.cuhk.edu.hk/Lexis/Canton2/>
    |title=粵語音韻集成 |date= |publisher=人文電算研究中心|author= }}

7.  {{ cite web |url=<http://humanum.arts.cuhk.edu.hk/Lexis/Canton/>
    |title=黃錫淩粵音韻彙電子版 |date= |publisher=人文電算研究中心|author= }}

8.  {{ cite web
    |url=<http://apps.itsc.cuhk.edu.hk/hanyu/Page/Cover.aspx>
    |title=現代標準漢語與粵語對照資料庫 |date= |publisher=香港中文大學中國語言及文學系
    |author= }}

9.  {{ cite web
    |url=<https://www.ilc.cuhk.edu.hk/chinese/downloads/Cantonese_make_easy.pdf>
    |title=輕輕鬆鬆學粵語 |date= |publisher=香港中文大學自學中心 |author= }}

10. {{ cite web
    |url=<http://web.chinese.hku.hk/20160709CantoSounds/resources.pdf>
    |title=CantoSounds 簡介會及工作坊 |date= |publisher=香港大學中文學院漢語中心 |author=
    }}

11. {{ cite web
    |url=<http://www.cacler.hku.hk/en/research/project/provision-of-services-2016-18-for-running-of-chinese-language-learning-support-centres-for-non-chinese-speaking-ncs-students/teachers-workshops/ssp-2016-18-workshop7-powerpoint>
    |title=如何利用粵語拼音教導非華語學生學習基礎粵語 |date= |publisher=香港大學教育學院 |author=戴忠沛
    }}

12. {{ cite news
    |url=<https://theinitium.com/article/20161130-hongkong-cantonesedictionary/>
    |title=490個義工合力編著粵語辭典，保存本土語言 |date=2016-11-30
    |publisher=端傳媒|author=黃浚煒}}

13. {{ cite web |url=<https://words.hk/> |title=《粵典》 |date= |publisher=
    |author=劉擇明 }}

14. {{ cite web
    |url=<http://www.cbs.polyu.edu.hk/ctxzhang/ab/ab2007b/userguide.pdf>
    |title=全衡2007 |date= |publisher=香港理工大學中文及雙語學系 |author=張小衡 }}

15. {{ cite web |url=<http://jyutping.lt.cityu.edu.hk/> |title=粵語拼音資源站
    |date= |publisher=香港城市大學\]\]翻譯及語言學系 |author= }}

16. {{ cite news
    |url=<https://hk.news.appledaily.com/local/daily/article/20051009/5294356>
    |title=城大推粵語標準糾正懶音 |date=2005-10-09 |publisher=蘋果日報 |author= }}

17. {{ cite web |url=<http://corpus.ied.edu.hk/hkcc/introduction.html>
    |title=香港二十世紀中期粵語語料庫 |date=2012-03-17 |publisher=香港教育學院語言學及現代語言系
    |author= }}

18. <http://humanum.arts.cuhk.edu.hk/Lexis/lexi-can/just.php>