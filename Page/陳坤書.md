**陳坤書**（），清[广西](../Page/广西.md "wikilink")[桂平人](../Page/桂平.md "wikilink")，參與[金田起義](../Page/金田起義.md "wikilink")，封「护王」，[太平天国後期著名将领](../Page/太平天国.md "wikilink")。眼有斜疾，绰号“陈斜眼”。

## 生平

### 初期

1854年被派往西征前線，駐守安徽[巢縣](../Page/巢縣.md "wikilink")，任殿前功曹副侍卫。1858年秋，又随[李秀成在安徽](../Page/李秀成.md "wikilink")[肥西县三河镇一带全歼入皖](../Page/肥西县.md "wikilink")[湘军](../Page/湘军.md "wikilink")[李續賓部](../Page/李續賓.md "wikilink")6000余众。

1860年1月28日至5月6日，陈坤书随忠王[李秀成二破](../Page/李秀成.md "wikilink")[江南大营](../Page/江南大营.md "wikilink")。接着东向討伐，开辟苏南连串战斗，于5月26日占[常州](../Page/常州.md "wikilink")，30日夺[无锡](../Page/无锡.md "wikilink")，6月2日下[苏州](../Page/苏州.md "wikilink")，13日，取[吴江](../Page/吴江.md "wikilink")、震泽，15日克[嘉兴](../Page/嘉兴.md "wikilink")，势如破竹。同年10月，陈坤书被派守[苏州](../Page/苏州.md "wikilink")。

1862年春，陈坤书移驻常州。10月，奉令攻打[金柱关](../Page/金柱关.md "wikilink")，截断[芜湖清军之援](../Page/芜湖.md "wikilink")。1863年春，随忠王跨江参加进北攻南战役圖救天京，又多次转战天京、[无锡](../Page/无锡.md "wikilink")，圖力保[天京](../Page/天京.md "wikilink")、苏州。

1861年11月曾悬出巨赏，榜示：“陈坤书为江、浙之巨魁，如城破之时，无论军民伪官有能生擒来献者，赏银五万两，得首级来献者，赏银二万两。”

1863年秋，李秀成於九月十六日、二十日两次下紧急命令派[陈坤书出兵救](../Page/陈坤书.md "wikilink")[蘇州](../Page/蘇州.md "wikilink")，並不聽命已藏降志；至[蘇州失守](../Page/蘇州.md "wikilink")，陳見[李鴻章殺降](../Page/李鴻章.md "wikilink")，方拼命守[常州](../Page/常州.md "wikilink")。

### 常州攻防戰

1863年12月25日，时任清廷[江苏巡撫的](../Page/江苏巡撫.md "wikilink")[李鸿章率](../Page/李鸿章.md "wikilink")[张樹聲](../Page/张樹聲.md "wikilink")、[刘铭传](../Page/刘铭传.md "wikilink")、[周盛波十万余众淮军兵临常州](../Page/周盛波.md "wikilink")；以[白銀五万](../Page/白銀.md "wikilink")[两雇](../Page/两银.md "wikilink")[英國人](../Page/英國人.md "wikilink")[查理·戈登](../Page/查理·喬治·戈登.md "wikilink")“[常胜军](../Page/常胜军.md "wikilink")”3000多人，联攻常州。

1864年4月25日，常州外城[金坛失守](../Page/金坛.md "wikilink")，兄长志王[陳志書中炮](../Page/陳志書.md "wikilink")[陣亡](../Page/陣亡.md "wikilink")；陈坤书率领军奮戰抗敵，歼敌3000余众，终于将[淮军再次击退](../Page/淮军.md "wikilink")。5月11日，[淮軍英軍水陆并进](../Page/淮軍.md "wikilink")，对[常州城进行猛烈炮轰](../Page/常州.md "wikilink")，城墙全轰塌，[淮军拥入城門內巷戰](../Page/淮军.md "wikilink")。

《[武阳志余](../Page/武阳志余.md "wikilink")》载：“悍太平軍以身塞缺，犹旋死旋集不少……。太平軍倾火药以长矛格刺，军士十坠六七不顾，卒拥而登城头，久之始败太平軍下城。”

今常州局前街187号為护王府，陳在此被[淮軍](../Page/淮軍.md "wikilink")[副將](../Page/副將.md "wikilink")[龔生陽擄](../Page/龔生陽.md "wikilink")，數日後刑前言：「……我欲保常州，为金陵犄角，奈事不成，只有尽忠。”不降，被[凌遲處死](../Page/凌遲.md "wikilink")。

[常州於](../Page/常州.md "wikilink")1860年四月初六日為[太平軍佔領](../Page/太平軍.md "wikilink")，1864年四月初六日清軍收復，恰巧整四年；自此[淮軍與](../Page/淮軍.md "wikilink")[金陵](../Page/金陵.md "wikilink")[湘軍聯絡暢通無阻也](../Page/湘軍.md "wikilink")。

[category:太平天國人物](../Page/category:太平天國人物.md "wikilink")
[category:清朝被處決者](../Page/category:清朝被處決者.md "wikilink")

[K坤](../Category/陳姓.md "wikilink")
[Category:桂平人](../Category/桂平人.md "wikilink")