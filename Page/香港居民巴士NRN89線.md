**香港居民巴士NRN89線**是[香港](../Page/香港.md "wikilink")[城巴一條已停辦的居民巴士路線](../Page/城巴.md "wikilink")，由位於[中環](../Page/中環.md "wikilink")（[港澳碼頭](../Page/港澳碼頭_\(香港\).md "wikilink")）巴士總站旁的[中景道](../Page/中景道.md "wikilink")（[海傍警署](../Page/海傍警署.md "wikilink")）單-{A|向}-往[沙田第一城](../Page/沙田第一城.md "wikilink")。

## 歷史

  - 1994年8月25日：投入服務。
  - 2002年：本路線改為袛提供凌晨單向往[沙田及清晨單](../Page/沙田_\(香港\).md "wikilink")-{向}-往[港島的服務](../Page/港島.md "wikilink")。
  - 2004年：取消往港島的服務。
  - 2011年4月11日：本線取消服務，由[過海隧道巴士N182線取代](../Page/過海隧道巴士N182線.md "wikilink")。

## 服務時間

  - 每日中環（港澳碼頭）開：0000、0030

## 收費

  - 全程收費：$23.50
      - 過[香港海底隧道後往](../Page/香港海底隧道.md "wikilink")[沙田第一城](../Page/沙田第一城.md "wikilink")：$11.80

## 途經街道

  - [中景道](../Page/中景道.md "wikilink")，[中港道](../Page/中港道.md "wikilink")，[西消防街](../Page/西消防街.md "wikilink")，-{[干諾道西](../Page/干諾道西.md "wikilink")}-，-{[干諾道中](../Page/干諾道中.md "wikilink")}-，[林士街](../Page/林士街.md "wikilink")，[德輔道中](../Page/德輔道中.md "wikilink")，[金鐘道](../Page/金鐘道.md "wikilink")，[添馬街](../Page/添馬街.md "wikilink")，[德立街](../Page/德立街.md "wikilink")，[紅棉路](../Page/紅棉路.md "wikilink")，[金鐘道](../Page/金鐘道.md "wikilink")，[軒尼詩道](../Page/軒尼詩道.md "wikilink")，[怡和街](../Page/怡和街.md "wikilink")，[銅鑼灣道](../Page/銅鑼灣道.md "wikilink")，[大坑道天橋](../Page/大坑道.md "wikilink")，[告士打道](../Page/告士打道.md "wikilink")，天橋，[香港海底隧道](../Page/香港海底隧道.md "wikilink")，[康莊道](../Page/康莊道.md "wikilink")，[公主道，公主道天橋](../Page/公主道.md "wikilink")，[窩打老道，窩打老道天橋，窩打老道](../Page/窩打老道.md "wikilink")，[獅子山隧道](../Page/獅子山隧道.md "wikilink")，[獅子山隧道公路](../Page/獅子山隧道公路.md "wikilink")，[沙田路](../Page/沙田路.md "wikilink")，[大涌橋路](../Page/大涌橋路.md "wikilink")，[小瀝源路及](../Page/小瀝源路.md "wikilink")[銀城街](../Page/銀城街.md "wikilink")。

## 外部連結

  - 《二十世紀巴士路線發展史－－渡海、離島及機場篇》，容偉釗編著，BSI出版

<!-- end list -->

1.  [Citybus All-Night Residental Bus Route
    城巴通宵居民巴士路線－N89R](http://www.681busterminal.com/n89r.html)
2.  [城巴-新巴－周大福企業及新創建集團成員－互動路線搜尋結果](https://www.nwstbus.com.hk/routes/routeinfo.aspx?intLangID=2&searchtype=1&routenumber=N89R&route=N89R&routetype=N&company=5&exactMatch=yes)

[N89R](../Category/已取消城巴路線.md "wikilink")
[N89R](../Category/中西區巴士路線.md "wikilink")
[N89R](../Category/沙田區巴士路線.md "wikilink")
[N89R](../Category/香港居民巴士路線.md "wikilink")