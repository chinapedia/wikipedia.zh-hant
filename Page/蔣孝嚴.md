**蔣孝嚴**（）原名**章孝嚴**，[中華民國政治人物](../Page/中華民國.md "wikilink")，籍贯[浙江](../Page/浙江.md "wikilink")[奉化](../Page/奉化.md "wikilink")，生於[廣西](../Page/廣西.md "wikilink")[桂林](../Page/桂林.md "wikilink")，[蔣經國](../Page/蔣經國.md "wikilink")[雙胞胎兒子中的兄長](../Page/雙胞胎.md "wikilink")。曾任[國民黨副主席](../Page/中國國民黨副主席.md "wikilink")、[立法委員](../Page/立法委員.md "wikilink")、[總統府秘書長](../Page/總統府秘書長.md "wikilink")、[國民黨秘書長](../Page/中國國民黨.md "wikilink")、[行政院副院長](../Page/行政院副院長.md "wikilink")、[外交部部長等職](../Page/外交部.md "wikilink")。

## 早年生活

章孝嚴與[章孝慈是](../Page/章孝慈.md "wikilink")[蔣經國與部屬](../Page/蔣經國.md "wikilink")[章亞若](../Page/章亞若.md "wikilink")[婚外所生的](../Page/外遇.md "wikilink")[雙胞胎](../Page/雙胞胎.md "wikilink")，他們於[中國抗日戰爭期間在](../Page/中國抗日戰爭.md "wikilink")[桂林出生](../Page/桂林.md "wikilink")。由於蔣經國已經與[蔣方良結婚](../Page/蔣方良.md "wikilink")，因此這兩名非婚生子僅能隨母親的[章姓](../Page/章姓.md "wikilink")，名字由[蔣中正定為](../Page/蔣中正.md "wikilink")「孝嚴」、「孝慈」\[1\]，與蔣經國的其他兒女一樣是「孝」[字輩](../Page/字輩.md "wikilink")。

章亚若於1942年8月被[謀殺](../Page/謀殺.md "wikilink")，兩兄弟由章亞若的弟弟章浩若及他的妻子紀琛扶養；舅舅及舅母在官方文件上登記為兩兄弟的生父母，直到2005年才改回真正的生父母。

[國共內戰其間](../Page/國共內戰.md "wikilink")，兩兄弟隨外婆周錦華移居台灣[新竹](../Page/新竹.md "wikilink")，由外婆、舅舅（章渙若）、舅母（章向鳳妹）繼續撫養。1960年章孝嚴畢業於臺灣省立新竹中學（今[國立新竹高級中學](../Page/國立新竹高級中學.md "wikilink")），與1959年[許信良同校但為下一期校友](../Page/許信良.md "wikilink")。

後來兩兄弟同時在[東吳大學就讀](../Page/東吳大學.md "wikilink")。1960年底，在他們18歲時才知道祖父是蔣中正、蔣經國為他們父親。\[2\]

## 從政資歷

### 外交工作

1974年到1977年間，章孝嚴在[中華民國駐美國大使館工作](../Page/中華民國駐美國大使館.md "wikilink")。這期間在[喬治城大學進修](../Page/喬治城大學.md "wikilink")，獲得碩士學位，[公家高層逐步拔擢](../Page/公家.md "wikilink")，升到[中華民國外交部北美司司長](../Page/中華民國外交部.md "wikilink")。

1996年6月10日至1997年10月20日期間，擔任[中華民國外交部部長](../Page/中華民國外交部.md "wikilink")。

  - 任內事蹟

<!-- end list -->

1.  斷交國家計有[尼日](../Page/尼日.md "wikilink")、[巴哈馬](../Page/巴哈馬.md "wikilink")、[聖露西亞三國](../Page/聖露西亞.md "wikilink")。
2.  關閉中華民國在北非唯一的商務辦事處，在[利比亞](../Page/利比亞.md "wikilink")。
3.  [南非宣布即將斷交](../Page/南非.md "wikilink")。

### 立法委員

2001年起，分別在[台北市南區及北區](../Page/台北市.md "wikilink")，先後當選第五及第六屆[立法委員](../Page/立法委員.md "wikilink")。

| 2004年臺北市第一選舉區立法委員選舉結果 |
| --------------------- |
| 應選10席                 |
| 號次                    |
| 1                     |
| 2                     |
| 3                     |
| 4                     |
| 5                     |
| 6                     |
| 7                     |
| 8                     |
| 9                     |
| 10                    |
| 11                    |
| 12                    |
| 13                    |
| 14                    |
| 15                    |
| 16                    |
| 17                    |
| 18                    |
| 19                    |
| 20                    |
| 21                    |
| 22                    |

2006年4月6日，參加中國國民黨台北市長初選，並不順利，於4月26日退選。

在[2008年立委選舉](../Page/2008年中華民國立法委員選舉.md "wikilink")[單一選區兩票制影響下](../Page/單一選區兩票制.md "wikilink")，他參選台北市第三（[中山區及](../Page/中山區_\(臺北市\).md "wikilink")[松山區](../Page/松山區_\(臺北市\).md "wikilink")）選區[立法委員](../Page/立法委員.md "wikilink")，最後以99959票(得票率60.26%)當選，擊敗同樣挑戰三連霸的民進黨立委[郭正亮](../Page/郭正亮.md "wikilink")。

| 2008年[臺北市第三選舉區](../Page/臺北市第三選舉區.md "wikilink")[立法委員選舉結果](../Page/立法委員.md "wikilink") |
| ------------------------------------------------------------------------------------- |
| 號次                                                                                    |
| 1                                                                                     |
| 2                                                                                     |
| 3                                                                                     |
| 4                                                                                     |
| 5                                                                                     |
| **選舉人數**                                                                              |
| **投票數**                                                                               |
| **有效票**                                                                               |
| **無效票**                                                                               |
| **投票率**                                                                               |

2008年11月，兼任[中國國民黨副主席](../Page/中國國民黨副主席.md "wikilink")，2014年4月底卸任。

2011年4月，[中國國民黨同黨的競爭者](../Page/中國國民黨.md "wikilink")[羅淑蕾在國民黨初選民調中以不到百分之一的差距險勝蔣孝嚴](../Page/羅淑蕾.md "wikilink")，雖然北市黨部聲明不是最終的結果，但蔣孝嚴仍決定退出臺北市第三選區立法委員的選舉並投入輔選，並展現君子風度主動擔任羅淑蕾[立法委員競選團隊主任委員](../Page/立法委員.md "wikilink")。

| 2011年[中國國民黨](../Page/中國國民黨.md "wikilink")[臺北市第三選舉區](../Page/臺北市第三選舉區.md "wikilink")[立法委員黨內初選結果](../Page/立法委員.md "wikilink") |
| --------------------------------------------------------------------------------------------------------------------------- |
| 號次                                                                                                                          |
| 1                                                                                                                           |
| 2                                                                                                                           |
| 3                                                                                                                           |

## 改為蔣姓

原稱「章孝嚴」，一直等到蔣經國配偶[蒋方良去世](../Page/蒋方良.md "wikilink")，正式改稱「蔣孝嚴」，並稱其子女將會跟隨。章孝嚴身份證父親欄寫的是舅父章浩若，母親欄是舅母紀琛。章孝嚴提出經由[王昇將軍證實在](../Page/王昇.md "wikilink")[章孝嚴](../Page/章孝嚴.md "wikilink")、[章孝慈小的時候](../Page/章孝慈.md "wikilink")，蔣經國提供了兩個孩子的生活費，有撫養的事實\[3\]。

2002年7月，章孝嚴到[江西](../Page/江西.md "wikilink")[南昌](../Page/南昌.md "wikilink")，找到章浩若兩個兒子章修純、章修維，寫下了聲明書，證明章浩若是他們的父親，非章孝嚴生父，章亞若是他們的三姑，也是章家雙胞胎的生母。此後章孝嚴從桂林醫院取得出生証明，母親欄寫的是舅媽紀琛女士，但她已經遷居美國多年。2003年章孝嚴到美國訪問舅媽，在[中華民國駐洛杉磯辦事處兩位人員的見證下](../Page/中華民國.md "wikilink")，取得幾根頭髮拿去做DNA化驗，在駐外辦事處見證下，紀琛和章孝嚴這對「法律母子」的關係劃斷。\[4\]2005年3月經由[臺北市政府民政局](../Page/臺北市政府民政局.md "wikilink")、[內政部](../Page/內政部.md "wikilink")、[法務部等部門一系列複雜的法律程序](../Page/法務部.md "wikilink")，將身份証父親欄改為蔣經國。\[5\]

其弟[章孝慈曾於](../Page/章孝慈.md "wikilink")《那段剝花生充饑的日子》一文中提過，兩人的外婆周錦華女士曾對他們說過，兩人的父親[蔣經國是個正直勇敢能幹的人](../Page/蔣經國.md "wikilink")。

## 外遇誹聞事件

1999年12月21日，時為總統府秘書長章孝嚴透過總統府發言人丁遠超發表聲明，坦承11月10日於國民黨秘書長任內「在不正常的情況下」簽下一紙「我承諾在明年六月底以前完成和她（黃美倫）辦妥離婚的手續」的台北晶華酒店便箋。\[6\]\[7\]\[8\]辭職後的章孝嚴成為迄今史上任期最短的[總統府秘書長](../Page/總統府秘書長.md "wikilink")。

## 家庭

[蒋孝严與妻子黃美倫育有兩女](../Page/蒋孝严.md "wikilink")[惠蘭](../Page/蔣蕙蘭.md "wikilink")、惠筠及一子[萬安](../Page/蔣萬安.md "wikilink")。

## 家世

<center>

</center>

  - **舅父**：[章浩若](../Page/章浩若.md "wikilink")（1916-1969）
  - **舅母**：[紀琛](../Page/紀琛.md "wikilink")（1919-2006）\[9\]

## 著作

|        |                     |        |         |                 |
| ------ | ------------------- | ------ | ------- | --------------- |
| **年份** | **書籍名稱**            | **作者** | **出版社** | **ISBN**        |
| 2004年  | 《春來燕歸：揭露二○○三春節包機始末》 | 章孝嚴    | 時周      | ISBN 9867586107 |
|        |                     |        |         |                 |

## 相關條目

  - [蔣中正家族](../Page/蔣中正家族.md "wikilink")
  - [蕭萬長內閣](../Page/蕭萬長內閣.md "wikilink")

## 參考文献

## 外部連結

  - [立法院
    蔣孝嚴委員簡介](http://www.ly.gov.tw/03_leg/0301_main/legIntro.action?lgno=00093&stage=7)
  - [蔣孝嚴的部落格](http://blog.udn.com/Johnchiang01)

{{-}}   |- |colspan="3"
style="text-align:center;"|**[Executive_Yuan,ROC_LOGO.svg](https://zh.wikipedia.org/wiki/File:Executive_Yuan,ROC_LOGO.svg "fig:Executive_Yuan,ROC_LOGO.svg")[行政院](../Page/行政院.md "wikilink")**
|-          |- |colspan="3"
style="text-align:center;"|**[ROC_Office_of_the_President_Emblem.svg](https://zh.wikipedia.org/wiki/File:ROC_Office_of_the_President_Emblem.svg "fig:ROC_Office_of_the_President_Emblem.svg")[總統府](../Page/總統府.md "wikilink")**
|-    |-  |- |colspan="3" style="text-align:center;"|

[J蔣孝嚴](../Category/1942年出生.md "wikilink")
[J蔣](../Category/中華民國總統府秘書長.md "wikilink")
[J蔣](../Category/中華民國外交部部長.md "wikilink")
[J蔣](../Category/中華民國僑務委員會委員長.md "wikilink")
[J蔣](../Category/第5屆中華民國立法委員.md "wikilink")
[J蔣](../Category/第6屆中華民國立法委員.md "wikilink")
[J蔣](../Category/第7屆中華民國立法委員.md "wikilink")
[J蔣](../Category/中國國民黨黨員.md "wikilink")
[J蔣](../Category/東吳大學校友.md "wikilink")
[Category:喬治城大學校友](../Category/喬治城大學校友.md "wikilink")
[J蔣](../Category/國立新竹高級中學校友.md "wikilink")
[J蔣](../Category/台灣戰後廣西移民.md "wikilink")
[J蔣](../Category/台灣戰後寧波移民.md "wikilink")
[奉化人](../Category/奉化人.md "wikilink")
[J蔣](../Category/桂林人.md "wikilink")
[J蔣孝嚴](../Category/蔣氏家族.md "wikilink")
[X](../Category/蒋经国.md "wikilink")
[Category:蔣家第三代](../Category/蔣家第三代.md "wikilink")
[X孝](../Category/蔣姓.md "wikilink")
[Category:臺灣雙胞胎](../Category/臺灣雙胞胎.md "wikilink")
[J](../Category/中華民國總統兒子.md "wikilink")

1.
2.  《[聯合報](../Page/聯合報.md "wikilink")》，台北，1990年1月2日
3.  [蔣孝嚴：身處“蔣家門外”，留下終身遺憾
    中國評論 2009-07-24](http://www.chinareviewnews.com/doc/1010/3/0/2/101030241_3.html?coluid=7&kindid=0&docid=101030241&mdate=0724120319)
4.  [章孝嚴跋涉萬里追蹤DNA](http://www.businessweekly.com.tw/article.php?id=15757)
5.  [蔣孝嚴：認祖歸宗背後的秘密
    瀟湘晨報 2005年07月23日](http://news.sina.com.cn/c/2005-07-23/07536507175s.shtml)
6.  \[<http://forums.chinatimes.com/special/king/88122202.htm>　章孝嚴婚外情
    王筱嬋傳為女主角\]
7.  \[<http://forums.chinatimes.com/special/king/scandal.htm>　章孝嚴緋聞案全收錄\]
8.  \[<http://old.ltn.com.tw/2002/new/nov/14/today-p12.htm>　王筱嬋︰曾打章孝嚴一巴掌\],
    [自由時報](../Page/自由時報.md "wikilink"), 2002-11-14
9.  [纪琛女士的儿子党宏
    在西安披露沧桑往事](http://news.sina.com.cn/c/2006-06-26/06559297308s.shtml).
    楚天金报，2006-6-26