[_Jian-ying_Bridge_2_,_Hualien.JPG](https://zh.wikipedia.org/wiki/File:_Jian-ying_Bridge_2_,_Hualien.JPG "fig:_Jian-ying_Bridge_2_,_Hualien.JPG")的箭瑛大橋\]\]
**箭瑛大橋**是位於[台灣](../Page/台灣.md "wikilink")[花蓮縣](../Page/花蓮縣.md "wikilink")[鳳林鎮的一座](../Page/鳳林鎮_\(臺灣\).md "wikilink")[水泥大橋](../Page/水泥.md "wikilink")，橫跨[花蓮溪連結大榮里與山興里](../Page/花蓮溪.md "wikilink")。

## 由來

山興（原名六階鼻，1937年改稱山崎，戰後再改為今名）與鳳林鎮中心之間的連絡通道只有一座用[竹子搭建的便橋](../Page/竹子.md "wikilink")，交通不便而且危險。1977年10月6日清晨，[黛納颱風來襲過後](../Page/颱風黛納_\(1977年\).md "wikilink")，遭花蓮溪水暴漲，便橋因此被沖毀，[山興國小](../Page/山興國小.md "wikilink")（現已廢校）教務主任陳國義與教師[張箭](../Page/張箭.md "wikilink")、[鄧玉瑛](../Page/鄧玉瑛.md "wikilink")、林寶炫等人為了到校上課，冒險以手牽手的方式渡河，但因洪流湍急，加上泥沙的影響，結果多人遭大水沖走，其中張箭、鄧玉瑛二人不幸溺斃。

時任[行政院長的](../Page/行政院長.md "wikilink")[蔣經國也深受感動](../Page/蔣經國.md "wikilink")，決定在該處興建水泥大橋，以解決當地的交通問題，並為表彰張、鄧二位老師的克盡職守、因公殉職的精神，取張箭的「箭」和鄧玉瑛的「瑛」命名為「箭瑛大橋」。此一故事曾於1985年拍攝為[電影](../Page/電影.md "wikilink")《[箭瑛大橋](../Page/箭瑛大橋_\(電影\).md "wikilink")》（編劇：姚慶康，導演：陳俊良，主演：[石雋](../Page/石雋.md "wikilink")、顏鳳嬌）。

時任[中華民國](../Page/中華民國.md "wikilink")[總統](../Page/總統.md "wikilink")[馬英九在](../Page/馬英九.md "wikilink")2012年1月時，盼兩位老師可以入祀[忠烈祠](../Page/忠烈祠.md "wikilink")\[1\]。

該地尚建有箭瑛公園，立有張箭和鄧玉瑛的紀念銅像與石碑以資紀念。

因花蓮溪河道變寬，已超出橋樑原設計的長度，將造成危險。因此花蓮縣政府已於2013年計劃改建此橋\[2\]。

2014年8月25日，由鳳林鎮文史協會透過臉書社團「鳳林幫」與幫友集思廣益，促成《箭瑛大橋》電影播放會，活動以「辦桌」型態舉行，免費提供瓜子、餅乾及開水，蚊子電影院就設在夜市旁邊，座位幾乎「座無虛席」，透過電影串起鳳林人的老回憶。鳳林鎮長彭宗乾表示，已和片商及文化部積極爭取，盼能將電影《箭瑛大橋》列為鳳林鎮公共財，讓這部片隨時都能在鳳林鎮免費播放\[3\]。

## 參考資料

<references/>

## 外部連結

  - [箭瑛大橋圖片集（Bambooson竹子小站）](http://tw.myblog.yahoo.com/jw!g9tKJ3iGGRLfMOaHmwnb7ttS/article?mid=383&prev=-1&next=356)
  - [「箭瑛」殉職30年
    學童冒雨繫黃絲帶追思](http://www.libertytimes.com.tw/2007/new/oct/6/today-life4.htm)

[Category:花蓮縣橋梁](../Category/花蓮縣橋梁.md "wikilink")
[Category:1978年完工橋梁](../Category/1978年完工橋梁.md "wikilink")
[Category:冠以人名的桥梁](../Category/冠以人名的桥梁.md "wikilink") [Category:鳳林鎮
(台灣)](../Category/鳳林鎮_\(台灣\).md "wikilink")
[Category:梁桥](../Category/梁桥.md "wikilink")

1.
2.
3.