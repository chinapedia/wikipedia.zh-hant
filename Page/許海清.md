**許海清**（），[臺灣企業家](../Page/臺灣.md "wikilink")、政治人物、知名[幫派人物](../Page/幫派.md "wikilink")，生於[日治台灣](../Page/日治台灣.md "wikilink")[台北州](../Page/台北州.md "wikilink")[台北市](../Page/台北市.md "wikilink")[艋舺](../Page/艋舺.md "wikilink")（今台北市[萬華區](../Page/萬華區.md "wikilink")），黑道兄弟因而尊稱他為**艋舺大仔**、**艋舺兄哥**、**艋舺阿哥**，略稱**艋哥**，後因個頭小、長相斯文，神似[蚊子](../Page/蚊子.md "wikilink")，而改稱之為[臺語音近的](../Page/臺語.md "wikilink")**蚊哥**，他有時則自嘲為**許仔蚊**，他在臺灣[江湖中有重大影響力](../Page/江湖.md "wikilink")，時常調停全臺[綠林紛爭](../Page/綠林.md "wikilink")，素有「黑道最後仲裁者」之稱。同時，他也和[日本](../Page/日本.md "wikilink")[山口組](../Page/山口組.md "wikilink")、[住吉會等](../Page/住吉會.md "wikilink")[極道團體交好](../Page/極道.md "wikilink")。

## 早年

許海清雙親早逝，由外婆撫養長大，5歲時在[艋舺](../Page/艋舺.md "wikilink")「[河溝頭](../Page/河溝頭.md "wikilink")」撿拾廢棄水果，賺取微薄生活費。外婆1921年過世，許海清從此自力更生，靠著拉板車運貨討生活。在今日[峨嵋街](../Page/峨嵋街.md "wikilink")、[武昌街和](../Page/武昌街.md "wikilink")[環河南路一帶的](../Page/環河南路.md "wikilink")「河溝頭」是當時最大[水果集散地](../Page/水果.md "wikilink")，各路人馬匯集，許海清因而結識不少道上好友，並開始混跡[江湖](../Page/江湖.md "wikilink")，俠義個性使然，從市場糾紛到黑道恩怨，許海清總能成功排解，由於處事明快、是非分明，許海清年方二十餘歲，就被推為艋舺（[萬華](../Page/萬華區.md "wikilink")）地方的[老大](../Page/老大.md "wikilink")。人稱「艋舺阿哥」（簡稱艋哥），艋哥綽號因此得名。

1947年[二二八事件期間](../Page/二二八事件.md "wikilink")，許海清憑著聲望化解不少官民衝突。他創立「香蕉青果公會」，成了[公家與地方](../Page/公家.md "wikilink")[黑道角頭的溝通橋樑](../Page/黑道.md "wikilink")，也一併打開台灣香蕉外銷日本的通路，因而結識[日本極道](../Page/日本極道.md "wikilink")[指定暴力團](../Page/指定暴力團.md "wikilink")[山口組](../Page/山口組.md "wikilink")、[住吉會等幫會領袖](../Page/住吉會.md "wikilink")。

許海清交遊廣闊，政商關係良好，勢力是當時唯一能橫跨各大[角頭和各大外省幫派屬於](../Page/角頭.md "wikilink")[縱貫線級數的](../Page/縱貫線.md "wikilink")「[任俠界](../Page/任俠.md "wikilink")[教父](../Page/教父.md "wikilink")」，更涉足政治，曾以國民黨籍高票當選第一屆[台北市議會議員](../Page/台北市議會.md "wikilink")。早年在萬華地盤的生意，從[賭場](../Page/賭場.md "wikilink")、[當鋪](../Page/當鋪.md "wikilink")、[錢莊到](../Page/錢莊.md "wikilink")[舞廳](../Page/舞廳.md "wikilink")、[歌廳](../Page/歌廳.md "wikilink")、[酒店等](../Page/酒店.md "wikilink")[特種行業](../Page/特種行業.md "wikilink")，甚至是[銀樓](../Page/銀樓.md "wikilink")、[餐館](../Page/餐館.md "wikilink")、[水果店](../Page/水果.md "wikilink")、[中藥行](../Page/中藥.md "wikilink")、[鐘錶行](../Page/鐘錶.md "wikilink")、[計程車行](../Page/計程車.md "wikilink")、南北雜貨等普通生意都有，身邊有數百分屬不同勢力的首領級[兄弟靠攏](../Page/兄弟.md "wikilink")，更有許多中南部的角頭大哥願意聽從他的指示，隨時效命。

1984年11月[台灣政府為了消滅](../Page/台灣政府.md "wikilink")[江南案的](../Page/江南案.md "wikilink")[竹聯幫](../Page/竹聯幫.md "wikilink")，連帶全台掃黑，執行[一清專案](../Page/一清專案.md "wikilink")，在[公家的特別注目下](../Page/公家.md "wikilink")，次年，許海清宣佈退出江湖，但仍時常為[江湖弟兄調解糾紛](../Page/江湖.md "wikilink")。後來，在某次南下調解幫派糾紛時，途中意外發生[車禍受傷](../Page/車禍.md "wikilink")，造成行動不便，加上重聽之故，晚年只偶而在婚喪喜慶上露面而已。

享壽高齡九十有四，過世時，治喪委員會榮譽主委是聞名[日本](../Page/日本.md "wikilink")[極道的](../Page/極道.md "wikilink")「木杞」[何戊己](../Page/何戊己.md "wikilink")，[天道盟](../Page/天道盟.md "wikilink")「圓仔花」[鄭仁治擔任](../Page/鄭仁治.md "wikilink")[主委](../Page/主委.md "wikilink")，其餘如曾任[立法委員的](../Page/立法委員.md "wikilink")「大貓」[羅福助](../Page/羅福助.md "wikilink")、「冬瓜標」[顏清標](../Page/顏清標.md "wikilink")、[竹聯幫](../Page/竹聯幫.md "wikilink")「周霸子」[周榕](../Page/周榕.md "wikilink")、「趙霸子」[趙爾文與](../Page/趙爾文.md "wikilink")「-{么么}-」[黃少岑](../Page/黃少岑.md "wikilink")、[四海幫](../Page/四海幫.md "wikilink")「蔡霸子」[蔡冠倫](../Page/蔡冠倫.md "wikilink")、「老賈」[賈潤年](../Page/賈潤年.md "wikilink")、天道盟[聯正會](../Page/聯正會.md "wikilink")「雙伍」[楊雙伍](../Page/楊雙伍.md "wikilink")、[濟公會](../Page/濟公會.md "wikilink")「蕭濟公」蕭澤宏、[同心會](../Page/同心會.md "wikilink")「芋粿」吳明貴、[至尊會](../Page/至尊會.md "wikilink")「文將」陳文將、[華西街會](../Page/華西街會.md "wikilink")「細漢他K」呂榮輝、[松聯幫](../Page/松聯幫.md "wikilink")「劉邦」劉震邦、「豹哥」[王知強](../Page/王知強.md "wikilink")、[中聯幫](../Page/中聯幫.md "wikilink")「空信」[張忠信](../Page/張忠信.md "wikilink")、[牛埔幫](../Page/牛埔幫.md "wikilink")「牛財」[葉明財](../Page/葉明財.md "wikilink")、[飛鷹幫](../Page/飛鷹幫.md "wikilink")「蔣哥」[蔣佩廉](../Page/蔣佩廉.md "wikilink")、[嘉義](../Page/嘉義.md "wikilink")「瘋琴」[盧照琴等](../Page/盧照琴.md "wikilink")[本省](../Page/本省人.md "wikilink")、[外省幫派大人物擔任副主委](../Page/外省人.md "wikilink")、委員、親友代表等，還有日本山口組弔唁代表西口茂男及野口松男，共有十萬名[江湖分子齊聚送葬](../Page/江湖.md "wikilink")，並宣布其出殯日（5月29日）為「黑道平安日」，禁止各幫派尋仇滋事，以紀念許海清。

## 參考資料

  - [黑道教父 蚊哥病逝](http://news.ltn.com.tw/news/society/paper/10297)
  - [河溝頭VS.頭北厝 艋舺角頭風雲起
    中生代比狠搶上位](http://magazine.chinatimes.com/ctweekly/20150814003279-300106)
  - [台湾"黑帮教父"出殡
    千人送行](http://news.bbc.co.uk/chinese/simp/hi/newsid_4590000/newsid_4591200/4591237.stm)
  - [黑幫老大喪禮 比排場拚團結](http://news.ltn.com.tw/news/society/paper/965113)

[Category:台灣黑社會成員](../Category/台灣黑社會成員.md "wikilink")
[Category:台灣企業家](../Category/台灣企業家.md "wikilink")
[Category:第1屆臺北省轄市議員](../Category/第1屆臺北省轄市議員.md "wikilink")
[Category:中國國民黨黨員](../Category/中國國民黨黨員.md "wikilink")
[Category:台北市政治人物](../Category/台北市政治人物.md "wikilink")
[Category:萬華人](../Category/萬華人.md "wikilink")
[Hai海](../Category/許姓.md "wikilink")