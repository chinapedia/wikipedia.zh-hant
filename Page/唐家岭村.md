[北京市](../Page/北京市.md "wikilink")[海淀区唐家岭村](../Page/海淀区.md "wikilink")，位于北京市区西北五环外的[西北旺镇](../Page/西北旺镇.md "wikilink")

## 地理位置

唐家岭村东邻[昌平区的](../Page/昌平区.md "wikilink")[回龙观](../Page/回龙观.md "wikilink")，西邻[土井村](../Page/土井村.md "wikilink")，南邻[后厂村](../Page/后厂村.md "wikilink")、[杨庄子](../Page/杨庄子.md "wikilink")、[东北旺](../Page/东北旺.md "wikilink")，北邻[辛店村和昌平区的](../Page/辛店村.md "wikilink")[二拨子村](../Page/二拨子.md "wikilink")，包含靠近[航天城的](../Page/航天城.md "wikilink")[邓庄子](../Page/邓庄子.md "wikilink")，属于比较典型的城乡结合部。
近年来南面的东北旺村土地被[中关村软件园征用](../Page/中关村软件园.md "wikilink")，造成该村直接面对城市规划的局面。

## 人文历史

据传说，该村现名源于原称[挡儿岭](../Page/挡儿岭.md "wikilink")。民间附会为[宋代](../Page/宋.md "wikilink")[佘太君在](../Page/佘太君.md "wikilink")[望儿山向北望儿](../Page/望儿山.md "wikilink")，因该处遮挡儿子身影，因而称此处为“挡儿岭”，后转演成今名。

## 人口

唐家岭村户籍人口2600人，[外来流动人口数万人](../Page/北漂.md "wikilink")。
近年来，随着上地附近的[树村](../Page/树村.md "wikilink")、[马连洼](../Page/马连洼.md "wikilink")、[东北旺等](../Page/东北旺.md "wikilink")[城中村的改造和拆迁](../Page/城中村.md "wikilink")，唐家岭村成了[中关村软件园及](../Page/中关村软件园.md "wikilink")[上地信息产业基地附近的最后一个城中村](../Page/上地信息产业基地.md "wikilink")。
因为周边社区的租房价格较高，有许多在附近工作的人在这里租房，2006年以来，该村很多村民都在新建和扩建新房，以出租给更多的人赚取房租，但该村并不属于[貧民窟](../Page/貧民窟.md "wikilink")。唐家岭村拆迁后，多数租客转而在昌平区[史各庄租房](../Page/史各庄街道.md "wikilink")。\[1\]

## 附近学校

唐家岭小学 [中国管理软件学院](../Page/中国管理软件学院.md "wikilink")

## 交通

公交车站有唐家岭南站和唐家岭北站。公交车有运通205，运通112，365，642，447，509等。
靠近北京地铁13号线[西二旗站](../Page/西二旗站.md "wikilink")。

## 参看

[蚁族](../Page/蚁族.md "wikilink")

[Category:海淀区](../Category/海淀区.md "wikilink")

1.