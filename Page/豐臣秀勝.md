**豐臣秀勝**（），三好吉房的次子，母親[瑞龍院阿智](../Page/瑞龍院.md "wikilink")，是[豐臣秀吉的外甥](../Page/豐臣秀吉.md "wikilink")，關白[秀次的親弟弟](../Page/豐臣秀次.md "wikilink")。豐臣秀勝幼名小吉，正室為[淺井長政三女](../Page/淺井長政.md "wikilink")[阿江](../Page/崇源院.md "wikilink")。

## 生平

天正13年（1585年）封予近江瀨田，在這期間，秀吉的養子[羽柴秀勝](../Page/羽柴秀勝.md "wikilink")（[織田信長五子](../Page/織田信長.md "wikilink")）病故。而秀吉也因為沒有繼承人的關係，讓秀勝做自己的養嗣子。不久之後秀勝改封丹波龜山。成為龜山城城主的期間，娶[淺井長政的三女阿江](../Page/淺井長政.md "wikilink")（後來[德川秀忠的正室](../Page/德川秀忠.md "wikilink")）為正室。

天正15年（1587年）參與「九州-{征}-討」。爾後秀勝向秀吉要求增加領地引起秀吉的不滿，轉封越前敦賀，之後由於蜂屋賴隆的去世，領了他遺留下來的五千石領地。

天正18年（1590年）的「小田原-{征}-討」秀勝十分活躍，與兄長秀次和當代名將[蒲生氏鄉一起出戰](../Page/蒲生氏鄉.md "wikilink")，負責圍攻小田原城的北部。戰後，秀勝被給予甲斐的府中城，不久改封美濃的歧阜城。據說秀勝一直被封在近畿內的大國，是因為母親阿智向秀吉一直要求。

文祿元年（1592年）的[文祿之役為第九軍的大將](../Page/文祿之役.md "wikilink")，領八千人與[細川忠興一起渡河](../Page/細川忠興.md "wikilink")。不過後來卻在[朝鮮](../Page/朝鮮.md "wikilink")[巨濟島病故](../Page/巨濟島.md "wikilink")。享年僅23歲。而在他去世不久，正室阿江生下一名女嬰，即[豐臣完姬](../Page/豐臣完子.md "wikilink")。完子的一位女性子孫後來就是[大正天皇的皇后](../Page/大正天皇.md "wikilink")，並誕下[昭和天皇](../Page/昭和天皇.md "wikilink")—[裕仁](../Page/裕仁.md "wikilink")。

## 軼聞

秀吉在中年才和側室南殿生下的長男[石松丸秀勝](../Page/羽柴秀勝_\(石松丸\).md "wikilink")，而此子又在六歲時夭折，因此秀吉十分懷念這第一個孩子。這可能就是將後來幾個養子都取名為「秀勝」的緣故，而豐臣秀勝也是其中一個。

## 家族

  - 父：三好吉房
  - 母：[瑞龍院阿智](../Page/瑞龍院.md "wikilink")
  - 舅：[豐臣秀吉](../Page/豐臣秀吉.md "wikilink")
  - 舅：[豐臣秀長](../Page/豐臣秀長.md "wikilink")
  - 姨：[旭姬](../Page/旭姬.md "wikilink")
  - 兄：[豐臣秀次](../Page/豐臣秀次.md "wikilink")
  - 弟：[豐臣秀保](../Page/豐臣秀保.md "wikilink")
  - 妻：[崇源院](../Page/崇源院.md "wikilink")
  - 女：[豐臣完姬](../Page/豐臣完子.md "wikilink")

[Category:織豐政權大名](../Category/織豐政權大名.md "wikilink")
[Category:木下氏](../Category/木下氏.md "wikilink")
[Category:豐臣氏](../Category/豐臣氏.md "wikilink")
[Category:戰國武將](../Category/戰國武將.md "wikilink")
[Category:尾張國出身人物](../Category/尾張國出身人物.md "wikilink")