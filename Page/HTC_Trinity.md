**HTC Trinity**，原廠型號**HTC P3600，HTC
P3600i**，是台灣[宏達電公司所推出的](../Page/宏達電.md "wikilink")[智慧型手機](../Page/智慧型手機.md "wikilink")，搭載微軟
[Windows Mobile](../Page/Windows_Mobile.md "wikilink") 5，首款GPS
PDA手機，2006年9月於歐洲首度發表。已知客製版本HTC P3600，HTC P3600i，Dopod D810，Dopod
CHT9100，Dopod CHT9110，Orange SPV M700，Vodafone VPA Compact GPS，SFR
S300+，Swisscom XPA v1510。

## 技術規格

  - [處理器](../Page/處理器.md "wikilink")：Samsung SC32442A 400 MHz
  - [作業系統](../Page/作業系統.md "wikilink")：Windows Mobile 5.0 PocketPC Phone
    Edition
  - [記憶體](../Page/記憶體.md "wikilink")：ROM：128MB，RAM：64MB
  - 尺寸：108 毫米 X 58.2 毫米 X 18.4 毫米
  - 重量：150g（含電池）
  - [螢幕](../Page/螢幕.md "wikilink")：QVGA 解析度、2.8 吋 TFT-LCD 平面式觸控感應螢幕
  - [網路](../Page/網路.md "wikilink")：HSDPA/WCDMA/GSM
  - [GPS](../Page/GPS.md "wikilink")：配備GPS及A-GPS
  - [藍牙](../Page/藍牙.md "wikilink")：2.0 with EDR
  - [Wi-Fi](../Page/Wi-Fi.md "wikilink")：IEEE 802.11 b/g
  - [相機](../Page/相機.md "wikilink")：200萬畫素相機，支援自動對焦功能
  - [電池](../Page/電池.md "wikilink")：1350mAh充電式鋰或鋰聚合物電池

## 參見

  - [HTC](../Page/HTC.md "wikilink")
  - [Qtek](../Page/Qtek.md "wikilink")
  - [Dopod](../Page/Dopod.md "wikilink")
  - [TouchFLO](../Page/TouchFLO.md "wikilink")

## 外部連結

  - [HTC P3600 概觀](http://www.htc.com/europe/product.aspx?id=15684)
  - [HTC P3600 技術規格](http://www.htc.com/europe/product.aspx?id=15688)
  - [HTC P3600i 概觀](http://www.htc.com/hk-tc/product.aspx?id=27376)
  - [HTC P3600i 技術規格](http://www.htc.com/hk-tc/product.aspx?id=27382)

[H](../Category/智能手機.md "wikilink")
[Trinity](../Category/宏達電手機.md "wikilink")