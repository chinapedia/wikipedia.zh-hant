**鍾離昧**，或作**鍾離眛**（），[秦朝末年](../Page/秦朝.md "wikilink")[項羽部將](../Page/項羽.md "wikilink")，[漢朝初年](../Page/漢朝.md "wikilink")，被[韓信逼迫而](../Page/韓信.md "wikilink")[自殺](../Page/自殺.md "wikilink")。

## 生平

鍾離昧在[秦末參加了](../Page/秦朝.md "wikilink")[項梁與](../Page/項梁.md "wikilink")[項羽的軍隊](../Page/項羽.md "wikilink")，[楚漢戰爭時是楚軍的重要將領](../Page/楚漢戰爭.md "wikilink")，與[范增](../Page/范增.md "wikilink")、[龍且](../Page/龍且.md "wikilink")、[周殷同为](../Page/周殷.md "wikilink")[項羽信任的人](../Page/項羽.md "wikilink")，[韓信和鍾離昧是好朋友](../Page/韓信.md "wikilink")，鍾離昧多次向[項羽推薦韓信](../Page/項羽.md "wikilink")，但[項羽仍未重用韓信](../Page/項羽.md "wikilink")。後來[項羽中了](../Page/項羽.md "wikilink")[陳平的](../Page/陳平_\(漢朝\).md "wikilink")[離間計](../Page/離間計.md "wikilink")，懷疑鍾離昧的忠誠度。[垓下之戰時](../Page/垓下之戰.md "wikilink")，鍾離昧逃離楚軍的陣營。

[項羽死後](../Page/項羽.md "wikilink")，鍾離昧投靠[韓信](../Page/韓信.md "wikilink")。[劉邦稱帝後](../Page/劉邦.md "wikilink")，封[韓信為楚王](../Page/韓信.md "wikilink")，[劉邦得知鍾離昧逃到楚國後](../Page/劉邦.md "wikilink")，要求韓信追捕，韓信則派兵保護鍾離昧的安全。高祖七年（前201年）[消滅異姓王風潮開始](../Page/消滅異姓王.md "wikilink")，有人告發楚王[謀反](../Page/謀反.md "wikilink")，[漢高祖](../Page/漢高祖.md "wikilink")[劉邦会诸侯于陈](../Page/劉邦.md "wikilink")，有意捉拿韓信。[韓信有發兵抵抗](../Page/韓信.md "wikilink")，自陳無罪，但又怕事情鬧大。有謀士跟韓信獻策說，殺鍾離昧“以謁上，上必喜，無患。”韩信又与钟离昧商量对策，鍾離昧告訴韓信說，皇帝之所以不敢攻打您，是因為我們在一起，“如公若欲捕我自媚汉，吾今死，公随手亡矣。”韓信不聽，鍾離昧大罵“公非長者”，遂自刎而死。\[1\]韓信帶著钟离昧的人頭於陈县（今[河南](../Page/河南.md "wikilink")[淮陽](../Page/淮陽.md "wikilink")）向[劉邦說明原委](../Page/劉邦.md "wikilink")，\[2\][劉邦令人將其擒拿](../Page/劉邦.md "wikilink")，後來[劉邦赦免](../Page/劉邦.md "wikilink")[韓信](../Page/韓信.md "wikilink")，被降為淮陰侯。

## 子孫

根據《[新唐書](../Page/新唐書.md "wikilink")》表15上宰相世系5上的記載，鍾離昧有二子，長子[鍾離發](../Page/鍾離發.md "wikilink")，二子[鍾離接](../Page/鍾離接.md "wikilink")，居[潁川](../Page/潁川.md "wikilink")[長社](../Page/長社.md "wikilink")。後來[鍾離接改為](../Page/鍾離接.md "wikilink")[鍾姓](../Page/鍾姓.md "wikilink")，名[鍾接](../Page/鍾接.md "wikilink")。

[東漢的](../Page/東漢.md "wikilink")[鍾離意](../Page/鍾離意.md "wikilink")、[東吳的](../Page/東吳.md "wikilink")[鍾離牧因為出身是](../Page/鍾離牧.md "wikilink")[會稽郡](../Page/會稽郡.md "wikilink")[山陰](../Page/山陰.md "wikilink")，很有可能是鍾離昧的子孫。

而根據鍾氏系譜的記載，[東漢的](../Page/東漢.md "wikilink")[鍾皓](../Page/鍾皓.md "wikilink")、[鍾迪](../Page/鍾迪.md "wikilink")，[三國](../Page/三國.md "wikilink")[曹魏的](../Page/曹魏.md "wikilink")[鍾繇](../Page/鍾繇.md "wikilink")、[鍾會](../Page/鍾會.md "wikilink")，都是[潁川](../Page/潁川.md "wikilink")[長社出身](../Page/長社.md "wikilink")，應該是其後代。

## 考據

### 出身地

## 影視形象

  - 電影《[西楚霸王](../Page/西楚霸王_\(電影\).md "wikilink")》（1994年）：由[徐錦江飾演鍾離昧](../Page/徐錦江.md "wikilink")。
  - 中國電視劇《[漢劉邦](../Page/漢劉邦.md "wikilink")》（1998年）：由[盧勇飾演鍾離昧](../Page/卢勇_\(演员\).md "wikilink")。
  - 香港電視劇《[楚汉骄雄](../Page/楚汉骄雄.md "wikilink")》（2004年）：由[麥嘉倫飾演鍾離昧](../Page/麥嘉倫.md "wikilink")。
  - 中國電視劇《[楚汉風雲](../Page/楚汉風雲.md "wikilink")》（2005年）：由[胡龍吟飾演鍾離昧](../Page/胡龍吟.md "wikilink")。
  - 中國電視劇《[神話](../Page/神話.md "wikilink")》（2010年）：由[張春仲飾演鍾離昧](../Page/張春仲.md "wikilink")。
  - 中國電視劇《[大風歌](../Page/大風歌_\(电视剧\).md "wikilink")》（2010年）：由[大力飾演鍾離昧](../Page/大力.md "wikilink")。
  - 中國電視劇《[楚汉爭雄](../Page/楚汉爭雄.md "wikilink")》（2011年）：由[應昊茗飾演鍾離昧](../Page/應昊茗.md "wikilink")。
  - 中國電視劇《[楚汉傳奇](../Page/楚汉傳奇.md "wikilink")》（2012年）：由[葉鵬飾演鍾離昧](../Page/葉鵬.md "wikilink")。
  - [杭州玄機科技信息技術有限公司製作的中國大陸](../Page/杭州玄機科技信息技術有限公司.md "wikilink")[三維](../Page/三維.md "wikilink")[武俠](../Page/武俠.md "wikilink")[動畫](../Page/動畫.md "wikilink")《[秦時明月](../Page/秦時明月_\(動畫\).md "wikilink")》第五季君臨天下第21集（2015年）：由[刘以嘉擔任鍾離昧的配音員](../Page/刘以嘉.md "wikilink")。

## 注释

[Z](../Category/秦朝軍事人物.md "wikilink")
[Z](../Category/西漢軍事人物.md "wikilink")
[M](../Category/钟离姓.md "wikilink")
[Category:楚漢戰爭人物](../Category/楚漢戰爭人物.md "wikilink")
[Category:西漢自殺人物](../Category/西漢自殺人物.md "wikilink")

1.  《史記》記載鍾離昧死事，實多矛盾處。《史记·秦楚之际月表》记载刘邦斩杀钟离昧的时间是汉五年九月。《资治通鉴·汉纪三》（卷十一）曰：六年，“冬，十月，人有上书告楚王反。”韓信被告發事件发生的时间是六年十月，當時钟离昧可能已被斬死。《史记·淮阴侯列传》未明言韓信“持其（钟离昧）首，谒高祖于陈”的时间，高祖本紀則繫於十二月。《史记·陈丞相世家》记载韓信被捕：“（刘邦）行未至陈，楚王信果郊迎道中，高帝豫具武士，见信至，即执缚之，载后车。”並未提及钟离昧。
2.  郭嵩燾《史記札記》說，“信斬鍾離昧以謁漢王，最為無理，儻亦所謂迷亂失次者耶？”