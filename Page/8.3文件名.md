**8.3文件名**是一種限制對[檔案名稱的長度的方法](../Page/檔案名稱.md "wikilink")，這在[DOS和](../Page/DOS.md "wikilink")[Microsoft
Windows的](../Page/Microsoft_Windows.md "wikilink")[Windows
95及](../Page/Windows_95.md "wikilink")[Windows
NT](../Page/Windows_NT.md "wikilink")
3.5以前的版本中，在[FAT](../Page/FAT.md "wikilink")[檔案系統中的常用方法](../Page/檔案系統.md "wikilink")。在以前的[CP/M及某些的](../Page/CP/M.md "wikilink")[通用資料和](../Page/通用資料.md "wikilink")[迪吉多](../Page/迪吉多.md "wikilink")[微型電腦作業系統中使用](../Page/微型電腦.md "wikilink")。

8.3檔案名稱最多只可以含有8個字元，跟著最多3個字元的[副檔名](../Page/文件扩展名.md "wikilink")，利用『`.`』號將兩者連接起來。檔案及[目錄名稱是全部使用](../Page/目錄_\(電腦\).md "wikilink")[大寫字母的](../Page/大寫字母.md "wikilink")。

## 對於8.3與長檔名的相容性

[VFAT](../Page/VFAT.md "wikilink")，是對於FAT系統中的加強版，在Windows 95和Windows NT
3.5加入。VFAT檔案系統可允許比傳統的8.3檔名，支援更長、而且能大小寫混合的長檔名（[LFN](../Page/VFAT.md "wikilink")）。

要維持對以前的應用程式（例如DOS和Windows
3.1）中的向下兼容性，每一個長檔名便會自動產生一個相應的8.3檔案名稱，可容許檔案可以繼續更名、刪除或開啟。

由於這對於一個長檔名沒有一個必然的[算法建立一個](../Page/算法.md "wikilink")8.3檔名，Windows會使用以下的常規以決定一個8.3檔案名稱是如何產生：

1.  如果長檔名是8.3大寫字母，在磁碟上不會儲存任何長檔名。
      - 例如："TEXTFILE.TXT"
2.  如果長檔名是8.3大小寫混合字母，那麼長檔名會儲存大小寫混合字母的檔名，在8.3的名稱會儲存它的大寫字母版本。
      - 例如："TextFile.Txt"會轉換成"TEXTFILE.TXT"。
3.  長檔名只會保留[基本名稱的首](../Page/基本名稱.md "wikilink")6位半形字元，以一個\~號連接著，再以一個數字作結尾以作識別，最後以副檔名的首3位字元作結束。從這個結果中再對無法使用的字元再作刪除，像（+）號會轉換成（_）號，另外這也會轉成全大寫字母。
      - 例如："TextFile1.Mine.txt"會轉換成"TEXTFI\~1.TXT"；"附屬應用程式"會轉換成"附屬應\~1"（或如果"TEXTFI\~1.TXT"已經存在的情況下，會儲存作"TEXTFI\~2.TXT"）。"ver
        +1.2.text"會轉換成"VER_12\~1.TEX"。
4.  從Windows
    2000開始，如果最少4個檔案或資料夾的短檔名的首6個字元是相同的話，該長檔名會另行將檔名轉作基本名稱的首2位字元（或如果基本名稱只有1個字元的話，便全取檔案名稱1個字元），再以4位[十六進位的檔案名稱的切細值連接上](../Page/十六進位.md "wikilink")，接著\~號，再接著一位的數字及`.`號，最後以首3位的副檔名作為結尾。結果，相比之中，這檔名是已經分拆及改用全大寫字母。
      - 例如："TextFile.Mine.txt"會轉換成"TE021F\~1.TXT"。

在Windows NT
family中的[NTFS檔案系統本身已經對長檔名作出支援](../Page/NTFS.md "wikilink")，但8.3的名稱仍然會保留，供舊有的應用程式使用。這可以選擇性地關上
這個功能以增加效能。

## 8.3的應用

在[ISO
9660檔案系統中](../Page/ISO_9660.md "wikilink")（多數使用於[CD光碟上](../Page/CD.md "wikilink")）在基本的等級1中也有相似的限制，在此再加上一些附加的限制，例如目錄名稱不能含有副檔名，另外檔案名稱不可含有一些半形的字元（通常是[连接号等的附號](../Page/连接号.md "wikilink")）。等級2的系統可以容許不多於31個字元的檔案名稱，這對於[Mac
OS檔案名稱的兼容性有所增加](../Page/Mac_OS.md "wikilink")。

有笑话称，在[微軟反壟斷案事件中](../Page/合众国诉微软案.md "wikilink")，MICROS\~1及MICROS\~2被用來代表微軟分割後可能出現的公司。\[1\]

## 参见

  - [文件系統](../Page/文件系統.md "wikilink")
  - [FAT](../Page/FAT.md "wikilink")

## 引用

[Category:磁盘文件系统](../Category/磁盘文件系统.md "wikilink")

1.  <http://www.netfunny.com/rhf/jokes/00/Jun/ms.html>