**大治**（1126年一月二十二日至1131年正月二十九日）是[日本的](../Page/日本.md "wikilink")[年號之一](../Page/年號.md "wikilink")。使用這個年號的[天皇是](../Page/天皇.md "wikilink")[崇德天皇](../Page/崇德天皇.md "wikilink")。

## 改元

  - 天治三年正月二十二日（1126年2月15日）改元大治

<!-- end list -->

  - 大治五年正月二十九日（1131年2月28日）改元天承

## 出處

## 出生

## 逝世

## 大事記

## 紀年、西曆、干支對照表

|                                |                                |                                |                                |                                |                                |                                |
| ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| 大治                             | 元年                             | 二年                             | 三年                             | 四年                             | 五年                             | 六年                             |
| [公元](../Page/公元.md "wikilink") | 1126年                          | 1127年                          | 1128年                          | 1129年                          | 1130年                          | 1131年                          |
| [干支](../Page/干支.md "wikilink") | [丙午](../Page/丙午.md "wikilink") | [丁未](../Page/丁未.md "wikilink") | [戊申](../Page/戊申.md "wikilink") | [己酉](../Page/己酉.md "wikilink") | [庚戌](../Page/庚戌.md "wikilink") | [辛亥](../Page/辛亥.md "wikilink") |

## 參考

  - 日語維基百科認爲出於《[河圖挺佐輔](../Page/河圖挺佐輔.md "wikilink")》：「黄帝修德立義，天下**大治**。」

  - 其他使用[大治為紀年的政權](../Page/大治.md "wikilink")。

  - [日本年號索引](../Page/日本年號索引.md "wikilink")

  - 同期存在的其他政權之紀年

      - [靖康](../Page/靖康.md "wikilink")（1126年正月至1127年四月）：[宋](../Page/北宋.md "wikilink")—[宋欽宗趙桓之年號](../Page/宋欽宗.md "wikilink")
      - [建炎](../Page/建炎.md "wikilink")（1127年五月至1130年十二月）：[宋](../Page/南宋.md "wikilink")—[宋高宗趙構之年號](../Page/宋高宗.md "wikilink")
      - [明受](../Page/明受.md "wikilink")（1129年三月至四月）：宋—元懿太子[趙敷之年號](../Page/趙敷.md "wikilink")
      - [紹興](../Page/紹興_\(宋高宗\).md "wikilink")（1131年正月至1162年十二月）：宋—宋高宗趙構之年號
      - [阜昌](../Page/阜昌.md "wikilink")（1130年十一月至1137年十一月）：宋時期—[劉豫之年號](../Page/劉豫.md "wikilink")
      - [天載](../Page/天載.md "wikilink")（1130年二月至三月）：南宋時期—[鍾相之年號](../Page/鍾相.md "wikilink")
      - [正法](../Page/正法.md "wikilink")：南宋時期—[李合戎](../Page/李合戎.md "wikilink")、[雷進之年號](../Page/雷進.md "wikilink")
      - [人知](../Page/人知.md "wikilink")：南宋時期—[雷進之年號](../Page/雷進.md "wikilink")
      - [太平](../Page/太平_\(李婆備\).md "wikilink")：南宋時期—[李婆備之年號](../Page/李婆備.md "wikilink")
      - [保天](../Page/保天.md "wikilink")（1129年起）：[大理](../Page/大理國.md "wikilink")—[段正嚴之年號](../Page/段正嚴.md "wikilink")
      - [廣運](../Page/廣運.md "wikilink")（至1147年）：大理—段正嚴之年號
      - [天符睿武](../Page/天符睿武.md "wikilink")（1120年至1126年）：[李朝](../Page/李朝_\(越南\).md "wikilink")—[李乾德之年號](../Page/李乾德.md "wikilink")
      - [天符慶壽](../Page/天符慶壽.md "wikilink")（1127年）：李朝—李乾德之年號
      - [天順](../Page/天順_\(李陽煥\).md "wikilink")（1128年至1132年）：李朝—[李陽煥之年號](../Page/李陽煥.md "wikilink")
      - [延慶](../Page/延慶_\(耶律大石\).md "wikilink")（1124年二月至1133年十二月）：[西遼](../Page/西遼.md "wikilink")—德宗[耶律大石之年號](../Page/耶律大石.md "wikilink")
      - [天會](../Page/天會_\(金太宗\).md "wikilink")（1123年九月至1137年十二月）：[金](../Page/金朝.md "wikilink")—[金太宗吳乞買](../Page/金太宗.md "wikilink")、金熙宗完颜亶之年號
      - [元德](../Page/元德.md "wikilink")（1119年正月至1127年三月）：[西夏](../Page/西夏.md "wikilink")—夏崇宗[李乾順之年號](../Page/李乾順.md "wikilink")
      - [正德](../Page/正德_\(李乾順\).md "wikilink")（1127年四月至1134年十二月）：西夏—夏崇宗李乾順之年號

## 參考文獻

  - 松橋達良，《元号はやわかり—東亜歷代建元考》，砂書房，1994年7月，ISBN 4915818276
  - 李崇智，《中国历代年号考》，中华书局，2001年1月ISBN 7101025129
  - 所功，《年号の歴史―元号制度の史的研究》，雄山閣，1988年3月，ISBN 4639007116

[Category:日本平安時代年號](../Category/日本平安時代年號.md "wikilink")
[Category:12世纪日本年号](../Category/12世纪日本年号.md "wikilink")
[Category:1120年代日本](../Category/1120年代日本.md "wikilink")
[Category:1130年代日本](../Category/1130年代日本.md "wikilink")