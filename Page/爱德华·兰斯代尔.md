**爱德华·兰斯代尔**（Edward Geary
Lansdale，）是一位[美国空军军官](../Page/美国空军.md "wikilink")，任职于[戰略情報局和](../Page/戰略情報局.md "wikilink")[中央情报局](../Page/中央情报局.md "wikilink")。军阶升至[少将](../Page/少将.md "wikilink")，1963年获得服役优异勋章。他是美国在冷战中采取强硬行动的早期建议者。他出生于[密歇根州](../Page/密歇根州.md "wikilink")[底特律](../Page/底特律.md "wikilink")，去世于弗吉尼亚州McLean，埋葬在[阿灵顿国家公墓](../Page/阿灵顿国家公墓.md "wikilink")。他曾2次结婚，與第一任妻子生了2个儿子。北越王牌間諜[范春安曾在蘭斯代爾手下受訓](../Page/范春安.md "wikilink")。

## 参考文献

## 外部链接

  - [Official Air Force
    Biography](https://web.archive.org/web/20040322070329/http://www.af.mil/bios/bio_print.asp?bioID=6141&page=1)
  - [Unofficial
    Biography](https://web.archive.org/web/20080509145224/http://www.spartacus.schoolnet.co.uk/COLDlansdale.htm)

[Category:美國空軍少將](../Category/美國空軍少將.md "wikilink")
[Category:美國中央情報局人物](../Category/美國中央情報局人物.md "wikilink")