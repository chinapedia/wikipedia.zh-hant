香港山地多，屬[亞熱帶氣候](../Page/亞熱帶氣候.md "wikilink")，年均[降雨量達](../Page/降雨量.md "wikilink")2,200毫米，所以香港的[河網密度稱得上是高](../Page/河.md "wikilink")，但在[季候風的影響下](../Page/季候風.md "wikilink")，[降雨有明顯季節性](../Page/降雨.md "wikilink")，引致香港河流上游河段大多數是[間歇河](../Page/間歇河.md "wikilink")。除了少數較大型河流外，下游的[泛濫平原都十分細小](../Page/泛濫平原.md "wikilink")。香港面積本來就不大，所以欠缺真正的大河流，嚴格來說，境內全是[溪澗](../Page/溪澗.md "wikilink")，不足以稱為江河。而較大的河流都集在香港西北部，如[山貝河](../Page/山貝河.md "wikilink")、[深圳河](../Page/深圳河.md "wikilink")、[錦田河](../Page/錦田河.md "wikilink")、[雙魚河](../Page/雙魚河.md "wikilink")、[梧桐河等](../Page/梧桐河.md "wikilink")。以長度來說，香港最長的河流是[深圳河](../Page/深圳河.md "wikilink")，全長37公里。

以下是主要的**[香港](../Page/香港.md "wikilink")[河流](../Page/河流.md "wikilink")**、溪澗、[明渠及排水道的列表](../Page/明渠.md "wikilink")：

## 主要河流及溪澗

### 新界東

  - 流入[深圳河](../Page/深圳河.md "wikilink")

<!-- end list -->

  - [梧桐河](../Page/梧桐河.md "wikilink")（上游支流：[麻笏河](../Page/麻笏河.md "wikilink")、[軍地河](../Page/軍地河.md "wikilink")、[丹山河](../Page/丹山河.md "wikilink")）
  - [雙魚河](../Page/雙魚河.md "wikilink")（支流：[古洞河](../Page/古洞河_\(香港\).md "wikilink")）
  - [石上河](../Page/石上河.md "wikilink")
  - [平原河](../Page/平原河.md "wikilink")

<!-- end list -->

  - 流入[沙頭角海](../Page/沙頭角海.md "wikilink")

<!-- end list -->

  - [沙頭角河](../Page/沙頭角河.md "wikilink")
  - [南涌河](../Page/南涌河.md "wikilink")

<!-- end list -->

  - 流入[印洲塘一帶](../Page/印洲塘.md "wikilink")

<!-- end list -->

  - [馬尿河](../Page/馬尿河.md "wikilink")
  - [黃竹涌](../Page/黃竹涌.md "wikilink")
  - [東灣坑](../Page/東灣坑.md "wikilink")

<!-- end list -->

  - 流入[吐露港或](../Page/吐露港.md "wikilink")[沙田海](../Page/沙田海.md "wikilink")

<!-- end list -->

  - [城門河](../Page/城門河.md "wikilink")（支流：[火炭渠](../Page/火炭渠.md "wikilink")、[小瀝源渠](../Page/小瀝源渠.md "wikilink")、[馬麗口坑](../Page/馬麗口坑.md "wikilink")）
  - [林村河](../Page/林村河.md "wikilink")（支流：[社山河](../Page/社山河.md "wikilink")）
  - [大埔河](../Page/大埔河.md "wikilink")
  - [大埔滘溪](../Page/大埔滘溪.md "wikilink")
  - [山寮溪](../Page/山寮溪.md "wikilink")
  - [洞梓溪](../Page/洞梓溪.md "wikilink")
  - [三潭溪](../Page/三潭溪.md "wikilink")
  - [九肚坑](../Page/九肚坑.md "wikilink")
  - [大水坑](../Page/大水坑.md "wikilink")
  - [坑槽水](../Page/坑槽水.md "wikilink")
  - [白沙澳河](../Page/白沙澳河.md "wikilink")

<!-- end list -->

  - 流入[城門水塘](../Page/城門水塘.md "wikilink")

<!-- end list -->

  - [大城石澗](../Page/大城石澗.md "wikilink")

<!-- end list -->

  - 流入[海下灣](../Page/海下灣.md "wikilink")

<!-- end list -->

  - [蠔涌河](../Page/蠔涌河.md "wikilink")
  - [沙角尾溪](../Page/沙角尾溪.md "wikilink")
  - [大涌口溪](../Page/大涌口溪.md "wikilink")
  - [蛇地坑](../Page/蛇地坑.md "wikilink")
  - [龍坑](../Page/龍坑.md "wikilink")
  - [馬尿河](../Page/馬尿河.md "wikilink")
  - [黃竹涌](../Page/黃竹涌.md "wikilink")
  - [牛角涌](../Page/牛角涌.md "wikilink")

<!-- end list -->

  - 流入[西貢](../Page/西貢.md "wikilink")[高塘口](../Page/高塘口.md "wikilink")

<!-- end list -->

  - [猴塘溪](../Page/猴塘溪.md "wikilink")

<!-- end list -->

  - 流入[西貢](../Page/西貢.md "wikilink")[大浪灣](../Page/大浪灣_\(西貢\).md "wikilink")

<!-- end list -->

  - [夾萬坑](../Page/夾萬坑.md "wikilink")

<!-- end list -->

  - 流入[將軍澳灣](../Page/將軍澳灣.md "wikilink")

<!-- end list -->

  - [井欄樹溪](../Page/井欄樹溪.md "wikilink")

### 新界西

  - [元朗區](../Page/元朗區.md "wikilink")

<!-- end list -->

  - [元朗河](../Page/元朗河.md "wikilink")
  - [錦田河](../Page/錦田河.md "wikilink")
  - [山貝河](../Page/山貝河.md "wikilink")

<!-- end list -->

  - [流浮山及](../Page/流浮山.md "wikilink")[后海灣一帶](../Page/后海灣.md "wikilink")

<!-- end list -->

  - [鰲磡沙溪](../Page/鰲磡沙溪.md "wikilink")
  - [上白泥溪](../Page/上白泥溪.md "wikilink")
  - [下白泥溪](../Page/下白泥溪.md "wikilink")
  - [白泥溪](../Page/白泥溪.md "wikilink")
  - [曾角溪](../Page/曾角溪.md "wikilink")
  - [大水坑溪](../Page/大水坑溪.md "wikilink")
  - [大冷溪](../Page/大冷溪.md "wikilink")
  - [小冷溪](../Page/小冷溪.md "wikilink")
  - [望后溪](../Page/望后溪.md "wikilink")

<!-- end list -->

  - [屯門區](../Page/屯門區.md "wikilink")

<!-- end list -->

  - [屯門河](../Page/屯門河.md "wikilink")（流入[青山灣](../Page/青山灣.md "wikilink")）
  - [大欖涌](../Page/大欖涌.md "wikilink")

<!-- end list -->

  - [荃灣區](../Page/荃灣區.md "wikilink")

<!-- end list -->

  - [曹公潭溪](../Page/曹公潭.md "wikilink")（流入[藍巴勒海峽](../Page/藍巴勒海峽.md "wikilink")）
  - [排棉角溪](../Page/排棉角溪.md "wikilink")
  - [三叠潭溪](../Page/三叠潭溪.md "wikilink")

<!-- end list -->

  - [葵青區](../Page/葵青區.md "wikilink")

<!-- end list -->

  - [九華徑溪](../Page/九華徑溪.md "wikilink")

### [大嶼山](../Page/大嶼山.md "wikilink")

[Wong_Lung_Hang_nullah_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Wong_Lung_Hang_nullah_\(Hong_Kong\).jpg "fig:Wong_Lung_Hang_nullah_(Hong_Kong).jpg")一段。\]\]

  - [梅窩河](../Page/梅窩河.md "wikilink")
  - [東涌河](../Page/東涌河.md "wikilink")
  - [黃龍坑](../Page/黃龍坑.md "wikilink")
  - [大澳涌](../Page/大澳涌.md "wikilink")
  - [銀河](../Page/銀河_\(香港\).md "wikilink")
  - [橫塘河](../Page/橫塘河.md "wikilink")

### 九龍

  - [鳳凰溪](../Page/鳳凰溪.md "wikilink")

### 新界其他地方

  - [深圳與香港邊界](../Page/深圳.md "wikilink")

<!-- end list -->

  - [深圳河](../Page/深圳河.md "wikilink")
  - [沙頭角河](../Page/沙頭角河.md "wikilink")

## 主要明渠

  - [黃竹坑明渠](../Page/黃竹坑明渠.md "wikilink")
  - [啟德明渠](../Page/啟德明渠.md "wikilink")（[啟德河](../Page/啟德河.md "wikilink")）
  - [火炭渠](../Page/火炭渠.md "wikilink")
  - [小瀝源渠](../Page/小瀝源渠.md "wikilink")
  - [天水圍渠](../Page/天水圍渠.md "wikilink")
  - [錦繡花園明渠](../Page/錦繡花園明渠.md "wikilink")
  - [廈村明渠](../Page/廈村明渠.md "wikilink")
  - [福民路明渠](../Page/福民路明渠.md "wikilink")
  - [元朗明渠](../Page/元朗明渠.md "wikilink")（[山貝河下游](../Page/山貝河.md "wikilink")）

## 主要排水道

  - [新田東排水道](../Page/新田東排水道.md "wikilink")
  - [元朗排水繞道](../Page/元朗排水繞道.md "wikilink")
  - [元朗錦田排水道](../Page/元朗錦田排水道.md "wikilink")
  - [深涌排水道](../Page/深涌排水道.md "wikilink")
  - [田村排水道](../Page/田村排水道.md "wikilink")
  - [牛潭尾主要排水道](../Page/牛潭尾主要排水道.md "wikilink")

## 參見

  - [香港已消失河流](../Page/香港已消失河流.md "wikilink")
  - [香港山澗](../Page/香港山澗.md "wikilink")

## 参考文献

<div class="references-small">

<references />

  - [香港地方 - 香港河流](http://www.hk-place.com/view.php?id=120)
  - [綠色力量 -
    從「河」而來教育計劃：本地河溪](https://web.archive.org/web/20071129234120/http://www.greenpower.org.hk/new/river/hkrivers.html)
  - [GovHK 香港政府一站通 -
    河溪水質](http://www.gov.hk/tc/residents/environment/water/riverwater.htm)
  - [環境保護署 -
    河溪水質](https://web.archive.org/web/20071213152808/http://www.epd.gov.hk/epd/tc_chi/environmentinhk/water/river_quality/rwq_home.html)
  - [環境保護署 -
    香港河溪水質監測20年](http://www.epd.gov.hk/epd/misc/river_quality/1986-2005/)
  - [網絡備忘 -
    全港10大最靚河溪](https://web.archive.org/web/20070129223130/http://vn2004memo.netfirms.com/040322ap_m02.html)

</div>

{{-}}

[香港河流](../Category/香港河流.md "wikilink")
[河流](../Category/香港地理相關列表.md "wikilink")
[Category:中国河流列表](../Category/中国河流列表.md "wikilink")