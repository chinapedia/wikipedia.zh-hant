**新軍**全稱「**新建陸軍**」，是[清朝政府於](../Page/清朝.md "wikilink")[甲午戰爭之後編練的新式](../Page/甲午戰爭.md "wikilink")[陸軍軍隊](../Page/陸軍.md "wikilink")。這支軍隊的特色是完全使用西式的[軍事制度](../Page/軍事制度.md "wikilink")、訓練以及裝備，是清朝最後一支有戰鬥力的[正規軍](../Page/正規軍.md "wikilink")。

## 成立經過

[YuanShika_Colour.jpg](https://zh.wikipedia.org/wiki/File:YuanShika_Colour.jpg "fig:YuanShika_Colour.jpg")
[Yuan_shikai.jpg](https://zh.wikipedia.org/wiki/File:Yuan_shikai.jpg "fig:Yuan_shikai.jpg")
1894年（光緒二十年）[中日甲午戰爭爆發後](../Page/中日甲午戰爭.md "wikilink")，原[廣西](../Page/廣西.md "wikilink")[按察使](../Page/按察使.md "wikilink")[胡燏棻奉清政府之命](../Page/胡燏棻.md "wikilink")，在[天津用西法編練十](../Page/天津.md "wikilink")[營](../Page/營.md "wikilink")[定武軍](../Page/定武軍.md "wikilink")，分別為[步兵三千人](../Page/步兵.md "wikilink")、[炮兵一千人](../Page/炮兵.md "wikilink")、[馬兵二百五十人](../Page/騎兵.md "wikilink")、和[工兵五百人](../Page/工兵.md "wikilink")，一共四千七百多人。次年胡燏棻調任[平漢鐵路督辦](../Page/平漢鐵路.md "wikilink")。

[袁世凱接手](../Page/袁世凱.md "wikilink")「定武軍」，在[天津小站訓練部隊](../Page/天津小站.md "wikilink")（人稱「小站練兵」），改為「**新建陸軍**」，一切依照[德國與](../Page/德意志帝國.md "wikilink")[日本的制度](../Page/大日本帝國.md "wikilink")，由德國人為主的洋人監督，更把規模擴至七千士兵，参照[德国陸军制度进行编制](../Page/德意志帝國陸軍.md "wikilink")，并分立[警](../Page/憲兵.md "wikilink")、步、馬、砲、工、[辎等](../Page/軍需.md "wikilink")[兵科](../Page/兵科.md "wikilink")。之後，被编为武卫军“前后左右中”五路中的武衛右軍，跟隨袁世凱到[山東去鎮壓](../Page/山東.md "wikilink")[義和團運動](../Page/義和團運動.md "wikilink")。在那裏，袁世凱將三十四營舊軍改編，命名為「武衛右軍先鋒隊」，令武衛右軍擴大至兩萬餘人。

在新建陸軍成立的同時，署理[兩江總督的](../Page/兩江總督.md "wikilink")[張之洞也編練了一支名叫](../Page/張之洞.md "wikilink")[自強軍的新軍](../Page/自強軍.md "wikilink")。這支軍隊也依照西式軍隊編制步、馬、砲、工等13營，共二千餘人。後來自強軍由[劉坤一接辦](../Page/劉坤一.md "wikilink")，到最後被袁世凱所收編，劃歸其武衛右軍。

[庚子事变中](../Page/庚子事变.md "wikilink")，武卫军诸军损失惨重，惟袁世凯所部右军及右军先锋队得以保存实力，成为清朝北方仅剩的新军武力，1902年改名常备军，[庚子新政中加入全国新军改编](../Page/庚子新政.md "wikilink")。

1905年2月28日，袁世凯等奏请统一全国新军[番号](../Page/番号.md "wikilink")，提出“以陆军编号通国一贯，脉络相连”，并呈请将“所有常备军各镇拟即一律改为陆军各镇，以符名实而遵定制”。即全国各镇统一数字番号。新的陆军营制在北洋常备军营制的基础上略加变化而来，一镇计步、马、炮、工、辎共二十营，与日本师团的组织相似。第一批即以北洋、京旗两支常备军改编为北洋六镇。

1907年陆军部拟订了全国编练三十六镇的计划。

1911年清亡时，共计成立十四个镇、十六个混成协、禁卫军2协，以及几个独立标、营。

新军采取的是[行省](../Page/行省.md "wikilink")（[城市](../Page/城市.md "wikilink")）加上[番号的方式](../Page/番号.md "wikilink")，如第一镇是“近畿陆军第一镇”，第八镇是“湖北陆军第八镇”（不是湖北地方部队，仍属中央国防军系列）等。

混成协在中国不算是战时编制。因为编练新军对于各省来说，财政花费实在太大，因此从河南新军开始，采取了变通办法，先成立混成一协，以后再扩为一镇。这个办法后在全国各地推广，于是出现了一批混成协。通常混成协按清廷定制，一般是步队一协、马炮各一营、工辎各一队，实际情况各省略有差别，不完全一致。

各省各自编练新军达到协的标准，呈请练兵处（后为陆军部）批准成协，如果审批通过则授予无数字番号的混成协（如“陕西陆军混成协”）或有数字番号暂编混成协（如“四川暂编陆军第三十三混成协”）。一部分混成协是以上的两种番号都经历过，如河南先成“河南陆军混成协”，再改称“河南暂编陆军第二十九混成协”。大多数混成协是准备将来扩编为镇的，所以都加“暂编”二字，只有从北洋六镇中划出驻守东北的陆军第一、二混成协不在此例。

各省编炼新军达到一镇的标准，报陆军部审核后授予暂编镇番号，如“四川暂编陆军第十七镇”。陆军部派员亲临校阅，若合格则去掉“暂编”二字，成为正规的陆军镇。至清亡，只有北洋六镇和湖北第八镇、南洋第九镇等几个镇是通过校阅的正规镇，其余各镇全属“暂编”镇。

## 北洋新軍

[缩略图](https://zh.wikipedia.org/wiki/File:Chinese_Army_manoeuvres.jpg "fig:缩略图")
[Beiyang_Army.jpg](https://zh.wikipedia.org/wiki/File:Beiyang_Army.jpg "fig:Beiyang_Army.jpg")
[北洋新军的正式名字叫做](../Page/北洋新军.md "wikilink")“新建陸軍”，原名“定武軍”。袁世凱為新建陸軍督辦。主要軍官由袁世凱親友、[北洋武备学堂畢業生和](../Page/北洋武备学堂.md "wikilink")[淮軍舊部組成](../Page/淮軍.md "wikilink")。编制机构及其人员：

  - 参谋营务处总办：[徐世昌](../Page/徐世昌.md "wikilink")，[河南](../Page/河南.md "wikilink")[卫辉人](../Page/卫辉.md "wikilink")
      - 参谋营务处及兵官学堂监督：[江朝宗](../Page/江朝宗.md "wikilink")，[安徽](../Page/安徽.md "wikilink")[旌德人](../Page/旌德.md "wikilink")

<!-- end list -->

  - 督操营务处总办兼步兵学堂监督：[冯国璋](../Page/冯国璋.md "wikilink")，直隶[河间人](../Page/河间.md "wikilink")
      - 督操营务处提调：[段芝贵](../Page/段芝贵.md "wikilink")，安徽[合肥人](../Page/合肥.md "wikilink")
      - 步队左翼翼长兼第一营统带：[姜桂题](../Page/姜桂题.md "wikilink")，安徽[亳州人](../Page/亳州.md "wikilink")
      - 步队右翼第二营统带：[吴长纯](../Page/吴长纯.md "wikilink")，安徽[庐江人](../Page/庐江.md "wikilink")
      - 步队右翼第三营后队：[雷震春](../Page/雷震春.md "wikilink")，安徽[宿州人](../Page/宿州.md "wikilink")
      - 马队第一营后队队官：[吴凤岭](../Page/吴凤岭.md "wikilink")，袁世凯家佣人之子，从小在袁家长大

<!-- end list -->

  - 炮兵营统带兼炮兵学堂监督：[段祺瑞](../Page/段祺瑞.md "wikilink")，安徽合肥人
      - 炮队第一营管带：[张怀芝](../Page/张怀芝.md "wikilink")，[山东](../Page/山东.md "wikilink")[东阿人](../Page/东阿.md "wikilink")

<!-- end list -->

  - 工程营统带兼讲武堂总教习：[王士珍](../Page/王士珍.md "wikilink")，直隶正定人
  - 工程营统带兼行营中军：[张勋](../Page/张勋.md "wikilink")，[江西](../Page/江西.md "wikilink")[奉新人](../Page/奉新.md "wikilink")

<!-- end list -->

  - 学兵营统带兼督操营务处提调：[曹锟](../Page/曹锟.md "wikilink")，直隶天津人
      - 基层军官：[王英楷](../Page/王英楷.md "wikilink")、[张永成](../Page/张永成.md "wikilink")、[赵国贤](../Page/赵国贤.md "wikilink")、[何宗莲](../Page/何宗莲.md "wikilink")、[李纯](../Page/李纯.md "wikilink")、[陈光远](../Page/陈光远.md "wikilink")、[李厚基](../Page/李厚基.md "wikilink")、[王占元](../Page/王占元.md "wikilink")、[卢永祥](../Page/卢永祥.md "wikilink")、[王汝贤](../Page/王汝贤.md "wikilink")、[杨善德](../Page/杨善德.md "wikilink")、[田中玉](../Page/田中玉.md "wikilink")、[鲍贵卿](../Page/鲍贵卿.md "wikilink")、[陆建章](../Page/陆建章.md "wikilink")、[田中玉](../Page/田中玉.md "wikilink")、[钟麟同](../Page/钟麟同.md "wikilink")、[马龙标](../Page/马龙标.md "wikilink")、[孟恩远等](../Page/孟恩远.md "wikilink")。

戊戌变法前夕新建陆军被编为武卫右军，跟武卫左、前、后、中，通于归属总督[荣禄管辖](../Page/荣禄.md "wikilink")。

  - 第一镇统制先后为[鳳山](../Page/鳳山_\(清朝人物\).md "wikilink")、何宗莲；
  - 第二镇统制先后为王英楷、张怀芝；
  - 第三镇统制先后为段祺瑞、曹锟；
  - 第四镇统制为吴凤岭；
  - 第五镇统制先后为吴长纯、张永成；
  - 第六镇统制先后为王士珍、赵国贤、段祺瑞。

1907年，从六镇各抽调一部分官兵，编成第二十镇。辛亥革命时期，[冯玉祥任第](../Page/冯玉祥.md "wikilink")40协第80标第三营管带。

1909年，袁世凯被开缺回乡。北洋六镇高层，有所调整。

  - 第二镇统制：[马龙标](../Page/马龙标.md "wikilink")
  - 第五镇统制：
  - 第六镇统制：[吴禄贞](../Page/吴禄贞.md "wikilink")

## 新政中的军事改革

[宗社党.jpg](https://zh.wikipedia.org/wiki/File:宗社党.jpg "fig:宗社党.jpg")等校阅[新建陆军](../Page/新建陆军.md "wikilink")。图为校阅陆军大臣1911年1月的合影。本图经裁切曾以《海陆军中之重要人物》为名刊登在《[东方杂志](../Page/东方杂志.md "wikilink")》1911年8卷8期，图下注左起：陆军大臣[廕昌](../Page/廕昌.md "wikilink")、[载振](../Page/载振.md "wikilink")、海军大臣[载洵](../Page/载洵.md "wikilink")、贵胄陆军学堂总理[载润](../Page/载润.md "wikilink")、禁卫军统领[载涛](../Page/载涛.md "wikilink")、[麟光](../Page/麟光.md "wikilink")、[蒙古三帕](../Page/帕勒塔.md "wikilink")、海军副大臣[谭学衡](../Page/谭学衡.md "wikilink")\]\]
[校阅陆军.jpg](https://zh.wikipedia.org/wiki/File:校阅陆军.jpg "fig:校阅陆军.jpg")等校阅[新建陆军](../Page/新建陆军.md "wikilink")。图为清政府高级官员及校阅陆军大臣1911年1月的合影。本图经裁切曾以《近时王公大臣之合影》为名刊登在《[东方杂志](../Page/东方杂志.md "wikilink")》1911年8卷7期。图中：
前排自左至右：多罗都楞郡王[贡桑诺尔布](../Page/贡桑诺尔布.md "wikilink")，1军谘大臣贝勒[载涛](../Page/载涛.md "wikilink")，未详，未详，2军谘府大臣贝勒[毓朗](../Page/毓朗.md "wikilink")，和硕亲王[那彦图](../Page/那彦图.md "wikilink")，3理藩大臣[肃亲王](../Page/肃亲王.md "wikilink")[善耆](../Page/善耆.md "wikilink")，度支大臣[载泽](../Page/载泽.md "wikilink")，6海军大臣贝勒[载洵](../Page/载洵.md "wikilink")，未详，7内阁协理大臣[那桐](../Page/那桐.md "wikilink")，8内阁协理大臣[徐世昌](../Page/徐世昌.md "wikilink")，邮传大臣[盛宣怀](../Page/盛宣怀.md "wikilink")，9署理外务大臣、弼德院副院长[邹嘉来](../Page/邹嘉来.md "wikilink")，未详。
后排自左至右：[帕勒塔](../Page/帕勒塔.md "wikilink")，未详，[那亲王世子](../Page/绍曾.md "wikilink")，[麟光](../Page/麟光.md "wikilink")，4[载振](../Page/载振.md "wikilink")，5陆军大臣[荫昌](../Page/荫昌.md "wikilink")，陆军副大臣[寿勋](../Page/寿勋.md "wikilink")，海军副大臣[谭学衡](../Page/谭学衡.md "wikilink")，正黄旗汉军都统[载润](../Page/载润.md "wikilink")，未详，10未详\]\]

清廷簽訂《[辛丑條約](../Page/辛丑條約.md "wikilink")》後，[慈禧決心推行新政](../Page/慈禧.md "wikilink")，進行[軍事改革](../Page/晚清军事改革.md "wikilink")。[光緒二十九年十月成立總理練兵處](../Page/光緒二十九年.md "wikilink")，並令各省成立督練公所，負責訓練新軍；並且裁減原有的舊軍（[防軍](../Page/防軍.md "wikilink")、[練軍](../Page/練軍.md "wikilink")、[綠營](../Page/綠營.md "wikilink")），剩下的精選若干營為常備、[後備軍及巡警營](../Page/後備軍.md "wikilink")（即[憲兵](../Page/憲兵.md "wikilink")）。清政府原訂計畫以北洋新軍為中央軍、各省的新軍為地方軍，一共在全國編練三十六鎮新軍；到清朝覆亡的時候，全國已練成新軍十六鎮和十六個混成協（一說為十四個鎮﹑十八個混成協），其中裝備與訓練為袁世凱的北洋六鎮為最佳，遍佈[直隸](../Page/直隸.md "wikilink")、山東與[东北](../Page/中国东北地区.md "wikilink")。

  - [近畿陆军第一鎮](../Page/陆军第一鎮.md "wikilink")：駐[北京](../Page/北京.md "wikilink")[仰山洼](../Page/仰山洼.md "wikilink")
  - [直隶陆军第二鎮](../Page/陆军第二镇.md "wikilink")：駐[保定](../Page/保定.md "wikilink")、[永平等府](../Page/永平.md "wikilink")
  - [近畿陆军第三鎮](../Page/陆军第三镇.md "wikilink")：駐吉林[吉林](../Page/吉林.md "wikilink")、[长春](../Page/长春.md "wikilink")、[宁安](../Page/宁安.md "wikilink")、[延吉及](../Page/延吉.md "wikilink")[奉天](../Page/奉天.md "wikilink")[锦州等处](../Page/锦州.md "wikilink")
  - [直隶陆军第四鎮](../Page/陆军第四镇.md "wikilink")：駐[天津](../Page/天津.md "wikilink")[馬廠](../Page/馬廠.md "wikilink")
  - [近畿陆军第五鎮](../Page/陆军第五镇.md "wikilink")：駐[济南](../Page/济南.md "wikilink")、[濰坊](../Page/濰坊.md "wikilink")、[昌邑等处](../Page/昌邑.md "wikilink")
  - [近畿陆军第六鎮](../Page/陆军第六镇.md "wikilink")：駐[北京](../Page/北京.md "wikilink")[南苑](../Page/南苑.md "wikilink")
  - [河南暂编陆军第七镇](../Page/陆军第七镇.md "wikilink")：驻防河南
  - [湖北陆军第八镇](../Page/陆军第八鎮.md "wikilink")：駐[武昌](../Page/武昌.md "wikilink")
  - [南洋陆军第九鎮](../Page/陆军第九镇.md "wikilink")：駐[江宁](../Page/江宁.md "wikilink")
  - [福建暂编陆军第十鎮](../Page/陆军第十镇.md "wikilink")：駐[福州](../Page/福州.md "wikilink")、[福宁](../Page/福宁.md "wikilink")、[延平等处](../Page/延平區.md "wikilink")
  - [四川暂编陆军第十七鎮](../Page/陆军第十七镇.md "wikilink")：駐[成都](../Page/成都.md "wikilink")
  - [云南暂编陆军第十九鎮](../Page/陆军第十九鎮.md "wikilink")：駐[昆明](../Page/昆明.md "wikilink")
  - [奉天暂编陆军第二十鎮](../Page/陆军第二十镇.md "wikilink")：駐[瀋陽](../Page/瀋陽.md "wikilink")
  - [浙江暂编陆军第二十一镇](../Page/陆军第二十一镇.md "wikilink")：驻[浙江](../Page/浙江.md "wikilink")
  - [吉林暂编陆军第二十三镇](../Page/陆军第二十三镇.md "wikilink")：驻[吉林](../Page/吉林.md "wikilink")
  - [广西暂编陆军第二十五镇](../Page/陆军第二十五镇.md "wikilink")：原驻[广西](../Page/广西.md "wikilink")[桂林](../Page/桂林.md "wikilink")，后因第二十六镇被遣散迁驻[广东](../Page/广东.md "wikilink")，改称广东陆军第二十五镇
  - [广东暂编陆军第二十六镇](../Page/陆军第二十六镇.md "wikilink")：驻[广州](../Page/广州.md "wikilink")，后因起义被遣散

<!-- end list -->

  - [第一混成协](../Page/陆军第一混成协.md "wikilink")：驻[东北](../Page/中国东北地区.md "wikilink")[新民](../Page/新民.md "wikilink")，协统[王化东](../Page/王化东.md "wikilink")
  - [第二混成协](../Page/陆军第二混成协.md "wikilink")：驻[沈阳](../Page/沈阳.md "wikilink")，协统[蓝天蔚](../Page/蓝天蔚.md "wikilink")
  - [第三混成协](../Page/陆军第三混成协.md "wikilink")：驻[永平](../Page/永平.md "wikilink")，协统[王占元](../Page/王占元.md "wikilink")
  - [第十三混成协](../Page/陆军第十三混成协.md "wikilink")：驻江苏江北，协统[杨保善](../Page/杨保善.md "wikilink")
  - [第二十一混成协](../Page/陆军第二十一混成协.md "wikilink")：驻武昌，协统[黎元洪](../Page/黎元洪.md "wikilink")
  - [第二十三混成协](../Page/陆军第二十三混成协.md "wikilink")：驻江苏[苏州](../Page/苏州.md "wikilink")
  - [第二十五混成协](../Page/陆军第二十五混成协.md "wikilink")：驻[湖南](../Page/湖南.md "wikilink")[长沙](../Page/长沙.md "wikilink")
  - [第二十七混成协](../Page/陆军第二十七混成协.md "wikilink")：驻[江西](../Page/江西.md "wikilink")[南昌](../Page/南昌.md "wikilink")
  - [第二十九混成协](../Page/陆军第二十九混成协.md "wikilink")：驻[河南](../Page/河南.md "wikilink")，协统[张锡元](../Page/张锡元.md "wikilink")
  - [第三十一混成协](../Page/陆军第三十一混成协.md "wikilink")：驻[安徽](../Page/安徽.md "wikilink")[安庆](../Page/安庆.md "wikilink")，协统[余大鸿](../Page/余大鸿.md "wikilink")
  - [第三十三混成协](../Page/陆军第三十三混成协.md "wikilink")：驻[四川](../Page/四川.md "wikilink")[成都](../Page/成都.md "wikilink")，协统[钟颖](../Page/钟颖.md "wikilink")
  - [第三十五混成协](../Page/陆军第三十五混成协.md "wikilink")：驻[新疆](../Page/新疆.md "wikilink")[乌鲁木齐](../Page/乌鲁木齐.md "wikilink")
  - [第三十九混成协](../Page/陆军第三十九混成协.md "wikilink")：驻[陕西](../Page/陕西.md "wikilink")[西安](../Page/西安.md "wikilink")
  - [第四十三混成协](../Page/陆军第四十三混成协.md "wikilink")：驻[山西](../Page/山西.md "wikilink")，协统[谭振德](../Page/谭振德.md "wikilink")
  - [广东混成协](../Page/广东混成协.md "wikilink")
  - [广西混成协](../Page/广西混成协.md "wikilink")
  - [甘肃混成协](../Page/甘肃混成协.md "wikilink")
  - [伊犁混成协](../Page/伊犁混成协.md "wikilink")
  - [直隶混成协](../Page/直隶混成协.md "wikilink")
  - [黑龙江混成协](../Page/黑龙江混成协.md "wikilink")

## 編制

按《陆军营制饷章》规定，国家常备军的编制以两镇为一军，两协为一镇（一镇官兵12512人）、两标为一协（一协官兵4038人），每标三营，每营四队。每镇还辖炮队一标（官兵1756名）、马队一标
（官兵1117名）、1个辎重营（官兵754人）、1个工程营
（官兵667人）。步、炮、工兵每队均为三排，每排三棚；马队两排，每排二棚；辎重队三排，每排三棚。常备军的编制与中国传统营制已完全不同，成为一个步、骑、炮、工、辎各兵种协同作战的单位，这是一种适合形势需要的现代化编制。

建軍初期，全軍由一名總統（即袁世凱）統率，有左右兩翼（相當於[旅](../Page/旅.md "wikilink")），每翼有一名翼長、一名統領和兩名分統負責；翼下面為營，有統帶官和幫統官各一名；營下面為隊（相當於[連](../Page/連.md "wikilink")），由一名領官管理；隊下面為哨（相當於[排](../Page/排.md "wikilink")），由一名哨官和兩名哨長負責；最下面為棚（相當於[班](../Page/班.md "wikilink")），有正副頭目各一名。（每棚十二人）

新軍機關以總部為首，其下有參謀營務處、執法營務處、督操營務處、稽查營務處等。

清政府推行「新政」之後，新軍改為常備軍制，並因人數的增加，故以改編作鎮、協、標、營、隊、排、棚，各級軍官改稱統制﹑協統﹑標統﹑管帶﹑隊官﹑排長和正副目。每鎮額設官兵一萬二千五百一十二人，仍有步、馬、炮、工程、輜重等兵種。軍官大多由學習軍事的留學生和國內各武備學堂的畢業生擔任，士兵則採取招募制，選拔標準較以往嚴格，對年齡、體格和文化程度等都有明文規定。

## 新軍與革命

在组建和训练新军的时候，一些开明地方大员如张之洞、端方等，注重招收有文化的青年入伍，和招收留学生任军官如[蔡锷](../Page/蔡锷.md "wikilink")、[阎锡山](../Page/阎锡山.md "wikilink")、[许崇智等](../Page/许崇智.md "wikilink")，这些知识青年读过书见识广，敢于持有不同政见，不同于旧军官兵愚昧效忠皇权。同时革命党人如黄兴、宋教仁等注重开展兵运，秘密向新军中输送革命分子，如[吴禄贞](../Page/吴禄贞.md "wikilink")、[赵声](../Page/赵声.md "wikilink")、[孙武等](../Page/孙武.md "wikilink")。这些军内革命党人待命伺机发动起义。

1911年武昌首义是新军起义，然后响应的陕西、湖南二省，是新军起义。接下来江西、安徽、江苏、浙江、福建、山西、四川、云南、贵州等地也是新军起义。除上海、广东、广西辛亥革命起义是以民军商团为主之外，其他各地起义均为新军起义为主。辛亥革命也可以说是一场规模空前的新军大起义。

## 相关条目

  - [中德合作
    (1911年-1941年)](../Page/中德合作_\(1911年-1941年\)#早期中德關係.md "wikilink")
  - [辛亥革命](../Page/辛亥革命.md "wikilink")
  - [定武軍](../Page/定武軍.md "wikilink")

## 參考文献

  - [軍諮處奏定陸軍人員補官暫行章程](../Page/軍諮處.md "wikilink")，[宣統元年九月](../Page/宣統.md "wikilink")

  -
  -
## 研究書目

  - 馮兆基著，郭太風譯：《軍事近代化與中國革命》（上海：上海人民出版社，1994）。

## 外部連結

  - [刘庆　魏鸿文，清末陆军编制发展的思路浅析](https://web.archive.org/web/20150923202835/http://www.china001.com/show_hdr.php?xname=PPDDMV0&dname=J68N641&xpos=45)

{{-}}

[新建陸軍](../Category/新建陸軍.md "wikilink")
[Category:清朝政府军队](../Category/清朝政府军队.md "wikilink")
[Category:中國陸軍](../Category/中國陸軍.md "wikilink")
[Category:1890年代中国军事](../Category/1890年代中国军事.md "wikilink")
[Category:1900年代中国军事](../Category/1900年代中国军事.md "wikilink")
[Category:1910年代中国军事](../Category/1910年代中国军事.md "wikilink")
[Category:1894年中國建立](../Category/1894年中國建立.md "wikilink")
[Category:1912年中國廢除](../Category/1912年中國廢除.md "wikilink")