**利佩茨克州**（）是[俄羅斯聯邦主體之一](../Page/俄羅斯聯邦主體.md "wikilink")，屬[中央聯邦管區成立於](../Page/中央聯邦管區.md "wikilink")1954年1月6日。面積24,100
平方公里，2010年統計人口117萬3513人。首府[利佩茨克](../Page/利佩茨克.md "wikilink")，距離[俄羅斯首都](../Page/俄羅斯.md "wikilink")[莫斯科](../Page/莫斯科.md "wikilink")536公里。

利佩茨克州位于东欧平原的中心地带，地处俄罗斯中部丘陵地带（高达246米）。其地处顿河水库上游，属中央黑土区地带。
利佩茨克州南部与[沃罗涅日](../Page/沃罗涅日州.md "wikilink")、[库尔斯克州相邻](../Page/库尔斯克州.md "wikilink")，西部与[奥廖尔州相邻](../Page/奥廖尔州.md "wikilink")，西北部与[图拉州相邻](../Page/图拉州.md "wikilink")，北邻[梁赞州](../Page/梁赞州.md "wikilink")，东接[坦波夫州](../Page/坦波夫州.md "wikilink")。境内主要河流为[顿河及其支流](../Page/顿河_\(俄罗斯\).md "wikilink")。其气候为[温带大陆性气候](../Page/温带大陆性气候.md "wikilink")，一月份的平均气温为零下10℃，七月份的平均气温为19℃，降雨量为每年500毫米。

## 注释

## 参考资料

[\*](../Category/利佩茨克州.md "wikilink")
[Category:中央聯邦管區](../Category/中央聯邦管區.md "wikilink")
[Category:俄罗斯州份](../Category/俄罗斯州份.md "wikilink")
[Lipe](../Category/1954年建立的行政區劃.md "wikilink")