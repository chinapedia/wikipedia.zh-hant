《**Stay with
Me**》為[香港歌手](../Page/香港.md "wikilink")[黃凱芹在](../Page/黃凱芹.md "wikilink")[寶麗金唱片公司的最後一张專輯](../Page/寶麗金唱片公司.md "wikilink")，1992年2月推出。\[1\]

隨著四大天王、寶麗金小花系統的形成，旗下歌手的唱片銷量開始下滑，黃凱芹雖然如[李克勤一樣仍受歡迎](../Page/李克勤.md "wikilink")，但唱片公司的創作方針改變，導致商業作品粗製濫造，他也開始跟唱片公司不咬弦，在推出此專輯後便轉會[飛圖唱片](../Page/飛圖唱片.md "wikilink")。

## 曲目

## 參考文獻

[Category:香港音樂專輯](../Category/香港音樂專輯.md "wikilink")
[Category:1992年音樂專輯](../Category/1992年音樂專輯.md "wikilink")
[Category:黃凱芹音樂專輯](../Category/黃凱芹音樂專輯.md "wikilink")

1.  [寧願告退 難像你醉 黃凱芹](http://news.stheadline.com/figure/?id=435)，頭條網