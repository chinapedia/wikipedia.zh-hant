**亨利·哈米尔·福勒**（**Henry Hammill
Fowler**，1908年9月5日[弗吉尼亚州](../Page/弗吉尼亚州.md "wikilink")[罗阿诺克](../Page/罗阿诺克_\(弗吉尼亚州\).md "wikilink")
-
2000年1月3日），[美国律师](../Page/美国.md "wikilink")、政治家，[美国民主党成员](../Page/美国民主党.md "wikilink")，曾任[美国财政部长](../Page/美国财政部长.md "wikilink")（1965年-1968年）。

## 參考資料

<references />

  - [Man in the
    news.](http://news.google.com/newspapers?nid=2209&dat=19650329&id=709gAAAAIBAJ&sjid=SXENAAAAIBAJ&pg=7269,2555475)

[F](../Category/1908年出生.md "wikilink")
[F](../Category/2000年逝世.md "wikilink")
[F](../Category/美国财政部长.md "wikilink")
[F](../Category/耶魯法學院校友.md "wikilink")