**日本短尾貓**，又名**日本截尾貓**。這種貓是由于[基因突變而產生的品種](../Page/基因突變.md "wikilink")。日本短尾貓在[日本本土已有一千年左右的歷史](../Page/日本.md "wikilink")，可能是從[中國帶入](../Page/中國.md "wikilink")\[1\]。日本短尾貓的尾巴僅長10厘米左右，與[馬恩島貓都是尾巴最短的貓品種](../Page/馬恩島貓.md "wikilink")。日本短尾貓中以[三色貓最為受歡迎](../Page/三色貓.md "wikilink")，但紅白貓、[白貓也很多](../Page/白貓.md "wikilink")。

[招財貓的樣子即來自于日本短尾貓](../Page/招財貓.md "wikilink")。

## 参考文献

[Category:源自日本的貓品種](../Category/源自日本的貓品種.md "wikilink")
[Category:天然的貓品種](../Category/天然的貓品種.md "wikilink")
[Category:突變生物](../Category/突變生物.md "wikilink")

1.  [Japanese
    Bobtail](http://www.cfa.org/client/breedJapaneseBobtail.aspx)