GIZA studio
[日本](../Page/日本.md "wikilink")[Being,Inc.旗下的唱片公司](../Page/Being,Inc..md "wikilink")1998年成立。在[近畿地區使用音樂事業積蓄了的資金廣泛地開展著高級公寓銷售](../Page/近畿地區.md "wikilink")·音樂大廳·音樂專門學校·飲食業。廠牌名稱的來源是
Garage Indies Zapping Association 的第一個字母合併而成。以[三枝夕夏 IN
db](../Page/三枝夕夏_IN_db.md "wikilink")、[小松未歩](../Page/小松未歩.md "wikilink")、[倉木麻衣](../Page/倉木麻衣.md "wikilink")、[愛内里菜](../Page/愛内里菜.md "wikilink")、[GARNET
CROW等人氣歌手而聞名](../Page/GARNET_CROW.md "wikilink")。目前GIZA studio已經暫停業務。

## 特徴

  - 旗下藝人大多為以[關西為據點的音樂人](../Page/近畿地方.md "wikilink")、許多音樂作品會搭配動畫片商業搭配
    發行，[倉木麻衣搭配](../Page/倉木麻衣.md "wikilink")[名偵探柯南的歌曲至今有](../Page/名偵探柯南.md "wikilink")16首，此外[愛内里菜與](../Page/愛内里菜.md "wikilink")[三枝夕夏
    IN db](../Page/三枝夕夏_IN_db.md "wikilink")
    以搭配許多動畫作品而提升人氣，例如：名偵探柯南、格闘美神
    武龍、魯莽天使。

<!-- end list -->

  - 廠牌設立的2000年初期，[小松未歩](../Page/小松未歩.md "wikilink")、[倉木麻衣](../Page/倉木麻衣.md "wikilink")、[愛内里菜](../Page/愛内里菜.md "wikilink")、[GARNET
    CROW](../Page/GARNET_CROW.md "wikilink")、[三枝夕夏 IN
    db等藝人藉由動畫商業搭配維持著穩定銷量](../Page/三枝夕夏_IN_db.md "wikilink")。之後[上原あずみ](../Page/上原あずみ.md "wikilink")、[松橋未樹](../Page/松橋未樹.md "wikilink")、[長谷実果](../Page/長谷実果.md "wikilink")、[菅崎茜](../Page/菅崎茜.md "wikilink")、[岸本早未](../Page/岸本早未.md "wikilink")、[スパークリングポイント](../Page/スパークリングポイント.md "wikilink")、[宇浦冴香等高中生出道顯示藝人出道年輕化的傾向](../Page/宇浦冴香.md "wikilink")。

<!-- end list -->

  - 後期音樂風格漸多元化並設立了JAZZ廠牌，2006年到2007年期間[森川七月](../Page/森川七月.md "wikilink")、[森田葉月](../Page/森田葉月.md "wikilink")、[青紀ひかり](../Page/青紀ひかり.md "wikilink")、[小泉ニロ](../Page/小泉ニロ.md "wikilink")、[早川えみ](../Page/早川えみ.md "wikilink")、[中島紅音以爵士樂專輯出道](../Page/中島紅音.md "wikilink")。
    2008年[羽田裕美以鋼琴演奏專輯出道](../Page/羽田裕美.md "wikilink")，2009年
    [さぁさ與](../Page/さぁさ.md "wikilink")[Chicago
    Poodle從地下廠牌轉為正式出道](../Page/Chicago_Poodle.md "wikilink")。

<!-- end list -->

  - 2010年舉辦試鏡「REAL\&BLACK AUDITION
    2010」，優勝者為有田長加，審査員特別賞是宮東里奈。2011年以白石乃梨為中心的女子舞稻團體[Caos
    Caos
    Caos正式出道](../Page/Caos_Caos_Caos.md "wikilink")。2012年搖滾系副廠牌D-GO成立。

## 沿革

  - 1998年9月1日公司設立於[大阪府](../Page/大阪府.md "wikilink")。。
  - 2009年 - JAZZ專門廠牌「**GIZA JAZZ**」を設立。跟主樣廠牌「GIZA studio」相同以女性歌手為主。
  - 2010年 - 「GIZA JAZZ」改為「**O-TOWN Jazz**」。
  - 2012年 - 搖滾系副廠牌「**D-GO**」成立。

## 現狀

GIZA
studio的主力歌手[倉木麻衣與](../Page/倉木麻衣.md "wikilink")[上木彩矢移籍](../Page/上木彩矢.md "wikilink")、[愛内里菜引退](../Page/愛内里菜.md "wikilink")、[小松未歩與](../Page/小松未歩.md "wikilink")[三枝夕夏
IN db活動休止](../Page/三枝夕夏_IN_db.md "wikilink")，[GARNET
CROW在](../Page/GARNET_CROW.md "wikilink")2013年6月解散，目前GIZA
studio旗下能進入公信榜前20名的藝人為[Chicago
Poodle](../Page/Chicago_Poodle.md "wikilink")、[doa與](../Page/doa.md "wikilink")[植田真梨惠](../Page/植田真梨惠.md "wikilink")。

## 旗下歌手

### 活動中

**GIZA ARTIST**

  - [doa](../Page/doa.md "wikilink")
  - [大野愛果](../Page/大野愛果.md "wikilink")（歌手活動休止中）
  - [Chicago Poodle](../Page/Chicago_Poodle.md "wikilink")
  - [Caos Caos Caos](../Page/Caos_Caos_Caos.md "wikilink")
  - [WAR-ED](../Page/WAR-ED.md "wikilink")
  - [植田真梨惠](../Page/植田真梨惠.md "wikilink")

**D-GO**

  - [grram](../Page/grram.md "wikilink")
  - ssllee
  - Sensation
  - [夏色](../Page/夏色_\(乐队\).md "wikilink")
  - Back pageZ
  - BLUE TURNS TO GREY

**GIZA JAZZ**

  - 森川七月
  - 青紀
  - [岡崎雪](../Page/岡崎雪.md "wikilink")

### 移籍

  - [倉木麻衣](../Page/倉木麻衣.md "wikilink")（2007年移籍至NORTHERN MUSIC）
  - [滴草由實](../Page/滴草由實.md "wikilink")（2006年移籍至ZAIN
    RECORDS後，2007年再移籍至NORTHERN MUSIC）
  - [上木彩矢](../Page/上木彩矢.md "wikilink")（2009年移籍至avex trax）
  - [梓](../Page/梓.md "wikilink")（移籍至PONY CANYON）

### 解散、活動中止

  - [小松未歩](../Page/小松未歩.md "wikilink")（2009年2月中止）
  - [愛内里菜](../Page/愛内里菜.md "wikilink")（2010年9月引退）
  - [GARNET CROW](../Page/GARNET_CROW.md "wikilink")（2013年6月解散）
  - [北原愛子](../Page/北原愛子.md "wikilink")（2011年7月引退）
  - [上原あずみ](../Page/上原あずみ.md "wikilink")（2010年7月違反契約而解雇）
  - [宇浦冴香](../Page/宇浦冴香.md "wikilink")（2009年12月活動休止）
  - [羽田裕美](../Page/羽田裕美.md "wikilink")
  - [さぁさ](../Page/さぁさ.md "wikilink")
  - [平田香織](../Page/平田香織.md "wikilink")
  - [PINC INC](../Page/PINC_INC.md "wikilink")（2009年12月解散）
  - [Naifu](../Page/Naifu.md "wikilink")（2009年12月解散）
  - [the★tambourines](../Page/the★tambourines.md "wikilink")（2009年6月解散）
  - [岸本早未](../Page/岸本早未.md "wikilink")（2008年4月中止、現在從事流行服飾相關工作）
  - [竹井詩織里](../Page/竹井詩織里.md "wikilink")（2008年2月中止）
  - [岩田さゆり](../Page/岩田さゆり.md "wikilink")（2008年6月中止）
  - [三枝夕夏 IN db](../Page/三枝夕夏_IN_db.md "wikilink")（2010年1月解散）
  - [スパークリング☆ポイント](../Page/スパークリング☆ポイント.md "wikilink")（2009年6月解散）
  - [高岡亜衣](../Page/高岡亜衣.md "wikilink")（2009年4月中止）
  - [OOM](../Page/OOM.md "wikilink")（2009年4月活動休止）
  - [菅崎茜](../Page/菅崎茜.md "wikilink")（2007年中止）
  - [JEWELRY](../Page/ジュエリー_\(歌手\).md "wikilink")（2005年9月、日本活動停止）
  - [RAMJET PULLEY](../Page/RAMJET_PULLEY.md "wikilink")（2003年、活動停止）
  - [Soul Crusaders](../Page/Soul_Crusaders.md "wikilink")（2002年、解散）
  - [松橋未樹](../Page/松橋未樹.md "wikilink")（2002年4月中止）
  - [New Cinema 蜥蜴](../Page/New_Cinema_蜥蜴.md "wikilink")（2002年8月解散）
  - [sweet velvet](../Page/sweet_velvet.md "wikilink")（2001年中止）
  - [長谷実果](../Page/長谷実果.md "wikilink")（2002年4月中止）
  - [GRASS ARC.](../Page/GRASS_ARC..md "wikilink")　（2000年中止）
  - [吉田知加](../Page/吉田知加.md "wikilink")（2002年中止）
  - [4D-JAM](../Page/4D-JAM.md "wikilink")（2002年10月中止）
  - [Nothin' but love](../Page/Nothin'_but_love.md "wikilink")（2003年中止）
  - [Les MAUVAIS
    GARÇONNES](../Page/Les_MAUVAIS_GARÇONNES.md "wikilink")（2002年中止）
  - [JASON ZODIAC](../Page/JASON_ZODIAC.md "wikilink")（2002年中止）
  - [cocott](../Page/cocott.md "wikilink")（楽曲制作・プログラミング担当の[北浦正尚は](../Page/北浦正尚.md "wikilink")[ビーイングから転出して本格的に](../Page/ビーイング.md "wikilink")[作曲家に転身](../Page/作曲家.md "wikilink")）
  - [WAG](../Page/WAG.md "wikilink")（2006年12月 解散）
  - [rumania
    montevideo](../Page/rumania_montevideo.md "wikilink")（2002年解散）
  - [山口裕加里](../Page/山口裕加里.md "wikilink")（2004年、レギュラー番組終了に伴い活動中止）
  - [DEAR
    RESONANCE](../Page/DEAR_RESONANCE.md "wikilink")（2001年9月29日発売のシングル「夢の間-NATURES
    RIDE-」を出したのみ、一ノ瀬響は現在も音楽活動中）
  - [Gulliver Get](../Page/Gulliver_Get.md "wikilink")（インディーズに転向）
  - [森田葉月](../Page/森田葉月.md "wikilink")（2008年、改名しインディーズ転向）

## 相關連結

  - [GIZA studio](http://www.giza.co.jp/)
  - [BEING GROUP](http://beinggiza.com/)
  - \[日商美音亞洲股份有限公司\]

[Category:日本唱片公司](../Category/日本唱片公司.md "wikilink")
[Category:大阪府公司](../Category/大阪府公司.md "wikilink")
[Category:Being](../Category/Being.md "wikilink")