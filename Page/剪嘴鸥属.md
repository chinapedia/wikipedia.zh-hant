**剪嘴鸥**，学名*Rhynchopidae*，是[鸟纲](../Page/鸟纲.md "wikilink")[鸻形目的一个小](../Page/鸻形目.md "wikilink")[科](../Page/科.md "wikilink")。其下只有**剪嘴鸥属**一属，包括三种，分别分布在[美洲](../Page/美洲.md "wikilink")、[非洲和](../Page/非洲.md "wikilink")[南亚的热带和亚热带地区](../Page/南亚.md "wikilink")。

剪嘴鸥为[涉禽](../Page/涉禽.md "wikilink")，一般在水面上低飞，捕捉小鱼为食。

## 参考

  - BirdLife International (2007) [Species factsheet: *Rynchops
    albicollis*.](http://www.birdlife.org/datazone/species/index.html?action=SpcHTMDetails.asp&sid=3206&m=0)
    Downloaded from <http://www.birdlife.org> on 20/5/2007

## 外部链接

  - [剪嘴鸥视频](http://ibc.hbw.com/ibc/phtml/familia.phtml?idFamilia=68)

[Category:鸻形目](../Category/鸻形目.md "wikilink")
[\*](../Category/剪嘴鸥科.md "wikilink")