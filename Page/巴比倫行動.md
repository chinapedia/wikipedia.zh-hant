**巴比倫行動**（又称**歌剧院行动**或**Ofra行动**）是[以色列在](../Page/以色列.md "wikilink")1981年6月7日發動的一場軍事行動，目標是摧毀[伊拉克首都](../Page/伊拉克.md "wikilink")[巴格達附近的](../Page/巴格達.md "wikilink")[奧斯拉克](../Page/奧斯拉克.md "wikilink")（Osirak）核反應堆。

## 背景

伊拉克早在1960年代便開始發展核計劃。到了1970年代中期，伊拉克意圖從[法國和](../Page/法國.md "wikilink")[意大利購買核反應堆失敗後](../Page/意大利.md "wikilink")，終於成功說服法國政府出售一座研究反應堆和相關實驗室。1973年[贖罪日戰爭結束後大規模石油禁運導致的油價大幅上升讓法國心有餘悸](../Page/贖罪日戰爭.md "wikilink")，而當時法國有20%的石油來自伊拉克，伊拉克總統[萨达姆·侯赛因提出的豐厚條件讓法國實在難以拒絕](../Page/萨达姆·侯赛因.md "wikilink")：

1.  伊拉克將以當時市場原油價格，在未來10年提供法國每年7千萬桶原油。
2.  伊拉克將採購數十億美金的法國軍火，包括100架[幻象F1戰鬥機](../Page/幻象F1戰鬥機.md "wikilink")。
3.  伊拉克將對[標緻與](../Page/標緻汽車.md "wikilink")[雷諾汽車提出各](../Page/雷諾汽車.md "wikilink")5萬輛汽車訂單。
4.  法國將在巴格達西邊興建一處十億美金等級的度假別墅。

在這樣優厚的採購條件下，萨达姆·侯赛因於1975年9月前往[巴黎親自簽訂雙方的合約](../Page/巴黎.md "wikilink")。1979年，伊拉克在法國的協助下開始在奧斯拉克建造一座40[兆瓦的](../Page/兆瓦.md "wikilink")[輕水式核反應堆與一座小型試驗性質的反應爐](../Page/輕水反應爐.md "wikilink")。

## 以色列的外交行動

伊拉克的核計劃使一向與[阿拉伯國家對抗的以色列深感不安](../Page/阿拉伯國家.md "wikilink")，擔心伊拉克成功發展[核武器](../Page/核武器.md "wikilink")，會對以色列構成威脅，特別是以國戰略腹地狹小，一旦遇到核打擊會是毀滅性的。

伊拉克指他們只會把核能用在和平用途，當時伊拉克已經簽署[不扩散核武器条约](../Page/不扩散核武器条约.md "wikilink")，而且該核反應堆是符合[國際原子能機構的規定的](../Page/國際原子能機構.md "wikilink")。可是法國在與伊拉克簽署的協議當中同意供應高純度，武器等級的濃縮鈾原料，使得以色列的專家仍然擔心伊拉克會利用核反應堆生產可制造核武的[鈈](../Page/鈈.md "wikilink")，即使是正當且最好的情況下，伊拉克的工程師還是會掌握核子物理的相關訣竅。

以色列首先從外交途徑希望化解核危機，以色列外長[摩西·达扬](../Page/摩西·达扬.md "wikilink")（Moshe
Dayan）先後跟意大利、法國和美國的官員談判，但以色列無法證明伊拉克將會把核反應堆用作軍事用途。以色列的外交行動隨後失敗，然而這並沒有阻止以色列的企圖心。

## 軍事行動

[F-15A-hatzerim-2.jpg](https://zh.wikipedia.org/wiki/File:F-15A-hatzerim-2.jpg "fig:F-15A-hatzerim-2.jpg")
以色列採取的行動包括情報人員破壞尚未運出法國的各項設備與原料，以及利用軍事力量摧毀這個反應爐。在考慮到兩國之間的地理位置以及以色列本身的軍事力量，空中攻擊成為唯一可採用的方式。

根據以色列蒐集到的情報顯示，此核反應堆計劃在1981年7月14日伊拉克國慶日啟用，由於擔心啟動之後的反應爐在轟炸下會散佈嚴重污染環境的殘骸碎片，以色列總理[梅納赫姆·貝京決定在它啟用前實施一次](../Page/梅納赫姆·貝京.md "wikilink")[外科手術式打擊](../Page/外科手術式打擊.md "wikilink")，摧毀核反應堆。但是這個行動在以色列政府高層核心引發非常大的爭議，無法妥協的意見與政治之間的衝突，幾乎導致這個行動在尚未開始計畫前就被終止。

### 意外的禮物

決定空中攻擊行動之後，以色列空軍接下來需要提出實際的計畫。首先面臨的問題是要用什麼飛機執行這個任務。[A-4攻擊機與](../Page/A-4攻擊機.md "wikilink")[幼獅戰鬥機因為航程與航電的關係](../Page/幼獅戰鬥機.md "wikilink")，馬上就被踢出考慮名單。[F-4的航程非常勉強](../Page/F-4.md "wikilink")，而且一架飛機需要兩位成員，如果發生損失下的代價太大。當時最新銳的[F-15戰鬥機使用的](../Page/F-15.md "wikilink")[F100渦輪扇發動機可靠性欠佳](../Page/F100渦輪扇發動機.md "wikilink")，雖然具有投擲核子炸彈的能力，在航程上如果沒有[適型油箱](../Page/適型油箱.md "wikilink")（CFT）的配合，也無法低空來回飛行。可是要美國出售這個油箱會引發更多的猜疑，以色列空軍在防禦領空上不會需要這麼大的航程。

促成以色列跨出任務的第一步是一項來自伊朗的意外禮物。1979年[伊朗爆發政變](../Page/伊朗伊斯蘭革命.md "wikilink")，接著與美國的關係直轉而下，在政變發生之前，伊朗已經提出向美國購買[F-16A戰鬥機的意願](../Page/F-16.md "wikilink")，[政變發生之後](../Page/政變.md "wikilink")，已經有8架飛機進入最後組裝階段可是買方卻消失了，為了要解決這些無主的飛機，[美國國防部詢問以色列是否願意接手](../Page/美國國防部.md "wikilink")，正在為任務機種傷腦筋的以色列空軍總司令非常興奮這個意想不到的發展，沈重的炸彈可以留給F-16負責載運，F-15輕裝護航使飛行距離大大增加，整趟任務就不必進行高風險的空中加油，任務終於邁出重要的第一步。

### 行動部署

以色列空軍挑選了12名飛行員，分成3批於1980年2月開始前往美國接受F-16的訓練，這一批飛行員當中將取8位加上兩後備組成攻擊隊伍，但是稍後並未前往美國一同受訓，身分上資深且富有傳奇性的堅持一定要在中途加入攻擊隊伍，並且越級不斷向以色列參謀總長遊說，最後空軍總司令在無可奈何下將原先預定的拉尼·法尔克（Rani
Falk）轉為後備組員而平息這個紛爭。

除了利用情報人員取得反應爐的相關資料以外，以色列還需要有關地區的[衛星照片與反應爐外牆強度的資料](../Page/衛星.md "wikilink")。美國雖然提供以色列衛星照片，但是對於提供的地區範圍有嚴格的限制，伊拉克附近根本是在提供的範圍以外。以色列試圖透過多種管道都無法順利取得照片，後來是利用[美國中央情報局內部對以色列情勢同情的人士提供的管道取得他們需要的高解析度照片](../Page/美國中央情報局.md "wikilink")。

有關反應爐外牆的強度以及所需要使用的彈藥種類與數量的問題，就算是以模擬的方式也無法得到令人滿意的結果。於是以色列派出Joseph
Kivity和Joseph
Saltovitz兩位教授前往[美國核能管理委員會](../Page/美國核能管理委員會.md "wikilink")（Nuclear
Regulatory
Commission）的代表討論，他們很巧妙的說明為了防範日後恐怖份子對以色列的反應爐進行破壞，他們需要知道一些相關細節。美方的代表雖然認為以色列是別有用心，最後資料還是提供給兩位教授。

經過評估之後，以色列空軍認為雖然[雷射導引炸彈的精確度很高](../Page/雷射導引炸彈.md "wikilink")，然而飛機需要在目標區停留一段時間，風險太大，最後決定採用無導引的[Mk
84](../Page/Mk_80系列炸彈.md "wikilink") 2000磅炸彈，每一架F-16將攜帶兩枚這種炸彈。

由於以色列和伊拉克之間並無邊界接壤，以色列的飛機需要經過約旦或[沙特阿拉伯才可飛往](../Page/沙特阿拉伯.md "wikilink")[伊拉克](../Page/伊拉克.md "wikilink")。以色列空軍最後決定去程由南部靠近約旦的機場起飛，直接越過紅海進入沙烏地的領空，全程超低空飛行，回程利用高空噴射氣流的協助減輕對燃料的需求，通過沙烏地阿拉伯與約旦的領空之後返回同一個機場。

### 行動經過

耶路撒冷時間6月7日16時，以色列飛行員駕駛14架戰鬥機從[西奈半島的埃其翁空軍基地出發](../Page/西奈半島.md "wikilink")，其中8架F-16執行轟炸任務，6架F-15A擔任護航以及轟炸時的**阻絕戰鬥巡邏**（BARCAP）任務。此外還有兩架雙座F-15B擔任無線電通訊中繼站的工作。這批飛機分成前後3批，以離地只有數十米高的低空飛行。

這14架飛機沿著[約旦和](../Page/約旦.md "wikilink")[沙特阿拉伯的邊界向巴格達進發](../Page/沙特阿拉伯.md "wikilink")，但是一起飛通過紅海上空時就被搭乘遊艇度假的約旦國王[侯赛因發現並且通報他們的防空單位](../Page/侯賽因·本·塔拉勒.md "wikilink")，除此之外，即使途中在沙特阿拉伯的沙漠棄置用完的[副油箱](../Page/副油箱.md "wikilink")，也沒有被雷達發現。

到了18時10分機群進入了伊拉克領空。18時30分，機群開始轟炸核反應堆，先後以兩波，每波4架的方式進行轟炸，共投下了16枚炸彈，成功的摧毀目標。但是實際上命中的只有14枚，有兩枚因為投擲的飛行員當天感冒，身體並不適合作戰，他在投彈前的鑽升筋斗過程中出現短暫昏迷而偏離預定瞄準點，因為目標已經摧毀，所以沒有人知道或者是追究原因，直到將近20年以後組員再度聚會時由當事人親口說出。

18時45分，以色列的機群開始回航。

参与轰炸的飞行员之一是后来成为以色列第一名[航天员的](../Page/航天员.md "wikilink")[伊兰·拉蒙](../Page/伊兰·拉蒙.md "wikilink")，後來他在[哥倫比亞太空梭最後一次任務中與其他太空人一起殉職](../Page/哥倫比亞太空梭.md "wikilink")。

## 結果

這次行動中，核反應堆被炸至嚴重損毀，不能被使用，同時有10名伊拉克軍人和1名法國的技術人員死亡。伊拉克當局試圖重建該核反應堆，法國政府也同意協助重建，然而法國卻在1984年退出重建計劃。

這次行動只有摧毀主反應爐，在附近的其他設施直在1991年的[波斯灣戰爭中](../Page/波斯灣戰爭.md "wikilink")，在聯軍猛烈轟炸下被完全摧毀。

## 各方反應

以色列指是次行動屬自衛行動，符合[聯合國憲章第](../Page/聯合國憲章.md "wikilink")51條。然而以色列攻擊核設施的做法被批評為違反國際法。

[安理會在](../Page/聯合國安全理事會.md "wikilink")6月19日通過[第487號決議](../Page/联合国安理会487号决议.md "wikilink")，嚴厲譴責以色列軍方的襲擊。

在事件後決定暫停向以色列出口F-16戰機，但同時指以色列有權自衛。

[阿拉伯國家聯盟駐聯合國特使](../Page/阿拉伯國家聯盟.md "wikilink")[馬科蘇德指出](../Page/馬科蘇德.md "wikilink")，以色列攻擊性外交發生了質的變化，阿拉伯國家應該協調相互的戰略。

## 相關條目

  - [恩德培行动](../Page/恩德培行动.md "wikilink")：以色列特種部隊在恩德培的反劫機行動

  - [木腳行動](../Page/木腳行動.md "wikilink")：以色列空軍襲擊在[突尼斯的](../Page/突尼斯.md "wikilink")[巴解總部](../Page/巴勒斯坦解放組織.md "wikilink")

  - 、[震網](../Page/震網.md "wikilink")：以網絡武器攻擊[伊朗核設施製造](../Page/伊朗核問題.md "wikilink")[濃縮鈾的](../Page/濃縮鈾.md "wikilink")[離心機控制設備](../Page/氣體離心法.md "wikilink")

## 更多資訊

### 參考資料

  - [*Jewish Virtual
    Library*,](http://www.jewishvirtuallibrary.org/jsource/History/osirak1.html),
    2006

  - [*F-16.net*](http://www.f-16.net/varia_article12.html), 2006

  -
  - [核武器擴散](../Page/核武器擴散.md "wikilink")

### 書本

  - Timothy L. H. McCormack, *Self-Defense in International Law: The
    Israeli Raid on the Iraqi Nuclear Reactor*, ISBN 0-312-16279-0
  - Rodger Claire, *Raid on the Sun : Inside Israel's Secret Campaign
    that Denied Saddam the Bomb*, ISBN 0-7679-1400-7
  - Judy Ellen Sund, Amos Perlmutter *Two Minutes Over Baghdad*, ISBN
    0-7146-8347-7
  - Clinton Dan McKinnon, Dan McKinnon, *Bullseye One Reactor*, ISBN
    0-941437-07-8

### 外部連結

  - [Documentary about the
    airstrike](http://www.youtube.com/watch?v=svzzrvN92Pg&mode=related&search=)
  - [Osiraq / Tammuz
    I](http://www.fas.org/nuke/guide/iraq/facility/osiraq.htm),
    Federation of American Scientists
  - [Iraq, Israel and the United Nations, Double
    standards](http://www.economist.com/world/na/displayStory.cfm?story_id=1378577),
    The Economist (UK)
  - [isayeret.com](http://www.isayeret.com) - The Israeli Special Forces
    Database
  - [Israel's Air Strike Against The Osiraq Reactor: A
    Retrospective](https://web.archive.org/web/20060214200101/http://anthonydamato.law.northwestern.edu/Adobefiles/A961-Isr.pdf)
    (Adobe PDF), Temple International and Comparative Law Journal
  - [Osirak: Over the
    reactor](http://news.bbc.co.uk/1/hi/world/middle_east/4774733.stm),
    [BBC](../Page/BBC.md "wikilink"), 5 June 2006 (interview with pilots
    involved)
  - [Security Council
    Resolution 487](https://web.archive.org/web/20110621231947/http://domino.un.org/UNISPAL.NSF/0/6c57312cc8bd93ca852560df00653995?OpenDocument)
  - [BBC](../Page/BBC.md "wikilink") [Factfile: How Osirak was
    bombed](http://news.bbc.co.uk/1/hi/world/middle_east/5020778.stm).
  - [Pilots
    Picture](https://web.archive.org/web/20070913192000/http://public.fotki.com/JewishMemorial/5_a_tribute_to_jewi/colonel_ilan_ramon/heroes.html)
  - [Operation Babylon Lithogrpah
    Print](https://web.archive.org/web/20060901070633/http://www.birkartwork.com/)
    - The Commemorative Lithograph of Operation Opera / Operation
    Babylon - Featuring Ilan Ramon & Relik Shafir

[Category:空袭](../Category/空袭.md "wikilink")
[Category:1981年衝突](../Category/1981年衝突.md "wikilink")
[Category:1981年以色列](../Category/1981年以色列.md "wikilink")
[Category:1981年伊拉克](../Category/1981年伊拉克.md "wikilink")
[Category:20世纪阿以冲突](../Category/20世纪阿以冲突.md "wikilink")
[Category:以色列国防军行动](../Category/以色列国防军行动.md "wikilink")
[Category:以色列空军历史](../Category/以色列空军历史.md "wikilink")
[Category:伊拉克军事事件](../Category/伊拉克军事事件.md "wikilink")
[Category:巴格达军事史](../Category/巴格达军事史.md "wikilink")
[Category:伊拉克－以色列關係](../Category/伊拉克－以色列關係.md "wikilink")
[Category:1981年6月](../Category/1981年6月.md "wikilink")