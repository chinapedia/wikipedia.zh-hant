**UltraEdit**（原名UltraEdit-32）是跨平台的一套商業性[文字編輯器](../Page/文字編輯器.md "wikilink")，由[IDM
Computer
Solutions在](../Page/IDM_Computer_Solutions.md "wikilink")1994年創造。UltraEdit有很强大的编程功能，支持[巨集](../Page/巨集.md "wikilink")、[語法高亮度顯示和](../Page/語法高亮度顯示.md "wikilink")[正则表达式等功能](../Page/正则表达式.md "wikilink")。檔案在標籤中可以被瀏覽和編輯。安裝需要約30
[MB的磁碟空間](../Page/MB.md "wikilink")。UltraEdit也支援以[Unicode和](../Page/Unicode.md "wikilink")[hex編輯的模式](../Page/Hex編輯器.md "wikilink")。官方網站提供30天試用版本的軟體下載。

## 參見

  - [文件编辑器列表](../Page/文件编辑器列表.md "wikilink")
  - [文件編輯器比較](../Page/文件編輯器比較.md "wikilink")

## 參考資料

## 外部链接

  - [UltraEdit](http://www.ultraedit.com/index.php?name=Content&pa=showpage&pid=1)

[Category:文本编辑器](../Category/文本编辑器.md "wikilink")
[Category:Linux文本编辑器](../Category/Linux文本编辑器.md "wikilink")