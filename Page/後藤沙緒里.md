**後藤沙緒里**是一名日本女性[配音員](../Page/配音員.md "wikilink")。出生於[神奈川縣](../Page/神奈川縣.md "wikilink")[橫濱市](../Page/橫濱市.md "wikilink")。身高152cm。愛稱、。

## 人物

家中排行第二，各有一個哥哥、妹妹、弟弟。 插畫的技巧不錯，會在活動等的簽名板上繪畫，2006年以後漸漸不畫了。
養了三隻狗，有來生的話想成為貓或樹木。

## 出演作品

  - **粗體字**為主要角色

### 電視動畫

  - 2004年

<!-- end list -->

  - [銀河天使](../Page/銀河天使.md "wikilink") - **烏丸千歲**

<!-- end list -->

  - 2005年

<!-- end list -->

  - [美少女學園](../Page/美少女學園.md "wikilink") - **飛鳥彌生**
  - [薔薇少女](../Page/薔薇少女.md "wikilink") - **薔薇水晶**

<!-- end list -->

  - 2006年

<!-- end list -->

  - [銀河天使II](../Page/銀河天使.md "wikilink") - 烏丸千歲
  - [落語天女](../Page/落語天女.md "wikilink") - **月島唯**
  - [草莓危機](../Page/草莓危機.md "wikilink") - 鬼屋敷桃實
  - [麵包超人](../Page/麵包超人.md "wikilink") - フーちゃん
  - [心跳回憶 Only Love](../Page/心跳回憶_Only_Love.md "wikilink") - 椎名菖蒲
  - [血色花园](../Page/血色花园.md "wikilink") - 杰西卡
  - [蔷薇少女～序曲～](../Page/薔薇少女.md "wikilink") - 薔薇水晶

<!-- end list -->

  - 2007年

<!-- end list -->

  - [天翔少女](../Page/天翔少女.md "wikilink") - **園宮可憐**
  - [殭屍借貸](../Page/殭屍借貸.md "wikilink") - 久世霜月
  - [絕望先生](../Page/絕望先生.md "wikilink") - 加賀愛
  - [鐵馬少年](../Page/鐵馬少年.md "wikilink") - 观众
  - [魔女獵人](../Page/魔女獵人.md "wikilink") - 修女B
  - [獸裝機攻](../Page/獸裝機攻.md "wikilink") - 露莉・露里
  - [發條武士](../Page/發條武士.md "wikilink") - 押入れわらこ、赤ちゃん
  - [Duel Masters Zero](../Page/Duel_Masters_Zero.md "wikilink") -
    機器人3號、選手D、女孩子
  - [南家三姊妹](../Page/南家三姊妹.md "wikilink") - 景子
  - [磁幽靈獵人](../Page/磁幽靈獵人.md "wikilink") - 美咲
  - [旋風管家](../Page/旋風管家.md "wikilink") - 同班女生、女生、售貨員
  - [永生之酒](../Page/永生之酒.md "wikilink") - 瑪麗・佩裏亞姆
  - [PRISM ARK](../Page/PRISM_ARK.md "wikilink") - オ兒ツィ、女性
  - 噗魯魯！小水滴 - チャイーラ姫
  - [流星洛克人Tribe](../Page/流星洛克人.md "wikilink") - ハヤヤ
  - [Ruby Gloom](../Page/Ruby_Gloom.md "wikilink") - 悲催姐

<!-- end list -->

  - 2008年

<!-- end list -->

  - [稻草男](../Page/稻草男.md "wikilink") - 女孩子
  - [絕對可憐CHILDREN](../Page/絕對可憐CHILDREN.md "wikilink") - 彌生子、操作員、女子
  - [俗·絕望先生](../Page/絕望先生.md "wikilink") - **加賀愛**
  - [超能少女](../Page/超能少女.md "wikilink") - 由美
  - [TIGER×DRAGON！](../Page/TIGER×DRAGON！.md "wikilink") -
    鹦鹉、濱田濑奈、女生A、女生B、壘球社員、行人A、毗沙門天國店員
  - [交響情人夢巴黎篇](../Page/交響情人夢.md "wikilink") - 出演瑪麗亞的女孩、女性奏者B
  - 野良耳2 - ミチ兒
  - [破天荒遊戲](../Page/破天荒遊戲.md "wikilink") - 馬德拉妹妹
  - [旋風管家](../Page/旋風管家.md "wikilink") - 女生
  - 噗魯魯！小水滴 啊哈☆ - チャイーラ姫
  - [企鵝的問題](../Page/企鵝的問題.md "wikilink") - **松井由美**、岡本恰克、小孩A
  - [無限之住人](../Page/無限之住人.md "wikilink") - 少女
  - [南家三姐妹 再來一碗](../Page/南家三姊妹.md "wikilink") - 景子
  - [棒球大聯盟4th season](../Page/棒球大聯盟.md "wikilink") - 場內廣播
  - [十字架與吸血鬼](../Page/十字架與吸血鬼.md "wikilink") - 少女
  - [十字架與吸血鬼 CAPU2](../Page/十字架與吸血鬼.md "wikilink") - でしでし子

<!-- end list -->

  - 2009年

<!-- end list -->

  - [閃電十一人](../Page/閃電十一人.md "wikilink") - 荒谷绀子
  - [大正野球娘](../Page/大正野球娘.md "wikilink") - **菊坂胡蝶**
  - [超人氣委員長](../Page/超人氣委員長.md "wikilink") - 伊藤理沙
  - 骷髅13 - 銀行員
  - [懺·絕望先生](../Page/絕望先生.md "wikilink") - **加賀愛**
  - [超能力大戰](../Page/超能力大戰.md "wikilink") - **剎那**
  - [戰鬥司書系列](../Page/戰鬥司書系列.md "wikilink") - 克萌拉
  - [旋風管家](../Page/旋風管家.md "wikilink") - 夏露娜·阿拉姆吉露、少女、記者、學生B、大姐姐
  - [潘多拉之心](../Page/潘多拉之心.md "wikilink") - 女僕
  - [瑪莉亞狂熱](../Page/瑪莉亞狂熱.md "wikilink") - **稻森弓弦**
  - [FAIRY TAIL](../Page/FAIRY_TAIL.md "wikilink") - 尼可拉（普魯）
  - [南家三姐妹 歡迎回來](../Page/南家三姊妹.md "wikilink") - 景子

<!-- end list -->

  - 2010年

<!-- end list -->

  - [元素獵人](../Page/元素獵人.md "wikilink") - 操作者
  - [百花繚亂](../Page/百花繚亂.md "wikilink") - **服部半藏美成**
  - 鹹面包咪咪 - **小巴塔**
  - [心靈偵探八雲](../Page/心靈偵探八雲.md "wikilink") - 少女的聲音、真由子
  - [交響情人夢Finale](../Page/交響情人夢.md "wikilink") - 少女A
  - [企鵝的問題 Max](../Page/企鵝的問題.md "wikilink") - **松井由美**、岡本恰克、110號
  - [管家後宮學園](../Page/管家後宮學園.md "wikilink") - 紅小路櫻子

<!-- end list -->

  - 2011年

<!-- end list -->

  - [快盜天使雙胞胎](../Page/快盜天使雙胞胎.md "wikilink") - 戶持娘
  - [「C」](../Page/C_\(動畫\).md "wikilink") - **Q**、三國貴子
  - [命運石之門](../Page/命運石之門.md "wikilink") - **桐生萌郁**
  - [Suzy's Zoo](../Page/Suzy's_Zoo.md "wikilink") - **Ellie Funt**
  - [丹特麗安的書架](../Page/丹特麗安的書架.md "wikilink") - 女子B
  - [哆啦A夢](../Page/哆啦A夢.md "wikilink") - 女友
  - [食夢者](../Page/食夢者.md "wikilink") - 沙緒裏
  - [FAIRY TAIL](../Page/FAIRY_TAIL.md "wikilink") - 梅兒蒂
  - [Happy\~Kappy](../Page/Happy~Kappy.md "wikilink") - **持田杏**
  - [企鵝的問題 DX？](../Page/企鵝的問題.md "wikilink") - **松井由美**
  - [瑪莉亞狂熱 Alive](../Page/瑪莉亞狂熱.md "wikilink") - **稻森弓弦**
  - [食夢者瑪莉](../Page/食夢者瑪莉.md "wikilink") - 帕蒂・三葉
  - [輕鬆百合](../Page/輕鬆百合.md "wikilink") - 松本理世

<!-- end list -->

  - 2012年

<!-- end list -->

  - [女子落](../Page/女子落.md "wikilink") - **暗落亭苦來**
  - [咲-Saki- 阿知賀篇 episode of
    side-A](../Page/咲-Saki-_阿知賀篇_episode_of_side-A.md "wikilink")
    - 小鍛治健夜
  - [FAIRY TAIL](../Page/FAIRY_TAIL.md "wikilink") - 尼可
  - [輕鬆百合♪♪](../Page/輕鬆百合♪♪.md "wikilink") - 松本理世

<!-- end list -->

  - 2013年

<!-- end list -->

  - [閃亂神樂](../Page/閃亂神樂.md "wikilink") - 未來
  - [南家三姐妹 我回來了](../Page/南家三姊妹.md "wikilink") - 景子
  - [百花繚亂 SAMURAI BRIDE](../Page/百花繚亂_SAMURAI_GIRLS.md "wikilink") -
    **服部半藏美成**
  - [星光少女 Rainbow Live](../Page/星光少女_Rainbow_Live.md "wikilink") -
    **小鳥遊音羽**
  - [旋風管家Cuties](../Page/旋風管家.md "wikilink") - 夏露娜·阿拉姆吉露

<!-- end list -->

  - 2014年

<!-- end list -->

  - [咲-Saki- 全国篇](../Page/咲-Saki-.md "wikilink") - 小鍛治健夜
  - [卡片鬥爭\!\! 先導者](../Page/卡片鬥爭!!_先導者.md "wikilink") - シャーリーン・チェン
  - [金田一少年之事件簿R](../Page/金田一少年之事件簿_\(動畫\).md "wikilink") - 夕凪遙
  - [FAIRY TAIL 魔導少年](../Page/FAIRY_TAIL.md "wikilink") - 梅兒蒂
  - [女友伴身邊](../Page/女友伴身邊.md "wikilink") - 黑川凪子

<!-- end list -->

  - 2015年

<!-- end list -->

  - [靈感少女](../Page/靈感少女.md "wikilink") - 相澤
  - [食戟之靈](../Page/食戟之靈.md "wikilink") - 貞塚奈央
  - [下流梗不存在的灰暗世界](../Page/下流梗不存在的灰暗世界.md "wikilink") - **不破冰菓**
  - [輕鬆百合 夏日時光！+](../Page/輕鬆百合.md "wikilink") - 松本理世
  - [輕鬆百合 3☆High！](../Page/輕鬆百合.md "wikilink") - 松本理世

<!-- end list -->

  - 2016年

<!-- end list -->

  - 食戟之靈 貳之皿 - 貞塚奈央
  - [Lostorage incited
    WIXOSS](../Page/Lostorage_incited_WIXOSS.md "wikilink") - 古滋子

<!-- end list -->

  - 2017年

<!-- end list -->

  - 食戟之靈 餐之皿 - 貞塚奈央

<!-- end list -->

  - 2018年

<!-- end list -->

  - 命運石之門0 - **桐生萌郁**
  - [Planet With](../Page/Planet_With.md "wikilink") - **白石小金**\[1\]
  - 閃亂神樂 SHINOVI MASTER -東京妖魔篇- - **未來**\[2\]

### OVA

**2006年**

  - [天翔少女](../Page/天翔少女.md "wikilink")（園宮可-{憐}-）

**2016年**

  - [食戟之靈](../Page/食戟之靈.md "wikilink")「暑假的繪里奈」（貞塚奈央）

### 遊戲

  - \-

  - [銀河天使](../Page/銀河天使.md "wikilink") Moonlit Lovers - 烏丸千歲（）

  - [銀河天使](../Page/銀河天使.md "wikilink") Eternal Lovers - 烏丸千歲（）

  - [銀河天使](../Page/銀河天使.md "wikilink") EX - 烏丸千歲（）

  - CROSS WORLD -

  - \-

  - \-

  - \-

  - \- （[PS2版](../Page/PlayStation_2.md "wikilink")）-

  - \- 桐谷佳奈子

  - [零～月蝕的假面～](../Page/零～月蝕的假面～.md "wikilink") - 月森圓香

  - [魔塔大陸](../Page/魔塔大陸.md "wikilink")2～少女們於世界中迴響的創造詩～ - アマリエ

  - [心跳回憶 Girl's Side 3rd
    Story](../Page/心跳回憶_Girl's_Side_3rd_Story.md "wikilink") -
    宇賀神美代

  - [命運石之門](../Page/命運石之門.md "wikilink") - **桐生萌郁**

  - [英雄傳說 閃之軌跡](../Page/英雄傳說_閃之軌跡.md "wikilink")（愛麗榭・舒華澤）

  - [臨時女友](../Page/臨時女友.md "wikilink") - 黑川凪子

  - [碧藍幻想](../Page/碧藍幻想.md "wikilink") - 茉莉

  - [碧藍航線](../Page/碧藍航線.md "wikilink") - 紐卡斯爾

### 電視節目

  - GATV（2004年4月～2005年9月）
  - MAG・ネット（2010年4月）

### 電台節目

  - \- （大阪廣播電臺・V-stastion線上廣播）2003年5月～2003年9月

  - （與[田村由香里](../Page/田村由香里.md "wikilink")、[新谷良子一起演出](../Page/新谷良子.md "wikilink")）

  - （松来未祐と金田朋子のRADIOデコピンないと2内番組）

  - airyth INTERNET
    RADIO（大阪廣播電臺・V-stastion線上廣播）（與[井口裕香一起演出](../Page/井口裕香.md "wikilink")）2003年10月～2005年3月

  - TOKYO→NIIGATA MUSIC CONVOY 火曜日　2005年10月担当（[FM
    PORT](../Page/新潟県民エフエム放送.md "wikilink")）

  - （[KONAMI](../Page/KONAMI.md "wikilink") db-FM
    与[小清水亞美一起演出](../Page/小清水亞美.md "wikilink")）

### 音樂作品

#### 專輯

  - Lip-Trip（2005年1月28日）
  - f（2005年8月24日）
  - platinum\*（2005年11月23日）

#### 角色單曲

  - （）

  - 約束（）

  - ANGEL CHARGE（烏丸千歲）

#### 廣播劇CD

  - （烏丸千歲）

  - （烏丸千歲）

  - 1年777組（）

  - （委員長）

  -
  - （松浦萌）

  - 廣播劇CD - [薔薇少女](../Page/薔薇少女～夢見～.md "wikilink")（薔薇水晶）

  - DramaCD–[Strawberry
    Panic\!](../Page/Strawberry_Panic!.md "wikilink") LYRIC2 （鬼屋敷桃實）

  - [瑪莉亞狂熱](../Page/瑪莉亞狂熱.md "wikilink") 廣播劇CD（稻森弓弦）

  - 桃組プラス戦記（雉乃木雪代）

### 其他

  - 1stDVD first date
  - Gamers Guardian Fairies（）
  - [銀河天使桌面裝飾集](../Page/銀河天使.md "wikilink")3（）

## 相關條目

  - [澤城美雪](../Page/澤城美雪.md "wikilink")
  - [田村由香里](../Page/田村由香里.md "wikilink")
  - [小清水亞美](../Page/小清水亞美.md "wikilink")

## 外部連結

  - [81produce -
    後藤沙緒里](https://web.archive.org/web/20071214040124/http://www.81produce.co.jp/list.cgi?lady+1535032105820)
  - [](https://web.archive.org/web/20060831055645/http://www.konami.jp/music/saori_goto/)

[Category:日本女性配音員](../Category/日本女性配音員.md "wikilink")
[Category:神奈川縣出身人物](../Category/神奈川縣出身人物.md "wikilink")
[Category:81 Produce](../Category/81_Produce.md "wikilink")
[Category:科樂美旗下藝人](../Category/科樂美旗下藝人.md "wikilink")

1.
2.