**秀林鄉**（[太魯閣語](../Page/德路固語.md "wikilink")：**Bsuring**），位於[台灣](../Page/台灣.md "wikilink")[花蓮縣北部](../Page/花蓮縣.md "wikilink")，北鄰[宜蘭縣](../Page/宜蘭縣.md "wikilink")[南澳鄉](../Page/南澳鄉.md "wikilink")，東北濱[太平洋](../Page/太平洋.md "wikilink")，東南連[新城鄉](../Page/新城鄉_\(台灣\).md "wikilink")、[花蓮市](../Page/花蓮市.md "wikilink")、[吉安鄉](../Page/吉安鄉.md "wikilink")、[壽豐鄉](../Page/壽豐鄉.md "wikilink")，西鄰[臺中市](../Page/臺中市.md "wikilink")[和平區](../Page/和平區_\(台灣\).md "wikilink")、[南投縣](../Page/南投縣.md "wikilink")[仁愛鄉](../Page/仁愛鄉.md "wikilink")，南接[萬榮鄉](../Page/萬榮鄉.md "wikilink")，為全台灣面積最大的鄉，其面積比[臺北市](../Page/臺北市.md "wikilink")、[基隆市](../Page/基隆市.md "wikilink")、[桃園市](../Page/桃園市.md "wikilink")、[新竹縣](../Page/新竹縣.md "wikilink")、[新竹市](../Page/新竹市.md "wikilink")、[彰化縣](../Page/彰化縣.md "wikilink")、[雲林縣](../Page/雲林縣.md "wikilink")、[嘉義市](../Page/嘉義市.md "wikilink")、[澎湖縣](../Page/澎湖縣.md "wikilink")、[金門縣](../Page/金門縣.md "wikilink")、[連江縣等縣市還大](../Page/連江縣.md "wikilink")。

秀林鄉地處[中央山脈東側](../Page/中央山脈.md "wikilink")，地勢高聳陡峭，氣候亦隨高度有所變化，鄉境大部分被劃入[太魯閣國家公園的範圍](../Page/太魯閣國家公園.md "wikilink")，鄉內居民以[台灣原住民](../Page/臺灣原住民族.md "wikilink")[太魯閣族為主](../Page/太魯閣族.md "wikilink")，[觀光業甚為發達](../Page/觀光業.md "wikilink")。

目前秀林鄉有更名**太魯閣鄉**的計畫，將續[嘉義縣](../Page/嘉義縣.md "wikilink")[阿里山鄉](../Page/阿里山鄉.md "wikilink")、[屏東縣](../Page/屏東縣.md "wikilink")[三地門鄉](../Page/三地門鄉.md "wikilink")、[高雄市](../Page/高雄市.md "wikilink")[那瑪夏區後](../Page/那瑪夏區.md "wikilink")，第四個恢復傳統名稱的[原住民鄉](../Page/山地鄉.md "wikilink")。\[1\]

## 鄉治沿革

秀林在清朝隸屬於台東直隸州蓮鄉地，在日據時代稱為「番地」，由研海支廳與花蓮港廳的番務課管理，一直到台灣光復之後，原屬於新城鄉管轄，隨後在政府積極提高原住民的民族平等地位以及扶助山地地區，在政治上廢除山地部落組織及頭目制度，又改制為「鄉」，實施地方自治，來提高其政治地位，設鄉治中心於「武士林」也就是現在的秀林村，最初訂定鄉名為「士林鄉」但是因為和台北「士林」同名，因此取其當地山明水秀林木蒼薈的優雅環境之意，改意為現在的鄉名「秀林鄉」。

## 交通

### 鐵路

  - [臺灣鐵路管理局](../Page/臺灣鐵路管理局.md "wikilink")

<!-- end list -->

  - [北迴線](../Page/北迴線.md "wikilink")：[和平車站](../Page/和平車站.md "wikilink")
    - [和仁車站](../Page/和仁車站.md "wikilink") -
    [崇德車站](../Page/崇德車站.md "wikilink") -
    [景美車站](../Page/景美車站.md "wikilink") -
    [新城（太魯閣）車站](../Page/新城（太魯閣）車站.md "wikilink")（主要門戶）

### 公路

#### 省道

  - ：[中橫公路](../Page/中橫公路.md "wikilink")，[大禹嶺](../Page/大禹嶺.md "wikilink")→[天祥](../Page/天祥.md "wikilink")→[太魯閣口](../Page/太魯閣牌樓.md "wikilink")(富世村)→[太魯閣大橋](../Page/太魯閣大橋.md "wikilink")

  - ：[蘇花公路](../Page/蘇花公路.md "wikilink")，和平村→[清水斷崖](../Page/清水斷崖.md "wikilink")→崇德村→[太魯閣大橋](../Page/太魯閣大橋.md "wikilink")

      - ：銅門村榕樹→仁壽橋→文蘭村→[鯉魚潭](../Page/鯉魚潭_\(花蓮縣\).md "wikilink")→[池南國家森林遊樂區](../Page/池南國家森林遊樂區.md "wikilink")

  - （未通，已解編）：文蘭村仁壽→銅門村→龍澗→天長斷崖→[奇萊](../Page/奇萊.md "wikilink")（為台電保線道路，奇萊以西接[能高越嶺道至](../Page/能高越嶺道.md "wikilink")[廬山](../Page/廬山.md "wikilink")）

      - ：[中橫公路霧社支線](../Page/霧社支線.md "wikilink")，[合歡山遊客服務中心](../Page/合歡山遊客服務中心.md "wikilink")→[克難關](../Page/克難關.md "wikilink")→[大禹嶺](../Page/大禹嶺.md "wikilink")

#### 鄉道

  - 花1線：崇德村海濱→下崇德（村內道路）
  - 花3線：崇德村→海濱→上崇德（村外海濱道路）
  - 花4線：富世村可樂部落→太魯閣大橋（橋下）→秀林村民有（陶樸閣）→[新城鄉新城村海濱](../Page/新城鄉_\(臺灣\).md "wikilink")
  - 花5線：太魯閣大橋→民治（固祿）→秀林村→民享（道拉斯）→景美村三棧部落（布拉旦）→新城鄉南三棧
  - 花8線：秀林村→[台9線](../Page/台9線.md "wikilink")→新城鄉順安村海濱
  - 花9線：加灣→景美村→佳民村→佳民橋（須美基溪Spiki）
  - 花10線：佳民村→新城鄉康樂村
  - 花13線：水源大橋
  - 花22線：花蓮水源地→水源大橋→[吉安鄉太昌村](../Page/吉安鄉.md "wikilink")
  - 花24線：花蓮水源地→水源村→水源大橋
  - 花34線：銅門大橋→銅門村榕樹部落→仁壽橋

### 客運

  - [花蓮客運](../Page/花蓮客運.md "wikilink")
  - [豐原客運](../Page/豐原客運.md "wikilink")(經台14甲線大禹嶺地區 往豐原9:00開 梨山13:00開)

### 港口

  - 和平水泥專用港

## 所屬部落

和平村

  - 克尼布部落(太魯閣族語:Knlibu)
  - 吾谷子部落(太魯閣族語:Gukut)
  - 卡納岸部落(太魯閣族語:Qnragan)

崇德村

  - 得卡倫部落(太魯閣族語:Dgarung)
  - 得吉利部落(太魯閣族語:Tkijig)

富世村

  - 可樂部落(太魯閣族語:Qrgi)
  - 富世部落(太魯閣族語:Bsngan)
  - 民樂部落(為大同部落、大禮部落之移住區)(太魯閣族語:Tnbarah)

秀林村

  - 陶樸閣部落(太魯閣族語:Tpuqu)
  - 秀林部落(太魯閣族語:Bsuring)
  - 固祿部落(太魯閣族語:Kulu)
  - 道拉斯部落(太魯閣族語:Dowras)

景美村

  - 克奧灣部落(太魯閣族語:Qowqan)
  - 波拉旦部落(太魯閣族語:Pratan)

佳民村

  - 格督尚部落(太魯閣族語:Kdusan)

水源村

  - 水源部落(太魯閣族語:Pajiq)
  - 比告部落(太魯閣族語:Bikaw)

銅門村

  - 依柏合部落(太魯閣族語:Ibuh)
  - 銅門部落(太魯閣族語:Dowmung)

文蘭村

  - 米亞丸部落(太魯閣族語:Myawan)
  - 文蘭部落(太魯閣族語:Tmunan)
  - 重光部落(太魯閣族語:Branaw)

## 行政區劃

[Hehuan_Lodge_side_angle_view.jpg](https://zh.wikipedia.org/wiki/File:Hehuan_Lodge_side_angle_view.jpg "fig:Hehuan_Lodge_side_angle_view.jpg")

<table>
<tbody>
<tr class="odd">
<td><p><font size="-1"><strong>秀林鄉行政區劃</strong></font></p></td>
</tr>
<tr class="even">
<td><div style="position: relative;font-size:100%">
<p><a href="https://zh.wikipedia.org/wiki/File:Sioulin_villages.svg" title="fig:Sioulin_villages.svg">Sioulin_villages.svg</a>         </p>
</div></td>
</tr>
</tbody>
</table>

<table>
<thead>
<tr class="header">
<th><p>村-{里}-名</p></th>
<th><p>原名</p></th>
<th><p>面積<br />
<small>（km²）</small>[2]</p></th>
<th><p>人口數</p></th>
<th><p>特色</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>秀林村</p></td>
<td><p>帛士林（Bsuring）</p></td>
<td><p>27.0024</p></td>
<td><p>2,197</p></td>
<td><p>包括陶樸閣部落、固祿部落、武士林部落、道拉斯部落等四大部村落組成</p></td>
</tr>
<tr class="even">
<td><p>水源村</p></td>
<td><p>巴支可（Pajiq）</p></td>
<td><p>25.0000</p></td>
<td><p>1,493</p></td>
<td><p>水源社區的居民屬於泰雅族東賽德克群，此群融合托魯閣、托賽、巴雷巴奧</p></td>
</tr>
<tr class="odd">
<td><p>文蘭村</p></td>
<td><p>托莫南、銅文蘭（Tmunan）</p></td>
<td><p>218.6215</p></td>
<td><p>1,371</p></td>
<td><p>本村有文蘭、米亞丸及重光三個聚落以太魯閣族人為主</p></td>
</tr>
<tr class="even">
<td><p>佳民村</p></td>
<td><p>格度善（Kdusan）</p></td>
<td><p>37.3929</p></td>
<td><p>1,223</p></td>
<td><p>有佳民（舊稱巴拉奧西拉克）及佳山（族人稱「委里」）兩個聚落</p></td>
</tr>
<tr class="odd">
<td><p>和平村</p></td>
<td><p>克尼布（Knlibu）</p></td>
<td><p>255.0351</p></td>
<td><p>1,790</p></td>
<td><p>花蓮最北村落。村內分和平、和中、和仁三個部落，以太魯閣族人佔多數</p></td>
</tr>
<tr class="even">
<td><p>富世村</p></td>
<td><p>玻士岸（Bsngan）</p></td>
<td><p>685.1600</p></td>
<td><p>2,250</p></td>
<td><p>內分得卡倫、落支煙、砂卡噹（大同）、赫赫斯（大禮）移住區、玻士岸與固錄（可樂）等六個聚落。以太魯閣族人為主，為本鄉及全國面積最大村，甚至大於臺北市，面積可排在鄉鎮市區中第九，比台灣本島最小縣彰化縣的三分之二略小,也是<a href="../Page/太魯閣國家公園.md" title="wikilink">太魯閣地區主體</a>。</p></td>
</tr>
<tr class="odd">
<td><p>景美村</p></td>
<td><p>加灣（Qowgan）、波拉旦（Pratan）</p></td>
<td><p>43.8077</p></td>
<td><p>2,200</p></td>
<td><p>分三棧聚落（舊巴拉丹溪下游）及加灣聚落（卡奧灣、得呂可、里奇麻、山廣）。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/銅門村.md" title="wikilink">銅門村</a></p></td>
<td><p>銅門（Dumung）、慕谷慕魚（Mqmgi）</p></td>
<td><p>287.7509</p></td>
<td><p>1,544</p></td>
<td><p>榕樹部落與銅門部落合而為一，慕谷慕魚係太魯閣族羣的一個大（頭目）家族</p></td>
</tr>
<tr class="odd">
<td><p>崇德村</p></td>
<td><p>達吉力、立霧(Tkijig)</p></td>
<td><p>62.0851</p></td>
<td><p>1,748</p></td>
<td><p>原名「得其黎」，國民黨接收後沿用，1960年代中橫公路竣工後改稱「立霧」。本村屬漁村有得豐、佳豐、東益發三大漁場</p></td>
</tr>
<tr class="even">
<td><p><strong>秀林鄉</strong></p></td>
<td></td>
<td><p>1,641.8555</p></td>
<td><p>15,904</p></td>
<td></td>
</tr>
</tbody>
</table>

## 教育

### 國民小學

  - [花蓮縣秀林鄉秀林國民小學](http://www.slips.hlc.edu.tw/)
  - [花蓮縣秀林鄉三棧國民小學](http://www.szps.hlc.edu.tw/)
  - [花蓮縣秀林鄉景美國民小學](http://www.zmps.hlc.edu.tw/)
  - [花蓮縣秀林鄉水源國民小學](http://www.syps.hlc.edu.tw/)
  - [花蓮縣秀林鄉文蘭國民小學](http://www.wlps.hlc.edu.tw/)
  - [花蓮縣秀林鄉和平國民小學](http://www.hpps.hlc.edu.tw/)
  - [花蓮縣秀林鄉西寶國民小學](http://www.spps.hlc.edu.tw/)
  - [花蓮縣秀林鄉佳民國民小學](http://www.cmps.hlc.edu.tw/)
  - [花蓮縣秀林鄉崇德國民小學](http://www.cdps.hlc.edu.tw/)
  - [花蓮縣秀林鄉富世國民小學](http://www.fusps.hlc.edu.tw/)
  - [花蓮縣秀林鄉銅門國民小學](https://web.archive.org/web/20070426154847/http://www.tmps.hlc.edu.tw/)
  - [花蓮縣秀林鄉銅蘭國民小學](http://www.tlaps.hlc.edu.tw/)

## 生活機能

  - [7-Eleven祥祐店](../Page/7-Eleven.md "wikilink")、崇德店、天祥店、泰雅店、太魯閣店
  - [全家便利商店秀林和平店](../Page/全家便利商店.md "wikilink")

## 旅遊

[Taiwan_2009_HuaLien_Taroko_Gorge_Biking_PB160057.jpg](https://zh.wikipedia.org/wiki/File:Taiwan_2009_HuaLien_Taroko_Gorge_Biking_PB160057.jpg "fig:Taiwan_2009_HuaLien_Taroko_Gorge_Biking_PB160057.jpg")
[Taiwan_2009_CingShui_Cliffs_on_SuHua_Highway_FRD_6762_Pano_Extracted.jpg](https://zh.wikipedia.org/wiki/File:Taiwan_2009_CingShui_Cliffs_on_SuHua_Highway_FRD_6762_Pano_Extracted.jpg "fig:Taiwan_2009_CingShui_Cliffs_on_SuHua_Highway_FRD_6762_Pano_Extracted.jpg")
[龍澗橋_-_panoramio.jpg](https://zh.wikipedia.org/wiki/File:龍澗橋_-_panoramio.jpg "fig:龍澗橋_-_panoramio.jpg")

  - [太魯閣國家公園](../Page/太魯閣國家公園.md "wikilink")
      - [太魯閣牌樓](../Page/太魯閣牌樓.md "wikilink")
      - 白楊瀑布
      - 長春祠
      - 翡翠谷
      - 燕子口
      - 天絲瀑布
      - 布洛灣遊憩區
      - [九曲洞](../Page/九曲洞.md "wikilink")
      - [水濂洞](../Page/水濂洞.md "wikilink")
      - 碧綠神木
      - [清水斷崖](../Page/清水斷崖.md "wikilink")
      - [奇萊山](../Page/奇萊山.md "wikilink")
      - 錐麓斷崖
      - [大禹嶺](../Page/大禹嶺.md "wikilink")
      - 蓮花池
      - [關原雲海](../Page/關原_\(台灣\).md "wikilink")
      - [天祥遊憩區](../Page/天祥_\(台灣\).md "wikilink")
      - [合歡山](../Page/合歡山.md "wikilink")
      - 翡翠谷
      - 天長斷崖
      - [龍澗水壩](../Page/水簾壩.md "wikilink")
      - [三棧遊憩區](../Page/三棧遊憩區.md "wikilink")
      - [萬代峽](../Page/萬代峽.md "wikilink")

## 参见

  - [太魯閣國家公園](../Page/太魯閣國家公園.md "wikilink")
  - [清水斷崖](../Page/清水斷崖.md "wikilink")

## 参考文献

## 外部連結

  - [秀林鄉公所](http://www.shlin.gov.tw/)
  - [太魯閣國家公園](http://www.taroko.gov.tw/)

[Category:花蓮縣行政區劃](../Category/花蓮縣行政區劃.md "wikilink")
[秀林鄉](../Category/秀林鄉.md "wikilink")
[Category:山地鄉](../Category/山地鄉.md "wikilink")
[Category:台灣行政區劃之最](../Category/台灣行政區劃之最.md "wikilink")

1.
2.