[Windows_logo_-_1985.svg](https://zh.wikipedia.org/wiki/File:Windows_logo_-_1985.svg "fig:Windows_logo_-_1985.svg")、[Windows
2.0](../Page/Windows_2.0.md "wikilink")、[Windows
3.0](../Page/Windows_3.0.md "wikilink")。\]\]
[Windows_logo_-_1992.svg](https://zh.wikipedia.org/wiki/File:Windows_logo_-_1992.svg "fig:Windows_logo_-_1992.svg")、[Windows
95](../Page/Windows_95.md "wikilink")、[Windows
98](../Page/Windows_98.md "wikilink")、[Windows
2000](../Page/Windows_2000.md "wikilink")、[Windows
ME](../Page/Windows_ME.md "wikilink")。\]\]
[Windows_logo_-_2002–2012_(Multicolored).svg](https://zh.wikipedia.org/wiki/File:Windows_logo_-_2002–2012_\(Multicolored\).svg "fig:Windows_logo_-_2002–2012_(Multicolored).svg")。[Windows
Vista和](../Page/Windows_Vista.md "wikilink")[Windows
7](../Page/Windows_7.md "wikilink")。\]\]
[Windows_logo_-_2012.svg](https://zh.wikipedia.org/wiki/File:Windows_logo_-_2012.svg "fig:Windows_logo_-_2012.svg")。[Windows
8.1和](../Page/Windows_8.1.md "wikilink")[Windows
10](../Page/Windows_10.md "wikilink")\]\]
1983年，美國企業[微软對外宣布在](../Page/微软.md "wikilink")[MS-DOS](../Page/MS-DOS.md "wikilink")（自1981年植根在[IBM](../Page/IBM.md "wikilink")[電腦上的操作介面](../Page/電腦.md "wikilink")）上開發一個[图形用户界面](../Page/图形用户界面.md "wikilink")[操作系統](../Page/操作系統.md "wikilink")，即微软视窗系统（[Microsoft
Windows](../Page/Microsoft_Windows.md "wikilink")）。

## 早期發展

微軟在1985年推出了第一版的Microsoft Windows，因功能不足而不獲電腦用户歡迎。它原本稱為Interface
Manager，但微軟的市場主管Rowland Hanson認為Windows這個名字比較能吸引消費者。[Windows
1.0並不是完整的操作系統](../Page/Windows_1.0.md "wikilink")，而是對[MS-DOS的拓展](../Page/MS-DOS.md "wikilink")，因此亦繼承了後者的問題。而且伴隨的應用程式功能太過薄弱，無法吸引企業用户。

再者，和[蘋果公司間的法律爭議限制它的功能](../Page/蘋果公司.md "wikilink")。例如視窗只能平鋪在螢幕上，而不能互相重叠，也沒有檔案-{zh-tw:回收筒;zh-cn:回收站}-。微軟後來和Apple簽署一份專利授權協定才解決這兩個問題。

1987年微軟發行[Windows
2.0](../Page/Windows_2.0.md "wikilink")，比起上一版本較受歡迎。主要原因是微軟發行「執行時期版本」的Excel
和 Word for Windows，即是程式可於MS-DOS執行，然後自動啟動Windows，結束程式時同時關閉Windows。

[Aldus
Pagemaker的Windows版本亦開始發行](../Page/Adobe_PageMaker.md "wikilink")，成為Windows的一大支援。電腦歷史學家認為這是第一個由微軟以外的軟體商發行的重要程式，標誌着Windows
成功的開始。

2.0x 版本使用真實模式記憶模式，限制了最多只可運用1M記憶體。後期再發行兩個版本，分別為Windows/286 2.1 和
Windows/386 2.1。Windows/286 2.1
仍使用真實模式記憶模式，但首次支援HMA。Windows/386則使用保護模式記憶模式，加上EMS模擬。

在2.03 版本和後期的3.0版本，Apple指控微軟抄襲麥金塔的外觀，例如重叠視窗。但在1989年，大部份的指控被撤銷。

## Windows 3.0

微軟在1990年發行[Windows
3.0非常成功](../Page/Windows_3.0.md "wikilink")。除了改進應用程式的能力之外，利用[虛擬記憶體](../Page/虛擬記憶體.md "wikilink")，Windows容許MS-DOS軟件有更好的多任务表現。加上個人電腦的圖像處理能力改良（使用VGA圖像卡），和使用保護模式記憶模式，應用程式能比較容易運用更多的記憶體。令個人電腦能和麥金塔一較高下。

Windows 3.0
可在真實（8086实模式），標準（80286保护模式），和80386增强模式上運行，而且和当时所有Intel微處理器相容。Windows
可以檢查最佳的運行模式，雖然亦可以強制設定要求的模式。這是第一個在[保護模式下執行應用程式的版本](../Page/保護模式.md "wikilink")。為了和以前的版本兼容，應用程式限制在16位元的環境中，無法充分发挥386微處理器的32位元能力。

多個月後，多媒體版本的Windows發行（Windows 3.0 with Multimedia Extensions
1.0）。它包括第一個音效卡／CD-ROM多媒體工具，例如MS
Bookshelf。它可作為Windows 3.1多媒體功能的預告。

Windows 3.0是如此的成功，在兩年內便賣出超過一千萬套，成為微軟重要的收入来源。亦令微軟更改它早期的計劃。

## OS/2

在80年代中末期，微軟和IBM合作開發OS/2以取代DOS，設計上會利用286微處理器的保護模式能力，可以使用多達16MB記憶體。OS/2
1.0 於1987年發行，支援多工和可以運行DOS程式。

但直至1988年發行版本1.1時，才有一個稱為Presentation Manager
(PM)的圖像介面。雖然有些使用者認為它優於Windows，但它的API與Windows不相容。

1989年發行版本1.2，引入名為HPFS的新檔案系統，以取代DOS的FAT系統。

在90年代初期，微軟和IBM的合作關係開始出現問題。微軟希望繼續開發Windows，IBM卻認為應該把資源放在OS/2。為了解決這些問題，雙方同意IBM可以繼續開發OS/2
2.0去取代OS/2 1.3和Windows 3.0，而微軟則開發一個新的作業系統 OS/2 3.0。

但這個協議很快便決裂，微軟和IBM的合作關係亦正式終結。IBM繼續開發OS/2，微軟則改為開發Windows
NT。但根據協議，雙方保留使用OS/2和Windows的技術的權利。

下一版本1.3修正一些1.x 的問題。1988年IBM發行版本2.0，這是一個重大的改良，它引入一個稱為Workspace Shell
(WPS)的物件導向的圖像介面，微軟在Windows
95中也參考了它。版本2.0也完整地支援32位元，提供優異的多工能力，程式亦可使用多達4G記憶體。但內部仍有一些16位元程式碼，例如驅動設備程式也是16位元的。而且因為IBM保留使用OS/2和Windows程式碼的權利，所以OS/2
2.0能運行DOS和Windows 3.0 的程式。

在這個時候，誰會勝出這場被稱為「桌面戰爭」仍是未知之數，但OS/2最終還是沒辦法獲得足夠的市場接受。

## Windows 3.1 與 NT

作為對OS/2 2.0發行的回應，微軟開發[Windows
3.1](../Page/Windows_3.1.md "wikilink")，它主要是修正Windows
3.0的一些問題和引入多媒體功能，並加入TrueType向量字型。它不再支援真實模式，只可在80286或後期的微處理器上運行。微軟接着發行Windows
3.11，它實際是Windows 3.1加上其所有的修正。 同時，微軟發行 Windows for Workgroups
(WfW)，它主要改良網絡驅動設備程式和協定堆疊，並且支援點對點網絡。 使用者可下載TCP/IP
協定堆疊的支援，以連上網際網絡。Windows for Workgroups共有二種版本：WfW 3.1
和 WfW 3.11。

這些版本和Windows 3.0一樣大受歡迎。雖然3.1x系列仍缺乏OS/2大部份重要功能，例如長檔名支援，易上手的使用者桌面，系統保護等。
微軟照樣橫掃操作系統和圖像介面的市場，Windows API 成為軟件產業的標準。

為了開發[Windows
NT](../Page/Windows_NT.md "wikilink")，微軟從[DEC聘請](../Page/DEC.md "wikilink")[戴夫·卡特勒這位](../Page/戴夫·卡特勒.md "wikilink")[VMS的主要設計師](../Page/VMS.md "wikilink")。Cutler曾開發VMS的下一代：Mica，當DEC停止這個計劃後，Cutler把Mica的技術和開發團隊帶到微軟。DEC認為Cutler帶走Mica的程式碼，為此微軟要付給DEC共1.5億美金和同意在NT上支援DEC的[Alpha處理器](../Page/Alpha.md "wikilink")。

Windows NT 3.1 的Beta版本初次在1992年七月的Professional Developers
Conference出現，在這個會議上，微軟宣佈有意開發一個作業系統以整合Windows NT和Windows
3.1的後繼者（即後來的Windows
95），名為[Cairo](../Page/Cairo.md "wikilink")，但直至[Windows
XP才能實現](../Page/Windows_XP.md "wikilink")。不過Cairo比微軟當初想像的要困難得多，所以部份的技術現在還未能完成，例如Cairo物件導向檔案系統（類似現在提出的WinFS子系統）。

在NT上開發驅動程式要困難得多，加上Windows NT對硬體的要求太高，而且它的圖形介面和Windows
3.1一樣不及OS/2，令Windows NT無法取代Windows 3.1。

但NT優異的網路能力，和先進的NTFS檔案系統，令NT極適合伺服器市場，Windows NT 3.51
是微軟首次進入這個市場的產品，並逐漸奪取Novell的市場。

微軟在NT中最大的優勢是新開發的32位元API，稱為Win32。儘管Windows
NT和Chicago之間的架構有很大差異，微軟還是允諾Win32
API在它們間盡量相容。

## Windows 95

[Windows 3.11](../Page/Windows_3.11.md "wikilink")
後，微軟開始開發下一代的操作系统，代號為Chicago。Chicago被設計為完整的32位元系統和支援強制性多工，類似[OS/2和](../Page/OS/2.md "wikilink")[Windows
NT](../Page/Windows_NT.md "wikilink")，以改善Windows
3.11的穩定性。系统多個部份被重新編寫或改良。Win32
API被定位為標準介面，但保持Win16相容性。並且引入隨插即用的功能和新的使用者介面。

因為相容性，效能和開發時間，微軟沒有把所有程式碼改寫為32位元，部份仍是16位元。[Windows
95](../Page/Windows_95.md "wikilink")
應用程式在保護模式運行，擁有32位元定址和使用虛擬記憶體的能力，令程式可使用多達2G虛擬記憶體，並且理論上避免其他程式影響自己的記憶位址空間。

同時IBM繼續發行OS/2，分別為OS/2 3.0 和 4.0（又名Warp）。但是當Windows 95 開始發行時，OS/2逐漸失去市場。

Windows 95 共有五種版本，

  - Windows 95 Original Release
  - Windows 95 A - 包括Windows 95 Service Pack 1
  - Windows 95 B - (OSR2) 包括一些改良例如[Internet Explorer
    3](../Page/Internet_Explorer_3.md "wikilink")
    和[FAT32的支援](../Page/FAT32.md "wikilink")
  - Windows 95 B USB - OSR2.1，包括基本的USB支援
  - Windows 95 C - (OSR2.5) 包括以上所有功能和IE 4.0，是最後的Windows 95版本

OSR2、OSR2.1和OSR2.5並沒有對公眾發行，而是給與廠商預先安裝在電腦上。

## Windows NT 4.0

作為進入工作站市場的嘗試，[微軟發行Windows](../Page/微軟.md "wikilink") NT
4.0。在其主要特色為使用[Windows
95介面](../Page/Windows_95.md "wikilink")，但基於[Windows
NT核心](../Page/Windows_NT.md "wikilink")。

Windows NT 4.0 共有四種版本，

  - Windows NT 4.0 工作站
  - Windows NT 4.0 伺服器
  - Windows NT 4.0 伺服器，企業版
  - Windows NT 4.0 終端伺服器

## Windows 98

[Windows 98](../Page/Windows_98.md "wikilink") 是 Windows 95
的一個規模較小的升級版，它包括新的硬體驅動程式和
[FAT32檔案系統](../Page/FAT32.md "wikilink")，後者支援大於2G的硬碟。Windows 98
亦把[Internet
Explorer整合至Windows介面和Windows檔案管理員中](../Page/Internet_Explorer.md "wikilink")。windows
98 plus

1999 年，微軟發行Windows 98 Second
Edition，主要新增功能為[因特网连接共享](../Page/因特网连接共享.md "wikilink")，容許多部電腦共用一個互聯網連接。
此外還修正了不少問題，所以被認為是基於Windows 9x核心中最穩定的版本。

## Windows 2000

微軟發行 [Windows 2000](../Page/Windows_2000.md "wikilink")（早期稱作的Windows NT
5.0）。它成功的部署在伺服器和工作站市場上，被認為是Windows中最好的版本。它參考了Windows 98的一些優點，尤其是使用者介面方面。

雖然Windows 98的用户可以升級為Windows 2000，但微軟並不視Windows 2000為家庭客户的目標。Windows
95/98 的產品線繼續發展並且發行新的版本：[Windows Me](../Page/Windows_Me.md "wikilink")。

Windows 2000 共有四種版本，

  - Windows 2000 專業版
  - Windows 2000 伺服器
  - Windows 2000 高級伺服器
  - Windows 2000 數據中心伺服器

## Windows Me

在2000年，微軟發行 [Windows Me](../Page/Windows_Me.md "wikilink") (Millennium
Edition)。相比 Windows
98，它主要在多媒體和互聯網功能上有所增強，并且首次引入「系统還原」功能。當系统損壞時，用户可以把系统還原至上一個「正常」的狀態。軟件[Windows
Movie Maker](../Page/Windows_Movie_Maker.md "wikilink") 亦首次包括在內。

Windows Me 本是作為介於 Windows 98 和 Windows XP間的過渡產品，所以 Windows Me
并不認為是一個「獨特」的作業系統。在許多情況下，使用 Windows 98SE 的用戶可以透過
Windows Update 機制提升到十分接近 Windows Me 的水準。對Windows Me
的主要批評為不夠穩定和缺乏對DOS真實模式的支援。所以又被戲稱為錯誤版本
(Mistake Edition)。

## Windows XP

在2001年，微軟發行 [Windows XP](../Page/Windows_XP.md "wikilink")，它整合了Windows
NT/2000 和Windows 3.1/95/98/ME。Windows XP 使用了Windows NT
5.1的核心，它的發行，標誌着Windows
NT開始普及並進入家庭客戶的市場，和16位元時代的終結。

Windows XP 有多種版本，

  - Windows XP Home Edition，目標為桌面電腦和筆記本電腦的使用者
  - Windows XP Home Edition N，和上一項相同，但根據歐盟的規定，不會預先安裝[Windows Media
    Player](../Page/Windows_Media_Player.md "wikilink")
  - Windows XP Professional Edition，目標為商務和專業使用者
  - Windows XP Professional Edition N，和上一項相同，但根據歐盟的規定，不會預先安裝[Windows
    Media Player](../Page/Windows_Media_Player.md "wikilink")
  - Windows XP Media Center Edition
    (MCE)，於2002年6月發行，目標為桌面電腦和筆記本電腦的使用者，但着重影音方面的功能
      - Windows XP Media Center Edition 2003，於2002年9月發行
      - Windows XP Media Center Edition 2004，於2003年11月發行
      - Windows XP Media Center Edition 2005，於2004年12月發行
  - Windows XP Tablet PC Edition，用於
    [平板电脑](../Page/平板电脑.md "wikilink")（裝置有觸控螢幕的筆記本電腦）
  - [Windows Embedded Standard
    2009](../Page/Windows_Embedded_Standard_2009.md "wikilink")（原名Windows
    XP Embedded），用於嵌入系统
  - Windows XP Starter Edition，目標為發展中國家的新使用者
  - Windows XP Professional x64 Edition，於2005年4月發行，实为[Windows Server
    2003](../Page/Windows_Server_2003.md "wikilink")
    [64位版本的桌面版](../Page/64位.md "wikilink")，用於64位元[微處理器系統](../Page/微處理器.md "wikilink")（AMD稱為AMD64,
    Intel稱為Intel EM64T）
  - Windows XP 64-bit
    Edition，用於Intel的[Itanium微處理器系統](../Page/Itanium.md "wikilink")，使用模擬的方式支持32位元軟件。於2005年9月停止研發，因為再没有廠商製造使用Itanium的工作站。
  - [Windows Fundamentals for Legacy
    PCs](../Page/Windows_Fundamentals_for_Legacy_PCs.md "wikilink"),基于Service
    Pack 2修改，在2006年三月发布的精簡客戶版本。只有參與微軟Software
    Assurance計劃的客戶才可使用。目標是給與這些公司一種昇級方案，令仍然使用Windows
    95, 98, ME 和 2000的舊電腦在未來數年仍有升級支援。大部份的應用程式都是經由Terminal
    Services或Citrix在遠端的電腦運行。

## Windows Server 2003

在2003年四月，微軟發行[Windows Server
2003](../Page/Windows_Server_2003.md "wikilink")，這是Windows 2000
Server後的一個重大升級。它加入了不少安全功能，一個稱為"Manage Your
Server"的工具，簡化了伺服器的設定，而且改進了效能。它的版本是Windows
NT 5.2。

在2005年12月，微軟發行Windows Server 2003 R2，它加入一些管理工具。

Windows Server 2003共有七種版本，

  - 小型商務伺服器
  - Web版
  - 標準版
  - 企業版
  - 數據中心版
  - 計算叢集版
  - 儲存伺服器

## Windows Vista

在2007年1月30日，[Windows Vista](../Page/Windows_Vista.md "wikilink")（原代號為
LongHorn，版本号為Windows NT
6.0。）正式發行。它引入了一種新的「限制使用者模式」，以取替現在的「默認是管理員的模式」，並且支援
[Windows OneCare Live](../Page/Windows_OneCare_Live.md "wikilink") 、內罝
[Windows Defender](../Page/Windows_Defender.md "wikilink")
、[防火牆以及用於硬碟加密的](../Page/防火牆.md "wikilink")
BitLocker ，令 Windows 的安全性增強不少。另外，[Windows
Vista亦加入了全新的](../Page/Windows_Vista.md "wikilink")
[Windows Aero](../Page/Windows_Aero.md "wikilink")
華麗介面，以及增強後的搜尋功能（Windows indexing
service）。

Windows Vista 有多種版本，

  - Windows Vista 簡易版（Starter）
  - Windows Vista 家用入門版（Home Basic）
  - Windows Vista 家用進階版（Home Premium）
  - Windows Vista 商業版（Business）
  - Windows Vista 企業版（Enterprise）
  - Windows Vista 旗艦版（Ultimate）

其中簡易版只在新兴市场国家(共139个国家)销售。\[1\]

除 Windows Vista
簡易版只有32位元外，其餘五個版本都會發行[32位元](../Page/32位元.md "wikilink")（[x86](../Page/x86.md "wikilink")）和[64位元](../Page/64位元.md "wikilink")（[x64](../Page/x64.md "wikilink")）兩種位元版本。

## Windows Server 2008

Windows Server 2008是微軟繼承Windows Server 2003的下一代伺服器作業系統的名称。Windows Server
2008在進行開發及測試時的代號為Windows Server "Longhorn"。

Windows Server 2008 將會是一套相等於Windows
Vista（代號為Longhorn）的伺服器系統，兩者很可能將會擁有很多相同功能；Vista及Server
2008與 XP及Server 2003 間存在相似的關係。（XP和Server 2003的代號分別為Whistler及Whistler
Server）

## Windows Home Server

Windows Home Server是微軟公司推出的一套家用伺服器作業系統。由微軟公司主席比爾·蓋茨在2007年1月7日的Consumer
Electronics Show中發表的這個系統，將會成為為家中多部電腦進行檔案分享、自動備份、遙距存取等等的解決方案。此系統是建基於
Windows Server 2003 SP2。

Windows Home Server 已於2007年9月29日在日本上市。

## Windows 7

Windows 7的核心沿用Vista的核心版本号为Windows NT 6.1。 Windows
7是以Vista進行更新，所以從Vista轉7的使用者，可以感覺到性能提升許多。

2008年10月28日，Windows 7
Pre-beta（版本號為6801）在PDC2008上向與會者推出。2009年1月9日（美國時間），Windows
7 Beta（版本號7000）向全世界用戶開放下載。

2009年4月30日，Windows 7 Release
Candidate（版本編號7100）釋出給[MSDN和](../Page/MSDN.md "wikilink")[TechNet用戶下載](../Page/TechNet.md "wikilink")，而在2009年5月5日，則開始供一般用戶下載。

2009年8月6日，太平洋時間6日上午10點，Windows 7 RTM （版本編號7600）正式發佈到 MSDN 與 TechNet
訂閱者下載網站。Windows 7於2009年10月22日在全球公開發售。

Windows 7繼續採用Windows
Vista的[Aero特效](../Page/Aero界面.md "wikilink")，但較Windows
Vista更為全面。

在 Windows 7
中，任务栏已完全经过重新设计，可以将软件“固定”到任务栏上。使用鼠标右键点按还有“JumpList”，快速访问您最常用的项目。

自從Windows Vista時代以上的作業系統都支援多點觸控。為了要讓觸控者便於點選，特意在 Windows 7
中加大了桌面按鈕，並加寬了下方開始與桌面按鈕。非觸控使用者，可以手動縮小工作列圖示及桌面圖示。

## Windows 8

Windows 8沿用Windows 7的核心，版本号为**Windows NT 6.2**。

[2009年11月](../Page/2009年11月.md "wikilink")，在[微軟發行](../Page/微軟.md "wikilink")[Windows
7之後便於](../Page/Windows_7.md "wikilink")[美國](../Page/美國.md "wikilink")[洛杉磯舉行的](../Page/洛杉磯.md "wikilink")「2009年[微軟專業開發者大會](../Page/微軟專業開發者大會.md "wikilink")」上展示的微軟最新產品路線圖，出現一個代號為“Windows
8”的產品，預告Windows 8將在2012年內推出。\[2\]

[2011年5月](../Page/2011年5月.md "wikilink")，微軟執行長[巴爾默](../Page/史蒂夫·巴爾默.md "wikilink")（Steve
Ballmer）表示，2012年將推出Windows
8新作業系統，其將支援[ARM架構](../Page/ARM架構.md "wikilink")，包括[平板電腦](../Page/平板電腦.md "wikilink")、[行動裝置](../Page/行動裝置.md "wikilink")、[筆記型電腦等](../Page/筆記型電腦.md "wikilink")，預告微軟將持續發展5大類技術領域，包括更自然的使用者介面、語言、HTML和JavaScript、以及雲端技術。其中使用者介面將涵蓋語音辨識、體感辨識、手寫辨識，以及觸控式螢幕等。
\[3\]\[4\]。

[2011年6月](../Page/2011年6月.md "wikilink")，微軟官方正式對Windows 8開發進展的透露，Windows
8將會對應平板裝置，同時將會針對[多點觸控操作模式予以調整](../Page/多點觸控.md "wikilink")，同時未來也將能以跨硬體平台的模式使用在各類裝置上，諸如手機或平板等行動裝置\[5\]

[2011年12月](../Page/2011年12月.md "wikilink")，微软正式公布了Windows
Store的詳情，並宣布Windows 8
Beta將於[2012年2月推出](../Page/2012年2月.md "wikilink")。\[6\]

[2012年6月](../Page/2012年6月.md "wikilink")，微软正式公布了[Windows Phone
8](../Page/Windows_Phone_8.md "wikilink")，Windows Phone 8采用和Windows
8相同的内核。

[2012年7月](../Page/2012年7月.md "wikilink")，微軟在[加拿大](../Page/加拿大.md "wikilink")[多倫多召開年度](../Page/多倫多.md "wikilink")「全球合作夥伴大會」（Worldwide
Partner Conference）宣布，Windows
8將在8月第1周發布[RTM版](../Page/RTM.md "wikilink")，10月底正式販售。\[7\]

2012年8月1日，Windows 8
[RTM版編譯完成](../Page/RTM.md "wikilink")。RTM版將與零售版一同於10月25日一起上市。

2012年10月25日，微軟在[紐約宣布Windows](../Page/紐約.md "wikilink") 8正式上市。

Windows 8基於Windows 7核心上，仍包含了許多的新功能：

  - Metro界面：[Windows 8](../Page/Windows_8.md "wikilink") 採用重新設計的
    [Metro](../Page/Metro.md "wikilink")
    風格「開始頁面」，而取消了已沿用多年的「開始功能表」，Metro界面適用於[觸控式螢幕和](../Page/觸控式螢幕.md "wikilink")[滑鼠控制](../Page/滑鼠.md "wikilink")。這個新的開始頁面類似
    [Windows Phone
    7](../Page/Windows_Phone_7.md "wikilink")，包含了許多生動的應用圖標。登入新的開始頁面後不會即時載入桌面，以增加速度。同時該界面上也包含了“桌面”图標以方便用戶回到傳統的系統桌面。Metro界面包含一个新的登入和鎖定頁面，並會顯示時間日期及通知。

<!-- end list -->

  - Charm工具列：[Windows 8](../Page/Windows_8.md "wikilink") 中還包含了
    Charm（超超級按鈕）工具列和Live Tile（動態磁貼）。 Charm工具列上有5
    個按鈕，分別是：搜索、分享、開始、設備、設置。新版Charm
    工具列位於畫面右側，而開發者預覽版中則位於界面左下方。

<!-- end list -->

  - [Ribbon界面](../Page/Ribbon.md "wikilink")：[資源管理器會採用與](../Page/資源管理器.md "wikilink")[Microsoft
    Office
    2010類似的界面](../Page/Microsoft_Office_2010.md "wikilink")，例如將「複製」和「刪除」等功能鍵放到視窗上方，同時提供了簡潔和專業兩種界面選擇。用戶可以在文件移動或複製的過程中，進行停止、暫停和恢復。

<!-- end list -->

  - 新的資源管理器支持右單擊加載[ISO](../Page/ISO_9660.md "wikilink")、[IMG](../Page/IMG.md "wikilink")
    和[VHD](../Page/VHD.md "wikilink") 文件。而[Windows
    7](../Page/Windows_7.md "wikilink")
    僅支持通過磁盤管理器或[命令行界面加載](../Page/cmd.md "wikilink")[VHD](../Page/VHD.md "wikilink")。
  - Windows 8 中內置了安全防護軟件[Windows
    Defender](../Page/Windows_Defender.md "wikilink")，內核和界面和[Microsoft
    Security
    Essentials相同](../Page/Microsoft_Security_Essentials.md "wikilink")。旨在提高系統安全性，查殺惡意軟件與引導啟動的[惡意軟件](../Page/惡意軟件.md "wikilink")。
  - Windows 7 軟體都可以直接在 Windows 8 上應用。
  - 更新過的工作管理員會將未在螢幕上執行的軟件暫時凍結，即不佔用CPU。
  - 全新的「Reset and Refresh PC」（重置與刷新 PC）功能，讓你可以很容易地把電腦抹乾淨並還原。
  - Windows 8 將內建 [Hyper-V](../Page/Hyper-V.md "wikilink")
    虛擬化軟體（原先只支援[Windows Server
    2008](../Page/Windows_Server_2008.md "wikilink")）。
  - 多螢幕將支援橫跨螢幕的單一桌布，以及各螢幕獨立的工作列。
  - 強化了[放大鏡功能](../Page/放大鏡（Windows）.md "wikilink")。
  - Mail、Photos、Calendar和 People 等軟件提供 Metro 式的更新。
  - 個人化設定可以在多台 Windows 8 裝置間同步。
  - 硬件配備需求降低，就算是 Lenovo S10（Intel Atom N270 1.6GHz，DDR2 667MHz
    1024MB）也可正常運行 Windows 8。
  - 內置 [近場通訊](../Page/近場通訊.md "wikilink") 功能的 Windows 8 裝置將可以用
    Tap-to-share 功能分享內容。
  - 登入系統會使用以[相片為基礎](../Page/相片.md "wikilink")。
  - Windows 8 會支援 [ARM
    架構處理器芯片](../Page/ARM架構.md "wikilink")，以改善在[平板電腦上的表現](../Page/平板電腦.md "wikilink")。\[8\]\[9\]<ref name="Why Microsoft has made developers horrified about coding for Windows 8">

</ref>

  - Windows 8 將原生支援[USB 3.0](../Page/USB_3.0.md "wikilink")，並向下相容USB
    2.0和USB1.1，用戶將會得到更快的傳送速度。\[10\]

<!-- end list -->

  - 圖片鎖定：除了傳統的密碼解鎖方式以外，一个新的授權方式允许用戶采採用一組滑動手勢在選定的圖片上来解鎖螢幕。這組手勢會紀錄滑動軌跡的形狀、方向、起點及終點，但僅限於點、線條或圓。微軟認為這樣的解鎖方式可以提高登入系統的速度。三次錯誤的手勢輸入會導致系統被鎖定，並要求用戶輸入傳統密碼。

## Windows 8.1

[Windows 8.1](../Page/Windows_8.1.md "wikilink")，又名[Windows
Blue](../Page/Windows_Blue.md "wikilink")，是微軟一個針對Windows
8的升級版。\[11\]這個升級版已於2013年10月17日正式發布。\[12\]

## Windows 10

Windows 10是續已發行的Windows 8、Windows 8.1、Windows Phone 8、Windows Phone
8.1、Xbox One系統、Windows Server 2012
R2後的新版作業系統。2014年9月30日，微軟正式發行技術預覽版，在2015年7月29日正式發行。

## Windows Server 2016

2014年10月1日，微軟推出「Windows Server Technical Preview」，這是Windows Server
2016（當時仍稱vNext）的第一個測試版本，這一版本的目標使用者為企業使用者。第一個技術預覽版本原定於2015年4月15日發布，但後來微軟推出了一個工具導致該版本發布日期延期，直至2015年5月4日第二個技術預覽版本推出。2015年8月19日，Windows
Server 2016的第三個預覽版本發布。2015年11月19日，Windows Server
2016的第四個預覽版本發布。2016年4月27日，微軟發布Windows Server
2016的第五個預覽版本。

Windows Server 2016於2016年9月26日正式推出，於同年10月12日正式發售。與前代不同的是，Windows Server
2016是根據處理器的核心數而非處理器的數量進行授權，在此之前，BizTalk Server 2013以及SQL Server
2014等就曾採用過這種授權方式。

## 微軟作業系統的歷史

### MS-DOS 產品的演變

  - MS-DOS 和 [PC-DOS](../Page/PC-DOS.md "wikilink")
  - Windows 1.0
  - Windows 2.0
  - Windows 2.1（又名 Windows/286 和 Windows/386）
  - Windows 3.0、Windows 3.1、Windows 3.11（和 Windows for Workgroups）
  - Windows 95（Windows 4.0）
  - Windows 98（Windows 4.1）、Windows 98 Second Edition
  - Windows Millennium Edition（Windows 4.9）

### OS/2 產品的演變

  - 16位元版本：OS/2 1.0（仅CLI）、1.1, 1.2, 1.3
  - 32位元版本：OS/2 2.0, 2.1, 2.11, 2.11 SMP, Warp 3, Warp 4

### 現在NT產品線的演變

  - Windows NT 3.1、3.5、3.51
  - Windows NT 4.0
  - Windows 2000（Windows NT 5.0）
  - Windows XP（Windows NT 5.1）
  - Windows Server 2003（Windows NT 5.2）
  - Windows Vista（Windows NT 6.0）
  - Windows Server 2008（Windows NT 6.0）
  - Windows 7（Windows NT 6.1）
  - Windows Server 2008 R2（Windows NT 6.1）
  - Windows Home Server
  - Windows 8（Windows NT 6.2）
  - Windows Server 2012（Windows NT 6.2）
  - Windows 8.1（Windows NT 6.3）
  - Windows Server 2012 R2（Windows NT 6.3）
  - Windows 10（Windows NT 6.4/10.0）
  - Windows Server 2016 (Windows NT 10)

## 重要里程

[Windows_Updated_Family_Tree.png](https://zh.wikipedia.org/wiki/File:Windows_Updated_Family_Tree.png "fig:Windows_Updated_Family_Tree.png")

| 日期          | 16位 x86平台                                                        | 32位 x86([IA-32](../Page/IA-32.md "wikilink"))平台                                                          | 64位 [x86-64](../Page/x86-64.md "wikilink")([AMD64](../Page/AMD64.md "wikilink"))平台               | 開發代號             |
| ----------- | ---------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------ | ---------------- |
| 1985年11月20日 | [Windows 1.0](../Page/Windows_1.0.md "wikilink")                 |                                                                                                          |                                                                                                  | width-"28%" |    |
| 1987年12月9日  | [Windows 2.0](../Page/Windows_2.0.md "wikilink")                 |                                                                                                          |                                                                                                  |                  |
| 1990年5月22日  | [Windows 3.0](../Page/Windows_3.0.md "wikilink")                 |                                                                                                          |                                                                                                  |                  |
| 1992年4月6日   | [Windows 3.1](../Page/Windows_3.1.md "wikilink")                 |                                                                                                          |                                                                                                  | Janus            |
| 1992年10月27日 | [Windows for Workgroups 3.1](../Page/Windows_3.x.md "wikilink")  |                                                                                                          |                                                                                                  |                  |
| 1993年7月27日  |                                                                  | [Windows NT 3.1](../Page/Windows_NT_3.1.md "wikilink")                                                   |                                                                                                  |                  |
| 1993年11月8日  | [Windows for Workgroups 3.11](../Page/Windows_3.x.md "wikilink") |                                                                                                          |                                                                                                  |                  |
| 1994年9月21日  |                                                                  | [Windows NT 3.5](../Page/Windows_NT_3.5.md "wikilink")                                                   |                                                                                                  |                  |
| 1995年5月30日  |                                                                  | [Windows NT 3.51](../Page/Windows_3.x.md "wikilink")                                                     |                                                                                                  |                  |
| 1995年8月24日  | [Windows 95](../Page/Windows_95.md "wikilink")                   |                                                                                                          | Chicago                                                                                          |                  |
| 1996年8月24日  |                                                                  | [Windows NT 4.0](../Page/Windows_NT_4.0.md "wikilink")                                                   |                                                                                                  | Cairo            |
| 1998年6月25日  | [Windows 98](../Page/Windows_98.md "wikilink")                   |                                                                                                          | Memphis                                                                                          |                  |
| 1999年5月9日   | [Windows 98 SE](../Page/Windows_98_SE.md "wikilink")             |                                                                                                          |                                                                                                  |                  |
| 2000年2月17日  |                                                                  | [Windows 2000](../Page/Windows_2000.md "wikilink")                                                       |                                                                                                  | For SP1 Asteroid |
| 2000年9月14日  | [Windows Me](../Page/Windows_Me.md "wikilink")                   |                                                                                                          | Millenium                                                                                        |                  |
| 2001年10月25日 |                                                                  | [Windows XP](../Page/Windows_XP.md "wikilink")                                                           |                                                                                                  | Whistler         |
| 2003年4月25日  |                                                                  | [Windows Server 2003](../Page/Windows_Server_2003.md "wikilink")                                         | Whistler Server                                                                                  |                  |
| 2003年12月18日 |                                                                  | [Windows XP Media Center Edition 2003](../Page/Windows_XP的版本#Media_Center_Edition（媒體中心版本）.md "wikilink") |                                                                                                  | Freestyle        |
|             |                                                                  | **Windows XP Media Center Edition 2004**                                                                 |                                                                                                  | Harmony          |
| 2004年10月12日 |                                                                  | [Windows XP Media Center Edition 2005](../Page/Windows_XP的版本#Media_Center_Edition（媒體中心版本）.md "wikilink") |                                                                                                  | Symphony         |
| 2005年4月25日  |                                                                  |                                                                                                          | [Windows XP Professional x64 Edition](../Page/Windows_XP_Professional_x64_Edition.md "wikilink") |                  |
| 2006年7月8日   |                                                                  | [Windows Fundamentals for Legacy PCs](../Page/Windows_Fundamentals_for_Legacy_PCs.md "wikilink")         |                                                                                                  | Eiger            |
| 2006年11月30日 |                                                                  | [Windows Vista](../Page/Windows_Vista.md "wikilink") 商業用途                                                | Longhorn                                                                                         |                  |
| 2007年1月30日  |                                                                  | [Windows Vista](../Page/Windows_Vista.md "wikilink") 個人用途;在50個國家發佈                                       | Longhorn                                                                                         |                  |
| 2007年11月7日  |                                                                  | [Windows Home Server](../Page/Windows_Home_Server.md "wikilink")                                         | Quattro                                                                                          |                  |
| 2008年2月27日  |                                                                  | [Windows Server 2008](../Page/Windows_Server_2008.md "wikilink")                                         | Longhorn Server                                                                                  |                  |
| 2009年10月22日 |                                                                  | [Windows 7](../Page/Windows_7.md "wikilink")                                                             | Blackcomb (Vienna)                                                                               |                  |
| 2009年10月22日 |                                                                  |                                                                                                          | [Windows Server 2008 R2](../Page/Windows_Server_2008_R2.md "wikilink")                           |                  |
| 2011年4月5日   |                                                                  |                                                                                                          | [Windows Home Server 2011](../Page/Windows_Home_Server_2011.md "wikilink")                       |                  |
| 2012年10月26日 |                                                                  | [Windows 8](../Page/Windows_8.md "wikilink")/[Windows RT](../Page/Windows_RT.md "wikilink")（ARM架構）       | Milestone                                                                                        |                  |
| 2012年10月26日 |                                                                  |                                                                                                          | [Windows Server 2012](../Page/Windows_Server_2012.md "wikilink")                                 |                  |
| 2013年10月18日 |                                                                  | [Windows 8.1](../Page/Windows_8.1.md "wikilink")/[Windows RT 8.1](../Page/Windows_RT_8.1.md "wikilink")  | Blue                                                                                             |                  |
| 2013年10月18日 |                                                                  |                                                                                                          | [Windows Server 2012 R2](../Page/Windows_Server_2012_R2.md "wikilink")                           |                  |
| 2015年7月29日  |                                                                  | [Windows 10](../Page/Windows_10.md "wikilink")                                                           | Threshold                                                                                        |                  |
| 2016年9月26日  |                                                                  |                                                                                                          | [Windows Server 2016](../Page/:Windows_Server_2016.md "wikilink")                                | Redstone         |

## 其它

  - [Windows CE](../Page/Windows_CE.md "wikilink")
  - [Windows Mobile](../Page/Windows_Mobile.md "wikilink")
  - [Windows Phone](../Page/Windows_Phone.md "wikilink")
  - [Windows Embedded](../Page/Windows_Embedded.md "wikilink")
  - [Windows Server](../Page/Windows_Server.md "wikilink")

## 參見

  - [Comparison of operating
    systems](../Page/Comparison_of_operating_systems.md "wikilink")
  - [Apple v. Microsoft](../Page/Apple_v._Microsoft.md "wikilink")
  - [藍屏死機](../Page/藍屏死機.md "wikilink")
  - [History of computing
    hardware](../Page/History_of_computing_hardware.md "wikilink")
  - [作業系統](../Page/作業系統.md "wikilink")
  - [ReactOS](../Page/ReactOS.md "wikilink")
  - [Microsoft Version
    Number](../Page/Microsoft_Version_Number.md "wikilink")
  - [Microsoft codenames](../Page/Microsoft_codenames.md "wikilink")

## 參考文獻

<div class="references-small">

<references />

</div>

## 外部連結

  - [PC Magazine's long article titled "20 Years of
    Windows"](http://www.pcmag.com/article2/0,1895,1868435,00.asp)
  - [Old Os (help get your old PC surfing
    again)](https://web.archive.org/web/20170105123757/http://www.oldos.org/)
  - [ToastyTech GUI Gallery (screenshots of early versions of
    Windows)](http://www.toastytech.com/guis/)
  - [Windows NT and VMS: The Rest of the Story (discussion of VMS and
    WNT
    similarities)](https://web.archive.org/web/20001117084600/http://www.win2000mag.com/Articles/Index.cfm?ArticleID=4494)
  - [Early Windows history
    (1982-1993)](https://web.archive.org/web/20060315163942/http://www.osviews.com/modules.php?op=modload&name=News&file=article&sid=4484)

[Category:计算机历史](../Category/计算机历史.md "wikilink")
[Category:微軟](../Category/微軟.md "wikilink")

1.
2.
3.  [新一代作業系統Windows 8明年出爐](http://www.ithome.com.tw/itadm/article.php?c=67879)
    iThome 2011-05-29
4.
5.  [微軟Windows 8首次公開亮相](http://bnext.com.tw/article/view/cid/134/id/18710)
     數位時代 2011-06-02
6.  \[[http://blogs.msdn.com/b/windowsstore/archive/2011/12/06/announcing-the-new-windows-store.aspx\]Microsoft](http://blogs.msdn.com/b/windowsstore/archive/2011/12/06/announcing-the-new-windows-store.aspx%5DMicrosoft)
    2011-12-06
7.
8.  [Windows 8
    新功能總整理](http://chinese.engadget.com/2011/09/13/windows-8-details-new-features-ui-enhancements-and-everything/)ENGADGET
    中文版 Andy Yang 2011-9-14
9.
10. [Microsoft 保證 Windows 8 將會有「強力的」USB 3.0
    支援](http://chinese.engadget.com/2011/08/24/microsoft-promises-robust-usb-3-0-support-in-windows-8/)ENGADGET
    中文版 2011-8-24
11. [外媒稱Windows Blue或明年發布
    非Win9](http://software.it168.com/a2012/0814/1384/000001384488.shtml)
12. [Windows 8.1正式放布](http://news.e-works.net.cn/category10/news52652.htm)