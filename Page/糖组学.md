**糖组学**（）又稱**醣體學**，是综合研究（[糖的全部补体](../Page/糖.md "wikilink")，无论是游离存在的还是在生物体的更复杂的分子中存在的），包括遗传，生理，病理等方面\[1\]\[2\]。糖组学“是给定细胞类型或生物体的所有聚糖结构的系统研究”，并且是[糖生物学的一个子集](../Page/糖生物学.md "wikilink")\[3\]。Glycomics一詞是從表示[糖或](../Page/糖.md "wikilink")[甜味的](../Page/甜味.md "wikilink")「glyco-」與[基因組學](../Page/基因組學.md "wikilink")（genomics，涉及[基因](../Page/基因.md "wikilink")）和[蛋白質組學](../Page/蛋白質組學.md "wikilink")（proteomics，涉及[蛋白質](../Page/蛋白質.md "wikilink")）之命名規律而來。此規律也導致了（glycome）一字的出現，用來表示生物個體內擁有的所有[碳水化合物](../Page/碳水化合物.md "wikilink")。

## 挑战

  - 糖的复杂性：关于它们的结构，它们不是线性的，而是高度分枝。
    此外，[聚糖可以被修饰](../Page/聚糖.md "wikilink")（修饰的糖），这增加了它的复杂性。
  - [聚糖的复杂生物合成途径](../Page/聚糖.md "wikilink")。
  - 通常发现聚糖与[蛋白质](../Page/蛋白质.md "wikilink")（[糖蛋白](../Page/糖蛋白.md "wikilink")）结合或与[脂质](../Page/脂质.md "wikilink")（[糖脂](../Page/糖脂.md "wikilink")）缀合。
  - 与[基因组不同](../Page/基因组.md "wikilink")，聚糖具有很强的动态性。

这一研究领域必须处理在其他应用生物学领域尚未见到的固有的复杂程度。68个构建单元（用于DNA，RNA和蛋白质的分子; 脂类的类别;
糖类的糖键类型）为构成细胞整个生命的分子编排提供了结构基础。[DNA和](../Page/DNA.md "wikilink")[RNA有四个构建块](../Page/RNA.md "wikilink")（[核苷或](../Page/核苷.md "wikilink")[核苷酸](../Page/核苷酸.md "wikilink")）。
基于[酮酰基和](../Page/酮酰基.md "wikilink")[异戊二烯将](../Page/异戊二烯.md "wikilink")[脂质分为八类](../Page/脂质.md "wikilink")。
[蛋白质有](../Page/蛋白质.md "wikilink")20个（[氨基酸](../Page/氨基酸.md "wikilink")）。
[糖类有](../Page/糖类.md "wikilink")32种类型的糖键\[4\]。虽然这些结构单元只能线性地附着于蛋白质和基因，但是它们可以排列成用于糖的分支阵列，进一步提高了复杂程度。

除此之外，所涉及的众多蛋白质的复杂性不仅仅是碳水化合物的载体，[糖蛋白](../Page/糖蛋白.md "wikilink")，而是特异性参与与碳水化合物反应的蛋白质：

  - 用于合成，调节和降解的碳水化合物特异性[酶](../Page/酶.md "wikilink")
  - [凝集素](../Page/凝集素.md "wikilink")，各种碳水化合物结合蛋白
  - [受体](../Page/受体_\(生物化学\).md "wikilink")，循环或膜结合的碳水化合物结合受体

## 使用的工具

以下是聚糖分析中常用技术的例子\[5\]

### 高分辨质谱法（MS）和高效液相色谱法（HPLC）

最常用的方法是[质谱法](../Page/质谱法.md "wikilink")（MS）和[高效液相色谱法](../Page/高效液相色谱法.md "wikilink")（HPLC），其中聚糖部分从目标物酶的或化学的裂解并进行分析\[6\]。在糖脂的情况下，它们可以直接分析而不分离脂质成分。

## 软件和数据库

有几种在线软件和数据库可供糖组研究使用。 这包括：

  -
  -
## 参看

  - [糖生物学](../Page/糖生物学.md "wikilink")
  - [组学](../Page/组学.md "wikilink")
      - [基因組學](../Page/基因組學.md "wikilink")
      - [代谢物组学](../Page/代谢物组学.md "wikilink")
      - [蛋白質組學](../Page/蛋白質組學.md "wikilink")
  - [系统生物学](../Page/系统生物学.md "wikilink")

## 参考资料

## 外部链接

  - [National Center for Functional Glycomics
    (NCFG)](https://ncfg.hms.harvard.edu)

  - [Glycomics - Glycan Recognizing
    Proteins](http://www.bio-world.com/glycobiology/lectins.html)

  - [List of Glycomics web
    sites](http://www.functionalglycomics.org/static/consortium/links2Website.shtml)

  - [Glycomics Portal at
    CCRC](https://web.archive.org/web/20120730034937/http://glycomics.ccrc.uga.edu/GlycomicsPortal/welcome.action),
    Software for glycomics

  - [Functional Glycomics Gateway](http://www.functionalglycomics.org/),
    a collaboration between the [Consortium for Functional
    Glycomics](../Page/Consortium_for_Functional_Glycomics.md "wikilink")
    and [Nature Publishing
    Group](../Page/Nature_Publishing_Group.md "wikilink")

  - [glycosciences.de](http://www.glycosciences.de/) This site provides
    databases and bioinformatics tools for glycobiology and glycomics.

  - [GlycomeDB](http://www.glycome-db.org/About.action), A carbohydrate
    structure [metadatabase](../Page/metadatabase.md "wikilink")

  - [EuroCarbDB](http://www.ebi.ac.uk/eurocarb/home.action)

  - [GlycoBase](http://glycobase.nibrt.ie) A web HPLC/UPLC resource that
    contains elution positions expressed as glucose unit values.

  - [ProGlycAn](http://www.proglycan.com/) A short introduction to
    glycan analysis and a nomenclature for N-Glycans

  -
[category:组学](../Page/category:组学.md "wikilink")

[糖组学](../Category/糖组学.md "wikilink")
[Category:糖化学](../Category/糖化学.md "wikilink")
[Category:糖](../Category/糖.md "wikilink")

1.

2.

3.

4.  [ucsd
    news](http://ucsdnews.ucsd.edu/newsrel/health/09-0868Molecules.asp)
    article *Do 68 Molecules Hold the Key to Understanding Disease?*
    published September 3, 2008

5.
6.