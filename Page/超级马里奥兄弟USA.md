是[任天堂在北美](../Page/任天堂.md "wikilink")1988年10月發售的[红白机平台](../Page/红白机.md "wikilink")[动作遊戲](../Page/平台动作游戏.md "wikilink")。

遊戲原本應為《[超级马里奥兄弟2](../Page/超级马里奥兄弟2.md "wikilink")》，但由於美國任天堂發現日本的超级马里奥兄弟2過於困難，任天堂迫於時間已經來不及重新再開發一個完整的遊戲，因此拿了一個只在日本本土發行過的遊戲《[夢工廠悸動恐慌](../Page/夢工廠悸動恐慌.md "wikilink")》，簡單修改了其中的角色為瑪利歐兄弟倆、碧琪公主及奇諾比奧就草草推出，以改編作為[瑪利歐系列的續集發售](../Page/瑪利歐系列.md "wikilink")。

這款遊戲成為商業上的成功，並最終遊戲也在日本發售。因為其成功的銷售產量、在世界各地超級馬里奧兄弟USA
一直被視為經典的超級馬里奧遊戲，包括日本。它重新發佈超級馬里奧全明星集合中，和它的某些設計項目已列入超級馬里奧
Wii U 系統的 3D 世界。

## 外部链接

  - [TGWTG video feature on the story behind Super Mario 2 and Doki Doki
    Panic](http://thatguywiththeglasses.com/bt/guruandwez/retroco/7877-smb2ddp)
  - [BS *Super Mario
    US*](http://www.themushroomkingdom.net/games/bssmusa)
  - [Super Mario Bros. 2 Madness - From Doki Doki Panic to
    SMB2](http://www.themushroomkingdom.net/smb2_ddp.shtml)
  - [*Super Mario
    Bros. 2*](https://web.archive.org/web/20140115125526/http://www.nindb.net/game/super-mario-bros-2.html)
    at NinDB

[Super 2](../Category/红白机游戏.md "wikilink") [Super
2](../Category/動作遊戲.md "wikilink") [Super
2](../Category/Game_Boy_Advance遊戲.md "wikilink") [Super
2](../Category/超级马里奥系列电子游戏.md "wikilink") [Super
2](../Category/任天堂游戏.md "wikilink") [Super
2](../Category/神游科技游戏.md "wikilink")
[Category:官方简体中文化游戏](../Category/官方简体中文化游戏.md "wikilink")
[Category:1988年电子游戏](../Category/1988年电子游戏.md "wikilink")