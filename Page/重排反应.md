**重排反应**（Rearrangement
reaction）是[分子的碳骨架发生重排生成](../Page/分子.md "wikilink")[结构异构体的化学反应](../Page/结构异构体.md "wikilink")，是[有机反应中的一大类](../Page/有机反应.md "wikilink")。\[1\]重排反应通常涉及[取代基由一个原子转移到同一个分子中的另一个原子上的过程](../Page/取代基.md "wikilink")，以下例子中取代基R由碳原子1移动至碳原子2：

[General_scheme_rearrangement.svg](https://zh.wikipedia.org/wiki/File:General_scheme_rearrangement.svg "fig:General_scheme_rearrangement.svg")

分子间重排反应也有可能发生。
通常不用弯箭头表示的电子转移图来描述重排反应的机理。例如在[Wagner-Meerwein重排反应中](../Page/Wagner-Meerwein重排反应.md "wikilink")，烃基迁移的实际机理很可能涉及烃基沿键的类似流动性的转移，而非离子性的断键与成键。而在[周环反应中](../Page/周环反应.md "wikilink")，以轨道间相互作用来解释机理要比用电子转移来描述清晰得多。因此虽然在很多情况下可以画出重排反应的电子转移图机理，但它们极有可能与真实机理有较大偏差。

一些典型的重排反应：

  - [1,2-重排反应](../Page/1,2-重排反应.md "wikilink")
  - [周环反应](../Page/周环反应.md "wikilink")
  - [烯烃复分解反应](../Page/烯烃复分解反应.md "wikilink")

## 1,2-重排反应

所谓1,2-重排反应，是指化合物中一个原子上的取代基转移到另一个原子上。1,2-重排通常发生在相邻两个原子上，但重排到更远的原子上也是有可能的。
例如， [Wagner-Meerwein重排反应](../Page/Wagner-Meerwein重排反应.md "wikilink")：

  -
    [Isoborneol2CampheneConversion.svg](https://zh.wikipedia.org/wiki/File:Isoborneol2CampheneConversion.svg "fig:Isoborneol2CampheneConversion.svg")

以及 [贝克曼重排](../Page/贝克曼重排.md "wikilink")：

  -
    [Beckmann_rearrangement.png](https://zh.wikipedia.org/wiki/File:Beckmann_rearrangement.png "fig:Beckmann_rearrangement.png")

## 周环反应

周环反应是指这样一类反应，其过渡态中存在着环状结构，同时在这一结构中存在着协同进行的多重碳碳键的断裂与生成。 例如,氢转移反应

和[克莱森重排反应](../Page/克莱森重排反应.md "wikilink")：

  -
    [Claisen_Rearrangement_Scheme.png](https://zh.wikipedia.org/wiki/File:Claisen_Rearrangement_Scheme.png "fig:Claisen_Rearrangement_Scheme.png")

## 烯烃复分解反应

根据[伍德沃德-霍夫曼规则](../Page/伍德沃德-霍夫曼规则.md "wikilink")，两个烯烃直接发生\[2+2\][环加成反应是对称禁阻的](../Page/环加成反应.md "wikilink")，[活化能很高](../Page/活化能.md "wikilink")。20世纪70年代时，Hérison和肖万提出了烯烃复分解反应的环加成机理，该机理是目前最广泛接受的反应机制。\[2\]
其中，首先发生烯烃双键与金属卡宾配合物的\[2+2\]环加成反应，生成金属杂环丁烷衍生物中间体。然后该中间体经由逆环加成反应，既可得到反应物，也可得到新的烯烃和卡宾配合物。新的金属卡宾再与另一个烯烃发生类似的反应，最后生成另一个新的烯烃，并再生原金属卡宾。

金属催化剂[*d*轨道与烯烃的相互作用降低了活化能](../Page/原子轨道.md "wikilink")，使烯烃复分解反应在适宜温度下就可发生，摆脱了以前多催化组分以及强[路易斯酸性的反应条件](../Page/路易斯酸.md "wikilink")。

[Metathesis_mechanism_jypx3.png](https://zh.wikipedia.org/wiki/File:Metathesis_mechanism_jypx3.png "fig:Metathesis_mechanism_jypx3.png")

## 参见

  - [:Category:重排反应](../Category/重排反应.md "wikilink")

## 参考资料

<div class="references-small">

<references/>

</div>

[\*](../Category/重排反应.md "wikilink")

1.  March, Jerry (1985). *Advanced Organic Chemistry, Reactions,
    Mechanisms and Structure*, third Edition, John Wiley & Sons. ISBN
    0-471-85472-7.
2.  Hérisson, J.-L.; Chauvin, Y. *Macromol. Chem.* **1970**, *141*, 161.