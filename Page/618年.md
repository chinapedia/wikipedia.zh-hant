## 大事记

  - **[中国](../Page/中国.md "wikilink")**
      - [1月12日](../Page/1月12日.md "wikilink")——[曹武徹舉兵反](../Page/曹武徹.md "wikilink")[隋](../Page/隋.md "wikilink")，建元[通聖](../Page/通聖.md "wikilink")。
      - [4月11日](../Page/4月11日.md "wikilink")——[隋炀帝死于兵變](../Page/隋炀帝.md "wikilink")。[宇文化及立秦王](../Page/宇文化及.md "wikilink")[楊浩為傀儡皇帝](../Page/楊浩.md "wikilink")。
      - [6月12日](../Page/6月12日.md "wikilink")——[隋恭帝](../Page/隋恭帝.md "wikilink")[楊侑禪位](../Page/楊侑.md "wikilink")[李渊](../Page/李渊.md "wikilink")（[唐高祖](../Page/唐高祖.md "wikilink")），建立[唐朝](../Page/唐.md "wikilink")，定都长安（今陕西西安市安南）。
      - [6月22日](../Page/6月22日.md "wikilink")——[王世充與](../Page/王世充.md "wikilink")[元文都](../Page/元文都.md "wikilink")、[盧楚等擁立越王](../Page/盧楚.md "wikilink")[楊侗為](../Page/楊侗.md "wikilink")[隋朝皇帝](../Page/隋朝.md "wikilink")，史稱[皇泰主](../Page/皇泰主.md "wikilink")。
      - [唐高祖](../Page/唐高祖.md "wikilink")[李淵命](../Page/李淵.md "wikilink")[裴寂等修订](../Page/裴寂.md "wikilink")[律令](../Page/律令.md "wikilink")，十一月颁布“五十三条格”
      - [高曇晟自封大乘皇帝](../Page/高曇晟.md "wikilink")，建元[法輪](../Page/法輪.md "wikilink")。
      - [梁師都稱帝](../Page/梁師都.md "wikilink")，國號“梁”，建元[永隆](../Page/永隆_\(梁師都\).md "wikilink")。
      - [高開道自立為燕王](../Page/高開道.md "wikilink")，改元[始兴](../Page/始兴_\(高开道\).md "wikilink")，以[漁陽郡為國都](../Page/漁陽郡.md "wikilink")，後歸附[高曇晟](../Page/高曇晟.md "wikilink")，後反擊殺高曇晟並兼併其勢力，與[竇建德合攻](../Page/竇建德.md "wikilink")[幽州總管](../Page/幽州.md "wikilink")[羅藝](../Page/羅藝.md "wikilink")。
      - 十一月，[竇建德稱夏王](../Page/竇建德.md "wikilink")，改元[五鳳](../Page/五鳳.md "wikilink")，都乐寿(河北献县)。
      - [郭子和歸附](../Page/郭子和.md "wikilink")[唐朝](../Page/唐朝.md "wikilink")。
      - [唐朝將領](../Page/唐朝.md "wikilink")[劉文靜](../Page/劉文靜.md "wikilink")、[殷開山等攻擊](../Page/殷開山.md "wikilink")[薛舉](../Page/薛舉.md "wikilink")，但遭擊敗，[薛舉欲趁勢進攻](../Page/薛舉.md "wikilink")[長安卻病逝](../Page/長安.md "wikilink")，[薛仁杲繼任](../Page/薛仁杲.md "wikilink")。
      - 七月[瓦崗軍於](../Page/瓦崗軍.md "wikilink")[黎陽擊潰自](../Page/黎陽.md "wikilink")[江都北上的](../Page/江都.md "wikilink")[宇文化及](../Page/宇文化及.md "wikilink")，但受到極大損傷。
      - 九月[王世充趁虛擊潰](../Page/王世充.md "wikilink")[瓦崗軍](../Page/瓦崗軍.md "wikilink")，[單雄信歸附](../Page/單雄信.md "wikilink")[王世充](../Page/王世充.md "wikilink")，[王伯當](../Page/王伯當.md "wikilink")、[李密投奔](../Page/李密.md "wikilink")[唐朝](../Page/唐朝.md "wikilink")，同年起兵反唐。
      - 九月[宇文化及杀](../Page/宇文化及.md "wikilink")[杨浩称帝](../Page/杨浩.md "wikilink")，國號「許」，年號[天壽](../Page/天壽.md "wikilink")，[隋朝覆亡](../Page/隋朝.md "wikilink")。
      - 6月至11月[淺水原之戰](../Page/淺水原之戰.md "wikilink")，[唐朝](../Page/唐朝.md "wikilink")[李世民](../Page/李世民.md "wikilink")、[劉文靜](../Page/劉文靜.md "wikilink")、[殷開山率軍在](../Page/殷開山.md "wikilink")[淺水原](../Page/淺水原.md "wikilink")（今[陝西](../Page/陝西.md "wikilink")[長武東北](../Page/長武.md "wikilink")）擊敗[隴西割據勢力](../Page/隴西.md "wikilink")[薛舉](../Page/薛舉.md "wikilink")、[薛仁杲](../Page/薛仁杲.md "wikilink")，[唐朝奪取](../Page/唐朝.md "wikilink")[隴西地方](../Page/隴西.md "wikilink")。
      - 十一月，[李軌佔](../Page/李軌.md "wikilink")[河西](../Page/河西.md "wikilink")（今[甘肅](../Page/甘肅.md "wikilink")[河西走廊](../Page/河西走廊.md "wikilink")）五郡之地，改稱涼帝。

### 中东及地中海地区

  - [萨珊波斯将领Shahrbaraz率军攻击并占领](../Page/萨珊波斯.md "wikilink")[东罗马帝国埃及省](../Page/东罗马帝国.md "wikilink")。

### 歐洲

  - [古不列顛帝國開始侵占東歐地區其勢力向東方擴張](../Page/古不列顛帝國.md "wikilink")。

## 出生

  -
## 逝世

  - [11月8日](../Page/11月8日.md "wikilink")——[羅馬教宗](../Page/羅馬教宗.md "wikilink")[阿迪奧達特一世](../Page/阿迪奧達特一世.md "wikilink")
  - [4月10日](../Page/4月10日.md "wikilink")——[杨广](../Page/杨广.md "wikilink")，隋炀帝，隋朝第二位皇帝（[569年出生](../Page/569年.md "wikilink")）
  - [薛舉](../Page/薛舉.md "wikilink")，[隋朝末年割據勢力之一](../Page/隋朝.md "wikilink")，號西秦霸王，[薛仁杲之父](../Page/薛仁杲.md "wikilink")。
  - [高曇晟](../Page/高曇晟.md "wikilink")，為麾下勢力[高開道所殺](../Page/高開道.md "wikilink")。
  - [楊浩](../Page/楊浩.md "wikilink")，[隋煬帝之姪](../Page/隋煬帝.md "wikilink")，秦王[楊俊之子](../Page/楊俊.md "wikilink")，被[宇文化及擁立為帝](../Page/宇文化及.md "wikilink")。

[\*](../Category/618年.md "wikilink")
[8年](../Category/610年代.md "wikilink")
[1](../Category/7世纪各年.md "wikilink")