**明升**（），[中国](../Page/中国.md "wikilink")[元末](../Page/元朝.md "wikilink")[明初](../Page/明朝.md "wikilink")[明夏政权末代皇帝](../Page/明夏.md "wikilink")。

明升1366年继其父[明玉珍为大夏皇帝](../Page/明玉珍.md "wikilink")，改元[开熙](../Page/开熙.md "wikilink")，时明升年仅10岁，其母[彭皇后摄政](../Page/彭皇后_\(明玉珍\).md "wikilink")。[明太祖朱元璋北伐成功后](../Page/明太祖.md "wikilink")，遣使命明升归降，遭到明升拒绝，[洪武四年](../Page/洪武.md "wikilink")（1371年），明军由[汤和](../Page/汤和.md "wikilink")、[傅友德率领攻破](../Page/傅友德.md "wikilink")[重庆](../Page/重庆.md "wikilink")，明升出降，大夏国灭亡。

洪武五年（1372年），明升全家被明太祖[流放至](../Page/流放.md "wikilink")[高丽](../Page/高丽.md "wikilink")，被[权知高丽国事](../Page/权知高丽国事.md "wikilink")[李成桂封为](../Page/李成桂.md "wikilink")**华蜀君**，享受“忠勋世禄”，定居[开城](../Page/开城.md "wikilink")[兴国寺](../Page/兴国寺.md "wikilink")。彭氏去世后，安葬在[松都](../Page/松都.md "wikilink")[万寿山的肃陵](../Page/万寿山.md "wikilink")，并建有祠宇。

明玉珍家族一行来到高丽后，備受禮遇，[高丽恭愍王把延安](../Page/高丽恭愍王.md "wikilink")、白川两县作为贡物，供奉给明升一家，并将位于[松都](../Page/松都.md "wikilink")（现朝鲜开城）北部梨井里的兴国寺提供给他们作为邸宅，配以[奴婢](../Page/奴婢.md "wikilink")。明氏一家在高丽定居后，明升娶总郎尹熙王之女尹氏为妻，生四男。**大儿子义，资宪公、资宪大夫；二儿子见，总郎公、嘉靖大夫；三儿子俊，副使公、嘉靖大夫；四儿子信，侍郎公、通训大夫。**

后世为朝鲜望族。现今有4万多明氏后裔在[韩国](../Page/韩国.md "wikilink")，两万多在[朝鲜](../Page/朝鲜.md "wikilink")，他是[延安明氏的始祖](../Page/延安明氏.md "wikilink")。而在中国境内的后人多改为[甘姓](../Page/甘姓.md "wikilink")。\[1\]

## 妻子

  - [王皇后](../Page/王皇后_\(明升\).md "wikilink")
  - 郡夫人尹氏，[坡平尹氏总郎尹熙王之女](../Page/坡平尹氏.md "wikilink")

## 参考文献

### 引用

### 来源

  -
{{-}}

[Category:明夏](../Category/明夏.md "wikilink")
[Category:元末民变政权皇帝](../Category/元末民变政权皇帝.md "wikilink")
[Category:高丽政治人物](../Category/高丽政治人物.md "wikilink")
[Category:中国末代皇帝](../Category/中国末代皇帝.md "wikilink")
[Category:幼君](../Category/幼君.md "wikilink")
[Category:延安明氏](../Category/延安明氏.md "wikilink")
[Category:朝鲜半岛华人](../Category/朝鲜半岛华人.md "wikilink")
[S](../Category/明姓.md "wikilink")

1.  [4万多明氏后裔在韩国，两万多在朝鲜](http://www.cq.xinhuanet.com/news/2009-03/03/content_15840663.htm)