[Organizational_Learning_and_KM.jpg](https://zh.wikipedia.org/wiki/File:Organizational_Learning_and_KM.jpg "fig:Organizational_Learning_and_KM.jpg")
[Kmp-scr-01.jpg](https://zh.wikipedia.org/wiki/File:Kmp-scr-01.jpg "fig:Kmp-scr-01.jpg")

**知识管理**（，[缩写为](../Page/缩写.md "wikilink")**KM**）包括一系列企业内部定义、创建、传播、采用新的[知识和](../Page/知识.md "wikilink")[经验的战略和实践](../Page/经验.md "wikilink")。这些知识和经验包括[认知](../Page/认知.md "wikilink")，可以是个人知识，以及组织中[商业流程或实践](../Page/商业流程.md "wikilink")。

知識管理是一項在1990年代中期開始在全球崛起的學術與商業應用主題，針對個人及社群所擁有的[显性知识和](../Page/外顯知識.md "wikilink")[隱性知識的確認](../Page/隱性知識.md "wikilink")、創造、掌握、使用、分享及傳播進行積極及有效的管理。其主要涵蓋的固有理論及應用層面包括[學習型組織](../Page/學習型組織.md "wikilink")、[企業文化](../Page/企業文化.md "wikilink")、[資訊科技應用](../Page/資訊科技.md "wikilink")，及[人事管理等](../Page/人事管理.md "wikilink")。而由於知識管理的概念通常與企業的各種改善願景扯上關係，知識管理在現今企業上的實踐愈來愈受到重視，亦因此為顧問和科技公司帶來了不少商機。知識管理在非商業上的應用亦很廣泛，其中[維基百科經常被指為](../Page/維基百科.md "wikilink")[互聯網上其中一個最成功的](../Page/互聯網.md "wikilink")[知識管理系統](../Page/知識管理系統.md "wikilink")。

根据1991年的[学术规则列表](../Page/学术规则列表.md "wikilink")，（参考[Nonaka
1991](../Page/知识管理#CITEREFNonaka1991.md "wikilink")），知识管理应用于[商业管理](../Page/商业管理.md "wikilink")、[信息系统](../Page/信息系统.md "wikilink")、管理学和图书馆、[信息科学](../Page/信息科学.md "wikilink")等领域。最近，其他领域也开始应用知识管理，包括信息和媒体、[计算机科学](../Page/计算机科学.md "wikilink")、[公共卫生和](../Page/公共卫生.md "wikilink")[公共政策等](../Page/公共政策.md "wikilink")。

很多大型企业都有内部知识管理凭条，作为其[战略管理](../Page/战略管理.md "wikilink")、信息科技、[人力资源管理等部门组成部分](../Page/人力资源管理.md "wikilink")。很多咨询公司也有关于知识管理的建议。

知识管理一般重视以改进效果、[竞争优势](../Page/竞争优势.md "wikilink")、创新、分享知识学习、整合知识和[持续改进为企业](../Page/持续改进.md "wikilink")[目标](../Page/目标.md "wikilink")。知识管理与[组织学习类似](../Page/组织学习.md "wikilink")，但也有区别，更注重对知识作为战略资源的管理，鼓励分享知识，是企业学习的一种好方法。\[1\]

## 概述

**知识的层級**可分为[资料](../Page/资料.md "wikilink")、[資訊](../Page/資訊.md "wikilink")、[知识及](../Page/知识.md "wikilink")[智慧四個阶段](../Page/智慧.md "wikilink")，而知識的形成則是透过收集一些資料，再從資料中找出有用的資訊，利用這些資訊加上自己的想法及做法，最後產生出知識，而智慧則是以知識為基礎加上個人的應用能力并将其運用於生活上。舉例來說，我們製做一份報告時，會先收集大量的資料，再從這堆資料中找出可以運用在報告的資訊，運用自己的想法和做法去完成報告，當你完成這份報告後，從報告中所學習到的東西便會轉化成自己的知識。

把個人及群體得到的知識進行有效管理，則是知識管理最根本的目的。知識管理的第一步是把[隱性知識轉化為](../Page/隱性知識.md "wikilink")[顯性知識](../Page/顯性知識.md "wikilink")，並加以保存。而保存的形式，可以是文字、聲音或影像等。久而久之，所保存的顯性知識便匯集成一個[知識庫](../Page/知識庫.md "wikilink")。而知識庫裡的東西需要不斷的更新，以確保透過知識庫能持續且有效率的進行知識分享。與此同時，在人事培訓方面，需著重於員工[資訊素養的提升以令知識庫能得到更有效的運用](../Page/資訊素養.md "wikilink")。另外，有時候知識庫的環節會被跳過，取而代之的是持續的社交性知識分享行為。

要達到持續的分享，企業內部的學習氛圍與個人的學習動力尤為重要，而[企業文化及人事管理政策是左右企業內部的主要項目](../Page/企業文化.md "wikilink")。良好的學習氛圍與[知識庫的應用](../Page/知識庫.md "wikilink")，可提高組織的創新能力、反應能力、生產率以及技術技能。

## 知识管理的定義

### 维度

为区分不同知识的[框架](../Page/框架.md "wikilink")，其中一种是区分为[隐性知识和](../Page/隐性知识.md "wikilink")[显性知识](../Page/显性知识.md "wikilink")。隐性知识包括一个人可能没有意识到的内在知识，例如完成某项任务。在[象限的另一端](../Page/象限.md "wikilink")，显性知识代表一个人头脑中明确认知的知识，可方便沟通给他人。\[2\]
。同样地，Hayes和Walsham指出知识的内容和关系，把知识管理从知识论上分为两种。内容说是指知识很容易储存，因为可以进行编码。关系说是指知识具有背景性、关系性，让知识在特定组织外很难发展。\[3\]

[knowledge_spiral.svg](https://zh.wikipedia.org/wiki/File:knowledge_spiral.svg "fig:knowledge_spiral.svg")
早期研究显示，成功的知识管理需要把内在知识转变为外在知识，让个人可以吸收，并且解读各种编码的知识。后来的研究显示，内在知识和外在知识具有差异，这样定义过于简单，外在知识有时自有矛盾。为了让知识成为外在知识，需要翻译为信息。(如头脑外的[符号](../Page/符号.md "wikilink"))。后来，日本的野中郁次郎提出一个SECI模型（社交、外在、结合、吸收），以知识螺旋表達[外显知识和内隐知识之間的互动](../Page/外显知识.md "wikilink")。在这个模型中，内隐知识可以'提取'为外显知识，外显知识可以'重新吸收'为隐形知识。后来，他和Georg
von Krogh共同推进了早期研究，试图推动知识转换的争论。

知识管理的奖励措施包括积极管理知识（推动策略）。鼓励个人积极把知识编码为共享知识库，如[数据库](../Page/数据库.md "wikilink")，或从中提取想要的知识\[4\]。

另一种方式是个人随时就需要的知识向专家咨询（拉动策略），专家可提供[知识给个人](../Page/知识.md "wikilink")。

企业常用的知识管理包括：

  - 奖励（鼓励知识共享）
  - [讲故事](../Page/讲故事.md "wikilink")（共享外在知识的方式）
  - 跨项目学习
  - [行动后检查](../Page/行动后检查.md "wikilink")
  - 知识图（全体职员可获得的知识概况图）
  - [案例库](../Page/案例库.md "wikilink")
  - 专家库（让知识寻求者可找到专家）
  - 最佳经验分享
  - [知识集会](../Page/知识集会.md "wikilink")
  - 竞争力管理（系统性评估、计划组织内个人竞争的方法）
  - 基础设施建设（可方便或阻碍个人获取知识）
  - 师徒制度
  - 合作科技（[群组软件](../Page/群组软件.md "wikilink")）
  - 知识库（数据库、[企业书签等](../Page/企业书签.md "wikilink")）
  - 测量、报告智力资本（对企业知识量进行量化）
  - [知识中介](../Page/知识中介.md "wikilink")（某些组织个人负责某一特别"领域"，收集相关主题）
  - [社会性软件](../Page/社会性软件.md "wikilink")（维基百科、社会书签、博客等）
  - 项目内知识分享

### 鼓励

一系列[鼓励知识管理的方法包括](../Page/鼓励.md "wikilink")：\[5\]

  - 在[新产品开发中逐渐加入知识](../Page/新产品开发.md "wikilink")
  - 更短的[新产品开发循环](../Page/新产品开发.md "wikilink")
  - 管理创新和组织学习
  - 对组织内[专家力量进行平衡](../Page/专家.md "wikilink")
  - 增加组织内、外部人员[社交网络](../Page/社交网络.md "wikilink")[连接性](../Page/连接性.md "wikilink")
  - 管理商业环境，允许员工获得相关知识和[创意](../Page/创意.md "wikilink")
  - 解决棘手问题、[古怪问题](../Page/古怪问题.md "wikilink")
  - 管理智力资本、智力资产（专家、[know-how等](../Page/know-how.md "wikilink")）

很多知识管理不仅仅用到一种软件，这些知识管理软件工具，利用组织目前的科技设施。组织和商业决策运用了大量的资源，对最新科技大量投入，进行知识管理。但需要明确哪些投资有用，选用最合适的软件，结合知识管理措施。

## 知识管理的障碍

  - 技术
  - 所有权/知识产权（取决于公众认为知识是公有的还是私有的）
  - 动机（如何让人们愿意分享知识）

## 知识管理的目的

知识管理最先在企业内部产生，为了提高[企业效益](../Page/企业.md "wikilink")、增强竞争优势、创新、分享教训、持续发展。

## 工具

  - [Web2.0工具](../Page/Web2.0.md "wikilink")，包括[blog](../Page/blog.md "wikilink")、[wiki](../Page/wiki.md "wikilink")、[社會性網絡系統](../Page/社會性網絡.md "wikilink")[1](http://onlinelibrary.wiley.com/doi/10.1111/j.1468-0270.2010.02053.x/abstract;jsessionid=456CCEF85075A83C0BD7208AB83CFAB3.f04t04?deniedAccessCustomisedMessage=&userIsAuthenticated=false)，及[分眾分類法](../Page/分眾分類法.md "wikilink")，还有[全球专家定位](../Page/全球专家定位.md "wikilink")、[Idea
    Generation](../Page/Idea_Generation.md "wikilink")
  - [知识管理系统](../Page/知识管理系统.md "wikilink")[瑞士Knowledge](../Page/瑞士.md "wikilink")
    Tool股份公司的Uni Viadrina
  - [IBM公司的Life](../Page/IBM.md "wikilink") Sciences DiscoveryLink
  - [西门子公司的](../Page/西门子.md "wikilink")[ShareNet](../Page/ShareNet.md "wikilink")
  - [叡揚資訊](http://www.gss.com.tw/)以企業社群、知識管理平台為核心理念開發出 [Vitals ESP
    知識管理系統](http://www.vitalsesp.com/)
  - [蓝凌](../Page/蓝凌.md "wikilink")（中国知识管理领导品牌）的[KMS专业知识管理平台](http://www.landray.com/product/kms.html)
  - [中软公司的](../Page/中软.md "wikilink")[KMS](../Page/KMS.md "wikilink")
  - [泛微](../Page/泛微.md "wikilink")（一家在中国领先的管理咨询和软件公司）的[协同知识管理系统](http://www.weaver.com.cn)
  - [阳光融通专业从事知识管理实施咨询和IT解决方案的高新技术企业](../Page/阳光融通.md "wikilink")、北京诺门科技的产品开发管理系统（PDMS）
  - [诺门科技](http://www.gnomontech.com)。[知识管理系统](http://www.sunroto.com)
  - [MindManager](../Page/MindManager.md "wikilink")
  - 企业知识管理形式：在职讨论；正规[学徒制](../Page/学徒.md "wikilink")；讨论峰会；企业。

## 参考文献

### 引用

### 来源

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  - Akscyn, Robert M., Donald L. McCracken and Elise A. Yoder (1988).
    "KMS: A distributed hypermedia system for managing knowledge in
    organizations". Communications of the ACM 31 (7): 820–835.

  - Benbya, H (2008). Knowledge Management Systems Implementation:
    Lessons from the Silicon Valley. Oxford, Chandos Publishing.

  - Langton, N & Robbins, S. (2006). Organizational Behaviour (Fourth
    Canadian Edition). Toronto, Ontario: Pearson Prentice Hall.

  - Li, R.Y.M. & Poon, S.W. USING WEB 2.0 TO SHARE KNOWLEDGE OF
    CONSTRUCTION SAFETY: THE FABLE OF ECONOMIC ANIMALS Economic Affairs
    Volume 31, Issue 1, pages 73–79, March 2011

  - Maier, R (2007): Knowledge Management Systems: Information And
    Communication Technologies for Knowledge Management. 3rd edition,
    Berlin: Springer.

  - Rhetorical Structure Theory (assumed from the reference of RST
    Theory above)
    <https://web.archive.org/web/20120325181330/http://acl.ldc.upenn.edu/W/W01/W01-1605.pdf>

  - Rosner, D.., Grote, B., Hartman, K, Hofling, B, Guericke, O. (1998)
    From natural language documents to sharable product knowledge: a
    knowledge engineering approach. in Borghoff Uwe M., and Pareschi,
    Remo (Eds.). Information technology for knowledge management.
    Springer Verlag, pp 35–51.

  - The RST site at <http://www.sfu.ca/rst/> run by Bill Mann

  - Jennex, M. E. (2008). Knowledge Management: Concepts, Methodologies,
    Tools, and Applications (pp. 1–3808).

## 外部链接

  -
  - [Knowledge@work
    community](http://www.ami-communities.eu/wiki/Knowledge%40Work)

  - [叡揚資訊](http://www.gss.com.tw/)產品 [Vitals ESP
    知識管理系統](http://www.vitalsesp.com/)相關資訊

## 参见

  - [知识社会](../Page/知识社会.md "wikilink")
  - [知识生态系统](../Page/知识生态系统.md "wikilink")
  - [知识工程](../Page/知识工程.md "wikilink")
  - [知识管理软件](../Page/知识管理软件.md "wikilink")
  - [知识共享](../Page/知识共享.md "wikilink")
  - [无知管理](../Page/无知管理.md "wikilink")
  - [法律案例管理](../Page/法律案例管理.md "wikilink")
  - [知识图](../Page/知识图.md "wikilink")
  - [知识模型](../Page/知识模型.md "wikilink")

<!-- end list -->

  - 国际期刊

<!-- end list -->

  - *[Electronic Journal of Knowledge
    Management](../Page/Electronic_Journal_of_Knowledge_Management.md "wikilink")*
  - *[Journal of Knowledge
    Management](../Page/Journal_of_Knowledge_Management.md "wikilink")*
  - *[Journal of Knowledge Management
    Practice](../Page/Journal_of_Knowledge_Management_Practice.md "wikilink")*

{{-}}

[Category:认知科学](../Category/认知科学.md "wikilink")
[Category:管理学分支](../Category/管理学分支.md "wikilink")
[Category:資訊管理](../Category/資訊管理.md "wikilink")
[Category:信息系统](../Category/信息系统.md "wikilink")
[Category:组群](../Category/组群.md "wikilink")
[Category:商业术语](../Category/商业术语.md "wikilink")
[Category:超文字](../Category/超文字.md "wikilink")

1.  Sanchez, R (1996) Strategic Learning and Knowledge Management,
    Wiley, Chichester
2.
3.
4.
5.