**日向雛田**（，Hyuga Hinata）
是[日本漫畫](../Page/日本.md "wikilink")《[火影忍者](../Page/火影忍者.md "wikilink")》系列中的一個人物，日向宗家的白眼公主，於《THE
LAST》片尾，《疾風傳》720集中與[漩渦鳴人結婚](../Page/漩渦鳴人.md "wikilink")，改名為漩渦雛田（うずまきヒナタ）。後來其夫漩渦鳴人繼任為第七代火影。

## 人物

### 個人檔案

[Boruto_uzumaki_family_mandarin.svg](https://zh.wikipedia.org/wiki/File:Boruto_uzumaki_family_mandarin.svg "fig:Boruto_uzumaki_family_mandarin.svg")

  - 性格：害羞、鑽牛角尖、內向
  - 喜歡的食物：紅豆湯圓，肉桂卷
  - 丈夫: [漩渦鳴人](../Page/漩渦鳴人.md "wikilink")
  - 兒女:
    [漩渦慕留人](../Page/漩渦慕留人.md "wikilink")(うずまきボルト)\[1\]、[漩渦向日葵](../Page/木叶村忍者列表#新生代忍者.md "wikilink")(うずまきヒマワリ)

<!-- end list -->

  - 希望交手的對象：[日向寧次](../Page/日向寧次.md "wikilink")、[日向日足](../Page/日向日足.md "wikilink")
  - 討厭的食物：蝦子，螃蟹
  - 興趣：壓花
  - 名言：「我向來都是說到做到，這也是我的忍道。」
  - 喜歡的話：自信
  - 忍者學校畢業年齡: 12
  - 中忍升級年齡：14
  - 查克拉性質：雷、火\[2\]
  - 嫁給鳴人後改姓漩渦：漩渦雛田（うずまきヒナタ）\[3\]
  - 一樂拉麵大胃王冠軍（慕留人和父親鳴人去吃拉麵才得知這件事，讓慕留人感到震驚。）

### 身世

隸屬[夕日紅所帶領的第八班](../Page/夕日紅.md "wikilink")，與[犬塚牙](../Page/犬塚牙.md "wikilink")、[油女志乃同為組員](../Page/油女志乃.md "wikilink")。藍黑短髮（第二部變為長髮），由於是日向一族的血脈，與[日向寧次一樣有著白色的雙瞳](../Page/日向寧次.md "wikilink")。性格內向害羞、勤奮刻苦，暗戀[漩渦鳴人並將其視為偶像](../Page/漩渦鳴人.md "wikilink")。日向家族宗家的長女，精通白眼、柔拳。《[博人传-火影次世代-](../Page/博人传-火影次世代-.md "wikilink")》雛田已成為人母，專念養育兒女，但對於慕留人的調皮搗蛋，而感到憂慮。[漩渦慕留人曾說出媽媽生氣起來是很恐怖的](../Page/漩渦慕留人.md "wikilink")。

### 家庭背景

雛田是[日向一族秘術的正宗繼承人](../Page/日向一族.md "wikilink")。但她的父親[日向日足對她失去信心](../Page/日向日足.md "wikilink")，因為她曾被小她五歲的妹妹[日向花火打敗](../Page/日向一族#宗家.md "wikilink")。因此，當[夕日紅成為雛田的師傅後](../Page/夕日紅.md "wikilink")，日足對夕日紅說：“愛怎麼教隨你的便，日向家不需要這種廢物。”與分家[日向寧次是堂兄妹關係](../Page/日向寧次.md "wikilink")，中忍考試時寧次非常仇視雛田並將雛田打成重傷，後由於日向日足誠懇致歉並坦白了寧次父親犧牲的真相，寧次消除了對宗家的仇恨並與雛田和好。
日向家是大筒木羽村後裔，雛田為白眼之姬。

### 性格

初時、雛田表現為一個缺乏自信且容易放棄的人。儘管她始終進行刻苦訓練，卻總在任務中失敗。但在中忍考試中她從鳴人那裡獲得了永不放棄的信念。中忍考試後的修行時，雛田曾主動請求志乃與牙協助其修行，並要求不要留情。在微香蟲任務及其他充滿艱難險阻的任務中，雛田展現出透過不斷修煉逐漸形成堅強、自信、永不服輸的意志，一改過去懦弱膽小的形象。火影的作者[岸本齊史曾在書中寫道](../Page/岸本齊史.md "wikilink")：“人們總認為出身的不同已決定了他們能力間的差異，但我相信人們可以通過自身努力來改變自己，改變這一切。雛田身上就帶著我的這些信念。”

看似柔弱的外表下卻有著相當驚人的食量，甚至遠在以食量大而出名的秋道一族之上，目前仍在一樂拉麵以連吃46碗的最高紀錄，被一樂拉麵的常客們稱作**大胃女王**並持續穩居著冠軍寶座。

婚後的雛田保持著溫柔的性格，但真的生氣起來的話連身為火影及丈夫的鳴人都得讓其三分，另外也是少數發怒時能讓長子慕留人害怕的對象。

## 參考資料

1.  《臨之書》，[岸本齊史](../Page/岸本齊史.md "wikilink")，[台灣東立出版社](../Page/台灣東立出版社.md "wikilink")，ISBN
    986-11-1631-1，2003年12月9日
2.  《兵之書》，[岸本齊史](../Page/岸本齊史.md "wikilink")，[台灣東立出版社](../Page/台灣東立出版社.md "wikilink")，ISBN
    986-11-4962-7，2005年8月17日
3.  《鬥之書》，[岸本齊史](../Page/岸本齊史.md "wikilink")，[台灣東立出版社](../Page/台灣東立出版社.md "wikilink")，ISBN
    986-11-8309-4，2006年6月29日
4.  《者之書》，[岸本齊史](../Page/岸本齊史.md "wikilink")，[集英社](../Page/集英社.md "wikilink")，ISBN
    978-4-08-874247-2，2008年9月9日
5.  《陣之書》，岸本齊史，台灣東立出版社，ISBN 978-986-382-631-6，2015年2月10日
6.  《火影忍者》漫畫和動畫

## 相關條目

  - [日向一族](../Page/日向一族.md "wikilink")

[Category:火影忍者人物](../Category/火影忍者人物.md "wikilink")
[Category:虛構女性動漫角色](../Category/虛構女性動漫角色.md "wikilink")
[Category:虛構女忍者](../Category/虛構女忍者.md "wikilink")

1.
2.  陣之書 P.154
3.