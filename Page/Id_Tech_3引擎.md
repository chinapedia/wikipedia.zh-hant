**id Tech 3引擎**是由[id
Software开发的用于多种游戏的游戏引擎](../Page/id_Software.md "wikilink")。
它和[虚幻引擎](../Page/虚幻引擎.md "wikilink")，[Source引擎是在世界上使用最广泛的游戏引擎](../Page/Source引擎.md "wikilink")。

id Tech 3引擎从[雷神之锤引擎](../Page/雷神之锤引擎.md "wikilink") 和[id Tech
2引擎发展而来](../Page/id_Tech_2引擎.md "wikilink")，和[毁灭战士引擎有着很大的区别](../Page/毁灭战士引擎.md "wikilink")。虽然很多人认为[毁灭战士3引擎是基于id](../Page/毁灭战士3引擎.md "wikilink")
Tech 3游戏引擎的，但是这种观点是错误的。
毁灭战士3引擎是由[約翰·卡馬克领导的开发小组完全重新开发的引擎](../Page/約翰·卡馬克.md "wikilink")。

2005年8月19日，[id
Software在遵循](../Page/id_Software.md "wikilink")[GPL许可证准则的情况下开放了id](../Page/GPL.md "wikilink")
Tech
3引擎的全部核心代码。形成的主要开发小组进行了bug修复和功能添加，这就是[ioquake3](../Page/ioquake3.md "wikilink")。

## 使用id Tech 3引擎的游戏

  - [雷神之鎚III](../Page/雷神之鎚III.md "wikilink")
  - [American McGee's
    Alice](../Page/American_McGee's_Alice.md "wikilink")
  - [決勝時刻](../Page/決勝時刻.md "wikilink")
  - [傭兵戰場2](../Page/傭兵戰場2.md "wikilink")
  - [荣誉勋章](../Page/荣誉勋章.md "wikilink")
  - [絕地武士 2 : 絕地浪子](../Page/絕地武士_2_:_絕地浪子.md "wikilink")
  - [星際大戰絕地武士 : 絕地學院](../Page/星際大戰絕地武士_:_絕地學院.md "wikilink")
  - [Space Trader](../Page/Space_Trader.md "wikilink")

## 使用id Tech 3改动引擎的游戏

  - [重返德軍總部](../Page/重返德軍總部.md "wikilink")
  - [World Of Padman](../Page/World_Of_Padman.md "wikilink")
  - [Tremulous](../Page/Tremulous.md "wikilink")
  - [Urban Terror](../Page/Urban_Terror.md "wikilink")

## 使用id Tech 3引擎游戏的特征

[Wolfconsole.jpg](https://zh.wikipedia.org/wiki/File:Wolfconsole.jpg "fig:Wolfconsole.jpg")一般来说，使用了id
Tech 3引擎的游戏在进入前都会出现一个控制台。你也可以在游戏里面用"\~"按键唤出控制台，其字体为雷神之锤III字体（雷神之锤引擎和id
Tech 2引擎游戏的字体则多趋向经典字体）。毁灭战士3引擎游戏内，字体则是毁灭战士菜单字体。有些使用id Tech
3引擎的游戏会有两个可执行文件，一个用于[单人游戏](../Page/单人游戏.md "wikilink")，另外一个则是[多人游戏](../Page/多人游戏.md "wikilink")。

## 持续发展

[ioquake3使此引擎产生了新的活力](../Page/ioquake3.md "wikilink")，添加了网络功能方面新技术支持；[XreaL则在](../Page/XreaL.md "wikilink")[ioquake3基础上进一步开发图像引擎](../Page/ioquake3.md "wikilink")，达到了接近id
下一代[id Tech 4的水准](../Page/id_Tech_4.md "wikilink")。

## 参考文献

## 外部連結

[Category:自由软件](../Category/自由软件.md "wikilink")
[Category:游戏引擎](../Category/游戏引擎.md "wikilink")
[Category:雷神之锤系列](../Category/雷神之锤系列.md "wikilink")