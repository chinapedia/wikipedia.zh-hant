**兔猻**（[学名](../Page/学名.md "wikilink")：）\[1\]是一種生存在[中亞的](../Page/中亞.md "wikilink")[貓科动物](../Page/貓科.md "wikilink")，为[猫科兔猻属的唯一物种](../Page/猫科.md "wikilink")。

## 特征

身長有60[公分](../Page/公分.md "wikilink")（24英寸）長，另外的尾長則有25公分（10英寸）長。毛髮是土黃色且帶有垂直的條紋，但有時會因為厚重的毛髮而變得不可見。

兔猻有幾個特徵和其他的貓科動物有所區分。牠的腳是短的，臀部較肥重，且毛髮也很長、很厚。這使得牠看起來特別地矮胖且多毛。牠的毛髮會隨著季節而改變－冬天時會較灰且較不花。牠的耳朵位置較低，且使其有一副有點像是貓頭鷹的面容。他牠的瞳孔和人類的一樣，是圓形的，而貓是直線型的。由于其外形，一度被列入[猫属](../Page/猫属.md "wikilink")，并被命名为。甚至乎，在更早的时候，因為牠那相對較肥的臉龐，一度被認為是[波斯貓品種的祖先](../Page/波斯貓.md "wikilink")。

每年早春發情，夏初產崽，北方的兔猻繁殖期一般比南方地區的稍晚，繁殖期持續42天，雌性發情期26-42個小時。妊娠期9-10周，一般每胎3-4仔，最多的一次可產6崽，小兔猻一般在4-5個月後，周身長滿毛茸茸的灰色外套，並開始獨立。

## 分布

兔猻居住在海拔4000[公尺](../Page/公尺.md "wikilink")（13000英尺）的亞洲草原上。牠在夜晚活動，捕捉的獵物有[齧齒目](../Page/齧齒目.md "wikilink")、[鼠兔和](../Page/鼠兔.md "wikilink")[鳥類](../Page/鳥類.md "wikilink")。

分布于自亚洲中部地带向东至西伯利亚以及[中国大陆的](../Page/中国大陆.md "wikilink")[黑龙江](../Page/黑龙江.md "wikilink")、[青海](../Page/青海.md "wikilink")、[内蒙古](../Page/内蒙古.md "wikilink")、[河北](../Page/河北.md "wikilink")、[四川](../Page/四川.md "wikilink")、[西藏](../Page/西藏.md "wikilink")、[新疆等地](../Page/新疆.md "wikilink")，多见于沙漠、荒漠或戈壁地区。该物种的模式产地在[贝加尔湖南](../Page/贝加尔湖.md "wikilink")[吉达河](../Page/吉达河.md "wikilink")（Dzhida）。\[2\]

## 保护状况

很長的一段時間裡，兔猻都因為牠的毛而被人類狩獵，但現在有很多地區已開始有所保護，如[中國](../Page/中國.md "wikilink")。因為牠會捕抓有害農作物的動物，所以被認為是有益的。但是，毒害齧齒目和鼠兔等有害農作物的動物所使用的藥劑也會影響到兔猻。

  - [中国国家重点保护动物等级](../Page/中國國家重點保護野生動物名錄.md "wikilink")：二级
  - [中国濒危动物红皮书等级](../Page/中国濒危动物红皮书.md "wikilink")：渐危

## 亚种

[Manul1.jpg](https://zh.wikipedia.org/wiki/File:Manul1.jpg "fig:Manul1.jpg")

  - [贝加尔湖兔狲](../Page/贝加尔湖兔狲.md "wikilink")（[学名](../Page/学名.md "wikilink")：），Pallas于1776年命名。居於[蒙古](../Page/蒙古.md "wikilink")、[中國西部](../Page/中國.md "wikilink")。在[中国大陆](../Page/中国大陆.md "wikilink")，分布于四川（北部）、[黑龙江](../Page/黑龙江.md "wikilink")、[河北](../Page/河北.md "wikilink")、[内蒙古等地](../Page/内蒙古.md "wikilink")。该物种的模式产地在贝加尔湖南。\[3\]
  - [赤色兔狲](../Page/赤色兔狲.md "wikilink")（[学名](../Page/学名.md "wikilink")：），Ognev,
    1928年命名。分布于[伊朗](../Page/伊朗.md "wikilink")、[亚美尼亚](../Page/亚美尼亚.md "wikilink")、[哈萨克斯坦](../Page/哈萨克斯坦.md "wikilink")、[吉尔吉斯斯坦](../Page/吉尔吉斯斯坦.md "wikilink")、[土库曼](../Page/土库曼.md "wikilink")、[乌兹别克斯坦](../Page/乌兹别克斯坦.md "wikilink")、[塔吉克斯坦](../Page/塔吉克斯坦.md "wikilink")、[阿富汗](../Page/阿富汗.md "wikilink")、[巴基斯坦](../Page/巴基斯坦.md "wikilink")。\[4\]
  - [高原兔狲](../Page/高原兔狲.md "wikilink")（[学名](../Page/学名.md "wikilink")：），Hodgson于1842年命名。居於[克什米爾](../Page/克什米爾.md "wikilink")、[尼泊爾](../Page/尼泊爾.md "wikilink")、中國西南部。在[中国大陆](../Page/中国大陆.md "wikilink")，分布于[西藏](../Page/西藏.md "wikilink")、[青海](../Page/青海.md "wikilink")、[新疆](../Page/新疆.md "wikilink")、[四川等地](../Page/四川.md "wikilink")。该物种的模式产地在西藏。\[5\]

## 參考文獻

## 外部連結

  - [兔猻的圖片](http://pets.webshots.com/album/557682793gVyLav/)

[Otocolobus manul](../Category/貓亞科.md "wikilink")
[Category:中國國家二級保護動物](../Category/中國國家二級保護動物.md "wikilink")

1.
2.
3.

4.

5.