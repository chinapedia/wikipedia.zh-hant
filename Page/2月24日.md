**2月24日**是[阳历年中的第](../Page/阳历.md "wikilink")55天，距一年的结束还有310天（[闰年则还有](../Page/闰年.md "wikilink")311天）。

## 大事记

### 4世紀

  - [303年](../Page/303年.md "wikilink")：[罗马帝国皇帝](../Page/罗马帝国.md "wikilink")[戴克里先发布首个迫害](../Page/戴克里先.md "wikilink")[基督徒的法令](../Page/基督徒.md "wikilink")。

### 10世紀

  - [947年](../Page/947年.md "wikilink")：[辽太宗](../Page/辽太宗.md "wikilink")[耶律德光在](../Page/耶律德光.md "wikilink")[开封改国号](../Page/开封.md "wikilink")[契丹为](../Page/契丹.md "wikilink")[大辽](../Page/大辽.md "wikilink")。

### 16世紀

  - [1525年](../Page/1525年.md "wikilink")：[帕維亞之戰](../Page/帕維亞之戰.md "wikilink")（[Battle
    of
    Pavia](../Page/帕维亚之战.md "wikilink")）结束，[查理五世指挥的](../Page/查理五世_\(神圣罗马帝国\).md "wikilink")[西班牙](../Page/西班牙.md "wikilink")－[神圣罗马帝国军队打败法军](../Page/神圣罗马帝国.md "wikilink")，俘虏[法国国王](../Page/法国.md "wikilink")[-{zh-tw:法蘭索瓦一世;
    zh-cn:弗朗索瓦一世;}-](../Page/法蘭索瓦一世.md "wikilink")。
  - [1582年](../Page/1582年.md "wikilink")：[教皇](../Page/教皇.md "wikilink")[格里高利十三世发布教皇训令实行](../Page/格里高利十三世.md "wikilink")[格里历](../Page/格里历.md "wikilink")，以取代[儒略历](../Page/儒略历.md "wikilink")。

### 19世紀

  - [1803年](../Page/1803年.md "wikilink")：[美国首席大法官](../Page/美国首席大法官.md "wikilink")[约翰·马歇尔对](../Page/约翰·马歇尔.md "wikilink")[马伯利诉麦迪逊案做出判决](../Page/马伯利诉麦迪逊案.md "wikilink")，确立了联邦法院的[司法审查权](../Page/司法审查.md "wikilink")。
  - [1824年](../Page/1824年.md "wikilink"):
    [第一次英缅战争爆发](../Page/第一次英缅战争.md "wikilink")。
  - [1848年](../Page/1848年.md "wikilink")：[法国](../Page/法国.md "wikilink")[二月革命迫使](../Page/法國二月革命.md "wikilink")[国王](../Page/法国国王.md "wikilink")[路易·菲利普退位并逃往](../Page/路易·菲利普.md "wikilink")[英国](../Page/英国.md "wikilink")。
  - [1881年](../Page/1881年.md "wikilink")：沙俄强迫中国清政府签订了《[中俄伊犁条约](../Page/中俄伊犁条约.md "wikilink")》。

### 20世紀

  - [1918年](../Page/1918年.md "wikilink")：[爱沙尼亚宣布从](../Page/爱沙尼亚.md "wikilink")[俄罗斯独立](../Page/俄罗斯.md "wikilink")。
  - [1933年](../Page/1933年.md "wikilink")：[國際聯盟於大會上以](../Page/國際聯盟.md "wikilink")40票對41票通過了國際聯盟調查團報告書的聲明。報告書指出[滿洲主權屬於](../Page/滿洲國.md "wikilink")[中華民國](../Page/中華民國.md "wikilink")，日本違反國際聯盟盟約戰取中國領土並使之獨立；[滿洲國是日本參謀本部指導組織的](../Page/滿洲國.md "wikilink")，其存在是因為日本軍隊的存在，滿洲國不是出自[民族自決的運動](../Page/民族自決.md "wikilink")；聲明認為日本應退出滿洲，滿洲由國際共管。
  - [1942年](../Page/1942年.md "wikilink")：[美国之音开播](../Page/美国之音.md "wikilink")。
  - [1946年](../Page/1946年.md "wikilink")：[胡安·裴隆当选为](../Page/胡安·裴隆.md "wikilink")[阿根廷总统](../Page/阿根廷总统.md "wikilink")。
  - [1949年](../Page/1949年.md "wikilink")：[第一次中东战争](../Page/第一次中东战争.md "wikilink")：[埃及与](../Page/埃及.md "wikilink")[以色列签订停战协定](../Page/以色列.md "wikilink")。
  - [1950年](../Page/1950年.md "wikilink")：中華民國[立法院因](../Page/立法院.md "wikilink")[國共內戰關係隨中央政府輾轉遷台](../Page/國共內戰.md "wikilink")，於[中山堂
    (臺北市)舉行第一屆第](../Page/中山堂_\(臺北市\).md "wikilink")5會期第1次會議，與會委員約三百八十餘名。其後並通過修改立法院組織法，將21個常設委員會縮編為12個常設委員會及其他特種委員會。
  - [1953年](../Page/1953年.md "wikilink")：[中華民國](../Page/中華民國.md "wikilink")[立法院廢止](../Page/立法院.md "wikilink")《[中蘇友好同盟條約](../Page/中蘇友好同盟條約.md "wikilink")》並拒絕承認[外蒙古獨立](../Page/外蒙古獨立.md "wikilink")。
  - [1967年](../Page/1967年.md "wikilink")：前[納粹德國戰犯在](../Page/納粹德國.md "wikilink")[慕尼黑判刑](../Page/慕尼黑.md "wikilink")。
  - [1976年](../Page/1976年.md "wikilink")：[马来西亚](../Page/马来西亚.md "wikilink")、[菲律宾](../Page/菲律宾.md "wikilink")、[泰国](../Page/泰国.md "wikilink")、[新加坡和](../Page/新加坡.md "wikilink")[印度尼西亚等国在印度尼西亚的](../Page/印度尼西亚.md "wikilink")[峇里签订了](../Page/峇里.md "wikilink")《[东南亚友好合作条约](../Page/东南亚友好合作条约.md "wikilink")》。
  - [1978年](../Page/1978年.md "wikilink")：台灣[西部幹線鐵路電氣化第一期工程](../Page/西部幹線.md "wikilink")(基隆至竹南)完工正式通車。
  - [1989年](../Page/1989年.md "wikilink")：[聯合航空811號班機事故](../Page/聯合航空811號班機事故.md "wikilink")。
  - [1991年](../Page/1991年.md "wikilink")：[波斯灣戰爭展開地面戰](../Page/波斯灣戰爭.md "wikilink")，連串攻勢令[伊拉克部隊潰不成軍](../Page/伊拉克.md "wikilink")。

### 21世紀

  - [2003年](../Page/2003年.md "wikilink")：[中國](../Page/中华人民共和国.md "wikilink")[新疆自治區](../Page/新疆自治區.md "wikilink")[伽師](../Page/伽師.md "wikilink")、[巴楚兩](../Page/巴楚.md "wikilink")[縣發生](../Page/縣.md "wikilink")[黎克特制](../Page/黎克特制.md "wikilink")6.8強烈地震，260多人死亡，大量房屋倒塌。
  - [2006年](../Page/2006年.md "wikilink")：菲律宾总统[格洛丽亚·马卡帕加尔-阿罗约宣布国家进入紧急状态](../Page/格洛丽亚·马卡帕加尔-阿罗约.md "wikilink")，试图阻止可能会发生的[军事政变](../Page/2006年菲律宾政变.md "wikilink")。

## 出生

  - [1103年](../Page/1103年.md "wikilink")：[鸟羽天皇](../Page/鸟羽天皇.md "wikilink")，[日本第](../Page/日本.md "wikilink")74代[天皇](../Page/天皇.md "wikilink")（逝於[1156年](../Page/1156年.md "wikilink")）
  - [1122年](../Page/1122年.md "wikilink")：[金海陵王完颜亮](../Page/金海陵王.md "wikilink")，[金朝第四位皇帝](../Page/金朝.md "wikilink")（逝於[1161年](../Page/1161年.md "wikilink")）
  - [1304年](../Page/1304年.md "wikilink")：[伊本·白图泰](../Page/伊本·白图泰.md "wikilink")，[摩洛哥](../Page/摩洛哥.md "wikilink")[大探險家](../Page/探險家.md "wikilink")（逝於[1377年](../Page/1377年.md "wikilink")）
  - [1463年](../Page/1463年.md "wikilink")：[若望·皮科·德拉·米兰多拉](../Page/若望·皮科·德拉·米兰多拉.md "wikilink")，[文艺复兴时期](../Page/文艺复兴.md "wikilink")[意大利哲学家](../Page/意大利.md "wikilink")（逝於[1494年](../Page/1494年.md "wikilink")）
  - [1500年](../Page/1500年.md "wikilink")：[查理五世](../Page/查理五世_\(神圣罗马帝国\).md "wikilink")，[神圣罗马帝国](../Page/神圣罗马帝国.md "wikilink")[皇帝](../Page/皇帝.md "wikilink")（逝於[1558年](../Page/1558年.md "wikilink")）
  - [1536年](../Page/1536年.md "wikilink")：[克勉八世](../Page/克勉八世.md "wikilink")，第231代罗马天主教[教皇](../Page/教皇.md "wikilink")（逝於[1605年](../Page/1605年.md "wikilink")）
  - [1547年](../Page/1547年.md "wikilink")：[奧地利的唐胡安](../Page/奧地利的唐胡安.md "wikilink")，尼德兰总督（逝於[1578年](../Page/1578年.md "wikilink")）
  - [1557年](../Page/1557年.md "wikilink")：[马蒂亚斯](../Page/马蒂亚斯_\(神圣罗马帝国\).md "wikilink")，神圣罗马帝国皇帝（逝於[1619年](../Page/1619年.md "wikilink")）
  - [1664年](../Page/1664年.md "wikilink")：[汤玛斯·纽科门](../Page/汤玛斯·纽科门.md "wikilink")，英国工程师、发明家（逝於[1729年](../Page/1729年.md "wikilink")）
  - [1767年](../Page/1767年.md "wikilink")：[拉玛二世](../Page/拉玛二世.md "wikilink")，[泰国](../Page/泰国.md "wikilink")[曼谷王朝皇帝](../Page/曼谷王朝.md "wikilink")（逝於[1824年](../Page/1824年.md "wikilink")）
  - [1772年](../Page/1772年.md "wikilink")：[威廉·H·克劳福德](../Page/威廉·H·克劳福德.md "wikilink")，美国战争部长、财政部长（逝於[1834年](../Page/1834年.md "wikilink")）
  - [1774年](../Page/1774年.md "wikilink")：[阿道弗斯王子](../Page/阿道弗斯王子_\(剑桥公爵\).md "wikilink")，剑桥公爵（逝於[1850年](../Page/1850年.md "wikilink")）
  - [1786年](../Page/1786年.md "wikilink")：[威廉·格林](../Page/威廉·格林.md "wikilink")，[德国](../Page/德国.md "wikilink")[童話故事作家](../Page/童話故事.md "wikilink")（逝於[1859年](../Page/1859年.md "wikilink")）
  - [1804年](../Page/1804年.md "wikilink")：[-{zh-hans:海因里希·楞次;zh-hk:海因里希·楞次;zh-tw:海因里希·冷次;}-](../Page/海因里希·楞次.md "wikilink")（Heinrich
    Friedrich Emil
    Lenz），[俄国](../Page/俄国.md "wikilink")[物理學家](../Page/物理學家.md "wikilink")，發現《[-{zh-tw:冷次;zh-cn:楞次;zh-hk:楞次}-定律](../Page/冷次定律.md "wikilink")（Lenz's
    law）》（逝於[1865年](../Page/1865年.md "wikilink")）
  - [1828年](../Page/1828年.md "wikilink")：[巴夏礼](../Page/巴夏礼.md "wikilink")，英国驻[清朝大使](../Page/清朝.md "wikilink")（逝於[1885年](../Page/1885年.md "wikilink")）
  - [1831年](../Page/1831年.md "wikilink")：[列奥·冯·卡普里维](../Page/列奥·冯·卡普里维.md "wikilink")，[德意志帝国](../Page/德意志帝国.md "wikilink")[总理](../Page/德国总理.md "wikilink")（逝於[1899年](../Page/1899年.md "wikilink")）
  - [1841年](../Page/1841年.md "wikilink")：[卡尔·格雷贝](../Page/卡尔·格雷贝.md "wikilink")，德国化学家（逝於[1927年](../Page/1927年.md "wikilink")）
  - [1842年](../Page/1842年.md "wikilink")：[阿里格·博伊托](../Page/阿里格·博伊托.md "wikilink")，意大利文学家，作曲家（逝於[1918年](../Page/1918年.md "wikilink")）
  - [1852年](../Page/1852年.md "wikilink")：[寺内正毅](../Page/寺内正毅.md "wikilink")，日本第18代内阁总理大臣（逝於[1919年](../Page/1919年.md "wikilink")）
  - [1874年](../Page/1874年.md "wikilink")：[何那斯·华格纳](../Page/何那斯·华格纳.md "wikilink")，美国职棒大联盟选手（逝於[1955年](../Page/1955年.md "wikilink")）
  - [1882年](../Page/1882年.md "wikilink")：[多田骏](../Page/多田骏.md "wikilink")，日本陆军大将（逝於[1948年](../Page/1948年.md "wikilink")）
  - [1885年](../Page/1885年.md "wikilink")：[切斯特·威廉·尼米兹](../Page/切斯特·威廉·尼米兹.md "wikilink")，[美國海軍](../Page/美國海軍.md "wikilink")[第二次世界大戰](../Page/第二次世界大戰.md "wikilink")[五星上將](../Page/五星上將.md "wikilink")（逝於[1966年](../Page/1966年.md "wikilink")）
  - [1892年](../Page/1892年.md "wikilink")：[康斯坦丁·亚历山大罗维奇·费定](../Page/康斯坦丁·亚历山大罗维奇·费定.md "wikilink")，俄国/苏联作家（逝於[1977年](../Page/1977年.md "wikilink")）
  - [1906年](../Page/1906年.md "wikilink")：[贝托尔德](../Page/贝托尔德_\(巴登\).md "wikilink")，巴登藩侯（逝於[1963年](../Page/1963年.md "wikilink")）
  - [1922年](../Page/1922年.md "wikilink")：[理查德·哈密尔顿](../Page/理查德·哈密尔顿_\(艺术家\).md "wikilink")，英国艺术家（逝於[2011年](../Page/2011年.md "wikilink")）
  - [1927年](../Page/1927年.md "wikilink")：[艾曼纽·丽娃](../Page/艾曼纽·丽娃.md "wikilink")，法国女演员（逝于[2017年](../Page/2017年.md "wikilink")）
  - [1933年](../Page/1933年.md "wikilink")：[上田敏也](../Page/上田敏也.md "wikilink")，[日本](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")
  - [1933年](../Page/1933年.md "wikilink")：[犹大·福克曼](../Page/犹大·福克曼.md "wikilink")，美国著名癌学家（逝於[2008年](../Page/2008年.md "wikilink")）
  - [1934年](../Page/1934年.md "wikilink")：[雷纳塔·史科朵](../Page/雷纳塔·史科朵.md "wikilink")，意大利女高音歌唱家、舞台导演
  - [1934年](../Page/1934年.md "wikilink")：[宾古·瓦·穆塔里卡](../Page/宾古·瓦·穆塔里卡.md "wikilink")，[馬拉威](../Page/馬拉威.md "wikilink")[总统](../Page/馬拉威總統.md "wikilink")（逝于[2012年](../Page/2012年.md "wikilink")）
  - [1940年](../Page/1940年.md "wikilink")：[丹尼斯·劳](../Page/丹尼斯·劳.md "wikilink")，[苏格兰足球名将](../Page/苏格兰.md "wikilink")
  - [1942年](../Page/1942年.md "wikilink")：[乔·利伯曼](../Page/乔·利伯曼.md "wikilink")，[美国副总统候选人](../Page/美国.md "wikilink")
  - [1942年](../Page/1942年.md "wikilink")：[馬之秦](../Page/馬之秦.md "wikilink")，[臺灣女演員](../Page/臺灣.md "wikilink")、製作人
  - [1943年](../Page/1943年.md "wikilink")：[特里·塞梅尔](../Page/特里·塞梅尔.md "wikilink")，[Yahoo前主席兼首席执行官](../Page/Yahoo.md "wikilink")
  - [1944年](../Page/1944年.md "wikilink")：[戴维·瓦恩兰](../Page/戴维·瓦恩兰.md "wikilink")，[2012年](../Page/2012年.md "wikilink")[诺贝尔物理学奖得主](../Page/诺贝尔物理学奖.md "wikilink")
  - [1946年](../Page/1946年.md "wikilink")：[格列戈里·亚历山德罗维奇·马尔古利斯](../Page/格列戈里·亚历山德罗维奇·马尔古利斯.md "wikilink")，[1978年](../Page/1978年.md "wikilink")[菲尔兹奖得主](../Page/菲尔兹奖.md "wikilink")
  - [1947年](../Page/1947年.md "wikilink")：[爱德华·詹姆斯·奥莫斯](../Page/爱德华·詹姆斯·奥莫斯.md "wikilink")，[美国著名男演员](../Page/美国.md "wikilink")
  - [1947年](../Page/1947年.md "wikilink")：[鄭少秋](../Page/鄭少秋.md "wikilink")，[香港演員](../Page/香港.md "wikilink")
  - [1951年](../Page/1951年.md "wikilink")：[劉培基](../Page/劉培基.md "wikilink")，[香港著名時裝設計師](../Page/香港.md "wikilink")
  - [1953年](../Page/1953年.md "wikilink")：[羅范椒芬](../Page/羅范椒芬.md "wikilink")，[香港政界人物](../Page/香港.md "wikilink")
  - [1954年](../Page/1954年.md "wikilink")：[王心慰](../Page/王心慰.md "wikilink")，[香港電視劇監製](../Page/香港.md "wikilink")
  - [1955年](../Page/1955年.md "wikilink")：[-{zh-tw:史蒂夫·賈伯斯;
    zh-hans:史蒂夫·乔布斯;
    zh-hk:史提夫·喬布斯;}-](../Page/史蒂夫·賈伯斯.md "wikilink")，[美國電腦工程師](../Page/美國.md "wikilink")、企業家，[苹果电脑公司創辦人之一](../Page/苹果电脑公司.md "wikilink")，前首席执行官，前董事会主席，[NeXT首席执行官](../Page/NeXT.md "wikilink")，[Pixar动画工作室首席执行官](../Page/Pixar.md "wikilink")（逝於[2011年](../Page/2011年.md "wikilink")）
  - [1955年](../Page/1955年.md "wikilink")：[亞倫·保魯斯](../Page/亞倫·保魯斯.md "wikilink")，法國著名[一級方程式賽車手](../Page/一級方程式.md "wikilink")
  - [1961年](../Page/1961年.md "wikilink")：[艾力克·罗尚](../Page/艾力克·罗尚.md "wikilink")，[法国导演](../Page/法国.md "wikilink")、编剧
  - [1963年](../Page/1963年.md "wikilink")：[卡洛](../Page/卡洛_\(两西西里\).md "wikilink")，[两西西里王子](../Page/兩西西里王國.md "wikilink")
  - 1963年：[山傑·李拉·班沙里](../Page/山傑·李拉·班沙里.md "wikilink")，印度电影导演
  - [1964年](../Page/1964年.md "wikilink")：[邵仲衡](../Page/邵仲衡.md "wikilink")，[香港演員](../Page/香港.md "wikilink")
  - [1967年](../Page/1967年.md "wikilink")：[布莱恩·施密特](../Page/布莱恩·施密特.md "wikilink")，[2011年](../Page/2011年.md "wikilink")[诺贝尔物理学奖得主](../Page/诺贝尔物理学奖.md "wikilink")
  - [1967年](../Page/1967年.md "wikilink")：[金勝友](../Page/金勝友.md "wikilink")，[韩国男演员](../Page/韩国.md "wikilink")
  - [1969年](../Page/1969年.md "wikilink")：[伍詠薇](../Page/伍詠薇.md "wikilink")，[香港女演員](../Page/香港.md "wikilink")
  - [1971年](../Page/1971年.md "wikilink")：[佩德罗·德·拉·罗萨](../Page/佩德罗·德·拉·罗萨.md "wikilink")，[西班牙](../Page/西班牙.md "wikilink")[一級方程式賽車手](../Page/一級方程式賽車.md "wikilink")
  - [1972年](../Page/1972年.md "wikilink")：[吳家樂](../Page/吳家樂.md "wikilink")，[香港男演員](../Page/香港.md "wikilink")
  - [1973年](../Page/1973年.md "wikilink")：[菲利普·罗斯勒](../Page/菲利普·罗斯勒.md "wikilink")，德国自民党主席，前联邦卫生部部长
  - [1974年](../Page/1974年.md "wikilink")：[麦克·洛威尔](../Page/麦克·洛威尔.md "wikilink")，[美国职业棒球手](../Page/美国.md "wikilink")
  - [1975年](../Page/1975年.md "wikilink")：[潘文迪](../Page/潘文迪.md "wikilink")，[香港足球評述員](../Page/香港.md "wikilink")、運動員
  - [1976年](../Page/1976年.md "wikilink")：[扎克·约翰逊](../Page/扎克·约翰逊.md "wikilink")，[美国高尔夫球名手](../Page/美国.md "wikilink")
  - [1977年](../Page/1977年.md "wikilink")：[布洛申·阿罗约](../Page/布洛申·阿罗约.md "wikilink")，[美国职业棒球手](../Page/美国.md "wikilink")
  - [1978年](../Page/1978年.md "wikilink")：[姜熙建](../Page/Gary_\(韓國藝人\).md "wikilink")，[韓國主持人](../Page/韓國.md "wikilink")、組合[Leessang成員](../Page/Leessang.md "wikilink")
  - [1978年](../Page/1978年.md "wikilink")：[竹宮悠由子](../Page/竹宮悠由子.md "wikilink")，[日本女性](../Page/日本.md "wikilink")[小說家](../Page/小說家.md "wikilink")
  - [1978年](../Page/1978年.md "wikilink")：[-{zh-tw:里昂·康斯坦丁;
    zh-hk:萊昂·干斯坦汀;
    zh-hans:里昂·康斯坦丁;}-](../Page/萊昂·干斯坦汀.md "wikilink")，[英格兰足球运动员](../Page/英格兰.md "wikilink")
  - [1981年](../Page/1981年.md "wikilink")：[莱顿·休伊特](../Page/莱顿·休伊特.md "wikilink")，[澳洲](../Page/澳洲.md "wikilink")[網球運動員](../Page/網球.md "wikilink")
  - [1981年](../Page/1981年.md "wikilink")：[奧田惠梨華](../Page/奧田惠梨華.md "wikilink")，[日本女演員](../Page/日本.md "wikilink")
  - [1982年](../Page/1982年.md "wikilink")：[陳法拉](../Page/陳法拉.md "wikilink")，[香港女演員](../Page/香港.md "wikilink")
  - [1982年](../Page/1982年.md "wikilink")：[程文欣](../Page/程文欣.md "wikilink")，[台湾女子羽毛球运动员](../Page/台湾.md "wikilink")
  - [1982年](../Page/1982年.md "wikilink")：[Melody.](../Page/Melody..md "wikilink")，[日本女歌手](../Page/日本.md "wikilink")、节目主持人
  - [1985年](../Page/1985年.md "wikilink")：[威廉·奎斯特](../Page/威廉·奎斯特.md "wikilink")，[丹麦足球运动员](../Page/丹麦.md "wikilink")
  - [1986年](../Page/1986年.md "wikilink")：[李雪瑩](../Page/李雪瑩.md "wikilink")，[香港女演員](../Page/香港.md "wikilink")、2010年香港小姐競選最上鏡小姐
  - [1987年](../Page/1987年.md "wikilink")：[金圭鐘](../Page/金圭鐘.md "wikilink")，[韓國偶像團體](../Page/韓國.md "wikilink")[SS501成員](../Page/SS501.md "wikilink")
  - [1987年](../Page/1987年.md "wikilink")：[岩佐真悠子](../Page/岩佐真悠子.md "wikilink")，[日本女演員](../Page/日本.md "wikilink")
  - [1991年](../Page/1991年.md "wikilink")：[羅惠美](../Page/羅惠美.md "wikilink")，[韓國女](../Page/韓國.md "wikilink")[演員](../Page/演員.md "wikilink")
  - [1994年](../Page/1994年.md "wikilink")：[岑日珈](../Page/岑日珈.md "wikilink")，[香港女歌手](../Page/香港.md "wikilink")
  - [1995年](../Page/1995年.md "wikilink")：[Zi.U](../Page/Zi.U.md "wikilink")，[韓國女子偶像團體](../Page/韓國.md "wikilink")[BP
    RANIA成員](../Page/BP_RANIA.md "wikilink")
  - [1997年](../Page/1997年.md "wikilink")：[矢倉楓子](../Page/矢倉楓子.md "wikilink")，[日本女子團體](../Page/日本.md "wikilink")[NMB48成員](../Page/NMB48.md "wikilink")
  - [2000年](../Page/2000年.md "wikilink")：[米穀奈奈未](../Page/米穀奈奈未.md "wikilink")，[日本女子團體](../Page/日本.md "wikilink")[櫸坂46成員](../Page/櫸坂46.md "wikilink")
  - [2002年](../Page/2002年.md "wikilink")：[丁程鑫](../Page/丁程鑫.md "wikilink")，[中國](../Page/中國.md "wikilink")，[台風少年團成員](../Page/台風少年團.md "wikilink")

## 逝世

  - [1281年](../Page/1281年.md "wikilink")：[瑪·登哈一世](../Page/瑪·登哈一世.md "wikilink")，[東方亞述教會牧首](../Page/巴比倫牧首列表.md "wikilink")，曾提拔“[一位來自中國的神父](../Page/瑪·約伯耶和華三世.md "wikilink")”成為[總主教](../Page/總主教.md "wikilink")，後來該名主教當選下任[牧首](../Page/牧首.md "wikilink")
  - [1856年](../Page/1856年.md "wikilink")：（[公曆](../Page/公曆.md "wikilink")，合[儒略曆](../Page/儒略曆.md "wikilink")[2月12日](../Page/2月12日.md "wikilink")）[羅巴切夫斯基](../Page/羅巴切夫斯基.md "wikilink")，俄國[數學家](../Page/數學家.md "wikilink")（1792年出生）
  - [1935年](../Page/1935年.md "wikilink")：[何叔衡](../Page/何叔衡.md "wikilink")，[中國共產黨最早的黨員之一](../Page/中國共產黨.md "wikilink")（1876年出生）
  - [1940年](../Page/1940年.md "wikilink")：[韦灿](../Page/韦灿.md "wikilink")，[中华民国](../Page/中华民国.md "wikilink")[国民革命军将领](../Page/国民革命军.md "wikilink")
  - [1962年](../Page/1962年.md "wikilink")：[胡適](../Page/胡適.md "wikilink")，中國文學家，外交家（1891年出生）
  - [2005年](../Page/2005年.md "wikilink")：[朱漢華](../Page/朱漢華.md "wikilink")，[香港政界人物](../Page/香港.md "wikilink")（1950年出生）
  - [2006年](../Page/2006年.md "wikilink")：
  - 2006年：[杜希德](../Page/杜希德.md "wikilink")，[英國](../Page/英國.md "wikilink")[漢學家](../Page/漢學.md "wikilink")，[歷史學家](../Page/歷史學家.md "wikilink")，在劍橋大學去世（1925年出生）
  - [2010年](../Page/2010年.md "wikilink")：[洪一峰](../Page/洪一峰.md "wikilink")，[台灣歌手](../Page/台灣.md "wikilink")（1927年出生）
  - 2010年：[宮粉紅](../Page/宮粉紅.md "wikilink")，[香港](../Page/香港.md "wikilink")[粵劇紅伶](../Page/粵劇.md "wikilink")（1911年出生）
  - [2015年](../Page/2015年.md "wikilink")：[蕭泰然](../Page/蕭泰然.md "wikilink")，臺灣音樂家，有「臺灣的[拉赫曼尼諾夫](../Page/拉赫曼尼諾夫.md "wikilink")」之稱\[1\]（1938年出生）
  - [2018年](../Page/2018年.md "wikilink")：[詩麗黛瑋](../Page/詩麗黛瑋.md "wikilink")，印度女影星（1963年出生）

## 节假日和习俗

  -
## 參考資料

1.