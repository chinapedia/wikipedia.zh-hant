**汤**，[商朝的创建者](../Page/商朝.md "wikilink")。(公元前1675年-前1587年）[子姓](../Page/子姓.md "wikilink")，名**履**，今人多称**商汤**，又称**武汤**、**天乙**、**成汤**、**成唐**，商代[金文和](../Page/金文.md "wikilink")[甲骨文称為](../Page/甲骨文.md "wikilink")**唐**\[1\]\[2\]\[3\]、**成**\[4\]\[5\]、**大乙**（**太乙**），又称**高祖乙**，原[商部落首领](../Page/商部落.md "wikilink")，与[有莘氏通婚后](../Page/有莘氏.md "wikilink")，任贤臣[伊尹和](../Page/伊尹.md "wikilink")[仲虺为左右相](../Page/仲虺.md "wikilink")，以[亳为前进据点](../Page/亳.md "wikilink")，积极治国，准备灭[夏朝](../Page/夏朝.md "wikilink")。

当时[夏朝](../Page/夏朝.md "wikilink")，帝[桀在位](../Page/桀.md "wikilink")，夏朝国势渐衰，矛盾异常尖锐，汤乘机起兵，首先攻灭[葛](../Page/葛.md "wikilink")（今[河南](../Page/河南.md "wikilink")[宁陵北](../Page/宁陵.md "wikilink")）及十多小国和部落。接着又克[韦](../Page/韦.md "wikilink")（今河南[滑县东南](../Page/滑县.md "wikilink")）、[顾](../Page/顾.md "wikilink")（今河南[范县东南](../Page/范县.md "wikilink")）、[昆吾](../Page/昆吾.md "wikilink")（今河南[许昌东部](../Page/许昌.md "wikilink")）等小国。经过11次战役，使得夏王朝空前的孤立無援，又利用有娀氏的反叛，起兵于[鸣条之战打敗夏桀王](../Page/鸣条之战.md "wikilink")，一举灭夏。由于商汤以武力灭夏，打破君王永定的说法，因而史称“湯武革命”。汤建立[商朝后](../Page/商朝.md "wikilink")，对内减轻征敛，鼓励生产，安抚民心，从而扩展了统治区域，影响远至[黄河上游](../Page/黄河.md "wikilink")，[氐](../Page/氐.md "wikilink")、[羌部落都来纳贡归服](../Page/羌.md "wikilink")。

成汤在商代文字称为“唐”\[6\]\[7\]\[8\]。在祭祀先祖时，殷商人也称呼成汤为“成”\[9\]\[10\]、“大（太）乙”。

## 在位年數

現今流傳文獻記載的成湯在位年數有2種說法：

  - 在位12年（若含商部落首領在位17年，合計29年），《今本[竹書紀年](../Page/竹書紀年.md "wikilink")》。
  - 在位13年（若含商部落首領在位17年，合計30年），《太平御覽》卷83引《韓詩外傳》，《[帝王世紀](../Page/帝王世紀.md "wikilink")》、《[皇極經世](../Page/皇極經世.md "wikilink")》、《[資治通鑑外紀](../Page/資治通鑑外紀.md "wikilink")》、《[通志](../Page/通志.md "wikilink")》、《[資治通鑑前編](../Page/資治通鑑前編.md "wikilink")》、《[文獻通考](../Page/文獻通考.md "wikilink")》均同。

## 評價

湯是[儒家推崇的](../Page/儒家.md "wikilink")[上古聖王之一](../Page/上古.md "wikilink")。
大乙配妣丙”（36194），只有她在受周祭的范围之内。另外单称“妣丙”

## 妻

  - 妣丙，[甲骨文作](../Page/甲骨文.md "wikilink")“大乙配妣丙”，又称“高妣丙”，出自[有莘氏](../Page/有莘氏.md "wikilink")，[太丁之母](../Page/太丁.md "wikilink")
  - 妣甲，[甲骨文作](../Page/甲骨文.md "wikilink")“卜丙母妣甲”，[外丙之母](../Page/外丙.md "wikilink")

## 注釋

## 參考文獻

## 參見

  -
{{-}}

[Category:夏朝人](../Category/夏朝人.md "wikilink")
[Category:子姓](../Category/子姓.md "wikilink")
[Category:商朝君主](../Category/商朝君主.md "wikilink")
[Category:入祀历代帝王庙景德崇圣殿](../Category/入祀历代帝王庙景德崇圣殿.md "wikilink")

1.

2.

3.

4.

5.

6.
7.
8.
9.
10.