**胡进洁**勋爵（Sir **George W.
Hunter**，）是一位[中国内地会](../Page/中国内地会.md "wikilink")[传教士](../Page/传教士.md "wikilink")，在中国新疆传教57年。

1861年，胡进洁出生在[苏格兰金卡丁郡](../Page/苏格兰.md "wikilink")（Kincardineshire），幼年丧母，年轻时，情侣杰西在22岁去世，安葬在[阿伯丁](../Page/阿伯丁.md "wikilink")，此后胡进洁便献身传教事业。1889年来到中国，先在甘肃传教，1906年进入[新疆](../Page/新疆.md "wikilink")[迪化](../Page/迪化.md "wikilink")，以后一直留在那里。他在中国传教57年，只在1900年回国一次。他将圣经翻译成各种民族的语言，印刷量很小，
\[1\]

1934年，新疆督办[盛世才借助苏联红军的力量打败了](../Page/盛世才.md "wikilink")[马仲英](../Page/马仲英.md "wikilink")，开始在新疆推行“反帝亲苏”政策，1935年，他驱逐了在新疆的各国传教士，胡进洁则被控以间谍罪名加以逮捕。

1946年12月20日，胡进洁在中国[甘肃省去世](../Page/甘肃省.md "wikilink")。

## 传记

  - [盖群英](../Page/盖群英.md "wikilink")、[冯贵石](../Page/冯贵石.md "wikilink")：新疆使徒胡进洁（1948）
  - B.V. Henry, Fakkelbæreren til de ukjente: pionermisjonæren 胡进洁i.中亚,
    [奥斯陆](../Page/奥斯陆.md "wikilink") :Lunde (2001)

## 参见

  - [内地会在华传教士列表](../Page/内地会在华传教士列表.md "wikilink")
  - [马之华](../Page/马之华.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

[H](../Category/内地会传教士.md "wikilink")
[H](../Category/英國基督徒.md "wikilink")

1.  [盖群英](../Page/盖群英.md "wikilink")、[冯贵石](../Page/冯贵石.md "wikilink")：新疆使徒胡进洁（1948）