**朔莊省**（\[1\]），一作“滀臻省”，又譯“蓄臻省”\[2\]，是[越南西南部的一個省](../Page/越南.md "wikilink")，區屬[湄公河三角洲](../Page/湄公河三角洲.md "wikilink")，省莅[朔莊市](../Page/朔莊市.md "wikilink")。

## 行政區劃

朔庄省下辖1市2市社8县。

  - [朔莊市](../Page/朔莊市.md "wikilink")（Thành phố Sóc Trăng）
  - [永州市社](../Page/永州市社.md "wikilink")（Thị xã Vĩnh Châu）
  - [我𠄼市社](../Page/我𠄼市社.md "wikilink")（Thị xã Ngã Năm）
  - [計策縣](../Page/計策縣.md "wikilink")（Huyện Kế Sách）
  - [隆富縣](../Page/隆富縣.md "wikilink")（Huyện Long Phú）
  - [岣崂榕县](../Page/岣崂榕县.md "wikilink")（Huyện Cù Lao Dung）
  - [美秀縣](../Page/美秀縣.md "wikilink")（Huyện Mỹ Tú）
  - [美川縣](../Page/美川縣.md "wikilink")（Huyện Mỹ Xuyên）
  - [盛治縣](../Page/盛治縣.md "wikilink")（Huyện Thạnh Trị）
  - [周城县](../Page/周城縣_\(朔庄省\).md "wikilink")（Huyện Châu Thành）
  - [镇夷县](../Page/镇夷县.md "wikilink")（Huyện Trần Đề）

## 相册

<File:Bưu> điện Sóc Trăng.jpg|朔庄市区夜景

## 注释

## 外部連結

  - [朔庄省政府电子通信门户网站](http://www.soctrang.gov.vn/)

[Category:越南省份](../Category/越南省份.md "wikilink")
[Category:朔莊省](../Category/朔莊省.md "wikilink")

1.  “朔莊”见于《[南国地舆](../Page/南国地舆.md "wikilink")》等法属时期的书籍；“滀臻”见于《[大南实录](../Page/大南实录.md "wikilink")》；“滀𪩮”见于《[皇越一統輿地志](../Page/皇越一統輿地志.md "wikilink")》和《[嘉定城通志](../Page/嘉定城通志.md "wikilink")》。
2.  多见于台湾出版的相关书籍和地图。