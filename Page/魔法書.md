**魔法書**（）指在[中世紀後期到](../Page/中世紀.md "wikilink")[十八世紀期間](../Page/十八世紀.md "wikilink")，描述關於[魔法的條列和實作的魔导書籍](../Page/魔法.md "wikilink")。內容有包含相關於[占星學的知識](../Page/占星學.md "wikilink")、也有條列[天使及](../Page/天使.md "wikilink")[惡魔的名單](../Page/惡魔.md "wikilink")、使用[符咒和](../Page/符咒.md "wikilink")[咒語的方法](../Page/咒語.md "wikilink")，用以製做藥物及護身符、召喚像是惡魔一類的[超自然现象存在實體](../Page/超自然现象.md "wikilink")。任何有關於魔法的書（尤其是寫關於咒語）也都可以被稱為魔法書。

许多魔法书都重点阐述了[卡巴拉](../Page/卡巴拉.md "wikilink")、[犹太教神秘哲学的魔法和占星术知识](../Page/犹太教.md "wikilink")。也有许多人称，魔法书中很多内容都来自《[圣经](../Page/圣经.md "wikilink")》中的[所罗门国王的相关作品](../Page/所罗门.md "wikilink")。一些著名的魔法书有《[所罗门的钥匙](../Page/所罗门的钥匙.md "wikilink")》、《[所罗门的小钥匙](../Page/所罗门的小钥匙.md "wikilink")》、《[阿巴太尔魔法](../Page/阿巴太尔魔法.md "wikilink")》、《[七日谈](../Page/七日谈.md "wikilink")》、《[洪诺流的魔法书](../Page/洪诺流的魔法书.md "wikilink")》、《[黑色小母鸡](../Page/黑色小母鸡.md "wikilink")》、《[万事通占卜家鲁道夫](../Page/万事通占卜家鲁道夫.md "wikilink")》、《[神圣的魔法](../Page/神圣的魔法.md "wikilink")》等。

## 参考书目

  -
## 外部链接

  - [Internet Sacred Text Archives:
    Grimoires](http://www.sacred-texts.com/grim/)
  - [Hermetics Library of Magical & Mystical
    E-Books](http://www.hermetics.org/ebooks.html)
  - [Magical
    Athenaeum](http://asiya.org/index.php?topic=MagicalAthenaeum) - a
    collection of magical PDF files
  - [Timeline of esoterica](http://www.esotericarchives.com/esotime.htm)
  - [1](http://www.digital-brilliance.com/kab/karr/Solomon/index.htm)
    Solomonic Magic
  - [魔法书org](https://web.archive.org/web/20130606194838/http://txt.mf-3.org/)

[Category:秘教](../Category/秘教.md "wikilink")
[Category:神秘學](../Category/神秘學.md "wikilink")
[Category:文學體裁](../Category/文學體裁.md "wikilink")
[Category:魔鬼交易](../Category/魔鬼交易.md "wikilink")