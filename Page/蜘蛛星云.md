[A_New_View_of_the_Tarantula_Nebula.jpg](https://zh.wikipedia.org/wiki/File:A_New_View_of_the_Tarantula_Nebula.jpg "fig:A_New_View_of_the_Tarantula_Nebula.jpg")（藍色）、[哈伯](../Page/哈伯太空望遠鏡.md "wikilink")（綠色）、[史匹哲](../Page/史匹哲太空望遠鏡.md "wikilink")（紅色）三架[太空望遠鏡的觀測資料疊合成的蜘蛛星云影像](../Page/太空望遠鏡.md "wikilink")，皆攝於2012年\]\]

**NGC 2070**
是位于[剑鱼座](../Page/剑鱼座.md "wikilink")[大麦哲伦星系中的一個](../Page/大麦哲伦星系.md "wikilink")[弥漫星云](../Page/弥漫星云.md "wikilink")，亦稱**蜘蛛星雲（Tarantula
Nebula）**或**剑鱼座30（30
Doradus）**。蜘蛛星云一开始被人们认为是一颗[恒星](../Page/恒星.md "wikilink")，直到1751年[法国](../Page/法国.md "wikilink")[天文学家](../Page/天文学家.md "wikilink")[尼可拉·路易·拉卡伊才发现它其实是一个](../Page/尼可拉·路易·拉卡伊.md "wikilink")[星云](../Page/星云.md "wikilink")。

## 特征

虽然蜘蛛星云的[视星等只有](../Page/视星等.md "wikilink")8等，但它是位于160,000[光年远的](../Page/光年.md "wikilink")[大麦哲伦星系内](../Page/大麦哲伦星系.md "wikilink")，所以实际上是相当明亮的\[1\]，如果将它移到[猎户座大星云的位置](../Page/猎户座大星云.md "wikilink")，它在夜晚发出的光芒将可以在地面照出影子。实际上，它是[本星系团中已知最活跃也是最大的一处恒星形成区](../Page/本星系团.md "wikilink")，其直径大约为200[秒差距](../Page/秒差距.md "wikilink")，将近1000光年。

在蜘蛛星云的中心是一个直径约35光年的巨大蓝巨星簇[R136](../Page/R136.md "wikilink")\[2\]，蜘蛛星云的大部分光芒就是由R136所激发而发出的。据估计R136大约有450,000个[太阳质量](../Page/太阳质量.md "wikilink")，在未来将极有可能变成一个[球状星团](../Page/球状星团.md "wikilink")\[3\]。2010年7月21日，[英国天文学家宣布在R](../Page/英国.md "wikilink")136中发现迄今为止已知的最大恒星[R136a1](../Page/R136a1.md "wikilink")\[4\]。
此外，蜘蛛星云中还拥有一个相对古老的[星团](../Page/星团.md "wikilink")—[霍奇301](../Page/霍奇301.md "wikilink")（Hodge
301），其年龄大约在两千万至两千五百万年左右，星团中一些大质量的恒星已经发生[超新星爆炸](../Page/超新星.md "wikilink")\[5\]。

1987年2月，位于蜘蛛星云外围的[蓝超巨星](../Page/蓝超巨星.md "wikilink")[Sanduleak -69°
202a死亡发生超新星爆炸](../Page/Sanduleak_-69°_202a.md "wikilink")，形成[SN
1987A](../Page/SN_1987A.md "wikilink")。这是自1604年[开普勒超新星](../Page/开普勒超新星.md "wikilink")（SN
1604）以来观测到的最明亮的超新星爆炸，也是[望远镜发明之后离](../Page/望远镜.md "wikilink")[地球距离最近的一次超新星爆炸](../Page/地球.md "wikilink")。

## 图集

<center>

Image:Tarantula Nebula - Hubble.jpg|蜘蛛星云的核心部分;
由[哈勃太空望远镜拍摄的](../Page/哈勃太空望远镜.md "wikilink")15张照片拼接而成.
版权:
[NASA](../Page/NASA.md "wikilink")/[ESA](../Page/ESA.md "wikilink")/Danny
LaCrue.
Image:ESO-Ring-shaped-Nebula-phot-34a-04-fullres.jpg|大麦哲伦星系靠近蜘蛛星云的部分
Image:Tarantula_Nebula.jpg|蜘蛛星云的中心: 位于中间的是R136，右上方为Hodge 301。 版权:
[ESO](../Page/ESO.md "wikilink"). Image:Tarantula Nebula and its
surroundings.jpg|蜘蛛星云和它的周围. 版权: [ESO](../Page/ESO.md "wikilink").
Image:ESO-Filaments in the Tarantula
Nebula-phot-34b-04-fullres.jpg|蜘蛛星云的触角. 版权:
[ESO](../Page/ESO.md "wikilink"). Image:ESO 2.2-m WFI Image of the
Tarantula Nebula (4598317299).jpg|大麦哲伦星系中的蜘蛛星云（中间部分）

</center>

## 参考文献

{{-}}

[103](../Category/科德韦尔天体.md "wikilink")
[Category:蜘蛛星云](../Category/蜘蛛星云.md "wikilink")

1.
2.

3.

4.

5.