## 语言、文字

  - H0　[语言学](../Page/语言学.md "wikilink")

:\*H01 [语音学](../Page/语音学.md "wikilink")

::\*H019
[朗诵法](../Page/朗诵法.md "wikilink")、[演讲术](../Page/演讲术.md "wikilink")

:\*H02 [文字学](../Page/文字学.md "wikilink")

:\*H03
[语义学](../Page/语义学.md "wikilink")、[词汇学](../Page/词汇学.md "wikilink")、[词义学](../Page/词义学.md "wikilink")

::\*H033 [熟语](../Page/熟语.md "wikilink")

::\*H034 [俗语](../Page/俗语.md "wikilink")

:\*H04 [语法学](../Page/语法学.md "wikilink")

:\*H05 [写作学](../Page/写作学.md "wikilink")、[修辞学](../Page/修辞学.md "wikilink")

::\*H059 [翻译学](../Page/翻译学.md "wikilink")

:\*H06 [词典学](../Page/词典学.md "wikilink")

::\*H061 [词典](../Page/词典.md "wikilink")

  - H1　[汉语](../Page/汉语.md "wikilink")

::\*H102
[汉语的规范化](../Page/汉语.md "wikilink")、标准化、推广[普通话](../Page/普通话.md "wikilink")

:::\*H109.2 [古代汉语](../Page/古代汉语.md "wikilink")

:::\*H109.4 [现代汉语](../Page/现代汉语.md "wikilink")

:\*H11 [语音](../Page/语音.md "wikilink")（[音韵学](../Page/音韵学.md "wikilink")）

:\*H12 [文字学](../Page/文字学.md "wikilink")

:::\*H125.4 拼音读物

::\*H126 [特种文字](../Page/特种文字.md "wikilink")

:\*H13
[语义](../Page/语义.md "wikilink")、[词汇](../Page/词汇.md "wikilink")、[词义](../Page/词义.md "wikilink")（[训诂学](../Page/训诂学.md "wikilink")）

::\*H131 [古代词汇](../Page/古代词汇.md "wikilink")

::\*H136 [现代词汇](../Page/现代词汇.md "wikilink")

:::\*H136.3 [成语](../Page/成语.md "wikilink")

:::\*H136.4 [俗语](../Page/俗语.md "wikilink")

::\*H139
[词源学](../Page/词源学.md "wikilink")（[字源学](../Page/字源学.md "wikilink")）

:\*H14 [语法](../Page/语法.md "wikilink")

::\*H141
[古代语法](../Page/古代语法.md "wikilink")（[文言语法](../Page/文言语法.md "wikilink")）

::\*H146 近现代语法

:\*H15 [写作](../Page/写作.md "wikilink")、[修辞](../Page/修辞.md "wikilink")

::\*H151 [风格论](../Page/风格论.md "wikilink")

::\*H152 [文体论](../Page/文体论.md "wikilink")

::\*H155 [标点法](../Page/标点法.md "wikilink")

::\*H159 [翻译](../Page/翻译.md "wikilink")

:\*H16
[字典](../Page/字典.md "wikilink")、[词典](../Page/词典.md "wikilink")、[古代字书](../Page/古代字书.md "wikilink")

::\*H161 《[说文](../Page/说文.md "wikilink")》

::\*H162 其他[字书](../Page/字书.md "wikilink")

::\*H163 [字典](../Page/字典.md "wikilink")

::\*H164 [词典](../Page/词典.md "wikilink")

:\*H17 [方言](../Page/方言.md "wikilink")

:\*H19 [汉语教学](../Page/汉语教学.md "wikilink")

::\*H194 汉语读物

:::\*H194.1 古代汉语读物

:::\*H194.3 现代汉语读物

:::\*H194.4 初级学校读物

:::\*H194.5 中级学校读物

  - H2　[中国少数民族语言](../Page/中国少数民族语言.md "wikilink")
  - H3　常用外国语

:\*H31 [英语](../Page/英语.md "wikilink")

:::\*H310.1 [非标准英语](../Page/非标准英语.md "wikilink")

:::\*H310.4 [英语水平考试](../Page/英语水平考试.md "wikilink")

::::\*H310.41 世界

::::\*H310.42 中国

::\*H311 [语音](../Page/语音.md "wikilink")

::\*H312 [文字](../Page/文字.md "wikilink")

::\*H313
[语义](../Page/语义.md "wikilink")、[词汇](../Page/词汇.md "wikilink")、[词义](../Page/词义.md "wikilink")

::\*H314 [语法](../Page/语法.md "wikilink")

::\*H315 写作修辞

::\*H316 英语词典

::\*H317 方言

::\*H319 英语语文教学

:::\*H319.4 英语读物

:::\*H319.6 习题、试题

:::\*H319.9 会话

:\*H32 [法语](../Page/法语.md "wikilink")

:\*H33 [德语](../Page/德语.md "wikilink")

:\*H34 [西班牙语](../Page/西班牙语.md "wikilink")

:\*H35 [俄语](../Page/俄语.md "wikilink")

:\*H36 [日语](../Page/日语.md "wikilink")

::\*H366 日语词典

:\*H37 [阿拉伯语](../Page/阿拉伯语.md "wikilink")

  - H4　[汉藏语系](../Page/汉藏语系.md "wikilink")
  - H5　[阿尔泰语系](../Page/阿尔泰语系.md "wikilink")（突厥-蒙古-通古斯语系）
  - H6 ...

:\*H61 [南亚语系](../Page/南亚语系.md "wikilink")（澳斯特罗-亚细亚语系）

:\*H62
[南印语系](../Page/南印语系.md "wikilink")（[达罗毗荼语系](../Page/达罗毗荼语系.md "wikilink")、[德拉维达语系](../Page/德拉维达语系.md "wikilink")）

:\*H63 [南岛语系](../Page/南岛语系.md "wikilink")（马来亚-玻里尼西亚语系）

:\*H64 [东北亚诸语言](../Page/东北亚诸语言.md "wikilink")

:\*H65 [高加索语系（伊比利亚-高加索语系）](../Page/高加索诸语言.md "wikilink")

:\*H66
[乌拉尔语系](../Page/乌拉尔语系.md "wikilink")（[芬兰-乌戈尔语系](../Page/芬兰-乌戈尔语族.md "wikilink")）

:\*H67 [闪-含语系](../Page/闪-含语系.md "wikilink")（阿非罗-亚细亚语系）

  - H7　[印欧语系](../Page/印欧语系.md "wikilink")
  - H8 ...

:\*H81 [非洲诸语言](../Page/非洲诸语系.md "wikilink")

:\*H83 [美洲诸语言](../Page/美洲诸语系.md "wikilink")

:\*H84 [大洋洲诸语言](../Page/非大洋诸语系.md "wikilink")

  - H9 [国际辅助语](../Page/国际辅助语.md "wikilink")

-----

[Category:中国图书馆图书分类法](../Category/中国图书馆图书分类法.md "wikilink")