[The_Sounds_of_Earth_Record_Cover_-_GPN-2000-001978.jpg](https://zh.wikipedia.org/wiki/File:The_Sounds_of_Earth_Record_Cover_-_GPN-2000-001978.jpg "fig:The_Sounds_of_Earth_Record_Cover_-_GPN-2000-001978.jpg")

**旅行者金唱片**（Voyager Golden
Records）是一張於1977年隨兩艘[航海家探測器發射到太空的](../Page/航海家計畫.md "wikilink")[唱片](../Page/唱片.md "wikilink")。唱片內收錄了用以表述[地球上各種](../Page/地球.md "wikilink")[文化及](../Page/文化.md "wikilink")[生命的](../Page/生命.md "wikilink")[聲音及](../Page/聲音.md "wikilink")[圖像](../Page/圖像.md "wikilink")，以期宇宙中其他[外星高智慧生物發現](../Page/外星生命.md "wikilink")。旅行者探測器在距今40000年後，才會靠近最接近地球（1.6[光年](../Page/光年.md "wikilink")）的[恆星](../Page/恆星.md "wikilink")。探測器被捕獲的可能性不大，因此唱片的最終目的雖然仍是與外星人[溝通](../Page/溝通.md "wikilink")，但其對人類與宇宙之間關係的象徵意義更大。

## 背景

[先驅者號計劃的](../Page/先驅者號計劃.md "wikilink")[10號和](../Page/先驅者10號.md "wikilink")[11號於](../Page/先驅者11號.md "wikilink")1972年及1973年分別發射，各攜有一塊細小的[先驅者鍍金鋁板](../Page/先驅者鍍金鋁板.md "wikilink")，以說明發射的時間和地點，以便未来發現它們的太空探險者辨識。[美國太空總署以此為基礎](../Page/美國太空總署.md "wikilink")，設計一個更加複雜及詳細的“訊息板”，置於旅行者探測器之上。新的訊息板更像一個[時間囊](../Page/時間囊.md "wikilink")，向外太空的其他生物介绍人類身處的世界。

## 唱片封面

<div align="center">

[<File:VgrCover_zh.jpg>](https://zh.wikipedia.org/wiki/File:VgrCover_zh.jpg "fig:File:VgrCover_zh.jpg")

*圖解旅行者金唱片封面上的圖案，翻譯自美國太空總署提供的解釋。*

</div>

另外，唱片封套上也包括了一块高纯度的[铀](../Page/铀.md "wikilink")238。由于已知其衰变为[钍](../Page/钍.md "wikilink")234的[半衰期约为](../Page/半衰期.md "wikilink")44.7亿年，捕获此唱片的外星生命可据此推算出探测器的发射日期。

## 內容

唱片的收錄內容由以[卡爾·薩根為首的美國太空總署委員會所決定](../Page/卡爾·薩根.md "wikilink")。委員會選擇115幅圖像，另外还附加了1张校准图像\[1\]，以[模拟信号方式收錄于唱片内](../Page/模拟信号.md "wikilink")。太空總署收到大眾针对[先驅者鍍金鋁板上描绘裸體男女圖像的批評後](../Page/先驅者鍍金鋁板.md "wikilink")，不再批准薩根和他的同事於唱片內收錄一名裸體男性及一名裸體孕婦的照片，改以一對男女的輪廓代替\[2\]。

唱片也附上卡特總統和时任[聯合國秘書長](../Page/聯合國.md "wikilink")[庫爾特·瓦爾德海姆的书面信息](../Page/庫爾特·瓦爾德海姆.md "wikilink")：

委員會也也選擇多種[大自然的聲音](../Page/大自然.md "wikilink")，包括[滑浪](../Page/滑浪.md "wikilink")、[風](../Page/風.md "wikilink")、[雷](../Page/雷.md "wikilink")、[鳥鳴](../Page/鳥.md "wikilink")、[鯨魚歌聲](../Page/鯨魚.md "wikilink")、其他[動物叫聲等](../Page/動物.md "wikilink")，並挑選來自不同文化及年代的音樂、地球人使用55種語言（或方言）问安的录音。这些录音以每分鐘16⅔轉的速度播放。

以下是55種收錄於金唱片內的語言：

  - [阿卡德語](../Page/阿卡德語.md "wikilink")
  - [閩南語](../Page/閩南語.md "wikilink")
  - [阿拉伯語](../Page/阿拉伯語.md "wikilink")
  - [亞拉姆語](../Page/亞拉姆語.md "wikilink")
  - [亞美尼亞語](../Page/亞美尼亞語.md "wikilink")
  - [孟加拉語](../Page/孟加拉語.md "wikilink")
  - [緬甸語](../Page/緬甸語.md "wikilink")
  - [粵語](../Page/粵語.md "wikilink")
  - [捷克語](../Page/捷克語.md "wikilink")
  - [荷蘭語](../Page/荷蘭語.md "wikilink")
  - [英語](../Page/英語.md "wikilink")
  - [法語](../Page/法語.md "wikilink")
  - [德語](../Page/德語.md "wikilink")
  - [古希臘語](../Page/古希臘語.md "wikilink")
  - [古吉拉特語](../Page/古吉拉特語.md "wikilink")
  - [希伯來語](../Page/希伯來語.md "wikilink")
  - [印地語](../Page/印地語.md "wikilink")
  - [西台語](../Page/西台語.md "wikilink")
  - [匈牙利語](../Page/匈牙利語.md "wikilink")
  - [伊拉族語](../Page/伊拉族語.md "wikilink")（[贊比亞](../Page/贊比亞.md "wikilink")）
  - [印尼語](../Page/印尼語.md "wikilink")
  - [意大利語](../Page/意大利語.md "wikilink")
  - [日語](../Page/日語.md "wikilink")
  - [卡納達語](../Page/卡納達語.md "wikilink")
  - [朝鮮語](../Page/朝鮮語.md "wikilink")
  - [拉丁語](../Page/拉丁語.md "wikilink")
  - [干達語](../Page/干達語.md "wikilink")
  - [普通话](../Page/現代標準漢語.md "wikilink")
  - [馬拉地語](../Page/馬拉地語.md "wikilink")
  - [尼泊爾語](../Page/尼泊爾語.md "wikilink")
  - [恩古尼語](../Page/恩古尼語.md "wikilink")
  - [齊切瓦語](../Page/齊切瓦語.md "wikilink")
  - [奧利亞語](../Page/奧利亞語.md "wikilink")
  - [波斯語](../Page/波斯語.md "wikilink")
  - [波蘭語](../Page/波蘭語.md "wikilink")
  - [葡萄牙語](../Page/葡萄牙語.md "wikilink")
  - [旁遮普語](../Page/旁遮普語.md "wikilink")
  - [克丘亞語](../Page/克丘亞語.md "wikilink")
  - [拉賈斯坦語](../Page/拉賈斯坦語.md "wikilink")
  - [羅馬尼亞語](../Page/羅馬尼亞語.md "wikilink")
  - [俄語](../Page/俄語.md "wikilink")
  - [塞爾維亞-克羅地亞語](../Page/塞爾維亞-克羅地亞語.md "wikilink")
  - [僧伽羅語](../Page/僧伽羅語.md "wikilink")
  - [塞索托語](../Page/塞索托語.md "wikilink")
  - [西班牙語](../Page/西班牙語.md "wikilink")
  - [蘇美爾語](../Page/蘇美爾語.md "wikilink")
  - [瑞典語](../Page/瑞典語.md "wikilink")
  - [泰盧固語](../Page/泰盧固語.md "wikilink")
  - [泰語](../Page/泰語.md "wikilink")
  - [土耳其語](../Page/土耳其語.md "wikilink")
  - [烏克蘭語](../Page/烏克蘭語.md "wikilink")
  - [烏爾都語](../Page/烏爾都語.md "wikilink")
  - [越南語](../Page/越南語.md "wikilink")
  - [威爾士語](../Page/威爾士語.md "wikilink")
  - [吳語](../Page/吳語.md "wikilink")

唱片收录的音乐总长90分鐘，曲目依序为：

<table>
<thead>
<tr class="header">
<th><p>国家/地区</p></th>
<th><p>词曲/性质</p></th>
<th><p>曲目</p></th>
<th><p>演奏/指挥/录制</p></th>
<th><p>时长</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p><a href="../Page/约翰·塞巴斯蒂安·巴赫.md" title="wikilink">约翰·塞巴斯蒂安·巴赫</a>（曲）</p></td>
<td><p>《F大調第二<a href="../Page/布蘭登堡協奏曲.md" title="wikilink">布蘭登堡協奏曲</a>》第一樂章</p></td>
<td><p>慕尼黑巴赫管弦樂團（演奏） <a href="../Page/卡爾·李希特.md" title="wikilink">卡爾·李希特</a>（指揮）</p></td>
<td><p>4:44</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/爪哇.md" title="wikilink">爪哇</a></p></td>
<td><p><a href="../Page/甘美朗.md" title="wikilink">甘美朗曲目</a></p></td>
<td><p>Ketawang: Puspåwårnå（《花的種類》）</p></td>
<td><p>Pura Paku Alaman（演奏） K.R.T. Wasitodipuro（指导）</p>
<p>羅伯特·布朗（录制）</p></td>
<td><p>4:47</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>敲击乐</p></td>
<td><p>Cengunmé</p></td>
<td><p>查理士·杜代爾（录制）</p></td>
<td><p>2:11</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/伊图利.md" title="wikilink">伊图利森林</a><a href="../Page/姆巴提人.md" title="wikilink">姆巴提人民谣</a></p></td>
<td><p>《阿丽玛之歌》（<a href="../Page/俾格米人.md" title="wikilink">俾格米女子創始曲</a>）</p></td>
<td><p>柯林·特恩布爾（录制）</p></td>
<td><p>1:01</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/澳洲原住民.md" title="wikilink">澳洲原住民谣</a></p></td>
<td><p>Barnumbirr（《星辰》）、Moikoi（《邪恶鸟》）</p></td>
<td><p>Sandra LeBrun Holmes（录制）</p></td>
<td><p>1:29</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Lorenzo Barcelata（曲） Rafael Carrión（编）</p></td>
<td><p>El Cascabel（《門鈴》）</p></td>
<td><p>Antonio Maciel Los Aguilillas</p>
<p>Mariachi México de Pepe Villa （联合演奏）</p></td>
<td><p>3:20</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/查克·贝瑞.md" title="wikilink">查克·贝瑞</a>（词曲）</p></td>
<td><p><a href="../Page/Johnny_B._Goode.md" title="wikilink">Johnny B. Goode</a></p></td>
<td><p>查克·贝瑞（奏唱）</p></td>
<td><p>2:41</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Nyaura族民谣</p></td>
<td><p>Mariuamangɨ（《男士家歌》）</p></td>
<td><p>Pranis Pandang Kumbui（联合演唱）</p>
<p>Robert MacLennan（录制）</p></td>
<td><p>1:25</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/尺八.md" title="wikilink">尺八曲</a></p></td>
<td><p><font lang=ja>鶴の巣篭もり</font>（《鶴巢》）</p></td>
<td><p>山口吾郎（演奏）</p></td>
<td><p>5:04</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>约翰·塞巴斯蒂安·巴赫（曲；第二首）</p></td>
<td><p>《<a href="../Page/无伴奏小提琴奏鸣曲与组曲#曲目#E大调第三组曲，BWV_1006.md" title="wikilink">E大調无伴奏小提琴組曲第三号</a>》之<a href="../Page/回旋曲.md" title="wikilink">回旋曲式的</a><a href="../Page/加沃特.md" title="wikilink">加沃特</a>（Gavotte en rondeaux）</p></td>
<td><p><a href="../Page/亞瑟·葛羅米歐.md" title="wikilink">亞瑟·葛羅米歐</a>（演奏）</p></td>
<td><p>2:58</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/沃尔夫冈·阿马德乌斯·莫扎特.md" title="wikilink">沃尔夫冈·阿马德乌斯·莫扎特</a></p></td>
<td><p>《<a href="../Page/魔笛.md" title="wikilink">魔笛</a>》（Die Zauberflöte）詠歎調「<a href="../Page/地獄之復仇沸騰在我心中.md" title="wikilink">地獄之復仇沸騰在我心中</a>」（第14曲）</p></td>
<td><p><a href="../Page/巴伐利亞國立歌劇院.md" title="wikilink">巴伐利亞國立歌劇院</a>（奏唱） <a href="../Page/沃爾夫岡·薩瓦利希.md" title="wikilink">沃爾夫岡·薩瓦利希</a>（指挥）</p></td>
<td><p>3:00</p></td>
</tr>
<tr class="even">
<td><p>（前身： ）</p></td>
<td><p>当地<a href="../Page/复音音乐.md" title="wikilink">复音</a><a href="../Page/无伴奏合唱.md" title="wikilink">无伴奏合唱</a></p></td>
<td><p>ჩაკრულო（Chakrulo）</p></td>
<td><p>格鲁吉亚国立民间歌舞团（演唱） Anzor Kavasdze（指导）</p>
<p>按：節錄自苏联莫斯科广播电台（今<a href="../Page/俄罗斯之声.md" title="wikilink">俄罗斯之声</a>）</p></td>
<td><p>2:21</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>-</p></td>
<td><p>《<a href="../Page/排笛.md" title="wikilink">排笛和</a><a href="../Page/鼓.md" title="wikilink">鼓</a>》</p></td>
<td><p><a href="../Page/安卡什大区.md" title="wikilink">安卡什音乐家</a>（演奏） 按：节录自<a href="../Page/利马.md" title="wikilink">利马文化之家电台</a></p></td>
<td><p>0:55</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>马蒂·布卢姆（曲） （曲）</p></td>
<td><p>忧郁<a href="../Page/蓝调.md" title="wikilink">蓝调组曲</a></p></td>
<td><p><a href="../Page/路易斯·阿姆斯特朗.md" title="wikilink">路易斯·阿姆斯特朗与热门七人乐队</a>（演奏）</p></td>
<td><p>3:06</p></td>
</tr>
<tr class="odd">
<td><p>（前身： ）</p></td>
<td><p>-</p></td>
<td><p>风笛<a href="../Page/木卡姆.md" title="wikilink">木卡姆套曲</a></p></td>
<td><p>（演奏） 按：节录自苏联莫斯科广播电台</p></td>
<td><p>2:35</p></td>
</tr>
<tr class="even">
<td><p>//</p></td>
<td><p><a href="../Page/伊戈尔·费奥多罗维奇·斯特拉文斯基.md" title="wikilink">伊戈尔·费奥多罗维奇·斯特拉文斯基</a></p></td>
<td><p>《<a href="../Page/春之祭.md" title="wikilink">春之祭</a>》第二部分：献祭，少女的献祭舞（尾声）</p></td>
<td><p>（演奏） 按：作曲家指挥</p></td>
<td><p>4:38</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>约翰·塞巴斯蒂安·巴赫（曲；第三首）</p></td>
<td><p>《<a href="../Page/平均律鋼琴曲集.md" title="wikilink">平均律鋼琴曲集</a>》第二卷第一號，C大調前奏曲及賦格，BWV870</p></td>
<td><p><a href="../Page/格連·古爾德.md" title="wikilink">格連·古爾德</a>（钢琴）</p></td>
<td><p>4:51</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/路德维希·范·贝多芬.md" title="wikilink">路德维希·范·贝多芬</a>（曲）</p></td>
<td><p>《C小调<a href="../Page/第5号交响曲_(贝多芬).md" title="wikilink">第五交響曲</a>》第一乐章，有活力的快板</p></td>
<td><p><a href="../Page/愛樂管弦樂團.md" title="wikilink">愛樂管弦樂團</a>（演奏） <a href="../Page/奧托·克倫佩勒.md" title="wikilink">奧托·克倫佩勒</a>（指揮）</p></td>
<td><p>4:38</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>民谣</p></td>
<td><p>（Izlel je Delyo Hajdutin）</p></td>
<td><p>（演唱）</p></td>
<td><p>5:04</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>原住民谣</p></td>
<td><p><a href="../Page/纳瓦霍人.md" title="wikilink">纳瓦霍人夜曲</a>、跳神曲</p></td>
<td><p>Ambrose Roan Horse, Chester Roan, and Tom Roan（奏唱） 洛狄斯（錄製）</p></td>
<td><p>1:01</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>《<a href="../Page/帕凡舞.md" title="wikilink">帕凡舞</a>、、德國土風舞（Almain）及其他短曲調》之仙女圍繞</p></td>
<td><p>（演奏） （指导）</p></td>
<td><p>1:19</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/排笛.md" title="wikilink">排笛曲</a></p></td>
<td><p>Naranaratana Kookokoo（《冢雉啼》）</p></td>
<td><p>Maniasinimae and Taumaetarau Chieftain Tribe of Oloha and Palasu'u Village Community（演奏） 按：節錄自</p></td>
<td><p>1:15</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>民谣</p></td>
<td><p>结婚曲</p></td>
<td><p><a href="../Page/万卡韦利卡.md" title="wikilink">万卡韦利卡某少女演唱</a> 高約翰（录制）</p></td>
<td><p>0:42</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/古琴.md" title="wikilink">古琴曲</a></p></td>
<td><p>《<a href="../Page/流水_(古琴曲).md" title="wikilink">流水</a>》（《<a href="../Page/天聞閣琴譜.md" title="wikilink">天聞閣琴譜</a>》打谱版）</p></td>
<td><p><a href="../Page/管平湖.md" title="wikilink">管平湖</a>（演奏）</p></td>
<td><p>7:36</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>，जात कहां हो（Jaat Kahan Ho，《你要去哪里》）</p></td>
<td></td>
<td><p>3:34</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>（《夜黑地冷》）</p></td>
<td><p>盲眼威利（弹唱）</p></td>
<td><p>3:32</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/路德维希·范·贝多芬.md" title="wikilink">路德维希·范·贝多芬</a>（曲；第二首）</p></td>
<td><p>《降B大調<a href="../Page/弦樂四重奏.md" title="wikilink">弦樂四重奏第</a>13号》Op. 130，第五乐章：短抒情调，极富感情的慢板（Cavatina. Adagio molto espressivo）</p></td>
<td><p>（演奏）</p></td>
<td><p>6:41</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 旅程

[旅行者1号於](../Page/旅行者1号.md "wikilink")1977年發射，於1990年經過[冥王星的軌道](../Page/冥王星.md "wikilink")，最終於2004年11月離開[太陽系](../Page/太陽系.md "wikilink")（以離開[衝擊邊界為準](../Page/衝擊邊界.md "wikilink")）。大約40000年後，它和它的姊妹船[旅行者2号將會到達離太陽系](../Page/旅行者2号.md "wikilink")1.7光年的地方：旅行者1號會遇上位於[小熊座星群的](../Page/小熊座.md "wikilink")[AC+79
3888](../Page/AC+79_3888.md "wikilink")；而旅行者2號將會靠近位於[仙女座星群的](../Page/仙女座.md "wikilink")[羅斯248](../Page/羅斯248.md "wikilink")。

兩艘旅行者雖然設計一樣，但其航行軌跡卻不一樣。在2005年5月，旅行者1號將會在距離太陽870億英哩的地方，以每年3.6個[天文單位的速度前進](../Page/天文單位.md "wikilink")。而旅行者2號將會在距離太陽650億英哩的地方，以每年3.3個天文單位的速度前進。

薩根博士：「要是宇宙裡有先進的太空文明，探測器才將會被遇上及被播放。但是發射這個“瓶子”到宇宙“海洋”裡是訴說著這顆星球上生命的願望。」

其他人則指探測器唯一會遇到的文明就只是我們，或許在若干年後我們會再次尋獲它，並放進太空博物館裡。

## 其他資料

大部份在唱片裡的影像（以黑白製作）連同其編輯的資料，可以在1978年出版由[卡爾·薩根](../Page/卡爾·薩根.md "wikilink")、[法蘭克·德雷克](../Page/法蘭克·德雷克.md "wikilink")、[安·德魯揚](../Page/安·德魯揚.md "wikilink")、提摩西·費瑞斯，喬恩·龍伯及蓮達·沙爾士文撰寫的《地球的呢喃：旅行者號星際唱片》裡找到。\[3\]而[CD-ROM版本則由](../Page/CD-ROM.md "wikilink")[華納新媒體於](../Page/時代華納.md "wikilink")1992年發行。\[4\]兩個版本都已經停止翻印，但1978年版仍可在不少學院或公共圖書館裡可以找到。

在1983年7月，[英國廣播公司](../Page/英國廣播公司.md "wikilink")[BBC Radio
4頻道裡播放了一段](../Page/BBC_Radio_4.md "wikilink")45分鐘的紀錄聲帶《來自小星球的音樂》，當中由薩根和德魯揚夫婦闡述了為唱片選擇音樂的過程，並為這段節錄作簡介。但這段紀錄聲帶究竟是英國廣播公司的原創紀錄聲帶，還是從美國[全國公共廣播電台引入](../Page/全國公共廣播電台.md "wikilink")，則不得而知。

在這張金唱片裡「地球之聲」的一部份聲音記錄當中，亦包括了一段以[摩斯電碼表達](../Page/摩斯電碼.md "wikilink")，令人鼓舞的字句：*ad
astra per aspera*。[拉丁語的意思為](../Page/拉丁語.md "wikilink")「通往星空之路困難叢生」。

## 虛構出現

  - 在動作[電影](../Page/電影.md "wikilink")《外星戀》裡描繪的旅行者金唱片被外星的高智慧生物尋獲，他們並且同樣地寄出他們的唱片以調查於地球上的生物。（但他們把當中的《[Johnny
    B.
    Goode](../Page/Johnny_B._Goode.md "wikilink")》換成了[滾石樂隊的](../Page/滾石樂隊.md "wikilink")《[(I
    Can't Get No)
    Satisfaction](../Page/\(I_Can't_Get_No\)_Satisfaction.md "wikilink")》（无法满足）
  - 旅行者探測器和金唱片出現於[動畫](../Page/動畫.md "wikilink")《[飞出個未來](../Page/飞出個未來.md "wikilink")》其中一集「Parasites
    Lost」（寄生生物的消失）之中。
    正當[莉娜停在銀河系貨車站時](../Page/莉娜.md "wikilink")，把探測器從她船上的擋風玻璃前撥去。
  - 在《變形金剛》系列中的《[Beast
    War](../Page/Beast_War.md "wikilink")》裡，其中一个[原始獸所偷去的](../Page/原始獸.md "wikilink")「金盘」正是旅行者的金唱片。這張金盘被視為變形金剛間的競賽獎品，因為只有碟片內藏有地球的位置，亦即獲得能量晶體的來源。同時，這張碟片也藏有一段[威震天的秘密訊息](../Page/威震天.md "wikilink")。
  - 在[美國](../Page/美國.md "wikilink")[NBC長壽節目](../Page/全國廣播公司.md "wikilink")《[週六夜現場](../Page/週六夜現場.md "wikilink")》的一個環節中，由[史提夫·馬丁宣佈已經接收到外星人傳回來的訊息](../Page/史提夫·馬丁.md "wikilink")。經過解讀後，那段訊息是：「請多傳一點[查克·貝里](../Page/查克·貝里.md "wikilink")」
  - 金唱片的封面一部份曾於電影《[星艦迷航記](../Page/星艦迷航記.md "wikilink")》中出現，是[V'ger的一部份](../Page/V'ger.md "wikilink")。但旅行者金唱片顯然地並不是放在電影中虛構的「旅行者6號」探測器上。
  - 在電視[連續劇](../Page/連續劇.md "wikilink")《[X檔案](../Page/X檔案.md "wikilink")》的其中一集「小綠人」裡，可以聽到一段旅行者金唱片的簡短節錄。
  - 在[羅恩·賀伯特的小說](../Page/羅恩·賀伯特.md "wikilink")《[地球戰場](../Page/地球戰場_\(小說\).md "wikilink")》裡，旅行者金唱片的發現帶領邪惡的外星人塞庫洛到地球來。
  - 在動畫《[交響詩篇](../Page/交響詩篇.md "wikilink")》第31集裡，[格雷格·伊根博士在他的實驗室裡展示了金唱片的複製版](../Page/格雷格·伊根.md "wikilink")。
  - 在電視連續劇《[白宮群英](../Page/白宮群英.md "wikilink")》的其中一集「[成吉思汗的戰役](../Page/成吉思汗.md "wikilink")」裡，[喬希·萊曼由](../Page/喬希·萊曼.md "wikilink")[盲眼威利提供的參考中提及到金唱片](../Page/盲眼威利.md "wikilink")（雖沒有提及過名字）。
  - 在动画《[黑白小姐](../Page/黑白小姐.md "wikilink")》第 6
    集出现，场景背景为旅行者探测器，台词为“宇宙规模的金唱片出道”。
  - 在遊戲《[Stellaris](../Page/群星_\(游戏\).md "wikilink")》中，有可能觸發事件，顯示發現一個金唱片。在完成研究計劃後，玩家可以得到太陽系的坐標和一些獎勵。

## 參考

  - 此條目是以[美國太空總署網站](http://voyager.jpl.nasa.gov/spacecraft/goldenrec.html)放置於公有領域上的文字為基礎，網站中可找到唱片內的圖片及聲音。
  - [噴氣推進實驗室旅行者號常問問題集](https://web.archive.org/web/20110721050617/http://voyager.jpl.nasa.gov/faq.html)（英語）

<div style="font-size:small">

<references/>

</div>

## 參閱

  - [旅行者1号](../Page/旅行者1号.md "wikilink")
  - [旅行者2号](../Page/旅行者2号.md "wikilink")
  - [時間囊](../Page/時間囊.md "wikilink")
  - [阿雷西博信息](../Page/阿雷西博信息.md "wikilink") -
    由[阿雷西博射電望遠鏡向外星人發出的訊息](../Page/阿雷西博射電望遠鏡.md "wikilink")，以告知他們地球的存在。
  - [先驅者鍍金鋁板](../Page/先驅者鍍金鋁板.md "wikilink") -
    旅行者金唱片的始祖，條目有較詳細闡述鍍金鋁板上的訊息。
  - [wow訊號](../Page/wow訊號.md "wikilink")

## 外部連結

  - [美國太空總署噴氣推進實驗室 -
    旅行者號星際記錄資訊](http://voyager.jpl.nasa.gov/spacecraft/goldenrec.html)
  - [金唱片上所有圖像、語言及聲音](http://re-lab.net/welcome/)
  - [CED
    Magic網站有關金唱片的資料](http://www.cedmagic.com/featured/voyager/voyager-record.html)
  - [愛阿華大學網站有關衝擊邊界](http://www-pw.physics.uiowa.edu/space-audio/voyager/termination-shock/)

[V](../Category/時間囊.md "wikilink") [V](../Category/旅行者系列.md "wikilink")
[Category:美国航空航天局项目](../Category/美国航空航天局项目.md "wikilink")

1.
2.  [喬恩·隆伯格](../Page/喬恩·隆伯格.md "wikilink"): "Pictures of Earth". in Carl
    Sagan: *Murmurs of Earth,* 1978, New York, ISBN 0-679-74444-4
3.  卡爾·薩根等(1978)《Murmurs of Earth: The Voyager Interstellar
    Record》（地球的呢喃：旅行者號星際唱片）紐約：Random House. ISBN
    0394410475（硬裝版）ISBN 0345283961（軟裝版）
4.  卡爾·薩根等(1992)《Murmurs of Earth : The Voyager Interstellar Record
    (computer file)》（地球的呢喃：旅行者號星際唱片（電腦檔案））柏班克：華納新媒體