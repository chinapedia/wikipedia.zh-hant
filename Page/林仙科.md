**林仙科**(Winteraceae)也叫[辛辣木科](../Page/辛辣木科.md "wikilink")、[冬木科](../Page/冬木科.md "wikilink")、[假八角科或直接音译为](../Page/假八角科.md "wikilink")[温特木科](../Page/温特木科.md "wikilink")，共有9[属约](../Page/属.md "wikilink")120[种](../Page/种.md "wikilink")，分布在南半球[热带至](../Page/热带.md "wikilink")[温带的广大区域](../Page/温带.md "wikilink")，包括[澳洲](../Page/澳洲.md "wikilink")、[南美洲](../Page/南美洲.md "wikilink")、[马达加斯加岛和](../Page/马达加斯加岛.md "wikilink")[马来西亚等地](../Page/马来西亚.md "wikilink")。

本科[植物都是](../Page/植物.md "wikilink")[乔木或](../Page/乔木.md "wikilink")[灌木](../Page/灌木.md "wikilink")，许多品种可以提炼香精油。单[叶互生](../Page/叶.md "wikilink")，全缘，叶背常灰白色，有细小的透明腺点，无托叶；[花单生](../Page/花.md "wikilink")，或成顶生或腋生的聚伞花序；[果实为](../Page/果实.md "wikilink")[浆果或](../Page/浆果.md "wikilink")[蓇葖果](../Page/蓇葖果.md "wikilink")，有时聚合成[蒴果或](../Page/蒴果.md "wikilink")[聚花果](../Page/聚花果.md "wikilink")。

生长在[阿根廷和](../Page/阿根廷.md "wikilink")[智利的冬木](../Page/智利.md "wikilink")（*Drimys
winteri*）是一种很好的观赏[树木](../Page/树.md "wikilink")，花有[茉莉香味](../Page/茉莉.md "wikilink")，假八角（*Tasmannia
lanceolata*）可以做调味用。

本[科植物属于比较原始的](../Page/科.md "wikilink")[被子植物](../Page/被子植物门.md "wikilink")，1981年的[克朗奎斯特分类法将其编入](../Page/克朗奎斯特分类法.md "wikilink")[木兰目](../Page/木兰目.md "wikilink")，1998年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
分类法认为无法列入任何一](../Page/APG_分类法.md "wikilink")[目](../Page/目.md "wikilink")，直接放到[被子植物分支之下](../Page/被子植物分支.md "wikilink")，2003年经过修订的[APG
II
分类法将其列入新设立的](../Page/APG_II_分类法.md "wikilink")[白樟目](../Page/白樟目.md "wikilink")。

## 参考文献

  - Feild, Taylor S., Brodribb, Tim, Holbrook, N. Michele.（2002）. Hardly
    a Relict: Freezing and the Evolution of Vesselless Wood in
    Winteraceae. [*Evolution* 2002 56:
    464-478](http://www.blackwell-synergy.com/doi/pdf/10.1111/j.0014-3820.2002.tb01359.x)。

[\*](../Category/林仙科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")