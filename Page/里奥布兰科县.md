**里奧布蘭科县** (**Rio Blanco County,
Colorado**)是[美國](../Page/美國.md "wikilink")[科羅拉多州西北部的一個縣](../Page/科羅拉多州.md "wikilink")，西鄰[猶他州](../Page/猶他州.md "wikilink")。面積8,347平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口5,986人。縣治[米克](../Page/米克_\(科羅拉多州\).md "wikilink")
(Meeker)。

成立於1889年3月25日。縣名是[懷特河的](../Page/懷特河_\(猶他州\).md "wikilink")[西班牙語叫法](../Page/西班牙語.md "wikilink")。\[1\]\[2\]

## 参考文献

<div class="references-small">

<references />

</div>

[R](../Category/科罗拉多州行政区划.md "wikilink")

1.
2.  <http://www.colorado.gov/dpa/doit/archives/arcgov.html>