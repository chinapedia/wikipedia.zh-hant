**丹佛國際機場**（，），是一座位於[美國](../Page/美國.md "wikilink")[科羅拉多州](../Page/科羅拉多州.md "wikilink")[丹佛市的民用機場](../Page/丹佛市.md "wikilink")，並且是美國总占地面積最大及全世界第三大機場，僅次於[法赫德国王国际机场和](../Page/法赫德国王国际机场.md "wikilink")[蒙特利尔－米拉贝勒国际机场](../Page/蒙特利尔－米拉贝勒国际机场.md "wikilink")\[1\]，跑道16R/34L是全美國最長的公共跑道。
2014年，以乘客流量計算，丹佛國際機場是全美第六大機場、[世界第十八大機場](../Page/全球机场客运吞吐量列表.md "wikilink")（超过5300万人次）。\[2\]
它也拥有第三大的区域航线网络。 這個機場位於[丹佛市的東北面](../Page/丹佛市.md "wikilink")，由丹佛市政府營運。

## 特色

[Denver_International_Airport_terminal.jpg](https://zh.wikipedia.org/wiki/File:Denver_International_Airport_terminal.jpg "fig:Denver_International_Airport_terminal.jpg")
此機場的特別之處在於屋頂用特殊布料覆蓋及採用[張力結構的設計](../Page/張力結構.md "wikilink")，令人聯想到冬天受冰雪覆蓋的[落磯山脈](../Page/落磯山脈.md "wikilink")。另外一樣富有盛名的是，連接各航廈和A大堂的行人天橋，能令旅客看著飛機掠過。此機場為[邊疆航空的主要樞紐](../Page/邊疆航空.md "wikilink")、[聯合航空和其子公司](../Page/聯合航空.md "wikilink")[泰德航空航空的第二樞紐和](../Page/泰德航空.md "wikilink")[大湖航空的重要樞紐](../Page/大湖航空.md "wikilink")。近來美國[西南航空在丹佛機場的營運量日與據增](../Page/西南航空.md "wikilink")。

丹佛機場內有[Wi-Fi無線網路的設施](../Page/Wi-Fi.md "wikilink")，這是由美國最大的電信公司[AT\&T](../Page/AT&T.md "wikilink")\[3\]提供的。在[聯合航空](../Page/聯合航空.md "wikilink")、[美國航空和](../Page/美國航空.md "wikilink")[達美航空的貴賓室內](../Page/達美航空.md "wikilink")，另外有[T-Mobile的](../Page/T-Mobile.md "wikilink")[無線區域網路的設施](../Page/無線區域網路.md "wikilink")\[4\]。

## 地理位置

丹佛機場和[丹佛市的距離有](../Page/丹佛市.md "wikilink")25英里（40公里），比被其取代的斯特普尔顿國際機場和丹佛市之間的距離遠19英里（31公里）。这一选点是為了防止給已開發的區域带来噪音，容納更多不受暴風雪影響的跑道，和方便未來擴建。機場占地54平方英里（34,524英畝；140平方公里），接近紐約[曼哈頓島面积的兩倍](../Page/曼哈頓.md "wikilink")，比[舊金山稍大](../Page/舊金山.md "wikilink")。

## 航廈／航點

丹佛國際機場有三個大廳，經由步行天橋可直接從航廈抵達A大廳，另有地下火車系統可抵達三大廳。旅客進入B、C大廳必須搭乘地下火車系統。因為機場除了火車系統外並沒有其他搭乘系統（例如沒有地下步行隧道），萬一火車系統無法使用時，將對旅客帶來不便。

[聯合航空掌有丹佛國際機場](../Page/聯合航空.md "wikilink")57%的航班。

丹佛機場為了要抵銷機場建設的費用，向航空公司收取頗高的航班費用，因此機場成了[聯合航空和](../Page/聯合航空.md "wikilink")[大陸航空的營運樞紐](../Page/大陸航空.md "wikilink")。[大陸航空在](../Page/大陸航空.md "wikilink")90年代機場即將啟用之前，宣告破產，放棄此營運樞紐。雖然收取航班費用有礙於與[科羅拉多泉機場的競爭](../Page/科羅拉多泉機場.md "wikilink")，但因[西太平航航空和](../Page/西太平航航空.md "wikilink")[西南航空撤出丹佛機場的決定落空](../Page/西南航空.md "wikilink")，使得丹佛機場成為當地區域唯一的國際機場。

2006年12月14日，[丹佛日報報導丹佛機場正在擴建C大廳](../Page/丹佛日報.md "wikilink")，此為其「首次的航廈擴建」，在C大廳的東端增設至少8個登機門，耗資大約1億6千萬美元，預計至少三年才能完工。完工之後，[西南航空以及其他航空公司便能大量增設航班](../Page/西南航空.md "wikilink")，丹佛機場成為美國發展快速的機場的其中之一。

丹佛機場最近也有擴建B大廳的計劃，預計在其東側增設一個區域性小型客機的大廳，此大廳包含兩小廳，以天橋連接B大廳\[5\]。增設的登機門將便於空橋直接連接區域性小型客機。此工程完工後，[聯合航空將全面撤出A大廳](../Page/聯合航空.md "wikilink")，轉移到B大廳營運。

丹佛機場最近也宣布機場環境改善的計畫。根據2006年12月14日[丹佛日報和](../Page/丹佛日報.md "wikilink")[落磯山日報的報導](../Page/落磯山日報.md "wikilink")，此計畫的草案預計先往南擴建主航廈，可容納更多售票窗口和[FasTracks的](../Page/FasTracks.md "wikilink")[通勤火車終點站](../Page/區域鐵路.md "wikilink")（起點為[丹佛市的](../Page/丹佛市.md "wikilink")[聯合車站](../Page/聯合車站.md "wikilink")）。

丹佛機場最近與[邊疆航空商討A大廳的區域性小型客機的航廈建設計畫](../Page/邊疆航空.md "wikilink")。由於丹佛機場的資金來源為航空公司，所有建設計畫的資金以契約制定。

### 傑柏遜航廈

[Denver_airport_USGA_2002_mod.jpg](https://zh.wikipedia.org/wiki/File:Denver_airport_USGA_2002_mod.jpg "fig:Denver_airport_USGA_2002_mod.jpg")
[Denver_International_Airport,_Concourse_B_-_2.jpg](https://zh.wikipedia.org/wiki/File:Denver_International_Airport,_Concourse_B_-_2.jpg "fig:Denver_International_Airport,_Concourse_B_-_2.jpg")

#### A大廳

A大堂有37個登机門，編號A24─A68

  - [加拿大航空](../Page/加拿大航空.md "wikilink")（登机門A41、A43）─[蒙特利爾](../Page/蒙特婁特魯多國際機場.md "wikilink")、[多倫多-皮爾遜](../Page/多倫多皮爾遜國際機場.md "wikilink")
  - [英國航空](../Page/英國航空.md "wikilink")（登机門A37）─[倫敦-希斯路](../Page/倫敦希斯路機場.md "wikilink")
  - [冠軍航空](../Page/冠軍航空.md "wikilink")（登机門A50）─坎昆、[拉斯維加斯](../Page/麥卡倫國際機場.md "wikilink")
  - [大陸航空](../Page/大陸航空.md "wikilink")（登机門A45、A47、A49）─克利夫蘭、[休斯敦-洲際](../Page/喬治布希洲際機場.md "wikilink")、[紐華克](../Page/紐華克自由國際機場.md "wikilink")
  - [邊疆航空](../Page/邊疆航空.md "wikilink")（登机門A24─A36、A38─A40、
    A42、A44、A46、A50─A53）─阿卡普爾科\[季節性\]，阿克倫／坎頓、[阿爾伯克基](../Page/阿爾伯克基國際太陽機場.md "wikilink")、[安克雷奇](../Page/泰德·史蒂文斯安克雷奇國際機場.md "wikilink")\[季節性\]、[亞特蘭大](../Page/亚特兰大哈兹菲尔德-杰克逊国际机场.md "wikilink")、奧斯汀、博伊西、坎昆、芝加哥-中途、科蘇梅爾、達拉斯／沃斯堡、德頓、[底特律](../Page/底特律都会韦恩县机场.md "wikilink")、勞德岱堡、休士頓-洲際、印第安納波利斯、堪薩斯城、[拉斯維加斯](../Page/麥卡倫國際機場.md "wikilink")、利比里亞
    （哥斯達黎加）、[洛杉磯](../Page/洛杉磯國際機場.md "wikilink")、馬薩特蘭、密爾沃基、明尼阿波利斯／聖保羅、納什維爾、[紐約-拉瓜地亞](../Page/拉瓜地亞機場.md "wikilink")、奧克拉荷馬市、奧馬哈、奧蘭多、[費城](../Page/費城國際機場.md "wikilink")、[鳳凰城](../Page/鳳凰城天港國際機場.md "wikilink")、波特蘭（奧勒岡州）、巴亞爾塔港、[沙加緬度](../Page/沙加緬度國際機場.md "wikilink")、鹽湖城、聖安東尼奧、[聖地牙哥](../Page/聖地牙哥國際機場.md "wikilink")、[舊金山](../Page/舊金山國際機場.md "wikilink")、[聖荷西（加州）](../Page/聖荷西國際機場.md "wikilink")、聖荷西
    （哥斯達黎加）\[季節性\]、聖塔安那／橙縣、斯波肯、[聖路易](../Page/聖路易蘭伯特國際機場.md "wikilink")、[西雅圖／塔科馬](../Page/西雅圖-塔科馬國際機場.md "wikilink")、坦帕、圖森、[溫哥華](../Page/溫哥華國際機場.md "wikilink")\[季節性\]、華盛頓-雷根
      - 邊疆航空（由[Lynx
        Aviation运营](../Page/Lynx_Aviation.md "wikilink")）─亞斯本、博伊西、波茲曼、[科羅拉多泉](../Page/科羅拉多泉機場.md "wikilink")、杜蘭戈、埃爾帕索、法戈、大章克興、海頓／蒸氣船溫泉\[季節性\]、傑克遜霍爾、俄克拉荷馬城、拉皮德城、威奇托
  - [捷藍航空](../Page/捷藍航空.md "wikilink")（登机門A35）─[波士頓](../Page/波士頓羅根國際機場.md "wikilink")、[紐約-甘迺迪](../Page/甘迺迪國際機場.md "wikilink")
  - [德國漢莎航空](../Page/德國漢莎航空.md "wikilink")（登机門A41、A43）─[法蘭克福](../Page/法蘭克福國際機場.md "wikilink")
  - [墨西哥航空](../Page/墨西哥航空.md "wikilink")（登机門A39）─墨西哥城
  - [墨西哥國際航空](../Page/墨西哥國際航空.md "wikilink")（登机門待定）─墨西哥城\[2009年3月開航\]\[6\]
  - [中西航空](../Page/中西航空.md "wikilink")
      - 中西航空聯運（由[共和航空經營](../Page/共和航空.md "wikilink")，登机門A48）─密爾瓦基
  - [大湖航空](../Page/大湖航空.md "wikilink")（登機門A57、A59、A61、A63）─阿拉莫薩、亞歷恩斯、查德隆、夏延、科爾特斯、狄金森、道奇城、法明頓、花園市、吉列、海斯、科爾尼、拉勒米、利博羅、莫阿布、北普拉特、佩吉、皮埃爾、普韋布洛、里弗敦、石頭泉、沙林納、斯科茨布拉夫、雪萊頓、特柳賴德、韋納爾

#### B大廳

[5014-eaglerock-side.jpg](https://zh.wikipedia.org/wiki/File:5014-eaglerock-side.jpg "fig:5014-eaglerock-side.jpg")

B大廳有83個登機門：編號B15─B39、B41─B99

  - [聯合航空](../Page/聯合航空.md "wikilink")（登機門B17、B19、B21、B23─B39、B43─B53、B55、B57）─阿伯克基、亞特蘭大、奧斯汀、巴爾的摩／華盛頓、比林斯、博伊西、波士頓、波茲曼\[季節性\]、[卡爾加里](../Page/卡爾加里國際機場.md "wikilink")、坎昆、芝加哥-歐海爾、哥倫布、達拉斯／沃斯堡、德梅因、底特律、老鷹鎮／韋爾\[季節\]、拉皮德城\[季節性\]、海登／蒸汽船溫泉\[季節性\]、[檀香山](../Page/檀香山國際機場.md "wikilink")、休斯敦-洲際、伊斯塔帕／芝華塔尼歐、傑克遜霍爾\[季節性\]
    、卡胡盧伊\[季節性\] 、堪薩斯城、科納\[季節性\] 、拉斯維加斯、利胡埃、倫敦-希斯路\[季節性\]
    、洛杉磯、洛斯卡波斯、邁阿密、明尼阿波利斯／聖保羅、新奧爾良、紐約-拉瓜地亞、紐華克、奧克蘭、俄克拉荷馬市、奧馬哈、[安大略（加州）](../Page/安大略国际机场.md "wikilink")、奧蘭多、棕櫚泉\[季節性\]、費城、鳳凰城、波特蘭（奧勒岡州）、巴亞爾塔港、[雷諾-太浩湖](../Page/雷諾-太浩国际机场.md "wikilink")、沙加緬度、鹽湖城、聖安東尼奧、聖地牙哥、舊金山、聖荷西（加州）、聖安娜／橙縣、[西雅圖-塔科馬](../Page/西雅圖-塔科馬國際機場.md "wikilink")、蘇族瀑布市、斯波肯、聖路易、坦帕、多倫多-皮爾遜、圖森、塔爾薩、溫哥華、[華\(\)盛頓-杜勒斯](../Page/華盛頓杜勒斯國際機場.md "wikilink")、華盛頓-雷根、威奇托、東京-成田（從2013年3月31日開始）
      - [聯合航空快捷](../Page/聯合航空快捷.md "wikilink")（登機門B54、B56、B58─B99）
          - [聯合航空快捷](../Page/聯合航空快捷.md "wikilink")（由[GoJet
            Airlines經營](../Page/GoJet_Airlines.md "wikilink")）─聖安東尼奧、聖路易、杜沙
          - [聯合航空快捷](../Page/聯合航空快捷.md "wikilink")（由[梅薩航空經營](../Page/梅薩航空.md "wikilink")）─亞斯本、亞特蘭大、奧斯汀、卡斯珀、雪松瀑布／愛荷華市、科迪、科羅拉多泉、杜蘭戈、老鷹鎮／韋爾、法戈、吉列、大章克興、甘尼森、傑克遜霍爾、納什維爾、拉皮德城、石頭泉、蘇族瀑布市
      - [聯合航空快捷](../Page/聯合航空快捷.md "wikilink")（由Shuttle
        America經營）─亞特蘭大、奧斯汀、哥倫布、達拉斯／沃斯堡、休斯頓-洲際、印第安納波利斯、明尼阿波利斯／聖保羅、匹茲堡、多倫多-皮爾遜
      - [聯合航空快捷](../Page/聯合航空快捷.md "wikilink")（由[天西航空經營](../Page/天西航空.md "wikilink")）─阿伯克基、阿普爾頓、亞斯本、奧斯汀、貝克斯菲爾德、班頓維爾／法葉維爾、[伯明罕（阿拉巴馬州）](../Page/伯明罕國際機場_\(美國\).md "wikilink")、比林斯、俾斯麥、博伊西、波茲曼、伯班克、卡爾加里、[辛辛那堤／北肯塔基](../Page/辛辛那堤／北肯塔基國際機場.md "wikilink")、克利夫蘭、科迪、科羅拉多泉、哥倫布、達拉斯／沃斯堡、德梅因、底特律、杜蘭戈、埃德蒙頓、埃爾帕索、尤金、法戈、弗雷斯諾、大章克興、大激流市，大瀑布市、海登／蒸汽船溫泉、[赫勒拿](../Page/海伦娜区域机场.md "wikilink")、[休斯敦-洲際](../Page/喬治·布什洲際機場.md "wikilink")、亨茨維爾、愛達荷瀑布市、印第安納波利斯\[季節性\]、卡里斯貝爾、諾克斯維爾、林肯、麥迪遜、梅德福、孟斐斯、密爾沃基、明尼阿波利斯／聖保羅、米蘇拉、莫林／闊德城、蒙特雷／卡梅爾、蒙特羅斯、俄克拉荷馬城、棕櫚泉、帕斯科，皮奧里亞、匹茲堡、拉皮德城、雷蒙德／本德、[鹽湖城](../Page/盐湖城国际机场.md "wikilink")、聖安東尼奧、聖塔芭芭拉、薩斯卡頓、斯普林菲爾德、特拉弗斯城\[季節性\]、圖森、塔爾薩、威奇塔、溫尼伯
      - [聯合航空快捷](../Page/聯合航空快捷.md "wikilink")（由 Trans States
        Airlines經營）─聖路易、小岩城、莫林／闊德城

#### C大廳

C大廳有22個登機門，編號C28─C50

  - [穿越航空](../Page/穿越航空.md "wikilink")（登機門C46）─亞特蘭大
  - [阿拉斯加航空](../Page/阿拉斯加航空.md "wikilink")（登機門C32）─安克雷奇\[季節性\]、波特蘭（奧勒岡州）、西雅圖／塔科馬
      - [地平線航空](../Page/地平線航空.md "wikilink")─波特蘭（奧勒岡州）
  - [美國航空](../Page/美國航空.md "wikilink")（登機門C35， C37，
    C39）─芝加哥-奧海爾、達拉斯／沃斯堡、洛杉磯、邁阿密
      - [美國航空聯運](../Page/美國航空聯運.md "wikilink")（由經營）─聖路易
  - [大天航空](../Page/大天航空.md "wikilink")（登機門C31）─雪萊頓
  - [達美航空](../Page/達美航空.md "wikilink")（登機門C40、C42、C44）─亞特蘭大、辛辛那堤／北肯塔基、紐約-甘迺迪，鹽湖城
      - [達美航空聯運](../Page/達美航空聯運.md "wikilink")（由[康航經營](../Page/康航.md "wikilink")）─辛辛那提／北肯塔基
      - [達美航空聯運](../Page/達美航空聯運.md "wikilink")（由[ExpressJet航空經營](../Page/ExpressJet航空.md "wikilink")）─洛杉磯
      - [達美航空聯運](../Page/達美航空聯運.md "wikilink")（由[天西航空經營](../Page/天西航空.md "wikilink")）─鹽湖城
  - [西北航空](../Page/西北航空.md "wikilink")（登機門C34、C36、C38）─底特律、印第安納波利斯、孟斐斯、明尼阿波利斯／聖保羅
  - [西南航空](../Page/西南航空.md "wikilink")（登機門C41、C43、C45、C47、C49）─亞伯科基、阿馬里洛、奧斯汀、巴爾的摩／華盛頓、芝加哥-中途、休士頓-哈比、堪薩斯城、拉斯維加斯、納什維爾、奧克蘭、奧克拉荷馬市、奧蘭多、鳳凰城、鹽湖城、聖地牙哥、西雅圖／塔科馬、坦帕
  - [太陽國航空公司](../Page/太陽國航空公司.md "wikilink")（登機門C40）─明尼阿波利斯／聖保羅
  - [全美航空](../Page/全美航空.md "wikilink")（登機門C28─C30）─夏洛特、費城、鳳凰城
      - [全美航空快運](../Page/全美航空快運.md "wikilink")（由[梅薩航空經營](../Page/梅薩航空.md "wikilink")）─鳳凰城

#### D、E大廳

丹佛國際機場已預先在C大廳旁保留兩棟新大廳未來的建設用地。建設D大廳可不必移動任何現存的結構，但地下火車系統必須延伸，建設E大廳則須移動[大陸航空的](../Page/大陸航空.md "wikilink")[飛機棚](../Page/飛機棚.md "wikilink")。然而，在D、E大廳建設開始之前，A、B、C大廳仍可往東西側延伸，各大廳可容納80個登機門，由此可知為何各大廳的中心附近的登機門以中間數40先編號，所以，在地下火車系統內，西側標示為1到40號登機門，東側為41到80號登機門。

## 參考

## 外部連結

  - [丹佛國際機場](http://www.flydenver.com/)

  -
## 相关条目

  - [世界最高海拔机场列表](../Page/世界最高海拔机场列表.md "wikilink")

[D](../Category/美国机場.md "wikilink") [D](../Category/丹佛.md "wikilink")
[Category:1989年啟用的機場](../Category/1989年啟用的機場.md "wikilink")
[Category:1995年启用的机场](../Category/1995年启用的机场.md "wikilink")
[Category:科罗拉多州机场](../Category/科罗拉多州机场.md "wikilink")

1.  [Coventry Airport News: Largest
    Airport](http://www.flightmapping.com/news/Coventry-Airport/Biggest-busiest-airports.asp)

2.  [Denver International Airport is the 5th Busiest Airport in the
    United
    States](http://www.airliners.net/discussions/general_aviation/read.main/3296489/)
3.  [About DIA - Airport
    Services](http://www.flydenver.com/guide/index.asp)
4.  [Denver, CO - Wireless
    Hotspots](http://www.wi-fihotspotlist.com/browse/us/2000258/2046750/p1.html)

5.  <http://www.united.com/page/article/0,6722,1114,00.html>
6.