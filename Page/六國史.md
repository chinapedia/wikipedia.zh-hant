**六國史**是對[日本](../Page/日本.md "wikilink")[奈良時代](../Page/奈良時代.md "wikilink")、[平安時代所編輯的六部史書的總稱](../Page/平安時代.md "wikilink")。西元720年[舍人親王臨摹](../Page/舍人親王.md "wikilink")[中國正史文筆](../Page/中國.md "wikilink")，主編《[日本書紀](../Page/日本書紀.md "wikilink")》，成為日本正史的濫觴。西元797—901年，日本朝廷陸續編撰五部正史：《續日本紀》、《日本後紀》、《續日本後紀》、《日本文德天皇實錄》、《日本三代實錄》，加上原本的《日本書紀》，合稱「六國史」。其後屢有修纂新史之計畫，卻每逢戰亂而以未遂告終。近代又有以[考證學角度修纂全](../Page/考證學.md "wikilink")[漢文](../Page/漢文.md "wikilink")[大日本編年史的計畫](../Page/大日本編年史.md "wikilink")，不幸遭[筆禍事件而停擺](../Page/筆禍事件.md "wikilink")。今日則有「[大日本史料](../Page/大日本史料.md "wikilink")」補足六國史之後的空缺，可惜停留在[史料](../Page/史料.md "wikilink")、[史稿程度](../Page/史稿.md "wikilink")，完成度不及六國史系列。

## 概略

  - **[日本書紀](../Page/日本書紀.md "wikilink")** -
    [神代開始](../Page/日本神話.md "wikilink")、[持統天皇結束](../Page/持統天皇.md "wikilink")（？
    - 697年）凡30卷。720年（養老4年）完成。撰者[舍人親王](../Page/舍人親王.md "wikilink")。
  - **[續日本紀](../Page/續日本紀.md "wikilink")** -
    [文武天皇開始](../Page/文武天皇.md "wikilink")、[桓武天皇結束](../Page/桓武天皇.md "wikilink")（697年
    -
    791年）凡40卷。797年（延曆16年）完成。撰者菅野真道・[藤原繼繩等](../Page/藤原繼繩.md "wikilink")。
  - **[日本後紀](../Page/日本後紀.md "wikilink")** -
    [桓武天皇開始](../Page/桓武天皇.md "wikilink")、[淳和天皇結束](../Page/淳和天皇.md "wikilink")（792年
    -
    833年）凡40卷（現存10卷）。840年（承和7年）完成。撰者藤原冬嗣・[藤原緒嗣等](../Page/藤原緒嗣.md "wikilink")。
  - **[續日本後紀](../Page/續日本後紀.md "wikilink")** -
    [仁明天皇時代](../Page/仁明天皇.md "wikilink")（833年 -
    850年）凡20卷。869年（貞觀11年）完成。撰者[藤原良房](../Page/藤原良房.md "wikilink")・[春澄善繩等](../Page/春澄善繩.md "wikilink")。
  - **[日本文德天皇實錄](../Page/日本文德天皇實錄.md "wikilink")** -
    [文德天皇時代](../Page/文德天皇.md "wikilink")（850年 -
    858年）凡10卷。879年（元慶3年）完成。撰者[藤原基經](../Page/藤原基經.md "wikilink")・菅原是善・嶋田良臣等。
  - **[日本三代實錄](../Page/日本三代實錄.md "wikilink")** -
    [清和天皇開始](../Page/清和天皇.md "wikilink")、[光孝天皇結束](../Page/光孝天皇.md "wikilink")（858年
    -
    887年）凡50卷。901年（延喜元年）完成。撰者[藤原時平](../Page/藤原時平.md "wikilink")・[大藏善行](../Page/大藏善行.md "wikilink")・[菅原道真等](../Page/菅原道真.md "wikilink")。

## 正史之外的其他重要史書

  - [古事記](../Page/古事記.md "wikilink") -
    據序文先于《[日本書紀](../Page/日本書紀.md "wikilink")》的史書。
  - [本朝世紀](../Page/本朝世紀.md "wikilink") -
    [鳥羽上皇欲編輯繼承](../Page/鳥羽天皇.md "wikilink")**六國史**之[國史所](../Page/國史.md "wikilink")[敕撰之](../Page/敕撰.md "wikilink")[史書](../Page/史書.md "wikilink")。未完。
  - [吾妻鏡](../Page/吾妻鏡.md "wikilink") -
    [鎌倉幕府](../Page/鎌倉幕府.md "wikilink")[編年体](../Page/編年体.md "wikilink")、日記体裁之[史書](../Page/史書.md "wikilink")。
  - [本朝通鑑](../Page/本朝通鑑.md "wikilink") -
    林家所編纂之[江戸幕府主導的](../Page/江戸幕府.md "wikilink")[編年体](../Page/編年体.md "wikilink")[史書](../Page/史書.md "wikilink")。
  - [大日本史](../Page/大日本史.md "wikilink") -
    [水戸藩所編纂的](../Page/水戸藩.md "wikilink")[紀傳體史書](../Page/紀傳體.md "wikilink")。
  - [大日本史料](../Page/大日本史料.md "wikilink") -
    [帝國大学文科大学](../Page/東京帝國大学.md "wikilink")[史料編纂掛](../Page/史料.md "wikilink")（[東京大学史料編纂所](../Page/東京大学史料編纂所.md "wikilink")）所持續編纂，整理**六國史**以降之史料。

## 外部連結

  - [六國史](http://miko.org/~uraki/kuon/furu/furu_index1.htm)

[Category:飛鳥時代典籍](../Category/飛鳥時代典籍.md "wikilink")
[Category:神道文獻](../Category/神道文獻.md "wikilink")
[Category:名數6](../Category/名數6.md "wikilink")
[Category:平安時代典籍](../Category/平安時代典籍.md "wikilink")