[Microburstnasa.JPG](https://zh.wikipedia.org/wiki/File:Microburstnasa.JPG "fig:Microburstnasa.JPG")
[Microburst_-_NOAA.jpg](https://zh.wikipedia.org/wiki/File:Microburst_-_NOAA.jpg "fig:Microburst_-_NOAA.jpg")

**微下擊爆流**（），又稱**微爆流**、**微爆氣流**、**微下衝氣流**，是一種局部性的下沉氣流，氣流到達地面後會產生一股與[龍捲風破壞力相約的直線風](../Page/龍捲風.md "wikilink")（straight-line
winds）向四方八面擴散。

微下擊暴流是[下擊暴流的一種](../Page/下擊暴流.md "wikilink")，氣象學者[籐田哲也把下爆氣流分為](../Page/藤田哲也_\(气象学家\).md "wikilink")**微暴流**和**巨暴流**（Macroburst），影響方圓4[公里或以下的被界定為](../Page/公里.md "wikilink")「微暴流」，影響方圓4[公里以上則被界定為](../Page/公里.md "wikilink")「巨暴流」。

微下擊暴流能夠產生時速超過270公里的風。

## 對飛機構成的危險

微下擊暴流的突發性和威力會對飛機構成重大危險，特別是位署低空正在起降的飛機。微下擊暴流大多是以順風方向吹流經[飛行器](../Page/飛行器.md "wikilink")，使飛行器[升力大減](../Page/升力.md "wikilink")。以下是幾宗因為微暴流而在機場附近墜毀的飛行事故：

  - [達美航空191號班機](../Page/達美航空191號班機.md "wikilink")
  - [美国航空1420号班机](../Page/美国航空1420号班机.md "wikilink")
  - [泛美航空759號班機](../Page/泛美航空759號班機.md "wikilink")
  - [全美航空1016號班機](../Page/全美航空1016號班機空难.md "wikilink")

微下擊暴流經常導致一些正在降落的飛機墜毀。飛機預備著陸時，[飛行員會把空速減至適當水平](../Page/飛行員.md "wikilink")。當微下擊暴流擊中飛機時，突然增強的頂頭風（Headwind）會令飛機升力增加。一些未遇過微暴流的飛行員就會減速以減低升力。之後飛機會穿過微暴流中心進入順風帶，順風又會再令飛機的升力減少，最後飛機會因升力不足而墜毀。

遇上微下擊暴流，最佳的處理方法就是一察覺到飛行速度突變時馬上加速，這樣，就算是遇上順風而導致升力減少，至少可以保持飛機不會墜地。

## 參考資料

  - [Fujita, T.T.](../Page/藤田哲也_\(气象学家\).md "wikilink") (1981).
    "Tornadoes and Downbursts in the Context of Generalized Planetary
    Scales". *[Journal of the Atmospheric
    Sciences](../Page/大氣科學期刊.md "wikilink")*, 38 (8).
  - [Fujita, T.T.](../Page/藤田哲也_\(气象学家\).md "wikilink") (1985). "The
    Downburst, microburst and macroburst". SMRP Research Paper 210, 122
    pp.
  - Wilson, James W. and [Roger M.
    Wakimoto](../Page/羅傑·脇本.md "wikilink") (2001). "The
    Discovery of the Downburst - TT Fujita's Contribution". [Bulletin of
    the American Meteorological
    Society](../Page/美國氣象學會通告.md "wikilink"), 82 (1).

## 外部連結

  - [半官方微下擊暴流手冊](https://web.archive.org/web/20051127025042/http://www-frd.fsl.noaa.gov/mab/microburst/)
  - [微下擊暴流（WW2010）](http://ww2010.atmos.uiuc.edu/\(Gh\)/guides/mtr/svr/comp/out/micro/)
  - [香港天文臺對微下擊暴流的簡單解說](http://www.hko.gov.hk/aviat/amt/cause/microburst_c.htm)

[Category:恶劣天气与对流](../Category/恶劣天气与对流.md "wikilink")
[Category:气流](../Category/气流.md "wikilink")
[Category:風暴](../Category/風暴.md "wikilink")
[Category:氣象災害](../Category/氣象災害.md "wikilink")
[Category:对航空器的气象灾害](../Category/对航空器的气象灾害.md "wikilink")