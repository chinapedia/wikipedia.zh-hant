**李植**，字**汝培**，[明朝政治人物](../Page/明朝.md "wikilink")，[南直](../Page/南直.md "wikilink")[揚州](../Page/揚州.md "wikilink")[江都人](../Page/江都.md "wikilink")，[籍貫](../Page/籍貫.md "wikilink")[山西省](../Page/山西省.md "wikilink")[大同縣](../Page/大同縣.md "wikilink")，在[張居正死後](../Page/張居正.md "wikilink")，彈劾[張居正與](../Page/張居正.md "wikilink")[馮保而出名](../Page/馮保.md "wikilink")。

## 生平

山西鄉試第二十名。萬曆五年（1577年）會試第一百八十一名丁丑科，進士第三甲第二百零七名\[1\]\[2\]\[3\]。初官江西道御史。

萬曆十年（1582年）十二月壬辰（初八日）上疏彈劾[馮保十二大罪狀](../Page/馮保.md "wikilink")，帝甚喜，發配馮保到[南京](../Page/南京.md "wikilink")[孝陵種菜](../Page/孝陵.md "wikilink")。又聯合雲南道御史[羊可立](../Page/羊可立.md "wikilink")、山東道監察御史[江東之](../Page/江東之.md "wikilink")、陝西道道御史[揚四知等攻訐](../Page/揚四知.md "wikilink")[張居正與馮保](../Page/張居正.md "wikilink")「交結恣橫」、「寶藏逾天府」。[明神宗下令籍居正之家](../Page/明神宗.md "wikilink")。又上疏彈劾[潘季馴](../Page/潘季馴.md "wikilink")，說潘季馴“無中生有，欺皇上於今日矣。”

萬曆以「盡忠言事，揭發大奸有功」，晉升為[太僕寺](../Page/太僕寺.md "wikilink")[少卿](../Page/少卿.md "wikilink")，最後官至[遼東](../Page/遼東.md "wikilink")[巡撫](../Page/巡撫.md "wikilink")，在任期間墾土積粟，得田四萬畝，歲獲糧萬石，戶部推其為九邊模範，後被劾罷官，命家居聽用，竟不召至卒，熹宗時贈兵部右侍郎。。\[4\]\[5\]

## 家族

曾祖父[李英](../Page/李英.md "wikilink")；祖父[李滿](../Page/李滿.md "wikilink")，贈奉政大夫兵部郎中；父親[李承式](../Page/李承式.md "wikilink")，曾任布政司左參政。母郭氏（贈宜人）；繼母閆氏（封宜人）\[6\]。

## 参考文献

[Category:明朝庶吉士](../Category/明朝庶吉士.md "wikilink")
[Category:明朝監察御史](../Category/明朝監察御史.md "wikilink")
[Category:明朝太僕寺少卿](../Category/明朝太僕寺少卿.md "wikilink")
[Category:揚州人](../Category/揚州人.md "wikilink")
[Category:大同人](../Category/大同人.md "wikilink")
[Z](../Category/李姓.md "wikilink")

1.
2.
3.
4.  《明史·[卷二百三十六](../Page/s:明史/卷236.md "wikilink")》
5.  [(明)李植](http://archive.ihp.sinica.edu.tw/ttscgi/ttsquery?0:16530311:mctauac:TM%3D%A7%F5%B4%D3)，[中央研究院歷史語言研究所](../Page/中央研究院.md "wikilink")
6.