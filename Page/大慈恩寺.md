[DaCiEnSi.jpg](https://zh.wikipedia.org/wiki/File:DaCiEnSi.jpg "fig:DaCiEnSi.jpg")
**大慈恩寺**，位于[中國](../Page/中华人民共和国.md "wikilink")[陝西省](../Page/陝西省.md "wikilink")[西安市南郊](../Page/西安市.md "wikilink")。大慈恩寺亦是[唐代長安的四大譯經場之一](../Page/唐长安城.md "wikilink")，也是[中國佛教](../Page/中國佛教.md "wikilink")[法相唯識宗的](../Page/法相唯識宗.md "wikilink")[祖庭](../Page/祖庭.md "wikilink")，迄今已有1360余年歷史。

## 历史

[Giant_Wild_Goose_Pagoda,_Xi'an,_China_-_006.jpg](https://zh.wikipedia.org/wiki/File:Giant_Wild_Goose_Pagoda,_Xi'an,_China_-_006.jpg "fig:Giant_Wild_Goose_Pagoda,_Xi'an,_China_-_006.jpg")\]\]
[Dayanta_200911_175.jpg](https://zh.wikipedia.org/wiki/File:Dayanta_200911_175.jpg "fig:Dayanta_200911_175.jpg")\]\]
大慈恩寺原为[隋代无漏寺](../Page/隋代.md "wikilink")，唐高祖武德年间已经废弃，[唐太宗](../Page/唐太宗.md "wikilink")[貞觀二十二年](../Page/貞觀_\(唐朝\).md "wikilink")（公元648年），是当时的太子[李治为了母亲](../Page/李治.md "wikilink")[长孙皇后追薦冥福而重新修建](../Page/长孙皇后.md "wikilink")，改名大慈恩寺。\[1\]

## 寺内建筑

### 大雁塔

[大雁塔建于唐代](../Page/大雁塔.md "wikilink")，寺院新建落成以后，[玄奘法師受朝廷圣命](../Page/玄奘.md "wikilink")，為首任上座主持，并在此地潛心翻譯佛經十余年。唐高宗永徽三年（公元652年），玄奘按照西域窣堵婆之法修建大石浮屠，高宗惧其难成，材料改为砖瓦，建成时有五层，高一百八十尺。\[2\][武则天长安年间](../Page/武则天.md "wikilink")，进行募捐对塔进行改造，增加高度。现在大雁塔高七层，一百九十四尺。

### 其他

寺院按中國傳統風格建造，寺院内[鐘樓內懸掛有鐵鐘一口](../Page/鐘樓.md "wikilink")，為[明代](../Page/明代.md "wikilink")[嘉靖二十七年十月](../Page/嘉靖.md "wikilink")（1548年）鑄造，重達三萬斤，上鑄有“雁塔晨鐘”字樣，為著名的[關中八景之一](../Page/關中八景.md "wikilink")。

寺內牡丹亭種植有著名[牡丹七十余種](../Page/牡丹.md "wikilink")，建有玄奘紀念館。

## 文化

唐代时每年科举进士及第，依惯例要曲江张宴，入慈恩寺，登大雁塔，题其姓名于壁上，作为纪念，是后世进士题名的起源。\[3\]<sup>页210</sup>

## 参考文献

{{-}}

[D](../Category/西安佛寺.md "wikilink") [D](../Category/西安旅游.md "wikilink")
[D](../Category/法相唯識宗.md "wikilink")
[D](../Category/汉族地区佛教全国重点寺院.md "wikilink")

1.

2.
3.