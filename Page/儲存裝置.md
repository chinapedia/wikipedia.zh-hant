**儲存裝置**是用于储存資訊的设备或裝置。通常是將資訊數位化後再以利用[電](../Page/電.md "wikilink")、[磁或](../Page/磁.md "wikilink")[光學等方式的媒體加以儲存](../Page/光學.md "wikilink")。

常見的儲存裝置（[電腦數據存貯器](../Page/電腦數據存貯器.md "wikilink")）有：

  - 利用電能方式儲存信息的裝置如：各式[記憶體](../Page/記憶體.md "wikilink")，如各式[随机存取存储器](../Page/随机存取存储器.md "wikilink")（RAM）、[只读存储器](../Page/只读存储器.md "wikilink")（ROM）等
  - 利用磁能方式儲存信息的裝置如：[硬碟](../Page/硬碟.md "wikilink")、[軟碟](../Page/軟碟.md "wikilink")、[磁帶](../Page/磁帶.md "wikilink")、[磁芯記憶體](../Page/磁芯記憶體.md "wikilink")、[磁泡存储器](../Page/磁泡存储器.md "wikilink")
  - 利用光學方式儲存信息的裝置如：[CD或](../Page/CD.md "wikilink")[DVD](../Page/DVD.md "wikilink")
  - 利用磁光方式儲存信息的裝置如：[磁光碟](../Page/磁光碟.md "wikilink")
  - 利用其他實體物如紙卡、紙帶等儲存信息的裝置如：[打孔卡](../Page/打孔卡.md "wikilink")、打孔帶等

具体驅動裝置（）的例子如：

  - [磁鼓記憶體](../Page/磁鼓存储器.md "wikilink") （**Drum memory**）
  - [磁带机](../Page/磁带机.md "wikilink") （magnetic tape machine）
  - [软磁盘](../Page/软磁盘.md "wikilink") （floppy diskette drive）
  - [硬磁盘](../Page/硬磁盘.md "wikilink") （hard disk drive）
  - [固態硬盤](../Page/固態硬盤.md "wikilink")（Solid State Disk）
  - [光盘机](../Page/光盘机.md "wikilink") （CD drive 或 DVD drive）

## 内部存储器

内部存储器是可以被电脑的[中央处理器直接访问而不需要通过](../Page/中央处理器.md "wikilink")[输入输出设备的存储设备](../Page/输入输出.md "wikilink")。内部存储器一般用来存储运算时的数据。内存一般速度很快，例如[随机存储器](../Page/随机存储器.md "wikilink")（RAM）。

RAM也是[非永久性存储器](../Page/非永久性存储器.md "wikilink")，在断电的时候，将失去所存储的内容。[只读存储器就不是易失去内容的](../Page/只读存储器.md "wikilink")，但不适合用来存储大量的数据，因為其造价的昂贵。通常，[只读存储器在写内容进去之前也必须完全的擦除原来的内容](../Page/只读存储器.md "wikilink")，这使得大规模的使用它不切实际。所以，单独的辅助存贮器或者叫外部存储器通常被用来保存长期的稳定的数据。

有时候，主存储器这个术语被混淆的使用在[在线存储和](../Page/在线存储.md "wikilink")[硬盘上](../Page/硬盘.md "wikilink")，而这些都通常应归类为辅助存储器。

主存储器可能包括几种不同的设备，例如[CPU缓存](../Page/CPU缓存.md "wikilink")，以及特殊的[处理器寄存器](../Page/寄存器.md "wikilink")，这些都能直接被处理器访问，主存储器可以被随机的访问，那就是在任何时间访问任何位置都用相同的时间。典型的位置信息使用内存的物理地址。无论存储的内容怎么变化，物理地址是不变的。

## 辨析解意

所有能与计算机配合使用，方便进行信息储存的物件都属于此类设备。不一定带有转动的部分，或位置于计算机外部。如以下各例：

  - [記憶卡](../Page/記憶卡.md "wikilink")（flash memory cube）
  - [USB隨身碟](../Page/闪存盘.md "wikilink")（flash memory stripe）
  - [唯讀記憶體](../Page/唯讀記憶體.md "wikilink") （ROM）
  - [随机存取存储器](../Page/随机存取存储器.md "wikilink") （RAM）
  - [電腦記憶體](../Page/電腦記憶體.md "wikilink")

<File:Exquisite-hdd> mount.png|硬盘 <File:Exquisite-zip> mount.png|ZIP存储器
<File:Exquisite-cdwriter> mount.png|光盘刻录机

[Category:電腦貯存裝置](../Category/電腦貯存裝置.md "wikilink")
[Category:記錄](../Category/記錄.md "wikilink")
[Category:數位媒體](../Category/數位媒體.md "wikilink")
[Category:通信](../Category/通信.md "wikilink")
[Category:数据管理](../Category/数据管理.md "wikilink")
[Category:影片和電影技術](../Category/影片和電影技術.md "wikilink")
[Category:媒體技術](../Category/媒體技術.md "wikilink")
[Category:存储媒体](../Category/存储媒体.md "wikilink")
[Category:美術材料](../Category/美術材料.md "wikilink")
[Category:圖書資訊科學](../Category/圖書資訊科學.md "wikilink")