《**駭客任務：重裝上陣**》（）是一部在西元[2003年上映的](../Page/2003年.md "wikilink")[電影](../Page/電影.md "wikilink")。其由[沃卓斯基姐妹导演](../Page/沃卓斯基姐妹.md "wikilink")，[基努·里维斯](../Page/基努·里维斯.md "wikilink")、[劳伦斯·費許朋等主演](../Page/劳伦斯·費許朋.md "wikilink")，[袁和平](../Page/袁和平.md "wikilink")[武術指導](../Page/武術指導.md "wikilink")。此電影是[駭客任務三部曲的第二集](../Page/駭客任務三部曲.md "wikilink")，上一集為《**[駭客任務](../Page/駭客任務.md "wikilink")**》。

## 剧情

在證實了救世主的身份不久后，尼歐（Neo）进一步学会运用他在母體（Matrix）的超能力，包括看清周围人和物質的代码，以及在空中飞行。人类的革命也剧烈反转，越来越多的人从母體中获得了自由。此时尼歐开始不断地梦见崔妮蒂（Trinity）的死亡。另一方面，机器聚集了一支大軍进攻人类抵抗的中心：真实世界最后的城市錫安（Zion）。莫菲斯（Morpheus）和尼歐焦急地等待和祭司（Oracle）的联系，他们相信先知会告诉他们如何阻止灾难。特工史密斯（Smith）由于在第一集中对尼歐追捕失败被系统删除，如今脫離母體並成为电脑病毒重新出现，他可以传染任何被他接触的人（将其复制成自己），他生存的目的就是为了消灭尼歐。为了拯救錫安、人类以及他最爱的女人，尼歐和莫菲斯及其他抵抗分子必须与各种被“流放”的程式、新升级的特工、以及一群史密斯的复制品战斗，以便到达萬物之源（the
Source）。但是在萬物之源，尼歐了解到的却是动摇之前所有認識的真相……

## 演員

  - [基努李維](../Page/基努李維.md "wikilink")（Keanu Reeves） -
    湯瑪斯·安德森/[尼歐](../Page/尼歐.md "wikilink")（）
  - [勞倫斯·費許朋](../Page/勞倫斯·費許朋.md "wikilink")（Laurence Fishburne）- 莫菲斯（）
  - [凱莉安摩絲](../Page/凱莉安摩絲.md "wikilink")（Carrie-Anne Moss）- 崔妮蒂（）
  - [曉高·韋榮](../Page/曉高·韋榮.md "wikilink")（Hugo Weaving）- 史密斯探員（）
  - [潔達·蘋姬·史密斯](../Page/潔達·蘋姬·史密斯.md "wikilink") as Niobe
  - [Gloria Foster](../Page/Gloria_Foster.md "wikilink") as [The
    Oracle](../Page/Oracle_\(The_Matrix\).md "wikilink")
  - [Harold Perrineau](../Page/Harold_Perrineau.md "wikilink") as
    [Link](../Page/Link_\(The_Matrix\).md "wikilink")
  - [Monica Bellucci](../Page/Monica_Bellucci.md "wikilink") as
    [Persephone](../Page/Persephone_\(The_Matrix\).md "wikilink")
  - [Lambert Wilson](../Page/Lambert_Wilson.md "wikilink") as [The
    Merovingian](../Page/Merovingian_\(The_Matrix\).md "wikilink")
  - [Randall Duk Kim](../Page/Randall_Duk_Kim.md "wikilink") as [The
    Keymaker](../Page/Keymaker.md "wikilink")
  - [Harry Lennix](../Page/Harry_Lennix.md "wikilink") as Commander Lock
  - [Anthony Zerbe](../Page/Anthony_Zerbe.md "wikilink") as Councillor
    Hamann
  - [Nona Gaye](../Page/Nona_Gaye.md "wikilink") as
    [Zee](../Page/Zee_\(The_Matrix\).md "wikilink")
  - [Helmut Bakaitis](../Page/Helmut_Bakaitis.md "wikilink") as [The
    Architect](../Page/Architect_\(The_Matrix\).md "wikilink")
  - Neil and Adrian Rayment as [The
    Twins](../Page/Twins_\(The_Matrix\).md "wikilink")
  - [Daniel Bernhardt](../Page/Daniel_Bernhardt.md "wikilink") as Agent
    Johnson
  - [鄒兆龍](../Page/鄒兆龍.md "wikilink") as
    [賽洛芙](../Page/Seraph_\(The_Matrix\).md "wikilink")
  - [Ian Bliss](../Page/Ian_Bliss.md "wikilink") as
    [Bane](../Page/Bane_\(The_Matrix\).md "wikilink")
  - [Leigh Whannell](../Page/Leigh_Whannell.md "wikilink") as Axel
  - [Gina Torres](../Page/Gina_Torres.md "wikilink") as
    [Cas](../Page/Cas_\(The_Matrix\).md "wikilink")
  - [Nathaniel Lees](../Page/Nathaniel_Lees.md "wikilink") as Captain
    Mifune
  - [Roy Jones, Jr.](../Page/Roy_Jones,_Jr..md "wikilink") as Captain
    Ballard
  - David A. Kilde as Agent Jackson
  - [Matt McColm](../Page/Matt_McColm.md "wikilink") as Agent Thompson
  - [Cornel West](../Page/Cornel_West.md "wikilink") as Councillor West
  - [Steve Bastoni](../Page/Steve_Bastoni.md "wikilink") as Captain
    Soren
  - [Anthony
    Wong](../Page/Anthony_Wong_\(Australian_actor\).md "wikilink") as
    [Ghost](../Page/Ghost_\(The_Matrix\).md "wikilink")
  - [Clayton Watson](../Page/Clayton_Watson.md "wikilink") as
    [Kid](../Page/Kid_\(The_Matrix\).md "wikilink")
  - [Tiger Chen](../Page/Tiger_Chen.md "wikilink") as the Merovingian's
    Ronin

## 發行

## 细节

这部系列片之所以受到诸多程序员和技术人员的热爱，很大程度上是精确的细节所致：

  - 当尼歐去萬物之源时，崔妮蒂需要攻击大楼的网络（详见剧情简介），她在屏幕上所输入的是一行标准的[SSH命令](../Page/Secure_Shell.md "wikilink")：

` `[`ssh`](../Page/SSH.md "wikilink")` 10.2.2.2 -l `[`root`](../Page/root.md "wikilink")

## 反響

### 專業評價

[爛番茄網站上對這部](../Page/爛番茄.md "wikilink")[電影的好評度為](../Page/電影.md "wikilink")73%，在238條專業評論中，174條專業評論贊同，64條專業評論反對，平均分為6.8/10，觀眾的評價則是72%，獲得全方面的讚譽。

### 票房

## 續集

由[華納兄弟發行](../Page/華納兄弟.md "wikilink")。於2003年11月5日上映

## 參見

  - [駭客任務](../Page/駭客任務.md "wikilink")（The Matrix）
  - [駭客任務3](../Page/駭客任務3.md "wikilink")（The Matrix: Revolution）
  - [駭客任務動畫版](../Page/駭客任務動畫版.md "wikilink") （The Animatrix）
  - [-{zh-tw:駭客任務：重裝上陣 (遊戲);zh-hk:廿二世紀殺人網絡：決戰未來
    (遊戲)}-](../Page/Enter_The_Matrix.md "wikilink") （Enter The
    Matrix）
  - [-{zh-tw:駭客任務Online;zh-hk:廿二世紀殺人網絡Online}-](../Page/MxO.md "wikilink")
    （The Matrix Online）

## 外部链接

  - [电影官方网页](http://www.thematrix.com)

  -
  - [爛番茄上](../Page/爛番茄.md "wikilink")《[The Matrix
    Reloaded](https://www.rottentomatoes.com/m/matrix_reloaded/?search=The%20Matrix%20Reloaded)》的資料

  -
[Category:2003年電影](../Category/2003年電影.md "wikilink")
[Category:美國科幻動作片](../Category/美國科幻動作片.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:美國動作驚悚片](../Category/美國動作驚悚片.md "wikilink")
[Category:冒險片](../Category/冒險片.md "wikilink")
[Category:續集電影](../Category/續集電影.md "wikilink")
[Category:華納兄弟電影](../Category/華納兄弟電影.md "wikilink")
[Category:威秀电影](../Category/威秀电影.md "wikilink")
[Category:黑色科幻片](../Category/黑色科幻片.md "wikilink")
[Category:人工智能題材作品](../Category/人工智能題材作品.md "wikilink")
[Category:黑客帝国系列](../Category/黑客帝国系列.md "wikilink")
[Category:澳大利亞取景電影](../Category/澳大利亞取景電影.md "wikilink")