**韋執誼**，字**宗仁**，[京兆](../Page/京兆府.md "wikilink")[杜陵](../Page/杜陵县.md "wikilink")（今[陕西省](../Page/陕西省.md "wikilink")[西安市](../Page/西安市.md "wikilink")）人。[唐朝政治人物](../Page/唐朝.md "wikilink")，曾為[同中书门下平章事](../Page/同中书门下平章事.md "wikilink")（[宰相](../Page/宰相.md "wikilink")），參與[永貞改革](../Page/永貞改革.md "wikilink")，失敗後，成為被[流放的](../Page/流放.md "wikilink")[八司马之一](../Page/二王八司馬.md "wikilink")。

## 簡介

韋執誼出自[京兆韦氏龙门公房](../Page/京兆韦氏.md "wikilink")，少有才名，及[进士第](../Page/进士.md "wikilink")，先後任[右拾遺](../Page/右拾遺.md "wikilink")、[翰林學士](../Page/翰林學士.md "wikilink")。娶[杜黄裳次女為妻](../Page/杜黄裳.md "wikilink")\[1\]。

[贞元二十一年](../Page/貞元_\(唐朝\).md "wikilink")（805年），[唐順宗即位](../Page/唐順宗.md "wikilink")。[永贞元年](../Page/永貞_\(唐朝\).md "wikilink")（805年）二月辛亥\[2\]，下诏任命[吏部郎中韦执谊守](../Page/吏部郎中.md "wikilink")[尚书左丞](../Page/尚书左丞.md "wikilink")、同中书门下平章事（宰相），任命[王叔文為翰林學士](../Page/王叔文.md "wikilink")，開始進行[永貞改革](../Page/永貞改革.md "wikilink")。王叔文主張廢除宦官軍權，受到宦官[俱文珍等人的抵制](../Page/俱文珍.md "wikilink")。韋執誼企圖施恩于岳父杜黃裳，封杜黃裳為[太常卿](../Page/太常卿.md "wikilink")，但杜黃裳並不領情。

同年八月，唐順宗被俱文珍發動的[政變逼退](../Page/政變.md "wikilink")，號稱[太上皇](../Page/太上皇.md "wikilink")；[唐宪宗李純即位](../Page/唐宪宗.md "wikilink")，史稱“[永貞內禪](../Page/永貞內禪.md "wikilink")”。十一月，宪宗下詔貶韋執誼為[崖州](../Page/崖州.md "wikilink")（今[海南省](../Page/海南省.md "wikilink")[瓊山](../Page/瓊山.md "wikilink")）[司馬](../Page/司馬.md "wikilink")\[3\]，再贬[司户](../Page/司户.md "wikilink")\[4\]。[元和三年](../Page/元和.md "wikilink")（808年）以前病死于崖州，杜夫人歸其喪。

## 家庭

### 夫人

  - [京兆杜氏](../Page/京兆杜氏.md "wikilink")，黄门侍郎[杜黄裳之女](../Page/杜黄裳.md "wikilink")

### 子女

  - [韦曙](../Page/韦曙.md "wikilink")
  - [韦昶](../Page/韦昶_\(唐朝\).md "wikilink")，朝请大夫、守太子左庶子、上柱国
  - [韦曈](../Page/韦曈.md "wikilink")
  - [韦旭](../Page/韦旭.md "wikilink")，字就之
  - 韦某
  - [韦绚](../Page/韦绚.md "wikilink")

## 注釋

[Category:唐朝同中书门下平章事](../Category/唐朝同中书门下平章事.md "wikilink")
[Category:唐朝進士](../Category/唐朝進士.md "wikilink")
[Category:右拾遗](../Category/右拾遗.md "wikilink")
[Category:唐朝翰林学士](../Category/唐朝翰林学士.md "wikilink")
[Category:唐朝吏部郎中](../Category/唐朝吏部郎中.md "wikilink")
[Category:唐朝尚书左丞](../Category/唐朝尚书左丞.md "wikilink")
[Category:唐朝州司马](../Category/唐朝州司马.md "wikilink")
[Category:唐朝司户参军](../Category/唐朝司户参军.md "wikilink")
[Z](../Category/京兆韦氏.md "wikilink") [Z](../Category/韦姓.md "wikilink")
[Category:唐朝宰相](../Category/唐朝宰相.md "wikilink")

1.  《旧唐书·杜黄裳传》云：“女嫁韦执谊，……及执谊谴逐，黄裳终保全之，洎死岭表，请归其丧，以办葬事。”
2.  《旧唐书·顺宗纪》作“辛卯”，误。
3.  《新唐书·宪宗纪》作永贞元年十一月壬申“贬韦执谊为崖州司马”。
4.  陈景云《韩集点勘》卷四：“当是自司马再贬司户。”