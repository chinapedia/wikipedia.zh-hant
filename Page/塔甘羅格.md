border |flag_caption= |anthem=[Anthem of
Taganrog](../Page/Anthem_of_Taganrog.md "wikilink") |anthem_ref=\[1\]
|holiday=September 12 |holiday_ref=\[2\] |federal_subject=[Rostov
Oblast](../Page/Rostov_Oblast.md "wikilink")
|federal_subject_ref=\[3\] |adm_data_as_of=November 2014
|adm_city_jur=Taganrog [Urban
Okrug](../Page/City_of_federal_subject_significance.md "wikilink")
|adm_city_jur_ref=\[4\] |adm_ctr_of=Taganrog Urban Okrug
|adm_ctr_of_ref=\[5\] |inhabloc_cat=City |inhabloc_cat_ref=\[6\]
|mun_data_as_of=October 2012 |urban_okrug_jur=Taganrog Urban Okrug
|urban_okrug_jur_ref=\[7\] |mun_admctr_of=Taganrog Urban Okrug
|mun_admctr_of_ref=\[8\] |leader_title=Head
|leader_title_ref=\[9\] |leader_name=Andrey Lisitsky
|leader_name_ref=\[10\] |representative_body=City Duma
|representative_body_ref=\[11\] |area_of_what= |area_as_of=
|area_km2=80 |area_km2_ref=\[12\] |pop_2010census=257681
|pop_2010census_rank=72nd |pop_2010census_ref=\[13\] |pop_density=
|pop_density_as_of= |pop_density_ref= |pop_latest=251100
|pop_latest_date=January 2016 |pop_latest_ref=\[14\]
|established_date=September 12, 1698 |established_title=
|established_date_ref=\[15\] |current_cat_date=1775
|current_cat_date_ref=\[16\] |prev_name1=[The Fortress Which Is on
Tagan-Rog](../Page/Taganrog_Fortress.md "wikilink") (officially,
Troitskaya Fortress) |prev_name1_date=1775 |prev_name1_ref=\[17\]
|prev_name2= |prev_name2_date= |prev_name2_ref
|postal_codes=347900, 347902, 347904, 347905, 347909, 347910, 347913,
347916, 347919, 347922–347924, 347927, 347928, 347930–347932, 347935,
347936, 347939, 347942, 347943, 347949, 347990 |dialing_codes=8634
|dialing_codes_ref= |website=<http://www.tagancity.ru> |website_ref=
|date=May 2010 }}
**塔甘羅格**（）位於[亞速海岸](../Page/亞速海.md "wikilink")，是[俄羅斯](../Page/俄羅斯.md "wikilink")[羅斯托夫州的一個港口城市](../Page/羅斯托夫州.md "wikilink")。2002年人口281,947人。

1698年9月12日，由沙皇[彼得大帝宣布建立](../Page/彼得大帝.md "wikilink")，是[俄羅斯海軍第一個基地](../Page/俄羅斯海軍.md "wikilink")。著名的劇作家[安東·契訶夫在這裡出生](../Page/安東·契訶夫.md "wikilink")。

2010年1月29日
[俄羅斯總統](../Page/俄羅斯.md "wikilink")[德米特里·梅德韋傑夫開始視察塔甘羅格市](../Page/德米特里·梅德韋傑夫.md "wikilink")，他首先向[安東·契訶夫雕像敬獻了花圈](../Page/安東·契訶夫.md "wikilink")。\[18\].

## 兄弟城市

  - [吕登沙伊德](../Page/吕登沙伊德.md "wikilink") 1991年

  - [马里乌波尔](../Page/马里乌波尔.md "wikilink") 1993年

  - [巴登维勒](../Page/巴登维勒.md "wikilink") 2002年

  - [济宁市](../Page/济宁市.md "wikilink") 2009年6月3日

  - [平斯克](../Page/平斯克.md "wikilink") 2009年6月25日

  - [哈尔齐兹克](../Page/哈尔齐兹克.md "wikilink") 2009年9月18日

  - [安特拉齐特](../Page/安特拉齐特.md "wikilink") 2012年12月7日

  - [弗利辛恩](../Page/弗利辛恩.md "wikilink")

## 參考資料

<references/>

[Т](../Category/羅斯托夫州城市.md "wikilink")

1.  Decision \#537

2.  Charter of Taganrog, Article 2

3.
4.
5.
6.
7.
8.
9.  Charter of Taganrog, Article 12

10. Official website of Taganrog. [Andrey Vladimirovich
    Lisitsky](https://tagancity.ru/page/rukovoditieli-i-podrazdielieniia),
    Head of the Administration of the City of Taganrog

11.
12. Official website of Taganrog. [Information About
    Taganrog](https://tagancity.ru/page/taghanrogh-gorod-voinskoi-slavy)


13.

14. Rostov Oblast Territorial Branch of the [Federal State Statistics
    Service](../Page/Russian_Federal_State_Statistics_Service.md "wikilink").
    [Cities with Populations of 100,000 and
    Over](http://rostov.gks.ru/wps/wcm/connect/rosstat_ts/rostov/resources/8cc0c50046e82b5ea1feb987789c42f5/%D0%93%D0%BE%D1%80%D0%BE%D0%B4%D0%B0+%D1%81+%D1%87%D0%B8%D1%81%D0%BB%D0%B5%D0%BD%D0%BD%D0%BE%D1%81%D1%82%D1%8C%D1%8E+%D0%BD%D0%B0%D1%81%D0%B5%D0%BB%D0%B5%D0%BD%D0%B8%D1%8F.pdf)


15.
16.
17.
18. [梅德韋傑夫向契訶夫雕像敬獻花圈 (in
    Chinese)](http://big5.rusnews.cn/photo/20100129/42693608.html)