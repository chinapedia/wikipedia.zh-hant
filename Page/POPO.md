**POPO**（又称：**网易POPO**、**泡泡**、**网易泡泡**），是[网易推出的](../Page/网易.md "wikilink")[即时通讯软件](../Page/即时通讯.md "wikilink")，支持即时文字聊天、语音通话、视频对话、文件断点续传等基本即时通讯功能，提供邮件提醒、多人兴趣组、在线及本地音乐播放、网络电台、发送网络多媒体文件、网络文件共享、自定义软件皮肤等多种功能，并可与移动通讯终端等多种通讯方式相连。

## 参考文献

## 外部连结

  - [POPO](http://popo.netease.com/)

[Category:网易](../Category/网易.md "wikilink")
[Category:即时通讯软件](../Category/即时通讯软件.md "wikilink")