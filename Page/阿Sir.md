**阿Sir**，通俗讀音沒有帶有[英語的](../Page/英語.md "wikilink")「r」音（[國際音標](../Page/國際音標.md "wikilink")：/<sub>3</sub>　sœ<sub>4</sub>/），此外也有讀作**阿蛇**（[國際音標](../Page/國際音標.md "wikilink")：/<sub>3</sub>　s<sub>4</sub>/），被認為是[中國大陸口音](../Page/中國大陸.md "wikilink")。「阿Sir」是[香港和其他](../Page/香港.md "wikilink")[粵語使用地區人宜的](../Page/粵語.md "wikilink")[慣用語](../Page/慣用語.md "wikilink")，借用自[英文的](../Page/英文.md "wikilink")[Sir](../Page/:en:Sir.md "wikilink")〈是對[男士的雅稱](../Page/男士.md "wikilink")，以表示向被稱呼者表達尊重以及敬意，用法不制限於有[爵士](../Page/爵士.md "wikilink")[名銜者](../Page/名銜.md "wikilink")〉，但是有一點與英文不同的，是英語中不限制於對年長者使用此稱呼，例如[侍應對著年青甚至男孩顧客](../Page/侍應.md "wikilink")，亦可以稱為Sir，在香港則通常不會這樣使用，較年少者通常稱為[先生](../Page/先生.md "wikilink")。

## 日常用法

在不同[行業](../Page/行業.md "wikilink")[工作的人士都有被稱作阿Sir](../Page/工作.md "wikilink")，或者配以受者的[姓稱作](../Page/姓.md "wikilink")「劉Sir」等，例子如下：

  - 在[政府部門](../Page/政府部門.md "wikilink")，尤其是[紀律部隊或](../Page/紀律部隊.md "wikilink")[制服團體](../Page/制服團體.md "wikilink")（例如[警察](../Page/警察.md "wikilink")、[消防員及](../Page/消防員.md "wikilink")[童軍等](../Page/童軍.md "wikilink")），下級向上級，或民眾向該等人員，其用意相等於長官；相對女性的雅稱為[Madam](../Page/:en:Madam.md "wikilink")〈[國際音標](../Page/國際音標.md "wikilink")：/m<sub>1</sub>　tm<sub>4</sub>/）
  - 男性[老師多被稱作阿Sir](../Page/老師.md "wikilink")，以至各行各業老前輩或學識豐富者，例如：[伍Sir](../Page/伍晃榮.md "wikilink")、[林Sir](../Page/林尚義.md "wikilink")、[李Sir](../Page/李慶華.md "wikilink")、[邵Sir](../Page/邵逸夫.md "wikilink")、[King
    Sir](../Page/鍾景輝.md "wikilink")、[毛Sir及](../Page/毛俊輝.md "wikilink")[曾Sir等](../Page/曾近榮.md "wikilink")；相對女性老師的雅稱為[Miss](../Page/:en:Miss.md "wikilink")〈[國際音標](../Page/國際音標.md "wikilink")：/miː<sub>1</sub>　siː<sub>4</sub>/）
  - 男性[社會工作者和其他社會福利機構人員](../Page/社會工作者.md "wikilink")、男性[護士和其他輔助醫療人員](../Page/護士.md "wikilink")（例如：[物理治療師](../Page/物理治療.md "wikilink")、[職業治療師及](../Page/職能治療師.md "wikilink")[言語治療師等](../Page/言語治療.md "wikilink")），會被稱作阿Sir；相對的女性稱謂是[姑娘](../Page/姑娘.md "wikilink")

當中，姓氏的粵音會出現變調，一般均以粵語第2聲陰上聲發音，例如梁SIR則會讀成「兩SIR」，陳SIR讀成「診SIR」。

## 流行娛樂中的「阿Sir」

[ChungSir.PNG](https://zh.wikipedia.org/wiki/File:ChungSir.PNG "fig:ChungSir.PNG")《新紮師妹》中，圖中內容為「Sir」字所虛構的[漢字](../Page/漢字.md "wikilink")\]\]

  - [香港1970年代末開始](../Page/香港1970年代.md "wikilink")，有一個家傳戶曉的[竹葉青酒](../Page/竹葉青酒.md "wikilink")[廣告](../Page/廣告.md "wikilink")（[見youtube.com的視頻](http://www.youtube.com/watch?v=rOQZ4CmhY4A&feature=related)），一群建築工地工人稱呼西洋籍貫的上司阿Sir，阿Sir以帶有西洋口音的粵語說：「你精我都精」，眾人起舉酒杯回應道：「飲杯竹葉青！」
  - 香港於1994年播映的[無綫電視劇集](../Page/無綫電視劇集列表.md "wikilink")《[阿Sir早晨](../Page/阿Sir早晨.md "wikilink")》（由[黎明飾演主角祝大保老師](../Page/黎明.md "wikilink")——祝Sir）中使用了阿Sir作為片名，當中更有一場妙用阿Sir稱謂的片段：一天晚上祝Sir在路上遇見一個女學生被幾個不良少年欺凌，上前阻止，女學生馬上呼叫阿Sir，不良少年初誤以為祝Sir是警察，立時收手；但是後來他們得知祝Sir是教師而非警察，立時向他襲擊。可見阿Sir一詞在日常應用中也會產生誤會。
  - 香港於2011年播映的[無綫電視劇集](../Page/無綫電視劇集列表.md "wikilink")《[點解阿Sir係阿Sir](../Page/點解阿Sir係阿Sir.md "wikilink")》，[陳豪飾演的主角羅耀華](../Page/陳豪.md "wikilink")（羅Sir），以臥底身分進入學校任教，因而同時有「警察阿Sir」與「教師阿Sir」的雙重身分，片名本身也同樣一語雙關。
  - [香港電影](../Page/香港電影.md "wikilink")[喜劇](../Page/喜劇.md "wikilink")《[新紮師妹](../Page/新紮師妹.md "wikilink")》系列中，[警官鍾Sir](../Page/警官.md "wikilink")（[許紹雄飾](../Page/許紹雄.md "wikilink")）在一次[臥底行動中害怕被](../Page/臥底.md "wikilink")[黑幫知道自己的](../Page/黑幫.md "wikilink")[身份](../Page/身份.md "wikilink")，於情急下創作了Sir字的[漢字寫法](../Page/漢字.md "wikilink")（見右圖）。

## 相關條目

  - [香港語文](../Page/香港語文.md "wikilink")
  - [香港粵語](../Page/香港粵語.md "wikilink")
  - [香港英語](../Page/香港英語.md "wikilink")

[分類:香港特定人群稱謂](../Page/分類:香港特定人群稱謂.md "wikilink")

[Category:香港俚語](../Category/香港俚語.md "wikilink")
[Category:男性称谓](../Category/男性称谓.md "wikilink")
[Category:尊称](../Category/尊称.md "wikilink")