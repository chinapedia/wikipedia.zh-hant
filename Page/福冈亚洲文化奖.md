**福冈亚洲文化奖**（日語：**福岡アジア文化賞**，英語：**Fukuoka
Prize**）是[日本](../Page/日本.md "wikilink")[福冈市为弘扬](../Page/福冈市.md "wikilink")[亚洲文化于](../Page/亚洲文化.md "wikilink")1990年设立的奖项。其目的是发展亚洲文化，为[亚洲人民的相互学习和广泛交流奠定基础](../Page/亚洲.md "wikilink")。受獎對象的「亞洲文化」以東亞、東南亞、南亞為限。

## 獎項

1990年第一屆所頒發的是不分種類的「**創設特別獎**」，之後則分為三種：

  - **大獎**

獎勵在學術研究或藝術文化領域向全世界展現亞洲文化的意義者，必要條件包含国際性、普遍性、大眾性、独創性等等。每年頒發予1人或1團體，獎金500万日圓。

  - **學術研究獎**

獎勵以亞洲為研究對象的學術研究貢獻，包含社会学、文化人類學、文化研究、歷史學、經濟學、政治學、考古學等領域。每年頒發予1-2人或1-2團體，獎金300万日圓。

  - **藝術·文化獎**

獎勵對亞洲固有藝術、文化的保存與發展，包含文學、音樂、繪畫、雕刻、電影、影像藝術、舞蹈、傳統藝能等等。每年頒發予1-2人或1-2團體，獎金300万日圓。

每一年的總獲獎人以4人為上限。

## 获奖者

### 創設特別獎

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>得主</p></th>
<th><p>國籍</p></th>
<th><p>事蹟</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1990</p></td>
<td><p><a href="../Page/巴金.md" title="wikilink">巴金</a></p></td>
<td></td>
<td><p>作家</p></td>
</tr>
<tr class="even">
<td><p>1990</p></td>
<td><p><a href="../Page/黑澤明.md" title="wikilink">黑澤明</a></p></td>
<td></td>
<td><p>導演</p></td>
</tr>
<tr class="odd">
<td><p>1990</p></td>
<td><p><a href="../Page/李約瑟.md" title="wikilink">李約瑟</a></p></td>
<td></td>
<td><p>中國科學史研究</p></td>
</tr>
<tr class="even">
<td><p>1990</p></td>
<td><p><a href="../Page/克立·巴莫.md" title="wikilink">克立·巴莫</a></p></td>
<td></td>
<td><p>作家、政治家</p></td>
</tr>
<tr class="odd">
<td><p>1990</p></td>
<td><p><a href="../Page/矢野暢.md" title="wikilink">矢野暢</a></p></td>
<td></td>
<td><p>社會科學、亞洲區域研究</p></td>
</tr>
</tbody>
</table>

### 大獎

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>得主</p></th>
<th><p>國籍</p></th>
<th><p>事蹟</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1991</p></td>
<td><p><a href="../Page/拉维·香卡.md" title="wikilink">拉维·香卡</a></p></td>
<td></td>
<td><p>音乐家、锡塔琴演奏家</p></td>
</tr>
<tr class="even">
<td><p>1992</p></td>
<td><p><a href="../Page/金元龍.md" title="wikilink">金元龍</a></p></td>
<td></td>
<td><p>考古學家</p></td>
</tr>
<tr class="odd">
<td><p>1993</p></td>
<td><p><a href="../Page/費孝通.md" title="wikilink">費孝通</a></p></td>
<td></td>
<td><p>社會學、人類學家</p></td>
</tr>
<tr class="even">
<td><p>1994</p></td>
<td><p><a href="../Page/M._C._Subhadradis_Diskul.md" title="wikilink">M. C. Subhadradis Diskul</a></p></td>
<td></td>
<td><p>考古學、美術史家</p></td>
</tr>
<tr class="odd">
<td><p>1995</p></td>
<td><p><a href="../Page/古加拉寧葛拉特.md" title="wikilink">古加拉寧葛拉特</a></p></td>
<td></td>
<td><p>文化人類學家</p></td>
</tr>
<tr class="even">
<td><p>1996</p></td>
<td><p><a href="../Page/王仲殊.md" title="wikilink">王仲殊</a></p></td>
<td></td>
<td><p>考古學家</p></td>
</tr>
<tr class="odd">
<td><p>1997</p></td>
<td><p><a href="../Page/Chheng_Phon.md" title="wikilink">Chheng Phon</a></p></td>
<td></td>
<td><p>剧作家、艺术家</p></td>
</tr>
<tr class="even">
<td><p>1998</p></td>
<td><p><a href="../Page/李基文.md" title="wikilink">李基文</a></p></td>
<td></td>
<td><p>語言學家</p></td>
</tr>
<tr class="odd">
<td><p>1999</p></td>
<td><p><a href="../Page/侯孝賢.md" title="wikilink">侯孝賢</a></p></td>
<td></td>
<td><p>導演</p></td>
</tr>
<tr class="even">
<td><p>2000</p></td>
<td><p><a href="../Page/普拉姆迪亞·阿南達·杜爾.md" title="wikilink">普拉姆迪亞·阿南達·杜爾</a></p></td>
<td></td>
<td><p>作家</p></td>
</tr>
<tr class="odd">
<td><p>2001</p></td>
<td><p><a href="../Page/穆罕默德·尤纳斯.md" title="wikilink">穆罕默德·尤纳斯</a><a href="https://zh.wikipedia.org/wiki/File:Nobel_prize_medal.svg" title="fig:Nobel_prize_medal.svg">Nobel_prize_medal.svg</a></p></td>
<td></td>
<td><p>經濟學家</p></td>
</tr>
<tr class="even">
<td><p>2002</p></td>
<td><p><a href="../Page/張藝謀.md" title="wikilink">張藝謀</a></p></td>
<td></td>
<td><p>導演</p></td>
</tr>
<tr class="odd">
<td><p>2003</p></td>
<td><p><a href="../Page/外間守善.md" title="wikilink">外間守善</a></p></td>
<td></td>
<td><p>沖繩學家</p></td>
</tr>
<tr class="even">
<td><p>2004</p></td>
<td><p><a href="../Page/Amjad_Ali_Khan.md" title="wikilink">Amjad Ali Khan</a></p></td>
<td></td>
<td><p>Sarod樂器演奏家</p></td>
</tr>
<tr class="odd">
<td><p>2005</p></td>
<td><p><a href="../Page/任東權.md" title="wikilink">任東權</a></p></td>
<td></td>
<td><p>民俗學家</p></td>
</tr>
<tr class="even">
<td><p>2006</p></td>
<td><p><a href="../Page/莫言.md" title="wikilink">莫言</a><a href="https://zh.wikipedia.org/wiki/File:Nobel_prize_medal.svg" title="fig:Nobel_prize_medal.svg">Nobel_prize_medal.svg</a></p></td>
<td></td>
<td><p>作家</p></td>
</tr>
<tr class="odd">
<td><p>2007</p></td>
<td><p><a href="../Page/阿希斯·南迪.md" title="wikilink">阿希斯·南迪</a></p></td>
<td></td>
<td><p>文明、社會評論家</p></td>
</tr>
<tr class="even">
<td><p>2008</p></td>
<td><p><a href="../Page/許鞍華.md" title="wikilink">許鞍華</a></p></td>
<td></td>
<td><p>導演</p></td>
</tr>
<tr class="odd">
<td><p>2009</p></td>
<td><p><a href="../Page/Augustin_Berque.md" title="wikilink">Augustin Berque</a></p></td>
<td></td>
<td><p>文化地理學家</p></td>
</tr>
<tr class="even">
<td><p>2010</p></td>
<td><p><a href="../Page/黃秉冀.md" title="wikilink">黃秉冀</a></p></td>
<td></td>
<td><p>音樂家</p></td>
</tr>
<tr class="odd">
<td><p>2011</p></td>
<td><p><a href="../Page/Ang_Choulean.md" title="wikilink">Ang Choulean</a></p></td>
<td></td>
<td><p>民族學家、高棉學家</p></td>
</tr>
<tr class="even">
<td><p>2012</p></td>
<td><p><a href="../Page/紈妲娜·希瓦.md" title="wikilink">紈妲娜·希瓦</a></p></td>
<td></td>
<td><p>|環保思想家</p></td>
</tr>
<tr class="odd">
<td><p>2013</p></td>
<td><p><a href="../Page/中村哲.md" title="wikilink">中村哲</a></p></td>
<td></td>
<td><p>醫生</p></td>
</tr>
<tr class="even">
<td><p>2014</p></td>
<td><p><a href="../Page/傅高義.md" title="wikilink">傅高義</a></p></td>
<td></td>
<td><p>社會學家</p></td>
</tr>
<tr class="odd">
<td><p>2015</p></td>
<td><p><a href="../Page/吴丹敏.md" title="wikilink">吴丹敏</a></p></td>
<td></td>
<td><p>历史学家</p></td>
</tr>
<tr class="even">
<td><p>2016</p></td>
<td><p><a href="../Page/A·R·拉曼.md" title="wikilink">A·R·拉曼</a></p></td>
<td></td>
<td><p>音乐家</p></td>
</tr>
</tbody>
</table>

### 學術研究獎

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>得主</p></th>
<th><p>國籍</p></th>
<th><p>事蹟</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1991</p></td>
<td><p><a href="../Page/中根千枝.md" title="wikilink">中根千枝</a></p></td>
<td></td>
<td><p>社會人類學家</p></td>
</tr>
<tr class="even">
<td><p>1991</p></td>
<td><p><a href="../Page/Taufik_Abdullah.md" title="wikilink">Taufik Abdullah</a></p></td>
<td></td>
<td><p>社會科學、史學家</p></td>
</tr>
<tr class="odd">
<td><p>1992</p></td>
<td><p><a href="../Page/竹內實.md" title="wikilink">竹內實</a></p></td>
<td></td>
<td><p>中國學家</p></td>
</tr>
<tr class="even">
<td><p>1992</p></td>
<td><p><a href="../Page/克利福德·格爾茨.md" title="wikilink">克利福德·格爾茨</a></p></td>
<td></td>
<td><p>文化人類學家</p></td>
</tr>
<tr class="odd">
<td><p>1993</p></td>
<td><p><a href="../Page/川喜田二郎.md" title="wikilink">川喜田二郎</a></p></td>
<td></td>
<td><p>民族地理學家</p></td>
</tr>
<tr class="even">
<td><p>1993</p></td>
<td><p><a href="../Page/翁姑·阿都·阿兹.md" title="wikilink">翁姑·阿都·阿兹</a></p></td>
<td></td>
<td><p>經濟學家</p></td>
</tr>
<tr class="odd">
<td><p>1994</p></td>
<td><p><a href="../Page/石井米雄.md" title="wikilink">石井米雄</a></p></td>
<td></td>
<td><p>東南亞學家</p></td>
</tr>
<tr class="even">
<td><p>1994</p></td>
<td><p><a href="../Page/王賡武.md" title="wikilink">王賡武</a></p></td>
<td></td>
<td><p>史學家</p></td>
</tr>
<tr class="odd">
<td><p>1995</p></td>
<td><p><a href="../Page/辛島昇.md" title="wikilink">辛島昇</a></p></td>
<td></td>
<td><p>史學家</p></td>
</tr>
<tr class="even">
<td><p>1995</p></td>
<td><p><a href="../Page/韓基彥.md" title="wikilink">韓基彥</a></p></td>
<td></td>
<td><p>教育學家</p></td>
</tr>
<tr class="odd">
<td><p>1996</p></td>
<td><p><a href="../Page/衞藤瀋吉.md" title="wikilink">衞藤瀋吉</a></p></td>
<td></td>
<td><p>國際關係學家</p></td>
</tr>
<tr class="even">
<td><p>1996</p></td>
<td><p><a href="../Page/潘輝黎.md" title="wikilink">潘輝黎</a></p></td>
<td></td>
<td><p>史學家</p></td>
</tr>
<tr class="odd">
<td><p>1997</p></td>
<td><p><a href="../Page/樋口隆康.md" title="wikilink">樋口隆康</a></p></td>
<td></td>
<td><p>考古學家</p></td>
</tr>
<tr class="even">
<td><p>1997</p></td>
<td><p><a href="../Page/Romila_Thapar.md" title="wikilink">Romila Thapar</a></p></td>
<td></td>
<td><p>史學家</p></td>
</tr>
<tr class="odd">
<td><p>1998</p></td>
<td><p><a href="../Page/上田正昭.md" title="wikilink">上田正昭</a></p></td>
<td></td>
<td><p>史學家</p></td>
</tr>
<tr class="even">
<td><p>1998</p></td>
<td><p><a href="../Page/Stanley_Jeyaraja_Tambiah.md" title="wikilink">Stanley Jeyaraja Tambiah</a></p></td>
<td></td>
<td><p>人類學家</p></td>
</tr>
<tr class="odd">
<td><p>1999</p></td>
<td><p><a href="../Page/大林太良.md" title="wikilink">大林太良</a></p></td>
<td></td>
<td><p>民族學家</p></td>
</tr>
<tr class="even">
<td><p>1999</p></td>
<td><p><a href="../Page/Nidhi_Eoseewong.md" title="wikilink">Nidhi Eoseewong</a></p></td>
<td></td>
<td><p>史學家</p></td>
</tr>
<tr class="odd">
<td><p>2000</p></td>
<td><p><a href="../Page/本尼迪克特·安德森.md" title="wikilink">本尼迪克特·安德森</a></p></td>
<td></td>
<td><p>政治學家</p></td>
</tr>
<tr class="even">
<td><p>2000</p></td>
<td><p><a href="../Page/Than_Tun.md" title="wikilink">Than Tun</a></p></td>
<td></td>
<td><p>史學家</p></td>
</tr>
<tr class="odd">
<td><p>2001</p></td>
<td><p><a href="../Page/速水佑次郎.md" title="wikilink">速水佑次郎</a></p></td>
<td></td>
<td><p>經濟學家</p></td>
</tr>
<tr class="even">
<td><p>2002</p></td>
<td><p><a href="../Page/Anthony_Reid.md" title="wikilink">Anthony Reid</a></p></td>
<td></td>
<td><p>史學家</p></td>
</tr>
<tr class="odd">
<td><p>2002</p></td>
<td><p><a href="../Page/Kingsley_Muthumuni_de_Silva.md" title="wikilink">Kingsley Muthumuni de Silva</a></p></td>
<td></td>
<td><p>史學家</p></td>
</tr>
<tr class="even">
<td><p>2003</p></td>
<td><p><a href="../Page/雷納爾多·伊萊托.md" title="wikilink">雷納爾多·伊萊托</a></p></td>
<td></td>
<td><p>史學家</p></td>
</tr>
<tr class="odd">
<td><p>2004</p></td>
<td><p><a href="../Page/Ram_Dayal_Rakesh.md" title="wikilink">Ram Dayal Rakesh</a></p></td>
<td></td>
<td><p>民俗學家</p></td>
</tr>
<tr class="even">
<td><p>2004</p></td>
<td><p><a href="../Page/厲以寧.md" title="wikilink">厲以寧</a></p></td>
<td></td>
<td><p>經濟學家</p></td>
</tr>
<tr class="odd">
<td><p>2005</p></td>
<td><p><a href="../Page/Thaw_Kaung.md" title="wikilink">Thaw Kaung</a></p></td>
<td></td>
<td><p>圖書館學家</p></td>
</tr>
<tr class="even">
<td><p>2006</p></td>
<td><p><a href="../Page/濱下武志.md" title="wikilink">濱下武志</a></p></td>
<td></td>
<td><p>史學家</p></td>
</tr>
<tr class="odd">
<td><p>2006</p></td>
<td><p><a href="../Page/Shagdaryn_Bira.md" title="wikilink">Shagdaryn Bira</a></p></td>
<td></td>
<td><p>史學家</p></td>
</tr>
<tr class="even">
<td><p>2007</p></td>
<td><p><a href="../Page/Srisakra_Vallibhotama.md" title="wikilink">Srisakra Vallibhotama</a></p></td>
<td></td>
<td><p>人類學、考古學家</p></td>
</tr>
<tr class="odd">
<td><p>2008</p></td>
<td></td>
<td></td>
<td><p>社會人類學家</p></td>
</tr>
<tr class="even">
<td><p>2008</p></td>
<td><p><a href="../Page/Savitri_Goonesekere.md" title="wikilink">Savitri Goonesekere</a></p></td>
<td></td>
<td><p>法學家</p></td>
</tr>
<tr class="odd">
<td><p>2009</p></td>
<td><p><a href="../Page/帕沙·查特吉.md" title="wikilink">帕沙·查特吉</a></p></td>
<td></td>
<td><p>政治學、史學家</p></td>
</tr>
<tr class="even">
<td><p>2010</p></td>
<td><p><a href="../Page/毛里和子.md" title="wikilink">毛里和子</a></p></td>
<td></td>
<td><p>現代中國研究家</p></td>
</tr>
<tr class="odd">
<td><p>2010</p></td>
<td><p><a href="../Page/James_C._Scott.md" title="wikilink">James C. Scott</a></p></td>
<td></td>
<td><p>政治學家、人類學家</p></td>
</tr>
<tr class="even">
<td><p>2011</p></td>
<td><p><a href="../Page/趙東一.md" title="wikilink">趙東一</a></p></td>
<td></td>
<td><p>文學研究者</p></td>
</tr>
<tr class="odd">
<td><p>2012</p></td>
<td><p><a href="../Page/Charnvit_Kasetsiri.md" title="wikilink">Charnvit Kasetsiri</a></p></td>
<td></td>
<td><p>|史學家</p></td>
</tr>
<tr class="even">
<td><p>2013</p></td>
<td><p><a href="../Page/Tessa_Morris-Suzuki.md" title="wikilink">Tessa Morris-Suzuki</a></p></td>
<td></td>
<td><p>亞洲研究家</p></td>
</tr>
<tr class="odd">
<td><p>2014</p></td>
<td><p><a href="../Page/Azyumardi_Azra.md" title="wikilink">Azyumardi Azra</a></p></td>
<td></td>
<td><p>史學家</p></td>
</tr>
<tr class="even">
<td><p>2015</p></td>
<td><p><a href="../Page/拉玛昌德拉·古哈.md" title="wikilink">拉玛昌德拉·古哈</a></p></td>
<td></td>
<td><p>历史学家・社会学家</p></td>
</tr>
<tr class="odd">
<td><p>2016</p></td>
<td><p><a href="../Page/安娜贝斯·R·奥坎波.md" title="wikilink">安娜贝斯·R·奥坎波</a></p></td>
<td></td>
<td><p>历史学家</p></td>
</tr>
</tbody>
</table>

### 藝術·文化獎

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>得主</p></th>
<th><p>國籍</p></th>
<th><p>事蹟</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1991</p></td>
<td><p><a href="../Page/唐纳德·基恩.md" title="wikilink">唐纳德·基恩</a></p></td>
<td></td>
<td><p>日本文學研究者</p></td>
</tr>
<tr class="even">
<td><p>1992</p></td>
<td><p><a href="../Page/Leandro_V._Locsin.md" title="wikilink">Leandro V. Locsin</a></p></td>
<td></td>
<td><p>建築師</p></td>
</tr>
<tr class="odd">
<td><p>1993</p></td>
<td><p><a href="../Page/Namjilyn_Norovbanzad.md" title="wikilink">Namjilyn Norovbanzad</a></p></td>
<td></td>
<td><p>歌唱家</p></td>
</tr>
<tr class="even">
<td><p>1994</p></td>
<td><p><a href="../Page/Padma_Subrahmanyam.md" title="wikilink">Padma Subrahmanyam</a></p></td>
<td></td>
<td><p>舞蹈家</p></td>
</tr>
<tr class="odd">
<td><p>1995</p></td>
<td><p><a href="../Page/白南准.md" title="wikilink">白南准</a></p></td>
<td></td>
<td><p>影像藝術家</p></td>
</tr>
<tr class="even">
<td><p>1996</p></td>
<td><p><a href="../Page/Nusrat_Fateh_Ali_Khan.md" title="wikilink">Nusrat Fateh Ali Khan</a></p></td>
<td></td>
<td><p>歌唱家</p></td>
</tr>
<tr class="odd">
<td><p>1997</p></td>
<td><p><a href="../Page/林權澤.md" title="wikilink">林權澤</a></p></td>
<td></td>
<td><p>導演</p></td>
</tr>
<tr class="even">
<td><p>1998</p></td>
<td><p><a href="../Page/R._M._Soedarsono.md" title="wikilink">R. M. Soedarsono</a></p></td>
<td></td>
<td><p>舞蹈家</p></td>
</tr>
<tr class="odd">
<td><p>1999</p></td>
<td><p><a href="../Page/唐大霧.md" title="wikilink">唐大霧</a></p></td>
<td></td>
<td><p>視覺藝術家</p></td>
</tr>
<tr class="even">
<td><p>2000</p></td>
<td></td>
<td></td>
<td><p>皮影戲藝師</p></td>
</tr>
<tr class="odd">
<td><p>2001</p></td>
<td><p><a href="../Page/Marilou_Díaz-Abaya.md" title="wikilink">Marilou Díaz-Abaya</a></p></td>
<td></td>
<td><p>導演</p></td>
</tr>
<tr class="even">
<td><p>2001</p></td>
<td><p><a href="../Page/Thawan_Duchanee.md" title="wikilink">Thawan Duchanee</a></p></td>
<td></td>
<td><p>畫家</p></td>
</tr>
<tr class="odd">
<td><p>2002</p></td>
<td></td>
<td></td>
<td><p>漫畫家</p></td>
</tr>
<tr class="even">
<td><p>2003</p></td>
<td><p><a href="../Page/李迪文.md" title="wikilink">李迪文</a></p></td>
<td></td>
<td><p>音樂家</p></td>
</tr>
<tr class="odd">
<td><p>2003</p></td>
<td><p><a href="../Page/徐冰.md" title="wikilink">徐冰</a></p></td>
<td></td>
<td><p>裝置藝術家</p></td>
</tr>
<tr class="even">
<td><p>2004</p></td>
<td><p><a href="../Page/Sembukuttiarachilage_Roland_Silva.md" title="wikilink">Sembukuttiarachilage Roland Silva</a></p></td>
<td></td>
<td><p>古蹟保存專家</p></td>
</tr>
<tr class="odd">
<td><p>2005</p></td>
<td><p><a href="../Page/Tashi_Norbu.md" title="wikilink">Tashi Norbu</a></p></td>
<td></td>
<td><p>音樂家</p></td>
</tr>
<tr class="even">
<td><p>2005</p></td>
<td><p><a href="../Page/Douangdeuane_Bounyavong.md" title="wikilink">Douangdeuane Bounyavong</a></p></td>
<td></td>
<td><p>紡織品研究家</p></td>
</tr>
<tr class="odd">
<td><p>2006</p></td>
<td><p><a href="../Page/Uxi_Mufti.md" title="wikilink">Uxi Mufti</a></p></td>
<td></td>
<td><p>民俗文化保存專家</p></td>
</tr>
<tr class="even">
<td><p>2007</p></td>
<td><p><a href="../Page/金德洙.md" title="wikilink">金德洙</a></p></td>
<td></td>
<td><p>傳統藝術家</p></td>
</tr>
<tr class="odd">
<td><p>2007</p></td>
<td><p><a href="../Page/朱銘.md" title="wikilink">朱銘</a></p></td>
<td></td>
<td><p>雕刻家</p></td>
</tr>
<tr class="even">
<td><p>2008</p></td>
<td><p><a href="../Page/Farida_Parveen.md" title="wikilink">Farida Parveen</a></p></td>
<td></td>
<td><p>歌唱家</p></td>
</tr>
<tr class="odd">
<td><p>2009</p></td>
<td><p><a href="../Page/蔡國強.md" title="wikilink">蔡國強</a></p></td>
<td></td>
<td><p>煙火藝術家</p></td>
</tr>
<tr class="even">
<td><p>2009</p></td>
<td><p><a href="../Page/三木稔.md" title="wikilink">三木稔</a></p></td>
<td></td>
<td><p>音樂家</p></td>
</tr>
<tr class="odd">
<td><p>2010</p></td>
<td><p><a href="../Page/王景生.md" title="wikilink">王景生</a></p></td>
<td></td>
<td><p>表演藝術家</p></td>
</tr>
<tr class="even">
<td><p>2011</p></td>
<td><p><a href="../Page/Niels_Gutschow.md" title="wikilink">Niels Gutschow</a></p></td>
<td></td>
<td><p>建築史家、古蹟修復專家</p></td>
</tr>
<tr class="odd">
<td><p>2012</p></td>
<td><p><a href="../Page/G.R.Ay._Koes_Murtiyah_Paku_Buwono.md" title="wikilink">G.R.Ay. Koes Murtiyah Paku Buwono</a></p></td>
<td></td>
<td><p>|舞蹈家</p></td>
</tr>
<tr class="even">
<td><p>2012</p></td>
<td><p><a href="../Page/Kidlat_Tahimik.md" title="wikilink">Kidlat Tahimik</a></p></td>
<td></td>
<td><p>|導演、表演藝術家</p></td>
</tr>
<tr class="odd">
<td><p>2013</p></td>
<td><p><a href="../Page/阿比查邦·魏拉希沙可.md" title="wikilink">阿比查邦·魏拉希沙可</a></p></td>
<td></td>
<td><p>導演</p></td>
</tr>
<tr class="even">
<td><p>2013</p></td>
<td><p><a href="../Page/Nalini_Malani.md" title="wikilink">Nalini Malani</a></p></td>
<td></td>
<td><p>裝置藝術家</p></td>
</tr>
<tr class="odd">
<td><p>2014</p></td>
<td><p><a href="../Page/榮念曾.md" title="wikilink">榮念曾</a></p></td>
<td></td>
<td><p>表演藝術家、裝置藝術家</p></td>
</tr>
<tr class="even">
<td><p>2015</p></td>
<td><p><a href="../Page/明兴.md" title="wikilink">明兴</a></p></td>
<td></td>
<td><p>时装设计师</p></td>
</tr>
<tr class="odd">
<td><p>2016</p></td>
<td><p><a href="../Page/亚斯敏·拉里.md" title="wikilink">亚斯敏·拉里</a></p></td>
<td></td>
<td><p>建築家</p></td>
</tr>
</tbody>
</table>

## 外部链接

  - [福冈亚洲文化奖網站（中文版）](http://fukuoka-prize.org/cn/)

[Category:福冈亚洲文化奖](../Category/福冈亚洲文化奖.md "wikilink")
[Category:1990年建立](../Category/1990年建立.md "wikilink")