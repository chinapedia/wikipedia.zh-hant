**杨梅**（[学名](../Page/学名.md "wikilink")：**），又稱為**樹梅**，[杨梅科](../Page/杨梅科.md "wikilink")[杨梅属](../Page/杨梅属.md "wikilink")[喬木植物](../Page/喬木.md "wikilink")，亦是[水果树种](../Page/水果.md "wikilink")。喜湿，耐阴寒，生長於[溫帶](../Page/溫帶.md "wikilink")、[亚热带](../Page/亚热带.md "wikilink")。\[1\]

## 分布

产于[中国大陸西南](../Page/中国西南地区.md "wikilink")、[华东](../Page/华东地区.md "wikilink")、[台灣](../Page/臺灣.md "wikilink")、[日本](../Page/日本.md "wikilink")、[朝鲜](../Page/朝鲜.md "wikilink")、[韓國](../Page/韓國.md "wikilink")、[菲律宾等地](../Page/菲律宾.md "wikilink")\[2\]\[3\]。

产于廣東[汕头市](../Page/汕头市.md "wikilink")[潮阳区](../Page/潮阳区.md "wikilink")[西胪镇的](../Page/西胪镇.md "wikilink")[西胪乌酥杨梅为中国优稀品种](../Page/西胪乌酥杨梅.md "wikilink")，是潮阳杨梅的代表。产于[广西](../Page/广西壮族自治区.md "wikilink")[贺州市](../Page/贺州市.md "wikilink")[昭平县](../Page/昭平县.md "wikilink")[黄姚镇的杨梅](../Page/黄姚镇.md "wikilink")，每年5至6月份就是新鲜上市的时间，可以加工製作成杨梅干、杨梅酒等。

台灣原生種有銳葉楊梅（Myrica rubra (Lour.) S. et
Zucc.）及[固有種恆春楊梅](../Page/特有種.md "wikilink")（Myrica
adenophora Hance）。

## 產期

一般在4月中下旬会剪枝剪果。[小满前后当季](../Page/小满.md "wikilink")，一般在[端午节後过季](../Page/端午节.md "wikilink")。

以台灣為例，每年則在5月底至6月，產期稍短。\[4\]

## 特性

### 營養成分

其樹葉、根及枝幹表皮，富含[單寧](../Page/單寧.md "wikilink")，可提煉[黃酮類和香精油](../Page/黃酮類.md "wikilink")。\[5\]

其果仁，含[維生素B17](../Page/維生素B17.md "wikilink")，具高蛋白、高油脂。\[6\]

其果實，含[維生素C](../Page/維生素C.md "wikilink")、有機酸。\[7\]

### 药用

《[本草纲目](../Page/本草纲目.md "wikilink")》记载：“鹽藏食，去痰止嘔噦，消食下酒。乾作屑，臨飲酒時服方寸匕，止吐酒。止渴，和五臟，能滌腸胃，除煩憒惡氣。燒灰服，斷下痢，甚驗。鹽者常含一枚，咽汁，利五臟下氣。”\[8\]

## 形态

[常绿](../Page/常绿.md "wikilink")[乔木](../Page/乔木.md "wikilink")；树皮灰色；叶革质，倒披针形或倒卵状椭圆形，全缘或先端稍有钝锯齿；雌雄异株，雄花序圆柱形，红黄色葇荑[花序](../Page/花序.md "wikilink")，雌花序卵状长椭圆形，穗状花序；2月开花结果，5月成熟，球形核果状果实，密生多数囊状体，果子有[红](../Page/红色.md "wikilink")、[白](../Page/白色.md "wikilink")、[紫三色](../Page/紫色.md "wikilink")，根据不同品种有不同种类的上品杨梅，一般鲜红色的比较酸甜，最为成熟的标志是紫红色。

## 歷史

楊梅最早的人工栽培，在中國[西漢](../Page/西汉.md "wikilink")\[9\]。

## 文化

李白的詩多次提及楊梅。

[臺灣](../Page/臺灣.md "wikilink")[楊梅區舊名](../Page/楊梅區.md "wikilink")「楊梅壢」，「壢」在[客家語裡是](../Page/客家語.md "wikilink")「坑谷」的意思，[清代](../Page/台灣清治時期.md "wikilink")[客家人先後移民到這裡](../Page/客家人.md "wikilink")，看到滿山遍野都是野生的楊梅樹，所以把這裡稱為楊梅壢，而楊梅樹因此也理所當然成為楊梅區的「區樹」。

## 参考文献

## 外部連結

  -
  - [楊梅
    Yangmei](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D00843)
    藥用植物圖像資料庫 (香港浸會大學中醫藥學院)

  - [葉綠醇
    Phytol](http://libproject.hkbu.edu.hk/was40/detail?channelid=22253&lang=cht&searchword=code=P00619)
    中草藥化學圖像數據庫 (香港浸會大學中醫藥學院)

[樹](../Category/藥用植物.md "wikilink")
[Category:杨梅科](../Category/杨梅科.md "wikilink")
[Category:水果](../Category/水果.md "wikilink")

1.
2.
3.

4.
5.
6.
7.
8.

9.