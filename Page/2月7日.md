**2月7日**是[阳历年的第](../Page/阳历.md "wikilink")38天，离一年的结束还有327天（[闰年是](../Page/闰年.md "wikilink")328天）。

## 大事记

### 5世紀

  - [457年](../Page/457年.md "wikilink")：[君士坦丁堡长老为](../Page/君士坦丁堡.md "wikilink")[利奥一世加冕为](../Page/利奥一世_\(拜占庭\).md "wikilink")[拜占庭皇帝](../Page/拜占庭皇帝列表.md "wikilink")，之后统治拜占庭帝国长达20年。

### 18世紀

  - [1795年](../Page/1795年.md "wikilink")：[美利坚合众国宪法第十一条修正案获得批准通过](../Page/美利坚合众国宪法第十一条修正案.md "wikilink")，并且推翻[美国最高法院对](../Page/美国最高法院.md "wikilink")[奇泽姆诉佐治亚州案的判决结果](../Page/奇泽姆诉佐治亚州案.md "wikilink")。

### 19世紀

  - [1807年](../Page/1807年.md "wikilink")：[拿破仑率领的](../Page/拿破仑·波拿巴.md "wikilink")[法国军队和](../Page/法国.md "wikilink")[俄国](../Page/俄国.md "wikilink")、[普鲁士等国组成的](../Page/普鲁士.md "wikilink")[第四次反法同盟在](../Page/第四次反法同盟.md "wikilink")[东普鲁士相遇](../Page/东普鲁士.md "wikilink")，[埃劳战役爆发](../Page/埃劳战役.md "wikilink")。

### 20世紀

  - [1914年](../Page/1914年.md "wikilink")：[北京政府统一币制](../Page/北京政府.md "wikilink")。
  - 1914年：英国喜剧演员[查理·卓别林在电影](../Page/查理·卓别林.md "wikilink")《[威尼斯儿童赛车](../Page/威尼斯儿童赛车.md "wikilink")》中首次饰演著名的流浪汉角色。
  - [1923年](../Page/1923年.md "wikilink")：[河南](../Page/河南.md "wikilink")[鄭州](../Page/鄭州.md "wikilink")[京汉铁路工人](../Page/京汉铁路.md "wikilink")[大罢工](../Page/二七大罢工.md "wikilink")。
  - [1939年](../Page/1939年.md "wikilink")：[中国国民党成立国防最高委员会](../Page/中国国民党.md "wikilink")，[蒋介石为委员长](../Page/蒋介石.md "wikilink")。
  - [1955年](../Page/1955年.md "wikilink")：[中华人民共和国全国人民代表大会常务委员会改](../Page/中华人民共和国全国人民代表大会常务委员会.md "wikilink")[志愿兵役制为](../Page/志愿兵役制.md "wikilink")[义务兵役制](../Page/义务兵役制.md "wikilink")。
  - [1957年](../Page/1957年.md "wikilink")：[中华人民共和国与](../Page/中华人民共和国.md "wikilink")[斯里兰卡建交](../Page/斯里兰卡.md "wikilink")。
  - [1959年](../Page/1959年.md "wikilink")：《[中苏经济合作协定](../Page/中苏经济合作协定.md "wikilink")》在[蘇聯](../Page/蘇聯.md "wikilink")[莫斯科签订](../Page/莫斯科.md "wikilink")。
  - [1964年](../Page/1964年.md "wikilink")：[披頭四樂隊抵達](../Page/披頭四樂隊.md "wikilink")[紐約作](../Page/紐約市.md "wikilink")[美國全國巡迴演唱](../Page/美國.md "wikilink")。
  - [1974年](../Page/1974年.md "wikilink")：[中美洲島國](../Page/中美洲.md "wikilink")[格瑞那達脫離](../Page/格瑞那達.md "wikilink")[英國](../Page/英國.md "wikilink")，獨立建國。
  - [1977年](../Page/1977年.md "wikilink")：在[中国共产党中央委员会主席](../Page/中国共产党中央委员会主席.md "wikilink")[华国锋的授意下](../Page/华国锋.md "wikilink")，《[人民日报](../Page/人民日报.md "wikilink")》发表社论，公开提出[两个凡是](../Page/两个凡是.md "wikilink")。
  - [1979年](../Page/1979年.md "wikilink")：運載2600多名[越南船民的貨輪](../Page/越南船民.md "wikilink")「天運號」駛入[香港](../Page/香港.md "wikilink")，在[南丫島水域停泊](../Page/南丫島.md "wikilink")。
  - [1981年](../Page/1981年.md "wikilink")：[日本政府确定本日为](../Page/日本.md "wikilink")“[北方领土日](../Page/北方四島.md "wikilink")”。
  - [1982年](../Page/1982年.md "wikilink")：[国际奥委会医学委员会公布第一批禁止使用的](../Page/国际奥委会医学委员会.md "wikilink")[兴奋剂名单](../Page/兴奋剂.md "wikilink")，正式向体育毒瘤动手。
  - [1984年](../Page/1984年.md "wikilink")：美国[宇航员](../Page/宇航员.md "wikilink")[麦克坎德雷斯和](../Page/布鲁斯·麦克坎德雷斯.md "wikilink")[斯图尔特离开](../Page/罗伯特·斯图尔特.md "wikilink")[挑战者号](../Page/挑戰者號太空梭.md "wikilink")[航天飞机](../Page/航天飞机.md "wikilink")，完成人类第一次不繫繩的[太空行走](../Page/太空行走.md "wikilink")。
  - [1986年](../Page/1986年.md "wikilink")：[海地总统](../Page/海地总统.md "wikilink")[让·克劳德·杜瓦利埃下台](../Page/让·克劳德·杜瓦利埃.md "wikilink")。
  - [1992年](../Page/1992年.md "wikilink")：[欧洲共同体](../Page/欧洲共同体.md "wikilink")12国代表在[荷兰签订旨在建立](../Page/荷兰.md "wikilink")[欧洲联盟的](../Page/欧洲联盟.md "wikilink")《[马斯特里赫特条约](../Page/马斯特里赫特条约.md "wikilink")》。
  - [1996年](../Page/1996年.md "wikilink")：[波兰总理](../Page/波兰总理.md "wikilink")因被指控为[莫斯科间谍而辞职](../Page/莫斯科.md "wikilink")。
  - [1997年](../Page/1997年.md "wikilink")：[NeXT與](../Page/NeXT.md "wikilink")[蘋果公司合併](../Page/蘋果公司.md "wikilink")，導致其公司老闆[喬布斯回歸至蘋果公司並取代阿梅里奧](../Page/喬布斯.md "wikilink")。
  - [1998年](../Page/1998年.md "wikilink")：[第18屆冬季奧運會在](../Page/第18屆冬季奧運會.md "wikilink")[日本](../Page/日本.md "wikilink")[長野開幕](../Page/長野.md "wikilink")。
  - [1999年](../Page/1999年.md "wikilink")：在[侯赛因·本·塔拉勒逝世后](../Page/侯赛因·本·塔拉勒.md "wikilink")，其儿子[阿卜杜拉二世继任成为约旦](../Page/阿卜杜拉二世.md "wikilink")[哈希姆王国](../Page/哈希姆家族.md "wikilink")[国王](../Page/约旦君主列表.md "wikilink")。

### 21世紀

  - [2009年](../Page/2009年.md "wikilink")：[馬達加斯加反對派領導人](../Page/馬達加斯加.md "wikilink")[拉喬利納率領的反政府示威者在首都](../Page/安德里·拉乔利纳.md "wikilink")[塔那那利佛遭到忠於總統](../Page/塔那那利佛.md "wikilink")[拉瓦盧馬納納的安全部隊的開火](../Page/馬克·拉瓦盧馬納納.md "wikilink")，導致至少30人死亡。
  - [2014年](../Page/2014年.md "wikilink")：[第22屆冬季奧運在](../Page/2014年冬季奧林匹克運動會.md "wikilink")[俄羅斯](../Page/俄羅斯.md "wikilink")[索契開幕](../Page/索契.md "wikilink")\[1\]。

## 出生

  - [1478年](../Page/1478年.md "wikilink")：[-{zh-hant:湯瑪斯·摩爾;
    zh-hans:托马斯·莫尔;}-](../Page/托马斯·莫尔.md "wikilink")，[英國哲學家](../Page/英國.md "wikilink")（逝於[1535年](../Page/1535年.md "wikilink")）
  - [1812年](../Page/1812年.md "wikilink")：[查尔斯·狄更斯](../Page/查尔斯·狄更斯.md "wikilink")，英國作家（逝於[1870年](../Page/1870年.md "wikilink")）
  - [1867年](../Page/1867年.md "wikilink")：[劳拉·英格斯·怀德](../Page/劳拉·英格斯·怀德.md "wikilink")，[美国作家](../Page/美国.md "wikilink")（逝於[1957年](../Page/1957年.md "wikilink")）
  - [1873年](../Page/1873年.md "wikilink")：[托马斯·安德鲁斯](../Page/托马斯·安德鲁斯.md "wikilink")，英國造船家
    （逝於[1912年](../Page/1912年.md "wikilink")）
  - [1885年](../Page/1885年.md "wikilink")：[辛克莱·刘易斯](../Page/辛克莱·刘易斯.md "wikilink")，
    英國作家（逝於[1951年](../Page/1951年.md "wikilink")）
  - [1906年](../Page/1906年.md "wikilink")：[溥仪](../Page/溥仪.md "wikilink")，中国[清朝末代皇帝](../Page/清朝.md "wikilink")（逝於[1967年](../Page/1967年.md "wikilink")）
  - [1910年](../Page/1910年.md "wikilink")：[夏鼐](../Page/夏鼐.md "wikilink")，中国考古学家（逝於[1985年](../Page/1985年.md "wikilink")）
  - [1929年](../Page/1929年.md "wikilink")：[薩爾塔傑·阿齊茲](../Page/薩爾塔傑·阿齊茲.md "wikilink")，[巴基斯坦政治人物和經濟學家](../Page/巴基斯坦.md "wikilink")
  - [1934年](../Page/1934年.md "wikilink")：[金美齡](../Page/金美齡.md "wikilink")，[台灣獨立運動成員](../Page/台灣獨立運動.md "wikilink")
  - [1952年](../Page/1952年.md "wikilink")：[劉永](../Page/劉永_\(演員\).md "wikilink")，香港演員
  - [1966年](../Page/1966年.md "wikilink")：[張世](../Page/張世.md "wikilink")，台灣演員
  - [1966年](../Page/1966年.md "wikilink")：[陶君行](../Page/陶君行.md "wikilink")，[香港政界人物](../Page/香港.md "wikilink")
  - [1968年](../Page/1968年.md "wikilink")：[張敏](../Page/張敏_\(香港演員\).md "wikilink")，香港女演員
  - [1968年](../Page/1968年.md "wikilink")：[洪莉娜](../Page/洪莉娜.md "wikilink")，韓國女演員
  - [1970年](../Page/1970年.md "wikilink")：[張宏艷](../Page/張宏艷.md "wikilink")，[香港女主播](../Page/香港.md "wikilink")
  - [1973年](../Page/1973年.md "wikilink")：[園崎未惠](../Page/園崎未惠.md "wikilink")，[日本女性](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")
  - [1974年](../Page/1974年.md "wikilink")：[史蒂夫·纳什](../Page/史蒂夫·纳什.md "wikilink")，[加拿大篮球运动员](../Page/加拿大.md "wikilink")
  - [1975年](../Page/1975年.md "wikilink")：[劉綽琪](../Page/劉綽琪.md "wikilink")，香港電視節目主持人、演員
  - [1979年](../Page/1979年.md "wikilink")：[小雪](../Page/小雪_\(香港\).md "wikilink")，香港歌手、演員
  - [1980年](../Page/1980年.md "wikilink")：[李貞賢](../Page/李貞賢.md "wikilink")，[韓國女演員](../Page/韓國.md "wikilink")
  - [1982年](../Page/1982年.md "wikilink")：[向井理](../Page/向井理.md "wikilink")，[日本男演員](../Page/日本.md "wikilink")
  - [1983年](../Page/1983年.md "wikilink")：[克里斯蒂安·克莱恩](../Page/克里斯蒂安·克莱恩.md "wikilink")，[奧地利](../Page/奧地利.md "wikilink")[一級方程式賽車運動員](../Page/一級方程式賽車.md "wikilink")
  - [1988年](../Page/1988年.md "wikilink")：[加護亞依](../Page/加護亞依.md "wikilink")，[日本女歌手](../Page/日本.md "wikilink")，前[早安少女組成員](../Page/早安少女組.md "wikilink")
  - [1990年](../Page/1990年.md "wikilink")：[華晨宇](../Page/華晨宇.md "wikilink")，[中国男歌手](../Page/中国.md "wikilink")
  - [1990年](../Page/1990年.md "wikilink")：[Jacksepticeye](../Page/Jacksepticeye.md "wikilink")，[愛爾蘭](../Page/愛爾蘭.md "wikilink")[Youtuber](../Page/Youtuber.md "wikilink")
  - [1992年](../Page/1992年.md "wikilink")：[矢島舞美](../Page/矢島舞美.md "wikilink")，[日本女性組合](../Page/日本.md "wikilink")[℃-ute隊長](../Page/℃-ute.md "wikilink")
  - [1993年](../Page/1993年.md "wikilink")：[斯里坎特·基達姆比](../Page/斯里坎特·基達姆比.md "wikilink")，[印度羽球選手](../Page/印度.md "wikilink")
  - [1994年](../Page/1994年.md "wikilink")：[金振煥](../Page/金振煥.md "wikilink")，[韓國男子偶像團體](../Page/韓國.md "wikilink")[iKON成員](../Page/iKON.md "wikilink")
  - [1995年](../Page/1995年.md "wikilink")：[何紫慧](../Page/何紫慧.md "wikilink")，[香港女歌手](../Page/香港.md "wikilink")
  - [1996年](../Page/1996年.md "wikilink")：[伊波杏樹](../Page/伊波杏樹.md "wikilink")，[日本女](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")
  - [1996年](../Page/1996年.md "wikilink")：[萩原舞](../Page/萩原舞_\(歌手\).md "wikilink")，[日本女性組合](../Page/日本.md "wikilink")[℃-ute成員](../Page/℃-ute.md "wikilink")
  - [1996年](../Page/1996年.md "wikilink")：[皮埃爾·蓋斯利](../Page/皮埃爾·蓋斯利.md "wikilink")，[法國](../Page/法國.md "wikilink")[一級方程式賽車車手](../Page/一級方程式賽車.md "wikilink")
  - [1997年](../Page/1997年.md "wikilink")：[朴昭英](../Page/朴昭英.md "wikilink")，[韓國女演員](../Page/韓國.md "wikilink")
  - [季遊月聰子](../Page/季遊月聰子.md "wikilink")，[日本女性](../Page/日本.md "wikilink")[漫畫家](../Page/漫畫家.md "wikilink")
  - [雷碧文](../Page/雷碧文.md "wikilink")，[台灣女性配音員](../Page/台灣.md "wikilink")

## 逝世

  - [199年](../Page/199年.md "wikilink")：[呂布](../Page/呂布.md "wikilink")，[三國時期名將](../Page/三國.md "wikilink")
  - [318年](../Page/318年.md "wikilink")：晉愍帝[司馬鄴](../Page/司馬鄴.md "wikilink")，字彥旗，[西晉的第四任也是最後一任皇帝](../Page/西晉.md "wikilink")。（生于[300年](../Page/300年.md "wikilink")）
  - [1799年](../Page/1799年.md "wikilink")：[愛新覺羅弘曆](../Page/愛新覺羅弘曆.md "wikilink")，[清朝](../Page/清朝.md "wikilink")[乾隆皇帝](../Page/乾隆帝.md "wikilink")（生于[1711年](../Page/1711年.md "wikilink")）
  - [1931年](../Page/1931年.md "wikilink")：[柔石](../Page/柔石.md "wikilink")（生于[1902年](../Page/1902年.md "wikilink")）、[胡也频](../Page/胡也频.md "wikilink")（生于[1903年](../Page/1903年.md "wikilink")）、[李伟森](../Page/李伟森.md "wikilink")（生于[1903年](../Page/1903年.md "wikilink")）、[冯铿](../Page/冯铿.md "wikilink")（生于[1907年](../Page/1907年.md "wikilink")）、[殷夫](../Page/殷夫.md "wikilink")（生于[1931年](../Page/1931年.md "wikilink")），[左联五烈士](../Page/左联五烈士.md "wikilink")
  - [1944年](../Page/1944年.md "wikilink")：[马本斋](../Page/马本斋.md "wikilink")，[八路军将领](../Page/八路军.md "wikilink")（生于[1902年](../Page/1902年.md "wikilink")）
  - [1965年](../Page/1965年.md "wikilink")：[李海泉](../Page/李海泉.md "wikilink")，[香港](../Page/香港.md "wikilink")[粵劇演員](../Page/粵劇.md "wikilink")，[李小龍之父](../Page/李小龍.md "wikilink")（生于[1901年](../Page/1901年.md "wikilink")）
  - [1974年](../Page/1974年.md "wikilink")：[竺可桢](../Page/竺可桢.md "wikilink")，[中國地理学家](../Page/中國.md "wikilink")（生于[1890年](../Page/1890年.md "wikilink")）
  - [1979年](../Page/1979年.md "wikilink")：[蘇振華](../Page/蘇振華.md "wikilink")，[中國共產黨中央政治局委員](../Page/中國共產黨中央政治局.md "wikilink")、[中國人民解放軍上將](../Page/中國人民解放軍上將.md "wikilink")（生于[1912年](../Page/1912年.md "wikilink")）
  - [1999年](../Page/1999年.md "wikilink")：[胡笙·賓·塔拉勒](../Page/胡笙·賓·塔拉勒.md "wikilink")，[约旦国王](../Page/约旦国王.md "wikilink")（生于[1935年](../Page/1935年.md "wikilink")）
  - [2007年](../Page/2007年.md "wikilink")：[陳煒恆](../Page/陳煒恆.md "wikilink")，[澳門文化界人士](../Page/澳門.md "wikilink")（生于[1962年](../Page/1962年.md "wikilink")）
  - [2011年](../Page/2011年.md "wikilink")：[陳蕉琴](../Page/陳蕉琴.md "wikilink")，[香港](../Page/香港.md "wikilink")[女性](../Page/女性.md "wikilink")[企業家和](../Page/企業家.md "wikilink")[慈善家](../Page/慈善家.md "wikilink")（生於[1915年](../Page/1915年.md "wikilink")）
  - [2013年](../Page/2013年.md "wikilink")：[李可](../Page/李可_\(中醫\).md "wikilink")，中國著名[中醫學家](../Page/中醫學.md "wikilink")（生於[1930年](../Page/1930年.md "wikilink")）\[2\]\[3\]

## 节假日和习俗

  - 2016年：[除夕](../Page/除夕.md "wikilink")

## 參考資料

1.
2.
3.