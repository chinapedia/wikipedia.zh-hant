**沙特弗里西語** （Saterfriesische
Sprache），其原文是「Seeltersk」，是世界上[弗里西語語系](../Page/弗里西語.md "wikilink")[東弗里西語分支現今唯一遺留下的語言](../Page/東弗里西語.md "wikilink")，雖然他一直被視為是[弗里西語的方言](../Page/弗里西語.md "wikilink")，但其實他在[下薩克森的薩特地區所存在的時間](../Page/下薩克森.md "wikilink")，甚至比[弗里西語更久](../Page/弗里西語.md "wikilink")。
[PSCHADDE.jpg](https://zh.wikipedia.org/wiki/File:PSCHADDE.jpg "fig:PSCHADDE.jpg")

## 歷史簡介

沙特弗里西語緣起於中古世紀，若就語言學的特殊性而言，許多專家不將其視為弗里西語的方言，而是一種獨立的語言。

在[下薩克森邦](../Page/下薩克森.md "wikilink")[克洛彭堡縣的薩特區中預估約有](../Page/克洛彭堡縣.md "wikilink")1,000至2,500人使用這種語言，故被視為歐洲裡的小型[語言孤島](../Page/语言岛.md "wikilink")。

東弗里西蘭的居民由於受到其來源地天災的迫害，因此必須離開，轉而定居於薩特弗里西蘭地區，也就是今日Ramsloh (沙特弗里西語:
Roomelse)、Scharrel (Schäddel)、Sedelsberg (Sedelsbierich)、Strücklingen
(Strukelje)等地。

這個中古世紀的語言能流傳至今主要是其地理的特殊性所致，沙特弗里西語在沼澤區存在了大概一世紀之久，沼澤就像天然的屏障保護著村民。
一直到19世紀才能靠獨木舟渡河到達。

直至歐洲在1992年11月5日通過對於地方語言或少數語言的保護政策，在德國的沙特弗里西語才享有法律的支持與保護。在中古世紀時，這種語言在鄉鎮裡也是一種官方語言，而在學校與幼稚園中有將近300位小朋友出於自願學習此語言。從2004年起，沙特弗里西語在Ems-Vechte-Welle地方電台中，有爭取到一個播放時段的機會。

對這個語言而言，他並非只是ISO-639-3中的簡略符號，而是和其他[日爾曼語一樣可以被使用的語言](../Page/日爾曼語.md "wikilink")。

## 文獻參考

  - 取自維基百科薩特弗里西語德語網頁: <http://de.wikipedia.org/wiki/Saterfriesisch>

## 外部連結

  -
  - [On-line course in
    English](https://web.archive.org/web/20091212004758/http://www.allezhop.de/frysk/seelter/)

  - [Saterland Frisian Wikipedia](http://stq.wikipedia.org/) (Saterland
    Frisian)

[Category:西日耳曼語支](../Category/西日耳曼語支.md "wikilink")
[Category:德國語言](../Category/德國語言.md "wikilink")