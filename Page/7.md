**7**（七）是[6与](../Page/6.md "wikilink")[8之间的](../Page/8.md "wikilink")[自然数](../Page/自然数.md "wikilink")。

## 数学性质

  - 第2个[快樂數](../Page/快樂數.md "wikilink")
  - 999,999除以7刚好是[142,857](../Page/142857.md "wikilink")，以7为[分母的](../Page/分母.md "wikilink")[真分数的](../Page/真分数.md "wikilink")[循环节为该六个数字的不同顺序](../Page/循环节.md "wikilink")。
      - 1/7 = 0.142857142...
      - 2/7 = 0.285714285...
      - 3/7 = 0.428571428...
      - 4/7 = 0.571428571...
      - 5/7 = 0.714285714...
      - 6/7 = 0.857142857...
  - 若某數之末位的二倍與剩餘數字的差為7的[倍數](../Page/倍數.md "wikilink")，則某數為7的倍數。
      -
        例如：1022，102-2×2=98，9-8×2=-7，故1022為7的倍數。
  - [正三边形](../Page/正三角形.md "wikilink")、[正四边形](../Page/正四边形.md "wikilink")、[正五边形](../Page/正五边形.md "wikilink")、[正六边形均可以以](../Page/正六边形.md "wikilink")[尺规作图的方式画出](../Page/尺规作图.md "wikilink")，但[正七边形卻不可](../Page/正七边形.md "wikilink")。
  - [西爾維斯特數列的第](../Page/西爾維斯特數列.md "wikilink")3項
  - [Frieze群有七種](../Page/Frieze群.md "wikilink")
  - [7維空間是除了](../Page/7維空間.md "wikilink")[3維空間外](../Page/3維空間.md "wikilink")能定義[叉積的空間](../Page/叉積.md "wikilink")
  - [佩蘭數列的第](../Page/佩蘭數列.md "wikilink")8項

### 基本运算

| [乘法](../Page/乘法.md "wikilink") | 1     | 2                              | 3                              | 4                              | 5                              | 6                              | 7                              | 8                              | 9                              | 10                             |  | 11                             | 12                             | 13                             | 14                             | 15                               | 16                               | 17                               | 18                               | 19                               | 20                               |  | 21                               | 22                               | 23                               | 24                               | 25                               |  | 50                               | 100                              | 1000                               |
| ------------------------------ | ----- | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |  | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | -------------------------------- | -------------------------------- | -------------------------------- | -------------------------------- | -------------------------------- | -------------------------------- |  | -------------------------------- | -------------------------------- | -------------------------------- | -------------------------------- | -------------------------------- |  | -------------------------------- | -------------------------------- | ---------------------------------- |
| 7 × *x*                        | **7** | [14](../Page/14.md "wikilink") | [21](../Page/21.md "wikilink") | [28](../Page/28.md "wikilink") | [35](../Page/35.md "wikilink") | [42](../Page/42.md "wikilink") | [49](../Page/49.md "wikilink") | [56](../Page/56.md "wikilink") | [63](../Page/63.md "wikilink") | [70](../Page/70.md "wikilink") |  | [77](../Page/77.md "wikilink") | [84](../Page/84.md "wikilink") | [91](../Page/91.md "wikilink") | [98](../Page/98.md "wikilink") | [105](../Page/105.md "wikilink") | [112](../Page/112.md "wikilink") | [119](../Page/119.md "wikilink") | [126](../Page/126.md "wikilink") | [133](../Page/133.md "wikilink") | [140](../Page/140.md "wikilink") |  | [147](../Page/147.md "wikilink") | [154](../Page/154.md "wikilink") | [161](../Page/161.md "wikilink") | [168](../Page/168.md "wikilink") | [175](../Page/175.md "wikilink") |  | [350](../Page/350.md "wikilink") | [700](../Page/700.md "wikilink") | [7000](../Page/7000.md "wikilink") |

| [除法](../Page/除法.md "wikilink")                          | 1                                                       | 2                                                         | 3                                                       | 4                                                           | 5                                                       | 6                                                       | 7 | 8                                                       | 9                                                       | 10                                                      |
| ------------------------------------------------------- | ------------------------------------------------------- | --------------------------------------------------------- | ------------------------------------------------------- | ----------------------------------------------------------- | ------------------------------------------------------- | ------------------------------------------------------- | - | ------------------------------------------------------- | ------------------------------------------------------- | ------------------------------------------------------- |
| 11                                                      | 12                                                      | 13                                                        | 14                                                      | 15                                                          |                                                         |                                                         |   |                                                         |                                                         |                                                         |
| 7 ÷ *x*                                                 | **7**                                                   | [3](../Page/3.md "wikilink").[5](../Page/5.md "wikilink") | 2\.<span style="text-decoration:overline">3</span>      | [1](../Page/1.md "wikilink").[75](../Page/75.md "wikilink") | 1\.[4](../Page/4.md "wikilink")                         | 1.1<span style="text-decoration:overline">6</span>      | 1 | [0](../Page/0.md "wikilink").875                        | 0\.<span style="text-decoration:overline">7</span>      | 0.7                                                     |
| 0\.<span style="text-decoration:overline">63</span>     | 0.58<span style="text-decoration:overline">3</span>     | 0\.<span style="text-decoration:overline">538461</span>   | 0.5                                                     | 0.4<span style="text-decoration:overline">6</span>          |                                                         |                                                         |   |                                                         |                                                         |                                                         |
| *x* ÷ 7                                                 | 0\.<span style="text-decoration:overline">142857</span> | 0\.<span style="text-decoration:overline">285714</span>   | 0\.<span style="text-decoration:overline">428571</span> | 0\.<span style="text-decoration:overline">571428</span>     | 0\.<span style="text-decoration:overline">714285</span> | 0\.<span style="text-decoration:overline">857142</span> | 1 | 1\.<span style="text-decoration:overline">142857</span> | 1\.<span style="text-decoration:overline">285714</span> | 1\.<span style="text-decoration:overline">428571</span> |
| 1\.<span style="text-decoration:overline">571428</span> | 1\.<span style="text-decoration:overline">714285</span> | 1\.<span style="text-decoration:overline">857142</span>   | [2](../Page/2.md "wikilink")                            | 2\.<span style="text-decoration:overline">142857</span>     |                                                         |                                                         |   |                                                         |                                                         |                                                         |

| [乘方](../Page/乘方.md "wikilink") | 1     | 2                                | 3                                | 4     | 5     | 6      | 7      | 8       | 9        | 10                                         |  | 11         | 12          | 13          |
| ------------------------------ | ----- | -------------------------------- | -------------------------------- | ----- | ----- | ------ | ------ | ------- | -------- | ------------------------------------------ |  | ---------- | ----------- | ----------- |
| 7<sup>*x*</sup>                | **7** | 49                               | [343](../Page/343.md "wikilink") | 2401  | 16807 | 117649 | 823543 | 5764801 | 40353607 | 282475249                                  |  | 1977326743 | 13841287201 | 96889010407 |
| *x*<sup>7</sup>                | 1     | [128](../Page/128.md "wikilink") | 2187                             | 16384 | 78125 | 279936 | 823543 | 2097152 | 4782969  | [10000000](../Page/10000000.md "wikilink") |  | 19487171   | 35831808    | 62748517    |

| [Radix](../Page/Radix.md "wikilink") | 1               | 5               | 10              | 15                     | 20              | 25              | 30 \<\!--             | 35 --\>              | 40 \<\!--             | 45 --\>             | 50               | 60               | 70                | 80                 | 90                   | 100             |
| ------------------------------------ | --------------- | --------------- | --------------- | ---------------------- | --------------- | --------------- | --------------------- | -------------------- | --------------------- | ------------------- | ---------------- | ---------------- | ----------------- | ------------------ | -------------------- | --------------- |
| 110                                  | 120             | 130             | 140             | 150 \<\!--             | 160             | 170             | 180                   | 190 --\>             | 200                   | 250                 | 500              | 1000             | 10000             | 100000             | 1000000              |                 |
| *x*<sub>7</sub>                      | 1               | 5               | 13<sub>7</sub>  | 21<sub>7</sub>         | 26<sub>7</sub>  | 34<sub>7</sub>  | 42<sub>7</sub> \<\!-- | 50<sub>7</sub> --\>  | 55<sub>7</sub> \<\!-- | 63<sub>7</sub> --\> | 101<sub>7</sub>  | 114<sub>7</sub>  | 130<sub>7</sub>   | 143<sub>7</sub>    | 156<sub>7</sub>      | 202<sub>7</sub> |
| 215<sub>7</sub>                      | 231<sub>7</sub> | 244<sub>7</sub> | 260<sub>7</sub> | 303<sub>7</sub> \<\!-- | 316<sub>7</sub> | 322<sub>7</sub> | 345<sub>7</sub>       | 361<sub>7</sub> --\> | 404<sub>7</sub>       | 505<sub>7</sub>     | 1313<sub>7</sub> | 2626<sub>7</sub> | 41104<sub>7</sub> | 564355<sub>7</sub> | 11333311<sub>7</sub> |                 |

## 在科学中

  - 在[酸碱度中](../Page/酸碱度.md "wikilink")，中性的[pH为](../Page/pH.md "wikilink")7
  - [氮的](../Page/氮.md "wikilink")[原子序數](../Page/原子序數.md "wikilink")。\[1\]
  - [北斗七星主要由七颗](../Page/北斗七星.md "wikilink")[亮星组成](../Page/亮星.md "wikilink")。
  - [彩虹經常被分為七種顏色](../Page/彩虹.md "wikilink")：[紅](../Page/紅色.md "wikilink")、[橙](../Page/橙色.md "wikilink")、[黃](../Page/黃色.md "wikilink")、[綠](../Page/綠色.md "wikilink")、[蓝](../Page/蓝色.md "wikilink")、[靛](../Page/靛色.md "wikilink")、[紫](../Page/紫色.md "wikilink")\[2\]。
  - [生物界分為七個等级](../Page/生物界.md "wikilink")：[界](../Page/界_\(生物\).md "wikilink")、[门](../Page/门_\(生物\).md "wikilink")、[纲](../Page/纲.md "wikilink")、[目](../Page/目_\(生物\).md "wikilink")、[科](../Page/科_\(生物\).md "wikilink")、[属](../Page/属.md "wikilink")、[种](../Page/物种.md "wikilink")。
  - [锂的原子量为](../Page/锂.md "wikilink")7。

## 在人類文化中

  - [華盛頓海軍條約簽訂前](../Page/華盛頓海軍條約.md "wikilink")，裝備了16寸炮的七艘[戰艦](../Page/戰艦.md "wikilink")：[科羅拉多號戰艦](../Page/科羅拉多號戰艦.md "wikilink")、[馬里蘭號戰艦](../Page/馬里蘭號戰艦.md "wikilink")、[西維吉尼亞號戰艦](../Page/西維吉尼亞號戰艦.md "wikilink")、[納爾遜號戰艦](../Page/納爾遜號戰艦.md "wikilink")、[羅德尼號戰艦](../Page/羅德尼號戰艦.md "wikilink")、[長門號戰艦](../Page/長門號戰艦.md "wikilink")、[陸奥號戰艦](../Page/陸奥號戰艦.md "wikilink")，被稱為七大戰艦（Big
    seven）。
  - 在[基督教及](../Page/基督教.md "wikilink")[猶太教信仰中](../Page/猶太教.md "wikilink")，上帝用6天創造世界，第7天休息。
  - 目前通用曆法中的[一週亦為](../Page/一週.md "wikilink")7天，[星期日一般被定为](../Page/星期日.md "wikilink")[公眾假期](../Page/公眾假期.md "wikilink")。
  - 在西方文化中，7普遍被視為幸運數字，而有Lucky 7的說法。
  - 西方古典音樂有7個音階，受到[畢達哥拉斯學派的重視](../Page/畢達哥拉斯.md "wikilink")。
  - [七言律詩](../Page/七言律詩.md "wikilink")。
  - 在[樂理中](../Page/樂理.md "wikilink")，[簡譜上的ti音用](../Page/簡譜.md "wikilink")7表示。
  - 韓國歌手[崔東昱以](../Page/崔東昱.md "wikilink")**SE7EN**為藝名。
  - 著名罐裝[汽水](../Page/汽水.md "wikilink")[品牌](../Page/品牌.md "wikilink")——[七喜](../Page/七喜.md "wikilink")。
  - 連鎖式便利店──[7-Eleven](../Page/7-Eleven.md "wikilink")。
  - 在[粵語裏](../Page/粵語.md "wikilink")，7的中國數字大寫「柒」被廣泛用作[粗口之用](../Page/粗口.md "wikilink")，故如同4的中國數字音「死」字一樣，甚少使用。
  - [Windows 7](../Page/Windows_7.md "wikilink")、[Windows Phone
    7为微軟作業系統](../Page/Windows_Phone_7.md "wikilink")。
  - 吃角子老虎機的大獎圖樣是7。
  - 世界共有[七大洲](../Page/七大洲.md "wikilink")。
  - 早起開門七件事，[柴](../Page/柴.md "wikilink")、[米](../Page/米.md "wikilink")、[油](../Page/油.md "wikilink")、[鹽](../Page/食盐.md "wikilink")、[醬](../Page/醬.md "wikilink")、[醋](../Page/醋.md "wikilink")、[茶](../Page/茶.md "wikilink")。
  - [竹林七賢](../Page/竹林七賢.md "wikilink")。
  - [全真七子](../Page/全真七子.md "wikilink")。
  - [武當七俠](../Page/武當七俠.md "wikilink")。
  - 日本春天七草，
    是指[水芹](../Page/水芹.md "wikilink")、[荠菜](../Page/荠菜.md "wikilink")、[鼠曲草](../Page/鼠曲草.md "wikilink")、[繁缕](../Page/繁缕.md "wikilink")、[宝盖草](../Page/宝盖草.md "wikilink")、[蔓菁](../Page/蔓菁.md "wikilink")、[萝卜](../Page/萝卜.md "wikilink")。
  - 日本秋天七草，
    是指[胡枝子](../Page/胡枝子.md "wikilink")、[葛藤](../Page/葛藤.md "wikilink")、[瞿麥](../Page/瞿麥.md "wikilink")、[女蘿](../Page/松蘿.md "wikilink")、[蘭草](../Page/蘭草.md "wikilink")、[桔梗和](../Page/桔梗.md "wikilink")[狗尾草](../Page/狗尾草.md "wikilink")。
  - [七夕](../Page/七夕.md "wikilink")。
  - 在動漫[家庭教師HITMAN
    REBORN\!中](../Page/家庭教師HITMAN_REBORN!#73.md "wikilink")，7<sup>3</sup>的原石正是創造這個世界的基石，分別代表七個「阿爾柯巴雷諾奶嘴」、七枚「彭哥列指環」、七枚「瑪雷指環」。
  - 在動漫[妖精的尾巴中有許多設定跟](../Page/妖精的尾巴.md "wikilink")7有關。
  - 1\~10中，用閩南語文音或白音唸，唯一一個發音相同的。
  - 在日本7有帶來幸運之意味。
  - 在曼聯中，7號球衣是給具有相當實力球員穿著。
  - 音乐有7个音符，是指[do](../Page/do.md "wikilink")、[re](../Page/re.md "wikilink")、[mi](../Page/mi.md "wikilink")、[fa](../Page/fa.md "wikilink")、[so](../Page/so.md "wikilink")、[la](../Page/la.md "wikilink")、[Si](../Page/Si.md "wikilink")/[Ti](../Page/Ti.md "wikilink")。
  - [戰國七雄分別為](../Page/戰國七雄.md "wikilink")[齊國](../Page/齊國.md "wikilink")、[楚國](../Page/楚國.md "wikilink")、[燕國](../Page/燕國.md "wikilink")、[秦國](../Page/秦國.md "wikilink")、[韓國](../Page/韓國_\(戰國\).md "wikilink")、[趙國](../Page/趙國.md "wikilink")、[魏國](../Page/魏國.md "wikilink")。
  - [格林童話當中的](../Page/格林童話.md "wikilink")[狼和七隻小山羊](../Page/狼和七隻小山羊.md "wikilink")。
  - 喜、怒、哀、懼、愛、惡、欲為[七情](../Page/七情.md "wikilink")。
  - [七宗罪](../Page/七宗罪.md "wikilink")：傲慢、妒忌、暴怒、懶惰、貪婪、暴食、色慾。
  - [七美德](../Page/七美德.md "wikilink")：謙卑、寬容、耐心、勤勉、慷慨、節制、貞潔 。
  - [牌七](../Page/牌七.md "wikilink")
  - 香菸品牌[七星香煙](../Page/七星香煙.md "wikilink")。
  - 著名小說[哈利波特有七部](../Page/哈利波特.md "wikilink")。

## 時間與曆法

  - 現代國際通用的[西曆](../Page/公历.md "wikilink")，將1[年分成](../Page/年.md "wikilink")12个[月](../Page/月.md "wikilink")。12个月每月長度不一，但都有7日，分別為[1月7日](../Page/1月7日.md "wikilink")、[2月7日](../Page/2月7日.md "wikilink")、[3月7日](../Page/3月7日.md "wikilink")、[4月7日](../Page/4月7日.md "wikilink")、[5月7日](../Page/5月7日.md "wikilink")、[6月7日](../Page/6月7日.md "wikilink")、[7月7日](../Page/7月7日.md "wikilink")、[8月7日](../Page/8月7日.md "wikilink")、[9月7日](../Page/9月7日.md "wikilink")、[10月7日](../Page/10月7日.md "wikilink")、[11月7日和](../Page/11月7日.md "wikilink")[12月7日](../Page/12月7日.md "wikilink")。
  - 此外，在公曆紀年方面，人類對公元[前7年](../Page/前7年.md "wikilink")、公元[7年](../Page/7年.md "wikilink")，公元[前7世纪及公元](../Page/前7世纪.md "wikilink")[7世纪均有記載](../Page/7世纪.md "wikilink")。
  - [農曆七月俗稱鬼月](../Page/農曆七月.md "wikilink")

## 另見

  -
  -
## 參考

<div class="references-small">

<references/>

</div>

[Category:整數素數](../Category/整數素數.md "wikilink")

1.  [Royal Society of Chemistry - Visual Element Periodic
    Table](http://www.rsc.org/periodic-table)
2.  《[现代汉语词典](../Page/现代汉语词典.md "wikilink")》[商务印书馆](../Page/商务印书馆.md "wikilink")
    1978年版、p.462。