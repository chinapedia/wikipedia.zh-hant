**廣州環城高速公路**位於[中國](../Page/中國.md "wikilink")[廣東省](../Page/廣東省.md "wikilink")[廣州市及](../Page/廣州市.md "wikilink")[佛山市](../Page/佛山市.md "wikilink")[南海区](../Page/南海区.md "wikilink")，是[中国大陆首條環繞城市的](../Page/中国大陆.md "wikilink")[高速公路](../Page/高速公路.md "wikilink")，中国国家高速公路网編號：**S81**（粤高速81號），其中北环段与**S15**[广佛高速公路共线](../Page/广佛高速公路.md "wikilink")。

## 概况

工程於1987年1月18日動工，全長60公里，总投资63亿元。分為東、南、西及北環4段。沙贝至广清段1989年8月1日通车；广清至广花段1990年7月3日试通车，12月2日正式通车；1993年12月18日北环全线通车；广氮至东圃段1997年10月底通车；东圃至三滘段1999年2月12日通车；同年12月2日海南至浔峰洲段通车；2000年6月26日全线通车。

東環由黃村立交起，跨過[廣園快速路](../Page/廣園快速路.md "wikilink")、[廣深鐵路](../Page/廣深鐵路.md "wikilink")、[中山大道及](../Page/中山大道_\(广州\).md "wikilink")[黃埔大道](../Page/黃埔大道.md "wikilink")，以[東圃大橋跨過](../Page/東圃大橋.md "wikilink")[珠江](../Page/珠江.md "wikilink")[北帝沙航道至](../Page/北帝沙.md "wikilink")[海珠區新洲轉入南環](../Page/海珠區.md "wikilink")。

南環由海珠區新洲立交起，經侖頭村、小洲村、三滘立交，以[丫髻沙大橋跨過珠江丫髻沙航道至](../Page/丫髻沙大橋.md "wikilink")[荔灣區](../Page/荔灣區.md "wikilink")[芳村南東沙開發區](../Page/芳村.md "wikilink")，跨過[東沙大道及](../Page/東沙大道.md "wikilink")[花地大道至海南立交轉入西環](../Page/花地大道.md "wikilink")。

西環由芳村海南立交起向北，經過增滘立交，官田村，跨過[廣三鐵路](../Page/廣三鐵路.md "wikilink")，[廣佛公路](../Page/324国道.md "wikilink")，珠江西航道至沙貝立交轉入北環。

北環是環城高速公路最繁忙的路段，由沙貝立交起，向東經金沙洲、橫沙村再到達[增槎路](../Page/增槎路.md "wikilink")，跨[京廣鐵路至三元里](../Page/京廣鐵路.md "wikilink")，以[高架橋架於](../Page/高架橋.md "wikilink")[廣園路上](../Page/廣園路.md "wikilink")，到達[大金鐘路附近回落於](../Page/大金鐘路.md "wikilink")[白雲山山腰](../Page/白雲山.md "wikilink")，以[白雲隧道穿越白雲山到達](../Page/白雲隧道.md "wikilink")[沙河](../Page/沙河.md "wikilink")，跨越[廣州大道及](../Page/廣州大道.md "wikilink")[天源路後抵達岑村立交](../Page/天源路.md "wikilink")，與[華南快速幹線相交](../Page/華南快速幹線.md "wikilink")，在黃村立交結合成環城高速公路。

其管理公司有[广州北环高速公路有限公司和](../Page/广州北环高速公路有限公司.md "wikilink")[广州东南西环高速公路有限公司](../Page/广州东南西环高速公路有限公司.md "wikilink")，分别负责管理北环和东南西环路段。东南西环路段于2007年9月21日起停止收费。2012年3月底，撤销广氮收费站，广州区域高速公路与深圳区域、珠三角区域高速公路网达到实质上的联网收费。\[1\]

2007年，香港[合和公路基建和](../Page/合和公路基建.md "wikilink")[長江基建與合作夥伴廣州市通達高速公路有限公司定立協議](../Page/長江基建.md "wikilink")，向其出售該路段之所有權益，代價分別為17.13億元和12​​.22億元人民幣。出售前，長建、合和公路及廣州通達各持有廣州東南西環高速公路45％、45％和10％權益。東南西環納入年票制後，由廣州交通投資集團有限公司屬下的一間公司經過招標負責日常管養，政府撥款金額為每年1000多萬。

## 互通枢纽和服务设施

[Guangdong_S81.svg](https://zh.wikipedia.org/wiki/File:Guangdong_S81.svg "fig:Guangdong_S81.svg")
[Guangdong_S15.svg](https://zh.wikipedia.org/wiki/File:Guangdong_S15.svg "fig:Guangdong_S15.svg")

[Aiga_departingflights_inv.svg](https://zh.wikipedia.org/wiki/File:Aiga_departingflights_inv.svg "fig:Aiga_departingflights_inv.svg")
[广州白云国际机场](../Page/广州白云国际机场.md "wikilink")
经[广园西路可至](../Page/广园路.md "wikilink")[内环路](../Page/广州内环路.md "wikilink")


## 参考资料

[S](../Category/广东省高速公路.md "wikilink")
[Category:广州市高速公路](../Category/广州市高速公路.md "wikilink")
[Category:環狀道路](../Category/環狀道路.md "wikilink")

1.  [广深、北环等高速将撤5个收费站](http://sznews.oeeee.com/a/20120322/1048778.html?bsh_bid=85631527)
     奥一网，于2012年3月22日查阅