**皮特·馬拉維奇**（，），綽號「**手槍皮特**」（**Pistol
Pete**），已故[國家籃球協會](../Page/國家籃球協會.md "wikilink")(NBA)球星。身高六呎五吋，司職後衛，能傳擅射，手法花巧刁鑽，被奉為NBA史上最出色的「娛樂家」（showman）之一，约翰·哈夫利切克称他是那个时代最佳的持球者。

## 生平

父親為籃球教練，自小已接受正規的籃球訓練。1966年入讀[路易西安納州州立大學](../Page/路易西安納州.md "wikilink")(Louisiana
State
University)，當時其父為該校籃球隊教練。按照當時規定，新生不能代表校隊出賽。以後的三年，鋒芒畢露，尖新的球技、鬆跨的灰襪開始成為他的標誌。三年大學生涯共得分3,667，平均每場44.2分，兩項紀錄至今未被打破。

1970年展開職業生涯，加入[亞特蘭大老鷹](../Page/亞特蘭大老鷹.md "wikilink")。效力四個球季，被換到[新奧爾良爵士隊](../Page/新奧爾良.md "wikilink")（[猶他爵士隊的前身](../Page/猶他爵士隊.md "wikilink")），重返路州，度過職業生涯的黃金期。1979-80年球季被換至[波士頓凱爾特人隊](../Page/波士頓凱爾特人隊.md "wikilink")，充當第六人，跟當年的新人[拉里·伯德為隊友](../Page/拉里·伯德.md "wikilink")。球季結束後，因傷患宣布退休。

職業生涯出賽688場，總得分15,948，平均每場得分24.4，一場最高得分68。五次入選明星賽，1977年當上得分王（平均每場得分31.1）。

退休後皈依[基督教](../Page/基督教.md "wikilink")，戒掉[酗酒的陋習](../Page/酗酒.md "wikilink")，以公眾人物的身分宣揚自己的信仰，並製作籃球錄像教材
(*Pistol Pete's Homework Basketball
video*)。1987年入選[籃球名人堂](../Page/籃球名人堂.md "wikilink")，為歷來最年輕的入選者。1988年，在一場即興的籃球賽中[心臟病發](../Page/心臟病.md "wikilink")，送院後證實不治。驗屍結果顯示，他的心臟只有一條[冠狀動脈](../Page/冠狀動脈.md "wikilink")，而非正常的兩條。

為紀念這位一代球星，其母校的主場館易名為皮特‧馬拉維奇綜合中心(Pete Maravich Assembly
Center)。1991年，[好萊塢製作了一套以其生平為題材的電影](../Page/好萊塢.md "wikilink")(*Pistol
Pete*)。1996年，馬拉維奇入選[NBA50大巨星](../Page/NBA50大巨星.md "wikilink")，是唯一一位在入選時已離世的球星。

## 参考资料

## 外部連結

  - [皮特·馬拉維奇官方網站](http://www.pistolpete23.com/)
  - [NBA Historical
    Bio](http://www.nba.com/history/players/maravich_bio.html)
  - [打球短片](http://www.youtube.com/watch?v=8Y5KAaercTI)

[category:美国男子篮球运动员](../Page/category:美国男子篮球运动员.md "wikilink")
[category:亚特兰大老鹰队球员](../Page/category:亚特兰大老鹰队球员.md "wikilink")
[category:犹他爵士队球员](../Page/category:犹他爵士队球员.md "wikilink")
[category:波士顿凯尔特人队球员](../Page/category:波士顿凯尔特人队球员.md "wikilink")

[Category:籃球名人堂成員](../Category/籃球名人堂成員.md "wikilink")
[Category:NBA退休背號球员](../Category/NBA退休背號球员.md "wikilink")
[Category:塞爾維亞裔美國人](../Category/塞爾維亞裔美國人.md "wikilink")
[Category:路易斯安那州立大學校友](../Category/路易斯安那州立大學校友.md "wikilink")