**煤气**从字面意思上讲是与[煤有关的](../Page/煤.md "wikilink")[气体](../Page/气体.md "wikilink")，但是在不同的使用环境下，煤气具有不同的解释：

  - 在[石油化學工程中](../Page/石油化學.md "wikilink")，「[合成气](../Page/合成气.md "wikilink")」，又稱為「水煤氣」，指的是[乾馏煤炭所得到的作为燃料的气体](../Page/乾馏.md "wikilink")，其主要成分是[氢](../Page/氢.md "wikilink")、[甲烷](../Page/甲烷.md "wikilink")、[乙烯](../Page/乙烯.md "wikilink")、[一氧化碳](../Page/一氧化碳.md "wikilink")、[石腦油](../Page/石腦油.md "wikilink")，另外还有少量的[氮和](../Page/氮.md "wikilink")[二氧化碳等不可](../Page/二氧化碳.md "wikilink")[燃烧的杂质](../Page/燃烧.md "wikilink")。
  - 俗話说的「煤气」指的是煤炭不完全燃烧所产生的气体，主要成分是一氧化碳，如「煤气中毒」，通常實際是指[一氧化碳中毒](../Page/一氧化碳中毒.md "wikilink")；也有的时候指的是含[硫量过高的煤燃烧后产生的含有](../Page/硫.md "wikilink")[二氧化硫等硫化物的混合气体](../Page/二氧化硫.md "wikilink")，如口语中的“煤气味”，而实际上一氧化碳是没有气味的。
  - 有时也指[液化石油气](../Page/液化石油气.md "wikilink")，如[煤气罐即是装液化石油气的钢瓶](../Page/煤气罐.md "wikilink")。

## 參見

  - [燃氣](../Page/燃氣.md "wikilink")
  - [香港中華煤氣](../Page/香港中華煤氣.md "wikilink")：香港的煤制气生產及供應商
  - [河南煤業化工集團煤氣化公司](../Page/河南煤業化工集團煤氣化公司.md "wikilink")：中國河南省的煤气生產及供應商
  - [2013年罗萨里奥煤气爆炸](../Page/2013年罗萨里奥煤气爆炸.md "wikilink")

## 外部連結

  - [家用調節器實測](http://tw.news.yahoo.com/%E7%93%A6%E6%96%AF%E6%80%8E%E8%80%97%E7%9B%A1-%E7%87%9F%E6%A5%AD-%E5%AE%B6%E7%94%A8%E8%AA%BF%E7%AF%80%E5%99%A8%E5%AF%A6%E6%B8%AC-022328409.html)
  - [桶裝瓦斯稽查](https://web.archive.org/web/20131219034200/http://tw.news.yahoo.com/%E6%A1%B6%E8%A3%9D%E7%93%A6%E6%96%AF%E7%A8%BD%E6%9F%A5-127%E5%AE%B6%E6%A5%AD%E8%80%85-9%E5%AE%B6%E4%B8%8D%E5%90%88%E6%A0%BC-200102765.html)

[Category:燃料气体](../Category/燃料气体.md "wikilink")
[Category:合成燃料](../Category/合成燃料.md "wikilink")
[Category:煤炭](../Category/煤炭.md "wikilink")