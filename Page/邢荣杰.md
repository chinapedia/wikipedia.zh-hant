**邢榮傑**（），[河北省](../Page/河北省.md "wikilink")[無極縣人](../Page/無極縣.md "wikilink")，[中國軍事人物](../Page/中國.md "wikilink")。

他於1933年擔任[國民黨政府無極縣](../Page/中國國民黨.md "wikilink")[保安隊大隊長](../Page/保安隊.md "wikilink")。1937年，他與[內丘縣保安隊參加](../Page/內丘縣.md "wikilink")[太行軍區](../Page/太行軍區.md "wikilink")[冀西游擊隊](../Page/冀西游擊隊.md "wikilink")，後來加入[八路軍](../Page/八路軍.md "wikilink")，並於同年加入[中國共產黨](../Page/中國共產黨.md "wikilink")。

在[中國抗日戰爭期間](../Page/中國抗日戰爭.md "wikilink")，他先後擔任[冀西游擊隊第五大隊大隊長](../Page/冀西游擊隊.md "wikilink")、司令部參謀長兼第三支隊支隊長、[冀豫抗日義勇軍參謀長](../Page/冀豫抗日義勇軍.md "wikilink")、[太行軍區第五軍分區第三十四團參謀長及第八軍分區參謀長等職位](../Page/太行軍區.md "wikilink")。

在[第二次國共內戰期間](../Page/第二次國共內戰.md "wikilink")，他先後擔任太行軍區第五分區參謀長、[晉冀魯豫野戰軍第六縱隊第十六旅參謀長](../Page/晉冀魯豫野戰軍.md "wikilink")、第十八旅副旅長兼參謀長、[中原軍區](../Page/中原軍區.md "wikilink")[軍政大學總隊長](../Page/軍政大學.md "wikilink")、第二[野戰軍第十二軍第三十六師](../Page/野戰軍.md "wikilink")[師長等職位](../Page/師長.md "wikilink")。他在內戰時先後參與了[白晉鐵路破襲戰](../Page/白晉鐵路破襲戰.md "wikilink")、[邯鄲戰役](../Page/邯鄲戰役.md "wikilink")、[襄樊戰役](../Page/襄樊戰役.md "wikilink")、[渡江戰役與](../Page/渡江戰役.md "wikilink")[西南戰役等戰役](../Page/西南戰役.md "wikilink")。

[中華人民共和國成立之後](../Page/中華人民共和國.md "wikilink")，邢榮傑先後擔任[中國人民解放軍第三兵團](../Page/中國人民解放軍.md "wikilink")[師長兼](../Page/師長.md "wikilink")[川東軍區涪陵軍分區司令官](../Page/川東軍區.md "wikilink")（1950年）、[南京軍事學院訓練部戰術教授會教員與副主任](../Page/南京軍事學院.md "wikilink")（1952年）、駐[越南大使館](../Page/越南.md "wikilink")[武官](../Page/武官.md "wikilink")，[重慶步兵學校校長](../Page/重慶步兵學校.md "wikilink")、[陝西省軍區副司令官等職](../Page/陝西省.md "wikilink")。

邢榮傑於1955年被授予[大校](../Page/大校.md "wikilink")，並於1964年晉升為[少將](../Page/少將.md "wikilink")。他曾獲得二級[獨立自由勳章與二級](../Page/獨立自由勳章.md "wikilink")[解放勳章](../Page/解放勳章.md "wikilink")。

1997年11月20日，他逝世於[西安](../Page/西安.md "wikilink")，終年86歲。

## 参考资料

  - [1](http://www.rwabc.com/diqurenwu/diqudanyirenwu.asp?p_name=邢荣杰&people_id=11426&id=20735)

[X邢](../Category/中華民國大陸時期軍事人物.md "wikilink")
[X邢](../Category/中國人民解放軍少將.md "wikilink")
[X邢](../Category/无极县人.md "wikilink") [Rong
Jie](../Category/邢姓.md "wikilink")