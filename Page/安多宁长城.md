[Antonine_wall.JPG](https://zh.wikipedia.org/wiki/File:Antonine_wall.JPG "fig:Antonine_wall.JPG")
[Hadrians_Wall_map.png](https://zh.wikipedia.org/wiki/File:Hadrians_Wall_map.png "fig:Hadrians_Wall_map.png")
**安東尼長城**（，，又译安多宁边墙）
位于今日[苏格兰境内](../Page/苏格兰.md "wikilink")，在[羅馬帝國](../Page/羅馬帝國.md "wikilink")[君主](../Page/君主.md "wikilink")[安敦宁·毕尤在位时兴建](../Page/安敦宁·毕尤.md "wikilink")（始建于142年，建成于154年）。它西起[克莱德河河口](../Page/克莱德河.md "wikilink")，东至[福斯湾](../Page/福斯湾.md "wikilink")，全长63公里\[1\]，高约3米，宽5米，将当时南部属古罗马管辖的[不列顛尼亚和北部的](../Page/不列顛尼亚.md "wikilink")[喀里多尼亞](../Page/喀里多尼亞.md "wikilink")（蘇格蘭在羅馬時代的古名）隔开，并通过其北方的深沟来协助保证安全。

## 概述

安东尼长城位于[克莱德湾和](../Page/克莱德湾.md "wikilink")[福斯湾之间](../Page/福斯湾.md "wikilink")。他有一个石头的地基以及一面青苔石建成的墙。每两英里有一个堡垒，自北向南这座长城由一条深沟、一堵墙和用作军事的道路构成。

## 位置及建造

罗马帝国君主[安敦宁·毕尤于约公元](../Page/安敦宁·毕尤.md "wikilink")142年下令建设安东尼长城。当时的罗马帝国的不列颠总督[乌尔比库斯](../Page/昆塔斯·罗利乌斯·乌尔比库斯.md "wikilink")，来监督建造，花费了12年左右才建造完成。这个长城自[克莱德湾附近的](../Page/克莱德湾.md "wikilink")[西邓巴顿郡的](../Page/西邓巴顿郡.md "wikilink")[旧基尔帕特里克到](../Page/旧基尔帕特里克.md "wikilink")[福斯湾附近的](../Page/福斯湾.md "wikilink")共计长63公里，它取代了南方的[哈德良长城](../Page/哈德良长城.md "wikilink")，扩张[罗马帝国的版图和统治地位](../Page/罗马帝国.md "wikilink")，作为[不列颠尼亚的边境](../Page/不列颠尼亚_\(罗马行省\).md "wikilink")。不过，即使罗马人为了保护自己的路线，在安东尼长城以北的[苏格兰北部建立了许多堡垒和临时营地](../Page/苏格兰.md "wikilink")，但他们并没有再向北征服[喀里多尼亞人](../Page/喀里多尼亞人.md "wikilink")，以至于安东尼长城经常遭受攻击。罗马人称安东尼长城以北为喀里多尼亞，尽管有时可以指[哈德良长城以北的广大地区](../Page/哈德良长城.md "wikilink")。
安东尼长城要比哈德良长城要短，而且是用青苔石建成的，但仍然是一个令人印象深刻的成就。塔楼的石头地基和翼墙表明，原计划是建立一个类似哈德良长城的石墙，但很快这个计划就被放弃。竣工后，安东尼长城就像一个土堆一样，大约4米高，用分层的草皮构成，北部偶尔有些深沟，南方就是[罗马大道](../Page/罗马道路.md "wikilink")。罗马人本来计划每10公里建造一座堡垒，但很快就被修改为每3.3公里为一座，所以最后建成了整整19个堡垒。保存最好的，也是最短的堡垒之一是。除了那些堡垒，还至少有9个更小的小堡，非常可能是每1.6公里一个，这也是原始方案的一部分，只不过后来其中一些被更大的堡垒取代了。如今最明显的小堡就是金内尔（Kinneil）小堡，在整个长城偏东的地方，博内斯附近。
除了长城本身在东部和西部都有可以被视为前哨站和（或）补给设施的堡垒外，长城本身也是。另外在[卡斯岭](../Page/卡斯岭.md "wikilink")（Gask
Ridge）地区的一些堡垒也发挥了作用，例如，，贝尔塔堡\[2\]，或许Dalginross和卡吉尔堡也发挥了那样的作用。\[3\]

## 放弃

这座长城建成八年后，[罗马军团向南退到](../Page/罗马军团.md "wikilink")[哈德良长城](../Page/哈德良长城.md "wikilink")，就被放弃了，随着时间的推移，一些操着[布立吞亚支语言的人在这里定居](../Page/布立吞亚支.md "wikilink")，他们就像一个缓冲带，后来那里被称为。公元197年这里经过一系列的攻击后，罗马帝国皇帝[赛维鲁于同年到达喀里多尼亚](../Page/塞普蒂米乌斯·塞维鲁.md "wikilink")（今苏格兰）前线，并且修复了安东尼长城。虽然重新收复安东尼长城的时间仅仅持续了几年，但这座长城有时候还被罗马的历史学家称为**赛维鲁之墙**（Severan
Wall），这导致了一些后世学者，如[比德误以为安东尼长城是](../Page/比德.md "wikilink")[哈德良长城的一部分](../Page/哈德良长城.md "wikilink")。

## 罗马人离开后的历史

### 严峻之堤

在一些[中世纪的史料中](../Page/中世纪.md "wikilink")，例如约翰·福尔登（John of
Fordun）所编写的编年史中，这座长城就被称为**格吕默之堤**（Gryme's
dyke）。福尔登说：“这个名字来自法克哈尔（Farquahar）的儿子，虚构的国王因格纽斯（Eugenius）的祖父”。随着时间的推移，逐渐演变成了“格雷厄姆之堤”（Graham's
dyke），这是一个仍被发现于博内斯附近的城墙之上的名字，并与[格雷厄姆家族有联系](../Page/格雷厄姆家族.md "wikilink")。值得注意的是“格雷厄姆”在苏格兰一些地区是魔鬼的昵称，因此格吕默之堤也就成了“恶魔之堤”，同样的在德国南部也有一个被称为“[托于菲尔斯毛尔](../Page/日耳曼长城.md "wikilink")（Teufelsmauer）”（意为恶魔墙）的（Limes）。Grímr和Grim曾被称为奥丁或者[沃登](../Page/沃登.md "wikilink")，可能是归功于想要在极短时间内完成工程中的人了。这个名称与多次在英格兰早期城墙发现的"严峻之沟"相似，例如：[沃灵福德附近的](../Page/沃灵福德_\(南牛津郡\).md "wikilink")，巴尔顿汉姆和[伯克翰斯德之间的](../Page/伯克翰斯德.md "wikilink")。在安敦宁·毕尤之后，其他使用过的名字有：毕尤之墙、安敦宁堡垒。\[4\]\[5\]
[赫克托在他](../Page/赫克托·博伊斯.md "wikilink")1527年所写的《苏格兰通史》中称它为“阿伯康之墙”，复述了它被格雷厄姆摧毁的故事。\[6\]

### 世界遗产

英国政府于2003年首次将[世界遗产安东尼长城提名至](../Page/世界遗产.md "wikilink")[联合国教科文组织](../Page/联合国教科文组织.md "wikilink")。\[7\]它自2005年以来就被[苏格兰政府支持](../Page/苏格兰政府.md "wikilink")，\[8\]而且从2006年开始苏格兰文化部长[帕特里夏同样对此表示支持](../Page/帕特里夏·弗格森.md "wikilink")。\[9\]
2007年1月，他被英国官方提名，\[10\]同年5月，苏格兰议会成员再次为此事而努力。\[11\]尽管在关于安东尼长城提到的文章中，它不会出现在联合国教科文组织的世界遗产地图中，\[12\]安东尼长城还是在2008年7月7日被列为世界遗产“罗马帝国的边境”。\[13\]\[14\]

### Historic Scotland

由[Historic
Scotland所放出的几个长城沿线的景点](../Page/Historic_Scotland.md "wikilink")。它们是：

  - 酒吧丘堡（Bar Hill Fort）
  - 贝尔斯登浴屋（Bearsden Bath House）
  - 凯里堡（Castle cary）
  - 克洛伊山（Croy Hill）
  - Dullatur
  - 粗糙之堡（Rough Castle）
  - Seabegs之木（Seabegs Wood）
  - 瓦特林小屋（Watling Lodge）
  - 韦斯特伍德（Westerwood）

任何景点都无人值守，并且在合理的时间开放。\[15\] 请参见：.

## 地图上的长城

[Rough.Castle.Antonine.Wall.jpg](https://zh.wikipedia.org/wiki/File:Rough.Castle.Antonine.Wall.jpg "fig:Rough.Castle.Antonine.Wall.jpg")
第一个有能力系统地映射出安东尼长城地图的制作是在1764年以[英国地形测量局之名](../Page/英国地形测量局.md "wikilink")，被威廉·罗伊（William
Roy）担任\[16\]。他提供了其遗迹准确的位置、详细的图纸和被破坏后的发展情况，他留下的地图和图纸，现在是这件事唯一可靠的记录。

## 小说中

这座北方长城在的一些历史演义小说中被描述到过；例如在*马主的印记*（The Mark of the Horse
Lord）中它被当做罗马的一个全功能前哨站，在*边疆之狼*（Frontier
Wolf）中它是一座废墟。

## 参见

  - [哈德良长城](../Page/哈德良长城.md "wikilink")

## 参考来源

[Category:英国军事设施](../Category/英国军事设施.md "wikilink")
[Category:英國軍事史](../Category/英國軍事史.md "wikilink")
[Category:英国建筑史](../Category/英国建筑史.md "wikilink")
[Category:蘇格蘭建築物](../Category/蘇格蘭建築物.md "wikilink")
[Category:古罗马城墙](../Category/古罗马城墙.md "wikilink")
[Category:欧洲隔离墙](../Category/欧洲隔离墙.md "wikilink")

1.
2.  name="L.Keppie, Scotland's Roman Remains 1986"
3.  D.J.Woolliscroft & B.Hoffmann, Romes First Frontier. The Flavian
    occupation of Northern Scotland（Stroud: Tempus 2006）
4.  Earthwork of England: prehistoric, Roman, Saxon, Danish, Norman and
    mediæval - Page 496, by Arthur Hadrian Allcroft
5.  'Transactions of the Woolhope Naturalists' Field Club - Page 255, by
    Woolhope Naturalists' Field Club, Hereford, England, G. H. Jack,
    1905
6.  Boece, Hector, *Historia Gentis Scotorum*，（1527）, book 7, chapter 16
7.
8.
9.
10.
11.
12. [Frontiers of the Roman Empire - UNESCO World Heritage
    Centre](http://whc.unesco.org/en/list/430)
13. [UNESCO World Heritage Centre. New Inscribed
    Properties](http://whc.unesco.org/en/newproperties/)
14. ["Wall gains World Heritage
    status'"](http://news.bbc.co.uk/1/hi/scotland/tayside_and_central/7494680.stm)
    BBC News. Retrieved 8 July 2008.
15. [Historic Scotland - Antonine Wall: Overview Property
    Detail](http://www.historic-scotland.gov.uk/index/places/propertyresults/propertydetail.htm?PropID=PL_305&PropName=Antonine%20Wall:%20Overview)
16.