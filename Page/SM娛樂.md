**SM娱乐**（；）是由歌手出身的[李秀滿于](../Page/李秀滿.md "wikilink")1995年创办的一间[韩国大型娛樂公司](../Page/韩国.md "wikilink")，其名稱是由「**Star
Museum**」縮寫而來，与[YG娱乐](../Page/YG娱乐.md "wikilink")、[JYP娱乐并称为](../Page/JYP娱乐.md "wikilink")**韩国三大娱乐公司，**是韓國最大規模的經紀公司。

公司最初的重點是娛樂管理業務，現在新的附屬公司業務領域不斷擴大，旗下組合及業務成功打入了國際市場。《[福布斯雜誌](../Page/福布斯.md "wikilink")》更形容韩国SM娱乐有限公司為[K-pop的創造者](../Page/K-pop.md "wikilink")\[1\]，可見其在[韩国娛樂圈的地位](../Page/韩国.md "wikilink")。2016年3月21日，旗下公司[SM
C\&C正式宣佈將於](../Page/SM_C&C.md "wikilink")6月设立全资子公司[Woollim
Entertainment](../Page/Woollim_Entertainment.md "wikilink")\[2\]。

## 大事記

### 1995至1999年

  - 1995年2月，法人设立，注册资金5000万韩元，SM Entertainment成立。
  - 1996年9月，韩国元祖级偶像團體[H.O.T出道](../Page/H.O.T.md "wikilink")，成為韓國90年代後期最有代表性的組合，不斷地創下驚人地銷售與得獎紀錄。
  - 1997年，[S.E.S出道](../Page/S.E.S._\(组合\).md "wikilink")，被譽為[K-pop首個標誌性女團](../Page/K-pop.md "wikilink")，其巨大成功被視為後輩女團之典範。
  - 1998年，[H.O.T](../Page/H.O.T.md "wikilink")、[S.E.S打入](../Page/S.E.S._\(组合\).md "wikilink")[中国及](../Page/中国.md "wikilink")[日本市場](../Page/日本.md "wikilink")，掀起了[亚洲](../Page/亚洲.md "wikilink")“[韩流](../Page/韩流.md "wikilink")”热潮。同年3月，韩国最长寿偶像團體[神话出道](../Page/神話_\(組合\).md "wikilink")。
  - 1999年，韩国二人男子偶像團體[Fly to the
    Sky出道](../Page/Fly_to_the_Sky.md "wikilink")。

[SM_Entertainment_building_2012.JPG](https://zh.wikipedia.org/wiki/File:SM_Entertainment_building_2012.JPG "fig:SM_Entertainment_building_2012.JPG")

### 2000至2004年

  - 2000年4月，韩国SM娱乐有限公司登记注册[韩国高斯达克市场](../Page/韩国.md "wikilink")，发行股票，成为韩国第一个娱乐文化股份公司。同年8月，[BoA出道](../Page/BoA.md "wikilink")。有＂韓國天后＂之稱的[BoA](../Page/BoA.md "wikilink")\[3\]取得的成就對韩国SM娱乐有限公司發展成[韩国最出色的经纪公司有着舉足輕重的地位](../Page/韩国.md "wikilink")。
  - 2001年，设立面向日本市场的SM Entertainment
    Japan，與[日本最大](../Page/日本.md "wikilink")[唱片公司](../Page/唱片公司.md "wikilink")[愛貝克思建立長久的合作關係](../Page/愛貝克思.md "wikilink")。同年，[H.O.T宣布正式解散](../Page/H.O.T.md "wikilink")\[4\]，現時只有[安七炫](../Page/安七炫.md "wikilink")（Kangta）為韩国SM娱乐有限公司藝人。
  - 2002年12月，与[日本的FANDANGO](../Page/日本.md "wikilink")
    JAPAN（YOSHIMOTO集团和KDDI的合资公司）、YOSHIMOTO集团以及AVEX共同创立了网络娱乐公司---FANDANGO
    KOREA，在网络与移动通信迅速发展的时代，将主要负责为大众开发数字信息资源。同年，[S.E.S解散](../Page/S.E.S._\(组合\).md "wikilink")。
  - 2003年，[神話不與SM](../Page/神話.md "wikilink") Entertainment續約，開始新的演藝生涯。
  - 2004年，[TRAX出道](../Page/TRAX.md "wikilink")。同年2月，韩国至尊偶像团体[東方神起出道](../Page/東方神起.md "wikilink")，一躍成為極受歡迎的新世代合聲團體。[東方神起的官方歌迷俱乐部Cassiopeia人數更以](../Page/東方神起.md "wikilink")80萬打破了當時的世界紀錄\[5\]。

### 2005至2008年

[Girls'_Generation_at_the_2014_SMTOWN_in_Seoul_01.jpg](https://zh.wikipedia.org/wiki/File:Girls'_Generation_at_the_2014_SMTOWN_in_Seoul_01.jpg "fig:Girls'_Generation_at_the_2014_SMTOWN_in_Seoul_01.jpg")\]\]

  - 2005年，安七炫在韩国SM娛樂有限公司擔任培訓藝人成為股東和理事。同年4月，[天上智喜出道](../Page/天上智喜.md "wikilink")。11月，推出韓國王牌男子天團[Super
    Junior](../Page/Super_Junior.md "wikilink")，至今為韓國獲得最多獎項的團體其中之一，橫掃三大頒獎典禮大賞的團體。
  - 2007年8月，韓國王牌女子天團[少女時代出道](../Page/少女時代.md "wikilink")，以卓越成績改寫韓國女子組合在韓國樂壇的歷史，在世界各地皆累積廣大的知名度，為韓國最著名及最受歡迎的音樂團體之一。
  - 2008年1月，韩国SM娱乐有限公司新成立了子公司S.M. Amusement，S.M. F\&B Development。
    5月，韩国SM娛樂有限公司平均年齡最小的韓國人氣偶像團體[SHINee出道](../Page/SHINee.md "wikilink")。同年，韩国SM娛樂有限公司設立面向[美國市場的SM](../Page/美國.md "wikilink")
    Entertainment U.S.A.擴展業務，現時為[股票上市企業](../Page/股票上市.md "wikilink")。

### 2009年

  - 2009年7月，[東方神起在中](../Page/東方神起.md "wikilink")、有天、俊秀三人與韩国SM娱乐有限公司關係惡化之下，向首爾中央地方法院訴請解除合約效力。最終[東方神起成員只剩下允浩](../Page/東方神起.md "wikilink")、昌珉二人。9月5日，韩国人氣女子偶像團體[f(x)出道](../Page/f\(x\)_\(組合\).md "wikilink")。[Super
    Junior成員](../Page/Super_Junior.md "wikilink")[起範暫停參與Super](../Page/起範.md "wikilink")
    Junior專輯發行等團體活動，專注影視發展，於2015年8月18日演員合約期滿，正式離開SM娛樂有限公司\[6\]。12月21日[Super
    Junior中國成員](../Page/Super_Junior.md "wikilink")[韓庚](../Page/韓庚.md "wikilink")，以不平等的合約而入稟法院要求與韩国SM娱乐有限公司解約，最終單飛回中國發展\[7\]\[8\]。

### 2011至2013年

  - 2011年4月8日，韩国SM娱乐有限公司宣佈與經理人公司[KEYEAST](../Page/KEYEAST.md "wikilink")、AM娛樂公司、Star
    J以及韩国三大娱乐公司之一的[YG
    Entertainment](../Page/YG_Entertainment.md "wikilink")、[JYP
    Entertainment等](../Page/JYP_Entertainment.md "wikilink")5大陣營攜手合作，成立[United
    Asia
    Management](../Page/United_Asia_Management.md "wikilink")（簡稱UAM）超大型國際經理人公司\[9\]
    。

<!-- end list -->

  - 2012年4月8日，韩国超人气偶像团体[EXO分別在](../Page/EXO.md "wikilink")[韓國及](../Page/韓國.md "wikilink")[中國同時出道](../Page/中國.md "wikilink")。EXO以超強人氣進軍中日韓市場，更加在出道兩年內獲得多個大獎及打破了多個世界紀錄。5月，新成立了子公司
    S.M Culture& Content。同年，成立了負責電視電影產業的SM C\&C（SM Culture &
    Contents）公司，積極開拓[電視劇和](../Page/電視劇.md "wikilink")[電影市場](../Page/電影.md "wikilink")，包括制作及投資。現任社長為金英敏，李秀滿為會長。9月19日，SM
    C\&C決定吸收合併旗下擁有[張東健和](../Page/張東健.md "wikilink")[金荷娜等明星的經紀公司AM娛樂](../Page/金荷娜.md "wikilink")\[10\]。

### 2014至2016年

  - 2014年3月21日，宣佈[安七炫和](../Page/安七炫.md "wikilink")[BoA被委任為非註冊董事](../Page/寶兒.md "wikilink")。兩人會以創意總監的身份去參與多個新事業計劃和內容，透過全球活動來展現出他們所累積的專門知識。另外，在21日舉行的股東大會中，金英敏、韓世敏、南昭英理事等等再次當選為董事會成員，而金英敏則繼續被委任為社長。

<!-- end list -->

  - 5月8日，韩国SM娱乐有限公司正式宣布与[百度达成战略合作关系](../Page/百度.md "wikilink")。通过此次战略合作，百度音乐、爱奇艺将获得SM旗下艺人正版数位音乐、MV、演出视频等在中国大陆地区的全面授权，同时将共同深度运营SM旗下艺人的官方贴吧。5月15日，[EXO-M隊長](../Page/EXO-M.md "wikilink")[Kris於首爾中央法院對公司提出](../Page/吳亦凡.md "wikilink")「合約再確認」的訴訟，並於2016年7月20日正式和解。8月1日，公司旗下新女子組合[Red
    Velvet出道](../Page/Red_Velvet.md "wikilink")。

<!-- end list -->

  - 9月30日，旗下王牌女子团体[少女時代成員](../Page/少女時代.md "wikilink")[鄭秀妍表示被公司告知退出少女時代](../Page/鄭秀妍.md "wikilink")。當日下午公司發表聲明，指出鄭秀妍因個人事業與團體活動的矛盾無法妥善處理，因此讓鄭秀妍離開少女時代，以個人身分進行演藝活動，而少女時代也將以八人之姿繼續活動\[11\]\[12\]。9月，推出項目‘BeatBurger
    Project’，意在聚集具有創造力的人才，共同創出屬於SM自己的舞蹈\[13\]。10月10日，[EXO-M成员](../Page/EXO-M.md "wikilink")[Luhan因自称身體亮起紅燈等原因](../Page/鹿晗.md "wikilink")，向韓國SM娛樂有限公司提出合約無效。

<!-- end list -->

  - 2015年4月23日，[EXO-M成员](../Page/EXO-M.md "wikilink")[Tao父親要求其與SM娛樂解約](../Page/黄子韜.md "wikilink")\[14\]。8月6日，[少女時代成員](../Page/少女時代.md "wikilink")[鄭秀妍與SM娛樂雙方發出聲明宣佈鄭秀妍將正式離开公司](../Page/鄭秀妍.md "wikilink")。8月7日，SM娛樂尊重[f(x)成員](../Page/f\(x\)_\(組合\).md "wikilink")[Sulli的決定](../Page/崔真理.md "wikilink")，同意讓其退出组合，從歌手身份改以演員身份繼續進行演藝活動，日後組合將以四人繼續以f(x)名義進行活動\[15\]。8月18日，[Super
    Junior成員](../Page/Super_Junior.md "wikilink")[起範與SM娛樂合約期滿](../Page/起範.md "wikilink")，正式離開公司並退出所屬組合\[16\]。

<!-- end list -->

  - 8月25日，SM娛樂推出新的演唱會系列品牌[THE
    AGIT](../Page/THE_AGIT.md "wikilink")，命名取自於「為SM的藝人們專門準備的用來招待歌迷的秘密活動指揮部（Agitpunkt的縮略）」。演唱會主要在韓國國內首度打造的輕量型公演場舉行，該公演場位於首爾三成洞SMTOWN
    THEATRE舉行，SM旗下的藝術家們以特別的概念展開接力演出。第一個帶來演出的是SHINee成員[鐘鉉](../Page/鐘鉉.md "wikilink")\[17\]。同日，SM娛樂宣布與體育運動行銷公司IB
    WORLDWIDE通過相互投資簽署了戰略聯盟，在娛樂和運動方面展開合作，並將商號改為「GALAXIA SM」。\[18\]。

<!-- end list -->

  - 11月6日，男子團體[Super
    Junior出道](../Page/Super_Junior.md "wikilink")10周年，同時所屬經紀公司SM娛樂宣布創立專屬廠牌[Label
    SJ](../Page/Label_SJ.md "wikilink")，此後將全面負責團體與成員個人之活動、專輯製作，公司方面將給予製作費及行銷上的支持。\[19\]。而[Label
    SJ的成立為](../Page/Label_SJ.md "wikilink")[Super
    Junior成員們與經紀公司商討兩年之結果](../Page/Super_Junior.md "wikilink")；營運方向由成員們及經紀人共同決策。\[20\]。

<!-- end list -->

  - 11月10日，[東亞日報訪問製作部本部長李聖秀](../Page/東亞日報.md "wikilink")
    (이성수)，本部長表示預計會在2016年推出多國籍新男子組合，也將設立
    Hip-Hop (嘻哈) 與 EDM (Electronic Dance Music / 電子舞曲)
    專門音樂品牌，也正在培訓許多抒情歌手\[21\]。11月18日，由浙江衛視、天娛傳媒、騰訊視頻聯合出品的青少年才藝養成勵志節目《燃燒吧少年》在北京富力萬麗酒店舉辦了發佈會。SM娛樂將一同參與冠軍團隊的專輯製作等，全方位提供支援。邁出SM娛樂計畫的Culture
    Technology中國當地語系化戰略的第一步\[22\]。

<!-- end list -->

  - 12月31日，宣布將與旗下擁有眾多知名模特兒、同時也是韓國國內首屈一指的模特兒綜合經紀公司 Esteem
    簽署戰略合作與持股投資合約，往後，兩家公司將共享資源，積極促進事業計畫，希望能創造互助效應，幫助彼此在全球娛樂市場的發展。而因為這是藝人經紀與模特兒經紀兩塊領域龍頭的合作，遂引起許多人的關注與討論。\[23\]。

<!-- end list -->

  - 2016年1月27日，在首爾三城洞SMTown Coex
    Artium舉辦發表會，創辦人李秀滿公開了即將新推出的各類企劃方案。將推出一年52周每週發行新曲的「[SM
    STATION](../Page/SM_STATION.md "wikilink")」，打破傳統的專輯銷售方式，以數碼單曲形式為歌迷帶來更持續、自由的音樂\[24\]。2月11日根據相關媒體報導，表示中國最大的電商產業集團「[阿里巴巴](../Page/阿里巴巴集团.md "wikilink")」對他們投資了355億韓元，將持有他們公司4%的股權，雙方會正式建立商業合作關係，攜手在中國市場推進在線音樂、宣傳、商品開發等方面的相關事業\[25\]。3月21日，SM
    C\&C正式宣佈將於6月设立全资子公司Woollim Entertainment\[26\]。4月9日和15日 ,
    公司旗下跨國籍男子組合[NCT之首支子團](../Page/NCT.md "wikilink")[NCT
    U分別在中國及韓國出道](../Page/NCT_U.md "wikilink")。
  - 2016年4月25日，SM娛樂設立新的EDM品牌ScreaM Records，ScreaM
    Records的首個作品是5月6日要發表的《Wave》並與Emart
    ElectroMart合作也是f(x)成員Amber、Luna參與的作品，將與國內DJ雙人組Xavi &
    Gi及海外有名的DJR3hab一起合作。\[27\]。

<!-- end list -->

  - 2016年5月27日，公司宣布與韓國知名補習班[鐘路學院合作](../Page/鐘路學院.md "wikilink")，將在首爾成立
    「K-POP國際學校」，致力於培養世界性大眾藝術人才\[28\]。
  - 2016年7月7日，公司旗下跨國籍男子組合NCT第二分隊NCT 127 在韓國正式出道。\[29\]2016年7月21日，與
    [EXO](../Page/EXO.md "wikilink") 離團成員
    [Kris](../Page/吳亦凡.md "wikilink")、[Luhan間的訴訟](../Page/鹿晗.md "wikilink")，
    長達約兩年的合約糾紛正式結束，三方當事人協議，合約將有效維持至2022年，但兩人不會作為EXO一員活動。且在韓國與日本的活动均需交由SM娛樂負責，而除此之外地區的活動則須向公司缴纳一定比例的收入，並正式退出組合\[30\]。

<!-- end list -->

  - 2016年8月12日，宣佈與Keyeast簽訂戰略性合作協議，推動多方面事業合作\[31\]。

<!-- end list -->

  - 2016年8月25日，公司旗下跨國籍男子組合NCT第三分隊NCT Dream 在韓國正式出道。

### 2017年至今

[缩略图](https://zh.wikipedia.org/wiki/File:SM_Entertainment_12.png "fig:缩略图")

  - 2017年1月3日[S.E.S.](../Page/S.E.S._\(组合\).md "wikilink")（SM的第一個女子團體）為紀念出道20周年重聚回歸韓國歌壇，在SM娛樂的協助之下發出紀念remix曲《Love\[story\]》，並發出以《Paradise》等為主打歌的紀念專輯《Remember》，在2017年舉辦出道20周年慈善演唱會。

<!-- end list -->

  - 2017年3月30日，公告投資[Mystic娛樂](../Page/Mystic娛樂.md "wikilink")，以28%股份成為最大投資者。\[32\]
  - 2017年11月1日，宣布更換舊有企業標識，代表色為粉、白、藍、灰，分別表示俐落、現代感性、革新、中性、知性。
  - 2017年12月18日，其旗下男團SHINee的27歲主音鐘鉉於韓國時間傍晚6點半於首爾清潭洞一間服務式住宅內被發現燒炭自殺身亡，初時情況危殆，心跳停止，被送往建大醫院，送院後證實不治。
  - 2018年3月14日，購買演員經紀公司[Keyeast部分股份](../Page/Keyeast.md "wikilink")\[33\]。並購買[FNC旗下FNC](../Page/FNC娛樂.md "wikilink")
    Add Culture部分股份，成為最大股東，與FNC達成戰略結盟。\[34\]
  - 2018年5月10日，宣布与越南最大型零售 &
    生活方式(lifestyle)企业联手战略合作，并签订MOU协议以推进共同事业。\[35\]
  - 2018年5月18日，以31%的持有股份完成了FNC ADD Culture的股權交替，FNC ADD
    Culture公司名稱正式改為“SM LIFE DESIGN
    GROUP”，FNC娛樂以18%的股份成為第二大股東。FNC的安碩俊理事今後將擔任SM LIFE
    DESIGN GROUP的經營顧問，通過SM LIFE DESIGN
    GROUP促進FNC與SM的戰略合作，實現綜合娛樂經紀產業聯合。\[36\]
  - 2018年9月5日，少女時代新小分隊「少女時代-Oh\! GG」出道并发行單曲專輯，分队由五位与公司续约的组合成員組成。
  - 2018年10月2日，SM Entertainment 在 Million Market 的股票持份多達
    50%，形同收購其為旗下品牌之一。目前雙方已經完成相關手續後，正在準備討論由 SM
    子公司委員擔任 Million Market 共同代表的方案。\[37\]
  - 2018年10月24日，SM Entertainment正式收購Million Market為旗下品牌，且由SM C\&C
    金代表擔任共同代表職。 \[38\]
  - 2019年1月17日，公司旗下中國籍及華裔男子組合NCT第四分隊WayV 在中國正式出道。

## 現有藝人

  - 斜體為目前已暫停活動。
  - 公司僅負責[Victoria](../Page/宋茜.md "wikilink")、[Lay及](../Page/張藝興.md "wikilink")[Amber韓國活動](../Page/劉逸雲.md "wikilink")。

### Solo歌手

  - [BoA](../Page/寶兒.md "wikilink")
  - [安七炫](../Page/安七炫.md "wikilink")
  - [J-Min](../Page/J-Min.md "wikilink")
  - *[喜悅Dana](../Page/喜悅Dana.md "wikilink")*
  - *智聲Sunday*
  - [泰民](../Page/李泰民.md "wikilink")
  - [周覓](../Page/周覓.md "wikilink")
  - [圭賢](../Page/圭賢.md "wikilink")
  - [Amber](../Page/劉逸雲.md "wikilink")
  - [瑜卤允浩](../Page/鄭允浩.md "wikilink")
  - [太妍](../Page/太妍.md "wikilink")
  - [最強昌珉](../Page/沈昌珉.md "wikilink")

<!-- end list -->

  - [厲旭](../Page/厲旭.md "wikilink")
  - [藝聲](../Page/藝聲.md "wikilink")
  - [Luna](../Page/朴善英.md "wikilink")
  - [Lay](../Page/張藝興.md "wikilink")
  - [孝淵](../Page/孝淵.md "wikilink")
  - [Victoria](../Page/宋茜.md "wikilink")
  - [俞利](../Page/俞利.md "wikilink")
  - [Key](../Page/Key_\(歌手\).md "wikilink")
  - [溫流](../Page/溫流.md "wikilink")
  - [Chen](../Page/金鐘大.md "wikilink")
  - [希澈](../Page/金希澈.md "wikilink")

### 組合

  - 男子组合

<!-- end list -->

  - [東方神起](../Page/東方神起.md "wikilink")
  - [TraxX](../Page/TraxX.md "wikilink")
  - [Super Junior](../Page/Super_Junior.md "wikilink")
  - [SHINee](../Page/SHINee.md "wikilink")
  - [EXO](../Page/EXO.md "wikilink")
  - [NCT](../Page/NCT.md "wikilink")

<!-- end list -->

  - 女子组合

<!-- end list -->

  - *[天上智喜](../Page/天上智喜.md "wikilink")*
  - [少女時代](../Page/少女時代.md "wikilink")
  - [f(x)](../Page/f\(x\)_\(組合\).md "wikilink")
  - [Red Velvet](../Page/Red_Velvet.md "wikilink")

### 子團組合

  - [天上智喜](../Page/天上智喜.md "wikilink")
      - *[天上智喜-Dana & Sunday](../Page/天上智喜.md "wikilink")*
  - [Super Junior](../Page/Super_Junior.md "wikilink")
      - *[Super
        Junior-K.R.Y.](../Page/Super_Junior-K.R.Y..md "wikilink")*
      - *[Super Junior-T](../Page/Super_Junior-T.md "wikilink")*
      - *[Super Junior-M](../Page/Super_Junior-M.md "wikilink")*
      - *[Super Junior-Happy](../Page/Super_Junior-Happy.md "wikilink")*
      - [Super Junior Donghae &
        Eunhyuk](../Page/Super_Junior_Donghae_&_Eunhyuk.md "wikilink")
  - [少女時代](../Page/少女時代.md "wikilink")
      - *[少女時代-太蒂徐](../Page/少女時代-太蒂徐.md "wikilink")*
      - [少女時代-Oh\! GG](../Page/少女時代-Oh!_GG.md "wikilink")

<!-- end list -->

  - [EXO](../Page/EXO.md "wikilink")
      - *[EXO-K](../Page/EXO-K.md "wikilink")*
      - *[EXO-M](../Page/EXO-M.md "wikilink")*
      - [EXO-CBX](../Page/EXO-CBX.md "wikilink")
  - [NCT](../Page/NCT.md "wikilink")
      - [NCT U](../Page/NCT.md "wikilink")
      - [NCT 127](../Page/NCT.md "wikilink")
      - [NCT DREAM](../Page/NCT.md "wikilink")
      - [WayV](../Page/NCT.md "wikilink")

### 限定/企划组合

  - 男子组合

<!-- end list -->

  -
  - [Kangta](../Page/安七炫.md "wikilink") &
    [Vanness](../Page/吳建豪.md "wikilink")

  - [金希澈&金政模](../Page/金希澈&金政模.md "wikilink")

  - [S.M. The Performance](../Page/S.M._The_Performance.md "wikilink")

  - [Toheart](../Page/Toheart.md "wikilink")

<!-- end list -->

  - 混合组合

<!-- end list -->

  - [S.M.THE BALLAD](../Page/S.M.THE_BALLAD.md "wikilink")
  - [Younique Unit](../Page/Younique_Unit.md "wikilink")
  - [Triple T](../Page/Triple_T.md "wikilink")

### 演員

  - 男演员

<!-- end list -->

  - [金旻鍾](../Page/金旻鍾.md "wikilink")
  - [李在龍](../Page/李在龍.md "wikilink")
  - [尹多勳](../Page/尹多勳.md "wikilink")
  - Hong Rocky
  - 崔钟允
  - 金京植
  - 金伊安
  - [李哲宇](../Page/李哲宇.md "wikilink")
  - 奇道勳

<!-- end list -->

  - 女演员

<!-- end list -->

  - [柳好貞](../Page/柳好貞.md "wikilink")
  - [李沇熹](../Page/李沇熹.md "wikilink")
  - [Sulli](../Page/崔真理.md "wikilink")

### 創作人

  - 作曲家

<!-- end list -->

  - [李秀滿](../Page/李秀滿.md "wikilink")（執行製作人）
  - Hitchhiker
  - Evans
  - [Kenzie](../Page/Kenzie.md "wikilink")
  - [俞永鎮](../Page/俞永鎮.md "wikilink")
  - 宋光植
  - [安七炫](../Page/安七炫.md "wikilink")
  - 朴秀賢

<!-- end list -->

  - 韓文作詞家

<!-- end list -->

  - [Kenzie](../Page/Kenzie.md "wikilink")
  - [俞永鎮](../Page/俞永鎮.md "wikilink")
  - Misfit
  - Seo Ji-eum
  - Kim Bu-min

<!-- end list -->

  - 中文作詞家

<!-- end list -->

  - [李源](../Page/李源.md "wikilink")
  - [王雅君](../Page/王雅君.md "wikilink")
  - 周煒傑

<!-- end list -->

  - 編舞家

<!-- end list -->

  - 黃尚勋
  - 沈在元
  - Kasper

<!-- end list -->

  - 唱歌老師

<!-- end list -->

  - 張鎮永

<!-- end list -->

  - 鋼琴家

<!-- end list -->

  - 宋光植

### [SM ROOKIES](../Page/SM_ROOKIES.md "wikilink") （已公開練習生）

目前練習生均為女性。

  - 女練習生

<!-- end list -->

  - 高恩
  - Hina
  - 寧寧
  - Lami

### 其他艺人

  - 与SM公司的合约经调解继续生效，并由公司负责其在韩国和日本的活动。
      - [Luhan](../Page/鹿晗.md "wikilink")
      - [Kris](../Page/吳亦凡.md "wikilink")
  - 艺人的合约无效诉讼经判决败诉，公司方面胜诉。
      - [TAO](../Page/黃子韜.md "wikilink")

## 旗下公司艺人

### S.M. Culture& Contents 旗下藝人

#### 演員

**男藝人**

  - [金秀路](../Page/金秀路.md "wikilink")

  - 姜弼錫

  - [金時厚](../Page/金時厚.md "wikilink")

  - 林炳根

  - 成斗燮

  - 金宰範

  - 朴振

  - 白鉉

  - 尹那武

  -
  - 朴正福

  - 鄭夏路

**女藝人**

  - [黃薪惠](../Page/黃薪惠.md "wikilink")
  - 金美靜
  - 金汝珍
  - 李采遠
  - 李娜允
  - 裴多彬

#### 主持

**男藝人**

  - 洪祿基

  - 李東宇

  - [姜鎬童](../Page/姜鎬童.md "wikilink")

  - 金京植

  - [申東燁](../Page/申東燁.md "wikilink")

  - [李壽根](../Page/李壽根.md "wikilink")

  - [金炳萬](../Page/金炳萬.md "wikilink")

  - [全炫茂](../Page/全炫茂.md "wikilink")

  - 金台鉉

  - 張東赫

  - [柳談](../Page/柳談.md "wikilink")

  - [张玉安](../Page/张玉安.md "wikilink")

  -
  -
**女藝人**

  - 吳靜妍

#### Woollim娱乐

### Mystic Entertainment旗下藝人

### Keyeast旗下藝人

## 影視製作

### 電視劇

  - 2009年
      - [向大地頭球](../Page/向大地頭球.md "wikilink")
  - 2011年
      - [天堂牧場](../Page/天堂牧場.md "wikilink")
  - 2012年
      - [致美麗的你](../Page/致美麗的你.md "wikilink")
  - 2013年
      - [總理與我](../Page/總理與我.md "wikilink")
      - [韓國小姐](../Page/韓國小姐_\(電視劇\).md "wikilink")

<!-- end list -->

  - 2014年
      - [MiMi](../Page/MiMi.md "wikilink")
  - 2015年
      - [我的鄰居是EXO](../Page/我的鄰居是EXO.md "wikilink")
      - [D-Day](../Page/D-Day_\(電視劇\).md "wikilink")
      - [生意之神－客主2015](../Page/生意之神－客主2015.md "wikilink")
  - 2016年
      - [鄰家律師趙德浩](../Page/鄰家律師趙德浩.md "wikilink")
      - [38師機動隊](../Page/38師機動隊.md "wikilink")
      - [嫉妒的化身](../Page/嫉妒的化身.md "wikilink")

<!-- end list -->

  - 2017年
      - [Missing9](../Page/Missing9.md "wikilink")
  - 2018年
      - [能先接吻嗎](../Page/能先接吻嗎.md "wikilink")
      - [油膩的Melo](../Page/油膩的Melo.md "wikilink")
      - [皇后的品格](../Page/皇后的品格.md "wikilink")

### 綜藝節目

  - 2012年
      - Vitamin
      - [神話放送](../Page/神話放送.md "wikilink")
  - 2013年
      - 人類的條件
      - 媽媽咪呀
      - [我們小區藝體能](../Page/我們小區藝體能.md "wikilink")
      - 尷尬學院
      - [Dancing 9](../Page/Dancing_9.md "wikilink")
      - [EXO's SHOWTIME](../Page/EXO's_SHOWTIME.md "wikilink")
  - 2014年
      - EXO 90:2014
      - [Super Junior-M](../Page/Super_Junior-M.md "wikilink") Guest
        House
  - 2015年
      - [channel 少女時代](../Page/channel_少女時代.md "wikilink")
      - 日常的Taeng9cam
      - [孝淵的百萬LIKE](../Page/孝淵的百萬LIKE.md "wikilink")
      - f(x)=1cm
      - The Mickey Mouse Club
      - [同床異夢，沒關係沒關係](../Page/同床異夢，沒關係沒關係.md "wikilink")

<!-- end list -->

  - 2016年
      - [孝淵的千萬LIKE](../Page/孝淵的千萬LIKE.md "wikilink")
      - [My SMT](../Page/My_SMT.md "wikilink")
      - [NCT LIFE](../Page/NCT_LIFE.md "wikilink")
      - [人生酒館](../Page/人生酒館.md "wikilink")
  - 2017年
      - [孝利家民宿](../Page/孝利家民宿.md "wikilink")
      - [认识的哥哥](../Page/认识的哥哥.md "wikilink")
      - [LEVEL UP PROJECT\!](../Page/LEVEL_UP_PROJECT!.md "wikilink")
      - [雪球項目](../Page/雪球項目.md "wikilink")
  - 2018年
      - [孝利家民宿](../Page/孝利家民宿.md "wikilink") S2
      - [LEVEL UP PROJECT\!
        S2](../Page/LEVEL_UP_PROJECT!_S2.md "wikilink")
      - [Keyword＃BoA](../Page/Keyword＃BoA.md "wikilink")
      - [秘密姐姐](../Page/秘密姐姐.md "wikilink")
      - [糧食日記](../Page/糧食日記.md "wikilink")

### 音樂劇

  - 2014年
      - Singin' In The Rain
  - 2015年
      - School OZ

## 過去旗下藝人

### 已离开艺人

  - 玄真英<small>（1990-1993）</small>
  - 韓東準<small>（1991-1992）</small>
  - 金光辰<small>（1991-1992）</small>
  - MAJOR
      - 徐妍秀<small>（1994）</small>
      - 尹本真<small>（1994）</small>
  - J\&J
      - Jay Kim<small>（1994）</small>
      - Jay Kang<small>（1994）</small>
  - [H.O.T.](../Page/H.O.T..md "wikilink")
      - [文熙俊](../Page/文熙俊.md "wikilink")<small>（1996-2005）</small>
      - 李在元<small>（1996-2001）</small>
      - [Tony An](../Page/安勝浩.md "wikilink")<small>（1996-2001）</small>
      - [張佑赫](../Page/張佑赫.md "wikilink")<small>（1996-2001）</small>
  - [S.E.S.](../Page/S.E.S..md "wikilink")<small>（1997-2002）</small>
  - [神話](../Page/神話_\(組合\).md "wikilink")<small>（1998-2003）</small>
  - [Fly to the
    Sky](../Page/Fly_to_the_Sky.md "wikilink")<small>（1999-2004）</small>
  - [張娜拉](../Page/張娜拉.md "wikilink")<small>（2001-2008）</small>
  - [M.I.L.K](../Page/M.I.L.K.md "wikilink")<small>（2001-2003）</small>
      - [徐玄振](../Page/徐玄振.md "wikilink")<small>（2001-2007）</small>
  - [Sugar](../Page/Sugar_\(組合\).md "wikilink")<small>（2001-2006）</small>
  - Black Beat<small>（2002-2007）</small>
  - [神飛樂團](../Page/神飛樂團.md "wikilink")<small>（2002-2003）</small>
  - Isak N Jiyeon<small>（2002-2004）</small>
      - Isak<small>（2002-2012）</small>

<!-- end list -->

  - [東方神起](../Page/東方神起.md "wikilink")
      - [金在中](../Page/金在中.md "wikilink")<small>（2003-2009）</small>
      - [朴有天](../Page/朴有天.md "wikilink")<small>（2003-2009）</small>
      - [金俊秀](../Page/金俊秀.md "wikilink")<small>（2003-2009）</small>
  - [TraxX](../Page/TraxX.md "wikilink")
      - 姜政佑<small>（2004-2006）</small>
      - [魯敏宇](../Page/魯敏宇.md "wikilink")<small>（2004-2009）</small>
  - [天上智喜](../Page/天上智喜.md "wikilink")
      - 天舞Stephanie<small>（2005-2016）</small>
  - [Super Junior](../Page/Super_Junior.md "wikilink")
      - [韓庚](../Page/韓庚_\(藝人\).md "wikilink")<small>（2005-2009）</small>
      - [起範](../Page/起範.md "wikilink")<small>（2004-2015）</small>
  - [少女時代](../Page/少女時代.md "wikilink")
      - [潔西卡](../Page/鄭秀妍.md "wikilink")<small>（2007-2015）</small>
      - [蒂芬妮](../Page/黃美英.md "wikilink")<small>（2007-2017）</small>
      - [秀英](../Page/崔秀榮.md "wikilink")<small>（2007-2017）</small>
      - [徐玄](../Page/徐朱玄.md "wikilink")<small>（2007-2017）</small>
  - [尹博](../Page/尹博.md "wikilink")<small>（2009-2013）</small>
  - 秋家十<small>（2002-2012）</small>
  - [高雅罗](../Page/高雅罗.md "wikilink")<small>（2003-2016）</small>
  - [張力尹](../Page/張力尹.md "wikilink")<small>（2006-2017）</small>
  - [金荷娜](../Page/金荷娜.md "wikilink")<small>（2013-2017）</small>
  - [張東健](../Page/張東健.md "wikilink")<small>（2013-2017）</small>
  - [SHINee](../Page/SHINee.md "wikilink")
      - [鐘鉉](../Page/金鐘鉉_\(歌手\).md "wikilink")<small>（2008-2018）</small>
  - [強藝元](../Page/強藝元.md "wikilink")<small>（2013-2018）</small>
  - [Super Junior-M](../Page/Super_Junior-M.md "wikilink")
      - [Henry](../Page/劉憲華.md "wikilink")<small>（2008-2018）</small>

<!-- end list -->

  -
    <small>
    在[H.O.T.解散之後](../Page/H.O.T..md "wikilink")，[張佑赫](../Page/張佑赫.md "wikilink")、[安勝浩](../Page/安勝浩.md "wikilink")、李在元離開公司，成立新组合JTL。</small>
    <small> 因2017年為S.E.S.成軍20年，成員為紀念及慶祝故重聚並與SM娛樂合作推出特別專輯及舉行演唱會。</small>
    <small> Black
    Beat於2002年解散后，鄭智勳、李小民离开公司。張鎮永担任公司的發聲老師，訓練練習生，而黃尙勳和沈在元則是公司的舞蹈老師及編舞家。</small>
    <small> Isak N Jiyeon解散後，Isak成为[Arirang
    TV的VJ和電台DJ](../Page/阿里郎電視台.md "wikilink")。</small>
    <small>
    2003年以東方神起成员出道的[金在中](../Page/金在中.md "wikilink")、[朴有天及](../Page/朴有天.md "wikilink")[金俊秀于](../Page/金俊秀.md "wikilink")2009年以收入分配和不平等合約等問題向公司提出訴訟，中止合約，並在2010年組成[JYJ](../Page/JYJ.md "wikilink")。</small>
    <small> 2005年以Super
    Junior成員出道的[韓庚在](../Page/韓庚_\(藝人\).md "wikilink")2009年12月以不平等合約而入稟法院要求與公司解約，2010年6月22日，正式離開公司及退出Super
    Junior。</small>
    <small> 2005年以Super
    Junior成员出道的[起範](../Page/起範.md "wikilink")，为專注影視發展，自2009年4月起，不參與團體專輯和宣傳活動，仅掛名在Super
    Junior團體。之後在2015年8月18日，合約到期并不續約，正式離開公司及退出Super Junior。</small>
    <small>
    2007年以少女時代成員出道的[潔西卡在](../Page/鄭秀妍.md "wikilink")2014年9月30日被宣佈退出少女時代。2015年8月6日，通过与SM娛樂协议，正式解約并離開公司。2016年2月29日，與Coridel娛樂公司簽約。目前以SOLO歌手及時裝設計師身份活動。</small>
    <small>
    2008年以SHINee成員出道的[鐘鉉於](../Page/金鐘鉉_\(歌手\).md "wikilink")2017年12月18日逝世，官方與2018年1月23日正式發表鐘鉉遺作。</small>

### 曾作为公司的练习生

  - 男练习生

<small>**演員**</small>

  - [郭明振](../Page/郭时暘.md "wikilink")（藝名為**郭时暘**）
  - [李鍾硕](../Page/李鍾硕.md "wikilink")
  - [李準基](../Page/李準基.md "wikilink")

<small>**歌手**</small>

  - 丁泽仁（[乐华七子](../Page/乐华七子.md "wikilink")）
  - 韓沐伯（Awaken-F）
  - [許永生](../Page/許永生.md "wikilink")（[SS501](../Page/SS501.md "wikilink")、[SS301](../Page/SS301.md "wikilink")）
  - [侯明昊](../Page/侯明昊.md "wikilink")
  - [黃仁虎](../Page/黃仁虎.md "wikilink")（[IN2IT](../Page/IN2IT.md "wikilink")）
  - 池韓率（NewKidd）
  - [趙珍虎](../Page/趙珍虎.md "wikilink")（[PENTAGON](../Page/Pentagon_\(男子組合\).md "wikilink")）
  - [鄭丙熙](../Page/G.O.md "wikilink")（[MBLAQ](../Page/MBLAQ.md "wikilink")，藝名為**G.O**）
  - [金力燦](../Page/金力燦.md "wikilink")
  - [金仁性](../Page/仁誠.md "wikilink")（[SF9](../Page/SF9.md "wikilink")）
  - [金振煥](../Page/金振煥.md "wikilink")（[iKON](../Page/iKON.md "wikilink")，藝名為**Jay**）
  - [金鍾旼](../Page/金鍾旼.md "wikilink")（[高耀太](../Page/高耀太.md "wikilink")）
  - 金文奎（[HOTSHOT](../Page/HOTSHOT.md "wikilink")，藝名為**Timoteo**）
  - 金湘訓（藝名為**C:ODE**）
  - 金宇珍（[Stray Kids](../Page/Stray_Kids.md "wikilink")）
  - 權宰煥（[N-SONIC](../Page/N-SONIC.md "wikilink")，藝名為**J-Heart**）
  - [权志龙](../Page/权志龙.md "wikilink")（[BIGBANG](../Page/BIGBANG.md "wikilink")，藝名為**G-Dragon**）
  - 权顺日（[城市札卡巴](../Page/城市札卡巴.md "wikilink")）
  - [李東憲](../Page/李東憲.md "wikilink")（[VERIVERY](../Page/VERIVERY.md "wikilink")）
  - 李長埈（[Golden Child](../Page/Golden_Child.md "wikilink")）
  - [李成烈](../Page/李成烈.md "wikilink")（[INFINITE](../Page/INFINITE.md "wikilink")）
  - [朴志訓](../Page/朴志訓.md "wikilink")
  - [禹智皓](../Page/Zico_\(歌手\).md "wikilink")（藝名為**Zico**）

<small>**參賽者**</small>

  - 李和英（《[少年24](../Page/少年24.md "wikilink")》）
  - 朴塗荷（《[少年24](../Page/少年24.md "wikilink")》、《[MIX
    NINE](../Page/MIX_NINE.md "wikilink")》）
  - [朴容主](../Page/朴容主.md "wikilink")（《星動亞洲》 S1 & S2、《[The
    Fan](../Page/The_Fan.md "wikilink")》）
  - 周彥辰（《[偶像練習生](../Page/偶像練習生.md "wikilink")》）

<!-- end list -->

  - 女练习生

<small>**演員**</small>

  - [安昭熙](../Page/安昭熙.md "wikilink")
  - [趙賢榮](../Page/趙賢榮.md "wikilink")
  - [高娜恩](../Page/高娜恩_\(藝人\).md "wikilink")
  - [具荷拉](../Page/具荷拉.md "wikilink")
  - [具惠善](../Page/具惠善.md "wikilink")
  - [許嘉允](../Page/許嘉允.md "wikilink")
  - [黄宝罗](../Page/黄宝罗.md "wikilink")
  - 金賢京
  - 金敏智
  - [朴奎利](../Page/朴奎利.md "wikilink")
  - [俞昇延](../Page/孔昇延.md "wikilink")（藝名為**孔升延**）

<small>**歌手**</small>

  - [程瀟](../Page/程瀟.md "wikilink")（[宇宙少女](../Page/宇宙少女.md "wikilink")）
  - [崔智秀](../Page/崔智秀.md "wikilink")（[ITZY](../Page/ITZY.md "wikilink")，藝名為**Lia**）
  - [咸𤨒晶](../Page/咸𤨒晶.md "wikilink")（[T-ara](../Page/T-ara.md "wikilink")，除本名另有藝名**Elsie**）
  - 張知秀（[七學年一班](../Page/七學年一班.md "wikilink")，藝名為**白世熙**）
  - 趙河朗（藝名為**趙敏雅**）
  - [金知妍](../Page/苞娜.md "wikilink")（[宇宙少女](../Page/宇宙少女.md "wikilink")，藝名為**苞娜**）
  - 金容媛（[C-REAL](../Page/C-REAL.md "wikilink")，藝名為**Chemi**）
  - [李孝利](../Page/李孝利.md "wikilink")
  - 李欢熙
  - [李露多](../Page/李露多.md "wikilink")（[宇宙少女](../Page/宇宙少女.md "wikilink")）
  - [李宣美](../Page/李宣美.md "wikilink")
  - 权志晏（藝名為**率悲**）
  - [吳惠麟](../Page/Raina.md "wikilink")（[After
    School](../Page/After_School.md "wikilink")、[橙子焦糖](../Page/橙子焦糖.md "wikilink")，藝名為**萊娜**）
  - 朴銀㩼
  - [朴祉映](../Page/嘉熙_\(歌手\).md "wikilink")（藝名為**朴嘉熙**）
  - [朴昭妍](../Page/朴昭妍.md "wikilink")
  - [沈泫京](../Page/沈泫京.md "wikilink")
  - [宋多惠](../Page/宋多惠.md "wikilink")
  - [俞延靜](../Page/俞璉靜.md "wikilink")（[宇宙少女](../Page/宇宙少女.md "wikilink")，藝名為**璉靜**）

<small>**參賽者**</small>

  - 許讚美（《[PRODUCE 101](../Page/PRODUCE_101.md "wikilink")》、《[MIX
    NINE](../Page/MIX_NINE.md "wikilink")》）
  - 徐慧仁（《[偶像學校](../Page/偶像學校.md "wikilink")》）
  - [徐藝洋](../Page/徐藝洋.md "wikilink")（《[中国梦之声·下一站传奇](../Page/中国梦之声·下一站传奇.md "wikilink")》）

## 參考資料

## 外部連結

  - [SMTOWN 韓國官方網站](http://www.smtown.com)

  - [SMTOWN 日本官方網站](https://smtown.jp/)

  - [SM Entertainment 集團官網](http://www.smentertainment.com/)

  - [S.M. Culture& Contents 官網](http://www.smcultureandcontents.com/)

  - [SM Entertainment Naver Tvcast](http://tvcast.naver.com/smtown)

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  - [SMTOWN](https://channels.vlive.tv/FD53B/video)的V LIVE頻道

  - [SM CCC LAB](https://channels.vlive.tv/CDF627/video)的V LIVE頻道

  - [SM娛樂演員-BLOG](https://post.naver.com/my.nhn?memberNo=22324545)

[Category:1989年成立的公司](../Category/1989年成立的公司.md "wikilink")
[Category:韓國證券交易所上市公司](../Category/韓國證券交易所上市公司.md "wikilink")
[Category:韓國藝人經紀公司](../Category/韓國藝人經紀公司.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.

9.

10.

11.

12.

13. [BeatBurger
    Project](https://hk.celebrity.yahoo.com/news/影片-sm推出-beatburger-計劃-釋出泰民-ace-影片吸睛-101118587.html)

14.

15.

16.

17. 韓星網（中文）

18. 韓星網（中文）

19.

20.

21. kpopn.com（中文）

22.

23.

24. 韓星網（中文）

25. 自由時報電子報（中文）

26.
27.

28.

29. [SM新男團NCT再度出撃！泰容及WINWIN組成「NCT-127」，本週公開出道舞台！](http://www.vlovekpop.com/20160704-nct-127/)
    vlovekpop

30.

31.

32.

33.

34.

35.

36.

37.

38.