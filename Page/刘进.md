**劉進**，[漢武帝之孫](../Page/漢武帝.md "wikilink")，[太子](../Page/皇太子.md "wikilink")[劉據與](../Page/劉據.md "wikilink")[史良娣之子](../Page/史良娣.md "wikilink")，號“史皇孫”，其父在[巫蠱之亂中被奸臣迫害](../Page/巫蛊之祸.md "wikilink")，舉兵反抗，兵敗逃亡，後來[自殺](../Page/自殺.md "wikilink")，劉進也隨之遇害。劉進之子[劉詢後來登上帝位](../Page/劉詢.md "wikilink")，是為漢宣帝。

劉進在正史中可考的妻妾，僅有家人子[王翁須](../Page/王翁須.md "wikilink")（皇孫的妻妾並無號位，皆稱[家人子](../Page/家人子.md "wikilink")），即漢宣帝生母。史書多稱其為“王夫人”，亦受到牽連死於[巫蠱之禍](../Page/巫蠱之禍.md "wikilink")。劉詢繼位後，追谥父親為“悼”，生母為“悼-{后}-”。

<center>

</center>

## 參考資料

  - [漢書](../Page/漢書.md "wikilink")

[Category:西漢宗室](../Category/西漢宗室.md "wikilink")
[J](../Category/刘姓.md "wikilink")