[Chung_Fu_Shopping_Centre_Phase_2.jpg](https://zh.wikipedia.org/wiki/File:Chung_Fu_Shopping_Centre_Phase_2.jpg "fig:Chung_Fu_Shopping_Centre_Phase_2.jpg")
[HK_ChungFuShoppingCentre.jpg](https://zh.wikipedia.org/wiki/File:HK_ChungFuShoppingCentre.jpg "fig:HK_ChungFuShoppingCentre.jpg")入口（2008年）\]\]
**T
Town**是一個位於[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")[天水圍](../Page/天水圍.md "wikilink")[天華路](../Page/天華路.md "wikilink")30至33號的購物商場，於1999年至2000年分階段落成。T
Town橫跨[天水圍新市鎮的南部和北部](../Page/天水圍新市鎮.md "wikilink")，是天水圍人流最多的購物商場之一，亦是[領展房地產投資信託基金](../Page/領展房地產投資信託基金.md "wikilink")（領展）的重點發展商場之一。該商場由[香港房屋委員會興建](../Page/香港房屋委員會.md "wikilink")、擁有及管理，於2005年售予「領匯房地產信託基金」（現已改名為[領展房地產信託基金](../Page/領展房地產信託基金.md "wikilink")）。該商場分為一期和二期，由橫跨天華路的密封式天橋連接，商場樓高三層，建築樓面面積達36,700平方米。商場設有[籃球場](../Page/籃球場.md "wikilink")、[排球場](../Page/排球場.md "wikilink")、[網球場和多層](../Page/網球場.md "wikilink")[停車場](../Page/停車場.md "wikilink")。商場落成時名為**頌富商場**（）\[1\]，2010年更名為**頌富廣場**\[2\]（Chung
Fu
Plaza），到2017年3月改稱現名；舊名源於商場位處[天頌苑和](../Page/天頌苑.md "wikilink")[天富苑之間的位置](../Page/天富苑.md "wikilink")。

## T Town South

[Chung_Fu_Plaza_Void_Access_201503.jpg](https://zh.wikipedia.org/wiki/File:Chung_Fu_Plaza_Void_Access_201503.jpg "fig:Chung_Fu_Plaza_Void_Access_201503.jpg")
T Town
South（原名頌富商場／廣場一期）於1999年年底落成，位於天華路30號，主要為[天頌苑](../Page/天頌苑.md "wikilink")、[天恩邨和](../Page/天恩邨.md "wikilink")[天華邨居民服務](../Page/天華邨.md "wikilink")。商場設有多層停車場，亦設有籃球場、排球場和網球場，為居於該商場附近的居民提供一個康樂設施。商場地下曾設有街市，與二期街市曾由不同承辦商同時營運，其後被改為零售舖位，由超級市場承租。

另外，領匯房產基金曾在2012年對頌富廣場一期二樓進行「優化工程」，以「引入多元化商舖」和「提升購物環境」。然而，原來位於頌富廣場一期二樓的商舖卻因此而結業。當地居民和區議員指出領匯這樣做使居民難以在該商場內好好的吃一頓飯，又質疑「優化工程」需時過長。他們批評領匯此舉漠視了居民的需要\[3\]
。

### 知名商舗

  - [大家樂](../Page/大家樂.md "wikilink")

  - [麥當勞](../Page/麥當勞.md "wikilink")

  - [吉野家](../Page/吉野家.md "wikilink")

  - [譚仔三哥米線](../Page/譚仔三哥米線.md "wikilink")

  - [鴻褔堂](../Page/鴻褔堂.md "wikilink")

  - [板長壽司](../Page/板長壽司.md "wikilink")

  -
  - [商務印書館](../Page/商務印書館_\(香港\).md "wikilink")

  - [中國移動香港](../Page/中國移動香港.md "wikilink")

  - [Three.](../Page/3_\(電訊\).md "wikilink")

  - [張毛記電業](../Page/張毛記.md "wikilink")

  - [Uniqlo](../Page/Uniqlo.md "wikilink")

  - [Circle K](../Page/Circle_K.md "wikilink")

## T Town North

[T_Town_North_GF_201901.jpg](https://zh.wikipedia.org/wiki/File:T_Town_North_GF_201901.jpg "fig:T_Town_North_GF_201901.jpg")
[T_Town_North_Level_1_201901.jpg](https://zh.wikipedia.org/wiki/File:T_Town_North_Level_1_201901.jpg "fig:T_Town_North_Level_1_201901.jpg")
T Town
North（原名頌富商場／廣場二期）於2000年落成，位於天華路33號，主要為[天富苑和](../Page/天富苑.md "wikilink")[天悅邨居民服務](../Page/天悅邨.md "wikilink")。由於進行「資產提升工程」的關係，除了T
Town街市，T Town
North其他部分都已在2016年4月被封閉，只保留行人通道，有街市商戶更指管理公司談及續約時未有提及商場即將進行裝修，指空氣不流通和多個出入口臨時封閉下，人流與生意大不如前。\[4\]翻新工程於2017年4月完工。

### 主要商舗

  - [7-11便利店](../Page/7-11便利店.md "wikilink")

  - [Baleno](../Page/Baleno.md "wikilink")

  - [Bossini](../Page/Bossini.md "wikilink")

  - [雞仔嘜](../Page/雞仔嘜.md "wikilink")

  -
  - fusion by [PARKNSHOP](../Page/百佳超級市場.md "wikilink")

  - Magic Touch（[爭鮮旗下](../Page/爭鮮.md "wikilink")）

  - [Delifrance](../Page/Delifrance.md "wikilink")

  - [叙福樓金閣](../Page/叙福樓集團.md "wikilink")

  - [榮華餅家](../Page/榮華餅家.md "wikilink")

  - [美心西餅](../Page/美心西餅.md "wikilink")

  - [屈臣氏](../Page/屈臣氏.md "wikilink")

  - [位元堂](../Page/位元堂.md "wikilink")

  - [華潤堂](../Page/華潤堂.md "wikilink")

  - [維特健靈專門店](../Page/維特健靈.md "wikilink")

### 街市

[T_MARKET_Interior_201901.jpg](https://zh.wikipedia.org/wiki/File:T_MARKET_Interior_201901.jpg "fig:T_MARKET_Interior_201901.jpg")
頌富街市原由予現代管理公司負責管理，到2018年10月1日由新承辦商[建華集團以](../Page/建華集團.md "wikilink")「香港街市」品牌營辦，進行三個月裝修後在同年12月30日開幕，命名為「T
MARKET 頌富市場」。街市以日本無印木系為主題，其中小食街引入超過10間的食店，營業至凌晨1點。

## 圖集

HK ChungFuShoppingCentre Interior1.jpg|翻新前頌富商場入口中庭（2008年） HK
ChungFuShoppingCentre Interior2.jpg|翻新前頌富商場內部（2008年） HK
ChungFuShoppingCentre Interior3.jpg|翻新前頌富商場內部（2008年） Chung Fu Plaza
Level 3 Shops 2015.jpg|翻新後T Town South 2樓商店 (2014年) Chung Fu Shopping
Centre Phase 2 in 2014.JPG|翻新後的頌富廣場二期外牆（2014年） Chung Fu Market
2015.jpg|翻新前的頌富街市（2015年） Temporary T
Market.jpg|頌富街市於2018年10月1日起翻新3個月，並於T Town
North設臨時街市T Market T Market Entrance.jpg|頌富市場入口 T Market
Fishmonger.jpg|頌富市場魚檔 T Market Butcher and Fruit Shop.jpg|頌富市場肉檔及水果檔
Restaurant at T Market.jpg|頌富市場的美食街 T Town.jpg|翻新中的T Town
North（2016年12月）

## 交通

<table>
<thead>
<tr class="header">
<th><p>交通路線列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/港鐵.md" title="wikilink">港鐵</a><a href="../Page/香港輕鐵.md" title="wikilink">輕鐵</a><a href="../Page/頌富站.md" title="wikilink">頌富站</a>：
<ul>
<li><a href="../Page/香港輕鐵705、706線.md" title="wikilink">705、706線</a>、<a href="../Page/香港輕鐵751線.md" title="wikilink">751線、751P線</a>、<a href="../Page/香港輕鐵761P線.md" title="wikilink">761P線</a></li>
</ul></li>
<li><a href="../Page/香港巴士.md" title="wikilink">巴士</a>：</li>
</ul></td>
</tr>
</tbody>
</table>

## 參見

  - [天頌苑](../Page/天頌苑.md "wikilink")
  - [天華邨](../Page/天華邨.md "wikilink")
  - [天富苑](../Page/天富苑.md "wikilink")
  - [天悅邨](../Page/天悅邨.md "wikilink")
  - [天水圍新市鎮](../Page/天水圍新市鎮.md "wikilink")
  - [香港商場列表](../Page/香港商場列表.md "wikilink")

## 參考文獻

<div class="references-small">

<references>

</references>

</div>

## 外部連結

  - [T
    Town](http://www.linkhk.com/_layouts/CustomerSite_ShopCentreDetail.aspx?lng=tc&shopping_id=135&type=false)

[category:香港建築小作品](../Page/category:香港建築小作品.md "wikilink")
[category:元朗區商場](../Page/category:元朗區商場.md "wikilink")
[category:天水圍](../Page/category:天水圍.md "wikilink")

[Category:領展商場及停車場](../Category/領展商場及停車場.md "wikilink")

1.  [「領匯非常學堂」 之
    陶傑與禇簡寧-不中不英樂在其中](http://www.thelinkreit.com/tc/promo/event_archive_photo.asp?id=352)
2.
3.
4.