**彭桓武**（），中国理论物理学家，[中国科学院院士](../Page/中国科学院院士.md "wikilink")，[两弹一星元勋之一](../Page/两弹一星元勋.md "wikilink")。

## 生平

彭桓武祖籍[湖北省](../Page/湖北省.md "wikilink")[麻城市铁门王岗](../Page/麻城市.md "wikilink")，1915年出生于[吉林省](../Page/吉林省.md "wikilink")[长春市](../Page/长春市.md "wikilink")，1935年从[清华大学物理系毕业后继续攻读研究生](../Page/清华大学.md "wikilink")。1937年7月[抗战爆发后](../Page/抗战.md "wikilink")，辗转赴[云南大学理化系执教](../Page/云南大学.md "wikilink")。1938年，彭桓武考取中英[庚款留学生资格](../Page/庚款.md "wikilink")，赴英国[爱丁堡大学留学](../Page/爱丁堡大学.md "wikilink")，在著名物理学家[马克斯·玻恩的指导下工作](../Page/马克斯·玻恩.md "wikilink")。分别于1940年和1945年在爱丁堡大学获得哲学和科学博士学位。

1941年，彭桓武在玻恩的推荐下赴[爱尔兰](../Page/爱尔兰.md "wikilink")[都柏林高等研究所工作](../Page/都柏林高等研究所.md "wikilink")，1941年8月至1943年7月期间，彭桓武和[海特勒](../Page/瓦尔特·海特勒.md "wikilink")、J.Hamilton合作研究[宇宙线](../Page/宇宙线.md "wikilink")，发展了[HHP理论](../Page/HHP理论.md "wikilink")。1945年，彭桓武与玻恩获得[爱丁堡皇家学会的](../Page/爱丁堡皇家学会.md "wikilink")[麦克杜加尔-布列兹班奖](../Page/麦克杜加尔-布列兹班奖.md "wikilink")。1948年，彭桓武当选为[爱尔兰皇家科学院院士](../Page/爱尔兰皇家科学院.md "wikilink")。

1947年，彭桓武辗转回到中国，先后在[云南大学](../Page/云南大学.md "wikilink")、清华大学、[北京大学](../Page/北京大学.md "wikilink")、[中国科学技术大学任教](../Page/中国科学技术大学.md "wikilink")，曾参与并领导了中国的[原子弹](../Page/原子弹.md "wikilink")、[氢弹的研制计划](../Page/氢弹.md "wikilink")。还先后担任[中国科学院近代物理研究所副所长](../Page/中国科学院近代物理研究所.md "wikilink")、[中国科学院高能物理研究所副所长等职](../Page/中国科学院高能物理研究所.md "wikilink")，并在1978年到1983年期间任[中国科学院理论物理研究所所长](../Page/中国科学院理论物理研究所.md "wikilink")。

## 荣誉

1982年获得中国[国家自然科学奖一等奖](../Page/国家自然科学奖.md "wikilink")，1985年获两项中国[国家科技进步奖特等奖](../Page/国家科技进步奖.md "wikilink")，1995年获[何梁何利基金科学与技术成就奖](../Page/何梁何利基金.md "wikilink")，并在1999年被授予“[两弹一星功勋奖章](../Page/两弹一星功勋奖章.md "wikilink")”。为纪念他对中国核物理做出的贡献，第48798号[小行星以他的名字命名为](../Page/小行星.md "wikilink")“”。

## 外部链接

  - [周光召讲述导师彭桓武遗愿](http://scitech.people.com.cn/GB/7456513.html)

{{-}}

[Category:理论物理学家](../Category/理论物理学家.md "wikilink")
[Category:中华人民共和国理论物理学家](../Category/中华人民共和国理论物理学家.md "wikilink")
[Category:中国核物理学家](../Category/中国核物理学家.md "wikilink")
[Category:清华大学校友](../Category/清华大学校友.md "wikilink")
[Category:庚子赔款留学生](../Category/庚子赔款留学生.md "wikilink")
[Category:长春人](../Category/长春人.md "wikilink")
[Huanwu](../Category/彭姓.md "wikilink")