**范遵**\[1\]（，）是[越南首位](../Page/越南.md "wikilink")[太空人](../Page/太空人.md "wikilink")，也是全[亞洲](../Page/亞洲.md "wikilink")，以至[第三世界國家遠征](../Page/第三世界.md "wikilink")[太空的第一人](../Page/太空.md "wikilink")，同時也是少數獲頒[蘇聯英雄勳章的外國人](../Page/蘇聯英雄.md "wikilink")。

范出生于越南北部的[太平省](../Page/太平省.md "wikilink")，他於1965年加入[北越空軍](../Page/北越.md "wikilink")，1967年毕业于前[苏联军事飞行学校](../Page/苏联.md "wikilink")，與支援南越的美軍交戰至1975年[越戰結束](../Page/越戰.md "wikilink")。

1972年12月27日，范於河內擊落了美軍的[B-52轟炸機](../Page/B-52.md "wikilink")，成為擊落此類軍機的第一人，隨後他獲得多個榮譽，包括胡志明勳章、[列寧勳章](../Page/列寧勳章.md "wikilink")、蘇聯英雄勳章等。

范於1979年4月1日獲選進入[蘇聯的太空計劃](../Page/蘇聯.md "wikilink")，其後備人選為（Bùi Thanh
Liêm）。1980年7月23日，當時仍為空軍[中尉的范遵](../Page/中尉.md "wikilink")，乘坐飛船發射升空，與蘇聯太空人[戈爾巴特科同行](../Page/維克托·戈爾巴特科.md "wikilink")，前往[禮炮6號太空站執行任務](../Page/禮炮6號.md "wikilink")，包括測試礦物样本在微重力狀態下的溶化實驗，又在越南上空拍下照片作測量之用。至7月31日完成任務返回地球，前後在太空共逗留7天20小時42分，並繞地球轉了142圈。　　　

1999年晋升[中將](../Page/中將.md "wikilink")。最高任职为越南国防工业总局局长。范已于2008年1月退休。

現時范遵已婚，育有一子一女。

## 参考文献

　　

## 外部链接

  - [范遵
    （Spacefacts網站）](http://www.spacefacts.de/bios/international/english/tuan_pham.htm)

[T](../Page/category:范姓.md "wikilink")

[Category:外籍蘇聯英雄](../Category/外籍蘇聯英雄.md "wikilink")
[Category:列寧勳章獲得者](../Category/列寧勳章獲得者.md "wikilink")
[Category:越南勞動英雄獲得者](../Category/越南勞動英雄獲得者.md "wikilink")
[Category:胡志明勳章獲得者](../Category/胡志明勳章獲得者.md "wikilink")
[Category:越南太空人](../Category/越南太空人.md "wikilink")
[Category:越南將軍](../Category/越南將軍.md "wikilink")
[Category:越南空軍人物](../Category/越南空軍人物.md "wikilink")
[Category:越南越戰人物](../Category/越南越戰人物.md "wikilink")
[Category:越南共產黨黨員](../Category/越南共產黨黨員.md "wikilink")
[Category:太平省人](../Category/太平省人.md "wikilink")
[Category:空軍中將](../Category/空軍中將.md "wikilink")

1.