**水仙尊王**，簡稱**水仙王**，是[中國](../Page/中國.md "wikilink")[海神之一](../Page/海神.md "wikilink")，以[貿易商人](../Page/貿易.md "wikilink")、[船員](../Page/船員.md "wikilink")、[漁夫最為信奉](../Page/漁夫.md "wikilink")。早年民眾所稱的水仙王是海神[媽祖的五名](../Page/媽祖.md "wikilink")[從神](../Page/從神.md "wikilink")，為馮璿兄弟三人及蔡某、丁仲修等五人\[1\]。但是時至今日，各地供奉的水仙尊王各有不同，通常以善於治水的[夏禹為主](../Page/夏禹.md "wikilink")，再加上其他英雄才子、忠臣烈士如[伍子胥](../Page/伍子胥.md "wikilink")、[屈原等人合併供奉](../Page/屈原.md "wikilink")，稱為「**諸水仙王**」。

## 名單

  - [夏禹](../Page/夏禹.md "wikilink")：古代賢君，[三官大帝中的](../Page/三官大帝.md "wikilink")[水官大帝](../Page/水官大帝.md "wikilink")，曾經治水。
  - [伯益](../Page/伯益.md "wikilink")：[夏禹助手](../Page/夏禹.md "wikilink")，據說首先鑿[井](../Page/井.md "wikilink")。
  - [寒澆](../Page/寒澆.md "wikilink")：[夏朝時](../Page/夏朝.md "wikilink")，[寒浞之子](../Page/寒浞.md "wikilink")，力大無窮，能陸地行舟。
  - [魯班](../Page/魯班.md "wikilink")：[魯國知名工匠](../Page/魯國.md "wikilink")，相傳善製船隻。
  - [伍員](../Page/伍員.md "wikilink")：[吳國忠臣](../Page/吳國.md "wikilink")，字子胥。被陷害而[自盡](../Page/自盡.md "wikilink")，遺體被[吳王](../Page/吳.md "wikilink")[夫差丟入河底](../Page/夫差.md "wikilink")。
  - [屈原](../Page/屈原.md "wikilink")：[楚國愛國詩人](../Page/楚國.md "wikilink")，曾任[三閭大夫](../Page/三閭大夫.md "wikilink")，不被[楚懷王](../Page/楚懷王.md "wikilink")、[楚襄王重用](../Page/楚襄王.md "wikilink")，國勢敗亡，自沉於江。
  - [項羽](../Page/項羽.md "wikilink")：[秦末](../Page/秦.md "wikilink")[起義軍](../Page/起義.md "wikilink")[領袖](../Page/領袖.md "wikilink")，號稱西楚霸王，與[劉邦相爭失利](../Page/劉邦.md "wikilink")，[烏江邊自刎](../Page/烏江.md "wikilink")。
  - [孫恩](../Page/孫恩.md "wikilink")：[東晉民變領袖](../Page/东晋.md "wikilink")，兵敗後投海自盡。被其信眾稱為水仙。
  - [王勃](../Page/王勃.md "wikilink")：[初唐四傑之一](../Page/初唐四傑.md "wikilink")，《[滕王閣序](../Page/滕王閣序.md "wikilink")》作者，年少渡江溺死。
  - [李白](../Page/李白.md "wikilink")：[唐朝大詩人](../Page/唐朝.md "wikilink")，傳說其醉於水裏撈月，溺死。

如臺灣奉祀水仙尊王之名廟，[嘉義縣](../Page/嘉義縣.md "wikilink")[新港水仙宮](../Page/新港水仙宮.md "wikilink")，奉祀大禹及上述之伍員、屈原、魯班、項羽，並以[后羿](../Page/羿_\(唐堯\).md "wikilink")、寒澆為侍神。\[2\]全臺祀典大天后宮的五水仙為「一帝二王二大夫」，為禹帝、奡王（寒澆）、楚王（項羽）、伍大夫（伍子胥）、三閭大夫（屈原）。\[3\]

## 划水仙

**划水仙**，是早期船員一種向水仙王祈禱的方式，船在遇到大風大浪，困於水中時，船員們要眾口一起喊叫模仿鑼鼓聲，每人手拿羹匙和筷子死命地-{划}-，假裝-{划}-[龍舟般](../Page/龍舟.md "wikilink")，就可以得到水仙王的救助，順利靠岸。

[郁永河](../Page/郁永河.md "wikilink")《採硫日記》中曾描述其景：「-{划}-水仙者，眾口齊作鉦鼓聲，人各挾一匕箸，虛作棹船勢，如午日競渡狀。凡洋中危急，不得近岸則為之。」

## 奉祀廟宇

### 台灣

#### 主祀

  - 基隆市仁愛區[清甯宮](../Page/清甯宮.md "wikilink")
  - 臺北市北投區[洲美屈原宮](../Page/洲美屈原宮.md "wikilink")
  - 臺北市士林區軟橋興安宮－供奉禹帝、伍員、屈原、王勃、李白等五位尊王\[4\]
  - 新北市板橋區[潮和宮](../Page/板橋保安宮潮和宮.md "wikilink")－原為水官大帝及水仙尊王廟，後增設神像成為三官大帝廟
  - 新北市板橋區江子翠水仙宮
  - 新北市蘆洲區清海宮－奉祀禹帝、金守將、玉守將（傳說此二位為禹帝治水助手）、盪舟尊者、善射尊者，後四者並有神將
  - 新竹市北區[水仙宮](../Page/新竹水仙宮.md "wikilink")
  - 新竹市香山區香山水仙宮
  - 宜蘭縣員山鄉大湖底大安宮
  - 臺中市中區水仙宮
  - 臺中市北屯區北屯水仙宮
  - 嘉義縣新港鄉[笨港水仙宮](../Page/笨港水仙宮.md "wikilink")
  - 嘉義縣中埔鄉鹽館福仙宮
  - 臺南市中西區[水仙宮](../Page/臺南水仙宮.md "wikilink")
  - 臺南市中西區代天水官大帝廟（原代天聖安宮）
  - 臺南市麻豆區麻豆口水仙府

<!-- end list -->

  - 臺南市永康區[鹽行禹帝宮](../Page/鹽行禹帝宮.md "wikilink")
  - 臺南市仁德區[田厝水明殿](../Page/田厝水明殿.md "wikilink")
  - 高雄市田寮區福水宮
  - 屏東縣新園鄉進海宮
  - 屏東縣南州鄉南清宮悟心壇
  - 屏東縣琉球鄉[水仙宮](../Page/小琉球水仙宮.md "wikilink")
  - 屏東縣琉球鄉水興宮
  - 屏東縣恆春鎮後壁湖鎮靈宮
  - 臺東縣成功鎮水仙宮
  - 臺東縣大武鄉水仙宮－位於彩虹街旁濱海公園內\[5\]
  - 澎湖縣馬公市[台廈郊水仙宮](../Page/澎湖水仙宮.md "wikilink")
  - 澎湖縣馬公市嵵裡水仙宮
  - 澎湖縣馬公市虎井水仙宮
  - 澎湖縣白沙鄉大倉水仙宮
  - 澎湖縣湖西鄉潭邊水仙宮
  - 金門縣金城鎮後浦南門境禹帝廟

#### 同祀

  - 臺北市萬華區[艋舺龍山寺](../Page/艋舺龍山寺.md "wikilink")－艋舺水仙宮拆毀後遷入
  - 臺北市萬華區[台北東隆宮](../Page/台北東隆宮.md "wikilink")
  - 臺北市松山區[慈祐宮](../Page/松山慈祐宮.md "wikilink")
  - 新北市淡水區[福佑宮](../Page/淡水福佑宮.md "wikilink")－供奉於正殿虎邊神龕
  - 新北市金山區[金包里慈護宮](../Page/金包里慈護宮.md "wikilink")
  - 新北市萬里區野柳仁和宮
  - 新北市瑞芳區深澳碧雲宮
  - 新北市瑞芳區鼻頭五聖宮－供奉於正殿龍邊神龕
  - 新北市貢寮區卯澳[利洋宮](../Page/利洋宮.md "wikilink")
  - 新北市貢寮區福隆聖經廟－供奉於正殿龍邊神龕
  - 宜蘭縣頭城鎮[草嶺慶雲宮](../Page/草嶺慶雲宮.md "wikilink")
  - 宜蘭縣頭城鎮龜山里拱蘭宮
  - 宜蘭縣宜蘭市[昭應宮](../Page/昭應宮.md "wikilink")－供奉於後殿二樓水仙殿，為水仙禹帝
  - 宜蘭縣冬山鄉八寶訓民堂－供奉於正殿主龕內
  - 苗栗縣後龍鎮慈雲宮－供奉於凌霄寶殿虎邊神龕
  - 彰化縣鹿港鎮[鹿港天后宮](../Page/鹿港天后宮.md "wikilink")
  - 彰化縣鹿港鎮埔頭南泉宮
  - 嘉義市東區[城隍廟](../Page/嘉義城隍廟.md "wikilink")－供奉於後殿三樓水仙宮殿
  - 嘉義市東區過溝仔震安宮－供奉於正殿龍邊神龕
  - 臺南市中西區[大天后宮](../Page/大天后宮.md "wikilink")－供奉於正殿龍邊神龕
  - 臺南市安平區[安平開臺天后宮](../Page/安平開臺天后宮.md "wikilink")－安平水仙宮拆毀後遷入\[6\]

<!-- end list -->

  - 臺南市安平區港仔尾社[靈濟殿](../Page/安平靈濟殿.md "wikilink")－奉祀水仙禹帝牌位，安平水仙宮拆毀後遷入\[7\]
  - 臺南市安平區囝仔宮社伍德宮－奉祀水仙禹帝及兩位太監神像，來自清代火房
  - 臺南市安南區[正統鹿耳門聖母廟](../Page/正統鹿耳門聖母廟.md "wikilink")
  - 臺南市安南區[鹿耳門天后宮](../Page/鹿耳門天后宮.md "wikilink")
  - 高雄市鹽埕區[三山國王廟](../Page/鹽埕三山國王廟.md "wikilink")－供奉於正殿中龕前神桌
  - 高雄市三民區[玉皇宮](../Page/高雄市玉皇宮.md "wikilink")－供奉於正殿下層中龕，尊稱為明司，為水仙禹帝
  - 高雄市鼓山區[哈瑪星代天宮](../Page/哈瑪星代天宮.md "wikilink")
  - 高雄市旗津區[天后宮](../Page/旗津天后宮.md "wikilink")
  - 高雄市旗津區七柱鳳山寺
  - 高雄市鳳山區武廟鎮安宮－供奉於正殿主龕內
  - 高雄市林園區[清水巖](../Page/林園清水巖.md "wikilink")－供奉於下層虎邊神龕
  - 屏東縣屏東市[海豐三山國王廟](../Page/海豐三山國王廟.md "wikilink")
  - 屏東縣林邊鄉崎峰真武殿
  - 屏東縣東港鎮[東隆宮](../Page/東港東隆宮.md "wikilink")
  - 屏東縣東港鎮安海街福安宮
  - 屏東縣東港鎮[東福殿城隍廟](../Page/東港東福殿城隍廟.md "wikilink")
  - 屏東縣東港鎮東南宮
  - 屏東縣枋寮鄉德興宮
  - 屏東縣琉球鄉[小琉球三隆宮](../Page/小琉球三隆宮.md "wikilink")
  - 金門縣金湖鎮[新頭伍德宮](../Page/金門伍德宮.md "wikilink")
  - 金門縣金沙鎮田浦東嶽泰山廟－奉祀水仙禹帝

## 參見

  - [水神](../Page/水神.md "wikilink")

## 參考資料

[\*](../Category/水仙尊王.md "wikilink")
[Category:中國民間信仰](../Category/中國民間信仰.md "wikilink")
[Category:王爺信仰](../Category/王爺信仰.md "wikilink")

1.  余琪蕙《由台南水仙宮看府城三郊的興衰》

2.

3.  [全臺祀典大天后宮-神尊介紹](http://tainanmazu.org.tw/grand_shuexian.html)

4.  根據廟內碑文記載而得

5.  [第0297篇\[台東大武](https://yingtingshih.pixnet.net/blog/post/171315081)台東大武彩虹街／彩虹村／大武濱海公園（非台九線大武濱海公園）／水仙宮／琉璃世界藥師壇城觀音像Ｘ影像導覽\]

6.

7.