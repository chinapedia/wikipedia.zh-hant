《**富貴逼人系列**》是指一系列由[董驃及](../Page/董驃.md "wikilink")[沈殿霞飾演](../Page/沈殿霞.md "wikilink")[基層家庭夫妻的](../Page/基層.md "wikilink")[香港電影](../Page/香港電影.md "wikilink")，故事內容大多是他們中了[彩票後如何被打回原型的故事](../Page/彩票.md "wikilink")。

## 特色

《富貴逼人系列》以輕鬆[幽默的故事情節](../Page/幽默.md "wikilink")，描寫出[香港八](../Page/香港1980年代.md "wikilink")[九十年代](../Page/香港1990年代.md "wikilink")[草根階層的生活](../Page/草根.md "wikilink")（如：逼[地鐵](../Page/香港地鐵.md "wikilink")、[賭馬](../Page/香港賽馬.md "wikilink")、賭[六合彩](../Page/六合彩.md "wikilink")、聽[日本流行音樂](../Page/日本.md "wikilink")、收看電視[健美操節目等](../Page/有氧健身操.md "wikilink")）及社會[時事](../Page/時事.md "wikilink")（如：[移民潮](../Page/香港移民潮.md "wikilink")、[銀行](../Page/銀行.md "wikilink")[擠提](../Page/擠提.md "wikilink")、[高空擲物](../Page/高空擲物.md "wikilink")、[大亞灣核電廠恐慌等](../Page/大亞灣核電廠.md "wikilink")），以及[香港普羅大眾對於](../Page/香港.md "wikilink")[香港主權移交中國等政治議題上的見解](../Page/香港主權移交.md "wikilink")。

## 評價

由於電影寫實有趣，惹起不少香港人的共鳴，深得香港人喜愛，故此叫好叫座，共拍攝了三輯。

《富貴逼人系列》當中涉及不少政治時事議題，觀點貼近香港普羅大眾，情節故事通俗，為不少影評人讚揚。

## 系列內容

  - 《[富貴逼人](../Page/富貴逼人.md "wikilink")》
  - 《[富貴再逼人](../Page/富貴再逼人.md "wikilink")》
  - 《[富貴再三逼人](../Page/富貴再三逼人.md "wikilink")》

## 衍生作品

以下[衍生作品起用了](../Page/衍生作品.md "wikilink")《富貴逼人系列》中大部份演員，但劇情與《富貴逼人系列》沒有關連。

  - 《[富貴黃金屋](../Page/富貴黃金屋.md "wikilink")》
  - 《[富貴開心鬼](../Page/富貴開心鬼.md "wikilink")》
  - 《[豪門夜宴](../Page/豪門夜宴_\(1991年電影\).md "wikilink")》（客串）

## 播放時間

[\*](../Category/富貴逼人系列.md "wikilink")
[Category:香港電影作品](../Category/香港電影作品.md "wikilink")