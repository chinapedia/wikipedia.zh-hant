**鬼针草**（[学名](../Page/学名.md "wikilink")：**），一年生[草本植物](../Page/草本植物.md "wikilink")。分布[亞熱帶到](../Page/亞熱帶.md "wikilink")[熱帶氣候地区](../Page/熱帶.md "wikilink")，原產於[美洲](../Page/美洲.md "wikilink")（[北美洲和](../Page/北美洲.md "wikilink")[南美洲](../Page/南美洲.md "wikilink")），地理分布[歐亞大陸](../Page/歐亞大陸.md "wikilink")、[非洲](../Page/非洲.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")、[太平洋島群等處](../Page/太平洋三大島群.md "wikilink")\[1\]。全草株可作为[中药使用](../Page/中药.md "wikilink")，[藥用植物](../Page/藥用植物.md "wikilink")。因其可改变土壤微生物群落结构，而成为一种[入侵物种](../Page/入侵物种.md "wikilink")\[2\]\[3\]。

## 異名

鬼针草一名始见于《[本草拾遺](../Page/本草拾遺.md "wikilink")》，異名有**鬼釵草**（《本草拾遗》）、**三叶鬼针**、**三叶鬼针草**、**鬼黄花**、**山东老鸦草**、**婆婆针**、**鬼骨针**、**叉婆子**、**鬼蒺藜**、**粘身草**、**钢叉针**等。

## 形态

一年生[草本植物](../Page/草本植物.md "wikilink")。高约40～85[厘米](../Page/厘米.md "wikilink")，茎直立，四棱形，无毛，上部分支可略带细毛。中下部叶对生，上部叶互生，[羽状脉](../Page/羽状脉.md "wikilink")，深裂，[披针形或卵状披针形](../Page/披针形.md "wikilink")。[头状花序](../Page/头状花序.md "wikilink")，有梗，[总苞杯状](../Page/总苞.md "wikilink")，中央[管状花黄色](../Page/管状花.md "wikilink")，边缘白色或淡黄[舌状花依不同变种有不同情况](../Page/舌状花.md "wikilink")：鬼针草有或没有舌状花，[咸丰草有较小的舌状花](../Page/咸丰草.md "wikilink")，而[大花咸丰草则有超过](../Page/大花咸丰草.md "wikilink")
5 *mm*
较大的舌状花\[4\]\[5\]。[瘦果长线形](../Page/瘦果.md "wikilink")，顶端冠毛芒状，3～4枚。花期8～9月，果期9～11月。

<File:Bidens> pilosa in Jiangxi.JPG
<File:Starr_030612-0113_Bidens_pilosa.jpg>
<File:Starr_060406-7570_Bidens_pilosa.jpg>
<File:Bidens_pilosa_(Flowers).jpg> <File:Bidens_pilosa_var_minor001.jpg>
<File:Bidenspilosa.jpg> <File:Bidens> pilosa 01.jpg <File:Bidens> pilosa
achene.jpg

## 药用

### 药理

鬼针草的[乙醇浸液在体外对](../Page/乙醇.md "wikilink")[革兰氏阳性菌有抑菌作用](../Page/革兰氏阳性菌.md "wikilink")，花、茎对[金黄色葡萄球菌也有抑菌作用](../Page/金黄色葡萄球菌.md "wikilink")。

### 性味

《本草拾遗》：味苦，平，无毒。

### 功用主治

清热解毒，散瘀消肿。主治[疟疾](../Page/疟疾.md "wikilink")、[痢疾](../Page/痢疾.md "wikilink")、跌打损伤、蛇虫咬伤。

## 参考文献

<div class="references-small">

<references />

  - [江苏新医学院](../Page/江苏新医学院.md "wikilink"),
    [中药大辞典](../Page/中药大辞典.md "wikilink"),
    [上海科学技术出版社](../Page/上海科学技术出版社.md "wikilink"),
    1986.5. ISBN 7-5323-0842-1

</div>

## 外部链接

  -
<!-- end list -->

  -
[Category:鬼针草属](../Category/鬼针草属.md "wikilink")
[Category:中药](../Category/中药.md "wikilink")
[Category:藥用植物](../Category/藥用植物.md "wikilink")
[Category:五瓣花](../Category/五瓣花.md "wikilink")
[Category:入侵物种](../Category/入侵物种.md "wikilink")

1.  [*Bidens
    pilosa*.](http://www.hear.org/pier/species/bidens_pilosa.htm)
    Pacific Island Ecosystems at Risk (PIER). USFS.
2.
3.
4.  <http://sowhc.sow.org.tw/html/note/ruician/ruician07/ruician07.htm>
5.