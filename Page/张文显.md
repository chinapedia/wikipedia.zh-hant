**张文显**（），[河南](../Page/河南.md "wikilink")[南阳人](../Page/南阳.md "wikilink")，法学家。[吉林大学法律系](../Page/吉林大学.md "wikilink")[法学理论专业硕士毕业](../Page/法学理论.md "wikilink")、[哲学系](../Page/哲学.md "wikilink")[马克思主义哲学专业博士毕业](../Page/马克思主义哲学.md "wikilink")，[研究生学历](../Page/研究生.md "wikilink")，教授。

1983年至1985年在美国[哥伦比亚大学法学院研修](../Page/哥伦比亚大学.md "wikilink")，1989年至1990年美国[华盛顿大学高级访问学者](../Page/华盛顿大学.md "wikilink")。1985年至1993先后任[吉林大学法律系副主任](../Page/吉林大学.md "wikilink")、法学院副院长、院长、副教授、教授、[博士生导师](../Page/博士生导师.md "wikilink")，1994年1月起任[吉林大学副校长](../Page/吉林大学.md "wikilink")、校党委常委、校学术委员会副主任、校学位委员会委员，2001年起担任[教育部人文社会科学重点研究基地](../Page/教育部.md "wikilink")·吉林大学理论法学研究中心主任、中心学术委员会副主任。2002年任吉林大学党委书记。

2007年12月任[吉林省高级人民法院副院长](../Page/吉林省高级人民法院.md "wikilink")、党组书记。2008年1月当选为[吉林省高级人民法院院长](../Page/吉林省高级人民法院.md "wikilink")。2008年起担任全国人大代表\[1\]。

2008年3月，不再担任[吉林大学党委书记](../Page/吉林大学.md "wikilink")。2013年1月，卸任[吉林省高级人民法院院长](../Page/吉林省高级人民法院.md "wikilink")。

他主编的《[法理学](../Page/法理学.md "wikilink")》是许多高校法学专业的学习[教材](../Page/教材.md "wikilink")。

## 參考文獻

{{-}}

[WEN](../Category/张姓.md "wikilink")
[Category:吉林大学校友](../Category/吉林大学校友.md "wikilink")
[Category:哥倫比亞大學校友](../Category/哥倫比亞大學校友.md "wikilink")
[Category:吉林大学教授](../Category/吉林大学教授.md "wikilink")
[Category:浙江大学兼职教授](../Category/浙江大学兼职教授.md "wikilink")
[Category:南阳人](../Category/南阳人.md "wikilink")
[Category:法理学家](../Category/法理学家.md "wikilink")
[Category:中国法学家](../Category/中国法学家.md "wikilink")
[Category:第十一届全国人大代表](../Category/第十一届全国人大代表.md "wikilink")
[Category:吉林大学党委书记](../Category/吉林大学党委书记.md "wikilink")
[Category:吉林大学副校长](../Category/吉林大学副校长.md "wikilink")
[Category:吉林省高級人民法院院長](../Category/吉林省高級人民法院院長.md "wikilink")
[Category:中国法学会副会长](../Category/中国法学会副会长.md "wikilink")
[Category:中共十七大代表](../Category/中共十七大代表.md "wikilink")

1.