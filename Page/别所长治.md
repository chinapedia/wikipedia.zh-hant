**別所長治**（1558年（有異說）－1580年2月2日）是[日本戰國時代至](../Page/日本戰國時代.md "wikilink")[安土桃山時代的武將和](../Page/安土桃山時代.md "wikilink")[大名](../Page/大名.md "wikilink")。父親是[別所安治](../Page/別所安治.md "wikilink")。通稱**小三郎**。

## 生平

在[永祿元年](../Page/永祿.md "wikilink")（1558年）出生（有異説），家中嫡男。[元龜元年](../Page/元龜.md "wikilink")（1570年），父親安治病死，以叔父[吉親和](../Page/別所吉親.md "wikilink")[重宗為](../Page/別所重宗.md "wikilink")[後見役](../Page/後見役.md "wikilink")，在年輕時就繼任[家督](../Page/家督.md "wikilink")。

在很早期就順從[織田信長](../Page/織田信長.md "wikilink")，繼任家督的長治亦在[天正](../Page/天正_\(日本\).md "wikilink")3年（1575年）10月謁見信長（以年齢來推測，應該在這段時期[元服](../Page/元服.md "wikilink")，名字中的「**長**」字亦是信長的[偏諱](../Page/偏諱.md "wikilink")），翌年都在新年時探訪信長。信長在壓制[中國地方的](../Page/中國地方.md "wikilink")[毛利氏時](../Page/毛利氏.md "wikilink")，長治與其呼應並擔任先鋒，但是對成為中國方面總司令官的[羽柴秀吉不滿](../Page/羽柴秀吉.md "wikilink")，於是響應[丹波國的](../Page/丹波國.md "wikilink")[波多野秀治](../Page/波多野秀治.md "wikilink")（妻子的娘家）而反抗信長（雖然長治本人亦抱持不滿，但是更重要的是受到叔父吉親影響）。與多數周邊勢力合作，進攻不順從的勢力，於是東[播磨國一帶成為反織田勢力](../Page/播磨國.md "wikilink")。

因此長治被受到信長命令的秀吉進攻。長治在籠城並徹底抗戰，令秀吉感到相當麻煩，後來更有[荒木村重的謀反和](../Page/荒木村重.md "wikilink")[毛利氏的援軍等良好條件](../Page/毛利氏.md "wikilink")，一度撃退織田軍，不久後遭到秀吉使用切斷兵糧運輸線（）的戰法所影響（[三木合戰](../Page/三木合戰.md "wikilink")），和等支城陷落，亦失去毛利氏的援軍，於是在籠城2年後的天正8年（1580年），以放過城內士兵的性命為條件，與妻子和兄弟一同自殺。[介錯是家臣](../Page/介錯.md "wikilink")[三宅治忠](../Page/三宅治忠.md "wikilink")。享年23歲（『[信長公記](../Page/信長公記.md "wikilink")』中記載是26歲）。

辭世句是「」。[法名](../Page/法名.md "wikilink")「英應院殿剛覺性金大居士」。

## 死後

部份系圖記載，[別所重宗的嫡子](../Page/別所重宗.md "wikilink")[吉治](../Page/別所吉治.md "wikilink")（後來的藩主）是長治的兒子，在城池陷落之際逃出。

根據『北攝三田之歷史』（著）等介紹的史料『上津畑之庄茶臼山記』中記載，家臣[後藤基國](../Page/後藤基國.md "wikilink")（[後藤基次](../Page/後藤基次.md "wikilink")（又兵衛）的父親）與長治的兒子千代丸（當時8歲）及[乳母](../Page/乳母.md "wikilink")、家臣一同逃到，千代丸在該城被攻陷後務農。

城跡的[上之丸公園中有辭世的歌碑](../Page/上之丸公園.md "wikilink")，以及近年當地的[國際獅子會寄贈的長治騎馬武者石像](../Page/國際獅子會.md "wikilink")。

## 墓所、靈廟

[250px](../Page/file:Mikij19.jpg.md "wikilink")）\]\]

  - 雲龍寺（[首塚](../Page/首塚.md "wikilink")）：[三木市](../Page/三木市.md "wikilink")[上之丸町](../Page/上之丸町.md "wikilink")

在毎年的1月17日（長治的[死忌](../Page/死忌.md "wikilink")）都會有法事，因為有飢餓的城兵進食[秸稈的故事](../Page/秸稈.md "wikilink")，所以會供奉與秸稈相似的[烏冬](../Page/烏冬.md "wikilink")。

  - 法界寺（靈廟、別所家[菩提寺](../Page/菩提寺.md "wikilink")）：三木市[別所町東這田](../Page/別所町.md "wikilink")

在4月17日有追悼法事，會舉行講解當時合戰情況的「三木合戰繪解」。

此外毎年5月5日會有記念長治的「別所公春祭」，在辭世句的歌碑前舉行歌碑祭，並有「」等的活動。

## 登場作品

  - 小説

<!-- end list -->

  - 『』（作者：）
  - 『』（收錄在短編集「」「」。作者：[司馬遼太郎](../Page/司馬遼太郎.md "wikilink")）
  - 『』（收錄在短編集「」。作者：）
  - 『』（作者：）
  - 『』（收錄在短編集「」。作者：）
  - 『』（收錄在短編集「」。作者：）

## 相關條目

  -
[Category:別所氏](../Category/別所氏.md "wikilink")
[Category:戰國大名](../Category/戰國大名.md "wikilink")
[Category:1558年出生](../Category/1558年出生.md "wikilink")
[Category:1580年逝世](../Category/1580年逝世.md "wikilink")
[Category:播磨國出身人物](../Category/播磨國出身人物.md "wikilink")
[Category:織豐政權大名](../Category/織豐政權大名.md "wikilink")