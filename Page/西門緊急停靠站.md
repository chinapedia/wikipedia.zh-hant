**西門緊急停靠站**為[交通部臺灣鐵路管理局](../Page/台灣鐵路管理局.md "wikilink")[縱貫線上](../Page/縱貫線_\(北段\).md "wikilink")、於[臺北鐵路地下化專案中](../Page/臺北鐵路地下化專案.md "wikilink")[臺北市市區路段所設置的緊急停靠站](../Page/臺北市.md "wikilink")，1987年9月2日設站。

此處於[日治時代設立](../Page/台灣日治時期.md "wikilink")**新起町車站**（），停靠汽油客車，直到[太平洋戰爭時期缺乏汽油為止](../Page/太平洋戰爭.md "wikilink")。戰後的1950年代，臺鐵計畫將其復站，命名為**西門車站**；但因臺鐵無力處理車站附近及月臺上的[違建](../Page/違建.md "wikilink")，從未啟用。

[臺北鐵路地下化專案後設立緊急月臺](../Page/臺北鐵路地下化專案.md "wikilink")，計畫辦理客運但仍未實現，平時無任何列車停靠。本站有3個緊急出口，分別為2號出口（長沙街緊急出口）、4A出口（武昌街緊急出口）、4B出口（洛陽街緊急出口），均位於臺北市[中華路一段](../Page/中華路_\(台北市\).md "wikilink")。4A出口、4B出口與[臺北捷運](../Page/台北捷運.md "wikilink")[西門站相通](../Page/西門站.md "wikilink")，2007年1月[台灣高鐵通車後為臺鐵與高鐵共用之緊急出口](../Page/台灣高鐵.md "wikilink")。

<File:Exit> 2, TRA Ximen Emergency Halt Station
20180929.jpg|西門緊急停靠站2號出口（長沙街緊急出口） <File:Exit> 4A,
TRA Ximen Emergency Halt Station 20180818.jpg|西門緊急停靠站4A出口（武昌街緊急出口）
<File:Exit> 4A door, TRA Ximen Emergency Halt Station
20180818.jpg|西門緊急停靠站4A出口鐵門 <File:Exit> 4B, TRA Ximen
Emergency Halt Station 20180818.jpg|西門緊急停靠站4B出口（洛陽街緊急出口）

[他](../Category/縱貫線車站_\(北段\).md "wikilink")