**安德雷·巴尔扎利**（**Andrea
Barzagli**，），現役[意大利職業足球員](../Page/意大利.md "wikilink")，2008年夏季由[意甲球會](../Page/意大利足球甲级联赛.md "wikilink")[巴勒莫轉投](../Page/巴勒莫足球會.md "wikilink")[德甲](../Page/德甲.md "wikilink")[沃尔夫斯堡](../Page/沃尔夫斯堡足球俱乐部.md "wikilink")\[1\]，2011年冬季转会中回到意大利，加盟[尤文图斯](../Page/尤文图斯足球俱乐部.md "wikilink")，司職後卫。

## 生平

巴沙利於Rondinella Calcio球队首次亮相，随后曾先后加入A.C.
Pistoiese、[阿斯科利及](../Page/阿斯科利足球俱樂部.md "wikilink")[切沃](../Page/切沃足球俱乐部.md "wikilink")，至2004年夏始签约加入巴勒莫球队，同年更入選意大利21歲以下國家隊代表參加[歐洲U-21足球錦標賽](../Page/歐洲U-21足球錦標賽.md "wikilink")，最後意大利在決賽以三比零塞爾維亞及黑山第五度奪標。繼後他再度代表意大利出戰[雅典奧運會](../Page/2004年夏季奧林匹克運動會.md "wikilink")，可是準決賽以零比三不敵大熱冠軍阿根廷，最終只能在季軍戰擊敗伊拉克取得一面奧運銅牌。

目前巴沙利为[意大利國家隊上陣九次](../Page/意大利國家足球隊.md "wikilink")，在[2006年世界盃八强赛](../Page/2006年世界杯足球赛.md "wikilink")，意大利对[乌克兰的赛事中](../Page/烏克蘭國家足球隊.md "wikilink")，他以新手的身份参赛，以代替受伤的球员[內斯塔及被罚停赛的球员](../Page/亚历山德罗·内斯塔.md "wikilink")[馬特拉斯](../Page/马尔科·马特拉齐.md "wikilink")。其後亦有份出戰2008年歐洲國家盃外圍賽，成功協助國家隊進軍決賽週。

他被看作[尼斯達](../Page/亚历山德罗·内斯塔.md "wikilink")、[簡拿華路之接班人](../Page/法比奧·卡纳瓦罗.md "wikilink")。世界盃後曾傳出[AC米蘭已秘密跟巴沙利簽約](../Page/AC米蘭.md "wikilink")\[2\]，然而在2008年，他卻意外地走到[德甲](../Page/德甲.md "wikilink")，加盟[德甲球會](../Page/德甲.md "wikilink")[禾夫斯堡](../Page/沃尔夫斯堡足球俱乐部.md "wikilink")。2009年，他協助禾夫斯堡首奪德甲聯賽冠軍，也是個人首個在[四大聯賽的聯賽冠軍](../Page/四大聯賽.md "wikilink")。

2011年1月30日，巴尔扎利回到意大利，加盟[尤文图斯](../Page/尤文图斯足球俱乐部.md "wikilink")，转会费为30万欧元。
於2011年至2012年球季，巴沙利協助祖雲達斯以全季不敗戰績，奪取意大利足球甲級聯賽冠軍。

## 榮譽

  - 意大利U-21國家隊

<!-- end list -->

  - [歐洲U-21足球錦標賽冠軍](../Page/歐洲U-21足球錦標賽.md "wikilink")：2004年

<!-- end list -->

  - 意大利奧運隊

<!-- end list -->

  - [雅典奧運會銅牌](../Page/2004年夏季奧林匹克運動會.md "wikilink")：[2004年](../Page/2004年夏季奧林匹克運動會足球比賽.md "wikilink")

<!-- end list -->

  - 意大利國家隊

<!-- end list -->

  - [世界盃冠軍](../Page/世界杯足球赛.md "wikilink")：[2006年](../Page/2006年世界杯足球赛.md "wikilink")

<!-- end list -->

  - 禾夫斯堡

<!-- end list -->

  - [德甲聯賽冠軍](../Page/德国足球甲级联赛.md "wikilink")：[2008/09賽季](../Page/2008年至2009年德國足球甲級聯賽.md "wikilink")

<!-- end list -->

  - 祖雲達斯

<!-- end list -->

  - [意甲聯賽冠軍](../Page/意大利足球甲級聯賽.md "wikilink"):
    [2011/12賽季](../Page/2011年至2012年意大利足球甲級聯賽.md "wikilink")、[2012/13賽季](../Page/2012年至2013年意大利足球甲級聯賽.md "wikilink")

## 參考

<references/>

## 外部連結

  - [有關巴沙利檔案(出自巴勒莫官方網站)](https://web.archive.org/web/20060711185528/http://www.ilpalermocalcio.it/it/prima/scheda.jsp?id=1643)

[Category:意大利足球运动员](../Category/意大利足球运动员.md "wikilink")
[Category:足球後衛](../Category/足球後衛.md "wikilink")
[Category:基爾禾球員](../Category/基爾禾球員.md "wikilink")
[Category:巴勒莫球員](../Category/巴勒莫球員.md "wikilink")
[Category:禾夫斯堡球員](../Category/禾夫斯堡球員.md "wikilink")
[Category:祖雲達斯球員](../Category/祖雲達斯球員.md "wikilink")
[Category:德甲球員](../Category/德甲球員.md "wikilink")
[Category:世界盃足球賽冠軍隊球員](../Category/世界盃足球賽冠軍隊球員.md "wikilink")
[Category:意大利奧林匹克運動會銅牌得主](../Category/意大利奧林匹克運動會銅牌得主.md "wikilink")
[Category:2004年夏季奧林匹克運動會獎牌得主](../Category/2004年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:德國外籍足球運動員](../Category/德國外籍足球運動員.md "wikilink")
[Category:2004年夏季奧林匹克運動會足球運動員](../Category/2004年夏季奧林匹克運動會足球運動員.md "wikilink")
[Category:2006年世界盃足球賽球員](../Category/2006年世界盃足球賽球員.md "wikilink")
[Category:2008年歐洲國家盃球員](../Category/2008年歐洲國家盃球員.md "wikilink")
[Category:2012年歐洲國家盃球員](../Category/2012年歐洲國家盃球員.md "wikilink")
[Category:2013年洲際國家盃球員](../Category/2013年洲際國家盃球員.md "wikilink")
[Category:2014年世界盃足球賽球員](../Category/2014年世界盃足球賽球員.md "wikilink")

1.  [狼堡敲定巴尔扎利 马加特为强援而自豪](http://www.dfo.cn/dfo/show.asp?id=7949)
2.  [意国脚铁卫加盟AC米兰已密谋1年](http://www.hangzhou.com.cn/20080310/ca1473324.htm)