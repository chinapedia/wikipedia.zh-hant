**飛龍**（）是[日本將棋的棋子之一](../Page/日本將棋.md "wikilink")。有在[平安大將棋](../Page/平安大將棋.md "wikilink")、[大大將棋](../Page/大大將棋.md "wikilink")、[大將棋](../Page/大將棋.md "wikilink")、[摩訶大大將棋](../Page/摩訶大大將棋.md "wikilink")、[泰將棋和](../Page/泰將棋.md "wikilink")[大局將棋出現](../Page/大局將棋.md "wikilink")。

## 飛龍在平安大將棋

可升級成[金將](../Page/金將.md "wikilink")（）。

<table>
<thead>
<tr class="header">
<th><p>前身棋</p></th>
<th><p>步法</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>飛龍</p></td>
<td><table>
<tbody>
<tr class="odd">
<td><p>＼</p></td>
<td><p><font color="white">■</font></p></td>
<td><p><font color="white">■</font></p></td>
<td><p><font color="white">■</font></p></td>
<td><p>／</p></td>
</tr>
<tr class="even">
<td><p>　</p></td>
<td><p>＼</p></td>
<td><p><font color="white">■</font></p></td>
<td><p>／</p></td>
<td><p>　</p></td>
</tr>
<tr class="odd">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p><strong>隋</strong></p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
<tr class="even">
<td><p>　</p></td>
<td><p>／</p></td>
<td><p><font color="white">■</font></p></td>
<td><p>＼</p></td>
<td><p>　</p></td>
</tr>
<tr class="odd">
<td><p>／</p></td>
<td><p><font color="white">■</font></p></td>
<td><p><font color="white">■</font></p></td>
<td><p><font color="white">■</font></p></td>
<td><p>＼</p></td>
</tr>
</tbody>
</table></td>
</tr>
</tbody>
</table>

## 飛龍在大將棋、摩訶大大將棋、泰將棋

可升級成[金將](../Page/金將.md "wikilink")（）。

<table>
<thead>
<tr class="header">
<th><p>前身棋</p></th>
<th><p>步法</p></th>
<th><p>升級棋</p></th>
<th><p>步法</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>飛龍</p></td>
<td><table>
<tbody>
<tr class="odd">
<td><p>○</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>○</p></td>
</tr>
<tr class="even">
<td><p>　</p></td>
<td><p>○</p></td>
<td><p>　</p></td>
<td><p>○</p></td>
<td><p>　</p></td>
</tr>
<tr class="odd">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p><strong>隋</strong></p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
<tr class="even">
<td><p>　</p></td>
<td><p>○</p></td>
<td><p>　</p></td>
<td><p>○</p></td>
<td><p>　</p></td>
</tr>
<tr class="odd">
<td><p>○</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>○</p></td>
</tr>
</tbody>
</table></td>
<td><p>可循斜向走兩格。不得越棋。</p></td>
<td><p>金將</p></td>
</tr>
</tbody>
</table>

## 飛龍在大大將棋

可升級成[龍王](../Page/龍王_\(日本將棋棋子\).md "wikilink")（）。

<table>
<thead>
<tr class="header">
<th><p>前身棋</p></th>
<th><p>步法</p></th>
<th><p>升級棋</p></th>
<th><p>步法</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>飛龍</p></td>
<td><table>
<tbody>
<tr class="odd">
<td><p>○</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>○</p></td>
</tr>
<tr class="even">
<td><p>　</p></td>
<td><p>○</p></td>
<td><p>　</p></td>
<td><p>○</p></td>
<td><p>　</p></td>
</tr>
<tr class="odd">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p><strong>隋</strong></p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
<tr class="even">
<td><p>　</p></td>
<td><p>○</p></td>
<td><p>　</p></td>
<td><p>○</p></td>
<td><p>　</p></td>
</tr>
<tr class="odd">
<td><p>○</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>○</p></td>
</tr>
</tbody>
</table></td>
<td><p>可循斜向走兩格。不得越棋。</p></td>
<td><p>龍王</p></td>
</tr>
</tbody>
</table>

## 飛龍在大局將棋

可升級成[龍王](../Page/龍王_\(日本將棋棋子\).md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>前身棋</p></th>
<th><p>步法</p></th>
<th><p>升級棋</p></th>
<th><p>步法</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>飛龍</p></td>
<td><table>
<tbody>
<tr class="odd">
<td><p>☆</p></td>
<td><p><font color="white">■</font></p></td>
<td><p>　</p></td>
<td><p><font color="white">■</font></p></td>
<td><p>☆</p></td>
</tr>
<tr class="even">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
<tr class="odd">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p><strong>隋</strong></p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
<tr class="even">
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
</tr>
<tr class="odd">
<td><p>☆</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>　</p></td>
<td><p>☆</p></td>
</tr>
</tbody>
</table></td>
<td><p>可循斜向越入第二格。</p></td>
<td><p>龍王</p></td>
</tr>
</tbody>
</table>

## 參見

  - [日本將棋](../Page/日本將棋.md "wikilink")
  - [日本將棋變體](../Page/日本將棋變體.md "wikilink")
  - [平安大將棋](../Page/平安大將棋.md "wikilink")
  - [大將棋](../Page/大將棋.md "wikilink")
  - [大大將棋](../Page/大大將棋.md "wikilink")
  - [摩訶大大將棋](../Page/摩訶大大將棋.md "wikilink")
  - [泰將棋](../Page/泰將棋.md "wikilink")
  - [大局將棋](../Page/大局將棋.md "wikilink")

[Category:日本將棋棋子](../Category/日本將棋棋子.md "wikilink")
[Category:平安大將棋棋子](../Category/平安大將棋棋子.md "wikilink")
[Category:大將棋棋子](../Category/大將棋棋子.md "wikilink")
[Category:大大將棋棋子](../Category/大大將棋棋子.md "wikilink")
[Category:摩訶大大將棋棋子](../Category/摩訶大大將棋棋子.md "wikilink")
[Category:泰將棋棋子](../Category/泰將棋棋子.md "wikilink")
[Category:大局將棋棋子](../Category/大局將棋棋子.md "wikilink")