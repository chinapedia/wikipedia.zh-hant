**西域构造域**是早[古生代](../Page/古生代.md "wikilink")（[寒武纪](../Page/寒武纪.md "wikilink")、[奥陶纪](../Page/奥陶纪.md "wikilink")、[志留纪的合称](../Page/志留纪.md "wikilink")）[中国五个](../Page/中国.md "wikilink")[构造域之一](../Page/构造域.md "wikilink")，因为它们在志留纪末期碰撞拼合成一个完整的[西域板块而得名](../Page/西域板块.md "wikilink")。

西域构造域是一个过渡性的构造域，它包括[塔里木板块](../Page/塔里木板块.md "wikilink")、[阿拉善地块](../Page/阿拉善地块.md "wikilink")、[中祁连地块](../Page/中祁连地块.md "wikilink")、[柴达木地块](../Page/柴达木地块.md "wikilink")、[化隆地块](../Page/化隆地块.md "wikilink")、[西昆仑中部地块等](../Page/西昆仑中部地块.md "wikilink")[板块或](../Page/板块.md "wikilink")[地块](../Page/地块.md "wikilink")，其中面积最大、最重要的是[塔里木板块](../Page/塔里木板块.md "wikilink")、[阿拉善地块和](../Page/阿拉善地块.md "wikilink")[柴达木地块](../Page/柴达木地块.md "wikilink")。

在[中元古代初期](../Page/中元古代.md "wikilink")，西域构造域各板块和地块与[中朝板块拼合在一起](../Page/中朝板块.md "wikilink")，在[构造运动](../Page/构造运动.md "wikilink")、[古生物和](../Page/古生物.md "wikilink")[古地磁上表现出亲中朝的特征](../Page/古地磁.md "wikilink")，因而与中朝板块共同组成**[亲中朝构造域](../Page/中朝构造域.md "wikilink")**，是中元古代中国四个构造域之一，。但在中元古代，塔里木板块、柴达木地块和古中朝板块（包括阿拉善地块）之间发生裂陷，而彼此分离成三部分，但间距不远。三者在[青白口纪一度再次拼合成一体](../Page/青白口纪.md "wikilink")，但在[南华纪](../Page/南华纪.md "wikilink")-[震旦纪再次裂陷](../Page/震旦纪.md "wikilink")，阿拉善地块也在这时裂陷出来，从此西域构造域各板块或地块开始远离中朝板块，靠近[扬子板块](../Page/扬子板块.md "wikilink")，而表现出亲扬子的特征。奥陶纪至志留纪早期，西域构造域各板块或地块彼此更加远离，但在志留纪晚期它们又相互会聚、碰撞，形成[祁连-阿尔金碰撞带](../Page/祁连-阿尔金碰撞带.md "wikilink")，从而拼合成西域板块。这些构造活动，在中国称为[祁连运动](../Page/祁连构造期.md "wikilink")，或者借用欧美地区的术语，称为[加里东运动](../Page/加里东构造期.md "wikilink")。

西域板块独立存在的时间并不长，仅在[泥盆纪时保持了一定的孤立状态](../Page/泥盆纪.md "wikilink")。到[早石炭世至](../Page/石炭纪.md "wikilink")[早二叠纪末期](../Page/二叠纪.md "wikilink")，它即和亲西伯利亚构造域各地块及[中朝板块一起与](../Page/中朝板块.md "wikilink")[劳亚板块发生碰撞](../Page/劳亚大陆.md "wikilink")，形成[天山-兴安碰撞带](../Page/天山-兴安碰撞带.md "wikilink")，而合并到劳亚板块之上。这个构造活动，在中国称为[天山运动](../Page/天山构造期.md "wikilink")，或者借用欧美地区的术语，称为[海西运动或华力西运动](../Page/海西构造期.md "wikilink")。

## 参考文献

  -
## 参见

  - [亲西伯利亚构造域](../Page/亲西伯利亚构造域.md "wikilink")
  - [中朝构造域](../Page/中朝构造域.md "wikilink")
  - [亲扬子构造域](../Page/亲扬子构造域.md "wikilink")
  - [亲冈瓦纳构造域](../Page/亲冈瓦纳构造域.md "wikilink")

[X](../Page/category:中国地质.md "wikilink")