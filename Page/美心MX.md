[Maxim'sMX_logo.svg](https://zh.wikipedia.org/wiki/File:Maxim'sMX_logo.svg "fig:Maxim'sMX_logo.svg")
[Maxim's_fast_food_in_New_Town_Plaza_2005.jpg](https://zh.wikipedia.org/wiki/File:Maxim's_fast_food_in_New_Town_Plaza_2005.jpg "fig:Maxim's_fast_food_in_New_Town_Plaza_2005.jpg")
[HK_Maxims_MX_60421.jpg](https://zh.wikipedia.org/wiki/File:HK_Maxims_MX_60421.jpg "fig:HK_Maxims_MX_60421.jpg")美心MX外觀\]\]
[Maxim's_MX2_interior_in_Luk_Yeung_Galleria_2014.jpg](https://zh.wikipedia.org/wiki/File:Maxim's_MX2_interior_in_Luk_Yeung_Galleria_2014.jpg "fig:Maxim's_MX2_interior_in_Luk_Yeung_Galleria_2014.jpg")[綠楊坊MX內部](../Page/綠楊坊.md "wikilink")\]\]
[MX_in_Paradise_Mall.jpg](https://zh.wikipedia.org/wiki/File:MX_in_Paradise_Mall.jpg "fig:MX_in_Paradise_Mall.jpg")MX\]\]
[Telford_Plaza_Maxim_Food2_Interior_2015.jpg](https://zh.wikipedia.org/wiki/File:Telford_Plaza_Maxim_Food2_Interior_2015.jpg "fig:Telford_Plaza_Maxim_Food2_Interior_2015.jpg")[德福廣場美心food](../Page/德福廣場.md "wikilink")²\]\]
**美心MX**（），是一間[香港](../Page/香港.md "wikilink")[連鎖](../Page/連鎖店.md "wikilink")[港式快餐店](../Page/港式快餐.md "wikilink")，為[美心食品有限公司主線快餐品牌](../Page/美心食品有限公司.md "wikilink")，在品牌改變前一直使用名稱「**美心快餐**」經營本地連鎖快餐業務。副線品牌包括**千燒百味**、**美心food²**、[m.a.x.
concepts管理的](../Page/m.a.x._concepts.md "wikilink")[can．teen](../Page/can．teen.md "wikilink")、[Deli-O等](../Page/Deli-O.md "wikilink")。其主要同類競爭對手有[大快活和](../Page/大快活.md "wikilink")[大家樂](../Page/大家樂.md "wikilink")。

美心的快餐部及機構餐務部現時為同一管理，機構餐務部主要為工商機構、大學、醫院、主題公園提供膳食服務。

## 歷史

1972年，首家**美心快餐**成立。

1990年代，曾一度出現數個副線品牌，包括「美心美食」（Maxim's
Delight）、「知味屋」、「燒味至尊」、「上海風味」、主攻[尖沙咀及](../Page/尖沙咀.md "wikilink")[中環地區的](../Page/中環.md "wikilink")「Maxim's
Cafe
Express」及[金鐘](../Page/金鐘.md "wikilink")[海富中心](../Page/海富中心.md "wikilink")「YoYo
Express」。

2000年代初，在[金鐘](../Page/金鐘.md "wikilink")[太古廣場開設](../Page/太古廣場.md "wikilink")「御膳」，[旺角](../Page/旺角.md "wikilink")[新世紀廣場及](../Page/新世紀廣場.md "wikilink")[九龍塘](../Page/九龍塘.md "wikilink")[又一城開設](../Page/又一城.md "wikilink")「**千燒百味**」，全為美食廣場攤位，主要提供燒味為主。「御膳」於2000年代初結業，而該兩間「千燒百味」於2008年中結業，只剩2007年4月於[翔天廊開設的](../Page/翔天廊.md "wikilink")「千燒百味」。

2000年，機構餐務部於[東涌](../Page/東涌.md "wikilink")[東薈城開設](../Page/東薈城.md "wikilink")「繽紛美食坊」（Funtasia），承包全部十個食品攤位及提供[自助餐](../Page/自助餐.md "wikilink")，於2006年結業。

2001年，機構餐務部曾於[龍翔中心開設東南亞菜快餐](../Page/龍翔中心.md "wikilink")「泰知味」，但於一年後結業。

2004年，美心請來[李永銓將](../Page/李永銓.md "wikilink")「美心快餐」改變形象，商標以「加多一點點」為題，用簡單心型線條及一點為標誌，主調為綠色。同年，推出即食微波爐飯盒及小菜，分別於[7-11便利店及](../Page/7-11便利店.md "wikilink")[惠康超級市場發售](../Page/惠康超級市場.md "wikilink")。

2005年起，「美心快餐」、「Cafe
Express」及「美心美食」開始陸續全部革新為「**美心MX**」，由[陳幼堅設計新標誌及](../Page/陳幼堅.md "wikilink")[梁志天執手室內設計](../Page/梁志天.md "wikilink")，請來多位名家為店內添上畫像，並獲多個室內設計雜誌社及組織多個獎項，包括：美國紐約室內設計雜誌《Hospitality
Design》快餐組金獎。首間位於[鰂魚涌的分店試行設有](../Page/鰂魚涌.md "wikilink")「Grab n
Go」攤位，提供三文治、焗飯、薄餅、小食等，及設「**千燒百味**」攤位，於2007年結束。但微波爐即食食品則繼續沿用「美心快餐」品牌。

2007年10月，[九龍灣](../Page/九龍灣.md "wikilink")[德福廣場](../Page/德福廣場.md "wikilink")**美心food²**開業，位置前身為「[大滿貫](../Page/大滿貫快餐店.md "wikilink")」，提供與美心MX近乎相同的食品，但環境較清雅，及即製食品由服務員代客捧餐，顧客只需持號碼牌在座位等候。

2009年4月，[赤鱲角](../Page/赤鱲角.md "wikilink")[香港國際機場一號客運大樓](../Page/香港國際機場.md "wikilink")**美心food²**開業，並推出「美心飛機燒鵝」券。

2010年5月，在[鴨脷洲](../Page/鴨脷洲.md "wikilink")[海怡半島開設](../Page/海怡半島.md "wikilink")[美心香港地茶餐廳](../Page/美心香港地.md "wikilink")，同年10月及12月再分別於[小西灣](../Page/小西灣.md "wikilink")[小西灣廣場及](../Page/小西灣廣場.md "wikilink")[上水](../Page/上水.md "wikilink")[上水廣場增加分店](../Page/上水廣場.md "wikilink")，及後再開設更多分店。

2015年5月，[紅磡](../Page/紅磡.md "wikilink")[黃埔花園美心MX全面翻新](../Page/黃埔花園.md "wikilink")，改為「**MX**」品牌，使用黑底黃字標誌，並更改原本食譜，引入自助點餐機，但裝潢及定位較一般美心MX高級，同年再分別為[鰂魚涌](../Page/鰂魚涌.md "wikilink")[康怡商場](../Page/康怡商場.md "wikilink")、[中環](../Page/中環.md "wikilink")[干諾道中](../Page/干諾道中.md "wikilink")、[葵涌](../Page/葵涌.md "wikilink")[新葵興商場](../Page/新葵興商場.md "wikilink")、[天水圍](../Page/天水圍.md "wikilink")[置富嘉湖及](../Page/置富嘉湖.md "wikilink")[青衣](../Page/青衣.md "wikilink")[長發商場等分店升格](../Page/長發商場.md "wikilink")。

2018年起，在一些新分店安裝自助點餐[觸控屏](../Page/觸控屏.md "wikilink")。

## 參見

  - [美心食品](../Page/美心食品.md "wikilink")
  - [美心香港地](../Page/美心香港地.md "wikilink")

## 外部連結

  - [美心MX官網](http://www.maximsmx.com.hk/)

[Category:香港连锁餐厅](../Category/香港连锁餐厅.md "wikilink")
[Category:香港快餐店](../Category/香港快餐店.md "wikilink")
[Category:美心食品](../Category/美心食品.md "wikilink")