[Monorail_Moskau_-_Einfahrt_in_Station_Telezentrum.jpg](https://zh.wikipedia.org/wiki/File:Monorail_Moskau_-_Einfahrt_in_Station_Telezentrum.jpg "fig:Monorail_Moskau_-_Einfahrt_in_Station_Telezentrum.jpg")[莫斯科單軌鐵路是一個](../Page/莫斯科單軌鐵路.md "wikilink")2004年開始進行試營運、2008年才正式開始啟用的單軌系統。\]\]
**單軌鐵路**簡稱**單軌**，是[鐵路的一種](../Page/鐵路.md "wikilink")，特點是使用的[軌道只有一條](../Page/鐵路軌道.md "wikilink")，而非傳統鐵路的兩條平衡路軌。單軌鐵路的路軌一般以超高硬度混凝土製造，比普通鋼軌寬很多。而單軌鐵路的車輛比路軌更寬。和[城市軌道交通系統相似](../Page/城市軌道交通系統.md "wikilink")，單軌鐵路主要應用在城市人口密集的地方，用來運載乘客，相比普通铁路有较强的爬坡能力，转弯半径小，适合起伏较多的地形。此外，单轨系统噪音小，符合环保要求，安全可靠，同时又具有建设费用低廉、建设周期短、易于维护以及易于实现零部件本地化生产的优点。亦有在遊樂場內建築的單軌鐵路，專門運載遊人。

單軌鐵路由於不能透過行車軌道把電流回流，故只能採用軌道供電（通常是第四軌，一條供電，一條回流）。

## 種類

現代的單軌鐵路由[電動機推進](../Page/電動機.md "wikilink")，一般使用輪胎而不使用鋼製的車輪。輪胎會在路軌的上面及兩旁轉動，推動列車及維持平衡。早期單軌系統的設計是不能使用[轉轍器的](../Page/轉轍器.md "wikilink")，讓列車運作上出現很多不便。現代的單軌系統多數已可使用轉轍器，讓車輛可以駛進不同的線路，而同一線路亦可作雙程行駛。

### 懸掛式單軌鐵路

懸掛式單軌鐵路的列車懸掛在軌道之下。最先提出懸掛式單軌的則是，也是所有單軌鐵路技術最早出現的，在中國大陸也被稱之為“**空軌**”或“**空铁**”，乃“空中軌道”或“空中铁路”之簡稱\[1\]。懸掛式的優點在於其重心位於軌道下方，轉彎的穩定性較佳，可以使迴旋半徑變小，轉轍器的結構設計也比較簡單，適合在較曲折且空間受限的路線，而且部分採用軌道內嵌設計的有抗天候問題的優勢，特別是中高[緯度或](../Page/緯度.md "wikilink")[海拔等易下雪的地區其軌道不易積雪或結冰而受影響](../Page/海拔.md "wikilink")。缺點是跟[索道一樣行駛易對地面的車輛與行人產生壓迫感](../Page/索道.md "wikilink")，而且也容易發生事故，此外其逃生也不易，因此目前較少這種單軌鐵路技術，也造成此技術的造價相對偏高。

### 跨座式單軌鐵路

跨座式單軌鐵路的列車跨座在路軌之上的。跨座式單軌最早是由[德國的](../Page/德國.md "wikilink")（Alweg
GmbH）發明。跨座式的優點在於其逃生較容易解決，而且不易對對地面的車輛與行人產生壓迫感和造成事故，乘客的安全感也較高。缺點是跟一般高架鐵道一樣會受天候如積雪等影響運行，也因為重心偏高，迴旋半徑也不可太小，轉轍器也較複雜。這種單軌鐵路技術目前較多，造價相對偏低。

## 優點與缺點

### 優點

  - 所佔空間小。不單是所佔的地面面積小，垂直空間亦較小。單軌鐵路所需的寬度主要由車輛的寬度決定，與軌距無關。且單軌鐵路多數以高架興建，地面上只需很小的空間建造承托路軌的橋墩。
  - 相比其他架空鐵路，單軌所佔的空間較小，較不影響視線。
  - 單軌使用[橡膠輪胎在混凝土路軌上行走](../Page/橡膠.md "wikilink")，比較安靜。
  - 單軌鐵路的攀爬能力及迴轉速度比普通鐵路好。
  - 單軌因為在自己的軌道上行走，拥有全程A级路权，與其他交通工具及行人分開，因此比其他在路面上行走的鐵路安全。跨座式單軌車輛以車身包圍路軌，因此不容易出軌。
  - 單軌的造價及維修價格比地下鐵路等為低。

### 缺點

[缩略图转辙完毕时的景象](../Page/file:Tama_toshi_monorail_switch.JPG.md "wikilink")，可见到两旁的轨道悬空\]\]

  - 必須另外興建軌道。
  - 對於地面而言，單軌所佔的空間較[地下鐵路多](../Page/地下鐵路.md "wikilink")。
  - 除[日本外](../Page/日本.md "wikilink")，單軌的路軌沒有大小標準。
  - 如果出現緊急情況，單軌鐵路上的乘客沒有[逃生的地方](../Page/逃生.md "wikilink")。車的兩旁沒有可站立的路軌，而且離地面很高。頭尾兩端的路軌亦很窄。有些單軌鐵路因此在路軌的兩旁建有可供人行的緊急通道。[中国](../Page/中国.md "wikilink")[重庆轨道交通三号线全程在两条轨道之间的下方位置铺设钢制的逃生通道](../Page/重庆轨道交通三号线.md "wikilink")，用于解决该问题，[日本甚至研發了一種特殊的逃生隧道供給自己國家的單軌鐵路路線使用](../Page/日本.md "wikilink")，原理類似於的隧道式的螺旋[溜滑梯](../Page/溜滑梯.md "wikilink")，只要將其攤開，再將一端拋於地面上，再直接鑽入隧道溜下來即可逃生。
  - 亦有人擔心單軌鐵路的速度及載客量及不上其他系統。不过少数单轨如日本、中国重庆的单轨部分区段达到了75Km/h的最大营运速度，已与部分地铁的运营速度相近。

## 各國與地區單軌鐵路系統

### 歐洲

[Schwebebahn_ueber_Strasse.jpg](https://zh.wikipedia.org/wiki/File:Schwebebahn_ueber_Strasse.jpg "fig:Schwebebahn_ueber_Strasse.jpg")[烏帕塔的](../Page/烏帕塔.md "wikilink")[懸浮列車](../Page/烏帕塔懸浮列車.md "wikilink")\]\]

  -   - [烏帕塔](../Page/烏帕塔.md "wikilink")：[伍珀塔爾空鐵](../Page/伍珀塔爾空鐵.md "wikilink")
        於1901年啟用，是仍然在運作中的全電動單軌鐵路系統中歷史最悠久者。採用獨特的鋼軌懸掛式列車來運行。管制期为1900年10月24日至今。
      - [德勒斯登](../Page/德勒斯登.md "wikilink")：是同樣於1901年通車，啟用時間稍晚於烏帕塔的系統。雖然兩者都是懸掛式的單軌鐵路系統，但德勒斯登的懸浮列車有兩點與烏帕塔不同，其一是它是用於攀登山坡的短程路線；其二是德勒斯登的列車本身並不具有電力驅動的能力，而是靠位於山頂的纜車站以鋼索牽引，所以亦屬於[纜索鐵路](../Page/纜索鐵路.md "wikilink")（登山纜車）的系統類型。

### 美洲

[MonorailOverLagoon_wb.jpg](https://zh.wikipedia.org/wiki/File:MonorailOverLagoon_wb.jpg "fig:MonorailOverLagoon_wb.jpg")\]\]

  -   - [加州](../Page/加州.md "wikilink")[迪士尼樂園度假區中的](../Page/迪士尼樂園度假區.md "wikilink")[迪士尼樂園單軌電車系統](../Page/迪士尼樂園單軌電車系統.md "wikilink")，及[佛羅里達州](../Page/佛羅里達州.md "wikilink")[華特迪士尼世界度假區中的](../Page/華特迪士尼世界度假區.md "wikilink")，每年載客量超過500萬人次。
      - [拉斯維加斯](../Page/拉斯維加斯.md "wikilink")：[拉斯維加斯單軌電車於](../Page/拉斯維加斯單軌電車.md "wikilink")2004年全線通車，是一條連結拉[斯維加斯大道沿線各](../Page/斯維加斯大道.md "wikilink")[賭場及](../Page/賭場.md "wikilink")[會議中心的單軌鐵路系統](../Page/會議中心.md "wikilink")。
      - [西雅圖](../Page/西雅圖.md "wikilink")：於1962年通車。
      - [底特律](../Page/底特律.md "wikilink")：于1987年建成通车。

### 亞洲

[TamaMonorail0841.jpg](https://zh.wikipedia.org/wiki/File:TamaMonorail0841.jpg "fig:TamaMonorail0841.jpg")（）\]\]
[Pyrmont_bridge_291105.JPG](https://zh.wikipedia.org/wiki/File:Pyrmont_bridge_291105.JPG "fig:Pyrmont_bridge_291105.JPG")[雪梨單軌電車](../Page/雪梨.md "wikilink")（已拆除）\]\]
[E-Da_Theme_Park_Monorail.jpg](https://zh.wikipedia.org/wiki/File:E-Da_Theme_Park_Monorail.jpg "fig:E-Da_Theme_Park_Monorail.jpg")

  -   - [重慶](../Page/重慶.md "wikilink")：市內有兩條跨座式單軌鐵路，其中[重慶軌道交通二號線於](../Page/重慶軌道交通二號線.md "wikilink")2005年正式運營，在兩條軌道之間設有乘客緊急通道的[重慶軌道交通三號線於](../Page/重慶軌道交通三號線.md "wikilink")2011年9月底開始載客運營。
      - [西安](../Page/西安.md "wikilink")：[曲江旅游单轨线](../Page/曲江.md "wikilink")，位于[曲江新区](../Page/曲江新区.md "wikilink")，为曲江新区旅游配套项目，线路起于环塔东路，沿线经[大雁塔北广场](../Page/大雁塔.md "wikilink")、雁塔南路、雁南二路、曲江池内环湖路、北池头一路、芙蓉东路，止于广场东路，全长约9.3公里。运行模式为单轨单向环路通行。项目机车采用电力驱动、橡胶轮运行，每列机车配置6节车厢，每节车厢满载8人，每列机车满载为48人\[2\]。
      - [深圳](../Page/深圳.md "wikilink")：连接[华侨城内](../Page/华侨城.md "wikilink")[锦绣中华](../Page/锦绣中华.md "wikilink")、[世界之窗](../Page/世界之窗_\(深圳\).md "wikilink")、[欢乐谷几个旅游度假区的](../Page/深圳歡樂谷.md "wikilink")[欢乐干线](../Page/欢乐干线.md "wikilink")。
      - [银川](../Page/银川.md "wikilink")：银川花博园[云轨环线长](../Page/云轨.md "wikilink")5.67公里，在2017年9月开通运营。
      - [柳州](../Page/柳州.md "wikilink")：轨道交通1、2号线正在建设中。
      - [桂林](../Page/桂林.md "wikilink")：连接[临桂区的](../Page/临桂区.md "wikilink")[桂林两江国际机场和](../Page/桂林两江国际机场.md "wikilink")[象山区的](../Page/象山区.md "wikilink")[桂林站的云轨预计于](../Page/桂林站.md "wikilink")2019年7月开工，线路全长29.27千米，共设车站13座。

<!-- end list -->

  -   - [臺北](../Page/臺北.md "wikilink")：[臺北市立兒童新樂園單軌列車](../Page/臺北市立兒童新樂園.md "wikilink")，提供園內連接
      - [南投](../Page/南投縣.md "wikilink")：[九族文化村單軌列車](../Page/九族文化村.md "wikilink")，提供園內連接
      - [高雄](../Page/高雄.md "wikilink")：[義大世界城堡列車](../Page/義大世界.md "wikilink")，提供園內連接

<!-- end list -->

  -   - [東京都](../Page/東京都.md "wikilink")
          - [東京單軌電車](../Page/東京單軌電車.md "wikilink")
          - [上野動物園懸吊式單軌](../Page/上野動物園懸吊式單軌.md "wikilink")
          - [多摩都市單軌電車](../Page/多摩都市單軌電車.md "wikilink")
      - [大阪府](../Page/大阪府.md "wikilink")：[大阪高速鐵道](../Page/大阪高速鐵道.md "wikilink")，一般又習稱為「[大阪單軌電車](../Page/大阪單軌電車.md "wikilink")」
      - [千葉縣](../Page/千葉縣.md "wikilink")
          - [千葉都市單軌電車](../Page/千葉都市單軌電車.md "wikilink")
          - [迪士尼度假區線](../Page/迪士尼度假區線.md "wikilink")，提供[東京迪士尼度假區內各區域連接](../Page/東京迪士尼度假區.md "wikilink")
      - [神奈川縣](../Page/神奈川縣.md "wikilink")：[湘南單軌電車](../Page/湘南單軌電車.md "wikilink")
      - [廣島縣](../Page/廣島縣.md "wikilink")：
      - [福岡縣](../Page/福岡縣.md "wikilink")：[北九州高速鐵道](../Page/北九州高速鐵道.md "wikilink")
      - [沖繩縣](../Page/沖繩縣.md "wikilink")：[沖繩都市單軌電車](../Page/沖繩都市單軌電車.md "wikilink")

<!-- end list -->

  -   - [吉隆坡](../Page/吉隆坡.md "wikilink")：[吉隆坡單軌列車主要是連接市中心的主要景点](../Page/吉隆坡單軌列車.md "wikilink")，每年乘客流量超過兩千萬人次。
      - [马六甲市](../Page/马六甲市.md "wikilink")：主要主要是用于观光用途，轨道建立在马六甲河旁，于2017年末重新启用。

<!-- end list -->

  -   - [聖淘沙](../Page/聖淘沙.md "wikilink")：[聖淘沙捷運連接](../Page/聖淘沙捷運.md "wikilink")[港灣和](../Page/港灣.md "wikilink")[聖淘沙的單軌鐵路](../Page/聖淘沙.md "wikilink")。另有[聖淘沙單軌列車](../Page/聖淘沙單軌列車.md "wikilink")，但已經廢除。
      - [裕廊飛禽公園遊覽單軌車](../Page/裕廊飛禽公園遊覽單軌車.md "wikilink")

<!-- end list -->

  -   - [馬哈拉施特拉邦](../Page/馬哈拉施特拉邦.md "wikilink")：[孟買單軌列車](../Page/孟買單軌火車.md "wikilink")

### 大洋洲

  -   - [悉尼](../Page/悉尼.md "wikilink")：[悉尼單軌電車穿梭於](../Page/悉尼單軌電車.md "wikilink")[達令港及](../Page/達令港.md "wikilink")[市中心](../Page/悉尼商業中心區.md "wikilink")，主要為遊客使用，因使用率不佳，當局決定拆除\[3\]。
      - [黃金海岸](../Page/黃金海岸_\(澳大利亞\).md "wikilink")：與單軌電車線。

### 預計建造的單軌鐵路系統

  -   - [遼寧省](../Page/遼寧省.md "wikilink")[沈阳市有计划修建单轨铁路连接棋盘山风景区各景点](../Page/沈阳市.md "wikilink")，包括[东陵公园](../Page/东陵公园.md "wikilink")、[世博园及](../Page/世博园.md "wikilink")[秀湖等](../Page/秀湖.md "wikilink")\[4\]。
      - [江苏省](../Page/江苏省.md "wikilink")[苏州市的](../Page/苏州市.md "wikilink")[滨湖新城单轨将于](../Page/吴江滨湖新城单轨交通工程.md "wikilink")2013年初开始建设\[5\]。
      - [吉林省](../Page/吉林省.md "wikilink")[吉林市计划修建跨座式单轨用于市内的公共交通](../Page/吉林市.md "wikilink")\[6\]。
      - [安徽省](../Page/安徽省.md "wikilink")[芜湖市的轨道交通项目计划采用跨座式单轨制式建设](../Page/芜湖市.md "wikilink")，2015年将陆续开展完善规划、上报审批工作\[7\]。
      - [河南省](../Page/河南省.md "wikilink")[郑州市的](../Page/郑州市.md "wikilink")[郑东新区将建两条跨坐式单轨](../Page/郑东新区.md "wikilink")，一条为龙湖线，沿中环路呈环状敷设，计划为双线。一条为如意线，连接正副CBD，计划为单线。在如意东路站交叉，规划采用付费区共用站厅换乘\[8\]。

  -   - 香港政府計劃在[啟德興建一条](../Page/啟德發展計劃.md "wikilink")[單軌鐵路連接各啟德](../Page/啟德環保連接系統.md "wikilink")、附近的基礎建設、文娛設施及[港鐵車站](../Page/港鐵.md "wikilink")。

  -   - [布城单轨目前仍在初步研究阶段](../Page/布城单轨.md "wikilink")，预计最早2021年完工，也可能建造[电车代替](../Page/电车.md "wikilink")。

      - [大槟城城市轨道交通预计将建设](../Page/大槟城.md "wikilink")3条单轨铁路，分别是[槟岛北岸的](../Page/槟岛.md "wikilink")[丹绒道光线](../Page/丹绒道光.md "wikilink")、槟岛中心的[亚依淡线以及](../Page/亚依淡.md "wikilink")[威省](../Page/威省.md "wikilink")[北海至](../Page/威省北海.md "wikilink")[大山脚的单轨线](../Page/大山脚.md "wikilink")。\[9\]

  -   - [胡志明市地鐵將興建兩至三條單軌鐵路或輕軌線路](../Page/胡志明市地鐵.md "wikilink")，全長37公里。

  -   - [臺南市政府規劃興建](../Page/臺南市政府.md "wikilink")[臺南捷運綠線](../Page/臺南捷運綠線.md "wikilink")、[臺南捷運藍線兩條中運量高架單軌捷運系統](../Page/臺南捷運藍線.md "wikilink")，其中安平線長約17公里，藍線已獲得核定通過，預計2019年進行綜合規劃，預計於2020年動工，2025年通車。\[10\]\[11\]。

  -   -
## 相關條目

  - [輕軌運輸系統](../Page/輕軌運輸系統.md "wikilink")

## 参考文献

## 外部連結

  - [The Monorail Org](http://www.monorails.org)

{{-}}

[單軌電車](../Category/單軌電車.md "wikilink")
[Category:鐵路](../Category/鐵路.md "wikilink")

1.
2.
3.
4.
5.
6.
7.  [芜湖城市轨道交通项目前期工作加快推进](http://news.163.com/15/0130/09/AH6SFJ6K00014AEE.html)
8.  [<http://henan.youth.cn/2015/0411/1100932> 郑东新区将建如意线龙湖线2条轻轨
    建设周期3年](http://henan.youth.cn/2015/0411/1100932.shtml)
9.  <http://pgmasterplan.penang.gov.my/index.php/en/2016-02-26-03-12-57/pengangkutan-awam/monorail>
10.
11.