**決明屬**（[学名](../Page/学名.md "wikilink")：**）是一種[豆科](../Page/豆科.md "wikilink")[決明族的植物分類](../Page/決明族.md "wikilink")。這個屬的植物有：

  - [決明子](../Page/決明子.md "wikilink") *Senna obtusifolia*
  - [望江南](../Page/望江南.md "wikilink") *Senna occidentalis*
  - [鵝鑾鼻決明](../Page/鵝鑾鼻決明.md "wikilink") *Cassia garambiensis*
  - [鐵刀木](../Page/鐵刀木.md "wikilink")
  - 金急雨([阿勃勒](../Page/阿勃勒.md "wikilink"))

## 外部链接

  -
[\*](../Category/决明属.md "wikilink")