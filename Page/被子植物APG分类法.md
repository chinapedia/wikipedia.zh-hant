《**被子植物APG分类法**》是1998年由[被子植物种系发生学组](../Page/被子植物种系发生学组.md "wikilink")（APG，Angiosperm
Phylogeny Group）出版的一种对于[被子植物的现代分类法](../Page/被子植物门.md "wikilink")。

和传统的依照形态分类不同，这种分类法主要依照[植物的三个](../Page/植物.md "wikilink")[基因组](../Page/基因.md "wikilink")[DNA的顺序](../Page/DNA.md "wikilink")，以[亲缘分支的方法分类](../Page/亲缘分支分类法.md "wikilink")，包括两个[叶绿体和一个](../Page/叶绿体.md "wikilink")[核糖体的基因编码](../Page/核糖体.md "wikilink")。虽然主要依据[分子生物学的数据](../Page/分子生物学.md "wikilink")，但是也参照其他方面的理论，例如将[真双子叶植物分支和其他原来分到](../Page/真双子叶植物分支.md "wikilink")[双子叶植物纲中的种类区分](../Page/双子叶植物纲.md "wikilink")，也是根据花粉形态学的理论。

2003年这种分类法出版了修订版《[被子植物APG II分类法
(修订版)](../Page/被子植物APG_II分类法_\(修订版\).md "wikilink")》，2009年又出版了
[APG III](../Page/APG_III.md "wikilink")，2016年更新到 [APG
IV](../Page/APG_IV.md "wikilink")。

## 背景

1970年代之後，[被子植物的分類系統似乎到達一個穩定的狀態](../Page/被子植物.md "wikilink")，藉助於新技術從事傳統的[分類學研究](../Page/分類學.md "wikilink")，並由於此類資料的大量累積，當時仍極為常用的[克朗奎斯特分類系統](../Page/阿瑟·克朗奎斯特.md "wikilink")(Arthur
John
Cronquist，1919年3月19日－1992年3月22日)及[塔赫塔江分類系統](../Page/亚美因·塔赫塔江.md "wikilink")(Armen
Takhtajan:1910年6月10日－2009年11月13日)系統漸漸顯得過時\[1\]\[2\]。加上當時許多研究，涉及了分子技術的引用，為許多分類群提供了明確的[种系发生学上的數據](../Page/种系发生学.md "wikilink")。即使[亚美因·塔赫塔江亦投注極大的心力於其系統的完善](../Page/亚美因·塔赫塔江.md "wikilink")，但被子植物分類學上一個新的[种系发生学的面相應被重新提出](../Page/种系发生学.md "wikilink")，尤其針對[科與](../Page/科_\(生物\).md "wikilink")[目等高階](../Page/目_\(生物\).md "wikilink")[分類群的](../Page/分類群.md "wikilink")[分類](../Page/分類學.md "wikilink")\[3\]。植物种系发生学组於是提出此APG系統。

## 方法

**APG
(1998年)**選擇相當數量的[單源的](../Page/單系群.md "wikilink")[亞科](../Page/亞科.md "wikilink")[分類群](../Page/分類群.md "wikilink")，如此對於[被子植物的高階](../Page/被子植物.md "wikilink")[分類極有為用](../Page/分類學_\(生物學\).md "wikilink")。但對於此[分類群的正式分類處理](../Page/分類群.md "wikilink")，與予以**种系发生学上的[命名](../Page/命名.md "wikilink")**實則不同，命名上當時仍存在諸多爭議\[4\]。APG採用了《國際植物命名法規》的方式，為[科級及](../Page/科_\(生物\).md "wikilink")[目級的分類群命名](../Page/目_\(生物\).md "wikilink")。

關於目級以上的分類，APG特別提出：習慣上使用的目級以上分類群概念，與命名上的優先律的命名概念經常造成衝突，在尋求平衡此衝突時，APG特別強調其處理並非依法(命名法規)為之\[5\]。

### 分類群的界線

  - 依[單系群概念界定](../Page/單系群.md "wikilink")[分類群](../Page/分類群.md "wikilink")。如此，種以上階層被定義或界定時，其演化序列未予以指明，如此可以方便溝通，特別是為[科級及](../Page/科_\(生物\).md "wikilink")[目級的分類群定義其範圍及屬於系統樹架構的位置時](../Page/目_\(生物\).md "wikilink")\[6\]。
  - 界定[分類群時若有替代性的選擇](../Page/分類群.md "wikilink")，APG會選擇具容易辨識的[形態](../Page/植物形態學.md "wikilink")[衍徵](../Page/衍徵.md "wikilink")，並參考[解剖](../Page/植物解剖學.md "wikilink")、[生化](../Page/生物化學.md "wikilink")、[發生等](../Page/發生學.md "wikilink")[特徵](../Page/特徵.md "wikilink")\[7\]。
  - 在[科级的分类时](../Page/科.md "wikilink")，依單系群概念，許多小科勢必被界定出來而不是在傳統的大科之中，這樣的争议亦在於在[目以上的分类群](../Page/目.md "wikilink")。

### 分類群的命名

  - **目以上的分類群**：本系統並未使用传统的分类階層學名：如[门](../Page/门.md "wikilink")、[纲等](../Page/纲.md "wikilink")，而是使用一般性的名詞定義“分支”，例如[单子叶植物分支](../Page/单子叶植物分支.md "wikilink")、[真双子叶植物分支](../Page/真双子叶植物分支.md "wikilink")、[蔷薇分支](../Page/蔷薇分支.md "wikilink")、[菊分支等](../Page/菊分支.md "wikilink")。
  - **目的定義與界定**：許多目的學名之採用，考慮到其大小，通常涵蓋約10至20個科。同時許多小目被界定出來，由於其為單系群，且其親緣關係已確立，因此不放在未確定群而成為一個**目**
    \[8\]。

## 注釋

<references />

## 參考文獻

  - ，retrieved 2011-06-29。

  - Angiosperm phylogeny group. 2003. [An update of the Angiosperm
    Phylogeny Group classification for the orders and families of
    flowering plants: APG
    II](http://onlinelibrary.wiley.com/doi/10.1046/j.1095-8339.2003.t01-1-00158.x/full).
    Botanical Journal of the Linnean Society 141: 399–436.[PDF檔
    (英文)](http://onlinelibrary.wiley.com/doi/10.1046/j.1095-8339.2003.t01-1-00158.x/pdf)，retrieved
    2011-06-29。

  - Angiosperm phylogeny group. 2009. [An update of the Angiosperm
    Phylogeny Group classification for the orders and families of
    flowering plants: APG
    III](http://onlinelibrary.wiley.com/doi/10.1111/j.1095-8339.2009.00996.x/full).
    Botanical Journal of the Linnean Society 161: 105–121. [PDF檔
    (英文)](http://onlinelibrary.wiley.com/doi/10.1046/j.1095-8339.2003.t01-1-00158.x/pdf)，retrieved
    2011-06-29。

  - Stevens, P. F. 1986. Evolutionary classification in botany,
    1960-1985. J. Arnold Arbor. 67: 313-339.

-----

## 系統

基本分支如下：

  - **被子植物** **angiosperms :**
      -
        **[单子叶植物分支](../Page/单子叶植物分支.md "wikilink")** **monocots**
          -
            **[鸭跖草分支](../Page/鸭跖草分支.md "wikilink")** **commelinids**
        **[真双子叶植物分支](../Page/真双子叶植物分支.md "wikilink")** **eudicots**
          -
            **[核心真双子叶植物分支](../Page/核心真双子叶植物分支.md "wikilink")** **core
            eudicots**
              -
                **[蔷薇分支](../Page/蔷薇分支.md "wikilink")** **rosids**
                  -
                    **[I类真蔷薇分支](../Page/I类真蔷薇分支.md "wikilink")**
                    **eurosids I**
                    **[II类真蔷薇分支](../Page/II类真蔷薇分支.md "wikilink")**
                    **eurosids II**
                ''' [菊分支](../Page/菊分支.md "wikilink") ''' **asterids**
                  -
                    *' [I类真菊分支](../Page/I类真菊分支.md "wikilink")*'
                    **euasterids I**
                    **[II类真菊分支](../Page/II类真菊分支.md "wikilink")**
                    **euasterids II**

参见 [颜色示意图](http://www.biologie.uni-hamburg.de/b-online/apg/APG.html)

-----

APG系统下设分类包括40个[目和](../Page/目.md "wikilink")462个[科](../Page/科.md "wikilink")，下列表中开始部分是没有分到任何一个[分支内的科或目](../Page/分支.md "wikilink")。有的科分布地域非常狭小或新分出的科，尚未有通用的[汉语译名](../Page/汉语.md "wikilink")。

  - **被子植物分支** **angiosperms**
      -

          -
            [无油樟科](../Page/无油樟科.md "wikilink") *Amborellaceae*
            [木兰藤科](../Page/木兰藤科.md "wikilink") *Austrobaileyaceae*
            [白桂皮科](../Page/白桂皮科.md "wikilink") *Canellaceae*
            [金粟兰科](../Page/金粟兰科.md "wikilink") *Chloranthaceae*
            [菌花科](../Page/菌花科.md "wikilink") *Hydnoraceae*
            [八角茴香科](../Page/八角茴香科.md "wikilink") *Illiciaceae*
            [睡莲科](../Page/睡莲科.md "wikilink") *Nymphaeaceae* +
            [莼菜科](../Page/莼菜科.md "wikilink") *Cabombaceae*
            [大花草科](../Page/大花草科.md "wikilink") *Rafflesiaceae*
            [五味子科](../Page/五味子科.md "wikilink") *Schisandraceae*
            [腺齿木科](../Page/腺齿木科.md "wikilink") *Trimeniaceae*
            [林仙科](../Page/林仙科.md "wikilink") *Winteraceae*

        <!-- end list -->

          -
            [金鱼藻目](../Page/金鱼藻目.md "wikilink") *Ceratophyllales*
              -

                  -
                    [金鱼藻科](../Page/金鱼藻科.md "wikilink")
                    *Ceratophyllaceae*
            [樟目](../Page/樟目.md "wikilink") *Laurales*
              -

                  -
                    [香皮茶科](../Page/香皮茶科.md "wikilink")
                    *Atherospermataceae*
                    [腊梅科](../Page/腊梅科.md "wikilink") *Calycanthaceae*
                    [腺蕊花科](../Page/腺蕊花科.md "wikilink") *Gomortegaceae*
                    [莲叶桐科](../Page/莲叶桐科.md "wikilink") *Hernandiaceae*
                    [樟科](../Page/樟科.md "wikilink") *Lauraceae*
                    [杯轴花科](../Page/杯轴花科.md "wikilink") *Monimiaceae*
                    [坛罐花科](../Page/坛罐花科.md "wikilink") *Siparunaceae*
            [木兰目](../Page/木兰目.md "wikilink") *Magnoliales*
              -

                  -
                    [番荔枝科](../Page/番荔枝科.md "wikilink") *Annonaceae*
                    [单室木兰科](../Page/单室木兰科.md "wikilink") *Degeneriaceae*
                    [帽花木科](../Page/帽花木科.md "wikilink") *Eupomatiaceae*
                    [舌蕊花科](../Page/舌蕊花科.md "wikilink") ''
                    Himantandraceae''
                    [木兰科](../Page/木兰科.md "wikilink") '' Magnoliaceae''
                    [肉豆蔻科](../Page/肉豆蔻科.md "wikilink") ''
                    Myristicaceae''
            [胡椒目](../Page/胡椒目.md "wikilink") *Piperales*
              -

                  -
                    [馬兜鈴科](../Page/馬兜鈴科.md "wikilink")
                    *Aristolochiaceae*
                    [短蕊花科](../Page/短蕊花科.md "wikilink") *Lactoridaceae*
                    [胡椒科](../Page/胡椒科.md "wikilink") *Piperaceae*
                    [三白草科](../Page/三白草科.md "wikilink") *Saururaceae*

        ''' [单子叶植物分支](../Page/单子叶植物分支.md "wikilink") ''' **monocots**

          -

              -
                [白玉簪科](../Page/白玉簪科.md "wikilink") *Corsiaceae*
                [樱井草科](../Page/樱井草科.md "wikilink") *Japonoliriaceae*
                [纳茜菜科](../Page/纳茜菜科.md "wikilink") *Nartheciaceae*
                [无叶莲科](../Page/无叶莲科.md "wikilink") *Petrosaviaceae*
                [霉草科](../Page/霉草科.md "wikilink") *Triuridaceae*

            <!-- end list -->

              -
                [菖蒲目](../Page/菖蒲目.md "wikilink") *Acorales*
                  -

                      -
                        [菖蒲科](../Page/菖蒲科.md "wikilink") *Acoraceae*
                [泽泻目](../Page/泽泻目.md "wikilink") *Alismatales*
                  -

                      -
                        [泽泻科](../Page/泽泻科.md "wikilink") ''
                        Alismataceae''
                        [水蕹科](../Page/水蕹科.md "wikilink")
                        *Aponogetonaceae*
                        [天南星科](../Page/天南星科.md "wikilink") *Araceae*
                        [花蔺科](../Page/花蔺科.md "wikilink") *Butomaceae*
                        [丝粉藻科](../Page/丝粉藻科.md "wikilink")
                        *Cymodoceaceae*
                        [水鳖科](../Page/水鳖科.md "wikilink")
                        *Hydrocharitaceae*
                        [水麦冬科](../Page/水麦冬科.md "wikilink")
                        *Juncaginaceae*
                        [黄花绒叶草科](../Page/黄花绒叶草科.md "wikilink")
                        *Limnocharitaceae*
                        [波喜荡草科](../Page/波喜荡草科.md "wikilink")
                        *Posidoniaceae*
                        [眼子菜科](../Page/眼子菜科.md "wikilink")
                        *Potamogetonaceae*
                        [川蔓藻科](../Page/川蔓藻科.md "wikilink") *Ruppiaceae*
                        [芝菜科](../Page/芝菜科.md "wikilink")
                        *Scheuchzeriaceae*
                        [岩菖蒲科](../Page/岩菖蒲科.md "wikilink")
                        *Tofieldiaceae*
                        [大叶藻科](../Page/大叶藻科.md "wikilink") *Zosteraceae*
                [天門冬目](../Page/天門冬目.md "wikilink") *Asparagales*
                  -

                      -
                        [百子莲科](../Page/百子莲科.md "wikilink")
                        *Agapanthaceae*
                        [龙舌兰科](../Page/龙舌兰科.md "wikilink") *Agavaceae*
                        [葱科](../Page/葱科.md "wikilink") *Alliaceae*
                        [石蒜科](../Page/石蒜科.md "wikilink")
                        *Amaryllidaceae*
                        [知母科](../Page/知母科.md "wikilink")
                        *Anemarrhenaceae*
                        [吊兰科](../Page/吊兰科.md "wikilink") *Anthericaceae*
                        [星捧月科](../Page/星捧月科.md "wikilink")
                        *Aphyllanthaceae*
                        [天門冬科](../Page/天門冬科.md "wikilink")
                        *Asparagaceae*
                        [独尾草科](../Page/独尾草科.md "wikilink")
                        *Asphodelaceae*
                        [芳香草科](../Page/芳香草科.md "wikilink") *Asteliaceae*
                        [非洲菝葜科](../Page/非洲菝葜科.md "wikilink")
                        *Behniaceae*
                        [香水花科](../Page/香水花科.md "wikilink")
                        *Blandfordiaceae*
                        [澳韭兰科](../Page/澳韭兰科.md "wikilink") *Boryaceae*
                        [铃兰科](../Page/铃兰科.md "wikilink")
                        *Convallariaceae*
                        [矛花科](../Page/矛花科.md "wikilink") *Doryanthaceae*
                        [萱草科](../Page/萱草科.md "wikilink")
                        *Hemerocallidaceae*
                        [异菝葜科](../Page/异菝葜科.md "wikilink")
                        *Herreriaceae*
                        [风信子科](../Page/风信子科.md "wikilink")
                        *Hyacinthaceae*
                        [仙茅科](../Page/仙茅科.md "wikilink") *Hypoxidaceae*
                        [鸢尾科](../Page/鸢尾科.md "wikilink") *Iridaceae*
                        [鸢尾蒜科](../Page/鸢尾蒜科.md "wikilink")
                        *Ixioliriaceae*
                        [毛石蒜科](../Page/毛石蒜科.md "wikilink") *Lanariaceae*
                        [异蕊草科](../Page/异蕊草科.md "wikilink")
                        *Laxmanniaceae*
                        [兰科](../Page/兰科.md "wikilink") *Orchidaceae*
                        [蓝星科](../Page/蓝星科.md "wikilink")
                        *Tecophilaeaceae*
                        [紫灯花科](../Page/紫灯花科.md "wikilink") *Themidaceae*
                        [刺叶树科](../Page/刺叶树科.md "wikilink")
                        *Xanthorrhoeaceae*
                        [血剑草科](../Page/血剑草科.md "wikilink")
                        *Xeronemataceae*
                [薯蕷目](../Page/薯蕷目.md "wikilink") *Dioscoreales*
                  -

                      -
                        [水玉簪科](../Page/水玉簪科.md "wikilink")
                        *Burmanniaceae*
                        [薯蕷科](../Page/薯蕷科.md "wikilink") *Dioscoreaceae*
                        [蒟蒻薯科](../Page/蒟蒻薯科.md "wikilink") *Taccaceae*
                        [肉质腐生科](../Page/肉质腐生科.md "wikilink")
                        *Thismiaceae*
                        [毛柄花科](../Page/毛柄花科.md "wikilink")
                        *Trichopodaceae*
                [百合目](../Page/百合目.md "wikilink") *Liliales*
                  -

                      -
                        [六出花科](../Page/六出花科.md "wikilink")
                        *Alstroemeriaceae*
                        [金梅草科](../Page/金梅草科.md "wikilink")
                        *Campynemataceae*
                        [秋水仙科](../Page/秋水仙科.md "wikilink")
                        *Colchicaceae*
                        [百合科](../Page/百合科.md "wikilink") *Liliaceae*
                        [菝葜木科](../Page/菝葜木科.md "wikilink")
                        *Luzuriagaceae*
                        [黑药花科](../Page/黑药花科.md "wikilink")
                        *Melanthiaceae*
                        [垂花科](../Page/垂花科.md "wikilink") *Philesiaceae*
                        [菝葜藤科](../Page/菝葜藤科.md "wikilink")
                        *Rhipogonaceae*
                        [菝葜科](../Page/菝葜科.md "wikilink") *Smilacaceae*
                [露兜树目](../Page/露兜树目.md "wikilink") *Pandanales*
                  -

                      -
                        [环花草科](../Page/环花草科.md "wikilink")
                        *Cyclanthaceae*
                        [露兜树科](../Page/露兜树科.md "wikilink") *Pandanaceae*
                        [百部科](../Page/百部科.md "wikilink") *Stemonaceae*
                        [翡若翠科](../Page/翡若翠科.md "wikilink")
                        *Velloziaceae*

            ''' [鸭跖草分支](../Page/鸭跖草分支.md "wikilink") '''
            **commelinoids**

              -

                  -
                    [阿波波达草科](../Page/阿波波达草科.md "wikilink")
                    *Abolbodaceae*
                    [凤梨科](../Page/凤梨科.md "wikilink") *Bromeliaceae*
                    [多须草科](../Page/多须草科.md "wikilink") *Dasypogonaceae*
                    [匍茎草科](../Page/匍茎草科.md "wikilink") *Hanguanaceae*
                    [苔草科](../Page/苔草科.md "wikilink") *Mayacaceae*
                    [偏穗草科](../Page/偏穗草科.md "wikilink") *Rapateaceae*

                <!-- end list -->

                  -
                    [槟榔目](../Page/槟榔目.md "wikilink") *Arecales*
                      -

                          -
                            [槟榔科](../Page/槟榔科.md "wikilink") *Arecaceae*
                    [鸭跖草目](../Page/鸭跖草目.md "wikilink") *Commelinales*
                      -

                          -
                            [鸭跖草科](../Page/鸭跖草科.md "wikilink")
                            *Commelinaceae*
                            [血皮草科](../Page/血皮草科.md "wikilink")
                            *Haemodoraceae*
                            [田葱科](../Page/田葱科.md "wikilink")
                            *Philydraceae*
                            [雨久花科](../Page/雨久花科.md "wikilink")
                            *Pontederiaceae*
                    [禾本目](../Page/禾本目.md "wikilink") *Poales*
                      -

                          -
                            [苞穗草科](../Page/苞穗草科.md "wikilink")
                            *Anarthriaceae*
                            [刺鳞草科](../Page/刺鳞草科.md "wikilink")
                            *Centrolepidaceae*
                            [莎草科](../Page/莎草科.md "wikilink")
                            *Cyperaceae*
                            [二柱草科](../Page/二柱草科.md "wikilink")
                            *Ecdeiocoleaceae*
                            [谷精草科](../Page/谷精草科.md "wikilink")
                            *Eriocaulaceae*
                            [须叶藤科](../Page/须叶藤科.md "wikilink")
                            *Flagellariaceae*
                            [独蕊草科](../Page/独蕊草科.md "wikilink")
                            *Hydatellaceae*
                            [拟苇科](../Page/拟苇科.md "wikilink")
                            *Joinvilleaceae*
                            [灯心草科](../Page/灯心草科.md "wikilink")
                            *Juncaceae*
                            [禾本科](../Page/禾本科.md "wikilink") *Poaceae*
                            [袖珍棕榈科](../Page/袖珍棕榈科.md "wikilink")
                            *Prioniaceae*
                            [帚灯草科](../Page/帚灯草科.md "wikilink")
                            *Restionaceae*
                            [黑三棱科](../Page/黑三棱科.md "wikilink")
                            *Sparganiaceae*
                            [梭子草科](../Page/梭子草科.md "wikilink")
                            *Thurniaceae*
                            [香蒲科](../Page/香蒲科.md "wikilink") *Typhaceae*
                            [黄眼草科](../Page/黄眼草科.md "wikilink")
                            *Xyridaceae*
                    [姜目](../Page/姜目.md "wikilink") *Zingiberales*
                      -

                          -
                            [美人蕉科](../Page/美人蕉科.md "wikilink")
                            *Cannaceae*
                            [闭鞘姜科](../Page/闭鞘姜科.md "wikilink")
                            *Costaceae*
                            [蝎尾蕉科](../Page/蝎尾蕉科.md "wikilink")
                            *Heliconiaceae*
                            [兰花蕉科](../Page/兰花蕉科.md "wikilink")
                            *Lowiaceae*
                            [竹芋科](../Page/竹芋科.md "wikilink")
                            *Marantaceae*
                            [芭蕉科](../Page/芭蕉科.md "wikilink") *Musaceae*
                            [鹤望兰科](../Page/鹤望兰科.md "wikilink")
                            *Strelitziaceae*
                            [姜科](../Page/姜科.md "wikilink")
                            *Zingiberaceae*

        ''' [真双子叶植物分支](../Page/真双子叶植物分支.md "wikilink") ''' **eudicots**

          -

              -
                [黄杨科](../Page/黄杨科.md "wikilink") *Buxaceae*
                [双颊果科](../Page/双颊果科.md "wikilink") *Didymelaceae*
                [清风藤科](../Page/清风藤科.md "wikilink") *Sabiaceae*
                [昆栏树科](../Page/昆栏树科.md "wikilink") *Trochodendraceae* +
                [水青树科](../Page/水青树科.md "wikilink") *Tetracentraceae*

            <!-- end list -->

              -
                [山龙眼目](../Page/山龙眼目.md "wikilink") *Proteales*
                  -

                      -
                        [莲科](../Page/莲科.md "wikilink") *Nelumbonaceae*
                        [悬铃木科](../Page/悬铃木科.md "wikilink") *Platanaceae*
                        [山龙眼科](../Page/山龙眼科.md "wikilink") *Proteaceae*
                [毛茛目](../Page/毛茛目.md "wikilink") *Ranunculales*
                  -

                      -
                        [小檗科](../Page/小檗科.md "wikilink") *Berberidaceae*
                        [星叶草科](../Page/星叶草科.md "wikilink")
                        *Circaeasteraceae* +
                        [独叶草科](../Page/独叶草科.md "wikilink")
                        *Kingdoniaceae*
                        [领春木科](../Page/领春木科.md "wikilink")
                        *Eupteleaceae*
                        [木通科](../Page/木通科.md "wikilink")
                        *Lardizabalaceae*
                        [防己科](../Page/防己科.md "wikilink")
                        *Menispermaceae*
                        [罂粟科](../Page/罂粟科.md "wikilink") *Papaveraceae*
                        + [荷包牡丹科](../Page/荷包牡丹科.md "wikilink")
                        *Fumariaceae*+
                        [蕨叶草科](../Page/蕨叶草科.md "wikilink")
                        *Pteridophyllaceae*
                        [毛茛科](../Page/毛茛科.md "wikilink") *Ranunculaceae*

            ''' [核心真双子叶植物分支](../Page/核心真双子叶植物分支.md "wikilink") '''
            **core eudicots**

              -

                  -
                    [鳞枝树科](../Page/鳞枝树科.md "wikilink") *Aextoxicaceae*
                    [智利藤科](../Page/智利藤科.md "wikilink")
                    *Berberidopsidaceae*
                    [五桠果科](../Page/五桠果科.md "wikilink") *Dilleniaceae*
                    [洋二仙草科](../Page/洋二仙草科.md "wikilink") *Gunneraceae*
                    [香灌木科](../Page/香灌木科.md "wikilink") *Myrothamnaceae*
                    [葡萄科](../Page/葡萄科.md "wikilink") *Vitaceae*

                <!-- end list -->

                  -
                    [石竹目](../Page/石竹目.md "wikilink") *Caryophyllales*
                      -

                          -
                            [玛瑙果科](../Page/玛瑙果科.md "wikilink")
                            *Achatocarpaceae*
                            [番杏科](../Page/番杏科.md "wikilink") *Aizoaceae*
                            [苋科](../Page/苋科.md "wikilink")
                            *Amaranthaceae*
                            [钩枝藤科](../Page/钩枝藤科.md "wikilink")
                            *Ancistrocladaceae*
                            [翼萼茶科](../Page/翼萼茶科.md "wikilink")
                            *Asteropeiaceae*
                            [落葵科](../Page/落葵科.md "wikilink")
                            *Basellaceae*
                            [仙人掌科](../Page/仙人掌科.md "wikilink")
                            *Cactaceae*
                            [石竹科](../Page/石竹科.md "wikilink")
                            *Caryophyllaceae*
                            [龙树科](../Page/龙树科.md "wikilink")
                            *Didiereaceae*
                            [双钩叶科](../Page/双钩叶科.md "wikilink")
                            *Dioncophyllaceae*
                            [茅膏菜科](../Page/茅膏菜科.md "wikilink")
                            *Droseraceae*
                            [露叶毛毡苔科](../Page/露叶毛毡苔科.md "wikilink")
                            *Drosophyllaceae*
                            [瓣鳞花科](../Page/瓣鳞花科.md "wikilink")
                            *Frankeniaceae*
                            [粟米草科](../Page/粟米草科.md "wikilink")
                            *Molluginaceae*
                            [猪笼草科](../Page/猪笼草科.md "wikilink")
                            *Nepenthaceae*
                            [紫茉莉科](../Page/紫茉莉科.md "wikilink")
                            *Nyctaginaceae*
                            [非洲桐科](../Page/非洲桐科.md "wikilink")
                            *Physenaceae*
                            [商陆科](../Page/商陆科.md "wikilink")
                            *Phytolaccaceae*
                            [蓝雪科](../Page/蓝雪科.md "wikilink")
                            *Plumbaginaceae*
                            [蓼科](../Page/蓼科.md "wikilink")
                            *Polygonaceae*
                            [马齿苋科](../Page/马齿苋科.md "wikilink")
                            *Portulacaceae*
                            [棒木科](../Page/棒木科.md "wikilink")
                            *Rhabdodendraceae*
                            [肉叶刺茎藜科](../Page/肉叶刺茎藜科.md "wikilink")
                            *Sarcobataceae*
                            [油蜡树科](../Page/油蜡树科.md "wikilink")
                            *Simmondsiaceae*
                            [闭籽花科](../Page/闭籽花科.md "wikilink")
                            *Stegnospermataceae*
                            [柽柳科](../Page/柽柳科.md "wikilink")
                            *Tamaricaceae*
                    [檀香目](../Page/檀香目.md "wikilink") *Santalales*
                      -

                          -
                            [铁青树科](../Page/铁青树科.md "wikilink")
                            *Olacaceae*
                            [山柚子科](../Page/山柚子科.md "wikilink")
                            *Opiliaceae*
                            [桑寄生科](../Page/桑寄生科.md "wikilink")
                            *Loranthaceae*
                            [羽毛果科](../Page/羽毛果科.md "wikilink")
                            *Misodendraceae*
                            [檀香科](../Page/檀香科.md "wikilink")
                            *Santalaceae*
                    [虎耳草目](../Page/虎耳草目.md "wikilink") *Saxifragales*
                      -

                          -
                            [枫香科](../Page/枫香科.md "wikilink")
                            *Altingiaceae*
                            [连香树科](../Page/连香树科.md "wikilink")
                            *Cercidiphyllaceae*
                            [景天科](../Page/景天科.md "wikilink")
                            *Crassulaceae*
                            [交让木科](../Page/交让木科.md "wikilink")
                            *Daphniphyllaceae*
                            [茶藨子科](../Page/茶藨子科.md "wikilink")
                            *Grossulariaceae*
                            [小二仙草科](../Page/小二仙草科.md "wikilink")
                            *Haloragaceae*
                            [金缕梅科](../Page/金缕梅科.md "wikilink")
                            *Hamamelidaceae*
                            [鼠刺科](../Page/鼠刺科.md "wikilink") *Iteaceae*
                            [芍药科](../Page/芍药科.md "wikilink")
                            *Paeoniaceae*
                            [扯根菜科](../Page/扯根菜科.md "wikilink")
                            *Penthoraceae*
                            [齿蕊科](../Page/齿蕊科.md "wikilink")
                            *Pterostemonaceae*
                            [虎耳草科](../Page/虎耳草科.md "wikilink")
                            *Saxifragaceae*
                            [四果木科](../Page/四果木科.md "wikilink")
                            *Tetracarpaeaceae*

                ''' [蔷薇分支](../Page/蔷薇分支.md "wikilink") ''' **rosids**

                  -

                      -
                        [单果树科](../Page/单果树科.md "wikilink") *Aphloiaceae*
                        [燧体木科](../Page/燧体木科.md "wikilink")
                        *Crossosomataceae*
                        [西兰木科](../Page/西兰木科.md "wikilink") *Ixerbaceae*
                        [刺球果科](../Page/刺球果科.md "wikilink")
                        *Krameriaceae*
                        [美洲苦木科](../Page/美洲苦木科.md "wikilink")
                        *Picramniaceae*
                        [川苔草科](../Page/川苔草科.md "wikilink")
                        *Podostemaceae*
                        [旌节花科](../Page/旌节花科.md "wikilink")
                        *Stachyuraceae*
                        [省沽油科](../Page/省沽油科.md "wikilink")
                        *Staphyleaceae*
                        [三裂科](../Page/三裂科.md "wikilink") *Tristichaceae*
                        [蒺藜科](../Page/蒺藜科.md "wikilink")
                        *Zygophyllaceae*

                    <!-- end list -->

                      -
                        [牻牛儿苗目](../Page/牻牛儿苗目.md "wikilink")
                        *Geraniales*
                          -

                              -
                                [花茎草科](../Page/花茎草科.md "wikilink")
                                *Francoaceae*
                                [牻牛儿苗科](../Page/牻牛儿苗科.md "wikilink")
                                *Geraniaceae* +
                                [高柱花科](../Page/高柱花科.md "wikilink")
                                *Hypseocharitaceae*
                                [鞘叶树科](../Page/鞘叶树科.md "wikilink")
                                *Greyiaceae*
                                [杜香果科](../Page/杜香果科.md "wikilink")
                                *Ledocarpaceae*
                                [蜜花科](../Page/蜜花科.md "wikilink")
                                *Melianthaceae*
                                [曲胚科](../Page/曲胚科.md "wikilink")
                                *Vivianiaceae*

                    **[I类真蔷薇分支](../Page/I类真蔷薇分支.md "wikilink")**
                    **eurosids I**

                      -

                          -
                            [卫矛科](../Page/卫矛科.md "wikilink")
                            *Celastraceae*
                            [蒜树科](../Page/蒜树科.md "wikilink") *Huaceae*
                            [梅花草科](../Page/梅花草科.md "wikilink")
                            *Parnassiaceae* +
                            [微形草科](../Page/微形草科.md "wikilink")
                            *Lepuropetalaceae*
                            [木根草科](../Page/木根草科.md "wikilink")
                            *Stackhousiaceae*

                        <!-- end list -->

                          -
                            [葫芦目](../Page/葫芦目.md "wikilink")
                            *Cucurbitales*
                              -

                                  -
                                    [四柱木科](../Page/四柱木科.md "wikilink")
                                    *Anisophylleaceae*
                                    [秋海棠科](../Page/秋海棠科.md "wikilink")
                                    *Begoniaceae*
                                    [马桑科](../Page/马桑科.md "wikilink")
                                    *Coriariaceae*
                                    [棒果木科](../Page/棒果木科.md "wikilink")
                                    *Corynocarpaceae*
                                    [葫芦科](../Page/葫芦科.md "wikilink")
                                    *Cucurbitaceae*
                                    [野麻科](../Page/野麻科.md "wikilink")
                                    *Datiscaceae*
                                    [四数木科](../Page/四数木科.md "wikilink")
                                    *Tetramelaceae*
                            [豆目](../Page/豆目.md "wikilink") *Fabales*
                              -

                                  -
                                    [豆科](../Page/豆科.md "wikilink")
                                    *Fabaceae*
                                    [远志科](../Page/远志科.md "wikilink")
                                    *Polygalaceae*
                                    [皂皮树科](../Page/皂皮树科.md "wikilink")
                                    *Quillajaceae*
                                    [海人树科](../Page/海人树科.md "wikilink")
                                    *Surianaceae*
                            [壳斗目](../Page/壳斗目.md "wikilink") *Fagales*
                              -

                                  -
                                    [桦木科](../Page/桦木科.md "wikilink")
                                    *Betulaceae*
                                    [木麻黄科](../Page/木麻黄科.md "wikilink")
                                    *Casuarinaceae*
                                    [山毛榉科](../Page/山毛榉科.md "wikilink")
                                    *Fagaceae*
                                    [胡桃科](../Page/胡桃科.md "wikilink")
                                    *Juglandaceae*
                                    [杨梅科](../Page/杨梅科.md "wikilink")
                                    *Myricaceae*
                                    [南青冈科](../Page/南青冈科.md "wikilink")
                                    *Nothofagaceae*
                                    [马尾树科](../Page/马尾树科.md "wikilink")
                                    *Rhoipteleaceae*
                                    [太果木科](../Page/太果木科.md "wikilink")
                                    *Ticodendraceae*
                            [金虎尾目](../Page/金虎尾目.md "wikilink")
                            *Malpighiales*
                              -

                                  -
                                    [钟花科](../Page/钟花科.md "wikilink")
                                    *Achariaceae*
                                    [槲树果科](../Page/槲树果科.md "wikilink")
                                    *Balanopaceae*
                                    [油桃木科](../Page/油桃木科.md "wikilink")
                                    *Caryocaraceae*
                                    [金壳果科](../Page/金壳果科.md "wikilink")
                                    *Chrysobalanaceae*
                                    [藤黄科](../Page/藤黄科.md "wikilink")
                                    *Clusiaceae*
                                    [毒鼠子科](../Page/毒鼠子科.md "wikilink")
                                    *Dichapetalaceae*
                                    [古柯科](../Page/古柯科.md "wikilink")
                                    *Erythroxylaceae*
                                    [大戟科](../Page/大戟科.md "wikilink")
                                    *Euphorbiaceae*
                                    [合丝花科](../Page/合丝花科.md "wikilink")
                                    *Euphroniaceae*
                                    [大风子科](../Page/大风子科.md "wikilink")
                                    *Flacourtiaceae*
                                    [毛药树科](../Page/毛药树科.md "wikilink")
                                    *Goupiaceae*
                                    [亚麻藤科](../Page/亚麻藤科.md "wikilink")
                                    *Hugoniaceae*
                                    [香膏科](../Page/香膏科.md "wikilink")
                                    *Humiriaceae*
                                    [金丝桃科](../Page/金丝桃科.md "wikilink")
                                    *Hypericaceae*
                                    [包芽树科](../Page/包芽树科.md "wikilink")
                                    *Irvingiaceae*
                                    [粘木科](../Page/粘木科.md "wikilink")
                                    *Ixonanthaceae*
                                    [裂药花科](../Page/裂药花科.md "wikilink")
                                    *Lacistemataceae*
                                    [亚麻科](../Page/亚麻科.md "wikilink")
                                    *Linaceae*
                                    [王冠草科](../Page/王冠草科.md "wikilink")
                                    *Malesherbiaceae*
                                    [金虎尾科](../Page/金虎尾科.md "wikilink")
                                    *Malpighiaceae*
                                    [水母柱科](../Page/水母柱科.md "wikilink")
                                    *Medusagynaceae*
                                    [金莲木科](../Page/金莲木科.md "wikilink")
                                    *Ochnaceae*
                                    [小盘木科](../Page/小盘木科.md "wikilink")
                                    *Pandaceae*
                                    [西番莲科](../Page/西番莲科.md "wikilink")
                                    *Passifloraceae*
                                    [非洲核果木科](../Page/非洲核果木科.md "wikilink")
                                    *Putranjivaceae*
                                    [羽叶树科](../Page/羽叶树科.md "wikilink")
                                    *Quiinaceae*
                                    [红树科](../Page/红树科.md "wikilink")
                                    *Rhizophoraceae*
                                    [杨柳科](../Page/杨柳科.md "wikilink")
                                    *Salicaceae*
                                    [杯盖花科](../Page/杯盖花科.md "wikilink")
                                    *Scyphostegiaceae*
                                    [三角果科](../Page/三角果科.md "wikilink")
                                    *Trigoniaceae*
                                    [时钟花科](../Page/时钟花科.md "wikilink")
                                    *Turneraceae*
                                    [堇菜科](../Page/堇菜科.md "wikilink")
                                    *Violaceae*
                            [酢浆草目](../Page/酢浆草目.md "wikilink")
                            *Oxalidales*
                              -

                                  -
                                    [土瓶草科](../Page/土瓶草科.md "wikilink")
                                    *Cephalotaceae*
                                    [牛栓藤科](../Page/牛栓藤科.md "wikilink")
                                    *Connaraceae*
                                    [火把树科](../Page/火把树科.md "wikilink")
                                    *Cunoniaceae*
                                    [杜英科](../Page/杜英科.md "wikilink")
                                    *Elaeocarpaceae*
                                    [酢浆草科](../Page/酢浆草科.md "wikilink")
                                    *Oxalidaceae*
                                    [孔药花科](../Page/孔药花科.md "wikilink")
                                    *Tremandraceae*
                            [蔷薇目](../Page/蔷薇目.md "wikilink") *Rosales*
                              -

                                  -
                                    [钩毛树科](../Page/钩毛树科.md "wikilink")
                                    *Barbeyaceae*
                                    [大麻科](../Page/大麻科.md "wikilink")
                                    *Cannabaceae*
                                    [伞树科](../Page/伞树科.md "wikilink")
                                    *Cecropiaceae*
                                    [朴科](../Page/朴科.md "wikilink")
                                    *Celtidaceae*
                                    [八瓣果科](../Page/八瓣果科.md "wikilink")
                                    *Dirachmaceae*
                                    [胡颓子科](../Page/胡颓子科.md "wikilink")
                                    *Elaeagnaceae*
                                    [桑科](../Page/桑科.md "wikilink")
                                    *Moraceae*
                                    [鼠李科](../Page/鼠李科.md "wikilink")
                                    *Rhamnaceae*
                                    [蔷薇科](../Page/蔷薇科.md "wikilink")
                                    *Rosaceae*
                                    [榆科](../Page/榆科.md "wikilink")
                                    *Ulmaceae*
                                    [荨麻科](../Page/荨麻科.md "wikilink")
                                    *Urticaceae*

                    **[II类真蔷薇分支](../Page/II类真蔷薇分支.md "wikilink")**
                    **eurosids II**

                      -

                          -
                            [瘿椒树科](../Page/瘿椒树科.md "wikilink")
                            *Tapisciaceae*

                        <!-- end list -->

                          -
                            [十字花目](../Page/十字花目.md "wikilink")
                            *Brassicales*
                              -

                                  -
                                    [叠珠树科](../Page/叠珠树科.md "wikilink")
                                    *Akaniaceae* +
                                    [钟萼木科](../Page/钟萼木科.md "wikilink")
                                    *Bretschneideraceae*
                                    [藜木科](../Page/藜木科.md "wikilink")
                                    *Bataceae*
                                    [十字花科](../Page/十字花科.md "wikilink")
                                    *Brassicaceae*
                                    [番木瓜科](../Page/番木瓜科.md "wikilink")
                                    *Caricaceae*
                                    [澳远志科](../Page/澳远志科.md "wikilink")
                                    *Emblingiaceae*
                                    [环蕊科](../Page/环蕊科.md "wikilink")
                                    *Gyrostemonaceae*
                                    [刺枝树科](../Page/刺枝树科.md "wikilink")
                                    *Koeberliniaceae*
                                    [池花科](../Page/池花科.md "wikilink")
                                    *Limnanthaceae*
                                    [辣木科](../Page/辣木科.md "wikilink")
                                    *Moringaceae*
                                    [瘤药树科](../Page/瘤药树科.md "wikilink")
                                    *Pentadiplandraceae*
                                    [木犀草科](../Page/木犀草科.md "wikilink")
                                    *Resedaceae*
                                    [刺茉莉科](../Page/刺茉莉科.md "wikilink")
                                    *Salvadoraceae*
                                    [夷白花菜科](../Page/夷白花菜科.md "wikilink")
                                    *Setchellanthaceae*
                                    [烈味三叶草科](../Page/烈味三叶草科.md "wikilink")
                                    *Tovariaceae*
                                    [旱金莲科](../Page/旱金莲科.md "wikilink")
                                    *Tropaeolaceae*
                            [锦葵目](../Page/锦葵目.md "wikilink") *Malvales*
                              -

                                  -
                                    [红木科](../Page/红木科.md "wikilink")
                                    *Bixaceae* +
                                    [弯子木科](../Page/弯子木科.md "wikilink")
                                    *Cochlospermaceae*
                                    [半日花科](../Page/半日花科.md "wikilink")
                                    *Cistaceae*
                                    [地果莲木科](../Page/地果莲木科.md "wikilink")
                                    *Diegodendraceae*
                                    [龙脑香科](../Page/龙脑香科.md "wikilink")
                                    *Dipterocarpaceae*
                                    [锦葵科](../Page/锦葵科.md "wikilink")
                                    *Malvaceae*
                                    [文定果科](../Page/文定果科.md "wikilink")
                                    *Muntingiaceae*
                                    [沙莓科](../Page/沙莓科.md "wikilink")
                                    *Neuradaceae*
                                    [旋花树科](../Page/旋花树科.md "wikilink")
                                    *Sarcolaenaceae*
                                    [球萼树科](../Page/球萼树科.md "wikilink")
                                    *Sphaerosepalaceae*
                                    [瑞香科](../Page/瑞香科.md "wikilink")
                                    *Thymelaeaceae*
                            [桃金娘目](../Page/桃金娘目.md "wikilink")
                            *Myrtales*
                              -

                                  -
                                    [双翼果科](../Page/双翼果科.md "wikilink")
                                    *Alzateaceae*
                                    [使君子科](../Page/使君子科.md "wikilink")
                                    *Combretaceae*
                                    [隐翼科](../Page/隐翼科.md "wikilink")
                                    *Crypteroniaceae*
                                    [异裂果科](../Page/异裂果科.md "wikilink")
                                    *Heteropyxidaceae*
                                    [千屈菜科](../Page/千屈菜科.md "wikilink")
                                    *Lythraceae*
                                    [野牡丹科](../Page/野牡丹科.md "wikilink")
                                    *Melastomataceae*
                                    [谷木科](../Page/谷木科.md "wikilink")
                                    *Memecylaceae*
                                    [桃金娘科](../Page/桃金娘科.md "wikilink")
                                    *Myrtaceae*
                                    [方枝树科](../Page/方枝树科.md "wikilink")
                                    *Oliniaceae*
                                    [柳叶菜科](../Page/柳叶菜科.md "wikilink")
                                    *Onagraceae*
                                    [管萼科](../Page/管萼科.md "wikilink")
                                    *Penaeaceae*
                                    [裸木科](../Page/裸木科.md "wikilink")
                                    *Psiloxylaceae*
                                    [喙萼花科](../Page/喙萼花科.md "wikilink")
                                    *Rhynchocalycaceae*
                                    [蜡烛树科](../Page/蜡烛树科.md "wikilink")
                                    *Vochysiaceae*
                            [无患子目](../Page/无患子目.md "wikilink")
                            *Sapindales*
                              -

                                  -
                                    [漆树科](../Page/漆树科.md "wikilink")
                                    *Anacardiaceae*
                                    [薰倒牛科](../Page/薰倒牛科.md "wikilink")
                                    *Biebersteiniaceae*
                                    [橄榄科](../Page/橄榄科.md "wikilink")
                                    *Burseraceae*
                                    [番苦木科](../Page/番苦木科.md "wikilink")
                                    *Kirkiaceae*
                                    [楝科](../Page/楝科.md "wikilink")
                                    *Meliaceae*
                                    [白刺科](../Page/白刺科.md "wikilink")
                                    *Nitrariaceae* +
                                    [骆驼蓬科](../Page/骆驼蓬科.md "wikilink")
                                    *Peganaceae*
                                    [芸香科](../Page/芸香科.md "wikilink")
                                    *Rutaceae*
                                    [无患子科](../Page/无患子科.md "wikilink")
                                    *Sapindaceae*
                                    [苦木科](../Page/苦木科.md "wikilink")
                                    *Simaroubaceae*

                ''' [菊分支](../Page/菊分支.md "wikilink") ''' **asterids**

                  -

                      -
                        [山茱萸目](../Page/山茱萸目.md "wikilink") *Cornales*
                          -

                              -
                                [山茱萸科](../Page/山茱萸科.md "wikilink")
                                *Cornaceae* +
                                [珙桐科](../Page/珙桐科.md "wikilink")
                                *Nyssaceae*
                                [假石南科](../Page/假石南科.md "wikilink")
                                *Grubbiaceae*
                                [绣球花科](../Page/绣球花科.md "wikilink")
                                *Hydrangeaceae*
                                [水穗草科](../Page/水穗草科.md "wikilink")
                                *Hydrostachyaceae*
                                [刺莲花科](../Page/刺莲花科.md "wikilink")
                                *Loasaceae*
                        [杜鹃花目](../Page/杜鹃花目.md "wikilink") *Ericales*
                          -

                              -
                                [猕猴桃科](../Page/猕猴桃科.md "wikilink")
                                *Actinidiaceae*
                                [凤仙花科](../Page/凤仙花科.md "wikilink")
                                *Balsaminaceae*
                                [山柳科](../Page/山柳科.md "wikilink")
                                *Clethraceae*
                                [翅萼树科](../Page/翅萼树科.md "wikilink")
                                *Cyrillaceae*
                                [岩梅科](../Page/岩梅科.md "wikilink")
                                *Diapensiaceae*
                                [柿树科](../Page/柿树科.md "wikilink")
                                *Ebenaceae*
                                [杜鹃花科](../Page/杜鹃花科.md "wikilink")
                                *Ericaceae*
                                [福桂花科](../Page/福桂花科.md "wikilink")
                                *Fouquieriaceae*
                                [银钟花科](../Page/银钟花科.md "wikilink")
                                *Halesiaceae*
                                [玉蕊科](../Page/玉蕊科.md "wikilink")
                                *Lecythidaceae*
                                [蜜囊花科](../Page/蜜囊花科.md "wikilink")
                                *Marcgraviaceae*
                                [紫金牛科](../Page/紫金牛科.md "wikilink")
                                *Myrsinaceae*
                                [假红树科](../Page/假红树科.md "wikilink")
                                *Pellicieraceae*
                                [花荵科](../Page/花荵科.md "wikilink")
                                *Polemoniaceae*
                                [报春花科](../Page/报春花科.md "wikilink")
                                *Primulaceae*
                                [捕蝇幌科](../Page/捕蝇幌科.md "wikilink")
                                *Roridulaceae*
                                [山榄科](../Page/山榄科.md "wikilink")
                                *Sapotaceae*
                                [瓶子草科](../Page/瓶子草科.md "wikilink")
                                *Sarraceniaceae*
                                [野茉莉科](../Page/野茉莉科.md "wikilink")
                                *Styracaceae*
                                [山矾科](../Page/山矾科.md "wikilink")
                                *Symplocaceae*
                                [厚皮香科](../Page/厚皮香科.md "wikilink")
                                *Ternstroemiaceae*
                                [四籽树科](../Page/四籽树科.md "wikilink")
                                *Tetrameristaceae*
                                [山茶科](../Page/山茶科.md "wikilink")
                                *Theaceae*
                                [假轮叶科](../Page/假轮叶科.md "wikilink")
                                *Theophrastaceae*

                    *' [I类真菊分支](../Page/I类真菊分支.md "wikilink")*'
                    **euasterids I**

                      -

                          -
                            [紫草科](../Page/紫草科.md "wikilink")
                            *Boraginaceae*
                            [环生籽科](../Page/环生籽科.md "wikilink")
                            *Plocospermataceae*
                            [二歧草科](../Page/二歧草科.md "wikilink")
                            *Vahliaceae*

                        <!-- end list -->

                          -
                            [绞木目](../Page/绞木目.md "wikilink") *Garryales*
                              -

                                  -
                                    [桃叶珊瑚科](../Page/桃叶珊瑚科.md "wikilink")
                                    *Aucubaceae*
                                    [杜仲科](../Page/杜仲科.md "wikilink")
                                    *Eucommiaceae*
                                    [绞木科](../Page/绞木科.md "wikilink")
                                    *Garryaceae*
                                    [五蕊茶科](../Page/五蕊茶科.md "wikilink")
                                    *Oncothecaceae*
                            [龙胆目](../Page/龙胆目.md "wikilink")
                            *Gentianales*
                              -

                                  -
                                    [夹竹桃科](../Page/夹竹桃科.md "wikilink")
                                    *Apocynaceae*
                                    [胡蔓藤科](../Page/胡蔓藤科.md "wikilink")
                                    *Gelsemiaceae*
                                    [龙胆科](../Page/龙胆科.md "wikilink")
                                    *Gentianaceae*
                                    [马钱科](../Page/马钱科.md "wikilink")
                                    *Loganiaceae*
                                    [茜草科](../Page/茜草科.md "wikilink")
                                    *Rubiaceae*
                            [唇形目](../Page/唇形目.md "wikilink") *Lamiales*
                              -

                                  -
                                    [爵床科](../Page/爵床科.md "wikilink")
                                    *Acanthaceae*
                                    [海榄雌科](../Page/海榄雌科.md "wikilink")
                                    *Avicenniaceae*
                                    [紫葳科](../Page/紫葳科.md "wikilink")
                                    *Bignoniaceae*
                                    [醉鱼草科](../Page/醉鱼草科.md "wikilink")
                                    *Buddlejaceae*
                                    [腺毛草科](../Page/腺毛草科.md "wikilink")
                                    *Byblidaceae*
                                    [盘果木科](../Page/盘果木科.md "wikilink")
                                    *Cyclocheilaceae*
                                    [苦苣苔科](../Page/苦苣苔科.md "wikilink")
                                    *Gesneriaceae*
                                    [唇形科](../Page/唇形科.md "wikilink")
                                    *Lamiaceae*
                                    [狸藻科](../Page/狸藻科.md "wikilink")
                                    *Lentibulariaceae*
                                    [苦槛蓝科](../Page/苦槛蓝科.md "wikilink")
                                    *Myoporaceae*
                                    [木犀科](../Page/木犀科.md "wikilink")
                                    *Oleaceae*
                                    [泡桐科](../Page/泡桐科.md "wikilink")
                                    *Paulowniaceae*
                                    [胡麻科](../Page/胡麻科.md "wikilink")
                                    *Pedaliaceae* +
                                    [角胡麻科](../Page/角胡麻科.md "wikilink")
                                    *Martyniaceae*
                                    [透骨草科](../Page/透骨草科.md "wikilink")
                                    *Phrymaceae*
                                    [车前科](../Page/车前科.md "wikilink")
                                    *Plantaginaceae*
                                    [夷地黄科](../Page/夷地黄科.md "wikilink")
                                    *Schlegeliaceae*
                                    [玄参科](../Page/玄参科.md "wikilink")
                                    *Scrophulariaceae*
                                    [密穗草科](../Page/密穗草科.md "wikilink")
                                    *Stilbaceae*
                                    [四粉草科](../Page/四粉草科.md "wikilink")
                                    *Tetrachondraceae*
                                    [马鞭草科](../Page/马鞭草科.md "wikilink")
                                    *Verbenaceae*
                            [茄目](../Page/茄目.md "wikilink") *Solanales*
                              -

                                  -
                                    [旋花科](../Page/旋花科.md "wikilink")
                                    *Convolvulaceae*
                                    [田基麻科](../Page/田基麻科.md "wikilink")
                                    *Hydroleaceae*
                                    [山醋李科](../Page/山醋李科.md "wikilink")
                                    *Montiniaceae*
                                    [茄科](../Page/茄科.md "wikilink")
                                    *Solanaceae*
                                    [楔瓣花科](../Page/楔瓣花科.md "wikilink")
                                    *Sphenocleaceae*

                    **[II类真菊分支](../Page/II类真菊分支.md "wikilink")**
                    **euasterids II**

                      -

                          -
                            [五福花科](../Page/五福花科.md "wikilink")
                            *Adoxaceae*
                            [鳞叶树科](../Page/鳞叶树科.md "wikilink")
                            *Bruniaceae*
                            [香茜科](../Page/香茜科.md "wikilink")
                            *Carlemanniaceae*
                            [弯药树科](../Page/弯药树科.md "wikilink")
                            *Columelliaceae* +
                            [离水花科](../Page/离水花科.md "wikilink")
                            *Desfontainiaceae*
                            [寄奴花科](../Page/寄奴花科.md "wikilink")
                            *Eremosynaceae*
                            [南鼠刺科](../Page/南鼠刺科.md "wikilink")
                            *Escalloniaceae*
                            [茶茱萸科](../Page/茶茱萸科.md "wikilink")
                            *Icacinaceae*
                            [多香木科](../Page/多香木科.md "wikilink")
                            *Polyosmaceae*
                            [楔蕊花科](../Page/楔蕊花科.md "wikilink")
                            *Sphenostemonaceae*
                            [智利木科](../Page/智利木科.md "wikilink")
                            *Tribelaceae*

                        <!-- end list -->

                          -
                            [伞形目](../Page/伞形目.md "wikilink") *Apiales*
                              -

                                  -
                                    [伞形科](../Page/伞形科.md "wikilink")
                                    *Apiaceae*
                                    [五加科](../Page/五加科.md "wikilink")
                                    *Araliaceae*
                                    [假茱萸科](../Page/假茱萸科.md "wikilink")
                                    *Aralidiaceae*
                                    [夷茱萸科](../Page/夷茱萸科.md "wikilink")
                                    *Griseliniaceae*
                                    [番茱萸科](../Page/番茱萸科.md "wikilink")
                                    *Melanophyllaceae*
                                    [海桐花科](../Page/海桐花科.md "wikilink")
                                    *Pittosporaceae*
                                    [鞘柄木科](../Page/鞘柄木科.md "wikilink")
                                    *Torricelliaceae*
                            [冬青目](../Page/冬青目.md "wikilink")
                            *Aquifoliales*
                              -

                                  -
                                    [冬青科](../Page/冬青科.md "wikilink")
                                    *Aquifoliaceae*
                                    [青荚叶科](../Page/青荚叶科.md "wikilink")
                                    *Helwingiaceae*
                                    [叶茶藨科](../Page/叶茶藨科.md "wikilink")
                                    *Phyllonomaceae*
                            [菊目](../Page/菊目.md "wikilink") *Asterales*
                              -

                                  -
                                    [假海桐科](../Page/假海桐科.md "wikilink")
                                    *Alseuosmiaceae*
                                    [雪叶科](../Page/雪叶科.md "wikilink")
                                    *Argophyllaceae*
                                    [菊科](../Page/菊科.md "wikilink")
                                    *Asteraceae*
                                    [头花草科](../Page/头花草科.md "wikilink")
                                    *Calyceraceae*
                                    [桔梗科](../Page/桔梗科.md "wikilink")
                                    *Campanulaceae* +
                                    [半边莲科](../Page/半边莲科.md "wikilink")
                                    *Lobeliaceae*
                                    [腕带花科](../Page/腕带花科.md "wikilink")
                                    *Carpodetaceae*
                                    [陀螺果科](../Page/陀螺果科.md "wikilink")
                                    *Donatiaceae*
                                    [草海桐科](../Page/草海桐科.md "wikilink")
                                    *Goodeniaceae*
                                    [睡菜科](../Page/睡菜科.md "wikilink")
                                    *Menyanthaceae*
                                    [五膜草科](../Page/五膜草科.md "wikilink")
                                    *Pentaphragmataceae*
                                    [石冬青科](../Page/石冬青科.md "wikilink")
                                    *Phellinaceae*
                                    [卢梭木科](../Page/卢梭木科.md "wikilink")
                                    *Rousseaceae*
                                    [花柱草科](../Page/花柱草科.md "wikilink")
                                    *Stylidiaceae*
                            [川续断目](../Page/川续断目.md "wikilink")
                            *Dipsacales*
                              -

                                  -
                                    [忍冬科](../Page/忍冬科.md "wikilink")
                                    *Caprifoliaceae*
                                    [黄锦带科](../Page/黄锦带科.md "wikilink")
                                    *Diervillaceae*
                                    [川续断科](../Page/川续断科.md "wikilink")
                                    *Dipsacaceae*
                                    [北极花科](../Page/北极花科.md "wikilink")
                                    *Linnaeaceae*
                                    [刺续断科](../Page/刺续断科.md "wikilink")
                                    *Morinaceae*
                                    [败酱科](../Page/败酱科.md "wikilink")
                                    *Valerianaceae*

注: "+ ..." 是一种建议选择，也可以单独分为一科。

-----

  - 系属不清的科：
      -
        [蛇菰科](../Page/蛇菰科.md "wikilink") *Balanophoraceae*
        [多子科](../Page/多子科.md "wikilink") *Bonnetiaceae*
        [心翼果科](../Page/心翼果科.md "wikilink") *Cardiopteridaceae*
        [垂籽树科](../Page/垂籽树科.md "wikilink") *Ctenolophonaceae*
        [锁阳科](../Page/锁阳科.md "wikilink") *Cynomoriaceae*
        [簇花草科](../Page/簇花草科.md "wikilink") *Cytinaceae*
        [十齿花科](../Page/十齿花科.md "wikilink") *Dipentodontaceae*
        [沟繁缕科](../Page/沟繁缕科.md "wikilink") *Elatinaceae*
        [四棱果科](../Page/四棱果科.md "wikilink") *Geissolomataceae*
        [单柱花科](../Page/单柱花科.md "wikilink") *Hoplestigmataceae*
        [扁果树科](../Page/扁果树科.md "wikilink") *Kaliphoraceae*
        [洋酢浆草科](../Page/洋酢浆草科.md "wikilink") *Lepidobotryaceae*
        [光果科](../Page/光果科.md "wikilink") *Lissocarpaceae*
        [五翼果科](../Page/五翼果科.md "wikilink") *Lophopyxidaceae*
        [毛丝花科](../Page/毛丝花科.md "wikilink") *Medusandraceae*
        [管花木科](../Page/管花木科.md "wikilink") *Mettenusiaceae*
        [帽蕊草科](../Page/帽蕊草科.md "wikilink") *Mitrastemonaceae*
        [盔瓣花科](../Page/盔瓣花科.md "wikilink") *Paracryphiaceae*
        [五列木科](../Page/五列木科.md "wikilink") *Pentaphylacaceae*
        [围盘树科](../Page/围盘树科.md "wikilink") *Peridiscaceae*
        [斜翼科](../Page/斜翼科.md "wikilink") *Plagiopteraceae*
        [单室木科](../Page/单室木科.md "wikilink") *Pottingeriaceae*
        [肋果茶科](../Page/肋果茶科.md "wikilink") *Sladeniaceae*
        [栓皮果科](../Page/栓皮果科.md "wikilink") *Strasburgeriaceae*
        [苦皮树科](../Page/苦皮树科.md "wikilink") *Tepuianthaceae*

## 外部链接

  - [分类对照](http://www.csdl.tamu.edu/FLORA/newgate/apg1ang.htm)

[Category:生物分類學](../Category/生物分類學.md "wikilink")
[Category:植物學](../Category/植物學.md "wikilink")
[Category:被子植物](../Category/被子植物.md "wikilink")

1.  \[APG, 1998.\]，第532頁，retrieved 2011-06-29。

2.  \[Stevens, P. F. 1986.\]

3.
4.
5.
6.
7.
8.