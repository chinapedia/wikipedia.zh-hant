**方從哲**（），[字](../Page/表字.md "wikilink")**中涵**，[直隸](../Page/北直隸.md "wikilink")[大興县](../Page/大興县.md "wikilink")（今[豐台區](../Page/豐台區.md "wikilink")）人，[祖籍](../Page/祖籍.md "wikilink")[浙江](../Page/浙江.md "wikilink")[德清](../Page/德清.md "wikilink")，[明朝政治人物](../Page/明朝.md "wikilink")，[進士出身](../Page/進士.md "wikilink")。

## 簡介

[萬曆十一年](../Page/萬曆.md "wikilink")（1583年）登癸未科[進士](../Page/進士.md "wikilink")，曾官[国子监](../Page/国子监.md "wikilink")[司业](../Page/司业.md "wikilink")、[祭酒](../Page/祭酒.md "wikilink")，請罷[矿税](../Page/矿税.md "wikilink")。得罪[司礼监](../Page/司礼监.md "wikilink")[秉笔太监](../Page/秉笔太监.md "wikilink")[田义](../Page/田义.md "wikilink")，放歸。[万历四十一年](../Page/万历.md "wikilink")（1613年）[叶向高举荐方从哲出任](../Page/叶向高.md "wikilink")[礼部右侍郎](../Page/礼部右侍郎.md "wikilink")。同年九月，與[吴道南同加](../Page/吴道南.md "wikilink")[礼部尚书](../Page/礼部尚书.md "wikilink")、[東阁大学士](../Page/東阁大学士.md "wikilink")\[1\]，多次上书言事，[万历四十四年](../Page/万历.md "wikilink")（1616年）[努爾哈赤号](../Page/努爾哈赤.md "wikilink")[后金国汗](../Page/后金国.md "wikilink")，建元[天命](../Page/天命.md "wikilink")。方从哲疏请曰：“今缺饷至于数月，诸军饥不得食，寒不得衣。……宜速发内帑数十万，先尽该镇，次及九边，用以抒燃眉之忧。”但[神宗不納](../Page/明神宗.md "wikilink")。從哲獨相七年，雖然不乏上疏力言國事，但往往只著重於名義上的爭取，實將順從皇帝的意思，終庸庸無所匡救。

[光宗時期任](../Page/明光宗.md "wikilink")[內閣首輔](../Page/內閣首輔.md "wikilink")，他將[李可灼帶進宮內任](../Page/李可灼.md "wikilink")[鴻臚寺丞](../Page/鴻臚寺.md "wikilink")，進呈紅丸，結果皇帝[朱常洛吃了一丸之後](../Page/朱常洛.md "wikilink")，直呼“忠臣！忠臣！”下午三時復進一丸，九月一日五更時暴斃，在位僅二十九天。這是[明朝著名的](../Page/明朝.md "wikilink")[紅丸案](../Page/紅丸案.md "wikilink")。[禮部尚書](../Page/禮部尚書.md "wikilink")[孫慎行聲討方從哲與李可灼](../Page/孫慎行.md "wikilink")。方從哲為了脫罪，說「李可灼沒有罪，給皇帝治病就該賞銀。」定調為「進藥不效，但亦臣愛君之意」。

[泰昌元年](../Page/泰昌.md "wikilink")（1620年）十一月三、二十日，连续两次上书乞休。[熹宗慰留之](../Page/明熹宗.md "wikilink")。十二月二十八日，批准。李可灼發戍邊疆。[天啟年間](../Page/天啟.md "wikilink")，[徐大化请起從哲](../Page/徐大化.md "wikilink")，但從哲不出。[崇祯元年](../Page/崇祯.md "wikilink")（1628年）卒。赠**太傅**，[谥](../Page/谥.md "wikilink")**文端**。

## 参考文献

### 出处

### 書目

  - 《[明史](../Page/明史.md "wikilink")》方從哲傳。

{{-}}

[Category:明朝國子監司業](../Category/明朝國子監司業.md "wikilink")
[Category:明朝國子監祭酒](../Category/明朝國子監祭酒.md "wikilink")
[Category:明朝禮部侍郎](../Category/明朝禮部侍郎.md "wikilink")
[Category:明朝禮部尚書](../Category/明朝禮部尚書.md "wikilink")
[Category:明朝東閣大學士](../Category/明朝東閣大學士.md "wikilink")
[Category:明朝内阁首辅](../Category/明朝内阁首辅.md "wikilink")
[Category:諡文端](../Category/諡文端.md "wikilink")
[Category:北京人](../Category/北京人.md "wikilink")
[C](../Category/方姓.md "wikilink")

1.  [张凤翼](../Page/张凤翼.md "wikilink")：《贺方中涵老师入相序》