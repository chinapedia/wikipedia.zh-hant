**盧納縣**（）是[美國](../Page/美國.md "wikilink")[新墨西哥州西南部的一個縣](../Page/新墨西哥州.md "wikilink")，南界[墨西哥](../Page/墨西哥.md "wikilink")，面積7,680平方公里。根據[2010年人口普查](../Page/美國2010年人口普查.md "wikilink")，本縣共有人口25,016人。本縣縣治為[德明](../Page/德明_\(新墨西哥州\).md "wikilink")（Deming）。

## 歷史

[DemingSunset.jpg](https://zh.wikipedia.org/wiki/File:DemingSunset.jpg "fig:DemingSunset.jpg")
[Luna_County_New_Mexico_Court_House.jpg](https://zh.wikipedia.org/wiki/File:Luna_County_New_Mexico_Court_House.jpg "fig:Luna_County_New_Mexico_Court_House.jpg")
本縣於1901年3月16日成立，縣名紀念當地的牧牛場主、政客所羅門·盧納（Soloman Luna）。

於1916年3月16日，[墨西哥革命領袖](../Page/墨西哥革命.md "wikilink")[龐丘·維拉曾經攻打位於盧納縣南部](../Page/龐丘·維拉.md "wikilink")[美墨邊境附近的](../Page/美墨邊境.md "wikilink")[哥倫布](../Page/哥倫布_\(新墨西哥州\).md "wikilink")，戰事導致十八名哥倫布市居民和美國士兵死亡，75名墨西哥士兵死亡。\[1\]是次入侵是[第二次世界大戰](../Page/第二次世界大戰.md "wikilink")[日本皇軍佔領](../Page/日本皇軍.md "wikilink")[阿圖島以前最近一次有外國軍隊侵入美國領土](../Page/阿圖島.md "wikilink")。\[2\]

## 地理

根據[2000年人口普查](../Page/2000年美国人口普查.md "wikilink")，盧納縣的總面積為，其中有，即99.99%為陸地；，即0.01%為水域。\[3\]

### 毗鄰縣和直轄市

  - 新墨西哥州 [格蘭特縣](../Page/格蘭特縣_\(新墨西哥州\).md "wikilink")：西方
  - 新墨西哥州 [伊達爾戈縣](../Page/伊達爾戈縣_\(新墨西哥州\).md "wikilink")：西方
  - [新墨西哥州](../Page/新墨西哥州.md "wikilink")
    [謝拉縣](../Page/謝拉縣_\(新墨西哥州\).md "wikilink")：東北方
  - 新墨西哥州 [唐娜安娜縣](../Page/唐娜安娜縣_\(新墨西哥州\).md "wikilink")：東方
  - [墨西哥](../Page/墨西哥.md "wikilink") [奇瓦瓦州](../Page/奇瓦瓦州.md "wikilink")
    [阿森松直轄市](../Page/阿森松直轄市.md "wikilink")：南方

## 人口

### 2010年

根據[2010年人口普查](../Page/2010年美國人口普查.md "wikilink")，盧納縣擁有25,095居民。而人口是由77.7%[白人](../Page/歐裔美國人.md "wikilink")、1.1%[黑人](../Page/非裔美國人.md "wikilink")、1.3%[土著](../Page/美國土著.md "wikilink")、0.5%[亞洲人](../Page/亞裔美國人.md "wikilink")、16.8%其他[種族和](../Page/種族.md "wikilink")2.6%[混血](../Page/混血.md "wikilink")[构成](../Page/种族构成.md "wikilink")。而[西班牙裔或](../Page/西班牙裔美国人.md "wikilink")[拉丁美洲人佔了人口](../Page/拉丁美洲人.md "wikilink")61.5%。\[4\]

### 2000年

根據[2000年人口普查](../Page/2000年美國人口普查.md "wikilink")，盧納縣擁有25,016居民、9,397住戶和6,596家庭。\[5\]其[人口密度為每平方英里](../Page/人口密度.md "wikilink")8居民（每平方公里3居民）。\[6\]本縣擁有11,291間房屋单位，其密度為每平方英里4間（每平方公里1間）。\[7\]而人口是由74.3%[白人](../Page/歐裔美國人.md "wikilink")、0.94%[黑人](../Page/非裔美國人.md "wikilink")、1.11%[土著](../Page/美國土著.md "wikilink")、0.34%[亞洲人](../Page/亞裔美國人.md "wikilink")、20.23%其他[種族和](../Page/種族.md "wikilink")3.08%[混血](../Page/混血.md "wikilink")[构成](../Page/种族构成.md "wikilink")。而[西班牙裔或](../Page/西班牙裔美国人.md "wikilink")[拉丁美洲人佔了人口](../Page/拉丁美洲人.md "wikilink")57.70%。\[8\]

在9,397住户中，有33.9%擁有一個或以上的兒童（18歲以下）、53.6%為夫妻、12.4%為單親家庭、29.8%為非家庭、26.4%為獨居、14%住戶有同居長者。平均每戶有2.64人，而平均每個家庭則有3.2人。在25,016居民中，有30%為18歲以下、7.6%為18至24歲、22.7%為25至44歲、21.5%為45至64歲以及18.2%為65歲以上。人口的年齡中位數為37歲，女子對男子的性別比為100：95.2。成年人的性別比則為100：93.2。\[9\]

本縣的住戶收入中位數為$20,784，而家庭收入中位數則為$24,252。男性的收入中位數為$25,008，而女性的收入中位數則為$16,883，[人均收入為](../Page/人均收入.md "wikilink")$11,218。約27.2%家庭和32.9%人口在[貧窮線以下](../Page/貧窮線.md "wikilink")，包括46.8%兒童（18歲以下）及15.8%長者（65歲以上）。\[10\]

## 參考文獻

[L](../Category/新墨西哥州行政区划.md "wikilink")

1.

2.

3.

4.  ["2010 Census P.L. 94-171 Summary File
    Data"](http://www2.census.gov/census_2010/01-Redistricting_File--PL_94-171/California/).
    United States Census Bureau.

5.  [Population Profile of the United
    States: 2000](http://www.census.gov/population/www/pop-profile/profile2000.html)

6.  [Statistical profile of Alger County,
    Michigan](http://censtats.census.gov/data/MI/05026003.pdf) , United
    States Census Bureau, Census 2000

7.  [State and County QuickFacts](http://quickfacts.census.gov/qfd/)

8.  [Census 2000 gateway](http://www.census.gov/main/www/cen2000.html)

9.  [American FactFinder](http://factfinder.census.gov/)

10.