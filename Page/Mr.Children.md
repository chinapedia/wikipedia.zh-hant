**Mr.Children**（簡稱 \[Misuchiru\]
）是[日本當代最著名的](../Page/日本.md "wikilink")之一，成員有四人，包括主唱[樱井和寿](../Page/樱井和寿.md "wikilink")、[吉他手](../Page/吉他.md "wikilink")[田原健一](../Page/田原健一.md "wikilink")、[貝斯手](../Page/貝斯.md "wikilink")[中川敬輔](../Page/中川敬輔.md "wikilink")，和[鼓手](../Page/鼓.md "wikilink")[鈴木英哉](../Page/鈴木英哉.md "wikilink")。所屬經紀事務所是烏龍舍（2014年4月以前），所屬唱片公司是TOY'S
FACTORY。

在高中時期，櫻井和壽與中川敬輔已經在輕音樂社結識並且計畫組團，櫻井和壽又遊說原為[棒球社社員的田原健一加入](../Page/棒球.md "wikilink")。1988年9月，三人皆高中畢業，在中川敬輔的介紹下，鈴木英哉加入，四人組成團名「THE
WALL」的團體。1989年1月1日改團名為Mr.Children，從此Mr.Children成為[地下樂團四處遊唱](../Page/地下樂團.md "wikilink")，直到1992年在好友[寺岡呼人的引薦下](../Page/寺岡呼人.md "wikilink")，四人認識名[製作人](../Page/音樂製作人.md "wikilink")[小林武史](../Page/小林武史.md "wikilink")，Mr.Children正式打入主流市場，成為日後撼動[日本流行樂界的一股新力量](../Page/日本流行樂.md "wikilink")，掀起當時名為「Misuchiru現象」的風潮。

Mr.Children至今已發表了32張冠軍[單曲](../Page/單曲.md "wikilink")（不含跟[桑田佳祐合唱的](../Page/桑田佳祐.md "wikilink")〈[奇跡の地球](../Page/奇跡の地球.md "wikilink")〉以及網路配信下載），17張原創性（非精選）冠軍[專輯](../Page/音樂專輯.md "wikilink")。截至2011年8月，生涯唱片總銷量為約5497.9萬張\[1\]。2019年2月，CD總銷量（不含配信限定）為7,362萬張\[2\]。

## 音樂特色

受1960至1970年代[披頭四和](../Page/披頭四.md "wikilink")[滾石樂隊等樂團的曲風影響](../Page/滾石樂隊.md "wikilink")，Mr.Children自身的作品主要揉合了[流行曲和](../Page/流行曲.md "wikilink")[經典搖滾的特色](../Page/搖滾.md "wikilink")。Mr.Children也經常試驗不同的音樂風格，讓作品得以變化多端。除了不同形式的搖滾之外，他們也嘗試過[爵士樂](../Page/爵士樂.md "wikilink")、[藍調](../Page/藍調.md "wikilink")、[民謠](../Page/民謠.md "wikilink")、[舞曲等音樂](../Page/舞曲.md "wikilink")。在歌詞內容上，Mr.Children與一般專注在男女情愛上的流行音樂歌手、團體不同，除了一般的愛情主題外，他們也經常利用歌曲抒發他們對社會、家庭、親情與友情等方面事物的觀感，除此之外，該團素來也以歌詞中大量使用不屬於[日文](../Page/日文.md "wikilink")[常用漢字的冷僻詞彙而聞名](../Page/常用漢字.md "wikilink")。

## 團員

主音樱井和寿是樂團的靈魂人物，大部分樂曲均由他作曲。樱井的聲線也是Mr.Children的特色之一，他的音域範圍較一般男性為廣，而唱歌帶有明顯的西洋風格。樱井和寿除了唱歌之外，也會與田原健一一起彈[吉他](../Page/吉他.md "wikilink")、彈[鋼琴和](../Page/鋼琴.md "wikilink")[口琴](../Page/口琴.md "wikilink")。

吉他手田原健一和貝斯手中川敬輔，算是樂團中比較文靜的成員，演唱時大部分時候都留在舞台上不顯眼之處。而又稱Jen的鼓手鈴木英哉，則是樂團中最外向的成員，每次登台似乎都要脫掉上衣。他最能對應樱井的幽默感。

## 生平

### 1988年－1992年

4人於1988年組成樂隊。樂隊原本的名稱是「THE
WALL」，當時[鈴木英哉並未為樂團成員](../Page/鈴木英哉.md "wikilink")，他是在原來的鼓手退出後才加入的。\[3\]他們寄了很多[樣本唱片到](../Page/樣本唱片.md "wikilink")[唱片公司](../Page/唱片公司.md "wikilink")，但都沒有-{回音}-。在1988後半年的一頓晚餐上，他們決定改名。他們覺得「children」這個字不錯，但因為他們實際上不是小孩，所以在名字前加上「Mr.」。\[4\]成員們稱這轉變了他們對自己組合的看法。

改名後，Mr.Children應徵一所叫作「[澀-{谷}-La](../Page/澀谷.md "wikilink").mama」的live
house，第一次失敗，但第二次就通過了。在live
house的表演之後，他們被要求嘗試出道當職業樂隊。他們為此寄出了五張樣本唱片，但都不能引起唱片公司的興趣。樂隊接着在1991年停止活動三個月：鈴木英哉在一家[經濟型酒店當接待](../Page/經濟型酒店.md "wikilink")，[櫻井和壽在他爸爸的建築公司工作](../Page/櫻井和壽.md "wikilink")。在他們重返樂隊後製作了第六張樣本唱片，終於引起了[TOY'S
FACTORY的注意](../Page/TOY'S_FACTORY.md "wikilink")。這家唱片公司跟Mr.Children簽約，並讓他們為旗下[搖滾樂隊](../Page/搖滾樂隊.md "wikilink")做開場表演。在這段時期，他們認識了現在的[製作人](../Page/製作人.md "wikilink")[小林武史](../Page/小林武史.md "wikilink")。當時小林武史當時已為[南方之星的](../Page/南方之星.md "wikilink")[桑田佳祐和](../Page/桑田佳祐.md "wikilink")[小泉今日子等歌手編曲](../Page/小泉今日子.md "wikilink")，在樂壇獲得一定知名度。

### 1992年－1994年

1992年5月10日，Mr.Children發行他們的出道[專輯](../Page/專輯.md "wikilink")《[EVERYTHING](../Page/EVERYTHING_\(Mr.Children專輯\).md "wikilink")》。三個月後，他們於同年8月21日發行了首張[單曲](../Page/單曲.md "wikilink")〈[有你的夏天](../Page/有你的夏天.md "wikilink")〉。其後他們為專輯舉辦了兩次[巡迴演唱會](../Page/巡迴演唱會.md "wikilink")，都在9月23日和11月5日之間舉行，分別是「92
EVERYTHING TOUR」和「92 Your Everything
Tour」，共12場表演。為92年作結並開展下一年，他們在12月1日發行了第2張專輯《[Kind
of
Love](../Page/Kind_of_Love.md "wikilink")》和第2張單曲〈[想擁抱你](../Page/想擁抱你_\(Mr.Children單曲\).md "wikilink")〉。〈想擁抱你〉之後被用作[日本電視劇](../Page/日本電視劇.md "wikilink")《天使之愛》（[ピュア](../Page/ピュア_\(テレビドラマ\).md "wikilink")）的插曲。不久他們舉辦一次名為「92-93
Kind of Love Tour」的巡迴演唱會，於1992年12月7日至1993年1月25日間舉行。

1993年，在巡迴演唱會完結後，他們投入了第3張專輯的工作。他們在該年首張單曲名為〈[Replay](../Page/Replay_\(單曲\).md "wikilink")〉，於7月1日發行，被用作[江崎固力果](../Page/江崎固力果.md "wikilink")「[Pocky](../Page/Pocky.md "wikilink")」的廣告歌。同年9月9日，第3張專輯《[Versus](../Page/Versus_\(Mr.Children專輯\).md "wikilink")》發行，但這張專輯未能令他們被世人所知。之後他們在同年9月23日至11月5日期間舉辦了一場新的巡迴演出「93
Versus Tour」，進行九場表演。不久後第4張單曲〈[CROSS
ROAD](../Page/CROSS_ROAD_\(Mr.Children單曲\).md "wikilink")〉發行，[A面曲被用作電視劇](../Page/A面與B面.md "wikilink")《[同窗會](../Page/同窗會（日劇）.md "wikilink")》的主題曲。雖然樂隊未能藉此竄紅，但〈CROSS
ROAD〉則變得越來越知名，最後在22個星期後達到百萬銷量，並成為1994年[Oricon公信榜年榜第](../Page/Oricon公信榜.md "wikilink")15位（但單曲於93年開始發售）\[5\]。主唱櫻井後來承認他並不喜歡到此為止的作品。
1994年6月1日，新單曲〈[innocent
world](../Page/innocent_world.md "wikilink")〉發行，A面曲被用作飲料[飛雪動奕的廣告歌](../Page/飛雪動奕.md "wikilink")。這首歌令Mr.Children變得更加出名。樂團憑〈innocent
world〉1,812,970張的銷量登上Oricon年榜第1位\[6\]。跟着他們開始投入第4張專輯《[Atomic
Heart](../Page/Atomic_Heart.md "wikilink")》的工作。這張專輯在同年9月1日開始發售，並成為當時樂團銷量最高的專輯\[7\]。由於上述單曲和專輯都取得了極大的成功，Mr.Children得以確立他們在樂壇的地位，在日本當時掀起「Misuchiru現象」（）\[8\]。

製作人兼經紀人小林武史亦替他們籌劃了兩次的巡迴演唱會。第一次「94 tour innocent
world」以其大熱單曲命名，於1994年9月18日至12月18日舉行。1994年11月10日，他們發行了第6張單曲〈[Tomorrow
never
knows](../Page/Tomorrow_never_knows_\(Mr.Children單曲\).md "wikilink")〉，A面曲是電視劇《[青春無悔](../Page/青春無悔.md "wikilink")》（若者のすべて）的主題曲。這張單曲是在巡迴演唱會期間所寫成的。〈Tomorrow
never knows〉其後於2006年在音樂節目[MUSIC
STATION被選為最喜愛的歌曲](../Page/MUSIC_STATION.md "wikilink")，至今仍是日本史上銷量第三的電視劇主題曲\[9\]。1994年12月12日，下一張單曲〈[everybody
goes -向沒有秩序的現代Drop
Kick-](../Page/everybody_goes_-向沒有秩序的現代Drop_Kick-.md "wikilink")〉發行，其A面曲曾為〈Tomorrow
never
knows〉[B面曲的候選](../Page/A面與B面.md "wikilink")。1994年年底，Mr.Children遂以〈innocent
world〉榮獲第36屆[日本唱片大獎](../Page/日本唱片大獎.md "wikilink")。\[10\]

### 1995年－1997年

1995年，第二場巡迴演唱會「95 Tour Atomic
Heart」開始，在1995年1月7日至2月20日期間舉行。Mr.Children亦開始參與慈善活動。他們跟[南方之星的](../Page/南方之星.md "wikilink")[桑田佳祐合作](../Page/桑田佳祐.md "wikilink")，組成「[桑田佳祐\&Mr.Children](../Page/桑田佳祐&Mr.Children.md "wikilink")」，並發行單曲〈[奇蹟的地球](../Page/奇蹟的地球.md "wikilink")〉，做為援助防治[愛滋病活動](../Page/愛滋病.md "wikilink")「Act
Against
AIDS」（AAA）的亞洲區主題曲。他們為宣傳單曲和活動，在1995年4月18日至5月14日舉行了一次長達一個月的巡迴演唱會「LIVE
UFO '95 ROCK OPERA "Acoustic Revolution with Orchestra"
奇蹟的地球」。演唱會中他們翻唱了不少英語歌曲，例如[滾石樂隊和](../Page/滾石樂隊.md "wikilink")[鮑布．迪倫的歌曲](../Page/鮑布．迪倫.md "wikilink")。\[11\]在巡迴演唱期間，他們亦拍攝了一部[紀錄片](../Page/紀錄片.md "wikilink")《【es】
Mr.Children in FILM》，電影於同年6月3日上映。在這之先，第8張單曲〈[【es】 ～Theme of
es～](../Page/【es】_～Theme_of_es～.md "wikilink")〉於5月10日發行，以宣傳電影。兩個月後，他們在7月16日至9月10日舉辦了其首次戶外巡迴演唱會「-Hounen
Mansaku- 夏祭1995 空\[ku:\]」。巡迴期間第9張單曲〈[See-Saw Game
～勇敢的戀曲～](../Page/See-Saw_Game_～勇敢的戀曲～.md "wikilink")〉於8月10日發行。

1996年2月6日，Mr.Children的第10張單曲〈[無名的詩](../Page/無名的詩.md "wikilink")〉開始發售，A面曲被用作電視劇《[天使之愛](../Page/天使之愛.md "wikilink")》的主題曲。〈無名的詩〉成為當時日本音樂史上首周銷量最高的單曲（120.8萬），紀錄多年來一直無人能破，到2011年才被[AKB48的](../Page/AKB48.md "wikilink")〈[Everyday，髮箍](../Page/Everyday，髮箍.md "wikilink")〉超越（133.4萬）。\[12\]另外，這首歌迄今仍是日本音樂史上銷量第八的電視劇主題曲\[13\]。然而這張單曲的成功亦是櫻井和壽的預料之外，因為當時他只有很少時間寫這首歌。兩個月後，第11張單曲在4月5日〈[花
-Mémento-Mori-](../Page/花_-Mémento-Mori-.md "wikilink")〉發行。接着還有6月24日第5張專輯《[深海](../Page/深海_\(專輯\).md "wikilink")》和8月8日第12張單曲〈[機關槍連續掃射
-Mr.Children
Bootleg-](../Page/機關槍連續掃射_-Mr.Children_Bootleg-.md "wikilink")〉發行。其後巡迴演唱會「regress
or progress
'96-'97」開始，由1996年8月24日開始，1997年2月16日結束。在這次巡迴演唱中，樂團曾於14個城市共舉辦了55場演唱會。

1997年2月5日，Mr.Children發行他們的第13張單曲〈[Everything (It's
you)](../Page/Everything_\(It's_you\).md "wikilink")〉，A面曲被用作電視劇《》的主題曲。一個月後，第6張專輯《[BOLERO](../Page/BOLERO_\(專輯\).md "wikilink")》亦於3月5日發行。其後開始有傳聞指樂團將會解散。

### 1998年－2000年

1998年2月11日，第14張單曲〈[向西向東](../Page/向西向東.md "wikilink")〉發行，A面曲被用作電視劇《的主題曲。這時樂團仍處於休息狀態，所以沒有進行現場表演宣傳單曲和參與[音樂影片的演出](../Page/音樂影片.md "wikilink")。在同年10月21日，樂團正式重聚，並發行第15張單曲〈[無盡的旅程](../Page/無盡的旅程.md "wikilink")〉，被用作電視劇《》的主題曲。這首歌一直是最受Mr.Children歌迷歡迎的歌曲之一，[Oricon指出其激勵性的歌詞是原因之一](../Page/Oricon.md "wikilink")。

1999年1月13日，Mr.Children發行其第16張單曲〈[向光映射的地方](../Page/向光映射的地方.md "wikilink")〉。同年2月3日發行第7張專輯《[DISCOVERY](../Page/DISCOVERY_\(Mr.Children專輯\).md "wikilink")》。
11天後，以這張專輯為名的巡迴演唱會「Mr.Children Concert Tour '99
DISCOVERY」，於1999年2月13日至7月11日期間舉行，在16個城市共辦了42場演唱會。在巡迴演唱期間，第17張單曲〈[I'LL
BE](../Page/I'LL_BE_\(Mr.Children單曲\).md "wikilink")〉發行。A面曲〈I'LL
BE〉曾收錄在專輯《DISCOVERY》中，原為〈[I'll
be](../Page/I'll_be.md "wikilink")〉，後將曲子改成輕快版本再次發行。這張單曲未能延續以往的成功，為樂團自〈[CROSS
ROAD](../Page/CROSS_ROAD_\(Mr.Children單曲\).md "wikilink")〉以來銷量最低的。除此之外，在巡迴演唱會時，他們亦製作了一張現場專輯，名為《[1/42](../Page/1/42.md "wikilink")》（意即42場演唱會的其中之一），於同年9月8日限量500,000張發行。專輯內大部分的音軌都來自6月26日在[真駒内屋内競技場的演出](../Page/真駒内屋内競技場.md "wikilink")，Bonus
Track〈[想擁抱你](../Page/想擁抱你_\(Mr.Children單曲\).md "wikilink")〉則是取自[沖繩縣](../Page/沖繩縣.md "wikilink")[宜野灣市海濱公園野外劇場的演出](../Page/宜野灣市.md "wikilink")。

踏入[千禧年](../Page/千禧年.md "wikilink")，第18張單曲〈[口笛](../Page/口笛.md "wikilink")〉於2000年1月13日發行。這張單曲的銷量相比起〈I'LL
BE〉高出很多，為724,070張。8月9日，他們發行了第19張單曲〈[NOT
FOUND](../Page/NOT_FOUND_\(單曲\).md "wikilink")〉，其中A面曲被用作電視劇《》的主題曲。一個月後，第9張原創專輯《[Q](../Page/Q_\(專輯\).md "wikilink")》。樂團特意為這張專輯的錄音工作前往[紐約](../Page/紐約.md "wikilink")，其中他們重新錄製了一些[獨立時期的歌曲](../Page/獨立音樂.md "wikilink")，製作人[小林武史亦有參與錄音的樂器演奏](../Page/小林武史.md "wikilink")。唱片封面是由Size,
Inc在美國[邦納維爾鹽窪地拍攝的](../Page/邦納維爾鹽窪地.md "wikilink")。\[14\]由於與當時正處於人氣巔峰的滨崎步的《Duty》同日發行，故未取得週排行冠軍，且專輯的銷量不太理想，為樂團自《[Atomic
Heart](../Page/Atomic_Heart.md "wikilink")》以來首張銷量未能超過百萬的專輯。在2000年10月14日至2001年2月23日期間，Mr.Children舉行巡迴演唱會「Mr.Children
Concert Tour Q 2000-2001」，於13個城市共有35場演唱會。

### 2001年－2003年

2001年，在巡迴演唱會完結後，Mr.Children發行了兩張[精選輯](../Page/精選輯.md "wikilink")，分別是《[Mr.Children
1992-1995](../Page/Mr.Children_1992-1995.md "wikilink")》和《[Mr.Children
1996-2000](../Page/Mr.Children_1996-2000.md "wikilink")》，同時於7月11日開始發售，合共售出約4,034,785張。
於兩張專輯發行後四天，「MR.CHILDREN CONCERT TOUR POPSAURUS
2001」巡迴演唱會開始，由同年7月14日舉行至9月24日，於10個城市共辦了15場演唱會。巡迴演唱會開始一個月後第20張單曲〈[溫柔的歌](../Page/溫柔的歌.md "wikilink")〉發行。這首歌亦是朝日飲料「」的廣告歌。巡迴演出結束兩個月後，第21張單曲〈[youthful
days](../Page/youthful_days.md "wikilink")〉亦開始發售。這首歌被用於電視劇《[西洋骨董洋菓子店](../Page/西洋骨董洋菓子店_\(電視劇\).md "wikilink")》的主題曲。這張單曲是樂團這年銷量中最高的歌。B面曲〈Drawing〉原先並無任何[商業搭配](../Page/商業搭配.md "wikilink")，但發行兩年多後的2003年，又成為日本電視台的電視劇《》主題曲。

2002年1月1日，Mr.Children發行其第22張單曲〈[喜歡你](../Page/喜歡你_\(Mr.Children單曲\).md "wikilink")〉，是《西洋骨董洋菓子店》的片尾插曲。四個月後，第10張原創專輯《[IT'S
A WONDERFUL
WORLD](../Page/IT'S_A_WONDERFUL_WORLD.md "wikilink")》於他們出道10週年當日——5月10日正式發行。這張專輯並不是早就計劃於10週年當日發行的，只是櫻井要求在春天發售專輯後，在想如何宣傳專輯時才選擇這個特別日子的。\[15\]其後，「TOUR
2002 DEAR WONDERFUL WORLD」巡迴演唱會於7月17日開始。\[16\]〈[I'LL
BE](../Page/I'LL_BE_\(Mr.Children單曲\).md "wikilink")〉的專輯版本被用作[2002年世界盃足球賽的主題曲之一](../Page/2002年世界盃足球賽.md "wikilink")。\[17\]同年5月24日，他們首次出席「[MTV日本音樂錄影帶大獎](../Page/MTV日本音樂錄影帶大獎.md "wikilink")」，並以〈喜歡你〉獲得「年度最佳音樂錄影帶獎」。\[18\]在專輯發行的兩個月後，第23張單曲〈[Any](../Page/Any.md "wikilink")〉於7月11日發行。由於主唱櫻井因[小腦梗塞入院](../Page/中風.md "wikilink")，樂團沒能好好宣傳這張單曲，進行中的「DEAR
WONDERFUL
WORLD」亦被迫取消。\[19\]\[20\]櫻井在休養中受到啟發，寫下了〈[-{HERO}-](../Page/HERO_\(Mr.Children單曲\).md "wikilink")〉。這張第24張單曲於同年12月11日發行。\[21\]單曲初版附有DVD一張。同年11月15日，官方網站宣佈將回歸舞台，並舉辦僅有「一晚限定」的演唱會。\[22\]12月21日，該演唱會「TOUR
2002 DEAR WONDERFUL WORLD IT'S A WONDERFUL WORLD ON DEC
21」舉行，其後亦發行了相關DVD影像。

樂團於2003年相當沉靜。在6月，櫻井協助成立了[非營利機構](../Page/非營利機構.md "wikilink")「」，機構主要工作是促進環境保育。\[23\]最初著名[作曲家](../Page/作曲家.md "wikilink")[坂本龍一想要興建一座風力發電廠](../Page/坂本龍一.md "wikilink")。在櫻井和小林武史的協助下，他們的目標變成投資環境保護的計劃，例如[可再生能源等](../Page/可再生能源.md "wikilink")\[24\]；其後2007年，ap
bank亦參與了其他的社會議題，如幫助[2007年新潟縣中越沖地震的災民等](../Page/2007年新潟縣中越沖地震.md "wikilink")。\[25\]接近年尾，Mr.Children發行第25張單曲〈[掌/Kurumi](../Page/掌/Kurumi.md "wikilink")〉，為他們的首張雙A面單曲。這張單曲的銷量是在2001年的〈[youthful
days](../Page/youthful_days.md "wikilink")〉之後最高的。

### 2004年－2006年

2004年，櫻井開始了一個從ap bank衍生出來的計劃，名為「」。Bank
Band發行了首張專輯《》，專輯中[翻唱了Mr](../Page/翻唱.md "wikilink").Children的兩首歌——〈[-{HERO}-](../Page/HERO_\(Mr.Children單曲\).md "wikilink")〉和〈[溫柔的歌](../Page/溫柔的歌.md "wikilink")〉。\[26\]4月4日，樂團第11張專輯《[至福之音](../Page/至福之音.md "wikilink")》發行，其中歌曲〈為了誰〉（）被用作[日清](../Page/日清.md "wikilink")[合味道杯麵的廣告歌](../Page/合味道.md "wikilink")。據櫻井所講，〈為了誰〉：

下一個月，他們發行了第26張單曲〈[Sign](../Page/Sign_\(Mr.Children單曲\).md "wikilink")〉，A面曲被用作電視劇《[Orange
Days](../Page/Orange_Days.md "wikilink")》的主題曲。Mr.Children憑此單曲，繼10年前的〈[innocent
world](../Page/innocent_world.md "wikilink")〉第36屆[日本唱片大賞後](../Page/日本唱片大賞.md "wikilink")，於第46屆再次獲得這項大獎。\[27\]

[Apfes05.jpg](https://zh.wikipedia.org/wiki/File:Apfes05.jpg "fig:Apfes05.jpg")
2005年，Mr.Children的大部份時間都投入於新專輯的製作。6月29日，第27張單曲〈[四次元 Four
Dimensions](../Page/四次元_Four_Dimensions.md "wikilink")〉終於發行。單曲首周銷量高達569,000張，最終有925,632張的銷量。這張四A面單曲中的四首歌都有[商業搭配](../Page/商業搭配.md "wikilink")：〈未來〉是[寶礦力水特的廣告歌](../Page/寶礦力水特.md "wikilink")，〈and
I love you〉是日清合味道杯麵的廣告歌，〈Running
High〉（）是電影《》的主題曲，〈Yoidon〉（）是兒童教育節目《》和《》的主題曲。雖然這張作品是以單曲形式發售，但因為收錄歌曲太多，被[日本唱片協會界定分類為](../Page/日本唱片協會.md "wikilink")[專輯](../Page/專輯.md "wikilink")。一個月後，樂團於7月16日至18日參與了櫻井舉辦的演唱會「ap
bank fes' 05」、7月23日的「SETSTOCK '05」和7月30日的「HIGHER GROUND
2005」。三個月後，第12張專輯《[I ♥
U](../Page/I_♥_U.md "wikilink")》於9月21日發行。再過兩個月，組合舉辦「MR.CHILDREN DOME
TOUR 2005 "I ♥
U"」巡迴演唱會，在11月12日開始，12月27日結束。這次巡迴演唱中首次於五大[巨蛋](../Page/巨蛋#日本巨蛋.md "wikilink")（[東京巨蛋](../Page/東京巨蛋.md "wikilink")、[大阪巨蛋](../Page/大阪巨蛋.md "wikilink")、[札幌巨蛋](../Page/札幌巨蛋.md "wikilink")、[名古屋巨蛋和](../Page/名古屋巨蛋.md "wikilink")[福岡巨蛋](../Page/福岡巨蛋.md "wikilink")）舉辦演唱會。\[28\]這次巨蛋巡迴演唱會於東京巨蛋完結，觀眾人數達45,000人；整個巡迴演出則有390,000人。\[29\]至於Mr.Children作品的總銷量於年尾突破4,500萬張。

2006年7月5日，Mr.Children推出他們的第28張單曲〈[箒星](../Page/箒星.md "wikilink")〉，被用作[豐田汽車的廣告歌和](../Page/豐田汽車.md "wikilink")[日本電視台](../Page/日本電視台.md "wikilink")（NTV）的[2006年世界盃足球賽主題曲](../Page/2006年世界盃足球賽.md "wikilink")。樂團由於參與了「ap
bank fes'
06」，只能舉辦三場現場演出來宣傳單曲，而且沒能在雜誌或電台作宣傳。不過，[商業搭配十分成功](../Page/商業搭配.md "wikilink")，在[Oricon的調查中](../Page/Oricon.md "wikilink")，〈箒星〉是在「6月份電視廣告歌曲
好感度排行榜」得第三位\[30\]，以及「夏天中最喜愛的非夏日歌曲」中得第一位。\[31\]單曲發行十天後，Mr.Children參與「ap
bank fes' 06」全部三天的演出，演唱了〈-{HERO}-〉、〈Strange
Chameleon〉（翻唱）、〈無盡的旅程〉和〈箒星〉等歌曲。\[32\]一個月後，樂團又以特別嘉賓的身份出席「MUJINTOU
Fes.2006」演唱會，並演唱了〈未來〉、〈innocent
world〉、〈Sign〉、〈[綻放](../Page/箒星.md "wikilink")〉（）
、〈worlds end〉和〈箒星〉。不久後Mr.Children宣佈跟日本搖滾樂隊[the
pillows合辦巡迴演唱會](../Page/the_pillows.md "wikilink")，名為「Mr.Children
& the pillows new big bang tour 〜This is Hybrid
Innocent〜」。這次演唱會由同年9月26日舉行至10月11日。11月15日，第29張單曲〈[印記](../Page/印記_\(Mr.Children單曲\).md "wikilink")〉發行，其中歌曲〈印記〉被用作[日本電視台電視劇](../Page/日本電視台.md "wikilink")《[14歲媽媽](../Page/14歲媽媽.md "wikilink")》的主題曲。\[33\]櫻井於2月開始寫這首歌，3月寫完，並在9月拍攝[音樂影片](../Page/音樂影片.md "wikilink")。單曲的B面〈Kurumi
- for the Film -
幸福的餐桌〉則是2003歌曲〈[Kurumi](../Page/掌/Kurumi.md "wikilink")〉的重製版本，被用作電影《》的主題曲。\[34\]

### 2007年

2007年1月24日，Mr.Children發行第30張單曲〈[Fake](../Page/Fake_\(Mr.Children單曲\).md "wikilink")〉，限量發售，其中A面曲被用作[電影](../Page/電影.md "wikilink")《[多羅羅](../Page/多羅羅.md "wikilink")》的主題曲。這張單曲成為Mr.Children連續第26張奪得Oricon公信榜周榜首位的單曲。\[35\]。3月14日，第13張原創專輯《[HOME](../Page/HOME_\(Mr.Children專輯\).md "wikilink")》發行，為組合自1994年《[Atomic
Heart](../Page/Atomic_Heart.md "wikilink")》以來，再次以原創專輯連續兩周登上Oricon公信榜首位。\[36\]《HOME》亦是2007年首張銷量破百萬的專輯。\[37\]\[38\]由於樂團中對這張專輯的製作方向有所分岐，導致錄音工作曾一度停頓。製作人[小林武史建議要做一張關懷世界的專輯](../Page/小林武史.md "wikilink")。
專輯內容有不少個人感想：〈更多〉（）有關[紐約的](../Page/紐約.md "wikilink")[九一一襲擊事件](../Page/九一一襲擊事件.md "wikilink")，〈不太記得了啊〉（）的靈感則來自櫻井生病的父親。專輯名稱原本還有「HOME
MADE」、「HOME
GROUND（）」等候選，讓人有親手製作之感，但最後為了不要限制在某個意念內，將專輯取名為「HOME」。\[39\]Mr.Children為宣傳專輯，舉辦了兩次巡迴演唱會。第一場是2007年5月4日至6月23日的「Mr.Children
"HOME" TOUR
2007」。在巡迴期間，他們又發行了一張[精選輯](../Page/精選輯.md "wikilink")《[B-SIDE](../Page/B-SIDE.md "wikilink")》，於出道15週年5月10日正式開始發售。這是一張只收錄了[A面與B面B面曲的專輯](../Page/A面與B面.md "wikilink")，源自櫻井進行《HOME》時想到的。他重新把這些放在單曲上的B面歌聽了一遍後，發覺：
 而根據TOY'S
FACTORY網站的介紹，《B-SIDE》中將不同時期的B面曲收起來，恰能讓成員對音樂的想法和感情完整地展現，故他們就出版了這張專輯。\[40\]5月5日，在「Mr.Children
"HOME" TOUR
2007」第二場演唱會後，鼓手[鈴木英哉因誤觸抽氣扇](../Page/鈴木英哉.md "wikilink")，造成[食指受傷](../Page/食指.md "wikilink")，須縫四針，其後兩場於[札幌舉行的演唱會亦因此延遲](../Page/札幌.md "wikilink")。\[41\]\[42\]巡迴演唱會相當盛大，例如6月7日於横浜的演唱會上就有15,000位觀眾\[43\]，證明「Misuchiru現象」仍未消退。\[44\]第二場宣傳《HOME》的巡迴演唱會是「Mr.Children
"HOME" TOUR 2007 -in the
field-」，由8月4日舉行至9月30日。\[45\]該次的巡迴演唱也十分成功，於9個城市14場演唱會中，共有55萬名觀眾參加，為2007年單一藝人的巡迴演唱會裡人數最多的。\[46\]另外，《HOME》裡的主要歌曲〈色彩〉（）更被用作[Olympus](../Page/Olympus.md "wikilink")
E-410的廣告歌。\[47\]

7月10日，Mr.Children在網站宣佈將會出新單曲〈[啟程之歌](../Page/啟程之歌.md "wikilink")〉，但並未宣佈發售日期，直到一個月後才宣佈在10月30日發行。A面曲〈啟程之歌〉被用作電影《[戀空](../Page/戀空.md "wikilink")》的主題曲。這張單曲令樂團連續奪得[Oricon公信榜周榜首位的單曲數目增至](../Page/Oricon公信榜.md "wikilink")27張。\[48\]之後，樂團計劃在7月14日至16日於「ap
bank fes'
07」演出，但由於[颱風萬宜侵襲](../Page/颱風萬宜_\(2007年\).md "wikilink")，只有最後一天能夠舉行。\[49\]11月14日，《Mr.Children
"HOME" TOUR
2007》[DVD發行](../Page/DVD.md "wikilink")。12月18日，[Oricon宣佈專輯](../Page/Oricon.md "wikilink")《[HOME](../Page/HOME_\(Mr.Children專輯\).md "wikilink")》以1,181,241張的銷量，壓倒[倖田來未](../Page/倖田來未.md "wikilink")《[Black
Cherry](../Page/Black_Cherry_\(倖田來未專輯\).md "wikilink")》的1,022,448張銷量，獲得年度專輯榜冠軍。\[50\]

### 2008年

年初，櫻井個人籌辦的計劃「Bank Band」於1月16日發行了第2張專輯《》和DVD《ap bank fes
'07》。一個月後，Mr.Children官方發表新歌〈少年〉，被用作電視劇《[野球少年](../Page/野球少年.md "wikilink")》的主題曲；其後亦發表了兩張新單曲的消息。首先是7月30日發行的〈[GIFT](../Page/GIFT_\(Mr.Children單曲\).md "wikilink")〉，其中A面曲〈GIFT〉被用作[NHK的](../Page/NHK.md "wikilink")[北京奧運會和](../Page/北京奧運會.md "wikilink")[北京殘奧會的主題曲](../Page/北京殘奧會.md "wikilink")。\[51\]對於這首歌，櫻井有這樣的解釋：

接着ap bank宣佈Mr.Children將於「ap bank fes'
08」全部三天都參與演出。\[52\]2008年9月3日，樂團推出第33張單曲的〈[HANABI](../Page/HANABI_\(Mr.Children單曲\).md "wikilink")〉，被用作[富士電視台電視劇](../Page/富士電視台.md "wikilink")《[空中急診英雄](../Page/空中急診英雄.md "wikilink")》。這張單曲於[Oricon公信榜連續兩周奪冠](../Page/Oricon公信榜.md "wikilink")，為連續第29張奪得周榜首位的單曲。其後下一張發行的單曲〈[花的香氣](../Page/花的香氣.md "wikilink")〉則僅開放在網路配信下載，電話鈴聲片段（）於10月1日開放下載，完整電話鈴聲（）則於11月1日開放下載。\[53\]12月10日，Mr.Children正式發行[錄音室專輯](../Page/錄音室專輯.md "wikilink")《[SUPERMARKET
FANTASY](../Page/SUPERMARKET_FANTASY.md "wikilink")》。專輯首周銷量為708,000張，取得周榜首位，\[54\]也是年度專輯榜亞軍。

### 2009年－2011年

2009年2月14日至8月6日，展開「Tour 2009 ～終末のコンフィデンスソングス～」巡迴演唱，後來並以武道館該場收錄成為影像。

10月20日，Mr.Children發行第二首音樂網路配信下載的限定單曲〈[fanfare](../Page/fanfare.md "wikilink")〉；11月16日開放鈴聲片段下載，12月2日開放鈴聲全曲下載。〈fanfare〉是電影《[ONE
PIECE FILM
強者天下](../Page/ONE_PIECE_FILM_強者天下.md "wikilink")》的主題曲，為Mr.Children第二次提供[動畫歌曲](../Page/動畫歌曲.md "wikilink")。\[55\]單曲也於[RIAJ付費音樂下載榜獲得首位](../Page/RIAJ付費音樂下載榜.md "wikilink")。\[56\]

11月11日，發行《[Mr.Children Tour 2009
～終末のコンフィデンスソングス～](../Page/Mr.Children_Tour_2009_～終末のコンフィデンスソングス～.md "wikilink")》DVD。

11月28日至12月27日，Mr.Children決定以五大巨蛋為名，舉辦「DOME TOUR 2009 SUPERMARKET
FANTASY」。

2010年1月，《[空中急診英雄](../Page/空中急診英雄.md "wikilink")》第二季上映，主題曲為〈[HANABI](../Page/HANABI.md "wikilink")〉。

2010年5月10日，Mr.Children發行《[Mr.Children DOME TOUR 2009〜SUPERMARKET
FANTASY〜](../Page/Mr.Children_DOME_TOUR_2009〜SUPERMARKET_FANTASY〜.md "wikilink")》巡迴演唱會的DVD，於官方公佈的發行日期前一天已累積銷量達49,000張，並以這個銷量獲得周排行榜的首位。\[57\]另外，樂團亦憑此平了[嵐和](../Page/嵐.md "wikilink")[KAT-TUN連續七張DVD獲得周榜首位的紀錄](../Page/KAT-TUN.md "wikilink")。\[58\]

9月4日，樂團第二部音樂[紀錄片](../Page/紀錄片.md "wikilink")《[Mr.Children / Split The
Difference](../Page/Mr.Children_/_Split_The_Difference.md "wikilink")》上映，並在11月10日發行DVD+CD，收錄電影以及一些歌曲。\[59\]DVD於初登場獲得周排行榜首位，因而以超過了上述嵐和KAT-TUN的紀錄。\[60\]

12月1日，第16張[錄音室專輯](../Page/錄音室專輯.md "wikilink")《[SENSE](../Page/SENSE_\(Mr.Children專輯\).md "wikilink")》正式發行，〈fanfare〉亦有收錄在內。所有關於專輯的詳情直到11月29日才公佈。\[61\]之後，展開為期不到三個月的「Mr.Children
Tour 2011 "SENSE"」巡迴演唱。

2011年2月19日開始「Mr.Children Tour 2011
"SENSE"」，期間發生了舉世震驚的311東日本大地震。4月4日，Mr.Children為[東日本大震災賑災](../Page/東日本大震災.md "wikilink")，發行第3首音樂下載限定單曲〈[數數歌](../Page/數數歌_\(單曲\).md "wikilink")〉。\[62\]
該曲壓過當紅[AKB48同樣為東日本大震災發行的](../Page/AKB48.md "wikilink")〈[為了誰 -What can I
do for
someone?-](../Page/為了誰_-What_can_I_do_for_someone?-.md "wikilink")〉，於[RIAJ付費音樂下載榜獲得首位](../Page/RIAJ付費音樂下載榜.md "wikilink")。\[63\]
在該次巡演最終的埼玉場裡，Mr.Children還公開表演〈數數歌〉，並收錄影像於11月23日發行的《[Mr.Children TOUR
2011 "SENSE"](../Page/Mr.Children_TOUR_2011_"SENSE".md "wikilink")》DVD &
Blu-ray之中。

8月9日起，以「Mr.Children STADIUM TOUR 2011 SENSE -in the
field-」為名的巡演從廣島開始，最終場是在9月25日的震災區宮城。為了幫災民加油打氣，Mr.Children特別在每一場舉行「元氣接力」方式錄影下來，最後則以Video
Letter在宮城播出。

### 2012年－2014年

2012年4月18日，Mr.Children發行第34張實體單曲CD〈[祈禱 ～眼淚的痕跡/End of the
day/pieces](../Page/祈禱_～眼淚的痕跡/End_of_the_day/pieces.md "wikilink")〉，距離上一張CD單曲〈[HANABI](../Page/HANABI_\(Mr.Children單曲\).md "wikilink")〉已有約3年零7個月。由發售前的3月17日開始，〈祈禱
～眼淚的痕跡〉的完整電話鈴聲亦於網上公開下載，樂團藉此在RIAJ付費音樂下載榜上連續4周獲得第一位\[64\]，平了[GReeeeN](../Page/GReeeeN.md "wikilink")〈〉的紀錄。跟單曲同日開始發售《[Mr.Children
STADIUM TOUR 2011 SENSE -in the
field-](../Page/Mr.Children_STADIUM_TOUR_2011_SENSE_-in_the_field-.md "wikilink")》的[DVD和](../Page/DVD.md "wikilink")[藍光光碟](../Page/藍光光碟.md "wikilink")（BD）分別登上DVD總合排行榜和BD總合排行榜的首位，再加上單曲亦取得周榜首位，令Mr.Children為Oricon榜首次能同時於三個排行榜取得首位的首組藝人。\[65\]
5月10日為Mr.Children出道20週年紀念日，當天發行兩張[精選輯](../Page/精選輯.md "wikilink")《[Mr.Children
2001-2005＜micro＞](../Page/Mr.Children_2001-2005＜micro＞.md "wikilink")》和《[Mr.Children
2005-2010＜macro＞](../Page/Mr.Children_2005-2010＜macro＞.md "wikilink")》，同時亦決定舉行全國巡迴演唱會「Mr.Children
Tour POPSAURUS
2012」。\[66\]在[大阪巨蛋演出時](../Page/大阪巨蛋.md "wikilink")，櫻井向歌迷告示為何選擇用「micro」和「macro」兩種的意涵，希望聽眾聽了他們的歌以後，能夠進一步產生嶄新的生命。8月29日，新單曲〈[hypnosis](../Page/hypnosis.md "wikilink")〉以《[特官
-特別國稅徵收官-](../Page/特官_-特別國稅徵收官-.md "wikilink")》的主題曲形式提供網路配信下載。11月28日，正式發行專輯《[［(an
imitation) blood
orange］](../Page/［\(an_imitation\)_blood_orange］.md "wikilink")》，收入有四首音樂影帶，同時宣布從12月15日起舉辦新專輯巡演40場（直到2013年6月9日，包括在沖繩有兩場加演的場次）；12月19日發行LIVE
DVD & Blu-ray《[Mr.Children TOUR POPSAURUS
2012](../Page/Mr.Children_TOUR_POPSAURUS_2012.md "wikilink")》。
2013年巡迴演出期間，Mr.Children於5月29日推出新曲〈[REM](../Page/REM.md "wikilink")〉，仍以配信限定方式發賣。8月10日、11日兩天參加在大阪與東京兩地舉辦的SUMMER
SONIC
2013演唱會，其中11日當天因天候打雷關係發生意外插曲。當主唱櫻井和壽唱到〈[Fake](../Page/Fake_\(Mr.Children單曲\).md "wikilink")〉時音響忽然無法出聲，但現場觀眾仍熱情唱和未止；他當場表示自己最喜歡有這種突發狀況出現，證明完全沒有修飾的演唱功力。\[67\]
12月3日，主唱櫻井抽空參加[小田和正的歲末慣例音樂節目](../Page/小田和正.md "wikilink")「耶誕的約會2013」(クリスマスの約束2013），並首次揭露合寫、合唱的「全景的街」（パノラマの街），\[68\]
成為日後〈[街的風景](../Page/街的風景.md "wikilink")〉（街の風景）一曲的原型（收在專輯《REFLECTION
{Naked} 》中）。
2014年5月24日，Mr.Children的第6首音樂下載限定單曲〈[放逐](../Page/放逐.md "wikilink")〉
發行，同時也是「劇團一人」初次擔綱執導的電影《青天の霹靂》主題曲。為了繼續維持能量，9月17日起，Mr.Children分別在東京、福岡、大阪、愛知、北海道的Zepp劇場舉辦小型演唱；11月19日，實體單曲CD〈[腳步聲
～Be
Strong](../Page/腳步聲_～Be_Strong.md "wikilink")〉正式發行。這段期間，Mr.Children也醞釀與經紀公司[烏龍舎分道揚鑣](../Page/烏龍舎.md "wikilink")；Zepp劇場演唱中負責鍵盤手與合音工作，則由原製作人[小林武史改為過去曾經合作過的](../Page/小林武史.md "wikilink")[Sunny負責](../Page/Sunny.md "wikilink")。\[69\]

### 2015年－2016年

甫過2015年，Mr.Children大致已完成他們的新專輯錄音製作。接下來，走出獨立製作和經紀活動的Mr.Children，幾乎是渾身解數地進行音樂新作品的宣傳，等於樹立了音樂人如何來試探市場可能性的模式。首先，以限定會員的21週年祭Zepp演唱會、最後定名為「Mr.Children
REFLECTION」電影（上映時間：109分），在2月7日至27日期間限定公開放映。\[70\]
緊接著從3月14日群馬起，為期近三個月、共20場的「Mr.Children
TOUR 2015
REFLECTION」正式開始，最終場6月4日選在埼玉縣超級競技場（[さいたまスーパーアリーナ](../Page/さいたまスーパーアリーナ.md "wikilink")）。參加巡迴演出的歌迷可說是在現場聆聽未曾接觸過新曲，提供了許多新鮮感。再者，在最終場當天還有另外兩項創舉：一是選擇以實況在戲院同步播放，另一是發行新專輯《[REFLECTION](../Page/REFLECTION.md "wikilink")》（有{Naked}和{Drip}
兩種版本）。第四，從5月22日至12月下旬，以Mr.Children寫真為主題的「薮田修身写真展 BLACK BOX －unpainted
face of Mr.Children－」先後也在東京、福岡、名古屋、札幌、仙台、廣島等地展出。\[71\]
第五，6月15日和20日，NHK電視台在「SONGS」節目中推出特別專集，以跟拍的方式呈現將近一年來Mr.Children製作音樂的歷程，同時現場演出新曲。第六是7月11日，收錄於《REFLECTION》之中的〈[Starting
Over](../Page/Starting_Over.md "wikilink")〉
成為電影《[怪物的孩子](../Page/怪物的孩子.md "wikilink")》（バケモノの子）主題曲。最後，正式為新專輯宣傳登場的「Mr.Children
Stadium Tour 2015
未完」，則從7月18日的福岡巨蛋開始，共進行16場演出。總計2015年整年度的兩次巡迴演唱，一共動員有100萬人次，後來分別以Live
DVD & Blu-ray《[Mr.Children TOUR 2015
REFLECTION](../Page/Mr.Children_TOUR_2015_REFLECTION.md "wikilink")》、《[Mr.Children
Stadium Tour 2015
未完](../Page/Mr.Children_Stadium_Tour_2015_未完.md "wikilink")》兩種發行出版
。
值得注意的是，上述兩種演唱會的影像出版均以故事性來呈現，傳達一種「未完待續」的感覺。2016年3月16日發行的《Mr.Children
Stadium Tour 2015 未完》，最後影像即以「Mr.Children 2 Man LIVE」（Mr.Children
2マンLIVE）演唱新曲〈[忙碌的我們](../Page/忙碌的我們.md "wikilink")〉（忙しい僕ら）片段來結束。這種體貼歌迷、率先讓他們聆聽新曲的模式，後來也持續重複在2016年4月15日起舉辦的「Mr.Children
Hall Tour 2016
虹」巡演之中。以「虹」為主題的巡迴演唱是從4月15日山梨縣開始，最後在2016年11月21日於神奈川的鎌倉藝術館畫下句點，專門提供偏遠地區會員、小場地為主的活動。其中，6月4日選在東京的日比谷野外大音樂堂，更是Mr.Children剛出道時公眾表演的場地，極富象徵意義。據稱現場僅有3,114人入內，但由於戶外場地緣故，卻吸引了更多數目的歌迷前來聆聽「音漏」。\[72\]
在各地演唱同時，Mr.Children深感唱片市場的日益蕭條景況，主唱櫻井和壽遂發起「I ❤ CD
shops\!」活動，親自到實體唱片行簽名，以實際行動來支持音樂。\[73\]
10月3日，NHK晨間劇《[別嬪小姐](../Page/別嬪小姐.md "wikilink")》（べっぴんさん）宣布選用Mr.Children的〈[光之畫室](../Page/光之畫室.md "wikilink")〉（ヒカリノアトリエ）為主題曲。

### 2017年

為了慶祝出道25週年，Mr.Children展開一連串的相關活動。

1月1日，即將發行的新單曲〈光之畫室〉音樂影帶以短版形式在[You
Tube官網上解禁](../Page/You_Tube.md "wikilink")。

1月10日，宣布於日本的「五大巨蛋」——[東京巨蛋](../Page/東京巨蛋.md "wikilink")、[札幌巨蛋](../Page/札幌巨蛋.md "wikilink")、[名古屋巨蛋](../Page/名古屋巨蛋.md "wikilink")、[大阪巨蛋](../Page/大阪巨蛋.md "wikilink")、[福岡巨蛋及大型競技場](../Page/福岡巨蛋.md "wikilink")（新橫濱[日產競技場](../Page/日產競技場.md "wikilink")、大阪[長居競技場](../Page/長居競技場.md "wikilink")、廣島[廣島廣域公園陸上競技場](../Page/廣島廣域公園陸上競技場.md "wikilink")、熊本[熊本県民総合運動公園陸上競技場](../Page/熊本県民総合運動公園陸上競技場.md "wikilink")）舉行出道25週年紀念巡迴演唱會。

1月11日，正式發行第36張實體單曲〈[光之畫室](../Page/光之畫室.md "wikilink")〉，並於20日開放網路配信下載。

3月4日起，Mr.Children開始為期不到兩個月的「Mr.Children Hall Tour 2017
光之畫室」巡迴公演，仍以偏鄉小場地為主。但是，3月16日在愛知的名古屋國際會議場世紀廳巡迴演出時，主唱櫻井和壽卻因感冒而無法發聲，被迫於第10首歌曲〈[口笛](../Page/口笛.md "wikilink")〉唱完後而結束演出，連同兩天後18日在三重的巡演也宣告中止。\[74\]
據親臨現場的觀眾事後描述：Mr.Children當場向大家表示歉意，而吉他手田原健一更說「如果這樣持續下去唱歌的話，櫻井將無法再發聲唱歌；如果無法唱歌的話，Mr.Children將必須走上結束一途。我們一定會回到這裡來，所以今天請讓櫻井休息吧。」\[75\]
為了實踐對歌迷的承諾，Mr.Children選在25年前出道當日——5月10日舉辦「振替公演」，其中還特別演唱了首張單曲〈[有你的夏天](../Page/有你的夏天.md "wikilink")〉，\[76\]
令現場3000多名歌迷充滿感動。馬不停蹄地Mr.Children並沒有因為失聲事件而沮喪。在「Mr.Children Hall Tour
2017 光之畫室」巡演期間，他們接連參加「ONE OK ROCK 2017 "Ambitions" JAPAN TOUR」和SUGA
SHIKAO 20th ANNIVERSARY「スガフェス！
～20年に一度のミラクルフェス～」，擔任演唱會嘉賓，試圖拉近跟年輕世代樂迷的距離和尋求新的音樂取向。

4月1日，Mr.Children正式公佈其25週年紀念巡迴演唱會的名稱：「Mr.Children DOME & STADIUM TOUR
2017 Thanksgiving 25」。

5月10日，舉辦「振替公演」同時，他們還推出25週年精選輯，並限定由網路配信下載，且為期一年。截至演唱會即將展開之前，兩張精選《[Mr.Children
1992-2002 Thanksgiving
25](../Page/Mr.Children_1992-2002_Thanksgiving_25.md "wikilink")》、《[Mr.Children
2003-2015 Thanksgiving
25](../Page/Mr.Children_2003-2015_Thanksgiving_25.md "wikilink")》依然蟬聯Oricon數位專輯榜的冠軍長達四週之久。\[77\]

6月10日，Mr.Children正式開始為期三個月的25週年紀念巡迴演唱會「Mr.Children DOME & STADIUM TOUR
2017 Thanksgiving
25」。演唱會中更獻唱當時的未發行歌曲〈\[<https://ja.wikipedia.org/wiki/Himawari_(Mr.Children%E3%81%AE%E6%9B%B2>)
himawari\]〉。\[78\]

7月26日，第37張單曲〈\[<https://ja.wikipedia.org/wiki/Himawari_(Mr.Children%E3%81%AE%E6%9B%B2>)
himawari\]〉正式發行，同時也是7月28日上映的電影《[我想吃掉你的胰臟](../Page/我想吃掉你的胰臟.md "wikilink")》（君の膵臓をたべたい）之主題曲。\[79\]
該支單曲首週銷售12.1萬張，獲得冠軍。\[80\]

12月20日，發行《[Mr.Children在光之畫室內描繪彩虹](../Page/Mr.Children在光之畫室內描繪彩虹.md "wikilink")》（Mr.Children、ヒカリノアトリエで虹の絵を描く）的DVD與BD，收錄2016年1月至2017年5月間的巡迴演唱及工作影像，包括初次映像化及首次公開披露的兩首歌曲〈[童話](../Page/童話.md "wikilink")〉（お伽話）及〈[內心](../Page/內心.md "wikilink")〉（こころ）。此外，加上繼《[1/42](../Page/1/42.md "wikilink")》之後推出的Live
CD和〈光之畫室〉完整版MV等內容。

### 2018年

1月1日，在新的一年開始的第一天，Mr.Children宣佈將參加「エレファントカシマシ 30th ANNIVERSARY TOUR “THE
FIGHTING MAN” SPECIAL
ド・ド・ドーンと集結！！～夢の競演～」。這場演唱會受到相當大地注目；當天另外一團參加演出的是Spitz。兩團都已出道30年。

1月18日，新單曲〈[here comes my
love](../Page/here_comes_my_love_\(Mr.Children單曲\).md "wikilink")〉公佈，並在隔日在各大音樂網站提供付費下載，同時也是富士電視台週四晚間連續劇《[鄰家月更圓](../Page/鄰家月更圓.md "wikilink")》（隣の家族は青く見える）主題曲。

2月9日，〈here comes my love〉的short film在官網公開。

3月2日，[椎名林檎新專輯](../Page/椎名林檎.md "wikilink")《亞當與夏娃的蘋果》（[アダムとイヴの林檎](../Page/アダムとイヴの林檎.md "wikilink")）發行，其中〈theウラシマ'S
／ 正しい街〉這首歌的鼓手由鈴木英哉擔任。

3月21日，LIVE DVD / Blu-ray《[Mr.Children DOME & STADIUM TOUR 2017
Thanksgiving
25](../Page/Mr.Children_DOME_&_STADIUM_TOUR_2017_Thanksgiving_25.md "wikilink")》開始販售。

4月10日，《[空中急診英雄](../Page/空中急診英雄.md "wikilink")》電影版的主題曲仍決定為〈[HANABI](../Page/HANABI.md "wikilink")〉。

4月13日，參加「ap bank fes 18」的演出。

5月18日，截至今年之前所有發行的單曲及專輯，開始在日本地區線上付費下載收聽。

6月29日，宣告新曲〈SINGLES〉成為7月19日朝日電視台的週四晚間連續劇《[企業併購王禿鷹](../Page/企業併購王禿鷹.md "wikilink")》（[ハゲタカ](../Page/ハゲタカ_\(2018年のテレビドラマ\).md "wikilink")）之主題曲。

8月2日，發佈了全新巡迴「Mr.Children Tour 2018-19」即將展開，以及新專輯及全曲詩集《[Your
Song](../Page/Your_Song.md "wikilink")》的發售消息。全曲詩集將分為一般版及Fan
Club限定的愛藏版發行，版稅收入捐給聽力需要幫助的相關單位。同時官方網站也宣佈首次舉行海外公演的消息，時間地點為2019年2月2日的台灣。台灣歌迷在台灣時間2018年8月1日晚間11點後看到此消息後，全都興奮到無法睡覺，熱烈地在社群網站討論分享。

8月18日，本次巡迴實施定價交易的規則，也是Mr.Children首次在巡迴中由官方來主導票券轉讓。

9月12日，宣佈在9月26日在東京舉辦一日會員限定的演唱會「FATHER\&MOTHER Special Prelive 2018.9.26
TOKYO DOME CITY HALL」。

9月17日，Mr.Children的官網出現了一串數字「10691059」，並且開始倒數新專輯，引發歌迷揣測。

9月20日，宣佈本次巡迴的名稱為「Mr.Children Tour 2018-19
重力と呼吸」。同時正式揭曉新專輯名稱：《[重力與呼吸](../Page/重力與呼吸.md "wikilink")》（重力と呼吸），此外在官方YouTube公開新歌〈Your
Song〉的MUSIC VIDEO簡短版。

10月3日，在官方YouTube公開〈Your Song〉（包括MV及Original Story兩種版本）、〈SINGLES〉(MUSIC
VIDEO簡短版）。同一天宣布所有的作品（除了最新專輯之外）在台灣開放線上付費下載收聽，另外也公佈台灣公演的門票販售時間為11月24日。

11月17日，對於即將到台北舉辦出道以來首場海外演出，成員們都非常興奮。櫻井向台灣媒體表示：「我很期待台灣演出，也希望不會愧對大家對我們的期待。」另外，也特地寫下獻給台灣歌迷的訊息：

11月24日，台灣場在12點開始售票後2分鐘內隨即完售。

11月25日，日本官網發佈台灣場追加公演，日期為2019年2月1日。

12月24日，宣佈新的巡迴「Mr.Children Dome Tour 2019 "Against All
GRAVITY"」，以五大巨蛋為場地，分別是：[福岡巨蛋](../Page/福岡巨蛋.md "wikilink")（2019年4月20日及21日）、[札幌巨蛋](../Page/札幌巨蛋.md "wikilink")（2019年5月2日）、[大阪巨蛋](../Page/大阪巨蛋.md "wikilink")（2019年5月11日及12日）、[東京巨蛋](../Page/東京巨蛋.md "wikilink")（2019年5月19日及20日）、[名古屋巨蛋](../Page/名古屋巨蛋.md "wikilink")（2019年5月25日及26日）。同時宣告將在2019年1月24日在大阪舉辦一日會員限定演唱會「FATHER\&MOTHER
Special Live 2019.1.24 Zepp Namba」。另外，「Mr.Children Tour 2018-19
重力と呼吸」發表新的週邊商品，追加了四款T恤及2019年2月1日追加公演的手環。T恤亦將台灣追加公演的場次印刷在T恤背面。

### 2019年

1月5日，台灣文學刊物《[聯合文學](../Page/聯合文學.md "wikilink")》發行以Mr.Children為題的專號，其中收錄文藝界人士、粉絲們長期以來聆聽的感受，以及各自回憶。

1月11日，開始「Mr.Children Dome Tour 2019 "Against All
GRAVITY"」巡迴的會員抽選，其中新增了一個抽選選項「哪一場都可以」（どの公演でもよい）。

1月25日，第十九張專輯《[重力與呼吸](../Page/重力與呼吸.md "wikilink")》同步在日本及台灣開放線上付費下載。

2月1日，官方[Facebook正式開始](../Page/Facebook.md "wikilink")，並且同時用日文、英文及繁體中文公告。（該專頁是延用原先的Tour
Mr.Children專頁）

2月1日，正式於台北小巨蛋開唱，接連兩天共吸引2.2萬聽眾，許多日本歌迷更特地專程來台觀看。為了台北場演出，Mr.Children展現無比誠意，主唱櫻井和壽抵達台北後就努力練習中文，不時請教懂得中文的工作人員，熟記「謝謝大家」、「開心嗎」、「一起嗨吧」、「一起唱」、「我愛你們」等問候語，還特別更換日本場次巡迴曲目，將〈Tomorrow
never knows〉、〈想擁抱你〉、〈innocent world〉、〈Sign〉以及〈無盡的旅程〉等5首金曲獻唱。\[81\]
兩天的演唱會除了呼籲場內絕對不可拍照、攝影，違規者將被強制退場外，還創下台灣首例希望購票進場歌迷不要攜帶任何發光物品如螢光棒、燈版入場。由[Facebook上社團](../Page/Facebook.md "wikilink")「We
Love
Mr.Children」則發起2月2日的手幅應援活動，在安可曲唱畢後舉出，現場共有8,740名歌迷參與。此舉令Mr.Children與工作人員相當感動，隔天在Father\&Mother（Fan
Club）及官方Facebook貼出手幅照片表達致謝之意。另外，據稱還有日、台、港的藝人與歌手紛紛前來「朝聖」，如[嵐的](../Page/嵐.md "wikilink")[櫻井翔](../Page/櫻井翔.md "wikilink")、[五月天的](../Page/五月天.md "wikilink")[阿信及](../Page/阿信.md "wikilink")[瑪莎夫婦](../Page/瑪莎.md "wikilink")、[楊丞琳](../Page/楊丞琳.md "wikilink")、[炎亞綸](../Page/炎亞綸.md "wikilink")、[余文樂](../Page/余文樂.md "wikilink")、[何韻詩等](../Page/何韻詩.md "wikilink")。

## 作品列表

### 單曲

#### CD单曲

1.  [有你的夏天](../Page/有你的夏天.md "wikilink")
2.  [想擁抱你](../Page/想擁抱你_\(Mr.Children單曲\).md "wikilink")
3.  [Replay](../Page/Replay_\(單曲\).md "wikilink")
4.  [CROSS ROAD](../Page/CROSS_ROAD_\(Mr.Children單曲\).md "wikilink")
5.  [innocent world](../Page/innocent_world.md "wikilink")
6.  [Tomorrow never
    knows](../Page/Tomorrow_never_knows_\(Mr.Children單曲\).md "wikilink")
7.  [everybody goes -向沒有秩序的現代Drop
    Kick-](../Page/everybody_goes_-向沒有秩序的現代Drop_Kick-.md "wikilink")
8.  [【es】 ～Theme of es～](../Page/【es】_～Theme_of_es～.md "wikilink")
9.  [See-Saw Game ～勇敢的戀曲～](../Page/See-Saw_Game_～勇敢的戀曲～.md "wikilink")
10. [無名的詩](../Page/無名的詩.md "wikilink")
11. [花 -Mémento-Mori-](../Page/花_-Mémento-Mori-.md "wikilink")
12. [機關槍連續掃射 -Mr.Children
    Bootleg-](../Page/機關槍連續掃射_-Mr.Children_Bootleg-.md "wikilink")
13. [Everything (It's
    you)](../Page/Everything_\(It's_you\).md "wikilink")
14. [向西向東](../Page/向西向東.md "wikilink")
15. [無盡的旅程](../Page/無盡的旅程.md "wikilink")
16. [向光映射的地方](../Page/向光映射的地方.md "wikilink")
17. [I'LL BE](../Page/I'LL_BE_\(Mr.Children單曲\).md "wikilink")
18. [口笛](../Page/口笛_\(單曲\).md "wikilink")
19. [NOT FOUND](../Page/NOT_FOUND_\(單曲\).md "wikilink")
20. [溫柔的歌](../Page/溫柔的歌.md "wikilink")
21. [youthful days](../Page/youthful_days.md "wikilink")
22. [喜歡你](../Page/喜歡你_\(Mr.Children單曲\).md "wikilink")
23. [Any](../Page/Any.md "wikilink")
24. [-{HERO}-](../Page/HERO_\(Mr.Children單曲\).md "wikilink")
25. [掌/Kurumi](../Page/掌/Kurumi.md "wikilink")
26. [Sign](../Page/Sign_\(Mr.Children單曲\).md "wikilink")
27. [四次元 Four Dimensions](../Page/四次元_Four_Dimensions.md "wikilink")
28. [箒星](../Page/箒星.md "wikilink")
29. [印記](../Page/印記_\(Mr.Children單曲\).md "wikilink")
30. [Fake](../Page/Fake_\(Mr.Children單曲\).md "wikilink")
31. [啟程之歌](../Page/啟程之歌.md "wikilink")
32. [GIFT](../Page/GIFT_\(Mr.Children單曲\).md "wikilink")
33. [HANABI](../Page/HANABI_\(Mr.Children單曲\).md "wikilink")
34. [祈禱 ～眼淚的痕跡/End of the
    day/pieces](../Page/祈禱_～眼淚的痕跡/End_of_the_day/pieces.md "wikilink")
35. [腳步聲 ～Be Strong](../Page/腳步聲_～Be_Strong.md "wikilink")
36. [光之畫室](../Page/光之畫室.md "wikilink")
37. [himawari](../Page/himawari_\(Mr.Children單曲\).md "wikilink")

#### 僅供網上下載單曲

1.  [花的香氣](../Page/花的香氣.md "wikilink") (花の匂い)
2.  [fanfare](../Page/fanfare.md "wikilink")
3.  [數數歌](../Page/數數歌_\(單曲\).md "wikilink") (かぞえうた)
4.  [hypnosis](../Page/hypnosis_\(Mr.Children單曲\).md "wikilink")
5.  [REM](../Page/REM_\(Mr.Children單曲\).md "wikilink")
6.  [放逐](../Page/放逐_\(單曲\).md "wikilink") (放たれる)
7.  [here comes my
    love](../Page/here_comes_my_love_\(Mr.Children單曲\).md "wikilink")

### 专辑

#### 原創專輯

1.  [EVERYTHING](../Page/EVERYTHING_\(Mr.Children專輯\).md "wikilink")
2.  [Kind of Love](../Page/Kind_of_Love.md "wikilink")
3.  [Versus](../Page/Versus_\(Mr.Children專輯\).md "wikilink")
4.  [Atomic Heart](../Page/Atomic_Heart.md "wikilink")
5.  [深海](../Page/深海_\(專輯\).md "wikilink")
6.  [BOLERO](../Page/BOLERO_\(專輯\).md "wikilink")
7.  [DISCOVERY](../Page/DISCOVERY_\(Mr.Children專輯\).md "wikilink")
8.  [Q](../Page/Q_\(專輯\).md "wikilink")
9.  [IT'S A WONDERFUL
    WORLD](../Page/IT'S_A_WONDERFUL_WORLD.md "wikilink")
10. [至福之音](../Page/至福之音.md "wikilink")
11. [I ♥ U](../Page/I_♥_U.md "wikilink")
12. [HOME](../Page/HOME_\(Mr.Children專輯\).md "wikilink")
13. [SUPERMARKET FANTASY](../Page/SUPERMARKET_FANTASY.md "wikilink")
14. [SENSE](../Page/SENSE_\(Mr.Children專輯\).md "wikilink")
15. [\[(an imitation) blood
    orange](../Page/［\(an_imitation\)_blood_orange］.md "wikilink")\]
16. [REFLECTION](../Page/REFLECTION.md "wikilink")
17. [重力与呼吸](../Page/重力与呼吸.md "wikilink")

#### 精選專輯

1.  [LAND IN ASIA](../Page/LAND_IN_ASIA.md "wikilink")（香港、台灣等部分亞洲地區限定）
2.  [Mr.Children
    1992-1995](../Page/Mr.Children_1992-1995.md "wikilink")（通称「肉」）
3.  [Mr.Children
    1996-2000](../Page/Mr.Children_1996-2000.md "wikilink")（通称「骨」）
4.  [B-SIDE](../Page/B-SIDE.md "wikilink")（B面曲精選）
5.  [Mr.Children
    2001-2005＜micro＞](../Page/Mr.Children_2001-2005＜micro＞.md "wikilink")
6.  [Mr.Children
    2005-2010＜macro＞](../Page/Mr.Children_2005-2010＜macro＞.md "wikilink")
7.  [Mr.Children 1992-2002 Thanksgiving
    25](../Page/Mr.Children_1992-2002_Thanksgiving_25.md "wikilink")
8.  [Mr.Children 2003-2015 Thanksgiving
    25](../Page/Mr.Children_2003-2015_Thanksgiving_25.md "wikilink")

#### 現場專輯

1.  [1/42](../Page/1/42.md "wikilink")

#### 書籍

1995.04.25 【es】Mr.Children in 370 DAYS

2001.12.10 Mr.Children 詩集　優しい歌

2018.10.03 Mr.Children全曲詩集『Your Song』(通常版)

2018.10.03 Mr.Children全曲詩集『Your Song』(愛蔵版), 僅限 Mr.Children Fan Club會員訂購,
登記預購的截止時間為2018年10月10日

## 獎項與紀錄

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>年份</p></th>
<th style="text-align: left;"><p>獎項</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>1994</p></td>
<td style="text-align: left;"><ul>
<li>第3回<a href="../Page/日劇學院賞.md" title="wikilink">日劇學院賞</a> 主題曲賞：《<a href="../Page/Tomorrow_never_knows_(Mr.Children單曲).md" title="wikilink">Tomorrow never knows</a>》[82]</li>
<li>第36回<a href="../Page/日本唱片大獎.md" title="wikilink">日本唱片大獎</a>[83]
<ul>
<li>日本唱片大獎：《<a href="../Page/innocent_world.md" title="wikilink">innocent world</a>》</li>
<li>最佳專輯獎：《<a href="../Page/Atomic_Heart.md" title="wikilink">Atomic Heart</a>》</li>
</ul></li>
</ul></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>1995</p></td>
<td style="text-align: left;"><ul>
<li>第9回<a href="../Page/日本金唱片大獎.md" title="wikilink">日本金唱片大獎</a>[84]
<ul>
<li>五大最佳藝人</li>
<li>最佳單曲大獎：《<a href="../Page/Tomorrow_never_knows_(Mr.Children單曲).md" title="wikilink">Tomorrow never knows</a>》</li>
<li>五大最佳單曲：《Tomorrow never knows》、《<a href="../Page/innocent_world.md" title="wikilink">innocent world</a>》</li>
<li>最佳專輯獎：《<a href="../Page/Atomic_Heart.md" title="wikilink">Atomic Heart</a>》</li>
</ul></li>
<li>第13回<a href="../Page/JASRAC獎.md" title="wikilink">JASRAC獎</a> 銀獎：《innocent world》[85]</li>
<li>第37回<a href="../Page/日本唱片大獎.md" title="wikilink">日本唱片大獎</a> 優秀作品賞：《<a href="../Page/See-Saw_Game_～勇敢的戀曲～.md" title="wikilink">See-Saw Game ～勇敢的戀曲～</a>》[86]</li>
</ul></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>1996</p></td>
<td style="text-align: left;"><ul>
<li>第10回<a href="../Page/日本金唱片大獎.md" title="wikilink">日本金唱片大獎</a> 五大最佳單曲：《<a href="../Page/See-Saw_Game_～勇敢的戀曲～.md" title="wikilink">See-Saw Game ～勇敢的戀曲～</a>》[87]</li>
</ul></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>1997</p></td>
<td style="text-align: left;"><ul>
<li>第11回<a href="../Page/日本金唱片大獎.md" title="wikilink">日本金唱片大獎</a> [88]
<ul>
<li>五大最佳藝人</li>
<li>最佳單曲大獎：《<a href="../Page/無名的詩.md" title="wikilink">無名的詩</a>》</li>
<li>五大最佳單曲：《<a href="../Page/花_-Mémento-Mori-.md" title="wikilink">花 -Mémento-Mori-</a>》、《無名的詩》</li>
<li>最佳專輯獎：《<a href="../Page/深海_(專輯).md" title="wikilink">深海</a>》</li>
</ul></li>
<li>第15回<a href="../Page/JASRAC獎.md" title="wikilink">JASRAC獎</a> 銀獎：《無名的詩》[89]</li>
<li><a href="../Page/國際唱片業協會#香港.md" title="wikilink">IFPIHK金唱片獎項</a>[90]
<ul>
<li>International Gold Disc：《<a href="../Page/LAND_IN_ASIA.md" title="wikilink">LAND IN ASIA</a>》</li>
<li>International Platinum Disc：《<a href="../Page/BOLERO_(專輯).md" title="wikilink">BOLERO</a>》、《LAND IN ASIA》、《深海》</li>
</ul></li>
</ul></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>1998</p></td>
<td style="text-align: left;"><ul>
<li>第12回<a href="../Page/日本金唱片大獎.md" title="wikilink">日本金唱片大獎</a> 年度搖滾專輯：《<a href="../Page/BOLERO_(專輯).md" title="wikilink">BOLERO</a>》[91]</li>
</ul></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>1999</p></td>
<td style="text-align: left;"><ul>
<li>第13回<a href="../Page/日本金唱片大獎.md" title="wikilink">日本金唱片大獎</a> 年度最佳歌曲：《<a href="../Page/無盡的旅程.md" title="wikilink">無盡的旅程</a>》[92]</li>
</ul></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>2000</p></td>
<td style="text-align: left;"><ul>
<li>第14回<a href="../Page/日本金唱片大獎.md" title="wikilink">日本金唱片大獎</a> 年度搖滾專輯：《<a href="../Page/DISCOVERY_(Mr.Children專輯).md" title="wikilink">DISCOVERY</a>》[93]</li>
</ul></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>2001</p></td>
<td style="text-align: left;"><ul>
<li>第15回<a href="../Page/日本金唱片大獎.md" title="wikilink">日本金唱片大獎</a> 年度搖滾專輯：《<a href="../Page/Q_(專輯).md" title="wikilink">Q</a>》[94]</li>
</ul></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>2002</p></td>
<td style="text-align: left;"><ul>
<li>第16回<a href="../Page/日本金唱片大獎.md" title="wikilink">日本金唱片大獎</a> 年度搖滾專輯：《<a href="../Page/Mr.Children_1992-1995.md" title="wikilink">Mr.Children 1992-1995</a>》、《<a href="../Page/Mr.Children_1996-2000.md" title="wikilink">Mr.Children 1996-2000</a>》[95]</li>
<li>SPACE SHOWER Music Video Awards - BEST GROUP VIDEO：《<a href="../Page/youthful_days.md" title="wikilink">youthful days</a>》[96]</li>
<li><a href="../Page/MTV日本音樂錄影帶大獎.md" title="wikilink">MTV日本音樂錄影帶大獎</a> 年度最佳音樂錄影帶獎、最佳導演：《<a href="../Page/喜歡你_(Mr.Children單曲).md" title="wikilink">喜歡你</a>》[97]</li>
<li>第31回<a href="../Page/日劇學院賞.md" title="wikilink">日劇學院賞</a> 主題曲賞：《youthful days》[98]</li>
</ul></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>2003</p></td>
<td style="text-align: left;"><ul>
<li>第17回<a href="../Page/日本金唱片大獎.md" title="wikilink">日本金唱片大獎</a> 年度最佳搖滾·流行專輯：《<a href="../Page/IT&#39;S_A_WONDERFUL_WORLD.md" title="wikilink">IT'S A WONDERFUL WORLD</a>》[99]</li>
<li>SPACE SHOWER Music Video Awards - BEST ANIMATION VIDEO：《<a href="../Page/HERO_(Mr.Children單曲).md" title="wikilink">-{HERO}-</a>》[100]</li>
</ul></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>2004</p></td>
<td style="text-align: left;"><ul>
<li>第18回<a href="../Page/日本金唱片大獎.md" title="wikilink">日本金唱片大獎</a> 年度最佳歌曲：《<a href="../Page/掌/Kurumi.md" title="wikilink">掌/Kurumi</a>》[101]</li>
<li>SPACE SHOWER Music Video Awards - BEST VIDEO OF THE YEAR、BEST GROUP VIDEO：《Kurumi》[102]</li>
<li>第41回<a href="../Page/日劇學院賞.md" title="wikilink">日劇學院賞</a> 主題曲賞：《<a href="../Page/Sign_(Mr.Children單曲).md" title="wikilink">Sign</a>》[103]</li>
<li>第46回<a href="../Page/日本唱片大獎.md" title="wikilink">日本唱片大獎</a> 日本唱片大獎、金獎：《Sign》[104][105]</li>
</ul></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>2005</p></td>
<td style="text-align: left;"><ul>
<li>第19回<a href="../Page/日本金唱片大獎.md" title="wikilink">日本金唱片大獎</a> [106]
<ul>
<li>年度最佳歌曲：《<a href="../Page/Sign_(Mr.Children單曲).md" title="wikilink">Sign</a>》</li>
<li>年度最佳搖滾·流行專輯：《<a href="../Page/至福之音.md" title="wikilink">至福之音</a>》</li>
<li>年度最佳音樂錄像帶：《至福之音》</li>
</ul></li>
</ul></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>2006</p></td>
<td style="text-align: left;"><ul>
<li>第20回<a href="../Page/日本金唱片大獎.md" title="wikilink">日本金唱片大獎</a> 年度最佳搖滾·流行專輯：《<a href="../Page/I_♥_U.md" title="wikilink">I ♥ U</a>》、《<a href="../Page/四次元_Four_Dimensions.md" title="wikilink">四次元 Four Dimensions</a>》[107]</li>
</ul></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>2007</p></td>
<td style="text-align: left;"><ul>
<li>第21回<a href="../Page/日本金唱片大獎.md" title="wikilink">日本金唱片大獎</a> 十大最佳單曲：《<a href="../Page/印記_(Mr.Children單曲).md" title="wikilink">印記</a>》、《<a href="../Page/箒星.md" title="wikilink">箒星</a>》[108]</li>
<li>SPACE SHOWER Music Video Awards - BEST VIDEO OF THE YEAR、BEST GROUP VIDEO：《印記》[109]</li>
</ul></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>2008</p></td>
<td style="text-align: left;"><ul>
<li>第22回<a href="../Page/日本金唱片大獎.md" title="wikilink">日本金唱片大獎</a>[110]
<ul>
<li>十大最佳單曲：《<a href="../Page/啟程之歌.md" title="wikilink">啟程之歌</a>》</li>
<li>十大最佳專輯：《<a href="../Page/HOME_(Mr.Children專輯).md" title="wikilink">HOME</a>》</li>
</ul></li>
</ul></td>
</tr>
</tbody>
</table>

### Oricon公信榜紀錄

  - 唱片總銷量：，約5497.9萬張（截至2011年8月，史上第二位）\[111\]
  - 單曲首周最高銷量：約120.8萬張（《[無名的詩](../Page/無名的詩.md "wikilink")》，史上第五位）
  - 沒有[商業搭配的單曲最高銷量](../Page/商業搭配.md "wikilink")：約181.2萬張（《[See-Saw Game
    ～勇敢的戀曲～](../Page/See-Saw_Game_～勇敢的戀曲～.md "wikilink")》，史上第一位）
  - 銷量破百萬的單曲有12張，專輯有10張（史上第二位）
  - 銷量破二百萬的單曲數目：2張（跟[恰克與飛鳥並列史上第一位](../Page/恰克與飛鳥.md "wikilink")）
  - 獲得周榜首位的單曲數目、連續獲得周榜首位的單曲數目：31張（截至2012年5月，連續紀錄史上第三位）\[112\]
  - 單曲連續獲得周榜首位的年數：15年（史上第二位）
  - 單曲於發售一段時間後再次獲得周榜首位的單曲數目：3張（並列史上第一位）
  - 獲得年榜首位的單曲數目：2張（並列史上第一位）
  - 獲得年榜頭10位的單曲數目：14張（包括《[奇蹟的地球](../Page/奇蹟的地球.md "wikilink")》，史上第二位）
  - 單曲連續獲得年榜頭10位的年數：6年（史上第一位）
  - 獲得男性藝人上半年榜首的專輯數目：3張（並列史上第一位）
  - 連續獲得周榜首位的音樂DVD數目：10張（截至2012年5月，史上第一位）\[113\]
  - 連續獲得音樂DVD周榜首位的年數：7年（史上第一位）
  - [Oricon公信榜史上首組藝人能於同一周獲得單曲](../Page/Oricon公信榜.md "wikilink")、DVD和藍光光碟榜首。\[114\]

## 參考資料

## 外部連結

  - [MR.CHILDREN](http://www.mrchildren.jp/)（官方網站）
  - [TOY'S
    FACTORY](https://web.archive.org/web/20101029142422/http://www.toysfactory.co.jp/mrchildren/index.html)（唱片公司的藝人官方網站）

[\*](../Category/Mr.Children.md "wikilink")
[Category:日本摇滚乐团](../Category/日本摇滚乐团.md "wikilink")
[Category:日本男子演唱團體](../Category/日本男子演唱團體.md "wikilink")
[Category:日本唱片大獎獲獎者](../Category/日本唱片大獎獲獎者.md "wikilink")
[Category:Oricon單曲年榜冠軍獲得者](../Category/Oricon單曲年榜冠軍獲得者.md "wikilink")
[Category:Oricon專輯年榜冠軍獲得者](../Category/Oricon專輯年榜冠軍獲得者.md "wikilink")
[Category:Billboard Japan Top
Albums年榜冠軍獲得者](../Category/Billboard_Japan_Top_Albums年榜冠軍獲得者.md "wikilink")
[Category:曾舉行五大巨蛋巡迴演唱會的音樂人](../Category/曾舉行五大巨蛋巡迴演唱會的音樂人.md "wikilink")
[Category:曾在日產體育場舉行演唱會的音樂人](../Category/曾在日產體育場舉行演唱會的音樂人.md "wikilink")
[Category:TOY'S
FACTORY旗下藝人](../Category/TOY'S_FACTORY旗下藝人.md "wikilink")
[Category:1988年成立的音樂團體](../Category/1988年成立的音樂團體.md "wikilink")
[Category:日本前衛搖滾樂團](../Category/日本前衛搖滾樂團.md "wikilink")

1.

2.

3.

4.
5.

6.
7.

8.
9.

10.

11.

12.

13.
14.

15.

16.

17.

18.

19.

20.

21.

22.

23.

24.

25.

26.

27.

28.
29.

30.

31.

32.

33.

34.

35.

36.

37.

38.

39.

40.

41.

42.

43.

44.

45.
46.

47.

48.

49.

50.

51.

52.

53.

54.

55.

56.

57.

58.
59.

60.

61.

62.

63.

64. 參閱中文維基條目：[2012年RIAJ付費音樂下載榜冠軍歌曲列表](../Page/2012年RIAJ付費音樂下載榜冠軍歌曲列表.md "wikilink")

65.

66.

67. [SUMMER SONIC 2013 2
    ＠東京](http://ameblo.jp/ikstnhw229/entry-11592502557.html)，2013-08-14
    。

68. [小田和正、ミスチル桜井と初の共作曲を披露
    ファン2000人大熱狂](http://www.oricon.co.jp/news/2031637/full/)，2013-12-05。其實該首歌曲由櫻井和壽獨立完成。

69. [（日文）キーボーディスト・SUNNYが語るMr.Childrenらとの秘話とハモリの真髄](http://www.excite.co.jp/News/90s/20151214/E1448870880470.html)，2015年12月14日。

70. [Mr.Children
    REFLECTION 3週間限定上映](https://hlo.tohotheater.jp/net/movie/TNPI3060J01.do?sakuhin_cd=011815)。

71. [Mr.Children11年間の軌跡を追った写真展、全国6都市巡回](http://natalie.mu/music/news/147207)，2015年5月15日。

72. [ミスチルライブツアー「虹」2016/6/4
    日比谷のセトリと感想まとめ](http://report-newage.com/1932)，2016-6-4。

73. [桜井和寿「I ♥ CD
    shops\!」プロジェクト開始「CDを手に取る喜びを感じてもらいたい」](http://ro69.jp/news/detail/151135?rtw)，2016-11-7。

74. [（日文）ミスチル、名古屋公演中止を謝罪 三重公演は延期に
    桜井和寿のど不調で](http://www.oricon.co.jp/news/2087663/full/)，2017-03-17。

75. [（日文）名古屋公演中止に思う事](http://55242601.blog112.fc2.com/blog-entry-574.html)，2017-3-18。

76. [（日文）ミスチル ライブ 2017/5/10 名古屋国際会議場 セットリスト レポ ネタバレ
    感想](http://live-concert.net/archives/3446) ，2017-5-10。

77. [（日文）強すぎ！ミスチル25周年ベストがデジタルアルバムランキング4週連続首位、DAが3位初登場、ジャスティンは5位に上昇](http://music-book.jp/music/news/news/145261)，2017-6-7
    \[2017-6-9\]。

78. [Mr.Children「himawari (Live ver.)」 MUSIC VIDEO (Short
    ver.)](https://www.youtube.com/watch?v=oGLKmSpgzXA)。

79. [（日文）映画『君の膵臓をたべたい』オフィシャルサイト](http://kimisui.jp/)。

80. [（日文）8/7付週間シングルランキング1位はMr.Childrenの「himawari」](http://www.oricon.co.jp/news/2094985/)，2017-8-2。

81. \[<https://www.cna.com.tw/news/amov/201902010279.aspx>
    Mr.Children相隔24年訪台開唱 台日粉絲齊追星，2019-2-1。

82.

83.
84.

85.

86.

87.

88.

89.
90.

91.

92.

93.

94.

95.

96.

97.
98.
99.

100.

101.

102.

103.
104.
105.

106.

107.

108.

109.

110.

111.
112.
113.
114.