**杭浦高速公路**，又称“[沪杭高速公路复线](../Page/沪杭高速公路.md "wikilink")”。北起上海浦东（名义上），南抵杭州大井入城口，与石大快速路互通。行程比[沪杭高速公路缩短一至半小时](../Page/沪杭高速公路.md "wikilink")，全塘至海盐枢纽段是G15[沈海高速公路的组成部分](../Page/沈海高速公路.md "wikilink")，乍浦至绕城东段为G92[杭州湾环线高速公路北线的组成部分](../Page/杭州湾环线高速公路.md "wikilink")，而绕城东至大井为省级高速公路，编号为S16。已于2008年1月29日建成通车。

## 互通枢纽和服务设施列表

[H](../Category/浙江省高速公路.md "wikilink")
[Category:杭州公路](../Category/杭州公路.md "wikilink")
[Category:嘉兴道路](../Category/嘉兴道路.md "wikilink")