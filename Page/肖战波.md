**肖战波**，是中国退役职业足球运动员，主要司职中场，也能够出任中后卫，肖战波主罚任意球的能力较为著名，曾经有多次进球或助攻。

## 足球启蒙

1984年底，[辽宁体育运动学院的足球教练](../Page/辽宁体育运动学院.md "wikilink")[张引招收](../Page/张引.md "wikilink")77、78年龄段的少年学员。肖战波的母亲战翠凤是省体育大院的体操教练，听到消息后第一时间就把肖战波送来。

## 职业生涯

1994年，肖战波代表辽宁二队参加乙级联赛。

肖战波21岁时进入辽宁一线队，其后深得老帅[苏永舜的信任](../Page/苏永舜.md "wikilink")，成为队中主力。2001年，肖战波因待遇问题与俱乐部发生分歧，转会[青岛啤酒](../Page/青岛中能.md "wikilink")。在这之前，肖战波主要打中场，直到青岛队主帅[李章洙将他调整到后卫线上](../Page/李章洙.md "wikilink")，才彰显出他的防守才能。此后，他凭借出色表现入选了[中国国家队](../Page/中国国家足球队.md "wikilink")。2004年，肖战波与青岛合同期满，以[自由转会形式加盟](../Page/中国足球转会制度#自由转会.md "wikilink")[上海申花](../Page/上海申花.md "wikilink")，正式公布的身价高达420万元，而外界普遍认为实际成交价远远超出这一数字，接近千万\[1\]，转会成功的肖战波也是中国足坛极少数获得五年长期合同的球员。2005年底，[杜威远赴](../Page/杜威_\(足球运动员\).md "wikilink")[苏格兰](../Page/苏格兰.md "wikilink")[凯尔特人之后](../Page/凯尔特人足球俱乐部.md "wikilink")，肖战波接任申花俱乐部的队长。

2007年初[申花联城大合并](../Page/申花联城大合并.md "wikilink")，肖战波成为“四队长”之一。不过2007年11月，肖战波由于在比赛前被安排为替补，擅自离开了球队，因此遭到了俱乐部处罚半年年薪的严厉制裁，被戏称为“战神门”\[2\]。但是2008年他依然入选了中国国家队，并且在下半赛季重新进入申花主力阵容，在母亲逝世前后仍坚持为球队比赛。

2008年赛季结束后，由于合同期满而且年龄较大等原因，肖战波被[上海申花挂牌](../Page/上海申花.md "wikilink")，但是未能转会成功，处于无球可踢的状态。2010年初，一年未能参加正式比赛的肖战波在试训后得到肯定，加盟[成都谢菲联](../Page/成都谢菲联.md "wikilink")，以近35岁的年龄成为中国足坛为数不多的高龄球员。而后的2011年赛季便渐渐消失在人们的视线中，可是肖战波没有真正挂靴。

2011年11月，肖战波以教练兼队员的身份加盟[沈阳东进](../Page/沈阳东进.md "wikilink")。

## 注释

[category:中国足球运动员](../Page/category:中国足球运动员.md "wikilink")
[category:上海申花球员](../Page/category:上海申花球员.md "wikilink")
[category:沈阳人](../Page/category:沈阳人.md "wikilink")
[Z](../Page/category:肖姓.md "wikilink")

[Category:遼寧宏運球員](../Category/遼寧宏運球員.md "wikilink")
[Category:青岛中能球员](../Category/青岛中能球员.md "wikilink")
[Category:中国国家足球队运动员](../Category/中国国家足球队运动员.md "wikilink")

1.
2.