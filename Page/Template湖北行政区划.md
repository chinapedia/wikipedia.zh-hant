[硚口区](../Page/硚口区.md "wikilink"){{.w}}[汉阳区](../Page/汉阳区.md "wikilink"){{.w}}[武昌区](../Page/武昌区.md "wikilink"){{.w}}[青山区](../Page/青山区_\(武汉市\).md "wikilink"){{.w}}[洪山区](../Page/洪山区.md "wikilink"){{.w}}[东西湖区](../Page/东西湖区.md "wikilink"){{.w}}[汉南区](../Page/汉南区.md "wikilink"){{.w}}[蔡甸区](../Page/蔡甸区.md "wikilink"){{.w}}[江夏区](../Page/江夏区.md "wikilink"){{.w}}[黄陂区](../Page/黄陂区.md "wikilink"){{.w}}[新洲区](../Page/新洲区.md "wikilink")
}}

|group3style =text-align: center; |group3 =
[地级市](../Page/地级市.md "wikilink") |list3 =
[西塞山区](../Page/西塞山区.md "wikilink"){{.w}}[下陆区](../Page/下陆区.md "wikilink"){{.w}}[铁山区](../Page/铁山区.md "wikilink"){{.w}}[大冶市](../Page/大冶市.md "wikilink"){{.w}}[阳新县](../Page/阳新县.md "wikilink")

|group4 = [十堰市](../Page/十堰市.md "wikilink") |list4=
[茅箭区](../Page/茅箭区.md "wikilink"){{.w}}[张湾区](../Page/张湾区.md "wikilink"){{.w}}[郧阳区](../Page/郧阳区.md "wikilink"){{.w}}[丹江口市](../Page/丹江口市.md "wikilink"){{.w}}[郧西县](../Page/郧西县.md "wikilink"){{.w}}[竹山县](../Page/竹山县.md "wikilink"){{.w}}[竹溪县](../Page/竹溪县.md "wikilink"){{.w}}[房县](../Page/房县.md "wikilink")

|group5 = [宜昌市](../Page/宜昌市.md "wikilink") |list5 =
[西陵区](../Page/西陵区.md "wikilink"){{.w}}[伍家岗区](../Page/伍家岗区.md "wikilink"){{.w}}[点军区](../Page/点军区.md "wikilink"){{.w}}[猇亭区](../Page/猇亭区.md "wikilink"){{.w}}[夷陵区](../Page/夷陵区.md "wikilink"){{.w}}[宜都市](../Page/宜都市.md "wikilink"){{.w}}[当阳市](../Page/当阳市.md "wikilink"){{.w}}[枝江市](../Page/枝江市.md "wikilink"){{.w}}[远安县](../Page/远安县.md "wikilink"){{.w}}[兴山县](../Page/兴山县.md "wikilink"){{.w}}[秭归县](../Page/秭归县.md "wikilink"){{.w}}[长阳土家族自治县](../Page/长阳土家族自治县.md "wikilink"){{.w}}[五峰土家族自治县](../Page/五峰土家族自治县.md "wikilink")

|group6 = [襄阳市](../Page/襄阳市.md "wikilink") |list6 =
[襄城区](../Page/襄城区.md "wikilink"){{.w}}[樊城区](../Page/樊城区.md "wikilink"){{.w}}[襄州区](../Page/襄州区.md "wikilink"){{.w}}[老河口市](../Page/老河口市.md "wikilink"){{.w}}[枣阳市](../Page/枣阳市.md "wikilink"){{.w}}[宜城市](../Page/宜城市.md "wikilink"){{.w}}[南漳县](../Page/南漳县.md "wikilink"){{.w}}[谷城县](../Page/谷城县.md "wikilink"){{.w}}[保康县](../Page/保康县.md "wikilink")

|group7 = [鄂州市](../Page/鄂州市.md "wikilink") |list7 =
[梁子湖区](../Page/梁子湖区.md "wikilink"){{.w}}[华容区](../Page/华容区.md "wikilink"){{.w}}[鄂城区](../Page/鄂城区.md "wikilink")

|group8 = [荆门市](../Page/荆门市.md "wikilink") |list8 =
[东宝区](../Page/东宝区.md "wikilink"){{.w}}[掇刀区](../Page/掇刀区.md "wikilink"){{.w}}[钟祥市](../Page/钟祥市.md "wikilink"){{.w}}[京山市](../Page/京山市.md "wikilink"){{.w}}[沙洋县](../Page/沙洋县.md "wikilink")

|group9 = [孝感市](../Page/孝感市.md "wikilink") |list9 =
[孝南区](../Page/孝南区.md "wikilink"){{.w}}[应城市](../Page/应城市.md "wikilink"){{.w}}[安陆市](../Page/安陆市.md "wikilink"){{.w}}[汉川市](../Page/汉川市.md "wikilink"){{.w}}[孝昌县](../Page/孝昌县.md "wikilink"){{.w}}[大悟县](../Page/大悟县.md "wikilink"){{.w}}[云梦县](../Page/云梦县.md "wikilink")

|group10 = [荆州市](../Page/荆州市.md "wikilink") |list10 =
[沙市区](../Page/沙市区.md "wikilink"){{.w}}[荆州区](../Page/荆州区.md "wikilink"){{.w}}[石首市](../Page/石首市.md "wikilink"){{.w}}[洪湖市](../Page/洪湖市.md "wikilink"){{.w}}[松滋市](../Page/松滋市.md "wikilink"){{.w}}[公安县](../Page/公安县.md "wikilink"){{.w}}[监利县](../Page/监利县.md "wikilink"){{.w}}[江陵县](../Page/江陵县.md "wikilink")

|group11 = [黄冈市](../Page/黄冈市.md "wikilink") |list11 =
[黄州区](../Page/黄州区.md "wikilink"){{.w}}[麻城市](../Page/麻城市.md "wikilink"){{.w}}[武穴市](../Page/武穴市.md "wikilink"){{.w}}[团风县](../Page/团风县.md "wikilink"){{.w}}[红安县](../Page/红安县.md "wikilink"){{.w}}[罗田县](../Page/罗田县.md "wikilink"){{.w}}[英山县](../Page/英山县.md "wikilink"){{.w}}[浠水县](../Page/浠水县.md "wikilink"){{.w}}[蕲春县](../Page/蕲春县.md "wikilink"){{.w}}[黄梅县](../Page/黄梅县.md "wikilink")

|group12 = [咸宁市](../Page/咸宁市.md "wikilink") |list12 =
[咸安区](../Page/咸安区.md "wikilink"){{.w}}[赤壁市](../Page/赤壁市.md "wikilink"){{.w}}[嘉鱼县](../Page/嘉鱼县.md "wikilink"){{.w}}[通城县](../Page/通城县.md "wikilink"){{.w}}[崇阳县](../Page/崇阳县.md "wikilink"){{.w}}[通山县](../Page/通山县.md "wikilink")

|group13 = [随州市](../Page/随州市.md "wikilink") |list13 =
[曾都区](../Page/曾都区.md "wikilink"){{.w}}[广水市](../Page/广水市.md "wikilink"){{.w}}[随县](../Page/随县.md "wikilink")
}} |group4style = text-align: center; |group4 =
[自治州](../Page/自治州.md "wikilink") |list4 =
[利川市](../Page/利川市.md "wikilink"){{.w}}[建始县](../Page/建始县.md "wikilink"){{.w}}[巴东县](../Page/巴东县.md "wikilink"){{.w}}[宣恩县](../Page/宣恩县.md "wikilink"){{.w}}[咸丰县](../Page/咸丰县.md "wikilink"){{.w}}[来凤县](../Page/来凤县.md "wikilink"){{.w}}[鹤峰县](../Page/鹤峰县.md "wikilink")
}}

|group5style = text-align: center; |group5 = [省直辖
县级行政区](../Page/省直辖县级行政区.md "wikilink") |list5 =
[潜江市](../Page/潜江市.md "wikilink"){{.w}}[天门市](../Page/天门市.md "wikilink"){{.w}}[神农架林区](../Page/神农架林区.md "wikilink")
}}

|belowstyle = text-align: left; font-size: 90%; |below =
参见：[中华人民共和国县级以上行政区列表](../Page/中华人民共和国县级以上行政区列表.md "wikilink")、[湖北省乡级以上行政区列表](../Page/湖北省乡级以上行政区列表.md "wikilink")。
}}<noinclude>

</noinclude>

[Category:中华人民共和国各省级行政区行政区划模板](../Category/中华人民共和国各省级行政区行政区划模板.md "wikilink")
[\*](../Category/湖北行政区划.md "wikilink")
[湖北行政区划模板](../Category/湖北行政区划模板.md "wikilink")