**架空歷史小說**(Alternate history、alternative
reality)，简称**架空小說**，即描述「并非真實發生的虛構[历史](../Page/历史.md "wikilink")」的小說，包括历史背景及未来。小说的最大不同在于故事通常会发生在一个作者以某些創作前提（如娛樂性、特定背景等）為本所虚构的或被改编的历史，一个由作者随意设定的世界。

《[康州美国佬在亚瑟王朝](../Page/康州美國佬大鬧亞瑟王朝.md "wikilink")》是美国作家马克·吐温的一部著名的长篇小说，这部小说开创了一百年后风行的“穿越小说”这一类型，被誉为“穿越文鼻祖”。

## 流派

### 完全架空类

小说开展的故事背景是由作者所彻底虚构的或被改编的历史或未来，这类小说准确的应定义为“架空小说”而非架空历史小说。大众所熟知的早期當代架空历史小说田中芳树的《[银河英雄传说](../Page/银河英雄传说.md "wikilink")》，近年中文网络上的作品《随波逐流之一代军师》为代表，就属于这一类背景完全虚构的架空小说，其背景和真实世界或历史没有什么联系，作者可以随意发挥的余地较大，对历史考据的要求也比较低。

### 架空历史类

### 穿越类

目前架空历史小说中最大的流派。主要模式是一个现代人在不知名的力量作用下回到古代或者近现代，从而改变历史进程的故事。根据穿越的模式不同，又分为身体灵魂完整穿、灵魂附体穿、自身还原穿、灵魂对穿、古人逆穿越等多种模式。可参见[穿越小说](../Page/穿越小说.md "wikilink")。

## 主要代表作品

### 香港作家作品

  - 《[建豐二年](../Page/建豐二年.md "wikilink")》
    作者：[陳冠中](../Page/陳冠中.md "wikilink")
  - 《[寻秦记](../Page/寻秦记.md "wikilink")》 作者：[黄易](../Page/黄易.md "wikilink")
  - 《[王道平天下](../Page/王道平天下.md "wikilink")》系列
    作者：[安德烈](../Page/安德烈.md "wikilink")
      - 《[侵略者與拯救者](../Page/侵略者與拯救者.md "wikilink")》
      - 《[報復與寬恕](../Page/報復與寬恕.md "wikilink")》

### 中國大陆作家作品

  - 《[天意](../Page/天意_\(科幻小说\).md "wikilink")》作者：钱莉芳
  - 《[新宋](../Page/新宋.md "wikilink")》 作者：[阿越](../Page/阿越.md "wikilink")
  - 《[宰执天下](../Page/宰执天下.md "wikilink")》作者：哥斯拉
  - 《[窃明](../Page/窃明.md "wikilink")》 作者：[灰熊猫](../Page/灰熊猫.md "wikilink")
  - 《[回到明朝当王爷](../Page/回到明朝当王爷.md "wikilink")》 作者：月关
  - 《[錦衣夜行](../Page/錦衣夜行.md "wikilink")》 作者：月关
  - 《步步生蓮》作者：月關
  - 《[后宫甄嬛传](../Page/后宫甄嬛传.md "wikilink")》
    作者：[流潋紫](../Page/流潋紫.md "wikilink")
  - 《[倾世皇妃](../Page/倾世皇妃.md "wikilink")》 作者：慕容湮儿
  - 《[江山美人謀](../Page/江山美人謀.md "wikilink")》 作者：袖唐
  - 《[琅琊榜](../Page/瑯琊榜_\(小说\).md "wikilink")》作者: 海宴
  - 《[临高启明](../Page/临高启明.md "wikilink")》 作者：吹牛者
  - 《[大明1937](../Page/大明1937_\(小说\).md "wikilink")》作者: 我是貓

### 台湾作家作品

  - 《[大宇宙戰爭年代志](../Page/大宇宙戰爭年代志.md "wikilink")》作者：楊立強
  - 《[噩盡島](../Page/噩盡島.md "wikilink")》作者：莫仁
  - 《[東寧記](../Page/東寧記.md "wikilink")》，網路小說。書中以[鄭克𡒉為主角](../Page/鄭克𡒉.md "wikilink")，描寫其用現代科技改造鄭氏政權，反攻中國大陸。書中充滿強烈漢族主義，並醜化[康熙帝](../Page/康熙帝.md "wikilink")。
  - 《[银河新世纪](../Page/银河新世纪.md "wikilink")》 作者：凯云

### 日本作家作品

  - 《[銀河英雄傳說](../Page/銀河英雄傳說.md "wikilink")》作者：[田中芳樹](../Page/田中芳樹.md "wikilink")
  - 《[紺碧艦隊](../Page/紺碧艦隊.md "wikilink")》作者：[荒卷義雄](../Page/荒卷義雄.md "wikilink")
  - 《[次元艦隊](../Page/次元艦隊.md "wikilink")》作者：[川口開治](../Page/川口開治.md "wikilink")

### 欧美相关作品

  - 《》作者：美国[马克·吐温](../Page/马克·吐温.md "wikilink")
  - 《[高堡奇人](../Page/高堡奇人.md "wikilink")》作者：美國[菲利普·狄克](../Page/菲利普·狄克.md "wikilink")
  - 《[把銀禧帶來](../Page/把銀禧帶來.md "wikilink")》作者：美国[沃德·摩爾](../Page/沃德·摩爾.md "wikilink")

## 外部連結

  - [AlternateHistory.com](http://www.alternatehistory.com/)—The largest
    English-language Alternate History community on the Internet.
  - [Counter-Factual.net](http://www.counter-factual.net/)—site
    dedicated to alternate history as well as other topics, such as
    science fiction and creative writing.
  - [Changing The Times](http://www.changingthetimes.net/), is an
    Alternate History Electronic Magazine written and maintained by
    alternate historians. It contains a discussion board.
  - ["For Want of a
    Genre"](https://www.webcitation.org/6ARFDeRRN?url=http://web.archive.org/web/20081209061941/http://www.nebulaawards.com/index.php/guest_blogs/for_want_of_a_genre/),
    article by Christopher M. Cevasco.
  - [Histalt.com](http://www.histalt.com/) is author [Richard J. (Rick)
    Sutcliffe](../Page/Richard_J._\(Rick\)_Sutcliffe.md "wikilink")'s
    collection of Alternate History links.
  - [The Sidewise Award for Alternate
    History](http://www.uchronia.net/sidewise) lists all the winners and
    nominees for the award since its inception and provides information
    for recommending works for consideration.
  - [Uchronia](http://www.uchronia.net/) has an [introduction to the
    topic](http://www.uchronia.net/intro.html), and lists over 2000
    works of alternate history.

[\*](../Category/架空歷史小說.md "wikilink")
[Category:設定](../Category/設定.md "wikilink")