**木油樹**或**[木油桐](../Page/木油桐.md "wikilink")**（拉丁學名：***Aleurites montana*
Lour.**，英文名：**Wood-oil
Tree**），是一種原生於中國南部的[落葉](../Page/落葉.md "wikilink")[喬木](../Page/喬木.md "wikilink")，由於其樹齡遠較同屬植物[油桐為長](../Page/油桐.md "wikilink")，因此也有**千年桐**的稱號。

木油樹为落叶乔木；成長高度可达12米，生長迅速。单叶互生，阔卵形，掌状脉，全缘或2－3裂，在裂口和叶柄顶端有兩枚杯狀的[蜜腺](../Page/蜜腺.md "wikilink")；春季开白色花，雌雄异株，聚伞花序生顶端；[果實形狀成三邊](../Page/果實.md "wikilink")，有三條縱長的溝紋，表面上多有皺紋。

木油樹與油桐一樣能夠提取樹油，稱作木油，但質量遠不及油桐的[桐油](../Page/桐油.md "wikilink")，因此一般只作工業用途。此外木油樹的果殼可製成[活性碳](../Page/活性碳.md "wikilink")。

## 參考資料

  - 《樹影花蹤－九龍公園樹木研習徑》，香港園藝學會 著，天地圖書 出版，2005年4月。116-117頁。ISBN 988211147-5。

## 外部連結

  - [木油桐
    Muyoutong](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D01342)
    藥用植物圖像數據庫 (香港浸會大學中醫藥學院)

[Category:大戟科](../Category/大戟科.md "wikilink")
[Category:喬木](../Category/喬木.md "wikilink")
[Category:中國植物](../Category/中國植物.md "wikilink")
[Category:香港植物](../Category/香港植物.md "wikilink")