[HK_Queen's_Pier_200608.jpg](https://zh.wikipedia.org/wiki/File:HK_Queen's_Pier_200608.jpg "fig:HK_Queen's_Pier_200608.jpg")
[HK_Queen_s_Pier_60310_N2.jpg](https://zh.wikipedia.org/wiki/File:HK_Queen_s_Pier_60310_N2.jpg "fig:HK_Queen_s_Pier_60310_N2.jpg")

**皇后碼頭**（[英文](../Page/英文.md "wikilink")：****，
是香港一座可以供予小型船隻泊岸上落客的公眾[碼頭](../Page/碼頭.md "wikilink")，現時存有的[建築物興建於](../Page/建築物.md "wikilink")1953年面對[維多利亞港](../Page/維多利亞港.md "wikilink")，香港[海事結構編號為HP](../Page/海事結構編號.md "wikilink")092。碼頭的運作於2007年4月26日起被[中環9號碼頭取代](../Page/中環碼頭.md "wikilink")；皇后碼頭本身亦於2008年2月從原址上被拆卸（連其底部樁柱也一併計算在內，則為2008年3月）。

[古物古蹟辦事處於評估報告中指出](../Page/古物古蹟辦事處.md "wikilink")，皇后碼頭為香港唯一用於舉行儀式的公眾碼頭，於[英國殖民地時期一直見證著維多利亞港海岸的變遷](../Page/英國殖民地.md "wikilink")，以及[第二次世界大戰後](../Page/第二次世界大戰.md "wikilink")[香港經濟急速發展](../Page/香港經濟.md "wikilink")，從現代建築主義形式興建而成的皇后碼頭，連同與[香港大會堂以及](../Page/香港大會堂.md "wikilink")[愛丁堡廣場所組成的建築群](../Page/愛丁堡廣場.md "wikilink")，至今仍然保留著[香港1950年代的實用主義型建築特色](../Page/香港1950年代.md "wikilink")\[1\]
。皇后碼頭後來因為[中環填海計劃而需要被拆卸](../Page/中環填海計劃.md "wikilink")，保育人士以拆卸皇后碼頭會破壞香港文化的承傳\[2\]、[集體回憶](../Page/集體回憶.md "wikilink")\[3\]和公共空間等理由反對。雖然[香港政府其後讓步](../Page/香港政府.md "wikilink")，答允拆卸後會將其重置，但是部份人士始終要求政府不要拆卸，並且必須於原來地址保留皇后碼頭。

皇后碼頭的組件現時被香港政府存放於[大嶼山](../Page/大嶼山.md "wikilink")[狗蝨灣政府爆炸倉庫內](../Page/狗蝨灣.md "wikilink")，政府在2009年8月不接納共建維港委員會原址重置皇后碼頭的建議，建議在中環9號及10號碼頭之間重置碼頭，原預計2013年完成。\[4\]其後到2016年才開展於9號及10號碼頭重置皇后碼頭的諮詢。但到2019年1月，發展局指正在詳細研究，未有具體時間表。\[5\]

## 歷史

[Queen's_Pier_in_1925.jpg](https://zh.wikipedia.org/wiki/File:Queen's_Pier_in_1925.jpg "fig:Queen's_Pier_in_1925.jpg")

### 前身及第一代

[HK_Queens_Pier_Parade_1928.jpg](https://zh.wikipedia.org/wiki/File:HK_Queens_Pier_Parade_1928.jpg "fig:HK_Queens_Pier_Parade_1928.jpg")
[Queen's_pier.gif](https://zh.wikipedia.org/wiki/File:Queen's_pier.gif "fig:Queen's_pier.gif")

皇后碼頭的前身是**皇后像停泊處**（****），\[6\]，約於20世紀初落成。停泊處因處於[皇后像廣場之前而得名](../Page/皇后像廣場.md "wikilink")，因此其「皇后」是指[維多利亞女王](../Page/維多利亞女王.md "wikilink")。

1921年，[海港工程處決定重建碼頭](../Page/海港工程處.md "wikilink")，於1924年7月31日被[香港立法局通過改名為皇后碼頭](../Page/香港立法局.md "wikilink")，碼頭於1925年10月落成，其位置在[皇后像廣場及](../Page/皇后像廣場.md "wikilink")[皇后行](../Page/皇后行.md "wikilink")（[香港文華東方酒店現址](../Page/香港文華東方酒店.md "wikilink")）附近。上蓋則以[鋼鐵建成](../Page/鋼.md "wikilink")，並且採用[混凝土樁柱](../Page/混凝土.md "wikilink")，耗資20萬港元興建，碼頭亦懸掛了[英國國旗](../Page/英國國旗.md "wikilink")\[7\]。

皇后碼頭專門供予[香港總督及](../Page/香港總督.md "wikilink")[英國主要官員往返香港時使用](../Page/英國.md "wikilink")，並且用以同時舉行歡迎或送別儀式。皇后碼頭的首個使用者，是[香港總督](../Page/香港總督.md "wikilink")[司徒拔於](../Page/司徒拔.md "wikilink")1925年10月31日因為任期結束而離開香港的時候所使用。此外，皇后碼頭在1928年曾經舉行巡遊以慶祝[英皇](../Page/英皇.md "wikilink")[喬治五世壽辰](../Page/喬治五世.md "wikilink")，其後也有慶祝英女皇壽辰的活動\[8\]。

### 第二代

[Queen's_Pier_Interior_200612.jpg](https://zh.wikipedia.org/wiki/File:Queen's_Pier_Interior_200612.jpg "fig:Queen's_Pier_Interior_200612.jpg")
[Queen's_Pier_Snack_Bar_200612.jpg](https://zh.wikipedia.org/wiki/File:Queen's_Pier_Snack_Bar_200612.jpg "fig:Queen's_Pier_Snack_Bar_200612.jpg")
1953年4月，由於中環進行填海工程，皇后碼頭與[天星碼頭被搬遷至今日的位置](../Page/天星碼頭.md "wikilink")。新皇后碼頭於1953年下旬正式使用，並且全面開放予公眾使用，而舊皇后碼頭於1954年2月被拆卸，並且於新碼頭興建上蓋\[9\]。新皇后碼頭的開幕儀式則由時任香港總督[葛量洪](../Page/葛量洪.md "wikilink")[夫人](../Page/夫人.md "wikilink")[葛慕蓮於同年](../Page/葛慕蓮.md "wikilink")6月28日舉行。

[英國殖民地時期](../Page/英國殖民地.md "wikilink")，皇后碼頭是[香港政府主要官員及](../Page/香港政府主要官員.md "wikilink")[英國皇室所使用的專用碼頭](../Page/英國皇室.md "wikilink")。歷任[香港總督上任的](../Page/香港總督.md "wikilink")[傳統](../Page/傳統.md "wikilink")，就是乘坐香港總督遊艇（由[柏立基開始為](../Page/柏立基.md "wikilink")[慕蓮夫人號](../Page/慕蓮夫人號.md "wikilink")）抵達中環，在皇后碼頭上岸，並且在[愛丁堡廣場舉行歡迎及](../Page/愛丁堡廣場.md "wikilink")[閱兵等就職儀式](../Page/閱兵.md "wikilink")，然後前往[香港大會堂](../Page/香港大會堂.md "wikilink")[宣誓](../Page/宣誓.md "wikilink")。1975年英女皇[伊利沙伯二世首次訪問](../Page/伊利沙伯二世.md "wikilink")[香港](../Page/香港.md "wikilink")，在5月4日下午於[啟德機場降落後](../Page/啟德機場.md "wikilink")，乘坐慕蓮夫人號渡過[維多利亞港](../Page/維多利亞港.md "wikilink")，於皇后碼頭上岸。

另一方面，皇后碼頭也是昔日[香港渡海泳的終點](../Page/香港渡海泳.md "wikilink")。

### 面臨拆卸及重置

[Ueen's_Pier_Last_Night_View_20070731.jpg](https://zh.wikipedia.org/wiki/File:Ueen's_Pier_Last_Night_View_20070731.jpg "fig:Ueen's_Pier_Last_Night_View_20070731.jpg")

2000年代，皇后碼頭因為需要配合[中環填海計劃](../Page/中環填海計劃.md "wikilink")，所以決定於2006年11月，新[中環碼頭啟用後](../Page/中環碼頭.md "wikilink")，新皇后碼頭與天星碼頭一同拆卸，但是皇后碼頭的清拆限期比天星碼頭長，香港民間團體連同[區議員決定阻止清拆決定](../Page/區議員.md "wikilink")。多個團體會在同年12月24日[平安夜晚上](../Page/平安夜.md "wikilink")8時在[愛丁堡廣場發起](../Page/愛丁堡廣場.md "wikilink")「盼望天星的聖誕」燭光集會，要求香港政府復修[愛丁堡廣場碼頭](../Page/愛丁堡廣場碼頭.md "wikilink")，並且爭取保留皇后碼頭。8月1日，仍然有十幾名爭取保留皇后碼頭人士在該處靜坐及[露宿](../Page/露宿.md "wikilink")，並且在該碼頭舉行圖片展覽及簽名活動。影星[周潤發更在](../Page/周潤發.md "wikilink")2007年4月28日清晨到達皇后碼頭簽名支持保留碼頭。2007年7月27日，3名保育碼頭人士宣佈由同日下午4時半開始，進行無限期靜坐及[絕食活動](../Page/絕食.md "wikilink")，直至香港政府承諾原址保留皇后碼頭為止。\[10\]而[發展局於當晚](../Page/發展局.md "wikilink")8時53分，向[香港傳媒發佈](../Page/香港傳媒.md "wikilink")[新聞稿表示不認同保育碼頭人士的絕食行動](../Page/新聞稿.md "wikilink")\[11\]。保育人士指政府沒有必要花費5千萬港元[公帑拆卸皇后碼頭](../Page/公帑.md "wikilink")，以及巨額費用，而是可以單用1至2千萬港元為皇后碼頭進行承托工程，而承托工程亦不妨礙中環填海工程，又能夠保留碼頭和節省大筆公帑。發展局則解釋，延遲移交皇后碼頭用地會使到中環填海計劃第三期工程停頓，可能導致每日達數十萬港元的[索償](../Page/索償.md "wikilink")。同時指出，已經為到碼頭進行激光掃瞄，而且貯存碼頭的立體影像及圖則，於稍後會與碼頭的非結構部份逐一拆除，拆除及存放工序會在4個月內完成。\[12\]。

2007年7月30日下午，[地政總署派遣人員到皇后碼頭的欄杆上貼上以下](../Page/地政總署.md "wikilink")[通告](../Page/通告.md "wikilink")，指在碼頭露宿的示威者是違例佔用政府土地，要求他們在8月1日午夜12前離開：\[13\]\[14\]

2007年7月31日，[香港高等法院接納](../Page/香港高等法院.md "wikilink")[本土行動成員](../Page/本土行動.md "wikilink")[何來和](../Page/何來.md "wikilink")[朱凱迪要求阻止政府清拆皇后碼頭的](../Page/朱凱迪.md "wikilink")[司法覆核申請](../Page/司法覆核.md "wikilink")，[法官](../Page/法官.md "wikilink")[林文瀚指出](../Page/林文瀚.md "wikilink")，申請當中兩個理據有法律爭議性，是故接納司法覆核申請，並且排期至2007年8月7日開審\[15\]。現時香港政府一時視[古物諮詢委員會為決策者](../Page/古物諮詢委員會.md "wikilink")
，一時則視為諮詢組織，[古物諮詢委員會成員](../Page/古物諮詢委員會.md "wikilink")[李律仁表示](../Page/李律仁.md "wikilink")，期望今次司法覆核能澄清委員會的角色\[16\]。[香港政府隨即表示午夜後將會如常展開清場行動](../Page/香港政府.md "wikilink")，以收回土地予承建商；不過香港政府以及警察沒有在凌晨零時作出清場行動，於早上11時才開始在地上清場，在傍晚6時半開始對[天台上的人士進行清場行動至晚上](../Page/天台.md "wikilink")8時45分。

<File:Queen's> Pier Last Night 20070731-1.jpg|團體在最後一晚留守皇后碼頭
<File:Queen's> Pier 2.JPG|停止使用後的皇后碼頭仍有人進行活動 <File:Queen's> Pier
3.JPG|周潤發在皇后碼頭的簽名 <File:Queen's> Pier
Notice.JPG|[地政總署所張貼的通告](../Page/地政總署.md "wikilink")

## 未來發展

[QueensPier_290907.JPG](https://zh.wikipedia.org/wiki/File:QueensPier_290907.JPG "fig:QueensPier_290907.JPG")
[Demolished_Queen's_Pier.jpg](https://zh.wikipedia.org/wiki/File:Demolished_Queen's_Pier.jpg "fig:Demolished_Queen's_Pier.jpg")
[HK_City_Hall_Views_Victoria_Harbour_Central_Land_Reclamation_n_Queen_s_Pier_n_TST_Kln.JPG](https://zh.wikipedia.org/wiki/File:HK_City_Hall_Views_Victoria_Harbour_Central_Land_Reclamation_n_Queen_s_Pier_n_TST_Kln.JPG "fig:HK_City_Hall_Views_Victoria_Harbour_Central_Land_Reclamation_n_Queen_s_Pier_n_TST_Kln.JPG")

皇后碼頭的命運至今未明，前[房屋及規劃地政局局長](../Page/房屋及規劃地政局局長.md "wikilink")[孫明揚表示](../Page/孫明揚.md "wikilink")，香港政府已經與專業團體取得共識，找到保留皇后碼頭的最佳方案，就是臨時遷走碼頭，等待填海工程完成後，在原址附近重置碼頭\[17\]。

2007年5月，[規劃署發表](../Page/規劃署.md "wikilink")[中環新海濱城市設計研究的第一階段公眾參與活動](../Page/中環新海濱城市設計研究.md "wikilink")，提出4個重置皇后碼頭的方案。同月，[古物諮詢委員會將皇后碼頭列為](../Page/古物諮詢委員會.md "wikilink")[香港一級歷史建築](../Page/香港一級歷史建築.md "wikilink")\[18\]，故此可能影響重置計劃。後來立法會財經事務委員會在2007年6月15日表示決決同意撥款遷拆皇后碼頭。

2008年4月，[規劃署發表](../Page/規劃署.md "wikilink")[中環新海濱城市設計研究的第二階段公眾參與活動](../Page/中環新海濱城市設計研究.md "wikilink")，提出兩個重置皇后碼頭的方案：一為將皇后碼頭重置於[九號及十號碼頭之間](../Page/中環碼頭.md "wikilink")，恢復其海事功能、將皇后碼頭重置於原址，保持與香港大會堂的關係，但是該海事功能將會喪失。

2008年5月8日，[香港政府表示皇后碼頭的組件安全放置在](../Page/香港政府.md "wikilink")[香港](../Page/香港.md "wikilink")[大嶼山](../Page/大嶼山.md "wikilink")[狗虱灣政府爆炸品倉庫庫內](../Page/狗虱灣.md "wikilink")。[香港政府計劃興建室內臨時倉庫](../Page/香港政府.md "wikilink")，內設溫度及濕度調節，妥善安置皇后碼頭的組件，尤其是其頂蓋部份，有待皇后碼頭正式重置於中環地區時能夠盡量保持其原貌，只是皇后碼頭在[中環九號碼頭與十號碼頭中間位置重置時可能要拆去兩側的](../Page/中環碼頭.md "wikilink")[樓梯](../Page/樓梯.md "wikilink")，以避免影響[中環九號碼頭與十號碼頭的運作](../Page/中環碼頭.md "wikilink")。\[19\]\[20\]\[21\]

到2016年3月，政府才開展重置皇后碼頭的諮詢，但並沒有事先諮詢[古物諮詢委員會](../Page/古物諮詢委員會.md "wikilink")，提供原置重址的選項。問卷調查中提出三個安置皇后碼頭於9號及10號碼頭的方案。當中1011人（52%）選擇成本最低的C選項，而37%為其他的選項。\[22\]但截至2019年1月，政府稱「正在詳細研究，未有具體時間表。」

## 交通

皇后碼頭可以供予[遊艇及小型船隻上落乘客](../Page/遊艇.md "wikilink")。而碼頭側的[愛丁堡廣場則可以作為停泊車輛用途](../Page/愛丁堡廣場.md "wikilink")。由於隔鄰的[香港大會堂設有婚姻登記處](../Page/香港大會堂.md "wikilink")，愛丁堡廣場亦是結婚照片的拍攝地點。廣場亦作為示威群眾的集合起點、旅遊團體集合出發及制服團體儀仗隊表演等用途。

## 外景場地

皇后碼頭在很多電視劇也常常出現，不論是男女主角英雄救美、偶遇、相識、談心、定情、分手及三角戀攤牌等。曾經在皇后碼頭拍攝的部份[電視劇](../Page/電視劇.md "wikilink")、[電影及](../Page/電影.md "wikilink")[音樂影片包括](../Page/音樂影片.md "wikilink")\[23\]：

1.  [無綫電視電視劇](../Page/無綫電視.md "wikilink")《[大時代](../Page/大時代.md "wikilink")》
2.  無綫電視電視劇 《[刑事偵緝檔案](../Page/刑事偵緝檔案.md "wikilink")》
3.  [亞洲電視電視劇](../Page/亞洲電視.md "wikilink")《[我和殭屍有個約會](../Page/我和殭屍有個約會.md "wikilink")》
4.  無綫電視電視劇《[水滸無間道](../Page/水滸無間道.md "wikilink")》
5.  無綫電視電視劇《[寫意人生](../Page/寫意人生.md "wikilink")》
6.  無綫電視電視劇《[阿旺新傳](../Page/阿旺新傳.md "wikilink")》
7.  無綫電視電視劇《[難兄難弟](../Page/難兄難弟.md "wikilink")》
8.  無綫電視電視劇《[東方之珠](../Page/東方之珠_\(劇集\).md "wikilink")》
9.  無綫電視電視劇《[創世紀II天地有情](../Page/創世紀.md "wikilink")》
10. 無綫電視電視劇《[天地豪情](../Page/天地豪情.md "wikilink")》
11. [張學友的](../Page/張學友.md "wikilink")《留住這時光》音樂影片
12. [林憶蓮音樂特輯](../Page/林憶蓮.md "wikilink")
13. [Beyond的](../Page/Beyond.md "wikilink")《活著便精彩》音樂影片
14. [Beyond的](../Page/Beyond.md "wikilink")《我早應該習慣》音樂影片
15. [鄭融](../Page/鄭融.md "wikilink") 《愛得耐》音樂影片
16. [薛凱琪](../Page/薛凱琪.md "wikilink")、[周國賢](../Page/周國賢.md "wikilink")
    《目黑》音樂影片
17. [許冠傑的](../Page/許冠傑.md "wikilink")《鐘聲響起》音樂影片
18. 電影《[最佳拍檔](../Page/最佳拍檔.md "wikilink")》
19. 電影《[少林足球](../Page/少林足球.md "wikilink")》 演員招募
20. 電影《[龍鳳鬥](../Page/龍鳳鬥.md "wikilink")》 宣傳活動

## [奧運聖火](../Page/奧運聖火.md "wikilink")

皇后碼頭是[1964年夏季奧林匹克運動會火炬接力的其中一站](../Page/1964年夏季奧林匹克運動會火炬接力.md "wikilink")。

## 鄰近建築物

  - [香港大會堂](../Page/香港大會堂.md "wikilink")
  - [愛丁堡廣場碼頭（舊中環天星碼頭）](../Page/愛丁堡廣場碼頭.md "wikilink")
  - [天星碼頭多層停車場](../Page/天星碼頭多層停車場.md "wikilink")
  - [解放軍駐港總部](../Page/解放軍駐港總部.md "wikilink")

## 參見

  - [愛丁堡廣場碼頭](../Page/愛丁堡廣場碼頭.md "wikilink")
  - [中環碼頭](../Page/中環碼頭.md "wikilink")
  - [九龍公眾碼頭](../Page/九龍公眾碼頭.md "wikilink")
  - [香港總督](../Page/香港總督.md "wikilink")
  - [保留舊中環天星碼頭事件](../Page/保留舊中環天星碼頭事件.md "wikilink")
  - [保留皇后碼頭事件](../Page/保留皇后碼頭事件.md "wikilink")

## 站外鏈結

  - [不告別皇后碼頭行動](http://www.queenpier.hk/)
  - [爭取保留天星與皇后碼頭](http://www.conservancy.org.hk/preleases/20060920.htm)

<!-- end list -->

  - [皇后碼頭組件存放處](http://www.queenspier.hk/tchi/QP%20History.html)

## 參考資料

[Category:中環](../Category/中環.md "wikilink")
[Category:香港地標](../Category/香港地標.md "wikilink")
[Category:香港一級歷史建築](../Category/香港一級歷史建築.md "wikilink")
[Category:香港已拆卸渡輪碼頭](../Category/香港已拆卸渡輪碼頭.md "wikilink")
[Category:香港已毀地標](../Category/香港已毀地標.md "wikilink")

1.

2.

3.

4.

5.

6.  [英王室港督踏腳點
    殖民味濃](https://web.archive.org/web/20071015201053/http://hk.news.yahoo.com/070728/12/2cg66.html)，《明報》，2007年7月29日

7.
8.
9.  工商日報，《中區填海工程進入　最後階段　新皇后碼頭開始建上蓋》，1954年2月12日

10.

11.

12.

13. [2007年7月30日下午地政總署派員到皇后碼頭的欄桿貼出的通告](http://www.rthk.org.hk/APSuppics/mfile_55_419822_1.jpg)

14. [香港特別行政區政府發展局就傳媒查詢有關注團體成員向高等法院就古物事務監督決定不根據《古物及古蹟條例》將皇后碼頭列為法定古蹟而申請司法覆核一事作出回應（2007年7月30日）](http://www.info.gov.hk/gia/general/200707/30/P200707300245.htm)

15.

16.

17. [政府提出皇后碼頭原址重置](https://web.archive.org/web/20070328084625/http://hk.news.yahoo.com/070321/12/244yf.html)

18. [皇后碼頭獲列為一級古蹟](https://web.archive.org/web/20070512122201/http://hk.news.yahoo.com/070509/12/2738f.html)

19.

20.

21.

22.

23.