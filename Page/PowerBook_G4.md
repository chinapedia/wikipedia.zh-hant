[15-inch-titanium-powerbook.jpg](https://zh.wikipedia.org/wiki/File:15-inch-titanium-powerbook.jpg "fig:15-inch-titanium-powerbook.jpg")
[AluminiumG4.jpg](https://zh.wikipedia.org/wiki/File:AluminiumG4.jpg "fig:AluminiumG4.jpg")

**PowerBook
G4**是由[蘋果電腦所設計生產的](../Page/蘋果電腦.md "wikilink")[筆記型電腦系列產品](../Page/筆記型電腦.md "wikilink")，使用[PowerPC](../Page/PowerPC.md "wikilink")
G4
[中央處理器](../Page/中央處理器.md "wikilink")（原先是由[摩托羅拉](../Page/摩托羅拉.md "wikilink")（[Motorola](../Page/Motorola.md "wikilink")）生產，現由[IBM生產](../Page/IBM.md "wikilink")），本系列的所有產品現今都已停產。

第一代的 PowerBook G4 是在 2001年1月 [MacWorld Conference &
Expo](../Page/MacWorld_Conference_&_Expo.md "wikilink")
的演說中由[史蒂夫·賈伯斯發表](../Page/史蒂夫·賈伯斯.md "wikilink")。特色是使用[鈦金屬作為外殼材料](../Page/鈦.md "wikilink")（因此被稱為：“鈦書”
），搭載有 400 或 500 MHz [Motorola的](../Page/Motorola.md "wikilink")[G4
處理器](../Page/G4_處理器.md "wikilink")。PowerBook G4
的另一特色是有前置的（當時算是先進的）吸入式光碟機（ CD 或
DVD ）。

PowerBook 鈦書生產線更新了好幾次，像是 [Gigabit
Ethernet](../Page/Gigabit_Ethernet.md "wikilink")、[DVI](../Page/DVI.md "wikilink")
接頭，以及可選購的 [SuperDrive](../Page/SuperDrive.md "wikilink") DVD
燒錄機。最後的改版在2002年11月推出，有 867 和 1000 MHz 兩種型號。

在2003年1月，[蘋果電腦推出了新的](../Page/蘋果電腦.md "wikilink") PowerBook G4
產品線，12和17吋螢幕大小的鋁質外殼。著名的是，[姚明和](../Page/姚明.md "wikilink")[Verne
Troyer](../Page/Verne_Troyer.md "wikilink") 使用 12吋和17吋的型號。

於2003年9月16日在[巴黎的](../Page/巴黎.md "wikilink")[Apple
Expo](../Page/Apple_Expo.md "wikilink")，[蘋果電腦增加了](../Page/蘋果電腦.md "wikilink")15吋的鋁製外殼的（後被稱為：鋁書）
PowerBook G4筆記型電腦產品，並且小幅升級到 12 和 17 吋的型式。

最終的版本，是在[2005年2月推出](../Page/2005年2月.md "wikilink")。包含有三種型號：12, 15 和 17
吋的螢幕大小（15 和
17吋顯示器是[寬螢幕](../Page/寬螢幕.md "wikilink")）的鋁質外殼機種。17吋的型號也是少量擁有如此大顯示器的少數筆記型電腦之一。17吋的型號天生就支援[蘋果電腦的](../Page/蘋果電腦.md "wikilink")30吋[Apple
Cinema Display](../Page/Apple_Cinema_Display.md "wikilink")，對於
15吋機種是選購配備。PowerBook 的規格範圍從 1.5 GHz 到 1.67 GHz
[中央處理器](../Page/中央處理器.md "wikilink")；512MB 到 2GB
的[隨機存取記憶體](../Page/隨機存取記憶體.md "wikilink")；60 到 100GB 的
[硬碟空間](../Page/硬碟.md "wikilink")；[nVidia](../Page/nVidia.md "wikilink")
的 GeForce FX Go 5200 和 [ATI Technologies](../Page/ATi.md "wikilink")
Radeon 9700 Mobility [繪圖處理器](../Page/GPU.md "wikilink")（GPU），選購的 128 MB
顯示記憶體空間和 Dual-Link DVI，以及多種其他特色。

鈦殼的 PowerBook G4 可以執行 [Mac OS 9](../Page/Mac_OS_9.md "wikilink") 或是 [Mac
OS X](../Page/Mac_OS_X.md "wikilink")
[作業系統](../Page/作業系統.md "wikilink")，而鋁殼的 PowerBook G4
只能一開始就執行 [Mac OS X](../Page/Mac_OS_X.md "wikilink")。兩種系列的機器都可以在 [Mac
OS X](../Page/Mac_OS_X.md "wikilink") 環境下用 [Classic
Mode](../Page/:en:Classic_\(Mac_OS_X\).md "wikilink") 來執行 Mac OS 9 的程式。

最終型號的 PowerBook G4
包含更新的[觸控板驅動程式](../Page/觸控板.md "wikilink")，可以在任何視窗下使用垂直捲動（例如，[Safari瀏覽器](../Page/Safari.md "wikilink")，或是
[Finder](../Page/Finder.md "wikilink")。在板上使用兩跟手指頭而不是一般的單一手指）。這種經常需要的特色，也有第三方廠商提供的觸控板驅動程式（[Sidetrack](http://www.ragingmenace.com/software/sidetrack)）可以把類似的能力加入到標準的
[PowerBook](../Page/PowerBook.md "wikilink") 和
[iBook上](../Page/iBook.md "wikilink")。Sidetrack
驅動程式把觸控板分為兩個不同的區域來作不同的事情；左手方向邊緣的動作可以捲動，中間當作一般滑鼠使用等等，都可以在喜好設定裡面由使用者自行調整。另外一種工具稱為
iScroll 2，可以在舊的型號上也有觸控板支援的機器"啟動"兩指捲動功能。

## 历史

### Titanium PowerBook G4

新一代的Titanium PowerBook
G4成为当时市场上最轻薄的全功能笔记本电脑。它最先配置了宽屏显示器，屏幕大而漂亮，特别适合高端专业软件的运行，因为这些软件在使用过程中，通常会需要许多调色窗口。但是，在生产制造方面，这又成了一个难题。另外，新的PowerBook还用一个塑料衬垫将钛制外壳分隔开来。复杂的内部框架，加上几个加固板，使整个机器的风格显得十分硬朗

仅在6周之内，乔纳森的团队就拿出了一个基本想法，之后苦干数月来攻克细节问题。PowerBook以显示屏上灵巧的锁闩为特色，合上盖子的时候锁闩会从内侧往下滑动，它像被施了魔法似的突然出现在用户面前，让用户眼前一亮。新用户会着迷地对盖子不断开合，只为了欣赏锁闩出现和消失的瞬间。
锁闩是指在PowerBook的主板上安放一小块磁铁，磁铁能把锁闩从盖子上的夹缝中吸出来。磁铁将被巧妙地运用在后续产品中，这种装置因此也预示着未来产品的到来，比如iPad
2。iPad 2中有一个带有磁性的Smart Cover智能保护盖，盖子一打开iPad
2就能启动；合上后就进入休眠状态。后来的一款采用纯平显示器的铝制iMac甚至是用磁铁连结显示屏，所以很容易就能看到其内部构造。

## 參考

  - [PowerBook page at
    Apple.com](http://www.apple.com/support/powerbook/)
  - The Genius Behind Apple's Greatest Products (乔纳森传)

[分類:筆記型電腦](../Page/分類:筆記型電腦.md "wikilink")

[Category:麦金塔](../Category/麦金塔.md "wikilink")