**B線第六大道快車**（），又稱**紐約地鐵B線**，是[紐約地鐵](../Page/紐約地鐵.md "wikilink")的[地鐵系統](../Page/地鐵.md "wikilink")。由於該線使用[IND第六大道線通過](../Page/IND第六大道線.md "wikilink")[曼哈頓主要地區](../Page/曼哈頓.md "wikilink")，因此其路線徽號為橙色\[1\]。

B線只在平日日間營運，來往[曼哈頓](../Page/曼哈頓.md "wikilink")[哈萊姆區](../Page/哈萊姆區.md "wikilink")[145街與](../Page/145街車站_\(IND第八大道線\).md "wikilink")[布魯克林](../Page/布魯克林.md "wikilink")[布萊頓海灘](../Page/布萊頓海灘車站_\(BMT布萊頓線\).md "wikilink")，在曼哈頓以快車營運（[34街及](../Page/34街-先驅廣場車站_\(IND第六大道線\).md "wikilink")[西四街](../Page/西四街車站_\(IND第六大道線\).md "wikilink")），在布魯克林和曼哈頓[145街與](../Page/145街車站_\(IND第八大道線\).md "wikilink")[59街以慢車營運](../Page/59街-哥倫布圓環車站_\(IND第八大道線\).md "wikilink")。繁忙時段，列車延長至超過145街，以[布朗克斯](../Page/布朗克斯.md "wikilink")[貝德福德公園林蔭路](../Page/貝德福德公園林蔭路車站_\(IND匯集線\).md "wikilink")，在布朗克斯以慢車營運。

B線以前幾乎完全在曼哈頓內營運，來往[華盛頓高地的](../Page/華盛頓高地_\(曼哈頓\).md "wikilink")[168街至](../Page/168街車站_\(IND第八大道線\).md "wikilink")[曼哈頓中城的](../Page/曼哈頓中城.md "wikilink")[34街-先驅廣場](../Page/34街-先驅廣場車站_\(IND第六大道線\).md "wikilink")。1967年，通車後，B線開始在布魯克林沿[BMT西城線](../Page/BMT西城線.md "wikilink")（慢車）及[BMT第四大道線](../Page/BMT第四大道線.md "wikilink")（快車）營運。1986年至1988年間受[曼哈頓大橋影響開行了黃色B線列車](../Page/曼哈頓大橋.md "wikilink")，在曼哈頓沿[BMT百老匯線營運](../Page/BMT百老匯線.md "wikilink")，在布魯克林沿BMT西城線營運；而原本的橙色B線列車則在168街和34街之間沿1967年以前的路線行走。1989年後，B線平日在[47-50街-洛克斐勒中心以北使用](../Page/47-50街-洛克斐勒中心車站_\(IND第六大道線\).md "wikilink")[IND第八大道線前往](../Page/IND第八大道線.md "wikilink")168街，晚上和周末則行走[IND
63街線](../Page/IND_63街線.md "wikilink")。深夜列車在西城線以接駁線形式營運。平日列車在1998年改道到匯集線，途經63街的非繁忙時段列車在2000年中止。B線自2004年起使用布萊頓線。

<div class="thumb tright" style="width:auto;">

<table>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><div class="thumbcaption">
<p><a href="../Page/R1_(紐約地鐵車輛).md" title="wikilink">R1</a><a href="../Page/方向幕.md" title="wikilink">方向幕</a></p>
</div></td>
<td></td>
</tr>
</tbody>
</table>

</div>

## 歷史

<div class="thumb tright" style="width:auto;">

<table>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><div class="thumbcaption">
<p>1967-1979年使用（圓形）</p>
</div></td>
</tr>
</tbody>
</table>

</div>

B線列車最早規劃為[曼哈頓](../Page/曼哈頓.md "wikilink")[華盛頓高地的快車](../Page/華盛頓高地_\(曼哈頓\).md "wikilink")，行駛於[中城的](../Page/中城_\(紐約曼哈頓\).md "wikilink")[IND第六大道線上](../Page/IND第六大道線.md "wikilink")。但1940年IND第六大道線通車及1967年[IND克莉絲蒂街連接道通車後](../Page/IND克莉絲蒂街連接道.md "wikilink")，只在尖峰行駛168街-華盛頓高地至34街-赫拉德廣場的慢車，快車則由BB線取代。

克莉絲蒂街連接道通車後，BB線列車曾有一段時間與[T線聯營行駛](../Page/紐約地鐵T線.md "wikilink")[BMT西側線](../Page/BMT西側線.md "wikilink")，自華盛頓高地行駛至康尼島。

曼哈頓橋曾於1986年與2004年關閉部份設施，B線亦將路線分成北B線（經由第六大道，稱橘B）與南B線（經由[BMT百老匯線](../Page/BMT百老匯線.md "wikilink")，稱黃B），前者大致走BB線，後者則走[T線](../Page/紐約地鐵T線.md "wikilink")。

1989年10月29日[IND63街線通車](../Page/IND63街線.md "wikilink")，B線於週末沿此線行駛至21街-皇后橋車站，一年後晚上時段亦加入。

1995年4月30日，曼哈頓橋北邊軌道關閉直到11月才再度開放，關閉期間內B線只行駛於太平洋街車站行駛至斯堤威爾車站，於BMT西側線行慢車、BMT第四大道現行快車。

1998年3月1日，B線與[C線北訖站對調](../Page/紐約地鐵C線.md "wikilink")，B線尖峰列車改走[IND匯集線](../Page/IND匯集線.md "wikilink")。

2000年起[IND63街線軌道與號誌維修](../Page/IND63街線.md "wikilink")，B線於非深夜時段改走中央公園西路至145街車站（尖峰至貝德福公園大道）。

2001年7月22日，曼哈頓橋再度關閉，B線採用近似於1986年的因應措施，但西側線的列車已更為[W線](../Page/紐約地鐵W線.md "wikilink")。B線於尖峰行駛至貝德福公園大道、中午及夜晚行駛至145街。

2004年2月22日，曼哈頓橋完全開放地鐵線路，B線沿大街車站跨過曼哈頓橋，進入[布魯克林](../Page/布魯克林.md "wikilink")，取代[菱形Q線至布來頓海灘](../Page/紐約地鐵Q線.md "wikilink")。

## 路線

### 服務形式

以下表格顯示B線所使用路線，特定時段在有陰影的格的路段內營運：\[2\]

<table>
<thead>
<tr class="header">
<th><p>路線</p></th>
<th><p>起點</p></th>
<th><p>終點</p></th>
<th><p>軌道</p></th>
<th><p>時段</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>平日</p></td>
<td><p>繁忙時段</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/IND匯集線.md" title="wikilink">IND匯集線</a></p></td>
<td><p><a href="../Page/貝德福德公園林蔭路車站_(IND匯集線).md" title="wikilink">貝德福德公園林蔭路</a></p></td>
<td><p><a href="../Page/155街車站_(IND匯集線).md" title="wikilink">155街</a></p></td>
<td><p>慢車</p></td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/145街車站_(IND匯集線).md" title="wikilink">145街</a></p></td>
<td><p>全部</p></td>
<td><p> </p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/IND第八大道線.md" title="wikilink">IND第八大道線</a></p></td>
<td><p><a href="../Page/135街車站_(IND第八大道線).md" title="wikilink">135街</a></p></td>
<td><p><a href="../Page/59街-哥倫布圓環車站_(IND第八大道線).md" title="wikilink">59街-哥倫布圓環</a></p></td>
<td><p>慢車</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/IND第六大道線.md" title="wikilink">IND第六大道線</a></p></td>
<td><p><a href="../Page/第七大道車站_(IND第六大道線).md" title="wikilink">第七大道</a></p></td>
<td><p><a href="../Page/百老匯-拉法葉街車站_(IND第六大道線).md" title="wikilink">百老匯-拉法葉街</a></p></td>
<td><p>快車</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/格蘭街車站_(IND第六大道線).md" title="wikilink">格蘭街</a></p></td>
<td><p>全部</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/曼哈頓大橋.md" title="wikilink">曼哈頓大橋</a></p></td>
<td><p>北</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/BMT布萊頓線.md" title="wikilink">BMT布萊頓線</a></p></td>
<td><p><a href="../Page/德卡爾布大道車站_(BMT布萊頓線).md" title="wikilink">德卡爾布大道</a></p></td>
<td><p><a href="../Page/布萊頓海灘車站_(BMT布萊頓線).md" title="wikilink">布萊頓海灘</a></p></td>
<td><p>快車</p></td>
<td></td>
</tr>
</tbody>
</table>

### 車站

更詳細的車站列表參見上方列出的路線。

<table style="width:154%;">
<colgroup>
<col style="width: 30%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 30%" />
<col style="width: 36%" />
<col style="width: 30%" />
</colgroup>
<thead>
<tr class="header">
<th><p><a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-B.svg" title="fig:NYCS-bull-trans-B.svg">NYCS-bull-trans-B.svg</a></p></th>
<th><p>中文站名</p></th>
<th><p>英文站名</p></th>
<th></th>
<th><p>轉乘</p></th>
<th><p>連接/備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong><a href="../Page/布朗克斯.md" title="wikilink">布朗克斯</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/IND匯集線.md" title="wikilink">匯集線</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/貝德福德公園林蔭路車站_(IND匯集線).md" title="wikilink">貝德福德公園林蔭路</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/王橋路車站_(IND匯集線).md" title="wikilink">王橋路</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>部分早上繁忙時段北行列車以此站為總站</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/福德漢姆路車站_(IND匯集線).md" title="wikilink">福德漢姆路</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/182-183街車站_(IND匯集線).md" title="wikilink">182-183街</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>以王橋路為總站的列車不停靠</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/特雷蒙特大道車站_(IND匯集線).md" title="wikilink">特雷蒙特大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/174-175街車站_(IND匯集線).md" title="wikilink">174-175街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/170街車站_(IND匯集線).md" title="wikilink">170街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/167街車站_(IND匯集線).md" title="wikilink">167街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/161街-洋基體育場車站_(IND匯集線).md" title="wikilink">161街-洋基體育場</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/IRT傑羅姆大道線.md" title="wikilink">IRT傑羅姆大道線</a>）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/曼哈頓.md" title="wikilink">曼哈頓</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/155街車站_(IND匯集線).md" title="wikilink">155街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/145街車站_(IND匯集線).md" title="wikilink">145街</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/IND第八大道線.md" title="wikilink">IND第八大道線</a>）</p></td>
<td><p>所有日間及黃昏列車的北端總站</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/IND第八大道線.md" title="wikilink">第八大道線</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/135街車站_(IND第八大道線).md" title="wikilink">135街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/125街車站_(IND第八大道線).md" title="wikilink">125街</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>往<a href="../Page/拉瓜迪亞機場.md" title="wikilink">拉瓜迪亞機場</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/116街車站_(IND第八大道線).md" title="wikilink">116街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/教堂公園道-110街車站_(IND第八大道線).md" title="wikilink">教堂公園道-110街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/103街車站_(IND第八大道線).md" title="wikilink">103街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/96街車站_(IND第八大道線).md" title="wikilink">96街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/86街車站_(IND第八大道線).md" title="wikilink">86街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/81街-自然歷史博物館車站_(IND第八大道線).md" title="wikilink">81街-自然歷史博物館</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/72街車站_(IND第八大道線).md" title="wikilink">72街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/59街-哥倫布圓環車站_(IND第八大道線).md" title="wikilink">59街-哥倫布圓環</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/IRT百老匯-第七大道線.md" title="wikilink">IRT百老匯-第七大道線</a>）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/IND第六大道線.md" title="wikilink">第六大道線</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/第七大道車站_(IND第六大道線).md" title="wikilink">第七大道</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/IND皇后林蔭路線.md" title="wikilink">IND皇后林蔭路線</a>）</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/47-50街-洛克斐勒中心車站_(IND第六大道線).md" title="wikilink">47-50街-洛克斐勒中心</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/42街-布萊恩特公園車站_(IND第六大道線).md" title="wikilink">42街-布萊恩特公園</a></p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Aiga_elevator.svg" title="fig:Aiga_elevator.svg">Aiga_elevator.svg</a></p></td>
<td><p><br />
（<a href="../Page/IRT法拉盛線.md" title="wikilink">IRT法拉盛線</a>，<a href="../Page/第五大道車站_(IRT法拉盛線).md" title="wikilink">第五大道</a>）</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/34街-先驅廣場車站_(IND第六大道線).md" title="wikilink">34街-先驅廣場</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/BMT百老匯線.md" title="wikilink">BMT百老匯線</a>）</p></td>
<td><p><br />
<a href="../Page/紐新航港局過哈德遜河捷運.md" title="wikilink">PATH</a>，<a href="../Page/33街車站_(PATH).md" title="wikilink">33街</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/西四街-華盛頓廣場車站_(IND第六大道線).md" title="wikilink">西四街-華盛頓廣場</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/IND第八大道線.md" title="wikilink">IND第八大道線</a>）</p></td>
<td><p><a href="../Page/紐新航港局過哈德遜河捷運.md" title="wikilink">PATH</a>，<a href="../Page/第9街車站_(PATH).md" title="wikilink">第9街</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/百老匯-拉法葉街車站_(IND第六大道線).md" title="wikilink">百老匯-拉法葉街</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/IRT萊辛頓大道線.md" title="wikilink">IRT萊辛頓大道線</a>，<a href="../Page/布利克街車站_(IRT萊辛頓大道線).md" title="wikilink">布利克街</a>）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/IND第六大道線.md" title="wikilink">克里斯蒂街支線</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/格蘭街車站_(IND第六大道線).md" title="wikilink">格蘭街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/布魯克林.md" title="wikilink">布魯克林</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/BMT布萊頓線.md" title="wikilink">布萊頓線</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/德卡爾布大道車站_(BMT布萊頓線).md" title="wikilink">德卡爾布大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/大西洋大道-巴克萊中心車站_(BMT布萊頓線).md" title="wikilink">大西洋大道-巴克萊中心</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/BMT第四大道線.md" title="wikilink">BMT第四大道線</a>）<br />
（<a href="../Page/IRT東公園道線.md" title="wikilink">IRT東公園道線</a>）</p></td>
<td><p><a href="../Page/長島鐵路.md" title="wikilink">長島鐵路</a>，</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/第七大道車站_(BMT布萊頓線).md" title="wikilink">第七大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/展望公園車站_(BMT布萊頓線).md" title="wikilink">展望公園</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/BMT富蘭克林大道線.md" title="wikilink">BMT富蘭克林大道線</a>）</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/教堂大道車站_(BMT布萊頓線).md" title="wikilink">教堂大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/紐科克廣場車站_(BMT布萊頓線).md" title="wikilink">紐科克廣場</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/金斯公路車站_(BMT布萊頓線).md" title="wikilink">金斯公路</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/羊頭灣車站_(BMT布萊頓線).md" title="wikilink">羊頭灣</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/布萊頓海灘車站_(BMT布萊頓線).md" title="wikilink">布萊頓海灘</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 外部連結

  - [MTA NYC Transit -
    路線介紹](https://web.archive.org/web/20061026045851/http://www.mta.info/nyct/service/bline.htm)
  - [B線歷史](https://web.archive.org/web/20011202055509/http://members.aol.com/bdmnqr2/linehistory.html)
  - [MTA NYC Transit —
    B線時刻表（PDF）](http://www.mta.info/nyct/service/pdf/tbcur.pdf)

[B](../Category/紐約地鐵服務路線.md "wikilink")

1.
2.