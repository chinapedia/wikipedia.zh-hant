[The_Sign_of_Yan_Chau_Tong_Marine_Park.JPG](https://zh.wikipedia.org/wiki/File:The_Sign_of_Yan_Chau_Tong_Marine_Park.JPG "fig:The_Sign_of_Yan_Chau_Tong_Marine_Park.JPG")

**印洲塘海岸公園**（）位於[香港](../Page/香港.md "wikilink")[船灣郊野公園的東北面](../Page/船灣郊野公園.md "wikilink")\[1\]\[2\]\[3\]，總面積約680[公頃](../Page/公頃.md "wikilink")。印洲塘海岸公園由兩個[海灣組成](../Page/海灣.md "wikilink")，比較少的位於[荔枝窩](../Page/荔枝窩.md "wikilink")，用連接[涌灣咀和](../Page/涌灣咀.md "wikilink")[九蘆頭北部的直線做界限](../Page/九蘆頭.md "wikilink")；另外一個海灣的兩條海上界線就分別連接著[西流江跟](../Page/西流江.md "wikilink")[石岩頭](../Page/石岩頭.md "wikilink")、[往灣洲的](../Page/往灣洲.md "wikilink")[老沙田與](../Page/老沙田.md "wikilink")[蕩排頭](../Page/蕩排頭.md "wikilink")。\[4\]

## 地理

印洲塘海岸公園的[吉澳海](../Page/吉澳海.md "wikilink")、[印洲塘和小島的](../Page/印洲塘.md "wikilink")[海岸線極為參差不齊](../Page/海岸線.md "wikilink")，形成了特別的地貌，包括[海灣](../Page/海灣.md "wikilink")、[岬角](../Page/岬角.md "wikilink")、[半島](../Page/半島.md "wikilink")、[陡崖和](../Page/陡崖.md "wikilink")[沙坑](../Page/沙坑.md "wikilink")。在每個沙灘之間，有很多[岩石和](../Page/岩石.md "wikilink")[島散佈](../Page/島.md "wikilink")。有一塊近[西流江的稱為](../Page/西流江.md "wikilink")[印洲](../Page/印洲.md "wikilink")，其形狀尤如[印般](../Page/印.md "wikilink")，此可能為印洲的名師來源。\[5\]

## 物種發現

早期的研究指出，荔枝窩與三椏涌曾經錄到[矮大葉藻](../Page/矮大葉藻.md "wikilink")、[喜鹽草和](../Page/喜鹽草.md "wikilink")[小喜鹽草這三種](../Page/小喜鹽草.md "wikilink")[海草](../Page/海草.md "wikilink")。這些海草床可以作為[哺育場](../Page/哺育場.md "wikilink")，提供一個藏身之所予[軟體動物跟](../Page/軟體動物.md "wikilink")[魚類的](../Page/魚類.md "wikilink")[幼體](../Page/幼體.md "wikilink")。除了作藏身之所外，海草亦都可以作為[海膽和](../Page/海膽.md "wikilink")[海龜等等](../Page/海龜.md "wikilink")[動物的](../Page/動物.md "wikilink")[食物](../Page/食物.md "wikilink")。\[6\]

除了海草之外，[紅樹在這個](../Page/紅樹.md "wikilink")[海岸公園物種比較繁多的地方生長](../Page/海岸公園.md "wikilink")。由於[紅樹林生長需要一些特定的環境和條件](../Page/紅樹林.md "wikilink")，如果[鹽濃度跜跟](../Page/鹽.md "wikilink")[潮水高低不一](../Page/潮水.md "wikilink")，它們就會[死亡](../Page/死亡.md "wikilink")。所以這些[動植物都要對嚴峻環境具備高度適應力](../Page/動植物.md "wikilink")。兩個有代表性的紅樹林分別位於荔枝窩和三椏村，兩個樹林[面積各大約](../Page/面積.md "wikilink")3[公頃](../Page/公頃.md "wikilink")。在荔枝窩可以找到[香港的記錄入面全數八種的真紅樹](../Page/香港.md "wikilink")，而在三椏村亦可找到5種。\[7\]

## 參考文獻

## 參見

  - [香港海岸公園](../Page/香港海岸公園.md "wikilink")

{{-}}

[Category:香港海岸公園](../Category/香港海岸公園.md "wikilink")
[海岸公園](../Category/印洲塘.md "wikilink") [Category:北區
(香港)](../Category/北區_\(香港\).md "wikilink")

1.

2.

3.

4.
5.
6.
7.