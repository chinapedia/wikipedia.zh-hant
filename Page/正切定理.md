[Trikotnik.png](https://zh.wikipedia.org/wiki/File:Trikotnik.png "fig:Trikotnik.png")

**正切定理**是[三角学中的一个](../Page/三角学.md "wikilink")[定理](../Page/定理.md "wikilink")。\[1\]根据该定理，在[平面](../Page/平面.md "wikilink")[三角形中](../Page/三角形.md "wikilink")，正切定理说明任意两条边的和除以第一条边减第二条边的差所得的商等于这两条边的对角的和的一半的[正切除以第一条边对角减第二条边对角的差的一半的正切所得的商](../Page/正切.md "wikilink")。即：

\[\frac{a-b}{a+b}=\frac{\mathrm{tan}\, \frac{\alpha -\beta }{2}}{\mathrm{tan}\, \frac{\alpha +\beta }{2}}\]

\[\frac{b-c}{b+c}=\frac{\mathrm{tan}\, \frac{\beta -\gamma }{2}}{\mathrm{tan}\, \frac{\beta +\gamma }{2}}\]

\[\frac{c-a}{c+a}=\frac{\mathrm{tan}\, \frac{\gamma -\alpha }{2}}{\mathrm{tan}\, \frac{\gamma +\alpha }{2}}\]

[法兰西斯·韦达](../Page/法兰西斯·韦达.md "wikilink")（François
Viète）曾在他对三角法研究的第一本著作《应用于三角形的数学法则》中提出正切定理。不过在沒有计算机的辅助求解三角形時，这定理可比[余弦定理更容易利用](../Page/余弦定理.md "wikilink")[对数來运算](../Page/对数.md "wikilink")[投影等问题](../Page/投影.md "wikilink")。

## 证明

由\(\frac{a+b}{a-b}\)开始，由[正弦定理得出](../Page/正弦定理.md "wikilink")

\[\begin{align}

\frac{a+b}{a-b} &= \frac{a\frac{\sin \alpha}{a} + b\frac{\sin \beta}{b}}{a\frac{\sin \alpha}{a} - b\frac{\sin \beta}{b}} \\

&= \frac{\sin(\alpha) + \sin(\beta)}{\sin(\alpha) - \sin(\beta)} \\

&= \frac{2\sin[\frac{1}{2}(\alpha+\beta)]\cos[\frac{1}{2}(\alpha-\beta)]}{2\cos[\frac{1}{2}(\alpha+\beta)]\sin[\frac{1}{2}(\alpha-\beta)]}\end{align}\]

  -

      -
        <small>*（参阅[三角恒等式](../Page/三角恒等式.md "wikilink")）*</small>

\[\begin{align}\frac{a+b}{a-b} &= \frac{\sin[\frac{1}{2}(\alpha+\beta)]\cos[\frac{1}{2}(\alpha-\beta)]}{\cos[\frac{1}{2}(\alpha+\beta)]\sin[\frac{1}{2}(\alpha-\beta)]} \\

&= \frac{\tan[\frac{1}{2}(\alpha+\beta)]}{\tan[\frac{1}{2}(\alpha-\beta)]}\end{align}\]

## 参见

  - [正切](../Page/正切.md "wikilink")
  - [畢氏定理](../Page/畢氏定理.md "wikilink")
  - [三角学](../Page/三角学.md "wikilink")
  - [三角函数](../Page/三角函数.md "wikilink")
  - [三角恒等式](../Page/三角恒等式.md "wikilink")
  - [正弦定理](../Page/正弦定理.md "wikilink")
  - [余弦定理](../Page/余弦定理.md "wikilink")

## 参考资料

[Category:三角学](../Category/三角学.md "wikilink")
[Category:几何定理](../Category/几何定理.md "wikilink")
[Category:三角形几何](../Category/三角形几何.md "wikilink")

1.  See [Eli Maor](../Page/Eli_Maor.md "wikilink"), *Trigonometric
    Delights*, [Princeton University
    Press](../Page/Princeton_University_Press.md "wikilink"), 2002.