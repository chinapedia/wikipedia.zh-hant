**大叶草目**又名洋二仙草目，是一类[显花植物](../Page/被子植物门.md "wikilink")，是2003年的《[APG II
分类法](../Page/APG_II_分类法.md "wikilink")》新分出来的一个[目](../Page/目.md "wikilink")，共包含有两个可以合并的[科](../Page/科.md "wikilink")。

  - 大叶草目
      - [大叶草科](../Page/大叶草科.md "wikilink")
      - [折扇叶科](../Page/折扇叶科.md "wikilink")（[香灌木科](../Page/香灌木科.md "wikilink")）

在[APG III 分类法和](../Page/APG_III_分类法.md "wikilink")[APG IV
分类法中](../Page/APG_IV_分类法.md "wikilink")，本目依然包含此两科。

在1981年的《[克朗奎斯特分类法](../Page/克朗奎斯特分类法.md "wikilink")》中，大叶草科被分到[小二仙草目中](../Page/小二仙草目.md "wikilink")，而折扇叶科被分到[金缕梅目中](../Page/金缕梅目.md "wikilink")。

## 参考文献

## 外部链接

  - [NCBI分类法](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Tree&id=232382&lvl=3&p=mapview&p=has_linkout&p=blast_url&p=genome_blast&lin=f&keep=1&srchmode=1&unlock)

[\*](../Category/大叶草目.md "wikilink")