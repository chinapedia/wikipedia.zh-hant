**威尔士**（〔發音：〕；〔〕），-{zh-cn:在台灣與港澳譯為**威爾斯**;zh-hant:在中國又譯為**威爾士**;}-，位于[大不列顛島西南部](../Page/大不列顛島.md "wikilink")，为[大不列顛與北愛爾蘭聯合王國構成國之一](../Page/英國的構成國.md "wikilink")，东界[英格蘭](../Page/英格蘭.md "wikilink")，西临[圣乔治海峡](../Page/聖佐治海峽.md "wikilink")，南面[布里斯托尔湾](../Page/布里斯托尔湾.md "wikilink")，北靠[爱尔兰海](../Page/爱尔兰海.md "wikilink")，[卡迪夫是其首都與最大城市](../Page/卡迪夫.md "wikilink")。

## 历史

威尔士王国的历史要比英格兰更加古老。就现有资料来看，公元前1000年时，就有[凯尔特人从欧洲大陆中部来到这里](../Page/凯尔特人.md "wikilink")。此后，威尔士的西北部地区最早出现称霸势力，其祖先是[西留尔人](../Page/西留尔人.md "wikilink")；西南方被爱尔兰人侵占，建立[布莱秦尼奥克王国](../Page/布莱秦尼奥克王国.md "wikilink")；中部地区的主要势力则是科诺伟部族。这些势力，后来都被[罗马帝国消灭](../Page/罗马帝国.md "wikilink")。

公元43年－410年，威尔士是罗马帝国[不列颠尼亚行省的一部分](../Page/不列颠尼亚.md "wikilink")。后因日耳曼蛮族侵犯欧洲，罗马帝国被迫抽调不列颠尼亚的军队前去迎战，致当地防守空虚，东南沿岸屡屡遭受[苏格兰地区](../Page/苏格兰.md "wikilink")[皮克特人的侵扰](../Page/皮克特人.md "wikilink")。当地的罗马贵族雇佣[日耳曼人中的](../Page/日耳曼人.md "wikilink")[盎格鲁人和](../Page/盎格鲁人.md "wikilink")[撒克逊人帮忙防守](../Page/撒克逊人.md "wikilink")，谁知这些外籍兵突然哗变，反而开始攻打罗马贵族的统治区；而欧洲大陆的罗马帝国则对这一情况视而不见，不肯派兵救援。如此混战到公元6世纪初，[不列颠人陆续退往岛西威尔士地区的群山之中](../Page/不列颠人.md "wikilink")，其中的[布立吞人开始自称威尔士人](../Page/布立吞人.md "wikilink")，同时建立[格温内斯王国](../Page/格温内斯王国.md "wikilink")（Kingdom
of Gwynedd）这一罗马帝国继承国，其国王享有“不列颠人之王”（King of the
Britons）的尊称。威尔士人不断抗击盎格鲁-撒克逊人一事，孕育了[亞瑟王傳說](../Page/亞瑟王傳說.md "wikilink")。

此后，英格兰地区由[盎格鲁-撒克逊人主导](../Page/盎格鲁-撒克逊人.md "wikilink")，开始所谓“[七国割据](../Page/七國時代.md "wikilink")”的历史阶段。威尔士则同样群雄林立，格温内斯王国和先后出现的[德赫巴斯王国](../Page/德赫巴斯王国.md "wikilink")（Deheubarth）、[格温特王国](../Page/格温特王国.md "wikilink")（Kingdom
of
Gwent）、[布雷切尼奥格王国](../Page/布雷切尼奥格王国.md "wikilink")（Brycheiniog）互有胜负。公元784年，英格兰七国之一的[麦西亚国王奥发](../Page/麦西亚王国.md "wikilink")（Offa）在英格兰和威尔士之间的海域筑堤，这个[奥发堤坝是两地间第一个永久性](../Page/奥发堤坝.md "wikilink")[边界](../Page/边界.md "wikilink")。

大概从公元865年开始，[维京人渡海攻打不列颠岛](../Page/维京人.md "wikilink")，称霸东南地带，直到878年方被英格兰南部[威塞克斯王国的](../Page/威塞克斯王国.md "wikilink")[阿尔弗雷德大王挥师击破](../Page/阿尔弗雷德大王.md "wikilink")，订立和约。阿尔弗雷德不断进取，将势力范围推至英格兰的东部和北部，后更以[英格兰国王自称](../Page/英格兰国王.md "wikilink")。其子爱德华（[长者爱德华](../Page/长者爱德华.md "wikilink")）继承父业，几乎将维京人彻底赶出英格兰地区，只留下[诺森布里亚南部的](../Page/诺森布里亚.md "wikilink")[约克地区尚存一息](../Page/约克.md "wikilink")，史称[斯堪的纳维亚约克王国](../Page/斯堪的纳维亚约克王国.md "wikilink")。威尔士的小国王纷纷向他称臣。

公元924年，长者爱德华去世，其继承者[艾塞斯坦灭亡约克的维京人王国](../Page/光荣者艾塞斯坦.md "wikilink")。公元927年7月12日，苏格兰国王和格温内斯国王等出席会议，都表示追随艾塞斯坦。会后，艾塞斯坦陈兵[赫德福德地区的](../Page/赫德福德.md "wikilink")[威河沿岸](../Page/威河.md "wikilink")，迫使威尔士其余割据势力向他称臣纳贡，同时首次规定以威河作为威尔士和英格兰的边境。威尔士由此得到一段和平安稳的时光，诸侯基本服从格温内斯国王指挥。

公元1066年，诺曼底公爵[威廉一世征服](../Page/威廉一世_\(英格兰\).md "wikilink")[英格兰](../Page/英格兰.md "wikilink")，建立[诺曼底王朝](../Page/诺曼底王朝.md "wikilink")。他援引艾塞斯坦的史事降伏威尔士诸侯。由此以降，威尔士诸侯长久附庸英格兰国王。

公元1216年，格温内斯王国的[罗埃林大王](../Page/罗埃林大王.md "wikilink")（Llywelyn the
Great）征得英格兰国王[亨利三世同意](../Page/亨利三世.md "wikilink")，把威尔士境内的全部王国撤销，同时成立[威尔士公国](../Page/威尔士公国.md "wikilink")（Principality
of Wales）并由罗埃林大王统治。

公元1277年，亨利三世的后继者[爱德华一世动兵攻打威尔士](../Page/爱德华一世.md "wikilink")，至1284年征服全境，同年颁行“[威尔士法](../Page/威尔士法.md "wikilink")”。爱德华一世接受威尔士人的要求，同意由一位在威尔士出生、不会讲英语、生下来第一句话说威尔士语的亲王来管理威尔士人。他把即将分娩的王后接到威尔士，生下的王子便是第一位“[威尔士亲王](../Page/威尔士亲王.md "wikilink")”[爱德华二世](../Page/爱德华二世.md "wikilink")。由此以降，英格兰国王和后来的[大不列颠国王总是把](../Page/大不列颠国王.md "wikilink")“威尔士亲王”的头衔赐给长子。久而久之，这个头衔就成了“英国王储”的同义词。

公元1400年9月16日，[波厄斯亲王](../Page/波厄斯亲王.md "wikilink")[欧文·格兰道尔不满英格兰国王亨利四世的统治](../Page/欧文·格兰道尔.md "wikilink")，公开叛乱，并于1404年自封威尔士亲王。这是历史上最后一位持有威尔士亲王头衔的威尔士人。1412年，他被英格兰军队打败，下落不明。此事直接加强了英格兰对威尔士的管理。

公元1536年，《1535年威爾士法律法》（Laws in Wales Act
1535）通过。威尔士从此受到英格兰法律管束，政治上则彻底联合。英语成为威尔士的官方语言之一。

## 地理

[Map_of_Wales.svg](https://zh.wikipedia.org/wiki/File:Map_of_Wales.svg "fig:Map_of_Wales.svg")

威爾斯在西[大不列顛島的西南部](../Page/大不列顛島.md "wikilink")，由[半島和近海](../Page/半島.md "wikilink")[島嶼組成](../Page/島嶼.md "wikilink")，最大島是[安格尔西岛](../Page/安格尔西岛.md "wikilink")，威爾斯東面與[英格蘭為邊界](../Page/英格蘭.md "wikilink")，另外三面環海：西臨[聖喬治海峽與](../Page/聖喬治海峽.md "wikilink")[卡迪根灣](../Page/卡迪根灣.md "wikilink")，南面[布里斯托灣](../Page/布里斯托爾灣.md "wikilink")，北靠[愛爾蘭海](../Page/愛爾蘭海.md "wikilink")。全長274公里，寬97公里，總面積2萬0779平方公里。擁有超過1200公里長的[海岸線](../Page/海岸線.md "wikilink")。

威爾斯是多[山地形](../Page/山.md "wikilink")，主要分布在三個區域：西北的[斯諾登尼亞地區](../Page/斯諾登尼亞.md "wikilink")（1951年被指定為[國家公園](../Page/國家公園.md "wikilink")）、威爾斯中部的[坎布里安山脉](../Page/坎布里安山脉.md "wikilink")、南部的[布雷肯](../Page/布雷肯.md "wikilink")。威爾斯山區是過去的[冰河時期所形成的特殊地形](../Page/冰河時期.md "wikilink")。斯諾登尼亞地區擁有最高峰，是[斯諾登山](../Page/斯諾登山.md "wikilink")。

[斯諾登山](../Page/斯諾登山.md "wikilink")[海拔](../Page/海拔.md "wikilink")1085米（3560[英呎](../Page/英呎.md "wikilink")），[相對高度](../Page/相對高度.md "wikilink")1038米，威爾斯第1高山，[英國第](../Page/英國.md "wikilink")3高山，[不列顛群島第](../Page/不列顛群島.md "wikilink")4高山。

## 人口

根據2005年5月的統計，威爾士共有2,903,085人。

## 行政区

### 1965年

  - 1965年，当时有13郡：

<table>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:WalesTradNumbered.png" title="fig:WalesTradNumbered.png">WalesTradNumbered.png</a></p></td>
<td><ol>
<li>蒙茅斯郡（格温特）</li>
<li>格拉摩根郡</li>
<li>卡马森郡</li>
<li>彭布魯克郡</li>
<li>卡迪根郡（Cardigan）</li>
<li>布雷克诺克郡（Brecknock）</li>
<li>雷德诺郡（Radnor）</li>
<li>蒙哥马利郡（Montgomery）</li>
<li>丹比夫郡</li>
<li>弗林特郡</li>
<li>麥里昂斯郡（Merioneth）</li>
<li>卡那封郡（Caernarfon）</li>
<li>安格魯西郡</li>
</ol></td>
</tr>
</tbody>
</table>

### 1974年

  - 1974年，格拉摩根郡分为三郡，11郡合为4郡，格温特郡不变；形成8郡：

<table>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Map_Cymru_gyda_rhifau.svg" title="fig:Map_Cymru_gyda_rhifau.svg">Map_Cymru_gyda_rhifau.svg</a></p></td>
<td><ol>
<li>蒙茅斯郡（格温特郡）</li>
<li>南格拉摩根郡（De Morgannwg）</li>
<li>中格拉摩根郡（Morgannwg Ganol）</li>
<li>西格拉摩根郡（Gorllewin Morgannwg）</li>
<li>达费德郡</li>
<li>波厄斯郡</li>
<li>格温内斯郡</li>
<li>克卢伊德郡</li>
</ol></td>
</tr>
</tbody>
</table>

### 1996年

  - 目前分为22一元行政单位（unitary authorities），其中包括郡（Counties）、城市郡（city and
    Cities \*）、郡级自治市镇（County Boroughs †）

[Map_Cymru_1996_gyda_rhifau.svg](https://zh.wikipedia.org/wiki/File:Map_Cymru_1996_gyda_rhifau.svg "fig:Map_Cymru_1996_gyda_rhifau.svg")

| 順序   | 行政區名稱                                                  | 面積（km²） | 人口（2007） | 密度（人/km²）   |
| ---- | ------------------------------------------------------ | ------- | -------- | ----------- |
| 1\.  | 梅瑟蒂德菲尔（Merthyr Tydfil）†                                | 111     | 55,600   | 501 / km²   |
| 2\.  | 卡菲利（Caerphilly）†                                       | 278     | 171,800  | 618 / km²   |
| 3\.  | 布莱諾格温特（Blaenau Gwent）†                                 | 109     | 69,200   | 635 / km²   |
| 4\.  | [托爾芬](../Page/托爾芬.md "wikilink")（Torfaen）†             | 126     | 91,100   | 723 / km²   |
| 5\.  | 蒙茅斯郡（Monmouthshire）                                    | 850     | 88,200   | 104 / km²   |
| 6\.  | [纽波特/新港](../Page/纽波特.md "wikilink")（Newport）\*         | 190     | 140,200  | 738 / km²   |
| 7\.  | [卡迪夫](../Page/卡迪夫.md "wikilink")（Cardiff）\*            | 140     | 321,000  | 4,392 / km² |
| 8\.  | 格拉摩根谷（Vale of Glamorgan）†                              | 335     | 124,000  | 370 / km²   |
| 9\.  | 布里真德（Bridgend）†                                        | 246     | 133,900  | 544 / km²   |
| 10\. | 朗达卡农塔夫（Rhondda Cynon Taff）†                            | 424     | 233,700  | 551 / km²   |
| 11\. | 下塔尔波特港（Neath Port Talbot）†                             | 442     | 137,400  | 331 / km²   |
| 12\. | [斯旺西](../Page/斯旺西.md "wikilink")（Swansea）\*            | 378     | 228,100  | 601 / km²   |
| 13\. | 卡马森郡（Carmarthenshire）                                  | 2,395   | 179,500  | 75 / km²    |
| 14\. | 锡尔迪金（Ceredigion）                                       | 1,795   | 77,800   | 43 / km²    |
| 15\. | 波厄斯（Powys）                                             | 5,196   | 132,000  | 25 / km²    |
| 16\. | [域斯咸](../Page/雷克瑟姆.md "wikilink")（Wrexham）†            | 498     | 131,900  | 265 / km²   |
| 17\. | 弗林特郡（Flintshire）                                       | 438     | 150,500  | 344 / km²   |
| 18\. | 登比郡（Denbighshire）                                      | 844     | 97,000   | 115 / km²   |
| 19\. | 康威（Conwy）†                                             | 1,130   | 111,700  | 99 / km²    |
| 20\. | 格温内斯（Gwynedd）                                          | 2,548   | 118,400  | 46 / km²    |
| 21\. | [安格尔西岛](../Page/安格尔西岛.md "wikilink")（Isle of Anglesey） | 714     | 69,000   | 97 / km²    |
| 22\. | 彭布魯克郡（Pembrokeshire）                                   | 1,590   | 117,900  | 74 / km²    |

无

## 媒體

### 廣播

  - **電視**
      - [BBC威尔士](../Page/BBC威尔士.md "wikilink")，總部在首府[卡迪夫](../Page/卡迪夫.md "wikilink")
      - [ITV威爾斯&西](../Page/ITV威爾士和西部.md "wikilink")（ITV Wales &
        West），總部在首府[加地夫郊區](../Page/加地夫.md "wikilink")
      - [S4C](../Page/威爾斯第四台.md "wikilink")，總部在首府[加地夫](../Page/加地夫.md "wikilink")，屬於第四頻道管理局

<!-- end list -->

  - **電台**
      - 英國廣播公司英語威尔士（）
      - BBC電台Cymru（）
      - 國家廣播電台（）
      - 錫爾迪電台（），設在[亞伯里斯威斯](../Page/阿伯里斯特威斯.md "wikilink")
      - 斯溫西之音（），範圍在[斯溫西及南威尔士](../Page/斯溫西.md "wikilink")
      - 102.1斯溫西灣電台（），設在尼斯
      - FM96.4
      - FM103.2 & 97.4（紅龍調頻）
      - 時事廣播（），在首府[卡迪夫](../Page/卡迪夫.md "wikilink")
      - 加地夫電台（）
      - 黃金電台（），在首府[卡迪夫](../Page/卡迪夫.md "wikilink")

### 報紙

  -
  -
  -
  -
## 交通

[BR_Class_175_at_Llandudno_Junction.jpg](https://zh.wikipedia.org/wiki/File:BR_Class_175_at_Llandudno_Junction.jpg "fig:BR_Class_175_at_Llandudno_Junction.jpg")
[Second_Severn_crossing.jpg](https://zh.wikipedia.org/wiki/File:Second_Severn_crossing.jpg "fig:Second_Severn_crossing.jpg")
[CardiffAirport1.jpg](https://zh.wikipedia.org/wiki/File:CardiffAirport1.jpg "fig:CardiffAirport1.jpg")

### 鐵路

  - [北威爾斯海岸線](../Page/威爾斯北海岸線.md "wikilink")

  -   - \- 屬於南威爾斯主幹線的支線。

  - \- 卡迪夫至南威爾斯谷地各城鎮的[城市軌道](../Page/區域鐵路.md "wikilink")，共有8條主線，2條周邊路線。

  - [泰爾依鐵道](../Page/泰爾依鐵道.md "wikilink") - 歷史鐵道。

### 公路

  - [高速公路](../Page/高速公路.md "wikilink")，大部分在南威爾斯：
      - [M4高速公路](../Page/英國M4高速公路.md "wikilink")：東-西向，由威尔士[卡馬森郡至](../Page/卡馬森.md "wikilink")[英格蘭](../Page/英格蘭.md "wikilink")[倫敦](../Page/倫敦.md "wikilink")，全長305公里，威爾斯境內約133公里，有27個匝道路口。

      - ︰東-西向，由威尔士[蒙茅斯郡至](../Page/蒙茅斯.md "wikilink")[英格蘭](../Page/英格蘭.md "wikilink")[格洛斯特郡南](../Page/格洛斯特郡.md "wikilink")，全長19.3公里。

      - ︰東-西向，由威尔士[卡迪夫經](../Page/卡迪夫.md "wikilink")[紐波特](../Page/紐波特.md "wikilink")（新港）最後至St
        Mellons，全長3.2公里，這也是唯一一條完全在威爾斯的[高速公路](../Page/高速公路.md "wikilink")。

<!-- end list -->

  - 主要道路：
      - ︰東南-西北向，由[英格蘭](../Page/英格蘭.md "wikilink")[西敏至北威爾斯](../Page/西敏_\(倫敦地區\).md "wikilink")[安格爾西島](../Page/安格爾西島.md "wikilink")。

      - （又稱**北威爾斯高速公路**）︰東-西向，由[英格蘭](../Page/英格蘭.md "wikilink")[切斯特至北威爾斯](../Page/切斯特.md "wikilink")[安格爾西島](../Page/安格爾西島.md "wikilink")，全長141公里。

      - （通稱**元首山谷道**）︰南-北向，由南威爾斯[卡地夫灣至北威爾斯](../Page/卡地夫.md "wikilink")[康威的蘭迪德諾](../Page/康威.md "wikilink")，全長299公里。

      - ︰東-西向，由南威爾斯下塔爾波特港的Llandarcy至[英格蘭](../Page/英格蘭.md "wikilink")[赫里福德郡的Bromyard](../Page/赫里福德郡.md "wikilink")，全長66公里。

      - ︰南-北向，由弗林特郡的Queensferry至圭內德的[多爾蓋萊](../Page/多爾蓋萊.md "wikilink")，全長58公里，皆在北威爾斯。

### 機場

威爾斯擁有兩座機場，一南一北；南部為[卡地夫國際機場](../Page/卡迪夫機場.md "wikilink")，是威爾斯唯一的國際機場，有四分之三的旅客經由此機場從威爾斯往返世界各地；北部為，為英國國內線的機場，規模與市場較小。

### 港口

在威爾斯，米爾福德港（Milford Haven）是石油產品的主要運輸港口；新港（紐波特）為鋼鐵運輸最繁忙的港口；塔爾伯特港（Port
Talbot）則是礦石的運輸港口，運礦噸量在英國排名第三。以下為威爾斯各港口依2005年的港口吞吐量所佔的比例：

  - 米爾福德港- 63.7 ％
  - 塔爾伯特港- 14.5 ％
  - 霍利黑德- 7 ％
  - 新港- 6.7 ％
  - 卡迪夫- 4.2 ％
  - 斯旺西- 1.2 ％
  - 菲什加德- 0.9 ％
  - 巴里- 0.8 ％
  - 尼思- 0.7 ％
  - Mostyn - 0.3 ％

### 樂器

[克魯斯琴是威爾斯最古老的傳統樂器之一](../Page/克魯斯琴.md "wikilink")。

## 体育

  - [威爾斯足球代表隊](../Page/威爾斯足球代表隊.md "wikilink")
  - [威爾士橄榄球隊](../Page/威爾士橄榄球隊.md "wikilink")

## 教育

  - [卡迪夫大學](../Page/卡迪夫大學.md "wikilink")
  - [卡迪夫都会大学](../Page/卡迪夫都会大学.md "wikilink")
  - [阿伯里斯特威斯大學](../Page/阿伯里斯特威斯大學.md "wikilink")
  - [斯旺西大学](../Page/斯旺西大学.md "wikilink")
  - [班戈大学](../Page/班戈大学.md "wikilink")
  - [南威尔士大学](https://en.wikipedia.org/wiki/University_of_South_Wales)
  - [格林多大学](../Page/格林多大学.md "wikilink")

## 注释

## 参考文献

## 外部連結

  - [Wales - World
    Nation](http://www.walesworldnation.com/server.php?show=nav.8581)
  - [WWW.XZQH（行政区划）威尔士网页](https://web.archive.org/web/20051222043831/http://www.xzqh.org/waiguo/europe/20243.htm)
  - [National Assembly for Wales](http://www.wales.gov.uk)
  - [Welsh Icons - About Wales and all things
    Welsh](http://www.welshicons.org.uk/)
  - [Myths of British
    ancestory](http://www.prospect-magazine.co.uk/article_details.php?id=7817)
  - [BBC Wales](http://www.bbc.co.uk/wales/)
  - <https://web.archive.org/web/20070620231938/http://www.llywelyn.co.uk/>
  - [WalesCymru.com](http://www.walescymru.com)
  - [100 Welsh
    Heroes](https://web.archive.org/web/20140413214449/http://www.100welshheroes.com/en/homepage)
  - [Wales Council for Voluntary Action](http://www.wcva.org.uk)
  - [Gathering the Jewels - Welsh Heritage and
    Culture](https://web.archive.org/web/20071119123113/http://www.gtj.org.uk/)
  - [The castles and history of Wales](http://www.castlewales.com)
  - [The medieval history of Wales](http://www.castles99.ukprint.com)
  - [What's on Wales](http://www.whatsonwales.co.uk)

{{-}}

[Category:英国的构成国](../Category/英国的构成国.md "wikilink")
[威爾斯](../Category/威爾斯.md "wikilink")
[Category:大不列颠](../Category/大不列颠.md "wikilink")