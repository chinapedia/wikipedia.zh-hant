**Java EE**，Java平台企业版（Java Platform Enterprise Edition），之前称为Java 2
Platform, Enterprise Edition (J2EE)，2018年3月更名为**Jakarta
EE**。\[1\]是[Sun公司为企业级应用推出的标准](../Page/Sun.md "wikilink")[平台](../Page/系統平台.md "wikilink")。Java平台共分为三个主要版本**Java
EE**、[Java SE和](../Page/Java_SE.md "wikilink")[Java
ME](../Page/Java_ME.md "wikilink")。

[Sun公司在](../Page/Sun.md "wikilink")1998年发表[JDK](../Page/JDK.md "wikilink")1.2版本的时候，使用了新名称Java
2 Platform，即“Java2平台”，修改后的[JDK称为Java](../Page/JDK.md "wikilink") 2
Platform Software Developing
Kit，即[J2SDK](../Page/J2SDK.md "wikilink")。并分为标准版（Standard
Edition，[J2SE](../Page/J2SE.md "wikilink")），企业版（Enterprise
Edition，J2EE），微型版（MicroEdition，[J2ME](../Page/J2ME.md "wikilink")）。J2EE便由此诞生。

2005年6月，JavaOne大会召开，SUN公司公开Java SE
6。此时，Java的各种版本已经更名以取消其中的数字“2”：J2EE更名为Java
EE, [J2SE更名为](../Page/J2SE.md "wikilink")[Java
SE](../Page/Java_SE.md "wikilink")，[J2ME更名为](../Page/J2ME.md "wikilink")[Java
ME](../Page/Java_ME.md "wikilink")。

随着[Java技术的发展](../Page/Java.md "wikilink")，J2EE平台得到了迅速的发展，成为[Java语言中最活跃的体系之一](../Page/Java.md "wikilink")。现如今，J2EE不仅仅是指一种标准平台，它更多的表达着一种[软件架构和设计思想](../Page/软件架构.md "wikilink")。

## 组件

Java EE是一系列技术标准所组成的平台，包括：

  - [Applet](../Page/Applet.md "wikilink") - Java Applet
  - [EJB](../Page/EJB.md "wikilink") -
    企业级[JavaBean](../Page/JavaBean.md "wikilink")（Enterprise Java
    Beans）
  - [JAAS](../Page/JAAS.md "wikilink") - Java Authentication and
    Authorization Service
  - [JACC](../Page/JACC.md "wikilink") - J2EE Authorization Contract for
    Containers
  - [JAF](../Page/JAF.md "wikilink") - Java Beans Activation Framework
  - [JAX-RPC](../Page/JAX-RPC.md "wikilink") - Java API for XML-Based
    Remote Procedure Calls
  - [JAX-WS](../Page/JAX-WS.md "wikilink") - Java API for XML Web
    Services
  - [JAXM](../Page/JAXM.md "wikilink") - Java API for XML Messaging
  - [JAXP](../Page/JAXP.md "wikilink") - Java XML解析API（Java API for XML
    Processing）
  - [JAXR](../Page/JAXR.md "wikilink") - Java API for XML Registries
  - [JCA](../Page/JCA.md "wikilink") - J2EE连接器架构（J2EE Connector
    Architecture）
  - [JDBC](../Page/JDBC.md "wikilink") - Java数据库联接（Java Database
    Connectivity）
  - [JMS](../Page/JMS.md "wikilink") - Java消息服务（Java Message Service）
  - [JMX](../Page/JMX.md "wikilink") - Java Management
  - [JNDI](../Page/JNDI.md "wikilink") - Java名稱与目录接口（Java Naming and
    Directory Interface）
  - [JSF](../Page/JSF.md "wikilink") - Java Server Faces
  - [JSP](../Page/JSP.md "wikilink") - Java服务器页面（Java Server Pages）
  - [JSTL](../Page/JSTL.md "wikilink") - Java服务器页面标准标签库（Java Server
    Pages Standard Tag Library）
  - [JTA](../Page/JTA.md "wikilink") - Java事务API（Java Transaction API）
  - [JavaMail](../Page/JavaMail.md "wikilink")
  - [Servlet](../Page/Servlet.md "wikilink") - Java Servlet
    [API](../Page/API.md "wikilink")
  - [StAX](../Page/StAX.md "wikilink") - Streaming APIs for XML Parsers
  - [WS](../Page/WS.md "wikilink") - Web Services

## 版本历史

  - J2EE 1.2 (1999年12月12日)
  - J2EE 1.3 (2001年9月24日)
  - J2EE 1.4 (2003年11月11日)
  - Java EE 5 (2006年5月11日)
  - Java EE 6 (2009年12月10日)
  - Java EE 7 (2013年5月28日)
  - Java EE 8 (2017年8月31日)

## 参见

  - [Web容器](../Page/Web容器.md "wikilink")
  - [部署描述符](../Page/部署描述符.md "wikilink")
  - [Java BluePrints](../Page/Java_BluePrints.md "wikilink")
  - [Sun Java System Portal
    Server](../Page/Sun_Java_System_Portal_Server.md "wikilink")
  - [Java EE version
    history](../Page/Java_EE_version_history.md "wikilink")
  - [Sun Community Source
    License](../Page/Sun_Community_Source_License.md "wikilink")
  - [Java Research License](../Page/Java_Research_License.md "wikilink")

## 参考文献

## 外部链接

  - [Oracle Technology Network's Java
    EE](http://www.oracle.com/technetwork/java/javaee/overview/index.html)
  - [Oracle's Java EE Compatibility
    page](http://www.oracle.com/technetwork/java/javaee/overview/compatibility-jsp-136984.html)
    - Certified Java EE Compatible Implementations
  - [Core J2EE
    Patterns](http://java.sun.com/blueprints/corej2eepatterns/Patterns/index.html)
  - [A short tutorial introducing beginning Java EE developers to the
    Java EE
    platform](http://docs.oracle.com/javaee/6/firstcup/doc/index.html)
  - [Cloud Tutorial - Java EE in a
    Day](http://www.turngeek.press/javaeeinaday) Java EE Tutorial that
    embraces the use of a Cloud IDE to let you learn the fundamentals of
    Java EE in just one day

{{-}}

[Category:计算平台](../Category/计算平台.md "wikilink")
[Category:Java平台](../Category/Java平台.md "wikilink")
[Category:Java规范请求](../Category/Java规范请求.md "wikilink")
[Java企业平台](../Category/Java企业平台.md "wikilink")
[Category:Web应用框架](../Category/Web应用框架.md "wikilink")

1.