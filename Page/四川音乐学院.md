**四川音樂學院**（簡稱**川音**）位於中國[成都市城南](../Page/成都市.md "wikilink")，創建於1939年，前身為**四川省立戲劇音樂實驗學校**，1959年6月改為現名，是中國最知名的音樂學院之一，首任院長[常蘇民先生](../Page/常蘇民.md "wikilink")。現任院長易柯教授，四川音乐学院作曲專業畢業，2011年起上任\[1\]。

## 校園

四川音乐学院由新、老兩個校區組成。老校區位於[成都市中心](../Page/成都市.md "wikilink")，北依[錦江](../Page/錦江_\(岷江支流\).md "wikilink")，南臨[四川大學](../Page/四川大學.md "wikilink")，佔地116畝。新校區位於[成都市新都區](../Page/成都市.md "wikilink")，距中心16公里，佔地1040畝。學院有學生10,000人，教職工1240人，專任教師810人\[2\]。

學院下設**川音綿陽藝術學院**，**川音通俗音樂學院**，**川音成都美術學院**，**川音國際演藝學院**，**川音附中**等教學單位。

## 著名校友\[3\]

  - [劉曉慶](../Page/劉曉慶.md "wikilink") - 1963年考入四川音乐学院附中，1970年畢業，著名演員。
  - [李雲迪](../Page/李雲迪.md "wikilink") -
    1994年考入四川音乐学院附中，後轉讀[深圳藝術學校](../Page/深圳藝術學校.md "wikilink")，第14屆[蕭邦國際鋼琴比賽第一名](../Page/蕭邦國際鋼琴比賽.md "wikilink")。
  - [李宇春](../Page/李宇春.md "wikilink") -
    2002年考入四川音乐学院通俗系，[2005年超級女聲全國總冠軍](../Page/2005年超級女聲.md "wikilink")。
  - [何潔](../Page/何潔.md "wikilink") - 2004年考入四川音乐学院通俗系，2005年超級女聲全國第4名。
  - [譚維維](../Page/譚維維.md "wikilink") -
    1998年考入四川音乐学院聲樂系，[2006年超級女聲全國亞軍](../Page/2006年超級女聲.md "wikilink")，2010年被聘為川音副教授。
  - [阿兰·达瓦卓玛](../Page/阿兰·达瓦卓玛.md "wikilink") -
    1997年考入四川音乐学院附中，現簽約[日本](../Page/日本.md "wikilink")[Avex并在日本發展的歌手](../Page/Avex.md "wikilink")。
  - [張力尹](../Page/張力尹.md "wikilink") -
    2000年考入四川音乐学院附中，現簽約[韓國](../Page/韓國.md "wikilink")[SM
    Entertainment并在韓國發展的](../Page/SM_Entertainment.md "wikilink")[R\&B歌手](../Page/R&B.md "wikilink")。
  - [魏晨](../Page/魏晨.md "wikilink") -
    2004年考入四川音乐学院流行歌舞系，2007年[快樂男聲全國第](../Page/快樂男聲.md "wikilink")3名。
  - [王錚亮](../Page/王錚亮.md "wikilink") -
    畢業于四川音乐学院手風琴專業，2007年快樂男聲全國第10名，2010年被聘為川音副教授。
  - [馬雪陽](../Page/馬雪陽.md "wikilink") -
    畢業于四川音乐学院小提琴專業，[天娛傳媒男子團體](../Page/天娛傳媒.md "wikilink")[至上勵合成員](../Page/至上勵合.md "wikilink")。
  - [潘虹樾](../Page/潘虹樾.md "wikilink") -
    2008年考入四川音乐学院聲樂系，2009年[快樂女聲全國第](../Page/快樂女聲.md "wikilink")8名。
  - [陳翔](../Page/陳翔.md "wikilink") -
    2009年考入川音綿陽藝術學院現代藝術系，2010年快樂男聲全國第5名。
  - [劉著](../Page/劉著.md "wikilink") -
    2009年考入四川音乐学院作曲系，2010年快樂男聲個性選手，以其女性化打扮而知名。
  - [范竞马](../Page/范竞马.md "wikilink")－1977年被四川音乐学院录取，毕业于川音声乐系，男高音歌唱家。
  - [汪小敏](../Page/汪小敏.md "wikilink") - 2011年9月考入四川音樂學院流行演唱系。
  - [代悅](../Page/代悅.md "wikilink") -
    2010年畢業於四川音樂學院流行演唱專業本科班；同年參加[花兒朵朵獲得全國第五名](../Page/花兒朵朵.md "wikilink")，隨後發行寫真集《無可取代》。
  - [刀郎](../Page/刀郎.md "wikilink")
  - [周奇奇](../Page/周奇奇.md "wikilink")

## 參考連結

## 外部連結

  - [四川音樂學院官方網站](http://www.sccm.cn/)
  - [四川音樂學院論壇](https://web.archive.org/web/20100329180825/http://scmusic.net/)

[\*](../Category/四川音乐学院.md "wikilink")
[Category:1939年創建的教育機構](../Category/1939年創建的教育機構.md "wikilink")

1.
2.
3.