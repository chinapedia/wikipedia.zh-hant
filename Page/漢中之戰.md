**漢中之戰**是[東漢末年群雄](../Page/東漢末年.md "wikilink")[劉備與](../Page/劉備.md "wikilink")[曹操爭奪](../Page/曹操.md "wikilink")[漢中的](../Page/漢中.md "wikilink")[戰爭](../Page/戰爭.md "wikilink")。

由於漢中是[益州北方的一個郡](../Page/益州.md "wikilink")，接近[長安](../Page/長安.md "wikilink")[三輔地區](../Page/三輔.md "wikilink")，而且易守難攻，因此劉備在214年平定益州後，於217年向北攻佔漢中，戰事維持兩年。最終，劉備領有漢中，控制中國西南地區，漢中同時也成為益州屏障，同年秋進位為[漢中王](../Page/漢中王.md "wikilink")，正式建立基業，不久後創立[蜀漢](../Page/蜀漢.md "wikilink")。

## 過程

建安二十年（215年），曹操進攻佔據漢中的[張魯](../Page/張魯.md "wikilink")，不久張魯遂投降。劉備知道消息後，立即和正爭奪[荊州的](../Page/荊州.md "wikilink")[孫權議和](../Page/孫權.md "wikilink")，回到[蜀中](../Page/蜀中.md "wikilink")。曹操留[夏侯淵督](../Page/夏侯淵.md "wikilink")[張郃](../Page/張郃.md "wikilink")、[徐晃等守漢中](../Page/徐晃.md "wikilink")；以丞相長史[杜襲為駙馬都尉](../Page/杜襲.md "wikilink")，留督漢中事。雖然後來曹軍有數次攻蜀，但[劉備已有防範](../Page/劉備.md "wikilink")。

建安二十二年（217年），[法正游說劉備](../Page/法正.md "wikilink")，分析出曹操所以留夏侯淵守備，必是有內亂；夏侯淵、張郃非一國之帥，用大軍必可攻克漢中；說出三個得到漢中的益處：上，可以討伐國賊，尊崇漢室；中，可以虎視雍、涼二州，開拓國境；下，可以固守要害，是持久的計謀。劉備贊同，開始漢中之戰。

劉備率[法正](../Page/法正.md "wikilink")、[黄忠](../Page/黄忠.md "wikilink")、[魏延](../Page/魏延.md "wikilink")、[赵云](../Page/赵云.md "wikilink")、[高翔](../Page/高翔_\(三国\).md "wikilink")、[刘封等](../Page/刘封.md "wikilink")，先用[黃權之計攻破](../Page/黃權.md "wikilink")[巴東郡的](../Page/巴東郡.md "wikilink")[朴胡及](../Page/朴胡.md "wikilink")[巴西郡的](../Page/巴西郡.md "wikilink")[杜濩](../Page/杜濩.md "wikilink")，進攻漢中的[陽平關](../Page/陽平關.md "wikilink")；另派張飛、[马超](../Page/马超.md "wikilink")、[吳蘭](../Page/吳蘭.md "wikilink")、[雷銅](../Page/雷銅.md "wikilink")、[任夔等攻](../Page/任夔.md "wikilink")[武都郡](../Page/武都郡.md "wikilink")，屯於[下辨](../Page/下辨.md "wikilink")，[氐人雷定等七部萬多部落響應](../Page/氐.md "wikilink")；而曹軍便以夏侯淵守阳平关，张郃守[广石](../Page/广石.md "wikilink")、徐晃負責[马鸣阁至](../Page/马鸣阁.md "wikilink")[阳平一带](../Page/阳平.md "wikilink")，主力抵擋劉備軍，[曹洪與](../Page/曹洪.md "wikilink")[曹真則率軍防衛張飛軍](../Page/曹真.md "wikilink")。

建安二十三年（218年），張飛與馬超改屯於固山，聲言要斷曹軍後路，曹洪此時想攻擊下辨的吳蘭，但眾人都因張飛軍的揚言舉動有所遲疑，惟獨[曹休認為敵人如要斷軍後路](../Page/曹休.md "wikilink")，應該伏兵以行；現在他們如此揚言，不如趁他們未合兵，儘早攻擊敵人。曹洪認同，進擊吳蘭軍，斬殺雷銅、任夔等人，吳蘭逃至氐族，被當地人[強端所殺](../Page/強端.md "wikilink")。至[三月](../Page/三月.md "wikilink")，張飛與馬超軍撤走。

同時，劉備與夏侯淵等對峙。[七月](../Page/七月.md "wikilink")，劉備派[陳式等攻擊馬鳴閣](../Page/陳式.md "wikilink")，被[徐晃大破](../Page/徐晃.md "wikilink")，士兵中有部份甚至在逃亡時掉進山谷中。劉備率精兵十部又不能攻克屯兵[廣石的張郃](../Page/廣石.md "wikilink")，急送書信到[成都要求增派援兵](../Page/成都.md "wikilink")。[諸葛亮接受](../Page/諸葛亮.md "wikilink")[楊洪](../Page/杨洪_\(三国\).md "wikilink")「若無漢中，則無蜀矣」的意見，於是立即發兵援救。劉軍與曹軍繼續對峙。

由於形勢緊張，曹操便在同年九月到長安坐鎮，不過由於內部問題，所以沒有立即揮軍抗敵。另一方面，夏侯淵與劉備已經相峙一年，劉備為突破局面，在建安二十四年（219年）[正月](../Page/正月.md "wikilink")，劉備由陽平南渡[沔水](../Page/沔水.md "wikilink")，沿著山前進，在定軍山依山勢立營。夏侯淵與張郃率兵來爭奪此營，在走馬谷設營。黃權進謀給劉備，於夜中，劉軍火燒曹軍鹿角。夏侯淵派張郃守護東圍，而自己則率軍守護南圍。劉備軍向張郃那面進攻，張郃不利，夏侯淵便將兵分半幫助張郃。此時，[法正說](../Page/法正.md "wikilink")：「可擊矣。（可以攻擊了）」劉備便派[黃忠乘著高勢](../Page/黃忠.md "wikilink")、擂鼓吶喊進攻，大破曹軍並擊斃夏侯淵及[趙顒](../Page/趙昂.md "wikilink")。張郃惟有率兵至漢水北下營。

當時曹軍失去元帥，軍中紛亂。杜襲與[郭淮收合散兵](../Page/郭淮.md "wikilink")，推舉張郃為統帥。張郃接受，指揮軍隊安定陣營，並授各將領節度，軍心才安定。明日，劉備想渡漢水進攻，各將領認為寡不敵眾，想依河建陣禦敵。郭淮卻認為這只是向敵人示弱，不足以挫敗敵人；建議在遠離漢水的地方設陣，引他們渡河，當過了一半時才作攻擊。張郃認為可行，設陣後，劉備疑惑，不敢渡河，曹軍於是退回阳平继续坚守。而曹操得知夏侯渊阵亡的消息，便命曹真立即增援阳平。曹真到达后，指挥徐晃军反击刘备派出的高翔军，获得胜利，暂时稳定了战场形势。

三月，曹操親自率軍由長安出兵斜谷，到達漢中。但劉備卻不擔心，認為：「曹公雖來，無能為也，我必有漢川矣。（曹公（曹操）雖然到來，但也無能為力了，我必定可以擁有漢川了。）」便聚集群眾，守備險地，不和曹軍交戰。

後曹操在北山下運糧，黃忠出兵奪取，但過期不還。[趙雲便率數十騎出營尋找](../Page/趙雲.md "wikilink")，正好遇上曹操大軍，被圍，趙雲突圍衝陣，一面戰鬥、一面退卻。曹兵再次集合，追到趙雲軍營下，趙雲入營後便大開門，偃旗息鼓。曹軍怕有伏兵便退卻，忽然趙雲軍雷鼓震天，用勁[弩從後射殺曹兵](../Page/弩.md "wikilink")。曹兵慌張害怕，自相踐踏，墮進漢水而死的人甚多。

曹操與劉備相守數月，曹軍死傷甚多，曹操欲撤退。至[五月](../Page/五月.md "wikilink")，曹操終於退兵回長安，劉備佔領漢中。

## 結果

劉備方面成功佔領漢中後，於[六月劉備派](../Page/六月.md "wikilink")[孟達由秭歸攻](../Page/孟達.md "wikilink")[房陵](../Page/房陵.md "wikilink")，殺死[太守](../Page/太守.md "wikilink")[蒯祺](../Page/蒯祺.md "wikilink")。後再命[劉封由漢中順](../Page/劉封.md "wikilink")[沔水而下](../Page/沔水.md "wikilink")，率孟達軍攻打[上庸](../Page/上庸.md "wikilink")，太守[申耽投降](../Page/申耽.md "wikilink")，為益州作了更完滿的防備。而劉備亦在同年[七月進位為漢中王](../Page/七月.md "wikilink")，與魏王曹操抗衡。

攻取取漢中是劉備生涯成就的最高峰，不僅僅因為漢中是極重要的戰略要地，也因為以軍事力量從正面擊敗曹操讓劉備的聲威如日中天，重振漢室似乎指日可待。

曹操方面，因怕撤軍以后，劉備乘機攻打[武都](../Page/武都郡.md "wikilink")，於是命[雍州](../Page/雍州刺史部.md "wikilink")[刺史](../Page/刺史.md "wikilink")[張既到武都領五萬多氐人遷徙到扶風郡和天水郡一帶](../Page/張既.md "wikilink")。

## 評價

曹操方面，雖然失去漢中重要戰略據點，但是大約40萬居民與大量資源轉進到魏國境內。

第一次遷徙，益州之戰之後，大約有20萬**巴郡**地區居民和7萬**武都**地區居民大量遷往魏國長安三輔地區。

第二次遷徙，曹操進攻佔據漢中的張魯，不久張魯遂投降。張魯率領族人與教民大約5萬人遷往魏國京兆地區。

第三次遷徙，漢中之戰後，丞相長史杜襲得知定軍山一役夏侯淵戰敗身亡．強制將漢中8萬居民緊急遷往洛陽與鄴城，並將大量糧草與資源與武器轉進魏國扶風地區，並且焚毀漢中大量民房與破壞大量田地，來不及撤退的糧草也一併燒毀。

劉備方面，雖然得到漢中重要戰略據點，為日後諸葛亮北伐打下重要基地。

由於漢中大量地區都已是赤地千里、十室九空狀態，大量人口和資源嚴重匱乏情況下，造成短期無法北伐曹魏。

之後劉備任命魏延為漢中太守兼任督漢中鎮遠將軍、魏延則是將梓潼與成都總共約10萬戶居民遷往漢中地區，並且大量開墾荒地種植糧食．軍隊則是進行軍屯，以便解決軍隊糧食問題，下令重修民房，建築國防工事、修補城牆、修築棧道。

## 參戰人物

<table>
<tbody>
<tr class="odd">
<td></td>
<td><ul>
<li>劉備軍
<ul>
<li><a href="../Page/劉備.md" title="wikilink">劉備</a>
<ul>
<li><a href="../Page/黃忠.md" title="wikilink">黃忠</a></li>
<li><a href="../Page/法正.md" title="wikilink">法正</a></li>
<li><a href="../Page/黃權.md" title="wikilink">黃權</a></li>
<li><a href="../Page/張飛.md" title="wikilink">張飛</a></li>
<li><a href="../Page/馬超.md" title="wikilink">馬超</a></li>
<li><a href="../Page/馬岱.md" title="wikilink">馬岱</a></li>
<li><a href="../Page/吳蘭.md" title="wikilink">吳蘭</a>、<a href="../Page/雷銅.md" title="wikilink">雷銅</a>、<a href="../Page/任夔.md" title="wikilink">任夔</a>（皆戰死）[1][2]</li>
<li><a href="../Page/魏延.md" title="wikilink">魏延</a></li>
<li><a href="../Page/高翔_(三国).md" title="wikilink">高翔</a></li>
<li><a href="../Page/刘封.md" title="wikilink">刘封</a></li>
<li><a href="../Page/趙雲.md" title="wikilink">趙雲</a></li>
<li><a href="../Page/陳式.md" title="wikilink">陳式</a></li>
<li><a href="../Page/張著.md" title="wikilink">張著</a></li>
<li><a href="../Page/雷定.md" title="wikilink">雷定</a>（響應馬超等人而起兵反曹）</li>
</ul></li>
</ul></li>
</ul></td>
<td><ul>
<li>曹操軍
<ul>
<li><a href="../Page/曹操.md" title="wikilink">曹操</a>
<ul>
<li><a href="../Page/夏侯淵.md" title="wikilink">夏侯淵</a>（戰死）[3]</li>
<li><a href="../Page/夏侯榮.md" title="wikilink">夏侯榮</a>（戰死）[4]</li>
<li><a href="../Page/曹洪.md" title="wikilink">曹洪</a></li>
<li><a href="../Page/張郃.md" title="wikilink">張郃</a></li>
<li><a href="../Page/杜襲.md" title="wikilink">杜襲</a></li>
<li><a href="../Page/郭淮.md" title="wikilink">郭淮</a></li>
<li><a href="../Page/徐晃.md" title="wikilink">徐晃</a></li>
<li><a href="../Page/曹休.md" title="wikilink">曹休</a></li>
<li><a href="../Page/辛毗.md" title="wikilink">辛毗</a>[5]</li>
<li><a href="../Page/赵昂.md" title="wikilink">趙顒</a>（戰死）[6]</li>
<li><a href="../Page/劉曄_(三國).md" title="wikilink">劉曄</a></li>
<li><a href="../Page/曹真.md" title="wikilink">曹真</a></li>
<li><a href="../Page/陳矯.md" title="wikilink">陳矯</a>[7]</li>
<li><a href="../Page/楊修.md" title="wikilink">楊修</a></li>
<li><a href="../Page/朴胡.md" title="wikilink">朴胡</a>、<a href="../Page/杜濩.md" title="wikilink">杜濩</a></li>
<li><a href="../Page/王平_(三国).md" title="wikilink">王平</a>（投降）</li>
</ul></li>
</ul></li>
</ul></td>
</tr>
</tbody>
</table>

## 另见

  - [兴势之战](../Page/兴势之战.md "wikilink")，[三国时期发生在](../Page/三国时期.md "wikilink")[洋县](../Page/洋县.md "wikilink")[兴势山的](../Page/兴势山.md "wikilink")[战役](../Page/战役.md "wikilink")。

## 註釋

<references/>

## 參考資料

  - 《[三國志](../Page/三國志.md "wikilink")》
  - 《[三國志註](../Page/三國志註.md "wikilink")》
  - 《[資治通鑒](../Page/資治通鑒.md "wikilink")》

[Category:三国战役](../Category/三国战役.md "wikilink")
[Category:汉朝战役](../Category/汉朝战役.md "wikilink")
[Category:東漢歷史事件](../Category/東漢歷史事件.md "wikilink")
[Category:3世纪中国](../Category/3世纪中国.md "wikilink")

1.  《三國志》（卷三十二·蜀書二）：「二十三年，先主率諸將進兵漢中。分遣將軍吳蘭、雷銅等入武都，皆為曹公軍所沒。」
2.  《三國志》（卷一·魏書一）：「二十三年春正月，……曹洪破吳蘭，斬其將任夔等。三月，張飛、馬超走漢中，陰平氐強端斬吳蘭，傳其首。」
3.  《三國志》（卷三十六·蜀書六）：「建安二十四年，於漢中定軍山擊夏侯淵。淵眾甚精，忠推鋒必進，勸率士卒，金鼓振天，歡聲動谷，一戰斬淵，淵軍大敗。」
4.  《三國志註魏略》（卷九·魏書九）：「漢中之敗，榮年十三，左右提之走，不肯，曰：『君親在難，焉所逃死！』乃奮劍而戰，遂沒陳。」
5.  《三國志》（卷二十五·魏志二十五）：久之，太祖遣都护曹洪平下辩，使毗与曹休参之
6.  《三國志》（卷三十二·蜀書二）：「二十四年春，……大破淵軍，斬淵及曹公所署益州刺史趙顒等。」
7.  《三國志》（卷二十二·魏志二十二）：（陳矯）从征汉中，还为尚书。