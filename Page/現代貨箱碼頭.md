**現代貨箱碼頭有限公司**簡稱**現代貨箱碼頭**、**現代貨箱**（[英文](../Page/英文.md "wikilink")[縮寫](../Page/縮寫.md "wikilink")：**MTL**），成立於1969年，是[香港歷史最悠久的](../Page/香港歷史.md "wikilink")[貨櫃](../Page/貨櫃.md "wikilink")[碼頭](../Page/碼頭.md "wikilink")\[1\]，是香港第二大[貨櫃](../Page/貨櫃.md "wikilink")[碼頭營運商](../Page/碼頭.md "wikilink")，僅次於[香港國際貨櫃碼頭有限公司](../Page/香港國際貨櫃碼頭有限公司.md "wikilink")，主要經營[葵青貨櫃碼頭](../Page/葵青貨櫃碼頭.md "wikilink")1、2、5及9（南）號碼頭，以及[聯營](../Page/聯營.md "wikilink")[廣東](../Page/廣東.md "wikilink")[深圳及](../Page/深圳.md "wikilink")[江蘇](../Page/江蘇.md "wikilink")[太倉的貨櫃碼頭](../Page/太倉.md "wikilink")。\[2\]現代貨箱碼頭擁有[蛇口集裝箱碼頭](../Page/蛇口集裝箱碼頭.md "wikilink")20%股權，旁邊[赤灣集裝箱碼頭亦間接持有](../Page/赤灣集裝箱碼頭.md "wikilink")8%股權，均與公司股東招商局國際合營。[大鏟灣碼頭則佔](../Page/大鏟灣碼頭.md "wikilink")65%。

## 歷史

1970年8月，[香港政府在](../Page/香港政府.md "wikilink")[葵涌貨櫃碼頭招標](../Page/葵涌貨櫃碼頭.md "wikilink")，現代貨箱即投得葵涌一號貨櫃碼頭的發展經營權，二、三號貨櫃碼頭則分別由日本大山船務公司及投得。

1972年，現代貨櫃再投得葵涌五號貨櫃碼頭。

1980年代，九倉透過現代貨箱積極拓展貨櫃碼頭業務，1985年以香港政府授權的葵涌六號貨櫃碼頭半數權益與於1975年購得日本大山船務株式會社二號貨櫃碼頭的國際貨櫃交換，至此其所擁有的一、二、五號貨櫃碼頭連成一體。

1991年，現代貨箱再獲得八號貨櫃碼頭4個泊位中的兩個，成為香港貨櫃碼頭業僅次於[香港國際貨櫃碼頭有限公司的另一大集團](../Page/香港國際貨櫃碼頭有限公司.md "wikilink")。

1995年，現代貨箱的貨櫃處理量達211萬個[標準箱](../Page/標準箱.md "wikilink")，約佔葵涌貨櫃碼頭吞吐量的25%。

## 股東

現代貨箱原是九龍倉的聯營公司，九龍倉持有其25.6%股權，但1994年後，現代貨箱的兩個股東歐洲航務公司[馬士基和英國](../Page/馬士基.md "wikilink")[鐵行輪船公司先後向九龍倉多次出售其所持股份](../Page/鐵行輪船公司.md "wikilink")，令九龍倉持有現代貨箱的股權大幅上升到50.84%（其他股東包括持20.31%的招商局、持17.65%的[太古洋行](../Page/太古洋行.md "wikilink")、持6.3%的[匯豐及](../Page/匯豐.md "wikilink")4.9%的[捷成洋行](../Page/捷成洋行.md "wikilink")），現代貨箱遂成為九倉的附屬公司。

2001年初，九龍倉向匯豐購進4.5%股份，使其股權增至55.3%\[3\]；而據年報其他主要股東為[招商局國際](../Page/招商局國際.md "wikilink")（22.1%）及太古公司（17.62%）。

2003年，太古全數出售所持股份，其中九龍倉購入12.59%（共持有67.89%），作價港幣20.72億元；而招商局購入5.03%（共持有27.04%），作價港幣8.28億元。\[4\]
交易後九龍倉進一步穩固大股東的地位。

2005年，現代貨箱向招商局及九倉回購股份\[5\]，使招商局國際與九龍倉分別持有 27.01%\[6\] 及 67.59%權益
。\[7\]\[8\]
截至2010年，九龍倉與招商局國際仍維持68%與27%的比例；[捷成洋行的附屬或合營公司Jebsen](../Page/捷成洋行.md "wikilink")
Securities Ltd持有5%。\[9\]

## 參考

<references />

## 連結

  - [現代貨箱碼頭有限公司](http://www.modernterminals.com/)

[Category:香港物流公司](../Category/香港物流公司.md "wikilink")
[Category:香港貨運碼頭](../Category/香港貨運碼頭.md "wikilink")
[Category:九龍倉集團](../Category/九龍倉集團.md "wikilink")
[Category:招商局](../Category/招商局.md "wikilink")
[Category:葵青區](../Category/葵青區.md "wikilink")
[Category:1969年成立的公司](../Category/1969年成立的公司.md "wikilink")
[Category:1969年香港建立](../Category/1969年香港建立.md "wikilink")

1.  [港澳地區物流業的發展](http://www.orientallogistics.com/newoll04/media/exarticles/eng/China%20Modern%20Logistics%20Development%20Report_Chapter9.pdf)
2.  [現代貨箱碼頭 -
    設施及裝備](http://www.modernterminals.com/chi/operations/equipment.html)

3.
4.
5.
6.  <http://www.cmhi.com.hk/Upload/9/images/2008ann_rpt_c.pdf>
7.
8.  <http://www.wharfholdings.com/download_chi/ar2008/Wharf-08_Chi.pdf>
9.  [現代貨箱碼頭 -
    發展里程碑](http://www.modernterminals.com/chi/theCompany/history.html)