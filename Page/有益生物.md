**有益生物**泛指進行對[人類有益作用的](../Page/人類.md "wikilink")[生物](../Page/生物.md "wikilink")。界定有益生物的標準相當主觀：人類往往衡量某個[品種的生物對人類的貢獻多寡去判斷其是否有益](../Page/種_\(生物\).md "wikilink")。例如從[農業的角度看](../Page/農業.md "wikilink")，種植[農作物是首要的目標](../Page/農作物.md "wikilink")，所以任何協助農作物生長的昆蟲均被視為**益蟲**。

許多種昆蟲可於一個領域裏被視為[害蟲](../Page/害蟲.md "wikilink")，而在另一個領域裏成為益蟲。\[1\]

**益蟲**在[有機種植中](../Page/有機種植.md "wikilink")，協助益蟲繁衍（如提供較好的生存環境）是[防治蟲害的常用策略](../Page/蟲害防治.md "wikilink")。有些從事蟲害防治的公司有向[農場或](../Page/農場.md "wikilink")[園圃出售益蟲](../Page/園圃.md "wikilink")，尤其受擁有[溫室的農場歡迎](../Page/溫室.md "wikilink")。

## 有關益蟲的實例

[Plumpollen0060.jpg](https://zh.wikipedia.org/wiki/File:Plumpollen0060.jpg "fig:Plumpollen0060.jpg")

  - [蜜蜂](../Page/蜜蜂.md "wikilink")
  - [家蠶](../Page/家蠶.md "wikilink")
  - [瓢蟲](../Page/瓢蟲.md "wikilink")
  - [螳螂](../Page/螳螂.md "wikilink")
  - [食蚜虻](../Page/食蚜虻.md "wikilink")
  - [寄生蜂](../Page/寄生蜂.md "wikilink")
  - [草蛉](../Page/草蛉.md "wikilink")
  - [螞蟻](../Page/螞蟻.md "wikilink")
  - [鼠婦](../Page/鼠婦.md "wikilink")
  - [蜻蜓](../Page/蜻蜓.md "wikilink")
  - [蝴蝶](../Page/蝴蝶.md "wikilink")（幼體不是）
  - [蜻蛉](../Page/蜻蛉.md "wikilink")
  - [步行蟲](../Page/步行蟲.md "wikilink")
  - [螢火蟲](../Page/螢火蟲.md "wikilink")（幼體以蝸牛為食）
  - [閰魔蟲](../Page/閰魔蟲.md "wikilink")
  - [高鼻蜂](../Page/高鼻蜂.md "wikilink")
  - [狩獵蜂類](../Page/狩獵蜂類.md "wikilink")（如[胡蜂](../Page/胡蜂.md "wikilink")、[蘭格氏壁蜂](../Page/蘭格氏壁蜂.md "wikilink")）
  - [花蜂類](../Page/花蜂類.md "wikilink")（如[熊蜂](../Page/熊蜂.md "wikilink")、[塗壁花蜂](../Page/塗壁花蜂.md "wikilink")）
  - [寄生蠅](../Page/寄生蠅.md "wikilink")（除少部分種類之外皆是）

## 不屬於昆蟲的有益生物

  - [美洲豹](../Page/美洲豹.md "wikilink")
  - [豹](../Page/豹.md "wikilink")
  - [虎](../Page/虎.md "wikilink")
  - [馬](../Page/馬.md "wikilink")
  - [蚯蚓](../Page/蚯蚓.md "wikilink")
  - [青蛙](../Page/青蛙.md "wikilink")
  - [壁虎](../Page/壁虎.md "wikilink")
  - [蚰蜒](../Page/蚰蜒.md "wikilink")
  - [捕植螨](../Page/捕植螨.md "wikilink")
  - [啄木鳥](../Page/啄木鳥.md "wikilink")

## 其他

  - [食蟲植物](../Page/食蟲植物.md "wikilink")（如[豬籠草](../Page/豬籠草.md "wikilink")、[毛氈苔](../Page/毛氈苔.md "wikilink")、[捕蠅草等](../Page/捕蠅草.md "wikilink")）
  - [冬蟲夏草](../Page/冬蟲夏草.md "wikilink")（寄生於蟲蛹，為著名的中草藥）

## 參見

  - [有害生物](../Page/有害生物.md "wikilink")

## 參考資料

<div class="references-small">

<references />

</div>

[\*](../Category/昆蟲綱.md "wikilink") [\*](../Category/益蟲.md "wikilink")

1.  [動物星球頻道](../Page/動物星球頻道.md "wikilink")：益蟲，<http://www.animalplanet.com.tw/insects/useful_insects/index.shtml>，2007年。擷取於2007年9月1日。