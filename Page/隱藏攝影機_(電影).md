《**隱藏攝影機**》（），[2005年](../Page/2005年电影.md "wikilink")[法國影片](../Page/法國.md "wikilink")，由[奧地利導演](../Page/奧地利.md "wikilink")[米開耳·韓內克](../Page/米開耳·韓內克.md "wikilink")（Michael
Haneke）執導。
影片講 Georges Laurent 跟 Anne Laurent
這對夫妻開始收到匿名人士寄來的錄影帶，為這個家庭帶來恐懼不安的氣氛。影片觸及到1961年10月17日的巴黎屠殺事件。

## 剧情

乔治（Georges）是一个非常出色的电视节目主持人，他与他的妻子安妮和儿子皮耶侯住在一起。但是家庭的宁静被一个不明来源的隐蔽摄影的录影带打破了。录影带记录了家里的行踪，起初这个录影带并没有对家里带来多大的伤害，可是随后收到录影带的同时还会收到一个不明卡片，上面畫着人头和鲜血。这使得乔治想起了他的童年生活，让这个家庭开始感到不安。

因为录影带还没有给家庭带来暴力伤害，所以警察不愿管理這個案子。后来乔治又收到了一盘录影带。這使他到了一个公寓，让他见到他小时候他家以前雇佣的小孩马吉（Majid）。当马吉小的时候，他的父母在巴黎的1961年遭受到迫害，于是马吉就暂时住在了乔治家住下，并在乔治家打工。乔治问到马吉关于录影带的事情，可是他告诉乔治没有做过这件事。在电影进行中，有时会闪回到乔治的童年，让他想起马吉小时候砍掉一只鸡的头，弄得满脸血的样子。这样安妮怀疑起这件事和乔治与马吉的小时候的关系有牵连。

有一天皮耶侯放学后并没有回到家，安妮也找不到她的去处。乔治和安妮怀疑是马吉绑架了孩子。他们报了警，并把马吉和他的孩子带到了警察局。但是第二天他们又被释放了，而同时皮耶侯也回了家。这一晚他没有告诉别人他与他的朋友住在一起，

马吉让乔治来到他的公寓，马吉说"我就是想让你在场"，之后什么也没有做，就割喉自杀了。当乔治回家后开始跟安妮说起他六岁时，他曾跟父母说让马吉离开他的家，可是父母并没有这么做，之后用计让马吉去砍掉鸡的头，并告诉父母马吉因此吓唬他，这样马吉就被乔治父母驱逐了家成为了孤儿。

马吉自杀后，他的儿子来到乔治那里。他否认他-{偷拍}-过录影带，同时乔治也拒绝对马吉的死负責。马吉的儿子说我只是想知道在我的父亲死后，你现在心里是什么样子的。喬治气愤的回到家，吃了两片安眠药，上床睡觉了。

之后开始镜头进入了乔治的梦中，回到了他的童年时代。马吉被迫拉上了车，离开了乔治的家。在影片的最后是皮耶侯和马吉的儿子在一起的镜头。

## 主要演员

  - Juliette Binoche : Anne Laurent
  - Daniel Auteuil : Georges Laurent
  - Maurice Bénichou : Majid
  - Annie Girardot : Georges 的母親
  - Lester Makedonsky : Pierrot Laurent
  - Bernard Le Coq : Georges 的總編輯
  - Walid Afkir : Majid 的兒子
  - Daniel Duval : Pierre
  - Nathalie Richard : Mathilde
  - Denis Podalydès : Yvon

## 得獎記錄

  - [2005年](../Page/2005年电影.md "wikilink") :
    [坎城影展](../Page/坎城影展.md "wikilink")
    [評審團大獎](../Page/評審團大獎.md "wikilink")、[最佳導演獎](../Page/最佳導演獎.md "wikilink")、[費比西獎](../Page/費比西獎.md "wikilink")
  - [2005年](../Page/第18屆歐洲電影獎.md "wikilink") :
    [歐洲電影獎最佳影片](../Page/歐洲電影獎.md "wikilink")、最佳導演、最佳男主角（Daniel
    Auteuil）、最佳剪接

<!-- end list -->

  - 2006年 : [盧米耶獎](../Page/盧米耶獎.md "wikilink")
    [最佳劇本獎](../Page/最佳劇本獎.md "wikilink")

## 外部链接

  - [官方网站](http://www.sonyclassics.com/cache/)

  -
  -
[Category:2005年电影](../Category/2005年电影.md "wikilink")
[Category:法語電影](../Category/法語電影.md "wikilink")
[Category:2000年代劇情片](../Category/2000年代劇情片.md "wikilink")
[Category:法國劇情片](../Category/法國劇情片.md "wikilink")
[Category:奧地利劇情片](../Category/奧地利劇情片.md "wikilink")
[Category:德国剧情片](../Category/德国剧情片.md "wikilink")
[Category:意大利剧情片](../Category/意大利剧情片.md "wikilink")
[Category:巴黎背景电影](../Category/巴黎背景电影.md "wikilink")
[Category:巴黎取景电影](../Category/巴黎取景电影.md "wikilink")
[Category:迈克尔·哈内克电影](../Category/迈克尔·哈内克电影.md "wikilink")
[Category:欧洲电影奖最佳影片](../Category/欧洲电影奖最佳影片.md "wikilink")
[Category:欧洲电影奖最佳导演获奖电影](../Category/欧洲电影奖最佳导演获奖电影.md "wikilink")
[Category:洛杉矶影评人协会奖最佳外语片](../Category/洛杉矶影评人协会奖最佳外语片.md "wikilink")
[Category:欧洲电影奖最佳男主角获奖电影](../Category/欧洲电影奖最佳男主角获奖电影.md "wikilink")