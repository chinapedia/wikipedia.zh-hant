**公民力量**（），[香港的一個](../Page/香港.md "wikilink")[建制派組織](../Page/建制派.md "wikilink")，主要活躍於[新界](../Page/新界.md "wikilink")[沙田區](../Page/沙田區.md "wikilink")、[西貢區](../Page/西貢區.md "wikilink")、[大埔區和](../Page/大埔區.md "wikilink")[北區](../Page/北區.md "wikilink")。該組織是在1993年12月成立，由當時8位沙田[區議員組成](../Page/區議員.md "wikilink")，包括當時剛退出[香港民主同盟](../Page/香港民主同盟.md "wikilink")（民主黨前身）的[劉江華](../Page/劉江華.md "wikilink")，後來[劉江華退出公民力量](../Page/劉江華.md "wikilink")。

公民力量曾在[沙田區議會和](../Page/沙田區議會.md "wikilink")[西貢區議會分別擁有](../Page/西貢區議會.md "wikilink")14個和4個議席，在沙田區是擁有最多議席的地區組織，因而亦被喻為「沙田執政黨」\[1\]。

## 歷史

### 2007年香港區議會選舉

公民力量總共派出20人參選，分別參選沙田區（16人）和西貢區（4人）的區議會，其中4人[自動當選](../Page/自動當選.md "wikilink")，其他候選人方面，首次參選的5人亦只有兩人落選。

### 與新民黨結盟

2014年2月12日，[新民黨宣佈與公民力量結成聯盟](../Page/新民黨_\(香港\).md "wikilink")，擴大組織架構。結盟後，公民力量19名區議員均加入新民黨，令新民黨區議員原來12名，增至31名。新民黨亦於2月11日舉行之特別會員大會中，通過委任公民力量的召集人潘國山為該黨副主席\[2\]。與此同時，[葉劉淑儀則成為公民力量會長](../Page/葉劉淑儀.md "wikilink")，[田北辰則成為公民力量榮譽主席](../Page/田北辰.md "wikilink")。

### 2015年香港區議會選舉

公民力量總共派出23人參選，當中4人[自動當選](../Page/自動當選.md "wikilink")，但由於得票下跌及政府取消所有委任議席，總議席數目下跌至11席。有分析指，公民力量慘敗與跟[新民黨結盟有關](../Page/新民黨_\(香港\).md "wikilink")\[3\]。

## 議員

### 立法會議員

<div style="overflow:auto; width:100%">

| 席位                                 | 選區／界別                                        | 議員                               | 1998-2000                                                                                    | 2000-2004                                                                                    | 2004-2008                                                                                    | 2008-2012                                                                                    | 2012-2016                                                                                                                                                                    | 2016-2020 | 備註                                        |
| ---------------------------------- | -------------------------------------------- | -------------------------------- | -------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------- | ----------------------------------------- |
| [地方選區](../Page/地方選區.md "wikilink") | [新界東](../Page/新界東選區.md "wikilink")           | [劉江華](../Page/劉江華.md "wikilink") | [Black_check.svg](https://zh.wikipedia.org/wiki/File:Black_check.svg "fig:Black_check.svg") | [Black_check.svg](https://zh.wikipedia.org/wiki/File:Black_check.svg "fig:Black_check.svg") | [Black_check.svg](https://zh.wikipedia.org/wiki/File:Black_check.svg "fig:Black_check.svg") | [Black_check.svg](https://zh.wikipedia.org/wiki/File:Black_check.svg "fig:Black_check.svg") |                                                                                                                                                                              |           | 同為[民建聯成員](../Page/民建聯.md "wikilink")，已退黨。 |
| [容海恩](../Page/容海恩.md "wikilink")   |                                              |                                  |                                                                                              |                                                                                              |                                                                                              | [Black_check.svg](https://zh.wikipedia.org/wiki/File:Black_check.svg "fig:Black_check.svg") | 同為[新民黨成員](../Page/新民黨_\(香港\).md "wikilink")                                                                                                                                  |           |                                           |
| [功能界別](../Page/功能界別.md "wikilink") | [工業界（第二）](../Page/工業界（第二）功能界別.md "wikilink") | [林大輝](../Page/林大輝.md "wikilink") |                                                                                              |                                                                                              |                                                                                              |                                                                                              | [Black_check.svg](https://zh.wikipedia.org/wiki/File:Black_check.svg "fig:Black_check.svg")→[B-check.svg](https://zh.wikipedia.org/wiki/File:B-check.svg "fig:B-check.svg") |           | 於2013年加入，2015年退出                          |
| **所得議席**                           | 1                                            | 1                                | 1                                                                                            | 1                                                                                            | 0→1→0                                                                                        | 1                                                                                            |                                                                                                                                                                              |           |                                           |

</div>

### 區議員

現時，公民力量在2個區議會共有11個議席，除黃宇翰、林松茵外全部均同為[新民黨成員](../Page/新民黨_\(香港\).md "wikilink")，議員包括：

| 區議會                                | 選區號碼 | 選區                                       | 議員                               | 備註     |
| ---------------------------------- | ---- | ---------------------------------------- | -------------------------------- | ------ |
| [西貢區](../Page/西貢區議會.md "wikilink") | Q16  | 翠林                                       | [譚領律](../Page/譚領律.md "wikilink") |        |
| Q17                                | 寶林   | [區能發](../Page/區能發.md "wikilink")         |                                  |        |
| Q23                                | 德明   | [溫悅昌](../Page/溫悅昌.md "wikilink")         |                                  |        |
| [沙田區](../Page/沙田區議會.md "wikilink") | R02  | 瀝源                                       | [黃宇翰](../Page/黃宇翰.md "wikilink") | 非新民黨成員 |
| R05                                | 愉城   | [梁家輝](../Page/梁家輝_\(新民黨\).md "wikilink") |                                  |        |
| R13                                | 顯嘉   | [林松茵](../Page/林松茵.md "wikilink")         | 非新民黨成員                           |        |
| R14                                | 下城門  | [唐學良](../Page/唐學良.md "wikilink")         |                                  |        |
| R15                                | 雲城   | [何厚祥](../Page/何厚祥.md "wikilink")         |                                  |        |
| R17                                | 田心   | [潘國山](../Page/潘國山.md "wikilink")         |                                  |        |
| R35                                | 愉欣   | [姚嘉俊](../Page/姚嘉俊.md "wikilink")         |                                  |        |
| R38                                | 廣源   | [陳敏娟](../Page/陳敏娟.md "wikilink")         |                                  |        |

## 參考資料

<references/>

## 外部連結

  - [公民力量官方網站](http://www.civilforce.org.hk/)
  - [香港立法會：劉江華議員](https://web.archive.org/web/20060106000245/http://www.legco.gov.hk/general/chinese/members/yr04-08/lkw.htm)

[Category:香港政黨](../Category/香港政黨.md "wikilink")
[Category:香港建制派組織](../Category/香港建制派組織.md "wikilink")

1.
2.  [新民黨宣佈與公民力量結盟](https://www.npp.org.hk/zh-hk/node/16339)，新民黨新聞稿，2014年2月11日
3.  [中環出更：公記啃「老」慘敗
    潘國山孭鑊](http://orientaldaily.on.cc/cnt/news/20151221/00176_106.html)，東方日報，2015年12月21日