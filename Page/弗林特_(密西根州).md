[Genesee_County_Michigan_Incorporated_and_Unincorporated_areas_Flint_Highlighted.svg](https://zh.wikipedia.org/wiki/File:Genesee_County_Michigan_Incorporated_and_Unincorporated_areas_Flint_Highlighted.svg "fig:Genesee_County_Michigan_Incorporated_and_Unincorporated_areas_Flint_Highlighted.svg")
**弗林特**，又譯作**芬林市**（Flint,
Michigan）是[美國](../Page/美國.md "wikilink")[密西根州的一個城市](../Page/密西根州.md "wikilink")。位于底特律西北106公里处弗林特河岸。
根据2010年人口普查，人口为102434,
为密歇根第七大城市。弗林特市为[傑納西縣縣治](../Page/傑納西縣_\(密西根州\).md "wikilink")。弗林特都市区为密歇根第七大，拥有425790人口（2010年数据）。

弗林特以通用汽车诞生地闻名，同时1936－1937年的一场发生在弗林特的罢工对于美国汽车工人工会的成立起到了关键的作用。

2014年以来，爆发,弗林特市居民的食水来源[弗林特河发现含有高浓度神经毒素](../Page/弗林特河.md "wikilink")[铅](../Page/铅.md "wikilink")。2016年，密歇根州长宣布弗林特进入紧急状态，弗林特市的居民只能依靠州政府提供的瓶装水过活\[1\]。

## 历史

1855年建市。早期是[伐木業中心](../Page/伐木.md "wikilink")，後來成為美國[汽車工業重鎮](../Page/汽車工業.md "wikilink")。但在1960年代以後逐漸衰落。

## 交通

弗林特市附近有Bishop国际机场。 美国铁路公司有“蓝水”(Blue Water)线经过本市每日往返芝加哥以及呼伦港(Port Huron)。
在市内，有弗林特公交（MTA）提供服务。 灰狗巴士也有班车往北至贝城(Bay City)，往南到底特律。
"印第安小道"巴士公司有班车往西去芝加哥。

## 地理

弗林特市面积88平方公里，其中87平方公里为陆地，1平方公里为水面。 弗林特市坐落在弗林特小丘的东北部。

## 教育

弗林特有密歇根大学弗林特分校

  - [University of Michigan–Flint](http://www.umflint.edu/)
    校内配套有图书馆，游泳馆，体育馆，有两个学生宿舍

## 文化设施

弗林特有弗林特文化中心，包括斯隆博物馆，弗林特艺术馆，弗林特音乐厅，以及一个天文馆，位于475洲际高速公路东边。其中斯隆博物馆为AAA重点推荐旅游目的地。

## 姐妹城市

  - [長春市](../Page/長春市.md "wikilink")

  - [陶里亞蒂](../Page/陶里亞蒂.md "wikilink")

  - [哈密爾頓](../Page/哈密尔顿_\(安大略\).md "wikilink")

  - [凱爾采](../Page/凱爾采.md "wikilink")

## 參見

  - [蒙娜·哈娜-阿提沙](../Page/蒙娜·哈娜-阿提沙.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

[F](../Category/密歇根州城市.md "wikilink")

1.  [密歇根州正送赠清洁食水－但不是给断水两年的弗林特市](https://wknews.org/node/1700)