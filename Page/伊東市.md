[Itou-Spa-object.JPG](https://zh.wikipedia.org/wiki/File:Itou-Spa-object.JPG "fig:Itou-Spa-object.JPG")\]\]
**伊東市**（）是[靜岡縣東部的](../Page/靜岡縣.md "wikilink")[市](../Page/市.md "wikilink")。位于[伊豆半島東海岸](../Page/伊豆半島.md "wikilink")，面向[相模灘](../Page/相模灘.md "wikilink")。主要經濟產業為[漁業和](../Page/漁業.md "wikilink")[觀光業](../Page/觀光業.md "wikilink")。

## 出身有名人

  - [木下杢太郎](../Page/木下杢太郎.md "wikilink")（醫學博士、文學家）
  - [伊代野貴照](../Page/伊代野貴照.md "wikilink")（職業棒球選手）