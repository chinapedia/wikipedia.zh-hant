**ACDSee**是一個[共享軟件](../Page/共享軟件.md "wikilink")，由*[ACD
Systems](../Page/ACD_Systems.md "wikilink")*开发的一款[图像浏览器](../Page/图像浏览器.md "wikilink")。

## 歷史

ACDSee是最早作为[JPEG解码器得以广泛使用的软件之一](../Page/JPEG.md "wikilink")，事实上他还被捆绑在一个早期的[网络浏览器](../Page/网络浏览器.md "wikilink")[Mosaic中作为图像解码工具](../Page/Mosaic.md "wikilink")\[1\]。

ACDSee分为标准版和加强版（PowerPack，在4.0版以後才出現）兩個版本，支持几乎所有的图形格式。同时加强版还有许多额外的功能，有的相当实用。日常使用的功能有微圖顯示、檔案格式轉換、幻燈片式放映圖片、[CD](../Page/CD.md "wikilink")／[DVD](../Page/DVD.md "wikilink")[燒錄](../Page/燒錄.md "wikilink")、[HTML畫廊製作等](../Page/HTML.md "wikilink")。註冊費用為50[美元](../Page/美元.md "wikilink")。

在4.0版之後，由于**ACDSee**支援的檔案格式愈來愈多，导致操作系统時消耗的资源急剧上升，且讀取圖像的速度也變慢（在7.0版後有改善），不少只是为了浏览图像的使用者反而转向
3.1及以前的版本。

## 特性

ACDSee在初期版本即提供了一种简便的图片注释方法，即在图片文件夹下新建一个隐藏的文本文件，名为
[Descript.ion](../Page/Descript.ion.md "wikilink")，将逐个图片的注释存储在这一文件，并能够在浏览和查看时显示。

格式如下：

` 文件名1.jpg 注释内容`
` 文件名2.png 文件2注释`

## 評論

ACDSee 7.0版本由於其最终用户协议中包含一段禁止观看色情图片的内容而饱受攻击。

在V11之前，ACDSee對[Unicode的支援很差](../Page/Unicode.md "wikilink")，若檔案名稱含有非当前系统编码的[字符](../Page/字符.md "wikilink")，可能會不能開啟。不過用戶不能直接輸入中文等Unicode的問題已在ACDSee
Photo Manager 11/ACDSee Pro Photo Manager 2.5之後的版本得到完美的支援\[2\]。

## 加强版中额外的程序

  - [Opanda IExif，一个支持在 ACDSee 裡查看图片 EXIF 信息的外挂式软件，按 Ctrl 或 Shift
    键选鼠标右键即可调用](http://www.opanda.com/cn/iexif/index.html)
  - *ACD Photo Editor*
  - *ACD FotoSlate*

## 參見

  - [IrfanView](../Page/IrfanView.md "wikilink")
  - [Picasa](../Page/Picasa.md "wikilink")
  - [XnView](../Page/XnView.md "wikilink")
  - [Konvertor](../Page/Konvertor.md "wikilink")
  - [圖像檢視器比較](../Page/圖像檢視器比較.md "wikilink")

## 參考資料

<references />

## 外部連結

  - [ACDSee官方網頁](http://www.acdsee.com/)

[Category:圖像檢視器](../Category/圖像檢視器.md "wikilink")
[Category:共享軟體](../Category/共享軟體.md "wikilink")

1.  [ACD Systems company
    profile](http://www.acdcorporate.com/english/Company/index)
    ，於2005年9月13日存取
2.  ACDSee 11開始支援unicode了：[Photo editor support unicode
    characters](http://gaaan.com/EZsoft?p=73916)