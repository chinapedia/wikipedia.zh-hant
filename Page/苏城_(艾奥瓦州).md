**蘇城**（），[美國](../Page/美國.md "wikilink")[愛荷華州西北部城市](../Page/愛荷華州.md "wikilink")，[伍德伯里郡](../Page/伍德伯里郡.md "wikilink")（Woodbury
County）[郡治所在處](../Page/首府.md "wikilink")。蘇城位於[密蘇里河上游沿岸](../Page/密蘇里河.md "wikilink")，地處愛荷華州、[內布拉斯加州與](../Page/內布拉斯加州.md "wikilink")[南達科他州交界處](../Page/南達科他州.md "wikilink")，其與周遭地區經常合稱為[蘇蘭](../Page/蘇蘭.md "wikilink")（Siouxland），在過去曾是[北美印地安人主要部族](../Page/北美印地安人.md "wikilink")——[蘇族的活動範圍](../Page/蘇族.md "wikilink")，因而得名。根據2005年時的普查結果蘇城有83,148人的[人口](../Page/人口.md "wikilink")，而整個周遭市鎮所構成的都會區，則有約143,053人的人口。

## 歷史

[091607-SiouxCity-Historic4th.jpg](https://zh.wikipedia.org/wiki/File:091607-SiouxCity-Historic4th.jpg "fig:091607-SiouxCity-Historic4th.jpg")
雖然人類在附近地區活動的紀錄，最早可以回溯到15,000年前，之後苏族印第安人在此居住，直到18世紀時西方文明才逐漸涉足到此地區，有來自[西班牙與](../Page/西班牙.md "wikilink")[法國的](../Page/法國.md "wikilink")[毛皮商人開始聚集在密蘇里河沿岸](../Page/毛皮商人.md "wikilink")，和此地[原住民進行接觸](../Page/原住民.md "wikilink")。1803年時，美國透過[路易西安納購地案](../Page/路易西安納購地案.md "wikilink")，自法國手中購得大片領土，當時的[美國總統](../Page/美國總統.md "wikilink")[托马斯·杰斐逊派出著名的](../Page/托马斯·杰斐逊.md "wikilink")[劉易斯與克拉克遠征隊](../Page/劉易斯與克拉克遠征隊.md "wikilink")（Lewis
and Clark
expedition）考察探勘這些新購得、但尚未實際探索的領土。遠征隊在1804年時來到今日蘇城附近、伍德伯里郡的範圍內，8月20日，遠征隊隊員之一的[查爾斯·弗洛伊德中士](../Page/查爾斯·弗洛伊德.md "wikilink")（Sgt.
Charles Floyd）突然因為[膽痛](../Page/膽痛.md "wikilink")（Bilious
colic）驟逝，遺體被葬在一個能俯瞰密蘇里河的斷崖上。1848年時，[威廉·湯普森](../Page/威廉·湯普森.md "wikilink")（William
Thompson）在此附近不遠處建立了一個[交易站](../Page/交易站.md "wikilink")（Trading
post），並將此地命名為[佛洛伊德崖](../Page/佛洛伊德崖.md "wikilink")（Floyd's
Bluff），希望能將它發展為一個城鎮。不過，接踵來至此地的[拓荒者並沒有實現湯普森的願望](../Page/拓荒者.md "wikilink")，而是在更上游[佛洛伊德河](../Page/佛洛伊德河.md "wikilink")（Floyd
River）與[大蘇河](../Page/大蘇河.md "wikilink")（Big Sioux
River）的匯口處建立新的市鎮，而逐漸發展成今日的蘇城。

## 交通

蘇城的主要[機場是位在市區東南郊的](../Page/機場.md "wikilink")[蘇城閘門機場](../Page/蘇城閘門機場.md "wikilink")（Sioux
Gateway Airport，SUX/KSUX），該機場又名「戴上校飛行場」（Col. Bud Day
Field），每天有[西北航空的班機固定往返蘇城與](../Page/西北航空.md "wikilink")[明尼亞波利](../Page/明尼亞波利.md "wikilink")（Minneapolis）之間。1989年時，一架因為[引擎故障造成全機](../Page/引擎.md "wikilink")[液壓系統失效的](../Page/液壓系統.md "wikilink")[聯合航空](../Page/聯合航空.md "wikilink")[道格拉斯DC-10型客機在蘇城機場迫降失敗翻覆](../Page/道格拉斯DC-10.md "wikilink")，被稱為「[蘇城空難](../Page/蘇城空難.md "wikilink")」，也因此使得此機場與蘇城聲名大噪。

[29號州際公路沿著密蘇里河沿岸經過蘇城市區](../Page/29號州際公路.md "wikilink")，是該市最重要的聯外幹線。

## 相關條目

  - [劉易斯與克拉克遠征](../Page/劉易斯與克拉克遠征.md "wikilink")
  - [密蘇里河](../Page/密蘇里河.md "wikilink")
  - [蘇城空難](../Page/蘇城空難.md "wikilink")

## 參考文獻

  - [蘇蘭社區協會網站-蘇蘭歷史](https://web.archive.org/web/20070901235425/http://www.siouxlandchamber.com/our_community/history/index.php)

## 外部連結

  - [蘇城市政府官方網站](http://www.sioux-city.org/)

  - [蘇蘭商務協會官方網站](https://web.archive.org/web/20070717144126/http://www.siouxlandchamber.com/main.html)

  - [蘇城機場官方網站](http://www.flysiouxgateway.com/)

[Category:艾奥瓦州城市](../Category/艾奥瓦州城市.md "wikilink")
[Category:艾奧瓦州縣城](../Category/艾奧瓦州縣城.md "wikilink")
[Category:密蘇里河聚居地](../Category/密蘇里河聚居地.md "wikilink")
[Category:蘇城都會區](../Category/蘇城都會區.md "wikilink")
[Category:1854年建立的聚居地](../Category/1854年建立的聚居地.md "wikilink")
[Category:1854年愛荷華州建立](../Category/1854年愛荷華州建立.md "wikilink")