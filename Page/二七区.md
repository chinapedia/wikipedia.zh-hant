**二七区**是[河南省](../Page/河南省.md "wikilink")[郑州市的一个市辖区](../Page/郑州市.md "wikilink")，1955年以前称郑州市第二区，处于城市交通结点，[郑州火车站位于此地](../Page/郑州火车站.md "wikilink")，商业繁荣。面积159平方千米，人口约49万。「二七區」的名稱源自於1923年在郑州發生的[二七大罷工事件](../Page/二七大罷工.md "wikilink")。\[1\]

## 行政区划

下辖13个[街道办事处](../Page/街道办事处.md "wikilink")、1个[乡](../Page/乡.md "wikilink")、1个[镇](../Page/镇.md "wikilink")：

  - 街道办事处：大学路街道、五里堡街道、一马路街道、解放路街道、德化街街道、铭功路街道、建中街街道、福华街街道、蜜蜂张街道、淮河路街道、嵩山路街道、长江路街道、京广路街道。
  - 乡：[侯寨乡](../Page/侯寨乡.md "wikilink")
  - 镇：[马寨镇](../Page/马寨镇.md "wikilink")

## 旅遊

  - [二七塔](../Page/二七塔.md "wikilink")
  - [二七廣場](../Page/二七廣場.md "wikilink")

## 參考

<references />

## 外部链接

  - [二七区连续三年入选中国综合实力和最具投资潜力百强区](http://zz.bendibao.com/wei/2016119/81558.shtm).2016-11-09

[二七区](../Page/category:二七区.md "wikilink")
[区](../Page/category:郑州区县市.md "wikilink")
[郑州](../Page/category:河南市辖区.md "wikilink")

1.  马丹，(2006年)，[郑州标志：二七塔结束无楹联历史](http://ha.people.com.cn/news/2006/01/27/86557.htm)
    ，[人民网](../Page/人民网.md "wikilink")：[河南視窗](../Page/河南視窗.md "wikilink")