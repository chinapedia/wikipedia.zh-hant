</ref> }}

《**超低能英雄**》（***Superhero
Movie***）是2008年的[喜劇](../Page/喜劇.md "wikilink")[電影](../Page/電影.md "wikilink")，承續《[驚聲尖笑](../Page/驚聲尖笑.md "wikilink")》（*Scary
Movie*）、《[史詩大帝國](../Page/史詩大帝國.md "wikilink")》（*Epic
Movie*）和《[正宗約會電影](../Page/正宗約會電影.md "wikilink")》（*Date
Movie*），主要諷刺[超級英雄電影](../Page/超級英雄電影.md "wikilink")。本片由[克雷格·麥辛](../Page/克雷格·麥辛.md "wikilink")（Craig
Mazin）負責導演及編劇。2007年9月在[洛杉磯開始進行製作](../Page/洛杉磯.md "wikilink")。

## 情節

高中生怪胎Rick Riker（Drake
Bell）被一隻基因改造的[蜻蜓叮咬後](../Page/蜻蜓.md "wikilink")，擁有了超強力量和裝甲皮膚超能力。他決定運用能力來打擊罪惡，自稱為「蜻蜓人（The
Dragonfly or The Mosquitoes）」。然而，他的對手the villainous Lou
Landers（Christopher McDonald），在一次實驗意外變成「沙漏人（The
Hourglass）」，擁有邪惡力量奪取人的生命力得到永生不朽。\[1\]

## 演員

  - [Drake Bell](../Page/Drake_Bell.md "wikilink")\[2\] 飾 瑞克·李克（Rick
    Riker） / 蜻蜓人（The Dragonfly）
  - [Sara Paxton](../Page/Sara_Paxton.md "wikilink")\[3\] 飾
    凱莉莎·辛普森（Carissa Simpson）
  - [Christopher
    McDonald](../Page/Christopher_McDonald_\(actor\).md "wikilink")\[4\]
    飾 路·蘭德斯（Lou Landers） / 沙漏人（The Hourglass）
  - [Ryan Hansen](../Page/Ryan_Hansen.md "wikilink")\[5\] 飾 藍斯·蘭德斯（Lance
    Landers）
  - [Jeffrey Tambor](../Page/Jeffrey_Tambor.md "wikilink")\[6\] 飾
    惠特比博士（Dr. Whitby）
  - [布伦特·斯派尔](../Page/布伦特·斯派尔.md "wikilink")\[7\] 飾 史特隆博士（Dr. Strom）
  - [Leslie Nielsen](../Page/Leslie_Nielsen.md "wikilink")\[8\] 飾
    阿爾伯特叔叔（Uncle Albert）
  - [Marion Ross](../Page/Marion_Ross.md "wikilink")\[9\] 飾 露西兒嬸嬸（Aunt
    Lucille）
  - [Kevin Hart](../Page/Kevin_Hart_\(actor\).md "wikilink")\[10\] 飾
    崔伊（Trey）
  - [Ryan Hansen](../Page/Ryan_Hansen.md "wikilink")\[11\] 飾
    曼尼（Manny）/螳螂（Mantis）
  - [Regina Hall](../Page/Regina_Hall.md "wikilink")\[12\] 飾
    史考皮亞（Scorpia）/蠍子（Scropion）
  - [Tracy Morgan](../Page/Tracy_Morgan.md "wikilink")\[13\] 飾
    史平尼（Spinny）/蜘蛛（Spider）

## 製作

**超低能英雄**於2006年4月在《[驚聲尖笑4](../Page/驚聲尖笑4.md "wikilink")》在[北美成功打出亮眼票房後首度公開](../Page/北美.md "wikilink")，電影起初預計在2007年2月9日上映。\[14\]
不過影片於2007年9月17日才實際在[洛杉磯開始進行製作](../Page/洛杉磯.md "wikilink")。\[15\]\[16\]
[大衛·薩克提到本片主要諷刺](../Page/大衛·薩克.md "wikilink")《[蜘蛛人](../Page/蜘蛛人.md "wikilink")》，同時也嘲諷了《[蝙蝠俠](../Page/蝙蝠俠.md "wikilink")》、《[X戰警](../Page/X戰警.md "wikilink")》、《[驚奇四超人](../Page/神奇四俠_\(電影\).md "wikilink")》和《[超人](../Page/超人.md "wikilink")》系列。
製作人也詳細說明「本片惡搞了所有英雄電影類型，如之前[驚聲尖笑系列](../Page/驚聲尖笑.md "wikilink")，但這次或許會更有整體性，像是笑彈龍虎榜（Naked
Gun）系列。」\[17\]

## 發行

**超低能英雄**在2008年3月28日上映。\[18\]

## 反映

### 評價

影評家多為給電影負面評價，根據2008年4月2日的電影評論網站[爛番茄](../Page/爛番茄.md "wikilink")，39篇影評中有15%的影評家給本片正面的評價，然而大部分的評論嚴厲地批評指出比起本片，先前的惡搞諷刺電影還比較可看，像是2007年的《[史詩大帝國](../Page/史詩大帝國.md "wikilink")》和近期的《[這不是斯巴達](../Page/這不是斯巴達.md "wikilink")》。[Metacritic報導根據](../Page/Metacritic.md "wikilink")13篇影評中，平均分數100分只得到33分，可看出本片為「一般負面評價」。

### 票房表現

電影首週票房，在上映的2960間戲院中，共有9,510,297美元票房收入，平均每個地方約三千二百一十二美元，在票房上排行第三。至到6月19日北美地區共收益25,787,144美元，加上海外票房29,298,186美元，總計全球票房高達55,085,330美元。

### 續集

[克雷格·麥辛宣稱將會出電影續集](../Page/克雷格·麥辛.md "wikilink")，[帕米拉·安德森](../Page/帕米拉·安德森.md "wikilink")、Drake
Bell和[萊斯里·尼爾森原班人馬全都續約接演第二集](../Page/萊斯里·尼爾森.md "wikilink")。克雷格說他將會惡搞[鋼鐵人](../Page/鋼鐵人.md "wikilink")，再加入更多的[X戰警](../Page/X戰警.md "wikilink")，Wooey
Kyle-Parks可能會加入團隊。

## 參見

## 外部連結

  - [官方網站](http://www.superhero-movie.net/)

  -
  -
[Category:2008年電影](../Category/2008年電影.md "wikilink")
[Category:美國電影作品](../Category/美國電影作品.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:喜劇片](../Category/喜劇片.md "wikilink")
[Category:惡搞電影](../Category/惡搞電影.md "wikilink")
[Category:美國惡搞片](../Category/美國惡搞片.md "wikilink")
[Category:超級英雄電影](../Category/超級英雄電影.md "wikilink")
[Category:昆虫電影](../Category/昆虫電影.md "wikilink")

1.

2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14.

15.

16.

17.

18.