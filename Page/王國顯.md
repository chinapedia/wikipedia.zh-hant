**王國顯**（**Kwok-Hin
Wong**）是一位[華裔美籍](../Page/華裔.md "wikilink")[基督教](../Page/基督教.md "wikilink")[傳道人和多產的](../Page/傳道人.md "wikilink")[福音](../Page/福音.md "wikilink")[作家](../Page/作家.md "wikilink")。

王國顯早年是[中國基督教](../Page/中國.md "wikilink")[家庭教會領袖](../Page/家庭教會.md "wikilink")[林獻羔的青年](../Page/林獻羔.md "wikilink")[同工](../Page/同工.md "wikilink")，因為他不願意加入政治宣傳式的[三自愛國教會](../Page/三自愛國教會.md "wikilink")，在1955年被捕入獄\[1\]\[2\]。1957年獲釋後，他獲准到[香港](../Page/香港.md "wikilink")，然後移民到[美國](../Page/美國.md "wikilink")\[3\]。現居[舊金山](../Page/舊金山.md "wikilink")，服事當地的[教會](../Page/教會.md "wikilink")。

## 著作

  - 《行過了死蔭的幽谷》──作者的自傳
  - 《在[基督裏長進](../Page/基督.md "wikilink")》
  - 《有一人比[所羅門更大](../Page/所羅門.md "wikilink")》
  - 《各房靠祂聯絡得合適》——教會生活的認識與操練
  - 《配得那世界的人》——[路加福音讀經劄記](../Page/路加福音.md "wikilink")
  - 《要在主裏同心》——[腓立比書讀經劄記](../Page/腓立比書.md "wikilink")
  - 《更美之約的中保》——[希伯來書讀經劄記](../Page/希伯來書.md "wikilink")
  - 《你們要住在主裏面》——[約翰壹、貳、叁書讀經劄記](../Page/約翰壹書.md "wikilink")
  - 《在曠野飄流四十年》——[民數記讀經劄記](../Page/民數記.md "wikilink")
  - 《你們要謹守遵行》——[申命記讀經劄記](../Page/申命記.md "wikilink")
  - 《你當剛強壯膽》——[約書亞記讀經劄記](../Page/約書亞記.md "wikilink")
  - 《[耶路撒冷中的歡聲](../Page/耶路撒冷.md "wikilink")》——[以斯拉記](../Page/以斯拉記.md "wikilink")、[尼希米記](../Page/尼希米記.md "wikilink")、[以斯帖記讀經劄記](../Page/以斯帖記.md "wikilink")
  - 《神眼中看為正的事》
  - 《[走天路的教會](../Page/走天路的教會.md "wikilink")》──教會歷史；[博饒本著](../Page/博饒本.md "wikilink")，梁素雅、王國顯譯

所有上述書籍均由香港晨星出版社發行。

## 外部連結

  - [House of God in San
    Francisco](https://web.archive.org/web/20081219102619/http://www.hgsf.net/sunday_message_ch.cfm)
    ──含王國顯的最近講辭的教會網站
  - [網站搜索結果](http://hgsf.net/search.php?s1=K.H.+Wong&image.x=23&image.y=2)
    ——王國顯弟兄主日講道列表
  - [Bible Study](http://hgsf.net/bible_study.php) ——王國顯弟兄帶領的聖經學習錄音

## 參考文獻

<div class="references-small">

<references />

</div>

[W](../Category/中国牧师.md "wikilink")
[Category:华裔美国人](../Category/华裔美国人.md "wikilink")
[Category:王姓](../Category/王姓.md "wikilink")

1.  自始至终，[耶和华都帮助我](../Page/耶和华.md "wikilink")——[广州市](../Page/广州市.md "wikilink")[大马站家庭教会林献羔弟兄的见证](../Page/大马站.md "wikilink")
2.  廣州《南方日报1955年9月15日》“广州的基督教内破获了一个以林献羔、王國顯、[張耀生為首的](../Page/張耀生.md "wikilink")[反革命集團](../Page/反革命.md "wikilink")”
3.  《行過了死蔭的幽谷》，王國顯著，1989年