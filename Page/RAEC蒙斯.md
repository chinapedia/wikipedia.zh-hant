**RAEC蒙斯**（**R.A.E.C.
Mons**）是[比利时足球俱乐部](../Page/比利时.md "wikilink")，位于[蒙斯](../Page/蒙斯.md "wikilink")。球队现为[比利时足球甲级联赛](../Page/比利时足球甲级联赛.md "wikilink")，俱乐部是2005-06赛季比利时乙级联赛的冠军，从而升入甲级行列。球队主体育场为沙勒斯·通德劳球场（Stade
Charles Tondreau）。

## 球队荣誉

  - **[比利时足球乙级联赛](../Page/比利时足球乙级联赛.md "wikilink")：**
      - **冠军（1次）:** 2005-06
  - **比利时足球乙级联赛决赛阶段：**
      - **冠军（1次）:** 2002
  - **[比利时足球丙A联赛](../Page/比利时足球丙A联赛.md "wikilink")：**
      - **冠军（1次）:** 1984-85

## 参考消息

  - [Official website](http://www.raec-mons.be)

[Category:比利时足球俱乐部](../Category/比利时足球俱乐部.md "wikilink")
[Category:1909年建立的足球俱樂部](../Category/1909年建立的足球俱樂部.md "wikilink")