**下郊**（[台羅](../Page/台羅.md "wikilink")：\[1\]），又稱為**廈郊**。下郊於[臺灣](../Page/臺灣.md "wikilink")，是指[臺灣清治時期](../Page/臺灣清治時期.md "wikilink")，居於[臺北](../Page/臺北.md "wikilink")[艋舺地區之](../Page/艋舺.md "wikilink")[泉州府](../Page/泉州府.md "wikilink")[同安縣](../Page/同安縣.md "wikilink")（今隸屬廈門）移民的商會。

## 簡介

由[明鄭時期至](../Page/明鄭時期.md "wikilink")[清代初年](../Page/清代.md "wikilink")，[臺南乃](../Page/臺南.md "wikilink")[臺灣的首府](../Page/臺灣.md "wikilink")，開發較盛，直到1709年，[陳賴章墾號得以開墾](../Page/陳賴章.md "wikilink")[大加臘之後](../Page/大加臘.md "wikilink")，新至的[福建移民多將未開墾的](../Page/福建.md "wikilink")[臺北盆地](../Page/臺北盆地.md "wikilink")，當成首要開發目標。

清朝18世紀中的[乾隆](../Page/乾隆.md "wikilink")、[嘉慶之後](../Page/嘉慶.md "wikilink")，大量[泉州漢族移民跨海沿著](../Page/泉州.md "wikilink")[淡水河靠岸定居臺北艋舺](../Page/淡水河.md "wikilink")，並與當地[平埔原住民通婚後人口大增](../Page/平埔族.md "wikilink")，艋舺因而大為興盛，於是產生了「[一府二鹿三艋舺](../Page/一府二鹿三艋舺.md "wikilink")」的說法。其中，移民於艋舺八甲-{庄}-（今[老松國小](../Page/老松國小.md "wikilink")）的泉州同安商人因為以[廈門交易為主](../Page/廈門.md "wikilink")，因此所設立的商業公會，亦稱為廈郊，後來轉音為下郊。

下郊商會因爭奪碼頭的利益，與[泉州三邑人組成的](../Page/泉州三邑.md "wikilink")[頂郊商會產生衝突](../Page/頂郊.md "wikilink")，1853年[三邑](../Page/三邑.md "wikilink")（[晉江](../Page/晉江.md "wikilink")、[南安](../Page/南安.md "wikilink")、[惠安](../Page/惠安.md "wikilink")）移民，由[艋舺龍山寺發兵](../Page/艋舺龍山寺.md "wikilink")，越過沼澤，燒毀[安溪移民的信仰中心](../Page/安溪.md "wikilink")[清水祖師廟](../Page/艋舺祖師廟.md "wikilink")，偷襲**下郊**的[泉州府](../Page/泉州府.md "wikilink")[同安人](../Page/同安.md "wikilink")，史稱[頂下郊拚](../Page/頂下郊拚.md "wikilink")。

艋舺的同安商人們敗逃往北方的[大龍峒](../Page/大龍峒.md "wikilink")，但不受當地同安移民接納，只好再轉到[大稻埕](../Page/大稻埕.md "wikilink")，沿著淡水河建起毗鄰店屋，形成街市，重建[霞海城隍廟](../Page/霞海城隍廟.md "wikilink")，利用[淡水河來從事對渡貿易](../Page/淡水河.md "wikilink")，形成以同安人為主的河港聚落區。

在[頂下郊拚後](../Page/頂下郊拚.md "wikilink")，艋舺的下郊就此消失。

## 備註

[Category:台北市歷史](../Category/台北市歷史.md "wikilink")
[Category:郊商](../Category/郊商.md "wikilink")
[Category:臺灣同業公會](../Category/臺灣同業公會.md "wikilink")

1.  臺灣閩南語常用詞辭典