**恩雅·帕翠西亞·伯倫南**（；；），[愛爾蘭共和國著名独立](../Page/愛爾蘭共和國.md "wikilink")[音樂家](../Page/音樂家.md "wikilink")。若以樂隊而言，恩雅一詞則包括：恩雅本人（負責作曲及演出）、Nicky
Ryan（負責專輯製作）、以及Roma Ryan（Nicky
Ryan之妻，負責填詞）。恩雅（Enya）此名實則是[愛爾蘭語名稱](../Page/愛爾蘭語.md "wikilink")（Eithne）在英語中的拼音。

## 開展音樂事業

恩雅於1961年出生於愛爾蘭共和國Donegal郡Gweedore鎮的一個音樂世家。她的祖父是一個在愛爾蘭巡迴演出的樂隊成員，而她父親在未經營酒吧生意前是一個名為Slieve
Foy Band的樂隊領隊，恩雅的母親則曾為一個舞蹈團成員，並在稍後時間於Gweedore Comprehensive
School任教音樂。此外，恩雅有四兄弟及四姊妹，其中幾位曾於1968年組成一隊名為An
Clann的樂隊（在1970年中改名為[Clannad樂隊](../Page/Clannad_\(樂團\).md "wikilink")）。

1980年，恩雅加入Clannad樂隊，樂隊成員包括她的兄弟姊妹，以及雙生的叔父Noel及Padraig Duggan。

在'' *（1980年）以及*Fuaim''
（1982年）這兩張專輯中，恩雅負責鍵盘演奏及唱背景音樂。1982年（在Clannad憑“Theme
From Harry's Game”成名前不久時），兼任製作人及經理的Nicky
Ryan離開Clannad樂隊。恩雅也同時離開樂隊與Nicky合作，開展她的個人事業。

## 获奖

  - 1993年[葛莱美奖](../Page/葛莱美奖.md "wikilink")，最佳新世纪音乐奖，获奖专辑《牧羊人之月》（*Shepherd
    Moons*）
  - 1997年葛莱美奖，最佳新世纪音乐奖，获奖专辑《树的回忆》（*The Memory of the Trees*）
  - 2002年葛莱美奖，最佳新世纪音乐奖，获奖专辑《雨过天晴》（*A Day without Rain*）
  - 2007年葛莱美奖，最佳新世纪音乐奖，获奖专辑 *Amarantine*
  - 2002年[奥斯卡最佳原创歌曲奖提名](../Page/奥斯卡最佳原创歌曲奖.md "wikilink")，提名歌曲 *May It
    Be*
  - 2002年[金球奖最佳原创歌曲奖提名](../Page/金球奖最佳原创歌曲.md "wikilink")，提名歌曲 *May It
    Be*

## 個人專輯/作品年表

1986年，恩雅為電影*The Frog Prince*（青蛙王子）配樂。

1987年，恩雅為BBC的特別節目*The Celts*（凯爾特人）配樂，BBC為這些配樂發行一張名為''enya
''的專輯。因為這張專輯使得恩雅的音樂受到wea總裁Rob
Dickins的注意，並與其簽下唱片合約。

1988年，恩雅在wea的第一張正式個人專輯*Watermark*（水印）發行。

1991年，第二張專輯*Shepherd Moons*（牧羊人之月）發行。

1992年，wea取得專輯''enya *的發行權，經過重新編曲混音後，更名為*The Celts''（凯爾特人）全球發行。

1995年，第四張專輯*The Memory of the Trees*（樹的回憶）發行。

1998年，精選輯*Paint The Sky with
Stars*（[星空彩繪](../Page/星空彩繪.md "wikilink")）發行。

2000年，第五張專輯*A Day without Rain*（雨過天晴）發行。

2001年，恩雅為電影*The Lord of the Rings: The Fellowship of the
Ring*（魔戒首部曲：魔戒現身）譜寫並演唱主題曲“May It Be”。

2005年，第六張專輯*Amarantine*（永恆之約）發行。

2008年，第七張專輯*And Winter Came*（冬季降临）于11月10日發行。

2009年，精選輯'' The Very Best of Enya''（恩雅 / 極緻典藏：跨世紀精選）發行。

2015年，第八張專輯'' Dark Sky Island ''（暗天之島）於11月20日發行。

## 参考文献

## 外部連結

  - [Enya.com](http://www.enya.com/) – 恩雅官方網站

  - [Enya](http://www.imdb.com/name/nm0258216/) 在 IMDB

  - \[ Enya\] 於 [Allmusic](../Page/Allmusic.md "wikilink")

  -
  -
{{-}}

[Category:葛莱美奖获得者](../Category/葛莱美奖获得者.md "wikilink")
[Category:1961年出生](../Category/1961年出生.md "wikilink")
[Category:新世纪音乐家](../Category/新世纪音乐家.md "wikilink")
[Category:愛爾蘭女歌手](../Category/愛爾蘭女歌手.md "wikilink")
[Category:華納音樂集團旗下歌手](../Category/華納音樂集團旗下歌手.md "wikilink")
[Category:世界音樂獎獲得者](../Category/世界音樂獎獲得者.md "wikilink")
[Category:愛爾蘭天主教徒](../Category/愛爾蘭天主教徒.md "wikilink")