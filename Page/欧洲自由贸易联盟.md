**歐洲自由貿易協會**（，縮寫为）是[歐洲的一個促进贸易的組織](../Page/歐洲.md "wikilink")。该組織於1960年5月3日成立。當時參加的成員國包括[英國](../Page/英國.md "wikilink")、[葡萄牙](../Page/葡萄牙.md "wikilink")、[瑞士](../Page/瑞士.md "wikilink")、[奧地利](../Page/奧地利.md "wikilink")、[丹麥](../Page/丹麥.md "wikilink")、[瑞典及](../Page/瑞典.md "wikilink")[挪威](../Page/挪威.md "wikilink")。

歐洲自由貿易協會會議於1960年1月4日於[瑞典首都](../Page/瑞典.md "wikilink")[斯德哥爾摩舉行](../Page/斯德哥爾摩.md "wikilink")，參與的國家包括上述的七個[國家](../Page/國家.md "wikilink")，並合稱為「外七國」。時至今天，聯盟只餘下[挪威](../Page/挪威.md "wikilink")、[冰島](../Page/冰島.md "wikilink")、[瑞士及](../Page/瑞士.md "wikilink")[列支敦士登](../Page/列支敦士登.md "wikilink")，其中只有[挪威和](../Page/挪威.md "wikilink")[瑞士是創會國](../Page/瑞士.md "wikilink")。現在[斯德哥爾摩會議已經由](../Page/斯德哥爾摩會議.md "wikilink")[瓦都茲會議所取代](../Page/瓦都茲.md "wikilink")。

這個會議給予成員國更進一步的貿易自由，其中三個國家都加入了[歐洲經濟區](../Page/歐洲經濟區.md "wikilink")，與[歐盟有更大的合作機遇](../Page/歐盟.md "wikilink")。而歐洲自由貿易聯盟當中只有[瑞士並未加入歐洲經濟區](../Page/瑞士.md "wikilink")，原因是[公投被公民否決](../Page/公投.md "wikilink")。瑞士另外透過參與歐洲單一市場。

## 成員國歷史

[
](https://zh.wikipedia.org/wiki/File:EFTA_AELE_countries_and_former_members.svg "fig: ")創會的成員國包括[英國](../Page/英國.md "wikilink")、[葡萄牙](../Page/葡萄牙.md "wikilink")、[瑞士](../Page/瑞士.md "wikilink")、[奧地利](../Page/奧地利.md "wikilink")、[丹麥](../Page/丹麥.md "wikilink")、[瑞典及](../Page/瑞典.md "wikilink")[挪威](../Page/挪威.md "wikilink")；1960年代，這些國家被稱為外七國，與[歐洲經濟共同體的](../Page/欧洲经济共同体.md "wikilink")[內六國有所區別](../Page/內六國.md "wikilink")。\[1\]
1961年，[芬蘭亦於加入成為準會員國](../Page/芬蘭.md "wikilink")，並於1986年成為正式會員；

1970年，[冰島於亦加入了聯盟](../Page/冰島.md "wikilink")；

1973年，英國及丹麥加入[歐洲共同體](../Page/歐洲共同體.md "wikilink")，因而退出了聯盟；

1986年，[葡萄牙亦於正式退出聯盟並加入](../Page/葡萄牙.md "wikilink")[歐洲經濟共同體](../Page/欧洲经济共同体.md "wikilink")；

1991年，[列支敦士登加入聯盟](../Page/列支敦士登.md "wikilink")；

1995年，[奧地利](../Page/奧地利.md "wikilink")、[芬蘭和](../Page/芬蘭.md "wikilink")[瑞典都退出了聯盟並且加入](../Page/瑞典.md "wikilink")[歐盟](../Page/歐盟.md "wikilink")。

## 現時成員國

<table>
<thead>
<tr class="header">
<th><p>國旗</p></th>
<th><p>國家</p></th>
<th><p>加入時間</p></th>
<th><p>人口</p></th>
<th><p>面積 <small>(km²)</small></p></th>
<th><p>首都</p></th>
<th><p><a href="../Page/GDP.md" title="wikilink">GDP</a>（亿美元）</p></th>
<th><p><a href="../Page/人均GDP.md" title="wikilink">人均GDP</a>（美元）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p><a href="../Page/冰島.md" title="wikilink">冰島</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="../Page/雷克雅未克.md" title="wikilink">雷克雅未克</a></p></td>
<td><p>121.33</p></td>
<td><p>37,976</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/列支敦士登.md" title="wikilink">列支敦士登</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="../Page/瓦都茲.md" title="wikilink">瓦都茲</a></p></td>
<td><p>47.97</p></td>
<td><p>134,045</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/挪威.md" title="wikilink">挪威</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="../Page/奧斯陸.md" title="wikilink">奧斯陸</a></p></td>
<td><p>5,158.32</p></td>
<td><p>101,271</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/瑞士.md" title="wikilink">瑞士</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="../Page/伯爾尼.md" title="wikilink">伯爾尼</a></p></td>
<td><p>6,461.99</p></td>
<td><p>80,276</p></td>
</tr>
</tbody>
</table>

## 前成員國

<table>
<thead>
<tr class="header">
<th><p>國旗</p></th>
<th><p>國家</p></th>
<th><p>首都</p></th>
<th><p><a href="../Page/GDP.md" title="wikilink">GDP</a>（亿美元）</p></th>
<th><p><a href="../Page/人均GDP.md" title="wikilink">人均GDP</a>（美元）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p><a href="../Page/英国.md" title="wikilink">英国</a></p></td>
<td><p><a href="../Page/伦敦.md" title="wikilink">伦敦</a></p></td>
<td><p>26,785</p></td>
<td><p>40,879</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/葡萄牙.md" title="wikilink">葡萄牙</a></p></td>
<td><p><a href="../Page/里斯本.md" title="wikilink">里斯本</a></p></td>
<td><p>2,192.89</p></td>
<td><p>20,633</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/丹麦.md" title="wikilink">丹麦</a></p></td>
<td><p><a href="../Page/哥本哈根.md" title="wikilink">哥本哈根</a></p></td>
<td><p>3,136.37</p></td>
<td><p>56,202</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/瑞典.md" title="wikilink">瑞典</a></p></td>
<td><p><a href="../Page/斯德哥尔摩.md" title="wikilink">斯德哥尔摩</a></p></td>
<td><p>5,520.42</p></td>
<td><p>57,297</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/芬兰.md" title="wikilink">芬兰</a></p></td>
<td><p><a href="../Page/赫尔辛基.md" title="wikilink">赫尔辛基</a></p></td>
<td><p>2,501.26</p></td>
<td><p>46,098</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/奥地利.md" title="wikilink">奥地利</a></p></td>
<td><p><a href="../Page/维也纳.md" title="wikilink">维也纳</a></p></td>
<td><p>3,985.94</p></td>
<td><p>47,083</p></td>
</tr>
</tbody>
</table>

## 成立

[歐洲煤鋼共同體成立時](../Page/欧洲煤钢共同体.md "wikilink")，歐洲各國曾經好意邀請，希望[英國能加入組織](../Page/英國.md "wikilink")，可是，由於英國人自恃有[英聯邦國家及](../Page/英聯邦.md "wikilink")[美國的貿易支持](../Page/美國.md "wikilink")，加上認為會失去[主權及控制國內經濟的權利](../Page/主權.md "wikilink")，最後就表明沒有打算參加。

幾年後，[歐洲煤鋼共同體重組成](../Page/歐洲煤鋼共同體.md "wikilink")[歐洲經濟共同體](../Page/歐洲經濟共同體.md "wikilink")，經濟發展非常迅速，亦從[二次大戰中的陰影走出來](../Page/二次大戰.md "wikilink")。英國隨後看到了歐洲重建的好處，不過依然不願配合煤鋼共同體的政策，遂自起爐灶，唯有與[葡萄牙](../Page/葡萄牙.md "wikilink")、[瑞士](../Page/瑞士.md "wikilink")、[奧地利](../Page/奧地利.md "wikilink")、[丹麥](../Page/丹麥.md "wikilink")、[瑞典及](../Page/瑞典.md "wikilink")[挪威共同成立歐洲自由貿易聯盟](../Page/挪威.md "wikilink")，希望可以得到和[歐洲經濟共同體一樣的成果](../Page/歐洲經濟共同體.md "wikilink")，可是卻始終未如人意，該組織實在比不上[內六國所成立的EEC](../Page/內六國.md "wikilink")，ETFA對比EEC下歐洲的強大實力還有所距離。

英國只好于1961年同其他EFTA中的四国（英国、丹麦、、挪威）申請加入[歐洲經濟共同體](../Page/歐洲經濟共同體.md "wikilink")（EEC），可是[法國總統](../Page/法國.md "wikilink")[夏爾·戴高樂認為英國的加入等於其盟友美國也能下指導棋](../Page/夏爾·戴高樂.md "wikilink")，會影響法國在組織中的領導地位，加上心中也不滿英國曾經反口卻還能加入，從而在1963年拒絕英國人的加入意願。

直至1973年，英国、爱尔兰、丹麦才成功加入EEC，挪威也被允许加入EEC；但是在1972年的公投中，民意显示挪威不愿加入EEC。

## 位置

聯盟的總部位於[瑞士的](../Page/瑞士.md "wikilink")[日內瓦](../Page/日內瓦.md "wikilink")。聯盟的監察部門則位於非會員國[比利時的](../Page/比利時.md "wikilink")[布魯塞爾](../Page/布魯塞爾.md "wikilink")\[2\]（[歐盟總部都是位於布魯塞爾](../Page/歐盟.md "wikilink")），而聯盟的法庭則與[歐洲法院一樣位於](../Page/欧洲法院.md "wikilink")[盧森堡](../Page/盧森堡.md "wikilink")\[3\]。

## 葡萄牙基金

葡萄牙基金於1975年創立，當時[葡萄牙還是聯盟的成員國](../Page/葡萄牙.md "wikilink")。聯盟會向所有[葡萄牙前殖民地提供金錢援助](../Page/葡萄牙.md "wikilink")，支持國家的發展。當[葡萄牙退出聯盟後](../Page/葡萄牙.md "wikilink")，聯盟決定繼續援助，令所有[葡萄牙前殖民地繼續受益](../Page/葡萄牙.md "wikilink")。基金由聯盟成員國以低息貸款100萬[美元予](../Page/美元.md "wikilink")[葡萄牙以支持其援助](../Page/葡萄牙.md "wikilink")。原本應該於1988年還款，但後來延至1998年。現在，基金已經停止運作。

## 國際關係

聯盟除與[歐盟有自由貿易協議外](../Page/歐盟.md "wikilink")，與世界各地都在不同的領域中有合作。現在，聯盟與20個國家都有自由貿易協定，再加上歐盟的28個國家，總共與47個國家沒有貿易的限制。

**自由貿易協定**

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  - [南部非洲關稅同盟](../Page/南部非洲關稅同盟.md "wikilink")

<!-- end list -->

  -
  -
  -
  -
  -
<!-- end list -->

  -
  -
現在聯盟正與[泰國及](../Page/泰國.md "wikilink")[海灣阿拉伯國家合作委員會](../Page/海灣阿拉伯國家合作委員會.md "wikilink")（、、、、和）協商合作。

**宣佈合作**

  -
  -
  -
  - [南方共同市場](../Page/南方共同市場.md "wikilink") （、、、）

  -
  -
  -
**工作組合作**

  -
  -
## 參見

  - [歐洲共同體](../Page/歐洲共同體.md "wikilink")
  - [歐盟擴大](../Page/歐盟擴大.md "wikilink")
  - [歐盟](../Page/歐盟.md "wikilink")
  - [歐洲經濟區](../Page/歐洲經濟區.md "wikilink")
  - [申根公約](../Page/申根公約.md "wikilink")

## 參考資料

<references/>

## 外部連結

  - [Official EFTA site](http://www.efta.int)

<!-- end list -->

  - [The European Commission's Delegation to Norway and
    Iceland](https://web.archive.org/web/20090130081953/http://www.europakommisjonen.no/en/index.htm)
  - [European Free Trade
    Association](http://www.cvce.eu/obj/convention_establishing_the_european_free_trade_association_consolidated_version-en-ad834690-735a-42d2-a8e8-d9055619805c.html)
    CVCE

[category:欧洲经济](../Page/category:欧洲经济.md "wikilink")
[category:欧洲国际性组织](../Page/category:欧洲国际性组织.md "wikilink")
[category:国际贸易](../Page/category:国际贸易.md "wikilink")

[Category:1960年建立](../Category/1960年建立.md "wikilink")

1.  [Now, the Seven and a
    Half](http://www.time.com/time/magazine/article/0,9171,874317,00.html)
2.  [EFTA Surveillance Authority](http://www.eftasurv.int/)
3.  [EFTA Court](http://www.eftacourt.lu/)