是一部於1976年上映的[香港電影](../Page/香港電影.md "wikilink")，由[梁普智和](../Page/梁普智.md "wikilink")[蕭芳芳導演](../Page/蕭芳芳.md "wikilink")，由[陳欣健編劇](../Page/陳欣健.md "wikilink")；主要演員為、[蕭芳芳和](../Page/蕭芳芳.md "wikilink")[陳惠敏](../Page/陳惠敏.md "wikilink")\[1\]。《跳灰》被視為[香港1970年代](../Page/香港1970年代.md "wikilink")「[新浪潮電影](../Page/香港新浪潮.md "wikilink")」的先驅，其拍攝手法、劇本及人物描寫皆與之前的香港電影大為不同。《跳灰》拍攝於[廉政公署成立後](../Page/廉政公署_\(香港\).md "wikilink")，描寫[警務處對付](../Page/香港警務處.md "wikilink")[毒販的故事](../Page/毒品.md "wikilink")。[編劇陳欣健出身於](../Page/編劇.md "wikilink")[香港警界](../Page/香港警界.md "wikilink")，故此對劇中的警察描寫寫實，而且為具有正義感之正面人物，與此前香港電影中形象化的[探長角色截然不同](../Page/探長.md "wikilink")；故此，《跳灰》亦被視為香港電影中首部真正的警匪片。除此以外，電影兩首歌曲〈[大丈夫](../Page/大丈夫.md "wikilink")〉及〈[問我](../Page/問我.md "wikilink")〉至今仍然是[粵語流行曲中的經典](../Page/粵語流行曲.md "wikilink")。

## 內容

一宗發生於[荷蘭之凶殺案](../Page/荷蘭.md "wikilink")，被懷疑是商人董祥德所指使。香港警方派探員梁嘉倫調查此案。董祥德表面是正當商人，實則是毒販，暗地進行販毒行為。而由於牽涉層面太大，梁嘉倫在查到中途被上司勒令放假三個月，但其間梁仍暗中繼續調查，至拘捕董祥德為止。

## 角色

  - [蕭芳芳](../Page/蕭芳芳.md "wikilink") 飾 佩珊
  - [梁嘉倫](../Page/梁嘉倫.md "wikilink") 飾 梁嘉倫
  - [林偉祺](../Page/林偉祺.md "wikilink") 飾 董祥德
  - [陳星](../Page/陳星.md "wikilink") 飾 笑面虎
  - [陳惠敏](../Page/陳惠敏.md "wikilink") 飾 殺手
  - [盧海鵬](../Page/盧海鵬.md "wikilink") 飾 彭沙展
  - [李志中](../Page/李志中.md "wikilink") 飾 凌警司
  - [胡楓](../Page/胡楓.md "wikilink") 飾 方醫生
  - [尹多明](../Page/尹多明.md "wikilink") 飾 驗屍官
  - [區樹湛](../Page/區樹湛.md "wikilink") 飾 老鼠仔
  - [楊貫一](../Page/楊貫一.md "wikilink") 飾 翁律師
  - [陳積奇](../Page/陳積奇.md "wikilink") 飾 練馬師

## 關於戲名

  - 「糶」即「賣」，音「跳」\[2\]；毒品[海洛英](../Page/海洛英.md "wikilink")，粵語俗稱「白粉」，[黑語為](../Page/黑語.md "wikilink")「灰」；「糶灰」即賣白粉、販毒，取諧音俗作「跳灰」。

## 電影歌曲

  - 主題曲〈大丈夫〉 作曲：[劉家昌](../Page/劉家昌.md "wikilink")
    填詞：[黃霑](../Page/黃霑.md "wikilink")
    主唱：[關正傑](../Page/關正傑.md "wikilink")
  - 插曲〈問我〉 作曲：[黎小田](../Page/黎小田.md "wikilink") 填詞：黃霑
    主唱：[陳麗斯](../Page/陳麗斯.md "wikilink")

## 關連項目

  - [第凡內早餐](../Page/第凡內早餐_\(電影\).md "wikilink")

## 注釋

<references />

## 外部連結

  -
  -
  -
  -
  -
[category:香港動作片](../Page/category:香港動作片.md "wikilink")
[category:香港警匪片](../Page/category:香港警匪片.md "wikilink")

[6](../Category/1970年代香港電影作品.md "wikilink")

1.
2.  相反的是「糴」即「買」，音「笛」