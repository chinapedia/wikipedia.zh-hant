《**千王之王**》（）是[香港](../Page/香港.md "wikilink")[電視廣播有限公司製作的民初](../Page/電視廣播有限公司.md "wikilink")[劇集](../Page/劇集.md "wikilink")，於1980年9月15日首播，由[謝賢](../Page/謝賢.md "wikilink")、[汪明荃](../Page/汪明荃.md "wikilink")、[楊群](../Page/楊群.md "wikilink")、[任達華](../Page/任達華.md "wikilink")、[曾慶瑜](../Page/曾慶瑜.md "wikilink")、[郭鋒和](../Page/郭鋒.md "wikilink")[雪梨主演](../Page/雪梨.md "wikilink")，由[鬼手阿建出任技术指导](../Page/鬼手阿建.md "wikilink")，共25集。主題曲《用愛將心偷》由[顧嘉煇作曲](../Page/顧嘉煇.md "wikilink")、[黃霑填詞](../Page/黃霑.md "wikilink")；[汪明荃主唱](../Page/汪明荃.md "wikilink")。

本劇于2007年5月21日在[TVB星河頻道](../Page/TVB星河頻道.md "wikilink")、2017年11月13日在[TVB經典台](../Page/TVB經典台.md "wikilink")、2018年9月13日在[翡翠台重播](../Page/翡翠台.md "wikilink")。

## 故事大綱

故事發生於1930年的廣州，當時賭業蓬勃，土豪洪彪、洪豹父子企圖吞併廣州全部賭場，幸得有「南神眼」之稱的羅四海（[謝賢飾](../Page/謝賢.md "wikilink")）鎮壓搗亂，但洪彪父子因此而懷恨在心。譚小棠（[汪明荃飾](../Page/汪明荃.md "wikilink")）是當時著名花旦，而弟譚昇（[任達華飾](../Page/任達華.md "wikilink")）因在賭場出千而被四海所擒，小棠為救弟與四海談判，期後更發展出一段感人的愛情故事。洪彪陷害四海不遂，遂聘有北方千王之稱的「北千手」卓一夫（[楊群飾](../Page/楊群.md "wikilink")）與四海鬥法，結果兩人打成平手。洪彪唯有改以收買南北千王於旗下。在一次鴉片買賣中，南北千王被洪彪利用，幸得小棠姊弟相救。可惜兩人最終逃不過洪彪父子的種種陰謀，四海因致雙目失明，而一夫則被暗藏禍心的養女卓莉（[曾慶瑜飾](../Page/曾慶瑜.md "wikilink")）挑斷手筋。為報血海深仇，兩人決集千術之精華授予譚昇，而譚昇因此得以擊敗卓莉，但是卓莉卻因她的兒子遭譚昇殺害而精神崩潰，從而看到幻象從山坡上跌下身亡；譚昇得到「千王之王」的稱號成為最終的贏家。

## 演員表

  - [謝賢](../Page/謝賢.md "wikilink") 飾 羅四海
  - [汪明荃](../Page/汪明荃.md "wikilink") 飾 譚小棠
  - [楊群](../Page/楊群.md "wikilink") 飾 卓一夫
  - [任達華](../Page/任達華.md "wikilink") 飾 譚昇
  - [曾慶瑜](../Page/曾慶瑜.md "wikilink") 飾 卓莉
  - [雪梨](../Page/嚴惠明.md "wikilink") 飾 洪盈盈
  - [陳嘉儀](../Page/陳嘉儀.md "wikilink") 飾 容惠清
  - [秦煌](../Page/秦煌.md "wikilink") 飾 花天嬌
  - [黃新](../Page/黃新.md "wikilink") 飾 洪彪
  - [郭鋒](../Page/郭鋒_\(演員\).md "wikilink") 飾 洪豹
  - [黃樹棠](../Page/黃樹棠.md "wikilink") 飾 楊堅
  - [杜平](../Page/杜平_\(演員\).md "wikilink") 飾 徐老健
  - [周吉](../Page/周吉.md "wikilink") 飾 師爺陳
  - [周驄](../Page/周驄.md "wikilink") 飾 霍萬庭
  - [羅蘭](../Page/羅蘭_\(香港\).md "wikilink") 飾 霍太
  - [金興賢](../Page/金興賢.md "wikilink") 飾 賀川
  - [譚炳文](../Page/譚炳文.md "wikilink") 飾 賀容
  - [陳百祥](../Page/陳百祥.md "wikilink") 飾 招積仔
  - [林丹鳳](../Page/林丹鳳.md "wikilink") 飾 女同學
  - [文潔雲](../Page/文潔雲.md "wikilink") 飾 女同學
  - [陳麗華](../Page/陳麗華.md "wikilink") 飾 女同學
  - [陳安瑩](../Page/陳安瑩.md "wikilink") 飾 女同學
  - [許逸華](../Page/許逸華.md "wikilink") 飾 女同學/少女
  - [梁潔芳](../Page/梁潔芳.md "wikilink") 飾 校長/花旦
  - [胡寶華](../Page/胡寶華.md "wikilink") 飾 女友
  - [李清薇](../Page/李清薇.md "wikilink") 飾 阿葵
  - [陳國權](../Page/陳國權.md "wikilink") 飾 阿九
  - [黎永強](../Page/黎永強.md "wikilink") 飾 阿東
  - [葉萍](../Page/葉萍.md "wikilink") 飾 月姐/阿丹
  - [黃敏儀](../Page/黃敏儀.md "wikilink") 飾 周曼兒
  - [黎壁光](../Page/黎壁光.md "wikilink") 飾 打手/羅手下
  - [廖骏雄](../Page/廖骏雄.md "wikilink") 飾 胖子打手/羅手下（第1集）
  - [虞堂容](../Page/虞堂容.md "wikilink") 飾 打手/羅手下
  - [羅振彪](../Page/羅振彪.md "wikilink") 飾 打手/羅手下
  - [梁鴻華](../Page/梁鴻華.md "wikilink") 飾 打手/羅手下
  - [李成昌](../Page/李成昌.md "wikilink") 飾 打手/羅手下/火車服務員
  - [俞明](../Page/俞明.md "wikilink") 飾 吳超
  - [張活游](../Page/張活游.md "wikilink") 飾 李旺
  - [魯振順](../Page/魯振順.md "wikilink") 飾 戲班人/小武
  - [白文彪](../Page/白文彪.md "wikilink") 飾 老金
  - [潘友聲](../Page/潘友聲.md "wikilink") 飾 阮玉郎
  - [鄺佐輝](../Page/鄺佐輝.md "wikilink") 飾 火神輝
  - [黃宗賜](../Page/黃宗賜.md "wikilink") 飾 文仔
  - [曾玉霞](../Page/曾玉霞.md "wikilink") 飾 姨太
  - [楊炎棠](../Page/楊炎棠.md "wikilink") 飾 阿發
  - [劉丹](../Page/劉慶基.md "wikilink") 飾 阿江
  - [上官玉](../Page/上官玉.md "wikilink") 飾 韓媽
  - [李潔瑩](../Page/李潔瑩.md "wikilink") 飾 老婦
  - [鄭少萍](../Page/鄭少萍.md "wikilink") 飾 農婦
  - [廖啟智](../Page/廖啟智.md "wikilink") 飾 阿滿
  - [張俊平](../Page/張俊平.md "wikilink") 飾 變態青年
  - [黃志偉](../Page/黃志偉.md "wikilink") 飾 黑仔/打手甲/保鏢
  - [黃國輝](../Page/黃國輝.md "wikilink") 飾 華哥
  - [吳博君](../Page/吳博君.md "wikilink") 飾 徐手下/保鏢
  - [羅偉平](../Page/羅偉平.md "wikilink") 飾 徐手下/保鏢
  - [梁潔華](../Page/梁潔華_\(演員\).md "wikilink") 飾 小　紅
  - [林偉圖](../Page/林偉圖.md "wikilink") 飾 王探長
  - [梁少狄](../Page/梁少狄.md "wikilink") 飾 昇兄弟
  - [李揚道](../Page/李揚道.md "wikilink") 飾 昇兄弟
  - [徐佑麟](../Page/徐佑麟.md "wikilink") 飾 艇家
  - [嚴秋華](../Page/嚴秋華.md "wikilink") 飾 小頭目
  - [黃宗賜](../Page/黃宗賜.md "wikilink") 飾 健仔
  - [歐炳南](../Page/歐炳南.md "wikilink") 飾 苦力頭/劉勇
  - [白蘭](../Page/白蘭.md "wikilink") 飾 佣婦
  - [湯鎮業](../Page/湯鎮業.md "wikilink") 飾 小劉
  - [陳嘉碧](../Page/陳嘉碧.md "wikilink") 飾 劉妻
  - [吳子山](../Page/吳子山.md "wikilink") 飾 小偷
  - [曹濟](../Page/曹濟.md "wikilink") 飾 車佚
  - \-{[陳有后](../Page/陳有后.md "wikilink")}- 飾 店東
  - [何璧堅](../Page/何璧堅.md "wikilink") 飾 班主祥叔/法官
  - [江毅](../Page/江毅.md "wikilink") 飾 江導演
  - [馬慶生](../Page/馬慶生.md "wikilink") 飾 部長
  - [容守怡](../Page/容守怡.md "wikilink") 飾 洪太
  - [胡美儀](../Page/胡美儀.md "wikilink") 飾 二姨太
  - [劉雅麗](../Page/劉雅麗_\(1970年代\).md "wikilink") 飾 四姨太
  - [韓江](../Page/韓江_\(演員\).md "wikilink") 飾 忠伯
  - [陳東](../Page/陳東_\(演員\).md "wikilink") 飾 牛叔
  - 陳強 飾 順伯
  - [艾威](../Page/艾威.md "wikilink") 飾 赌客（第1集）/豹手下
  - [鄭藩生](../Page/鄭藩生.md "wikilink") 飾 鬼眼
  - FARKAS 飾 亨特警司
  - [林玉麟](../Page/林玉麟.md "wikilink") 飾 警司
  - [梁少狄](../Page/梁少狄.md "wikilink") 飾 探員
  - [張生](../Page/張生.md "wikilink") 飾 老人
  - [徐威信](../Page/徐威信.md "wikilink") 飾 守衛
  - [野峰](../Page/野峰.md "wikilink") 飾 王逸祥
  - [李建川](../Page/李建川.md "wikilink") 飾 記者
  - [譚玉瑛](../Page/譚玉瑛.md "wikilink") 飾 記者
  - [林偉健](../Page/林偉健.md "wikilink") 飾 打手甲
  - [楊嘉諾](../Page/楊嘉諾.md "wikilink") 飾 侍應
  - [羅國維](../Page/羅國維.md "wikilink") 飾 獄長
  - [黃日華](../Page/黃日華.md "wikilink") 飾 獄卒
  - [苗僑偉](../Page/苗僑偉.md "wikilink") 飾 獄卒/苗先生 - 南北千王初遇，麻雀對決四人之一
  - [馮耀宗](../Page/馮耀宗.md "wikilink") 飾 獄卒
  - [周鼎元](../Page/周鼎元.md "wikilink") 飾 昇手下
  - [陳榮輝](../Page/陳榮輝.md "wikilink") 飾 昇手下
  - [陳子健](../Page/陳子健.md "wikilink") 飾 洪輝(譚輝)
  - [麥子雲](../Page/麥子雲.md "wikilink") 飾 近身
  - [甘国卫](../Page/甘國衛.md "wikilink") 飾 陈大文（陈大少）（第1集）
  - [李国麟](../Page/李國麟_\(演員\).md "wikilink") 飾 赌客（第1集）
  - [陳勉良](../Page/陳勉良.md "wikilink") 飾 荷官（第2集）
  - [陳狄克](../Page/陳狄克.md "wikilink") 飾 快手張（第2集）
  - [鄭家生](../Page/鄭家生.md "wikilink") 飾 快手張手下（第2集）
  - [焦 雄](../Page/焦雄.md "wikilink") 飾 駱冰（第2集）
  - 虞天伟 飾 荷官（第2集）

## 歌曲

  - 主題曲《用愛將心偷》

<!-- end list -->

  - 作曲：[顧嘉煇](../Page/顧嘉煇.md "wikilink")
  - 填詞：[黃霑](../Page/黃霑.md "wikilink")
  - 主唱：[汪明荃](../Page/汪明荃.md "wikilink")

<!-- end list -->

  - 插曲《輕舟情萬縷》

<!-- end list -->

  - 作曲：顧嘉煇
  - 填詞：[鄧偉雄](../Page/鄧偉雄.md "wikilink")
  - 主唱：汪明荃

<!-- end list -->

  - 插曲《美麗原野》

<!-- end list -->

  - 作曲：顧嘉煇
  - 填詞：黃霑
  - 主唱：汪明荃

<!-- end list -->

  - 插曲《歸帆》

<!-- end list -->

  - 作曲：顧嘉煇
  - 填詞：[江羽](../Page/江羽.md "wikilink")
  - 主唱：汪明荃

<!-- end list -->

  - 插曲《寧願給你欺騙》

<!-- end list -->

  - 作曲：顧嘉煇
  - 填詞：黃霑
  - 主唱：汪明荃

<!-- end list -->

  - 插曲《情枷愛鎖》

<!-- end list -->

  - 作曲：顧嘉煇
  - 填詞：黃霑
  - 主唱：汪明荃

## 参见

  - [千王之王重出江湖](../Page/千王之王重出江湖.md "wikilink")

## 參考資料

<references/>

## 外部連結

  - [《千王之王》 GOTV
    第1集重溫](https://web.archive.org/web/20140222163410/http://gotv.tvb.com/programme/103638/156378/)

[Category:1980年無綫電視劇集](../Category/1980年無綫電視劇集.md "wikilink")
[Category:無綫電視民初背景劇集](../Category/無綫電視民初背景劇集.md "wikilink")
[Category:廣州市背景電視劇](../Category/廣州市背景電視劇.md "wikilink")
[Category:賭博題材作品](../Category/賭博題材作品.md "wikilink")