**布魯塞爾條約**簽署於1948年3月17日，簽署國包括[比利時](../Page/比利時.md "wikilink")、[法國](../Page/法國.md "wikilink")、[盧森堡](../Page/盧森堡.md "wikilink")、[荷蘭以及](../Page/荷蘭.md "wikilink")[英國](../Page/英國.md "wikilink")，是前一年英法間防禦協定[敦克爾克条約的延伸](../Page/敦克爾克条約.md "wikilink")。

## 西歐聯盟

作為戰後歐洲安全合作上的一項努力，在歐洲共同防禦的考量上，布魯塞爾協定也堪稱促成[北大西洋公約組織的先驅](../Page/北大西洋公約組織.md "wikilink")。不過兩者在抵禦的對象上考量不同：本條約純粹主在抵禦德國，然而隔年的[北大西洋公約則因東西分裂情勢無可避免](../Page/北大西洋公約.md "wikilink")、[蘇聯取代重生的德國成為最大安全威脅來源](../Page/蘇聯.md "wikilink")，西歐的共同安全勢需外延至整個大西洋（即包含[北美](../Page/北美.md "wikilink")）。

本條約是由以下各國[全權代表所簽訂的](../Page/全權代表.md "wikilink"):

  - [比利時](../Page/比利時.md "wikilink")[攝政](../Page/攝政.md "wikilink")[王儲](../Page/王儲.md "wikilink")[博杜安](../Page/比利时的博杜安.md "wikilink")
  - [法國總統](../Page/法國總統.md "wikilink")[樊尚·奥里奥尔](../Page/樊尚·奥里奥尔.md "wikilink")
  - [盧森堡女大公](../Page/盧森堡女大公.md "wikilink")[夏洛特](../Page/夏洛特_\(卢森堡大公\).md "wikilink")
  - [荷蘭女王](../Page/荷蘭女王.md "wikilink")[威廉明娜](../Page/威廉明娜_\(荷兰\).md "wikilink")
  - [英國國王](../Page/英國國王.md "wikilink")[喬治六世](../Page/喬治六世.md "wikilink")
  - [比利時首相](../Page/比利時首相.md "wikilink")[保罗-昂瑞·斯巴克](../Page/保罗-亨利·斯巴克.md "wikilink")
  - 法國外交部代表
  - 盧森堡外交部代表[约瑟夫·伯克](../Page/约瑟夫·伯克.md "wikilink")
  - 比利時財政大臣
  - 荷蘭外交大臣
  - 英國[外交及國協事務大臣](../Page/外交及國協事務大臣.md "wikilink")[欧内斯特·贝文](../Page/欧内斯特·贝文.md "wikilink")
  - 法國駐比利時特命全權大使
  - 盧森堡駐比利時特命全權公使[羅貝爾特·阿爾斯](../Page/羅貝爾特·阿爾斯.md "wikilink")
  - 荷蘭駐比利時特命全權大使
  - 英國駐比利時特命全權大使

## NATO(北大西洋公約組織)

在歐洲無可避免分裂為兩陣營的情況下，來自[蘇聯及](../Page/蘇聯.md "wikilink")[華沙公約組織的威脅就顯得更重於德國重新武裝](../Page/東歐集團.md "wikilink")。西歐國家因此開始尋求訂定一個能包含軍事力量強大的[美國也能參與的共同防禦協定](../Page/美國.md "wikilink")。由於認知到蘇聯日漸增強的威脅，美國也支持這項構想。由此共同防禦協定的構想加速進行，3月底[美國](../Page/美國.md "wikilink")、[加拿大以及](../Page/加拿大.md "wikilink")[英國的官方代表就開始密會協商](../Page/英國.md "wikilink")，最終促成了[北大西洋公約組織於](../Page/北大西洋公約組織.md "wikilink")1949年在[華盛頓簽訂](../Page/華盛頓.md "wikilink")[北大西洋公約](../Page/北大西洋公約.md "wikilink")。

## 外部連結

  - [布魯塞爾條約 | The Brussels Treaty (17
    March 1948)](http://www.cvce.eu/obj/the_brussels_treaty_17_march_1948-en-3467de5e-9802-4b65-8076-778bc7d164d3.html)
    CVCE
  - [History until the creation of the
    WEU](http://www.weu.int/History.htm#1)
  - [布鲁塞尔条约中文版（Jetic
    Gu翻译）](http://pan.baidu.com/share/link?shareid=464739&uk=3003770454)

{{-}}

[Category:1948年歐洲](../Category/1948年歐洲.md "wikilink")
[Category:1948年条约](../Category/1948年条约.md "wikilink")
[Category:比利時條約](../Category/比利時條約.md "wikilink")
[Category:法國條約](../Category/法國條約.md "wikilink")
[Category:盧森堡條約](../Category/盧森堡條約.md "wikilink")
[Category:荷蘭條約](../Category/荷蘭條約.md "wikilink")
[Category:英國條約](../Category/英國條約.md "wikilink")
[Category:布鲁塞尔政治史](../Category/布鲁塞尔政治史.md "wikilink")
[Category:1948年3月](../Category/1948年3月.md "wikilink")