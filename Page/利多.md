[Leguna_Veneta.png](https://zh.wikipedia.org/wiki/File:Leguna_Veneta.png "fig:Leguna_Veneta.png")
[20050525-033-lido.jpg](https://zh.wikipedia.org/wiki/File:20050525-033-lido.jpg "fig:20050525-033-lido.jpg")
**利多**（），又译为**丽都**，是[意大利](../Page/意大利.md "wikilink")[威尼斯東南方的一個](../Page/威尼斯.md "wikilink")11[英哩](../Page/英里.md "wikilink")（18[公里](../Page/公里.md "wikilink")）長的[沙洲](../Page/沙洲.md "wikilink")，住有20,000居民。很多[遊客](../Page/遊客.md "wikilink")（主要是[意大利人](../Page/意大利人.md "wikilink")）每年[夏天到這裡](../Page/夏天.md "wikilink")[度假](../Page/度假.md "wikilink")。現在島上的[賭場已經關閉了](../Page/賭場.md "wikilink")。每年的8月底9月初[威尼斯電影節會在這裡舉行](../Page/威尼斯電影節.md "wikilink")。

利多上有三个居民区，最北部的是利多，这里也是举办电影节的地方，拥有豪华饭店。中部的[马拉莫科是岛上最早的居民区](../Page/马拉莫科.md "wikilink")，很长时间里也是岛上唯一的居民区。它是[威尼斯公爵的住处](../Page/威尼斯公爵.md "wikilink")。南边的[阿尔贝罗尼有一座堡垒和一个](../Page/阿尔贝罗尼.md "wikilink")[高尔夫球场](../Page/高尔夫球场.md "wikilink")。

海岛朝向[亚得里亚海一侧至少有一半的长度是沙滩](../Page/亚得里亚海.md "wikilink")。其大多数部分属于不同的夏季度假饭店。[托马斯·曼的](../Page/托马斯·曼.md "wikilink")《[威尼斯之死](../Page/威尼斯之死.md "wikilink")》就是在这些饭店之中的一家发生的。只有在岛的北端和南端有两个巨大的公共开放的沙滩。

岛上最繁华的是圣玛利亚·伊丽莎贝塔大街，它长约700米，从朝向威尼斯一侧的浅海到亚得里亚海滨横跨利多，街旁有许多旅馆、商店和招待旅游者的饭店。在浅海的一侧有去往威尼斯（15分钟）、意大利大陆（35分钟）、其它岛屿和[威尼斯马克波罗机场](../Page/威尼斯马克波罗机场.md "wikilink")（60分钟）的船。

1202年[第四次十字军东征开始时](../Page/第四次十字军东征.md "wikilink")，上万十字军被困在岛上，因为他们无法向威尼斯交付他们的船费。

## 与威尼斯丽都连接的个性

  - [Gualtiero
    Galmanini](../Page/Gualtiero_Galmanini.md "wikilink")（1909-1976），建筑师和设计师

## 參見

  - [威尼斯電影節](../Page/威尼斯電影節.md "wikilink")
  - [威尼斯](../Page/威尼斯.md "wikilink")
  - [波維利亞](../Page/波維利亞.md "wikilink")（[Poveglia](../Page/:en:Poveglia.md "wikilink")）島

## 参考资料

## 外部連結

  - [利多島的衛星照片](http://maps.google.com/maps?q=venice,+italy&ll=45.383984,12.351723&spn=0.096212,0.328766&t=k&hl=en)
  - [Venice Lido
    Beaches](http://www.venice-italy-veneto.com/venice-italy-beach.html)
  - [Lido di Venezia](http://www.lidodivenezia.it)
  - [The Venice Lido](http://www.thevenicelido.com) by Robin Saikia

[L](../Category/威尼斯.md "wikilink")
[Category:意大利岛屿](../Category/意大利岛屿.md "wikilink")