**時代劇**是指以[日本歷史](../Page/日本歷史.md "wikilink")（所指的是1868年[明治維新之前](../Page/明治維新.md "wikilink")）為背景的[日本](../Page/日本.md "wikilink")[戲劇](../Page/戲劇.md "wikilink")、[電影和](../Page/日本電影.md "wikilink")[電視劇](../Page/日本電視劇.md "wikilink")，主要敘述日本歷史事件和人物，展現當時[武士](../Page/武士.md "wikilink")、[農民](../Page/農民.md "wikilink")、工匠、[町人的生活](../Page/町人.md "wikilink")。時代劇電影有時也被稱為**劍劇**（因為以[日本刀](../Page/日本刀.md "wikilink")、[劍術比併為主要內容而得此名](../Page/劍術.md "wikilink")，有時也成為西方觀眾對時代劇的代稱）。多數時代劇以[日本戰國時代](../Page/戰國時代_\(日本\).md "wikilink")、[江戶時代為故事背景](../Page/江戶時代.md "wikilink")，但也有劇作把背景設定在更早的時代，例如《[地獄變](../Page/地獄變.md "wikilink")》就以[平安時代後期為背景](../Page/平安時代.md "wikilink")。

時代劇的人物形象、風俗習慣、台詞、情節等有一定體制，但與真正史實相比，時代劇多被大膽地、小說化地改編、加工過，主角的觀點也被改成與現代正義感的定義一致，這是為了讓現代的觀眾易於接受。

正如[時代小說與](../Page/時代小說.md "wikilink")[歷史小說的區別一樣](../Page/歷史小說.md "wikilink")，也有人將接近非虛構文藝作品的劇作稱為「[歷史劇](../Page/歷史劇.md "wikilink")」。然而在日本，不少人稱呼日本國外以歷史為背景的劇作為「歷史劇」，將日本國內的以歷史為背景的劇作稱為「時代劇」。

## 沿革

時代劇於1920年代興起，其發展與大眾文學流行有關。自明治時代以來，大眾文學成為日本文壇主流，對當時剛剛出現、同樣以通俗為主的時代劇產生影響，當時有許多時代劇即取材自大眾文學。更重要的是1923年發生[關東大地震](../Page/關東大地震.md "wikilink")，位於[東京的各大](../Page/東京.md "wikilink")[電影公司如](../Page/電影公司.md "wikilink")[日活](../Page/日活.md "wikilink")、[松竹等無一倖免地癱瘓停工](../Page/松竹.md "wikilink")，這迫使他們將製片廠由[關東遷移至](../Page/關東.md "wikilink")[關西地區](../Page/近畿地方.md "wikilink")。而[京都到處保留著歷史建築](../Page/京都.md "wikilink")，為拍攝時代劇提供現成場景。時代劇當時稱為「舊劇」，以劍劇、武術為主，大獲好評，極其興盛。以1936年為例，那一年拍攝的時代劇有311部，現代劇卻只有218部。

[二次大戰日本投降後](../Page/二次大戰.md "wikilink")，雖然日本電影業於1946年開始復甦，但在[同盟國最高司令官總司令部](../Page/同盟國最高司令官總司令部.md "wikilink")（GHQ）的管治下，時代劇的主角在武打場面揮舞日本刀，被認為有[軍國主義的象徵意味](../Page/軍國主義.md "wikilink")，有可能喚起民眾對[美國復仇的思想](../Page/美國.md "wikilink")。所以在這段時期，時代劇的製作被限制。

1950年代中期至1960年代，時代劇電影拍攝再次興旺，加上一批時代劇名導演如[溝口健二](../Page/溝口健二.md "wikilink")、[衣笠貞之助](../Page/衣笠貞之助.md "wikilink")、[黑澤明拍攝出一批風格獨特而富於藝術性的電影](../Page/黑澤明.md "wikilink")《[近松物語](../Page/近松物語.md "wikilink")》、《[樁三十郎](../Page/樁三十郎.md "wikilink")》等，其中《[羅生門](../Page/羅生門_\(電影\).md "wikilink")》得到多個國際獎項，使時代劇的發展達到頂峰。此後，時代劇走向注重娛樂大眾，加強趣味性的路線，例如《[座頭市](../Page/座頭市.md "wikilink")》系列描寫一位市井中的失明按摩師，實際上是一名劍術高手的傳奇故事。還有《[眠狂四郎](../Page/眠狂四郎系列電影.md "wikilink")》系列在敘述一名以使用「圓月殺法」而勝出數百場決鬥的虛無劍客的冒險經歷，自1980年代以降，由於年輕觀眾轉向觀看[好萊塢電影](../Page/好萊塢.md "wikilink")，時代劇電影逐漸衰落。直至1990年代末期，新一代製片人為時代劇加入新元素，例如《[武士暢想曲](../Page/武士暢想曲.md "wikilink")》的配樂加入[搖滾樂音樂](../Page/搖滾樂.md "wikilink")，使時代劇電影再現生機。

電視時代劇的興起源於1953年（[昭和](../Page/昭和.md "wikilink")28年），日本開始有電視廣播，同時也開始製作電視時代劇。1963年（昭和28年）[日本放送協會開始播放長壽時代劇](../Page/日本放送協會.md "wikilink")《[大河劇](../Page/大河劇.md "wikilink")》系列。但自1990年代戀愛偶像劇興起，除了日本放送協會繼續製作《大河劇》系列外，時代劇進入低潮期。直至2001年，《大河劇》系列首次選用偶像派年輕演員為主角，使時代劇再次被年輕觀眾接受。這種趨向一直延續著，例如[香取慎吾主演](../Page/香取慎吾.md "wikilink")《[新選組\!](../Page/新選組!.md "wikilink")》，[仲間由紀惠主演](../Page/仲間由紀惠.md "wikilink")《[功名十字路](../Page/功名十字路.md "wikilink")》、[內山理名主演](../Page/內山理名.md "wikilink")《[大奧·華之亂](../Page/大奧_\(電視劇\).md "wikilink")》都是箇中例子。

在拍攝技術上，電視時代劇的製作在1990年代後期傾向使用電影膠卷（16毫米膠卷）拍攝，尤其[東映系和](../Page/東映.md "wikilink")[朝日系製作的時代劇](../Page/朝日電視台.md "wikilink")。而為吸引年輕觀眾群，有關道具、髮型、造型的歷史考證工作也做得更細緻。

## 時代劇的種類

### 以內容主題分類

[MitoKomonSatomiKotaro.jpg](https://zh.wikipedia.org/wiki/File:MitoKomonSatomiKotaro.jpg "fig:MitoKomonSatomiKotaro.jpg")一角\]\]

  - 以劍（日本刀）格鬥場面為主，以劍術、[武士道和武士的忠誠作為主題](../Page/武士道.md "wikilink")，是大多數時代劇電影的類型。
  - 以講述主角到處流浪、行俠仗義的經歷為主題。
  - 以勸善懲惡為主題，是電視時代劇的主流，例如《[水戶黃門](../Page/水戶黃門.md "wikilink")》、《[暴坊將軍](../Page/暴坊將軍.md "wikilink")》。
  - 以一場[戰爭作完結的劇作](../Page/戰爭.md "wikilink")，大多數採用史實有名的戰爭作為題材，例如《[影武者](../Page/影武者_\(電影\).md "wikilink")》。
  - 改編文藝小說為時代劇作品，例如《[源氏物語](../Page/源氏物語.md "wikilink")》、《[雨月物語](../Page/雨月物語.md "wikilink")》。
  - 以[女性為主角](../Page/女性.md "wikilink")，講述後宮內鬥，例如《[大奧](../Page/大奧_\(電視劇\).md "wikilink")》系列。
  - 以歷史時代為背景，加入如巨大英雄、怪獸類等[特攝手法融入時代劇當中](../Page/特攝.md "wikilink")，例如《[獅子丸](../Page/獅子丸.md "wikilink")》系列。
  - 超級時代劇：無視應有的嚴謹歷史時代考證，混合[西部片](../Page/美國電影.md "wikilink")、[科幻等所有種類的要素](../Page/科幻.md "wikilink")，與特攝類時代劇相近。

### 以製作者和風格分類

  - 以傳統[歌舞伎風格打造炫麗武打場面](../Page/歌舞伎.md "wikilink")，時代劇最早期的風格。
  - 胡鬧時代劇：以劇情荒誕無稽作為賣點，流行於二次大戰前。
  - 東映時代劇：二戰後風靡一時的東映時代劇系列，多以江戶時代作故事背景。
  - [大映時代劇](../Page/大映.md "wikilink")：以改編如《[源氏物語](../Page/源氏物語.md "wikilink")》、《[雨月物語](../Page/雨月物語.md "wikilink")》等江戶時代以外的故事為主，重視歷史考據，如[染黑齒](../Page/染黑齒.md "wikilink")、[引眉等風俗](../Page/引眉.md "wikilink")。
  - 黑澤時代劇：[黑澤明作品系列](../Page/黑澤明.md "wikilink")，以寫實的武打場面為特徵。

## 時代劇中的角色

[Kitamachibugyosho.jpg](https://zh.wikipedia.org/wiki/File:Kitamachibugyosho.jpg "fig:Kitamachibugyosho.jpg")

### 武者

武者階級包括：

  - [武士](../Page/武士.md "wikilink")：世襲的[大名和](../Page/大名.md "wikilink")[幕府將軍](../Page/幕府將軍.md "wikilink")。
  - [浪人](../Page/浪人.md "wikilink")：沒有主公、固定工作的武士。
  - [女武藝者](../Page/女武藝者.md "wikilink")：以精進其武藝為己任，四處遊歷任俠。
  - [忍者](../Page/忍者.md "wikilink")

### 工匠

在時代劇中出現的[工匠包括](../Page/工匠.md "wikilink")[冶金工匠](../Page/冶金.md "wikilink")（經常被脅持，迫他們鑄假幣）、[木匠](../Page/木匠.md "wikilink")、[泥水匠](../Page/泥水匠.md "wikilink")，還有[木刻畫畫師等](../Page/木刻畫.md "wikilink")。

### 商人

時代劇中除了描述各大小商家外，也有描畫被[商人僱用的人](../Page/商人.md "wikilink")，包括店舖眾員工裡地位最高的[番頭](../Page/番頭.md "wikilink")，地位比較低的[手代和年紀幼小的](../Page/手代.md "wikilink")[丁稚](../Page/丁稚.md "wikilink")。

### 幕府

在背景為江戶時代的時代劇中，幕府的各個職位常會出現，包括將軍以下的[大老](../Page/大老.md "wikilink")、[老中](../Page/老中.md "wikilink")、[若年寄](../Page/若年寄.md "wikilink")、[側用人等](../Page/側用人.md "wikilink")，老中以下的三奉行：[勘定奉行](../Page/勘定奉行.md "wikilink")、[町奉行](../Page/町奉行.md "wikilink")、[寺社奉行及他們的下屬](../Page/寺社奉行.md "wikilink")。例如時代劇《[錢形平次](../Page/錢形平次.md "wikilink")》的主角[錢形平次是](../Page/錢形平次.md "wikilink")「」（一個類似[中國古代](../Page/中國.md "wikilink")「[捕快](../Page/捕快.md "wikilink")」的職位，但任職者的身份是平民，而且不是正式職位），就是由町奉行管轄。

此外，由於實行[參覲交代制度](../Page/參覲交代.md "wikilink")，各[藩主的妻妾親眷均留在](../Page/藩主.md "wikilink")[江戶](../Page/江戶.md "wikilink")，這批人加上[將軍家的](../Page/德川將軍家.md "wikilink")[正室](../Page/御台所.md "wikilink")、[側室也是重點描寫的對象](../Page/側室.md "wikilink")。

至於故事背景在一[藩之內的時代劇](../Page/藩.md "wikilink")，會著重提及[藩主](../Page/藩主.md "wikilink")、[家老和家臣等人物](../Page/家老.md "wikilink")。

### 其他

時代劇中也會提及當時市井中出現的各種人物，例如[小販](../Page/小販.md "wikilink")、[算命人](../Page/算命人.md "wikilink")、[雜耍藝人](../Page/雜耍.md "wikilink")、[三味線藝人](../Page/三味線.md "wikilink")、[歌舞伎藝人](../Page/歌舞伎.md "wikilink")、[藝伎](../Page/藝伎.md "wikilink")、[妓女](../Page/妓女.md "wikilink")、[僧人等等](../Page/僧人.md "wikilink")。

## 常見情節

  - [英雄通常眼部有](../Page/英雄.md "wikilink")[化妝](../Page/化妝.md "wikilink")，反派人物通常一頭亂髮。
  - 對白是經過設計的[舊式日語](../Page/舊式日語.md "wikilink")，以[現代日語的發音和文法為基礎](../Page/現代日語.md "wikilink")，加上大量[古語](../Page/古語.md "wikilink")，並配合以古代禮節。
  - 在長篇電視劇例如《水戶黃門》和《[錢形平次](../Page/錢形平次.md "wikilink")》中，有時會更換飾演主要人物的演員，劇中不會特意解釋，新演員通常在角色之前身處的場景出現，然後故事繼續。
  - 刀戰時，即使有大群反派角色攻擊主角，他們永不會一擁而上，而是逐個攻擊主角：他們持刀站於主角易於攻擊的距離，讓主角將他們一擊即倒。
  - 不論主角是武士或者平民，時代劇常常在劇終前有一場激烈的劍術比併高潮戲，通常由主角以[日本刀或者](../Page/日本刀.md "wikilink")[十手勝出](../Page/十手.md "wikilink")。有時主角會折斷對方的刀。
  - 在電視劇裡，敵人被殺死後，主角的刀劍總附有血跡。
  - 在電影裡場景表現比較暴力，有時被刀劍刺中的傷口會噴出大量鮮血，也經常有將敵人肢解和[斬首的場面](../Page/斬首.md "wikilink")。

### 經典對白

時代劇中常常出現經典的[警句](../Page/警句.md "wikilink")，例如「如夏日流螢撲火」（相當於中文[成語](../Page/成語.md "wikilink")「飛蛾撲火」，意即敵人將自取滅亡）、「火災和喧鬧聲代表江戶的繁華」等等。

另外，電視時代劇的角色往往在每集的相似情節說出自己的一句經典對白，例如《水戶黃門》主角[德川光圀常假扮成平民](../Page/德川光圀.md "wikilink")，在每集尾段的刀戰時，他的[侍從總會掏出一件有](../Page/侍從.md "wikilink")[家紋的隨身物品並斥喝](../Page/家紋.md "wikilink")：「退下！難道你認不出這家紋嗎？」反派人物看見了便馬上投降、求饒。

## 著名的時代劇

### 電影

  - [雄呂血](../Page/雄呂血.md "wikilink")
  - [一殺多生劍](../Page/一殺多生劍.md "wikilink")
  - [斬人斬馬劍](../Page/斬人斬馬劍.md "wikilink")
  - [赤西礪太](../Page/赤西礪太.md "wikilink")
  - [鴛鴦歌合戰](../Page/鴛鴦歌合戰.md "wikilink")
  - [無法松的一生](../Page/無法松的一生.md "wikilink")
  - [近松物語](../Page/近松物語.md "wikilink")
  - [椿三十郎](../Page/椿三十郎.md "wikilink")
  - [紅鬍子](../Page/紅鬍子.md "wikilink")
  - [鞍馬天狗系列](../Page/鞍馬天狗.md "wikilink")
  - [带子雄狼系列](../Page/带子雄狼.md "wikilink")
  - [丹下左膳系列](../Page/丹下左膳.md "wikilink")（代表作《[丹下左膳之百万两之壶](../Page/丹下左膳之百万两之壶.md "wikilink")》）
  - [座頭市系列](../Page/座頭市.md "wikilink")
  - [必殺系列](../Page/必殺.md "wikilink")
  - [眠狂四郎系列](../Page/眠狂四郎系列電影.md "wikilink")
  - [遠山金系列](../Page/遠山金.md "wikilink")
  - [水戶黃門系列](../Page/水戶黃門.md "wikilink")
  - [新吾十番勝負系列](../Page/新吾十番勝負.md "wikilink")
  - [宮本武藏相關作品](../Page/宮本武藏.md "wikilink")
  - [忠臣藏相關作品](../Page/忠臣藏.md "wikilink")
  - [新選組相關作品](../Page/新選組.md "wikilink")
  - [怪談系列](../Page/怪談.md "wikilink")
  - [黃昏清兵衛](../Page/黃昏清兵衛.md "wikilink")
  - [隱劍鬼爪](../Page/隱劍鬼爪.md "wikilink")
  - [一心太助](../Page/一心太助.md "wikilink")
  - [不知火檢校](../Page/不知火檢校.md "wikilink")
  - [影武者](../Page/影武者_\(電影\).md "wikilink")
  - [七人之侍](../Page/七人之侍.md "wikilink")

### 電視劇

  - [暴坊將軍系列](../Page/暴坊將軍.md "wikilink")（朝日電視台、1978年─2003年）
  - [編笠十兵衛](../Page/編笠十兵衛.md "wikilink")（富士電視台、東京電視台）

<!-- end list -->

  - [右門捕物帖](../Page/右門捕物帖.md "wikilink")（日本電視台、朝日電視台）
  - [繪島生島](../Page/繪島生島.md "wikilink")（東京12頻道）
  - [江戶特搜指令](../Page/江戶特搜指令.md "wikilink")
  - [江戶中町奉行所](../Page/江戶中町奉行所.md "wikilink")（東京電視台）
  - [江戶之渦潮](../Page/江戶之渦潮.md "wikilink")（富士電視台）
  - [江戶之旋風](../Page/江戶之旋風.md "wikilink")（富士電視台）
  - [江戶之牙](../Page/江戶之牙.md "wikilink")
  - [江戶之激鬥](../Page/江戶之激鬥.md "wikilink")（富士電視台）
  - [江戶之鷹 御用部屋犯科帖](../Page/江戶之鷹_御用部屋犯科帖.md "wikilink")（朝日電視台）
  - [江戶之用心棒](../Page/江戶之用心棒.md "wikilink")（日本電視台）

<!-- end list -->

  - [大江戶搜查網](../Page/大江戶搜查網.md "wikilink")（東京電視台）

<!-- end list -->

  - [大岡越前](../Page/大岡越前.md "wikilink")（TBS）
  - [大奧系列](../Page/大奧_\(富士電視劇\).md "wikilink")（富士電視台）
  - [狼無賴控](../Page/狼無賴控.md "wikilink")

<!-- end list -->

  - [啞侍‧鬼一法眼](../Page/啞侍‧鬼一法眼.md "wikilink")
  - [鴛鴦右京捕物車](../Page/鴛鴦右京捕物車.md "wikilink")（朝日放送系）

<!-- end list -->

  - [鬼平犯科帳](../Page/鬼平犯科帳.md "wikilink")（NET、富士電視台）
  - [父子鷹](../Page/父子鷹.md "wikilink")（日本電視台）

<!-- end list -->

  - [隱密奉行朝比奈](../Page/隱密奉行朝比奈.md "wikilink")（富士電視台）

<!-- end list -->

  - [快傑獅子丸](../Page/快傑獅子丸.md "wikilink")

<!-- end list -->

  -
    [風雲獅子丸](../Page/風雲獅子丸.md "wikilink")（富士電視台）

<!-- end list -->

  - [影同心](../Page/影同心.md "wikilink")（每日放送）
  - [服部半藏‧影之軍團](../Page/服部半藏‧影之軍團.md "wikilink")（關西電視台）
  - [影武者德川家康](../Page/影武者德川家康.md "wikilink")（朝日電視台）
  - [神谷玄次郎捕物控](../Page/神谷玄次郎捕物控.md "wikilink")（富士電視台）

<!-- end list -->

  - [騎馬奉行](../Page/騎馬奉行.md "wikilink")

<!-- end list -->

  - [雲霧仁左衛門](../Page/雲霧仁左衛門.md "wikilink")
  - [喧嘩屋右近](../Page/喧嘩屋右近.md "wikilink")（東京電視台）
  - [劍客生涯](../Page/劍客生涯.md "wikilink")（富士電視台）

<!-- end list -->

  - [荒野之素浪人](../Page/荒野之素浪人.md "wikilink")

<!-- end list -->

  - [御家人斬九郎](../Page/御家人斬九郎.md "wikilink")（富士電視台）
  - [帶子雄狼](../Page/帶子雄狼.md "wikilink")（朝日電視台）

<!-- end list -->

  - [座頭市系列](../Page/座頭市.md "wikilink")
  - [三匹斬系列](../Page/三匹斬.md "wikilink")（朝日電視台）
  - [參上！天空劍士](../Page/參上！天空劍士.md "wikilink")
  - [仕掛人‧藤枝梅安](../Page/仕掛人‧藤枝梅安.md "wikilink")（富士電視台）
  - [地獄之辰捕物控](../Page/地獄之辰捕物控.md "wikilink")
  - [疾風同心](../Page/疾風同心.md "wikilink")
  - [十手人](../Page/十手人.md "wikilink")
  - [十手無用 九丁堀事件帖](../Page/十手無用_九丁堀事件帖.md "wikilink")

<!-- end list -->

  - [白獅子假面](../Page/白獅子假面.md "wikilink")
  - [新五捕物帳](../Page/新五捕物帳.md "wikilink")（日本電視台）
  - [新春大型時代劇系列](../Page/新春大型時代劇.md "wikilink")（東京電視台、1981年─）
  - [新撰組血風錄](../Page/新撰組血風錄.md "wikilink")
  - [新‧桃太郎侍](../Page/新‧桃太郎侍.md "wikilink")（朝日電視台）
  - [素浪人月影兵庫](../Page/素浪人月影兵庫.md "wikilink")
  - [錢形平次](../Page/錢形平次.md "wikilink")（富士電視台、日本電視台、朝日電視台）

<!-- end list -->

  - [大河劇系列](../Page/NHK大河劇.md "wikilink")（NHK、1963年─）
  - [達磨大助事件帳](../Page/達磨大助事件帳.md "wikilink")
  - [流浪的紅花阿仙](../Page/流浪的紅花阿仙.md "wikilink")（NET）

<!-- end list -->

  - [旅人異三郎](../Page/旅人異三郎.md "wikilink")（東京12頻道）
  - [長七郎江戶日記](../Page/長七郎江戶日記.md "wikilink")（日本電視台）

<!-- end list -->

  - [照姬七變化](../Page/照姬七變化.md "wikilink")（富士電視台）
  - [天下御免](../Page/天下御免.md "wikilink")
  - [天下堂堂](../Page/天下堂堂.md "wikilink")

<!-- end list -->

  - [德川女人繪卷](../Page/德川女人繪卷.md "wikilink")（關西電視台）
  - [德川三國志](../Page/德川三國志.md "wikilink")
  - [德川無賴帳](../Page/德川無賴帳.md "wikilink")（東京電視台）
  - [傳七捕物帳](../Page/傳七捕物帳.md "wikilink")（NET、日本電視台）
  - [同心‧曉蘭之介](../Page/同心‧曉蘭之介.md "wikilink")（富士電視台、1981年─1982年）
  - [同心齋之介](../Page/同心齋之介.md "wikilink")

<!-- end list -->

  - [長崎犯科帳](../Page/長崎犯科帳.md "wikilink")（日本電視台）

<!-- end list -->

  - [人形左七捕物帳](../Page/人形左七捕物帳.md "wikilink")

<!-- end list -->

  - [眠狂四郎圓月殺法](../Page/眠狂四郎圓月殺法.md "wikilink")（東京電視台）
  - [眠狂四郎無賴控](../Page/眠狂四郎無賴控.md "wikilink")（東京電視台）

<!-- end list -->

  - [浮浪雲](../Page/浮浪雲.md "wikilink")（朝日電視台、TBS）
  - [旗本退屈男](../Page/旗本退屈男.md "wikilink")

<!-- end list -->

  - [八百八町夢日記](../Page/八百八町夢日記.md "wikilink")（日本電視台）
  - [盤嶽之一生](../Page/盤嶽之一生.md "wikilink")（富士電視台）
  - [半七捕物帳](../Page/半七捕物帳.md "wikilink")
  - [蟠髓院長兵衛](../Page/蟠髓院長兵衛.md "wikilink")（每日放送）
  - [必殺系列](../Page/必殺.md "wikilink")（朝日放送、1972年─1991年）

<!-- end list -->

  - [風雲真田幸村](../Page/風雲真田幸村.md "wikilink")（東京電視台）

<!-- end list -->

  - [變身忍者 嵐](../Page/變身忍者_嵐.md "wikilink")

<!-- end list -->

  - [松平右近事件帳](../Page/松平右近事件帳.md "wikilink")（日本電視台）

<!-- end list -->

  -
    新・松平右近

<!-- end list -->

  - [水戶黃門](../Page/水戶黃門.md "wikilink")（TBS）

<!-- end list -->

  - [名奉行！大岡越前](../Page/名奉行！大岡越前.md "wikilink")（朝日電視台）

<!-- end list -->

  - [桃太郎侍](../Page/桃太郎侍.md "wikilink")（日本電視台）

<!-- end list -->

  - [柳生一族的陰謀](../Page/柳生一族的陰謀.md "wikilink")（關西電視台）
  - [柳生十兵衛](../Page/柳生十兵衛.md "wikilink")（NHK、日本電視台、NET、東京12頻道）

<!-- end list -->

  - [柳生新陰流](../Page/柳生新陰流.md "wikilink")（東京電視台）
  - [柳生武藝帳](../Page/柳生武藝帳.md "wikilink")（日本電視台）

<!-- end list -->

  - [妖術武藝帳](../Page/妖術武藝帳.md "wikilink")

<!-- end list -->

  - [真田幸村的謀略](../Page/真田幸村的謀略.md "wikilink")

## 時代劇導演

### 電影

  - [二川文太郎](../Page/二川文太郎.md "wikilink")
  - [山中貞雄](../Page/山中貞雄.md "wikilink")
  - [牧野雅廣](../Page/牧野雅廣.md "wikilink")
  - [伊藤大輔](../Page/伊藤大輔.md "wikilink")
  - [內田吐夢](../Page/內田吐夢.md "wikilink")
  - [市川崑](../Page/市川崑.md "wikilink")
  - [黑澤明](../Page/黑澤明.md "wikilink")
  - [溝口健二](../Page/溝口健二.md "wikilink")
  - [稻垣浩](../Page/稻垣浩.md "wikilink")
  - [深作欣二](../Page/深作欣二.md "wikilink")
  - [山內鐵也](../Page/山內鐵也.md "wikilink")
  - [大曾根辰夫](../Page/大曾根辰夫.md "wikilink")
  - [松田定次](../Page/松田定次.md "wikilink")
  - [三隅研次](../Page/三隅研次.md "wikilink")

### 電視劇

  - [林徹](../Page/林徹.md "wikilink")

## 著名時代劇演員

### 二次大戰前

  - [尾上松之助](../Page/尾上松之助.md "wikilink")（1875年─1926）
  - **「時代劇六大明星」**
      - [大河內傳次郎](../Page/大河內傳次郎.md "wikilink")（1898年─1962年）
      - [片岡千惠藏](../Page/片岡千惠藏.md "wikilink")（1903年─1983年）
      - [嵐寬壽郎](../Page/嵐寬壽郎.md "wikilink")（1903年─1980年）
      - [阪東妻三郎](../Page/阪東妻三郎.md "wikilink")（1901年─1953年）
      - [長谷川一夫](../Page/長谷川一夫.md "wikilink")（1908年─1984年）
      - [市川右太衛門](../Page/市川右太衛門.md "wikilink")（1907年─1999年）

<!-- end list -->

  - [月形龍之介](../Page/月形龍之介.md "wikilink")（1902年─1970年）
  - [大友柳太朗](../Page/大友柳太朗.md "wikilink")（1912年─1985年）
  - [近衛十四郎](../Page/近衛十四郎.md "wikilink")（1914年─1977年）

### 二次大戰後（已故者）

  - [三船敏郎](../Page/三船敏郎.md "wikilink")（1920年─1997年）
  - [丹波哲郎](../Page/丹波哲郎.md "wikilink")（1922年─2006年）
  - [鶴田浩二](../Page/鶴田浩二.md "wikilink")（1924年─1987年）
  - [東千代之介](../Page/東千代之介.md "wikilink")（1926年─2000年）
  - [大川橋藏](../Page/大川橋藏.md "wikilink")（1929年─1984年）
  - [市川雷藏](../Page/市川雷藏.md "wikilink")（1931年─1969年）
  - [中村錦之助](../Page/萬屋錦之介.md "wikilink")（1932年─1997年、後來改名[萬屋錦之介](../Page/萬屋錦之介.md "wikilink")）
  - [若山富三郎](../Page/若山富三郎.md "wikilink")（1929年─1992年）
  - [勝新太郎](../Page/勝新太郎.md "wikilink")（1931年─1997年）

### 二次大戰後

  - [三國連太郎](../Page/三國連太郎.md "wikilink")（1923年─）
  - [高倉健](../Page/高倉健.md "wikilink")（1931年─）
  - [仲代達矢](../Page/仲代達矢.md "wikilink")（1932年─）
  - [菅原文太](../Page/菅原文太.md "wikilink")（1933年─）
  - [平幹二朗](../Page/平幹二朗.md "wikilink")（1933-）
  - [里見浩太郎](../Page/里見浩太朗.md "wikilink")（1936年─、後來改名[里見浩太朗](../Page/里見浩太朗.md "wikilink")）
  - [中村賀津雄](../Page/中村賀津雄.md "wikilink")（1938年─、[中村嘉葎雄](../Page/中村嘉葎雄.md "wikilink")）
  - [千葉真一](../Page/千葉真一.md "wikilink")（1939年─）
  - [中村敦夫](../Page/中村敦夫.md "wikilink")（1940年─）
  - [松方弘樹](../Page/松方弘樹.md "wikilink")（1942年─、近衛十四郎之子）
  - [北大路欣也](../Page/北大路欣也.md "wikilink")（1943年─、市川右太衛門之子）
  - [杉良太郎](../Page/杉良太郎.md "wikilink")（1944年─）
  - [中村吉右衛門](../Page/中村吉右衛門.md "wikilink")（1944年─）
  - [高橋英樹](../Page/高橋英樹.md "wikilink")（1944年─）
  - [草薙良一](../Page/草薙良一.md "wikilink")（1948年─）
  - [松平健](../Page/松平健.md "wikilink")（1953年─）
  - [村上弘明](../Page/村上弘明.md "wikilink")（1956年─）
  - [松本幸四郎](../Page/松本幸四郎.md "wikilink")（1942年─）
  - [京本政樹](../Page/京本政樹.md "wikilink")（1959年─）
  - [三田村邦彥](../Page/三田村邦彥.md "wikilink")（1953年─）
  - [役所廣司](../Page/役所廣司.md "wikilink")（1956年─）
  - [渡邊謙](../Page/渡邊謙.md "wikilink")（1959年─）
  - [片岡孝夫](../Page/片岡孝夫.md "wikilink")
  - [藤田真](../Page/藤田真.md "wikilink")

## 趣聞

電影系列[星球大戰導演](../Page/星球大戰.md "wikilink")[喬治·盧卡斯受到](../Page/喬治·盧卡斯.md "wikilink")[黑澤明系列電影影響](../Page/黑澤明.md "wikilink")，其影片中的武士專稱為[絕地](../Page/絕地.md "wikilink")（）與日語的**時代**（jidai）的發音相似;意為帶刀並擁有道力場的武士。

## 相關條目

  - [日本電影](../Page/日本電影.md "wikilink")
  - [古裝劇](../Page/古裝劇.md "wikilink")

## 相關書目

  - [川本三郎](../Page/川本三郎.md "wikilink")《時代劇在這裡》（日語：）平凡社 ISBN 4582832695

  - [逢坂剛](../Page/逢坂剛.md "wikilink")、川本三郎、[菊地秀行](../Page/菊地秀行.md "wikilink")、[永田哲朗](../Page/永田哲朗.md "wikilink")、[繩田一男](../Page/繩田一男.md "wikilink")、[宮本昌孝](../Page/宮本昌孝.md "wikilink")　《時代劇的邀請》（日語：）PHP研究所
    ISBN 456963270X

  - 《壓卷！無賴派時代劇──無情．英雄》（日語：）歷史群像系列　學研 ISBN 4056032688

## 參考資料

1.  [電影教室](http://movie.cca.gov.tw/CINEMA/basis_dic_result01.asp?rowid=12&category=2&strSearch=)，台灣電影筆記，國家電影資料館。

2.  [世界電影史
    第二十四章　遠東的電影](http://www.millionbook.net/lszl/s/saduer/sjdy/027.htm)

3.  [座頭市傳奇--日本時代劇的暴力崇拜](http://ent.sina.com.cn/2005-01-24/1003638913.html)，新快報，2005年1月24日。

4.  [日本年輕偶像　古裝時代劇磨演技](https://web.archive.org/web/20070526045155/http://stars.zaobao.com/pages3/japan060917.html)，聯合早報網，2006年9月17日。

5.  [《丹下左膳之百萬兩之壺》：向時代劇致敬的最高形式](http://www.qingyun.com/column/yishu/yingshi/pinglun/2005060913.htm)

6.  [Jidai Geki -
    Introduction](http://www.kungfucinema.com/categories/jidaigeki.htm)

## 外部連結

  - [時代劇專門頻道](http://www.jidaigeki.com/)

  - [A Man, a Blade, an Empty Road: Postwar Samurai Film
    to 1970](https://web.archive.org/web/20061211050453/http://www.greencine.com/article?action=view&articleID=70&pageID=138)

  - [时代剧复兴计划国际宣传组](https://web.archive.org/web/20110211114155/http://www.jidaigekirp.com/jidaigekirp/cn/index.html)

  - [东映京都电影城](http://www.toei-eigamura.com/?c=4)

[日本電視劇](../Category/日本電視劇.md "wikilink")
[日本電影](../Category/日本電影.md "wikilink")
[時代劇](../Category/時代劇.md "wikilink")
[Category:電影類型](../Category/電影類型.md "wikilink")