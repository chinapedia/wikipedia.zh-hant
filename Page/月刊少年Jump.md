《**月刊少年Jump**》是從1970年起到2007年為止[集英社發行的月刊](../Page/集英社.md "wikilink")[少年](../Page/少年漫画.md "wikilink")[漫畫雜誌](../Page/漫畫雜誌.md "wikilink")。

## 概要

「[週刊少年Jump](../Page/週刊少年Jump.md "wikilink")」的姊妹雜誌。1970年2月6日創刊，前身為「別冊少年Jump」。從1974年起月刊化。平均發行數量在1989年達到鼎盛，約140萬冊。到了2006年，銷量僅為42萬冊，遠不及[講談社的月刊漫畫雜誌](../Page/講談社.md "wikilink")《[月刊少年Magazine](../Page/月刊少年Magazine.md "wikilink")》。由於銷售量低迷，集英社在2007年6月6日7月號發售時作出休刊的決定\[1\]。

月刊漫畫雜誌《[Jump Square](../Page/Jump_Square.md "wikilink")》（JUMP
SQ）於2007年11月創刊以接替休刊的月刊少年Jump\[2\]。雖然一些作品在休刊後仍繼續發表，大部份作品随著休刊而終結。其中轉移至《Jump
Square》的連載作品在《Jump Square》創刊之前於《週刊少年Jump》上临時刊載。

## 部份連載作品

★標記表示連載至最終號的作品。☆標記表示移至Jump Square繼續連載的作品。

  - [I'll](../Page/I'll.md "wikilink")（[淺田弘幸](../Page/淺田弘幸.md "wikilink")）
  - [ETOILE－三劍客星羅－](../Page/ETOILE－三劍客星羅－.md "wikilink")（[井沢ひろし](../Page/井沢ひろし.md "wikilink")·[山田孝太郎](../Page/山田孝太郎.md "wikilink")）★
  - [NBA
    STORY](../Page/NBA_STORY.md "wikilink")（[高岩ヨシヒロ](../Page/高岩ヨシヒロ.md "wikilink")）
  - [冷面天使](../Page/冷面天使.md "wikilink")（[八木教廣](../Page/八木教廣.md "wikilink")）
  - [快傑蒸汽偵探團](../Page/快傑蒸汽偵探團.md "wikilink")（[麻宮騎亞](../Page/麻宮騎亞.md "wikilink")）→轉移至[Ultra
    Jump](../Page/Ultra_Jump.md "wikilink")
  - [鬼神童子ZENKI](../Page/鬼神童子ZENKI.md "wikilink")（[谷菊秀](../Page/谷菊秀.md "wikilink")・黒岩よしひろ）
  - [CLAYMORE](../Page/獵魔戰記.md "wikilink")（[八木教廣](../Page/八木教廣.md "wikilink")）★☆
  - [黒いラブレター](../Page/黒いラブレター.md "wikilink")（[東谷文仁](../Page/東谷文仁.md "wikilink")）★
  - [グロテスクへの招待](../Page/グロテスクへの招待.md "wikilink")（[手塚治虫](../Page/手塚治虫.md "wikilink")）
  - [警察犬キンゾー](../Page/警察犬キンゾー.md "wikilink")（[佐佐木惠](../Page/佐佐木惠.md "wikilink")）★
  - [GO
    AHEAD](../Page/GO_AHEAD.md "wikilink")（[樋口大輔](../Page/樋口大輔.md "wikilink")）
  - [GO DA
    GUN](../Page/GO_DA_GUN.md "wikilink")（[片倉・M・政憲](../Page/片倉政憲.md "wikilink")）
  - [SAKON（左近）-戦国風雲録-](../Page/SAKON（左近）-戦国風雲録-.md "wikilink")（[原哲夫](../Page/原哲夫.md "wikilink")・[隆慶一郎](../Page/隆慶一郎.md "wikilink")）
  - [地獄甲子園](../Page/地獄甲子園.md "wikilink")（[漫☆画太郎](../Page/漫☆画太郎.md "wikilink")）
  - [水平線のシャチ](../Page/水平線のシャチ.md "wikilink")（[東本一樹](../Page/東本一樹.md "wikilink")）★
  - [たたかえ\!
    たらんてら](../Page/たたかえ!_たらんてら.md "wikilink")（[葉生田采丸](../Page/葉生田采丸.md "wikilink")）★
  - [信蜂](../Page/信蜂.md "wikilink")（淺田弘幸）★☆
  - [動乱](../Page/動乱_\(漫畫\).md "wikilink")（原作：松元櫻 漫畫：大賀淺木）★
  - [馭龍少年](../Page/馭龍少年.md "wikilink")（[佐倉賢一](../Page/佐倉賢一.md "wikilink")）
  - [NORA-ノラ-](../Page/NORA-ノラ-.md "wikilink")（[筧一成](../Page/筧一成.md "wikilink")）★
  - [パッサカリア
    Op.7](../Page/パッサカリア_Op.7.md "wikilink")（[佐木飛朗斗](../Page/佐木飛朗斗.md "wikilink")・[山田秋太郎](../Page/山田秋太郎.md "wikilink")）★
  - [B-DASH](../Page/B-DASH.md "wikilink")（[松枝尚嗣](../Page/松枝尚嗣.md "wikilink")）
  - [BUZZER
    BEATER](../Page/BUZZER_BEATER.md "wikilink")（[井上雄彦](../Page/井上雄彦.md "wikilink")）
  - [筆神](../Page/筆神.md "wikilink")（[田村吉康](../Page/田村吉康.md "wikilink")）
  - [自由人HERO](../Page/自由人HERO.md "wikilink")（[柴田亞美](../Page/柴田亞美.md "wikilink")）
  - [風飛び一斗](../Page/風飛び一斗.md "wikilink")（[門馬もとき](../Page/門馬もとき.md "wikilink")）★
  - [BLUE DRAGON
    ST](../Page/藍龍.md "wikilink")（[柴田亞美](../Page/柴田亞美.md "wikilink")）★
  - [冒険王ビィト](../Page/冒険王ビィト.md "wikilink")（[三条陸](../Page/三条陸.md "wikilink")·[稲田浩司](../Page/稲田浩司.md "wikilink")）★
  - [増田こうすけ劇場
    ギャグマンガ日和](../Page/ギャグマンガ日和.md "wikilink")（[増田こうすけ](../Page/増田こうすけ.md "wikilink")）★☆
  - [魔砲使い黒姫](../Page/魔砲使い黒姫.md "wikilink")（[片倉・狼組・政憲](../Page/片倉政憲.md "wikilink")）★☆
  - [みずしな孝之のミズシネマ](../Page/ミズシネマ.md "wikilink")（みずしな孝之）★
  - [Mr.Clice](../Page/Mr.Clice.md "wikilink")（[秋本治](../Page/秋本治.md "wikilink")）
  - [Mr.Perfect](../Page/Mr.Perfect.md "wikilink")（[真弓和丸](../Page/真弓和丸.md "wikilink")）★
  - [眠兎](../Page/眠兎.md "wikilink")（淺田弘幸）
  - [十字架與吸血鬼](../Page/十字架與吸血鬼.md "wikilink")（[池田晃久](../Page/池田晃久.md "wikilink")）★☆
  - [校園偵探圓樂](../Page/校園偵探圓樂.md "wikilink")（小栗一又）

## 參考來源

<references />

## 外部連結

  - [月刊少年Jump Net](http://mj.shueisha.co.jp/index.html)

[Category:日本漫畫雜誌](../Category/日本漫畫雜誌.md "wikilink")
[Category:少年漫畫雜誌](../Category/少年漫畫雜誌.md "wikilink")
[Category:月刊漫畫雜誌](../Category/月刊漫畫雜誌.md "wikilink")
[J](../Category/集英社的漫畫雜誌.md "wikilink")
[-](../Category/週刊少年Jump.md "wikilink")
[\*](../Category/月刊少年Jump.md "wikilink")
[Category:1970年創辦的雜誌](../Category/1970年創辦的雜誌.md "wikilink")
[Category:2007年停刊的雜誌](../Category/2007年停刊的雜誌.md "wikilink")
[Category:1970年日本建立](../Category/1970年日本建立.md "wikilink")
[Category:2007年日本廢除](../Category/2007年日本廢除.md "wikilink")

1.
2.