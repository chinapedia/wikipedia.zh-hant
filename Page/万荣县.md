**万荣县**，是[中国](../Page/中国.md "wikilink")[山西省](../Page/山西省.md "wikilink")[运城市所辖的一个](../Page/运城市.md "wikilink")[县](../Page/县.md "wikilink")。

## 历史

万荣县1954年由万泉、荣河两县合并而成。[万泉县](../Page/万泉县.md "wikilink")，始置于唐[武德三年](../Page/武德.md "wikilink")，元曾省入[猗氏后复置](../Page/猗氏县.md "wikilink")。[荣河县](../Page/荣河县.md "wikilink")，战国为魏汾阴邑，西汉置汾县属河东郡，唐[开元十年以得](../Page/开元.md "wikilink")“宝鼎”之祥改称宝鼎县，宋[大中祥符四年改为荣河县](../Page/大中祥符.md "wikilink")。\[1\]

## 地理

万荣县地处晋西南[黄土高原](../Page/黄土高原.md "wikilink")，[黄河与](../Page/黄河.md "wikilink")[汾河交汇处](../Page/汾河.md "wikilink")，全县总面积1080.5平方公里。

## 行政区划

下辖4个[镇](../Page/行政建制镇.md "wikilink")、10个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

## 交通

[209国道](../Page/209国道.md "wikilink")、233省道、237省道

## 人口

2010年第六次全国人口普查43.9364万人。\[2\]

## 经济

工业有磁材、医药、化工建材、金属镁、焦化、农副产品加工六大主导产业。\[3\]

## 风景名胜

  - [全国重点文物保护单位](../Page/全国重点文物保护单位.md "wikilink")：[万荣东岳庙](../Page/万荣东岳庙.md "wikilink")、[万荣后土庙](../Page/万荣后土庙.md "wikilink")、[万荣稷王庙](../Page/万荣稷王庙.md "wikilink")、[万泉文庙](../Page/万泉文庙.md "wikilink")、[万荣稷王山塔](../Page/万荣稷王山塔.md "wikilink")、[中里庄八龙寺塔](../Page/中里庄八龙寺塔.md "wikilink")、[万荣旱泉塔](../Page/万荣旱泉塔.md "wikilink")、[南阳村寿圣寺塔](../Page/南阳村寿圣寺塔.md "wikilink")、[薛瑄家庙及墓地](../Page/薛瑄家庙及墓地.md "wikilink")、[闫景李家大院](../Page/闫景李家大院.md "wikilink")
  - [山西省文物保护单位](../Page/山西省文物保护单位.md "wikilink")：[荆村遗址](../Page/荆村遗址.md "wikilink")、[汾阴古城址及墓地](../Page/汾阴古城址及墓地.md "wikilink")、[北辛舍利塔](../Page/北辛舍利塔.md "wikilink")
  - [万荣县文物保护单位](../Page/万荣县文物保护单位.md "wikilink")
  - [孤峰山](../Page/孤峰山.md "wikilink")

## 名人

[王国正](../Page/王国正.md "wikilink")、[傅作义](../Page/傅作义.md "wikilink")

## 参考文献

## 外部链接

  - [万荣县政府网站](http://www.sxwanrong.gov.cn/)

[万荣县](../Category/万荣县.md "wikilink")
[Category:运城区县市](../Category/运城区县市.md "wikilink")
[运城](../Category/山西省县份.md "wikilink")

1.  [历史沿革](http://www.sxwanrong.gov.cn/zjwr/wrgk/lsyg/579d38a6_420f_4a19_8b05_d18e6bcaad3f.html)

2.  [运城市第六次全国人口普查主要数据公报](http://www.sxycrb.com/html/55/n-33455.html)
3.  [经济发展](http://www.sxwanrong.gov.cn/zjwr/wrgk/shjj/8ef9e2ce_58d4_4fa2_8ca7_35e4a0fcce28.html)