**鑽光魚科**（[学名](../Page/学名.md "wikilink")：）是[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[巨口魚目的其中一科](../Page/巨口魚目.md "wikilink")。

## 分類

**鑽光魚科**下分5個屬，如下：

### 波拿巴鑽光魚屬(*Bonapartia*)

  - [大口波拿巴鑽光魚](../Page/大口波拿巴鑽光魚.md "wikilink")(*Bonapartia pedaliota*)

### 圓罩魚屬(*Cyclothone*)

  - [斜齒圓罩魚](../Page/斜齒圓罩魚.md "wikilink")(*Cyclothone acclinidens*)
  - [白圓罩魚](../Page/白圓罩魚.md "wikilink")(*Cyclothone alba*)
  - [黑圓罩魚](../Page/黑圓罩魚.md "wikilink")(*Cyclothone atraria*)
  - [勃氏圓罩魚](../Page/勃氏圓罩魚.md "wikilink")(*Cyclothone braueri*)
  - [科氏圓罩魚](../Page/科氏圓罩魚.md "wikilink")(*Cyclothone kobayashii*)
  - [利維圓罩魚](../Page/利維圓罩魚.md "wikilink")(*Cyclothone livida*)
  - [小齒圓罩魚](../Page/小齒圓罩魚.md "wikilink")(*Cyclothone microdon*)
  - [暗圓罩魚](../Page/暗圓罩魚.md "wikilink")(*Cyclothone obscura*)
  - [蒼圓罩魚](../Page/蒼圓罩魚.md "wikilink")(*Cyclothone pallida*)
  - [擬斜齒圓罩魚](../Page/擬斜齒圓罩魚.md "wikilink")(*Cyclothone
    pseudoacclinidens*)
  - [近蒼圓罩魚](../Page/近蒼圓罩魚.md "wikilink")(*Cyclothone pseudopallida*)
  - [侏圓罩魚](../Page/侏圓罩魚.md "wikilink")(*Cyclothone pygmaea*)
  - [封記圓罩魚](../Page/封記圓罩魚.md "wikilink")(*Cyclothone signata*)

### 雙光魚屬(*Diplophos*)

  - [澳洲雙光魚](../Page/澳洲雙光魚.md "wikilink")(*Diplophos australis*)
  - [東方雙光魚](../Page/東方雙光魚.md "wikilink")(*Diplophos orientalis*)
  - [太平洋雙光魚](../Page/太平洋雙光魚.md "wikilink")(*Diplophos pacificus*)
  - [里氏雙光魚](../Page/里氏雙光魚.md "wikilink")(*Diplophos rebainsi*)
  - [帶紋雙光魚](../Page/帶紋雙光魚.md "wikilink")(*Diplophos taenia*)

### 鑽光魚屬(*Gonostoma*)

  - [西鑽光魚](../Page/西鑽光魚.md "wikilink")(*Gonostoma atlanticum*)
  - [裸鑽光魚](../Page/裸鑽光魚.md "wikilink")(*Gonostoma denudatum*)
  - [長鑽光魚](../Page/長鑽光魚.md "wikilink")(*Gonostoma elongatum*)

### 爵燈魚屬(*Manducus*)

  - [格雷氏爵燈魚](../Page/格雷氏爵燈魚.md "wikilink")(*Manducus greyae*)
  - [馬德拉爵燈魚](../Page/馬德拉爵燈魚.md "wikilink")(*Manducus maderensis*)

### 緣光魚屬(*Margrethia*)

  - [鈍吻緣光魚](../Page/鈍吻緣光魚.md "wikilink")(*Margrethia obtusirostra*)
  - [瓦氏緣光魚](../Page/瓦氏緣光魚.md "wikilink")(*Margrethia valentinae*)

### 纖鑽光魚屬(*Sigmops*)

  - [艾氏鑽光魚](../Page/艾氏鑽光魚.md "wikilink")(*Sigmops ebelingi*)
  - [纖鑽光魚](../Page/纖鑽光魚.md "wikilink")(*Sigmops gracilis*)
  - (*Sigmops longipinnis*)

### 三鉆光魚屬(*Triplophos*)

  - [三鉆光魚](../Page/三鉆光魚.md "wikilink")(*Triplophos hemingi*)

[\*](../Category/鑽光魚科.md "wikilink")