[Breville.jpg](https://zh.wikipedia.org/wiki/File:Breville.jpg "fig:Breville.jpg")
**家用电器**，简称**家电**，是指以[电能来进行驱动](../Page/电能.md "wikilink")（或以機械化動作）的用具，可幫助執行家庭雜務，如[炊食](../Page/炊食.md "wikilink")、食物保存或[清潔](../Page/清潔.md "wikilink")，除了家庭環境外，也可用於公司行號或是工業的環境裡。基本上，家用电器分為大型家電（[白色家电](../Page/白色家电.md "wikilink")、黑色家电）和小家电。

## 分類

### 大型家電

大型家電與小家電相比體積較為寬大，不易運送且較常固定擺放在一個位置，此外也可能因其耗電量較大而需要有特殊的裝置或[插頭來供應電力](../Page/插頭.md "wikilink")。

大型家電又可分為「白色家電」和「黑色家電」。白色家电是用来满足和提升基本生活功能（影音视听除外）的大型家电、如大型[-{zh-hans:空调;zh-tw:冷氣機;zh-hk:冷氣機;}-](../Page/空氣調節.md "wikilink")、[冰箱](../Page/冰箱.md "wikilink")、[洗衣机](../Page/洗衣机.md "wikilink")、[电炉](../Page/电炉.md "wikilink")、[微波炉](../Page/微波炉.md "wikilink")、[水波爐和](../Page/水波爐.md "wikilink")[电热水器等](../Page/电热水器.md "wikilink")。黑色家电則是用来提供影音视听娱乐功能，如[电视机](../Page/电视机.md "wikilink")、[影碟机](../Page/影碟机.md "wikilink")（[VCD](../Page/VCD.md "wikilink")、[DVD播放机](../Page/DVD播放机.md "wikilink")）、[家用遊戲機](../Page/遊戲機.md "wikilink")、家庭[音响和家用](../Page/音响.md "wikilink")[電話](../Page/電話.md "wikilink")、跑步機等。

<File:Korean> fans.jpg|大型[電風扇](../Page/電風扇.md "wikilink") <File:Air>
conditioner.svg|大型[空调](../Page/空调.md "wikilink")
<File:Refrigerator1.svg>|[冰箱](../Page/冰箱.md "wikilink")
<File:ElectricBlender.jpg>|[果汁機](../Page/果汁機.md "wikilink")
[File:Weibolu.jpg|大型](File:Weibolu.jpg%7C大型)[微波炉](../Page/微波炉.md "wikilink")
<File:LGwashingmachine.jpg>|[洗衣機](../Page/洗衣機.md "wikilink")
<File:Geschirrspuelmaschine.JPG>|[洗碗機](../Page/洗碗機.md "wikilink")
<File:Hyundai> HY-DV805
20100701.jpg|[DVD播放机](../Page/DVD播放机.md "wikilink")
<File:Eupa> TSI-IH1880.jpg|[电磁炉](../Page/电磁炉.md "wikilink") <File:Fatty>
watching himself on TV.jpg|[电视机](../Page/电视机.md "wikilink")
<File:Shower> Borth.JPG|[电热水器](../Page/电热水器.md "wikilink")

### 小家電

小家電，指的是體積較小便於攜帶，或是用在桌面上以及其他平台上的家用電器，它們可以被人拿起是一個很好的判斷標準。與黑色家電（或白色家電）不同的是，它們能夠幫助改善室內的狀況，如[濕度](../Page/濕度.md "wikilink")、[溫度等](../Page/溫度.md "wikilink")。

小家電需要電力驅動，所以都會帶有一個[插頭用來連接](../Page/插頭.md "wikilink")[插座](../Page/插座.md "wikilink")，有些甚至有電力儲存的裝置，另外也有一些手持式的家電是利用[電池供應電能](../Page/電池.md "wikilink")。某些家電會有[馬達裝置](../Page/馬達.md "wikilink")，如[攪拌器](../Page/攪拌器.md "wikilink")、[食品加工机或](../Page/食品加工机.md "wikilink")[果汁機](../Page/果汁機.md "wikilink")。

不正確使用小家電可能會導致[火災和財產損失](../Page/火災.md "wikilink")，若清潔不淨也會滋生細菌，所以使用者必須詳細閱讀使用說明書，並且確定家電帶有[地線可將多餘電力導入地底](../Page/地線.md "wikilink")。因為有火災的風險，有些家電會使用磁力附著的電線插頭，若家電與插座的距離過遠，電線便可以輕易地鬆脫，以防萬一。

常見的小家電如下：

  - [电动剃须刀](../Page/剃须刀.md "wikilink")、[除毛器](../Page/除毛器.md "wikilink")、[电动牙刷](../Page/电动牙刷.md "wikilink")、[-{zh-hans:电吹风;zh-hk:風筒;zh-tw:吹風機;}-](../Page/風筒.md "wikilink")
  - [吸塵器](../Page/吸塵器.md "wikilink")、[电熨斗](../Page/电熨斗.md "wikilink")
  - [电饭煲](../Page/电饭煲.md "wikilink")、[电压力锅](../Page/壓力鍋.md "wikilink")、可攜[電磁爐](../Page/電磁爐.md "wikilink")、[電熱水壺](../Page/電熱水壺.md "wikilink")、[微波炉](../Page/微波炉.md "wikilink")、[-{zh-hans:烤面包机;
    zh-hant:烤麵包機; zh-cn:烤面包机; zh-tw:烤麵包機;
    zh-hk:多士爐;}-](../Page/烤面包机.md "wikilink")
  - [攪拌器](../Page/攪拌器.md "wikilink")、[食品調理机](../Page/食品調理机.md "wikilink")、[果汁機](../Page/果汁機.md "wikilink")、[咖啡機](../Page/咖啡機.md "wikilink")、[麵包機](../Page/麵包機.md "wikilink")、豆漿機、[食物處理机](../Page/食物處理机.md "wikilink")
  - [除濕機](../Page/除濕機.md "wikilink")、[加濕機](../Page/加湿器.md "wikilink")
  - [縫紉機](../Page/縫紉機.md "wikilink")
  - [電風扇](../Page/電風扇.md "wikilink")、[空调](../Page/空调.md "wikilink")、
    水冷扇、[電暖器](../Page/红外加热器.md "wikilink")
  - [檯燈](../Page/檯燈.md "wikilink")

## 产业特性

家电产业有其特性，作为[经济发展的产业发展过程](../Page/经济.md "wikilink")，往往继成衣、農產加工业等经济发展入门产业之后，在[汽车等成熟工业化產品之前](../Page/汽车.md "wikilink")，成为承前启后的产业发展阶段的主要产业，[日本](../Page/日本.md "wikilink")、[亚洲四小龙](../Page/亚洲四小龙.md "wikilink")、[中国大陆等地区的家电产业发展突出体现了这个特点](../Page/中国大陆.md "wikilink")。

[家用電器](../Category/家用電器.md "wikilink")