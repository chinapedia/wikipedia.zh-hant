[Zhengguo_Canal_Map_zh.png](https://zh.wikipedia.org/wiki/File:Zhengguo_Canal_Map_zh.png "fig:Zhengguo_Canal_Map_zh.png")
**鄭國渠**，[中国](../Page/中国.md "wikilink")[战国时](../Page/战国.md "wikilink")，[韓國著名](../Page/韩国_\(战国\).md "wikilink")[水利学家](../Page/水利.md "wikilink")「[郑国](../Page/郑国_\(水工\).md "wikilink")」（鄭氏，名國），于[秦王政元年](../Page/秦王政.md "wikilink")（前246年）为[秦所筑河渠](../Page/秦国.md "wikilink")，長三百里，位於今日中國[陝西省](../Page/陝西省.md "wikilink")[涇陽縣上然村涇出口一帶](../Page/涇陽縣.md "wikilink")。為紀念[工程師鄭國](../Page/工程師.md "wikilink")，称“**郑国渠**”。

## 建造經過

鄭國渠由[韓國水工](../Page/韩国_\(战国\).md "wikilink")[郑国主持修建](../Page/郑国_\(水工\).md "wikilink")。郑国原本是西去秦国的[細作](../Page/細作.md "wikilink")，劝说[秦王政兴修](../Page/秦王政.md "wikilink")[水利工程](../Page/水利.md "wikilink")，企图使秦国把經費與人力放在国内，无暇佈署[东征](../Page/东征.md "wikilink")。後來秦王發覺鄭的陰謀，怒欲殺之，鄭卻說：「臣起初確實是來當[間諜的](../Page/間諜.md "wikilink")，但是渠道修建完成也對[秦國有利](../Page/秦國.md "wikilink")；臣幫助韓國延長短短幾年的國祚，卻可以為秦國創建萬世的大功。」\[1\]秦王政甚以為然，工程得以繼續進行，於公元前246年開始使用。\[2\]

## 流域

鄭國渠在[涇河從北方山谷流入](../Page/涇河.md "wikilink")[關中平原處](../Page/關中.md "wikilink")，把部份河水引入主渠；主渠起初與涇河平行而相近，進入關中平原時分開；渠道灌溉系統分為幾支，水流通過叫「斗」的出水閘門分別進入溝渠，流向農田。\[3\]郑国渠完工後灌渠全長三百[里](../Page/里.md "wikilink")，以谷口為起點，把[涇水引入](../Page/涇水.md "wikilink")[櫟陽的](../Page/櫟陽.md "wikilink")[渭水](../Page/渭水.md "wikilink")。

鄭國渠的经行地区，[郦道元在](../Page/郦道元.md "wikilink")《[水经注](../Page/水经注.md "wikilink")·沮水》中记称：“渠首上承泾水于中山西邸瓠口，……渠渎东径宜秋城北，又东径中山南，……又东径舍车宫南绝冶谷水。郑渠故渎又东径嶻嶭山南、池阳县故城北，又东绝清水，又东径北原下，浊水注焉，自浊水以上，今无人。……又东历原径曲梁城北，又东径太上陵南原下，北屈径原东，与沮水合。……沮循郑渠，东径当道城南，……又东径莲芍县故城北，……又东径粟邑县故城北，……其水又东北流，注于洛水也。”

## 作用

鄭國渠的作用維持了幾十年，\[4\]有良好的经济效益和政治效益，可澆灌[關中農田](../Page/關中.md "wikilink")4萬餘頃，約相當於現今7.4-24萬公頃，收皆畝一鍾。\[5\]於是關中為沃野，無凶年，[農業大盛](../Page/農業.md "wikilink")，無[水災](../Page/水災.md "wikilink")、[旱災](../Page/旱災.md "wikilink")。秦以富疆，卒之[吞併諸侯](../Page/秦滅六國.md "wikilink")\[6\]。

## 後繼者

[漢武帝](../Page/漢武帝.md "wikilink")[元鼎六年](../Page/元鼎.md "wikilink")（前111年），又穿鑿[六輔渠](../Page/六輔渠.md "wikilink")，[太始二年因應](../Page/太始.md "wikilink")[趙國](../Page/趙國.md "wikilink")[中大夫](../Page/中大夫.md "wikilink")[白公之請](../Page/白公.md "wikilink")，於鄭國渠之南，穿渠引[涇水](../Page/涇水.md "wikilink")，名[白渠](../Page/白渠.md "wikilink")，統稱**[鄭白渠](../Page/鄭白渠.md "wikilink")**。時民有歌曰：「田於何所？池陽谷口。鄭國在前，白渠在後。舉鍤為雲，決渠如雨。[涇水一石](../Page/涇水.md "wikilink")，其泥六斗。且溉且糞，長我禾忝。衣食[京師](../Page/京師.md "wikilink")，億萬之口。」

## 参考文献

## 參見

  - [白渠](../Page/白渠.md "wikilink")
  - [三白渠](../Page/三白渠.md "wikilink")
  - [豐利渠](../Page/豐利渠.md "wikilink")
  - [王御使渠](../Page/王御使渠.md "wikilink")
  - [廣惠渠](../Page/廣惠渠.md "wikilink")
  - [通濟渠](../Page/通濟渠.md "wikilink")
  - [龍洞渠](../Page/龍洞渠.md "wikilink")
  - [涇惠渠](../Page/涇惠渠.md "wikilink")

[Category:陕西水利](../Category/陕西水利.md "wikilink")
[Category:陕西农业史](../Category/陕西农业史.md "wikilink")
[Category:黄河水系](../Category/黄河水系.md "wikilink")
[Category:中国古代灌渠](../Category/中国古代灌渠.md "wikilink")
[Category:秦国建筑](../Category/秦国建筑.md "wikilink")
[Category:战国建筑](../Category/战国建筑.md "wikilink")

1.  《史記·河渠書》《漢書·溝洫志》：「始臣為間，然渠成亦秦之利也。臣為韓延數歲之命，而為秦建萬世之功。」
2.  Pierre-Etienne
    Will（魏丕信）著，王湘雲譯：〈軍閥和國民黨時期陝西省的灌溉工程與政治〉，載《法國漢學》，第九輯（北京：中華書局，2004），頁268。
3.  Will：〈軍閥和國民黨時期陝西省的灌溉工程與政治〉，頁269。
4.  Will：〈軍閥和國民黨時期陝西省的灌溉工程與政治〉，頁269。
5.  Will：〈軍閥和國民黨時期陝西省的灌溉工程與政治〉，頁311。
6.  《史記．河渠書》：“凿泾水自中山西邸瓠口（今[陕西](../Page/陕西.md "wikilink")[泾阳西北仲山](../Page/泾阳.md "wikilink")）为渠，并北山东注洛三百余里，欲以溉田。”
    “渠就，用注填阏之水，溉泽卤之地四万余顷，收皆亩一钟。于是关中为沃野，无凶年，秦以富强，卒并诸侯。”