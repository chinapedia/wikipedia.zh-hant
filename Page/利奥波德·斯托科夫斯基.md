[Leopold_Stokowski_LOC_26447u.jpg](https://zh.wikipedia.org/wiki/File:Leopold_Stokowski_LOC_26447u.jpg "fig:Leopold_Stokowski_LOC_26447u.jpg")

**利奥波德·斯托科夫斯基**（[英语](../Page/英语.md "wikilink")：****，），[英国指挥家](../Page/英国.md "wikilink")，古典音乐改编家。

斯托科夫斯基生前对自己的身世三缄其口，现在已知他出生于[伦敦并且以他的祖父列奥波尔德之名受洗](../Page/伦敦.md "wikilink")。

他就讀于牛津皇后学院、伦敦皇家音乐学院，后去[巴黎及](../Page/巴黎.md "wikilink")[慕尼黑深造](../Page/慕尼黑.md "wikilink")。

1905年去美国，任圣巴索洛缪教堂[管风琴手及圣咏团乐长](../Page/管风琴.md "wikilink")；1909年～1912年任辛辛那提交响乐团指挥；1912年～1938年任费城交响乐团指挥；后来又在美国青年交响乐团、[纽约爱乐乐团](../Page/纽约爱乐乐团.md "wikilink")、休斯顿交响乐团等担任指挥，成了20世纪一位成功的指挥家，虽然饱受争议。

斯托科夫斯基大概主持过2000场的作品首演，他对[巴赫作品的改编使得他名气崛起](../Page/巴赫.md "wikilink")。例如为管风琴而作的[d小调触技与赋格曲](../Page/D小調托卡塔與賦格,_BWV_565.md "wikilink")，他利用大型管弦乐队使作品焕发出前所未有的魅力。

斯托科夫斯基因其“音响魔术”倍受赞誉，但又因他对原曲的肆意改动而受到非议。他加盟[迪士尼的电影](../Page/迪士尼.md "wikilink")《[幻想曲](../Page/幻想曲_\(电影\).md "wikilink")》，使影片的配乐成为经典，因此获得[奥斯卡荣誉奖](../Page/奥斯卡荣誉奖.md "wikilink")。

怪杰钢琴家[格连·古尔德形容斯托科夫斯基是几个难得能让他由衷佩服的指挥家之一](../Page/格连·古尔德.md "wikilink")。他的[贝多芬第五钢琴协奏曲就是和斯托科夫斯基一起灌录的](../Page/贝多芬.md "wikilink")。

斯托科夫斯基与女演员[格洛丽亚·万德比特成婚](../Page/格洛丽亚·万德比特.md "wikilink")。

1979年利奥波德·斯托科夫斯基协会成立。协会致力于使世人永远记住这位大师，并且将他的录音再发布。

## 文字作品

  - *大众音乐*. Simon & Schuster, New York 1943

## 电影

  - 1936年 《1937年大广播》
  - 1937年 《100个男人和1个女孩》
  - 1940年 《幻想曲》（Fantasia）
  - 1947年 《卡耐基乐厅》

## 外部链接

  - [Homepage der Leopold Stokowski
    Society](https://web.archive.org/web/20051203230650/http://www.stokowskisociety.net/)
  - [Greta Garbo und Leopold Stokowski erleben eine gemeinsame Romanze
    in Italien ...](http://www.greta-garbo.de/presse.html#gretakommt)

[S](../Category/20世紀指揮家.md "wikilink")
[Category:英国指挥家](../Category/英国指挥家.md "wikilink")
[Category:奧斯卡終身成就獎獲得者](../Category/奧斯卡終身成就獎獲得者.md "wikilink")
[Category:范德比尔特家族](../Category/范德比尔特家族.md "wikilink")