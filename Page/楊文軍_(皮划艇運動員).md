**楊文軍**（），[江西省](../Page/江西省.md "wikilink")[宜春](../Page/宜春.md "wikilink")[丰城人](../Page/丰城.md "wikilink")，[中國](../Page/中國.md "wikilink")[皮划艇静水项目運動員兼助理教练](../Page/皮划艇.md "wikilink")。

## 生涯

楊文軍被中国皮划艇队[澳大利亚籍](../Page/澳大利亚.md "wikilink")[波兰裔外教马克](../Page/波兰.md "wikilink")（Marek
Ploch）从江西队中提拔上来，後來楊文軍由于在训练中多次违背教练的意图和指示而被退回省隊。後來，年青的楊文軍在省隊中有着优秀的表现，再次入選國家隊，之後又因同一原因被退回省隊。

此后，楊文軍再次有幸被喚回國家隊，後來因為跟不上教練的严酷的訓練進度而再三退回省隊。之後，國家隊為了準備[悉尼奧運而第四次召唤他入队](../Page/悉尼奧運.md "wikilink")，再之後，杨文军再也没有离开过国家队。

在2002年[釜山亚运会上杨文军贏得了男子C](../Page/2002年亞洲運動會.md "wikilink")2-500米與C2-1000米項目的[金牌](../Page/金牌.md "wikilink")，1年後於世錦賽的C1-1000米項目名列第7。

2004年，楊文軍與[孟關良搭档](../Page/孟關良.md "wikilink")，以1分40秒278的成绩，夺得了[雅典奧運会C](../Page/2004年夏季奧林匹克運動會.md "wikilink")2-500米項目的[金牌](../Page/金牌.md "wikilink")，這也是[中國](../Page/中國.md "wikilink")[奧運史上的首面皮划艇金牌](../Page/奧運.md "wikilink")。\[1\]\[2\]\[3\]

在雅典奥运会之后，个人能力明显提升的杨文军，开始专攻单人艇\[4\]。2005年，楊文軍於[十運會贏得C](../Page/十運會.md "wikilink")1-100米以及C1-500項目為[江西隊贏得兩面](../Page/江西.md "wikilink")[金牌](../Page/金牌.md "wikilink")。2006年12月，他在[多哈亞運会上以](../Page/2006年亞洲運動會.md "wikilink")1:55.009的成績贏得C1-500米項目的金牌。另外，在该项赛事中他还取得了男子C2-500米项目的[銀牌](../Page/銀牌.md "wikilink")。

在2005年[克罗地亚](../Page/克罗地亚.md "wikilink")[萨格勒布世锦赛上](../Page/萨格勒布.md "wikilink")，他参加了500米、1000米和200米三项比赛，分别取得了第6名、第7名和第10名的成绩。在2006年[匈牙利](../Page/匈牙利.md "wikilink")[塞格德世锦赛中](../Page/塞格德.md "wikilink")，杨文军开始专攻短距离项目，获得了C1-500米的铜牌，这也是中国队在皮划艇世锦赛上取得了第一块男子项目的奖牌，另外杨文军还参加了200米的比赛取得了第5名。在2007年[德国](../Page/德国.md "wikilink")[杜伊斯堡的世锦赛上](../Page/杜伊斯堡.md "wikilink")，杨文军再次夺得一枚C1-500米项目的铜牌。

2008年北京奧運會，他與拍檔[孟關良於C](../Page/孟關良.md "wikilink")2-500米比賽中以1分41秒25成功衛冕金牌。

2009年的[十一運會之後](../Page/中華人民共和國第十一屆全國運動會.md "wikilink")，楊文軍退役，之後出任江西省水上運動管理中心副主任，分管皮划艇。\[5\]

## 資料來源

<div class="references-small">

<references />

</div>

[category:中国奥运皮划艇运动员](../Page/category:中国奥运皮划艇运动员.md "wikilink")
[Wenjun](../Page/category:楊姓.md "wikilink")

[Category:中国皮划艇运动员](../Category/中国皮划艇运动员.md "wikilink")
[Category:2006年亞洲運動會金牌得主](../Category/2006年亞洲運動會金牌得主.md "wikilink")
[Category:中國奧林匹克運動會金牌得主](../Category/中國奧林匹克運動會金牌得主.md "wikilink")
[Category:丰城人](../Category/丰城人.md "wikilink")
[Category:2004年夏季奧林匹克運動會輕艇運動員](../Category/2004年夏季奧林匹克運動會輕艇運動員.md "wikilink")
[Category:2008年夏季奧林匹克運動會輕艇運動員](../Category/2008年夏季奧林匹克運動會輕艇運動員.md "wikilink")
[Category:2004年夏季奧林匹克運動會金牌得主](../Category/2004年夏季奧林匹克運動會金牌得主.md "wikilink")
[Category:2008年夏季奧林匹克運動會獎牌得主](../Category/2008年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:奧林匹克運動會輕艇獎牌得主](../Category/奧林匹克運動會輕艇獎牌得主.md "wikilink")
[Category:2002年亞洲運動會金牌得主](../Category/2002年亞洲運動會金牌得主.md "wikilink")
[Category:2006年亞洲運動會銀牌得主](../Category/2006年亞洲運動會銀牌得主.md "wikilink")

1.  [【奥运岁月】杨文军讲述蹬来的金牌](http://space.tv.cctv.com/video/VIDE1208274529418312)

2.  [【我的奥林匹克】杨文军](http://www.cctv.com/video/wodeaolinpike/2007/04/wodeaolinpike_300_20070403_1.shtml)
3.  [【我的奥林匹克】孟关良、杨文军](http://space.tv.cctv.com/video/VIDE1215439898396502)

4.  [杨文军入选奥运火炬手名单](http://www.cnr.cn/2008zt/hjcd/csmp/jx/qxp/200804/t20080422_504769702.html)
5.  [奥运冠军杨文军：冠军是拼出来的](http://news.sina.com.cn/c/2010-01-22/170716981696s.shtml).中国江西网.2010-1-23