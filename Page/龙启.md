**龙启**（933年-934年）是[闽惠宗](../Page/闽.md "wikilink")[王延钧的年号](../Page/王延钧.md "wikilink")，共计2年。这是闽政权所使用的第一个自己的年号。

## 纪年

| 龙启                               | 元年                             | 二年                             |
| -------------------------------- | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 933年                           | 934年                           |
| [干支](../Page/干支纪年.md "wikilink") | [癸巳](../Page/癸巳.md "wikilink") | [甲午](../Page/甲午.md "wikilink") |

## 參看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 同期存在的其他政权年号
      - [長興](../Page/長興_\(李嗣源\).md "wikilink")（930年二月至933年十二月）：[後唐](../Page/後唐.md "wikilink")—[李嗣源之年號](../Page/李嗣源.md "wikilink")
      - [應順](../Page/應順.md "wikilink")（934年正月至四月）：後唐—[李從厚之年號](../Page/李從厚.md "wikilink")
      - [清泰](../Page/清泰.md "wikilink")（934年四月至936閏十一月）：後唐—[李從珂之年號](../Page/李從珂.md "wikilink")
      - [大和](../Page/大和_\(楊溥\).md "wikilink")（929年十月至935年八月）：[吳](../Page/吳_\(十國\).md "wikilink")—楊溥之年號
      - [大有](../Page/大有_\(年號\).md "wikilink")（928年三月至942年三月）：[南漢](../Page/南漢.md "wikilink")—[劉龑之年號](../Page/劉龑.md "wikilink")
      - [明德](../Page/明德_\(孟知祥\).md "wikilink")（934年四月至937年）：[後蜀](../Page/後蜀.md "wikilink")—[孟知祥之年號](../Page/孟知祥.md "wikilink")
      - [大明](../Page/大明_\(楊干真\).md "wikilink")（931年至937年）：[大義寧](../Page/大義寧.md "wikilink")—[楊干真之年號](../Page/楊干真.md "wikilink")
      - [天顯](../Page/天顯.md "wikilink")（926年二月至938年）：[契丹](../Page/遼朝.md "wikilink")—[耶律阿保機](../Page/耶律阿保機.md "wikilink")、[耶律德光之年號](../Page/耶律德光.md "wikilink")
      - [甘露](../Page/甘露_\(耶律倍\).md "wikilink")（926年至936年）：[東丹](../Page/東丹.md "wikilink")—[耶律倍之年號](../Page/耶律倍.md "wikilink")
      - [同慶](../Page/同慶_\(尉遲烏僧波\).md "wikilink")（912年至966年）：[于闐](../Page/于闐.md "wikilink")—[尉遲烏僧波之年號](../Page/尉遲烏僧波.md "wikilink")
      - [天授](../Page/天授_\(高麗太祖\).md "wikilink")（918年—933年）：[高麗](../Page/高麗.md "wikilink")—[高麗太祖王建之年號](../Page/高麗太祖.md "wikilink")
      - [承平](../Page/承平_\(朱雀天皇\).md "wikilink")（931年四月二十六日至938年五月二十二日）：日本[朱雀天皇年号](../Page/朱雀天皇.md "wikilink")

[Category:闽年号](../Category/闽年号.md "wikilink")
[Category:930年代中国政治](../Category/930年代中国政治.md "wikilink")
[Category:933年](../Category/933年.md "wikilink")
[Category:934年](../Category/934年.md "wikilink")