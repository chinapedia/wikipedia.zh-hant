**乙酸钠**（，化學式：CH<sub>3</sub>COONa），又名**醋酸鈉**，晶體有無水和三水合物兩種形式。

## 物理性質

無水醋酸鈉（CH<sub>3</sub>COONa）為白色或灰白色的粉末，比重1.528，熔點324℃，溶於水，難溶於[有機溶劑](../Page/有機溶劑.md "wikilink")，水溶液呈鹼性。

結晶的三水合醋酸鈉（CH<sub>3</sub>COONa·3H<sub>2</sub>O）比重1.45，熔點58℃，在120℃時即失去所含的[結晶水](../Page/結晶水.md "wikilink")、變成無水醋酸鈉；且在大氣中有風化性，亦能逐漸失去水分，日久而成白色粉末。

## 化學反应

乙酸钠和[卤代烷](../Page/卤代烷.md "wikilink")（如[溴乙烷](../Page/溴乙烷.md "wikilink")）可用來产生[酯](../Page/酯.md "wikilink")：

  -
    CH<sub>3</sub>COONa + CH<sub>3</sub>CH<sub>2</sub>Br →
    [CH<sub>3</sub>COOCH<sub>2</sub>CH<sub>3</sub>](../Page/乙酸乙酯.md "wikilink")
    + [NaBr](../Page/溴化钠.md "wikilink")

此反应可以用[铯盐來催化](../Page/铯盐.md "wikilink")。

用[碳酸氫钠与](../Page/碳酸钠.md "wikilink")[醋酸生成](../Page/醋酸.md "wikilink")[醋酸鈉](../Page/醋酸鈉.md "wikilink")。

## 应用

### 工业

  - 纺织业：使用[苯胺的时候作为](../Page/苯胺.md "wikilink")[光刻胶使用](../Page/光刻胶.md "wikilink")，同时也可以用以中和硫酸废气
  - 合成橡胶生产：防止[氯丁二烯氯化](../Page/氯丁二烯.md "wikilink")
  - 一次性棉花垫生产：用于防止[静电的产生](../Page/静电.md "wikilink")

### 混凝土延寿

乙酸钠可以作为防水剂使用以减轻水对混凝土的损伤，相比常用的环氧树脂防水剂更环保经济。\[1\]

### 食品

乙酸钠可以作为调料（酸味剂）加入食品中（通常以[双乙酸钠的形式加入](../Page/双乙酸钠.md "wikilink")，化学式(CH<sub>3</sub>COO)<sub>2</sub>NaH），醋+盐味的薯片就是使用乙酸钠来调料的。欧盟给予乙酸钠[E编码E](../Page/E编码.md "wikilink")262作为其认可的食品添加物的编号，具有E编号的添加物代表已经由欧盟核可，能够使用在食物中。

### 缓冲剂

作为乙酸的共轭碱，乙酸钠可以与乙酸配置缓冲液，pH调节范围为3.7-5.6，这种缓冲剂在生物化学学科中非常有用。

### 暖包

[暖包中的一種即是由醋酸鈉的過飽和溶液製作而成](../Page/暖包.md "wikilink")。醋酸鈉的過飽和溶液可以通過加熱溶解醋酸鈉晶體後冷卻熱溶液的方式獲得，其中加熱溫度需超過晶體的熔點。三水合乙酸鈉的熔點為58℃。使用這暖包時，掰動暖包以形成一個晶核，溶液中的醋酸鈉晶體隨之析出放熱。與其他形式的暖包不同的是，只要在沸水中加熱數分鐘，這種暖包就可以再次使用。\[2\]

### 其他应用

  - [碱石灰与乙酸钠共热脱羧得到](../Page/碱石灰.md "wikilink")[碳酸钠和](../Page/碳酸钠.md "wikilink")[甲烷](../Page/甲烷.md "wikilink")。
  - 利用[结晶化放出的热可以取暖](../Page/结晶化.md "wikilink")。
  - 乙酸钠也作为电镀添加剂，提高镀层光泽以及作为电镀池消泡剂。

## 外部链接

  - [“热冰” – 制作教学、
    图像和视频](http://www.amazingrust.com/Experiments/how_to/Hot-Ice.html)

## 来源

[Category:乙酸盐](../Category/乙酸盐.md "wikilink")
[Category:有机酸钠盐](../Category/有机酸钠盐.md "wikilink")
[Category:食品添加剂](../Category/食品添加剂.md "wikilink")
[Category:照相药品](../Category/照相药品.md "wikilink")

1.
2.