**Apache Flex**（前稱**Adobe
Flex**）是一个基于[Adobe](../Page/Adobe.md "wikilink")[Flash平台的](../Page/Flash.md "wikilink")，用以开发和部署[RIA](../Page/多樣化網際網路應用程式.md "wikilink")（Rich
Internet Applications）的[SDK](../Page/軟體開發套件.md "wikilink")（Software
Development
Kit）。最初由[Macromedia公司于](../Page/Macromedia.md "wikilink")2004年3月发布，并且后来由[Adobe收购](../Page/Adobe.md "wikilink")，并由[Adobe于](../Page/Adobe.md "wikilink")2011年将之捐献给[Apache软件基金会](../Page/Apache软件基金会.md "wikilink")，在2012年推动成为首要项目。

Flex 3
[SDK在](../Page/軟體開發套件.md "wikilink")2008年以[开放源代码的](../Page/开放源代码.md "wikilink")[Mozilla公共许可证释出](../Page/Mozilla公共许可证.md "wikilink")。因此，可以用通用的[集成开发环境开发Flex应用](../Page/集成开发环境.md "wikilink")，如[IntelliJ
IDEA](../Page/IntelliJ_IDEA.md "wikilink")、[Eclipse](../Page/Eclipse.md "wikilink")、[自由及开放源代码的IDE](../Page/自由及开放源代码.md "wikilink")
[FlashDevelop](../Page/FlashDevelop.md "wikilink")，以及专属软件[Adobe Flash
Builder](../Page/Adobe_Flash_Builder.md "wikilink")。最新版的SDK版本是4.16.1，采用第2版的[Apache许可证释出](../Page/Apache许可证.md "wikilink")。

## 成因

传统的程序员在开发动画应用方面存在困难，Flex 平台最初就是因此而产生。Flex
试图通过提供一个程序员们已经熟知的工作流和编程模型来改善这个问题。

Flex 最初是作为一个[J2EE](../Page/J2EE.md "wikilink")（Java 2 Platform,
Enterprise Edition）应用，或者可以说是[JSP](../Page/JSP.md "wikilink")（JavaServer
Pages）标签库而发布的。它可以把运行中的[MXML](../Page/MXML.md "wikilink")（Flex标记语言）和[ActionScript编译成FLASH应用程序](../Page/ActionScript.md "wikilink")（即二进制的SWF文件）。最新版的FLEX支持创建静态文件，该文件使用解释编译方式并且不需要购买服务器许可证就可以在线部署。

Flex的目标是让程序员更快更简单地开发RIA应用。在多层式开发模型中，Flex应用属于表现层。

Flex 采用[GUI界面开发](../Page/圖形使用者介面.md "wikilink")，使用基于XML的MXML语言。Flex
具有多种组件，可实现Web Services，远程对象，drag and
drop，列排序，图表等功能；FLEX内建动画效果和其它简单互动界面等。相对于基于HTML的应用（如[PHP](../Page/PHP.md "wikilink")、[ASP](../Page/Active_Server_Pages.md "wikilink")、[JSP](../Page/JSP.md "wikilink")、[ColdFusion及](../Page/ColdFusion.md "wikilink")[CFMX等](../Page/CFMX.md "wikilink")）在每个请求时都需要执行服务器端的模板，由于客户端只需要载入一次，FLEX应用程序的工作流被大大改善。FLEX的语言和文件结构也试图把应用程序的逻辑从设计中分离出来。

Flex 服务器也是客户端和XML Web Services及远程对象（Coldfusion
CFCs，或[Java类](../Page/Java.md "wikilink")，等支持Action Message
Format的其他对象）之间通讯的通路。

一般被认为可能是 Flex
替代品的是[OpenLaszlo和](../Page/OpenLaszlo.md "wikilink")[AJAX技术](../Page/AJAX.md "wikilink")。

但在2014後重大的變革：由原先MXML+CSS+AS的發展路線，另外追加產生jｓ發展路線，在移動平台發展動盪的2014年隨著ＡＩＲ跨平台的支援上，產生了更彈性的變數
目前普遍認為相較於HTML更合適在移動平台App發展更有前瞻性

## Flex 和 ColdFusion

Macromedia 把一部份 Flex 1.5 的子集嵌入到了它的ColdFusion MX 7中间件平台中以供在 Flash
表单中使用。虽然可以使用这个平台来开发RIA，但是它原来的目的只是为了开发丰富的表单应用，所以这个功能并不为Macromedia
所支持。

## Flex 应用开发步骤

  - 使用 Flash Builder
  - 使用一系统预定义组件（窗口，按钮等）来定义一个开发界面。

<!-- end list -->

  - 组织安排组件，现在用户自定义的界面设计。

<!-- end list -->

  - 使用风格和主题来定义可见设计。

<!-- end list -->

  - 增加动态动作，如应用程序之间的互动。

<!-- end list -->

  - 定义并在需要时连接上一个数据服务。

<!-- end list -->

  - 从源代码轉換成各種平台安裝文件。

## 版本历史

|        |             |
| ------ | ----------- |
| 1.0    | /           |
| 1.5    | /           |
| 2      | /           |
| 3      | 2007年4月26日  |
| 4      | 2010年3月22日  |
| 4.5    | 2011年5月3日   |
| 4.8.0  | 2012年7月25日  |
| 4.9.0  | 2013年1月11日  |
| 4.9.1  | 2013年2月28日  |
| 4.10.0 | 2013年8月6日   |
| 4.11.0 | 2013年10月28日 |
| 4.12.1 | 2014年5月3日   |
| 4.13.0 | 2014年7月28日  |
| 4.14.0 | 2015年2月3日   |
| 4.14.1 | 2015年3月31日  |
| 4.15.0 | 2016年1月11日  |
| 4.16.0 | 2017年3月12日  |
| 4.16.1 | 2017年11月22日 |

## 相关术语

  - [XUL](../Page/XUL.md "wikilink")
  - [Thin client](../Page/Thin_client.md "wikilink")
  - [XAML](../Page/XAML.md "wikilink")
  - [Flash Player](../Page/Adobe_Flash_Player.md "wikilink")
  - [OpenLaszlo](../Page/OpenLaszlo.md "wikilink")
  - [MXML](../Page/MXML.md "wikilink")

## 技术邮件列表

  - [Flexcoders - Flex Developer Mailing
    List](http://groups.yahoo.com/group/flexcoders/)
  - [ColdFusion Flex
    List](https://web.archive.org/web/20060721184418/http://www.houseoffusion.com/groups/flex/)
    at House of Fusion.

## 參考資料

## 外部链接

  - [Official Adobe Flex site](http://www.adobe.com/products/flex/)
  - [Flex
    官方起始页](http://webarchive.loc.gov/all/20090217012122/http://flex.org/)
  - [官方中文Flex摘要页](http://www.adobe.com/cn/products/flex/)
  - [中文Flex例子](https://web.archive.org/web/20080415135740/http://blog.minidx.com/)
  - [CFlex - Community Driven Adobe Flex Developer
    site](http://www.cflex.net)
  - [Flex Authority - Adobe Flex Developer
    site](http://www.flexauthority.com/)
  - [Jesse Warden - Adobe Flex Developer Blog
    Site](http://www.jessewarden.com)
  - [Renaun Erickson - Adobe Flex Developer Blog
    Site](http://renaun.com/blog)
  - [完全利用FLEX開發的線上GIS網站](http://ngis.moea.gov.tw/ngisfxweb/Default.aspx)
  - [ReflexUtil](http://sites.google.com/site/reflexutil) - 测试工具(开源) 运行时
    为 Flex
  - [纬度网 Flex
    知识库](https://web.archive.org/web/20101020161410/http://www.wedoswf.com/)
    - 提供围绕ActionScript，Flash，Flex技术开发为主题的问答形知识库（中文）

[Category:程序设计工具](../Category/程序设计工具.md "wikilink")
[Category:Adobe软件](../Category/Adobe软件.md "wikilink")
[Category:跨平台軟體](../Category/跨平台軟體.md "wikilink")