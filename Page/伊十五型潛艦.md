[Japanese_submarine_I-21.jpg](https://zh.wikipedia.org/wiki/File:Japanese_submarine_I-21.jpg "fig:Japanese_submarine_I-21.jpg")
**伊一五型潛艇**（西方稱為**B1
Type**）是[大日本帝国海軍的](../Page/大日本帝国海軍.md "wikilink")[潛艦型號](../Page/潛艦.md "wikilink")。又名**巡潛乙型**。

## 概要

本艦級的潛艦於日本第三次及第四次海軍補充計劃時建造20艘。[太平洋戰争時為數最多的大日本帝国海軍大型潛艦](../Page/太平洋戰争.md "wikilink")。除[伊號第三六潛水艦外其餘皆於戰爭中沉沒](../Page/伊號第三六潛水艦.md "wikilink")。設計以巡潛甲型([伊九型潛艇](../Page/伊九型潛水舰.md "wikilink"))為基礎。日本潜艇所击沉的商船吨位中四成是该种艇创下。\[1\]

創下日軍潛艇擊沉紀錄(11條船)的[伊21](../Page/伊号第二一潜水舰.md "wikilink")，撃沈[美国海軍](../Page/美国海軍.md "wikilink")[黃蜂號航空母艦及](../Page/胡蜂號航空母艦_\(CV-7\).md "wikilink")[奥布莱恩号驱逐舰并重创](../Page/奥布莱恩号驱逐舰.md "wikilink")[北卡罗莱纳号战列舰的](../Page/北卡羅萊納號戰艦_\(BB-55\).md "wikilink")[伊19](../Page/伊號第一九潛水舰.md "wikilink")，以搭載機進行[美國本土空襲的](../Page/美國本土空襲.md "wikilink")[伊25皆屬此艦級](../Page/伊號第二五潛水舰.md "wikilink")。改良型為[伊四零型潛艇與](../Page/伊四零型潛艇.md "wikilink")[伊五四型潛艇](../Page/伊五四型潛艇.md "wikilink")。[伊30](../Page/伊号第三〇潜水舰.md "wikilink")、[伊34](../Page/伊号第三四潜水舰.md "wikilink")、[伊29分别进行了第一](../Page/伊号第二九潜水舰.md "wikilink")、三、四次遣德潜艇航行。

## 改装

1942年（昭和17年）5月伊27、伊28经改装装载甲標的微型潜艇准备进行攻击悉尼，因此拆掉了后部的14CM炮。
1944年（昭和19年）伊36、伊37拆掉了14cm砲装载了四条回天鱼雷。另外似乎这時在机库上部装了22号雷达、艦橋上装了雷达告警仪1台。1945年伊36还将前部航空设备撤去装了两条回天。似乎这时也装了13号雷达。

## 諸元

  - 全長：108.70[米](../Page/米.md "wikilink")
  - 全幅：9.30米
  - 排水量：水上2198[噸](../Page/噸.md "wikilink")、水中3654噸
  - 最大速度：水上23.6[kt](../Page/節.md "wikilink")、水中8kt
  - 水中航續距離：178km(96[海里](../Page/海里.md "wikilink"))/3kt
  - 水上航續距離：25900km(14000海里)/16kt
  - 武装：53cm[魚雷發射管艦首](../Page/魚雷發射管.md "wikilink")×6、14cm砲×1、25mm機銃×2、零式小型水上偵察機×1、[魚雷](../Page/魚雷.md "wikilink")×17

## 同型艦

  - [伊號第一五潛艇](../Page/伊號第一五潛水舰.md "wikilink")
  - [伊號第一七潛艇](../Page/伊號第一七潛水舰.md "wikilink")
  - [伊號第一九潛艇](../Page/伊號第一九潛水舰.md "wikilink")
  - [伊號第二一潛艇](../Page/伊號第二一潛水舰.md "wikilink")
  - [伊號第二三潛艇](../Page/伊號第二三潛水舰.md "wikilink")
  - [伊號第二五潛艇](../Page/伊號第二五潛水舰.md "wikilink")
  - [伊號第二六潛艇](../Page/伊號第二六潛水舰.md "wikilink")
  - [伊號第二七潛艇](../Page/伊號第二七潛水舰.md "wikilink")
  - [伊號第二八潛艇](../Page/伊號第二八潛水舰.md "wikilink")
  - [伊號第二九潛艇](../Page/伊號第二九潛水舰.md "wikilink")
  - [伊號第三〇潛艇](../Page/伊號第三〇潛水舰.md "wikilink")
  - [伊號第三一潛艇](../Page/伊號第三一潛水舰.md "wikilink")
  - [伊號第三二潛艇](../Page/伊號第三二潛水舰.md "wikilink")
  - [伊號第三三潛艇](../Page/伊號第三三潛水舰.md "wikilink")
  - [伊號第三四潛艇](../Page/伊號第三四潛水舰.md "wikilink")
  - [伊號第三五潛艇](../Page/伊號第三五潛水舰.md "wikilink")
  - [伊號第三六潛艇](../Page/伊號第三六潛水舰.md "wikilink")
  - [伊號第三七潛艇](../Page/伊號第三七潛水舰.md "wikilink")
  - [伊號第三八潛艇](../Page/伊號第三八潛水舰.md "wikilink")
  - [伊號第三九潛艇](../Page/伊號第三九潛水舰.md "wikilink")

## 参考

[Category:日本潛艦](../Category/日本潛艦.md "wikilink")
[I](../Category/日本軍艦艦級.md "wikilink")
[Category:潛艇級別](../Category/潛艇級別.md "wikilink")

1.  《写真 日本の軍艦 第12巻》p123的说法