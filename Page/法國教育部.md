**法國教育部**，全稱**法國國家教育、高等教育与研究部**（）是[法國](../Page/法國.md "wikilink")[政府掌管自](../Page/法國政府.md "wikilink")[小學至](../Page/小學.md "wikilink")[高等教育以及](../Page/高等教育.md "wikilink")[學術研究的政府部門](../Page/學術研究.md "wikilink")，接洽事務也擴及[體育活動和青](../Page/體育.md "wikilink")、少年議題，其運作佔用法國政府最大的[預算比例](../Page/預算.md "wikilink")。

## 組織

  - 事務總局 (SG)

  - 構想局 (DE)

  - 評量、預測與成效局 (DEPP)

  - 財務局 (DAF)

  - 法務局 (DAJ)

  - 歐洲及國際關係協調局 (DREIC)

  - (IGENR)

  - (MESR)

## 外部連結

  - [法國教育部](http://www.education.gouv.fr/)

  - [法國教育部歷史](http://www.education.gouv.fr/histoire/histoire_ministere.htm)

  - [研究部門](http://www.recherche.gouv.fr/)

[Category:法國教育](../Category/法國教育.md "wikilink")
[教](../Category/法国内阁部門.md "wikilink")
[Category:教育部門](../Category/教育部門.md "wikilink")