**渤海成王**（），名**大華璵**，是[渤海國第五代君主](../Page/渤海國.md "wikilink")，[大钦茂之孙](../Page/大钦茂.md "wikilink")，在位期間793年至794年，年號為[中兴](../Page/中兴.md "wikilink")。他是[大欽茂的嫡孫](../Page/大欽茂.md "wikilink")，因參與了渤海國國人殺害了殘暴的親戚[大元義而被推戴為渤海國國王](../Page/大元義.md "wikilink")，同時改元[中興](../Page/中興_\(渤海\).md "wikilink")。可惜又在準備重返渤海國上京[龙泉府期間因病過世](../Page/龙泉府.md "wikilink")，在位時間不足半年。死後，渤海國國人諡之為成王\[1\]。

## 家族列表

### 祖父

  - [大钦茂](../Page/大钦茂.md "wikilink")

### 父親

  - [大宏临](../Page/大宏临.md "wikilink")

### 叔叔

  - [大貞斡](../Page/大貞斡.md "wikilink")
  - [大英俊](../Page/大英俊.md "wikilink")
  - [大嵩璘](../Page/大嵩璘.md "wikilink")

## 《[桓檀古记](../Page/桓檀古记.md "wikilink")》

《[桓檀古记](../Page/桓檀古记.md "wikilink")》称大華璵的名字是大華興，被尊为仁宗成皇帝。

## 參考文獻

[Category:渤海國君主](../Category/渤海國君主.md "wikilink")
[Q](../Category/靺鞨人.md "wikilink")
[Category:諡成](../Category/諡成.md "wikilink")
[Category:朝鮮半島追尊皇帝](../Category/朝鮮半島追尊皇帝.md "wikilink")

1.  宋[歐陽修](../Page/歐陽修.md "wikilink")、[宋祁等](../Page/宋祁.md "wikilink")，《[新唐书·列傳第一百四十四·北狄·渤海传](../Page/s:新唐书/卷219.md "wikilink")》:“欽茂死，私諡文王。子宏臨早死，族弟元義立一歲，猜虐，國人殺之。推宏臨子華璵爲王，復還上京，改年中興。死，諡曰成王。”