**Њ**, **њ**（称呼为 ње,
*nje*）是一个[塞尔维亚语和](../Page/塞尔维亚语.md "wikilink")[马其顿语使用的](../Page/马其顿语.md "wikilink")[西里尔字母](../Page/西里尔字母.md "wikilink")，由
[Н](../Page/Н.md "wikilink") 与 [Ь](../Page/Ь.md "wikilink")
的[连字构成](../Page/连字.md "wikilink")。

## 字母的次序

在塞尔维亚语字母中排第17位，在马其顿语字母中排第18位。

## 音值

在塞尔维亚语和马其顿语为 ([硬顎鼻音](../Page/硬顎鼻音.md "wikilink"))，接近但不等于把 НЬ 二字母拼合 。

## 字符编码

| 字符编码                               | [Unicode](../Page/Unicode.md "wikilink") | [ISO 8859-5](../Page/ISO/IEC_8859-5.md "wikilink") | [KOI8-C](../Page/KOI-8.md "wikilink") |
| ---------------------------------- | ---------------------------------------- | -------------------------------------------------- | ------------------------------------- |
| [大写](../Page/大寫字母.md "wikilink") Њ | U+040A                                   | AA                                                 | BA                                    |
| [小写](../Page/小寫字母.md "wikilink") њ | U+045A                                   | FA                                                 | AA                                    |

## 字形代碼

<table>
<thead>
<tr class="header">
<th><p>形式</p></th>
<th><p>字形</p></th>
<th><p>十六進制字形代碼</p></th>
<th><p>代碼</p></th>
<th><p>說明</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>capitale</p></td>
<td><p><strong>Њ</strong></p></td>
<td></td>
<td><p><code>U+040A</code></p></td>
<td><p>西里爾大寫字母 Њ</p></td>
</tr>
<tr class="even">
<td><p>minuscule</p></td>
<td><p><strong>њ</strong></p></td>
<td></td>
<td><p><code>U+045A</code></p></td>
<td><p>西里爾小寫字母 њ</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 計算編碼

## 参看

  - <span lang="ru" style="font-size:120%;">[Н
    н](../Page/Н.md "wikilink")、[Ь
    ь](../Page/Ь.md "wikilink")</span>（西里尔字母）
  - <span lang="en" style="font-size:120%;">[NJ
    Nj](../Page/Nj.md "wikilink")、[Ń ń](../Page/Ń.md "wikilink")、[Ň
    ň](../Page/Ň.md "wikilink")</span>（[拉丁字母](../Page/拉丁字母.md "wikilink")）
  - [西里尔字母列表](../Page/西里尔字母列表.md "wikilink")

## 外部連結

  -
  -
  - [Буква Њ](http://graphemica.com/Њ) на сайте graphemica.com

  - [Буква њ](http://graphemica.com/њ) на сайте graphemica.com

[bs:Nj](../Page/bs:Nj.md "wikilink")
[hr:Nj](../Page/hr:Nj.md "wikilink")
[sh:NJ](../Page/sh:NJ.md "wikilink")

[Category:西里尔字母](../Category/西里尔字母.md "wikilink")