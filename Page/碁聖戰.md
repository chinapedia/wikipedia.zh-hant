**碁聖戰**（[日文](../Page/日文.md "wikilink")：、在[中國大陸有時亦稱為](../Page/中國大陸.md "wikilink")**小棋聖戰**\[1\]，是[日本七大圍棋赛之一](../Page/日本七大圍棋赛.md "wikilink")。創辦於1976年，由[日本棋院](../Page/日本棋院.md "wikilink")、[關西棋院與](../Page/關西棋院.md "wikilink")[新聞圍棋聯盟主辦](../Page/新聞圍棋聯盟.md "wikilink")（共有12家日本報社加盟：[河北新報](../Page/河北新報.md "wikilink")、[新潟日報](../Page/新潟日報.md "wikilink")、[信浓毎日新聞](../Page/信浓毎日新聞.md "wikilink")、[静岡新聞](../Page/静岡新聞.md "wikilink")、[北國新聞](../Page/北國新聞.md "wikilink")、[京都新聞](../Page/京都新聞.md "wikilink")、[中國新聞](../Page/中國新聞.md "wikilink")、[四國新聞](../Page/四國新聞.md "wikilink")、[高知新聞](../Page/高知新聞.md "wikilink")、[熊本日日新聞](../Page/熊本日日新聞.md "wikilink")、[南日本新聞](../Page/南日本新聞.md "wikilink")、[沖繩時報](../Page/沖繩時報.md "wikilink")）。

在中文，「碁聖戰」和「棋圣战」的發音和字面意義完全相同，但在日文中「棋」（Ki）和「」（Go或Igo）的發音、意思是不同的，棋泛指所有棋類，碁專指圍棋，不像中文會有混淆的問題。

在每年碁聖戰最終之獲勝者可獲得**碁聖**之頭銜，連續五年或累積十次獲得碁聖頭銜的棋士可在退休後或年滿六十歲之後使用**名譽碁聖**的頭銜，目前擁有此頭銜的棋士包括：[大竹英雄](../Page/大竹英雄.md "wikilink")、[小林光一和](../Page/小林光一.md "wikilink")[井山裕太](../Page/井山裕太.md "wikilink")。

## 歷史

日本開始使用頭銜戰之前，「棋聖」和「碁聖」的名稱是混雜使用的。

在日本，「碁聖」的名號最早出現於[平安時代的](../Page/平安時代.md "wikilink")[寬蓮](../Page/寬蓮.md "wikilink")\[2\]。[江戶時代的](../Page/江戶時代.md "wikilink")[本因坊道策與](../Page/本因坊道策.md "wikilink")[本因坊丈和亦曾獲得碁聖的稱號](../Page/本因坊丈和.md "wikilink")，其中道策被稱為「前聖」，丈和則稱為「後聖」。而[吳清源亦曾獲得](../Page/吳清源.md "wikilink")「昭和的碁聖」的稱呼。\[3\]\[4\]。

碁聖戰的前身是1970年開始舉辦的「[全日本第一位决定戰](../Page/全日本第一位决定戰.md "wikilink")」，直到1976年改為現在的碁聖戰。

## 賽制

現在是由至少24人組成本戰以[單敗淘汰賽方式產生挑戰者](../Page/單敗淘汰賽.md "wikilink")，再以五戰三勝制向前一年度的頭銜擁有者進行挑戰。

在第四屆以前，最初是由五人組成的本戰以[循環賽的方式產生挑戰者](../Page/循環賽.md "wikilink")，而第一屆比賽則是以前一年的「全日本第一位」[大竹英雄為挑戰的對象](../Page/大竹英雄.md "wikilink")。第五屆以後，本戰才改以[單敗淘汰賽的方式產生挑戰者](../Page/單敗淘汰賽.md "wikilink")，最初在淘汰賽的決賽是以三戰兩勝決定挑戰者，但在第八屆以後改為一戰分勝負。

## 历代碁聖

  - 名譽碁聖

<!-- end list -->

  - [大竹英雄](../Page/大竹英雄.md "wikilink")
  - [小林光一](../Page/小林光一.md "wikilink")
  - [井山裕太](../Page/井山裕太.md "wikilink")

<table>
<tbody>
<tr class="odd">
<td><table>
<thead>
<tr class="header">
<th><p>屆</p></th>
<th><p>年度</p></th>
<th><p>碁聖</p></th>
<th><p>勝敗</p></th>
<th><p>挑戰者</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p>1976</p></td>
<td><p><a href="../Page/大竹英雄.md" title="wikilink">大竹英雄</a></p></td>
<td><p>2-3</p></td>
<td><p><a href="../Page/加藤正夫.md" title="wikilink">加藤正夫八段</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>1977</p></td>
<td><p>加藤正夫</p></td>
<td><p>3-0</p></td>
<td><p><a href="../Page/武宮正樹.md" title="wikilink">武宮正樹八段</a></p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p>1978</p></td>
<td><p>加藤正夫</p></td>
<td><p>1-3</p></td>
<td><p>大竹英雄九段</p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p>1979</p></td>
<td><p>大竹英雄</p></td>
<td><p>0-3</p></td>
<td><p><a href="../Page/趙治勳.md" title="wikilink">趙治勳八段</a></p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p>1980</p></td>
<td><p>趙治勳</p></td>
<td><p>1-3</p></td>
<td><p>大竹英雄九段</p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p>1981</p></td>
<td><p>大竹英雄</p></td>
<td><p>3-1</p></td>
<td><p>加藤正夫九段</p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p>1982</p></td>
<td><p>大竹英雄</p></td>
<td><p>3-2</p></td>
<td><p>趙治勲九段</p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p>1983</p></td>
<td><p>大竹英雄</p></td>
<td><p>3-2</p></td>
<td><p><a href="../Page/淡路修三.md" title="wikilink">淡路修三八段</a></p></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p>1984</p></td>
<td><p>大竹英雄</p></td>
<td><p>3-1</p></td>
<td><p>加藤正夫九段</p></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p>1985</p></td>
<td><p>大竹英雄</p></td>
<td><p>3-1</p></td>
<td><p><a href="../Page/工藤紀夫.md" title="wikilink">工藤紀夫九段</a></p></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p>1986</p></td>
<td><p>大竹英雄</p></td>
<td><p>0-3</p></td>
<td><p>趙治勳九段</p></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td><p>1987</p></td>
<td><p>趙治勳</p></td>
<td><p>1-3</p></td>
<td><p>加藤正夫九段</p></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td><p>1988</p></td>
<td><p>加藤正夫</p></td>
<td><p>0-3</p></td>
<td><p><a href="../Page/小林光一.md" title="wikilink">小林光一九段</a></p></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td><p>1989</p></td>
<td><p>小林光一</p></td>
<td><p>3-1</p></td>
<td><p><a href="../Page/今村俊也.md" title="wikilink">今村俊也八段</a></p></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td><p>1990</p></td>
<td><p>小林光一</p></td>
<td><p>3-0</p></td>
<td><p><a href="../Page/小林覺.md" title="wikilink">小林覺八段</a></p></td>
</tr>
<tr class="even">
<td><p>16</p></td>
<td><p>1991</p></td>
<td><p>小林光一</p></td>
<td><p>3-2</p></td>
<td><p>小林覺九段</p></td>
</tr>
<tr class="odd">
<td><p>17</p></td>
<td><p>1992</p></td>
<td><p>小林光一</p></td>
<td><p>3-1</p></td>
<td><p>小林覺九段</p></td>
</tr>
<tr class="even">
<td><p>18</p></td>
<td><p>1993</p></td>
<td><p>小林光一</p></td>
<td><p>3-0</p></td>
<td><p><a href="../Page/林海峰_(圍棋).md" title="wikilink">林海峰九段</a></p></td>
</tr>
<tr class="odd">
<td><p>19</p></td>
<td><p>1994</p></td>
<td><p>小林光一</p></td>
<td><p>1-3</p></td>
<td><p>林海峰九段</p></td>
</tr>
<tr class="even">
<td><p>20</p></td>
<td><p>1995</p></td>
<td><p>林海峰</p></td>
<td><p>2-3</p></td>
<td><p>小林覺九段</p></td>
</tr>
<tr class="odd">
<td><p>21</p></td>
<td><p>1996</p></td>
<td><p>小林覺</p></td>
<td><p>●●●</p></td>
<td><p><a href="../Page/依田紀基.md" title="wikilink">依田紀基九段</a></p></td>
</tr>
<tr class="even">
<td><p>22</p></td>
<td><p>1997</p></td>
<td><p>依田紀基</p></td>
<td><p>○●○○</p></td>
<td><p><a href="../Page/結城聰.md" title="wikilink">結城聰九段</a></p></td>
</tr>
<tr class="odd">
<td><p>23</p></td>
<td><p>1998</p></td>
<td><p>依田紀基</p></td>
<td><p>○○○</p></td>
<td><p><a href="../Page/苑田勇一.md" title="wikilink">苑田勇一九段</a></p></td>
</tr>
<tr class="even">
<td><p>24</p></td>
<td><p>1999</p></td>
<td><p>依田紀基</p></td>
<td><p>○○●●●</p></td>
<td><p>小林光一十段</p></td>
</tr>
<tr class="odd">
<td><p>25</p></td>
<td><p>2000</p></td>
<td><p>小林光一</p></td>
<td><p>●●○○●</p></td>
<td><p><a href="../Page/山下敬吾.md" title="wikilink">山下敬吾六段</a></p></td>
</tr>
</tbody>
</table></td>
<td><table>
<thead>
<tr class="header">
<th><p>屆</p></th>
<th><p>年度</p></th>
<th><p>碁聖</p></th>
<th><p>勝敗</p></th>
<th><p>挑戰者</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>26</p></td>
<td><p>2001</p></td>
<td><p>山下敬吾</p></td>
<td><p>●○●○●</p></td>
<td><p>小林光一九段</p></td>
</tr>
<tr class="even">
<td><p>27</p></td>
<td><p>2002</p></td>
<td><p>小林光一</p></td>
<td><p>○○●○</p></td>
<td><p>結城聰九段</p></td>
</tr>
<tr class="odd">
<td><p>28</p></td>
<td><p>2003</p></td>
<td><p>小林光一</p></td>
<td><p>●●○○●</p></td>
<td><p>依田紀基名人</p></td>
</tr>
<tr class="even">
<td><p>29</p></td>
<td><p>2004</p></td>
<td><p>依田紀基</p></td>
<td><p>○●○○</p></td>
<td><p><a href="../Page/山田規三生.md" title="wikilink">山田規三生八段</a></p></td>
</tr>
<tr class="odd">
<td><p>30</p></td>
<td><p>2005</p></td>
<td><p>依田紀基</p></td>
<td><p>○○○</p></td>
<td><p>結城聰九段</p></td>
</tr>
<tr class="even">
<td><p>31</p></td>
<td><p>2006</p></td>
<td><p>依田紀基</p></td>
<td><p>●●●</p></td>
<td><p><a href="../Page/張栩.md" title="wikilink">張栩名人</a></p></td>
</tr>
<tr class="odd">
<td><p>32</p></td>
<td><p>2007</p></td>
<td><p>張栩</p></td>
<td><p>○○○</p></td>
<td><p><a href="../Page/橫田茂昭.md" title="wikilink">橫田茂昭九段</a></p></td>
</tr>
<tr class="even">
<td><p>33</p></td>
<td><p>2008</p></td>
<td><p>張栩</p></td>
<td><p>●○○○</p></td>
<td><p>山下敬吾棋聖</p></td>
</tr>
<tr class="odd">
<td><p>34</p></td>
<td><p>2009</p></td>
<td><p>張栩</p></td>
<td><p>○○○</p></td>
<td><p>結城聰九段</p></td>
</tr>
<tr class="even">
<td><p>35</p></td>
<td><p>2010</p></td>
<td><p>張栩</p></td>
<td><p>○●○●●</p></td>
<td><p><a href="../Page/坂井秀至.md" title="wikilink">坂井秀至七段</a></p></td>
</tr>
<tr class="odd">
<td><p>36</p></td>
<td><p>2011</p></td>
<td><p>坂井秀至</p></td>
<td><p>○○●●●</p></td>
<td><p><a href="../Page/羽根直樹.md" title="wikilink">羽根直樹九段</a></p></td>
</tr>
<tr class="even">
<td><p>37</p></td>
<td><p>2012</p></td>
<td><p>羽根直樹</p></td>
<td><p>●●●</p></td>
<td><p><a href="../Page/井山裕太.md" title="wikilink">井山裕太天元</a></p></td>
</tr>
<tr class="odd">
<td><p>38</p></td>
<td><p>2013</p></td>
<td><p>井山裕太</p></td>
<td><p>●●○○○</p></td>
<td><p>河野臨九段</p></td>
</tr>
<tr class="even">
<td><p>39</p></td>
<td><p>2014</p></td>
<td><p>井山裕太</p></td>
<td><p>●○○●○</p></td>
<td><p>河野臨九段</p></td>
</tr>
<tr class="odd">
<td><p>40</p></td>
<td><p>2015</p></td>
<td><p>井山裕太</p></td>
<td><p>○●○○</p></td>
<td><p>山下敬吾九段</p></td>
</tr>
<tr class="even">
<td><p>41</p></td>
<td><p>2016</p></td>
<td><p>井山裕太</p></td>
<td><p>○○○</p></td>
<td><p>村川大介八段</p></td>
</tr>
<tr class="odd">
<td><p>42</p></td>
<td><p>2017</p></td>
<td><p>井山裕太</p></td>
<td><p>○○○</p></td>
<td><p>山下敬吾九段</p></td>
</tr>
<tr class="even">
<td><p>43</p></td>
<td><p>2018</p></td>
<td><p>井山裕太</p></td>
<td><p>●●●</p></td>
<td><p><a href="../Page/許家元.md" title="wikilink">許家元七段</a></p></td>
</tr>
<tr class="odd">
<td><p>備註：</p>
<ol>
<li>紅底者為取得頭銜者</li>
<li>○為衛冕者取勝、●為挑戰者取勝、X為和局</li>
<li>段數為參賽當時之段數</li>
</ol></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table></td>
</tr>
</tbody>
</table>

## 外部連結

## 外部链接

  - [碁聖戦　|　財団法人日本棋院](http://www.nihonkiin.or.jp/match/gosei/index.html)

[J](../Category/日本围棋比赛.md "wikilink")

1.  由于“**碁聖戰**”與另一项围棋赛事**[棋圣战](../Page/棋圣战_\(日本围棋\).md "wikilink")**用[简体中文写出时都可是](../Page/简体中文.md "wikilink")“棋圣战”，因此為了分辨，在中国大陆有时會將“**碁聖戰**”翻译为“**小棋圣战**”，而有时则采取“[宏碁](../Page/宏碁.md "wikilink")”的做法而直接写作“**碁聖戰**”。
2.  『囲碁の文化史』108頁。
3.  中山典之『昭和囲碁風雲録（下）』（岩波書店）
4.  『囲碁の文化史』188頁