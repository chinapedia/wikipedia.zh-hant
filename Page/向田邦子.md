**向田邦子**（），為[日本](../Page/日本.md "wikilink")[電視劇作家](../Page/電視劇作家.md "wikilink")、[隨筆家](../Page/隨筆家.md "wikilink")、[小說家](../Page/小說家.md "wikilink")。出生於[東京都](../Page/東京都.md "wikilink")[世田谷區若林](../Page/世田谷區.md "wikilink")。由於[父親工作關係](../Page/父親.md "wikilink")，自小在日本各地屢次遷居中成長。目黑高女（現[目黑高校](../Page/目黑高校.md "wikilink")）、實踐女子專門學校（現[實踐女子大学](../Page/實踐女子大学.md "wikilink")）國文科畢業。1980年第83回[直木賞受賞](../Page/直木賞.md "wikilink")。

1981年8月22日，因隨筆集取材至[臺灣旅行](../Page/臺灣.md "wikilink")，搭乘[遠東航空103號班機](../Page/遠東航空103號班機.md "wikilink")（[波音737-200](../Page/波音737#737-200.md "wikilink")）自[台北松山機場至](../Page/台北松山機場.md "wikilink")[高雄國際機場時](../Page/高雄國際機場.md "wikilink")，因[空難而喪生](../Page/空難.md "wikilink")，享年51歲，該空難為[三義空難](../Page/三義空難.md "wikilink")。身亡後葬於東京都[府中市的](../Page/府中市.md "wikilink")[多磨靈園](../Page/多磨靈園.md "wikilink")，法號「芳章院釋清邦」。

1983年，為紀念向田對於劇本的貢獻，特別創設「[向田邦子賞](../Page/向田邦子賞.md "wikilink")」獎項，以獎勵其他從事劇本寫作人員。

## 作品

  - 寺內貫太郎一家（）
  - 父親的道歉信（）
  - 女兒的道歉信（）
  - 回憶·撲克牌（）
  - 隔壁女子（）
  - 午夜的玫瑰（）
  - 女人的食指（）
  - 宛如阿修羅（）

## 外部連結

  - [向田邦子文庫](http://www.jissen.ac.jp/library/mukoda/)

  - [三義空難](http://aviation-safety.net/database/record.php?id=19810822-0)

  - [三義空難紀念碑（劉梓潔
    攝）](http://www.flickr.com/photos/49809196@N05/6084836792/in/photostream/)

  -
[Category:日本編劇](../Category/日本編劇.md "wikilink")
[Category:日本小说家](../Category/日本小说家.md "wikilink")
[Category:直木獎得獎者](../Category/直木獎得獎者.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")
[Category:日本女性作家](../Category/日本女性作家.md "wikilink")
[Category:日本空難身亡者](../Category/日本空難身亡者.md "wikilink")