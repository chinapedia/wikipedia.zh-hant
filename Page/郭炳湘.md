**郭炳湘**（，），[香港](../Page/香港.md "wikilink")[企業家](../Page/企業家.md "wikilink")，[帝國集團創辦人兼](../Page/帝國集團.md "wikilink")[董事會主席](../Page/董事長.md "wikilink")，籍貫[廣東](../Page/廣東.md "wikilink")[中山](../Page/中山市.md "wikilink")[石岐](../Page/石岐.md "wikilink")，[新鴻基地產聯合創辦人](../Page/新鴻基地產.md "wikilink")[郭得勝長子](../Page/郭得勝.md "wikilink")，[郭炳江](../Page/郭炳江.md "wikilink")、[郭炳聯之兄長](../Page/郭炳聯.md "wikilink")。郭氏兄弟是香港第二大富豪，排名僅次於[李嘉誠](../Page/李嘉誠.md "wikilink")，身家達240億[美元](../Page/美元.md "wikilink")，折合1873億港元。\[1\]

郭炳湘幼時家族經營雜貨批發等。他本人曾赴[英國升學](../Page/英國.md "wikilink")，並獲得[英國](../Page/英國.md "wikilink")[倫敦大學](../Page/倫敦大學.md "wikilink")[帝國理工學院](../Page/帝國理工學院.md "wikilink")[土木工程系](../Page/土木工程.md "wikilink")[碩士學位及成為](../Page/碩士.md "wikilink")[英國土木工程師學會會員及](../Page/英國土木工程師學會.md "wikilink")[香港工程師學會資深會員](../Page/香港工程師學會.md "wikilink")。

在企業方面，1990年11月15日至1997年5月8日期間，郭炳湘分別出任[九龍巴士有限公司及](../Page/九龍巴士有限公司.md "wikilink")[龍運巴士有限公司](../Page/龍運巴士有限公司.md "wikilink")[董事](../Page/董事.md "wikilink")。1997年9月4日起，郭氏出任[九龍巴士控股有限公司董事](../Page/載通國際.md "wikilink")。郭氏曾任[新鴻基地產發展有限公司](../Page/新鴻基地產發展有限公司.md "wikilink")[董事局主席兼](../Page/董事長.md "wikilink")[行政總裁](../Page/行政總裁.md "wikilink")，曾為[新意網集團有限公司非執行董事](../Page/新意網集團有限公司.md "wikilink")、[威信（香港）停車場管理有限公司董事](../Page/威信（香港）停車場管理有限公司.md "wikilink")、[鴻昌進出口有限公司董事](../Page/鴻昌進出口有限公司.md "wikilink")、[香港地產建設商會董事](../Page/香港地產建設商會.md "wikilink")、[東尖沙咀地產發展商聯會有限公司董事及](../Page/東尖沙咀地產發展商聯會有限公司.md "wikilink")[香港酒店業聯會名譽司庫](../Page/香港酒店業聯會.md "wikilink")。

在社區參與方面，他為[香港公益金歷屆董事委員會主席](../Page/香港公益金.md "wikilink")、[衛奕信勳爵文物信託委員](../Page/衛奕信勳爵文物信託委員.md "wikilink")、[青年總裁協會](../Page/青年總裁協會.md "wikilink")（中國分會）會員及[太平洋地區經濟理事會](../Page/太平洋地區經濟理事會.md "wikilink")——中國香港委員會會員。他亦是[香港中文大學](../Page/香港中文大學.md "wikilink")[工商管理碩士課程顧問委員會委員及](../Page/工商管理.md "wikilink")[香港科技大學顧問委員會榮譽委員](../Page/香港科技大學.md "wikilink")。他亦被選為中國[北京市及](../Page/北京市.md "wikilink")[廣州市榮譽市民](../Page/廣州市.md "wikilink")、[中國](../Page/中國.md "wikilink")[全國政協委員會委員和](../Page/全國政協.md "wikilink")[中華全國工商業聯合會副主席](../Page/中華全國工商業聯合會.md "wikilink")。

## 曾遭綁架

1997年9月29日，郭炳湘被綽號「大富豪」的[香港犯罪集團首腦](../Page/香港.md "wikilink")[張子強綁架](../Page/張子強.md "wikilink")，張子強與一眾黨羽趁郭炳湘自行駕車至[深水灣道豪宅大門外時](../Page/深水灣道.md "wikilink")，強行將他截停及擄走，禁錮於[新界一間村屋內](../Page/新界.md "wikilink")。當時綁匪要求郭炳湘致電回家要求贖金，但被郭炳湘拒絕，張子強等人每日對他施以暴力，迫使郭炳湘在被綁架四日後致電妻子李天穎，要求妻子準備20億[港元贖金](../Page/港元.md "wikilink")。被綁架期間，郭炳湘遭脫去衣服，被困於小木箱中，需長期蜷曲身體，每日只有[叉燒飯及清水充飢](../Page/叉燒飯.md "wikilink")。經過多番斡旋，郭家於10月3日支付6億港元贖金，但張子強收款後卻未立即放人，要待至翌日，郭炳湘才獲釋回家。雖然平安回家，但遭綁架及虐待的恐怖經歷令郭炳湘身心受創。2008年，當他被終止新地董事局主席及行政總裁職務後，接受國際傳媒訪問，親口承認因被綁架而患上[抑鬱症](../Page/抑鬱症.md "wikilink")，經逾一年治療才康復。\[2\]

## 停職事件

2008年2月18日，新鴻基地產突然宣佈，董事局主席兼行政總裁郭炳湘因個人理由，由即日起暫時[休假](../Page/休假.md "wikilink")，郭氏在休假期間，不會履行行政總裁之一切職務及職責，而其職務及職責將由兩名[董事局副主席兼](../Page/副董事長.md "wikilink")[董事總經理](../Page/董事總經理.md "wikilink")[郭炳江及](../Page/郭炳江.md "wikilink")[郭炳聯分擔](../Page/郭炳聯.md "wikilink")；同時他也不會從事或參與任何行政職務，亦不會代表新地或其任何[附屬公司作出任何承諾](../Page/附屬公司.md "wikilink")。不過，新地沒有提及郭炳湘是以甚麼個人理由休假，也沒有披露要休假至何時。

據[星島日報及](../Page/星島日報.md "wikilink")[英文虎報報導](../Page/英文虎報.md "wikilink")，郭炳湘打算讓他一位頗信任的女性朋友[唐錦馨加入公司](../Page/唐錦馨.md "wikilink")，但遭弟弟郭炳江及郭炳聯反對，而公司最大股東郭氏兄弟母親[鄺肖卿為了家族利益](../Page/鄺肖卿.md "wikilink")，決定與兩幼子同一陣線，暫停郭炳湘在公司的職務。而郭炳湘早前私下成立一家名為新鴻基地產國際有限公司發展地產及貿易生意，遭到兩個弟弟反對，擔心新公司與新地混淆。\[3\]，

郭炳湘其後透過公關發表個人名義聲明，表示今次以個人理由即日起暫時休假，未來二至三個月會到訪[北京](../Page/北京.md "wikilink")、[美國及](../Page/美國.md "wikilink")[世界各地作](../Page/世界.md "wikilink")[旅遊](../Page/旅遊.md "wikilink")[考察](../Page/考察.md "wikilink")，完成後，會恢復董事局主席兼行政總裁職務。\[4\]。
2008年5月23日，因申請[禁制令等官司敗訴](../Page/禁制令.md "wikilink")，而失新鴻基集團的主導權，郭炳湘的代表律師[湯家驊循例考慮](../Page/湯家驊.md "wikilink")[上訴](../Page/上訴.md "wikilink")，認為存有復職協議承諾。
\[5\]
\[6\]不過，董事[李兆基建議他要](../Page/李兆基.md "wikilink")「聽阿媽話」。\[7\]最後官司被[高等法院上訴庭駁回](../Page/高等法院.md "wikilink")。\[8\]於2014年1月底，郭炳湘與家族達成共識，三兄弟均分新地股權，另外，郭炳湘亦辭去新鴻基地產非執行董事，專注發展個人事業。\[9\]

## 自立門戶

郭炳湘離任新鴻基地產董事局主席兼行政總裁後，先後以私人身份參股[長江實業](../Page/長江實業.md "wikilink")[亞皆老街住宅項目](../Page/亞皆老街.md "wikilink")[君柏及](../Page/君柏.md "wikilink")[海峽建設投資更偕](../Page/海峽建設投資.md "wikilink")[麗新發展](../Page/麗新發展.md "wikilink")（0488）聯手投得將軍澳住宅地皮\[10\]。

2010年，郭炳湘在家族分帳中獲得200億元，他先後註冊新公司，籌組「[帝國集團](../Page/帝國集團.md "wikilink")」，其中首先在2016年4月註冊成立「郭氏學者協會有限公司」，創辦成員包括郭炳湘本人。6月和7月，郭先後將旗下兩間私人公司Legacy
Group及立匡更名為「建國建築工程」及「帝國集團顧問」。\[11\]

## 涉及许仕仁贪污案

2012年5月4日，[新鴻基地產發公告指](../Page/新鴻基地產.md "wikilink")，非執行董事郭炳湘昨晚被[廉政公署拘留](../Page/廉政公署_\(香港\).md "wikilink")，懷疑涉及觸犯防止賄賂條例，並已獲保釋\[12\]。后据新鸿基地产通告称，郭炳湘涉及香港前政务司司长[许仕仁的贪污案](../Page/许仕仁.md "wikilink")，但因无证据表明其本人参与犯罪，而于2013年10月获释\[13\]。

## 病逝

郭炳湘於2018年8月27日深夜約11時48分在港島[深水灣道大宅因中風暈倒](../Page/深水灣道.md "wikilink")，而被送往[律敦治醫院治療](../Page/律敦治醫院.md "wikilink")\[14\]，並一直昏迷，要以儀器協助呼吸。延至10月20日早上，郭炳湘在[香港港安醫院病逝](../Page/香港港安醫院.md "wikilink")，享年68歲\[15\]。

## 家庭

郭炳湘首任妻子顧芝蓉（Lydia
Kui），她在[滙豐銀行任職高層](../Page/滙豐銀行.md "wikilink")，負責銀團貸款，在[史丹福大學畢業](../Page/史丹福大學.md "wikilink")，父親是商界名人顧新記老闆顧林慶。據稱郭炳湘父親郭得勝非常屬意顧芝蓉，希望她能成為兒媳婦，兩人遂於1982年結婚，但郭炳湘抗拒這樁婚事，結果只維持了約半年便離婚。\[16\]郭炳湘第二任妻子李天穎，出身醫學世家，她在英國讀書時認識郭炳湘，育有兒子郭基俊、郭基浩及女兒郭蕙珊。\[17\]

此外，郭炳湘有位紅顏知己唐錦馨（Ida
Tong），唐比郭年長三歲，兩人早在1960年代認識，兩人因郭得勝反對被迫分開\[18\]。唐錦馨1971年曾與醫生劉國霖結婚。郭炳湘1997年被綁架獲救後，和已離婚的唐錦馨舊情重燃，唐逐漸成為他身邊最倚重的女人。\[19\]02年，郭老太更為此定下「十一條家規」，包括不少更跟唐錦馨有關。當中包括，郭炳湘不能為唐錦馨與現任妻子李天穎離婚，更落閘，禁止唐錦馨和子女，加入新地和參與公司事務及管理、唐錦馨亦不准進入帝苑酒店及兩個辦公室，亦不能嫁郭炳湘及自稱郭太等，並由三兄弟見證簽名作實。\[20\]
2018年8月，唐錦馨知悉郭炳湘暈倒後入院後，想即時到醫院探望，但遭李天穎拒絕進入病房。10月唐錦馨得悉去世消息後傷心欲絕，未能見郭炳湘最後一面\[21\]。

  - 第一任妻：[顧芝蓉](../Page/顧芝蓉.md "wikilink")（1982年－1983年）
  - 第二任妻：[李天穎](../Page/李天穎.md "wikilink")（？－2018年）
      - 兒子：[郭基俊](../Page/郭基俊.md "wikilink")（Geoffrey）（1985年－）
      - 女兒：[郭蕙珊](../Page/郭蕙珊.md "wikilink")（Lesley）
      - 兒子：[郭基浩](../Page/郭基浩.md "wikilink")（Jonathan）

## 參見

  - [郭得勝家族](../Page/郭得勝家族.md "wikilink")

## 參考資料

## 外部連結

  - [新鴻基地產發展有限公司:董事及行政架構](https://web.archive.org/web/20070928095415/http://www.shkp.com.hk/data/investors/reports_detail/1/1/1_28_tc.pdf)
  - [新鴻基地產官方網站](http://www.shkp.com/)
  - [郭炳湘演王子復仇記？](http://the-sun.on.cc/cnt/news/20120330/00407_007.html?pubdate=20120330)

[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:新鴻基地產](../Category/新鴻基地產.md "wikilink")
[Category:全国政协香港委员](../Category/全国政协香港委员.md "wikilink")
[Category:第十届全国政协常务委员](../Category/第十届全国政协常务委员.md "wikilink")
[Category:第十一届全国政协常务委员](../Category/第十一届全国政协常务委员.md "wikilink")
[Category:倫敦帝國學院校友](../Category/倫敦帝國學院校友.md "wikilink")
[Category:香港中山人](../Category/香港中山人.md "wikilink")
[Category:帝國集團](../Category/帝國集團.md "wikilink")
[Category:香港工程師](../Category/香港工程師.md "wikilink")
[Category:郭得勝家族](../Category/郭得勝家族.md "wikilink")
[Category:被绑架的香港人](../Category/被绑架的香港人.md "wikilink")
[B炳](../Category/郭姓.md "wikilink")
[Category:罹患中風逝世者](../Category/罹患中風逝世者.md "wikilink")

1.  [李嘉誠位列大中華首席富豪](https://web.archive.org/web/20070519002423/http://hk.news.yahoo.com/070118/12/2072d.html)

2.

3.  [拒棄紅顏知己 郭老太心死
    郭炳湘無得分身家](http://www.eastweek.com.hk/index.php?aid=8769)
    東周刊

4.  [新地巨變 郭炳湘停職](http://hk.news.yahoo.com/fc/news_fc_hkkwokbro.html)

5.  [郭炳湘擬就申請禁制令上訴](https://web.archive.org/web/20080525064729/http://hk.news.yahoo.com/080523/12/2uit8.html)

6.  [董事局有權決定](https://web.archive.org/web/20080526112507/http://hk.news.yahoo.com/080523/12/2ujl6.html)

7.  [李兆基：建議郭炳湘要「聽阿媽話」。](https://web.archive.org/web/20080526222043/http://hk.news.yahoo.com/080526/74/2ulfs.html)

8.  [郭炳湘禁制令上訴被駁回](https://web.archive.org/web/20080527080046/http://hk.news.yahoo.com/080526/12/2umqd.html)

9.  [三兄弟權益均等
    郭炳湘冀發展個人事業](http://www2.hkej.com/instantnews/announcement/article/346965/%E6%96%B0%E5%9C%B0%E9%83%AD%E6%B0%8F%E4%B8%89%E5%85%84%E5%BC%9F%E5%B9%B3%E5%88%86%E5%AE%B6%E7%94%A2+%E9%83%AD%E7%82%B3%E6%B9%98%E8%BE%AD%E9%9D%9E%E5%9F%B7%E8%91%A3)

10.

11.

12. [郭炳湘深夜已獲廉署批准保釋](http://www.rthk.org.hk/rthk/news/expressnews/20120504/news_20120504_55_837972.htm)
    香港電台 2012年5月4日

13. [港媒称许仕仁贪污案出现新进展
    郭炳湘或将被释放](http://www.chinanews.com/ga/2013/10-11/5363639.shtml)

14.

15.

16.

17.

18.
19.

20.

21.