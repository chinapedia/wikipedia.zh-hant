**塔瓦雷·拉蒙·巴斯克斯·罗萨斯**（；），[乌拉圭政治家](../Page/乌拉圭.md "wikilink")，现任[烏拉圭總統](../Page/烏拉圭總統.md "wikilink")。

## 總統

曾是实习医师，1980年代加入[乌拉圭社会党](../Page/乌拉圭社会党.md "wikilink")。

1994年及1999年參選总统但失敗。2005年3月1日正式就任乌拉圭总统，是该国历史上第一位[左翼总统](../Page/左翼.md "wikilink")。任內他採取溫和的經濟及社會改革，並與[美國保持良好關係](../Page/美國.md "wikilink")。

2014年11月30日，以56.62%第二度當選總統，於2015年3月1日展開第二個任期。

## 外部連結

  - [烏拉圭總統網站](http://www.presidencia.gub.uy/)

|-    |-    |-

[Category:蒙特維多人](../Category/蒙特維多人.md "wikilink")
[Category:烏拉圭總統](../Category/烏拉圭總統.md "wikilink")
[Category:現任國家領導人](../Category/現任國家領導人.md "wikilink")