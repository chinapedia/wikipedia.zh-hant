****是历史上第三十四次航天飞机任务，也是[亚特兰蒂斯号航天飞机的第六次太空飞行](../Page/亞特蘭提斯號太空梭.md "wikilink")。任务内容为[美国国防部机密](../Page/美国国防部.md "wikilink")，据信发射载荷为[迷雾卫星](../Page/迷雾卫星.md "wikilink")。

## 任务成员

  - **[约翰·克雷顿](../Page/约翰·克雷顿.md "wikilink")**（，曾执行、以及任务），指令长
  - **[约翰·卡斯帕](../Page/约翰·卡斯帕.md "wikilink")**（，曾执行、、以及任务），飞行员
  - **[理查德·穆莱恩](../Page/理查德·穆莱恩.md "wikilink")**（，曾执行、以及任务），任务专家
  - **[大卫·希尔默斯](../Page/大卫·希尔默斯.md "wikilink")**（，曾执行、、以及任务），任务专家
  - **[皮埃尔·苏厄特](../Page/皮埃尔·苏厄特.md "wikilink")**（，曾执行、以及任务），任务专家

[Category:1990年美国](../Category/1990年美国.md "wikilink")
[Category:1990年科学](../Category/1990年科学.md "wikilink")
[Category:亚特兰蒂斯号航天飞机任务](../Category/亚特兰蒂斯号航天飞机任务.md "wikilink")
[Category:1990年2月](../Category/1990年2月.md "wikilink")
[Category:1990年3月](../Category/1990年3月.md "wikilink")