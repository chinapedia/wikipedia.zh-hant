**日本電信電話公社**（Nippon Telegraph and Telephone Public
Corporation；NTTPC），簡稱為「電電公社」（一般也被表記為「」），為曾存在於[日本的](../Page/日本.md "wikilink")[特殊法人](../Page/特殊法人.md "wikilink")，主要業務是[電報](../Page/電報.md "wikilink")（日本稱為「[電信](../Page/電信.md "wikilink")」）與[電話服務](../Page/電話.md "wikilink")。該組織是現在的[日本電信電話株式會社](../Page/日本電信電話株式會社.md "wikilink")（NTT）之前身，為一[公營事業機構](../Page/公營事業.md "wikilink")。
[Nippon_Telegraph_and_Telephone_Public_Corporation_Manhole_Cover.jpg](https://zh.wikipedia.org/wiki/File:Nippon_Telegraph_and_Telephone_Public_Corporation_Manhole_Cover.jpg "fig:Nippon_Telegraph_and_Telephone_Public_Corporation_Manhole_Cover.jpg")\]\]

## 概要

[Establishment_of_Nippon_Telegraph_and_Telephone_Public_Corporation.JPG](https://zh.wikipedia.org/wiki/File:Establishment_of_Nippon_Telegraph_and_Telephone_Public_Corporation.JPG "fig:Establishment_of_Nippon_Telegraph_and_Telephone_Public_Corporation.JPG")
電電公社是過去日本三公社（[日本國有鐵道](../Page/日本國有鐵道.md "wikilink")、[日本專賣公社](../Page/日本專賣公社.md "wikilink")、電電公社）之一。以前的電信產業的特別會計中的資產與負債之差額，由[日本政府全額出資](../Page/日本政府.md "wikilink")。

### 簡史

《日本電信電話公社法》自1952年8月1日通過，同日成立電電公社，因此電電公社是繼承[戰前](../Page/第二次世界大戰.md "wikilink")[電氣通信省的公眾電氣通信現業部門之業務](../Page/電氣通信省.md "wikilink")。電電公社則在1985年4月1日因《日本電信電話株式會社法》而解散，業務則由NTT繼承。

根據此修正法，於1985年的民營化，以及「後發通信事業者」（也就是所謂的[新電電](../Page/新電電.md "wikilink")）的-{新制}-法規加入前，日本國內的通訊（[電報](../Page/電報.md "wikilink")、專用線路等）、通話（電話）等業務都是由電電公社所獨占。

### 公共電信事業

電電公社的公共電信事業是根據1953年8月1日施行的《公眾電氣通信法》而來，而電電公社及[國際電信電話株式會社](../Page/國際電信電話株式會社.md "wikilink")（Kokusai
Denshin Denwa；KDD，現在的[KDDI之前身](../Page/KDDI.md "wikilink")）之營業項目由此法中定義。

公共電信服務則是以「使用電信設備為媒介與他人通訊，以及供應其他電信設備可以與他人通訊的使用」作為定義，而在電報的種類、電話的種類等之費率也在條文中明定之。

### 財務及會計

電電公社每年的營業年度之預算是由[郵政大臣提出](../Page/郵政大臣.md "wikilink")，由[內閣決定](../Page/日本內閣.md "wikilink")，並經由[國會的決議而成立](../Page/日本國會.md "wikilink")。資金的籌措則是用「電信電話債券」的方式公開發行，甚至是外債的使用則是由政府來負擔貸款及接受債券的償付，以及國庫剩餘款的使用來認列債務保證。

### 簡稱與標語

電電公社的英文簡稱為NTT（或NTTPC），這是在NTT民營化之前就已有的稱呼。此外，NTT的企業標誌在民營化時仍舊使用著，並且在民營化前的1980年初已在[電視廣告中使用](../Page/電視廣告.md "wikilink")「NTT」一詞；同時已在廣告中使用新標語「迎向更能夠理解彼此的未來」（），取代原本的標語「電話的彼端是什麼樣的表情？」（）。

### 標誌

電電公社的「TTS」標誌（公社章）是取「Telegraph」（電報）與「Telephone」（電話）二字的第一個[字母](../Page/字母.md "wikilink")「T」作成一個綠色的圓，並取「Service」（服務）的第一個字母「S」作成正中央的白色「S」。1986年，「TTS」標誌遭廢止。

## 關聯條目

  - [琉球電信電話公社](../Page/琉球電信電話公社.md "wikilink")
  - [NTT](../Page/日本電信電話.md "wikilink")
  - [NTT DOCOMO](../Page/NTT_DOCOMO.md "wikilink")

[Category:已不存在的日本公營事業](../Category/已不存在的日本公營事業.md "wikilink")
[Category:日本通信公司](../Category/日本通信公司.md "wikilink")
[Category:1952年成立的公司](../Category/1952年成立的公司.md "wikilink")
[Category:1985年廢除](../Category/1985年廢除.md "wikilink")