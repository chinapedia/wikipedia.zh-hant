**Cookies**是[香港的一隊](../Page/香港.md "wikilink")[女子音樂組合](../Page/女子音樂組合.md "wikilink")，由[鄧麗欣](../Page/鄧麗欣.md "wikilink")、[傅穎](../Page/傅穎.md "wikilink")、[楊愛瑾和](../Page/楊愛瑾.md "wikilink")[吳雨霏](../Page/吳雨霏.md "wikilink")4名成員組成，其[經理人為](../Page/經理人.md "wikilink")[黃柏高](../Page/黃柏高.md "wikilink")，由[金牌娛樂事業有限公司出版](../Page/金牌娛樂事業有限公司.md "wikilink")[唱片](../Page/唱片.md "wikilink")。Cookies於2002年成立，最初由9人組成，期間被譽為香港的[早安少女組](../Page/早安少女組.md "wikilink")。2003年將規模縮小至四人組合，坊間稱為**Mini
Cookies**，重組後的四人備受各界好評。而被淘汰的5名成員則被[香港傳媒戲稱為](../Page/香港傳媒.md "wikilink")「餅碎」。

2005年3月，Cookies成員宣佈各自發展，但強調絕不會拆夥。個人發展後的四位成員均發展不俗，[鄧麗欣](../Page/鄧麗欣.md "wikilink")、[吳雨霏及](../Page/吳雨霏.md "wikilink")[傅穎繼續往音樂方向發展](../Page/傅穎.md "wikilink")，而[楊愛瑾則轉型為電影演員](../Page/楊愛瑾.md "wikilink")。個人發展後的她們已經較少合作，但是同屬一間公司的她們還是會偶爾合體演出。2009年，成員[楊愛瑾與](../Page/楊愛瑾.md "wikilink")[傅穎先後轉投其他經理人公司](../Page/傅穎.md "wikilink")，隨著成員的離開，傳媒都指出Cookies已經名存實亡。2011年[吳雨霏及](../Page/吳雨霏.md "wikilink")[鄧麗欣亦轉投其他經理人公司](../Page/鄧麗欣.md "wikilink")，現時Cookies已經沒有任何成員仍然屬於[金牌大風](../Page/金牌大風.md "wikilink")，並且四位成員都屬於不同的經理人公司。

2010年8月11日，4名前成員[陳素瑩](../Page/陳素瑩.md "wikilink")、[何綺玲](../Page/何綺玲.md "wikilink")、[區文詩和](../Page/區文詩.md "wikilink")[馬思恆於](../Page/馬思恆.md "wikilink")[九龍灣展貿中心舉行](../Page/九龍灣展貿中心.md "wikilink")[演唱會](../Page/演唱會.md "wikilink")，四餅中的[楊愛瑾和](../Page/楊愛瑾.md "wikilink")[吳雨霏與已經退出](../Page/吳雨霏.md "wikilink")[娛樂圈的餅碎](../Page/娛樂圈.md "wikilink")[蒲茜兒均有到場支持](../Page/蒲茜兒.md "wikilink")，[傅穎及](../Page/傅穎.md "wikilink")[鄧麗欣亦均有為到前隊友打氣](../Page/鄧麗欣.md "wikilink")；前者則送上了花牌，後者則在[微博為她們打氣](../Page/微博.md "wikilink")。

2012年8月3日，[楊愛瑾](../Page/楊愛瑾.md "wikilink")、[吳雨霏](../Page/吳雨霏.md "wikilink")、[鄧麗欣以及](../Page/鄧麗欣.md "wikilink")4名前成員[何綺玲](../Page/何綺玲.md "wikilink")、[陳素瑩](../Page/陳素瑩.md "wikilink")、[馬思恆及](../Page/馬思恆.md "wikilink")[區文詩為同期出道的組合](../Page/區文詩.md "wikilink")[Shine於](../Page/Shine.md "wikilink")[九龍灣展貿中心舉行的SHINE](../Page/九龍灣展貿中心.md "wikilink")
AGAIN
2012演唱會擔任嘉賓，唱出心急人上。[鄧麗欣表示](../Page/鄧麗欣.md "wikilink")[傅穎正在台灣參與電視劇集演出而無法出席](../Page/傅穎.md "wikilink")，已經退出[娛樂圈的](../Page/娛樂圈.md "wikilink")[蒲茜兒因為身在加拿大安胎而未能夠出席](../Page/蒲茜兒.md "wikilink")。

## 發展歷程

多年前，日本女子組合[Morning娘大熱](../Page/Morning娘.md "wikilink")，[EMI香港區總裁](../Page/EMI.md "wikilink")[陳志光有意在](../Page/陳志光.md "wikilink")[香港成立一隊類似的多人組合](../Page/香港.md "wikilink")，因此在2000年開始為這隊組合物色成員。

2001年，被選中的女生開始接受訓練。最終，留下了九位16到21歲的成員，成為Cookies。

2002年6月，Cookies的首支廣告與出道單曲《[心急人上](../Page/心急人上.md "wikilink")》播出，九女正式出道。

2003年，[EMI人事變動](../Page/EMI.md "wikilink")，一手推出Cookies的陳志光離開了EMI，最後，Cookies的經紀合約改簽給了[金牌娛樂的](../Page/金牌娛樂.md "wikilink")[黃柏高](../Page/黃柏高.md "wikilink")。黃柏高接手後決定對Cookies進行成員裁減。2003年4月27日，正式對外宣布Cookies成員變動的最終結果，只留下綜合實力最強的[鄧麗欣](../Page/鄧麗欣.md "wikilink")、[楊愛瑾](../Page/楊愛瑾.md "wikilink")、[傅穎以及](../Page/傅穎.md "wikilink")[吳雨霏四位成員](../Page/吳雨霏.md "wikilink")，並於同日推出四女的首支單曲《[貪你可愛](../Page/貪你可愛.md "wikilink")》。同年7月，[EMI與](../Page/EMI.md "wikilink")[Neway卡拉OK共同舉辦了招募Mini](../Page/Neway.md "wikilink")
Cookies新成員的活動，但最終還是沒有加入新成員。改組後的**Mini
Cookies**大受歡迎，推出了多張唱片，并與2004年8月30日舉辦首場演唱會。

2005年3月28日，Mini
Cookies在新碟簽名會上宣佈在兩年內會暫時獨立發展，但強調并不是解散，并重申之後會再度一起。個人發展後，[鄧麗欣](../Page/鄧麗欣.md "wikilink")、[吳雨霏及](../Page/吳雨霏.md "wikilink")[傅穎繼續往音樂方向發展](../Page/傅穎.md "wikilink")，而[楊愛瑾則轉型為電影演員](../Page/楊愛瑾.md "wikilink")。

2005年，[鄧麗欣發行個人專輯](../Page/鄧麗欣.md "wikilink")，成為第一位發行個人專輯的成員。2006年，[吳雨霏發行個人專輯](../Page/吳雨霏.md "wikilink")。2007年，[傅穎發行個人專輯](../Page/傅穎.md "wikilink")。2008年，經理人[黃柏高有意替](../Page/黃柏高.md "wikilink")[楊愛瑾推出個人專輯](../Page/楊愛瑾.md "wikilink")，可惜最終因為黃柏高的調職而不成事，因此楊愛瑾是Cookies成員當中唯一一位沒有在[金牌娛樂推出個人專輯的成員](../Page/金牌娛樂.md "wikilink")，但是她在[電影上發展卻有著不錯的成績](../Page/電影.md "wikilink")。

2008年8月，[金牌娛樂以過億元成功收購](../Page/金牌娛樂.md "wikilink")[EMI大中華區業務](../Page/EMI.md "wikilink")，並正式改名為[金牌大風](../Page/金牌大風.md "wikilink")，Cookies所屬公司亦隨之成為[金牌大風](../Page/金牌大風.md "wikilink")。

2009年，成員[楊愛瑾](../Page/楊愛瑾.md "wikilink")、[傅穎先後約滿](../Page/傅穎.md "wikilink")[金牌大風](../Page/金牌大風.md "wikilink")，分別簽約[種星堂及](../Page/種星堂.md "wikilink")[星皓娛樂](../Page/星皓娛樂.md "wikilink")，結束與金牌大風8年的賓主關係；而吳雨霏與鄧麗欣則選擇與金牌大風續約。

2011年，[吳雨霏和](../Page/吳雨霏.md "wikilink")[鄧麗欣也先後約滿金牌大風](../Page/鄧麗欣.md "wikilink")，分別簽約[新藝寶唱片及](../Page/新藝寶唱片.md "wikilink")[北京海潤](../Page/北京海潤.md "wikilink")，結束與金牌大風10年的賓主關係，亦標誌着所有Mini
Cookies成員均已離開金牌大風。

2012年，Cookies成立十週年。8月3日，[鄧麗欣](../Page/鄧麗欣.md "wikilink")、[楊愛瑾](../Page/楊愛瑾.md "wikilink")、[吳雨霏以及前成員](../Page/吳雨霏.md "wikilink")[區文詩](../Page/區文詩.md "wikilink")、[何綺玲](../Page/何綺玲.md "wikilink")、[陳素瑩和](../Page/陳素瑩.md "wikilink")[馬思恆為](../Page/馬思恆.md "wikilink")[Shine於](../Page/Shine.md "wikilink")[九龍灣國際展貿中心舉行的](../Page/九龍灣國際展貿中心.md "wikilink")[SHINE
AGAIN
2012演唱會任嘉賓](../Page/SHINE_AGAIN_2012演唱會.md "wikilink")，唱出《[心急人上](../Page/心急人上.md "wikilink")》。[鄧麗欣表示](../Page/鄧麗欣.md "wikilink")，四餅之一的[傅穎因正在](../Page/傅穎.md "wikilink")[台灣拍劇而未能出席](../Page/台灣.md "wikilink")，而已經退出[娛樂圈的](../Page/娛樂圈.md "wikilink")[蒲茜兒則因為身在](../Page/蒲茜兒.md "wikilink")[加拿大安胎而未能出席](../Page/加拿大.md "wikilink")。這是Cookies原9位成員在解散之後再重聚的活動中最人齊的一次（7人）。

## 成員

  - Mini Cookies

<!-- end list -->

  - [鄧麗欣](../Page/鄧麗欣.md "wikilink")（Stephy），出道前是兼職模特兒，後被模特兒公司[Talent
    Bang推薦加入](../Page/Talent_Bang.md "wikilink")[EMI而被選入Cookies](../Page/EMI.md "wikilink")，亦是九人時期的隊長。同時，Stephy也是第一個作獨立發展推出個人專輯的成員。獨立發展初期是以唱歌爲主，推出多張個人專輯。2007年年尾舉行首次個人演唱會，是Cookies成員中第一位舉辦個人演唱會的一人。唱歌的同時亦參與多部電影的拍攝。2011年約滿[金牌大風](../Page/金牌大風.md "wikilink")，並簽約[北京](../Page/北京.md "wikilink")[海潤影視作爲經理人公司](../Page/海潤影視.md "wikilink")，正式開始進軍內地發展，主要拍攝內地電視劇以及電影，暫別樂壇，並於2012年推出兩首國語單曲。2013年回歸香港樂壇，推出粵語新歌。與男友[方力申自](../Page/方力申.md "wikilink")[獨家試愛起相戀](../Page/獨家試愛.md "wikilink")10年，但已分手。

<!-- end list -->

  - [吳雨霏](../Page/吳雨霏.md "wikilink")（Kary），出道前因參加《903 Clean and
    Clear手牽手大行動》被EMI發掘而被選加入Cookies，是Cookies中第二位推出個人專輯的成員。2004年，她曾擔任搖滾樂隊[Ping
    Pung的主唱](../Page/Ping_Pung.md "wikilink")，是Cookies成員中唯一同屬兩團的成員，但一切工作以Cookies爲先。樂隊推出一張專輯後便解散，隨後Cookies眾人也開始獨立發展。獨立發展後，她一直以唱歌事業爲主，期間推出多張個人專輯。2011年約滿金牌大風，同年簽約[環球唱片旗下的](../Page/環球唱片.md "wikilink")[新藝寶唱片](../Page/新藝寶.md "wikilink")，並迅速推出多張專輯及單曲，成爲新藝寶力捧歌手。Kary曾舉行多次個人演唱會，是Cookies中舉辦過最多演唱會的成員，亦是Cookies之中第一位及唯一一位在[紅磡香港體育館舉辦過個人演唱會的成員](../Page/紅磡香港體育館.md "wikilink")。於2015年承認與[洪立熙已婚](../Page/洪立熙.md "wikilink")。

<!-- end list -->

  - [傅穎](../Page/傅穎.md "wikilink")（Theresa），出道前是兼職模特兒，與鄧麗欣同樣被選入Cookies，是Cookies中第三位推出個人專輯的成員。獨立發展初期以拍廣告及電影爲主，2007年再戰樂壇，先後發佈4張個人專輯。2009年與金牌大風解約，2010年簽約香港[星皓娛樂爲經理人公司](../Page/星皓娛樂.md "wikilink")，並同年推出新專輯。2011年起正式進軍內地發展，拍攝電影及電視劇為主，暫別樂壇。除熒幕上的作品外，自2006年起，每年[香港書展她都會推出新書](../Page/香港書展.md "wikilink")，現已推出八本個人著作。2016年6月回歸樂壇推出新歌《最好是你》，首次參與填詞。隔月再推出國語單曲《最近好嗎》。

<!-- end list -->

  - [楊愛瑾](../Page/楊愛瑾.md "wikilink")（Miki），出道前是兼職模特兒，後被模特兒公司[Starz
    People推薦加入EMI而被選入Cookies](../Page/Starz_People.md "wikilink")。獨立發展之後，全面轉型爲電影演員，參與多部電影的拍攝。2009年約滿金牌大風，同年重新簽約模特兒公司[Starz
    People](../Page/Starz_People.md "wikilink")，以模特兒工作及商業演出爲主，電影作品大幅減少。2012年簽約[TVB部頭合約](../Page/TVB.md "wikilink")，拍攝電視劇及擔任節目主持。2012年年尾簽約電影公司[悅目映畫](../Page/悅目映畫.md "wikilink")，再次在影壇上發展。2013年推出新書《Mikitopia》，以及個人發展以來的第一首單曲《綿羊》。2016年與男友郭永淳結婚，2017年誕下兒子Luken。

## 舊成員

  - [區文詩](../Page/區文詩.md "wikilink")（*Angela*）（已於2003年4月退出，2003年起加入了[香港電台第二台成為電台DJ](../Page/香港電台.md "wikilink")。2010年簽約唱片公司[MusicNEXT成為旗下歌手](../Page/MusicNEXT.md "wikilink")，先後發表兩張個人專輯《Angelicious》、同名專輯《Angela區文詩》，以及單曲《盲婚論嫁》，成為Cookies第4位發表個人專輯的成員。2013年與唱片公司解約，2015年簽約經紀公司[天立娛樂](../Page/天立娛樂.md "wikilink")。）
  - [何綺玲](../Page/何綺玲.md "wikilink")（*Elaine*）（已於2003年4月退出，出生於[澳門](../Page/澳門.md "wikilink")，亦是唯一來自[澳門的成員](../Page/澳門.md "wikilink")。2014年3月19日於澳門舉行婚禮，於2015年初誕下女兒）
  - [陳素瑩](../Page/陳素瑩.md "wikilink")（*Gloria*）（已於2003年4月退出，在[英國修畢](../Page/英國.md "wikilink")[金融和](../Page/金融.md "wikilink")[會計](../Page/會計.md "wikilink")[課程及於](../Page/課程.md "wikilink")[曼徹斯特商學院](../Page/曼徹斯特商學院.md "wikilink")(Manchester
    Business School)
    修畢[碩士學位](../Page/碩士學位.md "wikilink")，現於香港[四大會計師樓之一氏](../Page/四大國際會計師事務所.md "wikilink")[德勤](../Page/德勤.md "wikilink")
    (Deloitte) 任職高級審計員）
  - [馬思恆](../Page/馬思恆.md "wikilink")（*Helena*）（已於2003年4月退出，並於同年移民[美國](../Page/美國.md "wikilink")，現於美國修讀[藝術治療課程](../Page/藝術治療.md "wikilink")）
  - [蒲茜兒](../Page/蒲茜兒.md "wikilink")（*Serena*）（已於2003年4月退出，現[已婚及已育一女](../Page/已婚.md "wikilink")，現移民[加拿大](../Page/加拿大.md "wikilink")）

## 音樂作品

### 專輯

#### Cookies時期

  - 2002年8月9日：[Happy
    Birthday](../Page/Happy_Birthday_\(Cookies專輯\).md "wikilink")
  - 2002年12月23日：[Merry
    X'mas](../Page/Merry_X'mas_\(Cookies專輯\).md "wikilink")

#### Mini Cookies時期

  - 2003年8月13日：[All The
    Best](../Page/All_The_Best_\(Cookies專輯\).md "wikilink")
  - 2004年1月21日：[4 Play](../Page/4_Play.md "wikilink")
  - 2004年12月18日：[4 In
    Love](../Page/4_In_Love_\(Cookies專輯\).md "wikilink")
  - 2005年2月16日：4 In Love (2nd Edition)
  - 2011年8月19日：[All The
    Best](../Page/All_The_Best_\(Cookies專輯\).md "wikilink")（[華納](../Page/華納.md "wikilink")+[EMI金唱片複刻王系列](../Page/EMI.md "wikilink")）

### 卡啦OK專輯

#### Cookies時期

  - 2002年10月23日：Holidays

#### Mini Cookies時期

  - 2003年11月17日：Channel Cookies

### 迷你演唱會專輯

#### Mini Cookies時期

  - 2004年12月24日：11團火音樂會

### 動畫歌曲

  - 2003年：信心爆棚（動畫《[音樂小彗星](../Page/音樂小彗星.md "wikilink")》粵語片頭曲，內地電台南方生活廣播「流行南方粵語兒歌大匯」冠軍歌曲）

## 派台歌曲成績

| **派台歌曲上榜成績**                                                     |
| ---------------------------------------------------------------- |
| 大碟                                                               |
| **2002年**                                                        |
| [Happy Birthday](../Page/Happy_Birthday_\(專輯\).md "wikilink")    |
| Happy Birthday                                                   |
| Happy Birthday                                                   |
| Happy Birthday                                                   |
| [Merry X'mas](../Page/Merry_X'mas_\(Cookies專輯\).md "wikilink")   |
| Merry X'mas                                                      |
| Merry X'mas                                                      |
| **2003年**                                                        |
| Merry X'mas                                                      |
| [All The Best](../Page/All_The_Best_\(Cookies專輯\).md "wikilink") |
| All The Best                                                     |
| All The Best                                                     |
| [4play](../Page/4_Play.md "wikilink")                            |
| **2004年**                                                        |
| 4play                                                            |
| 4play                                                            |
| [4 In Love](../Page/4_in_Love_\(專輯\).md "wikilink")              |
| 4 In Love                                                        |
| 4 In Love                                                        |

| **各台冠軍歌總數** |
| ----------- |
| 903         |
| **0**       |

## 獎項

  - 2002年：

<!-- end list -->

  - 2002年度勁歌金曲第三季季選 - 新星試打三屆台主《心急人上》
  - [新城勁爆頒獎禮2002](../Page/新城勁爆頒獎禮2002.md "wikilink") - 勁爆新登場組合 銀獎
  - [叱吒樂壇頒獎禮2002](../Page/2002年度叱吒樂壇流行榜頒獎典禮得獎名單.md "wikilink") -
    叱-{吒}-樂壇生力軍組合 銅獎
  - [十大勁歌金曲頒獎典禮](../Page/十大勁歌金曲頒獎典禮.md "wikilink") - 最受歡迎新組合 銅獎
  - [第二十五屆十大中文金曲頒獎典禮](../Page/第二十五屆十大中文金曲得獎名單.md "wikilink") - 最有前途新人組合
    銀獎
  - [IFPI香港唱片銷量大獎](../Page/IFPI.md "wikilink")2002 - 全年最高銷量新組合
  - [PM第一屆夏日人氣歌手頒獎禮2002](../Page/PM第一屆夏日人氣歌手頒獎禮2002.md "wikilink") -
    人氣女子組合

<!-- end list -->

  - 2003年：

<!-- end list -->

  - [兒歌金曲開心嘉年華](../Page/兒歌金曲開心嘉年華.md "wikilink") - 銅獎《Forever Friends》
  - [勁歌金曲第三季季選](../Page/勁歌金曲第三季季選.md "wikilink") - 手機網絡人氣女歌手
  - [2003年度新城勁爆頒獎禮](../Page/2003年度新城勁爆頒獎禮得獎名單.md "wikilink") - 勁爆人氣組合
  - [十大勁歌金曲頒獎典禮](../Page/2003年度十大勁歌金曲得獎名單.md "wikilink") - 最受歡迎廣告歌曲大獎銀獎
    《貪你可愛》
  - [RoadShow MV直選](../Page/RoadShow至尊音樂頒獎禮.md "wikilink") - 《一擊即中》
  - [PM第二屆夏日人氣歌手頒獎禮2003](../Page/PM第二屆夏日人氣歌手頒獎禮2003.md "wikilink") -
    人氣女子組合

<!-- end list -->

  - 2004年：

<!-- end list -->

  - [新城勁爆頒獎禮2004](../Page/2004年度新城勁爆頒獎禮得獎名單.md "wikilink") - 勁爆組合
  - [新城勁爆頒獎禮2004](../Page/2004年度新城勁爆頒獎禮得獎名單.md "wikilink") - 勁爆跳舞歌曲
    《派對動物》
  - 四台聯頒音樂大獎2004 - 《派對動物》
  - [PM第三屆夏日人氣歌手頒獎禮2004](../Page/PM第三屆夏日人氣歌手頒獎禮2004.md "wikilink") -
    人氣女子組合

## 電影

  - [九個女仔一隻鬼](../Page/九個女仔一隻鬼.md "wikilink")
  - [百分百感覺2003](../Page/百分百感覺2003.md "wikilink")
  - [龍咁威](../Page/龍咁威.md "wikilink")（Stephy參演）
  - [失驚無神](../Page/失驚無神.md "wikilink")（Stephy、Theresa參演）
  - [三更2之餃子](../Page/三更2之餃子.md "wikilink")（Miki參演）
  - [甜絲絲](../Page/甜絲絲.md "wikilink")（Stephy、Kary參演）
  - [B420](../Page/B420.md "wikilink")（Miki參演）
  - [龍咁威2之皇母娘娘呢](../Page/龍咁威2之皇母娘娘呢.md "wikilink")（Miki參演 Theresa客串）
  - [雀聖](../Page/雀聖.md "wikilink")（Theresa參演）
  - [春田花花同學會](../Page/春田花花同學會.md "wikilink")（Miki、Theresa、Kary客串）
  - [心想事成](../Page/心想事成.md "wikilink")（Miki參演）
  - [獨家試愛](../Page/獨家試愛.md "wikilink")（Stephy、Theresa參演）
  - [至尊無賴](../Page/至尊無賴.md "wikilink")（Theresa、Miki參演）
  - [戀愛初歌](../Page/戀愛初歌.md "wikilink")
  - [黑拳](../Page/黑拳.md "wikilink")（Miki、Theresa參演）
  - [愛上屍新娘](../Page/愛上屍新娘.md "wikilink")（Miki參演）
  - [阿飛](../Page/阿飛.md "wikilink")（Miki參演）
  - [三分鐘先生](../Page/三分鐘先生.md "wikilink")（Theresa參演）
  - [濠情歲月](../Page/濠情歲月.md "wikilink")（Stephy參演）
  - [十分愛](../Page/十分愛.md "wikilink")（Stephy、Miki參演）
  - [破事兒](../Page/破事兒.md "wikilink")（Stephy參演）
  - [校墓處](../Page/校墓處.md "wikilink")（Theresa參演）
  - [塚愛](../Page/塚愛.md "wikilink")（Stephy參演）
  - [我的最愛](../Page/我的最愛.md "wikilink")（Stephy、Miki參演）
  - [親愛的](../Page/親愛的.md "wikilink")（Miki參演）
  - [內衣少女](../Page/內衣少女.md "wikilink")（Stephy參演）
  - [絕代雙嬌](../Page/絕代雙嬌.md "wikilink")（Kary、Stephy參演）
  - [保持愛你](../Page/保持愛你.md "wikilink")（Miki、Stephy參演）
  - [家有囍事2009](../Page/家有囍事2009.md "wikilink")（Theresa客串）
  - [武動青春](../Page/武動青春.md "wikilink")（Theresa參演）
  - [撲克王](../Page/撲克王.md "wikilink")（Stephy參演）
  - [72家租客](../Page/72家租客.md "wikilink")（Stephy參演）
  - [越光寶盒](../Page/越光寶盒.md "wikilink")（Stephy客串）
  - [宅男總動員](../Page/宅男總動員.md "wikilink")（Stephy參演）
  - [最強囍事](../Page/最強囍事.md "wikilink")（Stephy客串）
  - [戀夏戀夏戀戀下](../Page/戀夏戀夏戀戀下.md "wikilink")（Stephy客串）
  - [熱浪球愛戰](../Page/熱浪球愛戰.md "wikilink")（Theresa參演）
  - [喜愛夜蒲](../Page/喜愛夜蒲.md "wikilink")（Miki參演）
  - [保衛戰隊之出動喇！朋友！](../Page/保衛戰隊之出動喇！朋友！.md "wikilink")（Stephy參演）
  - [東成西就2011](../Page/東成西就2011.md "wikilink")（Stephy參演）
  - [公主的誘惑](../Page/公主的誘惑.md "wikilink")（Stephy參演）
  - [詭婚](../Page/詭婚.md "wikilink")（Theresa參演）
  - [等我愛你](../Page/等我愛你.md "wikilink")（Stephy參演）
  - [兇間雪山](../Page/兇間雪山.md "wikilink")（Stephy參演）

## 電視劇

  - [美麗在望](../Page/美麗在望.md "wikilink")（第1集客串）（[TVB電視劇](../Page/TVB.md "wikilink")）
  - [當四葉草碰上劍尖時](../Page/當四葉草碰上劍尖時.md "wikilink")（第1集客串）（[TVB電視劇](../Page/TVB.md "wikilink")）
  - [戀愛自由式](../Page/戀愛自由式.md "wikilink")（Stephy、Theresa參演）（[TVB電視劇](../Page/TVB.md "wikilink")）
  - [爭分奪秒](../Page/爭分奪秒.md "wikilink")（Miki參演）（[TVB電視劇](../Page/TVB.md "wikilink")）
  - [风雨桃花镇](../Page/风雨桃花镇.md "wikilink")（Stephy参演）（內地電視劇）
  - [大戲法](../Page/大戲法.md "wikilink")（Stephy參演）（內地電視劇）
  - [七個朋友](../Page/七個朋友.md "wikilink")（Theresa參演）（中港台合拍[偶像劇](../Page/偶像劇.md "wikilink")）
  - [神槍狙擊2013](../Page/神槍狙擊2013.md "wikilink")（Miki參演）（[TVB電視劇](../Page/TVB.md "wikilink")）
  - [貓屎媽媽](../Page/貓屎媽媽.md "wikilink")（Miki參演）（[TVB電視劇](../Page/TVB.md "wikilink")）
  - [暗戰2013](../Page/暗戰2013.md "wikilink")（Stephy參演）（內地電視劇）

## 單元劇及港台劇

  - [奇幻潮](../Page/奇幻潮.md "wikilink")（Miki、Theresa參演）
  - [森之愛情](../Page/森之愛情.md "wikilink")（Stephy參演）
  - [廉政行動2007](../Page/廉政行動2007.md "wikilink")（Theresa參演）
  - [海關故事](../Page/海關故事.md "wikilink")（Miki參演）
  - [沒有墻的世界](../Page/沒有墻的世界.md "wikilink")（Miki參演）
  - [愛回家](../Page/愛回家.md "wikilink")（Miki參演）
  - [有房出租2](../Page/有房出租2.md "wikilink")（Miki參演）
  - [火速救兵2](../Page/火速救兵2.md "wikilink")（Miki參演）
  - [一屋住家人](../Page/一屋住家人.md "wikilink")（Theresa参演）

## 網劇

  - 藍罐最痛（now.com網劇）
  - 百份百感覺 冬日戀曲（now.com網劇）
  - 四曲奇談（now.com網劇）
  - 傻傻愛（大陸網劇）（Theresa參演）
  - 物·語女仔（Goyeah網劇）（Miki參演）

## 廣告

  - 2002年 [Meko蜜桃之水](../Page/Meko.md "wikilink") & 桃之紅茶廣告
  - 2002年 MEKO純水／加礦礦泉水／果凍
  - freshlook[隱形眼鏡硬照](../Page/隱形眼鏡.md "wikilink")
  - 2 Percent（硬照）
  - [古龍群俠傳](../Page/古龍群俠傳.md "wikilink") 2 \[七武器\] 網絡版
  - 古龍群俠傳 2 \[幽靈山莊\]（硬照）
  - Hanacha 纖之花茶（硬照）
  - [adidas硬照](../Page/adidas.md "wikilink")
  - [新天地婚紗](../Page/新天地婚紗.md "wikilink")（硬照）

## 外部連結

  - [MikiYeung.net](https://web.archive.org/web/20050924134402/http://mikiyeung.net/bbs/)
  - [Cookies in pinkwork (sound &
    video)](http://www.pinkwork.com/cookies/p1.htm)

[Category:香港女子演唱團體](../Category/香港女子演唱團體.md "wikilink")
[Category:粵語流行音樂團體](../Category/粵語流行音樂團體.md "wikilink")
[Category:2002年成立的音樂團體](../Category/2002年成立的音樂團體.md "wikilink")
[Category:已解散的女子演唱團體](../Category/已解散的女子演唱團體.md "wikilink")