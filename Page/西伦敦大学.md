**西伦敦大学**（**University of West
London**）一家位於[英國的](../Page/英國.md "wikilink")[大學](../Page/大學.md "wikilink")，原稱泰晤士河谷大学，由[伊靈高等學院](../Page/伊靈高等學院.md "wikilink")、[泰晤士河谷學院](../Page/泰晤士河谷學院.md "wikilink")、[夏洛特皇后健康看護學院和](../Page/夏洛特皇后健康看護學院.md "wikilink")[倫敦音樂學院等學院由](../Page/倫敦音樂學院.md "wikilink")1990年起合併成西伦敦理工学院。1992年，成為泰晤士河谷大学。2004年吸納[雷丁學院暨藝術及設計學校成為新成員學院](../Page/雷丁學院暨藝術及設計學校.md "wikilink")。2010年8月宣稱將更名為西伦敦大学，\[1\]
2011年4月6日正式更名。\[2\]

## 排名

该大学整体排名在英国位于中游。

## 参考文献

## 外部链接

  - [西伦敦大学網頁](http://www.uwl.ac.uk/)

[西伦敦大学](../Category/西伦敦大学.md "wikilink")
[Category:倫敦高等教育](../Category/倫敦高等教育.md "wikilink")
[Category:1990年創建的教育機構](../Category/1990年創建的教育機構.md "wikilink")
[Category:1860年創建的教育機構](../Category/1860年創建的教育機構.md "wikilink")

1.
2.