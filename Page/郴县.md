**郴县**，中国古旧[县名](../Page/县.md "wikilink")。[秦朝置县](../Page/秦朝.md "wikilink")，[治所在今](../Page/治所.md "wikilink")[湖南省](../Page/湖南省.md "wikilink")[郴州市](../Page/郴州市.md "wikilink")。

[西汉时属](../Page/西汉.md "wikilink")[桂阳郡](../Page/桂阳郡.md "wikilink")，[新莽](../Page/新朝.md "wikilink")[始建国元年](../Page/始建国.md "wikilink")（9年），改桂阳郡为[南平郡](../Page/南平郡.md "wikilink")，改郴县为“宣风县”\[1\]，郴县隶属南平郡，南平郡治至耒阳（改名南平亭）。[东汉](../Page/东汉.md "wikilink")[建武年间恢复郡县原名](../Page/建武_\(东汉\).md "wikilink")，郡治复于郴县。[永和元年](../Page/永和_\(東漢\).md "wikilink")（136年），分郴县地置汉宁县。[晋](../Page/晋朝.md "wikilink")[建兴三年](../Page/建兴_\(成汉\).md "wikilink")（315年），分郴县地立平阳郡领[平阳县](../Page/平阳县_\(湖南古县份\).md "wikilink")，始一分为二郡。[南朝梁](../Page/南朝梁.md "wikilink")[天监六年](../Page/天监.md "wikilink")（507年），复置郴县，旋即撤。

[隋](../Page/隋朝.md "wikilink")[开皇九年](../Page/开皇.md "wikilink")（589年），三郡合为[郴州](../Page/郴州_\(古代\).md "wikilink")，平阳县、便县均省入郴县。[大业十三年](../Page/大业_\(年号\).md "wikilink")（617年），析郴县南为[义章县](../Page/义章县.md "wikilink")，分郴县西复置平阳县。[唐朝时](../Page/唐朝.md "wikilink")，郴县为[上县](../Page/上县.md "wikilink")，属郴州、桂阳郡。[开元十三年](../Page/开元.md "wikilink")，析郴县置[安陵县](../Page/安陵县.md "wikilink")（即[高亭县](../Page/高亭县.md "wikilink")）\[2\]。[乾元元年](../Page/乾元_\(唐朝\).md "wikilink")（758年），郴州移治于平阳县。[五代后晋](../Page/后晋.md "wikilink")[天福元年](../Page/天福_\(后晋\).md "wikilink")（936年），改郴州为敦州，郴县为敦化县。[后汉](../Page/後漢.md "wikilink")[乾祐右三年](../Page/乾祐_\(后汉\).md "wikilink")（950年）；郴州、郴县复名。

[北宋](../Page/北宋.md "wikilink")[太平兴国元年](../Page/太平兴国.md "wikilink")（976年），[泰县并入郴县](../Page/泰县_\(湖南县份\).md "wikilink")。[南宋](../Page/南宋.md "wikilink")[嘉定二年](../Page/嘉定_\(南宋\).md "wikilink")（1209年），析郴县资兴、程水二乡置[资兴县](../Page/资兴县.md "wikilink")，属[郴州军](../Page/郴州军.md "wikilink")。嘉定四年（1211年），设置的[桂东县亦](../Page/桂东县.md "wikilink")“本郴县地\[3\]”。[元](../Page/元朝.md "wikilink")[至元十三年](../Page/至元.md "wikilink")（1276年），改军为路，郴县改名[郴阳县](../Page/郴阳县.md "wikilink")，隶属[郴州路](../Page/郴州路.md "wikilink")。[明](../Page/明朝.md "wikilink")[洪武元年](../Page/洪武.md "wikilink")（1368年），改路为府，郴阳县隶属[郴州府](../Page/郴州府.md "wikilink")。洪武九年（1376年），郴州撤府设直隶州，郴阳县并入郴州。

民国元年（1912年），全国[废府州，存道县](../Page/废府州厅改县.md "wikilink")；次年9月，[郴州直隶州改为郴县](../Page/郴州直隶州.md "wikilink")。民国26年12月，郴县隶属[湖南省第八行政督察区](../Page/湖南省_\(中華民國\).md "wikilink")。民国29年，改第第八行政督察区为第三行政督察区，郴县隶属。

1949年10月初，随着国府在湖南省的败局已定，当地驻军、警察相续撤离。6日，县长周庚星带领县自卫队、警察约200余人逃至塘昌铺。当日，正在向[桂阳县进军的](../Page/桂阳县.md "wikilink")[中国人民解放军湘南支队得知消息后](../Page/中国人民解放军.md "wikilink")，司令员[刘亚球命副参谋长李明成率郴县光华乡军管会武装进驻郴县县城](../Page/刘亚球.md "wikilink")。在未遭受军事抵抗的情况下，他们于7日凌晨，在县城与湘南游击司令部南区指挥所第一、三、五大队会师，宣告郴县“解放”\[4\]。同年11月25日，成立[郴县专区](../Page/郴县专区.md "wikilink")，郴县隶属。

1958年8月，郴县析设郴州市，隶属郴县。1959年3月，资兴并入郴县。1963年5月20日，撤销郴州市，仍为郴县县级镇。1994年12月，撤销[郴州地区](../Page/郴州地区.md "wikilink")、[郴州市和郴县](../Page/郴州市_\(县级市\).md "wikilink")，设立[郴州市](../Page/郴州市.md "wikilink")（地级市）。原郴县地分别划入[北湖区与](../Page/北湖区.md "wikilink")[苏仙区](../Page/苏仙区.md "wikilink")\[5\]\[6\]。

## 参考

[Category:秦朝县份](../Category/秦朝县份.md "wikilink")
[Category:汉朝县份](../Category/汉朝县份.md "wikilink")
[Category:晋朝县份](../Category/晋朝县份.md "wikilink")
[Category:南朝县份](../Category/南朝县份.md "wikilink")
[Category:隋朝县份](../Category/隋朝县份.md "wikilink")
[Category:唐朝县份](../Category/唐朝县份.md "wikilink")
[Category:五代十国县份](../Category/五代十国县份.md "wikilink")
[Category:宋朝县份](../Category/宋朝县份.md "wikilink")
[Category:元朝县份](../Category/元朝县份.md "wikilink")
[Category:中华民国湖南省县份](../Category/中华民国湖南省县份.md "wikilink")
[Category:湖南省已撤消的县份](../Category/湖南省已撤消的县份.md "wikilink")
[Category:郴州行政区划史](../Category/郴州行政区划史.md "wikilink")
[Category:苏仙区](../Category/苏仙区.md "wikilink")
[Category:北湖区](../Category/北湖区.md "wikilink")
[Category:1994年废除的行政区划](../Category/1994年废除的行政区划.md "wikilink")

1.  《[汉书](../Page/汉书.md "wikilink")·卷二十八上·地理志第八上
    》桂阳郡，高帝置。莽曰南平。属荆州……县十一：郴，耒山，耒水所出，西至湘南入湘。项羽所立义帝都此。莽曰宣风。临武……
2.  《新唐书·卷四十一·志第三十一》◎地理五……郴州桂阳郡，上。土贡……县八：郴，上。有马岭山。义章……高亭，中下。本安陵，开元十三年析郴置，天宝元年更名。义昌……
3.  《[宋史](../Page/宋史.md "wikilink")·卷八十八·志第四十一》地理四……郴州，中，桂阳郡，军事……县四：郴，紧，有新塘、浦溪二银坑。桂阳……南渡后，增县二：兴宁，嘉定二年，析郴县资兴、程水二乡置资兴县，后改今名。桂东。本郴县地。嘉定四年，析桂阳之零陵、宜城二乡置今县于上犹砦。宝庆府……
4.
5.  [行政区划网](http://www.xzqh.org/html/hn/10_107.html)
6.  [郴州门户网：《郴州历史沿革》](http://www.czs.gov.cn/sitepublish/site200/zjcz/lsyg/content_35298.html)