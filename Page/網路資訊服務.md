**網路資訊服務**簡稱 **NIS**（**N**etwork **I**nformation
**S**ervice），時又譯為**網路資料服務協定**，亦即一般簡稱之「黃頁」**YP**（**Y**ellow
**P**ages），最早由[昇陽公司開發出來](../Page/昇陽.md "wikilink")，為一套用來管理[電腦網路中所有與](../Page/電腦網路.md "wikilink")[電腦系統管理相關之設定檔](../Page/電腦.md "wikilink")，如使用者帳號、[密碼](../Page/密碼.md "wikilink")、主機名稱或群組等的[主從式](../Page/主從式.md "wikilink")[目錄服務](../Page/目錄服務.md "wikilink")[協定](../Page/協定.md "wikilink")。

由於 NIS 推出時的舊有名字 "Yellow
page"「[黃頁](../Page/黃頁.md "wikilink")」已為[英國](../Page/英國.md "wikilink")[電信集團註冊為該公司商業電話名錄的](../Page/英國電信集團.md "wikilink")[商標](../Page/商標.md "wikilink")，故昇陽將之改名為
NIS，然而所有的指令及函式命名仍以原縮寫 "yp" 為起首。

在許多環境下，已有其他目錄服務比 NIS 更先進、更安全，例如 [LDAP](../Page/LDAP.md "wikilink")
就已經逐漸取代 NIS。

## NIS 相關指令

  - `ypcat`
  - `ypmatch`
  - `ypwhich`
  - `ypclnt`
  - `yppasswd`
  - `ypset`
  - `ypmake`
  - `ypinit`
  - `yppush`
  - `ypserve`

## 另見

  - [遠端程序呼叫](../Page/遠端程序呼叫.md "wikilink") (RPC)
  - [網路檔案系統](../Page/網路檔案系統.md "wikilink")（NFS）
  - [網域名稱系統](../Page/網域名稱系統.md "wikilink")（DNS）

## 外部連結

  - [The NIS-HOWTO](http://www.tldp.org/HOWTO/NIS-HOWTO/) 摘自

  - : [<cite>NFS and NIS
    Security</cite>](http://www.securityfocus.com/infocus/1387)
    Securityfocus article Jan. 22, 2001

  - [<cite>Red Hat Linux 9: Red Hat Linux Security
    Guide</cite>](http://www.redhat.com/docs/manuals/linux/RHL-9-Manual/security-guide/s1-server-nis.html)
    - 5.3. Securing NIS

  - [NIS = Network Information
    System](http://nmc.nchu.edu.tw/linux/nis.htm)

  - [鳥哥的 Linux 私房菜 — NIS
    伺服器](http://linux.vbird.org/linux_server/0430nis.php)

[Category:Unix](../Category/Unix.md "wikilink")
[Category:昇陽電腦](../Category/昇陽電腦.md "wikilink")
[Category:信息技术](../Category/信息技术.md "wikilink")
[Category:网络技术](../Category/网络技术.md "wikilink")
[Category:電腦安全](../Category/電腦安全.md "wikilink")
[Category:系统软件](../Category/系统软件.md "wikilink")
[Category:进程间通信](../Category/进程间通信.md "wikilink")