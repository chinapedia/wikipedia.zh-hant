**斯文-約蘭·艾歷臣**（，）[瑞典籍](../Page/瑞典.md "wikilink")[足球教練](../Page/足球.md "wikilink")，2002年至2006年期間擔任[英格蘭足球代表隊教練](../Page/英格蘭足球代表隊.md "wikilink")。

2006年1月31日，埃里克森獲瑞典國王[卡爾十六世·古斯塔夫頒授瑞典](../Page/卡爾十六世·古斯塔夫.md "wikilink")[國王獎章](../Page/國王獎章.md "wikilink")（King's
Medal），是瑞典國王頒授的第二最高榮譽，以表揚他對國家及國際足球運動的貢獻。

## 执教生涯

### 早期

埃里克森出生於圖什比，球員生涯不算突出，只在瑞典低組別球會KB
Karlskoga及代格福什效力，其後在1975年因[膝傷而需永久退役](../Page/膝.md "wikilink")。退役後埃里克森首先出任教練，其後成為代格福什的球隊經理，在3年內把球會由丙組升至甲組聯賽。

他在代格福什的成功吸引不少國內球會注意。1979年埃里克森加盟[哥德堡](../Page/哥登堡足球會.md "wikilink")，首季執教便為球會取得[瑞典盃](../Page/瑞典盃.md "wikilink")，1982年更為球會取得三冠榮譽，包括聯賽、瑞典盃及[歐洲足協盃](../Page/歐洲足協盃.md "wikilink")（以4-0擊敗[漢堡](../Page/汉堡足球俱乐部.md "wikilink")）的榮譽。

埃里克森不久轉投[葡萄牙球會](../Page/葡萄牙.md "wikilink")[本菲卡](../Page/本菲卡.md "wikilink")，首季便為本菲卡獲得葡萄牙聯賽冠軍及打入歐洲足協盃4強。在取得第二屆聯賽冠軍後，埃里克森前往[意大利出任](../Page/意大利.md "wikilink")[羅馬的領隊](../Page/羅馬足球俱樂部.md "wikilink")，但他並未能取得佳績，只在1986年取得[意大利盃冠軍](../Page/意大利盃.md "wikilink")。

在[瓦伦西亚兩季皆無法取得任何錦標後](../Page/瓦伦西亚足球俱乐部.md "wikilink")，埃里克森在1989年回到本菲卡，並帶領球隊打入1990年[歐洲冠軍球會盃決賽](../Page/歐洲冠軍球會盃.md "wikilink")，以0-1敗於[AC米蘭](../Page/AC米蘭.md "wikilink")。1991年再取得葡萄牙聯賽冠軍。1992年，埃里克森又返回意大利測試自己的運氣，加盟[桑普多利亚](../Page/森多利亞足球會.md "wikilink")，但他只能協助球會取得另一次意大利盃。

埃里克森最終在意大利取得好成績是當他在1997年加盟[拉齐奥後](../Page/拉素體育會.md "wikilink")，在拉素他共為球會取得兩次意大利盃（1998年及2000年）和[歐洲盃賽冠軍盃](../Page/歐洲盃賽冠軍盃.md "wikilink")（1999年最後一屆）。1999年更帶領拉齐奥在聯賽排在前列，直到聯賽末段被[AC米蘭趕上而屈居亞軍](../Page/AC米蘭.md "wikilink")。翌年拉齐奥亦一度陷入危機，直到季末埃里克森重新振作，終帶領拉齐奥取得[意大利甲組聯賽冠軍](../Page/意大利甲組聯賽.md "wikilink")（2000年）。

### 英格蘭代表隊

2000年10月英格蘭正在找尋新的代表隊教練，其中包括艾歷臣。艾歷臣答應在他與拉素的合約在2001年夏季屆滿後出任英格蘭教練。他是英格蘭代表隊首位外籍教練，也是首位非西歐籍英格蘭國家隊教練，外界對此反應不一，有支持亦有感到不滿。

此時英格蘭代表隊在[2002年世界盃外圍賽一度排在榜末](../Page/2002年世界盃.md "wikilink")，艾歷臣先帶領代表隊在[弱隊身上取分](../Page/弱隊.md "wikilink")。到大局已定後，英格蘭在2001年9月1日在[慕尼黑作客](../Page/慕尼黑.md "wikilink")[德國](../Page/德國國家足球隊.md "wikilink")，艾歷臣顯示其才能，以5-1大勝德國；其後主場對[希臘一仗亦順利過關](../Page/希臘國家足球隊.md "wikilink")。英格蘭在2002年世界盃決賽周早段表現出色，雖置身死亡之組（同組有[尼日利亞](../Page/尼日利亞國家足球隊.md "wikilink")、[阿根廷及其祖國](../Page/阿根廷國家足球隊.md "wikilink")[瑞典](../Page/瑞典國家足球隊.md "wikilink")），仍可順利出線。最終在八強以2-1敗於[巴西腳下](../Page/巴西國家足球隊.md "wikilink")，巴西其後成為該屆世界盃冠軍。

[2004年歐洲國家杯英格蘭在次圈僅以互射十二碼才被](../Page/2004年歐洲國家杯.md "wikilink")[葡萄牙淘汰](../Page/葡萄牙國家足球隊.md "wikilink")。而他用人作風亦以才能為本，大膽地提拔一些國家隊新秀，為球隊注入生氣。在他帶領下所引入的新秀，不乏佼佼者，有神童[朗尼](../Page/朗尼.md "wikilink")、中場大腦[林柏特和車路士隊長](../Page/林柏特.md "wikilink")[-{zh-hans:特里;zh-hk:泰利;zh-tw:泰瑞;}-](../Page/約翰·泰利.md "wikilink")，完全摒棄過去英格蘭國家隊只崇名氣的用人作風。

2005年9月7日，艾歷臣帶領的英格蘭在世界盃外圍賽中以0-1敗於[北愛爾蘭](../Page/北愛爾蘭足球代表隊.md "wikilink")，是自1972年以來首次敗於北愛爾蘭。雖然這是艾歷臣帶領下首次在主要賽事外圍賽中落敗，但已令艾歷臣受到很大壓力，並受到部份球迷及[BBC評述員的批評](../Page/BBC.md "wikilink")。批評在英格蘭以1-0小勝[奧地利後並未停止](../Page/奧地利國家足球隊.md "wikilink")，因[碧咸在比賽中被調離場](../Page/贝克汉姆.md "wikilink")。部份批評其後獲得回應，英格蘭取得較佳的成績，雖然缺少幾位主力球員，包括碧咸被停賽及[-{zh-hans:索尔·坎贝尔;zh-hk:蘇·金寶;zh-tw:索爾·坎貝爾;}-與](../Page/蘇甘保.md "wikilink")[-{zh-hans:杰拉德;zh-hk:謝拉特;zh-tw:傑拉德;}-因傷缺陣](../Page/史提芬·謝拉特.md "wikilink")，英格蘭仍能以2-1擊敗[波蘭取得勝利](../Page/波蘭國家足球隊.md "wikilink")。

除此之外，艾歷臣的私生活亦常是英國人的焦點，最著名的是他與電視節目主持烏爾里卡·約翰遜（Ulrika
Jonsson），及足總女秘書法莉亞·阿萊姆（Faria
Alam）的關係，後者更令足總總幹事麦克·帕利奧斯（Mark
Palios）辭職\[1\]。雖然如此，艾歷臣仍獲英格蘭足總保證將帶領英格蘭國家隊至2008年。

之不過英國人對艾歷臣的態度，令艾歷臣最終難逃提早離開之厄運。2006年1月23日，英格蘭足總宣佈艾歷臣將在[2006年世界盃結束後離開英格蘭代表隊](../Page/2006年世界盃足球賽.md "wikilink")，艾歷臣指出他在先前已協議在世界盃結束後立即終止合約，其空缺轉由[-{zh-hans:麦克拉伦;zh-hk:麥卡倫;zh-tw:麥克拉倫;}-接任](../Page/史提夫·麥卡倫.md "wikilink")。

英格蘭在2006年世界盃分組賽被編入B組，同組對手包括[瑞典](../Page/瑞典國家足球隊.md "wikilink")、[巴拉圭及](../Page/巴拉圭國家足球隊.md "wikilink")[千里達](../Page/千里達及托巴哥國家足球隊.md "wikilink")。艾歷臣在最後一場在八強戰敗給[葡萄牙](../Page/葡萄牙國家足球隊.md "wikilink")，便離開英格蘭代表隊。

### 曼城

離任後的艾歷臣首一年未有執教任何球隊，直至2007年夏季[英超球會](../Page/英超.md "wikilink")[曼城被](../Page/曼城.md "wikilink")[泰國前總理](../Page/泰國.md "wikilink")[他信收購](../Page/他信.md "wikilink")，艾歷臣終獲邀「出山」，擔任曼城領隊。季初曼城成績出色，但新年後狀態下滑，季末他信稱艾歷臣「不是曼城領隊的正確人選」，尚有兩年合約的艾歷臣堅持不會辭職\[2\]。季後帶隊到[亞洲比賽後](../Page/亞洲.md "wikilink")，於2008年6月2日正式離任\[3\]。僅1日後獲聘為[墨西哥國家足球隊的教練](../Page/墨西哥國家足球隊.md "wikilink")\[4\]。

### 墨西哥國家隊

艾歷臣帶隊參賽[2010年世界盃外圍賽](../Page/2010年世界盃外圍賽_\(中北美洲及加勒比海區\).md "wikilink")，[墨西哥以第一種子排名直入初賽第二圈](../Page/墨西哥國家足球隊.md "wikilink")，在兩回合累計9-0輕鬆擊敗[伯利茲後艾歷臣才正式上任](../Page/伯利茲國家足球隊.md "wikilink")。但第三圈分組賽6戰3勝1和2負屈居[洪都拉斯之後](../Page/洪都拉斯國家足球隊.md "wikilink")，僅以[得失球差壓倒](../Page/得失球差.md "wikilink")[牙買加晉級決賽圈](../Page/牙買加國家足球隊.md "wikilink")。決賽圈首場作客0-2負於[美國](../Page/美國國家足球隊.md "wikilink")，艾歷臣開始受到壓力，同時有傳聞[-{zh-hans:朴茨茅斯;zh-hk:樸茨茅夫}-有意聘用艾歷臣代替離職的](../Page/朴茨茅斯足球俱乐部.md "wikilink")[東尼·阿當斯](../Page/托尼·亚当斯.md "wikilink")<small>\[5\]</small>。次場主場2-0擊敗[哥斯達黎加](../Page/哥斯達黎加國家足球隊.md "wikilink")，但2009年4月1日作客1-3負於[洪都拉斯](../Page/洪都拉斯國家足球隊.md "wikilink")，在最後6強的小組排名第四，於翌日被辭退<small>\[6\]</small>。

### 諾士郡

2009年7月22日艾歷臣加盟剛於一週前被[中東財團收購的](../Page/中東.md "wikilink")[英乙球會](../Page/英格蘭足球乙級聯賽.md "wikilink")[諾士郡](../Page/諾士郡足球會.md "wikilink")，擔任足球總監一職<small>\[7\]</small>。2010年2月11日在諾士郡轉手後即時離任<small>\[8\]</small>。

### 科特迪瓦國家隊

2010年3月28日獲聘為[科特迪瓦國家隊教練](../Page/科特迪瓦國家足球隊.md "wikilink")，帶隊參賽[2010年世界盃](../Page/2010年世界盃足球賽.md "wikilink")<small>\[9\]</small>，期間取得1勝1和1負的成績，未能晉級淘汰賽階段，賽後艾歷臣沒有獲得提供續約而離隊。

### 李斯特城

2010年10月3日艾歷臣重返[英格蘭球壇](../Page/英格蘭.md "wikilink")，執教仍處身降班區域內的[英冠球會](../Page/英冠.md "wikilink")[莱斯特城](../Page/莱斯特城足球俱乐部.md "wikilink")，簽約兩年，為爭取升上英格蘭超級聯賽作好準備\[10\]。雖然球隊於2011年夏季大事擴軍，但季初成績不穩，13場聯賽僅得5勝，艾歷臣同意提前解約\[11\]。

### 广州富力

2013年6月4日，[中国足球超级联赛球队](../Page/中国足球超级联赛.md "wikilink")[广州富力宣布埃里克森成为球队新任主教练](../Page/广州富力足球俱乐部.md "wikilink")，任期至2014年12月\[12\]，而据英国传媒报道，他的年薪约为200万[英镑](../Page/英镑.md "wikilink")\[13\]。2014赛季，埃里克森率领广州富力取得联赛第三名，获得亚冠参赛资格。2014年11月10日，广州富力官方宣布埃里克森不再担任球队主帅。\[14\]

### 上海上港

2014年11月18日，[中国足球超级联赛球队](../Page/中国足球超级联赛.md "wikilink")[上海上港宣布埃里克森成为球队新任主教练](../Page/上海上港集团足球俱乐部.md "wikilink")，任期至2016年12月\[15\]，而据传，他的年薪约为500万[英镑](../Page/英镑.md "wikilink")\[16\]。

2015赛季，埃里克森率领上海上港获得中超亚军，获得[亚冠参赛资格](../Page/亚足联冠军联赛.md "wikilink")。

2016赛季，埃里克森率领首次征战亚冠联赛的上海上港历史性进军亚冠联赛8强，但球队在赛季中期遭遇了大规模伤病，最终在亚冠1/4决赛中总比分0-5惨败给全北现代出局，中超联赛中，上海上港最终位列第三，再次获得亚冠资格，赛季结束后，上海上港宣布埃里克森离任\[17\]。

### 深圳佳兆业

2016年12月5日，[中国足球甲级联赛球队](../Page/中国足球协会甲级联赛.md "wikilink")[深圳佳兆业宣布埃里克森成为新任主教练](../Page/深圳市足球俱乐部.md "wikilink")\[18\]。

2017年6月14日，深圳佳兆业宣布埃里克森不再担任球队主教练一职\[19\]。埃里克森带领深圳队一路高开低走，在[中甲联赛开局豪取五连胜后](../Page/中国足球协会甲级联赛.md "wikilink")，球队被接下来的八轮比赛5平3负难求一胜，最终被辞退。

## 参考文献

## 外部連結

  - [TheFA.com官方資料](https://archive.is/20050305045303/http://www.thefa.com/England/SeniorTeam/Coaches/Postings/2003/09/SvenCoach.htm)

[分类:在中国执教的外籍足球主教练](../Page/分类:在中国执教的外籍足球主教练.md "wikilink")

[Category:2010年世界盃足球賽主教練](../Category/2010年世界盃足球賽主教練.md "wikilink")
[Category:2006年世界盃足球賽主教練](../Category/2006年世界盃足球賽主教練.md "wikilink")
[Category:2002年世界盃足球賽主教練](../Category/2002年世界盃足球賽主教練.md "wikilink")
[Category:2004年歐洲國家盃主教練](../Category/2004年歐洲國家盃主教練.md "wikilink")
[Category:瑞典足球運動員](../Category/瑞典足球運動員.md "wikilink")
[Category:足球後衛](../Category/足球後衛.md "wikilink")
[Category:瑞典足球主教練](../Category/瑞典足球主教練.md "wikilink")
[Category:哥登堡主教練](../Category/哥登堡主教練.md "wikilink")
[Category:賓菲加主教練](../Category/賓菲加主教練.md "wikilink")
[Category:羅馬主教練](../Category/羅馬主教練.md "wikilink")
[Category:費倫天拿主教練](../Category/費倫天拿主教練.md "wikilink")
[Category:森多利亞主教練](../Category/森多利亞主教練.md "wikilink")
[Category:拉素主教練](../Category/拉素主教練.md "wikilink")
[Category:英格蘭足球代表隊領隊](../Category/英格蘭足球代表隊領隊.md "wikilink")
[Category:曼城領隊](../Category/曼城領隊.md "wikilink")
[Category:墨西哥國家足球隊主教練](../Category/墨西哥國家足球隊主教練.md "wikilink")
[Category:李斯特城領隊](../Category/李斯特城領隊.md "wikilink")
[Category:英超領隊](../Category/英超領隊.md "wikilink")
[Category:意甲主教練](../Category/意甲主教練.md "wikilink")
[Category:葡超主教練](../Category/葡超主教練.md "wikilink")
[Category:英格蘭足球聯賽領隊](../Category/英格蘭足球聯賽領隊.md "wikilink")
[Category:深圳佳兆业主教练](../Category/深圳佳兆业主教练.md "wikilink")
[Category:广州富力主教练](../Category/广州富力主教练.md "wikilink")
[Category:中超主教练](../Category/中超主教练.md "wikilink")
[Category:中甲主教练](../Category/中甲主教练.md "wikilink")
[Category:足球主教練](../Category/足球主教練.md "wikilink")

1.  [bbc
    news](http://news.bbc.co.uk/chinese/trad/hi/newsid_3520000/newsid_3526600/3526622.stm)
2.  [Eriksson to be sacked by Man
    City](http://news.bbc.co.uk/sport2/hi/football/teams/m/man_city/7370698.stm)
3.  [Eriksson leaves Manchester
    City](http://news.bbc.co.uk/sport2/hi/football/teams/m/man_city/7430827.stm)
4.  [Eriksson appointed Mexico
    coach](http://news.bbc.co.uk/sport2/hi/football/internationals/7432298.stm)
5.
6.
7.
8.
9.
10.
11.
12.
13.
14.
15.
16.
17.
18.
19.