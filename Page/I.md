  -
    *因为[MediaWiki大写机制的缘故](../Page/MediaWiki.md "wikilink")，**ı**（[不带点小写i](../Page/带点与不带点I.md "wikilink")）会来到此页。*

**I**, **i**
是[拉丁字母的第](../Page/拉丁字母.md "wikilink")9个[字母](../Page/字母.md "wikilink")，源于[希腊字母](../Page/希腊字母.md "wikilink")
[Ι,
ι](../Page/Ι.md "wikilink")（Iota），与[伊特鲁里亚字母一样表示音值](../Page/伊特鲁里亚字母.md "wikilink")/i/。在[拉丁语](../Page/拉丁语.md "wikilink")（包括[当代拉丁语](../Page/当代拉丁语.md "wikilink")）还表示/j/（例如[英语](../Page/英语.md "wikilink")
Yoke 中的
[Y](../Page/Y.md "wikilink")）。在[闪族语](../Page/闪族语.md "wikilink")，/j/是

的普遍发音，/i/只用于外来语。在英语表示不同发音，包括[长音和](../Page/长音.md "wikilink")[短音](../Page/短音.md "wikilink")。

拉丁[Ɩ, ɩ](../Page/Ɩ.md "wikilink")，非洲语言。

在[土耳其语字母](../Page/土耳其语字母.md "wikilink")，有点的“”、“”与没有点的“”、“”是两个不同字母。

在[北约音标字母](../Page/北约音标字母.md "wikilink")，I表示为*India*，或更少见的，表示为*Indigo*。

## 字母I的含意

## 字符编码

| 字符编码                               | [ASCII](../Page/ASCII.md "wikilink") | [Unicode](../Page/Unicode.md "wikilink") | [EBCDIC](../Page/EBCDIC.md "wikilink") | [摩斯电码](../Page/摩斯电码.md "wikilink") |
| ---------------------------------- | ------------------------------------ | ---------------------------------------- | -------------------------------------- | ---------------------------------- |
| [大写](../Page/大写字母.md "wikilink") I | 73                                   | 0049                                     | 201                                    | `··`                               |
| [小写](../Page/小写字母.md "wikilink") i | 105                                  | 0069                                     | 137                                    |                                    |

## 其他表示方法

## 参看

  - [ı（不带点的小写i）与 İ（带点的大写i）](../Page/带点与不带点I.md "wikilink")

  - [次閉次前不圓唇元音](../Page/次閉次前不圓唇元音.md "wikilink")

### 其他字母中的相近字母

  - （[希腊字母](../Page/希腊字母.md "wikilink") Iota）

  - （西里尔字母
    I，只在[乌克兰语和](../Page/乌克兰语.md "wikilink")[白俄罗斯语使用](../Page/白俄罗斯语.md "wikilink")）

  - （[西里尔字母](../Page/西里尔字母.md "wikilink") I）

[Category:拉丁字母](../Category/拉丁字母.md "wikilink")