[Geylang_Road,_Nov_05.JPG](https://zh.wikipedia.org/wiki/File:Geylang_Road,_Nov_05.JPG "fig:Geylang_Road,_Nov_05.JPG")
[Geylang_Road_Shophouses.jpg](https://zh.wikipedia.org/wiki/File:Geylang_Road_Shophouses.jpg "fig:Geylang_Road_Shophouses.jpg")

**芽笼**（**Geylang**）与**芽笼士乃**（**Geylang
Serai**），是城市国家[新加坡之一个社区](../Page/新加坡.md "wikilink")，在新加坡[金融区以东](../Page/金融区.md "wikilink")，位于[新加坡河的东部](../Page/新加坡河.md "wikilink")。

## 源说

早於十九世紀初[新加坡開埠初年](../Page/新加坡.md "wikilink")，芽籠已出現於典籍內。1822年，當[新加坡剛剛成為](../Page/新加坡.md "wikilink")[英國殖民地不久](../Page/英國殖民地.md "wikilink")，[殖民政府著手](../Page/殖民.md "wikilink")[新加坡之地方規劃時](../Page/新加坡.md "wikilink")，已提到「芽籠河」一詞。芽籠
(英文名稱
*Geylang*)得名有多種源說，其中一個說法是馬來文"Kilang"，意思是[工廠](../Page/工廠.md "wikilink")，因當時該處有大量的[椰林及](../Page/椰子.md "wikilink")[檸檬草種植園](../Page/檸檬草.md "wikilink")，衍生出芽籠一帶眾多以椰子及檸檬草為原料的加工廠房為名。

另一種源說是指「芽籠」其實是指[新加坡的部落](../Page/新加坡.md "wikilink")*orang
gallang*。此部落是馬來半島沿海一帶*orang
laut*部族的其中一支，以掠奪[新加坡附近海域的船隻而著名](../Page/新加坡.md "wikilink")。

此外亦有一說指「芽籠」其實是福建話「雞籠」，因「雞籠」之福建話讀"kei-lang"，與『芽籠』的英文名稱"Geylang"讀音非常相似，是故有此推測。

## 历史

自英國殖民政府於開埠初年將馬來人及土著由新加坡河河口遷移到芽籠後，此區便成為新加坡馬來人的集中地。直到十九世紀後期芽籠亦漸漸成為馬來與阿拉伯富商的聚居地，當中包括[Alsagoff](../Page/Alsagoff.md "wikilink")、[Alkaff](../Page/Alkaff.md "wikilink")、[Aljunied家族](../Page/Aljunied.md "wikilink")。

到二十世紀三十年代，芽籠出現了多個純馬來人的區域，例如Kampong
Melayu（馬來村），及後演變成今天的芽籠士乃。近來於芽籠所發展的[馬來村就是要將此區當年的面貌重新塑造出來](../Page/馬來村.md "wikilink")，以迎合近年興起的文化旅遊。

## 今天的芽笼

芽籠好一部分至今仍未受[新加坡自二十世紀七十年代以來所展開的市區重建所影響](../Page/新加坡.md "wikilink")，該區將嶺南地區傳統的[店屋以及多姿多彩的夜生活融為一體](../Page/店屋.md "wikilink")。區內合法之[紅燈區](../Page/紅燈區.md "wikilink")、外籍傭工宿舍、卡拉OK酒廊等與[新加坡所予人](../Page/新加坡.md "wikilink")「花園城市」的形象形成強烈的對比。芽籠的[店屋歷史悠久](../Page/店屋.md "wikilink")，政府亦立法保護[店屋免被清拆](../Page/店屋.md "wikilink")，區內著名食肆林立，主要分佈於芽籠的主要道路─芽籠路上。

## 交通

有多个MRT地铁站通向芽笼附近的地方，他们包括[巴耶利峇地铁站](../Page/巴耶利峇地铁站.md "wikilink")，[阿裕尼地铁站和](../Page/阿裕尼地铁站.md "wikilink")[加冷地铁站](../Page/加冷地铁站.md "wikilink")。还有位于加冷规划地区的[芽笼1巷巴士终站提供多条交通线路](../Page/芽笼1巷巴士终站.md "wikilink")。

而今天的芽笼已经成为小吃和熟食中心。

## 参考资料

  - Peter K G Dunlop (2000), *Street Names of Singapore*, Who's Who
    Publishing, ISBN 981-4062-11-1
  - Victor R Savage, Brenda S A Yeoh (2003), *Toponymics - A Study of
    Singapore Street Names*, Eastern Universities Press, ISBN
    981-210-205-1

[category:芽笼](../Page/category:芽笼.md "wikilink")

[Category:紅燈區](../Category/紅燈區.md "wikilink")