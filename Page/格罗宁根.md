[Stadsdelen_groningen.jpg](https://zh.wikipedia.org/wiki/File:Stadsdelen_groningen.jpg "fig:Stadsdelen_groningen.jpg")
[Groningermuseum2.jpg](https://zh.wikipedia.org/wiki/File:Groningermuseum2.jpg "fig:Groningermuseum2.jpg")（Groninger
Museum） (2004)\]\]
[RijksUniversiteit_Groningen_-_University_of_Groningen.jpg](https://zh.wikipedia.org/wiki/File:RijksUniversiteit_Groningen_-_University_of_Groningen.jpg "fig:RijksUniversiteit_Groningen_-_University_of_Groningen.jpg")主建築
(2004)\]\]
[Groningen_Der_Aa-kerk_april04.jpg](https://zh.wikipedia.org/wiki/File:Groningen_Der_Aa-kerk_april04.jpg "fig:Groningen_Der_Aa-kerk_april04.jpg")
[20090218_Der_Aa-kerk_Groningen_NL.jpg](https://zh.wikipedia.org/wiki/File:20090218_Der_Aa-kerk_Groningen_NL.jpg "fig:20090218_Der_Aa-kerk_Groningen_NL.jpg")
[Herestraat_Groningen.JPG](https://zh.wikipedia.org/wiki/File:Herestraat_Groningen.JPG "fig:Herestraat_Groningen.JPG")
[100516_Gedempte_Zuiderdiep_96_Groningen_NL.jpg](https://zh.wikipedia.org/wiki/File:100516_Gedempte_Zuiderdiep_96_Groningen_NL.jpg "fig:100516_Gedempte_Zuiderdiep_96_Groningen_NL.jpg")

**格罗宁根**（）是位于[荷兰北部的一座中等城市](../Page/荷兰.md "wikilink")，也是[格罗宁根省的首府](../Page/格罗宁根省.md "wikilink")，人口约185,000（2007年），是荷兰北部人口最多的城市。

著名景点有[马提尼塔](../Page/马提尼塔.md "wikilink")（Martini
Tower）。塔高97[米](../Page/米.md "wikilink")，于1482年建成，是荷兰第四高的[教堂尖塔](../Page/教堂.md "wikilink")。格罗宁根还是[格罗宁根大学和格罗宁根高等职业学校的所在地](../Page/格罗宁根大学.md "wikilink")。全市大学生共有5万多，占人口很大比重。

## 歷史

目前最早記載格羅寧根的文件約在1040年。但目前發現最古老的考古遺跡約在西元前3950至3720年。直到西元三世紀，格羅寧根才出現第一個主要聚落。

13世紀，格羅寧根成為一個重要的貿易中心，並建造[城牆](../Page/城牆.md "wikilink")。該市對於周圍地區有極大的影響力，讓格羅寧根[方言成為當地的主要語言](../Page/方言.md "wikilink")。15世紀末，當時鄰近的[菲士蘭省由格羅寧根管理](../Page/菲士蘭.md "wikilink")，是格羅寧根最具影響力的時代。同一時期，格羅寧根修築了馬提尼塔，高約127公尺，成為當時歐洲最高的建築物。16世紀，[西班牙帝國與](../Page/西班牙帝國.md "wikilink")[荷蘭共和國爆發](../Page/荷蘭共和國.md "wikilink")[八十年戰爭](../Page/八十年戰爭.md "wikilink")。1594年，格羅寧根選擇支持[西班牙帝國](../Page/西班牙帝國.md "wikilink")，但之後改變立場，加入荷蘭共和國。

1614年，[格羅寧根大學成立](../Page/格羅寧根大學.md "wikilink")，最初的目的是[宗教教育](../Page/神學.md "wikilink")。同一時期，城市發展快速，並興建新城牆。1672年，[第三次英荷戰爭爆發](../Page/第三次英荷戰爭.md "wikilink")，格羅寧根受到[明斯特主教](../Page/明斯特.md "wikilink")的猛烈攻擊，但城牆成功阻擋了攻勢。每年的8月28日，民眾會以音樂和[煙火慶祝這次勝利](../Page/煙火.md "wikilink")。

格羅寧根沒有逃過二戰的蹂躪。1945年4月的[格羅寧根戰役](../Page/格羅寧根戰役.md "wikilink")，城中的主廣場格羅特市場廣場（Grote
Markt）遭到嚴重破壞。不過馬提尼塔、馬提尼教堂、和市政廳都未受到破壞。這場戰爭持續了數天。

## 藝術與文化

雖然格羅寧根不是個非常大的都市，但它仍是荷蘭該地區重要的都會，尤其在音樂和其他藝術、教育、商業等領域中有著重要地位。格羅寧根大量的學生族群也為城市建立了一個多元化的文化背景。

### 博物館

格羅寧根最重要且最知名的博物館是[格羅寧格博物館](../Page/格羅寧格博物館.md "wikilink")（Groninger
Museum）。由[亞歷山卓·門蒂尼](../Page/亞歷山卓·門蒂尼.md "wikilink")（Alessandro
Mendini）所設計的建築讓格羅寧格博物館成為荷蘭最現代且最創新的博物館之一。另外，格羅寧根還有一座[科學博物館](../Page/科學博物館.md "wikilink")、一座[漫畫博物館和一座](../Page/漫畫.md "wikilink")[菸草博物館](../Page/菸草.md "wikilink")。

### 夜生活

格羅寧根的夜生活大部分是依靠其學生族群。格羅特市場等廣場在夜晚十分擁擠，尤其是星期四與星期五，部分酒吧營業至早上7點。2006年，格羅寧根被票選為荷蘭「最佳市中心」（de
beste binnenstad）。

## 格羅寧根大學

1614年成立的[格羅寧根大學有著豐富的學術傳統](../Page/格羅寧根大學.md "wikilink")，是荷蘭第二悠久的大學（僅次於[萊頓大學](../Page/萊頓大學.md "wikilink")）。荷蘭第一位女性學生、荷蘭第一位太空人，[歐洲中央銀行總裁都來自格羅寧根大學](../Page/歐洲中央銀行.md "wikilink")，此外还有多位[諾貝爾獎得主](../Page/諾貝爾獎.md "wikilink")。

## 友好城市

  - [格拉茨](../Page/格拉茨.md "wikilink")

  - [贾巴里亚](../Page/贾巴里亚.md "wikilink")

  - [加里宁格勒](../Page/加里宁格勒.md "wikilink")

  - [卡托维兹](../Page/卡托维兹.md "wikilink")

  - [摩尔曼斯克](../Page/摩尔曼斯克.md "wikilink")

  - [泰恩河畔纽卡斯尔](../Page/泰恩河畔纽卡斯尔.md "wikilink")

  - [欧登塞](../Page/欧登塞.md "wikilink")

  - [奥尔登堡](../Page/奥尔登堡.md "wikilink")

  - [圣卡洛斯](../Page/圣卡洛斯.md "wikilink")

  - [塔林](../Page/塔林.md "wikilink")

  - [天津](../Page/天津.md "wikilink")

  - [西安](../Page/西安.md "wikilink")

  - [茲林](../Page/茲林.md "wikilink")

## 政治

市議會有39個席次。左派政黨[工黨和](../Page/工黨_\(荷蘭\).md "wikilink")[綠色左派是最大黨](../Page/綠色左派.md "wikilink")。2002年選舉結束後，他們與[基督教民主黨](../Page/基督教民主黨_\(荷蘭\).md "wikilink")、[自由民主人民黨組成聯盟](../Page/自由民主人民黨.md "wikilink")。2006年選舉結束後，三個左派政黨（工黨、綠色左派和[社會黨](../Page/社會黨_\(荷蘭\).md "wikilink")）在4月26日組成新政治聯盟。

## 工業

目前CSM（Centrale Suiker
Maatschappij）在格羅寧根有間[製糖](../Page/食糖.md "wikilink")[工廠](../Page/工廠.md "wikilink")，每年的產量約300,000[公噸](../Page/公噸.md "wikilink")。

## 運輸

格羅寧根被稱為「世界單車之都」，因為市內57%的旅遊是以單車為工具。\[1\]格羅寧根有著廣泛的單車網路、良好的公共運輸和市中心大範圍的步行區，十分適合想遠離汽車的人士。在一個義大利節目的調查中\[2\]，認為格羅寧根對於單車的使用是義大利值得的仿效對象。

### 鐵路

格羅寧根設有三個車站：

  - [格羅寧根火車站](../Page/格羅寧根火車站.md "wikilink")（Groningen railway station）
  - [格羅寧根歐羅巴公園火車站](../Page/格羅寧根歐羅巴公園火車站.md "wikilink")（Groningen
    Europapark railway station）
  - [格羅寧根北火車站](../Page/格羅寧根北火車站.md "wikilink")（Groningen Noord railway
    station）

主車站（由[荷蘭鐵路和](../Page/荷蘭鐵路.md "wikilink")[爱瑞发營運](../Page/爱瑞发.md "wikilink")）可通往荷蘭大多數的主要城市。

直達地有：

  - [茲沃勒](../Page/茲沃勒.md "wikilink")、[阿默斯福特](../Page/阿默斯福特.md "wikilink")、[希爾弗瑟姆](../Page/希爾弗瑟姆.md "wikilink")（Hilversum）、[阿姆斯特丹南方與](../Page/阿姆斯特丹.md "wikilink")[史基浦機場](../Page/史基浦機場.md "wikilink")：荷蘭鐵路

  - [茲沃勒](../Page/茲沃勒.md "wikilink")、[阿默斯福特](../Page/阿默斯福特.md "wikilink")、[烏特勒支](../Page/烏特勒支.md "wikilink")、[豪達](../Page/豪達.md "wikilink")（Gouda）與[海牙](../Page/海牙.md "wikilink")：荷蘭鐵路

  - [呂伐登](../Page/呂伐登.md "wikilink")：爱瑞发

  - [Hoogezand-Sappemeer](../Page/霍赫贊德-薩珀梅爾.md "wikilink")、[Winschoten](../Page/溫斯霍滕.md "wikilink")、德國[萊爾](../Page/萊爾.md "wikilink")（Leer）：爱瑞发

  - 和[代爾夫宰爾](../Page/代爾夫宰爾.md "wikilink")：爱瑞发

  - 爱瑞发計畫在2009年至2010年開始營運往[Veendam的路線](../Page/芬丹.md "wikilink")。

### 道路

[A28高速公路連結格羅寧根與](../Page/A28高速公路_\(荷蘭\).md "wikilink")[烏特勒支](../Page/烏特勒支.md "wikilink")（途中經由[茲沃勒和](../Page/茲沃勒.md "wikilink")[阿默斯福特](../Page/阿默斯福特.md "wikilink")）。[A7高速公路往西南方則通往](../Page/A7高速公路_\(荷蘭\).md "wikilink")[菲士蘭省和](../Page/菲士蘭省.md "wikilink")[阿姆斯特丹](../Page/阿姆斯特丹.md "wikilink")，往東則可到達[不萊梅](../Page/不萊梅.md "wikilink")。

### 公車

爱瑞发經營數條市區公車及都會公車。主要路線有：

  - 3: -市中心-主車站-Vinkhuizen
  - 6: Beijum-市中心-主車站-Hoornsemeer
  - 11: Zernike-市中心-主車站
  - 33/39/133:格羅寧根-
  - 50/51:格羅寧根-[阿森](../Page/阿森.md "wikilink")
  - 61:格羅寧根-[Bedum](../Page/贝德姆.md "wikilink")-
  - 82格羅寧根-

<!-- end list -->

  - 140:格羅寧根-[Appingedam](../Page/阿平厄丹.md "wikilink")
    -[代爾夫宰爾](../Page/代爾夫宰爾.md "wikilink")
  - 163:格羅寧根-Lauwersoog（連結通往[希蒙尼科赫](../Page/希蒙尼科赫.md "wikilink")（Schiermonnikoog）的渡輪）
  - 300/305:格羅寧根--[埃門](../Page/埃門.md "wikilink")（Emmen）
  - 301:格羅寧根-[Veendam](../Page/芬丹.md "wikilink")
  - 304/314:格羅寧根-[Drachten](../Page/德拉赫滕.md "wikilink")-[海倫芬](../Page/海倫芬.md "wikilink")
  - 315:格羅寧根-[海倫芬](../Page/海倫芬.md "wikilink")-[萊默](../Page/萊默.md "wikilink")（Lemmer）-[萊利斯塔德](../Page/萊利斯塔德.md "wikilink")（荷蘭最長的公車路線，由營運）

[Public Express](http://www.publicexpress.de)也營運格羅寧根（火車站）直達不萊梅（機場）的公車路線。

計畫再未來建造一條有軌電車路線，連結中央車站、市中心和大學集中地（Zernike）。

[格羅寧根機場](../Page/格羅寧根機場.md "wikilink")（Groningen Airport
Eelde）位於格羅寧根市中心南方10公里，提供往歐洲的預定和假日包機服務。

## 足球場館

[格羅寧根足球會的主場為](../Page/格羅寧根足球會.md "wikilink")[歐羅堡球場](../Page/歐羅堡球場.md "wikilink")（Euroborg）。歐羅堡球場在2006年1月正式啟用，可容納22,600觀眾。目前計劃要將容納人數增加到35,000至40,000人。格羅寧根足球會前主場為[奧斯特帕克球場](../Page/奧斯特帕克球場.md "wikilink")（Oosterpark
Stadion），可容納12,500人。

## 知名人物

  - [约翰·古德利克](../Page/约翰·古德利克.md "wikilink")，天文學家。
  - [丹尼爾·伯努利](../Page/丹尼爾·伯努利.md "wikilink")，物理學家。
  - [海克·卡末林·昂內斯](../Page/海克·卡末林·昂內斯.md "wikilink")，物理學家。
  - [約翰·赫伊津哈](../Page/約翰·赫伊津哈.md "wikilink")，語言學家。

## 社区

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -


<center>

<File:Goudkantoor> Groningen Netherlands.JPG <File:Dorkwerd> church1.jpg
<File:Kerk> te Engelbert.jpg <File:Groningen> - De Jonge Held.jpg
<File:Kerk> Middelbert.jpg <File:Noordermolen> Noorddijk.jpg
<File:Wilhelmina> Noorderhoogebrug.jpg <File::Tasmantoren> 20100519.jpg
<File:20090728> brug Hoendiep Hoogkerk-Vierverlaten Gn NL.jpg

</center>




## 注釋

[Category:格罗宁根省城市](../Category/格罗宁根省城市.md "wikilink")

1.
2.