《**魔法提琴手**》（）是[渡邊道明創作的奇幻冒險漫畫](../Page/渡邊道明.md "wikilink")。以及根據原作衍生出的[電視動畫](../Page/電視動畫.md "wikilink")、[動畫電影作品](../Page/動畫電影.md "wikilink")。

漫畫在「[月刊少年GANGAN](../Page/月刊少年GANGAN.md "wikilink")」（[艾尼克斯](../Page/史克威爾艾尼克斯.md "wikilink")）上，從1991年連載到2001年，單行本全37集。於1996年上映電影，接著電視動畫於1996年放映至1997年（全25話）。

## 概要

## 故事

主角的**[哈梅爾](../Page/花衣魔笛手.md "wikilink")**是個帶著大型[小提琴四處旅行的勇者](../Page/小提琴.md "wikilink")，但他其實是被封印在[潘朵拉的箱子的魔王的兒子](../Page/潘朵拉.md "wikilink")。他的小提琴具有特殊力量，能夠用音樂打倒魔族。在某個村莊哈梅爾與孤兒的村姑**芬兒特**相遇，將她從村子裡帶了出來。哈梅爾旅行的目的是為了前往魔王所居住的「北之都」。

在旅程途中，哈梅爾的青梅竹馬曁永遠的勁敵鋼琴手**萊艾爾**，與失去國家的王子**多倫**，在魔族之中長大，原為魔王軍妖鳳團長，哈梅爾的雙胞胎妹妹**西撒**等人加入伙伴。隨著故事進行，哈梅爾與芬兒特出生的秘密被闡明，敵人加入伙伴或是我方成為敵人，交織出複雜的人際關係，一邊描寫著每個人心中的糾葛，一行人繼續著邁向魔王所在北之都的旅行。

由登場人物全部取名自樂器，可以知道這部作品各處使用了古典音樂作為主題。主角哈梅爾與萊艾爾能夠演奏音樂做為武器，或是控制他人使其戰鬥的設定，隨處可見古典音樂登場，醞釀出作品獨特的味道。

## 動畫

### 電視動畫

自1996年10月2日至1997年3月26日於[東京電視台聯播網放映](../Page/東京電視台.md "wikilink")。全25話。將原作特色的搞笑場景完全省略，成為嚴肅而灰暗的故事展開。作畫本身並不差，可說是適合不曉得原作的人觀賞。

當初本來預定要播1年的，結果半年就被[腰斬了](../Page/腰斬.md "wikilink")。據說是在音樂方面使用過多預算的關係。後期的OPED影像拿前期的東西重新編輯製作，故事的進展多處過於強硬，以及大量使用靜止畫而被形容為「不動如山」等跡象，突顯出製作的遲緩。

#### 主題歌

**片頭曲**

  -
  -
**片尾曲**

  -
  -
#### 放送清單

<table>
<tbody>
<tr class="odd">
<td><ol>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
</ol></td>
<td><dl>
<dt></dt>
<dd><dl>
<dt></dt>
<dd>
</dd>
<dd>
</dd>
<dd>
</dd>
<dd>
</dd>
<dd>
</dd>
<dd>
</dd>
<dd>
</dd>
<dd>
</dd>
<dd>
</dd>
<dd>
</dd>
<dd>
</dd>
<dd>
</dd>
</dl>
</dd>
</dl></td>
</tr>
</tbody>
</table>

### 劇場版

於1996年4月20日公開上映。

## 游戏

[艾尼克斯](../Page/艾尼克斯.md "wikilink")1995年9月29日在[超級任天堂發行了同名動作遊戲](../Page/超級任天堂.md "wikilink")。

## 外部連結

  - [](http://ouendan.cocolog-nifty.com/blog/cat1810128/)

[Category:月刊少年GANGAN連載作品](../Category/月刊少年GANGAN連載作品.md "wikilink")
[Category:YOUNG GANGAN](../Category/YOUNG_GANGAN.md "wikilink")
[Category:奇幻漫畫](../Category/奇幻漫畫.md "wikilink")
[Category:少年漫畫](../Category/少年漫畫.md "wikilink")
[Category:音樂漫畫](../Category/音樂漫畫.md "wikilink")
[Category:1996年日本電視動畫](../Category/1996年日本電視動畫.md "wikilink")
[Category:1996年日本劇場動畫](../Category/1996年日本劇場動畫.md "wikilink")
[Category:日本動畫公司](../Category/日本動畫公司.md "wikilink")
[Category:東京電視網動畫](../Category/東京電視網動畫.md "wikilink")
[Category:奇幻動畫](../Category/奇幻動畫.md "wikilink")
[Category:超級任天堂遊戲](../Category/超級任天堂遊戲.md "wikilink")
[Category:1995年電子遊戲](../Category/1995年電子遊戲.md "wikilink")
[Category:史克威爾艾尼克斯遊戲](../Category/史克威爾艾尼克斯遊戲.md "wikilink")
[Category:古典音樂題材作品](../Category/古典音樂題材作品.md "wikilink")
[Category:格林童话题材作品](../Category/格林童话题材作品.md "wikilink")