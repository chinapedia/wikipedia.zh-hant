[ETC_(zh-cn).svg](https://zh.wikipedia.org/wiki/File:ETC_\(zh-cn\).svg "fig:ETC_(zh-cn).svg")，显示为紫色的复合物IV可以将氧气还原为水分子。\]\]
**细胞色素*c*氧化酶**（）是一种[氧化还原酶](../Page/氧化还原酶.md "wikilink")，通用名为“细胞色素-*c*氧化酶”，系统名称为“亚铁细胞色素-*c*:氧气氧化还原酶”（）。它是一种存在于[细菌或](../Page/细菌.md "wikilink")[线粒体上的大型](../Page/线粒体.md "wikilink")[跨膜蛋白](../Page/跨膜蛋白.md "wikilink")[复合物](../Page/蛋白质复合物.md "wikilink")。由于细胞色素氧化酶是[呼吸作用](../Page/呼吸作用.md "wikilink")[电子传递链的第四个中心酶复合物](../Page/电子传递链.md "wikilink")，因此又被称为**复合物IV**（英文）。它可以接受来自四个[细胞色素c的四个电子](../Page/细胞色素c.md "wikilink")，并传递到一个[氧气分子上](../Page/氧气.md "wikilink")，将氧气转化为两个水分子。在这一进程中，它结合来自基质内的四个[质子来制造水分子](../Page/质子.md "wikilink")，同时跨膜转运四个质子，从而有助于形成跨膜的质子[电化学势能差](../Page/电化学势能.md "wikilink")，而这一势能差可以被[三磷酸腺苷合酶用于制造生物体中最基本的能量分子](../Page/三磷酸腺苷合酶.md "wikilink")[ATP](../Page/三磷酸腺苷.md "wikilink")。

## 结构

[Cytochrome_C_Oxidase_1OCC_in_Membrane_2.png](https://zh.wikipedia.org/wiki/File:Cytochrome_C_Oxidase_1OCC_in_Membrane_2.png "fig:Cytochrome_C_Oxidase_1OCC_in_Membrane_2.png")
细胞色素氧化酶复合物是一个大型[蛋白質](../Page/蛋白質.md "wikilink")，位於[粒線體內膜](../Page/粒線體.md "wikilink")（inner-membrane）上，含有多个金属[辅因子和](../Page/辅因子.md "wikilink")13个[亚基](../Page/蛋白质亚基.md "wikilink")（在哺乳动物细胞中）。其中，10个亚基是来自[细胞核](../Page/细胞核.md "wikilink")，而另外三个亚基则是在线粒体中合成。复合物还含有两个血红素、一个细胞色素a和细胞色素a<sub>3</sub>以及两个[铜中心](../Page/铜.md "wikilink")（Cu<sub>A</sub>和Cu<sub>B</sub>）。\[1\]实际上，细胞色素a<sub>3</sub>和Cu<sub>B</sub>形成了一个双核心中心，作为氧气的还原位点。[细胞色素c被呼吸链复合物IV还原后](../Page/细胞色素c.md "wikilink")，會结合到Cu<sub>A</sub>双核中心，並把一個[電子傳遞給雙合中心](../Page/電子.md "wikilink")，[细胞色素c本身則恢復氧化状态](../Page/细胞色素c.md "wikilink")（细胞色素c上的铁从+2价氧化到+3价）。被还原的Cu<sub>A</sub>双核中心再将一个电子通过细胞色素a传递给细胞色素a<sub>3</sub>-Cu<sub>B</sub>双核中心。在这一双核中心上的两个金属离子相距4.5[Å](../Page/埃格斯特朗_\(单位\).md "wikilink")，并通过一个处于完全氧化状态的[氢氧根离子相连接](../Page/氢氧根离子.md "wikilink")。

对牛细胞色素氧化酶的[结构研究显示](../Page/结构生物学.md "wikilink")，它发生了特殊的[翻译后修饰](../Page/翻译后修饰.md "wikilink")，即其244位上的[酪氨酸](../Page/酪氨酸.md "wikilink")（Tyr244）的[C](../Page/碳.md "wikilink")6原子和240位上的[组氨酸](../Page/组氨酸.md "wikilink")[Nε原子被](../Page/氮.md "wikilink")[共价连接](../Page/共价键.md "wikilink")。这一修饰作用在细胞色素a<sub>3</sub>-Cu<sub>B</sub>双核中心接受4个电子来将氧气还原为水分子的过程中发挥了重要作用。过去认为还原机制包括有一个可以导致[超氧化物形成的](../Page/超氧化物.md "wikilink")[过氧化物中间体](../Page/过氧化物.md "wikilink")。但现在普遍接受的机制是一个快速的四电子还原过程，包括迅速的氧-氧键剪切，以避免任何中间产物形成超氧化物的可能性。\[2\]

## 组装

细胞色素氧化酶是由多个亚基和辅因子组成的，必须要通过组装才能形成完成的活性分子。它的组装位点被认为接近[TOM／TIM复合体](../Page/线粒体膜转运蛋白.md "wikilink")；在这一位置，复合物中间体可以与来自[原生质中的亚基结合](../Page/原生质.md "wikilink")。血红素和辅因子被插入到亚基I和II中。亚基I和IV可以启动组装。其他亚基可以先形成亚复合物中间体，然后再与亚基I和IV结合形成完整的细胞色素氧化酶。在组装后修饰中，酶分子发生二聚化以获得有效的酶活性。二聚体是通过一个[心磷脂](../Page/心磷脂.md "wikilink")（cardiolipin）分子来连接。整个组装机制的信息已经得到大量的揭示，\[3\]
但具体过程还有待进一步的研究。

## 生物化学性质

[Complex_IV.svg](https://zh.wikipedia.org/wiki/File:Complex_IV.svg "fig:Complex_IV.svg")
细胞色素氧化酶[催化的整体反应是](../Page/酶促反应.md "wikilink")：

4 Fe<sup>2+</sup>-细胞色素*c* + 8
H<sup>+</sup><sub><span style="font-size:smaller;">进</span></sub> +
O<sub>2</sub> → 4 Fe<sup>3+</sup>-细胞色素*c* + 2 H<sub>2</sub>O + 4
H<sup>+</sup><sub><span style="font-size:smaller;">出</span></sub>

整个催化过程\[4\]
如下：首先两个电子从两个细胞色素c分子通过Cu<sub>A</sub>和细胞色素a传递到细胞色素a<sub>3</sub>-Cu<sub>B</sub>双核中心，将中心的金属还原为Fe<sup>+2</sup>和Cu<sup>+1</sup>。连接两个金属离子的氢氧根在被质子化后生成水分子而被释放，从而两个金属离子之间产生了一个空腔，这一空腔被一个氧气分子所填充。氧气分子与细胞色素a<sub>3</sub>中的铁原子结合形成铁氧结合形式（Fe<sup>+2</sup>-O<sub>2</sub>）。结合的氧很快被还原，其中一个氧原子与铁形成Fe<sup>+4</sup>=O形式；而另一个接近Cu<sub>B</sub>的氧原子接受来自Cu<sup>+1</sup>的一个电子和来自Tyr244上[羟基的一个电子和一个质子](../Page/羟基.md "wikilink")，被转化为一个氢氧根，同时Tyr244转变为酪氨酰[自由基](../Page/自由基.md "wikilink")。来自另一个细胞色素c分子的第三个电子通过相同的途径被传递到细胞色素a<sub>3</sub>-Cu<sub>B</sub>双核中心，随后这个电子和两个质子将酪氨酰自由基重新还原为酪氨酸，并将结合在Cu<sub>B</sub><sup>+2</sup>上的氢氧根转化为水分子。同样来自细胞色素c分子的第四个电子在进入细胞色素a<sub>3</sub>-Cu<sub>B</sub>双核中心后，将Fe<sup>+4</sup>=O还原为Fe<sup>+3</sup>，同时氧原子接受一个质子转变为一个氢氧根连接于细胞色素a<sub>3</sub>-Cu<sub>B</sub>中心，从而整个循环回到起始状态。整个反应过程净利用了4个还原的细胞色素c分子（提供4个电子）、4个质子（消耗8个，产生4个），将一个氧气分子还原为两个水分子。

## 抑制剂

[氰化物](../Page/氰化物.md "wikilink")、[硫化氫](../Page/硫化氫.md "wikilink")、[叠氮化合物和](../Page/叠氮化合物.md "wikilink")[一氧化碳](../Page/一氧化碳.md "wikilink")\[5\]
都能够与细胞色素氧化酶结合，并抑制其活性，从而造成细胞的“化学[窒息](../Page/窒息.md "wikilink")”。

## 相关遗传紊乱疾病

细胞色素氧化酶[基因上的缺陷](../Page/基因.md "wikilink")，包括突变，能够导致严重的常常甚至是致命的[代谢紊乱](../Page/代谢紊乱.md "wikilink")。这种紊乱通常出现在幼儿时代的早期，并且主要影响需要高能量的组织器官（脑、心脏、肌肉）。在许多分级的[线粒体疾病中](../Page/线粒体疾病.md "wikilink")，与细胞色素氧化酶组装缺陷的疾病被认为是最严重的。\[6\]

细胞色素氧化酶所造成的紊乱主要是与细胞核编码的作为组装因子的亚基蛋白上的突变相关。这些组装因子对于酶复合物的结构和功能都是非常重要的，它们参与了多个关键的进程，包括线粒体编码的亚基的[转录和](../Page/转录.md "wikilink")[翻译](../Page/翻译_\(遗传学\).md "wikilink")、对前体蛋白的剪切以及膜的插入、辅因子的生物合成以及插入。\[7\]

在病人体内发现的带有突变的组装因子有[SURF1](../Page/SURF1.md "wikilink")、[SCO1](../Page/SCO1.md "wikilink")、[SCO2](../Page/SCO2.md "wikilink")、[COX10](../Page/COX10.md "wikilink")、[COX15和](../Page/COX15.md "wikilink")[LRPPRC](../Page/LRPPRC.md "wikilink")。这些突变能够导致亚复合物的组装、铜的运输或转录调控的功能发生改变。每一个基因突变都与一种特定疾病相关，其中一些还与多种紊乱疾病相关。由于基因突变引起的细胞色素氧化酶组装缺陷相关的紊乱症包括[Leigh综合征](../Page/Leigh综合征.md "wikilink")、[心肌病](../Page/心肌病.md "wikilink")（cardiomyopathy）、[脑白质病](../Page/脑白质病.md "wikilink")（leukodystrophy）、[贫血和](../Page/贫血.md "wikilink")[感觉神经性耳聋](../Page/感觉神经性耳聋.md "wikilink")。

## 参考资料

## 外部链接

  - [The Cytochrome Oxidase home
    page](https://web.archive.org/web/20021013113802/http://www-bioc.rice.edu/~graham/CcO.html)
    at [Rice University](../Page/Rice_University.md "wikilink")

  - [Interactive Molecular model of cytochrome c
    oxidase](https://web.archive.org/web/20090111220321/http://www2.ufp.pt/~pedros/anim/2frame-iven.htm)
    (Requires [MDL
    Chime](https://web.archive.org/web/20060320002451/http://www.mdl.com/products/framework/chime/))

  -
  -
[Category:细胞呼吸](../Category/细胞呼吸.md "wikilink") [Category:EC
1.9.3](../Category/EC_1.9.3.md "wikilink")
[Category:血红素蛋白](../Category/血红素蛋白.md "wikilink")
[Category:内在膜蛋白](../Category/内在膜蛋白.md "wikilink")

1.  Tsukihara T., Aoyama H., Yamashita E., Tomizaki T., Yamaguchi H.,
    Shinzawa-Itoh K., Nakashima R., Yaono R., Yoshikawa
    S.（1995）Structures of metal sites of oxidized bovine heart
    cytochrome c oxidase at 2.8 Å. Science 269, 1069-1074
2.  Voet D., Voet JG (2004) Biochemistry, 3rd Edition. John Wiley &
    Sons, pps. 818-820
3.
4.
5.
6.  Pecina, P., Houstkova, H., Hansikova, H., Zeman, J., Houstek, J.
    (2004). Genetic Defects of Cytocrhome *c* Oxidase Assembly. Physiol.
    Res. **53**(Suppl. 1): S213-S223.
7.  Zee, J.M., and Glerum, D.M. (2006). Defects in cytochrome oxidase
    assembly in humans: lessons from yeast. Biochem. Cell Biol. **84**:
    859-869.