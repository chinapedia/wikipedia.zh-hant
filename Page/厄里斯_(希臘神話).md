**厄里斯**（[希腊文](../Page/希腊文.md "wikilink")：****；[英文](../Page/英文.md "wikilink")：****，意為“不和”）是[希腊神话中的不和女神](../Page/希腊神话.md "wikilink")，她在罗马神话中被稱为**Discordia**。[荷马和](../Page/荷马.md "wikilink")[赫西俄德对她有不同的描写](../Page/赫西俄德.md "wikilink")：在《[伊利亚特](../Page/伊利亚特.md "wikilink")》中她是战神[阿瑞斯的姊妹](../Page/阿瑞斯.md "wikilink")，因此也是[宙斯和](../Page/宙斯.md "wikilink")[赫拉的女儿](../Page/赫拉.md "wikilink")，她在敌对双方间散布着痛苦和仇恨；在《[神谱](../Page/神谱.md "wikilink")》和《[工作与时日](../Page/工作与时日.md "wikilink")》中她则是[倪克斯和](../Page/倪克斯.md "wikilink")[克洛诺斯的女儿](../Page/克洛诺斯.md "wikilink")。

她因为没有被邀请参加[珀琉斯和](../Page/珀琉斯.md "wikilink")[忒提斯的婚礼而怀恨在心](../Page/忒提斯.md "wikilink")，抛下了一个刻有“献给最美的”（Ἡ
καλὴ
λαϐέτω）的[金苹果](../Page/金蘋果事件.md "wikilink")，引起了一連串的紛爭與混亂。（见[特洛伊战争](../Page/特洛伊战争.md "wikilink")）

## 註釋

<references/>

[E](../Category/希臘女神.md "wikilink") [E](../Category/邪恶神.md "wikilink")
[E](../Category/混沌神.md "wikilink")
[Category:宙斯的後裔](../Category/宙斯的後裔.md "wikilink")
[Category:詭計神](../Category/詭計神.md "wikilink")