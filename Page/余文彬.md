**余文彬**（），[臺灣](../Page/臺灣.md "wikilink")[棒球](../Page/棒球.md "wikilink")[選手](../Page/選手.md "wikilink")、[教練](../Page/教練.md "wikilink")，守備位置為[投手](../Page/投手.md "wikilink")，先後效力於[日本職棒](../Page/日本職棒.md "wikilink")[歐力士藍浪隊與](../Page/歐力士野牛.md "wikilink")[中華職棒](../Page/中華職棒.md "wikilink")[興農牛隊](../Page/興農牛隊.md "wikilink")，目前擔任[中華職棒](../Page/中華職棒.md "wikilink")[富邦悍將投手教練](../Page/富邦悍將.md "wikilink")。

## 經歷

  - [花蓮縣北富國小少棒隊](../Page/花蓮縣.md "wikilink")
  - [台北市華興中學青少棒隊](../Page/台北市.md "wikilink")
  - [台北市華興中學青棒隊](../Page/台北市.md "wikilink")
  - [文化大學棒球隊](../Page/文化大學.md "wikilink")（美孚巨人）
  - [日本職棒](../Page/日本職棒.md "wikilink")[歐力士野牛隊](../Page/歐力士野牛.md "wikilink")（2002年）
  - [中華職棒](../Page/中華職棒.md "wikilink")[興農牛隊](../Page/興農牛.md "wikilink")（2003年－2011年12月5日）
  - [中華職棒](../Page/中華職棒.md "wikilink")[興農牛隊牛棚教練兼球員](../Page/興農牛.md "wikilink")（2011年12月5日—2012年8月15日）
  - [中華職棒](../Page/中華職棒.md "wikilink")[興農牛隊投手教練兼球員](../Page/興農牛.md "wikilink")（2012年8月15日—2012年12月16日）
  - [中華職棒](../Page/中華職棒.md "wikilink")[義大犀牛隊投手教練兼球員](../Page/義大犀牛.md "wikilink")
    (2012年12月17日—2013年1月)
  - [中華職棒](../Page/中華職棒.md "wikilink")[義大犀牛隊投手教練](../Page/義大犀牛.md "wikilink")
    (2013年1月—)

## 職棒生涯成績

| 年度    | 球隊                               | 出賽  | 勝投 | 敗投 | 中繼 | 救援 | 完投 | 完封 | 四死  | 三振  | 責失  | 投球局數  | 防禦率    |
| ----- | -------------------------------- | --- | -- | -- | -- | -- | -- | -- | --- | --- | --- | ----- | ------ |
| 2003年 | [興農牛](../Page/興農牛.md "wikilink") | 43  | 6  | 1  | 0  | 3  | 0  | 0  | 50  | 53  | 24  | 97.2  | 2.21   |
| 2004年 | [興農牛](../Page/興農牛.md "wikilink") | 42  | 2  | 4  | 0  | 4  | 0  | 0  | 48  | 48  | 42  | 86.2  | 4.36   |
| 2005年 | [興農牛](../Page/興農牛.md "wikilink") | 24  | 3  | 3  | 7  | 0  | 0  | 0  | 20  | 35  | 15  | 44.1  | 3.05   |
| 2006年 | [興農牛](../Page/興農牛.md "wikilink") | 39  | 2  | 4  | 8  | 4  | 0  | 0  | 31  | 39  | 26  | 63.2  | 3.67   |
| 2007年 | [興農牛](../Page/興農牛.md "wikilink") | 36  | 3  | 2  | 9  | 0  | 0  | 0  | 29  | 40  | 35  | 59.2  | 5.28   |
| 2008年 | [興農牛](../Page/興農牛.md "wikilink") | 24  | 7  | 6  | 1  | 0  | 0  | 0  | 39  | 51  | 44  | 100.0 | 3.96   |
| 2009年 | [興農牛](../Page/興農牛.md "wikilink") | 40  | 6  | 4  | 4  | 1  | 0  | 0  | 35  | 50  | 50  | 104.0 | 4.326  |
| 2010年 | [興農牛](../Page/興農牛.md "wikilink") | 12  | 0  | 0  | 1  | 0  | 0  | 0  | 3   | 8   | 10  | 59.2  | 4.821  |
| 2011年 | [興農牛](../Page/興農牛.md "wikilink") | 31  | 2  | 4  | 1  | 0  | 0  | 0  | 25  | 38  | 27  | 68.1  | 3.556  |
| 2012年 | [興農牛](../Page/興農牛.md "wikilink") | 11  | 1  | 2  | 1  | 0  | 0  | 0  | 2   | 7   | 13  | 8.2   | 13.500 |
| 合計    | 10年                              | 302 | 32 | 30 | 32 | 12 | 0  | 0  | 282 | 369 | 286 | 651.2 | 3.949  |

## 特殊事蹟

## 外部連結

[Y](../Category/1978年出生.md "wikilink")
[Y](../Category/在世人物.md "wikilink")
[Y](../Category/台灣棒球選手.md "wikilink")
[Y](../Category/日本職棒外籍球員.md "wikilink")
[Y](../Category/中華青棒隊球員.md "wikilink")
[Y](../Category/中華成棒隊球員.md "wikilink")
[Y](../Category/歐力士野牛隊球員.md "wikilink")
[Y](../Category/興農牛隊球員.md "wikilink")
[Y](../Category/義大犀牛隊球員.md "wikilink")
[Y](../Category/台灣旅日棒球選手.md "wikilink")
[Y](../Category/台灣旅外歸國棒球選手.md "wikilink")
[Y](../Category/中國文化大學校友.md "wikilink")
[Y](../Category/阿美族人.md "wikilink")
[Y](../Category/花蓮人.md "wikilink")
[Y](../Category/光復人.md "wikilink")
[Y](../Category/余姓.md "wikilink")