**田东县**在[中国](../Page/中国.md "wikilink")[广西壮族自治区西部](../Page/广西壮族自治区.md "wikilink")、[右江中游](../Page/右江.md "wikilink")，是[百色市所辖的一个](../Page/百色市.md "wikilink")[县](../Page/县.md "wikilink")。

## 历史

[唐开元年间](../Page/唐.md "wikilink")（713～741年）唐王朝在今桂西地区置田州，辖都救（在今平果县）、横山（在今田东县）、武隆（在今田阳县、右江区）、惠佳（在今右江区）、如赖（在今田林县）5县，治所于都救县。[清光绪元年](../Page/清.md "wikilink")（1875年），田州改土归流，在今田东地置恩隆县，隶百色直隶厅。民国23年（1934年），更名为田东县。

## 资源

田东县目前已探明的可供开采的矿产资源有：石油、天然气、铝土矿、膨润土、褐煤、锰矿、辉绿岩、硅、铁、钛、锑、黄金等24种。

## 行政区划

田东县2003年辖11个镇、1个乡、1个民族乡：\[1\] 。

## 交通

  - 横越境内。

## 外部链接

  - [田东生活网](http://www.td776.com)
  - [百色家园Gxbs.com.cn
    最方便的分类信息门户网站](https://archive.is/20090605083210/http://www.gxbs.com.cn/)
  - [田东网咯](http://www.tdwg.net)

## 参考资料

[田东县](../Category/田东县.md "wikilink")
[县](../Category/百色区县市.md "wikilink")
[百色](../Category/广西县份.md "wikilink")
[Category:国家级贫困县](../Category/国家级贫困县.md "wikilink")

1.