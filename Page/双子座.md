**双子座**（，[天文符号](../Page/天文符号.md "wikilink")：♊）[黄道带](../Page/黄道带.md "wikilink")[星座之一](../Page/星座.md "wikilink")，面积513.76平方度，占全天面积的1.245%，在全天88个星座中，[面积排行第三十位](../Page/星座面积列表.md "wikilink")。双子座中亮于5.5等的[恒星有](../Page/恒星.md "wikilink")47颗，最亮星为[北河三](../Page/北河三.md "wikilink")（双子座β），[视星等为](../Page/视星等.md "wikilink")1.14。每年1月5日子夜双子座中心经过[上中天](../Page/中天_\(天文学\).md "wikilink")。

1781年，[英国](../Page/英国.md "wikilink")[天文学家](../Page/天文学家.md "wikilink")[威廉·赫歇尔和他的妹妹](../Page/威廉·赫歇尔.md "wikilink")[卡罗琳·赫歇尔在双子座H附近发现](../Page/卡罗琳·赫歇尔.md "wikilink")[天王星](../Page/天王星.md "wikilink")。1930年，[美国天文学家](../Page/美国.md "wikilink")[汤博在双子座δ附近發现](../Page/克莱德·汤博.md "wikilink")[冥王星](../Page/冥王星.md "wikilink")。美国的[双子星座计划就是以双子座来命名的](../Page/双子星座计划.md "wikilink")。

## 特征

[GeminiCC.jpg](https://zh.wikipedia.org/wiki/File:GeminiCC.jpg "fig:GeminiCC.jpg")\]
双子座的西边是[金牛座](../Page/金牛座.md "wikilink")，东边是比较暗淡的[巨蟹座](../Page/巨蟹座.md "wikilink")。[御夫座和非常不明显的](../Page/御夫座.md "wikilink")[天猫座位于它的北边](../Page/天猫座.md "wikilink")，[麒麟座和](../Page/麒麟座.md "wikilink")[小犬座位于它的南边](../Page/小犬座.md "wikilink")。

双子座有两颗非常亮的星—北河三(雙子座β)和北河二(雙子座α)。其它的星都比较暗，只有γ是在城市灯光下也能被看到的。但在远离灯光污染的地方，可以看到稀薄的[银河从双子座西部经过](../Page/银河.md "wikilink")。

北河三和金牛座的[毕宿五](../Page/毕宿五.md "wikilink")、御夫座的[五车二](../Page/五车二.md "wikilink")、小犬座的[南河三](../Page/南河三.md "wikilink")、[大犬座的](../Page/大犬座.md "wikilink")[天狼星](../Page/天狼星.md "wikilink")、[猎户座的](../Page/猎户座.md "wikilink")[参宿七组成](../Page/参宿七.md "wikilink")[冬季六边形](../Page/冬季六边形.md "wikilink")。

## 神话

[Costellazione_dei_Gemelli.jpg](https://zh.wikipedia.org/wiki/File:Costellazione_dei_Gemelli.jpg "fig:Costellazione_dei_Gemelli.jpg")筆下的雙子座\]\]
雙子座代表的是[希臘神話中的天神](../Page/希臘神話.md "wikilink")[宙斯與](../Page/宙斯.md "wikilink")[斯巴達王后](../Page/斯巴達.md "wikilink")[勒達所生的孿生子即](../Page/勒達.md "wikilink")([雙胞胎](../Page/雙胞胎.md "wikilink"))[卡斯托耳和](../Page/卡斯托耳.md "wikilink")[波魯克斯](../Page/波魯克斯.md "wikilink")（合称[狄俄斯库里兄弟](../Page/狄俄斯库里兄弟.md "wikilink")），两人一生充满无数的英雄壮举。但他们因和[伊達斯和](../Page/伊達斯.md "wikilink")[林叩斯分配战利品时产生矛盾](../Page/林叩斯_\(阿耳戈船英雄\).md "wikilink")，互相反目成仇并开始决斗，结果林叩斯被卡斯托耳杀死，卡斯托耳被伊达斯杀死，伊达斯又被波魯克斯杀死。
波魯克斯向宙斯哀求如果能让卡斯托耳复活，他宁愿放弃自己的不死之身。宙斯被兄弟俩的友爱精神所感动，将他们提升到天界，成为双子座

## 恒星

  - [北河三](../Page/北河三.md "wikilink")(双子座β)，全天第17亮星，[视星等](../Page/视星等.md "wikilink")1.14，红巨星，距离为35[光年](../Page/光年.md "wikilink")。
  - [北河二](../Page/北河二.md "wikilink")(双子座α)，是第一颗经过[天文观测确认的物理](../Page/天文.md "wikilink")[双星](../Page/双星.md "wikilink")，复合星等1.58，全天第22亮星，距离为31光年。子星αA的视星等是1.9等，子星αB是2.9等。此外还有一个视星等为8.8等的伴星αC，而且这三颗星分别都是分光双星。因此北河二是一颗六合星。
  - [井宿一](../Page/井宿一.md "wikilink")(双子座μ)，[不规则变星](../Page/不规则变星.md "wikilink")，亮度介于2.75-3.02之间，距离为160光年。
  - [井宿七](../Page/井宿七.md "wikilink")(双子座ζ)，[造父变星](../Page/造父变星.md "wikilink")，视星等最大为3.62，最小为4.18，变光周期为10天3小时37分。
  - 双子座R，[长周期变星](../Page/变星.md "wikilink")，亮度介于6.0-14.0之间，变光周期为369.91日。

### 重要主星表

| [拜耳命名法](../Page/拜耳命名法.md "wikilink") | 中國星官 | 英文名字            | 英文含意      | 視星等  |
| ------------------------------------ | ---- | --------------- | --------- | ---- |
| 双子座α                                 | 北河二  | Castor          | 马术师/卡斯托耳  | 1.58 |
| 双子座β                                 | 北河三  | Pollux          | 拳术师/波吕刻丢斯 | 1.14 |
| 双子座γ                                 | 井宿三  | Alhena          | 骆驼身上的烙印   | 1.93 |
| 双子座δ                                 | 天樽二  | Wasat           | 中间        | 3.53 |
| 双子座ε                                 | 井宿五  | Mebsuta         | 狮子伸出的爪子   | 2.98 |
| 双子座ζ                                 | 井宿七  | Mekbuda         | 狮子收起的爪子   | 3.79 |
| 双子座η                                 | 鉞    | Tejat Prior     | 前脚        | 3.28 |
| 双子座θ                                 | 五诸侯一 | Nageba          | 出身名门      | 3.60 |
| 双子座ι                                 | 五诸侯三 | \-              | \-        | 3.79 |
| 双子座κ                                 | 积薪   | Al Kirkab       | 葡萄园的农夫    | 3.57 |
| 双子座λ                                 | 井宿八  | Kebash          | 羊群        | 3.58 |
| 双子座μ                                 | 井宿一  | Tejat Posterior | 后脚        | 2.81 |
| 双子座ν                                 | 井宿二  | \-              | \-        | 4.51 |
| 双子座ξ                                 | 井宿四  | Alzir           | 花蕾        | 3.36 |
| 双子座ο                                 | 北河增二 | \-              | \-        | 4.85 |
| 双子座π                                 | \-   | \-              | \-        | 5.10 |
| 双子座ρ                                 | 北河一  | \-              | \-        | 4.18 |
| 双子座σ                                 | 北河增四 | \-              | \-        | 4.20 |
| 双子座τ                                 | 五诸侯二 | \-              | \-        | 4.41 |
| 双子座υ                                 | 五诸侯四 | \-              | \-        | 4.06 |
| 双子座φ                                 | 五诸侯五 | \-              | \-        | 4.97 |
| 双子座χ                                 | 爟增三  | \-              | \-        | 4.90 |
| 双子座ω                                 | 天樽三  | \-              | \-        | 5.18 |
| 双子座1                                 | 司怪二  | Propus          | 前脚        | 4.75 |
| 双子座36                                | 井宿六  | \-              | \-        | 5.25 |
| 双子座57                                | 天樽一  | \-              | \-        | 5.00 |
| HP34033                              | 四渎一  | \-              | \-        | 5.16 |

## [深空天体](../Page/深空天体.md "wikilink")

[Ngc2392.jpg](https://zh.wikipedia.org/wiki/File:Ngc2392.jpg "fig:Ngc2392.jpg")

  - [M35](../Page/M35.md "wikilink")，[疏散星团](../Page/疏散星团.md "wikilink")，位于双子座η西北，离[地球大约](../Page/地球.md "wikilink")2800[光年](../Page/光年.md "wikilink")，亮度为5.3等。
  - [爱斯基摩星云](../Page/爱斯基摩星云.md "wikilink")，[行星状星云](../Page/行星状星云.md "wikilink")，距离地球约2900光年，由[英国](../Page/英国.md "wikilink")[天文学家](../Page/天文学家.md "wikilink")[威廉·赫歇尔在](../Page/威廉·赫歇尔.md "wikilink")1787年发现，视星等为9.9。
  - [蛇妖星雲](../Page/蛇妖星雲.md "wikilink")，[行星状星云](../Page/行星状星云.md "wikilink")，距离地球约1510光年。

## 中国星官

中国古代传统中双子座天区属于[井宿的井](../Page/井宿.md "wikilink")、天樽、五诸侯、北河、鉞、积薪、四渎和[觜宿的司怪等](../Page/觜宿.md "wikilink")[星官](../Page/星官.md "wikilink")。

  - 司怪 Deity in Charge of Monsters(觜4)：双子座1
  - 井 Well(井8)：双子座μ、ν、γ、ξ、ε、36、ζ、λ
  - 北河 North River(井3)：双子座ρ、α、β
  - 天樽 Celestial Wine Cup(井3)：双子座57、δ、ω
  - 五诸侯 Five Feudal Kings(井5)：双子座θ、τ、ι、υ、φ
  - 鉞 Battle Axe(井1)：双子座η
  - 积薪 Pile of Firewood(井1)：双子座κ
  - 四渎 Four Channels(井4)：HP34033

## 参考资料

  - [AEEA天文教育資訊網 觜宿天區](http://aeea.nmns.edu.tw/2006/0607/ap060714.html)
  - [AEEA天文教育資訊網 井宿天區](http://aeea.nmns.edu.tw/2006/0607/ap060716.html)

## 外部連結

  - [The Deep Photographic Guide to the Constellations:
    Gemini](http://www.allthesky.com/constellations/gemini/)
  - [The clickable Gemini](http://160.114.99.91/astrojan/gemini.htm)
  - [wikisky.org: Gemini
    constellation](http://wikisky.org/?object=Gemini&zoom=2)
  - [Star Tales –
    Gemini](http://www.ianridpath.com/startales/gemini.htm)

[Shuang1](../Category/星座.md "wikilink")
[\*](../Category/雙子座.md "wikilink")
[Category:托勒密星座](../Category/托勒密星座.md "wikilink")
[Category:黃道星座](../Category/黃道星座.md "wikilink")