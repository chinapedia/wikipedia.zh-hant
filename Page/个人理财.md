**个人理财**是指应用[金融学原理](../Page/金融学.md "wikilink")，指导个人或家庭的财务[决策](../Page/决策.md "wikilink")，例如根据财务状况建立合理的个人财务规划、参与[投资活动等等](../Page/投资.md "wikilink")。包括：个人[收](../Page/收入.md "wikilink")[支](../Page/費用.md "wikilink")、[資產](../Page/資產.md "wikilink")、[债务](../Page/债务.md "wikilink")、[税務](../Page/稅.md "wikilink")、[保險等](../Page/保險.md "wikilink")。

## 個人理財規劃

個人理財首重個人財務規劃，包含定期的檢視及評估，通常包含以下5個步驟：

1.  評估目前财政狀況
2.  明确投资期限并目標設定
3.  建立适合自己的投资方案計畫
4.  執行
5.  檢視及重新評估

## 常見的財務目標

  - 人生支出项：
      - [生活费](../Page/生活费.md "wikilink")：[衣](../Page/服装.md "wikilink")、[食](../Page/食物.md "wikilink")、[住](../Page/住宅.md "wikilink")、[行](../Page/交通.md "wikilink")、[育](../Page/育.md "wikilink")、[樂](../Page/樂.md "wikilink")
      - [婚姻](../Page/婚姻.md "wikilink")
      - [抚育子女](../Page/抚育.md "wikilink")（包括抚养费和教育费）
      - [赡养父母](../Page/贍養費.md "wikilink")
      - [退休后的](../Page/退休.md "wikilink")[养老金](../Page/退休金.md "wikilink")
      - [纳税与](../Page/稅.md "wikilink")[節稅](../Page/節稅.md "wikilink")
      - [医疗](../Page/医疗卫生.md "wikilink")
  - [不動產](../Page/不動產.md "wikilink")（如：[房屋](../Page/房屋.md "wikilink")、[土地](../Page/土地.md "wikilink")）
  - [動產](../Page/動產.md "wikilink")（如：[汽车](../Page/汽车.md "wikilink")、[机车](../Page/摩托车.md "wikilink")）
  - [旅行](../Page/旅行.md "wikilink")（如：國內、國外）
  - 非经常性开支（以備不時之需）
  - 清償[债务](../Page/债务.md "wikilink")：[房屋按揭](../Page/抵押.md "wikilink")、[信用卡債務](../Page/卡奴.md "wikilink")、[學生貸款](../Page/助学贷款.md "wikilink")

[缩略图](https://zh.wikipedia.org/wiki/File:Putting_money_into_a_piggybank.jpg "fig:缩略图")

### KISS法则

KISS法则（**K**eep **I**t **S**imple & **S**tupid）：高风险工具（如股票等）的配置百分比 = 人均寿命
- 投资者年龄

  - 例如：-{zh-hans:现在;zh-hk:現時;zh-tw:現在;}-人的人均寿命约为80岁，若你现在30岁，则可用于高风险投资的资金占全部自有资金的比例为：
    \(80-30=50\%\)

### 财务自由度

`财务自由度 =（目前`[`净资产`](../Page/資產.md "wikilink")`×`[`-{zh-hans:投资回报率;``
 ``zh-hant:投資報酬率;}-`](../Page/投資報酬率.md "wikilink")`）/目前的年支出`

` `\(F=(S*N*R)/C\)

` `\(Y-C=S\)

` `\(S/Y=F/(F+N*R)\)

` `<small>**`注：`**`财务自由度的理想目标值为１`\[1\]
\(F :\)` 财务自由度`
\(S :\)` 年储蓄总额`
\(N :\)` 总工作年数`
\(R :\)` 投资回报率`
\(C :\)` 年支出总额`
\(Y :\)` 年所得总额`
\(S/Y :\)` 储蓄率`</small>

## 理财处理

对于自己的财产应进行合理安排。

  - 风险低，收益有限
      - [現金](../Page/現金.md "wikilink")、[存款](../Page/存款.md "wikilink")
      - [保險](../Page/保險.md "wikilink")、[年金](../Page/企业年金.md "wikilink")
  - 风险中等，占用资金多，收益可观，需专业知识
      - [邮票](../Page/邮票.md "wikilink")、[鈔票](../Page/鈔票.md "wikilink")、[錢幣](../Page/錢幣.md "wikilink")、[磁卡](../Page/电话卡.md "wikilink")
      - [古董](../Page/文物.md "wikilink")、[字画](../Page/书画.md "wikilink")、[艺术品](../Page/艺术品.md "wikilink")
      - [房地产](../Page/房地产.md "wikilink")（[住宅](../Page/住宅.md "wikilink")、[商铺](../Page/商店.md "wikilink")、[写字楼](../Page/辦公室.md "wikilink")、[廠房](../Page/工廠.md "wikilink")、[倉庫](../Page/倉庫.md "wikilink")、[码头](../Page/码头.md "wikilink")、[高速公路](../Page/高速公路.md "wikilink")）
  - 风险高，收益丰厚，变现能力强
      - [创业](../Page/创业公司.md "wikilink")、[投资](../Page/投资.md "wikilink")
      - [外汇](../Page/外汇.md "wikilink")
      - [债券](../Page/债券.md "wikilink")（[国债](../Page/国债.md "wikilink")、[地方政府债券](../Page/地方政府债券.md "wikilink")、[金融债券](../Page/金融债券.md "wikilink")、[公司债券](../Page/公司债券.md "wikilink")、[可转换债券](../Page/可转换债券.md "wikilink")）
      - [股票](../Page/股票.md "wikilink")
      - [基金](../Page/共同基金.md "wikilink")（[貨幣基金](../Page/貨幣基金.md "wikilink")、[证券投资基金](../Page/证券投资基金.md "wikilink")、[信托基金](../Page/信托基金.md "wikilink")、[不動產投資信託](../Page/不動產投資信託.md "wikilink")、[ETF](../Page/ETF.md "wikilink")[指数基金](../Page/指数基金.md "wikilink")）
      - [貴價重金屬](../Page/貴金屬.md "wikilink")（如：[黃金](../Page/金.md "wikilink")、[白金](../Page/铂.md "wikilink")、[白銀](../Page/銀.md "wikilink")、[纸黄金](../Page/特别提款权.md "wikilink")）
      - [期货](../Page/期货.md "wikilink")、[期权](../Page/期权.md "wikilink")、[金融衍生品](../Page/金融衍生工具.md "wikilink")

## 参考资料

{{-}}

[fr:Placement](../Page/fr:Placement.md "wikilink")
[nl:Belegging](../Page/nl:Belegging.md "wikilink")

[個人理財](../Category/個人理財.md "wikilink")
[Category:财务管理](../Category/财务管理.md "wikilink")

1.  [`财富人生：财务自由度最好大于1`](http://blog.sina.com.cn/s/blog_4cd035a101009v2j.html)