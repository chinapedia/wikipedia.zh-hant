[National_Days_map.png](https://zh.wikipedia.org/wiki/File:National_Days_map.png "fig:National_Days_map.png")}}
\]\]

**独立日**是某些[國家的周年節慶](../Page/國家.md "wikilink")，通常会庆祝结束[殖民地状态或外国的控制](../Page/殖民地.md "wikilink")，而获得[獨立](../Page/獨立.md "wikilink")。大部分国家的独立日也是[國慶日](../Page/國慶日.md "wikilink")。

## 獨立日列表

<table>
<colgroup>
<col style="width: 18%" />
<col style="width: 12%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="header">
<th><p>国家</p></th>
<th><p>日期</p></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p><a href="../Page/8月19日.md" title="wikilink">8月19日</a></p></td>
<td><p><a href="../Page/1919年.md" title="wikilink">1919年从</a><a href="../Page/英国.md" title="wikilink">英国手中夺回外交事务权从而独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/11月28日.md" title="wikilink">11月28日</a></p></td>
<td><p><a href="../Page/1912年.md" title="wikilink">1912年由</a><a href="../Page/伊斯梅尔·捷马利.md" title="wikilink">伊斯梅尔·捷马利宣布独立</a>，结束了<a href="../Page/奥斯曼帝国.md" title="wikilink">奥斯曼帝国对阿尔巴尼亚长达</a>5年的统治。亦被称作“Dita e Pavarësisë”。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/7月5日.md" title="wikilink">7月5日</a></p></td>
<td><p><a href="../Page/1962年.md" title="wikilink">1962年脱离</a><a href="../Page/法国.md" title="wikilink">法国独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/11月11日.md" title="wikilink">11月11日</a></p></td>
<td><p><a href="../Page/1975年.md" title="wikilink">1975年脱离</a><a href="../Page/葡萄牙.md" title="wikilink">葡萄牙独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/11月1日.md" title="wikilink">11月1日</a></p></td>
<td><p><a href="../Page/1981年.md" title="wikilink">1981年脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/7月9日.md" title="wikilink">7月9日</a></p></td>
<td><p><a href="../Page/1816年.md" title="wikilink">1816年脱离</a><a href="../Page/西班牙.md" title="wikilink">西班牙独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/9月21日.md" title="wikilink">9月21日</a></p></td>
<td><p><a href="../Page/1991年.md" title="wikilink">1991年脱离</a><a href="../Page/苏联.md" title="wikilink">苏联独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/10月26日.md" title="wikilink">10月26日</a></p></td>
<td><p><a href="../Page/1955年.md" title="wikilink">1955年脱离</a><a href="../Page/同盟国_(第二次世界大戰).md" title="wikilink">二战同盟国独立</a>。亦是该国的国庆日。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/5月28日.md" title="wikilink">5月28日和</a><a href="../Page/10月18日.md" title="wikilink">10月18日</a></p></td>
<td><p><a href="../Page/1918年.md" title="wikilink">1918年</a>5月28日从<a href="../Page/俄羅斯帝国.md" title="wikilink">俄羅斯帝国独立</a>；<a href="../Page/1991年.md" title="wikilink">1991年</a>10月18日再次宣布从<a href="../Page/苏联.md" title="wikilink">苏联独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/7月10日.md" title="wikilink">7月10日</a></p></td>
<td><p><a href="../Page/1973年.md" title="wikilink">1973年脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/12月16日.md" title="wikilink">12月16日</a></p></td>
<td><p><a href="../Page/1971年.md" title="wikilink">1971年脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>。亦是该国的国庆日。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/3月26日.md" title="wikilink">3月26日</a></p></td>
<td><p>脱离<a href="../Page/巴基斯坦.md" title="wikilink">巴基斯坦独立</a>；导致了长达9个月的战争，最终于<a href="../Page/1971年.md" title="wikilink">1971年</a><a href="../Page/12月16日.md" title="wikilink">12月16日结束</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/11月30日.md" title="wikilink">11月30日</a></p></td>
<td><p><a href="../Page/1966年.md" title="wikilink">1966年脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/7月3日.md" title="wikilink">7月3日</a></p></td>
<td><p><a href="../Page/1944年.md" title="wikilink">1944年由</a><a href="../Page/苏联.md" title="wikilink">苏联军队收复德占</a><a href="../Page/明斯克.md" title="wikilink">明斯克</a>，获得解放。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/7月21日.md" title="wikilink">7月21日</a></p></td>
<td><p>脱离<a href="../Page/荷兰.md" title="wikilink">荷兰</a><a href="../Page/比利時獨立.md" title="wikilink">独立</a>。<a href="../Page/利奥波德一世_(比利时).md" title="wikilink">利奥波德一世于</a><a href="../Page/1831年.md" title="wikilink">1831年宣誓成为比利时的第一位国王</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/9月21日.md" title="wikilink">9月21日</a></p></td>
<td><p><a href="../Page/1981年.md" title="wikilink">1981年脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>。亦称“九月欢庆”。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/8月1日.md" title="wikilink">8月1日</a></p></td>
<td><p><a href="../Page/1960年.md" title="wikilink">1960年脱离</a><a href="../Page/法国.md" title="wikilink">法国独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/8月6日.md" title="wikilink">8月6日</a></p></td>
<td><p><a href="../Page/1825年.md" title="wikilink">1825年脱离</a><a href="../Page/西班牙.md" title="wikilink">西班牙独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/3月1日.md" title="wikilink">3月1日</a></p></td>
<td><p><a href="../Page/1992年.md" title="wikilink">1992年脱离</a><a href="../Page/南斯拉夫社会主义联邦共和国.md" title="wikilink">南斯拉夫社会主义联邦共和国独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/9月30日.md" title="wikilink">9月30日</a></p></td>
<td><p><a href="../Page/1966年.md" title="wikilink">1966年脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/9月7日.md" title="wikilink">9月7日</a></p></td>
<td><p><a href="../Page/1822年.md" title="wikilink">1822年脱离</a><a href="../Page/葡萄牙.md" title="wikilink">葡萄牙独立</a>。称为“Dia da Independência”。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/1月1日.md" title="wikilink">1月1日</a></p></td>
<td><p><a href="../Page/1984年.md" title="wikilink">1984年脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/9月22日.md" title="wikilink">9月22日</a></p></td>
<td><p><a href="../Page/1908年.md" title="wikilink">1908年脱离</a><a href="../Page/奥斯曼帝国.md" title="wikilink">奥斯曼帝国独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/8月5日.md" title="wikilink">8月5日</a></p></td>
<td><p><a href="../Page/1960年.md" title="wikilink">1960年脱离</a><a href="../Page/法国.md" title="wikilink">法国独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/7月1日.md" title="wikilink">7月1日</a></p></td>
<td><p><a href="../Page/1962年.md" title="wikilink">1962年脱离</a><a href="../Page/比利时.md" title="wikilink">比利时独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/11月9日.md" title="wikilink">11月9日</a></p></td>
<td><p><a href="../Page/1953年.md" title="wikilink">1953年脱离</a><a href="../Page/法国.md" title="wikilink">法国独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/7月1日.md" title="wikilink">7月1日</a></p></td>
<td><p><a href="../Page/1867年.md" title="wikilink">1867年</a>，庆祝加拿大成立。亦称“加拿大日”。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/7月5日.md" title="wikilink">7月5日</a></p></td>
<td><p><a href="../Page/1975年.md" title="wikilink">1975年脱离</a><a href="../Page/葡萄牙.md" title="wikilink">葡萄牙独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/8月13日.md" title="wikilink">8月13日</a></p></td>
<td><p><a href="../Page/1960年.md" title="wikilink">1960年脱离</a><a href="../Page/法国.md" title="wikilink">法国独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/8月11日.md" title="wikilink">8月11日</a></p></td>
<td><p><a href="../Page/1960年.md" title="wikilink">1960年脱离</a><a href="../Page/法国.md" title="wikilink">法国独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/2月12日.md" title="wikilink">2月12日</a></p></td>
<td><p><a href="../Page/1818年.md" title="wikilink">1818年宣布脱离</a><a href="../Page/西班牙.md" title="wikilink">西班牙独立</a>。事实上，<a href="../Page/1844年.md" title="wikilink">1844年</a><a href="../Page/4月25日.md" title="wikilink">4月25日开始</a>，智利人民开始庆祝第一联合政府日（date of the first Government Junta），即<a href="../Page/9月18日.md" title="wikilink">9月18日</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/7月20日.md" title="wikilink">7月20日和</a><a href="../Page/8月7日.md" title="wikilink">8月7日</a></p></td>
<td><p><a href="../Page/1810年.md" title="wikilink">1810年脱离</a><a href="../Page/西班牙.md" title="wikilink">西班牙独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/6月30日.md" title="wikilink">6月30日</a></p></td>
<td><p><a href="../Page/1960年.md" title="wikilink">1960年脱离</a><a href="../Page/比利时.md" title="wikilink">比利时独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/9月15日.md" title="wikilink">9月15日</a></p></td>
<td><p><a href="../Page/1821年.md" title="wikilink">1821年脱离</a><a href="../Page/西班牙.md" title="wikilink">西班牙独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/8月7日.md" title="wikilink">8月7日</a></p></td>
<td><p><a href="../Page/1960年.md" title="wikilink">1960年脱离</a><a href="../Page/法国.md" title="wikilink">法国独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/10月8日.md" title="wikilink">10月8日和</a><a href="../Page/6月25日.md" title="wikilink">6月25日</a></p></td>
<td><p><a href="../Page/1991年.md" title="wikilink">1991年脱离</a><a href="../Page/南斯拉夫.md" title="wikilink">南斯拉夫独立</a>。经过全民公投后，议会于6月25日宣布独立（亦被称作“国家地位节”）；<a href="../Page/欧盟.md" title="wikilink">欧盟向克罗地亚施压</a>，要求其实施三个月的延期履行令，最终于10月8日正式独立。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/5月20日.md" title="wikilink">5月20日</a></p></td>
<td><p><a href="../Page/1902年.md" title="wikilink">1902年脱离</a><a href="../Page/美国.md" title="wikilink">美国独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/10月1日.md" title="wikilink">10月1日</a></p></td>
<td><p><a href="../Page/1960年.md" title="wikilink">1960年</a><a href="../Page/8月16日.md" title="wikilink">8月16日脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>，但塞浦路斯的独立日常常于10月1日庆祝。[1]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/10月28日.md" title="wikilink">10月28日和</a><a href="../Page/1月1日.md" title="wikilink">1月1日</a></p></td>
<td><p><a href="../Page/1918年.md" title="wikilink">1918年</a>10月28日，作为<a href="../Page/奥匈帝国.md" title="wikilink">奥匈帝国瓦解的标志</a>，成立<a href="../Page/捷克斯洛伐克.md" title="wikilink">捷克斯洛伐克共和国</a>；<a href="../Page/1993年.md" title="wikilink">1993年</a>1月1日，捷克斯洛伐克解体，<a href="../Page/捷克.md" title="wikilink">捷克独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/6月27日.md" title="wikilink">6月27日</a></p></td>
<td><p><a href="../Page/1977年.md" title="wikilink">1977年脱离</a><a href="../Page/法国.md" title="wikilink">法国独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/11月3日.md" title="wikilink">11月3日</a></p></td>
<td><p><a href="../Page/1978年.md" title="wikilink">1978年脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/2月27日.md" title="wikilink">2月27日</a></p></td>
<td><p><a href="../Page/1844年.md" title="wikilink">1844年脱离</a><a href="../Page/海地.md" title="wikilink">海地独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/5月20日.md" title="wikilink">5月20日</a></p></td>
<td><p><a href="../Page/2002年.md" title="wikilink">2002年脱离</a><a href="../Page/印度尼西亚.md" title="wikilink">印度尼西亚独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/8月10日.md" title="wikilink">8月10日和</a><a href="../Page/5月24日.md" title="wikilink">5月24日</a></p></td>
<td><p><a href="../Page/1809年.md" title="wikilink">1809年</a>8月10日预先宣布脱离<a href="../Page/西班牙.md" title="wikilink">西班牙独立</a>，但于<a href="../Page/1810年.md" title="wikilink">1810年</a><a href="../Page/8月2日.md" title="wikilink">8月2日处死了所有共谋独立者</a>，以失败而告终；最终于<a href="../Page/1822年.md" title="wikilink">1822年</a>5月25日皮欽查战役胜利而独立。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/9月15日.md" title="wikilink">9月15日</a></p></td>
<td><p>1821年脱离<a href="../Page/西班牙.md" title="wikilink">西班牙独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/5月24日.md" title="wikilink">5月24日</a></p></td>
<td><p><a href="../Page/1993年.md" title="wikilink">1993年脱离</a><a href="../Page/埃塞俄比亚.md" title="wikilink">埃塞俄比亚独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/2月24日.md" title="wikilink">2月24日和</a><a href="../Page/8月20日.md" title="wikilink">8月20日</a></p></td>
<td><p><a href="../Page/1918年.md" title="wikilink">1918年</a>2月24日脱离<a href="../Page/俄罗斯帝国.md" title="wikilink">俄罗斯帝国</a><a href="../Page/爱沙尼亚#.E7.AC.AC.E4.B8.80.E6.AC.A1.E7.8B.AC.E7.AB.8B.md" title="wikilink">独立</a>；<a href="../Page/1991年.md" title="wikilink">1991年</a>8月20日脱离苏联<a href="../Page/爱沙尼亚#.E7.AC.AC.E4.BA.8C.E6.AC.A1.E7.8B.AC.E7.AB.8B.md" title="wikilink">独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/10月10日.md" title="wikilink">10月10日</a></p></td>
<td><p><a href="../Page/1970年.md" title="wikilink">1970年脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/12月6日.md" title="wikilink">12月6日</a></p></td>
<td><p><a href="../Page/1917年.md" title="wikilink">1917年脱离</a><a href="../Page/俄罗斯帝国.md" title="wikilink">俄罗斯帝国</a><a href="../Page/芬兰独立日.md" title="wikilink">独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/2月18日.md" title="wikilink">2月18日</a></p></td>
<td><p><a href="../Page/1965年.md" title="wikilink">1965年脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/5月26日.md" title="wikilink">5月26日和</a><a href="../Page/4月9日.md" title="wikilink">4月9日</a></p></td>
<td><p><a href="../Page/1918年.md" title="wikilink">1918年成立第一共和国</a>；<a href="../Page/1991年.md" title="wikilink">1991年脱离</a><a href="../Page/苏联.md" title="wikilink">苏联独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/3月6日.md" title="wikilink">3月6日</a></p></td>
<td><p><a href="../Page/1957年.md" title="wikilink">1957年脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/3月25日.md" title="wikilink">3月25日</a></p></td>
<td><p><a href="../Page/1821年.md" title="wikilink">1821年脱离</a><a href="../Page/奥斯曼帝国.md" title="wikilink">奥斯曼帝国独立</a>。随后发生<a href="../Page/希腊独立战争.md" title="wikilink">希腊独立战争</a>。 |- valgin+top</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/9月15日.md" title="wikilink">9月15日</a></p></td>
<td><p><a href="../Page/1821年.md" title="wikilink">1821年脱离</a><a href="../Page/西班牙.md" title="wikilink">西班牙独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/10月2日.md" title="wikilink">10月2日</a></p></td>
<td><p><a href="../Page/1958年.md" title="wikilink">1958年脱离</a><a href="../Page/法国.md" title="wikilink">法国独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/5月26日.md" title="wikilink">5月26日</a></p></td>
<td><p><a href="../Page/1966年.md" title="wikilink">1966年脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/1月1日.md" title="wikilink">1月1日</a></p></td>
<td><p><a href="../Page/1804年.md" title="wikilink">1804年脱离</a><a href="../Page/法国.md" title="wikilink">法国独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/9月15日.md" title="wikilink">9月15日</a></p></td>
<td><p><a href="../Page/1821年.md" title="wikilink">1821年脱离</a><a href="../Page/西班牙.md" title="wikilink">西班牙独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/12月1日.md" title="wikilink">12月1日</a></p></td>
<td><p><a href="../Page/1918年.md" title="wikilink">1918年脱离</a><a href="../Page/丹麦王国.md" title="wikilink">丹麦王国独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/8月15日.md" title="wikilink">8月15日</a></p></td>
<td><p><a href="../Page/1947年.md" title="wikilink">1947年脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>。亦称“Swatantrata Divas”。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/8月17日.md" title="wikilink">8月17日</a></p></td>
<td><p><a href="../Page/1945年.md" title="wikilink">1945年脱离</a><a href="../Page/荷兰.md" title="wikilink">荷兰独立</a>。亦称“Hari Proklamasi Kemerdekaan R.I.”。荷兰于<a href="../Page/1949年.md" title="wikilink">1949年承认印度尼西亚的独立和主权</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/2月11日.md" title="wikilink">2月11日</a></p></td>
<td><p><a href="../Page/1979年.md" title="wikilink">1979年结束君主制</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/4月24日.md" title="wikilink">4月24日</a></p></td>
<td><p><a href="../Page/1916年.md" title="wikilink">1916年的</a><a href="../Page/复活节起义.md" title="wikilink">复活节起义以</a><a href="../Page/复活节宣言.md" title="wikilink">复活节宣言的形式宣布脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/5月14日.md" title="wikilink">5月14日</a></p></td>
<td><p><a href="../Page/1948年.md" title="wikilink">1948年脱离</a><a href="../Page/英属巴勒斯坦托管地.md" title="wikilink">英属巴勒斯坦托管地独立</a>（<a href="../Page/希伯来历.md" title="wikilink">希伯来历</a>5 以珥月 5078）。亦称“Yom Ha'atzmaut”。实际上该节日常常于最接近希伯来历的5 以珥月最近的星期二、<a href="../Page/星期三.md" title="wikilink">星期三或星期四庆祝</a>，这意味着对于公历来说这个节日可能出现在<a href="../Page/4月15日.md" title="wikilink">4月15日和</a><a href="../Page/5月15日.md" title="wikilink">5月15日之间的某天</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/8月6日.md" title="wikilink">8月6日</a></p></td>
<td><p><a href="../Page/1962年.md" title="wikilink">1962年脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/5月25日.md" title="wikilink">5月25日</a></p></td>
<td><p><a href="../Page/1946年.md" title="wikilink">1946年脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/12月16日.md" title="wikilink">12月16日</a></p></td>
<td><p><a href="../Page/1991年.md" title="wikilink">1991年脱离</a><a href="../Page/苏联.md" title="wikilink">苏联独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/12月12日.md" title="wikilink">12月12日</a></p></td>
<td><p><a href="../Page/1963年.md" title="wikilink">1963年脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/9月9日.md" title="wikilink">9月9日</a></p></td>
<td><p><a href="../Page/1948年.md" title="wikilink">1948年建国</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/8月15日.md" title="wikilink">8月15日</a></p></td>
<td><p><a href="../Page/1945年.md" title="wikilink">1945年脱离</a><a href="../Page/日本.md" title="wikilink">日本独立</a>。亦称“<a href="../Page/韩国光复节.md" title="wikilink">光复节</a>”。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/6月19日.md" title="wikilink">6月19日</a></p></td>
<td><p><a href="../Page/1961年.md" title="wikilink">1961年脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/2月17日.md" title="wikilink">2月17日</a></p></td>
<td><p><a href="../Page/2008年.md" title="wikilink">2008年脱离</a><a href="../Page/塞尔维亚.md" title="wikilink">塞尔维亚独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/8月31日.md" title="wikilink">8月31日</a></p></td>
<td><p><a href="../Page/1991年.md" title="wikilink">1991年脱离</a><a href="../Page/苏联.md" title="wikilink">苏联独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/11月18日.md" title="wikilink">11月18日和</a><a href="../Page/5月4日.md" title="wikilink">5月4日</a></p></td>
<td><p><a href="../Page/1918年.md" title="wikilink">1918年</a>11月18日脱离<a href="../Page/俄罗斯帝国.md" title="wikilink">俄罗斯帝国独立</a>；<a href="../Page/1990年.md" title="wikilink">1990年</a>5月4日脱离<a href="../Page/苏联.md" title="wikilink">苏联独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/11月22日.md" title="wikilink">11月22日</a></p></td>
<td><p><a href="../Page/1943年.md" title="wikilink">1943年脱离</a><a href="../Page/法国.md" title="wikilink">法国独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/10月4日.md" title="wikilink">10月4日</a></p></td>
<td><p><a href="../Page/1966年.md" title="wikilink">1966年脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/7月26日.md" title="wikilink">7月26日</a></p></td>
<td><p><a href="../Page/1847年.md" title="wikilink">1847年独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/12月24日.md" title="wikilink">12月24日</a></p></td>
<td><p><a href="../Page/1951年.md" title="wikilink">1951年脱离</a><a href="../Page/意大利.md" title="wikilink">意大利独立</a>。然而，<a href="../Page/1969年.md" title="wikilink">1969年</a><a href="../Page/9月1日.md" title="wikilink">9月1日的</a>“革命”后，该日的庆祝被<a href="../Page/卡扎菲.md" title="wikilink">卡扎菲废止</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/2月16日.md" title="wikilink">2月16日和</a><a href="../Page/3月11日.md" title="wikilink">3月11日</a></p></td>
<td><p><a href="../Page/1918年.md" title="wikilink">1918年</a>2月16日脱离<a href="../Page/俄罗斯帝国.md" title="wikilink">俄罗斯帝国和</a><a href="../Page/德意志帝国.md" title="wikilink">德意志帝国独立</a>；<a href="../Page/1990年.md" title="wikilink">1990年</a>3月11日脱离<a href="../Page/苏联.md" title="wikilink">苏联独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/9月8日.md" title="wikilink">9月8日</a></p></td>
<td><p><a href="../Page/1991年.md" title="wikilink">1991年脱离</a><a href="../Page/南斯拉夫.md" title="wikilink">南斯拉夫独立</a>。称为“Ден на независноста”。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/6月26日.md" title="wikilink">6月26日</a></p></td>
<td><p><a href="../Page/1960年.md" title="wikilink">1960年脱离</a><a href="../Page/法国.md" title="wikilink">法国独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/7月6日.md" title="wikilink">7月6日</a></p></td>
<td><p><a href="../Page/1964年.md" title="wikilink">1964年脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/8月31日.md" title="wikilink">8月31日</a></p></td>
<td><p><a href="../Page/1957年.md" title="wikilink">1957年脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>。亦称“Hari Merdeka”。（之后于1963年9月16日与北婆罗洲（沙巴），砂拉越和新加坡成立马来西亚联邦）。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/7月26日.md" title="wikilink">7月26日</a></p></td>
<td><p><a href="../Page/1965年.md" title="wikilink">1965年脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>。[2]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/9月22日.md" title="wikilink">9月22日</a></p></td>
<td><p><a href="../Page/1960年.md" title="wikilink">1960年脱离</a><a href="../Page/法国.md" title="wikilink">法国独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/9月21日.md" title="wikilink">9月21日</a></p></td>
<td><p><a href="../Page/1964年.md" title="wikilink">1964年脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/3月12日.md" title="wikilink">3月12日</a></p></td>
<td><p><a href="../Page/1968年.md" title="wikilink">1968年脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/9月16日.md" title="wikilink">9月16日</a></p></td>
<td><p><a href="../Page/1810年.md" title="wikilink">1810年脱离</a><a href="../Page/西班牙.md" title="wikilink">西班牙独立</a>。亦称“Grito de Dolores”。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/8月27日.md" title="wikilink">8月27日</a></p></td>
<td><p><a href="../Page/1991年.md" title="wikilink">1991年脱离</a><a href="../Page/苏联.md" title="wikilink">苏联独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/11月26日.md" title="wikilink">11月26日</a></p></td>
<td><p><a href="../Page/1921年.md" title="wikilink">1921年脱离</a><a href="../Page/中国.md" title="wikilink">中国独立</a>。但直到1946年才正式脫離<a href="../Page/中国.md" title="wikilink">中国獨立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/5月21日.md" title="wikilink">5月21日</a></p></td>
<td><p><a href="../Page/2006年.md" title="wikilink">2006年脱离</a><a href="../Page/塞黑.md" title="wikilink">塞黑独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/11月18日.md" title="wikilink">11月18日</a></p></td>
<td><p><a href="../Page/1956年.md" title="wikilink">1956年脱离</a><a href="../Page/法国.md" title="wikilink">法国和</a><a href="../Page/西班牙.md" title="wikilink">西班牙独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/6月25日.md" title="wikilink">6月25日</a></p></td>
<td><p><a href="../Page/1975年.md" title="wikilink">1975年脱离</a><a href="../Page/葡萄牙.md" title="wikilink">葡萄牙独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/1月4日.md" title="wikilink">1月4日</a></p></td>
<td><p><a href="../Page/1948年.md" title="wikilink">1948年脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/3月21日.md" title="wikilink">3月21日</a></p></td>
<td><p><a href="../Page/1990年.md" title="wikilink">1990年脱离</a><a href="../Page/南非.md" title="wikilink">南非独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/5月5日.md" title="wikilink">5月5日</a></p></td>
<td><p><a href="../Page/1945年.md" title="wikilink">1945年脱离</a><a href="../Page/纳粹德国.md" title="wikilink">纳粹德国独立</a>。亦称“Bevrijdingsdag”。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/9月15日.md" title="wikilink">9月15日</a></p></td>
<td><p><a href="../Page/1821年.md" title="wikilink">1821年脱离</a><a href="../Page/西班牙.md" title="wikilink">西班牙独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/8月3日.md" title="wikilink">8月3日</a></p></td>
<td><p><a href="../Page/1960年.md" title="wikilink">1960年脱离</a><a href="../Page/法国.md" title="wikilink">法国独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/10月1日.md" title="wikilink">10月1日</a></p></td>
<td><p><a href="../Page/1960年.md" title="wikilink">1960年脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/8月31日.md" title="wikilink">8月31日</a></p></td>
<td><p><a href="../Page/1963年.md" title="wikilink">1963年脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>。（之后于1963年9月16日与马来亚联合邦，砂拉越和新加坡成立马来西亚联邦）。后改名为<a href="../Page/沙巴.md" title="wikilink">沙巴</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/8月14日.md" title="wikilink">8月14日</a></p></td>
<td><p><a href="../Page/1947年.md" title="wikilink">1947年脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>，即<a href="../Page/伊斯兰历.md" title="wikilink">伊斯兰历中</a><a href="../Page/赖买丹月.md" title="wikilink">赖买丹月的第</a>27日。亦称“Yaum e Azadi”。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/11月3日.md" title="wikilink">11月3日</a></p></td>
<td><p><a href="../Page/1903年.md" title="wikilink">1903年</a>11月3日脱离<a href="../Page/大哥伦比亚.md" title="wikilink">大哥伦比亚独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/9月16日.md" title="wikilink">9月16日</a></p></td>
<td><p><a href="../Page/1975年.md" title="wikilink">1975年脱离</a><a href="../Page/澳大利亚.md" title="wikilink">澳大利亚独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/5月15日.md" title="wikilink">5月15日</a></p></td>
<td><p><a href="../Page/1811年.md" title="wikilink">1811年脱离</a><a href="../Page/西班牙.md" title="wikilink">西班牙独立</a>。亦称“Día de Independencia”。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/7月28日.md" title="wikilink">7月28日</a></p></td>
<td><p><a href="../Page/1821年.md" title="wikilink">1821年脱离</a><a href="../Page/西班牙.md" title="wikilink">西班牙独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/6月12日.md" title="wikilink">6月12日</a></p></td>
<td><p><a href="../Page/1898年.md" title="wikilink">1898年</a>6月12日由<a href="../Page/埃米利奥·阿奎纳多.md" title="wikilink">埃米利奥·阿奎纳多在菲律宾独立中宣布独立</a>，称“Araw ng Kalayaan”。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/11月11日.md" title="wikilink">11月11日</a></p></td>
<td><p><a href="../Page/1918年.md" title="wikilink">1918年脱离</a><a href="../Page/奥匈帝国.md" title="wikilink">奥匈帝国</a>、<a href="../Page/普鲁士.md" title="wikilink">普鲁士和</a><a href="../Page/俄罗斯帝国.md" title="wikilink">俄罗斯帝国长达</a>123年的割据后独立，亦称“Święto Niepodległości”。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/12月1日.md" title="wikilink">12月1日</a></p></td>
<td><p><a href="../Page/1143年.md" title="wikilink">1143年</a>10月5日脱离<a href="../Page/莱昂王国.md" title="wikilink">莱昂王国独立</a>；<a href="../Page/1640年.md" title="wikilink">1640年</a>12月1日脱离<a href="../Page/西班牙.md" title="wikilink">西班牙独立</a>。前者被称作“共和国日”，后者才是独立日。值得留意的是，葡萄牙的独立有别于其他国家，所谓的“独立”并非是国家主权的独立意识，而是国王对土地的管辖意识；事实上葡萄牙在1143年前、1580年至1640年与西班牙联盟期间一直都是独立存在的个体。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/12月18日.md" title="wikilink">12月18日</a></p></td>
<td><p><a href="../Page/1878年.md" title="wikilink">1878年卡塔尔当前执政党的始祖</a><a href="../Page/贾西姆·本·穆罕默德·阿勒萨尼.md" title="wikilink">贾西姆·本·穆罕默德·阿勒萨尼执政</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/6月12日.md" title="wikilink">6月12日</a></p></td>
<td><p><a href="../Page/1990年.md" title="wikilink">1990年</a>，当时的<a href="../Page/俄罗斯苏维埃联邦社会主义共和国.md" title="wikilink">俄罗斯苏维埃联邦社会主义共和国脱离</a><a href="../Page/苏联.md" title="wikilink">苏联独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/5月9日.md" title="wikilink">5月9日</a></p></td>
<td><p><a href="../Page/1877年.md" title="wikilink">1877年脱离</a><a href="../Page/奥斯曼帝国.md" title="wikilink">奥斯曼帝国独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/7月1日.md" title="wikilink">7月1日</a></p></td>
<td><p><a href="../Page/1962年.md" title="wikilink">1962年脱离</a><a href="../Page/比利时.md" title="wikilink">比利时独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/9月19日.md" title="wikilink">9月19日</a></p></td>
<td><p><a href="../Page/1983年.md" title="wikilink">1983年脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/6月1日.md" title="wikilink">6月1日</a></p></td>
<td><p><a href="../Page/1962年.md" title="wikilink">1962年脱离</a><a href="../Page/新西兰.md" title="wikilink">新西兰独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/7月12日.md" title="wikilink">7月12日</a></p></td>
<td><p><a href="../Page/1975年.md" title="wikilink">1975年脱离</a><a href="../Page/葡萄牙.md" title="wikilink">葡萄牙独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/7月22日.md" title="wikilink">7月22日</a></p></td>
<td><p><a href="../Page/1963年.md" title="wikilink">1963年脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>。（之后于1963年9月16日与马来亚联合邦，北婆罗洲（沙巴）和新加坡成立马来西亚联邦）。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/2月15日.md" title="wikilink">2月15日</a></p></td>
<td><p><a href="../Page/1804年.md" title="wikilink">1804年</a><a href="../Page/2月15日.md" title="wikilink">2月15日</a>，发起革命反对<a href="../Page/奥斯曼帝国.md" title="wikilink">奥斯曼帝国的侵占</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/6月29日.md" title="wikilink">6月29日</a></p></td>
<td><p><a href="../Page/1976年.md" title="wikilink">1976年脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/4月27日.md" title="wikilink">4月27日</a></p></td>
<td><p><a href="../Page/1961年.md" title="wikilink">1961年脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/8月9日.md" title="wikilink">8月9日</a></p></td>
<td><p><a href="../Page/1965年.md" title="wikilink">1965年脱离</a><a href="../Page/马来西亚.md" title="wikilink">马来西亚独立</a>。该日亦有<a href="../Page/新加坡国庆庆典.md" title="wikilink">国庆庆典</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/7月7日.md" title="wikilink">7月7日</a></p></td>
<td><p><a href="../Page/1978年.md" title="wikilink">1978年脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/7月17日.md" title="wikilink">7月17日</a></p></td>
<td><p><a href="../Page/1992年.md" title="wikilink">1992年</a>7月17日独立。事实上直到<a href="../Page/1993年.md" title="wikilink">1993年</a>1月1日才脱离<a href="../Page/捷克斯洛伐克.md" title="wikilink">捷克斯洛伐克独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/12月26日.md" title="wikilink">12月26日和</a><a href="../Page/6月25日.md" title="wikilink">6月25日</a></p></td>
<td><p><a href="../Page/1990年.md" title="wikilink">1990年</a>12月26日通过<a href="../Page/公民投票.md" title="wikilink">公民投票方式从</a><a href="../Page/南斯拉夫.md" title="wikilink">南斯拉夫分裂出来</a>；<a href="../Page/1991年.md" title="wikilink">1991年</a>6月25日再次宣布独立。前者被称作“独立和团结日”，后者被称作“国家日”。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/12月11日.md" title="wikilink">12月11日</a></p></td>
<td><p><a href="../Page/1931年.md" title="wikilink">1931年</a>12月11日脱离<a href="../Page/英国.md" title="wikilink">英国独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/7月9日.md" title="wikilink">7月9日</a></p></td>
<td><p><a href="../Page/2011年.md" title="wikilink">2011年</a>7月9日脱离<a href="../Page/苏丹.md" title="wikilink">苏丹独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/2月4日.md" title="wikilink">2月4日</a></p></td>
<td><p><a href="../Page/1948年.md" title="wikilink">1948年脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/1月1日.md" title="wikilink">1月1日</a></p></td>
<td><p><a href="../Page/1956年.md" title="wikilink">1956年脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/11月25日.md" title="wikilink">11月25日</a></p></td>
<td><p><a href="../Page/1975年.md" title="wikilink">1975年脱离</a><a href="../Page/荷兰.md" title="wikilink">荷兰独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/9月6日.md" title="wikilink">9月6日</a></p></td>
<td><p><a href="../Page/1968年.md" title="wikilink">1968年脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/8月1日.md" title="wikilink">8月1日</a></p></td>
<td><p><a href="../Page/1291年.md" title="wikilink">1291年脱离</a><a href="../Page/神圣罗马帝国.md" title="wikilink">神圣罗马帝国独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/9月9日.md" title="wikilink">9月9日</a></p></td>
<td><p><a href="../Page/1991年.md" title="wikilink">1991年脱离</a><a href="../Page/苏联.md" title="wikilink">苏联独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/12月9日.md" title="wikilink">12月9日</a></p></td>
<td><p><a href="../Page/1961年.md" title="wikilink">1961年脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/8月31日.md" title="wikilink">8月31日</a></p></td>
<td><p><a href="../Page/1962年.md" title="wikilink">1962年脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/4月27日.md" title="wikilink">4月27日</a></p></td>
<td><p><a href="../Page/1960年.md" title="wikilink">1960年脱离</a><a href="../Page/法国.md" title="wikilink">法国独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/3月20日.md" title="wikilink">3月20日</a></p></td>
<td><p><a href="../Page/1956年.md" title="wikilink">1956年脱离</a><a href="../Page/法国.md" title="wikilink">法国独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/10月27日.md" title="wikilink">10月27日</a></p></td>
<td><p><a href="../Page/1991年.md" title="wikilink">1991年脱离</a><a href="../Page/苏联.md" title="wikilink">苏联独立</a>。[3]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/8月24日.md" title="wikilink">8月24日和</a><a href="../Page/1月22日.md" title="wikilink">1月22日</a></p></td>
<td><p><a href="../Page/1991年.md" title="wikilink">1991年</a>8月24日脱离<a href="../Page/苏联.md" title="wikilink">苏联独立</a>；<a href="../Page/1919年.md" title="wikilink">1919年</a>1月22日乌克兰统一。前者亦称“Den' Nezalezhnosti”。[4]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/12月2日.md" title="wikilink">12月2日</a></p></td>
<td><p><a href="../Page/1971年.md" title="wikilink">1971年脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/7月4日.md" title="wikilink">7月4日</a></p></td>
<td><p><a href="../Page/1776年.md" title="wikilink">1776年宣布脱离</a><a href="../Page/英国.md" title="wikilink">英国</a><a href="../Page/美国独立日.md" title="wikilink">独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/8月25日.md" title="wikilink">8月25日</a></p></td>
<td><p><a href="../Page/1825年.md" title="wikilink">1825年脱离</a><a href="../Page/巴西.md" title="wikilink">巴西独立</a>。称为“Día de la Independencia”。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/9月1日.md" title="wikilink">9月1日</a></p></td>
<td><p><a href="../Page/1991年.md" title="wikilink">1991年脱离</a><a href="../Page/苏联.md" title="wikilink">苏联独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/7月30日.md" title="wikilink">7月30日</a></p></td>
<td><p><a href="../Page/1980年.md" title="wikilink">1980年脱离</a><a href="../Page/英国.md" title="wikilink">英国和</a><a href="../Page/法国.md" title="wikilink">法国独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/2月11日.md" title="wikilink">2月11日</a></p></td>
<td><p><a href="../Page/1929年.md" title="wikilink">1929年与</a><a href="../Page/意大利.md" title="wikilink">意大利签署</a><a href="../Page/拉特兰条约.md" title="wikilink">拉特兰条约</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/7月5日.md" title="wikilink">7月5日</a></p></td>
<td><p><a href="../Page/1811年.md" title="wikilink">1811年脱离</a><a href="../Page/西班牙.md" title="wikilink">西班牙独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/9月2日.md" title="wikilink">9月2日</a></p></td>
<td><p><a href="../Page/1945年.md" title="wikilink">1945年宣布脱离</a><a href="../Page/日本.md" title="wikilink">日本和</a><a href="../Page/法国.md" title="wikilink">法国独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/11月30日.md" title="wikilink">11月30日</a></p></td>
<td><p><a href="../Page/1967年.md" title="wikilink">1967年脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/10月24日.md" title="wikilink">10月24日</a></p></td>
<td><p><a href="../Page/1964年.md" title="wikilink">1964年脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>。</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/4月18日.md" title="wikilink">4月18日</a></p></td>
<td><p><a href="../Page/1980年.md" title="wikilink">1980年脱离</a><a href="../Page/英国.md" title="wikilink">英国独立</a>。</p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/2月27日.md" title="wikilink">2月27日</a></p></td>
<td><p><a href="../Page/1976年.md" title="wikilink">1976年宣告阿拉伯撒哈拉民主共和国政府成立</a>。</p></td>
</tr>
</tbody>
</table>

## 參見

  - [國慶日](../Page/國慶日.md "wikilink")
  - [解放日](../Page/解放日.md "wikilink")

## 参考文献

[独立日](../Category/独立日.md "wikilink") [\*](../Category/國慶日.md "wikilink")

1.
2.  [Avatterin.net](http://www.avatterin.net)
3.
4.