[缩略图](https://zh.wikipedia.org/wiki/File:Louvre_042005_10.jpg "fig:缩略图")[卢浮宫](../Page/卢浮宫.md "wikilink")\]\]
[缩略图](https://zh.wikipedia.org/wiki/File:NubianPharoahs.jpg "fig:缩略图")[法老](../Page/法老.md "wikilink")\]\]
**塔哈尔卡**（Taharqa）是[古埃及](../Page/古埃及.md "wikilink")[第二十五王朝](../Page/埃及第二十五王朝.md "wikilink")（**努比亚王朝**）[法老](../Page/法老.md "wikilink")（前689年—前663年在位）。他是法老[皮耶的兒子](../Page/皮耶.md "wikilink")，曾统治[庫施](../Page/庫施.md "wikilink")。后被其叔父召回，共同抗击[亚述国王](../Page/亚述.md "wikilink")[阿萨尔哈东](../Page/阿萨尔哈东.md "wikilink")。约于前673年击退阿萨尔哈东的进攻。在前671年，[亚述帝国卷土重来](../Page/亚述.md "wikilink")，占领包括[孟斐斯在内的下埃及](../Page/孟斐斯.md "wikilink")，后来塔哈尔卡成功地收回了大部分失地。但在前666年，亚述国王[亚述巴尼帕几乎征服了整个埃及](../Page/亚述巴尼帕.md "wikilink")，塔哈尔卡逃亡纳帕塔，并死在那里。

## 脚注

[Category:苏丹人](../Category/苏丹人.md "wikilink")
[Category:前664年逝世](../Category/前664年逝世.md "wikilink")
[Category:第二十五王朝法老](../Category/第二十五王朝法老.md "wikilink")