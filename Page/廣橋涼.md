**廣橋涼**（，）為[日本的](../Page/日本.md "wikilink")[女性配音員](../Page/配音員.md "wikilink")。出身於[新潟縣](../Page/新潟縣.md "wikilink")\[1\]\[2\]。O型[血](../Page/血型.md "wikilink")。

## 人物簡介

本名相同\[3\]。[青二Production所屬](../Page/青二製作.md "wikilink")，[青二塾大阪校第](../Page/青二塾.md "wikilink")16期畢業\[4\]\[5\]。

## 主要參與作品

### 電視動畫

  - 2002年

<!-- end list -->

  - [青出於藍](../Page/青出於藍_\(動畫\).md "wikilink")（三堀さん）
  - [水瓶戰記](../Page/水瓶戰記.md "wikilink")（女子）※動畫出道作
  - [朝霧的巫女](../Page/朝霧的巫女.md "wikilink")（小孩）
  - [GUN FRONTIER](../Page/ガンフロンティア_\(漫画\).md "wikilink")（ハナ）
  - [金肉人II世](../Page/金肉人II世.md "wikilink")（ドロシー〈初代〉）※第20話初登場。
  - [Chobits](../Page/Chobits.md "wikilink")（女僕PC、超商PC）
  - [灰羽連盟](../Page/灰羽連盟.md "wikilink")（**落下**）

<!-- end list -->

  - 2003年

<!-- end list -->

  - [宇宙星路](../Page/宇宙星路.md "wikilink")（**風祭りんな**）
  - [萬花筒之星](../Page/萬花筒之星.md "wikilink")（**苗木野空**）
  - [音速小子X](../Page/音速小子X.md "wikilink")（**塔爾斯**、起司）
  - [變形金剛：微型傳說](../Page/變形金剛：微型傳說.md "wikilink")（小孩）
  - [戰鬥陀螺G世代](../Page/戰鬥陀螺.md "wikilink")（莫妮卡）
  - [人間交差点](../Page/人間交差点.md "wikilink")（看護婦）
  - [ぽぽたん](../Page/ぽぽたん.md "wikilink")（千代）
  - [ONE PIECE](../Page/ONE_PIECE.md "wikilink")（伊索卡）

<!-- end list -->

  - 2004年

<!-- end list -->

  - [空戰88區](../Page/空戰88區.md "wikilink")（**金·亞柏**）
  - [北へ。〜Diamond Dust
    Drops〜](../Page/北へ。〜Diamond_Dust_Drops〜.md "wikilink")（通行人）
  - [Keroro軍曹](../Page/Keroro軍曹.md "wikilink")（[東谷小雪](../Page/東谷小雪.md "wikilink")、雪乃進）
  - [櫻桃小丸子](../Page/櫻桃小丸子.md "wikilink")（石原さん）
  - [光與水的女神](../Page/光與水的女神.md "wikilink")（アイ・マユズミ）
  - [冒險王比特](../Page/冒險王比特.md "wikilink")（女性獵人）
  - [魔法少女隊](../Page/魔法少女隊.md "wikilink")（**艾華**）

<!-- end list -->

  - 2005年

<!-- end list -->

  - [ARIA The ANIMATION](../Page/水星領航員.md "wikilink")（**愛麗絲・凱洛爾**）
  - [神樣中學生](../Page/神樣中學生.md "wikilink")（能登清美）
  - [CLUSTER EDGE](../Page/CLUSTER_EDGE.md "wikilink")（人造兵）
  - [Xenosaga THE ANIMATION](../Page/Xenosaga.md "wikilink")（キルシュヴァッサー）
  - [曙光少女](../Page/曙光少女.md "wikilink")（茵特古拉‧瑪提魯）
  - [嬉皮笑園](../Page/嬉皮笑園.md "wikilink")（白鳥鈴音）
  - [冒險王比特](../Page/冒險王比特.md "wikilink")（リズ）
  - [魔法少女加奈](../Page/魔法少女加奈.md "wikilink")（**柊千早/カーマイン**）
  - [魔法老師](../Page/魔法老師.md "wikilink")（阿妮亞）

<!-- end list -->

  - 2006年

<!-- end list -->

  - [怪 〜ayakashi〜](../Page/怪_〜ayakashi〜.md "wikilink")（伊藤お梅）
  - [ARIA The NATURAL](../Page/水星領航員.md "wikilink")（**愛麗絲・凱洛爾**）
  - [鍵姫物語 永久アリス輪舞曲](../Page/鍵姫物語_永久アリス輪舞曲.md "wikilink")（鳴見ナミ）
  - [神樣家族](../Page/神樣家族.md "wikilink")（ルル（少女期））
  - [銀魂](../Page/銀魂_\(動畫\).md "wikilink")（澄夜公主）
  - [星際海盜](../Page/星際海盜.md "wikilink")（**フランカ**）
  - [地獄少女 二籠](../Page/地獄少女.md "wikilink")（恩田麻紀）
  - [我的裘可妹妹](../Page/我的裘可妹妹.md "wikilink")（石田祐子）
  - [Project BLUE
    地球SOS](../Page/Project_BLUE_地球SOS.md "wikilink")（**ロッタ・ブレスト**）
  - [RAY THE ANIMATION](../Page/怪醫美女RAY.md "wikilink")（雅）

<!-- end list -->

  - 2007年

<!-- end list -->

  - [君吻 pure rouge](../Page/君吻_pure_rouge.md "wikilink")（**咲野明日夏**）
  - [CLANNAD](../Page/CLANNAD.md "wikilink")（**藤林杏**）
  - [鬼太郎
    第5作](../Page/鬼太郎.md "wikilink")（[座敷童子](../Page/座敷童子.md "wikilink")、メドチ）
  - [光明之淚X風](../Page/光明之淚X風.md "wikilink")（艾爾薇、久遠之森的詠唱者）
  - [Sketchbook ～full color's～](../Page/Sketchbook.md "wikilink")（春日野日和）
  - [精靈守護者](../Page/守護者系列.md "wikilink")（薩亞）
  - [爆丸](../Page/爆丸.md "wikilink")（**丸兆/丸藏兆治**）
  - [BACCANO\! -バッカーノ\!-](../Page/Baccano!.md "wikilink")（**夏涅・拉弗雷特**）
  - [BAMBOO BLADE](../Page/BAMBOO_BLADE.md "wikilink")（**川添珠姬**）
  - [羅密歐與茱麗葉](../Page/羅密歐×茱麗葉.md "wikilink")（安東尼奧）
  - ONE PIECE（ユウヤ）

<!-- end list -->

  - 2008年

<!-- end list -->

  - [ARIA The
    ORIGINATION](../Page/ARIA#テレビアニメ第3期.md "wikilink")（**愛麗絲・凱洛爾**）

  - [狂亂家族日記](../Page/狂亂家族日記.md "wikilink")（**亂崎雹霞/黑色十三號**）

  - [染红的街道](../Page/染红的街道.md "wikilink")（白石和美）

  - [鬼太郎 第5作](../Page/鬼太郎.md "wikilink")（大地）

  - [CLANNAD ～AFTER STORY～](../Page/CLANNAD.md "wikilink")（**藤林杏**、女學生）

  - （涼子）

  - [藥師寺涼子怪奇事件簿](../Page/藥師寺涼子怪奇事件簿.md "wikilink")（泉田真菜）

<!-- end list -->

  - 2009年

<!-- end list -->

  - [あぁ、レイモンド\!](../Page/あぁ、レイモンド!.md "wikilink")（**マルセロ**）
  - [空罐少女](../Page/空罐少女.md "wikilink")（宮下美咲）
  - [潘朵拉之心](../Page/潘朵拉之心.md "wikilink")（艾可/孳瓦）
  - [守護甜心！派對！](../Page/守護甜心！.md "wikilink")（ミー）
  - [續 夏目友人帳](../Page/續_夏目友人帳.md "wikilink")（笹舟）
  - [大正野球娘](../Page/大正野球娘.md "wikilink")（**石垣環**）
  - [加奈日記](../Page/加奈日記.md "wikilink")（**北岡夢**）
  - [戰鬥司書 The Book of Bantorra](../Page/戰鬥司書系列.md "wikilink")（凱撒莉羅＝朵朵娜）
  - [動物偵探奇魯米](../Page/動物偵探奇魯米.md "wikilink")（岩下瑠美子）
  - [P☆STAR](../Page/P☆STAR.md "wikilink")（ピスター）

<!-- end list -->

  - 2010年

<!-- end list -->

  - [鋼鐵人](../Page/鋼鐵人_\(2010年動畫\).md "wikilink")（翔〈幼少時代〉）
  - [最後大魔王](../Page/最後大魔王.md "wikilink")（**白石リリィ**）
  - [爆丸2大爆破](../Page/爆丸2大爆破.md "wikilink")（**丸藏兆治**）
  - [变身！公主偶像](../Page/变身！公主偶像.md "wikilink")（小野幸生）
  - [名偵探柯南](../Page/名偵探柯南_\(動畫\).md "wikilink")/[魔術快斗](../Page/魔術快斗.md "wikilink")（小孩）
  - [WORKING\!\!](../Page/迷糊餐廳.md "wikilink")（**山田葵**）
  - [刀語](../Page/刀語.md "wikilink")（真庭人鳥）
  - [戰鬥陀螺 鋼鐵奇兵 爆](../Page/戰鬥陀螺_鋼鐵奇兵.md "wikilink")（大鳥翼（幼年））
  - [STAR DRIVER 閃亮的塔科特](../Page/STAR_DRIVER_閃亮的塔科特.md "wikilink")（陽·鞠野）
  - [強襲魔女2](../Page/強襲魔女.md "wikilink")（露西安娜・瑪采）

<!-- end list -->

  - 2011年

<!-- end list -->

  - [亞斯塔蘿黛的後宮玩具](../Page/蘿黛的後宮玩具.md "wikilink")（優柏可·西葛諾）
  - [卡片戰鬥先導者](../Page/卡片戰鬥先導者.md "wikilink")（大文字渚）
  - [Double-j](../Page/Double-j.md "wikilink")（真田静馬、ウグイス嬢、占卜師）
  - [幸福光暈～hitotose～](../Page/幸福光暈.md "wikilink")（篠田小町）
  - [魔法小惡魔](../Page/魔法小惡魔.md "wikilink")（花梨、佐藤）
  - [日常](../Page/日常.md "wikilink")（まーちゃん）
  - [爆丸3狩獵保衛者](../Page/爆丸3狩獵保衛者.md "wikilink")（**丸藏兆治**）
  - [47都道府犬](../Page/声優バラエティー_SAY!YOU!SAY!ME!.md "wikilink")（新潟犬）
  - [WORKING'\!\!](../Page/迷糊餐廳.md "wikilink")（**山田葵**）

<!-- end list -->

  - 2012年

<!-- end list -->

  - [卡片戰鬥先導者 亞洲巡迴賽篇](../Page/卡片戰鬥先導者.md "wikilink")（大文字渚、ビーチ）
  - [CØDE:BREAKER](../Page/CØDE:BREAKER.md "wikilink")（ちさ）
  - [光明之心 ～幸福的麵包～](../Page/光明之心_～幸福的麵包～.md "wikilink")（蘿娜、拉娜）
  - [戰國Collection](../Page/戰國Collection.md "wikilink")（伊勢新九郎 / 北条早雲）
  - [謎樣女友X](../Page/謎樣女友X.md "wikilink")（**丘步子**）
  - [冰菓](../Page/古籍研究社系列#故事簡介.md "wikilink")（瀬之上真美子）
  - [Little Busters\!](../Page/Little_Busters!.md "wikilink")（中村由香里）

<!-- end list -->

  - 2013年

<!-- end list -->

  - 銀魂'（澄夜公主、小姓）
  - [櫻桃小丸子](../Page/櫻桃小丸子.md "wikilink")（ゆうちゃん）
  - [幸福光暈 ～More Aggressive～](../Page/幸福光暈.md "wikilink")（篠田小町）
  - [SERVANT×SERVICE](../Page/SERVANT×SERVICE.md "wikilink")（Daisy）
  - [鋼彈創鬥者](../Page/鋼彈創鬥者.md "wikilink")（佐崎進）

<!-- end list -->

  - 2014年

<!-- end list -->

  - [鬼燈的冷徹](../Page/鬼燈的冷徹.md "wikilink")（木靈）
  - [Keroro](../Page/Keroro軍曹_\(動畫\).md "wikilink")（**東谷小雪**）
  - [龍收藏](../Page/龍收藏.md "wikilink")（**哈尼奇**）
  - [鋼彈桑](../Page/機動戰士：鋼彈桑.md "wikilink")（基西莉亞）
  - [電波少女與錢仙大人](../Page/電波少女與錢仙大人.md "wikilink")（**市松小雛**）
  - [GUNDAM創戰者TRY](../Page/GUNDAM創戰者.md "wikilink")（佐崎薰子、幼少的祐馬）
  - [臨時女友](../Page/臨時女友.md "wikilink")（宮內希）

<!-- end list -->

  - 2015年

<!-- end list -->

  - [幸腹塗鴉](../Page/幸腹塗鴉.md "wikilink")（希玲的母親）

  - 銀魂°（澄夜公主）

  - （**阿斯卡·布斯**）

  - [WORKING\!\!\!](../Page/迷糊餐廳.md "wikilink")（**山田葵**）

  - [科學小飛俠Crowds Insight](../Page/科學小飛俠Crowds.md "wikilink")（丸山兒玉）

<!-- end list -->

  - 2016年

<!-- end list -->

  - [機動戰士GUNDAM UC RE:0096](../Page/機動戰士GUNDAM_UC.md "wikilink")（艾斯塔）
  - [我的英雄學院](../Page/我的英雄學院.md "wikilink")（**峰田實**）
  - [美少女戰士Crystal](../Page/美少女戰士Crystal.md "wikilink") Season III
    死亡毀滅者篇（**露娜**）
  - [GUNDAM創鬥者TRY 島上熱戰](../Page/GUNDAM創鬥者.md "wikilink")（**佐崎薰子**）
  - [數碼暴龍宇宙 應用怪獸](../Page/數碼暴龍宇宙_應用怪獸.md "wikilink")（和戶尊／華生）

<!-- end list -->

  - 2017年

<!-- end list -->

  - 我的英雄學院 第2期（**峰田實**）
  - [怪怪守護神](../Page/怪怪守護神.md "wikilink")（奈中井菜菜子）
  - [18if](../Page/18if.md "wikilink")（奈奈）
  - [寶石之國](../Page/寶石之國.md "wikilink")（**黑曜石**）
  - 鬼燈的冷徹 第貳期（木靈）
  - [見習神仙 秘密的心靈](../Page/見習神仙_秘密的心靈.md "wikilink")（**巧克蘭雪**）

<!-- end list -->

  - 2018年

<!-- end list -->

  - [琴之森](../Page/琴之森.md "wikilink")（亞理沙）
  - [音樂少女](../Page/音樂少女.md "wikilink")（亞子）
  - [Free\!-Dive to the Future-](../Page/Free!.md "wikilink")（國木田步\[6\]）

### OVA

  - [Macross Zero](../Page/Macross_Zero.md "wikilink")（少年）
  - [ARIA The OVA 〜ARIETTA〜](../Page/水星領航員.md "wikilink")（**愛麗絲・凱洛爾**（））
  - [電波系彼女](../Page/電波系彼女.md "wikilink")（**墮花雨**）

<!-- end list -->

  - 2014年

<!-- end list -->

  - [絕滅危愚少女 Amazing
    Twins](../Page/絕滅危愚少女_Amazing_Twins.md "wikilink")（情緒高木）

<!-- end list -->

  - 2015年

<!-- end list -->

  - [幸福光暈〜畢業照〜](../Page/幸福光暈.md "wikilink")（篠田小町）
  - [無頭騎士異聞錄 DuRaRaRa\!\!×2 承 外傳！？ 第4.5話
    我的心是火鍋的形狀](../Page/無頭騎士異聞錄_DuRaRaRa!!.md "wikilink")（紀田正臣〈幼少期〉）※劇場先行上映

<!-- end list -->

  - 2017年

<!-- end list -->

  - [鬼燈的冷徹](../Page/鬼燈的冷徹.md "wikilink") OAD4（木灵）

### 網路動畫

  - 2014年

<!-- end list -->

  - [美少女戰士Crystal](../Page/美少女戰士Crystal.md "wikilink")（**露娜**）

### 動畫電影

  - 超劇場版keroro軍曹（**東谷小雪**）

### 遊戲

  - [洛克人ZERO系列](../Page/洛克人ZERO.md "wikilink")（艾爾特）
  - [超音鼠系列](../Page/超音鼠.md "wikilink")（的第三代聲優）

<!-- end list -->

  - 2002年

<!-- end list -->

  - 學園都市 ヴァラノワール（リュート/デュプレ/ネイル）（[PS2](../Page/PS2.md "wikilink")，10月31日）

<!-- end list -->

  - 2003年

<!-- end list -->

  - ヴィーナス&ブレイブス～魔女と女神と滅びの予言～（フィニー）（PS2，2月12日）
  - [Summon
    Night](../Page/Summon_Night.md "wikilink")（シアリィ）（PS2，8月7日，2005年2月24日廉價版）
  - [問答魔法學院](../Page/問答魔法學院.md "wikilink")（百合）（[街機](../Page/街機.md "wikilink")，8月）
  - [TALES OF
    SYMPHONIA](../Page/TALES_OF_SYMPHONIA.md "wikilink")（ショコラ）（[GC](../Page/任天堂GameCube.md "wikilink")，8月29日，2004年9月22日移植至PS2）

<!-- end list -->

  - 2004年

<!-- end list -->

  - 學園都市 ヴァラノワール ローゼス（リュート/デュプレ/ネイル）（GC，1月23日）
  - [Hurrah\!
    Sailor](../Page/Hurrah!_Sailor.md "wikilink")（マレッタ）（PS2，2月26日）
  - [Keroro軍曹
    燃燒淘汰賽](../Page/Keroro軍曹.md "wikilink")（**東谷小雪**）（PS2，9月30日）
  - [Shining
    Tears](../Page/Shining_Tears.md "wikilink")（Elwing）（PS2，11月3日）
  - Keroro軍曹 競速對決（**東谷小雪**）（[GBA](../Page/GBA.md "wikilink")，12月9日）

<!-- end list -->

  - 2005年

<!-- end list -->

  - [旋光的輪舞](../Page/旋光的輪舞.md "wikilink")（ペルナ）
  - [Keroro軍曹
    ケロッとタイピングであります！](../Page/Keroro軍曹.md "wikilink")（**東谷小雪**）（[PC](../Page/PC.md "wikilink")，3月11日）
  - [Wild Arms: The 4th
    Detonator](../Page/Wild_Arms:_The_4th_Detonator.md "wikilink")（フィオレ/アーシア）（PS2，3月24日）
  - [Love★Smash\!5](../Page/Love★Smash!5.md "wikilink")（本条こづえ)（PS2，6月23日）
  - [(TALES OF
    LEGENDIA](../Page/\(TALES_OF_LEGENDIA.md "wikilink")（シャーリィ・フェンネス）（PS2，8月25日）
  - CRITICAL VELOCITY（パット）（PS2，10月13日）
  - Keroro軍曹 大混戰Z（**東谷小雪**）（PS2，11月17日）
  - [幸運☆星](../Page/幸運☆星.md "wikilink")
    萌えドリル（**泉此方**）（[NDS](../Page/NDS.md "wikilink")，12月1日）

<!-- end list -->

  - 2006年

<!-- end list -->

  - 【eM】-eNCHANT arM-（ユウキ）（[Xbox
    360](../Page/Xbox_360.md "wikilink")，1月12日）
  - [CLANNAD](../Page/CLANNAD.md "wikilink")（藤林杏）（PS2，2月23日）
  - [Separate
    Hearts](../Page/Separate_Hearts.md "wikilink")（深月真夜）（PS2，2月23日）
  - Keroro軍曹 ケロッとタイピング2であります\!（**東谷小雪**）（PS2，3月10日）
  - 超劇場版 Keroro軍曹 演習！全員集合（**東谷小雪**）（NDS，3月16日）
  - [君吻](../Page/君吻.md "wikilink")（咲野明日夏）（PS2，5月25日）
  - [Gadget trial](../Page/Gadget_trial.md "wikilink")（EPN-001GF
    IZEN）（PC，6月23日）
  - [ROCKMAN ZX](../Page/ROCKMAN_ZX.md "wikilink")（Prairie）（NDS，7月6日）
  - [旋光的輪舞 Rev.X](../Page/旋光的輪舞.md "wikilink")（ペルナ）（Xbox 360，7月27日）
  - [レッスルエンジェルス](../Page/レッスルエンジェルス.md "wikilink")（沢崎
    光、サキュバス真鍋）（PS2，8月24日）
  - [Quartett\!](../Page/Quartett!.md "wikilink")（リーナ・ユンハース）（PS2，9月28日）
  - [水星領航員](../Page/水星領航員.md "wikilink")（ARIA The NATURAL
    ～遠い記憶のミラージュ～（アリス・キャロル）（PS2，9月28日）

<!-- end list -->

  - 2007年

<!-- end list -->

  - [ENCHANT
    ARM](../Page/ENCHANT_ARM.md "wikilink")（ユウキ）（[PS3](../Page/PS3.md "wikilink")，1月25日）
  - [Keroro軍曹演習](../Page/Keroro軍曹.md "wikilink")！全員集合（**東谷小雪**）（NDS，2月22日）
  - 真・[幸運☆星](../Page/幸運☆星.md "wikilink") 萌えドリル～旅立ち～（**泉此方**）（NDS，4月26日）
  - [Shining Wind](../Page/Shining_Wind.md "wikilink")
    光明之風（久遠の森の詠み手）（PS2，5月17日）
  - [精霊の守り人](../Page/精霊の守り人.md "wikilink")（）
  - [ONE
    PIECE](../Page/ONE_PIECE.md "wikilink")-アンリミテッドアドベンチャー（波波雷）（Wii，4月26日）

<!-- end list -->

  - 2008年

<!-- end list -->

  - [女神Online](../Page/女神Online.md "wikilink")（莉莎）（PC，12月5日）
  - [女神Online](../Page/女神Online.md "wikilink")（伊奈）（PC，12月5日）
  - [双星物语2](../Page/双星物语2.md "wikilink")（昴）
  - [少女巡航艦](../Page/少女巡航艦.md "wikilink")（ティタ・ニューム）

<!-- end list -->

  - 2009年

<!-- end list -->

  - [絕體絕命都市3](../Page/絕體絕命都市3.md "wikilink")（牧村-{里}-奈）（PSP，4月23日）

<!-- end list -->

  - 2010年

<!-- end list -->

  - [紅魔城伝説II 妖幻の鎮魂歌](../Page/紅魔城伝説II_妖幻の鎮魂歌.md "wikilink")（魂魄妖夢）
  - [噬神戰士](../Page/噬神戰士.md "wikilink")（台場カノン）（PSP，10月28日）
  - [星空幻想Online](../Page/星空幻想Online.md "wikilink")（**蘋果**）
  - [少女巡航艦X](../Page/少女巡航艦X.md "wikilink")（ティタ・ニューム）（XBOX360，3月24日）

<!-- end list -->

  - 2012年

<!-- end list -->

  - [女友伴身邊](../Page/女友伴身邊.md "wikilink")（**宮內希**）

### 廣播劇

  - [英雄傳說VI「空之軌跡」](../Page/英雄傳說VI「空之軌跡」.md "wikilink")（喬兒·利德那）
  - [魔乳秘劍帖](../Page/魔乳秘劍帖.md "wikilink")（楓）

## 腳註

### 注釋

### 參考來源

## 外部連結

  - [青二Production公式官網的聲優簡介](http://www.aoni.co.jp/actress/ha/hirohashi-ryo.html)


[Category:日本女性配音員](../Category/日本女性配音員.md "wikilink")
[Category:新潟縣出身人物](../Category/新潟縣出身人物.md "wikilink")
[Category:青二Production](../Category/青二Production.md "wikilink")

1.
2.
3.
4.
5.
6.