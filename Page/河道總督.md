**河道總督**（）為[中國](../Page/中國.md "wikilink")[清朝相當特殊的官制名稱](../Page/清朝.md "wikilink")，該清朝為正二品的官職，與一般兼掌政務和军务的[總督不同](../Page/總督.md "wikilink")，而是專門治理[河道政务的总督官职](../Page/河道.md "wikilink")。

清朝設置三位河道總督。分別是

  - [江南河道總督](../Page/江南河道總督.md "wikilink")（簡稱南河總督）；駐節江苏省[淮安府](../Page/淮安府.md "wikilink")[清江浦](../Page/清江浦.md "wikilink")
  - [山東河南河道總督](../Page/山東河南河道總督.md "wikilink")（東河總督）駐山东省[濟寧州](../Page/濟寧.md "wikilink")
  - [直隸河道總督](../Page/直隸河道總督.md "wikilink")（北河總督）歸[直隸總督兼領](../Page/直隸總督.md "wikilink")。

並依需要另設[副總河](../Page/副總河.md "wikilink")。

  - 未分设之前的歷任總督

| 姓名                                      | 籍貫    | 任職日期                                                                      | 去職日期                                                                      | 備註       |
| --------------------------------------- | ----- | ------------------------------------------------------------------------- | ------------------------------------------------------------------------- | -------- |
| [杨方兴](../Page/杨方兴.md "wikilink")        | 汉军镶白旗 | [顺治元年](../Page/顺治.md "wikilink")（1644年）[七月十九](../Page/七月十九.md "wikilink") | 顺治十四年（1657年）[五月廿一](../Page/五月廿一.md "wikilink")                            | 驻济宁      |
| [朱之錫](../Page/朱之錫.md "wikilink")        | 浙江义乌  | 顺治十四年（1657年）[七月十九](../Page/七月十九.md "wikilink")                            | 顺治十六年（1659年）[十一月初四](../Page/十一月初四.md "wikilink")                          | 驻济宁      |
| [杨茂勋](../Page/杨茂勋.md "wikilink")        | 汉军镶红旗 | 顺治十六年（1659年）[十二月二十](../Page/十二月二十.md "wikilink")                          | 顺治十七年（1660年）[六月初五](../Page/六月初五.md "wikilink")                            | （署）驻济宁   |
| *[白色纯](../Page/白色纯.md "wikilink")*      | 汉军镶白旗 | 顺治十七年（1660年）[六月初十](../Page/六月初十.md "wikilink")                            |                                                                           | （署）驻济宁   |
| *[苗澄](../Page/苗澄.md "wikilink")*        | 直隶任丘  | 顺治十七年（1660年）[七月初六](../Page/七月初六.md "wikilink")                            |                                                                           | （代署）驻济宁  |
| 朱之錫                                     | 浙江义乌  | 顺治十七年（1660年）十二月                                                           | [康熙五年](../Page/康熙.md "wikilink")（1666年）[四月初三](../Page/四月初三.md "wikilink") | 卒于任，驻济宁  |
| [卢崇峻](../Page/卢崇峻.md "wikilink")        | 汉军镶黄旗 | 康熙五年（1666年）[三月十六](../Page/三月十六.md "wikilink")                             | 康熙五年（1666年）[十一月十五](../Page/十一月十五.md "wikilink")                           | 驻济宁      |
| 杨茂勋                                     | 汉军镶红旗 | 康熙五年（1666年）[十二月廿三](../Page/十二月廿三.md "wikilink")                           | 康熙八年（1669年）[九月初六](../Page/九月初六.md "wikilink")                             | 驻济宁      |
| [罗多](../Page/罗多.md "wikilink")          | 汉军旗   | 康熙八年（1669年）[十月初五](../Page/十月初五.md "wikilink")                             | 康熙十年（1671年）[正月廿三](../Page/正月廿三.md "wikilink")                             | 驻济宁      |
| [王光裕](../Page/王光裕.md "wikilink")        | 汉军镶红旗 | 康熙十年（1671年）[二月初七](../Page/二月初七.md "wikilink")                             | 康熙十六年（1677年）[二月初九](../Page/二月初九.md "wikilink")                            | 革职，驻济宁   |
| [靳輔](../Page/靳輔.md "wikilink")          | 汉军镶黄旗 | 康熙十六年（1677年）[二月廿四](../Page/二月廿四.md "wikilink")                            | 康熙二十七年（1688年）[三月十二](../Page/三月十二.md "wikilink")                           | 革职，驻清江浦  |
| *[馬齊](../Page/馬齊.md "wikilink")*        | 满洲镶黄旗 | 康熙二十七年（1688年）三月                                                           |                                                                           | （戡阅河工）   |
| [王新命](../Page/王新命.md "wikilink")        | 汉军镶蓝旗 | 康熙二十七年（1688年）[三月十六](../Page/三月十六.md "wikilink")                           | 康熙三十一年（1692年）[二月初一](../Page/二月初一.md "wikilink")                           | 革职，驻济宁   |
| *[董訥](../Page/董訥.md "wikilink")*        | 山东平原  | 康熙三十一年（1692年）[二月初一](../Page/二月初一.md "wikilink")                           |                                                                           | （署）驻清江浦  |
| 靳輔                                      | 汉军镶黄旗 | 康熙三十一年（1692年）二月初一                                                         | 康熙三十一年（1692年）[十一月十九](../Page/十一月十九.md "wikilink")                         | 卒于任，驻清江浦 |
| [于成龍](../Page/于成龍_\(旗人\).md "wikilink") | 汉军镶黄旗 | 康熙三十一年（1692年）[十二月初八](../Page/十二月初八.md "wikilink")                         | 康熙三十四年（1695年）[八月十四](../Page/八月十四.md "wikilink")                           | 驻清江浦     |
| [董安国](../Page/董安国.md "wikilink")        | 汉军镶红旗 | 康熙三十四年（1695年）[八月二十](../Page/八月二十.md "wikilink")                           | 康熙三十七年（1698年）[十一月廿七](../Page/十一月廿七.md "wikilink")                         | 革职，驻清江浦  |
| 于成龍                                     | 汉军镶黄旗 | 康熙三十七年（1698年）十一月廿七                                                        | 康熙三十九年（1700年）[三月初十](../Page/三月初十.md "wikilink")                           | 卒于任，驻清江浦 |
| [張鵬翮](../Page/張鵬翮.md "wikilink")        | 四川遂宁  | 康熙三十九年（1700年）三月初十                                                         | 康熙四十七年（1708年）[十月十三](../Page/十月十三.md "wikilink")                           | 驻清江浦     |
| [赵世显](../Page/赵世显.md "wikilink")        | 汉军镶黄旗 | 康熙四十七年（1708年）[十一月十一](../Page/十一月十一.md "wikilink")                         | 康熙六十年（1721年）[十一月初四](../Page/十一月初四.md "wikilink")                          | 驻清江浦     |
| [陈鹏年](../Page/陈鹏年.md "wikilink")        | 湖南湘潭  | 康熙六十年（1721年）十一月初四                                                         | [雍正元年](../Page/雍正.md "wikilink")（1723年）[正月十二](../Page/正月十二.md "wikilink") | 驻清江浦     |
| [齐苏勒](../Page/齐苏勒.md "wikilink")        | 满洲正白旗 | 雍正元年（1723年）正月十二                                                           | 雍正七年（1729年）二月                                                             | 卒于任，驻清江浦 |

## 参考文献

  - 钱实甫《清代职官年表》

[\*](../Category/清朝河道總督.md "wikilink")
[Category:清朝官制](../Category/清朝官制.md "wikilink")