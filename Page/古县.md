**古县**是[山西省](../Page/山西省.md "wikilink")[临汾市辖下的一个县](../Page/临汾市.md "wikilink")，人口约9万人，面积1206平方千米。

## 历史

[北魏](../Page/北魏.md "wikilink")[孝庄帝](../Page/孝庄帝.md "wikilink")[建义元年](../Page/建义.md "wikilink")（528年），境内建县，取安吉、泽泉两村名之首字取名为**安泽县**，治古岳村（今[古阳镇古阳村](../Page/古阳镇.md "wikilink")），属[义宁郡](../Page/义宁郡.md "wikilink")。隋[开皇十六年](../Page/开皇.md "wikilink")（596年），改属[沁州](../Page/沁州.md "wikilink")。

隋大业二年（606年），改称**岳阳县**，以地处太岳山南麓而得名，属[临汾郡](../Page/临汾郡.md "wikilink")，县治移往西赤壁（今[旧县镇旧县村](../Page/旧县镇.md "wikilink")）。唐[武德二年](../Page/武德.md "wikilink")（619年）移治东池堡（今[茶坊乡东池村](../Page/茶坊乡.md "wikilink")），[贞观六年](../Page/贞观.md "wikilink")（632年）移治岳阳城（今岳阳镇城关村），归[晋州](../Page/晋州.md "wikilink")。

五代时期，岳阳县改属[临汾郡](../Page/临汾郡.md "wikilink")。宋时属晋州。

元[至正二年](../Page/至正.md "wikilink")（1342年），[冀氏县](../Page/冀氏县.md "wikilink")、[和川县并入岳阳县](../Page/和川县.md "wikilink")。至正四年改称冀氏县，移治今安泽县[冀氏镇](../Page/冀氏镇.md "wikilink")。至正十三年复名岳阳，移治岳阳城，属[平阳府](../Page/平阳府.md "wikilink")。

明、清时，岳阳县治岳阳，属平阳府。

民国三年（1914年）因与湖南岳阳重名，岳阳县改称安泽县，仍治岳阳，属河东道。抗日战争时期（1938—1941年）分设安泽、岳阳、冀氏三县。1942年4月三县合并为安泽县，治义井村，属[晋冀鲁豫边区太岳区](../Page/晋冀鲁豫边区.md "wikilink")。1949年治和川（今安泽县[和川镇](../Page/和川镇.md "wikilink")），归晋南地区。1951年5月移治府城（今安泽县[府城镇](../Page/府城镇.md "wikilink")），归临汾地区。

1971年8月，从安泽县划出七乡、浮山县划出三乡，另设**古县**，属临汾地区行政公署，建治于岳阳（今古县岳阳镇城关村）。1973年在老县城南部建成新县城。
\[1\]

现在古县所辖的区域，历史上属于原安泽县（岳阳县）、浮山县，现在[安泽县所辖的区域则是原冀氏县](../Page/安泽县.md "wikilink")、和川县。

## 地理

位于[东经](../Page/东经.md "wikilink")111度55分，[北纬](../Page/北纬.md "wikilink")36度16分，平均[海拔](../Page/海拔.md "wikilink")663米。

北接[霍州市](../Page/霍州市.md "wikilink")、东邻[长治市的](../Page/长治市.md "wikilink")[沁源县](../Page/沁源县.md "wikilink")，西与[洪洞县接壤](../Page/洪洞县.md "wikilink")，南接[安泽县](../Page/安泽县.md "wikilink")、[浮山县](../Page/浮山县.md "wikilink")。

境内有[洪安涧河由东北至西南入](../Page/洪安涧河.md "wikilink")[汾河](../Page/汾河.md "wikilink")。

## 行政区划

下辖4个[镇](../Page/行政建制镇.md "wikilink")、3个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

## 交通

[309国道](../Page/309国道.md "wikilink")

## 人口

2010年第六次全国人口普查古县人口为91798人。\[2\]

## 经济

2012年，全县生产总值完成62.45亿元。\[3\]

## 风景名胜

  - [山西省文物保护单位](../Page/山西省文物保护单位.md "wikilink")：[热留关帝庙](../Page/热留关帝庙.md "wikilink")
  - [古县文物保护单位](../Page/古县文物保护单位.md "wikilink")
  - [三合牡丹](../Page/三合牡丹.md "wikilink")：誉为“天下第一[牡丹](../Page/牡丹.md "wikilink")”。
  - [蔺相如墓](../Page/蔺相如墓.md "wikilink")
  - [延庆观](../Page/延庆观.md "wikilink")

## 名人

[蔺相如](../Page/蔺相如.md "wikilink")

## 参考文献

[\*](../Category/古县.md "wikilink") [临汾](../Category/山西省县份.md "wikilink")

1.  [历史渊源](http://www.guxian.gov.cn/wMcms_govShowArticle.asp?/1026.html)

2.  [临汾市2010年第六次全国人口普查主要数据公报](http://www.lf-stats.gov.cn/show.aspx?id=309&cid=25)

3.  [古县概况](http://www.guxian.gov.cn/wMcms_govShowArticle.asp?/981.html)