[Laser_Pointer.jpg](https://zh.wikipedia.org/wiki/File:Laser_Pointer.jpg "fig:Laser_Pointer.jpg")
 **激光笔**（Laser
pointer），又稱**激光指示器**、**指星筆**等，是将激光模組（[二極體](../Page/二極體.md "wikilink")）設計、加工制成的便攜、手易持握的发射可見[激光的筆型發射器](../Page/激光.md "wikilink")。常見的激光指示器有[紅光](../Page/紅.md "wikilink")(650-660nm,
635nm)、[綠光](../Page/綠.md "wikilink")(515-520nm,
532nm)、[藍光](../Page/藍.md "wikilink")(445-450nm)、藍[紫光](../Page/紫.md "wikilink")(405nm)等，功率通常以[毫瓦为单位](../Page/毫瓦.md "wikilink")。通常被會報、教學或導賞人員用它來投映一個光點或光柱指向物體。因为激光會傷害到[眼睛](../Page/眼睛.md "wikilink")，任何情況下都不應該讓激光直射眼睛。

## 激光笔的分类

早期的激光笔使用[波长为](../Page/波长.md "wikilink")633[纳米](../Page/纳米.md "wikilink")(nm)的氦氖（HeNe）[气体激光](../Page/气体激光.md "wikilink")，通常用于产生能量不超过1毫瓦的激光束。最便宜的激光笔使用波长接近670/650nm的红色激光二极管。稍贵的则使用635nm的红-橙色二极管，这一波长更易于为人眼所识别。也有其他颜色的激光笔，最常见的是波长为532nm的绿光。最近几年，593.5nm的黄-橙激光笔也开始出现。2005年9月出现了473nm的蓝光激光笔。2010年初\[1\]出现了波长为405nm的蓝光（其实是紫光）激光笔。2012年至2013年随着510/520nm激光二极管的推出，也开始出现510\~520nm波长的蓝绿色激光笔。

激光笔照射出光点的表观亮度不光取决于激光的功率和表面反射率，还取决于人眼的色觉。例如，由于人眼对可见光谱中波长为520-570nm的绿光最敏感，对更红或者更蓝的波长敏感性下降，所以相同功率下绿光显得比其它颜色亮。

### 红/红-橙光

由于有产生该波长的激光二极管，所以其结构最简单，基本上仅是一个由电池做能源的二极管。红光激光笔最早出现于1980年代，是庞大笨重的价值数百美元的设备\[2\]；现在则很小并且很便宜。近些年出现了波长为671nm的二极管[泵浦固体激光](../Page/泵浦固体激光.md "wikilink")(DPSS)红光激光笔。虽然该波长可以用便宜的二极管（用在[DVD機內的](../Page/DVD.md "wikilink")650nm紅色激光）得到，但是DPSS技术可以产生质量更高，频段更窄的激光。

### 黄光

最近市场上出现了波长为593.5nm的黄光激光笔。基于DPSS技术将波长为1064nm和1342nm的两束激光通过一非线性晶体相加而得到。该过程的复杂使得黄光激光笔不稳定且低效率，随温度变化输出功率在1-10毫瓦，如果过热或过冷还会发生模式跳跃。这是因为激光笔的尺寸导致无法提供所需的温度稳定和冷却部件。另外，大部分593.5nm激光笔工作在脉冲模式下以便采用尺寸和功率较小的泵浦二极管。

### 綠光

[Green-lased_palm_tree_(crop).jpg](https://zh.wikipedia.org/wiki/File:Green-lased_palm_tree_\(crop\).jpg "fig:Green-lased_palm_tree_(crop).jpg")。注意因为[瑞利散射和空中的尘埃](../Page/瑞利散射.md "wikilink")，使光束是可见的。\]\]
使用波長808nm[紅外激光激發](../Page/紅外.md "wikilink")[非線性晶體](../Page/非線性.md "wikilink")，產生1064nm紅外光，再經[倍頻產生](../Page/倍頻.md "wikilink")532nm綠光，屬於[半导体泵浦固体激光](../Page/半导体泵浦固体激光.md "wikilink")（DPSS）。

一些绿光激光器工作在脉冲或者准连续模式下来减少冷却问题，延长电池寿命。

近期宣布的\[3\]不需要倍频的绿光激光有着更高的效率。

在夜晚即使是低功率的绿光由于大气分子的[瑞利散射也可以看见](../Page/瑞利散射.md "wikilink")，这种激光笔常被天文学爱好者们用于指点恒星和星座。绿光激光笔可以有多种输出功率。5mW(三类a)使用起来最安全，并且在较暗照明下也可见，所以为指点目的是不需要更强的功率。

### 藍光

原來只有經DPSS產生的473nm藍色激光，功率偏低及不穩定。近來，隨著[CASIO開發出包含藍色激光二極管](../Page/CASIO.md "wikilink")(445nm)的混合光源高亮度[投影機](../Page/投影機.md "wikilink")，功率高達1000mW的藍光半導體二極管被大量生產而普及化。

### 藍紫光

使用波長405nm的藍紫色激光二極管（使用在[藍光光碟內](../Page/藍光光碟.md "wikilink")），屬於半導體激光，接近[紫外光波段](../Page/紫外光.md "wikilink")，可視度較低，但能激發[螢光](../Page/螢光.md "wikilink")，具有[驗鈔和檢驗化學品作用](../Page/驗鈔.md "wikilink")。

## 安全性和管制

在美国，激光由[美国国家标准学会](../Page/美国国家标准学会.md "wikilink")\[4\]和[美国食品药品监督管理局](../Page/美国食品药品监督管理局.md "wikilink")（FDA）分类。功率小于1毫瓦的可见光（波长400-700nm）激光笔为第二类（Class
2 或 II）；功率介于1-5毫瓦的为第三类A（Class 3A 或 IIIa）。第三类B（Class
3B/IIIb）激光（功率5–500毫瓦）和第四类（Class
4/IV）激光（功率大于500毫瓦）按法律不能以激光笔的名义推广销售。\[5\]

2014年3月15日的[中国中央电视台](../Page/中国中央电视台.md "wikilink")“[315晚会](../Page/315晚会.md "wikilink")”曝光了激光笔的潜在危害，随后[淘宝网上的激光笔及相关产品全部下架](../Page/淘宝网.md "wikilink")\[6\]。

## 参考文献

<references/>

[Category:雷射](../Category/雷射.md "wikilink")
[Category:工具](../Category/工具.md "wikilink")
[Category:光學儀器](../Category/光學儀器.md "wikilink")

1.  [真正的 GaN
    二极管激光笔举例](http://www.dinodirect.com/50mw-405nm-mid-open-blue-violet-laser-pointer-stars-kaleidoscopic-laser-pen-2-aaa-included.html)

2.  Product Guide. Popular Science. 1981年11月
3.  [绿色二极管激光，激光技术的重大突破](http://arstechnica.com/science/news/2009/07/green-diode-lasers-a-big-breakthrough-for-laser-display-tech.ars)
    (i-micronews.com via arstechnica.com)
4.  *ANSI 分类方案* (ANSI Z136.1-1993, 美国激光安全使用国家标准)
5.  [FDA:
    给激光笔制造商的重要信息](http://www.fda.gov/cdrh/radhealth/products/LPM.html)
6.  [315晚会后鱼肝油激光笔在淘宝网上瞬间"消失" -
    西部网](http://news.cnwest.com/content/2014-03/15/content_10872825.htm)