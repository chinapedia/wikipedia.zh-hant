**語言接觸**（language
contact）是一個[語言學研究的現象](../Page/語言學.md "wikilink")，發生在不同的語言系統相互互動或影響之時。此種研究又稱**接觸語言學**（contact
linguistics）。

當不同語言的說話者密切的接觸時，這種接觸會影響至少一種語言，並帶來[語音](../Page/語音.md "wikilink")、[句法](../Page/句法.md "wikilink")、[語意或等等社會語言學形式的變化](../Page/語意.md "wikilink")。

## 語言間影響的形式

### 單字借用

最常見的語言影響就是單字的交換，如[英語就常被借入其他語言使用](../Page/英語.md "wikilink")。這種現象不是近代才有，早在[16和](../Page/十六世紀.md "wikilink")[17世紀時](../Page/十七世紀.md "wikilink")，大量的[拉丁語](../Page/拉丁語.md "wikilink")、[法語和其他語言被借入英語使用](../Page/法語.md "wikilink")。更有些語言因借用太多其他語言而導致不易與其他語言區分，如[亞美尼亞語借用相當多的](../Page/亞美尼亞語.md "wikilink")[伊朗語](../Page/伊朗語.md "wikilink")，使得人們認為它是[印度-伊朗語族的其中一個分支](../Page/印度-伊朗語族.md "wikilink")，其實並不然。\[1\]

### 其他語言特色借用

語言間的影響不僅於語言的基本單位特色的交換（如[構詞學或](../Page/構詞學.md "wikilink")[文法](../Page/文法.md "wikilink")），其影響還能更深。[尼泊爾的](../Page/尼泊爾.md "wikilink")[尼瓦爾語就是其中一例](../Page/尼瓦爾語.md "wikilink")，其屬於[漢藏語系中的一種語言](../Page/漢藏語系.md "wikilink")，與[漢語有很遠的關係](../Page/漢語.md "wikilink")，但與周邊的[印度-伊朗語族有幾百年的接觸](../Page/印度-伊朗語族.md "wikilink")，其吸收了印度-伊朗語族的[詞形變化的特色以及文法](../Page/詞形變化.md "wikilink")，如動詞[時態](../Page/時態.md "wikilink")。此外，[羅馬尼亞語在](../Page/羅馬尼亞語.md "wikilink")[羅馬帝國崩解時期](../Page/羅馬帝國.md "wikilink")，曾受到周邊部落的[斯拉夫語的影響](../Page/斯拉夫語.md "wikilink")，除了單字外，也受到[語音和](../Page/語音.md "wikilink")[構詞的影響](../Page/構詞學.md "wikilink")。這樣的影響在語言歷史中相當的常見。

## 互相及非互相的影響

語言接觸後的影響可能是一方的，在[印度](../Page/印度.md "wikilink")，[印地語和其他本土語言受到](../Page/印地語.md "wikilink")[英語大大地影響](../Page/英語.md "wikilink")，日常生活的語言擺脫不了英語詞彙的使用。

然而，語言接觸也會造成兩個語言間的相互影響，譬如[漢語就對](../Page/漢語.md "wikilink")[日語的發展的有莫大影響](../Page/日語.md "wikilink")，反觀近代，由於近代日本的先進，日本語彙大量流入並深深影響現代中文，日常生活的語言擺脫不了日語詞彙的使用。

這樣的情形也常會出現在特定的地理區域裡。譬如在[瑞士](../Page/瑞士.md "wikilink")，當地的[法語一直都受到](../Page/法語.md "wikilink")[德語的影響](../Page/德語.md "wikilink")，反之亦然；在[蘇格蘭](../Page/蘇格蘭.md "wikilink")，[蘇格蘭語深受英語影響](../Page/蘇格蘭語.md "wikilink")，但許多蘇格蘭語的詞彙也變成特定英語方言的特色。

## 語言霸權

顯然地，一個語言的使用者逐漸得到權力時，其語言的影響力就越大，如[中古漢語](../Page/中古漢語.md "wikilink")、[希臘語](../Page/希臘語.md "wikilink")、[拉丁語](../Page/拉丁語.md "wikilink")、[法語](../Page/法語.md "wikilink")、[西班牙語](../Page/西班牙語.md "wikilink")、[阿拉伯語](../Page/阿拉伯語.md "wikilink")、[波斯語](../Page/波斯語.md "wikilink")、[梵語](../Page/梵語.md "wikilink")、[俄語和](../Page/俄語.md "wikilink")[英語在歷史中曾都有不同程度的影響力](../Page/英語.md "wikilink")。

## 方言和次文化的影響

語言接觸也可能只影響某一特定的語言使用族群，因此產生的影響也僅止於一部分的[方言](../Page/方言.md "wikilink")、[行話或文獻記錄](../Page/行話.md "wikilink")（[register](../Page/:en:register.md "wikilink")）。舉例來說，[南非英語深受](../Page/南非英語.md "wikilink")[南非語的影響](../Page/南非語.md "wikilink")，特別是[词汇和](../Page/词汇.md "wikilink")[發音](../Page/發音.md "wikilink")，但整體來說，英語並未受到任何的影響。另外，某些狀況下，一個語言會受到其他優勢語言的影響，如[英格蘭在](../Page/英格蘭.md "wikilink")[中古世紀時](../Page/中古世紀.md "wikilink")，[上流社會的皇宮貴族崇尚](../Page/上流社會.md "wikilink")[法語](../Page/法語.md "wikilink")，因此將大量的法語帶進英語。

## 另見

  - [語言移轉](../Page/語言移轉.md "wikilink")
  - [語碼轉換](../Page/語碼轉換.md "wikilink")（code-switching）
  - [皮欽語](../Page/皮欽語.md "wikilink")（Pidgin）
  - [克里奧語](../Page/克里奧語.md "wikilink")（Creole）
  - [通用語](../Page/通用語.md "wikilink")

## 註釋

## 參考文獻

  - Sarah Thomason and Terrence Kaufman, *Language Contact, Creolization
    and Genetic Linguistics* (University of California Press 1988).
  - Sarah Thomason, *Language Contact - An Introduction* (Edinburgh
    University Press 2001).
  - Uriel Weinreich, *Languages in Contact* (Mouton 1963).
  - Donald Winford, *An Introduction to Contact Linguistics* (Blackwell
    2002) ISBN 0-631-21251-5.
  - Ghil'ad Zuckermann, *Language Contact and Lexical Enrichment in
    Israeli Hebrew* (Palgrave Macmillan 2003) ISBN 1-4039-1723-X.

[\*](../Category/語言接觸.md "wikilink")

1.  Waterman, John (1976). *A History of the German Language.*
    University of Washington Press (p. 4).