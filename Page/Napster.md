**Napster**是一种在线音乐服务，最初由[约翰·范宁](../Page/约翰·范宁.md "wikilink")、[肖恩·范宁和](../Page/肖恩·范宁.md "wikilink")[西恩·帕克共同創立的](../Page/西恩·帕克.md "wikilink")[檔案共享服务](../Page/檔案共享.md "wikilink")。Napster是第一个被广泛应用的[点对点](../Page/点对点.md "wikilink")（Peer-to-Peer，P2P）音乐共享服务，它极大地影响了人们，特别是[大学生使用](../Page/大学生.md "wikilink")[互联网的方式](../Page/互联网.md "wikilink")。它的出现，使音乐爱好者间共享[MP3音乐变得容易](../Page/MP3.md "wikilink")，却也因此招致音像界对其大规模[侵权行为的指责](../Page/侵权.md "wikilink")。尽管在法庭的责令下该服务已经终止，它却给点对点文件共享程序——如[Kazaa](../Page/Kazaa.md "wikilink")，[Limewire和](../Page/Limewire.md "wikilink")[BearShare](../Page/BearShare.md "wikilink")——的拓展铺好了路，对这种方式的文件共享的控制，亦变得愈加困难。如今Napster靠付费服务生存着，免费的Napster的流行和回响使之在电脑界和娱乐业里成为一个传奇的符号。

## 艺术团体的指控

美國重金属乐队金属製品（[Metallica](../Page/Metallica.md "wikilink")）发现他们的一首样本曲目“I
Disappear”早在发布前就流传于Napster网络。这最终使得该曲目在美国各地的数个[电台上被播放](../Page/电台.md "wikilink")，乐队发现他们过去的全部曲目也可在Napster网络上获得，並在當時與樂迷之間發生論戰。

## 关闭

Napster对非法行为的包庇激起数家主要[唱片公司的愤怒](../Page/唱片公司.md "wikilink")，他们联合起来于1999年12月递交了对该共享服务的诉讼。尽管诉讼的目的是要关闭Napster，然而随着审讯的深入，该服务却愈加流行，审讯反而成了对该服务最好的推广和宣传。不久吸引了上百万的用户，其中大部分为学生。

2008年[百思买以](../Page/百思买.md "wikilink")1.21亿美元价格收购了Napster，2011年10月[Rhapsody从百思买收购了Napster](../Page/Rhapsody（線上音樂服務）.md "wikilink")，根据协议，百思买将拥有其少数股权，交易细节并没有披露\[1\]
。Napster将被关闭，它的用户将合并到Rhapsody\[2\] 。2016年，Rhapsody重新把名字改回Napster。

## 参见

  - [LimeWire](../Page/LimeWire.md "wikilink")
  - [WinMX](../Page/WinMX.md "wikilink")
  - [Foxy](../Page/Foxy.md "wikilink")
  - [P2P](../Page/P2P.md "wikilink")
  - [Bit Torrent](../Page/BitTorrent.md "wikilink")

## 参考文献

<div class="references-small">

<references>

</references>

</div>

## 外部链接

  - [Napster公司主页（英语）](http://www.napster.com) -
    前身为[Roxio公司](../Page/Roxio.md "wikilink")
  - [体验Napster](http://www.napsterresearch.com)
  - [Napster侵权事件](http://news.chinabyte.com/307/1218807.shtml)

[Category:P2P](../Category/P2P.md "wikilink")

1.
2.