**XQuartz**（**X11.app**）是[蘋果電腦為](../Page/蘋果電腦.md "wikilink")[Mac OS X/OS
X/macOS上](../Page/macOS.md "wikilink")[X
Window系統的實作](../Page/X_Window系統.md "wikilink")。蘋果的X11是以[XFree86為基礎](../Page/XFree86.md "wikilink")（自OS
X 10.5开始使用 X.org
的实现），加上硬體加速的[2D繪圖支援](../Page/二维计算机图形.md "wikilink")，硬體[OpenGL加速](../Page/OpenGL.md "wikilink")，以及與macOS的[GUI](../Page/GUI.md "wikilink")
[Aqua整合在一起](../Page/Aqua_\(GUI\).md "wikilink")。在[Mac OS X
v10.2時Apple](../Page/Mac_OS_X_v10.2.md "wikilink")
X11原本一開始是當作可以下載的公開測試，後來已經在[Mac OS X
v10.3後包含進標準包裝裡面](../Page/Mac_OS_X_v10.3.md "wikilink")。不过在[OS X Mountain
Lion后](../Page/OS_X_Mountain_Lion.md "wikilink")[OS
X不再附带X](../Page/OS_X.md "wikilink")11\[1\]，用户需要自行前往XQuartz网站下载这个组件。

Apple X11的原始碼可以在蘋果電腦的[Darwin
Project網站以](../Page/Apple_Darwin.md "wikilink")[APSL](../Page/APSL.md "wikilink")（Apple
Public Source License）授權方式取得。

## 外部連結

  -
## 脚注

[Category:MacOS軟體](../Category/MacOS軟體.md "wikilink")
[Category:蘋果公司軟體](../Category/蘋果公司軟體.md "wikilink")

1.  [关于X11和OS X Mountain
    Lion](http://support.apple.com/kb/HT5293?viewlocale=zh_CN)