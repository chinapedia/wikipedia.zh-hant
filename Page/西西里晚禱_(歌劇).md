《**西西里晚禱**》（[法語](../Page/法語.md "wikilink")：、[意大利語](../Page/意大利語.md "wikilink")：）是一部由[朱塞佩·威尔第作曲的五幕](../Page/朱塞佩·威尔第.md "wikilink")[大歌劇](../Page/大歌劇.md "wikilink")。歌劇的腳本是又由夏爾·杜韋里埃（Charles
Duveyrier）和尤金·斯克利伯（Eugène
Scribe）所作的《[阿爾巴公爵](../Page/阿爾巴公爵.md "wikilink")（Le
duc
d'Albe）》\[1\]，並由他們二人親自將腳本套入1282年[西西里晚禱起事的背景](../Page/西西里晚禱.md "wikilink")，從而製成這套歌劇的[辭本](../Page/辭本.md "wikilink")。該劇於1855年6月13日於[巴黎歌劇院首演](../Page/巴黎歌劇院.md "wikilink")，並作為當年巴黎[世界博覽會的一項主要活動](../Page/世界博覽會.md "wikilink")。

如同威爾第所作的《[唐·卡洛斯](../Page/唐·卡洛斯_\(歌劇\).md "wikilink")》一般，意大利語的辭本很快被制備。但為防意大利地區審查當局的干涉，該歌劇在各地以不同名字上演。在[帕爾瑪大公劇院和](../Page/帕爾瑪.md "wikilink")[米蘭](../Page/米蘭.md "wikilink")[斯卡拉大劇院的](../Page/斯卡拉大劇院.md "wikilink")1856樂季，該劇是以《Giovanna
de
Guzman》之名上演的，而在1858年的[那不勒斯](../Page/那不勒斯.md "wikilink")，該劇的名字卻成了《Batilde
di
Turenna》。1860年，兩西西里歸併[薩丁尼亞](../Page/薩丁尼亞.md "wikilink")，後成為[意大利王國](../Page/意大利王國.md "wikilink")，該劇自此得以在意大利以《西西里晚禱（I
vespri siciliani）》之名上演。\[2\]

## 主要角色

## 劇情簡介

## 外部連結

## 備註

<div class="references-small">

<references />

</div>

[Category:法語歌劇](../Category/法語歌劇.md "wikilink")
[Category:意大利語歌劇](../Category/意大利語歌劇.md "wikilink")
[Category:悲劇](../Category/悲劇.md "wikilink")
[Category:歌劇作品](../Category/歌劇作品.md "wikilink")

1.  Michael Kennedy, *The Oxford Concise Dictionary of Music* 2004, ISBN
    0198608845
2.  [Portale Giusseppe
    Verdi](http://www.giuseppeverdi.it/page.asp?IDCategoria=162&IDSezione=580)
    資料