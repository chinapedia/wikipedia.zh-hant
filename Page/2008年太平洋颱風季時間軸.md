<timeline> ImageSize = width:1125 height:210 PlotArea = top:10 bottom:80
right:20 left:20 Legend = columns:3 left:30 top:58 columnwidth:270
AlignBars = early DateFormat = dd/mm/yyyy Period = from:01/01/2008
till:01/01/2009 TimeAxis = orientation:horizontal ScaleMinor =
grid:black unit:month increment:1 start:01/01/2008 Colors =

` id:canvas value:gray(0.88)`
` id:GP     value:red`
` id:TD     value:rgb(0.38,0.73,1)  legend:Tropical_Depression_(TD)_=_<62_km/h_(<39_mph)`
` id:TS     value:rgb(0,0.98,0.96)  legend:Tropical_Storm_(TS)_=_63-88_km/h_(39-54_mph)`
` id:ST     value:rgb(0.80,1,1)     legend:Severe_Tropical_Storm_(STS)_=_89-117_km/h_(55-73_mph)`
` id:TY     value:rgb(0.99,0.69,0.6)     legend:Typhoon_(TY)_=_>118_km/h_(>74_mph)`

Backgroundcolors = canvas:canvas BarData =

` barset:Hurricane`
` bar:Month`

PlotData=

` barset:Hurricane width:10 align:left fontsize:S shift:(4,-4) anchor:till`
` from:13/01/2008 till:17/01/2008 color:TD text:01W`
` from:22/01/2008 till:23/01/2008 color:TD text:TD`
` from:26/03/2008 till:27/03/2008 color:TD text:TD`
` from:12/04/2008 till:19/04/2008 color:TY text:Neoguri`
` from:07/05/2008 till:12/05/2008 color:TY text:Rammasun`
` from:15/05/2008 till:17/05/2008 color:ST text:Matmo`
` from:16/05/2008 till:20/05/2008 color:ST text:Halong`
` from:27/05/2008 till:03/06/2008 color:TY text:Nakri`
` barset:break`
` from:27/05/2008 till:28/05/2008 color:TD text:TD`
` from:06/06/2008 till:06/06/2008 color:TD text:TD`
` from:18/06/2008 till:25/06/2008 color:TY text:Fengshen`
` from:04/07/2008 till:08/07/2008 color:TD text:Gener`
` from:06/07/2008 till:08/07/2008 color:TD text:TD`
` from:12/07/2008 till:20/07/2008 color:TY text:Kalmaegi`
` from:13/07/2008 till:15/07/2008 color:TD text:TD`
` from:23/07/2008 till:29/07/2008 color:TY text:Fung-wong`
` barset:break`
` from:03/08/2008 till:07/08/2008 color:ST text:Kammuri`
` from:09/08/2008 till:11/08/2008 color:ST text:Phanfone `
` from:11/08/2008 till:12/08/2008 color:TD text:TD`
` from:13/08/2008 till:14/08/2008 color:TS text:11W`
` from:14/08/2008 till:15/08/2008 color:TD text:Kika`
` from:14/08/2008 till:17/08/2008 color:ST text:Vongfong`
` from:18/08/2008 till:23/08/2008 color:TY text:Nuri`
` from:25/08/2008 till:28/08/2008 color:TD text:14W`
` barset:break`
` from:08/09/2008 till:21/09/2008 color:TY text:Sinlaku `
` from:09/09/2008 till:11/09/2008 color:TS text:16W`
` from:13/09/2008 till:14/09/2008 color:TS text:17W`
` from:17/09/2008 till:25/09/2008 color:TY text:Hagupit`
` from:23/09/2008 till:01/10/2008 color:TY text:Jangmi`
` from:27/09/2008 till:30/09/2008 color:ST text:Mekkhala`
` from:29/09/2008 till:06/10/2008 color:TS text:Higos`
` from:13/10/2008 till:15/10/2008 color:TS text:22W`
` barset:break`
` from:18/10/2008 till:20/10/2008 color:TS text:Bavi`
` from:06/11/2008 till:12/11/2008 color:ST text:Maysak`
` from:07/11/2008 till:09/11/2008 color:TD text:Rolly`
` from:10/11/2008 till:12/11/2008 color:TD text:TD`
` from:13/11/2008 till:17/11/2008 color:TS text:Noul `
` from:15/11/2008 till:17/11/2008 color:TS text:Haishen`
` from:01/12/2008 till:02/12/2008 color:TD text:TD`
` from:08/12/2008 till:18/12/2008 color:TY text:Dolphin`
` barset:skip`

` bar:Month width:5 align:center fontsize:S shift:(0,-20) anchor:middle color:canvas`
` from:01/01/2008 till:31/01/2008 text:January`
` from:01/02/2008 till:29/02/2008 text:February`
` from:01/03/2008 till:01/04/2008 text:March`
` from:01/04/2008 till:01/05/2008 text:April`
` from:01/05/2008 till:01/06/2008 text:May`
` from:01/06/2008 till:01/07/2008 text:June`
` from:01/07/2008 till:01/08/2008 text:July`
` from:01/08/2008 till:01/09/2008 text:August`
` from:01/09/2008 till:01/10/2008 text:September`
` from:01/10/2008 till:01/11/2008 text:October`
` from:01/11/2008 till:01/12/2008 text:November`
` from:01/12/2008 till:31/12/2008 text:December`

</timeline>

## 1月

  - [1月13日](../Page/1月13日.md "wikilink")

:\*9 a.m. [UTC](../Page/UTC.md "wikilink") -
[馬尼拉西南方發現一股熱帶低氣壓](../Page/馬尼拉.md "wikilink")\[1\]。

  - [1月14日](../Page/1月14日.md "wikilink")

:\*3 a.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將熱帶低氣壓](../Page/聯合颱風警報中心.md "wikilink")01W升格至**熱帶風暴01W**\[2\]。。

:\*9 p.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將熱帶風暴](../Page/聯合颱風警報中心.md "wikilink")01W降格至**熱帶低氣壓01W**\[3\]。

  - [1月15日](../Page/1月15日.md "wikilink")

:\*9 a.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心為熱帶低氣壓](../Page/聯合颱風警報中心.md "wikilink")01W發出最後報告\[4\]。

  - [1月16日](../Page/1月16日.md "wikilink")

:\*3 a.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心為熱帶低氣壓](../Page/聯合颱風警報中心.md "wikilink")01W重新發出報告\[5\]。

:\*3 p.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心為熱帶低氣壓](../Page/聯合颱風警報中心.md "wikilink")01W發出最後報告\[6\]。

## 2月

:\*沒有任何熱帶氣旋形成於西北太平洋。

## 3月

:\*沒有任何熱帶氣旋形成於西北太平洋。

## 4月

  - [4月13日](../Page/4月13日.md "wikilink")

:\*9:45 p.m. [UTC](../Page/UTC.md "wikilink") -
菲律賓大氣地理天文部門將99W.INVEST命名為**熱帶低氣壓Ambo**。

  - [4月14日](../Page/4月14日.md "wikilink")

:\*3 a.m. [UTC](../Page/UTC.md "wikilink") -
[三寶顏北方發現一股熱帶低氣壓](../Page/三寶顏.md "wikilink")（即較早前菲律賓大氣地理天文部門命名的熱帶低氣壓Ambo）。

:\*12 p.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將熱帶低氣壓](../Page/聯合颱風警報中心.md "wikilink")02W升格至**熱帶風暴02W**。

  - [4月15日](../Page/4月15日.md "wikilink")

:\*6 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將熱帶風暴](../Page/日本氣象廳.md "wikilink")02W確認為熱帶風暴，並將它命名為浣熊。

:\*9 a.m. [UTC](../Page/UTC.md "wikilink") -
菲律賓大氣地理天文部門將熱帶低氣壓Ambo升格至**熱帶風暴Ambo**。

  - [4月16日](../Page/4月16日.md "wikilink")

:\*12 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將熱帶風暴浣熊升格為](../Page/日本氣象廳.md "wikilink")**強烈熱帶風暴浣熊**。

:\*9 a.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將熱帶風暴](../Page/聯合颱風警報中心.md "wikilink")02W升格至**颱風02W**。

:\*12 p.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將強烈熱帶風暴浣熊升格為](../Page/日本氣象廳.md "wikilink")**颱風浣熊**。

  - [4月18日](../Page/4月18日.md "wikilink")

:\*2:30 p.m. [UTC](../Page/UTC.md "wikilink") -
[中國氣象局宣布](../Page/中國氣象局.md "wikilink")**颱風浣熊在[文昌市登陸](../Page/文昌市.md "wikilink")**。（不獲[日本氣象廳及](../Page/日本氣象廳.md "wikilink")[聯合颱風警報中心承認](../Page/聯合颱風警報中心.md "wikilink")）

  - [4月19日](../Page/4月19日.md "wikilink")

:\*12 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將颱風浣熊降格為](../Page/日本氣象廳.md "wikilink")**強烈熱帶風暴浣熊**。

:\*3 a.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將颱風](../Page/聯合颱風警報中心.md "wikilink")02W降格至**熱帶風暴02W**。

:\*6 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將強烈熱帶風暴浣熊降格為](../Page/日本氣象廳.md "wikilink")**熱帶風暴浣熊**。

:\*c. 9 a.m. [UTC](../Page/UTC.md "wikilink") -
**熱帶風暴浣熊在[陽江登陸](../Page/陽江.md "wikilink")。**

:\*3 p.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心為熱帶風暴](../Page/聯合颱風警報中心.md "wikilink")02W發出最後報告。

:\*6 p.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將熱帶風暴浣熊降格為](../Page/日本氣象廳.md "wikilink")**熱帶低氣壓浣熊**。

:\*6 p.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳為熱帶低氣壓浣熊發出最後報告](../Page/日本氣象廳.md "wikilink")。

## 5月

  - [5月7日](../Page/5月7日.md "wikilink")

:\*3 a.m. [UTC](../Page/UTC.md "wikilink") -
菲律賓大氣地理天文部門將94W.INVEST命名為**熱帶低氣壓Butchoy**。

:\*9 a.m. [UTC](../Page/UTC.md "wikilink") -
[帛琉西方發現一股熱帶低氣壓](../Page/帛琉.md "wikilink")（即較早前菲律賓大氣地理天文部門命名的熱帶低氣壓Butchoy）。

:\*3 p.m. [UTC](../Page/UTC.md "wikilink") -
菲律賓大氣地理天文部門將熱帶低氣壓Butchoy升格至**熱帶風暴Butchoy**。

:\*6 p.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將熱帶低氣壓](../Page/日本氣象廳.md "wikilink")03W升格為熱帶風暴，並將它命名為威馬遜。

:\*9 p.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將熱帶低氣壓](../Page/聯合颱風警報中心.md "wikilink")03W升格至**熱帶風暴03W**。

  - [5月8日](../Page/5月8日.md "wikilink")

:\*6 p.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將熱帶風暴威馬遜升格為](../Page/日本氣象廳.md "wikilink")**強烈熱帶風暴威馬遜**。

  - [5月9日](../Page/5月9日.md "wikilink")

:\*3 a.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將熱帶風暴](../Page/聯合颱風警報中心.md "wikilink")03W升格至**颱風03W**。

:\*6 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將強烈熱帶風暴威馬遜升格為](../Page/日本氣象廳.md "wikilink")**颱風威馬遜**。

:\*3 p.m. [UTC](../Page/UTC.md "wikilink") -
菲律賓大氣地理天文部門將熱帶風暴Butchoy升格至**颱風Butchoy**。

  - [5月10日](../Page/5月10日.md "wikilink")

:\*9 a.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將颱風](../Page/聯合颱風警報中心.md "wikilink")03W升格至**超級颱風03W**。

  - [5月11日](../Page/5月11日.md "wikilink")

:\*9 a.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將超級颱風](../Page/聯合颱風警報中心.md "wikilink")03W降格至**颱風03W**。

  - [5月12日](../Page/5月12日.md "wikilink")

:\*6 p.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將颱風威馬遜降格為](../Page/日本氣象廳.md "wikilink")**強烈熱帶風暴威馬遜**。

:\*9 p.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將颱風](../Page/聯合颱風警報中心.md "wikilink")03W降格至**熱帶風暴03W**。

:\*9 p.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心為熱帶風暴](../Page/聯合颱風警報中心.md "wikilink")03W發出最後報告。

  - [5月13日](../Page/5月13日.md "wikilink")

:\*6 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將強烈熱帶風暴威馬遜降格為](../Page/日本氣象廳.md "wikilink")**溫帶氣旋威馬遜**。

:\*6 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳為溫帶氣旋威馬遜發出最後報告](../Page/日本氣象廳.md "wikilink")。

  - [5月14日](../Page/5月14日.md "wikilink")

:\*9 a.m. [UTC](../Page/UTC.md "wikilink") -
[馬尼拉東北方發現一股熱帶低氣壓](../Page/馬尼拉.md "wikilink")。

:\*9 a.m. [UTC](../Page/UTC.md "wikilink") -
菲律賓大氣地理天文部門將95W.INVEST命名為**熱帶低氣壓Cosme**。

  - [5月15日](../Page/5月15日.md "wikilink")

:\*12 p.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將熱帶低氣壓](../Page/日本氣象廳.md "wikilink")04W升格為熱帶風暴，並將它命名為麥德姆。

:\*3 p.m. [UTC](../Page/UTC.md "wikilink") -
菲律賓大氣地理天文部門將熱帶風暴麥德姆命名為**熱帶低氣壓Dindo**。

:\*3 p.m. [UTC](../Page/UTC.md "wikilink") -
[馬尼拉西南方發現一股熱帶低氣壓](../Page/馬尼拉.md "wikilink")（即較早前菲律賓大氣地理天文部門命名的熱帶低氣壓Cosme）。

:\*9 p.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將熱帶低氣壓](../Page/聯合颱風警報中心.md "wikilink")04W升格至**熱帶風暴04W**。

:\*9 p.m. [UTC](../Page/UTC.md "wikilink") -
菲律賓大氣地理天文部門將熱帶低氣壓Cosme升格至**熱帶風暴Cosme**。

:\*9 p.m. [UTC](../Page/UTC.md "wikilink") -
菲律賓大氣地理天文部門將熱帶低氣壓Dindo升格至**熱帶風暴Dindo**。

  - [5月16日](../Page/5月16日.md "wikilink")

:\*6 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將熱帶低氣壓](../Page/日本氣象廳.md "wikilink")05W升格為熱帶風暴，並將它命名為夏浪。

:\*6 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將熱帶風暴麥德姆升格為](../Page/日本氣象廳.md "wikilink")**強烈熱帶風暴麥德姆**。（事後報告加註
\[<https://www.webcitation.org/5YcDYNf6v?url=http://twister.sbs.ohio-state.edu/text/station/RJTD/AXPQ20.RJTD%5D%EF%BC%89>

:\*9 a.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將熱帶低氣壓](../Page/聯合颱風警報中心.md "wikilink")05W升格至**熱帶風暴05W**。

:\*12 p.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將強烈熱帶風暴麥德姆降格為](../Page/日本氣象廳.md "wikilink")**熱帶風暴麥德姆**。（事後報告加註）

:\*9 a.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心為熱帶風暴](../Page/聯合颱風警報中心.md "wikilink")04W發出最後報告。

  - [5月17日](../Page/5月17日.md "wikilink")

:\*12 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將熱帶風暴夏浪升格為](../Page/日本氣象廳.md "wikilink")**強烈熱帶風暴夏浪**。

:\*12 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將熱帶風暴麥德姆降格為](../Page/日本氣象廳.md "wikilink")**溫帶氣旋麥德姆**。

:\*12 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳為溫帶氣旋麥德姆發出最後報告](../Page/日本氣象廳.md "wikilink")。

:\*9 a.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將熱帶風暴](../Page/聯合颱風警報中心.md "wikilink")05W升格至**颱風05W**。

:\*c. 11 a.m. [UTC](../Page/UTC.md "wikilink") -
**強烈熱帶風暴夏浪在Pangasinan省登陸。**

:\*c. 1 p.m. [UTC](../Page/UTC.md "wikilink") - **強烈熱帶風暴夏浪在La
Union省登陸。**

:\*9 p.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將颱風](../Page/聯合颱風警報中心.md "wikilink")05W降格至**熱帶風暴05W**。

  - [5月18日](../Page/5月18日.md "wikilink")

:\*12 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將強烈熱帶風暴夏浪降格為](../Page/日本氣象廳.md "wikilink")**熱帶風暴夏浪**。

:\*6 p.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將熱帶風暴夏浪升格為](../Page/日本氣象廳.md "wikilink")**強烈熱帶風暴夏浪**。

  - [5月19日](../Page/5月19日.md "wikilink")

:\*6 p.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將強烈熱帶風暴夏浪降格為](../Page/日本氣象廳.md "wikilink")**熱帶風暴夏浪**。

  - [5月20日](../Page/5月20日.md "wikilink")

:\*3 a.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心為熱帶風暴](../Page/聯合颱風警報中心.md "wikilink")05W發出最後報告。

:\*12 p.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將熱帶風暴夏浪降格為](../Page/日本氣象廳.md "wikilink")**溫帶氣旋夏浪**。

:\*12 p.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳為溫帶氣旋夏浪發出最後報告](../Page/日本氣象廳.md "wikilink")。

  - [5月27日](../Page/5月27日.md "wikilink")

:\*3 a.m. [UTC](../Page/UTC.md "wikilink") -
[雅蒲島北方發現一股熱帶低氣壓](../Page/雅蒲島.md "wikilink")。

:\*6 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將熱帶低氣壓](../Page/日本氣象廳.md "wikilink")06W升格為熱帶風暴，並將它命名為娜基莉。

:\*9 a.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將熱帶低氣壓](../Page/聯合颱風警報中心.md "wikilink")06W升格至**熱帶風暴06W**。

  - [5月28日](../Page/5月28日.md "wikilink")

:\*12 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將熱帶風暴娜基莉升格為](../Page/日本氣象廳.md "wikilink")**強烈熱帶風暴娜基莉**。

:\*3 p.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將熱帶風暴](../Page/聯合颱風警報中心.md "wikilink")06W升格至**颱風06W**。

:\*6 p.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將強烈熱帶風暴娜基莉升格為](../Page/日本氣象廳.md "wikilink")**颱風娜基莉**。

  - [5月29日](../Page/5月29日.md "wikilink")

:\*9 p.m. [UTC](../Page/UTC.md "wikilink") -
菲律賓大氣地理天文部門將颱風娜基莉命名為**颱風Enteng**。

## 6月

  - [6月2日](../Page/6月2日.md "wikilink")

:\*12 p.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將颱風娜基莉降格為](../Page/日本氣象廳.md "wikilink")**強烈熱帶風暴娜基莉**。

:\*9 p.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將颱風](../Page/聯合颱風警報中心.md "wikilink")06W降格至**熱帶風暴06W**。

  - [6月3日](../Page/6月3日.md "wikilink")

:\*3 a.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心為熱帶風暴](../Page/聯合颱風警報中心.md "wikilink")06W發出最後報告。

:\*6 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將強烈熱帶風暴娜基莉降格為](../Page/日本氣象廳.md "wikilink")**溫帶氣旋娜基莉**。

:\*6 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳為溫帶氣旋娜基莉發出最後報告](../Page/日本氣象廳.md "wikilink")。

  - [6月18日](../Page/6月18日.md "wikilink")

:\*9 a.m. [UTC](../Page/UTC.md "wikilink") -
菲律賓大氣地理天文部門將94W.INVEST命名為**熱帶低氣壓Frank**。

:\*3 p.m. [UTC](../Page/UTC.md "wikilink") -
[帛琉西北方發現一股熱帶低氣壓](../Page/帛琉.md "wikilink")（即較早前菲律賓大氣地理天文部門命名的熱帶低氣壓Frank）。

:\*9 p.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將熱帶低氣壓](../Page/聯合颱風警報中心.md "wikilink")07W升格至**熱帶風暴07W**。

  - [6月19日](../Page/6月19日.md "wikilink")

:\*12 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將熱帶低氣壓](../Page/日本氣象廳.md "wikilink")07W升格為熱帶風暴，並將它命名為風神。

:\*3 a.m. [UTC](../Page/UTC.md "wikilink") -
菲律賓大氣地理天文部門將熱帶低氣壓Frank升格至**熱帶風暴Frank**。

:\*12 p.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將熱帶風暴風神升格為](../Page/日本氣象廳.md "wikilink")**強烈熱帶風暴風神**。

:\*6 p.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將強烈熱帶風暴風神升格為](../Page/日本氣象廳.md "wikilink")**颱風風神**。

:\*9 p.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將熱帶風暴](../Page/聯合颱風警報中心.md "wikilink")07W升格至**颱風07W**。

  - [6月20日](../Page/6月20日.md "wikilink")

:\*3 a.m. [UTC](../Page/UTC.md "wikilink") -
菲律賓大氣地理天文部門將熱帶風暴Frank升格至**颱風Frank**。

:\*c. 6 a.m. [UTC](../Page/UTC.md "wikilink") -
**颱風風神在[薩馬島登陸](../Page/薩馬島.md "wikilink")。**

:\*c. 12 p.m. [UTC](../Page/UTC.md "wikilink") - **颱風風神在馬斯巴特島登陸。**

  - [6月21日](../Page/6月21日.md "wikilink")

:\*c. 9 a.m. [UTC](../Page/UTC.md "wikilink") - **颱風風神在Tablas島登陸。**

:\*c. 3 p.m. [UTC](../Page/UTC.md "wikilink") - **颱風風神在馬林杜克島登陸。**

:\*c. 6 p.m. [UTC](../Page/UTC.md "wikilink") - **颱風風神在奎松省登陸。**

  - [6月22日](../Page/6月22日.md "wikilink")

:\*c. 3 a.m. [UTC](../Page/UTC.md "wikilink") -
**颱風風神在[布拉乾省登陸](../Page/布拉乾省.md "wikilink")。**

  - [6月23日](../Page/6月23日.md "wikilink")

:\*12 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將颱風風神降格為](../Page/日本氣象廳.md "wikilink")**強烈熱帶風暴風神**。

:\*3 a.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將颱風](../Page/聯合颱風警報中心.md "wikilink")07W降格至**熱帶風暴07W**。

:\*3 a.m. [UTC](../Page/UTC.md "wikilink") -
菲律賓大氣地理天文部門將颱風Frank降格至**熱帶風暴Frank**。

  - [6月24日](../Page/6月24日.md "wikilink")

:\*c. 9 p.m. [UTC](../Page/UTC.md "wikilink") -
**強烈熱帶風暴風神在[香港登陸](../Page/香港.md "wikilink")。**

  - [6月25日](../Page/6月25日.md "wikilink")

:\*12 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將強烈熱帶風暴風神降格為](../Page/日本氣象廳.md "wikilink")**熱帶風暴風神**。

:\*3 p.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將熱帶風暴](../Page/聯合颱風警報中心.md "wikilink")07W降格至**熱帶低氣壓07W**。

:\*3 p.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心為熱帶低氣壓](../Page/聯合颱風警報中心.md "wikilink")07W發出最後報告。

:\*6 p.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將熱帶風暴風神降格為](../Page/日本氣象廳.md "wikilink")**熱帶低氣壓風神**。

:\*6 p.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳為熱帶低氣壓風神發出最後報告](../Page/日本氣象廳.md "wikilink")。

## 7月

  - [7月4日](../Page/7月4日.md "wikilink")

:\*3:30 a.m. [UTC](../Page/UTC.md "wikilink") -
菲律賓大氣地理天文部門將98W.INVEST命名為**熱帶低氣壓Gener**。

  - [7月13日](../Page/7月13日.md "wikilink")

:\*9 p.m. [UTC](../Page/UTC.md "wikilink") -
菲律賓大氣地理天文部門將93W.INVEST命名為**熱帶低氣壓Helen**。

  - [7月14日](../Page/7月14日.md "wikilink")

:\*9 a.m. [UTC](../Page/UTC.md "wikilink") -
[台北東南方發現一股熱帶低氣壓](../Page/台北.md "wikilink")（即較早前菲律賓大氣地理天文部門命名的熱帶低氣壓Helen）。

:\*9 a.m. [UTC](../Page/UTC.md "wikilink") -
菲律賓大氣地理天文部門將熱帶低氣壓Helen升格至**熱帶風暴Helen**。

  - [7月15日](../Page/7月15日.md "wikilink")

:\*3 a.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將熱帶低氣壓](../Page/聯合颱風警報中心.md "wikilink")08W升格至**熱帶風暴08W**。

:\*6 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將熱帶風暴](../Page/日本氣象廳.md "wikilink")08W確認為熱帶風暴，並將它命名為海鷗。

:\*9 a.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將熱帶風暴](../Page/聯合颱風警報中心.md "wikilink")08W降格至**熱帶低氣壓08W**。

:\*3 p.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將熱帶低氣壓](../Page/聯合颱風警報中心.md "wikilink")08W升格至**熱帶風暴08W**。

  - [7月16日](../Page/7月16日.md "wikilink")

:\*6 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將熱帶風暴海鷗升格為](../Page/日本氣象廳.md "wikilink")**強烈熱帶風暴海鷗**。

  - [7月17日](../Page/7月17日.md "wikilink")

:\*12 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將強烈熱帶風暴海鷗升格為](../Page/日本氣象廳.md "wikilink")**颱風海鷗**。

:\*3 a.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將熱帶風暴](../Page/聯合颱風警報中心.md "wikilink")08W升格至**颱風08W**。

:\*9 a.m. [UTC](../Page/UTC.md "wikilink") -
菲律賓大氣地理天文部門將熱帶風暴Helen升格至**颱風Helen**。

:\*c. 3 p.m. [UTC](../Page/UTC.md "wikilink") -
**颱風海鷗在[宜蘭縣登陸](../Page/宜蘭縣.md "wikilink")。**

:\*9 p.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將颱風海鷗降格為](../Page/日本氣象廳.md "wikilink")**強烈熱帶風暴海鷗**。

  - [7月18日](../Page/7月18日.md "wikilink")

:\*3 a.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將颱風](../Page/聯合颱風警報中心.md "wikilink")08W降格至**熱帶風暴08W**。

:\*6 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將強烈熱帶風暴海鷗降格為](../Page/日本氣象廳.md "wikilink")**熱帶風暴海鷗**。

:\*c. 12 p.m. [UTC](../Page/UTC.md "wikilink") -
**熱帶風暴海鷗在[福建省登陸](../Page/福建省.md "wikilink")。**

:\*9 p.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心為熱帶風暴](../Page/聯合颱風警報中心.md "wikilink")08W發出最後報告。

  - [7月19日](../Page/7月19日.md "wikilink")

:\*c. 6 a.m. [UTC](../Page/UTC.md "wikilink") -
**熱帶風暴海鷗在[浙江省登陸](../Page/浙江省.md "wikilink")。**

:\*c. 3 p.m. [UTC](../Page/UTC.md "wikilink") -
**熱帶風暴海鷗在[江蘇省登陸](../Page/江蘇省.md "wikilink")。**

  - [7月20日](../Page/7月20日.md "wikilink")

:\*c. 3 p.m. [UTC](../Page/UTC.md "wikilink") -
**熱帶風暴海鷗在[黃海南道登陸](../Page/黃海南道.md "wikilink")。**

:\*6 p.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將熱帶風暴海鷗降格為](../Page/日本氣象廳.md "wikilink")**溫帶氣旋海鷗**。

:\*6 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳為溫帶氣旋海鷗發出最後報告](../Page/日本氣象廳.md "wikilink")。

  - [7月24日](../Page/7月24日.md "wikilink")

:\*3:30 a.m. [UTC](../Page/UTC.md "wikilink") -
菲律賓大氣地理天文部門將97W.INVEST命名為**熱帶低氣壓Igme**。

:\*3 p.m. [UTC](../Page/UTC.md "wikilink") -
[那霸東南方發現一股熱帶低氣壓](../Page/那霸.md "wikilink")（即較早前菲律賓大氣地理天文部門命名的熱帶低氣壓Igme）。

:\*3 p.m. [UTC](../Page/UTC.md "wikilink") -
菲律賓大氣地理天文部門將熱帶低氣壓Igme升格至**熱帶風暴Igme**。

  - [7月25日](../Page/7月25日.md "wikilink")

:\*3 a.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將熱帶低氣壓](../Page/聯合颱風警報中心.md "wikilink")09W升格至**熱帶風暴09W**。

:\*6 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將熱帶風暴](../Page/日本氣象廳.md "wikilink")09W確認為熱帶風暴，並將它命名為鳳凰。

  - [7月26日](../Page/7月26日.md "wikilink")

:\*12 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將熱帶風暴鳳凰升格為](../Page/日本氣象廳.md "wikilink")**強烈熱帶風暴鳳凰**。

:\*3 p.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將熱帶風暴](../Page/聯合颱風警報中心.md "wikilink")09W升格至**颱風09W**。

:\*3 p.m. [UTC](../Page/UTC.md "wikilink") -
菲律賓大氣地理天文部門將熱帶風暴Igme升格至**颱風Igme**。

  - [7月27日](../Page/7月27日.md "wikilink")

:\*12 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將強烈熱帶風暴鳳凰升格為](../Page/日本氣象廳.md "wikilink")**颱風鳳凰**。

  - [7月28日](../Page/7月28日.md "wikilink")

:\*c. 12 a.m. [UTC](../Page/UTC.md "wikilink") -
**颱風鳳凰在[台東縣登陸](../Page/台東縣.md "wikilink")。**

:\*6 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將颱風鳳凰降格為](../Page/日本氣象廳.md "wikilink")**強烈熱帶風暴鳳凰**。

:\*c. 3 p.m. [UTC](../Page/UTC.md "wikilink") -
**強烈熱帶風暴鳳凰在[福建省登陸](../Page/福建省.md "wikilink")。**

:\*3 p.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將颱風](../Page/聯合颱風警報中心.md "wikilink")09W降格至**熱帶風暴09W**。

:\*9 p.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心為熱帶風暴](../Page/聯合颱風警報中心.md "wikilink")09W發出最後報告。

  - [7月29日](../Page/7月29日.md "wikilink")

:\*12 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將強烈熱帶風暴鳳凰降格為](../Page/日本氣象廳.md "wikilink")**熱帶風暴鳳凰**。

:\*12 p.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將熱帶風暴鳳凰降格為](../Page/日本氣象廳.md "wikilink")**熱帶低氣壓鳳凰**。

:\*12 p.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳為熱帶低氣壓鳳凰發出最後報告](../Page/日本氣象廳.md "wikilink")。

## 8月

  - [8月3日](../Page/8月3日.md "wikilink")

:\*3 p.m. [UTC](../Page/UTC.md "wikilink") -
菲律賓大氣地理天文部門將90W.INVEST命名為**熱帶低氣壓Julian**。

  - [8月4日](../Page/8月4日.md "wikilink")

:\*3 a.m. [UTC](../Page/UTC.md "wikilink") -
[香港東南方發現一股熱帶低氣壓](../Page/香港.md "wikilink")（即較早前菲律賓大氣地理天文部門命名的熱帶低氣壓Julian）。

:\*3 a.m. [UTC](../Page/UTC.md "wikilink") -
菲律賓大氣地理天文部門將熱帶低氣壓Julian升格至**熱帶風暴Julian**。

:\*3 p.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將熱帶低氣壓](../Page/聯合颱風警報中心.md "wikilink")10W升格至**熱帶風暴10W**。

  - [8月5日](../Page/8月5日.md "wikilink")

:\*6 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將熱帶風暴](../Page/日本氣象廳.md "wikilink")10W確認為熱帶風暴，並將它命名為北冕。

  - [8月6日](../Page/8月6日.md "wikilink")

:\*12 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將熱帶風暴北冕升格為](../Page/日本氣象廳.md "wikilink")**強烈熱帶風暴北冕**。

:\*c. 6 a.m. [UTC](../Page/UTC.md "wikilink") -
'''強烈熱帶風暴北冕在[上川島登陸](../Page/上川島.md "wikilink")。

:\*c. 6 a.m. [UTC](../Page/UTC.md "wikilink") - **強烈熱帶風暴北冕在下川島登陸。**

:\*c. 9 a.m. [UTC](../Page/UTC.md "wikilink") -
**強烈熱帶風暴北冕在[陽江市登陸](../Page/陽江市.md "wikilink")。**

:\*c. 9 a.m. [UTC](../Page/UTC.md "wikilink") -
**強烈熱帶風暴北冕第二次在[陽江市登陸](../Page/陽江市.md "wikilink")。**

:\*c. 9 a.m. [UTC](../Page/UTC.md "wikilink") -
**強烈熱帶風暴北冕第三次在[陽江市登陸](../Page/陽江市.md "wikilink")。**

:\*12 p.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將強烈熱帶風暴北冕降格為](../Page/日本氣象廳.md "wikilink")**熱帶風暴北冕**。

:\*9 p.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心為熱帶風暴](../Page/聯合颱風警報中心.md "wikilink")10W發出最後報告。

  - [8月7日](../Page/8月7日.md "wikilink")

:\*c. 9 a.m. [UTC](../Page/UTC.md "wikilink") -
**熱帶風暴北冕在[防城港市登陸](../Page/防城港市.md "wikilink")。**

:\*6 p.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將熱帶風暴北冕降格為](../Page/日本氣象廳.md "wikilink")**熱帶低氣壓北冕**。

:\*6 p.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳為熱帶低氣壓北冕發出最後報告](../Page/日本氣象廳.md "wikilink")。

  - [8月10日](../Page/8月10日.md "wikilink")

:\*6 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將](../Page/日本氣象廳.md "wikilink")95W.INVEST升格為熱帶風暴，並將它命名為巴蓬。

  - [8月11日](../Page/8月11日.md "wikilink")

:\*6 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將熱帶風暴巴蓬降格為](../Page/日本氣象廳.md "wikilink")**溫帶氣旋巴蓬**。

:\*6 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳為溫帶氣旋巴蓬發出最後報告](../Page/日本氣象廳.md "wikilink")。

  - [8月13日](../Page/8月13日.md "wikilink")

:\*3 a.m. [UTC](../Page/UTC.md "wikilink") -
[那霸北方發現一股熱帶低氣壓](../Page/那霸.md "wikilink")。

  - [8月14日](../Page/8月14日.md "wikilink")

:\*3 p.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心為熱帶低氣壓](../Page/聯合颱風警報中心.md "wikilink")11W發出最後報告。

:\*9 p.m. [UTC](../Page/UTC.md "wikilink") -
[東京西南方發現一股熱帶低氣壓](../Page/東京.md "wikilink")。

  - [8月15日](../Page/8月15日.md "wikilink")

:\*3 a.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將熱帶低氣壓](../Page/聯合颱風警報中心.md "wikilink")12W升格至**熱帶風暴12W**。

:\*6 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將熱帶風暴](../Page/日本氣象廳.md "wikilink")12W確認為熱帶風暴，並將它命名為黃蜂。

  - [8月16日](../Page/8月16日.md "wikilink")

:\*9 p.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心為熱帶風暴](../Page/聯合颱風警報中心.md "wikilink")12W發出最後報告。

  - [8月17日](../Page/8月17日.md "wikilink")

:\*3 a.m. [UTC](../Page/UTC.md "wikilink") -
[關島西北方發現一股熱帶低氣壓](../Page/關島.md "wikilink")。

:\*6 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將熱帶風暴黃蜂降格為](../Page/日本氣象廳.md "wikilink")**溫帶氣旋黃蜂**。

:\*6 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳為溫帶氣旋黃蜂發出最後報告](../Page/日本氣象廳.md "wikilink")。

:\*3 p.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將熱帶低氣壓](../Page/聯合颱風警報中心.md "wikilink")13W升格至**熱帶風暴13W**。

:\*3 p.m. [UTC](../Page/UTC.md "wikilink") -
菲律賓大氣地理天文部門將熱帶風暴13W命名為**熱帶低氣壓Karen**。

:\*9 p.m. [UTC](../Page/UTC.md "wikilink") -
菲律賓大氣地理天文部門將熱帶低氣壓Karen升格至**熱帶風暴Karen**。

  - [8月18日](../Page/8月18日.md "wikilink")

:\*12 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將熱帶風暴](../Page/日本氣象廳.md "wikilink")13W確認為熱帶風暴，並將它命名為鸚鵡。

:\*12 p.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將熱帶風暴鸚鵡升格為](../Page/日本氣象廳.md "wikilink")**強烈熱帶風暴鸚鵡**。

:\*3 p.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將熱帶風暴](../Page/聯合颱風警報中心.md "wikilink")13W升格至**颱風13W**。

:\*6 p.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將強烈熱帶風暴鸚鵡升格為](../Page/日本氣象廳.md "wikilink")**颱風鸚鵡**。

:\*9 p.m. [UTC](../Page/UTC.md "wikilink") -
菲律賓大氣地理天文部門將熱帶風暴Karen升格至**颱風Karen**。

  - [8月21日](../Page/8月21日.md "wikilink")

:\*6 p.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將颱風鸚鵡降格為](../Page/日本氣象廳.md "wikilink")**強烈熱帶風暴鸚鵡**。

:\*9 p.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將颱風](../Page/聯合颱風警報中心.md "wikilink")13W降格至**熱帶風暴13W**。

  - [8月22日](../Page/8月22日.md "wikilink")

:\*c. 12 p.m. [UTC](../Page/UTC.md "wikilink") -
**強烈熱帶風暴鸚鵡在[香港登陸](../Page/香港.md "wikilink")。**

:\*12 p.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將強烈熱帶風暴鸚鵡降格為](../Page/日本氣象廳.md "wikilink")**熱帶風暴鸚鵡**。

:\*9 p.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心為熱帶風暴](../Page/聯合颱風警報中心.md "wikilink")13W發出最後報告。

  - [8月23日](../Page/8月23日.md "wikilink")

:\*12 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將熱帶風暴鸚鵡降格為](../Page/日本氣象廳.md "wikilink")**熱帶低氣壓鸚鵡**。

:\*12 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳為熱帶低氣壓鸚鵡發出最後報告](../Page/日本氣象廳.md "wikilink")。

  - [8月25日](../Page/8月25日.md "wikilink")

:\*9 a.m. [UTC](../Page/UTC.md "wikilink") -
菲律賓大氣地理天文部門將98W.INVEST命名為**熱帶低氣壓Lawin**。

  - [8月26日](../Page/8月26日.md "wikilink")

:\*3 p.m. [UTC](../Page/UTC.md "wikilink") -
[馬尼拉東北方發現一股熱帶低氣壓](../Page/馬尼拉.md "wikilink")（即較早前菲律賓大氣地理天文部門命名的熱帶低氣壓Lawin）。

  - [8月27日](../Page/8月27日.md "wikilink")

:\*3 a.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將熱帶低氣壓](../Page/聯合颱風警報中心.md "wikilink")14W升格至**熱帶風暴14W**。

:\*9 a.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將熱帶風暴](../Page/聯合颱風警報中心.md "wikilink")14W降格至**熱帶低氣壓14W**。

  - [8月28日](../Page/8月28日.md "wikilink")

:\*3 a.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心為熱帶低氣壓](../Page/聯合颱風警報中心.md "wikilink")14W發出最後報告。

:\*9 a.m. [UTC](../Page/UTC.md "wikilink") -
菲律賓大氣地理天文部門將熱帶低氣壓Lawin降格至**低壓區Lawin**。

## 9月

  - [9月8日](../Page/9月8日.md "wikilink")

:\*3 a.m. [UTC](../Page/UTC.md "wikilink") -
菲律賓大氣地理天文部門將95W.INVEST命名為**熱帶低氣壓Marce**。

:\*9 a.m. [UTC](../Page/UTC.md "wikilink") -
菲律賓大氣地理天文部門將熱帶低氣壓Marce升格至**熱帶風暴Marce**。

:\*3 p.m. [UTC](../Page/UTC.md "wikilink") -
[馬尼拉東北方發現一股熱帶低氣壓](../Page/馬尼拉.md "wikilink")（即較早前菲律賓大氣地理天文部門命名的熱帶風暴Marce）。

:\*6 p.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將熱帶低氣壓](../Page/日本氣象廳.md "wikilink")15W升格為熱帶風暴，並將它命名為森垃克。

:\*9 p.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將熱帶低氣壓](../Page/聯合颱風警報中心.md "wikilink")15W升格至**熱帶風暴15W**。

  - [9月9日](../Page/9月9日.md "wikilink")

:\*6 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將熱帶風暴森垃克升格為](../Page/日本氣象廳.md "wikilink")**強烈熱帶風暴森垃克**。

:\*3 p.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將熱帶風暴](../Page/聯合颱風警報中心.md "wikilink")15W升格至**颱風15W**。

:\*6 p.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將強烈熱帶風暴森垃克升格為](../Page/日本氣象廳.md "wikilink")**颱風森垃克**。

:\*9 p.m. [UTC](../Page/UTC.md "wikilink") -
菲律賓大氣地理天文部門將熱帶風暴Marce升格至**颱風Marce**。

  - [9月10日](../Page/9月10日.md "wikilink")

:\*3 a.m. [UTC](../Page/UTC.md "wikilink") -
**熱帶風暴16W**在[東京東南方形成](../Page/東京.md "wikilink")。

:\*3 p.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將熱帶風暴](../Page/聯合颱風警報中心.md "wikilink")16W降格至**熱帶低氣壓16W**。

:\*9 p.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將熱帶低氣壓](../Page/聯合颱風警報中心.md "wikilink")16W升格至**熱帶風暴16W**。

  - [9月11日](../Page/9月11日.md "wikilink")

:\*9 a.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將熱帶風暴](../Page/聯合颱風警報中心.md "wikilink")16W降格至**熱帶低氣壓16W**。

:\*9 p.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心為熱帶低氣壓](../Page/聯合颱風警報中心.md "wikilink")16W發出最後報告。

  - [9月13日](../Page/9月13日.md "wikilink")

:\*c. 6 p.m. [UTC](../Page/UTC.md "wikilink") -
**颱風森垃克在[宜蘭縣登陸](../Page/宜蘭縣.md "wikilink")。**

  - [9月14日](../Page/9月14日.md "wikilink")

:\*3 a.m. [UTC](../Page/UTC.md "wikilink") -
[東京東方發現一股熱帶低氣壓](../Page/東京.md "wikilink")。

:\*3 a.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心為熱帶低氣壓](../Page/聯合颱風警報中心.md "wikilink")17W發出最後報告。

:\*3 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將颱風森垃克降格為](../Page/日本氣象廳.md "wikilink")**強烈熱帶風暴森垃克**。

  - [9月15日](../Page/9月15日.md "wikilink")

:\*9 p.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將颱風](../Page/聯合颱風警報中心.md "wikilink")15W降格至**熱帶風暴15W**。

  - [9月16日](../Page/9月16日.md "wikilink")

:\*12 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將強烈熱帶風暴森垃克降格為](../Page/日本氣象廳.md "wikilink")**熱帶風暴森垃克**。

  - [9月17日](../Page/9月17日.md "wikilink")

:\*12 p.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將熱帶風暴森垃克升格為](../Page/日本氣象廳.md "wikilink")**強烈熱帶風暴森垃克**。

  - [9月18日](../Page/9月18日.md "wikilink")

:\*3 a.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將熱帶風暴](../Page/聯合颱風警報中心.md "wikilink")15W升格至**颱風15W**。

:\*c. 6 a.m. [UTC](../Page/UTC.md "wikilink") -
**強烈熱帶風暴森垃克在[屋久島登陸](../Page/屋久島.md "wikilink")。**

:\*9 a.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將颱風](../Page/聯合颱風警報中心.md "wikilink")15W降格至**熱帶風暴15W**。

:\*c. 9 a.m. [UTC](../Page/UTC.md "wikilink") -
**強烈熱帶風暴森垃克在[種子島登陸](../Page/種子島.md "wikilink")。**

:\*9 p.m. [UTC](../Page/UTC.md "wikilink") -
[關島西北方發現一股熱帶低氣壓](../Page/關島.md "wikilink")。

  - [9月19日](../Page/9月19日.md "wikilink")

:\*3 a.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將熱帶風暴](../Page/聯合颱風警報中心.md "wikilink")15W升格至**颱風15W**。

:\*3 a.m. [UTC](../Page/UTC.md "wikilink") -
菲律賓大氣地理天文部門將熱帶低氣壓18W命名為**熱帶低氣壓Nina**。

:\*9 a.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將熱帶低氣壓](../Page/聯合颱風警報中心.md "wikilink")18W升格至**熱帶風暴18W**。

:\*12 p.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將熱帶風暴](../Page/日本氣象廳.md "wikilink")18W確認為熱帶風暴，並將它命名為黑格比。

:\*3 p.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將颱風](../Page/聯合颱風警報中心.md "wikilink")15W降格至**熱帶風暴15W**。

:\*3 p.m. [UTC](../Page/UTC.md "wikilink") -
菲律賓大氣地理天文部門將熱帶低氣壓Nina升格至**熱帶風暴Nina**。

  - [9月20日](../Page/9月20日.md "wikilink")

:\*12 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將熱帶風暴黑格比升格為](../Page/日本氣象廳.md "wikilink")**強烈熱帶風暴黑格比**。

:\*3 a.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心為熱帶風暴](../Page/聯合颱風警報中心.md "wikilink")15W發出最後報告。

:\*12 p.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將強烈熱帶風暴黑格比升格為](../Page/日本氣象廳.md "wikilink")**颱風黑格比**。

:\*3 p.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將熱帶風暴](../Page/聯合颱風警報中心.md "wikilink")18W升格至**颱風18W**。

:\*6 p.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將強烈熱帶風暴森垃克降格為](../Page/日本氣象廳.md "wikilink")**熱帶風暴森垃克**。

:\*9 p.m. [UTC](../Page/UTC.md "wikilink") -
菲律賓大氣地理天文部門將熱帶風暴Nina升格至**颱風Nina**。

  - [9月21日](../Page/9月21日.md "wikilink")

:\*12 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳將熱帶風暴森垃克降格為](../Page/日本氣象廳.md "wikilink")**溫帶氣旋森垃克**。

:\*12 a.m. [UTC](../Page/UTC.md "wikilink") -
[日本氣象廳為溫帶氣旋森垃克發出最後報告](../Page/日本氣象廳.md "wikilink")。

  - [9月23日](../Page/9月23日.md "wikilink")

:\*10:15 p.m. [UTC](../Page/UTC.md "wikilink") -
[中國氣象局宣布](../Page/中國氣象局.md "wikilink")**颱風黑格比在[廣東省](../Page/廣東省.md "wikilink")[電白縣](../Page/電白縣.md "wikilink")[陳村鎮沿海登陸](../Page/陳村鎮.md "wikilink")**，登陸時中心最大風力有15級（48米／秒）。（未獲[日本氣象廳及](../Page/日本氣象廳.md "wikilink")[聯合颱風警報中心承認](../Page/聯合颱風警報中心.md "wikilink")）

  - [9月28日](../Page/9月28日.md "wikilink")

:\*6 a.m. [UTC](../Page/UTC.md "wikilink") -
[海南島西南方發現一股熱帶低氣壓](../Page/海南島.md "wikilink")。

:\*7 p.m. [UTC](../Page/UTC.md "wikilink") -
[聯合颱風警報中心將Tropical](../Page/聯合颱風警報中心.md "wikilink")
Cyclone Formation Alert WTPN21定為**熱帶風暴**。

  - [9月29日](../Page/9月29日.md "wikilink")
    \*8 a.m. [UTC](../Page/UTC.md "wikilink") -
    [日本氣象廳將熱帶風暴](../Page/日本氣象廳.md "wikilink")20W改名為**米克拉**。

<!-- end list -->

  - [9月30日](../Page/9月30日.md "wikilink")
    \*8 a.m. [UTC](../Page/UTC.md "wikilink") -
    [日本氣象廳將熱帶低氣壓](../Page/日本氣象廳.md "wikilink")21W改名為**海高斯**。
    \*3 p.m. [UTC](../Page/UTC.md "wikilink") -
    [日本氣象廳將](../Page/日本氣象廳.md "wikilink")**熱帶低氣壓海高斯**
    ***升格為*** **熱帶風暴海高斯**。

## 參考資料

[Category:2008年太平洋颱風季](../Category/2008年太平洋颱風季.md "wikilink")
[Category:太平洋颱風季時間軸](../Category/太平洋颱風季時間軸.md "wikilink")
[Category:使用簡易時間線語法的頁面](../Category/使用簡易時間線語法的頁面.md "wikilink")

1.
2.
3.
4.
5.
6.