[加勒比海及其附近区域](../Page/加勒比海.md "wikilink")。

## 定義

若依照[聯合國地理分區裡的地理亞區來判定](../Page/聯合國.md "wikilink")，加勒比地區的範圍為加勒比海上的諸島—[西印度群島](../Page/西印度群島.md "wikilink")，國家與地區包含[安圭拉](../Page/安圭拉.md "wikilink")、[安地卡及巴布達](../Page/安地卡及巴布達.md "wikilink")、[阿魯巴](../Page/阿魯巴.md "wikilink")、[巴哈馬](../Page/巴哈馬.md "wikilink")、[巴貝多](../Page/巴貝多.md "wikilink")、[博奈爾](../Page/博奈爾.md "wikilink")、[聖佑達修斯和](../Page/聖佑達修斯.md "wikilink")[荷屬沙巴](../Page/荷屬沙巴.md "wikilink")、[英屬維京群島](../Page/英屬維京群島.md "wikilink")、[開曼群島](../Page/開曼群島.md "wikilink")、[古巴](../Page/古巴.md "wikilink")、[古拉索](../Page/古拉索.md "wikilink")、[多米尼克](../Page/多米尼克.md "wikilink")、[多明尼加](../Page/多明尼加.md "wikilink")、[格瑞那達](../Page/格瑞那達.md "wikilink")、[瓜德羅普](../Page/瓜德羅普.md "wikilink")、[海地](../Page/海地.md "wikilink")、[牙買加](../Page/牙買加.md "wikilink")、[馬提尼克](../Page/馬提尼克.md "wikilink")、[蒙哲臘](../Page/蒙哲臘.md "wikilink")、[波多黎各](../Page/波多黎各.md "wikilink")、[聖巴泰勒米](../Page/聖巴泰勒米.md "wikilink")、[聖克里斯多福及尼維斯](../Page/聖克里斯多福及尼維斯.md "wikilink")、[聖露西亞](../Page/聖露西亞.md "wikilink")、[法屬聖馬丁](../Page/法屬聖馬丁.md "wikilink")、[聖文森及格瑞那丁](../Page/聖文森及格瑞那丁.md "wikilink")、[荷屬聖馬丁](../Page/荷屬聖馬丁.md "wikilink")、[千里達及托巴哥](../Page/千里達及托巴哥.md "wikilink")、[土克凱可群島](../Page/土克凱可群島.md "wikilink")、[美屬維京群島](../Page/美屬維京群島.md "wikilink")。

## 加勒比海国家協會成員列表

根据2002年[加勒比海国家协会](../Page/加勒比海国家协会.md "wikilink")（ACS）的成员国列出所有**加勒比海国家**：

### 國家

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
### 地区

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
## 参考文献

## 参见

  - [加勒比海](../Page/加勒比海.md "wikilink")
  - [加勒比岛屿列表](../Page/加勒比岛屿列表.md "wikilink")
  - [北美洲](../Page/北美洲.md "wikilink")、[中美洲](../Page/中美洲.md "wikilink")、[拉丁美洲](../Page/拉丁美洲.md "wikilink")

{{-}}

[加勒比地区](../Category/加勒比地区.md "wikilink")
[Category:拉丁美洲](../Category/拉丁美洲.md "wikilink")