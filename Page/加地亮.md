**加地亮**，[日本足球运动员](../Page/日本.md "wikilink")，司职右后卫，目前效力於[J2联赛](../Page/J2联赛.md "wikilink")[岡山綠雉](../Page/岡山綠雉.md "wikilink")。

先后就读于西淡町立御原中学校、滝川第二高等学校。在学校时就足球水平出众，1998年在[大阪樱花踢球](../Page/大阪樱花.md "wikilink")。1999年入选日本U-20足球队。

[Category:日本足球運動員](../Category/日本足球運動員.md "wikilink")
[Category:足球後衛](../Category/足球後衛.md "wikilink")
[Category:日職球員](../Category/日職球員.md "wikilink")
[Category:大阪櫻花球員](../Category/大阪櫻花球員.md "wikilink")
[Category:大分三神球員](../Category/大分三神球員.md "wikilink")
[Category:FC東京球員](../Category/FC東京球員.md "wikilink")
[Category:大阪飛腳球員](../Category/大阪飛腳球員.md "wikilink")
[Category:兵庫縣出身人物](../Category/兵庫縣出身人物.md "wikilink")
[Category:日本國家足球隊成員](../Category/日本國家足球隊成員.md "wikilink")
[Category:2004年亞洲盃足球賽球員](../Category/2004年亞洲盃足球賽球員.md "wikilink")
[Category:2005年洲際國家盃球員](../Category/2005年洲際國家盃球員.md "wikilink")
[Category:2006年世界盃足球賽球員](../Category/2006年世界盃足球賽球員.md "wikilink")
[Category:2007年亞洲盃足球賽球員](../Category/2007年亞洲盃足球賽球員.md "wikilink")
[Category:亞洲盃足球賽冠軍隊球員](../Category/亞洲盃足球賽冠軍隊球員.md "wikilink")
[Category:日本旅外足球運動員](../Category/日本旅外足球運動員.md "wikilink")