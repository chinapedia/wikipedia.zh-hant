**[尼加拉瓜共和國](../Page/尼加拉瓜共和國.md "wikilink")**是一個[單一制國家](../Page/單一制.md "wikilink")，下分15省
、2自治區 。之下再分為市：

[NicaraguaDepartmentsNumbered.png](https://zh.wikipedia.org/wiki/File:NicaraguaDepartmentsNumbered.png "fig:NicaraguaDepartmentsNumbered.png")

|    | 省／區                                           | 首府                                          | 2005年人口   | 面積（平方公里） | 人口密度 |
| -- | --------------------------------------------- | ------------------------------------------- | --------- | -------- | ---- |
| 1  | [博阿科省](../Page/博阿科省.md "wikilink")            | [博阿科](../Page/博阿科.md "wikilink")            | 150,636   | 4177     | 36   |
| 2  | [卡拉索省](../Page/卡拉索省.md "wikilink")            | [希諾特佩](../Page/希諾特佩.md "wikilink")          | 166,073   | 1081     | 154  |
| 3  | [奇南德加省](../Page/奇南德加省.md "wikilink")          | [奇南德加](../Page/奇南德加.md "wikilink")          | 378,970   | 4822     | 79   |
| 4  | [瓊塔萊斯省](../Page/瓊塔萊斯省.md "wikilink")          | [惠加爾帕](../Page/惠加爾帕.md "wikilink")          | 153,932   | 6481     | 24   |
| 5  | [埃斯特利省](../Page/埃斯特利省.md "wikilink")          | [埃斯特利](../Page/埃斯特利.md "wikilink")          | 201,548   | 2230     | 90   |
| 6  | [格拉納達省](../Page/格拉納達省_\(尼加拉瓜\).md "wikilink") | [格拉納達](../Page/格拉納達_\(尼加拉瓜\).md "wikilink") | 168,186   | 1040     | 162  |
| 7  | [希諾特加省](../Page/希諾特加省.md "wikilink")          | [希諾特加](../Page/希諾特加.md "wikilink")          | 331,335   | 9222     | 36   |
| 8  | [萊昂省](../Page/萊昂省_\(尼加拉瓜\).md "wikilink")     | [萊昂](../Page/萊昂_\(尼加拉瓜\).md "wikilink")     | 355,779   | 5138     | 69   |
| 9  | [馬德里斯省](../Page/馬德里斯省.md "wikilink")          | [索莫托](../Page/索莫托.md "wikilink")            | 132,459   | 1708     | 78   |
| 10 | [馬那瓜省](../Page/馬那瓜省.md "wikilink")            | [馬那瓜](../Page/馬那瓜.md "wikilink")            | 1,262,978 | 3465     | 365  |
| 11 | [馬薩亞省](../Page/馬薩亞省.md "wikilink")            | [馬薩亞](../Page/馬薩亞.md "wikilink")            | 289,988   | 611      | 475  |
| 12 | [馬塔加爾帕省](../Page/馬塔加爾帕省.md "wikilink")        | [馬塔加爾帕](../Page/馬塔加爾帕.md "wikilink")        | 469,172   | 6804     | 69   |
| 13 | [新塞哥維亞省](../Page/新塞哥維亞省.md "wikilink")        | [奧科塔爾](../Page/奧科塔爾.md "wikilink")          | 208,523   | 3491     | 60   |
| 14 | [里瓦斯省](../Page/里瓦斯省.md "wikilink")            | [里瓦斯](../Page/里瓦斯.md "wikilink")            | 156,283   | 2162     | 72   |
| 15 | [聖胡安河省](../Page/聖胡安河省.md "wikilink")          | [聖卡洛斯](../Page/聖卡洛斯_\(尼加拉瓜\).md "wikilink") | 95,596    | 7541     | 13   |
| 16 | [北加勒比海岸自治區](../Page/北加勒比海岸自治區.md "wikilink")  | [卡貝薩斯港](../Page/卡貝薩斯港.md "wikilink")        | 314,130   | 33,106   | 10   |
| 17 | [南加勒比海岸自治區](../Page/南加勒比海岸自治區.md "wikilink")  | [布盧菲爾茲](../Page/布盧菲爾茲.md "wikilink")        | 306,510   | 27,260   | 11   |

<div style="clear:both;">

</div>

## 塞拉亚省

曾經是尼加拉瓜共和國的一級行政區。1986年，原[塞拉亚省按](../Page/塞拉亚省.md "wikilink")「加勒比地區自治法」分成兩個自治區：[北大西洋自治区和](../Page/北加勒比海岸自治區.md "wikilink")[南大西洋自治区](../Page/南加勒比海岸自治區.md "wikilink")。

[\*](../Category/尼加拉瓜行政區劃.md "wikilink")