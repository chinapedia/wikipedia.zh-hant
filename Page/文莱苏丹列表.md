**文莱苏丹**是[文莱的国家元首兼](../Page/文莱.md "wikilink")[政府首腦](../Page/政府首腦.md "wikilink")。现任文莱苏丹是[哈吉·哈桑纳尔·博尔基亚](../Page/哈吉·哈桑纳尔·博尔基亚.md "wikilink")。[苏丹是](../Page/苏丹_\(称谓\).md "wikilink")[伊斯蘭國家依照](../Page/伊斯蘭國家.md "wikilink")[沙里亞法規設立的](../Page/沙里亞法規.md "wikilink")[政教合一統治者和](../Page/政教合一.md "wikilink")[最高領導人](../Page/最高領導人.md "wikilink")。

## 歷任蘇丹列表

1.  蘇丹[马合谟沙](../Page/马合谟沙.md "wikilink")（1363年 - 1402年） سلطان محمد
    شاه改奉[伊斯蘭教後第](../Page/伊斯蘭教.md "wikilink")1位君主
2.  蘇丹[麻那惹加那](../Page/麻那惹加那.md "wikilink")（1402年 - 1408年）سلطان عبد
    الحميد
    اواڠ不见于当地史籍，但在[中国史籍中有记载](../Page/中国.md "wikilink")。今天[南京有](../Page/南京.md "wikilink")[渤泥國王墓](../Page/渤泥國王墓.md "wikilink")，正是當年蘇丹病逝於[南京](../Page/南京.md "wikilink")，臨死前要求[永樂帝准許在南京下葬](../Page/永樂.md "wikilink")。唯可能因後來明清行海禁，加上十八世紀後文中雙方不約而同開始受[歐洲列強殖民所困擾](../Page/歐洲.md "wikilink")，漸漸被遺忘數百年，直至1959年才被重新發現。[麻那惹加那有一儿子](../Page/麻那惹加那.md "wikilink")[遐旺](../Page/遐旺.md "wikilink")。
3.  蘇丹[阿哈默德](../Page/阿哈默德.md "wikilink")（1408年 - 1425年）سلطان احمد
4.  蘇丹[沙里夫·阿里](../Page/沙里夫·阿里.md "wikilink")（1425年 - 1432年）سلطان شريف
    علي 汶萊全盛時期，今日汶萊國旗標誌，起源於此君。
5.  蘇丹（1432 - 1485）سلطان سليمان
6.  蘇丹[博爾基亞](../Page/博爾基亞.md "wikilink")（1485 - 1524）سلطان بلقية
7.  蘇丹[阿布加哈](../Page/阿布加哈.md "wikilink")（1524 - 1530）
8.  蘇丹[賽爾夫里賈](../Page/賽爾夫里賈.md "wikilink")（1533 - 1581）
9.  蘇丹[沙汶萊](../Page/沙汶萊.md "wikilink")（1581 - 1582）شاه بروني
10. 蘇丹[穆罕默德哈山](../Page/穆罕默德哈山.md "wikilink")（1582 - 1598）محمد حسن
11. 蘇丹[阿都賈里魯阿巴](../Page/阿都賈里魯阿巴.md "wikilink")（1598 - 1659）سلطان جليل
12. 蘇丹[阿都賈里魯賈巴](../Page/阿都賈里魯賈巴.md "wikilink")（1659 - 1660）
13. 蘇丹[哈只穆罕默德阿里](../Page/哈只穆罕默德阿里.md "wikilink")（1660 - 1661）سلطان حاج
    محمد علي 曾往[麥加朝聖](../Page/麥加.md "wikilink")，代子而死，受後人追頌。
14. 蘇丹[阿都赫古爾穆賓](../Page/阿都赫古爾穆賓.md "wikilink")（1661 -
    1673）在位期間發生內戰(Perang Saudara)Sultan Haji Muhammad
    Ali سلطان حاج محمد علي 的女婿Pengiran Muda Bungsu 殺了Bendahara Pengiran
    Abdul Mubin的兒子。Bendahara Pengiran Abdul
    Mubin為了復仇，就想在皇宮內製造政變，最後Sultan
    Haji Muhammad Ali被Bendahara Pengiran Abdul Mubin殺死。Bendahara
    Pengiran Abdul Mubin隨即自立為帝，史稱Sultan Abdul Hakkul Mubin。他任命Pengiran
    Muda Bungsu為新的Bendahara，此舉是為了避免觸怒Sultan Haji Muhammad
    Ali家族，但最終無助解決事件。新任Bendahara計劃為Sultan Haji
    Muhammad Ali復修，於是在皇宮內製造混亂。Bendahara Pengiran Abdul
    Mubin害怕，於是倉皇逃到鏡子島Pulau Chermin。不久Pengiran
    Muda Bungsu自封為皇，史稱Sultan Muhyiddin。內戰一觸即發，因為此時汶萊出現兩個蘇丹。Sultan
    Muhyiddin控制了Kota Batu کوتا باتو，蘇祿蘇丹更派兵援助Sultan Muhyiddin。後來Sultan
    Abdul Hakkul Mubin在鏡子島Pulau Chermin ڤولاو چرمين 被殺，事件擴大至沙巴 سابه
    和汶萊之間的衝突。蘇祿蘇丹提出若Sultan Muhyiddin割讓沙巴給蘇祿，他則可援助汶萊，但汶萊拒絕。
15. 蘇丹[姆爾汀](../Page/姆爾汀.md "wikilink")（1673 - 1690）سلطان مهي الدين
16. 蘇丹[那素魯汀](../Page/那素魯汀.md "wikilink")（1690 - 1710）سلطان نصر الدين
17. 蘇丹[胡先卡瑪魯汀](../Page/胡先卡瑪魯汀.md "wikilink")（1710 - 1730) (1737 -
    1740）سلطان حسين قمر الدين
18. 蘇丹[穆罕默德阿留汀](../Page/穆罕默德阿留汀.md "wikilink")（1730 - 1737）سلطان محمد
    علاءالدين
19. 蘇丹[奧瑪阿里賽義夫汀一世](../Page/奧瑪阿里賽義夫汀一世.md "wikilink")（1740-1795）سلطان عمر
    علي سيف الدين
20. 蘇丹[穆罕默德丹祖汀](../Page/穆罕默德丹祖汀.md "wikilink")（1795-1804)
    (1804-1807）محمد تحا الدين
21. 蘇丹[穆罕默德賈里魯阿蘭一世](../Page/穆罕默德賈里魯阿蘭一世.md "wikilink")（1804）سلطان محمد
    جمل العالم
22. 蘇丹[穆罕默德根祖阿蘭](../Page/穆罕默德根祖阿蘭.md "wikilink")（1807-1826）
23. 蘇丹[穆罕默德阿蘭](../Page/穆罕默德阿蘭.md "wikilink")（1826-1828）سلطان محمد عالم
24. 蘇丹[奧瑪阿里賽義夫汀二世](../Page/奧瑪阿里賽義夫汀二世.md "wikilink")（1828-1852）سلطان عمر
    علي سيف الدين 發生[汶萊之變](../Page/汶萊之變.md "wikilink")
25. 蘇丹[阿都姆汶](../Page/阿都姆汶.md "wikilink")（1852-1885）سلطان عبد المومن
26. Sultan Hashim Jalilul Alam Aqamaddin (1885-1906)سلطان هاشم جليل
    العالم 汶萊被列入為英國保護國（Zaman Sistem Residen زمان سيستيم ريسيدين
    ），其實變相是英國殖民地，只是形態上不同於一般殖民地而已。
27. 蘇丹[穆罕默德賈里魯阿蘭二世](../Page/穆罕默德賈里魯阿蘭二世.md "wikilink")（1906年-1924年）سلطان
    محمد جمل العالم
28. 蘇丹[阿哈默德丹祖汀](../Page/阿哈默德丹祖汀.md "wikilink")（1924年-1950年）سلطان احمد
    تجا الدين
29. 蘇丹[奧瑪阿里賽義夫汀三世](../Page/奧瑪阿里賽義夫汀三世.md "wikilink")（1950年-1967年）سلطان
    حاج عمر علي سيف الدين سعد الخير و الدين ثلاث
    為人和藹可親，尤以喜愛文學著名。1959年傳出汶萊將獲獨立，奧瑪阿里賽義夫汀三世與英國簽訂了汶萊政途去向的協議。在位期間又積極發展當地經濟，今日汶萊之所以有此成就，得益於他，故他亦有汶萊現代化建築師之美譽（Arsitektur
    Moden Brunei）在位後期發生汶萊叛亂（Pemerontakan Raykat
    Brunei），首都[汶萊市](../Page/汶萊市.md "wikilink") (Bandar
    Bruneiبندر بروني，即今之Bandar Seri
    Begawan)為國家政途問題發生叛亂。不久平息，阿扎哈里流亡。1967年他退位讓予其長子--哈山納博爾基亞。1986年駕崩。
30. 蘇丹[哈吉·哈桑纳尔·博尔基亚](../Page/哈吉·哈桑纳尔·博尔基亚.md "wikilink")（1967年-至今）سلطان
    حاج حسن البلقية معزالدين والدولة ابن المرحوم سلطان حاج عمر على سيف
    الدين سعد الخير والدين

دتوليس اوليه جاكي لي درڤد هوڠ کوڠ

[文莱苏丹](../Category/文莱苏丹.md "wikilink")