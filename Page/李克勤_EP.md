《**李克勤
EP**》是[李克勤在](../Page/李克勤.md "wikilink")1986年6月推出的第一張[EP唱片](../Page/EP.md "wikilink")，當中僅收錄四首歌，以黑膠唱片及卡式錄音帶推出市場。除第二首歌「理想的開始」外，其餘三首均重新收錄在1987年推出的個人大碟[命運符號中](../Page/命運符號.md "wikilink")（「雨景」則只收錄在CD版第13首）。

## 曲目

[Category:1986年音樂專輯](../Category/1986年音樂專輯.md "wikilink")
[Category:香港音樂專輯](../Category/香港音樂專輯.md "wikilink")
[Category:流行音樂專輯](../Category/流行音樂專輯.md "wikilink")
[Category:李克勤音樂專輯](../Category/李克勤音樂專輯.md "wikilink")