**愛可信公司**（ACCESS Co.,
Ltd.、株式会社ACCESS）於1984年在[日本](../Page/日本.md "wikilink")[東京創立](../Page/東京.md "wikilink")，是一家為如[手提電話](../Page/手提電話.md "wikilink")、[個人數碼助理](../Page/個人數碼助理.md "wikilink")、[遊戲機與](../Page/遊戲機.md "wikilink")[機頂盒等具連線能力的裝置提供嵌入式軟體的公司](../Page/機頂盒.md "wikilink")。

愛可信因其[瀏覽器產品](../Page/瀏覽器.md "wikilink")[NetFront而獲得廣泛認可](../Page/NetFront.md "wikilink")，到2005年11月為止已配置到20000萬個裝置上[1](https://web.archive.org/web/20060614225352/http://access-us-inc.com/news_releases/20051122a.html)
。NetFront是日本[NTT
DoCoMo數據服務](../Page/NTT_DoCoMo.md "wikilink")[i-mode的主要構成部分](../Page/i-mode.md "wikilink")，亦是[索尼](../Page/索尼.md "wikilink")[PSP與](../Page/PlayStation_Portable.md "wikilink")[PlayStation
3所採用的瀏覽器](../Page/PlayStation_3.md "wikilink")。

2005年9月愛可信收購[Palm
OS與](../Page/Palm_OS.md "wikilink")[BeOS的所有者](../Page/BeOS.md "wikilink")[PalmSource公司](../Page/PalmSource.md "wikilink")。

## 請參閱

  - [Qtopia](../Page/Qtopia.md "wikilink")
  - [Windows Mobile](../Page/Windows_Mobile.md "wikilink")
  - [Symbian OS](../Page/Symbian_OS.md "wikilink")

## 外部連結

  - [官方網站](http://www.access-company.com)

[Category:軟體公司](../Category/軟體公司.md "wikilink")
[Category:信息技术公司](../Category/信息技术公司.md "wikilink")
[Category:千代田區公司](../Category/千代田區公司.md "wikilink") [Category:Palm
OS](../Category/Palm_OS.md "wikilink")
[Category:日本品牌](../Category/日本品牌.md "wikilink")