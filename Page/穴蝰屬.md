**穴蝰屬**（[學名](../Page/學名.md "wikilink")：*Atractaspis*）是[蛇亞目](../Page/蛇亞目.md "wikilink")[穴蝰科下的一個](../Page/穴蝰科.md "wikilink")[有毒蛇](../Page/毒蛇.md "wikilink")[屬](../Page/屬.md "wikilink")，主要分布於[非洲](../Page/非洲.md "wikilink")，目前共有15個品種已被確認。\[1\]

## 地理分布

穴蝰主要分布於[非洲的亞](../Page/非洲.md "wikilink")[撒哈拉地區](../Page/撒哈拉沙漠.md "wikilink")，與及[以色列和](../Page/以色列.md "wikilink")[阿拉伯半島](../Page/阿拉伯半島.md "wikilink")。\[2\]

## 品種

| 品種\[3\]                                      | 學名及命名者\[4\]                                                   | 亞種數\[5\] | 異稱\[6\]    | 地理分布\[7\]                                                                                                                                                                   |
| -------------------------------------------- | ------------------------------------------------------------- | -------- | ---------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **[西非穴蝰](../Page/西非穴蝰.md "wikilink")**       | Atractaspis aterrima，<small>Günther，1863</small>              | 0        |            | [非洲](../Page/非洲.md "wikilink")：包括[塞內加爾](../Page/塞內加爾.md "wikilink")、[烏干達等](../Page/烏干達.md "wikilink")                                                                       |
| **[博穴蝰](../Page/博穴蝰.md "wikilink")**         | Atractaspis battersbyi，<small>Witte，1959</small>              | 0        |            | [非洲](../Page/非洲.md "wikilink")                                                                                                                                              |
| **[穴蝰](../Page/穴蝰.md "wikilink")**           | Atractaspis bibronii，<small>A. Smith，1849</small>             | 0        | 畢氏穴蝰       | 西非、[南非](../Page/南非.md "wikilink")：包括[納米比亞](../Page/納米比亞.md "wikilink")、[坦桑尼亞](../Page/坦桑尼亞.md "wikilink")、[肯雅海岸](../Page/肯雅.md "wikilink")、[索馬里](../Page/索馬里.md "wikilink") |
| **[中非穴蝰](../Page/中非穴蝰.md "wikilink")**       | Atractaspis boulengeri，<small>Mocquard，1897</small>           | 5        | 包氏穴蝰       | [非洲中部](../Page/非洲.md "wikilink")                                                                                                                                            |
| **[邦氏穴蝰](../Page/邦氏穴蝰.md "wikilink")**       | Atractaspis coalescens，<small>Perret，1960</small>             | 0        |            | [非洲](../Page/非洲.md "wikilink")：包括[喀麥隆](../Page/喀麥隆.md "wikilink")                                                                                                           |
| **[剛果穴蝰](../Page/剛果穴蝰.md "wikilink")**       | Atractaspis congica，<small>Peters，1877</small>                | 2        |            | 非洲                                                                                                                                                                          |
| **[胖穴蝰](../Page/胖穴蝰.md "wikilink")**         | Atractaspis corpulenta，<small>Hallowell，1954</small>          | 2        |            | 非洲                                                                                                                                                                          |
| **[達氏穴蝰](../Page/達氏穴蝰.md "wikilink")**       | Atractaspis dahomeyensis，<small>Bocage，1887</small>           | 0        |            | 非洲                                                                                                                                                                          |
| **A. duerdeni**                              | Atractaspis duerdeni，<small>Gough，1907</small>                | 0        |            | 非洲                                                                                                                                                                          |
| **[斯基馬尤穴蝰](../Page/斯基馬尤穴蝰.md "wikilink")**   | Atractaspis engdahli，<small>Lonnberg & Andersson，1913</small> | 0        |            | 非洲                                                                                                                                                                          |
| **[異穴蝰](../Page/異穴蝰.md "wikilink")**         | Atractaspis irregularis，<small>Reinhardt，1843</small>         | 4        | 不規則穴蝰、多變穴蝰 | 非洲                                                                                                                                                                          |
| **[埃塞俄比亞穴蝰](../Page/埃塞俄比亞穴蝰.md "wikilink")** | Atractaspis leucomelas，<small>Boulenger，1895</small>          | 0        |            | 非洲                                                                                                                                                                          |
| **[小鱗穴蝰](../Page/小鱗穴蝰.md "wikilink")**       | Atractaspis microlepidota，<small>Günther，1866</small>         | 1        |            | 非洲                                                                                                                                                                          |
| **[網紋穴蝰](../Page/網紋穴蝰.md "wikilink")**       | Atractaspis reticulata，<small>Sjöstedt，1896</small>           | 2        |            | 非洲中部                                                                                                                                                                        |
| **[索馬里穴蝰](../Page/索馬里穴蝰.md "wikilink")**     | Atractaspis scorteccii，<small>Parker，1949</small>             | 0        |            | 非洲                                                                                                                                                                          |
|                                              |                                                               |          |            |                                                                                                                                                                             |

## 備註

## 外部連結

  - [TIGR爬蟲類資料庫：穴蝰屬](http://reptile-database.reptarium.cz/search.php?&genus=Atractaspis&submit=Search)

[Category:蛇亞目](../Category/蛇亞目.md "wikilink")
[Category:穴蝰科](../Category/穴蝰科.md "wikilink")

1.

2.  Spawls S、Branch B：《The Dangerous Snakes of Africa》頁192，Ralph Curtis
    Books，[杜拜](../Page/杜拜.md "wikilink")：Oriental Press，1995年。ISBN
    0-88359-029-8

3.
4.
5.
6.
7.