**霞浦**（）位於[日本](../Page/日本.md "wikilink")[茨城縣東南部到](../Page/茨城縣.md "wikilink")[千葉縣東北部之間](../Page/千葉縣.md "wikilink")，是日本第二大的[淡水湖](../Page/淡水湖.md "wikilink")（第一大為[滋賀縣的](../Page/滋賀縣.md "wikilink")[琵琶湖](../Page/琵琶湖.md "wikilink")），[湖沼水質保全特別措置法指定湖泊](../Page/湖沼水質保全特別措置法.md "wikilink")，由數個湖所組成，總[面積達](../Page/面積.md "wikilink")220[平方公里](../Page/平方公里.md "wikilink")。

依[国土交通省的定義](../Page/国土交通省.md "wikilink")，霞浦是由西浦、北浦、外浪逆浦、北利根川、鰐川、常陸利根川等所構成的水域總稱，其中西浦是最大的湖泊，面積為172平方公里，所以有時候霞浦也專指西浦而言。

## 流域自治體

（**粗體字**為沿岸的自治體）

### [茨城縣](../Page/茨城縣.md "wikilink")

<table>
<tbody>
<tr class="odd">
<td><dl>
<dt>*<strong><a href="../Page/土浦市.md" title="wikilink">土浦市</a></strong><br />
*<strong><a href="../Page/潮来市.md" title="wikilink">潮来市</a></strong><br />
*<strong><a href="../Page/鹿嶋市.md" title="wikilink">鹿嶋市</a></strong><br />
*<strong><a href="../Page/神栖市.md" title="wikilink">神栖市</a></strong><br />
*<strong><a href="../Page/石岡市.md" title="wikilink">石岡市</a></strong><br />
*<strong><a href="../Page/小美玉市.md" title="wikilink">小美玉市</a></strong><br />
*<strong><a href="../Page/鉾田市.md" title="wikilink">鉾田市</a></strong><br />
*<strong><a href="../Page/稻敷市.md" title="wikilink">稻敷市</a></strong><br />
*<strong><a href="../Page/霞浦市.md" title="wikilink">霞浦市</a></strong><br />
*<strong><a href="../Page/行方市.md" title="wikilink">行方市</a></strong></dt>

</dl></td>
<td><dl>
<dt>*<a href="../Page/筑波市.md" title="wikilink">筑波市</a><br />
*<a href="../Page/筑西市.md" title="wikilink">筑西市</a><br />
*<a href="../Page/笠間市.md" title="wikilink">笠間市</a><br />
*<a href="../Page/龍崎市.md" title="wikilink">龍崎市</a><br />
*<a href="../Page/牛久市.md" title="wikilink">牛久市</a><br />
*<a href="../Page/櫻川市.md" title="wikilink">櫻川市</a><br />
*<a href="../Page/下妻市.md" title="wikilink">下妻市</a></dt>

</dl></td>
<td><dl>
<dt>*<strong><a href="../Page/阿見町.md" title="wikilink">阿見町</a></strong><br />
*<strong><a href="../Page/美浦村.md" title="wikilink">美浦村</a></strong><br />
*<a href="../Page/茨城町.md" title="wikilink">茨城町</a><br />
*<a href="../Page/利根町.md" title="wikilink">利根町</a><br />
*<a href="../Page/河内町.md" title="wikilink">河内町</a></dt>

</dl></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### [千葉縣](../Page/千葉縣.md "wikilink")

  - \***[香取市](../Page/香取市.md "wikilink")**

### [栃木縣](../Page/栃木縣.md "wikilink")

  - \*[益子町](../Page/益子町.md "wikilink")

## 關連項目

  - [湖泊列表](../Page/湖泊列表.md "wikilink")
  - [日本湖沼列表](../Page/日本湖沼列表.md "wikilink")
  - [新利根川](../Page/新利根川.md "wikilink")
  - [利根川河口堰](../Page/利根川河口堰.md "wikilink")
  - [利根川東遷事業](../Page/利根川東遷事業.md "wikilink")
  - [鹿島臨海工業地帯](../Page/鹿島臨海工業地帯.md "wikilink")
  - [霞浦開發事業](../Page/霞浦開發事業.md "wikilink")
  - [霞浦用水事業](../Page/霞浦用水事業.md "wikilink")
  - [霞浦導水事業](../Page/霞浦導水事業.md "wikilink")
  - [水鄉筑波國定公園](../Page/水鄉筑波國定公園.md "wikilink")
  - [霞浦駐屯地](../Page/霞浦駐屯地.md "wikilink")

## 外部連結

  - [かすみがうら\*ネット（市民による霞ヶ浦データベース）](http://www.kasumigaura.net/index.html)
  - [国土交通省関東地方整備局霞ヶ浦河川事務所](https://web.archive.org/web/20070928185700/http://www.kasumigaura.go.jp/index.asp)
  - [霞ヶ浦水質浄化プロジェクト（茨城県科学技術振興財団）](https://web.archive.org/web/20080520022159/http://www.i-step.org/kasumi/index.shtml)

[Category:日本湖泊](../Category/日本湖泊.md "wikilink")
[Kasu](../Category/日本百景.md "wikilink")
[Category:茨城縣地理](../Category/茨城縣地理.md "wikilink")
[Category:千葉縣地理](../Category/千葉縣地理.md "wikilink")
[Category:土浦市](../Category/土浦市.md "wikilink")
[Category:潮來市](../Category/潮來市.md "wikilink")
[Category:筑波市](../Category/筑波市.md "wikilink")
[Category:阿見町](../Category/阿見町.md "wikilink")
[Category:筑西市](../Category/筑西市.md "wikilink")
[Category:美浦村](../Category/美浦村.md "wikilink")
[Category:鹿嶋市](../Category/鹿嶋市.md "wikilink")
[Category:神栖市](../Category/神栖市.md "wikilink")
[Category:石岡市](../Category/石岡市.md "wikilink")
[Category:小美玉市](../Category/小美玉市.md "wikilink")
[Category:鉾田市](../Category/鉾田市.md "wikilink")
[Category:稻敷市](../Category/稻敷市.md "wikilink")
[Category:霞浦市](../Category/霞浦市.md "wikilink")
[Category:行方市](../Category/行方市.md "wikilink")
[Category:笠間市](../Category/笠間市.md "wikilink")
[Category:龍崎市](../Category/龍崎市.md "wikilink")
[Category:牛久市](../Category/牛久市.md "wikilink")
[Category:櫻川市](../Category/櫻川市.md "wikilink")
[Category:下妻市](../Category/下妻市.md "wikilink")
[Category:茨城町](../Category/茨城町.md "wikilink")
[Category:利根町](../Category/利根町.md "wikilink")
[Category:河內町](../Category/河內町.md "wikilink")
[Category:香取市](../Category/香取市.md "wikilink")
[Category:益子町](../Category/益子町.md "wikilink")
[Category:利根川水系](../Category/利根川水系.md "wikilink")