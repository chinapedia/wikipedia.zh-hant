**石毛佐和**（）是日本[女性](../Page/女性.md "wikilink")[聲優](../Page/聲優.md "wikilink")。於[千葉縣出身](../Page/千葉縣.md "wikilink")。前[劇團若草](../Page/劇團若草.md "wikilink")、[東京俳優生活協同組合及](../Page/東京俳優生活協同組合.md "wikilink")所属，现為自由身。別稱等。

## 人物

  - 第一人稱是「[おいら](../Page/僕少女.md "wikilink")」。
  - 喜歡料理，在自己的網站將她的煮食過程的片段上傳到自己的網站。
  - 飼養的猫名字是「Piko」和「Cream」。
  - 不擅於應付機械，對電腦苦戰。
  - 在出演[魔法老師期間](../Page/魔法老師.md "wikilink")，與[佐久間未帆](../Page/佐久間未帆.md "wikilink")、[皆川純子](../Page/皆川純子.md "wikilink")、[佐藤利奈](../Page/佐藤利奈.md "wikilink")、[Hazuki組成了](../Page/阿澄佳奈.md "wikilink")《魔法老师》俳协队。
  - 樣貌有點像[水谷優子也被說像](../Page/水谷優子.md "wikilink")[西村千波](../Page/西村千波.md "wikilink")。容貌被指為假的[矢口真里](../Page/矢口真里.md "wikilink")。
  - 喜歡的事物是粉紅色的和服、[ストレッチマン](../Page/ストレッチマン.md "wikilink")、くりいむしちゅーの[上田晉也](../Page/上田晉也.md "wikilink")、燒肉、赤の雑貨。
  - 討厭的事物包括大多數的青蛙、多數「圓」形物體、山葵、和がらし、雨、人混み、弱いものイジメする人。
  - 喜歡看搞笑節目，因此有參加搞笑節目。
  - 出演動畫作品『[ぱにぽにだっしゅ\!](../Page/ぱにぽに.md "wikilink")』で、由她製作[網頁的頁首に描かれているトレードマークが](../Page/網頁.md "wikilink")、[黑板の落書きとして描かれたことがある](../Page/黑板.md "wikilink")。落書きの隅には『佐和』と書かれていた。
  - 與[大泉洋髪型相似](../Page/大泉洋.md "wikilink")，有時被猜測為男性。

## 出演作品

### 電視動畫

**1999年**

  - [小魔女Doremi系列](../Page/小魔女Doremi.md "wikilink")（**春風泡泡**、花花）
  - [魔法使Tai\!](../Page/魔法使Tai!.md "wikilink")（高倉貴子）

**2000年**

  - [銀河冒險戰記系列](../Page/銀河冒險戰記.md "wikilink")（**パイウェイ・ウンダーベルク**）
  - [捍衛者](../Page/捍衛者.md "wikilink")（高梨聰子）

**2001年**

  - [小小雪精靈](../Page/小小雪精靈.md "wikilink")（諾瑪）

**2002年**

  - [犬夜叉](../Page/犬夜叉.md "wikilink")（千代）
  - [仰天人間バトシーラー](../Page/仰天人間バトシーラー.md "wikilink")（コッパラダンサーズ、ケナゲインコ）
  - [數碼寶貝最前線](../Page/數碼寶貝最前線.md "wikilink")（**織本泉**／風系鬥士仙女獸／風神獸）
  - [棋魂](../Page/棋魂.md "wikilink")（院生）

**2003年**

  - [星空防衛隊](../Page/星空防衛隊.md "wikilink")（岩崎ショーグ9ン）
  - [狼雨](../Page/狼雨.md "wikilink")（鍊金術師）
  - [魔法使的條件](../Page/魔法使的條件.md "wikilink")（森川瑠奈）
  - [急救超人兵團](../Page/急救超人兵團.md "wikilink")（**櫻咲小鈴**／タンポポ）
  - [驚爆危機？校園篇](../Page/驚爆危機.md "wikilink")（中學生A）

**2004年**

  - [SD GUNDAM
    FORCE](../Page/SD_GUNDAM_FORCE.md "wikilink")（エルメチュ、ポーンリーオー）
  - [向北](../Page/向北.md "wikilink")。～Diamond Dust Drops～（ミミ子）
  - [瑪莉亞的凝望](../Page/瑪莉亞的凝望.md "wikilink")（店員）

**2005年**

  - [韋駄天翔](../Page/韋駄天翔.md "wikilink")（**獅堂真琴**）
  - [極上生徒會](../Page/極上生徒會.md "wikilink")（江川智子）
  - [地獄少女](../Page/地獄少女.md "wikilink")（澁谷南）
  - [曙光少女](../Page/曙光少女.md "wikilink")（Meryl Tyler）
  - [月詠 -MOON PHASE-](../Page/月詠.md "wikilink")（アルト）
  - [哈姆太郎](../Page/哈姆太郎.md "wikilink")（ジェラードちゃん）
  - [嬉皮笑園](../Page/嬉皮笑園.md "wikilink")（柏木優麻、柏木優奈）
  - [光之美少女Max Heart](../Page/光之美少女.md "wikilink")（ハピネン）
  - [魔法少女迦南](../Page/魔法少女迦南.md "wikilink")（西京あゆみ）
  - [魔法老師](../Page/魔法老師_\(動畫\).md "wikilink")（[早乙女春奈](../Page/早乙女春奈.md "wikilink")）
  - [MÄR 魔法世界](../Page/MÄR_魔法世界.md "wikilink")（洛可、魅影的母親）

**2006年**

  - [機神咆吼Demonbane](../Page/機神咆吼Demonbane.md "wikilink")（アリスン）
  - [女子高生 GIRLS-HIGH](../Page/女子高生.md "wikilink")（**小川育惠**）
  - [新登場\!飛天小女警Z](../Page/新登場!飛天小女警Z.md "wikilink")（中目黑華代、藍寶石）
  - [魔法老師！？](../Page/魔法老師_\(動畫\).md "wikilink")（早乙女春奈） - 為第11話副題題字。
  - [血色花園](../Page/血色花園.md "wikilink")（露西）

**2007年**

  - [萌單](../Page/萌單.md "wikilink")（茵可媽媽）

**2008年**

  - [紅 kurenai](../Page/紅_\(小說\).md "wikilink")（**柔澤 紅香**）
  - [夏目友人帳](../Page/夏目友人帳.md "wikilink") （民子）

**2009年**

  - [獸之奏者](../Page/獸之奏者.md "wikilink")（サジュ、サジュの娘）

**2010年**

  - [哆啦A夢](../Page/哆啦A夢_\(電視動畫\).md "wikilink")（女孩子C）

**2013年**

  - [名偵探柯南](../Page/名偵探柯南.md "wikilink")（矢内みさき）
  - [八犬傳－東方八犬異聞－](../Page/八犬傳－東方八犬異聞－.md "wikilink")（小早川琉璃）

**2017年**

  - [悠久持有者：魔法老師！ 第2部](../Page/悠久持有者.md "wikilink")（早乙女春奈）

### OVA

  - [小魔女Doremiナ](../Page/小魔女Doremi.md "wikilink")・イ・ショ（春風寶寶）
  - [ヴァンドレッド](../Page/ヴァンドレッド.md "wikilink") 胎動篇（パイウェイ・ウンダーベルク）
  - ヴァンドレッド 激闘篇（パイウェイ・ウンダーベルク）
  - [トップをねらえ\!DVD](../Page/トップをねらえ!.md "wikilink")1巻・3巻新作映像（ゆみこ）
  - [魔法老師！？ 春](../Page/魔法老師_\(動畫\).md "wikilink")（早乙女春奈）
  - [魔法老師！？ 夏](../Page/魔法老師_\(動畫\).md "wikilink")（早乙女春奈）
  - [魔法老師 白之翼](../Page/魔法老師_\(動畫\).md "wikilink")（早乙女春奈）
  - [魔法老師 别一个世界](../Page/魔法老師_\(動畫\).md "wikilink")（早乙女春奈）

### 劇場版

  - 劇場版[小魔女Doremi各作品](../Page/小魔女Doremi.md "wikilink")（春風寶寶）
  - 劇場版[數碼寶貝最前線](../Page/數碼寶貝最前線.md "wikilink") 古代數碼寶貝オニスモン復活（織本泉）
  - [名侦探柯南 天空的遇难船](../Page/天空的遇难船.md "wikilink")（西谷霞）
  - [劇場版 魔法老師！
    ANIME_FINAL](../Page/劇場版_魔法老師！_ANIME_FINAL.md "wikilink")（早乙女春奈）

### 游戏

  - [拳皇EX](../Page/拳皇.md "wikilink")2 ～HOWLING BLOOD～（天羽忍）
  - [おジャ魔女あどべんちゃ～ ないしょのまほう](../Page/小魔女Doremi.md "wikilink")（春風寶寶）
  - [魔法老師系列](../Page/魔法老師.md "wikilink")（早乙女春奈）
      - 魔法先生ネギま\! 1時間目 お子ちゃま先生は魔法使い\!
      - 魔法先生ネギま\! 2時間目 戦う乙女たち\!麻帆良大運動会SP\!
  - スレッドカラーズ さよならの向こう側（当麻美桜）
      -
        [SIMPLE2000系列](../Page/SIMPLE系列.md "wikilink")『THE
        恋と涙と、追憶と…。』として再販
  - 彼女の伝説、僕の石版。～アミリオンの剣とともに～（ミリィ）
      -
        SIMPLE2000系列『THE ファンタジー恋愛アドベンチャー』として再販
  - [ホームメイド～終の館～](../Page/ホームメイド_-Homemaid-.md "wikilink")（久留間藤）
  - [F ～ファナティック～](../Page/F_～ファナティック～.md "wikilink")（アリシア・フィオリーゼ）
  - シークエンス・パラディウム（クリスティン＝ファークロス）
  - [片神名～喪われた因果律～](../Page/片神名～喪われた因果律～.md "wikilink")（ツクヨミ）
  - [幻想水滸傳V](../Page/幻想水滸傳V.md "wikilink")（）

### 実寫

  - [魔法老師](../Page/魔法老師.md "wikilink")：麻帆良学園中等部2-A
      -
        一学期特典DVD 麻帆良学園中等部2-A：入学式
        二学期特典DVD 麻帆良学園中等部2-A：一学期終業式
        三学期特典DVD 麻帆良学園中等部2-A：二学期終業式
        ホームルーム
  - ぐみさわパーティー！
  - マイクのある風景3

### 角色單曲

  - [Gatekeepers](../Page/Gatekeepers.md "wikilink")（政治思想研究会）
      - 『SPIRITS』
          - 漆黒の世界へ
  - [おジャ魔女どれみ♯](../Page/おジャ魔女どれみ.md "wikilink")（春風ぽっぷ）
      - 『そんぐ・ふぇすてぃばる♪』
          - ホップ・ステップ・ポップ
              -
                作詞：森林檎、作曲：貝田由里子、編曲：今泉洋
  - [ヴァンドレッド](../Page/ヴァンドレッド.md "wikilink")（パイウェイ・ウンダーベルクまたはメジェール・パイレーツ）
      - 『ヴォーカルコレクション』
          - 会ったとたんにひと目ぼれ
      - 『胎動編 マキシシングル JUSTICE』
          - SPACY SPICY LOVE
      - 『the second stage ボーカル&オリジナルサウンドトラック』
          - SPACY SPICY LOVE：セリフ入りバージョン
      - 『the second stage ヴォーカルコレクションアルバム』（DVD初回特典）
          - PROOF
  - [デジモンフロンティア](../Page/デジモンフロンティア.md "wikilink")（織本泉またはスピリットシンガーズとして）
      - 『キャラクターソング・コレクション サラマンダー』
          - 風のしずく～泉のテーマ～
      - 『クリスマススマイル』
          - More More Happy Christmas
          - With The Will
      - 『We Love DiGiMONMUSiC』（デジモンミュージック100タイトル記念作品 完全生産限定盤）
          - イノセント～無邪気なままで
  - [魔法老師](../Page/魔法老師.md "wikilink")（早乙女春奈またはまほらコーラス部として）
      - 『キャラクターマキシシングル 6月：図書館探検部』
          - Invitation～図書館へいらっしゃい～
              -
                2004年4月5日に[オリコンチャート史上初となる声優の歌う架空のキャラクター名義のイメージソングによるベスト](../Page/オリコン.md "wikilink")10入りを達成。
      - 『麻帆良学園中等部2-A：一学期」
          - 麻帆良学園校歌
      - 『出席番号のうた』（シングル）
          - 麻帆良学園中等部2-A・出席番号のうた
      - 『麻帆良学園中等部2-A：二学期」
          - 放課後ア☆ライブ
      - 『ハッピー☆マテリアル』（3月度OP）
          - ハッピー☆マテリアル（More Happy Instrumental Ver.）
  - [ぱにぽにだっしゅ\!](../Page/ぱにぽにだっしゅ!.md "wikilink")（柏木優奈&柏木優麻）
      - 『ガールッピ』（第1弾エンディングテーマ）
      - 『キャラクターボーカルアルバム 学園天国』
          - Link
  - 自主制作CD（入手先は外部リンクを参照）
      - オリジナルミニミニミニアルバム『オモチャ』
          -
            「ミニミニミニ」とは本人の表現。
      - オリジナルマキシシングル『誓い☆あのね』

### 廣播劇

  - [女子高生](../Page/女子高生.md "wikilink")（[Toraanara](../Page/Toraanara.md "wikilink")3500分交換品、非賣品）
  - タイガーサマー（[Toraanara専賣](../Page/Toraanara.md "wikilink")）
  - [ニニンがシノブ伝](../Page/ニニンがシノブ伝.md "wikilink") 其の一 忍と楓の☆ニニンがラジオ伝（エルエル）
  - [ぱにぽに](../Page/ぱにぽに.md "wikilink") セカンドシーズン Vol.1（犬神雅）
  - 魔法老師 劇場CD Vol.2（早乙女春奈）

### 廣告

  - ハローセット
    [デジモングッズプレゼント](../Page/數碼寶貝最前線.md "wikilink")（2002年[ロッテリアTVCM](../Page/ロッテリア.md "wikilink")）

### 其他

  - [タカラ](../Page/タカラ_\(玩具\).md "wikilink")「[リカちゃん人形](../Page/リカちゃん.md "wikilink")
    ラブリカメンバー」（ななみ）
  - [リカちゃん電話](../Page/リカちゃん.md "wikilink")
  - [BAIDAI](../Page/BAIDAI.md "wikilink") ロイヤルパトレーヌコール（春風ぽっぷ、ファファ）
      -
        [小魔女DoReMi♯の玩具](../Page/小魔女DoReMi.md "wikilink")
  - [東京電視台](../Page/東京電視台.md "wikilink")
    [熱鬥\!高爾夫向上委員會](../Page/熱鬥!高爾夫向上委員會.md "wikilink")（旁白）

## 外部連結

  - [石毛佐和官方網站](https://web.archive.org/web/20051026030811/http://ishigesawa.cool.ne.jp/)

[Category:日本女性配音員](../Category/日本女性配音員.md "wikilink")
[Category:千葉縣出身人物](../Category/千葉縣出身人物.md "wikilink")