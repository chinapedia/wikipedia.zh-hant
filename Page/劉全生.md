**劉全生**，[中華民國](../Page/中華民國.md "wikilink")[物理學家](../Page/物理學家.md "wikilink")，原籍[安徽省](../Page/安徽省.md "wikilink")，生於[廣西省](../Page/廣西省.md "wikilink")[全州縣](../Page/全州縣.md "wikilink")，父親[劉行之曾就讀](../Page/劉行之.md "wikilink")[國立東南大學](../Page/國立東南大學.md "wikilink")（[國立中央大學前身](../Page/國立中央大學.md "wikilink")）實驗中學（今[南師附中](../Page/南師附中.md "wikilink")），任[監察委員](../Page/監察委員.md "wikilink")，母親[王立文曾擔任](../Page/王立文.md "wikilink")[抗戰時期](../Page/抗戰.md "wikilink")[桂林難民救濟工作](../Page/桂林.md "wikilink")，後為[中華民國](../Page/中華民國.md "wikilink")[國民大會](../Page/中華民國國民大會.md "wikilink")[代表](../Page/國民大會代表.md "wikilink")。現任[澳門大學曹光彪書院創院院長](../Page/澳門大學.md "wikilink")。

## 簡歷

1960年畢業於[東海大學](../Page/東海大學_\(台灣\).md "wikilink")，1968年在[柏克萊加州大學取得物理博士學位](../Page/柏克萊加州大學.md "wikilink")，1971-1974年擔任[普林斯頓高等研究院長期研究員](../Page/普林斯頓高等研究院.md "wikilink")，以[電漿物理](../Page/電漿物理.md "wikilink")、[核融合理論做基本研究](../Page/核融合.md "wikilink")，在[雷射與](../Page/雷射.md "wikilink")[電漿相互作用之參數不穩定性及非線性理論方面有重要貢獻](../Page/電漿.md "wikilink")。

自1975年起，他任職[馬里蘭大學](../Page/馬里蘭大學.md "wikilink")27年。1975年，該校聘他為物理系正教授，領導電漿理論研究小組。1979年，他擔任該校剛成立的電漿暨核融合能實驗室的主任，領導電漿物理的研究。後任研究院院長及副校長，為該校首任華人副校長，並任全球華人事務研究所主任。1993年擔任美國物理學會電漿分會會長，1996-1999年擔任[美國科學院國家研究委員會](../Page/美國科學院國家研究委員會.md "wikilink")
(National Research Council, NRC)
電漿物理委員會主席，並於2000至2003年擔任美國物理天文科學政策及計劃制定的權威機構——NRC之物理及天文董事會董事。他曾獲頒馬里蘭大學傑出國際服務獎。他因電漿物理的成就，獲得瑞典[查爾摩斯工學院的榮譽博士](../Page/查爾摩斯工學院.md "wikilink")。2003年2月劉全生獲選為[國立中央大學校長](../Page/國立中央大學.md "wikilink")。回美國後，劉全生亦兼任了馬里蘭大學的孔子學院院長
(馬里蘭大學的孔子學院是全球第一所孔子學院)

對於大學教育，劉全生主張因材施教、發揮學生個性長處，以研究的精神教學，並且要善用[電腦及](../Page/電腦.md "wikilink")[網路等新的學習科技](../Page/網路.md "wikilink")。

## 外部連結

  - [熱愛東海，勤於治學－專訪物理系客座教授劉全生博士](https://web.archive.org/web/20100812233123/http://alumnus.thu.edu.tw/story/387/6247)

|- |colspan="3"
style="text-align:center;"|**[NCULogo.svg](https://zh.wikipedia.org/wiki/File:NCULogo.svg "fig:NCULogo.svg")[國立中央大學](../Page/國立中央大學.md "wikilink")**
|-    |- |colspan="3"
style="text-align:center;"|**[澳門大學](../Page/澳門大學.md "wikilink")**
|-

[Category:中華民國物理學會會士](../Category/中華民國物理學會會士.md "wikilink")
[Category:中華民國物理學家](../Category/中華民國物理學家.md "wikilink")
[Category:中華民國教育家](../Category/中華民國教育家.md "wikilink")
[Category:國立中央大學校長](../Category/國立中央大學校長.md "wikilink")
[Category:國立中央大學教授](../Category/國立中央大學教授.md "wikilink")
[Category:馬里蘭大學教師](../Category/馬里蘭大學教師.md "wikilink")
[Category:加州大學柏克萊分校校友](../Category/加州大學柏克萊分校校友.md "wikilink")
[Category:東海大學校友](../Category/東海大學校友.md "wikilink")
[Category:台灣戰後安徽移民](../Category/台灣戰後安徽移民.md "wikilink")
[Category:台灣戰後廣西移民](../Category/台灣戰後廣西移民.md "wikilink")
[Category:安徽人](../Category/安徽人.md "wikilink")
[Quan全](../Category/劉姓.md "wikilink")