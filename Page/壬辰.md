**壬辰**为[干支之一](../Page/干支.md "wikilink")，顺序为第29个。前一位是[辛卯](../Page/辛卯.md "wikilink")，后一位是[癸巳](../Page/癸巳.md "wikilink")。論[陰陽](../Page/陰陽.md "wikilink")[五行](../Page/五行.md "wikilink")，[天干之壬屬陽之水](../Page/天干.md "wikilink")，[地支之辰屬陽之土](../Page/地支.md "wikilink")，是土尅水相尅。

## 壬辰年

中国传统[纪年](../Page/纪年.md "wikilink")[农历的](../Page/农历.md "wikilink")[干支纪年中一个循环的第](../Page/干支纪年.md "wikilink")29年称“**壬辰年**”。以下各个[公元](../Page/公元.md "wikilink")[年份](../Page/年.md "wikilink")，年份數除以60餘32，或年份數減3，除以10的餘數是9，除以12的餘數是5，自當年[立春起至次年立春止的歲次内均为](../Page/立春.md "wikilink")“壬辰年”：

<table>
<caption><strong>壬辰年</strong></caption>
<thead>
<tr class="header">
<th><p><a href="../Page/第1千年.md" title="wikilink">第1千年</a></p></th>
<th><p><a href="../Page/第2千年.md" title="wikilink">第2千年</a></p></th>
<th><p><a href="../Page/第3千年.md" title="wikilink">第3千年</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/32年.md" title="wikilink">32年</a></li>
<li><a href="../Page/92年.md" title="wikilink">92年</a></li>
<li><a href="../Page/152年.md" title="wikilink">152年</a></li>
<li><a href="../Page/212年.md" title="wikilink">212年</a></li>
<li><a href="../Page/272年.md" title="wikilink">272年</a></li>
<li><a href="../Page/332年.md" title="wikilink">332年</a></li>
<li><a href="../Page/392年.md" title="wikilink">392年</a></li>
<li><a href="../Page/452年.md" title="wikilink">452年</a></li>
<li><a href="../Page/512年.md" title="wikilink">512年</a></li>
<li><a href="../Page/572年.md" title="wikilink">572年</a></li>
<li><a href="../Page/632年.md" title="wikilink">632年</a></li>
<li><a href="../Page/692年.md" title="wikilink">692年</a></li>
<li><a href="../Page/752年.md" title="wikilink">752年</a></li>
<li><a href="../Page/812年.md" title="wikilink">812年</a></li>
<li><a href="../Page/872年.md" title="wikilink">872年</a></li>
<li><a href="../Page/932年.md" title="wikilink">932年</a></li>
<li><a href="../Page/992年.md" title="wikilink">992年</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/1052年.md" title="wikilink">1052年</a></li>
<li><a href="../Page/1112年.md" title="wikilink">1112年</a></li>
<li><a href="../Page/1172年.md" title="wikilink">1172年</a></li>
<li><a href="../Page/1232年.md" title="wikilink">1232年</a></li>
<li><a href="../Page/1292年.md" title="wikilink">1292年</a></li>
<li><a href="../Page/1352年.md" title="wikilink">1352年</a></li>
<li><a href="../Page/1412年.md" title="wikilink">1412年</a></li>
<li><a href="../Page/1472年.md" title="wikilink">1472年</a></li>
<li><a href="../Page/1532年.md" title="wikilink">1532年</a></li>
<li><a href="../Page/1592年.md" title="wikilink">1592年</a></li>
<li><a href="../Page/1652年.md" title="wikilink">1652年</a></li>
<li><a href="../Page/1712年.md" title="wikilink">1712年</a></li>
<li><a href="../Page/1772年.md" title="wikilink">1772年</a></li>
<li><a href="../Page/1832年.md" title="wikilink">1832年</a></li>
<li><a href="../Page/1892年.md" title="wikilink">1892年</a></li>
<li><a href="../Page/1952年.md" title="wikilink">1952年</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/2012年.md" title="wikilink">2012年</a></li>
<li><a href="../Page/2072年.md" title="wikilink">2072年</a></li>
<li><a href="../Page/2132年.md" title="wikilink">2132年</a></li>
<li><a href="../Page/2192年.md" title="wikilink">2192年</a></li>
<li><a href="../Page/2252年.md" title="wikilink">2252年</a></li>
<li><a href="../Page/2312年.md" title="wikilink">2312年</a></li>
<li><a href="../Page/2372年.md" title="wikilink">2372年</a></li>
<li><a href="../Page/2432年.md" title="wikilink">2432年</a></li>
<li><a href="../Page/2492年.md" title="wikilink">2492年</a></li>
<li><a href="../Page/2552年.md" title="wikilink">2552年</a></li>
<li><a href="../Page/2612年.md" title="wikilink">2612年</a></li>
<li><a href="../Page/2672年.md" title="wikilink">2672年</a></li>
<li><a href="../Page/2732年.md" title="wikilink">2732年</a></li>
<li><a href="../Page/2792年.md" title="wikilink">2792年</a></li>
<li><a href="../Page/2852年.md" title="wikilink">2852年</a></li>
<li><a href="../Page/2912年.md" title="wikilink">2912年</a></li>
<li><a href="../Page/2972年.md" title="wikilink">2972年</a></li>
</ul></td>
</tr>
</tbody>
</table>

  - 1592年 - [壬辰衛國戰爭](../Page/壬辰衛國戰爭.md "wikilink")

## 壬辰月

天干丙年和辛年，[清明到](../Page/清明.md "wikilink")[立夏的時間段](../Page/立夏.md "wikilink")，就是**壬辰月**：

  - ……
  - [1976年](../Page/1976年.md "wikilink")4月清明到5月立夏
  - [1981年](../Page/1981年.md "wikilink")4月清明到5月立夏
  - [1986年](../Page/1986年.md "wikilink")4月清明到5月立夏
  - [1991年](../Page/1991年.md "wikilink")4月清明到5月立夏
  - [1996年](../Page/1996年.md "wikilink")4月清明到5月立夏
  - [2001年](../Page/2001年.md "wikilink")4月清明到5月立夏
  - [2006年](../Page/2006年.md "wikilink")4月清明到5月立夏
  - ……

## 壬辰日

## 壬辰時

天干丙日和辛日，[北京时间](../Page/北京时间.md "wikilink")（[UTC](../Page/协调世界时.md "wikilink")+8）7時到9時，就是**壬辰時**。

## 參考資料

1.
[Category:干支](../Category/干支.md "wikilink")
[\*](../Category/壬辰年.md "wikilink")