[Allene.png](https://zh.wikipedia.org/wiki/File:Allene.png "fig:Allene.png")，最简单的累积二烯烃。\]\]
**累积二烯烃**也称为“**聚集二烯烃**”或“**连烯烃**”是[分子中含有一对相邻碳碳双键](../Page/分子.md "wikilink")（即有一个[碳](../Page/碳.md "wikilink")[原子通过两个](../Page/原子.md "wikilink")[双键与相邻两个碳原子连接](../Page/双键.md "wikilink")）的一类[二烯烃](../Page/二烯烃.md "wikilink")。分子中具有超过一对相邻碳碳双键（[累积双键](../Page/累积双键.md "wikilink")）的[烯烃则称为](../Page/烯烃.md "wikilink")“[累积多烯烃](../Page/累积多烯烃.md "wikilink")”。

最简单的累积二烯烃是[丙二烯](../Page/丙二烯.md "wikilink")。虽然丙二烯等累积二烯烃是稳定的，但大多数的累积二烯烃皆很不稳定。由它们与[氯气等](../Page/氯气.md "wikilink")[化学物质反应可见](../Page/化学物质.md "wikilink")，累积二烯烃的活泼程度比一般的烯烃更强，与[炔烃相近](../Page/炔烃.md "wikilink")。因为累积二烯烃具有不稳定性，所以其存在与应用均不甚普遍，现主要用于[立体化学方面的研究](../Page/立体化学.md "wikilink")。

此类二烯烃中，C-C-C键角为180°，两个[π键和四个取代基在空间上处于正交](../Page/π键.md "wikilink")，中心碳原子为[sp杂化](../Page/sp杂化.md "wikilink")。类似的正交π键出现在[烯酮中](../Page/烯酮.md "wikilink")。

特定的催化剂（如[威尔金森催化剂](../Page/威尔金森催化剂.md "wikilink")）可使丙二烯的一个双键被还原，另一个不受影响。\[1\]

|                                                                                                          |                                                                                                             |
| -------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------- |
| [Allene_symmetry.png](https://zh.wikipedia.org/wiki/File:Allene_symmetry.png "fig:Allene_symmetry.png") | [Allene_chirality.png](https://zh.wikipedia.org/wiki/File:Allene_chirality.png "fig:Allene_chirality.png") |

累积二烯烃的合成方法有：

  - 由[偕二卤代](../Page/偕.md "wikilink")[环丙烷和](../Page/环丙烷.md "wikilink")[有机锂化合物发生](../Page/有机锂化合物.md "wikilink")[Skattebøl重排反应得到](../Page/Skattebøl重排反应.md "wikilink")。
  - 由末端[炔烃与](../Page/炔烃.md "wikilink")[甲醛](../Page/甲醛.md "wikilink")、[溴化铜和碱反应得到](../Page/溴化铜.md "wikilink")。\[2\]
  - 由某些二[卤化物的](../Page/卤化物.md "wikilink")[脱卤化氢反应得到](../Page/脱卤化氢反应.md "wikilink")。\[3\]

## 参见

  - [丙二烯](../Page/丙二烯.md "wikilink")，最简单的累积二烯烃

  - （cumulene）

  - [卡波姆](../Page/卡波姆.md "wikilink")

## 参考资料

## 外部链接

  - [IUPAC](../Page/IUPAC.md "wikilink")[金色书对](../Page/金色书.md "wikilink")“Allene”的定义：[](http://www.iupac.org/goldbook/A00238.pdf)
  - [立体化学](http://www.sparknotes.com/chemistry/organic3/stereochemistry/section2.rhtml)
  - [累积二烯烃的合成](https://www.organic-chemistry.org/synthesis/C1C/chains/allenes.shtm)

[Category:二烯烃](../Category/二烯烃.md "wikilink")
[L](../Category/官能团.md "wikilink")

1.  Bhagwat; Devaprabhakara, Tetrahedron Lett. p. 1391
    (1972)[Link](http://dx.doi.org/10.1016/S0040-4039\(01\)84636-0)
2.  [Organic Syntheses](../Page/Organic_Syntheses.md "wikilink"), Coll.
    Vol. 7, p.276 (1990); Vol. 63, p.203 (1985).
    [Link](http://www.orgsynth.org/orgsyn/pdfs/CV7P0276.pdf)
3.  [Organic Syntheses](../Page/Organic_Syntheses.md "wikilink"), Coll.
    Vol. 5, p.22 (1973); Vol. 42, p.12 (1962)
    [Link](http://www.orgsynth.org/orgsyn/pdfs/CV5P0022.pdf)