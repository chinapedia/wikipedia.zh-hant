[Folio_19v_-_The_Martyrdom_of_Saint_Mark.jpg](https://zh.wikipedia.org/wiki/File:Folio_19v_-_The_Martyrdom_of_Saint_Mark.jpg "fig:Folio_19v_-_The_Martyrdom_of_Saint_Mark.jpg")，位于[尚蒂利的](../Page/尚蒂利.md "wikilink")\]\]

**圣史马尔谷**（；；；；），[新教汉譯為](../Page/新教.md "wikilink")**马可**，唐朝[景教漢譯為](../Page/景教.md "wikilink")**摩矩辭法王**，[新約人物](../Page/新約.md "wikilink")，一般相信他是《[馬爾谷福音](../Page/马可福音.md "wikilink")》的作者，為[耶穌](../Page/耶穌.md "wikilink")[七十門徒之一](../Page/七十門徒.md "wikilink")，[亞歷山大科普特正教會的](../Page/亞歷山大科普特正教會.md "wikilink")[建立者](../Page/五大牧首.md "wikilink")。

圣玛尔谷的[瞻礼日在](../Page/罗马天主教圣人历.md "wikilink")4月25日\[1\]，他的[象征是](../Page/圣人符号学.md "wikilink")[獅鷲](../Page/獅鷲.md "wikilink")。\[2\]

## 教会传统与文献记载

傳統上相信，《[新約](../Page/新約.md "wikilink")》中的[約翰·馬可](../Page/約翰·馬可.md "wikilink")\[3\]，就是指圣史马尔谷。[天主教視他是一位聖人](../Page/天主教.md "wikilink")，稱為**聖若望·馬爾谷**。但是這在古代[教父中仍有爭議](../Page/教父.md "wikilink")，如[羅馬主教](../Page/羅馬主教.md "wikilink")（Hippolytus
of
Rome）的著作中，認圣史马尔谷，與[若望·马尔谷](../Page/約翰·馬可.md "wikilink")、[巴尔纳伯的亲戚马尔谷是三个不同人物](../Page/巴尔纳伯.md "wikilink")。\[4\]按照圣希玻里的意见，这三位马尔谷都属于耶稣派往[犹太传教的](../Page/犹太_\(罗马行省\).md "wikilink")[七十门徒](../Page/七十门徒.md "wikilink")。由此推断，当耶稣解释他的圣体是“真正可喫的”，他的圣血是“真正可饮的”时，许多门徒离开了他\[5\]，马尔谷大概也在其内。之后，他在[宗徒](../Page/宗徒.md "wikilink")[伯多禄的引导下](../Page/伯多禄.md "wikilink")，重拾信心，成了伯多禄的帮手，并写下了马尔谷福音，并建立了亞歷山大科普特正教會，成为其第一任主教。

根据史学家[游社博的记载](../Page/該撒利亞的優西比烏.md "wikilink")(*Eccl. Hist.
2.9.1–4*)，[黑落德·阿格黎帕一世统治犹太全省的第一年](../Page/希律亚基帕一世.md "wikilink")（公元41年）里，杀害了[载伯德的儿子雅各伯并抓捕了伯多禄](../Page/圣雅各伯.md "wikilink")，计划于[逾越节之后也将其杀害](../Page/逾越节.md "wikilink")。之后伯多禄被[天使所救](../Page/天使.md "wikilink")，逃离了黑落德的势力范围。伯多禄随后去了[安条克](../Page/安条克.md "wikilink")，再经过[小亚细亚](../Page/小亚细亚.md "wikilink")（到访了位于[本都](../Page/本都.md "wikilink")、[加拉太](../Page/加拉太.md "wikilink")、[卡帕多细亚](../Page/卡帕多细亚.md "wikilink")、[亚细亚行省](../Page/弗里吉亞.md "wikilink")、[比提尼亚的教会](../Page/比提尼亚.md "wikilink")\[6\]），于克劳狄一世在位第二年（公元42年）到达了罗马(*Eccl,
Hist.
2.14.6*)。在此途中，伯多禄邀请了马尔谷与他一起，做他的旅伴和翻译者。圣史马尔谷于是记录下了伯多禄的讲道词，因此马尔谷写下[福音](../Page/马可福音.md "wikilink")(*Eccl.
Hist.*
15–16)，应是在他于公元43年离开罗马前往[亚历山大港之前](../Page/亚历山大港.md "wikilink")。\[7\]

在公元49年，即[耶稣升天后](../Page/耶稣升天.md "wikilink")19年，马尔谷到达了[亚历山大港](../Page/亚历山大港.md "wikilink")\[8\]，并建立了亚历山大教会——现今，[亚历山大科普特正教会与](../Page/亚历山大科普特正教会.md "wikilink")都宣称继承了其传统。\[9\]科普特正教会的历法可以追溯到马尔谷本人。马尔谷成为了亚历山大第一任[主教](../Page/主教.md "wikilink")（[牧首](../Page/牧首.md "wikilink")），并被尊为[非洲基督教会的建立者](../Page/非洲.md "wikilink")。\[10\]

根据游社博所记(*Eccl. Hist.* 2.24.1)，马尔谷的主教职位由(St.
Annianus)接任，时于[尼禄在位第八年](../Page/尼禄.md "wikilink")（62年或63年），这有可能是因为马尔谷的殉难。但是之后的科普特传统上认为马尔谷是在68年殉道的。\[11\]\[12\]\[13\]\[14\]\[15\]

马尔谷被确信是于耶穌在[革责玛尼园被逮捕当晚](../Page/客西马尼园.md "wikilink")，那个落下衣服、赤身逃走的[少年](../Page/少年.md "wikilink")。\[16\]

[科普特正教会确定](../Page/科普特正教会.md "wikilink")[圣史马尔谷与](../Page/聖馬爾谷.md "wikilink")[若望·马尔谷为同一人](../Page/若望·马尔谷.md "wikilink")\[17\]，并叙述了在[耶稣受难后](../Page/耶稣受难.md "wikilink")，众位门徒留在[马可楼中](../Page/马可楼.md "wikilink")，以及[复活后的耶稣在马尔谷家中显现](../Page/耶稣复活.md "wikilink")\[18\]，还有在[五旬节时](../Page/五旬节.md "wikilink")[圣神降临于门徒也在马尔谷家中](../Page/圣神.md "wikilink")。\[19\]甚至，马尔谷被认为是[加纳婚宴上招待客人的侍者中的一人](../Page/加纳婚宴.md "wikilink")。\[20\]

## 传教经过

在宗徒[圣保禄第一次出外传教时](../Page/保禄.md "wikilink")，他与[圣巴尔纳伯](../Page/巴尔纳伯.md "wikilink")\]由[耶路撒冷回到](../Page/耶路撒冷.md "wikilink")[安条克](../Page/安条克.md "wikilink")，马尔谷随他们同往塞浦路斯。过了一段时间，马尔谷回到了耶路撒冷。因此，当保禄与巴尔纳伯准备第二次出外传教时，巴尔纳伯愿意也带马尔谷去，但保禄认为不应该带他去，因为马尔谷在[旁非利亚离开了他们](../Page/潘菲利亚.md "wikilink")。两人起了争执，于是巴尔纳伯带着马尔谷去了[塞浦路斯](../Page/塞浦路斯.md "wikilink")，保禄带了去了[叙利亚和](../Page/叙利亚_\(罗马行省\).md "wikilink")[基里基雅](../Page/奇里乞亚.md "wikilink")。

尽管如此，巴尔纳伯及马尔谷和保禄分离后，不久后又成了保禄的好朋友。\[21\]

圣保禄被捕后，首次解往[罗马时](../Page/罗马.md "wikilink")，马尔谷随侍左右。圣保禄第二次在罗马被囚禁时，殉道前不久，致书给[弟茂德](../Page/弟茂德.md "wikilink")，嘱他带马尔谷同行。

圣玛尔谷与圣伯多禄之间有着很密切的关系，如前面几段所叙述。伯多禄在罗马写信给信友，称马尔谷为“我儿马尔谷”\[22\]。

## 殉道与圣髑下落

关于圣玛尔谷殉道的时间尚不确定，有可能是在公元68年或者公元74年\[23\]\[24\]。据说是当马尔谷致力传扬基督教之时，由仇恨他的人抓住，并将绳索套在他的脖子上，拖拽过数个街道致死。\[25\]

[Stmark.jpg](https://zh.wikipedia.org/wiki/File:Stmark.jpg "fig:Stmark.jpg")所作，位于[佛罗伦萨圣弥额尔教堂](../Page/佛罗伦萨圣弥额尔教堂.md "wikilink")\]\]

关于圣玛尔谷的[圣髑](../Page/圣髑.md "wikilink")，起初被放置在[亚历山大港](../Page/亚历山大港.md "wikilink")。在828年，一部分被相信是圣玛尔谷圣髑的骨骸被[威尼斯的商人们从亚历山大港偷出](../Page/威尼斯.md "wikilink")。\[26\]\[27\]
这样的事情在当時可能会引起了严重的政治性后果。当圣髑到达威尼斯时，当时的总督为此建立了一座教堂来保存圣髑，这就是最初的[圣玛尔谷教堂](../Page/圣马尔谷圣殿宗主教座堂.md "wikilink")。\[28\]

在1063年，在将圣玛尔谷教堂扩建为[宗座圣殿时](../Page/宗座圣殿.md "wikilink")，发现圣玛尔谷的圣髑已遗失。然而，根据传说，于1094年，圣人自己显现指明了他遗体的具体位置。\[29\]
新发现的圣髑被放置在圣殿的[石棺内](../Page/石棺.md "wikilink")。\[30\]

[科普特基督徒们相信圣玛尔谷的头被保存在亚历山大港的](../Page/科普特人.md "wikilink")[聖馬可主教座堂](../Page/科普特正教会圣马可主教座堂_\(亚历山大城\).md "wikilink")，而部分遗体被保存在[开罗的](../Page/开罗.md "wikilink")[聖馬可主教座堂](../Page/科普特正教会圣马可主教座堂_\(开罗\).md "wikilink")。其余部分圣髑则位于意大利威尼斯的[圣马尔谷圣殿宗主教座堂](../Page/圣马尔谷圣殿宗主教座堂.md "wikilink")\[31\]

1968年，[教宗保禄六世将圣玛尔谷的余下圣髑归还给](../Page/保禄六世.md "wikilink")[科普特正教会牧首](../Page/亚历山大科普特正教教宗列表.md "wikilink")。

## 参见

  - [对观福音](../Page/对观福音.md "wikilink")：[马可福音](../Page/马可福音.md "wikilink")
  - [五大牧首](../Page/五大牧首.md "wikilink")：[亚历山大牧首](../Page/亚历山大牧首.md "wikilink")
  - [亞歷山大科普特正教會](../Page/亞歷山大科普特正教會.md "wikilink")

## 参考文献

### 引用

### 书籍

  -
  -
  -
  -
## 外部链接

  - [H.B. Swete,
    '新约中的圣马尔谷'](https://web.archive.org/web/20080422213152/http://homepage.mac.com/rc.vervoorn/swete/art13_a.html)

  - [H.B. Swete,
    '早期传统中的圣马尔谷'](https://web.archive.org/web/20080422213157/http://homepage.mac.com/rc.vervoorn/swete/art13_b.html)

  - [St. Mark the Apostle, Evangelist, and Preacher of the Christian
    Faith in
    Africa](http://www.copticchurch.net/topics/synexarion/mark.html)

  - [天主教百科全书: *St. Mark*](http://www.newadvent.org/cathen/09672c.htm)

  - [The Life, Miracles and Martyrdom of St. Mark the Evangelist of
    Jesus Christ](http://www.catholic-saints.net/saints/st-mark.php)

  - [H.B. Swete, 'St. Mark in the New
    Testament'](http://hbswete.co.uk/art13_a.html)

  - [H.B. Swete, 'St. Mark in Early
    Tradition'](http://hbswete.co.uk/art13_b.html)

  -
  - [Apostle Mark the Evangelist of the
    Seventy](http://ocafs.oca.org/FeastSaintsViewer.asp?SID=4&ID=1&FSID=100019)
    Orthodox [icon](../Page/icon.md "wikilink") and
    [synaxarion](../Page/synaxarion.md "wikilink")

  - [santiebeati](http://www.santiebeati.it/dettaglio/20850)

  - [saints.sqpn](http://saints.sqpn.com/catholic-encyclopedia-saint-mark/)

{{-}}

[Category:新约圣经中的人](../Category/新约圣经中的人.md "wikilink")
[Category:1世纪基督教圣人](../Category/1世纪基督教圣人.md "wikilink")
[Category:亞歷山大科普特正教會教宗](../Category/亞歷山大科普特正教會教宗.md "wikilink")

1.  [CALENDARIUM ROMANUM
    GENERALE](http://www.binetti.ru/collectio/liturgia/missale_files/crg.htm)

2.

3.  宗12:12；12:25；13:5；13:13-14；15:37-40

4.

5.  若6:44-66

6.  伯前1:1

7.

8.  参见宗15:39-41

9.   See drop-down essay on "Islamic Conquest and the Ottoman Empire"

10.

11.
12.

13. [Acts 15:36–40](http://www.biblegateway.com/passage/?search=acts%2015:36-40&version=NASB)

14. [2
    Timothy 4:11](http://www.biblegateway.com/passage/?search=2timothy%204:11&version=NASB)

15. [Philemon 1:24](http://www.biblegateway.com/passage/?search=philemon%201:24&version=NASB)

16. 参见宗徒大事录12:6-17，25；15:36-41；谷14:51-52

17.
18. 若20

19.
20. [H.H. Pope Shenouda
    III](../Page/Pope_Shenouda_III_of_Alexandria.md "wikilink"), *The
    Beholder of God Mark the Evangelist Saint and Martyr*, Chapter One.
    [Tasbeha.org](http://tasbeha.org/content/hh_books/Stmark/)

21. 参见格前6:6；哥4:10；弟后4:11；费24

22. 伯前5:13

23.
24.
25. [H.H. Pope Shenouda
    III](../Page/Pope_Shenouda_III_of_Alexandria.md "wikilink"). *The
    Beholder of God Mark the Evangelist Saint and Martyr*, Chapter
    Seven. [Tasbeha.org](http://tasbeha.org/content/hh_books/Stmark/)

26.
27.

28. [Gayford, Martin, "Treasures of Heaven, Saints, Relics and Devotion
    in Medieval Europe, British Museum",*The Telegraph*, June 11,
    2011](http://www.telegraph.co.uk/culture/art/8565805/Treasures-of-Heaven-Saints-Relics-and-Devotion-in-Medieval-Europe-British-Museum.html)

29.

30.

31.