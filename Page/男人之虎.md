[ManOfLaTiger2008.jpg](https://zh.wikipedia.org/wiki/File:ManOfLaTiger2008.jpg "fig:ManOfLaTiger2008.jpg")

《**男人之虎**》是[香港](../Page/香港.md "wikilink")[劇場組合聯合藝術總監](../Page/劇場組合.md "wikilink")[詹瑞文的獨腳戲](../Page/詹瑞文.md "wikilink")，一人分飾30多角，

2007年5月第7度公演，為「伸凸手版」，並以[香港特別行政區](../Page/香港特別行政區.md "wikilink")[特首](../Page/特首.md "wikilink")[選舉為題](../Page/選舉.md "wikilink")，加入「[煲呔曾](../Page/煲呔.md "wikilink")」及「[袋巾梁](../Page/袋巾.md "wikilink")」兩位[角色](../Page/角色.md "wikilink")，分別[借代](../Page/借代.md "wikilink")[2007年香港特別行政區行政長官選舉兩位候選人](../Page/2007年香港特別行政區行政長官選舉.md "wikilink")[曾蔭權和](../Page/曾蔭權.md "wikilink")[梁家傑](../Page/梁家傑.md "wikilink")，並由詹瑞文一人同時扮演兩位角色。

2008年6月第8度公演，取名為為「超級防火版」，並以2008年[北京奧運為題](../Page/2008年夏季奧林匹克運動會.md "wikilink")，由詹瑞文以火炬手造型飾演的「凸首詹」（即詹特首的諧音，實為扮演「煲呔曾」，借代當時香港特別行政區行政長官曾蔭權）作為主體。劇組在2008年5月6日在[旺角鬧市舉行](../Page/旺角.md "wikilink")「凸首補跑迎奧運」活動，詹瑞文扮演火炬手「凸首詹」，在兩名扮演[聖火護衛隊的工作人員護跑下](../Page/聖火護衛隊.md "wikilink")，從[新寶戲院跑往](../Page/新寶戲院.md "wikilink")[豉油街](../Page/豉油街.md "wikilink")，吸引不少途人圍觀\[1\]\[2\]。此外，在其宣傳海報上尤其放大傳送[香港區火炬接力的情節](../Page/2008年夏季奧林匹克運動會香港區火炬接力.md "wikilink")。

## 外部参考

  - [《男人之虎》官方網頁](http://www.mantiger.com)
  - [《男人之虎》創作人網誌](http://hk.myblog.yahoo.com/man-tiger)
  - [詹瑞文《男人之虎》扮凸手煲呔曾MK](http://www.youtube.com/watch?v=7jQ-bxWLC5o)
    [謝票](../Page/謝票.md "wikilink")

## 參考文獻

[category:香港戲劇作品](../Page/category:香港戲劇作品.md "wikilink")

[Category:香港舞台劇](../Category/香港舞台劇.md "wikilink")

1.
2.  [詹瑞文扮煲呔傳聖火](http://hk.youtube.com/watch?v=HlQGV_YhtT4)，蘋果動新聞，2008年05月06日