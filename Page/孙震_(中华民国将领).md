[Sun_Zhen.jpg](https://zh.wikipedia.org/wiki/File:Sun_Zhen.jpg "fig:Sun_Zhen.jpg")
**孫震**（），[譜名](../Page/譜名.md "wikilink")**定懋**，後改名**孫楙**，再改名**孫震**，字**德操**，別號**夢僧**。[中華民國軍事人物](../Page/中華民國.md "wikilink")，原籍[浙江](../Page/浙江.md "wikilink")[紹興](../Page/紹興縣.md "wikilink")[楊家壠](../Page/楊家壠.md "wikilink")（今[齊賢鎮](../Page/齊賢鎮.md "wikilink")），[光緒十八年](../Page/光緒.md "wikilink")（1892年）出生於[四川](../Page/四川.md "wikilink")[綿竹](../Page/綿竹.md "wikilink")\[1\]。他是[秦漢父親](../Page/秦漢_\(演員\).md "wikilink")[孫元良的叔父](../Page/孫元良.md "wikilink")。

## 早年

孫震於[宣統元年](../Page/宣統.md "wikilink")（1909年）進入[西安](../Page/西安.md "wikilink")[陸軍第二中學堂就讀](../Page/陸軍第二中學堂.md "wikilink")。之後加入[中國同盟會](../Page/中國同盟會.md "wikilink")。1912年，他進入[保定陸軍軍官學校第一期步兵科就讀](../Page/保定陸軍軍官學校.md "wikilink")，1913年[二次革命時](../Page/二次革命.md "wikilink")，他在學業未完成的狀況下離校跟隨[熊克武參加戰役](../Page/熊克武.md "wikilink")。1914年加入四川陸軍第二師擔任[排長一職](../Page/排長.md "wikilink")，後來持續升為[連長](../Page/連長.md "wikilink")、[營長](../Page/營長.md "wikilink")。1917年升為騎兵旅第一團[團長](../Page/團長.md "wikilink")，後來又升任為[旅長](../Page/旅長.md "wikilink")、[師長](../Page/師長.md "wikilink")、[川西北屯殖軍司令](../Page/川西北屯殖軍.md "wikilink")（1933年\[2\]）等職。1926年經姪兒[孫元良介紹投效](../Page/孫元良.md "wikilink")[國民革命軍](../Page/國民革命軍.md "wikilink")，擔任第二十九軍副軍長，參加[北伐](../Page/北伐.md "wikilink")。1935年升任第二十九軍[軍長](../Page/軍長.md "wikilink")。1936年2月25日被[國民政府授予](../Page/國民政府.md "wikilink")[陸軍](../Page/陸軍.md "wikilink")[中將軍銜](../Page/中將.md "wikilink")。

## 中國抗日戰爭

1937年7月，[中國抗日戰爭爆發](../Page/中國抗日戰爭.md "wikilink")。7月8日，川康軍事整理委員會在重慶召開第二次大會，主任委員[何應欽宣布日軍在](../Page/何應欽.md "wikilink")[蘆溝橋挑釁經過](../Page/蘆溝橋.md "wikilink")，謂中日大戰已不可避免，與會人員聞之憤慨異常，第四十一軍軍長孫震當場請纓，願率所部出川，參加抗戰\[3\]。孫震與四川省主席[劉湘一起出川](../Page/劉湘.md "wikilink")，擔任第二十二集團軍副司令，在[太原會戰後](../Page/太原會戰.md "wikilink")，1938年升任為第二十二集團軍總司令，後來奉令轉飭第四十一軍守衛[滕縣](../Page/滕縣.md "wikilink")，造就[滕縣戰鬥的勝利](../Page/滕縣戰鬥_\(抗日戰爭\).md "wikilink")，也使得之後的[台兒庄會戰與](../Page/台兒庄會戰.md "wikilink")[徐州會戰獲得勝利](../Page/徐州會戰.md "wikilink")。1939年5月，被授予[陸軍](../Page/陸軍.md "wikilink")[上將軍銜](../Page/上將.md "wikilink")。之後亦參與了[隨棗會戰](../Page/隨棗會戰.md "wikilink")、[棗宜會戰](../Page/棗宜會戰.md "wikilink")、[豫西鄂北會戰等等戰役](../Page/豫西鄂北會戰.md "wikilink")。

## 國共內戰

1947年5月26日，第五綏靖區奉令與陸軍總司令部鄭州指揮所合併，孫震繼范漢杰為鄭州陸軍指揮所主任\[4\]。[國共內戰期間](../Page/國共內戰.md "wikilink")，孫震曾先後擔任[徐州剿匪總司令部副總司令兼](../Page/徐州剿匪總司令部.md "wikilink")[徐州剿匪總部鄭州指揮部主任](../Page/徐州剿匪總司令部.md "wikilink")、[華中剿匪總司令部副總司令](../Page/華中剿匪總司令部.md "wikilink")、[第五綏靖區司令官兼川鄂邊區綏靖公署主任](../Page/第五綏靖區.md "wikilink")、西南軍政長官公署副長官\[5\]等職。

## 台灣時期

1949年12月，隨[國民政府](../Page/國民政府.md "wikilink")，從[四川到達](../Page/四川.md "wikilink")[台灣](../Page/台灣.md "wikilink")，1952年退役，後長期擔任總統府[戰略顧問](../Page/中華民國總統府戰略顧問.md "wikilink")、[光復大陸設計研究委員會委員](../Page/光復大陸設計研究委員會.md "wikilink")、[憲政研討委員會委員等職務](../Page/憲政研討委員會.md "wikilink")。1985年9月9日病逝於[台北](../Page/台北.md "wikilink")，享年93歲。

## 其他

孫震於1929年於[四川](../Page/四川.md "wikilink")[成都成立](../Page/成都.md "wikilink")[樹德小學和](../Page/樹德小學.md "wikilink")[樹德中學](../Page/樹德中學.md "wikilink")。1937年開辦高中。他曾著有《[八十年國事川事見聞錄](../Page/八十年國事川事見聞錄.md "wikilink")》與《[楙園隨筆](../Page/楙園隨筆.md "wikilink")》等書。

## 參考文獻

## 外部連結

  - [孫震個人資料](http://www.generals.dk/general/Sun_Zhen/_/China.html)

[Category:中華民國總統府戰略顧問](../Category/中華民國總統府戰略顧問.md "wikilink")
[Category:第1屆中華民國國民大會代表](../Category/第1屆中華民國國民大會代表.md "wikilink")
[Category:中華民國陸軍二級上將](../Category/中華民國陸軍二級上將.md "wikilink")
[Category:國共戰爭人物](../Category/國共戰爭人物.md "wikilink")
[Category:中國第二次世界大戰人物](../Category/中國第二次世界大戰人物.md "wikilink")
[Category:北伐人物](../Category/北伐人物.md "wikilink")
[Category:二次革命人物](../Category/二次革命人物.md "wikilink")
[Category:川軍將領](../Category/川軍將領.md "wikilink")
[Category:北洋将军府将军](../Category/北洋将军府将军.md "wikilink")
[Category:保定陆军军官学校校友](../Category/保定陆军军官学校校友.md "wikilink")
[Category:成都人](../Category/成都人.md "wikilink")
[Category:德陽人](../Category/德陽人.md "wikilink")
[Category:台灣戰後四川移民](../Category/台灣戰後四川移民.md "wikilink")
[Z震](../Category/孫姓.md "wikilink")

1.  有一說為[四川](../Page/四川.md "wikilink")[成都人](../Page/成都.md "wikilink")，見
    [台灣歷史辭典](http://taipedia.cca.gov.tw/taipedia/dict/DispText.aspx?id=2086&CurrPage=1&menu=0&n=10&key=&type=1&flags=2)
     與 [此](http://www.bashu.net/people/s/sunzhen.htm)

2.

3.

4.
5.  長官是[張群](../Page/張群.md "wikilink")