**渠江**，也称渠河，是[嘉陵江的一条支流](../Page/嘉陵江.md "wikilink")，有两个源头，东源州河出自[川](../Page/四川.md "wikilink")、[陕两省边界](../Page/陕西.md "wikilink")[大巴山西南麓](../Page/大巴山.md "wikilink")，北源巴河出自川、陕两省边界[米仓山南麓](../Page/米仓山.md "wikilink")，在[渠县](../Page/渠县.md "wikilink")[三汇镇汇合后就称为渠江](../Page/三汇镇.md "wikilink")。流经[渠县](../Page/渠县.md "wikilink")、[广安](../Page/广安.md "wikilink")、[岳池](../Page/岳池.md "wikilink")、[邻水等县境](../Page/邻水.md "wikilink")，在合川市钓鱼山下云门镇姚家沟村附近注入嘉陵江。全长720千米，流域面积3.92万平方千米，多年平均流量663立方米／秒。

[Category:四川河流](../Category/四川河流.md "wikilink")
[Category:嘉陵江水系](../Category/嘉陵江水系.md "wikilink")