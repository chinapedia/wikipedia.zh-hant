**子供之國線**（）\[1\]，又譯「兒童之國線」\[2\]，是一條連結[神奈川縣](../Page/神奈川縣.md "wikilink")[橫濱市](../Page/橫濱市.md "wikilink")[綠區](../Page/綠區_\(橫濱市\).md "wikilink")[長津田站與橫濱市](../Page/長津田站.md "wikilink")[青葉區](../Page/青葉區_\(橫濱市\).md "wikilink")[子供之國站的鐵路線](../Page/子供之國站_\(神奈川縣\).md "wikilink")。[橫濱高速鐵道作為](../Page/橫濱高速鐵道.md "wikilink")[第三種鐵道事業者保有軌道](../Page/鐵路公司#第三種鐵道事業.md "wikilink")，由[東京急行電鐵](../Page/東京急行電鐵.md "wikilink")（東急）作為[第二種鐵道事業者進行旅客運送](../Page/鐵路公司#第二種鐵道事業.md "wikilink")。2000年為止由子供之國協会保有設施。

路線圖與車站編號使用的路線顏色為藍色，路線記號為**KD**。

與其他的第二種鐵道事業線不同，車站等顯示的是第三種鐵道事業者橫濱高速鐵道的公司名。

## 路線資料

  - 管轄：橫濱高速鐵道（第三種鐵道事業者）、東京急行電鐵（第二種鐵道事業者）
  - 路線距離：3.4公里
  - [軌距](../Page/軌距.md "wikilink")：1067毫米
  - 複線路段：沒有（全線單線）
  - 電氣化路段：全線（直流1500V）
  - [閉塞方式](../Page/閉塞_\(鐵路\).md "wikilink")：車內信號閉塞式（[東急新CS-ATC](../Page/列車自動控制系統#東急田園都市線.md "wikilink")）

## 車輛

子供之國線的車輛如下。

  - 初代（開業時－1975年）： -
  - 第2代（1975年－1980年）： Deha 3608-Kuha 3772
  - 第3代（1980年－1989年）： Deha 7200-Kuha 7500（鋁車體試作車）
  - 第4代（1989年－2000年）： Deha 7057-Deha 7052（一人改造車）
  - 第5代（1999年－）：
      - 2節編組。多客時將2班連結成為4節編組運行。

<File:Tokyu> 7500 Kodomonokuni 198508a.jpg|東急7200系鋁車體試作車（1985年8月）
[File:Tokyu-7000.jpg|Y000系入線使用的東急7000系列車（1998年頃](File:Tokyu-7000.jpg%7CY000系入線使用的東急7000系列車（1998年頃)）
[File:Tokyu-8084-Kodomonokuni-Line.jpg|子供之國線使用的東急8090系列車（1991年頃](File:Tokyu-8084-Kodomonokuni-Line.jpg%7C子供之國線使用的東急8090系列車（1991年頃)）

## 車費

車費在通勤線化以前與其他東急各線屬於不同體系。全線均一制，截至2014年4月1日，使用IC卡為154日圓，購買車票為160日圓（兒童分別為77日圓、80日圓）。

## 車站列表

  - 所有車站均位於[神奈川縣](../Page/神奈川縣.md "wikilink")[橫濱市](../Page/橫濱市.md "wikilink")。
  - 2012年2月上旬起依次引入車站編號\[3\]。
  - 除起點站外均為[無人站](../Page/無人站.md "wikilink")。起點的長津田站本路線部分亦無站員常駐。

{{-}}

<table>
<thead>
<tr class="header">
<th><p>車站編號</p></th>
<th><p>中文站名</p></th>
<th><p>日文站名</p></th>
<th><p>英文站名</p></th>
<th><p>站間距離</p></th>
<th><p>累計距離</p></th>
<th><p>接續路線</p></th>
<th><p>所在地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>KD01</p></td>
<td><p><a href="../Page/長津田站.md" title="wikilink">長津田</a></p></td>
<td></td>
<td></td>
<td><p>-</p></td>
<td><p>0.0</p></td>
<td><p><a href="../Page/東京急行電鐵.md" title="wikilink">東京急行電鐵</a>： <a href="../Page/田園都市線.md" title="wikilink">田園都市線</a>（DT22）<br />
<a href="../Page/東日本旅客鐵道.md" title="wikilink">東日本旅客鐵道</a>： <a href="../Page/橫濱線.md" title="wikilink">橫濱線</a>（JH21）</p></td>
<td><p><a href="../Page/綠區_(橫濱市).md" title="wikilink">綠區</a></p></td>
</tr>
<tr class="even">
<td><p>KD02</p></td>
<td><p><a href="../Page/恩田站.md" title="wikilink">恩田</a></p></td>
<td></td>
<td></td>
<td><p>1.8</p></td>
<td><p>1.8</p></td>
<td><p> </p></td>
<td><p><a href="../Page/青葉區_(橫濱市).md" title="wikilink">青葉區</a></p></td>
</tr>
<tr class="odd">
<td><p>KD03</p></td>
<td><p><a href="../Page/子供之國站_(神奈川縣).md" title="wikilink">子供之國</a></p></td>
<td></td>
<td></td>
<td><p>1.6</p></td>
<td><p>3.4</p></td>
<td><p> </p></td>
<td></td>
</tr>
</tbody>
</table>

## 參見

  -
  - [日本鐵路線列表](../Page/日本鐵路線列表.md "wikilink")

  - [西武山口線](../Page/山口線_\(西武鐵道\).md "wikilink") - 起初為遊戯設施，後來改為鐵路線

  - [港未來線](../Page/港未來線.md "wikilink") - 橫濱高速鐵道本身擁有的路線

## 參考資料

[線](../Category/橫濱高速鐵道.md "wikilink")
[Category:東京急行電鐵路線](../Category/東京急行電鐵路線.md "wikilink")
[Category:關東地方鐵路路線](../Category/關東地方鐵路路線.md "wikilink")
[Category:1967年啟用的鐵路線](../Category/1967年啟用的鐵路線.md "wikilink")

1.
2.
3.  [東急線全駅で駅ナンバリングを導入します](http://www.tokyu.co.jp/contents_index/guide/news/120126-1.html)
     - 東京急行電鉄、2012年1月26日、2012年1月26日閲覧。