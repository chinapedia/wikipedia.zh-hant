[天心区](../Page/天心区.md "wikilink"){{.w}}[开福区](../Page/开福区.md "wikilink"){{.w}}[雨花区](../Page/雨花区.md "wikilink"){{.w}}[望城区](../Page/望城区.md "wikilink"){{.w}}[浏阳市](../Page/浏阳市.md "wikilink"){{.w}}[宁乡市](../Page/宁乡市.md "wikilink"){{.w}}[长沙县](../Page/长沙县.md "wikilink")

|group3 = [株洲市](../Page/株洲市.md "wikilink") |list3 =
[天元区](../Page/天元区.md "wikilink"){{.w}}[荷塘区](../Page/荷塘区.md "wikilink"){{.w}}[芦淞区](../Page/芦淞区.md "wikilink"){{.w}}[石峰区](../Page/石峰区.md "wikilink"){{.w}}[渌口区](../Page/渌口区.md "wikilink"){{.w}}[醴陵市](../Page/醴陵市.md "wikilink"){{.w}}[攸县](../Page/攸县.md "wikilink"){{.w}}[茶陵县](../Page/茶陵县.md "wikilink"){{.w}}[炎陵县](../Page/炎陵县.md "wikilink")

|group4 = [湘潭市](../Page/湘潭市.md "wikilink") |list4 =
[岳塘区](../Page/岳塘区.md "wikilink"){{.w}}[雨湖区](../Page/雨湖区.md "wikilink"){{.w}}[湘乡市](../Page/湘乡市.md "wikilink"){{.w}}[韶山市](../Page/韶山市.md "wikilink"){{.w}}[湘潭县](../Page/湘潭县.md "wikilink")

|group5 = [衡阳市](../Page/衡阳市.md "wikilink") |list5 =
[蒸湘区](../Page/蒸湘区.md "wikilink"){{.w}}[珠晖区](../Page/珠晖区.md "wikilink"){{.w}}[雁峰区](../Page/雁峰区.md "wikilink"){{.w}}[石鼓区](../Page/石鼓区.md "wikilink"){{.w}}[南岳区](../Page/南岳区.md "wikilink"){{.w}}[耒阳市](../Page/耒阳市.md "wikilink"){{.w}}[常宁市](../Page/常宁市.md "wikilink"){{.w}}[衡阳县](../Page/衡阳县.md "wikilink"){{.w}}[衡南县](../Page/衡南县.md "wikilink"){{.w}}[衡山县](../Page/衡山县.md "wikilink"){{.w}}[衡东县](../Page/衡东县.md "wikilink"){{.w}}[祁东县](../Page/祁东县.md "wikilink")

|group6 = [邵阳市](../Page/邵阳市.md "wikilink") |list6 =
[大祥区](../Page/大祥区.md "wikilink"){{.w}}[双清区](../Page/双清区.md "wikilink"){{.w}}[北塔区](../Page/北塔区.md "wikilink"){{.w}}[武冈市](../Page/武冈市.md "wikilink"){{.w}}[邵东县](../Page/邵东县.md "wikilink"){{.w}}[新邵县](../Page/新邵县.md "wikilink"){{.w}}[邵阳县](../Page/邵阳县.md "wikilink"){{.w}}[隆回县](../Page/隆回县.md "wikilink"){{.w}}[洞口县](../Page/洞口县.md "wikilink"){{.w}}[绥宁县](../Page/绥宁县.md "wikilink"){{.w}}[新宁县](../Page/新宁县.md "wikilink"){{.w}}[城步苗族自治县](../Page/城步苗族自治县.md "wikilink")

|group7 = [岳阳市](../Page/岳阳市.md "wikilink") |list7 =
[岳阳楼区](../Page/岳阳楼区.md "wikilink"){{.w}}[云溪区](../Page/云溪区.md "wikilink"){{.w}}[君山区](../Page/君山区.md "wikilink"){{.w}}[汨罗市](../Page/汨罗市.md "wikilink"){{.w}}[临湘市](../Page/临湘市.md "wikilink"){{.w}}[岳阳县](../Page/岳阳县.md "wikilink"){{.w}}[华容县](../Page/华容县.md "wikilink"){{.w}}[湘阴县](../Page/湘阴县.md "wikilink"){{.w}}[平江县](../Page/平江县.md "wikilink")

|group8 = [常德市](../Page/常德市.md "wikilink") |list8 =
[武陵区](../Page/武陵区.md "wikilink"){{.w}}[鼎城区](../Page/鼎城区.md "wikilink"){{.w}}[津市市](../Page/津市市.md "wikilink"){{.w}}[安乡县](../Page/安乡县.md "wikilink"){{.w}}[汉寿县](../Page/汉寿县.md "wikilink"){{.w}}[澧县](../Page/澧县.md "wikilink"){{.w}}[临澧县](../Page/临澧县.md "wikilink"){{.w}}[桃源县](../Page/桃源县.md "wikilink"){{.w}}[石门县](../Page/石门县.md "wikilink")

|group9 = [张家界市](../Page/张家界市.md "wikilink") |list9 =
[永定区](../Page/永定区_\(张家界市\).md "wikilink"){{.w}}[武陵源区](../Page/武陵源区.md "wikilink"){{.w}}[慈利县](../Page/慈利县.md "wikilink"){{.w}}[桑植县](../Page/桑植县.md "wikilink")

|group10 = [益阳市](../Page/益阳市.md "wikilink") |list10 =
[赫山区](../Page/赫山区.md "wikilink"){{.w}}[资阳区](../Page/资阳区.md "wikilink"){{.w}}[沅江市](../Page/沅江市.md "wikilink"){{.w}}[南县](../Page/南县.md "wikilink"){{.w}}[桃江县](../Page/桃江县.md "wikilink"){{.w}}[安化县](../Page/安化县.md "wikilink")

|group11 = [郴州市](../Page/郴州市.md "wikilink") |list11 =
[北湖区](../Page/北湖区.md "wikilink"){{.w}}[苏仙区](../Page/苏仙区.md "wikilink"){{.w}}[资兴市](../Page/资兴市.md "wikilink"){{.w}}[桂阳县](../Page/桂阳县.md "wikilink"){{.w}}[宜章县](../Page/宜章县.md "wikilink"){{.w}}[永兴县](../Page/永兴县.md "wikilink"){{.w}}[嘉禾县](../Page/嘉禾县.md "wikilink"){{.w}}[临武县](../Page/临武县.md "wikilink"){{.w}}[汝城县](../Page/汝城县.md "wikilink"){{.w}}[桂东县](../Page/桂东县.md "wikilink"){{.w}}[安仁县](../Page/安仁县.md "wikilink")

|group12 = [永州市](../Page/永州市.md "wikilink") |list12 =
[冷水滩区](../Page/冷水滩区.md "wikilink"){{.w}}[零陵区](../Page/零陵区.md "wikilink"){{.w}}[祁阳县](../Page/祁阳县.md "wikilink"){{.w}}[东安县](../Page/东安县.md "wikilink"){{.w}}[双牌县](../Page/双牌县.md "wikilink"){{.w}}[道县](../Page/道县.md "wikilink"){{.w}}[江永县](../Page/江永县.md "wikilink"){{.w}}[宁远县](../Page/宁远县.md "wikilink"){{.w}}[蓝山县](../Page/蓝山县.md "wikilink"){{.w}}[新田县](../Page/新田县.md "wikilink"){{.w}}[江华瑶族自治县](../Page/江华瑶族自治县.md "wikilink")

|group13 = [怀化市](../Page/怀化市.md "wikilink") |list13 =
[鹤城区](../Page/鹤城区.md "wikilink"){{.w}}[洪江市](../Page/洪江市.md "wikilink"){{.w}}[中方县](../Page/中方县.md "wikilink"){{.w}}[沅陵县](../Page/沅陵县.md "wikilink"){{.w}}[辰溪县](../Page/辰溪县.md "wikilink"){{.w}}[溆浦县](../Page/溆浦县.md "wikilink"){{.w}}[会同县](../Page/会同县.md "wikilink"){{.w}}[麻阳苗族自治县](../Page/麻阳苗族自治县.md "wikilink"){{.w}}[新晃侗族自治县](../Page/新晃侗族自治县.md "wikilink"){{.w}}[芷江侗族自治县](../Page/芷江侗族自治县.md "wikilink"){{.w}}[靖州苗族侗族自治县](../Page/靖州苗族侗族自治县.md "wikilink"){{.w}}[通道侗族自治县](../Page/通道侗族自治县.md "wikilink")

|group14 = [娄底市](../Page/娄底市.md "wikilink") |list14 =
[娄星区](../Page/娄星区.md "wikilink"){{.w}}[冷水江市](../Page/冷水江市.md "wikilink"){{.w}}[涟源市](../Page/涟源市.md "wikilink"){{.w}}[双峰县](../Page/双峰县.md "wikilink"){{.w}}[新化县](../Page/新化县.md "wikilink")
}}

|group3style =text-align: center; |group3 =
[自治州](../Page/自治州.md "wikilink") |list3 =
[泸溪县](../Page/泸溪县.md "wikilink"){{.w}}[凤凰县](../Page/凤凰县.md "wikilink"){{.w}}[花垣县](../Page/花垣县.md "wikilink"){{.w}}[保靖县](../Page/保靖县.md "wikilink"){{.w}}[古丈县](../Page/古丈县.md "wikilink"){{.w}}[永顺县](../Page/永顺县.md "wikilink"){{.w}}[龙山县](../Page/龙山县.md "wikilink")
}}

|belowstyle = text-align: left; font-size: 80%; |below =
参见：[中华人民共和国县级以上行政区列表](../Page/中华人民共和国县级以上行政区列表.md "wikilink")、[湖南省乡级以上行政区列表](../Page/湖南省乡级以上行政区列表.md "wikilink")。
}}<noinclude>

</noinclude>

[Category:中华人民共和国各省级行政区行政区划模板](../Category/中华人民共和国各省级行政区行政区划模板.md "wikilink")
[\*](../Category/湖南行政区划.md "wikilink")
[湖南行政区划模板](../Category/湖南行政区划模板.md "wikilink")