**奧林匹克運動會**（、、），簡稱**奧運會**、**奧運**，是[國際上最主要的綜合型](../Page/國際.md "wikilink")[體育賽事](../Page/運動會.md "wikilink")，由[國際奧林匹克委員會主辦](../Page/國際奧林匹克委員會.md "wikilink")；後因有別[冬季奧林匹克運動會](../Page/冬季奧林匹克運動會.md "wikilink")，又稱為「[夏季奧林匹克運動會](../Page/夏季奧林匹克運動會.md "wikilink")」，每4年舉行一次（從1987年起，冬季奧運會和夏季奧運會分開，每兩年交替舉行）。奥林匹克運動會[最早起源於](../Page/古代奥林匹克運動會.md "wikilink")[古希腊](../Page/古希腊.md "wikilink")，因為舉辦地在[奧林匹亚而得名](../Page/奧林匹亚.md "wikilink")；但因基督教皇帝禁止奧運競技，於是奧運在舉辦超過一千年後，大約於四世紀末或五世紀初停辦，奧運停辦了近1,500年，直到19世纪末由[法国的](../Page/法国.md "wikilink")[顾拜旦男爵創立了有真正奧運精神的](../Page/顾拜旦.md "wikilink")[現代奧林匹克運動會](../Page/現代.md "wikilink")。从此，自1896始每4年舉辦一次，只有在两次[世界大戰期間中斷過](../Page/世界大戰.md "wikilink")3次（分別是在1916年、1940年和1944年），更確立了會期不超過16日的傳統。奧林匹克運動會現在已經成為了全世界[和平與](../Page/和平.md "wikilink")[友誼的象徵](../Page/友誼.md "wikilink")。

## 古代奧林匹克運動會

根据[希臘神话](../Page/希臘神话.md "wikilink")，大力神[海克力斯在](../Page/海克力斯.md "wikilink")[奥林匹亚赢得了一场比赛的胜利后下令](../Page/奥林匹亚.md "wikilink")，类似的比赛应该每四年在奥林匹亚举行一次。另一个传说则称，[宙斯在战胜](../Page/宙斯.md "wikilink")[提坦巨人](../Page/提坦.md "wikilink")[克洛诺斯后创立了奥林匹克运动会](../Page/克洛诺斯.md "wikilink")。但比較現實的說法是，在基督教的勢力還很薄弱時，另有一種信仰，名叫
自然崇拜
，此信仰相信女人才是創世主，而代表女神之神維納斯的金星，也成為此信仰的標誌。他們發現，若每八年觀察一次，會發現這八年間，金星的軌道連起來會成為著名的五芒星，他們認為這是神的指示，所以就每八年舉辦一次運動會(無名)。而當此信仰消失後，在某個不可考的年代，當代領袖再次舉辦此運動會，並命名
奧林匹克。

运动会是在祭神的[奥林匹亚举行](../Page/奥林匹亚.md "wikilink")，那里有一个12米高的用象牙和黄金铸成的[宙斯神像](../Page/宙斯.md "wikilink")，这个神像是[世界七大奇迹之一](../Page/世界七大奇迹.md "wikilink")。

第一个有文字记载的奥运会于公元前776年举行，但是我们可以确定之前奥运会就已经存在了。当时只有短跑（希腊人称之为“斯泰德”*Stadion*，意为“场地跑步”，英语中“[体育场](../Page/体育场.md "wikilink")”*stadium*一词就出于此）一个项目，长度为192.27米，因为这是大力神脚长的六百倍。奥运会每四年举行一次，后来希腊人还将奥运会作为一种计算时间的方式，一个“奥林匹亚”就是两次奥运会之间的时间——4年。

后来，其他项目逐渐地加入到奥运会中：[拳击](../Page/拳击.md "wikilink")、[-{摔跤}-](../Page/摔跤.md "wikilink")、[古希臘式搏擊和](../Page/古希臘式搏擊.md "wikilink")[田径](../Page/田径.md "wikilink")（包括场地跑、[跳远](../Page/跳远.md "wikilink")、[标枪和](../Page/标枪.md "wikilink")[铁饼](../Page/铁饼.md "wikilink")）。新项目的加入使得运动会的长度从1天延长到5天，其中3天有比赛，其他2天则从事宗教活动。最后一天所有的参赛选手都可以参加一场盛宴，享用在比赛第一天时供奉给[宙斯的一百頭牛](../Page/宙斯.md "wikilink")。奥运会项目获胜者的奖品是[橄榄枝编成的花环以及莫大的荣誉](../Page/橄欖_\(木樨科\).md "wikilink")。雕塑家们还为获胜者雕刻人像。古人曾经约定奥运会举行期间，各城邦互不交战，久而久之，[橄榄枝就成了](../Page/橄榄枝.md "wikilink")[和平的象征](../Page/和平.md "wikilink")。因此奧運會雖然在現代是各國運動競技的項目，但主要的精神仍是以世界和平為主。

但在393年，[罗马帝国皇帝](../Page/罗马帝国.md "wikilink")[狄奧多西一世废除了古代奧林匹克会](../Page/狄奧多西一世.md "wikilink")，认为这是一项“异教徒的活动”。

## 現代奧林匹克運動會

[Olympic-flag-Victoria.jpg](https://zh.wikipedia.org/wiki/File:Olympic-flag-Victoria.jpg "fig:Olympic-flag-Victoria.jpg")
[Coubertin.jpg](https://zh.wikipedia.org/wiki/File:Coubertin.jpg "fig:Coubertin.jpg")

[德国人库齐乌斯花了多年时间挖掘古希腊的奥林匹亚村](../Page/德国.md "wikilink")，他在1852年1月在[柏林宣读了考察报告](../Page/柏林.md "wikilink")，并建议恢复奥运会。

被尊称为“现代奥林匹克之父”的法国教育家[皮埃尔·德·顾拜旦于](../Page/皮埃尔·德·顾拜旦.md "wikilink")1892年在[索邦大学把范围扩大到全世界](../Page/索邦大学.md "wikilink")。1894年，顾拜旦致函各国体育组织，邀请他们参加在[巴黎举行的国际体育大会](../Page/巴黎.md "wikilink")。在同年6月16日举行12国的代表在巴黎举行了“恢复奥林匹克运动大会”。会议决议每四年举行一次全球范围的奥林匹克运动会。6月23日，[国际奥林匹克委员会成立](../Page/国际奥林匹克委员会.md "wikilink")，希腊人[泽麦特里乌斯·维凯拉斯出任主席](../Page/泽麦特里乌斯·维凯拉斯.md "wikilink")，顾拜旦任秘书长，并亲自设计了奥运会的会徽、会旗。会议还通过了[奥林匹克宪章](../Page/奥林匹克宪章.md "wikilink")。1896年，第一届现代奥林匹克运动会终于在希腊[雅典正式举行](../Page/雅典.md "wikilink")。并决定此后每4年举行一次，会期不超过16天。

历届奥运会从1896年开始，每4年舉辦一次，其中第6届（1916年）、第12届（1940年）、第13届（1944年）由于[战争的原因没有举行](../Page/战争.md "wikilink")，但是届数仍然按照顺序排列。

[1896年奧運在](../Page/1896年夏季奥林匹克运动会.md "wikilink")1896年4月5日举行，虽然只有13个国家300名运动员参加，但是它第一次成为了国际性的比赛。这一届的冠军没有[金牌](../Page/金牌.md "wikilink")，因此不能被称作“金牌获得者”。在这一届奥运会上，也确定了奥运会由奥委会的各成员国轮流举办的原则。

[1900年奧運第一次有女子运动员参加](../Page/1900年夏季奥林匹克运动会.md "wikilink")，这也是女子进入体育运动的开始。[1912年奧運首次实行了限制职业运动员参赛的规定](../Page/1912年夏季奥林匹克运动会.md "wikilink")，从这届开始国际奥委会对女子参赛由默认转为公开支持的态度。

[1908年奧運时](../Page/1908年夏季奥林匹克运动会.md "wikilink")，在[圣保罗大教堂举行奥运会的宗教仪式上](../Page/圣保罗大教堂.md "wikilink")，[美国](../Page/美国.md "wikilink")[宾夕法尼亚州大主教在布道词中说](../Page/宾夕法尼亚州.md "wikilink")，奥运会“重要的是参与，不是胜利”，顾拜旦很欣赏这句话，以后多次引用，使得不少人认为这句话应该成为奥林匹克理想。1913年，为了宣传奥林匹克精神、鼓励参赛运动员，由顾拜旦提议，经国际奥委会批准，将“Citius、Altius、Fortius”（拉丁語：更快、更高、更强）作为奥林匹克格言。

[1920年奧運首次采用了国际奥委会会旗](../Page/1920年夏季奥林匹克运动会.md "wikilink")——[奥林匹克五环旗](../Page/奥林匹克五环.md "wikilink")。此外，开幕式上的运动员宣誓仪式和闭幕式上的会旗移交仪式也是从这一届开始的。[1928年奧運第一次在开幕式上点燃了火炬](../Page/1928年夏季奥林匹克运动会.md "wikilink")，这一形式也被以后的奥运会承袭下来。[1936年奧運首创了火炬接力并在开幕式上点燃火炬的仪式](../Page/1936年夏季奥林匹克运动会.md "wikilink")。[1968年奧運增加了裁判员宣誓](../Page/1968年夏季奥林匹克运动会.md "wikilink")。2012年倫敦奥運會增加教練員宣誓。而每屆開幕式中的運動員進場會以該國家語言的字母（或字體筆畫）開首按順序進場。如[2012年奧運](../Page/2012年夏季奥林匹克运动会.md "wikilink")（主辦國是[英國](../Page/英國.md "wikilink")），就會以英文字母順序進場。再如[2008年奧運](../Page/2008年夏季奥林匹克运动会.md "wikilink")（主辦國是[中國](../Page/中華人民共和國.md "wikilink")），就會以該國家的簡體字筆畫順序進場。雖然出場次序每屆不同，但根據傳統，希臘會是第一個進場（因爲希臘是奧運發源地），最後進場的是該屆主辦國國家。但如[2004年奧運](../Page/2004年夏季奥林匹克运动会.md "wikilink")（主辦國是[希臘](../Page/希臘.md "wikilink")），則會是希臘國旗先進場，希臘就會最後進場。當讀出出場國家的名字時都是先讀[法文](../Page/法文.md "wikilink")（因為「現代奧林匹克之父」[顧拜旦是法國人](../Page/皮埃爾·德·顧拜旦.md "wikilink")），然後讀[英文](../Page/英文.md "wikilink")。（如果主辦國家的官方語言並非是以上兩種，將會是緊隨英文以該國家的官方語言最後讀出）開幕式壓軸的點燃火炬儀式也是屆屆新穎，其中[1992年奧運由一名殘奧射箭運動員用弓箭把聖火點燃](../Page/1992年夏季奥林匹克运动会.md "wikilink")。而在1992年之前點燃聖火的同時都會把[白鴿放生](../Page/白鴿.md "wikilink")，寓意和平。但在1988年點燃聖火時有不少白鴿被燒死，因此在1992年開始不會放白鴿。

## 花費

自[1964年奧運以後耗资日益巨大](../Page/1964年夏季奥林匹克运动会.md "wikilink")，使得许多国家都不敢申請。美国举办的[1984年奧運大胆革新](../Page/1984年夏季奥林匹克运动会.md "wikilink")，引入商业模式，最后获得2.25亿美元的盈利，首开奥运会获利的先河。

為了解決這個花費過高的問題，自2001年6月19日起需同時接手[帕拉林匹克運動會](../Page/帕拉林匹克運動會.md "wikilink")，通常也要能夠之前或之後辦洲際賽事或其他國際賽事。例如[索契賽後會舉行](../Page/索契.md "wikilink")[2017年洲際國家盃](../Page/2017年洲際國家盃.md "wikilink")、[2018年國際足協世界盃等](../Page/2018年國際足協世界盃.md "wikilink")。

## 特殊

  - 以下獎牌奧運閉幕式才會頒發，非比賽完畢就頒發： 夏季奧運：男子馬拉松。冬季奧運：女子越野滑雪30公里自由式、男子越野滑雪50公里自由式。
  - 奧運會自1981年起，各項比賽「前8名」者會獲得一份「參賽證書」
  - 部分項目會規定預賽到決賽最多只能有2到3名同一個NOC代表進入最終決賽。例如：部分比賽取前8參加決賽，如果規定同代表團只能2個進入決賽那麼同代表團第三個以後且在前8以內的需要讓出決賽資格給第9名以後非同代表團且未滿額的參賽者。舉例體操2個、馬術3個。
  - 各項目會規定同一個國家最多只能有1到4個席位參加比賽。
  - 接受信用檢查（藥檢、年齡等）
      - 1968年之前沒有任何措施
      - 以往只有獎牌得主
      - 在21世紀以後，禁藥等問題嚴重，該項比賽前8到10名都要，必要時可要求所有人都做。

## 青年奧林匹克運動會

2007年[危地馬拉時間](../Page/危地馬拉.md "wikilink")7月5日，國際奧委會在第119屆全會中決定了創立[青年奧林匹克運動會](../Page/青年奧林匹克運動會.md "wikilink")，運動員的年齡需為14至18歲，首屆舉行的時間為2010年8月間，由[新加坡主辦](../Page/新加坡.md "wikilink")\[1\]\[2\]，2014年由[中國](../Page/中國.md "wikilink")[南京主辦](../Page/南京.md "wikilink")。

## 舉辦城市

奧運會的舉辦城市所屬的國家也就是本屆奧運會的主辦國。自從1984年[洛杉磯奧運以來](../Page/1984年夏季奧林匹克運動會.md "wikilink")，由於證明了奧運也能盈利，為該城市帶來相當大的利益，每屆奧運會舉辦城市的競爭日趨激烈，但舉辦城市則只能有一個。根據奧委會章程規定，奧運會屬國際奧委會的專利，所以奧委會根據申請舉辦城市的能力和財力，由全體委員投票決定下一屆奧運會的主辦城市。舉辦城市必須遵守奧委會的指示，例如興建[標準的](../Page/標準.md "wikilink")[運動場](../Page/運動場.md "wikilink")[地](../Page/地.md "wikilink")，為運動員提供[奧林匹克運動會村](../Page/奧運村.md "wikilink")。

一般來說，國際奧委會主席和主辦[國家的](../Page/國家.md "wikilink")[元首都會出席開幕禮和閉幕禮](../Page/元首.md "wikilink")，並在會上致詞，並由主辦元首宣佈大會開幕與閉幕。在以往，奧運會參賽國家由主辦國發出邀請，現在根據1986年國際奧委會漢城會議決定，由國際奧委會邀請。

## 比赛项目

[Diver2.jpg](https://zh.wikipedia.org/wiki/File:Diver2.jpg "fig:Diver2.jpg")上的高台跳水比赛\]\]

根据国际奥委会的规定，凡是被[国际奥委会承认的国家奥委会都可以派运动队参加比赛](../Page/国际奥委会.md "wikilink")。奥林匹克运动会的比赛项目有很多，因此每个项目的参加奥运会比赛的资格都有一定限制。

奥运会的比赛项目不仅有古老的運動——[體操](../Page/體操.md "wikilink")、[田径等](../Page/田径.md "wikilink")，也有许多新的项目不断加入进来。最新的競賽像是[沙灘排球](../Page/沙灘排球.md "wikilink")，甚至連亞洲的[跆拳道和](../Page/跆拳道.md "wikilink")[柔道都在賽程之內](../Page/柔道.md "wikilink")，但相關裁判爭議的比賽仍在討論是否適合放入競賽中。

根据最新规定，每屆奥运会的比赛大项不可多於28個(但可能會逐漸鬆綁)，而入选为正式的比赛项目，男子運動項目必須以一般的運動項目出現在至少75個國家、以及4大洲；女子運動項目則必須至少出現在40個國家、以及3大洲中，這些項目通常也非當時主辦國獨強項目。

此外，国际奥委会还授权给举办国，允许将本国开展较为普及的1～3个非奥运会项目列为本届奥运会的表演项目，其他国家亦可派队参加。作为非官方赛事，获胜者還是會得到奖牌，值得注意的是，雖然大部分的項目皆視奧運為最高榮耀，但仍有部分球類運動並非如此(例：世界盃足球賽)。

下面是目前夏季奥林匹克运动会的正式比赛项目：

<table>
<thead>
<tr class="header">
<th><p><strong>项目</strong></p></th>
<th><p><strong>时间</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/奥林匹克运动会射箭比赛.md" title="wikilink">射箭</a></p></td>
<td><p>1900年–1908年，1920年，1972年–</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/奥林匹克运动会田径比赛.md" title="wikilink">田径</a></p></td>
<td><p>1896年–</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/奥林匹克运动会壘球比赛.md" title="wikilink">壘球</a></p></td>
<td><p>1996年–2008年，2020年–</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/奥林匹克运动会棒球比赛.md" title="wikilink">棒球</a></p></td>
<td><p>1992年–2008年，2020年–<br />
（1984年–1988年為<a href="../Page/表演項目.md" title="wikilink">表演項目</a>）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/奥林匹克运动会羽毛球比赛.md" title="wikilink">羽毛球</a></p></td>
<td><p>1992年–<br />
（1988年為<a href="../Page/表演項目.md" title="wikilink">表演項目</a>）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/奥林匹克运动会篮球比赛.md" title="wikilink">篮球</a></p></td>
<td><p>1936年–</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/夏季奥林匹克运动会沙滩排球比赛.md" title="wikilink">沙滩排球</a></p></td>
<td><p>1996年–</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/奥林匹克运动会拳击比赛.md" title="wikilink">拳击</a></p></td>
<td><p>1904年–1908年，1920年–</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/夏季奥林匹克运动会皮划艇比赛.md" title="wikilink">皮划艇</a></p></td>
<td><p>1936年–<br />
（1924年為<a href="../Page/表演項目.md" title="wikilink">表演項目</a>）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/夏季奥林匹克运动会自行车比赛.md" title="wikilink">自行车</a></p></td>
<td><p>1896年–</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/奥林匹克运动会跳水比赛.md" title="wikilink">跳水</a></p></td>
<td><p>1904年–</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/夏季奥林匹克运动会马术比赛.md" title="wikilink">马术</a></p></td>
<td><p>1900年，1912年–</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/夏季奥林匹克运动会击剑比赛.md" title="wikilink">击剑</a></p></td>
<td><p>1896年–</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/奥林匹克运动会足球比赛.md" title="wikilink">足球</a></p></td>
<td><p>1900年–1928年，1936年–</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/奥林匹克运动会体操比赛.md" title="wikilink">体操</a></p></td>
<td><p>1896年–</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/奥林匹克运动会手球比赛.md" title="wikilink">手球</a></p></td>
<td><p>1936年，1972年–</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/奥林匹克运动会曲棍球比赛.md" title="wikilink">曲棍球</a></p></td>
<td><p>1908年，1920年–</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/奥林匹克运动会柔道比赛.md" title="wikilink">柔道</a></p></td>
<td><p>1964年，1972年–</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/奥林匹克运动会现代五项比赛.md" title="wikilink">现代五项</a></p></td>
<td><p>1912年–</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/奥林匹克运动会体操比赛.md" title="wikilink">艺术体操</a></p></td>
<td><p>1984年–</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/夏季奥林匹克运动会赛艇比赛.md" title="wikilink">赛艇</a></p></td>
<td><p>1900年–</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/夏季奥林匹克运动会帆船比赛.md" title="wikilink">帆船</a></p></td>
<td><p>1900年，1908年–</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/夏季奥林匹克运动会射击比赛.md" title="wikilink">射击</a></p></td>
<td><p>1896年–1924年，1932年–</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/奥林匹克运动会游泳比赛.md" title="wikilink">游泳</a></p></td>
<td><p>1896年–</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/夏季奥林匹克运动会花样游泳比赛.md" title="wikilink">花样游泳</a></p></td>
<td><p>1984年–</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/夏季奥林匹克运动会乒乓球比赛.md" title="wikilink">乒乓球</a></p></td>
<td><p>1988年–</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/奥林匹克运动会跆拳道比赛.md" title="wikilink">跆拳道</a></p></td>
<td><p>2000年–<br />
（1988年–1992年為<a href="../Page/表演項目.md" title="wikilink">表演項目</a>）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/夏季奥林匹克运动会网球比赛.md" title="wikilink">网球</a></p></td>
<td><p>1896年–1924年，1988年–<br />
（1968年、1984年為<a href="../Page/表演項目.md" title="wikilink">表演項目</a>）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/奥林匹克运动会体操比赛.md" title="wikilink">彈簧床</a></p></td>
<td><p>2000年–</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/夏季奥林匹克运动会铁人三项比赛.md" title="wikilink">铁人三项</a></p></td>
<td><p>2000年–</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/奥林匹克运动会排球比赛.md" title="wikilink">排球</a></p></td>
<td><p>1964年–</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/奥林匹克运动会水球比赛.md" title="wikilink">水球</a></p></td>
<td><p>1900年–</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/奥林匹克运动会举重比赛.md" title="wikilink">举重</a></p></td>
<td><p>1896年，1904年，1920年–</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/奥林匹克运动会摔跤比赛.md" title="wikilink">摔跤</a></p></td>
<td><p>1896年，1904年–</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/奥林匹克运动会高爾夫球比赛.md" title="wikilink">高爾夫</a></p></td>
<td><p>1900年–1904年，2016年-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/夏季奧林匹克運動會七人制橄欖球比賽.md" title="wikilink">七人制橄欖球</a></p></td>
<td><p>2016年-</p></td>
</tr>
</tbody>
</table>

## 比赛成绩和奖励

奥林匹克的精神是重在参与，只强调运动员之间和运动队之间的竞争，并不认为这是国家之间体育实力的较量。因此国际奥委会只公布比赛成绩，并不进行各国获奖情况的统计。但是东道国、新闻媒体以及各国的奥委会都进行这方面的统计，并按奖牌或分数排列总的名次。国际奥委会认为，精神奖励是奥林匹克精神的重要因素之一。因此在奖励方面，也着重在精神上和名誉上。在奥运会上获胜的前三名选手，只分别授予金、银、铜质奖章，并不发给任何獎金。但是各国对于获得前三名的运动员都会给予很丰厚的物质奖励，商界也会进行慷慨的赞助。讓许多原本默默无闻的运动员，从一夜之间变成了家喻户晓的明星。而每屆奧運獎牌榜的排名次序每個洲份也有不同的標準，除北美洲按總獎牌數來決定排名外，其餘洲份都是以金牌總數來決定排名。

### 奧林匹克運動會獎牌統計

以下是歷屆夏季奧林匹克運動會獎牌榜排名：

<table style="width:208%;">
<colgroup>
<col style="width: 18%" />
<col style="width: 14%" />
<col style="width: 60%" />
<col style="width: 14%" />
<col style="width: 60%" />
<col style="width: 42%" />
</colgroup>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>首名</p></th>
<th><p><strong><a href="../Page/金牌.md" title="wikilink">金牌</a></strong> <a href="https://zh.wikipedia.org/wiki/File:Med_1.png" title="fig:金牌">金牌</a></p></th>
<th><p>次名</p></th>
<th><p><strong><a href="../Page/金牌.md" title="wikilink">金牌</a></strong> <a href="https://zh.wikipedia.org/wiki/File:Med_1.png" title="fig:金牌">金牌</a></p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/1896年夏季奥林匹克运动会奖牌榜.md" title="wikilink">1896年夏季奥运会</a></p></td>
<td></td>
<td><p>11</p></td>
<td></td>
<td><p>10</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1900年夏季奥林匹克运动会奖牌榜.md" title="wikilink">1900年夏季奥运会</a></p></td>
<td></td>
<td><p>26</p></td>
<td></td>
<td><p>19</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1904年夏季奥林匹克运动会奖牌榜.md" title="wikilink">1904年夏季奥运会</a></p></td>
<td></td>
<td><p><strong>78</strong></p></td>
<td></td>
<td><p>4</p></td>
<td><p>首次由<a href="../Page/北美洲.md" title="wikilink">北美洲舉辦夏季奥林匹克运动会</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1908年夏季奥林匹克运动会奖牌榜.md" title="wikilink">1908年夏季奥运会</a></p></td>
<td></td>
<td><p><strong>56</strong></p></td>
<td></td>
<td><p>23</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1912年夏季奥林匹克运动会奖牌榜.md" title="wikilink">1912年夏季奥运会</a></p></td>
<td></td>
<td><p>25</p></td>
<td></td>
<td><p>24</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1916年夏季奥林匹克运动会.md" title="wikilink">1916年夏季奥运会</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>因<a href="../Page/第一次世界大戰.md" title="wikilink">第一次世界大戰而停辦</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1920年夏季奥林匹克运动会奖牌榜.md" title="wikilink">1920年夏季奥运会</a></p></td>
<td></td>
<td><p>41</p></td>
<td></td>
<td><p>19</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1924年夏季奥林匹克运动会奖牌榜.md" title="wikilink">1924年夏季奥运会</a></p></td>
<td></td>
<td><p>45</p></td>
<td></td>
<td><p>14</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1928年夏季奥林匹克运动会奖牌榜.md" title="wikilink">1928年夏季奥运会</a></p></td>
<td></td>
<td><p>22</p></td>
<td></td>
<td><p>10</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1932年夏季奥林匹克运动会奖牌榜.md" title="wikilink">1932年夏季奥运会</a></p></td>
<td></td>
<td><p>41</p></td>
<td></td>
<td><p>12</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1936年夏季奥林匹克运动会奖牌榜.md" title="wikilink">1936年夏季奥运会</a></p></td>
<td></td>
<td><p>34</p></td>
<td></td>
<td><p>24</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1940年夏季奥林匹克运动会.md" title="wikilink">1940年夏季奥运会</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>因<a href="../Page/第二次世界大戰.md" title="wikilink">第二次世界大戰而停辦</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1944年夏季奥林匹克运动会.md" title="wikilink">1944年夏季奥运会</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1948年夏季奥林匹克运动会奖牌榜.md" title="wikilink">1948年夏季奥运会</a></p></td>
<td></td>
<td><p>38</p></td>
<td></td>
<td><p>16</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1952年夏季奥林匹克运动会奖牌榜.md" title="wikilink">1952年夏季奥运会</a></p></td>
<td></td>
<td><p>40</p></td>
<td></td>
<td><p>22</p></td>
<td><p>蘇聯,香港首次參賽，兩岸政府原本亦計劃派運動員参赛,但其後都退出，中華人民共和國部分參加</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1956年夏季奥林匹克运动会奖牌榜.md" title="wikilink">1956年夏季奥运会</a></p></td>
<td></td>
<td><p>37</p></td>
<td></td>
<td><p>32</p></td>
<td><p>首次由<a href="../Page/大洋洲.md" title="wikilink">大洋洲舉辦夏季奥林匹克运动会</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1960年夏季奥林匹克运动会奖牌榜.md" title="wikilink">1960年夏季奥运会</a></p></td>
<td></td>
<td><p>43</p></td>
<td></td>
<td><p>34</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1964年夏季奥林匹克运动会奖牌榜.md" title="wikilink">1964年夏季奥运会</a></p></td>
<td></td>
<td><p>36</p></td>
<td></td>
<td><p>30</p></td>
<td><p>首次由<a href="../Page/亞洲.md" title="wikilink">亞洲城市舉辦夏季奥林匹克运动会</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1968年夏季奥林匹克运动会奖牌榜.md" title="wikilink">1968年夏季奥运会</a></p></td>
<td></td>
<td><p>45</p></td>
<td></td>
<td><p>29</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1972年夏季奥林匹克运动会奖牌榜.md" title="wikilink">1972年夏季奥运会</a></p></td>
<td></td>
<td><p><strong>50</strong></p></td>
<td></td>
<td><p>33</p></td>
<td><p><a href="../Page/慕尼黑慘案.md" title="wikilink">慕尼黑慘案發生</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1976年夏季奥林匹克运动会奖牌榜.md" title="wikilink">1976年夏季奥运会</a></p></td>
<td></td>
<td><p>49</p></td>
<td></td>
<td><p>40</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1980年夏季奥林匹克运动会奖牌榜.md" title="wikilink">1980年夏季奥运会</a></p></td>
<td></td>
<td><p><strong>80</strong></p></td>
<td></td>
<td><p>47</p></td>
<td><p>美國等64個國家因<a href="../Page/蘇聯入侵阿富汗.md" title="wikilink">蘇聯入侵阿富汗抵制本屆奧運</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1984年夏季奥林匹克运动会奖牌榜.md" title="wikilink">1984年夏季奥运会</a></p></td>
<td></td>
<td><p><strong>83</strong></p></td>
<td></td>
<td><p>20</p></td>
<td><p>蘇聯等16個國家抵制美國，<a href="../Page/中华人民共和国.md" title="wikilink">中華人民共和国夺得首枚</a><a href="../Page/金牌.md" title="wikilink">金牌</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1988年夏季奥林匹克运动会奖牌榜.md" title="wikilink">1988年夏季奥运会</a></p></td>
<td></td>
<td><p><strong>55</strong></p></td>
<td></td>
<td><p>37</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1992年夏季奥林匹克运动会奖牌榜.md" title="wikilink">1992年夏季奥运会</a></p></td>
<td></td>
<td><p>45</p></td>
<td></td>
<td><p>37</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1996年夏季奥林匹克运动会奖牌榜.md" title="wikilink">1996年夏季奥运会</a></p></td>
<td></td>
<td><p>44</p></td>
<td></td>
<td><p>26</p></td>
<td><p><a href="../Page/香港.md" title="wikilink">香港奪得首枚</a><a href="../Page/金牌.md" title="wikilink">金牌</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2000年夏季奥林匹克运动会奖牌榜.md" title="wikilink">2000年夏季奥运会</a></p></td>
<td></td>
<td><p>40</p></td>
<td></td>
<td><p>32</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2004年夏季奥林匹克运动会奖牌榜.md" title="wikilink">2004年夏季奥运会</a></p></td>
<td></td>
<td><p>36</p></td>
<td></td>
<td><p>32</p></td>
<td><p><a href="../Page/中華台北.md" title="wikilink">中華台北</a><sup>1</sup>奪得首枚金牌，[3]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2008年夏季奧林匹克運動會獎牌榜.md" title="wikilink">2008年夏季奧運會</a></p></td>
<td></td>
<td><p><strong>48</strong></p></td>
<td></td>
<td><p>36</p></td>
<td><p>首次由中国举办的夏季奥林匹克运动会</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2012年夏季奧林匹克運動會獎牌榜.md" title="wikilink">2012年夏季奧運會</a></p></td>
<td></td>
<td><p>46</p></td>
<td></td>
<td><p>38</p></td>
<td><p>首次每一個國家都有女子選手參賽</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2016年夏季奧林匹克運動會獎牌榜.md" title="wikilink">2016年夏季奧運會</a></p></td>
<td></td>
<td><p>46</p></td>
<td></td>
<td><p>27</p></td>
<td><p>首次由<a href="../Page/南美洲.md" title="wikilink">南美洲舉辦夏季奧林匹克運動會</a>。俄羅斯的禁藥醜聞、<a href="../Page/寨卡病毒.md" title="wikilink">寨卡病毒</a>、場地污染严重问题是該屆一大問題。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2020年夏季奧林匹克運動會獎牌榜.md" title="wikilink">2020年夏季奧運會</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>預定將由<a href="../Page/日本.md" title="wikilink">日本</a><a href="../Page/東京.md" title="wikilink">東京舉辦夏季奧林匹克運動會</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2024年夏季奧林匹克運動會獎牌榜.md" title="wikilink">2024年夏季奧運會</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>預定將由<a href="../Page/法國.md" title="wikilink">法國</a><a href="../Page/巴黎.md" title="wikilink">巴黎舉辦夏季奧林匹克運動會</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2028年夏季奧林匹克運動會獎牌榜.md" title="wikilink">2028年夏季奧運會</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>預定將由<a href="../Page/美國.md" title="wikilink">美國</a><a href="../Page/洛杉磯.md" title="wikilink">洛杉磯舉辦夏季奧林匹克運動會</a></p></td>
</tr>
</tbody>
</table>

以下是歷屆冬季奧林匹克運動會獎牌榜排名：

<table style="width:208%;">
<colgroup>
<col style="width: 18%" />
<col style="width: 14%" />
<col style="width: 60%" />
<col style="width: 14%" />
<col style="width: 60%" />
<col style="width: 42%" />
</colgroup>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>首名</p></th>
<th><p><strong><a href="../Page/金牌.md" title="wikilink">金牌</a></strong> <a href="https://zh.wikipedia.org/wiki/File:Med_1.png" title="fig:金牌">金牌</a></p></th>
<th><p>次名</p></th>
<th><p><strong><a href="../Page/金牌.md" title="wikilink">金牌</a></strong> <a href="https://zh.wikipedia.org/wiki/File:Med_1.png" title="fig:金牌">金牌</a></p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/1924年冬季奥林匹克运动会奖牌榜.md" title="wikilink">1924年冬季奥运会</a></p></td>
<td></td>
<td><p>4</p></td>
<td></td>
<td><p>4</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1928年冬季奥林匹克运动会奖牌榜.md" title="wikilink">1928年冬季奥运会</a></p></td>
<td></td>
<td><p>6</p></td>
<td></td>
<td><p>2</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1932年冬季奥林匹克运动会奖牌榜.md" title="wikilink">1932年冬季奥运会</a></p></td>
<td></td>
<td><p>6</p></td>
<td></td>
<td><p>3</p></td>
<td><p>首次由<a href="../Page/北美洲.md" title="wikilink">北美洲舉辦冬季奥林匹克运动会</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1936年冬季奥林匹克运动会奖牌榜.md" title="wikilink">1936年冬季奥运会</a></p></td>
<td></td>
<td><p>7</p></td>
<td></td>
<td><p>3</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1940年冬季奥林匹克运动会.md" title="wikilink">1940年冬季奥运会</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>因<a href="../Page/第二次世界大戰.md" title="wikilink">第二次世界大戰而停辦</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1944年冬季奥林匹克运动会.md" title="wikilink">1944年冬季奥运会</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1948年冬季奥林匹克运动会奖牌榜.md" title="wikilink">1948年冬季奥运会</a></p></td>
<td><p><br />
</p></td>
<td><p>4</p></td>
<td></td>
<td><p>3</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1952年冬季奥林匹克运动会奖牌榜.md" title="wikilink">1952年冬季奥运会</a></p></td>
<td></td>
<td><p>7</p></td>
<td></td>
<td><p>4</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1956年冬季奥林匹克运动会奖牌榜.md" title="wikilink">1956年冬季奥运会</a></p></td>
<td></td>
<td><p>7</p></td>
<td></td>
<td><p>4</p></td>
<td><p>蘇聯首次參賽</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1960年冬季奥林匹克运动会奖牌榜.md" title="wikilink">1960年冬季奥运会</a></p></td>
<td></td>
<td><p>7</p></td>
<td></td>
<td><p>4</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1964年冬季奥林匹克运动会奖牌榜.md" title="wikilink">1964年冬季奥运会</a></p></td>
<td></td>
<td><p><strong>11</strong></p></td>
<td></td>
<td><p>4</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1968年冬季奥林匹克运动会奖牌榜.md" title="wikilink">1968年冬季奥运会</a></p></td>
<td></td>
<td><p>6</p></td>
<td></td>
<td><p>5</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1972年冬季奥林匹克运动会奖牌榜.md" title="wikilink">1972年冬季奥运会</a></p></td>
<td></td>
<td><p>8</p></td>
<td></td>
<td><p>4</p></td>
<td><p>首次由<a href="../Page/亞洲.md" title="wikilink">亞洲舉辦冬季奥林匹克运动会</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1976年冬季奥林匹克运动会奖牌榜.md" title="wikilink">1976年冬季奥运会</a></p></td>
<td></td>
<td><p><strong>13</strong></p></td>
<td></td>
<td><p>7</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1980年冬季奥林匹克运动会奖牌榜.md" title="wikilink">1980年冬季奥运会</a></p></td>
<td></td>
<td><p>10</p></td>
<td></td>
<td><p>9</p></td>
<td><p>中華人民共和國首次參賽</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1984年冬季奥林匹克运动会奖牌榜.md" title="wikilink">1984年冬季奥运会</a></p></td>
<td></td>
<td><p>9</p></td>
<td></td>
<td><p>6</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1988年冬季奥林匹克运动会奖牌榜.md" title="wikilink">1988年冬季奥运会</a></p></td>
<td></td>
<td><p><strong>11</strong></p></td>
<td></td>
<td><p>9</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1992年冬季奥林匹克运动会奖牌榜.md" title="wikilink">1992年冬季奥运会</a></p></td>
<td></td>
<td><p>10</p></td>
<td></td>
<td><p>9</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/1994年冬季奥林匹克运动会奖牌榜.md" title="wikilink">1994年冬季奥运会</a></p></td>
<td></td>
<td><p><strong>11</strong></p></td>
<td></td>
<td><p>10</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/1998年冬季奥林匹克运动会奖牌榜.md" title="wikilink">1998年冬季奥运会</a></p></td>
<td></td>
<td><p><strong>12</strong></p></td>
<td></td>
<td><p>10</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2002年冬季奥林匹克运动会奖牌榜.md" title="wikilink">2002年冬季奥运会</a></p></td>
<td></td>
<td><p><strong>13</strong></p></td>
<td></td>
<td><p>13</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2006年冬季奥林匹克运动会奖牌榜.md" title="wikilink">2006年冬季奥运会</a></p></td>
<td></td>
<td><p><strong>11</strong></p></td>
<td></td>
<td><p>9</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2010年冬季奥林匹克运动会奖牌榜.md" title="wikilink">2010年冬季奥运会</a></p></td>
<td></td>
<td><p><strong>14</strong></p></td>
<td></td>
<td><p>10</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2014年冬季奧林匹克運動會獎牌榜.md" title="wikilink">2014年冬季奧運會</a></p></td>
<td></td>
<td><p><strong>11</strong></p></td>
<td></td>
<td><p>11</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/2018年冬季奧林匹克運動會獎牌榜.md" title="wikilink">2018年冬季奧運會</a></p></td>
<td><p><br />
</p></td>
<td><p><strong>14</strong></p></td>
<td></td>
<td><p>11</p></td>
<td><p><small><a href="../Page/俄罗斯.md" title="wikilink">俄罗斯被禁止参加平昌冬奥会</a>，成为史上首个因为使用兴奋剂而被禁止派运动员参加奥运会的国家。<strong>莫斯科称此举是西方羞辱俄罗斯的阴谋</strong>。作为一种妥协，被国际奥委会认为“清白的”俄罗斯运动员可以“奥林匹克运动员”的名义参赛，俄罗斯的“制服、国旗和国歌”将不会出现在平昌冬奥会。</small>[4]</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/2022年冬季奧林匹克運動會獎牌榜.md" title="wikilink">2022年冬季奧運會</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>由<a href="../Page/中國.md" title="wikilink">中國</a><a href="../Page/北京市.md" title="wikilink">北京申辦成功</a></p></td>
</tr>
</tbody>
</table>

## 问题与争议事件

就現實面而言，歷史上的賽事沒有一項賽事可以100%沒有問題（例如：[2012年奧運所有名單上會員國到齊且每隊都有女子參賽](../Page/2012年奧運.md "wikilink")，但是卻被批評門票太貴、吉祥物太醜等），但是還是有些問題很嚴重。

### 主要事件

随着奥林匹克运动会的发展，[金錢和](../Page/金錢.md "wikilink")[禁藥逐渐成为了一种潜在的威胁](../Page/體育禁藥.md "wikilink")。许多人认为[商业化对于奥林匹克运动会的发展起着很大作用](../Page/商业化.md "wikilink")，但是也担心它由于金钱的影响而失去公正，使得奥运的比賽成為經濟實力的較量。奥运会从一开始也为了避免受到金钱的影响，而不职业化，强调业余的原则。另外，服用禁藥的问题也影响着奥运会。为了提高成绩，一些运动员服用禁藥来达到目的，這是不被允許且違背運動精神的。

2014年8月4日，BBC節目《[廣角鏡](../Page/广角镜_\(电视节目\).md "wikilink")》播出《聖火下的交易》（Buying
the
Games），揭發2012年奧運會候選城市賄賂國際奧委會委員、而國際奧委會委員受賄的內幕。\[5\]\[6\]播出後，國際奧委會主席[雅克·羅格](../Page/雅克·羅格.md "wikilink")（Jacques
Rogge）公開承諾，將針對該紀錄片指控的2012年奧運會申辦活動賄選事件，採取任何必要的措施調查。但是，奧運申辦的[公正性](../Page/公正.md "wikilink")，無疑已經蒙上了陰影。

近年來奧運的權威性備受挑戰，其中尤其以[足球](../Page/足球.md "wikilink")、[網球和](../Page/網球.md "wikilink")[棒球項目尤為嚴重](../Page/棒球.md "wikilink")，其中足球的[世界盃足球賽在全球整體的被關注和受歡迎程度明顯的超過了奧運會](../Page/世界盃足球賽.md "wikilink")，由於奧運會的綜合項目較雜，一些項目，如[舉重](../Page/舉重.md "wikilink")、[跳水等由於觀賞性不高並不受到大眾的普遍歡迎](../Page/跳水.md "wikilink")，而像[乒乓球](../Page/乒乓球.md "wikilink")、[體操](../Page/體操.md "wikilink")、[射擊等項目由於商業化不強](../Page/射擊.md "wikilink")，只有少數個體國家重視，並不普及，亦缺少大家耳熟能詳的明星運動員而不被廣泛關注，導致大多數的項目不能吸引公眾注意；反觀[田徑](../Page/田徑.md "wikilink")、[游泳等大型項目又因為項目以及獎牌數量設置太多](../Page/游泳.md "wikilink")，導致綜合金牌榜的權威性亦受到公正問題方面的挑戰。

近年，新兴国家的崛起，使得传统体育强国的公民和媒体对奥运会的负面指责增多，如2012年伦敦奥运会，中国[叶诗文打破世界纪录](../Page/叶诗文.md "wikilink")，成绩过于突出，导致了一些传统游泳强国媒体的围攻，质疑是否使用了兴奋剂，以致于国际泳联都出面澄清支持叶诗文。

2022年北京冬季奥运会的高山滑雪雪道和雪橇竞赛区被规划建设在北京[松山国家级自然保护区的核心区](../Page/松山国家级自然保护区.md "wikilink")。该地区包括了[丁香叶忍冬](../Page/丁香叶忍冬.md "wikilink")、[山西杓兰等多种珍贵的生物物种](../Page/山西杓兰.md "wikilink")，且其中许多无法迁地保护。中国的许多生物学专业人员和环保主义者声称，如果将此区域开发为奥运场馆，该地的珍惜物种和综合生态环境将受到毁灭性破坏。中国政府试图将这片区域移除出自然保护区的范畴，另选择一些缺乏珍贵物种资源的区域划定为保护区，\[7\]此外还在中国国内广泛地删除或限制传播所有要求严格遵守中国法律，保护松山国家级保护区的相关言论。所有这些举动都引发了部分媒体和中国的生物学相关人士的批评。\[8\]\[9\]\[10\]目前，国际奥委会尚未对这些指责作出任何解释。

而如果因為一些事件必須取消或改地點，國際奧委會可能需要在之後的同樣賽事主辦權直接給予當時受影響的主辦城市作為補償。

此外也發現[2002年冬季奧林匹克運動會有過行賄行為](../Page/2002年冬季奧林匹克運動會.md "wikilink")，至2015年的[FIFA](../Page/FIFA.md "wikilink")、[IAAF也相繼捲入行賄醜聞](../Page/IAAF.md "wikilink")，還有[比賽造假](../Page/比賽造假.md "wikilink")、禁藥醜聞等破壞比賽公平競爭問題，讓奧運的永續發展增加變數。

### 禁藥問題

從1968年開始，幾乎每屆[奧運都有和禁藥有關的事件](../Page/奧運.md "wikilink")（除1980年，但可能存疑，因為沒有全部都藥檢），被剝奪的獎牌（16面獎牌是之後獲得清白或申訴成功才得以保留）大多和禁藥有關，只有3面是比賽上不當行為（如：拒領獎牌，攻擊或辱罵評審、裁判、其他參賽者，使用帶有政治、[歧視物品](../Page/歧視.md "wikilink")）或身分欺詐（如：[謊報年齡或異裝參賽](../Page/年齡造假.md "wikilink")）而造成。

  - 至2018年，有52個國家或地區共查獲200起以上違規，一共拔掉117面項目次的獎牌，其中[俄羅斯就失去](../Page/俄羅斯.md "wikilink")41項獎牌。
  - 就項目而言，共20項目，前七嚴重的項目是[田徑](../Page/田徑.md "wikilink")、[舉重](../Page/舉重.md "wikilink")、[馬術](../Page/馬術.md "wikilink")、[越野滑雪](../Page/越野滑雪.md "wikilink")、[角力](../Page/角力.md "wikilink")、[單車](../Page/單車.md "wikilink")，其中馬術部分大部分是對馬匹的藥檢未過。
  - 就用藥種類而言，一共47種，其中16起違規是[類固醇](../Page/類固醇.md "wikilink")。其他違規或嫌疑如：
      - 拒絕或逃避藥檢、企圖竄改藥檢結果(共3起)
      - 捲入[BALCO醜聞](../Page/BALCO醜聞.md "wikilink")(3起)
      - 2012年更是起出16起案例是雖藥檢未使用禁藥，但是「[生物護照](../Page/生物護照.md "wikilink")」異常。

2016年也因為國際奧委會針對俄羅斯的[禁藥醜聞並沒有採納](../Page/體育禁藥.md "wikilink")[世界反禁藥組織的建議對俄羅斯其全體禁賽](../Page/世界反禁藥組織.md "wikilink")，而是交由各單項運動協會決定，引來不少爭議，但俄羅斯對國際奧委會的決定表示「雖然嚴格但是客觀」，最後從389人降為282人。

### 獎牌的變動

  - 奧運會從1912年以來至2016年12月8日褫奪或變動了120個項目的獎牌（1968年褫奪獎牌開始確定），一共41金36銀43銅，大部分原因是因為使用禁藥，其他如在賽事上的不當行為（2面）、欺騙比賽單位自身身分（1面）也會做出此行動。
  - 其中12面是團體賽團體獎牌（都有遞補）、4面是團體賽個人獎牌（團體成績保留）。
  - 被褫奪的獎牌大多會順位遞補，但個人賽獎牌9面（1金2銀6銅）無遞補，這些獎牌會送至國際奧委會保存。例如：1992年男子舉重82.5公斤級銅牌，因為其獲得者頒獎典禮途中蓄意丟棄獎牌後離去，未遞補是因為違規不是比賽中發生。
  - 最多的項目為舉重（42面），失去最多獎牌的代表團為俄羅斯（31面）
  - 褫奪的個人獎牌有16面恢復，一共6金4銀6銅，為申訴成功或之後平反的結果。
  - 多數情形下會在賽事當日後8到10年內追回（追溯期），超過這個時間者通常不追回，追回也是賽事當日後15年之內。

參見[遭剥夺奥运会奖牌列表](../Page/遭剥夺奥运会奖牌列表.md "wikilink")、各年[奧林匹克運動會獎牌榜](../Page/奧林匹克運動會獎牌榜.md "wikilink")。

### 年齡限制

由於為維護運動員的健康及其他因素考量，因此有些項目設定了年齡的限制（通常以當年的12月31日的年齡為準），但還是發生了以非真實年齡參賽的案例。

  - 體操：16歲以上
  - 田徑（十項運動、10000公尺、競走、女子投擲）：18歲以上
  - 田徑（馬拉松、50公里競走）：20歲以上
  - 田徑（其他項目）：16歲以上
  - 男子足球：23歲以下但允許3名超過23歲

## 最高榮譽的例外

奧運是體育運動界最高水準的比賽，奧運冠軍亦是多數項目的運動員一生追求的終極目標，並視之為最高榮譽；不過隨著體育運動職業化，一些體育項目的運動員並不視奧運冠軍為最高榮譽，但部分運動員仍然視奧運冠軍爲目標。

### 男子足球

奧運男子足球僅限23歲或以下的運動員參加（但可加入3名超齡球員）。這是[國際足協為了要保持](../Page/國際足協.md "wikilink")[世界盃足球賽的權威性](../Page/世界盃足球賽.md "wikilink")（從前奧運足球限制只容許業餘球員參加，但奧委會和國際足協在「業餘球員」的定義上有不同意見）。

即使[國際足協放寬年齡限制](../Page/國際足協.md "wikilink")，由於奧運會舉行的時間，正值歐洲各國足球聯賽球季開始的時段，再加上在奧運開始前約一個月歐洲球員剛剛完成[欧洲足球锦标赛](../Page/欧洲足球锦标赛.md "wikilink")，因此難以吸引更多職業足球員，放棄球會季初的比賽去參加奧運。以[2016年為例](../Page/2016年夏季奧林匹克運動會男子足球比賽.md "wikilink")，有份參與[2016年歐洲足球錦標賽的球員](../Page/2016年歐洲足球錦標賽.md "wikilink")（有部分的年齡符合參加奧運）當中沒有一位參與2016年奧運足球賽。

足球水準較高​​的歐洲，其國家隊在世界盃決賽週中佔有近一半席位，但在奧運足球各洲席位平均分佈下，不少歐洲足球強國沒有取得參賽資格。現在[歐洲足協乾脆用](../Page/歐洲足協.md "wikilink")[歐洲21歲以下青年足球錦標賽的前三名作為奧運男子足球參賽資格](../Page/歐洲21歲以下青年足球錦標賽.md "wikilink")，而歐洲各國都視奧運男子足球為鍛鍊青年球員的練習場地。反而[世界盃足球賽和奧運會同年舉行的](../Page/世界盃足球賽.md "wikilink")[欧洲足球锦标赛才是歐洲各國的目標](../Page/欧洲足球锦标赛.md "wikilink")，此舉亦令奧運會與作為“世界第一運動”的​​足球結合的極其尷尬。

  - [世界盃足球賽](../Page/世界盃足球賽.md "wikilink")（四年一度的國際賽）、[欧洲足球锦标赛](../Page/欧洲足球锦标赛.md "wikilink")（四年一度的國際賽）、[歐洲冠軍盃](../Page/欧洲足球联盟冠军联赛.md "wikilink")（一年一度的球會級賽事）無論在規模和吸引力都較奧運男子足球為大。

一般情況下，奧運男子足球的精彩程度被認為不如一個國家的國內頂級聯賽有質量，在這樣的環境下，奧運男子足球的去留問題一度成為話題。

### 网球

  - 国际網球运动有四大公開賽——[澳網](../Page/澳大利亚网球公开赛.md "wikilink")、[法網](../Page/法国网球公开赛.md "wikilink")、[-{zh-hans:温布尔登;zh-hk:溫布頓;zh-tw:溫布頓;}-](../Page/温布尔登网球锦标赛.md "wikilink")、[美網](../Page/美国网球公开赛.md "wikilink")，这四项赛事即人们通常说到的「大滿貫系列賽」；無論是從賽事的規模、歷史、獎金、積分还是影響力來說，[大滿貫系列賽都堪稱職業網壇最重要的賽事](../Page/网球大满贯.md "wikilink")。
  - 網球四大公開賽（澳網、法網、-{zh-hans:温布尔登;zh-hk:溫布頓;zh-tw:溫布頓;}-、美網）頭銜，對每個職業網球選手來說都是最終的夢想，被視為至高無上的榮耀。不過原先大滿貫再加上奧運冠軍的「金滿貫」，仍是許多網球選手努力的目標，因為「金滿貫」每4年才有一次，入賽機會更小。

### 棒球

棒球比賽項目已被118屆[國際奧委會](../Page/國際奧委會.md "wikilink")（[意大利](../Page/意大利.md "wikilink")[都灵](../Page/都灵.md "wikilink")）排除在[2012年倫敦奧運之外](../Page/2012年夏季奧林匹克運動會.md "wikilink")。

國際奧委會批評[美國職棒大聯盟](../Page/美國職棒大聯盟.md "wikilink")
(MLB)不容許球員參加奧運棒球比賽，加上例行賽和奧運會的舉行時段有衝突，讓奧運棒球比賽的可看性下降，最後導致棒球項目進入了奧運會的瘦身行列。

### 拳擊

職業拳擊選手不能參加奧運會拳擊項目。

### 摔跤

與拳擊一樣，職業摔跤選手不能參加奧運會摔跤項目。

### 籃球

與部分項目一樣，職業籃球運動員（尤其[NBA](../Page/NBA.md "wikilink")）初期不能參加奧運會籃球項目。不過於[巴塞隆拿奧運起放寬限制](../Page/1992年夏季奥林匹克运动会.md "wikilink")，允許職業球員參加，因此延伸之後美國的夢幻隊。

## 最多金牌的得主

历届夏季奥运会获金牌最多的运动员是[美國男性游泳运动员](../Page/美國.md "wikilink")[迈克尔·菲尔普斯](../Page/迈克尔·菲尔普斯.md "wikilink")，他合計获得了23枚金牌，另外共有4人各自都获得了9枚金牌：

<table>
<thead>
<tr class="header">
<th><p>運動員</p></th>
<th><p>國籍</p></th>
<th><p>運動</p></th>
<th><p>奧運會</p></th>
<th><p><a href="https://zh.wikipedia.org/wiki/File:Gold_medal_icon.svg" title="fig:File:Gold medal icon.svg"><a href="File:Gold">File:Gold</a> medal icon.svg</a> 金牌</p></th>
<th><p><a href="https://zh.wikipedia.org/wiki/File:Silver_medal_icon.svg" title="fig:File:Silver medal icon.svg"><a href="File:Silver">File:Silver</a> medal icon.svg</a> 銀牌</p></th>
<th><p><a href="https://zh.wikipedia.org/wiki/File:Bronze_medal_icon.svg" title="fig:File:Bronze medal icon.svg"><a href="File:Bronze">File:Bronze</a> medal icon.svg</a> 銅牌</p></th>
<th><p>總數</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/邁克爾·菲爾普斯.md" title="wikilink">邁克爾·菲爾普斯</a></p></td>
<td></td>
<td><p><a href="../Page/游泳.md" title="wikilink">游泳</a></p></td>
<td><p>2000～2016</p></td>
<td><p>23</p></td>
<td><p>3</p></td>
<td><p>2</p></td>
<td><p>28</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/拉里莎·拉特尼娜.md" title="wikilink">拉里莎·拉特尼娜</a></p></td>
<td></td>
<td><p><a href="../Page/体操.md" title="wikilink">体操</a></p></td>
<td><p>1956～1964</p></td>
<td><p>9</p></td>
<td><p>5</p></td>
<td><p>4</p></td>
<td><p>18</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/帕沃·努尔米.md" title="wikilink">帕沃·努尔米</a></p></td>
<td></td>
<td><p><a href="../Page/田径.md" title="wikilink">田径</a></p></td>
<td><p>1920～1928</p></td>
<td><p>9</p></td>
<td><p>3</p></td>
<td><p>0</p></td>
<td><p>12</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/马克·施皮茨.md" title="wikilink">马克·施皮茨</a></p></td>
<td></td>
<td><p><a href="../Page/游泳.md" title="wikilink">游泳</a></p></td>
<td><p>1968～1972</p></td>
<td><p>9</p></td>
<td><p>1</p></td>
<td><p>1</p></td>
<td><p>11</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/卡尔·刘易斯.md" title="wikilink">卡尔·刘易斯</a></p></td>
<td></td>
<td><p><a href="../Page/田徑.md" title="wikilink">田徑</a></p></td>
<td><p>1984～1996</p></td>
<td><p>9</p></td>
<td><p>1</p></td>
<td><p>0</p></td>
<td><p>10</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 相關

  - [奧林匹克運動會主辦城市列表](../Page/奧林匹克運動會主辦城市列表.md "wikilink")
  - [奧林匹克運動會獎牌](../Page/奧林匹克運動會獎牌.md "wikilink")
  - [国际奥林匹克委员会](../Page/国际奥林匹克委员会.md "wikilink")
  - [奥林匹克长跑日](../Page/奥林匹克长跑日.md "wikilink")（每年的6月23日舉辦）

## 参考文献

## 外部链接

  - [國際奧委會官方網站](https://www.olympic.org)
  - [奧林匹克博物館](https://www.olympic.org/museum)

## 另见

  - [世界大学生运动会](../Page/世界大学生运动会.md "wikilink")
  - [世界运动会](../Page/世界运动会.md "wikilink")
  - [世界智力运动会](../Page/世界智力运动会.md "wikilink")
  - [國際科學奧林匹克](../Page/國際科學奧林匹克.md "wikilink")
  - [奥运经济](../Page/奥运经济.md "wikilink")

{{-}}

[奧林匹克運動會](../Category/奧林匹克運動會.md "wikilink")
[Category:1896年建立的週期性體育事件](../Category/1896年建立的週期性體育事件.md "wikilink")

1.  [青少年奧運會正式創立
    首屆賽事2010年舉行](http://2008.sports.tom.com/2007-07-06/0BMJ/04409452.html)

2.
3.  <sup>1</sup>[中华民国因政治问题](../Page/中华民国.md "wikilink")，奥委会同意中华民国使用的名稱參加國際運動賽事，而國際組織，如[國際棒球總會及](../Page/國際棒球總會.md "wikilink")[國際大學運動總會也因政治因素稱呼中華民國為中華台北](../Page/國際大學運動總會.md "wikilink")（詳見[中華台北](../Page/中華台北.md "wikilink")），至今[兩岸對於主權仍有爭議](../Page/兩岸.md "wikilink")，請參見[海峽兩岸關係](../Page/海峽兩岸關係.md "wikilink")。
4.
5.
6.
7.
8.
9.
10.