[Slime_Mold_Olympic_National_Park_North_Fork_Sol_Duc.jpg](https://zh.wikipedia.org/wiki/File:Slime_Mold_Olympic_National_Park_North_Fork_Sol_Duc.jpg "fig:Slime_Mold_Olympic_National_Park_North_Fork_Sol_Duc.jpg")的黏菌。\]\]
[Slime_Mold_DP2.jpg](https://zh.wikipedia.org/wiki/File:Slime_Mold_DP2.jpg "fig:Slime_Mold_DP2.jpg")
**黏菌**是一種[原生生物](../Page/原生生物.md "wikilink")，分類學上的名稱為「**Myxomycota**」，意思是「真菌動物」，這樣的名稱表現了其外觀與生活型態。它們保有[變形蟲的身體構造](../Page/變形蟲.md "wikilink")，但是也與[真菌類同樣擁有能夠釋放](../Page/真菌類.md "wikilink")[孢子的](../Page/孢子.md "wikilink")[子實體](../Page/子實體.md "wikilink")，而這些特徵也使他們看起來和[黴菌相似](../Page/黴菌.md "wikilink")。現在的[系統分類學將其歸位在](../Page/系統分類學.md "wikilink")[植物與](../Page/植物.md "wikilink")[真菌之間](../Page/真菌.md "wikilink")，與其他原生生物在親源關係上有段距離，兩者之間由部分植物相隔。

黏菌分佈於世界各地，具有許多不同的分類群。其中較為著名兩大類是[原生質體黏菌與](../Page/原生質體黏菌.md "wikilink")[細胞性黏菌](../Page/細胞性黏菌.md "wikilink")。其中原生質體黏菌在分類上稱為[黏菌亞綱](../Page/黏菌亞綱.md "wikilink")（Myxogastria），也稱為「真黏菌」或「非細胞黏菌」。而細胞性黏菌則屬於[網柱黏菌亞綱](../Page/網柱黏菌亞綱.md "wikilink")（Dictyostelia）。兩者的主要差異在於生命週期與生理結構。

## 生活史

原生質體黏菌的生活史以[絨泡黏菌屬](../Page/絨泡黏菌屬.md "wikilink")（*Physarum*）為例；細胞性黏菌的生活史則以[網柱細胞黏菌屬](../Page/網柱細胞黏菌屬.md "wikilink")（*Dictyostelium*）為例。

### 原生質體黏菌

原生質體黏菌的特色是沒有單一細胞，而形成一整團的[原生質](../Page/原生質.md "wikilink")。其生活史可分為[二倍體時期與](../Page/二倍體.md "wikilink")[單倍體時期](../Page/單倍體.md "wikilink")。

二倍體時期從兩個單倍體細胞經由配子生殖形成合子開始，之後合子進行[有絲分裂之後](../Page/有絲分裂.md "wikilink")，會形成擁有許多細胞核，但是只有一團原生質的原生質團（[合胞體](../Page/合胞體.md "wikilink")），稱為變形體（plasmodium）。變形體發展成熟之後，會形成網狀型態，且依照食物、[水與](../Page/水.md "wikilink")[氧氣等所需養分改變其表面積](../Page/氧氣.md "wikilink")。此時也稱為營養時期（feeding
stage），[吞噬作用為其進食方式](../Page/吞噬作用.md "wikilink")。接下來形成孢子囊（sporangium），孢子囊發展成熟後發展成為子實體。之後進行[減數分裂](../Page/減數分裂.md "wikilink")，釋放出[單倍體孢子](../Page/單倍體.md "wikilink")。

接下來進入[單倍體時期](../Page/單倍體.md "wikilink")，釋放出來的孢子會經由空氣傳播，而且這些孢子會產生兩種配子，其中一種為變形蟲細胞（amoeboid
cell）；另一種則是鞭毛細胞（flagellated
cell）。這兩種細胞可以互相變換，但是最後都只會與同類細胞結合進行[配子生殖](../Page/配子生殖.md "wikilink")（syngamy），產生二倍體的合子。

### 細胞性黏菌

細胞性黏菌的生活史可分為[無性生殖與](../Page/無性生殖.md "wikilink")[有性生殖兩種週期](../Page/有性生殖.md "wikilink")，兩者之間可以互換。其中二倍體時期出現在有性生殖週期中。

剛離開孢子的黏菌細胞稱為單一細胞（solitary
cell），在單一細胞的階段為營養時期，此時細胞以吞噬細菌的方式生存。當食物耗盡時，許多原本分開生活的單一細胞會聚集在一起，形成一個變形蟲，長相類似蛞蝓，而且可以爬行移動。之後有些細胞進行配子生殖，形成二倍體配子。再經過[減數分裂形成新的單倍體變形蟲](../Page/減數分裂.md "wikilink")，重回無性生殖週期。有些細胞則會組成子實體，生產並釋放單倍體孢子。孢子外殼破裂放出單一細胞，完成一次生命週期。

## 黏菌的性別決定

黏菌的配子也叫做性細胞，其性別決定於3個[基因座](../Page/基因座.md "wikilink")（locus）上的基因，分別是matA、matB與matC。因此每一株黏菌皆可形成8種性別的性細胞。

此外這3個基因座上的基因皆有多種變化，目前已知matA與matB各有13種變型，而matC則擁有3種變型。由這些已知的變化，可以排列組合出500種以上的性別。

## 分類

全世界的黏菌大約有100個[屬與](../Page/屬.md "wikilink")500個[種](../Page/種.md "wikilink")。可分為3個或4個亞綱。

  - Mycetozoa（黏菌下门）
      - Protostelia（原柱黏菌綱）
          - Protostelida（目）'\[
      - Myxogastria（黏菌綱）
          - Liceida（目）
          - Echinosteliida（目）
          - Trichiida（目）
          - Stemonitida（目）
          - Physarida（目）
      - Dictyostelia（網柱黏菌綱）
          - Dictyosteliida（目）

## 參考書目

  - Neil A. Campbell & Jane B Reece. BIOLOGY 6th edition. ISBN
    0-8053-6624-5
  - Olivia Judson. 《Dr. Tatiana給全球生物的性忠告》（Dr. Tatiana's Sex Advice to
    All Creation） 潘勛譯 ISBN 986-7691-08-3

[Category:原生生物](../Category/原生生物.md "wikilink")