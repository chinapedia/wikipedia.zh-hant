border |office = 5th [美國常駐聯合國代表](../Page/美國常駐聯合國代表列表.md "wikilink")
|president = [約翰·肯尼迪](../Page/約翰·肯尼迪.md "wikilink")
[林登·B·約翰遜](../Page/林登·B·約翰遜.md "wikilink") |term_start = January 23,
1961 |term_end = July 14, 1965 |predecessor = 詹姆斯·J·華茲沃斯 |successor =
阿瑟·J·戈德堡 |order1 = 31st [伊利诺伊州州长](../Page/伊利诺伊州州长.md "wikilink")
|lieutenant1 = 舍伍德·迪克森 |term_start1 = January 10, 1949 |term_end1 =
January 12, 1953 |predecessor1 = [Dwight H.
Green](../Page/Dwight_H._Green.md "wikilink") |successor1 = 德懷特·H·格林
|birth_name = Adlai Ewing Stevenson II |birth_date =  |birth_place =
[洛杉磯](../Page/洛杉磯.md "wikilink") |death_date =  |death_place =
[倫敦](../Page/倫敦.md "wikilink") |restingplace = 常青公墓 |party =
[美国民主党](../Page/美国民主党.md "wikilink") |spouse =  |children = 3,
including [阿德萊·史蒂文森三世](../Page/阿德萊·史蒂文森三世.md "wikilink") |relatives =
劉易斯·史蒂文森  |education = [普林斯頓大學](../Page/普林斯頓大學.md "wikilink")
[西北大學](../Page/西北大學.md "wikilink")  |allegiance =  |branch =
|serviceyears = 1918 |rank =
[E2_SM_USN.png](https://zh.wikipedia.org/wiki/File:E2_SM_USN.png "fig:E2_SM_USN.png")
西曼學徒 }} **阿德萊·史蒂文森**（**Adlai Ewing Stevenson
II**，），[美國政治家](../Page/美國.md "wikilink")，以其辯論技巧聞名。

## 生平

曾於1952年和1956年两次代表[美国民主党參選](../Page/美国民主党.md "wikilink")[美國總統](../Page/美國總統.md "wikilink")，但皆敗選。後出任[美国常驻联合国代表](../Page/美国常驻联合国代表.md "wikilink")，在[古巴导弹危机中](../Page/古巴导弹危机.md "wikilink")，发挥了重要作用。

## 史蒂文森家庭

  - 祖父: [阿德萊·E·史蒂文森](../Page/阿德萊·E·史蒂文森.md "wikilink")
    ([美國副總統](../Page/美國副總統.md "wikilink"), 1835-1914)
  - 父: [劉易斯·史蒂文森](../Page/劉易斯·史蒂文森.md "wikilink") (伊利諾伊州國務卿, 1868-1929)
  - 子: [阿德萊·史蒂文森三世](../Page/阿德萊·史蒂文森三世.md "wikilink") (伊利諾伊州參議員, 1930-)
  - 孫: [阿德萊·史蒂文森四世](../Page/阿德萊·史蒂文森四世.md "wikilink") (1956-)

## 参考文献

## 外部链接

{{-}}

[Category:美國基督徒](../Category/美國基督徒.md "wikilink")
[Category:美國反共主義者](../Category/美國反共主義者.md "wikilink")
[Category:美國民主黨總統候選人](../Category/美國民主黨總統候選人.md "wikilink")
[Category:美国民主党州长](../Category/美国民主党州长.md "wikilink")
[Category:美国文理科学院院士](../Category/美国文理科学院院士.md "wikilink")
[Category:伊利諾州州長](../Category/伊利諾州州長.md "wikilink")
[Category:哈佛法學院校友](../Category/哈佛法學院校友.md "wikilink")
[Category:美国自由主义](../Category/美国自由主义.md "wikilink")
[Category:美国常驻联合国代表](../Category/美国常驻联合国代表.md "wikilink")
[Category:普林斯顿大学校友](../Category/普林斯顿大学校友.md "wikilink")
[Category:1956年美國總統選舉](../Category/1956年美國總統選舉.md "wikilink")
[Category:1952年美國總統選舉](../Category/1952年美國總統選舉.md "wikilink")
[Category:苏格兰－爱尔兰裔美国人](../Category/苏格兰－爱尔兰裔美国人.md "wikilink")