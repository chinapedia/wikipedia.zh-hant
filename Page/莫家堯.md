**莫家堯**（Mok Ka Yiu
Edward）（1969年5月24日生），[香港](../Page/香港.md "wikilink")[藝人](../Page/藝人.md "wikilink")，[畢業於](../Page/畢業.md "wikilink")[香港演藝學院](../Page/香港演藝學院.md "wikilink")。早年為[亞洲電視藝員](../Page/亞洲電視.md "wikilink")，後轉到[無線電視](../Page/無線電視.md "wikilink")。目前已離開TVB，轉職其他行業。\[1\]\[2\]

## 演出作品

### 電視劇（[亞洲電視](../Page/亞洲電視.md "wikilink")）

#### 1990年

  - [赤子雄風](../Page/赤子雄風.md "wikilink")
  - [中華英雄](../Page/中華英雄_\(1990年電視劇\).md "wikilink") 飾 赤蠟
  - [鬼做你老婆](../Page/鬼做你老婆.md "wikilink")

#### 1991年

  - [上海一九四九](../Page/上海一九四九.md "wikilink")
  - [觸電情緣](../Page/觸電情緣.md "wikilink")

#### 1992年

  - [點解阿sir係隻鬼](../Page/點解阿sir係隻鬼.md "wikilink")
  - [龍在江湖](../Page/龍在江湖_\(電視劇\).md "wikilink") 飾 李繼祖
  - [八婆會館](../Page/八婆會館.md "wikilink")

#### 1993年

  - [點解阿sir係隻鬼II](../Page/點解阿sir係隻鬼II.md "wikilink")
  - [馬場風雲](../Page/馬場風雲.md "wikilink") 飾 馬成功
  - [劍神不敗](../Page/劍神不敗.md "wikilink")（傲劍至尊） 飾 太子

### 電視劇（[無線電視](../Page/無線電視.md "wikilink")）

#### 1996年

  - [五個醒覺的少年](../Page/五個醒覺的少年.md "wikilink") 飾 彭大軍
  - [廉政行動1996](../Page/廉政行動1996.md "wikilink") 飾 張成東
  - [900重案追兇](../Page/900重案追兇.md "wikilink") 飾 徐敬業
  - [西遊記](../Page/西遊記_\(1996年電視劇\).md "wikilink") 飾 寶象國太子

#### 1997年

  - [刑事偵緝檔案III](../Page/刑事偵緝檔案III.md "wikilink") 飾 蔡志釗 (柏恩霖之前男友)
  - [醉打金枝](../Page/醉打金枝.md "wikilink") 飾 二皇子

#### 1998年

  - [離島特警](../Page/離島特警.md "wikilink") 飾 曾英偉
  - [陀槍師姐](../Page/陀槍師姐.md "wikilink") 飾 關sir
  - [妙手仁心](../Page/妙手仁心.md "wikilink") 飾 林志輝

#### 1999年

  - [刑事偵緝檔案IV](../Page/刑事偵緝檔案IV.md "wikilink") 飾
    [警察馮添海](../Page/警察.md "wikilink") (江子山之下屬)
  - [鑑證實錄II](../Page/鑑證實錄II.md "wikilink") 飾 謝守一 Richard
    (「唐詠心分屍案」：唐詠心男友)

#### 2000年

  - [倚天屠龍記](../Page/倚天屠龍記.md "wikilink") 飾
    [殷梨亭](../Page/殷梨亭.md "wikilink")
  - [廟街·媽·兄弟](../Page/廟街·媽·兄弟.md "wikilink") 飾

#### 2002年

  - [無考不成冤家](../Page/無考不成冤家.md "wikilink") 飾 談天鐸
  - [再生緣](../Page/再生緣.md "wikilink") 飾 孟子儒
  - [絕世好爸](../Page/絕世好爸.md "wikilink") 飾 龍Sir

#### 2003年

  - [衛斯理](../Page/衛斯理.md "wikilink") 飾 猛哥
  - [衝上雲霄](../Page/衝上雲霄.md "wikilink") 飾 阿才（第19-20集）（客串）

#### 2004年

  - [烽火奇遇結良緣](../Page/烽火奇遇結良緣.md "wikilink") 飾 樊　威
  - [廉政行動2004](../Page/廉政行動2004.md "wikilink") 飾 戚家明（以權謀私篇）
  - [陀槍師姐IV](../Page/陀槍師姐IV.md "wikilink") 飾 艇仔
  - [下一站彩虹](../Page/下一站彩虹.md "wikilink") 飾 關碩倫
  - [無名天使3D](../Page/無名天使3D.md "wikilink") 飾 Herman
  - [惊艳一枪](../Page/惊艳一枪.md "wikilink") 飾 冷血

#### 2005年

  - [心花放](../Page/心花放.md "wikilink") 飾 馬永田
  - [酒店風雲](../Page/酒店風雲.md "wikilink") 飾 王啟志
  - [心慌·心郁·逐個捉](../Page/心慌·心郁·逐個捉.md "wikilink") 飾
    程友仁（在2006年於[翡翠台播映](../Page/翡翠台.md "wikilink")）
  - [秀才遇著兵](../Page/秀才遇著兵.md "wikilink") 飾 馬立光
  - [心理心裏有個謎](../Page/心理心裏有個謎.md "wikilink") 飾 Don
  - [同撈同煲](../Page/同撈同煲.md "wikilink") 飾 余旦明

### 節目主持（[亞洲電視](../Page/亞洲電視.md "wikilink")）

#### 1992年

  - [有腦新時代](../Page/有腦新時代.md "wikilink")（與[雪梨](../Page/雪梨.md "wikilink")、[張錦程合作](../Page/張錦程.md "wikilink")）

#### 1994年

  - [今日睇真D](../Page/今日睇真D.md "wikilink")（與[林建明](../Page/林建明.md "wikilink")、[歐錦棠](../Page/歐錦棠.md "wikilink")、[林祖輝](../Page/林祖輝.md "wikilink")、[盧希萊](../Page/盧希萊.md "wikilink")、[劉錦玲等合作](../Page/劉錦玲.md "wikilink")）

## 參考資料

<div class="references-small">

<references />

</div>

[ka](../Category/莫姓.md "wikilink")
[Category:前無綫電視男藝員](../Category/前無綫電視男藝員.md "wikilink")
[Category:前亞洲電視藝員](../Category/前亞洲電視藝員.md "wikilink")
[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[Category:玫瑰崗學校校友](../Category/玫瑰崗學校校友.md "wikilink")
[Category:香港演藝學院校友](../Category/香港演藝學院校友.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")

1.  [黃宇詩 Blog](http://blog.tvb.com/ursulewong/2008/09/09/?show=slide)
2.  [1](https://hk.news.appledaily.com/local/daily/article/20150324/19087616)