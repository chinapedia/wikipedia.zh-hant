《**-{zh-hans:收割;zh-hk:魔疫;zh-tw:報應;}-**》（），以[宗教為主題的](../Page/宗教.md "wikilink")[驚悚片](../Page/驚悚片.md "wikilink")，題材根據《[聖經](../Page/聖經.md "wikilink")》《[出埃及記](../Page/出埃及記.md "wikilink")》中記載的「[十災](../Page/十災.md "wikilink")」。

電影由[喬·西佛](../Page/喬·西佛.md "wikilink")、[罗伯特·泽米基斯及](../Page/罗伯特·泽米基斯.md "wikilink")[蘇珊·道尼監製](../Page/蘇珊·道尼.md "wikilink")，[凯莉·海耶斯及](../Page/凯莉·海耶斯.md "wikilink")[乍得·海耶斯編劇](../Page/乍得·海耶斯.md "wikilink")，[斯蒂芬·霍普金斯導演](../Page/斯蒂芬·霍普金斯.md "wikilink")，[希拉里·斯旺克主演](../Page/希拉里·斯旺克.md "wikilink")。電影最先於2007年4月4日在[台灣上映](../Page/台灣.md "wikilink")，接著於翌日4月5日在[美國及](../Page/美國.md "wikilink")[香港上映](../Page/香港.md "wikilink")。

## 劇情

嘉芙蓮是一個自小信奉神的[天主教徒](../Page/天主教徒.md "wikilink")，當她在[非洲](../Page/非洲.md "wikilink")[蘇丹傳教時](../Page/蘇丹.md "wikilink")，丈夫及女兒因宗教原因被當為祭品遭殺害，令她開始對信仰產生疑惑，漸漸更成為拆解宗教迷信現象的專家，在[-{zh-hans:路易斯安那州立大学;zh-hk:路易斯安那州立大學;zh-tw:路易西安納州立大學;}-當](../Page/路易斯安那州立大学.md "wikilink")[教授](../Page/教授.md "wikilink")。

當[-{zh-hans:路易斯安那州;zh-hk:路易斯安那州;zh-tw:路易西安納州;}-一個小鎮的教師道格向她尋求幫助](../Page/路易斯安那州.md "wikilink")，調查連串與《[聖經](../Page/聖經.md "wikilink")》《[出埃及記](../Page/出埃及記.md "wikilink")》記載的[十災如出一轍的疫災時](../Page/十災.md "wikilink")，她發現不能用任何科學的理論去解釋，小鎮人心惶惶，眾人都將罪魁禍首推到一個神祕小女孩蘿倫身上。

所有人說她是[魔鬼的化身](../Page/魔鬼.md "wikilink")，只有嘉芙蓮認為她清白，需要出手打救，她越接近神祕事件真相，越發現自己陷於末世恐怖陰謀的漩渦。她必須尋回失去的信仰與勇氣，去與終極的邪惡對決。

## 角色

| 演員                                         | 角色               |
| ------------------------------------------ | ---------------- |
| [希拉里·斯旺克](../Page/希拉里·斯旺克.md "wikilink")   | Katherine Winter |
| [大衛·莫利塞](../Page/大衛·莫利塞.md "wikilink")     | Doug             |
| [艾德里斯·艾爾巴](../Page/艾德里斯·艾爾巴.md "wikilink") | Ben              |
| [安娜蘇菲亞·羅伯](../Page/安娜蘇菲亞·羅伯.md "wikilink") | Loren McConnell  |
| [斯蒂芬·瑞](../Page/斯蒂芬·瑞.md "wikilink")       | Father Costigan  |
|                                            |                  |

## 拍攝

影片大部份在美國路易斯安那州內拍攝，包括首府[巴頓魯治](../Page/巴頓魯治.md "wikilink")、[-{zh-hans:新奥尔良;zh-hk:新奧爾良;zh-tw:紐奧良;}-](../Page/新奥尔良.md "wikilink")、[薛夫波特與](../Page/薛夫波特.md "wikilink")[法蘭西斯鎮](../Page/法蘭西斯鎮.md "wikilink")。此外的取景地點還有[德克薩斯州首府](../Page/德克薩斯州.md "wikilink")[奧斯汀](../Page/奧斯汀.md "wikilink")
(Austin, Texas)
和[波多黎各的](../Page/波多黎各.md "wikilink")[聖胡安](../Page/聖胡安.md "wikilink")
(San Juan, Puerto Rico)。

故事中發生災疫的小鎮就是現實中的法蘭西斯鎮，[颶風-{zh-hans:卡特里娜;zh-hk:卡特里娜;zh-tw:卡崔娜;}-於](../Page/颶風卡特里娜.md "wikilink")2005年8月26日突然來襲，令拍攝受阻一星期。當攝製隊在巴頓魯治拍攝時，[颶風麗塔於](../Page/颶風麗塔.md "wikilink")2005年9月24日襲擊，又令拍攝進度受阻一天，但幸好兩次颶風都未對巴頓魯治市中心造成嚴重損毀。

## 電影配樂

電影配樂原來由[菲利普·格拉斯編曲](../Page/菲利普·格拉斯.md "wikilink")，並一直進行至錄音階段，但是因為始終未能滿足監製需要，於是再另覓人選，結果最後由[約翰·費塞爾完成](../Page/約翰·費塞爾.md "wikilink")。

## 幕後花絮

電影最初原定於2006年8月11日在戲院上映，然後延至2006年11月8日，後來再次延期至2007年3月30日，又再一次延遲至2007年4月6日。結果最後於2007年4月5日上映（美國），因為當日是[基督受難日](../Page/基督受難日.md "wikilink")，所以決定於該天上映。

## 爭議

[智利城市Concepción的市長Jacqueline](../Page/智利.md "wikilink") Van
Rysselberghe，曾正式發出反對信：反對電影中對該城市的描寫，因為電影開首的片段其實屬於[古巴](../Page/古巴.md "wikilink")。

## 外部連結

  -
  -
  -
  -
  -
[Category:2007年電影](../Category/2007年電影.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:美國恐怖片](../Category/美國恐怖片.md "wikilink")
[Category:宗教恐怖片](../Category/宗教恐怖片.md "wikilink")