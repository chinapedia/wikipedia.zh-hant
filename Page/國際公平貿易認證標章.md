[Fairtradelogofinalversiona.jpg](https://zh.wikipedia.org/wiki/File:Fairtradelogofinalversiona.jpg "fig:Fairtradelogofinalversiona.jpg")
**國際公平貿易認證標章**是一個獨立的消費者標章，目前有23個國家使用。產品上印有這個標章代表[發展中國家的生產者在這件產品的貿易上得到較公平的待遇](../Page/發展中國家.md "wikilink")。標章由[國際公平貿易標籤組織](../Page/國際公平貿易標籤組織.md "wikilink")（FLO
International）代表二十個公平貿易倡議者所擁有與保護。

帶有這個標章的產品，必定來自於[公平貿易認證組織](../Page/公平貿易認證組織.md "wikilink")（FLO-CERT）所認證與督察的生產者。該產品的種植與收成都符合國際公平貿易標籤組織所規範的公平貿易認證標準。供應鏈同時也受到FLO-CERT的督察，以確保產品的完整性。只有被授權者才可以使用這個標章。

標章的圖樣所呈現的是一個歡呼的人---代表生產者透過公平貿易獲得了公平的交易，同時也代表消費者透過公平貿易商品，知道自己做一件與眾不同的好事。

2006年的今天，公平貿易標章總共發展出下列這些商品：[咖啡](../Page/咖啡.md "wikilink")、[茶葉](../Page/茶葉.md "wikilink")、[巧克力](../Page/巧克力.md "wikilink")、[可可](../Page/可可.md "wikilink")、[糖](../Page/糖.md "wikilink")、[香蕉](../Page/香蕉.md "wikilink")、[蘋果](../Page/蘋果.md "wikilink")、[西洋梨](../Page/西洋梨.md "wikilink")、[葡萄](../Page/葡萄.md "wikilink")、[李子](../Page/李子.md "wikilink")、[檸檬](../Page/檸檬.md "wikilink")、[橘子](../Page/橘子.md "wikilink")、[蜜橘](../Page/蜜橘.md "wikilink")、[小柑橘](../Page/小柑橘.md "wikilink")、[荔枝](../Page/荔枝.md "wikilink")、[酪梨](../Page/酪梨.md "wikilink")、[鳳梨](../Page/鳳梨.md "wikilink")、[芒果](../Page/芒果.md "wikilink")、[果汁](../Page/果汁.md "wikilink")、[藜麥](../Page/藜麥.md "wikilink")、[胡椒](../Page/胡椒.md "wikilink")、[青豆](../Page/青豆.md "wikilink")、[椰子](../Page/椰子.md "wikilink")、[乾果](../Page/乾果.md "wikilink")、[南非茶](../Page/南非茶.md "wikilink")、[綠茶](../Page/綠茶.md "wikilink")、[蛋糕](../Page/蛋糕.md "wikilink")、[餅乾](../Page/餅乾.md "wikilink")、[蜂蜜](../Page/蜂蜜.md "wikilink")、[葡萄乾](../Page/葡萄乾.md "wikilink")、[麥片](../Page/麥片.md "wikilink")、[果醬](../Page/果醬.md "wikilink")、[酸辣醬](../Page/酸辣醬.md "wikilink")、
[調味醬](../Page/調味醬.md "wikilink")、[香草](../Page/香草.md "wikilink")、[香料](../Page/香料.md "wikilink")、[堅果](../Page/堅果.md "wikilink")、[葡萄酒](../Page/葡萄酒.md "wikilink")、[啤酒](../Page/啤酒.md "wikilink")、[甜酒](../Page/甜酒.md "wikilink")、[花](../Page/花.md "wikilink")、[足球](../Page/足球.md "wikilink")、[稻米](../Page/稻米.md "wikilink")、[酸乳酪](../Page/酸乳酪.md "wikilink")、嬰兒食品、[棉花](../Page/棉花.md "wikilink")[羊毛以及棉製品](../Page/羊毛.md "wikilink")。

## 歷史

第一個貼有公平貿易標章的商品是1988年出現在[荷蘭的公平貿易咖啡](../Page/荷蘭.md "wikilink")。這個標章名叫“Max
Havelaar”，由荷蘭人Nico Roozen和傳教士Fans VanDer
Hoff依據荷蘭小說中一名在[殖民地上反對剝削咖啡農的人物所取名](../Page/殖民地.md "wikilink")。這個獨立的認證使得商品能在[世界商店以外銷售](../Page/世界商店.md "wikilink")，進入主流市場，讓公平貿易商品的銷售顯著增長。這個標籤行動同時也讓顧客及通路商能追蹤產品的來源，以確保讓[供應鍊另一端的生產者受益](../Page/供應鍊.md "wikilink")\[1\]。

這個概念自此受到廣泛關注，此後數年在[北美及](../Page/北美.md "wikilink")[歐洲出現了許多類似的非營利公平貿易標籤組織](../Page/歐洲.md "wikilink")，1997年，一個彙整標籤（Labelling
Initiatives，簡稱LIs）的過程催生了[國際公平貿易標籤組織](../Page/國際公平貿易標籤組織.md "wikilink")（FLO）的成立，FLO是一個傘形組織，其任務在訂定公平貿易的標準，支持、檢查並認證那些弱勢的生產者，同時協調公平貿易運動。

2002年，FLO發起新的國際公平貿易認證標章（International Fairtrade Certification
Mark），目標在於增加標章在[超級市場貨架上的能見度](../Page/超級市場.md "wikilink")、促進跨國貿易及簡化生產者及進口者之間的程序。隨着國際公平貿易認證標章的被認同，公平貿易體系已經成為國際關係與全球公平的標準。

公平貿易標章的整合仍然持續進行中，目前全世界除了三個國家（美國、加拿大、瑞士）都已經整合為同一個新的國際公平貿易標章。新標章的全面取代工作，正依據各個不同的國家而有不同的進展，應當不久之後就會成真\[2\]。

現今，有超過16個FLO的認證會員，基於FLO的認證，在數十個產品上使用這個國際公平貿易認證標章，包括有咖啡、茶、米、香蕉、芒果、可可、棉花、糖、蜂蜜、果汁、堅果、新鮮水果、奎寧、藥草、香料、紅酒及足球等產品。

## 參見

  - [貿易要公平](../Page/貿易要公平.md "wikilink")
  - [公平貿易](../Page/公平貿易.md "wikilink")
  - [公平貿易認證](../Page/公平貿易認證.md "wikilink")
  - [公平貿易認證組織](../Page/公平貿易認證組織.md "wikilink")
  - [國際公平貿易標籤組織](../Page/國際公平貿易標籤組織.md "wikilink")
  - [另類貿易組織](../Page/另類貿易組織.md "wikilink")（ATO）
  - [世界商店](../Page/世界商店.md "wikilink")（Worldshop）

## 參考資料

## 外部連結

  - [Fairtrade Labelling Organizations
    International](http://www.fairtrade.net)

[en:International Fairtrade Certification
Mark](../Page/en:International_Fairtrade_Certification_Mark.md "wikilink")

[Category:公平贸易](../Category/公平贸易.md "wikilink")
[Category:認證標誌](../Category/認證標誌.md "wikilink")

1.  Redfern A. & Snedker P. (2002) [Creating Market Opportunities for
    Small Enterprises: Experiences of the Fair Trade
    Movement](http://www.ilo.org/dyn/empent/docs/F1057768373/WP30-2002.pdf)
    . International Labor Office. p7
2.  Fairtrade Labelling Organizations International (2006). [About Fair
    Trade](http://www.fairtrade.net/aboutfairtrade.html)  URL accessed
    on August 4, 2006.