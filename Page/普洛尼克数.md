在[數學中](../Page/數學.md "wikilink")，**普洛尼克数**（pronic
number），也叫**矩形数**（oblong
number），是两个连续非负整数积，即\(n\times(n+1)\)。第n个普洛尼克数都是n的[三角形数的两倍](../Page/三角形数.md "wikilink")。开头的几个普洛尼克数是

  -

      -
        [0](../Page/0.md "wikilink")，[2](../Page/2.md "wikilink")，[6](../Page/6.md "wikilink")，[12](../Page/12.md "wikilink")，[20](../Page/20.md "wikilink")，[30](../Page/30.md "wikilink")，[42](../Page/42.md "wikilink")，[56](../Page/56.md "wikilink")，[72](../Page/72.md "wikilink")，[90](../Page/90.md "wikilink")，[110](../Page/110.md "wikilink")，[132](../Page/132.md "wikilink")，[156](../Page/156.md "wikilink")，[182](../Page/182.md "wikilink")，[210](../Page/210.md "wikilink")，[240](../Page/240.md "wikilink")，[272](../Page/272.md "wikilink"),
        [306](../Page/306.md "wikilink"),
        [342](../Page/342.md "wikilink"),
        [380](../Page/380.md "wikilink"),
        [420](../Page/420.md "wikilink"),
        [462](../Page/462.md "wikilink"),
        [506](../Page/506.md "wikilink"),
        [552](../Page/552.md "wikilink"),
        [600](../Page/600.md "wikilink")，650, 702, 756, 812, 870, 930,
        992, 1056, 1122, 1190, 1260, 1332, 1406, 1482, 1560, 1640, 1722,
        1806, 1892, 1980, 2070, 2162

## 性質

  - 普洛尼克数也可以表达成\(n^2+n\)。

  - 对于第n个普洛尼克数也正好等于头n个[偶数的和](../Page/偶数.md "wikilink")，即\((2n- 1)^2\)与[中心六邊形數的差](../Page/中心六邊形數.md "wikilink")。

  - 普洛尼克数不可能是奇数。

  -
  -
  -
  - 除了0以外，普洛尼克數也不可能是[平方數](../Page/平方數.md "wikilink")。

  -
  -
  - 普洛尼克數的4倍加1是[平方數](../Page/平方數.md "wikilink")。

  - [直角](../Page/直角.md "wikilink")（90°）是普洛尼克數。

  - 連續兩個普洛尼克數的[平均是](../Page/平均.md "wikilink")[平方數](../Page/平方數.md "wikilink")。

  - 显然，[2是唯一的一个](../Page/2.md "wikilink")[素普洛尼克数](../Page/素数.md "wikilink")，也是[斐波那契数列中唯二的普洛尼克数](../Page/斐波那契数列.md "wikilink")（另一個是0）\[1\]。

## 註釋

## 参考资料

[category:有形數](../Page/category:有形數.md "wikilink")

1.