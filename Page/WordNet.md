**WordNet**是一个由[普林斯顿大学认识科学实验室在心理学教授](../Page/普林斯顿大学.md "wikilink")[乔治·A·米勒的指导下建立和维护的](../Page/乔治·A·米勒.md "wikilink")[英语](../Page/英语.md "wikilink")[字典](../Page/字典.md "wikilink")。开发工作从1985年开始，从此以后该项目接受了超过300万美元的资助（主要来源于对[机器翻译有兴趣的](../Page/机器翻译.md "wikilink")[政府机构](../Page/政府机构.md "wikilink")）\[1\]。

由于它包含了[语义信息](../Page/语义信息.md "wikilink")，所以有别于通常意义上的字典。WordNet根据词条的意义将它们分组，每一个具有相同意义的字条组称为一个synset（同义词集合）。WordNet为每一个synset提供了简短，概要的定义，并记录不同synset之间的语义关系。

WordNet的开发有两个目的：

1.  它既是一个[字典](../Page/字典.md "wikilink")，又是一个[辞典](../Page/辞典.md "wikilink")，它比单纯的辞典或词典都更加易于使用。
2.  支持自动的[文本分析以及人工智能应用](../Page/文本分析.md "wikilink")。

WordNet的数据库及相应的软件工具的发放遵照[BSD许可证书](../Page/BSD许可证.md "wikilink")，可以自由的下载和使用，亦可在线查询和使用。

## WordNet内部结构

在WordNet中，[名词](../Page/名词.md "wikilink")，[动词](../Page/动词.md "wikilink")，[形容词和](../Page/形容词.md "wikilink")[副词各自被组织成一个同义词的网络](../Page/副词.md "wikilink")，每个同义词集合都代表一个基本的语义概念，并且这些集合之间也由各种关系连接。（一个多义词将出现在它的每个意思的同义词集合中）。在WordNet的第一版中（标记为1.x），四种不同词性的网络之间并无连接。WordNet的名词网络是第一个发展起来的。

名词网络的主干是蕴涵关系的层次（上位／下位关系），它占据了关系中的将近80%。层次中的最顶层是11个抽象概念，称为基本类别始点（unique
beginners），例如实体（entity，“有生命的或无生命的具体存在”），心理特征（psychological
feature，“生命有机体的精神上的特征）。名词层次中最深的层次是16个节点。

## 外部链接

  - [WordNet 首页](http://wordnet.princeton.edu/)
  - [英文相關 WordNet 查詢](http://cdict.info/eedict.php)
  - [日文相關 WordNet 查詢](http://cdict.info/ejdict.php)
  - [-{zh-hans:灵格斯词霸; zh-hant:靈格斯詞霸;
    zh-tw:靈格斯翻譯家}-离线可用的](../Page/灵格斯.md "wikilink")
    [WordNet English
    Dictionary](http://www.lingoes.cn/zh/dictionary/dict_down.php?id=12D98EC3940843498672A92149455292)
    词典文档

{{-}}

[Category:语义词典](../Category/语义词典.md "wikilink")
[Category:自由词典软件](../Category/自由词典软件.md "wikilink")

1.