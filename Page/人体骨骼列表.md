一具标准成年人的[骨骼包括下面](../Page/骨骼.md "wikilink")206块： （标示**黑体**的数字与右边图示相对照）

[Human_skeleton_diagram.png](https://zh.wikipedia.org/wiki/File:Human_skeleton_diagram.png "fig:Human_skeleton_diagram.png")

## 颅骨

[颅骨](../Page/颅骨.md "wikilink")（22）：

  - 头盖骨
      - **1.** [额骨](../Page/额骨.md "wikilink")（frontal
        bone）：[額骨位於前額處](../Page/額骨.md "wikilink")，可以分為[額鱗](../Page/額鱗.md "wikilink")、[眶部和](../Page/眶部.md "wikilink")[鼻部](../Page/鼻部.md "wikilink")。
      - **2.** [顶骨](../Page/顶骨.md "wikilink")（parietal
        bone，2）：[頂骨位於](../Page/頂骨.md "wikilink")[頭顱頂部的兩側](../Page/頭顱.md "wikilink")，是一種方形的[扁骨](../Page/扁骨.md "wikilink")。
      - **3.** [颞骨](../Page/颞骨.md "wikilink")（temporal
        bone，2）：[顳骨位於](../Page/顳骨.md "wikilink")[頭顱兩側](../Page/頭顱.md "wikilink")，並延伸至頭顱的底部。
      - **4.** [枕骨](../Page/枕骨.md "wikilink")（occipital
        bone）：[枕骨位於](../Page/枕骨.md "wikilink")[頂骨的後面](../Page/頂骨.md "wikilink")，並延伸至頭顱的底部。
      - [蝶骨](../Page/蝶骨.md "wikilink")（sphenoid
        bone）：[蝶骨的形狀好像](../Page/蝶骨.md "wikilink")[蝴蝶](../Page/蝴蝶.md "wikilink")，故命名為蝶骨。它位於[額骨](../Page/額骨.md "wikilink")、[篩骨](../Page/篩骨.md "wikilink")、[顳骨和](../Page/顳骨.md "wikilink")[枕骨之間](../Page/枕骨.md "wikilink")，並橫向延伸至頭顱的底部。
      - [筛骨](../Page/筛骨.md "wikilink")（ethmoid
        bone）：[篩骨位於兩個眼眶之間](../Page/篩骨.md "wikilink")，上接[額骨鼻部並突入進](../Page/額骨.md "wikilink")[鼻腔內](../Page/鼻腔.md "wikilink")。
  - 面骨：
      - **5.** [颧骨](../Page/颧骨.md "wikilink")（zygomatic bone,
        2）：[顴骨位於](../Page/顴骨.md "wikilink")[面部兩側](../Page/臉部.md "wikilink")，呈四邊形，厚而堅硬。
      - **6.**
        [上颌骨](../Page/上颌骨.md "wikilink")（maxilla，2）：[上頜骨位於](../Page/上頜骨.md "wikilink")[面部的中央](../Page/臉部.md "wikilink")。
      - **7.**
        [下颌骨](../Page/下颌骨.md "wikilink")\[1\]（mandible）：[下頜骨位於](../Page/下頜骨.md "wikilink")[上頜骨的下方](../Page/上頜骨.md "wikilink")。
      - **9.** [鼻骨](../Page/鼻骨.md "wikilink")（nasal bone,
        2）：[鼻骨是構成鼻背的小骨片](../Page/鼻骨.md "wikilink")。
      - [顎骨](../Page/顎骨.md "wikilink")\[2\]（palatine bone,
        2）：[顎骨位於](../Page/顎骨.md "wikilink")[上頜骨鼻面後方](../Page/上頜骨.md "wikilink")，為「L」形的薄骨片。
      - [泪骨](../Page/泪骨.md "wikilink")（lacrimal
        bone，2）：[淚骨是面骨當中最小的骨頭之一](../Page/淚骨.md "wikilink")。
      - [犁骨](../Page/犁骨.md "wikilink")（vomer）：[犁骨是一個薄骨片](../Page/犁骨.md "wikilink")，它參與組成鼻中間隔的後方下部。
      - [下鼻甲](../Page/下鼻甲.md "wikilink")（inferior nasal concha,
        2）：[下鼻甲為捲曲的貝殼狀薄骨片](../Page/下鼻甲.md "wikilink")，它附著在[上頜骨的鼻面](../Page/上頜骨.md "wikilink")。

## 耳骨

[耳骨](../Page/耳骨.md "wikilink")（在[中耳部](../Page/中耳.md "wikilink"), 6）：

  - [锤骨](../Page/锤骨.md "wikilink")（malleus, 2）
  - [砧骨](../Page/砧骨.md "wikilink")\[3\]（incus, 2）
  - [镫骨](../Page/镫骨.md "wikilink")（stapes, 2）

## 喉部骨骼

[喉部骨骼](../Page/喉部.md "wikilink")（1）：

  - [舌骨](../Page/舌骨.md "wikilink")（hyoid bone）

## 肩部骨骼

[肩部骨骼](../Page/肩部.md "wikilink")（4）：

  - **25.** [锁骨](../Page/锁骨.md "wikilink")（clavicle or collarbone,
    2）：[鎖骨位於胸廓上方前面的皮下](../Page/鎖骨.md "wikilink")，呈「S」形，內側2/3凸向前，外側1/3凸向後。
  - **29.** [肩胛骨](../Page/肩胛骨.md "wikilink")（scapula or shoulder blade,
    2）：[肩胛骨是一種](../Page/肩胛骨.md "wikilink")[扁骨](../Page/扁骨.md "wikilink")，呈三角形，它位於胸廓背面[脊柱的兩側](../Page/脊柱.md "wikilink")。

## 胸部骨骼

[胸部骨骼](../Page/胸部.md "wikilink")（25）：

  - **10.**
    [胸骨](../Page/胸骨.md "wikilink")（sternum）：[胸骨位於胸前壁正中的](../Page/胸骨.md "wikilink")[扁骨](../Page/扁骨.md "wikilink")，其形狀很像一柄短劍。
  - **28.** [肋骨](../Page/肋骨.md "wikilink")（ribs, 2 x
    12）：[肋骨是彎曲而富有彈性的骨塊](../Page/肋骨.md "wikilink")，它們互相聯結成一副防護的支架，有利於保護人體的[內臟器官](../Page/內臟.md "wikilink")。

## 脊椎

[脊椎](../Page/脊椎.md "wikilink")\[4\]（24）：

  - **8.** [颈椎](../Page/颈椎.md "wikilink")（cervical
    vertebrae，7）包括和：[頸椎的椎體較小](../Page/頸椎.md "wikilink")，近似長方形，具有支撐[頭顱和](../Page/頭顱.md "wikilink")[頸部的作用](../Page/頸部.md "wikilink")。
  - [胸椎](../Page/胸椎.md "wikilink")（Thoracic
    vertebrae，12）：[胸椎從上向下的椎體逐漸增大](../Page/胸椎.md "wikilink")，其橫截面近似三角形。它們與[肋骨聯結](../Page/肋骨.md "wikilink")，從而得以固定。
  - **14.** [腰椎](../Page/腰椎.md "wikilink")（lumbar
    vertebrae，5）：[腰椎是人體](../Page/腰椎.md "wikilink")[脊柱上最強大的骨頭](../Page/脊柱.md "wikilink")，其功能是支撐上半身的重量。

## 手臂骨骼

手臂骨骼（6）：

  - **11.**
    [肱骨](../Page/肱骨.md "wikilink")\[5\]（humerus，2）：[肱骨是臂部的長管狀骨](../Page/肱骨.md "wikilink")，分為一體兩端。
      - **26.**肱骨骨节
  - **12.**
    [尺骨](../Page/尺骨.md "wikilink")（ulna，2）：[尺骨位於前臂兩側](../Page/尺骨.md "wikilink")，可以分為一體兩端。[尺骨上端粗大](../Page/尺骨.md "wikilink")，前面有一半月形的[關節面](../Page/關節.md "wikilink")。尺骨稍微彎曲，呈三棱柱形。
  - **13.**
    [桡骨](../Page/桡骨.md "wikilink")\[6\]（radius，2）：[橈骨位於前臂兩側](../Page/橈骨.md "wikilink")，可以分為一體兩端。[橈骨上端是個扁圓形的橈骨頭](../Page/橈骨.md "wikilink")，下端特別膨大，近似立方形。
      - **27.**桡骨头

## 手骨

手骨（54）：

  - [腕骨](../Page/腕骨.md "wikilink")\[7\]（carpus bone）：
      - [手舟骨](../Page/手舟骨.md "wikilink")（scaphoid bone，2）
      - [月骨](../Page/月骨.md "wikilink")（lunate bone，2）
      - [三角骨](../Page/三角骨.md "wikilink")（triquetral bone，2）
      - [豌豆骨](../Page/豌豆骨.md "wikilink")\[8\]（pisiform bone，2）
      - [大多角骨](../Page/大多角骨.md "wikilink")（trapezium bone，2）
      - [小多角骨](../Page/小多角骨.md "wikilink")（trapezoid bone，2）
      - [头状骨](../Page/头状骨.md "wikilink")（capitate bone，2）
      - [钩骨](../Page/钩骨.md "wikilink")（hamate bone，2）
  - [掌骨](../Page/掌骨.md "wikilink")（metacarpal
    bones，5×2）：[掌骨可以分為一體兩端](../Page/掌骨.md "wikilink")，近側端稱為底，掌骨體呈稜柱形，稍向背側彎曲。遠側端為掌骨小頭，呈球形，與[指骨相連](../Page/指骨.md "wikilink")。
  - [指骨](../Page/指骨.md "wikilink")：
      - [近节指骨](../Page/近节指骨.md "wikilink")（proximal phalanx，5×2）
      - [中节指骨](../Page/中节指骨.md "wikilink")（middle phalanx，4×2）
      - [远节指骨](../Page/远节指骨.md "wikilink")（distal phalanx，5×2）

## 骨盆

[骨盆](../Page/骨盆.md "wikilink")（4）：

  - **15.** [髋骨](../Page/髋骨.md "wikilink")\[9\]（os coxae，2）
  - **16.** [骶骨](../Page/骶骨.md "wikilink")\[10\]（sacrum）
  - [尾骨](../Page/尾骨.md "wikilink")（coccyx）

## 腿骨

[腿骨](../Page/腿部.md "wikilink")（8）：

  - **18.**
    [股骨](../Page/股骨.md "wikilink")（femur，2）：[股骨是人體內最長且最粗的管狀骨](../Page/股骨.md "wikilink")，一般占人體高度的27%左右。[股骨上端朝向內上方](../Page/股骨.md "wikilink")，其末端膨大呈球形。
      - **17.**股骨头
      - **22.**大转子
      - **23.**大腿骨节
  - **19.** [髌骨](../Page/髌骨.md "wikilink")\[11\]（patella，2）
  - **20.**
    [胫骨的轴部和](../Page/胫骨.md "wikilink")**24.**骨节（tibia，2）：[脛骨就像兩根鐵柱](../Page/脛骨.md "wikilink")，承擔著人全身的重量。[脛骨是人體內最堅硬的骨頭](../Page/脛骨.md "wikilink")。
  - **21.**
    [腓骨](../Page/腓骨.md "wikilink")\[12\]（fibula，2）：[腓骨形狀細長](../Page/腓骨.md "wikilink")，其上端膨大，稱為[腓骨小頭](../Page/腓骨小頭.md "wikilink")，在皮膚表面可以觸及。

## 足部

[足部](../Page/足.md "wikilink")（52）：

  - [踝骨](../Page/踝.md "wikilink")（tarsals）：
      - [跟骨](../Page/跟骨.md "wikilink")
        (Calcaneus)（2）：[跟骨位於](../Page/跟骨.md "wikilink")[距骨的下方](../Page/距骨.md "wikilink")，前端為一[鞍狀關節面](../Page/鞍狀關節.md "wikilink")；後部膨大，稱為[跟結節](../Page/跟結節.md "wikilink")。
      - [距骨](../Page/距骨.md "wikilink")
        (Talus)（2）：[距骨位於](../Page/距骨.md "wikilink")[跟骨的上方](../Page/跟骨.md "wikilink")。
      - [足舟骨](../Page/足舟骨.md "wikilink")（2）：[足舟骨呈舟狀](../Page/足舟骨.md "wikilink")，位於[距骨和](../Page/距骨.md "wikilink")3塊[楔骨之間](../Page/楔骨.md "wikilink")。
      - [内侧楔骨](../Page/内侧楔骨.md "wikilink")\[13\]（2）
      - [中间楔骨](../Page/中间楔骨.md "wikilink")（2）
      - [外侧楔骨](../Page/外侧楔骨.md "wikilink")（2）
      - [骰骨](../Page/骰骨.md "wikilink")\[14\]（2）：[骰骨呈立方形](../Page/骰骨.md "wikilink")，位於[跟骨與第四](../Page/跟骨.md "wikilink")、五[跖骨之間](../Page/跖骨.md "wikilink")，內側面接[外側楔骨及](../Page/外側楔骨.md "wikilink")[足舟骨](../Page/足舟骨.md "wikilink")。
  - 足背骨（metatarsals）：
      - [跖骨](../Page/跖骨.md "wikilink")\[15\]（5×2）
  - [趾骨](../Page/趾.md "wikilink")（phalanges）：
      - [近节趾骨](../Page/近节趾骨.md "wikilink")（5×2）
      - [中节趾骨](../Page/中节趾骨.md "wikilink")（4×2）
      - [远节趾骨](../Page/远节趾骨.md "wikilink")（5×2）

## 幼兒骨骼

幼儿骨骼还包括下面这些骨骼：

1.  [骶椎](../Page/骶椎.md "wikilink")（4或5），成年后融合为骶骨
2.  [尾椎](../Page/尾椎.md "wikilink")（3到5），成年后融合为尾骨
3.  [髂骨](../Page/髂骨.md "wikilink")\[16\]，[坐骨和](../Page/坐骨.md "wikilink")[耻骨](../Page/耻骨.md "wikilink")，成年后融合成整体的髋骨

## 備註

<references group="注"/>

## 参见

  - [人体骨架](../Page/人体骨架.md "wikilink")
  - [人體器官列表](../Page/人體器官列表.md "wikilink")
  - [人體肌肉列表](../Page/人體肌肉列表.md "wikilink")

[Category:人体解剖学](../Category/人体解剖学.md "wikilink")
[\*](../Category/骨骼.md "wikilink")
[Category:醫學列表](../Category/醫學列表.md "wikilink")

1.  [下頜骨又稱為](../Page/下頜骨.md "wikilink")「下顎骨」。
2.  「顎」和「齶」雖然在教育部字典通用，可是仔細細分的話還是有差別。「顎」是指整個口腔包括骨頭的部分，而「齶」是指在口腔裡的結構，例如：[硬顎和](../Page/硬顎.md "wikilink")[軟顎](../Page/軟顎.md "wikilink")。
3.  。
4.  。
5.  。
6.  。
7.  。
8.  。
9.  。
10. 。
11. 。
12. 。
13. 。
14. 。
15. 。
16. 。