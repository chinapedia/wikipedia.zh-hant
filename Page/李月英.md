**李月英**（**Hazel Ying
Lee**，），[華裔美國人](../Page/華裔美國人.md "wikilink")，出生於美國[奧勒岡州](../Page/奧勒岡州.md "wikilink")[波特蘭市](../Page/波特蘭.md "wikilink")，祖籍[廣東](../Page/廣東.md "wikilink")[台山市大江镇水楼村](../Page/台山.md "wikilink")。

## 早年

父亲经商，母亲是传统的家庭主妇，要幫忙丈夫的生意，也要照顾八个孩子。她在年輕時游泳，打手球，喜歡玩牌，也學會了開車。李月英1929年高中毕业后，在波特兰市中心的一家百货公司作[电梯操作员的工作](../Page/电梯.md "wikilink")，在当时歧視有色人種的環境裡，这是当时华人女性被许可从事的少数工作之一。

## 學習飛行與返回中國

1932年不顧父母亲坚决反对，接受愛國[華僑的資助](../Page/華僑.md "wikilink")，在美國奧勒岡州波特蘭[美國華僑航空學校學習飛行](../Page/美國華僑航空學校.md "wikilink")。校友及同學包括[陳瑞鈿](../Page/陳瑞鈿.md "wikilink")、[黃泮揚](../Page/黃泮揚.md "wikilink")、黃桂燕、蘇英祥、[雷炎均](../Page/雷炎均.md "wikilink")、劉龍光、林覺天、雷國來、[楊仲安等](../Page/楊仲安.md "wikilink")。1933年與男同學一起志願回到中國希望加入空軍，可惜當時空軍不收女性飛行員，後居留廣東擔任民航飛行員。1937年全面抗戰爆發，再度志願加入中國空軍的飛行行列不遂，於是從事宣傳工作與開辦學校。1938年返美國[紐約任](../Page/紐約.md "wikilink")[國民政府駐美採購員](../Page/國民政府.md "wikilink")。

## 美國婦女飛行隊

1943年加入新成立的美國[婦女空軍服務飛行隊](../Page/婦女空軍服務飛行隊.md "wikilink")，為少數華人女子飛行員的先驅。有一次因為引擎故障，飛機迫降在[堪薩斯州的麥田裡](../Page/堪薩斯州.md "wikilink")。農夫以為她是來侵略的日本人，拿起草叉指著李月英，她請農夫放下草叉並請農夫的兒子打電話到基地澄清這誤會並派車來接她。1944年4月，李月英接受儀器訓練，準備飛更高級的飛機。6月希望成為正式軍官的她接受軍官培養訓練，當時盛傳她們也會成為正式軍官，但後來並未成事實。9月她又接受驅逐機訓練，並於10月2日畢業，成為第一位駕駛軍機的華人女子飛行員，也成為第一位駕駛[驅逐機](../Page/驅逐機.md "wikilink")（當時戰鬥機的名稱）的華人女子飛行員。當時主要任務是將新飛機由工廠飛到目的地機場。

許多女子飛行員為美國付出生命，但在因公殉職後，並未得到應享的國家待遇。一直到1977年，一部有关承认婦女飛行隊在军队中地位的法律获得通过，婦女飛行隊成员们终于能和其他退役军人一样，获得公正的历史评价。

## 開朗的個性

她的幽默感确实让大家难以忘怀，也很淘气，恶作剧的功夫属一流。例如她用口红在自己与同事的飞机机翼写上中国字，还有一次她开一名身材肥胖的飞行员玩笑，在对方的机尾以中文写上他看不懂的绰号。李月英还有一个特色，不管在任何大城市或小乡镇，她总能找到中国餐馆吃上一顿丰盛的中国菜，甚至鑽進廚房請廚師或自己參與作幾道菜。另一位婦女飞行员Sylvia
Dahmes Clayton说：“Hazel给了我一个机会让我了解不同的文化，增广我的视野，也扩大了我的生活领域。”

## 意外喪生

1944年11月10日她由[紐約州](../Page/紐約州.md "wikilink")[尼亞加拉瀑布](../Page/尼亞加拉瀑布.md "wikilink")[貝爾飛機公司機場將援俄的](../Page/貝爾直升機公司.md "wikilink")[P-63驅逐機飛到](../Page/P-63.md "wikilink")[蒙大拿州的](../Page/蒙大拿.md "wikilink")[大瀑布城機場](../Page/大瀑布城.md "wikilink")（男飛行員再飛到[阿拉斯加交給](../Page/阿拉斯加.md "wikilink")[蘇聯飛行員](../Page/蘇聯.md "wikilink")）。她在[北達科他州受到壞天氣的延誤](../Page/北達科他州.md "wikilink")，等到天氣好轉以後，李月英於1944年11月23日下午两点左右，就要在蒙大拿州大瀑布城機場降落的同时，有許多飛機一同到達，她的後方有另一架战机通讯系统故障，飞行员无法听从塔台的指挥，飞近同一跑道准备用燈號降落。塔台指揮李月英重飛，卻無法連絡另一架战机。结果這架战机与準備重飛而正在爬高的李月英的飞机相撞，两架飞机同时爆炸。在意外中受到大面積燒傷的李月英於11月25日在醫院去世。

## 李氏家族對抗種族歧視

李氏家族在得知李月英的噩耗之後的第三天，得知李月英在美軍裝甲部隊服役正在法國戰場作戰的兄弟也為國犧牲了。當李氏家族為他們選好一個風景優美俯視[哥倫比亞河的山坡為墓地時](../Page/哥倫比亞河.md "wikilink")，遭到墓園反對，認為[有色人種不能葬在](../Page/有色人種.md "wikilink")[白人區墓地](../Page/白人.md "wikilink")。
經過冗長的抗爭，李氏家族終於如願在所選好的墓地為李月英舉行了一場非軍人的葬禮。

## 記錄片與其他媒體

她的故事，经由美国[记录片导演](../Page/记录片.md "wikilink") Alan H.Rosenberg
与华裔制片谭立人（Montgomery
Hom）等人拍摄成记录片《短暂的凌空飞翔：驾驶战斗机的女人李月英》（A
Brief Flight: Hazel Ying Lee and The Women Who Flew
Pursuit）在美国[公共电视台](../Page/公共电视台.md "wikilink")（PBS
Station）播出后，在美国华人社区引起强烈的反响。

2003年5月18日，美国中文[世界日報刊物](../Page/世界日報.md "wikilink")《世界周刊》登載《李月英传奇》，并刊載了她驾驶飞机的照片。

## 婚姻

夫婿是美國華僑航空學校的同學[雷炎均](../Page/雷炎均.md "wikilink")。

## 參考資料

  - [飞行器上的好姑娘——二战美国陆军妇女飞行队](http://bbs.joy.cn/showtopic-2012069.htm)
  - [华裔女飞行员李月英传奇](http://book.sina.com.cn/longbook/cha/1102659271_meiguojingcha/10.shtml)
  - [A Brief Flight](http://www.hazelyinglee.com/)
  - [National Museum of the USAF - Fact Sheets: Hazel Ying
    Lee](https://web.archive.org/web/20081207011253/http://www.nationalmuseum.af.mil/factsheets/factsheet.asp?id=1556)

[Category:女子飛行員](../Category/女子飛行員.md "wikilink")
[Category:華裔美國軍事人物](../Category/華裔美國軍事人物.md "wikilink")
[Category:美國陸軍女性](../Category/美國陸軍女性.md "wikilink")
[Category:中華民國飛行員](../Category/中華民國飛行員.md "wikilink")
[Category:美國第二次世界大戰人物](../Category/美國第二次世界大戰人物.md "wikilink")
[Category:中國第二次世界大戰人物](../Category/中國第二次世界大戰人物.md "wikilink")
[Category:第二次世界大戰女性人物](../Category/第二次世界大戰女性人物.md "wikilink")
[Category:华裔美国人](../Category/华裔美国人.md "wikilink")
[Category:台山人](../Category/台山人.md "wikilink")
[Category:俄勒岡州人](../Category/俄勒岡州人.md "wikilink")
[Yue月](../Category/李姓.md "wikilink")
[Category:美國空難身亡者](../Category/美國空難身亡者.md "wikilink")