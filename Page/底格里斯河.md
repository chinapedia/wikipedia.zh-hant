[TigrisRiver.JPG](https://zh.wikipedia.org/wiki/File:TigrisRiver.JPG "fig:TigrisRiver.JPG")的底格-{里}-斯河\]\]
[Tigris_river_Mosul.jpg](https://zh.wikipedia.org/wiki/File:Tigris_river_Mosul.jpg "fig:Tigris_river_Mosul.jpg")\]\]
[Haifa_street,_as_seen_from_the_medical_city_hospital_across_the_tigres.jpg](https://zh.wikipedia.org/wiki/File:Haifa_street,_as_seen_from_the_medical_city_hospital_across_the_tigres.jpg "fig:Haifa_street,_as_seen_from_the_medical_city_hospital_across_the_tigres.jpg")
**底格里斯河**（；）是[中东](../Page/中东.md "wikilink")[名河](../Page/河流.md "wikilink")，与位于其西面的[幼发拉底河共同界定](../Page/幼发拉底河.md "wikilink")[美索不达米亚](../Page/美索不达米亚.md "wikilink")，源自[土耳其](../Page/土耳其.md "wikilink")[安纳托利亚的山区](../Page/安纳托利亚.md "wikilink")，流经[伊拉克](../Page/伊拉克.md "wikilink")，最后与幼发拉底河合流成为[阿拉伯河注入](../Page/阿拉伯河.md "wikilink")[波斯湾](../Page/波斯湾.md "wikilink")。

底格里斯河流域面积37.5万平方公里，年均流量42亿立方米，为[西南亚水量最大的河流](../Page/西南亚.md "wikilink")，底格里斯河的中下游都位於伊拉克境內，令原本位處乾旱氣候地區的伊拉克擁有水源豐富的河流，主要城市都在該河流的流域範圍，是伊拉克人口的主要集中地。

伊拉克首都[巴格达正位于底格里斯河西岸](../Page/巴格达.md "wikilink")。

## 圣经中的底格里斯河

《[圣经](../Page/圣经.md "wikilink")》是最早提到底格里斯河的文獻。在[创世纪第二章提到此河是流經](../Page/创世纪.md "wikilink")[伊甸園的四條河之一](../Page/伊甸園.md "wikilink")，在《圣经》中底格里斯河依其希伯來名稱譯为希底结河，其餘三條分別是[比逊河](../Page/比逊河.md "wikilink")（Pishon）、[基训河](../Page/基训河.md "wikilink")（Gihon）及「伯拉大河」，也就是幼发拉底河。

## 歷史的底格里斯河

底格里斯河在5000年前與幼發拉底河是兩條分開的河。直到約三、四千年前，由於從[兩河流域帶來的泥沙不斷在河口的](../Page/兩河流域.md "wikilink")[波斯灣沉積](../Page/波斯灣.md "wikilink")，填出土地來，最後使兩河下游在伊拉克南部匯合在一起。

## 外部链接

  - [Livius.org:
    Tigris](http://www.livius.org/men-mh/mesopotamia/tigris.html)
  - [Managing the Tigris and Euphrates
    Watershed](https://web.archive.org/web/20110720184608/http://zunia.org/uploads/media/knowledge/Geopolicity%20-%20Managing%20the%20Tigris%20and%20Euphrates%20Watershed%20-%20The%20Challenge%20Facing%20Iraq1280855782.pdf)
  - [Bibliography on Water Resources and International
    Law](http://www.ppl.nl/index.php?option=com_wrapper&view=wrapper&Itemid=82)
    Peace Palace Library
  - [1](https://web.archive.org/web/20151119012635/http://www.naval-history.net/WW1Battle1408Mesopotamia.htm)

[Category:圣经地名](../Category/圣经地名.md "wikilink")
[\*](../Category/两河流域.md "wikilink")
[Category:肥沃月彎](../Category/肥沃月彎.md "wikilink")
[Category:亞洲跨國河流](../Category/亞洲跨國河流.md "wikilink")
[Category:土耳其河流](../Category/土耳其河流.md "wikilink")
[Category:敘利亞河流](../Category/敘利亞河流.md "wikilink")
[Category:伊拉克河流](../Category/伊拉克河流.md "wikilink")
[Category:敘利亞-土耳其邊界](../Category/敘利亞-土耳其邊界.md "wikilink")
[Category:伊拉克-敘利亞邊界](../Category/伊拉克-敘利亞邊界.md "wikilink")
[Category:底格里斯河](../Category/底格里斯河.md "wikilink")