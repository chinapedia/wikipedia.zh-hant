**K-net**（[英文](../Page/英文.md "wikilink")：）是[香港一家主要的](../Page/香港.md "wikilink")[卡拉OK](../Page/卡拉OK.md "wikilink")[版權管理公司](../Page/版權.md "wikilink")，成立於2006年，為唱片公司代理旗下歌手[卡拉OK歌曲的版權](../Page/卡拉OK.md "wikilink")。而K-net亦出租牌照予各[卡拉OK](../Page/卡拉OK.md "wikilink")、[酒吧及](../Page/酒吧.md "wikilink")[酒店等地方](../Page/酒店.md "wikilink")，協助[香港及](../Page/香港.md "wikilink")[澳門第一時間獲得最新最熱而且合法轉播的播放版權](../Page/澳門.md "wikilink")。K-net在於維護[知識產權](../Page/知識產權.md "wikilink")，致力打擊[侵權行為](../Page/侵權.md "wikilink")，以保障整個音樂創作工業享有公平守法的營商環境及持續的發展。

## 影響

K-net的出現，改變了原來由[Neway及](../Page/Neway.md "wikilink")[加州紅壟斷的情況](../Page/加州紅.md "wikilink")。由[香港音像聯盟創辦會員為全球四大國際](../Page/香港音像聯盟.md "wikilink")[唱片公司](../Page/唱片公司.md "wikilink")，包括[環球唱片及旗下品牌](../Page/環球唱片_\(香港\).md "wikilink")﹙[新藝寶唱片](../Page/新藝寶唱片.md "wikilink")、[正東唱片](../Page/正東唱片.md "wikilink")、[上華唱片](../Page/上華唱片.md "wikilink")（香港）及[寶麗金](../Page/寶麗金.md "wikilink")）、[EMI
Group Hong Kong Ltd.](../Page/百代唱片_\(香港\).md "wikilink")（註：[百代唱片
(香港)](../Page/百代唱片_\(香港\).md "wikilink")
，現時其運作實際由[華納唱片（香港）維持](../Page/香港華納唱片.md "wikilink")，但隨著[環球唱片正式收購](../Page/環球唱片_\(香港\).md "wikilink")[EMI關係](../Page/百代唱片_\(香港\).md "wikilink")，2013年末起交由[環球唱片負責管理](../Page/環球唱片_\(香港\).md "wikilink")）、[華納唱片與旗下品牌](../Page/華納唱片_\(香港\).md "wikilink")
—
[金牌大風及](../Page/金牌大風.md "wikilink")[索尼音樂娛樂香港均先後把其新歌的獨家試唱轉至K](../Page/索尼音樂娛樂香港.md "wikilink")-net，而[英皇娛樂](../Page/英皇娛樂.md "wikilink")、及部份小型唱片公司都有給予版權。

而曾經與K-net合作的唱片公司則包括有[蜂鳥音樂](../Page/蜂鳥音樂.md "wikilink")、[寰宇國際](../Page/寰宇國際.md "wikilink")、[大名娛樂](../Page/大名娛樂.md "wikilink")、[東亞唱片﹙集團﹚及](../Page/東亞唱片﹙集團﹚.md "wikilink")[A
Music](../Page/A_Music.md "wikilink")。

仍然把其新歌的獨家試唱留在[Neway的](../Page/Neway.md "wikilink")，只有[金牌大風](../Page/金牌大風.md "wikilink")、[東亞唱片
(集團)及](../Page/東亞唱片_\(集團\).md "wikilink")[A
Music等](../Page/A_Music.md "wikilink")。直至2008年10月起，K-net授權其代理的歌曲版權予[加州紅](../Page/加州紅.md "wikilink")。唯[加州紅於](../Page/加州紅.md "wikilink")[2010年3月被](../Page/2010年3月.md "wikilink")[Neway收購](../Page/Neway.md "wikilink")，[Neway其後把](../Page/Neway.md "wikilink")[加州紅其下大部份分店結束營業或者改為](../Page/加州紅.md "wikilink")[Neway](../Page/Neway.md "wikilink")，致系統更改原故，所有K-net持有的歌曲被刪除。直至2010年12月18日，[BMA集團主席](../Page/BMA.md "wikilink")[羅傑承開設](../Page/羅傑承.md "wikilink")[Red
Mr卡拉OK開幕](../Page/Red_Mr.md "wikilink")，旗下店舖與K-net合作，意味著K-net的歌曲再次出現在大型卡拉OK場所。

值得一提的是，同屬K-net陣營的[英皇娛樂並沒有將旗下的新歌試唱權供應給Red](../Page/英皇娛樂.md "wikilink")
Mr，只集中於酒吧及的士高，而[英皇娛樂仍然與Neway保持合作關係](../Page/英皇娛樂.md "wikilink")。

2014年，[華納唱片正式收購](../Page/華納唱片_\(香港\).md "wikilink")[金牌大風](../Page/金牌大風.md "wikilink")，成為旗下的子公司，並於2014年完成交易；由於現時金牌大風的新歌獨家試唱權仍然留在Neway關係，所以會在交易完成後正式離開Neway，並限隨母公司加盟K-net及其合作伙伴Red
Mr，並於翌年正式加盟。

## 目前向K-net提供新歌試唱的唱片公司

  - [環球唱片](../Page/環球唱片_\(香港\).md "wikilink")
      - [新藝寶唱片](../Page/新藝寶唱片.md "wikilink")
      - [正東唱片](../Page/正東唱片.md "wikilink")
      - [上華唱片 (香港)](../Page/上華唱片.md "wikilink")
      - [寶麗金唱片](../Page/寶麗金唱片.md "wikilink")
      - [百代唱片](../Page/百代唱片_\(香港\).md "wikilink")
  - [華納唱片](../Page/華納唱片_\(香港\).md "wikilink")﹙K-net、[Red
    Mr](../Page/紅人派對.md "wikilink")、[Neway共同提供](../Page/Neway.md "wikilink")﹚
      - [金牌大風](../Page/金牌大風.md "wikilink")﹙即將加盟K-net﹚
  - [索尼音樂娛樂 (香港)](../Page/索尼音樂娛樂_\(香港\).md "wikilink")﹙K-net、[Red
    Mr](../Page/紅人派對.md "wikilink")、[Neway共同提供](../Page/Neway.md "wikilink")﹚
      - [杰威爾音樂](../Page/杰威爾音樂.md "wikilink")
  - [CMD](../Page/Clot_Media_Division.md "wikilink")
  - [輝皇娛樂](../Page/輝皇娛樂.md "wikilink")
  - [英皇娛樂](../Page/英皇娛樂.md "wikilink")﹙K-net、[Neway共同提供](../Page/Neway.md "wikilink")﹚

## 參考資料

  - [K-net官方網頁](http://www.k-netmusic.com/)
  - [K-net Facebook](http://www.facebook.com/pages/K-Net/42406859584)

[Category:香港卡拉OK公司](../Category/香港卡拉OK公司.md "wikilink")