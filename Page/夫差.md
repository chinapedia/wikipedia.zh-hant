[thumb](../Page/image:Wuwangfuchaimao.JPG.md "wikilink")
**夫差**（），**姬**姓，[春秋时期](../Page/春秋时期.md "wikilink")[吴国第](../Page/吴国.md "wikilink")25任[君主](../Page/君主.md "wikilink")，在位时期为前495年—前473年。

## 生平

夫差为了洗雪其父[阖闾败给越王](../Page/阖闾.md "wikilink")[勾践的耻辱](../Page/勾践.md "wikilink")，夫差励精图治，吳國也迅速增強。夫差二年（前494年），於[夫椒](../Page/夫椒.md "wikilink")（今[江蘇](../Page/江苏省.md "wikilink")[苏州西南](../Page/苏州市.md "wikilink")[太湖中](../Page/太湖.md "wikilink")）打败越王[勾践](../Page/勾践.md "wikilink")。勝利後，勾践向[吳國表示臣服](../Page/吳國.md "wikilink")，夫差将勾践釋回[越国](../Page/越國.md "wikilink")。

勾踐在越國[卧薪尝胆](../Page/卧薪尝胆.md "wikilink")，十年生聚十年教訓，迅速恢復越國國力，圖謀報復。前486年，夫差在[邗](../Page/扬州_\(古代\).md "wikilink")（今[江蘇](../Page/江蘇.md "wikilink")[揚州附近](../Page/扬州市.md "wikilink")）築城，又開鑿[邗溝](../Page/邗溝.md "wikilink")，連結了[長江和](../Page/長江.md "wikilink")[淮河](../Page/淮河.md "wikilink")，在[艾陵之戰中全殲十萬](../Page/艾陵之戰.md "wikilink")[齊軍](../Page/齊.md "wikilink")。前482年，夫差在[黃池](../Page/黃池.md "wikilink")（今[河南](../Page/河南省.md "wikilink")[封丘西南](../Page/封丘县.md "wikilink")）會盟[諸侯](../Page/諸侯.md "wikilink")，與[晉爭霸獲勝](../Page/晋国.md "wikilink")。但夫差僅使[太子友和老弱守國](../Page/太子友.md "wikilink")，勾踐乘虛而入，大敗吳師、殺太子友。

夫差二十三年（前473年），都城[姑苏](../Page/苏州市.md "wikilink")（今[苏州](../Page/苏州.md "wikilink")）被勾踐興兵攻破，夫差被圍困在吳都西面的[姑蘇山上](../Page/姑蘇山.md "wikilink")，吳國滅亡，而夫差也被勾踐流放，最后自杀身亡。

日本《[新撰姓氏錄](../Page/新撰姓氏錄.md "wikilink")》[右京](../Page/右京.md "wikilink")[松野一族認為自己出自夫差](../Page/松野.md "wikilink")\[1\]。

## 家庭

### 子女

  - [太子友](../Page/太子友.md "wikilink")，前482年，[勾踐乘虛而入](../Page/勾践.md "wikilink")，大敗吳師、太子友被殺。
  - [王子地](../Page/王子地.md "wikilink")
  - 有一女紫玉

### 孫

[王孙弥庸](../Page/王孙弥庸.md "wikilink")、[王孙寿](../Page/王孙寿.md "wikilink")

## 在位年與西曆對照表

<div class="NavFrame" style="clear: both; border: 1px solid #999; margin: 0.5em auto;">

<div class="NavHead" style="background-color: #CCCCFF; font-size: 100%; border-left: 3em soli; text-align: center; font-weight: bold;">

在位年與西曆對照表

</div>

<div class="NavContent" style="padding: 1em 0 0 0; font-size: 100%; text-align: center;">

| 吳王夫差                           | 元年                             | 2年                             | 3年                             | 4年                             | 5年                             | 6年                             | 7年                             | 8年                             | 9年                             | 10年                            |
| ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| 西元                             | 前495年                          | 前494年                          | 前493年                          | 前492年                          | 前491年                          | 前490年                          | 前489年                          | 前488年                          | 前487年                          | 前486年                          |
| [干支](../Page/干支.md "wikilink") | [丙午](../Page/丙午.md "wikilink") | [丁未](../Page/丁未.md "wikilink") | [戊申](../Page/戊申.md "wikilink") | [己酉](../Page/己酉.md "wikilink") | [庚戌](../Page/庚戌.md "wikilink") | [辛亥](../Page/辛亥.md "wikilink") | [壬子](../Page/壬子.md "wikilink") | [癸丑](../Page/癸丑.md "wikilink") | [甲寅](../Page/甲寅.md "wikilink") | [乙卯](../Page/乙卯.md "wikilink") |
| 吳王夫差                           | 11年                            | 12年                            | 13年                            | 14年                            | 15年                            | 16年                            | 17年                            | 18年                            | 19年                            | 20年                            |
| 西元                             | 前485年                          | 前484年                          | 前483年                          | 前482年                          | 前481年                          | 前480年                          | 前479年                          | 前478年                          | 前477年                          | 前476年                          |
| [干支](../Page/干支.md "wikilink") | [丙辰](../Page/丙辰.md "wikilink") | [丁巳](../Page/丁巳.md "wikilink") | [戊午](../Page/戊午.md "wikilink") | [己未](../Page/己未.md "wikilink") | [庚申](../Page/庚申.md "wikilink") | [辛酉](../Page/辛酉.md "wikilink") | [壬戌](../Page/壬戌.md "wikilink") | [癸亥](../Page/癸亥.md "wikilink") | [甲子](../Page/甲子.md "wikilink") | [乙丑](../Page/乙丑.md "wikilink") |
| 吳王夫差                           | 21年                            | 22年                            | 23年                            |                                |                                |                                |                                |                                |                                |                                |
| 西元                             | 前475年                          | 前474年                          | 前473年                          |                                |                                |                                |                                |                                |                                |                                |
| [干支](../Page/干支.md "wikilink") | [丙寅](../Page/丙寅.md "wikilink") | [丁卯](../Page/丁卯.md "wikilink") | [戊辰](../Page/戊辰.md "wikilink") |                                |                                |                                |                                |                                |                                |                                |

</div>

</div>

## 参见

  - [吴王夫差矛](../Page/吴王夫差矛.md "wikilink")

  - [越灭吴之战](../Page/越灭吴之战.md "wikilink")

  -
  - [吳越爭霸史事系年考辨（俞志慧）《學燈》第二十六期](https://web.archive.org/web/20140727110527/http://www.confucius2000.com/admin/list.asp?id=5623)

## 参考文献

[Category:吴国君主](../Category/吴国君主.md "wikilink")
[Category:春秋人](../Category/春秋人.md "wikilink")
[J姬](../Category/苏州人.md "wikilink")
[Category:中国末代君主](../Category/中国末代君主.md "wikilink")
[Category:姬姓](../Category/姬姓.md "wikilink")
[Category:春秋時代自殺人物](../Category/春秋時代自殺人物.md "wikilink")

1.