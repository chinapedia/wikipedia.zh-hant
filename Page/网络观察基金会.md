**网络观察基金会**，是一个[互联网](../Page/互联网.md "wikilink")[自律协会](../Page/自律协会.md "wikilink")，成立于1996年，负责监督互联网上的有害（尤其針对儿童）信息并汇总。对社会宣传健康的网络使用习惯，监督互联网业者的非法和／或不道德行为。2008年，發生[網路觀察基金會與維基百科事件](../Page/網路觀察基金會與維基百科.md "wikilink")。

## 外部链接

  - [英国网络观察基金会](http://www.iwf.org.uk)
  - <http://www.rogerdarlington.co.uk/iwf.html>
  - <https://web.archive.org/web/20060211004711/http://dtiinfo1.dti.gov.uk/safety-net/>
  - <http://www.streamshield.com/index.php?option=com_content&task=view&id=62&Itemid=130>
  - <https://web.archive.org/web/20070928081309/http://www.devon-cornwall.police.uk/v3/crime/online/iwf/>
  - <https://web.archive.org/web/20060509132237/http://www.warwickshire.police.uk/onlineservices/IWF>
  - <https://web.archive.org/web/20030624185103/http://www.derbyshire.police.uk/reducingcrime/4.html>
  - <https://web.archive.org/web/20060622083239/http://www.ispa.org.uk/ispa_awards/categories/iwf.htm>
  - <https://web.archive.org/web/20061206085114/http://blog.eun.org/insafe/2006/02/uk_the_internet_watch_foundati.html>

[分類:英國慈善團體](../Page/分類:英國慈善團體.md "wikilink")

[Category:1996年建立](../Category/1996年建立.md "wikilink")