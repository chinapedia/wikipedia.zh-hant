**王叔文**（），[中國](../Page/中國.md "wikilink")[唐朝中期政治人物](../Page/唐朝.md "wikilink")。[越州](../Page/越州.md "wikilink")[山陰](../Page/山陰.md "wikilink")（今[浙江](../Page/浙江.md "wikilink")[紹興](../Page/紹興.md "wikilink")）人，自称是[王猛后裔](../Page/王猛.md "wikilink")\[1\]。[永貞改革領導人](../Page/永貞改革.md "wikilink")[二王八司馬中的](../Page/二王八司馬.md "wikilink")「二王」之一。

## 簡介

[蘇州](../Page/蘇州.md "wikilink")[司功出身](../Page/司功.md "wikilink")，善[圍棋](../Page/圍棋.md "wikilink")。[唐德宗時](../Page/唐德宗.md "wikilink")，擔任太子[李誦侍讀](../Page/李誦.md "wikilink")，“常為太子言民間疾苦”，獲太子喜愛。

[永貞元年](../Page/永貞_\(唐朝\).md "wikilink")（805年）正月，唐德宗駕崩，太子即位，即為順宗，授叔文[翰林待詔兼](../Page/翰林待詔.md "wikilink")[度支使](../Page/度支使.md "wikilink")、[鹽鐵轉運使](../Page/鹽鐵轉運使.md "wikilink")，王叔文聯合[王伾](../Page/王伾.md "wikilink")、[劉禹錫等人](../Page/劉禹錫.md "wikilink")，有意推行政治改革。拉攏德宗朝宰相兼[度支使](../Page/度支使.md "wikilink")[鹽鐵使的](../Page/鹽鐵使.md "wikilink")[杜佑](../Page/杜佑.md "wikilink")，但實際主導政事；再以[世族出身](../Page/世族.md "wikilink")、時任[監察御史的](../Page/監察御史.md "wikilink")[柳宗元出任](../Page/柳宗元.md "wikilink")[禮部](../Page/禮部.md "wikilink")[員外郎](../Page/員外郎.md "wikilink")，作為緩和世族官僚反彈之手段。

王叔文在[內侍省則以東宮系統的](../Page/內侍省.md "wikilink")[李忠言對抗](../Page/李忠言.md "wikilink")[神策軍系統的](../Page/神策軍.md "wikilink")[俱文珍](../Page/俱文珍.md "wikilink")、[劉光琦](../Page/劉光琦.md "wikilink")，但由於以整肅宦官為訴求，無法在[內侍省得到支持](../Page/內侍省.md "wikilink")。王叔文著手[減稅](../Page/減稅.md "wikilink")，罷諸道速奉，廢止[宦官把持的](../Page/宦官.md "wikilink")[宮市](../Page/宮市.md "wikilink")，史稱“市里歡呼”，“人情大悅”，但損及俱文珍等人的利益。

三月，[俱文珍聯合](../Page/俱文珍.md "wikilink")[裴均等人迫使順宗立](../Page/裴均.md "wikilink")[李純為太子](../Page/李純.md "wikilink")、[監國](../Page/監國.md "wikilink")，而王叔文隨後與[西川節度使](../Page/西川節度使.md "wikilink")[韋皋決裂](../Page/韋皋.md "wikilink")，韋皋投靠太子一方。五月，王叔文被削[翰林学士一职](../Page/翰林学士.md "wikilink")。七月，[太子](../Page/太子.md "wikilink")[监国](../Page/监国.md "wikilink")。同月，王叔文因母丧[丁憂](../Page/丁憂.md "wikilink")，回家守丧。八月，俱文珍、劉光琦、韋皋等扶立李純，即憲宗，尊順宗為[上皇](../Page/上皇.md "wikilink")，是為「[永貞內禪](../Page/永貞內禪.md "wikilink")」，憲宗貶王叔文為[渝州](../Page/渝州.md "wikilink")[司戶](../Page/司戶.md "wikilink")，[元和元年](../Page/元和_\(唐朝\).md "wikilink")（806年）[賜死](../Page/賜死.md "wikilink")。王伾被貶為[開州](../Page/開州.md "wikilink")[司馬](../Page/司馬.md "wikilink")，不久病死。[韓泰](../Page/韓泰.md "wikilink")、[陳諫](../Page/陳諫.md "wikilink")、柳宗元、劉禹錫、[韓曄](../Page/韓曄.md "wikilink")、[凌准](../Page/凌准.md "wikilink")、[程異及](../Page/程異.md "wikilink")[韋執誼等八人先後被貶為邊遠八州司馬](../Page/韋執誼.md "wikilink")，史稱「二王八司馬」。二王前後掌權一百四十六天，史稱「[永貞革新](../Page/永貞革新.md "wikilink")」。

## 評價

  - [王夫之肯定王叔文](../Page/王夫之.md "wikilink")“革德宗末年之乱政，以快人心，清国纪，亦云善矣。”但批評其：“器小而易盈，气浮而不守”，“胶漆以同其类，亢傲以待异己，得志自矜。”\[2\]

<!-- end list -->

  - [王鸣盛认为](../Page/王鸣盛.md "wikilink")“叔文行政，上利于国，下利于民，独不利于弄权之阉臣，跋扈之强藩。”\[3\]

<!-- end list -->

  -
## 注釋及參考資料

  - 《[大不列顛百科全書](../Page/大不列顛百科全書.md "wikilink")》（1987年版，台北：丹青圖書公司），15冊121頁，條目〈王叔文〉。

[W](../Category/唐朝官员.md "wikilink") [W](../Category/紹興人.md "wikilink")
[S](../Category/王姓.md "wikilink")

1.  《新唐书·卷一百六十八·列传第九十三》：叔文，北海人，自言猛之后，有远祖风，东平吕温、陇西李景俭、河东柳宗元以为信然。
2.  《读通鉴论》卷二十五“顺宗”
3.  《十七史商榷》卷七十四〈顺宗纪所书善政〉条