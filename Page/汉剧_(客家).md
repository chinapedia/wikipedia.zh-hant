**汉剧**（也称**广东汉剧**）是一种传统[客家](../Page/客家.md "wikilink")[戏剧](../Page/戏剧.md "wikilink")，也是[客家人喜闻乐见的艺术品种之一](../Page/客家人.md "wikilink")。该剧种主要流行于[粤东](../Page/粤东.md "wikilink")、[闽西](../Page/闽西.md "wikilink")、[赣南等地部分](../Page/赣南.md "wikilink")[客家地区](../Page/客家地区.md "wikilink")，演绎以[中州古调为准](../Page/中州.md "wikilink")，发音近似于现在的[普通话](../Page/普通话.md "wikilink")。

## 简介

汉剧，原名“**外江戏**”或“兴梅汉戏”，又称“乱弹”，在[晚清被诸多举人士绅崇为雅乐](../Page/晚清.md "wikilink")。

1933年，[汕头](../Page/汕头.md "wikilink")[公益国乐社出版社刊](../Page/公益国乐社.md "wikilink")《公益社乐剧月刊》，主要介绍外江戏剧目、[汉乐曲调](../Page/汉乐.md "wikilink")、[潮州音乐等相关资讯](../Page/潮州音乐.md "wikilink")\[1\]。作为刊物主编的[埔籍学者](../Page/大埔縣.md "wikilink")[钱热储](../Page/钱热储.md "wikilink")，在内文《汉剧提纲》中，提及此剧种似有创自[汉口之可能](../Page/汉口.md "wikilink")，遂倡议并将其定名作“汉剧”\[2\]。

1950年代末，漢劇又按分布地區不同，逐漸分成「廣東漢劇」和「[閩西漢劇](../Page/閩西漢劇.md "wikilink")」兩大類，領奏的[胡琴](../Page/胡琴.md "wikilink")（主弦）均為[頭弦](../Page/頭弦.md "wikilink")，都屬於板式變化豐富的[皮黃劇種](../Page/皮黃.md "wikilink")。

关于汉剧，其源流始祖说法不一，有称“来自[徽班](../Page/徽班.md "wikilink")”\[3\]、“源于[湖北汉剧](../Page/湖北汉剧.md "wikilink")”\[4\]、或是“秦戏班底”等等。但据现有资料记载和考证，当数与[湖南](../Page/湖南.md "wikilink")[祁阳戏](../Page/祁阳戏.md "wikilink")（亦称“楚南戏”）关系密切。

## 流传范围

  - [广东](../Page/广东.md "wikilink")[梅州](../Page/梅州.md "wikilink")（[梅县](../Page/梅县.md "wikilink")、[大埔](../Page/大埔县.md "wikilink")）
  - [福建](../Page/福建.md "wikilink")[龙岩](../Page/龙岩.md "wikilink")、[漳州](../Page/漳州.md "wikilink")（部分）、[三明](../Page/三明.md "wikilink")（部分）

## 经典剧目

  - 《百里奚认妻》
  - 《齐王求将》

## 知名演員

  - [李仙花](../Page/李仙花.md "wikilink")

## 臺灣客家大戲

在[臺灣](../Page/臺灣.md "wikilink")，當地的客家人在各種民間[廟會](../Page/廟會.md "wikilink")（如酬神會、[中元節](../Page/中元節.md "wikilink")、甚至年底的收冬祭等等）裏，都會上演「客家大戲」。這種「大戲」常以「四縣客語」（[四縣腔](../Page/客語.md "wikilink")）來發音演繹，不僅與[中國大陸的傳統客家漢劇有明顯差異](../Page/中國大陸.md "wikilink")，連使用的語言和曲調都有所不同。

從演出題材和內容方面看，台灣的客家戲曲主要以「歷史故事」或「民間傳說」為背景，借以宣揚「忠、孝、節、義」四項為本、「勤儉」之大傳統為輔，對下一代進行道德教化，保持社會風氣之優良。

## 参考资料

<references/>

[Category:戏曲剧种](../Category/戏曲剧种.md "wikilink")
[Category:福建戏曲](../Category/福建戏曲.md "wikilink")
[Category:广东戏曲](../Category/广东戏曲.md "wikilink")
[Category:中国非物质文化遗产](../Category/中国非物质文化遗产.md "wikilink")
[Category:客家音樂](../Category/客家音樂.md "wikilink")

1.
2.
3.
4.