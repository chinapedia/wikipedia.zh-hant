[Dimoffree.svg](https://zh.wikipedia.org/wiki/File:Dimoffree.svg "fig:Dimoffree.svg")是一點，[1維是線](../Page/一維空間.md "wikilink")，[2维是一個長和寬](../Page/二維空間.md "wikilink")（或曲線）[面積](../Page/面積.md "wikilink")，[3维是](../Page/三維空間.md "wikilink")2维加上高度形成**體積面**，[4維則是建立在](../Page/四維空間.md "wikilink")3維之上又多一個參數\]\]
**维度**，又稱**维数**，是[数学中](../Page/数学.md "wikilink")[独立参数的数目](../Page/参数.md "wikilink")。在[物理学和](../Page/物理学.md "wikilink")[哲学的领域内](../Page/哲学.md "wikilink")，指独立的时空坐标的数目。

0维是一點，沒有長度。1维是線，只有長度。2维是一個平面，是由長度和寬度（或曲線）形成[面積](../Page/面積.md "wikilink")。3维是2维加上高度形成「體積面」。雖然在一般人中習慣了整數维，但在[碎形中維度不一定是](../Page/碎形.md "wikilink")[整數](../Page/整數.md "wikilink")，可能会是一个非整的[有理数或者](../Page/有理数.md "wikilink")[无理数](../Page/无理数.md "wikilink")。

我们周围的[空间有](../Page/空间.md "wikilink")3个维（上下、前后、左右）。我們可以往上下、東南西北移動，其他方向的移動只需用3個三维空間軸來表示。向下移就等於負方向地向上移，向西北移就只是向西和向北移的混合。

在[物理學上](../Page/物理學.md "wikilink")[時間是第四维](../Page/時間.md "wikilink")，與三個空間维不同的是，它只有一個，且只能往一方向前進。

我们所居於的[时空有四个维](../Page/时空.md "wikilink")（3个空间轴和1个时间轴），根據[愛因斯坦的概念稱為](../Page/愛因斯坦.md "wikilink")[四维时空](../Page/四维时空.md "wikilink")，我們的宇宙是由[時间和](../Page/時间.md "wikilink")[空间構成](../Page/空间.md "wikilink")，而這條時間軸是一條虛數值的軸。

**[弦理論](../Page/弦理論.md "wikilink")**認為我們所居於的[宇宙實際上有更多的維度](../Page/宇宙.md "wikilink")（通常10、11或24個）。但是這些附加的维度所量度的是次原子大小的宇宙。

维度是[理论模型](../Page/理论模型.md "wikilink")，在[非古典物理学中这点更为明显](../Page/非古典物理学.md "wikilink")。所以不用计较[宇宙的维数是多少](../Page/宇宙.md "wikilink")，只要方便描述就行了。

在[物理學中](../Page/物理.md "wikilink")，質的量纲通常以質的基本單位表示：例如，[速率的量纲就是](../Page/速率.md "wikilink")[長度除以時間](../Page/長度.md "wikilink")。

## 物理學上的維度

## 參見

  - [相對論](../Page/相對論.md "wikilink")
  - [四维空间](../Page/四维空间.md "wikilink")
  - [閔可夫斯基時空](../Page/閔可夫斯基時空.md "wikilink")
  - [空間](../Page/空間.md "wikilink")

[ku:Rehend](../Page/ku:Rehend.md "wikilink")
[nl:Dimensie](../Page/nl:Dimensie.md "wikilink")
[ro:Dimensiune](../Page/ro:Dimensiune.md "wikilink")
[sk:Rozmer](../Page/sk:Rozmer.md "wikilink")
[sl:Razsežnost](../Page/sl:Razsežnost.md "wikilink")
[sq:Përmasa](../Page/sq:Përmasa.md "wikilink") [ur:بُعد (لکیری
الجبرا)](../Page/ur:بُعد_\(لکیری_الجبرا\).md "wikilink")

[W](../Category/維度.md "wikilink")