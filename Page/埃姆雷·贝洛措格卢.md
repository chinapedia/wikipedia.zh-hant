**安利·比路蘇古魯**（，），通常簡稱為**安利**（**Emre**），是一名[土耳其足球員](../Page/土耳其.md "wikilink")，現時效力土超[巴沙克舒希](../Page/巴沙克舒希.md "wikilink")。2004年，安利獲[巴西著名球星](../Page/巴西.md "wikilink")[比利選入](../Page/比利.md "wikilink")[在世的最偉大的125名球員名單之中](../Page/FIFA_100.md "wikilink")。

## 生平

安利16歲已替土耳其球隊[加拉塔沙雷上陣](../Page/加拉塔沙雷.md "wikilink")，翌年隨即成為球隊的正選。

2001年，安利以1,000萬[英鎊轉會費加盟](../Page/英鎊.md "wikilink")[國際米蘭](../Page/國際米蘭.md "wikilink")。由於他擅於組織攻擊，腳法出眾，被球迷稱為「[博斯普魯斯](../Page/博斯普魯斯海峽.md "wikilink")[馬勒當拿](../Page/馬勒當拿.md "wikilink")」。2002年，安利憑著出色的表現帶領[土耳其成為](../Page/土耳其國家足球隊.md "wikilink")[2002年世界盃第三名](../Page/2002年世界盃.md "wikilink")。由於在國際米蘭被投閒置散，上陣機會不多，加上有傳與領隊[文仙尼不和](../Page/羅拔圖·文仙尼.md "wikilink")，不少球隊如[愛華頓和](../Page/愛華頓.md "wikilink")[紐卡素都有興趣羅致](../Page/紐卡素足球會.md "wikilink")\[1\]。

2005年7月14日，安利以380萬[英鎊從](../Page/英鎊.md "wikilink")[國際米蘭轉會至](../Page/國際米蘭.md "wikilink")[紐卡素](../Page/紐卡素足球會.md "wikilink")\[2\]。由於紐卡素戰績不濟，不斷更換領隊，在2007年夏季上任的[萨姆·阿勒代斯已是安利加盟以來的第三名領隊](../Page/萨姆·阿勒代斯.md "wikilink")，有傳聞安利將返回[土耳其加盟母會加拉塔沙雷或對頭](../Page/土耳其.md "wikilink")[費倫巴治](../Page/费内巴切体育俱乐部.md "wikilink")，但安利選擇留隊爭取正選席位\[3\]。2008年夏季，安利終於重返土耳其，加盟費倫巴治，簽約3年\[4\]。

## 榮譽

  - **[加拉塔沙雷](../Page/加拉塔萨雷体育俱乐部.md "wikilink")**

      - [欧洲超级杯](../Page/欧洲超级杯.md "wikilink")（1）：2000年；
      - [歐洲足協盃](../Page/歐洲足協盃.md "wikilink")（1）：2000年；

  - **[土耳其](../Page/土耳其國家足球隊.md "wikilink")**

      - [2002年韓日世界盃](../Page/2002年世界盃足球賽.md "wikilink")：**季軍**

  - **[國際米蘭](../Page/国际米兰足球俱乐部.md "wikilink")**

      - [意大利盃](../Page/意大利盃.md "wikilink")（1）：2005年；

  - **[紐卡素](../Page/纽卡斯尔联足球俱乐部.md "wikilink")**

      - [歐洲足協圖圖盃](../Page/歐洲足協圖圖盃.md "wikilink")（1）：2006年；

## 参考文献

## 外部連結

  - [Soccerbase
    安利資料](http://www.soccerbase.com/players_details.sd?playerid=17904)
  - [UEFA.com 歐洲足協
    安利資料](http://www.uefa.com/competitions/euro/players/player=30996/index.html)
  - [香港紐卡素球迷會
    安利資料](https://web.archive.org/web/20111129140338/http://www.hknewcastle.com/mainpage.php?page=playerinfo&id=14)
  - [123 Football
    Stats](http://www.123football.com/players/b/emre-belozoglu/index.htm)

[Category:土耳其足球運動員](../Category/土耳其足球運動員.md "wikilink")
[Category:足球中場](../Category/足球中場.md "wikilink")
[Category:加拉塔沙雷球員](../Category/加拉塔沙雷球員.md "wikilink")
[Category:國際米蘭球員](../Category/國際米蘭球員.md "wikilink")
[Category:紐卡素球員](../Category/紐卡素球員.md "wikilink")
[Category:費倫巴治球員](../Category/費倫巴治球員.md "wikilink")
[Category:馬德里體育會球員](../Category/馬德里體育會球員.md "wikilink")
[Category:FIFA 100](../Category/FIFA_100.md "wikilink")
[Category:土超球員](../Category/土超球員.md "wikilink")
[Category:意甲球員](../Category/意甲球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:西甲球員](../Category/西甲球員.md "wikilink")
[Category:義大利外籍足球運動員](../Category/義大利外籍足球運動員.md "wikilink")
[Category:英格蘭外籍足球運動員](../Category/英格蘭外籍足球運動員.md "wikilink")
[Category:西班牙外籍足球運動員](../Category/西班牙外籍足球運動員.md "wikilink")
[Category:2002年世界盃足球賽球員](../Category/2002年世界盃足球賽球員.md "wikilink")
[Category:2008年歐洲國家盃球員](../Category/2008年歐洲國家盃球員.md "wikilink")
[Category:巴沙克舒希球員](../Category/巴沙克舒希球員.md "wikilink")
[Category:阿爾巴尼亞裔土耳其人](../Category/阿爾巴尼亞裔土耳其人.md "wikilink")

1.
2.
3.  [Emre commits future to
    Newcastle](http://news.bbc.co.uk/sport2/hi/football/teams/n/newcastle_united/6237206.stm)
4.  [Emre 'deal done', say
    Fenerbahce](http://news.bbc.co.uk/sport2/hi/football/teams/n/newcastle_united/7429144.stm)