[Languedoc-Roussillon_map.png](https://zh.wikipedia.org/wiki/File:Languedoc-Roussillon_map.png "fig:Languedoc-Roussillon_map.png")
**朗格多克-鲁西永**（、）是[法国南部一個](../Page/法国.md "wikilink")[大區](../Page/大區.md "wikilink")，别称为“法国南部”（）。该大区南鄰[加泰羅尼亞與](../Page/加泰羅尼亞.md "wikilink")[地中海](../Page/地中海.md "wikilink")。面积27,376km²，人口2,295,648。共有[奧德省](../Page/奧德省.md "wikilink")、[加爾省](../Page/加爾省.md "wikilink")、[埃羅省](../Page/埃羅省.md "wikilink")、[洛澤爾省及](../Page/洛澤爾省.md "wikilink")[東比利牛斯省](../Page/東比利牛斯省.md "wikilink")。

## 城堡

[France-Chateau_de_Queribus-Vue_d'ensemble-2005-08-05.jpg](https://zh.wikipedia.org/wiki/File:France-Chateau_de_Queribus-Vue_d'ensemble-2005-08-05.jpg "fig:France-Chateau_de_Queribus-Vue_d'ensemble-2005-08-05.jpg")\]\]

  - **[奥德省](../Page/奥德省.md "wikilink") (11)**

<!-- end list -->

  -
    [Aguilar](../Page/Château_d'Aguilar.md "wikilink"){{·}} [Tour
    Barberousse (Gruissan)](../Page/Tour_Barberousse.md "wikilink"){{·}}
    [Durban-Corbières](../Page/Château_de_Durban-Corbières.md "wikilink"){{·}}
    [Lastours](../Page/Châteaux_de_Lastours.md "wikilink"){{·}}
    [Peyrepertuse](../Page/Peyrepertuse.md "wikilink"){{·}}
    [Pieusse](../Page/Château_de_Pieusse.md "wikilink"){{·}}
    [Puilaurens](../Page/Puilaurens.md "wikilink"){{·}}
    [Puivert](../Page/Château_de_Puivert.md "wikilink"){{·}}
    [Quéribus](../Page/Château_de_Quéribus.md "wikilink"){{·}}
    [Saissac](../Page/Château_de_Saissac.md "wikilink"){{·}}
    [Termes](../Page/Château_de_Termes.md "wikilink")

<!-- end list -->

  - **[加尔省](../Page/加尔省.md "wikilink")(30)**

<!-- end list -->

  -
    [Portes](../Page/Château_de_Portes.md "wikilink"){{·}}
    [Uzès](../Page/Uzès.md "wikilink")

<!-- end list -->

  - **[埃罗省](../Page/埃罗省.md "wikilink")(34)**

<!-- end list -->

  -
    [Castles in Hérault](../Page/Castles_in_Hérault.md "wikilink")
    <small>\[This article includes Agel, Aigues-Vives, Aspiran de
    Ravanès (Thézan-Lès-Béziers), Aumelas, Autignac, Bélarga,
    Cabrerolles, Cabrières, Castelnau-de-Guers, Creissan, Cruzy, Dio,
    Faugères, Fos, Grézan (Laurens), Malavieille, Montouliers,
    Mourcairol, Pézènes-les-Mines, Puisserguier, Roquessels,
    Saint-Bauléry (Cébazan)\]</small> {{·}} [Guilhem
    (Clermont-l'Hérault)](../Page/Château_des_Guilhem.md "wikilink"){{·}}
    [Pézenas](../Page/Château_de_Pézenas.md "wikilink")

<!-- end list -->

  - **[洛泽尔省](../Page/洛泽尔省.md "wikilink")(48)**

<!-- end list -->

  -
    [Florac](../Page/Château_de_Florac.md "wikilink")

<!-- end list -->

  - **[东比利牛斯省](../Page/东比利牛斯省.md "wikilink")(66)**

<!-- end list -->

  -
    [Castelnou](../Page/Château_de_Castelnou.md "wikilink"){{·}} Château
    Royal de [Collioure](../Page/Collioure.md "wikilink"){{·}} [Fort de
    Salses](../Page/Fort_de_Salses.md "wikilink")

<small>（[→Top](../Page/#top.md "wikilink")）</small> {{-}}

## 参考文献

{{-}}

[Category:奧克西塔尼大區](../Category/奧克西塔尼大區.md "wikilink")
[Category:法國已撤銷的大區](../Category/法國已撤銷的大區.md "wikilink")