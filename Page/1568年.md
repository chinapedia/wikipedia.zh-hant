## 大事记

### 中国

  - [辽西走廊发生大地震](../Page/辽西走廊.md "wikilink")。
  - [李春芳出任](../Page/李春芳.md "wikilink")[明朝首辅](../Page/明朝首辅列表.md "wikilink")。
  - [5月15日](../Page/5月15日.md "wikilink")——[西安发生](../Page/西安市.md "wikilink")7级地震。

### 日本

  - [德川家康入侵](../Page/德川家康.md "wikilink")[今川氏真的領土](../Page/今川氏真.md "wikilink")，今川氏滅亡。
  - [蒲生賢秀归顺織田信長](../Page/蒲生賢秀.md "wikilink")。
  - [2月](../Page/2月.md "wikilink")——[足利義榮為](../Page/足利義榮.md "wikilink")[松永久秀等人擁立為](../Page/松永久秀.md "wikilink")[室町幕府第十四代將軍](../Page/室町幕府.md "wikilink")。
  - [9月](../Page/9月.md "wikilink")——[織田信長进入](../Page/織田信長.md "wikilink")[京都](../Page/京都.md "wikilink")，擁護[足利義昭為](../Page/足利義昭.md "wikilink")[室町幕府第十五代](../Page/室町幕府.md "wikilink")（末代）[征夷大將軍](../Page/征夷大將軍.md "wikilink")。

### 欧洲

  - [奧圖曼帝國](../Page/奧圖曼帝國.md "wikilink")[大維齊爾](../Page/大維齊爾.md "wikilink")[索庫魯·穆罕默德帕夏建議](../Page/索庫魯·穆罕默德帕夏.md "wikilink")[塞利姆二世在](../Page/塞利姆二世.md "wikilink")[伏爾加河與](../Page/伏爾加河.md "wikilink")[頓河之間掘一條](../Page/頓河.md "wikilink")[運河支援](../Page/運河.md "wikilink")[中亞的](../Page/中亞.md "wikilink")[突厥人對抗](../Page/突厥人.md "wikilink")[俄羅斯沙皇國](../Page/俄羅斯沙皇國.md "wikilink")，爆發[俄土戰爭](../Page/俄土戰爭（1568至1570年）.md "wikilink")。
  - 《[威廉·凡·那叟](../Page/威廉·凡·那叟.md "wikilink")》被指定为[荷兰国歌](../Page/荷兰国歌.md "wikilink")。
  - [瑞典国王](../Page/瑞典.md "wikilink")[埃里克十四世被议会废黜](../Page/埃里克十四世.md "wikilink")，[约翰三世成为瑞典国王](../Page/约翰三世_\(瑞典\).md "wikilink")。
  - [波格丹四世登基成为摩尔多瓦统治者](../Page/波格丹四世.md "wikilink")。
  - [葡萄牙国王](../Page/葡萄牙.md "wikilink")[塞巴斯蒂昂开始亲政](../Page/塞巴斯蒂昂_\(葡萄牙\).md "wikilink")。
  - [爱尔兰爆发反对](../Page/爱尔兰.md "wikilink")[英国的起义](../Page/英国.md "wikilink")（直[1573年被扑灭](../Page/1573年.md "wikilink")）。
  - [隆朱莫条约条约](../Page/隆朱莫条约.md "wikilink")：[法国第二次宗教战争结束](../Page/胡格诺战争.md "wikilink")。
  - [土达法令](../Page/土达法令.md "wikilink")：个人宗教信仰自由。
  - [路德维希继位为](../Page/路德维希_\(符腾堡\).md "wikilink")[符腾堡公爵](../Page/符腾堡.md "wikilink")。
  - [尤利乌斯继任为不伦瑞克](../Page/尤利乌斯_\(不伦瑞克-沃尔芬比特尔\).md "wikilink")-沃尔芬比特尔公爵。
  - [西班牙航海家发现](../Page/西班牙.md "wikilink")[圖瓦盧和](../Page/圖瓦盧.md "wikilink")[威克島](../Page/威克島.md "wikilink")。西班牙航海探险家[阿尔瓦罗·德·门达尼亚到达](../Page/阿尔瓦罗·德·门达尼亚.md "wikilink")[所罗门群岛](../Page/所罗门群岛.md "wikilink")。
  - 荷蘭製圖學者[墨卡托創立了](../Page/墨卡托.md "wikilink")[正軸等角圓柱投影](../Page/正軸等角圓柱投影.md "wikilink")。
  - [2月17日](../Page/2月17日.md "wikilink")——[神圣罗马帝国与](../Page/神圣罗马帝国.md "wikilink")[鄂图曼帝国之间签订和平条约](../Page/奥斯曼帝国.md "wikilink")。
  - [5月19日](../Page/5月19日.md "wikilink")——[英格兰的](../Page/英格兰.md "wikilink")[伊丽莎白一世女王将其表妹](../Page/伊丽莎白一世_\(英格兰\).md "wikilink")，[苏格兰女王](../Page/蘇格蘭.md "wikilink")[玛丽一世投入监狱](../Page/玛丽一世_\(苏格兰\).md "wikilink")。
  - [5月23日](../Page/5月23日.md "wikilink")——[荷兰争取从](../Page/荷兰.md "wikilink")[西班牙独立的](../Page/西班牙.md "wikilink")[八十年战争开始](../Page/八十年戰爭.md "wikilink")。

## 出生

  - [魏忠贤](../Page/魏忠贤.md "wikilink")，明朝宦官（逝世于[1627年](../Page/1627年.md "wikilink")）
  - [官應震](../Page/官應震.md "wikilink")，明朝大臣（逝世于[1635年](../Page/1635年.md "wikilink")）
  - [豐臣秀次](../Page/豐臣秀次.md "wikilink")，日本战国时代将军（逝世于[1595年](../Page/1595年.md "wikilink")）
  - [羽柴秀勝](../Page/羽柴秀勝.md "wikilink")，日本武将（逝世于[1586年](../Page/1586年.md "wikilink")）
  - [石川五右衛門](../Page/石川五右衛門.md "wikilink")，日本大盗（约出生于1568年，逝世于[1594年](../Page/1594年.md "wikilink")）
  - [袁宏道](../Page/三袁#袁宏道.md "wikilink")，明朝文人（逝世于[1610年](../Page/1610年.md "wikilink")）
  - [曾鲸](../Page/曾鲸.md "wikilink")，明朝画家（逝世于[1650年](../Page/1650年.md "wikilink")）
  - [吉田政重](../Page/吉田政重.md "wikilink")，日本家臣（逝世于[1628年](../Page/1628年.md "wikilink")）
  - 1月——[立花成家](../Page/立花成家.md "wikilink")，日本贵族（约出生于1568年，逝世于[1620年](../Page/1620年.md "wikilink")）
  - [4月5日](../Page/4月5日.md "wikilink")——[乌尔巴诺八世](../Page/乌尔巴诺八世.md "wikilink")，教宗（逝世于[1644年](../Page/1644年.md "wikilink")）
  - [12月21日](../Page/12月21日.md "wikilink")——[黑田長政](../Page/黑田長政.md "wikilink")，日本武将（逝世于[1622年](../Page/1622年.md "wikilink")）

## 逝世

  - [亚历山德鲁·勒普什内亚努](../Page/亚历山德鲁·勒普什内亚努.md "wikilink")，摩尔多瓦统治者
  - [李開先](../Page/李開先.md "wikilink")，明朝作家（出生于[1502年](../Page/1502年.md "wikilink")）
  - [中條藤資](../Page/中條藤資.md "wikilink")，日本家臣（出生年代不明）
  - [壽桂尼](../Page/壽桂尼.md "wikilink")，日本大名今川氏親的正室（出生时间不详）
  - [2月7日](../Page/2月7日.md "wikilink")——[色部勝長](../Page/色部勝長.md "wikilink")，日本家臣（出生于[1493年](../Page/1493年.md "wikilink")）
  - [6月11日](../Page/6月11日.md "wikilink")——[亨利二世](../Page/亨利二世_\(不伦瑞克-沃尔芬比特尔\).md "wikilink")，不伦瑞克-沃尔芬比特尔公爵（出生于[1489年](../Page/1489年.md "wikilink")）
  - [12月28日](../Page/12月28日.md "wikilink")——[克里斯托夫](../Page/克里斯托夫_\(符腾堡\).md "wikilink")，符腾堡公爵（出生于[1515年](../Page/1515年.md "wikilink")）

[\*](../Category/1568年.md "wikilink")
[8年](../Category/1560年代.md "wikilink")
[6](../Category/16世纪各年.md "wikilink")