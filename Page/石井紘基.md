**石井紘基**（）[日本](../Page/日本.md "wikilink")[民主黨所屬的](../Page/民主黨.md "wikilink")[眾議院議員](../Page/眾議院.md "wikilink")。

## 經歷

**石井紘基**出生於[東京都](../Page/東京都.md "wikilink")[世田谷區](../Page/世田谷區.md "wikilink")。[池之上小學](../Page/池之上小學.md "wikilink")，[成城學園中學](../Page/成城學園.md "wikilink")，高中，[日本中央大學法學院](../Page/日本中央大學.md "wikilink")，[早稻田大學研究所](../Page/早稻田大學.md "wikilink")，[莫斯科大學獲得哲學博士學位](../Page/莫斯科大學.md "wikilink")，妻子娜塔莎是[俄羅斯人](../Page/俄羅斯.md "wikilink")。
[社會民主聯合事務局長](../Page/社會民主聯合.md "wikilink")，1992年秋天加入[日本新黨](../Page/日本新黨.md "wikilink")。1993年是以當時「日本新黨」黨籍身分首次當選眾議員。之後，連任當選。1996年，參加民主黨的黨團議員活動，主要是[日本政府支出浪費使用一事](../Page/日本政府.md "wikilink")。2000年，[電影](../Page/電影.md "wikilink")「[大逃殺](../Page/大逃殺.md "wikilink")」一事，以對青少年造成不良影響，反對放映。2002年10月25日，在世田谷區自己的住宅停車場被[右翼團體](../Page/右翼團體.md "wikilink")「[守皇塾](../Page/守皇塾.md "wikilink")」代表[伊藤白水刺殺身亡](../Page/伊藤白水.md "wikilink")，伊籐白水用柳刀刃菜刀刺左邊胸部死亡。享年62歲
。

## 著作

  - 「相連的話：對政治改革的我直言」（創樹社，1988年12月）
  - 「官僚天堂日本破產 」（道出版，1996年4月） ISBN 4-7901-0130-4
  - 「檢舉漫畫特權列島：切斷援助交際政治現場（文藝春秋，1999年10月）交政治的現場」ISBN 4-89036-090-5
  - 「吃竭盡日本的寄生蟲：完全廢除特殊法人．公益法人 \!」 （道出版，2001年11月）ISBN 4-944154-40-2
  - 「日本自然滅亡之日：「官制經濟體制」吃光國民的錢 \!」 （PHP 研究所，2002年1月）ISBN 4-569-61414-0
  - 「誰也不知道的日本國家的幕後帳簿 : 消滅國家的特權財政的實況 \!」 （道出版，2002年2月）ISBN 4-944154-41-0

## 參考文獻

  - 『繼承政治家石井紘基那個遺志』（明石書店、2003年10月）ISBN 4-7503-1804-3

## 外部連結

  - [已故眾議院議員石井紘基事件的真相調查](https://web.archive.org/web/20061215205132/http://homepage1.nifty.com/kito/ishii/index.htm)

[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")
[Category:遇刺身亡的日本政治人物](../Category/遇刺身亡的日本政治人物.md "wikilink")
[Category:莫斯科国立大学校友](../Category/莫斯科国立大学校友.md "wikilink")
[Category:早稻田大學校友](../Category/早稻田大學校友.md "wikilink")
[Category:日本中央大學校友](../Category/日本中央大學校友.md "wikilink")
[Category:日本新黨黨員](../Category/日本新黨黨員.md "wikilink")
[Category:日本民主黨 (1998年)黨員](../Category/日本民主黨_\(1998年\)黨員.md "wikilink")
[Category:日本眾議院議員
1993–1996](../Category/日本眾議院議員_1993–1996.md "wikilink")
[Category:日本眾議院議員
1996–2000](../Category/日本眾議院議員_1996–2000.md "wikilink")
[Category:日本眾議院議員
2000–2003](../Category/日本眾議院議員_2000–2003.md "wikilink")
[Category:東京都選出日本眾議院議員](../Category/東京都選出日本眾議院議員.md "wikilink")
[Category:東京比例代表區選出日本眾議院議員](../Category/東京比例代表區選出日本眾議院議員.md "wikilink")