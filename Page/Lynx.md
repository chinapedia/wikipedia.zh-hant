**Lynx**是個純文字[網頁瀏覽器](../Page/網頁瀏覽器.md "wikilink")，是在具有高亮文字功能的[終端上使用的](../Page/終端.md "wikilink")。它最早开始于1992年，是目前被使用和開發的历史最悠久的網頁瀏覽器。

## 歷史

Lynx是[堪薩斯大學學術計算服務部門內分佈式計算小組的產品](../Page/堪薩斯大學.md "wikilink")，最初是由該大學的一組學生團隊和工作人員（[Lou
Montulli](../Page/Lou_Montulli.md "wikilink")、Michael Grobec和Charles
Rezac）於1992年開發的一種[超文字瀏覽器](../Page/超文字.md "wikilink")，僅用於分發校園訊息，作為整個校園訊息服務器的一部分並用於瀏覽[Gopher空間](../Page/Gopher_\(网络协议\).md "wikilink")。1992年7月22日於[Usenet公佈了測試版本](../Page/Usenet.md "wikilink")。1993年，Montulli增加了一個網際網路介面，並發布瀏覽器的新版本。

截至2007年7月，Lynx中的[網路傳輸協定支援是使用](../Page/網路傳輸協定.md "wikilink")[Libwww的一個版本實作的](../Page/Libwww.md "wikilink")，1996年從函式庫的代碼庫中[分支出來](../Page/複刻.md "wikilink")。支持的協定包括[Gopher](../Page/Gopher.md "wikilink")、[HTTP](../Page/HTTP.md "wikilink")、[HTTPS](../Page/HTTPS.md "wikilink")、[FTP](../Page/FTP.md "wikilink")、[NNTP和](../Page/NNTP.md "wikilink")[WAIS](../Page/WAIS.md "wikilink")。在1994年進行開發中的Lynx為Libwww增加了NNTP支援。後來Lynx的Libwww分支增加了HTTPS支援，最初是關於加密問題的修補程式。

Garrett Blythe於1994年4月建立了DosLynx，後來也加入Lynx團隊。接著Foteos
Macrides將Lynx移植到[VMS系統](../Page/OpenVMS.md "wikilink")，並且維持了這個軟體一段時間。1995年，Lynx在[GPL下發佈](../Page/GPL.md "wikilink")，自此至今由一群志願者維護。

## 特點

[Lynx_vs._Firefox_rendering.png](https://zh.wikipedia.org/wiki/File:Lynx_vs._Firefox_rendering.png "fig:Lynx_vs._Firefox_rendering.png")呈現同一網頁\]\]
Lynx瀏覽方式包括以方向鍵選擇[超連結](../Page/超連結.md "wikilink")，而Lynx會將已選擇的超連結高亮顯示，或者由Lynx先將網頁上所有超連結都編號，再輸入號碼選擇超連結。目前版本的Lynx支援[SSL](../Page/传输层安全.md "wikilink")和支援不少[HTML功能](../Page/HTML.md "wikilink")。表格是將各格內容排成一條線來呈現，而沒有呈現成表格結構。框架則是呈現成像是具有數個不同名字的分散網頁。Lynx不能在網路上顯示各種類型的非文字內容，例如圖像和影片，但它可以啟動外部程式來處理，如圖像檢視器或影片播放器。

與大多數網頁瀏覽器不同，Lynx不支持[JavaScript或](../Page/JavaScript.md "wikilink")[Adobe
Flash](../Page/Adobe_Flash.md "wikilink")，這是某些網站正常運作所必備的。

純文字瀏覽速度的優勢在使用低頻寬網際網路或使用較老舊的電腦硬體最為明顯，這些硬體在呈現大量圖像內容時可能速度較慢。

因為其介面適合[文字轉語音技術和](../Page/文字轉語音.md "wikilink")[點字顯示機](../Page/點字顯示機.md "wikilink")，Lynx一度很受視障者歡迎，但後來出現更好的[螢幕閱讀器減少了Lynx對](../Page/螢幕閱讀器.md "wikilink")[盲人的吸引力](../Page/盲人.md "wikilink")。

## 参考文献

### 引用

### 来源

  -
  -
  -
  -
  -
  -
## 外部链接

  - [官方網站](http://lynx.invisible-island.net/)

{{-}}

[Category:1992年軟體](../Category/1992年軟體.md "wikilink")
[Category:自由跨平台軟體](../Category/自由跨平台軟體.md "wikilink")
[Category:嵌入式Linux](../Category/嵌入式Linux.md "wikilink")
[Category:自由网页浏览器](../Category/自由网页浏览器.md "wikilink")
[Category:HTTP客戶端](../Category/HTTP客戶端.md "wikilink")
[Category:綠色軟體](../Category/綠色軟體.md "wikilink")
[Category:用C編程的自由軟體](../Category/用C編程的自由軟體.md "wikilink")