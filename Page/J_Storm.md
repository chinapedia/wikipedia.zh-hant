**J
Storm（）**是[日本的一間](../Page/日本.md "wikilink")[唱片公司](../Page/唱片公司.md "wikilink")，成立於2001年11月12日，當時是為了[嵐而成立的專屬品牌](../Page/嵐.md "wikilink")，目前則負責為[傑尼斯旗下組合](../Page/傑尼斯.md "wikilink")（包括[TOKIO](../Page/TOKIO.md "wikilink")、嵐、[KAT-TUN](../Page/KAT-TUN.md "wikilink")、[關西傑尼斯8及](../Page/關西傑尼斯8.md "wikilink")[Hey\!
Say\! JUMP](../Page/Hey!_Say!_JUMP.md "wikilink")）出版相關的影音產品。2006年3月，J
Storm為KAT-TUN出道而成立另一個品牌「**J-One
Records**」。目前社長是[藤島茱莉景子](../Page/藤島茱莉景子.md "wikilink")。除了為屬下的組合推出影音產品外，更會為旗下藝人拍攝[電影及](../Page/電影.md "wikilink")[電視劇](../Page/電視劇.md "wikilink")。

雖然影音產品是以J Storm或J-One
Records名義推出，但實質是由另一間唱片公司負責發售。在2006年7月前是由[東芝EMI負責發行](../Page/東芝EMI.md "wikilink")，現在是由[索尼音樂](../Page/日本索尼音樂娛樂.md "wikilink")（Sony
Music Distribution (Japan)
Inc.）負責發行；[台灣方面](../Page/台灣.md "wikilink")，2006年5月前是由[EMI代理](../Page/科藝百代.md "wikilink")，目前由[愛貝克思代理](../Page/愛貝克思_\(台灣\).md "wikilink")。

## 旗下組合

### J Storm

  - [TOKIO](../Page/TOKIO.md "wikilink") - 由第39張單曲『雨傘』開始。
  - [嵐](../Page/嵐.md "wikilink") - 由第7張單曲『a Day in Our Life』開始。
  - [KAT-TUN](../Page/KAT-TUN.md "wikilink")
  - [Hey\! Say\! JUMP](../Page/Hey!_Say!_JUMP.md "wikilink")
  - [關西傑尼斯8](../Page/關西傑尼斯8.md "wikilink") - 由第30張單曲『言ったじゃないか/CloveR』開始。

#### 限定企劃作品

  - 木更津Cat's Eye 日本系列 Original SoundTrack
  - 流星花園（日本版）Original SoundTrack
  - 木更津Cat's Eye feat. MCU -
  - [Hey\! Say\! 7](../Page/Hey!_Say!_JUMP.md "wikilink") - Hey\! Say\!

#### 影像作品

  - NEWS NIPPON 0304 - [NEWS](../Page/NEWS.md "wikilink")
  - SUMMARY of Johnnys World - NEWS・KAT-TUN
  - KAT-TUN Live 海賊帆 - KAT-TUN
  - [DREAM BOYS](../Page/DREAM_BOYS.md "wikilink") -
    KAT-TUN・[關西傑尼斯8](../Page/關西傑尼斯8.md "wikilink")
  - Cat in the Red Boots - 由[生田斗真主演的舞台影像作品](../Page/生田斗真.md "wikilink")。
  - JAILBREAKERS - 由[松岡昌宏主演的舞台影像作品](../Page/松岡昌宏.md "wikilink")。

### J-One Records

  - [KAT-TUN](../Page/KAT-TUN.md "wikilink")

### INFINITY RECORDS

  - [關西傑尼斯8](../Page/關西傑尼斯8.md "wikilink")

## 電影

### 嵐

  - [PIKA☆NCHI
    生活艱難但是快樂](../Page/PIKA☆NCHI_生活艱難但是快樂.md "wikilink")（2002年）
  - PIKA☆☆NCHI LIFE IS HARD 所以 HAPPY（2004年）
  - 黃色眼淚（2007年）
  - 電影
    [戰國英豪](../Page/戰國英豪.md "wikilink")（2008年）\*[松本潤主演](../Page/松本潤.md "wikilink")

### V6

  - COSMIC RESCUE（2003年）\*Coming Century主演
  - Hold Up Down（2005年）

### 其他組合

  - Fantastipo（2005年）\*[堂本剛及](../Page/堂本剛.md "wikilink")[國分太一主演](../Page/國分太一.md "wikilink")
  - 電影
    [詐欺獵人](../Page/詐欺獵人.md "wikilink")（2008年）\*[山下智久主演](../Page/山下智久.md "wikilink")

## 相關項目

  - [傑尼斯事務所](../Page/傑尼斯事務所.md "wikilink")
  - [傑尼斯娛樂](../Page/傑尼斯娛樂.md "wikilink")
  - [TOKIO](../Page/TOKIO.md "wikilink")
  - [嵐](../Page/嵐.md "wikilink")
  - [KAT-TUN](../Page/KAT-TUN.md "wikilink")
  - [Hey\! Say\! JUMP](../Page/Hey!_Say!_JUMP.md "wikilink")
  - [關西傑尼斯8](../Page/關西傑尼斯8.md "wikilink")

[J](../Category/傑尼斯事務所相關組織.md "wikilink")
[Category:日本唱片公司](../Category/日本唱片公司.md "wikilink")