**哈羅德·布魯姆**（，），美國[文學評論家](../Page/文學評論.md "wikilink")，著有《[西方正典：伟大作家和不朽作品](../Page/西方正典.md "wikilink")》。

## 生平

1930年出生於[紐約](../Page/紐約.md "wikilink")，受[哈特·克莱恩](../Page/哈特·克莱恩.md "wikilink")、[威廉·巴特勒·叶芝](../Page/威廉·巴特勒·叶芝.md "wikilink")、[威廉·布萊克等人的作品影響](../Page/威廉·布萊克.md "wikilink")。就讀於[康奈爾大學和](../Page/康奈爾大學.md "wikilink")[耶魯大學](../Page/耶魯大學.md "wikilink")，畢業後在耶魯大學任教，早期研究浪漫主义诗歌，出版布莱克、雪莱、叶芝、斯蒂文斯等英美诗人傳記，強調「修正式批評理論」，是美國“耶魯學派”批評家。1973年推出《[影响的焦虑](../Page/影响的焦虑.md "wikilink")》。1979年与[保罗·德曼](../Page/保罗·德曼.md "wikilink")、杰弗里·哈特曼（Geoffrey
Hartman）、希利斯·米勒（J.Hillis
Miller）合作出版《解构和批评》。又與[史景迁友好](../Page/史景迁.md "wikilink")。

1994年出版《西方正典》，重新評比26位西方文學家，引起轩然大波。2005年4月2日获得[安徒生文学奖](../Page/安徒生文学奖.md "wikilink")。最近他語出驚人的表示，《[哈利波特](../Page/哈利波特.md "wikilink")》作者[J·K·羅琳是蹩腳作家](../Page/J·K·羅琳.md "wikilink")\[1\]。作品還有《误读之图》、《卡巴拉犹太神秘哲学与批评》、《诗与隐抑》等。

## 参考资料

[Category:美國文學評論家](../Category/美國文學評論家.md "wikilink")
[Category:康乃爾大學校友](../Category/康乃爾大學校友.md "wikilink")
[Category:耶魯大學校友](../Category/耶魯大學校友.md "wikilink")
[Category:劍橋大學彭布羅克學院校友](../Category/劍橋大學彭布羅克學院校友.md "wikilink")
[Category:古根海姆学者](../Category/古根海姆学者.md "wikilink")
[Category:猶太學者](../Category/猶太學者.md "wikilink")
[Category:麦克阿瑟学者](../Category/麦克阿瑟学者.md "wikilink")
[Category:紐約大學教師](../Category/紐約大學教師.md "wikilink")
[Category:布朗克斯人](../Category/布朗克斯人.md "wikilink")
[Category:耶鲁大学教师](../Category/耶鲁大学教师.md "wikilink")

1.  [布鲁姆：罗琳是蹩脚作家](http://www.artcn.cn/art/zgwh/whxw/200505/2008.html)