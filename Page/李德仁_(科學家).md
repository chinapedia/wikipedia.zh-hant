**李德仁**（），[中国摄影测量与](../Page/中国.md "wikilink")[遥感学家](../Page/遥感学.md "wikilink")。出生于[江苏](../Page/江苏.md "wikilink")[泰县](../Page/泰县_\(江苏县份\).md "wikilink")。籍贯江苏镇江[丹徒](../Page/丹徒.md "wikilink")。1963年毕业于[武汉测绘学院航测系](../Page/武汉测绘学院.md "wikilink")，1981年获摄影测量与遥感专业硕士学位。1985年获德国[斯图加特大学博士学位](../Page/斯图加特大学.md "wikilink")。1991年当选为[中国科学院学部委员](../Page/中国科学院学部委员.md "wikilink")(院士)。1994年被选聘为中国工程院院士。武汉大学教授，测绘遥感信息工程国家重点实验室主任。曾任武汉测绘科技大学校长、中国测绘学会理事长。\[1\]

## 履歷

1957年畢業於中國江蘇省[泰州中學](../Page/泰州中學.md "wikilink")，1963年毕业于[武汉测绘学院](../Page/武汉测绘学院.md "wikilink")[航空摄影测量系](../Page/航空摄影测量系.md "wikilink")，1981年获同校硕士学位，曾先後師從[王之卓](../Page/王之卓.md "wikilink")[院士](../Page/院士.md "wikilink")，[陈永龄院士](../Page/陈永龄.md "wikilink")。次年，他世界首创从[验后方差估计导出粗差定位的](../Page/验后方差.md "wikilink")[选权迭代法](../Page/选权迭代法.md "wikilink")，後來被世界测量学界称为“[李德仁方法](../Page/李德仁方法.md "wikilink")”，現在世界上所有航空測量都會使用該方法來調整測量誤差。同年到西德（[聯邦德國](../Page/聯邦德國.md "wikilink")）的[斯图加特大学師從](../Page/斯图加特大学.md "wikilink")[阿克曼](../Page/阿克曼.md "wikilink")[教授深造](../Page/教授.md "wikilink")，三年後获得摄影测量与遥感专业博士学位，他的博士论文保持着该校的最高得分记录至今，大地测量学家[格拉法韧特等對該論文高度評價](../Page/格拉法韧特.md "wikilink")，认为该论文解决了测量学上的百年难题。李德仁為中國大陸不多的並且是最年輕的[雙院士](../Page/雙院士.md "wikilink")（[中国科学院院士](../Page/中国科学院.md "wikilink")（[学部委员](../Page/学部委员.md "wikilink")，1991年），[中国工程院院士](../Page/中国工程院.md "wikilink")（1994年）），[国际欧亚科学院院士](../Page/国际欧亚科学院.md "wikilink")，[中國国家级有突出贡献的中青年专家](../Page/中國国家级有突出贡献的中青年专家.md "wikilink")，原任[武漢測繪科技大學校長](../Page/武漢測繪科技大學.md "wikilink")，現任[武汉大学教授和](../Page/武汉大学.md "wikilink")[博士生導師](../Page/博士生導師.md "wikilink")，[测绘遥感信息工程国家重点实验室主任](../Page/测绘遥感信息工程国家重点实验室.md "wikilink")，[中國国家重点学科](../Page/中國国家重点学科.md "wikilink")[摄影测量与遥感学科的](../Page/摄影测量与遥感.md "wikilink")[学术带头人](../Page/学术带头人.md "wikilink")。李德仁是歷届[全國優秀博士學位論文評選中獲得指導教師獎最多的](../Page/中國優秀博士學位論文評選.md "wikilink")[博士生導師](../Page/博士生導師.md "wikilink")（分別是：關澤群（1999年），袁修孝（2001年），吳華意（2002年），王树良（2005年），共4次）。

## 科研

李德仁致力於3S系統（[遥感](../Page/遥感.md "wikilink")（[RS](../Page/RS.md "wikilink")）、[全球卫星定位系统](../Page/全球卫星定位系统.md "wikilink")（[GPS](../Page/GPS.md "wikilink")）和[地理信息系统](../Page/地理信息系统.md "wikilink")（[GIS](../Page/GIS.md "wikilink")））为代表的[空间信息科学以及](../Page/空间信息科学.md "wikilink")[多媒体通信技术的科研並積極推動其產業化進程](../Page/多媒体通信技术.md "wikilink")，领导[武汉吉奥信息工程技术有限公司研制了](../Page/武汉吉奥信息工程技术有限公司.md "wikilink")[吉奥之星](../Page/吉奥之星.md "wikilink")（GeoStar）系列GIS高科技产品，打破了国外GIS软件“一统天下”的局面。他領導[武汉武大瑞风科技有限公司研製的瑞風可視電話系列產品達到國際先進水平](../Page/武汉武大瑞风科技有限公司.md "wikilink")。他領導[武汉立得空间信息技术发展有限公司研制的中國首套](../Page/武汉立得空间信息技术发展有限公司.md "wikilink")“[移动道路测量系统](../Page/移动道路测量系统.md "wikilink")”（立得三S汽車道路測量與導航系統），专家們一致认为是一项达到世界先进水平的“革命性的高科技产品”，现已向全世界推广。他有十余項研究成果獲得了國家或部委級科技進步獎，其中“GPS航空摄影测量技术”获得了1999年国家科技进步二等奖。在以他为首的專家团体的努力下，中国测绘科学与美、德并驾齐驱，保持着世界的先進水平。
2013年度获得国家科学技术最高奖提名。\[2\]

## 職務

李德仁的職務還有中國湖北省科协副主席，湖北省武汉市关爱协会理事长，武汉欧美同学会会长，武汉留学回国博士联谊会会长，[武汉中国光谷首席科学家](../Page/武汉中国光谷.md "wikilink")，中国测绘学会理事长，中国图象图形学会副理事长，[中国地理学会环境遥感分会副理事长](../Page/中国地理学会.md "wikilink")，中国GIS协会顾问，第九届全国政协委员，亚洲GIS协会創始会长。

## 未來

李德仁準備組織武漢大學的科技與工程力量獨立研發一顆高分辨率的衛星，將用于土地利用、城市發展等領域的商業應用。他希望到2020年，中國也能建成独立自主的全球导航定位系统。

## 學生

李德仁共培養了60多位碩士生，在讀或已畢業的100多位博士生，其中較著名的有：

  - [龚健雅](../Page/龚健雅.md "wikilink")，现任测绘遥感信息工程国家重点实验室主任，第一批长江学者，2011年当当选中国科学院院士。
  - [袁修孝](../Page/袁修孝.md "wikilink")，参与的“GPS用于空中三角测量的实验研究”科研項目，荣获中國国家科技进步二等奖。
  - [吴华意](../Page/吴华意.md "wikilink")，獨立完成“面向对象GIS基础软件的研究”中的数据结构研究，荣获中國国家科技进步二等奖。
  - [李清泉](../Page/李清泉.md "wikilink")，现任[深圳大学校长](../Page/深圳大学.md "wikilink")，原武汉大学常务副校长。獨立完成“面向对象GIS基础软件的研究”中的数据结构研究，荣获中國国家科技进步二等奖。

## 家庭

  - 祖先:[李承霖](../Page/李承霖.md "wikilink")（清朝政治人物）
  - 岳父：[朱裕璧](../Page/朱裕璧.md "wikilink")（医学家）
  - 妻：[朱宜萱](../Page/朱宜萱.md "wikilink")（遥感学家）
  - 弟：[李德毅](../Page/李德毅.md "wikilink")([中国工程院院士](../Page/中国工程院院士.md "wikilink")，指挥自动化和人工智能专家)

## 注釋

## 外部連結

  - [李德仁简介](https://web.archive.org/web/20070608085345/http://www.cae.cn/experts/detail.jsp?id=343)
    - 中国工程院
  - [李德仁：数字地球与“三S”技术](https://web.archive.org/web/20070804160813/http://www.casad.ac.cn/2006-3/2006320104528.htm)，来源：《科学与中国——院士专家巡讲报告集》（第一辑）
  - [他如何成为全国获指导奖最多的博导](https://web.archive.org/web/20070930100048/http://www.cunews.edu.cn/html2006/hzdq/hzdqxw/103354111.htm)，《中国教育报》2006年2月15日第3版
  - [两院院士介绍-李德仁](https://web.archive.org/web/20070804095156/http://www.whu.edu.cn/cn/ljjy/ljjy208.htm)，武漢大學官方網
  - [知名院士－李德仁院士](https://web.archive.org/web/20070925043230/http://gtxx.hncj.edu.cn/zmys3.htm)，2007年7月23日造訪
  - [测绘遥感信息工程国家重点实验室](https://web.archive.org/web/20070808010725/http://www.liesmars.wtusm.edu.cn/index.asp)
  - [李德仁的美丽脚印](https://web.archive.org/web/20070928044215/http://www.cast.org.cn/n435777/n435787/8529.html)，[中国科学技术协会](../Page/中国科学技术协会.md "wikilink")，2005年12月7日
  - [武汉武大吉奥信息工程技术有限公司](http://www.geostar.com.cn/)

[Category:武汉测绘科技大学校长](../Category/武汉测绘科技大学校长.md "wikilink")
[Category:中华人民共和国教育家](../Category/中华人民共和国教育家.md "wikilink")
[Category:中国工程院信息与电子工程学部院士](../Category/中国工程院信息与电子工程学部院士.md "wikilink")
[Category:国际欧亚科学院院士](../Category/国际欧亚科学院院士.md "wikilink")
[Category:武汉大学教授](../Category/武汉大学教授.md "wikilink")
[Category:武汉大学校友](../Category/武汉大学校友.md "wikilink")
[Category:武汉测绘学院校友](../Category/武汉测绘学院校友.md "wikilink")
[Category:斯图加特大学校友](../Category/斯图加特大学校友.md "wikilink")
[Category:江苏科学家](../Category/江苏科学家.md "wikilink")
[Category:测绘学家](../Category/测绘学家.md "wikilink")
[Category:丹徒人](../Category/丹徒人.md "wikilink")
[Category:泰州人](../Category/泰州人.md "wikilink")
[D德](../Category/李姓.md "wikilink")

1.  [中国科学院学部与院士·院士信息·地学部·李德仁
    (测绘学家)](http://sourcedb.cas.cn/sourcedb_ad_cas/zw2/ysxx/dxb/)

2.  [2013年度国家科学技术奖](http://www.cutech.edu.cn/cn/kjjl/gjkjjl/sgwj/2013/01/1354173350685730.htm)