创新的定义：创新是指以现有的思维模式提出有别于常规或常人思路的见解为导向，利用现有的知识和物质，在特定的环境中，本着理想化需要或为满足社会需求，去改进或创造新的事物、方法、元素、路径、环境，并能获得一定有益效果的行为。

**创新**的定义是推出新事物。\[1\]
[Light_Bulb_Icon.svg](https://zh.wikipedia.org/wiki/File:Light_Bulb_Icon.svg "fig:Light_Bulb_Icon.svg")
[中文](../Page/中文.md "wikilink")“**创新**”一词是在1980年代才被人们接受的。是指科技上的[发明](../Page/发明.md "wikilink")、[创造](../Page/创造.md "wikilink")。后来意义发生推广，用于指代在[人的主观作用推动下产生所有以前没有的](../Page/人.md "wikilink")[设想](../Page/设想.md "wikilink")、[技术](../Page/技术.md "wikilink")、[文化](../Page/文化.md "wikilink")、[商业或者](../Page/商业.md "wikilink")[社会方面的关系](../Page/社会.md "wikilink")。也指[自然科学的新发现](../Page/自然科学.md "wikilink")。

創新具有某种不可预见性\[2\]。科技创新是各创新主体、各创新要素交互作用下的一种复杂涌现现象，是技术进步与应用创新所构成的创新双螺旋共同演进的产物\[3\]。大多数的创新行为都是未经计划的产物，因而创新是不能计划的\[4\]。

创新具有一定的风险，可是如果企业选择在原地踏步，同样也是一种危险的行为，因为竞争者可能会开发出更好或者更价廉的产品。
Christensen将这种局面表述为“创新者的难题”（The Innovator's
Dilemma）。随着当今世界的变革不断加速，作为对变革的应对手段的创新也就越来越多地成为人们谈论的话题。信息技术的发展进一步带动了以用户为中心、以社会实践为舞台、以共同创新、开放创新为特点的用户参与的创新2.0模式，带动了创新的扩散和普适，创新2.0被认为是创新形态应对知识社会来临的新变化。\[5\]

由创新带动的生产力的增长是经济财富增长的根本原因。革新如果被世界广泛采纳就是创新。在很多领域中，一些新的事物革新的必须与先前的充分的不同，而不是没有意义的改变，比如说在艺术、经济、商业和政治领域中。在经济领域，创新必须带来价值的增长，比如客户价值、产品价值。创新的目的是积极意义的变革，使得事情变得更好。创新可以指思想，事物，过程或服务中的质变和量变。创新在经济、商业、技术、社会学以及建筑学这些领域的研究中有着举足轻重的分量。口语上，经常用“创新”一词表示改革的结果。既然改革被视为经济发展的主要推动力，那么对决策者来说，促进创新的因素也被视为至关重要。

## 创新的类型

### Gene Meieran

[英特尔](../Page/英特尔.md "wikilink")<sup><span style="font-family:arial;">®</span></sup>高级院士Gene
Meieran认为，创新有三种类型：突破性创新，其特征是打破陈规，改变传统和大步跃进；渐进式创新，特征是采取下一逻辑步骤，让事物越来越美好；再运用式创新，特征是采用横向思维，以全新的方式应用原有事物。

### 新三級創新模型

#### 第一級

創新事物與現今人類社會沒有任何相似點。

#### 第二級

創新事物需要遵守特定規則。可被歸入某種類型。與人類社會有共通點。例子：新產業形成，但可被歸入產業一類型

#### 第三級

將多種固有領域融合發展成新的模式。商業與科技融合的例子：網上訂單

## 参见

  - [點子](../Page/點子.md "wikilink")
  - [创造](../Page/创造.md "wikilink")
  - [创造力](../Page/创造力.md "wikilink")
  - [发明](../Page/发明.md "wikilink")
  - [改革](../Page/改革.md "wikilink")
  - [发现](../Page/发现.md "wikilink")
  - [约瑟夫·熊彼特](../Page/约瑟夫·熊彼特.md "wikilink")
  - [全球創新指數](../Page/全球創新指數.md "wikilink")

## 参考文献

  -
[創新](../Category/創新.md "wikilink")
[Category:设计](../Category/设计.md "wikilink")
[Category:创新经济学](../Category/创新经济学.md "wikilink")

1.
2.
3.
4.
5.