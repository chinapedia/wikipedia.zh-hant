**白泉社**（）是一家[日本的](../Page/日本.md "wikilink")[出版社](../Page/出版社.md "wikilink")。主要發行雜誌、漫畫、文庫、繪本等書籍。1973年12月1日從[集英社分支出去成立](../Page/集英社.md "wikilink")。跟[小學館](../Page/小學館.md "wikilink")、[集英社同屬於](../Page/集英社.md "wikilink")[一橋集團](../Page/一橋集團.md "wikilink")（）。

## 發行雜誌

### [漫畫雜誌](../Page/漫畫雜誌.md "wikilink")

  - [花與夢](../Page/花與夢.md "wikilink")（）
  - 別冊花與夢（）
  - The 花與夢（）
  - [LaLa](../Page/LaLa.md "wikilink")
  - LaLa DX
  - 月刊MELODY（）
  - [YOUNG ANIMAL](../Page/YOUNG_ANIMAL.md "wikilink")（）
  - YOUNG ANIMAL嵐（）
  - YOUNG ANIMAL增刊Island（）
  - 樂園 Le Paradis（）（[季刊](../Page/季刊.md "wikilink")(每年2、6、10月發售)）

### 一般雜誌

  - MOE
  - CANDy（休刊）
  - 小說花丸

### 網絡雜誌

  - Love Silky

## 漫畫

  - 花與夢COMICS（）
  - [JETS COMICS](../Page/JETS_COMICS.md "wikilink")（）
  - 白泉社LADIES' COMICS（）
  - 花丸COMICS（）

## 書籍

  - 白泉社文庫（）
  - 花丸文庫・花丸文庫BLACK
  - 花丸novels（）

## 參見

  - [花與夢](../Page/花與夢.md "wikilink")
  - [LaLa](../Page/LaLa.md "wikilink")
  - [JETS COMICS](../Page/JETS_COMICS.md "wikilink")
  - [白泉社雅典娜新人大賞](../Page/白泉社雅典娜新人大賞.md "wikilink")
  - [集英社](../Page/集英社.md "wikilink")
  - [小學館](../Page/小學館.md "wikilink")
  - [一橋集團](../Page/一橋集團.md "wikilink")

## 外部連結

  - [官方網站](http://www.hakusensha.co.jp/)
  - [白泉社Twitter](https://twitter.com/hakusensha)

<!-- end list -->

  -
    [](http://www.hakusensha.co.jp/shoten/pop/)（販售促進用A4大小的POP海報圖）
      -
        [](https://web.archive.org/web/20080329130132/http://www.hakusensha.co.jp/shoten/pop/jet.shtml)、[](https://web.archive.org/web/20051128230217/http://www.hakusensha.co.jp/shoten/pop/hanayume.shtml)

<!-- end list -->

  - [白泉社文庫
    全作品目録2012](http://www.hakusensha.co.jp/hbstation/catalog/hbcatalog.pdf)
  - [白泉社寄稿作家陣から被災地の皆様へ |
    白泉社](http://www.hakusensha.co.jp/message/index.html)

[Category:一橋集團](../Category/一橋集團.md "wikilink")
[白泉社](../Category/白泉社.md "wikilink")
[Category:日本出版社](../Category/日本出版社.md "wikilink")
[Category:漫畫出版社](../Category/漫畫出版社.md "wikilink")
[Category:1973年建立](../Category/1973年建立.md "wikilink")
[Category:千代田區公司](../Category/千代田區公司.md "wikilink")