**庫頁湖** ()
是[俄羅斯](../Page/俄羅斯.md "wikilink")[堪察加半島上南部的一個大的](../Page/堪察加半島.md "wikilink")[破火山口上的](../Page/破火山口.md "wikilink")[湖](../Page/湖.md "wikilink")。湖的總面積為77[平方公里](../Page/平方公里.md "wikilink")，平均深度為176米，最深處為306米。她是[歐亞大陸上最大的](../Page/歐亞大陸.md "wikilink")[索克耶鮭魚](../Page/索克耶鮭魚.md "wikilink")（學名：*Oncorhynchus
nerka*）繁衍地。

## 閱讀材料

  -
  -
## 外部連結

  - [全球火山研究項目](http://www.volcano.si.edu/world/volcano.cfm?vnum=1000-023)

[Category:俄罗斯湖泊](../Category/俄罗斯湖泊.md "wikilink")
[Category:火山湖](../Category/火山湖.md "wikilink")