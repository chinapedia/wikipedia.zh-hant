## 政治

  - 總統：陳水扁
  - 副總統：呂秀蓮
  - 行政院院長：張俊雄

## 大事记

  - [1月14日](../Page/1月14日.md "wikilink")——[希臘籍貨輪](../Page/希臘.md "wikilink")[阿瑪斯號於](../Page/阿瑪斯號.md "wikilink")[墾丁外海](../Page/墾丁.md "wikilink")[擱淺漏油](../Page/阿瑪斯號貨輪油污事件.md "wikilink")，對生態造成嚴重破壞。
  - [1月15日](../Page/1月15日.md "wikilink")——[大法官作出](../Page/大法官.md "wikilink")[釋字第520號解釋文](../Page/釋字第520號.md "wikilink")，要求行政院若欲變更國家重要政策，停止預算執行，則須向立法院報告，並獲立法院決議後始得停止執行。(意指行政院片面決定停建核四舉動，違憲。)
  - [2月14日](../Page/2月14日.md "wikilink")——[行政院正式宣佈](../Page/行政院.md "wikilink")[第四核能發電廠工程復工](../Page/第四核能發電廠.md "wikilink")。
  - [5月12日](../Page/5月12日.md "wikilink")——[臺北縣](../Page/新北市.md "wikilink")[汐止市](../Page/汐止區.md "wikilink")[東方科學園區發生大火](../Page/東方科學園區.md "wikilink")，延燒達43小時之久，至[14日凌晨才撲滅](../Page/5月14日.md "wikilink")。
  - [5月18日](../Page/5月18日.md "wikilink")——[新竹縣](../Page/新竹縣.md "wikilink")[湖口鄉](../Page/湖口鄉.md "wikilink")[新竹工業區發生](../Page/新竹工業區.md "wikilink")[化工爆炸事故](../Page/2001年福國化工爆炸事故.md "wikilink")，共造成1人死亡，112人受傷。
  - [7月11日](../Page/7月11日.md "wikilink")——[潭美颱風侵襲台灣](../Page/熱帶風暴潭美_\(2001年\).md "wikilink")，後續的西南氣流在高雄及屏東造成豪雨及[嚴重積水](../Page/七一一水災.md "wikilink")。5人死亡。
  - [7月11日](../Page/7月11日.md "wikilink")——[台灣](../Page/台灣.md "wikilink")[茄萣和](../Page/茄萣.md "wikilink")[巴西](../Page/巴西.md "wikilink")[混血兒](../Page/混血兒.md "wikilink")[吳憶樺爭監護權事件](../Page/吳憶樺.md "wikilink")。
  - [7月29日](../Page/7月29日.md "wikilink")——中度[颱風](../Page/颱風.md "wikilink")[桃芝侵襲](../Page/桃芝颱風.md "wikilink")[台灣](../Page/台灣.md "wikilink")，在[南投](../Page/南投縣.md "wikilink")、[花蓮等地造成嚴重災情](../Page/花蓮縣.md "wikilink")，造成111人死亡。
  - [8月12日](../Page/8月12日.md "wikilink")——[台灣團結聯盟成立](../Page/台灣團結聯盟.md "wikilink")。
  - [9月16日至](../Page/9月16日.md "wikilink")[19日](../Page/9月19日.md "wikilink")——中颱[納莉襲台](../Page/颱風納莉_\(2001年\).md "wikilink")，造成北台灣嚴重水患，[台北捷運及](../Page/台北捷運.md "wikilink")[台北車站淹水](../Page/台北車站.md "wikilink")，產業損失超過[新台幣](../Page/新台幣.md "wikilink")80億，94人死亡。
  - [11月6日至](../Page/11月6日.md "wikilink")[18日](../Page/11月18日.md "wikilink")——[2001年世界盃棒球賽於台灣舉行](../Page/2001年世界盃棒球賽.md "wikilink")，[台灣於季軍戰中擊敗](../Page/中華成棒隊.md "wikilink")[日本](../Page/日本棒球代表隊.md "wikilink")，獲得季軍。
  - [11月10日](../Page/11月10日.md "wikilink")——[臺灣獲准於](../Page/臺灣.md "wikilink")[2002年加入](../Page/2002年.md "wikilink")[世界貿易組織](../Page/世界貿易組織.md "wikilink")。
  - [12月1日](../Page/12月1日.md "wikilink")——[民主進步黨於](../Page/民主進步黨.md "wikilink")[第5屆立法委員選舉贏得](../Page/2001年中華民國立法委員選舉.md "wikilink")87席、首次成為國會最大黨，國民黨68席、親民黨46席、台灣團結聯盟13席、新黨1席

## 出生

## 逝世

  - [2月8日](../Page/2月8日.md "wikilink")——[陈立夫](../Page/陈立夫.md "wikilink")，[台灣政治人物](../Page/台灣.md "wikilink")（－[1898年](../Page/1898年.md "wikilink")）。
  - [4月9日](../Page/4月9日.md "wikilink")——[謝東閔](../Page/謝東閔.md "wikilink")，[台灣政治人物](../Page/台灣.md "wikilink")，[中華民國第六任副總統](../Page/中華民國.md "wikilink")（－[1908年](../Page/1908年.md "wikilink")）。
  - [5月31日](../Page/5月31日.md "wikilink")——[李國鼎](../Page/李國鼎.md "wikilink")，[台灣經濟學家及政治人物](../Page/台灣.md "wikilink")（－[1910年](../Page/1910年.md "wikilink")）。
  - [12月1日](../Page/12月1日.md "wikilink")——[林海音](../Page/林海音.md "wikilink")，[台灣作家](../Page/台灣.md "wikilink")（－[1918年](../Page/1918年.md "wikilink")）。

## 資料來源

[Category:2001年](../Category/2001年.md "wikilink")
[\*](../Category/2001年台灣.md "wikilink")