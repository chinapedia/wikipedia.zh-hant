**奔山龍屬**（屬名：*Orodromeus*）是[稜齒龍類](../Page/稜齒龍類.md "wikilink")[恐龍的一屬](../Page/恐龍.md "wikilink")，生存於晚[白堊紀](../Page/白堊紀.md "wikilink")（[坎潘階](../Page/坎潘階.md "wikilink")）的[北美洲](../Page/北美洲.md "wikilink")，約7500萬年前的[蒙大拿州](../Page/蒙大拿州.md "wikilink")，化石僅發現於[雙麥迪遜組](../Page/雙麥迪遜組.md "wikilink")。

## 發現與命名

奔山龍的化石是由Robert
Makela發現於[蒙大拿州](../Page/蒙大拿州.md "wikilink")[提頓縣](../Page/提頓縣_\(蒙大拿州\).md "wikilink")，跟[慈母龍發現於相同地區](../Page/慈母龍.md "wikilink")。[模式種是](../Page/模式種.md "wikilink")**馬氏奔山龍**（*O.
makelai*），是由[傑克·霍納](../Page/傑克·霍納.md "wikilink")（Jack
Horner）與大衛·威顯穆沛（David B.
Weishampel）在1988年敘述、命名。屬名意為「山的奔跑者」，種名則是以發現化石的Robert
Makela為名\[1\]。

[正模標本](../Page/正模標本.md "wikilink")（編號MOR
294）是一個部分身體骨骼與頭顱骨，發現於[雙麥迪遜組](../Page/雙麥迪遜組.md "wikilink")（Two
Medicine
Formation）的下層，地質年代約7500萬年前，相當於[白堊紀晚期的](../Page/白堊紀.md "wikilink")[坎潘階](../Page/坎潘階.md "wikilink")。[副模標本包含](../Page/副模標本.md "wikilink")：編號MOR
249標本的19個蛋化石與胚胎化石、編號PP 22412標本的後肢化石、編號MOR 331標本的ㄧ個部分身體骨骼、編號MOR
248的頭顱骨與身體骨骼、編號MOR
403標本的腦殼\[2\]。目前還沒有奔山龍的詳細研究，但過去曾有一篇奔山龍的未發表研究\[3\]。

## 敘述

奔山龍是小型、二足、[草食性恐龍](../Page/草食性.md "wikilink")，可能與[懼龍](../Page/懼龍.md "wikilink")（*Daspletosaurus*）、[野牛龍](../Page/野牛龍.md "wikilink")（*Einiosaurus*）等恐龍共同生存於同一地區。身長被估計約2.5公尺長\[4\]。

奔山龍的鑑定特徵有：[眼瞼骨後端接觸到](../Page/眼瞼骨.md "wikilink")[眶後骨](../Page/眶後骨.md "wikilink")、[顴骨有隆起](../Page/顴骨.md "wikilink")、手腕骨頭沒有癒合、[上頜骨與](../Page/上頜骨.md "wikilink")[齒骨的牙齒呈三角形](../Page/齒骨.md "wikilink")\[5\]。

奔山龍胚胎化石的骨頭、牙齒已發展良好，霍納等人推測奔山龍是[早熟性動物](../Page/早熟性.md "wikilink")；而生存於相同環境的[慈母龍](../Page/慈母龍.md "wikilink")，幼體孵化後仍需要長時間的親代養育\[6\]。

奔山龍被推測是穴居動物，如同牠們的近親[掘奔龍](../Page/掘奔龍.md "wikilink")（*Oryctodromeus*），這是根據牠們骨頭的散佈與地點來判斷的\[7\]。

## 分類

在奔山龍的命名研究裡，牠們被歸類於[稜齒龍科](../Page/稜齒龍科.md "wikilink")，是已知生存年代最晚的物種\[8\]。目前稜齒龍類被視為[並系群](../Page/並系群.md "wikilink")，而奔山龍是[真鳥腳類的原始物種](../Page/真鳥腳類.md "wikilink")。

## 大眾文化

奔山龍出現在[探索頻道的](../Page/探索頻道.md "wikilink")《[恐龍星球](../Page/恐龍星球.md "wikilink")》（*Dinosaur
Planet*）電視節目第三輯，在節目裡牠們以群體方式生活，並且有一群[傷齒龍獵食牠們](../Page/傷齒龍.md "wikilink")。

## 參考資料

  - <https://web.archive.org/web/20130927064547/http://www.thescelosaurus.com/ornithopoda.htm>
  - Horner, J. and Weishampel, D., 1988, “A comparative embryological
    study of two ornithischian dinosaurs”, Nature (London), 332(No.
    6161); 256-257 (1988).

[Category:稜齒龍科](../Category/稜齒龍科.md "wikilink")
[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")
[Category:北美洲恐龍](../Category/北美洲恐龍.md "wikilink")

1.  Horner, J. and Weishampel, D., 1988, “A comparative embryological
    study of two ornithischian dinosaurs”, *Nature* (London),
    **332**(No. 6161): 256-257 (1988)

2.
3.  Scheetz, R.D., 1999, ''Osteology of *Orodromeus makelai* and the
    phylogeny of basal ornithopod dinosaurs'' D. Ph. Thesis in Biology,
    Montana State University, Bozeman, 189 pp

4.
5.
6.
7.

8.