**tri-Ace**（、）是一間於成立的[日本](../Page/日本.md "wikilink")[電子遊戲軟體開發公司](../Page/電子遊戲.md "wikilink")。由原涉及[南夢宮](../Page/南夢宮.md "wikilink")『[傳奇系列](../Page/傳奇系列.md "wikilink")』開發的[日本通訊網絡中的開發團隊之一](../Page/日本通訊網絡.md "wikilink")「[狼組](../Page/狼組.md "wikilink")」的一部分人員與公司高層衝突而從中獨立。

在2003年4月1日以前公司開發的遊戲皆由[艾尼克斯代理](../Page/艾尼克斯.md "wikilink")；隨著艾尼克斯與[史克威爾合併](../Page/史克威爾.md "wikilink")，於2003年4月1日後則由[史克威爾艾尼克斯代理發售](../Page/史克威爾艾尼克斯.md "wikilink")。tri-Ace原公司總社是租借舊艾尼克斯大樓（[東京都](../Page/東京都.md "wikilink")[澀谷區](../Page/澀谷區.md "wikilink")[代代木](../Page/代代木.md "wikilink")4-31-8），但與史克威爾艾尼克斯沒有[資本關係](../Page/資本.md "wikilink")。

到現在為止，tri-Ace發售的全部都是電子遊戲的遊戲軟件，因遊戲系統而擁有很多愛好者；但因為[除錯不足](../Page/除錯.md "wikilink")，有很多各種大大小小的系統錯誤。延期發售多數是因為與其他公司協調不足或除錯不足引起，在遊戲雜誌和攻略本中常看到他們的採訪。

tri-Ace的作品以「快速破關」及「遊玩的心」為特徵。而[星海遊俠系列及](../Page/星海遊俠系列.md "wikilink")[女神戰記系列的遊戲音樂都是由](../Page/女神戰記.md "wikilink")[櫻庭統負責](../Page/櫻庭統.md "wikilink")。在業界中他們特別重視畫質，從遊戲在[PlayStation
2平台發售開始](../Page/PlayStation_2.md "wikilink")，加入對應寬螢幕及[逐行掃描](../Page/逐行掃描.md "wikilink")，音樂方面則對應[Dolby
Pro
Logic](../Page/杜比數碼.md "wikilink")，而《[女神戰記2：希爾梅莉亞](../Page/女神戰記2：希爾梅莉亞.md "wikilink")》更對應高解析度顯示。

由於tri-Ace沒有參與開發[PlayStation
Portable版女神戰記和](../Page/PlayStation_Portable.md "wikilink")《[星海遊俠1](../Page/星海遊俠1_First_Departure.md "wikilink")》及《[星海遊俠2](../Page/星海遊俠2_Second_Evolution.md "wikilink")》的重製版，所以都沒有刊載tri-Ace官方網站中。

2015年3月，成為NEPRO JAPAN Co., Ltd.的子公司。\[1\]\[2\]。

## 發展

  - 1995年3月16日 設立公司
  - 1996年7月19日 「[星海遊俠](../Page/星海遊俠.md "wikilink")」發售
  - 1998年7月30日 「[星海遊俠 The Second
    Story](../Page/星海遊俠_The_Second_Story.md "wikilink")」發售
  - 1999年12月22日 「[女神戰記](../Page/女神戰記.md "wikilink")」發售
  - 2001年6月28日 「[星海遊俠 Blue
    Sphere](../Page/星海遊俠_Blue_Sphere.md "wikilink")」發售
  - 2003年2月27日 「[星海遊俠 Till the End of
    Time](../Page/星海遊俠_Till_the_End_of_Time.md "wikilink")」發售
  - 2004年1月22日 「星海遊俠 Till the End of Time Director's Cut」發售
  - 2005年1月27日 「[Radiata
    Stories](../Page/Radiata_Stories.md "wikilink")」發售
  - 2005年2月3日 「[PSone books](../Page/PSone_books.md "wikilink") Valkyrie
    Profile」發售
  - 2005年3月3日 「PSone books 星海遊俠 The Second Story」發售
  - 2005年10月11日
    總社轉移至[東京都](../Page/東京都.md "wikilink")[品川區](../Page/品川區.md "wikilink")[大井](../Page/大井.md "wikilink")1-28-1。
  - 2006年6月22日 「[女神戰記2：希爾梅莉亞](../Page/女神戰記2：希爾梅莉亞.md "wikilink")」發售
  - 2007年6月28日「女神戰記 2：希爾梅莉亞 (ULTIMATE HITS)」發售
  - 2008年3月6日「女神戰記：蕾娜絲 (Ultimate Hits)」PSP平台 發售
  - 2008年9月25日「無盡探險」XBOX360平台 發售
  - 2008年11月1日「女神戰記：負罪者」NDS平台 發售
  - 2009年2月19日「星海遊俠 THE LAST HOPE」XBOX360平台 發售

## 相關條目

  - [tri-Crescendo](../Page/tri-Crescendo.md "wikilink")

## 参考文献

## 外部連結

  - [官方網站](http://www.tri-ace.co.jp/)

[Tri-Ace](../Category/Tri-Ace.md "wikilink")
[Category:1995年開業電子遊戲公司](../Category/1995年開業電子遊戲公司.md "wikilink")
[Category:日本電子遊戲公司](../Category/日本電子遊戲公司.md "wikilink")
[Category:電子遊戲開發公司](../Category/電子遊戲開發公司.md "wikilink")
[Category:品川區公司](../Category/品川區公司.md "wikilink")

1.
2.  [ネプロジャパン，「スターオーシャン」シリーズなどで知られるトライエースを子会社化したと発表](http://www.4gamer.net/games/999/G999905/20150309061/),4Gamer.net,2015年3月9日