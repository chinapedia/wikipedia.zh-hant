[StephenPearlAndrews.jpg](https://zh.wikipedia.org/wiki/File:StephenPearlAndrews.jpg "fig:StephenPearlAndrews.jpg")
**史蒂芬·皮爾·安德魯斯**（Stephen Pearl Andrews,
）是[美國個人無政府主義者和作家](../Page/美國個人無政府主義.md "wikilink")。

## 生平

1812年出生於[马萨诸塞州](../Page/马萨诸塞州.md "wikilink")[烏斯特郡](../Page/烏斯特郡.md "wikilink")（Worcester）的。他在18歲時前往[路易斯安那州學習法律](../Page/路易斯安那州.md "wikilink")；他被當地的蓄奴現象給震撼，從此成為[廢奴主義者](../Page/廢奴主義.md "wikilink")。他在1839年前往[德州](../Page/德州.md "wikilink")，由於他激進的廢奴主義授課言論，他和他的家人差點遭到當地人的殺害，於是他們在1843年逃離了那裡。接著安德魯斯前往英國，試圖在那裡籌措資金以回國資助廢奴運動，但並沒有成功。

他對語音學和外國語言也相當感興趣，最後他學習了高達30種語言。到了1840年代他開始將注意力放在烏托邦的社區上，他和另外一名[個人無政府主義者](../Page/個人無政府主義.md "wikilink")[約書亞·沃倫](../Page/約書亞·沃倫.md "wikilink")（沃倫也是促使安德魯斯轉變為激進個人主義的人）一同在1851年於紐約州創立了Modern
Times烏托邦社區，接著在1857年又在[紐約市創建Unity](../Page/紐約市.md "wikilink")
Home。到了1860年代他已經構思出了一種名為「Pantarchy」（由所有人統治）的理想社會，並接著構思出了一套名為「宇宙學」（universology）的哲學，強調所有知識和行動的一貫性。

## 著作

  - *Cost the Limit of Price* (1851年)
  - *The Constitution of Government in the Sovereignty of the
    Individual* (1851年)
  - *The Science of Society* (1851年)
  - *The Sovereignty of the Individual* (1853年)
  - *Principles of Nature, Original Physiocracy, the New Order of
    Government* (1857年)
  - *The Pantarchy* (1871年)
  - *The Basic Outline of Universology* (1872年)
  - *The Labor Dollar* (1881年)
  - *Elements of Universology* (1881年)
  - *The New Civilization* (1885年)

[Category:無政府主義者](../Category/無政府主義者.md "wikilink")
[Category:個人無政府主義者](../Category/個人無政府主義者.md "wikilink")
[Category:美国废奴主义者‎](../Category/美国废奴主义者‎.md "wikilink")