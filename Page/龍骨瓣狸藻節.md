**龍骨瓣狸藻節**\[1\]（[学名](../Page/学名.md "wikilink")：***Utricularia* sect.
''Nigrescentes**''），又称**黑狸藻组**，為[狸藻屬下的一節](../Page/狸藻屬.md "wikilink")。该節中的三個物種皆為小型的[陸生](../Page/陆生植物.md "wikilink")[食虫植物](../Page/食虫植物.md "wikilink")，分布於[非洲熱帶](../Page/非洲.md "wikilink")、[亞洲以及](../Page/亞洲.md "wikilink")[澳大利亚](../Page/澳大利亚.md "wikilink")。1859年，[丹尼爾·奧利弗最先描述了该组](../Page/丹尼爾·奧利弗.md "wikilink")，然而未明確說明该组的归属。[小宮定志於](../Page/小宮定志.md "wikilink")1973年修訂了该節。[彼得·泰勒在他](../Page/彼得·泰勒_\(植物學家\).md "wikilink")1989年的分類狸藻属专著《[狸藻属——分类学专著](../Page/狸藻属——分类学专著.md "wikilink")》中將该節放置於[狸藻亞屬](../Page/狸藻亞屬.md "wikilink")下。但根据进一步的[系统发生学数据](../Page/系统发生学.md "wikilink")，在恢复[雙瓣狸藻亞屬](../Page/雙瓣狸藻亞屬.md "wikilink")的同时，也将龍骨瓣狸藻组置于其下。\[2\]\[3\]

## 参考文献

[\*](../Category/狸藻屬.md "wikilink")
[\*](../Category/非洲食虫植物.md "wikilink")
[\*](../Category/亚洲食虫植物.md "wikilink")
[\*](../Category/澳大利亚食虫植物.md "wikilink")
[\*](../Category/龙骨瓣狸藻组.md "wikilink")

1.
2.  Taylor, Peter. (1989). *[The genus Utricularia - a taxonomic
    monograph](../Page/狸藻属——分类学专著.md "wikilink")*. Kew Bulletin
    Additional Series XIV: London.
3.  Müller, K.F., Borsch, T., Legendre, L., Porembski, S., and
    Barthlott, W. (2006). Recent progress in understanding the evolution
    of carnivorous Lentibulariaceae (Lamiales). *Plant Biology*, 8:
    748-757.