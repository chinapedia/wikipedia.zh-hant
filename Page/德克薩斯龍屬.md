**德克薩斯龍屬**（屬名：*Texasetes*）是種[甲龍類](../Page/甲龍類.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，生存於下[白堊紀的](../Page/白堊紀.md "wikilink")[北美洲](../Page/北美洲.md "wikilink")。德克薩斯龍的研究有限，化石發現於[德州](../Page/德州.md "wikilink")[塔蘭特縣的](../Page/塔蘭特縣.md "wikilink")[爪爪組](../Page/爪爪組.md "wikilink")（Paw
Paw
Formation），地質年代為[阿爾比階晚期](../Page/阿爾比階.md "wikilink")，該地也發現了另一種[結節龍科恐龍](../Page/結節龍科.md "wikilink")，[爪爪龍](../Page/爪爪龍.md "wikilink")。德克薩斯龍的身長為2.5到3公尺。

## 發現與種

德克薩斯龍的[模式標本](../Page/模式標本.md "wikilink")（編號USNM
337987）包含部份[肩胛烏喙骨板](../Page/肩胛烏喙骨板.md "wikilink")（Scapulocoracoid）、[骨盆](../Page/骨盆.md "wikilink")、前肢與後肢的骨頭、[脊椎](../Page/脊椎.md "wikilink")、[皮內成骨](../Page/皮內成骨.md "wikilink")（Osteoderms）、顱骨碎片、以及一個牙齒。這些化石是由M.
K.
Brett-Surman所發現，最初被誤認為是[蜥腳下目恐龍](../Page/蜥腳下目.md "wikilink")，但之後由Walter
P.
Coombs確認為[甲龍下目](../Page/甲龍下目.md "wikilink")，並命名為**鄰海德克薩斯龍**（*Texasetes
pleurohalio*），其種名在[希臘文中意思是](../Page/希臘文.md "wikilink")「靠近海的」（ / pleuro
為旁邊的；halio
原指海，這裡指德州南方的[墨西哥灣](../Page/墨西哥灣.md "wikilink")）\[1\]。Vickaryous等人與Coombs將德克薩斯龍鑑定為：擁有水平[腸骨](../Page/腸骨.md "wikilink")、無孔的[髖臼](../Page/髖臼.md "wikilink")、甲龍類形態的[肩胛骨](../Page/肩胛骨.md "wikilink")，包含明顯的[肩峰與前棘窩](../Page/肩峰.md "wikilink")\[2\]\[3\]。

## 分類

然而，Lee在1996年質疑這些化石是否足以鑒定，並懷疑德克薩斯龍可能是爪爪龍的一個[異名](../Page/異名.md "wikilink")\[4\]。Coombs將這些化石歸類於[結節龍科](../Page/結節龍科.md "wikilink")，但Vickaryous等人認為是德克薩斯龍是[甲龍下目的未分類物種](../Page/甲龍下目.md "wikilink")\[5\]。

## 參考資料

<div class="references-small">

<references>

</references>

</div>

## 外部連結

  - [*Texasetes* in The Dinosaur
    Encyclopaedia](http://web.me.com/dinoruss/de_4/5a786ad.htm) at Dino
    Russ's Lair

[Category:下白堊紀恐龍](../Category/下白堊紀恐龍.md "wikilink")
[Category:北美洲恐龍](../Category/北美洲恐龍.md "wikilink")
[Category:結節龍科](../Category/結節龍科.md "wikilink")

1.

2.

3.
4.

5.