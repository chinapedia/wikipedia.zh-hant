**軟食中獸**（*Hapalodectes*），又名**軟中獸**，是一類像[水獺的](../Page/水獺.md "wikilink")[中爪獸目](../Page/中爪獸目.md "wikilink")，生存於5500萬年前的[古新世晚期至](../Page/古新世.md "wikilink")[始新世早期](../Page/始新世.md "wikilink")。雖然軟食中獸的第一個[化石是在](../Page/化石.md "wikilink")[美國](../Page/美國.md "wikilink")[懷俄明州的始新世地層發現](../Page/懷俄明州.md "wikilink")，但牠們卻是起源於[蒙古](../Page/蒙古.md "wikilink")，最古老的*H.
dux*是在古新世晚期的地層發現。\[1\]牠們與[古鯨亞目](../Page/古鯨亞目.md "wikilink")，如[巴基鯨有關](../Page/巴基鯨.md "wikilink")，這是由於牠們在[頭顱骨及](../Page/頭顱骨.md "wikilink")[牙齒有極相似的地方](../Page/牙齒.md "wikilink")。

## 參考

[Category:軟食中獸屬](../Category/軟食中獸屬.md "wikilink")
[Category:古新世哺乳類](../Category/古新世哺乳類.md "wikilink")
[Category:始新世哺乳類](../Category/始新世哺乳類.md "wikilink")

1.