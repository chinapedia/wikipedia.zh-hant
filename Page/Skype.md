**Skype**（，與英文“type”同韻）是一款通信[应用软件](../Page/应用软件.md "wikilink")，可通过[網際網路为電腦](../Page/網際網路.md "wikilink")、平板电脑和移动设备提供与其他联网设备或传统电话/智能手机间进行视频通话和语音通话的服务。\[1\]使用者也可通过Skype收发[即时通信信息](../Page/即时通信.md "wikilink")，传输文件，收发[多媒体信息](../Page/MMS.md "wikilink")，进行[視象會議](../Page/视频会议.md "wikilink")。Skype可使用于[Microsoft
Windows](../Page/Microsoft_Windows.md "wikilink")，[Mac和](../Page/Mac_OS.md "wikilink")[Linux桌面环境下](../Page/Linux.md "wikilink")，同时也可在运行[Android](../Page/Android.md "wikilink")、[Blackberry](../Page/BlackBerry_OS.md "wikilink")、[iOS和](../Page/iOS.md "wikilink")[Windows
10
Mobile的](../Page/Windows_10_Mobile.md "wikilink")[智能手机和](../Page/智能手机.md "wikilink")[平板电脑中](../Page/平板电脑.md "wikilink")。Skype服务大部份免费，但当使用者需要打到固定电话或手提电话时，需要购买Skype点数或订购套餐。Skype软件基于[freemium许可协议框架进行授权](../Page/freemium.md "wikilink")。

Skype于2003年8月问世。该软件由[瑞典人Niklas](../Page/瑞典.md "wikilink")
Zennström，[丹麦人Janus](../Page/丹麦.md "wikilink")
Friis以及开发软件后端的[爱沙尼亚人](../Page/爱沙尼亚.md "wikilink")[Ahti
Heinla](../Page/Ahti_Heinla.md "wikilink")、[Priit
Kasesalu和](../Page/Priit_Kasesalu.md "wikilink")[Jaan
Tallinn合作创建](../Page/Jaan_Tallinn.md "wikilink")。Skype所采用的后台也被音乐分享软件[Kazaa所使用](../Page/Kazaa.md "wikilink")。\[2\]2005年9月，[eBay以](../Page/eBay.md "wikilink")26亿美元的价格购得Skype。\[3\]2009年9月，\[4\][Silver
Lake](../Page/Silver_Lake_Partners.md "wikilink"),[Andreessen
Horowitz和](../Page/Andreessen_Horowitz.md "wikilink")[Canada Pension
Plan Investment
Board宣布向](../Page/Canada_Pension_Plan_Investment_Board.md "wikilink")[eBay以](../Page/eBay.md "wikilink")19亿美元购入Skype的65%的股份。此次收购折合的Skype估价为27.5亿美元。2011年5月，微软以85亿美元的价格购入Skype。微软Skype部门总部位于[卢森堡](../Page/卢森堡.md "wikilink")，不过部门中大部份开发团队和部门内的44%雇员仍然在[爱沙尼亚的](../Page/爱沙尼亚.md "wikilink")[塔林和](../Page/塔林.md "wikilink")[塔尔图工作](../Page/塔尔图.md "wikilink")。

Skype允许用麦克风來进行交流，又可以通过[摄像头來进行视像通訊](../Page/摄像头.md "wikilink")，也可以在线即时通讯。Skype软件之间的通话（Skype-to-Skype
calls）无需付费，但当需要（通过传统的[公共交换电话网](../Page/公共交换电话网.md "wikilink")）致电给[固定电话或](../Page/固定电话.md "wikilink")[移动电话时](../Page/移动电话.md "wikilink")，用户需要通过名为“Skype点数”的用户账户系统付费。与大多数[VoIP软件不同](../Page/VoIP.md "wikilink")，Skype混合使用了[点对点架构和](../Page/对等网络.md "wikilink")[主从式架构](../Page/主从式架构.md "wikilink")。这一架构通过Skype安装在计算机上的[后台进程实现](../Page/后台进程.md "wikilink")。这一架构体现在Skype的原计划启用名称**Sky
Peer-to-Peer**（**對等天**）。

在部份社团、政府、家庭、学校网络中，网络管理员会以不恰当资源使用、占用带宽过多和安全等理由\[5\]禁止使用Skype\[6\]。

Skype的竞争者包括[会话发起协议和基于](../Page/会话发起协议.md "wikilink")[H.323的网络服务](../Page/H.323.md "wikilink")，例如[Linphone](../Page/Linphone.md "wikilink")，\[7\][Google
Hangouts和](../Page/Google_Hangouts.md "wikilink")[Viber等](../Page/Viber.md "wikilink")。

## 历史

Skype品牌由来自丹麦的 [Janus
Friis和来自瑞典的](../Page/Janus_Friis.md "wikilink")[Niklas
Zennström创建](../Page/Niklas_Zennström.md "wikilink")。\[8\]Skype软件则是由爱沙尼亚人[Ahti
Heinla](../Page/Ahti_Heinla.md "wikilink")，Priit Kasesalu，和[Jaan
Tallinn编写](../Page/Jaan_Tallinn.md "wikilink")。\[9\] Skype
在2003年8月29日发布首个公众测试版\[10\]

2005年，Skype与波兰门户网站Onet.pl达成进驻协议。\[11\]
2005年9月12日，[eBay以价值约](../Page/eBay.md "wikilink")25亿[美元的现金和ebay支票作为前期付款](../Page/美元.md "wikilink")，附加与潜在绩效相关的后期报酬。\[12\]

2009年9月1日，ebay宣布将Skype股份的65%以19亿美元出售给Silver Lake、Andreessen
Horowitz和Canada Pension Plan Investment Board。此次交易将Skype估价折合为27.5亿美元。

### 微软收购Skype

2011年5月10日，[微软以](../Page/微软.md "wikilink")85亿的价格收购[Skype Communications,
S.à
r.l](../Page/Skype_Technologies.md "wikilink")。\[13\]Skype公司合并为微软的一个部门，而微软通过本次交易获得Skype全部技术。2011年10月13日，交易完成。\[14\]\[15\]

微软在收购后，停止问世已久的即时通讯服务[Windows Live
Messenger](../Page/Windows_Live_Messenger.md "wikilink")。\[16\]
微软自2013年4月8日开始进行除中国外的软件转换工作，\[17\]该工作完成于同年4月30日，\[18\]其中台湾于4月25日转换。\[19\]中国的Windows
Live Messenger服务于2014年10月31日停止。\[20\]

## 介绍

Skype是一家全球性[互联网电话](../Page/互联网.md "wikilink")，它通过在全世界范围内向客户提供免费的高品质通话服务，採用[點對點技術與其他用戶連接](../Page/點對點技術.md "wikilink")，可以进行[高品質語音聊天](../Page/高品質語音.md "wikilink")。連線雙方網路順暢時，音质可能超过普通电话，正在逐渐改变电信业。Skype是网络即时语音沟通工具。具备IM所需的其他功能，比如视频[聊天](../Page/聊天.md "wikilink")、多人语音会议、多人聊天、传送文件、文字聊天等功能。它可以免费高清晰的与其他用户语音对话，也可以拨打国内[国际电话](../Page/国际电话.md "wikilink")，无论[固定电话](../Page/固定电话.md "wikilink")、[行動電話](../Page/行動電話.md "wikilink")、[小灵通均可直接拨打](../Page/小灵通.md "wikilink")，并且可以实现呼叫转移、短信发送等功能。

Skype軟體會在電腦上開啟一個網路連線[端口來监听其他Skype用戶的連線呼叫](../Page/端口.md "wikilink")；當其他電腦能順利連線到這部電腦，Skype稱呼該用戶為「Super
node」（超級節點）。Super
Node在該[P2P環境中的角色](../Page/P2P.md "wikilink")，即為提供其他無法連線的用戶的之間的中繼站，借用諸多Super
Nodes的些許網路[頻寬](../Page/頻寬.md "wikilink")，協助其他的Skype使用者之間能夠順利的互相聯繫。這種行為，在P2P環境中，這算是相當常見的手法，也是[點對點連線的精髓之一](../Page/點對點連線.md "wikilink")。Skype是第一個將此種做法運用到[網路語音通話與](../Page/網路語音.md "wikilink")[即時訊息應用層面上](../Page/即時訊息.md "wikilink")。

Skype在[台灣是與](../Page/台灣.md "wikilink")[網-{}-路家庭](../Page/網路家庭.md "wikilink")（PChome
Online）合作，推出的Skype稱為PChome &
Skype。在[中國大陸](../Page/中國大陸.md "wikilink")，Skype曾經與[TOM集團旗下北京讯能网](../Page/TOM集團.md "wikilink")-{}-络有限公司TOM在-{}-线合作，所推出的Skype又稱為Tom-Skype，并在[中华人民共和国政府的要求下](../Page/中华人民共和国政府.md "wikilink")，加入了将敏感訊息过滤记录并传送到服务器的功能（详见[中国服务](../Page/#中国服务.md "wikilink")）。在微軟收購Skype後改與光明方正公司合作。Skype的国际官方网站（www.skype.com）在中国大陆无法直接進入，输入www.skype.com会自动跳转到光明方正Skype。在[香港](../Page/香港.md "wikilink")，Skype與[和記環球電訊合作](../Page/和記環球電訊.md "wikilink")，推出的Skype稱為HGC-Skype。在日本則與[Buffalo和](../Page/Buffalo.md "wikilink")[Excite合作](../Page/Excite.md "wikilink")。

Skype於2005年9月12日被網路拍賣巨擘eBay公司以26億美元（約新台幣849億元或合人民幣210億元）的[現金跟](../Page/現金.md "wikilink")[股票併購](../Page/股票.md "wikilink")。初期支付13億美元現金與價值13億美元的3240萬股eBay[股票](../Page/股票.md "wikilink")，若Skype在2008年或2009年達到業績目標，eBay可能要再支付15億美元，使得併購總金額高達41億美元。

在2008年4月，公司推出了用於[Windows
Mobile的Skype軟體](../Page/Windows_Mobile.md "wikilink")。此軟體建基於[Java](../Page/Java.md "wikilink")，可以接收Skype和SkypeIn的呼叫，亦可以收發即時訊息\[21\]。

2010年12月，出现过一次全球宕機事件\[22\]。

2011年5月10日，微軟宣布以85億美元現金併購了Skype\[23\]。

2011年10月14日，[微軟宣布完成這項](../Page/微軟.md "wikilink")85億美元現金併購交易程序，Skype執行長Tony
Bates轉任微軟Skype事業部總裁。

2014年底，推出Skype 翻译（[Skype
Translator](../Page/Skype_Translator.md "wikilink")）试图打破语音隔阂。

## 用户使用量与互联网流量

| 年份   | 国际长途通话市场市占率                              |
| ---- | ---------------------------------------- |
| 2005 | 2.9 %\[24\]                              |
| 2006 | 4.4 %\[25\]                              |
| 2008 | 8 %\[26\]                                |
| 2009 | 12 %<ref name="GigaOm2009q4">{{Cite_web |
| 2010 | 13 %<ref name="inquis">{{Cite_web       |
| 2012 | 33 %<ref name="Phonearena">{{Cite_web   |
| 2013 | 36 %<ref name="Telegeo">{{Cite_web      |
| 2014 | 40 %<ref name="Anorak">{{Cite_web       |

2011年1月，在 iPhone 版 Skype 推出视频通话服务后，Skype
同时在线人数达到创纪录的2700万，\[27\]该记录随后被2011年2月21日的2900万打破，\[28\]，在2011年3月28日又被3000万打破。\[29\]2011年3月28日该纪录再次被修正为3000万人。\[30\]2012年2月25日，Skype宣布同时在线人数首次达到3200万。\[31\]2012年3月5日，同时在线人数刷新为3500万。\[32\]在不到一年之后的2013年1月21日，同时在线人数突破5000万。\[33\]

2012年7月，Android 版 Skype 突破7000万下载。\[34\]

2012年8月19日，Microsoft 宣布当季内 Skype 用户进行了1150亿分钟通话，较上一季增长50%。\[35\]

2014年1月15日，Skype 的国际通话流量持续增长。据 TeleGeography 估计，2013年 Skype
客户端间国际通话的流量增长了36%，达到了令人惊讶的2140亿分钟。\[36\]

## 系统和软件

### 客户端程序和设备支持

Skype运行于包括 [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink")
([XP](../Page/Windows_XP.md "wikilink"),
[Vista](../Page/Windows_Vista.md "wikilink"),
[7](../Page/Windows_7.md "wikilink"),
[8](../Page/Windows_8.md "wikilink")), [OS
X](../Page/OS_X.md "wikilink") (10.6及之后版本),
[Linux](../Page/Linux.md "wikilink")
([Ubuntu](../Page/Ubuntu.md "wikilink") 与其他 Linux 系统),
[Android](../Page/Android.md "wikilink"), [BlackBerry
10](../Page/BlackBerry_10.md "wikilink"),
[iOS](../Page/iOS.md "wikilink"),
[Symbian](../Page/Symbian.md "wikilink") 和 [Windows Phone
8](../Page/Windows_Phone_8.md "wikilink") 在内的众多平台上。

2007年10月29日，Skype与[3电信合作](../Page/3_\(电讯\).md "wikilink")，以[3
Skypephone为品牌名发售移动电话](../Page/3_Skypephone_Series.md "wikilink")。该移动电话运行[BREW系统](../Page/BREW.md "wikilink")。\[37\]

Skype同时发售 Skype Wi-Fi Phone，这是一种使用无线网络的无线电话，使用者可以在这种设备上建立 Skype 通话。Skype
Wi-Fi Phone 通过机载OSD菜单可以以类似PC版软件的显示方法显示哪些好友在线，哪些好友可以接听使用者拨打的电话。该设备可以通过
SkypeOut 拨打非 Skype 用户的普通电话号码，拨打费率低且无需按月缴费。Skype Wi-Fi Phone
内没有浏览器，所以无法使用需要网页登陆或网页认证的无线热点。\[38\]

其他Skype正式支持的平台包括：

  - 运行[Maemo的](../Page/Maemo.md "wikilink")[Nokia
    N800](../Page/Nokia_N800.md "wikilink"),
    [N810](../Page/Nokia_N810.md "wikilink") 和
    [N900](../Page/Nokia_N900.md "wikilink") 互联网平板。
  - 运行[MeeGo的](../Page/MeeGo.md "wikilink")[Nokia
    N9](../Page/诺基亚_N9.md "wikilink")，整合了 Skype
    语音通话和文字简讯功能，但不支持视频通话。
  - [Sony mylo](../Page/mylo.md "wikilink") COM-1 和 COM-2。
  - [PlayStation Portable](../Page/PlayStation_Portable.md "wikilink")
    Slim &
    Lite（[PSP-2000](../Page/PSP-2000.md "wikilink")）系列,用户需要购买一枚特殊设计的外接麦克风。[PSP-3000](../Page/PSP-3000.md "wikilink")
    则由于内置麦克风，可无需外接设备，直接进行通话。\[39\][PSP Go](../Page/PSP_Go.md "wikilink")
    内的 Skype
    在内置麦克风以外，可使用[蓝牙设备进行通话](../Page/蓝牙.md "wikilink")。\[40\][PlayStation
    Vita可通过](../Page/PlayStation_Vita.md "wikilink") PlayStation Network
    下载安装 Skype 程序，此平台上的 Skype 可以在后台待机，等待通话拨入。
  - 三星智能电视可以免费下载 Skype 程序。\[41\]
    较新一些电视型号内的程序会使用内置的摄像头和麦克风。而对于一些老一些的型号，用户可以购买内置扬声器和麦克风的
    Skype 摄像头。\[42\]
  - 有些设备被设计用于配合桌面版 Skype 使用，也有些设备直接内嵌 Skype 程序。这些设备有些需要插入 PC 使用，也有些设备内置
    [Wi-Fi](../Page/Wi-Fi.md "wikilink") 客户端可通过 Wi-Fi 热点通话。例如
    [Netgear](../Page/Netgear.md "wikilink") SPH101 Skype Wi-Fi Phone,
    [SMC](../Page/SMC_Networks.md "wikilink") WSKP100 Skype Wi-Fi Phone,
    [Belkin](../Page/Belkin.md "wikilink") F1PP000GN-SK Wi-Fi Skype
    Phone, [Panasonic](../Page/Panasonic.md "wikilink") KX-WP1050 Wi-Fi
    Phone for Skype Executive Travel Set,
    [IPEVO](../Page/IPEVO.md "wikilink") So-20 Wi-Fi Phone for Skype 和
    [Linksys](../Page/Linksys.md "wikilink") CIT200 Wi-Fi Phone。

第三方软件开发商，例如 [Truphone](../Page/Truphone.md "wikilink"),
[Nimbuzz](../Page/Nimbuzz.md "wikilink") 和
[Fring](../Page/Fring_\(VoIP\).md "wikilink")，过去曾允许 Skype 与其他几个 VoIP/IM
网络服务同时出现在他们的 Symbian 和 Java 平台的软件中(Truphone 和 Nimbuzz 提供与 Skype 竞争的
TruphoneOut 的 NimbuzzOut 付费服务)。Nimbuzz 将 Skype 引入
[BlackBerry](../Page/BlackBerry.md "wikilink")，Fring 则不但完成了 Android
平台的支援，它还通过 Skype 网络提供视频通话服务。Skype 在2010年7月停止 Fring 用户使用 Skype
网络，\[43\]Nimbuzz 则于2010年10月停止 Skype 服务。\[44\]

在 Microsoft 购得 Skype 之前，Skype
收回了对几家第三方软硬件开发商的服务许可。[Digium](../Page/Digium.md "wikilink")
旗下 [Asterisk](../Page/Asterisk_\(PBX\).md "wikilink")
产品的许可以“不再提供销售”为名被撤回。[Senao](../Page/Senao_International,_Co.,_Ltd..md "wikilink")
SN358+ 远程(10–15公里)无线电话由于失去 Skype 许可，无法继续成为 Skype
网络节点而被中止。这两款设备结合使用可以经由可靠设备间的信息传递建立可漫游的无线网状网络。

### Skype网络协议

Skype采用名为[Skype
protocol的私有化网络协议](../Page/Skype协定.md "wikilink")。Skype并未公开这个协议的内容，采用这个协议的正式软件也均属于[专有软件](../Page/专有软件.md "wikilink")。协议的部分内容依赖于[Joltid
Ltd.公司的](../Page/Joltid_Ltd..md "wikilink")[Global Index
P2P协议](../Page/Global_Index_P2P.md "wikilink")。Skype与标准VoIP区别在于，不同于通常使用的[主从式架构](../Page/主从式架构.md "wikilink")(client–server
model)模型，Skype网络使用[对等网络](../Page/对等网络.md "wikilink")（peer-to-peer）模型（该模型起源于[Kazaa软件](../Page/Kazaa.md "wikilink")\[45\])）。

需要指出的是，[SIP这一非常受欢迎的VoIP模型](../Page/Session_Initiation_Protocol.md "wikilink")，同Skype一样，同属于需要向服务器注册的对等网络模型。

Skype尽管对支持网络堆叠表示关注，但该软件依然仅支持[IPv4协议](../Page/IPv4.md "wikilink")。目前尚欠缺对下一代互联网协议[IPv6的支持](../Page/IPv6.md "wikilink")。\[46\]

### Skype网络的探测和控制

很多网络和安全公司都声称企业和运营商可以使用它们的软件探测和控制Skype协议。尽管这些探测法一般不公开，[皮尔森卡方检定和](../Page/皮尔森卡方检定.md "wikilink")[朴素贝叶斯分类器是](../Page/朴素贝叶斯分类器.md "wikilink")2008年发布的两种可用的处理方法。\[47\]
结合负载特性（例如字节频率和初始字节序列）的统计测量和网络流特点（例如网络包的大小和网络包的方向）进行识别也被证明是探测Skype基于TCP和UDP协议的有效办法。\[48\]

### 语音编解码器

在采用[G.729和](../Page/G.729.md "wikilink")[SVOPC技术之余](../Page/SVOPC.md "wikilink")，Skype从Windows
4.0版本开始向同期所有客户端加入了自己研发的[SILK音频编解码器](../Page/SILK.md "wikilink")。\[49\]
\[50\] SILK 具有“轻量”和“可嵌入”的特点。\[51\]
此外，Skype还发布了名为[Opus的开源编解码器](../Page/Opus_codec.md "wikilink")。Opus在人声方面使用了SILK编解码器的相关技术细节；在需要高质量声音传输的场合，例如现场音乐表演方面，继承了[CELT编解码器的相关技术原理](../Page/CELT.md "wikilink")。Opus在2010年9月被提交给[互联网工程任务组](../Page/互联网工程任务组.md "wikilink")
(Internet Engineering Task Force,IETF) 。\[52\] 后该编解码器成为RFC 6716标准。\[53\]

### 视频编解码器

Skype
5.5之前的版本使用[VP7视频编解码器](../Page/VP7.md "wikilink")。\[54\]\[55\]\[56\]\[57\]\[58\]

从5.7版本开始，Skype使用[VP8作为标准清晰度下群组和个人视频通话的编解码器](../Page/VP8.md "wikilink")，\[59\]
使用[H.264](../Page/H.264/MPEG-4_AVC.md "wikilink")
作为[720p和](../Page/720p.md "wikilink")[1080p高清下群组和个人视频通话的编解码器](../Page/1080p.md "wikilink")。\[60\]\[61\]

### Skype Qik

Skype在2011年收购视频服务Qik。2014年4月关闭服务以后，Skype在2014年10月14日以[Skype
Qik的名称重新上线这一服务](../Page/Skype_Qik.md "wikilink")。不同于原Qik服务所提供的视频会议和网络流媒体功能，新Skype
Qik服务着重于个人与团体使用移动设备交流视频信息。\[62\]

## Skype 翻译

为了打破语音隔阂，自2014年底微软首度释出Skype 翻译（[Skype
Translator](../Page/Skype_Translator.md "wikilink")）公开预览版给开发者，率先支援英文和西班牙文的语音翻译，后来又扩增中文、意大利文两种语系。2015年10月2日正式发布Skype
翻译，未来将可在Windows 7/8/10系统环境下免费使用这个功能。

目前Skype
翻译支援六种语系：[英文](../Page/英文.md "wikilink")、[法文](../Page/法文.md "wikilink")、[德文](../Page/德文.md "wikilink")、[义大利文](../Page/义大利文.md "wikilink")、[中文以及](../Page/中文.md "wikilink")[西班牙文的语音互译](../Page/西班牙文.md "wikilink")，以及50种语系的文字讯息翻译。

## Skype 网页版

不需要安装任何软件，只要打开浏览器，登入Skype.com就可以使用Skype。支援Internet
Explorer、Chrome、Firefox、Safari等浏览器。

不过目前还只是Beta测试版。

## 語言

Skype自带以下語系：[阿拉伯语](../Page/阿拉伯语.md "wikilink")，[保加利亚语](../Page/保加利亚语.md "wikilink")，[加泰罗尼亚语](../Page/加泰罗尼亚语.md "wikilink")，[中文](../Page/中文.md "wikilink")（[繁体中文和](../Page/繁体中文.md "wikilink")[简体中文](../Page/简体中文.md "wikilink")），[克罗地亚语](../Page/克罗地亚语.md "wikilink")，[捷克语](../Page/捷克语.md "wikilink")，[丹麦语](../Page/丹麦语.md "wikilink")，[荷兰语](../Page/荷兰语.md "wikilink")，[爱沙尼亚语](../Page/爱沙尼亚语.md "wikilink")，[芬兰语](../Page/芬兰语.md "wikilink")，[法语](../Page/法语.md "wikilink")，[德语](../Page/德语.md "wikilink")，[希腊语](../Page/希腊语.md "wikilink")，[希伯来语](../Page/希伯来语.md "wikilink")，[匈牙利语](../Page/匈牙利语.md "wikilink")，[印尼语](../Page/印尼语.md "wikilink")，[意大利语](../Page/意大利语.md "wikilink")，[日语](../Page/日语.md "wikilink")，[韩语](../Page/韩语.md "wikilink")，[拉脱维亚语](../Page/拉脱维亚语.md "wikilink")，[立陶宛语](../Page/立陶宛语.md "wikilink")，[尼泊尔语](../Page/尼泊尔语.md "wikilink")，[挪威语](../Page/挪威语.md "wikilink")，[波兰语](../Page/波兰语.md "wikilink")，[葡萄牙语](../Page/葡萄牙语.md "wikilink")（[巴西葡萄牙语和](../Page/巴西葡萄牙语.md "wikilink")[欧洲葡萄牙语](../Page/葡萄牙语.md "wikilink")），[罗马尼亚语](../Page/罗马尼亚语.md "wikilink")，[俄语](../Page/俄语.md "wikilink")，[塞尔维亚语](../Page/塞尔维亚语.md "wikilink")，[斯洛伐克语](../Page/斯洛伐克语.md "wikilink")，[斯洛文尼亚语](../Page/斯洛文尼亚语.md "wikilink")，[西班牙语](../Page/西班牙语.md "wikilink")，[瑞典语](../Page/瑞典语.md "wikilink")，[泰语](../Page/泰语.md "wikilink")，[土耳其语](../Page/土耳其语.md "wikilink")，[乌克兰语](../Page/乌克兰语.md "wikilink")，以及[越南语](../Page/越南语.md "wikilink")。

由于Skype（Windows版）程序允许用户自行创建新语种文件，至少31种其他语种（全部或部分）翻译版本可供下载[阿迪格语](../Page/阿迪格语.md "wikilink")，[南非语](../Page/南非语.md "wikilink")，[阿尔巴尼亚语](../Page/阿尔巴尼亚语.md "wikilink")，[阿拉贡语](../Page/阿拉贡语.md "wikilink")，[亚美尼亚语](../Page/亚美尼亚语.md "wikilink")，[巴斯克语](../Page/巴斯克语.md "wikilink")，[白俄罗斯语](../Page/白俄罗斯语.md "wikilink")，[波斯尼亚语](../Page/波斯尼亚语.md "wikilink")，[布列塔尼语](../Page/布列塔尼语.md "wikilink")，[楚瓦什语](../Page/楚瓦什语.md "wikilink")，[康沃尔语](../Page/康沃尔语.md "wikilink")，[厄尔兹亚语](../Page/厄尔兹亚语.md "wikilink")，[世界语](../Page/世界语.md "wikilink")，[法罗群岛语](../Page/法罗群岛语.md "wikilink")，[格鲁吉亚语](../Page/格鲁吉亚语.md "wikilink")，[爱尔兰语](../Page/爱尔兰语.md "wikilink")，[高棉语](../Page/高棉语.md "wikilink")，[利古里亚语](../Page/利古里亚语.md "wikilink")，[马其顿语](../Page/马其顿语.md "wikilink")，[米兰德斯语](../Page/米兰德斯语.md "wikilink")，[尼亚斯语](../Page/尼亚斯语.md "wikilink")，[挪威语](../Page/挪威语.md "wikilink")，[普法尔茨德语](../Page/普法尔茨德语.md "wikilink")，[波斯语](../Page/波斯语.md "wikilink")，[苏格兰盖尔语](../Page/苏格兰盖尔语.md "wikilink")，[塔吉克语](../Page/塔吉克语.md "wikilink")，[泰米尔语](../Page/泰米尔语.md "wikilink")，[泰卢固语](../Page/泰卢固语.md "wikilink")，[维吾尔语（波斯文维吾尔语和拉丁字母维吾尔语）](../Page/维吾尔语.md "wikilink")，[威尔士语](../Page/威尔士语.md "wikilink")。\[63\]

## Skype的URIs（统一资源标志符）

Skype的[统一资源标志符为](../Page/统一资源标志符.md "wikilink") <skype:USER?call>

## 客户服务

从2012年2月开始，Skype 通过在线入口、支持社区、Twitter 上的 @skypesupport 账户和 Facebook 上面的
Skype 专页共同向用户提供支持服务。在线入口支持用户通过电子邮件和实时聊天两种方式提交反馈。其中实时聊天是一项仅供 Skype
高级用户和一些付费用户使用的额外服务。

2010年1月，Skype
取消了回收非活跃账号内余额的政策，非活跃用户指180天无付费通话的用户。这次调整是一场集团诉讼中和解方案的一部分。\[64\]
Skype 同时为参与集团诉讼的账户提供4美元的补偿。

Skype
的退款政策指出，在用户充值后，当用户提出退款请求时，如果用户所使用的点数少于1欧元，Skype会提供全额退款。“在正式提交请求后，Skype
会按未使用产品的占比退款。”

Skype 因为无法完全关闭用户的账号而受到批评。Skype 用户可以通过删除除用户名以外所有个人信息的方式冻结他们的账号。\[65\]

## 教育使用

尽管 Skype 是一款商业软件，它的免费版本正在越发频繁的被关心全球教育项目的老师和学校使用。\[66\]比如，Skype
正在用于促进不同语种用户间的互相学习。分居世界不同地域的学生被分成一个个两人一组的小组，小组中每个人都是另一个人想学习语言的母语使用者。通过
Skype，他们可以轮流教授这两种语言。\[67\]\[68\]\[69\]

教师正在以一种独特的方法使用 Skype 实现教学目标。这些老师认为 Skype
软件中视频会议的部分非常有用。因为通过这个功能，使用不同语言的学生可以联系到一起，进行虚拟的实地考察，接触不同领域的专家。这使得学生可以实际应用他们在课堂上所学习到的知识，并为他们赢得未来的学习机会。\[70\]\[71\]

Skype in the classroom（直译为“课室中的Skype”）是 Skype
设置在其网站上的另一项免费工具。该工具可以使老师的课堂更加具有交互性、更加有趣。Skype
in the classroom
是一项老师可以登记使用的服务，它允许学生可以在线上与其他同学见面，与专家交谈并交流感想。老师可以在线上与世界上其他老师合作，为学生带来不一样的学习体验。学生可以参加不同的
Skype 课程。老师也可在线上选择特定领域的专家，这些专家已经预先选好他们所擅长的领域。\[72\]

## 费用

虽然是免费软件，但是某些功能还需要另行支付费用，例如打普通电话。Skype现在也有很多种选择，计时卡、套餐卡和定向卡等。\[73\]

### 点数

拨打普通电话，每分钟1.1美分\[74\]起。
透过点数拨打普通电话和使用来电转接，每通需加收一笔「话务接取服务费」，费用依地区而定。以台湾为例，目前市内电话1.451新台币/通(或0.036欧元/通)、行动电话2.635新台币/通(或0.066欧元/通)。大陸不论固網电话或手提电话均为0.3欧元/通。

### 套餐

套餐可以每月使用固定分钟数。套餐服务不会被加收转接费，因为费用已经包含在内。

### 货币

可使用货币：[新台幣](../Page/新台幣.md "wikilink")、[港幣](../Page/港幣.md "wikilink")、[美元](../Page/美元.md "wikilink")、[日元](../Page/日元.md "wikilink")、[韓幣](../Page/韓元.md "wikilink")、[英鎊](../Page/英鎊.md "wikilink")、[加幣](../Page/加拿大元.md "wikilink")、[歐羅](../Page/歐羅.md "wikilink")、[澳元](../Page/澳元.md "wikilink")、[丹麥克朗](../Page/丹麥克朗.md "wikilink")、[挪威克朗](../Page/挪威克朗.md "wikilink")、[波蘭茲羅提](../Page/波蘭茲羅提.md "wikilink")、[瑞典克朗](../Page/瑞典克朗.md "wikilink")、[瑞士法郎](../Page/瑞士法郎.md "wikilink")、[巴西雷亞爾和](../Page/巴西雷亞爾.md "wikilink")[人民币](../Page/人民币.md "wikilink")。

## 缺點

  - 使用80 Port穿透防火牆。\[75\]
  - 通話雖然加密，但Skype仍被美國[國安局](../Page/國安局.md "wikilink")[稜鏡計畫監控](../Page/稜鏡計畫.md "wikilink")，中国版Skype服務仍被中国政府監控和過濾。（参阅[中國版Skype服務](../Page/#中国服务.md "wikilink")）
  - 非開源通訊協議及軟件威脅使用者隱私。\[76\]
  - 客戶資金如果180日未使用則過期，很多客人發現很難從Skype退款。\[77\]\[78\]
  - 不提供技術支持及客戶服務電話，針對其服務及收費的投訴相當多。\[79\]\[80\]
  - 目前Skype TM也会有推荐Skype付费项目的广告。\[81\]
  - 當機事件：於2013年7月4日上午，在[台灣](../Page/台灣.md "wikilink")、[新加坡和](../Page/新加坡.md "wikilink")[馬來西亞等地区出現Skype大當機的現象](../Page/馬來西亞.md "wikilink")，使用者的帳號登入後發現聯絡人全部為離線狀態。\[82\]
  - 曾因駭客入侵使用者帳號，官方因為過度煩惱的垃圾郵件和資訊安全問題而封鎖大陸使用者。而回復帳號則需列舉三個自己的好友並且填寫冗長的回復表格。

[缩略图](https://zh.wikipedia.org/wiki/File:Skype-reply.JPG "fig:缩略图")

## 安全和隐私

### 美國國安局

美國[國安局透過](../Page/國安局.md "wikilink")[稜鏡計劃大規模竊聽](../Page/稜鏡計劃.md "wikilink")，該計劃文件指出，美國國安局可以對即時通信和既存資料進行深度的監聽並監控所有Skype帳號及通話\[83\]。

### 中国服务

Skype在中国被全面封鎖，目前只有企業版可以使用。

2005年9月，有中国媒体报道，
中国的固网电话运营商[中国电信停止一些互联网公司提供的网络电话服务](../Page/中国电信.md "wikilink")，例如停止为电脑使用者提供登录Skype的网站的服务。中国电信深圳分公司就曾经对使用网络电话使用者中断服务，并且要他们必须保证不再使用网络电话才能恢复服务\[84\]\[85\]。同时TOM集团旗下北京讯能网-{}-络有限公司TOM在-{}-线和Skype于2004年11月在中國联合推出的Skype中国版即时通讯软件。从2007年开始，访问Skype官方网站skype.com的大陆用户会被重定向至Tom版Skype网站skype.tom.com。截至2007年6月底，TOM-Skype注册用户已经突破4200万，超越美国成为Skype全球最大的市场。TOM在-{}-线也为大陆使用者推出了几款仅能在[TOM-Skype使用的中國特别优惠卡和电话月费计划](../Page/TOM-Skype.md "wikilink")（如Skype国内卡，国际版Skype无法使用）。然而由于TOM-Skype有文字审查过滤系统，所以那软件的安全和隐私一直备受诟病。

2008年10月，[加拿大](../Page/加拿大.md "wikilink")[多伦多大学Citizen实验室的研究人员发现中國版TOM](../Page/多伦多大学.md "wikilink")-Skype（非简体中文国际版）会根据关键字将使用者的文字聊天记录和使用者信息上传并存放在可以从公司外部进入的8个大陆服务器上，被记录的聊天信息包含了与政治相关的内容。由于缺乏足够保密措施，这些记录很容易被第三方获取，从而威胁到了用户的隐私\[86\]\[87\]\[88\]。

Skype指TOM-Skype在2006年已經按照[中华人民共和国政府的规定](../Page/中华人民共和国政府.md "wikilink")，使用文本过滤器来监控敏感信息。不过包含敏感关键字的信息会在客户端被丢弃，故对用户的隐私并无威胁。不过，TOM-Skype后来在未经Skype同意的情况下擅自改变了该过滤器的功能，存储了部分用户的信息。随后，Skype的总裁Josh
Silverman在Skype官方网志上发表声明，表示自己公司很“无辜”和“无奈”\[89\]。最后，Skype作出了道歉声明\[90\]。

2013年11月8日，TOM在线在TOM-Skype页面发布公告，称于2013年11月24日正式将Skype在中国大陆的运营权移交微软\[91\]；之后，微软Skype亚太区市场宣传部门主管朱特·哈康博（Judd
Harcombe）在给英国科技媒体The
Register的一份声明中表示，微软不会亲自接手Skype的在华业务，而是依旧交由中國的合资公司运作\[92\]。2013年11月14日，Skype中国官方网站已经不再重定向至，而是直接打开。

2013年11月26日，《[光明日报](../Page/光明日报.md "wikilink")》和[方正集团的](../Page/方正集团.md "wikilink")[合资公司光明方正与微软正式宣布达成战略合作](../Page/合资.md "wikilink")，开始负责運作中国市场。\[93\]目前在中國访问Skype官网会被重定向至。官方聲称新的大陸版Skype不再像TOM在-{}-线时代那样内置关键词过滤和在中國服务器上储存使用者的聊天记录，改为使用境外的微软服务器並且使用加密傳送的數據，但仍受到很多怀疑。\[94\]

2017年10月，Skype个人版从苹果App Store中国区被下架，苹果发言人回应《纽约时报》称是应中国公安部要求下架\[95\]。

2017年11月，Skype个人版全面被下架，不管手機是Apple、Android安卓，還是小米華為，全部都已經下架。只剩企業版可使用。\[96\]

### 印度境内Skype停止拨打普通电话服务

Skype的母公司微软，在2014年10月8日的一份声明中称“自2014年11月10日起，印度境内用户将无法拨打-{}-印度固定电话号码和移动电话号码”。微软称“印度境内用户可继续使用免费的Skype-to-Skype通话，也可拨打海外固定电话号码和移动电话号码进行国际长途通话。印度境外用户也可正常拨打-{}-印度固定电话号码和移动电话号码”。\[97\]

## 從MSN轉移到Skype

由於Skype內建的匯入功能，只能取得[MSN的聯絡人資料](../Page/Windows_Live_Messenger.md "wikilink")，卻無法取得MSN的群組資料。故此有[MVP黑暗執行緒](../Page/微软最有价值专家.md "wikilink")，開發了Skype
MSN連絡人群組匯入工具。\[98\]\[99\]

## 相關

  - [即时通讯软件列表](../Page/即时通讯软件列表.md "wikilink")
  - [即时通讯软件比较](../Page/即时通讯软件比较.md "wikilink")
  - [尼可拉斯·詹士莊](../Page/尼可拉斯·詹士莊.md "wikilink")（Niklas Zennström）
  - [雅努斯·傅易斯](../Page/雅努斯·傅易斯.md "wikilink")（Janus Friis）
  - [Wickr](../Page/Wickr.md "wikilink")(Wickr)

## 参考资料

## 外部链接

  - [Skype](https://www.skype.com/intl/zh-Hant/)

  - [Skype](https://www.skype.com/intl/zh-Hans/)

  - [Skype](https://www.skype.com/intl/en/)

  - [香港HGC-Skype](https://web.archive.org/web/20061103115617/http://skype.hgcbroadband.com/b5/index.htm)


  - [台灣PChome-Skype](https://web.archive.org/web/20050507045804/http://skype.pchome.com.tw/index.htm)


  - [光明方正Skype](http://skype.gmw.cn)

  -
  -
  -
  -
  -
[Category:即时通讯软件](../Category/即时通讯软件.md "wikilink")
[Category:跨平台軟體](../Category/跨平台軟體.md "wikilink")
[Category:2003年软件](../Category/2003年软件.md "wikilink")
[Category:P2P](../Category/P2P.md "wikilink")
[Category:VoIP軟體](../Category/VoIP軟體.md "wikilink")
[Category:IP電話公司](../Category/IP電話公司.md "wikilink")
[Category:免費軟件](../Category/免費軟件.md "wikilink")
[Category:付費軟件](../Category/付費軟件.md "wikilink")
[Category:微軟软件](../Category/微軟软件.md "wikilink")
[Category:MacOS軟體](../Category/MacOS軟體.md "wikilink")
[Category:Linux](../Category/Linux.md "wikilink")
[Category:Windows即時通訊客戶端](../Category/Windows即時通訊客戶端.md "wikilink")
[Category:Android软件](../Category/Android软件.md "wikilink")
[Category:IOS软件](../Category/IOS软件.md "wikilink") [Category:Windows
Phone軟體](../Category/Windows_Phone軟體.md "wikilink")
[Category:使用Qt的軟體](../Category/使用Qt的軟體.md "wikilink")
[Category:Skype](../Category/Skype.md "wikilink") [Category:Modern
UI應用程式](../Category/Modern_UI應用程式.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.

9.

10.

11.

12.

13.

14.
15.

16.

17.

18.

19. [MSN今熄燈　Skype卻無法登入挨轟](http://www.appledaily.com.tw/realtimenews/article/life/20130425/176854/)，蘋果日報，2013年4月25日

20.

21. [Skype推出Java客户端大幅增加支持手机](http://news.mydrivers.com/1/104/104718.htm)

22.

23. [Microsoft to Buy Skype for $8.5
    Billion](http://dealbook.nytimes.com/2011/05/10/microsoft-to-buy-skype-for-8-5-billion/)

24.

25.
26.

27.

28.

29.

30.

31.

32.

33.

34.

35.

36.

37.

38.

39.

40.

41.

42.

43.

44.

45.

46.

47.

48.

49. [SILK](http://developer.skype.com/silk)

50. [SILK Speech Codec](http://tools.ietf.org/html/draft-vos-silk-02)

51.

52.

53.

54.

55.

56.

57.

58.

59. [One-to-One VP8 Video Calling Now Supported in
    Skype](http://blog.webmproject.org/2011/08/one-to-one-vp8-video-calling-now.html)

60.

61.

62.

63. [SkypeInYourLanguage](https://sourceforge.net/projects/skypeinyourlang)

64.

65. [How to delete a Skype
    account?](http://en.kioskea.net/faq/30642-how-to-delete-a-skype-account)

66.

67. Language Magazine
    [article](http://languagemagazine.com/?page_id=2565)

68. [LinguaeLive.ca](http://www.linguaelive.ca/)

69. [the language exchange](http://www.language-exchanges.org/)

70.

71. [Video Conferencing with
    Skype](http://www.edtechteacher.org/index.php/teaching-technology/discussion-ollaboration/25-video-conferencing)


72.

73. [从美国打中国电话各种方式总结（包括手机、电话卡、skype等）](http://www.renrencall.com/2012/09/tel-china-from-usa/)

74. [费率_Skype简体中文版官方网站-清晰的免费网络电话](http://skype.gmw.cn/product/getMoreRates.html)

75. [80 Port穿透防火牆](http://www.tkwu.net/tkwu/pictures/skype2.png)

76. [FSF High Priority Projects
    非開源通訊協議及軟體威脅使用者隱私](http://www.fsf.org/campaigns/priority.html#skypereplacement)

77. [Skype
    法律政策、条款和条件](http://www.skype.com/legal/terms/voip/#payment_issues)
    Credit expiration

78. [skype forum
    topic](http://forum.skype.com/index.php?showtopic=104480)

79.

80. [skype poor customer
    service](http://blog.tmcnet.com/blog/tom-keating/skype/skype-poor-customer-service.asp)

81. [目前Skype
    TM也會有推薦Skype付費項目的廣告。](http://clip2net.com/clip/m96404/1316513444-clip-15kb.jpg)

82. [Skype大當機聯絡人全離線](http://tw.news.yahoo.com/%E5%8F%B0%E6%98%9F%E9%A6%ACskype%E5%A4%A7%E7%95%B6%E6%A9%9F-%E8%81%AF%E7%B5%A1%E4%BA%BA%E5%85%A8%E9%9B%A2%E7%B7%9A-222806309.html)


83. [FBI欲监听Skype等网络通信专家称或带来安全威胁](http://world.huanqiu.com/regions/2013-05/3956840.html)

84.

85.

86.

87.

88.

89.

90.

91.

92.

93.

94.

95.

96.

97.

98.

99.