**秦祥林**（，），[香港](../Page/香港.md "wikilink")、[臺灣](../Page/臺灣.md "wikilink")[電影](../Page/電影.md "wikilink")[演員](../Page/演員.md "wikilink")，[香港长大](../Page/香港.md "wikilink")，生於[南京](../Page/南京.md "wikilink")，祖籍[湖北](../Page/湖北.md "wikilink")[黄冈](../Page/黄冈.md "wikilink")。

## 生平

[秦祥林幼時隨家人逃難至香港](../Page/秦祥林.md "wikilink")，居於[調景嶺](../Page/調景嶺.md "wikilink")。十二歲時，到[台灣求學](../Page/台灣.md "wikilink")，在[復興劇校學習](../Page/復興劇校.md "wikilink")[平劇](../Page/平劇.md "wikilink")，是第一屆學生，按[字輩起名為秦復林](../Page/字輩.md "wikilink")，所學的行當為[武生](../Page/武生.md "wikilink")，專長樂器是[月琴](../Page/月琴.md "wikilink")\[1\]\[2\]。他在校時跑了八年龍套，節奏感不佳，是以頗為自卑。然而他當時所練習的基本功，倒是為其後演出電影打下良好的基礎\[3\]。

1968年[秦祥林劇校畢業後](../Page/秦祥林.md "wikilink")，返回[香港拍了一些武打片](../Page/香港.md "wikilink")，但始終默默無聞。在[香港期間曾經在](../Page/香港.md "wikilink")[國泰電影公司](../Page/國泰電影公司.md "wikilink")、[電懋電影公司演出](../Page/電懋電影公司.md "wikilink")。直到1973年到台湾，由當代紅遍台港日東南亞的玉女巨星[甄珍提攜下](../Page/甄珍.md "wikilink")，[秦祥林首次擔任男主角所主演的](../Page/秦祥林.md "wikilink")《[心有千千结](../Page/心有千千结.md "wikilink")》後一夕成名，兩人再度合作《[一簾幽夢](../Page/一簾幽夢.md "wikilink")》《[婚姻大事](../Page/婚姻大事.md "wikilink")》後，[秦祥林更是爆紅後才逐漸開展了二秦二林文藝片時期](../Page/秦祥林.md "wikilink")。
《雪花片片》使他獲得了第20属[亚洲影展演技優秀男主角獎](../Page/亚洲影展.md "wikilink")。當時的故事題材主要是[瓊瑤的愛情小說](../Page/瓊瑤.md "wikilink")。
他曾與[甄珍](../Page/甄珍.md "wikilink")、[蕭芳芳](../Page/蕭芳芳.md "wikilink")、[林青霞](../Page/林青霞.md "wikilink")、[沈殿霞等人合作](../Page/沈殿霞.md "wikilink")，並且是[銀色鼠隊的成員之一](../Page/銀色鼠隊.md "wikilink")。及後，[秦祥林曾經分別在](../Page/秦祥林.md "wikilink")1975年（第十二屆[金馬獎](../Page/金馬獎.md "wikilink")）和1977年（第十四屆[金馬獎](../Page/金馬獎.md "wikilink")）憑著《[長情萬縷](../Page/長情萬縷.md "wikilink")》和《[人在天涯](../Page/人在天涯.md "wikilink")》獲得[最佳男主角](../Page/最佳男主角.md "wikilink")。感情方面，他曾與[蕭芳芳維持了三年的婚姻](../Page/蕭芳芳.md "wikilink")。（1975年結婚，1977年協議分居，1978年完成當時香港法律上分居期滿的離婚程序）1980年9月5日。[秦祥林与](../Page/秦祥林.md "wikilink")[林青霞在美国](../Page/林青霞.md "wikilink")[旧金山宣布订婚](../Page/旧金山.md "wikilink")，1984年由林青霞提出解除婚约。現在已經再婚並育有二子，居於[美國](../Page/美國.md "wikilink")[洛杉磯](../Page/洛杉磯.md "wikilink")。
三廳電影全盛時期，[秦祥林與](../Page/秦祥林.md "wikilink")[秦漢](../Page/秦漢_\(藝人\).md "wikilink")、[林青霞](../Page/林青霞.md "wikilink")、[林鳳嬌](../Page/林鳳嬌.md "wikilink")，合稱「二秦二林」。2013年11月23日，[秦祥林接受](../Page/秦祥林.md "wikilink")[臺北金馬影展執行委員會邀請擔任](../Page/臺北金馬影展執行委員會.md "wikilink")[第50屆金馬獎](../Page/第50屆金馬獎.md "wikilink")[終身成就獎頒獎嘉賓](../Page/終身成就獎.md "wikilink")。

## 電影作品

| 年代                                                 | 片名                                               | 角色     |
| -------------------------------------------------- | ------------------------------------------------ | ------ |
| 1968年                                              | 《[夏日初戀](../Page/夏日初戀.md "wikilink")》             |        |
| 1969年                                              | 《[黑豹](../Page/黑豹.md "wikilink")》                 |        |
| 《[一劍情深](../Page/一劍情深.md "wikilink")》               |                                                  |        |
| 《[蓮花寨](../Page/蓮花寨.md "wikilink")》                 |                                                  |        |
| 《[龍吟虎嘯](../Page/龍吟虎嘯.md "wikilink")》               |                                                  |        |
| 《[楓林渡](../Page/楓林渡.md "wikilink")》                 |                                                  |        |
| 1970年                                              | 《[異鄉客](../Page/異鄉客.md "wikilink")》               |        |
| 《[火鳥第一號](../Page/火鳥第一號.md "wikilink")》             |                                                  |        |
| 《[壯士血](../Page/壯士血.md "wikilink")》                 |                                                  |        |
| 《[我愛莎莎](../Page/我愛莎莎.md "wikilink")》               |                                                  |        |
| 《[雪路血路](../Page/雪路血路.md "wikilink")》               |                                                  |        |
| 1971年                                              | 《[浪子之歌](../Page/浪子之歌.md "wikilink")》             |        |
| 《[財色驚魂](../Page/財色驚魂.md "wikilink")》               |                                                  |        |
| 《[無敵鐵沙掌](../Page/無敵鐵沙掌.md "wikilink")》             |                                                  |        |
| 1972年                                              | 《[男人女人](../Page/男人女人.md "wikilink")》             |        |
| 《[珮詩](../Page/珮詩.md "wikilink")》                   |                                                  |        |
| 《[騙術大觀](../Page/騙術大觀.md "wikilink")》               |                                                  |        |
| 《[輕煙](../Page/輕煙.md "wikilink")》                   |                                                  |        |
| 《[三十六彈腿](../Page/三十六彈腿.md "wikilink")》             |                                                  |        |
| 1973年                                              | 《[心有千千結](../Page/心有千千結.md "wikilink")》           | 耿若塵    |
| 《[女警察](../Page/女警察.md "wikilink")》                 |                                                  |        |
| 《[追殺](../Page/追殺.md "wikilink")》                   |                                                  |        |
| 《[大密探](../Page/大密探.md "wikilink")》                 |                                                  |        |
| 1974年                                              | 《[廣島廿八](../Page/廣島廿八.md "wikilink")》             |        |
| 《[長情萬縷](../Page/長情萬縷.md "wikilink")》               | 林維揚                                              |        |
| 《[一簾幽夢](../Page/一簾幽夢.md "wikilink")》               | 楚濂                                               |        |
| 《[雪花片片](../Page/雪花片片.md "wikilink")》               |                                                  |        |
| 《[純純的愛](../Page/純純的愛.md "wikilink")》               | 李愛芳                                              |        |
| 《[我父我夫我子](../Page/我父我夫我子.md "wikilink")》           |                                                  |        |
| 《[半山飄雨半山晴](../Page/半山飄雨半山晴.md "wikilink")》         |                                                  |        |
| 《[婚姻大事](../Page/婚姻大事.md "wikilink")》               | 陳堯                                               |        |
| 《[純情](../Page/純情.md "wikilink")》                   |                                                  |        |
| 1975年                                              | 《[西貢, 台北, 高雄](../Page/西貢,_台北,_高雄.md "wikilink")》 |        |
| 《[我心深處](../Page/我心深處.md "wikilink")》               | 龍小江                                              |        |
| 《[戰地英豪](../Page/戰地英豪.md "wikilink")》               |                                                  |        |
| 《[煙雨](../Page/煙雨.md "wikilink")》                   | 林義章                                              |        |
| 《[金雲夢](../Page/金雲夢.md "wikilink")》                 |                                                  |        |
| 《[長青樹](../Page/長青樹.md "wikilink")》                 | 陸長榮                                              |        |
| 《[女朋友](../Page/女朋友.md "wikilink")》                 | [高淩風](../Page/高淩風.md "wikilink")                 |        |
| 1976年                                              | 《[追球追求](../Page/追球追求.md "wikilink")》             |        |
| 《[情話綿綿](../Page/情話綿綿.md "wikilink")》               |                                                  |        |
| 《[夏日假期玫瑰花](../Page/夏日假期玫瑰花.md "wikilink")》         | 吳宗勇                                              |        |
| 《[微風細雨點點晴](../Page/微風細雨點點晴.md "wikilink")》         |                                                  |        |
| 《[秋歌](../Page/秋歌.md "wikilink")》                   | 殷超凡                                              |        |
| 《[我是一沙鷗](../Page/我是一沙鷗.md "wikilink")》             | 程傑                                               |        |
| 《[今夜我和你](../Page/今夜我和你.md "wikilink")》             |                                                  |        |
| 《[我是一片雲](../Page/我是一片雲.md "wikilink")》             | 孟喬                                               |        |
| 《[不一樣的愛](../Page/不一樣的愛.md "wikilink")》             | 文星雨                                              |        |
| 1977年                                              | 《[異鄉夢](../Page/異鄉夢.md "wikilink")》               |        |
| 《[人在天涯](../Page/人在天涯.md "wikilink")》               | 高志遠                                              |        |
| 《[愛的賊船](../Page/愛的賊船.md "wikilink")》               |                                                  |        |
| 《[奔向彩虹](../Page/奔向彩虹.md "wikilink")》               | 羅敬晨                                              |        |
| 《[風雲人物](../Page/風雲人物.md "wikilink")》               |                                                  |        |
| 《[杜鵑花開時](../Page/杜鵑花開時.md "wikilink")》             |                                                  |        |
| 《[彩雲在飛躍](../Page/彩雲在飛躍.md "wikilink")》             | 何志高                                              |        |
| 《[不要在街上吻我](../Page/不要在街上吻我.md "wikilink")》         |                                                  |        |
| 1978年                                              | 《[真白蛇傳](../Page/真白蛇傳.md "wikilink")》             | 許仙     |
| 《[情竇初開](../Page/情竇初開.md "wikilink")》               | 林權                                               |        |
| 《[踩在夕陽裡](../Page/踩在夕陽裡.md "wikilink")》             |                                                  |        |
| 《[金木水火土](../Page/金木水火土.md "wikilink")》             |                                                  |        |
| 《[又是黃昏](../Page/又是黃昏.md "wikilink")》               | 余夢湖                                              |        |
| 《[月朦朧鳥朦朧](../Page/月朦朧鳥朦朧.md "wikilink")》           | 韋鵬飛                                              |        |
| 《[男孩與女孩的戰爭](../Page/男孩與女孩的戰爭.md "wikilink")》       | 徐牧                                               |        |
| 1979年                                              | 《[一個女工的故事](../Page/一個女工的故事.md "wikilink")》       | 林若可    |
| 《[昨日雨瀟瀟](../Page/昨日雨瀟瀟.md "wikilink")》             |                                                  |        |
| 《[摘星](../Page/摘星.md "wikilink")》                   | 高烈                                               |        |
| 《[成功嶺上](../Page/成功嶺上.md "wikilink")》               |                                                  |        |
| 《[難忘的一天](../Page/難忘的一天.md "wikilink")》             | 沙孟雄                                              |        |
| 《[一片深情](../Page/一片深情.md "wikilink")》               | 高寧                                               |        |
| 《[落花流水春去也](../Page/落花流水春去也.md "wikilink")》         |                                                  |        |
| 1980年                                              | 《[皇天后土](../Page/皇天后土.md "wikilink")》             |        |
| 《[愛的小草](../Page/愛的小草.md "wikilink")》               |                                                  |        |
| 《[晚間新聞](../Page/晚間新聞.md "wikilink")》               |                                                  |        |
| 1981年                                              | 《[愛殺](../Page/愛殺.md "wikilink")》                 | Louie  |
| 《[海軍與我](../Page/海軍與我.md "wikilink")》               |                                                  |        |
| 《[凶榜](../Page/凶榜.md "wikilink")》                   | 張勁強                                              |        |
| 1982年                                              | 《[血戰大二膽](../Page/血戰大二膽.md "wikilink")》           |        |
| 《[殺出西營盤](../Page/殺出西營盤.md "wikilink")》             | 高達夫                                              |        |
| 1983年                                              | 《[最長的一夜](../Page/最長的一夜.md "wikilink")》           |        |
| 《[奇謀妙計五福星](../Page/奇謀妙計五福星.md "wikilink")》（台譯:五福星） | 凡士林                                              |        |
| 1984年                                              | 《[神勇雙響炮](../Page/神勇雙響炮.md "wikilink")》           | 客串-清潔員 |
| 《[上天救命](../Page/上天救命.md "wikilink")》               | 殺手                                               |        |
| 1985年                                              | 《[福星高照](../Page/福星高照.md "wikilink")》             | 花旗參    |
| 《[夏日福星](../Page/夏日福星.md "wikilink")》               | 花旗參                                              |        |
| 1986年                                              | 《[八二三炮戰](../Page/八二三炮戰.md "wikilink")》           |        |
| 《[日內瓦的黃昏](../Page/日內瓦的黃昏.md "wikilink")》           |                                                  |        |
| 1987年                                              | 《[東方禿鷹](../Page/東方禿鷹.md "wikilink")》             |        |
| 1988年                                              | 《[大飯店](../Page/大飯店.md "wikilink")》               |        |
| 《[褲甲天下](../Page/褲甲天下.md "wikilink")》               |                                                  |        |
| 《[亡命鴛鴦](../Page/亡命鴛鴦.md "wikilink")》               |                                                  |        |
| 1992年                                              | 《[五福星撞鬼](../Page/五福星撞鬼.md "wikilink")》           | 花旗參    |
|                                                    |                                                  |        |

### 電視劇

| 年份                                   | 片名                                 | 角色 |
| ------------------------------------ | ---------------------------------- | -- |
| 1986年                                | 《[第四代](../Page/第四代.md "wikilink")》 |    |
| 《[金色山莊](../Page/金色山莊.md "wikilink")》 | 鐘思齊                                |    |
|                                      |                                    |    |

## 獎項

### 金馬獎

<table>
<tbody>
<tr class="odd">
<td><p><strong>年份</strong></p></td>
<td><p><strong>頒獎典禮</strong></p></td>
<td><p><strong>獎項</strong></p></td>
<td><p><strong>作品</strong></p></td>
<td><p><strong>結果</strong></p></td>
</tr>
<tr class="even">
<td><p>1975年</p></td>
<td><p>《第12屆金馬獎》</p></td>
<td><p>最佳男主角</p></td>
<td><p>《長情萬縷》 – 林維揚</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1977年</p></td>
<td><p>《第14屆金馬獎》</p></td>
<td><p>最佳男主角</p></td>
<td><p>《人在天涯》 – 高志遠</p></td>
<td></td>
</tr>
</tbody>
</table>

## 參考資料

<references/>

## 外部連結

  - [開眼電影](https://web.archive.org/web/20070311110548/http://app.atmovies.com.tw/star/star.cfm?action=stardata&starid=s003040941)

  - [友緣相聚
    秦祥林](http://jade.tvb.com/special/where_are_they_now/visits/10.html)

  - [中文電影資料庫─秦祥林](http://www.dianying.com/ft/person/QinXianglin)

  -
  -
[台](../Category/金馬獎最佳男主角獲得者.md "wikilink")
[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[Category:臺灣電影男演員](../Category/臺灣電影男演員.md "wikilink")
[Category:文藝片演員](../Category/文藝片演員.md "wikilink")
[Category:华裔美国人](../Category/华裔美国人.md "wikilink")
[Category:在美國的中華民國人](../Category/在美國的中華民國人.md "wikilink")
[Category:台灣戰後南京移民](../Category/台灣戰後南京移民.md "wikilink")
[Category:台灣戰後湖北移民](../Category/台灣戰後湖北移民.md "wikilink")
[Category:黄冈人](../Category/黄冈人.md "wikilink")
[Category:南京人](../Category/南京人.md "wikilink")
[Xiang祥](../Category/秦姓.md "wikilink")
[Category:國立臺灣戲曲學院校友](../Category/國立臺灣戲曲學院校友.md "wikilink")

1.
2.
3.