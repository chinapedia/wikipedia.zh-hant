## Summary

Author: ABC

## Licensing

Though this image is subject to copyright, its use is covered by the
U.S. fair use laws because:

1.  The exhibition of low-resolution images of logos, **to illustrate
    the television program in question on the English-language
    Wikipedia, hosted on servers in the United States by the non-profit
    Wikimedia Foundation** qualifies as fair use under United States
    copyright law.
2.  It is a low resolution copy of a logo.
3.  It does not limit the copyright owner's rights to sell the TV series
    or said image.
4.  The image is significant for the article as it shows visual from
    series.
5.  No free or public domain images are possible, as this is a logo.
6.  Image is intended as publicity for the network.
7.  It is used for informational purposes only.
8.  Such images are distributed with implicit license for their use in
    discussing the subject that is being promoted.
9.  Image is of considerably lower resolution than the original.
10. Copies made from it will be of very inferior quality.
11. Its use does not detract from either the original image, or from the
    network itself.
12. Image is used solely for the identification of the subject.
13. The further use of this image on Wikipedia is not believed to
    disadvantage the copyright holder in any way.
14. No free equivalent is available or could be created that would serve
    the same encyclopedic purpose.
15. This image is of a low-enough resolution not to supplant any
    possible market role that may at some point in the future be created
    for the image.
16. Low- rather than high-resolution/fidelity is used.
17. This content has been published outside Wikipedia.
18. Use meets general Wikipedia content requirements and is
    encyclopedic.
19. Use meets Wikipedia's media-specific policy.
20. This content is used in at least one article.
21. This content contributes significantly to an article.
22. Use significantly increases readers' understanding of the topic in a
    way that words alone cannot.
23. Use is not merely decorative and is thus acceptable.
24. Use aids the identification of the subjwct, which was independently
    discussed in many publications outside of Wikipedia.
25. This content is not used on templates.
26. This content is not replaceable by a free alternative image because
    allall visual appearances are from modern copyrighted works.
27. This content is also not replaceable by free text as the
    comprehension of a discussion would be impaired by the lack of an
    image of this subject.