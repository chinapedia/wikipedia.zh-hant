**梁固**（），字仲坚，出生于郓州须城（今[山东](../Page/山东.md "wikilink")[东平](../Page/东平.md "wikilink")）。

年少时，著有《漢春秋》，受其父赞赏。以父遗荫受赐进士出身，梁固不肯接受，直接参加科举乡试。[宋真宗](../Page/宋真宗.md "wikilink")[大中祥符二年](../Page/大中祥符.md "wikilink")（1009年）[状元](../Page/状元.md "wikilink")，授将作监丞，入值史馆。曾任[户部](../Page/户部.md "wikilink")[判官](../Page/判官.md "wikilink")[勾院等职](../Page/勾院.md "wikilink")。与其父[梁灏一起被称为](../Page/梁灏.md "wikilink")[父子状元](../Page/父子状元.md "wikilink")，三十三岁因病早逝。著有《梁司农文集》十卷。

## 参考

  - 《[宋史](../Page/宋史.md "wikilink")·梁灏传》

[L](../Category/宋朝人.md "wikilink") [L](../Category/宋朝狀元.md "wikilink")
[Category:大中祥符二年己酉科進士](../Category/大中祥符二年己酉科進士.md "wikilink")
[L](../Category/东平人.md "wikilink") [G](../Category/梁姓.md "wikilink")