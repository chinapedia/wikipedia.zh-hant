**新北区**是[中国](../Page/中国.md "wikilink")[江苏省](../Page/江苏省.md "wikilink")[常州市所辖的一个](../Page/常州市.md "wikilink")[市辖区](../Page/市辖区.md "wikilink")。

## 行政区划

新北区辖3个街道、7个镇：河海街道、三井街道、龙虎塘街道；新桥镇、薛家镇、罗溪镇、西夏墅镇、春江镇、[孟河镇](../Page/孟河镇.md "wikilink")、[奔牛镇](../Page/奔牛镇.md "wikilink")。

## 外部链接

  - [常州市新北区政府网站](http://www.czxd.gov.cn/)

## 参考文献

[新北区](../Category/新北区.md "wikilink") [区](../Category/常州区市.md "wikilink")
[常州](../Category/江苏市辖区.md "wikilink")