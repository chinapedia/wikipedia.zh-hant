**奧克斯納德** （Oxnard,
California）是[美國](../Page/美國.md "wikilink")[加利福尼亞州](../Page/加利福尼亞州.md "wikilink")[文圖拉縣最大的城市](../Page/文圖拉縣_\(加利福尼亞州\).md "wikilink")，臨[太平洋](../Page/太平洋.md "wikilink")。面積94.8平方公里，2006年人口184,463人。\[1\]

1903年6月30日建市。

## 参考文献

<div class="references-small">

<references />

</div>

[Oxnard](../Category/文圖拉縣城市.md "wikilink")

1.  [Oxnard, California - Population Finder - American
    FactFinder](http://factfinder.census.gov/servlet/SAFFPopulation?_event=Search&geo_id=16000US0678582&_geoContext=01000US%7C04000US06%7C16000US0678582&_street=&_county=Oxnard+city&_cityTown=Oxnard+city&_state=04000US06&_zip=&_lang=en&_sse=on&ActiveGeoDiv=geoSelect&_useEV=&pctxt=fph&pgsl=160&_submenuId=population_0&ds_name=null&_ci_nbr=null&qr_name=null&reg=null%3Anull&_keyword=&_industry=)