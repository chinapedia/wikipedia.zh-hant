**箭齒龍**（[學名](../Page/學名.md "wikilink")：*Belodon*）意為「箭狀牙齒」，是[植龍目的一個](../Page/植龍目.md "wikilink")[屬](../Page/屬.md "wikilink")，生存於[三疊紀晚期](../Page/三疊紀.md "wikilink")，化石發現於[歐洲](../Page/歐洲.md "wikilink")。[模式種是](../Page/模式種.md "wikilink")*B.
plieningeri*，是在1844年由[克莉斯汀·艾瑞克·赫爾曼·汪邁爾](../Page/克莉斯汀·艾瑞克·赫爾曼·汪邁爾.md "wikilink")（Christian
Erich Hermann von
Meyer）命名。在19世紀晚期到20世紀早期，劍齒龍常被視為是[植龍](../Page/植龍.md "wikilink")、[劍鼻鱷的](../Page/劍鼻鱷.md "wikilink")[異名](../Page/異名.md "wikilink")。

之後有許多種被歸類於箭齒龍，包含：*B. buceros*\[1\]、*B. kapfii*、*B. lepturus*\[2\]、*B.
priscus*\[3\]、*B. scolopax*\[4\]、*B. validus*\[5\]。但目前有模式種具有效力。

## 參考文獻

<div class="references-small">

<references>

</references>

</div>

## 外部連結

  - <https://web.archive.org/web/20070328204157/http://www.dinosauria.com/dml/names/phyto.htm>

[Category:植龍目](../Category/植龍目.md "wikilink")

1.  Cope, E.D. (1881). "*Belodon* in New Mexico". *American Naturalist*
    15: 922-923.

2.  Cope, E.D. (1870). "Reptilia of the Triassic Formation of the United
    States". *American Naturalist* 4: 562-563.

3.  Leidy, J. (1856). "Notice of some remains of extinct vertebrated
    animals". *Proceedings of the Academy of Natural Sciences of
    Philadelphia* 163-165.

4.
5.  Marsh, O.C. (1893). "Restoration of *Anchisaurus*". *The American
    Journal of Science*. Series 3 45: 169-170.