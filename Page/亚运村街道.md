**亚运村街道**是隶属于[北京市](../Page/北京.md "wikilink")[朝阳区的](../Page/朝阳区_\(北京市\).md "wikilink")[街道](../Page/街道.md "wikilink")。

## 简介

1990年[第十一届亚洲运动会在北京召开时](../Page/第十一届亚洲运动会.md "wikilink")，在北京中轴路北端的[安慧桥附近建设了一批高层住宅楼及附属设施](../Page/安慧桥.md "wikilink")，即“亚运村”，作为运动员的住宿场所。当时，[国家奥林匹克体育中心和亚运村同期建造](../Page/国家奥林匹克体育中心.md "wikilink")，二者的占地总面积97.5公顷，其中包括供比赛使用的游泳馆、体育馆、田径馆、曲棍球场等，以及供记者、运动员使用的亚运村、会议中心、旅馆、办公楼、公寓、康乐宫等，共计30个子项、50栋建筑，总建筑面积达到64.7万平方米，由[北京市建筑设计研究院设计](../Page/北京市建筑设计研究院.md "wikilink")。\[1\]

亚运会结束后，这一地区逐渐发展成为北京的大型高档居住社区，拥有[国家奥林匹克体育中心](../Page/国家奥林匹克体育中心.md "wikilink")、[五洲大酒店](../Page/五洲大酒店.md "wikilink")、[北辰购物中心](../Page/北辰购物中心.md "wikilink")、[名人广场](../Page/名人广场.md "wikilink")、[远大中心](../Page/远大中心.md "wikilink")、[炎黄艺术馆等公共建筑](../Page/炎黄艺术馆.md "wikilink")。[北京四环路穿过亚运村的南部](../Page/北京四环路.md "wikilink")。

[2008年北京奥运会的主会场](../Page/2008年北京奥运会.md "wikilink")[中国国家体育场](../Page/中国国家体育场.md "wikilink")、[国家游泳中心](../Page/国家游泳中心.md "wikilink")、[北京奥林匹克公园均建在亚运村街道辖区的西侧](../Page/北京奥林匹克公园.md "wikilink")。

## 下辖社区

亚运村街道目前下辖的[社区有](../Page/社区.md "wikilink")：

  - [安慧里社区](../Page/安慧里社区.md "wikilink")
  - [安慧里南社区](../Page/安慧里南社区.md "wikilink")
  - [华严北里社区](../Page/华严北里社区.md "wikilink")
  - [华严北里西社区](../Page/华严北里西社区.md "wikilink")
  - [安翔里社区](../Page/安翔里社区.md "wikilink")
  - [丝竹园社区](../Page/丝竹园社区.md "wikilink")
  - [北辰东路社区](../Page/北辰东路社区.md "wikilink")
  - [安苑里社区](../Page/安苑里社区.md "wikilink")
  - [京民社区](../Page/京民社区.md "wikilink")
  - [祁家豁子社区](../Page/祁家豁子社区.md "wikilink")

## 参考文献

{{-}}

[Category:北京市朝阳区街道](../Category/北京市朝阳区街道.md "wikilink")
[Category:1990年亞洲運動會](../Category/1990年亞洲運動會.md "wikilink")

1.  [国家奥林匹克体育中心与亚运村，首都之窗，于2013-04-20查阅](http://www.beijing.gov.cn/rwbj/sdjz/jsndjz/t359486.htm)