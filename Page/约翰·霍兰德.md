**约翰·亨利·霍兰德**（，），[美国科学家](../Page/美国.md "wikilink")，[复杂理论和](../Page/复杂理论.md "wikilink")[非线性科学的先驱](../Page/非线性科学.md "wikilink")，[遗传算法之父](../Page/遗传算法.md "wikilink")。主要研究领域为复杂[自适应系统](../Page/自适应系统.md "wikilink")、[认知过程的计算机模型等](../Page/认知过程.md "wikilink")。

1950年获得[麻省理工学院学士学位](../Page/麻省理工学院.md "wikilink")。后获得[密歇根大学博士](../Page/密歇根大学.md "wikilink")，并长期任教于该校。现为[心理学和](../Page/心理学.md "wikilink")[电气工程与](../Page/电气工程.md "wikilink")[计算机科学教授](../Page/计算机科学.md "wikilink")。他还是[圣达菲研究所的理事和科学委员会成员](../Page/圣达菲研究所.md "wikilink")。

## 著作

  - *Hidden Order: How Adaptation Builds Complexity* (1995)
  - *Emergence: From Chaos to Order* (1998)
  - *Adaptation in Natural and Artificial Systems* (1975,1992)（遗传算法开山之作）

## 外部链接

  - [个人主页](https://web.archive.org/web/20080131142510/http://www.lsa.umich.edu/psych/people/directory/profiles/faculty/?uniquename=jholland)
  - [生平](https://web.archive.org/web/20070311033435/http://www.cs.oswego.edu/~blue/hx/courses/cogsci1/s2001/section05/subsection5/main.html)
  - [在圣达菲研究所的Echo项目](https://web.archive.org/web/20060213043209/http://www.santafe.edu/projects/echo/)

[Category:美国科学家](../Category/美国科学家.md "wikilink")
[Category:系统科学家](../Category/系统科学家.md "wikilink")
[Category:认知科学家](../Category/认知科学家.md "wikilink")
[Category:密西根大學校友](../Category/密西根大學校友.md "wikilink")
[Category:麻省理工學院校友](../Category/麻省理工學院校友.md "wikilink")
[Category:人工智能研究者](../Category/人工智能研究者.md "wikilink")
[Category:美国心理学家](../Category/美国心理学家.md "wikilink")
[Category:密歇根大學教師](../Category/密歇根大學教師.md "wikilink")