**李文田**（），字**畬光**，號**若農**、**芍農**，諡**文誠**，[廣東](../Page/廣東.md "wikilink")[順德人](../Page/順德.md "wikilink"),均安鎮，南浦村。清代翰林、書法家、蒙古史研究專家。曾為《[蒙古秘史](../Page/蒙古秘史.md "wikilink")》作註。

## 生平

[咸豐九年](../Page/咸豐.md "wikilink")（1859年）進士，[殿試獲一甲第三名](../Page/殿試.md "wikilink")[探花](../Page/探花.md "wikilink")，屢次上書極言朝政得失。歷任翰林院編修、侍讀，提督江西[學政](../Page/學政.md "wikilink")，官至禮部侍郎，入值南書房。

好學不倦，於天文、地理，無不通曉。公務之餘，勤於治學，對元史及本北水地研究尤精。

李文田是亦[嶺南一代書法名家](../Page/嶺南.md "wikilink")，對碑帖源流有深入的砧研。長於篆書、隸書，俸祿大多用於購買古籍、碑帖。年少時專工[歐陽詢](../Page/歐陽詢.md "wikilink")，精熟於《九成宮》等碑帖，旁及其他唐碑，後來轉學隋碑《蘇孝慈墓誌》，中年以後，博採漢、魏碑刻。嘗大論《蘭亭序》非[王羲之書文](../Page/王羲之.md "wikilink")，爭議一時。

## 著作

著有《元秘史注》、《元史地名考》、《耶律楚材西游錄注》、《和林金石考》、《和林诗》等。

李文田工書法，其傳世書法作品中

  - [廣東省博物館藏有](../Page/廣東省博物館.md "wikilink")：《楷書軸》、《楷書八言聯》、《行書七言聯》、《隸書四屏》、《書畫團扇》
  - [廣州美術館藏有](../Page/廣州美術館.md "wikilink")：《仿李營丘筆法團扇》、《與蘇六朋合作風景人物扇面》、《楷書軸》、《楷書八言聯》、《隸書六屏》。
  - [廣州博物館藏有](../Page/廣州博物館.md "wikilink")：《篆書八言聯》。
  - [佛山市博物館藏有](../Page/佛山市博物館.md "wikilink")：《節錄張猛龍碑楷書軸》等6種書法。

其他如：[東莞市](../Page/東莞市.md "wikilink")、[江門市博物館和](../Page/江門市博物館.md "wikilink")[香港中文大學文物館均藏有其作品](../Page/香港中文大學文物館.md "wikilink")。

## 軼事

傳說李文田上京赴試，在[北京的](../Page/北京.md "wikilink")[關帝廟內拜神求籤](../Page/關帝廟.md "wikilink")。簽語雲「名在孫山外」，遂以為是個不好的兆頭。同行的另一粵人忙問其故，李答：「我求了一支『名在孫山外』的簽，想必今科落第無疑了。」那人哈哈大笑，曰：「說不定姓孫的中在你前面，你又何必發愁呢？」[殿試的結果是](../Page/殿試.md "wikilink")：[狀元為](../Page/狀元.md "wikilink")[孫家鼐](../Page/孫家鼐.md "wikilink")，[榜眼為](../Page/榜眼.md "wikilink")[孫念祖](../Page/孫念祖.md "wikilink")，李文田中的是[探花](../Page/探花.md "wikilink")，恰巧是「名在孫山外」了。

## 紀念

  - [泰华楼](../Page/泰华楼.md "wikilink")

## 外部連結

  - [李文田](http://npmhost.npm.gov.tw/ttscgi2/ttsquery?0:0:npmauac:TM%3D%A7%F5%A4%E5%A5%D0)
    [中研院史語所](../Page/中研院.md "wikilink")
  - [李探花墓湮没半世纪后重见天日](http://dadao.net/php/prtime/temp_news.php?ArticleID=30620)
    [珠江時報](../Page/珠江時報.md "wikilink")

[Category:清朝翰林](../Category/清朝翰林.md "wikilink")
[Category:清朝江西學政](../Category/清朝江西學政.md "wikilink")
[Category:清朝詹事府少詹事](../Category/清朝詹事府少詹事.md "wikilink")
[Category:清朝禮部侍郎](../Category/清朝禮部侍郎.md "wikilink")
[Category:清朝順天學政](../Category/清朝順天學政.md "wikilink")
[Category:清朝歷史學家](../Category/清朝歷史學家.md "wikilink")
[Category:清朝書法家](../Category/清朝書法家.md "wikilink")
[Category:順德人](../Category/順德人.md "wikilink")
[W文](../Category/李姓.md "wikilink")
[Category:諡文誠](../Category/諡文誠.md "wikilink")
[Category:南書房行走](../Category/南書房行走.md "wikilink")