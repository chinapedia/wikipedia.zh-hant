**常熟路站**位于[上海](../Page/上海.md "wikilink")[徐汇区](../Page/徐汇区.md "wikilink")[淮海中路](../Page/淮海路_\(上海\).md "wikilink")[常熟路](../Page/常熟路_\(上海\).md "wikilink")，为[上海轨道交通1号线和](../Page/上海轨道交通1号线.md "wikilink")[7号线](../Page/上海轨道交通7号线.md "wikilink")[换乘站](../Page/换乘站.md "wikilink")，其中1号线车站位于淮海中路，由北京市城建设计院设计\[1\]，7号线车站位于常熟路，均为地下[島式车站](../Page/島式站臺.md "wikilink")，两线通过通道换乘。

## 周边

  - [上海音乐学院](../Page/上海音乐学院.md "wikilink")

## 車站設計

### 1号线

1号线车站为地下[岛式站台](../Page/岛式站台.md "wikilink")。

  - 1号线月台配置

<table>
<thead>
<tr class="header">
<th><p>上行</p></th>
<th></th>
<th><p><a href="../Page/徐家汇站.md" title="wikilink">徐家汇</a>·<a href="../Page/上海体育馆站.md" title="wikilink">上海体育馆</a>·<a href="../Page/上海南站站.md" title="wikilink">上海南站</a>·<a href="../Page/莘庄站.md" title="wikilink">莘庄方向</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>下行</p></td>
<td></td>
<td><p><a href="../Page/陕西南路站.md" title="wikilink">陕西南路</a>·<a href="../Page/人民广场站_(上海市).md" title="wikilink">人民广场</a>·<a href="../Page/上海火车站站.md" title="wikilink">上海火车站</a>·<a href="../Page/富锦路站.md" title="wikilink">富锦路方向</a></p></td>
</tr>
</tbody>
</table>

### 7号线

7号线车站为地下[岛式站台](../Page/岛式站台.md "wikilink")。

  - 7号线月台配置

<table>
<thead>
<tr class="header">
<th><p>上行</p></th>
<th></th>
<th><p><a href="../Page/静安寺站.md" title="wikilink">静安寺</a>·<a href="../Page/镇坪路站.md" title="wikilink">镇坪路</a>·<a href="../Page/上海大学站.md" title="wikilink">上海大学</a>·<a href="../Page/美蘭湖站.md" title="wikilink">美蘭湖方向</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>下行</p></td>
<td></td>
<td><p><a href="../Page/东安路站.md" title="wikilink">东安路</a>·<a href="../Page/耀华路站.md" title="wikilink">耀华路</a>·<a href="../Page/龙阳路站.md" title="wikilink">龙阳路</a>·<a href="../Page/花木路站.md" title="wikilink">花木路方向</a></p></td>
</tr>
</tbody>
</table>

## 公交换乘

15、26、40、42、45、49、93、96、167、315、320、327、816、824、830、911、911区间、920、926、927

## 车站出口

  - 1号口：淮海中路北侧，常熟路东
  - 2号口：淮海中路南侧，汾阳路西
  - 3号口：淮海中路北侧，华亭路东
  - 4号口：淮海中路南侧，宝庆路东
  - 6号口：常熟路东侧，淮海中路北侧
  - 7号口：淮海中路北侧，常熟路西
  - 8号口：五原路北侧，常熟路西

注：1号口设有轮椅升降台、6号口设有洗手间

## 邻近车站

## 参考资料

  - [常熟路站建设工程规划许可证](https://web.archive.org/web/20070927192941/http://www.shghj.gov.cn/Ct_1.aspx?ct_id=00070605F08552)

[Category:徐汇区地铁车站](../Category/徐汇区地铁车站.md "wikilink")
[Category:1995年启用的铁路车站](../Category/1995年启用的铁路车站.md "wikilink")
[Category:以街道命名的中國鐵路車站](../Category/以街道命名的中國鐵路車站.md "wikilink")

1.  [《上海市政工程志》\>\>第六篇 地下铁道\>\>第二章 一号线前期工作\>\>第三节
    设计](http://shtong.gov.cn/newsite/node2/node2245/node68289/node68299/node68387/node68393/userobject1ai65937.html)