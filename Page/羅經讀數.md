[Brosen_windrose_Full.svg](https://zh.wikipedia.org/wiki/File:Brosen_windrose_Full.svg "fig:Brosen_windrose_Full.svg")
**羅經讀數**，又種**羅經點**，是指[羅盤上的](../Page/羅盤.md "wikilink")32個基本[方位](../Page/方位.md "wikilink")，一般自0度（正北方）順序排列出來。

| \# | 方位                 | 簡稱.  | 方位角     |
| -- | ------------------ | ---- | ------- |
| 1  | **北**              | N    | 0.00°   |
| 2  | North by east      | NbE  | 11.25°  |
| 3  | 東北北                | NNE  | 22.50°  |
| 4  | Northeast by north | NEbN | 33.75°  |
| 5  | **東北**             | NE   | 45.00°  |
| 6  | Northeast by east  | NEbE | 56.25°  |
| 7  | 東北東                | ENE  | 67.50°  |
| 8  | East by north      | EbN  | 78.75°  |
| 9  | **東**              | E    | 90.00°  |
| 10 | East by south      | EbS  | 101.25° |
| 11 | 東南東                | ESE  | 112.50° |
| 12 | Southeast by east  | SEbE | 123.75° |
| 13 | **東南**             | SE   | 135.00° |
| 14 | Southeast by south | SEbS | 146.25° |
| 15 | 東南南                | SSE  | 157.50° |
| 16 | South by east      | SbE  | 168.75° |
| 17 | **南**              | S    | 180.00° |
| 18 | South by west      | SbW  | 191.25° |
| 19 | 西南南                | SSW  | 202.50° |
| 20 | Southwest by south | SWbS | 213.75° |
| 21 | **西南**             | SW   | 225.00° |
| 22 | Southwest by west  | SWbW | 236.25° |
| 23 | 西南西                | WSW  | 247.50° |
| 24 | West by south      | WbS  | 258.75° |
| 25 | **西**              | W    | 270.00° |
| 26 | West by north      | WbN  | 281.25° |
| 27 | 西北西                | WNW  | 292.50° |
| 28 | Northwest by west  | NWbW | 303.75° |
| 29 | **西北**             | NW   | 315.00° |
| 30 | Northwest by north | NWbN | 326.25° |
| 31 | 西北北                | NNW  | 337.50° |
| 32 | North by west      | NbW  | 348.75° |

[bs:Sjeverozapad](../Page/bs:Sjeverozapad.md "wikilink")
[br:Gwalarn](../Page/br:Gwalarn.md "wikilink")
[bg:Северозапад](../Page/bg:Северозапад.md "wikilink")
[da:Kompasretning](../Page/da:Kompasretning.md "wikilink") [en:Boxing
the compass](../Page/en:Boxing_the_compass.md "wikilink")
[et:Loe](../Page/et:Loe.md "wikilink")
[ko:북서쪽](../Page/ko:북서쪽.md "wikilink") [id:Boxing the
compass](../Page/id:Boxing_the_compass.md "wikilink") [ku:Bakur
rojava](../Page/ku:Bakur_rojava.md "wikilink")
[no:Nordvest](../Page/no:Nordvest.md "wikilink") [pl:Północny
zachód](../Page/pl:Północny_zachód.md "wikilink")
[ru:Румб](../Page/ru:Румб.md "wikilink")
[sr:Северозапад](../Page/sr:Северозапад.md "wikilink")
[sh:Severozapad](../Page/sh:Severozapad.md "wikilink") [fi:Luode
(ilmansuunta)](../Page/fi:Luode_\(ilmansuunta\).md "wikilink") [vi:Hướng
Tây Bắc](../Page/vi:Hướng_Tây_Bắc.md "wikilink")

[Category:方向](../Category/方向.md "wikilink")