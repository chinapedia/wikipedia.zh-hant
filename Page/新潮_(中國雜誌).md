《**新潮**》，英文名*The
Renaissance*，中國民國時期雜誌，創刊於1919年1月1日，由[北京大學學生組織](../Page/北京大學.md "wikilink")[新潮社出版發行](../Page/新潮社_\(中國\).md "wikilink")。到1921年停刊，共出版12期\[1\]。《新潮》與北京大学教授[陈独秀編輯的](../Page/陈独秀.md "wikilink")《[新青年](../Page/新青年.md "wikilink")》杂志，同属于[新文化运动的一部分](../Page/新文化运动.md "wikilink")。《新潮》獲北大教授[胡適贊助](../Page/胡適.md "wikilink")，但被另一些北大學者創辦的《國故》批評。\[2\]“**新潮**”二字，則由北大校长[蔡元培题写](../Page/蔡元培.md "wikilink")。\[3\]\[4\]

## 編輯

《新潮》的始創成員有[傅斯年](../Page/傅斯年.md "wikilink")、[羅家倫](../Page/羅家倫.md "wikilink")、[顧頡剛](../Page/顧頡剛.md "wikilink")、[俞平伯](../Page/俞平伯.md "wikilink")、[葉紹鈞](../Page/葉紹鈞.md "wikilink")、[毛子水](../Page/毛子水.md "wikilink")。第一期主編是[傅斯年和](../Page/傅斯年.md "wikilink")[羅家倫](../Page/羅家倫.md "wikilink")，傅斯年撰寫〈發刊旨趣書〉，羅家倫發表署名社論加以補充。1920年後期，[周作人出任總編](../Page/周作人.md "wikilink")。\[5\]

## 宗旨

新潮社希望出版一份代表學生的新刊物，與《[新青年](../Page/新青年.md "wikilink")》雜誌爭競。政治態度上，《新潮》強調[個人主義](../Page/個人主義.md "wikilink")。學術上，《新潮》主張，知識份子有責任發現當代中國社會的需要，加以推動，並要了解歷史的潮流，透過國學研究，以探求現代中國及現代思潮應取的方向。\[6\]

## 內容

《新潮》偏重于[思想](../Page/思想.md "wikilink")、[文学方面](../Page/文学.md "wikilink")，介绍一些外国文学\[7\]。《新潮》以[白话文推进文学革命](../Page/白话文.md "wikilink")，探讨道德与人生观，提倡妇女解放，批判大家庭制度，促进教育，改善教育质素等\[8\]。

## 参考文献

<div class="references-small">

<references />

</div>

  - 羅志田：〈[中國文藝復興之夢：從清季的古學復興到民國的新潮](http://ccs.ncl.edu.tw/Chinese_studies_20_1/277-307.pdf)〉。

[Category:北京市杂志](../Category/北京市杂志.md "wikilink")
[Category:中华民国大陆时期杂志](../Category/中华民国大陆时期杂志.md "wikilink")
[Category:中华民国大陆时期已停刊杂志](../Category/中华民国大陆时期已停刊杂志.md "wikilink")
[Category:北京文学史](../Category/北京文学史.md "wikilink")
[Category:1919年創辦的雜誌](../Category/1919年創辦的雜誌.md "wikilink")
[Category:1921年停刊的雜誌](../Category/1921年停刊的雜誌.md "wikilink")
[Category:1919年中國建立](../Category/1919年中國建立.md "wikilink")
[Category:1921年中國廢除](../Category/1921年中國廢除.md "wikilink")

1.  李杰泉，《新潮》杂志综述。《近代史研究》1988年1月总第43期，155页。
2.  Laurence A. Schneider著，梅寅生譯：《顧頡剛與中國新史學》（台北：華世出版社，1984），頁31、44-45。
3.  [俞平伯](../Page/俞平伯.md "wikilink")，回忆《新潮》。《学术中国》 2005年5月。
4.  李杰泉，《新潮》杂志综述。《近代史研究》1988年1月总第43期，155页。
5.  Schneider：《顧頡剛與中國新史學》，頁31。
6.  Schneider：《顧頡剛與中國新史學》，頁31-33。
7.  [俞平伯](../Page/俞平伯.md "wikilink")，回忆《新潮》。《学术中国》 2005年5月。
8.  李杰泉，《新潮》杂志综述。《近代史研究》1988年1月总第43期，155页。