**卡尔帕娜·乔拉**（，），生於印度[卡尔纳尔](../Page/卡尔纳尔.md "wikilink")（Karnal），是一名[印裔美籍](../Page/印度裔美國人.md "wikilink")[太空人](../Page/太空人.md "wikilink")。她是第一位印度裔美國籍太空人，也是首位進入太空的印度女性。1997年，參與[哥倫比亞號太空梭的太空任務](../Page/哥倫比亞號太空梭.md "wikilink")，首次進入太空。2003年2月1日，[哥倫比亞號太空梭爆炸解體時](../Page/哥倫比亞號太空梭.md "wikilink")，她是七位罹難太空人之一。[小行星51826號以其名字命名作為紀念](../Page/小行星51826.md "wikilink")。\[1\]

## 参考文献

[Category:國會太空榮譽勳章獲得者](../Category/國會太空榮譽勳章獲得者.md "wikilink")
[Category:美国宇航员](../Category/美国宇航员.md "wikilink")
[Category:印度太空人](../Category/印度太空人.md "wikilink")
[Category:女太空人](../Category/女太空人.md "wikilink")
[Category:女性工程師](../Category/女性工程師.md "wikilink")
[Category:業餘無線電人物](../Category/業餘無線電人物.md "wikilink")
[Category:德州大學阿靈頓分校校友](../Category/德州大學阿靈頓分校校友.md "wikilink")
[Category:科羅拉多大學校友](../Category/科羅拉多大學校友.md "wikilink")
[Category:印度裔美國人](../Category/印度裔美國人.md "wikilink")
[Category:移民美國的印度人](../Category/移民美國的印度人.md "wikilink")
[Category:旁遮普人](../Category/旁遮普人.md "wikilink")
[Category:哈里亞納邦人](../Category/哈里亞納邦人.md "wikilink")
[Category:美國空難身亡者](../Category/美國空難身亡者.md "wikilink")

1.