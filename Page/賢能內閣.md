[epmis05Wyndham.jpg](https://zh.wikipedia.org/wiki/File:epmis05Wyndham.jpg "fig:epmis05Wyndham.jpg")，1806年至1807年出任[英國首相](../Page/英國首相.md "wikilink")。\]\]

**賢能內閣**（**Ministry of All the
Talents**）是指[威廉·溫德姆·格倫維爾，第一代格倫維爾男爵在](../Page/威廉·溫德姆·格倫維爾，第一代格倫維爾男爵.md "wikilink")[小皮特身故後](../Page/小皮特.md "wikilink")，於1806年2月11日獲委任為[英國首相後所組成的內閣](../Page/英國首相.md "wikilink")。由於當時正值[拿破崙戰爭](../Page/拿破崙戰爭.md "wikilink")，格倫維爾勳爵為組織一個強而有力的政府，而邀請了幾乎所有黨派的領導人物入閣，當中只有以[喬治·坎寧為首的小皮特支持者拒絕邀請](../Page/喬治·坎寧.md "wikilink")。在眾多閣臣之中，以[查爾斯·詹姆士·福克斯獲邀入閣最受外界關注](../Page/查爾斯·詹姆士·福克斯.md "wikilink")，這是因為福克斯曾經與英-{zh:皇;zh-hans:王;zh-hk:皇;zh-tw:王;}-[喬治三世不少過節之故](../Page/喬治三世_\(英國\).md "wikilink")。不過，由於當時國難當前，喬治三世也不計前嫌，除不反對福克斯入閣外，還鼓勵不少人士加入或支持政府。

然而，賢能內閣並非一個成功的政府。這個內閣除了未能成功和[法國立下和約外](../Page/法國.md "wikilink")，到翌年3月更因為在[天主教解放一事出現分歧而以垮台告終](../Page/天主教解放.md "wikilink")。但值得慶幸的是，賢能內閣在垮台前成功在1807年廢除[奴隸貿易](../Page/奴隸貿易.md "wikilink")。內閣垮台以後，由[威廉·亨利·卡文迪許-本廷克，第三代波特蘭公爵組成了新政府](../Page/威廉·亨利·卡文迪許-本廷克，第三代波特蘭公爵.md "wikilink")。

## 賢能內閣 （1806年2月—1807年3月）

  - 格倫維爾勳爵 —
    [第一財政大臣和](../Page/第一財政大臣.md "wikilink")[上議院領袖](../Page/上議院領袖.md "wikilink")
  - [查爾斯·詹姆士·福克斯](../Page/查爾斯·詹姆士·福克斯.md "wikilink") —
    [外務大臣和](../Page/外交及聯邦事務大臣.md "wikilink")[下議院領袖](../Page/下議院領袖.md "wikilink")
  - [厄爾斯金勳爵](../Page/托馬斯·厄爾斯金，第一代厄爾斯金男爵.md "wikilink") —
    [大法官](../Page/大法官_\(英國\).md "wikilink")
  - [菲茨威廉勳爵](../Page/威廉·溫特渥-菲茨威廉，第四代菲茨威廉伯爵.md "wikilink") —
    [樞密院議長](../Page/樞密院議長.md "wikilink")
  - [西德默斯勳爵](../Page/亨利·阿丁頓，第一代西德默斯子爵.md "wikilink") —
    [掌璽大臣](../Page/掌璽大臣.md "wikilink")
  - [史賓塞勳爵](../Page/喬治·史賓塞，第二代史賓塞伯爵.md "wikilink") —
    [內務大臣](../Page/內務大臣.md "wikilink")
  - [威廉·溫德姆](../Page/威廉·溫德姆.md "wikilink") —
    [陸軍及殖民地大臣](../Page/陸軍及殖民地大臣.md "wikilink")
  - [霍威克勳爵](../Page/查理斯·格雷，第二代格雷伯爵.md "wikilink") —
    [第一海軍大臣](../Page/第一海軍大臣.md "wikilink")
  - [亨利·佩蒂勳爵](../Page/亨利·佩蒂-費茨莫里斯，第三代蘭斯多恩侯爵.md "wikilink") —
    [財政大臣](../Page/財政大臣.md "wikilink")
  - [莫伊拉勳爵](../Page/弗朗西斯·羅頓-哈斯丁斯，第一代哈斯丁斯侯爵.md "wikilink") —
    [軍械總局局長](../Page/軍械總局局長.md "wikilink")
  - [埃倫巴勒勳爵](../Page/愛德華·勞，第一代埃倫巴勒男爵.md "wikilink") —
    [皇座法庭首席法官](../Page/高等法院首席法官.md "wikilink")

### 變動

  - 1806年9月：查爾斯·詹姆士·福克斯於任內去世，霍威克勳爵遂接任外務大臣兼下議院領袖之職。而[托馬斯·格倫維爾則接替霍威克勳爵為](../Page/托馬斯·格倫維爾.md "wikilink")[第一海軍大臣](../Page/第一海軍大臣.md "wikilink")。此外，菲茨威廉勳爵調為[不管部大臣](../Page/不管部大臣.md "wikilink")，樞密院議長一職由西德默斯勳爵接替，而[霍蘭勳爵則接替西德默斯勳爵為掌璽大臣](../Page/亨利·華素爾-福克斯，第三代霍蘭男爵.md "wikilink")。

[T](../Category/英國內閣.md "wikilink")