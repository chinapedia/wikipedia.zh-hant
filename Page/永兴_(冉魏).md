**永兴**（350年閏二月—352年四月）是[十六国时期](../Page/十六国.md "wikilink")[冉魏政权君主](../Page/冉魏.md "wikilink")[冉閔的](../Page/冉閔.md "wikilink")[年号](../Page/年号.md "wikilink")，共计3年。

## 纪年

| 永兴                               | 元年                             | 二年                             | 三年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 350年                           | 351年                           | 352年                           |
| [干支](../Page/干支纪年.md "wikilink") | [庚戌](../Page/庚戌.md "wikilink") | [辛亥](../Page/辛亥.md "wikilink") | [壬子](../Page/壬子.md "wikilink") |

## 参看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 其他时期使用的[永兴年号](../Page/永兴.md "wikilink")
  - 同期存在的其他政权年号
      - [永和](../Page/永和_\(晋穆帝\).md "wikilink")（345年-356年）：东晋[穆帝司馬聃的年号](../Page/晉穆帝.md "wikilink")
      - [建兴](../Page/建兴_\(晋愍帝\).md "wikilink")：[前凉政权年号](../Page/前凉.md "wikilink")
      - [建国](../Page/建国.md "wikilink")（338年十一月-376年）：[代政权](../Page/代国.md "wikilink")[拓跋什翼犍的年号](../Page/拓跋什翼犍.md "wikilink")
      - [永宁](../Page/永宁_\(石祗\).md "wikilink")（350年十一月-351年）：[後趙政权](../Page/後趙.md "wikilink")[石祗的年号](../Page/石祗.md "wikilink")
      - [皇始](../Page/皇始_\(苻健\).md "wikilink")（351年正月-355年五月）：[前秦政权](../Page/前秦.md "wikilink")[苻健的年号](../Page/苻健.md "wikilink")
      - [元璽](../Page/元璽.md "wikilink")（352年十一月-357年正月）：[前燕政权](../Page/前燕.md "wikilink")[慕容儁的年号](../Page/慕容儁.md "wikilink")
      - [建昌](../Page/建昌_\(張琚\).md "wikilink")（352年正月-五月）：后赵时期领袖[張琚的年号](../Page/張琚.md "wikilink")

## 参考文献

1.  李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129

[Category:十六国年号](../Category/十六国年号.md "wikilink")
[Category:4世纪](../Category/4世纪.md "wikilink")