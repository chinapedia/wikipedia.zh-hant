[Snodgrass_Dissostera_carolina-e.jpg](https://zh.wikipedia.org/wiki/File:Snodgrass_Dissostera_carolina-e.jpg "fig:Snodgrass_Dissostera_carolina-e.jpg")

**草-{}-蜢**，又名**蚱-{}-蜢**，在中国北方也称**蛨（蚂）蚱**，古稱**虴蛨**。是[蝗蟲和](../Page/蝗蟲.md "wikilink")[螽斯共有的俗稱](../Page/螽斯.md "wikilink")，但一般多指[蝗蟲而言](../Page/蝗蟲.md "wikilink")。在生物學分類上，「蚱蜢」一詞可分指「菱蝗科」和「短角蝗科」的蝗蟲；「菱蝗科」又稱之為「蚱科」，而「短角蝗科」則又稱之為「蜢科」。[蝗蟲屬](../Page/蝗蟲.md "wikilink")[直翅目锥尾亚目草食性](../Page/直翅目.md "wikilink")[昆蟲](../Page/昆蟲.md "wikilink")。

## 外形

蝗蟲腿部發達，用後腿可以跳比身體長數十倍的距離，後翅呈半透明，體色有[綠色和](../Page/綠色.md "wikilink")[褐色](../Page/褐色.md "wikilink")（與品種無關；是[生活](../Page/生活.md "wikilink")[環境的](../Page/環境.md "wikilink")[保護色](../Page/保護色.md "wikilink")）。

蝗蟲屬[不完全變態](../Page/不完全變態.md "wikilink")[昆蟲](../Page/昆蟲.md "wikilink")，稚蟲和[成蟲相似](../Page/成蟲.md "wikilink")，只有[翅膀有無的分別](../Page/翅膀.md "wikilink")。

## 食性

[蝗蟲](../Page/蝗蟲.md "wikilink")[口大](../Page/口.md "wikilink")、[下颚發達](../Page/下颚.md "wikilink")，主要以[植物](../Page/植物.md "wikilink")[葉片](../Page/葉片.md "wikilink")
為食，常在田間、草叢間出沒。相較之下，型體與[蝗蟲相似的](../Page/蝗蟲.md "wikilink")[螽斯則是](../Page/螽斯.md "wikilink")[雜食性的品種](../Page/雜食性.md "wikilink")。\~

## 求偶

一些種類的雄蟲會以磨擦翅膀和後腿發出[聲音](../Page/聲音.md "wikilink")，吸引雌蟲，與[蟋蟀用前翅的](../Page/蟋蟀.md "wikilink")[發音](../Page/發音.md "wikilink")[器官發聲的方式不同](../Page/器官.md "wikilink")。

## 變異

有些種類的蝗蟲，在繁衍過剩，以致種群密度極高，食物短缺的情況下，便會改變體色、行為和生活型態，成群結隊遷移至新的地方尋找食物。某些種類的蝗蟲翅膀退化變小，也有一些種類甚至已喪失[飛行能力](../Page/飛行.md "wikilink")，如「菱蝗」。

## 相關條目

  - [蝗蟲](../Page/蝗蟲.md "wikilink")
  - [昆蟲分類表](../Page/昆蟲分類表.md "wikilink")

## 參考文獻

  - *Firefly Encyclopedia of Insects and Spiders*, edited by Christopher
    O'Toole, ISBN 1-55297-612-2, 2002
  - [Grasshopper species to watch for on the Canadian Prairies and
    Northern Great
    Plains](http://people.uleth.ca/~dan.johnson/htm/dj_gh_guide.htm),
    Dan Johnson
  - [Grasshoppers: Life Cycle, Damage Assessment and Management
    Strategy](http://www1.agric.gov.ab.ca/$department/deptdocs.nsf/all/agdex3497),
    Govt. of Alberta

## 外部連結

  - [Tree of Life Web Project](http://tolweb.org/tree?group=Caelifera)
  - [The Grasshopper
    suicide](http://en.ird.fr/the-media-library/videos-on-line-canal-ird/a-manipulative-parasite/a-manipulative-parasite)
  - [American
    grasshopper](https://web.archive.org/web/20090224004943/http://entomology.ifas.ufl.edu/creatures/field/amhopper.htm)
    on the [UF](../Page/University_of_Florida.md "wikilink") /
    [IFAS](../Page/Institute_of_Food_and_Agricultural_Sciences.md "wikilink")
    Featured Creatures Web site
  - [A grasshopper plague is at hand in
    U.S. | Newsdesk.org](http://newsdesk.org/2010/03/a-grasshopper-plague-is-at-hand-in-u-s/)
    (March 30, 2010)
  - [AcridAfrica](http://acrida.info)

[錐尾亞目](../Category/錐尾亞目.md "wikilink")