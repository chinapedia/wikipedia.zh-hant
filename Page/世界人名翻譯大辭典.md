《**世界人名翻譯大辭典**》由[中國對外翻譯出版公司出版](../Page/中國對外翻譯出版公司.md "wikilink")，[新華社譯名室編輯](../Page/新華社譯名室.md "wikilink")，是[中華人民共和國第一本提供規範化](../Page/中華人民共和國.md "wikilink")[人名翻譯的](../Page/人名翻譯.md "wikilink")[工具書](../Page/工具書.md "wikilink")\[1\]。本书有姊妹篇《[世界地名翻译大辞典](../Page/世界地名翻译大辞典.md "wikilink")》。

## 特點

1.  收錄詞目65萬條。
2.  涉及100多個[國家和](../Page/國家.md "wikilink")[地區](../Page/地區.md "wikilink")。
3.  採用混合排列，擴大約定俗成譯名人物量。
4.  提供了[日本](../Page/日本.md "wikilink")、[朝鮮](../Page/朝鮮.md "wikilink")、[越南](../Page/越南.md "wikilink")、[新加坡等使用](../Page/新加坡.md "wikilink")[漢字或曾經使用過漢字的國家和地區以外的外國人名](../Page/漢字.md "wikilink")。

## 翻譯方法

外國人名按該[語言發音為主](../Page/語言.md "wikilink")，並參考名字拼寫譯出，用字依據該語言對應漢語的譯音表。一部份外文名有約定俗成的譯名，那麼這些譯名繼續採用。書後附有各語言的譯音表，和一些國家民族人名構成的介紹。

### 譯音表

譯音表一般按各語言的[拉丁拼寫](../Page/拉丁.md "wikilink")，列出該語言可能的輔音母音的組合與對應的漢字；但是[英語因為發音不規則](../Page/英語.md "wikilink")，所以按[國際音標列出](../Page/國際音標.md "wikilink")。下列是書中的譯音表所涵盖的语言：

  - [英語](../Page/英語.md "wikilink")
  - [愛爾蘭語](../Page/愛爾蘭語.md "wikilink")
  - [威爾士語](../Page/威爾士語.md "wikilink")
  - [德語](../Page/德語.md "wikilink")
  - [法語](../Page/法語.md "wikilink")
  - [西班牙語](../Page/西班牙語.md "wikilink")
  - [葡萄牙語](../Page/葡萄牙語.md "wikilink")
  - [意大利語](../Page/意大利語.md "wikilink")
  - [荷蘭語](../Page/荷蘭語.md "wikilink")
  - [馬爾他語](../Page/馬爾他語.md "wikilink")
  - [希臘語](../Page/希臘語.md "wikilink")
  - [芬蘭語](../Page/芬蘭語.md "wikilink")
  - [瑞典語](../Page/瑞典語.md "wikilink")
  - [挪威語](../Page/挪威語.md "wikilink")
  - [丹麥語](../Page/丹麥語.md "wikilink")
  - [冰島語](../Page/冰島語.md "wikilink")
  - [俄語](../Page/俄語.md "wikilink")
  - [立陶宛語](../Page/立陶宛語.md "wikilink")
  - [拉脫維亞語](../Page/拉脫維亞語.md "wikilink")
  - [愛沙尼亞語](../Page/愛沙尼亞語.md "wikilink")
  - [波蘭語](../Page/波蘭語.md "wikilink")
  - [捷克語](../Page/捷克語.md "wikilink")
  - [匈牙利語](../Page/匈牙利語.md "wikilink")
  - [羅馬尼亞語](../Page/羅馬尼亞語.md "wikilink")
  - [保加利亞語](../Page/保加利亞語.md "wikilink")
  - [阿爾巴尼亞語](../Page/阿爾巴尼亞語.md "wikilink")
  - [塞爾維亞語](../Page/塞爾維亞語.md "wikilink")
  - [蒙古語](../Page/蒙古語.md "wikilink")
  - [高棉語](../Page/高棉語.md "wikilink")
  - [老撾語](../Page/老撾語.md "wikilink")
  - [緬甸語](../Page/緬甸語.md "wikilink")
  - [泰語](../Page/泰語.md "wikilink")
  - [印度尼西亞語](../Page/印度尼西亞語.md "wikilink")
  - [他加祿語](../Page/他加祿語.md "wikilink")
  - [馬來語](../Page/馬來語.md "wikilink")
  - [印地語](../Page/印地語.md "wikilink")
  - [烏爾都語](../Page/烏爾都語.md "wikilink")
  - [僧伽羅語](../Page/僧伽羅語.md "wikilink")
  - [孟加拉語](../Page/孟加拉語.md "wikilink")
  - [普什圖語](../Page/普什圖語.md "wikilink")
  - [泰米爾語](../Page/泰米爾語.md "wikilink")
  - [土耳其語](../Page/土耳其語.md "wikilink")
  - [波斯語](../Page/波斯語.md "wikilink")
  - [阿拉伯語](../Page/阿拉伯語.md "wikilink")
  - [希伯來語](../Page/希伯來語.md "wikilink")
  - [阿姆哈拉語](../Page/阿姆哈拉語.md "wikilink")
  - [索馬里語](../Page/索馬里語.md "wikilink")
  - [馬達加斯加語](../Page/馬達加斯加語.md "wikilink")
  - [豪薩語](../Page/豪薩語.md "wikilink")
  - [斯瓦希里語](../Page/斯瓦希里語.md "wikilink")
  - [布爾語（南非荷蘭語）](../Page/南非語.md "wikilink")
  - [茨瓦納語](../Page/茨瓦納語.md "wikilink")
  - [沃洛夫語](../Page/沃洛夫語.md "wikilink")
  - [班圖語](../Page/班圖語.md "wikilink")
  - [斐濟語](../Page/斐濟語.md "wikilink")

## 参见

[Wikipedia:外語譯音表](../Page/Wikipedia:外語譯音表.md "wikilink")

## 参考文献

[Category:1993年書籍](../Category/1993年書籍.md "wikilink")
[Category:中華人民共和國書籍](../Category/中華人民共和國書籍.md "wikilink")

1.