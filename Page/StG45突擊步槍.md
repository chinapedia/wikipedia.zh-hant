**Sturmgewehr 45**（簡稱**StG
45**）是[納粹德國在](../Page/納粹德國.md "wikilink")[二戰末期的試驗型](../Page/二戰.md "wikilink")[突击步枪](../Page/突击步枪.md "wikilink")，由[毛瑟公司在](../Page/毛瑟.md "wikilink")1945年設計及生產。字面解作「45式突擊步槍」。該槍採用了創新的[滾輪延遲反沖式原理](../Page/反沖作用_\(槍械\)#滾輪延遲反沖.md "wikilink")，也是第一支使用該原理的槍械。

## 設計及歷史

[Gerat06H_Prototype.jpg](https://zh.wikipedia.org/wiki/File:Gerat06H_Prototype.jpg "fig:Gerat06H_Prototype.jpg")
[Munster_Sturmgewehr_45_(dark1).jpg](https://zh.wikipedia.org/wiki/File:Munster_Sturmgewehr_45_\(dark1\).jpg "fig:Munster_Sturmgewehr_45_(dark1).jpg")
[StG
44突击步枪虽然是一种革命性的单兵武器](../Page/StG44突击步枪.md "wikilink")，但其结构复杂，生产费时。为了在日益恶化的战局中提高武器产量，[毛瑟公司第](../Page/毛瑟.md "wikilink")37科的工程师开始研究StG
44更为廉价的替代品。他们第一次试验的产物为MKb Gerät
06（6号自动[卡宾枪](../Page/卡宾枪.md "wikilink")），在StG
44突击步枪的基础上简化了工艺，同样发射[7.92×33mm中间威力型子弹](../Page/7.92×33mm_Kurz.md "wikilink")，但改用[MG42通用机枪的滚轮闭锁机构](../Page/MG42通用机枪.md "wikilink")。相比StG
44，Gerät
06的成本仅降低10%，对于转换生产线而言仍显过于高昂，因此[德国陆军没有采用](../Page/德国陆军.md "wikilink")\[1\]。

在进行[Gew 43步枪的改进型Gerät](../Page/Gew_43步枪.md "wikilink")
03的射击实验中，毛瑟的工程师卡尔·迈耶发现，当枪机在闭锁过程中向前复进时，并不是一步到位，而是在撞击机槽后向后反弹一定距离再向前闭锁。这使他突发奇想：能否利用枪机反弹这短暂的开放时间完成退壳和上膛\[2\]。经过精密计算，他重新设计了Gerät
06滚轮的位置和闭锁榫的角度，延迟了枪机再次闭锁的时间，只利用反冲便完成了射击循环。由于枪机开放时子弹仍在枪管内，因此[枪膛刻有平行枪管的沟槽](../Page/枪膛.md "wikilink")，使部分火药气体向后帮助退壳，防止弹头分离。这款改进型被称为Gerät
06(H)，H指Halbverriegelt（半闭锁）。

由于Gerät
06(H)完全舍弃了复杂昂贵的[导气管结构](../Page/導氣管式.md "wikilink")，大大减少了生产工序和时间成本，从StG
44的70[帝国马克降至](../Page/帝国马克.md "wikilink")45帝国马克，甚至低于[毛瑟Kar98k步枪的成本](../Page/毛瑟Kar98k步枪.md "wikilink")。它計畫配用StG44的30发弹匣，由於戰後被擄獲的StG45零件皆尚未完成出廠許多的資料照片以及博物館展示的StG45裝備[VG1-5半自动步枪的](../Page/VG1-5半自动步枪.md "wikilink")10发弹匣以方便毛瑟工程師將其固定在工廠內的機台上測試\[3\]。1945年3月，Gerät
06(H)获准投产，命名为StG 45(M)，M指Mauser（毛瑟）。毛瑟公司生产了30把StG
45(M)突击步枪的零部件，随着[盟军的逼近](../Page/盟军.md "wikilink")，它们随着工厂机械撤退至[奥地利境内](../Page/奥地利.md "wikilink")。此时[德国宣布无条件投降](../Page/第二次世界大战欧洲战场的结束.md "wikilink")，這批零件隨後被[同盟國繳獲](../Page/同盟國.md "wikilink")，最终没有一把StG
45(M)投入实战。雖然沒有正式服役，但由[毛瑟設計的](../Page/毛瑟.md "wikilink")[滾輪延遲反沖式技術由其员工传播至欧洲各地](../Page/反沖作用_\(槍械\)#滾輪延遲反沖.md "wikilink")，被後來的[CETME自動步槍](../Page/CETME自動步槍.md "wikilink")、[SIG
SG 510自動步槍](../Page/SIG_SG_510自動步槍.md "wikilink")、[HK
G3自動步槍及](../Page/HK_G3自動步槍.md "wikilink")[MP5冲锋枪大量採用](../Page/MP5冲锋枪.md "wikilink")。

## 資料來源

<div class="references-small">

<references />

  - [German Weapons during
    WWII](http://www.feldgrau.com/articles.php?ID=60)
  - [gotavapen.se](http://gotavapen.se/gota/ak/mkb3.htm)
  - [mil.news.sina.com.cn
    StG45、CETME、G3](http://mil.news.sina.com.cn/pc/2003-08-04/29/90.html)

</div>

## 相關條目

  - [Sturmgewehr 44、MP44、StG44](../Page/StG44突击步枪.md "wikilink")
  - [Volkssturmgewehr 1-5、VG 1-5](../Page/VG1-5半自動步槍.md "wikilink")
  - [Wimmersperg Spz-kr](../Page/Wimmersperg_Spz-kr.md "wikilink")
  - [HK
    G3](../Page/HK_G3自動步槍.md "wikilink")、[CETME](../Page/CETME自動步槍.md "wikilink")

## 外部链接

  - —[militaryphotos.net-StG 45(M)圖片](http://www.sturmgewehr.com/bhinton/German-WWII_Rifles/StG45_A.jpg)

  - —[Modern Firearms
    StG44突击步枪](https://web.archive.org/web/20080910145835/http://world.guns.ru/assault/as93-e.htm)

[Category:自動步槍](../Category/自動步槍.md "wikilink")
[Category:突擊步槍](../Category/突擊步槍.md "wikilink")
[Category:反衝式槍械](../Category/反衝式槍械.md "wikilink")
[Category:7.92×33毫米槍械](../Category/7.92×33毫米槍械.md "wikilink")
[Category:德國槍械](../Category/德國槍械.md "wikilink")
[Category:德國二戰武器](../Category/德國二戰武器.md "wikilink")
[Category:試驗和研究槍械](../Category/試驗和研究槍械.md "wikilink")

1.  The EM-2 (Rifle No. 9, Mk 1): Britain's Original Bullpup Rifle.
    (n.d.). Retrieved February 12, 2017, from
    <http://www.historyofwar.org/articles/weapons_EM-2_rifle.html>
2.  Gerat 06H. (2014, April 14). Retrieved February 12, 2017, from
    <https://www.forgottenweapons.com/german-ww2-rifles/gerat-06h/>
3.