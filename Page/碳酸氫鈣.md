**碳酸氫鈣**（化學式：[Ca](../Page/鈣.md "wikilink")([H](../Page/氫.md "wikilink")[C](../Page/碳.md "wikilink")[O](../Page/氧.md "wikilink")<sub>3</sub>)<sub>2</sub>），是一種[無機](../Page/無機化合物.md "wikilink")[酸式鹽](../Page/酸式鹽.md "wikilink")。

產生碳酸氫鈣最重要的化學反應即是以[碳酸鈣溶於](../Page/碳酸鈣.md "wikilink")[二氧化碳水溶液而成碳酸氫鈣](../Page/二氧化碳.md "wikilink")，反應式如下：

  -
    Ca(OH)<sub>2</sub> + CO<sub>2</sub> → CaCO<sub>3</sub>↓ +
    H<sub>2</sub>O
    CaCO<sub>3</sub> + H<sub>2</sub>O + CO<sub>2</sub> →
    Ca(HCO<sub>3</sub>)<sub>2</sub>

碳酸氢钙微溶于水，但碳酸钙难溶。[钟乳石的形状即是由微溶的碳酸氢钙与难溶的碳酸钙在一定](../Page/钟乳石.md "wikilink")[二氧化碳浓度下的互相转变造成的](../Page/二氧化碳.md "wikilink")。

碳酸氢钙只存在溶液中；将碳酸氢钙溶液蒸发则得到碳酸钙固体。

## 化学性质

碳酸氢钙不稳定，稍受热即可分解：

  -
    Ca(HCO<sub>3</sub>)<sub>2</sub> → CaCO<sub>3</sub> + H<sub>2</sub>O
    + CO<sub>2</sub>↑

它也可以和酸反应，产生钙盐：

  -
    Ca(HCO<sub>3</sub>)<sub>2</sub> + 2 HCl → CaCl<sub>2</sub> + 2
    H<sub>2</sub>O + 2 CO<sub>2</sub>↑

## 參照

  - [碳酸氫鈉](../Page/碳酸氫鈉.md "wikilink")

## 參考文獻

  - [Baking
    Soda](http://www.newton.dep.anl.gov/askasci/chem99/chem99492.htm)

<references/>

[Category:碳酸氫鹽](../Category/碳酸氫鹽.md "wikilink")
[Category:鈣化合物](../Category/鈣化合物.md "wikilink")
[Category:缺少物质图片的化学品条目](../Category/缺少物质图片的化学品条目.md "wikilink")