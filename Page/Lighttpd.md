**lighttpd**（發音為lighty）是一套[开放源代码的](../Page/开放源代码.md "wikilink")[網頁伺服器](../Page/網頁伺服器.md "wikilink")，以[BSD許可證釋出](../Page/BSD許可證.md "wikilink")。相較於其他的網頁伺服器，lighttpd僅需少量的内存及[CPU資源即可達到同樣的效能](../Page/中央处理器.md "wikilink")。

## 简介

Lighttpd是一个德国人领导的开源Web服务器软件，其根本的目的是提供一个专门针对高性能网站，安全、快速、兼容性好并且灵活的网页服务器环境。具有非常低的内存开销、CPU占用率低、效能好以及丰富的模块等特点。Lighttpd是众多开源轻量级的网页服务器中较为优秀的一个。支持FastCGI、CGI、Auth、输出压缩（output
compress）、URL重写、别名等重要功能；而Apache之所以流行，很大程度也是因为功能丰富，在lighttpd上很多功能都有相应的实现了，这点对于Apache的用户是非常重要的，因为迁移到lighttpd就必须面对这些问题。

## 特色

  - 提供[FastCGI及](../Page/FastCGI.md "wikilink")[SCGI的負載平衡](../Page/SCGI.md "wikilink")
  - 支援[chroot](../Page/chroot.md "wikilink")
  - 支援`select()`/`poll()`及更有效率的`kqueue`/`epoll`連線狀態判斷
  - 支援條件重寫（Conditional rewrites）
  - 支援[SSL連線](../Page/SSL.md "wikilink")
  - 透過[LDAP](../Page/LDAP.md "wikilink") server認證
  - [rrdtool狀態輸出](../Page/rrdtool.md "wikilink")
  - 基于规则的下载
  - 服务器侧包含支持
  - 虚拟主机
  - 模块支持
  - 缓存元语言
  - 最小WebDAV支持
  - Servlet（AJP）支持（1.5.x版後）

## 操作系统支持

  - lighttpd至少为下列平台定期构建和发布
  - Linux（在Fedora
    Core、SuSE、OpenSUSE、Debian、Ubuntu、Gentoo、PLD-Linux、OpenWRT等發行版中可直接使用[套件包](../Page/軟體套件.md "wikilink")）
  - 原始碼安裝（[CentOS等](../Page/CentOS.md "wikilink")）
  - \*BSD（FreeBSD、NetBSD、OpenBSD、Mac OS X）
  - SGIIRIX
  - Windows（[Cygwin](../Page/Cygwin.md "wikilink")、[MinGW等](../Page/MinGW.md "wikilink")）
  - Solaris
  - AIX

## 外部連結

  - [lighttpd fly light](http://www.lighttpd.net/)，lighttpd的官方網站。
  - [lighttpd
    forum](https://web.archive.org/web/20051207024505/http://forum.lighttpd.net/)，lighttpd的論壇。
  - [lighty's
    life](http://blog.lighttpd.net/)，lighttpd的[網誌](../Page/網誌.md "wikilink")。
  - [1](http://www.oschina.net/p/lighttpd), Open Source China 的介绍

[Category:用C編程的自由軟體](../Category/用C編程的自由軟體.md "wikilink")
[Category:自由网络服务器软件](../Category/自由网络服务器软件.md "wikilink")
[Category:自由跨平台軟體](../Category/自由跨平台軟體.md "wikilink")
[Category:Unix网络相关软件](../Category/Unix网络相关软件.md "wikilink")
[Category:Linux网络服务器软件](../Category/Linux网络服务器软件.md "wikilink")
[Category:Windows互联网软件](../Category/Windows互联网软件.md "wikilink")