**台北聯絡線**，編號為**[中華民國國道三號甲線](../Page/中華民國國道.md "wikilink")**，是[國道三號的一條](../Page/福爾摩沙高速公路.md "wikilink")**國道支線**，連接[國道三號與](../Page/福爾摩沙高速公路.md "wikilink")[臺北市](../Page/臺北市.md "wikilink")[大安區](../Page/大安區_\(臺北市\).md "wikilink")、[文山區](../Page/文山區.md "wikilink")、[新北市](../Page/新北市.md "wikilink")[深坑區等地的一條](../Page/深坑區.md "wikilink")**[國道快速公路](../Page/中華民國快速道路#國道快速公路.md "wikilink")**，可行駛250[c.c.以上之大型重型機車](../Page/毫升.md "wikilink")，也是[國道計程收費的實施路段](../Page/國道計程收費.md "wikilink")。由於本公路是[臺北都會區進出福爾摩沙高速公路的要道之一](../Page/臺北都會區.md "wikilink")，且與國道三號相交處並未完全立體化，因此[尖峰時段時常有](../Page/尖峰時段.md "wikilink")[塞車的情形](../Page/塞車.md "wikilink")。

## 路線

國道三甲起點位於臺北市[辛亥路](../Page/辛亥路.md "wikilink")[辛亥隧道旁](../Page/辛亥隧道.md "wikilink")，經過二處隧道後，設有[萬芳交流道與](../Page/萬芳交流道.md "wikilink")[信義快速道路交會](../Page/信義快速道路.md "wikilink")，之後與國道三號主線於[木柵交流道會合](../Page/木柵交流道.md "wikilink")，最後連接至[新北市](../Page/新北市.md "wikilink")[深坑區](../Page/深坑區.md "wikilink")[文山路與](../Page/文山路.md "wikilink")[新光路](../Page/新光路_\(臺北市\).md "wikilink")。

本路線曾計畫由台北端繼續延伸，沿辛亥路二、三段兩旁之[分隔島高架](../Page/分隔島.md "wikilink")，最後接上[水源快速道路及](../Page/水源快速道路.md "wikilink")[建國快速道路](../Page/建國快速道路.md "wikilink")。目前以辛亥路平面道路與穿越基隆路之車行地下道與建國高架道路銜接。

## 歷史

  - 1996年
      - 3月21日，與國道三號[汐止系統至](../Page/汐止系統交流道.md "wikilink")[木柵路段一齊通車](../Page/木柵交流道.md "wikilink")。
  - 2005年
      - 5月14日，萬芳交流道隨著[信義快速道路的開通而全部通車](../Page/信義快速道路.md "wikilink")。
  - 2007年
      - 11月1日，開放550c.c.以上大型[重型機車通行](../Page/重型機車.md "wikilink")，同時也由高速公路降級至快速公路。
  - 2012年
      - 7月1日，開放250c.c.以上大型重型機車行駛。
  - 2013年
      - 12月30日，隨著國道全面[計程收費而開始收費](../Page/高速公路電子收費系統_\(台灣\).md "wikilink")。

## 交流道

## 車道與速限

<table>
<thead>
<tr class="header">
<th><p>區間</p></th>
<th><p><a href="../Page/車道.md" title="wikilink">車道</a><br />
東向+西向=東西向</p></th>
<th><p>速限</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>台北端-深坑端</p></td>
<td><p>2+2=4</p></td>
<td><p>80km/h</p></td>
<td><p>西向4.4k至3.7k<br />
降為70km/h</p></td>
</tr>
</tbody>
</table>

## 參考來源

## 外部連結

  -
  - [交通部高速公路局](https://www.freeway.gov.tw/default.aspx)

      - [台北聯絡線交流道里程一覽表](http://www.freeway.gov.tw/Publish.aspx?cnid=1906&p=4619)

  -
[Category:國道三號 (中華民國)](../Category/國道三號_\(中華民國\).md "wikilink")
[Category:台灣快速公路](../Category/台灣快速公路.md "wikilink")
[Category:台北市道路](../Category/台北市道路.md "wikilink")
[Category:新北市道路](../Category/新北市道路.md "wikilink")