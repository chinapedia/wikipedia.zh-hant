**Comic
Sans**是一個似手寫的字體，由[文森特·康奈爾](../Page/文森特·康奈爾.md "wikilink")（Vincent
Connare）設計，並在1994年由[微軟公司發布](../Page/微軟公司.md "wikilink")。其被分類為一種隨性且不連續的字體，設計基礎來自於[漫画书裡的字體](../Page/漫画书.md "wikilink")，可使用在非正式的文件裡。從[Windows
95之後](../Page/Windows_95.md "wikilink")，此字體就一直附帶在微軟的[視窗系統裡](../Page/視窗系統.md "wikilink")，已變成微軟系統內最常用的字型之一。

Comic
Sans經常用在紙本[漫畫以及](../Page/漫畫.md "wikilink")[線上漫畫](../Page/線上漫畫.md "wikilink")（WebComic）以取代手寫，但有許多漫畫家仍喜用傳統的[電腦字體](../Page/電腦字體.md "wikilink")。

## 歷史

[Comic_Sans1.png](https://zh.wikipedia.org/wiki/File:Comic_Sans1.png "fig:Comic_Sans1.png")可選用\]\]
微軟設計師文生·康奈爾從1994年10月開始進行Comic Sans的設計工作，在這之前他已創造許多孩童取向的字體，當他看到[Microsoft
Bob](../Page/Microsoft_Bob.md "wikilink") Beta版使用[Times New
Roman為對話框的字體時](../Page/Times_New_Roman.md "wikilink")，便決定要根據他辦公室裡的漫画书，設計一個新的字體。

之後，Comic Sans成為[Windows 95](../Page/Windows_95.md "wikilink")
[OEM版本的標準字體](../Page/Original_equipment_manufacturer.md "wikilink")，最後成為[Microsoft
Publisher和](../Page/Microsoft_Publisher.md "wikilink")[Internet
Explorer的預設字體](../Page/Internet_Explorer.md "wikilink")。

## 批評

Comic
Sans後來受到一些[平面設計師的反對](../Page/平面設計.md "wikilink")，他們想要縮減甚至去除此字體的使用，指出其設計不佳，附帶在微軟字型裡造成了不正確的使用情況，譬如在文件內文以及簽名處使用此字體。另有一些字型設計師表明Comic
Sans的直橫的筆畫都一致，字母間的[字距都一樣](../Page/字距.md "wikilink")，不像是真實的手寫字體。

文生·康奈爾本人反駁說，此字體並不適用於任何情況，只適合給兒童使用的軟體。

## 反Comic Sans風潮

《[波士頓鳳凰報](../Page/波士頓鳳凰報.md "wikilink")》（*The Boston
Phoenix*）報導，此反對風潮從兩個[印第安那波利斯的平面設計師](../Page/印第安那波利斯.md "wikilink")[戴夫·康斯](../Page/戴夫·康斯.md "wikilink")（Dave
Combs）和[賀利·康斯](../Page/賀利·康斯.md "wikilink")（Holly
Combs）開始，他們發現大量錯用此字體後，架設了名为[“Ban
Comic Sans”](http://www.bancomicsans.com)（抵制Comic
Sans）的网站。此風潮帶動了更多批評的聲音，但也有支持者，如Comic
Sans Appreciation Society。

Snog部落格訪談文生·康奈爾本人之後，戴夫承認他未經康奈爾允許就使用了其網站的圖片，康奈爾沒允許的原因是Dave的電子郵件內容並沒有與他的使用目的相符。之後，戴夫受到[Shepard
Fairey的](../Page/Shepard_Fairey.md "wikilink")「[Obey
Giant](../Page/Obey_Giant.md "wikilink")」街頭塗鴉藝術的啟發。

## 著名的用途

此字體常被使用在：

  - [Beanie Babies布偶娃娃的標籤](../Page/Beanie_Babies.md "wikilink")
  - [好時](../Page/好時.md "wikilink")（Hershey's）巧克力棒的包裝說明
  - 《[模擬人生](../Page/模擬人生.md "wikilink")》（*The Sims*）遊戲軟體的包裝
  - 遊戲《[索尼克大冒险2](../Page/索尼克大冒险2.md "wikilink")》（*Sonic Adventure
    2*）和《[索尼克大冒险2戰鬥版](../Page/索尼克大冒险2戰鬥版.md "wikilink")》（*Sonic
    Adventure 2 Battle*）的字幕和多數的內文
  - [Xbox 360的遊戲](../Page/Xbox_360.md "wikilink")《[Viva
    Piñata](../Page/Viva_Piñata.md "wikilink")》
  - 目前[葡萄牙國家籃球隊的制服](../Page/葡萄牙國家籃球隊.md "wikilink")
  - 2004年[加拿大日的紀念版](../Page/加拿大日.md "wikilink")25分[加拿大元硬幣](../Page/加拿大元.md "wikilink")\[1\]
  - 《[萬惡城市](../Page/萬惡城市.md "wikilink")》（*Sin City*）電影對話文字。
  - [Doge伴隨的多色文字](../Page/Doge.md "wikilink")
  - [Undertale裡Sans的對話框所使用的字體](../Page/Undertale.md "wikilink")

## 参考文献

<div class="references-small">

<references />

  - [Connare, Vincent. “Comic Sans Background Information.” Comic Sans
    Café.](http://www.microsoft.com/typography/web/fonts/comicsns/default.htm)
  - [Connare, Vincent. “Why Comic
    Sans?”](http://www.connare.com/comic.htm)
  - Macmillan, Neil,. *An A–Z of Type Designers.* Yale University Press:
    2006. ISBN 0-300-11151-7.

</div>

## 另見

  - [網頁核心字型](../Page/網頁核心字型.md "wikilink")

## 外部連結

### 英文

  - [Comic Sans
    MS字型資訊](http://www.microsoft.com/typography/fonts/font.aspx?FID=3&FNAME=Comic%20Sans%20MS)
  - [Typowiki: Comic
    Sans](https://web.archive.org/web/20070601002901/http://typophile.com/wiki/comic_sans)
  - [Comic Sans
    Café](http://www.microsoft.com/typography/web/fonts/comicsns/default.htm)
  - [下載Windows用的Comic Sans
    MS](http://prdownloads.sourceforge.net/corefonts/comic32.exe?download)
  - [反Comic Sans宣導](http://bancomicsans.com/)
  - [對Comic
    Sans的批評](http://lmnop.blogs.com/lauren/2006/10/americas_most_f.html)
  - [NOT FUNNY Fighting the good fight against a very bad
    font](https://web.archive.org/web/20080302221412/http://bostonphoenix.com/boston/news_features/this_just_in/documents/04731913.asp)
  - [Snog
    Blog：與文生·康奈爾的訪談](http://www.manic.com.sg/blog/archives/000118.php)
  - [Comic Sans | Font for the masses or weed of the graphic
    world?](https://web.archive.org/web/20070820195754/http://seattletimes.nwsource.com/html/living/2003765237_webcomicsans28.html)
  - [Comic Sans Appreciation
    Society](https://web.archive.org/web/20060715060411/http://www.littlewonder.pwp.blueyonder.co.uk/ourcause/index.htm)
  - [Comical Euri Shop](http://www.spreadshirt.net/shop.php?sid=200098)
  - [Comic Sans Flickr group](http://www.flickr.com/groups/comicsans/)

[Category:字體](../Category/字體.md "wikilink") [Category:Windows
XP字體](../Category/Windows_XP字體.md "wikilink")
[Category:網頁核心字型](../Category/網頁核心字型.md "wikilink")
[Category:微软字体](../Category/微软字体.md "wikilink")

1.  <https://web.archive.org/web/20060210170008/http://www.cnw.ca/fr/releases/archive/June2004/22/c7529.html>