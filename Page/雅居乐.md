**雅居樂集團控股有限公司**（），前稱**雅居樂地產控股有限公司**，是[中國最具規模的](../Page/中國.md "wikilink")[房地產開發商之一](../Page/房地產開發商.md "wikilink")。

## 历史

雅居樂起初是在[廣東省](../Page/廣東省.md "wikilink")[中山市](../Page/中山市.md "wikilink")[三鄉鎮經營一家](../Page/三鄉鎮_\(中山市\).md "wikilink")[傢俬廠](../Page/傢俬.md "wikilink")，在1992年開始涉足[房地產](../Page/房地產.md "wikilink")，於2005年在[香港聯合交易所](../Page/香港聯合交易所.md "wikilink")[主板](../Page/主板.md "wikilink")[上市](../Page/上市.md "wikilink")，以房地產開發及經營爲主，同時亦廣泛涉足[酒店營運](../Page/酒店.md "wikilink")、[物業](../Page/物業.md "wikilink")[投資及物業](../Page/投資.md "wikilink")[管理等多個領域](../Page/管理.md "wikilink")，[品牌知名度享譽全國](../Page/品牌.md "wikilink")，是少數獲納入[恒生綜合指數](../Page/恒生綜合指數.md "wikilink")、[恒生綜合市值指數](../Page/恒生綜合市值指數.md "wikilink")、[恒生中國內地100及](../Page/恒生中國內地100.md "wikilink")[摩根士丹利資本國際中國指數成份股的中國房地產](../Page/摩根士丹利資本國際中國指數.md "wikilink")[企業](../Page/企業.md "wikilink")。

於2014年3月31日，雅居樂的[業務覆蓋範圍已擴展至全中國逾](../Page/業務.md "wikilink")40個[城市及](../Page/城市.md "wikilink")[地區](../Page/地區.md "wikilink")，共有逾90個處於不同發展階段的房地產項目，廣泛分佈於[廣州](../Page/廣州.md "wikilink")、中山、[佛山](../Page/佛山.md "wikilink")、[河源](../Page/河源.md "wikilink")、[惠州](../Page/惠州.md "wikilink")、[上海](../Page/上海.md "wikilink")、[南京](../Page/南京.md "wikilink")、[常州](../Page/常州.md "wikilink")、[南通](../Page/南通.md "wikilink")、[揚州](../Page/揚州.md "wikilink")、[無錫](../Page/無錫.md "wikilink")、[鎮江](../Page/鎮江.md "wikilink")、[杭州](../Page/杭州.md "wikilink")、[寧波](../Page/寧波.md "wikilink")、[滁州](../Page/滁州.md "wikilink")、[成都](../Page/成都.md "wikilink")、[西安](../Page/西安.md "wikilink")、[重慶](../Page/重慶.md "wikilink")、[鄭州](../Page/鄭州.md "wikilink")、[海南](../Page/海南.md "wikilink")、[雲南](../Page/雲南.md "wikilink")、[瀋陽及](../Page/瀋陽.md "wikilink")[天津等城市和地區](../Page/天津.md "wikilink")，[土地儲備總](../Page/土地.md "wikilink")[建築面積共計約](../Page/建築面積.md "wikilink")4,308萬[平方米](../Page/平方米.md "wikilink")（包括全部已取得[土地使用權證和擁有權益的土地](../Page/土地使用權證.md "wikilink")）。

2017年12月，雅居樂以4億港元購入[鰂魚涌](../Page/鰂魚涌.md "wikilink")[英皇道](../Page/英皇道.md "wikilink")992至998號[公務員合作社住宅](../Page/香港政府公務員建屋合作社.md "wikilink")。\[1\]

## 董事会

現時雅居樂的[董事會](../Page/董事會.md "wikilink")[主席是](../Page/主席.md "wikilink")[陳卓林](../Page/陳卓林.md "wikilink")，其他董事會成員包括[執行董事陳卓雄](../Page/執行董事.md "wikilink")、黃奉潮、梁正堅及陳忠其；[非執行董事陳卓賢](../Page/非執行董事.md "wikilink")、陸倩芳、陳卓喜及陳卓南；以及獨立非執行董事鄭漢鈞、鄺志強及張永銳。

## 參考

[Category:香港上市地產公司](../Category/香港上市地產公司.md "wikilink")
[Category:中國房地產開發公司](../Category/中國房地產開發公司.md "wikilink")
[Category:中國民營企業](../Category/中國民營企業.md "wikilink")
[Category:中山公司](../Category/中山公司.md "wikilink")
[Category:香港交易所上市認股證](../Category/香港交易所上市認股證.md "wikilink")

1.  [雅居樂4億 購前公僕合作社 鰂魚涌4幢舊樓
    料補價後重建](http://ps.hket.com/article/1962549/雅居樂4億%20購前公僕合作社%20鰂魚涌4幢舊樓%20料補價後重建)