**放线菌酮**是一种具苦味的物質，在[真核生物中对](../Page/真核生物.md "wikilink")[蛋白質生物合成过程有](../Page/蛋白質生物合成.md "wikilink")[抑制效应的化合物](../Page/酶抑制剂.md "wikilink")，它是[灰色链霉菌](../Page/灰色链霉菌.md "wikilink")（）的一种产物。它通过干扰蛋白质合成过程中的易位步骤而阻碍[轉译过程](../Page/轉译_\(遗传学\).md "wikilink")。在生物药学研究中放线菌酮常被用来抑制生物体外真核细胞的蛋白质合成。这个方法非常有效、快速和廉价。而且通过将放线菌酮从试管中去除掉可以很快地取消它的作用。

由于放线菌酮拥有非常强烈的毒性，包括[损害DNA](../Page/DNA修復.md "wikilink")、[导致畸形胎儿和其它对](../Page/畸形學.md "wikilink")[繁殖过程的效应](../Page/繁殖.md "wikilink")（包括出生障碍和对[精子的毒性](../Page/精子.md "wikilink")\[1\]），它一般只被用在体外的研究应用中，它不适宜在人体内作为[抗菌素使用](../Page/抗菌素.md "wikilink")。过去在农业中它被用来作为[杀真菌剂](../Page/杀真菌剂.md "wikilink")，但是由于对它的危险性的认识不断提高这个用法已经很少了。

[碱可以破坏放线菌酮](../Page/碱.md "wikilink")，因此假如工作台面或者容器被放线菌酮污染后，只要使用无害的碱溶液（比如肥皂）清洗就可以了。

## 体外应用

在分子生物学中放线菌酮可被用来确定[蛋白质](../Page/蛋白质.md "wikilink")（[酶](../Page/酶.md "wikilink")）的半衰期。使用放线菌酮处理细胞，然后使用[蛋白质印迹法来显示要被研究的蛋白质随时间的数量变化](../Page/蛋白质印迹法.md "wikilink")。由于放线菌酮阻碍蛋白质的合成，细胞内的蛋白质不断减少。

## 参考资料

<references/>

[Category:抗生素](../Category/抗生素.md "wikilink")
[Category:杀真菌剂](../Category/杀真菌剂.md "wikilink")
[Category:酮](../Category/酮.md "wikilink")
[Category:戊二酰亚胺](../Category/戊二酰亚胺.md "wikilink")
[Category:醇](../Category/醇.md "wikilink")
[Category:蛋白质合成抑制剂](../Category/蛋白质合成抑制剂.md "wikilink")

1.  [TOXNET](http://toxnet.nlm.nih.gov/index.html)