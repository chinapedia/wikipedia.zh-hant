**宫相**（）是欧洲[中世纪早期的一个官职](../Page/中世纪.md "wikilink")，7世纪至8世纪间[法兰克王国都有此官](../Page/法兰克王国.md "wikilink")。名义上是掌管宫廷政务以及辅佐君王的亲近官员，但后来演变成王国内实权所在。

[墨洛温王朝晚期的君王](../Page/墨洛温王朝.md "wikilink")，许多都被宫相用作[傀儡](../Page/傀儡政權.md "wikilink")，后来因而被称为“无为王”（）。随后此官职被允许父子传承，更加巩固了其权力，最终传到[查理·马特手中](../Page/查理·马特.md "wikilink")，737年国王逝世，查理虽无篡位，却也不立新君，直到741年其子[丕平三世便建立了](../Page/丕平三世.md "wikilink")[卡洛林王朝](../Page/卡洛林王朝.md "wikilink")。

这个官职后世多指管理贵族豪宅事务的[傭人](../Page/傭人.md "wikilink")，与其本意（Maior
domus实指“管家”）更接近。

## 歷任宮相列表

#### [奧斯特拉西亞](../Page/奧斯特拉西亞.md "wikilink")

  - ？-548 : Parthemius
  - ？-581 : [Gogon](../Page/Gogon.md "wikilink")\[1\]{{,}}\[2\]
  - 581-? : Wandelin\[3\]
  - 596-600 : [Gundulf](../Page/Gundulf_de_Tongres.md "wikilink") (†
    607), évêque de Tongres\[4\]
  - 613-616 : [Radon](../Page/Radon_\(maire_du_palais\).md "wikilink")
  - 616-618 : [Hugues](../Page/Hugues_\(maire_du_palais\).md "wikilink")
    ancêtre probable des
    [Hugobertides](../Page/Hugobertides.md "wikilink")\[5\]
  - av. 624-632 : [蘭登的丕平](../Page/蘭登的丕平.md "wikilink") († 640)\[6\]
  - 632-639 : [Adalgisel](../Page/Adalgisel.md "wikilink")\[7\]
  - 639-640 : 蘭登的丕平 († 640), de nouveau
  - 640-643 : [Otton](../Page/Otton_\(maire_du_palais\).md "wikilink")
    († 643)\[8\]
  - 643-657 : [老格里摩爾德](../Page/老格里摩爾德.md "wikilink") († 657), fils de
    Pépin l'Ancien\[9\]
  - 662-676 : [Wulfoald](../Page/Wulfoald.md "wikilink") (†
    680)\[10\]{{,}}\[11\]
  - 676-714 : [丕平二世](../Page/丕平二世.md "wikilink") († 714), petit-fils de
    Pépin l'Ancien\[12\]
  - 714-717 :
    [Théodebald](../Page/Théodebald_\(maire_du_palais\).md "wikilink")
    († 741), fils de Grimoald II\[13\]
  - 717-741 : [查理·馬特](../Page/查理·馬特.md "wikilink") († 741), fils de
    Pépin le Jeune\[14\]
  - 741-747 : [卡洛曼](../Page/卡洛曼_\(查理·馬特的長子\).md "wikilink") († 754),
    fils de Charles Martel\[15\]
  - 747-753 :
    [Drogon](../Page/Drogon_\(fils_de_Carloman\).md "wikilink"), fils de
    Carloman\[16\]

#### [紐斯特利亞](../Page/紐斯特利亞.md "wikilink")

  - 604-613 : [Landéric](../Page/Landéric.md "wikilink") († 613)\[17\]
  - 613-ap. 630 : [Gundoland](../Page/Gundoland.md "wikilink") (†
    ap.630)\[18\]
  - av. 635-642 : [Aega](../Page/Aega.md "wikilink") († 642)\[19\]
  - 642-658 : [Erchinoald](../Page/Erchinoald.md "wikilink") († 658),
    cousin de [Dagobert Ier](../Page/Dagobert_Ier.md "wikilink")\[20\]
  - 658-675 : [Ébroïn](../Page/Ébroïn_\(maire_du_palais\).md "wikilink")
    († 681)\[21\]
  - 675-676 : [Leudesius](../Page/Leudesius.md "wikilink") († 676), fils
    d'Erchinoald\[22\]
  - 676-681 : Ébroïn († 681), de nouveau
  - 681-684 : [Waratto](../Page/Waratton.md "wikilink") († 686)\[23\]
  - 684-685 : [Ghislemar](../Page/Ghislemar.md "wikilink") († 685), fils
    de Warrato\[24\]
  - 685-686 : Waratto († 686), de nouveau\[25\]
  - 686-687 :
    [Berchaire](../Page/Berchaire_\(maire_du_palais\).md "wikilink") (†
    688), gendre de Warrato\[26\]
  - 687-695 : [Nordebert](../Page/Nordebert.md "wikilink") († 695),
    nommé par Pépin le Jeune, maire du palais d'Austrasie\[27\]
  - 695-714 : [格里莫二世](../Page/格里莫二世.md "wikilink") († 714), fils de
    Pépin le Jeune\[28\]
  - 714-715 :
    [Théodebald](../Page/Théodebald_\(maire_du_palais\).md "wikilink")
    († 741), fils de Grimoald II\[29\]
  - 715-717 : [Ragenfred](../Page/Rainfroi.md "wikilink")\[30\]
  - 717-741 : [查理·馬特](../Page/查理·馬特.md "wikilink") († 741), fils de
    Pépin le Jeune\[31\]
  - 741-751 : [矮子丕平](../Page/矮子丕平.md "wikilink") († 768), fils de
    Charles Martel\[32\]

#### [勃艮第王國](../Page/勃艮第王國.md "wikilink")

  - 596-600 : [Warnachaire Ier](../Page/Warnachaire_Ier.md "wikilink")
    († 600)\[33\]
  - 600-603 : [Bertoald](../Page/Bertoald.md "wikilink") († 603)\[34\]
  - 603-607 : [Protadius](../Page/Protadius.md "wikilink") († 607)\[35\]
  - 607-v. 610 :
    [Claudius](../Page/Claudius_\(maire_du_palais\).md "wikilink") (†
    610)\[36\]
  - v. 610-627 : [Warnachaire II](../Page/Warnachaire_II.md "wikilink")
    († 627), fils probable de Warnachaire \[37\]

À sa mort, les nobles du royaume de Bourgogne décident de ne plus avoir
de maire du palais. Ils sont gouvernés directement par la Neustrie, mais
cela n'empêche pas la reine Nantilde de nommer un maire du palais de
Bourgogne\[38\] :

  - 642-643 : [Flachoad](../Page/Flachoad.md "wikilink") nommé aussi
    [Floachatus](../Page/Floachatus.md "wikilink")\[39\] († 643), marié
    à Ragnoberte, nièce de la reine
    [Nantilde](../Page/Nantilde.md "wikilink")\[40\]

En 690, après avoir vaincu la Neustrie, [Pépin le
Jeune](../Page/Pépin_de_Herstal.md "wikilink") nomme ses fils comme «
duc des Bourguignons » :

  - 690-708 : [Drogon](../Page/Drogon_de_Champagne.md "wikilink") (†
    708), duc de Champagne et des Bourguignons, fils de Pépin le
    Jeune\[41\]
  - 708-714 : [格里莫二世](../Page/格里莫二世.md "wikilink") († 714), duc des
    Bourguignons, fils de Pépin le Jeune\[42\]

#### [阿基坦](../Page/阿基坦.md "wikilink")

  - 627-632 [Brodulf](../Page/Brodulf.md "wikilink")

## 相關條目

  - [霸府政治](../Page/霸府政治.md "wikilink")（[魏晉南北朝](../Page/魏晉南北朝.md "wikilink")）

  - [中国公](../Page/中国公.md "wikilink")（[大理國](../Page/大理國.md "wikilink")）

  - [幕府政治](../Page/幕府_\(日本\).md "wikilink")（[日本](../Page/日本.md "wikilink")）

  - [武家政權](../Page/武家政權.md "wikilink")（[日本](../Page/日本.md "wikilink")）

  - [武臣政權](../Page/武臣政權.md "wikilink")（[高麗國](../Page/高麗國.md "wikilink")）

  - [鄭主府寮](../Page/鄭主.md "wikilink")（[後黎朝](../Page/後黎朝.md "wikilink")）

  - （[馬拉塔帝國](../Page/馬拉塔帝國.md "wikilink")）

  - [造王者](../Page/造王者.md "wikilink")

## 參考文獻

[Category:官职](../Category/官职.md "wikilink")
[Category:中世纪政治](../Category/中世纪政治.md "wikilink")

1.  [Venance Fortunat](../Page/Venance_Fortunat.md "wikilink"), *Poèmes*
    (*Carmina*), livre VII, chap. 4, lettre de Fortunat à Gogon, maire
    du palais d’Austrasie. ; Grégoire de Tours, Historia Francorum

2.

3.
4.

5.   et

6.   et

7.  Site de la FMG :
    [Adalgisel](http://fmg.ac/Projects/MedLands/FRANKSMaiordomi.htm#_Toc184117343)

8.  Site de la FMG :
    [Otton](http://fmg.ac/Projects/MedLands/FRANKSMaiordomi.htm#_Toc184117345)

9.  Site de la FMG : [Pepin et
    Grimoald](http://fmg.ac/Projects/MedLands/FRANKSMaiordomi.htm#_Toc184117347)

10.

11. Site de la FMG :
    [Wulfoad](http://fmg.ac/Projects/MedLands/FRANKSMaiordomi.htm#_Toc184117348)

12.

13.

14.

15.

16.

17. Site de la FMG : [*Maiores Domus* of the Kingdom of
    Neustria](http://fmg.ac/Projects/MedLands/FRANKSMaiordomi.htm#_Toc184117356)

18.
19. Site de la FMG :
    [Aega](http://fmg.ac/Projects/MedLands/FRANKSMaiordomi.htm#_Toc184117358)

20. Site de la FMG : [Erchinoald et
    Leudegisius](http://fmg.ac/Projects/MedLands/FRANKSMaiordomi.htm#_Toc184117359)

21. Site de la FMG : [Ebroin, Warrato et
    Ghislemar](http://fmg.ac/Projects/MedLands/FRANKSMaiordomi.htm#_Toc184117361)

22.
23.
24.
25.
26. Site de la FMG :
    [Berthechar](http://fmg.ac/Projects/MedLands/FRANKSMaiordomi.htm#_Toc184117362)

27. Site de la FMG :
    [Nordbert](http://fmg.ac/Projects/MedLands/FRANKSMaiordomi.htm#_ftnref496)

28.
29.
30.

31.
32.

33. Site de la FMG : [*Maiores Domus* of the Kingdom of
    Burgundy](http://fmg.ac/Projects/MedLands/FRANKSMaiordomi.htm#_Toc184117355)

34.
35.
36.
37.
38. Site de la FMG : [Merovingian Nobility :
    Introduction](http://fmg.ac/Projects/MedLands/FRANKSMaiordomi.htm#_Toc184117341).

39. De l'esprit des lois Par Charles-Louis de Secondat Montesquieu où
    Flachoad est nommé Floachatus

40.
41.

42.