\-{H|zh:卡內基美濃;zh-hans:卡内基梅隆;zh-hant:卡内基美隆;zh-tw:卡內基美濃;}-
**reCAPTCHA**計畫是由[卡內基美濃大學所發展的系統](../Page/卡內基梅隆大學.md "wikilink")，主要目的是利用[CAPTCHA技術來幫助典籍數位化的進行](../Page/CAPTCHA.md "wikilink")，這個計畫將由書本掃描下來無法準確的被[光學文字辨識技術](../Page/OCR.md "wikilink")（，[縮寫](../Page/縮寫.md "wikilink")：OCR）識別的文字顯示在CAPTCHA問題中，讓人類在回答CAPTCHA問題時用人腦加以識別\[1\]。reCAPTCHA正數位化《[紐約時報](../Page/紐約時報.md "wikilink")》（*New
York
Times*）的掃描存檔\[2\]，目前已經完成20年份的資料，並希望在2010年完成110年份的資料。2009年9月17日，[Google宣布收购reCAPTCHA](../Page/Google.md "wikilink")。\[3\]

## 運作方式

為了驗證人類所輸入的文字是正確的，而不是隨意輸入，有兩個字會被顯示出來；一個是光學文字辨識軟體無法辨別的字，另一個是一個已經知道正確答案的字。如果使用者正確的回答出已知正確答案的字，那麼就假設所輸入的另一個光學辨識軟體無法辨識的字是認真的檢視後被輸入而非隨便輸入。\[4\]\[5\]

[Modern-captcha.jpg](https://zh.wikipedia.org/wiki/File:Modern-captcha.jpg "fig:Modern-captcha.jpg")

reCAPTCHA問題的所需的文字圖片，首先會由reCAPTCHA計畫網站利用[Javascript](../Page/Javascript.md "wikilink")
[API取得](../Page/API.md "wikilink")\[6\]，在終端使用者回答問題後，伺服器再連回reCAPTCHA計畫的主機驗證使用者的輸入是否正確。reCAPTCHA計畫提供了許多程式語言的函式庫，讓整合reCAPTCHA服務到現有程式的過程可以輕鬆些。除非有較大的頻寬需求，否則reCAPTCHA原則上是一個免費的服務。\[7\]

2012年起，reCAPTCHA除了原來的文字掃描圖片外，也採用[Google街景拍攝的門牌號碼照片](../Page/Google街景.md "wikilink")。\[8\]

2014年年底，改以「我不是機器人」（I'm not a robot）於方框中打勾，進而完成判別。\[9\]

## 參考文獻

## 外部連結

  - [Official Google Blog: Teaching computers to read: Google acquires
    reCAPTCHA](http://googleblog.blogspot.com/2009/09/teaching-computers-to-read-google.html)
  - [reCAPTCHA計畫](https://www.google.com/recaptcha)

[Category:人工智能](../Category/人工智能.md "wikilink")
[Category:電腦安全](../Category/電腦安全.md "wikilink")
[Category:卡內基美隆大學](../Category/卡內基美隆大學.md "wikilink")
[Category:众包](../Category/众包.md "wikilink")
[Category:Google](../Category/Google.md "wikilink")
[Category:光学字符识别](../Category/光学字符识别.md "wikilink")
[Category:图灵测试](../Category/图灵测试.md "wikilink")
[Category:網頁技術](../Category/網頁技術.md "wikilink")

1.  [reCAPTCHA: Human-Based Character Recognition via Web Security
    Measures](http://www.cs.cmu.edu/~biglou/reCAPTCHA_Science.pdf)
2.  <http://recaptcha.net/learnmore.html>
3.
4.
5.  [用對抗垃圾信件的武器協助保存書籍](http://news.bbc.co.uk/2/hi/technology/7023627.stm);
    BBC news report by Paul Rubens, 2007-10-02.
6.  [reCAPTCHA計畫](http://recaptcha.net/)  -
    由[卡內基美濃的卡內基美濃學院資訊科學系執行](../Page/CMU.md "wikilink")。
7.
8.
9.  <https://www.ithome.com.tw/news/92757>
    Google釋出新版reCAPTCHA機制，只要點「我不是機器人」！
    文/林妍溱 2014-12-04 ， iThome臺灣