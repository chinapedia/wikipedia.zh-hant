**BVE Trainsim**，（原名為「\[1\]，Boso View Express」），中譯為**暴走列車**，是一款在[Windows
系統](../Page/Microsoft_Windows.md "wikilink")\[2\]上[免費使用的](../Page/免費軟體.md "wikilink")[模擬鐵路列車駕駛](../Page/列车模拟器.md "wikilink")[電腦遊戲](../Page/電腦遊戲.md "wikilink")，由[日本網友Mackoy](../Page/日本.md "wikilink")\[3\]開發。由於[玩家可以為](../Page/玩家.md "wikilink")[遊戲自行設計](../Page/遊戲.md "wikilink")、製作列車、-{zh:路轨;zh-hans:路軌;zh-tw:軌道;zh-hk:路軌;}-、佈景等元素，同時可下載世界上眾多網路用戶公開的[車輛檔案](../Page/鐵路列車.md "wikilink")，享受各式各樣的[路線和](../Page/鐵軌.md "wikilink")[車輛的駕駛樂趣](../Page/鐵路列車.md "wikilink")，因此深受多國[玩家的歡迎](../Page/玩家.md "wikilink")。

## 簡介

BVE的遊戲除了[-{zh:软件;zh-hans:软件;zh-tw:主程式;zh-hk:軟件;}-外](../Page/軟件.md "wikilink")，主要由兩種[-{zh:要素;zh-hans:要素;zh-tw:附加檔案;zh-hk:要素;}-組成](../Page/外掛程式.md "wikilink")：[**列車**和](../Page/鐵路列車.md "wikilink")**路線**。

在BVE，一列模擬列車的元素包括：駕駛室的環境（一般以圖像材質顯示）、極速、自動或手動操控、[軌距](../Page/軌距.md "wikilink")、行走音和預設的加減速率等等。而一條模擬路線則包括定線[走向](../Page/方位.md "wikilink")、[-{zh:景物;zh-hans:景物;zh-tw:場景;zh-hk:景物;}-](../Page/虛擬現實.md "wikilink")、[-{zh:讯号;zh-hans:讯号;zh-tw:號誌;zh-hk:訊號;}-](../Page/鐵路訊號.md "wikilink")、限速、[車站](../Page/鐵路車站.md "wikilink")（停車處）及沿途所需[聲音](../Page/音效.md "wikilink")（如[月台](../Page/車站月台.md "wikilink")[廣播](../Page/廣播.md "wikilink")）等元素。

  -
    <small>本條目後段「路線和列車的製作」有更詳細的描述。</small>

某程度上，BVE像一台鐵路列車[駕駛](../Page/駕駛.md "wikilink")[模擬器](../Page/模擬器.md "wikilink")。原作者提供的[-{zh:软件;zh-hans:软件;zh-tw:軟體;zh-hk:軟件;}-只定義了操控列車的方法](../Page/軟件.md "wikilink")（鍵盤控制）以及列車行走時的景物變換規則，而幾乎沒有提供列車及路線\[4\]供[玩家使用](../Page/玩家.md "wikilink")。然而，世界各地的BVE玩家製作了極多不同的列車和路線，大大提高了遊戲的豐富性和可玩性。

## 版本

### BVE 1

2000年11月BVE1.22版類似目前的
BVE2，當初作者開發時因為當時沒有和[TAITO](../Page/TAITO.md "wikilink")[電車でGO\!的遊戲而自行製作此駕駛遊戲](../Page/電車GO!.md "wikilink")，後來陸續修改並受到前述遊戲的影響，正式公開發表時與BVE2最大的不同特點在於有計分模式和自由模式，當計分模式中駕駛不當或分數過低時可能直接結束遊戲，於
BVE2 正式公開發表後停止公開。

### BVE 2

從2001年開始至2004年3月的最終版本為止是絕大多數世界各地的[玩家認識且製作列車和路線的開始](../Page/玩家.md "wikilink")，與前版本不同處在於增加[ATC
系統](../Page/自動列車控制裝置.md "wikilink")，可使用[-{zh:窗口;zh-hans:窗口;zh-tw:視窗模式;zh-hk:視窗;}-執行](../Page/視窗.md "wikilink")，增加列車音源、路線中-{zh:景物;zh-hans:景物;zh-tw:物件;zh-hk:景物;}-可運用數量，並且路線和-{zh:景物;zh-hans:景物;zh-tw:物件;zh-hk:景物;}-新增加
[CSV](../Page/Csv.md "wikilink")
-{zh:文件;zh-hans:文件;zh-tw:檔案;zh-hk:檔案;}-格式，需要安裝[微軟
DirectX](../Page/DirectX.md "wikilink")，不過因為[DirectX
10不支援而無法在](../Page/DirectX.md "wikilink") [Windows
Vista](../Page/Windows_Vista.md "wikilink")
運行。而適用於[BVE2的路線同時亦可與在手機上的應用程式](../Page/BVE2.md "wikilink")——[Hmmsim上遊玩](../Page/Hmmsim.md "wikilink")

### BVE 3

2003年間曾經公佈改良BVE2的計畫說明，但後來並未完成及公開，在2004年BVE4開發時有提到因為「 Ver. 3
は、抵抗制御を再現しようとして失敗したものです。」而停止開發及公開的訊息。\[5\]

### BVE 4

2005年1月正式公開發表的BVE4取消了BVE2的輔助視窗及取消支援RW-{zh:文件;zh-hans:文件;zh-tw:檔案;zh-hk:檔案;}--{zh:格式;zh-hans:格式;zh-tw:格式;zh-hk:延伸;}-的路線，加入視野放大縮小、駕駛台的光暗、按鍵變更等功能，以更真實的全畫面取代，另外採用[DLL增加了列車的各種新功能](../Page/動態連結庫.md "wikilink")，如信號、儀表、雨刷、列車保安裝置等等，[幀率更提高至](../Page/幀率.md "wikilink")60fps，需要在[微軟
.NET平台方能運行](../Page/.NET_Framework.md "wikilink")，因為[DirectX
10不支援而無法在](../Page/DirectX.md "wikilink")[Windows
Vista運行](../Page/Windows_Vista.md "wikilink")。此版本的路線亦能與[Hmmsim上遊玩](../Page/Hmmsim.md "wikilink")。

### BVE 5

2008年7月Mackoy正式發表開發[BVE5](../Page/BVE5.md "wikilink")，主要原因是BVE4無法在[Windows
Vista運行](../Page/Windows_Vista.md "wikilink")，因此以新的程式語法重新編寫BVE5。從目前體驗版本得知路線、車輛將使用新的指令語法、句法和如同[OpenBVE取消](../Page/OpenBVE.md "wikilink")25m的區塊限制等\[6\]，-{zh:景物;zh-hans:景物;zh-tw:物件;zh-hk:景物;}-僅能使用[微軟
DirectX](../Page/DirectX.md "wikilink")-{zh:景物;zh-hans:景物;zh-tw:物件;zh-hk:景物;}-檔案（.x
-{zh:格式;zh-hans:格式;zh-tw:格式;zh-hk:延伸;}-）。自2011年9月起正式開放下載，目前仍在持續開發中，且已演進至5.7版(2017年3月17日公開)。此外從Ver.im0.8開始，對向列車以及平交道、號誌、沿線公路上的汽車等物件在遊戲中皆能以動態形式呈現，克服了歷代BVE遊戲中除了玩家的列車外其餘物件皆為靜止不動的缺點。

### 衍生程式

#### openBVE (Train Simulation Framework)

2008年4月由reschanger所發表的 [openBVE](../Page/openBVE.md "wikilink")（現稱[Train
Simulation
Framework](../Page/Train_Simulation_Framework.md "wikilink")）並非BVE的新版本，但成為新一代能夠執行BVE2、BVE4路線及車輛規格所編寫的列車駕駛模擬器，開發主因為BVE並非一個[開放源碼和](../Page/開放源碼.md "wikilink")[跨平台的](../Page/跨平台.md "wikilink")[-{zh:软件;zh-hans:软件;zh-tw:主程式;zh-hk:軟件;}-](../Page/軟件.md "wikilink")\[7\]，還有
BVE 的後續版本開發遲緩。[Train Simulation
Framework的特色包括使用](../Page/Train_Simulation_Framework.md "wikilink")[OpenGL](../Page/OpenGL.md "wikilink")（圖形）、[OpenAL](../Page/OpenAL.md "wikilink")（音效）處理而非支援
[DirectX](../Page/DirectX.md "wikilink")，並可以經由
[Mono或](../Page/Mono.md "wikilink")[Wine在](../Page/Wine.md "wikilink")[Linux和](../Page/Linux.md "wikilink")[OS
X平台上使用](../Page/Mac_OS_X.md "wikilink")，而且增加了功能包括車内外視角自由觀察、立體駕駛室等。

## 列車操控

BVE 的列車可由[鍵盤或](../Page/鍵盤.md "wikilink")所用的控制桿來操控。

除了普通的向前、向後、加速、制動(減速)等功能外，BVE亦支援多種列車操控功能，例如[自動行車操作
(ATO)及緊急制動等](../Page/ATO.md "wikilink")。

## 路線和列車的製作

路線和列車的定義是由多個-{zh:纯文本文件;zh-hans:纯文本文件;zh-tw:純文字檔案;zh-hk:純文字檔案;}-組成。

### 檔案延伸

在 BVE1.22版本，路線的-{zh:文件扩展名;zh-hans:文件扩展名;zh-tw:副檔名;zh-hk:延伸名稱;}-是
[RW](../Page/RW.md "wikilink")，而-{zh:景物;zh-hans:景物;zh-tw:物件;zh-hk:景物;}-的-{zh:文件扩展名;zh-hans:文件扩展名;zh-tw:副檔名;zh-hk:延伸名稱;}-是
[B3D](../Page/B3D.md "wikilink")，所謂的-{zh:景物;zh-hans:景物;zh-tw:物件;zh-hk:景物;}-就是
[3D圖型](../Page/三維電腦圖形.md "wikilink")。

BVE 2.0～2.63 的版本則新支援的 [CSV](../Page/Csv.md "wikilink")
-{zh:文件;zh-hans:文件;zh-tw:檔案;zh-hk:檔案;}-格式。無論是定義路線還是-{zh:景物;zh-hans:景物;zh-tw:物件;zh-hk:景物;}-，均可以
[CSV](../Page/Csv.md "wikilink") 的形式儲存。

目前的 4.2 版本已取消支援 RW
-{zh:文件;zh-hans:文件;zh-tw:檔案;zh-hk:檔案;}-延伸的路線。此版本亦局部支援微軟
DirectX-{zh:景物;zh-hans:景物;zh-tw:物件;zh-hk:景物;}-檔案（.x
-{zh:格式;zh-hans:格式;zh-tw:格式;zh-hk:延伸;}-）。

### 圖形

部份純色的圖形可透過程式碼直接於路線或列車檔中定義。其他圖形則必需以[點陣圖](../Page/點陣圖.md "wikilink") (BMP)
格式儲存，並於定義路線或列車的檔案中取用。

### 聲音

在 BVE 的所有聲音均以簡單的 [WAV](../Page/WAV.md "wikilink") 格式儲存，並由定義路線或列車的檔案取用。

## 輔助工具

### 官方輔助工具

除了[模擬器的](../Page/模擬器.md "wikilink")[-{zh:软件;zh-hans:软件;zh-tw:主程式;zh-hk:軟件;}-外](../Page/軟件.md "wikilink")，
BVE
的官方網站還提供了一些輔助工具（除特別註明外均有[日文及](../Page/日文.md "wikilink")[英文版本提供](../Page/英文.md "wikilink")）：

  - \-{zh:路轨;zh-hans:路軌;zh-tw:路線;zh-hk:路軌;}-檢視器（Track Viewer）：用以開啟並觀看 RW
    或 CSV 格式的路線檔。
  - \-{zh:景物;zh-hans:景物;zh-tw:物件;zh-hk:景物;}-檢視器（Structure
    Viewer）：用以開啟並觀看 B3D 或 CSV 格式的景物檔。

　　　　　　　　　　　　　　　 最新的版本更可讀入及輸出 [DirectX](../Page/DirectX.md "wikilink")
靜態景物。

  - [-{zh:摩打;zh-hans:摩打;zh-tw:動力;zh-hk:摩打;}-編輯器](../Page/摩托.md "wikilink")（Motor
    Editor）：用以編輯列車運行的聲音。
  - \-{zh:列車;zh-hans:列車;zh-tw:車輛;zh-hk:列車;}-編輯器（Train
    Editor）：（日）用以設定列車性能。
  - 儀錶背景製作器（Gauge）：（日）用以製作刻度平均的儀錶背景。
  - 物件鏡像器（Mirror）：（日）輸入某物件，產生的物件左右倒轉，方便[玩家觀看](../Page/玩家.md "wikilink")、編輯及修改。
  - B3D-CSV-{zh:景物;zh-hans:景物;zh-tw:物件;zh-hk:景物;}-格式轉換器（B3D to CSV
    converter）：（日）能為 B3D -{zh:景物;zh-hans:景物;zh-tw:物件;zh-hk:景物;}-檔轉換格式成
    CSV，反之亦然。
  - 駕駛台升級器（Panel Upgrader）：（英）匯入舊版列車駕駛台定義檔，輸出BVE第四版適用的新格式。
  - CSV-X-{zh:景物;zh-hans:景物;zh-tw:物件;zh-hk:景物;}-格式轉換器（CSV to X
    converter）：（英）能為 CSV -{zh:景物;zh-hans:景物;zh-tw:物件;zh-hk:景物;}-檔轉換格式成
    X。

### 其他愛好者製作的工具

另外，部份網友\[8\]亦自行製作了一個能自動建置路線檔的程式：RouteBuilder。
此程式的網頁見「相關鏈接網站」一節。除此以外，尚有不少-{zh:软件;zh-hans:软件;zh-tw:軟體;zh-hk:軟件;}-和試算表幫助使用者計算、製作景物、等等。

## 缺點

以下是其中幾個關於 BVE 的主要缺點：

1.  由於 BVE2
    並未提供有效的[字串轉換功能](../Page/字串.md "wikilink")，很多由[亞洲](../Page/亞洲.md "wikilink")[玩家設計的列車或路線需要經過重整才能在使用其他](../Page/玩家.md "wikilink")[亞洲](../Page/亞洲.md "wikilink")、[西歐](../Page/西歐.md "wikilink")[字元](../Page/字元.md "wikilink")[編碼的](../Page/編碼.md "wikilink")[電腦上運行](../Page/電腦.md "wikilink")。
2.  同上述問題，使用[逗號為小數點的國家在讀取使用](../Page/逗號.md "wikilink")[句點做小數點的國家製作之路線時](../Page/句點.md "wikilink")，玩家需在[控制面板中變更](../Page/控制面板.md "wikilink")[小數符號](../Page/小數點.md "wikilink")、數字分位符號、清單分隔字元更換，避免造成[-{zh:景物;zh-hans:景物;zh-tw:場景;zh-hk:景物;}-發生錯誤](../Page/虛擬現實.md "wikilink")，甚至無法執行。
3.  在
    BVE1至BVE4中，唯一移動的東西就是[玩家駕駛的列車](../Page/玩家.md "wikilink")，其他周圍的事物是[靜態的](../Page/靜止.md "wikilink")。所以，[玩家在中途遇到的移動物件](../Page/玩家.md "wikilink")（如迎面而來的另一列車）在
    BVE 看來是完全靜止的。(此項缺點已在BVE5中克服)
4.  缺少列車車燈燈光效果、[駕駛](../Page/駕駛.md "wikilink")[視角受到控制無法改變](../Page/視角.md "wikilink")，無法開發車輛的各種特別任務或製作[折返形式的路線有限制](../Page/折返式鐵路.md "wikilink")，路線及-{zh:景物;zh-hans:景物;zh-tw:物件;zh-hk:景物;}-能夠使用簡單、基本的程式開發、編輯，但是在無「[-{zh-cn:所见即所得;zh-tw:所見即所得;zh-hk:所視即所得;}-](../Page/所見即所得.md "wikilink")」的製作程式下，部份對空間感不佳之玩家花費不少時間研究而認為過於麻煩。\[9\]

## 相關條目

  - [列车模拟器](../Page/列车模拟器.md "wikilink")
  - [-{zh:免费软件;zh-hans:免费软件;zh-tw:免費軟體;zh-hk:免費軟件;}-](../Page/免費軟件.md "wikilink")
  - [鐵道迷](../Page/鐵道迷.md "wikilink")

## 注釋

## 外部連結

  - BVE 官方網站
      - [BVE Trainsim web site](http://bvets.net/)（日語）
  - openBVE 官方網站
      - [openBVE Official Homepage](http://trainsimframework.org/)（英語）
  - 路線數據搜尋資料庫
      - [International database of BVE and openBVE
        Routes](https://web.archive.org/web/20110307121101/http://wiki.bve-routes.com/index.php/Main_Page)
      - [](https://archive.is/20121219080935/http://members.jcom.home.ne.jp/kashiwarailwayforum/)（日語）
      - [BVE Data
        Search](https://web.archive.org/web/20070405223647/http://krf.sakura.ne.jp/bve2/index.htm)（日語）
      - [BVE Data Station](http://www.345kei.net/bve/)（日語）
      - [Yahoo\! JAPAN -
        BVE](http://dir.yahoo.co.jp/Recreation/Games/PC_Games/Free_Games/Simulation/BVE/)（日語）
      - [BVEwiki](http://www11.atwiki.jp/bvewiki/)（日語）
  - BVE鐵路論壇
      - [BVEHK香港模擬鐵路聯盟](https://web.archive.org/web/20170927052931/http://bvehk.net/)
  - 路線下載、製作教導
      - [HKRSC香港模擬鐵路發展中心](https://web.archive.org/web/20170613124521/http://hkrsc.info/)
      - [BVEHK香港模擬鐵路聯盟下載中心](https://web.archive.org/web/20070415100112/http://www.bvehk.net/plugin.php?identifier=download&module=download)
      - [港鐵之最](http://bvezone.web.fc2.com/)
      - [香港急行](http://www.konkyu.net/)
      - [RouteBuilder for
        BVE](https://web.archive.org/web/20180322124953/http://routebuilder.bve-routes.com/)（英語）
      - [馬鞍山支線](https://www.webcitation.org/query?id=1256557029666328&url=hk.geocities.com/mosrail/)
      - [Chikainn83 bve 臨時站](http://island.geocities.jp/chikainn83/)
      - [I-Circle BVE 網](http://bve.i-circle.net/)
      - [近畿鐵路網站](http://kinkidt.i-cweb.net/)
      - [香港 openBVE
        開發及研究中心](https://web.archive.org/web/20121020043130/http://www.hochun.nets.hk/)
      - [香港模擬鐵路製作討論區](https://web.archive.org/web/20100926064021/http://simhk.i-cweb.net/forum/index.php)

[Category:Windows遊戲](../Category/Windows遊戲.md "wikilink")
[Category:免費遊戲](../Category/免費遊戲.md "wikilink")
[Category:铁路模拟游戏](../Category/铁路模拟游戏.md "wikilink")

1.  名稱簡略為 BVE 的「暴走ビューエクスプレス（暴走View
    Express）」，命名來源是[JR東日本255系電車](../Page/JR東日本255系電車.md "wikilink")「（」的暱稱，因為[房総日語讀音同](../Page/房總半島.md "wikilink")之趣味，不過原名也造成[漢字使用者之誤解](../Page/漢字.md "wikilink")，因此統一以[英文作表記](../Page/英文.md "wikilink")「BVE
    Trainsim」。
2.  在2007年3月1日[微軟推出的OS](../Page/微軟.md "wikilink")「[Windows
    Vista](../Page/Windows_Vista.md "wikilink")」在網路上有玩不到BVE的報告。[報告1](http://www.bvehk.net/viewthread.php?tid=1520&extra=page%3D1)
    [報告2](http://www.bvehk.net/viewthread.php?tid=1483&extra=page%3D1)
    [報告3](http://tw.myblog.yahoo.com/chikainn83-chikainn83/article?mid=172&prev=-1&next=153)
     ，不過後來有位[英國網友公佈在Vista下執行BVE](../Page/英國.md "wikilink") 4的方法
    [資料1](http://www.railforums.co.uk/showthread.php?t=13429)[資料2](http://www.bvehk.net/thread-3683-1-1.html)。
3.  原作者真正姓名：Takashi Kojima（こじま たかし），詳見
4.  。BVE官方網站只釋出兩條路線，分別為[内房線](../Page/内房線.md "wikilink")、[京成千葉線的局部路線](../Page/京成千葉線.md "wikilink")。
5.  官方站頁面已刪除，可參考 [Wayback
    Machine](https://web.archive.org/web/20040814015233/http://mackoy.cool.ne.jp/beta/)（開啟頁面為[亂碼時請轉換](../Page/亂碼.md "wikilink")[編碼為](../Page/編碼.md "wikilink")：[Shift
    JIS](../Page/Shift_JIS.md "wikilink")，）
6.  mackoy 的 [BVE Trainsim 開發筆記](http://mackoy.exblog.jp/)
7.  <http://trainsimulationframework.org/about/index.html>
8.  RouteBuilder 作者為德國的 Uwe Post 與奥地利的 Thomas Tschofenig 合作編寫。
9.  臺灣 BVE
    [網站](http://tw.myblog.yahoo.com/chikainn83-chikainn83/article?mid=153&sc=1)
     對 BVE 主程式的缺點看法。