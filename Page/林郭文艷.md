**林郭文艷**（），[台灣](../Page/台灣.md "wikilink")[企業家](../Page/企業家.md "wikilink")，現任[大同公司董事長兼總經理](../Page/大同公司.md "wikilink")。

## 家庭

[郭德焜](../Page/郭德焜.md "wikilink")、[郭林碧蓮之女](../Page/郭林碧蓮.md "wikilink")，大同公司[董事長](../Page/董事長.md "wikilink")[林蔚山之](../Page/林蔚山.md "wikilink")[妻](../Page/妻.md "wikilink")，育有二子，長子林建文
( 1977年 - 2005年 3月 24日殁 )。

## 學歷

[台北市立女子中學](../Page/台北市立女子中學.md "wikilink")（現名為[台北市立金華國民中學](../Page/台北市立金華國民中學.md "wikilink"))、[台北市立第一女子高級中學](../Page/台北市立第一女子高級中學.md "wikilink")、[國立台灣大學商學系](../Page/國立台灣大學.md "wikilink")、美國[馬里蘭大學](../Page/馬里蘭大學.md "wikilink")[經濟學](../Page/經濟學.md "wikilink")[碩士](../Page/碩士.md "wikilink")

## 經歷

歷任[大同工學院](../Page/大同工學院.md "wikilink")（[大同大學前身](../Page/大同大學_\(臺灣\).md "wikilink")）經濟學系[教授](../Page/教授.md "wikilink")、[大同電信董事長](../Page/大同電信.md "wikilink")、[大同世界科技董事長](../Page/大同世界科技.md "wikilink")、[精英電腦董事長](../Page/精英電腦.md "wikilink")、大同公司執行副總經理、大同公司總經理。

## 外部連結

  - [林郭文艷 讓大同變年輕了](http://www.gvm.com.tw/Boardcontent_9854.html)
  - [長期經營大同績效不佳，大同公司多年沒有獲利，大同寶寶哭了](http://www.ettoday.net/news/20170417/905815.htm)

[L林](../Category/台灣女性企業家.md "wikilink")
[g郭](../Category/林尚志家族.md "wikilink")
[L林](../Category/臺北市立第一女子高級中學校友.md "wikilink")
[L林](../Category/國立臺灣大學管理學院校友.md "wikilink")
[L林](../Category/馬里蘭大學校友.md "wikilink")