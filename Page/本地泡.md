[local_bubble.jpg](https://zh.wikipedia.org/wiki/File:local_bubble.jpg "fig:local_bubble.jpg")和[大犬座β](../Page/大犬座β.md "wikilink")）和Loop
I Bubble（包含[心宿二](../Page/心宿二.md "wikilink")）。\]\]
**本地泡**（）是在[銀河系](../Page/銀河系.md "wikilink")[獵戶臂內的](../Page/獵戶臂.md "wikilink")[星際物質中的一个空洞](../Page/星際物質.md "wikilink")，它跨越的範圍至少有300[光年](../Page/光年.md "wikilink")。這個炙熱的本地泡擴散的氣體輻射出[X射線](../Page/X射線.md "wikilink")，單位體積內所含有的中性[氫只有正常值的十分之一](../Page/氫.md "wikilink")。[銀河系內](../Page/銀河系.md "wikilink")[星際物質的正常值是每立方公分](../Page/星際物質.md "wikilink")0.5個[原子](../Page/原子.md "wikilink")。

[太陽系已經在這個氣泡內至少旅行了](../Page/太陽系.md "wikilink")300萬年，現在的位置在[本星際雲或](../Page/本星際雲.md "wikilink")，氣泡內物質比較密集的一個小區域內。這是本地泡和Loop
I Bubble遭遇的地方，本星際雲的密度大約是每立方公分0.1個原子。

多數的天文學家相信本地泡是數十萬年至數百萬年前的[超新星爆炸](../Page/超新星.md "wikilink")，將該處[星際物質的氣體和塵埃推開所形成的](../Page/星際物質.md "wikilink")，留下了炙熱和低密度的物質。最可能的候選者是在[雙子座的超新星殘骸](../Page/雙子座.md "wikilink")[杰敏卡](../Page/杰敏卡γ射线源.md "wikilink")。

天文學者猜測，於二百六十萬年前發生於本地泡的一次[超新星可能與在](../Page/超新星.md "wikilink")[上新世與](../Page/上新世.md "wikilink")[更新世邊界發生的一次海洋](../Page/更新世.md "wikilink")[巨型動物群集群滅絕事件有關](../Page/巨型動物群.md "wikilink")，估計大約有36%的物種（genera）被滅絕，包括[巨齒鯊在內](../Page/巨齒鯊.md "wikilink")。\[1\]

本地泡的形狀不是球型，在銀河盤面的部份比較狹窄，因此好像是橢圓型或是卵形，在銀河盤面上的較寬，盤面下的較窄，變得像是沙漏的形狀。

本地泡緊鄰著其他密度較低的星際物質，包括最明顯的**Loop I
Bubble**。在[天蠍-半人馬星協內是由](../Page/天蠍-半人馬星協.md "wikilink")[超新星和](../Page/超新星.md "wikilink")[恆星風造成的](../Page/恆星風.md "wikilink")，距離[太陽](../Page/太陽.md "wikilink")500光年。與本地泡緊鄰的還有**Loop
II Bubble**和**Loop III Bubble**。

Loop I
Bubble內的[心宿二](../Page/心宿二.md "wikilink")（天蝎座α）顯示在圖中的右上方的泡内，而在图中左边标注为Betelgeuse的泡外亮点为[猎户座的](../Page/猎户座.md "wikilink")[参宿四](../Page/参宿四.md "wikilink")。

## 相關條目

  - [超級氣泡](../Page/超級氣泡.md "wikilink")
  - [獵戶-波江超級氣泡](../Page/獵戶-波江超級氣泡.md "wikilink")
  - [古爾德帶](../Page/古爾德帶.md "wikilink")
  - [獵戶臂](../Page/獵戶臂.md "wikilink")
  - [英仙臂](../Page/英仙臂.md "wikilink")

## 參考資料

  - <https://web.archive.org/web/20060813160705/http://science.nasa.gov/headlines/y2003/06jan_bubble.htm>
  - <https://web.archive.org/web/20080229231156/http://science.nasa.gov/headlines/y2004/17dec_heliumstream.htm>
  - <http://antwrp.gsfc.nasa.gov/apod/ap020210.html>
  - <http://www.solstation.com/x-objects/chimney.htm>
  - Mark Anderson, "Don't stop till you get to the Fluff", "New
    Scientist" no. 2585, 6 Jan, 2007, pp. 26-30

[Category:獵戶臂](../Category/獵戶臂.md "wikilink")

1.