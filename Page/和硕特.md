**和碩特**（）是[清代](../Page/清代.md "wikilink")[衛拉特蒙古四部之一](../Page/衛拉特.md "wikilink")，原居[乌鲁木齐一带](../Page/乌鲁木齐.md "wikilink")，[明朝末年迁徙游牧于安多](../Page/明朝.md "wikilink")，主要分佈在[安多地區](../Page/安多地區.md "wikilink")，是[成吉思汗的兄弟](../Page/成吉思汗.md "wikilink")[合撒兒之後](../Page/合撒兒.md "wikilink")。曾建立统治[青藏高原的](../Page/青藏高原.md "wikilink")[和硕特汗国](../Page/和硕特汗国.md "wikilink")。

合撒兒的七世孫**阿克薩噶勒代**有兩子，阿魯克特穆爾為[內蒙古](../Page/內蒙古.md "wikilink")[科爾沁部始祖](../Page/科爾沁.md "wikilink")。次子烏魯克特穆爾居[兀良哈地區](../Page/兀良哈.md "wikilink")，與[韃靼的](../Page/鞑靼_\(蒙古\).md "wikilink")[呼倫貝爾毗連](../Page/呼倫貝爾.md "wikilink")。[瓦剌](../Page/瓦剌.md "wikilink")[脫歡](../Page/脫歡.md "wikilink")[太師打敗韃靼後](../Page/太師.md "wikilink")，烏魯克特穆爾前往衛拉特，脫歡稱他们為和碩特，因此得名。他們最遠侵襲至[希瓦汗國](../Page/希瓦汗國.md "wikilink")。

此部有兩人十分著名，[咱雅班第達與](../Page/咱雅班第達.md "wikilink")[固始汗](../Page/固始汗.md "wikilink")。他們是西藏[格魯派保護者與西藏實際管理者](../Page/格魯派.md "wikilink")，直到有[駐藏大臣為止](../Page/駐藏大臣.md "wikilink")。[拉藏汗死前](../Page/拉藏汗.md "wikilink")，有分管理西藏，[罗卜藏丹津作乱后](../Page/罗卜藏丹津.md "wikilink")，不再占重要地位（被分为二十一旗外加八旗成为[青海蒙古](../Page/青海蒙古.md "wikilink")）。[蒙古国](../Page/蒙古国.md "wikilink")[布尔干省与](../Page/布尔干省.md "wikilink")[阿拉善也有他们](../Page/阿拉善.md "wikilink")。他们也是现代青海蒙古一个来源。向清太宗[皇太极贡丹书那位](../Page/皇太极.md "wikilink")[伊拉古克三呼图克图也是此部](../Page/伊拉古克三呼图克图.md "wikilink")。到了1949年人口只有二千人。

## 历任首领

| 世代  | 姓名                                       | 年份          | 备注         |
| --- | ---------------------------------------- | ----------- | ---------- |
| 第一代 | [博贝密尔咱](../Page/博贝密尔咱.md "wikilink")     | 16世纪后半叶     |            |
| 第二代 | [哈尼诺颜洪果尔](../Page/哈尼诺颜洪果尔.md "wikilink") | ？－1585年     | 博贝密尔咱之子    |
| 第三代 | [拜巴噶斯](../Page/拜巴噶斯.md "wikilink")       | 1585年－1640年 | 哈尼诺颜洪果尔的次子 |
| 第三代 | [昆都伦乌巴什](../Page/昆都伦乌巴什.md "wikilink")   | 1640年－1644年 | 哈尼诺颜洪果尔的三子 |
| 第四代 | [鄂齐尔图汗](../Page/鄂齐尔图汗.md "wikilink")     | 1644年—1676年 | 拜巴噶斯之子     |
| 第三代 | [固始汗圖魯拜琥](../Page/固始汗.md "wikilink")     | 1642年－1655年 | 哈尼诺颜洪果尔的四子 |
| 第四代 | [鄂齊爾汗達延](../Page/達延鄂齊爾汗.md "wikilink")   | 1655年－1668年 | 固始汗的長子     |
| 第四代 | [达什巴图尔](../Page/达什巴图尔.md "wikilink")     | 1668年－1671年 | 固始汗的第十子    |
| 第五代 | [貫綽克達賴汗朋素克](../Page/達賴汗.md "wikilink")   | 1671年－1701年 | 達延鄂齊爾汗的長子  |
| 第六代 | [拉藏汗](../Page/拉藏汗.md "wikilink")         | 1703年－1717年 | 達賴汗朋素克之子   |
| 第五代 | [罗卜藏丹津](../Page/罗卜藏丹津.md "wikilink")     | 1723年－1724年 | 达什巴图尔之子    |

## 参考文献

  - *Санчиров В. П.* О Происхождении этнонима торгут и народа, носившего
    это название // Монголо-бурятские этнонимы: cб. ст. — Улан-Удэ: БНЦ
    СО РАН, 1996. C. 31—50. - in Russian
  - [*Ovtchinnikova O., Druzina E., Galushkin S., Spitsyn V.,
    Ovtchinnikov I.* An Azian-specific 9-bp deletion in region V of
    mitochondrial DNA is found in Europe // Medizinische Genetic. 9
    Tahrestagung der Gesellschaft für Humangenetik, 1997, p.
    85.](https://www.academia.edu/4855138/An_Asian-specific_9-bp_deletion_in_region_V_of_mitochondrial_DNA_is_found_in_Europe)
  - [*Galushkin S.K., Spitsyn V.A., Crawford M.H.* Genetic Structure of
    Mongolic-speaking Kalmyks // Human Biology, December 2001, v.73, no.
    6,
    pp. 823–834.](https://www.academia.edu/4855211/Genetic_Structure_of_Mongolic-speaking_Kalmyks)
  - [*Хойт С.К.* Генетическая структура европейских ойратских групп по
    локусам ABO, RH, HP, TF, GC, ACP1, PGM1, ESD, GLO1, SOD-A //
    Проблемы этнической истории и культуры тюрко-монгольских
    народов. Сборник научных трудов. Вып. I. Элиста: КИГИ РАН,
    2009. с. 146-183. - in
    Russian](https://www.academia.edu/4855556/_ABO_RH_HP_TF_GC_ACP1_PGM1_ESD_GLO1_SOD-A_Genetic_Structure_of_European_Oyrat_groups_based_on_loci_ABO_RH_HP_TF_GC_ACP1_PGM1_ESD_GLO1_SOD-A_)
  - \[hamagmongol.narod.ru/library/khoyt_2008_r.htm *Хойт С.К.*
    Антропологические характеристики калмыков по данным
    исследователей XVIII-XIX вв. // Вестник Прикаспия:
    археология, история, этнография. № 1. Элиста: Изд-во КГУ,
    2008. с. 220-243.\]
  - [*Хойт С.К.* Кереиты в этногенезе народов Евразии: историография
    проблемы. Элиста: Изд-во КГУ, 2008. – 82 с. ISBN
    978-5-91458-044-2 (Khoyt S.K. Kereits in enthnogenesis of peoples of
    Eurasia: historiography of the problem. Elista: Kalmyk State
    University Press, 2008. – 82 p. (in
    Russian))](https://www.academia.edu/5253542/_._-_2008._-_82_._ISBN_978-5-91458-044-2_Kereits_in_enthnogenesis_of_peoples_of_Eurasia_historiography_of_the_problem._Elista_Kalmyk_State_University_Press_2008._-_82_p._in_Russian_)
  - \[hamagmongol.narod.ru/library/khoyt_2012_r.htm *Хойт С.К.*
    Калмыки в работах антропологов первой половины XX вв. //
    Вестник Прикаспия: археология, история, этнография. № 3, 2012.
    с. 215-245.\]
  - [*Boris Malyarchuk, Miroslava Derenko, Galina Denisova, Sanj Khoyt,
    Marcin Wozniak, Tomasz Grzybowski and Ilya Zakharov* Y-chromosome
    diversity in the Kalmyks at theethnical and tribal levels // Journal
    of Human Genetics (2013),
    1–8.](https://www.academia.edu/4855574/Y-chromosome_diversity_in_the_Kalmyks_at_the_ethnical_and_tribal_levels)

{{-}}

[Category:蒙古族支系](../Category/蒙古族支系.md "wikilink")
[和硕特部](../Category/和硕特部.md "wikilink")
[Category:青海历史](../Category/青海历史.md "wikilink")
[Category:安多地区](../Category/安多地区.md "wikilink")