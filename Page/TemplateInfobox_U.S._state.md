}

</td>

<td>

州徽|州徽\]\]}}}

</td>

</tr>

</table>

| data1 = {{\#if:  |
[暱稱](../Page/美国各州昵称列表.md "wikilink")：<span class="nickname"></span>
}}

| data2 = {{\#if:  | [格言](../Page/美国各州格言列表.md "wikilink")：}}

| data3 = {{\#if:  |
[州歌](../Page/美国各州州歌列表.md "wikilink")：《<span class="State anthem"></span>》}}

| class4 = maptable | data4 =
[{{{Map}}}](https://zh.wikipedia.org/wiki/File:{{{Map}}} "fig:{{{Map}}}")

| label5 = 官方语言 | data5 =

| rowclass6 = mergedrow | label6 = 口头语言 | data6 =

| rowclass7 = mergedrow | label7 = [居民称谓](../Page/区域居民称谓词.md "wikilink")
| data7 =

| rowclass8 = mergedtoprow | label8 = {{\#ifeq:|capital

` |`[`首府`](../Page/美国各州首府列表.md "wikilink")

` |`[`首府`](../Page/美国各州首府列表.md "wikilink")
` }}`

| data8 =

| rowclass9 = mergedrow | label9 = {{\#ifeq:|capital

`  |`
`  |最大城市`
`  }}`

| data9 = {{\#ifeq:|capital||}}

| rowclass10 = mergedrow | label10 = 最大都会区 | data10 =

| rowclass11 = mergedtoprow | label11 = 面积 | data11 =
[全美第名](../Page/美国各州面积列表.md "wikilink")

| rowclass15 = mergedtoprow | label15 = 人口 | data15 =
[全美第名](../Page/美国各州人口列表.md "wikilink") }

` | rowclass2 = {{#if:``|mergedrow|mergedbottomrow}}`
` | label2 =  - 密度`
` | data2 = ``}}}/平方公里`
[`全美第``名`](../Page/美国州份人口密度列表.md "wikilink")

` | rowclass3 = mergedbottomrow`
` | label3 =  - `
` }}`

| rowclass20 = mergedtoprow | label20 = **** | data20 = {{\#if:|米}}

` | rowclass2 = mergedrow`
` | label2 =  - 平均`
` | data2 = `

` | rowclass3 = mergedbottomrow`
` | label3 =  - 最低点`
` | data3 = {{#if: `` |`
`}} {{#ifeq:``|0|（海平面）|``米}}`

}} | rowclass25 = mergedtoprow | label25 = **建州前** | data25 =
{{\#ifexist:|[}}}](../Page/{{{Former.md "wikilink")}}

| rowclass26 = {{\#ifexist:|mergedbottomrow}} | label26 =
**[加入联邦](../Page/美国州份依加入联邦顺序排列列表.md "wikilink")** | data26 =
（第个加入联邦）

| rowclass27 = mergedtoprow | label27 = **** | data27 =

| rowclass28 = mergedrow | label28 = **** | data28 =

| rowclass29 = mergedtoprow | label29 =
**[立法机构](../Page/立法机构.md "wikilink")** | data29 =

| rowclass35 = mergedtoprow | label35 = **** | data35 =

| rowclass36 = mergedbottomrow | label36 =
**[众议院议员](../Page/美国众议院.md "wikilink")** | data36 =
{{\#if:

` | ``（``）`
` | `
` }}`

| rowclass37 = mergedtoprow | label37 =
**[时区](../Page/美国各州时区列表.md "wikilink")** | data37 =
{{\#if:

` | ``    `
` | `` `
` }}`

| label38 = **缩写** | data38 = \[\[美国各州缩写列表\#邮政代码||  }}

| rowclass40 = mergedtoprow | data40 = {{\#if: | <small>**注释：** </small>
}} }} }}<noinclude>  </noinclude>