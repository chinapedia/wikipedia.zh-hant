**黎府**（，）是[泰國人口密度最低的府之一](../Page/泰國.md "wikilink")，位於[東北部鄰近府](../Page/依善地區.md "wikilink")（從東起順時針）依序為[廊開府](../Page/廊開府.md "wikilink")、[烏泰他尼府](../Page/烏泰他尼府.md "wikilink")、[農磨蘭普府](../Page/農磨蘭普府.md "wikilink")、[孔敬府](../Page/孔敬府.md "wikilink")、[碧差汶府](../Page/碧差汶府.md "wikilink")、[彭世洛府](../Page/彭世洛府.md "wikilink")。北部和[寮國](../Page/寮國.md "wikilink")[沙耶武里省](../Page/沙耶武里省.md "wikilink")、[永珍交界](../Page/永珍.md "wikilink")。

## 地理

黎屬於丘陵地，是**黎府**府都，位於豐饒盆地內。[湄公河支流黎河貫通該府](../Page/湄公河.md "wikilink")，構成黎府北部與[寮國邊境](../Page/寮國.md "wikilink")。

四月左右的熱季，溫度可高達40度以上；在12月左右的寒季，這裡也是泰國唯一夜間溫度通常會降至零度以下的地方。

府內有數座山，包括著名的游览圣地[浦卡东山](../Page/浦卡东山.md "wikilink")（Phu Kradung），山里的一部分是。

## 歷史

1853年[孟固王](../Page/拉瑪四世.md "wikilink")（即拉瑪四世）在此建立黎城，以便於統馭日益增加的人口。1907年建府。

## 府徽

|  |                                                                                                                                                                                                                                                                  |
|  | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
|  | 府徽圖案是1506年建於Phra That Si Song Rak的*[佛塔](../Page/佛塔.md "wikilink")* (tower)。[阿瑜陀耶王朝的瑪哈恰克拉帕王建側該做佛塔](../Page/大城王朝.md "wikilink")，[澜沧国的](../Page/澜沧国.md "wikilink")[赛塔提拉王將之定為兩國國界](../Page/赛塔提拉.md "wikilink")。府樹是[思茅松](../Page/思茅松.md "wikilink") (*Pinus kesiya*)。 |

## 行政區

黎府有12個縣（*mphoe*）和2個次級縣（*King Amphoe*），又可分成89個區（*tambon*）和839村（*muban*）。

<table>
<tbody>
<tr class="odd">
<td><table>
<thead>
<tr class="header">
<th><p>#</p></th>
<th><p>中文名</p></th>
<th><p>泰文名</p></th>
<th><p>英文名</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td></td>
<td><p>เมืองเลย</p></td>
<td><p>Mueang Loei</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td></td>
<td><p>นาด้วง</p></td>
<td><p>Na Duang</p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td></td>
<td><p>เชียงคาน</p></td>
<td><p>Chiang Khan</p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td></td>
<td><p>ปากชม</p></td>
<td><p>Pak Chom</p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td></td>
<td><p>ด่านซ้าย</p></td>
<td><p>Dan Sai</p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td></td>
<td><p>นาแห้ว</p></td>
<td><p>Na Haeo</p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td></td>
<td><p>ภูเรือ</p></td>
<td><p>Phu Ruea</p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td></td>
<td><p>ท่าลี่</p></td>
<td><p>Tha Li</p></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td></td>
<td><p>วังสะพุง</p></td>
<td><p>Wang Saphung</p></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td></td>
<td><p>ภูกระดึง</p></td>
<td><p>Phu Kradueng</p></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td></td>
<td><p>ภูหลวง</p></td>
<td><p>Phu Luang</p></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td></td>
<td><p>ผาขาว</p></td>
<td><p>Pha Khao</p></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td></td>
<td><p>เอราวัณ</p></td>
<td><p>Erawan</p></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td></td>
<td><p>หนองหิน</p></td>
<td><p>Nong Hin</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table></td>
<td><center>
<p><a href="https://zh.wikipedia.org/wiki/File:Amphoe_Loei.svg" title="fig:Amphoe_Loei.svg">Amphoe_Loei.svg</a></p></td>
</tr>
</tbody>
</table>

## 外部連結

  - [黎府官方网址（泰文，包含旅游信息）](http://www.loei.go.th/)

<!-- end list -->

  - [黎府(Loei)地图及邮区分划图](http://www.thailex.info/THAILEX/THAILEXENG/LEXICON/Loei.htm)

[Category:依善地区](../Category/依善地区.md "wikilink")
[Category:泰國府份](../Category/泰國府份.md "wikilink")
[Category:黎府](../Category/黎府.md "wikilink")