[Selenga_delta.jpg](https://zh.wikipedia.org/wiki/File:Selenga_delta.jpg "fig:Selenga_delta.jpg")
[Selengerivermap.png](https://zh.wikipedia.org/wiki/File:Selengerivermap.png "fig:Selengerivermap.png")
**色楞格河**（，）是一條流經[蒙古國和](../Page/蒙古國.md "wikilink")[俄羅斯的](../Page/俄羅斯.md "wikilink")[河流](../Page/河流.md "wikilink")。古名**娑陵水**，由[伊德爾河和](../Page/伊德爾河.md "wikilink")[木倫河合成](../Page/木倫河.md "wikilink")，最後流入[貝加爾湖](../Page/貝加爾湖.md "wikilink")。全長992公里\[1\]，[流域面積](../Page/流域.md "wikilink")447,000平方公里。\[2\]是[葉尼塞河](../Page/葉尼塞河.md "wikilink")－[安加拉河的](../Page/安加拉河.md "wikilink")[源頭之一](../Page/源頭.md "wikilink")。支流有[乌第河](../Page/乌第河_\(布里亚特\).md "wikilink")、[楚庫河](../Page/楚庫河.md "wikilink")、[鄂爾渾河等](../Page/鄂爾渾河.md "wikilink")。

## 参考文献

{{-}}

[色楞格河](../Category/色楞格河.md "wikilink")
[Category:葉尼塞河](../Category/葉尼塞河.md "wikilink")
[Category:貝加爾湖](../Category/貝加爾湖.md "wikilink")
[Selenga](../Category/亞洲跨國河流.md "wikilink")
[Category:俄罗斯河流](../Category/俄罗斯河流.md "wikilink")
[Category:蒙古国河流](../Category/蒙古国河流.md "wikilink")
[Category:布里亞特共和國](../Category/布里亞特共和國.md "wikilink")

1.  H. Barthel, Mongolei-Land zwischen Taiga und Wüste, Gotha 1990,
    p.34f
2.