**馮紹波**（，），[香港經濟日報集團主席兼創辦人](../Page/香港經濟日報集團.md "wikilink")，香港[策略發展委員會委員](../Page/策略發展委員會.md "wikilink")，[香港集思會主席](../Page/香港集思會.md "wikilink")。

馮氏畢業於[東院道官立小學](../Page/東院道官立小學.md "wikilink")，後在中學時期就讀於[英皇書院](../Page/英皇書院.md "wikilink")，並於1968年至1972年期間就讀於[香港大學社會科學系](../Page/香港大學.md "wikilink")，期間曾於1971年當選為[香港大學學生會會長](../Page/香港大學學生會.md "wikilink")，其後負笈[英國](../Page/英國.md "wikilink")[曼徹斯特大學深造](../Page/曼徹斯特大學.md "wikilink")，於1977年獲經濟學碩士銜。1988年創辦《[香港經濟日報](../Page/香港經濟日報.md "wikilink")》。

## 榮譽

  - [金紫荊星章](../Page/金紫荊星章.md "wikilink") (2003年)

[Category:東院道官立小學校友](../Category/東院道官立小學校友.md "wikilink")
[Category:英皇書院校友](../Category/英皇書院校友.md "wikilink")
[Category:香港大學校友](../Category/香港大學校友.md "wikilink")
[Category:曼徹斯特大學校友](../Category/曼徹斯特大學校友.md "wikilink")
[Category:香港大學名譽博士](../Category/香港大學名譽博士.md "wikilink")
[Category:香港傳媒工作者](../Category/香港傳媒工作者.md "wikilink")
[Category:香港報界人士](../Category/香港報界人士.md "wikilink")
[S](../Category/馮姓.md "wikilink")