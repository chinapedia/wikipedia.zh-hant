**Dock**是[图形用户界面中用于启动](../Page/图形用户界面.md "wikilink")、切换运行中的[应用程序的一种功能界面](../Page/应用程序.md "wikilink")。Dock是[苹果公司](../Page/苹果公司.md "wikilink")[Mac
OS
X](../Page/Mac_OS_X.md "wikilink")[操作系统](../Page/操作系统.md "wikilink")，及其始祖[NeXTSTEP和](../Page/NeXTSTEP.md "wikilink")[OPENSTEP操作系统中重要组成部分](../Page/OPENSTEP.md "wikilink")。在[Newton
OS中也有dock概念的一些早期例子](../Page/Newton_OS.md "wikilink")。现在在不同操作系统中有很多不同的dock程序。

## 历史

在NeXTSTEP和OPENSTEP操作系统中，Dock主要功能是应用程序的启动器，用户可以在里面放置常用[程序的](../Page/程序.md "wikilink")[图标](../Page/图标.md "wikilink")，而系统的Workspace
Manager（工作区管理器）和Recycler（回收站）则是一直显示的。Dock通过图标下部的[省略号可以显示程序当前是否正在运行](../Page/省略号.md "wikilink")：通常状态下图标是带有省略号的，如果程序正在运行则省略号消失。（注意和Mac
OS X中dock的不同：MacOS X中通常状态下没有附加显示，而当运行时候会有三角形或指示灯显示）

## 苹果电脑和Mac OS X

尽管如此，在Mac OS
X中Dock可用来存放操作系统中任意的程序和文件，而且存放数目不受限制，可以动态更改大小，并在鼠标靠近时自动放大。在默认状态下它位于屏幕底部，但用户可以更改设置将其移到屏幕的左边或者右边。没有被加入dock的程序，在运行的时候，其图标会在dock中被显示出来，并在程序退出之后消失。

这些功能和NeXT操作系统中的dock不同，因为Dock容量受到屏幕[分辨率限制](../Page/分辨率.md "wikilink")。Dock的这些变化使其在功能上接近于[苹果公司的](../Page/苹果公司.md "wikilink")[Newton
OS](../Page/Newton_OS.md "wikilink") MessagePad 2x00系列的*Button
Bar*，应用程序可以从Extras
Drawer（一个类似于[Finder的程序](../Page/Finder.md "wikilink")）中拖进或拖出。而且和Mac
OS X一样，当屏幕进入横放状态时，用户可以可以选择将Button Bar改到左侧或者右侧。

### 图标

Mac OS X的Dock一般分成左右（上下）两个区域。

左（上）边用于放置程序图标，默认状态下Finder图标始终出现。用户可以将程序图标拖入dock即完成放置操作，将已有的图标拖到dock以外的桌面区域即删除图标。

右（下）边用于放置“废纸篓”，在默认状态下图标始终出现。用户还可以将任意的文件和文件夹放在里面以便快速访问。[Mac OS X
v10.5](../Page/Mac_OS_X_v10.5.md "wikilink") Leopard系统中追加了文件夹的“堆栈”等功能。

两个区域之间有分割线区别开。leopard系统界面中进化为为类似公路车道的图形。

### 菜单

Mac OS
X的Dock有拓展[菜单](../Page/菜单.md "wikilink")，可以不用将程序弹出dock就进行一些操作。绝大多数应用程序包含简单控制命令，如“退出”“在dock中保留”“从dock中去掉”等，而一些其他程序拥有更丰富的选项菜单，如[iTunes在dock中菜单可以让用户进行许多播放操作](../Page/iTunes.md "wikilink")。一些即时消息软件，如[MSN
messenger](../Page/MSN_messenger.md "wikilink")，[iChat的dock控制菜单中包含传输文件](../Page/iChat.md "wikilink")、更改在线状态等实用功能。

Dock的拓展控制菜单可以通过右键单击图标，或者control+左键，或者长按左键等多种方法调出。

## 其他操作系统

很多程序可以在[视窗系统中模拟Mac](../Page/视窗.md "wikilink") OS
X的dock，如[ObjectDock和](../Page/ObjectDock.md "wikilink")[RocketDock](../Page/RocketDock.md "wikilink")。早期的[Mac
OS](../Page/Mac_OS.md "wikilink")（版本10.0之前）并没有dock，但可以通过[A-dock等工具追加](../Page/A-dock.md "wikilink")。

在[Linux和](../Page/Linux.md "wikilink")[BSD系统中也有dock的运用](../Page/BSD.md "wikilink")，如[Window
Maker](../Page/Window_Maker.md "wikilink")（模拟NeXTstep系统界面）、[GNOME系统中的Gnome](../Page/GNOME.md "wikilink")
Dock、[Avant Window
Navigator以及](../Page/Avant_Window_Navigator.md "wikilink")[Cairo-Dock](../Page/Cairo-Dock.md "wikilink")，[KDE用的](../Page/KDE.md "wikilink")[KXDocker等](../Page/KXDocker.md "wikilink")。

在[Windows
Vista中引入了](../Page/Windows_Vista.md "wikilink")[Dockapp的一种形式](../Page/Dockapp.md "wikilink")：[Windows
Sidebar](../Page/Windows_Sidebar.md "wikilink")。[Windows
7任务栏则很类似老版本的Dock](../Page/Windows_7.md "wikilink")。

## 注释

## 参见

  - [任务栏](../Page/任务栏.md "wikilink")

[Category:NeXT](../Category/NeXT.md "wikilink")
[Category:GNUstep](../Category/GNUstep.md "wikilink")
[Category:图形用户界面](../Category/图形用户界面.md "wikilink")