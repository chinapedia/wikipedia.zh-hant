**持不同政见者**（），也称**异议人士**、**异议分子**、**异见人士**、**異己人士**、**反對派**，是指一些持有與[国家或](../Page/国家.md "wikilink")[政府的](../Page/政府.md "wikilink")[政策](../Page/政策.md "wikilink")、[法律不同的意見](../Page/法律.md "wikilink")，或質疑执政者的[合法性的人士](../Page/合法性.md "wikilink")。相对于[在野派或](../Page/在野黨.md "wikilink")[反对派](../Page/反对派.md "wikilink")，持不同政见者这一[术语通常指他们的意见為執政的](../Page/术语.md "wikilink")[政权所不能容忍](../Page/政权.md "wikilink")，或[主流](../Page/主流.md "wikilink")[观念不能容忍的](../Page/观念.md "wikilink")，在[自由](../Page/自由.md "wikilink")[法治国家](../Page/法治.md "wikilink")，主流社会不能容忍的不同观点，通常会通过法律诉讼的来解决。1974年，[水门事件上诉到](../Page/水门事件.md "wikilink")[美国联邦最高法院](../Page/美国联邦最高法院.md "wikilink")，宣判时，[美国最高法院大法官在判决的时侯曾说](../Page/美国最高法院大法官.md "wikilink")：“[国家安全并不是孤立的概念](../Page/国家安全.md "wikilink")，最大的安全就在于我们的自由的制度。当权者必须容忍媒体‘吵吵嚷嚷，一意孤行，碍手碍脚’，才能保护更高的价值，那就是[表达的自由和人民的](../Page/表达自由.md "wikilink")[知情权](../Page/知情权.md "wikilink")。”

在[专制国家](../Page/专制.md "wikilink")，**持不同政见者**很可能会受到人身威胁与伤害，[獨裁者会利用手中的執法机构对于](../Page/獨裁者.md "wikilink")[司法界的控制](../Page/司法機構.md "wikilink")，来制造司法腐败，制造司法[冤案来打壓异己](../Page/冤案.md "wikilink")，包括被硬性[劳改](../Page/劳改.md "wikilink")、恐嚇性收監、[軟禁等](../Page/軟禁.md "wikilink")，其實這類人有部分是爲了讓社會/國家更好的付出行動的[愛國者](../Page/愛國者.md "wikilink")，但多數情況下此類人為圖謀顛覆政權、製造國家混亂的犯罪者。在民主國家，也要小心意外、偷拍抹黑、干擾等隱蔽行為。

##

##

  - [黨外](../Page/黨外_\(臺灣\).md "wikilink")
  - [非主流派](../Page/非主流派_\(中國國民黨\).md "wikilink")
  - [第三勢力](../Page/第三勢力#中華民國（臺灣）.md "wikilink")

##

  - [穆罕默德·葉夫洛耶夫](../Page/穆罕默德·葉夫洛耶夫.md "wikilink")

##

  - [愛德華‧斯諾登](../Page/爱德华·斯诺登.md "wikilink")

## 参考文献

## 參見

  - [政治犯](../Page/政治犯.md "wikilink")
  - [良心犯](../Page/良心犯.md "wikilink")
  - [公共知识分子](../Page/公共知识分子.md "wikilink")
  - [脱逃者](../Page/脱逃者.md "wikilink")
  - [政治庇护](../Page/政治庇护.md "wikilink")
  - 「我不同意你的説法，但我誓死捍衛你説話的權利，在不歪曲事实的情况下。」（[伏爾泰](../Page/伏爾泰.md "wikilink")）

{{-}}

[D](../Category/特定人群称谓_\(政治术语\).md "wikilink")
[持不同政见者](../Category/持不同政见者.md "wikilink")