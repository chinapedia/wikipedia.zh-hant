[Cristian_Mungiu_Cannes_2012.jpg](https://zh.wikipedia.org/wiki/File:Cristian_Mungiu_Cannes_2012.jpg "fig:Cristian_Mungiu_Cannes_2012.jpg")

**克里斯蒂安·蒙久**（[罗马尼亚语](../Page/罗马尼亚语.md "wikilink")：****，），生于[雅西](../Page/雅西.md "wikilink")，[罗马尼亚电影制作者](../Page/罗马尼亚.md "wikilink")，2007年凭电影《[4个月3星期零2天](../Page/4个月3星期零2天.md "wikilink")》获得[戛纳电影节](../Page/戛纳电影节.md "wikilink")[金棕榈奖和](../Page/金棕榈奖.md "wikilink")[欧洲电影奖最佳影片](../Page/欧洲电影奖.md "wikilink")、最佳导演奖。

在[雅西大学修读](../Page/雅西大学.md "wikilink")[英语文学后](../Page/英语文学.md "wikilink")，蒙久曾任职教师和记者，之后入读[布加勒斯特的电影大学](../Page/布加勒斯特.md "wikilink")，1998年毕业。2002年，他完成首部长篇电影《[幸福在西方](../Page/幸福在西方.md "wikilink")》，获得一定成功；2007年他拍摄第二部长篇电影《[4个月3星期零2天](../Page/4个月3星期零2天.md "wikilink")》，获得影评人的普遍赞誉，并获得[2007年戛纳电影节](../Page/第60届戛纳国际电影节.md "wikilink")[金棕榈奖](../Page/金棕榈奖.md "wikilink")，是首部获得该奖项的罗马尼亚电影。2012年他又凭借《[山之外](../Page/山之外.md "wikilink")》夺得[第65届戛纳电影节最佳编剧奖](../Page/第65届戛纳电影节.md "wikilink")\[1\]。2016年作品《[毕业会考](../Page/毕业会考.md "wikilink")》获得[第69届戛纳电影节最佳导演奖](../Page/第69届戛纳电影节.md "wikilink")。

## 作品

| 年份    | 电影名称                                           |
| ----- | ---------------------------------------------- |
| 中文译名  | 原名                                             |
| 2016年 | 《[毕业会考](../Page/毕业会考.md "wikilink")》           |
| 2012年 | 《[山之外](../Page/山之外.md "wikilink")》             |
| 2007年 | 《[4个月3星期零2天](../Page/4个月3星期零2天.md "wikilink")》 |
| 2002年 | 《[幸福在西方](../Page/幸福在西方.md "wikilink")》         |

## 参考资料

<references/>

## 外部链接

  -
[Category:罗马尼亚电影导演](../Category/罗马尼亚电影导演.md "wikilink")
[Category:坎城影展獲獎者](../Category/坎城影展獲獎者.md "wikilink")
[Category:欧洲电影奖最佳导演获得者](../Category/欧洲电影奖最佳导演获得者.md "wikilink")

1.