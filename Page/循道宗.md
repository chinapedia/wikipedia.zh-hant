**循道宗**（），又稱**卫斯理宗**（）、**監理宗**，現代亦以**衛理宗**、**衛理公會**之名而著稱。是[基督教](../Page/基督教.md "wikilink")[新教主要宗派之一](../Page/新教.md "wikilink")，現傳佈於英國、美國、中國，和世界各地。世界最大的循道宗教會是[聯合循道會](../Page/聯合循道會.md "wikilink")。

## 历史

1738年由[英国人](../Page/英国人.md "wikilink")[约翰·卫斯理](../Page/约翰·卫斯理.md "wikilink")（1703年－1791年）和其弟[查理·卫斯理于](../Page/查理·卫斯理.md "wikilink")[伦敦创立](../Page/伦敦.md "wikilink")，即[英國循道會](../Page/英國循道會.md "wikilink")。原为[圣公会内的一派](../Page/圣公会.md "wikilink")，后逐渐独立。[美國獨立之後](../Page/美國獨立.md "wikilink")，美國衛斯理宗脫離[聖公會而組成](../Page/聖公會.md "wikilink")[宗派](../Page/宗派.md "wikilink")，其後教會分裂為[美以美會](../Page/美以美會.md "wikilink")、[監理會](../Page/監理會.md "wikilink")、[美普會](../Page/美普會.md "wikilink")、[循理會和](../Page/循理會.md "wikilink")[聖教會等](../Page/聖教會.md "wikilink")。1939年，美以美會、監理會和美普會合併成現今的衛理公會。该宗认为传统[教会的活动方式已不足以应付新的](../Page/教会.md "wikilink")[社会问题](../Page/社会问题.md "wikilink")，主张着重在下层群众中进行传教活动，宣称求得“内心的平安喜乐”便是幸福。主要分布于英美等国。

當時英國國教[聖公會偏向向](../Page/聖公會.md "wikilink")[上流社會傳](../Page/上流社會.md "wikilink")[福音](../Page/福音.md "wikilink")，而[長老會跟](../Page/長老會.md "wikilink")[浸信會向](../Page/浸信會.md "wikilink")[中產階級與商人](../Page/中產階級.md "wikilink")[傳教](../Page/傳教.md "wikilink")，而約翰·衛斯理，就專門向窮苦的[工人階級的人傳福音](../Page/工人階級.md "wikilink")。衛斯理兄弟以感情豐富的方式講道，但其實在當時的教會文化中並不受歡迎，特別對當時講求冷靜的英國國教聖公會來說，是很大的極端，因此常被趕出教會外，不允許他們以這種熱情的方式在會堂裡講道。於是他們只好著力在一些外面的基督徒小組（[團契](../Page/團契.md "wikilink")）裡講道，甚至舉辦露天的大佈道會。他們最著名的露天講道是發生在金斯伍德（Kingswood）的曠野，向眾多礦工傳福音，宣称「信靠耶穌」來獲得「內心的平安喜樂」便是幸福。許多低下階層的人深受他的講道所感動，紛紛歸信基督教。

著名之全球性教會與慈善團體一體的組織[救世軍](../Page/救世軍.md "wikilink")，乃是衛理公會旗下牧師[卜威廉所創](../Page/卜威廉.md "wikilink")，但為獨立於衛理公會的教會組織。

## 音樂方面事工

對於循理宗音樂方面做出最大貢獻的非約翰·衛斯理和查爾斯·衛斯理莫屬了。他們不僅給循理會的讚美詩帶來巨大貢獻，他們的音樂創作更是使得十八世紀其他讚美詩作家黯然失色。\[1\]他們創作之初，主要是以消除信徒間宗派的局限性為目的。1735年，約翰·衛斯理和他的兄弟
查爾斯·衛斯理他們在去喬治亞州旅行的路程中遇到了摩拉維亞教徒，他們讚美的形式給衛斯理弟兄留下了非常深刻的印象，之後約翰·衛斯理也到了摩拉維亞信徒中聚會，約翰
衛斯理非常喜愛他們的音樂，所以翻譯了很多讚美詩。而他的兄弟查爾斯
衛斯理在音樂方面更是有不凡的恩賜。他曾經說：“帶有個人體驗的歌曲，標記了在基督教朝聖者的一生中，懺悔、皈依、釋罪、寬恕、淨化的連續過程……”\[2\]

早在1737年有約翰·衛斯理在美國出版了第一本聖詩，\[3\]之後，循道宗於1742年發行了第一本聖歌集，但當時他們的音樂都為單旋律，沒有和聲。“十八世紀至十九世紀初，循理會的音樂似乎和英格蘭立甘宗及蘇格蘭長老會的音樂一樣，具有裝飾音修飾歌詠的習慣，其中有些內容顯得十分華麗，有些是採用賦格形式寫的多聲部作品。歌詞的最後部分常有男聲、女聲及混聲的反復。有些樂曲只有兩個聲部，并帶有樂器的低音；其他樂曲在各節之後有合唱或哈利路亞的疊句。”\[4\]

在十七世紀到十八世紀這段期間，循道宗還因著在崇拜是否要使用管風琴而展開一場討論。最終結果，允許在較大的教堂的崇拜中使用管風琴，來引導會眾敬拜上帝。而那些不同意使用管風琴的人最終從循道會分離出去。而今天，在循道宗的教堂中不論是管風琴還是其他鍵盤樂器都是允許使用的了。\[5\]

十九世紀末，教會崇拜對於管風琴和聖歌隊的過度依賴，形成了聖歌隊和佈道者相爭的局面。在這之後，循道宗的聖樂逐漸追求外在技巧，不再那麼注重內在的經歷。“在以後二十世紀的1933年聯合教團刊行了新的聖歌集，1934年又成立了相應的協會以促進該教派教會音樂的發展。此前該教派發行了有關教會音樂的特別雜誌《The
Choir》。”\[6\]

## 各地循道宗

###

**卫理公会**（The Methodist
Church），是**衛理宗**的[美以美會](../Page/美以美會.md "wikilink")、[监理会和](../Page/监理会.md "wikilink")[美普會合併而成的](../Page/美普會.md "wikilink")[基督新教](../Page/基督新教.md "wikilink")[教會](../Page/教會.md "wikilink")。現傳佈於英國、美國、中國，和世界各地。

  - 来自[美国北方的称为](../Page/美国.md "wikilink")[美以美会](../Page/美以美会.md "wikilink")，来自美国南方的称为[监理会](../Page/监理会.md "wikilink")，1939年连同[美普会](../Page/美普会.md "wikilink")，三会合一，合并成为[卫理公会](../Page/卫理公会.md "wikilink")。1947年在衛理公會來華宣教百週年時，全國已成立了九個正式年議會：福州、興化（福建莆田）、延平（福建南平）、江西、山東、華東（原监理会）、華中、華北及華西年議會。另外还有张家口临时年议会（原美普会）。总部设在[福州天安堂](../Page/福州天安堂.md "wikilink")。卫理公会在[福建省约有](../Page/福建省.md "wikilink")10万信徒。详见“[美以美会](../Page/美以美会.md "wikilink")”条目。

<!-- end list -->

  - 来自[英国的包括](../Page/英国.md "wikilink")[聖道公會](../Page/聖道公會.md "wikilink")（United
    Methodist Church
    Mission，UMC）、[大英循道會](../Page/大英循道會.md "wikilink")（Wesleyan
    Methodist Missionary
    Society，WMMS）等许多分支，后来统称为[循道公会](../Page/循道公会.md "wikilink")，包括[温州](../Page/温州.md "wikilink")、[宁波](../Page/宁波.md "wikilink")、西南（[重庆](../Page/重庆主城区.md "wikilink")、[云南](../Page/云南.md "wikilink")[昭通和](../Page/昭通.md "wikilink")[贵州](../Page/贵州.md "wikilink")[威宁石门坎一带](../Page/威宁.md "wikilink")）、[华北](../Page/华北.md "wikilink")（[山东乐陵](../Page/山东.md "wikilink")、惠民和[天津一带](../Page/天津.md "wikilink")）、[湖北](../Page/湖北.md "wikilink")、[湖南](../Page/湖南.md "wikilink")、[华南](../Page/华南.md "wikilink")（两广）七个教区，总部设在[汉口](../Page/汉口.md "wikilink")。

<!-- end list -->

  - 来自于[加拿大的称为](../Page/加拿大.md "wikilink")[英美会](../Page/英美会.md "wikilink")，后来改名[美道会](../Page/美道会.md "wikilink")。分布在[四川](../Page/四川.md "wikilink")。

[File:Wuhan_-_former_Methodist_School_-_P1050044.JPG|为英国基督教循道公会1885年创建的学校(武汉市第十五中学)](File:Wuhan_-_former_Methodist_School_-_P1050044.JPG%7C为英国基督教循道公会1885年创建的学校\(武汉市第十五中学\))
<File:Wuhan> - former Methodist church, now Crown Bakery - interior -
P1050063.JPG|武汉的中华循道公会弘道会旧址

###

  - 1975年，在[香港的](../Page/香港.md "wikilink")[循道公会与](../Page/循道公会.md "wikilink")[卫理公会合并成为](../Page/卫理公会.md "wikilink")[循道衛理聯合教會](../Page/香港基督教循道卫理联合教会.md "wikilink")。

###

戰前，日本美以美會來[台灣傳教](../Page/台灣.md "wikilink")，分別在[台北和](../Page/台北.md "wikilink")[台南建立昭和町教會和壽町教會](../Page/台南.md "wikilink")。目前台灣的衛理宗主要是在1949年國民黨撤退來台後由原在中國的衛理公會傳入，1953年在台北正式建立台北衛理堂，1963年招開首屆臨時年議會，建立南北兩教區。1972年台灣教會自立，定名中華基督教衛理公會。

<File:Taipei> Wesley Methodist Church Birdview.jpg|台北衛理堂 <File:Taipei>
Wesley Methodist Church 20101002.jpg|台北衛理堂遠景

## 参考文献

## 外部連結

  - 一般资料

<!-- end list -->

  - [The Largest United Methodist
    Communities](http://adherents.com/largecom/com_umc.html)
  - [Adventure Education
    永康衛理堂撒該牧師體驗教育](https://web.archive.org/web/20110808152831/http://tw.myblog.yahoo.com/adventure-education)
  - [NPMPS 北角衛理小學](http://npmps.edu.hk/)

<!-- end list -->

  - 文獻

<!-- end list -->

  - [China Free Methodist Church
    基督教中華循理會](http://lib-nt2.hkbu.edu.hk/sca_fb/org/btw01_cfmc.html)
    香港浸會大學圖書館 華人基督宗教文獻保存計劃

<!-- end list -->

  - 循道宗各诸团体官方网页

<!-- end list -->

  - [中華基督教衛理公會](http://www.methodist.org.tw/)
  - [中華基督教衛理公會台灣台南永康衛理堂](https://web.archive.org/web/20060826154347/http://www.churches.org.tw/yungkang.methodist/findex.asp)
  - [循道衛理聯合教會的歷史](http://www.methodist.org.hk/about/)
  - [衛理公會歷史](https://web.archive.org/web/20051223161459/http://mgst.org.tw/intro/intro-methodist.htm)
  - [衛理神學研究院](http://www.mgst.org.tw/)
  - [衛道神學研究院](https://web.archive.org/web/20070928091507/http://uwgi.church.org.hk/index.htm)

## 参见

  - [约翰·卫斯理](../Page/约翰·卫斯理.md "wikilink")
  - [成圣](../Page/成圣.md "wikilink")
  - [圣洁运动](../Page/圣洁运动.md "wikilink")
  - [阿民念主义](../Page/阿民念主义.md "wikilink")

{{-}}

[循道宗](../Category/循道宗.md "wikilink")
[Category:阿民念主义](../Category/阿民念主义.md "wikilink")
[Category:创立于英国的基督教新教教派](../Category/创立于英国的基督教新教教派.md "wikilink")

1.  陶理博士，《基督教兩千年史》，李伯明、李牧野譯（香港：海天書樓，2012），459。
2.  安德魯·威爾遜-迪克森，《基督教音樂之旅》，畢祎、戴丹譯（上海：上海人民美術出版社，2002），113-114。
3.  麥愛鄰，《教會音樂史》，林惠慧譯（台灣：人光出版社，1993），145。
4.  陳小魯，《基督宗教音樂史》，（中國：宗教文化出版社，2010），401。
5.  陳小魯，《基督宗教音樂史》，402。
6.  陳小魯，《基督宗教音樂史》，402。