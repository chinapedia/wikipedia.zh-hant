**樟树**（[学名](../Page/学名.md "wikilink")：**）是[樟科常绿大](../Page/樟科.md "wikilink")[乔木](../Page/乔木.md "wikilink")，别名**香樟**、**本樟**、**鸟樟**、**栳樟**、**樟仔**。

## 形态

常綠[喬木](../Page/喬木.md "wikilink")，树高可达40米；\[1\]散发樟树的特有清香气息。树皮暗褐色，有纵裂溝紋。卵形或椭圆状卵形的单叶互生，薄革质，全缘，表面光滑，背面微有白粉，无毛，叶缘微呈波状，有离基三出脉，脉腋有明显腺体。雌雄同花，花两性，圆锥花序腋生于枝顶端，黄绿色小花，花期4～5月。球形[核果](../Page/核果.md "wikilink")，10～11月成熟，成熟时由绿色转为黑紫色；果皮呈紫黑色，有光泽。

## 分布

原產於[越南](../Page/越南.md "wikilink")、[中国長江以南](../Page/中国.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、[韓國](../Page/韓國.md "wikilink")、[日本](../Page/日本.md "wikilink")，並且已被引入許多其它國家。见于湿润的山谷、山腰下、河流两岸、路旁等。\[2\]

分布於東亞至澳洲、南太平洋。台灣原產14種，分布於中低海拔闊葉林，是台灣中低海拔主要樹種之一，其最適海拔為500\~1800公尺的[暖溫帶氣候區](../Page/暖溫帶氣候區.md "wikilink")。

## 用途

  - 將樟樹（在台灣常分本樟、芳樟、油樟、陰陽樟等）樹幹削成薄片，在腦寮中蒸餾出[樟脑及樟腦油](../Page/樟脑.md "wikilink")，初製品再經分餾等程序可製其他產品，主要供賽璐珞等工業使用。
  - 为常見的[行道樹](../Page/行道樹.md "wikilink")：枝葉濃密、樹形美觀，可作行道樹及防風林。
  - 因木質芳香，抗蟲害、耐水濕，可作建材、造船、家具、雕刻用。
  - 提煉樟腦、樟腦油：根、木材、枝、葉均可提煉樟腦、樟腦油；樟腦供醫藥、塑料、炸藥、防腐、殺蟲等用，樟腦油可作農藥、肥皂、假漆及香精等原料。
  - 藥用：效用：根、材：袪風散寒，溫中健胃，止癢止痛。根、幹、枝、葉：通竅，殺蟲，止痛，提製樟腦。治心腹賬痛，牙痛，跌打，疥癬。

[File:A061.jpg|提煉樟腦](File:A061.jpg%7C提煉樟腦) <File:Refined> Camphor 5 3
2011 009.jpg|樟腦

## 毒性

  - [蠶豆症](../Page/蠶豆症.md "wikilink")（[G6PD缺乏症](../Page/G6PD缺乏症.md "wikilink")）患者不宜接觸樟腦之衍生製品[萘丸及](../Page/萘丸.md "wikilink")[水晶腦](../Page/对二氯苯.md "wikilink")，會產生溶血反應\[3\]。\[4\]\[5\]

## 其他

樟树是[中國](../Page/中华人民共和国.md "wikilink")[浙江省](../Page/浙江省.md "wikilink")[杭州市](../Page/杭州市.md "wikilink")、[宁波市](../Page/宁波市.md "wikilink")、[台州市](../Page/台州市.md "wikilink")、[湖南省](../Page/湖南省.md "wikilink")[长沙市](../Page/长沙市.md "wikilink")、[常德市](../Page/常德市.md "wikilink")、[江苏省](../Page/江苏省.md "wikilink")[苏州市和](../Page/苏州市.md "wikilink")[无锡市](../Page/无锡市.md "wikilink")、[江西省](../Page/江西省.md "wikilink")[南昌市和](../Page/南昌市.md "wikilink")[九江市](../Page/九江市.md "wikilink")、[四川省](../Page/四川省.md "wikilink")[绵阳市](../Page/绵阳市.md "wikilink")、[臺灣](../Page/臺灣.md "wikilink")[苗栗縣](../Page/苗栗縣.md "wikilink")、[雲林縣](../Page/雲林縣.md "wikilink")、[南投縣](../Page/南投縣.md "wikilink")、[臺南縣及](../Page/臺南縣.md "wikilink")[日本](../Page/日本.md "wikilink")[下关市的市樹或縣樹](../Page/下关市.md "wikilink")。

## 图片

<File:Kamferboom_B.jpg>
<File:Tsuboi-Hachimangu_Kusunoki(Osaka_midorino_hyakusen).jpg>
<File:Cinnamomum_camphora7.jpg>
[File:Cinnamomum_camphora6.jpg|花](File:Cinnamomum_camphora6.jpg%7C花)
[File:Cinnamomum_camphora5.jpg|幼葉](File:Cinnamomum_camphora5.jpg%7C幼葉)
[File:Cinnamomum_camphora4.jpg|漿果](File:Cinnamomum_camphora4.jpg%7C漿果)
[File:Cinamommum_camphora_fruit.JPG|漿果](File:Cinamommum_camphora_fruit.JPG%7C漿果)
[File:Seeds_of_Cinnamomum_camphora.JPG|漿果](File:Seeds_of_Cinnamomum_camphora.JPG%7C漿果)

## 註釋

## 外部链接

  -
  - \[<http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D00867樟>,
    Zhang\]藥用植物圖像數據庫（香港浸會大學中醫藥學院）

[Category:樟属](../Category/樟属.md "wikilink")
[樹](../Category/藥用植物.md "wikilink")
[Category:行道樹](../Category/行道樹.md "wikilink")

1.
2.

3.

4.

5.