[1964-09_1964年_广东鼎湖瀑布.jpg](https://zh.wikipedia.org/wiki/File:1964-09_1964年_广东鼎湖瀑布.jpg "fig:1964-09_1964年_广东鼎湖瀑布.jpg")
**鼎湖山**位于[廣東](../Page/廣東.md "wikilink")[肇庆市区东北](../Page/肇庆.md "wikilink")18公里，穿过[北回归线](../Page/北回归线.md "wikilink")。面积1133公顷，最高1000.3米（鸡笼顶），岭南四大名山之首。原名顶湖，得名于山顶有一个常盈的湖。有传说黄帝打败蚩尤，在此铸鼎，故称鼎湖。

从山麓到山顶，自下而上分布着沟谷雨林、常绿阔林、亚热带季风常绿阔叶林等多种森林类型。被誉为华南生物种类的“[基因储存库](../Page/基因.md "wikilink")”，“绿色宝库”和“活的自然博物馆”，又因北回归线穿过的地方大都是[沙漠](../Page/沙漠.md "wikilink")，而鼎湖山因其终年常绿，又被誉为“北回归线上的绿宝石”。

## 旅游

[Dinghushan_dingyuan.JPG](https://zh.wikipedia.org/wiki/File:Dinghushan_dingyuan.JPG "fig:Dinghushan_dingyuan.JPG")
鼎湖山一直是著名的[佛教圣地和旅游胜地](../Page/佛教圣地.md "wikilink")。山上有[岭南四大名刹之一的](../Page/岭南.md "wikilink")[庆云寺](../Page/庆云寺_\(肇庆\).md "wikilink")，还有白云寺、跃龙庵等。鼎湖山林木茂密，[瀑布众多](../Page/瀑布.md "wikilink")，是一个避暑胜地。人称鼎湖集六大名山之特色，有[王屋之奇](../Page/王屋山.md "wikilink")，[青城之秀](../Page/青城山.md "wikilink")，[武夷之清](../Page/武夷山.md "wikilink")，[西樵之逸](../Page/西樵山.md "wikilink")，[丹霞之媚](../Page/丹霞山.md "wikilink")，[委羽之幽](../Page/委羽山.md "wikilink")。

鼎湖山分为三大景区：天溪--庆云景区；鼎湖--天湖景区；云溪--老鼎景区。鼎湖山景区内还可乘坐电屏车观光，包来回，现票价为20元人民币/人。

1982年，鼎湖山与[七星岩组成的星湖风景名胜区](../Page/七星岩.md "wikilink")，成为国家首批44个[国家重点风景名胜区之一](../Page/国家重点风景名胜区.md "wikilink")

## 科研

[Qingyunshi.JPG](https://zh.wikipedia.org/wiki/File:Qingyunshi.JPG "fig:Qingyunshi.JPG")

  - 1956年，成为中国第一个自然保护区；
  - 1979年，成为中国第一批加入[联合国教科文组织](../Page/联合国教科文组织.md "wikilink")“人与生物圈”计划的保护区，列为国际第17号生物圈保护区；
  - 1990年，纳入“中国生态系统研究网络”。

## 植被

鼎湖山主要的植被可划分自然、半自然和人工植被三大类型。

自然植被有：

  - 海拔30-400米：季风常绿阔叶林；
  - 海拔500-800米：山地常绿阔叶林；
  - 海拔500-900米：山地灌木草丛；
  - 海拔30-250米：沟谷雨林；
  - 海拔30米以下：河岸林。

半自然植被有：

  - 海拔100-450米：次生季风常绿阔叶林和针阔叶混交林；
  - 海拔300米以下：丘陵的针叶林；
  - 海拔500-600米：山坡上的常绿灌丛；

人工植被有：

  - [大叶桉林](../Page/大叶桉.md "wikilink")、
  - [竹林](../Page/竹.md "wikilink")
  - [广东](../Page/广东.md "wikilink")[油茶林](../Page/油茶.md "wikilink")。

## 全景图

[Panoramic_Dinghu.jpg](https://zh.wikipedia.org/wiki/File:Panoramic_Dinghu.jpg "fig:Panoramic_Dinghu.jpg")的鼎湖全景图\]\]

## 外部链接

[D](../Category/肇庆地理.md "wikilink") [D](../Category/广东山脉.md "wikilink")