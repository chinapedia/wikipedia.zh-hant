**牙萼狸藻**（[學名](../Page/學名.md "wikilink")：***Utricularia
odontosepala***）為[狸藻屬](../Page/狸藻屬.md "wikilink")[一年生小型至中型的](../Page/一年生植物.md "wikilink")[食虫植物](../Page/食虫植物.md "wikilink")。其[种加词](../Page/种加词.md "wikilink")“*odontosepala*”来源于[希腊文](../Page/希腊文.md "wikilink")“*odous*”和[英文](../Page/英文.md "wikilink")“sepal”，意为“牙齿”和“萼片”，指其齿状萼片。牙萼狸藻原產於[非洲熱帶](../Page/非洲.md "wikilink")，存在于[剛果民主共和國](../Page/剛果民主共和國.md "wikilink")、[馬拉威以及](../Page/馬拉威.md "wikilink")[尚比亞](../Page/尚比亞.md "wikilink")。牙萼狸藻陆生于潮濕[泥炭质的草原中](../Page/泥炭.md "wikilink")，海拔分布范围为1300米至2200米。花期为四月至九月。1912年，[奥托·施塔普夫最先发表了牙萼狸藻的描述](../Page/奥托·施塔普夫.md "wikilink")。1964年，[彼得·泰勒将其处理为](../Page/彼得·泰勒_\(植物學家\).md "wikilink")[威爾維茨狸藻](../Page/威爾維茨狸藻.md "wikilink")的[變種](../Page/變種.md "wikilink")。但之后他又推翻了自己的处理，恢复了牙萼狸藻的物种地位。\[1\]

## 参考文献

## 外部連結

[Utricularia odontosepala](../Category/非洲食虫植物.md "wikilink")
[Category:刚果民主共和国植物](../Category/刚果民主共和国植物.md "wikilink")
[Category:马拉维植物](../Category/马拉维植物.md "wikilink")
[Category:赞比亚植物](../Category/赞比亚植物.md "wikilink")
[odontosepala](../Category/狸藻屬.md "wikilink")
[odontosepala](../Category/瓮盘狸藻组.md "wikilink")

1.  Taylor, Peter. (1989). *[The genus Utricularia - a taxonomic
    monograph](../Page/狸藻属——分类学专著.md "wikilink")*. Kew Bulletin
    Additional Series XIV: London.