**五逆罪**是[佛教所有惡](../Page/佛教.md "wikilink")[業中最重者](../Page/業.md "wikilink")；「逆」者，「罪大惡極，極逆於常理」之意。造作五逆罪為墮[無間地獄](../Page/無間地獄.md "wikilink")（亦稱[阿鼻地獄](../Page/阿鼻地獄.md "wikilink")）的業因，命終就是死亡時將墮落到無間地獄，故亦稱**五無間業**或**五不救罪**。
無間地獄，顧名思義，受苦「無有間斷」，所以叫做無間。

五逆和[十惡業常合稱](../Page/十惡業.md "wikilink")**五逆十惡**。

## 部派佛教五逆

1.  殺掉母親
2.  殺掉父親：父母生身，弒父母大違孝順雙親、老師、尊長、大人、諸佛的教導。
3.  殺掉[阿羅漢](../Page/阿羅漢.md "wikilink")
4.  破和合僧：破壞修行眾之间的和諧。意思就是破壞修[六和敬](../Page/六和敬.md "wikilink")（見[僧伽](../Page/僧伽.md "wikilink")）的修行眾(不分出家在家，皆為「僧團」)的和諧無諍。
5.  出佛身血：就是驅使故意使佛的身體流血，包括毁佛像。如[提婆達多欲以大石殺害](../Page/提婆達多.md "wikilink")[釋迦牟尼佛](../Page/釋迦牟尼佛.md "wikilink")，以代之領導僧團的歷史事件。因為前世時釋迦牟尼和提婆達多互為兄弟，提婆為了爭遺產把佛推下懸崖，再以大石擊碎，所以今世有如此果報。

## 大乘佛教五逆\[1\]

  - 犯**小乘五逆**罪之一。
  - 盜毀[常住](../Page/常住.md "wikilink")：自為或教唆他人破壞盜取塔寺、經像等[三寶之物](../Page/三寶_\(佛教\).md "wikilink")。
  - 誹謗大乘：誹謗[聲聞](../Page/聲聞.md "wikilink")、[緣覺以及大乘法](../Page/緣覺.md "wikilink")(謗法)。
  - 殺害[出家人或妨礙出家人](../Page/出家人.md "wikilink")[修行](../Page/修行.md "wikilink")：如[歌利王](../Page/歌利王.md "wikilink")[凌遲](../Page/凌遲.md "wikilink")[忍辱仙人的](../Page/忍辱仙人.md "wikilink")[本生譚](../Page/本生譚.md "wikilink")。
  - 不信[因果](../Page/因果.md "wikilink")：主張所有心行皆無[業報](../Page/業報.md "wikilink")，或不畏[果報](../Page/果報.md "wikilink")，自行或教唆他人行[十惡之事](../Page/十惡.md "wikilink")。
  - （以上白话文的举例来源不明，佛经原文无此举例）

### 五逆罪與往生

《[無量壽經](../Page/無量壽經.md "wikilink")》[阿彌陀佛四十八願之本願](../Page/阿彌陀佛.md "wikilink")——第十八願「十念必生」末後有一句「唯除五逆，[誹謗正法](../Page/誹謗.md "wikilink")」，乍看犯五逆罪者不得[往生](../Page/往生.md "wikilink")[西方極樂世界](../Page/西方極樂世界.md "wikilink")；但在《[觀無量壽佛經](../Page/觀無量壽佛經.md "wikilink")》下品下生章裡面可以見到：即使造五逆罪也能往生，僅誹謗正法不得往生。另外[阿闍世王弒父害母](../Page/阿闍世王.md "wikilink")、勾結欲殺害[釋迦牟尼佛的](../Page/釋迦牟尼佛.md "wikilink")[提婆達多破壞僧團](../Page/提婆達多.md "wikilink")，他在臨壽終前懺悔，念佛亦得往生，並且為[上品中生](../Page/上品中生.md "wikilink")，亦是一證。

依[淨空法師的解釋](../Page/淨空法師.md "wikilink")\[2\]：「不能往生的是『誹謗正法』，因為他不相信、不能接受、不願往生，這才是真正的障礙。」

## 相關典籍

  - 《[地藏經](../Page/地藏經.md "wikilink")》
  - 《[阿闍世王問五逆經](../Page/阿闍世王問五逆經.md "wikilink")》
  - 《[佛說滅除五逆罪大陀羅尼經](../Page/佛說滅除五逆罪大陀羅尼經.md "wikilink")》
  - 《[無量壽經](../Page/無量壽經.md "wikilink")》
  - 《[觀無量壽佛經](../Page/觀無量壽佛經.md "wikilink")》

## 參考資料

[category:佛教名數5](../Page/category:佛教名數5.md "wikilink")

[Category:佛教戒律](../Category/佛教戒律.md "wikilink")

1.   CBETA
    漢文大藏經|url=[http://tripitaka.cbeta.org/T09n0272_004|work=tripitaka.cbeta.org|accessdate=2019-02-08](http://tripitaka.cbeta.org/T09n0272_004%7Cwork=tripitaka.cbeta.org%7Caccessdate=2019-02-08)}}
2.  [淨宗朝暮課誦經文講記（第四卷）](http://book.bfnn.org/books/0953.htm)