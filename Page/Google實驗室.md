[Labs_logo2.gif](https://zh.wikipedia.org/wiki/File:Labs_logo2.gif "fig:Labs_logo2.gif")
**Google實驗室**（**Google
Labs**）是一個用來演示[Google還在測試階段的新項目產品的網站](../Page/Google.md "wikilink")。它也作為一個正開發中新服務的試驗場。另外，也是Google在發出產品最後版本前收集用戶意見的途徑。但不是所有Google服務都會透過這實驗室來測試，有些測試版是透過邀請用家來測試的，例如[Gmail與](../Page/Gmail.md "wikilink")[Google日曆](../Page/Google日曆.md "wikilink")。
2006年起，所有Google實驗室產品都有一個一致的標誌，就是標誌裡附有一個[燒瓶](../Page/燒瓶.md "wikilink")，和使用灰色標題，這也正相反於其他有顏色的Google產品，例如[Google新聞和](../Page/Google新聞.md "wikilink")[Google地圖](../Page/Google地圖.md "wikilink")。這也用來提醒用戶這些產品可能還有問題，或可能不適合一般使用。
2011年7月，[Google宣布中止Google实验室](../Page/Google.md "wikilink")\[1\]，大部分实验项目被中止，少数项目移动到了搜索主页。

## 現時產品／服務

  - Google Accessible Search : Google Accessible
    Search是一個針對視障人士的搜索引擎。它的做法是先分析網頁內容，並去除那些包含大量圖像的網站，之後再按照視障人士用戶的特殊設定排列搜尋結果，例如興趣或地點等。
    [Blogger](../Page/Blogger.md "wikilink") Web Comments : Blogger Web
    Comments是[Mozilla
    Firefox瀏覽器的](../Page/Mozilla_Firefox.md "wikilink")[擴充套件](../Page/Firefox扩展列表.md "wikilink")，它可以顯示來自Blogger網誌的留言，並使用戶能直接發新網誌到Blogger。
    Google Browser Sync : Google Browser
    Sync是Firefox的擴充套件。它能透過Google帳戶的控制，來同步處理多部電腦的書籤，[cookies](../Page/cookie.md "wikilink")
    和瀏覽器設定等。
    Google Send to Phone : Google Send to
    Phone是Firefox的一個擴充套件，它使用戶能傳送文字訊息包括網頁內容到他們的流動電話。這個服務現在只能在美國使用。
    [Google Code Search](../Page/Google_Code_Search.md "wikilink") :
    Google Code Search是網上程式編碼的搜索引擎。
    Google [Dashboard](../Page/Dashboard.md "wikilink") Widgets for
    [Mac](../Page/Mac_OS.md "wikilink") : Google Macintosh Dashboard
    Widgets
    是小型應用程式。器件如[Blogger](../Page/Blogger.md "wikilink")，[Gmail和搜索紀錄都可用](../Page/Gmail.md "wikilink")。
    [Froogle](../Page/Froogle.md "wikilink") Mobile : Froogle
    Mobile是Google Froogle版本的價格引擎，適用於流動裝置。它能容許用戶去搜索他們想要的數以百間商店的產品。
    [Google Mars](../Page/Google_Maps#Google_Mars.md "wikilink") :
    Google
    Mars是一個網頁應用程式，它能顯示來自[火星的](../Page/火星.md "wikilink")[衛星和](../Page/衛星.md "wikilink")[紅外線圖像](../Page/紅外線.md "wikilink")。
    [Google Music
    Trends](../Page/Google_Trends#Google_Music_Trends.md "wikilink") :
    Google Music Trendsis是一個選用的服務，它統計Google
    Talk用戶最常收聽的音樂，並顯示每週最高收聽的歌曲，它也能以音樂類型以及國家來分類歌曲。
    [Google Page Creator](../Page/Google_Page_Creator.md "wikilink") :
    Google Page
    Creator是一個[所見即所得的編輯器](../Page/所見即所得.md "wikilink")，容許用戶使用Google帳戶製作簡單的網頁。特色包括100MB的儲存空間，和多款可選的網頁設計模版。
    [Picasa](../Page/Picasa.md "wikilink") for
    [Linux](../Page/Linux.md "wikilink") : Picasa for
    Linux是相片組織和編輯管理程式，運作和Windows版的[Picasa相似](../Page/Picasa.md "wikilink")。為了使它能在[Linux下運作](../Page/Linux.md "wikilink")，Google使用[Wine](../Page/Wine.md "wikilink")，一個轉譯並執行[Windows
    API的軟體](../Page/Windows_API.md "wikilink")。
    [Google Reader](../Page/Google_Reader.md "wikilink") : Google Reader
    是一個web-based形式的新聞聚合器，它能夠閱讀Atom
    和RSS格式的文章，並容許用戶搜索，載入和訂閱自訂的網站內容，此外也能把音頻嵌入頁內。Google
    Reader的主要修改版建於2006年10月。
    Google Related Links : Google Related
    Links是把網站、搜索、新聞、影像等放在網頁的單一表單框架裡。表單的大小，內容種類和顏色都可以由用戶自訂。
    [Google Ride
    Finder](../Page/Google_Maps#Google_Ride_Finder.md "wikilink") :
    Google Ride
    Finder是一項為用戶找計程車，小型客車或穿梭汽車的服務，它提供了美國十四個城市這些汽車的即時地理位置。Ride
    Finder透過使用Google Maps的介面，並與參加這服務的汽車服務公司合作來運作。
    Google Sets : Google
    Sets會在用戶輸入一些字串後顯示一些相關的字串的列表。例如，當用戶輸入："Green、Purple、Red"，它就會列出"Green、Purple、Red、Blue、Black、White、Yellow、Orange、Brown"。
    Google Suggest : Google
    Suggest是當你輸入關鍵字時，它就會自動顯示其他常用的相關關鍵字。這服務也適用於[日文](../Page/日文.md "wikilink")。
    [Google Transit](../Page/Google_Maps#Google_Transit.md "wikilink") :
    Google Transit 透過Google Maps介面提供了公共交通行程計畫。Google
    Transit在2005年12月7日推出，現在已於六個城市可以使用。
    [Google
    Trends](../Page/Google_Trends.md "wikilink")（[Google趨勢](../Page/Google趨勢.md "wikilink")）:
    Google
    Trends是一個網頁搜索分析程式，使用戶透過此服務可以知道之前使用Google搜索引擎的使用者的喜好。當關鍵字被輸入，便能顯示不同國家城市或語言在不同年份搜索這個關鍵字的統計數據。此服務也適用於中文搜索。
    [GOOG-411](../Page/GOOG-411.md "wikilink")／[Google Voice Local
    Search](../Page/Google_Voice_Local_Search.md "wikilink"):
    只要利用電話打1-800-GOOG-411，就可以搜索當地的公司，這服務是免費的。
    [Google Web
    Accelerator](../Page/Google_Web_Accelerator.md "wikilink") : Google
    Web Accelerator是一個下載的軟件程式，它利用不同方法來增加瀏覽網頁的速度。
    [Experimental Search](../Page/Experimental_Search.md "wikilink") :
    查看Google最新的意念。
    [Product Search for
    Mobile](../Page/Product_Search_for_Mobile.md "wikilink") :
    用流動電話透過Product Search for Mobile來搜索產品。
    [Google人体浏览器](../Page/Google人体浏览器.md "wikilink") : Google body
    browser是一个人体虚拟解剖模型。

## Google實驗室的「畢業生」

以下是已經完成測試階段的Google產品以及服務：

  - [Google笔记本](../Page/Google笔记本.md "wikilink")
  - [Google文件](../Page/Google文件.md "wikilink")
  - [Google Video](../Page/Google_Video.md "wikilink")
  - [Personalized Search](../Page/Personalized_Search.md "wikilink")
  - [iGoogle](../Page/iGoogle.md "wikilink")
  - [Google地图](../Page/Google地图.md "wikilink")
  - [Google Scholar](../Page/Google_Scholar.md "wikilink")
  - [Google SMS (US)](../Page/Google_SMS_\(US\).md "wikilink")
  - [Google桌面搜索](../Page/Google桌面搜索.md "wikilink")
  - [Google Groups 2](../Page/Google_Groups_2.md "wikilink")
  - [Web Alerts](../Page/Web_Alerts.md "wikilink")
  - [Search by Location](../Page/Search_by_Location.md "wikilink")
  - [Google Glossary](../Page/Google_Glossary.md "wikilink")
  - [Google News Alerts](../Page/Google_News_Alerts.md "wikilink")
  - [Gmail](../Page/Gmail.md "wikilink")
  - [Google日曆](../Page/Google日曆.md "wikilink")
  - [Google Co-op](../Page/Google_Co-op.md "wikilink")
  - [Google新聞](../Page/Google新聞.md "wikilink")

有少數產品因種種原因，在上線正式運作後仍長期加上BETA字樣，例如Gmail及Google新聞。

## 外部連結

  - [Google
    Labs](https://web.archive.org/web/20101001094449/http://www.googlelabs.com/)

## 参考

[Category:已終止的Google服務](../Category/已終止的Google服務.md "wikilink")

1.