**獻仁陵**是[朝鮮王朝時代的君主](../Page/朝鮮王朝.md "wikilink")[太宗](../Page/朝鮮太宗.md "wikilink")（献陵）和[純祖的陵墓](../Page/朝鮮純祖.md "wikilink")（仁陵）的合稱，位於[大韓民國](../Page/大韓民國.md "wikilink")[首爾特別市](../Page/首爾特別市.md "wikilink")[瑞草區](../Page/瑞草區.md "wikilink")[內谷洞的](../Page/內谷洞.md "wikilink")[大母山](../Page/大母山.md "wikilink")。獻仁陵是韓國的法定歷史遺蹟，特色是陵園內有很多不同的石像。

1450年，[朝鮮世宗去世時](../Page/朝鮮世宗.md "wikilink")，本來亦與父親一起葬在献陵，[睿宗元年被移葬往](../Page/朝鮮睿宗.md "wikilink")[骊州](../Page/骊州.md "wikilink")[城山的](../Page/城山.md "wikilink")[英陵](../Page/英陵.md "wikilink")。

## 特點

獻陵由雙墩組成，國王被埋在左邊，兩個土丘通過欄杆相連埋在右邊的王后。而仁陵土堆，既包括國王和王后。
由於[風水的吉祥原因](../Page/風水.md "wikilink")，純祖原先埋在[坡州的](../Page/坡州.md "wikilink")[長陵](../Page/長陵（Injo）.md "wikilink")，而他的墳墓於1856年搬到了現在的位置。\[1\]

<table>
<thead>
<tr class="header">
<th><p>王陵</p></th>
<th><p>埋葬者</p></th>
<th><p>年份</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>獻陵 (헌릉)</p></td>
<td><p><a href="../Page/朝鮮太宗.md" title="wikilink">朝鮮太宗</a><br />
<a href="../Page/元敬王后.md" title="wikilink">元敬王后</a></p></td>
<td><p>1420</p></td>
</tr>
<tr class="even">
<td><p>仁陵 (인릉)</p></td>
<td><p><a href="../Page/朝鮮純祖.md" title="wikilink">朝鮮純祖</a><br />
<a href="../Page/純元王后.md" title="wikilink">純元王后</a></p></td>
<td><p>1856</p></td>
</tr>
</tbody>
</table>

[Heolleung.jpg](https://zh.wikipedia.org/wiki/File:Heolleung.jpg "fig:Heolleung.jpg")

[Category:朝鲜王朝王室墓葬](../Category/朝鲜王朝王室墓葬.md "wikilink")
[Category:首尔墓葬](../Category/首尔墓葬.md "wikilink")
[Category:韓國小作品](../Category/韓國小作品.md "wikilink")

1.