**雙瓣狸藻亞屬**（[学名](../Page/学名.md "wikilink")：）是[狸藻屬中的一個](../Page/狸藻屬.md "wikilink")[亞屬](../Page/亞屬.md "wikilink")。
本亚屬的最初描述是在1874年由所發表。在[彼得·泰勒的](../Page/彼得·泰勒_\(植物學家\).md "wikilink")1989年本屬的[專題論文上](../Page/專題論文.md "wikilink")，他將本亞屬降低成[異名](../Page/異名.md "wikilink")，置於[蚌實狸藻節中](../Page/蚌實狸藻節.md "wikilink")。由於分子[種系發生學研究的見解](../Page/種系發生學.md "wikilink")，這項判定後來被撤銷，並連帶的恢復了本亞屬。\[1\]\[2\]

## 相關條目

  - [狸藻屬物種列表](../Page/狸藻屬物種列表.md "wikilink")

## 注釋

[狸藻屬](../Category/狸藻屬.md "wikilink")

1.  Taylor, Peter. (1989). ''The genus *Utricularia*: A taxonomic
    monograph.'' Kew Bulletin Additional Series XIV: London.
2.  Müller, K.F., Borsch, T., Legendre, L., Porembski, S., and
    Barthlott, W. (2006). Recent progress in understanding the evolution
    of carnivorous Lentibulariaceae (Lamiales). *Plant Biology*, 8:
    748-757.