## [9月30日](../Page/9月30日.md "wikilink")

## [9月29日](../Page/9月29日.md "wikilink")

  - 载有150余人的[巴西戈尔航空公司1907航班在](../Page/巴西戈尔航空公司1907航班.md "wikilink")[马托格罗索州境内失踪](../Page/马托格罗索州.md "wikilink")。\[1\]
  - [泰國媒體報導](../Page/泰國.md "wikilink")，軍事執政團選擇前陸軍總司令、現為皇室顧問的[蘇拉育為新任總理](../Page/蘇拉育.md "wikilink")。\[2\]
  - 台海[两岸中秋包机服务首航](../Page/两岸.md "wikilink")。\[3\]

## [9月28日](../Page/9月28日.md "wikilink")

  - [泰國](../Page/泰國.md "wikilink")[新曼谷國際機場當地時間凌晨](../Page/新曼谷國際機場.md "wikilink")3時開幕，取代有80多年歷史的[曼谷廊曼國際機場](../Page/曼谷國際機場.md "wikilink")。
  - 香港[国泰航空公司宣布完成收购](../Page/国泰航空公司.md "wikilink")[港龙航空公司](../Page/港龙航空公司.md "wikilink")。\[4\]
  - [中国最新一代](../Page/中国.md "wikilink")[核聚变实验装置](../Page/核聚变.md "wikilink")[EAST在](../Page/EAST.md "wikilink")[安徽](../Page/安徽.md "wikilink")[合肥首次](../Page/合肥.md "wikilink")[放电](../Page/放电现象.md "wikilink")，成功获得[电流超过](../Page/电流.md "wikilink")200[千安](../Page/千安.md "wikilink")、[时间近](../Page/时间.md "wikilink")3秒的[高温](../Page/高温.md "wikilink")[等离子体放电](../Page/等离子体.md "wikilink")。

## [9月27日](../Page/9月27日.md "wikilink")

  - [中國](../Page/中國.md "wikilink")[國務院總理](../Page/國務院總理.md "wikilink")[溫家寶主持召開國務院常務會議](../Page/溫家寶.md "wikilink")，嚴肅處理[鄭州市違法批准征收佔用土地建設龍子湖高校園區問題](../Page/鄭州市.md "wikilink")。[中央紀委常委會已決定](../Page/中國共產黨中央紀律檢查委員會.md "wikilink")，分別給予[河南省委常委](../Page/河南省.md "wikilink")、政法委書記（原任河南省副省長）[李新民和河南省委常委](../Page/李新民.md "wikilink")、鄭州市委書記[王文超](../Page/王文超.md "wikilink")（原任鄭州市市長）嚴重黨內警告處分。\[5\]
  - [香港](../Page/香港.md "wikilink")[公共廣播服務檢討委員會建議](../Page/公共廣播服務檢討委員會.md "wikilink")，日後[香港電台脫離政府](../Page/香港電台.md "wikilink")，由獨立的董事局監管，行政總裁為總編輯。\[6\]
  - [伊朗总统](../Page/伊朗.md "wikilink")[马哈茂德·艾哈迈迪内贾德在一个研讨会上重申](../Page/马哈茂德·艾哈迈迪内贾德.md "wikilink")，伊朗不会在自己正当的核权利问题上作出让步，也不会中止铀浓缩活动。\[7\]\[8\]
  - 一份公佈的[聯合國報告指出](../Page/聯合國.md "wikilink")，[伊拉克戰爭為](../Page/伊拉克.md "wikilink")[蓋達組織提供了訓練中心及生力軍](../Page/基地组织.md "wikilink")。\[9\]
  - [美國](../Page/美國.md "wikilink")[科羅拉多州一間中學發生槍擊案](../Page/科羅拉多州.md "wikilink")：一位53歲男子挾持人質對峙，最後槍殺1名學生後。\[10\]
  - [美国众议院](../Page/美国众议院.md "wikilink")25名[共和党和](../Page/共和党.md "wikilink")[民主党议员致函众议院议长](../Page/民主党.md "wikilink")[丹尼斯·哈斯特德](../Page/丹尼斯·哈斯特德.md "wikilink")，要求众议院尽快处理“随军慰安妇决议案”，要求[日本政府承认动员随军慰安妇的事实和责任](../Page/日本.md "wikilink")，承诺不再发生此类事情。\[11\]
  - [菲律宾遭遇](../Page/菲律宾.md "wikilink")[台风象神侵袭](../Page/台风象神.md "wikilink")，造成上千名旅客滞留。\[12\]
  - [法国在一架飞机上模拟失重情况下](../Page/法国.md "wikilink")，成功完成一项外科手术。\[13\]\[14\]
  - [喬治亞政府以](../Page/喬治亞.md "wikilink")[間諜罪名起訴四名](../Page/間諜.md "wikilink")[俄羅斯軍官](../Page/俄羅斯.md "wikilink")，指控他們長期於喬治亞從事間諜活動；俄國政府稱這是「嚴重挑釁」。

## [9月26日](../Page/9月26日.md "wikilink")

  - 上午傳出[2007年維基媒體國際大會主要贊助商](../Page/維基媒體國際會議#2007年度.md "wikilink")[中華電信未能全額贊助](../Page/中華電信.md "wikilink")，恐怕使[臺北市主辦大會增添變數](../Page/臺北市.md "wikilink")；但後來經由中文維基百科管理員（同時為本次大會主要[公關](../Page/公關.md "wikilink")）的溝通，中華電信已於下午發布新聞稿澄清並全力支持2007年維基媒體國際大會的舉辦。
  - [安倍晋三就任第](../Page/安倍晋三.md "wikilink")90届、第57位[日本首相](../Page/日本首相.md "wikilink")。\[15\]

## [9月25日](../Page/9月25日.md "wikilink")

  - 經過超過30小時的討論後，[維基媒體國際大會主辦城市評審團於UTC時間凌晨零時正式宣佈](../Page/維基媒體國際大會.md "wikilink")，[2007年維基媒體國際大會將由](../Page/m:Wikimania_2007.md "wikilink")[台湾](../Page/台湾.md "wikilink")[臺北市舉行](../Page/臺北市.md "wikilink")。
  - 針對[台灣](../Page/台灣.md "wikilink")[陳水扁總統的修憲變更領土說](../Page/陳水扁.md "wikilink")，[美國國務院發言人](../Page/美國國務院.md "wikilink")[凱西表示](../Page/凱西.md "wikilink")，美國繼續反對任何一方片面改變現狀，美國「非常嚴肅看待陳水扁總統一再重申的各項保證」，包括他所說的，不允許憲改議題觸及領土定義。\[16\]\[17\]

## [9月24日](../Page/9月24日.md "wikilink")

  - [中共中央政治局召開會議](../Page/中共中央政治局.md "wikilink")，決定由中共[中央紀律檢查委員會對中共中央政治局委員](../Page/中央紀律檢查委員會.md "wikilink")、[上海市委書記](../Page/上海市.md "wikilink")[陳良宇因](../Page/陳良宇.md "wikilink")[社保基金挪用案立案檢查](../Page/2006年上海社保基金挪用案.md "wikilink")，免去他的上海市委書記、常委、委員職務，停止其中央政治局委員、中央委員職務。\[18\]\[19\]
  - 在[马德里举行的](../Page/马德里.md "wikilink")[世界羽毛球锦标赛上](../Page/世界羽毛球锦标赛.md "wikilink")，中国选手[林丹获得男单冠军](../Page/林丹.md "wikilink")，[谢杏芳获得女单冠军](../Page/谢杏芳.md "wikilink")，[傅海峰](../Page/傅海峰.md "wikilink")／[蔡赟获得男双冠军](../Page/蔡赟.md "wikilink")，[高崚](../Page/高崚.md "wikilink")／[黄穗获得女双冠军](../Page/黄穗.md "wikilink")，英国选手[罗布森](../Page/罗布森.md "wikilink")／[埃姆斯获得混双冠军](../Page/埃姆斯.md "wikilink")。搜狐体育\[20\]\[21\]\[22\]\[23\]\[24\]
  - [英国执政党](../Page/英国.md "wikilink")[工党年会在各反战团体的抗议声中在英国第三大城市](../Page/工党.md "wikilink")[曼彻斯特拉开帷幕](../Page/曼彻斯特.md "wikilink")。\[25\]
  - 由[埃及总理](../Page/埃及.md "wikilink")[纳齐夫直接领导的埃最高能源委员会在](../Page/纳齐夫.md "wikilink")[开罗举行第一次会议](../Page/开罗.md "wikilink")，通过了一项利用包括核能在内的新能源发电计划。
    \[26\]\[27\]
  - [萨达姆辩护律师团首席律师](../Page/萨达姆.md "wikilink")[哈利勒·杜莱米表示](../Page/哈利勒·杜莱米.md "wikilink")，由于审判萨达姆的[伊拉克高等法庭存在严重违法行为](../Page/伊拉克.md "wikilink")，律师团将无限期抵制对萨达姆犯有种族屠杀罪名的审判。\[28\]
  - [中國孔子基金會在](../Page/中國孔子基金會.md "wikilink")[孔子故里](../Page/孔子.md "wikilink")[山東](../Page/山東.md "wikilink")[曲阜向全球正式發布](../Page/曲阜.md "wikilink")[孔子標準像定稿](../Page/孔子標準像.md "wikilink")。\[29\]\[30\]
  - [台灣總統](../Page/台灣.md "wikilink")[陳水扁在](../Page/陳水扁.md "wikilink")[民進黨憲政改造研討會中拋出](../Page/民進黨.md "wikilink")「準制憲」議題，表示現行憲法中以「固有疆域」來界定領土範圍，已對現實問題產生衝擊。\[31\]\[32\]

## [9月23日](../Page/9月23日.md "wikilink")

  - [委内瑞拉强烈抗议](../Page/委内瑞拉.md "wikilink")[美国扣留该国外长](../Page/美国.md "wikilink")[尼古拉斯·马杜罗](../Page/尼古拉斯·马杜罗.md "wikilink")。\[33\][美国国土安全部发言人否认有关马杜罗在纽约](../Page/美国国土安全部.md "wikilink")[肯尼迪国际机场受到警方短暂拘留的报道](../Page/肯尼迪国际机场.md "wikilink")。[美国国务院代理发言人证实](../Page/美国国务院.md "wikilink")，在马杜罗抵达肯尼迪机场时“确实发生过令人遗憾的事情”，并称，“美国政府已向马杜罗外长和委内瑞拉政府表示道歉”。\[34\]
  - 应[中国国家航天局局长](../Page/中国国家航天局.md "wikilink")[孙来燕的邀请](../Page/孙来燕.md "wikilink")，[美国宇航局局长](../Page/美国宇航局.md "wikilink")[迈克尔·格里芬开始对中国进行](../Page/迈克尔·格里芬.md "wikilink")6天的访问。\[35\]
  - [法國地區報章共和報指出](../Page/法國.md "wikilink")，根據法國秘密情報單位的報告，[阿爾蓋達組織首腦](../Page/阿爾蓋達.md "wikilink")[拉登在](../Page/拉登.md "wikilink")8月底已在[巴基斯坦死於](../Page/巴基斯坦.md "wikilink")[傷寒](../Page/傷寒.md "wikilink")，但[美國和法國政府也無法證實有關消息](../Page/美國.md "wikilink")。\[36\]
  - [日本航天局成功发射一颗](../Page/日本航天局.md "wikilink")[太阳航天探测器](../Page/太阳航天探测器.md "wikilink")。\[37\]
  - 全世界的[穆斯林先后进入斋月](../Page/穆斯林.md "wikilink")“[拉玛丹月](../Page/拉玛丹月.md "wikilink")”。\[38\]

## [9月22日](../Page/9月22日.md "wikilink")

  - 在[德国南部](../Page/德国.md "wikilink")[埃姆斯兰的一輛](../Page/埃姆斯兰.md "wikilink")[磁悬浮列車於高架路试验路段](../Page/磁悬浮列車.md "wikilink")[與停在上面的工程車相撞](../Page/德國磁浮試驗線碰撞事故.md "wikilink")，相撞时列车速度为200公里／小时。事故造成23人死亡。事故发生后，总理[安格拉·梅克尔和联邦交通部长分别中断各自的会议赶到现场](../Page/安格拉·梅克尔.md "wikilink")。\[39\]\[40\]
  - 州衛生官員表示，[美国](../Page/美国.md "wikilink")[愛達荷州一名兩歲男童](../Page/愛達荷州.md "wikilink")，以及[馬里蘭州一名](../Page/馬里蘭州.md "wikilink")86歲老婦人疑因感染[大腸桿菌喪命](../Page/大腸桿菌.md "wikilink")，使得這波新鮮[菠菜大腸桿菌疫情死亡人數達到三人](../Page/菠菜.md "wikilink")。\[41\]
  - 新加坡媒体报道，[新加坡有关部门已完成对](../Page/新加坡.md "wikilink")6种[SK-II品牌化妆品的检测工作](../Page/SK-II.md "wikilink")，结果发现这些产品中确实含有少量的重金属[铬和](../Page/铬.md "wikilink")[钕](../Page/钕.md "wikilink")。\[42\]
  - [真主党领导人](../Page/真主党.md "wikilink")[纳斯鲁拉出席庆祝](../Page/哈桑·纳斯鲁拉.md "wikilink")“战胜以色列”活动时说，真主党永远不会被迫放下武器。\[43\]\[44\]
  - [香港警方以涉嫌企圖導致爆炸及意圖危害生命或財產](../Page/香港警察.md "wikilink")，將一名21歲的網民拘捕。該網民自稱「[真主教](../Page/真主黨.md "wikilink")[恐怖分子](../Page/恐怖分子.md "wikilink")」，詢問如何製造[炸彈炸毀](../Page/炸彈.md "wikilink")「[迪迪尼](../Page/香港迪士尼樂園.md "wikilink")」。

## [9月21日](../Page/9月21日.md "wikilink")

  - [东京都地方法院做出判决](../Page/东京.md "wikilink")，裁定教职员工没有向“日之丸”国旗肃立以及齐唱“[君之代](../Page/君之代.md "wikilink")”国歌的义务。\[45\]
  - 正在[美國訪問的](../Page/美國.md "wikilink")[巴基斯坦總統](../Page/巴基斯坦.md "wikilink")[穆沙拉夫接受訪問時說](../Page/穆沙拉夫.md "wikilink")，美國曾以轟炸巴基斯坦脅逼巴國加入反恐行列。\[46\]\[47\]

## [9月20日](../Page/9月20日.md "wikilink")

  - [安倍晋三在](../Page/安倍晋三.md "wikilink")[日本自民党总裁竞选中获胜](../Page/日本自民党.md "wikilink")，很可能出任日本下一届[首相](../Page/日本首相.md "wikilink")。\[48\]
  - [匈牙利總理](../Page/匈牙利.md "wikilink")[玖爾恰尼承認在選舉中向選民說謊](../Page/玖爾恰尼.md "wikilink")，引發[反政府抗議](../Page/2006年匈牙利抗議.md "wikilink")，要求總理下台。在[布達佩斯街頭](../Page/布達佩斯.md "wikilink")，連續第二晚發生警民衝突。示威者向警察丟擲石塊，而警察則以催淚彈與水炮回敬示威者。\[49\]
  - [泰國軍事政變領導人](../Page/2006年泰國軍事政變.md "wikilink")[颂提·汶雅叻格林表示軍方將在兩周內任命替換](../Page/颂提·汶雅叻格林.md "wikilink")[-{zh-hans:他信;zh-hk:他信;zh-tw:塔克辛;}-的新總理](../Page/塔信·欽那瓦.md "wikilink")。\[50\]
  - [欧洲空间局说](../Page/欧洲空间局.md "wikilink")，[欧洲环境卫星发回的图片显示](../Page/欧洲.md "wikilink")，欧洲北部至[北冰洋一带的永冻冰正在以前所未有的速度松动融化](../Page/北冰洋.md "wikilink")。\[51\]
  - 一架载有多名[伊朗政府高官的客机在经由](../Page/伊朗.md "wikilink")[土耳其领空返回](../Page/土耳其.md "wikilink")[德黑兰途中](../Page/德黑兰.md "wikilink")，被土耳其强制要求在[伊斯坦布尔机场降落](../Page/伊斯坦布尔机场.md "wikilink")，并被扣留数小时。\[52\]

## [9月19日](../Page/9月19日.md "wikilink")

[Thai_coup_soldier.jpg](https://zh.wikipedia.org/wiki/File:Thai_coup_soldier.jpg "fig:Thai_coup_soldier.jpg")

  - [美国宇航局宣布](../Page/美国宇航局.md "wikilink")[亚特兰蒂斯号](../Page/亚特兰蒂斯号.md "wikilink")[航天飞机由于有神秘物体脱落将推迟一天着陆](../Page/航天飞机.md "wikilink")，宇航员将检查航天飞机受损情况。\[53\]\[54\]
  - [泰国首都](../Page/泰国.md "wikilink")[曼谷发生](../Page/曼谷.md "wikilink")[军事政变](../Page/2006年泰國軍事政變.md "wikilink")，軍方派出十多輛坦克封鎖了曼谷政府大樓周圍的道路，正在紐約參加[聯合國大會的](../Page/聯合國.md "wikilink")[他信宣佈泰國進入緊急狀態](../Page/他信.md "wikilink")，並宣布解除陸軍司令職務。\[55\]\[56\]\[57\]

## [9月18日](../Page/9月18日.md "wikilink")

  - [西南索马里](../Page/西南索马里.md "wikilink")[拜多阿國會門前](../Page/拜多阿.md "wikilink")15分鐘內發生兩宗汽車炸彈爆炸。汽車爆炸時總統[阿卜杜拉希·尤素福·艾哈邁德的車隊正經過現場](../Page/阿卜杜拉希·尤素福·艾哈邁德.md "wikilink")，索馬里外長指爆炸旨在謀殺總統。\[58\]
  - [梵蒂冈派出神職人員到](../Page/梵蒂冈.md "wikilink")[伊斯兰教國家](../Page/伊斯兰教.md "wikilink")，解釋[教皇](../Page/教皇.md "wikilink")[本篤十六世並無意冒犯伊斯兰教](../Page/本篤十六世.md "wikilink")。\[59\]
  - [國際貨幣基金會以](../Page/國際貨幣基金會.md "wikilink")90.6%成员国同意通過了一項計劃，增加[中國](../Page/中國.md "wikilink")、[韓國](../Page/韓國.md "wikilink")、[墨西哥和](../Page/墨西哥.md "wikilink")[土耳其四個新興經濟體的投票權比重](../Page/土耳其.md "wikilink")，以更能反映他們在全球經濟中的分量。\[60\]
  - 来自世界各国的60多位国家元首和领导人的夫人及教育部长聚集在[纽约公共图书馆参加](../Page/纽约公共图书馆.md "wikilink")[白宫全球扫盲会议](../Page/白宫全球扫盲会议.md "wikilink")，呼吁各国政府为推进扫盲事业作出不懈努力。\[61\]

## [9月17日](../Page/9月17日.md "wikilink")

  - [伊朗总统](../Page/伊朗.md "wikilink")[内贾德对](../Page/内贾德.md "wikilink")[委内瑞拉进行访问](../Page/委内瑞拉.md "wikilink")，加强两国的双边合作。在内贾德访问期间，两国签署20项商业协议，覆盖了[石油勘探等各个经济领域](../Page/石油.md "wikilink")。\[62\]
  - [颱風珊珊下午登陸](../Page/颱風珊珊.md "wikilink")[日本西南部的](../Page/日本.md "wikilink")[九州](../Page/九州.md "wikilink")，強風暴雨下有8人死亡，240人受傷。\[63\]
  - 瑞典温和联合党主席[林费尔德率领在野的瑞典温和联合党](../Page/林费尔德.md "wikilink")、人民党、中央党和基督教民主党组成的中右联盟获胜，将取代以社会民主党（社民党）为主联合左翼党、环境党组成的中左联盟，在今后4年执政。\[64\]

## [9月16日](../Page/9月16日.md "wikilink")

  - \-{zh-hans:教皇;
    zh-hant:教宗;}-[本篤十六世發表聲明](../Page/本篤十六世.md "wikilink")，對他較早時的言論令伊斯蘭教徒感到受傷害，表達抱歉。\[65\]
  - [伊朗总统](../Page/伊朗.md "wikilink")[马哈茂德·艾哈迈迪内贾德与](../Page/马哈茂德·艾哈迈迪内贾德.md "wikilink")[朝鲜最高人民会议常任委员会委员长](../Page/朝鲜.md "wikilink")[金永南在](../Page/金永南.md "wikilink")[古巴首都](../Page/古巴.md "wikilink")[哈瓦那举行了会谈](../Page/哈瓦那.md "wikilink")。双方决定共同推进核开发，团结一致对抗[美国](../Page/美国.md "wikilink")。\[66\]

## [9月15日](../Page/9月15日.md "wikilink")

  - [台灣的](../Page/台灣.md "wikilink")[百萬人民倒扁運動進行圍城遊行](../Page/百萬人民倒扁運動.md "wikilink")，行動總指揮[施明德宣布圍城成功](../Page/施明德.md "wikilink")，大會聲稱有一百萬人參與，而[台北市警方則說有三十二萬人](../Page/台北市.md "wikilink")。\[67\]
  - 纏訟長達十年之久的[日本](../Page/日本.md "wikilink")[奧姆真理教前教主](../Page/奧姆真理教.md "wikilink")[麻原彰晃](../Page/麻原彰晃.md "wikilink")，涉嫌發動[東京](../Page/東京.md "wikilink")[地鐵毒氣案](../Page/地鐵.md "wikilink")，日本最高法院判定麻原彰晃維持死刑，全案定讞不得上訴。\[68\]

## [9月14日](../Page/9月14日.md "wikilink")

  - [香港以南水域在晚上](../Page/香港.md "wikilink")7時53分（香港時間）[黎克特制](../Page/黎克特制.md "wikilink")3.5
    級[地震](../Page/地震.md "wikilink")，歷時2-3秒，[震央位於香港的東南偏南約](../Page/震央.md "wikilink")36[公里](../Page/公里.md "wikilink")（北緯22.0度，東經114.3度）的[擔桿列島海域](../Page/擔桿列島.md "wikilink")，該次發生地震的海域並不屬於[香港特別行政區管轄範圍內](../Page/香港特別行政區.md "wikilink")，是屬於珠海市管轄範圍內。香港上一次發生地震是[1995年](../Page/1995年.md "wikilink")。
    \[69\]\[70\]\[71\]\[72\]。
  - [日本](../Page/日本.md "wikilink")193人集體控告下任首相大熱人選[安倍晉三支持右翼篡改歷史教科書](../Page/安倍晉三.md "wikilink")。\[73\]
  - 驻[伊拉克美军发表声明说](../Page/伊拉克.md "wikilink")，首都[巴格达当天发生多起针对驻伊美军的袭击事件](../Page/巴格达.md "wikilink")，共造成4名美军士兵丧生，25名士兵受伤。\[74\]
  - [俄罗斯航天兵发言人对媒体说](../Page/俄罗斯.md "wikilink")，俄当天成功将一颗属于“宇宙”系列的军用[卫星送入了预定轨道](../Page/卫星.md "wikilink")。\[75\]

## [9月13日](../Page/9月13日.md "wikilink")

  - [中華民國總統](../Page/中華民國.md "wikilink")[陳水扁表示](../Page/陳水扁.md "wikilink")，面對連續14年叩關[聯合國挫敗後](../Page/聯合國.md "wikilink")，必須採取不同策略來爭取台灣的權利；在七成九的民眾贊成以「台灣」名義申請加入聯合國下，未來應該認真思考以「台灣」名義直接申請加入聯合國，以新會員國的身分重新申請，以回應民意的要求。\[76\]
  - [美国国家海洋和大气管理局报告说](../Page/美国国家海洋和大气管理局.md "wikilink")，种种迹象表明，“[厄尔尼诺](../Page/厄尔尼诺.md "wikilink")”已在[赤道附近的](../Page/赤道.md "wikilink")[太平洋海域形成并将持续到明年初](../Page/太平洋.md "wikilink")，预计将有多个国家受波及。\[77\]
  - 据[伊拉克内政部消息人士透露](../Page/伊拉克.md "wikilink")，首都[巴格达地区在短短一天之内就发现](../Page/巴格达.md "wikilink")60具尸体，这些尸体全部身份不明。\[78\]
  - [俄羅斯央行第一副總裁](../Page/俄羅斯.md "wikilink")[柯茲洛夫](../Page/柯茲洛夫.md "wikilink")（Andrei
    Kozlov）遭槍擊，傷勢嚴重，在醫院仍未脫離險境。\[79\]
  - [联合国环境规划署宣布](../Page/联合国环境规划署.md "wikilink")，该署与网上搜索引擎公司“[谷歌](../Page/谷歌.md "wikilink")”（Google）合作开发的网上3维环保热点地图，正式开通。\[80\]
  - [联合国人口基金](../Page/联合国人口基金.md "wikilink")（UNFPA）和[国际移民组织](../Page/国际移民组织.md "wikilink")（IOM）在[联合国总部共同举办妇女与移民和发展问题专题研讨会](../Page/联合国.md "wikilink")。\[81\]

## [9月12日](../Page/9月12日.md "wikilink")

  - [美国驻](../Page/美国.md "wikilink")[叙利亚大使馆遭袭击](../Page/叙利亚.md "wikilink")，数人丧生。\[82\]\[83\]\[84\]\[85\]
  - [日本](../Page/日本.md "wikilink")[秋篠宮文仁親王与](../Page/秋篠宮文仁親王.md "wikilink")[秋篠宮妃紀子的長男被命名為](../Page/秋篠宮妃紀子.md "wikilink")「[悠仁](../Page/秋篠宮悠仁.md "wikilink")」，他的象徵記號是[高野槙](../Page/高野槙.md "wikilink")。\[86\]\[87\]
  - [日本各大媒體報導](../Page/日本.md "wikilink")，日本現任[農林水產省副大臣](../Page/農林水產省.md "wikilink")[宮腰光寬曾於八月秘密訪台](../Page/宮腰光寬.md "wikilink")，並會見包括[陳水扁總統在內的政府高層](../Page/陳水扁.md "wikilink")。\[88\]
  - [台灣](../Page/台灣.md "wikilink")[民進黨主席](../Page/民進黨.md "wikilink")[游錫堃飛抵](../Page/游錫堃.md "wikilink")[華府](../Page/華府.md "wikilink")，展開四天的政黨外交訪問。上午與[白宮副國家安全顧問](../Page/白宮.md "wikilink")[柯羅契進行晤談](../Page/柯羅契.md "wikilink")。\[89\]\[90\]
  - [联合国连续](../Page/联合国.md "wikilink")14年否决[台灣参与议案](../Page/台灣.md "wikilink")。\[91\]

## [9月11日](../Page/9月11日.md "wikilink")

  - 南[太平洋島國](../Page/太平洋.md "wikilink")[東加王國的國王](../Page/湯加.md "wikilink")[杜包四世凌晨於](../Page/圖普四世.md "wikilink")[新西蘭病逝](../Page/新西蘭.md "wikilink")，享年88歲。他在位41年，是當代在位年期最長的君主之一；體重一度達462磅的他，更被列入[健力士世界紀錄最肥胖君主](../Page/健力士世界紀錄.md "wikilink")。\[92\]\[93\]
  - [亞歐高峰會議結束](../Page/亞歐高峰會議.md "wikilink")，與會代表同意致力減少全球暖化、反恐和繼續進行世貿會談。\[94\]
  - 在[卡塔尔](../Page/卡塔尔.md "wikilink")[半岛电视台播出的一份錄影帶裡](../Page/半岛电视台.md "wikilink")，“[基地](../Page/基地.md "wikilink")”组织二号人物[扎瓦希里威胁说](../Page/扎瓦希里.md "wikilink")，该组织接下来将要袭击的目标是[以色列和](../Page/以色列.md "wikilink")[波斯灣国家](../Page/波斯灣.md "wikilink")。\[95\]
  - 去年被[中國驅逐出國的](../Page/中國.md "wikilink")[新疆](../Page/新疆.md "wikilink")[維吾爾族女商人](../Page/維吾爾族.md "wikilink")、著名維權人士[熱比婭](../Page/熱比婭.md "wikilink")，獲[諾貝爾和平獎提名](../Page/諾貝爾和平獎.md "wikilink")。\[96\]\[97\]
  - 「[記者無疆界](../Page/記者無國界.md "wikilink")」組織發表聲明，對[中國頒布管理外國通訊社的新辦法表示嚴重關注](../Page/中國.md "wikilink")，並呼籲國際社會對中國限制資訊自由傳播的新企圖採取聯合行動。[美國國務院發言人](../Page/美國國務院.md "wikilink")[蓋勒戈斯對此限制表示嚴重關切](../Page/蓋勒戈斯.md "wikilink")。[歐盟貿易專員](../Page/歐盟.md "wikilink")[曼德森指出](../Page/曼德森.md "wikilink")，[北京此舉顯然違背](../Page/北京.md "wikilink")[2001年加入](../Page/2001年.md "wikilink")[世貿組織](../Page/世貿組織.md "wikilink")（[WTO](../Page/WTO.md "wikilink")）時所做的金融資訊自由化承諾，並誓言積極催逼此事。\[98\]\[99\]\[100\]

## [9月10日](../Page/9月10日.md "wikilink")

  - [一級方程式賽車世界冠軍](../Page/一級方程式.md "wikilink")[-{zh-hans:迈克尔·舒马赫;
    zh-hk:米高·舒密加;}-宣布将在赛季结束后退役](../Page/迈克尔·舒马赫.md "wikilink")。\[101\]
  - [新华社发布](../Page/新华社.md "wikilink")《外国通讯社在中国境内发布新闻信息管理办法》，并从发布之日起施行。\[102\]\[103\]

## [9月9日](../Page/9月9日.md "wikilink")

  - 效力於[紐約洋基隊的](../Page/紐約洋基.md "wikilink")[台灣](../Page/台灣.md "wikilink")[旅美球員](../Page/旅美棒球選手.md "wikilink")[王建民先發出戰](../Page/王建民.md "wikilink")[巴爾的摩金鶯隊](../Page/巴爾的摩金鶯.md "wikilink")，以主投7.1局，被擊出八支[安打失一分的表現](../Page/安打.md "wikilink")，率領洋基以3:2力克金鶯，獲得本季個人第十七勝，並列[美國聯盟勝投王](../Page/美國聯盟.md "wikilink")，同時單季投球局數也達到200局的里程碑。\[104\]\[105\]
  - [中國國務院總理](../Page/中國國務院總理.md "wikilink")[温家宝抵达](../Page/温家宝.md "wikilink")[芬兰首都](../Page/芬兰.md "wikilink")[赫爾辛基出席](../Page/赫爾辛基.md "wikilink")[亞歐會議](../Page/亞歐會議.md "wikilink")。\[106\]
  - [第63届威尼斯国际电影节正式闭幕](../Page/第63届威尼斯国际电影节.md "wikilink")，中国导演[贾樟柯凭借电影](../Page/贾樟柯.md "wikilink")《[三峡好人](../Page/三峡好人.md "wikilink")》获得該影展最高影片奖项[金狮奖](../Page/金狮奖.md "wikilink")。\[107\]
  - 中国选手[刘翔在德国](../Page/刘翔.md "wikilink")[斯图加特](../Page/斯图加特.md "wikilink")[国际田联总决赛男子](../Page/国际田联.md "wikilink")110米栏决赛中以12秒93夺得冠军并打破赛会纪录。\[108\]
  - [印度西部馬哈拉什特拉省馬列加恩市分別發生數起爆炸事件](../Page/印度.md "wikilink")，造成最少四十七死、超過一千人受傷。\[109\]
  - 台灣[倒扁静坐運動开始](../Page/百萬人民倒扁運動.md "wikilink")。\[110\]\[111\]\[112\]
  - [美國國家航空暨太空總署成功發射](../Page/美國國家航空暨太空總署.md "wikilink")「[亞特蘭提斯號](../Page/亞特蘭提斯號太空梭.md "wikilink")」[太空梭](../Page/太空梭.md "wikilink")，此行目的是要接續停頓已久的[國際太空站建造工程](../Page/國際太空站.md "wikilink")。\[113\]

## [9月8日](../Page/9月8日.md "wikilink")

  - [台灣](../Page/台灣.md "wikilink")[百萬人倒扁運動領導人](../Page/百萬人倒扁運動.md "wikilink")[施明德的前妻](../Page/施明德.md "wikilink")[艾琳達](../Page/艾琳達.md "wikilink")，上午出面呼籲停止靜坐，強調[9月9日是](../Page/9月9日.md "wikilink")[毛澤東的忌日](../Page/毛澤東.md "wikilink")，不應該把[凱達格蘭大道變成一片](../Page/凱達格蘭大道.md "wikilink")「紅海」，不然將會成國際大笑話。\[114\]\[115\]\[116\]
  - [伊拉克總理](../Page/伊拉克.md "wikilink")[馬里奇和駐伊美軍最高指官](../Page/馬里奇.md "wikilink")[凱西上將簽署了一份協議](../Page/凱西.md "wikilink")，[美國把伊拉克軍隊的作戰指揮權交還給伊拉克政府](../Page/美國.md "wikilink")。\[117\]
  - [台灣](../Page/台灣.md "wikilink")[旅美棒球選手](../Page/旅美棒球選手.md "wikilink")[郭泓志首度在](../Page/郭泓志.md "wikilink")[大聯盟擔任](../Page/大聯盟.md "wikilink")[道奇隊先發投手](../Page/道奇隊.md "wikilink")，在[紐約大都會球場作客之役](../Page/紐約大都會.md "wikilink")，表現優異，投出生涯首場先發勝投。\[118\]

## [9月7日](../Page/9月7日.md "wikilink")

  - [英國首相](../Page/英國首相.md "wikilink")[布萊爾宣佈](../Page/貝理雅.md "wikilink")，自己將會在[2007年](../Page/2007年.md "wikilink")[工黨大會舉行前辭職](../Page/工黨.md "wikilink")。\[119\]
  - [中国新东方教育科技集团在美国](../Page/中国新东方教育科技集团.md "wikilink")[纽约股票交易所上市](../Page/纽约股票交易所.md "wikilink")，成为第一家在[美国上市的中国教育服务公司](../Page/美国.md "wikilink")。\[120\]
  - [以色列总理办公室宣布](../Page/以色列.md "wikilink")，以色列已经解除对[黎巴嫩机场的封锁](../Page/黎巴嫩.md "wikilink")，对黎港口的封锁将推迟48小时。\[121\]
  - [卡塔尔](../Page/卡塔尔.md "wikilink")[半岛电视台播放了一盘新录像带](../Page/半岛电视台.md "wikilink")，介绍了[基地组织在发动](../Page/基地组织.md "wikilink")[九一一袭击事件前的准备活动](../Page/九一一袭击事件.md "wikilink")。\[122\]
  - [俄罗斯国防部新闻和公共关系局对新闻界说](../Page/俄罗斯.md "wikilink")，俄军方当天试射的一枚“布拉瓦”（又称“圆锤”）[洲际弹道导弹偏离轨道并坠入海中](../Page/洲际弹道导弹.md "wikilink")。试射活动宣告失败。\[123\]
  - [中國](../Page/中國.md "wikilink")[國台辦主任](../Page/國台辦.md "wikilink")[陳雲林與來訪的](../Page/陳雲林.md "wikilink")[國民黨中央政策會執行長](../Page/國民黨.md "wikilink")[曾永權共同宣布](../Page/曾永權.md "wikilink")，國、共兩黨[兩岸農業論壇將改在大陆舉行](../Page/兩岸.md "wikilink")，此一宣布意味著陳雲林訪台案破局。\[124\]
  - 由[微軟協助設計](../Page/微軟.md "wikilink")，被形容為「未來學校」的高科技學校，在[美國](../Page/美國.md "wikilink")[賓州](../Page/賓州.md "wikilink")[費城正式開學](../Page/費城.md "wikilink")。\[125\]
  - [台灣原中正國際機場正式更名為](../Page/台灣.md "wikilink")[台灣桃園國際機場](../Page/台灣桃園國際機場.md "wikilink")。

## [9月6日](../Page/9月6日.md "wikilink")

  - [日本王妃](../Page/日本.md "wikilink")[秋篠宮妃紀子剖腹誕下一名男嬰](../Page/秋篠宮妃紀子.md "wikilink")，使日本皇室41年來首次有男性出生。此嬰兒亦成為[日本天皇第三順位繼承人](../Page/日本天皇.md "wikilink")。\[126\]
  - [台灣原](../Page/台灣.md "wikilink")[中正國際機場正式更名為](../Page/中正國際機場.md "wikilink")[台灣桃園國際機場](../Page/台灣桃園國際機場.md "wikilink")。\[127\]
  - 由於[英國首相](../Page/英國.md "wikilink")[布萊爾拒絕說明何時交棒](../Page/布萊爾.md "wikilink")，包括國防部次官[瓦森在內的](../Page/瓦森.md "wikilink")7名內閣成員提出辭呈。\[128\]
  - [俄羅斯海軍一艘](../Page/俄羅斯.md "wikilink")[核子動力潛艦晚間因為線路短路失火](../Page/核子動力潛艦.md "wikilink")，造成一名海軍士官長和一名水手死亡，軍方表示，這起事故並沒有造成輻射外洩，情況在控制之中。\[129\]
  - [美國總統](../Page/美國.md "wikilink")[布什晚間首度坦承](../Page/乔治·沃克·布什.md "wikilink")，為了探究[911事件真相](../Page/911事件.md "wikilink")，[中央情報局曾將](../Page/中央情報局.md "wikilink")14名主謀「留置」在秘密監獄之中。\[130\]
  - [美國](../Page/美國.md "wikilink")[國土安全部和](../Page/國土安全部.md "wikilink")[司法部宣布](../Page/美国司法部.md "wikilink")，美國執法人員已查獲15個[貨櫃](../Page/貨櫃.md "wikilink")，裝有13萬5千多雙從[中國](../Page/中國.md "wikilink")[走私](../Page/走私.md "wikilink")[進口的假](../Page/進口.md "wikilink")[耐吉](../Page/耐吉.md "wikilink")（[Nike](../Page/Nike.md "wikilink")）鞋，共起訴6名涉案者。據稱這是美國執法當局破獲的歷史上最大假冒商品走私案之一。\[131\]
  - [中國](../Page/中國.md "wikilink")[旅滬級導彈](../Page/旅滬級.md "wikilink")[驅逐艦](../Page/驅逐艦.md "wikilink")「青島號」和補給燃料船「洪澤湖號」駛抵[美國](../Page/美國.md "wikilink")[夏威夷](../Page/夏威夷.md "wikilink")[珍珠港](../Page/珍珠港.md "wikilink")。這是6年來，中國軍艦首度到訪美國。\[132\]

## [9月5日](../Page/9月5日.md "wikilink")

  - [中國](../Page/中國.md "wikilink")[甘肅省](../Page/甘肅省.md "wikilink")[徽縣水陽鄉二千多村民遠道來](../Page/徽縣.md "wikilink")[西安進行](../Page/西安.md "wikilink")[血鉛檢測](../Page/血鉛檢測.md "wikilink")，發現[血液含鉛量超標](../Page/血液.md "wikilink")，超過三百名兒童鉛中毒。距離幾百公尺的造鉛工廠可能是污染源頭，被懷疑是這次事件的起因。\[133\]\[134\]\[135\]\[136\]\[137\]\[138\]\[139\]\[140\]
  - [中國國務院總理](../Page/中國國務院.md "wikilink")[溫家寶在接受](../Page/溫家寶.md "wikilink")[英國](../Page/英國.md "wikilink")《[路透社](../Page/路透社.md "wikilink")》、《[泰晤士報](../Page/泰晤士報.md "wikilink")》、[德國](../Page/德國.md "wikilink")《[德新社](../Page/德新社.md "wikilink")》、《[法蘭克福匯報](../Page/法蘭克福匯報.md "wikilink")》、[芬蘭](../Page/芬蘭.md "wikilink")《[赫爾辛基新聞報](../Page/赫爾辛基新聞報.md "wikilink")》等五家[歐洲媒體的聯合採訪時表示](../Page/歐洲.md "wikilink")，[中國在現階段推動鄉鎮一級政府領導人直選的條件還不成熟](../Page/中國.md "wikilink")。\[141\]
  - [墨西哥最高選舉法院](../Page/墨西哥.md "wikilink")7位法官一致裁定[7月舉行的大選不存在舞弊](../Page/2006年墨西哥總統選舉.md "wikilink")，保守派的執政黨候選人[費利佩·卡爾德隆當選總統](../Page/費利佩·卡爾德隆.md "wikilink")，使兩個月來的舞弊指控落幕。\[142\]

## [9月4日](../Page/9月4日.md "wikilink")

  - [澳洲](../Page/澳洲.md "wikilink")[鱷魚專家](../Page/鱷魚.md "wikilink")[史帝夫·厄文在拍攝紀錄片中途](../Page/史帝夫·厄文.md "wikilink")，被[魔鬼魚的尾鉤刺中胸部死亡](../Page/魔鬼魚.md "wikilink")。\[143\]
  - 全球最大型空中巴士[A380在](../Page/A380.md "wikilink")[法國乘載](../Page/法國.md "wikilink")474名乘客試飛成功。\[144\]

## [9月3日](../Page/9月3日.md "wikilink")

  - 2006年[金字塔杯世界男子壁球公开赛在](../Page/金字塔杯世界男子壁球公开赛.md "wikilink")[开罗进行了两场四分之一决赛](../Page/开罗.md "wikilink")。现排名世界第六、前世界头号[壁球手](../Page/壁球.md "wikilink")[英国人](../Page/英国.md "wikilink")[彼得·尼科尔](../Page/彼得·尼科尔.md "wikilink")，在赛后宣布退役。\[145\]
  - 伊拉克[库尔德部族领导人要求](../Page/库尔德人.md "wikilink")[伊拉克政府更换](../Page/伊拉克.md "wikilink")[萨达姆时代的](../Page/萨达姆.md "wikilink")[国旗](../Page/伊拉克国旗.md "wikilink")，否则[库尔德斯坦将会正式宣布从伊拉克独立](../Page/库尔德斯坦.md "wikilink")。\[146\]
  - [伊拉克当局宣布](../Page/伊拉克.md "wikilink")，他们拘捕了[伊拉克基地组织的第二号人物](../Page/伊拉克基地组织.md "wikilink")[赛义迪](../Page/赛义迪.md "wikilink")。\[147\]
  - [欧洲空间局发射的](../Page/欧洲空间局.md "wikilink")[SMART-1号](../Page/SMART-1.md "wikilink")[空间探测器成功撞击](../Page/空间探测器.md "wikilink")[月球](../Page/月球.md "wikilink")，完成了持续16个月的任务。\[148\]
  - 美國前世界[網球球王](../Page/網球.md "wikilink")[阿格西因傷於](../Page/阿加西.md "wikilink")[美國網球公開賽](../Page/2006年美國網球公開賽.md "wikilink")[第三輪賽事中落敗](../Page/2006年美國網球公開賽男子單打比賽.md "wikilink")，無緣晉級第四輪，正式結束了長達21年、傳奇性的職業生涯。\[149\]\[150\]\[151\]\[152\]

## [9月2日](../Page/9月2日.md "wikilink")

  - [美國數以百計的](../Page/美國.md "wikilink")[天文學家連署](../Page/天文學.md "wikilink")，要求推翻[2006年行星重定義](../Page/2006年行星重定義.md "wikilink")。\[153\]

## [9月1日](../Page/9月1日.md "wikilink")

  - 一架[伊朗客机在伊朗东北部城市](../Page/伊朗.md "wikilink")[马什哈德着陆时起火](../Page/马什哈德.md "wikilink")，目前已造成近29名乘客丧生。\[154\]\[155\]\[156\]
  - [美國](../Page/美國.md "wikilink")「[國家癌症研究中心](../Page/國家癌症研究中心.md "wikilink")」（NCI）宣布：該中心的研究團隊已於[癌症的](../Page/癌症.md "wikilink")[基因治療取得重大進展](../Page/基因治療.md "wikilink")，兩名罹患晚期[黑色素瘤](../Page/黑色素瘤.md "wikilink")（一種[皮膚癌](../Page/皮膚癌.md "wikilink")）的病人，在注射經過[基因改造的](../Page/基因改造.md "wikilink")[T細胞](../Page/T細胞.md "wikilink")（[白血球的一種](../Page/白血球.md "wikilink")）之後，病況大為好轉，且已經一年多沒有發病。\[157\]\[158\]
  - [美國國務院發布聲明指出](../Page/美國國務院.md "wikilink")，[美國政府正式同意](../Page/美國.md "wikilink")[中華民國總統](../Page/中華民國.md "wikilink")[陳水扁在](../Page/陳水扁.md "wikilink")[9月6日由](../Page/9月6日.md "wikilink")[諾魯返回台灣時過境](../Page/諾魯.md "wikilink")[關島的需求](../Page/關島.md "wikilink")。\[159\]
  - [美國](../Page/美國.md "wikilink")[飛彈防禦系統成功擊落模擬](../Page/飛彈防禦系統.md "wikilink")[北韓](../Page/北韓.md "wikilink")[大浦洞2號飛彈軌道的來襲彈頭](../Page/大浦洞2號飛彈.md "wikilink")，這是美國第1次成功攔截到模擬北韓飛彈，也是從1999年以來，飛彈防禦系統第6次測試成功。\[160\]
  - [香港的新教育制度三三四制的第一批學生升上中學](../Page/香港.md "wikilink")。

## 参考資料

1.  [新浪](http://news.sina.com.cn/w/2006-10-01/142610153463s.shtml)
2.  [自由電子報](http://www.libertytimes.com.tw/2006/new/sep/30/today-int6.htm)

3.  [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_5390000/newsid_5390900/5390930.stm)
4.  [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_5380000/newsid_5387500/5387542.stm)
5.  [新華網](http://news.xinhuanet.com/politics/2006-09/27/content_5145416.htm)
6.  [明報](http://hk.news.yahoo.com/060927/12/1trvk.html)
7.  [新华网](http://news.xinhuanet.com/world/2006-09/28/content_5146392.htm)
8.  [CNN](http://www.cnn.com/WORLD/)
9.  [雅虎新闻](http://hk.news.yahoo.com/060928/3/1tsep.html)
10. [雅虎新闻](http://hk.news.yahoo.com/060928/12/1tsdx.html)
11. [新浪网](http://news.sina.com.cn/w/2006-09-28/082211124964.shtml)
12. [新华网](http://news.xinhuanet.com/world/2006-09/27/content_5147312.htm)
13. [CNN](http://www.cnn.com/2006/HEALTH/09/27/space.surgery.ap/index.html)

14. [新华网](http://news.xinhuanet.com/world/2006-09/26/content_5137311.htm)
15. [共同社](http://china.kyodo.co.jp/modules/fsStory/index.php?sel_lang=schinese&storyid=33584)

16. [中時電子報](http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,110501+112006092600001,00.html)
17. [自由電子報](http://www.libertytimes.com.tw/2006/new/sep/26/today-fo2.htm)

18. [新華網](http://news.xinhuanet.com/politics/2006-09/25/content_5134888.htm)
19. [1](http://news.xinhuanet.com/politics/2006-09/25/content_5134894.htm)
20. [林丹挫鲍春来赢得巅峰之战
    首次问鼎世锦赛冠军-搜狐体育](http://sports.sohu.com/20060924/n245507648.shtml)
21. [谢杏芳再度击败张宁
    蝉联羽毛球世锦赛女单冠军-搜狐体育](http://sports.sohu.com/20060924/n245507991.shtml)
22. [付海峰/蔡赟完胜
    中国男双17年世锦赛首次登顶-搜狐体育](http://sports.sohu.com/20060924/n245508292.shtml)
23. [女双决赛2比0胜队友
    高崚/黄穗力夺世锦赛首金-搜狐体育](http://sports.sohu.com/20060924/n245500245.shtml)
24. [羽球世锦赛混双决赛
    罗布森/埃姆斯克队友折桂-搜狐体育](http://sports.sohu.com/20060924/n245507394.shtml)
25. [新华网](http://news.xinhuanet.com/world/2006-09/25/content_5130245.htm)
26. [雅虎新闻](https://web.archive.org/web/20070228102537/http://hk.news.yahoo.com/060924/12/1tkfr.html)
27. [新华网](http://news.xinhuanet.com/world/2006-09/25/content_5130256.htm)
28. [新浪网](http://news.sina.com.cn/w/2006-09-25/061010099988s.shtml)
29. [中國時報](http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,110505+112006092500071,00.html)
30. [人民网](http://pic.people.com.cn/GB/31655/4851554.html)
31. [中時電子報](http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,110501+112006092500002,00.html)
32. [自由電子報](http://www.libertytimes.com.tw/2006/new/sep/25/today-fo6.htm)

33. [新华网](http://news.xinhuanet.com/world/2006-09/24/content_5129567.htm)
34. [新华网](http://news.xinhuanet.com/world/2006-09/24/content_5129566.htm)
35. [TOM](http://news.tom.com/2006-09-23/000T/42939832.html)
36. [路透社](http://hk.news.yahoo.com/060923/3/1tj9a.html)
37. [BBC中文網](http://news.bbc.co.uk/chinese/simp/hi/newsid_5370000/newsid_5372900/5372910.stm)
38. [新华网](http://news.xinhuanet.com/world/2006-09/25/content_5133166.htm)
39. [德国之声中文网](http://www.dw-world.de/dw/article/0,,2181476,00.html?maca=chi-newsletter_teaser_front_page-gb2312-291-html)
40. [BBC中文網](http://news.bbc.co.uk/chinese/trad/hi/newsid_5370000/newsid_5371000/5371032.stm)
41. [雅虎新闻](http://hk.news.yahoo.com/060923/3/1tj10.html)
42. [新华网](http://news.xinhuanet.com/world/2006-09/23/content_5127014.htm)
43. [新浪网](http://news.sina.com.cn/w/2006-09-23/090111084245.shtml)
44. [新华网](http://news.xinhuanet.com/world/2006-09/23/content_5127297.htm)
45. [新华网](http://news.xinhuanet.com/world/2006-09/22/content_5122474.htm)
46. [雅虎新闻](http://hk.news.yahoo.com/060921/12/1tgkb.html)
47. [新浪网](http://news.sina.com.cn/w/2006-09-22/062810080642s.shtml)
48. [新华网](http://www.xinhuanet.com/world/zt060920jp/)
49. [路透社](http://hk.news.yahoo.com/060920/3/1tcf8.html)
50. [BBC中文網](http://news.bbc.co.uk/chinese/trad/hi/newsid_5360000/newsid_5363100/5363158.stm)
51. [新华网](http://news.xinhuanet.com/world/2006-09/21/content_5117821.htm)
52. [CCTV](http://news.cctv.com/xwlb/20060921/103609.shtml)
53. [SBS](http://www9.sbs.com.au/theworldnews/region.php?id=131404&region=4)
54. [新浪网](http://tech.sina.com.cn/d/2006-09-20/08451148646.shtml)
55. [BBC新聞](http://news.bbc.co.uk/chinese/trad/hi/newsid_5360000/newsid_5361100/5361186.stm)
56. [新華網](http://news.xinhuanet.com/world/2006-09/20/content_5112102.htm)
57. [大公報](http://www.takungpao.com/news/06/09/20/YM-624785.htm)
58. [雅虎新闻](http://hk.news.yahoo.com/060918/12/1t8ma.html)
59. [雅虎新闻](http://hk.news.yahoo.com/060918/12/1t8yj.html)
60. [雅虎新闻](http://hk.news.yahoo.com/060918/3/1t8xv.html)
61. [新华网](http://news.xinhuanet.com/world/2006-09/19/content_5107299.htm)
62. [新华网](http://news.xinhuanet.com/world/2006-09/17/content_5101148.htm)
63. [雅虎新闻](http://hk.news.yahoo.com/060917/12/1t5t8.html)
64. [新浪新闻](http://news.sina.com.cn/o/2006-09-19/111810055739s.shtml)
65. [明報即時新聞](http://www.mpinews.com/htm/INews/20060916/ta62006a.htm)
66. [新浪网](http://news.sina.com.cn/w/2006-09-18/082211035887.shtml)
67. [Yahoo\!新聞](http://hk.news.yahoo.com/060915/12/1t4cf.html)
68. [東森新聞報](http://www.ettoday.com/2006/09/15/334-1991612.htm)
69. [香港電台新聞](http://www.rthk.org.hk/rthk/news/expressnews/news.htm?expressnews&20060914&55&342123)

70. [香港天文台地震新聞稿](http://www.hko.gov.hk/gts/equake/eqpress_c/eqpress_c.20060914.2042.htm)
71. [珠海資訊網](http://www.zhuhai.gov.cn/waiwang/72621647797944320/20030904/2001826.html)

72. [Yahoo\!新聞](https://web.archive.org/web/20070816090116/http://hk.news.yahoo.com/060914/12/1t1x3.html)
73. [Yahoo\!新聞](http://hk.news.yahoo.com/060914/12/1t28w.html)
74. [新华网](http://news.xinhuanet.com/world/2006-09/15/content_5093148.htm)
75. [新华网](http://news.xinhuanet.com/world/2006-09/14/content_5092791.htm)
76. [東森新聞報](http://www.ettoday.com/2006/09/14/301-1990785.htm)
77. [新华网](http://news.xinhuanet.com/world/2006-09/14/content_5088929.htm)
78. [新浪网](http://news.sina.com.cn/w/2006-09-13/154310010370s.shtml)
79. [雅虎新闻](http://hk.news.yahoo.com/060913/3/1t025.html)
80. [联合国新闻](http://www.un.org/chinese/News/fullstorynews.asp?newsID=6424)
81. [联合国新闻](http://www.un.org/chinese/News/fullstorynews.asp?newsID=6417)
82. [人民网](http://world.people.com.cn/GB/1029/42361/4808130.html)
83. [纽约时报（英文）](http://www.nytimes.com/reuters/world/international-syria.html?hp&ex=1158120000&en=fff9691e68dbbb65&ei=5094&partner=homepage)
84. [明镜（德文）](http://www.spiegel.de/politik/ausland/0,1518,436549,00.html)
85. [世界报（法文）](http://www.lemonde.fr/web/article/0,1-0@2-3218,36-812052@51-812054,0.html)
86. [Yahoo\!新聞](http://hk.news.yahoo.com/060912/12/1swjj.html)
87. [中時電子報](http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,110504+112006091300076,00.html)
88. [自由電子報](http://www.libertytimes.com.tw/2006/new/sep/13/today-p8.htm)

89. [自由電子報](http://www.libertytimes.com.tw/2006/new/sep/13/today-p6.htm)

90. [自由電子報](http://www.libertytimes.com.tw/2006/new/sep/13/today-p7.htm)

91. [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_5340000/newsid_5340800/5340870.stm)
92. [雅虎新闻](http://hk.news.yahoo.com/060911/12/1suvp.html)
93. [新华网](http://news.xinhuanet.com/world/2006-09/12/content_5079369.htm)
94. [雅虎新闻](http://hk.news.yahoo.com/060911/12/1sumt.html)
95. [新华网](http://news.xinhuanet.com/world/2006-09/11/content_5078560.htm)
96. [中時電子報](http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,110505+112006091200075,00.html)
97. [自由電子報](http://www.libertytimes.com.tw/2006/new/sep/12/today-int4.htm)

98. [中時電子報](http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,130505+132006091201097,00.html)
99. [自由電子報](http://www.libertytimes.com.tw/2006/new/sep/13/today-int3.htm)

100. [大紀元時報](http://tw.epochtimes.com/bt/6/9/13/n1451905.htm)
101. [网易](http://sports.163.com/06/0910/23/2QMOPF6000051CDQ.html)
102. [网易](http://news.163.com/06/0910/14/2QLQCA2J0001124J.html)
103. [中時電子報](http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,110505+112006091100060,00.html)
104. [自由電子報](http://www.libertytimes.com.tw/2006/new/sep/11/today-sp1.htm)

105. [聯合新聞網](http://udn.com/NEWS/SPORTS/SPOS1/3510794.shtml)
106. [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_5330000/newsid_5330600/5330650.stm)
107. [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_5330000/newsid_5331600/5331620.stm)
108. [搜狐体育](http://sports.sohu.com/20060910/n245253472.shtml)
109. [民視](http://tw.news.yahoo.com/article/url/d/a/060909/11/3eeh.html)
110. [新浪](http://news.sina.com.cn/c/2006-09-09/164410966004.shtml)
111. [大紀元時報](http://tw.epochtimes.com/bt/6/9/9/n1449152.htm)
112. [東森新聞報](http://www.ettoday.com/2006/09/10/334-1989197.htm)
113. [自由電子報](http://www.libertytimes.com.tw/2006/new/sep/10/today-int3.htm)

114. [東森新聞報](http://www.ettoday.com/2006/09/08/301-1988218.htm)
115. [中時電子報](http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,110101+112006090800769,00.html)
116. [自由電子報](http://www.libertytimes.com.tw/2006/new/sep/9/today-fo3.htm)

117. [中時電子報](http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,110504+112006090900064,00.html)
118. [東森新聞報](http://www.ettoday.com/2006/09/09/341-1988678.htm)
119. [BBC中文网](http://news.bbc.co.uk/chinese/trad/hi/newsid_5320000/newsid_5324500/5324500.stm)
120. [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_5320000/newsid_5323400/5323422.stm)
121. [新华网](http://news.xinhuanet.com/world/2006-09/08/content_5063512.htm)
122. [新浪网](http://news.sina.com.cn/w/2006-09-08/06419966410s.shtml)
123. [新浪网](http://news.sina.com.cn/w/2006-09-08/03079964638s.shtml)
124. [中時電子報](http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,110505+112006090800074,00.html)
125. [中時電子報](http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,110504+112006090900058,00.html)
126. [Yahoo\!新聞](http://hk.news.yahoo.com/060906/12/1sk5e.html)
127. [中華日報](http://news01.cdns.com.tw/20060907/news/zyxw/733250002006090620472735.htm)
128. [自由電子報](http://www.libertytimes.com.tw/2006/new/sep/7/today-int4.htm)

129. [東森新聞報](http://www.ettoday.com/2006/09/07/334-1987956.htm)
130. [自由電子報](http://www.libertytimes.com.tw/2006/new/sep/8/today-int3.htm)

131. [中時電子報](http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,110505+112006090800078,00.html)
132. [中時電子報](http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,110505+112006090800081,00.html)
133. [大公報網](http://www.takungpao.com/news/06/09/05/ZM-617972.htm)
134. [亞洲時報](http://www.atchinese.com/index.php?option=com_content&task=view&id=21629&Itemid=33)
135. [中國廣播公司](http://www.bcc.com.tw/news/newsview.asp?cde=291711)
136. [東方網](http://news.eastday.com/eastday/node81741/node81762/node159379/u1a2298477.html)
137. [亞洲電視](http://app.hkatvnews.com/content/2006/09/05/atvnews_94134.html)
138. [星島日報](http://www.rti.org.tw/News/NewsContentHome.aspx?NewsID=43688&t=1)

139. [香港商業電台](http://pshweb02.881903.com/apps/news/html/news/20060905/2006090512274529400.htm)
140. [BBC新聞](http://news.bbc.co.uk/chinese/trad/hi/newsid_5310000/newsid_5314800/5314820.stm)
141. [中時電子報](http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,110505+112006090700079,00.html)
142. [Yahoo\!新聞](http://hk.news.yahoo.com/060906/3/1sk9k.html)
143. [Yahoo\!新聞](https://web.archive.org/web/20070122161157/http://hk.news.yahoo.com/060905/12/1siok.html)
144. [Yahoo\!新聞](http://hk.news.yahoo.com/060905/60/1si4s.html)
145. [新华网](http://news.xinhuanet.com/sports/2006-09/04/content_5045614.htm)
146. [中新网](http://news.xinhuanet.com/world/2006-09/04/content_5045624.htm)
147. [BBC中文网](http://news.bbc.co.uk/chinese/simp/hi/newsid_5310000/newsid_5310000/5310006.stm)
148. [新浪网](http://tech.sina.com.cn/d/focus/smart-1/index.shtml)
149. [番薯藤](http://sports.yam.com/show.php?id=0000083994)
150. [聯合新聞網](http://udn.com/NEWS/SPORTS/SPOS2/3500896.shtml)
151. [中時電子報](http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,11051204+112006090400764,00.html)
152. [東森新聞報](http://www.ettoday.com/2006/09/04/341-1986384.htm)
153. [Yahoo\! 新聞](http://hk.news.yahoo.com/060902/3/1sd7c.html)
154. [新华网](http://news.xinhuanet.com/world/2006-09/01/content_5037438.htm)
155. [自由電子報](http://www.libertytimes.com.tw/2006/new/sep/2/today-int2.htm)

156. [中時電子報](http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,110504+112006090200071,00.html)
157. [中時電子報](http://news.chinatimes.com/Chinatimes/newslist/newslist-content/0,3546,110504+112006090200066,00.html)
158. [自由電子報](http://www.libertytimes.com.tw/2006/new/sep/2/today-int1.htm)

159. [東森新聞網](http://www.ettoday.com/2006/09/02/334-1985753.htm)
160. [東森新聞網](http://www.ettoday.com/2006/09/02/334-1985762.htm)