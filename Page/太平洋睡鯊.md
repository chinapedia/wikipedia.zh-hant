**太平洋睡鯊**（[拉丁文](../Page/拉丁文.md "wikilink")[學名](../Page/學名.md "wikilink")：***Somniosus
pacificus***）是[軟骨魚綱](../Page/軟骨魚綱.md "wikilink")[角鯊目](../Page/角鯊目.md "wikilink")[梦棘鲛科的睡鯊](../Page/梦棘鲛科.md "wikilink")。

## 分布

出沒於北緯70度到南緯47度之間的[大陸棚和水溫較高的海域](../Page/大陸棚.md "wikilink")，其活動範圍可深至海底2,000米之處。其體長可達4.4米（14呎），但亦有一些文獻指出其身長最多可達7米。1990年，一隻巨型的太平洋睡鯊在[日本東京海灣被食餌引誘而被捕獲](../Page/日本.md "wikilink")，期間被攝錄了下來。那頭鯊魚估計長約7米（23呎），牠的體長可以跟[大白鯊媲美](../Page/大白鯊.md "wikilink")，甚至可能比大白鯊更大。一些報紙，尤其是[太陽報甚至不合理地說](../Page/太陽報.md "wikilink")，那根本就是一隻「[巨牙鯊](../Page/巨牙鯊.md "wikilink")」。

## 生態

太平洋睡鯊吃魚類、[章魚](../Page/章魚.md "wikilink")、[烏賊](../Page/烏賊.md "wikilink")、[蟹](../Page/蟹.md "wikilink")、[梭尾螺](../Page/梭尾螺.md "wikilink")（triton）、[海豹和](../Page/海豹.md "wikilink")[腐肉](../Page/腐肉.md "wikilink")。活物或新鮮者有很高含量的[尿素](../Page/尿素.md "wikilink")，以及很多軟骨魚類的肌肉和體液內都含有的[三甲胺氧化物](../Page/三甲胺氧化物.md "wikilink")（trimethylamine
oxide）\[1\]。在經過其他動物的消化系統時，[三甲胺](../Page/三甲胺.md "wikilink")（trimethylamine）會分離出來，對動物或人造成的效果就像喝醉了酒一般。動物如果不小心吃了太平洋睡鯊的鮮肉，由於[神經毒素](../Page/神經毒素.md "wikilink")（neurotoxins）的影響，牠們會無法站立起來。牠和[抹香鯨是僅有的兩種會吃](../Page/抹香鯨.md "wikilink")[大王烏賊的動物](../Page/大王烏賊.md "wikilink")，這可從其腹中之物得知。由於一隻7米長的鯊魚要捕獵一隻12到14米長的大王烏賊並不太可能，因此人們推測太平洋睡鯊吃的是大王烏賊的腐肉，而不是自己活活捕捉一隻。\[2\]

牠們屬卵胎生之動物，一胎產300隻幼鯊。一隻剛出生的幼鯊僅長約42公分，或甚至比這還要更短。

## 天敵

於2009年一次[虎鯨的獵殺後](../Page/虎鯨.md "wikilink")，研究人員發現殘骸至少來自16條太平洋睡鯊。

## 參看

  - [小頭睡鯊](../Page/小頭睡鯊.md "wikilink")

## 参考文献

  -
  -
  -
## 外部連結

  - [「捕獲巨型的烏賊掠食者」（New giant squid predator
    found）](http://news.bbc.co.uk/1/hi/sci/tech/3370019.stm) -
    2004年1月8日，英國廣播公司新聞

[pacificus](../Category/睡鯊屬.md "wikilink")

1.  [三甲胺氧化物](http://bk.51player.com/view/1089275.htm) - 百科探秘
2.  <http://news.bbc.co.uk/2/hi/science/nature/3370019.stm>