**吏部侍郎**，別名吏侍、二銓、小天官、小宰、天官之貳、天官貳卿、倅天官、藻鏡，掌管文官任免、考課、升降、勛封、調動等事務。「吏部」乃六部之一，東漢始置吏曹，改自尚書常侍曹，魏晉以後稱吏部。隋唐列為“[尚書省](../Page/尚書省.md "wikilink")”六部之首。「侍郎」，相當於今日的副部長、次長。尚書是部長。今日，这一职位相当于[中华民国銓敘部](../Page/中华民国銓敘部.md "wikilink")、[考選部次長](../Page/考選部.md "wikilink")、[中共中央组织部副部长或](../Page/中共中央组织部.md "wikilink")[人力资源和社会保障部副部长](../Page/人力资源和社会保障部.md "wikilink")、[国家公务员局局长](../Page/国家公务员局.md "wikilink")。

## 历代

### 唐朝

  - 高祖

[殷开山](../Page/殷开山.md "wikilink") [杨师道](../Page/杨师道.md "wikilink")
[张锐](../Page/张锐.md "wikilink")

  - 太宗

[韩仲良](../Page/韩仲良.md "wikilink") [温彦博](../Page/温彦博.md "wikilink")
[刘林甫](../Page/刘林甫.md "wikilink") [韦挺](../Page/韦挺.md "wikilink")
[杨纂](../Page/杨纂.md "wikilink") [唐皎](../Page/唐皎.md "wikilink")
[苏勗](../Page/苏勗.md "wikilink") [高季辅](../Page/高季辅.md "wikilink")

  - 高宗

[唐临](../Page/唐临.md "wikilink") [高敬言](../Page/高敬言.md "wikilink")
[刘祥道](../Page/刘祥道.md "wikilink") [郝处俊](../Page/郝处俊.md "wikilink")
[杨思玄](../Page/杨思玄.md "wikilink") [李安期](../Page/李安期.md "wikilink")
[卢承业](../Page/卢承业.md "wikilink") [赵仁本](../Page/赵仁本.md "wikilink")
[李敬玄](../Page/李敬玄.md "wikilink") [裴行俭](../Page/裴行俭.md "wikilink")
[马载](../Page/马载.md "wikilink") [元大士](../Page/元大士.md "wikilink")
[韦万石](../Page/韦万石.md "wikilink") [崔元奖](../Page/崔元奖.md "wikilink")
[魏玄同](../Page/魏玄同.md "wikilink") [裴敬彝](../Page/裴敬彝.md "wikilink")
[魏克己](../Page/魏克己.md "wikilink")

  - 武后

[魏克己](../Page/魏克己.md "wikilink") [邓玄挺](../Page/邓玄挺.md "wikilink")
[郭待举](../Page/郭待举.md "wikilink") [张楚金](../Page/张楚金.md "wikilink")
[魏玄同](../Page/魏玄同.md "wikilink") [孟允中](../Page/孟允中.md "wikilink")
[范履冰](../Page/范履冰.md "wikilink") [李景谌](../Page/李景谌.md "wikilink")
[任令晖](../Page/任令晖.md "wikilink") [姚璹](../Page/姚璹.md "wikilink")
[李至远](../Page/李至远.md "wikilink") [韦承庆](../Page/韦承庆.md "wikilink")
[许子儒](../Page/许子儒.md "wikilink") [石抱忠](../Page/石抱忠.md "wikilink")
[刘奇](../Page/刘奇.md "wikilink") [何彦先](../Page/何彦先.md "wikilink")
[崔冬日](../Page/崔冬日.md "wikilink") [张询古](../Page/张询古.md "wikilink")
[王勮](../Page/王勮.md "wikilink") [李峤](../Page/李峤.md "wikilink")
[苏味道](../Page/苏味道.md "wikilink") [陆元方](../Page/陆元方.md "wikilink")
[吉顼](../Page/吉顼.md "wikilink") [郑杲](../Page/郑杲.md "wikilink")
[顾琮](../Page/顾琮.md "wikilink") [张锡](../Page/张锡.md "wikilink")
[房颖叔](../Page/房颖叔.md "wikilink") [李迥秀](../Page/李迥秀.md "wikilink")
[崔玄暐](../Page/崔玄暐.md "wikilink") [韦嗣立](../Page/韦嗣立.md "wikilink")
[张敬之](../Page/张敬之.md "wikilink")

  - 中宗

[刘宪](../Page/刘宪.md "wikilink") [李峤](../Page/李峤.md "wikilink")
[韦嗣立](../Page/韦嗣立.md "wikilink") [宋璟](../Page/宋璟.md "wikilink")
[司马锽](../Page/司马锽.md "wikilink") [魏知古](../Page/魏知古.md "wikilink")
[萧至忠](../Page/萧至忠.md "wikilink") [元希声](../Page/元希声.md "wikilink")
[岑羲](../Page/岑羲.md "wikilink") [崔湜](../Page/崔湜.md "wikilink")
[薛稷](../Page/薛稷.md "wikilink") [郑愔](../Page/郑愔.md "wikilink")
[李元恭](../Page/李元恭.md "wikilink") [卢藏用](../Page/卢藏用.md "wikilink")

  - 睿宗

[卢藏用](../Page/卢藏用.md "wikilink") [崔湜](../Page/崔湜.md "wikilink")
[卢从愿](../Page/卢从愿.md "wikilink") [李乂](../Page/李乂.md "wikilink")
[赵彦昭](../Page/赵彦昭.md "wikilink") [马怀素](../Page/马怀素.md "wikilink")

  - 玄宗

[卢从愿](../Page/卢从愿.md "wikilink") [吴道师](../Page/吴道师.md "wikilink")
[崔日用](../Page/崔日用.md "wikilink") [卢怀慎](../Page/卢怀慎.md "wikilink")
[李朝隐](../Page/李朝隐.md "wikilink") [姜晦](../Page/姜晦.md "wikilink")
[裴漼](../Page/裴漼.md "wikilink") [慕容珣](../Page/慕容珣.md "wikilink")
[魏奉古](../Page/魏奉古.md "wikilink") [王丘](../Page/王丘.md "wikilink")
[陆象先](../Page/陆象先.md "wikilink") [杨滔](../Page/杨滔.md "wikilink")
[王易从](../Page/王易从.md "wikilink") [韩思复](../Page/韩思复.md "wikilink")
[许景先](../Page/许景先.md "wikilink") [李元纮](../Page/李元纮.md "wikilink")
[苏颋](../Page/苏颋.md "wikilink") [韦抗](../Page/韦抗.md "wikilink")
[卢从愿](../Page/卢从愿.md "wikilink") [徐坚](../Page/徐坚.md "wikilink")
[宇文融](../Page/宇文融.md "wikilink") [崔琳](../Page/崔琳.md "wikilink")
[崔沔](../Page/崔沔.md "wikilink") [韦虚心](../Page/韦虚心.md "wikilink")
[贾曾](../Page/贾曾.md "wikilink") [蒋钦绪](../Page/蒋钦绪.md "wikilink")
[苏晋](../Page/苏晋.md "wikilink") [齐澣](../Page/齐澣.md "wikilink")
[韩朝宗](../Page/韩朝宗.md "wikilink") [李林甫](../Page/李林甫.md "wikilink")
[刘彤](../Page/刘彤.md "wikilink") [席豫](../Page/席豫.md "wikilink")
[严挺之](../Page/严挺之.md "wikilink") [裴宽](../Page/裴宽.md "wikilink")
[裴思义](../Page/裴思义.md "wikilink") [苗晋卿](../Page/苗晋卿.md "wikilink")
[徐安贞](../Page/徐安贞.md "wikilink") [宋遥](../Page/宋遥.md "wikilink")
[陆景融](../Page/陆景融.md "wikilink") [韦陟](../Page/韦陟.md "wikilink")
[李彭年](../Page/李彭年.md "wikilink") [达奚珣](../Page/达奚珣.md "wikilink")
[封希颜](../Page/封希颜.md "wikilink") [韦见素](../Page/韦见素.md "wikilink")
[班景倩](../Page/班景倩.md "wikilink") [韦镒](../Page/韦镒.md "wikilink")
[宋昱](../Page/宋昱.md "wikilink") [张倚](../Page/张倚.md "wikilink")
[蒋洌](../Page/蒋洌.md "wikilink")

  - 肃宗

[崔漪](../Page/崔漪.md "wikilink") [庾光先](../Page/庾光先.md "wikilink")
[崔涣](../Page/崔涣.md "wikilink") [苏震](../Page/苏震.md "wikilink")
[李暐](../Page/李暐.md "wikilink") [崔器](../Page/崔器.md "wikilink")
[李麟](../Page/李麟.md "wikilink") [李揖](../Page/李揖.md "wikilink")
[崔器](../Page/崔器.md "wikilink") [裴遵庆](../Page/裴遵庆.md "wikilink")
[杜鸿渐](../Page/杜鸿渐.md "wikilink")

  - 代宗

[张孚](../Page/张孚.md "wikilink") [李岘](../Page/李岘.md "wikilink")
[颜真卿](../Page/颜真卿.md "wikilink") [崔涣](../Page/崔涣.md "wikilink")
[严武](../Page/严武.md "wikilink") [王翊](../Page/王翊.md "wikilink")
[畅璀](../Page/畅璀.md "wikilink") [李季卿](../Page/李季卿.md "wikilink")
[王延昌](../Page/王延昌.md "wikilink") [杨绾](../Page/杨绾.md "wikilink")
[徐浩](../Page/徐浩.md "wikilink") [薛邕](../Page/薛邕.md "wikilink")
[杨炎](../Page/杨炎.md "wikilink") [韦肇](../Page/韦肇.md "wikilink")
[薛述](../Page/薛述.md "wikilink") [裴淑](../Page/裴淑.md "wikilink")
[崔祐甫](../Page/崔祐甫.md "wikilink")

  - 德宗

[崔祐甫](../Page/崔祐甫.md "wikilink") [邵说](../Page/邵说.md "wikilink")
[房宗偃](../Page/房宗偃.md "wikilink") [张镒](../Page/张镒.md "wikilink")
[关播](../Page/关播.md "wikilink") [赵涓](../Page/赵涓.md "wikilink")
[卢翰](../Page/卢翰.md "wikilink") [班宏](../Page/班宏.md "wikilink")
[李纾](../Page/李纾.md "wikilink") [刘滋](../Page/刘滋.md "wikilink")
[崔纵](../Page/崔纵.md "wikilink") [裴谞](../Page/裴谞.md "wikilink")
[崔倕](../Page/崔倕.md "wikilink") [吉中孚](../Page/吉中孚.md "wikilink")
[杜黄裳](../Page/杜黄裳.md "wikilink") [郑珣瑜](../Page/郑珣瑜.md "wikilink")
[奚陟](../Page/奚陟.md "wikilink") [顾少连](../Page/顾少连.md "wikilink")
[袁滋](../Page/袁滋.md "wikilink") [郑余庆](../Page/郑余庆.md "wikilink")
[韦夏卿](../Page/韦夏卿.md "wikilink") [杨於陵](../Page/杨於陵.md "wikilink")
[赵宗儒](../Page/赵宗儒.md "wikilink")

  - 顺宗

[赵宗儒](../Page/赵宗儒.md "wikilink") [崔邠](../Page/崔邠.md "wikilink")

  - 宪宗

[许孟容](../Page/许孟容.md "wikilink") [赵宗儒](../Page/赵宗儒.md "wikilink")
[崔邠](../Page/崔邠.md "wikilink") [权德舆](../Page/权德舆.md "wikilink")
[张弘靖](../Page/张弘靖.md "wikilink") [裴佶](../Page/裴佶.md "wikilink")
[杨於陵](../Page/杨於陵.md "wikilink") [韦贯之](../Page/韦贯之.md "wikilink")
[刘伯刍](../Page/刘伯刍.md "wikilink") [韦顗](../Page/韦顗.md "wikilink")
[吕元膺](../Page/吕元膺.md "wikilink") [王涯](../Page/王涯.md "wikilink")

  - 穆宗

[李建](../Page/李建.md "wikilink") [崔群](../Page/崔群.md "wikilink")
[孔戣](../Page/孔戣.md "wikilink") [丁公著](../Page/丁公著.md "wikilink")
[柳公绰](../Page/柳公绰.md "wikilink") [窦易直](../Page/窦易直.md "wikilink")
[韩愈](../Page/韩愈.md "wikilink") [李程](../Page/李程.md "wikilink")

  - 敬宗

[韩愈](../Page/韩愈.md "wikilink") [李程](../Page/李程.md "wikilink")
[崔从](../Page/崔从.md "wikilink") [韦弘景](../Page/韦弘景.md "wikilink")
[韦顗](../Page/韦顗.md "wikilink") [王起](../Page/王起.md "wikilink")
[庾承宣](../Page/庾承宣.md "wikilink")

  - 文宗

[王起](../Page/王起.md "wikilink") [庾承宣](../Page/庾承宣.md "wikilink")
[杨嗣复](../Page/杨嗣复.md "wikilink") [丁公著](../Page/丁公著.md "wikilink")
[李宗闵](../Page/李宗闵.md "wikilink") [王璠](../Page/王璠.md "wikilink")
[庾敬休](../Page/庾敬休.md "wikilink") [高釴](../Page/高釴.md "wikilink")
[郑澣](../Page/郑澣.md "wikilink") [李固言](../Page/李固言.md "wikilink")
[沈传师](../Page/沈传师.md "wikilink") [李虞仲](../Page/李虞仲.md "wikilink")
[李汉](../Page/李汉.md "wikilink") [郑肃](../Page/郑肃.md "wikilink")
[崔琯](../Page/崔琯.md "wikilink") [崔郸](../Page/崔郸.md "wikilink")
[高锴](../Page/高锴.md "wikilink") [归融](../Page/归融.md "wikilink")
[陈夷行](../Page/陈夷行.md "wikilink") [杨汝士](../Page/杨汝士.md "wikilink")

  - 武宗

[孙简](../Page/孙简.md "wikilink") [韦温](../Page/韦温.md "wikilink")
[崔蠡](../Page/崔蠡.md "wikilink") [高铢](../Page/高铢.md "wikilink")
[崔珙](../Page/崔珙.md "wikilink") [柳仲郢](../Page/柳仲郢.md "wikilink")

  - 宣宗

[崔璪](../Page/崔璪.md "wikilink") [封敖](../Page/封敖.md "wikilink")
[孔温业](../Page/孔温业.md "wikilink") [蒋系](../Page/蒋系.md "wikilink")
[裴谂](../Page/裴谂.md "wikilink") [卢懿](../Page/卢懿.md "wikilink")
[周敬复](../Page/周敬复.md "wikilink") [柳仲郢](../Page/柳仲郢.md "wikilink")
[郑涯](../Page/郑涯.md "wikilink") [韦有翼](../Page/韦有翼.md "wikilink")
[郑颢](../Page/郑颢.md "wikilink")

  - 懿宗

[郑薰](../Page/郑薰.md "wikilink") [蕭倣](../Page/蕭倣_\(唐朝\).md "wikilink")
[韦澳](../Page/韦澳.md "wikilink") [郑处诲](../Page/郑处诲.md "wikilink")
[郑从谠](../Page/郑从谠.md "wikilink")
[王铎](../Page/王铎_\(唐朝\).md "wikilink")
[卢匡](../Page/卢匡.md "wikilink") [李蔚](../Page/李蔚.md "wikilink")
[韦荷](../Page/韦荷.md "wikilink") [杨知温](../Page/杨知温.md "wikilink")
[于德孙](../Page/于德孙.md "wikilink") [李当](../Page/李当.md "wikilink")
[归仁晦](../Page/归仁晦.md "wikilink") [独孤云](../Page/独孤云.md "wikilink")
[王讽](../Page/王讽.md "wikilink")

  - 僖宗

[韦荷](../Page/韦荷.md "wikilink") [郑畋](../Page/郑畋.md "wikilink")
[裴坦](../Page/裴坦.md "wikilink") [张禓](../Page/张禓.md "wikilink")
[崔蕘](../Page/崔蕘.md "wikilink") [孔晦](../Page/孔晦.md "wikilink")
[崔沆](../Page/崔沆.md "wikilink") [崔澹](../Page/崔澹.md "wikilink")
[孔纬](../Page/孔纬.md "wikilink") [裴瓒](../Page/裴瓒.md "wikilink")
[孙纬](../Page/孙纬.md "wikilink") [卢告](../Page/卢告.md "wikilink")
[张读](../Page/张读.md "wikilink")

  - 昭宗

[柳玭](../Page/柳玭.md "wikilink") [徐彦若](../Page/徐彦若.md "wikilink")
[崔胤](../Page/崔胤.md "wikilink") [张袆](../Page/张袆.md "wikilink")
[赵崇](../Page/赵崇.md "wikilink") [杨涉](../Page/杨涉.md "wikilink")
[裴枢](../Page/裴枢.md "wikilink") [独孤损](../Page/独孤损.md "wikilink")
[卢光启](../Page/卢光启.md "wikilink") [杨钜](../Page/杨钜.md "wikilink")
[赵光逢](../Page/赵光逢.md "wikilink") [薛贻矩](../Page/薛贻矩.md "wikilink")

  - 昭宣帝

[赵光逢](../Page/赵光逢.md "wikilink") [薛贻矩](../Page/薛贻矩.md "wikilink")
[杨涉](../Page/杨涉.md "wikilink") [薛廷珪](../Page/薛廷珪.md "wikilink")\[1\]

## 参考文献

## 參見

  - [吏部](../Page/吏部.md "wikilink")：[吏部尚書](../Page/吏部尚書.md "wikilink")

{{-}}

[吏部侍郎](../Category/吏部侍郎.md "wikilink")
[Category:六部侍郎](../Category/六部侍郎.md "wikilink")
[Category:吏部官员](../Category/吏部官员.md "wikilink")

1.  严耕望《唐仆尚丞郎表》