**馬克·福斯特**（，）是一位[德国](../Page/德国.md "wikilink")[電影導演和](../Page/電影導演.md "wikilink")[編劇](../Page/編劇.md "wikilink")。

## 经历

馬克·福斯特父母分別為德國醫師與建築師，童年時隨父母搬遷至[瑞士](../Page/瑞士.md "wikilink")[達沃斯長居](../Page/達沃斯.md "wikilink")，雖然在[瑞士長大](../Page/瑞士.md "wikilink")，不過依然是[德國公民](../Page/德國.md "wikilink")。福斯特童年時觀看的第一部的戲院電影是12歲時，[法蘭西斯·柯波拉作品](../Page/法蘭西斯·柯波拉.md "wikilink")《[現代啟示錄](../Page/現代啟示錄.md "wikilink")》，受此啟發，福斯特便夢想朝[導演之路前進](../Page/導演.md "wikilink")。

1990年，福斯特搬到[紐約進入](../Page/紐約.md "wikilink")[紐約大學電影學校就讀](../Page/紐約大學.md "wikilink")，拍攝了幾隻紀錄片。三年後，搬遷至[好萊塢](../Page/好萊塢.md "wikilink")，並且拍攝了實驗性質的低成本影片《Longers》。而福斯特的第一部電影，則是《Everything
Put Together》，在當年曾獲得[日舞影展大獎提名](../Page/日舞影展.md "wikilink")。

《[擁抱豔陽天](../Page/擁抱豔陽天.md "wikilink")》則是讓福斯特聲名大噪的電影，這部由[荷莉·貝瑞主演的電影](../Page/荷莉·貝瑞.md "wikilink")，在當年獲得2項[奧斯卡金像獎提名](../Page/奧斯卡金像獎.md "wikilink")，而[荷莉·貝瑞也成為首位](../Page/荷莉·貝瑞.md "wikilink")[封后的黑人演員](../Page/奧斯卡最佳女主角獎.md "wikilink")。福斯特的下部作品《[尋找新樂園](../Page/尋找新樂園.md "wikilink")》，改編自[彼得潘作者](../Page/彼得潘.md "wikilink")[詹姆斯·馬修·巴里的故事](../Page/詹姆斯·馬修·巴里.md "wikilink")，獲得了7項[奧斯卡金像獎提名](../Page/奧斯卡金像獎.md "wikilink")，包括[最佳影片](../Page/奧斯卡最佳影片獎.md "wikilink")，而福斯特本身，也獲得[金球獎](../Page/金球獎.md "wikilink")、[英國影藝學院獎](../Page/BAFTA.md "wikilink")、[美國導演工會獎的獎項提名](../Page/美國導演工會獎.md "wikilink")。

2005年[驚悚片作品](../Page/驚悚片.md "wikilink")《[離魂](../Page/離魂.md "wikilink")》則不被影評所認同，儘管有複雜的視覺效果和[伊旺·麥奎格](../Page/伊旺·麥奎格.md "wikilink")、[娜歐蜜·華茲演出](../Page/娜歐蜜·華茲.md "wikilink")，但在北美票房僅僅只有400萬美元收入而已。福斯特下部作品《[口白人生](../Page/口白人生.md "wikilink")》，則在票房大獲成功，全球賺進5400萬美元的票房收入。\[1\]

福斯特接下來作品為改編自同名暢銷小說的《[追風箏的孩子](../Page/追風箏的孩子.md "wikilink")》，這部作品由阿富汗裔美國人[卡勒德·胡賽尼所著](../Page/卡勒德·胡賽尼.md "wikilink")，而電影則獲得了1項[奧斯卡提名](../Page/奧斯卡.md "wikilink")。

福斯特在2008年執導第22部[007系列電影](../Page/007.md "wikilink")《[007量子危機](../Page/007量子危機.md "wikilink")》，2008年年底上映\[2\]\[3\]，全球收获5.86亿美圆\[4\]。

## 電影作品

| 年度   | 片名                                         | 奧斯卡提名 | 奧斯卡獲獎 |
| ---- | ------------------------------------------ | ----- | ----- |
| 1995 | 《Longers》                                  |       |       |
| 2000 | 《Everything Put Together》                  |       |       |
| 2001 | 《[擁抱豔陽天](../Page/擁抱豔陽天.md "wikilink")》     | 2     | 1     |
| 2004 | 《[尋找新樂園](../Page/尋找新樂園.md "wikilink")》     | 7     | 1     |
| 2005 | 《[離魂](../Page/離魂.md "wikilink")》           |       |       |
| 2006 | 《[口白人生](../Page/口白人生.md "wikilink")》       |       |       |
| 2007 | 《[追風箏的孩子](../Page/追風箏的孩子.md "wikilink")》   | 1     |       |
| 2008 | 《[007量子危機](../Page/007量子危機.md "wikilink")》 |       |       |
| 2011 | 《[機槍教父](../Page/機槍教父.md "wikilink")》       |       |       |
| 2013 | 《[地球末日戰](../Page/地球末日戰.md "wikilink")》     |       |       |
| 2016 | 《[盲女驚心](../Page/盲女驚心.md "wikilink")》       |       |       |
| 2018 | 《[摯友維尼](../Page/摯友維尼.md "wikilink")》       |       |       |

## 参考资料

## 外部連結

  -
  -
  - ["Straight on Till
    Morning"](http://www.alternet.org/movies/20454/?page=entire) Marc
    Forster interviewed at AlterNet.

[Category:1969年出生](../Category/1969年出生.md "wikilink")
[Category:德国电影导演](../Category/德国电影导演.md "wikilink")
[Category:德国电影监制](../Category/德国电影监制.md "wikilink")
[Category:德国编剧](../Category/德国编剧.md "wikilink")
[Category:纽约市人](../Category/纽约市人.md "wikilink")
[Category:在世人物](../Category/在世人物.md "wikilink")
[Category:在美国的德国人](../Category/在美国的德国人.md "wikilink")
[Category:男性编剧](../Category/男性编剧.md "wikilink")

1.  [Box office data
    at](http://www.boxofficemojo.com/movies/?id=strangerthanfiction.htm)
    [Box Office Mojo](../Page/Box_Office_Mojo.md "wikilink")
2.
3.
4.  [Box office data
    at](http://boxofficemojo.com/movies/?id=jamesbond22.htm) [Box Office
    Mojo](../Page/Box_Office_Mojo.md "wikilink")