**Ubuntu**（[國際音標](../Page/國際音標.md "wikilink")：，）\[1\]\[2\]是以桌面應用為主的[Linux發行版](../Page/Linux發行版.md "wikilink")，Ubuntu由[Canonical公司發布](../Page/Canonical公司.md "wikilink")，他們提供商業支持\[3\]。它是基於自由軟件，其名稱來自[非洲南部](../Page/非洲.md "wikilink")[祖魯語或](../Page/祖魯語.md "wikilink")[科薩語的](../Page/科薩語.md "wikilink")「ubuntu」一詞（譯為[烏班圖](../Page/烏班圖.md "wikilink")），意思是「人性」、「我的存在是因為大家的存在」，\[4\]是非洲傳統的一種價值觀。

Ubuntu的開發由英國Canonical有限公司主導，南非企業家[Mark
Shuttleworth所創立](../Page/Mark_Shuttleworth.md "wikilink")。Canonical通過銷售與Ubuntu相關的技術支持和其他服務來產生收益。\[5\]\[6\]Ubuntu項目公開承諾開源軟件開發的原則；鼓勵人們使用自由軟件，研究它的運作原理，改進和分發。\[7\]\[8\]

Ubuntu是著名的Linux發行版之一，它也是目前最多使用者的Linux版本，用戶數超過10億人(含伺服器、手機與其分支版本)\[9\]。

## 概述

[Wikipedia_Mainpage_zhtw_with_Firefox25_on_Ubuntu1310.ogv](https://zh.wikipedia.org/wiki/File:Wikipedia_Mainpage_zhtw_with_Firefox25_on_Ubuntu1310.ogv "fig:Wikipedia_Mainpage_zhtw_with_Firefox25_on_Ubuntu1310.ogv")瀏覽[中文维基百科首頁](../Page/中文维基百科.md "wikilink")\]\]

Ubuntu基於[Debian發行版和](../Page/Debian.md "wikilink")[GNOME桌面環境](../Page/GNOME.md "wikilink")，與Debian的不同在於它每6個月會發佈一個新版本（即每年的四月与十月），每2年發布一個LTS長期支援版本。
普通的桌面版可以獲得發布後18個月内的支援，標為LTS（長期支持）的桌面版可以獲得更長時間的支援。\[10\]例如，Ubuntu 8.04
LTS（代號Hardy Heron），其桌面應用系列可以獲得為期3年的技術支援，伺服器版可以獲得為期5年的技術支援\[11\]。而自Ubuntu
12.04 LTS開始，桌面版和伺服器版均可獲得為期5年的技術支援。2013年3月有消息指出，Ubuntu計劃在4月25日Ubuntu
13.04發布後，將非LTS版本的支援時間自18個月縮短至9個月，並採用[滾動發布模式](../Page/滾動發布.md "wikilink")，允許開發者在不升級整個發行版的情况下升級單個核心套件。\[12\]

Ubuntu的目標在於為一般用戶提供一個最新同時又相當穩定，主要以[自由軟體建構而成的作業系統](../Page/自由軟體.md "wikilink")。Ubuntu目前具有龐大的社群力量支持\[13\]\[14\]，用戶可以方便地從社群獲得幫助。

Ubuntu在Ubuntu
12.04的發布頁面上使用了“**友幫拓**”作為官方譯名\[15\]。之前一些中文使用者曾使用班圖、烏班圖、烏斑兔、烏幫圖、笨兔\[16\]等作為非官方譯名。

Ubuntu在2013年推出了新產品[Ubuntu Phone
OS](https://web.archive.org/web/20161018002125/https://www.ubuntu.com/phone)和[Ubuntu
Tablet](https://web.archive.org/web/20161018235955/https://www.ubuntu.com/tablet)，希望统一桌面設備和[移動設備的畫面](../Page/移動設備.md "wikilink")。

## 歷史與發展過程

[Mark_Shuttleworth_NASA.jpg](https://zh.wikipedia.org/wiki/File:Mark_Shuttleworth_NASA.jpg "fig:Mark_Shuttleworth_NASA.jpg")\]\]
Ubuntu由[馬克·舍特爾沃斯創立](../Page/馬克·沙特爾沃思.md "wikilink")，其首個版本—4.10\[17\]發佈於2004年10月20日，它以[Debian為開發藍本](../Page/Debian.md "wikilink")。\[18\]與Debian穩健的升級策略不同，Ubuntu每六個月便會發佈一個新版，以便人們即時地獲取和使用新軟體。Ubuntu的開發目的是為了使個人電腦變得簡單易用，同時也提供針對企業應用的[伺服器版本](../Page/伺服器.md "wikilink")。Ubuntu的每個新版本均會包含當時最新的[GNOME桌面環境](../Page/GNOME.md "wikilink")，通常在[GNOME發佈新版本後一個月內發行](../Page/GNOME.md "wikilink")。與其它基於Debian的Linux發行版，如[MEPIS](../Page/MEPIS.md "wikilink")、[Xandros](../Page/Xandros.md "wikilink")、[Linspire](../Page/Linspire.md "wikilink")、[Progeny和](../Page/Progeny_Componentized_Linux.md "wikilink")[Libranet等相比](../Page/Libranet.md "wikilink")，Ubuntu更接近Debian的開發理念，它主要使用[自由](../Page/自由軟體.md "wikilink")、[開源的軟體](../Page/開放原始碼.md "wikilink")\[19\]，而其它發行版往往會附帶很多閉源的軟體。

Ubuntu建基於Debian的不穩定分支：不論其軟體格式（[deb](../Page/deb.md "wikilink")）還是軟體管理與安裝系統（[Debian
Apt](../Page/高級包裝工具.md "wikilink")）。Ubuntu的開發者會把對軟體的修改即時反饋給Debian社群，而不是在發佈新版時才宣佈這些修改\[20\]。事實上，很多Ubuntu的開發者同時也是Debian主要軟體的維護者。不過，Debian與Ubuntu的軟體並不一定完全兼容，也就是說，將Debian的套件安裝在Ubuntu上可能會出現兼容性問題，反之亦然。\[21\]

Ubuntu的運作主要依賴[Canonical有限公司的支援](../Page/Canonical.md "wikilink")，同時亦有來自Linux社區的熱心人士提供協助。Ubuntu的開發人員多稱馬克·舍特爾沃斯為SABDFL（是**s**elf-**a**ppointed
**b**enevolent **d**ictator **f**or
**l**ife的縮寫，即自封終生開源碼大佬）\[22\]。在2005年7月8日，馬克·舍特爾沃斯與Canonical有限公司宣佈成立[Ubuntu基金會](../Page/Ubuntu基金會.md "wikilink")，並提供1千萬美元作為啟始營運資金。成立基金會的目的是為了確保將來Ubuntu得以持續開發與獲得支持，但直至2006年，此基金會仍未投入運作。馬克·舍特爾沃斯形容此基金會是在Canonical有限公司出現財務危機時的緊急營運資金。

在過去的版本使用者可以透過[船運服務](../Page/船運服務.md "wikilink")（shipit）\[23\]來獲得免費的安裝光碟。Ubuntu
6.06版有提供免費船運服務，然而其後的Ubuntu
6.10版卻沒有提供免費的船運郵寄光碟服務，使用者-{只}-可由網站上下載光碟映像檔燒錄並安裝。\[24\]。Ubuntu
6.06釋出當時，曾有消息指出往後不會再對非長期支援版提供船運服務，但在Ubuntu7.04版推出時，船運服務再度啟動，而此版並非長期支援版。在Ubuntu11.04发布前夕，船運服務被停止。

目前Ubuntu共有六個長期支援版本（Long Term Support，LTS）：Ubuntu
6.06、8.04、10.04、12.04、14.04、16.04與18.04。自Ubuntu
12.04起，桌面版與伺服器版都有5年支援周期。而之前的長期支援版本為桌面版3年，伺服器版5年。

## 特色

[Ubuntu_install_and_remove.ogg](https://zh.wikipedia.org/wiki/File:Ubuntu_install_and_remove.ogg "fig:Ubuntu_install_and_remove.ogg")[JauntyGUI.png](https://zh.wikipedia.org/wiki/File:JauntyGUI.png "fig:JauntyGUI.png")

### 系統管理

Ubuntu所有系統相關的任務均需使用[Sudo指令是它的一大特色](../Page/Sudo.md "wikilink")，這種方式比傳統的以系統管理員帳號進行管理工作的方式更為安全，此為Linux、Unix系統的基本思維之一。Windows
在较新的版本内也引入了类似的 UAC 机制，但使用者数量不多。同時，Ubuntu也相當注重系統的易用性，標準安裝完成後（或Live
CD启动完成后）就可以立即投入使用，簡單地說，就是安裝完成以後，使用者無需再費神安裝瀏覽器、Office套裝程式、多媒體播放程式等常用軟體，一般也無需下載安裝網卡、音效卡等硬體設備的驅動（但部份顯示卡需要額外下載的驅動程式，且不一定能用套件庫中所提供的版本）；Ubuntu的開發者與[Debian和](../Page/Debian.md "wikilink")[GNOME開源社區合作密切](../Page/GNOME.md "wikilink")，其各個正式版本的桌面環境均採用GNOME的最新版本，通常會緊隨GNOME項目的進展而及時更新（同時，也提供基於KDE、XFCE等桌面環境的衍生版本）。Ubuntu與Debian使用相同的[deb](../Page/deb.md "wikilink")
[軟件包格式](../Page/軟件包.md "wikilink")，可以安裝絕大多數為Debian編譯的-{zh-hans:软件包;
zh-hant:軟體套件;}-，雖然不能保證完全相容，但大多數情況是通用的。

### 開發理念

Ubuntu計劃強調[易用性和](../Page/易用性.md "wikilink")[國際化](../Page/國際化與本地化.md "wikilink")，以便能為儘可能多的人所用。在發佈5.04版時，Ubuntu就已經把[萬國碼](../Page/unicode.md "wikilink")（UTF-8
Unicode）作為系統預設編碼，用以應對各國各地區不同的語言文字，試圖給使用者提供一個無亂碼的交流平台。它在語言支援方面，算是Linux發行版中相當好的。

Ubuntu的所有發行版本都可以免費獲取。除了可下載[光碟映像檔](../Page/光碟映像檔.md "wikilink")（CD
Image）外，过去使用者也可通過郵寄服務\[25\]免費獲取安裝光碟，但是现在此服务已经停止，不过有需要的使用者还可以在Ubuntu网上商店付费购买Ubuntu光盘。與其它大型Linux廠商不同，Ubuntu不對所謂「企業版」收取升級訂購費（意即沒有所謂的企業版本，人人所使用的版本皆一樣，使用者-{只}-有在購買官方技術支援服務\[26\]時才要付錢）。Ubuntu社群推薦用戶自行下載[光碟映像檔燒錄成光碟安裝外](../Page/光碟映像檔.md "wikilink")，也推薦使用USB隨身碟進行安裝。

此外，Ubuntu計劃強調要儘量使用自由軟體，以便為各個版本的用戶提供便捷的升級途徑。

### 安裝設定

[Ubuntu_9.04_Jaunty_Jackalope_(LiveCD).png](https://zh.wikipedia.org/wiki/File:Ubuntu_9.04_Jaunty_Jackalope_\(LiveCD\).png "fig:Ubuntu_9.04_Jaunty_Jackalope_(LiveCD).png")会话）\]\]

一直以來，Ubuntu均支援主流的[i386](../Page/X86.md "wikilink")、[AMD64與](../Page/X86-64.md "wikilink")[PowerPC平台](../Page/PowerPC.md "wikilink")，自2006年6月，Ubuntu新增了對[-{zh-hans:升阳;
zh-hant:昇陽}-的](../Page/昇陽.md "wikilink")[UltraSPARC與](../Page/SPARC.md "wikilink")[UltraSPARC
T1平台的支援](../Page/UltraSPARC_T1.md "wikilink")。

Ubuntu主要是透過[Live
CD進行安裝](../Page/Live_CD.md "wikilink")。Ubuntu操作系统可以直接从CD启动（會有一些效率低落的情況），允许用户测试硬件兼容性和驱动程序支持。CD中带有一个安装器，讓用户可以将系统永久地装在计算机上。所有版本的CD镜像都可以在[Ubuntu网站](https://web.archive.org/web/20091009115011/http://www.ubuntu.com//getubuntu//download)下载。要透過CD安装的話至少要有256MB内存。\[27\]可以将CD镜像烧录到CD中，也可以使用一些工具（USB启动盘创建器、UNetBootin等）将其制作成USB启动盘进行测试或安装。

Live
CD中还带有一个[Wubi工具](../Page/Wubi.md "wikilink")，可以在不改变分区的情况下安装Ubuntu，不过性能稍有一些损失。
新版ubuntu支持在windows中进行在线安装。

### 重新打包

许多软件（比如[remastersys和](../Page/remastersys.md "wikilink")[Reconstructor](../Page/Reconstructor.md "wikilink")）可以将Ubuntu进行修改后重新打包成Ubuntu
Live CD。

### 其它特色

先前Ubuntu有一個叫*ubuntu-calendar*的-{zh-hans:软件包;
zh-hant:軟體套件;}-，安裝後，它會隨系統升級自動下載桌面圖檔。由於其中部分桌布為女性裸體照片，以致Ubuntu被幽默地稱為「情色發行版」\[28\]、「Linuxxx」\[29\]與其它類似名稱。2005年5月，頗具有爭議的Ubuntu-calendar未經公告就暫停發行了。現在，2004年10月到2005年4月的月曆桌布仍可以在Ubuntu儲存庫中找到。

## 套件管理

[Ubuntu_Login.png](https://zh.wikipedia.org/wiki/File:Ubuntu_Login.png "fig:Ubuntu_Login.png")
Ubuntu的套件管理系統與Debian的類似，所有軟體分為main、restricted、universe和multiverse等4類，每一類為一個「組件（component）」，代表著不同的使用許可和可用的支援級別。一般來說，官方支持的main組件主要用來滿足大多數個人電腦用戶的基本要求，restricted（「版權限制」）組件主要用來提高系統的可用性，因此通常需要安裝這兩類組件中的軟體。

### 組件介紹

''main
''即「基本」組件，其中只包含符合Ubuntu的許可證要求並可以從Ubuntu團隊中獲得支持的軟體，致力於滿足日常使用，位於這個組件中的軟體可以確保得到技術支援和及時的安全更新。此組件內的軟體是必須符合Ubuntu版權要求（Ubuntu
license
requirements）\[30\]的自由軟體，而Ubuntu版權要求大致上與[Debian自由軟體指導綱要](../Page/Debian自由軟體指導綱要.md "wikilink")（Debian
Free Software Guidelines）相同。

''restricted
''即「受限」組件，其中包含了非常重要的，但並不具有合適的自由許可證的軟體，例如-{只}-能以二進位形式獲得的[顯卡](../Page/顯示卡.md "wikilink")[驅動程式](../Page/驅動程式.md "wikilink")。由於Ubuntu開發者無法獲得相應的[原始碼](../Page/原始碼.md "wikilink")，restricted組件能夠獲得的支持與main組件相比是非常有限的。

''universe
''即「社群維護」組件，其中包含的軟體種類繁多，均为[自由软件](../Page/自由软件.md "wikilink")，但都不為Ubuntu團隊所支援。

''multiverse ''即「非自由」組件，其中包括了不符合自由軟體要求而且不被Ubuntu團隊支援的-{zh-hans:软件包;
zh-hant:軟體套件;}-，通常為商業公司編寫的軟體。

各類組件說明可見下表：

|       | [自由軟體](../Page/自由軟體.md "wikilink") | 非自由軟體      |
| ----- | ---------------------------------- | ---------- |
| 官方支援  | Main                               | Restricted |
| 非官方支援 | Universe                           | Multiverse |

### 軟體維護

Ubuntu的新版一旦發行，該版本的套件庫就會被凍結，此後只對該套件庫提供安全性更新。為此，官方推出了一個名為Ubuntu
Backports\[31\]的後續支援計劃，讓使用者可以在不更新套件庫的情況下，獲得和使用各類新版的應用軟體。

由於Linux系統受病毒的威脅不大\[32\]，因此Ubuntu系統通常不必安裝防毒軟體。管理員們如有需要，可自行安裝[ClamAV](../Page/Clam_AntiVirus.md "wikilink")，以便掃瞄和清除伺服器中的Windows病毒。Ubuntu系統中預設帶有[ufw防火牆軟體](../Page/ufw.md "wikilink")，但不提供相應的圖形設置界面，使用者可自行安裝[firestarter](../Page/firestarter.md "wikilink")，以便通過圖形界面設置防火牆。

安裝軟體時可以通過執行apt-get命令，或使用圖形介面的[Synaptic工具或](../Page/Synaptic.md "wikilink")“软件中心”來完成。與Windows不同，Ubuntu的使用者通常不必四處搜尋、逐一下載或購買相應的安裝程式。Ubuntu能夠使用的軟體大多存放在被稱為「軟體源」的伺服器中，使用者只要執行相應的apt-get指令（或使用Synaptic工具進行相關操作），系統就會自動尋找、下載和安裝軟體了。

### 私有版權軟體的採用

雖然Ubuntu主要採用自由軟體，但也接納部分可以自由散發的私有軟體，並將它們放在multiverse組件中。Ubuntu還為第三方軟體設立了認證程序\[33\]。

## 新版發佈週期

Ubuntu每6個月發佈一個新版，每個版本都有代號和版本號。版本號源自發佈日期，例如第一個版本，*4.10*，代表是在2004年10月發行的\[34\]。下表列出了以前和計劃中的發布：

<table>
<thead>
<tr class="header">
<th><p>版本</p></th>
<th><p>开发代号</p></th>
<th><p>中譯</p></th>
<th><p>发布日期</p></th>
<th><p>支援结束时间</p></th>
<th><p><a href="../Page/Linux內核.md" title="wikilink">内核版本</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>桌面版</p></td>
<td><p>伺服器版</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>4.10</p></td>
<td><p>Warty Warthog</p></td>
<td><p>多疣的<a href="../Page/疣猪.md" title="wikilink">疣猪</a></p></td>
<td><p>2004-10-20</p></td>
<td><p>colspan="2" </p></td>
<td><p>2.6.8</p></td>
</tr>
<tr class="odd">
<td><p>5.04</p></td>
<td><p>Hoary Hedgehog</p></td>
<td><p>白发的<a href="../Page/刺猬.md" title="wikilink">刺猬</a></p></td>
<td><p>2005-04-08</p></td>
<td><p>colspan="2" </p></td>
<td><p>2.6.10</p></td>
</tr>
<tr class="even">
<td><p>5.10</p></td>
<td><p>Breezy Badger</p></td>
<td><p>活泼的<a href="../Page/獾.md" title="wikilink">獾</a></p></td>
<td><p>2005-10-13</p></td>
<td><p>colspan="2" </p></td>
<td><p>2.6.12</p></td>
</tr>
<tr class="odd">
<td><p>6.06 LTS</p></td>
<td><p>Dapper Drake</p></td>
<td><p>整洁的公<a href="../Page/鸭.md" title="wikilink">鸭</a></p></td>
<td><p>2006-06-01</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>6.10</p></td>
<td><p>Edgy Eft</p></td>
<td><p>尖利的小<a href="../Page/蜥蜴.md" title="wikilink">蜥蜴</a></p></td>
<td><p>2006-10-26</p></td>
<td><p>colspan="2" </p></td>
<td><p>2.6.17</p></td>
</tr>
<tr class="odd">
<td><p>7.04</p></td>
<td><p>Feisty Fawn</p></td>
<td><p>烦躁不安的<a href="../Page/鹿科.md" title="wikilink">鹿</a></p></td>
<td><p>2007-04-19</p></td>
<td><p>colspan="2" </p></td>
<td><p>2.6.20</p></td>
</tr>
<tr class="even">
<td><p>7.10</p></td>
<td><p>Gutsy Gibbon</p></td>
<td><p>胆大的<a href="../Page/长臂猿.md" title="wikilink">长臂猿</a></p></td>
<td><p>2007-10-18</p></td>
<td><p>colspan="2" </p></td>
<td><p>2.6.22</p></td>
</tr>
<tr class="odd">
<td><p>8.04 LTS</p></td>
<td><p>Hardy Heron</p></td>
<td><p>坚强的<a href="../Page/鹭科.md" title="wikilink">鹭</a></p></td>
<td><p>2008-04-24</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>8.10</p></td>
<td><p>Intrepid Ibex</p></td>
<td><p>无畏的<a href="../Page/羱羊.md" title="wikilink">羱羊</a></p></td>
<td><p>2008-10-30</p></td>
<td><p>colspan="2" </p></td>
<td><p>2.6.27</p></td>
</tr>
<tr class="odd">
<td><p>9.04</p></td>
<td><p>Jaunty Jackalope</p></td>
<td><p>活潑的<a href="../Page/鹿角兔.md" title="wikilink">鹿角兔</a></p></td>
<td><p>2009-04-23</p></td>
<td><p>colspan="2" </p></td>
<td><p>2.6.28</p></td>
</tr>
<tr class="even">
<td><p>9.10</p></td>
<td><p>Karmic Koala</p></td>
<td><p>幸运的-{zh-hans:<a href="../Page/无尾熊.md" title="wikilink">无尾熊</a>;zh-cn:<a href="../Page/树袋熊.md" title="wikilink">树袋熊</a>;zh-hk:<a href="../Page/樹袋熊.md" title="wikilink">樹袋熊</a>;zh-sg:<a href="../Page/树袋熊.md" title="wikilink">树袋熊</a>;zh-tw:<a href="../Page/無尾熊.md" title="wikilink">無尾熊</a>;}-</p></td>
<td><p>2009-10-29</p></td>
<td><p>colspan="2" </p></td>
<td><p>2.6.31</p></td>
</tr>
<tr class="odd">
<td><p>10.04 LTS</p></td>
<td><p>Lucid Lynx</p></td>
<td><p>清醒的<a href="../Page/山貓.md" title="wikilink">山貓</a></p></td>
<td><p>2010-04-29</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>10.10</p></td>
<td><p>Maverick Meerkat</p></td>
<td><p>标新立异的<a href="../Page/狐獴.md" title="wikilink">狐獴</a></p></td>
<td><p>2010-10-10</p></td>
<td><p>colspan="2" </p></td>
<td><p>2.6.35</p></td>
</tr>
<tr class="odd">
<td><p>11.04</p></td>
<td><p>Natty Narwhal</p></td>
<td><p>敏捷的<a href="../Page/独角鲸.md" title="wikilink">独角鲸</a></p></td>
<td><p>2011-04-28</p></td>
<td><p>colspan="2" </p></td>
<td><p>2.6.38</p></td>
</tr>
<tr class="even">
<td><p>11.10</p></td>
<td><p>Oneiric Ocelot</p></td>
<td><p>有梦的<a href="../Page/虎猫.md" title="wikilink">虎猫</a></p></td>
<td><p>2011-10-13</p></td>
<td><p>colspan="2" </p></td>
<td><p>3.0</p></td>
</tr>
<tr class="odd">
<td><p>12.04 LTS</p></td>
<td><p>Precise Pangolin</p></td>
<td><p>精準的<a href="../Page/穿山甲.md" title="wikilink">穿山甲</a></p></td>
<td><p>2012-04-26[35]</p></td>
<td><p>colspan="2" </p></td>
<td><p>3.2[36]</p></td>
</tr>
<tr class="even">
<td><p>12.10</p></td>
<td><p>Quantal Quetzal</p></td>
<td><p>量子的<a href="../Page/格查尔鸟.md" title="wikilink">格查尔鸟</a></p></td>
<td><p>2012-10-18</p></td>
<td><p>colspan="2" </p></td>
<td><p>3.5[37]</p></td>
</tr>
<tr class="odd">
<td><p>13.04</p></td>
<td><p>Raring Ringtail</p></td>
<td><p>卯足了勁的<a href="../Page/環尾貓熊.md" title="wikilink">環尾貓熊</a></p></td>
<td><p>2013-04-25</p></td>
<td><p>colspan="2" [38]</p></td>
<td><p>3.8[39]</p></td>
</tr>
<tr class="even">
<td><p>13.10</p></td>
<td><p>Saucy Salamander</p></td>
<td><p>活潑的<a href="../Page/蠑螈.md" title="wikilink">蠑螈</a></p></td>
<td><p>2013-10-17[40]</p></td>
<td><p>colspan="2" [41]</p></td>
<td><p>3.11</p></td>
</tr>
<tr class="odd">
<td><p>14.04 LTS</p></td>
<td><p>Trusty Tahr</p></td>
<td><p>可靠的<a href="../Page/塔爾羊.md" title="wikilink">塔爾羊</a></p></td>
<td><p>2014-04-17[42]</p></td>
<td><p>colspan="2" </p></td>
<td><p>3.13</p></td>
</tr>
<tr class="even">
<td><p>14.10</p></td>
<td><p>Utopic Unicorn</p></td>
<td><p>烏托邦的<a href="../Page/獨角獸.md" title="wikilink">獨角獸</a></p></td>
<td><p>2014-10-23[43]</p></td>
<td><p>colspan="2" [44]</p></td>
<td><p>3.16[45]</p></td>
</tr>
<tr class="odd">
<td><p>15.04</p></td>
<td><p>Vivid Vervet</p></td>
<td><p>活潑的</p></td>
<td><p>2015-04-23[46]</p></td>
<td><p>colspan="2" [47]</p></td>
<td><p>3.19[48]</p></td>
</tr>
<tr class="even">
<td><p>15.10</p></td>
<td><p>Wily Werewolf</p></td>
<td><p>老謀深算的<a href="../Page/狼人.md" title="wikilink">狼人</a></p></td>
<td><p>2015-10-22[49]</p></td>
<td><p>colspan="2" [50]</p></td>
<td><p>4.2[51]</p></td>
</tr>
<tr class="odd">
<td><p>16.04 LTS</p></td>
<td><p>Xenial Xerus</p></td>
<td><p>好客的<a href="../Page/非洲地松鼠.md" title="wikilink">非洲地松鼠</a></p></td>
<td><p>2016-04-21[52]</p></td>
<td><p>colspan="2" </p></td>
<td><p>4.4[53]</p></td>
</tr>
<tr class="even">
<td><p>16.10</p></td>
<td><p>Yakkety Yak</p></td>
<td><p>喋喋不休的<a href="../Page/氂牛.md" title="wikilink">氂牛</a></p></td>
<td><p>2016-10-13[54]</p></td>
<td><p>colspan="2" </p></td>
<td><p>4.8</p></td>
</tr>
<tr class="odd">
<td><p>17.04</p></td>
<td><p>Zesty Zapus</p></td>
<td><p>熱情的<a href="../Page/美洲林跳鼠.md" title="wikilink">美洲林跳鼠</a></p></td>
<td><p>2017-04-13[55]</p></td>
<td><p>colspan="2" </p></td>
<td><p>4.10[56]</p></td>
</tr>
<tr class="even">
<td><p>17.10</p></td>
<td><p>Artful Aardvark</p></td>
<td><p>巧妙的<a href="../Page/土豚.md" title="wikilink">土豚</a></p></td>
<td><p>2017-10-19[57]</p></td>
<td><p>colspan="2" </p></td>
<td><p>4.13[58]</p></td>
</tr>
<tr class="odd">
<td><p>18.04 LTS</p></td>
<td><p>Bionic Beaver[59][60]</p></td>
<td><p>仿生的<a href="../Page/海狸.md" title="wikilink">海狸</a></p></td>
<td><p>2018-04-26[61]</p></td>
<td><p>colspan="2" </p></td>
<td><p>4.15</p></td>
</tr>
<tr class="even">
<td><p>18.10</p></td>
<td><p>Cosmic Cuttlefish</p></td>
<td><p>宇宙的<a href="../Page/墨鱼目.md" title="wikilink">墨魚</a></p></td>
<td><p>2018-10-18[62]</p></td>
<td><p>colspan="2" </p></td>
<td><p>4.18[63]</p></td>
</tr>
<tr class="odd">
<td><p>19.04</p></td>
<td><p>Disco Dingo</p></td>
<td><p>迪斯可的<a href="../Page/澳洲野犬.md" title="wikilink">澳洲野犬</a></p></td>
<td><p>2019-04-18[64]</p></td>
<td><p>colspan="2" </p></td>
<td><p>5.0[65]</p></td>
</tr>
<tr class="even">
<td><p><small></small></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 長期支援版本

長期支援版本（LTS），更新維護的時間比較長，約2年會推出一個LTS版本。LTS針對企業用戶，有別於一般版本的9個月支援。代號「Dapper
Drake」的Ubuntu 6.06
LTS是第一個獲得長期支援的版本，Canonical公司計劃對6.06的桌面系列版本提供3年的更新及付費技術支援服務，對伺服器版則提供5年的支援。Ubuntu
6.06 LTS包括[GNOME](../Page/GNOME.md "wikilink") 2.14、[Mozilla
Firefox](../Page/Mozilla_Firefox.md "wikilink")
1.5.0.3、[OpenOffice.org](../Page/OpenOffice.org.md "wikilink")
2.0.2、[Xorg](../Page/X.Org服务器.md "wikilink")7.0、[GCC](../Page/GCC.md "wikilink")
4.0.3以及2.6.15版的[Linux核心](../Page/Linux核心.md "wikilink")，2006年8月10日發佈的首個維護更新版本6.06.1。\[66\]因為其較長的支援週期，Canonical宣佈將繼續為Dapper
Drake提供送達（Shipping）服務，但不支援隨後發佈的Edgy Eft。不過，隨著Feisty
Fawn版的發布，送達（Shipping）服務再次啟動，但是，Natty
Narwhal（11.04）发布前，送達（Shipping）服務再次停止。\[67\]

最新的長期支援版本為2019年2月15日发布的18.04.2 LTS。

### 其它分支

Ubuntu還有一個代號為Grumpy
Groundhog的分支\[68\]，這個分支直接從Ubuntu的軟體[版本控制系統裡獲取軟體的原始碼](../Page/版本控制系統.md "wikilink")，主要用於測試和開發。由於這個分支不穩定，因此不對公眾開放。

### 版本代號命名

Ubuntu版本的命名規則是根據正式版發行的年月命名，Ubuntu
8.10也就意味著2008年10月發行的Ubuntu，研發人員與使用者可從版本號碼就知道正式發布的時間。Ubuntu是基於[Debian開發的](../Page/Debian.md "wikilink")[Linux發行版](../Page/Linux發行版.md "wikilink")，Debian的開發代號來自於電影[玩具總動員](../Page/玩具總動員.md "wikilink")，不過，Ubuntu各版本的代號卻固定是形容詞加上動物名稱，而且這2個词的英文首字母一定是相同的。从Ubuntu
6.06开始，两个词的首字母按照英文字母表的排列顺序取用。\[69\]

### 時間線

## 各界評價

### 回應

2005年於倫敦舉行的[Linux世界論壇及會議](../Page/Linux世界論壇及會議.md "wikilink")（LinuxWorld
Conference and Expo）上，Ubuntu被評為讀者所選的最佳Linux發行版。\[70\]
Ubuntu也經常被網路和平面出版媒體評審，\[71\]\[72\]很多評審者認為Ubuntu的成功主要原因在於其擁有一個龐大的社群，使用者可以便捷地從中獲得幫助和支援。\[73\]\[74\]
informationweek網站於2008年5月對7款主流的Linux發行版系統進行了測試，包括openSUSE，Ubuntu
8.04，PCLinuxOS，Mandriva Linux One，Fedora，SimplyMEPIS和CentOS
5.1，結果是Ubuntu獲勝。目前[維基百科的網站伺服器](../Page/維基百科.md "wikilink")，也是採用Ubuntu
Linux。

### 批評

Ubuntu源自[Debian](../Page/Debian.md "wikilink")，但Debian的創始人[Ian
Murdock卻不滿意Ubuntu](../Page/Ian_Murdock.md "wikilink")。他認為，雖然Ubuntu是優秀的Linux發行版，也促進了Debian的全球化，但Ubuntu另建-{zh-hans:软件包;
zh-hant:套件庫;}-，而不是直接改進Debian已有的-{zh-hans:软件包;
zh-hant:套件庫;}-，因此出現了與Debian不相容的問題。他希望Ubuntu能與Debian進行更為緊密的合作，使其改進也可以被Debian所採用\[75\]。

2010年欧洲GUADEC会议上公布的“GNOME开发者分布”，显示出Ubuntu的母公司Canonical对GNOME项目的贡献十分小。由此，一些人抱怨，觉得Canonical应该作出更多的贡献\[76\]。前Red
Hat开发者Greg DeKoenigsberg亦对Ubuntu批评：“Red
Hat对开源的贡献远高于Canonical，而Canonical是一家伪装成技术企业的营销机构”，后来对其言论进行了公开道歉，但一直坚持Canonical应该为Linux作出更大的贡献\[77\]。

因为Ubuntu基于[Debian的不稳定分支](../Page/Debian.md "wikilink")（sid），更容易遇到和弹出内部错误。

由于Ubuntu母公司Canonical帮助微软公司开发了Windows下的Linux兼容层[Windows Subsystem for
Linux](../Page/Windows_Subsystem_for_Linux.md "wikilink")，部分用户称是卖友求荣。

## 分支版本

[Ubuntu_6.06_LTS_CDs.jpg](https://zh.wikipedia.org/wiki/File:Ubuntu_6.06_LTS_CDs.jpg "fig:Ubuntu_6.06_LTS_CDs.jpg")
Linux各種發行版是使用Linux核心一類開放型的作業系統。由發行版定製其應用軟體、桌面環境的組合和配置，因此同一發行版也可分支。Ubuntu官方認可的分支系統眾多，其主要差異在於使用的桌面系統不同，而內部的預設軟體也會有所歧異。此外尚有許多基於Ubuntu的非官方衍生版本，還有基於Ubuntu開發的發行版。

所謂的Ubuntu系統，指的是預設的Ubuntu版本。11.04版以及之前支援[Gnome桌面環境](../Page/Gnome.md "wikilink")，之后的版本採用母公司研發的[Unity界面](../Page/Unity_\(使用者介面\).md "wikilink")。但17.10版及之后又回归了Gnome，Ubuntu
17.04是最后一个预载Unity桌面环境的版本。

### 正式衍生版本

正式衍生版本統一使用和Ubuntu一样的-{zh-hans:软件包; zh-hant:套件庫;}- 目前Ubuntu正式支援的衍生版本包括：

  - [Kubuntu](../Page/Kubuntu.md "wikilink")：采用[KDE作为默认的桌面环境](../Page/KDE_Plasma_Workspaces.md "wikilink")，以满足偏爱[KDE的Ubuntu用户](../Page/KDE.md "wikilink")。

  - [Edubuntu](../Page/Edubuntu.md "wikilink")：为教育量身定做，包含很多教育软件，可以帮助教师方便的搭建网络学习环境，管理电子教室。采用[Unity界面](../Page/Unity_\(使用者介面\).md "wikilink")。

  - [Xubuntu](../Page/Xubuntu.md "wikilink")：属于轻量级发行版，使用[Xfce](../Page/Xfce.md "wikilink")4作为默认的桌面环境。

  - [Lubuntu](../Page/Lubuntu.md "wikilink")：使用[LXDE桌面环境的轻量级发行版](../Page/LXDE.md "wikilink")\[78\]，从10.04版本开始正式发行。

  - Ubuntu Server Edition：自Ubuntu 5.10版（*Breezy
    Badger*）起，与桌面版同步发行，\[79\]可当作多种软件服务器，如[电子邮件服务器](../Page/電子郵件.md "wikilink")、基于[LAMP的Web网站服务器](../Page/LAMP.md "wikilink")、[DNS服务器](../Page/DNS.md "wikilink")、文件服务器与[数据库服务器等](../Page/資料庫.md "wikilink")。\[80\]服务器版通常不预装任何桌面环境，与桌面版本相比，佔用空間少，运行时对硬件要求较低，最少只需要500MB硬盘空间和64MB内存。\[81\]

  - [Ubuntu
    Studio](../Page/Ubuntu_Studio.md "wikilink")：適合於音訊，視訊和圖像設計的版本。使用Xfce4作为默认的桌面环境。

  - ：一套基于Ubuntu的面向媒体中心电脑的发行版，Mythbuntu=MythTV+Ubuntu，MythTV是其中关键的软件包，用于实现媒体中心等功能。它没有包含一些不必要的程序，如OpenOffice,
    Evolution和完全安裝的Gnome。

  - [Ubuntu
    Kylin](../Page/Ubuntu_Kylin.md "wikilink")（优麒麟）：语言的默认设置为简体中文，为中国用户专门定制。\[82\]\[83\]

  - [Ubuntu
    MATE](../Page/Ubuntu_MATE.md "wikilink")：針對老舊桌上型、筆記型、樹莓派（Raspberry
    Pi）電腦，及硬體效能等級不高，或喜歡簡潔、不用特效桌面環境者，使用[MATE桌面環境的Ubuntu發行版](../Page/MATE.md "wikilink")。

  - [Ubuntu for
    Android](../Page/Ubuntu_for_Android.md "wikilink")：在[Android手機上運行的Ubuntu](../Page/Android.md "wikilink")。

  - [Ubuntu
    Touch](../Page/Ubuntu_Touch.md "wikilink")：基於Ubuntu和Android的手機/平板作業系統。

  - [Ubuntu TV](../Page/Ubuntu_TV.md "wikilink")：用於智能電視的版本。

上述衍生計劃與Ubuntu緊密相關，並與Ubuntu同步發行。另外，以下版本曾被Ubuntu正式支援，但相關支援已被終止

  - [Ubuntu Netbook
    Edition](../Page/Ubuntu_Netbook_Edition.md "wikilink")：專門為EeePC等小螢幕Netbook設計的Ubuntu，介面盡量精簡以省螢幕空間，例如GNOME的Panel減成只剩一條，且與視窗標題合在一起。后来上网本版开始使用稱為[Unity的桌面环境](../Page/Unity_\(使用者介面\).md "wikilink")，但是随着[Unity在](../Page/Unity_\(使用者介面\).md "wikilink")11.04中成为默认的桌面环境，上网本版被取消。

  - ：一個高度精簡的、專門針對虛擬化應用的版本。

  - [Ubuntu
    Mobile](../Page/Ubuntu_Mobile.md "wikilink")：Ubuntu在[MID上運行的版本](../Page/移动互联网设备.md "wikilink")。

  - [Gobuntu](../Page/Gobuntu.md "wikilink")：只使用[自由軟體的版本](../Page/自由軟體.md "wikilink")。

  - [Ubuntu
    GNOME](../Page/Ubuntu_GNOME.md "wikilink")：在Ubuntu使用Unity时使用[GNOME桌面環境的Ubuntu發行版](../Page/GNOME.md "wikilink")。现已合并至Ubuntu主线。

另外，[馬克·舍特爾沃斯承諾將製作](../Page/馬克·沙特爾沃思.md "wikilink")**Ubuntu-libre**發行版，只使用[自由軟體基金會認證過的](../Page/自由軟體基金會.md "wikilink")[自由軟體](../Page/自由軟體.md "wikilink")。\[84\]\[85\]

### 非正式衍生版本

  - [Ubuntu Lite](../Page/Ubuntu_Lite.md "wikilink")：為舊電腦而設的版本。
  - [Elbuntu](../Page/Elbuntu.md "wikilink")：基於[Enlightenment
    0.17桌面環境並附有](../Page/Enlightenment.md "wikilink")[視窗管理員的Ubuntu修改版](../Page/視窗管理員.md "wikilink")。
  - [Fluxbuntu](../Page/Fluxbuntu.md "wikilink")：基於[Fluxbox桌面環境的修改版](../Page/Fluxbox.md "wikilink")。
  - [Gnoppix](../Page/Gnoppix.md "wikilink")：基於Ubuntu [Live
    CD而研製的以](../Page/Live_CD.md "wikilink")[GNOME為預設桌面環境的](../Page/GNOME.md "wikilink")[Live
    CD](../Page/Live_CD.md "wikilink")。
  - [PUD
    GNU/Linux](../Page/PUD_GNU/Linux.md "wikilink")：由台灣愛好者所製作的改良版，以輕量化為目標，可安裝在光碟或256
    MB以上的USB隨身碟上，並使用Ubuntu的套件來源。
  - [gOS](../Page/gOS.md "wikilink")：基於[Enlightenment桌面環境](../Page/Enlightenment.md "wikilink")，整合Google多數的線上服務的版本。（並非由Google官方所開發）
  - [Linux
    Mint](../Page/Linux_Mint.md "wikilink")：基於Ubuntu的Linux發行版，目標是提供一種更完整的即刻可用體驗\[86\]。
  - [Easy
    Peasy](../Page/Easy_Peasy.md "wikilink")（[eeeXubuntu](../Page/eeeXubuntu.md "wikilink")）：專為[華碩的](../Page/華碩.md "wikilink")[Eee
    PC定製](../Page/Eee_PC.md "wikilink")。
  - [gNewSense](../Page/gNewSense.md "wikilink")：由自由軟體基金會官方於2006年11月2日推出，為一基於Ubuntu且只使用自由軟體的Linux發行版，但與[馬克·沙特爾沃思所提到的Ubuntu](../Page/馬克·沙特爾沃思.md "wikilink")-libre沒有直接關係。
  - [Elementary
    OS](../Page/Elementary_\(操作系统\).md "wikilink")：Elementary开发团队推出的Ubuntu修改版。
  - [UbuntuBSD](../Page/UbuntuBSD.md "wikilink")：将[FreeBSD内核移植到Ubuntu的项目](../Page/FreeBSD.md "wikilink")。

## 參與中文翻譯

由Ubuntu母公司[Canonical有限公司所架設的](../Page/Canonical有限公司.md "wikilink")[Launchpad網站提供了線上翻譯服務](../Page/Launchpad_\(網站\).md "wikilink")，任何人都可以通過這個網站協助翻譯Ubuntu。但是經由此方式對非Ubuntu獨有組件的翻譯成果將不會自動反饋到上游，故不被推薦。\[87\]

## 回報錯誤

由Ubuntu母公司[Canonical有限公司所架設的](../Page/Canonical有限公司.md "wikilink")[Launchpad網站提供了一套線上回報軟體](../Page/Launchpad_\(網站\).md "wikilink")[程序错误的機制](../Page/程序错误.md "wikilink")，任何人都可以把自己所發現的軟體[程序错误](../Page/程序错误.md "wikilink")、功能缺陷和安全漏洞通過這套機制回報給開發小組。然而，由于文化习惯上的原因，中文Ubuntu社群上[程序错误回馈的积极性顯然不够高](../Page/程序错误.md "wikilink")。

## 註釋

<div style="font-size: 85%">

這是免費提供的支援，其包括了安全性更新、引数据損失的重要漏洞修補與額外的翻譯。若要其它的技術支援，則需要購買其商業支援服務。值得注意的是LTS版（即長期支援版）所獲的支援期更長，詳情可參看內文。

sudo為substitute user
do的簡寫，即超級用戶的工作，在Ubuntu的預設環境裡，root（即管理員）帳號是停用的，所有與系統相關的工作指令均需在進行時於終端機介面輸入sudo在指令前方，並輸入密碼確認，這樣做是為了防止因一時失誤對系統造成破壞。sudo工具的預設密碼是目前帳戶的密碼。

</div>

## 参考文献

<div class="references-small" style="height: 220px; overflow: auto; padding: 3px">

  - 引用

<!-- end list -->

  - 来源

<!-- end list -->

  - [Ubuntu Releases Page](http://www.ubuntulinux.org/ubuntu/releases)
    by Daishi, 20 December 2005, retrieved 21 December 2005

  - [What is it About
    Ubuntu?](https://web.archive.org/web/20070928125448/http://www.xyzcomputing.com/index.php?option=content&task=view&id=492&Itemid=0&limit=1&limitstart=0)
    - by Sal Cangeloso, XYZ Computing, Monday, 05 December 2005,
    retrieved 21 December 2005

  - [Debian and Ubuntu](http://www.ubuntulinux.org/ubuntu/relationship)

  - [About Ubuntu](http://www.ubuntulinux.org/ubuntu/) by Daishi, 08
    September 2005, retrieved 21 December 2005

  - [RootSudo - Ubuntu
    Wiki](https://wiki.ubuntu.com/RootSudo?highlight=%28sudo%29) by
    Dooglus, 21 December 2005, retrieved 21 December 2005

  - [Ubuntu - Philosophy](http://www.ubuntulinux.org/ubuntu/philosophy)
    by Daishi, 01 March 2005, retrieved 21 December 2005

  - [Once I have installed Ubuntu, will I be able to upgrade to the next
    release?](http://www.ubuntulinux.org/support/faq#head-5d258da97e700cf78c82c09dcd0c6a401ee51ca5)
    by Viktorija, 10 April 2005, retrieved 21 December 2005

  - [Screenshots](http://www.ubuntulinux.org/screenshots) by Daishi, 13
    October 2005, retrieved 21 December 2005

  - [The Hottest New Distrobution:
    Linuxxx](http://humorix.org/articles/2004/11/linuxxx/) by Dances
    With Herring, Humorix, 23 November 2004, retrieved 21 December 2005

  - [Ubuntu & You, Part 1: Trying Today's Most Popular Linux
    Distribution](https://web.archive.org/web/20060503070741/http://www.communitymx.com/content/article.cfm?page=2&cid=C5776)
    by Thomas Pletcher, CommunityMX, 15 August 2005, retrieved 21
    December 2005

  - [Restricted Formats](https://wiki.ubuntu.com/RestrictedFormats) last
    edited 2005-12-16 02:24:00 by RichRudnick, Ubuntu Wiki, retrieved 21
    December 2005

  - [Restricted Items
    Wiki](http://fedoraproject.org/wiki/ForbiddenItems#head-97873514e1b74022d2efc181c95e757deb515b39)
    last edited 2005-12-20 02:37:30 by ChrisLennert, retrieved 21
    December 2005

  -
  -
  -
  - [就Ubuntu採訪Mark
    Shuttleworth](http://interviews.slashdot.org/article.pl?sid=05/04/04/1859255)

  - [Ubuntu Tweak（中國網友撰寫的Ubuntu配置工具）](http://ubuntu-tweak.com/)

  - [Lazyscripts（台灣網友撰寫的Ubuntu調整工具&軟體大補帖，前身為Lazybuntu）](http://lazyscripts.org/)

  - [電腦不難（於10.04版後開始出現Ubuntu及其各分支的安裝圖文流程）](http://it-easy.tw/category/os-teach/linux/)

  - [乌帮图的博客（Ubuntu打开Windows中txt文件显示乱码的解决办法）](http://wubangtu.com/854)

  - [马丁博客（Ubuntu使用学习以及性能优化）](https://web.archive.org/web/20110816161253/http://www.blags.org/tags/ubuntu/)

  -
  - [次世代Linux – Ubuntu玩全手冊
    (網頁與PDF完整版),蔡東邦and李聖瑋](https://web.archive.org/web/20131029203506/https://www.dbtsai.com/blog/2006_ubuntu_book/)

</div>

## 外部連結

  - [官方網站（Ubuntu）](http://www.ubuntu.com/)
      - [中文官方網站](http://cn.ubuntu.com/)
  - [英屬曼島商肯諾有限公司（Canonical Ltd）](http://www.canonical.com/)
  - [正體中文站](http://www.ubuntu-tw.org/)

## 参见

  - [Ubuntu发行版列表](../Page/Ubuntu发行版列表.md "wikilink")
  - [Wubi](../Page/Wubi.md "wikilink")
  - [Linux套件列表](../Page/Linux套件列表.md "wikilink")
  - [烏班圖](../Page/烏班圖.md "wikilink")（）
  - [微軟視窗與Linux的比較](../Page/微軟視窗與Linux的比較.md "wikilink")
  - [Kubuntu](../Page/Kubuntu.md "wikilink")
  - [Xubuntu](../Page/Xubuntu.md "wikilink")
  - [Lubuntu](../Page/Lubuntu.md "wikilink")
  - [Edubuntu](../Page/Edubuntu.md "wikilink")
  - [Ubuntu MATE](../Page/Ubuntu_MATE.md "wikilink")

{{-}}

[Ubuntu](../Category/Ubuntu.md "wikilink")
[Category:Linux發行版](../Category/Linux發行版.md "wikilink")
[Category:自由作業系統](../Category/自由作業系統.md "wikilink")
[Category:LiveCD](../Category/LiveCD.md "wikilink")
[Category:2004年软件](../Category/2004年软件.md "wikilink")
[Category:基于Debian的发行版](../Category/基于Debian的发行版.md "wikilink")
[Category:可从只读媒体启动的操作系统发行版](../Category/可从只读媒体启动的操作系统发行版.md "wikilink")

1.

2.

3.

4.  [Ubuntu's African
    Roots](http://www.easy-ubuntu-linux.com/ubuntu-african-word.html)

5.

6.
7.

8.

9.  [柯克兰:Ubuntu操作系统全球用户已超10亿－Yesky天极新闻](http://wap.yesky.com/477/99506977.shtml)

10. [ubuntu/releases -
    Ubuntu](http://www.ubuntulinux.org/ubuntu/releases)，2006年3月11日更新。

11. [Announcing Beta release of Ubuntu 6.06
    LTS](https://lists.ubuntu.com/archives/ubuntu-announce/2006-April/000065.html)，2006年4月26日更新。

12. [Ubuntu计划缩短非LTS版本支持时间](http://cnbeta.com/articles/230518.htm)

13. [community - Ubuntu](http://ubuntulinux.org/community)
    ，2006年7月20日更新。

14. [XYZ computing article on
    kubuntu](http://www.xyzcomputing.com/index.php?option=content&task=view&id=492&Itemid=0&limit=1&limitstart=1)
    ，2006年7月20日更新。

15.

16. 笨兔的名字来源于

17. Ubuntu的版本編號是以釋出的年份與月份命名。

18.

19.

20.
21.

22.

23.

24.

25.
26.

27.

28.

29.

30.

31.

32.

33.

34.

35.

36.

37.

38.
39.

40.

41.
42.
43.
44.

45. ["Ubuntu 14.10 (Utopic Unicorn) Now Based on Linux
    Kernel 3.16"](http://news.softpedia.com/news/Ubuntu-14-10-Utopic-Unicorn-Now-Based-on-Linux-Kernel-3-16-451606.shtml),
    22 July 2014, Silviu Stahie

46.
47.

48.

49.
50.

51.

52.
53.

54.
55.

56.

57.

58.

59.

60.

61.

62.

63.

64.

65.

66.

67.

68.

69.

70.

71.

72.

73.
74.
75.

76.

77.

78. <http://linuxtoy.org/archives/lubuntu-10-04.html>

79. <http://www.ubuntu.com/server>

80.
81.
82.

83.

84.

85.

86.

87. [LaunchpadFAQ](http://wiki.ubuntu.org.cn/index.php?title=LaunchpadFAQ&variant=zh-hans).Ubuntu_Launchpad