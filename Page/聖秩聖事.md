**聖秩聖事**（或稱**神品圣事**、**按立圣职圣礼**，簡稱**按立禮**）是[天主教](../Page/天主教.md "wikilink")、[東正教等](../Page/東正教.md "wikilink")[基督宗教教派的](../Page/基督宗教.md "wikilink")[聖事禮儀之一](../Page/聖事.md "wikilink")；[新教並不視按立圣职為聖事](../Page/新教.md "wikilink")，只視為[教會的](../Page/教會.md "wikilink")[聖禮](../Page/聖禮.md "wikilink")。

在教會中认为，该圣事源与[耶稣在升天前就赋予了](../Page/耶稣.md "wikilink")[使徒们几种](../Page/使徒.md "wikilink")[神权](../Page/神权.md "wikilink")，包括献弥撒（《[路加福音](../Page/路加福音.md "wikilink")》22：19）、赦罪（《[約翰福音](../Page/約翰福音.md "wikilink")》20：23）；训导、圣化人类（《[馬太福音](../Page/馬太福音.md "wikilink")》28：18-20）。他们也相信，此种职权能够通过“覆手禮”，被[天主特别恩宠](../Page/天主.md "wikilink")（[弟茂德後書](../Page/弟茂德後書.md "wikilink")/[提摩太後書](../Page/提摩太後書.md "wikilink")1：6）而代代相传下去。继承这种神权的仪式叫做圣秩。

这种聖事的产生可追述到[使徒时代](../Page/使徒时代.md "wikilink")，毕竟教会是要发展的、一代一代的传承下去，神权也借着这种仪式继承到了今天。

## 天主教

根据《[新约](../Page/新约.md "wikilink")》的记载，[耶稣主持并设立了如](../Page/耶稣.md "wikilink")[洗礼](../Page/洗礼.md "wikilink")、[共餐](../Page/洗腳禮.md "wikilink")、[洗脚](../Page/洗腳禮.md "wikilink")、[抹油](../Page/抹油.md "wikilink")、[赶鬼这些惯例](../Page/赶鬼.md "wikilink")，逐渐的随时代的演化便有了“[圣事](../Page/圣事.md "wikilink")”的感念，当然在千百年来随教会的发展，对“圣事”的理解各个时代也有不同。关于[天主教会的七件圣事](../Page/天主教会.md "wikilink")，据说是P.隆巴德提出由[托马斯·阿奎那整理最后经](../Page/托马斯·阿奎那.md "wikilink")[特伦托会议再颁布的](../Page/天特會議.md "wikilink")，包括有[洗礼](../Page/洗礼.md "wikilink")、[坚振](../Page/坚振.md "wikilink")、[圣体](../Page/圣体.md "wikilink")、[告解](../Page/告解.md "wikilink")、[病人傅油](../Page/病人傅油.md "wikilink")、[婚姻與圣秩](../Page/婚姻.md "wikilink")。

领受圣秩后的天主教徒被称作[神职人员](../Page/神职人员.md "wikilink")，包括[主教](../Page/主教.md "wikilink")、[神父与](../Page/神父.md "wikilink")[执事](../Page/执事.md "wikilink")（在天主教中已婚的男性可以当[执事](../Page/执事.md "wikilink")）。在西方禮[天主教會](../Page/天主教會.md "wikilink")，[主教和](../Page/主教.md "wikilink")[神父必须是](../Page/神父.md "wikilink")[单身](../Page/单身.md "wikilink")。

在[華人地區的](../Page/華人地區.md "wikilink")[天主教會裡](../Page/天主教會.md "wikilink")，將成為神父的聖秩聖事稱為「**晉鐸**」（即「晉升[司鐸](../Page/司鐸.md "wikilink")」之意，司鐸是對神父的另一種稱呼）、成為主教的聖秩聖事稱為「**晉牧**」（即「晉升牧職」之意，天主教會常將主教比喻為[牧人](../Page/牧羊.md "wikilink")），這是[汉语](../Page/汉语.md "wikilink")[语境中產生的特殊用法](../Page/语境.md "wikilink")。

## 東正教

[東方禮天主教會和](../Page/東方禮天主教會.md "wikilink")[正教會允許已](../Page/正教會.md "wikilink")[結婚的神父和執事](../Page/結婚.md "wikilink")，只是婚禮要在上任前完成。如果[離婚或是成了](../Page/離婚.md "wikilink")[鰥夫](../Page/鰥夫.md "wikilink")，神父和執事不能再婚，除非他們不再當神父和執事。

## 新教

### 聖公宗

### 信義宗

### 其他新教教派或团体

## 参考文献

## 参见

  - [聖事](../Page/聖事.md "wikilink")
  - [天主教会圣统制](../Page/天主教会圣统制.md "wikilink")

{{-}}

[category:基督教禮儀](../Page/category:基督教禮儀.md "wikilink")

[de:Ordination\#Kirchen katholischer und orthodoxer
Tradition](../Page/de:Ordination#Kirchen_katholischer_und_orthodoxer_Tradition.md "wikilink")

[Category:天主教礼拜仪式](../Category/天主教礼拜仪式.md "wikilink")
[Category:聖事](../Category/聖事.md "wikilink")