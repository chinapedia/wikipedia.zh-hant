[Brahma_and_the_Lokapalas.jpg](https://zh.wikipedia.org/wiki/File:Brahma_and_the_Lokapalas.jpg "fig:Brahma_and_the_Lokapalas.jpg")
[Surya_Majapahit_Diagram.svg](https://zh.wikipedia.org/wiki/File:Surya_Majapahit_Diagram.svg "fig:Surya_Majapahit_Diagram.svg")
**方位護法**（ /
दिक्पाल）是按[印度教經典](../Page/印度教.md "wikilink")[摩诃婆罗多排列多個座守不同方位的](../Page/摩诃婆罗多.md "wikilink")[婆羅門](../Page/婆羅門.md "wikilink")[護法神](../Page/護法神.md "wikilink")。傳統上方位護法都會繪於寺廟的牆壁及天花板上，亦有寺廟或其他建築物在外牆根據記載雕刻或安放對應位置的神像。其他的经典记载的版本如[阿闼婆吠陀有微小的差别](../Page/阿闼婆吠陀.md "wikilink")，但一般以摩诃婆罗多的版本最常见。

## [摩诃婆罗多记载版本](../Page/摩诃婆罗多.md "wikilink")

护世四天王 - लोकपाल / Lokapāla守護四個正角（disha - 东南西北）方位，他們是：

  - [阎摩](../Page/阎摩.md "wikilink")（यम / Yama），位置东方（Pūrva / Prāchi /
    Prāk），司死者審判，掌管地獄。
  - [因陀羅](../Page/因陀羅.md "wikilink")（इन्द्र / Indra），位置南方（Dakshina /
    Avāchi），司氣候/戰爭。
  - [伐楼拿](../Page/伐楼拿.md "wikilink")（वरुण / Varuna），位置西方（Paśchima /
    Pratīchi / Apara），司宇宙天則及海水。
  - [俱毗罗](../Page/俱毗罗.md "wikilink")（कुबेर / Kubera），位置北方（Uttara /
    Udīchi），司財富。

四天王常見成雙繪於墓穴入口，傳說他們在需要時能召喚異世界的精靈協助他們保護墓穴。[佛教吸收了印度教的Lokapāla創立了自己的](../Page/佛教.md "wikilink")[四大天王](../Page/四大天王.md "wikilink")，其中北方的俱吠罗與[多闻天王有較明顯的關聯性](../Page/多闻天王.md "wikilink")；西方的伐楼拿與[广目天王同樣是率領](../Page/广目天王.md "wikilink")[那伽族](../Page/那伽.md "wikilink")。印度教的四天王之一因陀羅的佛教對應神祇帝釋天（Śakra）成為了佛教四大天王的侍奉對象，地位截然不同。

除了以上四個正角方位，又伸展出四個斜角方位構成合計八個方位的護法 - अष्ट-दिक्पाल / :

  - [伊舍那](../Page/伊舍那.md "wikilink")（Ishana /
    伊舍那天），位置東北（Īśānya），[濕婆朝上方的第五個頭](../Page/濕婆.md "wikilink")。
  - [阿耆尼](../Page/阿耆尼.md "wikilink")（अग्नि / Agni），位置東南（Āgneya），司火焰與淨化。
  - [伐由](../Page/伐由.md "wikilink")（वायु / Vayu），位置西北（Vāyavya），司風與大氣。
  - [尼利提](../Page/尼利提.md "wikilink")（Nirrti /
    [羅剎天](../Page/羅剎.md "wikilink")），位置西南（），司死亡。

最後[梵天坐於八神的](../Page/梵天.md "wikilink")（天極）正中央上方（Urdhva），[阿南塔](../Page/阿南塔.md "wikilink")（[毗湿奴蛇的化身](../Page/毗湿奴.md "wikilink")）於下方（Atha），十位神祇合稱Daśa-dikpāla。

## [阿闼婆吠陀记载版本](../Page/阿闼婆吠陀.md "wikilink")

  - [阿耆尼](../Page/阿耆尼.md "wikilink")，位置东方
  - [因陀羅](../Page/因陀羅.md "wikilink")，位置南方
  - [伐楼拿](../Page/伐楼拿.md "wikilink")，位置西方
  - [苏摩](../Page/苏摩.md "wikilink")，位置北方

## [广林奥义书记载版本](../Page/广林奥义书.md "wikilink")

  - [苏利耶](../Page/苏利耶.md "wikilink")，位置东方，太阳神
  - [閻魔](../Page/閻魔.md "wikilink")，位置南方
  - [伐楼拿](../Page/伐楼拿.md "wikilink")，位置西方
  - [苏摩](../Page/苏摩.md "wikilink")，位置北方
  - [阿耆尼](../Page/阿耆尼.md "wikilink")，位置天極上方

## 伸延閱讀

  - [四大護法](../Page/四大護法.md "wikilink")

[Category:印度教神祇](../Category/印度教神祇.md "wikilink")