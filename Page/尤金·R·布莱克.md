**小尤金·罗伯特·布莱克**（**Eugene Robert Black,
Jr.**，），[美国银行家](../Page/美国.md "wikilink")、商人、经济学家，曾任[美国联邦储备委员会主席](../Page/美国联邦储备委员会主席.md "wikilink")（1933年-1934年）。

[Category:乔治亚大学校友](../Category/乔治亚大学校友.md "wikilink")
[Category:美国经济学家](../Category/美国经济学家.md "wikilink")
[Category:美国联邦储备委员会主席](../Category/美国联邦储备委员会主席.md "wikilink")