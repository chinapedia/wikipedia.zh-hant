**旺达·兰多芙丝卡**（Wanda
Landowska，），[波兰裔法国](../Page/波兰.md "wikilink")[大键琴演奏家](../Page/大键琴.md "wikilink")。她是首位用大键琴录制[巴赫的](../Page/巴赫.md "wikilink")《[哥德堡变奏曲](../Page/哥德堡变奏曲.md "wikilink")》的音乐家（1931年）。通过她的出色表演，使得大键琴这种古老的乐器在20世纪又受到了人们的关注。1938年她入籍成为法国公民。\[1\]

## 简介

兰多芙丝卡出生于[华沙](../Page/华沙.md "wikilink")，是犹太人，父亲是律师，母亲是语言学家。她早年曾在[华沙音乐学院和](../Page/华沙音乐学院.md "wikilink")[柏林等地学习](../Page/柏林.md "wikilink")[钢琴](../Page/钢琴.md "wikilink")，1900年移居[巴黎](../Page/巴黎.md "wikilink")，与波兰民俗学者Henry
Lew结婚。Henry在1918年车祸去世。她积极钻研[古典音乐](../Page/古典音乐.md "wikilink")，改良了大键琴，使之复活成为了现代的乐器。1913年－1919年间在[柏林高等音乐学校执教](../Page/柏林高等音乐学校.md "wikilink")，1925年返回巴黎，积极推广和从事大键琴的演奏。1940年由于[德国军队的占领她和助理兼伴侣Denise](../Page/德国.md "wikilink")
Restout前往[瑞士避难](../Page/瑞士.md "wikilink")\[2\]，次年移居[美国](../Page/美国.md "wikilink")。1941年12月她抵达纽约，来到美国时基本上没有资产。\[3\]1949年她定居在[康涅狄格州](../Page/康涅狄格州.md "wikilink")，并开始巡回演出。她最后一次公开演出是在1954年。\[4\]1959年8月16日她在家中去世，得年80岁。她的女助理兼伴侣Denise
Restout编辑和翻译了她的音乐作品，在1964年出版。\[5\]

## 参考资料

## 外部链接

  - Smith, Patricia Juliana [Landowska
    profile](http://www.glbtq.com/arts/landowska_w.html), GLBTQ.com
    (2002)

  - ["Wanda Landowska
    biography"](http://www.naxos.com/artistinfo/Wanda_Landowska_1432/1432.htm),
    Naxos.com

  - [The Interpretation of Bach's
    Works](http://www.usc.edu/dept/polish_music/PMJ/issue/6.1.03/landowskabach.html)
    by Wanda Landowska (translated by Edward Burlingame Hill)

  - [Wanda Landowska's commentaries to Bach's *The Well Tempered
    Clavier*](http://martinwguy.co.uk/martin/doc/Landowska/WTC.html)

  -
[L](../Category/波蘭音樂家.md "wikilink")
[Category:法國音樂家](../Category/法國音樂家.md "wikilink")
[Category:猶太古典音樂家](../Category/猶太古典音樂家.md "wikilink")
[Category:LGBT音樂家](../Category/LGBT音樂家.md "wikilink")
[Category:波蘭猶太人](../Category/波蘭猶太人.md "wikilink")
[Category:法國猶太人](../Category/法國猶太人.md "wikilink")
[Category:LGBT猶太人](../Category/LGBT猶太人.md "wikilink")
[Category:歸化法國公民](../Category/歸化法國公民.md "wikilink")

1.

2.

3.
4.

5.