**让·奥古斯特·多米尼克·安格尔**（**Jean Auguste Dominique
Ingres**，）是[法国画家](../Page/法国.md "wikilink")，[新古典主义画派的最后一位领导人](../Page/新古典主义画派.md "wikilink")，他和[浪漫主义画派的杰出代表](../Page/浪漫主义画派.md "wikilink")[欧仁·德拉克罗瓦之间的著名争论震动了整个法国画坛](../Page/欧仁·德拉克罗瓦.md "wikilink")。

安格尔的画风线条工整，轮廓确切，色彩明晰，构图严谨，对后来许多画家如[德加](../Page/德加.md "wikilink")、[雷诺阿](../Page/皮耶-奧古斯特·雷諾瓦.md "wikilink")、甚至[毕加索都有影响](../Page/毕加索.md "wikilink")。

## 生平

安格尔出生于法国南部加龙河上游的[塔恩-加龙省首府](../Page/塔恩-加龙省.md "wikilink")[蒙托邦](../Page/蒙托邦.md "wikilink")，父亲是一位肖像画家和音乐家，是他的[艺术启蒙教师](../Page/艺术.md "wikilink")，1791年他入图卢兹学院学习[绘画](../Page/绘画.md "wikilink")；1797年到[巴黎进入古典主义画派的主要代表人](../Page/巴黎.md "wikilink")[雅克-路易·大卫的画室](../Page/雅克-路易·大卫.md "wikilink")；1799年考入美术学院油画系；1801年获一等[罗马奖](../Page/羅馬大獎.md "wikilink")；1806年到[罗马设画室](../Page/罗马.md "wikilink")，创作了大量的作品；1820年迁到[佛罗伦萨](../Page/佛罗伦萨.md "wikilink")，接受了大批肖像画的订作；1824年回到巴黎，立即获得很高荣誉；1825年被选为[法蘭西學院院士](../Page/法蘭西學院_\(機構\).md "wikilink")；1829年任美术学院院长；1835年再次回到罗马，创作了一系列肖像[素描和油画](../Page/素描.md "wikilink")；1841年回巴黎，继续创作多幅巨作。

安格尔一生最崇拜[拉斐尔和古代大师](../Page/拉斐尔.md "wikilink")，直到87岁高龄还在研究拉斐尔的作品，其最后一件作品就是根据[乔托的](../Page/乔托.md "wikilink")[构图画的](../Page/构图.md "wikilink")。他的主要作品有《[土耳其浴女](../Page/土耳其浴女.md "wikilink")》、《[泉](../Page/泉_\(安格尔\).md "wikilink")》、《[路易十三世的宣誓](../Page/路易十三世的宣誓.md "wikilink")》、《[奧德利斯克與奴隸](../Page/奧德利斯克與奴隸.md "wikilink")》以及一系列肖像画和大量的素描，还有类似[中国画](../Page/中国画.md "wikilink")[白描的只用线描钩轮廓的作品](../Page/白描.md "wikilink")。

安格尔的声誉如日中天时，也正是古典主义面临终结，浪漫主义崛起的时代，他和新生的浪漫主义代表人德拉克罗瓦之间发生许多次辩论，浪漫主义强调[色彩的运用](../Page/色彩.md "wikilink")，古典主义则强调轮廓的完整和构图的严谨，安格尔把持的美术学院对新生的各种画风嗤之以鼻，形成学院派风格。

安格尔生前享有很大的声誉，死后安葬在[巴黎著名的拉雪兹神父公墓](../Page/巴黎.md "wikilink")。

## 畫廊

<File:Ingres> Le bain
turc.jpg|《[土耳其浴女](../Page/土耳其浴女.md "wikilink")》（*The
Turkish Bath*），1862年，收藏於[羅浮宮](../Page/羅浮宮.md "wikilink")
<File:Jean-Auguste-Dominique> Ingres - La Baigneuse
Valpinçon.jpg|《[瓦平松的浴女](../Page/瓦平松的浴女.md "wikilink")》（*The
Bather*），1808年，收藏於[羅浮宮](../Page/羅浮宮.md "wikilink") <File:Ingre>, Grande
Odalisque.jpg|《[大宮女](../Page/大宮女.md "wikilink")》（*The Grand
Odalisque*），1814年，收藏於[羅浮宮](../Page/羅浮宮.md "wikilink")
<File:Dominique> Ingres - Mme
Moitessier.jpg|《[靜坐的墨瓦特雪夫人](../Page/靜坐的墨瓦特雪夫人.md "wikilink")》（*Madame
Moitessier Seated*），1856年，收藏於[英國國家美術館](../Page/英國國家美術館.md "wikilink")
<File:Louis-Francois>
Bertin.jpg|《[路易·伯坦像](../Page/路易·伯坦像.md "wikilink")》（*Louis-Francois
Bertin*），1832年，收藏於[羅浮宮](../Page/羅浮宮.md "wikilink")
<File:Jean_Auguste_Dominique_Ingres_-_Roger_Delivering_Angelica.jpg>|《[營救安潔麗卡的羅傑](../Page/營救安潔麗卡的羅傑.md "wikilink")》（*Rudiger
Freeing Angelica*），1819年，收藏於[羅浮宮](../Page/羅浮宮.md "wikilink") <File:Jean>
Auguste Dominique Ingres - The Spring - Google Art Project
2.jpg|《[泉](../Page/泉_\(安格爾\).md "wikilink")》（*The
Source*），1856年，收藏於[奧塞美術館](../Page/奧塞美術館.md "wikilink")
<File:Jean-Paul_Flandrin_-_Odalisque_with_Slave_-_Walters_37887.jpg>|《[宮女和奴僕](../Page/宮女和奴僕.md "wikilink")》（*Odalisque
with a
Slave*），1840年，收藏於[美國](../Page/美國.md "wikilink")[麻州](../Page/麻州.md "wikilink")[劍橋](../Page/劍橋.md "wikilink")[哈佛大學](../Page/哈佛大學.md "wikilink")[費哥美術館](../Page/費哥美術館.md "wikilink")
<File:IngresOdipusAndSphinx.jpg>|《[俄狄浦斯和斯芬克斯](../Page/俄狄浦斯和斯芬克斯.md "wikilink")》（*Oedipus
and the Sphinx*），1808年，收藏於[羅浮宮](../Page/羅浮宮.md "wikilink")
<File:Jean_auguste_dominique_ingres_raphael_and_the_fornarina.jpg>|《[拉斐爾和弗馬里娜](../Page/拉斐爾和弗馬里娜.md "wikilink")》（*Raphael
and the
Fornarina*），1811年—1812年，收藏於[美國](../Page/美國.md "wikilink")[麻州](../Page/麻州.md "wikilink")[劍橋](../Page/劍橋.md "wikilink")[哈佛大學](../Page/哈佛大學.md "wikilink")[費哥美術館](../Page/費哥美術館.md "wikilink")

[Category:法國新古典主義畫家](../Category/法國新古典主義畫家.md "wikilink")
[Category:學院藝術](../Category/學院藝術.md "wikilink")
[Category:雅克-路易·大衛的學生](../Category/雅克-路易·大衛的學生.md "wikilink")
[Category:安葬于拉雪兹神父公墓者](../Category/安葬于拉雪兹神父公墓者.md "wikilink")