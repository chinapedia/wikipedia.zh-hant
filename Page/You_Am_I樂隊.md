**You Am
I樂隊**，是一個來自[澳洲](../Page/澳洲.md "wikilink")[悉尼的搖滾樂隊](../Page/悉尼.md "wikilink")，成員有Tim
Rogers,Davey Lane,Rusell Hopkinson和ANdy Kent
他們暫時是[澳洲唯一一隊有](../Page/澳洲.md "wikilink")3張大碟有拿個[澳洲排行磅冠軍](../Page/澳洲.md "wikilink")。

## 成員

[安德魯.肯特](../Page/安德魯.肯特.md "wikilink")（Andy
Kent）-[貝斯](../Page/貝斯.md "wikilink")

羅素’何奇信（Russell Hopkinson）-[鼓](../Page/鼓.md "wikilink")

添’羅渣斯（Tim Rogers）-[歌手](../Page/歌手.md "wikilink")

大衛’凌（David Lane）-[吉他](../Page/吉他.md "wikilink")

## 關於

早期，[添’羅渣斯與同学力克](../Page/添’羅渣斯.md "wikilink")’廸输拿和兄長斉米在1989年12月開始組成。樂隊的名字在喝醉后的午夜錄音時想到的。但這組合在1990年后己解散。(You
Am I 1993年首張大碟的歌 “Jaimme’s Got A Gal”
中提及添的兄長為何退出，后來找到樂隊的前錄音員安廸’肯特彈貝斯和马克’
吞拉利當鼓手。

在[九十年代早期樂隊己發出几張細碟](../Page/九十年代.md "wikilink")，但最大突破是在1993年的Big Day
Out演出時被[青年会的利](../Page/青年会.md "wikilink")’ 朗拿道看上。然后在利’
朗拿道當[監制下発出首張大碟](../Page/監制.md "wikilink")
“Sound As Ever”。大碟在1993年[澳洲音樂大獎中贏得最佳另類音樂獎](../Page/澳洲.md "wikilink")。
樂隊跟了 Ra Record 簽約，是一個 rootArt Record
期下一個另類搖滾部門，在[美国己](../Page/美国.md "wikilink")
Restless Record 發行。

在首張大碟發行后，吞拉利退出，他被[羅素’何奇信](../Page/羅素’何奇信.md "wikilink")（前幼椎園罪案鼓手），造出了直至今天的核心組合，由始開始，樂隊加入了[大衛’凌當第二結他](../Page/大衛’凌.md "wikilink")，原因是樂隊對大衛的樂隊
The Pictures
的作品感到興趣，和在樂隊的官方網站用高精確度去抄寫樂團的歌譜，但大衛在訪問中提及到他想退出原因是他認為他破壞了自己喜歡的樂隊是因為自己的加入。原因是在首次演出時在歌曲里加了太多自己的[結他獨奏](../Page/結他.md "wikilink")。

You Am I 在九十年代中期開始在[美国演出](../Page/美国.md "wikilink")，包括在羅拉巴洛剎跟
[Soundgarden演出](../Page/Soundgarden.md "wikilink")，當中也有 Redd Kross, [The
Strokes](../Page/The_Strokes.md "wikilink") 和Smoking Pipes.（但
Soundgarden
也在[一九九四年](../Page/一九九四年.md "wikilink")[澳洲](../Page/澳洲.md "wikilink")
Big Day Out 跟 You Am I
演出），就算在[美国成功度不大](../Page/美国.md "wikilink")，但在[澳洲己很有名](../Page/澳洲.md "wikilink")。

You Am I 的第二和第三唱片令 You Am I
更成功，兩隻碟也拿了[澳洲](../Page/澳洲.md "wikilink")[音樂大獎的第一名](../Page/音樂.md "wikilink")。而第三大碟更是第一次拿到銷量榜第一。碟內的歌有了十二線[結他的民歌味和](../Page/結他.md "wikilink")[六十年代流行曲風格](../Page/六十年代.md "wikilink")，而不再是一般[搖滾味](../Page/搖滾.md "wikilink")。

少數[民曲味較重的是他們](../Page/民曲.md "wikilink")1998年的第四張大碟，包括 “Heavy
Heart”，一首摥心，邊緣味和[KTV](../Page/KTV.md "wikilink")
風格的情歌，內容說關於男女分手。1999年，主音[添’
羅渣斯首次出個人大碟](../Page/添’_羅渣斯.md "wikilink")
“What Rhymes With Car and Girls?”

由於[唱片公司問題](../Page/唱片公司.md "wikilink")，You Am I
在2001年才發出第5隻大碟，而[大衛’凌已第一次已正式成員在唱片中出現](../Page/大衛’凌.md "wikilink")。2002年發行第６張大碟，但不及過去成功。當時己單曲
“Who Put The Devil In You” 放在網上給人下載。雖然在1998年 rootArt Records
己被[BMG](../Page/BMG.md "wikilink") 收購，但You Am I 在2003年才加入
[BMG](../Page/BMG.md "wikilink")。

## 影響力

You Am I
對現代搖滾影響深遠，大部份[九十年代和](../Page/九十年代.md "wikilink")[二千年代的](../Page/二千年代.md "wikilink")[澳洲](../Page/澳洲.md "wikilink")[樂隊直言受到](../Page/樂隊.md "wikilink")
You Am I
的影響，特別是[JET樂隊](../Page/Jet_\(樂隊\).md "wikilink")，[狼母](../Page/狼母.md "wikilink")，[奇利士般樂隊和](../Page/奇利士般樂隊.md "wikilink")[银椅樂隊等后輩](../Page/银椅樂隊.md "wikilink")。

## 個人發展和前成員

在2002至2006年之間，[大衛’凌和](../Page/大衛’凌.md "wikilink")[添’羅渣斯主要集中在個人作品上](../Page/添’羅渣斯.md "wikilink")，大衛在2004年以
The Wrights
成員身份在[澳洲音樂大獎典禮演出](../Page/澳洲.md "wikilink")，隊內其他成員全是[澳洲名樂隊成員](../Page/澳洲.md "wikilink")。大衛的樂隊，The
Pictures，首次在2005年出碟，羅渣斯的另外樂隊，Tim Rogers and the Temperance，在2004年出了大碟
Spit Polish 和2005年雙大碟 Dirty Ron/Ghost Songs.

2005年后期，You Am I 在基格’ 衛以斯監制下發行第七張大碟，名為
“罪犯”，大碟名相信是[英国人對](../Page/英国人.md "wikilink")[澳洲人的揶揄](../Page/澳洲人.md "wikilink")。因為樂隊成員的種族背景，也可能是[九十年代中期樂隊的別名](../Page/九十年代.md "wikilink")。“罪犯”
在於2006年5月13日推出，以[維珍唱片發行](../Page/維珍.md "wikilink")，在[美国以](../Page/美国.md "wikilink")
Yep Roc Record 出版。You Am I
在2007年主力在[美国演出和作](../Page/美国.md "wikilink")[宣傳](../Page/宣傳.md "wikilink")。

斉米和力克’ 廸输拿是樂隊早期的成員，在1989年12月到1990年尾內，沒有一首歌錄到。 馬克’ 吞拉利是樂隊第一個正式鼓手，和唯一在 You
Am I
的[重金属風格鼓手和使用雙低音鼓](../Page/重金属音乐.md "wikilink")。除了是位打鼓好手，馬克不是一個粗獷的人，退出也是不可避免。

基利格’希冶葛曾在1999年前替 You Am
I在演唱會上付責[結他和](../Page/結他.md "wikilink")[電子琴](../Page/電子琴.md "wikilink")。雖然在1999年前是付責第二[結他的位置](../Page/結他.md "wikilink")，但不像大衛’凌一樣，他從不是樂隊里的一位[正式成員](../Page/正式.md "wikilink")。主音羅渣斯對希治葛不滿是他認為希治葛的[電子琴聲令動樂隊的風格不自然](../Page/電子琴.md "wikilink")

## 大碟

  - *[Sound As Ever](../Page/Sound_As_Ever.md "wikilink")*（rooArt
    Records, 1993年11月）
  - *[Hi Fi Way](../Page/Hi_Fi_Way.md "wikilink")*（rooArt Records,
    1995年2月）
  - *[Hourly, Daily](../Page/Hourly,_Daily.md "wikilink")*（rooArt
    Records, 1996年7月）
  - *[\#4 Record](../Page/No._4_Record.md "wikilink")*（rooArt Records,
    1998年4月）
  - *[...Saturday Night, 'Round
    Ten](../Page/...Saturday_Night,_'Round_Ten.md "wikilink")*（rooArt
    Records, 1999年11月）
  - *[Dress Me Slowly](../Page/Dress_Me_Slowly.md "wikilink")*（BMG
    Australia, 2001年4月）
  - *[Deliverance](../Page/Deliverance_\(You_Am_I_album\).md "wikilink")*（BMG
    Australia, 2002年9月）
  - *[No After You Sir...: An Introduction to You Am
    I](../Page/No_After_You_Sir...:_An_Introduction_to_You_Am_I.md "wikilink")*（Transcopic
    Records, 2003年5月）
  - *[The Cream & The Crock - The Best Of You Am
    I](../Page/The_Cream_&_The_Crock_-_The_Best_Of_You_Am_I.md "wikilink")*（BMG
    Australia, 2003年9月15日）
  - *[Convicts](../Page/Convicts_\(You_Am_I_album\).md "wikilink")*（Virgin/EMI,
    2006年5月13日／Yep Roc Records, 2007年1月23日）

## 外部連結

  - [www.youami.com.au](http://www.youami.com.au/)
  - [www.youami.net](http://www.youami.net/)
  - [You Am I at
    Musichead.com.au](https://web.archive.org/web/20070927030122/http://www.musichead.com.au/site/artist.asp?actID=53500)
  - [Trouser Press
    entry](http://www.trouserpress.com/entry.php?a=you_am_i)
  - [Howlspace
    bio](https://web.archive.org/web/20070927025641/http://www.whiteroom.com.au/howlspace/en/youami/youami.htm)

[Category:澳大利亚乐团](../Category/澳大利亚乐团.md "wikilink")
[Category:澳大利亚搖滾乐团](../Category/澳大利亚搖滾乐团.md "wikilink")
[Category:另類摇滚乐团](../Category/另類摇滚乐团.md "wikilink")