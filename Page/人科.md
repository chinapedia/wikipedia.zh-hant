**人科**（[学名](../Page/学名.md "wikilink")：，又称**猩猩科**）是[生物分類學中](../Page/生物分類學.md "wikilink")[靈長目一科](../Page/灵长目.md "wikilink")。本科除了[智人之外](../Page/智人.md "wikilink")，還包括所有絕種的人類祖先和近親及所有猩猩。在早期的分类法中，人科仅包括智人及其已灭绝的祖先，而所有猩猩都被分入猩猩科，后来的研究厘清各物种的演化关系，**猩猩科**成为了人科的异名。

[长臂猿科是本科最亲近的](../Page/长臂猿科.md "wikilink")[旁系群](../Page/旁系群.md "wikilink")，两者组成[人猿总科](../Page/人猿总科.md "wikilink")。现时，人科一共分為四屬八种（具体种数有微小争议）。

[國際自然保護聯盟分类中](../Page/國際自然保護聯盟.md "wikilink")，[智人是人科](../Page/智人.md "wikilink")[物种中唯一](../Page/物种.md "wikilink")[無危物种](../Page/無危物种.md "wikilink")。

## 演化史

## 分类

### 系统发生学

人科现存各物种和他们的[旁系姐妹群](../Page/旁系群.md "wikilink")：

[\*](../Category/人科.md "wikilink")
[Category:猿](../Category/猿.md "wikilink")