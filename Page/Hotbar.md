**Hotbar**是一個由Hotbar.com, Inc.製作的軟體，被指會對使用者的電腦造成不良影響。

## 聲稱的功能

它聲稱是一個[微軟](../Page/微軟.md "wikilink")[Internet
Explorer](../Page/Internet_Explorer.md "wikilink")、[Outlook及](../Page/Microsoft_Office_Outlook.md "wikilink")[Outlook
Express的插件](../Page/Outlook_Express.md "wikilink")，在這些軟件的畫面上新增一條[工具列](../Page/工具列.md "wikilink")，亦可為這些軟件改變[皮膚](../Page/皮膚.md "wikilink")、及提借[表情符號圖案](../Page/表情符號.md "wikilink")、100張[桌面牆紙及](../Page/桌面牆紙.md "wikilink")[電子賀卡以供使用](../Page/電子賀卡.md "wikilink")，與及提供即時的[天氣](../Page/天氣.md "wikilink")、[搜索](../Page/搜索.md "wikilink")、[黃頁以及更多](../Page/黃頁.md "wikilink")。

Hotbar聲稱可以透過[控制台中的](../Page/控制台.md "wikilink")[添加/刪除程序](../Page/添加/刪除程序.md "wikilink")“方便地刪除”這些功能，也不會泄漏使用者的私隱。

## 影響

Hotbar是一個[廣告軟體](../Page/廣告軟體.md "wikilink")，因為它經常透過使用彈出式視窗來顯示廣告訊息。它亦是一個[間諜軟體](../Page/間諜軟體.md "wikilink")，會監察用戶瀏覽的網頁。Hotbar還會悄悄地連接軟體的[伺服器](../Page/伺服器.md "wikilink")，並下載執行程序來安裝，而用戶全不知情。事實上，上述這些行為都有在Hotbar安裝前的[使用協議裡透露](../Page/使用協議.md "wikilink")。

此外，Hotbar會將位址欄和瀏覽器中的搜尋器默認指向它的搜尋引擎。

## 支援性

Hotbar完全不兼容[Internet Explorer
7之後的所有版本](../Page/Internet_Explorer_7.md "wikilink")。

## 反安裝

使用者可以通過在控制台中的添加/刪除程序來刪除，但它留下的一些檔案必須手動刪除。刪除程序後​​，使用者需要透過[註冊表編輯器](../Page/註冊表編輯器.md "wikilink")，刪除一些機碼。\[1\]

使用者也可以通過[防毒軟件刪除Hotbar](../Page/防毒軟件.md "wikilink")，例如[Microsoft
Defender](../Page/Microsoft_Defender.md "wikilink")、[SpyBot和](../Page/SpyBot.md "wikilink")[Norton
Antivirus](../Page/Norton_Antivirus.md "wikilink")。但是，有些工具不能夠刪除[Windows註冊表中的機碼](../Page/Windows註冊表.md "wikilink")。部份軟件是專門用作刪除Hotbar的\[2\]。

## 參考資料

## 參看

  - [廣告軟件](../Page/廣告軟件.md "wikilink")
  - [間諜軟件](../Page/間諜軟件.md "wikilink")
  - [電腦病毒](../Page/電腦病毒.md "wikilink")

## 外部連結

  - [More information about Hotbar on
    Doxdesk.com](http://www.doxdesk.com/parasite/Hotbar.html)
  - [Information on how to remove
    Hotbar](http://sarc.com/avcenter/venc/data/adware.hotbar.html)
  - [Official site of
    Hotbar](https://web.archive.org/web/20100408083319/http://www.hotbar.com/)

[Category:間諜軟件](../Category/間諜軟件.md "wikilink")

1.
2.  [HotBar Adware Removal
    Tool 1.0](http://www.softpedia.com/get/Internet/Popup-Ad-Spyware-Blockers/HotBar-Adware-Removal-Tool.shtml)