[Shanghai_Science_and_Technology_Museum_Plaza_Surrounding.jpg](https://zh.wikipedia.org/wiki/File:Shanghai_Science_and_Technology_Museum_Plaza_Surrounding.jpg "fig:Shanghai_Science_and_Technology_Museum_Plaza_Surrounding.jpg")
[上海科技馆内地球家园_-_panoramio.jpg](https://zh.wikipedia.org/wiki/File:上海科技馆内地球家园_-_panoramio.jpg "fig:上海科技馆内地球家园_-_panoramio.jpg")
[Shanghai_Science_&_Technology_Museum.JPG](https://zh.wikipedia.org/wiki/File:Shanghai_Science_&_Technology_Museum.JPG "fig:Shanghai_Science_&_Technology_Museum.JPG")远眺上海科技馆\]\]

**上海科技館**位於[中國](../Page/中國.md "wikilink")[上海市](../Page/上海市.md "wikilink")[浦东新区](../Page/浦东新区.md "wikilink")[花木行政文化中心區的](../Page/花木街道.md "wikilink")[世纪大道](../Page/世纪大道_\(上海\).md "wikilink")2000號，是以科普教育為主的展覽館，以「自然·人·科技」為主題。

## 简介

上海科技館佔地6萬8000平方米，建築面積9萬8000平方米，總投資額15億[人民幣](../Page/人民幣.md "wikilink")。科技館由美國RTKL國際有限公司及[上海建築設計研究院合作設計](../Page/上海建築設計研究院.md "wikilink")。2001年10月21日，上海科技館舉行了[亞太經濟合作組織第九次領導人非正式會議](../Page/亞太經濟合作組織.md "wikilink")，[江泽民签名留念](../Page/江泽民.md "wikilink")\[1\]。2001年12月18日正式對外開放。

科技馆还有2个分馆，分别是延安东路260号的[自然博物分馆](../Page/上海自然博物馆.md "wikilink")（已经关闭）和龙吴路1102号的标本楼\[2\]。

2012年，上海科技馆被[国家文物局核定为第二批](../Page/国家文物局.md "wikilink")[国家一级博物馆](../Page/国家一级博物馆.md "wikilink")\[3\]。

## 設施

上海科技館目前有13個展區對外開放，包括：

一樓：

  - 生物萬象
  - 地殼探密
  - 設計師摇籃
  - 彩虹兒童樂園
  - 智慧之光
  - 動物世界展

二樓：

  - 地球家園
  - 機械人世界
  - 信息時代
  - 蜘蛛展

三樓：

  - 探索之光
  - 人與健康
  - 宇航天地

🔍🔍🔍詳情請參閱http://m.sstm.org.cn/show/index

上海科技館共有4個影院，每年可放映8,000場次：

  - [IMAX立體巨幕影院](../Page/IMAX.md "wikilink")
  - 球幕影院
  - 四維影院
  - 太空影院

## 参观信息

  - 开放时间：周二至周日9:00-17:15，法定节假日开放，时间为8:45-18:30。
  - 公共交通：[上海軌道交通二號線](../Page/上海軌道交通二號線.md "wikilink")[上海科技館站](../Page/上海科技館站.md "wikilink")，上海公交593，638，640，794，936路。

## 参考文献

## 外部链接

  - [上海科技館](https://web.archive.org/web/20130722234119/http://www.sstm.org.cn/)
  - [上海市爱国主义教育基地官网](http://www.sh-aiguo.gov.cn)

## 参见

  - [上海市科普教育基地](../Page/上海市科普教育基地.md "wikilink")
  - [世纪公园](../Page/世纪公园.md "wikilink")
  - [世纪广场](../Page/世纪广场.md "wikilink")

{{-}}

[沪](../Category/国家5A级旅游景区.md "wikilink")
[Category:上海市爱国主义教育基地](../Category/上海市爱国主义教育基地.md "wikilink")
[Category:中国科学博物馆](../Category/中国科学博物馆.md "wikilink")
[Category:上海各博物馆](../Category/上海各博物馆.md "wikilink")
[Category:上海科技](../Category/上海科技.md "wikilink")

1.
2.
3.