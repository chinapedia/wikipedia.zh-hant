[Discurso_de_Perón.jpg](https://zh.wikipedia.org/wiki/File:Discurso_de_Perón.jpg "fig:Discurso_de_Perón.jpg")创始人[胡安·裴隆](../Page/胡安·裴隆.md "wikilink")\]\]

**裴隆主义**（），又称**正义主义**（），是[阿根廷](../Page/阿根廷.md "wikilink")[正义党创始人](../Page/正义党.md "wikilink")、前[总统](../Page/阿根廷总统.md "wikilink")[胡安·裴隆提出的一种属于](../Page/胡安·裴隆.md "wikilink")“[第三位置](../Page/第三位置.md "wikilink")”的[政治](../Page/政治.md "wikilink")[理念](../Page/理念.md "wikilink")（[主义](../Page/主义.md "wikilink")），以[国家复兴和](../Page/国家.md "wikilink")[民族](../Page/民族.md "wikilink")[解放为主要内容](../Page/解放.md "wikilink")，同时主张[中央集权](../Page/中央集权.md "wikilink")。\[1\]

## 发展

裴隆在1938到1940年间在阿根廷驻[意大利大使馆任武官](../Page/意大利.md "wikilink")，当时[法西斯主义方兴未艾](../Page/法西斯主义.md "wikilink")，[墨索里尼正动员民众参与重建意大利社会](../Page/墨索里尼.md "wikilink")。这一段经历“开阔了他的眼界”，对裴隆的思想产生了深远的影响。\[2\]

裴隆在1946年上任后采取有利于[工人阶级的政策](../Page/工人阶级.md "wikilink")。他大规模扩大了加入工会的工人数量，帮助建立了[劳工总同盟](../Page/劳工总同盟.md "wikilink")。他将此称为介于资本主义和社会主义之间的“[第三位置](../Page/第三位置.md "wikilink")”，后被人称为“**裴隆主义**”。他强烈地反对[美国和](../Page/美国.md "wikilink")[英国](../Page/英国.md "wikilink")，没收了这两个国家在[阿根廷的大量资产](../Page/阿根廷.md "wikilink")。他致力于推进国家的工业化，于1947年颁布了旨在发展国有工业的第一个五年计划。裴隆政府的特點就是混合了[社團主義和社會福利措施](../Page/社團主義.md "wikilink")。\[3\]
裴隆认为：“无论是资本主义还是共产主义都是已经过时了的制度。资本主义通过资本剥削人，而共产主义是通过国家剥削人，两者通过不同的制度同样损害人。”“我们若选择其中的任何一种制度，都不能为我国人民带来应得的福利，因此我们决定创立一个第三立场”。\[4\]

第三位置与社團主義并无必然的联系，但选择社團主義却必定是持第三位置立场的。尽管裴隆主义政权没有公开表明它是社團主義的，但意大利法西斯主义的社團主義制度对裴隆政权的影响是明确的。裴隆本人也承认法西斯主义对他的影响，他说：“对我来说，一切都开始于1938年1月”，
在[都灵所学的法西斯主义的政治经济课程](../Page/都灵.md "wikilink")，“为我指点了迷津”，法西斯意大利是“第一个[国家社会主义](../Page/国家社会主义.md "wikilink")，……这是一条在俄国社会主义和美国资本主义之间的第三条道路”。\[5\]
裴隆主义认为，第三位置学说的目标是实现人类社会中人的幸福，而达到这一目标的方法是协调物质主义、理想主义、个人主义和集体主义这几种力量，并以基督教的精神珍视每一种力量。\[6\]
他的思想成为了阿根廷政党中的主流。

尽管阿根廷[種族偏見现象普遍](../Page/種族主義.md "wikilink")，但庇隆主義并无種族偏見内容。对[同性恋的宽容态度也与法西斯主义者决然不同](../Page/同性恋.md "wikilink")。

## 相关条目

  - [胡安·庇隆](../Page/胡安·庇隆.md "wikilink")
  - [正义党](../Page/正义党.md "wikilink")
  - [第三位置](../Page/第三位置.md "wikilink")

## 参见

  - [社團主義](../Page/社團主義.md "wikilink")
  - [法西斯主义](../Page/法西斯主义.md "wikilink")
  - [国家社会主义](../Page/国家社会主义.md "wikilink")

## 注释

[Category:政治理論](../Category/政治理論.md "wikilink")
[Category:第三位置](../Category/第三位置.md "wikilink")
[Category:阿根廷政治](../Category/阿根廷政治.md "wikilink")

1.  Crassweller, David. Perón and the Enigmas of Argentina. W.W. Norton
    and Company. 1987: 221. ISBN 0-393-30543-0

2.  Robert J. Alexander, "Juan Domingo Peron: A History" Westview Press,
    1979, P.21-22

3.
4.  肖楠等，当代拉丁美洲政治思潮，北京：东方出版社，P.243-244，1988

5.
6.  复旦大学历史系拉丁美洲教研室编译：《裴隆与阿根廷》，第174～175页，上海人民出版社，1974