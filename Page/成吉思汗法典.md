《**成吉思汗法典**》（），即**大扎撒**，是世界上第一套應用範圍最廣泛的[成文法典](../Page/成文法.md "wikilink")，同時也是世界上最早的[憲法性文件](../Page/憲法.md "wikilink")\[1\]，於1206年由[蒙古帝國的](../Page/蒙古帝國.md "wikilink")[成吉思汗頒布實施](../Page/成吉思汗.md "wikilink")。法典將[行政權及](../Page/行政權.md "wikilink")[司法權分開](../Page/司法權.md "wikilink")，建立了一套有部落[民主色彩的](../Page/民主.md "wikilink")[君主政體制度](../Page/君主政體.md "wikilink")。這種以兩權制約的[判例制度](../Page/判例.md "wikilink")，比[英國](../Page/英國.md "wikilink")19世紀的判例制度早約600年。

不過，《成吉思汗法典》的古本於[元末](../Page/元朝.md "wikilink")[明初的戰亂入面已經散失](../Page/明朝.md "wikilink")，結果失傳了600多年。雖然法典的不少部份仍然可以零碎地在多份文獻內找到，但由於這些文獻數量和種類均很多，又涉及[英文](../Page/英文.md "wikilink")、[古體蒙古文](../Page/古體蒙古文.md "wikilink")、[現代蒙古文](../Page/現代蒙古文.md "wikilink")、[漢文等達](../Page/漢文.md "wikilink")8種文字，所以要進行研究具有很高難度，結果一直未有學者或者研究機構可以完整重構這一部著作。

直到2007年8月，[中華人民共和國](../Page/中華人民共和國.md "wikilink")[內蒙古自治区人民政府法制办公室所屬的內蒙古典章法學與社會學研究所經過](../Page/內蒙古自治区人民政府.md "wikilink")14個月整理，再重新出版一套最為完整的版本\[2\]。

法典內有不少不同的法律規範，其中包括[同性戀問題](../Page/同性戀.md "wikilink")、保護[草原等自然資源](../Page/草原.md "wikilink")、保護[野生動物](../Page/野生動物.md "wikilink")、規範[信託投資等等](../Page/信託.md "wikilink")\[3\]。根據內蒙古自治區政府法制辦《法典》版本，「男子之間雞姦的，並處[死刑](../Page/死刑.md "wikilink")」；「草綠後挖坑致使草原被損壞的，失火致使草原被燒的，對全家處死刑」；「狩獵結束後，要對傷殘的、幼小的和雌性的獵物進行放生」；「以信託資金經商累計三次虧本的，處死刑」。

## 参考文献

<div class="references-small">

<references />

</div>

[Category:历史上的宪法](../Category/历史上的宪法.md "wikilink")
[Category:蒙古帝國](../Category/蒙古帝國.md "wikilink")
[Category:散佚書籍](../Category/散佚書籍.md "wikilink")

1.  [世界上最早的憲法《成吉思汗法典》出版發行](http://news.xinhuanet.com/legal/2007-08/29/content_6623859.htm)
2.  [《成吉思汗法典》重現人間](http://www.mpinews.com/htm/INews/20070829/ca31442a.htm)
    ，《明報》，2007年8月29日
3.  [《成吉思汗法典》重見天日](http://news.wenweipo.com/2007/08/29/IN0708290079.htm)，《文匯報》，2007年8月29日