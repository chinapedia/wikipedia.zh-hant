**GameFAQs**是一个关于[电子游戏的流行网站](../Page/电子游戏.md "wikilink")，从1995年11月开始提供游戏的FAQS和[攻略](../Page/攻略.md "wikilink")，是繼[IGN之后网上第二大的游戏信息库](../Page/IGN.md "wikilink")。它的创始人是Jeff
"CJayC" Veasey。

## 参见

  - [游戏基地](../Page/游戏基地.md "wikilink")（**Gamebase**）

## 參考資料

## 外部連結

  - [GameFAQs](http://www.gamefaqs.com/)

[Category:社群網站](../Category/社群網站.md "wikilink")
[Category:電子遊戲網站](../Category/電子遊戲網站.md "wikilink")
[Category:CNET](../Category/CNET.md "wikilink")
[Category:1995年建立的网站](../Category/1995年建立的网站.md "wikilink")