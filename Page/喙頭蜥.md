**喙頭蜥**是**楔齒蜥屬**（[学名](../Page/学名.md "wikilink")：）动物的通称，因牙齒構造也稱為**楔齒蜥**。其他名稱也有**鱷蜥**或**紐西蘭鱷蜥**，亦可稱為**刺背鱷蜥**，僅分佈於[紐西蘭](../Page/紐西蘭.md "wikilink")[科克海峽中的數個小島上](../Page/科克海峽.md "wikilink")，是[喙頭目僅存的成員](../Page/喙頭目.md "wikilink")，只有1科1屬2種\[1\]。由於[鼬類與](../Page/鼬.md "wikilink")[老鼠的引進](../Page/老鼠.md "wikilink")，已瀕臨絕種。額頭有（又译作“顶眼”，俗稱“第三隻眼”）的痕跡，是非常原始的[蜥蜴](../Page/蜥蜴.md "wikilink")，被認為是[活化石](../Page/活化石.md "wikilink")\[2\]。

## 簡介

喙頭蜥新陳代謝很慢，性成熟時間與壽命也很長，據估計，牠們的壽命可能達到100年，而性成熟至少需要十年以上。
喙頭蜥的牙齿排列方式独一无二，下颚的一排牙齿紧紧咬合在上颚的两排牙齿之间。另外，蜥蜴的耳朵开口清楚可见，而喙頭蜥的耳孔则看不到。

## 参考文献

[Category:喙頭蜥目](../Category/喙頭蜥目.md "wikilink")
[Category:紐西蘭動物](../Category/紐西蘭動物.md "wikilink")
[Category:活化石](../Category/活化石.md "wikilink")

1.
2.