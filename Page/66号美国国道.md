[Amboy_route_66.JPG](https://zh.wikipedia.org/wiki/File:Amboy_route_66.JPG "fig:Amboy_route_66.JPG")
**66號美國國道**（，又譯為**美國66號公路**），又名**美國大街**（）、**母親之路**（）或**威爾·羅傑斯高速公路**（，命名自美國[諧星](../Page/諧星.md "wikilink")），是[美國歷史上主要的交通要道之一](../Page/美國.md "wikilink")。國道66號始建於1926年11月\[1\]，由[芝加哥一路連貫到](../Page/芝加哥.md "wikilink")[洛杉磯](../Page/洛杉磯.md "wikilink")。途經[伊利諾州](../Page/伊利諾州.md "wikilink")、[密蘇里州](../Page/密蘇里州.md "wikilink")、[堪薩斯州](../Page/堪薩斯州.md "wikilink")、[奧克拉荷馬州](../Page/奧克拉荷馬州.md "wikilink")、[德克薩斯州](../Page/德克薩斯州.md "wikilink")、[新墨西哥州](../Page/新墨西哥州.md "wikilink")、[亞利桑那州以及](../Page/亞利桑那州.md "wikilink")[加利福尼亞州](../Page/加利福尼亞州.md "wikilink")，全長\[2\]。因為在[州際公路興起之後重要性逐漸被取代利用率降低](../Page/州際公路.md "wikilink")，而在1985年6月27日被正式從[美國國道系統中刪除](../Page/美國國道.md "wikilink")\[3\]。它原來經過伊利諾州、密蘇里州、新墨西哥州和亞利桑那州的部分道路被劃為[國家風景道路](../Page/美國風景道路.md "wikilink")，稱作「66號歷史道路」（）\[4\]\[5\]。有幾個州將原來66號國道的部分路段納入州內公路系統，並仍然使用66號公路的編號和標誌.

## 歷史

### 興起

儘管美國66號公路直到1938年才鋪設完成，一般認為它是在1926年4月30日於[密蘇里州的](../Page/密蘇里州.md "wikilink")[-{zh-hans:斯普林菲爾德;zh-hk:春田;zh-tw:春田;}-誕生的](../Page/斯普林菲爾德_\(密蘇裡州\).md "wikilink")\[6\]。而在1927年，66號公路是美國最早的法律上認定的幾條公路編號之一。美國66號公路的大多路段平穩好走，因此往來旅客甚多，尤其深受卡車司機的青睞。

  - 1930年代的[黑色風暴事件時期](../Page/黑色風暴事件.md "wikilink")，許多家庭通過它遷至美國西部的加利福尼亞以農為生，開拓新生活。
  - 在[經濟大蕭條時期](../Page/經濟大蕭條.md "wikilink")，66號公路也給定居在它附近的人們帶來了希望：隨著客流量的增多，一些路邊小規模企業應運而生，例如加油站、餐館、汽車旅館等。
  - [第二次世界大戰期間](../Page/第二次世界大戰.md "wikilink")，因為加利福尼亞州與軍備生產相關行業的發展，更多的人通過66號公路向西遷移。原本就十分繁忙的公路，又成為了運輸軍用物資的主要道路之一，使其更加興盛。
  - 1950年代，66號公路成為了去洛杉磯度假者的首選線路。由於它橫穿[佩恩特沙漠](../Page/佩恩特沙漠.md "wikilink")，毗鄰[大峽谷](../Page/科羅拉多大峽谷.md "wikilink")，又途徑亞利桑那州的[隕石坑](../Page/巴林傑隕石坑.md "wikilink")，使得當地旅遊業迅速發展，這一現象又轉而促進了各種新興行業的興旺。
  - 這條公路也為許多連鎖賣場奠基，早在1920年代，隨著人口的增加，連鎖店開始在公路旁開設大型賣場以增加業務和銷售。

### 路線變化

美國66號公路多處路段曾經經歷了重大的變化。
[Route_66_2073773569_7b3fae3b91_b.jpg](https://zh.wikipedia.org/wiki/File:Route_66_2073773569_7b3fae3b91_b.jpg "fig:Route_66_2073773569_7b3fae3b91_b.jpg")

1930年，在[伊利諾伊州](../Page/伊利諾伊州.md "wikilink")[-{zh-hans:斯普林菲爾德;zh-hk:春田;zh-tw:春田;}-和](../Page/斯普林菲尔德_\(伊利诺伊州\).md "wikilink")[東聖路易之間](../Page/東聖路易斯_\(伊利諾州\).md "wikilink")，66號公路被轉移往更東邊到大約現在55號州際公路（I-55）的地方，而原駛路線大致是循著目前伊利諾伊州4號公路（IL-4）\[7\]。

1940年，洛杉磯的第一條高速公路 (Arroyo Seco Parkway) 被納入了66號公路，這段就是後來著名的\[8\]。

1953年，通過黑山地區的奧特曼公路完全被[亞利桑那州](../Page/亞利桑那州.md "wikilink")[金曼和](../Page/金曼_\(亞利桑那州\).md "wikilink")[加州](../Page/加州.md "wikilink")[尼德爾斯之間的新公路取代](../Page/尼德爾斯_\(加利福尼亞州\).md "wikilink")\[9\]。到1960年代，亞利桑那州[奧特曼幾乎成為被廢棄的鬼鎮](../Page/奧特曼_\(亞利桑那州\).md "wikilink")。

自1950年代以來，隨著眾多州際公路的興建，66號公路逐漸變成為州際公路的備援替代道路，甚至它的道路編號也被移轉取代。而在1965年頒布的公路美化法案（Highway
Beautification
Act）嚴格限制了在新的州際公路旁設立廣告招牌，使得在舊66號公路上的商家更不容易招攬到生意，使得這些商家不得不結束營業，66號公路也因而愈加荒涼\[10\]。

66號公路在經過大城市時，會圍繞它的周邊通過以避開市中心的繁忙交通，例如[伊利諾伊州的](../Page/伊利諾伊州.md "wikilink")[-{zh-hans:斯普林菲爾德;zh-hk:春田;zh-tw:春田;}-](../Page/斯普林菲爾德_\(密蘇裡州\).md "wikilink")、[密蘇里州的](../Page/密蘇里州.md "wikilink")[聖路易](../Page/聖路易斯_\(密蘇里州\).md "wikilink")、[羅拉](../Page/羅拉.md "wikilink")、，以及[奧克拉荷馬市](../Page/奧克拉荷馬市.md "wikilink")。

[OldalignIL.jpg](https://zh.wikipedia.org/wiki/File:OldalignIL.jpg "fig:OldalignIL.jpg")

### 衰落

美國66號公路使命的終結，始於1956年[艾森豪總統大力推動的](../Page/艾森豪.md "wikilink")《[州際公路法案](../Page/州際公路系統.md "wikilink")》。1919年作為一名年輕軍官時的經歷深深影響了他，因此他十分期望美國也能擁有像德國一樣便捷的高速公路，並認為這是國家保衛系統中必不可少的一環\[11\]。

1960年代後期，隨著州際公路的修建和投入使用，越來越多的人樂於享受更快速便捷的交通網絡，66號公路逐步遭到遺棄，不再被使用。1985年，被正式廢除。

### 廢除之後

[Whiting_bros.jpg](https://zh.wikipedia.org/wiki/File:Whiting_bros.jpg "fig:Whiting_bros.jpg")
一些路段變為州內公路，一些成為地方公路，還有一些淪為私人車道，甚至直接被遺棄。儘管完整不間斷地從芝加哥經66號公路開車去洛杉磯早已成為過去，但許多原先的線路以及替代線路還是可以通車的。

另外，即使作為州內公路，在一些地方人們依然沿用了它的“66”這個名字，一些國道以及分佈於老公路周圍的市內道路也保留著“66”這個數字。

### 復興

從1987年開始，各種關於66號公路的團體、協會陸續成立\[12\]\[13\]。1990年，密蘇里州宣佈此路為“州歷史公路”。許多當地群體甚至直接把有關於“66”的圖案印在路面上，還有一些人努力去保存66號公路邊的老汽車旅館和霓虹燈。1999年，[克林頓總統簽署了國家](../Page/比爾·克林頓.md "wikilink")66號公路保護法案，撥款1千萬美金作為基金，用以保護、修復分佈於公路不同路段有歷史價值的景點\[14\]。經過各方努力，原本已經消失的美國66號公路，又重新回到了地圖上。

## 線路介紹

[End_of_route_66_in_santa_monica.jpg](https://zh.wikipedia.org/wiki/File:End_of_route_66_in_santa_monica.jpg "fig:End_of_route_66_in_santa_monica.jpg")

### 加利福尼亞州

[Rte66btwnOatmanAndKingman.JPG](https://zh.wikipedia.org/wiki/File:Rte66btwnOatmanAndKingman.JPG "fig:Rte66btwnOatmanAndKingman.JPG")
66號公路的西端終點處於[加利福尼亞州](../Page/加利福尼亞州.md "wikilink")[聖塔莫尼卡的太平洋海岸公路](../Page/聖塔莫尼卡.md "wikilink")。它在加利福尼亞州橫跨\[15\]，並沿途與美國101國道以及加利福尼亞5號、15號、40號洲際公路相交。

### 亞利桑那州

在[亞利桑那州的路段有](../Page/亞利桑那州.md "wikilink")，在許多路段上與40號洲際公路平行，穿越托泊克峽（Topock-Gorge），經過鬼鎮奧特曼
\[16\]。而在[金曼和](../Page/金曼_\(亞利桑那州\).md "wikilink")[賽里格曼之間的道路](../Page/塞利格曼_\(亞利桑那州\).md "wikilink")，仍被標示為亞利桑那州66號公路（）。

### 新墨西哥州

在[新墨西哥州的路段有](../Page/新墨西哥州.md "wikilink")，西半段經過數個
\[17\]，東段經過大城[阿布奎基和](../Page/阿布奎基_\(新墨西哥州\).md "wikilink")[聖塔菲](../Page/聖塔菲_\(新墨西哥州\).md "wikilink")。如同在亞利桑那州一樣，新墨西哥州的這段公路也大致與40號洲際公路平行\[18\]。

### 德克薩斯州

[Route66_024.jpg](https://zh.wikipedia.org/wiki/File:Route66_024.jpg "fig:Route66_024.jpg")
66號公路穿過[德州北緣](../Page/德克薩斯州.md "wikilink")，全長，這裡也是整條公路的中間點\[19\]。

### 奧克拉荷馬州與堪薩斯州

66號公路在[奧克拉荷馬州跨越了](../Page/奧克拉荷馬州.md "wikilink")，在[奧克拉荷馬市以西被標示為](../Page/奧克拉荷馬市.md "wikilink")40號洲際公路，奧克拉荷馬市以東則為奧克拉荷馬州66號公路（）\[20\]。再往東邊就進入[堪薩斯州](../Page/堪薩斯州.md "wikilink")，這段路全長只有\[21\]。

### 密蘇里州

在這一路段全長，經過了喬普林、[-{zh-hans:斯普林菲爾德;zh-hk:春田;zh-tw:春田;}-以及](../Page/斯普林菲爾德_\(密蘇裡州\).md "wikilink")[聖路易](../Page/聖路易斯_\(密蘇里州\).md "wikilink")。途中會經過－世界上第一家[得來速](../Page/得來速.md "wikilink")（音譯自
Drive-thru）服務商店\[22\]。

### 伊利諾州

66號公路跨越[密西西比河在](../Page/密西西比河.md "wikilink")[東聖路易進入伊利諾州](../Page/東聖路易斯_\(伊利諾州\).md "wikilink")，在州境內全長，經過[卡霍基亞遺址](../Page/卡霍基亞遺址.md "wikilink")－美國僅有的21個世界遺產中的一個，並經伊利諾州首府[春田市](../Page/斯普林菲爾德_\(伊利諾州\).md "wikilink")
\[23\]，最後進入[芝加哥抵達](../Page/芝加哥.md "wikilink")[密西根湖畔](../Page/密西根湖.md "wikilink")，這裡是66號公路的東端終點\[24\]。

## 流行文化

  - 《[66號公路](../Page/66號公路_\(電視劇\).md "wikilink")》（*Route
    66*）：一齣在1960年時開播的美國電視[連續劇](../Page/連續劇.md "wikilink")。
  - 《[憤怒的葡萄](../Page/憤怒的葡萄.md "wikilink")》（*The Grapes of
    Wrath*）：美國作家[約翰·史坦貝克的長篇](../Page/約翰·史坦貝克.md "wikilink")[小說](../Page/小說.md "wikilink")，是一部美國社會紀實文學。記錄了主角在66號公路上發上的故事。後來被改編為同名電影。
  - 《[在66號公路上找樂子](../Page/在66號公路上找樂子.md "wikilink")》（*(Get Your Kicks
    on) Route
    66*）：[爵士作曲家兼演員](../Page/爵士樂.md "wikilink")[鮑比·特魯普](../Page/鮑比·特魯普.md "wikilink")（Bobby
    Troup）畢生最有名的一首歌曲。後來經知名歌手[納金高](../Page/納金高.md "wikilink")（Nat King
    Cole）發行，使這首歌成為當時的熱門歌曲。
  - 《[汽車總動員](../Page/汽車總動員.md "wikilink")》（*Cars*）：由[皮克斯動畫出品的](../Page/皮克斯動畫.md "wikilink")2006年美國[卡通電影](../Page/卡通.md "wikilink")。故事舞臺「油車水鎮」（Radiator
    Springs）被設定成一個位在66號公路上的沒落西部小鎮。
  - 《[鬥陣特攻](../Page/鬥陣特攻.md "wikilink")》（*Overwatch*）：一張護送地圖。

## 參考資料

## 外部連結

  - [Official Website of Historic Route 66 (National Scenic Byway) in
    Illinois](http://www.illinoisroute66.org)

[66號美國國道](../Category/66號美國國道.md "wikilink")
[Category:美國國道](../Category/美國國道.md "wikilink")
[U](../Category/美國風景道路.md "wikilink")
[US66](../Category/已廢除美國國道.md "wikilink")
[US66](../Category/1926年美國建立.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.
9.
10.

11.

12.

13.

14.

15.

16.

17.

18.

19.

20.

21.

22.

23.

24.