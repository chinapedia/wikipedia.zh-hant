**江川達也**（），[日本男性](../Page/日本.md "wikilink")[漫畫家](../Page/漫畫家.md "wikilink")、[電視藝人](../Page/藝人.md "wikilink")。出身於[愛知縣](../Page/愛知縣.md "wikilink")[名古屋市](../Page/名古屋市.md "wikilink")[千種區](../Page/千種區.md "wikilink")，[愛知教育大學教育學部畢業](../Page/愛知教育大學.md "wikilink")。血型為A型。已婚並育有子女。

代表作有『神通小精靈』、『東京大學物語』等。

## 人物概要

  - 住在[東京都](../Page/東京都.md "wikilink")[澀谷區](../Page/澀谷區.md "wikilink")[松濤建設費約](../Page/松濤.md "wikilink")6億日圓的[豪宅裡](../Page/豪宅.md "wikilink")。
  - 為最近的年輕人（漫畫助手）沒有責任感的態度感到生氣，現在是完全不用助手、一個人畫漫畫（自述）。
  - 持有相当明显的[右翼观点](../Page/右翼.md "wikilink")。

## 作品列表

### 漫畫

  - 原名《》
  - [神通小精靈](../Page/神通小精靈.md "wikilink")、全21冊、原名《》
  - 黃金小子、原名《》
  - [東京大學物語](../Page/東京大學物語.md "wikilink")、全21冊、原名《》
  - 原名《》
  - 原名《》
  - [魔動天使](../Page/魔動天使.md "wikilink")、原名《》
  - 原名《》
  - 原名《》
  - [THE LAST MAN](../Page/THE_LAST_MAN.md "wikilink")、8冊待續、原名《》
  - [文化祭（祕）委員會](../Page/文化祭（祕）委員會.md "wikilink")、全2冊、原名《》
  - [日俄戰爭物語](../Page/日俄戰爭物語.md "wikilink")、22冊（第一部完）、原名《》
  - 《[源氏物語](../Page/源氏物語.md "wikilink")》
  - 原名《》
  - 原名《》
  - [家畜人鴉俘](../Page/家畜人鴉俘.md "wikilink")、原名《》
  - 原名《》
  - [江川式學習法](../Page/江川式學習法.md "wikilink")、原名《》
  - 《[假面骑士 THE FIRST](../Page/假面骑士_THE_FIRST.md "wikilink")》
  - 《[BOCCHAN 坊っちゃん](../Page/BOCCHAN_坊っちゃん.md "wikilink")》
  - [家庭教師神宮山美佳](../Page/家庭教師神宮山美佳.md "wikilink")、原名《》

## 過去助手

  - [藤島康介](../Page/藤島康介.md "wikilink")
  - [藤澤亨](../Page/藤澤亨.md "wikilink")
  - [坂本真一](../Page/坂本真一.md "wikilink")
  - [山田玲司](../Page/山田玲司.md "wikilink")
  - [松浦聰彥](../Page/松浦聰彥.md "wikilink")

## 外部連結

  - [個人網頁](http://homepage3.nifty.com/egawa-tatuya/)

  -
  - [](https://web.archive.org/web/20080308175518/http://trendy.nikkeibp.co.jp/lc/jidai/060302_egawa1/)日經BP
    漫畫家、電影導演 江川達也訪談（前篇）

  - [](https://web.archive.org/web/20080112122642/http://trendy.nikkeibp.co.jp/lc/jidai/060307_egawa2/)日經BP
    漫畫家、電影導演 江川達也訪談（後篇）

  - [江川達也 - 遊戲基地
    WeKey](http://www.gamebase.com.tw/wekey/ac/newgame.php?gno=2787)

  - [博客來網路書店 - 目前您搜尋的關鍵字為:
    江川達也](http://search.books.com.tw/exep/openfind.php?cat=001&key=%A6%BF%A4t%B9F%A4%5D)

[Category:愛知縣出身人物](../Category/愛知縣出身人物.md "wikilink")
[Category:日本漫畫家](../Category/日本漫畫家.md "wikilink")