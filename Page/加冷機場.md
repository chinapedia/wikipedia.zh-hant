**加冷機場**（英文名稱：Kallang Airport，位置：北緯：1°18' 32'；東經：103°54'5"
，海拔高度：0）原是[新加坡第二個](../Page/新加坡.md "wikilink")[國際機場](../Page/國際機場.md "wikilink")，由[英國政府建造](../Page/英國政府.md "wikilink")，在1937年6月12日開始服務，當時被譽為：「[大英帝國最好的機場](../Page/大英帝國.md "wikilink")」。

1955年8月20日，由於新建的[巴耶利峇機場](../Page/巴耶利峇機場.md "wikilink")（Paya Lebar
Airport）開幕，被取代而關閉，原址亦重新建設成容納25,000[民眾的](../Page/民眾.md "wikilink")[組屋](../Page/組屋.md "wikilink")、[加冷地铁站及](../Page/加冷地铁站.md "wikilink")[高速公路等](../Page/高速公路.md "wikilink")。

現時除了原來的「搭客樓」則在1960年7月1日被改用作[新加坡人民協會](../Page/新加坡人民協會.md "wikilink")（Singapore
People's Association）的辦公樓被保留外，其他設施已無跡可尋。

## 接待過的名人

  - 英國駐新加坡總督（Governor）[列誥爵士](../Page/列誥.md "wikilink")（Sir John Fearns
    Nicoll）
  - 1955年4月16日上午，[中華人民共和國國務院總理](../Page/中華人民共和國國務院總理.md "wikilink")[周恩來為參加](../Page/周恩來.md "wikilink")[萬隆會議曾取道於此](../Page/萬隆會議.md "wikilink")
  - 著名教育家[陳嘉庚先生](../Page/陳嘉庚.md "wikilink")（Mr. Tan Kah Kee）

## 參看

  - [實里達機場](../Page/實里達機場.md "wikilink")（Seletar Airport）
  - [登加空軍基地](../Page/登加空軍基地.md "wikilink")（Tengah Air Base）
  - [勝寶旺機場](../Page/勝寶旺機場.md "wikilink")（Sembawang Air Base）
  - [巴耶利峇空軍基地](../Page/巴耶利峇空軍基地.md "wikilink")（Paya Lebar Air Base）
  - [樟宜機場](../Page/樟宜機場.md "wikilink")（Changi Airport）

## 外部連結

  - [新加坡民航局的官方網站（英文）](http://www.caas.gov.sg/caas/index.jsp)
  - [新加坡人民協會的官方網站（英文）](http://www.pa.gov.sg)

[Category:新加坡機場](../Category/新加坡機場.md "wikilink")
[Category:1937年启用的机场](../Category/1937年启用的机场.md "wikilink")
[Category:1955年廢除](../Category/1955年廢除.md "wikilink")