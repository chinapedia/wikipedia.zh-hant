[岩石依不同的形成方式](../Page/岩石.md "wikilink")，可粗略分為三類：火成岩、沉積岩和變質岩。

## [火成岩](../Page/火成岩.md "wikilink") Igneous rocks

  - [火山岩](../Page/火山岩.md "wikilink") (Volcanic rock)
      - [流紋岩](../Page/流紋岩.md "wikilink") (Rhyolite)
          - [黑曜岩](../Page/黑曜岩.md "wikilink") (Obsidian)
          - [松脂岩](../Page/松脂岩.md "wikilink") (Pitchstone)
          - [珍珠岩](../Page/珍珠岩.md "wikilink") (Perlite)
      - [粗面岩](../Page/粗面岩.md "wikilink") (Trachyte)
      - [響岩](../Page/響岩.md "wikilink") (Phonolite)
      - [英安岩](../Page/英安岩.md "wikilink") (Dacite)
      - [安山岩](../Page/安山岩.md "wikilink") (Andesite)
      - [粗安岩](../Page/粗安岩.md "wikilink") (Latite/Trachyandesite)
      - [玄武岩](../Page/玄武岩.md "wikilink") (Basalt)

<!-- end list -->

  - [浅成岩](../Page/浅成岩.md "wikilink") (Hypabyssal rock)
      - [斑岩](../Page/斑岩.md "wikilink") (Porphyry)
      - [霏細岩](../Page/霏細岩.md "wikilink") (Felsite)
      - [細晶岩](../Page/細晶岩.md "wikilink") (Aplite)
      - [偉晶岩](../Page/偉晶岩.md "wikilink") (Pegmatite)
      - [玢岩](../Page/玢岩.md "wikilink") (Porphyrite)
      - [粒玄岩](../Page/粒玄岩.md "wikilink") (Dolerite)
      - [煌斑岩](../Page/煌斑岩.md "wikilink") (Lamprophyre)

[Granit.jpg](https://zh.wikipedia.org/wiki/File:Granit.jpg "fig:Granit.jpg")

  - [深成岩](../Page/深成岩.md "wikilink") (Plutonic rock)
      - [花崗岩](../Page/花崗岩.md "wikilink") (Granite)
      - [正長岩](../Page/正長岩.md "wikilink") (Syenite)
      - [二長岩](../Page/二長岩.md "wikilink") (Monzonite)
      - [花崗閃長岩](../Page/花崗閃長岩.md "wikilink") (Granodiorite)
      - [閃長岩](../Page/閃長岩.md "wikilink") (Diorite)
      - [輝長岩](../Page/輝長岩.md "wikilink") (Gabbro)
      - [斜長岩](../Page/斜長岩.md "wikilink") (Anorthosite)
      - [橄欖岩](../Page/橄欖岩.md "wikilink") (Peridotite)
      - [輝石岩](../Page/輝石岩.md "wikilink") (Pyroxenite)
      - [角閃石岩](../Page/角閃石岩.md "wikilink") (Hornblendite)
      - [蛇紋大理岩](../Page/蛇紋大理岩.md "wikilink") (Ophicalcite)
      - [碳酸岩](../Page/碳酸岩.md "wikilink") (Carbonatite)

## [沉積岩](../Page/沉積岩.md "wikilink") Sedimentary rocks

[ShaleUSGOV.jpg](https://zh.wikipedia.org/wiki/File:ShaleUSGOV.jpg "fig:ShaleUSGOV.jpg")

  - [碎屑岩](../Page/碎屑岩.md "wikilink") (Clastic rock)
      - [礫岩](../Page/礫岩.md "wikilink") (Conglomerate)
      - [角礫岩](../Page/角礫岩.md "wikilink") (Breccia)
      - [砂岩](../Page/砂岩.md "wikilink")／沙岩 (Sandstone)
          - [長石砂岩](../Page/長石砂岩.md "wikilink") (Arkose)
          - [雜砂岩](../Page/雜砂岩.md "wikilink") (Greywacke)
      - [泥岩](../Page/泥岩.md "wikilink") (Mudstone)
      - [頁岩](../Page/頁岩.md "wikilink") (Shale)

<!-- end list -->

  - [火山碎屑岩](../Page/火山碎屑岩.md "wikilink") (Pyroclastic rock)
      - [集塊岩](../Page/集塊岩.md "wikilink") (Agglomerate)
      - [凝灰岩](../Page/凝灰岩.md "wikilink") (Tuff)

<!-- end list -->

  - [生物岩](../Page/生物岩.md "wikilink") (Biolite)
      - [石灰岩](../Page/石灰岩.md "wikilink") (Limestone)
          - [白堊岩](../Page/白堊岩.md "wikilink") (Chalk)
      - [燧石](../Page/燧石.md "wikilink") (Chert)
      - [矽藻土](../Page/矽藻土.md "wikilink") (Diatomite)
      - [疊層岩](../Page/疊層岩.md "wikilink") (Stromatolites)
      - [煤炭](../Page/煤炭.md "wikilink") (Coal)
      - [油頁岩](../Page/油頁岩.md "wikilink") (Oil Shale)

<!-- end list -->

  - [化學岩](../Page/化學岩.md "wikilink") (Chemical sedimentary rock)
      - [石灰岩](../Page/石灰岩.md "wikilink") (Limestone)
      - [白雲岩](../Page/白雲岩.md "wikilink") (Dolomite)
      - [燧石](../Page/燧石.md "wikilink") (Chert)

## [變質岩](../Page/變質岩.md "wikilink") Metamorphic rocks

[MarbleUSGOV.jpg](https://zh.wikipedia.org/wiki/File:MarbleUSGOV.jpg "fig:MarbleUSGOV.jpg")

  - [接觸變質岩](../Page/接觸變質岩.md "wikilink") (Contact metamorphic rock)
      - [角頁岩](../Page/角頁岩.md "wikilink") (Hornfels)
      - [大理岩](../Page/大理岩.md "wikilink") (Marble)
      - [石英岩](../Page/石英岩.md "wikilink") (Quartzite)
      - [矽卡岩](../Page/矽卡岩.md "wikilink") (Skarn)
      - [雲英岩](../Page/雲英岩.md "wikilink") (Greisen)

<!-- end list -->

  - [區域變質岩](../Page/區域變質岩.md "wikilink") (Regional metamorphic rock)
      - [板岩](../Page/板岩.md "wikilink") (Slate)
      - [千枚岩](../Page/千枚岩.md "wikilink") (Phyllite)
      - [片岩](../Page/片岩.md "wikilink") (Schist)
      - [片麻岩](../Page/片麻岩.md "wikilink") (Gneiss)
      - [混合岩](../Page/混合岩.md "wikilink") (Migmatite)
      - [角閃岩](../Page/角閃岩.md "wikilink") (Amphibolite)
      - [麻粒岩](../Page/麻粒岩.md "wikilink")／粒變岩 (Granulite)
      - [榴輝岩](../Page/榴輝岩.md "wikilink") (Eclogite)
      - [蛇紋岩](../Page/蛇紋岩.md "wikilink") (Serpentinite)

<!-- end list -->

  - [動力變質岩](../Page/動力變質岩.md "wikilink") (Dynamic metamorphic rock)
      - [糜稜岩](../Page/糜稜岩.md "wikilink") (Mylonite)

## 參考文獻

  - 都城秋穂・久城育夫　『岩石学II - 岩石の性質と分類』　共立出版〈共立全書〉、1975年、ISBN 4-320-00205-9。

## 參見

  - [岩石學](../Page/岩石學.md "wikilink")
  - [礦物列表](../Page/礦物列表.md "wikilink")
  - [熔岩](../Page/熔岩.md "wikilink")
  - [浮石](../Page/浮石.md "wikilink")／[火山渣](../Page/火山渣.md "wikilink")
  - [隕石](../Page/隕石.md "wikilink")

## 外部链接

  - [British Geological Survey rock classification
    scheme](http://www.bgs.ac.uk/bgsrcs/details.html)

  - [Igneous rock
    classification](http://csmres.jmu.edu/geollab/Fichter/IgnRx/Ighome.html)

  - [Rock Types Article by Encyclopaedia
    Britannica](http://www.britannica.com/EBchecked/topic/505970/rock)

  - [Classification of common rocks and
    soils](http://www.seafriends.org.nz/enviro/soil/rocktbl.htm)

  - [Metamorphic Rock
    Classification](https://web.archive.org/web/20051214134842/http://geology.csupomona.edu/alert/metamorphic/metaclass.htm)

  - [Volcanic
    rocks](https://web.archive.org/web/20091214164707/http://volcano.oregonstate.edu/vwdocs/vwlessons/volcanic_rocks.html)

  - [Earth Science Education Unit virtual rock
    kit](http://www.earthscienceeducation.com/virtual_rock_kit/index.htm)

[岩石](../Category/地質學列表.md "wikilink") [\*](../Category/岩石.md "wikilink")
[Category:岩石学](../Category/岩石学.md "wikilink")
[Category:變質岩](../Category/變質岩.md "wikilink")
[Category:火成岩](../Category/火成岩.md "wikilink")
[Category:水成岩](../Category/水成岩.md "wikilink")