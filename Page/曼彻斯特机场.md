**曼彻斯特机场**（英文：Manchester
Airport）是一座位于[英格兰](../Page/英格兰.md "wikilink")[曼彻斯特市中心西南部](../Page/曼彻斯特.md "wikilink")13.9公里的国际机场。2016年，曼彻斯特机场是[英国客运量第三繁忙的机场](../Page/英国.md "wikilink")。整个机场包括三座客运航站楼，一座货运航站楼以及两条跑道，是英国除[伦敦希思罗机场外](../Page/伦敦希思罗机场.md "wikilink")，唯一一座跑道运营长度超过2999米的机场。机场占地面积达到1400公顷，服务199个国内外航点，机场通达度排名世界第13位。
曼彻斯特机场于1938年6月25日开航，最初机场命名为Ringway Airport。
在[二战期间是](../Page/二战.md "wikilink")[英国皇家空军的基地所在地](../Page/英国皇家空军.md "wikilink")。机场由曼彻斯特机场集团运营和管理，曼彻斯特市议会是该集团的最大股东。

2016年，曼彻斯特机场一共处理了2560万人次的旅客，创机场开航以来的纪录。按目前机场的设计，一年可以处理5000万人次的旅客。由于机场每小时的飞机起降量被限制在每小时61架次，因此预计潜在旅客量而缩减。未来机场将会投入8亿英镑对机场后勤物流、基础设施、办公区、酒店和机场周边的交通以及高速公路进行升级改造。

## 航站、航空公司及其航點

### 第一航站

第一航站於1962年啟用，主要處理國際航班。第一航站擁有24個泊位，其中18個有空橋連接。

  - [愛爾蘭航空](../Page/愛爾蘭航空.md "wikilink")（[都柏林機場](../Page/都柏林機場.md "wikilink")）
  - [芬蘭航空](../Page/芬蘭航空.md "wikilink")（[赫爾辛基萬塔國際機場](../Page/赫爾辛基萬塔國際機場.md "wikilink")）
  - [漢莎航空](../Page/漢莎航空.md "wikilink")（[法蘭克福國際機場](../Page/法蘭克福國際機場.md "wikilink")、[慕尼黑國際機場](../Page/慕尼黑國際機場.md "wikilink")）
  - [瑞安航空](../Page/瑞安航空公司.md "wikilink")（[都柏林國際機場](../Page/都柏林國際機場.md "wikilink")、[香農國際機場](../Page/香農國際機場.md "wikilink")）
  - [北歐航空](../Page/北歐航空.md "wikilink")（[哥本哈根機場](../Page/哥本哈根機場.md "wikilink")、[斯德哥爾摩機場](../Page/斯德哥爾摩機場.md "wikilink")、[奧斯陸國際機場](../Page/奧斯陸國際機場.md "wikilink")）

### 第二航站

第二航站於1993年啟用，主要處理國際航班。

  - [海南航空](../Page/海南航空.md "wikilink")（[北京首都國際機場](../Page/北京首都國際機場.md "wikilink")）
  - [國泰航空](../Page/國泰航空.md "wikilink")
    （[香港國際機場](../Page/香港國際機場.md "wikilink")）
  - [聯合航空](../Page/聯合航空.md "wikilink")（[紐華克自由國際機場](../Page/紐華克自由國際機場.md "wikilink")）
  - [阿聯酋國際航空](../Page/阿聯酋國際航空.md "wikilink")（[杜拜國際機場](../Page/杜拜國際機場.md "wikilink")）
  - [巴基斯坦國際航空](../Page/巴基斯坦國際航空.md "wikilink")（[真納國際機場](../Page/真納國際機場.md "wikilink")、[伊克巴爾國際機場](../Page/伊克巴爾國際機場.md "wikilink")、[伊斯蘭堡國際機場](../Page/伊斯蘭堡國際機場.md "wikilink")）
  - [卡達航空](../Page/卡達航空.md "wikilink")（[多哈國際機場](../Page/多哈國際機場.md "wikilink")）
  - [新加坡航空](../Page/新加坡航空.md "wikilink")（[樟宜國際機場](../Page/樟宜國際機場.md "wikilink")、[德克薩斯州](../Page/德克薩斯州.md "wikilink")[侯斯頓](../Page/侯斯頓.md "wikilink")[喬治布希洲際機場](../Page/喬治布希洲際機場.md "wikilink")）
  - [美國航空](../Page/美國航空.md "wikilink")（[費城國際機場](../Page/費城國際機場.md "wikilink")）
  - [維珍航空](../Page/維珍航空.md "wikilink")（[奧蘭多國際機場](../Page/奧蘭多國際機場.md "wikilink")、[甘迺迪國際機場](../Page/甘迺迪國際機場.md "wikilink")、[哈茨菲爾德-傑克遜亞特蘭大國際機場](../Page/哈茨菲爾德-傑克遜亞特蘭大國際機場.md "wikilink")）

### 第三航站

第三航站於1989年由威爾斯王妃[戴安娜主持開幕式](../Page/戴安娜.md "wikilink")\[1\]。第三航站原為[英國航空專用航站](../Page/英國航空.md "wikilink")，現在則與[美國航空](../Page/美國航空.md "wikilink"),及其他內陸航線分享使用。

  - [英國航空](../Page/英國航空.md "wikilink")（[盖特威克机场](../Page/盖特威克机场.md "wikilink")、[希斯罗機場](../Page/希斯罗機場.md "wikilink")）
  - [美國航空](../Page/美國航空.md "wikilink")（[奧黑爾國際機場](../Page/奧黑爾國際機場.md "wikilink")、[費城國際機場](../Page/費城國際機場.md "wikilink")）
  - [荷蘭皇家航空](../Page/荷蘭皇家航空.md "wikilink")（[斯希普霍尔机场](../Page/斯希普霍尔机场.md "wikilink")）
  - [法國航空](../Page/法國航空.md "wikilink")（[戴高樂國際機場](../Page/戴高樂國際機場.md "wikilink")）

## 國際貨運航站

曼徹斯特國際機場的主要貨運航點在遠東地區及北美洲─遠東地區主要是運送進口貨物，而往北美洲則主要是出口貨物。其中香港國際機場是曼徹斯特國際機場的主要貨運航點，國泰航空每周有24班來回航線。2007年8月，聯邦快遞開設直航孟斐斯國際機場的貨運航班，每週4班。\[2\]2007年9月，俄羅斯航空開設直航莫斯科的貨運航線，是該航空公司第一條直航英國的貨運航線。

截至10月，曼徹斯特國際機場於2007年處理了150,300[公噸貨物及郵件](../Page/公噸.md "wikilink")，預料全年將處理164,300公噸貨物，比往年上升9.8%。預計貨物吞吐量將於2015年達到250,000公噸。

曼徹斯特國際機場現時每日平均處理六班[波音747的貨運航班](../Page/波音747.md "wikilink")。

  - [俄羅斯航空](../Page/俄羅斯航空.md "wikilink")（[谢列梅捷沃國際機場](../Page/谢列梅捷沃國際機場.md "wikilink")）
  - [中國國際航空](../Page/中國國際航空.md "wikilink")（[浦東國際機場](../Page/浦東國際機場.md "wikilink")）
  - [國泰航空](../Page/國泰航空.md "wikilink")（[斯希普霍尔机场](../Page/斯希普霍尔机场.md "wikilink")、[布魯塞爾機場](../Page/布魯塞爾機場.md "wikilink")、[杜拜國際機場](../Page/杜拜國際機場.md "wikilink")、[香港國際機場](../Page/香港國際機場.md "wikilink")、[马尔彭萨国际机场](../Page/马尔彭萨国际机场.md "wikilink")）
  - [長城航空](../Page/長城航空.md "wikilink")（[斯希普霍尔机场](../Page/斯希普霍尔机场.md "wikilink")、[浦東國際機場](../Page/浦東國際機場.md "wikilink")）
  - [中華航空](../Page/中華航空.md "wikilink")（[台北桃園國際機場](../Page/台北桃園國際機場.md "wikilink")）
  - [聯邦快遞](../Page/聯邦快遞.md "wikilink")（[孟斐斯國際機場](../Page/孟斐斯國際機場.md "wikilink")）

## 註解

<div class="references-small">

<references />

</div>

[Category:英格兰机场](../Category/英格兰机场.md "wikilink")
[Category:曼彻斯特交通](../Category/曼彻斯特交通.md "wikilink")
[Category:1938年启用的机场](../Category/1938年启用的机场.md "wikilink")

1.  [Manchester
    airport:history](http://www.manchesterairport.co.uk/web.nsf/Content/AboutUsHistory)

2.  [FedEx adds third U.K.
    route](http://memphis.bizjournals.com/memphis/stories/2007/08/27/daily6.html?jst=cn_cn_lk)－Memphis
    Business Journal