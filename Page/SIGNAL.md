《**SIGNAL**》是[KAT-TUN的第](../Page/KAT-TUN.md "wikilink")2張單曲。2006年7月19日開始發售。發行公司為[J-One
Records](../Page/J_Storm.md "wikilink")。
此細碟也取下2006年7月份的（[ORICON](../Page/ORICON.md "wikilink")）公信榜月冠軍。

## 解説

  - 初回限定盤附DVD，收錄「Making of "SIGNAL"」。
  - 現在銷售數量約56.8萬張左右。
  - 拿下日本ORICON公信榜單曲榜年終排行第5名。

## 收錄曲目

### 通常盤

1.  SIGNAL
2.  I'll be with you
3.  SIGNAL（原創・卡拉OK）
4.  I'll be with you（原創・卡拉OK）

### DVD付限定盤

1.  SIGNAL
2.  SIGNAL（原創・卡拉OK）

### 詞曲相關

「SIGNAL」
作詞：[ma-saya](../Page/ma-saya.md "wikilink")／[JOKER](../Page/JOKER.md "wikilink")　作曲：JOEY
CARBONE／LISA HUANG　編曲：[AKIRA](../Page/AKIRA.md "wikilink")
「I'll be with you」
作詞：[白井裕紀](../Page/白井裕紀.md "wikilink")／[新美香](../Page/新美香.md "wikilink")　作曲：Shusui／Carl
Sahlin／Peter Ledin／Peter Moden　編曲：[長岡成貢](../Page/長岡成貢.md "wikilink")

## 特別相關

  - [NTT DoCoMo](../Page/NTT_DoCoMo.md "wikilink") [new 9
    series](../Page/new_9_series.md "wikilink") 手機廣告主題曲

## 排行榜

<table>
<tbody>
<tr class="odd">
<td><p><a href="../Page/ORICON.md" title="wikilink">ORICON公信榜週間</a><a href="../Page/單曲.md" title="wikilink">單曲排行榜的第</a>1位<br />
2006年7月31日</p></td>
</tr>
<tr class="even">
<td><p>前作：<br />
<a href="../Page/堂本光一.md" title="wikilink">堂本光一</a><br />
『<a href="../Page/Deep_in_your_heart/+MILLION_but_-LOVE.md" title="wikilink">Deep in your heart／+MILLION but -LOVE</a>』</p></td>
</tr>
</tbody>
</table>

[en:Signal (KAT-TUN
song)](../Page/en:Signal_\(KAT-TUN_song\).md "wikilink") [ja:SIGNAL
(KAT-TUNの曲)](../Page/ja:SIGNAL_\(KAT-TUNの曲\).md "wikilink")

[Category:2006年單曲](../Category/2006年單曲.md "wikilink")
[Category:KAT-TUN歌曲](../Category/KAT-TUN歌曲.md "wikilink")
[Category:廣告歌曲](../Category/廣告歌曲.md "wikilink")
[Category:2006年Oricon單曲月榜冠軍作品](../Category/2006年Oricon單曲月榜冠軍作品.md "wikilink")
[Category:2006年Oricon單曲週榜冠軍作品](../Category/2006年Oricon單曲週榜冠軍作品.md "wikilink")