**埃内斯托·桑佩尔·皮萨诺**（[西班牙语](../Page/西班牙语.md "wikilink")：****，），
前任[哥倫比亞总统](../Page/哥倫比亞.md "wikilink")。

## 早年

桑佩尔曾就读于Gimnasio Moderno school和哈维里亚那天主教大学（Pontificia Universidad
Javeriana），穫得法律和經濟學碩士學位。後赴墨西哥國立金融大學和美國[哥倫比亞大學學習](../Page/哥倫比亞大學.md "wikilink")。

## 政治生涯

桑佩尔1981年步入政壇，1982年任駐聯合國大使，1986年任波哥大市政委員和國民議會參議員。1990年8月－1991年11月任經濟發展部長。1992年任駐西班牙大使至1993年回國參加總統競選。1994年6月當選哥倫比亞總統，8月7日就職。1995年任[不結盟運動主席](../Page/不結盟運動.md "wikilink")。

他在1989年在一次暴力活動中身中11槍，至今還有4顆子彈殘留體內。

## 大使推荐

[Category:哥伦比亚总统](../Category/哥伦比亚总统.md "wikilink")
[Category:哥伦比亚政府部长](../Category/哥伦比亚政府部长.md "wikilink")
[Category:哥伦比亚经济学家](../Category/哥伦比亚经济学家.md "wikilink")
[Category:哥倫比亞大學校友](../Category/哥倫比亞大學校友.md "wikilink")
[Category:哥伦比亚驻西班牙大使](../Category/哥伦比亚驻西班牙大使.md "wikilink")
[Category:波哥大人](../Category/波哥大人.md "wikilink")