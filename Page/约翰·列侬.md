**约翰·温斯顿·小野·列侬**，（，出生名为；）是一位英国歌手和词曲作者，作为[披头士乐队的创始成员闻名全球](../Page/披头士乐队.md "wikilink")，该乐队是流行音乐史上在商业上最成功的团体。他与乐队成员[保罗·麦卡特尼组成了著名的](../Page/保罗·麦卡特尼.md "wikilink")[藍儂–麥卡尼创作组合](../Page/藍儂–麥卡尼.md "wikilink")。

列侬在[利物浦出生长大](../Page/利物浦.md "wikilink")，在青少年时期参与了[噪音爵士乐热潮](../Page/噪音爵士乐.md "wikilink")；他的第一支乐队[采石工人](../Page/采石工人乐队.md "wikilink")（）在1960年转变为披头士樂團。披头士于1970年解散后，他开始了自己的个人职业生涯，发行了受好评的专辑《》和《》，以及标志性的歌曲如《》、《》和《》。在1969年与[小野洋子结婚后](../Page/小野洋子.md "wikilink")，他把名字改为约翰·小野·列侬。1975年，列侬从乐坛隐退，在家照顾年幼的儿子[西恩](../Page/西恩·列侬.md "wikilink")。1980年，他和小野携新专辑《》复出，在专辑发行三周后[遇害](../Page/约翰·列侬之死.md "wikilink")。

列侬在他的音乐、写作、绘画、影片和采访中展现了他反叛的天性和尖酸的幽默。他因参与政治及[和平运动而受到争议](../Page/和平运动.md "wikilink")。1971年，他搬家到纽约市[曼哈顿](../Page/曼哈顿.md "wikilink")。在那里，列侬对[越战的批评使](../Page/越战.md "wikilink")[理查德·尼克松政府长期试图把他驱逐出境](../Page/理查德·尼克松.md "wikilink")。而他的一些歌曲被[反战运动和](../Page/反戰.md "wikilink")[60年代反文化运动视为圣歌](../Page/60年代反文化运动.md "wikilink")。

截至2012年，列侬的个人专辑在美国的销量超过了1400万。作为创作者或演唱者，他参与了25首[公告牌百强单曲榜冠军单曲](../Page/公告牌百强单曲榜.md "wikilink")。2002年，他在的“[最伟大的100名英国人](../Page/最伟大的100名英国人.md "wikilink")”榜单中被民众票选为第8位。2008年，《[滚石](../Page/滚石_\(杂志\).md "wikilink")》杂志把他选为“史上最伟大的歌手”第5位。他在去世后于1987年进入[词曲作者名人堂](../Page/词曲作者名人堂.md "wikilink")（），还两次进入[摇滚名人堂](../Page/摇滚名人堂.md "wikilink")，分别是在1988年以披头士一员身份和在1994年以个人身份入选。

## 生平

### 1940–57年：早年

列侬出生在二战时期的英国，于1940年10月9日在利物浦妇产科医院（）诞生。母亲为[茱莉亚·列侬](../Page/茱莉亚·列侬.md "wikilink")（娘家姓“”），父亲为[阿尔弗雷德·列侬](../Page/阿尔弗雷德·列侬.md "wikilink")（），是一位爱尔兰裔[商业海军](../Page/商业海军.md "wikilink")（）船员，儿子出生时不在场。名字约翰·温斯顿·列侬取自他祖父约翰·“杰克”·列侬和当时的首相[温斯顿·邱吉尔的名字](../Page/温斯顿·邱吉尔.md "wikilink")。父亲阿尔弗雷德常离家出海，但定期寄支票到母子的住处——纽卡斯尔路9号。1944年2月，他从军队[擅离职守](../Page/擅离职守.md "wikilink")（)），停止了寄支票。他终于在六个月后回到家，表示愿意继续供养家庭，但那时茱莉亚已怀着另一男人的孩子，拒绝了这一提议。在茱莉亚的姐姐[咪咪·史密斯两次向利物浦的社会服务机构投诉后](../Page/咪咪·史密斯.md "wikilink")，茱莉亚把儿子交给她抚养。1946年7月，阿尔弗雷德拜访了史密斯，把列侬带到布莱克浦（），试图带儿子一起偷偷移民新西兰。茱莉亚和她当时的恋人“鲍比”·迪金斯（）跟踪了他们，挫败了阿尔弗雷德的计划。在一场激烈的争吵后，阿尔弗雷德迫使列侬在父母中选择一个。列侬两次选择了父亲，但当母亲走开时，他开始大哭着追了上去。这一别使他直到二十年后才再次和父亲取得联系。
[Mendipsnationaltrust.JPG](https://zh.wikipedia.org/wiki/File:Mendipsnationaltrust.JPG "fig:Mendipsnationaltrust.JPG")

列侬在他剩余的童年和青少年时期住在姨妈家里——伍尔顿郊区的曼洛夫街251号。咪咪姨妈和乔治·史密斯（）姨夫没有自己的孩子。姨妈为他买了许多短篇小说集，姨夫在家庭牧场做奶工，给他买了一个口琴，教他做纵横填字游戏。茱莉亚定期来看望他，列侬自11岁起常常拜访母亲在布鲁姆菲尔德路1号的住处（）。在那里，茱莉亚播放[猫王的唱片](../Page/猫王.md "wikilink")，教他弹[班卓琴](../Page/班卓琴.md "wikilink")，弹奏[胖子多米诺的歌曲](../Page/胖子多米诺.md "wikilink")《》给他听。

1956年，茱莉亚给列侬买了他的第一把吉他，是一把便宜的[加罗顿冠军牌](../Page/加罗顿冠军牌.md "wikilink")（）。她知道自己的姐姐不支持她儿子的音乐兴趣，便“借”了5镑10先令给列侬，让吉他被派送到她的住处而不是咪咪的房子。列侬宣称自己将来有一天会成名，咪咪对此很怀疑，希望他会渐渐对音乐感到厌倦，经常告诉他：“那吉他好倒是好，约翰，但你永远没法靠它谋生。{{\#tag:ref|原文：|group="原文"}}”1958年7月15日，他的母亲在从咪咪姨妈家走回家的路上被车撞后身亡，当时列侬17岁。

列侬没有通过任何一门[普通教育证书](../Page/普通教育证书.md "wikilink")考试，在他姨妈和校长的干涉下才被利物浦艺术学院（）录取。入学后，他开始穿[泰迪男孩式](../Page/泰迪男孩式.md "wikilink")（）服装，取得了扰乱课堂和嘲弄老师的名声。他因此被绘画课拒之门外，接着是平面艺术课。因举止不端，他受到了要被开除的威胁，他的不良行为包括在写生课上坐到一位裸模的大腿上。尽管他的同学兼未来的妻子[辛西娅帮助了他](../Page/辛西娅·列侬.md "wikilink")，列侬还是没有通过年度考试，在进入最后一个年级前被踢出了学校。

### 1957-70年：披头士时期

#### 1957-66年：组建、商业突破和巡演时期

[Televisie-optreden_van_The_Beatles_in_Treslong_te_Hillegom_vlnr._Paul_McCartney,_Bestanddeelnr_916-5099.jpg](https://zh.wikipedia.org/wiki/File:Televisie-optreden_van_The_Beatles_in_Treslong_te_Hillegom_vlnr._Paul_McCartney,_Bestanddeelnr_916-5099.jpg "fig:Televisie-optreden_van_The_Beatles_in_Treslong_te_Hillegom_vlnr._Paul_McCartney,_Bestanddeelnr_916-5099.jpg")
1956年9月，十五岁的列侬创建了一支[噪音爵士乐队](../Page/噪音爵士乐.md "wikilink")，叫采石工人（），得名于采石坡高中（）。到1957年夏天，采石工人乐队已演奏了“一系列生气勃勃的歌曲”，歌单上一半是噪音爵士乐，一半是[摇滚](../Page/摇滚.md "wikilink")（）。同年7月6日，乐队的第二次演出在圣彼得教堂的花园庆典上举行，列侬在那里与[保罗·麦卡特尼相遇](../Page/保罗·麦卡特尼.md "wikilink")，他随后邀请麦卡特尼加入乐队。

麦卡特尼建议让他的朋友[乔治·哈里森成为主音吉他手](../Page/乔治·哈里森.md "wikilink")。列侬认为十四岁的哈里森年纪太小。麦卡特尼在一辆公交巴士的上层安排了试音，哈里森弹奏了器乐曲《》之后受邀加入乐队。列侬在艺术学院的朋友[斯图尔特·萨克利夫在之后加入成为贝斯手](../Page/斯图尔特·萨克利夫.md "wikilink")。列侬、麦卡特尼、哈里森和萨克利夫四人在1960年初组成了披头士乐队。同年8月，乐队获得了一份到德国汉堡驻唱48天的合同，急需一位鼓手，便邀请了[皮特·贝斯特加入](../Page/皮特·贝斯特.md "wikilink")。当时列侬十九岁，咪咪阿姨被他要去旅行的消息吓坏了，恳求他留下继续学业。在第一次汉堡之行后，乐队在1961年4月再次前往汉堡，在1962年4月去了第三次。列侬和其他乐队成员在汉堡接触了[苯甲吗啉](../Page/苯甲吗啉.md "wikilink")，常规地服用该药物，还有[安非他明](../Page/安非他明.md "wikilink")，作为支撑他们通宵演出的兴奋剂
。

[布莱恩·爱普斯坦自](../Page/布莱恩·爱普斯坦.md "wikilink")1962年起成为披头士的经理人，他之前没有任何艺人管理经验，但对乐队早期的造型和在台上的态度产生了重要影响。列侬一开始对爱普斯坦建议乐队展现职业面貌感到抗拒，但最终顺从了，说：“如果有人付我钱的话，我他妈穿个气球都行。{{\#tag:ref|原文：|group="原文"}}”在萨克利夫决定留在汉堡退出乐队后，麦卡特尼接替他成为贝斯手，而鼓手[林戈·斯塔尔取代了贝斯特](../Page/林戈·斯塔尔.md "wikilink")，这一四人阵容一直维持到乐队在1970年的解散。披头士的第一张单曲《》于1962年10月发行，在英国的榜单上最高达到第十七位。1963年2月11日，他们在十小时内完成了首张专辑《》的录音。那天，列侬受感冒困扰，可从最后一首录音的歌《》中听出来。列侬-麦卡特尼创作组合写了专辑里十四首歌中的八首。在大多数歌曲中（专辑同名曲是一个例外），列侬还没有在歌词中加入他最爱的文字游戏，说道：“我们只是写写歌……流行歌曲。只想弄出点声响，没别的想法了。歌词几乎是无关紧要的。{{\#tag:ref|原文：|group="原文"}}”在1987年的一次采访中，麦卡特尼表示其他披头士成员都崇拜列侬：“他像我们自己的小艾尔维斯（指猫王）……我们都仰视约翰。他年纪较长，是无疑的领袖；他最风趣，最聪明。{{\#tag:ref|原文：|group="原文"}}”

1963年初，披头士在英国取得了主流商业成功。列侬的长子[朱利安在同年](../Page/朱利安·列侬.md "wikilink")4月出生，他当时在外巡演。他们登台了[皇家大汇演](../Page/皇家大汇演.md "wikilink")（），观众有[王母太后和其他英国王室成员](../Page/伊丽莎白·鲍斯-莱昂.md "wikilink")。在演出期间，列侬拿观众开玩笑：“为下一首歌，我想请求你们的帮助。坐在便宜位子的人，请拍手……其他人，请摇动你的珠宝首饰。{{\#tag:ref|原文：|group="原文"}}”在英国的披头士狂热进行了一年后，他们在1964年2月在[埃德·沙利文秀上历史性的首次在美国亮相](../Page/埃德·沙利文秀.md "wikilink")，标志着乐队开始取得国际性的明星地位。接下来的两年里，乐队不间断地巡演、拍电影和写歌。在此期间，列侬写了两本书：《》和《》。在[1965年女王生日授勋](../Page/1965年女王生日授勋.md "wikilink")（）中，披头士乐队被授予了[大英帝國勳章的员佐勋章](../Page/大英帝國勳章.md "wikilink")（）。

列侬渐渐担心演唱会观众因歌迷尖叫声太响而无法听见音乐，乐队的音乐性因此开始受到负面影响。列侬1965年的歌曲《[Help\!](../Page/Help!_\(歌曲\).md "wikilink")》表达了他的感受：“歌词是真心的……那是我在呼救。{{\#tag:ref|原文：|group="原文"}}”他的体重增加了（他日后称之为自己的“肥胖猫王”时期），在潜意识中试图寻求改变。同年3月，列侬在不知情的情况下首次接触了。他和哈里森夫妇出席了一位牙医的晚宴，牙医偷偷地在他们的咖啡中加入了这种毒品。当他们想要离开时，主人透露了他们摄入的东西是什么，强烈建议他们因可能产生的效果不要离开房子。
其后在一家夜店的电梯里，他们都相信那里着火了“我们在尖叫……又热又癫狂” 。在1966年3月与《标准晚报》记者[Maureen
Cleave的采访中](../Page/Maureen_Cleave.md "wikilink")，列侬说道，“基督教会走的。它会消失和萎缩……我们现在比耶稣要受欢迎——我不知道谁会先走，是摇滚乐还是基督教。”这当时在英格兰实际上没人注意。但是五个月后在被一家杂志引用后，这一评论却在美国被视为巨大的冒犯。怒火使人们焚毁甲壳虫乐队的专辑，且[三K党对列侬发出威胁](../Page/三K党.md "wikilink")，最终导致乐队决定停止巡演。

#### 1967-70年：录音室时期、解散和个人作品

1966年8月29日，披头士举行了最后一场商业演唱会。停止了日常的现场演出后，列侬感到迷茫，考虑退出乐队。自从被动地接触到后，他增加了对这种毒品的使用，几乎在1967年的大部分时候都处于药物影响下。据传记作者伊恩·麦克唐纳（）所写，列侬在那年对的持续使用让他“几乎丧失了自我意识（）”。那一年里，歌曲《》发行，《[时代](../Page/时代_\(杂志\).md "wikilink")》杂志称赞了它“惊人的创造性”。乐队里程碑式的专辑《》也在同年发行，其中列侬的歌词已和列侬-麦卡特尼早期创作的简单情歌截然不同。

8月，在被介绍认识了[玛哈瑞诗·玛哈士](../Page/玛哈瑞诗·玛哈士.md "wikilink")（）后，乐队前往威尔士[班戈参加他为期一个周末的](../Page/班戈_\(圭内斯\).md "wikilink")[超觉静坐](../Page/超觉静坐.md "wikilink")（）研讨会。乐队在研讨会期间得知了爱普斯坦死亡的消息。“我那时就知道我们有麻烦了”，列侬在日后说道，“我很清楚我们除了玩音乐外做其他事的能力是什么水平，我感到害怕。{{\#tag:ref|原文：|group="原文"}}”主要缘于哈里森和列侬对东方宗教的兴趣，披头士之后到玛哈瑞诗在印度的[静修处](../Page/静修处.md "wikilink")（）继续寻求指引。他们在印度创作了“[白色专辑](../Page/白色专辑.md "wikilink")”和《》中的大部分内容。

列侬于1969年9月退出了披头士，同意在乐队重新协商他们的录音合同时不告知媒体。但麦卡特尼在1970年4月发行[他的首张个人专辑时公开了自己退出的消息](../Page/McCartney_\(專輯\).md "wikilink")，这一行为让列侬非常愤怒。列侬的反应为：“老天！他占据了全部功劳！{{\#tag:ref|原文：|group="原文"}}”他日后写道：“我创建了乐队。我解散了它。就那么简单。{{\#tag:ref|原文：|group="原文"}}”在后来与《[滚石](../Page/滚石_\(杂志\).md "wikilink")》杂志的一次采访中，他表达了自己对麦卡特尼的怨恨，说道：“保罗利用这事（披头士解散）来卖他的新专辑，我是个傻子才没那么做。{{\#tag:ref|原文：|group="原文"}}”他也谈及自己察觉到其他成员对小野的敌意，还有他、哈里森和斯塔尔“对成为保罗的伴奏者忍无可忍……布莱恩·爱普斯坦死后，我们分崩离析了。保罗接管了一切，据说是成为了我们的领袖。但让我们原地转圈算什么领导？{{\#tag:ref|原文：|group="原文"}}”

### 1970-80年：個人時期

[1_West_72nd_Street_(The_Dakota)_entrance_by_David_Shankbone.jpg](https://zh.wikipedia.org/wiki/File:1_West_72nd_Street_\(The_Dakota\)_entrance_by_David_Shankbone.jpg "fig:1_West_72nd_Street_(The_Dakota)_entrance_by_David_Shankbone.jpg")

#### 1970-72年:最初的成功和社会行动

[John_Lennon_Imagine_1971.jpg](https://zh.wikipedia.org/wiki/File:John_Lennon_Imagine_1971.jpg "fig:John_Lennon_Imagine_1971.jpg")的广告。\]\]
1970年，列侬和洋子在洛杉矶和[亚瑟·亚诺夫博士体验了](../Page/亚瑟·亚诺夫.md "wikilink")（Primal
therapy）。为了消除对童年生活的痛苦的恐惧，治疗进行了4个月，每周2天半。亚诺夫曾试图让列侬夫妇多做停留，但他们认为没有必要，便回到了伦敦。列侬充满个人情感的首张个人专辑《[约翰·列侬/塑胶小野乐团](../Page/约翰·列侬/塑胶小野乐团.md "wikilink")》，获得了高度赞扬。评论家评论道：“列侬在歌曲《[上帝](../Page/上帝_\(约翰·列侬歌曲\).md "wikilink")》（God）中最后一句的演唱，可能是摇滚歌曲中最好的。”专辑收录了《》（Mother）这首歌，在歌曲中列侬直面了儿时被抛弃的悲惨经历，而迪伦风格的歌曲《[工人阶级英雄](../Page/Working_Class_Hero.md "wikilink")》（Working
Class
Hero），对资本主义官僚社会体制做出了讽刺性的批判，其中的歌词“你们还是他妈的贫农”目的就是可以激怒听众。同年，在他采访列侬时的政治革命言论启发了列侬写出歌曲《[权力交给人们](../Page/Power_to_the_People.md "wikilink")》（Power
to the
People）。随后，列侬参与到了阿里的抗议停办的活动中去，列侬认为停办杂志的言论是“恶心的法西斯主义”，他和洋子发布了单曲《God
Save Us/Do the Oz》并参与游行声援杂志。

列侬的下一张专辑《[想象](../Page/想象.md "wikilink")》，收到的评价相对保守。《滚石杂志》评论认为“专辑充满了优秀的音乐”，但也警告“列侬的自我展现很可能会在不久让人觉得无聊而且令人摸不着头脑”。专辑的同名曲成为了反战圣歌，而另一首歌曲《》（How
Do You
Sleep?）则是对麦卡特尼专辑[RAM中的讽刺歌词的回应](../Page/RAM（专辑）.md "wikilink")，麦卡特尼后来也承认，他专辑中的歌词的确是指列侬和洋子。但其中的歌詞"How
do you
sleep,brother"還是表明了他對保羅的情誼，列侬在20世纪70年代中期与麦卡特尼重归于好，辩称《你如何入睡？》其实是在说自己。在1980年，他回忆称披头士后期的不愉快经历和对保罗·麦卡特尼的怨恨的确成为了歌曲的灵感，但是这种充满怨念的想法并非一直萦绕在他脑海中。

列侬与小野于1971年8月移居纽约，同年11月发行单曲《[圣诞快乐，战争结束了](../Page/圣诞快乐，战争结束了.md "wikilink")》。在新的一年中，[尼克松当局针对列侬的反战和反政府行为采取了所谓的](../Page/尼克松.md "wikilink")“战略反击方案”，试图将列侬驱逐出境。在1972年，[麦戈文竞选总统失败后](../Page/乔治·麦戈文.md "wikilink")，列侬和小野在活动家纽约的家中参加了一次守夜活动。\[1\]\[2\]列侬卷入了和当局的法律纠纷中之后，美国政府拒绝发给列侬[永久居留权](../Page/永久居留权.md "wikilink")。列侬当时心灰意冷，和一位女性客人发生了性关系，这让洋子感到难堪。她的歌曲《Death
of Samantha》就是受这件事的启发。\[3\]

在洋子和纽约乐队的合作下，《》（Some Time in New York
City）于1972年发行。专辑中包括了关于女性权益、种族关系、北爱尔兰问题以及列侬与美国政府的问题的歌曲。专辑所获评价极差——评论家认为专辑“无法入耳”。其中的歌曲《》（Woman
Is the Nigger of the
World）作为单曲于同年发行，并在电视上播出。许多电台因歌曲中的单词[Nigger因为拒绝播放此曲](../Page/黑鬼.md "wikilink")。在1972年8月30日，列侬、小野大象的回忆乐队等为援助[威罗布克州立学校残障学院举行了在](../Page/威罗布克州立学校.md "wikilink")[麦迪逊广场花园两场公益演唱会](../Page/麦迪逊广场花园.md "wikilink")，演出大受欢迎，这也是列侬生前最后一次举行完整长度的演唱会。

#### 1973-75年: “迷失的周末”

[John_Lennon_last_television_interview_Tomorrow_show_1975.JPG](https://zh.wikipedia.org/wiki/File:John_Lennon_last_television_interview_Tomorrow_show_1975.JPG "fig:John_Lennon_last_television_interview_Tomorrow_show_1975.JPG")
当列侬录制专辑《》（Mind
Games）时，列侬和小野分居。列侬后来将这段长达18个月的分手称为他自己的“迷失的周末”，在这段时间内列侬在洛杉矶和纽约由[庞凤仪陪伴](../Page/庞凤仪.md "wikilink")。《思想游戏》在1973年发行，记为塑胶洋子乐队的作品。列侬在这期间也为斯塔尔的专辑《林格》提供了歌曲《I'm
the Greatest》，这首歌曲的另一个由列侬伴唱的版本收录在专辑《》中。

在1974年初，列侬大量饮酒并经常与歌手[哈利·尼尔森在酒后做出滑稽举动](../Page/哈利·尼尔森.md "wikilink")，这也成为了新闻头条。3月，列侬先是头戴卫生巾与一位酒吧女服务员扭打在一起，后来又在同一家酒吧里因与[窒息兄弟乐队争吵而被驱逐出去](../Page/窒息兄弟.md "wikilink")。列侬决定帮助制作尼尔森的专辑《Pussy
Cats》，同时，庞凤仪在洛杉矶租用了一家房屋邀请所有音乐人来做客。但是在度过一个花天酒地的3月后，列侬决定搬到了纽约在庞的帮助下完成新专辑。在4月，列侬成为了[米克·贾格尔歌曲](../Page/米克·贾格尔.md "wikilink")《太多厨师（毁了汤）》（Too
Many Cooks (Spoil the
Soup)）的制作人，出于种种原因，这首歌曲在30年后才广为人知。2007年，麦克·贾格尔的精选集中收录了这首歌。\[4\]

在纽约定居后，列侬录制了专辑《墙与桥》（Walls and
Bridges）。在1974年10月专辑发行后，列侬收获了他生前唯一一支单飞时期的冠军单曲——《无论什么让你度过黑夜》（Whatever
Gets You Thru the
Night），歌曲是列侬与[埃尔顿·约翰合作的杰作](../Page/埃尔顿·约翰.md "wikilink")，埃尔顿·约翰负责和声伴奏和弹钢琴。列侬还在年末发行了专辑中的另外一首歌曲《》（\#9
Dream）。同期，列侬又一次斯塔尔的新专辑提供帮助。在11月28日，列侬在[麦迪逊广场花园举行的埃尔顿](../Page/麦迪逊广场花园.md "wikilink")·约翰感恩节演出上登场，这次意外登场是列侬之前许下的承诺（如果《无论什么让你度过黑夜》能成为冠军单曲，列侬就会与埃尔顿·约翰同台演出，而事实上列侬之前并不看好这首歌）的兑现。列侬演唱了两首披头士时期的歌曲——《[露西在缀满钻石的天空](../Page/露西在缀满钻石的天空.md "wikilink")》和《[我看见她站在那里](../Page/I_Saw_Her_Standing_There.md "wikilink")》。在演唱最后一首歌曲之前，列侬介绍到：“这首歌献给我阔别已久的老未婚妻保罗。”这也是约翰·列侬的最后一次现场表演。

1975年1月，列侬合作创作了大卫·鲍威的第一支美国冠军单曲《Fame》并为鲍威提供了吉他伴奏。同月，埃尔顿·约翰翻唱了《露西在缀满钻石的天空》，列侬帮助伴唱和吉他伴奏（在单曲的内页上出现了列侬的化名“Dr.
Winston
O'Boogie”），这一翻唱版本成为了一支冠军单曲。不久，小野洋子与列侬重归于好。列侬在1975年2月发行新专辑《》，在专辑中，列侬翻唱了在自己青年时代影响过自己的经典摇滚乐。这是列侬退出乐坛之前的最后一张专辑，专辑中也收录了列侬引退前的最后一支单曲《[Stand
By
Me](../Page/Stand_By_Me.md "wikilink")》。随后，列侬在4月18日参加了向传媒大亨致敬的一档电视节目，这也是列侬生前最后一次在电视上表演，演唱了《Stand
By
Me》和《Imagine》。\[5\]随着与小野和好以及新专辑的畅销与收到的好评，列侬的“迷失的周末”就此结束，列侬的生活也将出现新的转变。

#### 1975–80:退出乐坛和短暂回归

他的第二个儿子西恩于1975年10月9日出生后，列侬承担了家庭主夫的角色，
他把他的音乐产业中断了五年，期间他把他所有精力用来关注他的家庭。在一个月内，他履行了他的合同义务，在此期间，百代公司的一张名为*[Shaved
Fish](../Page/Shaved_Fish.md "wikilink")*的专辑被发布（一个曾经录制过的曲目的精选集）。
他致力于肖恩，他每天早上六点计划并准备他的饭菜，并与他共度时光。
他曾经写到“充满爱的厨房”给[斯塔尔](../Page/斯塔尔.md "wikilink")*[Ringo's
Rotogravure](../Page/Ringo's_Rotogravure.md "wikilink")*（1976）,
六月赛道上的表演将成为他直到1980年的最后一次录音。
在1977年东京，他正式宣布了他在音乐上的重大突破，他说：“我们已经基本决定了：我们没有任何伟大的决策，我们决定尽我们所能与我们的孩子一起，直到我们觉得我们可以花时间来放纵自己去创造家庭以外的东西。
在他离职期间他创造了几个系列的图纸并起草了一本包含自传材料和被他称作“疯狂的东西”的书。 但所有的这一切都将在他死后出版。

列侬在1980年10月独自休假。他下个月的专辑中*[Double
Fantasy](../Page/Double_Fantasy.md "wikilink")*就包含了列侬在六月前在一个43英尺的帆船在百慕大群岛时所写的歌。
这反映了他在找到稳定的家庭生活方面上的新成就。 额外的材料被记录在一个后续专辑的计划中。它叫做 [Milk and
Honey](../Page/Milk_and_Honey.md "wikilink")（于1984年死后被公布）。 由列侬和小野洋子共同发布。

### 1980年12月8日：遇刺身亡

1980年12月8日22時50分，列侬在纽约的寓所前被一名据称患有精神病的美國狂热男性歌迷[馬克·大衛·查普曼枪杀](../Page/馬克·大衛·查普曼.md "wikilink")，死时年仅40岁，举世震惊。摇滚乐坛也失去了一位传奇色彩的人物。

## 与他人的关系

### 辛西娅·列侬

列侬和辛西娅·鲍威尔（，1939年-2015年）为利物浦艺术学院的同学，于1957年相遇。她虽然被列侬的举止态度和外表吓到，但她听说他为法国演员[碧姬·芭杜着迷后把头发染成了金色](../Page/碧姬·芭杜.md "wikilink")。列侬邀请她约会，但当她说自己已经订婚，他大叫：“我又没让你他妈的嫁给我，对吗？{{\#tag:ref|原文：|group="原文"}}”她经常陪他去采石工人乐队的演出，还与麦卡特尼当时的女友一起去汉堡探望他。天性善妒的列侬发展出了强烈的占有欲，他的脾气和暴力常使辛西娅受到惊吓。列侬在日后表示，他在遇到小野前从未怀疑过自己对女性的[大男子主义态度](../Page/沙文主义.md "wikilink")。他说披头士歌曲《》讲述的是他自己的故事：“我曾对女人很冷酷，在身体方面——任何女人。我打人。我无法表达自己，就打人。我和男人打架，我打女人。那是为什么我总是呼吁和平。{{\#tag:ref|原文：|group="原文"}}”

1962年7月，列侬得知辛西娅怀孕了。回忆自己当时的反应，他说：“只有一条路可走了，辛。我们必须得结婚。{{\#tag:ref|原文：|group="原文"}}”两人于8月23日在利物浦愉快山（）登记处结婚。他婚后生活的开始正值[披头士狂热席卷英国](../Page/披头士狂热.md "wikilink")。在结婚当日的晚上，他有演唱会要出场，之后的每一天也几乎都是如此。爱普斯坦担心已婚的披头士成员这一概念会疏远歌迷，让列侬夫妇将结婚一事保密。朱利安于1963年4月8日出生。列侬当时在巡演途中，直到三天后才见到自己的新生儿。

辛西娅把这场婚姻瓦解的开始归因于，她感到列侬渐渐对她失去兴趣。1967年，乐队乘火车前往威尔士[班戈去参加](../Page/班戈_\(圭内斯\).md "wikilink")[玛哈瑞诗·玛哈士](../Page/玛哈瑞诗·玛哈士.md "wikilink")（）的超觉静坐研讨会，一位警察没有认出辛西娅，不让她上车。她后来回忆道，这个意外事件似乎象征了他们婚姻的终结。1968年5月，辛西娅从度假地早一天回家时，发现列侬与小野在一起，她离开家去了朋友的住处。[亚历克谢斯·马尔达斯](../Page/亚历克谢斯·马尔达斯.md "wikilink")（）日后声称自己在当晚和她发生了关系。几周后马尔达斯告知辛西娅，列侬试图以他俩通奸为由要求离婚和朱利安的抚养权。协商后，列侬屈从了，同意她用相同理由提出离婚诉讼。这一案于1968年11月达成庭外和解，辛西娅获得了朱利安的抚养权，列侬给了她10万英镑，每年还需支付她一笔小额费用。

### 布莱恩·爱普斯坦

1961年11月，当披头士乐队在利物浦[洞穴俱乐部完成一次午场演唱会后](../Page/洞穴俱乐部.md "wikilink")，他们被介绍认识了爱普斯坦。爱普斯坦是一位同性恋，据传记作者菲利普·诺曼（）所写，他想要管理披头士的原因之一是他被列侬的长相所吸引。朱利安刚出生后不久，列侬和爱普斯坦一起去西班牙度假，引发了对两人关系的臆测。日后被问及此事时，列侬说道：“啊，那几乎是一起风流韵事，但不完全是。它从未真正实现。但那是一段蛮强烈的关系。那是我首次和一位我知道他是同性恋的同性恋在一起。我们曾常常坐在[托雷莫利诺斯的一家咖啡馆里看着那里的小伙子们](../Page/托雷莫利诺斯.md "wikilink")，然后我会说：‘你喜欢那个吗？你喜欢这个吗？’我那时正享受着这个经历，像个作家一样心想：我正在经历这个。{{\#tag:ref|原文：|group="原文"}}”他们从西班牙回来后不久，在1963年6月，麦卡特尼的二十一岁生日会上，列侬揍了洞穴俱乐部的司仪鲍勃·伍勒（），因为后者问他：“你的蜜月过得如何，约翰？”伍勒闻名于他的文字游戏和善意但讽刺的评论，他是在开玩笑。但那时距离列侬结婚已过去十个月，而被推迟的蜜月要到两个月后才发生。对于当时醉酒的列侬来说，他的理解很简单：“他叫我[酷兒](../Page/酷兒.md "wikilink")，所以我要把他该死的肋骨打坏。{{\#tag:ref|原文：|group="原文"}}”

列侬喜欢嘲弄爱普斯坦的性取向和犹太人身份。当爱普斯坦为他的自传书名征询意见时，列侬提议《基佬犹太人》（）；得知最终采用的标题为《一地下室的噪音》（）后，他讽刺道：“更像是《一地下室的男孩子》（）。”他粗率地对一位拜访爱普斯坦住处的客人说：“你是来这里敲诈他的吗？如果不是的话，你是伦敦唯一一个没这么做的操屁股的。{{\#tag:ref|原文：|group="原文"}}”在录制歌曲《》期间，列侬把副歌歌词唱成“”（意为“宝贝，你是个有钱的基佬犹太人”）。

### 朱利安·列侬

[Julian_Lennon.jpg](https://zh.wikipedia.org/wiki/File:Julian_Lennon.jpg "fig:Julian_Lennon.jpg")揭幕仪式上的朱利安，2010年10月。\]\]

列侬的长子朱利安出生时正值[披头士狂热在英国达到顶峰](../Page/披头士狂热.md "wikilink")，他愈加投身于披头士的音乐事业。朱利安出生于1963年4月8日，当时列侬正随乐队巡演。朱利安的诞生和他母亲辛西娅与列侬的婚姻一样是一个秘密，因为爱普斯坦认为公众得知此事会威胁到披头士的商业利益。
朱利安回忆自己差不多四岁大时，放学回家拿着一副自己创作的水彩画。画的内容是班上的一名女同学在群星之间。他父亲看了便问：“你画的这是什么？”朱利安答道：“这是露西在缀满钻石的天空。”
列侬便由此创作了[同名歌曲](../Page/Lucy_in_the_Sky_with_Diamonds.md "wikilink")，虽然后来因为歌名首字母缩写为[LSD而饱受争议](../Page/D-麦角酸二乙胺.md "wikilink")，列侬坚持认定这首歌与毒品无关。
麦卡特尼同样认定列侬的解释，即歌名的来源就是朱利安童年单纯的画作。
列侬和朱利安关系疏远，后者觉得自己和麦卡特尼的关系比和父亲的更亲近。列侬处理离婚期间，麦卡特尼在去探望辛西娅和朱利安的轿车上创作了歌曲《》，试图安慰他。这首歌最终成为了披头士歌曲《》。列侬后来说：“那是他最好的歌。它开始是一首关于我儿子朱利安的歌……他将它变成了《》。我一直以为它是关于我和洋子的，但他说它不是。{{\#tag:ref|原文：|group="原文"}}”

列侬和朱利安的关系已经相当紧张，他和小野在1971年搬到纽约后，朱利安直到1973年才再一次见到父亲。在[庞凤仪的撮合下](../Page/庞凤仪.md "wikilink")，朱利安（和他母亲）被安排到洛杉矶与列侬见面，父子两人一起去了[迪士尼乐园](../Page/迪士尼乐园.md "wikilink")。朱利安开始定期地与父亲见面，列侬让他在歌曲《》中打鼓。他给朱利买了一把吉他以及其他乐器，鼓励他对音乐的兴趣，向他展示吉他和弦技巧。朱利安回忆他和父亲在他留在纽约期间“相处得好了很多”：“我们玩得很开心，一起大笑，总的来说度过了许多好时光。{{\#tag:ref|原文：|group="原文"}}”

在去世前不久的一次与《花花公子》采访中，列侬说：“西恩是一个计划内的孩子，区别就在这一点上。我对朱利安的爱不少一分。他仍旧是我的儿子，不管他的诞生是因为一瓶威士忌还是因为那些年还没有避孕药。他在这里，他属于我，他永远都是。{{\#tag:ref|原文：|group="原文"}}”他表示自己正尝试与当时17岁的长子重新建立父子关系，自信地预言道：“朱利安和我在将来会有一段关系{{\#tag:ref|原文：|group="原文"}}”列侬去世后，他的遗嘱据显示给朱利安的部分非常少。

### 小野洋子

1968年: 与先锋派艺术家小野洋子相遇,之后和辛西亚的婚姻破裂 1969年:与此同时小野洋子在直布羅陀结婚.
70年代:与小野洋子在纽约安家,继续音乐和艺术创作。

### 西恩·列侬

列侬和小野復合后，她怀孕了。但她在此前试图和列侬生孩子时已经历了三次[流产](../Page/流产.md "wikilink")，小野说这次她想要[堕胎](../Page/堕胎.md "wikilink")。她同意在列侬成为[家庭主夫的条件下继续妊娠](../Page/家庭主夫.md "wikilink")，他照做了。西恩于1975年10月9日由[剖宫产出生](../Page/剖宫产.md "wikilink")，正逢列侬的35岁生日。列侬在接下来的五年中暂停了音乐事业，让摄影师在西恩生命的第一年中每天为他拍一张照片。他还为西恩创作了无数绘画，在他死后出版为《真爱：为西恩作的画》（）。列侬日后自豪地宣称：“他没从我的肚子里生出来，但老天作证，我做了他的骨头，因为我照料了他的每顿饭，注意了他睡得如何，注意了他像鱼一样游泳的事实。{{\#tag:ref|原文：|group="原文"}}”

### 前披头士成员

[The_Beatles_in_America.JPG](https://zh.wikipedia.org/wiki/File:The_Beatles_in_America.JPG "fig:The_Beatles_in_America.JPG")

披头士在1970年解散后，尽管列侬和斯塔尔的友谊一直维持不变，但他与麦卡特尼及哈里森的关系有所起伏。他一开始与哈里森关系亲近，但在列侬搬去美国后，两人渐行渐远。1974年12月，哈里森为他的《》专辑巡演来到纽约。列侬答应在哈里森的演唱会上登场，但在一场争论后没有亮相。争论缘起于列侬拒签正式解除披头士合伙关系的法律文件（他最终在弗罗里达与[庞凤仪和朱利安度假时签署了这份文件](../Page/庞凤仪.md "wikilink")）。1980年，哈里森出版的自传中几乎没有提及列侬，让他觉得受到了冒犯。列侬告诉《[花花公子](../Page/花花公子.md "wikilink")》杂志：“我受到了伤害。因显而易见的忽略……我对他人生的影响完全为零……他记得后来的日子里遇到的每一个无足轻重的萨克斯风乐手或吉他手。而书里没有我。{{\#tag:ref|原文：|group="原文"}}”

列侬对麦卡特尼保留着最强烈的情感。除了用歌曲《》的歌词攻击他外，列侬在乐队解散后的三年中通过报刊与他进行了骂战。后来，两人开始渐渐重拾他们曾经拥有的亲密友谊。1974年，他们甚至再次在一起鼓捣音乐，但之后最终又疏远。列侬说，麦卡特尼在1976年4月最后一次拜访他期间，他们看了一集《[周六夜现场](../Page/周六夜现场.md "wikilink")》，制作人[洛恩·迈克尔斯](../Page/洛恩·迈克尔斯.md "wikilink")（）为披头士在节目上重组悬赏了3000美元现金。两人考虑当即前往录影棚完成一次搞笑亮相拿到奖金，但因太累了没有去。列侬在去世前三天的一次采访中总结了他对麦卡特尼的感情：“在我的整个音乐生涯中，我只选择过……两个搭档：保罗·麦卡特尼和小野洋子……我选得不坏。{{\#tag:ref|原文：|group="原文"}}”

在与麦卡特尼失和期间，列侬一直感到和他有音乐上的竞争关系，时刻留意麦卡特尼的作品。在他远离乐坛的五年间，只要列侬认为麦卡特尼在制作平庸的音乐，他就满足于自己的隐退生活。麦卡特尼于1980年发行歌曲《》时，他注意到了。那一年也是他重返录音室的一年和他生命的最后一年。他开玩笑地抱怨道：“那首歌把我逼疯了！”，因为它的旋律在他的脑海中挥之不去。当被问到乐队成员之间的关系是可畏的敌人还是最好的朋友，列侬说两种都不是，他已经有很长时间没见到任何一位披头士成员了。但他也说道：“我依旧爱这些人。披头士已经终结了，但约翰、保罗、乔治和林戈继续前进。{{\#tag:ref|原文：|group="原文"}}”

## 身后评价

[Lennon_Statue,_Liverpool.jpg](https://zh.wikipedia.org/wiki/File:Lennon_Statue,_Liverpool.jpg "fig:Lennon_Statue,_Liverpool.jpg")外的塑像\]\]
音乐历史学家辛德和施瓦茨在写到上世纪50到60年代的音乐风格变化时写道，披头士对音乐的影响说得再大都不过分，因为他们“使声音、风格、对流行音乐的态度发生了翻天覆地的改变，为英国摇滚艺术家的浪潮开辟了道路”，此外，乐队“在60年代中后期拓宽了摇滚风格的边疆”。很多乐队都视约翰·列侬为个人英雄，其中包括[绿洲乐队](../Page/绿洲乐队.md "wikilink")，其主唱[連恩·蓋勒格将自己的孩子取名为列侬](../Page/連恩·蓋勒格.md "wikilink")·蓋勒格以表达对约翰的敬意。在1999年举行的上，BBC宣布了此前举行的“英国人最爱的歌词”投票的结果，列侬的《想象》（Imagine）成为赢家。

在[卫报](../Page/卫报.md "wikilink")2006年的一篇文章中，乔恩·维纳写道：“对于那些经历过1972年的年轻人来讲，见证列侬和\[美国总统\]尼克松作对的勇气是惊心动魄的。那种愿意舍弃自己的音乐生涯与生命的意愿是当今人们仍敬仰他的原因之一。”对于音乐历史学家尤里什和比伦来说，列侬最重要的贡献是“创作出了歌曲中的那些为了人类处境、描述人类处境、面向人类处境的自我剖析。”

在2002年，利物浦的机场被重命名为[利物浦约翰·列侬机场](../Page/利物浦约翰·列侬机场.md "wikilink")。\[6\]2010年，在列侬诞辰70周年之际，[约翰·列侬和平纪念碑在利物浦](../Page/约翰·列侬和平纪念碑.md "wikilink")[查韦斯公园落成](../Page/查韦斯公园.md "wikilink")，并由辛西娅和朱利安·列侬揭幕。\[7\]雕塑被命名为“和平与和谐”，上有和平标志，并配有注释“为保护生命而渴求和平·纪念约翰·列侬1940–1980”。\[8\]

2013年12月，[国际天文联合会将水星上的一个陨石坑以列侬命名](../Page/国际天文联合会.md "wikilink")。\[9\]

2018年9月7日，美国纽约邮政局发行一枚印有约翰·列侬照片的邮票。这张照片拍摄于1974年美国纽约[曼哈顿的一幢公寓楼顶上](../Page/曼哈顿.md "wikilink")\[10\]。

### 获奖与销量

列侬—麦卡特尼的作曲组合被认为是20世纪最有影响，最为成功的作曲组合之一。作为表演者、作者和合作者，列侬共有25支美国[告示牌百强单曲榜冠军单曲](../Page/告示牌百强单曲榜.md "wikilink")。他的单飞生涯美国专辑销量至少为1400万。列侬生前的最后一张专辑《[Double
Fantasy》是他单飞生涯中销量最高的专辑](../Page/Double_Fantasy.md "wikilink")，在美国销售300万张并获得1981年[格莱美年度专辑奖项](../Page/格莱美.md "wikilink")。次年，列侬获得[全英音乐奖](../Page/全英音乐奖.md "wikilink")。

列侬是2002年BBC举行的[最伟大的100名英国人投票中](../Page/最伟大的100名英国人.md "wikilink")，列侬位列第8。在2003年到2008年之间，[滚石杂志多次将他列入各种榜单](../Page/滚石杂志.md "wikilink")，在“历史上最伟大100名歌手”中，列侬位居第5，在“历史上最伟大100位音乐家”中，披头士高居榜首，列侬凭借单飞生涯成就位列38。他的专辑《[约翰·列侬/塑胶小野乐团](../Page/约翰·列侬/塑胶小野乐团.md "wikilink")》和《[想象](../Page/想像_\(专辑\).md "wikilink")》分列滚石杂志评选的[滚石杂志五百大专辑榜单的](../Page/滚石杂志五百大专辑.md "wikilink")22位和76位，而披头士则有10张专辑上榜，4张进入前十，并凭借专辑《[佩珀中士的寂寞之心俱乐部乐队](../Page/佩珀中士的寂寞之心俱乐部乐队.md "wikilink")》取得榜首位置。1965年，他与披头士其他成员获得[大英帝國勳章](../Page/MBE.md "wikilink")。列侬身后于1987年进入，于1994年被引进[摇滚名人堂](../Page/摇滚名人堂.md "wikilink")。

## 作品目录

  - 《》 （与小野洋子合作）（1968）
  - 《》 （与小野洋子合作）（1969）
  - 《》（与小野洋子合作）（1969）
  - 《》（1970）
  - 《》（1971）
  - 《》（与小野洋子合作）（1972）
  - 《》（1973）
  - 《》（1974）
  - 《》（1975）
  - 《》（与小野洋子合作）（1980）
  - 《》（与小野洋子合作）（1984）

### 电影

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>片名</p></th>
<th><p>角色</p></th>
<th><p>注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1964</p></td>
<td><p>《<a href="../Page/一夜狂欢_(电影).md" title="wikilink">一夜狂欢</a>》</p></td>
<td><p>他自己</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1965</p></td>
<td><p><a href="../Page/救命_(电影).md" title="wikilink">救命</a></p></td>
<td><p>他自己</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1967</p></td>
<td><p><em><a href="../Page/How_I_Won_the_War.md" title="wikilink">How I Won the War</a></em></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1967</p></td>
<td><p><em><a href="../Page/Magical_Mystery_Tour_(film).md" title="wikilink">Magical Mystery Tour</a></em></p></td>
<td><p>他自己 / Ticket Salesman / Magician with Coffee</p></td>
<td><p>Also Narrator, writer and director (producer uncredited)</p></td>
</tr>
<tr class="odd">
<td><p>1968</p></td>
<td><p>《<a href="../Page/黄色潜水艇_(电影).md" title="wikilink">黄色潜水艇</a>》</p></td>
<td><p>他自己</p></td>
<td><p>片末的彩蛋出场</p></td>
</tr>
<tr class="even">
<td><p>1968</p></td>
<td><p><em><a href="../Page/Unfinished_Music_No._1:_Two_Virgins.md" title="wikilink">Two Virgins</a></em></p></td>
<td></td>
<td><p>短片</p></td>
</tr>
<tr class="odd">
<td><p>1970</p></td>
<td><p><em>Apotheosis</em></p></td>
<td></td>
<td><p>Short Film</p></td>
</tr>
<tr class="even">
<td><p>1970</p></td>
<td><p><em><a href="../Page/Let_It_Be_(1970_film).md" title="wikilink">Let It Be</a></em></p></td>
<td><p>他自己</p></td>
<td><p>Documentary (executive producer - as The Beatles)</p></td>
</tr>
</tbody>
</table>

### 电视

| Year      | Title                                                                | Role                       | Notes                                                      |
| --------- | -------------------------------------------------------------------- | -------------------------- | ---------------------------------------------------------- |
| 1965-1966 | *[Not Only... But Also](../Page/Not_Only..._But_Also.md "wikilink")* | Lavatory Attendant / Guest | Episode: "Episode \#1.1 (1965) & Christmas Special (1966)" |

### 書籍

  - 《[In His Own
    Write](../Page/In_His_Own_Write.md "wikilink")》（1964年4月27日）
  - 《[A Spaniard in the
    Works](../Page/A_Spaniard_in_the_Works.md "wikilink")》（1965年6月14日）
  - 《[Skywriting by Word of
    Mouth](../Page/Skywritng_by_Word_of_Mouth.md "wikilink")》（1986年10月10日）

## 注释

Lennon was responsible for 25 [*Billboard* Hot
100](../Page/Billboard_Hot_100.md "wikilink") number one singles as
performer, writer or co-writer.

  - Solo (2): "[Whatever Gets You thru the
    Night](../Page/Whatever_Gets_You_thru_the_Night.md "wikilink")",
    "[(Just Like) Starting
    Over](../Page/\(Just_Like\)_Starting_Over.md "wikilink")".
  - With The Beatles (20): "[Can't Buy Me
    Love](../Page/Can't_Buy_Me_Love.md "wikilink")", "[I Feel
    Fine](../Page/I_Feel_Fine.md "wikilink")", "[I Want to Hold Your
    Hand](../Page/I_Want_to_Hold_Your_Hand.md "wikilink")", "[Love Me
    Do](../Page/Love_Me_Do.md "wikilink")", "[She Loves
    You](../Page/She_Loves_You.md "wikilink")", "[A Hard Day's
    Night](../Page/A_Hard_Day's_Night_\(song\).md "wikilink")", "[Eight
    Days a Week](../Page/Eight_Days_a_Week.md "wikilink")",
    "[Help\!](../Page/Help!_\(song\).md "wikilink")", "[Ticket to
    Ride](../Page/Ticket_to_Ride.md "wikilink")",
    "[Yesterday](../Page/Yesterday_\(Beatles_song\).md "wikilink")",
    "[Paperback Writer](../Page/Paperback_Writer.md "wikilink")", "[We
    Can Work It Out](../Page/We_Can_Work_It_Out.md "wikilink")", "[All
    You Need Is Love](../Page/All_You_Need_Is_Love.md "wikilink")",
    "[Hello, Goodbye](../Page/Hello,_Goodbye.md "wikilink")", "[Penny
    Lane](../Page/Penny_Lane.md "wikilink")", "[Hey
    Jude](../Page/Hey_Jude.md "wikilink")",
    "[Something](../Page/Something_\(Beatles_song\).md "wikilink")"/"[Come
    Together](../Page/Come_Together.md "wikilink")", "[Get
    Back](../Page/Get_Back.md "wikilink")", "[Let It
    Be](../Page/Let_It_Be_\(song\).md "wikilink")", "[The Long and
    Winding
    Road](../Page/The_Long_and_Winding_Road.md "wikilink")"/"[For You
    Blue](../Page/For_You_Blue.md "wikilink")".\[11\]
  - As co-writer of and performer on releases by another artist (2):
    "[Fame](../Page/Fame_\(David_Bowie_song\).md "wikilink")" (David
    Bowie)., "[Lucy in the Sky with
    Diamonds](../Page/Lucy_in_the_Sky_with_Diamonds.md "wikilink")"
    (Elton John).
  - As co-writer of release by other artists (1): "[A World Without
    Love](../Page/A_World_Without_Love.md "wikilink")" ([Peter and
    Gordon](../Page/Peter_and_Gordon.md "wikilink"))

### 译注

## 参考文献

### 引用

### 来源

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
## 延伸阅读

  - [Kane, Larry](../Page/Larry_Kane.md "wikilink") (2007). *[Lennon
    Revealed](http://books.google.ca/books?id=TFEr_3tDLYQC&lpg=PP1&dq=Lennon%20Revealed&pg=PP1#v=onepage&q&f=true)*.
    Running Press. ISBN 978-0-7624-2966-0
  - Pang, May; Edwards, Henry (1983). *Loving John: The Untold Story*.
    Warner Books. ISBN 978-0-446-37916-8.
  - [Riley, Tim](../Page/Tim_Riley_\(music_critic\).md "wikilink")
    (2011). *[Lennon: Man, Myth,
    Music](http://books.google.com/books?id=Pf2YAAAAQBAJ)*. Hyperion.
    ISBN 978-1-4013-2452-0
  - Wiener, Jon. [The John Lennon FBI
    Files](http://www.LennonFBIfiles.com/)
  - Yorke, Richard (1969). ["John Lennon: Ringo's Right, We Can't Tour
    Again"](https://web.archive.org/web/20071210181240/http://crawdaddy.wolfgangsvault.com/Article.aspx?id=4372),
    *New Musical Express*, 7 June 1969, reproduced by
    *[Crawdaddy\!](../Page/Crawdaddy!.md "wikilink")*, 2007.

## 外部連結

  -
  -
  - [列侬在美国联邦调查局（FBI）的档案](http://vault.fbi.gov/john-winston-lennon)

[Category:英國搖滾歌手](../Category/英國搖滾歌手.md "wikilink")
[L](../Category/披頭四樂團.md "wikilink")
[约翰·列侬](../Category/约翰·列侬.md "wikilink")
[L](../Category/英国吉他手.md "wikilink")
[Category:英國創作歌手](../Category/英國創作歌手.md "wikilink")
[Category:节奏吉他手](../Category/节奏吉他手.md "wikilink")
[Category:男中音](../Category/男中音.md "wikilink")
[L](../Category/在美國被謀殺身亡者.md "wikilink")
[L](../Category/在國外被謀殺身亡英國人.md "wikilink")
[L](../Category/利物浦人.md "wikilink")
[Category:愛爾蘭裔英格蘭人](../Category/愛爾蘭裔英格蘭人.md "wikilink")
[L](../Category/MBE勳銜.md "wikilink")
[L](../Category/摇滚名人堂入选者.md "wikilink")
[Category:葛萊美終身成就獎獲得者](../Category/葛萊美終身成就獎獲得者.md "wikilink")
[Category:艾弗·诺韦洛奖获得者](../Category/艾弗·诺韦洛奖获得者.md "wikilink")
[Category:被谋杀的男演员](../Category/被谋杀的男演员.md "wikilink")
[Category:英國反戰人士](../Category/英國反戰人士.md "wikilink")
[L](../Category/素食主義者.md "wikilink")
[Category:英國女性主義者](../Category/英國女性主義者.md "wikilink")
[Category:男性女性主義者](../Category/男性女性主義者.md "wikilink")
[Category:移民美國的英格蘭人](../Category/移民美國的英格蘭人.md "wikilink")
[Category:帕洛風唱片歌手](../Category/帕洛風唱片歌手.md "wikilink")

1.
2.
3.  *LennoNYC*, PBS Television 2010
4.  *[The Very Best of Mick
    Jagger](../Page/The_Very_Best_of_Mick_Jagger.md "wikilink")* liner
    notes
5.  Madinger, *Eight Arms to Hold You*, 44.1 Publishing, 2000, ISBN
    978-0-615-11724-9
6.
7.
8.
9.
10. [John Lennon celebrated with release of commemorative
    stamp](https://www.houstonchronicle.com/entertainment/article/John-Lennon-celebrated-with-release-of-13213038.php).Houston
    Chronicle.
11.