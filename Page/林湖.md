**林湖**（，），[韓國](../Page/韓國.md "wikilink")[男演員](../Page/男演員.md "wikilink")，名字常被音譯為**任豪**。KBS第15期公開招募中入行，其多數飾演古裝劇中之帝王角色，其中以《[大長今](../Page/大長今_\(電視劇\).md "wikilink")》飾演之中宗大王最為觀眾熟知。

2010年3月6日，與小11歲的珠寶設計師尹貞姬結婚。育有一女二子。

## 演出作品

### 電視劇

  - 1994年：KBS《[韓明澮](../Page/韓明澮_\(電視劇\).md "wikilink")》飾演 月山大君
  - 1995年：[SBS](../Page/SBS_\(韓國\).md "wikilink")《[張禧嬪](../Page/張禧嬪_\(1995年電視劇\).md "wikilink")》飾演
    [朝鮮肅宗](../Page/朝鮮肅宗.md "wikilink")
  - 1996年：SBS《[萬剛](../Page/萬剛.md "wikilink")》飾演 萬剛
  - 1997年：[KBS](../Page/韓國放送公社.md "wikilink")《[欲望的海洋](../Page/欲望的海洋.md "wikilink")》飾演
    鄭敏宇
  - 1998年：[MBC](../Page/文化廣播_\(韓國\).md "wikilink")《[田園日記](../Page/田園日記.md "wikilink")》飾演
    金童
  - 1998年：MBC《[大王之道](../Page/大王之道.md "wikilink")》飾演
    [思悼世子](../Page/思悼世子.md "wikilink")
  - 2000年：MBC《[許浚](../Page/醫道.md "wikilink")》飾演 李政明
  - 2000年：MBC《[為了你](../Page/為了你.md "wikilink")》飾演 한영준
  - 2001年：MBC《[每日與你](../Page/每日與你.md "wikilink")》飾演 崔東河
  - 2001年：MBC《[那女人的家](../Page/那女人的家.md "wikilink")》飾演 金南赫
  - 2001年：MBC《美味美爱》飾演 女主角相親對象
  - 2002年：KBS《[女高中同學](../Page/女高中同學.md "wikilink")》
  - 2002年：KBS《[太陽人李濟馬](../Page/太陽人李濟馬.md "wikilink")》飾演 崔文煥
  - 2002年：KBS《總是忐忑不安》飾演 李俊浩
  - 2003年：MBC《[大長今](../Page/大長今_\(電視劇\).md "wikilink")》飾演
    [朝鮮中宗](../Page/朝鮮中宗.md "wikilink")
  - 2003年：MBC《[1%的可能性](../Page/1%的可能性.md "wikilink")》飾演 江先宇
  - 2003年：SBS《[初戀](../Page/初戀_\(2003年電視劇\).md "wikilink")》
  - 2003年：KBS《[千年之夢](../Page/千年之夢.md "wikilink")》
  - 2004年：SBS《[四個小冤家](../Page/四個小冤家.md "wikilink")》飾演 李鍾喆
  - 2004年：SBS《[不要獨自一人](../Page/不要獨自一人.md "wikilink")》飾演 林湖
  - 2004年：MBC《[我想要愛](../Page/我想要愛.md "wikilink")》飾演 朴修詠
  - 2004年：MBC《[想結婚的女子](../Page/想結婚的女子.md "wikilink")》飾演 朴善友
  - 2005年：KBS《[風花](../Page/風花.md "wikilink")》飾演 崔亨柱
  - 2005年：SBS《[花样女子](../Page/花样女子.md "wikilink")》飾演 尹秀遠
  - 2005年：MBC《[春天的微笑](../Page/春天的微笑.md "wikilink")》
  - 2006年：SBS《[赤足爱情](../Page/赤足爱情.md "wikilink")》飾演 黃振石
  - 2006年：KBS《[大祚荣](../Page/大祚榮_\(電視劇\).md "wikilink")》飾演
    [泉男生](../Page/泉男生.md "wikilink")
  - 2007年：OCN《[黑道奶爸](../Page/黑道奶爸.md "wikilink")》飾演 趙彪奇
  - 2008年：KBS《[新夫婦的誕生](../Page/新夫婦的誕生.md "wikilink")》飾演 金鋒水
  - 2008年：SBS《[為什麼來我家](../Page/為什麼來我家.md "wikilink")》飾演 金泰平
  - 2008年：KBS《[最強七迂](../Page/最強七迂.md "wikilink")》飾演
    [昭顯世子](../Page/昭顯世子.md "wikilink")
  - 2008年：SBS《[風之畫員](../Page/風之畫員.md "wikilink")》飾演 李命基
  - 2009年：MBC《[善德女王](../Page/善德女王_\(韓國電視劇\).md "wikilink")》飾演
    [真智王](../Page/真智王.md "wikilink")
  - 2009年：KBS《[不能結婚的男人](../Page/不能結婚的男人_\(韓國電視劇\).md "wikilink")》飾演 朴光南
  - 2009年：KBS《[她的風格](../Page/她的風格.md "wikilink")》飾演 女主角相親對象
  - 2010年：SBS《[不懂女人](../Page/不懂女人.md "wikilink")》飾演 姜盛燦
  - 2011年：KBS《[廣開土太王](../Page/廣開土太王_\(电视剧\).md "wikilink")》飾演
    [慕容宝](../Page/慕容宝.md "wikilink")
  - 2012年：MBN《[可疑的家族](../Page/可疑的家族.md "wikilink")》飾演 姜道尚
  - 2012年：[tvN](../Page/tvN.md "wikilink")《[我愛李太利](../Page/我愛李太利.md "wikilink")》飾演
    金山
  - 2013年：SBS《[你的女人](../Page/你的女人.md "wikilink")》飾演 羅鎮九
  - 2014年：KBS《[鄭道傳](../Page/鄭道傳_\(電視劇\).md "wikilink")》飾演
    [鄭夢周](../Page/鄭夢周.md "wikilink")
  - 2014年：KBS《[看書痴列傳](../Page/看書痴列傳.md "wikilink")》飾演
    [光海君](../Page/光海君.md "wikilink")
  - 2014年：KBS《[家人之間為何這樣](../Page/家人之間為何這樣.md "wikilink")》飾演 法官（特別出演）
  - 2015年：MBC《[華政](../Page/華政.md "wikilink")》飾演
    [崔鳴吉](../Page/崔鳴吉.md "wikilink")
  - 2015年：KBS《[星星閃爍](../Page/星星閃爍.md "wikilink")》飾演 徐東弼
  - 2015年：KBS《[生意之神 - 客主2015](../Page/生意之神_-_客主2015.md "wikilink")》飾演
    閔謙鎬
  - 2016年：KBS《[Page Turner](../Page/Page_Turner.md "wikilink")》飾演 醫生
  - 2016年：MBC《[獄中花](../Page/獄中花.md "wikilink")》飾演 姜善浩
  - 2016年：MBC《[不夜城](../Page/不夜城_\(電視劇\).md "wikilink")》飾演 姜在賢
  - 2017年：MBC《[訓長吳純南](../Page/訓長吳純南.md "wikilink")》飾演 張地浩（特別出演）
  - 2017年：KBS《[花開了，達順啊](../Page/花開了，達順啊.md "wikilink")》飾演 韓太成
  - 2019年：MBC《[龍王保祐](../Page/龍王保祐.md "wikilink")》飾演 趙志煥（特別出演）
  - 2019年：SBS《[獬豸](../Page/獬豸_\(電視劇\).md "wikilink")》飾演 李光佐

### 電影

  - 2000年：《[恐怖計程車](../Page/恐怖計程車.md "wikilink")》
  - 2001年：《[我的野蠻女友](../Page/我的野蠻女友.md "wikilink")》飾演 相親男（客串）
  - 2004年：《[Clementine](../Page/Clementine.md "wikilink")》飾演 韓俊雄
  - 2004年：《[Don't Tell Papa](../Page/Don't_Tell_Papa.md "wikilink")》飾演
    寶莉秀
  - 2006年：《[突然有一天之2月29日](../Page/突然有一天之2月29日.md "wikilink")》飾演 刑警
  - 2010年：《[秘密愛](../Page/秘密愛_\(2010年電影\).md "wikilink")》
  - 2010年：《地球代表 Rolling Star》（配音）

## 主持節目

  - 2014 MBC《向星葵》MC

## 綜藝嘉賓

  - SBS《X MAN》 E26
  - SBS《夜心萬萬》E211
  - 07062009 KBS《想像PLUS》 E233嘉賓
  - 06072009 MBC《來玩吧》E250嘉賓
  - 31102009 湖南衞視《快樂大本營》首爾之行嘉賓
  - 16062011 KBS《HAPPY TOGETHER》 E201嘉賓
  - 17062012 SBS《RUNNING MAN》E99嘉賓
  - 21082012 SBS《強心臟》E142嘉賓
  - 28082012 SBS《強心臟》E143嘉賓
  - 23082013 MBC《明星跳水秀》
  - 2014 KBS《媽媽的誕生》EP01-EP19
  - 20161113《蒙面歌王》EP85 參賽者

## 獎項

  - 2015年：[KBS演技大賞](../Page/KBS演技大賞.md "wikilink")－日日劇部門 男子優秀演技賞（《星星閃爍》）
  - 2016年：韓國大眾文化藝術賞－（文化勳章）林忠熙 (影視作品作家)，林湖替父親領獎

## 外部連結

  - [林湖 台灣粉絲 建立 Facebook](http://www.facebook.com/LimHo.Taiwan.Fans)

  - [EPG](https://web.archive.org/web/20070826223202/http://epg.epg.co.kr/Star/Profile/index.asp?actor_id=651)


  - [naver](http://people.search.naver.com/search.naver?where=nexearch&sm=tab_ppn&query=%EC%9E%84%ED%98%B8&os=94911&ie=utf8&key=PeopleService)

  - [Raemongraein](http://www.raemongraein.co.kr/)

  -
[L](../Category/韩国電視演员.md "wikilink")
[L](../Category/韓國電影演員.md "wikilink")
[L](../Category/韓國中央大學校友.md "wikilink")
[L](../Category/首爾特別市出身人物.md "wikilink")
[L](../Category/林姓.md "wikilink")
[L](../Category/韓國男演員.md "wikilink")