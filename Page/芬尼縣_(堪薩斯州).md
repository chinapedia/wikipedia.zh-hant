**芬尼县**（**Finney County,
Kansas**，簡稱**FI**）是[美國](../Page/美國.md "wikilink")[堪薩斯州西南部的一個縣](../Page/堪薩斯州.md "wikilink")。面積3,374平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口40,523人。縣治[加登城](../Page/加登城_\(堪薩斯州\).md "wikilink")
（Garden City）。

成立於1863年3月6日，原名**Sequoyah**；1883年2月21日改名。縣名紀念[州議員](../Page/堪薩斯州州議會.md "wikilink")、副州長大衛·W·芬尼
（David W. Finney）。\[1\]

## 参考文献

<div class="references-small">

<references />

</div>

[F](../Category/堪薩斯州行政區劃.md "wikilink")
[Finney](../Category/芬尼縣_\(堪薩斯州\).md "wikilink")
[Category:1883年堪薩斯州建立](../Category/1883年堪薩斯州建立.md "wikilink")

1.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.