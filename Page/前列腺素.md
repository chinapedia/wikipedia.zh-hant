[Prostaglandin_E1.svg](https://zh.wikipedia.org/wiki/File:Prostaglandin_E1.svg "fig:Prostaglandin_E1.svg")

**前列腺素**（，简称：）是一类具有五元[脂肪环](../Page/脂肪环.md "wikilink")、带有两个侧链（上侧链7个[碳原子](../Page/碳原子.md "wikilink")、下侧链8个碳原子）的20个碳的[酸](../Page/酸.md "wikilink")。是一类[激素](../Page/激素.md "wikilink")。

## 分类

  - 根据分子中五元脂肪环上[取代基](../Page/取代基.md "wikilink")（主要是[羟基和](../Page/羟基.md "wikilink")[氢](../Page/氢.md "wikilink")）的不同将PG分为A、B、C、D、E、F等类型，分别用PGA、PGB、PGC、PGD、PGE、PGF等表示；
  - 分子中侧链的双键数则标在E或F等的右下角，如上侧链和下侧链分别有一个[双键](../Page/双键.md "wikilink")，则称做[PGE<sub>2</sub>或](../Page/PGE2.md "wikilink")[PGF<sub>2</sub>](../Page/PGF2.md "wikilink")；
  - 再根据脂肪环上9位的[立体构型在命名时在数字之后加上α或β](../Page/立体构型.md "wikilink")，如[PGF<sub>2α</sub>](../Page/PGF2α.md "wikilink")。

## 化学结构

[PGA](../Page/PGA.md "wikilink") [PGB](../Page/PGB.md "wikilink")
[PGC](../Page/PGC.md "wikilink") [PGD](../Page/PGD.md "wikilink")
[PGE](../Page/PGE.md "wikilink") [PGF](../Page/PGF.md "wikilink")
[PGG](../Page/PGG.md "wikilink") [PGH](../Page/PGH.md "wikilink")
[PGI](../Page/PGI.md "wikilink") <sup>\[需要消歧义\]</sup>

## 发现史

  - 1936年，[Goldblatt和](../Page/w:Goldblatt.md "wikilink")[von
    Euler分别发现人](../Page/w:von_Euler.md "wikilink")[精液中含有一种可以引起](../Page/精液.md "wikilink")[平滑肌及](../Page/平滑肌.md "wikilink")[血管收缩的液体成分](../Page/血管.md "wikilink")，von
    Eular证明此物质是[脂溶性化合物](../Page/脂溶性化合物.md "wikilink")。
  - 1957年[Bergstorm及其](../Page/w:Bergstorm.md "wikilink")[瑞典同事分离出两种PG纯品](../Page/瑞典.md "wikilink")（[PGF<sub>1</sub>和](../Page/PGF1.md "wikilink")[PGF<sub>2α</sub>](../Page/PGF2α.md "wikilink")）
  - 1964年[艾里亞斯·詹姆斯·科里用](../Page/艾里亞斯·詹姆斯·科里.md "wikilink")[全合成法制备PG成功](../Page/全合成.md "wikilink")，这是PG类药物研究的重大突破。
  - 现在，不但所有天然PG已能用全合成法制取，还合成出许多PG类似物，他们常常比天然的PG有更好的临床应用范围及效果。

## 功能作用

  - PGE和PGF类衍生物可使妇女[子宫强烈收缩](../Page/子宫.md "wikilink")，可用于[终止妊娠和](../Page/流產.md "wikilink")[催产](../Page/催产.md "wikilink")。
  - [PGE<sub>1</sub>或](../Page/PGE1.md "wikilink")[PGE<sub>2</sub>和](../Page/PGE2.md "wikilink")[PGA能抑制](../Page/PGA.md "wikilink")[胃液的分泌](../Page/胃液.md "wikilink")，保护[胃壁细胞](../Page/胃壁细胞.md "wikilink")，可以用于治疗[胃溃疡](../Page/胃溃疡.md "wikilink")、出血性[胃炎及](../Page/胃炎.md "wikilink")[肠炎](../Page/肠炎.md "wikilink")。
  - [PGI<sub>2</sub>对](../Page/PGI2.md "wikilink")[血小板功能有多种生理作用](../Page/血小板.md "wikilink")，是当前[血栓形成药物研究的重要对象](../Page/血栓.md "wikilink")。

## 生物合成

  - 在研究[花生四烯酸生物合成和PG衍生物生物代谢时发现](../Page/花生四烯酸.md "wikilink")，PG与导致[炎症有关](../Page/炎症.md "wikilink")，[血栓素A](../Page/血栓素A2.md "wikilink")（[TXB](../Page/TXB2.md "wikilink")）则是促使[血小板凝聚形成](../Page/血小板.md "wikilink")[血栓的原因](../Page/血栓.md "wikilink")。
  - 这一发现不但解释了[非甾体抗炎药的作用机制](../Page/非甾体抗炎药.md "wikilink")，同时发现了[阿司匹林作为预防血栓的新功能](../Page/阿司匹林.md "wikilink")。

## 常见PG类药物

  - [前列地尔](../Page/前列地尔.md "wikilink")[Alprostadil](../Page/w:Alprostadil.md "wikilink")
      - 属[PGE<sub>1</sub>类](../Page/PGE1.md "wikilink")
      - [化学结构](../Page/:File:Alprostadil.png.md "wikilink")<图片链接>
      - 可扩张[血管](../Page/血管.md "wikilink")，抑制血小板[血栓素的合成](../Page/血栓素.md "wikilink")
      - 用于治疗[心绞痛](../Page/心绞痛.md "wikilink")、[心肌梗死](../Page/心肌梗死.md "wikilink")、[脑梗死](../Page/脑梗死.md "wikilink")
  - [米索前列醇](../Page/米索前列醇.md "wikilink")[Misoprostol](../Page/w:Misoprostol.md "wikilink")
      - 属[PGE<sub>1</sub>衍生物类](../Page/PGE1.md "wikilink")
      - [化学结构](../Page/:File:Misoprostol.png.md "wikilink")<图片链接>
      - 可抑制[胃酸分泌](../Page/胃酸.md "wikilink")，保护[胃粘膜](../Page/胃粘膜.md "wikilink")
      - 用于[消化道](../Page/消化道.md "wikilink")[溃疡和](../Page/溃疡.md "wikilink")[妊娠早期](../Page/妊娠.md "wikilink")[流产](../Page/流产.md "wikilink")
  - [地诺前列酮](../Page/地诺前列酮.md "wikilink")[Dinoprostone](../Page/w:Dinoprostone.md "wikilink")
      - 属[PGE<sub>2</sub>类](../Page/PGE2.md "wikilink")
      - [化学结构](../Page/:File:Dinoprostone.png.md "wikilink")<图片链接>
      - 可收缩[子宫](../Page/子宫.md "wikilink")[平滑肌](../Page/平滑肌.md "wikilink")，濃度高時肌層細胞收縮，低濃度下平滑肌鬆弛（[子宮乏力](../Page/子宮乏力.md "wikilink")）。（威廉產科學（上冊），第292頁)。
      - 用于[妊娠早期](../Page/妊娠.md "wikilink")[流產](../Page/流產.md "wikilink")
  - [卡前列素](../Page/卡前列素.md "wikilink")[Carboprost](../Page/w:Carboprost.md "wikilink")
      - 属[PGF<sub>2α</sub>类](../Page/PGF2α.md "wikilink")
      - [化学结构](../Page/:File:Carboprost.png.md "wikilink")<图片链接>
      - 可收缩[子宫](../Page/子宫.md "wikilink")
      - 用于抗[早孕](../Page/早孕.md "wikilink")、扩[宫颈及中期](../Page/宫颈.md "wikilink")[引产](../Page/引产.md "wikilink")
  - [卡前列甲酯](../Page/卡前列甲酯.md "wikilink")[Carboprost
    Methylate](../Page/W/w:Carboprost_Methylate.md "wikilink")
      - 属[PGE<sub>2α</sub>酯类](../Page/PGE2α.md "wikilink")
      - [化学结构](../Page/:File:Carboprost_Methylate.png.md "wikilink")<图片链接>
      - 可收缩子宫平滑肌
      - 用于抗早孕、扩宫颈及中期[引產](../Page/引產.md "wikilink")
  - [拉坦前列素](../Page/拉坦前列素.md "wikilink")[Latanoprost](../Page/w:Latanoprost.md "wikilink")
      - 属[PGF<sub>2α</sub>衍生物类](../Page/PGF2α.md "wikilink")
      - [化学结构](../Page/:File:Latanoprost.png.md "wikilink")<图片链接>
      - 用于治疗[青光眼](../Page/青光眼.md "wikilink")
  - [前列环素](../Page/前列环素.md "wikilink")[Prostacyclin](../Page/w:Prostacyclin.md "wikilink")
      - 属[PGI<sub>2</sub>类](../Page/PGEI2.md "wikilink")
      - [化学结构](../Page/:File:Prostacyclin.png.md "wikilink")<图片链接>
      - 具有抗[血小板凝集作用和扩张](../Page/血小板.md "wikilink")[血管作用](../Page/血管.md "wikilink")，对[冠脉有强力扩张作用](../Page/冠脉.md "wikilink")
      - 用于治疗[冠心病](../Page/冠心病.md "wikilink")、[心绞痛](../Page/心绞痛.md "wikilink")、[心肌梗死](../Page/心肌梗死.md "wikilink")

## 代谢

[PGE<sub>2</sub>在](../Page/PGE2.md "wikilink")[肺脏内合成及代谢](../Page/肺脏.md "wikilink")，经[动脉注入后](../Page/动脉.md "wikilink")5[分钟达最大的稳定](../Page/分钟.md "wikilink")[血药浓度](../Page/血药浓度.md "wikilink")，停止注入后5分钟恢复基础血药浓度。首次经过[肺循环后](../Page/肺循环.md "wikilink")，60-90％被代谢，经过[β氧化和](../Page/β氧化.md "wikilink")[ω氧化后](../Page/ω氧化.md "wikilink")，72小時内经[尿液和](../Page/尿液.md "wikilink")[粪便排出](../Page/粪便.md "wikilink")。

## 参见

  - [激素](../Page/激素.md "wikilink")
  - [药物列表](../Page/药物列表.md "wikilink")
  - 威廉產科學（上，下），F.GARY CUNNINGHAM著，第20版，蘇純閨譯，合記圖書發行。

## 外部連結

  -
[前列腺素类](../Category/前列腺素类.md "wikilink")
[Category:世界卫生组织基本药物](../Category/世界卫生组织基本药物.md "wikilink")