[Map_mn_bayankhongor_aimag.png](https://zh.wikipedia.org/wiki/File:Map_mn_bayankhongor_aimag.png "fig:Map_mn_bayankhongor_aimag.png")

**巴彦洪戈尔省**（）位於[蒙古國南部](../Page/蒙古國.md "wikilink")，面積115,977平方公里，人口76,085
(2011年)。首府[巴彦洪戈尔](../Page/巴彦洪戈尔.md "wikilink")。

## 参考文献

## 参见

  - [巴彦洪戈尔](../Page/巴彦洪戈尔.md "wikilink")

{{-}}

[Category:蒙古国省份](../Category/蒙古国省份.md "wikilink")
[巴彦洪戈尔省](../Category/巴彦洪戈尔省.md "wikilink")