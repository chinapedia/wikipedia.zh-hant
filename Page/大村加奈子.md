**大村加奈子**（），出生於[日本](../Page/日本.md "wikilink")[京都府](../Page/京都府.md "wikilink")[京都市](../Page/京都市.md "wikilink")，是[日本排球運動員](../Page/日本.md "wikilink")，身高184公分，體重68公斤，司職[副攻](../Page/副攻.md "wikilink")。

自1997年多次間斷地入選日本女排，並先後在2004年[雅典奥运会及](../Page/2004年夏季奧林匹克運動會.md "wikilink")2008年[北京奥运会參賽](../Page/2008年夏季奧林匹克運動會.md "wikilink")。

大村目前是[久光製藥Springs女排隊的旗下選手](../Page/久光製藥Springs.md "wikilink")。2008年4月，因在V-Legaue的生涯參賽場數超過240場以上，而獲得了V-Legaue長期活躍選手特別榮譽獎。

## 外部链接

  - [久光製藥Springs女排隊官方網站](http://www.springs.jp)
  - [大村加奈子個人資料](https://web.archive.org/web/20081205111419/http://www.springs.jp/team/players/index08.html)

[Category:日本女子排球运动员](../Category/日本女子排球运动员.md "wikilink")
[Category:京都府出身人物](../Category/京都府出身人物.md "wikilink")
[Category:2004年夏季奧林匹克運動會排球運動員](../Category/2004年夏季奧林匹克運動會排球運動員.md "wikilink")
[Category:2008年夏季奧林匹克運動會排球運動員](../Category/2008年夏季奧林匹克運動會排球運動員.md "wikilink")