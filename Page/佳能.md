[_Canon(1).jpg](https://zh.wikipedia.org/wiki/File:_Canon\(1\).jpg "fig:_Canon(1).jpg")
[Zhongshan006.jpg](https://zh.wikipedia.org/wiki/File:Zhongshan006.jpg "fig:Zhongshan006.jpg")中山市的打印机生产基地\]\]
**佳能株式會社**（），是一家生产影像、光学、医疗设备、半导体工业设备和办公自动化产品的[日本](../Page/日本.md "wikilink")[企業製造商](../Page/企業.md "wikilink")，产品包括[照相机](../Page/照相机.md "wikilink")、[摄像机](../Page/摄像机.md "wikilink")、[复印机](../Page/复印机.md "wikilink")、[传真机](../Page/传真机.md "wikilink")、[影像掃描器](../Page/影像掃描器.md "wikilink")、[打印机](../Page/打印机.md "wikilink")、[眼科及](../Page/眼科.md "wikilink")[X射线成像设备](../Page/X射线.md "wikilink")、电影摄影机和镜头、半导体[光刻机等](../Page/光刻机.md "wikilink")。

佳能的主要竞争对手包括[尼康](../Page/尼康.md "wikilink")、[索尼](../Page/索尼.md "wikilink")、[奥林巴斯](../Page/奥林巴斯.md "wikilink")、[柯尼卡美能达](../Page/柯尼卡美能达.md "wikilink")、[富士軟片](../Page/富士軟片.md "wikilink")、[徠卡](../Page/徠卡.md "wikilink")、[宾得](../Page/宾得.md "wikilink")、[爱普生](../Page/爱普生.md "wikilink")、[柯达](../Page/柯达.md "wikilink")、[惠普](../Page/惠普.md "wikilink")、[施乐等](../Page/施乐.md "wikilink")。

## 名稱

在1934年夏开发了他们的第一台相机“觀音”（即くゎんおんKwan-on，日語「觀音」的傳統讀法）。次年，该公司改名为“Canon”（「観音」かんおんkan-on
（「觀音」的現代讀法） =\> kanon =\> Canon，最終轉音為「佳能」）。香港曾用**錦囊**為名，現全球統一中文譯名為「佳能」。

## 歷史

佳能是由[吉田五郎和他的义弟](../Page/吉田五郎.md "wikilink")[内田三郎筹划](../Page/内田三郎.md "wikilink")，而由内田三郎的好友[御手洗毅筹资创建于](../Page/御手洗毅.md "wikilink")1933年，名为“-{精機光学研究所}-”（せいきこうがくけんきゅうじょ或Precision
Optical Instruments Laboratory）。其初衷只是为了研究高品质相机的发展。

该公司最初的相机都是仿制[莱卡螺旋口照相机的](../Page/徕卡相机.md "wikilink")。他们模仿得最好的一部是配合莱卡螺旋口镜头规格（L39）的机身，但添加了取景器和测距仪。佳能最初也没有自己的光学制造厂，所以早期佳能[镜头都是由](../Page/镜头.md "wikilink")[尼康制造](../Page/尼康.md "wikilink")。但佳能很快便开始自行制造[镜头](../Page/镜头.md "wikilink")，并使用“Serenar”的品牌。这种镜头直到现在仍受到大量莱卡旁轴相机使用者的喜爱。

2009年11月17日Canon以7.3億歐元（折合11億美元）現金價收購[荷蘭專業輸出設備商Oce](../Page/荷蘭.md "wikilink")，以擴張其[印表機業務版圖](../Page/印表機.md "wikilink")，並協助強化該公司在全球辦公室設備市場的領先地位。

2015年2月10日佳能以近28億美元（折合3300億日圓）236億瑞典克朗，收購瑞典網路監控攝影設備製造商「安迅士網路通訊公司」（Axis
Communications AB）

2016年3月17日佳能以6655億日圓(約459.94億港元)近60億美元收購日本最大的醫療裝置供應商[東芝醫療系統公司Toshiba](../Page/東芝.md "wikilink")
Medical Systems

[缩略图](https://zh.wikipedia.org/wiki/File:Lynn_Wu_and_Canon_Maxify_MB5070_20150421.png "fig:缩略图")代言廣告\]\]

## EOS系列

[EOS代表電子光學系統](../Page/EOS.md "wikilink")（**E**lectro **O**ptical
**S**ystem）的意思，同時亦蘊含希臘神話中[黎明女神的意思正代表創新科技邁向高峰的光明時期](../Page/厄俄斯.md "wikilink")。

1987年[日本褔島工廠開始生產EOS相機](../Page/日本.md "wikilink")，首部推出的EOS相機系列為1987年3月出產的EOS
650自動對焦單鏡反光相機；EOS 650當年全球首部配備全電子鏡頭接環系統的相機。

2000年推出配備自行研發的CMOS影像感應器的EOS
D30正式步入單鏡反光相機數碼化年代。由EOS系列相機於1987年推出至2015年11月產量已超過8千萬台。而EOS專用EF系列鏡頭則在2015年6月達到1億2千萬支的總產量。

## 参见

  - [佳能EOS系列](../Page/佳能EOS系列.md "wikilink")
  - [佳能FD接環鏡頭](../Page/佳能FD接環鏡頭.md "wikilink")
  - [佳能EF接環鏡頭](../Page/佳能EF接環鏡頭.md "wikilink")
  - [佳能EF-S接環鏡頭](../Page/佳能EF-S接環鏡頭.md "wikilink")
  - [Canon PowerShot](../Page/Canon_PowerShot.md "wikilink")
  - [Canon Digital IXUS](../Page/Canon_Digital_IXUS.md "wikilink")
  - [佳能產品列表](../Page/佳能產品列表.md "wikilink")

## 外部連結

  - [全球官方網站](http://www.canon.com/)
  - [佳能日本官方網站](https://web.archive.org/web/20050508235347/http://www.canon.co.jp/)
  - [佳能中国官方網站](http://www.canon.com.cn/)
  - [佳能香港官方網站](http://www.canon.com.hk/)
  - [佳能台灣官方網站](http://www.canon.com.tw/)

[佳能](../Category/佳能.md "wikilink")
[Category:日本摄影器材生产商](../Category/日本摄影器材生产商.md "wikilink")
[Category:日本電子公司](../Category/日本電子公司.md "wikilink")
[Category:跨国公司](../Category/跨国公司.md "wikilink")
[Category:香港交易所上市認股證](../Category/香港交易所上市認股證.md "wikilink")
[Category:日本品牌](../Category/日本品牌.md "wikilink")
[Category:大田區公司](../Category/大田區公司.md "wikilink")