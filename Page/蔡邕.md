[Cai_yong.jpg](https://zh.wikipedia.org/wiki/File:Cai_yong.jpg "fig:Cai_yong.jpg")》的蔡邕像\]\]
**蔡邕\[1\]**（），字**伯喈**，陈留圉（今[河南](../Page/河南.md "wikilink")[杞县南](../Page/杞县.md "wikilink")）人，中國[东汉末年名士](../Page/东汉.md "wikilink")。東漢著名才女[蔡琰](../Page/蔡琰.md "wikilink")（文姬）之父。

## 生平

蔡邕博学多才，好辞章、[数术](../Page/数术.md "wikilink")、天文，精通音律，尤擅[书法](../Page/书法.md "wikilink")，發明[飛白書](../Page/飛白書.md "wikilink")。[汉献帝时曾拜](../Page/汉献帝.md "wikilink")[左中郎将](../Page/左中郎将.md "wikilink")，故亦称“蔡中郎”。据史书可推测出来的有两女一子，其中只有長女[蔡琰有記載姓名](../Page/蔡琰.md "wikilink")，字昭姬，因犯諱（司马昭的諱字）《后汉书》记为[蔡文姬](../Page/蔡文姬.md "wikilink")，有名字记入史书，亦有文才。还有一女嫁给上党太守[羊衜](../Page/羊衜.md "wikilink")，史书上没留下名姓，但所生二子一女，大儿子早夭，女儿[羊徽瑜嫁给](../Page/羊徽瑜.md "wikilink")[司马师](../Page/司马师.md "wikilink")，史书上称为“景献羊皇后”、“弘训太后”\[2\]；小儿子[羊祜大大有名](../Page/羊祜.md "wikilink")；蔡邕之孙[蔡袭封](../Page/蔡袭.md "wikilink")[关内侯](../Page/关内侯.md "wikilink")，可见蔡邕有子，但名不详\[3\]。或然孙子和儿子根本没有\[4\]。

东汉[灵帝](../Page/汉灵帝.md "wikilink")[熹平四年](../Page/熹平.md "wikilink")，蔡邕等上奏请求正定[六经文字](../Page/六经.md "wikilink")，得到许可。蔡邕亲自用丹砂将经文书写于石碑，再命工匠镌刻，立于太学门外。这就是[熹平石经](../Page/熹平石经.md "wikilink")。

灵帝后期，蔡邕不容于内宠，乃亡命江海十二年\[5\]，隐居于[吴郡](../Page/吴郡.md "wikilink")（[治所在](../Page/治所.md "wikilink")[吴县](../Page/吴县.md "wikilink")，今[江苏省](../Page/江苏省.md "wikilink")[苏州市](../Page/苏州市.md "wikilink")）一段时间,之后回到中原\[6\]。有一次，他听到一块桐木在火中爆裂的声音，赶快把它拣出来做成琴，音色非常美妙，而琴尾已焦，这就是[焦尾琴](../Page/焦尾琴.md "wikilink")。

董卓專權，強令蔡邕出仕，最初蔡邕不肯，董卓威脅他說，「我力能族人」（[滿門抄斬](../Page/滿門抄斬.md "wikilink")），於是蔡邕趕忙坐[牛車](../Page/牛車.md "wikilink")，从家乡\[7\]前往洛陽報到，後拜左中郎將。封高陽鄉候，頗受重用，「卓重邕才學，厚相遇待，每集燕，輒令邕鼓琴贊事。」\[8\]期间大臣[王允经常和蔡邕商讨](../Page/王允.md "wikilink")，但王允常常辩论不过蔡邕，心里感到不满\[9\]。[董卓被杀](../Page/董卓.md "wikilink")，蔡邕伤感其遇，被[司徒](../Page/司徒.md "wikilink")[王允下狱](../Page/王允.md "wikilink")，公卿們都惋惜蔡邕的才學，勸王允赦免蔡邕，太尉[馬日磾](../Page/馬日磾.md "wikilink")（音密低）對王允說：「伯喈曠世逸才，多識漢事，當續成後史，為一代大典。且忠孝素著，而所坐無名，誅之無奈失人望乎？」王允曰：「昔武帝不殺司馬遷，使作謗書，流於後世。方今國祚中衰，戎馬在郊，不可令佞臣執筆在幼主左右，後令吾徒並受謗議。」邕遂死狱中\[10\]。

## 家世

### 本家

  - 祖父

<!-- end list -->

  -
    蔡攜，字叔業，有周之冑。汉顺帝时「以司空高第迁新蔡长」 。

<!-- end list -->

  - 父

<!-- end list -->

  -
    蔡棱，字伯直，东汉新蔡长

<!-- end list -->

  - 母

<!-- end list -->

  -
    袁氏，东汉司徒[袁滂妹](../Page/袁滂.md "wikilink")\[11\]、[袁渙姑母](../Page/袁渙.md "wikilink")\[12\]。

<!-- end list -->

  - 女

:\*[蔡文姬](../Page/蔡文姬.md "wikilink")

:\*蔡氏：上党太守[羊衜妻](../Page/羊衜.md "wikilink")，西晉時封濟陽[縣君](../Page/縣君.md "wikilink")

  - 孙 或是 从孙\[13\]

<!-- end list -->

  -
    蔡袭，[关内侯](../Page/关内侯.md "wikilink")
    《[蔡充别传](../Page/蔡充.md "wikilink")》载蔡充祖父蔡睦也是蔡邕之孙。但根据《晋书·[蔡豹传](../Page/蔡豹.md "wikilink")》，蔡睦应为蔡邕叔父蔡质孙，即蔡邕堂侄，《蔡充别传》误。\[14\]

<!-- end list -->

  - 外孫

<!-- end list -->

  -
    [羊承](../Page/羊承.md "wikilink")，早夭
    [羊祜](../Page/羊祜.md "wikilink")，西晉太傅
    [羊徽瑜](../Page/羊徽瑜.md "wikilink")，晉景帝[司馬師妻](../Page/司馬師.md "wikilink")

### 宗親

  - 六世祖

<!-- end list -->

  -
    蔡勳，字君嚴，好黃老之學，西漢平帝時為郿令。王莽篡漢初，不願出仕王莽所立新朝而遁山隱居。

<!-- end list -->

  - 叔父

<!-- end list -->

  -
    蔡質，字子文，靈帝時任衛尉，以罪下獄死。曾撰《[漢官典職儀式選用](../Page/漢官典職儀式選用.md "wikilink")》若幹篇，簡稱《漢官典儀》，雜記漢代官制及上書、谒見等儀式。

<!-- end list -->

  - 堂兄弟

<!-- end list -->

  -
    蔡谷，蔡邕在董卓麾下任官時向他商議辭官避居兗州一事。

<!-- end list -->

  - 堂侄

<!-- end list -->

  -
    蔡睦，蔡質之孙，遷徙宗族避居考城。

## 相關逸事

  - 蔡邕有很高的音樂水平，一些著名樂器都是出自他手，例如[焦尾琴](../Page/焦尾琴.md "wikilink")、[柯亭笛等](../Page/柯亭笛.md "wikilink")。
  - 曾于《[曹娥碑](../Page/曹娥碑.md "wikilink")》背面题八字隱語「黃絹幼婦，外孫虀臼。」\[15\]为[楊脩所道破](../Page/楊脩.md "wikilink")：「黃縜乃顏色之絲也。『色』傍加『絲』，是『-{絶}-（絕）』字。幼婦者，少女也。『女』傍『少』字，是『妙』字。外孫乃女之子也。『女』傍『子』字，是『好』字。虀臼乃[五辛之器也](../Page/五辛.md "wikilink")。『受』傍『辛』字，是『-{辤}-（辭）』字。總而言之，是『絕妙好辭』四字。」
  - [元代末年的著名](../Page/元代.md "wikilink")[南戲](../Page/南戲.md "wikilink")《[琵琶記](../Page/琵琶記.md "wikilink")》中，主人翁名字為蔡伯喈，陳留人，明顯指向蔡邕。

## 评价

  - [马日磾](../Page/马日磾.md "wikilink")：「伯喈旷世逸才，多识汉事，当续成后史，为一代大典。且忠孝素著，而所坐无名，诛之无乃失人望乎？」（《后汉书·卷六十下·蔡邕列传第五十下
    》）
  - [陆机](../Page/陆机.md "wikilink")：「彼洪川之方割，岂一等之所堙。故尼父之惠训，智必愚而後贤。谅知道之已妙，曷信道之未坚。忽宁子之保已，效苌淑之违天。冀澄河之远日，忘朝露之短年。」（《全晋文·卷九十九》）
  - [范晔](../Page/范晔.md "wikilink")：「意气之感，士所不能忘也。流极之运，有生所共深悲也。当伯喈抱钳扭，徙幽裔，仰日月而不见照烛，临风尘而不得经过，其意岂及语平日幸全人哉！及解刑衣，窜欧越，潜舟江壑，不知其远，捷步深林，尚苦不密，但愿北首旧丘，归骸先垄，又可得乎？[董卓一旦入朝](../Page/董卓.md "wikilink")，辟书先下，分明枉结，信宿三迁。匡导既申，狂僭屡革，资《同人》之先号，得北叟之后福。屡其庆者，夫岂无怀？君子断刑，尚或为之不举，况国宪仓卒，虑不先图，矜情变容，而罚同邪党？执政乃追怨子长谤书流后，放此为戮，未或闻之典刑。」
    「邕实慕静，心精辞绮。斥言金商，南徂北徒，籍梁怀董，名浇身毁。」（《后汉书·卷六十下·蔡邕列传第五十下 》）
  - [裴松之](../Page/裴松之.md "wikilink")：「蔡邕虽为卓所亲任，情必不党。宁不知卓之奸凶，为天下所毒，闻其死亡，理无叹惜。纵复令然，不应反言于王允之坐。斯殆谢承之妄记也。」
  - [张彦远](../Page/张彦远.md "wikilink")：「今分为三古以定贵贱，以汉、魏三国为上古，则[赵岐](../Page/赵岐.md "wikilink")、[刘亵](../Page/刘亵.md "wikilink")、蔡邕、[张衡](../Page/张衡.md "wikilink")、[曹髦](../Page/曹髦.md "wikilink")、[杨修](../Page/杨修.md "wikilink")、[桓范](../Page/桓范.md "wikilink")、[徐邈](../Page/徐邈.md "wikilink")、[曹不兴](../Page/曹不兴.md "wikilink")、[诸葛亮之流是也](../Page/诸葛亮.md "wikilink")。」
  - [姚铉](../Page/姚铉.md "wikilink")：「如[刘向](../Page/刘向.md "wikilink")、[司马迁](../Page/司马迁.md "wikilink")、[扬子云](../Page/扬子云.md "wikilink")，东京二班、崔、蔡之徒，皆命世之才，垂后代之法，张大德业，浩然无际。」（《水东日记·卷十二》）
  - [陈普](../Page/陈普.md "wikilink")：「不际明时论石渠，空将薄命仕鸿都。天公似把词人戏，父子然脐子坠胡。」「万岁黄金欲散时，柯亭风笛尚堪吹。一时谋卓人无数，不遣中郎一个知。」
  - [黄伯思](../Page/黄伯思.md "wikilink")《[东观余论](../Page/东观余论.md "wikilink")》：「想文饶（[刘宽](../Page/刘宽.md "wikilink")）之高风，玩中郎（蔡邕）之妙楷。」
  - [王应麟](../Page/王应麟.md "wikilink")：「蔡邕文，今存九十篇，而铭墓居其半。曰碑，曰铭，曰神诰，曰哀赞，其实一也。自云为《郭有道碑》，独无愧辞，则其他可知矣。其颂胡广、黄琼，几于老、韩同传，若继成汉史，岂有董、南之笔？」（《困学纪闻·卷十三·考史》）
  - [徐钧](../Page/徐钧.md "wikilink")：「琴遇知音始可调，卓非善听亦徒劳。早知应聘终罹祸，罪死何如节死高。」
  - [归有光](../Page/归有光.md "wikilink")：「贾生之通达，蔡邕之文学，张衡之精思，卓茂之循良，李膺之高节，黄宪之雅度，邓禹之功勋，有不可一二数者。」（震川先生制科文）
  - [李贽](../Page/李贽.md "wikilink")：「今人俱以蔡邕哭董卓为非，是论固正矣。然情有可原，事有足录，何也？士各为知己者死。设有人受恩桀纣，在他人固为桀纣，在此人则尧舜也，何可概论也？董卓诚为邕之知己，哭而报之，杀而残之，不为过也。犹胜今之势盛则借其余润，势衰则掉臂去之，甚至为操戈，为下石，无所不至者。毕竟蔡为君子，而此辈则真小人也。」（汇评三国演义）
  - [钟敬伯](../Page/钟敬伯.md "wikilink")：「士为知己者死，蔡邕哭卓，未为不是。第卓非可知己人，而邕翻成知己死，哀哉！」（汇评三国演义）
  - [王夫之](../Page/王夫之.md "wikilink")：「蔡邕意气之士也，始而以危言召祸，终而以党贼逢诛，皆意气之为也。何言之？曰：合刑赏之大权于一人者，天子也；兼进贤退不肖之道，以密赞于坐论者，大臣也；而羣工异是。奸人之在君侧，弗容不击矣。击之而吾言用，奸人退，贤者之道自伸焉。吾言不用，奸人且反噬于我，我躬不阅，而无容以累君子，使犹安焉，其犹有人乎君侧也。君子用而不任，弗容不为白其忠矣。白之而吾言用，君子进，奸人之势且沮焉。吾言不用，奸人不得以夺此与彼之名加之于我，而犹有所惮焉。邕苟疾夫张颢、伟璋、赵玹、盖升之为国蠹也，则专其力以击之可耳。若以郭禧、桥玄、刘宠之忠而劝之以延访也，则抑述其德以赞君之敬礼已耳。而一章之中，抑彼伸此，若将取在廷之多士而惟其所更张者。为国谋邪？为君子谋邪？则抑其一往之意气以排异己而伸交好者之言耳，庸有听之者哉！」（《[读通鉴论](../Page/读通鉴论.md "wikilink")》）「蔡邕之愚，不亡身而不止。」
  - [韩菼](../Page/韩菼.md "wikilink")：「栾布哭彭，朱诩葬董，伯喈一叹，未足为累。且十年亡命，三日尚书，朝廷伸国讨，国士感私恩，不妨并美也。」
  - [顾景星](../Page/顾景星.md "wikilink")：「始邕直言为阉待所中，囚徙朔方，赭衣抱拲，全室流离，可谓难矣。及宥还畏祸，亡命吴会，十有二年，无意功名，而且以弹琴著书终老牗下矣。使邕如梅福，长流江湖，岂不高哉？董卓擅权，辟署祭酒，补御史，迁尚书，不三日而周历三合。伊何为者？卓盖惜邕致天下豪杰，不加望外之荣，无以市德。故举之髡钳之余，爵之卿贰之上。且邕有何功？遂封侯食五百户、禄五十万？夫无故之利，圣人恶之。邕初议卓不可受尚父之称，而自出显位，何也？今夫捕鸟者，择其黠者以为囮，毇米为饲，滤流而饮，凡所以慰囮，靡弗至也。笯而出于野，置之丛薄之间，悲呼众鸟，至日暮，翾然投于罗者众矣。夫囮，未始乐为是也，而鸣致众鸟，谓非囮罪不可也，邕，卓之囮也，邕未始乐为是也，而厚禄高位，将以风天下为邕之类者，而邕甘心受之，谓非邕罪不可。桓帝召邕鼓琴，行次偃师，称疾而返。卓每宴集，邕辄赞事鼓琴。后遂为表荐卓，时卓已为太尉，封郿侯、进相国，废少帝、放太后，倾逼人主。邕谓宜益隆委。厚其爵赏，岂欲卓加九锡、封安汉而已哉！然则邕死，不亦宜乎！」
  - [沈铭彝](../Page/沈铭彝.md "wikilink")：「邕荐董卓表，极为推重，收邕时不闻以此罪之，当由王允未见此表耳，而流传至今，为后世增一口实，才之为累如此。」
  - [蔡东藩](../Page/蔡东藩.md "wikilink")：「蔡邕一文学士，所陈奏议，未始非守正之谈，然或嫌迂远，或涉虚浮，才有余而忠不足，吾于邕犹有余憾焉。但曹鸾一言而即遭掠死，国家无道之秋，固未足与陈谠论者。邕之所失，在可去而不去耳，文字之间，固无容苛求也。」

## 艺术形象

### 登場作品

  - [蒼天航路](../Page/蒼天航路.md "wikilink")（[王欣太](../Page/王欣太.md "wikilink")）
  - [火鳳燎原](../Page/火鳳燎原.md "wikilink")（[陳某](../Page/陳某.md "wikilink")）

### 影視作品

  - 中国中央电视台电视剧《[武圣关公](../Page/武圣关公.md "wikilink")》（2004年）：[段军](../Page/段军.md "wikilink")
  - 电视剧《[曹操](../Page/曹操_\(电视剧\).md "wikilink")》（2014年）：[孙万清](../Page/孙万清.md "wikilink")

## 参见

  - [曹娥碑](../Page/曹娥碑.md "wikilink")
  - [琵琶记](../Page/琵琶记.md "wikilink")

## 注釋

## 參考文獻

  - 《[后汉书](../Page/后汉书.md "wikilink")·[蔡邕列传](../Page/s:後漢書/卷60下.md "wikilink")》

[Category:蔡姓](../Category/蔡姓.md "wikilink")
[Y邕](../Category/蔡姓.md "wikilink")
[C](../Category/东汉官员.md "wikilink")
[C](../Category/漢朝書法家.md "wikilink")
[C](../Category/漢朝作家.md "wikilink")
[C](../Category/杞县人.md "wikilink")

1.
2.  《[晋书](../Page/晋书.md "wikilink")·[后妃传](../Page/s:晉書/卷031.md "wikilink")》
3.  《[晋书](../Page/晋书.md "wikilink")·[羊祜传](../Page/s:晉書/卷034.md "wikilink")》
4.  《後漢書補注續》「所謂舅子者，非必即邕之孫．雖従孫亦得蒙此稱也」
5.  蔡邕《让尚书乞在闲冗表》:「臣流离藏窜十有二年」
6.  中平年间蔡邕曾经向大将军何进推荐边让，并且为在此期間大量去世的名人撰写碑文（《故太尉桥公庙碑》，《陈太丘碑》，《汉太尉杨公碑》，《司徒袁公夫人马氏碑》，《太尉刘宽碑》等）。《后汉书·边让传》:「议郎(蔡邕当时可能不是议郎)蔡邕深敬之(边让)，以为让（边让）宜处高任，乃荐于何进。」
7.  《后汉书·蔡邕传》:「切敕州郡举邕诣府」
8.  《後漢書》
9.  《太平广记》卷164引《商芸小说》:初司徒王允，数与邕会议，允词常屈，由是衔邕。
10. 《後漢書》
11. 《[博物志](../Page/博物志_\(張華\).md "wikilink")·卷之六》蔡伯喈母，袁公妹、曜卿姑也。
12. 《[太平御览](../Page/太平御览.md "wikilink")·卷五百一十三》◎宗亲部三《[先贤行状](../Page/先贤行状.md "wikilink")》曰：蔡伯喈母，袁曜卿之姑也。
13. 侯康《後漢書補注續》
14. 《世說·輕詆篇》注引《蔡充別傳》曰：充祖睦，蔡邕孫也。
15. [曹娥碑文](../Page/s:曹娥碑.md "wikilink")。