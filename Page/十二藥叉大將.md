[Giappone,_periodo_kamakura,_due_guardiani_del_buddha_yakushi_nyorai,_1185-1333,_01.jpg](https://zh.wikipedia.org/wiki/File:Giappone,_periodo_kamakura,_due_guardiani_del_buddha_yakushi_nyorai,_1185-1333,_01.jpg "fig:Giappone,_periodo_kamakura,_due_guardiani_del_buddha_yakushi_nyorai,_1185-1333,_01.jpg")

**十二藥叉大將**，是[佛教的](../Page/佛教.md "wikilink")[護法神祇](../Page/護法神.md "wikilink")，主要記載在《[藥師經](../Page/藥師經.md "wikilink")》，是守護修持[藥師佛法門眾生的十二位](../Page/藥師佛.md "wikilink")[藥叉鬼神](../Page/藥叉.md "wikilink")。在印度神話《阿婆縛紗》中已經登場，[婆羅門教](../Page/婆羅門教.md "wikilink")（印度教）認為祂們的坐騎就是十二生肖的來源。

## 簡介

十二大將也稱作「十二大藥叉將」，俗稱「十二神將」，他們是[佛教的](../Page/佛教.md "wikilink")[護法神祇](../Page/護法神.md "wikilink")，主要是在《[藥師經](../Page/藥師經.md "wikilink")》裡面記載，守護修持[藥師佛法門眾生的十二大將](../Page/藥師佛.md "wikilink")。每位大將各有七千位藥叉作為他們的眷屬，全部合起來共有八萬四千位藥叉[護法](../Page/護法.md "wikilink")，在各地保護修持[佛法與修持藥師佛法門的眾生](../Page/佛法.md "wikilink")。

大乘佛教結合印度教傳說和《大集經》記載，認為十二大將是佛菩薩的化身，祂們的坐騎神獸也是其化身，而由於十二神獸和生肖的對應關係，而將十二大將信仰與中國文化結合而付予新的詮釋說：「十二大將對應[十二位佛、菩薩與十二生肖、時辰](../Page/生肖#本命佛.md "wikilink")。」在[日本佛教中](../Page/日本佛教.md "wikilink")，受[本地垂跡的影響](../Page/本地垂跡.md "wikilink")，十二大將也對應了十二尊“[本地佛](../Page/本地佛.md "wikilink")”，認為是祂們的[權現](../Page/權現.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>神將名<br />
（括號內爲意譯）</p></th>
<th><p>梵音</p></th>
<th><p>十二支</p></th>
<th><p>生肖<br />
（括號內爲中國對應生肖）</p></th>
<th><p>日本佛教<a href="../Page/本地佛.md" title="wikilink">本地佛</a></p></th>
<th><p>漢傳佛教本尊<br />
（本命佛）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>（鱷魚）</p></td>
<td><p>Kuṁbhīra</p></td>
<td><p>亥神</p></td>
<td><p>獅（虎）</p></td>
<td><p><a href="../Page/彌勒菩薩.md" title="wikilink">彌勒菩薩</a></p></td>
<td><p><a href="../Page/虛空藏菩薩.md" title="wikilink">虛空藏菩薩</a></p></td>
</tr>
<tr class="even">
<td><p>伐折羅（<a href="../Page/金剛.md" title="wikilink">金剛</a>）</p></td>
<td><p>Vajra</p></td>
<td><p>戌神</p></td>
<td><p>兔</p></td>
<td><p><a href="../Page/大勢至菩薩.md" title="wikilink">大勢至菩薩</a></p></td>
<td><p><a href="../Page/曼殊室利菩薩.md" title="wikilink">曼殊室利菩薩</a></p></td>
</tr>
<tr class="odd">
<td><p>迷企羅</p></td>
<td><p>Mekhila</p></td>
<td><p>酉神</p></td>
<td><p><a href="../Page/那伽.md" title="wikilink">那伽</a>（龍）</p></td>
<td><p><a href="../Page/阿彌陀佛.md" title="wikilink">阿彌陀佛</a></p></td>
<td><p><a href="../Page/普賢菩薩.md" title="wikilink">普賢菩薩</a></p></td>
</tr>
<tr class="even">
<td><p>安底羅</p></td>
<td><p>Āṇḍīra</p></td>
<td><p>申神</p></td>
<td><p><a href="../Page/摩呼洛伽.md" title="wikilink">摩呼洛伽</a>（蛇）</p></td>
<td><p><a href="../Page/觀世音菩薩.md" title="wikilink">觀世音菩薩</a></p></td>
<td><p><a href="../Page/普賢菩薩.md" title="wikilink">普賢菩薩</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Anila</p></td>
<td><p>未神</p></td>
<td><p>馬</p></td>
<td><p><a href="../Page/如意輪菩薩.md" title="wikilink">如意輪菩薩</a></p></td>
<td><p><a href="../Page/大勢至菩薩.md" title="wikilink">大勢至菩薩</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Śāṇḍilya</p></td>
<td><p>午神</p></td>
<td><p>羊</p></td>
<td><p><a href="../Page/虛空藏菩薩.md" title="wikilink">虛空藏菩薩</a></p></td>
<td><p><a href="../Page/大日如來.md" title="wikilink">大日如來</a></p></td>
</tr>
<tr class="odd">
<td><p>因達羅<br />
</p></td>
<td><p>Indra</p></td>
<td><p>巳神</p></td>
<td><p>猴</p></td>
<td><p><a href="../Page/地藏菩薩.md" title="wikilink">地藏菩薩</a></p></td>
<td><p><a href="../Page/大日如來.md" title="wikilink">大日如來</a></p></td>
</tr>
<tr class="even">
<td><p>波夷羅</p></td>
<td><p>Pajra</p></td>
<td><p>辰神</p></td>
<td><p>金翅鳥（雞）</p></td>
<td><p><a href="../Page/曼殊室利菩薩.md" title="wikilink">曼殊室利菩薩</a></p></td>
<td><p><a href="../Page/不動明王.md" title="wikilink">不動明王</a></p></td>
</tr>
<tr class="odd">
<td><p>摩虎羅（摩呼洛伽，蟒蛇）</p></td>
<td><p>Mahoraga</p></td>
<td><p>卯神</p></td>
<td><p>犬</p></td>
<td><p><a href="../Page/大威德明王.md" title="wikilink">大威德明王</a></p></td>
<td><p><a href="../Page/阿彌陀佛.md" title="wikilink">阿彌陀佛</a></p></td>
</tr>
<tr class="even">
<td><p>眞達羅（<a href="../Page/緊那羅.md" title="wikilink">緊那羅</a>）</p></td>
<td><p>Kiṃnara</p></td>
<td><p>寅神</p></td>
<td><p>豕</p></td>
<td><p><a href="../Page/普賢菩薩.md" title="wikilink">普賢菩薩</a></p></td>
<td><p><a href="../Page/阿彌陀佛.md" title="wikilink">阿彌陀佛</a></p></td>
</tr>
<tr class="odd">
<td><p>招杜羅</p></td>
<td><p>Catura</p></td>
<td><p>丑神</p></td>
<td><p>鼠</p></td>
<td><p><a href="../Page/大日如來.md" title="wikilink">大日如來</a></p></td>
<td><p><a href="../Page/觀世音菩薩.md" title="wikilink">觀世音菩薩</a></p></td>
</tr>
<tr class="even">
<td><p>毘羯羅</p></td>
<td><p>Vikarāla</p></td>
<td><p>子神</p></td>
<td><p>牛</p></td>
<td><p><a href="../Page/釋迦牟尼如來.md" title="wikilink">釋迦牟尼如來</a></p></td>
<td><p><a href="../Page/虛空藏菩薩.md" title="wikilink">虛空藏菩薩</a></p></td>
</tr>
</tbody>
</table>

## 相關咒語

在《藥師經》中，十二神將誓願要護持藥師經和修持此經的佛弟子，并宣說了《十二神將饒益有情結願神咒》，神咒的後半段就是《藥師灌頂真言》，前半段意爲歸命[三寶及歸命十二藥叉大將的聖號](../Page/三寶.md "wikilink")，因為十二藥叉大將是十二尊佛菩薩的化身，在娑婆世界與眾多眷屬護持修藥師法門的佛弟子，所以其聖號等於是秘密真言，有很大的威德力。按經典說法，若有受持此咒，能滅身中過去生死一切重罪，不復徑歷三途遠離九橫，超越眾苦，十方世界隨處安樂自在無礙，諸有願求，悉令滿足。

《藥師經》中十二藥叉大將還言：「以五色縷，結我名字，得如願已，然後解結」，所以修藥師法門祈願結[五色線的時候](../Page/五色線.md "wikilink")，如果能夠念上面這個《十二神將饒益有情結願神咒》，那就更符合經義了。

## 參考資料

  - [藥師經的相關電子書（包括各類註解及白話譯文）](http://www.book853.com/list.aspx?cid=83)
  - [藥師琉璃光如來](https://web.archive.org/web/20110625145952/http://bgvpr.org/)

[Category:夜叉](../Category/夜叉.md "wikilink")
[Category:護法神](../Category/護法神.md "wikilink")
[Category:佛教神祇](../Category/佛教神祇.md "wikilink")
[Category:佛教名數12](../Category/佛教名數12.md "wikilink")
[Category:眾神](../Category/眾神.md "wikilink")