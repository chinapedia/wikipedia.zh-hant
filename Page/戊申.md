**戊申**为[干支之一](../Page/干支.md "wikilink")，顺序为第45个。前一位是[丁未](../Page/丁未.md "wikilink")，后一位是[己酉](../Page/己酉.md "wikilink")。論[陰陽](../Page/陰陽.md "wikilink")[五行](../Page/五行.md "wikilink")，[天干之戊屬陽之土](../Page/天干.md "wikilink")，[地支之申屬陽之金](../Page/地支.md "wikilink")，是土生金相生。

## 戊申年

中国传统[纪年](../Page/纪年.md "wikilink")[农历的](../Page/农历.md "wikilink")[干支纪年中一个循环的第](../Page/干支纪年.md "wikilink")45年称“**戊申年**”。以下各个[公元](../Page/公元.md "wikilink")[年份](../Page/年.md "wikilink")，年份數除以60餘8，或年份數減3，除以10的餘數是5，除以12的餘數是9，自當年[立春起至次年立春止的歲次内均为](../Page/立春.md "wikilink")“戊申年”：

<table>
<caption><strong>戊申年</strong></caption>
<thead>
<tr class="header">
<th><p><a href="../Page/第1千年.md" title="wikilink">第1千年</a></p></th>
<th><p><a href="../Page/第2千年.md" title="wikilink">第2千年</a></p></th>
<th><p><a href="../Page/第3千年.md" title="wikilink">第3千年</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/48年.md" title="wikilink">48年</a></li>
<li><a href="../Page/108年.md" title="wikilink">108年</a></li>
<li><a href="../Page/168年.md" title="wikilink">168年</a></li>
<li><a href="../Page/228年.md" title="wikilink">228年</a></li>
<li><a href="../Page/288年.md" title="wikilink">288年</a></li>
<li><a href="../Page/348年.md" title="wikilink">348年</a></li>
<li><a href="../Page/408年.md" title="wikilink">408年</a></li>
<li><a href="../Page/468年.md" title="wikilink">468年</a></li>
<li><a href="../Page/528年.md" title="wikilink">528年</a></li>
<li><a href="../Page/588年.md" title="wikilink">588年</a></li>
<li><a href="../Page/648年.md" title="wikilink">648年</a></li>
<li><a href="../Page/708年.md" title="wikilink">708年</a></li>
<li><a href="../Page/768年.md" title="wikilink">768年</a></li>
<li><a href="../Page/828年.md" title="wikilink">828年</a></li>
<li><a href="../Page/888年.md" title="wikilink">888年</a></li>
<li><a href="../Page/948年.md" title="wikilink">948年</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/1008年.md" title="wikilink">1008年</a></li>
<li><a href="../Page/1068年.md" title="wikilink">1068年</a></li>
<li><a href="../Page/1128年.md" title="wikilink">1128年</a></li>
<li><a href="../Page/1188年.md" title="wikilink">1188年</a></li>
<li><a href="../Page/1248年.md" title="wikilink">1248年</a></li>
<li><a href="../Page/1308年.md" title="wikilink">1308年</a></li>
<li><a href="../Page/1368年.md" title="wikilink">1368年</a></li>
<li><a href="../Page/1428年.md" title="wikilink">1428年</a></li>
<li><a href="../Page/1488年.md" title="wikilink">1488年</a></li>
<li><a href="../Page/1548年.md" title="wikilink">1548年</a></li>
<li><a href="../Page/1608年.md" title="wikilink">1608年</a></li>
<li><a href="../Page/1668年.md" title="wikilink">1668年</a></li>
<li><a href="../Page/1728年.md" title="wikilink">1728年</a></li>
<li><a href="../Page/1788年.md" title="wikilink">1788年</a></li>
<li><a href="../Page/1848年.md" title="wikilink">1848年</a></li>
<li><a href="../Page/1908年.md" title="wikilink">1908年</a></li>
<li><a href="../Page/1968年.md" title="wikilink">1968年</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/2028年.md" title="wikilink">2028年</a></li>
<li><a href="../Page/2088年.md" title="wikilink">2088年</a></li>
<li><a href="../Page/2148年.md" title="wikilink">2148年</a></li>
<li><a href="../Page/2208年.md" title="wikilink">2208年</a></li>
<li><a href="../Page/2268年.md" title="wikilink">2268年</a></li>
<li><a href="../Page/2328年.md" title="wikilink">2328年</a></li>
<li><a href="../Page/2388年.md" title="wikilink">2388年</a></li>
<li><a href="../Page/2448年.md" title="wikilink">2448年</a></li>
<li><a href="../Page/2508年.md" title="wikilink">2508年</a></li>
<li><a href="../Page/2568年.md" title="wikilink">2568年</a></li>
<li><a href="../Page/2628年.md" title="wikilink">2628年</a></li>
<li><a href="../Page/2688年.md" title="wikilink">2688年</a></li>
<li><a href="../Page/2748年.md" title="wikilink">2748年</a></li>
<li><a href="../Page/2808年.md" title="wikilink">2808年</a></li>
<li><a href="../Page/2868年.md" title="wikilink">2868年</a></li>
<li><a href="../Page/2928年.md" title="wikilink">2928年</a></li>
<li><a href="../Page/2988年.md" title="wikilink">2988年</a></li>
</ul></td>
</tr>
</tbody>
</table>

## 戊申月

天干丁年和壬年，[立秋到](../Page/立秋.md "wikilink")[白露的時間段](../Page/白露.md "wikilink")，就是**戊申月**：

  - ……
  - [1977年](../Page/1977年.md "wikilink")8月立秋到9月白露
  - [1982年](../Page/1982年.md "wikilink")8月立秋到9月白露
  - [1987年](../Page/1987年.md "wikilink")8月立秋到9月白露
  - [1992年](../Page/1992年.md "wikilink")8月立秋到9月白露
  - [1997年](../Page/1997年.md "wikilink")8月立秋到9月白露
  - [2002年](../Page/2002年.md "wikilink")8月立秋到9月白露
  - [2007年](../Page/2007年.md "wikilink")8月立秋到9月白露
  - ……

## 戊申日

## 戊申時

天干丁日和壬日，[北京时间](../Page/北京时间.md "wikilink")（[UTC](../Page/协调世界时.md "wikilink")+8）15時到17時，就是**戊申時**。

## 參考資料

1.
[Category:干支](../Category/干支.md "wikilink")
[\*](../Category/戊申年.md "wikilink")