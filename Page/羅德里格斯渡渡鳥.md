**羅德里格斯渡渡鳥**（[学名](../Page/学名.md "wikilink")：**）是[鸽形目](../Page/鸽形目.md "wikilink")[鸠鸽科的一種已](../Page/鸠鸽科.md "wikilink")[滅絕](../Page/滅絕.md "wikilink")[鳥類](../Page/鳥類.md "wikilink")，也是**罗岛渡渡鸟属**的唯一[物种](../Page/物种.md "wikilink")，為[印度洋](../Page/印度洋.md "wikilink")[馬達加斯加東邊](../Page/馬達加斯加.md "wikilink")[毛里求斯](../Page/毛里求斯.md "wikilink")[羅德里格斯岛上的](../Page/羅德里格斯岛.md "wikilink")[特有種](../Page/特有種.md "wikilink")，与同样生存在毛里求斯另一种鸟类
[渡渡鳥](../Page/渡渡鳥.md "wikilink")（）是近亲，現存親緣關係最近的近親則為[綠簑鳩](../Page/綠簑鳩.md "wikilink")。

羅德里格斯渡渡鳥體型可比擬[天鵝](../Page/天鵝.md "wikilink")，也有明顯的[兩性異形](../Page/兩性異形.md "wikilink")。雄鳥體長可達
，體重可達 ；而雌鳥體長則僅達 ，體重則只有
。[羽毛為棕色或灰色](../Page/羽毛.md "wikilink")，雌鳥毛色較雄鳥淺。雌鳥一次產下一顆卵，由雌鳥和雄鳥共同孵育。以[水果和](../Page/水果.md "wikilink")[種子為食](../Page/種子.md "wikilink")。

## 形态

[缩略图](https://zh.wikipedia.org/wiki/File:Leguat1891solitaire.jpg "fig:缩略图")
本种樣子像渡渡鳥，跟渡渡鳥一樣不會飛行。

## 灭绝

本种也遭到人类带来的[入侵物种](../Page/入侵物种.md "wikilink")——[家猫的大量捕杀](../Page/家猫.md "wikilink")，直到1761年间，本种已经非常罕见，最后在1778年彻底灭绝。

## 分类

本属属于[渡渡鸟亚科](../Page/渡渡鸟亚科.md "wikilink")、[渡渡鸟族](../Page/渡渡鸟族.md "wikilink")，与渡渡鳥互为[姐妹群](../Page/姐妹群.md "wikilink")，而[蓑鸠属是](../Page/蓑鸠属.md "wikilink")[现生种中与它们最亲近的类群](../Page/现生种.md "wikilink")。

本属与其它鸠鸽科的关系如下：

## 参考文献

## 參見

  - [渡渡鳥](../Page/渡渡鳥.md "wikilink")
  - [留尼旺渡渡鳥](../Page/留尼旺渡渡鳥.md "wikilink")
  - [已滅絕動物列表](../Page/已滅絕動物列表.md "wikilink")

## 外部链接

  -
[PS](../Category/IUCN絕滅物種.md "wikilink")
[PS](../Category/鸠鸽科.md "wikilink")
[PS](../Category/已滅絕鳥類.md "wikilink")
[PS](../Category/不会飞的鸟.md "wikilink")