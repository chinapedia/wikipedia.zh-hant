是[萬代發售的以](../Page/萬代.md "wikilink")[SD造形機動戰士的](../Page/SD.md "wikilink")[戰略遊戲系列](../Page/戰略遊戲.md "wikilink")，以[超級任天堂晚期推出的雙匣帶遊戲](../Page/超級任天堂.md "wikilink")「SD高達世紀」為基礎開發的遊戲系列。

## 系列的特徵

最早期版本為[超級任天堂遊戲](../Page/超級任天堂.md "wikilink")「SD高達世紀」，當中透過俘虜、開發、合成設計圖等方式收集機體的玩法，亦為往後所有系列所繼承（早期G世紀中，敵我陣營都擁有被四座炮台包圍的「基地」，基地被敵機入侵便等同Game
Over、每部機體只有四招等設計，即使G世紀續作已陸續廢除，亦反映出G世紀與SD高達世紀的繼承關係。）

而在掌上主機系列可以養成駕駛員利用補給點來補充或強化自軍的機體，另外取自[GUNDAM系列作品](../Page/GUNDAM系列作品.md "wikilink")（包括MSV，小說還有延伸遊戲）著名台詞的ID
command也是一大賣點。

## 系列作

### 家用主機系列

#### SD Gundam GENERATION

1996年發售的[超級任天堂遊戲](../Page/超級任天堂.md "wikilink")，是SD鋼彈世紀首部作品。是超任晚期推出雙匣帶遊戲系列之一，定下以收集機體為遊戲特色的基礙。

##### 登場作品

  - [機動戰士GUNDAM](../Page/機動戰士GUNDAM.md "wikilink")
  - [機動戰士Z GUNDAM](../Page/機動戰士Z_GUNDAM.md "wikilink")
  - [機動戰士GUNDAM ZZ](../Page/機動戰士GUNDAM_ZZ.md "wikilink")
  - [機動戰士GUNDAM 逆襲的夏亞](../Page/機動戰士GUNDAM_逆襲的夏亞.md "wikilink")
  - [機動戰士GUNDAM F91](../Page/機動戰士GUNDAM_F91.md "wikilink")
  - [機動戰士V GUNDAM](../Page/機動戰士V_GUNDAM.md "wikilink")
  - [機動武鬥傳G GUNDAM](../Page/機動武鬥傳G_GUNDAM.md "wikilink")

#### SD Gundam GGENERATION

1998年8月6日發售的[PS遊戲是SD鋼彈世紀的續作](../Page/PlayStation.md "wikilink")，從初代[GUNDAM到](../Page/GUNDAM.md "wikilink")[機動戰士GUNDAM
逆襲的夏亞有](../Page/機動戰士GUNDAM_逆襲的夏亞.md "wikilink")30幾個關卡和包括[F-91至](../Page/機動戰士GUNDAM_F91.md "wikilink")[鋼彈X的多架鋼彈系列主人公座機登場](../Page/鋼彈X.md "wikilink")。

##### 登場作品

<table width="70%">

<tr>

<td valign=top width="35%">

  - [機動戰士GUNDAM](../Page/機動戰士GUNDAM.md "wikilink")
  - [機動戰士GUNDAM 第08MS小隊](../Page/機動戰士GUNDAM_第08MS小隊.md "wikilink")
  - [機動戰士GUNDAM外傳系列](../Page/機動戰士GUNDAM_外傳_THE_BLUE_DESTINY.md "wikilink")
  - [機動戰士GUNDAM0080 口袋裡的戰爭](../Page/機動戰士GUNDAM0080_口袋裡的戰爭.md "wikilink")
  - [機動戰士GUNDAM0083 STARDUST
    MEMORY](../Page/機動戰士GUNDAM0083_STARDUST_MEMORY.md "wikilink")
  - [機動戰士Z GUNDAM](../Page/機動戰士Z_GUNDAM.md "wikilink")
  - [機動戰士GUNDAM ZZ](../Page/機動戰士GUNDAM_ZZ.md "wikilink")
  - [機動戰士GUNDAM 逆襲的夏亞](../Page/機動戰士GUNDAM_逆襲的夏亞.md "wikilink")
  - [機動戰士GUNDAM F91](../Page/機動戰士GUNDAM_F91.md "wikilink")
  - [機動戰士V GUNDAM](../Page/機動戰士V_GUNDAM.md "wikilink")
  - [機動武鬥傳G GUNDAM](../Page/機動武鬥傳G_GUNDAM.md "wikilink")
  - [新機動戰記GUNDAM W](../Page/新機動戰記GUNDAM_W.md "wikilink")
  - [機動新世紀GUNDAM X](../Page/機動新世紀GUNDAM_X.md "wikilink")

</td>

<td valign=top width="35%">

  - [MOBILE SUIT VARIATION:
    MSV](../Page/MOBILE_SUIT_VARIATION:_MSV.md "wikilink")
  - MS-X
  - Ζ-MSV
  - ΖΖ-MSV
  - CCA-MSV
  - [機動戰士GUNDAM F90](../Page/機動戰士GUNDAM_F90.md "wikilink")
  - [新機動戰記GUNDAM W Endless
    Waltz](../Page/新機動戰記GUNDAM_W_Endless_Waltz.md "wikilink")
  - [MS GENERATION](../Page/機動戰士GUNDAM_MS_GENERATION.md "wikilink")
  - [機動戰士GUNDAM CROSS
    DIMENSION](../Page/機動戰士GUNDAM_CROSS_DIMENSION_0079.md "wikilink")
  - [機動戰士GUNDAM 基連之野望](../Page/機動戰士GUNDAM_基連之野望.md "wikilink")
  - [GUNDAM THE BATTLE
    MASTER](../Page/GUNDAM_THE_BATTLE_MASTER.md "wikilink")

</td>

</tr>

</table>

#### SD Gundam GGENERATION-ZERO

1999年8月12日發售的PS遊戲，除了前作的劇本亦加入[V
GUNDAM的劇本有](../Page/機動戰士V_GUNDAM.md "wikilink")50幾個關卡，此外在其它模式收錄「[平成三部曲](../Page/平成三部曲.md "wikilink")」作品[G
GUNDAM](../Page/G_GUNDAM.md "wikilink")、[GUNDAM
W和](../Page/GUNDAM_W.md "wikilink")[GUNDAM
X登場](../Page/GUNDAM_X.md "wikilink")。由於本作以[宇宙世紀為主軸](../Page/宇宙世紀.md "wikilink")，[平成三部曲只收錄故事中早期機體](../Page/平成三部曲.md "wikilink")。例如-{zh-hans:神高达;zh-hant:神威鋼彈;zh-hk:神高達}-，另亦收錄當時的新作品[∀GUNDAM](../Page/∀GUNDAM.md "wikilink")。

##### 登場作品

<table width="70%">

<tr>

<td valign=top width="35%">

  - [機動戰士GUNDAM](../Page/機動戰士GUNDAM.md "wikilink")
      - 機動戰士GUNDAM I
      - 機動戰士GUNDAM II 哀戰士編
      - 機動戰士GUNDAM III 相逢宇宙編
  - [機動戰士Z GUNDAM](../Page/機動戰士Z_GUNDAM.md "wikilink")
  - [機動戰士GUNDAM ZZ](../Page/機動戰士GUNDAM_ZZ.md "wikilink")
  - [機動戰士V GUNDAM](../Page/機動戰士V_GUNDAM.md "wikilink")
  - [機動戰士GUNDAM 逆襲的夏亞](../Page/機動戰士GUNDAM_逆襲的夏亞.md "wikilink")
  - [機動戰士GUNDAM F91](../Page/機動戰士GUNDAM_F91.md "wikilink")
  - [機動戰士GUNDAM0080 口袋裡的戰爭](../Page/機動戰士GUNDAM0080_口袋裡的戰爭.md "wikilink")
  - [機動戰士GUNDAM0083 STARDUST
    MEMORY](../Page/機動戰士GUNDAM0083_STARDUST_MEMORY.md "wikilink")
  - [機動戰士GUNDAM 第08MS小隊](../Page/機動戰士GUNDAM_第08MS小隊.md "wikilink")
  - [機動武鬥傳G GUNDAM](../Page/機動武鬥傳G_GUNDAM.md "wikilink")
  - [新機動戰記GUNDAM W](../Page/新機動戰記GUNDAM_W.md "wikilink")
  - [機動新世紀GUNDAM X](../Page/機動新世紀GUNDAM_X.md "wikilink")
  - [MOBILE SUIT VARIATION:
    MSV](../Page/MOBILE_SUIT_VARIATION:_MSV.md "wikilink")

</td>

<td valign=top width="35%">

  - MS-X
  - Ζ-MSV
  - ΖΖ-MSV
  - CCA-MSV
  - [機動戰士GUNDAM F90](../Page/機動戰士GUNDAM_F90.md "wikilink")
  - [機動戰士GUNDAM 影子方程式91](../Page/機動戰士GUNDAM_影子方程式91.md "wikilink")
  - [機動戰士GUNDAM Double
    Fake](../Page/Double_Fake_Under_the_Gundam.md "wikilink")
  - [GUNDAM SENTINEL](../Page/GUNDAM_SENTINEL.md "wikilink")
  - [MS GENERATION](../Page/機動戰士GUNDAM_MS_GENERATION.md "wikilink")
  - [模型狂四郎](../Page/模型狂四郎.md "wikilink")
  - [新機動戰記GUNDAM W G-UNIT](../Page/新機動戰記GUNDAM_W_G-UNIT.md "wikilink")
  - [∀GUNDAM](../Page/∀GUNDAM.md "wikilink")
  - [機動戰士GUNDAM CROSS
    DIMENSION](../Page/機動戰士GUNDAM_CROSS_DIMENSION_0079.md "wikilink")
  - [機動戰士GUNDAM外傳系列](../Page/機動戰士GUNDAM_外傳_THE_BLUE_DESTINY.md "wikilink")
  - [GUNDAM THE BATTLE
    MASTER](../Page/GUNDAM_THE_BATTLE_MASTER.md "wikilink")
  - [機動戰士GUNDAM 基連之野望](../Page/機動戰士GUNDAM_基連之野望.md "wikilink")

</td>

</tr>

</table>

#### SD Gundam GGENERATION-F

2000年8月3日發售。
加入了能自選劇本系統，收錄的劇本也由前作的只有初代至V高達增加到X高達（上集收錄的G、W、X不對應玩家進度），而且更收綠了DC所推出的另一個一年戰爭外傳（之前一直只收錄BD）、W的[無盡的華爾茲](../Page/新機動戰記GUNDAM_W_無盡的華爾茲.md "wikilink")、G-Unit等等。
本作加入了兩個新系統：

  - 改造：
    即機體改造系統，令機體在升至ace級後在開發外的另一強化方式，但在改造過後的機體無法再開發，等級會換成改1-99。
  - 黑歷史code
    對應ggen卡牌上的密碼，只要有足夠的科技（炮台），便可以使用卡片上的密碼生產未登陸生產list的機體，更包括該作最強的萬能key機（能與99%機體設計其成其他99%機體）鳳凰高達。

而在收錄機體方面，除了由初代至X，包括前幾作未有登場過的爆烈空中霸王高達等機體外，更收錄了隱藏關卡∀中的接近全部機體，包括未能在隱藏關卡登場的TURN-X等。另外，可以利用前作光碟放映前作的戰鬥電影。收錄部分SD高達系列遊戲舊作的原創機體，如旋風高達、骰子高達等……

光碟數是前作的2倍（前作ZERO 2片光碟），但4片中其中一片是對應ws版的光碟。

##### 登場作品

<table width="70%">

<tr>

<td valign=top width="35%">

  - [機動戰士GUNDAM](../Page/機動戰士GUNDAM.md "wikilink")
      - 機動戰士GUNDAM I
      - 機動戰士GUNDAM II 哀戰士編
      - 機動戰士GUNDAM III 相逢宇宙編
  - [機動戰士GUNDAM 第08MS小隊](../Page/機動戰士GUNDAM_第08MS小隊.md "wikilink")
  - [機動戰士GUNDAM外傳系列](../Page/機動戰士GUNDAM_外傳_THE_BLUE_DESTINY.md "wikilink")
  - [機動戰士GUNDAM外傳 殖民地墮落之地](../Page/機動戰士GUNDAM外傳_殖民地墮落之地.md "wikilink")
  - [機動戰士GUNDAM0080 口袋裡的戰爭](../Page/機動戰士GUNDAM0080_口袋裡的戰爭.md "wikilink")
  - [機動戰士GUNDAM0083 STARDUST
    MEMORY](../Page/機動戰士GUNDAM0083_STARDUST_MEMORY.md "wikilink")
  - [機動戰士Z GUNDAM](../Page/機動戰士Z_GUNDAM.md "wikilink")
  - [GUNDAM SENTINEL](../Page/GUNDAM_SENTINEL.md "wikilink")
  - [機動戰士GUNDAM ZZ](../Page/機動戰士GUNDAM_ZZ.md "wikilink")
  - [機動戰士GUNDAM 逆襲的夏亞](../Page/機動戰士GUNDAM_逆襲的夏亞.md "wikilink")
  - [機動戰士GUNDAM 閃光的哈薩維](../Page/機動戰士GUNDAM_閃光的哈薩維.md "wikilink")
  - [機動戰士GUNDAM F90](../Page/機動戰士GUNDAM_F90.md "wikilink")
  - [機動戰士GUNDAM 影子方程式91](../Page/機動戰士GUNDAM_影子方程式91.md "wikilink")
  - [機動戰士GUNDAM F91](../Page/機動戰士GUNDAM_F91.md "wikilink")
  - [機動戰士海盜GUNDAM](../Page/機動戰士海盜GUNDAM.md "wikilink")
  - [機動戰士V GUNDAM](../Page/機動戰士V_GUNDAM.md "wikilink")
  - [機動武鬥傳G GUNDAM](../Page/機動武鬥傳G_GUNDAM.md "wikilink")
  - [新機動戰記GUNDAM W](../Page/新機動戰記GUNDAM_W.md "wikilink")
  - [新機動戰記GUNDAM W G-UNIT](../Page/新機動戰記GUNDAM_W_G-UNIT.md "wikilink")
  - [新機動戰記GUNDAM W Endless
    Waltz](../Page/新機動戰記GUNDAM_W_Endless_Waltz.md "wikilink")
  - [機動新世紀GUNDAM X](../Page/機動新世紀GUNDAM_X.md "wikilink")
  - [∀GUNDAM](../Page/∀GUNDAM.md "wikilink")

</td>

<td valign=top width="35%">

  - [模型狂四郎](../Page/模型狂四郎.md "wikilink")
  - [MOBILE SUIT VARIATION:
    MSV](../Page/MOBILE_SUIT_VARIATION:_MSV.md "wikilink")
  - MS-X
  - [Ζ-MSV](../Page/Ζ-MSV.md "wikilink")
  - ΖΖ-MSV
  - [機動戰士GUNDAM Double
    Fake](../Page/Double_Fake_Under_the_Gundam.md "wikilink")
  - CCA-MSV
  - [新機動戰記GUNDAM W BATTLEFIELD OF
    PACIFIST](../Page/新機動戰記GUNDAM_W_BATTLEFIELD_OF_PACIFIST.md "wikilink")
  - SD Gundam GGENERATION GATHER BEAT
  - [MS GENERATION](../Page/機動戰士GUNDAM_MS_GENERATION.md "wikilink")
  - [機動戰士GUNDAM CROSS DIMENSION
    0079](../Page/機動戰士GUNDAM_CROSS_DIMENSION_0079.md "wikilink")
  - [GUNDAM THE BATTLE
    MASTER](../Page/GUNDAM_THE_BATTLE_MASTER.md "wikilink")
  - [機動戰士GUNDAM 基連之野望](../Page/機動戰士GUNDAM_基連之野望.md "wikilink")
  - [GUNDAM WEAPONS](../Page/GUNDAM_WEAPONS.md "wikilink")
  - [SDガンダムワールド ガチャポン戦士
    スクランブルウォーズ](../Page/SDガンダムワールド_ガチャポン戦士.md "wikilink")
  - [SD GUNDAM GX](../Page/SD_GUNDAM_GX.md "wikilink")
  - [SD GUNDAM 英雄傳](../Page/SD_GUNDAM_英雄傳.md "wikilink")
  - [SD GUNDAM 武者G世紀](../Page/SD_GUNDAM_武者G世紀.md "wikilink")
  - [GUNDAM TACTICS MOBILITY
    FLEET0079](../Page/GUNDAM_TACTICS_MOBILITY_FLEET0079.md "wikilink")

</td>

</tr>

</table>

#### SD Gundam GGENERATION-F-IF

2001年5月2日發售，要搭配「GGF」玩的GGF補充資料片。

有原創關卡10關，另外有只能使用一機與電腦互鬥（事實上電腦自己也會互鬥）來拿特定op的g-fight，也可以買入專用機供本編使用，花錢提升原作人物能力。亦可以模擬兩機交戰（如只能在太空戰鬥的的機與只能在陸地的機體在海底的夢幻對決）。

#### SD Gundam GGENERATION NEO

2002年11月28日發售的[PS2遊戲](../Page/PS2.md "wikilink")，與重現原作的之前作品不同，本作有著類似[超級機器人大戰系列的跨系列鋼彈互動發展的原創劇情](../Page/超級機器人大戰系列.md "wikilink")。[SEED系列也在此作中於本系列中初登場](../Page/機動戰士GUNDAM_SEED.md "wikilink")。

##### 登場作品

<table width="70%">

<tr>

<td valign=top width="35%">

  - [機動戰士GUNDAM](../Page/機動戰士GUNDAM.md "wikilink")
  - [機動戰士GUNDAM 第08MS小隊](../Page/機動戰士GUNDAM_第08MS小隊.md "wikilink")
  - [機動戰士GUNDAM0080 口袋裡的戰爭](../Page/機動戰士GUNDAM0080_口袋裡的戰爭.md "wikilink")
  - [機動戰士GUNDAM0083 STARDUST
    MEMORY](../Page/機動戰士GUNDAM0083_STARDUST_MEMORY.md "wikilink")
  - [機動戰士Z GUNDAM](../Page/機動戰士Z_GUNDAM.md "wikilink")
  - [機動戰士GUNDAM ZΖ](../Page/機動戰士GUNDAM_ZΖ.md "wikilink")
  - [機動戰士GUNDAM 逆襲的夏亞](../Page/機動戰士GUNDAM_逆襲的夏亞.md "wikilink")
  - [機動戰士GUNDAM F91](../Page/機動戰士GUNDAM_F91.md "wikilink")
  - [機動戰士V GUNDAM](../Page/機動戰士V_GUNDAM.md "wikilink")
  - [機動武鬥傳G GUNDAM](../Page/機動武鬥傳G_GUNDAM.md "wikilink")
  - [新機動戰記GUNDAM W](../Page/新機動戰記GUNDAM_W.md "wikilink")
  - [機動新世紀GUNDAM X](../Page/機動新世紀GUNDAM_X.md "wikilink")
  - [∀GUNDAM](../Page/∀GUNDAM.md "wikilink")

</td>

<td valign=top width="35%">

  - [模型狂四郎](../Page/模型狂四郎.md "wikilink")
  - [MOBILE SUIT VARIATION:
    MSV](../Page/MOBILE_SUIT_VARIATION:_MSV.md "wikilink")
  - [GUNDAM SENTINEL](../Page/GUNDAM_SENTINEL.md "wikilink")
  - CCA-MSV
  - [機動戰士海盜GUNDAM](../Page/機動戰士海盜GUNDAM.md "wikilink")
  - [新機動戰記GUNDAM W Endless
    Waltz](../Page/新機動戰記GUNDAM_W_Endless_Waltz.md "wikilink")
  - [機動戰士GUNDAM SEED](../Page/機動戰士GUNDAM_SEED.md "wikilink")
  - SD GUNDAM GGENERATION 獨眼鋼彈
  - [SD GUNDAM 英雄傳](../Page/SD_GUNDAM_英雄傳.md "wikilink")

</td>

</tr>

</table>

#### SD Gundam GGENERATION SEED

2004年12月19日發售的PS2遊戲，主打SEED系列的G
Generation，而部份[Astray也有登場](../Page/機動戰士GUNDAM_SEED_ASTRAY.md "wikilink")，不過不包括SEED的MSV及續作[DESTINY](../Page/機動戰士GUNDAM_SEED_DESTINY.md "wikilink")。

##### 登場作品

<table width="70%">

<tr>

<td valign=top width="35%">

  - [機動戰士GUNDAM](../Page/機動戰士GUNDAM.md "wikilink")
  - [機動戰士GUNDAM 第08MS小隊](../Page/機動戰士GUNDAM_第08MS小隊.md "wikilink")
  - [機動戰士GUNDAM 0083 STARDUST
    MEMORY](../Page/機動戰士GUNDAM_0083_STARDUST_MEMORY.md "wikilink")
  - [機動戰士Z GUNDAM](../Page/機動戰士Z_GUNDAM.md "wikilink")
  - [機動戰士GUNDAM ZZ](../Page/機動戰士GUNDAM_ZZ.md "wikilink")
  - [機動戰士GUNDAM 逆襲的夏亞](../Page/機動戰士GUNDAM_逆襲的夏亞.md "wikilink")
  - [機動戰士V GUNDAM](../Page/機動戰士V_GUNDAM.md "wikilink")
  - [新機動戰記GUNDAM W Endless
    Waltz](../Page/新機動戰記GUNDAM_W_Endless_Waltz.md "wikilink")
  - [機動戰士GUNDAM SEED](../Page/機動戰士GUNDAM_SEED.md "wikilink")

</td>

<td valign=top width="35%">

  - [模型狂四郎](../Page/模型狂四郎.md "wikilink")
  - [機動戰士GUNDAM0080 口袋裡的戰爭](../Page/機動戰士GUNDAM0080_口袋裡的戰爭.md "wikilink")
  - [MOBILE SUIT VARIATION:
    MSV](../Page/MOBILE_SUIT_VARIATION:_MSV.md "wikilink")
  - [GUNDAM SENTINEL](../Page/GUNDAM_SENTINEL.md "wikilink")
  - CCA-MSV
  - [機動戰士GUNDAM F91](../Page/機動戰士GUNDAM_F91.md "wikilink")
  - [機動戰士海盜GUNDAM](../Page/機動戰士海盜GUNDAM.md "wikilink")
  - [機動武鬥傳G GUNDAM](../Page/機動武鬥傳G_GUNDAM.md "wikilink")
  - [新機動戰記GUNDAM W](../Page/新機動戰記GUNDAM_W.md "wikilink")
  - [機動新世紀GUNDAM X](../Page/機動新世紀GUNDAM_X.md "wikilink")
  - [∀GUNDAM](../Page/∀GUNDAM.md "wikilink")
  - [GUNDAM SEED MSV](../Page/機動戰士GUNDAM_SEED_MSV.md "wikilink")
  - [機動戰士GUNDAM SEED
    ASTRAY](../Page/機動戰士GUNDAM_SEED_ASTRAY.md "wikilink")
  - SD Gundam GGENERATION 獨眼鋼彈
  - [SD GUNDAM 英雄傳](../Page/SD_GUNDAM_英雄傳.md "wikilink")

</td>

</tr>

</table>

#### SD Gundam GGENERATION SPIRITS

2007年11月29日發售的PS2遊戲，特色主要圍繞宇宙世紀系列作品和大型戰艦單位，隱藏機體為正史(UC)的∀高達。
開發系統上，玩家一方的機體廢除了ace等級，開發系統由升至ace等級才可以開發，變為每部機體在不同等級可開發不同的機體（等級比較高才可以開發更強機體）。

##### 登場作品

<table width="70%">

<tr>

<td valign=top width="35%">

  - [機動戰士GUNDAM](../Page/機動戰士GUNDAM.md "wikilink")（劇場特別版）
  - [機動戰士GUNDAM 第08MS小隊](../Page/機動戰士GUNDAM_第08MS小隊.md "wikilink")
  - [機動戰士GUNDAM MS IGLOO](../Page/機動戰士GUNDAM_MS_IGLOO.md "wikilink")
      - 機動戰士GUNDAM MS IGLOO - 1年戰爭秘錄 -
      - 機動戰士GUNDAM MS IGLOO - 默示錄0079 -
  - [機動戰士GUNDAM 外傳 THE BLUE
    DESTINY](../Page/機動戰士GUNDAM_外傳_THE_BLUE_DESTINY.md "wikilink")
  - [機動戰士GUNDAM戰記 Lost War
    Chronicles](../Page/機動戰士GUNDAM戰記_Lost_War_Chronicles.md "wikilink")
  - [機動戰士GUNDAM外傳
    宇宙、閃光的盡頭…](../Page/機動戰士GUNDAM外傳_宇宙、閃光的盡頭….md "wikilink")
  - [機動戰士GUNDAM 0080
    口袋裡的戰爭](../Page/機動戰士GUNDAM_0080_口袋裡的戰爭.md "wikilink")
  - [機動戰士GUNDAM 0083 STARDUST
    MEMORY](../Page/機動戰士GUNDAM_0083_STARDUST_MEMORY.md "wikilink")
  - 機動戰士GUNDAM 0083 STARDUST MEMORY 宇宙之蜉蝣
  - [機動戰士Z GUNDAM](../Page/機動戰士Z_GUNDAM.md "wikilink")（劇場版）
  - [機動戰士GUNDAM ZZ](../Page/機動戰士GUNDAM_ZZ.md "wikilink")
  - [機動戰士GUNDAM 逆襲的夏亞](../Page/機動戰士GUNDAM_逆襲的夏亞.md "wikilink")
  - [機動戰士GUNDAM 閃光的哈薩維](../Page/機動戰士GUNDAM_閃光的哈薩維.md "wikilink")
  - [機動戰士GUNDAM F90](../Page/機動戰士GUNDAM_F90.md "wikilink")
  - [機動戰士GUNDAM 影子方程式91](../Page/機動戰士GUNDAM_影子方程式91.md "wikilink")
  - [機動戰士GUNDAM F91](../Page/機動戰士GUNDAM_F91.md "wikilink")
  - [機動戰士海盜GUNDAM](../Page/機動戰士海盜GUNDAM.md "wikilink")
  - [機動戰士海盜GUNDAM 骷髏之心](../Page/機動戰士海盜GUNDAM_骷髏之心.md "wikilink")
  - [機動戰士V GUNDAM](../Page/機動戰士V_GUNDAM.md "wikilink")

</td>

<td valign=top width="35%">

  - [MSV](../Page/MOBILE_SUIT_VARIATION:_MSV.md "wikilink")
  - MS-X
  - [機動戰士GUNDAM外傳 殖民地墮落之地](../Page/機動戰士GUNDAM外傳_殖民地墮落之地.md "wikilink")
  - [自護前線 機動戰士GUNDAM 0079](../Page/自護前線_機動戰士GUNDAM_0079.md "wikilink")
  - [ADVANCE OF Z
    -迪坦斯的軍旗下-](../Page/ADVANCE_OF_Z_-迪坦斯的軍旗下-.md "wikilink")
  - Z-MSV
  - [GUNDAM SENTINEL](../Page/GUNDAM_SENTINEL.md "wikilink")
  - CCA-MSV
  - [機動戰士海盜GUNDAM 鋼鐵之七人](../Page/機動戰士海盜GUNDAM_鋼鐵之七人.md "wikilink")
  - [GUNDAM WEAPONS](../Page/GUNDAM_WEAPONS.md "wikilink")
  - [SDガンダムワールド ガチャポン戦士
    スクランブルウォーズ](../Page/SDガンダムワールド_ガチャポン戦士.md "wikilink")
  - [SD GUNDAM GX](../Page/SD_GUNDAM_GX.md "wikilink")
  - [機動戰士GUNDAM 基連的野望](../Page/機動戰士GUNDAM_基連的野望.md "wikilink")
  - [機動戰士GUNDAM CROSS DIMENSION 0079
    對死去的人們祈禱](../Page/機動戰士GUNDAM_CROSS_DIMENSION_0079.md "wikilink")
  - [GUNDAM TACTICS MOBILITY
    FLEET0079](../Page/GUNDAM_TACTICS_MOBILITY_FLEET0079.md "wikilink")
  - [機動戰士GUNDAM 戰場之絆](../Page/機動戰士GUNDAM_戰場之絆.md "wikilink")
  - [∀ GUNDAM](../Page/∀_GUNDAM.md "wikilink") (只有System－∀99
    ∀Gundam作為最終頭目登場)

</td>

</tr>

</table>

#### SD Gundam GGENERATION WARS

2009年8月6日
發售的[Wii](../Page/Wii.md "wikilink")/[PS2遊戲](../Page/PS2.md "wikilink")，一如以往地将加入更多的参战作品，如TV动画《[高达00](../Page/高达00.md "wikilink")》；除引入前作的大型战舰单位外，还加入名为“War
Break”的新系统，制作方称其将令系列得到惊人的进化。

##### 登場作品

<table width="70%">

<tr>

<td valign=top width="35%">

  - [機動戰士GUNDAM](../Page/機動戰士GUNDAM.md "wikilink")（劇場特別版）
  - [機動戰士GUNDAM 第08MS小隊](../Page/機動戰士GUNDAM_第08MS小隊.md "wikilink")
  - [機動戰士GUNDAM0080 口袋裡的戰爭](../Page/機動戰士GUNDAM0080_口袋裡的戰爭.md "wikilink")
  - [機動戰士GUNDAM0083 STARDUST
    MEMORY](../Page/機動戰士GUNDAM0083_STARDUST_MEMORY.md "wikilink")
  - [機動戰士ZGUNDAM](../Page/機動戰士ZGUNDAM.md "wikilink")（劇場版）
  - [機動戰士GUNDAM ZZ](../Page/機動戰士GUNDAM_ZZ.md "wikilink")
  - [機動戰士GUNDAM 逆襲的夏亞](../Page/機動戰士GUNDAM_逆襲的夏亞.md "wikilink")
  - [機動戰士GUNDAM F91](../Page/機動戰士GUNDAM_F91.md "wikilink")
  - [機動戰士V GUNDAM](../Page/機動戰士V_GUNDAM.md "wikilink")
  - [機動武鬥傳G GUNDAM](../Page/機動武鬥傳G_GUNDAM.md "wikilink")
  - [新機動戰記GUNDAM W](../Page/新機動戰記GUNDAM_W.md "wikilink")
  - [新機動戰記GUNDAM W Endless
    Waltz](../Page/新機動戰記GUNDAM_W_Endless_Waltz.md "wikilink")
  - [機動新世紀GUNDAM X](../Page/機動新世紀GUNDAM_X.md "wikilink")
  - [∀GUNDAM](../Page/∀GUNDAM.md "wikilink")
  - [機動戰士GUNDAM SEED](../Page/機動戰士GUNDAM_SEED.md "wikilink")
  - [機動戰士GUNDAM SEED
    DESTINY](../Page/機動戰士GUNDAM_SEED_DESTINY.md "wikilink")
  - [機動戰士GUNDAM SEED C.E.73
    STARGAZER](../Page/機動戰士GUNDAM_SEED_C.E.73_STARGAZER.md "wikilink")
  - [機動戰士GUNDAM 00](../Page/機動戰士GUNDAM_00.md "wikilink")

</td>

<td valign=top width="35%">

  - [機動戰士GUNDAM MS IGLOO](../Page/機動戰士GUNDAM_MS_IGLOO.md "wikilink")
      - 機動戰士GUNDAM MS IGLOO - 1年戰爭秘錄 -
      - 機動戰士GUNDAM MS IGLOO - 默示錄0079 -
  - [機動戰士GUNDAM 外傳 THE BLUE
    DESTINY](../Page/機動戰士GUNDAM_外傳_THE_BLUE_DESTINY.md "wikilink")
  - [機動戰士GUNDAM外傳
    宇宙、閃光的盡頭…](../Page/機動戰士GUNDAM外傳_宇宙、閃光的盡頭….md "wikilink")
  - [自護前線 機動戰士GUNDAM 0079](../Page/自護前線_機動戰士GUNDAM_0079.md "wikilink")
  - [ADVANCE OF Z
    -迪坦斯的軍旗下-](../Page/ADVANCE_OF_Z_-迪坦斯的軍旗下-.md "wikilink")
  - [GUNDAM SENTINEL](../Page/GUNDAM_SENTINEL.md "wikilink")
  - [機動戰士GUNDAM 閃光的哈薩維](../Page/機動戰士GUNDAM_閃光的哈薩維.md "wikilink")
  - [機動戰士GUNDAM F90](../Page/機動戰士GUNDAM_F90.md "wikilink")
  - [機動戰士GUNDAM 影子方程式91](../Page/機動戰士GUNDAM_影子方程式91.md "wikilink")
  - [機動戰士海盜GUNDAM](../Page/機動戰士海盜GUNDAM.md "wikilink")
  - [機動戰士海盜GUNDAM 骷髏之心](../Page/機動戰士海盜GUNDAM_骷髏之心.md "wikilink")
  - [機動戰士海盜GUNDAM 鋼鐵之七人](../Page/機動戰士海盜GUNDAM_鋼鐵之七人.md "wikilink")
  - [MSV](../Page/MOBILE_SUIT_VARIATION:_MSV.md "wikilink")
  - [MS-X](../Page/MS-X.md "wikilink")
  - Z-MSV
  - CCA-MSV
  - [SDガンダムワールド ガチャポン戦士
    スクランブルウォーズ](../Page/SDガンダムワールド_ガチャポン戦士.md "wikilink")
  - [SD GUNDAM GX](../Page/SD_GUNDAM_GX.md "wikilink")
  - [機動戰士GUNDAM CROSS DIMENSION 0079
    對死去的人們祈禱](../Page/機動戰士GUNDAM_CROSS_DIMENSION_0079.md "wikilink")
  - [機動戰士GUNDAM 戰場之絆](../Page/機動戰士GUNDAM_戰場之絆.md "wikilink")

</td>

</tr>

</table>

#### SD Gundam GGENERATION WORLD

2011年2月24日在[PSP及](../Page/PSP.md "wikilink")[Wii上推出的遊戲](../Page/Wii.md "wikilink")。是集合至今“机动战士高达”作品的策略角色扮演游戏《SD高达
G世代》系列作，一如以往地将加入更多的参战作品，如刚完结的TV动画《[高达00](../Page/高达00.md "wikilink")》《[高达UC](../Page/高达UC.md "wikilink")》；除引入前作的大型战舰单位外，还加入名为“Generation
Break”的新系统，制作方称其将令系列得到惊人的进化。

##### 登場作品

<table width="70%">

<tr>

<td valign=top width="35%">

  - [機動戰士GUNDAM](../Page/機動戰士GUNDAM.md "wikilink")（劇場特別版）
  - [MSV](../Page/MOBILE_SUIT_VARIATION:_MSV.md "wikilink")
  - MS-X
  - [機動戰士GUNDAM 第08MS小隊](../Page/機動戰士GUNDAM_第08MS小隊.md "wikilink")
  - [機動戰士GUNDAM MS IGLOO](../Page/機動戰士GUNDAM_MS_IGLOO.md "wikilink")
      - 機動戰士GUNDAM MS IGLOO - 1年戰爭秘錄 -
      - 機動戰士GUNDAM MS IGLOO - 默示錄0079 -
  - [機動戰士GUNDAM 外傳 THE BLUE
    DESTINY](../Page/機動戰士GUNDAM_外傳_THE_BLUE_DESTINY.md "wikilink")
  - [自護前線 機動戰士GUNDAM 0079](../Page/自護前線_機動戰士GUNDAM_0079.md "wikilink")
  - [機動戰士GUNDAM外傳
    宇宙、閃光的盡頭…](../Page/機動戰士GUNDAM外傳_宇宙、閃光的盡頭….md "wikilink")
  - [機動戰士GUNDAM0080 口袋裡的戰爭](../Page/機動戰士GUNDAM0080_口袋裡的戰爭.md "wikilink")
  - [機動戰士GUNDAM戰記 BATTLEFIELD RECORD
    U.C.0081](../Page/機動戰士GUNDAM戰記_\(PS3\).md "wikilink")
  - [機動戰士GUNDAM0083 STARDUST
    MEMORY](../Page/機動戰士GUNDAM0083_STARDUST_MEMORY.md "wikilink")
  - [ADVANCE OF Ζ 迪坦斯的軍旗下-](../Page/ADVANCE_OF_Ζ_迪坦斯的軍旗下-.md "wikilink")
  - [機動戰士Z GUNDAM](../Page/機動戰士Z_GUNDAM.md "wikilink")（劇場版）
  - Z-MSV
  - [GUNDAM SENTINEL](../Page/GUNDAM_SENTINEL.md "wikilink")
  - [機動戰士GUNDAM ZZ](../Page/機動戰士GUNDAM_ZZ.md "wikilink")
  - [機動戰士GUNDAM 逆襲的夏亞](../Page/機動戰士GUNDAM_逆襲的夏亞.md "wikilink")
  - CCA-MSV
  - [M-MSV](../Page/M-MSV.md "wikilink")
  - [機動戰士GUNDAM UC](../Page/機動戰士GUNDAM_UC.md "wikilink")
  - [機動戰士GUNDAM 閃光的哈薩維](../Page/機動戰士GUNDAM_閃光的哈薩維.md "wikilink")
  - [機動戰士GUNDAM F90](../Page/機動戰士GUNDAM_F90.md "wikilink")
  - [機動戰士GUNDAM 影子方程式91](../Page/機動戰士GUNDAM_影子方程式91.md "wikilink")
  - [機動戰士GUNDAM F91](../Page/機動戰士GUNDAM_F91.md "wikilink")
  - F91-MSV

</td>

<td valign=top width="35%">

  - [機動戰士海盜GUNDAM](../Page/機動戰士海盜GUNDAM.md "wikilink")
  - [機動戰士海盜GUNDAM 骷髏之心](../Page/機動戰士海盜GUNDAM_骷髏之心.md "wikilink")
  - [機動戰士海盜GUNDAM 鋼鐵之七人](../Page/機動戰士海盜GUNDAM_鋼鐵之七人.md "wikilink")
  - [機動戰士VGUNDAM](../Page/機動戰士VGUNDAM.md "wikilink")
  - [機動武鬥傳G GUNDAM](../Page/機動武鬥傳G_GUNDAM.md "wikilink")
  - [新機動戰記GUNDAM W](../Page/新機動戰記GUNDAM_W.md "wikilink")
  - [新機動戰記GUNDAM W Endless
    Waltz](../Page/新機動戰記GUNDAM_W_Endless_Waltz.md "wikilink")
  - [機動新世紀GUNDAM X](../Page/機動新世紀GUNDAM_X.md "wikilink")
  - [∀GUNDAM](../Page/∀GUNDAM.md "wikilink")
  - [機動戰士GUNDAM SEED](../Page/機動戰士GUNDAM_SEED.md "wikilink")
  - [機動戰士GUNDAM SEED
    ASTRAY](../Page/機動戰士GUNDAM_SEED_ASTRAY.md "wikilink")
  - [機動戰士GUNDAM SEED X
    ASTRAY](../Page/機動戰士GUNDAM_SEED_X_ASTRAY.md "wikilink")
  - [機動戰士GUNDAM SEED
    DESTINY](../Page/機動戰士GUNDAM_SEED_DESTINY.md "wikilink")
  - [機動戰士GUNDAM SEED C.E.73
    STARGAZER](../Page/機動戰士GUNDAM_SEED_C.E.73_STARGAZER.md "wikilink")
  - [機動戰士GUNDAM 00](../Page/機動戰士GUNDAM_00.md "wikilink")
  - [劇場版 機動戰士GUNDAM 00 -A wakening of the
    Trailblazer-](../Page/劇場版_機動戰士GUNDAM_00_-A_wakening_of_the_Trailblazer-.md "wikilink")
  - [SDガンダムワールド ガチャポン戦士
    スクランブルウォーズ](../Page/SDガンダムワールド_ガチャポン戦士.md "wikilink")
  - [SD GUNDAM GX](../Page/SD_GUNDAM_GX.md "wikilink")
  - [機動戰士GUNDAM 基連的野望](../Page/機動戰士GUNDAM_基連的野望.md "wikilink")
  - [機動戰士GUNDAM CROSS DIMENSION 0079
    對死去的人們祈禱](../Page/機動戰士GUNDAM_CROSS_DIMENSION_0079.md "wikilink")
  - [機動戰士GUNDAM 戰場之絆](../Page/機動戰士GUNDAM_戰場之絆.md "wikilink")
  - [SD GUNDAM三國傳
    BraveBattleWarriors](../Page/SD_GUNDAM三國傳_BraveBattleWarriors.md "wikilink")

</td>

</tr>

</table>

#### SD Gundam GGENERATION GENESIS

預計在2016年11月22日在[PS4以及](../Page/PlayStation_4.md "wikilink")[PS
Vita平台發售](../Page/PS_Vita.md "wikilink")，官方取消推出PS3平台以及收錄作品只到機動戰士高達UC。由於本作容量較大，所以PSV會有兩枚遊戲卡作遊玩。本作標題主打「傳承靈魂、回歸全新原點」，為配合高達遊戲30周年的紀念作品之一。此外，本作是官方首次釋出繁體中文版，讓玩家能對背景故事有更深入的了解。同時本作也是系列作中首次透過DLC追加登場作品及其他附加內容。

##### 登場作品

<table width="70%">

<tr>

<td valign=top width="35%">

  - [機動戰士GUNDAM](../Page/機動戰士GUNDAM.md "wikilink")（包括劇場特別版）
  - [MSV](../Page/MOBILE_SUIT_VARIATION:_MSV.md "wikilink")
  - MS-X
  - [M-MSV](../Page/M-MSV.md "wikilink")
  - MSV-R
  - [機動戰士GUNDAM MS IGLOO](../Page/機動戰士GUNDAM_MS_IGLOO.md "wikilink")
      - 機動戰士GUNDAM MS IGLOO - 1年戰爭秘錄 -
      - 機動戰士GUNDAM MS IGLOO - 默示錄0079 -
      - 機動戰士GUNDAM MS IGLOO2 -重力戰線-
  - [機動戰士GUNDAM 第08MS小隊](../Page/機動戰士GUNDAM_第08MS小隊.md "wikilink")
  - [機動戰士GUNDAM 外傳 THE BLUE
    DESTINY](../Page/機動戰士GUNDAM_外傳_THE_BLUE_DESTINY.md "wikilink")
  - [機動戰士GUNDAM外傳 殖民地墮落之地](../Page/機動戰士GUNDAM外傳_殖民地墮落之地.md "wikilink")
  - [自護前線 機動戰士GUNDAM 0079](../Page/自護前線_機動戰士GUNDAM_0079.md "wikilink")
  - [機動戰士GUNDAM 相逢宇宙](../Page/機動戰士GUNDAM_相逢宇宙.md "wikilink")
  - [機動戰士GUNDAM外傳
    宇宙、閃光的盡頭…](../Page/機動戰士GUNDAM外傳_宇宙、閃光的盡頭….md "wikilink")
  - [機動戰士GUNDAM戰記 Lost War
    Chronicles](../Page/機動戰士GUNDAM戰記_Lost_War_Chronicles.md "wikilink")
  - [機動戰士GUNDAM戰記 BATTLEFIELD RECORD
    U.C.0081](../Page/機動戰士GUNDAM戰記_\(PS3\).md "wikilink")
  - [機動戰士GUNDAM外傳 Missing
    Link](../Page/機動戰士GUNDAM外傳_Missing_Link.md "wikilink")
  - [機動戰士GUNDAM CROSS DIMENSION
    0079](../Page/機動戰士GUNDAM_CROSS_DIMENSION_0079.md "wikilink")
  - [ハーモニー・オブ・ガンダム](../Page/ハーモニー・オブ・ガンダム.md "wikilink")
  - [機動戰士高達 戰場之絆](../Page/機動戰士高達_戰場之絆.md "wikilink")
  - [機動戰士高達 基連的野望](../Page/機動戰士高達_基連的野望.md "wikilink")
  - [機動戰士GUNDAM0080 口袋裡的戰爭](../Page/機動戰士GUNDAM0080_口袋裡的戰爭.md "wikilink")
  - [機動戰士GUNDAM0083 STARDUST
    MEMORY](../Page/機動戰士GUNDAM0083_STARDUST_MEMORY.md "wikilink")

</td>

<td valign=top width="35%">

  - 機動戦士ガンダム ファントム・ブレット
  - [機動戰士Z GUNDAM](../Page/機動戰士Z_GUNDAM.md "wikilink")
  - [機動戰士Z GUNDAM劇場版](../Page/機動戰士Z_GUNDAM劇場版.md "wikilink")
  - Z-MSV
  - [ADVANCE OF Z
    -迪坦斯的軍旗下-](../Page/ADVANCE_OF_Z_-迪坦斯的軍旗下-.md "wikilink")
  - [GUNDAM SENTINEL](../Page/GUNDAM_SENTINEL.md "wikilink")
  - [機動戰士GUNDAM ZZ](../Page/機動戰士GUNDAM_ZZ.md "wikilink")
  - [機動戰士GUNDAM 逆襲的夏亞](../Page/機動戰士GUNDAM_逆襲的夏亞.md "wikilink")
  - CCA-MSV
  - [機動戰士GUNDAM UC](../Page/機動戰士GUNDAM_UC.md "wikilink")
  - 機動戰士GUNDAM UC-MSV
  - [機動戰士鋼彈 閃光的哈薩威](../Page/機動戰士鋼彈_閃光的哈薩威.md "wikilink") (DLC)
  - [GUNDAM G之复国运动](../Page/GUNDAM_G之复国运动.md "wikilink") (只有 YG-111
    Gundam G-SELF Perfect Pack作為隱藏機登場)
  - [機動戰士GUNDAM THE
    ORIGIN](../Page/機動戰士GUNDAM_THE_ORIGIN.md "wikilink")(在12月DLC追加)
  - [機動戰士鋼彈 鐵血的孤兒](../Page/機動戰士鋼彈_鐵血的孤兒.md "wikilink") (Gundam Barbatos
    Lupus在12月DLC追加)
  - [GUNDAM創戰者TRY](../Page/GUNDAM創戰者TRY.md "wikilink") (在2月DLC追加)
  - [機動戰士GUNDAM
    CDA年輕彗星的肖像](../Page/機動戰士GUNDAM_CDA年輕彗星的肖像.md "wikilink")(在2月DLC追加)
  - [機動戰士GUNDAM
    Thunderbolt](../Page/機動戰士GUNDAM_Thunderbolt.md "wikilink")(在2月DLC追加)
  - [∀ GUNDAM](../Page/∀_GUNDAM.md "wikilink") (只有System－∀99
    ∀Gundam作為最終頭目登場)
  - SD GUNDAM GX
  - SD Gundam World Gachapon warrior
  - 其他高達系列作品

</td>

</tr>

</table>

#### SD Gundam GGENERATION CROSS RAYS

### 掌上主機系列

#### SD Gundam GGENERATION GATHER BEAT

2000年7月13日在[WonderSwan上推出的遊戲](../Page/WonderSwan.md "wikilink")。
整個掌機系列，除了在PSP登場的2個版本以外，開發系統變成只能使用特定零件加在特定機體開發成新機體，大部分機體亦無法直接購買，只能以開發才能得到新機體（包括已經有的同類機體）。
另外，在捕獲系統方面，由擊破戰艦改為以三組或以上小隊圍捕沒有小隊的敵機（戰艦不能捕獲，但計算為為圍捕小隊），另外戰艦亦只能透過故事進度入手。
小隊系統則是以最多三機為一小隊（部分大型機只能組一普通機，甚至不能組隊），攻擊次序為：攻擊方射擊-\>守方射擊-\>攻方格鬥-\>守方格鬥。另外沒有了地圖炮，取而代之的射擊指令是能以一機同時攻擊敵方一小隊而不會被還擊。除了部分遠攻武器外，包括射擊系武器只能在貼近對手時攻擊。
另外，整個系列中，能開發的大部分只有uc系列的機體，而非uc系列的如G、W、X、SEED等大多都只能以捕獲或依照故事進度加入。

#### SD Gundam GGENERATION GATHER BEAT 2

2001年6月12日在[WonderSwan
Color上推出的遊戲](../Page/WonderSwan_Color.md "wikilink")。

#### SD Gundam GGENERATION 獨眼GUNDAM

2002年9月26日在[WonderSwan
Crystal上推出的遊戲](../Page/WonderSwan_Crystal.md "wikilink")，劇情從初代高達橫跨到機動戰士Z高達，或是隨著玩家選擇路線而進入機動戰士高達ZZ的劇情。主角是原創角色，其他原創角色還有、、、等。本作亦延續了前作Gather
Beat和Gather Beat 2的遊戲系統，並且罕見地以自護公國的視角展開故事。此外，前作Gather Beat
2的機體升級系統用的零件以及機體開發系譜亦被重新編排，並且被後來的Advance和DS等作品所採納。對於前作“格鬥武器比射擊武器的傷害還要高”的設定進行了修改，變成了“射擊武器傷害較高且可先發製人但命中率較低，而格鬥武器相對的有著高命中率”的設定，並被後來的作品繼續沿用這套系統。此外除了機體人物的圖鑑外亦收錄了遊戲BGM和發生過的特殊遊戲事件，可以在通關後進行重溫閱覽。

#### SD Gundam GGENERATION ADVANCE

2003年11月27日在[GBA上發售的遊戲](../Page/GBA.md "wikilink")，主線是[一年战争跟](../Page/一年战争.md "wikilink")[SEED](../Page/機動戰士Gundam_SEED.md "wikilink")。

由于机体及卡帶容量本身限制，很多机体和机师并未出场，像《[-{zh-hk:高达W;zh-tw:鋼彈W}-](../Page/新機動戰記GUNDAM_W.md "wikilink")》里面的5人只剩下[希洛](../Page/希洛·唯.md "wikilink")，还有SEED只剩下[-{zh-hk:基拉;zh-tw:煌}-](../Page/煌·大和.md "wikilink")、[卡嘉莉和](../Page/卡嘉莉·尤拉·阿斯哈.md "wikilink")[阿斯兰等等](../Page/阿斯兰.md "wikilink")。部分动画是重复的，且部分机体的表现比较让人失望，像V
GUNDAM。但是总体而言音乐动画表現和可玩性都很高，戰鬥畫面也是首次變成45度角。

#### SD Gundam GGENERATION DS

2005年5月26日在[NDS上發售的遊戲系列第一彈](../Page/NDS.md "wikilink")，劇情主軸在吉翁開始，算是在G世代少見的劇情表現。

實際上，本作的故事內容為獨眼高達的再編，以吉翁軍的角度重新作描寫。與獨眼高達一作不同，本作並沒有明確的主角存在，除了獨眼高達有登場的等角色外，本作還有新的原創少女角色、和與外貌完全相同的、登場。此外，本作的故事流程可分為三條路線，分別為宇宙世紀路線、平成高達路線、和隱藏的敵軍路線（以通常時為敵軍的角色的視點進行攻略）。

與「GGENERATION
ADVANCE」一樣，因容量問題，以致參戰作品中很多主要機體和機師均沒有登場。但另一方面，因著上述由吉翁側開始、各GGENERATION作的參加、奇特的故事展開（特別是終章的時候）等，使本作成為歷代中尤其受歡迎的一作。在之後推出的「CROSS
DRIVE」發售後的一段時間內，本作在二手商店的賣值比該作還要高。

#### SD Gundam GGENERATION PORTABLE

2006年8月3日在[PSP上发售的游戏](../Page/PSP.md "wikilink")，收录1133部机体、787个人物及70个关卡。部分系統上使用近似NEO系，但資料庫和畫面方面，除了SEED、SEED
DESTINY及SEED MSV外只有極小量新作，其他均來自舊作GGF。

此作中，被取消的故事包括[The Blue
Destiny](../Page/機動戰士GUNDAM_The_Blue_Destiny.md "wikilink")、[S高達](../Page/GUNDAM前哨戰.md "wikilink")、[無盡的華爾茲及G](../Page/新機動戰記GUNDAM_W_無盡的華爾茲.md "wikilink")-Unit等等。

##### 登場作品

<table width="70%">

<tr>

<td valign=top width="35%">

  - [機動戰士GUNDAM](../Page/機動戰士GUNDAM.md "wikilink")（劇場版）
  - [機動戰士GUNDAM 第08MS小隊](../Page/機動戰士GUNDAM_第08MS小隊.md "wikilink")
  - [機動戰士GUNDAM0080 口袋裡的戰爭](../Page/機動戰士GUNDAM0080_口袋裡的戰爭.md "wikilink")
  - [機動戰士GUNDAM0083 STARDUST
    MEMORY](../Page/機動戰士GUNDAM0083_STARDUST_MEMORY.md "wikilink")
  - [機動戰士Z GUNDAM](../Page/機動戰士Z_GUNDAM.md "wikilink")
  - [機動戰士GUNDAM ZZ](../Page/機動戰士GUNDAM_ZZ.md "wikilink")
  - [機動戰士GUNDAM 逆襲的夏亞](../Page/機動戰士GUNDAM_逆襲的夏亞.md "wikilink")
  - [機動戰士GUNDAM F91](../Page/機動戰士GUNDAM_F91.md "wikilink")
  - [機動戰士V GUNDAM](../Page/機動戰士V_GUNDAM.md "wikilink")
  - [機動武鬥傳G GUNDAM](../Page/機動武鬥傳G_GUNDAM.md "wikilink")
  - [新機動戰記GUNDAM W](../Page/新機動戰記GUNDAM_W.md "wikilink")
  - [機動新世紀GUNDAM X](../Page/機動新世紀GUNDAM_X.md "wikilink")
  - [∀GUNDAM](../Page/∀GUNDAM.md "wikilink")
  - [機動戰士GUNDAM SEED](../Page/機動戰士GUNDAM_SEED.md "wikilink")
  - [機動戰士GUNDAM SEED
    DESTINY](../Page/機動戰士GUNDAM_SEED_DESTINY.md "wikilink")

</td>

<td valign=top width="35%">

  - [MSV](../Page/MOBILE_SUIT_VARIATION:_MSV.md "wikilink")
  - MS-X
  - [M-MSV](../Page/M-MSV.md "wikilink")
  - [機動戰士GUNDAM外傳系列](../Page/機動戰士GUNDAM_外傳_THE_BLUE_DESTINY.md "wikilink")
  - [機動戰士GUNDAM MS IGLOO - 1年戰爭秘錄
    -](../Page/機動戰士GUNDAM_MS_IGLOO.md "wikilink")
  - ADVANCE OF Ζ -{zh-hk:泰坦斯的旗下;zh-tw:-迪坦斯的軍旗下-}-
  - [Ζ-MSV](../Page/Ζ-MSV.md "wikilink")
  - [GUNDAM SENTINEL](../Page/GUNDAM_SENTINEL.md "wikilink")
  - ΖΖ-MSV
  - CCA-MSV
  - [機動戰士GUNDAM 閃光的哈薩維](../Page/機動戰士GUNDAM_閃光的哈薩維.md "wikilink")
  - [機動戰士GUNDAM F90](../Page/機動戰士GUNDAM_F90.md "wikilink")
  - [機動戰士海盜GUNDAM](../Page/機動戰士海盜GUNDAM.md "wikilink")
  - [新機動戰記GUNDAM W Endless
    Waltz](../Page/新機動戰記GUNDAM_W_Endless_Waltz.md "wikilink")
  - [機動戰士GUNDAM SEED
    ASTRAY](../Page/機動戰士GUNDAM_SEED_ASTRAY.md "wikilink")
  - [機動戰士GUNDAM SEED X
    ASTRAY](../Page/機動戰士GUNDAM_SEED_X_ASTRAY.md "wikilink")
  - [GUNDAM WEAPONS](../Page/GUNDAM_WEAPONS.md "wikilink")
  - [SDガンダムワールド ガチャポン戦士
    スクランブルウォーズ](../Page/SDガンダムワールド_ガチャポン戦士.md "wikilink")
  - [SD GUNDAM GX](../Page/SD_GUNDAM_GX.md "wikilink")
  - [機動戰士GUNDAM 基連之野望](../Page/機動戰士GUNDAM_基連之野望.md "wikilink")
  - [機動戰士GUNDAM CROSS DIMENSION
    0079](../Page/機動戰士GUNDAM_CROSS_DIMENSION_0079.md "wikilink")
  - [GUNDAM TACTICS MOBILITY
    FLEET0079](../Page/GUNDAM_TACTICS_MOBILITY_FLEET0079.md "wikilink")

</td>

</tr>

</table>

#### SD Gundam GGENERATION CROSS DRIVE

2007年8月9日在NDS發售的系列第二彈，收錄合計120個關卡，為NDS GGENERATION系列之冠。 系統上改變了以下幾點：

  - 戰艦
    無法搭載機體，只能使用補血指令回複友軍，也並非必須出擊。
  - 小隊
    直接以最大三機的小隊出擊。
  - 開發
    部分機體須等級足夠配上部件才可以轉換成新機體，開發後即使未有機體變化也會有經驗值增加。不同改造技師會影響機體性能。另外能開發的機體大增，包括SEED、SEED
    DESTINY、W系的非UC系列都能開發，而且不像前幾作，一些終極級機體（如[攻擊自由鋼彈](../Page/ZGMF-X20A_Strike_Freedom.md "wikilink")、[飛翼鋼彈零式特裝型](../Page/新機動戰記GUNDAM_W_機體列表#劇場版_Endless_Waltz（無盡的華爾茲）.md "wikilink")）也可以直接開發。另外開發當中有機會隨機增加特殊部分。

操控以觸控移動機體位置來決定使用的武器，特定機師加上特定機體及武器更可使出cross
drive組合技（與一般的分別是能攻擊對方整個小隊）。首次收錄了[SEED
DESTINY外傳觀星者](../Page/机动战士GUNDAM_SEED_C.E.73_STARGAZER.md "wikilink")。
此作中能開發的：UC系列、SEED系列、SEED DENSITY系列、WING系列、SEED
DENSITY的外傳觀星者中大部分機體及[X
Astray](../Page/機動戰士GUNDAM_SEED_X_ASTRAY.md "wikilink")。
此作中不能開發的包括V2（V高達是可以開發的）、G高達系列、X高達系列、SEED
Astray的Astray紅藍二機及觀星者高達。

#### SD Gundam GGENERATION 3D

2011年12月22日在[3DS上推出的遊戲](../Page/3DS.md "wikilink")。

##### 登場作品

<table width="70%">

<tr>

<td valign=top width="35%">

  - [機動戰士GUNDAM](../Page/機動戰士GUNDAM.md "wikilink")（劇場特別版）
  - [機動戰士GUNDAM 第08MS小隊](../Page/機動戰士GUNDAM_第08MS小隊.md "wikilink")
  - [機動戰士GUNDAM0080 口袋裡的戰爭](../Page/機動戰士GUNDAM0080_口袋裡的戰爭.md "wikilink")
  - [機動戰士GUNDAM0083 STARDUST
    MEMORY](../Page/機動戰士GUNDAM0083_STARDUST_MEMORY.md "wikilink")
  - [機動戰士Z GUNDAM](../Page/機動戰士Z_GUNDAM.md "wikilink")（劇場版）
  - [機動戰士GUNDAM ZZ](../Page/機動戰士GUNDAM_ZZ.md "wikilink")
  - [機動戰士GUNDAM 逆襲的夏亞](../Page/機動戰士GUNDAM_逆襲的夏亞.md "wikilink")
  - [機動戰士GUNDAM UC](../Page/機動戰士GUNDAM_UC.md "wikilink")
  - [機動戰士GUNDAM F91](../Page/機動戰士GUNDAM_F91.md "wikilink")
  - [機動戰士海盜GUNDAM](../Page/機動戰士海盜GUNDAM.md "wikilink")
  - [機動戰士V GUNDAM](../Page/機動戰士V_GUNDAM.md "wikilink")

</td>

<td valign=top width="35%">

  - [機動武鬥傳G GUNDAM](../Page/機動武鬥傳G_GUNDAM.md "wikilink")
  - [新機動戰記GUNDAM W](../Page/新機動戰記GUNDAM_W.md "wikilink")
  - [新機動戰記GUNDAM W Endless
    Waltz](../Page/新機動戰記GUNDAM_W_Endless_Waltz.md "wikilink")
  - [機動新世紀GUNDAM X](../Page/機動新世紀GUNDAM_X.md "wikilink")
  - [∀GUNDAM](../Page/∀GUNDAM.md "wikilink")
  - [機動戰士GUNDAM SEED](../Page/機動戰士GUNDAM_SEED.md "wikilink")
  - [機動戰士GUNDAM SEED
    DESTINY](../Page/機動戰士GUNDAM_SEED_DESTINY.md "wikilink")
  - [機動戰士GUNDAM SEED C.E.73
    STARGAZER](../Page/機動戰士GUNDAM_SEED_C.E.73_STARGAZER.md "wikilink")
  - [機動戰士GUNDAM 00](../Page/機動戰士GUNDAM_00.md "wikilink")
  - [劇場版 機動戰士GUNDAM 00 -A wakening of the
    Trailblazer-](../Page/劇場版_機動戰士GUNDAM_00_-A_wakening_of_the_Trailblazer-.md "wikilink")
  - [模型戰士GUNDAM模型製作家 起始G](../Page/模型戰士GUNDAM模型製作家_起始G.md "wikilink")
  - [機動戰士GUNDAM
    AGE](../Page/機動戰士GUNDAM_AGE.md "wikilink")（註：只有第1部的人物和機體參戰。）

</td>

</tr>

</table>

#### SD Gundam GGENERATION WORLD

2011年2月24日在PSP及Wii上推出的遊戲。

*見[\#SD Gundam GGENERATION
WORLD](../Page/#SD_Gundam_GGENERATION_WORLD.md "wikilink")*

#### SD Gundam GGENERATION OVER WORLD

2012年9月27日在[PSP上推出的遊戲](../Page/PSP.md "wikilink")。

##### 登場作品

<table width="70%">

<tr>

<td valign=top width="35%">

  - [機動戰士GUNDAM](../Page/機動戰士GUNDAM.md "wikilink")（劇場特別版）
  - [MSV](../Page/MOBILE_SUIT_VARIATION:_MSV.md "wikilink")
  - MS-X
  - [機動戰士GUNDAM 第08MS小隊](../Page/機動戰士GUNDAM_第08MS小隊.md "wikilink")
  - [機動戰士GUNDAM MS IGLOO](../Page/機動戰士GUNDAM_MS_IGLOO.md "wikilink")
      - 機動戰士GUNDAM MS IGLOO - 1年戰爭秘錄 -
      - 機動戰士GUNDAM MS IGLOO - 默示錄0079 -
      - 機動戰士GUNDAM MS IGLOO2 -重力戰線-
  - [機動戰士GUNDAM 外傳 THE BLUE
    DESTINY](../Page/機動戰士GUNDAM_外傳_THE_BLUE_DESTINY.md "wikilink")
  - [機動戰士GUNDAM外傳
    宇宙、閃光的盡頭…](../Page/機動戰士GUNDAM外傳_宇宙、閃光的盡頭….md "wikilink")
  - [自護前線 機動戰士GUNDAM 0079](../Page/自護前線_機動戰士GUNDAM_0079.md "wikilink")
  - [機動戰士GUNDAM0080 口袋裡的戰爭](../Page/機動戰士GUNDAM0080_口袋裡的戰爭.md "wikilink")
  - [機動戰士GUNDAM戰記 BATTLEFIELD RECORD
    U.C.0081](../Page/機動戰士GUNDAM戰記_\(PS3\).md "wikilink")
  - [機動戰士GUNDAM0083 STARDUST
    MEMORY](../Page/機動戰士GUNDAM0083_STARDUST_MEMORY.md "wikilink")
  - [ADVANCE OF Ζ 泰坦斯的旗下](../Page/ADVANCE_OF_Ζ_泰坦斯的旗下.md "wikilink")
  - [機動戰士Z GUNDAM](../Page/機動戰士Z_GUNDAM.md "wikilink")
  - 機動戰士Z GUNDAM（劇場版）
  - [Ζ-MSV](../Page/Ζ-MSV.md "wikilink")
  - [GUNDAM SENTINEL](../Page/GUNDAM_SENTINEL.md "wikilink")
  - [機動戰士GUNDAM ZZ](../Page/機動戰士GUNDAM_ZZ.md "wikilink")
  - [機動戰士GUNDAM 逆襲的夏亞](../Page/機動戰士GUNDAM_逆襲的夏亞.md "wikilink")
  - CCA-MSV
  - [M-MSV](../Page/M-MSV.md "wikilink")
  - [機動戰士GUNDAM UC](../Page/機動戰士GUNDAM_UC.md "wikilink")
  - 機動戰士GUNDAM UC-MSV
  - [機動戰士GUNDAM 閃光的哈薩維](../Page/機動戰士GUNDAM_閃光的哈薩維.md "wikilink")
  - [機動戰士GUNDAM F90](../Page/機動戰士GUNDAM_F90.md "wikilink")
  - [機動戰士GUNDAM 影子方程式91](../Page/機動戰士GUNDAM_影子方程式91.md "wikilink")
  - [機動戰士GUNDAM F91](../Page/機動戰士GUNDAM_F91.md "wikilink")
  - F91-MSV

</td>

<td valign=top width="35%">

  - [機動戰士海盜GUNDAM](../Page/機動戰士海盜GUNDAM.md "wikilink")
  - [機動戰士海盜GUNDAM 骷髏之心](../Page/機動戰士海盜GUNDAM_骷髏之心.md "wikilink")
  - [機動戰士海盜GUNDAM 鋼鐵之七人](../Page/機動戰士海盜GUNDAM_鋼鐵之七人.md "wikilink")
  - [機動戰士VGUNDAM](../Page/機動戰士VGUNDAM.md "wikilink")
  - [機動武鬥傳G GUNDAM](../Page/機動武鬥傳G_GUNDAM.md "wikilink")
  - [新機動戰記GUNDAM W](../Page/新機動戰記GUNDAM_W.md "wikilink")
  - [新機動戰記GUNDAM W Endless
    Waltz](../Page/新機動戰記GUNDAM_W_Endless_Waltz.md "wikilink")
  - [新機動戰記GUNDAM W Dual Story
    G-UNIT](../Page/新機動戰記GUNDAM_W_Dual_Story_G-UNIT.md "wikilink")
  - [機動新世紀GUNDAM X](../Page/機動新世紀GUNDAM_X.md "wikilink")
  - [∀GUNDAM](../Page/∀GUNDAM.md "wikilink")
  - [機動戰士GUNDAM SEED](../Page/機動戰士GUNDAM_SEED.md "wikilink")
  - [SEED-MSV](../Page/機動戰士GUNDAM_SEED_MSV.md "wikilink")
  - [機動戰士GUNDAM SEED
    ASTRAY](../Page/機動戰士GUNDAM_SEED_ASTRAY.md "wikilink")
  - [機動戰士GUNDAM SEED X
    ASTRAY](../Page/機動戰士GUNDAM_SEED_X_ASTRAY.md "wikilink")
  - [機動戰士GUNDAM SEED
    DESTINY](../Page/機動戰士GUNDAM_SEED_DESTINY.md "wikilink")
  - SEED DESTINY-MSV
  - [機動戰士GUNDAM SEED C.E.73
    STARGAZER](../Page/機動戰士GUNDAM_SEED_C.E.73_STARGAZER.md "wikilink")
  - [機動戰士GUNDAM 00](../Page/機動戰士GUNDAM_00.md "wikilink")
  - [劇場版 機動戰士GUNDAM 00 -A wakening of the
    Trailblazer-](../Page/劇場版_機動戰士GUNDAM_00_-A_wakening_of_the_Trailblazer-.md "wikilink")
  - [機動戰士GUNDAM 00外傳](../Page/機動戰士GUNDAM_00#外傳.md "wikilink")
      - 機動戰士GUNDAM 00P
      - 機動戰士GUNDAM 00F
      - 機動戰士GUNDAM 00V
  - [機動戰士GUNDAM AGE](../Page/機動戰士GUNDAM_AGE.md "wikilink")
  - [模型戰士GUNDAM模型製作家 起始G](../Page/模型戰士GUNDAM模型製作家_起始G.md "wikilink")
  - 機動戦士ガンダム シークレット・ウェポンズ ファントム・ブレット
  - [SDガンダムワールド ガチャポン戦士
    スクランブルウォーズ](../Page/SDガンダムワールド_ガチャポン戦士.md "wikilink")
  - [SD GUNDAM GX](../Page/SD_GUNDAM_GX.md "wikilink")
  - [機動戰士GUNDAM 基連之野望](../Page/機動戰士GUNDAM_基連之野望.md "wikilink")
  - [機動戰士GUNDAM CROSS DIMENSION 0079
    對死去的人們祈禱](../Page/機動戰士GUNDAM_CROSS_DIMENSION_0079.md "wikilink")
  - 其他GUNDAM遊戲作品

</td>

</tr>

</table>

#### SD Gundam GGENERATION GENESIS

在2016年11月22日在PS4以及PS Vita平台發售，官方取消推出PS3平台以及收錄作品只到機動戰士高達UC。

*見[\#SD Gundam GGENERATION
GENESIS](../Page/#SD_Gundam_GGENERATION_GENESIS.md "wikilink")*

#### SD Gundam GGENERATION CROSS RAYS

### PC系列

  - SD Gundam GGENERATION-DA
  - SD Gundam GGENERATION CROSS RAYS

### 手遊系列

SD Gundam GGENERATION FRONTIER  SD Gundam GGENERATION RE

## 外部連結

  - [SD Gundam GGENERATION 官方網站](http://www.ggene.jp/)
  - [SD Gundam
    GGENERATION](http://www.bandaigames.channel.or.jp/list/_vg/ggene0.htm)
  - [SD Gundam
    GGENERATION-ZERO](http://www.bandaigames.channel.or.jp/list/gzero/index.htm)
  - [SD Gundam
    GGENERATION-F-IF](http://www.gundam.channel.or.jp/videogame/ggenefif/)
  - [SD Gundam GGENERATION
    NEO](http://www.gundam.channel.or.jp/goods/videogame/ggeneneo/)
  - [SD Gundam GGENERATION
    SEED](http://www.bandaigames.channel.or.jp/list/ggseed/)
  - [SD Gundam GGENERATION
    PORTABLE](https://web.archive.org/web/20120103093900/http://www.ggenep.com/pc/index.html)
  - [SD Gundam GGENERATION SPIRITS](http://www.ggene.jp/spirits/)
  - [SD Gundam GGENERATION WARS](http://www.ggene.jp/wars/)
  - [SD Gundam GGENERATION WORLD](http://www.ggene.jp/world/)
  - [SD Gundam GGENERATION 3D](http://www.ggene.jp/3D/)
  - [SD Gundam GGENERATION OVER
    WORLD](http://pgdp.channel.or.jp/gundam/ggow/index.html)
  - [SD Gundam GGENERATION GATHER
    BEAT](http://www.swan.channel.or.jp/swan/software/line_up/index_GND04.html)
  - [SD Gundam GGENERATION GATHER
    BEAT 2](http://www.swan.channel.or.jp/swan/software/line_up/index_GND08.html)
  - [SD Gundam GGENERATION
    獨眼高達](http://www.swan.channel.or.jp/swan/software/line_up/index_GND12.html)
  - [SD Gundam GGENERATION
    ADVANCE](http://www.bandaigames.channel.or.jp/list/ggba/)
  - [SD Gundam GGENERATION
    DS](http://www.bandaigames.channel.or.jp/list/ds_gene/)
  - [SD Gundam GGENERATION CROSS DRIVE](http://www.ggene.jp/crossdrive/)
  - [SD Gundam GGENERATION
    MOBILE](http://www.bandainamcogames.co.jp/mobile/content/appli.php?appli_id=2522&site_id=282&carrier=1)
  - [SD Gundam
    GGENERATION-DA](http://www.gundam.channel.or.jp/goods/videogame/pcggeneda/)
  - [SD Gundam GGENERATION GENESIS](http://ggg.ggame.jp/)
  - [SD Gundam GGENERATION CROSS RAYS](https://ggcr.ggame.jp/)

[G Generation](../Category/SD_GUNDAM.md "wikilink")
[Category:战略游戏](../Category/战略游戏.md "wikilink")
[Category:GUNDAM系列跨界作品](../Category/GUNDAM系列跨界作品.md "wikilink")
[Category:GUNDAM系列電子遊戲](../Category/GUNDAM系列電子遊戲.md "wikilink")
[Category:電子遊戲跨界作品](../Category/電子遊戲跨界作品.md "wikilink")
[Category:日本開發電子遊戲](../Category/日本開發電子遊戲.md "wikilink")