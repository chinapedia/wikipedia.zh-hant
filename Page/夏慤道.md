[C360_2011-08-02_15-16-59.jpg](https://zh.wikipedia.org/wiki/File:C360_2011-08-02_15-16-59.jpg "fig:C360_2011-08-02_15-16-59.jpg")
[Harcourt_Road_west_section_at_night.JPG](https://zh.wikipedia.org/wiki/File:Harcourt_Road_west_section_at_night.JPG "fig:Harcourt_Road_west_section_at_night.JPG")
[Harcourt_Road_at_night_(revised).jpg](https://zh.wikipedia.org/wiki/File:Harcourt_Road_at_night_\(revised\).jpg "fig:Harcourt_Road_at_night_(revised).jpg")
**夏慤道**（[英文](../Page/英文.md "wikilink")：**Harcourt
Road**）是[香港](../Page/香港.md "wikilink")[中西區一條主要幹線](../Page/中西區_\(香港\).md "wikilink")，位於[香港島](../Page/香港島.md "wikilink")[金鐘](../Page/金鐘.md "wikilink")，在新[填海前部分是](../Page/填海.md "wikilink")（[添馬艦](../Page/添馬艦_\(香港\).md "wikilink")）沿岸，連接[灣仔](../Page/灣仔.md "wikilink")[告士打道及](../Page/告士打道.md "wikilink")[中環](../Page/中環.md "wikilink")[-{干}-諾道中](../Page/干諾道中.md "wikilink")。全線為三線雙程分隔道路，設有香港首條高架[立交行車天橋](../Page/行車天橋.md "wikilink")。

## 歷史

[第二次世界大戰之前](../Page/第二次世界大戰.md "wikilink")，[灣仔](../Page/灣仔.md "wikilink")[告士打道和](../Page/告士打道.md "wikilink")[中環](../Page/中環.md "wikilink")-{干}-諾道中被[英國](../Page/英國.md "wikilink")[海軍船塢分隔開](../Page/海軍.md "wikilink")，來往兩地的車輛需繞道[金鐘道](../Page/金鐘道.md "wikilink")，頗為不便。1950年代，[香港政府於](../Page/香港政府.md "wikilink")[英國](../Page/英國.md "wikilink")[海軍船塢對出海面](../Page/海軍.md "wikilink")[填海](../Page/填海.md "wikilink")，即原本英國軍艦[添馬艦停泊之處](../Page/添馬艦_\(軍艦\).md "wikilink")。1959年11月，船塢遷至填海地段後，原有地段就開闢成新路，於1961年落成通車。

夏慤道通車初期經常發生交通意外，在1962年某六個月曾發生十三宗車禍，肇因皆為車輛駛經彎位時速度過快，故當局在1962年8月13日起於夏慤道豎設車速限制路牌，限制車輛駛經彎位時車速不得超過時速35英里（約時速55公里），為香港第一條車速限制的道路。

夏慤道曾經是[4號幹線的一部份](../Page/香港4號幹線.md "wikilink")。2019年[中環灣仔繞道通車後](../Page/中環灣仔繞道.md "wikilink")，取代夏慤道成為4號幹線一部份。

## 名稱

夏慤道是以於第二次世界大戰後率領[英軍艦隊從](../Page/英軍.md "wikilink")[日本接收返回](../Page/日本.md "wikilink")[香港的](../Page/香港.md "wikilink")[海軍](../Page/海軍.md "wikilink")[少將](../Page/少將.md "wikilink")[夏慤](../Page/夏慤.md "wikilink")（H.J.Harcourt）而命名。當中的「慤」字[粵音同](../Page/粵語.md "wikilink")「確」，字型結構為上「殼」下「心」，亦有不少人使用其異體字「-{愨}-」，即「**夏-{愨}-道**」。

## 夏慤道天橋

**夏慤道天橋**是香港島首條高架行車天橋。

為疏導[皇后大道東與](../Page/皇后大道東.md "wikilink")[花園道交界一帶的交通樽頸](../Page/花園道.md "wikilink")，當局在1964年中落實「花園道交通總匯計劃」，修建與花園道平行的木棉道（後改稱[紅棉路](../Page/紅棉路.md "wikilink")），並同時在夏慤道西行綫修築行車天橋，供非轉入木棉道的西行車輛使用。該天橋於1964年10月動工，並在1966年3月落成，4月19日上午十時正式通車。該橋三合土橋躉十二個，全長960呎，路面闊24呎，設兩條西行行車綫，橋頂更設有巴士站，為當時全港首創。

隨著[中環和](../Page/中環.md "wikilink")[金鐘一帶不斷發展](../Page/金鐘.md "wikilink")，為解決日益擠塞的交通，[香港政府在](../Page/香港.md "wikilink")1980年代中實行「干諾道改善工程計劃」，興建[畢打街隧道及](../Page/畢打街隧道.md "wikilink")[林士街天橋](../Page/林士街天橋.md "wikilink")，並決定將只設西行的夏慤道天橋重建為雙程橋，以營造無交通燈阻隔的主幹道路，連接港島東西兩端，路政署於1986年9月8日將畢打街隧道與夏慤道天橋重建工程的建造合約批予基利（香港）有限公司，該合約價值港幣一億九千七百萬元；重建工程隨即展開。新橋在1989年落成，為東、西行雙程行車，並增設由夏慤道西行通往紅棉路的分支橋，而巴士站則被取消，行車天橋橫跨[海富中心與](../Page/海富中心.md "wikilink")[香港大會堂之間的一段夏慤道](../Page/香港大會堂.md "wikilink")，下層設有交匯處，連接[金鐘及](../Page/金鐘.md "wikilink")[半山區](../Page/半山區.md "wikilink")。

## 事件

[Umbrella_Revolution_Harcourt_Road_View_20141028.jpg](https://zh.wikipedia.org/wiki/File:Umbrella_Revolution_Harcourt_Road_View_20141028.jpg "fig:Umbrella_Revolution_Harcourt_Road_View_20141028.jpg")一個月，示威者繼續[佔領夏慤道近海富天橋一帶](../Page/佔領.md "wikilink")\]\]
[Harcourt_Road_under_Umbrella_Movement_20141101.jpg](https://zh.wikipedia.org/wiki/File:Harcourt_Road_under_Umbrella_Movement_20141101.jpg "fig:Harcourt_Road_under_Umbrella_Movement_20141101.jpg")

2014年9月28日至12月11日，香港發生[雨傘運動](../Page/雨傘運動.md "wikilink")，其中夏慤道被示威者[佔領逾](../Page/佔領.md "wikilink")2個月，成為[香港佔領區的金鐘佔領區的主要地段](../Page/雨傘運動佔領.md "wikilink")。

<File:Tamar> Zone Substation
201308.jpg|香港電燈[金鐘夏慤道電力站大廈](../Page/金鐘.md "wikilink")
<File:Harcourt> Road View
201308.jpg|夏慤道沿路都是甲級商業[寫字樓](../Page/寫字樓.md "wikilink")
<File:HK_HKRedCrossHQ_Admiralty.JPG>|[香港紅十字會總辦事處](../Page/香港紅十字會.md "wikilink")
<File:Harcourt> Road View1
201308.jpg|[分域碼頭街通往夏慤道之行車天橋](../Page/分域碼頭街.md "wikilink")
<File:Harcourt> Road Flyover East
Entrance.jpg|夏慤道通往[干諾道中之行車天橋](../Page/干諾道中.md "wikilink")

## 鄰近

  - [金鐘站](../Page/金鐘站.md "wikilink")
  - [龍匯道](../Page/龍匯道.md "wikilink")
  - [夏慤花園](../Page/夏慤花園.md "wikilink")
  - [添美道](../Page/添美道.md "wikilink")
  - [和記大廈](../Page/和記大廈.md "wikilink")
  - [美國銀行中心](../Page/美國銀行中心.md "wikilink")
  - [政府總部](../Page/政府總部.md "wikilink")
  - [遠東金融中心](../Page/遠東金融中心.md "wikilink")
  - [海富中心](../Page/海富中心.md "wikilink")
  - 前[香港紅十字會總辦事處](../Page/香港紅十字會.md "wikilink")

## 相關

  - [添馬艦發展工程](../Page/添馬艦發展工程.md "wikilink")

[Category:金鐘街道](../Category/金鐘街道.md "wikilink")
[Category:冠以人名的香港道路](../Category/冠以人名的香港道路.md "wikilink")
[Category:紅色公共小巴可行駛的幹線街道](../Category/紅色公共小巴可行駛的幹線街道.md "wikilink")
[Category:曾經的香港4號幹線路段](../Category/曾經的香港4號幹線路段.md "wikilink")