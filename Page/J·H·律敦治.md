**傑汗智·賀穆士治·律敦治**，[CBE](../Page/英帝國司令勳章.md "wikilink")，[JP](../Page/太平紳士.md "wikilink")<small></small>（，），[香港](../Page/香港.md "wikilink")[企業家和](../Page/企業家.md "wikilink")[慈善家](../Page/慈善家.md "wikilink")，是來自[印度](../Page/英屬印度.md "wikilink")[孟買的](../Page/孟買.md "wikilink")[帕西人](../Page/帕西人.md "wikilink")，1948年參與創立[香港防癆會](../Page/香港防癆會.md "wikilink")，另分別於1949年、1956年和1957年先後創立[律敦治療養院](../Page/律敦治療養院.md "wikilink")（[律敦治醫院前身](../Page/律敦治醫院.md "wikilink")）、[傅麗儀療養院和](../Page/傅麗儀療養院.md "wikilink")[葛量洪醫院](../Page/葛量洪醫院.md "wikilink")。

律敦治1892年跟隨母親來港，父親則早於1884年來港經營[洋酒](../Page/酒.md "wikilink")[進出口貿易](../Page/貿易.md "wikilink")，而且頗為成功。從聖若瑟書院畢業後\[1\]，他協助父親打理業務，到1913年還因為父親退休而正式接手全盤生意。他後來進一步涉足[地產](../Page/地產.md "wikilink")，在[中環](../Page/中環.md "wikilink")、[尖沙咀](../Page/尖沙咀.md "wikilink")、[深水埗和](../Page/深水埗.md "wikilink")[深井等地購入多幅土地](../Page/深井.md "wikilink")。1930年，律敦治創立[香港啤酒廠有限公司](../Page/香港啤酒廠有限公司.md "wikilink")，選址深井興建[啤酒廠](../Page/啤酒.md "wikilink")，生產以HB啤為品牌的本地啤酒，啤酒廠在[香港淪陷期間一度停產](../Page/香港淪陷.md "wikilink")，但戰後旋在律敦治主持下恢復運作。惟他不久以後於1947年決定出售啤酒廠，該廠隨後於1948年成為[生力啤啤酒廠](../Page/生力啤.md "wikilink")。

香港淪陷期間，律敦治接濟不少香港帕西人和印度人，又與兒子[鄧律敦治暗中安排把食物偷運入獄](../Page/鄧律敦治.md "wikilink")。後來他們的行為被日方揭發，結果在1944年被捕下獄，並且受盡折磨，其後再被判監五年，到1945年[香港重光始獲提早釋放](../Page/香港重光.md "wikilink")。1944年，他的次女蒂美因[肺結核病逝](../Page/肺結核.md "wikilink")，對他構成很大的打擊。為了紀念次女和向社會大眾宣揚防治肺結核的重要性，他在戰後促成防癆會成立，及後又進一步捐出鉅款創立律敦治療養院、傅麗儀療養院和葛量洪醫院，當中傅麗儀療養院乃紀念1952年因[癌病逝世的幼女傅麗儀而命名](../Page/癌病.md "wikilink")。歷年來，他對防癆會和各家院所的累積個人捐款高達200萬港元。律敦治身後，家族業務和慈善公益事務由其子鄧律敦治接手打理。鄧律敦治歷任[市政局及](../Page/香港市政局.md "wikilink")[立法局非官守議員等公職](../Page/香港立法局.md "wikilink")，1962年至1968年任立法局[首席非官守議員](../Page/首席非官守議員.md "wikilink")。

## 生平

### 早年生涯

律敦治是[帕西人](../Page/帕西人.md "wikilink")，1880年10月30日生於[英屬印度](../Page/英屬印度.md "wikilink")[孟買省](../Page/孟買管轄區.md "wikilink")[布爾薩](../Page/瓦爾薩德.md "wikilink")（[印度](../Page/印度.md "wikilink")[古吉拉特邦](../Page/古吉拉特邦.md "wikilink")[瓦爾薩德前身](../Page/瓦爾薩德.md "wikilink")）。\[2\]他的家族世代信奉[祆教](../Page/祆教.md "wikilink")，先世為逃避[伊斯蘭教的](../Page/伊斯蘭教.md "wikilink")[宗教迫害而隨其他祆教徒從](../Page/宗教迫害.md "wikilink")[波斯](../Page/波斯.md "wikilink")（[伊朗前身](../Page/伊朗.md "wikilink")）移居[印度半島西部](../Page/印度半島.md "wikilink")，而律敦治的家族則落戶於印度[孟買](../Page/孟買.md "wikilink")。\[3\]律敦治的父母分別名叫[賀穆士治·律敦治](../Page/賀穆士治·律敦治.md "wikilink")（Hormusjee
Ruttonjee）和甸那·律敦治（Dina
Ruttonjee）。\[4\]他的父親於1884年獨自前往[香港發展](../Page/香港.md "wikilink")，經營[洋酒](../Page/酒.md "wikilink")[進出口貿易業務](../Page/貿易.md "wikilink")，後來生意不斷擴展，在貿易界有所成就的他除了獲[香港政府奉委非官守](../Page/香港政府.md "wikilink")[太平紳士以外](../Page/太平紳士.md "wikilink")，還成為了香港的印籍社群僑領之一，並於1926年[印籍後備警察隊成立後出任贊助人一職](../Page/香港輔警.md "wikilink")。\[5\]\[6\]\[7\]\[8\]1892年，有見於父親的業務略有所成，律敦治跟母親一同移居香港生活，\[9\]他早年受教於[域多利亞英文學校](../Page/域多利亞英文學校.md "wikilink")，\[10\]喜愛[木球的他還分別於](../Page/板球.md "wikilink")1897年和1898年連續兩年成為[紀利華木球會平均得分最佳的木球運動員之一](../Page/紀利華木球會.md "wikilink")。\[11\]

### 工商生涯

從聖若瑟書院畢業後\[12\]，律敦治加入父親的律敦治父子洋酒行工作，到1913年還因為父親退休而正式接手打理全盤生意。\[13\]律敦治父子原本依靠進口和代理各國各式洋酒和[波打酒起家](../Page/波打酒.md "wikilink")，除了設總店於[中環](../Page/中環.md "wikilink")[都爹利街以外](../Page/都爹利街.md "wikilink")，[九龍](../Page/九龍.md "wikilink")[尖沙咀也有分店](../Page/尖沙咀.md "wikilink")。\[14\]\[15\]到律敦治接手後，還開始涉足[地產](../Page/地產.md "wikilink")，設立律敦治置業有限公司，分別在[中環](../Page/中環.md "wikilink")、九龍尖沙咀[廣東道海傍](../Page/廣東道.md "wikilink")、[深水埗和](../Page/深水埗.md "wikilink")[新界](../Page/新界.md "wikilink")[深井等地投資買地](../Page/深井.md "wikilink")，\[16\]\[17\]而都爹利街總行更在1925年和1935年分別重建成[律敦行](../Page/律敦行.md "wikilink")（Ruttonjee
House）和[甸那行](../Page/甸那行.md "wikilink")（Dina House），分別以其父母命名。\[18\]

另一方面，從事洋酒進口貿易的律敦治認為香港[啤酒市場有利可圖](../Page/啤酒.md "wikilink")，所以他很早就萌生在香港投資興建廠房以自行釀造啤酒的念頭。\[19\]但事實上，香港最早於1904年已曾經有商家在[北角創立香港啤酒廠公司](../Page/北角.md "wikilink")（Hong
Kong Brewery
Company），其後又有在1907年成立於[跑馬地的帝國啤酒廠](../Page/跑馬地.md "wikilink")（Imperial
Brewing），以及在同一年設於九龍[荔枝角的東方啤酒廠](../Page/荔枝角.md "wikilink")（Oriental
Brewery）。\[20\]不過，香港啤酒廠公司未曾投產便先行倒閉，而帝國啤酒廠和東方啤酒廠也在營運短短數年後便告結業，使得香港市場要繼續依賴外地進口啤酒。\[21\]

雖然如此，在得到[香港總督](../Page/香港總督.md "wikilink")[司徒拔爵士的支持下](../Page/司徒拔.md "wikilink")，律敦治自1920年代初便開始與港府探討設立啤酒廠的可能，但經過多年談判都沒有很大進展。\[22\]\[23\]一直到1930年，律敦治正式創辦[香港啤酒廠有限公司](../Page/香港啤酒廠有限公司.md "wikilink")（Hong
Kong Brewers and Distillers
Limited，與1904年成立的香港啤酒廠公司沒有關係），並邀得英商[天祥洋行](../Page/天祥洋行.md "wikilink")[大班](../Page/大班.md "wikilink")[斯坦利·鐸威路](../Page/斯坦利·鐸威路.md "wikilink")（Stanley
Dodwell）出任公司主席，而作為最大股東的律敦治則任營運總監。\[24\]\[25\]公司成立後，旋於同年11月選址新界[青山公路沿海的深井動工興建啤酒廠](../Page/青山公路.md "wikilink")，而釀酒設備則專門從[捷克斯洛伐克](../Page/捷克斯洛伐克.md "wikilink")[皮爾森的](../Page/皮爾森.md "wikilink")[斯柯達公司訂購](../Page/斯柯達公司.md "wikilink")，務求在香港自家生產正宗[皮爾森啤酒](../Page/皮爾森啤酒.md "wikilink")。\[26\]此外，公司英文名稱雖有「啤酒廠」（Brewers）和「酒房」（Distillers）的含義，但廠房只設啤酒廠、不設酒房；\[27\]至於樓高兩層的啤酒廠則由[利安建築師樓](../Page/利安顧問有限公司.md "wikilink")（Leigh
and
Orange）設計，並直接引用廠房後方山邊的溪水釀酒，初時預計每年可生產7,500桶、合共122萬[公升的啤酒](../Page/公升.md "wikilink")。\[28\]

1933年8月，啤酒廠正式落成啟用，由時任[駐華英軍司令](../Page/駐港英軍司令.md "wikilink")[波老少將夫人主持啟用儀式](../Page/波老.md "wikilink")，出席嘉賓多達600人，酒廠生產的啤酒名叫HB啤，縮寫「HB」有「Hongkong
Brewers」的意思，並主要聘用深井[原居民擔任廠房職工](../Page/香港原居民.md "wikilink")。\[29\]\[30\]\[31\]啤酒廠成立之初，律敦治原計劃以低於進口啤酒一半的價格出售由他生產的本地啤酒，可是，當時正值環球經濟[大蕭條](../Page/大蕭條.md "wikilink")，再加上香港奉行[銀本位的政策造成](../Page/銀本位.md "wikilink")[港元價格持續高企](../Page/港元.md "wikilink")，導致從英國等地進口的啤酒售價急降，價格甚至可與本地啤酒看齊。\[32\]\[33\]同時間，港元價格高企也不利本地啤酒的外銷競爭力，對律敦治的生意業務構成沉重打擊。\[34\]兩年多後，律敦治難抵[外匯價格急劇波動](../Page/外匯.md "wikilink")，終於在1935年12月宣佈把公司自行清盤，但同時展開重組，以及繼續維持公司的正常業務運作。\[35\]翌年，律敦治另組新公司從清盤人手上買回啤酒廠，新公司的中文名稱同樣是香港啤酒廠有限公司，但英文名稱則由原來的「Hong
Kong Brewers and Distillers Limited」稍微改成為「Hong Kong Brewery and
Distillery Limited」。\[36\]新公司成立後，律敦治也繼續擔任營運總監一職。\[37\]

重組後的香港啤酒廠平穩發展，並在1939年高調慶祝成立六周年紀念，而[第二次世界大戰在](../Page/第二次世界大戰.md "wikilink")1939年爆發後，廠房仍舊繼續生產啤酒。\[38\]然而，1941年12月[香港淪陷後](../Page/香港淪陷.md "wikilink")，啤酒廠與香港不少企業一樣遭到日方接管，直到1945年8月[香港重光](../Page/香港重光.md "wikilink")，律敦治才得以在同年9月在[皇家海軍人員陪同下重返啤酒廠視察廠房的損壞情況](../Page/英國皇家海軍.md "wikilink")。\[39\]戰後，律敦治重整香港啤酒廠的業務，廠房得以在短時間內恢復生產，以1946年9月為例，由律敦治生產的HB啤在普通店舖和[酒吧的標售價格分別是每](../Page/酒吧.md "wikilink")[品脫](../Page/品脫.md "wikilink")1.1港元和1.5港元；相對其他像[嘉士伯等進口啤酒品牌在酒吧定價每品脫](../Page/嘉士伯.md "wikilink")1.7港元，售價相對便宜。\[40\]可是，戰後把目光投放到慈善事業的律敦治卻在1947年決定淡出，並作價600萬港元把啤酒廠賣盤予[菲律賓啤酒商](../Page/菲律賓.md "wikilink")[安德士·蘇理安諾上校](../Page/安德士·蘇理安諾.md "wikilink")（Colonel
Andres
Soriano），由律敦治生產的HB啤遂成為歷史。\[41\]據說，律敦治當時與蘇理安諾僅僅商談了一個小時，便達成了賣盤協議。\[42\]原本由律敦治擁有位於深井的啤酒廠遂於1948年5月起正式成為了[生力啤廠房](../Page/生力啤.md "wikilink")，該址地皮多年後於1996年售出，並重建成大型屋苑[碧堤半島](../Page/碧堤半島.md "wikilink")。\[43\]

### 防癆公益

[HK_WC_Ruttonjee_Hospital_TCNHDA_Headquarters.jpg](https://zh.wikipedia.org/wiki/File:HK_WC_Ruttonjee_Hospital_TCNHDA_Headquarters.jpg "fig:HK_WC_Ruttonjee_Hospital_TCNHDA_Headquarters.jpg")會址\]\]
[Ruttonjee_Hospital_(2015).jpg](https://zh.wikipedia.org/wiki/File:Ruttonjee_Hospital_\(2015\).jpg "fig:Ruttonjee_Hospital_(2015).jpg")於1991年重建為[律敦治醫院](../Page/律敦治醫院.md "wikilink")（圖）\]\]
1941年香港淪陷後，開展了前後三年零八個月的[日治時期](../Page/香港日治時期.md "wikilink")，期間，律敦治的生意業務雖然一落千丈，但他積極與兒子[鄧律敦治接濟香港的帕西和](../Page/鄧律敦治.md "wikilink")[印度社群](../Page/印度人.md "wikilink")，律敦治一家位於[中環](../Page/中環.md "wikilink")[都爹利街甸那行的居所更差不多收容了所有居港帕西人](../Page/都爹利街.md "wikilink")，為數多達40至50人，另外又收容數名英國和華籍朋友。\[44\]\[45\]同時間，他還與兒子暗中安排把食物和日用物資偷運進監獄，接濟被囚於獄中的印籍英兵和其他[英軍和](../Page/英軍.md "wikilink")[加拿大戰俘](../Page/加拿大.md "wikilink")。\[46\]\[47\]日治政府一直對律敦治一家十分猜忌，又對律敦治父子拒絕號召香港的帕西社群積極與日治政府合作感到十分不滿。\[48\]\[49\]1942年，日治政府更一度派兵守住他們的居所，為時數星期，嚴防有人組織[反日活動](../Page/反日.md "wikilink")。\[50\]1944年，律敦治兩父子偷運物資入監獄的活動終被[日軍憲兵隊揭發](../Page/日本憲兵.md "wikilink")，兩人旋遭拘捕下獄前後九個月，期間受盡日軍折磨。\[51\]\[52\]\[53\]及後經日方審訊，兩人再被指從事反日活動而被判監五年，到1945年8月香港重光後始從[赤柱監獄提早獲釋](../Page/赤柱監獄.md "wikilink")。\[54\]\[55\]

被日軍囚禁期間，律敦治的次女蒂美還於1944年6月因[肺結核病逝](../Page/肺結核.md "wikilink")，對他構成沉重打擊。\[56\]為了紀念逝世的女兒，律敦治戰後銳意提倡成立[香港防癆會](../Page/香港防癆會.md "wikilink")，並與[周錫年](../Page/周錫年.md "wikilink")、[顏成坤](../Page/顏成坤.md "wikilink")、[岑維休](../Page/岑維休.md "wikilink")、[賓臣和](../Page/賓臣_\(香港銀行家\).md "wikilink")[李耀祥等中外士紳於](../Page/李耀祥.md "wikilink")1948年10月正式成立防癆會，由周錫年任創會主席，負責向社會大眾宣揚防治肺結核的重要性。\[57\]翌年，律敦治更進一步捐出鉅款50萬港元，一力促成港府把位於[灣仔的前](../Page/灣仔.md "wikilink")[皇家海軍醫院改建成為香港首家專治肺結核的](../Page/皇家海軍醫院.md "wikilink")[律敦治療養院](../Page/律敦治療養院.md "wikilink")（Ruttonjee
Sanatorium），免費服務普通香港市民。\[58\]\[59\]\[60\]其後，他再獨力捐出25萬港元善款，用以擴建療養院，使總捐款增至約80萬港元。\[61\]另外，為了物色專業人士打理療養院，律敦治成功與[愛爾蘭](../Page/愛爾蘭.md "wikilink")[聖高隆龐傳教女修會](../Page/聖高隆龐傳教女修會.md "wikilink")（St.
Columban Mission of
Ireland）達成協議，由修會於1949年派出首批以[區桂蘭修女](../Page/區桂蘭.md "wikilink")（Sister
Mary Aquinas
Monaghan）為首的專業醫護人員來港，負責療養院的日常運作。\[62\]\[63\]隨著時代轉變，聖高隆龐修會一直到1988年才卸下營運療養院的角色。\[64\]

除了律敦治療養院，律敦治還聯同防癆會在1956年於[香港島](../Page/香港島.md "wikilink")[半山](../Page/半山區.md "wikilink")[肇輝臺近](../Page/肇輝臺.md "wikilink")[司徒拔道建成](../Page/司徒拔道.md "wikilink")[傅麗儀療養院](../Page/傅麗儀療養院.md "wikilink")（Freni
Memorial Convalescent
Home），以紀念在1952年因[癌病病逝的幼女傅麗儀](../Page/癌病.md "wikilink")。\[65\]其後在1957年，同樣由律敦治出資興建、並設有625張病床的[葛量洪醫院也在](../Page/葛量洪醫院.md "wikilink")[黃竹坑落成啟用](../Page/黃竹坑.md "wikilink")。\[66\]傅麗儀療養院專門為肺結核病康復者提供復康服務，\[67\]而葛量洪醫院則是一所胸肺科專科醫院。\[68\]\[69\]有別於律敦治療養院和傅麗儀療養院，葛量洪醫院早年需要向住院病人收取部份費用，用以支持兩家療養院的營運開支。\[70\]歷年來，律敦治對防癆會、律敦治療養院、傅麗儀療養院和葛量洪醫院持續作出捐款，累積個人捐款總額高達200萬港元，以當時來計是相當高的金額。\[71\]\[72\]為了肯定他在慈善公益方面的表現，以及嘉許他在日治時期對英國展現的「勇氣和忠誠服務」，他在1947年獲英廷頒授[CBE勳銜](../Page/英帝國司令勳章.md "wikilink")，\[73\]其後也獲得港府奉委非官守[太平紳士](../Page/太平紳士.md "wikilink")。\[74\]

### 晚年生涯

律敦治在二戰後雖然繼續擔任律敦治洋酒行和律敦治置業有限公司的董事會主席，但年事漸高，業務已逐漸交由兒子[鄧律敦治打理](../Page/鄧律敦治.md "wikilink")。\[75\]晚年的律敦治專注於防癆工作和其他公益慈善事業，除了擔任[香港保護兒童會副主席](../Page/香港保護兒童會.md "wikilink")、主席和[香港國殤紀念基金委員會委員等職以外](../Page/香港國殤紀念基金委員會.md "wikilink")，也繼續擔任防癆會董事、律敦治療養院及葛量洪醫院兩家院所的管治委員會主席以及[香港大學校董等職](../Page/香港大學.md "wikilink")，1952年至1958年又任[香港紅十字會第四會長](../Page/香港紅十字會.md "wikilink")。\[76\]\[77\]\[78\]

[Mhe_small_copy.jpg](https://zh.wikipedia.org/wiki/File:Mhe_small_copy.jpg "fig:Mhe_small_copy.jpg")的興建\]\]
另一方面，律敦治特別關注香港戰後房屋不足的問題，鑑於當時港府仍未有大型的[公營房屋興建計劃](../Page/香港公營房屋.md "wikilink")，他早於1948年至1951年出任[香港房屋協會主席](../Page/香港房屋協會.md "wikilink")，\[79\]而且於1950年聯同紳商[周錫年等人成立](../Page/周錫年.md "wikilink")[香港模範屋宇會](../Page/香港模範屋宇會.md "wikilink")，並擔任主席一職。\[80\]在港府撥地和[香港上海匯豐銀行出資支持下](../Page/香港上海匯豐銀行.md "wikilink")，模範屋宇會於1951年至1956年分期建成[北角](../Page/北角.md "wikilink")[模範邨](../Page/模範邨.md "wikilink")，是為香港早期的[廉租屋邨](../Page/廉租屋邨.md "wikilink")。\[81\]1952年，律敦治再與周錫年、[李耀祥](../Page/李耀祥.md "wikilink")、[顏成坤和](../Page/顏成坤.md "wikilink")[馮秉芬等人籌組](../Page/馮秉芬.md "wikilink")[香港平民屋宇有限公司](../Page/香港平民屋宇有限公司.md "wikilink")，並任董事一職，透過興建平民屋出租予有需要的居民。\[82\]

律敦治晚年身體健康倒退，1957年以後淡出公開場合。\[83\]1960年2月10日，他於赤柱別墅因[心臟病突發病逝](../Page/心臟病.md "wikilink")，終年79歲，按照帕西人傳統，他的遺體旋於翌日下午五時於[跑馬地](../Page/跑馬地.md "wikilink")[祆教墳場舉行安葬儀式](../Page/祆教墳場.md "wikilink")。\[84\]\[85\]有鑑於律敦治生前積極投身慈善公益，再加上是香港的印度社群僑領，因此當日親臨葬禮致哀者包括時任[港督](../Page/港督.md "wikilink")[柏立基爵士](../Page/柏立基.md "wikilink")、[輔政司](../Page/香港輔政司.md "wikilink")[白嘉時](../Page/白嘉時.md "wikilink")、副輔政司[戴斯德](../Page/戴斯德.md "wikilink")、[駐港英軍司令](../Page/駐港英軍.md "wikilink")[巴斯田少將](../Page/巴斯田.md "wikilink")、[財政司](../Page/香港財政司.md "wikilink")[歧樂嘉](../Page/歧樂嘉.md "wikilink")、[華民政務司](../Page/華民政務司.md "wikilink")[石智益](../Page/石智益.md "wikilink")、醫務總監[麥敬時醫生和](../Page/麥敬時.md "wikilink")[教育司](../Page/教育司.md "wikilink")[高詩雅等多名港府高官](../Page/高詩雅.md "wikilink")、另外還有多名兩局議員以及政商界人士等數百人。\[86\]葬禮儀式簡單而莊重，由港督柏立基爵士主持撒土儀式，以示悼念。\[87\]

律敦治身後，其子鄧律敦治接手打理家族業務，以及繼續其父生前的慈善公益事業。\[88\]鄧律敦治於1974年逝世後，家族業務和慈善工作再由律敦治的外孫女維拉·律敦治-荻茜和侄兒[勞士·施羅孚等人主持](../Page/勞士·施羅孚.md "wikilink")，\[89\]而原本由律敦治家族持有的律敦行和甸那行則在1980年代拆卸重建成為[律敦治中心](../Page/律敦治中心.md "wikilink")，中心分為兩期，分別是第一期的律敦治大廈和第二期的帝納大廈。\[90\]到1988年，家族後人正式結束洋酒貿易業務，以便把焦點投放到房地產業務。\[91\]

此外，隨著醫學倡明，加上肺結核病逐漸得到有效控制，肺結核病患者日益減少，律敦治生前參與創立的香港防癆會遂於1967年改名為香港防癆及胸病協會，1980年再正式改名為[香港防癆心臟及胸病協會](../Page/香港防癆心臟及胸病協會.md "wikilink")，以反映協會關注的範疇已進一步推廣至[心臟病和胸病](../Page/心臟病.md "wikilink")。\[92\]至於律敦治療養院後來於1991年重建成為[律敦治醫院](../Page/律敦治醫院.md "wikilink")，並擴充成為一所擁有600張病床的全科醫院；\[93\]而葛量洪醫院則由原來的胸肺科醫院，到1980年代擴充成為一所心臟及胸肺科專科醫院。\[94\]兩所醫院原本都是由防癆會打理，並且是由政府撥款的補助醫院，一直到1991年才正式交由由港府設立的[醫院管理局營運](../Page/醫院管理局.md "wikilink")，成為公立醫院。\[95\]\[96\]1999年，傅麗儀療養院重建成為傅麗儀護理安老院，但繼續由防癆會營辦。\[97\]

## 個人生活

[Ruttonjee_family.jpg](https://zh.wikipedia.org/wiki/File:Ruttonjee_family.jpg "fig:Ruttonjee_family.jpg")，律敦治夫人手抱的女嬰是律敦治的次女蒂美，坐在地上的女童是長女科雪芙，攝於1908年\]\]
[HK_Ruttonjee_Hospital_J_H_Ruttonjee_CBE_1880-1960.jpg](https://zh.wikipedia.org/wiki/File:HK_Ruttonjee_Hospital_J_H_Ruttonjee_CBE_1880-1960.jpg "fig:HK_Ruttonjee_Hospital_J_H_Ruttonjee_CBE_1880-1960.jpg")大堂展示一幅律敦治紀念碑\]\]
律敦治在1902年於[印度迎娶巴努](../Page/英屬印度.md "wikilink")·馬斯特（Banoo
Master，1884年－1972年）為妻，\[98\]\[99\]兩人育有一子三女，皆在香港出生：\[100\]

  - [鄧律敦治](../Page/鄧律敦治.md "wikilink")（Dhun
    Ruttonjee，1903年－1974年），律敦治之子，曾任[香港立法局](../Page/香港立法局.md "wikilink")[首席非官守議員](../Page/首席非官守議員.md "wikilink")，並獲封[CBE勳銜和非官守](../Page/英帝國司令勳章.md "wikilink")[太平紳士等名銜](../Page/太平紳士.md "wikilink")；\[101\]\[102\]有華籍妻子安妮·律敦治（Anne
    Ruttonjee，1913年－2009年），同為非官守太平紳士。\[103\]兩人育有一名養女，名叫瑞蓮·律敦治（Shirley
    Ruttonjee），由安妮於前一段婚姻所生。\[104\]\[105\]
  - 科雪芙·律敦治（Khorshev Ruttonjee），律敦治長女，早年逝世。\[106\]
  - 蒂美·律敦治（Tehmi Ruttonjee，1907年－1944年），律敦治次女，1925年嫁給魯斯圖姆·艾都治·荻茜（Rustom
    Eduljee Desai，1895年－1985年），兩人育有一女，名叫維拉·律敦治-荻茜（Vera
    Ruttonjee-Desai，1926年－2014年）。\[107\]\[108\]\[109\]蒂美於1944年死於[肺結核](../Page/肺結核.md "wikilink")。\[110\]
  - 傅麗儀·律敦治（Freni
    Ruttonjee，？－1952年），律敦治幼女，1945年嫁給[勞士·施羅孚](../Page/勞士·施羅孚.md "wikilink")（Rusy
    Shroff，1917年－2017年），1952年因[癌症病逝](../Page/癌症.md "wikilink")。\[111\]\[112\]

律敦治生前還收養了兩名姪兒和一名姪女，分別是勞士·施羅孚、拜吉·施羅孚（Beji Shroff）和米妮·施羅孚（Minnie
Shroff），三人的父親在一次[颱風吹襲期間遇上海難身亡](../Page/颱風.md "wikilink")。\[113\]律敦治一家以[中環](../Page/中環.md "wikilink")[都爹利街的律敦行和甸那行作為營商和居住的地方](../Page/都爹利街.md "wikilink")；\[114\]此外，律敦治在1930年代選址於[新界](../Page/新界.md "wikilink")[深井興建啤酒廠時](../Page/深井.md "wikilink")，在附近[青山公路購地興建一座命名為](../Page/青山公路.md "wikilink")「賀米園」（Homi
Villa）的白色別墅。\[115\]別墅在二戰後被港府購入作為高級公務員宿舍，[夏鼎基爵士在任](../Page/夏鼎基.md "wikilink")[財政司期間曾遷入別墅居住](../Page/香港財政司.md "wikilink")。\[116\]1995年，港府把別墅闢作[機場核心計劃展覽中心](../Page/機場核心計劃展覽中心.md "wikilink")，並於1996年開放，成為一座以《[香港機場核心計劃](../Page/香港機場核心計劃.md "wikilink")》為主題的展覽館。\[117\]除了「賀米園」，律敦治在[赤柱也有一座別墅](../Page/赤柱.md "wikilink")，建於1949年，以其妻命名為「巴努園」（Banoo
Villa），別墅於1987年由家族後人售出作重新發展。\[118\]\[119\]

律敦治二戰後於1948年偕妻子巴努和外孫女維拉環遊[歐洲和](../Page/歐洲.md "wikilink")[美國](../Page/美國.md "wikilink")，行程重點包括在[倫敦](../Page/倫敦.md "wikilink")[白金漢宮舉行的皇家園遊會獲引見英皇](../Page/白金漢宮.md "wikilink")[喬治六世](../Page/喬治六世.md "wikilink")、[伊利沙伯皇后和](../Page/伊利沙伯皇太后.md "wikilink")[瑪麗皇太后](../Page/特克的玛丽.md "wikilink")，另外又獲時任[首相](../Page/英國首相.md "wikilink")[艾德禮接見談論香港近況](../Page/艾德禮.md "wikilink")。\[120\]十分重視[宗教的律敦治在到訪](../Page/宗教.md "wikilink")[梵蒂岡期間](../Page/梵蒂岡.md "wikilink")，也獲時任[教宗](../Page/教宗.md "wikilink")[庇護十二世親自單獨接見](../Page/庇護十二世.md "wikilink")。\[121\]1952年，律敦治夫婦於[半島酒店慶祝金婚](../Page/半島酒店.md "wikilink")，並廣邀友好，出席賓客達700人，成為一時佳話。\[122\]

## 榮譽

### 殊勳

  - 以下列出榮譽全稱及縮寫：
      - [英帝國司令勳章](../Page/英帝國司令勳章.md "wikilink")（C.B.E.）
        （1947年元旦授勳名單\[123\]）
      - 非官守[太平紳士](../Page/太平紳士.md "wikilink")（J.P.）\[124\]

### 以他命名的事物

  - [律敦治醫院](../Page/律敦治醫院.md "wikilink")（Ruttonjee
    Hospital）：位於[香港島](../Page/香港島.md "wikilink")[灣仔](../Page/灣仔.md "wikilink")，原本是啟用於1949年的[律敦治療養院](../Page/律敦治療養院.md "wikilink")（Ruttonjee
    Sanatorium），1991年重建成為全科醫院。\[125\]

## 相關條目

  - [香港防癆心臟及胸病協會](../Page/香港防癆心臟及胸病協會.md "wikilink")
  - [律敦治療養院](../Page/律敦治療養院.md "wikilink")
  - [葛量洪醫院](../Page/葛量洪醫院.md "wikilink")
  - [鄧律敦治](../Page/鄧律敦治.md "wikilink")
  - [勞士·施羅孚](../Page/勞士·施羅孚.md "wikilink")
  - [帕西人](../Page/帕西人.md "wikilink")
  - [生力啤](../Page/生力啤.md "wikilink")
  - [機場核心計劃展覽中心](../Page/機場核心計劃展覽中心.md "wikilink")

## 注腳

## 參考資料

### 英文資料

  - "Kowloon residents", *The China Mail*, 14 April 1897, p.2.
  - "Desai-Ruttonjee", *The China Mail*, 7 February 1925, p.8.
  - "Hong Kong's New Brewery Opened", *Hong Kong Daily Press*, 17 August
    1933, p.7.
  - "Police Reserve, Indian Company: Headquarters Found: Mr. J. H.
    Ruttonjee Gives Premises Rent-Free", *The China Mail*, 26 September
    1935, p.10.
  - "Tributes to Sterling Police Commissioner", *Hong Kong Daily Press*,
    21 May 1941, p.2.
  - "Craigengower Cricket Club's First Sports Challenge in 1894", *The
    China Mail*, 1 July 1941, p.14.
  - "Personality Parade - Merchant Philanthropist", *The China Mail*, 24
    July 1950, p.2.
  - "Sudden Death of Mr J. H. Ruttonjee", *The South China Morning
    Post*, 11 February 1960, pp.1 & 20.
  - "Late Mr. J. H. Ruttonjee - Wise Philosopher And Good Friend Who
    Will Be Long Remembered", *The South China Morning Post*, 12
    February 1960, p.8.
  - "[Hon. Dhun
    Ruttonjee](https://mmis.hkpl.gov.hk/coverpage/-/coverpage/view?p_r_p_-1078056564_c=QF757YsWv58JCjtBMMIqog1NphfKoGbt)",
    *Time to Remember* \[sound recording\]. Hong Kong: RTHK, 1969.
  - Daswani, Kavita, "Nostalgia aside, family has to go", *The South
    China Morning Post*, 23 August 1987, p.3.
  - White, Barbara-Sue, *Turbans and Traders: Hong Kong's Indian
    Communities*. Hong Kong: Oxford University Press, 1994. ISBN
    978-0-19585-287-5
  - "[Rusy M. Shroff: Global NRI for Dec.
    -'97](https://web.archive.org/web/20160305072317/http://www.zoominfo.com/CachedPage/?archive_id=0&page_id=63301150&page_url=%2F%2Fwww.nriworld.com%2Fachievers%2Fglobal%2Fdec97%2Fglbaldec.htm&page_last_updated=2002-07-11T07%3A12%3A34)",
    *Global NRI*, 1997.
  - Chu, Yik-yi, Cindy, *[Foreign Communities in Hong
    Kong, 1840s-1950s](https://books.google.com.hk/books?id=PUfHAAAAQBAJ)*.
    New York: Palgrave Macmillan, 2005. ISBN 978-1-40398-055-7
  - Starling, A. E., *[Plague, SARS and the Story of Medicine in Hong
    Kong](https://books.google.com.hk/books?id=WBx6McA35iYC)*. Hong
    Kong: Hong Kong University Press, 2006. ISBN 978-9-62209-805-3
  - Shroff-Gander, Sooni, "Ruttonjee, Dhun", *Dictionary of Hong Kong
    Biography*. Hong Kong: Hong Kong University Press, 2012. ISBN
    978-9-88808-366-4
  - Shroff-Gander, Sooni, "Ruttonjee, Jehangir Hormusjee", *Dictionary
    of Hong Kong Biography*. Hong Kong: Hong Kong University Press,
    2012. ISBN 978-9-88808-366-4
  - "[Roll out the
    barrel](http://www.scmp.com/magazines/post-magazine/article/1298073/roll-out-barrel)",
    *Post Magazine*, 25 August 2013.
  - "Historic Building Appraisal: Homi Villa, Castle Peak Road, Tsuen
    Wan, N.T.", *[Historic Building
    Appraisal](https://web.archive.org/web/20130922201512/http://www.lcsd.gov.hk/ce/Museum/Monument/form/Brief_Information_on_proposed_Grade_III_Items.pdf)*.
    Hong Kong: Antiquities Advisory Board, retrieved on 25 May 2015.
  - "[Ruttonjee, Anne
    Dhun](https://webb-site.com/dbpub/natperson.asp?p=40436)",
    *Webb-site Who's Who*, retrieved on 25 May 2015.
  - "[Ruttonjee,
    Shirley](https://webb-site.com/dbpub/natperson.asp?p=50555)",
    *Webb-site Who's Who*, retrieved on 25 May 2015.
  - "[Ruttonjee-Desai,
    Vera](https://webb-site.com/dbpub/natperson.asp?p=50554)",
    *Webb-site Who's Who*, retrieved on 25 May 2015.
  - "[Rustom Eduljee
    Desai](http://www.findagrave.com/cgi-bin/fg.cgi?page=gr&GSln=Desai&GSfn=Rustom&GSbyrel=all&GSdyrel=all&GSob=n&GRid=62037237&df=all&)",
    *Find A Grave*, retrieved on 25 May 2015.

### 中文資料

  - 〈波打清釀〉，《香港華字日報》第三頁，1906年2月27日。
  - 〈模範屋宇會成立，擬在北角興建平價樓四百層〉，《香港工商日報》第六頁，1950年7月26日。
  - 〈登律敦治夫人周末赴英渡假〉，《華僑日報》第二張第二頁，1951年12月6日。
  - 〈九龍塘村災區地皮，港府宣佈立即收回〉，《大公報》第一張第四版，1952年5月4日。
  - 〈印籍慈善家律敦治逝世〉，《香港工商日報》第五頁，1960年2月12日。
  - 〈訃聞〉，《華僑日報》第二張第二頁，1972年10月7日。
  - 〈前立法局議員慈善家鄧律敦治逝世，遺體今日下午安葬〉，《香港工商日報》第八頁，1974年7月29日。
  - 呂大樂，《[山上之城：香港紅十字會的故事，1950-2000](https://books.google.com.hk/books?id=QSdidEwOUGQC)》。香港：香港大學出版社，2000年。ISBN
    978-9-62209-528-1
  - 〈[律敦治中心變身](http://the-sun.on.cc/channels/fea/20051216/20051215233301_0000.html)〉，《太陽報》，2005年12月16日。
  - 《[60周年紀念特刊](https://web.archive.org/web/20140116114801/http://www.antitb.org.hk/pdf_files/HKC_60th_Ann_Book.pdf)》。香港：香港防癆心臟及胸病協會，2008年。
  - 關慧玲，〈[律敦治與拜火教](http://eastweek.my-magazine.me/index.php?aid=20972)〉，《東周刊》464期，2012年7月18日。
  - 《[香港房屋協會70周年紀念特刊](https://www.hkhs.com/70th-Anniversary-Commemorative-Book/tc/files/assets/common/downloads/Housing_70th_TC.pdf)》。香港：香港房屋協會，2018年。
  - 〈[永遠懷念勞士施羅孚副會長BBS,
    MBE](http://www.antitb.org.hk/tc/news_detail.php?nid=73)〉。香港：香港防癆心臟及胸病協會，造訪於2018年6月2日。

## 外部連結

  - [香港防癆心臟及胸病協會](http://www.ha.org.hk/org/antitb/default.htm)
  - [律敦治醫院](http://www.ha.org.hk/visitor/ha_hosp_details.asp?Content_ID=100144&Lang=CHIB5)
  - [葛量洪醫院](https://www.ha.org.hk/visitor/ha_hosp_details.asp?Content_ID=100139&Lang=CHIB5)
  - [傅麗儀護理安老院](https://web.archive.org/web/20160304081233/http://www.antitb.org.hk/FreniCare/zh/)

[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:香港慈善家](../Category/香港慈善家.md "wikilink")
[Category:CBE勳銜](../Category/CBE勳銜.md "wikilink")
[Category:帕西人](../Category/帕西人.md "wikilink")
[Category:皇仁書院校友](../Category/皇仁書院校友.md "wikilink")
[Category:太平紳士](../Category/太平紳士.md "wikilink")
[Category:印度裔香港人](../Category/印度裔香港人.md "wikilink")
[Category:瑣羅亞斯德教徒](../Category/瑣羅亞斯德教徒.md "wikilink")

1.

2.  Shroff-Gander (2012), pp.377-378.

3.  關慧玲（2012年7月18日）

4.
5.  〈波打清釀〉（1906年2月27日）

6.  Starling (2006), p.230.

7.  "Police Reserve, Indian Company: Headquarters Found: Mr. J. H.
    Ruttonjee Gives Premises Rent-Free" (26 September 1935)

8.  "Tributes to Sterling Police Commissioner" (21 May 1941)

9.
10. "Personality Parade - Merchant Philanthropist" (24 July 1950)

11. "Craigengower Cricket Club's First Sports Challenge in 1894" (1 July
    1941)

12.

13.
14.
15. "Kowloon residents" (14 April 1897)

16.
17. Chu (2005), pp.158-159.

18. 〈律敦治中心變身〉（2005年12月16日）

19.
20. "Roll out the barrel" (25 August 2013)

21.
22.
23. "Hong Kong's New Brewery Opened" (17 August 1933)

24.
25.
26.
27.
28.
29.
30.
31.
32.
33.
34.
35.
36.
37.
38.
39.
40.
41.
42.
43.
44.
45.
46.
47.
48.
49.
50.
51.
52. White (1994), p.57.

53.
54.
55. 〈前立法局議員慈善家鄧律敦治逝世，遺體今日下午安葬〉（1974年7月29日）

56.
57. 《60周年紀念特刊》（2008年），頁46。

58.
59.
60. 《60周年紀念特刊》（2008年），頁49。

61.
62.
63.
64.
65.
66.
67. 《60周年紀念特刊》（2008年），頁56。

68.
69. 《60周年紀念特刊》（2008年），頁53。

70.
71.
72.
73.
74.
75.
76.
77. 呂大樂（2000年），頁86至87。

78. 〈登律敦治夫人周末赴英渡假〉（1951年12月6日）

79. 《香港房屋協會70周年紀念特刊》（2018年），頁15。

80.
81. 〈模範屋宇會成立，擬在北角興建平價樓四百層〉（1950年7月26日）

82. 〈九龍塘村災區地皮，港府宣佈立即收回〉（1952年5月4日）

83. "Sudden Death of Mr J. H. Ruttonjee" (11 February 1960)

84.
85. 〈印籍慈善家律敦治逝世〉（1960年2月12日）

86.
87.
88.
89. "Rusy M. Shroff: Global NRI for Dec. -'97" (1997)

90.
91.
92. 《60周年紀念特刊》（2008年），頁84。

93.
94.
95.
96.
97.
98. 又作巴努拜·馬斯特（Banubai Master）。（參見Shroff-Gander (2012), pp.377-388.）

99. 〈訃聞〉（1972年10月7日）

100.
101. "[Supplement to
     Issue 43343](https://www.thegazette.co.uk/London/issue/43343/supplement/4961)",
     *London Gazette*, 5 June 1964, p.4961.

102. Shroff-Gander (2012), pp.376-377.

103. "Ruttonjee, Anne Dhun" (retrieved on 25 May 2015)

104.
105. "Ruttonjee, Shirley" (retrieved on 25 May 2015)

106. "Hon. Dhun Ruttonjee" (1969)

107. "Desai-Ruttonjee" (7 February 1925)

108. "Rustom Eduljee Desai" (retrieved on 25 May 2015)

109. "Ruttonjee-Desai, Vera" (retrieved on 25 May 2015)

110.
111.
112. 〈永遠懷念勞士施羅孚副會長BBS, MBE〉（造訪於2018年6月2日）

113.
114.
115. "Historic Building Appraisal: Homi Villa, Castle Peak Road, Tsuen
     Wan, N.T." (retrieved on 25 May 2015)

116.
117.
118.
119. Daswani (23 August 1987)

120.
121.
122. "Late Mr. J. H. Ruttonjee - Wise Philosopher And Good Friend Who
     Will Be Long Remembered" (12 February 1960)

123. "[Supplement to
     Issue 37835](https://www.thegazette.co.uk/London/issue/37835/supplement/21)",
     *London Gazette*, 31 December 1946, p.21.

124.
125.