**锝**（-{zh-hans:，台湾称****;zh-hk:，台灣稱**鎝**;zh-tw:，中國大陸、港澳稱**鍀**;}-）是一種[化學元素](../Page/化學元素.md "wikilink")，其[原子序數是](../Page/原子序數.md "wikilink")43，[化學符號是](../Page/化學符號.md "wikilink")**Tc**。其所有[同位素都具有](../Page/同位素.md "wikilink")[放射性](../Page/放射性.md "wikilink")，是原子序最小的非[穩定元素](../Page/穩定同位素.md "wikilink")。地球上現存的大部分鍀都是人工製造的，自然界中僅有極少量存在。在[鈾礦中](../Page/鈾礦.md "wikilink")，鍀是一種[自發裂變產物](../Page/自發裂變.md "wikilink")；在[鉬礦石中](../Page/鉬.md "wikilink")，鉬經[中子俘獲后可以生成鍀](../Page/中子俘獲.md "wikilink")。鍀是一種銀灰色的[金屬](../Page/金屬.md "wikilink")[晶體](../Page/晶體.md "wikilink")，其[化學性質介於](../Page/化學性質.md "wikilink")[錳和](../Page/錳.md "wikilink")[錸之間](../Page/錸.md "wikilink")。

在鍀發現以前，[德米特里·門捷列夫就已經預測了它的許多性質](../Page/德米特里·門捷列夫.md "wikilink")。在他的[周期表中](../Page/周期表.md "wikilink")，門捷列夫把這種尚未發現的元素叫做“類錳”，符號為Em。1937年，鍀（準確的說是[鍀-97](../Page/鍀-97.md "wikilink")）成為第一個大部分由人工製造的元素。它的英文名來自希腊語，意為“人造”。

鍀的短壽命同位素[鍀-99m具有γ放射性](../Page/鍀-99m.md "wikilink")，廣泛用於核醫學。[鍀-99僅具有β放射性](../Page/鍀-99.md "wikilink")。商業上，鍀的長壽命同位素是[反應堆中](../Page/反應堆.md "wikilink")[鈾-235裂變的副產物](../Page/鈾-235.md "wikilink")，可以從[乏燃料中提取得到](../Page/乏燃料.md "wikilink")。鍀最長壽命的同位素是[鍀-98](../Page/鍀-98.md "wikilink")（[半衰期為](../Page/半衰期.md "wikilink")420萬年）。1952年，有人在壽命超過十億年的[紅巨星中發現了鍀](../Page/紅巨星.md "wikilink")-98，讓人們認識到恆星可以製造[重元素](../Page/重元素.md "wikilink")。

## 歷史

### 搜尋43號元素

根據早期的[元素週期律](../Page/元素週期律.md "wikilink")，許多研究者急於找到和命名第43號元素，其[原子序數似乎顯示它應當比其他未知元素更容易發現](../Page/原子序數.md "wikilink")。1828年，[德國](../Page/德國.md "wikilink")[化學家哥特弗雷德](../Page/化學家.md "wikilink")·威爾海姆·奧桑（Gottfried
Wilhelm
Osann）相信他在[鉑礦裏發現了](../Page/鉑.md "wikilink")43號元素，並命名為polinium。但後來證明那-{只}-不過是不純的[銥](../Page/銥.md "wikilink")。1846年，有人聲稱發現了元素[ilmenium](../Page/ilmenium.md "wikilink")，但後來被證明是不純的[鈮](../Page/鈮.md "wikilink")。1847年，有人說發現了新元素[pelopium](../Page/pelopium.md "wikilink")，但也只是[鈮和](../Page/鈮.md "wikilink")[鉭的](../Page/鉭.md "wikilink")[混合物](../Page/混合物.md "wikilink")。\[1\]\[2\]

在門捷列夫的[元素周期表中](../Page/元素周期表.md "wikilink")，元素鉬（原子序為42）與[釕](../Page/釕.md "wikilink")（原子序為44）之間有一個空白。1871年，門捷列夫預言這個當時尚未發現的元素應當排在錳之下，因此與錳有相似的化學性質。門捷列夫把它叫做“類錳”。\[3\]

1877年，[俄羅斯化學家謝爾蓋](../Page/俄羅斯.md "wikilink")·柯恩（Serge
Kern）稱在鉑礦裏發現了43號元素，並以[英國化學家](../Page/英國.md "wikilink")[杭弗瑞·戴維](../Page/杭弗瑞·戴維.md "wikilink")（Sir
Humphry
Davy）的名字命名為davyum，但最後發現它是銥、[銠和](../Page/銠.md "wikilink")[鐵的混合物](../Page/鐵.md "wikilink")。1896年發現的[lucium最後被確定為](../Page/lucium.md "wikilink")[釔](../Page/釔.md "wikilink")。1908年，[日本化學家](../Page/日本.md "wikilink")在[方釷石中發現了一種新元素](../Page/方釷石.md "wikilink")，並稱之為“日本素”（nipponium）。2004年，日本有學者重新檢驗了小川正孝家族保留下來的方釷石樣品。X-射綫譜證明，該樣品中不含43號元素，而是含有75號元素[錸](../Page/錸.md "wikilink")。小川正孝可能是發現錸的第一人。\[4\]

1925年，德國化學家沃爾特·諾達克（Walter Noddack）、奧托·伯格（Otto
Berg）和[伊達·諾達克](../Page/伊達·諾達克.md "wikilink")（Ida
Noddack）稱他們發現了75號元素和43號元素，並將43號元素根據諾達克家族的發源地命名為masurium。\[5\]他們用[電子流轟擊](../Page/電子.md "wikilink")[鈮鐵礦石](../Page/鈮鐵礦.md "wikilink")，然後通過對[X-射綫](../Page/X-射綫.md "wikilink")[波長的分析發現了新元素](../Page/波長.md "wikilink")。\[6\]因該小組的發現無法重現，他們的發現沒有受到廣泛承認。\[7\]\[8\]但直到1933年還有人把43號元素叫做masurium。\[9\]\[10\]1980年[商务印书馆的](../Page/商务印书馆.md "wikilink")《[新华词典](../Page/新华词典.md "wikilink")》中，将-{zh-hans:;
zh-hant:鎷;}-作为该元素Masurium的中文译名。至今，1925年諾達克等人是否真正發現了鍀還存在爭議。\[11\]\[12\]

### 鍀的正式發現

1936年12月，[意大利](../Page/意大利.md "wikilink")[巴勒莫大學的](../Page/巴勒莫大學.md "wikilink")[卡羅·佩里爾](../Page/卡羅·佩里爾.md "wikilink")（Carlo
Perrier）和[埃米利奧·吉諾·塞格雷](../Page/埃米利奧·吉諾·塞格雷.md "wikilink")（Emilio
Segrè）終于證實了43號元素的存在。\[13\]1936年中，塞格雷在[美國先後訪問了](../Page/美國.md "wikilink")[哥倫比亞大學和](../Page/哥倫比亞大學.md "wikilink")[勞倫斯伯克利國家實驗室](../Page/勞倫斯伯克利國家實驗室.md "wikilink")。他向[迴旋加速器的發明者](../Page/迴旋加速器.md "wikilink")[歐內斯特·勞倫斯要一些迴旋加速器上帶有放射性的廢棄部件](../Page/歐內斯特·勞倫斯.md "wikilink")。於是勞倫斯寄給他了一些曾用做迴旋加速器[偏向板的鉬箔](../Page/偏向板.md "wikilink")。

塞格雷在同事佩里爾的協助下，用化學方法證明鉬箔的放射性來源於一種原子序數為43的新元素。他們成功的分離出了鍀的同位素鍀-95m和鍀-97。\[14\]\[15\]巴勒莫大學當局希望他們把新元素以巴勒莫的拉丁文名稱Panormus命名為
“panormium”。\[16\]
1947年，43號元素根據希腊語命名為鍀，意為“人造”。\[17\]\[18\]塞格雷再度訪問美國。他和[格倫·西奧多·西博格一起分離了鍀的](../Page/格倫·西奧多·西博格.md "wikilink")[介穩態同位素鍀](../Page/介穩態.md "wikilink")-99m。現在這種同位素被廣泛用於許多[核醫學診斷中](../Page/核醫學.md "wikilink")。\[19\]

1952年，美國[天文學者保羅](../Page/天文學.md "wikilink")·麥裏爾在S-型[紅巨星的](../Page/紅巨星.md "wikilink")[光譜中觀察到了鍀的發射譜綫](../Page/光譜.md "wikilink")。\[20\]這些星體的年齡是鍀最長壽同位素半衰期的幾千倍。這意味著它們還在通過[核反應產生鍀](../Page/核反應.md "wikilink")。當時，[恆星通過](../Page/恆星.md "wikilink")[核合成產生重元素只是一個假說](../Page/核合成.md "wikilink")。此觀測無疑給這一假說提供了證據。\[21\]近來，有觀測表明重元素是在[s-過程中通過](../Page/s-過程.md "wikilink")[中子俘獲生成的](../Page/中子俘獲.md "wikilink")。\[22\]

自發現以來，人們一直在搜索自然界中的鍀。1962年，有人從[比属刚果的](../Page/比属刚果.md "wikilink")[瀝青鈾礦中分離出了鍀](../Page/瀝青鈾礦.md "wikilink")-99。其含量極低，每公斤鈾礦僅含有0.2[納克鍀](../Page/納克.md "wikilink")，\[23\]是[鈾-238](../Page/鈾-238.md "wikilink")[自發裂變的產物](../Page/自發裂變.md "wikilink")。有證據表明，[加蓬](../Page/加蓬.md "wikilink")[奧克洛](../Page/奧克洛.md "wikilink")[天然核反应堆曾產生大量的鍀](../Page/天然核反应堆.md "wikilink")-99，但在過去億萬年中已經幾乎全部[衰變為](../Page/衰變.md "wikilink")[釕-99](../Page/釕-99.md "wikilink")。\[24\]

## 性質

### 物理性質

鍀是一種銀灰色放射性[金屬](../Page/金屬.md "wikilink")，外觀與[鉑相似](../Page/鉑.md "wikilink")。粉狀時呈灰色。純金屬鍀的[晶型為](../Page/晶型.md "wikilink")[六方最密堆积](../Page/六方最密堆积.md "wikilink")。在[原子發射光譜中](../Page/原子發射光譜.md "wikilink")，鍀的特徵譜綫位于363.3、403.1、426.2、429.7和485.3納米。\[25\]

金屬鍀具有[順磁性](../Page/順磁性.md "wikilink")。在溫度降到7.46
K時，純金屬鍀的[單晶成為](../Page/單晶.md "wikilink")[二型超導體](../Page/二型超導體.md "wikilink")（type-II
superconductor）。\[26\]\[27\]在這個溫度以下，鍀具有很高的[倫敦穿透深度](../Page/倫敦穿透深度.md "wikilink")，在所有金屬中僅次於[鈮](../Page/鈮.md "wikilink")。\[28\]

### 化學性質

在[元素周期表中](../Page/元素周期表.md "wikilink")，鍀處在[第五周期和](../Page/第5周期元素.md "wikilink")[第七族](../Page/7族元素.md "wikilink")，位于[錳和](../Page/錳.md "wikilink")[錸之間](../Page/錸.md "wikilink")。根據[元素週期律](../Page/元素週期律.md "wikilink")，其化學性質應介於這兩种元素之間。但鍀比較不活潑，容易形成[共價鍵](../Page/共價鍵.md "wikilink")，不易生成[陽離子](../Page/陽離子.md "wikilink")。這些性質上鍀與錳不同，而更接近于錸。\[29\]鍀常見的[氧化數有](../Page/氧化數.md "wikilink")+4、+5和+7。\[30\]金屬鍀能溶于[王水](../Page/王水.md "wikilink")、[硝酸和](../Page/硝酸.md "wikilink")[濃硫酸](../Page/濃硫酸.md "wikilink")，但不溶于[鹽酸](../Page/鹽酸.md "wikilink")。\[31\]

### 鍀的同位素

在元素周期表中，鍀是沒有[穩定同位素的原子序數最小的元素](../Page/穩定同位素.md "wikilink")；下一個沒有穩定同位素的元素是[鉕](../Page/鉕.md "wikilink")，其原子序數為61。\[32\]即便[中子數為](../Page/中子數.md "wikilink")[偶數](../Page/偶數.md "wikilink")，[原子核中](../Page/原子核.md "wikilink")[質子數為](../Page/質子.md "wikilink")[奇數的](../Page/奇數.md "wikilink")[核素要比質子數為偶數的更不穩定](../Page/核素.md "wikilink")。\[33\]因此對於[原子核中](../Page/原子核.md "wikilink")[質子數為](../Page/質子.md "wikilink")[奇數的](../Page/奇數.md "wikilink")[核素來說](../Page/核素.md "wikilink")，一般穩定同位素較少。

鍀最穩定的[同位素分別是鍀](../Page/同位素.md "wikilink")-98（半衰期為420萬年）、鍀-97（半衰期為260萬年）和鍀-99（半衰期為21萬1千年）。\[34\]已知其它還有30种鍀同位素，[質量數在](../Page/質量數.md "wikilink")85到118之間。\[35\]大多數這些同位素的半衰期不超過一個小時。半衰期在兩個小時以上的有鍀-93
（半衰期為2.73小時）、鍀-94 （4.88小時）、鍀-95 （20小時）和鍀-96（4.3天）。\[36\]

質量數小於鍀-98的鍀同位素衰變時以[電子俘獲為主](../Page/電子俘獲.md "wikilink")，產物是鉬（原子序為42）。\[37\]質量數高於鍀-98的鍀同位素衰變時以[β衰變為主](../Page/β衰變.md "wikilink")，產物是釕（原子序為44）。唯一的例外是鍀-100：它既可以俘獲電子生成鉬，也可以發生β衰變生成釕。\[38\]\[39\]

鍀還有很多[介穩態同位素](../Page/介穩態.md "wikilink")，也稱為[同質異能同位素](../Page/同質異能.md "wikilink")。鍀-97m（<sup>97m</sup>Tc；“m”代表“介穩態”）最為穩定，半衰期為91天。\[40\]鍀-95m半衰期次之（61天）。鍀-99m半衰期為6.01小時。它釋放出[γ射線](../Page/γ射線.md "wikilink")，轉變成鍀-99。\[41\]

鍀-99是核反應堆中[鈾-235和](../Page/鈾-235.md "wikilink")[鈈-239](../Page/鈈-239.md "wikilink")[裂變的主要產物之一](../Page/裂變.md "wikilink")，因此是最常見、最易得的鍀同位素。一克鍀-99每秒鐘裂變6.2×10<sup>8</sup>次（即0.62G[Bq](../Page/貝可勒爾.md "wikilink")/克）。\[42\]

## 化合物

### 氫化物和氧化物

鍀和[氫氣反應生成鍀](../Page/氫氣.md "wikilink")[氫化物](../Page/氫化物.md "wikilink")[陰離子](../Page/陰離子.md "wikilink")，其[晶體結構跟錸氫化物](../Page/晶體結構.md "wikilink")類似。六個[氫原子組成一個](../Page/氫原子.md "wikilink")[三棱柱](../Page/三棱柱.md "wikilink")，鍀位于三棱柱的体心位置。另外三個氫[原子位于通過中心鍀原子並與上](../Page/原子.md "wikilink")、下[底面](../Page/底面.md "wikilink")[平行的](../Page/平行.md "wikilink")[平面上](../Page/平面.md "wikilink")，和上、下底面的氫原子呈交錯構象（gauche）。九個氫原子不等同，但是在[電子結構上非常相似](../Page/電子結構.md "wikilink")。鍀在這個[離子中的](../Page/離子.md "wikilink")[配位數是九](../Page/配位數.md "wikilink")。這是鍀化合物的最高配位數。\[43\]

[Technetiumhydrid.png](https://zh.wikipedia.org/wiki/File:Technetiumhydrid.png "fig:Technetiumhydrid.png")

金屬鍀在潮濕[空氣中會漸漸失去光澤](../Page/空氣.md "wikilink")。\[44\]粉狀鍀在氧氣中可以燃燒。現已制得兩種鍀的氧化物：TcO<sub>2</sub>和Tc<sub>2</sub>O<sub>7</sub>。鍀可以被[氧化成](../Page/氧化.md "wikilink")[高鍀酸離子](../Page/高鍀酸.md "wikilink")。\[45\]\[46\]

在400–450 °C，鍀可以被氧化成淺黃色的[七氧化二鍀](../Page/七氧化二鍀.md "wikilink")：

  -
    4 Tc + 7 O<sub>2</sub> → 2 Tc<sub>2</sub>O<sub>7</sub>

此化合物的結構[中心對稱](../Page/中心對稱.md "wikilink")，共有兩種不同的鍀-氧鍵，鍵長分別為167 pm和184
pm。\[47\]

以七氧化二鍀為原料，可以制得[高鍀酸鈉](../Page/高鍀酸鈉.md "wikilink")：\[48\]

  -
    Tc<sub>2</sub>O<sub>7</sub> + 2 NaOH → 2 NaTcO<sub>4</sub> +
    H<sub>2</sub>O

黑色的[二氧化鍀可以用鍀或氫氣](../Page/二氧化鍀.md "wikilink")[還原七氧化二鍀來製備](../Page/還原.md "wikilink")。\[49\]

[高鍀酸可以通過Tc](../Page/高鍀酸.md "wikilink")<sub>2</sub>O<sub>7</sub>和水或[氧化性酸反應來製備](../Page/氧化性酸.md "wikilink")。可用的酸包括硝酸、濃硫酸、王水和硝酸與鹽酸的混酸。深紅色的高鍀酸是[強酸](../Page/強酸.md "wikilink")，易吸水。和[高錳酸](../Page/高錳酸.md "wikilink")（）不同，高鍀酸是一種弱[氧化劑](../Page/氧化劑.md "wikilink")。因為水溶性好，高鍀酸鹽是臨床上常用的鍀試劑和催化劑。\[50\]

高鍀酸陰離子是[正四面體結構](../Page/正四面體.md "wikilink")：四個[氧原子在](../Page/氧.md "wikilink")[四面體的四角](../Page/四面體.md "wikilink")，鍀原子位于四面體的中心。在濃硫酸中，高鍀酸會轉化成[八面體結構的](../Page/八面體.md "wikilink")[異高鍀酸](../Page/異高鍀酸.md "wikilink")（technetic(VII)
acid）TcO<sub>3</sub>(OH)(H<sub>2</sub>O)<sub>2</sub>。\[51\]

### 硫化物、硒化物和碲化物

鍀有多种[硫化物](../Page/硫化物.md "wikilink")。鍀可以和[硫直接起反應生成TcS](../Page/硫.md "wikilink")<sub>2</sub>。Tc<sub>2</sub>S<sub>7</sub>則是通過高鍀酸與[硫化氫的反應製備](../Page/硫化氫.md "wikilink")：

  -
    2 HTcO<sub>4</sub> + 7 H<sub>2</sub>S → Tc<sub>2</sub>S<sub>7</sub>
    + 8 H<sub>2</sub>O

在這個反應中鍀(VII)被[還原成四价](../Page/還原.md "wikilink")，過量的硫生成[雙硫鍵](../Page/雙硫鍵.md "wikilink")。[七硫化二鍀是](../Page/七硫化二鍀.md "wikilink")[聚合物](../Page/聚合物.md "wikilink")，結構為(Tc<sub>3</sub>(µ<sup>3</sup>–S)(S<sub>2</sub>)<sub>3</sub>S<sub>6</sub>)<sub>n</sub>，類似于鉬的硫化物
Mo<sub>3</sub>(µ<sup>3</sup>–S)(S<sub>2</sub>)<sub>6</sub><sup>2–</sup>.\[52\]

七硫化二鍀受熱後會分解為[二硫化鍀和硫](../Page/二硫化鍀.md "wikilink")[單質](../Page/單質.md "wikilink")：

  -
    Tc<sub>2</sub>S<sub>7</sub> → 2 TcS<sub>2</sub> + 3 S

[硒和](../Page/硒.md "wikilink")[碲與鍀的反應和硫相似](../Page/碲.md "wikilink")。\[53\]

[Technetiumcluster.png](https://zh.wikipedia.org/wiki/File:Technetiumcluster.png "fig:Technetiumcluster.png")

### 卤化物

锝能形成包括[四氯化锝](../Page/四氯化锝.md "wikilink")、[六氟化锝在内的多种卤化物](../Page/六氟化锝.md "wikilink")，有关综述参见文献。\[54\]

### 碳化鍀和鍀的正交晶相

金屬鍀中如果有少量[碳雜質](../Page/碳.md "wikilink")，會使由部分鍀由[六方密堆积轉變成為](../Page/六方密堆积.md "wikilink")[正交晶系結構](../Page/正交晶系.md "wikilink")。當含碳量超過15-17%時，轉變趨於完全，生成[立方晶系的碳化鍀](../Page/立方晶系.md "wikilink")，化學式近似為Tc<sub>6</sub>C。其結構不會再隨碳含量增加而變化。\[55\]

### 簇合物和有機配合物

鍀有幾种已知的[簇合物](../Page/簇合物.md "wikilink")，包括Tc<sub>4</sub>、Tc<sub>6</sub>、Tc<sub>8</sub>和Tc<sub>13</sub>。\[56\]\[57\]較穩定的Tc<sub>6</sub>和Tc<sub>8</sub>簇合物擁有[棱柱型結構](../Page/棱柱.md "wikilink")。垂直方向上的鍀原子以[三鍵相連接](../Page/三鍵.md "wikilink")，水平方向的則以[單鍵連接](../Page/單鍵.md "wikilink")。每個鍀原子有六個鍵，剩餘[價電子通過](../Page/價電子.md "wikilink")[直立鍵同一個](../Page/直立鍵.md "wikilink")[鹵素原子](../Page/鹵素.md "wikilink")，或者通過[橋鍵與兩個鹵素原子相連接](../Page/橋鍵.md "wikilink")，比如[氯和](../Page/氯.md "wikilink")[溴](../Page/溴.md "wikilink")。\[58\]

[Technetiumkomplex.svg](https://zh.wikipedia.org/wiki/File:Technetiumkomplex.svg "fig:Technetiumkomplex.svg")

鍀可以形成衆多的有機[配合物](../Page/配合物.md "wikilink")。因為它們在[核醫學方面的重要性](../Page/核醫學.md "wikilink")，對鍀配合物的研究較為充分。[羰基鍀Tc](../Page/羰基鍀.md "wikilink")<sub>2</sub>(CO)<sub>10</sub>為白色固體。\[59\]此化合物中，兩個鍀原子之間的[化學鍵很弱](../Page/化學鍵.md "wikilink")，每個鍀原子與五個[羰基以](../Page/羰基.md "wikilink")[八面體結構鍵合](../Page/八面體.md "wikilink")。鍀-鍀鍵[鍵長為](../Page/鍵長.md "wikilink")303
[pm](../Page/pm.md "wikilink")，\[60\]\[61\]比金屬鍀中原子間距（272pm）長得多。和鍀同族的錳和錸也可形成類似的羰基化合物。\[62\]

右圖中所示的有機鍀配合物在核醫學中應用廣泛。其鍀-氧[雙鍵垂直於環平面](../Page/雙鍵.md "wikilink")。這個氧原子也可以被氮原子替換。\[63\]

## 存象和生產

[Uraniumore.jpg](https://zh.wikipedia.org/wiki/File:Uraniumore.jpg "fig:Uraniumore.jpg")

[地殼中僅含有極微量天然鍀](../Page/地殼.md "wikilink")，是鈾礦自發裂變的產物。据估計，一公斤鈾礦含有一[納克鍀](../Page/納克.md "wikilink")。\[64\]\[65\]\[66\]某些S型、M型和N型[紅巨星的](../Page/紅巨星.md "wikilink")[吸收光譜中有鍀的特徵吸收](../Page/吸收光譜.md "wikilink")。\[67\]\[68\]這種紅巨星被戲稱為“鍀星”（technetium
stars）。\[69\]

### 裂變核廢料

每年大量的鍀被從[乏燃料中被分離提取出來](../Page/乏燃料.md "wikilink")。[反應堆中](../Page/反應堆.md "wikilink")1克[鈾-235](../Page/鈾-235.md "wikilink")[裂變可以產生](../Page/裂變.md "wikilink")27毫克鍀-99，[產額為](../Page/產額.md "wikilink")6.1%。\[70\]如果以[鈾-233為燃料](../Page/鈾-233.md "wikilink")，鍀-99產額為4.9%；如果以[鈈-239為燃料](../Page/鈈-239.md "wikilink")，鍀-99產額為6.21%。\[71\]据估計，從1983年到1994年，人類反應堆中一共產生了78噸鍀，是地球上鍀最主要的來源。\[72\]\[73\]其中只有一小部分用於商業用途。\[74\]

鈾-235和鈈-239的裂變都可以產生鍀-99。因此，鍀-99不但存在于核廢料中，也存在于裂變[核武器爆炸后產生的](../Page/核武器.md "wikilink")[放射性落下灰中](../Page/放射性落下灰.md "wikilink")。在乏燃料貯存過程中，鍀-99的放射性在10<sup>4</sup>到10<sup>6</sup>年之間將居主導地位。\[75\]据估算，1945年到1994年間[大氣層](../Page/大氣層.md "wikilink")[核試驗一共向環境釋放了](../Page/核試驗.md "wikilink")250公斤鍀。\[76\]\[77\]截至1986年，人類核反應堆一共排放了1600公斤鍀，主要是在乏燃料[再處理過程中排放的](../Page/再處理.md "wikilink")；大部分進入海洋。隨著再處理技術的發展，反應堆鍀的排放逐漸減少。到2005年，最主要的排放源是[英國](../Page/英國.md "wikilink")[謝拉斐爾德再處理厰](../Page/謝拉斐爾德再處理厰.md "wikilink")（Sellafield
Ltd）。据估計，1995年到1999年，該廠一共向[愛爾蘭海排放了](../Page/愛爾蘭海.md "wikilink")900公斤鍀。\[78\]2000年后，法律規定該廠每年只能排放140公斤鍀。\[79\]該廠的排放導致某些海產品含有微量的鍀。比如，英國[坎布里亞郡西部捕獲的](../Page/坎布里亞郡.md "wikilink")[歐洲龍蝦和魚含有](../Page/歐洲龍蝦.md "wikilink")1
Bq/公斤的鍀。\[80\]\[81\]\[82\]

### 鍀的商業用途

以鈾或鈈為燃料的核反應堆一直不停地產生[介穩態核素鍀](../Page/介穩態核素.md "wikilink")-99m。但乏燃料都要經過數年冷卻，到再處理的時候鉬-99和鍀-99m已經完全衰變了。經過[鈈鈾提取](../Page/鈈鈾提取.md "wikilink")（PUREX）之後的液料含有高濃度的鍀（以形式存在），基本上全部是鍀-99，鍀-99m已經消失殆盡。\[83\]

絕大部分醫用鍀-99m是在反應堆裏[輻照高](../Page/輻照.md "wikilink")[濃縮鈾](../Page/濃縮鈾.md "wikilink")（鈾-235含量超過20%）靶子，然后在再處理厰分離出鉬-99，\[84\]最後在醫院提取由鉬-99衰變而生成的鍀-99m。\[85\]\[86\]在[鍀-99m製備機中](../Page/鍀-99m製備機.md "wikilink")，含有鉬-99的[鉬酸](../Page/鉬酸.md "wikilink")（）被[吸附在](../Page/吸附.md "wikilink")[酸性](../Page/酸性.md "wikilink")[氧化鋁上](../Page/氧化鋁.md "wikilink")，裝載到一個帶有[輻射防護屏的](../Page/輻射防護.md "wikilink")[色譜柱中](../Page/色譜柱.md "wikilink")。這種機器又被稱為“鍀奶牛”，有時也叫做“鉬奶牛”。鉬-99半衰期為66小時，比較便於運輸。用鹽水（[氯化鈉水溶液](../Page/氯化鈉.md "wikilink")）做[淋洗劑](../Page/淋洗劑.md "wikilink")，便可將含有鍀-99m的[高鍀酸鹽淋洗下來](../Page/高鍀酸鹽.md "wikilink")。

和從[裂變產物中提取鉬](../Page/裂變產物.md "wikilink")-99相比，通過輻照高濃縮鈾的方法大大減化了化學分離的步驟。此法的缺點是需要高濃縮鈾-235，會引起諸多的安全問題，比如[核材料](../Page/核材料.md "wikilink")[走私](../Page/走私.md "wikilink")、[核擴散等](../Page/核擴散.md "wikilink")。\[87\]\[88\]

世界上近三分之二的鍀由兩座反應堆提供：[加拿大](../Page/加拿大.md "wikilink")[曹爾克河實驗室的](../Page/曹爾克河實驗室.md "wikilink")[國家研究通用反應堆和](../Page/國家研究通用反應堆.md "wikilink")[荷蘭](../Page/荷蘭.md "wikilink")[核研究和咨詢集團的](../Page/核研究和咨詢集團.md "wikilink")[帕滕反應堆](../Page/帕滕反應堆.md "wikilink")。這兩座反應堆建於1960年代，都已經接近退役期限。加拿大擬建的兩座“多功能應用物理晶格實驗”反應堆本來計劃生產超過全世界總需求一倍的鍀，但在2008年在試車成功后被封堆，整個計劃取消。這給鍀將來的供應前景帶來了巨大的不確定性。\[89\]

2009年8月，曹爾克河實驗室反應堆開始封堆維修，計劃於2010年4月再次運行。2010年2月19日，帕滕反應堆開始了為期六個月的封堆維修。每年千百萬人需要鍀-99m，供應缺口迫使一些醫生重新啓用20年前的老技術。\[90\]令人稍感寬慰的是，[波蘭](../Page/波蘭.md "wikilink")[瑪利亞研究反應堆宣佈開發了一種製備鍀](../Page/瑪利亞研究反應堆.md "wikilink")-99m的新技術。\[91\]曹爾克河實驗室反應堆於2010年8月重新啓動.一個月后帕滕反應堆也再次運轉。\[92\]鍀-99m的供應危機暫時得以緩解。

### 廢物處理

鍀-99的半衰期長，容易形成易溶于水的高鍀酸鹽（）。這些對於核廢料的長期儲存和處理以及減少排放不利。在再處理工廠中，許多提取和分離裂變產物的過程和工藝主要針對陽離子，比如[銫-137和](../Page/銫-137.md "wikilink")[鍶-90](../Page/鍶-90.md "wikilink")，類似高鍀酸鹽的放射性陰離子則暢通無阻。在現有的陸地埋藏方案中，最大的問題是[水](../Page/水.md "wikilink")[污染](../Page/污染.md "wikilink")。高鍀酸鹽和[碘離子](../Page/碘離子.md "wikilink")（）溶解性好，不易被[土壤](../Page/土壤.md "wikilink")、岩石和礦物[吸附](../Page/吸附.md "wikilink")，很有可能隨水遷移。比較而言，鈈、鈾和銫更容易被土壤吸附。通過某些[微生物活動](../Page/微生物.md "wikilink")，鍀可以在一些環境富集，比如在湖底[沉積物中](../Page/沉積物.md "wikilink")。\[93\]因此，鍀的環境化學是相當活躍的研究領域。\[94\]

還有一種處理方式是[核嬗變](../Page/核嬗變.md "wikilink")。[歐洲核子研究組織](../Page/歐洲核子研究組織.md "wikilink")（CERN）已經證明鍀-99的嬗變可行。用[中子流轟擊金屬鍀](../Page/中子.md "wikilink")-99生成短壽命產物鍀-100（半衰期為16秒），後者經過β衰變變成釕-100（穩定同位素）。這個方法對鍀-99靶的純度要求很高。如果靶中有微量[錒系元素](../Page/錒系元素.md "wikilink")，比如[鎇或者](../Page/鎇.md "wikilink")[鋦](../Page/鋦.md "wikilink")，它們會在中子的轟擊下裂變，生成具有高放射性的裂變產物。該方法還可能生成釕-106（半衰期為374天）。它的存在會延長分離釕-100之前所需要的冷卻時間。\[95\]

從乏燃料中分離鍀-99是個漫長過程。在再處理工藝中，它出現在鈾和鈈回收后的高放射性廢液中。經過幾年的冷卻后，該廢液的放射性下降到一定程度之後，鍀-99可以用幾種化學[萃取方法回收](../Page/萃取.md "wikilink")，產生高純度的金屬鍀。\[96\]

### 中子活化

鉬-98經[中子活化后可生成鉬](../Page/中子活化.md "wikilink")-99，然後衰變成鍀-99m。\[97\]其它鍀同位素在裂變中產額較低，一般通過中子輻照前体同位素製備。比如，鍀-97是用中子轟擊釕-96生產。\[98\]

### 粒子加速器

1971年，有人在醫用[回旋加速器用](../Page/回旋加速器.md "wikilink")22兆[電子伏特](../Page/電子伏特.md "wikilink")[質子轟擊鉬](../Page/質子.md "wikilink")-100（純度\>99.5%）靶子，通過反應<sup>100</sup>Mo(p,2n)<sup>99m</sup>Tc製備鍀-99m。\[99\]2010年代鍀的短缺讓人們重新審視此法。\[100\]使用加速器，還可以通過(n,2n)或(γ,n)反應從鉬-100製備鉬-99，進而得到鍀-99m。\[101\]\[102\]\[103\]

## 應用

### 核醫學和生物學

[Basedow-vor-nach-RIT.jpg](https://zh.wikipedia.org/wiki/File:Basedow-vor-nach-RIT.jpg "fig:Basedow-vor-nach-RIT.jpg")患者的頸部鍀[閃爍掃描圖譜](../Page/閃爍掃描.md "wikilink")。|alt=Upper
image: two drop-like features merged at their bottoms; they have a
yellow centre and a red rim on a black background. Caption: Graves'
Disease Tc-Uptake 16%. Lower image: red dots on black background.
Caption: 250 Gy (30mCi) + Prednison.\]\]

鍀-99m半衰期較短（6.01小時），並釋放出容易檢測的軟γ射線（140千電子伏特），因此在核醫學上用於人體[示蹤劑](../Page/示蹤劑.md "wikilink")。\[104\]\[105\]截止2000年，含有鍀-99m的常見[放射性藥物有](../Page/放射性藥物.md "wikilink")31種，用於[大腦](../Page/大腦.md "wikilink")、[心肌](../Page/心肌.md "wikilink")、[甲狀腺](../Page/甲狀腺.md "wikilink")、[肺](../Page/肺.md "wikilink")、[肝](../Page/肝.md "wikilink")、[膽囊](../Page/膽囊.md "wikilink")、[腎](../Page/腎.md "wikilink")、[骨骼](../Page/骨骼.md "wikilink")、[血液等器官和](../Page/血液.md "wikilink")[腫瘤的](../Page/腫瘤.md "wikilink")[造影和功能性研究](../Page/造影.md "wikilink")。\[106\]

鍀-95m半衰期稍長，為61天。它也可以用做[植物和](../Page/植物.md "wikilink")[牲畜等的示蹤劑](../Page/牲畜.md "wikilink")，以及研究鍀在環境中的遷移。\[107\]

### 工業及化學應用

鍀-99是一種純β[輻射源](../Page/輻射源.md "wikilink")，釋放出低能量β粒子。它的半衰期很長，所以輻射變化很緩慢。從放射性廢物中提取的鍀可以達到很高的[化學純度和](../Page/化學純度.md "wikilink")[同位素純度](../Page/同位素純度.md "wikilink")。因此，鍀-99是[美國](../Page/美國.md "wikilink")[國家標準技術研究所](../Page/國家標準技術研究所.md "wikilink")（NIST）認證的標準β輻射源，用於儀器校準。\[108\]有人提議將鍀-99用於[光電器件和](../Page/光電器件.md "wikilink")[納米級的](../Page/納米.md "wikilink")[核電池](../Page/核電池.md "wikilink")。\[109\]

同[錸和](../Page/錸.md "wikilink")[鈀類似](../Page/鈀.md "wikilink")，鍀也可以作為[催化劑](../Page/催化劑.md "wikilink")。對某些反應，比如[異丙醇](../Page/異丙醇.md "wikilink")[脫氫反應](../Page/脫氫反應.md "wikilink")，鍀的催化活性比錸和鈀高得多。但是，其放射性限制了它在催化上的應用。\[110\]

[高鍀酸鉀在很低的濃度](../Page/高鍀酸鉀.md "wikilink")（55
ppm）就可以防止[鋼鐵的](../Page/鋼鐵.md "wikilink")[銹蝕](../Page/銹蝕.md "wikilink")，甚至在250
°C仍有抗銹的能力。\[111\]因此，[高鍀酸鹽可能可以用作鋼鐵的](../Page/高鍀酸鹽.md "wikilink")[防銹劑](../Page/防銹劑.md "wikilink")。但是鍀的放射性決定了鍀防銹劑只能用於封閉體系。\[112\]鉻酸鹽也能防銹，但其有效濃度是高鍀酸鉀的10倍。在一個實驗中，一塊[碳鋼被浸泡在高鍀酸鹽水溶液中](../Page/碳鋼.md "wikilink")20年仍沒有銹蝕。\[113\]高鍀酸鹽防銹的機理尚不明確，但似乎是在鋼鐵表面形成了一層[保護層](../Page/保護層.md "wikilink")。一種理論認為，高鍀酸鹽和鋼鐵的表面發生反應，生成了一薄層緻密的[二氧化鍀](../Page/二氧化鍀.md "wikilink")，保護鋼鐵不受[氧化](../Page/氧化.md "wikilink")。這個理論可以解釋為什麽鐵粉能從水中吸附高鍀酸離子。但當高鍀酸離子濃度下降到某一最小濃度時，鐵粉對該離子的吸附停止。高濃度干擾離子的存在也會抑制鐵粉對高鍀酸離子的吸附。\[114\]

如上所述，鍀的放射性限制了它作為防銹劑的實際應用。有人提議把高鍀酸鹽用於[沸水堆鋼鐵構件的](../Page/沸水堆.md "wikilink")[腐蝕防護](../Page/腐蝕防護.md "wikilink")，但是此建議未獲採用。\[115\]

## 鍀的安全使用

在生物體内鍀沒有任何已知的功能。\[116\]鍀毒性似乎比較小。連續幾個星期給[小白鼠喂飼含有](../Page/小白鼠.md "wikilink")15[微克](../Page/微克.md "wikilink")/克鍀的食物，其[血液](../Page/血液.md "wikilink")、[體重](../Page/體重.md "wikilink")、[器官重量和進食量未觀察到顯著的變化](../Page/器官.md "wikilink")。\[117\]鍀的[放射毒性取決於鍀](../Page/放射毒性.md "wikilink")[化合物組成](../Page/化合物.md "wikilink")、輻射類型和鍀同位素的半衰期。\[118\]

使用所有鍀同位素都必須謹慎。最大的健康風險是吸入帶有鍀同位素的灰塵，可以致肺癌。最常見的同位素鍀-99釋放出低能量β粒子，甚至無法貫穿[實驗室的](../Page/實驗室.md "wikilink")[玻璃器皿](../Page/玻璃.md "wikilink")。因此和鍀化合物接觸時，[手套箱基本沒有必要](../Page/手套箱.md "wikilink")，[通風櫥便可以很好的消除其危害](../Page/通風櫥.md "wikilink")。\[119\]

## 註釋

## 參考資料

## 引用書目

  -
  -
  -
  -
  -
## 外部連結

  -
  -
  -
  - [WebElements.com –
    Technetium](http://www.webelements.com/webelements/elements/text/Tc/index.html),
    and [EnvironmentalChemistry.com –
    Technetium](http://environmentalchemistry.com/yogi/periodic/Tc.html)


  - [Nudat 2](http://www.nndc.bnl.gov/nudat2/index.jsp)
    美國[布魯克黑文國家實驗室國家核數據中心的核素圖表](../Page/布魯克黑文國家實驗室.md "wikilink")

  - *[Nuclides and Isotopes](http://chartofthenuclides.com/default.html)
    Fourteenth Edition: Chart of the Nuclides*, General Electric
    Company, 1989

  - [Chemistry in its element
    podcast](http://www.rsc.org/chemistryworld/podcast/element.asp)（MP3）取自[皇家化學學會的](../Page/皇家化學學會.md "wikilink")《化學世界》雜誌（Chemistry
    World）：
    [Technetium](http://www.rsc.org/images/CIIE_Technitium_48kbps_tcm18-125831.mp3)

[Category:过渡金属](../Category/过渡金属.md "wikilink")
[Category:人工合成元素](../Category/人工合成元素.md "wikilink")
[5G](../Category/第5周期元素.md "wikilink")
[5G](../Category/化学元素.md "wikilink")
[\*](../Category/鍀.md "wikilink")
[Category:1930年代发现的物质](../Category/1930年代发现的物质.md "wikilink")

1.

2.

3.

4.

5.
6.

7.

8.

9.

10.

11.

12.

13.

14.
15.

16.

17.
18.

19.

20.

21.
22.

23.
24.
25.

26. 對於99.9%純度的鍀，晶體缺陷和雜質會使轉變溫度上升至11.2 K

27.

28.

29.

30.
31.
32.
33.

34.
35.

36.
37.
38.
39.

40.

41.
42.
43.

44.

45.

46.
47.

48.

49.

50.

51.

52.

53.

54.

55.

56.

57.

58.

59.

60.

61.

62.

63.

64.
65.

66.

67.

68.

69.

70.
71.

72.

73.
74. 截至2005年，持有[橡樹嶺國家實驗室許可的人仍然可以獲得含鍀](../Page/橡樹嶺國家實驗室.md "wikilink")-99的[高鍀酸銨](../Page/高鍀酸銨.md "wikilink")。

75.
76.
77.

78.

79.

80.

81.

82. [厭氧的](../Page/厭氧.md "wikilink")[梭菌屬](../Page/梭菌屬.md "wikilink")（Clostridium）細菌可以將鍀(VII)還原成鍀(IV)。其還原能力也許在很大程度上決定了鍀在工業污水和其他地表環境中的遷移。

83.

84.

85.

86.

87.

88.

89.

90.
91.

92.

93.

94.

95.

96.

97.

98.

99.

100.

101.

102.

103.

104.
105.
106.

107.

108.

109.

110.

111.

112.

113.
114.
115.

116.
117.

118.

119.