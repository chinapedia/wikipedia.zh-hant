**9月**是[公历年中的第九个月](../Page/公历.md "wikilink")，是[小月](../Page/小月.md "wikilink")，共有30天。

  - 在[北半球](../Page/北半球.md "wikilink")，9月是[秋季的第一个月](../Page/秋季.md "wikilink")，本月[节气](../Page/节气.md "wikilink")：[白露](../Page/白露.md "wikilink")、[秋分](../Page/秋分.md "wikilink")。
  - 在[英文中](../Page/英文.md "wikilink")，9月（September）源自[拉丁语](../Page/拉丁语.md "wikilink")*September*，意为第七个月，因加入7、8月而後移，在月曆上的簡寫為Sep.或Sept.。
  - [日本的九月有很多別名](../Page/日本.md "wikilink")，如「長月」、「稻刈月」、「寢覺月」等。

## 9月的節日

  - 在[中華民國](../Page/中華民國.md "wikilink")，[9月1日是](../Page/9月1日.md "wikilink")[記者節](../Page/記者節.md "wikilink")。
  - 在[中華民國](../Page/中華民國.md "wikilink")，[9月3日是](../Page/9月3日.md "wikilink")[軍人節](../Page/軍人節.md "wikilink")。
  - 在[天主教傳統](../Page/天主教.md "wikilink")，[9月8日是](../Page/9月8日.md "wikilink")[聖母聖誕](../Page/聖母瑪利亞.md "wikilink")，[9月21日是](../Page/9月21日.md "wikilink")[聖瑪竇日](../Page/聖馬太.md "wikilink")，[9月29日是](../Page/9月29日.md "wikilink")[聖彌額爾，聖加俾額爾及聖辣法爾天使日](../Page/Michaelmas.md "wikilink")。
  - 在[中華民國](../Page/中華民國.md "wikilink")，[9月9日是](../Page/9月9日.md "wikilink")[國民體育日](../Page/國民體育日.md "wikilink")
  - 在[中華人民共和國](../Page/中華人民共和國.md "wikilink")，[9月10日是](../Page/9月10日.md "wikilink")[教师节](../Page/教师节.md "wikilink")。
  - 在[中華民國](../Page/中華民國.md "wikilink")，[9月28日是](../Page/9月28日.md "wikilink")[教師節](../Page/教師節.md "wikilink")（孔子誕辰紀念日）。
  - 在[美国和](../Page/美国.md "wikilink")[加拿大](../Page/加拿大.md "wikilink")，
    9月第一個[星期一是](../Page/星期一.md "wikilink")[勞動節](../Page/勞動節.md "wikilink")。
  - 9月第三个[星期二是](../Page/星期二.md "wikilink")[国际和平日](../Page/国际和平日.md "wikilink")。
  - 9月第三個週六及週日是[古蹟日](../Page/古蹟日.md "wikilink")。\[1\]
  - 9月的第三個週末是[世界清潔日](../Page/世界清潔日.md "wikilink")，又名[世界清洁地球日](../Page/世界清洁地球日.md "wikilink")，也有组织和个人把[9月14日作为参与该活动的时间](../Page/9月14日.md "wikilink")。
  - 在[中華人民共和國](../Page/中華人民共和國.md "wikilink")，9月第三个星期六是[全民国防教育日](../Page/全民国防教育日.md "wikilink")。
  - 9月第四个[星期日是](../Page/星期日.md "wikilink")[国际聋人节](../Page/国际聋人节.md "wikilink")。
  - 9月最后一个[星期日是](../Page/星期日.md "wikilink")[世界心脏日和](../Page/世界心脏日.md "wikilink")[世界海事日](../Page/世界海事日.md "wikilink")。
  - 在[中华人民共和国](../Page/中华人民共和国.md "wikilink")，9月30日是烈士纪念日。
  - 9月21日是國際失智症日。

## 其它

  - 9月跟12月的首周星期位置相同
  - 9月涉及星座有[處女座及](../Page/處女座.md "wikilink")[天秤座](../Page/天秤座.md "wikilink")

## 參考資料

[月09](../Category/月份.md "wikilink") [月09](../Category/日历.md "wikilink")

1.  [文化部-認識古蹟日](http://www.moc.gov.tw/information_250_13105.html)