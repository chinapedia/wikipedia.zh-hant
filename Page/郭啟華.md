**郭啟華**（），是[東亞唱片顧問兼](../Page/東亞唱片.md "wikilink")[鄭秀文經理人](../Page/鄭秀文.md "wikilink")。

郭啟華祖籍廣東潮州，於香港出生。中學時就讀[聖若瑟英文中學及](../Page/聖若瑟英文中學.md "wikilink")[觀塘瑪利諾書院](../Page/觀塘瑪利諾書院.md "wikilink")，畢業於[香港中文大學](../Page/香港中文大學.md "wikilink")，曾為[商業電台DJ](../Page/商業電台.md "wikilink")、助理節目總監；「大路傳播」創辦人；[新城電台節目總監](../Page/新城電台.md "wikilink")、東亞唱片顧問兼鄭秀文經理人。曾主持的電台節目有《後浪五前速》、《11時衝動》（或名《霎時衝動》）。業餘時參與填詞工作，較知名作品有鄭秀文的《快樂不快樂》、《理智與感情》、《Water
Of Love》。

## 幕後製作列表

### 1996年

  - [許志安](../Page/許志安.md "wikilink") - 奇奇怪怪（曲；與[C. Y.
    Kong合寫](../Page/C._Y._Kong.md "wikilink")）

### 1997年

  - [許志安](../Page/許志安.md "wikilink") -
    我的天，我的歌（詞；與[林夕合填](../Page/林夕.md "wikilink")）
  - 許志安 - 心靈相通（監）
  - 許志安 - 滑浪（詞）
  - [趙學而](../Page/趙學而.md "wikilink") - 無色無相（詞）

### 1998年

  - [古巨基](../Page/古巨基.md "wikilink") - 愛與夢飛行（詞）（以筆名**林家敏**發表）
  - 古巨基 - Be My Valentine（詞）（以筆名**林家敏**發表）
  - [梁詠琪](../Page/梁詠琪.md "wikilink") - 幸福國（詞）（以筆名**林家敏**發表）
  - 梁詠琪 - 小宇宙（詞）（以筆名**林家敏**發表）
  - 梁詠琪 - 空氣污染指數（詞）

### 1999年

  - [古巨基](../Page/古巨基.md "wikilink") - 羅馬假期（詞）（以筆名**林家敏**發表）
  - [鄭秀文](../Page/鄭秀文.md "wikilink") - 真命天子（詞）（以筆名**林家敏**發表）

### 2000年

  - [黃耀明](../Page/黃耀明.md "wikilink") -
    心頭好（曲、詞；與[蔡德才合寫](../Page/蔡德才.md "wikilink")）
  - [鄭秀文](../Page/鄭秀文.md "wikilink") - 快樂不快樂（曲、詞）

### 2002年

  - [at17](../Page/at17.md "wikilink") - 他和她的事情（詞）
  - 蔡德才- 美麗無常（詞）
  - 普普樂團 - 一半半（詞）

### 2003年

  - [at17](../Page/at17.md "wikilink") - 三分鐘後（詞）

### 2006年

  - [Shine](../Page/Shine.md "wikilink") - 迷失決勝分（詞）（以筆名**林家敏**發表）
  - [何韻詩](../Page/何韻詩.md "wikilink") - 忘了你是你（詞）

### 2010年

  - [何韻詩](../Page/何韻詩.md "wikilink") -
    無名（詞；與[魏如萱合填](../Page/魏如萱.md "wikilink")）
  - [東亞](../Page/東亞唱片_\(集團\).md "wikilink")[群星](../Page/群星.md "wikilink")
    - 在歲月飛揚（詞）
  - [鄭秀文](../Page/鄭秀文.md "wikilink") - 愛（詞）
  - 鄭秀文 - Water of Love（詞）
  - 鄭秀文 - Water of Love（國）（詞）

### 2011年

  - [何韻詩](../Page/何韻詩.md "wikilink") - 青春祭（詞）
  - [蔡卓妍](../Page/蔡卓妍.md "wikilink") - 夏宇的愛情（詞）（以筆名**林家敏**發表）

### 2012年

  - [何韻詩](../Page/何韻詩.md "wikilink") - 青春祭（國）（詞）
  - [許志安](../Page/許志安.md "wikilink") - 心照不宣（詞）

### 2017年

  - [鄭秀文](../Page/鄭秀文.md "wikilink") -
    理智與感情（詞；與[陳耀森合填](../Page/陳耀森.md "wikilink")）

### 2019年

  - [盧凱彤](../Page/盧凱彤.md "wikilink") - 光（詞）

## 外部链接

[Category:香港經理人](../Category/香港經理人.md "wikilink")
[Category:香港填詞人](../Category/香港填詞人.md "wikilink")
[K](../Category/聖若瑟英文中學校友.md "wikilink")
[Category:香港中文大學校友](../Category/香港中文大學校友.md "wikilink")
[Category:觀塘瑪利諾書院校友](../Category/觀塘瑪利諾書院校友.md "wikilink")