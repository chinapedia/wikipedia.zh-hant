**靚元亨**，\[1\]，原名李雁秋，\[2\]又名寸度亨，號稱小武王，\[3\]清末民初的著名[粵劇演員](../Page/粵劇.md "wikilink")。早年出身自由[陳少白](../Page/陳少白.md "wikilink")、[程子儀](../Page/程子儀.md "wikilink")、[李紀堂所創立的](../Page/李紀堂.md "wikilink")[采南歌戲班](../Page/采南歌戲班.md "wikilink")，\[4\]後來組織了[永壽年戲班](../Page/永壽年戲班.md "wikilink")，在[南洋一帶尋找人才](../Page/南洋.md "wikilink")，如[陳非儂](../Page/陳非儂.md "wikilink")、[馬師曾](../Page/馬師曾.md "wikilink")、[薛覺先](../Page/薛覺先.md "wikilink")、[廖俠懷](../Page/廖俠懷.md "wikilink")、[何劍秋](../Page/何劍秋.md "wikilink")。由於他的南派武功紮實，分寸不差，行內稱他為「寸度亨」。\[5\]香港無線藝員[李子奇](../Page/李子奇.md "wikilink")，就是李雁秋的孫。李姓家族中有多人均在演藝界。

## 参考文献

<div class="references-small">

<references />

</div>

[Category:粵劇演員](../Category/粵劇演員.md "wikilink")
[Category:李姓](../Category/李姓.md "wikilink")

1.  大公報, 1966-02-18 第5頁
2.  [靚榮：粵劇傳奇“武生王”](http://www.zhgpl.com/crn-webapp/search/siteDetail.jsp?id=100276784&sw=%E5%87%BA%E6%BC%94),中國評論新聞網
3.  [香港粵劇面面觀講座-1](http://yuensiufai.com/news-talk-1.htm)
4.  [五邑名人故事：陈少白创立革命剧社](http://www.jmnews.com.cn/c/2006/03/22/09/c_846580.shtml)

5.  [淺論薛覺先的藝術成就（二）](http://www.wenweipo.com/news.phtml?news_id=EN0701090030&cat=011EN)
    ,文匯報,2007年1月9日