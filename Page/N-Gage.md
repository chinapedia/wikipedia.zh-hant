[Nokia_N-Gage.png](https://zh.wikipedia.org/wiki/File:Nokia_N-Gage.png "fig:Nokia_N-Gage.png")
[N-GageQD.png](https://zh.wikipedia.org/wiki/File:N-GageQD.png "fig:N-GageQD.png")
**N-Gage**是一款由[诺基亚发行的具有手機和遊戲功能的掌上游戏设备](../Page/诺基亚.md "wikilink")。此设备于2002年11月4日宣布并于2003年10月7日发行。\[1\]它在塞班OSV6.1的S60平台运行。相比同時期其它智能手機來說，它們具有很大的運行內存，運行遊戲非常流暢。它們的區別在於N-Gage
QD取消了N-Gage的立體聲喇叭和收音機。另外N-Gage沒有推出中文版本。

2004年，诺基亚通过重新设计了最初的N-Gage，推出了**N-Gage QD**。

2007年11月，N-Gage會改為另一種方式，登陸在Nokia最新型號的手機內，只要用戶的手機對應N-Gage平台，便可以付費下載最新的遊戲等。

## 对应游戏

## 参考来源

[Category:行動電話](../Category/行動電話.md "wikilink")
[Category:掌上遊戲機](../Category/掌上遊戲機.md "wikilink")
[Category:诺基亚手机](../Category/诺基亚手机.md "wikilink")
[Category:第六世代遊戲機](../Category/第六世代遊戲機.md "wikilink")
[Category:2003年面世的產品](../Category/2003年面世的產品.md "wikilink")

1.