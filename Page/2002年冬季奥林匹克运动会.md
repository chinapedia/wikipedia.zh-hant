</div>

</div>

[2002_Winter_Olympics_flame.jpg](https://zh.wikipedia.org/wiki/File:2002_Winter_Olympics_flame.jpg "fig:2002_Winter_Olympics_flame.jpg")
**第十九届冬季奥林匹克运动会**（，），於2002年2月8日至24日在[美国](../Page/美国.md "wikilink")[犹他州的](../Page/犹他州.md "wikilink")[盐湖城举行](../Page/盐湖城.md "wikilink")。
在本屆冬季奧運會舉行前，部分[國際奧委會成員因為被揭發不正當收受禮物](../Page/國際奧委會.md "wikilink")，以換取投票支持鹽湖城申辦而被迫令辭職。新任[國際奧委會主席](../Page/國際奧委會.md "wikilink")[雅克·羅格及](../Page/雅克·羅格.md "wikilink")[鹽湖城冬季奧運會行政總裁](../Page/鹽湖城.md "wikilink")[米特·罗姆尼及後主理大會並接受公眾對醜聞的嚴厲批評](../Page/米特·罗姆尼.md "wikilink")。

[911事件令本屆冬奧會成為奧運歷史上安全要求最高的一次](../Page/911事件.md "wikilink")，[美国國土安全部定義](../Page/美国國土安全部.md "wikilink")[奧運會為國家特別安全事項](../Page/奧運會.md "wikilink")（National
Special Security
Event、NSSE）。雅克·羅格首次以國際奧委會主席名義出席[奧運開幕典禮](../Page/奧運.md "wikilink")，表示主辦國及其運動員將會克服恐怖威脅，並且在共同信念下團結起來。然而，主辦國於開幕典禮展示一面在[世貿中心遺跡發掘的](../Page/世貿中心.md "wikilink")[美國國旗](../Page/美國國旗.md "wikilink")，被輿論批評為利用[奧運會作為政治宣傳](../Page/奧運會.md "wikilink")。

## 背景

<table>
<thead>
<tr class="header">
<th><p>第十九屆冬季奧林匹克運動會申辦結果<br />
1995年6月16日於<a href="../Page/匈牙利.md" title="wikilink">匈牙利</a><a href="../Page/布達佩斯.md" title="wikilink">布達佩斯</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>城市</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鹽湖城.md" title="wikilink">鹽湖城</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/錫永.md" title="wikilink">錫永</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/厄斯特松德.md" title="wikilink">厄斯特松德</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/魁北克市.md" title="wikilink">魁北克市</a></p></td>
</tr>
</tbody>
</table>

## 焦點

[中國在](../Page/中華人民共和國.md "wikilink")[短道速滑女子](../Page/短道速滑.md "wikilink")500米決賽中，[楊揚擊敗了](../Page/楊揚.md "wikilink")[保加利亞的](../Page/保加利亞.md "wikilink")[葉夫根尼亞·拉達諾娃和隊友](../Page/葉夫根尼亞·拉達諾娃.md "wikilink")[王春露](../Page/王春露.md "wikilink")，奪得了冠軍，為中國自[1980年冬季奥林匹克运动会參賽以來首枚金牌](../Page/1980年冬季奥林匹克运动会.md "wikilink")。此後，她在女子1000米比賽中再奪金牌。

## 比赛项目

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/越野滑雪.md" title="wikilink">越野滑雪</a></li>
<li><a href="../Page/自由式滑雪.md" title="wikilink">自由式滑雪</a></li>
<li><a href="../Page/跳台滑雪.md" title="wikilink">-{zh-hans:跳台滑雪;zh-hk:跳台滑雪;zh-tw:滑雪跳躍;}-</a></li>
<li><a href="../Page/有舵雪橇.md" title="wikilink">-{zh-hans:有舵雪橇;zh-hk:有舵雪橇;zh-tw:雪車;}-</a></li>
<li><a href="../Page/無舵雪橇.md" title="wikilink">無舵雪橇</a></li>
<li><a href="../Page/花样滑冰.md" title="wikilink">花样滑冰</a></li>
<li><a href="../Page/俯式冰橇.md" title="wikilink">俯式冰橇</a></li>
<li><a href="../Page/冰球.md" title="wikilink">冰球</a></li>
</ul></td>
<td></td>
<td><ul>
<li><a href="../Page/高山滑雪.md" title="wikilink">高山滑雪</a></li>
<li><a href="../Page/冰壺.md" title="wikilink">冰壺</a></li>
<li><a href="../Page/短道速滑.md" title="wikilink">短道速滑</a></li>
<li><a href="../Page/單板滑雪.md" title="wikilink">單板滑雪</a></li>
<li><a href="../Page/北歐兩項.md" title="wikilink">北歐兩項</a></li>
<li><a href="../Page/冬季兩項.md" title="wikilink">冬季兩項</a></li>
<li><a href="../Page/速度滑冰.md" title="wikilink">速度滑冰</a></li>
</ul></td>
</tr>
</tbody>
</table>

## 參賽國家及地區

[2002_Winter_Olympics_participants.PNG](https://zh.wikipedia.org/wiki/File:2002_Winter_Olympics_participants.PNG "fig:2002_Winter_Olympics_participants.PNG")
77个国家或地区参加了本届奥运会：

<table>
<tbody>
<tr class="odd">
<td><ul>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
</ul></td>
<td><ul>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
</ul></td>
<td><ul>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
</ul></td>
<td><ul>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
</ul></td>
</tr>
</tbody>
</table>

## 奖牌榜

<table>
<tbody>
<tr class="odd">
<td><p>style="border-left:0px"; | <a href="https://zh.wikipedia.org/wiki/File:Olympic_rings.svg" title="fig:Olympic_rings.svg">Olympic_rings.svg</a></p></td>
<td><p>colspan=6 style="border-right:0px;";| <a href="../Page/2002年冬季奥林匹克运动会奖牌榜.md" title="wikilink">2002年冬季奥林匹克运动会奖牌榜前</a>10位</p></td>
<td><p>style="border-left:0px"; ! align="center"| <a href="https://zh.wikipedia.org/wiki/File:Olympic_rings.svg" title="fig:Olympic_rings.svg">Olympic_rings.svg</a></p></td>
</tr>
<tr class="even">
<td><p><strong>名次</strong></p></td>
<td><p><strong>编码</strong></p></td>
<td><p><strong>队伍</strong></p></td>
</tr>
<tr class="odd">
<td><p>1</p></td>
<td><p>NOR</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>GER</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p>USA</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p>CAN</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p>RUS</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p>FRA</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p>ITA</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p>FIN</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p>NED</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p>AUT</p></td>
<td></td>
</tr>
</tbody>
</table>

## 參見

  - [2002年冬季残奥会](../Page/2002年冬季残奥会.md "wikilink")
  - [奥运会](../Page/奥运会.md "wikilink")
  - [冬奥会](../Page/冬奥会.md "wikilink")
  - [国际奥林匹克委员会](../Page/国际奥林匹克委员会.md "wikilink")
  - [2002年冬季奥林匹克运动会贿选丑闻](../Page/2002年冬季奥林匹克运动会贿选丑闻.md "wikilink")

## 參考資料

## 外部連結

  - [國際奧委會關於鹽湖城冬運會的資料](http://www.olympic.org/uk/games/past/index_uk.asp?OLGT=2&OLGY=2002)

{{-}}

[Category:冬季奥林匹克运动会](../Category/冬季奥林匹克运动会.md "wikilink")
[2002年冬季奧林匹克運動會](../Category/2002年冬季奧林匹克運動會.md "wikilink")
[奥林匹克运动会](../Category/2002年美国体育.md "wikilink")
[Category:奧林匹克運動會在美國](../Category/奧林匹克運動會在美國.md "wikilink")