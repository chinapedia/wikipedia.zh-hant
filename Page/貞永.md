**貞永**（1232年四月二日至1233年四月十五日）是[日本的](../Page/日本.md "wikilink")[年號之一](../Page/年號.md "wikilink")。這個時代的[天皇是](../Page/天皇.md "wikilink")[後堀河天皇與](../Page/後堀河天皇.md "wikilink")[四條天皇](../Page/四條天皇.md "wikilink")、[鎌倉幕府](../Page/鎌倉幕府.md "wikilink")[征夷大將軍為](../Page/征夷大將軍.md "wikilink")[藤原賴經](../Page/藤原賴經.md "wikilink")、[執權為](../Page/執權.md "wikilink")[北條泰時](../Page/北條泰時.md "wikilink")。

## 改元

  - 寬喜四年四月二日（1232年4月23日）改元貞永
  - 貞永二年四月十五日（1233年5月25日）改元天福

## 出處

  - 《[易經](../Page/易經.md "wikilink")》

## 出生

## 逝世

## 大事記

## 紀年、西曆、干支對照表

|                                    |                                |                                |
| ---------------------------------- | ------------------------------ | ------------------------------ |
| 貞永                                 | 元年                             | 二年                             |
| [儒略曆日期](../Page/儒略曆.md "wikilink") | 1232年                          | 1233年                          |
| [干支](../Page/干支.md "wikilink")     | [壬辰](../Page/壬辰.md "wikilink") | [癸巳](../Page/癸巳.md "wikilink") |

## 參考

  - [日本年號索引](../Page/日本年號索引.md "wikilink")
  - 同期存在的其他政權之紀年
      - [紹定](../Page/紹定.md "wikilink")（1228年正月至1233年十二月）：[宋](../Page/南宋.md "wikilink")—[宋理宗趙昀之年號](../Page/宋理宗.md "wikilink")
      - [天輔](../Page/天輔_\(段智祥\).md "wikilink")：[大理](../Page/大理國.md "wikilink")—[段智祥之年號](../Page/段智祥.md "wikilink")
      - [仁壽](../Page/仁壽_\(段智祥\).md "wikilink")：大理—段智祥之年號
      - [建中](../Page/建中_\(陳太宗\).md "wikilink")（1226年至1232年）：[陳朝](../Page/陳朝_\(越南\).md "wikilink")—[陳太宗陳日煚之年號](../Page/陳太宗.md "wikilink")
      - [天應政平](../Page/天應政平.md "wikilink")（1232年至1254年）：陳朝—陳日煚之年號
      - [開興](../Page/開興.md "wikilink")（1232年正月至四月）：[金](../Page/金朝.md "wikilink")—[金哀宗完顏守緒之年號](../Page/金哀宗.md "wikilink")
      - [天興](../Page/天興_\(金哀宗\).md "wikilink")（1232年四月至1234正月）：金—金哀宗完顏守緒之年號
      - [大同](../Page/大同_\(蒲鮮萬奴\).md "wikilink")（1224年正月至1233年九月）：金時期—[蒲鮮萬奴之年號](../Page/蒲鮮萬奴.md "wikilink")

## 參考文獻

  - 松橋達良，《元号はやわかり—東亜歷代建元考》，砂書房，1994年7月，ISBN 4915818276
  - 李崇智，《中国历代年号考》，中华书局，2001年1月ISBN 7101025129

[Category:13世纪日本年号](../Category/13世纪日本年号.md "wikilink")
[Category:1230年代日本](../Category/1230年代日本.md "wikilink")