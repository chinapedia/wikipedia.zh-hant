**金黄色葡萄球菌**（[學名](../Page/學名.md "wikilink")：）为一种[革兰氏染色阳性球型](../Page/革兰氏阳性菌.md "wikilink")[细菌](../Page/细菌.md "wikilink")。工业上利用金黄色葡萄球菌制备[蛋白质A](../Page/蛋白质A.md "wikilink")——抗激素化学分析中的细胞壁组成成分。

## 形态

[Staphylococcus_aureus_on_TSA.jpg](https://zh.wikipedia.org/wiki/File:Staphylococcus_aureus_on_TSA.jpg "fig:Staphylococcus_aureus_on_TSA.jpg")使在[琼脂上生长的金黃色葡萄球菌拥有金黄色的色澤](../Page/琼脂.md "wikilink")\]\]
金黃色葡萄球菌在[显微镜下排列成葡萄串状](../Page/显微镜.md "wikilink")，金黄色葡萄球菌无[芽孢](../Page/芽孢.md "wikilink")、[鞭毛](../Page/鞭毛.md "wikilink")，大多数无[荚膜](../Page/荚膜.md "wikilink")。是常见的引起[食物中毒的致病菌](../Page/食物中毒.md "wikilink")。常见于皮肤表面及上呼吸道黏膜。金黄色葡萄球菌可以适应含高浓度氯化钠的培养基，因此可以用这种选择培养基将该菌分离出来。

## 致病菌种

金黄色葡萄球菌為表皮之正常菌叢，常造成伺機性感染。细菌引起不同程度的化脓性炎症扩散疾病，如疖、痈、中耳炎、鼻窦炎、骨髓炎和脓毒病等。金黄色葡萄球菌已经进化出了抵抗免疫系统攻击的特征。

在金黃色葡萄球菌的种类中，[抗藥性金黃色葡萄球菌](../Page/抗藥性金黃色葡萄球菌.md "wikilink")（超級細菌）很难治疗。

### 对被白血球釋放的活性氧的自卫

金黄色葡萄球菌的金黄色来源于细菌所制造的[类胡萝卜素](../Page/类胡萝卜素.md "wikilink")[葡萄球菌黄素](../Page/葡萄球菌黄素.md "wikilink")，这色素可以抵抗身体[免疫系统为了杀死病菌而制造的被白血球釋放的活性氧](../Page/免疫系统.md "wikilink")。这样金黄色葡萄球菌就可以免于被活性氧杀死。\[1\]

## 参考文献

## 外部連結

  - [微生物免疫學-Staphylococcus
    aureus(金黃色葡萄球菌)](http://smallcollation.blogspot.com/2013/01/staphylococcus-aureus.html)

[Category:葡萄球菌科](../Category/葡萄球菌科.md "wikilink")

1.