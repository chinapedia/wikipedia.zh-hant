**野牛龍屬**（[學名](../Page/學名.md "wikilink")：*Einiosaurus*）是[角龍科下的中型](../Page/角龍科.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，屬於[角龍亞科](../Page/角龍亞科.md "wikilink")，其[化石在](../Page/化石.md "wikilink")[美國](../Page/美國.md "wikilink")[蒙大拿州的](../Page/蒙大拿州.md "wikilink")[雙麥迪遜組發現](../Page/雙麥迪遜組.md "wikilink")，年代為[上白堊紀](../Page/上白堊紀.md "wikilink")，約為7,500萬年前。屬名是[阿爾岡昆語](../Page/阿爾岡昆諸語言.md "wikilink")「野牛」與[古希臘文](../Page/古希臘文.md "wikilink")「[蜥蜴](../Page/蜥蜴.md "wikilink")」的意思，而[種小名則是](../Page/種小名.md "wikilink")[拉丁文及](../Page/拉丁文.md "wikilink")[古希臘文](../Page/古希臘文.md "wikilink")「向前彎的角」的意思。

## 化石

[Einiosaurus_skull.jpg](https://zh.wikipedia.org/wiki/File:Einiosaurus_skull.jpg "fig:Einiosaurus_skull.jpg")\]\]
[Einiosaurus_procurvicornis.jpg](https://zh.wikipedia.org/wiki/File:Einiosaurus_procurvicornis.jpg "fig:Einiosaurus_procurvicornis.jpg")的[頂骨](../Page/頂骨.md "wikilink")\]\]
野牛龍的化石只有在[美國](../Page/美國.md "wikilink")[蒙大拿州被發現](../Page/蒙大拿州.md "wikilink")，所有已知的[化石現都存放在蒙大拿州](../Page/化石.md "wikilink")[落磯山博物館](../Page/落磯山博物館.md "wikilink")。目前已發現最少有15頭不同年齡的野牛龍化石，包含三個[頭顱骨](../Page/頭顱骨.md "wikilink")，以及發現於兩個低密度[屍骨層的上百件骨頭](../Page/屍骨層.md "wikilink")。這些化石都是由[傑克·霍納](../Page/傑克·霍納.md "wikilink")（Jack
Horner）在1985年發現，並由落磯山博物館的挖掘隊伍在之後4年間陸續挖出。這些屍骨層原先被認為包括了[戟龍的新種化石](../Page/戟龍.md "wikilink")\[1\]。在1990年，[斯特芬·柯瑞克斯](../Page/斯特芬·柯瑞克斯.md "wikilink")（Stephen
Czerkas）在一份文獻裡用「"*Styracosaurus
makeli*"」，使用在這些化石，但沒有經過正式研究的發表，因此是個[無資格名稱](../Page/無資格名稱.md "wikilink")\[2\]。在1992年，[傑克·霍納將個屍骨層的角龍類化石](../Page/傑克·霍納.md "wikilink")，分類為Type
A、B、與C，共計三個未命名種\[3\]。在1995年，[史考特·山普森](../Page/史考特·山普森.md "wikilink")（Scott
D. Sampson）將Type B正式描述及命名為野牛龍屬，[正模標本為編號MOR](../Page/正模標本.md "wikilink")
456標本；他也把相同屍骨層的Type A命名為[河神龍屬](../Page/河神龍屬.md "wikilink")\[4\]。

## 描述

野牛龍是[草食性](../Page/草食性.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，身長被估計可達4.5公尺長，體重被估計約1.3公噸\[5\]。野牛龍的口鼻部狹窄、前端尖，牠們通常被描繪成有一個低矮、大幅向前彎的鼻角，就像一個開瓶器，不過這個角可能只在成年個體中才有。野牛龍與有明顯額角的[角龍科](../Page/角龍科.md "wikilink")（如[三角龍](../Page/三角龍.md "wikilink")）不同，牠們的額角是較低、較短。野牛龍的頭盾較小型，頂端有一對大的[頸盾緣骨突](../Page/頸盾緣骨突.md "wikilink")，向後方延伸。

## 埋葬學及古生物學

低密度的屍骨層代表有群體動物在災難（如旱災或洪水）中集體死去。這證實了野牛龍及其他的[尖角龍亞科](../Page/尖角龍亞科.md "wikilink")（如[厚鼻龍及](../Page/厚鼻龍.md "wikilink")[尖角龍](../Page/尖角龍.md "wikilink")）都是群居的動物，就像現今的[美洲野牛或](../Page/美洲野牛.md "wikilink")[角馬](../Page/角馬.md "wikilink")。相反地，[開角龍亞科](../Page/開角龍亞科.md "wikilink")（如[三角龍及](../Page/三角龍.md "wikilink")[牛角龍](../Page/牛角龍.md "wikilink")）通常被發現的是單獨的[化石](../Page/化石.md "wikilink")，因此牠們被認為是獨居的動物，不過有[足跡化石推翻這種假說](../Page/足跡化石.md "wikilink")\[6\]。在2010年，一群科學家研究野牛龍的多個化石，發現野牛龍的成長快速，直到三至五歲時，生長速率才緩慢下來，可能是未達到性成熟\[7\]

就像其他的[角龍科](../Page/角龍科.md "wikilink")，牠有複雜的[齒系可以咬碎最粗糙的](../Page/齒系.md "wikilink")[植物](../Page/植物.md "wikilink")\[8\]。野牛龍生存於內陸環境\[9\]。

野牛龍的化石發現於[蒙大拿州的](../Page/蒙大拿州.md "wikilink")[雙麥迪遜組](../Page/雙麥迪遜組.md "wikilink")，地質年代為[白堊紀晚期的](../Page/白堊紀.md "wikilink")[坎潘階中晚期](../Page/坎潘階.md "wikilink")，約7500萬到7000萬年前。同期的恐龍包括：基礎[鳥腳下目的](../Page/鳥腳下目.md "wikilink")[奔山龍](../Page/奔山龍.md "wikilink")、[鴨嘴龍科的](../Page/鴨嘴龍科.md "wikilink")[亞冠龍](../Page/亞冠龍.md "wikilink")、[慈母龍及](../Page/慈母龍.md "wikilink")[原櫛龍](../Page/原櫛龍.md "wikilink")、[甲龍科的](../Page/甲龍科.md "wikilink")[埃德蒙頓甲龍及](../Page/埃德蒙頓甲龍.md "wikilink")[包頭龍](../Page/包頭龍.md "wikilink")、[暴龍科的](../Page/暴龍科.md "wikilink")[懼龍](../Page/懼龍.md "wikilink")，以及小型的[獸腳亞目](../Page/獸腳亞目.md "wikilink")[斑比盜龍](../Page/斑比盜龍.md "wikilink")、[纖手龍及](../Page/纖手龍.md "wikilink")[傷齒龍](../Page/傷齒龍.md "wikilink")，[反鳥亞綱的](../Page/反鳥亞綱.md "wikilink")[鳥龍鳥](../Page/鳥龍鳥.md "wikilink")，及角龍科的[短角龙及](../Page/短角龙.md "wikilink")[河神龍](../Page/河神龍.md "wikilink")。野牛龍生活於溫暖及半乾燥的的季節性環境。其他與野牛龍一同發現的化石包括有[雙殼綱及](../Page/雙殼綱.md "wikilink")[腹足綱](../Page/腹足綱.md "wikilink")，野牛龍的骨頭被認為是埋在淺湖之中。

[Nasals_of_Einiosaurus.jpg](https://zh.wikipedia.org/wiki/File:Nasals_of_Einiosaurus.jpg "fig:Nasals_of_Einiosaurus.jpg")

## 種系發生學

野牛龍在尖角龍亞科中的[種系發生學位置有些爭議](../Page/種系發生學.md "wikilink")，這是由於野牛龍[頭顱骨有幾個過渡性的特徵](../Page/頭顱骨.md "wikilink")，牠們的最近親應為[尖角龍及](../Page/尖角龍.md "wikilink")[戟龍](../Page/戟龍.md "wikilink")，或是[河神龍及](../Page/河神龍.md "wikilink")[厚鼻龍](../Page/厚鼻龍.md "wikilink")。後來有假說指出野牛龍是[厚鼻龍族演化過程中的最早期物種](../Page/厚鼻龍族.md "wikilink")，其後為河神龍及厚鼻龍，鼻角逐漸演化成圓形隆起，而頭盾亦發展得更為複雜\[10\]。不論哪一個假說是正確的，野牛龍似乎是在尖角龍亞科演化的中間位置。

## 參考資料

<div class="references-small">

<references>

</references>

</div>

## 外部連結

  - [恐龍博物館——野牛龍](http://www.fossil.org.cn/museum/Einiosaurus.htm)
  - [*Einiosaurus*](http://www.dinodata.org/index.php?option=com_content&task=view&id=6517&Itemid=67)
    at DinoData
  - [*Einiosaurus*, from the Dinosaur
    Encyclopaedia](https://web.archive.org/web/20050418101342/http://www.isgs.uiuc.edu/dinos/de_4/5a7954a.htm)
    at Dino Russ's Lair

[Category:尖角龍亞科](../Category/尖角龍亞科.md "wikilink")
[Category:北美洲恐龍](../Category/北美洲恐龍.md "wikilink")
[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")

1.

2.  Czerkas, S.J. & Czerkas, S.A., 1990, *Dinosaurs. a Global View*,
    Dragons’ World, 247 pp

3.
4.

5.  Paul, G.S., 2010, *The Princeton Field Guide to Dinosaurs*,
    Princeton University Press p. 262

6.
7.  Reizner, J., 2010, ''An ontogenetic series and population histology
    of the ceratopsid dinosaur *Einiosaurus procurvicornis*.'' Montana
    State University master’s thesis, pp 97

8.

9.  Lehman, T. M., 2001, Late Cretaceous dinosaur provinciality: In:
    Mesozoic Vertebrate Life, edited by Tanke, D. H., and Carpenter, K.,
    Indiana University Press, pp. 310–328.

10.