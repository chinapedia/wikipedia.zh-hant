**3DLABS**（三維實驗室）成立於1984年，曾是一家[無廠半導體公司](../Page/無廠半導體公司.md "wikilink")，目前已歇業。3DLABS總部位於[美國](../Page/美國.md "wikilink")[加利福尼亞州的](../Page/加利福尼亞州.md "wikilink")[森尼韋爾](../Page/森尼韦尔_\(加利福尼亚州\).md "wikilink")。3DLABS主要开發高端繪圖晶片。其著名產品是Wildcat與Oxygen系列顯示卡。

[ELSA_Gloria_L-88.jpg](https://zh.wikipedia.org/wiki/File:ELSA_Gloria_L-88.jpg "fig:ELSA_Gloria_L-88.jpg")

## 公司歷史

公司前身創建於1984年，早期公司名為benchMark Technology，1988年與DuPont公司合併，旗下成立DuPont
Pixel，開始為[升陽開發IRISGL工作站顯示卡](../Page/升陽.md "wikilink")，在1994年4月從DuPont分離正式更名為3DLABS，專為個人電腦開發繪圖晶片。1995年發表當時世界最快的商業OpenGL顯示晶片GLINT
300SX。1996年10月在美國[NASDAQ上市](../Page/NASDAQ.md "wikilink")。其後在1998年7月收購了Dynamic
Pictures，2000年7月收購了Intense3D。最後在2002年6月被[创新科技收購](../Page/创新科技.md "wikilink")，成為旗下子公司。2009年1月與創新科技的個人數位娛樂部門合併，更名為[ZiiLABS](../Page/ZiiLABS.md "wikilink")（籽亿）公司。

3DLABS曾是[OpenGL架构评估委员会](../Page/OpenGL.md "wikilink")（OpenGL
ARB）與[Khronos
Group的成員](../Page/Khronos_Group.md "wikilink")。它是該組織的重要一員，尤其是在开發OpenGL
2.0和不間斷改进OpenGL
API方面。在1999年之前，3DLABS一直是一家还跟得上时代的主流显示芯片制造商，但由于Permedia3研发不顺利，被[NVIDIA迅速甩离主流显卡芯片制造商的地位](../Page/NVIDIA.md "wikilink")。

## 产品

[Dynamic_Pictures_Oxygen402.jpg](https://zh.wikipedia.org/wiki/File:Dynamic_Pictures_Oxygen402.jpg "fig:Dynamic_Pictures_Oxygen402.jpg")

  - 3DLABS Wildcat Realizm 100, 200, 500, 800
  - 3DLABS Wildcat 4 7110, 7210
  - 3DLABS Wildcat VP560, VP570, VP760, VP870, VP880 Pro, VP990, VP990
    Pro
  - 3DLABS Oxygen RPM, GMX, 102, 202, 402, V192, VX1, VX1-1600SW, GVX1,
    GVX1 Pro, GVX210, GVX420
  - 3DLABS Wildcat III 6110, 6210
  - 3DLABS Wildcat II 5000, 5110
  - 3DLABS Wildcat 4000, 4105, 4110, 4210
  - 3DLABS Intense 3D 1000, 2200, 2200S, 3400, 3410, 3510, 3600

## 參考連結

  - [創新科技](../Page/創新科技.md "wikilink")
  - [ZiiLABS](../Page/ZiiLABS.md "wikilink")（籽亿）

## 外部連結

  - [3DLABS
    官方網站](https://web.archive.org/web/20051217211053/http://www.3dlabs.com/)
  - [ZiiLABS
    （籽亿）官方網站](https://web.archive.org/web/20111016193609/http://www.ziilabs.com/)

[Category:電腦硬件公司](../Category/電腦硬件公司.md "wikilink")
[Category:顯示設備硬件公司](../Category/顯示設備硬件公司.md "wikilink")
[Category:已結業電腦公司](../Category/已結業電腦公司.md "wikilink")
[Category:1984年成立的公司](../Category/1984年成立的公司.md "wikilink")