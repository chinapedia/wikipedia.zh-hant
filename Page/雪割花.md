**雪割花**（）是，1998年11月26日由[索尼電腦娛樂發售的](../Page/索尼電腦娛樂.md "wikilink")[PS遊戲軟體](../Page/PlayStation.md "wikilink")，為《[YARUDORA](../Page/YARUDORA.md "wikilink")》（）系列的第4款作品。動畫製作是[Production
I.G](../Page/Production_I.G.md "wikilink")。2005年7月28日在[PSP上發售了重製版](../Page/PlayStation_Portable.md "wikilink")。

## 作品概要

描寫春夏秋冬的《YARUDORA》系列的「冬」。象徵花為[雪割草](../Page/雪割草.md "wikilink")*Hepatica
nobilis*。與另外三個作品的動畫觸感呈強烈對比，人物設定相當獨特的關係，有許多玩家敬而遠之。

另外，遊戲途中常常因為些小事情使得女主角記憶復甦，變成悲慘的結局（bad
end）；悲慘結局的頻率是《YARUDORA》系列中第一名，是PS上唯一沒有普通結局的作品。

## 劇情大綱

時間正值冬季，住在北國一間廉價公寓的主角，愛上了住在隔壁房間的女性櫻木花織。有天晚上主角目睹花織和男人抱在一起。某天警察來到失意的男主角住處，告知說花織喪失了記憶，目前人在醫院裡。為了因男友去世而封印記憶的女主角，主角決定要成為花織的男友。

## 登場人物

  - 主角 （配音：[檜山修之](../Page/檜山修之.md "wikilink")）
    20歳。就讀道南大学的学生。在大學附近的公寓獨自一人居住。（主角的聲音可以關掉）

  - 櫻木花織（） （配音：[日高法子](../Page/日高法子.md "wikilink")）
    住在主角隔壁的OL。年幼時與父母離異，由祖母帶大。但是祖母也在花織念短期大學時過世。性格上有點愛夢想，容易情緒激動，也有些比較幼稚的地方。

  - 伊達昂（）
    花織的恋人。兩人在3年前認識。不過在[美國](../Page/美國.md "wikilink")[邁阿密出差時因事故死亡](../Page/邁阿密.md "wikilink")。

  - 小林勇一（） （配音：[堀内賢雄](../Page/堀内賢雄.md "wikilink")）
    小林美雪（） （配音：[淵崎有-{里}-子](../Page/淵崎有里子.md "wikilink")）
    相川教授（） （配音：[飯塚昭三](../Page/飯塚昭三.md "wikilink")）

## 主要工作人員

  - 制作人及总监修：[东乡光宏](../Page/东乡光宏.md "wikilink")
  - 动画监督、演出：[西久保瑞穗](../Page/西久保瑞穗.md "wikilink")
  - 劇本：关岛真赖
  - 人物设定：[荒川真嗣](../Page/荒川真嗣.md "wikilink")
  - 主題歌「GHOST DANCE」
      - 歌：Yu-Kalie，作词及作曲：岩村缘，编曲：上村佳弘、今井了介

## 相關條目

  - [雙面嬌娃](../Page/雙面嬌娃.md "wikilink")（）
  - [擁抱季節](../Page/擁抱季節.md "wikilink")（）
  - [茉莉花 (遊戲)](../Page/茉莉花_\(遊戲\).md "wikilink")（）

[Category:1998年电子游戏](../Category/1998年电子游戏.md "wikilink")
[Category:PlayStation
(游戏机)游戏](../Category/PlayStation_\(游戏机\)游戏.md "wikilink")
[Category:PlayStation
Portable游戏](../Category/PlayStation_Portable游戏.md "wikilink")
[Category:Production I.G](../Category/Production_I.G.md "wikilink")
[Category:索尼互動娛樂遊戲](../Category/索尼互動娛樂遊戲.md "wikilink")
[Category:函館市背景作品](../Category/函館市背景作品.md "wikilink")
[Category:冒險遊戲](../Category/冒險遊戲.md "wikilink")