[Sanchi2.jpg](https://zh.wikipedia.org/wiki/File:Sanchi2.jpg "fig:Sanchi2.jpg")

**窣堵坡**\[1\]（[梵文](../Page/梵文.md "wikilink")：स्तूप，*stūpa*；[巴利文](../Page/巴利文.md "wikilink")：Thūpa），又譯**卒塔婆**、**窣都婆**、**窣堵波**、**私偷簸**、**塔婆**、**率都婆**、**素覩波**、**藪斗婆**等\[2\]\[3\]，对中国影响最深的名称是“卒塔婆”，也就是“塔”的名称来源。爲印度地區[墓地墳包式樣](../Page/墓.md "wikilink")，後爲佛教所用；在[印度](../Page/印度.md "wikilink")、[巴基斯坦](../Page/巴基斯坦.md "wikilink")、[尼泊尔等](../Page/尼泊尔.md "wikilink")[南亚国家及](../Page/南亚.md "wikilink")[泰國](../Page/泰國.md "wikilink")、[緬甸等](../Page/緬甸.md "wikilink")[东南亚国家比较普遍](../Page/东南亚.md "wikilink")。在佛教中因為通常是儲放佛陀、高僧[舍利所用](../Page/舍利.md "wikilink")，故亦稱爲**舍利塔**、**佛塔**、**浮屠塔**。

## 窣堵坡的起源

[印度的窣堵坡原是埋葬佛祖](../Page/印度.md "wikilink")[释迦牟尼火化后留下的](../Page/释迦牟尼.md "wikilink")[舍利的一种](../Page/舍利.md "wikilink")[佛教建筑](../Page/佛教.md "wikilink")，窣堵坡就是坟塚的意思。开始为纪念佛祖释迦牟尼，在佛出生、涅槃的地方都要建塔，随着佛教在各地的发展，在佛教盛行的地方也建起很多塔，争相供奉佛舍利。后来塔也成为高僧圆寂后埋藏舍利的建筑。

## 窣堵坡的形式

[ButkaraStupa.jpg](https://zh.wikipedia.org/wiki/File:ButkaraStupa.jpg "fig:ButkaraStupa.jpg")(例1)、[印度-希臘王國](../Page/印度-希臘王國.md "wikilink")(例2、3)、[印度-塞人王國](../Page/印度-塞人王國.md "wikilink")(例4)和[貴霜帝國](../Page/貴霜帝國.md "wikilink")(例5)各時期的演化過程。\]\]

关于窣堵坡的形式，许多佛经都有记载，其中[律部](../Page/律部.md "wikilink")《[根本说一切有部毗奈耶杂事](../Page/根本说一切有部毗奈耶杂事.md "wikilink")》中记载：

我今欲於顯敞之處以尊者（指舍利弗）骨起窣堵波。得使眾人隨情供養。佛言長者隨意當作。長者便念。云
何而作。[佛言應可用磚兩重作基](../Page/佛.md "wikilink")。次安塔身上安覆缽。隨意高下上置平頭。高一二尺方二三尺。準量大小中豎輪竿次著相輪。其相輪重數。或一二三四
乃至十三。次安寶瓶。長者自念。唯[舍利子得作如此窣堵波耶](../Page/舍利子.md "wikilink")。為餘亦得。即往白佛。佛告長者若為如來造窣堵波者。應可如前具足而作。若為獨覺勿安寶瓶。若阿羅漢相輪四重。不還至三。一來應二。預流應一。凡夫善人但可平頭無有輪蓋

## 窣堵坡传入汉地

东汉时传入中国的与当时中国本土的建筑相结合形成了中国的[塔](../Page/塔.md "wikilink")，尤其是舍利塔，古中国[楼阁式塔的塔刹就是窣堵坡造型的](../Page/楼阁式塔.md "wikilink")。这类塔称为佛塔或浮屠塔。

### 各地知名窣堵坡

  - [舍利塔
    (桂林)](../Page/舍利塔_\(桂林\).md "wikilink")，位于广西壮族自治区[桂林市](../Page/桂林市.md "wikilink")，是[广西壮族自治区文物保护单位](../Page/广西壮族自治区文物保护单位.md "wikilink")。
  - [慈云寺塔
    (赣州)](../Page/慈云寺塔_\(赣州\).md "wikilink")，又称舍利塔，位于江西省[赣州市](../Page/赣州市.md "wikilink")，是[全国重点文物保护单位](../Page/全国重点文物保护单位.md "wikilink")。
  - [栖霞寺舍利塔](../Page/栖霞寺舍利塔.md "wikilink")，位于江苏省[南京市](../Page/南京市.md "wikilink")，是全国重点文物保护单位。
  - [无垢净光舍利塔](../Page/无垢净光舍利塔.md "wikilink")，位于辽宁省[沈阳市](../Page/沈阳市.md "wikilink")，是[辽宁省文物保护单位](../Page/辽宁省文物保护单位.md "wikilink")。
  - [湖镇舍利塔](../Page/湖镇舍利塔.md "wikilink")，位于浙江省[龙游县](../Page/龙游县.md "wikilink")，是全国重点文物保护单位。
  - [开福寺舍利塔](../Page/开福寺舍利塔.md "wikilink")，位于河北省[景县](../Page/景县.md "wikilink")，是全国重点文物保护单位。
  - [鸠摩罗什舍利塔](../Page/鸠摩罗什舍利塔.md "wikilink")，位于陕西省[户县](../Page/户县.md "wikilink")，是全国重点文物保护单位。
  - [宝轮寺舍利塔](../Page/宝轮寺舍利塔.md "wikilink")，位于河南省[三门峡市](../Page/三门峡市.md "wikilink")，是[河南省文物保护单位](../Page/河南省文物保护单位.md "wikilink")。
  - [兴国寺舍利塔](../Page/兴国寺舍利塔.md "wikilink")，位于河南省[邓州市](../Page/邓州市.md "wikilink")，是河南省文物保护单位。
  - [延庆寺舍利塔](../Page/延庆寺舍利塔.md "wikilink")，位于河南省[济源市](../Page/济源市.md "wikilink")，是河南省文物保护单位。
  - [禅林寺舍利塔](../Page/禅林寺舍利塔.md "wikilink")，位于河北省[赵县](../Page/赵县.md "wikilink")，是[河北省文物保护单位](../Page/河北省文物保护单位.md "wikilink")。
  - [武安舍利塔](../Page/武安舍利塔.md "wikilink")，位于河北省[武安县](../Page/武安县.md "wikilink")，是河北省文物保护单位。
  - [金山寺舍利塔](../Page/金山寺舍利塔.md "wikilink")，位于河北省[涞水县](../Page/涞水县.md "wikilink")，是河北省文物保护单位。
  - [岳麓山舍利塔](../Page/岳麓山舍利塔.md "wikilink")，位于湖南省长沙市岳麓山，是[长沙市文物保护单位](../Page/长沙市文物保护单位.md "wikilink")。

## 窣堵坡传入藏地

窣堵坡早期在汉地發展为覆钵式塔，再传入藏地，成为藏传佛教佛塔的典型样式。藏语称佛塔爲“曲登”（)。

## 参见

  - [浮屠](../Page/浮屠.md "wikilink")
  - [塔](../Page/塔.md "wikilink")
  - [覆钵式塔](../Page/覆钵式塔.md "wikilink")
  - [桑吉](../Page/桑吉.md "wikilink")

## 注釋

[Category:塔](../Category/塔.md "wikilink")
[Category:佛教術語](../Category/佛教術語.md "wikilink")
[Category:佛教历史](../Category/佛教历史.md "wikilink")
[Category:梵语词汇](../Category/梵语词汇.md "wikilink")
[Category:巴利語詞彙](../Category/巴利語詞彙.md "wikilink")

1.  窣，大陸[普通话拼音sū](../Page/普通话.md "wikilink")，讀同蘇；台灣[國語注音ㄙㄨˋ](../Page/國語.md "wikilink")，讀同素；粵語seot；閩南語sut
2.  《佛學辭典》(高觀盧編纂)：「率都婆，又作窣堵波、窣覩波、素覩波、藪斗婆。」
3.