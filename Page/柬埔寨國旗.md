現時的**[柬埔寨國旗](../Page/柬埔寨.md "wikilink")**是以[紅](../Page/紅.md "wikilink")、[藍及](../Page/藍.md "wikilink")[白色為主色](../Page/白色.md "wikilink")，正中間白色殿堂為[吳哥窟](../Page/吳哥窟.md "wikilink")，被紅及藍條包圍著（線條比例
1:2:1）紅色代表民族，白色代表佛教，藍色象徵王室，符合柬國的國家銘言「民族、宗教、國王」。在1993年柬埔寨政體回到[君主制後重新使用](../Page/君主制.md "wikilink")。

## 历史

自1859年起，柬埔寨便以吳哥窟作為主圖，設計出第一面代表柬埔寨的國旗，並在1948年獨立後沿用至今。1970年10月9日，[柬埔寨內戰結束](../Page/柬埔寨內戰.md "wikilink")，[龍諾建立的](../Page/龍諾.md "wikilink")[高棉共和國採用了新國旗](../Page/高棉共和國.md "wikilink")，這面國旗才第一次被取代。而紅色背景和黃色吳哥窟構成了[民主柬埔寨的國旗](../Page/民主柬埔寨.md "wikilink")；1979年，[柬埔寨人民共和國成立](../Page/柬埔寨人民共和國.md "wikilink")，把前政权國旗稍做修改，卻沒有被[联合國所承認](../Page/联合國.md "wikilink")。隨後，柬埔寨都曾使用過其它方案的國旗，但都沒有脫離吳哥窟的設計構思。1993年開始，這面國旗被重新使用。

| 國旗                                                                                                                                                                                             | 國家                                                   | 使用          |
| ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------- | ----------- |
| [Flag_of_Cambodia_(pre-1863).svg](https://zh.wikipedia.org/wiki/File:Flag_of_Cambodia_\(pre-1863\).svg "fig:Flag_of_Cambodia_(pre-1863).svg")                                               | [古代柬埔寨王國](../Page/柬埔寨的黑暗时代.md "wikilink")            | 1863年以前     |
| [Flag_of_Cambodia_under_French_protection.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Cambodia_under_French_protection.svg "fig:Flag_of_Cambodia_under_French_protection.svg")        | [柬埔寨保護國](../Page/柬埔寨保護國.md "wikilink")               | 1863年－1887年 |
| [Flag_of_France.svg](https://zh.wikipedia.org/wiki/File:Flag_of_France.svg "fig:Flag_of_France.svg")                                                                                         | [法属印度支那](../Page/法属印度支那.md "wikilink")               | 1887年－1942年 |
| [Flag_of_Cambodia_under_Japanese_occupation.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Cambodia_under_Japanese_occupation.svg "fig:Flag_of_Cambodia_under_Japanese_occupation.svg")  | [日佔柬埔寨](../Page/日佔柬埔寨.md "wikilink")                 | 1942年－1945年 |
| [Flag_of_Cambodia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Cambodia.svg "fig:Flag_of_Cambodia.svg")                                                                                   | [法屬印度支那](../Page/法屬印度支那.md "wikilink")               | 1948年－1953年 |
| [Flag_of_Cambodia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Cambodia.svg "fig:Flag_of_Cambodia.svg")                                                                                   | [柬埔寨王国](../Page/柬埔寨王国_\(1953年－1970年\).md "wikilink") | 1953年－1970年 |
| [Flag_of_the_Khmer_Republic.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_Khmer_Republic.svg "fig:Flag_of_the_Khmer_Republic.svg")                                                   | [高棉共和國](../Page/高棉共和國.md "wikilink")                 | 1970年－1975年 |
| [Flag_of_Cambodia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Cambodia.svg "fig:Flag_of_Cambodia.svg")                                                                                   | [柬埔寨王國民族團結政府](../Page/柬埔寨王國民族團結政府.md "wikilink")     | 1970年－1976年 |
| [Flag_of_Democratic_Kampuchea.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Democratic_Kampuchea.svg "fig:Flag_of_Democratic_Kampuchea.svg")                                              | [民主柬埔寨](../Page/民主柬埔寨.md "wikilink")                 | 1975年－1979年 |
| [Flag_of_the_People's_Republic_of_Kampuchea.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_People's_Republic_of_Kampuchea.svg "fig:Flag_of_the_People's_Republic_of_Kampuchea.svg") | [柬埔寨人民共和國](../Page/柬埔寨人民共和國.md "wikilink")           | 1979年－1989年 |
| [Flag_of_the_State_of_Cambodia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_State_of_Cambodia.svg "fig:Flag_of_the_State_of_Cambodia.svg")                                         | [柬埔寨国](../Page/柬埔寨国.md "wikilink")                   | 1989年－1992年 |
| [Flag_of_Cambodia_under_UNTAC.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Cambodia_under_UNTAC.svg "fig:Flag_of_Cambodia_under_UNTAC.svg")                                             | [聯柬權力機構](../Page/联合国柬埔寨过渡时期权力机构.md "wikilink")       | 1992年－1993年 |
| [Flag_of_Cambodia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Cambodia.svg "fig:Flag_of_Cambodia.svg")                                                                                   | [柬埔寨王國](../Page/柬埔寨王國.md "wikilink")                 | 1993年至今     |

## 參見

  - [柬埔寨](../Page/柬埔寨.md "wikilink")
  - [國旗](../Page/國旗.md "wikilink")
  - [國徽](../Page/國徽.md "wikilink")

[J](../Category/国旗.md "wikilink")
[Category:柬埔寨国家象征](../Category/柬埔寨国家象征.md "wikilink")
[Category:亞洲國旗](../Category/亞洲國旗.md "wikilink")