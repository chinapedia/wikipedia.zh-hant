}}}} | image = {{\#invoke:InfoboxImage|InfoboxImage|image=}}}}}

| headerstyle = background-color: gainsboro;

| label1 = 車隊名稱 | data1 = }}} | class1 = fn org | label2 = 所在地 | data2 =
}}} | class2 = label | label3 = 車隊主席 | data3 = }}} | class3 = agent |
label4 = }}} | data4 = {{\#if:}}}|}}}}} | class4 = agent | label5 = 技術指導
| data5 = }}} | class5 = agent | label6 = 創辦人 | data6 = }}} | class6 =
agent | label7 = 著名員工 | data7 = }}} | class7 = agent | label8 = 著名車手 |
data8 = }}} | class8 = agent | label9 = 網站 | data9 = }}}}}} | label10 =
先前名稱 | data10 = }}} | label11 = 之後名稱 | data11 = }}}

| header22 =
{{\#if:}}}}}}}}}}}}}}}}}}|[2019年世界一級方程式錦標賽](../Page/2019年世界一級方程式錦標賽.md "wikilink")}}

| label23 = 主車手 | data23 = }}} | label24 = 測試車手 | data24 = }}} | label25
= 底盤 | data25 = }}} | label26 = 引擎 | data26 = }}} | label27 = 輪胎 |
data27 = }}}

| header29 =
{{\#if:}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}|[世界一級方程式錦標賽歷史](../Page/世界一級方程式錦標賽.md "wikilink")}}

| label30 = 首次出賽 | data30 = }}} | label31 = 最近出賽 | data31 = }}} |
label32 = 完賽數 | data32 = }}} | label33 = 製造商 | data33 = }}} | label34 =
引擎 | data34 = }}} | label35 = \[\[一级方程式世界车队冠军列表|车队
} | label36 = \[\[一级方程式世界车手冠军列表|車手
} | label37 = 分站冠軍 | data37 = }}} | label38 = 頒獎台 | data38 = }}} |
label39 = 積分 | data39 = }}} | label40 = [桿位](../Page/桿位.md "wikilink") |
data40 = }}} | label41 = [最快圈速](../Page/最快圈速.md "wikilink") | data41 =
}}} | label42 = [}}}世界一級方程式錦標賽](../Page/{{{Last_season.md "wikilink")
總成績 | data42 = }}} | label43 = 最後出賽 | data43 = }}} }}<noinclude>
</noinclude>