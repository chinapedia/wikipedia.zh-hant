教宗**西斯都五世**（，），原名，於1585年4月24日—1590年8月27日岀任教宗。在位期间恢复了[教宗国的治安](../Page/教宗国.md "wikilink")，着力恢复[罗马教廷的财政](../Page/罗马教廷.md "wikilink")，并慷慨投资公共事业，使[罗马的面貌接近现在的样子](../Page/罗马.md "wikilink")。此外也將[伊麗莎白一世和](../Page/伊麗莎白一世.md "wikilink")[亨利四世逐出了教會](../Page/亨利四世_\(法蘭西\).md "wikilink")。他虽然受到了诸多批评，但其业绩确是历代教宗中数一数二的。

## 譯名列表

  - 西斯都：[天主教香港教區禮儀委員會：禧年專頁](http://catholic-dlc.org.hk/2000.htm)作西斯都。
  - 思道：[香港天主教教區檔案　歷任教宗](https://web.archive.org/web/20051023073732/http://archives.catholic.org.hk/popes/index.htm)作思道。
  - 西克斯圖斯：[《大英簡明百科知識庫》2005年版](http://wordpedia.britannica.com/concise/quicksearch.aspx?keyword=sixtus&mode=3)、《[世界人名翻譯大辭典](../Page/世界人名翻譯大辭典.md "wikilink")》1993年版作西克斯圖斯。

## 参考文献

## 外部链接

{{-}}

[S](../Category/16世纪意大利人.md "wikilink")
[Category:義大利出生的教宗](../Category/義大利出生的教宗.md "wikilink")