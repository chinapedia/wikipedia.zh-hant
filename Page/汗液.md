[Sweating_at_Wilson_Trail_Stage_One_1.jpg](https://zh.wikipedia.org/wiki/File:Sweating_at_Wilson_Trail_Stage_One_1.jpg "fig:Sweating_at_Wilson_Trail_Stage_One_1.jpg")上的汗珠\]\]

**汗液**，或**汗**，是由[人等](../Page/人.md "wikilink")[高等动物透過](../Page/高等动物.md "wikilink")[汗腺所分泌出的液體](../Page/汗腺.md "wikilink")。汗的分泌受到[植物性神经系统调节](../Page/植物性神经系统.md "wikilink")。

## 汗液的作用

  - 降温：在汗腺排出汗液的过程中，汗液的排出和蒸发帮助生物带走大量的热量，达到散热的目的；
  - 保护：湿润[皮肤](../Page/皮肤.md "wikilink")、软化角质，酸化并抑制[细菌](../Page/细菌.md "wikilink")；
  - [排泄](../Page/排泄.md "wikilink")：排出體內廢物。

## 主要成分

汗液的主要成份是[水](../Page/水.md "wikilink")，约占总成分的98%到99%，其余物质为[氯化钠](../Page/氯化钠.md "wikilink")，极少量的[尿素](../Page/尿素.md "wikilink")、[氨和其他](../Page/氨.md "wikilink")[盐类](../Page/盐.md "wikilink")。

在人体生理中，小汗腺分泌的汗液，主要包括水、氯化钠、[钾](../Page/钾.md "wikilink")、[钙](../Page/钙.md "wikilink")、尿、
阿摩尼亞、[乳酸及](../Page/乳酸.md "wikilink")[氨基酸等](../Page/氨基酸.md "wikilink")；大汗腺分泌的汗液，除水外主要有[铁](../Page/铁.md "wikilink")、脂质(中性脂肪、[脂肪酸](../Page/脂肪酸.md "wikilink")、[胆固醇及](../Page/胆固醇.md "wikilink")[类脂质](../Page/类脂.md "wikilink"))、荧光物质、有臭物质。

## 中医观点

在中医学上认为：

  - 汗系人体内津液所化生，通过卫气的调节从汗孔而排泄。《[灵枢](../Page/灵枢.md "wikilink")·决气》：“汗出溱溱，是谓津。”
  - 发汗有散热和调节体温的作用。《[素问](../Page/素问.md "wikilink")·生气通天论》：“体若燔炭，汗出而散。”
  - 汗与血液有密切关系，由于“心主血”，汗是津液所化生，故称“汗为心之液”，并有“夺血者无汗，夺汗者无血”之说(《灵枢·营卫生会》)。

### 相关中医概念

  - 盗汗
  - [无汗](../Page/无汗.md "wikilink")

## 参考文献

### 引用

### 来源

  - [汗腺与相关问题](http://www.zgxl.net/sljk/imgbody/hanxian.htm)

## 参见

  - [汗毛](../Page/汗毛.md "wikilink")
  - [皮肤](../Page/皮肤.md "wikilink")
  - [皮脂腺](../Page/皮脂腺.md "wikilink")
  - [新陳代謝](../Page/新陳代謝.md "wikilink")

{{-}}

[Category:体液](../Category/体液.md "wikilink")
[Category:动物生理學](../Category/动物生理學.md "wikilink")
[Category:反射](../Category/反射.md "wikilink")
[Category:排泄](../Category/排泄.md "wikilink")