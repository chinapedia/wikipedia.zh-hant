**美國小鎮大街**（）是各迪士尼樂園內的一個主題園區。**美國小鎮大街**分別位於[加州的迪士尼樂園](../Page/加州.md "wikilink")、[佛羅里達州的神奇王國](../Page/佛羅里達州.md "wikilink")、[東京迪士尼樂園](../Page/東京.md "wikilink")、[巴黎迪士尼樂園](../Page/巴黎.md "wikilink")、[香港迪士尼樂園以及](../Page/香港.md "wikilink")[上海迪士尼樂園](../Page/上海.md "wikilink")
。此園區為世界各地迪士尼樂園均擁有的一大園區，唯[東京迪士尼樂園用](../Page/東京迪士尼樂園.md "wikilink")“世界市集”（），[上海迪士尼樂園用](../Page/上海迪士尼樂園.md "wikilink")「**米奇大街**」（）取代傳統「**美國小鎮大街**」的名称，是唯二不称该园区为美國小鎮大街的迪士尼樂園。

## 迪士尼樂園

**美國小鎮大街**（**Main Street, U.S.A.**）是迪士尼樂園裡其中一個主題園區。

### 遊樂設施

  - 迪士尼樂園鐵路
  - 小鎮大街電影院
  - 小鎮大街古董車
  - 公共馬車

### 食肆

  - Blue Ribbon Bakery
  - Carnation Café
  - Gibson Girl Ice Cream Parlor
  - Main Street Cone Shop
  - Plaza Inn
  - Refreshment Corner

### 商店

  - Emporium
  - Candy Palace
  - China Closet
  - Disneyana
  - Penny Arcade
  - Newsstand
  - Disney Showcase
  - Main Street Pin Shop
  - Main Street Magic Shop
  - New Century Jewelry
  - 20th Century Music Company

## 神奇王國

**美國小鎮大街**（**Main Street, U.S.A.**）是神奇王國裡其中一個主題園區。

### 遊樂設施

  - 神奇王國鐵路小鎮大街站
  - 小鎮大街古董車（包括公共馬車）

### 食肆

  - Casey's Corner
  - Crystal Palace
  - Edy's Ice Cream Parlor
  - Main Street Bakery
  - Main Street Confectionery
  - Plaza Ice Cream Parlor
  - Plaza Restaurant
  - Tony's Town Square Restaurant

### 商店

  - Art Gallery
  - Athletic Club
  - Barber Shop
  - Book Store
  - Disney Clothers
  - Emporium
  - Locker Rental
  - Main Street Confectionery
  - Newstand
  - Stroller Rental
  - Town Square Exposition Hall
  - Watches

## 東京迪士尼樂園

世界市集是東京迪士尼樂園中的一個園區，相當於其他迪士尼樂園的「美國小鎮大街」。它由玻璃維多利亞建築風格的溫室屋頂覆蓋，以保護客人遠離日本的多雨天氣。其中，世界集市以20世紀50年代美國小鎮為設計特色。世界集市也是東京迪士尼樂園的唯一入口。世界集市擁有三大餐飲服務餐廳，其中「主要街道」的餐廳最多。另外還有一條叫做「中心街」的大街，穿過主街，從兩邊出口到明日世界和探險樂園。這是第一個沒有火車站的美國小鎮大街，另一個沒有火車站的位於上海迪士尼樂園。

## 巴黎迪士尼樂園

[巴黎迪士尼樂園的](../Page/巴黎迪士尼樂園.md "wikilink")**美國小鎮大街**（**Main Street,
U.S.A.**）與其他版本略有不同；細節裝飾上較接近1920年代而非20世紀初葉的風格，但除此之外大部分建築物與佛羅里達州[神奇王國的版本幾乎相同](../Page/神奇王國.md "wikilink")。在原始的規劃中有著配合1920年代風格的路面電車，但後來由馬車和維多利亞風格的汽車取代。

此外，由於樂園所在地經常的陰雨低溫天氣。設計師在大街的兩側走道加上了頂棚，這些有頂棚的區域稱為「拱廊」（arcade），除了提供天氣的遮蔽外，拱廊也連結了大街上的所有商店。此外，在大街上因為煙火和遊行而充滿人潮時，兩邊的拱廊也成為了可通行的走道。

### 遊樂設施

  - 巴黎迪士尼樂園鐵路 - 小鎮大街站（Disneyland Railroad - Main Street Station）
  - 公共馬車（Horse-Drawn Streetcars）
  - 小鎮大街古董車（Main Street Vehicles）
  - 自由拱廊（Liberty Arcade）
  - 發現拱廊（Discovery Arcade）

### 餐飲設施

  - Walt's - An American Restaurant
  - Plaza Gardens Restaurant
  - Casey's Corner
  - Victoria's Home-Style Restaurant
  - Market House Deli
  - Cable Car Bake shop
  - Cookie Kitchen
  - The Coffee Grinder
  - The Ice Cream Company
  - The Gibson Girl Ice Cream Parlour

### 商店

  - Plaza East & West Boutiques
  - The Storybook Store
  - Ribbons & Bows Hat Shop
  - The Bixby Brothers
  - Emporium
  - Dapper Dan's Hair Cuts
  - Town Square Photography
  - Boardwalk Candy Palace
  - Disney Clothiers, Ltd.
  - Main Street Motors
  - Harrington's Fine China & Porcelains
  - Disneyana Collectibles
  - Lilly's Boutique
  - Disney & Co.

## 香港迪士尼樂園

[HKDL_Main_Street_USA_Christmas_Night_view_201312.jpg](https://zh.wikipedia.org/wiki/File:HKDL_Main_Street_USA_Christmas_Night_view_201312.jpg "fig:HKDL_Main_Street_USA_Christmas_Night_view_201312.jpg")
如同其他的樂園，美國小鎮大街是[香港迪士尼樂園的入口](../Page/香港迪士尼樂園.md "wikilink")。大街上的建築與加州[安納罕市](../Page/安納罕市.md "wikilink")[迪士尼樂園的版本幾乎完全相同](../Page/迪士尼樂園.md "wikilink")。最初原本預計在香港迪士尼樂園鐵路的車站底下開設餐廳，但由於建設預算的關係最後取消了這個計畫。

大街上的裝飾靈感來自20世紀初的美國小鎮風格，實際的年代約在1890年至1910年間。雖然在外觀上與加州迪士尼樂園十分相似，但香港版本的背景故事則深深受到歐洲[移民的影響](../Page/移民.md "wikilink")。例如廣場飯店（Plaza
Inn）與加州版本的外觀設計相同，但香港版本的背景故事是敘述一對富裕的美國夫妻來到香港後，愛上當地的文化和美食，因此決定回國後開設一間傳統的英式餐館，並以他們在旅途中搜集的物品裝飾其中。市集餅店（Market
House
Bakery）則是由一位[維也納甜點主廚所開設](../Page/越南人.md "wikilink")，他從[奧地利的皇家市集採購了一些世界上最有名的甜點和咖啡蛋糕](../Page/奧地利.md "wikilink")。

與其他樂園的小鎮大街採用[石材為主的方式不同](../Page/石頭.md "wikilink")，香港迪士尼樂園的小鎮大街主要使用[木材做為建築材料](../Page/木材.md "wikilink")，這樣的做法在香港實屬少見。街道上沒有馬匹拉動的街車，但從早期的概念草圖上可看見街車的軌道。

### 遊樂設施

  - 香港迪士尼樂園鐵路
  - 動畫藝術廊
  - 動畫藝術教室
  - 小鎮大街古董車
  - 布公仔流動實驗
  - 街頭表演

### 娛樂設施

  - 迪士尼飛天巡遊
  - 米奇水花巡遊

### 餐飲設施

  - [廣場餐廳](../Page/香港迪士尼樂園餐飲設施#廣場餐廳.md "wikilink")
  - [大街餐廳](../Page/香港迪士尼樂園餐飲設施#大街餐廳.md "wikilink")
  - [市集餅店](../Page/香港迪士尼樂園餐飲設施#市集餅店.md "wikilink")
  - [咖啡及蒙牛牛奶售賣點](../Page/香港迪士尼樂園餐飲設施#咖啡及蒙牛牛奶售賣點.md "wikilink")

### 商店

  - [百貨店](../Page/香港迪士尼樂園商店#百貨店.md "wikilink")
  - [大街糖果店](../Page/香港迪士尼樂園商店#大街糖果店.md "wikilink")
  - [小鎮雜貨店](../Page/香港迪士尼樂園商店#小鎮雜貨店.md "wikilink")
  - [奇趣店](../Page/香港迪士尼樂園商店#奇趣店.md "wikilink")
  - [小鎮沖印店](../Page/香港迪士尼樂園商店#小鎮沖印店.md "wikilink")
  - [小鎮珠寶店](../Page/香港迪士尼樂園商店#小鎮珠寶店.md "wikilink")
  - [水晶藝術廊](../Page/香港迪士尼樂園商店#水晶藝術廊.md "wikilink")
  - [剪影軒](../Page/香港迪士尼樂園商店#剪影軒.md "wikilink")
  - [廣場時尚店](../Page/香港迪士尼樂園商店#廣場時尚店.md "wikilink")

## 上海迪士尼樂園

上海迪士尼樂園包含一個类似于美国华特迪士尼世界度假区，迪士尼好莱坞影城主题乐园入口區域的同名入场园区「**米奇大街**」（），取代傳統的「**美國小鎮大街**」（），區域內將設有多間以迪士尼各時期歷史為主題的商店和餐廳。

### 景点

  - 上海迪士尼樂園樂團（Shanghai Disneyland Band）

### 餐飲

  - Club 33
  - 米奇好夥伴美味集市（Mickey & Pals Market Café）
  - 小米大廚烘焙坊（Rémy's Patisserie）
  - 甜心糖果（Sweethearts Confectionery）

### 商店

  - M大街購物廊（Avenue M Arcade）
  - Whistle Stop Shop

[Category:香港迪士尼樂園主題園區](../Category/香港迪士尼樂園主題園區.md "wikilink")
[M](../Category/迪士尼度假區及主題公園主題園區.md "wikilink")
[M](../Category/東京迪士尼樂園.md "wikilink")
[M](../Category/神奇王國.md "wikilink")
[M](../Category/迪士尼樂園.md "wikilink")
[M](../Category/巴黎迪士尼樂園.md "wikilink")
[Category:1955年加利福尼亞州建立](../Category/1955年加利福尼亞州建立.md "wikilink")
[Category:1971年佛羅里達州建立](../Category/1971年佛羅里達州建立.md "wikilink")
[Category:1983年日本建立](../Category/1983年日本建立.md "wikilink")
[Category:1992年法國建立](../Category/1992年法國建立.md "wikilink")
[Category:2005年香港建立](../Category/2005年香港建立.md "wikilink")
[Category:2016年中國建立](../Category/2016年中國建立.md "wikilink")