**MathWorld**是線上[數學](../Page/數學.md "wikilink")[百科全書](../Page/百科全書.md "wikilink")，由[沃夫朗研究公司](../Page/沃夫朗研究公司.md "wikilink")（[Wolfram
Research](../Page/Wolfram_Research.md "wikilink") inc.，WRI）贊助和享有版权，大部分由
Eric W. Weisstein
创建和编写。沃夫朗研究公司即是全球闻名的数学软件[Mathematica的生产商](../Page/Mathematica.md "wikilink")。MathWorld亦有接受美國[国家科学基金会的](../Page/国家科学基金会.md "wikilink")承認的[伊利諾大學厄巴納-香檳分校支持](../Page/伊利諾大學厄巴納-香檳分校.md "wikilink")。

## 歷史

[埃立克·魏爾斯史甸是該網的作者](../Page/埃立克·魏爾斯史甸.md "wikilink")，原為[物理和](../Page/物理.md "wikilink")[天文學生](../Page/天文.md "wikilink")，有記錄數學筆記的習慣。1995年他將筆記放上網並稱之為「Eric's
Treasure Trove of
Mathematics」。那裡有許多文章，包涵了很大範圍的數學主題。那時，它譽為網上最好的獨立數學資源，很受歡迎。1998年，魏爾斯史甸和
CRC Press 聯絡，將網站內容出版為書本和[CD-ROM](../Page/CD-ROM.md "wikilink")，以「CRC
Concise Encyclopedia of
Mathematics」為題，其時免費網上版本只部分開放。1999年魏爾斯史甸加入WRI，WRI將該網站重新命名為
*MathWorld* 並放在公司网站 <http://mathworld.wolfram.com> 而且不设访问权限。

## CRC 纠纷

2000年，CRC
Press控告WRI、WRI主席[史蒂芬·沃爾夫勒姆及魏爾斯史甸](../Page/史蒂芬·沃爾夫勒姆.md "wikilink")，因為他們違反合約：MathWorld的內容-{只}-能在紙上出現。MathWorld在訴訟期間暫時關閉。WRI給了CRC
Press一笔數目不详的金錢，庭外解決。这个案件在在线出版界引起不小的波浪。

MathWorld的暫時關閉令很多人嘗試編寫其他免費的網上數學百科全書，例如[PlanetMath和](../Page/PlanetMath.md "wikilink")[維基百科](../Page/維基百科.md "wikilink")。

## 對手

某些數學圈子，特別是[usenet社群sci](../Page/usenet.md "wikilink").math，對MathWorld的文章質素頗有微言。\[1\]

## 外部連結

  - [MathWorld](http://mathworld.wolfram.com)

## 注釋

<references />

[Category:数学网站](../Category/数学网站.md "wikilink")
[Category:在线百科全书](../Category/在线百科全书.md "wikilink")

1.  [1](http://groups-beta.google.com/group/sci.math/browse_thread/thread/97a906ba4b8d774b/f6488e9717c7f352?q=mathworld+errors&_done=%2Fgroup%2Fsci.math%2Fsearch%3Fq%3Dmathworld+errors%26start%3D10%26&_doneTitle=Back+to+Search&&d#f6488e9717c7f352)[2](http://groups-beta.google.com/group/sci.math/browse_thread/thread/e4b7b1197966d698/4f82bf4d1db37d1f?q=mathworld+errors&_done=%2Fgroup%2Fsci.math%2Fsearch%3Fgroup%3Dsci.math%26q%3Dmathworld+errors%26qt_g%3D1%26searchnow%3DSearch+this+group%26&_doneTitle=Back+to+Search&&d#4f82bf4d1db37d1f)