1956年[環球小姐比賽是第五届](../Page/環球小姐.md "wikilink")，由[美國小姐](../Page/美國小姐.md "wikilink")得到。

## 有關資料

  - 届數：第五届
  - 參賽人數：三十位
  - 總決賽地點：[美國](../Page/美國.md "wikilink")[加利福尼亞洲](../Page/加利福尼亞洲.md "wikilink")[長灘](../Page/長灘_\(加利福尼亞州\).md "wikilink")
  - 總決賽日期：七月二十日

## 最後結果

  - **冠軍**：[美國小姐](../Page/美國.md "wikilink")
  - **亞軍**：[德國小姐Marina](../Page/德國.md "wikilink") Orschel
  - **季軍**：[瑞典小姐Ingrid](../Page/瑞典.md "wikilink") Goude
  - **第四名**：[英格蘭小姐Iris](../Page/英格蘭.md "wikilink") Alice Kathleen Waller
  - **第五名**：[意大利小姐Rossana](../Page/意大利.md "wikilink") Galli

**其他入圍者**（排名不分前後）：

  - [阿根廷小姐Ileana](../Page/阿根廷.md "wikilink") Carré
  - [比利時小姐Lucienne](../Page/比利時.md "wikilink") Auquier
  - [巴西小姐Maria](../Page/巴西.md "wikilink") José Cardoso
  - [古巴小姐Marcia](../Page/古巴.md "wikilink") Rodríguez Echevarría
  - [法國小姐Anita](../Page/法國.md "wikilink") Treyens
  - [希臘小姐Rita](../Page/希臘.md "wikilink") Gouma
  - [以色列小姐Sara](../Page/以色列.md "wikilink") Tal
  - [墨西哥小姐Erna](../Page/墨西哥.md "wikilink") Marta Bauman
  - [秘魯小姐Lola](../Page/秘魯.md "wikilink") Sabogal Morzán
  - [委內瑞拉小姐Blanquita](../Page/委內瑞拉.md "wikilink") Heredia Osío

**特別獎項**：

  - **友誼小姐第一名**：[哥斯達黎加小姐](../Page/哥斯達黎加.md "wikilink")
      - 第二名：[新墨西哥小姐Jackie](../Page/新墨西哥.md "wikilink") Brown
  - **最上鏡小姐第一名**：[德國小姐](../Page/德國.md "wikilink")
      - 第二名：[德國小姐](../Page/德國.md "wikilink")
      - 第三名：[瑞典小姐](../Page/瑞典.md "wikilink")
      - 第四名：[意大利小姐](../Page/意大利.md "wikilink")
      - 第五名：[加利福尼亞小姐Shurlee](../Page/加利福尼亞.md "wikilink") Witty
  - **最受歡迎小姐第一名**：[美國小姐Carol](../Page/美國.md "wikilink") Morris
      - 第二名：[德克萨斯小姐Jo](../Page/德克萨斯.md "wikilink") Dodson
      - 第三名：[德國小姐](../Page/德國.md "wikilink")

**注**：

  - 部分獎項得獎者是[美國小姐的參賽選手](../Page/美國小姐.md "wikilink")。

## 參賽選手

  - **[Flag_of_Alaska.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Alaska.svg "fig:Flag_of_Alaska.svg")
    [阿拉斯加](../Page/阿拉斯加.md "wikilink")** - Barbara Maria Sellar
  - **** - Marina Orschel
  - **** - Ileana Carré
  - **** - Lucienne Auquier
  - **** - Maria José Cardoso
  - **** - Elaine Bishenden
  - **** - Concepción Obach Chacana
  - **** - Anabella Granados
  - **** - Marcía Rodríguez
  - **** - Mercedes Flores Espín
  - **** - Carol Morris
  - **** - Isabel Rodriguez
  - **** - Anita Treyens
  - **** - Rita Gouma
  - **** - Ileana Garlinger Díaz

<!-- end list -->

  - **[Flag_of_Guyana.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Guyana.svg "fig:Flag_of_Guyana.svg")
    [英屬圭亞那](../Page/英屬圭亞那.md "wikilink")** - Rosalind Iva Joan Fung
  - **[Flag_of_the_Netherlands.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_Netherlands.svg "fig:Flag_of_the_Netherlands.svg")
    [荷蘭](../Page/荷蘭.md "wikilink")** - Rita Schmidt
  - **** - Iris Alice Kathleen Waller
  - **** - Gudlaug Gudmundsdóttir
  - **** - Sara Tal
  - **** - Rossana Galli
  - **** - Yoshie Baba
  - **** - Erna Marta Bauman
  - **** - Lola Sabogal Morzán
  - **** - Paquita Vivo
  - **** - Olga Fiallo Oliva
  - **** - Ingrid Goude
  - **** - Can Yusal
  - **** - Titina Aguirre
  - **** - Blanquita Heredia Osío

注：[英屬圭亞那即是現在的](../Page/英屬圭亞那.md "wikilink")[圭亞那](../Page/圭亞那.md "wikilink")。

## 重要記錄

  - [瑞典小姐是](../Page/瑞典.md "wikilink")1956年[歐洲小姐亞軍](../Page/歐洲.md "wikilink")。
  - [英格蘭小姐參加了](../Page/英格蘭.md "wikilink")1956年[世界小姐比賽](../Page/世界小姐.md "wikilink")。
  - [荷蘭小姐是](../Page/荷蘭.md "wikilink")1956年[歐洲小姐第五名](../Page/歐洲.md "wikilink")。
  - [芬蘭小姐和](../Page/芬蘭.md "wikilink")[菲律賓小姐退出比賽](../Page/菲律賓.md "wikilink")。但後來[菲律賓派了另一位選手參賽](../Page/菲律賓.md "wikilink")。

## 評判員

  - Betty Jones
  - Alberto Vargas
  - Tom Kelly
  - Earl Wilson
  - Claude Berr
  - Max Factor
  - Robert Palmer
  - Dorothy Kirsten
  - James H. Noguer
  - Vincent Trotta

## 外部链接

  - [環球小姐官方網站](http://www.missuniverse.com)

[Category:環球小姐](../Category/環球小姐.md "wikilink")