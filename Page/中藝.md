[HK_Central_Chinese_Arts_&_Crafts_Name.jpg](https://zh.wikipedia.org/wiki/File:HK_Central_Chinese_Arts_&_Crafts_Name.jpg "fig:HK_Central_Chinese_Arts_&_Crafts_Name.jpg")
**中藝**（）是[香港的一家專賣](../Page/香港.md "wikilink")[中國藝術](../Page/中國藝術.md "wikilink")[工藝品的](../Page/工藝品.md "wikilink")[百貨公司](../Page/百貨公司.md "wikilink")，始自1959年，現時中藝在[香港島有](../Page/香港島.md "wikilink")3間[商店](../Page/商店.md "wikilink")，在[九龍半島有](../Page/九龍半島.md "wikilink")1間，都是在[商業中心區](../Page/商業中心區.md "wikilink")，走優質[品牌路線](../Page/品牌.md "wikilink")。商品分為[翡翠](../Page/翡翠.md "wikilink")、[珠寶](../Page/珠寶.md "wikilink")、[玉石](../Page/玉石.md "wikilink")、[雕刻](../Page/雕刻.md "wikilink")、[古玩](../Page/古玩.md "wikilink")、[瓷器](../Page/瓷器.md "wikilink")、[刺繡品](../Page/刺繡.md "wikilink")、[景泰藍](../Page/景泰藍.md "wikilink")、[絲綢](../Page/絲綢.md "wikilink")、中式[晚裝](../Page/晚裝.md "wikilink")、[抽紗等](../Page/抽紗.md "wikilink")。\[1\]

中藝是[華潤零售的](../Page/華潤零售.md "wikilink")[子公司](../Page/子公司.md "wikilink")，都是[華潤創業的集團成員](../Page/華潤創業.md "wikilink")。

## 分店

  - [尖沙咀](../Page/尖沙咀.md "wikilink")[星光行](../Page/星光行.md "wikilink")（於2013年第四季結業搬遷）
  - [尖沙咀](../Page/尖沙咀.md "wikilink")[中港城](../Page/中港城.md "wikilink")（於2013年10月中旬開業）(於2018年結業)
  - [灣仔](../Page/灣仔.md "wikilink")[華潤大廈](../Page/華潤大廈.md "wikilink")（於2011年12月16日結業）
  - [灣仔](../Page/灣仔.md "wikilink")[灣景中心大廈](../Page/灣景中心大廈.md "wikilink")（於2012年1月5日開業）
  - [佐敦](../Page/佐敦.md "wikilink")[嘉利大廈](../Page/嘉利大廈.md "wikilink")（現址[佐敦薈](../Page/佐敦薈.md "wikilink")）（於1996年11月20日五級火後結業，詳見[嘉利大廈大火](../Page/嘉利大廈大火.md "wikilink")）
  - [金鐘](../Page/金鐘.md "wikilink")[太古廣場](../Page/太古廣場.md "wikilink")
  - [中環](../Page/中環.md "wikilink")[德輔道中](../Page/德輔道中.md "wikilink")[協成行大廈](../Page/協成行大廈.md "wikilink")（2012年8月16日開業，[皇后大道中](../Page/皇后大道中.md "wikilink")[泛海大廈分店已於](../Page/泛海大廈.md "wikilink")2012年7月結業）\[2\]

<File:HK> TST Arts & Crafts 3 Salisbury Road n Star House Chinese
a.jpg|星光行分店 <File:Chinese> Arts & Crafts Central Store.jpg|皇后大道中分店
[File:HK_CAC_ChinaResourcesBuilding.JPG|華潤大廈分店](File:HK_CAC_ChinaResourcesBuilding.JPG%7C華潤大廈分店)

## 参考文献

## 外部連結

  - [中藝官方網頁](http://www.cachk.com/)

[Category:香港百貨公司](../Category/香港百貨公司.md "wikilink")
[Category:華潤](../Category/華潤.md "wikilink")
[Category:1959年成立的公司](../Category/1959年成立的公司.md "wikilink")

1.  [中藝官方網頁](http://www.cachk.com/)
2.  [信報：傳中藝100萬預租協成行（2012年4月19日）](http://www.hkej.com/template/property/php/detail.php?title_id=1326)