**“中国皇帝”**（英文：****）是[印度尼西亚](../Page/印度尼西亚.md "wikilink")[班达海西部海域中的一座](../Page/班达海.md "wikilink")[海底火山](../Page/海底火山.md "wikilink")。这座[火山拥有](../Page/火山.md "wikilink")[盾状的外形](../Page/盾.md "wikilink")，从[海底到其顶部有](../Page/海底.md "wikilink")1,500米左右（约5,000英尺）\[1\]。

## 概况

“中国皇帝”火山属于[尼沃凯尔克](../Page/尼沃凱爾克_\(火山\).md "wikilink")（）火山链的一部分，而该火山链按科学的命名法应该叫尼沃凯尔克–中国皇帝[海岭](../Page/海岭.md "wikilink")（英文：，简称），其[深度大约在](../Page/深度.md "wikilink")[海平面以下](../Page/海平面.md "wikilink")3,100–2,700米（10,170–8,858英尺）\[2\]。

尼沃凯尔克–中国皇帝海岭位于南[班达海的两个](../Page/班达海.md "wikilink")[海盆之一的](../Page/海盆.md "wikilink")[达马尔海盆](../Page/达马尔海盆.md "wikilink")。班达海被[东巽他和](../Page/巽他弧.md "wikilink")[班达两个](../Page/班达弧.md "wikilink")[火山弧所包围](../Page/火山弧.md "wikilink")，它位于[亚欧](../Page/亚欧板块.md "wikilink")、[太平洋和](../Page/太平洋板块.md "wikilink")[印度-澳洲三大](../Page/印度-澳洲板块.md "wikilink")[板块交界处](../Page/板块.md "wikilink")，而这三大板块自[中生代起就开始活跃地碰撞并靠拢](../Page/中生代.md "wikilink")。而[上新世](../Page/上新世.md "wikilink")[第四纪以来的](../Page/第四纪.md "wikilink")1–2千米厚的沉淀使得海底变得相对的平坦了\[3\]。

“中国皇帝”和尼沃凯尔克都位于海岭的末端，它们都是[活火山](../Page/活火山.md "wikilink")。基于[地质学和](../Page/地质学.md "wikilink")[地球化学的研究](../Page/地球化学.md "wikilink")，“中国皇帝”、尼沃凯尔克、在班达海中的另一个名叫[鲁西帕拉](../Page/鲁西帕拉.md "wikilink")（）的海岭以及[韦塔岛](../Page/韦塔岛.md "wikilink")（）弧在7万年前左右是一个单独的[火山弧](../Page/火山弧.md "wikilink")\[4\]。而火山活动与[印度洋](../Page/印度洋.md "wikilink")[岩石圈所在的](../Page/岩石圈.md "wikilink")[澳大利亚板块的下沉有关](../Page/澳大利亚板块.md "wikilink")。在1979年的一次测量中，根据结构和年龄，“中国皇帝”和纽维尔柯克火山都被定义为[休眠火山](../Page/休眠火山.md "wikilink")，因为根据定义的标准，它们的火山活动估计在7万年以前就已经停止了\[5\]。海岭的[岩浆活动在](../Page/岩浆.md "wikilink")3万年前就结束了，据信是[帝汶岛和](../Page/帝汶岛.md "wikilink")[巽他岛弧的韦塔段碰撞的结果](../Page/巽他岛弧.md "wikilink")\[6\]。

## 参见

  - [印度尼西亚火山列表](../Page/印度尼西亚火山列表.md "wikilink")

## 参考文献

[Category:海底火山](../Category/海底火山.md "wikilink")
[Category:活火山](../Category/活火山.md "wikilink")
[Category:印度尼西亚火山](../Category/印度尼西亚火山.md "wikilink")
[Category:海底山](../Category/海底山.md "wikilink")

1.

2.

3.
4.
5.

6.