**波爾克縣**（）是[美國](../Page/美國.md "wikilink")[密蘇里州西南部的一個縣](../Page/密蘇里州.md "wikilink")。面積1,664平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口26,992人。縣治[波利瓦爾](../Page/波利瓦爾_\(密蘇里州\).md "wikilink")（Bolivar）。

成立於1835年1月5日。縣名原來是紀念[美國獨立戰爭時期將領](../Page/美國獨立戰爭.md "wikilink")[以西家·波爾克](../Page/以西家·波爾克.md "wikilink")，但在正式立法時卻以其孫，後來成為[總統的](../Page/美國總統.md "wikilink")[眾議院議長](../Page/美國眾議院議長.md "wikilink")[詹姆斯·諾克斯·波爾克](../Page/詹姆斯·諾克斯·波爾克.md "wikilink")。

[Category:密蘇里州行政區劃](../Category/密蘇里州行政區劃.md "wikilink")
[波爾克縣_(密蘇里州)](../Category/波爾克縣_\(密蘇里州\).md "wikilink")
[Category:斯普林菲爾德都會區
(密蘇里州)](../Category/斯普林菲爾德都會區_\(密蘇里州\).md "wikilink")
[Category:1835年密蘇里州建立](../Category/1835年密蘇里州建立.md "wikilink")