**林忠正**（），台灣政治人物，為[閩南裔](../Page/閩南裔.md "wikilink")[穆斯林後裔](../Page/穆斯林.md "wikilink")，有阿拉伯血統，\[1\]曾任[民主進步黨主席](../Page/民主進步黨主席.md "wikilink")[施明德特別助理](../Page/施明德.md "wikilink")，曾代表[民主進步黨任職全國不分區](../Page/民主進步黨.md "wikilink")[立法委員](../Page/立法委員.md "wikilink")，在立法委員任內的綽號叫做「頑皮豹」。

## 簡歷

1997年8月，林忠正被[年代影視](../Page/年代影視.md "wikilink")[董事長](../Page/董事長.md "wikilink")[邱復生聘為年代影視副董事長同時也兼任](../Page/邱復生.md "wikilink")[台灣職棒大聯盟](../Page/台灣職棒大聯盟.md "wikilink")[嘉南年代勇士隊領隊](../Page/嘉南勇士.md "wikilink")\[2\]。後來林忠正擔任[行政院金融監督管理委員會委員](../Page/行政院金融監督管理委員會.md "wikilink")，因涉嫌[收賄為](../Page/收賄.md "wikilink")[開發金控](../Page/開發金控.md "wikilink")、[金鼎證券](../Page/金鼎證券.md "wikilink")、[宏遠證券等公司護航](../Page/宏遠證券.md "wikilink")，遭[羈押禁見](../Page/羈押.md "wikilink")，向金管會請辭委員獲-{准}-。2006年11月6日，[中華民國總統府公布人事令](../Page/中華民國總統府_\(臺北\).md "wikilink")：林忠正已-{准}-辭職，應予免職，自2006年10月30日生效\[3\]。一審宣判，林忠正遭判[有期徒刑](../Page/有期徒刑.md "wikilink")16年。2009年8月21日，[台灣高等法院二審宣判](../Page/台灣高等法院.md "wikilink")，林忠正受賄改判有期徒刑14年徒刑、[褫奪公權](../Page/褫奪公權.md "wikilink")5年。2010年12月30日，[中華民國最高法院駁回林忠正上訴](../Page/中華民國最高法院.md "wikilink")，以貪污、洩密等罪判決林忠正有期徒刑14年、褫奪公權5年定讞。2011年1月26日，林忠正在[施明德](../Page/施明德.md "wikilink")、[林向愷](../Page/林向愷.md "wikilink")、[姚立明及](../Page/姚立明.md "wikilink")[楊憲宏等人陪同下召開](../Page/楊憲宏.md "wikilink")[記者會表示](../Page/記者會.md "wikilink")，「我沉默，不代表我認罪」，他得知判決後曾想過要輕生，但他認為必須活著證明自己清白、活著看到司法改革，才打消自殺念頭。2011年2月11日，林忠正入監服刑。

2016年，林忠正擔任[台灣通訊傳播產業協進會顧問](../Page/台灣通訊傳播產業協進會.md "wikilink")\[4\]、[東森媒體集團首席顧問](../Page/東森媒體集團.md "wikilink")，在東森集團位於[臺北市](../Page/臺北市.md "wikilink")[忠孝西路一段的](../Page/忠孝西路_\(臺北市\).md "wikilink")[崇聖大樓](../Page/崇聖大樓.md "wikilink")15樓有自己的辦公室，東森集團各公司的大小會議都能見到他\[5\]。

## 注釋

[Category:第3屆中華民國立法委員](../Category/第3屆中華民國立法委員.md "wikilink")
[Category:第4屆中華民國立法委員](../Category/第4屆中華民國立法委員.md "wikilink")
[Category:第5屆中華民國立法委員](../Category/第5屆中華民國立法委員.md "wikilink")
[Category:民主進步黨黨員](../Category/民主進步黨黨員.md "wikilink")
[Category:中央研究院研究員](../Category/中央研究院研究員.md "wikilink")
[Category:國立中央大學教授](../Category/國立中央大學教授.md "wikilink")
[Category:夏威夷大學校友](../Category/夏威夷大學校友.md "wikilink")
[Category:國立臺灣大學教授](../Category/國立臺灣大學教授.md "wikilink")
[Category:國立臺灣大學社會科學院校友](../Category/國立臺灣大學社會科學院校友.md "wikilink")
[Category:東海大學校友](../Category/東海大學校友.md "wikilink")
[Category:阿拉伯裔台灣人](../Category/阿拉伯裔台灣人.md "wikilink")
[Category:新北市人](../Category/新北市人.md "wikilink")
[Category:臺灣貪污犯](../Category/臺灣貪污犯.md "wikilink")
[Zhong忠](../Category/林姓.md "wikilink")

1.
2.
3.
4.
5.