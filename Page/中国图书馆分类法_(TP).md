  - TP
    [自动化技术](../Page/自动化技术.md "wikilink")、[计算机技术](../Page/计算机技术.md "wikilink")

:\*TP1 自动化基础理论

::\*TP11 自动化系统理论

::\*TP13 自动控制理论

::\*TP14 自动信息理论

::\*TP15 自动模拟理论（自动仿真理论）

::\*TP17 开关电路理论

::\*TP18 [人工智能理论](../Page/人工智能.md "wikilink")

:::\*TP181
[自动推理](../Page/自动推理.md "wikilink")、[机器学习](../Page/机器学习.md "wikilink")

:::\*TP182
[专家系统](../Page/专家系统.md "wikilink")、[知识工程](../Page/知识工程.md "wikilink")

:::\*TP183 [人工神经网络与计算](../Page/人工神经网络.md "wikilink")

:\*TP2 [自动化技术及设备](../Page/自动化技术.md "wikilink")

::\*TP20 一般性问题

::\*TP21 自动化元件、部件

::\*TP23 自动化装置与设备

::\*TP24 机器人技术

::\*TP27 自动化系统

::\*TP29 自动化技术在各方面的应用

:\*TP3 计算技术、计算机技术

::\*TP3-05 计算机与其他学科的关系

::\*TP30 一般性问题

:::\*TP309 安全保密

::::\*TP309.1　计算机设备安全

::::\*TP309.2　数据安全

::::\*TP309.3　数据备份与恢复

::::\*TP309.5　计算机病毒与防治

::::\*TP309.7　加密与解密

::\*TP31 [计算机软件](../Page/计算机软件.md "wikilink")

:::\*TP311
[程序设计](../Page/程序设计.md "wikilink")、[数据库](../Page/数据库.md "wikilink")、[软件工程](../Page/软件工程.md "wikilink")

:::\*TP311.1 [程序设计](../Page/程序设计.md "wikilink")

:::\*TP311.13 [数据库理论与系统](../Page/数据库.md "wikilink")

:::\*TP311.5 [软件工程](../Page/软件工程.md "wikilink")

:::\*TP312
[程序语言](../Page/程序语言.md "wikilink")、[算法语言](../Page/算法语言.md "wikilink")

:::\*TP313
[汇编语言](../Page/汇编语言.md "wikilink")、[汇编程序](../Page/汇编程序.md "wikilink")

:::\*TP314
[编译程序](../Page/编译程序.md "wikilink")、[解释程序](../Page/解释程序.md "wikilink")

:::\*TP315 [管理程序](../Page/管理程序.md "wikilink")、管理系统

:::\*TP316 [操作系统](../Page/操作系统.md "wikilink")

:::\*TP317
[应用软件](../Page/应用软件.md "wikilink")（[程序包](../Page/程序包.md "wikilink")）

::::\*TP317.1 [办公自动化系统](../Page/办公自动化系统.md "wikilink")

::::\*TP317.2 [文字处理软件](../Page/文字处理软件.md "wikilink")

::::\*TP317.3 [表处理软件](../Page/表处理软件.md "wikilink")

::::\*TP317.4 [图形图像处理软件](../Page/图形图像处理软件.md "wikilink")

::::\*TP317.5 [多媒体软件](../Page/多媒体软件.md "wikilink")

::::\*TP317.6 [游戏软件](../Page/游戏软件.md "wikilink")

:::\*TP319 [专用软件](../Page/专用软件.md "wikilink")

::\*TP32
一般[计算器和](../Page/计算器.md "wikilink")[计算机](../Page/计算机.md "wikilink")

::\*TP33
[电子数字计算机](../Page/电子数字计算机.md "wikilink")（[不连续作用电子计算机](../Page/不连续作用电子计算机.md "wikilink")）

::\*TP34 [电子模拟计算机](../Page/电子模拟计算机.md "wikilink")（连续作用电子计算机）

::\*TP35 [混合电子计算机](../Page/混合电子计算机.md "wikilink")

::\*TP36 [微型计算机](../Page/微型计算机.md "wikilink")

:::\*TP368 各种微型计算机

::\*TP37
[多媒体技术与](../Page/多媒体技术.md "wikilink")[多媒体计算机](../Page/多媒体计算机.md "wikilink")

::\*TP38 其他计算机

:::\*TP381 [激光计算机](../Page/激光计算机.md "wikilink")

:::\*TP383 [超导计算机](../Page/超导计算机.md "wikilink")

:::\*TP384 [分子计算机](../Page/分子计算机.md "wikilink")

:::\*TP385 [量子计算机](../Page/量子计算机.md "wikilink")

:::\*TP387 [智能型计算机](../Page/智能型计算机.md "wikilink")

:::\*TP389.1 [人工神经网络计算机](../Page/人工神经网络计算机.md "wikilink")

::\*TP39 计算机的应用

:::\*TP391 [信息处理](../Page/信息处理.md "wikilink")（信息加工）

::::\*TP391.1 [文字信息处理](../Page/文字信息处理.md "wikilink")

:::::\*TP391.12 [汉字处理系统](../Page/汉字处理系统.md "wikilink")

:::::\*TP391.13 [表格处理系统](../Page/表格处理系统.md "wikilink")

:::::\*TP391.14 [文字录入技术](../Page/文字录入技术.md "wikilink")

::::\*TP391.2 [翻译系统](../Page/翻译系统.md "wikilink")

::::\*TP391.3 [检索系统](../Page/检索系统.md "wikilink")

::::\*TP391.4
[模式识别](../Page/模式识别.md "wikilink")、[射频识别](../Page/射频识别.md "wikilink")

:::::\*TP391.41 [图形图像识别](../Page/图形图像识别.md "wikilink")

::::::\*TP391.411 [计算机图形学](../Page/计算机图形学.md "wikilink")

::::::\*TP391.412 [图形识别](../Page/图形识别.md "wikilink")

::::::\*TP391.413 [图像识别](../Page/图像识别.md "wikilink")

::::::\*TP391.414 [三维动画制作](../Page/三维动画制作.md "wikilink")

:::::\*TP391.43 [文字识别](../Page/文字识别.md "wikilink")

:::::\*TP391.44 [光模式识别](../Page/光模式识别.md "wikilink")

:::::\*TP391.45 [无线射频识别](../Page/无线射频识别.md "wikilink")（RFID）

::::\*TP391.7 [计算机辅助技术](../Page/计算机辅助技术.md "wikilink")

:::::\*TP391.72 [计算机辅助设计](../Page/计算机辅助设计.md "wikilink")（CAD）、辅助制图

:::::\*TP391.73 [计算机辅助技术制造](../Page/计算机辅助技术制造.md "wikilink")（CAM）

:::::\*TP391.75 [计算机辅助计算](../Page/计算机辅助计算.md "wikilink")（CAC）

:::::\*TP391.76 [计算机辅助测试](../Page/计算机辅助测试.md "wikilink")（CAT）

:::::\*TP391.77 [计算机辅助分析](../Page/计算机辅助分析.md "wikilink")（CAA）

::::\*TP391.9 [计算机仿真](../Page/计算机仿真.md "wikilink")

:::::\*TP391.91 计算机仿真原理

:::::\*TP391.92 计算机仿真技术、仿真系统

:::::\*TP391.97 计算机仿真测试、评估

:::::\*TP391.98 计算机[虚拟现实](../Page/虚拟现实.md "wikilink")

:::::\*TP391.99 计算机仿真应用

:::\*TP392 各种专用数据库

:::\*TP393 [计算机网络](../Page/计算机网络.md "wikilink")

:::::\*TP393.0 一般性问题

:::::\*TP393.08 [计算机网络安全](../Page/计算机网络安全.md "wikilink")

:::::\*TP393.09 [计算机网络应用](../Page/计算机网络应用.md "wikilink")

::::\*TP393.1
[局域网](../Page/局域网.md "wikilink")（LAN）、[城域网](../Page/城域网.md "wikilink")（MAN）

::::\*TP393.2 [广域网](../Page/广域网.md "wikilink")（WAN）

::::\*TP393.4 [国际互联网](../Page/国际互联网.md "wikilink")

:::\*TP399 在其他方面的应用

:\*TP6 [射流技术](../Page/射流技术.md "wikilink")（流控技术）

:\*TP7 [遥感技术](../Page/遥感技术.md "wikilink")

::\*TP73 探测仪器及系统

:\*TP8 [远动技术](../Page/远动技术.md "wikilink")

-----

  -

[Category:中国图书馆图书分类法](../Category/中国图书馆图书分类法.md "wikilink")