**Jump
Festa**（）是雜誌[週刊少年Jump](../Page/週刊少年Jump.md "wikilink")、[V-Jump](../Page/V-Jump.md "wikilink")、[Jump
Square](../Page/Jump_Square.md "wikilink")（至2007年Jump
Festa前是[月刊少年Jump](../Page/月刊少年Jump.md "wikilink")）舉辦的活動。

## 概要

從2000年開始，每年12月中～下旬（第三週的星期六）在[幕張展覽館舉辦](../Page/幕張展覽館.md "wikilink")。每年動員8～10萬人。初回會場是在[東京國際展示場](../Page/東京國際展示場.md "wikilink")。

在會場內先行發售JUMP系的漫畫商品等，有週刊少年Jump的世界體驗的體驗展示、原畫展示，動畫[聲優演出的Jump舞台](../Page/聲優.md "wikilink")，賣漫畫上的食物等的Jump攤販，也有Jump作品遊戲化的關係公司的展示。也有販賣[JUMP
SHOP過去的商品](../Page/JUMP_SHOP.md "wikilink")。

## Anime Tour

**1995年**

  - [橙路](../Page/橙路.md "wikilink")
  - [烏龍派出所](../Page/烏龍派出所.md "wikilink")

**1998年**

  - [ONE PIECE 打倒！海賊強薩克！](../Page/ONE_PIECE_打倒！海賊強薩克！.md "wikilink")
  - [Hunter × Hunter](../Page/Hunter_×_Hunter.md "wikilink")
  - [世紀末領袖傳\!](../Page/世紀末領袖傳!.md "wikilink")

**2002年**

  - [棋魂](../Page/棋魂.md "wikilink") （裁決的一局\! 綻放吧 往昔之花\!\!）
  - [搞笑漫畫日和](../Page/搞笑漫畫日和.md "wikilink")
  - [网球王子](../Page/网球王子.md "wikilink") A DAY OF SURVIVAL MOUNTAIN
    ～恐怖的強化訓練～

**2003年**

  - [火影忍者](../Page/火影忍者.md "wikilink") 尋找紅色四葉草
  - [网球王子](../Page/网球王子.md "wikilink") THE BAND OF PRINCES FILM THE
    FUTURE\!

**2004年**

  - [火影忍者](../Page/火影忍者.md "wikilink") 在隱蔽瀑布的死鬥 我就是英雄！
  - [光速蒙面俠21](../Page/光速蒙面俠21.md "wikilink") 幻的黃金盃
  - [草莓100%](../Page/草莓100%.md "wikilink") 戀愛開始\!? 攝影合宿 ～動搖的心向東向西～
  - [BLEACH](../Page/BLEACH.md "wikilink") Memories in the rain

**2005年** 9-10月

  - [光速蒙面俠21](../Page/光速蒙面俠21.md "wikilink") 前往聖誕盃的道路～南島的特訓！YA-HA-\!～
  - [BLEACH](../Page/BLEACH.md "wikilink") The sealed sword frenzy
  - [银魂](../Page/银魂.md "wikilink")～就算沒喝醉也要裝成酩酊大醉好讓上司有面子～

**2008年** 9月21日-11月23日

  - [搞怪吹笛手](../Page/搞怪吹笛手.md "wikilink")～上映前注意～
  - [BLEACH](../Page/BLEACH.md "wikilink")
  - [遊戲王5D's](../Page/遊戲王5D's.md "wikilink")～進化的決鬥！星塵龍VS真紅眼魔龍～
  - [七龙珠](../Page/七龙珠.md "wikilink") 好！歸來的孫悟空與同伴們\!\!
  - [藍龍](../Page/藍龍.md "wikilink")
  - [信蜂](../Page/信蜂.md "wikilink")～光與青的幻想夜話～
  - 劇場版[银魂預告](../Page/银魂.md "wikilink")～白夜叉降誕～
  - [ONE PIECE ROMANCE DAWN
    STORY](../Page/ONE_PIECE_ROMANCE_DAWN_STORY.md "wikilink")

**2009年** 10月12日-11月29日

  - [信蜂](../Page/信蜂.md "wikilink") 上映前注意
  - [家庭教師HITMAN REBORN\!](../Page/家庭教師HITMAN_REBORN!.md "wikilink")
    彭哥列家族總登場！彭哥列式修業旅行來了\!\!
  - [美食獵人TORIKO](../Page/美食獵人TORIKO.md "wikilink")
  - [火影忍者THE](../Page/火影忍者.md "wikilink") CROSS ROADS
  - 10週年紀念 劇場版[遊戲王](../Page/遊戲王.md "wikilink") 預告映像
  - [ONE PIECE FILM STRONG
    WORLD](../Page/ONE_PIECE_FILM_STRONG_WORLD.md "wikilink") 預告映像

**2010年** 10月23日-11月21日

  - [惡魔奶爸](../Page/惡魔奶爸.md "wikilink") 撿到大魔王的嬰兒\!?
  - [BLEACH](../Page/BLEACH.md "wikilink")
  - [SKET DANCE動畫化告知CM](../Page/SKET_DANCE.md "wikilink")
  - [信蜂](../Page/信蜂.md "wikilink") REVERSE
  - [靈異E接觸動畫化告知CM](../Page/靈異E接觸.md "wikilink")
  - [妖怪少爺](../Page/妖怪少爺.md "wikilink")
  - [美食獵人TORIKO](../Page/美食獵人TORIKO.md "wikilink")

**2013年** 10月6日-11月24日

  - [偽戀](../Page/偽戀.md "wikilink")
  - [黑子的籃球](../Page/黑子的籃球.md "wikilink")
  - [暗殺教室](../Page/暗殺教室.md "wikilink") 修學旅行篇

**2016年**

  - [我的英雄學院](../Page/我的英雄學院.md "wikilink") -預計-
  - [黑色五叶草](../Page/黑色五叶草.md "wikilink") -預計-

## 外部連結

  - [官方網站](http://www.jumpfesta.com/)

[Category:週刊少年Jump](../Category/週刊少年Jump.md "wikilink")
[Category:動畫會展](../Category/動畫會展.md "wikilink")
[\*](../Category/月刊少年Jump.md "wikilink")
[\*](../Category/Jump_Square.md "wikilink")
[\*](../Category/V_Jump.md "wikilink")
[Category:遊戲會展](../Category/遊戲會展.md "wikilink")
[Category:日本书展](../Category/日本书展.md "wikilink")