**開放大學**（），也譯作**公開大學**，是[英國的](../Page/英國.md "wikilink")[-{zh-hans:远程教育;
zh-hant:遙距教育;}-](../Page/遙距教育.md "wikilink")[大學](../Page/大學.md "wikilink")，於1969年建立，1971年1月招收第一批學生。該大學大部分的學生是住在英國的，但也有來自[歐洲其他地方](../Page/歐洲.md "wikilink")、[非洲和](../Page/非洲.md "wikilink")[遠東的學生](../Page/遠東.md "wikilink")。当中許多的學生（包括大學部及研究所的學生）都在校外接受遠距教學。研究所學生可以使用校內48公頃的土地以及各種實驗設施，來進行研究。大學行政部門位於[英國東南部](../Page/英國.md "wikilink")[白金漢郡](../Page/白金漢郡.md "wikilink")[米爾頓凱恩斯的](../Page/米爾頓凱恩斯.md "wikilink")[沃爾頓大廳](../Page/沃爾頓大廳.md "wikilink")，另外在英國其他地方設立了有13座區域中心，其餘的歐洲各國也大多設有該大學的區域考試中心。公開大學有授予[學士學位和](../Page/學士學位.md "wikilink")[碩士學位](../Page/碩士學位.md "wikilink")，也有非學位類的證明，如文憑和證書，或繼續教育單位。該校有超過1,000名學者以及2500名行政人員、業務和支援人員。

公開大學有180,000名學生入學，其中包括了25,000名海外學生，若以學生數量來說，它是[英國本土及歐洲最大的學術單位](../Page/英國.md "wikilink")，並且也是世界上最大的大學之一。自從創校以來，已經有超過300萬學生修習過該大學所開設的課程。在[英國政府所舉辦的國際學生滿意度調查](../Page/英國政府.md "wikilink")，英國空中大學分別於2005年，2006年拿下第一，2007年第二的成績。

公開大學也是世界上兩所獲得美國[中部各州高等教育委員會評鑑通過](../Page/中部各州高等教育委員會.md "wikilink")，且有學院機構進駐的學校之一，也獲得[美國教育部長及](../Page/美國.md "wikilink")[高等教育委員會的認可](../Page/高等教育委員會.md "wikilink")。

## 参考文献

## 外部連結

  - [Open University](http://www.open.ac.uk/)– Main website
  - [H2G2 Open University
    Information](http://www.bbc.co.uk/dna/h2g2/A592490) at the
    [BBC](../Page/BBC.md "wikilink")

[英国公开大学](../Category/英国公开大学.md "wikilink")
[Category:1969年創建的教育機構](../Category/1969年創建的教育機構.md "wikilink")