《**亂**》（日語：****，英語：**）是[黑澤明晚年的代表作品](../Page/黑澤明.md "wikilink")，自[莎士比亞的](../Page/莎士比亞.md "wikilink")《[李爾王](../Page/李爾王.md "wikilink")》以及根據[毛利元就折箭的故事改編](../Page/毛利元就.md "wikilink")。

本片预算经费为1200万美元，这部电影在當時创下了日本电影制作费纪录。\[1\]《乱》在1985年5月31日的[东京国际电影节发布](../Page/东京国际电影节.md "wikilink")，同年7月1日在日本上映。这部电影将色彩运用到极致，其服装设计[和田惠美因此部电影获得了](../Page/和田惠美.md "wikilink")[奥斯卡最佳服装设计奖](../Page/奥斯卡.md "wikilink")。

## 演員表

一文字秀虎：[仲代達矢](../Page/仲代達矢.md "wikilink")
一文字太郎孝虎：[寺尾聰](../Page/寺尾聰.md "wikilink")
一文字次郎正虎：[根津甚八](../Page/根津甚八.md "wikilink")
一文字三郎直虎：[隆大介](../Page/隆大介.md "wikilink")
楓之方（太郎孝虎正室）：[原田美枝子](../Page/原田美枝子.md "wikilink")
末之方（次郎正虎正室）：[宮崎美子](../Page/宮崎美子.md "wikilink")
鶴丸（末之方弟）：野村武司（後襲名為[野村萬齋](../Page/野村萬齋.md "wikilink")）
鐵修理（次郎正虎側近）：[井川比佐志](../Page/井川比佐志.md "wikilink")
狂阿彌（秀虎臣）：[池畑慎之介](../Page/池畑慎之介.md "wikilink")
平出丹後（秀虎重臣）：[油井昌由樹](../Page/油井昌由樹.md "wikilink")
長山主水：[伊藤敏八](../Page/伊藤敏八.md "wikilink")
白根左門（次郎正虎側近）：[兒玉謙次](../Page/兒玉謙次.md "wikilink")
生駒勘解由（秀虎重臣）：[加藤和夫](../Page/加藤和夫.md "wikilink")
小倉主馬助（太郎孝虎側近）：[松井範雄](../Page/松井範雄.md "wikilink")
藤-{卷}-的老將：鈴木平八郎、渡邊隆
秀虎的側室：[南條玲子](../Page/南條玲子.md "wikilink")、古知佐和子
楓的老侍女：[東鄉晴子](../Page/東鄉晴子.md "wikilink")
末的老侍女：神田時枝
秀虎側室的老侍女：[音羽久米子](../Page/音羽久米子.md "wikilink")
畠山小彌太：[加藤武](../Page/加藤武.md "wikilink")
綾部政治（鄰國領主：[田崎潤](../Page/田崎潤.md "wikilink")
藤-{卷}-信弘（鄰國領主）：[植木等](../Page/植木等.md "wikilink")
其他：[頭師孝雄](../Page/頭師孝雄.md "wikilink")、[頭師佳孝](../Page/頭師佳孝.md "wikilink")

## 獲獎紀錄

  - 聖賽巴斯丁影展OCIC Award
  - 美國國家評論獎
  - 日本[每日電影獎最佳導演](../Page/每日電影獎.md "wikilink")
  - 日本每日電影獎最佳影片獎
  - 義大利電影金像獎最佳外語片
  - [法國凱撒電影獎最佳外語片](../Page/法國凱撒電影獎.md "wikilink")
  - 日本[藍絲帶獎最佳影片](../Page/藍絲帶獎_\(電影\).md "wikilink")
  - 英國影藝學院最佳外語片
  - 1986年[奧斯卡金像獎最佳服裝設計](../Page/奧斯卡金像獎.md "wikilink")

## 外部連結

  -
## 参考资料

[Category:黑澤明電影](../Category/黑澤明電影.md "wikilink")
[Category:日本电影作品](../Category/日本电影作品.md "wikilink")
[Category:1985年电影](../Category/1985年电影.md "wikilink")
[Category:日语电影](../Category/日语电影.md "wikilink")
[Category:日本剧情片](../Category/日本剧情片.md "wikilink")
[Category:1980年代剧情片](../Category/1980年代剧情片.md "wikilink")
[Category:戰國時代背景電影
(日本)](../Category/戰國時代背景電影_\(日本\).md "wikilink")
[Category:莎士比亚作品题材作品](../Category/莎士比亚作品题材作品.md "wikilink")
[Category:每日電影獎日本電影大獎獲獎作品](../Category/每日電影獎日本電影大獎獲獎作品.md "wikilink")
[Category:藍絲帶獎最佳影片](../Category/藍絲帶獎最佳影片.md "wikilink")
[Category:奥斯卡最佳服装设计奖获奖电影](../Category/奥斯卡最佳服装设计奖获奖电影.md "wikilink")
[Category:英国电影学院奖最佳外语片](../Category/英国电影学院奖最佳外语片.md "wikilink")
[Category:凯撒电影奖最佳外语片](../Category/凯撒电影奖最佳外语片.md "wikilink")
[Category:纽约影评人协会奖最佳外语片](../Category/纽约影评人协会奖最佳外语片.md "wikilink")

1.