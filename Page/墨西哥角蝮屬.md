**墨西哥角蝮屬**（[學名](../Page/學名.md "wikilink")：*Ophryacus*）是[蛇亞目](../Page/蛇亞目.md "wikilink")[蝰蛇科](../Page/蝰蛇科.md "wikilink")[蝮亞科下的一個](../Page/蝮亞科.md "wikilink")[有毒蛇](../Page/毒蛇.md "wikilink")[屬](../Page/屬.md "wikilink")，主要包括分布於[墨西哥一帶的一種蝮蛇](../Page/墨西哥.md "wikilink")。其學名「Ophryacus」源自[拉丁語](../Page/拉丁語.md "wikilink")，當中「Ophtys」意思是「眉額」，而「-acutus」的詞綴代表「屬於」，二詞所指的正是墨西哥角蝮長於額上的鱗角。\[1\]目前已有2個品種被確認。\[2\]

## 特徵

墨西哥角蝮體長達55至70[公分](../Page/公分.md "wikilink")，在牠們雙眼間的眉額位置長有一枚相當突出、具標誌性的鱗角，因而得此命名。\[3\]地理分布方面，墨西哥角蝮僅分布於[墨西哥中部及南部的山區](../Page/墨西哥.md "wikilink")。\[4\]

## 品種

| 品種\[5\]                                                                     | 學名及命名者\[6\]                                    | 異稱\[7\] | 地理分布\[8\]                                                                        |
| --------------------------------------------------------------------------- | ---------------------------------------------- | ------- | -------------------------------------------------------------------------------- |
| **[黑尾角蝮](../Page/黑尾角蝮.md "wikilink")**                                      | Ophryacus melanurus，<small>Müller，1924</small> |         | [墨西哥山區](../Page/墨西哥.md "wikilink")                                               |
| **[墨西哥角蝮](../Page/墨西哥角蝮.md "wikilink")**<font size="-1"><sup>T</sup></font> | Ophryacus undulatus，<small>Jan，1859</small>    |         | [墨西哥中部及南部的山區](../Page/墨西哥.md "wikilink")，包括[委內瑞拉等地區](../Page/委內瑞拉.md "wikilink") |
|                                                                             |                                                |         |                                                                                  |

## 備註

## 外部連結

  - [TIGR爬蟲類資料庫：墨西哥角蝮屬](http://reptile-database.reptarium.cz/search.php?&genus=Ophryacus&submit=Search)

[Category:蝰蛇科](../Category/蝰蛇科.md "wikilink")

1.  Campbell JA、Lamar WW：《The Venomous Reptiles of the Western
    Hemisphere》頁870，[倫敦](../Page/倫敦.md "wikilink")：Comstock Publishing
    Associates，2004年。ISBN 0-8014-4141-2

2.

3.
4.
5.
6.
7.
8.