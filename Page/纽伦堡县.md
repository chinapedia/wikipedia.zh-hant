**纽伦堡县**（Landkreis Nürnberger
Land）是[德国](../Page/德国.md "wikilink")[巴伐利亚州的一个县](../Page/巴伐利亚州.md "wikilink")，隶属于[中弗兰肯行政区](../Page/中弗兰肯行政区.md "wikilink")，首府[佩格尼茨河畔劳夫](../Page/佩格尼茨河畔劳夫.md "wikilink")。

## 華語譯名

由於兩岸對於德國行政區「**Landkreis**」翻譯的不同，台灣譯為「**紐倫堡-{zh-tw:郡;
zh-cn:郡}-**」；中國大陸譯為「**纽伦堡-{zh-tw:縣; zh-cn:县}-**」。

## 地理

纽伦堡县北面与[福希海姆县和](../Page/福希海姆县.md "wikilink")[拜罗伊特县](../Page/拜罗伊特县.md "wikilink")，东面与[安贝格-苏尔茨巴赫县](../Page/安贝格-苏尔茨巴赫县.md "wikilink")，南面与[诺伊马克特县](../Page/诺伊马克特县.md "wikilink")，西面与[罗特县](../Page/罗特县.md "wikilink")、[埃尔朗根-赫希施塔特县和](../Page/埃尔朗根-赫希施塔特县.md "wikilink")[纽伦堡市相邻](../Page/纽伦堡.md "wikilink")。

[N](../Category/巴伐利亚州行政区划.md "wikilink")