## 摘要

| 描述摘要 | Icon of Copa America |
| ---- | -------------------- |
| 來源   | Pete                 |
| 日期   | 22-08-2007           |
| 作者   | Pete                 |
| 許可   | below {{\#switch:    |

## 许可协议

[de:Datei:Venezuela-2007.png](../Page/de:Datei:Venezuela-2007.png.md "wikilink")
[en:<File:Venezuela-2007.PNG>](../Page/en:File:Venezuela-2007.PNG.md "wikilink")
[fr:Fichier:Venezuela-2007.PNG](../Page/fr:Fichier:Venezuela-2007.PNG.md "wikilink")
[hu:Fájl:Venezuela-2007.PNG](../Page/hu:Fájl:Venezuela-2007.PNG.md "wikilink")
[lt:Vaizdas:800px-Venezuela-2007.png](../Page/lt:Vaizdas:800px-Venezuela-2007.png.md "wikilink")
[ro:Fișier:Venezuela-2007.PNG](../Page/ro:Fișier:Venezuela-2007.PNG.md "wikilink")
[ru:Файл:Venezuela Copa América
2007.png](../Page/ru:Файл:Venezuela_Copa_América_2007.png.md "wikilink")
[sv:Fil:Venezuela-2007.png](../Page/sv:Fil:Venezuela-2007.png.md "wikilink")