**賀姓**为[中文姓氏之一](../Page/中文姓氏.md "wikilink")，在《[百家姓](../Page/百家姓.md "wikilink")》中排名第70位。

## 起源

  - 出自[姜姓](../Page/姜姓.md "wikilink")，是为[帝王名](../Page/帝王.md "wikilink")[避讳所改的](../Page/避讳.md "wikilink")[姓氏](../Page/姓氏.md "wikilink")。[春秋时](../Page/春秋.md "wikilink")，[齐桓公](../Page/齐桓公.md "wikilink")（姜姓）有个孙子叫公孙庆克，他的儿子庆封以父名命氏，称为[庆氏](../Page/庆氏.md "wikilink")。庆封在[齐灵公时任](../Page/齐灵公.md "wikilink")[大夫](../Page/大夫.md "wikilink")，在[齐庄公时与](../Page/齐庄公.md "wikilink")[崔杼曾为上卿](../Page/崔杼.md "wikilink")，执掌国政。后两人再升为左右[相国](../Page/相国.md "wikilink")。因崔杼家内发生内乱，庆封以[弑君罪灭掉崔氏](../Page/弑君.md "wikilink")，独霸朝政。庆封得意，自己享樂，縱慾酒色，把[政權交给儿子庆舍](../Page/政權.md "wikilink")，引起了[朝野对庆氏的不满](../Page/朝野.md "wikilink")，后庆封的亲信卢蒲癸和王何，趁庆封外出之机，杀死了庆舍，庆封见势不妙，便逃到了[吴国](../Page/吴国.md "wikilink")。吴王把[朱方封给庆封](../Page/朱方.md "wikilink")，庆氏[宗室闻讯赶来相聚](../Page/宗室.md "wikilink")，从此，庆氏比在齐国时还要富裕。至[西汉末](../Page/西汉.md "wikilink")，子孙徙[会稽](../Page/會稽郡.md "wikilink")[山阴](../Page/山阴县_\(浙江省\).md "wikilink")，东汉时传至庆仪为[汝阴令](../Page/汝阴县_\(秦朝\).md "wikilink")，其曾孙庆纯官拜[侍中](../Page/侍中.md "wikilink")，为避[汉安帝的父亲刘庆的名讳](../Page/汉安帝.md "wikilink")，"庆"字改为同义的"贺"字。庆纯改为贺纯。史称贺姓正宗。是为[江苏贺氏](../Page/江苏.md "wikilink")。

<!-- end list -->

  - 出自[少數民族改姓](../Page/少數民族.md "wikilink")。据《[魏书](../Page/魏书.md "wikilink")。官氏志》等所载，[南北朝时](../Page/南北朝.md "wikilink")[孝文漢化](../Page/孝文漢化.md "wikilink")，[迁都](../Page/迁都.md "wikilink")[洛邑后](../Page/洛邑.md "wikilink")，[北魏孝文帝将](../Page/北魏孝文帝.md "wikilink")[鲜卑族复姓](../Page/鲜卑族.md "wikilink")[贺兰](../Page/贺兰.md "wikilink")、[贺拔](../Page/贺拔.md "wikilink")、[贺狄](../Page/贺狄.md "wikilink")、贺赖、贺敦等氏皆改为[汉姓贺氏](../Page/汉姓.md "wikilink")。是为[河南贺氏](../Page/河南.md "wikilink")。

<!-- end list -->

  - 出自[日本人](../Page/日本人.md "wikilink")，[日本](../Page/日本.md "wikilink")[浪人姓](../Page/浪人.md "wikilink")[伊賀氏](../Page/伊賀.md "wikilink")、[加賀氏之兩人為](../Page/加賀.md "wikilink")[倭寇劫掠](../Page/倭寇.md "wikilink")[泉州](../Page/泉州.md "wikilink")，諸倭寇回[日本後](../Page/日本.md "wikilink")，兩人感義覺迷，反倒幫助[明朝](../Page/明朝.md "wikilink")，參與平定[李闖之役](../Page/李闖.md "wikilink")，後兩人改姓賀氏，自署[郡望](../Page/郡望.md "wikilink")。二人子孫留於[中國](../Page/中國.md "wikilink")，時稱「東賀」。

## 古代名人

  - [贺齐](../Page/贺齐.md "wikilink")：
    [三国时期](../Page/三国时期.md "wikilink")[吴国名将](../Page/吴国.md "wikilink")，[徐州](../Page/徐州.md "wikilink")[牧](../Page/牧.md "wikilink")
  - [賀邵](../Page/賀邵.md "wikilink")：三國吳末名臣，賀齊之孫
  - [賀循](../Page/賀循.md "wikilink")：西晉名儒，賀邵之子
  - [賀知章](../Page/賀知章.md "wikilink")：[唐朝](../Page/唐朝.md "wikilink")[詩人](../Page/詩人.md "wikilink")。
  - [賀鑄](../Page/賀鑄.md "wikilink")：[北宋詩人](../Page/北宋.md "wikilink")
  - [賀太平](../Page/賀太平.md "wikilink")：[元朝](../Page/元朝.md "wikilink")[官吏](../Page/官吏.md "wikilink")，原為[漢人](../Page/漢人.md "wikilink")，後改為[蒙古族](../Page/蒙古族.md "wikilink")。
  - [贺长龄](../Page/贺长龄.md "wikilink")：[清朝](../Page/清朝.md "wikilink")[官吏](../Page/官吏.md "wikilink")，号耐庵，[湖南](../Page/湖南.md "wikilink")[善化人](../Page/善化.md "wikilink")。

## 現代名人

  - [賀龍](../Page/賀龍.md "wikilink")：[中華人民共和國的](../Page/中華人民共和國.md "wikilink")[十大元帥之一](../Page/十大元帥.md "wikilink")。
  - [賀子珍](../Page/賀子珍.md "wikilink")：[毛澤東第三任夫人](../Page/毛澤東.md "wikilink")。
  - [賀軍翔](../Page/賀軍翔.md "wikilink")：[台灣](../Page/台灣.md "wikilink")[藝人](../Page/藝人.md "wikilink")
  - [贺国强](../Page/贺国强.md "wikilink")，[中国政治人物](../Page/中华人民共和国.md "wikilink")
  - [贺万里](../Page/贺万里.md "wikilink")，[中国山水画](../Page/中国山水画.md "wikilink")[画家](../Page/画家.md "wikilink")，[美术理论家](../Page/美术理论家.md "wikilink")

## 贺陈姓

《註》[賀陳旦](../Page/賀陳旦.md "wikilink")(台灣企業家，曾任[中華電信董事長](../Page/中華電信.md "wikilink"))不姓[賀](../Page/賀.md "wikilink")，是為複姓[賀陳](../Page/賀陳.md "wikilink")。

## 参考

[会稽贺氏世系图](../Page/会稽贺氏世系图.md "wikilink")

[H賀](../Category/漢字姓氏.md "wikilink") [\*](../Category/贺姓.md "wikilink")