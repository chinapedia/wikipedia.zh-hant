**蒂倫豪特戰役**（Battle of
Turnhout）是[八十年戰爭的其中一個戰役](../Page/八十年戰爭.md "wikilink")，發生於1597年[北尼德蘭與](../Page/北尼德蘭.md "wikilink")[南尼德蘭交界的](../Page/南尼德蘭.md "wikilink")[蒂爾瑙特](../Page/蒂爾瑙特.md "wikilink")。

## 背景

雖然蒂倫豪特沒有城牆，但這個城鎮仍具有優越的戰略價值。1597年1月24日，[莫里斯王子帶領的騎兵隊在鄰近的](../Page/莫里斯王子.md "wikilink")[提倫打敗了軍容較大](../Page/提倫.md "wikilink")，由伐拉克斯率領的[西班牙](../Page/西班牙.md "wikilink")[騎兵隊](../Page/騎兵.md "wikilink")。

[荷蘭軍隊燒燬當地的城堡後](../Page/荷蘭.md "wikilink")，西班牙騎兵隊逃之夭夭。然而，一向精明的莫里斯竟未趁勝追擊，將勝利的成果擴大。

## 註腳

[Category:八十年戰爭戰役](../Category/八十年戰爭戰役.md "wikilink")