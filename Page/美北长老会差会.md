**美北长老会差会（American Presbyterian Mission (North)，PN)**
是[美北长老会的海外传教机构](../Page/美北长老会.md "wikilink")。1831年在[匹兹堡成立](../Page/匹兹堡.md "wikilink")[差会](../Page/差会.md "wikilink")。1838年，第一位传教士抵达[新加坡](../Page/新加坡.md "wikilink")。1959年南北长老会再度合并。

## 在中国

1844年6月21日，美北长老会派遣[麦嘉缔](../Page/麦嘉缔.md "wikilink")（D.B.
McCartee）博士抵达[中国浙江省](../Page/中国.md "wikilink")[宁波](../Page/宁波.md "wikilink")。1890年，美北长老会在华總共有48名傳教士，18名女助手，23名中國牧師，8名本地助手，近4千名聖餐者。
1919年，美北长老会的在华传教士有502人，仅次于[中国内地会](../Page/中国内地会.md "wikilink")；传教站36个，次于中国内地会和[英国圣公会](../Page/英国圣公会.md "wikilink")。\[1\]\[2\]

### 华中教区（CENTRAL CHINA MISSION）

华中教区是美北长老会首先进入的地区，负责在[长江三角洲一带的](../Page/长江三角洲.md "wikilink")[吴语区进行传教](../Page/吴语.md "wikilink")。

  - 传教站:自1844年[麦嘉缔博士](../Page/麦嘉缔.md "wikilink")[宁波后](../Page/宁波.md "wikilink")，[上海](../Page/上海.md "wikilink")(1850-1950)，[杭州](../Page/杭州.md "wikilink")(1859)，[苏州](../Page/苏州.md "wikilink")(1872)，[余姚](../Page/余姚.md "wikilink")（1918）也陆续被开辟。
  - 教堂：宁波府前街大教堂、槐树路会堂；杭州[思澄堂](../Page/思澄堂.md "wikilink")、[鼓楼堂](../Page/鼓楼堂.md "wikilink")；上海大南门[清心堂](../Page/清心堂.md "wikilink")、窦乐安路[鸿德堂](../Page/鸿德堂.md "wikilink")、宝通路[闸北堂](../Page/闸北堂.md "wikilink")；苏州上津桥[救恩堂](../Page/救恩堂.md "wikilink")
  - 学校：杭州[之江大学](../Page/之江大学.md "wikilink")；上海清心中学、清心女中；苏州萃英中学
  - 医院：苏州更生医院，余姚惠爱医院
  - 上海[美华书馆](../Page/美华书馆.md "wikilink")：是一个重要的出版印刷机构。

### 华南教区 SOUTH CHINA MISSION

华中教区是美北长老会第二个进入的地区，负责在[广东省中部和西南部地区进行传教](../Page/广东省.md "wikilink")。

  - 传教站:[广州](../Page/广州.md "wikilink")(1845)、
    [连州](../Page/连州.md "wikilink")(1890)、[阳江](../Page/阳江.md "wikilink")(1892)、[高州](../Page/高州.md "wikilink")。
  - 教堂：广州[仁济堂](../Page/仁济堂.md "wikilink")、[逢源堂](../Page/逢源堂.md "wikilink")、[中华堂](../Page/中华堂.md "wikilink")、芳村平民堂、[白鹤洞堂](../Page/白鹤洞堂.md "wikilink")、黄沙堂；
  - 学校：广州[岭南大学](../Page/岭南大学_\(广州\).md "wikilink")、白鹤洞真光女中、培英中学
  - 医院：广州博济医院、柔济医院、阳江化民博济医院、连州惠爱医院

### 山东教区（SHANTUNG MISSION）

山东教区是美北长老会第三个进入的地区，传教范围包括几乎全部[山东省](../Page/山东省.md "wikilink")。1890年约有3000信徒。

  - 传教站:
    美北长老会山东差会首先进入[登州](../Page/登州.md "wikilink")([蓬莱](../Page/蓬莱.md "wikilink")，1861)，随后进入新开辟的通商口岸[烟台](../Page/烟台.md "wikilink")(1862)，1874年进入省会[济南](../Page/济南.md "wikilink")，1883年在中部的[潍县建立起重要的传教中心](../Page/潍县.md "wikilink")，1890年以后主要向山东南部扩展，先后进入[沂州](../Page/沂州.md "wikilink")（[临沂](../Page/临沂.md "wikilink")，1890）、[济宁](../Page/济宁.md "wikilink")（1892）、[峄县](../Page/峄县.md "wikilink")（枣庄市[峄城区](../Page/峄城区.md "wikilink")，1905）、[滕县](../Page/滕县.md "wikilink")(1913)，此外，在1898年进入德国租借地[青岛](../Page/青岛.md "wikilink")。
  - 教堂：潍县[乐道院教堂](../Page/乐道院.md "wikilink")；烟台[毓璜顶教堂](../Page/毓璜顶教堂.md "wikilink")；济南东关教堂；[滕县北关教堂](../Page/滕县北关教堂.md "wikilink")
  - 学校：济南[齐鲁大学](../Page/齐鲁大学.md "wikilink")；烟台[毓璜顶益文商专](../Page/毓璜顶益文商专.md "wikilink")；[青岛崇德中学](../Page/青岛第十一中学.md "wikilink")；[滕县华北弘道院](../Page/滕县华北弘道院.md "wikilink")
  - 医院：潍县教会医院，烟台[毓璜顶医院](../Page/毓璜顶医院.md "wikilink")，济宁德门医院，沂州教会医院，峄县瑞门德男女医院，滕县华北医院，

### 华北教区（NORTH CHINA MISSION）

  - 传教站:北京(1863)，保定(1893)，[邢台](../Page/邢台.md "wikilink")（1903）。
  - 教堂：北京交道口教堂
  - 学校：北京交道口
  - 医院：北京交道口[道济医院](../Page/道济医院.md "wikilink")（今第六医院），[保定思罗医院](../Page/保定思罗医院.md "wikilink")，[邢台福音医院](../Page/邢台福音医院.md "wikilink")

### 江安教区（KIANGNAN MISSION）

江安教区负责在[江苏](../Page/江苏.md "wikilink")、[安徽](../Page/安徽.md "wikilink")2省的[官话区传教](../Page/官话.md "wikilink")，先在南京落脚，以后主要在安徽省北部的[淮河两岸传播](../Page/淮河.md "wikilink")，发展到1万多人的规模。

  - 传教站:[南京](../Page/南京.md "wikilink")(1874)；[怀远](../Page/怀远.md "wikilink")(1901)，[宿州](../Page/宿州.md "wikilink")(1913)，寿州(1921)
    。
  - 教堂：南京莫愁路[汉中堂](../Page/汉中堂.md "wikilink")、户部街教堂、双塘教堂；
  - 学校：南京金陵大学；莫愁路明德女中，怀远淮西中学，
  - 医院：怀远民望医院，民康女医院，宿州民爱医院，寿县春华医院。

### 海南教区（HAINAN MISSION）

  - 传教站:琼州－海口(1885)，嘉积(琼海，1900)，那大(儋州，1886).
  - 教堂：
  - 医院：海口福音医院（今海南省人民医院），嘉积基督医院，那大福音医院，

### 湖南教区（HUNAN MISSION）

  - 传教站:常德(1898)，郴州(1903)，衡州(1902);[湘潭](../Page/湘潭.md "wikilink")(1900)，长沙(1912)，
  - 教堂：长沙北门永仁堂
  - 医院：衡阳仁济医院，郴州惠爱医院，湘潭惠景医院，桃源问津医院，常德广德医院

### 云南教区（YUNNAN MISSION）

  - 传教站:九龙江(1917-1933), 元江(1924-1933)

<!-- end list -->

  - 1925年26个传教站加入[中华基督教会](../Page/中华基督教会.md "wikilink")；山东有5个传教站仍留在中华长老会。但在上海共有一个财务机构（司库）。

## 著名人物

  - [哈巴](../Page/哈巴.md "wikilink")（Happer, Andrew Patton）
  - [麦嘉缔](../Page/麦嘉缔.md "wikilink")（McCartee, Divie Bethune）
  - [倪维思](../Page/倪维思.md "wikilink")
  - [狄考文](../Page/狄考文.md "wikilink")
  - [郭显德](../Page/郭显德.md "wikilink")
  - [娄理华](../Page/娄理华.md "wikilink")
  - [费启鸿](../Page/费启鸿.md "wikilink")
  - [嘉约翰](../Page/嘉约翰.md "wikilink")

## 參考文献

<references />

{{-}}

[Category:基督教教會與教派](../Category/基督教教會與教派.md "wikilink")
[Category:基督教在华差会](../Category/基督教在华差会.md "wikilink")

1.  《[中华归主](../Page/中华归主.md "wikilink")》
2.