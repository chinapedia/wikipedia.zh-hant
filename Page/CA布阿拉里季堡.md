**CA布阿拉里季堡**（**Chabab Ahly Bordj Bou Arreridj**）（, *CA
BBA*）是位于[阿尔及利亚](../Page/阿尔及利亚.md "wikilink")[布阿拉里季堡的足球俱乐部](../Page/布阿拉里季堡.md "wikilink")，始建于1931年，球队队色为黄色和黑色。主体育场是可以容纳8,000人的1955年8月20日综合运动场（Complexe
Sportif du 20 août 1955）。

## 外部链接

  - [footballsquads.co.uk](http://www.footballsquads.co.uk/algeria/2005-2006/div1/bordj.htm)

[Category:阿尔及利亚足球俱乐部](../Category/阿尔及利亚足球俱乐部.md "wikilink")