**有孔蟲門**（[学名](../Page/学名.md "wikilink")：），為[變形蟲狀](../Page/變形蟲狀.md "wikilink")[原生生物的大分類](../Page/原生生物.md "wikilink")。牠們擁有的網狀假足及幼細線狀細胞質會分散及融合而形成動態的網\[1\]，它們會形成有一個或多個室的外殼，部分在結構上有高度發展\[2\]。牠們大部分大小小於一[毫米](../Page/毫米.md "wikilink")，但部分則較大，紀錄中最大的樣本大小達19[厘米](../Page/厘米.md "wikilink")\[3\]。

雖然現尚未有[形態學上相關的支持](../Page/形態學.md "wikilink")，分子資料強力提出因為有孔蟲門與[絲足蟲類](../Page/絲足蟲類.md "wikilink")（）及[放散蟲門](../Page/放散蟲門.md "wikilink")（）都為變形蟲狀及有複習外殼，所以有密切關係。這三類形成有孔蟲界（）\[4\]。但有孔蟲門與其他類別的實際關係還不是完全清楚。

## 现生有孔蟲門生物

### 生存環境及形態

[live_Ammonia_tepida.jpg](https://zh.wikipedia.org/wiki/File:live_Ammonia_tepida.jpg "fig:live_Ammonia_tepida.jpg")（*Ammonia
tepida*）\]\]
現代有孔蟲門生物主要為海洋生物，它們也可以在其他含有一定盐分的環境生存\[5\]。部分物種在淡水生存及一種在潮濕的雨林土壤中生活。它們在[小型底棲生物](../Page/小型底棲生物.md "wikilink")（meiobenthos）中十分常見，有約40種為[浮游生物](../Page/浮游生物.md "wikilink")\[6\]。但是，这个数字可能只部分正确，因為有很多基因上不同的物種在形態上是不能夠区分的\[7\]。其細胞分為粒狀內質（endoplasm）及透明外質（ectoplasm）。其假足狀的網可以在一個開口或在外殼的很多穿孔中出現，其特色是當中有細粒在網中自由流動\[8\]。
[benthic_foraminifera.jpg](https://zh.wikipedia.org/wiki/File:benthic_foraminifera.jpg "fig:benthic_foraminifera.jpg")

### 活動及進食

假足的作用在於移動、固定及捕捉食物，包括細少生物例如[矽藻](../Page/矽藻.md "wikilink")（diatom）或[細菌](../Page/細菌.md "wikilink")\[9\]。部分形態為以[內共生體](../Page/內共生體.md "wikilink")（endosymbiont）的單細胞[藻類](../Page/藻類.md "wikilink")，有多種多樣的世系如[綠藻](../Page/綠藻.md "wikilink")（green
alga）、[紅藻](../Page/紅藻.md "wikilink")（red
alga）、[金藻](../Page/金藻.md "wikilink")（golden
alga）、矽藻及[溝鞭藻類](../Page/溝鞭藻類.md "wikilink")（dinoflagellate）\[10\]。部分有孔蟲門生物會[盜食質體](../Page/盜食質體.md "wikilink")（kleptoplasty），保留攝取藻類的[葉綠體去進行](../Page/葉綠體.md "wikilink")[光合作用](../Page/光合作用.md "wikilink")\[11\]。

### 繁殖方式

有孔蟲門生物的生命週期包括[雙倍體及](../Page/雙倍體.md "wikilink")[單倍體產生的交替](../Page/單倍體.md "wikilink")，雖然兩者形態上大部分相近。單倍體或產配子體（gamont）初期有一個[細胞核](../Page/細胞核.md "wikilink")，及後分裂成為很多[生殖細胞](../Page/生殖細胞.md "wikilink")，通常有兩個[鞭毛](../Page/鞭毛.md "wikilink")。雙倍體或裂殖體（schizont）為[多核細胞](../Page/多核細胞.md "wikilink")（multinucleate），經過[減數分裂後分裂製成新的產配子體](../Page/減數分裂.md "wikilink")。有性別的世代在水底進行多重[無性生殖並不罕見](../Page/無性生殖.md "wikilink")\[12\]
。

## 外殼

### 成分

[QuinqueloculinaDonegalBay.jpg](https://zh.wikipedia.org/wiki/File:QuinqueloculinaDonegalBay.jpg "fig:QuinqueloculinaDonegalBay.jpg")
外殼的形狀及成分是对有孔蟲門生物进行分类和鉴定的主要方法。大部分有孔虫们生物具有钙质（石灰质）外殼，由[碳酸鈣形成](../Page/碳酸鈣.md "wikilink")\[13\]。或为有機質，或由有机物或钙质成分粘接細小的[沉積物形成](../Page/沉積物.md "wikilink")，硅质外壳极为罕见。

### 組成沉積物

有孔虫門生物壳体的化石记录可以追溯至[寒武紀時期](../Page/寒武紀.md "wikilink")，现在的很多海洋沉積物也由它們組成。例如建造[金字塔的](../Page/金字塔.md "wikilink")[石灰岩便大部分由栖息于海底的有孔蟲門生物](../Page/石灰岩.md "wikilink")[貨幣蟲組成](../Page/貨幣蟲.md "wikilink")\[14\]。有估計指出生活于生物礁环境的有孔蟲門生物每年製造大約四千三百萬噸碳酸鈣，所以有孔蟲門生物為製造礁中的碳酸鈣的一個重要角色\[15\]。

### 特別的有孔蟲門生物

遺傳學研究證實裸變形虫Reticulomyxa及罕見的xenophyophore為沒有外殼的有孔蟲門生物。其他擁有網狀假足的阿米巴狀的生物與有孔蟲門生物被分類為[粒網蟲門](../Page/粒網蟲門.md "wikilink")（Granuloreticulosa），
但已不再被認為是一個自然組別，而現在會把其放在[絲足蟲類](../Page/絲足蟲類.md "wikilink")（Cercozoa）中\[16\]。

## 演化學上的重要性

### 化石的發現

死去的浮游有孔蟲門生物不斷大量地向海床落下，它們蘊含豐富礦物的外殼以化石的形態在沉積物中被保留著。在1960年代起，一方面在[深海鑽探計劃](../Page/深海鑽探計劃.md "wikilink")（Deep
Sea Drilling Program）、[海洋鑽探計劃](../Page/海洋鑽探計劃.md "wikilink")（Ocean
Drilling Program）及[綜合海洋鑽探計劃](../Page/綜合海洋鑽探計劃.md "wikilink")（Integrated
Ocean Drilling Program）的贊助，另一方面亦受到[石油探採](../Page/石油探採.md "wikilink")（oil
exploration）的目的所推動下，由先進的深海鑽探技術所帶出的沉積物核心中，可以發現幾百萬年前的有孔蟲門生物化石。

### 大量的高品質素材

在其實際上無限量供應的化石外殼及其核心中有相對高準確度的時期抑制原型下，有孔蟲門生物能夠製造出非常高品質的浮游有孔蟲門生物化石記錄，最早可以追溯至[侏羅紀中期](../Page/侏羅紀.md "wikilink")，其化石亦可以提供科學家一個不平衡的記錄去測試及記錄演化過程。因為化石記錄的品質極高，令物種間的相互關係可以以此為基礎，能夠造成極詳細的影像。在很多情況下其後會在現存的樣本上以分子基因研究的方法作獨立確認。

## 有孔蟲門生物的應用

### 地層測量

因為有孔蟲門生物的多樣性、數量充足及複雜的形態的關係，有孔蟲門生物的化石集合物對[生物地層學](../Page/生物地層學.md "wikilink")（biostratigraphy）十分有用，亦可以用作準確地測量出岩石的年代。[石油工業](../Page/石油工業.md "wikilink")（oil
industry）十分依賴[微化石](../Page/微化石.md "wikilink")（microfossil）如有孔蟲門生物去找尋潛在石油庫存。

### 古代環境的重建

石灰質的有孔蟲門生物化石由其生活的古代海洋元素組成。所以其化石對[古氣候學](../Page/古氣候學.md "wikilink")（paleoclimatology）及[古海洋學](../Page/古海洋學.md "wikilink")（paleoceanography）十分有用。她們一方面可以利用測定氧氣中[穩定同位素](../Page/穩定同位素.md "wikilink")（stable
isotope）的比例去重組古代氣候，另一方面也可以利用測定碳中[穩定同位素](../Page/穩定同位素.md "wikilink")（stable
isotope）的比例去重組[碳循環的歷史及古海洋生產力](../Page/碳循環.md "wikilink")\[17\]；參看[δ18O及](../Page/δ18O.md "wikilink")[δ13C](../Page/δ13C.md "wikilink")。浮游有孔蟲門生物化石記錄的地理模式亦可以用作重組古代[洋流情況](../Page/洋流.md "wikilink")。因為部分有孔蟲門生物只可以在特定環境中找到，它們可以用作找出每種古代海洋沉積物所沉積的環境。

### 氣候及環境的生物指示劑

基於對於生物地層學有價值的同樣原因，活有孔蟲門生物的集合物亦被利用作為海岸地區的[生物指示劑](../Page/生物指示劑.md "wikilink")（bioindicators），包括指示出[珊瑚礁的健康程度](../Page/珊瑚礁.md "wikilink")。因為碳酸鈣易於溶在酸性環境的影響，有孔蟲門生物可能特別受到氣候轉變及[海洋酸化](../Page/海洋酸化.md "wikilink")（ocean
acidification）的影響。

## 參考資料

<span style="display:none">\[18\]</span>

## 外部連結

  - [The University of California Museum of
    Paleontology](http://www.ucmp.berkeley.edu/index.html) website has
    an [Introduction to the
    Foraminifera](http://www.ucmp.berkeley.edu/foram/foramintro.html)
  - The [star\*sand
    project](https://web.archive.org/web/20060620040556/http://www.bowserlab.org/starsand.html)（part
    of
    [micro\*scope](https://web.archive.org/web/20080609213132/http://starcentral.mbl.edu/mv/portal.php?pagetitle=index)）is
    a cooperative database of information about foraminifera
  - [The Smithsonian Institution's National Museum of Natural
    History](https://web.archive.org/web/20080517091701/http://www.nmnh.si.edu/paleo/index.html)
    has the largest [type collection of
    foraminifera](https://web.archive.org/web/20070225110952/http://www.nmnh.si.edu/paleo/foram/)
    in the world
  - Researchers at the University of South Florida developed a system
    [using foraminifera for monitoring coral reef
    environments](http://www.marine.usf.edu/reefslab/foramcd/html_files/titlepage.htm)
  - University College London's [micropaleontology
    site](http://www.ucl.ac.uk/GeolSci/micropal/foram.html) has an
    overview of foraminifera, including many high-quality
    [SEMs](../Page/SEM.md "wikilink")
  - [Illustrated glossary of terms used in foraminiferal
    research](http://paleopolis.rediris.es/cg/CG2006_M02/index.html) is
    the Lukas Hottinger's glossary published in the OA e-journal
    ["Carnets de Géologie - Notebooks on
    Geology"](http://paleopolis.rediris.es/cg/uk-index.html)
  - [CHRONOS](https://web.archive.org/web/19981205122129/http://www.chronos.org/)
    has [several foraminifera
    resources](https://web.archive.org/web/20061115112221/http://portal.chronos.org/gridsphere/gridsphere?cid=res_foram)，including
    a [taxon search
    page](https://web.archive.org/web/20060512035435/http://portal.chronos.org/gridsphere/gridsphere?cid=res_taxondb)
    and a [micro-paleo
    section](http://portal.chronos.org/gridsphere/gridsphere?cid=micropaleo)
  - [**eForams**](https://web.archive.org/web/20071213034749/http://www.eforams.icsr.agh.edu.pl/index.php/Main_Page)
    is a web site focused on foraminifera and modeling of foraminiferal
    shells
  - [Benthic foraminifera
    information](http://ethomas.web.wesleyan.edu/BFhandout.htm) from the
    2005 Urbino Summer School of Paleoclimatology
  - [Information on
    Foraminifera](https://web.archive.org/web/20070810004729/http://www.paleontology.uni-bonn.de/frame02.htm)
    Martin Langer's Micropaleontology Page
  - [有孔虫画廊-插图目录](http://www.foraminifera.eu/indexcn.html) 与5000+影像

[F](../Category/有孔蟲界.md "wikilink") [\*](../Category/有孔蟲門.md "wikilink")
[Category:原生生物](../Category/原生生物.md "wikilink")

1.

2.

3.

4.

5.

6.
7.

8.
9.
10.
11.

12.
13.
14. [Foraminifera: History of
    Study](http://www.ucl.ac.uk/GeolSci/micropal/foram.html#histofstudy),
    [University College
    London](../Page/University_College_London.md "wikilink"), retrieved
    [20 September](../Page/20_September.md "wikilink") 2007

15.

16.

17.

18. <http://web.tcfsh.tc.edu.tw/natu/earth/geo/georoom/fossil/forams.htm>