**高齒羊**（*Merycoidodon*）是一屬史前的[偶蹄目](../Page/偶蹄目.md "wikilink")[哺乳動物](../Page/哺乳動物.md "wikilink")。高齒羊原先的學名為*Oreodon*，但是這個名稱是一類稱為*Orodus*的史前[鯊魚的](../Page/鯊魚.md "wikilink")[異名](../Page/異名.md "wikilink")，故此這個名稱並非有效的科學命名。

高齒羊屬於[岳齒獸科](../Page/岳齒獸科.md "wikilink")，與[駱駝有關](../Page/駱駝.md "wikilink")，且是[北美洲的](../Page/北美洲.md "wikilink")[原住民](../Page/原住民.md "wikilink")。高齒羊的祖先於[始新世出現](../Page/始新世.md "wikilink")，而其後裔則出現於[上新世](../Page/上新世.md "wikilink")，故此一般來說，高齒羊生存了整個[第三紀](../Page/第三紀.md "wikilink")。

高齒羊的身體很長，大小如[羊](../Page/羊.md "wikilink")，而四肢很短。牠們的前肢有五趾，但第一趾已退化，後肢則有四趾。牠們的[齒列在結構上像](../Page/齒列.md "wikilink")[鹿的](../Page/鹿.md "wikilink")，但卻有顯著的[犬齒](../Page/犬齒.md "wikilink")。

## 參考

  - Benes, Josef. Prehistoric Animals and Plants. Pg. 219. Prague:
    Artua, 1979.

[Category:岳齒獸科](../Category/岳齒獸科.md "wikilink")
[Category:始新世哺乳類](../Category/始新世哺乳類.md "wikilink")
[Category:漸新世哺乳類](../Category/漸新世哺乳類.md "wikilink")