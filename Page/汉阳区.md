[缩略图](https://zh.wikipedia.org/wiki/File:Hanyang_Guodingshan_Waste_to_Energy_Plant,_Wuhan,_China.JPG "fig:缩略图")\]\]
[缩略图](https://zh.wikipedia.org/wiki/File:Wuhan_Yangtze_River_Bridge_-_the_railway_level.jpg "fig:缩略图")
**汉阳区**，[湖北省](../Page/湖北省.md "wikilink")[武汉市的一个](../Page/武汉市.md "wikilink")[市辖区](../Page/市辖区.md "wikilink")。汉阳在武汉市区西南部。东隔[长江与](../Page/长江.md "wikilink")[武昌区相望](../Page/武昌区.md "wikilink")，有[武汉长江大桥和武汉长江三桥](../Page/武汉长江大桥.md "wikilink")（[白沙洲长江大桥](../Page/白沙洲长江大桥.md "wikilink")）相连；北隔[汉江与](../Page/汉江.md "wikilink")[汉口对峙](../Page/汉口.md "wikilink")，有[江汉桥和汉江铁路桥等多座汉江桥梁相连](../Page/江汉桥.md "wikilink")。区内地势低洼，有[月湖](../Page/月湖_\(武汉\).md "wikilink")、莲花湖等湖泊和[龜山](../Page/龜山_\(武漢\).md "wikilink")、凤凰山等山丘，全区人口47.8414万人。

## 历史

[西汉时属](../Page/西汉.md "wikilink")[沙羡县地](../Page/沙羡县.md "wikilink")。[东汉末年在鲁山](../Page/东汉.md "wikilink")（今龟山）北筑却月城。[三国时](../Page/三国.md "wikilink")[吴于鲁山西南筑鲁山城](../Page/东吴.md "wikilink")，为[江夏郡治](../Page/江夏郡.md "wikilink")。[隋初为汉津县](../Page/隋朝.md "wikilink")，后更名为[汉阳县](../Page/汉阳县.md "wikilink")。唐[武德四年](../Page/武德.md "wikilink")（621年
）在凤栖山南筑汉阳城。[五代周为](../Page/五代.md "wikilink")[汉阳军治](../Page/汉阳军.md "wikilink")，元、明、清为[汉阳府治](../Page/汉阳府.md "wikilink")。1912年汉阳县治
。1926年汉阳城区属[汉口市](../Page/汉口市.md "wikilink")。1929年属[武昌市](../Page/武昌市.md "wikilink")。1930年仍入汉阳县，后几经变动。1949年汉阳县城区与武昌市、汉口市合置武汉市。1952年改今名。

按照[中国传统的](../Page/中国.md "wikilink")[阴阳地名命名规则](../Page/阴阳.md "wikilink")，汉阳本应该是指“[汉水之北](../Page/汉水.md "wikilink")”。但在[明朝](../Page/明朝.md "wikilink")[成化年间](../Page/成化.md "wikilink")，汉江下游部分改道，使得汉阳这个地方成了汉水之南了。不过“汉阳”一词形成较早，人们的习惯形成之后，没有人因此而改称该地为“汉阴”，而是沿用了汉阳一名。

汉阳在[中国历史文化上](../Page/中国.md "wikilink")，最初是因为[钟子期和](../Page/钟子期.md "wikilink")[俞伯牙](../Page/俞伯牙.md "wikilink")[高山流水的知音故事而出名](../Page/高山流水.md "wikilink")。汉阳境内至今还有“古琴台”、“琴断口”等地名，其中“古琴台”（或简称“琴台”）是汉阳区乃至整个[武汉市的最重要的](../Page/武汉市.md "wikilink")[交通枢纽之一](../Page/交通.md "wikilink")。

[近代历史上](../Page/近代历史.md "wikilink")，汉阳因坐落该地的[汉阳铁厂和](../Page/汉阳铁厂.md "wikilink")[汉阳兵工厂而出名](../Page/汉阳兵工厂.md "wikilink")。汉阳兵工厂是[清朝末年著名](../Page/清朝.md "wikilink")[洋务运动领袖](../Page/洋务运动.md "wikilink")[张之洞创办的](../Page/张之洞.md "wikilink")。它生产的[步枪](../Page/步枪.md "wikilink")，人称“[汉阳造](../Page/汉阳造.md "wikilink")”，是当时中国最先进的枪械。汉阳造步枪直至[中华民国时期](../Page/中华民国.md "wikilink")，还在被广泛装备于各部队。

今天的汉阳，特别是沌口[经济技术开发区](../Page/经济技术开发区.md "wikilink")，集中了大量的制造业企业。这些企业绝大多数都是[汽车零部件或整车生产企业](../Page/汽车.md "wikilink")，它们使得该地区成为武汉市最重要的工业中心之一。

## 參見

  - [汉阳造](../Page/汉阳造.md "wikilink")

## 外部链接

  - [汉阳区政府官方网站](http://www.hanyang.gov.cn/)

<!-- end list -->

  - 地图

<!-- end list -->

  - [Google地图 - 汉阳区](https://www.google.co.uk/maps/place/Hanyang,+Wuhan)

[汉阳区](../Category/汉阳区.md "wikilink")
[Category:武汉市辖区](../Category/武汉市辖区.md "wikilink")