Tiến sĩ |chunom= |hantu= |hannom=黃甲{{\`}}進士 |vietnamesetext= |ryutitle=
|ryuhanji= |ryukana= |ryuromaji= |mongoltitle= |mongol= |kiril=
|monglatin= |manchutitle= |manchu= ᡩᠣᠰᡳᡴᠠᠰᡳ |mollendorff= dosikasi }}

**进士**原是[科舉的科目之一](../Page/科舉.md "wikilink")。中國古代科舉制度中，通過最後一級[中央政府](../Page/中央政府.md "wikilink")[朝廷考試者](../Page/朝廷.md "wikilink")，稱為進士。到了[元朝以後](../Page/元朝.md "wikilink")，惟存進士一科，遂成為科舉功名的最高等級。民间又称考中进士为“[金榜题名](../Page/金榜.md "wikilink")”。

## 历史

[隋煬帝於](../Page/隋煬帝.md "wikilink")[大業元年](../Page/大业_\(年号\).md "wikilink")（605年）首次開的進士科，為[科舉的開始](../Page/科舉.md "wikilink")。

[隋](../Page/隋朝.md "wikilink")、[唐時](../Page/唐朝.md "wikilink")，進士科和明經科都算是考試科別，[明經科考](../Page/明經.md "wikilink")[經學和時務策](../Page/經學.md "wikilink")。[進士科除考](../Page/進士科.md "wikilink")[經學和時務策以外](../Page/經學.md "wikilink")，還要“加考[詩](../Page/詩.md "wikilink")[賦](../Page/賦.md "wikilink")”。[武后為了與唐朝](../Page/武后.md "wikilink")[宗室](../Page/宗室.md "wikilink")、善考[明經的](../Page/明經.md "wikilink")[士族抗衡](../Page/士族.md "wikilink")，特別重視進士科的地位，以選拔賢才。[唐武宗下诏](../Page/唐武宗.md "wikilink")，凡进士及第者称为“衣冠户”，其家享受轻税免役之权，成为一种殊荣\[1\]。宋真宗时，尚有衣冠户之名\[2\]，後來由“官户”所取代。\[3\]

[唐朝](../Page/唐朝.md "wikilink")[科舉](../Page/科舉.md "wikilink")，[進士科最難](../Page/進士科.md "wikilink")，[明經科較易](../Page/明經科.md "wikilink")，唐人有諺云：“三十老明經，五十少進士。”\[4\]意思是說，三十歲考上[明經科](../Page/明經科.md "wikilink")，已算是年老，因為明經科較易，而五十歲登進士第，已經很年輕了。一般進士百人中取一二人，明經十人中取一二人\[5\]。[韓愈稱進士考試](../Page/韓愈.md "wikilink")，到最后錄取時，“有終身不得與者焉”，一旦得第時，“班白之老半焉”\[6\]。依據[清](../Page/清朝.md "wikilink")[徐松](../Page/徐松.md "wikilink")《登科記考》記載，[孟郊於](../Page/孟郊.md "wikilink")[貞元十二年](../Page/貞元.md "wikilink")（西元796年）登進士第，時四十六歲。從他的〈登科後〉一詩來看，登科的喜事幾乎把幾十年為仕途的努力和艱辛都一掃而空。
\[7\]

[進士科是常科](../Page/進士科.md "wikilink")，考取又最難，一般每次只取二、三十人\[8\]，僅是[明經科的十分之一](../Page/明經.md "wikilink")，故此最為尊貴，地位亦成為各科之首。時人稱[進士及第者为](../Page/進士.md "wikilink")「[白衣公卿](../Page/白衣公卿.md "wikilink")」\[9\]。

在唐朝，進士考試可以在考前公開推舉，稱作「[通榜](../Page/通榜.md "wikilink")」\[10\]。因此凡是應[進士舉的人](../Page/進士.md "wikilink")，常常將自己的作品送給朝中有文學聲望的人看，希望通過他們宣揚自己的名譽，甚至推薦給主考官，這就是「溫卷」。而二者之間的關係稱為「知己」。在[唐朝](../Page/唐朝.md "wikilink")，進士及第後，要到吏部進行[關試](../Page/關試.md "wikilink")，才能得到官職。

[宋代以前](../Page/宋代.md "wikilink")，進士只需要通過在[尚書省舉行的省試](../Page/尚書省.md "wikilink")。自宋以後，進士一律要經過由[皇帝主持的](../Page/皇帝.md "wikilink")「[殿試](../Page/殿試.md "wikilink")」一關覆核和決定名次。[宋仁宗時](../Page/宋仁宗.md "wikilink")，曾有一名考生[張元因通過省試](../Page/張元_\(西夏\).md "wikilink")，但在殿試被黜落，憤而投奔[西夏](../Page/西夏.md "wikilink")。自此以後殿試都只定名次，而不會黜落考生。

[宋朝的科舉制度](../Page/宋朝.md "wikilink")，一開始，也分為進士、明經、諸科等。直到[宋神宗時](../Page/宋神宗.md "wikilink")，[王安石變法改革](../Page/王安石變法.md "wikilink")，廢明經、諸科，只用進士一科取士。

[元朝正式僅存進士一科](../Page/元朝.md "wikilink")。明朝、清朝繼承，亦只有進士一科取士。自此，通過最後一級考試者，稱為**進士**。

在[明朝和](../Page/明朝.md "wikilink")[清朝](../Page/清朝.md "wikilink")，殿試封录取考生为三等称[三甲](../Page/三甲.md "wikilink")。一甲僅錄取三人，依次为[状元](../Page/状元.md "wikilink")、[榜眼](../Page/榜眼.md "wikilink")、[探花](../Page/探花.md "wikilink")，称**进士及第**，在於「史」的意味。二甲若干（清朝時一般為40或50人），称**进士出身**，在於“子”的意味。二甲第一名稱為**傳臚**，又稱**亞元**。三甲（[清朝时一般为](../Page/清朝.md "wikilink")100-300人）称**同进士出身**，在於“集”的意味。世人统称录取者为进士。

進士是功名的盡頭，就算是對名次不滿意，亦不可以重考。民間對各類名次的進士習慣皆以“[狀元郎](../Page/狀元.md "wikilink")”稱呼之。

## 日本

日本在[奈良時代模仿中國唐代制度](../Page/奈良時代.md "wikilink")，由[式部省實行的次於](../Page/式部省.md "wikilink")[秀才](../Page/秀才_\(日本\).md "wikilink")、[明經的第三等官吏考試制度](../Page/明經.md "wikilink")，亦稱**進士試**。
因為比秀才叙位低，問題又難，所以考進士的人並不多。[平安時代](../Page/平安時代.md "wikilink")[貞觀年間廢除進士制度](../Page/貞觀_\(日本\).md "wikilink")，後遂成為[漢文考試選拔](../Page/漢文.md "wikilink")[紀傳道学生](../Page/紀傳道.md "wikilink")[文章生的別称](../Page/文章生.md "wikilink")。

## 参考文献

## 参见

  - [科舉](../Page/科舉.md "wikilink")
  - [殿試](../Page/殿試.md "wikilink")
  - [狀元](../Page/狀元.md "wikilink")
  - [明朝進士列表](../Page/明朝進士列表.md "wikilink")
  - [清朝進士列表](../Page/清朝進士列表.md "wikilink")
  - [醫科進士](../Page/醫科進士.md "wikilink")
  - [举人](../Page/举人.md "wikilink")
  - [公车](../Page/公車_\(漢朝\).md "wikilink")

[Category:科举身份](../Category/科举身份.md "wikilink")
[进士](../Category/进士.md "wikilink")

1.  《[文苑英华](../Page/文苑英华.md "wikilink")》卷429《会昌五年正月三日南郊赦文》
2.  《续资治通鉴长编》卷95天禧四年(1020年)正月辛未
3.  《[宋史](../Page/宋史.md "wikilink")》卷377《王庠传》
4.  《[唐摭言](../Page/唐摭言.md "wikilink")》卷一《散序進士》：進士科“歲貢常不減八九百人”，“其艱難謂之‘三十老明經，五十少進士’”。
5.  《[通典](../Page/通典.md "wikilink")》卷15《選舉三》：“其進士大抵千人，得第者百一二；明經倍之，得第者十一二。”
6.  《全唐文》卷555，《贈張童子序》
7.  李建崑、邱燮友 等 著：〈[序](http://cls.hs.yzu.edu.tw/MPOEM/front1.htm)
    〉，《孟郊詩集校注》上下冊。[國立編譯館中華學術著作委員會](../Page/國立編譯館.md "wikilink")
    主編，新文豐出版有限公司 出版，1997年10月。
8.  傅璇琮《唐代科舉與文學．序》中指出：“唐代進士科所取的人數，前後期有所不同，但大致在三十人左右。”
9.  《[唐摭言](../Page/唐摭言.md "wikilink")》卷一，「散序[進士](../Page/進士.md "wikilink")」條
10. 《[唐摭言](../Page/唐摭言.md "wikilink")》卷八，「通榜」條