[Nasreddin.jpg](https://zh.wikipedia.org/wiki/File:Nasreddin.jpg "fig:Nasreddin.jpg")博物館。\]\]
**纳斯尔丁**（，，），阿拉伯世界称**朱哈**（），13世紀的[土耳其人](../Page/土耳其.md "wikilink")，是一位活跃在西起[摩洛哥](../Page/摩洛哥.md "wikilink")，东到[新疆](../Page/新疆.md "wikilink")[突厥诸民族中的传说人物](../Page/突厥.md "wikilink")，大智若愚，才辩超群。由于他才智过人，因此在不同地区的传说中往往以智者或者导师的头衔来称呼他，如那速列丁、纳斯尔丁·**[阿凡提](../Page/阿凡提_\(称号\).md "wikilink")**、纳斯尔丁·[霍加](../Page/霍加.md "wikilink")、[毛拉](../Page/毛拉.md "wikilink")·纳斯尔丁等。某些宗教史论，把他归入流行伊斯兰世界的‘[托钵僧团](../Page/托钵僧团.md "wikilink")’-
[达尔维斯](../Page/达尔维斯.md "wikilink")（波斯语Darvis，衍生出英语Dervish，藉此有译‘[德尔维希](../Page/德尔维希.md "wikilink")’）旋舞修会。

## 生平

是否确有其人，以及此人活动地区，生卒年月俱已不可考。据一般的推测，他可能是在11世纪到14世纪之中的某段时间活跃在[波斯或者](../Page/波斯.md "wikilink")[安纳托利亚](../Page/安纳托利亚.md "wikilink")。今日，在[土耳其的](../Page/土耳其.md "wikilink")[阿克謝希爾城](../Page/阿克謝希爾.md "wikilink")，还有一座陵墓据称是纳西鲁丁之墓。同时，在[乌兹别克斯坦的](../Page/乌兹别克斯坦.md "wikilink")[布哈拉](../Page/布哈拉.md "wikilink")，也有一座他的雕塑，形象是倒骑毛[驴的中年男子](../Page/驴.md "wikilink")。在[巴库和](../Page/巴库.md "wikilink")[大不里士](../Page/大不里士.md "wikilink")，据说也发现了以他的名字撰写的游记。

## 故事

许多纳西鲁丁的故事都富有深意，类似于[禅宗的](../Page/禅宗.md "wikilink")[公案](../Page/公案.md "wikilink")，因此[伊斯兰教中的神秘主义](../Page/伊斯兰教.md "wikilink")[苏菲教派经常使用纳西鲁丁的故事作为案例宣讲教义](../Page/苏菲教派.md "wikilink")。另外，亦有不少故事內容帶有低下階層向貴族或宗教領袖如[巴依](../Page/巴依.md "wikilink")、[阿訇挑戰的意味](../Page/阿訇.md "wikilink")。

纳赛尔丁的故事在整个中东都很闻名，并且对世界文明都产生了影响。从表面上看，大部分纳赛尔丁的故事都是笑话或者幽默轶事。它们在亚洲的茶馆和商队旅馆中被无止尽的口口相传。但其固有之处在于，纳赛尔丁的故事可以有很多层次的不同理解。笑话中伴随着道德和一点潜在的神秘意识从而得以实现。

在[金庸的長篇小說](../Page/金庸.md "wikilink")《[書劍恩仇錄](../Page/書劍恩仇錄.md "wikilink")》中，亦有一小段講述[陳家洛到了回部](../Page/陳家洛_\(書劍恩仇錄\).md "wikilink")，遇到了阿凡提的故事情節。

## 相關作品

  - 木偶剧《[阿凡提的故事](../Page/阿凡提的故事.md "wikilink")》
  - 小说《[发现阿凡提](../Page/发现阿凡提.md "wikilink")》（上/下）：（前苏联）[索洛维耶夫·列昂尼德·瓦西里耶维奇著](../Page/索洛维耶夫·列昂尼德·瓦西里耶维奇.md "wikilink").
    新疆青少年出版社
  - 音樂《[阿凡提的傳說](../Page/阿凡提的傳說.md "wikilink")》雙千斤[板胡與](../Page/板胡.md "wikilink")[巴拉曼](../Page/巴拉曼.md "wikilink")，[士心](../Page/士心.md "wikilink")、[劉湘作曲](../Page/劉湘.md "wikilink")

[N](../Category/傳說人物.md "wikilink") [N](../Category/口頭傳統.md "wikilink")
[N](../Category/神話傳說中的詭計者.md "wikilink")