**龎均**（），生於中國[上海](../Page/上海.md "wikilink")，祖籍江蘇常熟，著名畫家，專長油畫。曾任[香港中文大學校外進修部暨](../Page/香港中文大學.md "wikilink")[香港浸會學院音樂美術系](../Page/香港浸會學院.md "wikilink")[美術](../Page/美術.md "wikilink")[教師](../Page/教師.md "wikilink")，現任[國立臺灣藝術大學視覺傳達設計系](../Page/國立臺灣藝術大學.md "wikilink")[教授](../Page/教授.md "wikilink")。

## 生平

其父[龎薰琹](../Page/龎薰琹.md "wikilink")，是水墨畫家，曾留學法國，回國後曾擔任[杭州藝專教務長](../Page/杭州藝專.md "wikilink")、[中央美術學院實用美術系主任](../Page/中央美術學院.md "wikilink")。中華人民共和國建國後，曾受[周恩來總理之托](../Page/周恩來.md "wikilink")，籌辦[中央工藝美院並擔任第一副院長](../Page/中央工藝美院.md "wikilink")。其母[丘堤](../Page/丘堤.md "wikilink")，中國第一代油畫家之一，曾留學日本東京。

龎均13歲時考取杭州藝專，三年後考取[北京](../Page/北京.md "wikilink")[中央美術學院並在此畢業](../Page/中央美術學院.md "wikilink")，是[徐悲鴻的](../Page/徐悲鴻.md "wikilink")「末代入室[弟子](../Page/徒弟.md "wikilink")」。1980年，舉家移居香港。1987年移居台灣。

龎均喜愛[寫生](../Page/寫生.md "wikilink")。他於2008年3月在香港不同地方寫生時，7日內在3處地方被該地的管理人員驅趕，使他對香港的印象大打折扣。其中一次他在[中環至半山自動扶梯系統寫生時](../Page/中環至半山自動扶梯系統.md "wikilink")，被管理人員以「未經事先申請在此寫生」為理由要求立即離開，龎均不肯，結果要出動警員到來調停。\[1\]

## 家庭

其妻[籍虹](../Page/籍虹.md "wikilink")，為油畫家，為中華民國立法委員[張子揚之女](../Page/張子揚.md "wikilink")。其女[龎銚是台灣藝術家](../Page/龎銚.md "wikilink")，[旅遊生活頻道節目](../Page/旅遊生活頻道.md "wikilink")《[龎銚敲敲門](../Page/龎銚敲敲門.md "wikilink")》的主持人。

## 著作

  - 《龎均 Pang Jiun 70》ISBN 9867034112

## 參見

  - [中國油畫](../Page/中國油畫.md "wikilink")

## 參考資料

<div class="references-small">

<references/>

</div>

## 外部連結

  - [龎均的官方簡歷](https://web.archive.org/web/20080313080036/http://www.arttime.com.tw/exhibition/gallery/pangj/pangj_4.htm)
  - [書籍作品《龎均 Pang
    Jiun 70》](http://www.readingtimes.com.tw/ReadingTimes/ProductPage.aspx?gp=productdetail&cid=mcec\(SellItems\)&id=YT1205)
  - [定居台灣的油畫家龎](http://tw.people.com.cn/BIG5/14814/4735309.html)
  - [龎均油畫藝術](https://web.archive.org/web/20080509130402/http://www.yangallery.com/Paintings/paintings.htm)

[龐](../Category/中国画家.md "wikilink")
[Category:龎姓](../Category/龎姓.md "wikilink")
[Category:中央美术学院校友](../Category/中央美术学院校友.md "wikilink")
[Category:上海人](../Category/上海人.md "wikilink")
[Category:國立臺灣藝術大學教授](../Category/國立臺灣藝術大學教授.md "wikilink")

1.  [《鬧市寫生名畫家三被趕 指走遍世界首被刁難
    「為香港感丟臉」》](http://hk.news.yahoo.com/080330/12/2rhhq.html)，香港《明報》，2008年3月31日。