**RISINGPRODUCTION**（），簡稱**RISINGPRO**，是[日本一家大型](../Page/日本.md "wikilink")[演藝](../Page/演藝.md "wikilink")[經紀公司](../Page/經紀公司.md "wikilink")，旗下擁有眾多[歌舞系的歌手與音樂組合](../Page/歌舞系.md "wikilink")。

## 公司简介

## 所属艺人

### 歌手·音乐家

  -
  - [MAX](../Page/MAX_\(日本音樂團體\).md "wikilink")（、、、）

  - [岛袋宽子](../Page/岛袋宽子.md "wikilink")<small>（）</small>

  - [今井绘理子](../Page/今井绘理子.md "wikilink")<small>（）</small>

  - [上原多香子](../Page/上原多香子.md "wikilink")

  - [DA
    PUMP](../Page/DA_PUMP.md "wikilink")（[ISSA](../Page/ISSA_日本歌手\).md "wikilink")、DAICHI、、TOMO、KIMI、YORI、U-YEAH）

  - [三浦大知](../Page/三浦大知.md "wikilink")

  - [w-inds.](../Page/w-inds..md "wikilink")（[千叶凉平](../Page/千叶凉平.md "wikilink")、[橘庆太](../Page/橘庆太.md "wikilink")<small>（）</small>、[绪方龙一](../Page/绪方龙一.md "wikilink")）

  - [Lead](../Page/Lead.md "wikilink")（[谷内伸也](../Page/谷内伸也.md "wikilink")、[古屋敬多](../Page/古屋敬多.md "wikilink")、[键本辉](../Page/键本辉.md "wikilink")）

  - [Fairies](../Page/Fairies.md "wikilink")（井上理香子、野元空、[伊藤萌萌香](../Page/伊藤萌萌香.md "wikilink")、、）

  - （SHUNKUN、MAKOTO、HAYATO、FUNKY、KAMI）

  - [新里宏太](../Page/新里宏太.md "wikilink")

  - （[Ryuichi](../Page/绪方龙一.md "wikilink")、、Ryuki）

  - （泷泽彩夏、辰巳早矢香、野村舞铃）

  - [ERIHIRO](../Page/ERIHIRO.md "wikilink")（[ERI](../Page/今井绘理子.md "wikilink")、[HIRO](../Page/岛袋宽子.md "wikilink")）

  - （、田村优太、、都筑聪二）

### 演员·模特·谐星

  - [觀月亞里莎](../Page/觀月亞里莎.md "wikilink")
  - [西內瑪麗亞](../Page/西內瑪麗亞.md "wikilink")

## PALETTE移籍艺人

## 曾属艺人

[安室奈美惠](../Page/安室奈美惠.md "wikilink")

## 旗下音乐厂牌

  - [SONIC GROOVE](../Page/SONIC_GROOVE.md "wikilink")

  -
## VISION FACTORY精选合集

## UNITED～RISING DANCE FESTIVAL～

UNITED vol.1 〜RISING DANCE FESTIVAL〜（2010年4月4日） - DA
PUMP、千葉涼平、緒方龍一、Lead、三浦大知、Blue Tokyo

UNITED vol.2 〜RISING DANCE FESTIVAL〜（2010年6月5日） - DA
PUMP、w-inds.、Lead、三浦大知、延山信弘、Blue Tokyo

UNITED vol.3 〜RISING DANCE FESTIVAL〜（2010年9月23日） - DA
PUMP、ISSA×SoulJa、w-inds.、Lead、三浦大知、延山信弘、Blue Tokyo

UNITED vol.4 〜RISING DANCE FESTIVAL〜（2011年2月13日） - DA
PUMP、w-inds.、Lead、三浦大知、延山信弘、Blue Tokyo

UNITED vol.5 〜RISING DANCE FESTIVAL〜（2011年6月10日・11日） - DA
PUMP、w-inds.、Lead、三浦大知、延山信弘、Blue Tokyo

UNITED vol.6 〜RISING DANCE FESTIVAL〜（2011年10月30日） - DA
PUMP、w-inds.、Lead、三浦大知、延山信弘、Blue Tokyo

## Rising Studio

## 面试

## RISING舞蹈学校

## VISION CAST

## 自制节目

  -
## 关联公司

  - ——RISINGPRO旗下代理非音乐门类艺人的经纪公司。2016年自主终止营业。

## 参考资料

### 脚注

<references group="注" />

### 出处

## 外部链接

  - [RISINGPRO公司官方网站](http://www.rising-pro.jp)

  - [OPEN CAST｜RISINGPRO官方视频站](http://www.opencast.jp)

  -
  -
[\*](../Category/RISINGPRO.md "wikilink")
[R](../Category/日本藝人經紀公司.md "wikilink")
[R](../Category/日本音樂事業者協會.md "wikilink")
[R](../Category/港區公司_\(東京都\).md "wikilink")
[R](../Category/1985年成立的公司.md "wikilink")