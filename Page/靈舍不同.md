《**靈舍不同**》（****）是[香港](../Page/香港.md "wikilink")[亞洲電視](../Page/亞洲電視.md "wikilink")[本港台的黑色處境](../Page/本港台.md "wikilink")[喜劇](../Page/喜劇.md "wikilink")，並以[模擬立體效果播映](../Page/模擬立體.md "wikilink")，由[陳展鵬](../Page/陳展鵬.md "wikilink")、[小雪](../Page/小雪_\(歌手\).md "wikilink")、[樓南光](../Page/樓南光.md "wikilink")、[樊亦敏主演](../Page/樊亦敏.md "wikilink")，於2007年11月5日至2008年1月20日期間首播，2008年1月6日起，改至逢星期日播映，並由半小時增加至一小時為一集。此劇為全新排陣的[亞洲電視後第二套自製劇](../Page/亞洲電視.md "wikilink")。

此劇同時為[陳展鵬於亞視的告別作](../Page/陳展鵬.md "wikilink")。

## 故事簡介

故事講述於在[九龍區一幢有四十多年歷史](../Page/九龍.md "wikilink")、還有三個月便清拆的[公屋內](../Page/公屋.md "wikilink")[靈異的東西](../Page/靈異.md "wikilink")，鬧出不少趣事。美琦([小
雪飾](../Page/小雪_\(歌手\).md "wikilink"))為一[失明人士](../Page/失明.md "wikilink")，鄰居劉志鵬([陳展鵬飾](../Page/陳展鵬.md "wikilink"))對她有好感，但其已死母親忠嫂([吳浣儀飾](../Page/吳浣儀.md "wikilink"))反對……

[公屋內的人與鬼](../Page/公屋.md "wikilink")，從誤會到瞭解，從對立到互相幫助，解決了彼此的困難與心結，學懂了「愛與包容」，捍衛[屋邨](../Page/屋邨.md "wikilink")，反對清拆。

## 演員表

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係/暱稱</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳展鵬.md" title="wikilink">陳展鵬</a></p></td>
<td><p>劉志鵬</p></td>
<td><p><a href="../Page/輕型貨車.md" title="wikilink">輕型貨車司機</a><br />
劉野之子<br />
COCO異父異母的<a href="../Page/哥哥.md" title="wikilink">哥哥</a><br />
余美仁之<a href="../Page/孫.md" title="wikilink">孫</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/小雪_(歌手).md" title="wikilink">小 雪</a></p></td>
<td><p>美琦</p></td>
<td><p><a href="../Page/鋼琴.md" title="wikilink">鋼琴老師</a><br />
忠嫂之<a href="../Page/女.md" title="wikilink">女</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/樓南光.md" title="wikilink">樓南光</a></p></td>
<td><p>劉野</p></td>
<td><p>余美仁之子<br />
劉志鵬的<a href="../Page/父親.md" title="wikilink">父親</a><br />
COCO的<a href="../Page/養父.md" title="wikilink">養父</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/樊亦敏.md" title="wikilink">樊亦敏</a></p></td>
<td><p>神鳳</p></td>
<td><p>靈媒</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/朱健鈞.md" title="wikilink">朱健鈞</a></p></td>
<td><p>溫文濤</p></td>
<td><p><a href="../Page/區議員.md" title="wikilink">區議員</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/戚黛黛.md" title="wikilink">戚黛黛</a></p></td>
<td><p>余美仁</p></td>
<td><p>女鬼<br />
劉野之母<br />
劉志鵬的<a href="../Page/祖母.md" title="wikilink">祖母</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/吳浣儀.md" title="wikilink">吳浣儀</a></p></td>
<td><p>忠嫂</p></td>
<td><p>女鬼<br />
美琦之母</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黃璦瑤.md" title="wikilink">黃璦瑤</a></p></td>
<td><p>雲影嫦</p></td>
<td><p>女鬼<br />
曾為高級<a href="../Page/醫生.md" title="wikilink">醫生</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/李麗霞.md" title="wikilink">李麗霞</a></p></td>
<td><p>小白光</p></td>
<td><p>女鬼<br />
鬼界雜藝團的台柱<br />
嗜好<a href="../Page/唱歌.md" title="wikilink">唱歌</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/尹光.md" title="wikilink">尹 光</a></p></td>
<td><p>尹光明</p></td>
<td><p>鬼王</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/威利.md" title="wikilink">威 利</a></p></td>
<td><p>李萬基</p></td>
<td><p><a href="../Page/地產.md" title="wikilink">地產富商</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/周子濠.md" title="wikilink">周子濠</a></p></td>
<td><p>濠仔</p></td>
<td><p>李萬基手下</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/姜皓文.md" title="wikilink">姜皓文</a></p></td>
<td><p>黑仔</p></td>
<td><p>退休<a href="../Page/消防員.md" title="wikilink">消防員</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/譚芷翎.md" title="wikilink">譚芷翎</a></p></td>
<td><p>COCO</p></td>
<td><p><a href="../Page/餐廰.md" title="wikilink">餐廰侍應</a><br />
劉野的<a href="../Page/養女.md" title="wikilink">養女</a><br />
劉志鵬異父異母的<a href="../Page/妹妹.md" title="wikilink">妹妹</a></p></td>
</tr>
</tbody>
</table>

## 歌曲

**主題曲** 《心有靈犀》

  - 主唱：[陳展鵬](../Page/陳展鵬.md "wikilink")、[小
    雪](../Page/小雪_\(歌手\).md "wikilink")
  - 作曲：徐家霖
  - 填詞：張美賢
  - 歌曲提供:[環星娛樂](../Page/環星娛樂.md "wikilink")

**插曲** 《越愛越勇》

  - 主唱：[陳展鵬](../Page/陳展鵬.md "wikilink")、[小
    雪](../Page/小雪_\(歌手\).md "wikilink")
  - 作曲：徐家霖
  - 填詞：[喬星](../Page/喬星.md "wikilink")
  - 歌曲提供:[環星娛樂](../Page/環星娛樂.md "wikilink")

## 收視

以下為該劇於[亞洲電視首播之收視紀錄](../Page/亞洲電視.md "wikilink")(05/11/2007-28/12/2007)**星期一至五**：

| 週次 | 集數    | 日期                 | 平均收視                            |
| -- | ----- | ------------------ | ------------------------------- |
| 1  | 01-05 | 2007年11月5日-11月9日   | 3[點](../Page/收視點.md "wikilink") |
| 2  | 06-10 | 2007年11月12日-11月16日 | 3[點](../Page/收視點.md "wikilink") |
| 3  | 11-15 | 2007年11月19日-11月23日 | 3[點](../Page/收視點.md "wikilink") |
| 4  | 16-20 | 2007年11月26日-11月30日 | 3[點](../Page/收視點.md "wikilink") |
| 5  | 21-25 | 2007年12月3日-12月7日   | 3[點](../Page/收視點.md "wikilink") |
| 6  | 26-30 | 2007年12月10日-12月14日 | 3[點](../Page/收視點.md "wikilink") |
| 7  | 31-35 | 2007年12月17日-12月21日 | 3[點](../Page/收視點.md "wikilink") |
| 8  | 36-40 | 2007年12月24日-12月28日 | 3[點](../Page/收視點.md "wikilink") |

以下為該劇於[亞洲電視首播之收視紀錄](../Page/亞洲電視.md "wikilink")(06/01/2008-)**星期日**：

| 週次 | 集數 | 日期         | 平均收視                            |
| -- | -- | ---------- | ------------------------------- |
| 9  | 41 | 2008年1月6日  | 4[點](../Page/收視點.md "wikilink") |
| 10 | 42 | 2008年1月13日 | 4[點](../Page/收視點.md "wikilink") |
| 11 | 43 | 2008年1月20日 | 3[點](../Page/收視點.md "wikilink") |

資料來源：CSM媒介研究(一個[收視點代表](../Page/收視點.md "wikilink")64,820名觀眾)

## 外部連結

  - [亞洲電視
    -靈舍不同](https://web.archive.org/web/20080709032229/http://www.hkatv.com/v3/drama/07/hkghostbusters/)

|align=center colspan=3|[天之驕子](../Page/天之驕子.md "wikilink")
\-11月16日 |align=center colspan=4|[繡娘蘭馨](../Page/繡娘蘭馨.md "wikilink")
11月19日-12月29日 |- |align=center|**上一套：**
[激流青春](../Page/激流青春.md "wikilink")
\-11月2日 |align=center
colspan="5"|**[本港台第二線劇集](../Page/本港台電視劇集列表_\(2007年\)#第二線劇集.md "wikilink")**
**靈舍不同**
11月5日-12月28日
21:00-21:30 |align=center|**下一套：**
[總有艷陽天](../Page/總有艷陽天.md "wikilink")
1月1日-5月6日
20:00-21:00 |- |align=center
colspan=3|[詠春](../Page/詠春_\(電視劇\).md "wikilink")
\-11月30日 |align=center colspan=2|[功夫狀元](../Page/功夫狀元.md "wikilink")
12月3日-12月14日 |align=center
colspan=2|[霍元甲1981精武版](../Page/霍元甲1981精武版.md "wikilink")
12月17日-12月28日

[Category:香港電視情景喜劇](../Category/香港電視情景喜劇.md "wikilink")
[Category:2007年亞視電視劇集](../Category/2007年亞視電視劇集.md "wikilink")
[Category:2008年亞視電視劇集](../Category/2008年亞視電視劇集.md "wikilink")
[Category:九龍背景電視劇](../Category/九龍背景電視劇.md "wikilink")
[Category:公寓背景電視劇](../Category/公寓背景電視劇.md "wikilink")
[Category:鬼題材電視劇](../Category/鬼題材電視劇.md "wikilink")