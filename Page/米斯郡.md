**米斯郡** （County Meath，[愛爾蘭語](../Page/愛爾蘭語.md "wikilink")：Contae na
Mí），俗稱**The Royal
County**，是[愛爾蘭的一郡](../Page/愛爾蘭共和國.md "wikilink")，歷史上屬[倫斯特省](../Page/倫斯特省.md "wikilink")。位於[愛爾蘭島東部](../Page/愛爾蘭島.md "wikilink")，海岸線長10公里。面積2,342
km²，2006年人口162,831人。首府[納文](../Page/納文.md "wikilink")。

被列入世界遺產的[博因河谷位於本郡](../Page/博因河谷.md "wikilink")。

## 姐妹城市

  - [卡瑞](../Page/卡瑞_\(北卡羅萊納州\).md "wikilink")

  - [贵阳](../Page/贵阳.md "wikilink")

## 參考文獻

## 外部連結

  - [Meath County Council](http://www.meath.ie/)
  - [Meath Tourism](http://www.meathtourism.ie/)
  - [Meath Event Guide](http://www.meg.ie/)
  - [CSO Website](http://www.cso.ie/census/)
  - [Cultur - Celebrating Diversity Website](http://www.cultur.ie)
  - [County Meath History Geography map
    guide](https://web.archive.org/web/20051214053622/http://meath.travelinireland.com/)
  - [Navan
    Hurling.com](https://web.archive.org/web/20090808062715/http://navanhurling.com/)
  - [Gaelscoil
    stats](http://www.gaelscoileanna.ie/assets/Staitistic%C3%AD-2010-2011_Gaeilge.pdf)
  - [Gaeltacht Comprehensive Language
    Study 2007](https://web.archive.org/web/20140912104226/http://www.ahg.gov.ie/ie/Straiteis20BliaindonGhaeilge/Foilseachain/Staid%C3%A9ar%20Cuimsitheach%20Teangeola%C3%ADoch%20ar%20%C3%9As%C3%A1id%20na%20Gaeilge%20sa%20Ghaeltacht%20%28achoimre%29.pdf)
  - [The Harp That Once Through Tara's Hall (Sara Banleigh at Lincoln
    Center)](http://www.youtube.com/watch?v=X-oERF0CqtM)

[\*](../Category/米斯郡.md "wikilink")
[Category:倫斯特省](../Category/倫斯特省.md "wikilink")
[Category:愛爾蘭共和國的郡](../Category/愛爾蘭共和國的郡.md "wikilink")