[Arthur_W_Hummel_Jr.jpg](https://zh.wikipedia.org/wiki/File:Arthur_W_Hummel_Jr.jpg "fig:Arthur_W_Hummel_Jr.jpg")
**恒安石**（**Arthur W. Hummel
Jr.**，），[美国著名](../Page/美国.md "wikilink")[外交官](../Page/外交官.md "wikilink")。

## 生平

恒安石出生在[中国](../Page/中国.md "wikilink")[山西省](../Page/山西省.md "wikilink")[汾阳](../Page/汾阳.md "wikilink")，父亲[恒慕义是一位](../Page/恒慕义.md "wikilink")[公理会](../Page/公理会.md "wikilink")[传教士](../Page/传教士.md "wikilink")，也是一位[汉学家](../Page/汉学家.md "wikilink")，担任汾阳[铭义中学的校长](../Page/铭义中学.md "wikilink")。1923年，恒安石随父移居[北京](../Page/北京.md "wikilink")。1928年，恒慕义应聘担任美国国会图书馆东亚部主任，于是恒安石一家返回[华盛顿](../Page/华盛顿哥伦比亚特区.md "wikilink")。1940年，恒安石大学没有毕业，就回到北平，在[辅仁大学附中担任英文教师](../Page/輔仁中學.md "wikilink")。不久[太平洋战争爆发](../Page/太平洋战争.md "wikilink")，恒安石与在华北的英美籍人士一同被日军拘捕，关押在山东潍坊[乐道院集中营](../Page/乐道院.md "wikilink")，1944年6月9日夜，恒安石在地方抗日部队的帮助下成功越狱。二战结束以后，恒安石回到北平，参加联合国救济总署的工作，1949年回到美国。

恒安石在[芝加哥大学获得中国研究](../Page/芝加哥大学.md "wikilink")[硕士学位后](../Page/硕士.md "wikilink")，1950年开始在[美国国务院工作](../Page/美国国务院.md "wikilink")。1960年代曾於[台北的](../Page/台北.md "wikilink")[美國駐華大使館擔任副館長](../Page/美國駐華大使館.md "wikilink")（）。1968年，首次担任[美国大使出使](../Page/美国.md "wikilink")[缅甸](../Page/缅甸.md "wikilink")。1971年回到[华盛顿](../Page/华盛顿哥伦比亚特区.md "wikilink")，担任[美国国务卿](../Page/美国国务卿.md "wikilink")[基辛格的高级顾问](../Page/基辛格.md "wikilink")。1975年担任美国驻[埃塞俄比亚大使](../Page/埃塞俄比亚.md "wikilink")。卸任回国后出任[美國東亞暨太平洋事務助理國務卿](../Page/美國東亞暨太平洋事務助理國務卿.md "wikilink")，後來还曾担任美国驻[巴基斯坦大使](../Page/巴基斯坦.md "wikilink")。

1981年[里根当选](../Page/里根.md "wikilink")[总统以后](../Page/美国总统.md "wikilink")，任命恒安石出任[美国驻华大使](../Page/美国驻华大使.md "wikilink")。任内两国达成了《[八一七公报](../Page/八一七公报.md "wikilink")》。从[美国国务院退休后](../Page/美国国务院.md "wikilink")，曾任[南京大学](../Page/南京大学.md "wikilink")-[霍普金斯大学](../Page/霍普金斯大学.md "wikilink")[中美文化研究中心主任](../Page/中美文化研究中心.md "wikilink")。

2001年2月6日，恆安石先生在[马里兰州去世](../Page/马里兰州.md "wikilink")，享年80歲。他生前具有深厚的中國文化修養和豐富的個人珍貴圖書收藏，其英文部分藏書由[哈佛大學購買收藏](../Page/哈佛大學.md "wikilink")，中文圖書最重要部分包括[晚清和](../Page/晚清.md "wikilink")[民國時期收藏保存在](../Page/民國.md "wikilink")[美國亞洲文化學院文庫](../Page/美國亞洲文化學院.md "wikilink")－－[崇德宣印堂](../Page/崇德宣印堂.md "wikilink")，其他檔案及晚期圖書分佈在[美國國務院和其母校](../Page/美國國務院.md "wikilink")[芝加哥大學](../Page/芝加哥大學.md "wikilink")。
2012年5月29日[美國亞洲文化學院校董會主席](../Page/美國亞洲文化學院.md "wikilink")[趙曉明代表學院將該院收藏的全部恆安石中文藏書捐贈給中國](../Page/趙曉明.md "wikilink")[浙江大學檔案館](../Page/浙江大學.md "wikilink")。該批藏書是研究恆安石先生一生和伴隨其一生中國波瀾壯闊的百年史以及[中美關係發展史的重要文](../Page/中美關係.md "wikilink")​​獻。

[Category:美國駐中華人民共和國大使](../Category/美國駐中華人民共和國大使.md "wikilink")
[Category:美國駐巴基斯坦大使](../Category/美國駐巴基斯坦大使.md "wikilink")
[Category:美國駐衣索匹亞大使](../Category/美國駐衣索匹亞大使.md "wikilink")
[Category:美國駐緬甸大使](../Category/美國駐緬甸大使.md "wikilink")
[Category:美国外交官](../Category/美国外交官.md "wikilink")
[Category:芝加哥大學校友](../Category/芝加哥大學校友.md "wikilink")
[Category:约翰霍普金斯大学教师](../Category/约翰霍普金斯大学教师.md "wikilink")
[Category:在台灣的美國人](../Category/在台灣的美國人.md "wikilink")
[Category:在华美国传教士子女](../Category/在华美国传教士子女.md "wikilink")
[Category:汾陽人](../Category/汾陽人.md "wikilink")