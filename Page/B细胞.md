[Blausen_0624_Lymphocyte_B_cell.png](https://zh.wikipedia.org/wiki/File:Blausen_0624_Lymphocyte_B_cell.png "fig:Blausen_0624_Lymphocyte_B_cell.png")
**B细胞**（B淋巴球）有時稱之為「朝囊定位細胞」（bursa oriented
cells），這是因為它們首次在雞的[腔上囊](../Page/腔上囊.md "wikilink")（Bursa
of Fabricius）被提及的關係\[1\]。

在[腸道的](../Page/腸道.md "wikilink")[派亞氏腺體](../Page/派亞氏腺體.md "wikilink")（Peyer's
glands）中的[淋巴組織](../Page/淋巴組織.md "wikilink")，被認為具有與鳥類的Fabricius組織中的[鳥囊](../Page/腔上囊.md "wikilink")（avian
bursa）同樣的功能。在魚類，它們可能就是那位於腸中的[淋巴樣組織](../Page/淋巴樣組織.md "wikilink")，因為口服疫苗時，會刺激魚血液中產生相對應的[抗體蛋白](../Page/抗體蛋白.md "wikilink")。

它是一种在[骨髓中成熟的细胞](../Page/骨髓.md "wikilink")，在體液[免疫中產生](../Page/免疫.md "wikilink")[抗體](../Page/抗體.md "wikilink")，起到重要作用。當遇到[抗原時](../Page/抗原.md "wikilink")，會分化成核比例較大的[大淋巴球](../Page/大淋巴球.md "wikilink")，叫[漿細胞](../Page/漿細胞.md "wikilink")。漿細胞的細胞質中且會出現一些顆粒，這些顆粒容易被甲基藍等天青染料所染色，同時會出現抗體，表現在[細胞膜或釋放出去](../Page/細胞膜.md "wikilink")。另一部分B细胞经过抗原激活后并不成为浆细胞，而是成为[记忆B细胞](../Page/记忆B细胞.md "wikilink")。当再次遇到相同抗原时，记忆B细胞能迅速做出反应，大量分化增殖。

## 词源

B细胞中的这个缩写B来自于鸟类B细胞分化成熟的器官——[法氏囊](../Page/腔上囊.md "wikilink")（Bursa of
Fabricius）\[2\]。而在大部分哺乳动物中，B细胞形成于骨髓（bone
marrow），所以B细胞一词被沿用下来，尽管其它由多能干细胞分化而来的血细胞也是在骨髓中成熟的\[3\]。

## 参见

  - [T细胞](../Page/T细胞.md "wikilink")

## 参考文献

[Category:淋巴细胞](../Category/淋巴细胞.md "wikilink")

1.  [Bursa of
    Fabricius](http://www.nlm.nih.gov/cgi/mesh/2007/MB_cgi?mode=&term=Bursa+of+Fabricius&field=entry)

2.
3.  <http://www.albrecht-kossel-institut.de/en/4_publications/2006/Mix_J_Neurol_2006.pdf>
    page V/12 top-left column