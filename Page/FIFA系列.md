《FIFA》是[足球类](../Page/association_football.md "wikilink")[电子游戏的一种](../Page/video_game.md "wikilink")，该游戏类型又称“足球模拟器”,
“FIFA”每年由[艺电（EA）旗下的](../Page/Electronic_Arts.md "wikilink")[艺电体育（EA
Sports）部门发布](../Page/EA_Sports.md "wikilink")。*[Sensible
Soccer](../Page/Sensible_Soccer.md "wikilink")*，*[Kick
Off](../Page/Kick_Off_\(series\).md "wikilink")*，*[Match
Day](../Page/Match_Day_\(series\).md "wikilink")*
是这类游戏的开山之作。足球类电子游戏[从八十年代末开始发展](../Page/1980s_in_video_gaming.md "wikilink")，到[艺电（EA）宣布足球游戏作为艺电体育](../Page/EA_Sports.md "wikilink")（EA
Sports）品牌的下一个新成员的时候，足球类电子游戏已经在游戏市场上具有竞争力。

值得注意的是，这个系列在1993年末就得到了[FIFA的官方许可](../Page/FIFA.md "wikilink")。[FIFA
International
Soccer](../Page/FIFA_International_Soccer.md "wikilink")（《FIFA》系列第一代作品）就有许多联赛的独家许可，有德国的[德国足球甲级联赛和](../Page/德国足球甲级联赛.md "wikilink")[德国足球乙级联赛](../Page/德国足球乙级联赛.md "wikilink")、英国的[英格兰足球超级联赛和](../Page/英格兰足球超级联赛.md "wikilink")[足球联赛](../Page/English_Football_League.md "wikilink")、意大利的[意大利足球甲级联赛和](../Page/意大利足球甲级联赛.md "wikilink")[意大利足球乙级联赛](../Page/意大利足球乙级联赛.md "wikilink")、西班牙的
[西班牙足球甲级联赛和](../Page/西班牙足球甲级联赛.md "wikilink")[西班牙足球乙级联赛](../Page/西班牙足球乙级联赛.md "wikilink")、法国的
[法国足球甲级联赛和](../Page/法国足球甲级联赛.md "wikilink")[法国足球乙级联赛](../Page/法国足球乙级联赛.md "wikilink")、葡萄牙的[葡萄牙足球超级联赛](../Page/葡萄牙足球超级联赛.md "wikilink")、土耳其的[土耳其足球超级联赛](../Page/Süper_Lig.md "wikilink"),、荷兰的[荷兰足球甲级联赛](../Page/Eredivisie.md "wikilink")、墨西哥的[墨西哥足球甲级联赛](../Page/Liga_MX.md "wikilink")、美国的
[美国职业足球大联盟](../Page/美国职业足球大联盟.md "wikilink")、韩国的[K联赛](../Page/K联赛.md "wikilink")、沙特阿拉伯的[沙特阿拉伯职业联赛](../Page/Saudi_Professional_League.md "wikilink")、澳大利亚的[A联赛](../Page/A-League.md "wikilink")、智利的[智利足球甲级联赛](../Page/Chilean_Primera_División.md "wikilink")、巴西的[巴西足球甲级联赛](../Page/巴西足球甲级联赛.md "wikilink")、阿根廷的
[阿根廷足球甲级联赛](../Page/Argentine_Primera_División.md "wikilink")、中国的[中国超级联赛](../Page/中国足球协会超级联赛.md "wikilink")。“FIFA”还可以在游戏里面使用真实的联赛名字、俱乐部名字、球员名字和肖像。此外，还拥有一些国际知名的俱乐部的许可，例如在[希腊足球超级联赛和](../Page/Superleague_Greece.md "wikilink")[南非足球超级联赛中的一些俱乐部](../Page/South_African_Premier_Division.md "wikilink")，但是没有当地整个联赛的许可。每一代“FIFA”主要是补充一些联赛的数据，例如
[世界杯](../Page/世界杯.md "wikilink"),
[欧洲足球锦标赛](../Page/欧洲足球锦标赛.md "wikilink")、[欧洲冠军联赛](../Page/欧洲冠军联赛.md "wikilink")，也补充一系列新的足球管理的职称。

[克里斯蒂亚诺·罗纳尔多（C罗）是](../Page/Cristiano_Ronaldo.md "wikilink")《FIFA
18》的代言人，在《FIFA
18》的封面、促销活动和广告中都能看到他的身影。[里奥·梅西从](../Page/Lionel_Messi.md "wikilink")《FIFA
13》到《FIFA
16》连续四次担任代言人。自从《[FIFA手机游戏](../Page/FIFA_Mobile.md "wikilink")》发布，多特蒙德前锋[马尔科·罗伊斯开始担任这个版本的代言人](../Page/Marco_Reus.md "wikilink")。

到了2011年，《FIFA》已经支持18种语言，并且在51个国家成功发布，更是被列入[吉尼斯世界纪录最畅销体育电子游戏榜单](../Page/Guinness_World_Records.md "wikilink")。在2016年，《FIFA》系列已经售出1.5亿份。此外，《[FIFA
12](../Page/FIFA_12.md "wikilink")》发售第一周就卖出3200万份，零售额超过1.86亿美元，成为有史以来销售最快的体育游戏。

## 历史

《FIFA 95》仅仅添加俱乐部比赛的功能，而《FIFA
96》则有重大创新，首次获得国际职业足球运动员联合会使用球员真实名字的许可。在[PlayStation](../Page/PlayStation.md "wikilink")、[PC](../Page/Personal_computer.md "wikilink")、[世嘉32X](../Page/Sega_32X.md "wikilink")、[世嘉土星上使用](../Page/Sega_Saturn.md "wikilink")“虚拟足球场（Virtual
Stadium）”引擎，能够使2D的球员在一个实时的3D足球场上运动。FIFA
97开始使用粗糙的多边形制作球员模型并添加室内足球场模式，但真正的成功则是《FIFA:
Road to World Cup 98|FIFA 98世界杯之路》创造的。《FIFA: Road to World Cup 98|FIFA
98世界杯之路》拥有改进的图像、改良的游戏性和带有资格赛的完整世界杯赛程（包括在国家足联注册的所有国家队）。几个月以后，它成为第一个[艺电（EA）官方认可的职业足球游戏](../Page/Electronic_Arts.md "wikilink")。

接下来几年游戏发行遭到不同程度的批评，玩家开始抱怨游戏中[AI](../Page/人工智能.md "wikilink")，还有从未被修复过的程序错误，售后服务质量差以及相比上一代太少的改进。随着游戏主机市场的不断扩大，《FIFA》不断受到其他同类游戏的挑战，比如[科乐美的](../Page/Konami.md "wikilink")《[实况足球](../Page/Pro_Evolution_Soccer.md "wikilink")》。《FIFA》和《实况足球》都拥有大量的支持者，即便如此，《FIFA》的销量仍然每年同比上升23%。

2012年，艺电体育（EA
Sports）和[里奥·梅西签下](../Page/Lionel_Messi.md "wikilink")《FIFA》的代言权，吸引他离开竞争对手《实况足球》。梅西的肖像很快就用在《[街头FIFA](../Page/FIFA_Street_\(2012_video_game\).md "wikilink")》上。2013年，西班牙职业女子足球运动员[Vero
Boquete在](../Page/Verónica_Boquete.md "wikilink")[Change.org上写下请愿书](../Page/Change.org.md "wikilink")，要求艺电（EA）在《FIFA》中增加女性球员，请愿书在24小时内吸引两万人签名。

## 游戏系列

### 90年代

#### FIFA国际足球（*FIFA International Soccer*）

  - 代言人: [David Platt](../Page/David_Platt_（footballer）.md "wikilink")
    and [Piotr Świerczewski](../Page/Piotr_Świerczewski.md "wikilink")
    （[Packie Bonner](../Page/Packie_Bonner.md "wikilink") and [Ruud
    Gullit](../Page/Ruud_Gullit.md "wikilink") in some versions）
  - 发售平台: PC, [DOS](../Page/DOS.md "wikilink"),
    [Amiga](../Page/Amiga.md "wikilink"), [Sega
    CD](../Page/Sega_CD.md "wikilink") （as *FIFA International Soccer
    Championship Edition*）,
    [3DO](../Page/3DO_Interactive_Multiplayer.md "wikilink"),
    [SNES](../Page/Super_Nintendo_Entertainment_System.md "wikilink"),
    [Mega Drive/Genesis](../Page/Sega_Genesis.md "wikilink"), [Master
    System](../Page/Master_System.md "wikilink"), [Sega Game
    Gear](../Page/Sega_Game_Gear.md "wikilink"), [Game
    Boy](../Page/Game_Boy.md "wikilink")
  - 发售日期: 1993年12月15日

`   在开发过程中被称为“EA Soccer”，有时也被称为“FIFA 94”，该系列的第一款游戏在1993年圣诞节前几周发布。 这个大肆炒作的足球游戏凭借它所采用的是`[`等距视角而不是俯视视角`](../Page/isometric_projection.md "wikilink")`（`*[`开球（Kick``
 ``Off）`](../Page/Kick_Off_\(series\).md "wikilink")*`）、侧视视角（`*[`欧洲俱乐部足球`](../Page/European_Club_Soccer.md "wikilink")*`)或者鸟瞰视角（`*[`Sensible``
 ``Soccer`](../Page/Sensible_Soccer_\(series\).md "wikilink")*`) ，游戏只包含国家队，但没有球员的真实姓名。`
`   FIFA 94 有一个臭名昭著的错误，就是允许球员站在守门员面前，这样球就能从守门员身上反弹回球门，从而得分。FIFA 94 在英国游戏排行榜上超越`[`街头霸王2排名第一`](../Page/Street_Fighter_II:_Hyper_Fighting.md "wikilink")`，并且连续六个月占据第一的位置。`
`  `[`世嘉Mega`](../Page/Mega_\(magazine\).md "wikilink")` 将 FIFA 94 放在他们的五十大`[`Mega``
 ``Drive游戏中的第十一位`](../Page/Mega_Drive.md "wikilink")`。世嘉Mega的CD版本以“FIFA International Soccer Championship Edition（FIFA国际足球锦标赛版）”发布。它拥有FIFA 95中的一些功能，在原版的基础上提高了游戏的画质。这个版本在十大Mega CD游戏中排行第七。在`[`3DO游戏机的是FIFA`](../Page/3DO.md "wikilink")` 94画质最高的版本，配备了虚拟3D摄像机（pseudo-3D cameras）。`
`   此外，`[`PlayStation``
 ``2版本的`](../Page/PlayStation_2.md "wikilink")[`FIFA``
 ``06包含FIFA`](../Page/FIFA_06.md "wikilink")` 94的游戏内容，它是用于庆祝1994年美国成功举办世界杯。值得注意的是超级NES版本，尽管比世嘉Mega少了许多可选择的球队，但依然有三支国家队有资格参加“真实生活锦标赛 （the real-life tournament）”，它们是`[`玻利维亚国家队`](../Page/Bolivia_national_football_team.md "wikilink")`、`[`沙特阿拉伯国家队`](../Page/Saudi_Arabia_national_football_team.md "wikilink")`、`[`韩国国家队`](../Page/South_Korea_national_football_team.md "wikilink")`。`
`   这款游戏之所以称为“国际足球（International Soccer）”，是因为如果美国人对它不感兴趣的话，EA也可以在欧洲成功销售。`

#### *FIFA 95*

  - 代言人: [Erik Thorstvedt](../Page/Erik_Thorstvedt.md "wikilink")
    （在一些版本中是[Alexi
    Lalas](../Page/Alexi_Lalas.md "wikilink")）
  - 发售平台: [Mega Drive/Genesis](../Page/Sega_Genesis.md "wikilink")
  - 发售日期: 1994年11月10日

#### *FIFA 96*

  - 代言人: [Frank de Boer和](../Page/Frank_de_Boer.md "wikilink")[Jason
    McAteer](../Page/Jason_McAteer.md "wikilink") （欧洲）
  - 发售平台:
    [DOS](../Page/DOS.md "wikilink")/[Windows](../Page/Microsoft_Windows.md "wikilink"),
    [PlayStation](../Page/PlayStation_（console）.md "wikilink"), [Sega
    Saturn](../Page/Sega_Saturn.md "wikilink"), [Sega
    32X](../Page/Sega_32X.md "wikilink"),
    [SNES](../Page/Super_Nintendo_Entertainment_System.md "wikilink"),
    [Mega Drive/Genesis](../Page/Sega_Genesis.md "wikilink"), [Sega Game
    Gear](../Page/Sega_Game_Gear.md "wikilink"), [Game
    Boy](../Page/Game_Boy.md "wikilink")
  - 发售日期: 1995年9月30日

#### *FIFA 97*

  - 代言人: [David Ginola](../Page/David_Ginola.md "wikilink")
    （欧洲）；[Bebeto](../Page/Bebeto.md "wikilink") （其他地区）
  - 发售平台: [DOS](../Page/DOS.md "wikilink"),
    [Windows](../Page/Microsoft_Windows.md "wikilink"),
    [PlayStation](../Page/PlayStation_（console）.md "wikilink"), [Sega
    Saturn](../Page/Sega_Saturn.md "wikilink"), [Mega
    Drive/Genesis](../Page/Sega_Genesis.md "wikilink"),
    [SNES](../Page/Super_Nintendo_Entertainment_System.md "wikilink"),
    [Game Boy](../Page/Game_Boy.md "wikilink")
  - 发售日期: 1996年11月30日

#### FIFA 98世界杯之路（*FIFA: Road to World Cup 98*）

  - 代言人: [Roy Lassiter](../Page/Roy_Lassiter.md "wikilink") （美国）； [David
    Beckham](../Page/David_Beckham.md "wikilink") （英国）； [Paolo
    Maldini](../Page/Paolo_Maldini.md "wikilink") （意大利）； [David
    Ginola](../Page/David_Ginola.md "wikilink") （法国）； [Andreas
    Möller](../Page/Andreas_Möller.md "wikilink") （德国）；
    [Raúl](../Page/Raúl_（footballer）.md "wikilink") （西班牙）
  - 主题音乐: "[Song 2](../Page/Song_2.md "wikilink")" by
    [Blur](../Page/Blur_（band）.md "wikilink")
  - 发售平台: [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink"),
    [PlayStation](../Page/PlayStation.md "wikilink"), [Nintendo
    64](../Page/Nintendo_64.md "wikilink"), [Sega
    Saturn](../Page/Sega_Saturn.md "wikilink"),
    [SNES](../Page/Super_Nintendo_Entertainment_System.md "wikilink"),
    [Mega Drive/Genesis](../Page/Sega_Genesis.md "wikilink"), [Game
    Boy](../Page/Game_Boy.md "wikilink")
  - 发售日期: 1997年11月8日
  - 宣传语：“你的唯一目标：晋级”

此款游戏被许多人认为是《FIFA》系列中最优秀的一款。它拥有优化过的图形引擎，用户定制球员、球队的选项，16座真实的体育场，更高的[AI以及受欢迎的](../Page/AI.md "wikilink")“世界杯之路”模式和所有在[国际足联注册的国家队](../Page/国际足联.md "wikilink")。

英国乐队[Blur为这个游戏创作了著名歌曲](../Page/Blur.md "wikilink")《Song
2》。游戏开场音乐选用了英国乐队[Chumbawamba的歌曲](../Page/Chumbawamba.md "wikilink")《[Tubthumping](../Page/Tubthumping.md "wikilink")》。

#### FIFA 99

  - 代言人: [Dennis Bergkamp](../Page/Dennis_Bergkamp.md "wikilink") （全球）,
    [Kasey Keller](../Page/Kasey_Keller.md "wikilink") （美国）, [Fabien
    Barthez](../Page/Fabien_Barthez.md "wikilink") （法国）, [Hidetoshi
    Nakata](../Page/Hidetoshi_Nakata.md "wikilink") （日本）； [Olaf
    Thon](../Page/Olaf_Thon.md "wikilink") （德国）； [Rui
    Costa](../Page/Rui_Costa.md "wikilink") （葡萄牙）； [Christian
    Vieri](../Page/Christian_Vieri.md "wikilink") （意大利）； [Ahn
    Jung-hwan](../Page/Ahn_Jung-hwan.md "wikilink") （韩国）； [Fernando
    Morientes](../Page/Fernando_Morientes.md "wikilink") （西班牙）； [Jason
    Kreis](../Page/Jason_Kreis.md "wikilink") （美国测试版）
  - 主题音乐: "[The Rockafeller
    Skank](../Page/The_Rockafeller_Skank.md "wikilink") （Remix）" by
    [Fatboy Slim](../Page/Norman_Cook.md "wikilink")
  - 发售平台: [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink"),
    [PlayStation](../Page/PlayStation.md "wikilink"), [Nintendo
    64](../Page/Nintendo_64.md "wikilink")
  - 发售日期: 1998年11月30日

遊戲開場音樂選用了英國電子音樂大師[Fatboy
Slim](../Page/Fatboy_Slim.md "wikilink")（流線胖小子）的知名歌曲《[The
Rockafeller Skank](../Page/The_Rockafeller_Skank.md "wikilink")》‏。

### 2000年以后

#### '' FIFA 2000''

  - 代言人: [Hidetoshi Nakata](../Page/Hidetoshi_Nakata.md "wikilink")
    （日本）； [Sol Campbell](../Page/Sol_Campbell.md "wikilink")
    （英国）； [Vincenzo
    Montella](../Page/Vincenzo_Montella.md "wikilink") （意大利）； [Pep
    Guardiola](../Page/Pep_Guardiola.md "wikilink") （西班牙）； [Emmanuel
    Petit](../Page/Emmanuel_Petit.md "wikilink") （法国）； [Jaap
    Stam](../Page/Jaap_Stam.md "wikilink") （Netherlands）； [Vassilios
    Tsiartas](../Page/Vassilios_Tsiartas.md "wikilink") （希腊）； [Mehmet
    Scholl](../Page/Mehmet_Scholl.md "wikilink") （德国）；
    [Simão](../Page/Simão_Sabrosa.md "wikilink") （葡萄牙）； [Eddie
    Pope](../Page/Eddie_Pope.md "wikilink") （美国）；
    [Raí](../Page/Raí.md "wikilink") （巴西）； [Par
    Zetterberg](../Page/Par_Zetterberg.md "wikilink") （Sweden）； [Kim
    Byung-ji](../Page/Kim_Byung-ji.md "wikilink") （韩国）； [Kiatisak
    Senamuang](../Page/Kiatisak_Senamuang.md "wikilink") （泰国）；
  - 主题音乐: "[It's Only Us](../Page/It's_Only_Us.md "wikilink")" by
    [Robbie Williams](../Page/Robbie_Williams.md "wikilink")
  - 发售平台: [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink"),
    [PlayStation](../Page/PlayStation.md "wikilink"), [Game Boy
    Color](../Page/Game_Boy_Color.md "wikilink")
  - 发售日期: 1999年10月26日

#### *FIFA 2001*

  - 代言人: [Edgar Davids](../Page/Edgar_Davids.md "wikilink")
    （Netherlands）； [Paul
    Scholes](../Page/Paul_Scholes.md "wikilink") （英国）； [Gheorghe
    Hagi](../Page/Gheorghe_Hagi.md "wikilink") （Romania）； [Ben
    Olsen](../Page/Ben_Olsen.md "wikilink") （美国）； [Ricardo Sá
    Pinto](../Page/Ricardo_Sá_Pinto.md "wikilink") （葡萄牙）； [Gaizka
    Mendieta](../Page/Gaizka_Mendieta.md "wikilink") （西班牙）； [Filippo
    Inzaghi](../Page/Filippo_Inzaghi.md "wikilink") （意大利）； [Lothar
    Matthäus](../Page/Lothar_Matthäus.md "wikilink") （德国）； [Thierry
    Henry](../Page/Thierry_Henry.md "wikilink") （法国）；
    [Leonardo](../Page/Leonardo_Araújo.md "wikilink") （巴西）； [Shimon
    Gershon](../Page/Shimon_Gershon.md "wikilink") （以色列）； [Ko
    Jong-soo](../Page/Ko_Jong-soo.md "wikilink") （韩国）
  - 主题音乐: "[Bodyrock](../Page/Bodyrock_（song）.md "wikilink")" by
    [Moby](../Page/Moby.md "wikilink")
  - 发售平台: [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink"),
    [PlayStation 2](../Page/PlayStation_2.md "wikilink"),
    [PlayStation](../Page/PlayStation_（console）.md "wikilink")
  - 发售日期: 2000年11月8日

#### FIFA足球2002（*FIFA Football 2002*）

[FIFA2002.JPG](https://zh.wikipedia.org/wiki/File:FIFA2002.JPG "fig:FIFA2002.JPG")

  - 代言人: [Hidetoshi Nakata](../Page/Hidetoshi_Nakata.md "wikilink")
    （日本）； [Thierry Henry](../Page/Thierry_Henry.md "wikilink")
    （France, UK）； [Zlatan
    Ibrahimović](../Page/Zlatan_Ibrahimović.md "wikilink") （Sweden）；
    [Nuno Gomes](../Page/Nuno_Gomes.md "wikilink") （葡萄牙）； [Francesco
    Totti](../Page/Francesco_Totti.md "wikilink") （意大利）； [Ruud van
    Nistelrooy](../Page/Ruud_van_Nistelrooy.md "wikilink")
    （Netherlands）； [İlhan
    Mansız](../Page/İlhan_Mansız.md "wikilink") （土耳其）； [Gerald
    Asamoah](../Page/Gerald_Asamoah.md "wikilink") （德国）； [Lampros
    Choutos](../Page/Lampros_Choutos.md "wikilink") （希腊）； [Hong
    Myung-bo](../Page/Hong_Myung-bo.md "wikilink") （韩国）； [Sibusiso
    Zuma](../Page/Sibusiso_Zuma.md "wikilink") （South Africa & Denmark）；
    [Nawaf Al-Temyat](../Page/Nawaf_Al-Temyat.md "wikilink") （Saudi
    Arabia）； [Tomasz Radzinski](../Page/Tomasz_Radzinski.md "wikilink")
    （美国）； [Roberto Carlos](../Page/Roberto_Carlos.md "wikilink") （巴西）；
    [Iker Casillas](../Page/Iker_Casillas.md "wikilink") （西班牙）； [Tomasz
    Frankowski](../Page/Tomasz_Frankowski.md "wikilink") （波兰）；
  - 主题音乐: "[19-2000](../Page/19-2000.md "wikilink") （Soulchild Remix）"
    by [Gorillaz](../Page/Gorillaz.md "wikilink")
  - 发售平台: [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink"),
    [PlayStation 2](../Page/PlayStation_2.md "wikilink"),
    [GameCube](../Page/GameCube.md "wikilink"),
    [PlayStation](../Page/PlayStation.md "wikilink")
  - 发售日期: 2001年11月1日

由于[科乐美](../Page/科乐美.md "wikilink")（Konami）在行业上的领先地位，电子艺界决定推出在射门和传球增加力量槽来改善游戏。但是，很多玩家注意到本游戏似乎在难度较高时预先设定比赛结果。《FIFA》的这个版本也不再使用原来的以颜色条代替队徽，而首次得到了全部球队的正式队徽。力量槽也可以根据玩家喜好来调整。

[FIFA2002Rewards.JPG](https://zh.wikipedia.org/wiki/File:FIFA2002Rewards.JPG "fig:FIFA2002Rewards.JPG")

电子艺界从意大利贴纸公司购得版权，在玩家赢得某些奖项（[欧洲冠军杯](../Page/欧洲冠军杯.md "wikilink")，[欧洲联盟杯](../Page/欧洲联盟杯.md "wikilink")，[世界杯欧洲区](../Page/世界杯.md "wikilink")、南美洲区、中北美洲区、亚洲区预选赛以及后来分别激活[欧锦赛](../Page/歐洲足球錦標賽.md "wikilink")、[美洲杯](../Page/美洲盃足球賽.md "wikilink")、[美洲金盃](../Page/美洲金盃.md "wikilink")，以及[亚洲杯](../Page/亚洲杯.md "wikilink")，以及赢得以上所有奖项时激活的[联合会杯](../Page/联合会杯.md "wikilink")）时，游戏将会颁发一张卡片，上有一位球员的照片。关于这个奖励系统经常被提出的问题就是赢得各种奖励对游戏的可玩性并没有什么改善。

并且，已经晋级世界杯的[法国](../Page/法国国家足球隊.md "wikilink")（上届冠军）、[日本和](../Page/日本国家足球隊.md "wikilink")[韩国](../Page/韩国国家足球隊.md "wikilink")（东道主）国家队也可以参加各自大洲的世界杯预选赛，并通过预选赛或者友谊赛来改变本国家队在国际足联的排名。

球迷们期待着的国家队比赛模式中包括了所有的国家队和选择大名单的权利。但是，许多国家队的版权都没有得到，个别足联（比如非洲）的球队名单甚至都不完整。游戏中也没有非洲杯这项赛事。很多球迷非常愤怒，对电子艺界的信心也有所减弱。

另外，《FIFA 2002》和前后版本的另一区别在于没有内置的世界杯模式，而是由电子艺界另外发行的。

#### FIFA足球2003（*FIFA Football 2003*）

  - 代言人: [Roberto
    Carlos](../Page/Roberto_Carlos_（footballer）.md "wikilink"),
    [Ryan Giggs](../Page/Ryan_Giggs.md "wikilink"), and [Edgar
    Davids](../Page/Edgar_Davids.md "wikilink") （in the United States,
    only [Landon Donovan](../Page/Landon_Donovan.md "wikilink")
    appeared）
  - 主题音乐: "[To Get Down](../Page/To_Get_Down.md "wikilink") （[Fatboy
    Slim](../Page/Fatboy_Slim.md "wikilink") remix）" by [Timo
    Maas](../Page/Timo_Maas.md "wikilink")
  - 发售平台: [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink"),
    [PlayStation 2](../Page/PlayStation_2.md "wikilink"),
    [Xbox](../Page/Xbox_（console）.md "wikilink"),
    [GameCube](../Page/GameCube.md "wikilink"),
    [PlayStation](../Page/PlayStation.md "wikilink"), [Game Boy
    Advance](../Page/Game_Boy_Advance.md "wikilink"), [Mobile
    phone](../Page/Mobile_phone.md "wikilink")
  - 发售日期: 2002年11月5日

#### FIFA足球2004（*FIFA Football 2004*）

  - 代言人: [Thierry Henry](../Page/Thierry_Henry.md "wikilink"),
    [Alessandro Del Piero](../Page/Alessandro_Del_Piero.md "wikilink")
    and [Ronaldinho](../Page/Ronaldinho.md "wikilink")
  - 主题音乐: "[Red Morning Light](../Page/Red_Morning_Light.md "wikilink")"
    by [Kings of Leon](../Page/Kings_of_Leon.md "wikilink")
  - 发售平台: [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink"),
    [PlayStation 2](../Page/PlayStation_2.md "wikilink"),
    [Xbox](../Page/Xbox_（console）.md "wikilink"),
    [GameCube](../Page/GameCube.md "wikilink"),
    [PlayStation](../Page/PlayStation.md "wikilink"), [Game Boy
    Advance](../Page/Game_Boy_Advance.md "wikilink"), [Nokia
    N-Gage](../Page/N-Gage_（device）.md "wikilink"), [Mobile
    phone](../Page/Mobile_phone.md "wikilink")
  - 发售日期: 2003年10月8日

本版《FIFA》除了增加了游戏的流畅性以外，并未对游戏引擎做太多修改。最大的新特性是增加了世界范围内的二级联赛。也就是说允许玩家使用一支二流乃至三流球队参加世界顶级联赛和[欧洲冠军联赛](../Page/欧洲冠军联赛.md "wikilink")。游戏有一个被称做“离球控制一”，即在某些情况下需要玩家同时操纵两个球员以完成一些花样。虽然理论上这个概念看起来很诱人，但由于操作太烦琐，玩家很难将它运用得很好。如果没有带方向杆的手柄，这样的操作甚至根本没办法完成。

#### FIFA足球2005（*FIFA Football 2005*）

  - 代言人: [Patrick Vieira](../Page/Patrick_Vieira.md "wikilink"),
    [Fernando Morientes](../Page/Fernando_Morientes.md "wikilink") and
    [Andriy Shevchenko](../Page/Andriy_Shevchenko.md "wikilink") （in
    North America, [Oswaldo
    Sánchez](../Page/Oswaldo_Sánchez.md "wikilink") replaced Patrick
    Vieira）
  - 主题音乐: "[Surfing on a
    Rocket](../Page/Surfing_on_a_Rocket.md "wikilink")" by
    [Air](../Page/Air.md "wikilink")
  - 发售平台: [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink"),
    [PlayStation 2](../Page/PlayStation_2.md "wikilink"),
    [Xbox](../Page/Xbox_（console）.md "wikilink"),
    [GameCube](../Page/GameCube.md "wikilink"),
    [PlayStation](../Page/PlayStation.md "wikilink"), [PlayStation
    Portable](../Page/PlayStation_Portable.md "wikilink") （America
    only）, [Game Boy Advance](../Page/Game_Boy_Advance.md "wikilink"),
    [Nokia N-Gage](../Page/N-Gage_（device）.md "wikilink"),
    [Gizmondo](../Page/Gizmondo.md "wikilink"), [Mobile
    phone](../Page/Mobile_phone.md "wikilink")
  - 发售日期: 2004年10月11日

#### *FIFA 06*

  - 代言人: [Wayne Rooney](../Page/Wayne_Rooney.md "wikilink") and
    [Ronaldinho](../Page/Ronaldinho.md "wikilink") （in North America,
    [Omar Bravo](../Page/Omar_Bravo.md "wikilink") and [Freddy
    Adu](../Page/Freddy_Adu.md "wikilink") joined Ronaldinho on the
    cover）
  - 主题音乐:
    "[Helicopter](../Page/Helicopter_（Bloc_Party_song）.md "wikilink")"
    by [Bloc Party](../Page/Bloc_Party.md "wikilink")
  - 发售平台: [GameCube](../Page/GameCube.md "wikilink"), [Microsoft
    Windows](../Page/Microsoft_Windows.md "wikilink"), [Nintendo
    DS](../Page/Nintendo_DS.md "wikilink"), [PlayStation
    2](../Page/PlayStation_2.md "wikilink"),
    [Xbox](../Page/Xbox_（console）.md "wikilink"), [Game Boy
    Advance](../Page/Game_Boy_Advance.md "wikilink"), [PlayStation
    Portable](../Page/PlayStation_Portable.md "wikilink"), [Xbox
    360](../Page/Xbox_360.md "wikilink")
  - 发售日期： 2005年10月4日

#### *FIFA 07*

  - 代言人: [Wayne Rooney](../Page/Wayne_Rooney.md "wikilink") and
    [Ronaldinho](../Page/Ronaldinho.md "wikilink") （全球）；
    [Ronaldinho](../Page/Ronaldinho.md "wikilink") and [Lukas
    Podolski](../Page/Lukas_Podolski.md "wikilink") （德国）；
    [Ronaldinho](../Page/Ronaldinho.md "wikilink"), [Landon
    Donovan](../Page/Landon_Donovan.md "wikilink") and [Francisco
    Fonseca](../Page/Francisco_Fonseca.md "wikilink") （北美）；
    [Ronaldinho](../Page/Ronaldinho.md "wikilink") and [Juninho
    Pernambucano](../Page/Juninho_Pernambucano.md "wikilink") （法国）；
    [Ronaldinho](../Page/Ronaldinho.md "wikilink") and [David
    Villa](../Page/David_Villa.md "wikilink") （西班牙）
  - 主题音乐: "[Can't Get Enough （Mekon
    Remix）](../Page/Can't_Get_Enough_（Mekon_Remix）.md "wikilink")"
    by [The Infadels](../Page/The_Infadels.md "wikilink")
  - 发售平台: [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink"),
    [Xbox 360](../Page/Xbox_360.md "wikilink"), [PlayStation
    2](../Page/PlayStation_2.md "wikilink"),
    [Xbox](../Page/Xbox_（console）.md "wikilink"),
    [GameCube](../Page/GameCube.md "wikilink"), [PlayStation
    Portable](../Page/PlayStation_Portable.md "wikilink"), [Nintendo
    DS](../Page/Nintendo_DS.md "wikilink"), [Game Boy
    Advance](../Page/Game_Boy_Advance.md "wikilink"), [Java
    ME](../Page/Java_ME.md "wikilink")
  - 发售日期: 2006年9月27日

《FIFA 07》中包括来自20个国家的27个联赛，510余支球队各有其精确真实的球衣和徽章。玩家可以参加 EA
SPORTS™互动联赛，在网上用一支的俱乐部对抗各路对手，每周包括用户访谈在内的会播客宣传将玩家的胜绩。EA设计了全新的障碍比赛系统。此作强化了经理模式：使事物都接近真实。真实的资金、青年球员的成长、媒体和球迷的反应，令玩家全身心地沉浸在足球经理的世界中。人群会根据你的表现作出反应。进球和控球将会令球迷发出震耳的欢呼，赛事不利则会让他们垂头丧气。视角会集中在那些情绪特别明显的球员身上。玩家还可以创建你自己的俱乐部。

《FIFA
07》同样有技术上的改进。使用左摇杆可以控制足球旋转，作出更具创造性的定位球和射门。游戏设置了更复杂的射门机制和更精确的操控。以及更高级的智能守门员，使得守门员的智能更真实，有着逼真的反应以及各种躲闪和救球动作。

#### *FIFA 08*

  - 代言人: [Wayne Rooney](../Page/Wayne_Rooney.md "wikilink") and
    [Ronaldinho](../Page/Ronaldinho.md "wikilink") （全球）；
    [Ronaldinho](../Page/Ronaldinho.md "wikilink") and [Miroslav
    Klose](../Page/Miroslav_Klose.md "wikilink") （德国）；
    [Ronaldinho](../Page/Ronaldinho.md "wikilink") and [Sergio
    Ramos](../Page/Sergio_Ramos.md "wikilink") （西班牙）；
    [Ronaldinho](../Page/Ronaldinho.md "wikilink"), [Jozy
    Altidore](../Page/Jozy_Altidore.md "wikilink") and [Guillermo
    Ochoa](../Page/Guillermo_Ochoa.md "wikilink") （北美）；
    [Ronaldinho](../Page/Ronaldinho.md "wikilink") and [Euzebiusz
    Smolarek](../Page/Euzebiusz_Smolarek.md "wikilink") （波兰）；
  - 主题音乐: "Sketches （20 Something Life）" by La Rocca
  - 发售平台: [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink"),
    [Xbox 360](../Page/Xbox_360.md "wikilink"), [PlayStation
    3](../Page/PlayStation_3.md "wikilink"),
    [Wii](../Page/Wii.md "wikilink"), [PlayStation
    2](../Page/PlayStation_2.md "wikilink"), [PlayStation
    Portable](../Page/PlayStation_Portable.md "wikilink"), [Nintendo
    DS](../Page/Nintendo_DS.md "wikilink"), [Java
    ME](../Page/Java_ME.md "wikilink")
  - 发售日期: 2007年9月20日

#### *FIFA 09*

  - 代言人: [Wayne Rooney](../Page/Wayne_Rooney.md "wikilink") and
    [Ronaldinho](../Page/Ronaldinho.md "wikilink") （全球）； [Wayne
    Rooney](../Page/Wayne_Rooney.md "wikilink") and [Sergio
    Ramos](../Page/Sergio_Ramos.md "wikilink") （西班牙）；
    [Ronaldinho](../Page/Ronaldinho.md "wikilink") and [Kevin
    Kuranyi](../Page/Kevin_Kuranyi.md "wikilink") （德国）；
    [Ronaldinho](../Page/Ronaldinho.md "wikilink") and [Balázs
    Dzsudzsák](../Page/Balázs_Dzsudzsák.md "wikilink") （Hungary）；
    [Ronaldinho](../Page/Ronaldinho.md "wikilink") and [Daniele De
    Rossi](../Page/Daniele_De_Rossi.md "wikilink") （意大利）；
    [Ronaldinho](../Page/Ronaldinho.md "wikilink"), [Maurice
    Edu](../Page/Maurice_Edu.md "wikilink") and [Guillermo
    Ochoa](../Page/Guillermo_Ochoa.md "wikilink") （北美）
  - 主题音乐: "Let U Know" by [Plastilina
    Mosh](../Page/Plastilina_Mosh.md "wikilink")
  - 发售平台: [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink"),
    [PlayStation 3](../Page/PlayStation_3.md "wikilink"), [Xbox
    360](../Page/Xbox_360.md "wikilink"),
    [Wii](../Page/Wii.md "wikilink"), [PlayStation
    2](../Page/PlayStation_2.md "wikilink"), [PlayStation
    Portable](../Page/PlayStation_Portable.md "wikilink"), [Nintendo
    DS](../Page/Nintendo_DS.md "wikilink"), [Java
    ME](../Page/Java_ME.md "wikilink")
  - 发售日期: 2008年10月3日

FIFA 09是美国EA公司旗下由EA
Canada小组负责开发的FIFA系列电子游戏，于2008年10月登陆Windows、任天堂DS、PlayStation
2、PlayStation 3、PlayStation Portable、Wii、Xbox 360和Zeebo平台，同年11月，登陆N-Gage
2.0和移动电话平台。游戏标语为“Let's FIFA 09”。

### 2010年以后

#### *FIFA 10*

  - 代言人: [Theo Walcott](../Page/Theo_Walcott.md "wikilink"), [Frank
    Lampard和](../Page/Frank_Lampard.md "wikilink")[Wayne
    Rooney](../Page/Wayne_Rooney.md "wikilink") （英国）； [Wayne
    Rooney和](../Page/Wayne_Rooney.md "wikilink")[Tim
    Cahill](../Page/Tim_Cahill.md "wikilink") （澳大利亚）； [Wayne
    Rooney和](../Page/Wayne_Rooney.md "wikilink")[Andreas
    Ivanschitz](../Page/Andreas_Ivanschitz.md "wikilink") （奥地利）； [Wayne
    Rooney](../Page/Wayne_Rooney.md "wikilink") and [Balázs
    Dzsudzsák](../Page/Balázs_Dzsudzsák.md "wikilink") （Hungary）,
    [Wayne Rooney](../Page/Wayne_Rooney.md "wikilink") and [Robert
    Lewandowski](../Page/Robert_Lewandowski.md "wikilink") （波兰）；
    [Ronaldinho和](../Page/Ronaldinho.md "wikilink")[Giorgio
    Chiellini](../Page/Giorgio_Chiellini.md "wikilink") （意大利）； [Karim
    Benzema](../Page/Karim_Benzema.md "wikilink"), [Steve
    Mandanda和](../Page/Steve_Mandanda.md "wikilink")[Guillaume
    Hoarau](../Page/Guillaume_Hoarau.md "wikilink") （法国）； [Wayne
    Rooney和](../Page/Wayne_Rooney.md "wikilink")[Bastian
    Schweinsteiger](../Page/Bastian_Schweinsteiger.md "wikilink") （德国）；
    [Frank
    Lampard和](../Page/Frank_Lampard.md "wikilink")[Simão](../Page/Simão_Sabrosa.md "wikilink")
    （葡萄牙）； [Karim
    Benzema和](../Page/Karim_Benzema.md "wikilink")[Xavi](../Page/Xavi.md "wikilink")
    （西班牙）； [Frank Lampard](../Page/Frank_Lampard.md "wikilink")； [Sacha
    Kljestan](../Page/Sacha_Kljestan.md "wikilink") and [Cuauhtémoc
    Blanco](../Page/Cuauhtémoc_Blanco.md "wikilink") （北美）； [Sergei
    Semak](../Page/Sergei_Semak.md "wikilink") （俄罗斯）
  - 主题音乐: "[Nothing to Worry
    About](../Page/Nothing_to_Worry_About.md "wikilink")" by [Peter
    Bjorn and John](../Page/Peter_Bjorn_and_John.md "wikilink")
  - 发售平台: [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink"),
    [Xbox 360](../Page/Xbox_360.md "wikilink"), [PlayStation
    3](../Page/PlayStation_3.md "wikilink"),
    [Wii](../Page/Wii.md "wikilink"), [PlayStation
    2](../Page/PlayStation_2.md "wikilink"), [PlayStation
    Portable](../Page/PlayStation_Portable.md "wikilink"), [Nintendo
    DS](../Page/Nintendo_DS.md "wikilink"),
    [iOS](../Page/iOS_（Apple）.md "wikilink"),
    [Android](../Page/Android_（operating_system）.md "wikilink"), [Java
    ME](../Page/Java_ME.md "wikilink"), [Wii](../Page/Wii.md "wikilink")
  - 发售日期: 2009年10月2日（欧洲）, 2009年10月20日 （美国）

#### *FIFA 11*

  - 代言人: [Kaká](../Page/Kaká.md "wikilink") （全球）, [Wayne
    Rooney](../Page/Wayne_Rooney.md "wikilink") （英国，爱尔兰共和国，澳大利亚）, [Mesut
    Özil](../Page/Mesut_Özil.md "wikilink") & [René
    Adler](../Page/René_Adler.md "wikilink") （德国）, [Tim
    Cahill](../Page/Tim_Cahill.md "wikilink") （澳大利亚）, [Jakub
    Błaszczykowski](../Page/Jakub_Błaszczykowski.md "wikilink") （波兰）；
  - 发售平台: [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink"),
    [Xbox 360](../Page/Xbox_360.md "wikilink"), [PlayStation
    3](../Page/PlayStation_3.md "wikilink"),
    [Wii](../Page/Wii.md "wikilink"), [PlayStation
    2](../Page/PlayStation_2.md "wikilink"), [PlayStation
    Portable](../Page/PlayStation_Portable.md "wikilink"), [Nintendo
    DS](../Page/Nintendo_DS.md "wikilink"),
    [iOS](../Page/iOS_（Apple）.md "wikilink"), [BlackBerry
    OS](../Page/BlackBerry_OS.md "wikilink"), [Java
    ME](../Page/Java_ME.md "wikilink")
  - 发售日期: 2010年9月28日（美国）,，2010年10月1日（欧洲）

#### *FIFA 12*

  - 代言人: [Wayne Rooney](../Page/Wayne_Rooney.md "wikilink") and [Jack
    Wilshere](../Page/Jack_Wilshere.md "wikilink") （英国，爱尔兰共和国）, [Landon
    Donovan](../Page/Landon_Donovan.md "wikilink") and [Rafael
    Márquez](../Page/Rafael_Márquez.md "wikilink") （北美）, [Jakub
    Błaszczykowski](../Page/Jakub_Błaszczykowski.md "wikilink") （波兰）；
  - 主题音乐: "Kids" by Sleigh Bells
  - 发售平台: [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink"),
    [OS X](../Page/OS_X.md "wikilink"), [PlayStation
    3](../Page/PlayStation_3.md "wikilink"), [Xbox
    360](../Page/Xbox_360.md "wikilink"),
    [Wii](../Page/Wii.md "wikilink"), [PlayStation
    2](../Page/PlayStation_2.md "wikilink"), [PlayStation
    Vita](../Page/PlayStation_Vita.md "wikilink"), [Nintendo
    3DS](../Page/Nintendo_3DS.md "wikilink"), [PlayStation
    Portable](../Page/PlayStation_Portable.md "wikilink"),
    [iOS](../Page/iOS.md "wikilink"), [Java
    ME](../Page/Java_Platform,_Micro_Edition.md "wikilink")
  - 发售日期: 2011年9月27日（美国），2011年9月30日（欧洲）

<!-- end list -->

  - 宣传语：“爱足球，玩足球”

#### *FIFA 13*

  - 代言人: [Lionel Messi](../Page/Lionel_Messi.md "wikilink") （[Joe
    Hart](../Page/Joe_Hart.md "wikilink") and [Alex
    Oxlade-Chamberlain](../Page/Alex_Oxlade-Chamberlain.md "wikilink")
    also feature in the UK version）, [Jakub
    Błaszczykowski](../Page/Jakub_Błaszczykowski.md "wikilink") （波兰）；
  - 主题音乐: "[Club Foot](../Page/Club_Foot_（song）.md "wikilink")" by
    [Kasabian](../Page/Kasabian.md "wikilink")
  - 发售平台: [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink"),
    [Wii U](../Page/Wii_U.md "wikilink"), [PlayStation
    3](../Page/PlayStation_3.md "wikilink"), [Xbox
    360](../Page/Xbox_360.md "wikilink"),
    [Wii](../Page/Wii.md "wikilink"), [PlayStation
    2](../Page/PlayStation_2.md "wikilink"), [PlayStation
    Vita](../Page/PlayStation_Vita.md "wikilink"), [Nintendo
    3DS](../Page/Nintendo_3DS.md "wikilink"), [PlayStation
    Portable](../Page/PlayStation_Portable.md "wikilink"),
    [iOS](../Page/iOS.md "wikilink"), [Windows
    Phone](../Page/Windows_Phone.md "wikilink"),
    [Android](../Page/Android_（operating_system）.md "wikilink"), [Java
    ME](../Page/Java_ME.md "wikilink")
  - 发售日期: 2012年9月25日（美国），2012年9月27日（澳大利亚），2012年9月28日（欧洲）

《FIFA13》于2012年9月25日在北美区上市。梅西擔任此部作品的代言人。遊戲加入新的傳球、進球慶祝動作以及新的守門員撲救等動作或方式。同时在《FIFA
12》原有的30个联赛的基础中，新增了[沙特职业足球联赛](../Page/沙特职业足球联赛.md "wikilink")。FIFA
13中共有46支国家队。捷克和巴拉圭回到了游戏当中，印度、玻利维亚和委内瑞拉在自FIFA Football
2002以来的11年间断后也重返主系列。而克罗地亚则被去掉了。

#### *FIFA 14*

  - 代言人: [Lionel Messi](../Page/Lionel_Messi.md "wikilink")（全球）, [Javier
    Hernández](../Page/Javier_Hernández.md "wikilink") （北美）, \[Stephan
    El Shaarawy\]\] （意大利）, [Arturo
    Vidal](../Page/Arturo_Vidal.md "wikilink") and [Radamel
    Falcao](../Page/Radamel_Falcao.md "wikilink") （阿根廷，智利，巴拿马，委内瑞拉）,
    [Gareth Bale](../Page/Gareth_Bale.md "wikilink") （英国，爱尔兰共和国）,
    [Michal Kadlec](../Page/Michal_Kadlec.md "wikilink") （Czech
    Republic）, [Robert
    Lewandowski](../Page/Robert_Lewandowski.md "wikilink") （波兰）, [Balázs
    Dzsudzsák](../Page/Balázs_Dzsudzsák.md "wikilink") （匈牙利）, [Xherdan
    Shaqiri](../Page/Xherdan_Shaqiri.md "wikilink") （瑞士）, [David
    Alaba](../Page/David_Alaba.md "wikilink") （奥地利）, [Tim
    Cahill](../Page/Tim_Cahill.md "wikilink") （澳大利亚）, [Maya
    Yoshida](../Page/Maya_Yoshida.md "wikilink") and [Makoto
    Hasebe](../Page/Makoto_Hasebe.md "wikilink") （日本）, [Mustafa
    Al-Bassas](../Page/Mustafa_Al-Bassas.md "wikilink") （中东）
  - 发售平台: [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink"),
    [PlayStation 4](../Page/PlayStation_4.md "wikilink"), [Xbox
    One](../Page/Xbox_One.md "wikilink"), [PlayStation
    3](../Page/PlayStation_3.md "wikilink"), [Xbox
    360](../Page/Xbox_360.md "wikilink"),
    [Wii](../Page/Wii.md "wikilink"), [PlayStation
    2](../Page/PlayStation_2.md "wikilink"), [PlayStation
    Vita](../Page/PlayStation_Vita.md "wikilink"), [Nintendo
    3DS](../Page/Nintendo_3DS.md "wikilink"), [PlayStation
    Portable](../Page/PlayStation_Portable.md "wikilink"),
    [iOS](../Page/iOS.md "wikilink"), [Windows
    Phone](../Page/Windows_Phone.md "wikilink"),
    [Android](../Page/Android_（operating_system）.md "wikilink"), [Java
    ME](../Page/Java_ME.md "wikilink")
  - 发售日期: 2013年9月24日（美国）2013年9月23日（欧洲）

《FIFA14》在2013年9月下旬上市，梅西继续担任此部作品的代言人。支持的平台有[PlayStation
2](../Page/PlayStation_2.md "wikilink")， [PlayStation
3](../Page/PlayStation_3.md "wikilink")，[PlayStation
4](../Page/PlayStation_4.md "wikilink")，[PSP](../Page/PSP.md "wikilink")，[PlayStation
Vita](../Page/PlayStation_Vita.md "wikilink")，[Xbox
360](../Page/Xbox_360.md "wikilink")，[Xbox
One](../Page/Xbox_One.md "wikilink")，[Wii](../Page/Wii.md "wikilink")，[Nintendo
3DS](../Page/Nintendo_3DS.md "wikilink") 和 [Microsoft
Windows](../Page/Microsoft_Windows.md "wikilink")，.同样放出了适合手机的版本，支持[iOS](../Page/iOS.md "wikilink")，并且重新支持[Android操作系统](../Page/Android.md "wikilink")。在PlayStation
4和Xbox One上本作采用了全新的[Ignite引擎](../Page/Ignite.md "wikilink")，
Ignite引擎融合了EA的全新的技术，通过全新的渲染，实时物理，动画，智能，运动和网上系统，构成了一个全新的、强力的引擎。EA表示，该引擎能够赋予游戏中运动员“类人的智慧”，玩家会体验到真正的运动员一样的变速和转向的敏捷性。

#### *FIFA 15*

  - 代言人: [Lionel Messi](../Page/Lionel_Messi.md "wikilink") （全球）, [Eden
    Hazard](../Page/Eden_Hazard.md "wikilink") （英国，爱尔兰共和国，法国，比利时，荷兰）,
    [Gonzalo Higuaín](../Page/Gonzalo_Higuaín.md "wikilink") （意大利）,
    [Clint Dempsey](../Page/Clint_Dempsey.md "wikilink") （美国）, [Tim
    Cahill](../Page/Tim_Cahill.md "wikilink") （澳大利亚）, [Robert
    Lewandowski](../Page/Robert_Lewandowski.md "wikilink") （波兰）, [David
    Alaba](../Page/David_Alaba.md "wikilink") （奥地利）, [Xherdan
    Shaqiri](../Page/Xherdan_Shaqiri.md "wikilink") （Switzerland）,
    [Javier Hernández](../Page/Javier_Hernández.md "wikilink") （Mexico）,
    [Arturo Vidal](../Page/Arturo_Vidal.md "wikilink") （南非）, [Michal
    Kadlec](../Page/Michal_Kadlec.md "wikilink") （Czech Republic）, [Arda
    Turan](../Page/Arda_Turan.md "wikilink") （土耳其）, [Atsuto
    Uchida](../Page/Atsuto_Uchida.md "wikilink") （日本）, [Yahya
    Al-Shehri](../Page/Yahya_Al-Shehri.md "wikilink") （Arabian
    Peninsula）.
  - 发售平台: [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink"),
    [PlayStation 3](../Page/PlayStation_3.md "wikilink"), [PlayStation
    4](../Page/PlayStation_4.md "wikilink"), [PlayStation
    Vita](../Page/PlayStation_Vita.md "wikilink"), [Nintendo
    3DS](../Page/Nintendo_3DS.md "wikilink"),
    [Wii](../Page/Wii.md "wikilink"), [iOS](../Page/iOS.md "wikilink"),
    [Android](../Page/Android_（operating_system）.md "wikilink"), [Xbox
    360](../Page/Xbox_360.md "wikilink"), [Xbox
    One](../Page/Xbox_One.md "wikilink"), [Windows
    8.1](../Page/Windows_8.1.md "wikilink"), [Windows Phone
    8.1](../Page/Windows_Phone_8.1.md "wikilink")
  - 发售日期: 2014年9月23日（美国），2014年9月25日（欧洲），2014年9月26日（英国）

#### *FIFA 16*

  - 代言人: [Lionel Messi](../Page/Lionel_Messi.md "wikilink") （全球）,
    [Jordan Henderson](../Page/Jordan_Henderson.md "wikilink")
    （英国，爱尔兰共和国）, [Shinji
    Kagawa](../Page/Shinji_Kagawa.md "wikilink") （日本）, [David
    Alaba](../Page/David_Alaba.md "wikilink") （奥地利）,
    [Oscar](../Page/Oscar_\(footballer,_born_1991\).md "wikilink") （巴西）,
    [Antoine Griezmann](../Page/Antoine_Griezmann.md "wikilink") （法国）,
    [Eden Hazard](../Page/Eden_Hazard.md "wikilink") (Belgium), [Yann
    Sommer](../Page/Yann_Sommer.md "wikilink") (Switzerland), [Juan
    Cuadrado](../Page/Juan_Cuadrado.md "wikilink") (Latin America),
    [Marco Fabián](../Page/Marco_Fabián.md "wikilink") (Mexico), [Steph
    Catley](../Page/Steph_Catley.md "wikilink") & [Tim
    Cahill](../Page/Tim_Cahill.md "wikilink") （澳大利亚）, [Alex
    Morgan](../Page/Alex_Morgan.md "wikilink") （美国）, [Christine
    Sinclair](../Page/Christine_Sinclair.md "wikilink") (Canada), [Mauro
    Icardi](../Page/Mauro_Icardi.md "wikilink") （意大利）, [Arkadiusz
    Milik](../Page/Arkadiusz_Milik.md "wikilink") （波兰）, [Arda
    Turan](../Page/Arda_Turan.md "wikilink") （土耳其）, [Omar
    Hawsawi](../Page/Omar_Hawsawi.md "wikilink") （阿拉伯国家）, [Sebastian
    Giovinco](../Page/Sebastian_Giovinco.md "wikilink") (美国职业足球大联盟版本)
  - 发售平台: [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink"),
    [PlayStation 3](../Page/PlayStation_3.md "wikilink"), [PlayStation
    4](../Page/PlayStation_4.md "wikilink"), [Xbox
    360](../Page/Xbox_360.md "wikilink"), [Xbox
    One](../Page/Xbox_One.md "wikilink"),
    [iOS](../Page/iOS.md "wikilink"),
    [Android](../Page/Android_\(operating_system\).md "wikilink")
  - 发售日期: 2015年9月22日（北美），2015年9月24日（欧洲），2015年10月1日（巴西），2015年10月8日（日本）

#### *FIFA 17*

  - 代言人: [Marco Reus](../Page/Marco_Reus.md "wikilink") (World, chosen
    by popular vote over [Anthony
    Martial](../Page/Anthony_Martial.md "wikilink"), [Eden
    Hazard](../Page/Eden_Hazard.md "wikilink") and [James
    Rodríguez](../Page/James_Rodríguez.md "wikilink"))
  - 发售平台: [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink"),
    [PlayStation 3](../Page/PlayStation_3.md "wikilink"), [PlayStation
    4](../Page/PlayStation_4.md "wikilink"), [Xbox
    360](../Page/Xbox_360.md "wikilink"), [Xbox
    One](../Page/Xbox_One.md "wikilink")
  - 发售日期: 2016年9月27日（北美），2016年9月29日（全球）

《FIFA 17》[馬高·列奧斯担任此部作品的代言人](../Page/馬高·列奧斯.md "wikilink")。在[PlayStation
4和](../Page/PlayStation_4.md "wikilink")[Xbox
One上本作采用了全新的引擎寒霜引擎](../Page/Xbox_One.md "wikilink")（Frostbite
Engine，即《[Battlefield
4](../Page/戰地風雲4.md "wikilink")》引擎）开发，Frostbite引擎融合了EA的全新的技术，通过全新的渲染，实时物理，动画，智能，运动和网上系统，构成了一个全新的、强力的引擎。EA表示，该引擎能够赋予游戏中运动员“类人的智慧”，玩家会体验到真正的运动员一样的变速和转向的敏捷性。《FIFA
17》更加入了类似《NBA 2K16》「My Career」的剧情模式「The
Journey」。在此模式下，玩家可以自创或使用现实既有的球员，去体验一次他们的球员生涯，从球场到球场外的生活应有尽有。

#### FIFA手机游戏（*FIFA Mobile*）

  - 代言人: [Marco Reus](../Page/Marco_Reus.md "wikilink")(October 11, 2016
    – December 23, 2016)；[Eden
    Hazard](../Page/Eden_Hazard.md "wikilink")(December 24, 2016 –
    October 31, 2017)；[Cristiano
    Ronaldo](../Page/Cristiano_Ronaldo.md "wikilink")(November 1, 2017 -
    present)
  - 发售平台: [iOS](../Page/iOS.md "wikilink"),
    [Android](../Page/Android_\(operating_system\).md "wikilink"),
    [Windows 10 Mobile](../Page/Windows_10_Mobile.md "wikilink")
  - 发售日期: 2016年10月11日

#### *FIFA 18*

  - 代言人: [Cristiano Ronaldo](../Page/Cristiano_Ronaldo.md "wikilink")
    （全球） & [Ronaldo](../Page/Ronaldo.md "wikilink") (Icon Edition)
  - 发售平台: [Microsoft Windows](../Page/Microsoft_Windows.md "wikilink"),
    [PlayStation 4](../Page/PlayStation_4.md "wikilink"), [Xbox
    One](../Page/Xbox_One.md "wikilink"), [Nintendo
    Switch](../Page/Nintendo_Switch.md "wikilink"), [PlayStation
    3](../Page/PlayStation_3.md "wikilink") and [Xbox
    360](../Page/Xbox_360.md "wikilink").\[1\]
  - 发售日期: 2017年9月29日

《[FIFA
18](../Page/FIFA_18.md "wikilink")》[基斯坦奴·朗拿度担任此部作品的代言人](../Page/基斯坦奴·朗拿度.md "wikilink")。將會在
Xbox One、PS4 及 PC 平台上繼續採用「Frostbite」寒霜引擎 ，而新加入的 [Nintendo
Switch](../Page/Nintendo_Switch.md "wikilink") 則採用其他引擎開發。今年《FIFA
18》擁有更逼真的臨場氣氛、球場及球員的細緻度均有所提高，而且更引入新玩家控制模式及新的團隊風格，增強了遊戲的可玩性。此外，廠方指出球員的人物特性將更進一步，透過找來球星們進行度身的
motion capture，像 C罗標誌性奔跑、斯特林獨特的轉身或基士文的特別都能夠如實還原出來，不論控球在腳或是無球走動時均會更加真實。

《FIFA 17》加入的剧情模式「The Journey」來到《FIFA 18》，緊接上集 「The Journey」 故事贏得 FA CUP
之後亦來到續集: The Journey 2 : Hunter Returns。玩家將會繼續使用 Alex Hunter
在職業生涯中尋找更多機會，拍其進行重大的轉會決定、比賽、訓練等。而且今集更可以替主角轉換髮型、顏色、紋身等等，讓玩家可以像
BE A PRO 般自訂球員外形\! 值得一提，在 FIFA 18 玩家可以和朋友一起進行 Local 主機上的多人遊戲，玩法更加靈活。
FIFA FUT ICON
一直是最受玩家歡迎的遊戲模式之一，而今集更加入多位超級經典球星坐陣，包括:巴西的朗拿度、比利，阿根廷的馬勒當拿、法國的亨利、以及超經典、被喻為史上最強門將「八爪魚」耶辛等等。除此之外，Ultimate
Team、開球模式、地方賽季、職業生涯、在線賽季、錦標賽、女子世界盃和技能挑戰在內的模式均可以在 FIFA 18
中找到，滿足不同足球粉絲的口味。

#### *FIFA 19*

  - 发售平台:[Microsoft Windows](../Page/Microsoft_Windows.md "wikilink"),
    [PlayStation 4](../Page/PlayStation_4.md "wikilink"), [Xbox
    One](../Page/Xbox_One.md "wikilink"), [Nintendo
    Switch](../Page/Nintendo_Switch.md "wikilink"), [PlayStation
    3](../Page/PlayStation_3.md "wikilink") and [Xbox
    360](../Page/Xbox_360.md "wikilink")
  - 发售日期: 2018年9月28日

《[FIFA
19](../Page/FIFA_19.md "wikilink")》[基斯坦奴·朗拿度繼續担任此部作品的代言人](../Page/基斯坦奴·朗拿度.md "wikilink")。將會在
[Xbox
One](../Page/Xbox_One.md "wikilink")、[PS4](../Page/PS4.md "wikilink")
及 PC 平台上繼續採用「Frostbite」寒霜引擎开发 ，而新加入的 [Nintendo
Switch](../Page/Nintendo_Switch.md "wikilink") 則採用其他引擎開發。E3
2018展前發佈會就確認傳聞,
正式公佈[歐洲聯賽冠軍盃賽事將會加入](../Page/歐洲聯賽冠軍盃.md "wikilink")《FIFA
19》 ,
並同時公开相关宣传片以及推出时间。系列首次加入[中国足球协会超级联赛赛事](../Page/中国足球协会超级联赛.md "wikilink")。

## 其他版本

### FIFA网络游戏

2006年，艺电（EA）发布了专门针对亚洲市场的在线游戏版本。

  - *[FIFA Online](../Page/FIFA_Online.md "wikilink")*
  - *[FIFA Online 2](../Page/FIFA_Online_2.md "wikilink")*
  - *[FIFA Online （western
    version）](../Page/FIFA_Online_（western_version）.md "wikilink")*
  - *[FIFA Online 3](../Page/FIFA_Online_3.md "wikilink")*
  - *[FIFA Online 4](../Page/FIFA_Online_4.md "wikilink")*

### FIFA世界杯（FIFA World Cup licensed games）

1997年，艺电（EA）从[FIFA购买了版权](../Page/FIFA.md "wikilink")，在每一届世界杯比赛开始之前发布FIFA官方世界杯游戏，艺电（EA）到目前依然持有该版权。

  - *[World Cup 98](../Page/World_Cup_98_（video_game）.md "wikilink")*
  - *[2002 FIFA World
    Cup](../Page/2002_FIFA_World_Cup_（video_game）.md "wikilink")*
  - *[2006 FIFA World
    Cup](../Page/2006_FIFA_World_Cup_（video_game）.md "wikilink")*
  - *[2010 FIFA World Cup South
    Africa](../Page/2010_FIFA_World_Cup_South_Africa_（video_game）.md "wikilink")*
  - *[2014 FIFA World Cup
    Brazil](../Page/2014_FIFA_World_Cup_Brazil_（video_game）.md "wikilink")*
  - *[2018 FIFA World Cup](../Page/2018_FIFA_World_Cup.md "wikilink")
    （作为[FIFA 18的免费更新](../Page/FIFA_18.md "wikilink")）*

### 欧洲冠军杯（UEFA European Championship licensed games）

同“FIFA世界杯”游戏，2000年，艺电（EA）从[欧洲足球协会联盟购买了版权](../Page/UEFA.md "wikilink")，在每一届欧洲冠军杯比赛开始之前发布UEFA官方欧洲冠军杯游戏。

  - *[UEFA Euro
    2000](../Page/UEFA_Euro_2000_（video_game）.md "wikilink")*
  - *[UEFA Euro
    2004](../Page/UEFA_Euro_2004_（video_game）.md "wikilink")*
  - ''[UEFA Euro
    2008](../Page/UEFA_Euro_2008_（video_game）.md "wikilink")
  - *[UEFA Euro
    2012](../Page/UEFA_Euro_2012_（video_game）.md "wikilink")*（不是一个独立游戏,而是[FIFA
    12的扩展包](../Page/FIFA_12.md "wikilink")）

### 欧洲冠军联赛（UEFA Champions League licensed games）

  - *UEFA Champions League 2004–2005*
  - *[UEFA Champions League
    2006–2007](../Page/UEFA_Champions_League_2006–2007.md "wikilink")*
  - *[FIFA 19](../Page/FIFA_19.md "wikilink")*

### 街头足球（Street football games）

  - *[FIFA Street](../Page/FIFA_Street_（2005_video_game）.md "wikilink")*
    （2005）
  - *[FIFA Street 2](../Page/FIFA_Street_2.md "wikilink")* （2006）
  - *[FIFA Street 3](../Page/FIFA_Street_3.md "wikilink")* （2008）
  - *[FIFA Street](../Page/FIFA_Street_（2012_video_game）.md "wikilink")*
    （2012）

### 经营游戏（Management games）

  - *[FIFA Manager
    series](../Page/FIFA_Manager_（video_game_series）.md "wikilink")*
      - *[FIFA Soccer
        Manager](../Page/FIFA_Soccer_Manager.md "wikilink")* （1997）
      - *[The FA Premier League Football Manager
        99](../Page/The_FA_Premier_League_Football_Manager_99.md "wikilink")*
      - *[The FA Premier League Football Manager
        2000](../Page/The_FA_Premier_League_Football_Manager_2000.md "wikilink")*
      - *[The FA Premier League Football Manager
        2001](../Page/The_FA_Premier_League_Football_Manager_2001.md "wikilink")*
      - *[The FA Premier League Football Manager
        2002](../Page/The_FA_Premier_League_Football_Manager_2002.md "wikilink")*
      - *[Total Club Manager
        2003](../Page/Total_Club_Manager_2003.md "wikilink")*
      - *[Total Club Manager
        2004](../Page/Total_Club_Manager_2004.md "wikilink")*
      - *[Total Club Manager
        2005](../Page/Total_Club_Manager_2005.md "wikilink")*
      - *[FIFA Manager 06](../Page/FIFA_Manager_06.md "wikilink")*
      - *[FIFA Manager 07](../Page/FIFA_Manager_07.md "wikilink")*
      - *[FIFA Manager 08](../Page/FIFA_Manager_08.md "wikilink")*
      - *[FIFA Manager 09](../Page/FIFA_Manager_09.md "wikilink")*
      - *[FIFA Manager 10](../Page/FIFA_Manager_10.md "wikilink")*
      - *[FIFA Manager 11](../Page/FIFA_Manager_11.md "wikilink")*
      - *[FIFA Manager 12](../Page/FIFA_Manager_12.md "wikilink")*
      - *[FIFA Manager 13](../Page/FIFA_Manager_13.md "wikilink")*
      - *[FIFA Manager 14](../Page/FIFA_Manager_14.md "wikilink")*

## 参见

  - *[Pro Evolution Soccer](../Page/Pro_Evolution_Soccer.md "wikilink")*
  - [Electronic Arts](../Page/Electronic_Arts.md "wikilink")

## 参考文献

## 外部链接

  -
  - [EA Sports
    Football](https://web.archive.org/web/20101004235609/http://www.ea.com/uk/football/)

[Category:1993年首发的电子游戏系列](../Category/1993年首发的电子游戏系列.md "wikilink")
[FIFA系列](../Category/FIFA系列.md "wikilink")
[Category:足球游戏](../Category/足球游戏.md "wikilink") [Category:EA
Sports游戏](../Category/EA_Sports游戏.md "wikilink")
[Category:电子竞技游戏](../Category/电子竞技游戏.md "wikilink")
[Category:国际足球联合会](../Category/国际足球联合会.md "wikilink")

1.