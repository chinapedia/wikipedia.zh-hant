[福澳村](../Page/福澳村.md "wikilink"){{.w}}[清水村](../Page/清水村_\(連江縣\).md "wikilink")<small><small>（鄉公所所在地）</small></small>{{.w}}[仁愛村](../Page/仁愛村_\(連江縣\).md "wikilink"){{.w}}[津沙村](../Page/津沙村.md "wikilink"){{.w}}[珠螺村](../Page/珠螺村.md "wikilink"){{.w}}[馬祖村](../Page/馬祖村.md "wikilink"){{.w}}[四維村](../Page/四維村.md "wikilink")

`|group2 = `[`北竿鄉`](../Page/北竿鄉.md "wikilink")
`| list2 = `[`塘岐村`](../Page/塘岐村.md "wikilink")<small><small>`（鄉公所所在地）`</small></small>`{{.w}}`[`-{后沃村}-`](../Page/后沃村.md "wikilink")`{{.w}}`[`橋仔村`](../Page/橋仔村.md "wikilink")`{{.w}}`[`芹壁村`](../Page/芹壁村.md "wikilink")`{{.w}}`[`上村`](../Page/上村_\(北竿鄉\).md "wikilink")`{{.w}}`[`坂里村`](../Page/坂里村.md "wikilink")`{{.w}}`[`白沙村`](../Page/白沙村_\(連江縣\).md "wikilink")
`|group3 = `[`莒光鄉`](../Page/莒光鄉.md "wikilink")
`| list3 = `[`西坵村`](../Page/西坵村.md "wikilink")`{{.w}}`[`田沃村`](../Page/田沃村.md "wikilink")
` |group2 = `[`東莒島`](../Page/東莒島.md "wikilink")
` | list2 = `[`大坪村`](../Page/大坪村.md "wikilink")`{{.w}}`[`福正村`](../Page/福正村.md "wikilink")
` }}`
`|group4 = `[`東引鄉`](../Page/東引鄉.md "wikilink")
`| list4 = `[`中柳村`](../Page/中柳村.md "wikilink")<small><small>`（鄉公所所在地）`</small></small>`{{.w}}`[`樂華村`](../Page/樂華村_\(臺灣\).md "wikilink")

}} }} <includeonly></includeonly><noinclude> </noinclude>

[Category:連江縣行政區劃](../Category/連江縣行政區劃.md "wikilink")
[Category:連江縣模板](../Category/連江縣模板.md "wikilink")
[](../Category/台灣行政區劃模板.md "wikilink")