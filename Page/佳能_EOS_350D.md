**佳能
EOS-350D**（2005年3月上市）是[佳能公司生產的](../Page/佳能.md "wikilink")[數碼單鏡反光相機](../Page/數碼單鏡反光相機.md "wikilink")，又稱為**EOS
Kiss Digital N**（[日本市場](../Page/日本.md "wikilink")）或**Digital Rebel
XT**（[北美市場](../Page/北美.md "wikilink")）。

## 參見

  - [佳能 EOS 300D](../Page/佳能_EOS_300D.md "wikilink")
  - [佳能 EOS 400D](../Page/佳能_EOS_400D.md "wikilink")
  - [佳能 EOS](../Page/佳能_EOS.md "wikilink")
  - [尼康 D70](../Page/尼康_D70.md "wikilink")

## 外部連結

  - 官方網頁

      - [日本佳能官方網頁](http://web.canon.jp/Imaging/eosdigital2/index.html)

      - [香港佳能官方網頁](http://www.canon.com.hk/tc/Product/Product.aspx?product_id=10169)

  - [Flickr](../Page/Flickr.md "wikilink"): [Camera Finder: Canon: EOS
    Digital Rebel
    XT](http://www.flickr.com/cameras/canon/eos_digital_rebel_xt/)

[Category:数码单反相机](../Category/数码单反相机.md "wikilink")
[Category:佳能相機](../Category/佳能相機.md "wikilink")
[Category:2008年面世的相機](../Category/2008年面世的相機.md "wikilink")