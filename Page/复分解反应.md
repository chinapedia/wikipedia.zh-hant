**複分解反应**又稱**雙置換反應**，是由两种[化合物](../Page/化合物.md "wikilink")，通過互相交换成分並生成两种新化合物的[反应](../Page/化学反应.md "wikilink")，模式为AB+CD→AD+CB。必发生在[水溶液中](../Page/水溶液.md "wikilink")，它是基本类型的[化学反应之一](../Page/化学反应.md "wikilink")。複分解都不是[氧化还原反应](../Page/氧化还原反应.md "wikilink")（有些反应是複分解产物再发生氧化还原，而不是複分解的结果）。


[Metathesis_reaction_Chemical_equation.png](https://zh.wikipedia.org/wiki/File:Metathesis_reaction_Chemical_equation.png "fig:Metathesis_reaction_Chemical_equation.png")

<center>

<div style="font-size:92%">

[硝酸银](../Page/硝酸银.md "wikilink")＋[盐酸](../Page/盐酸.md "wikilink")→[硝酸](../Page/硝酸.md "wikilink")＋[氯化银](../Page/氯化银.md "wikilink")↓

</div>

</center>

上图是一个复分解反应示例。图中的各种物质组成[元素](../Page/元素.md "wikilink")、[原子团的](../Page/原子团.md "wikilink")[化合价在反应前后保持不变](../Page/化合价.md "wikilink")。

有机化学中的类似反应为[取代反应](../Page/取代反应.md "wikilink")。

## 发生条件

### [鹼性氧化物加](../Page/鹼性氧化物.md "wikilink")[酸](../Page/酸.md "wikilink")

<chem>Na2O + 2HCl -\> 2NaCl + H2O</chem>

<chem>Fe2O3 + 6HCl -\> 2FeCl3 + 3H2O</chem>
([鹽酸除](../Page/鹽酸.md "wikilink")[鐵鏽](../Page/鐵鏽.md "wikilink")）

### [酸加](../Page/酸.md "wikilink")[鹼](../Page/鹼.md "wikilink")

也就是[中和反應](../Page/中和反應.md "wikilink")

<chem>NaOH + HCl -\> NaCl + H2O</chem>

### [酸加](../Page/酸.md "wikilink")[鹽](../Page/鹽.md "wikilink")

<chem>2HCl + CaCO3 -\> CaCl2 + H2O + CO2 ^</chem>

### [鹼加](../Page/鹼.md "wikilink")[鹽](../Page/鹽.md "wikilink")

<chem>2NaOH + CuSO4 -\> Na2SO4 + Cu(OH)_2 v</chem>

### [鹽加](../Page/鹽.md "wikilink")[鹽](../Page/鹽.md "wikilink")

生成物至少有一種沉澱(除非產生氣體)

<chem>Na2CO3 + CaCl2 -\> 2NaCl + CaCO3 v</chem>

## 无机複分解反应

溶液中複分解发生条件：可从设想的生成物而定，如生成物有[气体](../Page/气体.md "wikilink")、[沉淀](../Page/沉淀.md "wikilink")、[弱电解质](../Page/弱电解质.md "wikilink")、配（络）合物生成时，则複分解反应一般能进行。即有产物脱离体系时，复分解反应一般能进行。

### 離子反應

例1：[HCl](../Page/HCl.md "wikilink")(*aq*) +
[KOH](../Page/KOH.md "wikilink")(*aq*) →
[KCl](../Page/KCl.md "wikilink")(*aq*) +
[H<sub>2</sub>O](../Page/H2O.md "wikilink")(*ℓ*)

H<sup>+</sup>與OH<sup>−</sup>結合生成弱電解質水，所以反應發生

H<sup>+</sup> + OH<sup>−</sup> → H<sub>2</sub>O
([H<sub>3</sub>O<sup>+</sup>](../Page/H3O+.md "wikilink") +
OH<sup>−</sup> → 2 H<sub>2</sub>O)

K<sup>+</sup>與Cl<sup>−</sup>留在溶液中，所以說生成了KCl與H<sub>2</sub>O

例2：[AgNO<sub>3</sub>](../Page/AgNO3.md "wikilink")(*aq*) +
[HCl](../Page/HCl.md "wikilink")(*aq*) →
[AgCl](../Page/AgCl.md "wikilink")(*s*)↓ +
[HNO<sub>3</sub>](../Page/HNO3.md "wikilink")(*aq*)

Ag<sup>+</sup>與Cl<sup>−</sup>結合生成的AgCl几乎无法溶于水中，以固体形态存在，所以反應發生

<chem>Ag+ + Cl- -\> AgCl v</chem>

### 氣體生成

例1：<chem>{H3PO4({\\it aq})} + 3NaCl({\\it s}) -\>\[\\Delta\] 3HCl({\\it
g}) \\uparrow + Na3PO4({\\it aq})</chem>

加熱，溫度升高，沸點低的HCl氣體大部分脫離反應體系，反應發生。

例2：<chem>{CH3COONa({\\it s})} + NaOH({\\it s}) -\>\[\\Delta\] {CH4({\\it
g})} + Na2CO3({\\it s})</chem>

同上

例3：[CaCO<sub>3</sub>](../Page/CaCO3.md "wikilink")(*s*) + 2
[HCl](../Page/HCl.md "wikilink")(*aq*) →
[CaCl<sub>2</sub>](../Page/CaCl2.md "wikilink")(*aq*) +
[H<sub>2</sub>CO<sub>3</sub>](../Page/H2CO3.md "wikilink")(*aq*)

首先通過離子反應生成弱電解質H<sub>2</sub>CO<sub>3</sub>（強酸制弱酸，但有例外），由于H<sub>2</sub>CO<sub>3</sub>极不稳定，分解產生CO<sub>2</sub>氣體：

[H<sub>2</sub>CO<sub>3</sub>](../Page/H2CO3.md "wikilink")(*aq*) →
[H<sub>2</sub>O](../Page/H2O.md "wikilink")(*ℓ*) +
[CO<sub>2</sub>](../Page/CO2.md "wikilink")(*g*)↑

## 烯烃複分解反应

透過研究[金屬卡賓在](../Page/金屬卡賓.md "wikilink")[有機化合物生成中所扮演的角式而優化](../Page/有機化合物.md "wikilink")[有機化合物的生產過程](../Page/有機化合物.md "wikilink")。由於生產過程中讓[金屬擔當](../Page/金屬.md "wikilink")[催化劑的角色](../Page/催化劑.md "wikilink")，使這些有機化合物無需要再透過利用[卤化過程來達至產品的生成](../Page/卤化.md "wikilink")，從而使有機化合物的生產過程可以更[環保](../Page/環保.md "wikilink")。[伊夫·肖萬](../Page/伊夫·肖萬.md "wikilink")、[羅伯特·格拉布及](../Page/羅伯特·格拉布.md "wikilink")[理查德·施罗克憑這個研究獲得](../Page/理查德·施罗克.md "wikilink")2005年度[諾貝爾化學獎](../Page/諾貝爾化學獎.md "wikilink")。

## 参见

  - [化合反应](../Page/化合反应.md "wikilink")
  - [分解反应](../Page/分解反应.md "wikilink")
  - [置换反应](../Page/置换反应.md "wikilink")

[F](../Category/化学反應.md "wikilink")