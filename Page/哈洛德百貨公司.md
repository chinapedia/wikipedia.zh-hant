**哈洛德百貨**（）是一家位於[英國](../Page/英國.md "wikilink")[倫敦騎士橋的](../Page/倫敦.md "wikilink")[百貨商店](../Page/百貨商店.md "wikilink")，擁有近二百年歷史。它現由旗下的「卡塔爾控股公司」經營\[1\]。哈洛德的品牌名也用於哈洛德集團旗下的哈洛德銀行（）
、哈洛德不動產公司（）、哈洛德航空（及Air Harrods）等企業，以及哈洛德布宜諾斯艾利斯（，由哈洛德集團出售於1922年）\[2\]。

該百貨店鋪占地，擁有330個銷售部門，涵蓋一百萬平方呎()的銷售空間。

哈洛德的[銘言是](../Page/座右銘.md "wikilink")*Omnia Omnibus
Ubique*，即是[拉丁文的](../Page/拉丁语.md "wikilink")“萬事萬物，無處不在”。
其中的幾個部門，包括季節性的聖誕部門和食品廳最為眾人周知。

## 歷史

[Harrods_1909.jpg](https://zh.wikipedia.org/wiki/File:Harrods_1909.jpg "fig:Harrods_1909.jpg")
哈洛德百貨在1834年於[倫敦的](../Page/倫敦.md "wikilink")[東區](../Page/倫敦東區.md "wikilink")，當時創立者查爾斯·亨利·哈洛德（Charles
Henry
Harrod，1799–1885）對[茶相當有興趣](../Page/茶.md "wikilink")，因而在[斯特普尼成立了一](../Page/斯特普尼_\(倫敦\).md "wikilink")[批發商](../Page/批發.md "wikilink")。1849年，為了遠離城市中心的髒亂，並把握1851年在[海德公园舉辦的](../Page/海德公园.md "wikilink")[萬國工業博覽會所帶來的商機](../Page/萬國工業博覽會.md "wikilink")，哈洛德便頂下騎士橋附近的一個小商店，也就是現今百貨所在的位置。開始經營時，店鋪只有一個房間，只雇用兩名助理和一名傳信者，但哈洛德的兒子查爾斯·迪比·哈洛德（Charles
Digby
Harrod）仍將生意發展的有聲有色，販賣[藥品](../Page/药物.md "wikilink")、[香水](../Page/香水.md "wikilink")、[文具](../Page/文具.md "wikilink")、[水果和](../Page/果实.md "wikilink")[蔬菜](../Page/蔬菜.md "wikilink")，此後哈洛德百貨迅速的擴張，並收購相鄰的建築，雇用了100位的員工。

然而，百貨竄紅帶來的財富在1883年12月上旬的一場大火一夕成空，。驚訝的是，災禍過後，查爾斯·哈洛德仍向顧客保證[耶誕節前夕仍有代客送禮的服務](../Page/圣诞节.md "wikilink")，也因此賺了一大筆錢。不久之後，新的建築在原址重建，在諸多重要的顧客的心中建立起良好的公司形象，如作家[奥斯卡·王尔德](../Page/奥斯卡·王尔德.md "wikilink")、傳奇女演員[莉莉·兰特里和](../Page/莉莉·兰特里.md "wikilink")[爱伦·泰瑞](../Page/爱伦·泰瑞.md "wikilink")、男演員[诺埃尔·科沃德](../Page/诺埃尔·科沃德.md "wikilink")、精神學家[西格蒙德·弗洛伊德](../Page/西格蒙德·弗洛伊德.md "wikilink")、作家A.
A. Milne，和其他許多的[英國皇室家族成員](../Page/英國王室.md "wikilink")。

1898年，哈洛德百貨建立了世界第一座[手扶梯](../Page/電動扶梯.md "wikilink")，並提供[白兰地酒以舒緩顧客搭乘手扶梯時的緊張感](../Page/白兰地.md "wikilink")。

### 重要歷程

[HarrodsDay.jpg](https://zh.wikipedia.org/wiki/File:HarrodsDay.jpg "fig:HarrodsDay.jpg")

  - 1834年，查爾斯·亨利·哈洛德（1799－1885）在[倫敦東區創立一間批發雜貨店](../Page/倫敦東區.md "wikilink")。
  - 1849年，批發店搬遷至[骑士桥](../Page/骑士桥.md "wikilink")，靠近[海德公园](../Page/海德公园.md "wikilink")。
  - 1861年，百貨由哈洛德之子查爾斯·迪比·哈洛德（1841－1905）接手後，大幅度的重整。
  - 1883年12月6日，一場大火燒掉了店鋪，哈洛德家族因而有機會新建一個更大的建築。
  - 1889年，查爾斯·迪比·哈洛德退休，同時百貨正式登入[伦敦证券交易所](../Page/伦敦证券交易所.md "wikilink")，名為「哈洛德商店股份有限公司」（Harrod's
    Stores Limited）。
  - 1905年，由查爾斯·威廉·史蒂芬斯設計的新建築完工\[3\]。
  - 1912年，開設唯一的海外分店，位在[阿根廷的首都](../Page/阿根廷.md "wikilink")[布宜諾斯艾利斯](../Page/布宜諾斯艾利斯.md "wikilink")，在1940年代晚期時與脫離母店獨立，仍使用「哈洛德」名稱。
  - 1914年，購併Regent街上的Dickins & Jones百貨。
  - 1919年，購併[曼彻斯特的Kendals百貨](../Page/曼彻斯特.md "wikilink")，1920年代使用哈洛德名稱不久後，因顧客和員工抗議，又換回Kendals。\[4\]
  - 1959年，由英國的House of Fraser百貨控股公司併購。
  - 1983年，一顆炸彈遭到引爆，六個人不幸喪命。
  - 1985年，百貨由Fayed兄弟以6億1500萬[英镑買下](../Page/英镑.md "wikilink")。
  - 1994年，House of Fraser和哈洛德間的關係惡化，百貨仍由Fayed家族持有，House of
    Fraser登上[伦敦证券交易所](../Page/伦敦证券交易所.md "wikilink")。
  - 1997年，英國法院命令位在[布宜諾斯艾利斯的已獨立的哈洛德百貨停止使用哈洛德名稱](../Page/布宜諾斯艾利斯.md "wikilink")。\[5\]
  - 2006年，哈洛德102百貨在總店旁開設，內有[Krispy
    Kreme甜甜圈專賣店](../Page/Krispy_Kreme.md "wikilink")、Yo\!
    Sushi壽司店，以及花店、藥草店、按摩店和活氧[spa店](../Page/spa.md "wikilink")。
  - 2006年，Mohamed最年輕的兒子Omar Fayed進入哈洛德百貨工作。
  - 2009至10年，在新加坡國際機場1號客運大樓及2號客運大樓以授權型式各開設1間商店專賣「Harrods」品牌產品。
  - 2010年5月8日，埃及商人阿法耶茲以15億英鎊（約170億港元）出售哈洛德（Harrods）給。將成為百貨公司第五名擁有者。

## 皇家認證

[Harrods'_Egyptian_room.JPG](https://zh.wikipedia.org/wiki/File:Harrods'_Egyptian_room.JPG "fig:Harrods'_Egyptian_room.JPG")風格色彩，百貨內的許多內部裝潢採用古埃及的主題以呈現持有人的傳統\]\]

哈洛德百貨擁有以下英國[皇家認證](../Page/皇家認證.md "wikilink")：

  - [伊丽莎白二世](../Page/伊丽莎白二世.md "wikilink")：家居用品
  - [菲利普親王 (愛丁堡公爵)](../Page/爱丁堡公爵菲利普亲王.md "wikilink")：服飾
  - [威爾士親王](../Page/威爾士親王.md "wikilink")：服飾和[馬鞍](../Page/馬鞍.md "wikilink")
  - [伊麗莎白·鮑斯-萊昂 (伊莉莎白王太后)](../Page/伊麗莎白·鮑斯-萊昂.md "wikilink")：瓷器和玻璃製品

哈洛德百貨自1956年以來就持有愛丁堡公爵的認證，但於2001年12月21日遭菲利普親王撤銷，因為百貨與公爵的貿易關係大幅衰落。

百貨持有人Al
Fayed接著就將百貨內所有皇室認證的標誌全部移除，但其他獲頒的皇室認證仍然有效。自1997年[黛安娜王妃去世後](../Page/黛安娜王妃.md "wikilink")，從未有認證授與人在哈洛德百貨消費過。

## 產品和服務

哈洛德百貨的330個部門提供了多元化的產品和服務。產品包括服飾（男女裝、童裝、嬰兒裝）、電器、珠寶、運動器材、結婚禮服、寵物和周邊產品、玩具、飲食、健康美容產品、禮品、文具、家居用品、家電、家具等等。

百貨內服務設施包括：28間餐廳（提供[下午茶](../Page/下午茶.md "wikilink")、pub料理、西式料理等等）、預約式的尊貴個人購物服務
(By Appointment Personal Shopping &
Beyond)、鐘錶修復、服裝訂製、藥妝部、美容spa和[沙龍](../Page/沙龍.md "wikilink")、理髮廳、哈洛德金融服務、哈洛德銀行、私人活動據點、食物遞送、個人蛋糕訂製、個人香水調配等等。

另外，百貨內還有其他知名店鋪，如[Turnbull &
Asser](../Page/Turnbull_&_Asser.md "wikilink")、[HMV](../Page/HMV.md "wikilink")、[Waterstones](../Page/Waterstones.md "wikilink")、[Krispy
Kreme和David](../Page/Krispy_Kreme.md "wikilink") Clulow Opticians。

### 百貨之最

  - 旺季時，超過30萬的顧客會拜訪此百貨。
  - 多達5000名員工，來自超過50個不同的國家。
  - 50輛遞送服務的車，每年創造達225,000的遞送次數。
  - 約使用11,500個節能燈泡，每天更換300個。

## 紀念碑

[112407-Harrods-DiannaDodiMemorial2.jpg](https://zh.wikipedia.org/wiki/File:112407-Harrods-DiannaDodiMemorial2.jpg "fig:112407-Harrods-DiannaDodiMemorial2.jpg")
[112407-Harrods-DiannaDodiMemorial1.jpg](https://zh.wikipedia.org/wiki/File:112407-Harrods-DiannaDodiMemorial1.jpg "fig:112407-Harrods-DiannaDodiMemorial1.jpg")

自[黛安娜王妃和](../Page/黛安娜王妃.md "wikilink")[多迪·法耶茲](../Page/多迪·法耶茲.md "wikilink")（Mohamed
Al Fayed的兒子）去世後，Mohamed Al
Fayed在百貨內設立兩個紀念碑以紀念這對情侶。第一座紀念碑在1998年4月12日落成，紀念碑上有：兩人的照片、有黛安娜王妃唇印的酒杯、Dodi在去世前一天買的[訂婚戒指](../Page/訂婚.md "wikilink")。\[6\]

第二座紀念碑在2005年完成，擺設在第三道門旁帶有埃及風格的手扶梯旁，為一[銅雕像](../Page/銅.md "wikilink")，稱做「無辜的受害者」（Innocent
Victims），描繪兩人在沙灘漫舞的情景，天空還有一群[信天翁相伴](../Page/信天翁.md "wikilink")，而信天翁代表的就是「聖靈」（Holy
Spirit）。\[7\]雕像由一位年近80的雕刻師比爾·米契爾完成，他是Al Fayed的密友，擔任哈洛德百貨的美術設計顧問已達40餘年之久。

Al Fayed表示，雕像的目的是要讓兩人的靈魂長存。\[8\]

## 參見

  - [梅西百貨：一家美國的大型百貨店](../Page/梅西百貨.md "wikilink")

## 延伸閱讀

  -
  -
## 參考資料

<div class="references-small">

<references />

</div>

  - *[Guinness World
    Records](../Page/Guinness_World_Records.md "wikilink") 2007*,
    published by Guinness（August 8, 2006）, ISBN 978-1904994121

## 外部連結

  - [官方網站](https://www.harrods.com)

  - [Harrods store
    elevation](http://www.streetsensation.co.uk/sights/harrods.htm)

  - [Harrods來自英國百年品牌](http://www.skm.com.tw/harrods/)

  -
  -
  -
[Category:英國百貨公司](../Category/英國百貨公司.md "wikilink")
[Category:倫敦建築物](../Category/倫敦建築物.md "wikilink")
[Category:倫敦旅遊景點](../Category/倫敦旅遊景點.md "wikilink")
[Category:英國皇家御用企業](../Category/英國皇家御用企業.md "wikilink")
[Category:1834年成立的公司](../Category/1834年成立的公司.md "wikilink")

1.  [Harrods](http://www.harrods.com/HarrodsStore/Default.aspx?gclid=CPS9_sveu5QCFROA1QodijGwTQ)
2.  [La Nación newspaper, Buenos Aires, Harrods, the return of an icon
    of Bienos Aires,
    April 2010](http://www.lanacion.com.ar/nota.asp?nota_id=1255536)
3.  [Trevor Square Area: The Estate
    since 1909](http://www.british-history.ac.uk/report.aspx?compid=45917)
4.  [Kendals name dropped
    forever](http://www.manchestereveningnews.co.uk/news/s/179/179681_kendals_name_dropped_forever.html)

5.  [Harrods wins domain name dispute](http://www.out-law.com/page-1776)
6.  Rick Steves, [Getting Up To Snuff In
    London](http://www.ricksteves.com/plan/destinations/britain/london.htm),
    /www.ricksteves.com.
7.  [Harrods unveils Diana, Dodi
    statue](http://edition.cnn.com/2005/WORLD/europe/09/01/diana.dodi.statue/index.html),
    CNN.com, September 1, 2005.
8.  [Diana bronze unveiled at
    Harrods](http://news.bbc.co.uk/1/hi/england/london/4204364.stm) BBC
    1 September 2005