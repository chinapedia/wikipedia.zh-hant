[缩略图](https://zh.wikipedia.org/wiki/File:Burshaski-lang.png "fig:缩略图")
**布魯夏斯基語**（），即**勃律语**，是一種[中亞的](../Page/中亞.md "wikilink")[語言](../Page/語言.md "wikilink")，使用該語言的人口約有9.68萬（2004年統計[\[資料來源\]](https://www.ethnologue.com/language/bsk)）。此種語言的使用者大部分为[布魯紹人](../Page/布魯紹人.md "wikilink")（Burúšo），布魯紹人當中有5萬到6萬居住在[克什米爾北部的](../Page/克什米爾.md "wikilink")[坎巨提](../Page/坎巨提.md "wikilink")、[那格尔等地的山區](../Page/那格尔.md "wikilink")。由於山區地方偏僻，各地的居民都形成了各自的方言體系。现在的布魯夏斯基語的词汇裡除了[固有名词以外](../Page/固有名词.md "wikilink")，有相當一部分是從[烏爾都語及其他周邊民族的语言而來的](../Page/烏爾都語.md "wikilink")[借詞](../Page/借詞.md "wikilink")。這些借詞，有些來自[達爾德語支](../Page/達爾德語支.md "wikilink")（Dardic
languages）。布魯夏斯基語有三種方言，取名於當地的三個主要山谷。這三種方言分別為：洪扎（Hunza）、那格尔（Nagar）及亚辛（Yasin）。

## 語言分類

在過去，布魯夏斯基語被認為是一種[孤立語言](../Page/孤立語言.md "wikilink")，但如今有說法指出其可能是[葉尼塞語系的一種語言](../Page/葉尼塞語系.md "wikilink")。
根据Berger的说法，《[英國英语词典](../Page/美國傳統英語字典.md "wikilink")》认为单词\*abel
‘apple（苹果）’，[原始印欧语中唯一表示一种水果](../Page/原始印欧语.md "wikilink")（树）的词汇，可能是由布魯夏斯基語的一种源语言中借用而来。在现代布魯夏斯基語中，表示‘苹果’或‘苹果树’的词是báalt。
其他猜想从语言谱系学的角度推断了布魯夏斯基語与[北高加索语系](../Page/北高加索语系.md "wikilink")、[叶尼塞语系](../Page/叶尼塞语系.md "wikilink")、与/或[印欧语系的关系](../Page/印欧语系.md "wikilink")，常常是从[总语系的角度来讨论](../Page/總語系.md "wikilink")。
\*
假说中的[得内-高加索语系以布魯夏斯基語为一个主要分支](../Page/得内-高加索语系.md "wikilink")，此外还有[北高加索语系和](../Page/北高加索语系.md "wikilink")[叶尼塞语系](../Page/叶尼塞语系.md "wikilink")。

  - [卡拉苏克语系](https://en.wikipedia.org/wiki/Karasuk_languages)，一个假想的总语系，包括布魯夏斯基語和北高加索语系作为一个分支的一部分，这两者都与叶尼塞语系有着更远的亲缘关系。
  - [Eric P.
    Hamp](https://en.wikipedia.org/wiki/Eric_P._Hamp)提出了布魯夏斯基語与[印度-赫梯语系](https://en.wikipedia.org/wiki/Indo-Hittite)的一些关联。

## 书写系统

布魯夏斯基語主要的沟通方式是口語交流而非书写。有些时候人们会使用乌尔都语的书写系统，但是并不存在规范的[正字法](../Page/正寫法.md "wikilink")。Adu
Wazir Shafi使用[拉丁转写著述了一本书](../Page/拉丁转写.md "wikilink")，*Burushaski
Razon。*

## 語音系統

有5个元音：/a o i u

|                                       | [双唇音](../Page/双唇音.md "wikilink")               | [龈音](../Page/龈音.md "wikilink") | [龈颚音](../Page/龈颚音.md "wikilink") | [卷舌音](../Page/卷舌音.md "wikilink") | [软腭音](../Page/软腭音.md "wikilink") | [小舌](../Page/小舌.md "wikilink") | 声门音  |
| ------------------------------------- | ---------------------------------------------- | ------------------------------ | -------------------------------- | -------------------------------- | -------------------------------- | ------------------------------ | ---- |
| [鼻音](../Page/鼻音_\(辅音\).md "wikilink") | *m*                                            | *n*                            |                                  |                                  | *ṅ*                              |                                |      |
| [塞音](../Page/塞音.md "wikilink")        | <small> 清送气</small>                            | *ph*                           | *th*                             |                                  | *ṭh*                             | *kh*                           | *qh* |
| <small> 不送气</small>                   | *p*                                            | *t*                            |                                  | *ṭ*                              | *k*                              | *q*                            |      |
| <small>浊不送气</small>                   | *b*                                            | *d*                            |                                  | *ḍ*                              | *g*                              |                                |      |
| [塞擦音](../Page/塞擦音.md "wikilink")      | <small> 清送气</small>                            |                                | *ch*                             | *ćh*                             | *c̣h*                            |                                |      |
| <small> 清不送气 </small>                 |                                                | *c*                            | *ć*                              | *c̣*                             |                                  |                                |      |
| <small>浊不送气</small>                   |                                                |                                | *j*                              | *j̣*                             |                                  |                                |      |
| [擦音](../Page/擦音.md "wikilink")        | <small> [清音](../Page/清音.md "wikilink")</small> |                                | *s*                              | *ś*                              | *ṣ*                              |                                |      |
| <small> 浊音</small>                    |                                                | *z*                            |                                  |                                  |                                  | *ġ*                            |      |
| [颤音](../Page/颤音.md "wikilink")        |                                                | *r*                            |                                  |                                  |                                  |                                |      |
| [边近音](../Page/边近音.md "wikilink")      |                                                | *l*                            | *y*                              | *ỵ*                              | *w*                              |                                |      |

语音细节：

1.  有时发成\[p͡f\]或\[f\]。
2.  有时发成\[q͡χ\]或\[χ\]。
3.  亚辛方言中缺乏此类音，而使用不送气音。
4.  有时发成\[ʑ\]。
5.  有时发成\[ʐ\]。
6.  伯格认为\[w\]、\[j\]是出现在外来语中/i/、/u/之前的变体。
7.  有各種不常見發音。比如\[ʐʲ\]、\[ʐ\]。

## 參考文獻

  - Anderson, Gregory D. S. 1997. Burushaski Morphology. Pages 1021–1041
    in volume 2 of *Morphologies of Asia and Africa*, ed. by Alan Kaye.
    Winona Lake, IN: Eisenbrauns.
  - Anderson, Gregory D. S. 1999. M. Witzel’s "South Asian Substrate
    Languages" from a Burushaski Perspective. *Mother Tongue* (Special
    Issue, October 1999).
  - Anderson, Gregory D. S. forthcoming b. Burushaski. In *Language
    Islands: Isolates and Microfamilies of Eurasia*, ed. by D.A.
    Abondolo. London: Curzon Press.
  - Backstrom, Peter C. *Burushaski* in Backstrom and Radloff (eds.),
    *Languages of northern areas, Sociolinguistic Survey of Northern
    Pakistan, 2. Islamabad*, National Institute of Pakistan Studies,
    Qaid-i-Azam University and [Summer Institute of
    Linguistics](../Page/Summer_Institute_of_Linguistics.md "wikilink")
    (1992), 31-54.
  - Bashir, Elena. 2000. A Thematic Survey of Burushaski Research.
    *History of Language* 6.1: 1–14.
  - Berger, Hermann. 1956. Mittelmeerische Kulturpflanzennamen aus dem
    Burušaski \[Names of Mediterranean cultured plants from B.\].
    *Münchener Studien zur Sprachwissenschaft* 9: 4-33.
  - Berger, Hermann. 1959. Die Burušaski-Lehnwörter in der
    Zigeunersprache \[The B. loanwords in the [Gypsy
    language](../Page/Romani_language.md "wikilink")\]. Indo-Iranian
    Journal 3.1: 17-43.
  - Berger, Hermann. 1974. *Das Yasin-Burushaski (Werchikwar)*. Volume 3
    of *Neuindische Studien*, ed. by Hermann Berger, Lothar Lutze and
    Günther Sontheimer. Wiesbaden: Otto Harrassowitz.
  - Berger, Hermann. 1998. *Die Burushaski-Sprache von Hunza und Nager*
    \[The B. language of H. and N.\]. Three volumes: *Grammatik*
    \[grammar\], *Texte mit Übersetzungen* \[texts with translations\],
    *Wörterbuch* \[dictionary\]. Altogether Volume 13 of *Neuindische
    Studien* (ed. by Hermann Berger, Heidrun Brückner and Lothar Lutze).
    Wiesbaden: Otto Harassowitz.
  - Casule, Ilija. 2010. *Burushaski as an Indo-European language.*
    Languages of the World 38. Munich: Lincom.
  - Casule, Ilija. 2003. Evidence for the Indo-European laryngeals in
    Burushaski and its genetic affiliation with Indo-European. The
    Journal of Indo-European Studies 31:1–2, pp 21–86.
  - van Driem, George. 2001. *Languages of the Himalayas: An
    Ethnolinguistic Handbook of the Greater Himalayan Region, containing
    an Introduction to the Symbiotic Theory of Language* (2 vols.).
    Leiden: Brill.
  - Greenberg, Joseph H., and Merritt Ruhlen. 1992. Linguistic Origins
    of Native Americans. *Scientific American* 267(5): 94–99.
  - Grune, Dick. 1998. [Burushaski – An Extraordinary Language in the
    Karakoram
    Mountains](http://www.few.vu.nl/~dick/Summaries/Languages/Burushaski.pdf).
  - [Lorimer, D. L. R.](../Page/Lorimer,_D._L._R..md "wikilink")
    1935–1938. *The Burushaski Language* (3 vols.). Oslo: Instituttet
    for Sammenlignende Kulturforskning.
  - [Morgenstierne, Georg](../Page/Georg_Morgenstierne.md "wikilink").
    1945. Notes on Burushaski Phonology. *Norsk Tidsskrift for
    Sprogvidenskap* 13: 61–95.
  - Munshi, Sadaf. 2006. *Jammu and Kashmir Burushaski: Language,
    language contact, and change.* Unpublished Ph.D. Dissertation.
    Austin: University of Texas at Austin, Department of Linguistics.
  - van Skyhawk, Hugh. 1996. Libi Kisar. Ein Volksepos im Burushaski von
    Nager. *Asiatische Studien* 133. ISBN 3-447-03849-7.
  - van Skyhawk, Hugh. 2003. Burushaski-Texte aus Hispar. Materialien
    zum Verständnis einer archaischen Bergkultur in Nordpakistan.
    *Beiträge zur Indologie* 38. ISBN 3-447-04645-7.
  - Starostin, Sergei A. 1996. Comments on the Basque–Dene–Caucasian
    Comparisons. *Mother Tongue* 2: 101–109.
  - Tiffou, Étienne. 1993. *Hunza Proverbs*. University of Calgary
    Press. ISBN 1-895176-29-8
  - Tiffou, Étienne. 1999. *Parlons Bourouchaski*. Paris: L'Harmattan.
    ISBN 2-7384-7967-7
  - Tiffou, Étienne. 2000. Current Research in Burushaski: A Survey.
    *History of Language* 6(1): 15–20.
  - Tikkanen, Bertil. 1988. On Burushaski and other ancient substrata in
    northwest South Asia. *Studia Orientalia* 64: 303–325.
  - Varma, Siddheshwar. 1941. Studies in Burushaski Dialectology.
    *Journal of the Royal Asiatic Society of Bengal, Letters* 7:
    133–173.
  - Witzel, Michael. 1999. [Early Sources for South Asian Substrate
    Languages. *Mother Tongue* (Special Issue, October 1999):
    1–70.](http://www.people.fas.harvard.edu/~witzel/MT-Substrates.pdf)

## 参考资料

## 外部連結

  - [Burushaski: An Extraordinary Language in the Karakoram
    Mountains](http://www.few.vu.nl/~dick/Summaries/Languages/Burushaski.pdf)
    (PDF)
  - [Ilija Casule publications on Thraco-Phrygian and Balto-Slavic
    connections of
    Burushaski](http://www.ling.mq.edu.au/about/staff/casule_ilija/publications.html)
  - [A language profile for Burushaski. Get a detailed look at the
    language, from population to dialects and
    usage.](https://www.ethnologue.com/language/bsk)

[Category:孤立语言](../Category/孤立语言.md "wikilink")
[Category:巴基斯坦語言](../Category/巴基斯坦語言.md "wikilink")