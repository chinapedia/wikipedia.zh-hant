[AVCHD.gif](https://zh.wikipedia.org/wiki/File:AVCHD.gif "fig:AVCHD.gif")
**AVCHD（Advanced Video Codec High
Definition）**是[索尼與](../Page/索尼.md "wikilink")[松下電器於](../Page/松下電器.md "wikilink")2006年5月聯合發表的[高畫質](../Page/高畫質.md "wikilink")[光碟壓縮技術](../Page/光碟.md "wikilink")。AVCHD標準基於[H.264/MPEG-4
AVC](../Page/H.264/MPEG-4_AVC.md "wikilink")[視訊編碼](../Page/視訊編碼.md "wikilink")，支援[480i](../Page/480i.md "wikilink")、[720p](../Page/720p.md "wikilink")、[1080i](../Page/1080i.md "wikilink")、[1080p等格式](../Page/1080p.md "wikilink")，同時支援[杜比數碼](../Page/杜比數碼.md "wikilink")5.1聲道或線性[PCM](../Page/脈衝編碼調變.md "wikilink")
7.1聲道音頻壓縮。

AVCHD使用8[釐米的](../Page/釐米.md "wikilink")[MiniDVD光碟](../Page/MiniDVD.md "wikilink")，單張可存儲大約20分鐘的高解析度視訊內容，今後的雙層和雙面光碟可存儲1[小時以上](../Page/小時.md "wikilink")；而沒有AVCHD編碼的MiniDVD光碟，一般只能存儲30分鐘左右的480i視訊內容。

## 規格

| 影像        |
| --------- |
| 視訊信號      |
| 像素（水平×垂直） |
| 顯示比例      |
| 壓縮方式      |
| 明度取樣頻率    |
| 色度取樣格式    |
| 量化        |
| 聲音        |
| 壓縮方式      |
| 壓縮位元率     |
| 聲道數       |
| 系統        |
| 串流類型      |
| 系統資料流量    |
| 副檔名（通常）   |
| 紀錄媒體      |

### 規格（AVCHD 2.0）

| 影像        |
| --------- |
| 子類型       |
| 像素（水平×垂直） |
| 每秒畫格數     |
| 顯示比例      |
| 壓縮方式      |
| 明度取樣頻率    |
| 色度取樣格式    |
| 量化        |
| 聲音（杜比數碼）  |
| AC-3壓縮方式  |
| AC-3聲道數   |
| AC-3壓縮位元率 |
| 聲音（PCM）   |
| 線性PCM     |
| PCM聲道數    |
| PCM位元率    |
| 系統        |
| 串流類型      |
| 系統資料流量    |
| 副檔名（通常）   |
| 紀錄媒體      |

## 相關條目

  - [H.264/MPEG-4 AVC](../Page/H.264/MPEG-4_AVC.md "wikilink")
  - [視頻編解碼器](../Page/視頻編解碼器.md "wikilink")
  - [Sony](../Page/Sony.md "wikilink")
  - [Panasonic](../Page/Panasonic.md "wikilink")

AVCHD视频处理 [AVCHD converter for mac &
windows](http://www.avchdconverter.net)

[Category:索尼](../Category/索尼.md "wikilink")
[Category:視頻編解碼器](../Category/視頻編解碼器.md "wikilink")