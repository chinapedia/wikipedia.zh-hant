《**Everything**》（意思：一切）是[香港](../Page/香港.md "wikilink")[歌手](../Page/歌手.md "wikilink")[王靖雯的第二張大碟](../Page/王靖雯.md "wikilink")，[粵語專輯](../Page/粵語.md "wikilink")，於1990年6月发行。一共11首歌曲，5首為翻唱。銷量再次突破金唱片\[1\]。

## 唱片版本

  - 首版：[韓國壓盤](../Page/韓國.md "wikilink")，內圈為銀圈。
  - 再版：
      - 《Recall》版：《Recall》是將王菲前3張大碟捆綁發售。
      - 《精彩全記錄》版（首版、再版）：《精彩全記錄》是將王菲前8張大碟捆綁發售。
      - 《從頭認識》版：《從頭認識》是將王菲在新藝寶一共9張粵語大碟捆綁發售。
      - 環球復黑版：2004年8月27日。
      - [黑膠唱片版](../Page/黑膠唱片.md "wikilink")
      - [卡帶](../Page/卡帶.md "wikilink") [錄音帶版](../Page/錄音帶.md "wikilink")

## 曲目

## 獎項

香港：

  - [商業電台的](../Page/商業電台.md "wikilink")[叱咤樂壇流行榜](../Page/叱咤樂壇流行榜.md "wikilink")：
      - 1990年度：「叱咤女歌手銅獎」\[2\]

## 注釋

<div class="references-small" style="-moz-column-count:2; column-count:2;">

<references />

</div>

[Category:王菲音樂專輯](../Category/王菲音樂專輯.md "wikilink")
[Category:1990年音樂專輯](../Category/1990年音樂專輯.md "wikilink")
[Category:新艺宝唱片音乐专辑](../Category/新艺宝唱片音乐专辑.md "wikilink")

1.  2001年2月16日《寓言》日文版中的王菲介紹頁：「」
2.  [商業電台官方網站](http://www1.881903.com/framework/pccs.gateway?url=jsp/lsc/index.jsp&menuID=36&pageURL=/fwcontent/lsc/html/lsc_20021206award1990.htm)