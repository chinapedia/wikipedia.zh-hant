**ALGOL**，名稱源自**演算法語言**（）的[縮寫](../Page/縮寫.md "wikilink")\[1\]，是一族[指令式編程語言](../Page/指令式編程.md "wikilink")，發展於1950年代中期，對許多其它程式語言產生了重大影響。[计算机协会在教科書及學術文章採用此語言做為描述](../Page/计算机协会.md "wikilink")[演算法的標準語法超過三十年](../Page/演算法.md "wikilink")。\[2\]

由大多數近代程式語言皆使用類似ALGOL的語法來看\[3\]，ALGOL可與差不多同時期的[FORTRAN](../Page/FORTRAN.md "wikilink")、[LISP及](../Page/LISP.md "wikilink")[COBOL並列為四大最有影響力的高階語言](../Page/COBOL.md "wikilink")\[4\]。ALGOL被設計用來避免FORTRAN中一些已知的問題，最終引領了許多其它程式語言的興起，包括[PL/I](../Page/PL/I.md "wikilink")、[Simula](../Page/Simula.md "wikilink")、[BCPL](../Page/BCPL.md "wikilink")、[B](../Page/B語言.md "wikilink")、[Pascal及](../Page/Pascal_\(程式語言\).md "wikilink")[C](../Page/C语言.md "wikilink")。

ALGOL引入了程式碼區間，並用`begin`⋯`end`來分隔。它是第一個利用[詞法作用域實作巢狀函式的語言](../Page/作用域.md "wikilink")，也是第一個注重[形式語言定義的語言](../Page/形式語言.md "wikilink")，並在[ALGOL
60報告中引入了](../Page/ALGOL_60.md "wikilink")[巴科斯範式來作為設計語言](../Page/巴科斯範式.md "wikilink")[形式文法的原則](../Page/形式文法.md "wikilink")。

ALGOL主要有三種規格，以初次發表的年份命名：

  - [ALGOL 58](../Page/ALGOL_58.md "wikilink") –
    原提議命名為*IAL*，代表*International Algebraic Language*。
  - [ALGOL 60](../Page/ALGOL_60.md "wikilink") – 在1960年代中期首先實作成*X1 ALGOL
    60*，1963年修訂。\[5\]\[6\]
  - [ALGOL 68](../Page/ALGOL_68.md "wikilink") –
    引入許多新元素，像是可變陣列、切片、平行化及算子識別。1973年修訂。\[7\]

[尼克勞斯·維爾特在發展](../Page/尼克勞斯·維爾特.md "wikilink")[Pascal之前](../Page/Pascal_\(程式語言\).md "wikilink")，在ALGOL
60的基礎下建立了[ALGOL W](../Page/ALGOL_W.md "wikilink")。ALGOL
W本是下一代ALGOL的提議，但ALGOL委員會決定採用更先進複雜的設計，而不是一個簡潔化ALGOL
60。

## ALGOL 58 (IAL)

ALGOL 58沒有I/O機制。

## ALGOL 60 family

1960年1月，[图灵奖获得者](../Page/图灵奖.md "wikilink")：[艾伦·佩利在](../Page/艾伦·佩利.md "wikilink")[巴黎举行的有全世界一流软件专家参加的讨论会上](../Page/巴黎.md "wikilink")，发表了"算法语言Algol
60报告"，确定了程序设计语言Algol
60。Algol60语言的第一个编译器由[艾茲赫爾·戴克斯特拉来实现](../Page/艾茲赫爾·戴克斯特拉.md "wikilink")。1962年，艾伦。佩利又对Algol
60进行了修正。

Algol
60引进了许多新的概念如：[局部性概念](../Page/局部性.md "wikilink")、[动态](../Page/动态.md "wikilink")、[递归](../Page/递归.md "wikilink")、[巴科斯-诺尔范式BNF](../Page/巴科斯范式.md "wikilink")（Backus-Naur
Form）等等。

Algol 60是程序设计语言发展史上的一个里程碑，它标志着程序设计语言成为一门独立的科学学科，并为后来软件自动化及软件可靠性的发展奠定了基础。

## ALGOL 68

ALGOL 68的“Hello, World”

**`begin`**
`  printf(($gl$,"Hello, world!"))`
**`end`**

**[Algol
W](../Page/Algol_W.md "wikilink")**：1966年，[IFIP吸收](../Page/IFIP.md "wikilink")[尼克劳斯·维尔特参加对Algol语言进行完善与扩充的工作小组](../Page/尼克劳斯·维尔特.md "wikilink")。沃思参加进去以后，提交了一份建议书并由[東尼·霍爾等人修改](../Page/東尼·霍爾.md "wikilink")、完善以后形成Algol
W。同时还催生了一个新的语言[PL360](../Page/PL360.md "wikilink")。

## 參考資料

[Category:系統程式語言](../Category/系統程式語言.md "wikilink")
[\*](../Category/ALGOL程式語言家族.md "wikilink")

1.  有人大小寫混用([*Algol 60*](http://www.masswerk.at/algol60/report.htm))，有人用全大寫
    ([*ALGOL68*](http://www.cs.ru.nl/~hubbers/courses/sl1/rr.pdf))，本文採用全大寫*ALGOL*。
2.  [*Collected Algorithms of the ACM*](http://calgo.acm.org/)
    Compressed archives of the algorithms.
    [ACM](../Page/计算机协会.md "wikilink")
3.
4.  ["The ALGOL Programming
    Language"](http://groups.engin.umd.umich.edu/CIS/course.des/cis400/algol/algol.html)
    , University of Michigan-Dearborn
5.
6.
7.