[加裝ITS系統後的EMU600型電聯車.jpg](https://zh.wikipedia.org/wiki/File:加裝ITS系統後的EMU600型電聯車.jpg "fig:加裝ITS系統後的EMU600型電聯車.jpg")
[Daciao_sta_2.jpg](https://zh.wikipedia.org/wiki/File:Daciao_sta_2.jpg "fig:Daciao_sta_2.jpg")
[TRA_EMU_women-only_passenger_coaches_location_sign_at_Taipei_Station_20060531.jpg](https://zh.wikipedia.org/wiki/File:TRA_EMU_women-only_passenger_coaches_location_sign_at_Taipei_Station_20060531.jpg "fig:TRA_EMU_women-only_passenger_coaches_location_sign_at_Taipei_Station_20060531.jpg")
[TRA_EMU_coach_only_for_the_ladies_20060605.jpg](https://zh.wikipedia.org/wiki/File:TRA_EMU_coach_only_for_the_ladies_20060605.jpg "fig:TRA_EMU_coach_only_for_the_ladies_20060605.jpg")

**通勤電聯車，又稱電聯車、通勤電車或電車，**為[台鐵自](../Page/臺灣鐵路管理局.md "wikilink") 1990 年 11
月 10 日起使用的車型，也曾是台鐵的客運車種名稱之一，為現今[區間車之前身](../Page/臺鐵區間車.md "wikilink")。

## 概要

### 沿革

由於[台灣北部區間短途](../Page/北臺灣.md "wikilink")[通勤人口日益增加](../Page/通勤.md "wikilink")，原台鐵之[普通車與](../Page/普快車.md "wikilink")[平快車無法負荷所需](../Page/普快車.md "wikilink")，且無冷氣[空調設備](../Page/空調.md "wikilink")，天氣炎熱時乘車舒適度不佳，[機關車牽引加減速較慢而拉長行車時間](../Page/機關車.md "wikilink")，因此自
1990 年 11 月 10 日起，台鐵首度引進 [EMU400](../Page/台鐵EMU400型電聯車.md "wikilink")
型通勤電聯車行駛於[縱貫線](../Page/縱貫線_\(鐵路\).md "wikilink")[基隆至](../Page/基隆車站.md "wikilink")[新竹間路段](../Page/新竹車站.md "wikilink")（少數班次延長至三義、臺中），以取代原本老舊不堪的普通車與平快車。

EMU400 型通勤電聯車參考原 [SP32850](../Page/SP32850.md "wikilink")
型通勤客車之配置，上下車門大而且車門多，可大幅節省上下車的時間，對於[通勤旅客有很大的幫助](../Page/通勤.md "wikilink")；加上列車加減速度快而且有空調，大幅節省通勤旅客的時間、改善乘車品質，上路之後立即獲得社會大眾的青睞。

因通勤電車的推出受到好評，台鐵在 1995 年的 810 輛新車購車專案時，決定大量增購
[EMU500](../Page/台鐵EMU500型電聯車.md "wikilink") 型通勤電聯車 86 組 344
輛（現存 340＋2 輛），為此[西部幹線的普通車大量被電車取代](../Page/西部幹線.md "wikilink")。1999 年
12
月再度以[宜蘭線](../Page/宜蘭線.md "wikilink")、[北迴線電化的名義再度購入](../Page/北迴線.md "wikilink")
[EMU600](../Page/台鐵EMU600型電聯車.md "wikilink") 型電聯車 56 輛。

[臺鐵捷運化中](../Page/臺鐵捷運化.md "wikilink")，有預計仿效都市中的[捷運系統](../Page/捷運.md "wikilink")，設置新車站與新路線並購入通勤電聯車作為開闢新客源的計畫，但是已購的通勤電車數量仍感不足，於是在
2005 年 3 月向[台灣車輛採購](../Page/台灣車輛.md "wikilink") 160 輛新車，即為
[EMU700](../Page/台鐵EMU700型電聯車.md "wikilink") 型，於 2007 年逐步上路。

2011
年，台鐵因應[六家線](../Page/六家線.md "wikilink")、[沙崙線通車](../Page/沙崙線.md "wikilink")、[臺東線電氣化以及西部幹線](../Page/臺東線.md "wikilink")[捷運化政策](../Page/臺鐵捷運化.md "wikilink")，加上近年來中短途旅運量大幅成長、在原有車輛數已不敷使用情況下，再度購置的新一批
74 組 296 輛電聯車，為 EMU800。

2015 年 6 月 9 日因為日幣貶值關係，使得當初的採購 EMU800 型列車經費因匯差而產生餘款，台鐵宣布將再增購EMU800 型列車共計
12 組 48 輛。增購之後將從原本的 37 列 74 組增為 43 列 86 組，EMU800 所有編組 2017 年 5 月 2 3
日投入營運。

2016年，配合臺鐵整體購置及汰換車輛計畫(104-113年)的推動，開始辦理520輛通勤電聯車之採購作業，初期分成8輛編組的160輛與10輛編組的320輛兩標案進行招標，但因為招標過程不順利，經檢討後將兩案合併，以10輛編組520輛重新進行招標。

2018 年 6 月 4
日，520輛通勤電聯車採購案在第二次開標時，由唯一投標廠商[現代Rotem得標](../Page/現代Rotem.md "wikilink")，並預定於正式簽約後兩年內開始交車，預定2024年全數完成交付。

### 營運概況

台鐵的各大通勤區間均可見其蹤跡，列車基本[編成為](../Page/編成.md "wikilink") 4 輛一組，視實際的運量可以連掛 2
組（8輛）至 3 組（12輛）運行。在北部及南部通勤區間大多為 2
組編成，中部及宜蘭、花蓮則為單組，運行時每站均停靠。不過台鐵為了紓解[連續假期的旅客需求](../Page/連續假期.md "wikilink")，會使用通勤電聯車行駛[區間快車](../Page/台鐵區間快車.md "wikilink")，中途小站不停，以達到大量快速運輸的效益。有時[對號列車發生事故或故障也會以通勤電聯車代為跑完全程](../Page/對號列車.md "wikilink")。

通勤電聯車的票價為[復興](../Page/復興號列車.md "wikilink")／區間等級，每公里為[新台幣](../Page/新台幣.md "wikilink")
1.46 元。坐椅配置為長條椅（EMU400～600、EMU800 後續增購編組）或非字形椅（EMU700、EMU800 原購置之 74
組）。除了 EMU400
型車側自動門為二個之外，其餘款式的車廂兩側皆設有三個自動門，並設有[身心障礙專用乘車空間](../Page/身心障礙.md "wikilink")。

### 女性優先車廂

自
2006年6月1日起，台鐵於通勤電聯車內試辦「[女性專用車廂](../Page/女性專用車廂.md "wikilink")」服務，多於列車的中間或其中一端車廂，並以勸導的方式希望民眾配合，但無強制性的處罰措施。因實施配套措施不足，導致效果不佳，加上社會大眾對此措施仍有疑慮，故試辦半年期滿後宣佈不再實施。

但是在2010年5月26日晨間，於[屏東車站到](../Page/屏東車站.md "wikilink")[枋寮車站之間的區間車上發生男性乘客性侵高中女學生的案件](../Page/枋寮車站.md "wikilink")，因此在5月28日時，台鐵又緊急指定晨間、夜間各級列車之第1節車廂為[女性優先車廂](../Page/女性專用車廂.md "wikilink")（通勤電聯車於西幹線南下端的第1節車廂的車窗，張貼「晨（夜）間
女性優先車廂」告示）\[1\]\[2\]\[3\]。

## 使用車型

### EMU400

[TRA_EMU400_in_DingPu_Station.jpg](https://zh.wikipedia.org/wiki/File:TRA_EMU400_in_DingPu_Station.jpg "fig:TRA_EMU400_in_DingPu_Station.jpg")

  - 已停用
  - 輛數：12 組 48 輛
  - 製造商：南非[聯邦鐵路客貨車公司](../Page/聯邦鐵路客貨車公司.md "wikilink")（UCW）
  - 電機系列：[英國通用電氣](../Page/英國通用電氣.md "wikilink")（GEC）
  - 轉向架（bogie）：（不明）
  - 編組：4 輛一組（EMC-ET-EP-EM\[4\]，兩端皆有駕駛室，最大可 3 組 12 輛連掛）
  - 最高時速：110 km/h（設計時速為 120 km/h）
  - 輸出：1920 kW／2574 HP
  - 製造年份：1990 年

### EMU500

[TRA_EMU502_leaving_Dasi_Station_20060630.jpg](https://zh.wikipedia.org/wiki/File:TRA_EMU502_leaving_Dasi_Station_20060630.jpg "fig:TRA_EMU502_leaving_Dasi_Station_20060630.jpg")

  - 輛數：86 組 344 輛，現有 85 組 340＋2 輛，EMU508
    報廢，詳見[臺鐵大里車站事故](../Page/臺鐵大里車站事故.md "wikilink")。
  - 製造商：韓國大宇重工（DAEWOO Heavy Industries）
  - 電機系列：[德國西門子](../Page/西門子公司.md "wikilink")（SIEMENS SIBAS16R）
  - 轉向架：ABB（動力車：P3-1a；無動力車：T3-18b）
  - 編組：4 輛一組（EMC-EP-ET-EM\[5\]，兩端皆有駕駛室，最大可 4 組 16 輛連掛）
  - 最高時速：110 km/h
  - 輸出：2000 kW／2681 HP
  - 製造年份：1995～1997 年

### EMU600

[加裝ITS系統後的EMU600型電聯車.jpg](https://zh.wikipedia.org/wiki/File:加裝ITS系統後的EMU600型電聯車.jpg "fig:加裝ITS系統後的EMU600型電聯車.jpg")

  - 輛數：14 組 56 輛
  - 製造商：[ROTEM](../Page/Rotem.md "wikilink") \[6\]
  - 電機系列：[日本東芝](../Page/東芝.md "wikilink")（TOSHIBA Corporation）技術轉移 ROTEM
  - 轉向架：ALSTOM
  - 編組：4 輛一組（EMC-EP-ET-EM\[7\]，兩端皆有駕駛室，最大可 4 組 16 輛連掛）
  - 最高時速：110 km/h
  - 輸出：2000 kW／2681 HP
  - 製造年份：2001～2002 年

### EMU700

[缩略图](https://zh.wikipedia.org/wiki/File:TRA_EMU707_at_Xike_Station_20071231.jpg "fig:缩略图")

  - 數量：40 組 160 輛
  - 製造商：[日本車輛及](../Page/日本車輛.md "wikilink")[台灣車輛](../Page/台灣車輛.md "wikilink")
  - 電機系列：[東芝電機](../Page/東芝.md "wikilink")
  - 編組：4 輛一組（EMC-EP-ET-EM，固定由 2 組對接編成）
  - 轉向架：空氣彈簧無搖枕轉向架
  - 設計最高速度：120 km/h
  - 輸出：2740 kW／5148 HP(兩組編成時)
  - 製造年份：2007年～2008 年

### EMU800

[缩略图](https://zh.wikipedia.org/wiki/File:TRA-EMU800-2013-08-27-0001\(車頭2\).JPG "fig:缩略图")

  - 數量：86 組 344 輛（原先編組 74 組＋增購編組 12 組）
  - 車輛編號：原先編組 EMU801～874；增購編組 EMU881～892）
  - 製造商：[日本車輛製造及](../Page/日本車輛製造.md "wikilink")[台灣車輛](../Page/台灣車輛.md "wikilink")
  - 電機系列：[東芝電機](../Page/東芝.md "wikilink")
  - 轉向架：空氣彈簧式無枕梁轉向架（動力車：ND-741M 型；無動力車：ND-741T 型）
  - 設計最高速度：140 km/h
  - 輸出：3740 kW／4870 HP（兩組編成時）
  - 製造年份：2012年～2017 年

### EMU900

  - 數量：52 列 104 組 520 輛
  - 車輛編號：未知
  - 製造商：[現代Rotem](../Page/現代Rotem.md "wikilink")\[8\]
  - 電機系列：[東芝電機](../Page/東芝.md "wikilink")
  - 轉向架：未知
  - 設計最高速度：140 km/h
  - 輸出：未知
  - 製造年份：預定2020年～2024年

## 相關條目

  - [通勤型電聯車](../Page/通勤型電聯車.md "wikilink")
  - [台鐵區間車](../Page/臺鐵區間車.md "wikilink")
  - [台鐵區間快車](../Page/臺鐵區間快車.md "wikilink")
  - [台鐵電聯車](../Page/臺鐵電聯車.md "wikilink")

## 備註

[Category:台灣鐵路列車](../Category/台灣鐵路列車.md "wikilink")
[+](../Category/臺灣鐵路管理局車輛.md "wikilink")

1.  [臺鐵新聞稿 - 2010.05.28 -
    高中女通勤列車上遭性侵.doc](http://www.railway.gov.tw/news/News_Content.aspx?NTC=H&NewsSN=2145)

2.

3.

4.  通勤電聯車的車輛型式中，EMC為駕駛馬達客車，ET為客車，EP為動力客車，EM為馬達客車（不一定有駕駛艙），ED為駕駛客車

5.
6.  EMU600招標時韓商是以1999年由大宇重工、現代精工、韓進重工等三家公司的鐵路部門合組成的KOROS公司（Korea Rolling
    Stock Company）參與競標,當時主事者多為前大宇重工的員工, 得標後因股權變動再更名為Rotem Company（a
    member of Hyundai Motor
    Group）；在數次招標過程中，現代精工也是有單獨投標,但1999年KOROS成立後，三家母公司即依約定不得再參與任何韓國或海外的車輛標案。

7.
8.