**高永文**醫生（，）\[1\]，生於[香港](../Page/香港.md "wikilink")，籍貫[廣東](../Page/廣東.md "wikilink")[潮州](../Page/潮州.md "wikilink")\[2\]，前香港[食物及衛生局局長](../Page/食物及衛生局局長.md "wikilink")，[骨科專科](../Page/骨科.md "wikilink")[醫生](../Page/醫生.md "wikilink")，[香港醫務委員會委員](../Page/香港醫務委員會.md "wikilink")、[香港防癌會主席](../Page/南朗醫院.md "wikilink")、[香港浸會大學校董會委員](../Page/香港浸會大學.md "wikilink")、[香港紅十字會董事會委員](../Page/香港紅十字會.md "wikilink")、[香港潮商學校校董會會董](../Page/香港潮商學校.md "wikilink")、[新民黨顧問](../Page/新民黨_\(香港\).md "wikilink")、[香港新聲會顧問](../Page/香港新聲會.md "wikilink")、[樂善公益基金會顧問](../Page/樂善公益基金會.md "wikilink")、[亞太無添加餐飲食品發展促進會顧問](../Page/亞太無添加餐飲食品發展促進會.md "wikilink")\[3\]、[中國香港醫療衛生學會會長及](../Page/中國香港醫療衛生學會.md "wikilink")[策略發展委員會委員及](../Page/策略發展委員會.md "wikilink")[中醫中藥發展委員會主席](../Page/中醫中藥發展委員會.md "wikilink")、潮州市政協常委。

高永文在2013年成為[香港大學民意研究計劃成立以來民望最高的局長](../Page/香港大學民意研究計劃.md "wikilink")。他的民望淨值達80%（支持率82%減去反對率2%）。一直到2014年6月公布的一次調查，他的民望淨值仍高踞在77%。

## 簡歷

高永文早年在[北角邨](../Page/北角邨.md "wikilink")\[4\]居住，畢業於昔日位於北角清華街的[聖猶達小學](../Page/聖猶達小學.md "wikilink")後於[皇仁書院畢業](../Page/皇仁書院.md "wikilink")，與[袁國勇為同班同學](../Page/袁國勇.md "wikilink")\[5\]。高永文於1980年在[香港大學醫學院](../Page/香港大學醫學院.md "wikilink")[內外全科醫學士畢業](../Page/內外全科醫學士.md "wikilink")，其後加入[瑪嘉烈醫院](../Page/瑪嘉烈醫院.md "wikilink")，師承[骨科顧問馮醫生](../Page/骨科.md "wikilink")，擔任骨科醫生實習3年，及後以此作為他的醫療專業。

1991年，高永文加入新成立的[香港醫院管理局](../Page/香港醫院管理局.md "wikilink")，任職專業事務及人力資源總監。

1996年，於[HKU
SPACE就讀中醫學課程](../Page/HKU_SPACE.md "wikilink")\[6\]。1997年，於HKU
SPACE就讀中醫診斷學及中草藥學\[7\]。1998年，於HKU SPACE就讀方劑學、中醫婦科學及中醫兒科學\[8\]。

2003年[嚴重急性呼吸系統綜合症肆虐香港期間](../Page/嚴重急性呼吸系統綜合症.md "wikilink")，時任醫院管理局[行政總裁](../Page/行政總裁.md "wikilink")[何兆煒染病入院](../Page/何兆煒.md "wikilink")，高永文臨危受命，署任行政總裁。2004年12月，他因為醫管局主席[梁智鴻請辭而有所觸動](../Page/梁智鴻.md "wikilink")，因此辭職。離職後，高永文與他人合夥開辦骨科診所。

2006年，高永文為[苗圃行動擔任助學大使](../Page/苗圃行動.md "wikilink")；現時為[泰山公德會的](../Page/泰山公德會.md "wikilink")[滅癌獻愛心國際慈善基金會信託人](../Page/滅癌獻愛心國際慈善基金會.md "wikilink")\[9\]。

2012年7月1日，高永文接替[周一嶽擔任](../Page/周一嶽.md "wikilink")[食物及衛生局局長](../Page/食物及衛生局局長.md "wikilink")。任期屆滿離開政府後高永文重新投入私營醫療市場執業。
\[10\]

## 賽跑摔倒血流披面

2015年12月6日，高永文在[上水參加一項跑步賽事](../Page/上水.md "wikilink")，在開跑後僅幾秒，因前面的一名參賽兒童首先跌倒，隨即絆倒高永文，高的眼鏡飛脫，額頭、眼角及嘴角擦傷，血流披面\[11\]，並立即用雙手掩護口鼻，被工作人員攙扶往救護站作初步敷治，及後送往[北區醫院進一步治理](../Page/北區醫院.md "wikilink")，送院時清醒，[食物及衛生局副局長](../Page/食物及衛生局.md "wikilink")[陳肇始及](../Page/陳肇始.md "wikilink")[行政長官](../Page/香港特首.md "wikilink")[梁振英先後到醫院探望](../Page/梁振英.md "wikilink")，陳肇始表示做了電腦掃瞄，他腦部無大礙，但[顴骨裂了](../Page/顴骨.md "wikilink")，額頭縫了數針。高永文在當日中午前已出院，惟需在家休息兩星期。\[12\]

## 醫學專業資格

  - [香港大學內外全科醫學士](../Page/香港大學.md "wikilink")
  - 英國愛丁堡皇家外科醫學院院士
  - 香港醫學專科學院院士
  - 香港醫學專科學院院士（骨科）
  - 香港醫學專科學院院士（社會醫學）

## 個人生活

高永文已婚，育有兩子。妻子江孟儀及兩名兒子偉琛、諾琛均在香港生活，一家4口都是醫生。

## 榮譽

  - 非官守[太平紳士](../Page/太平紳士.md "wikilink")（2001年）
  - [銅紫荊星章](../Page/銅紫荊星章.md "wikilink")（2008年）
  - [金紫荊星章](../Page/金紫荊星章.md "wikilink")（2017年）

## 節目主持

  - 2006年：《[康健DNA](../Page/康健DNA.md "wikilink")》（亞洲電視）
  - 2016年：《[聆聽無聲世界](../Page/聆聽無聲世界.md "wikilink")》客席主持（香港電台，[龍耳製作](../Page/龍耳.md "wikilink")）

## 曾經參與拍攝電視廣告/宣傳段片

  - [無綫電視](../Page/電視廣播有限公司.md "wikilink")《[香港力量](../Page/香港力量.md "wikilink")》宣傳片段（2009年）[1](http://hk.youtube.com/watch?v=GEJ75RazZjA)
  - [亞洲電視](../Page/亞洲電視.md "wikilink")《[ATV2012感動香港人物推選](../Page/ATV2012感動香港人物推選.md "wikilink")》擔任「感動香港大使」（2012年）[2](https://web.archive.org/web/20120501232829/http://www.hkatv.com/v5/12/hklovingheart2012/content.html)

## 注釋

## 參考文獻

## 外部链接

  - [高永文：人类依赖家禽为食
    禽流感病毒难消除](https://web.archive.org/web/20160304142636/http://kfq.ce.cn/sy/gnxw/201405/11/t20140511_1605948.shtml)
    中国经济网 2014-05-16

[Category:香港醫學界人士](../Category/香港醫學界人士.md "wikilink")
[Category:香港醫生](../Category/香港醫生.md "wikilink")
[Category:聖猶達小學校友](../Category/聖猶達小學校友.md "wikilink")
[Category:皇仁書院校友](../Category/皇仁書院校友.md "wikilink")
[Category:香港大學校友](../Category/香港大學校友.md "wikilink")
[Category:新南威爾士大學校友](../Category/新南威爾士大學校友.md "wikilink")
[Category:香港大學專業進修學院校友](../Category/香港大學專業進修學院校友.md "wikilink")
[Category:香港潮汕人](../Category/香港潮汕人.md "wikilink")
[Category:潮州人](../Category/潮州人.md "wikilink")
[W](../Category/高姓.md "wikilink")
[Category:太平紳士](../Category/太平紳士.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:香港建制派人士](../Category/香港建制派人士.md "wikilink")
[Category:前香港行政會議成員](../Category/前香港行政會議成員.md "wikilink")
[Category:前香港政府官員](../Category/前香港政府官員.md "wikilink")
[Category:潮州市政协常务委员](../Category/潮州市政协常务委员.md "wikilink")
[Category:第十三届全国政协委员](../Category/第十三届全国政协委员.md "wikilink")
[Category:醫生出身的政治人物](../Category/醫生出身的政治人物.md "wikilink")

1.  [高永文-登六-搶風頭-議員趁CY餞別宴送驚喜賀壽兼大合照](https://www.hk01.com/%E6%B8%AF%E8%81%9E/98464/%E9%AB%98%E6%B0%B8%E6%96%87-%E7%99%BB%E5%85%AD-%E6%90%B6%E9%A2%A8%E9%A0%AD-%E8%AD%B0%E5%93%A1%E8%B6%81CY%E9%A4%9E%E5%88%A5%E5%AE%B4%E9%80%81%E9%A9%9A%E5%96%9C%E8%B3%80%E5%A3%BD%E5%85%BC%E5%A4%A7%E5%90%88%E7%85%A7)

2.  [潮州老饕
    高永文](http://www.hkheadline.com/culture/culture_content.asp?contid=21895&srctype=g)

3.

4.  [RTHK香港故事第18輯03：由春秧街說起 2011-12-26](http://www.youtube.com/watch?v=AlhEXs80Fgk)

5.

6.  [高永文《天人合一》【HKU SPACE 60週年「活 ‧
    學空間」短片】](https://www.youtube.com/watch?v=HUNbAp8ASd8)

7.
8.
9.  [泰山公德會](http://www.taishan-international.org/profile.html)

10.

11.

12.