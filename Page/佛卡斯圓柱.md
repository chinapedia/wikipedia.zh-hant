[RomaForoRomanoColonnaFoca2.JPG](https://zh.wikipedia.org/wiki/File:RomaForoRomanoColonnaFoca2.JPG "fig:RomaForoRomanoColonnaFoca2.JPG")\]\]

**佛卡斯圓柱**位于[意大利](../Page/意大利.md "wikilink")[罗马的](../Page/罗马.md "wikilink")[古罗马广场](../Page/古羅馬廣場.md "wikilink")，修建於608年8月1日，用以紀念[拜占庭帝國皇帝](../Page/拜占庭帝國.md "wikilink")[佛卡斯](../Page/福卡斯.md "wikilink")。它是罗马广场最后修建的一个部分。看起来像是公元2世纪建成的，高13.6米（44码）的[科林斯式神柱矗立在其正方体乳白色](../Page/科林斯柱式.md "wikilink")[大理石基座上](../Page/大理石.md "wikilink")。砖-{zh-hans:制;
zh-hant:製;}-的[正方形地基](../Page/正方形.md "wikilink")（见右图）本来是看不到的。直到19世纪，罗马广场的地表才开凿到早先[奥古斯都时代的路面水平](../Page/奥古斯都.md "wikilink")。

虽然佛卡斯正式将[万神殿赠与](../Page/万神殿.md "wikilink")[教宗](../Page/教宗.md "wikilink")[博義四世](../Page/博義四世.md "wikilink")，但是修建这个圆柱的确實原因还不清楚。教皇又将万神殿赐予所有的[殉道圣人](../Page/殉道者.md "wikilink")。[圣母玛丽亚在](../Page/圣母玛丽亚.md "wikilink")[中世纪也被列为殉道圣人](../Page/中世纪.md "wikilink")，因此后来万神殿改名成为[圣母与诸殉道者教堂](../Page/圣母与诸殉道者教堂.md "wikilink")。佛卡斯圓柱的顶部曾经竖立着由[拉文纳督主教](../Page/拉文纳督主教.md "wikilink")[斯马拉格达斯制造的](../Page/斯马拉格达斯.md "wikilink")“眩目”的镀金佛卡斯雕像，但可能只存在了很短的时间。610年10月，出身低微的篡位者佛卡斯被叛徒出卖，然后被逮捕、折磨、暗杀、最后被肢解。作为一种“[除忆诅咒](../Page/除忆诅咒.md "wikilink")”（damnatio
memoriae），他在各处的雕像也被推倒。虽然在某些时候被宣称为为了表达教宗的感激之情，圆柱顶部的这个镀金雕像更像是君主统治罗马的权力象征。不过，这种统治很快就在[伦巴底人的压力之下结束了](../Page/伦巴底人.md "wikilink")。这个雕像也表达了斯马拉格达斯的个人感恩。佛卡斯将斯马拉格达斯从长期的放逐状态中召回，并重新给予他在[拉文纳的地位和权力](../Page/拉文纳.md "wikilink")。

佛卡斯圓柱的前身是一根用来安放罗马大帝[戴克里先雕像的圆柱](../Page/戴克里先.md "wikilink")。后来这个铭刻被砍凿掉了，以给现在的雕刻提供空间。

在1813年挖掘時發現圓柱底部有下列文字:

圆柱现在仍在其原位。它孤立的矗立在废墟中，成为罗马广场的一个地标。它也经常出现在[城市景观图以及雕刻中](../Page/城市景观图.md "wikilink")。在18世纪中叶，当[裘塞佩·瓦西和](../Page/裘塞佩·瓦西.md "wikilink")[皮拉内西·吉安巴蒂斯塔创作](../Page/皮拉内西·吉安巴蒂斯塔.md "wikilink")[雕刻和](../Page/雕刻.md "wikilink")[蚀刻的时候](../Page/蚀刻.md "wikilink")，由于侵蚀而导致的地平面上升已经完全埋没了基座。

## 外部链接

  - [雷纳·塞达尔，"佛卡斯圆柱"](http://sights.seindal.dk/photo/10017,s166f.html)

  - [*罗马百科全书*
    "佛卡斯圆柱"](http://penelope.uchicago.edu/~grout/encyclopaedia_romana/romanforum/phocas.html)

  - [克里斯丁·胡森，1906.
    *罗马广场：历史和纪念*](http://penelope.uchicago.edu/Thayer/E/Gazetteer/Places/Europe/Italy/Lazio/Roma/Rome/Forum_Romanum/_Texts/Huelsen*/2/14.html)
    "佛卡斯圆柱"

[Category:纪念柱](../Category/纪念柱.md "wikilink")
[Category:罗马的古罗马建筑物](../Category/罗马的古罗马建筑物.md "wikilink")
[Category:2世紀建立](../Category/2世紀建立.md "wikilink")