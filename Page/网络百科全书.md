**网络百科全书**，是在[互联网上公开给](../Page/互联网.md "wikilink")[網友查阅的](../Page/網友.md "wikilink")[百科全书](../Page/百科全书.md "wikilink")，网络百科有开放和非开放两种。

## 公信力

网络百科全书由于是[共笔性质](../Page/共笔.md "wikilink")，少有专业人士审核，其公信力受到广泛质疑。另外涉及政治、宗教的文章也会受到不同国家、政治立场、语言使用者的影响，导致出现[编辑战](../Page/编辑战.md "wikilink")、[审查或屏蔽](../Page/审查.md "wikilink")。網絡百科的多數編者往往只是義務參與撰寫，不一定是該領域的專家。種種因素都使網絡百科全書的公信力比不上傳統百科全書。

## 网络百科全书

### 开放的网络百科全书

开放的网络百科可以给網友共同查閱、[编辑](../Page/编辑.md "wikilink")，大多為免費性質，开放的网络百科全书有：

<table>
<thead>
<tr class="header">
<th><p>標題文字</p></th>
<th><p>条目</p></th>
<th><p>简介</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/中文维基百科.md" title="wikilink">中文维基百科</a></p></td>
<td><p>条</p></td>
<td><p>漢语种的网络百科全书</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/英文维基百科.md" title="wikilink">英文维基百科</a></p></td>
<td><p>约5,500,000条[1]</p></td>
<td><p>英语种的网络百科全书</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/搜狗百科.md" title="wikilink">搜狗百科</a></p></td>
<td><p>900,000条</p></td>
<td><p><a href="../Page/腾讯网.md" title="wikilink">腾讯网推出的中文网络百科</a>。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/互动百科.md" title="wikilink">互动百科</a></p></td>
<td><p>16,741,930条</p></td>
<td><p>口号“百科更权威”。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/百度百科.md" title="wikilink">百度百科</a></p></td>
<td><p>14,977,147条</p></td>
<td><p>口号“全球最大中文百科全書”，由<a href="../Page/百度.md" title="wikilink">百度營運</a>。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/360百科.md" title="wikilink">360百科</a></p></td>
<td><p>用戶累計貢獻20,250,143次</p></td>
<td><p>口号“让求知更简单”，由<a href="../Page/奇虎360.md" title="wikilink">奇虎360營運</a>。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/维基百科:文言文維基百科.md" title="wikilink">维基大典</a></p></td>
<td><p>範例</p></td>
<td><p><a href="../Page/文言文.md" title="wikilink">文言文版的中文维基百科分支</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/公共百科.md" title="wikilink">公共百科</a></p></td>
<td><p>範例</p></td>
<td><p>简体中文，目前主要介绍肿瘤医学</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/DGSO百科.md" title="wikilink">DGSO百科</a></p></td>
<td><p>範例</p></td>
<td><p>中文，只轉百度百科的內容。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/大众百科.md" title="wikilink">大众百科</a></p></td>
<td><p>範例</p></td>
<td><p>英文，由维基百科共同创办人拉里·桑格创办</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/网络天书.md" title="wikilink">网络天书</a></p></td>
<td><p>範例</p></td>
<td><p>中国大陆最早的Wiki技术网络百科。（已关闭）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/维库.md" title="wikilink">维库</a></p></td>
<td><p>範例</p></td>
<td><p>中国大陆的另一家Wiki技术网络百科。（已关闭）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/凤凰网百科.md" title="wikilink">凤凰网百科</a></p></td>
<td><p>範例</p></td>
<td><p>只有财经、汽车、房产、健康类的百科。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/MBA智库百科.md" title="wikilink">MBA智库百科</a></p></td>
<td><p>範例</p></td>
<td><p>專注於經濟管理領域知識的創建</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Qwiki.md" title="wikilink">Qwiki</a></p></td>
<td><p>範例</p></td>
<td><p>英文，多媒体维基</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/中文百科在线.md" title="wikilink">中文百科在线</a></p></td>
<td><p>範例</p></td>
<td><p>科学、系统、开放的网络百科全书（已损坏）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/華佗百科.md" title="wikilink">華佗百科</a></p></td>
<td><p>範例</p></td>
<td><p>基於wiki技術創立的醫學百科全書</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Knol.md" title="wikilink">Knol</a></p></td>
<td><p>範例</p></td>
<td><p><a href="../Page/Google.md" title="wikilink">Google推出的网络百科</a>。（已关闭）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/金融百科.md" title="wikilink">金融百科</a></p></td>
<td><p>範例</p></td>
<td><p>旨在创造一个涵盖金融所有领域的百科全书。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/A+医学百科.md" title="wikilink">A+医学百科</a></p></td>
<td><p>範例</p></td>
<td><p>医学百科全书。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/萌娘百科.md" title="wikilink">萌娘百科</a></p></td>
<td><p>範例</p></td>
<td><p><a href="../Page/ACG.md" title="wikilink">ACG主题</a><a href="../Page/在线百科全书.md" title="wikilink">在线百科全书</a>。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/邮编库.md" title="wikilink">邮编库</a></p></td>
<td><p>超1,844,219邮编信息</p></td>
<td><p>该网站提供国内、国际邮编查询、反向查询等功能。（GFDL授权）</p></td>
</tr>
</tbody>
</table>

### 非开放的网络百科全书

非开放的网络百科是有[著作權的传统百科全书网络化给網友查阅的网络百科](../Page/著作權.md "wikilink")，有時限於僅[會員查閱與需付費等性質](../Page/會員.md "wikilink")，使用者不能编辑其内容。著名非开放的网络百科全书有：

  - [大不列颠百科全书网络版](../Page/大不列颠百科全书.md "wikilink")
  - [中国大百科全书网络版](../Page/中国大百科全书.md "wikilink")
  - [中华百科全书网络版](../Page/中华百科全书.md "wikilink")

### 地區性的網絡百科全書

  - [台灣大百科全書](../Page/台灣大百科全書.md "wikilink")：收錄一切跟台灣有關的事物，包括社會時事。（已關閉）
  - [台灣百科全書](http://www.zh.pedia.wikia.com/)：收錄一切跟台灣有關的事物，包括社會時事以及網路內容。
  - [香港網絡大典](../Page/香港網絡大典.md "wikilink")：收錄一切跟網絡有關的事物，也包括社會時事，內容主要圍繞香港和東亞。
  - [香港巴士大典](http://hkbus.wikia.com)：收錄香港-{zh-hans:公交;zh-hk:巴士;zh-tw:公車}-、小巴和居民巴士路線、車站、收費和歷史等資料。
  - [香港鐵路大典](http://hkrail.wikia.com)：收錄香港鐵路路線、車站和歷史等資料。

### 其他具有特色的网络百科

  - [偽基百科](../Page/偽基百科.md "wikilink")：一個模仿维基百科的戲謔網站，让[网民收集编写各种恶搞的资料](../Page/网民.md "wikilink")。由于恶搞不能无中生有，所以大多恶搞参考了各种百科的词条，实际上不属于百科全书类别。
  - [萌娘百科](../Page/萌娘百科.md "wikilink")：收录一切有关[ACG的事物](../Page/ACG.md "wikilink")，同时收录[萌拟人化](../Page/萌拟人化.md "wikilink")、[同人作品](../Page/同人.md "wikilink")。
  - [网址百科](../Page/网址百科.md "wikilink")：只分享网址。（已关闭）
  - [百科全说](../Page/百科全说.md "wikilink")：“中国第一文学教育门户—秋千网”旗下频道。（非正式百科）
  - [家事国事网](../Page/家事国事网.md "wikilink")：只有互动百科有所记录的百科，內容不明。（已关闭）
  - [PTT乡民百科](http://zh.pttpedia.wikia.com/wiki/PTT%E9%84%89%E6%B0%91%E7%99%BE%E7%A7%91)：词条特别少，收录內容不太普及的特色文章。（非正式百科）
  - [網路百科 (Fandom)](../Page/網路百科_\(Fandom\).md "wikilink"): 綜合型的線上百科全書。
  - [Everipedia](../Page/Everipedia.md "wikilink"):
    綜合型的線上百科全書，建立於[EOS](../Page/EOS.IO.md "wikilink")[区块链技術上](../Page/区块链.md "wikilink")，擁有超過六百萬個條目。
  - [中医百科](../Page/中医百科.md "wikilink"):
    介绍研究中医相关知识，收集整理中医中药相关资料，交流中医中药相关体验的非盈利性网站。

## 参考文献

## 参见

  - [在线百科全书列表](../Page/在线百科全书列表.md "wikilink")

{{-}}

[在线百科全书](../Category/在线百科全书.md "wikilink")

1.