**R星協**是照亮[反射星雲的星協](../Page/反射星雲.md "wikilink")，這個名稱是[Sidney van den
Bergh在發現這些恆星在星雲內的分布是不均勻之後所建議的](../Page/Sidney_van_den_Bergh.md "wikilink")\[1\]。因為形成這些恆星的分子雲都不大，使這些年輕恆星集團所擁有的[主序星質量都不大](../Page/主序星.md "wikilink")\[2\]，使得天文學家得以觀察附近暗星雲的本質。因為R星協的數量比OB星協更豐富，可以用來追蹤銀河系中螺旋臂的結構\[3\]。[麒麟R2是R星協的一個例子](../Page/麒麟R2.md "wikilink")，距離我們的太陽約830±50[秒差距](../Page/秒差距.md "wikilink")\[4\]。

## 參考資料

[Category:星協](../Category/星協.md "wikilink")

1.

2.

3.

4.