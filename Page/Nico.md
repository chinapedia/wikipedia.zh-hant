[Nico1_-Mutter_Erde_fec.jpg](https://zh.wikipedia.org/wiki/File:Nico1_-Mutter_Erde_fec.jpg "fig:Nico1_-Mutter_Erde_fec.jpg")

**妮可**（，原名克里斯塔·帕夫根，，）是一位[德國歌手與流行音樂歌曲作者](../Page/德國.md "wikilink")，也是一位時裝[模特兒與](../Page/模特兒.md "wikilink")[演員](../Page/演員.md "wikilink")，是與[地下絲絨樂團合作的歌手之一](../Page/地下絲絨.md "wikilink")。

1988年7月18日，在与儿子阿里于地中海上的[伊维萨岛度假期间](../Page/伊维萨岛.md "wikilink")，妮可骑自行车时突然心脏病发作，摔倒导致她头部受伤。一位路过的出租车司机发现她不省人事，艰难地将她送到当地医院。她被误诊为热暴露，当晚8点便去世。X光检查后发现严重的[颅内出血才是导致她死亡的原因](../Page/颅内出血.md "wikilink")。\[1\]

## 参考资料

## 外部連結

  -
  -
  - [Habits of Waste,
    Pt. 1](https://archive.is/20110929231356/http://www.habitsofwaste.wwu.edu/issues/2/iss2art5a.shtml)
    Evaluation of Nico's early work

  - [Visit her burial
    place](http://www.thing.de/blinkface/vr/nicframe.htm) in Berlin,
    Grunewald Forest Cemetery, Grave 81

  - [Nico, the Voice of Disaffected
    Youth](http://www.npr.org/templates/story/story.php?storyId=5044641)
    - Audio story from [National Public
    Radio](../Page/National_Public_Radio.md "wikilink")

  - [Nico](http://www.bbc.co.uk/radio4/womanshour/01/2007_35_thu.shtml)
    (BBC Radio 4 *Woman's Hour* audio item)

  - [1](http://punkcast.com/1183/index.html) Bats for Lashes

[Category:德國歌手](../Category/德國歌手.md "wikilink")
[Category:德國搖滾歌手](../Category/德國搖滾歌手.md "wikilink")
[Category:德國女性模特兒](../Category/德國女性模特兒.md "wikilink")
[Category:德國電影女演員](../Category/德國電影女演員.md "wikilink")
[Category:素食主義者](../Category/素食主義者.md "wikilink")
[Category:北萊因-西發里亞人](../Category/北萊因-西發里亞人.md "wikilink")
[Category:女性搖滾歌手](../Category/女性搖滾歌手.md "wikilink")
[Category:死于神经系统疾病的人](../Category/死于神经系统疾病的人.md "wikilink")

1.