**六鳃鲨科**（学名*Hexanchidae*）是[板鰓亞綱](../Page/板鰓亞綱.md "wikilink")[六鳃鲨目的其中一科](../Page/六鳃鲨目.md "wikilink")。

## 分類

**六鳃鲨科**下分3個屬，如下：

  - [七鳃鲨属](../Page/七鳃鲨属.md "wikilink")(*Heptranchias*)
    <small>[Rafinesque](../Page/Constantine_Samuel_Rafinesque.md "wikilink"),
    1810</small>
      - [尖吻七鳃鲨](../Page/尖吻七鳃鲨.md "wikilink")(*Heptranchias perlo*)
        <small>[Bonnaterre](../Page/Pierre_Joseph_Bonnaterre.md "wikilink"),
        1788</small>
  - [六鳃鲨属](../Page/六鳃鲨属.md "wikilink")(*Hexanchus*) <small>Rafinesque,
    1810</small>
      - [灰六鳃鲨](../Page/灰六鳃鲨.md "wikilink")(*Hexanchus griseus*)
        <small>Bonnaterre, 1788</small>

      - [大眼六鳃鲨](../Page/大眼六鳃鲨.md "wikilink")(*Hexanchus nakamurai*)
        <small>[Teng](../Page/Teng_Huo-Tu.md "wikilink"),
        1962</small>：又稱長吻六鰓鯊。

      - *Hexanchus vitulus* <small>Daly-Engel, 2018</small>\[1\]\[2\]
  - [哈那鲨属](../Page/哈那鲨属.md "wikilink")(*Notorynchus*)
    <small>[Ayres](../Page/William_Orville_Ayres.md "wikilink"),
    1855</small>
      - [扁头哈那鲨](../Page/扁头哈那鲨.md "wikilink")(*Notorynchus cepedianus*)
        <small>[Péron](../Page/弗朗索瓦·佩龍.md "wikilink"),
        1807</small>：又稱油夷鮫。

## 參考文獻

[\*](../Category/六鳃鲨科.md "wikilink")

1.  <https://www.sciencedaily.com/releases/2018/02/180220122928.htm>
2.  <https://phys.org/news/2018-02-species-shark-genetic.html>