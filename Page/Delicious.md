**Delicious**原名del.icio.us，是[免費的](../Page/免費.md "wikilink")[社群性](../Page/社會性軟件.md "wikilink")[網絡服務](../Page/Web服務.md "wikilink")，用於交流網頁書籤网摘。它在2003年底上線，由開發。

Delicious不同於[我的最愛或其他書籤網站](../Page/我的最愛.md "wikilink")，主要用於和別人分享和交流書籤，使用者亦可以在Delicious儲存或管理私人書籤（但需記得勾選"do
not share"）。

|  |  |
|  |  |
|  |  |
|  |  |

Delicious的圖示由四個不同色而同大小的[正方形拼成一個較大的正方形](../Page/正方形.md "wikilink")，左上角為[白色](../Page/白色.md "wikilink")，右上角[藍色](../Page/藍色.md "wikilink")，左下角[黑色](../Page/黑色.md "wikilink")，右下角透明。

Delicious已于2005年12月9日被[Yahoo\!所收购](../Page/Yahoo!.md "wikilink")，但仍然會獨立運作。在2008年中，網站推出2.0版本。新網站運行在新平台中，提高了頁面的載入速度。亦使用了全新的[搜尋引擎](../Page/搜尋引擎.md "wikilink")，令使用者更易找到所需。主域名亦會改為Delicious.com。\[1\]

2010年12月，Yahoo\!宣佈準備關閉Delicious。\[2\]

2011年4月27日，雅虎宣布[YouTube联合创始人](../Page/YouTube.md "wikilink")和[陈士骏对Delicious进行收购](../Page/陈士骏.md "wikilink")，Delicious将并入到由他们创建的[AVOS](../Page/AVOS_Systems.md "wikilink")。\[3\]

2012年3月31日，Delicious宣布正式进入中国，确定中文名为**美味书签**，启动域名http://mei.fm/，目前网站正在公测阶段\[4\]。但在2014年美味书签宣布将在2月15日后关闭网站，用户可以登录网站导出书签文件。

2014年5月，[AVOS将Delicious出售给Science](../Page/AVOS_Systems.md "wikilink")
Inc.\[5\]

## 包含技術

  - [分众分类法](../Page/分众分类法.md "wikilink")
  - 單純的[HTML介面](../Page/HTML.md "wikilink")
  - [REST](../Page/REST.md "wikilink") [API](../Page/API.md "wikilink")
  - [RSS](../Page/RSS.md "wikilink")

## 參見

  - [digg](../Page/digg.md "wikilink")
  - [社会性书签](../Page/社会性书签.md "wikilink")

## 參考資料

## 外部連結

  - [Delicious.com](https://web.archive.org/web/20100304195628/http://delicious.com/)
  - [Delicious網誌](https://web.archive.org/web/20110429093750/http://blog.delicious.com/)
  - [de.lirio.us](http://de.lirio.us)：開放源碼的del.icio.us模仿者

[Category:網站](../Category/網站.md "wikilink")
[Category:雅虎](../Category/雅虎.md "wikilink")
[Category:社群網站](../Category/社群網站.md "wikilink") [Category:Web
2.0](../Category/Web_2.0.md "wikilink")
[Category:2003年建立的网站](../Category/2003年建立的网站.md "wikilink")

1.  [雅虎Delicious书签站升级2.0版](http://news.mydrivers.com/1/112/112807.htm)
2.  [雅虎将关闭多项服务：Delicious和Buzz失宠](http://cnbeta.com/articles/129922.htm)
3.  [YouTube联合创始人收购雅虎Delicious](http://www.cnbeta.com/articles/141272.htm)
4.  [信息服务网站美味书签Delicious正式进入中国](http://tech.sina.com.cn/i/2012-04-01/11046905196.shtml)
5.