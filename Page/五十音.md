**五十音**，又稱**五十音圖**，是將[日語的](../Page/日語.md "wikilink")[假名](../Page/假名.md "wikilink")（[平假名](../Page/平假名.md "wikilink")、[片假名](../Page/片假名.md "wikilink")）以[母音](../Page/母音.md "wikilink")、[子音為分類依據所排列出來的一個圖表](../Page/子音.md "wikilink")。

「50音」、「」的名稱始於[日本](../Page/日本.md "wikilink")[江戶時代](../Page/江戶時代.md "wikilink")，古時候亦有「」、「」、「五音五位之次第」、「」、「」、「」、「」等稱呼。

## 五十音

<table>
<tbody>
<tr class="odd">
<td><p><strong>五十音</strong>（<a href="../Page/平文式羅馬字.md" title="wikilink">平文式羅馬字</a>）</p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td><p>()<br />
n</p></td>
</tr>
<tr class="even">
<td><p><br />
wi</p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p><br />
we</p></td>
</tr>
<tr class="odd">
<td><p><br />
wo</p></td>
</tr>
<tr class="even">
<td></td>
</tr>
</tbody>
</table>



<table>
<tbody>
<tr class="odd">
<td><p><strong>五十音</strong>（<a href="../Page/訓令式羅馬字.md" title="wikilink">訓令式羅馬字</a>）</p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td><p>()<br />
n</p></td>
</tr>
<tr class="even">
<td><p><br />
i</p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p><br />
e</p></td>
</tr>
<tr class="odd">
<td><p><br />
wo</p></td>
</tr>
<tr class="even">
<td></td>
</tr>
</tbody>
</table>

## 五十音的特徵

五十音圖中每行為母音的變化，每列為子音的變化。每行依序稱為、、、、，每列依序稱為、、、、、、、、、。五十音圖通常只包含清音，不包含撥音。

五十音裡一共有的、的、的是重覆的，另外和在現代日語也不再使用，只有古文或是名字中才可能會見到。因此，加上撥音，日常生活所使用的假名只有46個。計上和有48個假名。不過，以發音來說，除了清音、撥音以外，還有濁音、半濁音、長音、促音、拗音等等，總數在100個以上。

## 歷史背景

五十音以段和行作為排序的方式。現代五十音的順序大概是在[室町時代以後形成的](../Page/室町時代.md "wikilink")。

會以這樣的順序排序，其中一個影響是根據當時[梵文的音韻學](../Page/梵文.md "wikilink")——[悉曇學](../Page/悉曇學.md "wikilink")。將當時梵文的母音用拉丁字母表示，會是
a â i ī u û ri ê ai ô au an a' 這樣的順序，對應到日文的順序正好是。的順序會把放到最後也有可能是受到悉曇學的影響。

另一個影響來自中國的[漢語音韻學](../Page/漢語音韻學.md "wikilink")。古代漢語的表音方式為[反切](../Page/反切.md "wikilink")，以這種方法所形成的[五音](../Page/五音（音韻）.md "wikilink")、[清濁](../Page/清濁音.md "wikilink")、[韻書](../Page/韻書.md "wikilink")、[韻圖等等方法也都傳到日本](../Page/韻圖.md "wikilink")。五十音圖最初的提案應為天台宗的僧侶明覺。他的著作《反音作法》利用反切的方法，將同一子音的置於同一行，同一母音的置於同一段。此處母音的順序為，而子音的順序為（喉音）（舌音）（唇音）；子音的排序方法是按照口腔發音的位置由內至外排列（當時的是唇音[{{IPA](../Page/清雙唇擦音.md "wikilink")）。後來將往後挪，除了可能受到悉曇學的影響以外，也有可能是
y r w 有半母音性質的原因。

## 記憶方法

在五十音普及以前，對於假名的記憶方法，有「」「」「」等等。

## 参考文献

## 外部連結

  - [假名一覽圖 (PDF)](http://brng.jp/benri50on.pdf)

## 參見

  - [五十音順](../Page/五十音順.md "wikilink")
  - [悉曇學](../Page/悉曇學.md "wikilink")
  - [伊吕波](../Page/伊吕波.md "wikilink")

[Category:日语假名](../Category/日语假名.md "wikilink")
[假](../Category/日語書寫系統.md "wikilink")