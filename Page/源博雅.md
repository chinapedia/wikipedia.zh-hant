**源博雅**（[延喜](../Page/延喜.md "wikilink")18年－[天元](../Page/天元_\(年號\).md "wikilink")3年九月廿八）是[平安時代的貴族及](../Page/平安時代.md "wikilink")[雅樂家](../Page/日本雅樂.md "wikilink")，有“雅乐之神”之称。他是[克明親王的第一皇子](../Page/克明親王.md "wikilink")、[醍醐天皇之孫](../Page/醍醐天皇.md "wikilink")。他的母親是[藤原時平的女兒](../Page/藤原時平.md "wikilink")。他在日本朝廷的職位最高達到“非[参議從三位皇后宮權大夫](../Page/参議.md "wikilink")”，因此被稱為**博雅三位**。他是[管弦的名手](../Page/日本雅樂#管弦.md "wikilink")。也擅长围棋，被称为“長秋卿”。

他放棄皇室身分降入臣籍，被賜姓「源」，為[醍醐源氏始祖之一](../Page/醍醐源氏.md "wikilink")。934年官敘**從四位下**。947年官拜中務大輔，959年右兵衛督，965年左中將。974年叙官從三位**皇太后宮権大夫**。

擅長雅樂，**郢曲**習自**敦實親王**、[箏習自](../Page/箏.md "wikilink")**醍醐天皇**、[琵琶習自](../Page/琵琶.md "wikilink")**源修**、[笛習自](../Page/笛.md "wikilink")**大石峰吉**、[篳篥則向峰吉之子富門與](../Page/篳篥.md "wikilink")**良峰行正**學習。最擅長**大篳篥**、喜愛舞與歌。

951年时曾在天皇私宴中演奏。966年受[村上天皇敕令編撰](../Page/村上天皇.md "wikilink")「新撰樂譜（長秋卿竹譜）」（別名「博雅笛譜」）。其也是現今日本雅樂仍然演奏的樂曲「長慶子」的作曲者。

## 文學形象

[Suzakumon_no_tsuki.jpg](https://zh.wikipedia.org/wiki/File:Suzakumon_no_tsuki.jpg "fig:Suzakumon_no_tsuki.jpg")門之[鬼对笛](../Page/妖怪.md "wikilink")，选自《月百姿》\]\]
他在**今昔物語**許多故事中出現，如從[朱雀門之](../Page/朱雀門.md "wikilink")[鬼手中獲得名笛](../Page/妖怪.md "wikilink")「葉二」、從[羅城門找回琵琶名器](../Page/羅城門.md "wikilink")「玄象」、與逢坂的[蟬丸相交三年](../Page/蟬丸.md "wikilink")
後獲傳授琵琶秘曲「**流泉**」「**啄木**」等等。

**藤原實資**在日記「[小右記](../Page/小右記.md "wikilink")」中評論道「稠人命云、定頼才能太賢、然而緩怠無極。**如博雅（源）者、博雅文筆・管絃者也。但天下懈怠白物（しれもの）也**、世以相伝、為定頼辛事也。」

在「[陰陽師](../Page/陰陽師_\(小說\).md "wikilink")」（小說：[夢枕獏](../Page/夢枕獏.md "wikilink")、漫畫：[岡野玲子](../Page/岡野玲子.md "wikilink")）中，博雅以主人公[安倍晴明同伴的身分登場](../Page/安倍晴明.md "wikilink")。電影「[陰陽師](../Page/阴阳师_\(电影\).md "wikilink")」、「陰陽師２」中由[伊藤英明擔綱演出博雅一角](../Page/伊藤英明.md "wikilink")。〔注意：史料記載中，安倍晴明與源博雅並沒有確切關聯之證據。〕

[Category:918年出生](../Category/918年出生.md "wikilink")
[Category:980年逝世](../Category/980年逝世.md "wikilink")
[Category:醍醐源氏](../Category/醍醐源氏.md "wikilink")
[Category:平安時代中期貴族](../Category/平安時代中期貴族.md "wikilink")
[Category:平安時代的王](../Category/平安時代的王.md "wikilink")