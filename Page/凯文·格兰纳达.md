[Kevin_Granata_Memorial_Tree.JPG](https://zh.wikipedia.org/wiki/File:Kevin_Granata_Memorial_Tree.JPG "fig:Kevin_Granata_Memorial_Tree.JPG")
**凯文·格兰纳达**（****，）
是位于[美国](../Page/美国.md "wikilink")[弗吉尼亚州黑堡镇的](../Page/弗吉尼亚州.md "wikilink")[弗吉尼亚理工学院暨州立大学工程学系](../Page/弗吉尼亚理工学院暨州立大学.md "wikilink")、机械工程学系的正教授。格兰纳达还在附近的一些高校授课。格兰纳达教授是[2007年弗吉尼亚理工大学校园枪击案](../Page/2007年弗吉尼亚理工大学校园枪击案.md "wikilink")32名罹难者之一。

## 弗吉尼亚理工大学

格兰纳达在弗吉尼亚理工大学的骨骼肌生物力学实验室研究脑瘫的生物力学、运动动力学，他是该领域领先的科研人员之一\[1\]。他最近的研究重点是肌肉和反射控制与有腿机器人、骨骼肌运动的神经肌肉控制、生物力学稳定及肌肉动力学,
防治腰背疼痛的控制、病理性走路及跑步的计算机仿真及临床诠释\[2\]。

## 教育及职位

格兰纳达出生于[俄亥俄州](../Page/俄亥俄州.md "wikilink")[托莱多](../Page/托莱多.md "wikilink")\[3\]。1984年毕业于[俄亥俄州立大学获工程物理学及电子工程学学士学位](../Page/俄亥俄州立大学.md "wikilink")。1986年在普渡大学物理系获物理学硕士学位。1993年在俄亥俄州立大学获生物力学博士\[4\].

## 参考文献

<div class="references-small">

<references/>

</div>

[Category:在美國被謀殺身亡者](../Category/在美國被謀殺身亡者.md "wikilink")
[Category:校園槍擊事件人物](../Category/校園槍擊事件人物.md "wikilink")
[Category:美國科學家](../Category/美國科學家.md "wikilink")
[Category:維吉尼亞大學教師](../Category/維吉尼亞大學教師.md "wikilink")
[Category:俄亥俄州人](../Category/俄亥俄州人.md "wikilink")
[Category:俄亥俄州立大學校友](../Category/俄亥俄州立大學校友.md "wikilink")
[Category:普渡大學校友](../Category/普渡大學校友.md "wikilink")
[Category:被谋杀的教育家](../Category/被谋杀的教育家.md "wikilink")

1.  [TMJ4 local=Va. Shootings: The
    Aftermath](http://www.todaystmj4.com/news/local/7061667.html)
2.  [Toledo Blade. St. Francis de Sales graduate among the victims of
    the Virginia Tech
    rampag。](http://toledoblade.com/apps/pbcs.dll/article?AID=/20070418/NEWS03/70418003/-1/NEWS)
3.  [Toledo Blade. St. Francis de Sales graduate among the victims of
    the Virginia Tech
    rampag。](http://toledoblade.com/apps/pbcs.dll/article?AID=/20070418/NEWS03/70418003/-1/NEWS)
4.  \[<http://www.myfoxcleveland.com/myfox/pages/News/Detail?contentId=2951244&version=1&locale=EN-US&layoutCode=TSTY&pageId=3.2.1>。
    Fox8=Ohio Native Killed in Shooting Spree at Virginia Tech\]