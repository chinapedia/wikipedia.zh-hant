**鄧亮洪**（Tang Liang Hong,
1935年—），[新加坡政治人物](../Page/新加坡.md "wikilink")、律師，曾是反對黨領袖。

## 1997年大選

他曾於1997年以[新加坡工人黨候選人身份參與國會大選](../Page/新加坡工人黨.md "wikilink")，出戰[靜山集選區](../Page/靜山集選區.md "wikilink")，在點票結果中，鄧的候選團隊共獲得45%的選票落選。

鄧亮洪在大選前曾多次遭新加坡媒體指為「反基督教、危險人物、大漢沙文主義者」等，及曾收到死亡恐嚇信，便向警局報案，惟警方不受理有關指控，鄧遂離境前往[馬來西亞](../Page/馬來西亞.md "wikilink")[柔佛州](../Page/柔佛州.md "wikilink")[新山逗留](../Page/新山.md "wikilink")。在大選過後遭國會內閣資政[李光耀](../Page/李光耀.md "wikilink")、總理[吳作棟等](../Page/吳作棟.md "wikilink")11人控告[誹謗](../Page/誹謗.md "wikilink")。除此之外，鄧也曾被稅局指控逃稅，妻子的護照曾遭沒收，及後獲發還，先流亡香港，其後鄧氏前往[澳洲尋求政治庇護](../Page/澳洲.md "wikilink")\[1\]。

### 回憶錄與新加坡選舉

鄧亮洪所著的《與李光耀較量：新加坡異見者鄧亮洪回憶錄》描述了[人民行動黨如何用行政和司法手段限制反對黨](../Page/人民行動黨.md "wikilink")，並操弄選舉制度和規則以確保反對黨無法構成實質性挑戰，這些手段如操縱選舉時間、减少新聞採訪反對黨、針對反對黨勝選選區進行限制公共經費、選區重新劃分等等，指出新加坡選舉的長期不公平制度。\[2\]

## 參考資料

## 參見

  - [李光耀](../Page/李光耀.md "wikilink")
  - [新加坡工人黨](../Page/新加坡工人黨.md "wikilink")

## 外部連結

  - [鄧亮洪論壇](http://www.tangtalk.com)
  - [Life on the run by James Gomez (4
    April 2000)](https://web.archive.org/web/20050915033529/http://www.sfdonline.org/Link%20Pages/Link%20Folders/Gomez/tanggomez.html)
  - [新加坡工人黨歷史:1991至2000](https://web.archive.org/web/20051127033312/http://wp.org.sg/party/history/1991_2000.htm)
  - [赴会华商
    尝鲜72小时过境免签](http://e.chengdu.cn/html/2013-09/27/content_428795.htm)

[Category:新加坡政治人物](../Category/新加坡政治人物.md "wikilink")
[Category:1935年出生](../Category/1935年出生.md "wikilink")
[Category:持不同政见者](../Category/持不同政见者.md "wikilink")
[Category:新加坡律師](../Category/新加坡律師.md "wikilink")
[Category:新加坡華人](../Category/新加坡華人.md "wikilink")
[Category:澳大利亚华人](../Category/澳大利亚华人.md "wikilink")
[L](../Category/邓姓.md "wikilink")
[Category:新加坡南洋大學校友](../Category/新加坡南洋大學校友.md "wikilink")
[Category:新加坡國立大學校友](../Category/新加坡國立大學校友.md "wikilink")

1.  [你所不知的新加坡？](http://www.inmediahk.net/node/1017797)

2.