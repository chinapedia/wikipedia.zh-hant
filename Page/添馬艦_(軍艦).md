[Troopshiptamar.jpg](https://zh.wikipedia.org/wiki/File:Troopshiptamar.jpg "fig:Troopshiptamar.jpg")
[The_Anchor_of_HMS_Tamar.JPG](https://zh.wikipedia.org/wiki/File:The_Anchor_of_HMS_Tamar.JPG "fig:The_Anchor_of_HMS_Tamar.JPG")
[HMS_Tamar.jpg](https://zh.wikipedia.org/wiki/File:HMS_Tamar.jpg "fig:HMS_Tamar.jpg")\]\]

**添馬艦**（）原是[英國皇家海軍的運兵艦](../Page/英國皇家海軍.md "wikilink")\[1\]，於1863年在[英國建造](../Page/英國.md "wikilink")，是一艘排水量4,650噸的[三桅帆船](../Page/三桅帆船.md "wikilink")，並設有[蒸汽機和](../Page/蒸汽機.md "wikilink")[推進器](../Page/推進器.md "wikilink")，擁有自行動力\[2\]。此艦取名自位於英國[康瓦爾郡和](../Page/康瓦爾郡.md "wikilink")[德文郡之間的](../Page/德文郡.md "wikilink")[添馬河](../Page/塔馬河.md "wikilink")，是第四艘使用此名的英國海軍艦隻。

## 歷史

### 建造

添馬艦於1863年6月，在英國[倫敦的造船廠下水](../Page/倫敦.md "wikilink")，作為一艘供運送人員和裝備的運兵艦\[3\]。添馬艦新造時航速為12[海里](../Page/海里.md "wikilink")，其後曾經進行改裝，增加[鍋爐數量](../Page/鍋爐.md "wikilink")，煙囪由原來雙煙囪減少為單煙囪。添馬艦首航於1864年1月12日，前往南非[好望角](../Page/好望角.md "wikilink")，運送英軍往中國。

### 使用

添馬艦在早期曾經活躍於非洲地區，於1874年參與英國在[西非的戰爭](../Page/西非.md "wikilink")，1882年則前往埃及[亞歷山大港支援英軍任務](../Page/亞歷山大港.md "wikilink")\[4\]。添馬艦在1897年第三次抵達[香港後](../Page/香港.md "wikilink")，即留守於[維多利亞港內](../Page/維多利亞港.md "wikilink")，成為[駐港英軍的主力艦](../Page/駐港英軍.md "wikilink")，停泊在[香港島的皇家船塢旁](../Page/香港島.md "wikilink")。

### 沉沒

1941年12月8日，[大日本帝國陸軍越過](../Page/大日本帝國陸軍.md "wikilink")[深圳河邊界從](../Page/深圳河.md "wikilink")[中國大陸入侵香港](../Page/中國大陸.md "wikilink")。12月12日，於[香港保衛戰中失利的](../Page/香港保衛戰.md "wikilink")[駐港英軍退守至香港島](../Page/駐港英軍.md "wikilink")，[港英政府下令炸毀港內所有船隻](../Page/香港殖民地時期#香港政府.md "wikilink")，以免被日軍利用，添馬艦亦因此被炸沉\[5\]。沉沒後，添馬艦的桅桿一直露出水面，但[日佔香港政府直至終戰後都沒有理會](../Page/香港日佔時期.md "wikilink")\[6\]。1947年12月，英國政府外判一間中國海事公司進行打撈殘骸的工程，但添馬艦主體則仍然存於海床，未能打撈\[7\]。

### 紀念

1945年8月[香港重光後](../Page/香港重光.md "wikilink")，添馬艦殘存的木板被撈起，部份被用作建造[聖約翰座堂的大門作為紀念](../Page/聖約翰座堂.md "wikilink")。[船錨則被安放於](../Page/船錨.md "wikilink")[中環的](../Page/中環.md "wikilink")[海軍基地正門處](../Page/海軍基地.md "wikilink")，其後被移至[昂船洲海軍基地](../Page/昂船洲海軍基地.md "wikilink")，1997年移交[香港歷史博物館保存](../Page/香港歷史博物館.md "wikilink")。現時船錨展出於[香港海防博物館所在的](../Page/香港海防博物館.md "wikilink")[鯉魚門渡口炮台外](../Page/鯉魚門.md "wikilink")，[筲箕灣避風塘的水道入口旁](../Page/筲箕灣避風塘.md "wikilink")。

原本停泊添馬艦的[金鐘海旁](../Page/金鐘.md "wikilink")，在填海後被命名為[添馬](../Page/添馬.md "wikilink")（英文：Tamar）以作紀念，而作為前駐港英軍三軍司令部的[威爾斯親王大厦](../Page/威爾斯親王大厦.md "wikilink")，也聯同所在的海軍基地被稱為「添馬艦」。1990年代中期，海軍基地被遷往[昂船洲](../Page/昂船洲.md "wikilink")，[威爾斯親王大厦則於](../Page/威爾斯親王大厦.md "wikilink")1997年7月移交予[解放軍駐港部隊成為](../Page/解放軍駐港部隊.md "wikilink")[中國人民解放軍駐香港部隊大廈](../Page/中國人民解放軍駐香港部隊大廈.md "wikilink")，而原有的海軍基地港池，經填海後建有[香港特區政府總部和](../Page/香港特區政府總部.md "wikilink")[立法會綜合大樓](../Page/立法會綜合大樓.md "wikilink")\[8\]。

### 發現

2015年，在[中環及灣仔填海計劃範圍的](../Page/中環及灣仔填海計劃.md "wikilink")[灣仔海床](../Page/灣仔.md "wikilink")，發現懷疑沉船的金屬殘骸。有學者估計是[香港保衛戰中被炸毀的船隻之一](../Page/香港保衛戰.md "wikilink")，有可能是添馬艦，發展局指出要撈起殘骸後才能確定沉船的身份\[9\]。

## 參見

  - [添馬](../Page/添馬.md "wikilink")
  - [香港海防博物館](../Page/香港海防博物館.md "wikilink")
  - [香港保衛戰](../Page/香港保衛戰.md "wikilink")
  - [添馬艦發展工程](../Page/添馬艦發展工程.md "wikilink")
  - [政府總部 (香港)](../Page/政府總部_\(香港\).md "wikilink")
  - [立法會綜合大樓](../Page/立法會綜合大樓.md "wikilink")

## 參考資料

[category:駐港英軍](../Page/category:駐港英軍.md "wikilink")

[Category:香港船艦](../Category/香港船艦.md "wikilink")

1.

2.  [添馬艦](http://www.hkmemory.org/postcard/record.php?subject=military&acc_no=59&lang=cht)，香港記憶

3.  [Harbour bed holds
    memories](http://www.thestandard.com.hk/news_detail.asp?pp_cat=&art_id=41861&sid=&con_type=1&d_str=19971113&sear_year=1997)
    ，[英文虎報](../Page/英文虎報.md "wikilink")，13 November 1997

4.
5.  [官方首次披露：灣仔海底金屬物體 或是二戰添馬艦殘骸
    當局將移走殘骸續填海](http://news.mingpao.com/ins/%E5%AE%98%E6%96%B9%E9%A6%96%E6%AC%A1%E6%8A%AB%E9%9C%B2%EF%BC%9A%E7%81%A3%E4%BB%94%E6%B5%B7%E5%BA%95%E9%87%91%E5%B1%AC%E7%89%A9%E9%AB%94%20%20%E6%88%96%E6%98%AF%E4%BA%8C%E6%88%B0%E6%B7%BB%E9%A6%AC%E8%89%A6%E6%AE%98%E9%AA%B8%20%20%E7%95%B6%E5%B1%80%E5%B0%87%E7%A7%BB%E8%B5%B0%E6%AE%98%E9%AA%B8%E7%BA%8C%E5%A1%AB%E6%B5%B7/web_tc/article/20150521/s00001/1432215139377)，明報，2015年5月21日

6.  [滄海遺珠 -
    香港水底考古寶藏](https://www.youtube.com/watch?v=VP4Q0EEUgEI)，星期日檔案，無綫電視，2017年2月05日

7.  [剪報](https://gwulo.com/sites/gwulo.com/files/styles/large/public/images/1947-12-20_hms_tamar.jpg?itok=Q97bZRkp)；「Underwater
    "Attack" on HMS Tamar」，《德臣西報》（The China Mail），1947年12月20日。

8.  [添馬艦 － 昔日海軍基地
    今日政府總部](http://news.stheadline.com/figure/index_r.asp?id=490)，頭條日報

9.  [灣仔疑似沉船殘骸
    土木署指或屬添馬艦](http://hk.apple.nextmedia.com/news/art/20150522/19156445)，蘋果日報，2015年5月22日