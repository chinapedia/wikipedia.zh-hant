**南京大學中山樓**坐落于[江苏省](../Page/江苏省.md "wikilink")[南京市鼓楼区汉口路](../Page/南京市.md "wikilink")22号[南京大學南園北門東側](../Page/南京大學.md "wikilink")，-{松}-林樓對面，是[孫中山在](../Page/孫中山.md "wikilink")[南京的故居](../Page/南京.md "wikilink")。

建築為二層樓，银灰色，2002年和2012年南大校慶時曾加以整修，現作為南京大學学生工作处的办公所。在[孫穗芳主編的](../Page/孫穗芳.md "wikilink")《我的祖父孫中山》一書中，該樓照片被標記為“祖父就任臨時大總統職時之官邸外觀（南京）”。\[1\]\[2\]

## 参考资料

## 參見

  - [孫中山故居](../Page/孫中山故居.md "wikilink")

[N](../Category/南京大学鼓楼校区.md "wikilink")
[N](../Category/南京名人故居.md "wikilink")
[N](../Category/孙中山故居.md "wikilink")

1.
2.