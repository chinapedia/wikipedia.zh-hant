**寶麗金**（**PolyGram**）一般是指**寶麗金唱片公司**，是1999年以前世界上最著名、規模最大的[唱片](../Page/唱片.md "wikilink")[公司](../Page/公司.md "wikilink")，亦是到目前爲止世界上最悠久的唱片公司，曾經擁有[飞利浦](../Page/飞利浦.md "wikilink")（[環球音樂集團已放棄跟](../Page/環球唱片.md "wikilink")[飞利浦集團續約](../Page/飞利浦.md "wikilink")，不再使用這品牌，更銷毀所有相關印刷品和產品）、[迪卡](../Page/迪卡唱片公司.md "wikilink")、[Grammphon等跨國唱片企業](../Page/德意志留聲機公司.md "wikilink")，以擁有高知名度[歌手和高質量的音樂品質而聞名全球](../Page/歌手.md "wikilink")。1980年代末期，寶麗金旗下曾經擁有幾乎世界上最著名的藝術家和樂團：世界三大男高音（[帕瓦羅蒂](../Page/帕瓦羅蒂.md "wikilink")、[多明戈和](../Page/多明戈.md "wikilink")[卡雷拉斯](../Page/卡雷拉斯.md "wikilink")），而[指揮家有最富盛名的](../Page/指揮家.md "wikilink")[卡拉揚](../Page/卡拉揚.md "wikilink")、[阿巴多](../Page/阿巴多.md "wikilink")、[小澤征爾等等](../Page/小澤征爾.md "wikilink")，樂團則有最著名的[維也納愛樂樂團](../Page/維也納愛樂樂團.md "wikilink")、[柏林愛樂樂團](../Page/柏林愛樂樂團.md "wikilink")、[阿姆斯特丹管弦樂團等等](../Page/阿姆斯特丹管弦樂團.md "wikilink")。在1990年代，寶麗金先后收购了[美国的](../Page/美国.md "wikilink")[A\&M](../Page/A&M.md "wikilink")、[Motown及](../Page/Motown.md "wikilink")[英国的](../Page/英国.md "wikilink")[Island唱片公司](../Page/Island.md "wikilink")，加上原来的[寶麗多](../Page/寶麗多.md "wikilink")（Polydor）、[倫敦飛利浦](../Page/倫敦飛利浦.md "wikilink")（LondonPhilips）品牌，进一步垄断了全球的唱片市场。1999年，被現時的美國[環球音樂收購](../Page/環球唱片.md "wikilink")。2013年，香港[環球唱片重建](../Page/環球唱片.md "wikilink")「寶麗金」廠牌，[陳慧嫻其為首位歌手加入旗下](../Page/陳慧嫻.md "wikilink")。

2017年，環球唱片重建了寶麗金廠牌，並將其更名“寶麗金娛樂”(PolyGram Entertainment) ，作為電影、影劇發行商。

## 发展历史

1972年，[德意志留聲機公司和](../Page/德意志留聲機公司.md "wikilink")[飞利浦公司合并](../Page/飞利浦公司.md "wikilink")，取名为“PolyGram
International”意为“宝丽金国际有限公司”，即“Polydor”的“poly”与“Phonogram”的“gram”合成，总部在[英国的](../Page/英国.md "wikilink")[伦敦](../Page/伦敦.md "wikilink")。合并后的宝丽金集团于1970年代帮助[飞利浦发明并且创造了](../Page/飞利浦.md "wikilink")[卡式錄音帶](../Page/卡式錄音帶.md "wikilink")，1980年代又一起合作发明创造了[CD激光唱片](../Page/CD.md "wikilink")，在唱片行业上有了高速度的发展，并将英国[迪卡公司并入集团](../Page/迪卡唱片公司.md "wikilink")。

### 迪卡唱片公司

迪卡唱片公司创立在1929年，但早在1914年，一家留声机公司就以“迪卡”命名，在后期这家公司发展成为[迪卡唱片公司](../Page/迪卡唱片公司.md "wikilink")。直到[第二次世界大战前夕](../Page/第二次世界大战.md "wikilink")，迪卡公司不仅兼并了很多同类公司，并且拥有了一批行业最优秀的[录音师](../Page/录音师.md "wikilink")，共中包括音乐发烧友们所熟悉的[威尔金森](../Page/威尔金森.md "wikilink")（Kenneth
Wilkinson）。迪卡公司不间断的研制开发，推出新的录音技术，使其在录音制作方面始终处于世界领先地位。如“[全频段录音](../Page/全频段录音.md "wikilink")”（ffrr）、“[全频段立体声](../Page/全频段立体声.md "wikilink")”（ffss）等等。1957年，迪卡公司还取得了“[Argo](../Page/Argo.md "wikilink")”商标出版[古典唱片](../Page/古典唱片.md "wikilink")。

### 麦克瑞唱片公司

进入到1960年代，在1961年飞利浦公司的[美国分公司将美国](../Page/美国.md "wikilink")[麦克瑞](../Page/麦克瑞.md "wikilink")（Mercury）唱片公司收购。麦克瑞公司始建于1945年，总部设于美国的[芝加哥](../Page/芝加哥.md "wikilink")。公司初期出版[流行音乐为主](../Page/流行音乐.md "wikilink")，但随后步入[古典音乐市场](../Page/古典音乐.md "wikilink")。公司特别重视[录音技术的研究与试验](../Page/录音技术.md "wikilink")，并且首次运用了“[三只麦克风](../Page/三只麦克风.md "wikilink")”录音系统并发明了“[传真重现](../Page/传真重现.md "wikilink")”（[Living
Presence](../Page/Living_Presence.md "wikilink")）系列，深受各类音乐爱好者的欢迎。宝丽金集团旗下的三家（[德唱](../Page/德唱.md "wikilink")、[飞利浦](../Page/飞利浦.md "wikilink")、[迪卡](../Page/迪卡.md "wikilink")）唱片公司不仅是该公司的支柱产业，亦成为了世界古典唱片市场中最有影响力和最富有声誉的公司。它们各自都在录音技术方面拥有属于自己的优势，例如：德唱公司的[4D技术](../Page/4D技术.md "wikilink")，麦克瑞公司的[Living
Presence](../Page/Living_Presence.md "wikilink")，迪卡公司的[ffrr及飞利浦公司的](../Page/ffrr.md "wikilink")[数字影碟技术等等](../Page/数字影碟技术.md "wikilink")。然后它们在对录制古典音乐曲目选择上也都情投意合。例如：[DG公司的古典及浪漫交响乐](../Page/DG公司.md "wikilink")，飞利浦公司的室内乐及声乐，迪卡公司的古典歌剧及麦克瑞公司的古典“[发烧天碟](../Page/发烧天碟.md "wikilink")”。

1990年代，宝丽金集团先后收购了美国的A\&M、Motown及英国的Island唱片公司，加上原来的Polydor、LondonPhilips等厂标，形成了欧洲流行音乐制作最为庞大的群体，大量录制并出版受全世界乐迷欢迎的[欧美流行音乐](../Page/欧美流行音乐.md "wikilink")。除此之外，宝丽金继续大力发展高质量激光录像制品如[MTV](../Page/MTV.md "wikilink")、古典及[卡拉OK影碟](../Page/卡拉OK.md "wikilink")。在1992年公司推出[数码音带](../Page/数码音带.md "wikilink")（[Digital
Compact
Cassette](../Page/Digital_Compact_Cassette.md "wikilink")），而在1995年则推出新技术产品——[Video
CD](../Page/Video_CD.md "wikilink")，也就是大家耳熟能详的[VCD](../Page/VCD.md "wikilink")。自从1990年代初期开始，宝丽金开始制作发行[电影](../Page/电影.md "wikilink")、[电视片](../Page/电视片.md "wikilink")。宝丽金除在欧美各个国家中设有分公司外，早在1970年代已在亚洲的[日本](../Page/日本.md "wikilink")、[香港](../Page/香港.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")、[马来西亚等地区设立分公司](../Page/马来西亚.md "wikilink")。而1990年代中期又在[韩国](../Page/韩国.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[菲律宾](../Page/菲律宾.md "wikilink")、[泰国](../Page/泰国.md "wikilink")、[印度及](../Page/印度.md "wikilink")[台湾等国家或地区成立分公司](../Page/台湾.md "wikilink")，使其在亚洲地区的业务获得更大的发展。另外，当年著名的美国泰拉克（Telarc）公司的录音制品，也由宝丽金代理在世界推广发行。

## 收购

1990年代，寶麗金在台灣收購了[王振敬股份有限公司](../Page/王振敬股份有限公司.md "wikilink")（[東尼機構](../Page/東尼機構.md "wikilink")）、[綜一股份有限公司](../Page/綜一股份有限公司.md "wikilink")、[飛羚唱片](../Page/飛羚唱片.md "wikilink")、[齊飛唱片](../Page/齊飛唱片.md "wikilink")、[金聲唱片](../Page/金聲唱片.md "wikilink")、[天王唱片等唱片公司的歌曲版權](../Page/天王唱片.md "wikilink")。

## 歌手列表（1970－1999）

### 香港

**參見：[寶麗金 (香港)](../Page/寶麗金_\(香港\).md "wikilink")**

香港寶麗金早於[1970年代成立](../Page/1970年代.md "wikilink")，為[香港培育了第一批粵語歌手](../Page/香港.md "wikilink")，如[許冠傑](../Page/許冠傑.md "wikilink")、[譚詠麟等人](../Page/譚詠麟.md "wikilink")。[1980年代末至](../Page/1980年代.md "wikilink")[1990年代中為全盛時期](../Page/1990年代.md "wikilink")，公司擁有大量的知名音樂製作人，并曾培養出如[張學友等國際地位的歌手](../Page/張學友.md "wikilink")；但1997年開始后的[金融風暴後公司業績開始回落](../Page/金融風暴.md "wikilink")，連番被加入新公司的舊寶麗金歌手炮轟（如1994年解約的[唐韋琪指其於寶麗金五年只出版一張專輯](../Page/唐韋琪.md "wikilink");1997年轉到[力圖唱片的](../Page/力圖唱片.md "wikilink")[黎瑞恩](../Page/黎瑞恩.md "wikilink")，指寶麗金要求她灌錄不合其音樂理念的歌曲;1998年去[加拿大另谋发展的內地歌手](../Page/加拿大.md "wikilink")[王佩](../Page/王佩.md "wikilink")，更指寶麗金只把精力放在香港歌手，自1993年4月签约六年以來只在1998年出版一張專輯《与爱共舞》等），及後因資金不足，只能購買[TVB的音樂錄影帶來做](../Page/TVB.md "wikilink")[卡啦OK](../Page/卡啦OK.md "wikilink")，最終在1999年被[環球唱片收購](../Page/環球唱片_\(香港\).md "wikilink")。

#### 男

以[倫敦飛利浦](../Page/倫敦飛利浦_\(寶麗金品牌\).md "wikilink")/[寶麗多作品牌](../Page/寶麗多_\(寶麗金品牌\).md "wikilink")

  - [許冠傑](../Page/許冠傑.md "wikilink")（1971年－1983年，1990年－1991年），1983年轉到[康藝成音](../Page/康藝成音.md "wikilink")，1985年轉到[新藝寶](../Page/新藝寶唱片.md "wikilink")，1992年退出歌壇，2004年復出轉到[EC
    Music](../Page/EC_Music.md "wikilink")，現已淡出歌壇
  - [張國榮](../Page/張國榮.md "wikilink")（1977年－1981年）1982年轉到[華星唱片](../Page/華星唱片.md "wikilink")，1987年轉到[新藝寶](../Page/新藝寶唱片.md "wikilink")，1990年封咪，1995年復出轉到[滾石唱片](../Page/滾石唱片.md "wikilink")，1999年轉到環球唱片，2003年逝世
  - [區瑞強](../Page/區瑞強.md "wikilink")（1977年－1982年）1983年轉到[百代唱片](../Page/百代唱片_\(香港\).md "wikilink")，1990年轉到[銀星唱片](../Page/銀星唱片.md "wikilink")，現已淡出歌壇
  - [許冠英](../Page/許冠英.md "wikilink")（1977年－1985年，1993年）1985年退出歌壇，1992年復出，1993年轉到[新藝寶](../Page/新藝寶唱片.md "wikilink")，2011年逝世
  - [泰迪羅賓](../Page/泰迪羅賓.md "wikilink")（1978年－1990年）1990年退出歌壇，1992年復出，1994年轉到[滾石唱片](../Page/滾石唱片.md "wikilink")，2006年轉到[EC
    Music](../Page/EC_Music.md "wikilink")，現在專注幕後
  - [關正傑](../Page/關正傑.md "wikilink")（1979年－1983年）1984年轉到[百代唱片](../Page/百代唱片_\(香港\).md "wikilink")，1989年退出歌壇並移民
  - [譚詠麟](../Page/譚詠麟.md "wikilink")（1979年－1999年）合約轉到環球唱片
  - [鍾鎮濤](../Page/鍾鎮濤.md "wikilink")（1980年－1988年）1988年轉到[EPIC唱片](../Page/史诗唱片.md "wikilink")，2006年轉到[BMA](../Page/博美娛樂.md "wikilink")，2012年自立製作公司[Good
    Day Production](../Page/Good_Day_Production.md "wikilink")
  - [彭健新](../Page/彭健新.md "wikilink")（1981年－1987年）1987年解約及退出歌壇
  - [蔡國權](../Page/蔡国权.md "wikilink")（1982年－1992年）1993年退出歌壇
  - [張學友](../Page/张学友.md "wikilink")（1984年－1999年，2013年－）合約轉到環球唱片。
  - [李克勤](../Page/李克勤.md "wikilink")（1986年－1993年）1993年轉到[星光唱片](../Page/星光唱片.md "wikilink")，1999年轉到環球唱片，2016年轉到[英皇娛樂](../Page/英皇娛樂.md "wikilink")
  - [蔣志光](../Page/蔣志光.md "wikilink")（1987年－1988年）1988年轉到[銀星唱片](../Page/銀星唱片.md "wikilink")，1994年退出歌壇
  - [黃凱芹](../Page/黃凱芹.md "wikilink")（1987年－1992年）港台DJ出身，1992年轉到[飛圖唱片](../Page/飛圖唱片.md "wikilink")，1995年退出歌壇，2002年復出轉到環球唱片，2005年自立製作公司Everlasting
    Limited
  - [黃翊](../Page/黃翊_\(香港\).md "wikilink")（1988年－1992年）限製作約，唱片約於[新藝寶](../Page/新藝寶唱片.md "wikilink")，1992年因為與藝人[黃寶欣交往被寶麗金雪藏](../Page/黃寶欣.md "wikilink")，隨後退出歌壇，2017年復出樂壇
  - [何家勁](../Page/何家勁.md "wikilink")（1988年－1994年）1994年轉到[藝能動音](../Page/藝能動音.md "wikilink")，1995年退出歌壇
  - [李健達](../Page/李健達.md "wikilink")（1988年－1990年）限製作人約，唱片約於[新藝寶](../Page/新藝寶唱片.md "wikilink")，1990年轉到[世紀唱片](../Page/世紀唱片.md "wikilink")，1992年與[羅大佑](../Page/羅大佑.md "wikilink")[音樂工廠簽約而退居幕後](../Page/滾石唱片_\(香港\).md "wikilink")，2014年復出自立製作公司[亞帆達音樂有限公司](../Page/亞帆達音樂有限公司.md "wikilink")
  - [黎明](../Page/黎明.md "wikilink")（1990年－1998年）1998年轉到[新力音樂](../Page/索尼音樂娛樂香港.md "wikilink")，2004年與商人[林建岳合資創立](../Page/林建岳.md "wikilink")[A
    Music並成為旗下藝人](../Page/東亞唱片_\(製作\).md "wikilink")
  - [鄭嘉穎](../Page/鄭嘉穎.md "wikilink")（1993年－1995年）1997年轉到[金點唱片](../Page/金點唱片.md "wikilink")，1998年淡出歌壇專注電視劇及電影，2005年復出歌壇轉到[TVB旗下](../Page/電視廣播有限公司.md "wikilink")[正視音樂](../Page/正視音樂.md "wikilink")，2012年轉到[英皇娛樂](../Page/英皇娛樂.md "wikilink")，現為自由身
  - [洪楗華](../Page/洪楗華.md "wikilink")（1994年－1997年）1997年轉到[上華唱片](../Page/上華唱片.md "wikilink")，1999年退出歌壇
  - [劉錫明](../Page/劉錫明.md "wikilink")（1995年）限台灣，[奇花音樂為製作公司](../Page/奇花音樂.md "wikilink")，1996年改由[現代派唱片發行](../Page/現代派唱片.md "wikilink")，1998年轉投[代言者唱片](../Page/代言者唱片.md "wikilink")
  - [陳百祥](../Page/陳百祥.md "wikilink")（1995年－1996年）玩票形式簽約
  - [鄭家麒](../Page/鄭家麒.md "wikilink")（1995年－1997年）1997年解約及退出歌壇
  - [吳銘聰](../Page/吳銘聰.md "wikilink")（1995年－1997年）1997年解約及退出歌壇
  - [鄭中基](../Page/鄭中基.md "wikilink")（1995年－1999年）[小島音樂為製作公司](../Page/小島音樂.md "wikilink")，合約轉到環球唱片，2001年轉投[上華唱片](../Page/上華唱片.md "wikilink")，2003年轉到[金牌娛樂](../Page/金牌大風.md "wikilink")，2018年自設[機動娛樂並轉投](../Page/機動娛樂.md "wikilink")[維高文化](../Page/維高文化.md "wikilink")
  - [陳曉東](../Page/陳曉東_\(藝人\).md "wikilink")（1995年－1999年）合約轉到環球唱片，2007年轉到[EC
    Music](../Page/EC_Music.md "wikilink")，2011年自立製作公司火爆娛樂
  - [林漢洋](../Page/漢洋.md "wikilink")（[林漢揚](../Page/漢洋.md "wikilink")，1996年－1997年）限台灣，現代派唱片為製作公司，1997年現代派唱片自行發行，2004年自立製作公司[Freeway
    Production](../Page/Freeway_Production.md "wikilink")
  - [陳展鵬](../Page/陳展鵬.md "wikilink")（1997年－1999年）[TVB出身](../Page/電視廣播有限公司.md "wikilink")，1999年解約，淡出歌壇專注電視劇及電影，2013年復出歌壇，2016年轉到[太陽娛樂文化](../Page/太陽娛樂文化.md "wikilink")
  - [蔡俊輝](../Page/蔡俊輝.md "wikilink")（1997年－1999年）1999年解約及退出歌壇，其後移民到加拿大多倫多
  - [陳浩民](../Page/陳浩民.md "wikilink")（1998年－1999年）合約轉到環球唱片，2004年轉到[新亞洲娛樂集團](../Page/新亞洲娛樂集團.md "wikilink")，2012年轉到[艾迪昇傳播事業](../Page/艾迪昇傳播事業.md "wikilink")
  - [恭碩良](../Page/恭碩良.md "wikilink")（1998年－1999年）合約轉到環球唱片，2005年轉到[東亞唱片](../Page/東亞唱片_\(製作\).md "wikilink")，2014其後轉到[太陽娛樂文化](../Page/太陽娛樂文化.md "wikilink")
  - [梁焯皓](../Page/梁焯皓.md "wikilink")（1999年）1999年解約及退出歌壇，2010年復出轉到[香港萬事通音樂](../Page/香港萬事通音樂.md "wikilink")

#### 女

以[寶麗多](../Page/寶麗多_\(寶麗金品牌\).md "wikilink")/[倫敦飛利浦作品牌](../Page/倫敦飛利浦_\(寶麗金品牌\).md "wikilink")

  - [葛劍青](../Page/葛劍青.md "wikilink")（1975年）1978年轉到有聲唱片，1979年退出歌壇
  - [陳秋霞](../Page/陳秋霞.md "wikilink")（1976年－1981年）1981年退出歌壇
  - [鄧麗君](../Page/鄧麗君.md "wikilink")（1975年－1995年）來自台灣，1995年逝世
  - [陳麗斯](../Page/陳麗斯.md "wikilink")（1977年－1979年）1979年退出歌壇
  - [景黛音](../Page/景黛音.md "wikilink")（1977年－1978年）1983年轉到[永恆唱片](../Page/永恆唱片.md "wikilink")，1986年退出歌壇
  - [關菊英](../Page/關菊英.md "wikilink")（1979年－1983年）1983年轉到[華星唱片](../Page/華星唱片.md "wikilink")，2012年轉到[星娛樂](../Page/星娛樂.md "wikilink")，現為自由身
  - [陳美玲](../Page/陳美玲_\(香港藝人\).md "wikilink")（1980年－1982年）1985年轉到[百代唱片](../Page/百代唱片_\(香港\).md "wikilink")，1990年退出歌壇
  - [余安安](../Page/余安安.md "wikilink")（1980年－1985年）影視圈出身，1985年退出歌壇
  - [雷安娜](../Page/雷安娜.md "wikilink")（1980年－1988年）1987年移民，1989年一度回港簽約[永高創意](../Page/飛圖唱片.md "wikilink")，1990年淡出樂壇，2010年復出轉到[環星娛樂](../Page/環星娛樂.md "wikilink")，2016年轉到[風行唱片](../Page/風行唱片.md "wikilink")
  - [蔣麗萍](../Page/蔣麗萍.md "wikilink")（1981年－1987年）1987年轉行至宗教界
  - [露雲娜](../Page/露雲娜.md "wikilink")（1981年－1987年）1987年解約，兩年後退出歌壇，2010年獲[陳奕迅邀請而復出樂壇](../Page/陳奕迅.md "wikilink")
  - [王雅文](../Page/王雅文.md "wikilink")（1982年－1984年）港台DJ出身，1984年轉到[百代唱片](../Page/百代唱片_\(香港\).md "wikilink")，1987年退出歌壇
  - [盧業瑂](../Page/盧業瑂.md "wikilink")（1983年－1985年）1987年轉到[銀星唱片](../Page/銀星唱片.md "wikilink")，1989年退出歌壇
  - [柳影虹](../Page/柳影虹.md "wikilink")（1983年－1987年）1987年退出歌壇
  - [夏妙然](../Page/夏妙然.md "wikilink")（1985年）港台DJ出身，1985年轉到[新力唱片](../Page/索尼音樂娛樂香港.md "wikilink")，1987年退出歌壇
  - [魏綺清](../Page/魏綺清.md "wikilink")（1985年）1987年轉到[百代唱片](../Page/百代唱片_\(香港\).md "wikilink")，1989年退出歌壇
  - [鄺美雲](../Page/鄺美雲.md "wikilink")（1985年－1989年）1989年轉到[DMI唱片](../Page/迪生唱片.md "wikilink")，1998年退出歌壇
  - [徐小鳳](../Page/徐小鳳.md "wikilink")（1985年－1992年）1992年合約期滿淡出歌壇
  - [方心美](../Page/方心美.md "wikilink")（1986年－1987年）1987年解約及退出歌壇
  - [陳慧嫻](../Page/陳慧嫻.md "wikilink")（1986年－1997年，2013年－）1990年留學海外，但仍有保留合約及出唱片，1995年復出，1997年合約轉到[新藝寶唱片](../Page/新藝寶唱片.md "wikilink")，2003年轉到[環球音樂](../Page/環球唱片_\(香港\).md "wikilink")，2007年－2008年與中國國際娛樂合作，2013年環球唱片特意重開寶麗金廠牌為其唱片公司
  - [李明珠](../Page/李明珠_\(歌手\).md "wikilink")（1987年－1988年）1988年解約及退出歌壇
  - [關淑怡](../Page/關淑怡.md "wikilink")（1988年－1996年）1996年解約及退出歌壇，2001年復出轉到[BMG唱片](../Page/博德曼音樂_\(香港\).md "wikilink")，2002年因生孩子退出歌壇，2005年再次復出轉到[大國文化](../Page/大國文化集團.md "wikilink")，2007年再轉到[星娛樂](../Page/星娛樂.md "wikilink")，2018年轉到[環球唱片](../Page/環球唱片_\(香港\).md "wikilink")
  - [周影](../Page/周影.md "wikilink")（1989年－1991年）港台DJ出身，1991年解約，翌年簽約[TVB並逐漸轉型為幕後行政人員](../Page/電視廣播有限公司.md "wikilink")
  - [沈殿霞](../Page/沈殿霞.md "wikilink")（1990年－1991年）玩票形式簽約，2008年逝世
  - [黎明詩](../Page/黎明詩.md "wikilink")（1991年）1991年轉到[百代唱片](../Page/百代唱片_\(香港\).md "wikilink")，1995年退出歌壇
  - [劉小慧](../Page/劉小慧.md "wikilink")（1991年－1994年）1994年轉到[BMG唱片](../Page/博德曼音樂_\(香港\).md "wikilink")，1997年退出歌壇
  - [唐韋琪](../Page/唐韋琪.md "wikilink")（1991年－1994年）1995年轉到[雅樂唱片](../Page/雅樂唱片.md "wikilink")，千禧年代後轉型為幕後行政人員，2009年自立製作公司[韋琪娛樂製作](../Page/韋琪娛樂製作.md "wikilink")
  - [周慧敏](../Page/周慧敏.md "wikilink")（1991年－1997年）台灣由[福茂唱片發行](../Page/福茂唱片.md "wikilink")，1997年淡出歌壇，2006年復出開紅館個唱，2011年轉到[星娛樂](../Page/星娛樂.md "wikilink")，2014年轉到[英皇娛樂](../Page/英皇娛樂.md "wikilink")，現為自由身
  - [黎瑞恩](../Page/黎瑞恩.md "wikilink")（1991年－1997年）1995年經理人公司轉到[雄圖娛樂](../Page/雄圖娛樂.md "wikilink")，1997年轉到[力圖唱片](../Page/力圖唱片.md "wikilink")，2002年轉到[百代唱片](../Page/百代唱片_\(香港\).md "wikilink")，2016年復出轉到[星娛樂](../Page/星娛樂.md "wikilink")
  - [湯寶如](../Page/湯寶如.md "wikilink")（1992年－1998年）1998年解約及退出歌壇，2012年復出轉到[musicNEXT](../Page/環星娛樂.md "wikilink")，2017年自立製作公司K
    Art Workshop
  - [王馨平](../Page/王馨平.md "wikilink")（1993年－1999年）1999年經理人公司轉到台灣[協和經紀](../Page/協和經紀.md "wikilink")，唱片公司轉到[坎城唱片](../Page/坎城唱片.md "wikilink")（[友善的狗代理發行](../Page/友善的狗.md "wikilink")），2013年自立製作公司秘密花園娛樂
  - [陳奕](../Page/陳奕_\(香港\).md "wikilink")（1994年）1995年轉到[雅樂唱片](../Page/雅樂唱片.md "wikilink")，2000年退出歌壇
  - [吳君如](../Page/吳君如.md "wikilink")（1995年）玩票形式簽約
  - [-{zh-hans:李君筑;
    zh-hant:李君筑;}-](../Page/李君筑.md "wikilink")（1995年－1997年）幕後出身，1994年獲關淑怡引薦往幕前發展，1997年解約及重回幕後
  - [陳琪](../Page/陳琪_\(歌手\).md "wikilink")（1995年－1997年）1997年解約及退出歌壇
  - [許秋怡](../Page/許秋怡.md "wikilink")（1996年－1997年）限台灣，[現代派唱片為製作公司](../Page/現代派唱片.md "wikilink")，1997年現代派唱片自行發行，1998年退出歌壇
  - [陳秀雯](../Page/陳秀雯.md "wikilink")（1996年－1997年）1997年解約，1998年轉到[環星音樂](../Page/環星娛樂.md "wikilink")，現已淡出歌壇
  - [謝凱珊](../Page/謝凱珊.md "wikilink")（1997年－1999年）1999年解約及退出歌壇
  - [李蕙敏](../Page/李蕙敏.md "wikilink")（1998年－1999年）合約轉到環球音樂，2007年轉到[星娛樂](../Page/星娛樂.md "wikilink")
  - [高雪嵐](../Page/傅又宣.md "wikilink")（1998年－1999年）合約轉到環球音樂，2001年沿用真名為[傅珮嘉正式加入樂壇](../Page/傅又宣.md "wikilink")，2004年轉到[BMA](../Page/博美娛樂.md "wikilink")，2015年化名[傅又宣轉到台灣](../Page/傅又宣.md "wikilink")[夢想沙龍娛樂文化](../Page/夢想沙龍娛樂文化.md "wikilink")，2016年實行網上集資募款計劃自立製作單曲

#### 組合

以下以[倫敦飛利浦作品牌](../Page/倫敦飛利浦_\(寶麗金品牌\).md "wikilink")

  - [温拿樂隊](../Page/温拿樂隊.md "wikilink")（1973年－1978年,1981—1993）1978年解散。1981年復出，1993年再度解散。2008年再度復出轉到環球唱片
  - [達明一派](../Page/達明一派.md "wikilink")（1984年－1990年，2016年－）1990年解約，1991年解散，1996年復出轉到[正東唱片](../Page/正東唱片.md "wikilink")，1997年再度解散，2004年再度復出轉到[英皇娛樂](../Page/英皇娛樂.md "wikilink")，2006年再度解散，2012年再度復出，2016年再度加盟寶麗金
  - [草蜢](../Page/草蜢_\(組合\).md "wikilink")（1988年－1997年）1997年轉到[滾石唱片](../Page/滾石唱片.md "wikilink")，2005年轉到[BMA](../Page/博美娛樂.md "wikilink")，2009年轉到環球唱片，2010年轉到[正視音樂](../Page/正視音樂.md "wikilink")，2012年轉到[寰亞唱片](../Page/寰亞唱片.md "wikilink")
  - [風之Group](../Page/風之Group.md "wikilink")（1989年）1989年解約，成員[方樹樑轉型為音樂人](../Page/方樹樑.md "wikilink")
  - [CD
    Voice](../Page/CD_Voice.md "wikilink")（1994年－1997年）台灣由以[福茂唱片發行](../Page/福茂唱片.md "wikilink")，1997年解散

以下以[寶麗多](../Page/寶麗多.md "wikilink")（寶麗金品牌）寶麗多作品牌

  - [Beyond](../Page/Beyond.md "wikilink")（1986年-1987年）1987年轉入Kinn's
    Music，1988年轉入新藝寶，1992年轉到[華納](../Page/香港華納唱片.md "wikilink")，1992年轉到[滾石唱片](../Page/滾石唱片.md "wikilink")，2000年解散，2003年復出以單一合作形式和[百代唱片合作](../Page/百代唱片_\(香港\).md "wikilink")，2004年正式解散
  - Sisters（1997年－1999年）1999年解約及退出歌壇
  - T\&T（1997年－1999年）1999年解約及退出歌壇

以下以[新藝寶作品牌](../Page/新藝寶.md "wikilink")

  - [軟硬天師](../Page/軟硬天師.md "wikilink")（1991年－1996年）限製作約，1996年解散，2006年復出以單一合作形式和[百代唱片合作](../Page/百代唱片_\(香港\).md "wikilink")，2013年再復出以單一合作形式和[金牌娛樂合作](../Page/金牌大風.md "wikilink")

#### 參考

  - [寶麗金皇者之聲歌唱大賽](../Page/寶麗金皇者之聲歌唱大賽.md "wikilink")──香港寶麗金舉辦

### 臺灣

**參見：[寶麗金 (臺灣)](../Page/寶麗金_\(臺灣\).md "wikilink")**

#### 男

  - [費翔](../Page/費翔.md "wikilink")（1984年－1985年）
  - [劉文正](../Page/劉文正.md "wikilink")（1982年－1984年）
  - [李恕權](../Page/李恕權.md "wikilink")（1984年－1987年）1996年退出樂壇
  - [羅時豐](../Page/羅時豐.md "wikilink")（1986年－1990年代）收購[飛羚唱片取得合約](../Page/飛羚唱片.md "wikilink")
  - [齊秦](../Page/齊秦.md "wikilink")（1981年－1989年）收購[綜一唱片取得合約](../Page/綜一唱片.md "wikilink")，1989年合約轉至[滾石唱片](../Page/滾石唱片.md "wikilink")
  - [童安格](../Page/童安格.md "wikilink")（1985年－1996年）1996年合約轉至[點將唱片](../Page/點將唱片.md "wikilink")
  - [周治平](../Page/周治平.md "wikilink")（1990年－1996年）1996年退居幕後
  - [何潤東](../Page/何潤東.md "wikilink")（1998年－1999年）1999年合約轉至[環球唱片](../Page/環球唱片.md "wikilink")
  - [高明駿](../Page/高明駿.md "wikilink")（1993年－1996年）1996年退出樂壇
  - [金城武](../Page/金城武.md "wikilink")（1992年－1994年）[年代唱片發行合約](../Page/年代唱片.md "wikilink"),
    1994年發行合約轉至[科藝百代](../Page/科藝百代.md "wikilink")
  - [張鎬哲](../Page/張鎬哲.md "wikilink")（1987年－1992年）1992年合約轉至[波麗佳音](../Page/波麗佳音.md "wikilink")
  - [林東松](../Page/林東松.md "wikilink")（1995年－1997年）1997年退居幕後
  - [畢國勇](../Page/畢國勇.md "wikilink")（1997年－1998年）1998年退居幕後
  - [張洪量](../Page/張洪量.md "wikilink")（1994年－1996年）1997年合約轉至[滾石唱片](../Page/滾石唱片.md "wikilink")
  - [吳百倫](../Page/吳百倫.md "wikilink")（1993年－1994年）
  - [胡瓜](../Page/胡瓜.md "wikilink")（1990年－1995年）
  - [糯米團](../Page/糯米團.md "wikilink")（1994年－1999年）1999年退伍後加入[魔岩唱片](../Page/魔岩唱片.md "wikilink")
  - [洪榮宏](../Page/洪榮宏.md "wikilink")（1991年－1994年）[羅飛音樂製作公司發行合約](../Page/羅飛音樂製作公司.md "wikilink"),
    1994年發行合約轉至[真善美唱片](../Page/真善美唱片.md "wikilink")

#### 女

  - [藍心湄](../Page/藍心湄.md "wikilink")（1984年－1995年）收購[飛羚唱片取得合約](../Page/飛羚唱片.md "wikilink")，1996年合約轉至[新力音樂](../Page/新力音樂_\(台灣\).md "wikilink")
  - [金元萱](../Page/金元萱.md "wikilink")（1993年－1997年）限華語合約，1997年退出樂壇
  - [況明潔](../Page/況明潔.md "wikilink")（1984年－1991年）1991年退出樂壇
  - [黃雅珉](../Page/黃雅珉.md "wikilink")（1984年－1993年）1994年合約轉至[科藝百代](../Page/科藝百代.md "wikilink")
  - [黃鶯鶯](../Page/黃鶯鶯.md "wikilink")（1982年－1986年）1986年合約轉至[飛碟唱片](../Page/飛碟唱片.md "wikilink")
  - [金素梅](../Page/金素梅.md "wikilink")（1990年－1994年）1994年退出樂壇
  - [林佳儀](../Page/林佳儀.md "wikilink")（1998年－1999年）1999年合約轉至[環球唱片](../Page/環球唱片.md "wikilink")
  - [伊雪莉](../Page/伊雪莉.md "wikilink")（1998年－1999年）1999年合約轉至[環球唱片](../Page/環球唱片.md "wikilink")
  - [蔡健雅](../Page/蔡健雅.md "wikilink")（1998年－1999年）1999年合約轉至[環球唱片](../Page/環球唱片.md "wikilink")
  - [林嘉欣](../Page/林嘉欣.md "wikilink")（1995年－1999年）1999年合約轉至[環球唱片](../Page/環球唱片.md "wikilink")
  - [趙詠華](../Page/趙詠華.md "wikilink")（1996年－1998年）經理人公司為[小島音樂](../Page/小島音樂.md "wikilink")，1999年合約轉至[滾石唱片](../Page/滾石唱片.md "wikilink")
  - [甄秀珍](../Page/甄秀珍.md "wikilink")（1978年－1987年）收購[東尼機構取得合約](../Page/東尼機構.md "wikilink")，1988年退出樂壇
  - [楊林](../Page/楊林_\(歌手\).md "wikilink")（1982年－1997年）收購[綜一唱片取得合約](../Page/綜一唱片.md "wikilink")，1997年合約轉至[非常喜音樂](../Page/非常喜音樂.md "wikilink")
  - [堂娜](../Page/堂娜.md "wikilink")（1986年－1988年）1988年合約轉至[喜瑪拉雅唱片](../Page/喜瑪拉雅唱片.md "wikilink")
  - [金佩珊](../Page/金佩珊.md "wikilink")（1984年－1988年）收購[綜一唱片取得合約](../Page/綜一唱片.md "wikilink")，1988年合約轉至[歌林唱片](../Page/歌林唱片.md "wikilink")
  - [何如惠](../Page/何如惠.md "wikilink")（1989年－？）
  - [苏芮](../Page/苏芮.md "wikilink")（1981－1983）出道时艺名为“苏君丽”，1983年转到[飞碟唱片](../Page/飞碟唱片.md "wikilink")

### 中國大陆

#### 男

  - [-{zh-hans:麥子杰;
    zh-hant:麥子杰;}-](../Page/麥子杰.md "wikilink")（1994年－1998年）1998年解約
  - [-{zh-hans:郑钧; zh-hant:郑钧;}-](../Page/郑钧.md "wikilink")（合约时间不详）

#### 女

  - [王佩](../Page/王佩.md "wikilink")（1993年－1998年）1998年解約

## 注释

## 参考文献

## 外部連結

  - [Last.fm上的寶麗金](http://www.last.fm/label/Polygram?setlang=en)

{{-}}

[Category:飞利浦](../Category/飞利浦.md "wikilink")
[Category:環球唱片](../Category/環球唱片.md "wikilink")
[Category:美國唱片公司](../Category/美國唱片公司.md "wikilink")
[Category:已结业公司](../Category/已结业公司.md "wikilink")
[Category:1945年成立的公司](../Category/1945年成立的公司.md "wikilink")
[Category:1999年結業公司](../Category/1999年結業公司.md "wikilink")
[Category:威望迪子公司](../Category/威望迪子公司.md "wikilink")