**乾安县**是[中国](../Page/中国.md "wikilink")[吉林省](../Page/吉林省.md "wikilink")[松原市所辖的一个](../Page/松原市.md "wikilink")[县](../Page/县.md "wikilink")。
[缩略图](https://zh.wikipedia.org/wiki/File:吉林，乾安泥林1.jpg "fig:缩略图")\]\]

## 行政区划

乾安县辖6个镇、9个乡\[1\] 。

## 参考文献

## 外部链接

  - [松原市乾安县简介](https://web.archive.org/web/20051219125809/http://www.jilin.gov.cn/cn/sxfm/qax.html)

[乾安县](../Category/乾安县.md "wikilink") [县](../Category/松原区县.md "wikilink")
[松原](../Category/吉林省县份.md "wikilink")

1.