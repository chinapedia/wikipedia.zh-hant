**李慨俠**（\[1\]；\[2\]），綽號**滅民敵**\[3\]，[香港](../Page/香港.md "wikilink")[西北航運前](../Page/西北航運.md "wikilink")[行政總裁](../Page/行政總裁.md "wikilink")，[全民在野黨創黨主席](../Page/全民在野黨.md "wikilink")，現職[遊艇教練](../Page/遊艇.md "wikilink")。

## 簡歷

李慨俠祖籍[廣東](../Page/廣東.md "wikilink")[番禺](../Page/番禺.md "wikilink")[市橋](../Page/市橋.md "wikilink")\[4\]，中學分別就讀[南華中學及](../Page/南華中學.md "wikilink")[大同中學](../Page/大同中學.md "wikilink")。曾擔任[香港水警](../Page/香港水警.md "wikilink")（1974年－1988年，最高職級為警長）\[5\]，也曾代表[自民聯參選](../Page/自民聯.md "wikilink")[1991年香港立法局選舉](../Page/1991年香港立法局選舉.md "wikilink")[九龍東直選議席](../Page/九龍東.md "wikilink")，但成為最低票數的候選人。

後來李慨俠轉職為[香港的士司機](../Page/香港的士.md "wikilink")，其後任職機場保安主任（及[香港機場航空保安職工會主席](../Page/香港機場航空保安職工會.md "wikilink")），但被革職\[6\]。2001年，他被懷疑涉及多宗銀行劫案，因而被捕，但最後確定為誤會\[7\]。

2003年，[西北航運投得來往香港](../Page/西北航運.md "wikilink")[屯門及](../Page/屯門.md "wikilink")[澳門航線的專營權](../Page/澳門.md "wikilink")，李慨俠擔任當時的行政總裁。2006年11月，西北航運首條航線啟航，來往[屯門及](../Page/屯門.md "wikilink")[珠海](../Page/珠海.md "wikilink")，但因無法開辦澳門航線，決定辭職。

2007年10月31日，李慨俠正式參選[2007年香港立法會港島選區補選](../Page/2007年香港立法會港島選區補選.md "wikilink")，成為參選人之一\[8\]，但後來於2007年11月2日被選舉事務處以有效提名不足，而裁定提名無效。\[9\]

[2016年香港立法會選舉](../Page/2016年香港立法會選舉.md "wikilink")，李慨俠參選[新界東選區](../Page/新界東選區.md "wikilink")，但再次因提名不足而被裁定提名無效。\[10\]

## 參考資料

[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:香港政治人物](../Category/香港政治人物.md "wikilink")
[Category:前香港協進聯盟成員](../Category/前香港協進聯盟成員.md "wikilink")
[G](../Category/李姓.md "wikilink")
[Category:前香港自由民主聯會成員](../Category/前香港自由民主聯會成員.md "wikilink")

1.

2.

3.

4.
5.
6.  [原定去年開航 至今改裝未竣
    屯門港澳碼頭恐爛尾](http://the-sun.on.cc/channels/news/20050212/20050212031748_0001.html)，《太陽報》，2005年2月12日

7.
8.  [立法會香港島補選共有九人獲提名](http://www.info.gov.hk/gia/general/200710/31/P200710310221.htm)，香港政府新聞公報，2007年10月31日

9.  [李慨俠不獲參選立會港島區補選資格](http://www.atchinese.com/index.php?option=com_content&task=view&id=41802&Itemid=34)，2007年11月2日

10.