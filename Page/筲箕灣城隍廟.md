[HK_ShauKeiWan_ShingWongTemple.JPG](https://zh.wikipedia.org/wiki/File:HK_ShauKeiWan_ShingWongTemple.JPG "fig:HK_ShauKeiWan_ShingWongTemple.JPG")
[Shau_Kei_Wan_Shing_Wong_Temple_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Shau_Kei_Wan_Shing_Wong_Temple_\(Hong_Kong\).jpg "fig:Shau_Kei_Wan_Shing_Wong_Temple_(Hong_Kong).jpg")
[Shing_Wong_Temple,_Shau_Kei_Wan_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Shing_Wong_Temple,_Shau_Kei_Wan_\(Hong_Kong\).jpg "fig:Shing_Wong_Temple,_Shau_Kei_Wan_(Hong_Kong).jpg")。\]\]
**筲箕灣城隍廟**，原稱**筲簊灣福德祠**，是[香港一座](../Page/香港.md "wikilink")[城隍廟](../Page/城隍廟.md "wikilink")，位於[香港島](../Page/香港島.md "wikilink")[筲箕灣](../Page/筲箕灣.md "wikilink")[金華街
(香港)與](../Page/金華街_\(香港\).md "wikilink")[筲箕灣東大街交界](../Page/筲箕灣東大街.md "wikilink")，1987年被評為[二級歷史建築](../Page/香港二級歷史建築.md "wikilink")，於2010年2月4日確定為[香港三級歷史建築](../Page/香港三級歷史建築.md "wikilink")。每年[農曆五月十一](../Page/農曆.md "wikilink")、五月廿八、七月廿四（[城隍誕](../Page/城隍.md "wikilink")）、二月初二（[土地誕](../Page/土地.md "wikilink")）及五月初五（[五通誕](../Page/五通.md "wikilink")），較多善信參拜。

## 歷史

筲箕灣城隍廟建於[清朝](../Page/清朝.md "wikilink")[光緒三年](../Page/光緒.md "wikilink")（1877年），由當地的坊眾集資興建，原本是供奉[福德公與](../Page/土地神.md "wikilink")[五通神](../Page/五通神.md "wikilink")，所以當時被稱為「福德祠」，同時亦供奉[觀音及](../Page/觀音.md "wikilink")[齊天大聖](../Page/齊天大聖.md "wikilink")。該廟初期由坊眾所管理，曾易名為「五通廟」。1928年，該廟交由[華人廟宇委員會管理](../Page/華人廟宇委員會.md "wikilink")，並進行擴建及維修，由於[香港島以往沒有專門供奉](../Page/香港島.md "wikilink")[城隍的廟宇](../Page/城隍.md "wikilink")，因此華人廟宇委員會於1974年在該廟旁興建一座相連的祠堂供奉城隍，改稱「城隍廟」。

城隍廟[門聯](../Page/門聯.md "wikilink")：「古廟重新靈爽式憑開福地 四方永賴恩光普照庇烝民」

## 交通

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{港島綫色彩}}">█</font>[港島綫](../Page/港島綫.md "wikilink")：[筲箕灣站B](../Page/筲箕灣站.md "wikilink")1出口(沿[筲箕灣東大街步行](../Page/筲箕灣東大街.md "wikilink")5分鐘)

## 外部連結

  - [筲箕灣城隍廟 -
    華人廟宇委員會](http://www.ctc.org.hk/b5/directcontrol/temple6.asp)
  - [玄來係呢度 第17集第1節 筲箕灣譚公廟／城隍廟 -
    道通天地電視頻道](http://www.taoist.tv/show_episode.php?eid=319)
  - [東區文物徑簡介](https://web.archive.org/web/20090207083057/http://qcrc.qef.org.hk/webpage/19984196/1/eheritagetrail.html)

[Category:香港三級歷史建築](../Category/香港三級歷史建築.md "wikilink")
[城](../Category/香港廟宇.md "wikilink")
[Category:筲箕灣](../Category/筲箕灣.md "wikilink")
[Category:城隍廟](../Category/城隍廟.md "wikilink")