**Notepad++**是一套為[自由軟體的](../Page/自由軟體.md "wikilink")[純文字編輯器](../Page/文本编辑器.md "wikilink")，由[侯今吾基于同是](../Page/侯今吾.md "wikilink")[開放原始碼的](../Page/開放原始碼.md "wikilink")[Scintilla文本编辑组件並獨力研發](../Page/Scintilla文本编辑组件.md "wikilink")，整个项目起初托管于[SourceForge.net之上](../Page/SourceForge.net.md "wikilink")，截止到2011年，已被下載超過2700萬次，\[1\]\[2\]并两度获得SourceForge社群選擇獎——最佳開發工具。\[3\]2010年6月托管于TuxFamily，现由GitHub托管。

該軟體以[GPL發佈](../Page/GPL.md "wikilink")，有完整的[中文化介面及支援多國](../Page/中文.md "wikilink")[語言撰寫的功能](../Page/語言.md "wikilink")（採用[UTF-8编码](../Page/UTF-8.md "wikilink")）。它的功能比[Windows中的](../Page/Windows.md "wikilink")[記事本](../Page/記事本.md "wikilink")（Notepad）強大，除了可以用來製作一般的純文字的說明文件，也十分適合用作撰寫[電腦程式的編輯器](../Page/電腦程式.md "wikilink")。由6.2.3版本起，Notepad++的文件預設文字格式由ANSI改為除去[BOM的UTF](../Page/位元組順序記號.md "wikilink")8（UTF8
without
BOM）。Notepad++不僅有[語法高亮度顯示](../Page/語法高亮度顯示.md "wikilink")，也有[語法摺疊功能](../Page/語法摺疊.md "wikilink")，並且支援[巨集以及擴充基本功能的](../Page/巨集.md "wikilink")[外掛模組](../Page/外掛模組.md "wikilink")。

## 主要功能

  - 所見即所得（[WYSIWYG](../Page/WYSIWYG.md "wikilink")）
  - 用戶自定[程序語言](../Page/程序語言.md "wikilink")
  - 字詞[自動完成功能](../Page/自動完成.md "wikilink")（Auto-completion）
  - 支持同時編輯多重文件
  - 支持多重視窗同步編輯
  - 支援以[正則表達式查找和替換字詞](../Page/正則表達式.md "wikilink")
  - 支持拖曳功能
  - 內部[視窗位置可任意移動](../Page/視窗.md "wikilink")
  - 自動偵測開啟[檔案狀態](../Page/檔案.md "wikilink")
  - 放大與縮小功能
  - 雖說是支持多國[語言](../Page/語言.md "wikilink")，但是僅止於操作介面，編輯檔案時部分文字無法正常顯示，而會顯示成□
  - 支持[書籤](../Page/書籤.md "wikilink")
  - 高亮度括號及縮排輔助
  - 支持[巨集](../Page/巨集.md "wikilink")
  - [語法高亮度顯示及](../Page/語法高亮度顯示.md "wikilink")[語法摺疊功能](../Page/語法摺疊.md "wikilink")，支援的[程序設計語言如下](../Page/程序設計語言.md "wikilink")：

| 支援語言                                               |
| -------------------------------------------------- |
| [ActionScript](../Page/ActionScript.md "wikilink") |
| [Caml](../Page/Caml.md "wikilink")                 |
| [HTML](../Page/HTML.md "wikilink")                 |
| [Matlab](../Page/Matlab.md "wikilink")             |
| [R](../Page/R语言.md "wikilink")                     |
|                                                    |

## 事件

### 2008年奧運風波

2008年，Notepad++在主页上發起「抵制奥运」為抗議[中國政府在](../Page/中國政府.md "wikilink")[西藏的鎮壓行動](../Page/西藏.md "wikilink")\[4\]，。

### 2015年我是查理

在2015年1月7日，查理周刊总部枪击案之後。Notepad++ 6.7.4版安裝後，於第一次開啟時，會出現下列內容。

    Freedom of expression is like the air we breathe, we don't feel it, until people take it away from us.

    For this reason, Je suis Charlie, not because I endorse everything they published, but because I cherish the right to speak out freely without risk even when it offends others.
    And no, you cannot just take someone's life for whatever he/she expressed.

    Hence this "Je suis Charlie" edition.

## 參見

  - [文本编辑器的比较](../Page/文本编辑器的比较.md "wikilink")
  - [Scintilla](../Page/Scintilla.md "wikilink")

## 參考資料

## 外部連結

  - [Notepad++官網](http://notepad-plus-plus.org/)
  - [PortableApps.com上的](../Page/PortableApps.com.md "wikilink")

[Category:2003年软件](../Category/2003年软件.md "wikilink")
[Category:Windows文本编辑器](../Category/Windows文本编辑器.md "wikilink")
[Category:自由文本编辑器](../Category/自由文本编辑器.md "wikilink")
[Category:用C++編程的自由軟體](../Category/用C++編程的自由軟體.md "wikilink")
[Category:Notepad替代](../Category/Notepad替代.md "wikilink")
[Category:Windows独占自由软件](../Category/Windows独占自由软件.md "wikilink")
[Category:自由多语言软件](../Category/自由多语言软件.md "wikilink")
[Category:自由HTML编辑器](../Category/自由HTML编辑器.md "wikilink")
[Category:使用GPL许可证的软件](../Category/使用GPL许可证的软件.md "wikilink")
[Category:使用Scintilla的软件](../Category/使用Scintilla的软件.md "wikilink")

1.
2.
3.
4.