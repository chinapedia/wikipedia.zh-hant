**楚德湖**（，，），又称佩普西湖，是欧洲最大的[跨国湖泊](../Page/公海.md "wikilink")，位于[俄罗斯和](../Page/俄罗斯.md "wikilink")[爱沙尼亚边境](../Page/爱沙尼亚.md "wikilink")。

楚德湖面积3,555[平方公里](../Page/平方公里.md "wikilink")，是[欧洲排在](../Page/欧洲.md "wikilink")[俄罗斯](../Page/俄罗斯.md "wikilink")[拉多加湖与](../Page/拉多加湖.md "wikilink")[奥涅加湖](../Page/奥涅加湖.md "wikilink")、[瑞典](../Page/瑞典.md "wikilink")[维纳恩湖和](../Page/维纳恩湖.md "wikilink")[芬兰](../Page/芬兰.md "wikilink")[塞马湖之后第五大淡水湖](../Page/塞马湖.md "wikilink")，平均水深7[米](../Page/米.md "wikilink")，最深处15米。该湖是著名的[观光胜地和](../Page/旅游.md "wikilink")[渔业基地](../Page/渔业.md "wikilink")，但是近年来受到比较严重的[工业](../Page/工业.md "wikilink")[污染](../Page/污染.md "wikilink")。

1242年，[亚历山大·涅夫斯基率](../Page/亚历山大·涅夫斯基.md "wikilink")[诺夫哥罗德军队在此击败](../Page/诺夫哥罗德.md "wikilink")[條頓骑士团](../Page/條頓骑士团.md "wikilink")，史称“[冰上大激战](../Page/楚德湖战役.md "wikilink")”。
[LakePeipus01.JPG](https://zh.wikipedia.org/wiki/File:LakePeipus01.JPG "fig:LakePeipus01.JPG")
[缩略图](https://zh.wikipedia.org/wiki/File:納爾瓦河-流域-水系.svg "fig:缩略图")

[Category:俄罗斯湖泊](../Category/俄罗斯湖泊.md "wikilink")
[Peipsi-Pihkva](../Category/愛沙尼亞湖泊.md "wikilink")
[Category:愛沙尼亞-俄羅斯邊界](../Category/愛沙尼亞-俄羅斯邊界.md "wikilink")
[Peipsi-Pihkva](../Category/歐洲跨國湖泊.md "wikilink")
[Category:普斯科夫州](../Category/普斯科夫州.md "wikilink")
[Category:納爾瓦河](../Category/納爾瓦河.md "wikilink")