[Dollarnote_siegel_hq.jpg](https://zh.wikipedia.org/wiki/File:Dollarnote_siegel_hq.jpg "fig:Dollarnote_siegel_hq.jpg")，或稱為全視之眼，被一些人認為是牽涉[美國創始人和](../Page/美國開國元勛.md "wikilink")[光明會的陰謀證據](../Page/光明會.md "wikilink")。\]\]
**陰謀論**通常是指對[歷史或當代事件作出特別解釋的說法](../Page/歷史.md "wikilink")，通常暗指事件的公开解释为故意欺骗，而背后有集团操纵事态发展及结果，以达至该集团损人利己的目的。此類特別解釋不同於一般廣為接受的解釋，而解釋事件為個人或是團體秘密策劃的結果。而且此類特別解釋中激進者還會進一步駁斥該等廣為接受的解釋，認為該等是陰謀策劃者的掩飾。

陰謀論不只是指理論，也用來指許多沒有證據的說法、傳聞。阴谋论往往缺乏[證據](../Page/證據.md "wikilink")，其荒謬邏輯不符合[奥卡姆剃刀原则](../Page/奥卡姆剃刀.md "wikilink")，是很多谣传之基础。但是被指责为阴谋当事人的一方，也很难证伪阴谋论的说法，许多阴谋论有[不可证伪的特点](../Page/不可证伪.md "wikilink")，即常言道“难以自证清白”。逻辑上，任何一件自然发生的事件，事后都可以被描绘成事先策划。因此，說一個[理論是陰謀論同时有该理論不被廣為接受的意味](../Page/理論.md "wikilink")。

## 陰謀論與英文

### Conspiracy

英文中的**Conspiracy**是指兩人以上的人作一件非法或不道德之事的約定，未必是秘密策劃。不過中文有時將Conspiracy
theory譯為陰謀論，而英文中的Conspiracy theory也常指秘密策劃的陰謀。

中文中也有**「共謀」**、**「共謀論」**等語。

### Conspiracism

Frank P. Mintz說Conspiracism是「相信歷史會揭露共謀」之信念（"belief in the primacy of
conspiracies in the unfolding of history"）。

## 研究

陰謀論常面臨的問題是證據不充分、難以被否證、其他理論可用更少的假設得出更好的解釋（[奥卡姆剃刀](../Page/奥卡姆剃刀.md "wikilink")）、邏輯上不可能、以及推論有繆誤。

對於為什麼人們會製造出並相信陰謀論，有以下解釋：

  - 心理學基礎：人類傾向對發生的事情找一個原因來解釋，而陰謀論正是簡單易懂的理論，「一切都是某某組織的計畫」很容易被接受。
  - 政治、社會基礎：政治人物的操弄、傳媒加速資訊的傳播、在人群中受他人影響等。

## 故事性

由於陰謀論常富有戲劇性，許多小說和影劇會以陰謀作為故事主題，並時常取材自現有的陰謀論。

## 案例

### 政治

  - [新世界秩序](../Page/新世界秩序.md "wikilink")：此理論認為有少數權力精英組成的秘密集團在幕後操控世界，透過各種秘密社團、兄弟會、紳士俱樂部、科研機構、非營利組織、智庫、政府部門、企業行號與許多國際組織來推動議程。其操控範圍廣布於政治、國防、金融、商業、運輸、教育、科學、資訊、電信、醫藥、食物、能源、民生、工業、宗教、媒體、娛樂、文化等領域，目標是建立少數人控制、全球一體化的專制世界政府，取代傳統的主權民族國家，搭建出一個長期穩定但不保障個人權益的社會。常見的指稱對象包括：[猶太人](../Page/猶太人.md "wikilink")\[1\]、[美帝國主義](../Page/美帝國主義.md "wikilink")\[2\]、[納粹主義](../Page/納粹主義.md "wikilink")\[3\]、[一美元紙幣](../Page/一美元紙幣.md "wikilink")\[4\]、[撒旦教](../Page/撒旦教.md "wikilink")\[5\]、[羅斯柴爾德家族](../Page/羅斯柴爾德家族.md "wikilink")\[6\]、[洛克菲勒家族](../Page/洛克菲勒家族.md "wikilink")\[7\]、[肯尼迪家族](../Page/肯尼迪家族.md "wikilink")\[8\]、[沃伯格家族](../Page/沃伯格家族.md "wikilink")\[9\]、\[10\]、\[11\]、\[12\]、\[13\]、[薩伏依家族](../Page/薩伏依王朝.md "wikilink")\[14\]、[青幫](../Page/青幫.md "wikilink")、[洪門](../Page/洪門.md "wikilink")、[光明會](../Page/光明會.md "wikilink")\[15\]、[共濟會](../Page/共濟會.md "wikilink")\[16\]、[骷髏會](../Page/骷髏會.md "wikilink")\[17\]、[玫瑰十字會](../Page/玫瑰十字會.md "wikilink")\[18\]、[聖殿騎士](../Page/聖殿騎士團.md "wikilink")\[19\]、[三百人委員會](../Page/三百人委員會.md "wikilink")\[20\]、[百人會](../Page/百人會.md "wikilink")\[21\]、[穆斯林兄弟會](../Page/穆斯林兄弟會.md "wikilink")\[22\]、[費邊社](../Page/費邊社.md "wikilink")\[23\]、[畢德堡俱樂部](../Page/畢德堡俱樂部.md "wikilink")\[24\]、\[25\]、[羅馬俱樂部](../Page/羅馬俱樂部.md "wikilink")\[26\]、[三邊委員會](../Page/三邊委員會.md "wikilink")\[27\]、[美國外交關係協會](../Page/美國外交關係協會.md "wikilink")\[28\]、[錫安長老會紀要](../Page/錫安長老會紀要.md "wikilink")\[29\]、[華爾街](../Page/華爾街.md "wikilink")\[30\]、[美聯儲](../Page/美聯儲.md "wikilink")\[31\]、[中央情報局](../Page/中央情報局.md "wikilink")\[32\]、[美國國家安全局](../Page/美國國家安全局.md "wikilink")\[33\]、[聯邦調查局](../Page/聯邦調查局.md "wikilink")\[34\]、[特定入侵行動辦公室](../Page/特定入侵行動辦公室.md "wikilink")\[35\]、[聯邦緊急事務管理署](../Page/聯邦緊急事務管理署.md "wikilink")\[36\]、[美國網戰司令部](../Page/美國網戰司令部.md "wikilink")\[37\]、[國防高等研究計劃署](../Page/國防高等研究計劃署.md "wikilink")\[38\]、\[39\]、[美國國家航空航天局](../Page/美國國家航空航天局.md "wikilink")\[40\]、[美國國家稅務局](../Page/美國國家稅務局.md "wikilink")\[41\]、[美國疾病控制與預防中心](../Page/美國疾病控制與預防中心.md "wikilink")\[42\]、[美國食品藥品監督管理局](../Page/美國食品藥品監督管理局.md "wikilink")\[43\]、[美國科學促進會](../Page/美國科學促進會.md "wikilink")\[44\]、[英國秘密情報局](../Page/英國秘密情報局.md "wikilink")\[45\]、[以色列情報及特殊使命局](../Page/以色列情報及特殊使命局.md "wikilink")\[46\]、[巴基斯坦三軍情報局](../Page/巴基斯坦三軍情報局.md "wikilink")\[47\]、[國際農業生物技術應用服務組織](../Page/國際農業生物技術應用服務組織.md "wikilink")\[48\]、[歐洲核子研究組織](../Page/歐洲核子研究組織.md "wikilink")\[49\]、[國家科學基金會](../Page/國家科學基金會.md "wikilink")\[50\]、[美國醫學會](../Page/美國醫學會.md "wikilink")\[51\]、[美國以色列公共事務委員會](../Page/美國以色列公共事務委員會.md "wikilink")\[52\]、[布魯金斯學會](../Page/布魯金斯學會.md "wikilink")\[53\]、[自由之家](../Page/自由之家.md "wikilink")\[54\]、[戰略與國際研究中心](../Page/戰略與國際研究中心.md "wikilink")\[55\]、[彼得森國際經濟研究所](../Page/彼得森國際經濟研究所.md "wikilink")\[56\]、[英國皇家國際事務研究所](../Page/漆咸樓.md "wikilink")\[57\]、[史密森尼學會](../Page/史密森尼學會.md "wikilink")\[58\]、[伍德羅·威爾遜國際學者中心](../Page/伍德羅·威爾遜國際學者中心.md "wikilink")\[59\]、[普林斯頓高等研究院](../Page/普林斯頓高等研究院.md "wikilink")\[60\]、[胡佛研究所](../Page/胡佛研究所.md "wikilink")\[61\]、[國際戰略研究所](../Page/國際戰略研究所.md "wikilink")\[62\]、[高頻主動式極光研究計劃](../Page/高頻主動式極光研究計劃.md "wikilink")\[63\]、[國家民主基金會](../Page/國家民主基金會.md "wikilink")\[64\]、[洛克菲勒基金會](../Page/洛克菲勒基金會.md "wikilink")\[65\]、[福特基金會](../Page/福特基金會.md "wikilink")\[66\]、[比爾與美琳達·蓋茨基金會](../Page/比爾與美琳達·蓋茨基金會.md "wikilink")\[67\]、[卡內基國際和平基金會](../Page/卡內基國際和平基金會.md "wikilink")\[68\]、[量子基金](../Page/量子基金.md "wikilink")\[69\]、[開放社會基金會](../Page/開放社會基金會.md "wikilink")\[70\]、[亞洲文化協會](../Page/亞洲文化協會.md "wikilink")\[71\]、[美國公民自由聯盟](../Page/美國公民自由聯盟.md "wikilink")\[72\]、[無國界記者](../Page/無國界記者.md "wikilink")\[73\]、[瑞銀集團](../Page/瑞銀集團.md "wikilink")\[74\]、[瑞德集團](../Page/Lazard.md "wikilink")\[75\]、[高盛](../Page/高盛.md "wikilink")\[76\]["Crime
    Inc.: Redistribution of
    Wealth"](http://www.glennbeck.com/content/articles/article/198/40758/),
    , May 17,
    2010</ref>、[摩根大通集團](../Page/摩根大通集團.md "wikilink")\[77\]、[花旗集團](../Page/花旗集團.md "wikilink")\[78\]、\[79\]、[領航投資](../Page/領航投資.md "wikilink")\[80\]、[凱雷集團](../Page/凱雷集團.md "wikilink")\[81\]、[黑石集團](../Page/黑石集團.md "wikilink")\[82\]、\[83\]、[IQT電信](../Page/IQT電信.md "wikilink")\[84\]、[微軟](../Page/微軟.md "wikilink")\[85\]、[金吉達品牌國際公司](../Page/金吉達品牌國際公司.md "wikilink")\[86\]、[孟山都公司](../Page/孟山都公司.md "wikilink")\[87\]、[杜邦公司](../Page/杜邦公司.md "wikilink")\[88\]、[先正達](../Page/先正達.md "wikilink")\[89\]、[陶氏化學](../Page/陶氏化學.md "wikilink")\[90\]、[巴斯夫](../Page/巴斯夫.md "wikilink")\[91\]、[拜耳](../Page/拜耳.md "wikilink")\[92\]、[輝瑞製藥](../Page/輝瑞製藥.md "wikilink")\[93\]、[洛克希德·馬丁](../Page/洛克希德·馬丁.md "wikilink")\[94\]、[雷神公司](../Page/雷神公司.md "wikilink")\[95\]、[通用動力公司](../Page/通用動力公司.md "wikilink")\[96\]、[普華永道](../Page/普華永道.md "wikilink")\[97\]、[德勤](../Page/德勤.md "wikilink")\[98\]、[康卡斯特](../Page/康卡斯特.md "wikilink")\[99\]、[21世紀福斯公司](../Page/21世紀福斯公司.md "wikilink")\[100\]、[全美娛樂公司](../Page/全美娛樂公司.md "wikilink")\[101\]、[新聞集團](../Page/新聞集團.md "wikilink")\[102\]、[威望迪](../Page/威望迪.md "wikilink")\[103\]、[電通](../Page/電通.md "wikilink")\[104\]、[華特迪士尼公司](../Page/華特迪士尼公司.md "wikilink")\[105\]、[時代華納](../Page/時代華納.md "wikilink")\[106\]、[米高梅公司](../Page/米高梅公司.md "wikilink")\[107\]、[紐約時報](../Page/紐約時報.md "wikilink")\[108\]、[華盛頓時報](../Page/華盛頓時報.md "wikilink")\[109\]、[新聞周刊](../Page/新聞周刊.md "wikilink")\[110\]、[華爾街日報](../Page/華爾街日報.md "wikilink")\[111\]、[ABC新聞](../Page/ABC新聞.md "wikilink")\[112\]、[CBS電視網](../Page/CBS電視網.md "wikilink")\[113\]、[國家廣播公司](../Page/國家廣播公司.md "wikilink")\[114\]、[英國石油](../Page/英國石油.md "wikilink")\[115\]、[艾克森美孚](../Page/艾克森美孚.md "wikilink")\[116\]、[荷蘭皇家殼牌](../Page/荷蘭皇家殼牌.md "wikilink")\[117\]、[巴西石油](../Page/巴西石油.md "wikilink")\[118\]、[蘇伊士環境集團](../Page/蘇伊士環境集團.md "wikilink")\[119\]、[戴比爾斯](../Page/戴比爾斯.md "wikilink")\[120\]、[比奇特爾公司](../Page/比奇特爾公司.md "wikilink")\[121\]、[沃爾瑪公司](../Page/沃爾瑪公司.md "wikilink")\[122\]、[奇異公司](../Page/通用电气.md "wikilink")\[123\]、[麥肯錫公司](../Page/麥肯錫公司.md "wikilink")\[124\]、[蘭德公司](../Page/蘭德公司.md "wikilink")\[125\]、[埃森哲](../Page/埃森哲.md "wikilink")\[126\]、[蘋果公司](../Page/蘋果公司.md "wikilink")\[127\]、[Facebook](../Page/Facebook.md "wikilink")\[128\]、[Google](../Page/Google.md "wikilink")\[129\]、[美國在線](../Page/美國在線.md "wikilink")\[130\]、[美國聯合通訊社](../Page/美國聯合通訊社.md "wikilink")\[131\]、[美國之音](../Page/美國之音.md "wikilink")\[132\]、[自由歐洲電台／自由電台](../Page/自由歐洲電台／自由電台.md "wikilink")\[133\]、[諾華公司](../Page/諾華公司.md "wikilink")\[134\]、[蘇伊士環境集團](../Page/蘇伊士環境集團.md "wikilink")\[135\]、\[136\]、\[137\]、[梵蒂岡](../Page/梵蒂岡.md "wikilink")\[138\]、[歐洲委員會](../Page/歐洲委員會.md "wikilink")\[139\]、[聯合國](../Page/聯合國.md "wikilink")\[140\]、[歐盟](../Page/歐盟.md "wikilink")\[141\]、[北大西洋公約組織](../Page/北大西洋公約組織.md "wikilink")\[142\]、[世界衛生組織](../Page/世界衛生組織.md "wikilink")\[143\]、、[國際癌症研究機構](../Page/國際癌症研究機構.md "wikilink")\[144\]、[國際計劃生育聯合會](../Page/國際計劃生育聯合會.md "wikilink")\[145\]、[世界貿易組織](../Page/世界貿易組織.md "wikilink")\[146\]、\[147\]、[世界銀行](../Page/世界銀行.md "wikilink")\[148\]、[國際清算銀行](../Page/國際清算銀行.md "wikilink")\[149\]、[國際貨幣基金組織](../Page/國際貨幣基金組織.md "wikilink")\[150\]、[布列敦森林體系](../Page/布列敦森林體系.md "wikilink")\[151\]、[關稅與貿易總協定](../Page/關稅與貿易總協定.md "wikilink")\[152\]、[北美自由貿易協定](../Page/北美自由貿易協定.md "wikilink")\[153\]、[跨太平洋戰略經濟夥伴關係協議](../Page/跨太平洋戰略經濟夥伴關係協議.md "wikilink")\[154\]、[高頻主動式極光研究計劃](../Page/高頻主動式極光研究計劃.md "wikilink")\[155\]、\[156\]、[創價學會](../Page/創價學會.md "wikilink")\[157\]、[喬治亞引導石](../Page/佐治亚引导石.md "wikilink")\[158\]等。

  - 假政變：各國政府，特別是[美國政府](../Page/美國.md "wikilink")，被指控進行虛假政變，以便在國外設置友好政府。其中一些被證實，如[1953年伊朗政變](../Page/1953年伊朗政變.md "wikilink")，秘密政變推翻[伊朗民選領導人](../Page/伊朗.md "wikilink")。其他一些政變被一些人認為可能是積極支持美國政府，包括：1963年美國[肯尼迪遇刺事件](../Page/肯尼迪遇刺案.md "wikilink")、[1991年海地政變](../Page/海地历史.md "wikilink")、[1992年委內瑞拉政變企圖](../Page/1992年委內瑞拉政變企圖.md "wikilink")、、[1999年巴基斯坦政變](../Page/1999年巴基斯坦政變.md "wikilink")

  - [1947年國際共產主義陰謀論](../Page/麦卡锡主义.md "wikilink")

  - [1968年芝加哥審判陰謀論](../Page/芝加哥七人案.md "wikilink")

  -
  - [肯尼迪遇刺案陰謀論](../Page/肯尼迪遇刺案陰謀論.md "wikilink")：是最著名的陰謀論之一，認為美國政府某個秘密組織[暗殺了甘迺迪](../Page/暗殺.md "wikilink")，並且一手遮掩。這個故事拍成了[電影](../Page/電影.md "wikilink")「[刺杀肯尼迪](../Page/刺杀肯尼迪.md "wikilink")」。

  - 臺灣[三一九槍擊事件](../Page/三一九槍擊事件.md "wikilink")

  - [深層政府](../Page/深層政府.md "wikilink")：指一個非經民選，由[軍事工業複合體](../Page/軍事工業複合體.md "wikilink")／金融巨頭／[情報機構所組成的](../Page/情報機構.md "wikilink")，為保護它們自己的既得利益，幕後真正控制國家的非法集團\[159\]。由於[唐納·川普就任](../Page/唐納·川普.md "wikilink")[美國總統後的政治局勢緊張](../Page/美國總統.md "wikilink")，該術語已自2017年被廣泛使用\[160\]。

### 科技

  - [加水就能跑的車子](../Page/加水就能跑的車子.md "wikilink")，阴谋论者认为，其觸犯了既得利益者之利益，而被特務機關抹殺。

  -
  -
  - [誰消滅了電動車](../Page/誰消滅了電動車.md "wikilink")

  - [尼古拉·特斯拉](../Page/尼古拉·特斯拉.md "wikilink")

  - [太陽神卡特爾](../Page/太陽神卡特爾.md "wikilink")

  - [免費能源被打壓陰謀論](../Page/免費能源被打壓陰謀論.md "wikilink")

  - [蒙淘克計劃](../Page/蒙淘克計劃.md "wikilink")

  - [高頻主動式極光研究計劃](../Page/高頻主動式極光研究計劃.md "wikilink")

  -
  - [費城實驗](../Page/費城實驗.md "wikilink")

  - [MKUltra計劃](../Page/MKUltra計劃.md "wikilink")：CIA的洗腦研究

  - [阿波羅登月計劃陰謀論](../Page/阿波羅登月計劃陰謀論.md "wikilink")，認為[美國政府在攝影棚內製造出登陸月球的假象](../Page/美國.md "wikilink")。

  - [通用汽車電車陰謀](../Page/通用汽車電車陰謀.md "wikilink")

### 外星人

  - 美國軍方隱瞞論：軍方刻意向外散佈一些誇張失實的外星人謠言。久而久之，使國民覺得外星人只是無稽之談，降低普羅大眾對外星人的興趣及相信程度。
  - [納粹飛碟](../Page/納粹飛碟.md "wikilink")：[納粹德國於](../Page/納粹德國.md "wikilink")[第二次世界大戰期間在](../Page/第二次世界大戰.md "wikilink")[布拉格建立了飛行試驗基地](../Page/布拉格.md "wikilink")，並研發出。而在蘇俄紅軍攻入布拉格後，納粹科學家緊急銷燬當時眾多的原型、藍圖、樣本，根據傳言，這些頂尖的科學家在戰後被帶往美國從事秘密武器的研發。此理論被[Discovery拍成紀錄片](../Page/Discovery.md "wikilink")《[真實的飛碟](../Page/真實的飛碟.md "wikilink")》。
  - [51區](../Page/51區.md "wikilink")
  - [羅斯威爾飛碟墜毀事件](../Page/羅斯威爾飛碟墜毀事件.md "wikilink")：由於美國政府對其進行情報控制，相關的小報消息与[都會傳奇就成為陰謀推測的材料](../Page/都會傳奇.md "wikilink")，著名的包括[51區跟軍方的新型秘密飛行器](../Page/51區.md "wikilink")。
  - 外星技術：近代的一些科學成果其實是參考、模仿或學習一些外星智慧生命的產物，因此某些組織或政府想隱藏當中的一些資料，特別是為面對國與國之間的軍事和技術競賽。
  - [黑衣人](../Page/黑衣人.md "wikilink")
  - [MJ-12](../Page/MJ-12.md "wikilink")

### 災難和末日

#### [SARS](../Page/SARS.md "wikilink")

[嚴重急性呼吸道症候群](../Page/嚴重急性呼吸道症候群.md "wikilink")（SARS）在2002年至2003年間[爆發疫情時](../Page/SARS事件.md "wikilink")，因其源於[中國南方並向外蔓延至世界各國](../Page/中國南方.md "wikilink")，故有傳言指出SARS是[中华人民共和國政府有意或意外外流的](../Page/中华人民共和國政府.md "wikilink")[生物武器](../Page/生物武器.md "wikilink")，在SARS發生期間便有民眾私下議論此一可能，部分中國民眾則認為SARS是[广东沿海居民長期嗜食](../Page/广东.md "wikilink")[野味](../Page/野味.md "wikilink")，因而感染的一种类似[禽流感的变异生命体](../Page/禽流感.md "wikilink")[细菌](../Page/细菌.md "wikilink")；但2008年[台湾](../Page/台湾.md "wikilink")[國安局局長](../Page/中華民國國家安全局.md "wikilink")[蔡朝明在](../Page/蔡朝明.md "wikilink")[立法院接受質詢時](../Page/立法院.md "wikilink")，曾聲稱SARS可能為[中共政府的生物武器](../Page/中共政府.md "wikilink")，[中國人民解放軍](../Page/中國人民解放軍.md "wikilink")[大校](../Page/大校.md "wikilink")[戴旭亦曾稱SARS是](../Page/戴旭.md "wikilink")[美國對中國使用的生物心理武器](../Page/美國.md "wikilink")。

#### [911恐怖攻擊](../Page/911恐怖攻擊.md "wikilink")

有論者認為[美國政府高層與大商家覬覦](../Page/美國政府.md "wikilink")[伊拉克境內的](../Page/伊拉克.md "wikilink")[石油儲備](../Page/石油.md "wikilink")，所以自導自演是次[恐怖襲擊](../Page/恐怖襲擊.md "wikilink")，不惜犧牲國民性命，更誣衊伊拉克境內有大殺傷力武器以便決定全面出兵。論者對[世貿大樓的襲擊](../Page/世貿大樓.md "wikilink")，有以下懷疑：

1.  飛機撞擊大樓後漏出的燃油所燃燒的高溫，並不足以燒熔鋼筋以至影響全棟大廈的結構。
2.  建築歷史上的所有鋼筋大樓，並沒有一棟是因為燃燒而倒塌。
3.  大樓倒塌的速度類似裝有炸藥的拆樓解體。
4.  世貿一號及二號大樓遭飛機撞擊，七號大樓並沒有遭到任何有記錄的襲擊，卻在同一日與一號及二號大樓一同倒塌。
5.  世貿大樓群的擁有者Larry Silverstein於911襲擊前幾個月為大樓更新99年期租約，及購買大額損毀賠償，指明包括恐怖襲擊。
6.  時任紐約市長[朱利阿尼迅速地將世貿殘骸當作廢物運出國外](../Page/朱利阿尼.md "wikilink")，當中有否炸藥引致的爆炸痕跡已經不能被鑑別。

亦有論者認為是次恐怖襲擊的幕後策劃係[以色列](../Page/以色列.md "wikilink")[猶太復國主義者](../Page/猶太復國主義者.md "wikilink")。目的是製做世界混亂，國與國甚至人與人之間互不信任，從而販賣各種大小武器圖利為副，最終實現統一世界為主。

### 體育

每一屆世界盃都必然有至少一組被譽為[死亡之組](../Page/死亡之組.md "wikilink")，2006年世界盃抽籤，[阿根廷被編入被譽為](../Page/阿根廷國家足球隊.md "wikilink")**死亡之組**的C組，同組對手包括歐洲勁旅[荷蘭](../Page/荷蘭國家足球隊.md "wikilink")、非洲黑馬[科特迪瓦和pot](../Page/科特迪瓦國家足球隊.md "wikilink")4唯一一隊歐洲隊[塞黑](../Page/塞爾維亞和黑山國家足球隊.md "wikilink")，阿根廷傳媒揚言有幕後黑手做小動作，將阿根廷兩度被抽進死亡之組。而[意大利傳媒亦認為德國前國家隊隊長](../Page/意大利.md "wikilink")[馬圖斯將pot](../Page/馬圖斯.md "wikilink")4中實力較強的[美國抽中和意大利同組是有內情](../Page/美國.md "wikilink")。

### 宗教

[撒但](../Page/撒但.md "wikilink")\[161\]是[基督教對](../Page/基督教.md "wikilink")[魔鬼的名稱](../Page/魔鬼.md "wikilink")。由於[猶太人相信](../Page/猶太人.md "wikilink")“[666](../Page/666.md "wikilink")”這個數字是魔鬼的代表，有不少物品並認為與魔鬼崇拜有聯繫。例如：[條碼的](../Page/條碼.md "wikilink")“6”字無論從左往右或是從右往左掃描都是一樣，所以亦被用來當作條碼的分隔線。這樣，每一組條碼都會兩個“６”字包著。有人因此而說每一個條碼都隱藏著“[獸名數目](../Page/獸名數目.md "wikilink")”，並聲言當[世界末日來臨之時](../Page/世界末日.md "wikilink")，魔鬼會要求每個人在額上印上一個條碼。

### 經濟

国际大財團在背後壟斷了國家經濟以及政治的主導權，为自己图谋暴利。宋鸿兵的《[货币战争](../Page/货币战争.md "wikilink")》对此详细描述。

## 参考文献

## 相關主題

  - [都會傳奇](../Page/都會傳奇.md "wikilink")
  - [訴諸恐懼](../Page/訴諸恐懼.md "wikilink")
  - [鬼祟謬誤](../Page/鬼祟謬誤.md "wikilink")
  - [抵消假設](../Page/抵消假設.md "wikilink")
  - [時代精神運動](../Page/時代精神運動.md "wikilink")
  - [秘密结社](../Page/秘密结社.md "wikilink")

## 延伸閱讀

  -
  -
  - Burnett, Thom. *[Conspiracy Encyclopedia: The Encyclopedia of
    Conspiracy
    Theories](../Page/Conspiracy_Encyclopedia:_The_Encyclopedia_of_Conspiracy_Theories.md "wikilink")*

  -
  -
  - \*

  - Fleming, Chris and Emma A. Jane. *Modern Conspiracy: The Importance
    of Being Paranoid*. New York and London: Bloomsbury, 2014. ISBN
    978-1-62356-091-1.

  - Harris, Lee. [*"The Trouble with Conspiracy
    Theories,"*](https://web.archive.org/web/20130114231139/http://american.com/archive/2013/january/the-trouble-with-conspiracy-theories)
    The American, 12 January 2013.

  - Hofstadter, Richard. *The paranoid style in American politics*
    (1954).
    [online](http://www.fuminyang.com/michelle/Paranoid%20Style.pdf)

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  - Slosson, W. [*"The 'Conspiracy'
    Superstition,"*](https://archive.org/stream/unpopularreview07newy#page/394/mode/2up)
    The Unpopular Review, Vol. VII, N°. 14, 1917.

  - Uscinski, Joseph E. and Joseph M. Parent, *American Conspiracy
    Theories* (2014)
    [excerpt](https://www.amazon.com/dp/0199351813/ref=rdr_ext_tmb)

  - Uscinski, Joseph E. "The 5 Most Dangerous Conspiracy Theories of
    2016' [*POLITICO Magazine* (Aug 22,
    2016)](http://www.politico.com/magazine/story/2016/08/conspiracy-theories-2016-donald-trump-hillary-clinton-214183)

  -
  - Wood, Gordon S. "Conspiracy and the paranoid style: causality and
    deceit in the eighteenth century." *William and Mary Quarterly*
    (1982): 402–441. [in jstor](http://www.jstor.org/stable/1919580)

  - Zwierlein,Cornel / Beatrice de Graaf (eds.) [*Security and
    Conspiracy in History, 16th to 21st
    Century*](http://www.gesis.org/en/hsr/current-issues/). [Historical
    Social Research](../Page/Historical_Social_Research.md "wikilink")
    38, Special Issue, 2013

## 外部链接

  - [State Department's Todd Leventhal Discusses Conspiracy
    Theories](http://iipdigital.usembassy.gov/st/english/texttrans/2009/07/20090714143549iaecnav0.4049581.html),
    2009, U.S. Department of State's Bureau of International Information
    Programs usembassy.gov

  - [September 11 Conspiracy Theories: Confused stories
    continue](http://iipdigital.usembassy.gov/st/english/texttrans/2006/01/20060120112446atlahtnevel0.627331.html),
    2006, usembassy.gov

  - [Why Rational People Buy Into Conspiracy
    Theories](http://www.nytimes.com/2013/05/26/magazine/why-rational-people-buy-into-conspiracy-theories.html),
    Maggie Koerth-Baker, 21 May 2013, NYT.

  -
  -
  - [Conspiracism](http://www.publiceye.org/tooclose/conspiracism.html),
    Political Research Associates

{{-}}

[Category:歷史學](../Category/歷史學.md "wikilink")
[Category:心理学](../Category/心理学.md "wikilink")
[Category:社會學](../Category/社會學.md "wikilink")
[陰謀論](../Category/陰謀論.md "wikilink")

1.  Don Wilkey, book review of New World Order, ["A Christian Looks At
    the Religious Right"](http://www.livingston.net/wilkyjr/link26.htm)
    . Retrieved 2006-12-11

2.

3.

4.

5.

6.
7.  McDonald, Lawrence P. Introduction. [*The Rockefeller
    File*](http://www.mega.nu:8080/ampp/gary_allen_rocker/). By . Seal
    Beach, CA: '76 Press, 1976. ISBN 0-89245-001-0.

8.
9.

10.
11.
12.
13.
14.
15.
16.

17.
18.
19.
20.

21.

22.

23.

24.

25.
26.
27.
28.
29.
30.
31.
32.

33.

34.

35.
36.

37.
38.

39.

40.
41.

42.

43.
44.
45.

46.
47.
48.
49.
50.
51.
52.
53.
54.
55.
56.
57.
58.
59.
60.
61.
62.
63.
64.
65.

66.

67.
68.
69.

70.

71.
72.
73.
74.
75.
76.
77.
78.
79.
80.
81.
82.
83.
84.
85.

86.
87.
88.
89.

90.
91.
92.
93.
94.
95.
96.
97.
98.
99.

100.
101.
102.
103.
104.
105.
106.
107.
108.
109.
110.
111.
112.
113.
114.
115.
116.
117.
118.
119.
120.
121.
122.
123.
124.
125.
126.
127.
128.
129.
130.
131.
132.
133.
134.
135.
136.
137.
138.
139.

140.
141.

142.

143.
144.
145.
146.
147.
148.
149.
150.
151.
152.
153.
154.
155.
156.
157.
158.

159.

160.

161. 查目前比較流行的[國語和合本](../Page/國語和合本.md "wikilink")[聖經](../Page/聖經.md "wikilink")、[新標點和合本](../Page/新標點和合本.md "wikilink")[聖經](../Page/聖經.md "wikilink")、[和合本修訂版](../Page/和合本修訂版.md "wikilink")[聖經](../Page/聖經.md "wikilink")、[現代中文譯本](../Page/現代中文譯本.md "wikilink")[聖經](../Page/聖經.md "wikilink")，還有比較古老的[光緒](../Page/光緒.md "wikilink")19年[福州美華](../Page/福州.md "wikilink")[書局活板文理](../Page/書局.md "wikilink")[聖經](../Page/聖經.md "wikilink")、[光緒](../Page/光緒.md "wikilink")34年[上海大](../Page/上海.md "wikilink")[美國](../Page/美國.md "wikilink")[聖經會](../Page/聖經.md "wikilink")[官話串珠](../Page/官話.md "wikilink")[聖經](../Page/聖經.md "wikilink")、[宣統](../Page/宣統.md "wikilink")3年[聖經公會的文理](../Page/聖經.md "wikilink")[聖經](../Page/聖經.md "wikilink")，Satan均譯作「撒但」，不作「撒旦」