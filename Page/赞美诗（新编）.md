[ChineseNewHymnal.jpg](https://zh.wikipedia.org/wiki/File:ChineseNewHymnal.jpg "fig:ChineseNewHymnal.jpg")

《**赞美诗（新编）**》（）在1980年代开始用中文被编辑出版。又叫“圣歌集”，由[中国基督教协会出版](../Page/中国基督教协会.md "wikilink")，南京[爱德印刷有限公司印刷](../Page/爱德印刷有限公司.md "wikilink")。现在，[中国基督教新教](../Page/中国基督教新教.md "wikilink")[三自爱国教会的礼拜](../Page/三自爱国教会.md "wikilink")，大致上无不例外用这个赞美歌本。

## 历史

在[文化大革命对](../Page/文化大革命.md "wikilink")[宗教](../Page/宗教.md "wikilink")，特别是对[基督教的压制大体上被结束](../Page/基督教.md "wikilink")，重新恢复礼拜的1980年左右，《赞美诗（新编）》聚集了国内外的赞美歌而编辑，从此到1983年出版[简谱版](../Page/简谱.md "wikilink")，到1985年出版[五线谱版本](../Page/五线谱.md "wikilink")。简谱是指广泛使用的“数字乐谱”，用数字1、2、3、4、5等音阶表现的乐谱法。现在在中国通常见的是简乐谱版《赞美诗（新编）》。

## 内容

共收录400首歌曲，期中附录有42曲“短歌”。

编辑的最大特征是，来自欧美的赞美诗歌之外，大量地采用了[中国人所做的歌曲](../Page/中国人.md "wikilink")，但作词者、作曲者、译者名字只在[五线谱版中记载](../Page/五线谱.md "wikilink")。

第394篇《[主祷文](../Page/主祷文.md "wikilink")》和395篇《[使徒信经](../Page/使徒信经.md "wikilink")》等诗歌在礼拜当中不配音乐也能歌唱。

“短歌”里有第41篇《[旧约圣经目录歌](../Page/旧约圣经.md "wikilink")》和第42篇《[新约圣经目录歌](../Page/新约圣经.md "wikilink")》。

## 中英双语本

1998年出版了中英双语版的《赞美诗（新编）（中英文双语本）》（*The New Hymnal, English-Chinese
Bilingual*）由[中国基督教两会出版](../Page/两会_\(消歧义\).md "wikilink")。采用[五线谱记谱](../Page/五线谱.md "wikilink")，而且在中国作的歌词也用英文翻译，在教会礼拜中，常提供给外国信徒使用，这样就可以一起赞美。

## 补充本

《赞美诗（新编）补充本》于2009年11月出版。该诗本共有赞美诗200首，分类与《新编》基本相同，增加了新的栏目，如“家庭儿童”、“爱护环境”、“祈祷和平”等，为崇拜提供更多可选择的用诗，也丰富了信徒的灵修生活。

《补充本》收入了国外教会中广泛流传、脍炙人口的诗歌（包括传统与近代作品）；也收入了中国信徒的新创作，是灵性经验的心声，“补充本”将与“新编”同时使用。

## 使用情况

当代[中国基督教新教的礼拜中普遍使用](../Page/中国基督教新教.md "wikilink")《[圣经](../Page/圣经.md "wikilink")（[和合本](../Page/和合本.md "wikilink")）》、《**赞美诗（新编）**》及其《补充本》、《[主祷文](../Page/主祷文.md "wikilink")》和《[使徒信经](../Page/使徒信经.md "wikilink")》。华语新教教会一般不使用《[尼西亚信经](../Page/尼西亚信经.md "wikilink")》。

## 参考文献

## 参见

  - [中国基督教史](../Page/中国基督教史.md "wikilink")
      - [中国基督教新教](../Page/中国基督教新教.md "wikilink")
          - [和合本](../Page/和合本.md "wikilink")
          - [普天頌讚](../Page/普天頌讚.md "wikilink")
          - [中国基督教协会](../Page/中国基督教协会.md "wikilink")

<!-- end list -->

  - [赞美](../Page/赞美.md "wikilink")
      - [赞美诗](../Page/赞美诗.md "wikilink")
      - [基督教音乐](../Page/基督教音乐.md "wikilink")

<!-- end list -->

  - [音乐记谱法](../Page/音乐记谱法.md "wikilink")
      - [五线谱](../Page/五线谱.md "wikilink")
      - [简谱](../Page/简谱.md "wikilink")

{{-}}

[Category:中华人民共和国基督教新教文献](../Category/中华人民共和国基督教新教文献.md "wikilink")
[Category:中国基督教协会](../Category/中国基督教协会.md "wikilink")
[赞美诗新编](../Category/赞美诗新编.md "wikilink")
[Category:中国基督教新教诗歌集](../Category/中国基督教新教诗歌集.md "wikilink")
[Category:中华人民共和国诗集](../Category/中华人民共和国诗集.md "wikilink")