## 摘要

<table style="width:10%;">
<colgroup>
<col style="width: 1%" />
<col style="width: 8%" />
</colgroup>
<thead>
<tr class="header">
<th><p>描述摘要</p></th>
<th><p>香港<a href="../Page/東區區議會.md" title="wikilink">東區區議會徽號</a><br />
非官方規格（<a href="../Page/User:Wrightbus.md" title="wikilink">User:Wrightbus版本</a>）</p>
<ul>
<li>區徽顏色：紅</li>
<li>會徽包含四條半圓弧線、四條直線及四個圓點。</li>
<li>半圓弧線的外圈半徑為100單位，內圈半徑為50單位，弧線寬度為50單位；</li>
<li>直線寬度為50單位；</li>
<li>圓點直徑為54單位，其中1單位與弧線重疊。</li>
</ul></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>來源</p></td>
<td><p>按照<a href="http://www.districtcouncils.gov.hk/archive/east/chinese/welcome.htm">舊版網頁圖片</a>，由<a href="../Page/User:Wrightbus.md" title="wikilink">Wrightbus以記事本重新繪製</a><br />
（<a href="http://www.districtcouncils.gov.hk/east/tc/welcome.html">新版網頁圖片</a>採用了本頁重製版本）</p></td>
</tr>
<tr class="even">
<td><p>日期</p></td>
<td><p>1982年 (採用)<br />
2006年11月6日 (SVG)</p></td>
</tr>
<tr class="odd">
<td><p>作者</p></td>
<td><p><a href="../Page/User:Wrightbus.md" title="wikilink">Wrightbus</a> (SVG原始碼)</p></td>
</tr>
<tr class="even">
<td><p>許可</p></td>
<td><p>合理使用</p></td>
</tr>
</tbody>
</table>

## 许可协议

[Category:香港區議會徽號](../Category/香港區議會徽號.md "wikilink")