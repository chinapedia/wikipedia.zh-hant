[thumb](../Page/圖像:Viscontisforzatarot.jpg.md "wikilink") – The Devil
card is a 20<sup>th</sup> Century remake of the card supposed to be
missing from the original 15<sup>th</sup> Century Deck--\>\]\]

**塔羅牌**（）是一套源自公元前3世紀～14世紀中發展形成的、集合了多國神秘學、[基督教](../Page/基督教.md "wikilink")、[猶太教](../Page/猶太教.md "wikilink")、[埃及神話](../Page/埃及神話.md "wikilink")、[星座](../Page/星座.md "wikilink")、[數字](../Page/數字.md "wikilink")[符號](../Page/符號.md "wikilink")、象徵學等嚴謹又複雜的占卜體系\[1\]\[2\]\[3\]；15世紀中期在[歐洲各國廣為流傳](../Page/歐洲.md "wikilink")，到了21世紀，全世界都可以找到塔羅牌的蹤影。

塔羅牌雖有很強大的占卜和預測事物的能力，但在10～17世紀[天主教占多數的歐洲](../Page/天主教.md "wikilink")，由於宗教保守氣氛和基督教系統禁止占卜，以至於塔羅牌逐漸放棄原本的占卜形式——在很長一段時間內擔任了[紙牌遊戲的功能](../Page/紙牌遊戲.md "wikilink")，直到最後蛻變為現在所常見的[撲克牌](../Page/撲克牌.md "wikilink")\[4\]\[5\]。

早期歐洲的塔羅牌有如[法國塔羅牌](../Page/法國塔羅牌.md "wikilink")\[6\]及義大利塔羅牌。不過，從18世紀後期開始，塔羅牌不再僅使用歐洲的民間解讀方式，[神秘主義者們把塔羅牌更加](../Page/神秘主義.md "wikilink")「科學化、規範化」，從原始民俗學形態的占卜進化為可以獨自發展出一套理論的[神秘學](../Page/神秘學.md "wikilink")[占卜](../Page/占卜.md "wikilink")，信奉塔羅牌的神秘學主義者認為塔羅牌是「平衡心理及精神地圖的引路徑、反映個人生活的鏡子、以及幫助人們去沉思的工具」。

塔羅牌跟撲克牌有著相同的規格，但是更加複雜。它有和撲克牌一樣四[組卡牌](../Page/花色.md "wikilink")，這些被稱為[小阿爾克那](../Page/小阿爾克那.md "wikilink")（簡稱：小牌）；另外塔羅牌還有被稱為[大阿爾克那的牌](../Page/大阿爾克那.md "wikilink")（簡稱：大牌），這是撲克牌所沒有的，大牌代表宇宙萬物運行的成因。小牌它的花色會因地區而異，例如：法國組合用於[北歐](../Page/北歐.md "wikilink")，歐洲南部使用拉丁組合，而[歐洲中部使用德國組合](../Page/歐洲.md "wikilink")。每組卡片都有四種花色，從1到10，另外各有四種[人頭牌](../Page/人頭牌.md "wikilink")，包括皇帝、皇后、武士和騎士，每組14張牌。此外，大牌固定有21張獨立的花色，和一張稱為[愚者的卡片](../Page/愚者.md "wikilink")；在占卜中每張大牌地位平等而且都佔有絕對的重要地位，但是在卡牌遊戲中卻是只有愚者這一張能充當頂級皇牌或能夠避免跟隨\[7\]。

在很長一段時間內，塔羅牌於整個歐洲都用作紙牌遊戲，作為占卜使用只能龜縮在秘密的團體之中\[8\]\[9\]；然而，在19世紀[英語國家中](../Page/英語國家.md "wikilink")，由於基督教的影響（基督教不允許算命）塔羅牌遊戲大多都未盛行；不過在進入21世紀后，塔羅牌在英語系國家的發展發展迅速（尤其是美國），在英語系國家主要是用於[占卜為目的](../Page/占卜.md "wikilink")，而且這個模式迅速反過來傳回歐洲，在亞洲的台灣、日本、新加坡等地塔羅牌也成為一門非常受歡迎占卜術。\[10\]
。神秘學者所說的皇牌跟愚者卡片是[大阿爾克那](../Page/大阿爾克那.md "wikilink")，而數字及四張人頭牌被稱為[小阿爾克那](../Page/小阿爾克那.md "wikilink")。那些卡牌都是由一些[超自然作家追蹤](../Page/超自然.md "wikilink")[古埃及或](../Page/古埃及.md "wikilink")[卡巴拉而製作的](../Page/卡巴拉.md "wikilink")，然而這裡沒有任何18世紀以前有關塔羅牌占卜的起源及使用方法的書面文件和證據\[11\]。

## 詞源

  - **「塔羅牌」**這個單詞是源自[義大利的](../Page/意大利.md "wikilink")「Tarocchi」，其詞源不能確定\[12\]，然而「Tarocchi」一詞於15世紀末至16世紀初被用作愚蠢的代名詞\[13\]\[14\]。在15世紀，「trionfi」一詞被用作稱為甲板。

<!-- end list -->

  - 大約於1502年，「tarocho」這個名稱首次出現於[布雷西亞](../Page/布雷西亞.md "wikilink")\[15\]。在16世紀，一種稱為「」的新遊戲迅速就變得流行起來，恰好有一種較老舊的遊戲名為「tarocchi」\[16\]。在現代的[義大利語中](../Page/意大利語.md "wikilink")，「tarocchi」一詞是[血橙的一個品種](../Page/血橙.md "wikilink")。

<!-- end list -->

  - 當Tarot一詞被傳開時，那詞語會轉為[法語的](../Page/法語.md "wikilink")「Tarot」與[德語的](../Page/德語.md "wikilink")「Tarock」。對於這個由來有很多不同的理論，很多都跟[神秘學沒有關連](../Page/神秘學.md "wikilink")\[17\]。

<!-- end list -->

  - 有理論涉及「撲克牌」的名稱，是源自義大利的[塔拉河](../Page/塔拉河_\(波河支流\).md "wikilink")，鄰近[帕爾馬](../Page/帕爾馬_\(意大利\).md "wikilink")；紙牌遊戲似乎源於義大利北部的[米蘭或](../Page/米蘭.md "wikilink")[博洛尼亞](../Page/博洛尼亞.md "wikilink")\[18\]。其他作家認為它來自[阿拉伯文](../Page/阿拉伯語.md "wikilink")
    “طرق”，這意味著「辦法」\[19\]；又或是[阿拉伯語](../Page/阿拉伯語.md "wikilink")
    “ترك”，意思是離開、遺棄、略去、遺下\[20\]。

<!-- end list -->

  - [弗朗索瓦·拉伯雷在他的](../Page/弗朗索瓦·拉伯雷.md "wikilink")《[巨人傳](../Page/巨人傳.md "wikilink")》中由巨人卡岡都亞玩的一種遊戲“tarau”\[21\]，這很可能是名稱的法國形式的最早認證。

## 歷史

[經典塔羅牌.jpg](https://zh.wikipedia.org/wiki/File:經典塔羅牌.jpg "fig:經典塔羅牌.jpg")
14世紀後期，塔羅牌首次引進歐洲，其最有可能來自[埃及](../Page/埃及.md "wikilink")[馬木留克王朝](../Page/馬木留克王朝.md "wikilink")，人們穿著西裝帶著細長的棒或馬球棒（從事占卜或是神秘學的人會稱它為魔杖），以及硬幣（通常被稱為圓盤，或是神秘學及占卜學中的「」、劍和杯。這些裝束跟現代塔羅占卜師非常相似，傳統的義大利、[西班牙和](../Page/西班牙人.md "wikilink")[葡萄牙占卜師仍然沿用](../Page/葡萄牙人.md "wikilink")\[22\]。

由於缺乏必要的文字記載，塔羅牌的起源截至今天已變得難以考證。首批塔羅牌的文件記錄載於1440年至1450年之間的[米蘭](../Page/米蘭.md "wikilink")、[費拉拉](../Page/費拉拉.md "wikilink")、[佛羅倫斯及](../Page/佛羅倫斯.md "wikilink")[波隆那](../Page/波隆那.md "wikilink")，當時人們把有寓言的插圖加入到額外的王牌卡片，置於一般的四款卡片之中。這些新的卡牌被稱為「carte
da trionfi」、「凱旋卡（triumph cards）」，而其他卡片簡稱為「」，成為了英語中的「王牌」。

1425年，有記錄指塔羅牌作為遊戲使用，被稱為[塔羅牌遊戲](../Page/塔羅牌遊戲.md "wikilink")，如[法國塔羅牌](../Page/法國塔羅牌.md "wikilink")\[23\]。截至1440年塔羅牌才有正式的記錄，佛羅倫薩的法庭記錄中發現有關Trionfi的書面陳述，有關貴族把兩名占卜師轉移的事情\[24\]\[25\]。

關於塔羅牌的起源有很多說法，最古老並已失傳的類似塔羅牌，是15世紀中葉受到[米蘭公國統治者](../Page/米蘭公國.md "wikilink")[菲利波·瑪麗亞·維斯孔蒂公爵的委託而繪製塔羅牌](../Page/菲利波·瑪麗亞·維斯孔蒂.md "wikilink")\[26\]，據所描述，該繪製者於1418年回到米蘭，而馬提安奴於1425年去世，故該套塔羅牌可能繪於1418年到1425年之間。他描述該套塔羅牌有60張卡片，當中有16張卡片上有[希臘神話的圖像](../Page/希臘神話.md "wikilink")，而套裝有4種鳥類。該16張牌被認為是「王牌（trumps）」。雅格布·安東尼奧·馬塞羅（Jacopo
Antonio
Marcello）於1449年回憶說，當時經已去世的公爵發明了「一種新穎及精緻的凱旋卡」\[27\]。其他早期的塔羅牌也展示了包括1940年代的及波達度-維提（Boiardo-Viti）的經典圖案塔羅牌\[28\]。

在[佛羅倫斯](../Page/佛羅倫斯.md "wikilink")，人們利用一個名為「」的擴展卡片，該套卡片有97張牌，包括占星符號和四個元素，以及傳統的塔羅牌圖案\[29\]。然而在15世紀的一篇講道中，有一位多米尼加傳教士猛烈抨擊，指卡片中有邪惡（主要是因為人們在賭博中使用）\[30\]，但在早期歷史中並沒有發現塔羅牌的常規譴責\[31\]。

由於早期的塔羅牌都是手繪的，所以數量被認為是相當少的。隨著印刷機發明之後才有機會大規模生產。塔羅牌於義大利以外的地區擴張，首先是在[義大利戰爭期間於法國和瑞士流傳](../Page/意大利戰爭.md "wikilink")。此兩個地方沿用的是米蘭的馬賽塔羅牌\[32\]。

另一說法，目前得到較多人支持的是源於義大利的一種宮廷牌塔羅奇（Tarocchi），另一種較多人認同的起源是：塔羅牌是為了教授卡巴拉系統而被創造。其22張大牌正好對應到卡巴拉中的22條路徑。在英語世界裡，塔羅牌普遍被視為一種紙牌占卜的形式。塔羅牌長久以來被視為一種[禁忌](../Page/禁忌.md "wikilink")，因為它和19世紀的[神祕學有曖昧的關連](../Page/神祕學.md "wikilink")。羅馬[天主教講道中抨擊紙牌天生帶有邪惡](../Page/天主教.md "wikilink")，最早可以追溯到14世紀。

隨著心理學的發展和新時代運動的興起，現代人更傾向於將塔羅牌作爲一種自我探索和發展的工具。世界各地的一些心理醫生將塔羅牌作爲一種輔助治療手段加以運用。透過塔羅牌對自我內在的探索，引導個人獲得更大的自由。同時，也將其從神秘學的未知領域，提升到可以被認知的更廣闊的生命智慧的範疇。塔羅牌主要分為[馬賽體系](../Page/馬賽體系.md "wikilink")、[偉特體系](../Page/偉特體系.md "wikilink")、[托特體系](../Page/托特體系.md "wikilink")，不同的體系之間，對於每張牌的解釋不盡相同，大牌的排列順序也不完全一致。

## 牌具

傳統上塔羅牌有78張牌，由兩種不同的牌組所組成。第一種稱為[大阿爾克那](../Page/大阿爾克那.md "wikilink")，由22張沒有[花色的牌所組成](../Page/花色.md "wikilink")，相當於所謂的「[王牌](../Page/王牌.md "wikilink")」。第二種稱為[小阿爾克那](../Page/小阿爾克那.md "wikilink")（Arcanes
mineurs），由56張牌所組成，而這56張牌又可以分成4種花色。每種花色有數字2到數字10的牌，加上一張[Ace和四張宮廷牌](../Page/Ace.md "wikilink")。阿爾克那*arcana*是[拉丁文](../Page/拉丁文.md "wikilink")*arcanum*的複數，意思為「隱藏的真理」或「秘密的知識」。其他的稱呼有大塔羅／小塔羅、大秘儀／小秘儀、主牌／副牌、大牌／小牌、Arcanes
mineurs／Arcanes
majeurs等等。小阿爾克那的傳統[義大利式牌組為](../Page/義大利.md "wikilink")[寶劍](../Page/寶劍.md "wikilink")（épées）、[權杖](../Page/權杖.md "wikilink")（bâtons）、[聖杯](../Page/聖杯.md "wikilink")（coupes）和[錢幣](../Page/錢幣_\(塔羅牌\).md "wikilink")（deniers）。隨著心理學的發展和[新時代運動的興起](../Page/新時代運動.md "wikilink")，現代人更傾向于將塔羅牌作爲一種自我探索和發展的工具。世界各地的一些心理醫生將塔羅牌作爲一種輔助治療手段加以運用
。

[Tarockkarten_in_der_Hand_eines_Spielers.jpg](https://zh.wikipedia.org/wiki/File:Tarockkarten_in_der_Hand_eines_Spielers.jpg "fig:Tarockkarten_in_der_Hand_eines_Spielers.jpg")

### 主要體系

塔羅牌主要分為[馬賽體系](../Page/馬賽體系.md "wikilink")、[偉特體系](../Page/偉特體系.md "wikilink")、[托特體系](../Page/托特體系.md "wikilink")，[威斯康緹塔羅](../Page/威斯康緹塔羅.md "wikilink")，其他體系，不同的體系之間，對於每張牌的解釋不盡相同，大牌的排列順序也不完全一致。

#### 馬賽體系

[馬賽體系出現的時間比偉特體系和托特體系都來得早](../Page/馬賽體系.md "wikilink")。流行於法國南部，常做為塔羅牌遊戲用，代表作是[馬賽塔羅牌](../Page/馬賽塔羅牌.md "wikilink")（Tarot
de
Marseilles），它代表着最流行的古代塔羅體系，不論在牌面结構、释義和牌的编号上都和现在的塔罗牌有較大差異。也有專用於遊戲的塔羅牌，如法國塔羅牌。
做為經典塔羅(Classical
Tarot)的是被稱做威斯康緹塔羅牌仿繪品。威牌被認為中古世紀就已發現和流傳。這也是部分占卜師解牌師主要用於收藏。
威斯康緹塔羅牌可做為任何解，而做為繼承者的馬賽塔羅，當然是以承襲其牌義為主。但主要來說，馬賽塔羅和偉特牌又有藝曲同工。也有人認為不該和偉特牌同一而論，但主要還是因著解牌師的解法和說法而有不同。

#### 偉特體系

[偉特體系](../Page/偉特體系.md "wikilink")，為目前最普遍流行，其代表是由[亞瑟·愛德華·偉特](../Page/亞瑟·愛德華·偉特.md "wikilink")（Arthur
Edward
Waite）所設計的[萊德偉特塔羅牌](../Page/萊德偉特.md "wikilink")。韋特牌有很多牌本，都是由不同的畫家上色的，有Aquatic
Tarot和Albano-Waite
Tarot等。現在最受歡迎的其中一款是普及版偉特塔羅牌，是[美國遊戲公司](../Page/美國遊戲公司.md "wikilink")（US
Games Systems, Inc.）出版，由美國畫家Mary
Hanson-Roberts上色，使用的是原牌的構圖，但顏色鮮艷，以及運用漸層色彩，令單調的牌面更覺豐富。[愛德華·偉特是](../Page/愛德華·偉特.md "wikilink")19世紀末英國著名的[神秘學團體](../Page/神秘學.md "wikilink")[黄金黎明協會的會員](../Page/黄金黎明協會.md "wikilink")。[愛德華·偉特](../Page/愛德華·偉特.md "wikilink")（A.E.Waite）設計的塔羅牌，最初的牌是黑白的，因為受歡迎，後來由史密斯女士（Pamela
Colman
Smith）上色。該體系改善了原本[小阿爾克那缺乏圖像和故事的缺點](../Page/小阿爾克那.md "wikilink")，加上了豐富的象徵和顯而易懂的圖像，初學者也不難從牌面猜測牌義，成為近代最受歡迎的塔羅牌。韋特牌的圖案內藏不少暗示，其中有占星學、數字學、顏色學，且牌中內容更與神話或聖經故事結合，如「6．戀人」裡的亞當和夏娃、「20．審判」裡上帝的审判與死人之復活。由於牌面已充滿了意義，解牌時會令牌師能更流暢及更易引發聯想，故出版後即為不少牌師所選用。最初推出韋特牌的是美國的騎士出版社，韋特牌又稱「騎士韋特牌」（Rider-Waite），A.E.韋特亦於推出此牌的同時推出了此牌的使用手冊（A
Pictorial Key to Tarot）。

近年來發行的眾多琳瑯滿目的塔羅牌，大多遵從偉特體系，雖然稍有差異，但多不離其本。現在，大多數的塔羅書籍和網站也是使用韋特牌來介紹塔羅牌这一來由已久的占卜工具。

#### 托特體系

[托特體系的代表作是托特塔羅牌](../Page/托特體系.md "wikilink")，他的設計者[亞歷斯特·克勞利也來自](../Page/亞歷斯特·克勞利.md "wikilink")[黄金黎明協會](../Page/黄金黎明協會.md "wikilink")，並且據稱是根據[托特之書](../Page/托特之書.md "wikilink")（Book
of
Thoth）設計的，用上較多的[神秘學的象徵圖案或符號](../Page/神秘學.md "wikilink")。[亞歷斯特·克勞利在](../Page/亞歷斯特·克勞利.md "wikilink")1920年代曾被稱為是世界上最邪惡的男人，他設計的這副塔羅牌也因為其黑魔法師的背景，而添加了神祕的色彩。此牌更加抽象，強調塔羅牌的神祕學意義，運用了眾多的符號，適合作為[靈修之用](../Page/靈修.md "wikilink")。

#### 其他體系

另外亦有很多具主題性的塔羅牌，如女神塔羅牌、動物塔羅牌、性愛塔羅牌等，都以各自主題配合一般塔羅牌牌義而成。較特別的還有如[奧修禪卡](../Page/奧修禪卡.md "wikilink")（Osho
Zen Tarot），主要是藉由牌上特殊的圖像，來顯示占卜者身心靈的狀況，此種塔羅牌則多用於檢視自身。

#### 直覺塔羅

直覺塔羅一詞通常指[托特塔羅](../Page/托特塔羅.md "wikilink")，是因其參入了[心理學](../Page/心理學.md "wikilink")，[神祕學](../Page/神祕學.md "wikilink")，[符號學等表現元素和人物](../Page/符號學.md "wikilink")。若要實際來說，托特和偉特，馬賽解法並無不同。但托特卻又多了那麼一絲特別感，也少人專研。普遍的來說，如果在不違反由抽牌者依其心智運行抽牌動作，任何塔羅都能做為直覺塔羅。

## 象徵主義

[卡尔·荣格](../Page/卡尔·荣格.md "wikilink")（Carl
Jung）是第一位正視塔羅牌並給予正面評價的[心理學家](../Page/心理學家.md "wikilink")。他将塔羅牌视为有象徵性的[原型](../Page/原型.md "wikilink")：人的基本類型或境遇都隱含在人類所有的[潛意識行為中](../Page/潛意識.md "wikilink")。比如“[皇帝](../Page/皇帝.md "wikilink")（L'Empereur）”，是最高權威（家長）或父親的形象的象徵。

這種原型理論引發了很多種心理學方面的應用。一些心理學家讓病人選擇一張適合他的塔羅牌，協助心理學家辨識該病人如何看待自己。有些心理學家試圖利用塔羅牌圖像使病人通過想像他的境遇或牌與他的关联以釐清他的想法：是像「[寶劍騎士](../Page/寶劍騎士.md "wikilink")」（le
Chevalier
d'Épées）那樣急性子和衝動呢？還是像在[韋特牌中](../Page/韋特牌.md "wikilink")“[寶劍二](../Page/寶劍二.md "wikilink")（Deux
d'Épées）”那樣盲目、逃避？塔羅牌可被視為潛意識的一種表達方式，只要遵循著一定的規範就可以對潛意識的認知進行解讀。

有趣的是，[Visconti-Sforza牌和](../Page/Visconti-Sforza.md "wikilink")[Marseille牌這樣以前的牌面比現代塔羅牌更倾於粗糙及少有](../Page/Marseille.md "wikilink")“代數”表達。這不僅是現代藝術的錯覺，它反應出以往幾世纪在塔羅牌技藝上重大變革的方向，尤其自1900年起。塔羅牌的象徵意義已經非常成功的創立下良好的典範。

塔羅牌也會因應現代時代趨勢而牌面的解釋有所改變，並非一成不變的解釋，但是在原形的理論基礎方向內是一致的，會有所改變的是牌內依照現今社會風潮而解釋有所變化。不僅在心理學上可以使用參考，對於很多邏輯性的推敲也有很大的幫助。

以學術心理學的角度來詮釋塔羅牌，不應該用[占卜兩個字](../Page/占卜.md "wikilink")。正確的用語是[讀牌](../Page/讀牌.md "wikilink")。以英文的全名為Tarot
Reading以當今翻譯為占卜確實會誤導對於塔羅牌真正的詮釋意義。

塔羅牌之所以有大阿爾克那（Arcanes majeurs，又稱大祕儀）與小阿爾克那（Arcanes
mineurs，又稱小祕儀）的分別，可以追溯至中古歐洲的宗教觀及世界觀。當時的人們普遍認為這個世界分為「聖」跟「俗」這兩種相對而極端的境界。[大阿爾克那代表的是聖](../Page/大阿爾克那.md "wikilink")、天界、抽象而偉大的觀念，[小阿爾克那代表的則是俗](../Page/小阿爾克那.md "wikilink")、凡人、芸芸眾生、具體而實際的日常處境、心境。

## 占卜法

### 牌陣

依照塔羅牌的牌陣而言有所謂的固定牌陣解讀法，塔羅牌的牌陣取決於讀牌者的經驗，塔羅牌的牌陣可以是非常活躍並不需要按照固定的牌陣使用。塔羅牌為邏輯應用訓練的一項工具。塔羅牌的牌陣分為三種：一種為正逆牌混合、一種為全正向讀牌、一種為全逆向讀牌。
塔羅牌所反應以集體意識為原形導向理論為出發點，所有的牌陣及讀牌的方式會有一個邏輯性。

#### 凱爾特十字占卜法

凱爾特十字占卜法是最著名的展開法牌陣之一。屬於較高級的[牌陣](../Page/牌陣.md "wikilink")，學習過塔羅牌有一段時間的人才比較容易掌握，初學者應選用其他較簡單的[牌陣](../Page/牌陣.md "wikilink")。它的陣式一般包含十或十一張牌。第一張是[切牌或](../Page/切牌.md "wikilink")[指示牌](../Page/指示牌.md "wikilink")，表示問卜者本人或狀態（這有時是可選的，所以這種展開法牌陣也可只用到十張），接下來的六張牌放在第一張牌的上部和周圍形成十字型，最後的四張在右側排一豎列。

### 牌義

總共78張牌，分為大阿爾克那與小阿爾克那

**大牌**0（愚人）～21（世界）總共22張牌，象徵一個人自我追尋與成長的故事。

第一張牌0號牌愚人，收拾包袱昂首向前，隱含人生開始之意。

之後便要經歷三個層面的洗禮

第一層為「物質」層面，由1號牌魔術師到7號牌戰車。 第二層為「心靈」層面，由8號牌力量到14號牌節制。
第三層為「精神」層面，由15號牌惡魔到21號牌世界。

**小牌**較大牌生活化，也較易理解，可分為四個屬性，每個屬性包括公廷牌4張，數字牌10張，故總共56張。

權杖-火，代表熱情、行動力與創造力。

寶劍-風，代表智慧、果斷與傷害。

聖杯-水，代表心靈、感情與人際關係。

錢幣-土，代表物質、工作與享受。

#### 大阿爾克那（Arcanes majeurs）

大阿爾克那有二十二張，分別為：

  - 0號：[愚者](../Page/愚者.md "wikilink")（Le Fou / Le Mat）
  - 1號：[魔術師](../Page/魔术师_\(塔罗牌\).md "wikilink")（Le Bateleur）
  - 2號：[女祭司](../Page/女祭司_\(塔羅牌\).md "wikilink")（La Papesse）
  - 3號：[皇后](../Page/皇后_\(塔罗牌\).md "wikilink")（L'Impératrice）
  - 4號：[皇帝](../Page/皇帝_\(塔罗牌\).md "wikilink")（L'Empereur）
  - 5號：[教宗](../Page/教宗_\(塔羅牌\).md "wikilink")（Le Pape）
  - 6號：[戀人](../Page/恋人_\(塔罗牌\).md "wikilink")（L'Amoureux）
  - 7號：[戰車](../Page/战车_\(塔罗牌\).md "wikilink")（Le Chariot）
  - 8號：[正義](../Page/正义_\(塔罗牌\).md "wikilink")（La Justice）/
    8號：[力量](../Page/力量_\(塔罗牌\).md "wikilink")（La Force）
  - 9號：[隱者](../Page/隱者.md "wikilink")（L'Ermite）
  - 10號：[命運之輪](../Page/命運之輪.md "wikilink")（Le Roue de Fortune）
  - 11號：[力量](../Page/力量_\(塔罗牌\).md "wikilink")（La Force） /
    11號：[正義](../Page/正义_\(塔罗牌\).md "wikilink")（La Justice）
  - 12號：[倒吊人](../Page/倒吊人.md "wikilink")（Le Pendu）
  - 13號：[死亡](../Page/死亡_\(塔罗牌\).md "wikilink")（La Mort）
  - 14號：[節制](../Page/節制.md "wikilink")（Tempérance）
  - 15號：[惡魔](../Page/惡魔_\(塔罗牌\).md "wikilink")（Le Diable）
  - 16號：[高塔](../Page/塔_\(塔罗牌\).md "wikilink")（La Maison Dieu）
  - 17號：[星星](../Page/星星_\(塔罗牌\).md "wikilink")（L'Étoile）
  - 18號：[月亮](../Page/月亮_\(塔罗牌\).md "wikilink")（Le Lune）
  - 19號：[太陽](../Page/太阳_\(塔罗牌\).md "wikilink")（Le Soleil）
  - 20號：[審判](../Page/审判_\(塔罗牌\).md "wikilink")（Le Jugement）
  - 21號：[世界](../Page/世界_\(塔罗牌\).md "wikilink")（Le Monde）

##### 「力量」和「正義」的順序

其實在以往的傳統演進上，力量是在第11張，而正義是第8張牌。這是固定的配置。

在馬賽體系，以及托特體系的塔羅，仍然對於這種配置方法加以保留，所以這兩種塔羅，以及其系統下的塔羅，都是可以見到以8號對應正義，11號對應力量的排列方法。與星象要素的連結上，力量所對應的是獅子座；正義對應的是天秤座。原始的排列其實也就等於是星座的序列顛倒過來，為使星座順序不對調，就只能調整兩張牌的順序了。

在偉特設計塔羅的過程中，參酌了卡巴拉，占星與靈數的觀念，並與之進行連結對應。

其次，以靈數的觀點：8的能量循環與交換不息，還有（內在）力量的統合角度來說，Strength是最能符合這個概念的。11等同於2，2有分別、揀擇、均衡等意涵，所以11的平衡對應，與正義的內容是相呼應的。

所以這是不同的塔羅體系下的差異。不至於影響解析上的精準。

#### 小阿爾克那（Arcanes mineurs）

小阿爾克那為總數56張牌所組成，分為四種屬性兩種牌組。簡言之，四種屬性及火（權杖）、水（聖杯）、風（寶劍）、土（錢幣）這[四元素](../Page/四元素.md "wikilink")；兩種牌組以及四種屬性的牌組各自可分為數字牌與宮廷牌兩組。

##### 寶劍牌組

寶劍牌組是小祕儀中表示風原素的象徵。

（一）數字牌組

  - 寶劍As或稱寶劍一
  - 寶劍二
  - 寶劍三
  - 寶劍四
  - 寶劍五
  - 寶劍六
  - 寶劍七
  - 寶劍八
  - 寶劍九
  - 寶劍十

（二）宮廷牌組

  - 寶劍隨從，風原素中風的展現。（風之風）
  - 寶劍騎士，風原素中火的展現。（風之火）
  - 寶劍王后，風原素中水的展現。（風之水）
  - 寶劍國王，風原素中土的展現。（風之土）

##### 權杖牌組

權杖牌組是小祕儀中表示火原素的象徵。

（一）數字牌組

  - 權杖Ace或稱權杖一
  - 權杖二
  - 權杖三
  - 權杖四
  - 權杖五
  - 權杖六
  - 權杖七
  - 權杖八
  - 權杖九
  - 權杖十

（二）宮廷牌組

  - 權杖隨從，火原素中風的展現。（火之風）
  - 權杖騎士，火原素中火的展現。（火之火）
  - 權杖王后，火原素中水的展現。（火之水）
  - 權杖國王，火原素中土的展現。（火之土）

##### 聖杯牌組

聖杯牌組是小祕儀中表示水原素的象徵。

（一）數字牌組

  - 聖杯Ace或稱聖杯一
  - 聖杯二
  - 聖杯三
  - 聖杯四
  - 聖杯五
  - 聖杯六
  - 聖杯七
  - 聖杯八
  - 聖杯九
  - 聖杯十

（二）宮廷牌組

  - 聖杯隨從，水原素中風的展現。（水之風）
  - 聖杯騎士，水原素中火的展現。（水之火）
  - 聖杯王后，水原素中水的展現。（水之水）
  - 聖杯國王，水原素中土的展現。（水之土）

##### 錢幣牌組

錢幣牌組是小祕儀中表示土原素的象徵。

（一）數字牌組

  - 錢幣Ace或稱錢幣一
  - 錢幣二
  - 錢幣三
  - 錢幣四
  - 錢幣五
  - 錢幣六
  - 錢幣七
  - 錢幣八
  - 錢幣九
  - 錢幣十

（二）宮廷牌組

  - 錢幣隨從，土原素中風的展現。（土之風）
  - 錢幣騎士，土原素中火的展現。（土之火）
  - 錢幣王后，土原素中水的展現。（土之水）
  - 錢幣國王，土原素中土的展現。（土之土）

#### 其他

##### 空白牌（Bl）

空白牌是一張沒有任何圖案，只有邊框的一張塔羅牌， 一般在一組塔羅牌裡面會至少附上一張（有些版本的塔羅牌會附上兩張或是不附上），
使用方式因每位占卜師習慣而有所不同，

以下為目前较为常见的使用方式：

  - 做為塔羅牌時的替代品。在做為替代品時，空白牌將直接取代遺失牌的意義；

<!-- end list -->

  - 保護塔羅牌損壞。將空白牌放置於整組塔羅牌的最上方與最下方，如此可以避免塔羅牌因攜帶時造成的不必要磨損；

<!-- end list -->

  - 做為一張具有意義的牌。將空白牌納入整副塔羅牌中進行占卜，當使用者抽出時因應牌陣，進行解讀。

牌義：空白牌本身大方向的含義為「未知的事物、空洞、虛無（choses
inconnues）」泛指一切未知的事情，問卜者的問題發展方向會朝向失控或是難以想像的局面發展。

##### 占星學對應關係

每個塔羅牌占卜師都有不同的見解，然而每張塔羅牌的圖案所對應的星座也有不同，

###### 大秘儀

  - 愚人（Le Fou, Le Mat）：[天王星](../Page/天王星.md "wikilink")
  - 魔法師（Le Bateleur）：[水星](../Page/水星.md "wikilink")
  - 女祭司（La Papesse）：[月亮](../Page/月亮.md "wikilink")
  - 皇后（L'Impératrice）：[金星](../Page/金星.md "wikilink")
  - 皇帝（L'Empereur）：[白羊座](../Page/白羊座.md "wikilink")
  - 教宗（Le Pape）：[金牛座](../Page/金牛座.md "wikilink")
  - 戀人（L'Amoureux）：[雙子座](../Page/雙子座.md "wikilink")
  - 戰車（Le Chariot）：[巨蟹座](../Page/巨蟹座.md "wikilink")
  - 力量（La Force）：[獅子座](../Page/獅子座.md "wikilink")
  - 隱者（L'Ermite）：[室女座](../Page/室女座.md "wikilink")
  - 命運之輪（Le Roue de Fortune）：[木星](../Page/木星.md "wikilink")
  - 正義（La Justice）：[天秤座](../Page/天秤座.md "wikilink")
  - 倒吊人（Le Pendu）：[海王星](../Page/海王星.md "wikilink")
  - 死神（La Mort）：[天蠍座](../Page/天蠍座.md "wikilink")
  - 節制（Tempérance）：[人馬座](../Page/人馬座.md "wikilink")
  - 惡魔（Le Diable）：[摩羯座](../Page/摩羯座.md "wikilink")
  - 高塔（La Maison Dieu）：[火星](../Page/火星.md "wikilink")
  - 星星（L'Étoile）：[寶瓶座](../Page/寶瓶座.md "wikilink")
  - 月亮（La Lune）：[雙魚座](../Page/雙魚座.md "wikilink")
  - 太陽（Le Soleil）：[太陽](../Page/太陽.md "wikilink")
  - 審判（Le Jugement）：[冥王星](../Page/冥王星.md "wikilink")
  - 世界（Le Monde）：[土星](../Page/土星.md "wikilink")

###### 小秘儀

  - 寶劍（épées）：[風象星座](../Page/風象星座.md "wikilink")
  - 權杖（bâtons）：[火象星座](../Page/火象星座.md "wikilink")
  - 聖杯（coupes）：[水象星座](../Page/水象星座.md "wikilink")
  - 錢幣（deniers）：[土象星座](../Page/土象星座.md "wikilink")

## 相關學科

  - 卡巴拉靈數學
  - 也門占星學
  - 西洋占星學

## 註釋

## 参考文献

### 引用

### 来源

  - *The Game of Tarot* by [Michael
    Dummett](../Page/Michael_Dummett.md "wikilink") ISBN
    978-0-7156-1014-5 - a history of the Tarot, and a compilation of
    Tarot card games.（out of print）
  - *A Wicked Pack of Cards: The Origins of the Occult Tarot* by Ronald
    Decker, Thierry de Paulis, and [Michael
    Dummett](../Page/Michael_Dummett.md "wikilink") ISBN
    978-0-312-16294-8 - a history of the French origin of the occult
    Tarot, focusing on Etteilla, Le Normand, and Lévi.
  - *A History of the Occult Tarot* by Ronald Decker and [Michael
    Dummett](../Page/Michael_Dummett.md "wikilink"), ISBN
    978-0-7156-3122-5, continuing the story of the occult Tarot through
    the Golden Dawn tradition and its reception in the English-speaking
    world.
  - *Seventy-Eight Degrees of Wisdom* by [Rachel
    Pollack](../Page/Rachel_Pollack.md "wikilink") - comprehensive,
    covering and the minor as well as the major arcana; and taking
    several angles on the Tarot.
  - *Complete Guide to the Tarot* by Eden Grey - concentrates on
    classical divination, but has some information on the more spiritual
    aspects.
  - <cite>Qabalistic Tarot</cite> by Robert Wang - a comprehensive and
    highly regarded, but frequently challenging, reference to the
    esoteric aspects of Tarot.
  - *The Encyclopedia of Tarot,* Stuart Kaplan, 3 vols - repertory of
    illustrations and history..

## 外部連結

  - [塔羅牌的禁忌](http://wicca.tw/%E5%A1%94%E7%BE%85%E7%89%8C%E7%9A%84%E7%A6%81%E5%BF%8C/)
  - [如何開始學習塔羅牌](https://wicca.tw/%E5%A6%82%E4%BD%95%E9%96%8B%E5%A7%8B%E5%AD%B8%E7%BF%92%E5%A1%94%E7%BE%85%E7%89%8C/)
  - [新手如何挑選塔羅牌](https://wicca.tw/%E6%96%B0%E6%89%8B%E5%A6%82%E4%BD%95%E6%8C%91%E9%81%B8%E5%A1%94%E7%BE%85%E7%89%8C/)
  - [塔羅牌的看圖說故事](https://wicca.tw/%E5%A1%94%E7%BE%85%E7%89%8C%E7%9A%84%E7%9C%8B%E5%9C%96%E8%AA%AA%E6%95%85%E4%BA%8B/)

{{-}}

[T](../Category/占卜.md "wikilink") [塔羅牌](../Category/塔羅牌.md "wikilink")

1.  Vitali, Andrea. [About the etymology of
    Tarocco](http://www.letarot.it/page.aspx?id=220) at Le Tarot
    Cultural Association. Retrieved 4 February 2018.

2.  Vitali, Andrea. [Taroch
    - 1494](http://www.letarot.it/page.aspx?id=264) at Le Tarot Cultural
    Association. Retrieved 4 February 2018.

3.

4.

5.

6.  [Description of the Michelino deck - Translated
    text](http://trionfi.com/0/b/11/t1.php)

7.

8.

9.  Pratesi, Franco. [Studies on Giusto
    Giusti](http://trionfi.com/giusto-giusti) at trionfi.com. Retrieved
    4 February 2018.

10.
11.
12.
13. Vitali, Andrea. [About the etymology of
    Tarocco](http://www.letarot.it/page.aspx?id=220) at Le Tarot
    Cultural Association. Retrieved 4 February 2018.

14. Vitali, Andrea. [Taroch
    - 1494](http://www.letarot.it/page.aspx?id=264) at Le Tarot Cultural
    Association. Retrieved 4 February 2018.

15.

16.
17. [About the etymology of
    Tarot](http://www.letarot.it/page.aspx?id=220&lng=eng) , Michael S.
    Howard - Le Tarot

18. [Cassandra Eason](../Page/Cassandra_Eason.md "wikilink"), *Complete
    Guide to Tarot*, p. 3 (Crossing Press, 2000; ISBN 978-1-58091-068-2)

19.

20. [Etymology for
    *Tarot*](http://www.etymonline.com/index.php?search=tarot&searchmode=none),
    Douglas Harper - The [Online Etymology
    Dictionary](../Page/Online_Etymology_Dictionary.md "wikilink")

21. [François Rabelais](../Page/François_Rabelais.md "wikilink"),
    *Gargantua and Pantagruel*, ch. 22, "Les Jeux de Gargantua"

22. [Donald Laycock](../Page/Donald_Laycock.md "wikilink") in
    *Skeptical—a Handbook of Pseudoscience and the Paranormal*, ed
    [Donald Laycock](../Page/Donald_Laycock.md "wikilink"), [David
    Vernon](../Page/David_Vernon_\(writer\).md "wikilink"), [Colin
    Groves](../Page/Colin_Groves.md "wikilink"), [Simon
    Brown](../Page/Simon_Brown_\(author\).md "wikilink"), Imagecraft,
    Canberra, 1989, , p. 67

23. [Description of the Michelino deck - Translated
    text](http://trionfi.com/0/b/11/t1.php)

24.

25. Pratesi, Franco. [Studies on Giusto
    Giusti](http://trionfi.com/giusto-giusti) at trionfi.com. Retrieved
    4 February 2018.

26.

27.

28.
29.
30. Robert Steele. A Notice of the Ludus Triumphorum and some Early
    Italian Card Games; With Some Remarks on the Origin of the Playing
    Cards." **Archaeologia,** vol LVII, 1900: pp 185-200.

31.
32.