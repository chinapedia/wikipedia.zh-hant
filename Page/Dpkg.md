**dpkg**是[Debian](../Page/Debian.md "wikilink")[软件包管理器的基础](../Page/软件包管理器.md "wikilink")，它被[伊恩·默多克创建于](../Page/伊恩·默多克.md "wikilink")1993年。dpkg与[RPM十分相似](../Page/RPM_Package_Manager.md "wikilink")，同样被用于安装、卸载和供给和[.deb软件包相关的信息](../Page/deb.md "wikilink")。

dpkg本身是一个底层的工具。上层的工具，像是[APT](../Page/高级包装工具.md "wikilink")，被用于从远程获取软件包以及处理复杂的软件包关系。
“dpkg”是“Debian Package”的简写。

## 外部链接

  - [Dpkg.org](http://www.dpkg.org/)
  - [Debian的dpkg软件包](http://packages.debian.org/dpkg)

<references />

[Dpkg](../Category/Dpkg.md "wikilink")
[Category:用C編程的自由軟體](../Category/用C編程的自由軟體.md "wikilink")