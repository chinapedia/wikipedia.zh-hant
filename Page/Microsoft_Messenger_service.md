**Messenger**（前稱：**MSN Messenger Service**、**.NET Messenger
Service**、**Windows Live Messenger
Service**）是[微軟開發的](../Page/微軟.md "wikilink")[即時通訊和](../Page/即時通訊.md "wikilink")[在線系統](../Page/在線.md "wikilink")，使用專有的即時消息協議Microsoft通知協議進行通信。允許任何擁有Microsoft帳戶的人登錄並與其他已登錄的人實時通信。

## 歷史

儘管多年來對服務及其客戶端軟件的名稱進行了多次更改，但由於MSN
Messenger的歷史，信使服務通常被簡稱為MSN。該服務本身在1999年至2001年間被稱為MSN
Messenger Service，此時更名為.NET Messenger
Service，並開始提供不再攜帶MSN名稱的客戶端，例如Windows
Messenger客戶端包含在Windows XP中，最初的目的是成為MSN
Messenger的簡化版本，免費投放廣告並集成到Windows中。

微軟在2005年底之前仍繼續提供更多的MSN Messenger升級服務，當時所有以前版本的MSN Messenger和Windows
Messenger都被新程序Windows Live Messenger取代，這是微軟推出Windows Live在線服務的一部分。

幾年來，該服務的官方名稱仍然是.NET Messenger
Service，正如其官方網絡狀態網頁上所指出的，儘管微軟很少使用該名稱來推廣該服務。由於用於訪問該服務的主要客戶端被稱為Windows
Live Messenger，微軟在2000年代中期在其支持文檔中開始將整個服務稱為Windows Live Messenger服務。

該服務可以與Windows操作系統集成，在用戶登錄到其Windows帳戶時自動並同時登錄網絡。組織也可以將他們的Microsoft Office
Communications Server和Active
Directory與服務集成。微軟在2011年12月發布了Messenger服務的XMPP接口。

作為重塑其許多Windows Live服務的更大努力的一部分，微軟將該服務稱為Messenger，後來整合至Skype即時消息功能。

## 客戶端軟件

  - [Windows 8](../Page/Windows_8.md "wikilink")
  - [Windows Live
    Messenger](../Page/Windows_Live_Messenger.md "wikilink")
      - [MSN Messenger](../Page/MSN_Messenger.md "wikilink")
      - [Windows Messenger](../Page/Windows_Messenger.md "wikilink")
  - [Microsoft Messenger for
    Mac](../Page/Microsoft_Messenger_for_Mac.md "wikilink")
  - [Outlook.com](../Page/Outlook.com.md "wikilink")
      - [Hotmail](../Page/Hotmail.md "wikilink")
      - [Windows Live Web
        Messenger](../Page/Windows_Live_Web_Messenger.md "wikilink")
      - [MSN Web Messenger](../Page/MSN_Web_Messenger.md "wikilink")
  - [Xbox Live](../Page/Xbox_Live.md "wikilink")
  - [Messenger on Windows
    Phone](../Page/Messenger_on_Windows_Phone.md "wikilink")
  - [Windows Live Messenger for iPhone and iPod
    Touch](../Page/Windows_Live_Messenger_for_iPhone_and_iPod_Touch.md "wikilink")
  - [Messenger Play\!](../Page/Messenger_Play!.md "wikilink")
  - [Windows Live Messenger for
    Nokia](../Page/Windows_Live_Messenger_for_Nokia.md "wikilink")
  - [Windows Live Messenger for
    BlackBerry](../Page/Windows_Live_Messenger_for_BlackBerry.md "wikilink")
  - [Adium](../Page/Adium.md "wikilink")
  - [aMSN](../Page/aMSN.md "wikilink")
  - [Ayttm](../Page/Ayttm.md "wikilink")
  - [BitlBee](../Page/BitlBee.md "wikilink")
  - [CenterIM](../Page/CenterIM.md "wikilink")
  - [emesene](../Page/emesene.md "wikilink")
  - [Empathy](../Page/Empathy.md "wikilink")
  - [eBuddy](../Page/eBuddy.md "wikilink")
  - [Fire](../Page/Fire.md "wikilink")
  - [XMPP](../Page/Extensible_Messaging_and_Presence_Protocol.md "wikilink")
  - [Kopete](../Page/Kopete.md "wikilink")
  - [Meebo](../Page/Meebo.md "wikilink")
  - [Meetro](../Page/Meetro.md "wikilink")
  - [Miranda IM](../Page/Miranda_IM.md "wikilink")
  - [Pidgin](../Page/Pidgin.md "wikilink")
  - [tmsnc](../Page/tmsnc.md "wikilink")
  - [Trillian](../Page/Trillian.md "wikilink")
  - [Yahoo\! Messenger](../Page/Yahoo!_Messenger.md "wikilink")

## 參考文獻

## 外部連結

[Category:微軟](../Category/微軟.md "wikilink")
[Category:即時通訊軟件](../Category/即時通訊軟件.md "wikilink")