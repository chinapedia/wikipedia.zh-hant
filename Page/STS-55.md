****是[美国航天历史上第五十四次航天飞机任务](../Page/美国航天.md "wikilink")，也是[哥伦比亚号航天飞机的第十四次太空飞行](../Page/哥倫比亞號太空梭.md "wikilink")。

## 任务成员

  - **[斯蒂芬·纳格尔](../Page/斯蒂芬·纳格尔.md "wikilink")**（，曾执行、、以及任务），指令长
  - **[特伦斯·亨里克斯](../Page/特伦斯·亨里克斯.md "wikilink")**（，曾执行、、以及任务），飞行员
  - **[杰瑞·罗斯](../Page/杰瑞·罗斯.md "wikilink")**（，曾执行、、、、、以及任务），任务专家
  - **[查尔斯·普里克特](../Page/查尔斯·普里克特.md "wikilink")**（，曾执行、、以及任务），任务专家
  - **[伯纳德·哈里斯](../Page/伯纳德·哈里斯.md "wikilink")**（，曾执行以及任务），任务专家
  - **[烏利希·瓦爾特](../Page/烏利希·瓦爾特.md "wikilink")**（，[德国宇航员](../Page/德国.md "wikilink")，曾执行任务），任务专家
  - **[汉斯·西里格](../Page/汉斯·西里格.md "wikilink")**（，[德国宇航员](../Page/德国.md "wikilink")，曾执行任务），任务专家

[Category:1993年佛罗里达州](../Category/1993年佛罗里达州.md "wikilink")
[Category:1993年科學](../Category/1993年科學.md "wikilink")
[Category:哥伦比亚号航天飞机任务](../Category/哥伦比亚号航天飞机任务.md "wikilink")
[Category:1993年4月](../Category/1993年4月.md "wikilink")
[Category:1993年5月](../Category/1993年5月.md "wikilink")