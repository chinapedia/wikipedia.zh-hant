**麦哲伦体育俱乐部**（**Club Deportivo
Magallanes**）是智利足球俱乐部，位于首都[大圣地亚哥地区](../Page/圣地亚哥_\(智利\).md "wikilink")。俱乐部以大航海家[麦哲伦作为队名](../Page/麦哲伦.md "wikilink")，球队在2006年[智利足球乙级联赛中垫底](../Page/智利足球乙级联赛.md "wikilink")，从而降至[智利足球丙级联赛](../Page/智利足球丙级联赛.md "wikilink")
。

俱乐部成立于1897年10月27日，当时叫做**Atlético Escuela Normal
F.C.**。在1933年球队成为了智利首届联赛的冠军，在随后球队取得了联赛冠军「[帽子戏法](../Page/帽子戏法.md "wikilink")」(1933年,
1934年和1935年)，但是球队最后一次夺得联赛冠军是在1938年。

尽管球队近70年没有取得联赛冠军但俱乐部仍然是智利足球史上最成功的俱乐部中排名第7位。

## 球队荣誉

  - [智利足球甲级联赛](../Page/智利足球甲级联赛.md "wikilink"): 1933年, 1934年, 1935年,
    1938年
  - [智利杯](../Page/智利杯.md "wikilink"): 1937年
  - [智利足球丙级联赛](../Page/智利足球丙级联赛.md "wikilink"): 1995年

## 外部链接

  - [Magallanes Unofficial site](http://www.albiceleste.cl/)
  - [Magallanes Official site](http://www.cdmagallanes.cl/)
  - [Magallanes Supporter´s
    site](https://web.archive.org/web/20070513210112/http://www.magallanesbarra.cl/)


{{ Liga Chilena de Fútbol: Primera B }}

[Category:智利足球俱乐部](../Category/智利足球俱乐部.md "wikilink")