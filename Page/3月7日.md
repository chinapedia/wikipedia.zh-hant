**3月7日**是[公历一年中的第](../Page/公历.md "wikilink")66天（[闰年第](../Page/闰年.md "wikilink")67天），离全年的结束还有299天。

## 大事记

### 2世紀

  - [161年](../Page/161年.md "wikilink")：[罗马帝国皇帝](../Page/罗马帝国.md "wikilink")[安敦尼去世](../Page/安敦宁·毕尤.md "wikilink")，[马克奥里略和](../Page/馬爾庫斯·奧列里烏斯.md "wikilink")[维鲁斯成为共治新皇帝](../Page/路奇乌斯·维鲁斯.md "wikilink")。

### 4世紀

  - [321年](../Page/321年.md "wikilink")：[罗马帝国](../Page/罗马帝国.md "wikilink")[皇帝](../Page/罗马皇帝.md "wikilink")[君士坦丁一世正式宣布星期日为罗马的休息日](../Page/君士坦丁一世_\(罗马帝国\).md "wikilink")。

### 10世紀

  - [905年](../Page/905年.md "wikilink")：[朱全忠杀](../Page/朱全忠.md "wikilink")[唐昭宗诸皇子于九曲池](../Page/唐昭宗.md "wikilink")。

### 18世紀

  - [1798年](../Page/1798年.md "wikilink")：[法国军队进入](../Page/法国.md "wikilink")[罗马](../Page/罗马.md "wikilink")，[罗马共和国成立](../Page/罗马共和国.md "wikilink")。

### 19世紀

  - [1814年](../Page/1814年.md "wikilink")：[法国](../Page/法国.md "wikilink")[拿破仑取得](../Page/拿破仑.md "wikilink")的胜利。

### 20世紀

  - [1912年](../Page/1912年.md "wikilink")：[中国](../Page/中国.md "wikilink")[保定陆军军官学校建校](../Page/保定陆军军官学校.md "wikilink")。
  - [1918年](../Page/1918年.md "wikilink")：[芬兰与](../Page/芬兰.md "wikilink")[德国建立同盟](../Page/德国.md "wikilink")，加入[第一次世界大战](../Page/第一次世界大战.md "wikilink")。
  - [1918年](../Page/1918年.md "wikilink")：[日本企业家](../Page/日本.md "wikilink")[松下幸之助在](../Page/松下幸之助.md "wikilink")[大阪创立松下电气器具制作所](../Page/大阪.md "wikilink")，成为[松下电器的前身](../Page/松下电器.md "wikilink")。
  - [1932年](../Page/1932年.md "wikilink")：[美国总统](../Page/美国总统.md "wikilink")[富兰克林·罗斯福宣布实行](../Page/富兰克林·罗斯福.md "wikilink")[新政](../Page/新政.md "wikilink")。
  - [1933年](../Page/1933年.md "wikilink")：由于[热河被](../Page/热河.md "wikilink")[日本占领](../Page/日本.md "wikilink")，[张学良被迫引咎辞职下野](../Page/张学良.md "wikilink")。
  - [1936年](../Page/1936年.md "wikilink")：[德國破坏](../Page/德國.md "wikilink")《[凡尔赛条约](../Page/凡尔赛条约.md "wikilink")》和《[洛迦诺公约](../Page/洛迦诺公约.md "wikilink")》，派兵進入非军事区[莱茵兰地區](../Page/莱茵兰.md "wikilink")。
  - [1945年](../Page/1945年.md "wikilink")：[第二次世界大战](../Page/第二次世界大战.md "wikilink")：[魯爾包圍戰開始](../Page/魯爾包圍戰.md "wikilink")。
  - [1947年](../Page/1947年.md "wikilink")：[国共内战](../Page/国共内战.md "wikilink")：[国民政府和](../Page/国民政府.md "wikilink")[中国共产党谈判破裂](../Page/中国共产党.md "wikilink")，中共全部谈判代表撤回[延安](../Page/延安.md "wikilink")，内战再起。
  - [1948年](../Page/1948年.md "wikilink")：国共内战：[临汾战役](../Page/临汾战役.md "wikilink")，[中国人民解放军占领](../Page/中国人民解放军.md "wikilink")[临汾](../Page/临汾.md "wikilink")。
  - [1961年](../Page/1961年.md "wikilink")：[中](../Page/中華人民共和國.md "wikilink")[美兩國外交官員在](../Page/美國.md "wikilink")[波蘭](../Page/波蘭.md "wikilink")[華沙舉行會談](../Page/華沙.md "wikilink")，當時中美仍然未建交，這次是兩國官員罕有接觸。
  - [1965年](../Page/1965年.md "wikilink")：[美国黑人](../Page/美国黑人.md "wikilink")[争取民权的](../Page/非裔美國人民權運動.md "wikilink")[游行队伍向](../Page/游行.md "wikilink")[蒙哥马利进发时](../Page/蒙哥马利.md "wikilink")，遇到[亚拉巴马州](../Page/亚拉巴马州.md "wikilink")[警察的](../Page/警察.md "wikilink")。
  - [1966年](../Page/1966年.md "wikilink")：[法国总统](../Page/法国总统.md "wikilink")[戴高乐致信](../Page/戴高乐.md "wikilink")[美国总统](../Page/美国总统.md "wikilink")[林登·约翰逊](../Page/林登·约翰逊.md "wikilink")，宣布退出[北约军事一体化](../Page/北约.md "wikilink")。
  - [1976年](../Page/1976年.md "wikilink")：[英國](../Page/英國.md "wikilink")[冰島因捕魚問題引起領海糾紛](../Page/冰島.md "wikilink")。
  - [1987年](../Page/1987年.md "wikilink")：[三七事件](../Page/三七事件.md "wikilink")（[東崗慘案](../Page/東崗慘案.md "wikilink")）：[中華民國國軍](../Page/中華民國國軍.md "wikilink")[小金門駐軍](../Page/小金門.md "wikilink")[屠殺](../Page/屠殺.md "wikilink")[越南難民後毀證滅跡](../Page/越南難民.md "wikilink")。
  - [1989年](../Page/1989年.md "wikilink")：
  - [1997年](../Page/1997年.md "wikilink")：[北京一辆](../Page/北京.md "wikilink")[22路公共汽车在](../Page/北京公交22路.md "wikilink")[西单商场一带遭遇](../Page/西单商场.md "wikilink")[恐怖袭击而发生爆炸](../Page/恐怖袭击.md "wikilink")，2人死亡，11人受伤。
  - [1998年](../Page/1998年.md "wikilink")：世界上首条主要为[互联网服务的](../Page/互联网.md "wikilink")[海底光缆开通](../Page/海底光缆.md "wikilink")，连接[纽约和](../Page/纽约.md "wikilink")[伦敦](../Page/伦敦.md "wikilink")。

### 21世紀

  - [2008年](../Page/2008年.md "wikilink")：[中国南方航空6901号班机](../Page/中國南方航空6901號班機劫機事件.md "wikilink")（[波音757](../Page/波音757.md "wikilink")）遭遇爆炸未遂，备降[兰州中川机场](../Page/兰州中川机场.md "wikilink")，无人伤亡。
  - [2015年](../Page/2015年.md "wikilink")：[馬來西亞反对派支持者因不满法庭判](../Page/馬來西亞.md "wikilink")[安华监禁](../Page/安华.md "wikilink")5年而发动307集會，越有上千名集会者，最终在首都[吉隆坡地标](../Page/吉隆坡.md "wikilink")[双峰塔结束短短](../Page/双峰塔.md "wikilink")4小时的集会。

## 出生

  - [1547年](../Page/1547年.md "wikilink")：[佐竹義重](../Page/佐竹義重.md "wikilink")，[日本戰國時代武將](../Page/日本戰國時代.md "wikilink")（逝于[1612年](../Page/1612年.md "wikilink")）
  - [1765年](../Page/1765年.md "wikilink")：[約瑟夫·尼塞福爾·涅普斯](../Page/約瑟夫·尼塞福爾·涅普斯.md "wikilink")，[法國](../Page/法國.md "wikilink")[發明家](../Page/發明家.md "wikilink")（逝于[1833年](../Page/1833年.md "wikilink")）
  - [1837年](../Page/1837年.md "wikilink")：[亨利·德雷伯](../Page/亨利·德雷伯.md "wikilink")，[美國](../Page/美國.md "wikilink")[天文学家](../Page/天文学家.md "wikilink")（逝于[1882年](../Page/1882年.md "wikilink")）
  - [1875年](../Page/1875年.md "wikilink")：[拉威爾](../Page/拉威爾.md "wikilink")，[法國](../Page/法國.md "wikilink")[作曲家](../Page/作曲家.md "wikilink")（逝于[1937年](../Page/1937年.md "wikilink")）
  - [1845年](../Page/1845年.md "wikilink")：[徐建寅](../Page/徐建寅.md "wikilink")，中国[科学家](../Page/科学家.md "wikilink")（逝于[1901年](../Page/1901年.md "wikilink")）
  - [1850年](../Page/1850年.md "wikilink")：[托馬斯·馬薩里克](../Page/托馬斯·馬薩里克.md "wikilink")，[捷克斯洛伐克首任總統](../Page/捷克斯洛伐克.md "wikilink")（逝于[1937年](../Page/1937年.md "wikilink")）
  - [1857年](../Page/1857年.md "wikilink")：[朱利葉斯·瓦格納-堯雷格](../Page/朱利葉斯·瓦格納-堯雷格.md "wikilink")，[奧地利](../Page/奧地利.md "wikilink")[醫學家](../Page/醫學家.md "wikilink")（逝于[1940年](../Page/1940年.md "wikilink")）
  - [1872年](../Page/1872年.md "wikilink")：[蒙德里安](../Page/皮特·蒙德里安.md "wikilink")，[荷蘭](../Page/荷蘭.md "wikilink")[風格派畫家](../Page/風格派.md "wikilink")（逝于[1944年](../Page/1944年.md "wikilink")）
  - [1884年](../Page/1884年.md "wikilink")：[後藤文夫](../Page/後藤文夫.md "wikilink")，日本政治人物（逝于[1980年](../Page/1980年.md "wikilink")）
  - [1886年](../Page/1886年.md "wikilink")：[傑弗里·泰勒](../Page/傑弗里·泰勒.md "wikilink")，[英國](../Page/英國.md "wikilink")[物理學](../Page/物理學.md "wikilink")、[數學家](../Page/數學家.md "wikilink")（逝于[1975年](../Page/1975年.md "wikilink")）
  - [1890年](../Page/1890年.md "wikilink")：[竺可桢](../Page/竺可桢.md "wikilink")，[中國](../Page/中國.md "wikilink")[地理学家](../Page/地理学家.md "wikilink")、气象学家（逝于[1974年](../Page/1974年.md "wikilink")）
  - [1896年](../Page/1896年.md "wikilink")：[華里士](../Page/華里士.md "wikilink")，英國軍官（逝于[1982年](../Page/1982年.md "wikilink")）
  - [1897年](../Page/1897年.md "wikilink")：[喬伊·保羅·吉爾福特](../Page/喬伊·保羅·吉爾福特.md "wikilink")，美國[心理學家](../Page/心理學.md "wikilink")（逝于[1987年](../Page/1987年.md "wikilink")）
  - [1902年](../Page/1902年.md "wikilink")：[呂炯](../Page/呂炯.md "wikilink")，[中國氣象學家](../Page/中國.md "wikilink")、海洋氣象與農業氣象專家、教育家（逝于[1985年](../Page/1985年.md "wikilink")）
  - [1904年](../Page/1904年.md "wikilink")：[賴因哈德·海德里希](../Page/賴因哈德·海德里希.md "wikilink")，德国政治人物（逝于[1942年](../Page/1942年.md "wikilink")）
  - [1916年](../Page/1916年.md "wikilink")：[王世真](../Page/王世真.md "wikilink")，[中国科学院院士](../Page/中国科学院.md "wikilink")、原子核医学家（逝于[2016年](../Page/2016年.md "wikilink")）
  - [1920年](../Page/1920年.md "wikilink")：[柏楊](../Page/柏楊.md "wikilink")，[台灣作家](../Page/台灣.md "wikilink")、史學家、政治評論家（逝于[2008年](../Page/2008年.md "wikilink")）
  - [1922年](../Page/1922年.md "wikilink")：[安迪·菲利普](../Page/安迪·菲利普.md "wikilink")，美國NBA聯盟職業籃球運動員
  - [1924年](../Page/1924年.md "wikilink")：[安部公房](../Page/安部公房.md "wikilink")，[日本小說家](../Page/日本.md "wikilink")（逝于[1993年](../Page/1993年.md "wikilink")）
  - [1938年](../Page/1938年.md "wikilink")：[戴維·巴爾的摩](../Page/戴維·巴爾的摩.md "wikilink")，[美國生物學家](../Page/美國.md "wikilink")，1975年获得[諾貝爾醫學獎](../Page/諾貝爾醫學獎.md "wikilink")
  - [1938年](../Page/1938年.md "wikilink")：[艾爾伯·費爾](../Page/艾爾伯·費爾.md "wikilink")，[法國物理學家](../Page/法國.md "wikilink")，2007年獲得[諾貝爾物理學獎](../Page/諾貝爾物理學獎.md "wikilink")
  - [1939年](../Page/1939年.md "wikilink")：[大衛·格里格斯](../Page/大衛·格里格斯.md "wikilink")，[美國](../Page/美國.md "wikilink")[太空人](../Page/太空人.md "wikilink")（逝于[1989年](../Page/1989年.md "wikilink")）
  - [1939年](../Page/1939年.md "wikilink")：[悉尼·奧爾特曼](../Page/悉尼·奧爾特曼.md "wikilink")，[加拿大分子物理學家](../Page/加拿大.md "wikilink")
  - [1945年](../Page/1945年.md "wikilink")：[谷垣禎一](../Page/谷垣禎一.md "wikilink")，日本政治人物
  - [1950年](../Page/1950年.md "wikilink")：[艾德蒙多·藤田](../Page/艾德蒙多·藤田.md "wikilink")，[巴西](../Page/巴西.md "wikilink")[大使](../Page/大使.md "wikilink")，該國首位[日裔外交官](../Page/日本裔巴西人.md "wikilink")（逝于[2016年](../Page/2016年.md "wikilink")）
  - [1955年](../Page/1955年.md "wikilink")：[阿努帕姆·卡爾](../Page/阿努帕姆·卡爾.md "wikilink")，[印度男演員](../Page/印度.md "wikilink")
  - [1957年](../Page/1957年.md "wikilink")：[羅伯特·哈里斯](../Page/羅伯特·哈里斯.md "wikilink")，[英國](../Page/英國.md "wikilink")[小說家](../Page/小說家.md "wikilink")
  - [1958年](../Page/1958年.md "wikilink")：[井上倫宏](../Page/井上倫宏.md "wikilink")，日本聲優
  - [1959年](../Page/1959年.md "wikilink")：[湯姆·雷曼](../Page/湯姆·雷曼.md "wikilink")，美國[高爾夫球選手](../Page/高爾夫球.md "wikilink")
  - [1960年](../Page/1960年.md "wikilink")：[伊凡·藍道](../Page/伊凡·藍道.md "wikilink")，[捷克](../Page/捷克.md "wikilink")[網球運動员](../Page/網球.md "wikilink")
  - [1961年](../Page/1961年.md "wikilink")：[高市早苗](../Page/高市早苗.md "wikilink")，[日本女性政治家](../Page/日本.md "wikilink")
  - [1963年](../Page/1963年.md "wikilink")：[埃里卡·倫納德](../Page/埃里卡·倫納德.md "wikilink")，英國情色小說作家
  - [1966年](../Page/1966年.md "wikilink")：[朱慧珊](../Page/朱慧珊.md "wikilink")，[香港主持人](../Page/香港.md "wikilink")
  - [1967年](../Page/1967年.md "wikilink")：[矢澤愛](../Page/矢澤愛.md "wikilink")，日本漫畫家
  - [1967年](../Page/1967年.md "wikilink")：[神谷悠](../Page/神谷悠.md "wikilink")，日本[漫畫家](../Page/漫畫家.md "wikilink")
  - [1967年](../Page/1967年.md "wikilink")：[趙增熹](../Page/趙增熹.md "wikilink")，香港作曲家、唱片監製
  - [1969年](../Page/1969年.md "wikilink")：[黄碧仁](../Page/黄碧仁.md "wikilink")，[新加坡演员](../Page/新加坡.md "wikilink")
  - [1970年](../Page/1970年.md "wikilink")：[王理惠](../Page/王理惠.md "wikilink")，台湾媒体工作者
  - [1970年](../Page/1970年.md "wikilink")：[瑞秋·懷茲](../Page/瑞秋·懷茲.md "wikilink")，[英國女演員](../Page/英國.md "wikilink")
  - [1972年](../Page/1972年.md "wikilink")：[張東健](../Page/張東健.md "wikilink")，[韓國男演員](../Page/韓國.md "wikilink")
  - [1974年](../Page/1974年.md "wikilink")：[徐濠縈](../Page/徐濠縈.md "wikilink")，[香港女演員](../Page/香港.md "wikilink")
  - [1975年](../Page/1975年.md "wikilink")：[李靜](../Page/李靜_\(乒乓球運動員\).md "wikilink")，香港乒乓球運動員
  - [1975年](../Page/1975年.md "wikilink")：[王艷娜](../Page/王艷娜.md "wikilink")，[香港演员](../Page/香港.md "wikilink")
  - [1977年](../Page/1977年.md "wikilink")：[長谷川博己](../Page/長谷川博己.md "wikilink")，[日本男演員](../Page/日本.md "wikilink")
  - [1978年](../Page/1978年.md "wikilink")：[原子鏸](../Page/原子鏸.md "wikilink")，[香港演員](../Page/香港.md "wikilink")
  - [1978年](../Page/1978年.md "wikilink")：[艾拿度·路華斯·杜斯·山度士](../Page/艾拿度·路華斯·杜斯·山度士.md "wikilink")，[巴西](../Page/巴西.md "wikilink")[足球選手](../Page/足球.md "wikilink")
  - [1979年](../Page/1979年.md "wikilink")：[蔡仲南](../Page/蔡仲南.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[棒球選手](../Page/棒球.md "wikilink")
  - [1980年](../Page/1980年.md "wikilink")：[蘿拉·普萊潘](../Page/蘿拉·普萊潘.md "wikilink")，美國女演員
  - [1981年](../Page/1981年.md "wikilink")：[利偉倫](../Page/利偉倫.md "wikilink")，香港足球選手
  - [1983年](../Page/1983年.md "wikilink")：[羅曼·馬丁內斯](../Page/羅曼·馬丁內斯.md "wikilink")，[阿根廷足球選手](../Page/阿根廷.md "wikilink")
  - [1983年](../Page/1983年.md "wikilink")：[塞巴斯蒂安·比埃拉](../Page/塞巴斯蒂安·比埃拉.md "wikilink")，[烏拉圭足球選手](../Page/烏拉圭.md "wikilink")
  - [1984年](../Page/1984年.md "wikilink")：[馬蒂厄·弗拉米尼](../Page/馬蒂厄·弗拉米尼.md "wikilink")，[法國足球選手](../Page/法國.md "wikilink")
  - [1984年](../Page/1984年.md "wikilink")：[杜格拉斯·達·施華](../Page/杜格拉斯·達·施華.md "wikilink")，[巴西足球選手](../Page/巴西.md "wikilink")
  - [1984年](../Page/1984年.md "wikilink")：[南波杏](../Page/南波杏.md "wikilink")，[日本AV女優](../Page/日本.md "wikilink")
  - [1986年](../Page/1986年.md "wikilink")：[妃乃光](../Page/妃乃光.md "wikilink")，[日本AV女優](../Page/日本.md "wikilink")
  - [1989年](../Page/1989年.md "wikilink")：，[日本前AV女優](../Page/日本.md "wikilink")
  - [1990年](../Page/1990年.md "wikilink")：[崔鍾訓](../Page/崔鍾訓.md "wikilink")，韓國歌手，[FTIsland隊長](../Page/FTIsland.md "wikilink")、吉他手及鋼琴手
  - [1992年](../Page/1992年.md "wikilink")：[任炫植](../Page/任炫植.md "wikilink")，韓國男子團體[BTOB成員](../Page/BTOB.md "wikilink")
  - [1995年](../Page/1995年.md "wikilink")：[菊池風磨](../Page/菊池風磨.md "wikilink")，[日本男演員](../Page/日本.md "wikilink")
  - [1997年](../Page/1997年.md "wikilink")：[李愈彬](../Page/李愈彬.md "wikilink")，[韓國女子偶像團體](../Page/韓國.md "wikilink")[Dreamcatcher成員](../Page/Dreamcatcher.md "wikilink")。
  - [2002年](../Page/2002年.md "wikilink")：[羽賀朱音](../Page/羽賀朱音.md "wikilink")，[日本女子組合](../Page/日本.md "wikilink")[早安少女組。第十一期成員](../Page/早安少女組。.md "wikilink")

## 逝世

  - [前322年](../Page/前322年.md "wikilink")：[亞里斯多德](../Page/亞里斯多德.md "wikilink")，[古希腊哲学家](../Page/古希腊哲学家.md "wikilink")（[前384年出生](../Page/前384年.md "wikilink")）
  - [664年](../Page/664年.md "wikilink")：[唐三藏](../Page/玄奘.md "wikilink")，[唐朝](../Page/唐朝.md "wikilink")[佛教](../Page/佛教.md "wikilink")[法相宗創始人](../Page/法相宗.md "wikilink")（[602年出生](../Page/602年.md "wikilink")）
  - [1274年](../Page/1274年.md "wikilink")：[湯瑪斯·阿奎那](../Page/湯瑪斯·阿奎那.md "wikilink")，[意大利神学家](../Page/意大利.md "wikilink")（[1225年出生](../Page/1225年.md "wikilink")）
  - [1724年](../Page/1724年.md "wikilink")：[諾森十三世](../Page/諾森十三世.md "wikilink")，[天主教](../Page/天主教.md "wikilink")[教宗](../Page/教宗.md "wikilink")（[1655年出生](../Page/1655年.md "wikilink")）
  - [1869年](../Page/1869年.md "wikilink")：[王永彬](../Page/王永彬.md "wikilink")，[中國](../Page/中國.md "wikilink")[文學家](../Page/文学家.md "wikilink")（[1792年出生](../Page/1792年.md "wikilink")）
  - [1932年](../Page/1932年.md "wikilink")：[阿里斯蒂德·白里安](../Page/阿里斯蒂德·白里安.md "wikilink")，[法國政治家](../Page/法國.md "wikilink")（[1862年出生](../Page/1862年.md "wikilink")）
  - [1954年](../Page/1954年.md "wikilink")：[楊喚](../Page/楊喚.md "wikilink")，[台灣詩人](../Page/台灣.md "wikilink")（[1930年出生](../Page/1930年.md "wikilink")）
  - [1959年](../Page/1959年.md "wikilink")：[鳩山一郎](../Page/鳩山一郎.md "wikilink")，[日本第](../Page/日本.md "wikilink")52至54任[首相](../Page/日本首相.md "wikilink")（[1883年出生](../Page/1883年.md "wikilink")）
  - [1999年](../Page/1999年.md "wikilink")：[史丹利·庫柏力克](../Page/史丹利·庫柏力克.md "wikilink")，[美国电影导演](../Page/美国电影.md "wikilink")（[1928年出生](../Page/1928年.md "wikilink")）
  - [2004年](../Page/2004年.md "wikilink")：[保罗·温菲尔德](../Page/保罗·温菲尔德.md "wikilink")，[美國演员](../Page/美國.md "wikilink")（[1941年出生](../Page/1941年.md "wikilink")）
  - [2009年](../Page/2009年.md "wikilink")：[张紫妍](../Page/张紫妍.md "wikilink")，[韓國女藝人](../Page/韓國.md "wikilink")（[1980年出生](../Page/1980年.md "wikilink")）

## 节日、风俗习惯

  - ：[女生节](../Page/女生节.md "wikilink")

  - ：[豪華汽車](../Page/豪華汽車.md "wikilink")**[BMW](../Page/BMW.md "wikilink")**成立日

## 參考資料