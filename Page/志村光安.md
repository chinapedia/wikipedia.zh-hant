**志村光安**（生年不詳－1609年／1611年）是[日本戰國時代至](../Page/日本戰國時代.md "wikilink")[江戶時代前期武將](../Page/江戶時代.md "wikilink")。[最上氏家臣](../Page/最上氏.md "wikilink")。通稱[伊豆守](../Page/伊豆守.md "wikilink")。別名是**高治**。

## 簡歷

父親[志村光清是最上家的](../Page/志村光清.md "wikilink")[譜代家臣並仕於](../Page/譜代.md "wikilink")[最上義光](../Page/最上義光.md "wikilink")。被評為「性格剛強，口才亦讓人折服，不論是怎樣的強敵在他面前都只得投降。」[最上義光希望獲得認可擔任出羽守](../Page/最上義光.md "wikilink")，於是派他前往京都面見織田信長，交涉期間展示最上家的系圖，成功獲得敘任出羽守。

在[慶長](../Page/慶長.md "wikilink")5年（1600年）的[慶長出羽合戰中](../Page/慶長出羽合戰.md "wikilink")，於上杉軍2萬軍勢侵攻最上領地時，光安擔任[長谷堂城的守備](../Page/長谷堂城.md "wikilink")，[鮭延秀綱也派出支援](../Page/鮭延秀綱.md "wikilink")，利用伏兵等戰術來抵抗[直江兼續的軍勢](../Page/直江兼續.md "wikilink")，對於城池的守備有很大貢獻。此外，追撃從狐越街道撤退的上杉軍，以及參與攻撃上杉軍[志駄義秀籠城的](../Page/志駄義秀.md "wikilink")[東禪寺城](../Page/東禪寺城.md "wikilink")（酒田城）等等，屢獲軍功。在戰後被賜予改稱為龜崎城的東禪寺城3萬石（有指是因為在慶長8年（1603年），有一隻大龜爬到酒田的地面上，於是向主君義光報告。義光以此為吉兆而把東禪寺城改名為龜崎城，把大寶寺城改名為鶴岡城）。還曾經為復興[庄内的寺社](../Page/庄内.md "wikilink")（在1608年修造羽黑山五重塔等）作出貢獻。在慶長14年（1609年）或慶長16年（1611年）（[山形縣](../Page/山形縣.md "wikilink")[飽海郡](../Page/飽海郡.md "wikilink")[遊佐町的永泉寺中石塔的記錄是慶長](../Page/遊佐町.md "wikilink")16年）逝世。

光安死後，兒子[光惟繼承](../Page/志寸光惟.md "wikilink")[家督](../Page/家督.md "wikilink")，但是在[元和](../Page/元和.md "wikilink")3年（1617年）[最上氏的內紛中](../Page/最上氏.md "wikilink")，被[清水義親指使的](../Page/清水義親.md "wikilink")[一栗高春加以殺害](../Page/一栗高春.md "wikilink")。殘留的一族在最上氏被改易後成為[山形藩的](../Page/山形藩.md "wikilink")[家老](../Page/家老.md "wikilink")，後來移至[江戶](../Page/江戶.md "wikilink")，擔任[水野忠邦的第一家老等職務](../Page/水野忠邦.md "wikilink")，在[明治維新後留在江戶藩邸](../Page/明治維新.md "wikilink")（現今[涉谷區神宮前](../Page/涉谷區.md "wikilink")）。還有其他族人在最上氏改易後，於[東根地方歸農並改姓横尾](../Page/東根地方.md "wikilink")，代代擔任郡中總代名主。

## 外部連結

  - [最上義光歴史館
    最上家臣余録　～知られざる最上家臣たちの姿～](http://samidare.jp/yoshiaki/note?p=log&lid=187449)

## 相關條目

  - [志村氏](../Page/志村氏.md "wikilink")

[Category:戰國武將](../Category/戰國武將.md "wikilink")
[Category:山形藩](../Category/山形藩.md "wikilink")
[Category:出羽國出身人物](../Category/出羽國出身人物.md "wikilink")
[Category:1609年逝世](../Category/1609年逝世.md "wikilink")
[Category:1611年逝世](../Category/1611年逝世.md "wikilink")
[Category:江戶時代武士](../Category/江戶時代武士.md "wikilink")