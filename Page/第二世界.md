[Communist_block.svg](https://zh.wikipedia.org/wiki/File:Communist_block.svg "fig:Communist_block.svg")

**第二世界**（）是一個用來描述國際[政治](../Page/政治.md "wikilink")[經濟體系的概念](../Page/經濟.md "wikilink")，在[冷戰時期開始使用](../Page/冷戰.md "wikilink")，主要有兩個版本。

## 歷史

在[第三世界的說法於](../Page/第三世界.md "wikilink")1950年代開始廣為人們接受之後，[第一世界](../Page/第一世界.md "wikilink")、第二世界的說法也應運而生；後者通常指冷戰時期以[蘇聯為首的](../Page/蘇聯.md "wikilink")[东方集团](../Page/东方集团.md "wikilink")，以及一些在世界其他地區陸續建立的共產主义政權，例如[蒙古人民共和國](../Page/蒙古人民共和國.md "wikilink")、[古巴](../Page/古巴.md "wikilink")、[剛果人民共和國等](../Page/剛果人民共和國.md "wikilink")。这个含义，在1980年代末期東歐非共化，以及1991年[苏联解体後](../Page/苏联.md "wikilink")，逐渐退出历史舞台。\[1\]尽管如此，即使在1950年代至1991年[苏联解体之前](../Page/苏联.md "wikilink")，第二世界概念甚少包括亚洲的[中國和](../Page/中華人民共和國.md "wikilink")[越南](../Page/越南社會主義共和國.md "wikilink")，通常认为他们属于[第三世界](../Page/第三世界.md "wikilink")。

根据[毛泽东在](../Page/毛泽东.md "wikilink")1970年代提出的三个世界的划分，「第二世界」是指政經實力介於[美蘇兩大](../Page/美國.md "wikilink")「超級強權（Superpowers）」和第三世界之间的一些[工業化国家](../Page/工業化国家.md "wikilink")，包括[歐洲共同體](../Page/歐洲聯盟.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")、[加拿大和](../Page/加拿大.md "wikilink")[日本等](../Page/日本.md "wikilink")。毛的這種說法直接反駁了較早將中國包含在以蘇聯為首的集團之內的看法，反映出自1950年代末期以來蘇中共關係惡化的戰略轉變。

1991年[苏联解体后](../Page/苏联解体.md "wikilink")，第二世界的说法逐渐退出了历史舞台，同时第一世界的代稱也开始囊括所有的发达国家。

目前，[第一世界](../Page/第一世界.md "wikilink")、第二世界、[第三世界的划分并不十分明确](../Page/第三世界.md "wikilink")。[第三世界通常指](../Page/第三世界.md "wikilink")[发展中国家](../Page/发展中国家.md "wikilink")，第一、第二世界這兩個名詞則甚少被使用。

## 参考文献

## 参见

  - [三个世界模式](../Page/三个世界模式.md "wikilink")
  - 第二世界
  - [第三世界](../Page/第三世界.md "wikilink")
  - [第四世界](../Page/第四世界.md "wikilink")
  - [冷戰](../Page/冷戰.md "wikilink")
  - [意識形態](../Page/意識形態.md "wikilink")
  - [資本主義](../Page/資本主義.md "wikilink")
  - [社會主義](../Page/社會主義.md "wikilink")
  - [共產主義](../Page/共產主義.md "wikilink")
  - [蘇聯](../Page/蘇聯.md "wikilink")
  - [東方集團](../Page/東方集團.md "wikilink")
  - [社會主義國家列表](../Page/社會主義國家列表.md "wikilink")

### 类似

  - [三个世界理论](../Page/三个世界理论.md "wikilink")

[Category:國際政治](../Category/國際政治.md "wikilink")
[2](../Category/三个世界.md "wikilink")

1.