**始太古代**（），是[太古宙的第一個代](../Page/太古宙.md "wikilink")，前一個是[早雨海代](../Page/早雨海代.md "wikilink")，後一個是[古太古代](../Page/古太古代.md "wikilink")，時間介於[40](../Page/40.md "wikilink")\~[36](../Page/36.md "wikilink")[億](../Page/億.md "wikilink")[年之間](../Page/年.md "wikilink")。

在地质学历史上，始太古代是指地球表面凝固的最早时期。它在[古太古代之前](../Page/古太古代.md "wikilink")、早雨海代之后。这个时期大气压大概是10到100個大氣壓。\[1\]\[2\]\[3\]

## 参考

[分類:太古宙](../Page/分類:太古宙.md "wikilink")

[Category:地质年代](../Category/地质年代.md "wikilink")

1.
2.
3.