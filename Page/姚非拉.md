**姚非拉**（*FLY*，），[中国当代](../Page/中国.md "wikilink")[漫画家](../Page/漫画家.md "wikilink")。他的画风清新，叙事散文化，代表作《[梦里人](../Page/梦里人.md "wikilink")》，曾创内地漫画界连载最长纪录。目前其开设有北京非拉艺术工作室有限公司（*Summer
Studio*），为男性漫画家，1994年在《画书大王》发表《快乐平安夜》，1995年在《北京卡通》上连载《梦里人》被CCTV改编成为内地第一部被改编成动画片的漫画作品。2001年应邀参加亚洲漫画论坛。姚非拉的《梦里人》则创下国内漫画连载时间最长纪录\[1\]，说漫画里60％以上跟画面没关系\[2\]，单行本1\~3在马来西亚出版了马来文版，也是中国内地第一部对海外授权的漫画。\[3\]作者被获评北京十大创意人物，\[4\]于1974年出生于武汉，在高中开始接触漫画。1992年读华中理工大学计算机系，次年才创作个人漫画。1995年，借着《梦里人》在《北京卡通》上出道。历时5年后被中央电视台买了版权。\[5\]梦里人连载约6年。\[6\]

## 个人经历

  - 1974年生于[湖北](../Page/湖北.md "wikilink")[武汉](../Page/武汉.md "wikilink")；祖籍[广东](../Page/广东.md "wikilink")[梅县](../Page/梅县.md "wikilink")。
  - 1992年——1996年就读于[华中理工大学计算机系](../Page/华中理工大学.md "wikilink")。
  - 1994年在[画书大王发表处女作](../Page/画书大王.md "wikilink")《快乐平安夜》。
  - 1995年起在[北京卡通连载长篇](../Page/北京卡通.md "wikilink")《梦里人》。
  - 1996年毕业后来[北京](../Page/北京.md "wikilink")，在一家软件公司工作。
  - 1998年辞职，正式开始职业漫画创作。
  - 2003年成立[夏天島工作室](../Page/夏天島工作室.md "wikilink")，主要成员猪乐桃（[齐潇](../Page/齐潇.md "wikilink")）（已退出）、喵呜（[张弛](../Page/张弛_漫畫家.md "wikilink")）、[于彦舒](../Page/于彦舒.md "wikilink")（已退出）、[夏达](../Page/夏达.md "wikilink")（已退出）。

## 主要作品

  - 1994年：画书大王，《快乐平安夜》（短篇）；
  - 1995年：少年漫画，《疯狂到雨季》（中篇）；
  - 1995年：北京卡通，《[梦里人](../Page/梦里人.md "wikilink")》（长篇，连载达六年）；
  - 1996年：北京卡通，《与我共舞吧》（短篇）；
  - 1996年：北京卡通，《雪花》（中篇）；
  - 1996年：科普画王，《游戏》（短篇）；
  - 1996年：北京卡通，《特殊使命》（短篇）；
  - 1997年：超速风暴，《[明天不会有泪](../Page/明天不会有泪.md "wikilink")》（长篇）；
  - 1997年：少年漫画，《这个和那个》（中篇）；
  - 1998年：北京卡通，《洋娃娃和小熊跳舞》编剧（中篇）；
  - 2000年：日本亚洲漫画论坛，《Where Are We Going？》（短篇）；
  - 2001年：北京卡通，《FLY东游记》（游记）；
  - 2001年：北京卡通FLY俱乐部，《FLY俱乐部会刊-SUMMER》；
  - 2002年：北京卡通，《梦里西游乱记》（中长篇）；
  - 2002年：北京卡通FLY俱乐部，《FLY俱乐部会刊-SPRING》；
  - 2002年：北京卡通FLY俱乐部，《FLY俱乐部会刊-SUMMER》；
  - 2002年：北京卡通，《奇迹》（中长篇）；
  - 2002年：香港B\&C，《外星人袭击地球之桃花攻击事件》（短篇）；
  - 2003年：漫画家，《猪头漫画教室》（专栏）；
  - 2003年：漫友，《80℃》（系列中短篇）；
  - 2003年：漫友Story100，《FLY博士的问题研究所》（专栏）；
  - 2003年：漫友Story100，《和拉弗尔去旅行》（系列彩色封底）；
  - 2004年：《80℃》单行本1、2；
  - 2005年：《[80℃](../Page/80℃.md "wikilink")》单行本3、4；\[7\]\[8\]
  - 2006年：《80℃》单行本5、6；\[9\]\[10\]
  - 2010年：《[鬼吹灯 (漫画)](../Page/鬼吹灯_\(漫画\).md "wikilink")》漫画版。

## 外部链接

  - [SUMMERZOO - 姚非拉“Summer Studio”官方主页](http://www.summerzoo.com/)
  - [姚非拉的BLOG](http://www.summerzoo.com/fly/blog/index.php)

## 参考文献

[Category:中国漫画家](../Category/中国漫画家.md "wikilink")
[Category:华中理工大学校友](../Category/华中理工大学校友.md "wikilink")
[Category:姚姓](../Category/姚姓.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.