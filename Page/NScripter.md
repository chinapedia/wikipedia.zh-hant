**NScripter**，簡稱**NS**，是一款由[高橋直樹所開發用於製作](../Page/高橋直樹_\(剧本作家\).md "wikilink")[視覺小說或](../Page/視覺小說.md "wikilink")[美少女遊戲的引擎](../Page/美少女遊戲.md "wikilink")。作業系統為[Windows](../Page/Microsoft_Windows.md "wikilink")。**N**取自作者名字中的「直樹」（平假名：なおき
罗马字：Naoki）。同出自於高橋之手的**Scripter3**是它的前身。

## 簡介

開發者可利用所謂的[中間語言來撰寫遊戲的代碼](../Page/中間語言.md "wikilink")。由於NScripter上手容易，執行速度快，擴充性也不錯，廣泛受到[同人遊戲製作群的歡迎](../Page/同人遊戲.md "wikilink")。引擎程式碼本身雖然為非公開，但是在非商業用途下是屬於免費軟體。

NScripter本身只支援Windows，不過相容度極高非官方的[ONScripter可支援跨平台的需求](../Page/ONScripter.md "wikilink")。相似的遊戲引擎還有W.Dee氏所開發的[吉里吉里以及](../Page/吉里吉里.md "wikilink")
Alicesoft 的[System4.0](../Page/System4.0.md "wikilink")。

目前流傳的中文版並無官方授權，乃由玩家自行改版產生。

## 使用NScripter的著名作品

### 商用作品

  - [銀色](../Page/銀色\(遊戲\).md "wikilink")（[ねこねこソフト](../Page/貓貓軟件.md "wikilink")）
  - [月東日西](../Page/月東日西.md "wikilink")（[オーガスト](../Page/AUGUST.md "wikilink")）
  - [バイナリィ・ポット](../Page/バイナリィ・ポット.md "wikilink")（オーガスト）
  - [Princess Holiday](../Page/Princess_Holiday.md "wikilink")（オーガスト）
  - [みずいろ](../Page/みずいろ.md "wikilink")（ねこねこソフト）
  - [お姉ちゃんの3乗](../Page/お姉ちゃんの3乗.md "wikilink")（[Marron](../Page/Marron.md "wikilink")）
  - [妳是主人我是僕君が主で執事が俺で](../Page/妳是主人我是僕.md "wikilink")（[みなとそふと](../Page/みなとそふと.md "wikilink")）

### 同人作品

  - [暮蟬悲鳴時系列](../Page/暮蟬悲鳴時.md "wikilink")（[07thExpansion](../Page/07thExpansion.md "wikilink")）
  - [海貓悲鳴時](../Page/海貓悲鳴時.md "wikilink")（[07thExpansion](../Page/07thExpansion.md "wikilink")）
  - [月姫](../Page/月姬.md "wikilink")（[TYPE-MOON](../Page/TYPE-MOON.md "wikilink")）
  - [OneWayLove〜ミントちゃん物語](../Page/OneWayLove〜ミントちゃん物語.md "wikilink")（[王宮魔法劇団](../Page/王宮魔法劇団.md "wikilink")）
  - [ひとかた](../Page/ひとかた.md "wikilink")（お竜）
  - [narcissu系列](../Page/narcissu.md "wikilink")（ステージなな）
  - [魔法言語リリカル☆Lisp](../Page/魔法言語リリカル☆Lisp.md "wikilink")
  - [レミュオールの錬金術師](../Page/レミュオールの錬金術師.md "wikilink")（[犬と猫](../Page/犬と猫.md "wikilink")）
  - [海洋レストラン☆海猫亭](../Page/海洋レストラン☆海猫亭.md "wikilink")（[犬と猫](../Page/犬と猫.md "wikilink")）
  - [LimitLessBit～王国の勇士たち～](../Page/LimitLessBit～王国の勇士たち～.md "wikilink")（[犬と猫](../Page/犬と猫.md "wikilink")）
  - [まじかるブリンガーころな系列](../Page/まじかるブリンガーころな.md "wikilink")（さんだーぼると）
  - [結-OneintheWorld-](../Page/結-OneintheWorld-.md "wikilink")

## 相關書籍

  - NScripterオフィシャルガイド
      - 介绍：NScripter官方手册，由[秀和システム](../Page/秀和システム.md "wikilink")2004年9月11日出版（第一版）。ISBN編碼：ISBN4-7980-0867-2。
      - **注意：目前该书已经绝版，具体请参考后面的修订版**
  - あどばんすどNScripterオフィシャルガイド
      - 介绍：Nscripter進階官方手冊，由[秀和システム](../Page/秀和システム.md "wikilink")2005年7月9日出版（第一版）。ISBN編碼：ISBN4-7980-1104-5
  - NScripterオフィシャルガイド（改訂版）
      - 介绍：Nscripter官方手册修訂版，由[秀和システム](../Page/秀和システム.md "wikilink")2007年12月21日出版（第一版）。ISBN編碼：ISBN978-4-7980-1852-2
  - NScripterではじめるノベルゲーム制作
      - 介绍：Nscripter從無到有製作冒險（小說）遊戲，由[新紀元社](../Page/新紀元社.md "wikilink")2006年9月1日出版（第一版）。ISBN編碼：ISBN4-7753-0496-8

## 参见

  - [视觉小说引擎列表](../Page/视觉小说引擎列表.md "wikilink")

## 外部链接

  - [Takahashi's
    Web](http://www.nscripter.com/)（[過去的站點](http://www2.osk.3web.ne.jp/~naokikun/)），作者高橋直樹的主页

  - [ONScripter](http://onscripter.sourceforge.jp/onscripter.html) -
    一個可執行NScripter語法的跨平台程式

[Category:遊戲引擎](../Category/遊戲引擎.md "wikilink")
[Category:遊戲製作軟件](../Category/遊戲製作軟件.md "wikilink")