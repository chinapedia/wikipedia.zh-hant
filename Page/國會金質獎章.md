[Congressional_Gold_Medal_for_Fighter_Aces.tiff](https://zh.wikipedia.org/wiki/File:Congressional_Gold_Medal_for_Fighter_Aces.tiff "fig:Congressional_Gold_Medal_for_Fighter_Aces.tiff")
**國會金質獎章**（英語：**Congressional Gold
Medal**，簡稱**國會金章**）是[美國國會所頒發](../Page/美國國會.md "wikilink")，與[總統自由勳章並列為美國最高的平民榮譽](../Page/總統自由勳章.md "wikilink")。頒給「對美國歷史及文化有影響，並被認為做出該方面的主要成就」的人。\[1\]

最早的受獎者是一些參與了[美國獨立戰爭以及](../Page/美國獨立戰爭.md "wikilink")[墨西哥戰爭的人民](../Page/墨西哥戰爭.md "wikilink")，後來的獲獎者包括演員、作家、音樂家、探險家、太空人、救生員、科學家、運動員、人道主義者等。对于获奖者没有国籍限制，非美国公民亦可以授予奖章。

## 外部連結

  - [List of
    recipients](http://clerk.house.gov/art_history/house_history/goldMedal.html)
  - [Congressional Research Service (CRS) Reports regarding
    Congressional Gold
    Medals](https://web.archive.org/web/20071121061205/http://digital.library.unt.edu/govdocs/crs/search.tkl?type=subject&q=Congressional%20gold%20medals&q2=LIV)
  - [PDF of CRS Report RL30076, Congressional Gold Medals, 1776-2006,
    Updated
    December 20, 2006](http://www.senate.gov/reference/resources/pdf/RL30076.pdf)

[Category:美国獎勵](../Category/美国獎勵.md "wikilink")
[Category:美国联邦政府立法机构](../Category/美国联邦政府立法机构.md "wikilink")

1.