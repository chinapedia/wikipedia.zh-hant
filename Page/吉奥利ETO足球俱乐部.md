**吉奧利ETO足球俱乐部**（**Győri ETO
FC**）是[匈牙利足球俱乐部](../Page/匈牙利.md "wikilink")，位于[匈牙利](../Page/匈牙利.md "wikilink")[杰尔](../Page/杰尔.md "wikilink")，球队现属于[匈牙利足球甲级联赛](../Page/匈牙利足球甲级联赛.md "wikilink")（Borsodi
Liga）。

吉奥利ETO的主场球衣色为白色上衣，深绿色短裤和深绿色球袜。

## 知名球员

  - [吉尤拉·哈伊赞](../Page/吉尤拉·哈伊赞.md "wikilink")

  - [彼得·维尔梅斯](../Page/彼得·维尔梅斯.md "wikilink")

  - [瓦西利·米柳塔](../Page/瓦西利·米柳塔.md "wikilink")

  - [米科洛斯·菲赫尔](../Page/米科洛斯·菲赫尔.md "wikilink")

  - [托马斯·布里斯金](../Page/托马斯·布里斯金.md "wikilink")

## 球队荣誉

  - **[匈牙利足球甲級聯賽冠军](../Page/匈牙利足球甲級聯賽.md "wikilink"): 3次**
      - 1963年, 1982年, 1983年
  - **[匈牙利盃](../Page/匈牙利盃.md "wikilink"): 4次**
      - 1965年, 1966年, 1967年, 1979年
  - **[歐洲冠軍聯賽](../Page/歐洲冠軍聯賽.md "wikilink")**
      - *準决赛*: 1965年 **1次**

## 外部链接

  - [Official Club website](http://www.eto.hu/)

[Category:匈牙利足球俱乐部](../Category/匈牙利足球俱乐部.md "wikilink")