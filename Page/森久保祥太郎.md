**森久保祥太郎**（）是[日本的男性](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")。[東京都](../Page/東京都.md "wikilink")[八王子市出生](../Page/八王子市.md "wikilink")，[血型](../Page/血型.md "wikilink")[B型](../Page/ABO血型系統.md "wikilink")，現隸屬於[ADD9TH](../Page/ADD9TH.md "wikilink")（原屬[VIMS](../Page/VIMS.md "wikilink")）。前妻是同樣為聲優的[淺川悠](../Page/淺川悠.md "wikilink")。

## 人物介紹

  - 帶一點鼻音聽起來有點「洋腔」的語調使他的聲音辨識度極高，輕浮慢悠的口氣很有貴族氣息，也常接到一些外表看似花花公子實則非常專情的角色。
  - 在音樂方面十分活躍。與[聲優的](../Page/聲優.md "wikilink")[石川英郎共組了樂團](../Page/石川英郎.md "wikilink")[AN's
    ALL STARS](../Page/AN's_ALL_STARS.md "wikilink")。也常為友人的劇團擔任音樂創作。
  - 演戲劇團「」的成員。
  - 與[鳥海浩輔關係不錯](../Page/鳥海浩輔.md "wikilink")，兩人在2008～2009的跨年夜曾整整兩天都待在一起。
  - 2007年2月22日與[淺川悠](../Page/淺川悠.md "wikilink")[結婚](../Page/結婚.md "wikilink")。
  - 2009年1月14日[離婚](../Page/離婚.md "wikilink")。
  - 2014年7月4日發表再婚消息，對方為一般人。
  - 2017年12月29日於廣播節目[THE BAY
    LINE發表女儿出生的消息](../Page/THE_BAY_LINE.md "wikilink")。
  - 2018年8月31日發佈退出原事務所[VIMS](../Page/VIMS.md "wikilink")。
  - 2018年9月1日正式進入[ADD9TH](../Page/ADD9TH.md "wikilink")。

## 參與作品

※**粗體字**為主要角色

### 電視動畫

**1996年**

  - [機動新世紀GUNDAM X](../Page/機動新世紀GUNDAM_X.md "wikilink")（威利斯）
  - [VS騎士檸檬汽水&40炎](../Page/VS騎士檸檬汽水&40炎.md "wikilink")（水戶、波爾多）
  - [爆走兄弟](../Page/爆走兄弟.md "wikilink")（四驅戰士）

**1998年**

  - [金田一少年之事件簿](../Page/金田一少年之事件簿_\(动画\).md "wikilink")（棟方健）
  - [超速搖搖](../Page/超速搖搖.md "wikilink")（**堂本瞬一**）
  - [魔術士歐菲](../Page/魔術士歐菲.md "wikilink")（**歐菲**）

**1999年**

  - [迷糊女戰士](../Page/迷糊女戰士.md "wikilink")（岩田紀國）
  - [神風怪盜貞德](../Page/神風怪盜貞德.md "wikilink")（山際順次、彼方木神樂）
  - [格鬥小霸王](../Page/格鬥小霸王.md "wikilink")（雹）
  - [天使不設防\!](../Page/天使不設防!.md "wikilink")（拉菲魯、冬雪）
  - [魔術士歐菲Revenge](../Page/魔術士歐菲.md "wikilink")（**歐菲**）
  - [MONKEY MAGIC](../Page/MONKEY_MAGIC.md "wikilink")（**悟空**）
  - [爆球連發\!\! 超級彈珠人](../Page/爆球連發!!_超級彈珠人.md "wikilink")（彈珠攻略王）

**2000年**

  - [外星人田中太郎](../Page/外星人田中太郎.md "wikilink")（老師）
  - 金田一少年之事件簿（四至本貞治）
  - [真·女神轉生 惡魔之子](../Page/真·女神轉生_惡魔之子.md "wikilink")（甲斐剎那）
  - [櫻桃小丸子](../Page/櫻桃小丸子.md "wikilink")（男子）
  - [哈姆太郎](../Page/哈姆太郎.md "wikilink")（星垣）

**2001年**

  - [足球小將(2001年)](../Page/足球小將.md "wikilink")（葵新伍）
  - [聖石小子](../Page/聖石小子.md "wikilink")（**姆吉卡**）
  - [人造人009](../Page/人造人009.md "wikilink")（002）
  - [熱帶雨林的爆笑生活](../Page/熱帶雨林的爆笑生活.md "wikilink")（羅伯特）
  - [新白雪姬傳說](../Page/新白雪姬傳說.md "wikilink")（**豪**）
  - [地球防衛家族](../Page/地球防衛家族.md "wikilink")（HOSHI）
  - [網球王子](../Page/網球王子.md "wikilink")（**切原赤也**）
  - [RUN=DIM](../Page/RUN=DIM.md "wikilink")（本田仁）

**2002年**

  - [閃靈二人組](../Page/閃靈二人組.md "wikilink")（**天野銀次**）
  - [東京地底奇兵](../Page/東京地底奇兵.md "wikilink")（華泰）
  - [火影忍者](../Page/火影忍者.md "wikilink")（**奈良鹿丸**）
  - [魯莽天使](../Page/魯莽天使.md "wikilink")（岳山隆雄）

**2003年**

  - [一騎當千](../Page/一騎當千.md "wikilink")（左慈、王允）
  - [貯金大冒險](../Page/貯金大冒險.md "wikilink")（Paru）
  - [魔法少年賈修](../Page/魔法少年賈修.md "wikilink")（春彥）
  - [魔偵探洛基RAGNAROK](../Page/魔偵探洛基.md "wikilink")（**鳴神**（托爾））

**2004年**

  - [SAMURAI GUN](../Page/SAMURAI_GUN.md "wikilink")（**市松**）
  - [混沌武士](../Page/混沌武士.md "wikilink")（半吉）
  - [忘卻的旋律](../Page/忘卻的旋律.md "wikilink")（赫魯）
  - [電郵寵物MOMO](../Page/電郵寵物MOMO.md "wikilink")（MOMO）
  - [名偵探柯南](../Page/名侦探柯南_\(动画\).md "wikilink")（世古國繁）(第392-393集)

**2005年**

  - [棒球大聯盟2nd season](../Page/棒球大聯盟.md "wikilink")（**茂野吾郎**）

**2006年**

  - [練馬大根兄弟](../Page/練馬大根兄弟.md "wikilink")（一朗）
  - [地獄少女 二籠](../Page/地獄少女.md "wikilink")（須崎幹夫）
  - [棒球大聯盟3rd season](../Page/棒球大聯盟.md "wikilink")（**茂野吾郎**）
  - [完美小姐進化論](../Page/完美小姐進化論.md "wikilink")（**高野恭平**）

**2007年**

  - [彩雲國物語](../Page/彩雲國物語.md "wikilink")（**陸清雅**）
  - [鬼面騎士](../Page/鬼面騎士.md "wikilink")（）
  - [無敵偵探貴公子](../Page/無敵偵探貴公子.md "wikilink")（信濃紫炎）
  - [驅魔少年](../Page/驅魔少年.md "wikilink")（**積斯迪羅**）
  - [火影忍者疾風傳](../Page/火影忍者疾風傳.md "wikilink")（**奈良鹿丸**）

**2008年**

  - [雨月之夜](../Page/雨月之夜.md "wikilink")（露草）
  - [棒球大聯盟4th season](../Page/棒球大聯盟.md "wikilink")（**茂野吾郎**）

**2009年**

  - [機巧魔神](../Page/機巧魔神.md "wikilink")（佐伯玲士郎）
  - [11eyes
    -罪與罰與贖的少女-](../Page/11eyes_-罪與罰與贖的少女-.md "wikilink")（**田島賢久**）
  - 名偵探柯南（兒島乾史）
  - [棒球大聯盟5th season](../Page/棒球大聯盟.md "wikilink")（**茂野吾郎**）

**2010年**

  - [薄櫻鬼 ～新選組奇譚～](../Page/薄櫻鬼.md "wikilink")（**沖田總司**）
  - [薄櫻鬼 碧血錄](../Page/薄櫻鬼.md "wikilink")（**沖田總司**）
  - [棒球大聯盟6th season](../Page/棒球大聯盟.md "wikilink")（**茂野吾郎**）
  - [BLEACH](../Page/BLEACH.md "wikilink")（天鎖斬月）
  - [爆漫王。](../Page/爆漫王。.md "wikilink")（間界野昂次／KOOGY）

**2011年**

  - [滑头鬼之孙千年魔京](../Page/妖怪少爺.md "wikilink")（花开院秋房）
  - [卡片戰鬥先導者](../Page/卡片戰鬥先導者.md "wikilink")（三和泰志）
  - [女神異聞錄4](../Page/女神異聞錄4.md "wikilink")（**花村陽介**）

**2012年**

  - [卡片戰鬥先導者 亞洲巡迴賽篇](../Page/卡片戰鬥先導者.md "wikilink")（三和泰志）
  - [新網球王子](../Page/網球王子.md "wikilink")（**切原赤也**）
  - [超譯百人一首歌之戀](../Page/超譯百人一首歌之戀.md "wikilink")（陽成院）
  - [薄櫻鬼 黎明錄](../Page/薄櫻鬼.md "wikilink")（**沖田總司**）
  - [魔奇少年](../Page/魔奇少年.md "wikilink")（迦尔鲁卡）

**2013年**

  - [卡片戰鬥先導者 王牌的連接篇](../Page/卡片戰鬥先導者.md "wikilink")（三和泰志）
  - 名偵探柯南（蟹山銀二 \#686）
  - [歌之王子殿下 MAJI LOVE 2000%](../Page/歌之王子殿下.md "wikilink")（**寿嶺二**）
  - [魔奇少年 The Kingdom of Magic](../Page/魔奇少年.md "wikilink")（迦爾鲁卡）
  - [飆速宅男](../Page/飆速宅男.md "wikilink")（**卷島裕介**）
  - [眼鏡部！](../Page/眼鏡部！.md "wikilink")（缽嶺信司）
  - [武士弗拉明戈](../Page/武士弗拉明戈.md "wikilink")（青島蒼一）

**2014年**

  - [WIZARD
    BARRISTERS～弁魔士賽希爾](../Page/WIZARD_BARRISTERS～弁魔士賽希爾.md "wikilink")（清水憐生、清水津久司）
  - [海賊王](../Page/海賊王.md "wikilink")（巴特洛馬）
  - [幕末Rock](../Page/幕末Rock.md "wikilink")（**桂小五郎**）
  - [人生諮詢電視動畫「人生」](../Page/人生_\(輕小說\).md "wikilink")（淺野浩太）
  - [桃心之劍](../Page/桃心之劍.md "wikilink")（**猿神**）
  - [LOVE STAGE\!\!](../Page/LOVE_STAGE!!.md "wikilink")（龍崎虎次郎）
  - [女神異聞錄4 黃金版](../Page/女神異聞錄4.md "wikilink")（**花村陽介**）
  - [巴哈姆特之怒 GENESIS](../Page/巴哈姆特之怒.md "wikilink")（漢撒）
  - [飆速宅男 GRANDE ROAD](../Page/飆速宅男.md "wikilink")（**卷島裕介**）
  - [大圖書館的牧羊人](../Page/大圖書館的牧羊人.md "wikilink")（高峰一景）

**2015年**

  - [元氣少女緣結神◎](../Page/元氣少女緣結神.md "wikilink")（大國主）
  - [美男高校地球防衛部LOVE！](../Page/美男高校地球防衛部LOVE！.md "wikilink")（城崎鋼）
  - [歌之王子殿下 真愛革命](../Page/歌之王子殿下.md "wikilink")（壽嶺二）
  - [Classroom☆Crisis](../Page/Classroom☆Crisis.md "wikilink")（**瀨良海斗**）
  - [鑽石王牌－SECOND SEASON－](../Page/鑽石王牌.md "wikilink")（梅宮聖一）
  - [魔鬼戀人 MORE,BLOOD](../Page/魔鬼戀人.md "wikilink")（月浪辛）\[1\]
  - [高校星歌劇](../Page/高校星歌剧.md "wikilink")（曉鏡司）

**2016年**

  - 名侦探柯南（鴻江保人 \#822-823）
  - [排球少年！！第二季](../Page/排球少年！！.md "wikilink")（中島勇）
  - [金田一少年之事件簿R 第2期](../Page/金田一少年之事件簿_\(动画\).md "wikilink")（蝉泽忍）
  - [JoJo的奇妙冒險 不碎鑽石](../Page/JoJo的奇妙冒險.md "wikilink")（嗆辣紅椒、音石明）
  - [薄櫻鬼 〜御伽草子〜](../Page/薄櫻鬼.md "wikilink")（**沖田總司**）
  - [在下坂本，有何貴幹？](../Page/在下坂本，有何貴幹？.md "wikilink")（瀨良裕也）
  - [初戀怪獸](../Page/初戀怪獸.md "wikilink")（**金子十六**、松藏〈湯姆〉）
  - [B-PROJECT〜鼓動＊Ambitious〜](../Page/B-PROJECT.md "wikilink")（**王茶利暉**）
  - [美男高校地球防衛部LOVE！LOVE！](../Page/美男高校地球防衛部LOVE！.md "wikilink")（城崎鋼）
  - [時間旅行少女～真理、和花與8名科學家～](../Page/時間旅行少女～真理、和花與8名科學家～.md "wikilink")（麥可·法拉第）
  - [齊木楠雄的災難](../Page/齊木楠雄的災難.md "wikilink")（蝶野雨綠）
  - [歌之王子殿下 真愛傳奇之星](../Page/歌之王子殿下.md "wikilink")（壽嶺二）

**2017年**

  - [時間飛船24](../Page/時間飛船24.md "wikilink")（伽利略·伽利雷）
  - [鎖鏈戰記 ～赫克瑟塔斯之光～](../Page/鎖鏈戰記.md "wikilink")（梅魯提歐魯）
  - [élDLIVE宇宙警探](../Page/élDLIVE宇宙警探.md "wikilink")（歐文）
  - [新選組鎮魂歌 二分之一](../Page/新選組鎮魂歌_二分之一.md "wikilink")（**山南敬助**） \[2\]
  - 飆速宅男 NEW GENERATION（卷島裕介）
  - [Hand Shakers](../Page/Hand_Shakers.md "wikilink")（槙原長政）
  - [高校星歌劇 第2期](../Page/高校星歌剧.md "wikilink")（曉鏡司）
  - [火影新世代BORUTO -NARUTO NEXT
    GENERATIONS-](../Page/火影忍者.md "wikilink")（奈良鹿丸）
  - 巴哈姆特之怒 VIRGIN SOUL（漢撒\[3\]）
  - [潔癖男子青山！](../Page/潔癖男子青山！.md "wikilink")（尾崎篤夢／松優）
  - [戰刻夜血](../Page/戰刻夜血.md "wikilink")（真田信之）
  - [Code:Realize
    ～創世的公主～](../Page/Code:Realize_～創世的公主～.md "wikilink")（**因倍·巴比康**）
  - [DYNAMIC CHORD](../Page/DYNAMIC_CHORD.md "wikilink")（**城坂依都**）

**2018年**

  - [POP TEAM
    EPIC](../Page/POP_TEAM_EPIC.md "wikilink")（**POP子**〈第7話B段〉）
  - [霸穹 封神演義](../Page/封神演義_\(漫畫\).md "wikilink")（道德真人）
  - [魔法少女 我](../Page/魔法少女_我.md "wikilink")（矢茂小波）
  - [棒球大聯盟2nd](../Page/棒球大聯盟.md "wikilink")（**茂野吾郎**）
  - [奴隸區 The Animation](../Page/奴隸區_我與23個奴隸.md "wikilink")（立川震乃助）

**2019年**

  - [W'z](../Page/W'z.md "wikilink")（旁白）
  - [B-PROJECT ～絕頂＊Emotion～](../Page/B-PROJECT.md "wikilink")（**王茶利暉**）
  - [偶像夢幻祭](../Page/偶像夢幻祭.md "wikilink")（**遊木真**\[4\]）

### OVA

  - [快刀乱麻](../Page/快刀乱麻.md "wikilink")（楯岡新十朗）
  - [怪童丸](../Page/怪童丸.md "wikilink")（源賴光）
  - [網球王子](../Page/網球王子.md "wikilink") Original Video Animation
    全國大會篇（切原赤也）
  - [東京大学物語](../Page/東京大学物語.md "wikilink")（村上直樹）
  - [鬼太郎](../Page/鬼太郎.md "wikilink")（山田秀一）
  - [Final Fantasy VII Advent
    Children](../Page/太空戰士VII:降臨神子.md "wikilink")（**卡達裘**）
  - [FREEDOM-PROJECT](../Page/FREEDOM-PROJECT.md "wikilink")（和馬）
  - [秋之回憶5：中斷的影片](../Page/秋之回憶5：中斷的影片.md "wikilink") THE
    ANIMATION（**河合春人**）
  - [冬之蟬](../Page/冬之蟬.md "wikilink")（相澤之進）
  - [魔奇少年](../Page/魔奇少年.md "wikilink")（迦爾魯卡）

**2005年**

  - [妹妹戀人](../Page/妹妹戀人.md "wikilink")（**結城賴**）

**2016年**

  - [高校星歌劇](../Page/高校星歌劇.md "wikilink") 第1期 OVA1（曉鏡司） \[5\]
  - 高校星歌劇 第1期 OVA2（曉鏡司） \[6\]

### 動畫電影

  - [鬼神傳](../Page/鬼神傳.md "wikilink")（渡邊綱）

  - [轟天高校生 世界末日](../Page/轟天高校生.md "wikilink")（**御神苗優**）

  - [寵物小精靈劇場版：超夢的逆襲](../Page/神奇寶貝劇場版：超夢的逆襲.md "wikilink")（超夢（幼體））

  - [網球王子系列](../Page/網球王子.md "wikilink")（切原赤也）

  - （**[奈良鹿丸](../Page/奈良鹿丸.md "wikilink")**）

      -
      -
      -
      -
  - [棒球大聯盟](../Page/棒球大聯盟劇場版_友情的一球.md "wikilink") （**茂野吾郎**（青年期））

  - [爆走兄弟](../Page/爆走兄弟.md "wikilink")

**2012年**

  - [FAIRY TAIL 魔導少年
    鳳凰的巫女](../Page/FAIRY_TAIL_\(動畫\).md "wikilink")（迪斯特）

**2013年**

  - [薄櫻鬼 劇場版 第一章 京都亂舞](../Page/薄櫻鬼_～新選組奇譚～.md "wikilink")（**沖田總司**）

**2014年**

  - 薄櫻鬼 劇場版 第二章 士魂蒼穹（**沖田總司**）

**2016年**

  - [KING OF PRISM by
    PrettyRhythm](../Page/KING_OF_PRISM_by_PrettyRhythm.md "wikilink")（黑川冷）

**2018年**

  - 劇場版 [無敵鐵金剛](../Page/無敵鐵金剛.md "wikilink")（**兜甲兒**）

### 網路動畫

  - 2017年

<!-- end list -->

  - [梦王国与沉睡的100王子
    短篇动画](../Page/梦王国与沉睡的100王子.md "wikilink")（**約修亞**、**白葉**）

<!-- end list -->

  - 2018年

<!-- end list -->

  - （**尼古拉斯**） \[7\]

### 遊戲

  - \[PC/PS2\]（）

  - \[PC\]妖之宮（積興之介）

  - \[PS2\]（**紅**）

  - \[PS2\][夢幻奇緣2](../Page/夢幻奇緣2.md "wikilink")（**亞克·海靈頓**）

      - \[PC/PS2\]夢幻奇緣2☆☆☆

  - \[PC\]三国恋战记～少女的兵法！～（**仲謀**）

  - \[DS\]（**鷹島疾斗**）

  - \[PS2\]系列（**吉鲁斯**）

      - 于是在这宇宙里闪耀着你的诗篇
      - 于是在这宇宙里闪耀着你的诗篇 XXX

  - \[PS2\][心跳回憶 Girl's
    Side](../Page/心跳回憶_Girl's_Side.md "wikilink")（**蒼樹千晴**）

      - \[DS\]心跳回忆 Girl's Side 1st Love
      - \[DS\]心跳回忆 Girl's Side 1st Love Plus

  - [薄樱鬼系列](../Page/薄樱鬼_～新选组奇谭～.md "wikilink")（**[冲田总司](../Page/冲田总司.md "wikilink")**）

      - \[PS2\]薄樱鬼 ～新选组奇谭～
      - \[DS\]薄樱鬼 DS
      - \[PSP\]薄樱鬼 携带版
      - \[PS3\]薄樱鬼 巡想录
      - \[PS2\]薄樱鬼 随想录
      - \[PSP\]薄樱鬼 随想录 携带版
      - \[DS\]薄樱鬼 随想录
      - \[PSP\]薄樱鬼 游戏录
      - \[DS\]薄樱鬼 游戏录
      - \[PS2\]薄樱鬼 黎明录
      - \[PSP\]薄樱鬼 黎明录 携带版
      - \[3DS\]薄樱鬼 3D
      - \[PSP\]薄樱鬼 幕末無雙錄

  - \[PS2\]（[永倉新八](../Page/永倉新八.md "wikilink")）

      - \[DS\]幕末恋华 新选组DS

  - \[PSP\]B's-LOG Party♪（冲田总司）

  - \[PS2\]（****）

      - \[PSP\]VitaminZ Revolution
      - \[PSP\]VitaminXtoZ

  - \[PC/DC/PS2\]Fragrance Tale（洛基）

  - \[PS2\]（**雪草純**）

  - \[PSP\]

  - \[PS2\]（**道明寺晓尚**）

  - \[PSP\]文明華開 葵座異聞錄（**神鳴劍助**）

  - （**岬虎太郎**）

  - \[PSP\]官能昔話 携带版（****）

  - \[PSP\]歌之☆王子殿下♪ Debut（寿嶺二）

  - \[PSVITA\]DIABOLIK LOVERA DARK FATE（月浪シン）

  - [11eyes
    CrossOver](../Page/11eyes_-罪與罰與贖的少女-.md "wikilink")（**田島賢久**）

  - Grandia2/格兰蒂亚2/冒险奇谈2（**里特**）

  - [閃靈二人組系列](../Page/閃靈二人組.md "wikilink")

      - （**美堂蠻**）

      - （**天野銀次**）

      - （**天野銀次**）

      - （**天野銀次**）

  - Shining Force NEO（**MAX**）

  - 怪人ZONA（**怪人ZONA**）

  - 新世紀GPX[閃電霹靂車](../Page/閃電霹靂車.md "wikilink") Road to the Infinity 3（）

  - [白騎士物語](../Page/白騎士物語.md "wikilink")（）

  - [真·女神轉生 惡魔之子](../Page/真·女神轉生_惡魔之子.md "wikilink") （**甲斐剎那**）

  - [轟天高校生](../Page/轟天高校生.md "wikilink") 特種部隊：月面獵殺（SPRIGGAN:LUNAR
    VERSE）（御神苗優）

  - （鷹島疾斗）

  - [雙面嬌娃](../Page/雙面嬌娃.md "wikilink")（二村英樹）

      - [網球王子系列](../Page/網球王子.md "wikilink")（切原赤也）

      -
      -
      -
      -
      -
      -
      -
      -
      -
      -
      -
      -
      -
  - [電撃学園RPG Cross of
    Venus](../Page/電撃学園RPG_Cross_of_Venus.md "wikilink")（佐伯玲士郎）

  - [魔術師歐菲](../Page/魔術師歐菲.md "wikilink") Sorcerous Stabber ORPHEN 魔術師歐菲
    （**歐菲**）

  - [秋之回憶5：中断的影片](../Page/秋之回憶5：中断的影片.md "wikilink")（**河合春人**）

      - [秋之回忆5：安可](../Page/秋之回忆5：安可.md "wikilink")

  - [女神異聞錄4](../Page/女神異聞錄4.md "wikilink")（**花村陽介**）

  - [洛克人系列](../Page/洛克人系列.md "wikilink")

      -
      - [洛克人X5](../Page/洛克人X5.md "wikilink")（X、戴那蒙）

      - [洛克人X6](../Page/洛克人X6.md "wikilink")（X、戴那蒙）

      - [洛克人X7](../Page/洛克人X7.md "wikilink")（X）

  - [火影忍者系列](../Page/火影忍者.md "wikilink")（**奈良鹿丸**）

  - [夢王國與沈睡中的100位王子陛下](../Page/夢王國與沈睡中的100位王子陛下.md "wikilink")（**白葉**、傑書亞）

  - [偶像夢幻祭](../Page/偶像夢幻祭.md "wikilink")（**遊木真**）

  - [奥丁领域](../Page/奥丁领域.md "wikilink")（英格威）

  - [戀愛警報異能特勤部](../Page/戀愛警報異能特勤部.md "wikilink")（**月代理人**）

  - Shounen Sunday Vs. Shounen Magazine （美堂蠻）

  - [BLEACH BRAVE SOULS](../Page/BLEACH.md "wikilink")（天锁斩月）

  - \[
    [PS4](../Page/PlayStation_4.md "wikilink")/[NS](../Page/任天堂Switch.md "wikilink")
    \] [超級機器人大戰T](../Page/超級機器人大戰T.md "wikilink")（**兜甲兒**）

### 吹替

**電視/電影**

| 年份                                       | 作品                                         | 角色         | 演員                                           | 媒介         |
| ---------------------------------------- | ------------------------------------------ | ---------- | -------------------------------------------- | ---------- |
| **2002**                                 | [愛情白皮書](../Page/愛情白皮書.md "wikilink")       | **取手守**    | [彭于晏](../Page/彭于晏.md "wikilink")             | DVD版本      |
| **2012**                                 | [太極1從零開始](../Page/太極1從零開始.md "wikilink")   | **方子敬**    | 影碟版本                                         |            |
| [寒戰](../Page/寒戰.md "wikilink")           | **李家俊**                                    |            |                                              |            |
| [太極2英雄崛起](../Page/太極2英雄崛起.md "wikilink") | **方子敬**                                    |            |                                              |            |
| **2014**                                 | [黃飛鴻之英雄有夢](../Page/黃飛鴻之英雄有夢.md "wikilink") | **黃飛鴻**    |                                              |            |
| **2016**                                 | [特工爺爺](../Page/特工爺爺.md "wikilink")         | 武警隊長       |                                              |            |
| [危城](../Page/危城.md "wikilink")           | **馬鋒**                                     |            |                                              |            |
| [寒戰II](../Page/寒戰II.md "wikilink")       | **李家俊**                                    |            |                                              |            |
| [湄公河行動](../Page/湄公河行動.md "wikilink")     | **方新武**                                    |            |                                              |            |
| **2017**                                 | [悟空傳](../Page/悟空傳.md "wikilink")           | **孫悟空**    |                                              |            |
| **2005**                                 | [犯罪心理](../Page/犯罪心理.md "wikilink")（至今）     | **史班舍·瑞得** | [馬修·葛雷·古博勒](../Page/馬修·葛雷·古博勒.md "wikilink") | 電視平台/DVD版本 |
|                                          |                                            |            |                                              |            |

### 手提電玩

  -
  - [白猫Project](../Page/白猫Project.md "wikilink")（蘇芳·小次郎·義之）

  - [陰陽師](../Page/陰陽師_\(遊戲\).md "wikilink")（赤舌）

### 廣播節目

**現在的廣播節目**

  - SWITCH （bayfm 2009年4月3日 - ）

  - SWITCH\! MIDNIGHT （bayfm 2010年4月2日 ‐ ）

  -
  - [BAY LINE GO\!GO\!](../Page/BAY_LINE_GO!GO!.md "wikilink")（bayfm
    2010年4月2日 - ）

  - （[HiBiKi Radio Station](../Page/HiBiKi_Radio_Station.md "wikilink")
    2010年4月21日 - ）

**過去的廣播節目**

  - 1998年7月 - 1999年3月・1999年10月 - 2000年3月）

  - （[文化放送](../Page/文化放送.md "wikilink") 1998年10月9日 - 1999年4月2日）

  - （文化放送 1999年4月9日 - ）

<!-- end list -->

  -
    [關俊彥](../Page/關俊彥.md "wikilink")

<!-- end list -->

  - 1999年7月24日 - 放送終了）

  - [祥太郎、麻衣・CURE HOUSE](../Page/祥太郎、麻衣・CURE_HOUSE.md "wikilink")（）

  -
<!-- end list -->

  -

<!-- end list -->

  -
  - [JAM PUNCH\!](../Page/JAM_PUNCH!.md "wikilink")（ 2004年4月2日 -
    2009年3月27日）

  -
  -
<!-- end list -->

  -

<!-- end list -->

  -
  -
<!-- end list -->

  -

<!-- end list -->

  -
  -
  -
  -
  -
<!-- end list -->

  -

<!-- end list -->

  -
<!-- end list -->

  -

**廣播劇**

  - [VOMIC](../Page/VOMIC.md "wikilink") （**紫門元**）

  -
**DJCD**

  -
  -
  -
  -
### 廣播劇CD

  - [少年同盟](../Page/少年同盟.md "wikilink")（東晃一）

  - [Are you Alice?系列](../Page/Are_you_Alice?.md "wikilink")（**白兔子**）

  -
  - Vol.1 Vol.2（）

  - [雨月之夜系列](../Page/雨月之夜.md "wikilink")（露草）

      -
      -
      -
      -
      -
  - [戀遊記](../Page/戀遊記.md "wikilink") （**紅**）

  - [EREMENTAR GERAD](../Page/EREMENTAR_GERAD.md "wikilink")

  - [ALMIGHTY×10](../Page/ALMIGHTY×10.md "wikilink")（冬賀李玖）

  - （德美秀太）

  - [危機之介御免系列](../Page/危機之介御免.md "wikilink")（柳生十三）

      - 音之壱盤『毒味之危機』
      - 音之弐盤『未来之危機』

  -   -
      - 高校生篇 2

      -
  - [逆境九壯士](../Page/逆境九壯士.md "wikilink")（高田二流、登別）

  - [閃靈二人組系列](../Page/閃靈二人組.md "wikilink")（**天野銀次**）

      - TARGET G
      - TWINS

  - 幻想水滸傳II（）

  - （北野細雪）

  - [最遊記](../Page/最遊記.md "wikilink") 第9巻（雪羅）

  - [紅茶王子](../Page/紅茶王子.md "wikilink") 1、2（内山美佳）

  - [幸福喫茶3丁目](../Page/幸福喫茶3丁目.md "wikilink")（安倍川草）

  -
  - （坂本龍馬）

  - [魔王愛勇者系列](../Page/魔王愛勇者.md "wikilink")（**艾克薩**）

      -
      -
  - （**豪**）

  - （**甲斐剎那**）

  - （本庄新）

  - [仙界傳封神演義](../Page/封神演義_\(漫畫\).md "wikilink")（16歲的聞仲）

  - ZERO ZANY.（ZERO）

      - ZERO ZANY. CODE-2

  - Snow Flower（**仁科雪**）

  -
  -
  - [心跳回憶 Girl's Side](../Page/心跳回憶_Girl's_Side.md "wikilink")
    Clovers'Graffiti7 蒼樹千晴（蒼樹千晴）

  - [火影忍者](../Page/火影忍者.md "wikilink")
    系列（**[奈良鹿丸](../Page/奈良鹿丸.md "wikilink")**）

      -
  - （陸）

  -
  - （**沖田総司**）

      -
      -
      -
      -
      - ～音声奏曲集～

  - [幕末恋華 新選組](../Page/幕末恋華_新選組.md "wikilink") （永倉新八）

  - [偷偷愛著你](../Page/偷偷愛著你.md "wikilink")（**中津秀一**）

  -   -
      -
      -
  - [夢幻奇緣2系列](../Page/夢幻奇緣2.md "wikilink")（**亞克‧海靈頓**）

      - Marine Style
      - Aoi Romance
      - Aqua Balance

  - [驚爆危機](../Page/驚爆危機.md "wikilink")（**相良宗介**）

  - Fragrance Tale CD drama 1 - 2（）

  - [女神異聞錄4Vol](../Page/女神異聞錄4.md "wikilink").1 - 2（**花村陽介**）

  - [妹妹戀人系列](../Page/妹妹戀人.md "wikilink")（**結城賴**）

  - （乙部清丸）

  -
  - （**朱皇**）

  - （迫井雷治郎）

      -
      -
      -
  - [魔術師歐菲系列](../Page/魔術師歐菲.md "wikilink")（**歐菲**）

      -
      -
  - [魔偵探洛基系列](../Page/魔偵探洛基.md "wikilink")（鳴神）

  - [Mr.FULLSWING系列](../Page/Mr.FULLSWING.md "wikilink")（虎鐵大河）

  - 新音盤物語（高條筑陽）

  - [完美小姐進化論](../Page/完美小姐進化論.md "wikilink")（**高野恭平**）

  -
### BLCD

  - （**吉峰清太郎**）

  -
  - （**羽村結**）

  - （那智）

  - （**杉浦真琴**）

  - WEST END4、5（）

  - 過激系列（****）

      -
      -
      -
      -
      -
  - （九鬼虎次郎）

      -
      -
  - 絆―KIZUNA― 2、4（）

  - （**高梨亨**）

      - （**高梨亨**）

  - （**藤堂一輝**）

      -
      -
      -
  - ～Be Yell～（武藤健吾）

  - 系列（新田薰）

      -
      -
      -
      -
      -
      -
      -
  - （聖）

  - 純情系列（淺香津）

      - 純情BOY禁獵區

      -
      -
      -
  - 絕對服從（芹澤）

  - （真行寺兼滿）

      -
      -
      -
      -
      -
      -
  -
  - （樋野勇樹）

  - （**三本木恒彥**）

  - （淺野伸之）

      - （相澤正之進）

  - BL探偵（八雲和也）

  - （廣瀨克）

  - （**風間廉示**）

  - （**柳瀨敦**）

      -
      -
      -
      -
  - （飯田賢吾）

  -   -
      -
      - Kiss station

      -
      - Sweet Collection

      - Ruby Special CD Version A・B

  -   -
### 舞台

  -
  -   -
      -
  -
  -
  -
  -
## 音樂唱片

### 本人名義

**單曲**

  - The Answer（2001.06.20/CPDA-1016（TGCS-1143））
      - 01\. The Answer（）
      - 02\. Happy Monday Man
  - begin the TRY（2002.10.02）
      - 01\. begin the TRY（）
      - 02\. begin the TRY（）
  - [Lazy
    Mind](../Page/Lazy_Mind.md "wikilink")（2003.08.06/CPDA-1022（TGCS-1792））
      - 01\. Lazy Mind（）
      - 02\. Lazy Mind（Instrumental）
      - 03\. ROCKMAN X Voice Collection
  - [Ride Free](../Page/Ride_Free.md "wikilink")（2008.11.26/LACM-4540）
      - 01\. Ride Free
      - 02\. Crazy Night
      - 03\. DoPaMiNe
  - Parallel World（2009.05.13/LACM-4608）
      - 01\. Parallel World（）
      - 02\. Home sweet home（）
      - 03\. Never surrender（）
  - Stand down （2010.12.08/LACM-4770）
      - 01.Stand down
      - 02\. MIRROR
      - 03\. Cloudy sky

**專輯**

  - 髄（Zui）2001.07.18/CPCA-1054（TGCS-1144））
      - 01\. Build Up\!\!\!（鍛）
      - 02\. Power
      - 03\. G行為
      - 04\. 身体檢査
      - 05\. Rainy Day
      - 06\. The Answer（）
      - 07\. Hedgehog Peter
      - 08\. 臆病者
      - 09\. Moon Light（）
      - 10\. End of the sky

**DVD**

  - Showtaro Morikubo LIVE TOUR '01
  - Original Entertainment Paradise ”（2009.4.22）

###

  - A・G・A・S/dream power （Single）

  -
  - （Single）

  - （Single）

      - 01\.
      - 02\. Whenever I go（森久保祥太郎名義）

  -   - 01\. （GAME VERSION）
      - 02\. （GAME VERSION）
      - 34\. （FULL SIZE VERSION）
      - 35\. （FULL SIZE VERSION）

  - （Single）

      - 01\.
      - 02\.

  -   - 04\.

  - Brand-new ∞
    JUNCTION（森久保祥太郎、[杉田智和](../Page/杉田智和.md "wikilink")、[森田成一](../Page/森田成一.md "wikilink")）（Single）

  - （[關俊彥](../Page/關俊彥.md "wikilink")、森久保祥太郎）

**Cross Chord関連**

  - shootin’stars（Heart-beat/[高橋廣樹](../Page/高橋廣樹.md "wikilink")）（Single）
  - CONTINUED（Heart-beat）（Single）
  - Hard Spirit（Heart-beat）（Album）
  - SHOW（ starring 森久保祥太郎）（Album）

### 樂隊

**Mosquito Milk**

  - Monkey（Single）
      - 01\. Monkey（）
      - 02\. 水之中（）
  - Space Surfer（Single）
      - 01\. Space Surfer（）
      - 02\. Sunny Day
      - 03\. Free Jam Question

**[AN's ALL STARS](../Page/AN's_ALL_STARS.md "wikilink")**

  - PAINT IT WHITE（Single）

      - 01\. PAINT IT WHITE
      - 02\. A-N-S～We are An's～

  - Alright\!\!（Single）

      - 01\. Alright\!\!
      - 02\. Special Track「酒」
      - 03\. Alright\!\! with Hide ver.
      - 04\. Alright\!\! with Showtaro ver.

  - AN'S JAM（Album）

      - 01\. INTRODUCTION
      - 02\.
      - 03\. PJ
      - 04\. OVER
      - 05\. Stand
      - 06\. Jamm'n da An's
      - 07\. Happy X'mas
      - 08\. 『Peace』
      - 09\. Thank U

  -   - 01\. (Full Version)
      - 02\. You are my sky,You are my ocean...(Full Version)
      - 35\. (Karaoke)
      - 36\. You are my sky,You are my ocean...(Karaoke)

  - （Album）

      - 01\.
      - 02\. A-N-S～We are An's～（feat.Honey-Hony-Horns）
      - 03\. （feat.Honey-Hony-Horns）
      - 04\. XTC...
      - 05\. Brand-new-day
      - 06\. Alright\!\!（feat.Honey-Hony-Horns）
      - 07\. （Album ver.）
      - 08\. Native Song

  - Liv’n Roll 1st TOUR（企画Album）

  - X-tension（Album）

      - 01\. LOUD
      - 02\. Like a cloud
      - 03\. Go my way
      - 04\. You are my sky, you are my ocean...～Next generation～
      - 05\. 「」
      - 06\. Paint it white
      - 07\. Happy Birthday
      - 08\. 未来 -ANS ver.-
      - 09\. Thank U '09
      - Special track

  -
  - Twinkle Date

  -
  - Vitamin（DVD）

  - Virgin?Live Tour2008\~Welcome to An’s HOSPITAL（DVD）

### 角色歌

  - [Are you Alice?](../Page/Are_you_Alice?.md "wikilink") （白兔子）

      -
        「LIDDELL.」
        「THE END.」

  - [閃靈二人組](../Page/閃靈二人組.md "wikilink")（天野銀次）

      -
        「Born again」
        「TWINS」（）

  - （織田優生）

      -
        「sign」
        「Who am I...? 」（）

  - （豪）

      -
        「THE GREAT JOURNEY～大航海～」

  - [真·女神轉生 惡魔之子](../Page/真·女神轉生_惡魔之子.md "wikilink")（甲斐剎那）

      -
        「Invisible Hearts」
        「Peace and Love」

  - [無敵偵探貴公子](../Page/無敵偵探貴公子.md "wikilink")（信濃紫炎）

      -
        「I'm on Fire

  - （歐菲）

      -
        「Deep In The Darkest Blue」

  - ZERO ZANY.（ZERO）

      -
        「UNDER THE WORLD」（THE WORKS 3.0に収録）

  - [閃耀的宇宙之詩](../Page/閃耀的宇宙之詩.md "wikilink")（）

      -
        「OUR NEWS」
        「little by little」

  - [超速搖搖](../Page/超速搖搖.md "wikilink")（堂本瞬一）

      -
        「Loop\&Loop」

  - [網球王子](../Page/網球王子.md "wikilink")（切原赤也）

      -
        「
        「Time has come」
        「Gather」（
        「Wonderful days」（
        「業火絢爛」（

  - [心跳回憶 Girl's Side](../Page/心跳回憶_Girl's_Side.md "wikilink")（蒼樹千晴）

      -

        「大胆華恋」（[三木真一郎](../Page/三木真一郎.md "wikilink")、森久保祥太郎、[山口勝平](../Page/山口勝平.md "wikilink")）

  - [火影忍者](../Page/火影忍者.md "wikilink")（奈良鹿丸）

      -

        「Re:member」

  - [薄櫻鬼](../Page/薄櫻鬼.md "wikilink")（沖田總司）

      -
        「牙月」

  - [幕末恋華 新撰組](../Page/幕末恋華_新撰組.md "wikilink")（永倉新八）

      -

  - [VitaminZ](../Page/VitaminZ.md "wikilink")

      -

        「Single Match」

  - [魔偵探洛基 RAGNAROK](../Page/魔偵探洛基_RAGNAROK.md "wikilink")（鳴神）

      -

  - [棒球大聯盟](../Page/棒球大聯盟.md "wikilink")（茂野吾郎）

      -

  - [秋之回憶系列](../Page/秋之回憶系列.md "wikilink")（河合春人）

      -

  - [AN's ALL STARS](../Page/AN's_ALL_STARS.md "wikilink")

## 參考資料

## 外部連結

  - [森久保祥太郎日本官方網站](https://add9th.co.jp/)
  - [A-W-S An's all stars web
    site](https://web.archive.org/web/20070402235832/http://www.ans-allstars.com/)


[Category:日本男性配音員](../Category/日本男性配音員.md "wikilink")
[Category:日本廣播主持人](../Category/日本廣播主持人.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")
[Category:VIMS所属声优](../Category/VIMS所属声优.md "wikilink")
[Category:明治大學校友](../Category/明治大學校友.md "wikilink")

1.

2.

3.

4.

5.

6.
7.