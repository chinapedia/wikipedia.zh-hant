**長樂**（399年正月-401年七月）是[十六国时期](../Page/十六国.md "wikilink")[后燕君主](../Page/后燕.md "wikilink")[慕容盛的](../Page/慕容盛.md "wikilink")[年號](../Page/年號.md "wikilink")，共計3年，也是後燕的第八個年號。

## 纪年

| 長樂                               | 元年                             | 二年                             | 三年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 399年                           | 400年                           | 401年                           |
| [干支](../Page/干支纪年.md "wikilink") | [己亥](../Page/己亥.md "wikilink") | [庚子](../Page/庚子.md "wikilink") | [辛丑](../Page/辛丑.md "wikilink") |

## 参看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 同期存在的其他政权年号
      - [隆安](../Page/隆安_\(年号\).md "wikilink")（397年正月-401年十二月）：東晉皇帝[晋安帝司马德宗的年号](../Page/晋安帝.md "wikilink")
      - [太初](../Page/太初_\(乞伏乾歸\).md "wikilink")（388年六月-400年七月）：[西秦政权](../Page/西秦.md "wikilink")[乞伏乾歸年号](../Page/乞伏乾歸.md "wikilink")
      - [皇初](../Page/皇初.md "wikilink")（394年五月-399年九月）：[后秦政权](../Page/后秦.md "wikilink")[姚興年号](../Page/姚興.md "wikilink")
      - [弘始](../Page/弘始.md "wikilink")（399年九月-416年正月）：[后秦政权](../Page/后秦.md "wikilink")[姚兴年号](../Page/姚兴.md "wikilink")
      - [龙飞](../Page/龙飞_\(后凉\).md "wikilink")（396年六月-399年十二月）：[后凉政权](../Page/后凉.md "wikilink")[吕光年号](../Page/吕光.md "wikilink")
      - [咸宁](../Page/咸宁_\(后凉\).md "wikilink")（399年十二月-401年正月）：[后凉政权](../Page/后凉.md "wikilink")[吕纂年号](../Page/吕纂.md "wikilink")
      - [神鼎](../Page/神鼎.md "wikilink")（401年二月-403年八月）：[后凉政权](../Page/后凉.md "wikilink")[吕隆年号](../Page/吕隆.md "wikilink")
      - [太初](../Page/太初_\(南凉\).md "wikilink")（397年正月-399年十二月）：[南凉政权](../Page/南凉.md "wikilink")[秃发乌孤年号](../Page/秃发乌孤.md "wikilink")
      - [建和](../Page/建和_\(南凉\).md "wikilink")（400年正月-402年三月）：[南凉政权](../Page/南凉.md "wikilink")[秃发利鹿孤年号](../Page/秃发利鹿孤.md "wikilink")
      - [庚子](../Page/庚子_\(西涼\).md "wikilink")（400年十一月-404年）：[西凉政权](../Page/西凉.md "wikilink")[李暠年号](../Page/李暠.md "wikilink")
      - [神玺](../Page/神玺.md "wikilink")（397年五月-399年正月）：[北凉政权](../Page/北凉.md "wikilink")[段业年号](../Page/段业.md "wikilink")
      - [天玺](../Page/天玺_\(北凉\).md "wikilink")（399年二月-401年五月）：[北凉政权](../Page/北凉.md "wikilink")[段业年号](../Page/段业.md "wikilink")
      - [永安](../Page/永安_\(北凉\).md "wikilink")（401年六月-412年十月）：[北凉政权](../Page/北凉.md "wikilink")[沮渠蒙逊年号](../Page/沮渠蒙逊.md "wikilink")
      - [天兴](../Page/天兴_\(北魏\).md "wikilink")（398年十二月-404年十月）：[北魏政权](../Page/北魏.md "wikilink")[拓跋珪年号](../Page/拓跋珪.md "wikilink")
      - [建平](../Page/建平_\(南燕\).md "wikilink")（400年正月-405年十一月）：[南燕政权](../Page/南燕.md "wikilink")[慕容德年号](../Page/慕容德.md "wikilink")

## 参考文献

1.  李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129

## 消歧義

  - [長樂](../Page/長樂.md "wikilink")

[Category:後燕年號](../Category/後燕年號.md "wikilink")
[Category:4世纪年号](../Category/4世纪年号.md "wikilink")
[Category:5世纪年号](../Category/5世纪年号.md "wikilink")
[Category:390年代中国政治](../Category/390年代中国政治.md "wikilink")
[Category:400年代中国政治](../Category/400年代中国政治.md "wikilink")