## 大事记

  - [伊凡四世之子](../Page/伊凡四世.md "wikilink")[費奧多爾一世即位](../Page/費奧多爾一世.md "wikilink")[俄羅斯沙皇國](../Page/俄羅斯沙皇國.md "wikilink")[沙皇](../Page/沙皇.md "wikilink")，[俄羅斯沙皇國日漸遠離](../Page/俄羅斯沙皇國.md "wikilink")[西歐的政局和影響力](../Page/西歐.md "wikilink")。
  - [暹罗对](../Page/泰国.md "wikilink")[缅甸作战](../Page/缅甸.md "wikilink")，这是历史上最大的一次[战象战斗](../Page/战象.md "wikilink")，缅甸战胜，从此从暹罗独立。
  - [利玛窦绘制第一张中文](../Page/利玛窦.md "wikilink")[世界地图](../Page/世界地图.md "wikilink")《[山海舆地全图](../Page/山海舆地全图.md "wikilink")》。
  - [朱载堉出版](../Page/朱载堉.md "wikilink")《[律吕精义](../Page/律吕精义.md "wikilink")》，发明[十二平均律](../Page/十二平均律.md "wikilink")。
  - [阿尔汉格尔斯克建城](../Page/阿尔汉格尔斯克.md "wikilink")。
  - [定陵开建](../Page/定陵.md "wikilink")。
  - 辽东[巡抚](../Page/巡抚.md "wikilink")[李松](../Page/李松_\(明朝\).md "wikilink")、总兵[李成梁用](../Page/李成梁.md "wikilink")“市圈计”杀死叶赫贝勒[杨吉砮](../Page/杨吉砮.md "wikilink")、[清佳砮](../Page/清佳砮.md "wikilink")。
  - 3月——[冲田畷之战](../Page/冲田畷之战.md "wikilink")，[有馬晴信](../Page/有馬晴信.md "wikilink")、[島津家久聯軍擊敗](../Page/島津家久.md "wikilink")[龍造寺隆信](../Page/龍造寺隆信.md "wikilink")。
  - [3月3日](../Page/3月3日.md "wikilink")——[織田信雄處死家臣](../Page/織田信雄.md "wikilink")[岡田重孝](../Page/岡田重孝.md "wikilink")（尾張星崎城主）、[津川義冬](../Page/津川義冬.md "wikilink")（伊勢松島城主）、[淺井長時](../Page/淺井長時.md "wikilink")，形同與[羽柴秀吉宣戰](../Page/羽柴秀吉.md "wikilink")。
  - [3月17日](../Page/3月17日.md "wikilink")——[英山](../Page/英山县.md "wikilink")[地震](../Page/地震.md "wikilink")。
  - [3月18日](../Page/3月18日.md "wikilink")——[费多尔·伊万诺维奇继为](../Page/费奥多尔·伊万诺维奇.md "wikilink")[俄罗斯帝国](../Page/俄罗斯帝国.md "wikilink")[沙皇](../Page/沙皇.md "wikilink")。
  - [3月28日](../Page/3月28日.md "wikilink")——[德川家康抵達小牧山與](../Page/德川家康.md "wikilink")[織田信雄集結](../Page/織田信雄.md "wikilink")，[小牧·長久手之戰開始](../Page/小牧·長久手之戰.md "wikilink")。
  - [7月10日](../Page/7月10日.md "wikilink")——[奧蘭治王子威廉一世遭到暗殺](../Page/威廉一世_\(奥兰治\).md "wikilink")。
  - [11月7日](../Page/11月7日.md "wikilink")——[羽柴秀吉與](../Page/羽柴秀吉.md "wikilink")[織田信雄達成休戰協定](../Page/織田信雄.md "wikilink")，[小牧·長久手之戰落幕](../Page/小牧·長久手之戰.md "wikilink")。
  - 12月——[德川家康將次子](../Page/德川家康.md "wikilink")（後來的[結城秀康](../Page/結城秀康.md "wikilink")）送與秀吉作養子，臣服於羽柴秀吉。

## 出生

  - [宫本武藏](../Page/宫本武藏.md "wikilink")，日本武士（逝世[1645年](../Page/1645年.md "wikilink")）
  - [胡正言](../Page/胡正言.md "wikilink")，明朝镌刻家（逝世不详）
  - [6月6日](../Page/6月6日.md "wikilink")——[袁崇焕](../Page/袁崇焕.md "wikilink")，明朝将军（逝世[1630年](../Page/1630年.md "wikilink")）

## 逝世

  - [天野隆重](../Page/天野隆重.md "wikilink")。
  - [3月18日](../Page/3月18日.md "wikilink")——[伊凡四世](../Page/伊凡四世.md "wikilink")，俄罗斯沙皇（出生[1530年](../Page/1530年.md "wikilink")）
  - [5月4日](../Page/5月4日.md "wikilink")——[龍造寺隆信](../Page/龍造寺隆信.md "wikilink")、[百武賢兼](../Page/百武賢兼.md "wikilink")、[木下昌直](../Page/木下昌直.md "wikilink")、[江里口信常](../Page/江里口信常.md "wikilink")、[圓城寺信胤](../Page/圓城寺信胤.md "wikilink")。
  - [5月18日](../Page/5月18日.md "wikilink")——[森長可](../Page/森長可.md "wikilink")、[池田恆興](../Page/池田恆興.md "wikilink")、[池田元助](../Page/池田元助.md "wikilink")。
  - [5月26日](../Page/5月26日.md "wikilink")——[蒲生賢秀](../Page/蒲生賢秀.md "wikilink")，日本室町末期，安土桃山時代武士（出生[1534年](../Page/1534年.md "wikilink")）
  - [7月10日](../Page/7月10日.md "wikilink")——[威廉一世
    (奧蘭治)](../Page/威廉一世_\(奧蘭治\).md "wikilink")，[八十年戰爭領導人之一](../Page/八十年戰爭.md "wikilink")，曾任[荷蘭共和國第一任](../Page/荷蘭共和國.md "wikilink")[執政](../Page/荷蘭省督.md "wikilink")。
  - [9月15日](../Page/9月15日.md "wikilink")——[筒井順慶](../Page/筒井順慶.md "wikilink")。

[\*](../Category/1584年.md "wikilink")
[4年](../Category/1580年代.md "wikilink")
[8](../Category/16世纪各年.md "wikilink")