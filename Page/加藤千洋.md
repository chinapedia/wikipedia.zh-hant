**加藤千洋**（）是[朝日新聞的](../Page/朝日新聞.md "wikilink")[編輯委員](../Page/編輯委員.md "wikilink")。[東京都出身](../Page/東京都.md "wikilink")。[東京外國語大学](../Page/東京外國語大学.md "wikilink")[中國語学科畢業](../Page/中國語.md "wikilink")。2004年起，擔任『[報道STATION](../Page/報道STATION.md "wikilink")』（[朝日電視台](../Page/朝日電視台.md "wikilink")）[評論員](../Page/評論員.md "wikilink")（[周一](../Page/周一.md "wikilink")
- [周四](../Page/周四.md "wikilink")）。

## 主要經歷

  - [麻布中学校・高等学校畢業](../Page/麻布中学校・高等学校.md "wikilink")。
  - 1972年　[東京外國語大学中国語学畢業後](../Page/東京外國語大学.md "wikilink")、進入[朝日新聞社](../Page/朝日新聞社.md "wikilink")。
      - 曾任[北京特派員](../Page/北京.md "wikilink")、[AERA編集部記者](../Page/AERA.md "wikilink")、論説委員、[亞洲總局長](../Page/亞洲.md "wikilink")、[中国總局長](../Page/中華人民共和國.md "wikilink")、外報部長。
  - 2002年　任編集委員。
  - 2004年　出演報道STAITON評論員。

## 著作

### 單獨著作

  - （ no.213） [岩波書店](../Page/岩波書店.md "wikilink") 1991年8月 ISBN
    4-00-003153-8

  - [平凡社](../Page/平凡社.md "wikilink") 2003年10月 ISBN 4-582-83185-0

  - 朝日新聞社 2004年6月 ISBN 4-02-257927-7

  - [小學館](../Page/小學館.md "wikilink") 2005年3月 ISBN 4-09-387548-0

### 共同著作

  - （[堀江義人](../Page/堀江義人.md "wikilink")） 朝日新聞社 1983年2月

  - （[天兒慧](../Page/天兒慧.md "wikilink")）（[岩波新書](../Page/岩波新書.md "wikilink")
    新赤版 137） 岩波書店 1990年9月 ISBN 4-00-430137-8

  - （[辻康吾](../Page/辻康吾.md "wikilink")・共編）『原典中国現代史　第4巻 -- 社会』 岩波書店
    1995年4月 ISBN 4-00-003804-4

  - （[楊炳章](../Page/楊炳章.md "wikilink")）
    ・著、加藤千洋・加藤優子・）『[鄧小平政治的傳記](../Page/鄧小平.md "wikilink")』
    朝日新聞社 1999年9月 ISBN 4-02-257353-8

  - （朝日新聞アタ取材班・著） [草思社](../Page/草思社.md "wikilink") 2002年4月 ISBN
    4-7942-1137-6

  - （[加藤周一](../Page/加藤周一.md "wikilink")・[王敏](../Page/王敏.md "wikilink")・[王暁平](../Page/王暁平.md "wikilink")）
    2006年7月 ISBN 4-7803-0043-6

::\*

[category:東京都出身人物](../Page/category:東京都出身人物.md "wikilink")

[Category:東京外國語大學校友](../Category/東京外國語大學校友.md "wikilink")