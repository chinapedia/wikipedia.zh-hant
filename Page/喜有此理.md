《**喜有此理**》（）是由[香港](../Page/香港.md "wikilink")[亞洲電視製作的](../Page/亞洲電視.md "wikilink")[處境喜劇](../Page/處境喜劇.md "wikilink")，採用即拍即播形式，令劇情緊貼時事新聞，並以此為賣點。

《喜有此理》由多位演員主演，包括復出拍[電視劇的](../Page/電視劇.md "wikilink")[盧海鵬](../Page/盧海鵬.md "wikilink")，已很少在電視出現的[李香琴](../Page/李香琴.md "wikilink")，及從未拍過電視劇的[元秋等](../Page/元秋.md "wikilink")。劇中重心以滕氏一家與隔鄰街坊為主，以時事為主題，務求引起觀眾共鳴。元秋與亞視的三個月合約約滿後，退出《喜有此理》，改由[森森繼續飾演其角色](../Page/森森.md "wikilink")。亞視以「到韓國整容」為兩位演員作出過渡安排。《喜有此理》是亞視自製電視劇中集數最多的（共213集）的電視劇集，但此紀錄已被《[大城小故事](../Page/大城小故事.md "wikilink")》超越。

此劇曾到[番禺作實景拍攝](../Page/番禺.md "wikilink")，製作特別版《喜有此理之大班群星賀中秋》。

劇中演員[元秋](../Page/元秋.md "wikilink")、[曾瑋明](../Page/曾瑋明.md "wikilink")、[翟威廉己轉投](../Page/翟威廉.md "wikilink")[無綫電視](../Page/無綫電視.md "wikilink")，而出身於無綫電視[李香琴及](../Page/李香琴.md "wikilink")[陳展鵬先後两年重返](../Page/陳展鵬.md "wikilink")「娘家」。

## 角色

  - [盧海鵬](../Page/盧海鵬.md "wikilink") 飾 滕一鳴
  - [元秋](../Page/元秋.md "wikilink")／[森森](../Page/森森.md "wikilink") 飾 廖小蝶
  - [李香琴](../Page/李香琴.md "wikilink") 飾 白雪
  - [樓南光](../Page/樓南光.md "wikilink") 飾 滕拓海
  - [周子濠](../Page/周子濠.md "wikilink") 飾 滕子濠
  - [郭家頤](../Page/郭家頤.md "wikilink") 飾 滕家頤
  - [黃璦瑤](../Page/黃璦瑤.md "wikilink") 飾 朱意盛
  - [陳展鵬](../Page/陳展鵬.md "wikilink") 飾 珍寶/浩南
  - [吳亭欣](../Page/吳亭欣.md "wikilink") 飾 馬如風
  - [曾瑋明](../Page/曾瑋明.md "wikilink") 飾 米奇
  - [翟威廉](../Page/翟威廉.md "wikilink") 飾 程光輝
  - [麥家琪](../Page/麥家琪.md "wikilink") 飾 MAY MAY
  - [吳浣儀](../Page/吳浣儀.md "wikilink") 飾 滕一菲

[Category:2005年亞視電視劇集](../Category/2005年亞視電視劇集.md "wikilink")
[Category:2006年亞視電視劇集](../Category/2006年亞視電視劇集.md "wikilink")
[Category:香港電視情景喜劇](../Category/香港電視情景喜劇.md "wikilink")