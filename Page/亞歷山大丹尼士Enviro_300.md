[Alexander_Dennis_Enviro_300_demonstrator_YN12_AAK.jpg](https://zh.wikipedia.org/wiki/File:Alexander_Dennis_Enviro_300_demonstrator_YN12_AAK.jpg "fig:Alexander_Dennis_Enviro_300_demonstrator_YN12_AAK.jpg")
**亞歷山大丹尼士Enviro
300**是一款由[英國](../Page/英國.md "wikilink")[亞歷山大丹尼士生產的兩軸單層巴士](../Page/亞歷山大丹尼士.md "wikilink")，它也是亞歷山大丹尼士製造的其中一款Enviro系列巴士。它於2001年面世，主要取代ALX300車身。

## 設計

Enviro
300的底盤是由亞歷山大丹尼士自行生產，可以配用[康明斯ISBe](../Page/康明斯.md "wikilink")-225歐盟五代環保標準引擎或[康明斯ISBe](../Page/康明斯.md "wikilink")-250歐盟五代環保標準引擎，另外配合三款波箱，分別Voith
DIWA854.5四前速波箱或者ZF Ecolife 6AP1200B六前速波箱及Allison T280R五前速波箱。

Enviro
300的車身採用[鋁製支架](../Page/鋁.md "wikilink")，[玻璃纖維外殼](../Page/玻璃纖維.md "wikilink")，亦可以安裝在[猛獅](../Page/猛獅.md "wikilink")18.240、[富豪B7RLE或](../Page/富豪B7RLE.md "wikilink")[斯堪尼亞K230UB底盤上](../Page/斯堪尼亞K230UB.md "wikilink")。

2011年，Enviro 300改成符合歐洲委員會的Whole Vehicle Type Approval標準（簡稱ECWVTA）。

2012年，亞歷山大丹尼士與Scania合作研發Enviro 300SG（Scania Gas）大型單層天然氣巴士，採用Scania
K270UB底盤及Enviro 300車身。

### Enviro 300車身配其他底盤

2008年，英國Stagecoach購入超過200輛裝配Enviro 300車身的猛獅18.240。同年，首批共45輛配Enviro
300車身的富豪B7RLE付運予[北愛爾蘭的巴士公司Ulsterbus](../Page/北愛爾蘭.md "wikilink")。

Stagecoach於2011年開始接收首50輛Scania K230UB/Enviro 300。

## 停產

最後一輛Enviro 300於2015年底交付予Stagecoach。Enviro 300底盤，同時被全長11.5米或11.8米的Enviro
200 MMC取代，另外亞歷山大丹尼士將會推出Enviro 300 MMC車身，配上富豪B8RLE或Scania KUB大型巴士底盤。

## 參見

其他亞歷山大丹尼士巴士產品：

  - [飛鏢SLF](../Page/丹尼士飛鏢巴士.md "wikilink")
  - [三叉戟二型](../Page/丹尼士三叉戟二型.md "wikilink")
  - [Enviro 200 Dart](../Page/亞歷山大丹尼士Enviro_200_Dart.md "wikilink")
  - [Enviro 350H](../Page/亞歷山大丹尼士Enviro_350H.md "wikilink")
  - [Enviro 400](../Page/亞歷山大丹尼士Enviro_400.md "wikilink")
  - [Enviro 500](../Page/亞歷山大丹尼士Enviro_500.md "wikilink")

## 外部連結

  - [Enviro 300
    官方網站](https://web.archive.org/web/20110616210351/http://www.alexander-dennis.com/product-details.php?s=82&subs=43&tableID=220&itemID=1)

[Category:巴士型號](../Category/巴士型號.md "wikilink")
[Category:丹尼士巴士](../Category/丹尼士巴士.md "wikilink")
[Category:低地台巴士](../Category/低地台巴士.md "wikilink")