[Yamakunigawa_River_and_Mount_Kyoshuho_from_Zenkaibashi_Bridge_2.jpg](https://zh.wikipedia.org/wiki/File:Yamakunigawa_River_and_Mount_Kyoshuho_from_Zenkaibashi_Bridge_2.jpg "fig:Yamakunigawa_River_and_Mount_Kyoshuho_from_Zenkaibashi_Bridge_2.jpg")
**耶馬溪**是位於[日本](../Page/日本.md "wikilink")[九州](../Page/九州.md "wikilink")[大分縣](../Page/大分縣.md "wikilink")[中津市](../Page/中津市.md "wikilink")[山國川的中上游的](../Page/山國川.md "wikilink")[溪谷](../Page/溪谷.md "wikilink")，獲選為[新日本三景之一](../Page/新日本三景.md "wikilink")，1923年日本政府將此地指定為名勝，1950年，進一步將附近區域劃為「[耶馬日田英彥山國定公園](../Page/耶馬日田英彥山國定公園.md "wikilink")」。

該地屬於石灰質的[熔岩地形](../Page/熔岩.md "wikilink")，經水流切割而成溪谷地形，包括本耶馬溪、裏耶馬溪、深耶馬溪、奧耶馬溪，都以耶馬溪為名。

「耶馬溪」一詞，是由1818年日本學者[賴山陽](../Page/賴山陽.md "wikilink")（陽明學派、漢詩學家，1780年生、1832年歿）來此地造訪，看見山中的美麗景致，便吟「耶馬溪耶麻天下無」詠嘆美景，從此之後此地便以「耶馬溪」命名，不過這個只是當地的傳說，並沒有相關的考據可以證實這個說法。

## 景點

[Ao_no_Domon_tunnel_2.jpg](https://zh.wikipedia.org/wiki/File:Ao_no_Domon_tunnel_2.jpg "fig:Ao_no_Domon_tunnel_2.jpg")
[Hitome-Hakkei_in_Shin-Yabakei_Valley_from_observation_deck_20141117-4.JPG](https://zh.wikipedia.org/wiki/File:Hitome-Hakkei_in_Shin-Yabakei_Valley_from_observation_deck_20141117-4.JPG "fig:Hitome-Hakkei_in_Shin-Yabakei_Valley_from_observation_deck_20141117-4.JPG")

### 本耶馬溪

本耶馬溪的範圍為[青之洞門及](../Page/青之洞門.md "wikilink")[競秀峰週邊的區域](../Page/競秀峰.md "wikilink")。

### 深耶馬溪

深耶馬溪的範圍為山國川支流山移川附近的溪谷，主要景點為「一目八景」，可同時眺望海望嶺、仙人岩、嘯猿山、夫婦岩、群猿山、烏帽子岩、雄鹿長尾嶺、鷲之巢山八個景色。

### 裏耶馬溪

裏耶馬溪為金吉川上游的溪古，在靠近[玖珠町邊界處](../Page/玖珠町.md "wikilink")。

### 奥耶馬溪

奥耶馬溪位於山國川上游，主要景點為以「猿飛」命名的景點，包括猿飛千壺峽，耶馬溪猿飛的甌穴群等。

### 椎屋耶馬溪

椎屋耶馬溪在靠近[宇佐市的驛館川支流溫見川源頭附近](../Page/宇佐市.md "wikilink")，隣近[岳切溪谷](../Page/岳切溪谷.md "wikilink")。

### 津民耶馬溪

津民耶馬溪位於山國川支流津民川。

## 日本各地的耶馬溪

  - [日高耶馬溪](../Page/日高耶馬溪.md "wikilink")（[北海道](../Page/北海道.md "wikilink")[日高支廳](../Page/日高支廳.md "wikilink")）
  - 蝦夷之耶馬溪 →
    [定山溪](../Page/定山溪.md "wikilink")（北海道[石狩支廳](../Page/石狩支廳.md "wikilink")）
  - 東北之耶馬溪 →
    [抱返溪谷](../Page/抱返溪谷.md "wikilink")（[秋田縣](../Page/秋田縣.md "wikilink")）・[猊鼻溪](../Page/猊鼻溪.md "wikilink")（[岩手縣](../Page/岩手縣.md "wikilink")）
  - 關東之耶馬溪 →
    [吾妻峽](../Page/吾妻峽.md "wikilink")（[群馬縣](../Page/群馬縣.md "wikilink")）・[高津戶峽](../Page/高津戶峽.md "wikilink")（群馬縣）
  - 信州（信濃）之耶馬溪 →
    [角間溪谷](../Page/角間溪谷.md "wikilink")・[內山峽](../Page/內山峽.md "wikilink")・[布引溪谷](../Page/布引溪谷.md "wikilink")（[長野縣](../Page/長野縣.md "wikilink")）
  - 關西之耶馬溪 →
    [香落溪](../Page/香落溪.md "wikilink")（[三重縣](../Page/三重縣.md "wikilink")）
  - 攝津之耶馬溪 →
    [攝津峽](../Page/攝津峽.md "wikilink")（[大阪府](../Page/大阪府.md "wikilink")）
  - 山陰之耶馬溪 →
    [立久惠峽](../Page/立久惠峽.md "wikilink")（[島根縣](../Page/島根縣.md "wikilink")）
  - [筑紫耶馬溪](../Page/筑紫耶馬溪.md "wikilink")（[福岡縣](../Page/福岡縣.md "wikilink")）
  - 國東之耶馬溪 →
    [大分縣](../Page/大分縣.md "wikilink")[國東半島一帶](../Page/國東半島.md "wikilink")。
      - 田染耶馬（[豐後高田市田染](../Page/豐後高田市.md "wikilink")、[杵築市大田](../Page/杵築市.md "wikilink")、杵築市山香町）
      - 文殊耶馬（[國東市大恩寺](../Page/國東市.md "wikilink")）
      - 黑土耶馬（豐後高田市黑土）
      - 夷耶馬（豐後高田市夷谷）
      - 天然寺耶馬（豐後高田市長岩屋）
      - 千灯寺耶馬（國東市千灯）
      - 岩戶寺耶馬（國東市岩戶寺）

## 台灣的耶馬溪

  - 平溪耶馬溪（[新北市](../Page/新北市.md "wikilink")[平溪線沿線的](../Page/平溪線.md "wikilink")[基隆河](../Page/基隆河.md "wikilink")）
  - 埔里耶馬溪（[南投縣](../Page/南投縣.md "wikilink")[埔里鎮耶馬溪](../Page/埔里鎮.md "wikilink")）
  - 六龜耶馬溪（[高雄市](../Page/高雄市.md "wikilink")[六龜區](../Page/六龜區.md "wikilink")[荖濃溪](../Page/荖濃溪.md "wikilink")）
  - 台東耶馬溪（[台東縣](../Page/台東縣.md "wikilink")[馬武窟溪](../Page/馬武窟溪.md "wikilink")）

## 關連項目

  - [青之洞門](../Page/青之洞門.md "wikilink")
  - [耶馬溪町](../Page/耶馬溪町.md "wikilink")
  - [本耶馬溪町](../Page/本耶馬溪町.md "wikilink")

## 外部連結

  - [中津耶馬溪觀光協會](http://www.nakatsuyaba.com/)

[Category:中津市](../Category/中津市.md "wikilink")
[Category:大分縣觀光地](../Category/大分縣觀光地.md "wikilink")
[Category:日本峽谷](../Category/日本峽谷.md "wikilink")
[Category:大分縣地理](../Category/大分縣地理.md "wikilink")