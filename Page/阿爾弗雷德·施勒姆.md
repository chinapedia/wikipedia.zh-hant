**阿爾弗雷德·施勒姆** **Alfred
Schlemm**（）德軍[傘兵二級](../Page/傘兵.md "wikilink")[上將](../Page/上將.md "wikilink")，他最後一場英勇戰鬥是帥領德國傘兵在1945年1月「德國保衛戰」時在[萊茵河西岸](../Page/萊茵河.md "wikilink")[芮斯華森林之戰抵抗](../Page/芮斯華森林之戰.md "wikilink")**加拿大第1軍**進攻，卻在橫渡[萊茵河向](../Page/萊茵河.md "wikilink")[盟軍和談時](../Page/盟軍.md "wikilink")，在德軍艦艇上被[英軍](../Page/英軍.md "wikilink")[戰鬥機射傷退役](../Page/戰鬥機.md "wikilink")；施勒姆負傷透露出訊息，表示英國消滅所有尋找[艾森豪談判的納粹代表](../Page/艾森豪.md "wikilink")，拒絕給納粹「有條件投降」的決心，對戰後[冷戰局面形成影響巨大且深遠](../Page/冷戰.md "wikilink")。

## 早年

施勒姆出生在[Rudolstadt](../Page/Rudolstadt.md "wikilink")[Schwarzburg-Rudolstadt](../Page/Schwarzburg-Rudolstadt.md "wikilink")，在1913年3月加入德軍，單位是**德軍第2野戰炮兵團**擔任Fahnenjunker職務Nr.56
，在1913年10月到「[但澤戰爭學校](../Page/但澤.md "wikilink")」學習，[第一次世界大戰開始前他返回炮兵團服役](../Page/第一次世界大戰.md "wikilink")，直到1919年9月這段期間他擔任過許多職（包括排長、Ordonnanzoffizier、炮台司令和副團長)。

在納粹德國參與[西班牙內戰時](../Page/西班牙內戰.md "wikilink")，當過許多職務，包括各級參謀和訓練官，直到1937年10月春任德軍「空軍部」部長助理，1938年2月晉升為[上校](../Page/上校.md "wikilink")，6月起在德國空軍司令部任軍事參謀官，後又晉升為「西戰場德國空軍參謀長」。

## 第二次世界大戰

1939年10月成為德軍「Luftgau第11空戰區參謀長」，歸[中將](../Page/中將.md "wikilink")[Ludwig
Wolff轄管](../Page/Ludwig_Wolff.md "wikilink")，1940年12月擔任德國空軍**第11飛航軍參謀長**，上司是夙有「德軍第一位傘兵」讚譽的[少將軍長](../Page/少將.md "wikilink")[克特·司徒登](../Page/克特·司徒登.md "wikilink")，**第11飛航軍**是德軍[傘兵與空降部隊大本營](../Page/傘兵.md "wikilink")，1941年5月20日[克里特島戰役](../Page/克里特島戰役.md "wikilink")，他謀略這場戰役確實也攻佔此島，德軍卻也付出戰歿失蹤德軍6,000人代價，導致希特勒從此禁止德軍大部隊空降作戰。

### 德蘇戰爭

1942年2月起隸屬於**德軍第8飛航軍**
，軍長是[沃尔弗拉姆·冯·里希特霍芬大將](../Page/沃尔弗拉姆·冯·里希特霍芬.md "wikilink")，參與[巴巴羅薩作戰](../Page/巴巴羅薩作戰.md "wikilink")，此時他是德軍Luftwaffen-Gefechtsverbande（"戰時編制"）司令，空軍師支援德軍陸軍軍團司令[中將](../Page/中將.md "wikilink")[哥特哈德·海因里希指揮](../Page/哥特哈德·海因里希.md "wikilink")**德軍第4軍團**作戰，單位包括：**德軍第40坦克軍**與**德軍LVI坦克軍**。1942年6月起晉升為**德軍第1飛航師**師長，基地飛機場在[Dugino](../Page/Dugino.md "wikilink")，負責空軍多重作戰任務並支援陸軍。

1942年10月晉升為「德蘇戰爭」**德軍第2空降軍團**軍長，之後領軍轉戰義大利戰場也是這支部隊，但此時他在蘇聯戰鬥空降軍團包括4個**空軍師**並需防守[Nevel南邊到](../Page/Nevel.md "wikilink")[白俄](../Page/白俄.md "wikilink")[道加瓦河東邊的](../Page/道加瓦河.md "wikilink")[維捷布斯克](../Page/維捷布斯克.md "wikilink")，這也是德軍侵蘇**中央方面防線集團軍群**底轄**第3裝甲軍團**空中優勢由施勒姆空軍軍支援地面德軍作戰。

1943年2月到3月**德軍第2空降軍團**參與戰鬥[Operation
Kugelblitz攻擊盟軍波蘭軍隊出沒的](../Page/Operation_Kugelblitz.md "wikilink")[Surazh
Rayon東北方位](../Page/Surazh_Rayon.md "wikilink")[維捷布斯克](../Page/維捷布斯克.md "wikilink")，1943年10月6日在一次蘇軍猛攻下，**德軍第2空降軍團**遭到重創，導致德軍防線出口10英哩缺口與Nevel德軍失守，
**德軍第2空降軍團**防線縮回至[Gorodok的西邊](../Page/Gorodok.md "wikilink")。

### 義大利攻防戰

1943年11月率軍退出俄國防線，他的**4個空軍師**編配給**德軍陸軍LIII與IX Army
Corps軍團**並都移防至義大利，1944年1月1日施勒姆改率**德軍第1空降軍團**，有24,000後備軍人兵力派守[羅馬地區](../Page/羅馬.md "wikilink")，1944年1月1日率**德軍第1空降軍團**離開羅馬速派至增援[冬季防線](../Page/冬季防線.md "wikilink")（也就是古斯塔夫防線）沿著[加利格里阿諾河佈防](../Page/加利格里阿諾河.md "wikilink")，沒多久又被令派至[安濟奧灘頭堡去滅勦](../Page/安濟奧.md "wikilink")[盟軍](../Page/盟軍.md "wikilink")（[掛招牌軍事行動](../Page/掛招牌軍事行動.md "wikilink")），僅以3天急行軍快速支援到前線**德軍第14軍團**，司令官是[上將](../Page/上將.md "wikilink")[埃貝哈德·馮·馬肯森](../Page/埃貝哈德·馮·馬肯森.md "wikilink")，**德軍第1空降軍團**平日作戰訓練有素，在安濟奧大戰[盟軍](../Page/盟軍.md "wikilink")3個月，導致[盟軍傷亡慘重](../Page/盟軍.md "wikilink")，施勒姆因此戰功榮膺[騎士鐵十字勳章](../Page/鐵十字勳章.md "wikilink")。

古斯塔夫防線被盟軍在攻佔[卡西諾後防線瓦解](../Page/卡西諾.md "wikilink")，盟軍在安濟奧灘頭堡被困解圍，
施勒姆率**德軍第1空降軍團**與其他德軍撤出義大利中部；8月德軍在北[亞平寧山脈佈署安諾河與歌德防線](../Page/亞平寧山脈.md "wikilink")；施勒姆把**德軍第1空降軍團**職權改交給[中將Richard](../Page/中將.md "wikilink")
Heidrich。

### 芮斯華森林之戰

成為**德軍第3空降軍**軍長但是這預設成立軍並未實際作戰，取而代之是調派接任[克特·司徒登](../Page/克特·司徒登.md "wikilink")[上將職位前去接掌](../Page/上將.md "wikilink")[西方戰線駐](../Page/西方戰線.md "wikilink")[荷蘭](../Page/荷蘭.md "wikilink")**第1空降軍團**司令官，此時**第1空降軍團**正因為在[芮斯華森林與](../Page/芮斯華森林.md "wikilink")**加拿大第1軍團**大戰中；施勒姆的軍團收編自各個失散的**德軍步兵師**形成雜牌軍但是作戰並不含糊，固守德軍[西方戰線](../Page/西方戰線.md "wikilink")[齊格菲防線數次擊退](../Page/齊格菲防線.md "wikilink")[盟軍攻勢](../Page/盟軍.md "wikilink")，由於施勒姆軍團的勝戰，阻擋延遲了盟軍會由德國南部發動再次攻勢（因為盟軍無法往南會師）的當時觀點，並令各方相信這批德軍建立了一道恐怖的強悍防禦工事。

**加拿大第1軍團**司令[中將](../Page/中將.md "wikilink")[威廉·胡得·辛普森率領](../Page/威廉·胡得·辛普森.md "wikilink")**美軍第9軍團**最後還是把**德軍第1空降軍團**圍困在[萊茵河西岸對岸](../Page/萊茵河.md "wikilink")[威塞爾](../Page/威塞爾.md "wikilink")，1945年3月10日**德軍第1空降軍團**後衛終於撤守轉退，撤退前把橋炸斷，3月21日被英軍空襲炸傷（另說是在[哈爾滕新司令部](../Page/哈爾滕.md "wikilink")），**德軍第1空降軍團**立刻改由[上將](../Page/上將.md "wikilink")[袞特·布魯門特里特接掌](../Page/袞特·布魯門特里特.md "wikilink")。

## 戰爭後

戰爭結束後他被列為「戰犯」關押在[英國](../Page/英國.md "wikilink")，1947年10月釋放回德國[漢堡市](../Page/漢堡市.md "wikilink")，1986年逝於[阿登接近](../Page/阿登.md "wikilink")[漢諾威市](../Page/漢諾威.md "wikilink")。

## 參考聯結

  - [Prisoner of War
    biography](http://www.specialcamp11.fsnet.co.uk/General%20der%20Fallschirmtruppe%20Alfred%20Schlemm%20\(Luftwaffe\).htm)

[Category:納粹德國人物](../Category/納粹德國人物.md "wikilink")
[Category:德國第二次世界大戰人物](../Category/德國第二次世界大戰人物.md "wikilink")
[Category:德國空軍人物](../Category/德國空軍人物.md "wikilink")
[Category:圖林根人](../Category/圖林根人.md "wikilink")