[Rapsfeld_2007.jpg](https://zh.wikipedia.org/wiki/File:Rapsfeld_2007.jpg "fig:Rapsfeld_2007.jpg")
**十字花科**（[學名](../Page/學名.md "wikilink")：，中文名譯自舊稱）为[真双子叶植物](../Page/真双子叶植物.md "wikilink")[十字花目的一科](../Page/十字花目.md "wikilink")，是[植物中最繁盛的](../Page/植物.md "wikilink")[科之一](../Page/科_\(生物\).md "wikilink")。有许多种[人类食用的](../Page/人类.md "wikilink")[蔬菜出自本科](../Page/蔬菜.md "wikilink")，大约共有338個[属](../Page/属.md "wikilink")，约3700[种](../Page/种.md "wikilink")，原产自[北半球](../Page/北半球.md "wikilink")[温带地区](../Page/温带.md "wikilink")，现已被引种到世界各地。[中国原产](../Page/中国.md "wikilink")85属约360餘种，另引种7属20餘种。由於十字花科的[種](../Page/種.md "wikilink")[屬繁多](../Page/屬.md "wikilink")，現除了大中華地區原產的[品種以外](../Page/品種.md "wikilink")，其他的種屬有不少到現在還未有[中文譯名](../Page/中文.md "wikilink")，包括不少新擬定的種屬。

## 特征

  - 主要特征
      - [茎和](../Page/茎.md "wikilink")[叶](../Page/叶.md "wikilink")：草本，常具辛辣味。单叶，基生叶常呈莲座状，茎生叶互生，无托叶。
      - [花](../Page/花.md "wikilink")：\*K<sub>2+2</sub>C<sub>2+2</sub>A<sub>2+4</sub><u>G</u><sub>(2:1)</sub>。[总状花序](../Page/花序.md "wikilink")。十字花冠，四强雄蕊，[蜜腺生于](../Page/蜜腺.md "wikilink")[花萼上](../Page/花萼.md "wikilink")，常与[萼片对生](../Page/萼片.md "wikilink")。[子房](../Page/子房.md "wikilink")1室，被假隔膜分为假2室，横切-{面}-上有2个侧膜胎座。
      - [果实和](../Page/果实.md "wikilink")[种子](../Page/种子.md "wikilink")：[角果](../Page/角果.md "wikilink")（长角果和短角果）。种子无[胚乳](../Page/胚乳.md "wikilink")，胚弯曲。

<!-- end list -->

  - 代表植物
      - [油菜](../Page/油菜.md "wikilink")（*Brassica campestris L.*）
      - [荠菜](../Page/荠菜.md "wikilink")（*Capsella bursa - pastoris (L.)
        Medic.*）
      - [蘿蔔](../Page/蘿蔔.md "wikilink")（*Raphanus sativus*）
      - [西洋菜](../Page/西洋菜.md "wikilink")
      - [花椰菜](../Page/花椰菜.md "wikilink")
      - [捲心菜](../Page/捲心菜.md "wikilink")/包心菜

## 分類

以下各屬依其拉丁名字順序排列：

### A

  - *[Acanthocardamum](../Page/Acanthocardamum.md "wikilink")*
  - *[Aethionema](../Page/Aethionema.md "wikilink")*
  - *[Agallis](../Page/Agallis.md "wikilink")*
  - [蔥芥屬](../Page/蔥芥屬.md "wikilink")（*Alliaria*）
  - *[Alyssoides](../Page/Alyssoides.md "wikilink")*
  - *[Alysopsis](../Page/Alysopsis.md "wikilink")*
  - [庭薺屬](../Page/庭薺屬.md "wikilink")（*Alyssum*）
  - *[Ammosperma](../Page/Ammosperma.md "wikilink")*
  - [含生草属](../Page/含生草属.md "wikilink")（*Anastatica*）
  - *[Anchonium](../Page/Anchonium.md "wikilink")*
  - *[Andrzeiowskia](../Page/Andrzeiowskia.md "wikilink")*
  - *[Anelsonia](../Page/Anelsonia.md "wikilink")*
  - [寒原薺屬](../Page/寒原薺屬.md "wikilink")（*Aphragmus*）
  - *[Aplanodes](../Page/Aplanodes.md "wikilink")*
  - *[Arabidella](../Page/Arabidella.md "wikilink")*
  - [鼠耳芥屬](../Page/鼠耳芥屬.md "wikilink")（*Arabidopsis*）
  - [南芥属](../Page/南芥属.md "wikilink")（*Arabis*）
  - *[Arcyosperma](../Page/Arcyosperma.md "wikilink")*
  - [辣根屬](../Page/辣根屬.md "wikilink")（*Armoracia*）
  - *[Aschersoniodoxa](../Page/Aschersoniodoxa.md "wikilink")*
  - *[Asperuginoides](../Page/Asperuginoides.md "wikilink")*
  - *[Asta](../Page/Asta_\(genus\).md "wikilink")*
  - [异药芥属](../Page/异药芥属.md "wikilink")（*Atelanthera*）
  - *[Athysanus](../Page/Athysanus.md "wikilink")*
  - *[Aubrieta](../Page/Aubrieta.md "wikilink")*
  - *[Aurinia](../Page/Aurinia.md "wikilink")*

### B

  - *[Ballantinia](../Page/Ballantinia.md "wikilink")*
  - [山芥属](../Page/山芥属.md "wikilink")（*Barbarea*）
  - *[Beringia](../Page/Beringia_\(plant\).md "wikilink")*
  - [团扇荠属](../Page/团扇荠属.md "wikilink")（*Berteroa*）
  - [锥果芥属](../Page/锥果芥属.md "wikilink")（*Berteroella*）
  - *[Biscutella](../Page/Biscutella.md "wikilink")*
  - *[Bivonaea](../Page/Bivonaea.md "wikilink")*
  - *[Blennodia](../Page/Blennodia.md "wikilink")*
  - *[Boleum](../Page/Boleum.md "wikilink")*
  - *[Boreava](../Page/Boreava.md "wikilink")*
  - *[Bornmuellera](../Page/Bornmuellera.md "wikilink")*
  - *[Borodinia](../Page/Borodinia.md "wikilink")*
  - *[Botscantzevia](../Page/Botscantzevia.md "wikilink")*
  - *[Brachycarpaea](../Page/Brachycarpaea.md "wikilink")*
  - [芸苔屬](../Page/芸苔屬.md "wikilink")（*Brassica*）
  - [柏蕾荠属](../Page/柏蕾荠属.md "wikilink")（*Braya*）
  - *[Brayopsis](../Page/Brayopsis.md "wikilink")*
  - *[Brossardia](../Page/Brossardia.md "wikilink")*
  - [匙荠属](../Page/匙荠属.md "wikilink")（*Bunias*）

### C

  - *[Cakile](../Page/Cakile.md "wikilink")*
  - *[Calepina](../Page/Calepina.md "wikilink")*
  - *[Calymmatium](../Page/Calymmatium.md "wikilink")*
  - [亞麻薺屬](../Page/亞麻薺屬.md "wikilink")（*Camelina*） -
  - *[Camelinopsis](../Page/Camelinopsis.md "wikilink")*
  - [薺菜屬](../Page/薺菜屬.md "wikilink")（*Capsella*） -
  - [碎米荠属](../Page/碎米荠属.md "wikilink")（*Cardamine*）
  - *[Cardaminopsis](../Page/Cardaminopsis.md "wikilink")*
  - *[Cardaria](../Page/Cardaria.md "wikilink")*
  - *[Carinavalva](../Page/Carinavalva.md "wikilink")*
  - *[Carrichtera](../Page/Carrichtera.md "wikilink")*
  - *[Catadysia](../Page/Catadysia.md "wikilink")*
  - *[Catenulina](../Page/Catenulina.md "wikilink")*
  - *[Caulanthus](../Page/Caulanthus.md "wikilink")*
  - *[Caulostramina](../Page/Caulostramina.md "wikilink")*
  - *[Ceratocnemum](../Page/Ceratocnemum.md "wikilink")*
  - *[Ceriosperma](../Page/Ceriosperma.md "wikilink")*
  - *[Chalcanthus](../Page/Chalcanthus.md "wikilink")*
  - *[Chamira](../Page/Chamira.md "wikilink")*
  - *[Chartoloma](../Page/Chartoloma.md "wikilink")*
  - *[Cheesemania](../Page/Cheesemania.md "wikilink")*
  - *[Cheiranthus](../Page/Cheiranthus.md "wikilink")*
  - *[Chlorocrambe](../Page/Chlorocrambe.md "wikilink")*
  - [離子芥屬](../Page/離子芥屬.md "wikilink")（*Chorispora*）
  - [高原芥屬](../Page/高原芥屬.md "wikilink")（*Christolea*）
  - *[Chrysobraya](../Page/Chrysobraya.md "wikilink")*
  - *[Chrysochamela](../Page/Chrysochamela.md "wikilink")*
  - *[Cithareloma](../Page/Cithareloma.md "wikilink")*
  - *[Clastopus](../Page/Clastopus.md "wikilink")*
  - [香芥屬](../Page/香芥屬.md "wikilink")（*Clausia*） -
  - *[Clypeola](../Page/Clypeola.md "wikilink")*
  - *[Cochlearia](../Page/Cochlearia.md "wikilink")*
  - [穴絲薺屬](../Page/穴絲薺屬.md "wikilink")（*Coelonema*）
  - *[Coincya](../Page/Coincya.md "wikilink")*
  - *[Coluteocarpus](../Page/Coluteocarpus.md "wikilink")*
  - [線果芥屬](../Page/線果芥屬.md "wikilink")（*Conringia*）
  - *[Cordylocarpus](../Page/Cordylocarpus.md "wikilink")*
  - [腎果薺屬](../Page/腎果薺屬.md "wikilink")（*Coronopus*）
  - [兩節薺屬](../Page/兩節薺屬.md "wikilink")（*Crambe*）
  - *[Crambella](../Page/Crambella.md "wikilink")*
  - *[Cremolobus](../Page/Cremolobus.md "wikilink")*
  - *[Crucihimalaya](../Page/Crucihimalaya.md "wikilink")*
  - [隱子芥屬](../Page/隱子芥屬.md "wikilink")（*Cryptospora*）
  - *[Cuphonotus](../Page/Cuphonotus.md "wikilink")*
  - *[Cusickiella](../Page/Cusickiella.md "wikilink")*
  - *[Cycloptychis](../Page/Cycloptychis.md "wikilink")*
  - *[Cymatocarpus](../Page/Cymatocarpus.md "wikilink")*
  - *[Cyphocardamum](../Page/Cyphocardamum.md "wikilink")*

### D

  - *[Dactylocardamum](../Page/Dactylocardamum.md "wikilink")*
  - *[Degenia](../Page/Degenia.md "wikilink")*
  - *[Delpinophytum](../Page/Delpinophytum.md "wikilink")*
  - *[Descurainia](../Page/Descurainia.md "wikilink")*
  - *[Diceratella](../Page/Diceratella.md "wikilink")*
  - *[Dichasianthus](../Page/Dichasianthus.md "wikilink")*
  - *[Dictyophragmus](../Page/Dictyophragmus.md "wikilink")*
  - *[Didesmus](../Page/Didesmus.md "wikilink")*
  - *[Didymophysa](../Page/Didymophysa.md "wikilink")*
  - *[Dielsiocharis](../Page/Dielsiocharis.md "wikilink")*
  - *[Dilophia](../Page/Dilophia.md "wikilink")*
  - *[Dimorphocarpa](../Page/Dimorphocarpa.md "wikilink")*
  - *[Diplotaxis](../Page/Diplotaxis.md "wikilink")*
  - *[Dipoma](../Page/Dipoma.md "wikilink")*
  - *[Diptychocarpus](../Page/Diptychocarpus.md "wikilink")*
  - *[Dithyrea](../Page/Dithyrea.md "wikilink")*
  - *[Dolichirhynchus](../Page/Dolichirhynchus.md "wikilink")*
  - *[Dontostemon](../Page/Dontostemon.md "wikilink")*
  - *[Douepea](../Page/Douepea.md "wikilink")*
  - [葶藶屬](../Page/葶藶屬.md "wikilink")（*Draba*）
  - *[Drabastrum](../Page/Drabastrum.md "wikilink")*
  - *[Drabopsis](../Page/Drabopsis.md "wikilink")*
  - *[Dryopetalon](../Page/Dryopetalon.md "wikilink")*

### E

  - *[Eigia](../Page/Eigia.md "wikilink")*
  - *[Elburzia](../Page/Elburzia.md "wikilink")*
  - *[Enarthrocarpus](../Page/Enarthrocarpus.md "wikilink")*
  - *[Englerocharis](../Page/Englerocharis.md "wikilink")*
  - *[Eremobium](../Page/Eremobium.md "wikilink")*
  - *[Eremoblastus](../Page/Eremoblastus.md "wikilink")*
  - *[Eremodraba](../Page/Eremodraba.md "wikilink")*
  - *[Eremophyton](../Page/Eremophyton.md "wikilink")*
  - *[Ermania](../Page/Ermania.md "wikilink")*
  - *[Ermaniopsis](../Page/Ermaniopsis.md "wikilink")*
  - *[Erophila](../Page/Erophila.md "wikilink")*
  - *[Eruca](../Page/Eruca.md "wikilink")*
  - *[Erucaria](../Page/Erucaria.md "wikilink")*
  - *[Erucastrum](../Page/Erucastrum.md "wikilink")*
  - *[Erysimum](../Page/Erysimum.md "wikilink")*
  - *[Euclidium](../Page/Euclidium.md "wikilink")*
  - *[Eudema](../Page/Eudema.md "wikilink")*
  - *[Eutrema](../Page/Eutrema.md "wikilink")*
  - *[Euzomodendron](../Page/Euzomodendron.md "wikilink")*

### F

  - *[Farsetia](../Page/Farsetia.md "wikilink")*
  - *[Fezia](../Page/Fezia.md "wikilink")*
  - *[Fibigia](../Page/Fibigia.md "wikilink")*
  - *[Foleyola](../Page/Foleyola.md "wikilink")*
  - *[Fortuynia](../Page/Fortuynia.md "wikilink")*

### G

  - *[Galitzkya](../Page/Galitzkya.md "wikilink")*
  - *[Geococcus](../Page/Geococcus.md "wikilink")*
  - *[Glaribraya](../Page/Glaribraya.md "wikilink")*
  - *[Glastaria](../Page/Glastaria.md "wikilink")*
  - *[Glaucocarpum](../Page/Glaucocarpum.md "wikilink")*
  - *[Goldbachia](../Page/Goldbachia.md "wikilink")*
  - *[Gorodkovia](../Page/Gorodkovia.md "wikilink")*
  - *[Graellsia](../Page/Graellsia.md "wikilink")*
  - *[Grammosperma](../Page/Grammosperma.md "wikilink")*
  - *[Guillenia](../Page/Guillenia.md "wikilink")*
  - *[Guiraoa](../Page/Guiraoa.md "wikilink")*
  - *[Gynophorea](../Page/Gynophorea.md "wikilink")*

### H

  - *[Halimolobos](../Page/Halimolobos.md "wikilink")*
  - *[Harmsiodoxa](../Page/Harmsiodoxa.md "wikilink")*
  - *[Hedinia](../Page/Hedinia.md "wikilink")*
  - *[Heldreichia](../Page/Heldreichia.md "wikilink")*
  - *[Heliophila](../Page/Heliophila.md "wikilink")*
  - *[Hemicrambe](../Page/Hemicrambe.md "wikilink")*
  - *[Hemilophia](../Page/Hemilophia.md "wikilink")*
  - *[Hesperis](../Page/Hesperis.md "wikilink")*
  - *[Heterodraba](../Page/Heterodraba.md "wikilink")*
  - *[Hirschfeldia](../Page/Hirschfeldia.md "wikilink")*
  - *[Hollermayera](../Page/Hollermayera.md "wikilink")*
  - *[Hormathophylla](../Page/Hormathophylla.md "wikilink")*
  - *[Hornungia](../Page/Hornungia.md "wikilink")*
  - *[Hornwoodia](../Page/Hornwoodia.md "wikilink")*
  - *[Hugueninia](../Page/Hugueninia.md "wikilink")*
  - *[Hymenolobus](../Page/Hymenolobus.md "wikilink")*

### I

  - *[Ianhedgea](../Page/Ianhedgea.md "wikilink")*
  - *[Iberis](../Page/Iberis.md "wikilink")*
  - *[Idahoa](../Page/Idahoa.md "wikilink")*
  - *[Iodanthus](../Page/Iodanthus.md "wikilink")*
  - *[Ionopsidium](../Page/Ionopsidium.md "wikilink")*
  - *[Irenepharsus](../Page/Irenepharsus.md "wikilink")*
  - *[Isatis](../Page/Isatis.md "wikilink")*
  - *[Ischnocarpus](../Page/Ischnocarpus.md "wikilink")*
  - *[Iskandera](../Page/Iskandera.md "wikilink")*

[湖生芥屬](../Page/湖生芥屬.md "wikilink")（*Iti*） -
[依瓦芥屬](../Page/依瓦芥屬.md "wikilink")（*Ivania*） -

### K

  - *[Kernera](../Page/Kernera.md "wikilink")*
  - *[Kremeriella](../Page/Kremeriella.md "wikilink")*

### L

  - *[Lachnocapsa](../Page/Lachnocapsa.md "wikilink")*
  - [綿果薺屬](../Page/綿果薺屬.md "wikilink")（*Lachnoloma*）
  - *[Leavenworthia](../Page/Leavenworthia.md "wikilink")*
  - [獨行菜屬](../Page/獨行菜屬.md "wikilink")（*Lepidium*）
  - *[Lepidostemon](../Page/Lepidostemon.md "wikilink")*
  - [絲葉芥屬](../Page/絲葉芥屬.md "wikilink")（*Leptaleum*）
  - *[Lesquerella](../Page/Lesquerella.md "wikilink")*
  - [彎梗芥屬](../Page/彎梗芥屬.md "wikilink")（*Lignariella*）
  - *[Lithodraba](../Page/Lithodraba.md "wikilink")*
  - [香雪球屬](../Page/香雪球屬.md "wikilink")（*Lobularia*）
  - *[Lonchophora](../Page/Lonchophora.md "wikilink")*
  - [彎蕊芥屬](../Page/彎蕊芥屬.md "wikilink")（*Loxostemon*）
  - *[Lunaria](../Page/Lunaria.md "wikilink")*
  - *[Lyocarpus](../Page/Lyocarpus.md "wikilink")*
  - *[Lyrocarpa](../Page/Lyrocarpa.md "wikilink")*

### M

  - *[Macropodium](../Page/Macropodium.md "wikilink")*
  - *[Malcolmia](../Page/Malcolmia.md "wikilink")*
  - *[Mancoa](../Page/Mancoa.md "wikilink")*
  - *[Maresia](../Page/Maresia.md "wikilink")*
  - *[Mathewsia](../Page/Mathewsia.md "wikilink")*
  - [紫羅蘭屬](../Page/紫羅蘭屬.md "wikilink")*Matthiola*
  - *[Megacarpaea](../Page/Megacarpaea.md "wikilink")*
  - *[Megadenia](../Page/Megadenia.md "wikilink")*
  - *[Menkea](../Page/Menkea.md "wikilink")*
  - *[Menonvillea](../Page/Menonvillea.md "wikilink")*
  - *[Microlepidium](../Page/Microlepidium.md "wikilink")*
  - *[Microsysymbrium](../Page/Microsysymbrium.md "wikilink")*
  - *[Microstigma](../Page/Microstigma.md "wikilink")*
  - *[Morettia](../Page/Morettia.md "wikilink")*
  - *[Moricandia](../Page/Moricandia.md "wikilink")*
  - *[Moriera](../Page/Moriera.md "wikilink")*
  - *[Morisia](../Page/Morisia.md "wikilink")*
  - *[Murbeckiella](../Page/Murbeckiella.md "wikilink")*
  - *[Muricaria](../Page/Muricaria.md "wikilink")*
  - *[Myagrum](../Page/Myagrum.md "wikilink")*

### N

  - *[Nasturtiopsis](../Page/Nasturtiopsis.md "wikilink")*
  - [豆瓣菜屬](../Page/豆瓣菜屬.md "wikilink")（*Nasturtium*）
  - *[Neomartinella](../Page/Neomartinella.md "wikilink")*
  - *[Neotchihatchewia](../Page/Neotchihatchewia.md "wikilink")*
  - *[Neotorularia](../Page/Neotorularia.md "wikilink")*
  - *[Nerisyrenia](../Page/Nerisyrenia.md "wikilink")*
  - *[Neslia](../Page/Neslia.md "wikilink")*
  - *[Neuontobotrys](../Page/Neuontobotrys.md "wikilink")*
  - *[Notoceras](../Page/Notoceras.md "wikilink")*
  - *[Notothlaspi](../Page/Notothlaspi.md "wikilink")*

### O

  - *[Ochthodium](../Page/Ochthodium.md "wikilink")*
  - *[Octoceras](../Page/Octoceras.md "wikilink")*
  - *[Olimarabidopsis](../Page/Olimarabidopsis.md "wikilink")*
  - *[Onuris](../Page/Onuris_\(plant\).md "wikilink")*
  - *[Oreoloma](../Page/Oreoloma.md "wikilink")*
  - *[Oreophyton](../Page/Oreophyton.md "wikilink")*
  - *[Ornithocarpa](../Page/Ornithocarpa.md "wikilink")*
  - *[Orychophragmus](../Page/Orychophragmus.md "wikilink")*
  - *[Otocarpus](../Page/Otocarpus.md "wikilink")*
  - *[Oudneya](../Page/Oudneya.md "wikilink")*

### P

  - *[Pachycladon](../Page/Pachycladon.md "wikilink")*
  - *[Pachymitus](../Page/Pachymitus.md "wikilink")*
  - *[Pachyphragma](../Page/Pachyphragma.md "wikilink")*
  - *[Pachypterygium](../Page/Pachypterygium.md "wikilink")*
  - *[Parlatoria](../Page/Parlatoria.md "wikilink")*
  - *[Parodiodoxa](../Page/Parodiodoxa.md "wikilink")*
  - *[Parolinia](../Page/Parolinia.md "wikilink")*
  - *[Parrya](../Page/Parrya.md "wikilink")*
  - *[Parryodes](../Page/Parryodes.md "wikilink")*
  - *[Pegaeophyton](../Page/Pegaeophyton.md "wikilink")*
  - *[Peltaria](../Page/Peltaria.md "wikilink")*
  - *[Peltariopsis](../Page/Peltariopsis.md "wikilink")*
  - *[Pennellia](../Page/Pennellia.md "wikilink")*
  - *[Petiniotia](../Page/Petiniotia.md "wikilink")*
  - *[Petrocallis](../Page/Petrocallis.md "wikilink")*
  - *[Phaeonychium](../Page/Phaeonychium.md "wikilink")*
  - *[Phlebolobium](../Page/Phlebolobium.md "wikilink")*
  - *[Phlegmatospermum](../Page/Phlegmatospermum.md "wikilink")*
  - *[Phoenicaulis](../Page/Phoenicaulis.md "wikilink")*
  - *[Physaria](../Page/Physaria.md "wikilink")*
  - *[Physocardamum](../Page/Physocardamum.md "wikilink")*
  - *[Physoptychis](../Page/Physoptychis.md "wikilink")*
  - *[Physorrhynchus](../Page/Physorrhynchus.md "wikilink")*
  - *[Platycraspedum](../Page/Platycraspedum.md "wikilink")*
  - *[Polyctenium](../Page/Polyctenium.md "wikilink")*
  - *[Polypsecadium](../Page/Polypsecadium.md "wikilink")*
  - *[Pringlea](../Page/Pringlea.md "wikilink")*
  - *[Prionotrichon](../Page/Prionotrichon.md "wikilink")*
  - *[Pritzelago](../Page/Pritzelago.md "wikilink")*
  - *[Pseuderucaria](../Page/Pseuderucaria.md "wikilink")*

[假鼠耳芥屬](../Page/假鼠耳芥屬.md "wikilink")（*Pseudoarabidopsis*） -
[假亞麻薺屬](../Page/假亞麻薺屬.md "wikilink")（*Pseudocamelina*） -
[假香芥屬](../Page/假香芥屬.md "wikilink")（*Pseudoclausia*） -

  - *[Pseudofortuynia](../Page/Pseudofortuynia.md "wikilink")*
  - *[Pseudovesicaria](../Page/Pseudovesicaria.md "wikilink")*
  - *[Psychine](../Page/Psychine.md "wikilink")*
  - *[Pterygiosperma](../Page/Pterygiosperma.md "wikilink")*
  - *[Pterygostemon](../Page/Pterygostemon.md "wikilink")*
  - *[Pugionium](../Page/Pugionium.md "wikilink")*
  - *[Pycnoplinthopsis](../Page/Pycnoplinthopsis.md "wikilink")*
  - *[Pycnoplinthus](../Page/Pycnoplinthus.md "wikilink")*
  - *[Pyramidium](../Page/Pyramidium.md "wikilink")*

### Q

  - *[Quezeliantha](../Page/Quezeliantha.md "wikilink")*
  - *[Quidproquo](../Page/Quidproquo.md "wikilink")*

### R

  - *[Raffenaldia](../Page/Raffenaldia.md "wikilink")*
  - *[Raphanorhyncha](../Page/Raphanorhyncha.md "wikilink")*
  - [蘿蔔屬](../Page/蘿蔔屬.md "wikilink")（*Raphanus*）
  - *[Rapistrum](../Page/Rapistrum.md "wikilink")*
  - *[Reboudia](../Page/Reboudia.md "wikilink")*
  - *[Redowskia](../Page/Redowskia.md "wikilink")*
  - *[Rhizobotrya](../Page/Rhizobotrya.md "wikilink")*
  - *[Ricotia](../Page/Ricotia.md "wikilink")*
  - *[Robeschia](../Page/Robeschia.md "wikilink")*
  - *[Rollinsia](../Page/Rollinsia.md "wikilink")*
  - *[Romanschulzia](../Page/Romanschulzia.md "wikilink")*
  - *[Roripella](../Page/Roripella.md "wikilink")*
  - [蔊菜屬](../Page/蔊菜屬.md "wikilink")（*Rorippa*）
  - *[Rytidocarpus](../Page/Rytidocarpus.md "wikilink")*

### S

  - *[Sameraria](../Page/Sameraria.md "wikilink")*
  - *[Sarcodraba](../Page/Sarcodraba.md "wikilink")*
  - *[Savignya](../Page/Savignya.md "wikilink")*
  - *[Scambopus](../Page/Scambopus.md "wikilink")*
  - *[Schimpera](../Page/Schimpera.md "wikilink")*
  - *[Schivereckia](../Page/Schivereckia.md "wikilink")*
  - *[Schizopetalon](../Page/Schizopetalon.md "wikilink")*
  - *[Schlechteria](../Page/Schlechteria.md "wikilink")*
  - *[Schoenocrambe](../Page/Schoenocrambe.md "wikilink")*
  - *[Schouwia](../Page/Schouwia.md "wikilink")*
  - *[Scoliaxon](../Page/Scoliaxon.md "wikilink")*
  - *[Selenia](../Page/Selenia.md "wikilink")*
  - *[Sibara](../Page/Sibara.md "wikilink")*
  - *[Silicularia](../Page/Silicularia.md "wikilink")*
  - *[Sinapidendron](../Page/Sinapidendron.md "wikilink")*
  - [白芥属](../Page/白芥属.md "wikilink")（**）
  - *[Sisymbrella](../Page/Sisymbrella.md "wikilink")*
  - *[Sisymbriopsis](../Page/Sisymbriopsis.md "wikilink")*
  - *[Sisymbrium](../Page/Sisymbrium.md "wikilink")*
  - *[Smelowskia](../Page/Smelowskia.md "wikilink")*
  - *[Sobolewslia](../Page/Sobolewslia.md "wikilink")*
  - *[Sohms-Laubachia](../Page/Sohms-Laubachia.md "wikilink")*
  - *[Sophiopsis](../Page/Sophiopsis.md "wikilink")*
  - *[Sphaerocardamum](../Page/Sphaerocardamum.md "wikilink")*
  - *[Spirorhynchus](../Page/Spirorhynchus.md "wikilink")*
  - *[Spryginia](../Page/Spryginia.md "wikilink")*
  - *[Staintoniella](../Page/Staintoniella.md "wikilink")*
  - *[Stanfordia](../Page/Stanfordia.md "wikilink")*
  - *[Stanleya](../Page/Stanleya.md "wikilink")*
  - *[Stenopetalum](../Page/Stenopetalum.md "wikilink")*
  - *[Sterigmostemum](../Page/Sterigmostemum.md "wikilink")*
  - *[Stevenia](../Page/Stevenia.md "wikilink")*
  - *[Straussiella](../Page/Straussiella.md "wikilink")*
  - *[Streptanthella](../Page/Streptanthella.md "wikilink")*
  - *[Streptanthus](../Page/Streptanthus.md "wikilink")*
  - *[Streptoloma](../Page/Streptoloma.md "wikilink")*
  - *[Stroganowia](../Page/Stroganowia.md "wikilink")*
  - *[Stubebdorffia](../Page/Stubebdorffia.md "wikilink")*
  - *[Subularia](../Page/Subularia.md "wikilink")*
  - *[Succowia](../Page/Succowia.md "wikilink")*
  - *[Synstemon](../Page/Synstemon.md "wikilink")*
  - *[Synthlipsis](../Page/Synthlipsis.md "wikilink")*

### T

  - *[Taphrospermum](../Page/Taphrospermum.md "wikilink")*
  - *[Tauscheria](../Page/Tauscheria.md "wikilink")*
  - *[Teesdalia](../Page/Teesdalia.md "wikilink")*
  - *[Teesdaliopsis](../Page/Teesdaliopsis.md "wikilink")*
  - *[Tetracme](../Page/Tetracme.md "wikilink")*
  - *[Thelypodiopsis](../Page/Thelypodiopsis.md "wikilink")*
  - *[Thelypodium](../Page/Thelypodium.md "wikilink")*
  - *[Thlaspeocarpa](../Page/Thlaspeocarpa.md "wikilink")*
  - *[Thlaspi](../Page/Thlaspi.md "wikilink")*
  - *[Thysanocarpus](../Page/Thysanocarpus.md "wikilink")*
  - *[Trachystoma](../Page/Trachystoma.md "wikilink")*
  - *[Trichotolinum](../Page/Trichotolinum.md "wikilink")*
  - *[Trochiscus](../Page/Trochiscus.md "wikilink")*
  - *[Tropidocarpum](../Page/Tropidocarpum.md "wikilink")*
  - *[Turritis](../Page/Turritis.md "wikilink")*

### V

  - *[Vella](../Page/Vella.md "wikilink")*

### W

  - *[Warea](../Page/Warea.md "wikilink")*
  - *[Wasabia](../Page/Wasabia.md "wikilink")*
  - *[Weberbauera](../Page/Weberbauera.md "wikilink")*
  - *[Werdermannia](../Page/Werdermannia.md "wikilink")*
  - *[Winklera](../Page/Winklera.md "wikilink")*

### X

  - *[Xerodraba](../Page/Xerodraba.md "wikilink")*

### Y

  - [陰山薺屬](../Page/陰山薺屬.md "wikilink")（*Yinshania*）

### Z

  - *[Zerdana](../Page/Zerdana.md "wikilink")*
  - *[Zilla](../Page/Zilla_\(genus\).md "wikilink")*

## 外部链接

  - [Brassicaceae](http://www.mobot.org/MOBOT/Research/APWeb/orders/brassicalesweb.htm#Brassicoideae)
    in Stevens, P. F. (2001 onwards). [Angiosperm Phylogeny
    Website](http://www.mobot.org/MOBOT/Research/APWeb/) Version 7, May
    2006 \[and more or less continuously updated since\].
  - [Brassicaceae](http://delta-intkey.com/angio/www/crucifer.htm) in
    [L. Watson and M.J. Dallwitz (1992 onwards). The families of
    flowering plants: descriptions, illustrations, identification,
    information
    retrieval.](https://web.archive.org/web/20070103200438/http://delta-intkey.com/angio/)
    <http://delta-intkey.com>
  - [Brassicaceae at
    www.botany.hawaii.edu](http://www.botany.hawaii.edu/faculty/carr/brassic.htm)
  - [十字花科](http://libproject.hkbu.edu.hk/was40/outline?lang=ch&page=2&channelid=1288&searchword=%28%E5%8D%81%E5%AD%97%E8%8A%B1%E7%A7%91%29&extension=all&ispage=yes&trslc=50332297.1324129295.1)
    藥用植物圖像數據庫 (香港浸會大學中醫藥學院)

[\*](../Category/十字花科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")