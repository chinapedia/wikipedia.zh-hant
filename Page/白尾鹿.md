**白尾鹿**（[學名](../Page/學名.md "wikilink")：Odocoileus virginianus，）是鹿的一种。

## 主要特征

白尾鹿是北美洲最小的鹿种，肩高为1米，体重40-130公斤。
[Hunter_with_buck.jpg](https://zh.wikipedia.org/wiki/File:Hunter_with_buck.jpg "fig:Hunter_with_buck.jpg")

## 分布

白尾鹿是世界上分布最广的鹿之一，从[加拿大东部森林](../Page/加拿大.md "wikilink")，到[美国东部森林](../Page/美国.md "wikilink")、[佛罗里达半岛](../Page/佛罗里达半岛.md "wikilink")、[墨西哥](../Page/墨西哥.md "wikilink")、[中美洲国家](../Page/中美洲.md "wikilink")，到[秘鲁北部](../Page/秘鲁.md "wikilink")。

## 亚种

### 北美洲的白尾鹿

  - [北森林白尾鹿](../Page/北森林白尾鹿.md "wikilink")（*O. virginianus borealis*）
  - [礁鹿](../Page/礁鹿.md "wikilink")（*O. virginianus clavium*）
  - [卡门山白尾鹿](../Page/卡门山白尾鹿.md "wikilink")（*O. virginianus carminis*）
  - [亚利桑那白尾鹿](../Page/亚利桑那白尾鹿.md "wikilink")（*O. virginianus couesi*）
  - [达科达白尾鹿](../Page/达科达白尾鹿.md "wikilink")（*O. virginianus dakotensis*）
  - [希氏白尾鹿](../Page/希氏白尾鹿.md "wikilink")（*O. virginianus hiltonensis*）
  - [哥伦比亚白尾鹿](../Page/哥伦比亚白尾鹿.md "wikilink")（*O. virginianus leucurus*）
  - [堪萨斯白尾鹿](../Page/堪萨斯白尾鹿.md "wikilink")（*O. virginianus macrourus*）
  - [艾弗里岛白尾鹿](../Page/艾弗里岛白尾鹿.md "wikilink")（*O. virginianus
    mcilhennyi*）
  - [蒂奇岛白尾鹿](../Page/蒂奇岛白尾鹿.md "wikilink")（*O. virginianus nigribarbis*）
  - [西北白尾鹿](../Page/西北白尾鹿.md "wikilink")（*O. virginianus ochrourus*）
  - [海岸白尾鹿](../Page/海岸白尾鹿.md "wikilink")（*O. virginianus osceola*）
  - [佛罗里达白尾鹿](../Page/佛罗里达白尾鹿.md "wikilink")（*O. virginianus seminolus*）
  - [牛岛白尾鹿](../Page/牛岛白尾鹿.md "wikilink")（*O. virginianus taurinsulae*）
  - [德克萨斯白尾鹿](../Page/德克萨斯白尾鹿.md "wikilink")（*O. virginianus texanus*）
  - [猎岛白尾鹿](../Page/猎岛白尾鹿.md "wikilink")（*O. virginianus venatorius*）
  - [弗吉尼亚白尾鹿](../Page/弗吉尼亚白尾鹿.md "wikilink")（*O. virginianus
    virginianus*）

[Odocoileus_viginianus_clavium.jpg](https://zh.wikipedia.org/wiki/File:Odocoileus_viginianus_clavium.jpg "fig:Odocoileus_viginianus_clavium.jpg")在佛罗里达礁鹿保护区的美国红树林。\]\]
[Keydeer2.gif](https://zh.wikipedia.org/wiki/File:Keydeer2.gif "fig:Keydeer2.gif")\]\]

### 中美洲和南美洲的白尾鹿

  - [南墨西哥白尾鹿](../Page/南墨西哥白尾鹿.md "wikilink")（*O. virginianus
    acapulcensis*）
  - [圭亚那白尾鹿](../Page/圭亚那白尾鹿.md "wikilink")（*O. virginianus cariacou*）
  - [巴拿马白尾鹿](../Page/巴拿马白尾鹿.md "wikilink")（*O. virginianus
    chiriquensis*）
  - [库腊索白尾鹿](../Page/库腊索白尾鹿.md "wikilink")（*O. virginianus
    curassavicus*）
  - [哥伦比亚白尾鹿](../Page/哥伦比亚白尾鹿.md "wikilink")（*O. virginianus goudotii*）
  - [委内瑞拉白尾鹿](../Page/委内瑞拉白尾鹿.md "wikilink")（*O. virginianus gymnotis*）
  - [草原白尾鹿](../Page/草原白尾鹿.md "wikilink")
  - [玛格丽塔白尾鹿](../Page/玛格丽塔白尾鹿.md "wikilink")（*O. virginianus
    margaritae*）
  - [中墨西哥白尾鹿](../Page/中墨西哥白尾鹿.md "wikilink")（*O. virginianus mexicanus*）
  - [密氏白尾鹿](../Page/密氏白尾鹿.md "wikilink")（*O. virginianus
    miquihuanensis*）
  - [奈氏白尾鹿](../Page/奈氏白尾鹿.md "wikilink")（*O. virginianus nelsoni*）
  - [南墨西哥白尾鹿](../Page/南墨西哥白尾鹿.md "wikilink")（*O. virginianus
    oaxacensis*）
  - [秘鲁白尾鹿](../Page/秘鲁白尾鹿.md "wikilink")（*O. virginianus peruvianus*）
  - [科伊瓦白尾鹿](../Page/科伊瓦白尾鹿.md "wikilink")（*O. virginianus rothschildi*）
  - [西墨西哥白尾鹿](../Page/西墨西哥白尾鹿.md "wikilink")（*O. virginianus sinaloae*）
  - [东南白尾鹿](../Page/东南白尾鹿.md "wikilink")（*O. virginianus thomasi*）
  - [托尔白尾鹿](../Page/托尔白尾鹿.md "wikilink")（*O. virginianus toltecus*）
  - [西哥伦比亚白尾鹿](../Page/西哥伦比亚白尾鹿.md "wikilink")（*O. virginianus
    tropicalis*）
  - [尼加拉瓜白尾鹿](../Page/尼加拉瓜白尾鹿.md "wikilink")（*O. virginianus truei*）
  - [厄瓜多尔白尾鹿](../Page/厄瓜多尔白尾鹿.md "wikilink")（*O. virginianus ustus*）
  - [东墨西哥白尾鹿](../Page/东墨西哥白尾鹿.md "wikilink")（*O. virginianus
    veraecrucis*）
  - [尤卡坦白尾鹿](../Page/尤卡坦白尾鹿.md "wikilink")（*O. virginianus
    yucatanensis*）

<File:WhitetailedDeerFawn.jpg> <File:Cerfs> de Virginie.jpg <File:Deer>
running.jpg

[Category:空齒鹿屬](../Category/空齒鹿屬.md "wikilink")