**阪南市**（）是位於[大阪府西南部的城市](../Page/大阪府.md "wikilink")，為大阪府轄下最南端的城市，距離大阪市中心約45公里，但距離相鄰的[和歌山市區僅](../Page/和歌山市.md "wikilink")10公里。

## 歷史

現在的阪南市範圍在令制國時代屬於[和泉國](../Page/和泉國.md "wikilink")[日根郡鳥取鄉](../Page/日根郡.md "wikilink")，在[平安時代](../Page/平安時代.md "wikilink")[京都的](../Page/京都.md "wikilink")[上賀茂神社曾在此設有莊園](../Page/賀茂別雷神社.md "wikilink")「筥作莊」，[河內](../Page/河內國.md "wikilink")[觀心寺也曾設有](../Page/觀心寺.md "wikilink")「鳥取莊」。\[1\]

在[江戶時代大部分區域成為](../Page/江戶時代.md "wikilink")[幕府直屬的領地](../Page/幕府.md "wikilink")，另有少部分區域屬於和；1871年[廢藩置縣後](../Page/廢藩置縣.md "wikilink")，先後被整併至[堺縣](../Page/堺縣.md "wikilink")，但在1881年再度被併入[大阪府](../Page/大阪府.md "wikilink")。1889年實施[町村制](../Page/町村制.md "wikilink")，現在的阪南市轄區在當時分屬、、、4個行政區劃；尾崎村先於1939年改制為尾崎町，後由於1956年與下莊村、西鳥取村合併為，1972年南海町又與東鳥取村改制後的東鳥取町合併為**阪南町**，阪南町最後在1991年升格為現在的**阪南市**\[2\]，也是目前為止大阪府轄下最後一個成立的市級行政區劃。

1980年代，大阪府配合關西國際空港的新建，在大阪府南部規劃了多個新市鎮，在阪南市境內也規劃了面積約170公頃的。

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1889年4月1日</p></th>
<th><p>1889年 - 1926年</p></th>
<th><p>1926年 - 1944年</p></th>
<th><p>1945年 - 1954年</p></th>
<th><p>1955年 - 1988年</p></th>
<th><p>1989年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>尾崎村</p></td>
<td><p>1939年6月1日<br />
尾崎町</p></td>
<td><p>1956年9月30日<br />
合併為南海町</p></td>
<td><p>1972年10月20日<br />
合併為阪南町</p></td>
<td><p>1991年10月1日<br />
阪南市</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>下莊村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>西鳥取村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>東鳥取村</p></td>
<td><p>1960年11月1日<br />
東鳥取町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 交通

[南海電氣鐵道](../Page/南海電氣鐵道.md "wikilink")[南海本線自西北側沿著海岸線通過轄內](../Page/南海本線.md "wikilink")，[西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")[阪和線則自東側往南通過轄內山區](../Page/阪和線.md "wikilink")，進入[和歌山](../Page/和歌山.md "wikilink")；兩條鐵路往北接通往大阪市、堺市，往南也都會接往[和歌山市](../Page/和歌山市.md "wikilink")。轄內主要車站則為位於北部屬於的南海本線。

### 鐵路

  - [南海電氣鐵道](../Page/南海電氣鐵道.md "wikilink")
      - [Nankai_mainline_symbol.svg](https://zh.wikipedia.org/wiki/File:Nankai_mainline_symbol.svg "fig:Nankai_mainline_symbol.svg")
        [南海本線](../Page/南海本線.md "wikilink")：（←[泉南市](../Page/泉南市.md "wikilink")）
        -  -  -  - （[岬町](../Page/岬町.md "wikilink")→）
  - [西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")
      - [阪和線](../Page/阪和線.md "wikilink")：（←泉南市） -  -  -
        （[和歌山市](../Page/和歌山市.md "wikilink")）

<File:Ozaki-Sta001.JPG>| 尾崎車站 <File:鳥取ノ荘駅> - panoramio.jpg| 鳥取之莊車站
<File:Hakotsukuri> Station 02.jpg| 箱作車站 <File:Izumi-Tottori>
Station.jpg| 和泉鳥取車站 <File:Yamanakadani> Station.jpg| 山中溪車站

### 道路

  - 高速道路

<!-- end list -->

  - [阪和自動車道](../Page/阪和自動車道.md "wikilink")：（泉南市） -
    [阪南交流道](../Page/阪南交流道.md "wikilink") - （和歌山市）

## 本地出身之名人

  - [塚地武雅](../Page/塚地武雅.md "wikilink")：搞笑藝人及演員

## 參考資料

## 相關條目

  -
## 外部連結

  -

  -

  -

<!-- end list -->

1.

2.