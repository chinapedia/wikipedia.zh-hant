**盜龍屬**（屬名：*Rapator*）意為「盜賊」，是種[獸腳亞目](../Page/獸腳亞目.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，生存於早[白堊紀的](../Page/白堊紀.md "wikilink")[澳洲](../Page/澳洲.md "wikilink")。盜龍长度為9公尺。盜龍起初是在1932年，由[德國](../Page/德國.md "wikilink")[古生物學家](../Page/古生物學家.md "wikilink")[休尼](../Page/休尼.md "wikilink")（Friedrich
von
Huene）根據他所發現的單一骨頭來命名\[1\]。這個骨頭起初被認為來自於一隻不確定屬的獸腳類恐龍手部；後來這骨頭被認為類似[阿瓦拉慈龍科恐龍的手指骨頭](../Page/阿瓦拉慈龍科.md "wikilink")\[2\]。如果盜龍屬於阿瓦拉慈龍科，牠將成為該科最大型的物種\[3\]。但近期的研究顯示盜龍可能是種原始的[虛骨龍類](../Page/虛骨龍類.md "wikilink")，類似[恩霹渥巴龍](../Page/恩霹渥巴龍.md "wikilink")\[4\]。

## 參考資料

<div class="references-small">

<references>

</references>

</div>

## 外部链接

  - [恐龙.NET](https://web.archive.org/web/20170527222928/http://dinosaur.net.cn/)——中国古生物门户网站。
  - [*Rapator* in The Theropod
    Database](https://web.archive.org/web/20070716134106/http://home.myuw.net/eoraptor/Ornithomimosauria.htm#Rapatorornitholestoides)
  - [*Rapator*](http://www.artistwd.com/joyzine/australia/articles/dinosaurs/rapator.php)

[Category:大洋洲恐龍](../Category/大洋洲恐龍.md "wikilink")
[Category:虛骨龍類](../Category/虛骨龍類.md "wikilink")
[Category:下白堊紀恐龍](../Category/下白堊紀恐龍.md "wikilink")

1.

2.

3.
4.  Salisbury, Agnolin, Ezcurra, and Pias (2007). "A critical
    reassessment of the Cretaceous non-avian dinosaur faunas of
    Australia and New Zealand." *Journal of Vertebrate Paleontology*,
    **27**(3): 138A.