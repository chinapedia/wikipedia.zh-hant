[Japan_prov_map_mutsu701.png](https://zh.wikipedia.org/wiki/File:Japan_prov_map_mutsu701.png "fig:Japan_prov_map_mutsu701.png")
[Japan_prov_map_mutsu718.png](https://zh.wikipedia.org/wiki/File:Japan_prov_map_mutsu718.png "fig:Japan_prov_map_mutsu718.png")
[Japan_prov_map_mutsu.png](https://zh.wikipedia.org/wiki/File:Japan_prov_map_mutsu.png "fig:Japan_prov_map_mutsu.png")～1869年的領域\]\]
[thumb](../Page/file:Old_Japan_Mutsu_\(1869\).svg.md "wikilink")
**陸奧國**（），[日本古代的](../Page/日本.md "wikilink")[令制國之一](../Page/令制國.md "wikilink")，屬[東山道](../Page/東山道.md "wikilink")，又稱**奧州**。陸奧國的領域在歷史上變動過4次，但就一般概念（即為時最久的[鎌倉時代至](../Page/鎌倉時代.md "wikilink")1869年的領域）而言，其領域大約包含今日的[福島縣](../Page/福島縣.md "wikilink")、[宮城縣](../Page/宮城縣.md "wikilink")、[岩手縣](../Page/岩手縣.md "wikilink")、[青森縣](../Page/青森縣.md "wikilink")、[秋田縣東北的](../Page/秋田縣.md "wikilink")[鹿角市與](../Page/鹿角市.md "wikilink")[小坂町](../Page/小坂町.md "wikilink")。

## 沿革

陸奧國因為位於[東山道的邊陲](../Page/東山道.md "wikilink")，所以最初稱為「道奧」（），[平安時代後改稱](../Page/平安時代.md "wikilink")「陸奧」（）。

### 7世紀末～712年

陸奧國是在7世紀時由[常陸国分出](../Page/常陸国.md "wikilink")，最初的領域僅相當於今日[東北地方南部區域而已](../Page/東北地方.md "wikilink")。具體範圍如下：

  - [宮城縣的](../Page/宮城縣.md "wikilink")[松島丘陵以南全境](../Page/松島丘陵.md "wikilink")，計有：
      - [仙台平原南半部](../Page/仙台平原.md "wikilink")
      - [阿武隈川流域的](../Page/阿武隈川.md "wikilink")[盆地群](../Page/盆地.md "wikilink")
  - [山形縣的](../Page/山形縣.md "wikilink")[奧羽山脈西側](../Page/奧羽山脈.md "wikilink")[盆地群](../Page/盆地.md "wikilink")，計有：
      - [最上地方的](../Page/最上地方.md "wikilink")[新-{庄}-盆地](../Page/新庄盆地.md "wikilink")
      - [村山地方的](../Page/村山地方.md "wikilink")[山形盆地](../Page/山形盆地.md "wikilink")
      - [置賜地方的](../Page/置賜地方.md "wikilink")[米澤盆地](../Page/米澤盆地.md "wikilink")
  - [福島縣全境扣除東南隅的](../Page/福島縣.md "wikilink")[菊多郡](../Page/菊多郡.md "wikilink")（即現在的[磐城市南部](../Page/磐城市.md "wikilink")），計有
      - [濱通](../Page/濱通.md "wikilink")（該縣東部沿海區域）
      - [中通](../Page/中通_\(福島縣\).md "wikilink")（該縣中部區域）
      - [會津地方](../Page/會津.md "wikilink")

### 712年～718年

[和銅五年](../Page/和銅.md "wikilink")（712年）時陸奧國[最上川流域的](../Page/最上川.md "wikilink")[最上郡](../Page/最上郡.md "wikilink")（今日的最上地方及[村山地方](../Page/村山地方.md "wikilink")）與[置賜郡](../Page/置賜郡.md "wikilink")（今日的置賜地方）分給新成立的[出羽国](../Page/出羽国.md "wikilink")（現在[-{庄}-內地方](../Page/庄內地方.md "wikilink")），僅餘上述的宮城縣境和福島縣境。

### 718年～724年

[養老二年](../Page/養老_\(日本年號\).md "wikilink")（718年）陸奧国再分出[石城國和](../Page/石城國.md "wikilink")[石背國](../Page/石背國.md "wikilink")，此時陸奧國的領域只剩下[阿武隈川下游北岸到](../Page/阿武隈川.md "wikilink")[宮城縣中部的狹小範圍](../Page/宮城縣.md "wikilink")。不過養老六年（722年）開始到[神龜元年](../Page/神龜.md "wikilink")（724年）的這一段期間，石城國與石背國又陸續併回陸奧國，而且前述的[菊多郡也納入陸奧國所領](../Page/菊多郡.md "wikilink")。

### 鎌倉時代～1869年

### 1869年～廢藩置縣

[明治元年十二月七日](../Page/明治.md "wikilink")（[1869年](../Page/1869年.md "wikilink")[1月19日](../Page/1月19日.md "wikilink")）將面積廣大的陸奧國和[出羽國各自分割成幾個小國](../Page/出羽國.md "wikilink")，其中陸奧國共拆分為陸奧國、[陸中國](../Page/陸中國.md "wikilink")、[陸前國](../Page/陸前國.md "wikilink")、[岩代國](../Page/岩代國.md "wikilink")、[磐城國等](../Page/磐城國.md "wikilink")5個部份。此時陸奧國的領域相當於今日[青森縣加上](../Page/青森縣.md "wikilink")[岩手縣西北的](../Page/岩手縣.md "wikilink")[二戶郡](../Page/二戶郡.md "wikilink")，離7世紀設置之初的陸奧國有300公里之遙。隨著[明治維新如火如荼的推展](../Page/明治維新.md "wikilink")，[中央集權已是大勢所趨](../Page/中央集權.md "wikilink")，於是明治四年七月十四日（1871年8月29日）正式宣布[廢藩置縣](../Page/廢藩置縣.md "wikilink")，陸奧國的領域納入[青森縣與](../Page/青森縣.md "wikilink")[岩手縣所領](../Page/岩手縣.md "wikilink")。

## 郡

### 延喜式的郡

<table>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="../Page/膽澤郡.md" title="wikilink">膽澤郡</a></li>
<li><a href="../Page/西白河郡.md" title="wikilink">白河郡</a></li>
<li><a href="../Page/岩瀨郡.md" title="wikilink">磐瀨郡</a></li>
<li><a href="../Page/会津郡.md" title="wikilink">会津郡</a></li>
<li><a href="../Page/耶麻郡.md" title="wikilink">耶麻郡</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/安積郡.md" title="wikilink">安積郡</a></li>
<li><a href="../Page/安達郡.md" title="wikilink">安達郡</a></li>
<li><a href="../Page/信夫郡.md" title="wikilink">信夫郡</a></li>
<li><a href="../Page/刈田郡.md" title="wikilink">刈田郡</a></li>
<li><a href="../Page/柴田郡.md" title="wikilink">柴田郡</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/名取郡.md" title="wikilink">名取郡</a></li>
<li><a href="../Page/菊多郡.md" title="wikilink">菊田郡</a></li>
<li><a href="../Page/石城郡.md" title="wikilink">磐城郡</a></li>
<li><a href="../Page/標葉郡.md" title="wikilink">標葉郡</a></li>
<li><a href="../Page/行方郡.md" title="wikilink">行方郡</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/宇多郡.md" title="wikilink">宇多郡</a></li>
<li><a href="../Page/江刺郡.md" title="wikilink">江刺郡</a></li>
<li><a href="../Page/伊具郡.md" title="wikilink">伊具郡</a></li>
<li><a href="../Page/亘理郡.md" title="wikilink">亘理郡</a></li>
<li><a href="../Page/宮城郡.md" title="wikilink">宮城郡</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/黑川郡.md" title="wikilink">黑川郡</a></li>
<li><a href="../Page/加美郡.md" title="wikilink">賀美郡</a></li>
<li><a href="../Page/色麻郡.md" title="wikilink">色麻郡</a></li>
<li><a href="../Page/玉造郡.md" title="wikilink">玉造郡</a></li>
<li><a href="../Page/志田郡.md" title="wikilink">志太郡</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/栗原郡.md" title="wikilink">栗原郡</a></li>
<li><a href="../Page/磐井郡.md" title="wikilink">磐井郡</a></li>
<li><a href="../Page/長岡郡_(陸奧国).md" title="wikilink">長岡郡</a></li>
<li><a href="../Page/新田郡_(陸奧国).md" title="wikilink">新田郡</a></li>
<li><a href="../Page/大沼郡.md" title="wikilink">大沼郡</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/小田郡_(陸奧国).md" title="wikilink">小田郡</a></li>
<li><a href="../Page/遠田郡.md" title="wikilink">遠田郡</a></li>
<li><a href="../Page/氣仙郡.md" title="wikilink">氣仙郡</a></li>
<li><a href="../Page/牡鹿郡.md" title="wikilink">牡鹿郡</a></li>
<li><a href="../Page/登米郡.md" title="wikilink">登米郡</a></li>
</ul></td>
<td><ul>
<li><a href="../Page/桃生郡.md" title="wikilink">桃生郡</a></li>
</ul></td>
</tr>
</tbody>
</table>

### 明治期陸奧国的郡

  - [津輕郡](../Page/津輕郡.md "wikilink")
  - [北郡](../Page/北郡.md "wikilink")
  - [三戶郡](../Page/三戶郡.md "wikilink")
  - [二戶郡](../Page/二戶郡.md "wikilink")

## 相關項目

  - [日本令制國列表](../Page/日本令制國列表.md "wikilink")

[Category:令制國](../Category/令制國.md "wikilink")