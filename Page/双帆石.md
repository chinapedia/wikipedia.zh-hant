[Xiangshui_Bay_and_Shuangfanshi.jpg](https://zh.wikipedia.org/wiki/File:Xiangshui_Bay_and_Shuangfanshi.jpg "fig:Xiangshui_Bay_and_Shuangfanshi.jpg")
**双帆石**，位于[中国](../Page/中国.md "wikilink")[海南省](../Page/海南省.md "wikilink")[陵水黎族自治县东部海域](../Page/陵水黎族自治县.md "wikilink")，地理坐标为，位于[香水湾东南部](../Page/香水湾.md "wikilink")，西北东南二海里处有[西岛](../Page/西岛.md "wikilink")，共有两块相距100米左右的巨大礁石，西面的礁石较平坦，水深约8-26米，海底为礁石和珊瑚，水域有各种鱼类出没，是矶钓的好去处。

  - 1996年，中国政府发布关于领海范围的声明，双帆石是中国[领海基点之一](../Page/领海基点.md "wikilink")。[](https://web.archive.org/web/20070216040721/http://www.soa.gov.cn/law/960515a.htm)

## 参考

<div class="references-small">

<references/>

</div>

## 另见

  - [大洲岛](../Page/大洲岛.md "wikilink")
  - [双帆](../Page/双帆.md "wikilink")(文昌市)
  - [陵水角](../Page/陵水角.md "wikilink")

## 外部链接

  - [中国东海１０座领海基点石碑建成](http://news.xinhuanet.com/politics/2006-09/14/content_5090042.htm)

  - [中华人民共和国政府关于中华人民共和国领海基线的声明（1996年5月15日）](https://web.archive.org/web/20070216040721/http://www.soa.gov.cn/law/960515a.htm)

  - [Declaration of the Government of the People's Republic of China on
    the baselines of the territorial
    sea（May15th, 1996)](http://www.un.org/Depts/los/LEGISLATIONANDTREATIES/PDFFILES/CHN_1996_Declaration.pdf)

  - [在GOOGLEMAP查看双帆石位置](http://www.google.com/maps?f=q&hl=en&q=+18%C2%B026%276.00%22N,110%C2%B0+8%2724.00%22E&ie=UTF8&z=12&ll=18.443461,110.139999&spn=0.145908,0.43396&t=k&om=1&iwloc=addr)

  - [海南省民政厅受权公告500㎡以上海岛（礁）及海南岛沿岸岬角共108条地名标准化](https://web.archive.org/web/20070310185604/http://www.hainan.gov.cn/data/news/2006/08/16869/)

[Category:海南岛屿](../Category/海南岛屿.md "wikilink")
[琼](../Category/中国大陆领海基点.md "wikilink")