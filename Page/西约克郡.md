**西約克郡**（），[英國](../Page/英國.md "wikilink")[英格蘭](../Page/英格蘭.md "wikilink")[約克郡-亨伯地區的](../Page/約克郡-亨伯.md "wikilink")[名譽郡](../Page/名譽郡.md "wikilink")、[都市郡](../Page/都市郡.md "wikilink")，包含5個[都市自治市](../Page/都市自治市.md "wikilink")。以人口計算，[列斯市](../Page/里茲.md "wikilink")、[布拉德福德市](../Page/布拉德福德市.md "wikilink")、[韋克菲爾德市分別是第](../Page/韋克菲爾德市.md "wikilink")1、2、3大[城市](../Page/英國的城市地位.md "wikilink")、都市自治市；[列斯](../Page/列斯.md "wikilink")、[布拉德福德](../Page/布拉德福德.md "wikilink")、[哈德斯菲爾德](../Page/哈德斯菲爾德.md "wikilink")、[哈利法克斯](../Page/哈利法克斯_\(西約克郡\).md "wikilink")
、[韋克菲爾德](../Page/韋克菲爾德.md "wikilink")、[迪斯伯里](../Page/迪斯伯里.md "wikilink")
、[基斯利](../Page/基斯利.md "wikilink")
、[莫利](../Page/莫利_\(西約克郡\).md "wikilink")、[巴特利](../Page/巴特利.md "wikilink")
分別是第1－9大的鎮。

## 歷史

[中世紀以前](../Page/中世紀.md "wikilink")，[盎格魯-撒克遜人和](../Page/盎格魯-撒克遜人.md "wikilink")[斯堪的納維亞人已在此定居](../Page/斯堪的納維亞.md "wikilink")。土地不利農耕，惟畜牧業發達，牧羊業、乳牛業皆有基礎，而[菊苣](../Page/菊苣.md "wikilink")（根可充咖啡用）與[甘草則幾乎全產於此](../Page/甘草.md "wikilink")。十五世紀時曾發生數次戰役，紡織工業也在此時得到發展。[工業革命時期因](../Page/工業革命.md "wikilink")[煤藏量大](../Page/煤.md "wikilink")，並有著豐富的[鐵礦](../Page/鐵.md "wikilink")，工業曾盛極一時，其中以毛紡織業在全國具有重要性。

## 地方沿革

《[1972年地方政府法案](../Page/1972年地方政府法案.md "wikilink")》在1974年4月1日通過，[約克郡西瑞丁被廢除](../Page/約克郡西瑞丁.md "wikilink")，新設[都市郡](../Page/都市郡.md "wikilink")「西約克郡」，下轄的次級行政區都是[都市自治市](../Page/都市自治市.md "wikilink")（也就是[單一管理區](../Page/單一管理區.md "wikilink")），有獨立的都市自治市議會，實際不受西約克郡的管轄。西約克郡議會更在1986年被廢除。不過，都市自治市仍以「西約克郡」名義組成聯合部門（Joint-boards）統籌、協調牽涉多個都市自治市的民政事務，負責警務、消防、公共交通等。

每一個[名譽郡都有一個代表](../Page/名譽郡.md "wikilink")[英國皇室但沒有實權的](../Page/英國皇室.md "wikilink")[郡尉](../Page/郡尉_\(英國\).md "wikilink")（Lord
Lieutenant）常駐，西約克郡是其一。從地方政府或郡尉的角度看，西約克郡都只是「有郡界的地理範圍」而已。

## 經濟

以下經濟數據來源是英國[國家統計辦工室](../Page/國家統計辦工室.md "wikilink")\[1\]

| 年    | 地區生產總值\[2\] | 農業\[3\] | 工業\[4\] | 服務\[5\] |
| ---- | ----------- | ------- | ------- | ------- |
| 1995 | **21,302**  | 132     | 7,740   | 13,429  |
| 2000 | **27,679**  | 80      | 8,284   | 19,314  |
| 2003 | **31,995**  | 91      | 8,705   | 23,199  |

## 行政區劃

[ `1.``   `[`列斯市`](../Page/里茲.md "wikilink")
`2.``   `[`韋克菲爾德市`](../Page/韋克菲爾德市.md "wikilink")
`3.``   `[`柯克利斯`](../Page/柯克利斯.md "wikilink")
`4.``   `[`科爾德河谷`](../Page/科爾德河谷.md "wikilink")
`5.``   `[`布拉德福德市`](../Page/布拉德福德市.md "wikilink")
](https://zh.wikipedia.org/wiki/File:EnglandWestYorkshireNumbered.png "fig: 1. 列斯市 2. 韋克菲爾德市 3. 柯克利斯 4. 科爾德河谷 5. 布拉德福德市 ")

西約克郡包含5個[都市自治市](../Page/都市自治市.md "wikilink")：

<table>
<thead>
<tr class="header">
<th><p>地方（中譯）</p></th>
<th><p>地方（英文）</p></th>
<th><p>面積（km<sup>2</sup>）</p></th>
<th><p>人口</p></th>
<th><p>密度</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td></td>
<td><p>366.42</p></td>
<td><p>493,100</p></td>
<td><p>1,346</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>Calderdale</p></td>
<td><p>363.92</p></td>
<td><p>198,500</p></td>
<td><p>545</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Kirklees</p></td>
<td><p>408.60</p></td>
<td><p>398,200</p></td>
<td><p>975</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p>551.72</p></td>
<td><p>750,200</p></td>
<td><p>1,360</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p>338.61</p></td>
<td><p>321,200</p></td>
<td><p>949</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 地理

下圖顯示英格蘭48個名譽郡的分佈情況。西約克郡北、東北、東與[北約克郡相鄰](../Page/北約克郡.md "wikilink")，東南與[南約克郡相鄰](../Page/南約克郡.md "wikilink")，南與[打比郡相鄰](../Page/打比郡.md "wikilink")，西南與[大曼徹斯特郡相鄰](../Page/大曼徹斯特.md "wikilink")，西北與[蘭開夏郡相鄰](../Page/蘭開夏.md "wikilink")。

西約克郡位處英格蘭中部[山脈地區](../Page/山脈.md "wikilink")，西部為山地，有河谷，東部為低地。

## 註釋

[Category:英格蘭的郡](../Category/英格蘭的郡.md "wikilink")
[Category:都市郡](../Category/都市郡.md "wikilink")

1.  [published](http://webarchive.nationalarchives.gov.uk/20050303060209/http://www.statistics.gov.uk/downloads/theme_economy/RegionalGVA.pdf)
    (p.240－253) by ''Office for National Statistics.
2.  數字經[四捨五入](../Page/四捨五入.md "wikilink")，與實際數據或有偏差。
3.  包括：狩獵、林業
4.  包括：能源、建築
5.  包括間接統計出來的金融仲裁服務