是一部於1992年上映的[香港電影](../Page/香港電影.md "wikilink")，由[唐季禮擔任導演兼武術指導](../Page/唐季禮.md "wikilink")，以及由[成龍](../Page/成龍.md "wikilink")、[張曼玉和](../Page/張曼玉.md "wikilink")[楊紫瓊擔任領銜主演](../Page/楊紫瓊.md "wikilink")；此電影為《警察故事系列》中的第三部，講述成龍飾演的警察陳家駒的故事。

值得注意的是在这部影片中[張曼玉最后一次扮演陳家駒的女朋友阿梅](../Page/張曼玉.md "wikilink")，此後张開始接演藝術性較高的電影，也開始了與[王家衛的長期合作](../Page/王家衛.md "wikilink")。楊紫瓊之后主演了本片的一部續集《[超級計劃](../Page/超級計劃.md "wikilink")》。那部續集不能算在《[警察故事](../Page/警察故事.md "wikilink")》系列中，而是一个分岔出的新系列。

## 故事

神勇幹探陳家駒遠赴大陸，與大陸美女公安楊建華兩人合作擔任臥底，化身囚犯打入國際軍火毒品走私集團的內部。在楊建華的幫助下，陳家駒順利接近了正在服刑的集團成員豹強，欲偵破販毒頭目猜霸之巢穴，並取出他存於[瑞士銀行的贓款](../Page/瑞士.md "wikilink")。

家駒先爭取猜霸頭號手下豹強賞識，豹強並視家駒為心腹，和他策劃越獄行動，為了放長線，家駒爽快答應了。他們越獄後，豹強主張到家駒隨口編出來的家鄉躲避，幸得楊建華提前安排，才沒有漏出馬腳，楊建華自稱家駒的妹妹，豹乃帶其兩人到[馬來西亞見販毒集團老大猜霸](../Page/馬來西亞.md "wikilink")，並與他們同行。

猜霸看重兩人敏捷的身手，兩人順利成為集團的一員，並吩咐他們前往吉隆坡去營救被大馬政府拘禁的妻子程穎思；家駒巧遇導遊女友阿May，阿May不知家駒正在執行任務，以為他和楊建華在偷情，竟把秘密告訴同事，不料阿May的舉動卻引起了豹部下所獲悉，他以阿May做為人質，威脅家駒儘快救出程穎思。

程穎思被大馬政府判處死刑，給押去法庭，陳與楊兩人先救回阿May，再奮力與豹強和猜霸作生死決鬥。最後豹強和猜霸死去，程說出了瑞士銀行戶口號碼，陳和楊成功破案。

## 演員表

|                                       |         |         |
| ------------------------------------- | ------- | ------- |
| **演　員**                               | **角　色** | **備　註** |
| [成龍](../Page/成龍.md "wikilink")        | 陳家駒     |         |
| [張曼玉](../Page/張曼玉.md "wikilink")      | 阿May    | 家駒女友    |
| [楊紫瓊](../Page/楊紫瓊.md "wikilink")      | 楊建華     | 國際刑事科長  |
| [陳欣健](../Page/陳欣健.md "wikilink")      | 署長      | 驃叔上司    |
| [董驃](../Page/董驃.md "wikilink")        | 驃叔      | 陳家駒上司   |
| [元華](../Page/元華.md "wikilink")        | 豹強      | 猜霸手下    |
| [曾江](../Page/曾江.md "wikilink")        | 猜霸      | 販毒集團首腦  |
| [顧美華](../Page/顧美華.md "wikilink")      | 程穎思     | 猜霸之妻    |
| [王霄](../Page/王霄_\(演員\).md "wikilink") | Pierre  | 猜霸手下    |
| [盧惠光](../Page/盧惠光.md "wikilink")      |         | 猜霸手下    |
| [薛春煒](../Page/薛春煒.md "wikilink")      |         | 猜霸手下    |
| [羅烈](../Page/羅烈.md "wikilink")        | 將軍      | 毒品供應商   |
| [段偉倫](../Page/段偉倫.md "wikilink")      |         | 將軍手下    |
| [沈威](../Page/沈威.md "wikilink")        |         | 毒販      |
| [韓義生](../Page/韓義生.md "wikilink")      |         | 毒販      |
| [火星](../Page/火星_\(演员\).md "wikilink") | 辣雞雄     | 豹強手下    |
|                                       |         |         |

## 電影歌曲

| 曲別  | 歌曲      | 演唱者                            |
| --- | ------- | ------------------------------ |
| 主題曲 | 《我有我路向》 | [成龍](../Page/成龍.md "wikilink") |

## 獎項

<table>
<thead>
<tr class="header">
<th><p>頒獎典禮</p></th>
<th><p>獎項</p></th>
<th><p>名單</p></th>
<th><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong><a href="../Page/第12届香港電影金像獎.md" title="wikilink">第12届香港電影金像獎</a></strong></p></td>
<td><p><a href="../Page/香港電影金像獎最佳男主角.md" title="wikilink">最佳男主角</a></p></td>
<td><p><a href="../Page/成龍.md" title="wikilink">成龍</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/香港電影金像獎最佳動作指導.md" title="wikilink">最佳動作指導</a></p></td>
<td><p><a href="../Page/唐季禮.md" title="wikilink">唐季禮</a>、<a href="../Page/鄧德榮.md" title="wikilink">鄧德榮</a>、<a href="../Page/薛春煒.md" title="wikilink">薛春煒</a>、<a href="../Page/陳文清.md" title="wikilink">陳文清</a>、<a href="../Page/黃明昇.md" title="wikilink">黃明昇</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/第29届金馬獎.md" title="wikilink">第29届金馬獎</a></strong></p></td>
<td><p>最佳男主角</p></td>
<td><p><a href="../Page/成龍.md" title="wikilink">成龍</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳剪輯</p></td>
<td><p><a href="../Page/張耀宗.md" title="wikilink">張耀宗</a>、<a href="../Page/張嘉輝.md" title="wikilink">張嘉輝</a></p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 票房

《警察故事3超級警察》在香港戲院票房為32,609,783港幣，尽管本土成绩远远不及[周星驰的](../Page/周星驰.md "wikilink")《[审死官](../Page/審死官_\(1992年電影\).md "wikilink")》，但台湾的票房创下3.1亿新台币票房纪录，直到2008年才被《[海角7号](../Page/海角7号.md "wikilink")》打破。之後在《[紅番區](../Page/紅番區.md "wikilink")》於[北美票房的成功](../Page/北美.md "wikilink")，《警察故事3超級警察》也在北美地區於1996年7月25日上映，總共獲得1627万美元票房。

## 警察故事系列

警察故事的成功使其多次開拍續集。

  - **[警察故事](../Page/警察故事.md "wikilink")**（1985年）
  - **[警察故事續集](../Page/警察故事續集.md "wikilink")**（1988年）
  - **[警察故事3超級警察](../Page/警察故事3超級警察.md "wikilink")**（1992年）
  - **[警察故事4之簡單任務](../Page/警察故事4之簡單任務.md "wikilink")**（1996年）
  - **[新警察故事](../Page/新警察故事.md "wikilink")**（2004年）
  - **[警察故事2013](../Page/警察故事2013.md "wikilink")**（2013年）

## 外部連結

  - {{@movies|fShk00104558}}

  -
  -
  -
  -
  -
  -
[Category:1990年代动作片](../Category/1990年代动作片.md "wikilink")
[Category:續集電影](../Category/續集電影.md "wikilink")
[Category:香港動作片](../Category/香港動作片.md "wikilink")
[2](../Category/1990年代香港電影作品.md "wikilink")
[Category:马来西亚背景电影](../Category/马来西亚背景电影.md "wikilink")
[Category:毒品相关电影](../Category/毒品相关电影.md "wikilink")
[Category:嘉禾电影](../Category/嘉禾电影.md "wikilink")
[Category:唐季禮電影](../Category/唐季禮電影.md "wikilink")
[Category:金馬獎最佳男主角獲獎電影](../Category/金馬獎最佳男主角獲獎電影.md "wikilink")
[Category:金馬獎最佳剪輯獲獎電影](../Category/金馬獎最佳剪輯獲獎電影.md "wikilink")
[Category:卧底题材电影](../Category/卧底题材电影.md "wikilink")
[Category:佛山背景电影](../Category/佛山背景电影.md "wikilink")