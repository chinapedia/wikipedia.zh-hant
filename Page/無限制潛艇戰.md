**無限制潛艇戰**（Unrestricted submarine
warfare）是指不分軍用或民用的設備或工具，於戰時為求打擊敵方士氣及消耗敵方物資，不惜攻擊非軍用机构或設施，尤其是載具的戰略。這種戰略對與戰事無關的平民百姓而言尤為殘忍，儘管此舉確實有效避免敵人偽裝成民用船來補給或偷襲，但大多仍是擊沉單純的民船，對人民的生命與財產損失極為嚴重。这一战略是基于*航路斩断*学说之上为海军弱国制衡海军强国的不对称战略。

## 第一次世界大戰

[第一次世界大戰中](../Page/第一次世界大戰.md "wikilink")，[德國對](../Page/德國.md "wikilink")[英國實施的](../Page/英國.md "wikilink")[海上封鎖](../Page/海上封鎖.md "wikilink")。英國作为[島國](../Page/島國.md "wikilink")，需要其他國家的物資援助。在此次[封鎖中](../Page/封鎖.md "wikilink")，不論任何船隻，只要向英國方向行驶，且不管是前往英國還是經過，一律以潛艇擊沉，當中有不少無辜甚至德國船隻被擊沉。

此事令[美國經濟大受打擊](../Page/美國.md "wikilink")，原因是美國最依靠作戰國買入軍火作經濟支柱。其後，德軍在[1915年擊沉一隻美國商船及一隻載有美國遊客的英國](../Page/1915年.md "wikilink")[郵輪](../Page/郵輪.md "wikilink")「[盧西塔尼亞號](../Page/盧西塔尼亞號.md "wikilink")」(RMS
Lusitania)，令1198人死亡，美國雖不滿，但未因此而參戰，後因德國[煽動墨西哥反美才參戰](../Page/齊默爾曼電報.md "wikilink")，间接造成最後德國戰敗。

德國每一年遞增每月出動潛艇襲擊英美商船隊，希望能迫使仰賴海上運輸的英國經濟受到打擊，根據，德國的出動潛艇如下：

  - 1915年：出動360艘（美國抗議而有停止）
  - 1916年：出動720艘
  - 1917年：出動1320艘（再次實行）
  - 1918年：出動180艘

[缩略图](https://zh.wikipedia.org/wiki/File:U_570.jpg "fig:缩略图")

## 第二次世界大戰

[德軍於](../Page/德軍.md "wikilink")[二次大戰於](../Page/二次大戰.md "wikilink")[大西洋使用大量](../Page/大西洋.md "wikilink")[潛艦](../Page/潛艦.md "wikilink")（[U潛艇](../Page/U潛艇.md "wikilink")；Untersee-boot）截擊越洋船隻，以圖阻止[美國援](../Page/美國.md "wikilink")[歐的行動](../Page/欧洲.md "wikilink")。所攻擊船艦包括軍艦、運兵船、運送軍事物資醫院艦、以及其他民用船艦。美軍則以護航艦團作為對策，減少大量損失。

同時在[太平洋戰場](../Page/太平洋戰場.md "wikilink")，美軍潛艇同樣對日本進行無差別潛艇戰略，擊沉大量日本商船，尤其是大批往返於日本、台灣、菲律賓、中國戰區、東南亞戰區等地的運兵船及運輸艦，使得日本無法將各戰區或佔領區的軍用和民用物資作有效分配；到了二戰末期，因美軍的海路封鎖，日本本土已經變成名副其實的"海上孤島"。

[Category:戰略](../Category/戰略.md "wikilink")
[Category:德國海軍史](../Category/德國海軍史.md "wikilink")
[Category:潛艇事件](../Category/潛艇事件.md "wikilink")
[Category:海戰戰術](../Category/海戰戰術.md "wikilink")