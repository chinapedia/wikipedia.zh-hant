[以[台鐵柴電機車牽引的一列莒光號列車正通過三貂嶺車站](../Page/台鐵柴電機車.md "wikilink")|thumb|upright](https://zh.wikipedia.org/wiki/File:TRA_R123_passing_through_Sandiaoling_Station_20050516.jpg "fig:以台鐵柴電機車牽引的一列莒光號列車正通過三貂嶺車站|thumb|upright")
[三貂嶺車站前明隧道](https://zh.wikipedia.org/wiki/File:TRA_Sandiaoling_Station_20070727.jpg "fig:三貂嶺車站前明隧道")
**三貂嶺車站**位於[台灣](../Page/台灣.md "wikilink")[新北市](../Page/新北市.md "wikilink")[瑞芳區](../Page/瑞芳區.md "wikilink")，為[臺灣鐵路管理局](../Page/臺灣鐵路管理局.md "wikilink")[宜蘭線](../Page/宜蘭線.md "wikilink")、[平溪線的](../Page/平溪線.md "wikilink")[鐵路車站](../Page/鐵路車站.md "wikilink")。由於原本是為了平溪線與[正線匯集時的](../Page/正線.md "wikilink")[閉塞控制而設置的工作用車站](../Page/閉塞_\(鐵路\).md "wikilink")，並不作為對外營運用途，三貂嶺車站是台灣唯一沒有道路可以直接抵達的鐵路車站，而是與鄰近的[公路隔著](../Page/公路.md "wikilink")[基隆河相望](../Page/基隆河.md "wikilink")。欲在本站搭車的旅客，必須自上游一段距離的[鐵路橋處利用沿著鐵路線旁的人行步道跨越](../Page/鐵路橋.md "wikilink")[河床之後](../Page/河床.md "wikilink")，徒步抵達本站。

## 車站構造

  - [岸式月台兩座](../Page/岸式月台.md "wikilink")。
  - 車站建設在河岸與山壁間，腹地狹窄而無法建設天橋及地下道，往[七堵](../Page/七堵車站.md "wikilink")、[瑞芳的乘客須在站務人員引導下跨越軌道乘車](../Page/瑞芳車站.md "wikilink")。
  - 山側月台因山壁滲水終年潮濕。

## 營運狀況

  - 目前為[三等站](../Page/臺灣鐵路管理局車站等級#三等站.md "wikilink")。僅停靠[區間車](../Page/臺鐵區間車.md "wikilink")。
  - [平溪線於本站以南自](../Page/平溪線.md "wikilink")[宜蘭線分歧](../Page/宜蘭線.md "wikilink")。
  - 全台唯一無公路可抵達之車站。
  - 站內號誌樓廢棄後曾屹立一段時間\[1\]，今已拆除\[2\]。
  - 本站設有「鐵路之旅－小站巡禮紀念章」之戳章，可供旅客蓋戳留念。
  - 本站開放使用[悠遊卡](../Page/悠遊卡.md "wikilink")、[一卡通及](../Page/一卡通_\(台灣\).md "wikilink")[icash
    2.0付費](../Page/icash.md "wikilink")。

| 年度   | 日均進站旅次（人） |
| ---- | --------- |
| 1998 | 37        |
| 1999 | 41        |
| 2000 | 35        |
| 2001 | 48        |
| 2002 | 33        |
| 2003 | 35        |
| 2004 | 34        |
| 2005 | 28        |
| 2006 | 31        |
| 2007 | 27        |
| 2008 | 29        |
| 2009 | 29        |
| 2010 | 30        |
| 2011 | 53        |
| 2012 | 48        |
| 2013 | 51        |
| 2014 | 62        |
| 2015 | 80        |
| 2016 | 81        |
| 2017 | 86        |

歷年旅客人次紀錄 \[3\]

## 車站週邊

  - [基隆河](../Page/基隆河.md "wikilink")
  - [三貂嶺古道](../Page/三貂嶺古道.md "wikilink")（[淡蘭古道](../Page/淡蘭古道.md "wikilink")）
  - [三瓜子隧道遺蹟](../Page/三瓜子隧道.md "wikilink")
  - [三貂嶺隧道](../Page/三貂嶺隧道.md "wikilink")
  - 三貂煤礦遺跡
  - 幼坑瀑布（九層瀑布）

## 歷史

  - 本站名稱由來請參見[聖地亞哥](../Page/聖地牙哥.md "wikilink")、[三貂角](../Page/三貂角.md "wikilink")。
  - 1922年5月16日：「三貂嶺」驛開業。
  - 2013年9月30日：啟用多卡通刷卡機

## 歷任站長

## 鄰近車站

|-

## 腳注

## 外部連結

<http://jp-shitman.blogspot.tw/2013/08/blog-post_28.html>

[Category:宜蘭線車站](../Category/宜蘭線車站.md "wikilink")
[Category:平溪線車站](../Category/平溪線車站.md "wikilink")
[Category:瑞芳區鐵路車站](../Category/瑞芳區鐵路車站.md "wikilink")
[Category:新北市旅遊景點](../Category/新北市旅遊景點.md "wikilink")
[Category:1922年启用的铁路车站](../Category/1922年启用的铁路车站.md "wikilink")

1.
2.  [平溪線的情調就要毀了！](https://web.archive.org/web/20140106043245/http://railway.tw/?p=426)
    @ 2013.02.20
3.  [新北市交通統計年報](https://drive.google.com/file/d/0B5UnHdQTGrk2N0xGNHJLOXZaMkE/edit)