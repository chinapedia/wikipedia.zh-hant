是1989年[吳宇森导演的警匪電影](../Page/吳宇森.md "wikilink")。由[周潤發](../Page/周潤發.md "wikilink")（飾演小莊），[葉倩文](../Page/葉倩文.md "wikilink")（飾演珍妮）與[李修賢](../Page/李修賢.md "wikilink")（飾演李鷹）主演。導演[吳宇森因本片紅至西洋](../Page/吳宇森.md "wikilink")，製片為同是導演的[徐克](../Page/徐克.md "wikilink")。本片獲視為亞洲英雄片最巅峰的作品之一。

## 劇情介紹

小莊（[周潤發飾](../Page/周潤發.md "wikilink")）是一個[職業殺手](../Page/職業殺手.md "wikilink")，因為在「工作」時被發現而遭雇主解僱，然而開槍時的閃光意外弄瞎了年輕的夜總會女歌手珍妮（[葉蒨文飾](../Page/葉蒨文.md "wikilink")）。为了帮助那个幾乎瞎了的女人，他籌了一大筆錢，來替她做恢復視力的眼角膜移植。小莊吸引了一個警探李鹰（[李修賢飾](../Page/李修賢.md "wikilink")）調查他的犯罪，最終兩人因緣際會地遇上。看到先前小莊救的小女孩與得知珍妮的手術後，李鷹明瞭了逮捕這個殺手並不能完成什麼正義。電影的高潮，是兩人與三合會在廢棄的教堂內的激烈槍戰，其導演手法遠超出當時人所能想像的程度。此時小莊要李鷹答應，要是他有什麼三長兩短，一定要幫他把眼角膜捐給珍妮。

電影結尾，小莊被鎗打瞎眼睛，重傷的珍妮在黑暗之中爬向他……

黑道老大被警方逮捕而免於一死，但李鷹卻義無反顧在警網之中射殺了他。李鷹在小莊死後認識到，法律是沒辦法還給小莊這種人一個公義。他最後選擇了自我的良知，違抗了維護世俗正義的法律規條。

## 人物角色

|                                         |        |                                        |                                     |
| --------------------------------------- | ------ | -------------------------------------- | ----------------------------------- |
| **演員**                                  | **角色** | **粵語配音**                               | **關係／備註**                           |
| [周潤發](../Page/周潤發.md "wikilink")        | 小莊     | 周潤發                                    | 殺手                                  |
| [李修賢](../Page/李修賢.md "wikilink")        | 李鷹     | [朱子聰](../Page/朱子聰.md "wikilink")       | 警探                                  |
| [葉蒨文](../Page/葉蒨文.md "wikilink")        | 珍妮     | [盧素娟](../Page/盧素娟.md "wikilink")       | 酒廊歌手                                |
| [朱江](../Page/朱江.md "wikilink")          | 馮剛（四哥） | 朱江                                     | 小莊經紀人，叔父輩                           |
| [曾江](../Page/曾江.md "wikilink")          | 老曾     | 曾江                                     | 警探                                  |
| [成奎安](../Page/成奎安.md "wikilink")        | 汪海     | [黃志成](../Page/黃志成.md "wikilink")       | 社團頭目，殺手，汪東源之侄子，小莊之敵人                |
| [伊凡威](../Page/王俊棠.md "wikilink")        | FRANK  | [陳永信](../Page/陳永信.md "wikilink")       | 殺手集團首領，受僱汪海對付小莊                     |
| [黃光亮](../Page/黃光亮.md "wikilink")        | 黃熊     | [陳永信](../Page/陳永信.md "wikilink")       | 變態殺手                                |
| [葉榮祖](../Page/葉榮祖.md "wikilink")        | 汪東源    | [陳永信](../Page/陳永信.md "wikilink")       | 暗殺目標                                |
| [黃炳耀](../Page/黃炳耀_\(編劇\).md "wikilink") | 杜警司    | [楊捷康](../Page/楊捷康.md "wikilink")       | 李鷹及老曾之上司                            |
| [黃栢文](../Page/黃栢文.md "wikilink")        | 陳博文    | [陳欣](../Page/陳欣_\(配音員\).md "wikilink") |                                     |
| [吳少雄](../Page/吳少雄.md "wikilink")        | 殺手     |                                        |                                     |
| [楊星](../Page/楊星.md "wikilink")          | 汪海保鑣   |                                        |                                     |
| [顏昭雄](../Page/顏昭雄.md "wikilink")        | 汪海保鑣   |                                        |                                     |
| [林聰](../Page/林聰.md "wikilink")          | 張宏     |                                        | 汪東源之知己，暗殺目標                         |
| [黃志偉](../Page/黃志偉.md "wikilink")        | 汪海保鑣   |                                        |                                     |
| [李耀敬](../Page/李耀敬.md "wikilink")        | 汪海保鑣   |                                        |                                     |
| [王偉樑](../Page/王偉樑.md "wikilink")        | 急症室醫生  | 陳欣                                     |                                     |
| [盧雄](../Page/盧雄.md "wikilink")          | 投訴科主管  | [楊捷康](../Page/楊捷康.md "wikilink")       | 處理李鷹在電車上開槍射殺黃熊而導致一名女乘客因心臟病受驚致死的投訴個案 |

## 创作

劇情類似1954年由 [洛克·哈德森](../Page/洛克·哈德森.md "wikilink")（Rock
Hudson）與[珍·惠曼主演的](../Page/珍·惠曼.md "wikilink")[好萊塢電影](../Page/好萊塢.md "wikilink")《地老天荒不了情》。在那部影片中，洛饰演一名花花公子，意外弄瞎了一個年輕女性，之后处于良心发现和对女子的怜悯与女生結交，並想要讓她動眼睛手術來消除自己的罪惡感，自首至尾沒有透露他自己的身份，到后来永恒的爱情。

然而《喋血雙雄》着墨重點落在小莊（[周潤發飾](../Page/周潤發.md "wikilink")）身上，小莊的角色是源自[阿倫·狄龍在](../Page/阿倫·狄龍.md "wikilink")《[獨行殺手](../Page/獨行殺手.md "wikilink")》中扮演的柯斯特罗，柯斯特罗是一个酷酷的杀手，在一场舞台表演时戴着白手套，枪杀了一名男子。

劇情上參考由[高倉健和](../Page/高倉健.md "wikilink")[丹波哲郎主演的](../Page/丹波哲郎.md "wikilink")《雙雄喋血記》，而[高倉健和](../Page/高倉健.md "wikilink")[丹波哲郎分别飾演殺手和警探](../Page/丹波哲郎.md "wikilink")。

## 獎項

<table>
<thead>
<tr class="header">
<th><p>頒獎典禮</p></th>
<th><p>獎項</p></th>
<th><p>名字</p></th>
<th><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/1990年香港電影金像獎.md" title="wikilink">第九屆香港電影金像獎</a></p></td>
<td><p><strong>最佳導演</strong></p></td>
<td><p><a href="../Page/吳宇森.md" title="wikilink">吳宇森</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>最佳剪接</strong></p></td>
<td><p>樊恭榮</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳电影</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳编剧</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳男配角</p></td>
<td><p>朱江</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳摄影</p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 電影歌曲

  - 主題曲〈淺醉一生〉主唱：[葉蒨文](../Page/葉蒨文.md "wikilink")
  - 插曲〈隨緣〉主唱：葉蒨文

## 影响

《喋血双雄》是吴宇森平生最完美的作品，他可能毕生都无法超越这部电影的成就。在当年的[香港电影金像奖评选中](../Page/香港电影金像奖.md "wikilink")，《喋血双雄》获得最佳导演、最佳剪接（樊恭荣）两项奖。在刚刚推出的时候，这部电影被视为完美的商业暴力片，此后十年中，人们对它的评价不断改变，1999年，美国《[时代周刊](../Page/时代周刊.md "wikilink")》将它列为20世纪亚洲十大影片之一。

喋血雙雄無論在藝術上或是票房上都有著耀眼的成績，不只是片中的暴力美學，也包括了榮譽，愛情以及友情，近似宗教殉道式的救贖方式（在動作片中少見），大大的震撼了觀眾。本片也對[倫理是否高於](../Page/倫理.md "wikilink")[法律加以著墨](../Page/法律.md "wikilink")，使片中的警察開始懷疑自身維護法律的動機是否不近人情。

## 参考资料

## 外部链接

  - {{@movies|fkhk40097202|喋血双雄}}

  -
  -
  -
  -
  -
  -
[Category:1989年电影](../Category/1989年电影.md "wikilink")
[Category:粤语电影](../Category/粤语电影.md "wikilink")
[Category:1980年代动作片](../Category/1980年代动作片.md "wikilink")
[Category:吴宇森电影](../Category/吴宇森电影.md "wikilink")
[Category:香港動作片](../Category/香港動作片.md "wikilink")
[Category:香港警匪片](../Category/香港警匪片.md "wikilink")
[Category:枪战片](../Category/枪战片.md "wikilink")
[Category:杀手主角题材电影](../Category/杀手主角题材电影.md "wikilink")
[Category:邪典电影](../Category/邪典电影.md "wikilink")
[Category:悲劇电影](../Category/悲劇电影.md "wikilink")
[9](../Category/1980年代香港電影作品.md "wikilink")
[Category:香港电影金像奖最佳导演获奖电影](../Category/香港电影金像奖最佳导演获奖电影.md "wikilink")
[Category:香港电影金像奖最佳剪接获奖电影](../Category/香港电影金像奖最佳剪接获奖电影.md "wikilink")
[Category:卢冠廷配乐电影](../Category/卢冠廷配乐电影.md "wikilink")