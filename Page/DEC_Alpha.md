[DEC_Alpha_21-35023-13_J40793-28_top.jpg](https://zh.wikipedia.org/wiki/File:DEC_Alpha_21-35023-13_J40793-28_top.jpg "fig:DEC_Alpha_21-35023-13_J40793-28_top.jpg")
**DEC Alpha**，也称为**Alpha
AXP**，是由迪吉多公司開發的[64位](../Page/64位.md "wikilink")[RISC](../Page/精简指令集.md "wikilink")[指令集架構](../Page/指令集架構.md "wikilink")[微处理器](../Page/微处理器.md "wikilink")。最初由[DEC公司制造](../Page/DEC.md "wikilink")，并被用于DEC自己的[工作站和](../Page/工作站.md "wikilink")[服务器中](../Page/服务器.md "wikilink")。作为[VAX的后续被开发](../Page/VAX.md "wikilink")，支援[VMS](../Page/VMS.md "wikilink")[操作系统](../Page/操作系统.md "wikilink")，如[Digital
UNIX](../Page/Digital_UNIX.md "wikilink")。不久之后[开放源代码的操作系统也可以在其上运行](../Page/开放源代码.md "wikilink")，如[Linux和](../Page/Linux.md "wikilink")[BSD](../Page/BSD.md "wikilink")。[Microsoft
Windows支持这款处理器](../Page/Microsoft_Windows.md "wikilink")，直到[Windows NT
4.0](../Page/Windows_NT.md "wikilink") SP6，但是从[Windows
2000](../Page/Windows_2000.md "wikilink") beta3开始放弃了对Alpha的支援。

1998年，随着DEC被一起卖给[康柏](../Page/康柏.md "wikilink")。2001年，被康柏卖给[Intel](../Page/Intel.md "wikilink")。同年，[惠普收购康柏](../Page/惠普.md "wikilink")，继续开发基于Alpha处理器的产品到2004年。

2011年，部署在中国超级计算济南中心的[神威蓝光](../Page/神威蓝光.md "wikilink")[超级计算机曝光](../Page/超级计算机.md "wikilink")，其采用了据称是自主知识产权的神威蓝光SW-1600处理器。根据网络资料，神威蓝光处理器基于专利已经过期的DEC
ALPHA 21164A
EV-56架构，单CPU中集成了16个核心，主频975MHz到1.2GHz，浮点数计算峰值性能140.8GFlops@1.1GHz，集成了DDR3内存控制器，并支持16GB内存。\[1\]

## 参考资料

[Category:微处理器](../Category/微处理器.md "wikilink")

1.