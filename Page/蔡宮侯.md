**蔡宮侯**，**姬**姓，本字失傳，是[西周](../Page/西周.md "wikilink")[諸侯國](../Page/諸侯國.md "wikilink")[蔡国第四任國君](../Page/蔡国.md "wikilink")。

他是[蔡伯荒之子](../Page/蔡伯荒.md "wikilink")，在位年代及年數不明，[陳夢家](../Page/陳夢家.md "wikilink")《西周年代考》認為當在周[共](../Page/周共王.md "wikilink")、[懿二王時期](../Page/周懿王.md "wikilink")。《史記》記載「[蔡伯荒卒](../Page/蔡伯荒.md "wikilink")，子宮侯立」，《尚史》、《資治通鑑外紀》均沿用《史記》原文。他死後傳位給兒子蔡厲侯。

<div style="clear: both; width: 100%; padding: 0; text-align: left; border: none;" class="NavFrame">

<div style="background: #ccddcc; text-align: center; border: 1px solid #667766" class="NavHead">

**蔡宮侯的祖先**

</div>

<div class="NavContent" style="display:none;">

<center>

</center>

</div>

</div>

## 參考文獻

  - [陳夢家](../Page/陳夢家.md "wikilink")《西周年代考》第4部附表，四、西周諸侯世表
  - [司馬遷](../Page/司馬遷.md "wikilink")《史記》卷三十五《管蔡世家》第五
  - [李鍇](../Page/李鍇.md "wikilink")《尚史》卷十《世家十一蔡世家》
  - [劉恕](../Page/劉恕.md "wikilink")《資治通鑑外紀》卷三

[Category:蔡國君主](../Category/蔡國君主.md "wikilink")
[Category:西周人](../Category/西周人.md "wikilink")