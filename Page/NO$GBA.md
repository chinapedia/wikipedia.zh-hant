**NO$GBA**是一个免费的[任天堂DS和](../Page/任天堂DS.md "wikilink")[Game Boy
Advance在Windows和DOS上运行的](../Page/Game_Boy_Advance.md "wikilink")[模拟器](../Page/模拟器.md "wikilink")。它能够通过加载游戏ROM运行包括自制的游戏或者商业游戏。本模拟器是第一款可以运行商业ROM的任天堂DS模拟器。

推出新版本后，次新版的就可以免费下载了，如2.7a以后的版本都必须付出2.50[美元才能](../Page/美元.md "wikilink")[下载](../Page/下载.md "wikilink")（2.7b必须付費，2.7a可以[免费下载](../Page/免费下载.md "wikilink")）。從2.7c版本開始,最新版也提供免費下載。No$gba不衹能模拟任天堂DS，也能模拟[Game
Boy
Advance的游戏](../Page/Game_Boy_Advance.md "wikilink")，但GBA的運行水準反而不如NDS好。

## 历史

Martin Korth最初于1997年在DOS上制作了称为NO$GMB的Game Boy模拟器。当1998年Game Boy
Color发行后，本模拟器又升级可以运行GBC游戏。

当任天堂于2004年发行任天堂DS后，研发在NO$GBA上运行DS游戏的计划开始。2005年5月17日，2.1版本开始支持运行任天堂DS游戏。

## No$gba更新改進

  - NO$GBA V2.2──版本包含了对一些任天堂DS的仿真支持。
  - NO$GBA V2.3──包含了几乎全部的任天堂DS仿真支持包括[三维视频](../Page/三维视频.md "wikilink")。
  - NO$GBA V2.3a──还包含了任天堂DS的声音模拟。
  - NO$GBA V2.3b
  - NO$GBA V2.3c──一些重要的任天堂DS相关的新增／[补丁](../Page/补丁.md "wikilink")。
  - NO$GBA
    V2.3d──增添[麦克风支持](../Page/麦克风.md "wikilink")，三维视频改进，完整的[dos](../Page/dos.md "wikilink")-dpmi
    compatibilty ，和更多的[编程格式](../Page/编程格式.md "wikilink")。
  - NO$GBA V2.4
  - NO$GBA V2.4a──修复 discoverd adpcm 的声音错误。
  - NO$GBA V2.4b──完美的[GBA电子阅读器](../Page/GBA电子阅读器.md "wikilink")（英：[GBA
    e-Reader](../Page/GBA_e-Reader.md "wikilink")）模拟功能。
  - NO$GBA V2.4c──一些细节（主要是和电子阅读器有关）
  - NO$GBA V2.4d
  - NO$GBA V2.4e──[游戏作弊码输入功能](../Page/游戏作弊码.md "wikilink")。
  - NO$GBA V2.4f
  - NO$GBA V2.5──游戏速度大大增加。
  - NO$GBA V2.5a──调试版本和修正。
  - NO$GBA V2.5b──三维视频 frameskip 功能，三维视频 vram viewer 功能和
    [WiFi](../Page/WiFi.md "wikilink") 服务细节。
  - NO$GBA V2.5c
  - NO$GBA
    V2.6──[三维绘制软件](../Page/三维绘制软件.md "wikilink")（速度比[OpenGL快大约](../Page/OpenGL.md "wikilink")2倍）。
  - NO$GBA V2.6a──游戏速度更快／更好的三维绘制和[备份侦查](../Page/备份侦查.md "wikilink")。
  - NO$GBA V2.7
  - NO$GBA V2.7a──新增視窗放大功能。
  - NO$GBA V2.7b──記錄檔儲存容量加大。
  - NO$GBA V2.7c
  - NO$GBA V2.7d
  - NO$GBA V2.8
  - NO$GBA V2.8a
  - NO$GBA V2.8b
  - NO$GBA V2.8c
  - NO$GBA V2.8d
  - NO$GBA V2.8e
  - NO$GBA V2.8f 最新版

## 參考來源

## 外部連結

  - [NO$GBA官方網站](http://problemkaputt.de/gba.htm)

[Category:遊戲機模擬器](../Category/遊戲機模擬器.md "wikilink")
[Category:任天堂DS](../Category/任天堂DS.md "wikilink")