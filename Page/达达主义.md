[Dada1.jpg](https://zh.wikipedia.org/wiki/File:Dada1.jpg "fig:Dada1.jpg")

**达达主义**（或）是一场兴起于一战时期的[苏黎世](../Page/苏黎世.md "wikilink")，涉及[视觉艺术](../Page/视觉艺术.md "wikilink")、[文学](../Page/文学.md "wikilink")（主要是[诗歌](../Page/诗歌.md "wikilink")）、[戏剧和](../Page/戏剧.md "wikilink")[美术设计等领域的文艺运动](../Page/美术设计.md "wikilink")。达达主义是20世纪西方文艺发展历程中的一个重要流派，是[第一次世界大战颠覆](../Page/第一次世界大战.md "wikilink")、摧毁旧有[欧洲社会和文化秩序的产物](../Page/欧洲.md "wikilink")。达达主义作为一场文艺运动持续的时间并不长，波及范围却很广，对20世纪的一切[现代主义文艺流派都产生了影响](../Page/现代主义.md "wikilink")。

## 特征

达达主义者的活动包括公开集会、[示威](../Page/示威.md "wikilink")、出版[艺术及](../Page/艺术.md "wikilink")[文学期刊等等](../Page/文学.md "wikilink")。在达达主义者的出版物中，充滿着对艺术、[政治](../Page/政治.md "wikilink")、[文化的热情洋溢的评述和见解](../Page/文化.md "wikilink")。

达达主义的主要特徵包括：追求清醒的非理性状态、拒绝约定俗成的艺术标准、幻灭感、愤世嫉俗；追求无意、偶然和随兴而做的境界等等。这场运动的诞生是对野蛮的[第一次世界大战](../Page/第一次世界大战.md "wikilink")（1914-1918）的一种抗议。达达主义者们坚信是[中产阶级的价值观催生了第一次世界大战](../Page/中产阶级.md "wikilink")，而这种价值观是一种僵化、呆板的压抑性力量，不仅仅体现在艺术上，还遍及日常生活的方方面面。达达主义运动影响了后来的一些文艺流派，包括[超现实主义和](../Page/超现实主义.md "wikilink")[激浪派](../Page/激浪派.md "wikilink")。

达达主义者认为“达达”并不是一种艺术，而是一种“反艺术”。无论现行的艺术标准是什么，达达主义都与之针锋相对。由于艺术和[美学相关](../Page/美学.md "wikilink")，于是达达干脆就连美学也忽略了。传统艺术品通常要传递一些必要的、暗示性的、潜在的信息，而达达者的创作则追求“无意义”的境界。对于达达主义作品的解读完全取决于欣赏者自己的品味。此外，艺术诉求于给人以某种感观，而达达艺术品则要给人以某种“侵犯”。讽刺的是，尽管达达主义如此的反艺术，达达主义本身就是[现代主义的一个重要的流派](../Page/现代艺术.md "wikilink")。“达达”作为对艺术和世界的一种注解，它本身也就变成了一种艺术。

达达主义运动的大部分参与者都深受[虚无主义观点的影响](../Page/虚无主义.md "wikilink")，认为[人类创造的一切都无实际价值](../Page/人类.md "wikilink")，包括艺术在内。达达主义者进行艺术创作的根基在于机遇和偶然性因素。

达达主义的理念反映了第一次世界大战对许多人旧有价值观的颠覆力量。既然很难从无序的世界中找到实际的意义，那么便索性把这种无序当作是这个世界的某种天性，并以之去颠覆那些维系着旧秩序的旧美学体系。他们认为，正是这种旧的秩序导致了第一次世界大战这场惨绝人寰的人间悲剧。达达主义者试图通过对旧秩序的拒绝达到彻底瓦解旧秩序的目的。

## 历史

### “达达”一词的由来

关于“达达”一词的由来，历来众说纷纭。有些人认为这是一个没有实际意义的词，有一些人则认为它来自[罗马尼亚艺术家](../Page/罗马尼亚.md "wikilink")[查拉和](../Page/查拉.md "wikilink")[詹可频繁使用的口头语](../Page/詹可.md "wikilink")“da,
da”，在[罗马尼亚语中意为](../Page/罗马尼亚语.md "wikilink")“是的，是的”。最流行的一种说法是，1916年，一群艺术家在[苏黎世集会](../Page/苏黎世.md "wikilink")，准备为他们的组织取个名字。他们随便翻开一本[法](../Page/法语.md "wikilink")[德词典](../Page/德语.md "wikilink")，任意选择了一个词，就是“dada”。在[法语中](../Page/法语.md "wikilink")，“达达”一词意为儿童玩耍用的[摇木马](../Page/摇木马.md "wikilink")。

因此，这场运动就被命名为“达达主义”，以昭显其随意性，而非一场一般意义上的“文艺运动”。

### 苏黎世

1916年，[雨果·巴尔](../Page/雨果·巴尔.md "wikilink")、艾米·翰宁斯、[特里斯坦·查拉](../Page/查拉.md "wikilink")、[汉斯·阿尔普](../Page/汉斯·阿尔普.md "wikilink")、[理查德·胡森贝克和苏菲](../Page/理查德·胡森贝克.md "wikilink")·托伯等流亡[苏黎世的艺术家在当地的](../Page/苏黎世.md "wikilink")“伏尔泰酒店”成立了一个文艺活动社团，他们通过讨论[艺术话题和演出等方式来表达对](../Page/艺术.md "wikilink")[战争](../Page/战争.md "wikilink")，以及催生战争的价值观的厌恶。同年10月6日，这个组织正式取名为“达达”。

1916年7月14日，在这个组织的第一次公开集会上，巴尔公开宣读了所谓的“[达达主义宣言](https://web.archive.org/web/20051113225720/http://wikisource.org/wiki/Dada_Manifesto_%281916%2C_Hugo_Ball%29)”。1918年，[查拉撰写了另外一份达达主义宣言](../Page/查拉.md "wikilink")，这份宣言被认为是达达主义最重要的宣言之一。在此之后，很多艺术家都发表过类似的宣言。一本名为《伏尔泰酒店》的期刊是这一运动前期的主要成果。

酒店停業之后，达达主义者的活动开始转移向一个新的画廊。不久，巴尔离开了[欧洲](../Page/欧洲.md "wikilink")，而[查拉则开始大肆宣扬达达主义的观点](../Page/查拉.md "wikilink")。他给[法国和](../Page/法国.md "wikilink")[意大利的艺术家和作家们写信](../Page/意大利.md "wikilink")，激烈的抨击他们的作品。很快，[查拉便成为达达主义的领袖以及名副其实的战略](../Page/查拉.md "wikilink")[统帅](../Page/统帅.md "wikilink")。

以[查拉为舵手的](../Page/查拉.md "wikilink")[苏黎世达达主义者们出版了一本名为](../Page/苏黎世.md "wikilink")《达达》的期刊。这本期刊1917年7月创刊，在[苏黎世出版了](../Page/苏黎世.md "wikilink")5期，并在[巴黎出版了最后两期](../Page/巴黎.md "wikilink")。

1918年，[第一次世界大战结束](../Page/第一次世界大战.md "wikilink")。旅居[苏黎世的达达主义者们大多返回自己的国家](../Page/苏黎世.md "wikilink")，其中有一些人开始在其他城市宣扬达达主义思想。

### 柏林

[德国的达达主义运动并不如其他国家的达达主义运动那样](../Page/德国.md "wikilink")“反艺术”，而是具有非常显著的[政治和社会变革色彩](../Page/政治.md "wikilink")。[德国的达达主义者热衷于发表煽动性的宣言](../Page/德国.md "wikilink")，动用宣传和讽刺的力量，发动大规模的公众示威和政治活动。

1918年2月，[理查德·胡森贝克在](../Page/理查德·胡森贝克.md "wikilink")[柏林第一次发表关于达达主义的演讲](../Page/柏林.md "wikilink")，并在同年年底发表了一篇达达主义宣言。[汉纳·胡赫和](../Page/汉纳·胡赫.md "wikilink")[乔治·格罗茨用达达主义的观点来表达对战后](../Page/乔治·格罗茨.md "wikilink")[共产主义的同情](../Page/共产主义.md "wikilink")。[格罗茨和](../Page/格罗茨.md "wikilink")[约翰·哈特菲德在这一时期共同发明了](../Page/约翰·哈特菲德.md "wikilink")“[摄影蒙太奇](../Page/摄影蒙太奇.md "wikilink")”技术。艺术家们出版了一系列政治刊物（都很短命），并在1920年举办了一场达达主义的国际展览会。

[柏林的达达主义运动内部矛盾重重](../Page/柏林.md "wikilink")。[库尔特·施威特斯纠集一些其他艺术家脱离了柏林的达达主义组织](../Page/库尔特·施威特斯.md "wikilink")。施威特斯迁至[汉诺威](../Page/汉诺威.md "wikilink")，并在那里发展了更具个人风格的达达主义。

### 科隆

1920年，[马克思·恩斯特](../Page/马克思·恩斯特.md "wikilink")、[约翰奈斯·西奥多·巴尔杰德和](../Page/约翰奈斯·西奥多·巴尔杰德.md "wikilink")[阿尔普在德国](../Page/阿尔普.md "wikilink")[科隆举办了一场广受争议的达达主义展览](../Page/科隆.md "wikilink")。这场展览的核心观点就是艺术的[虚无性和反](../Page/虚无.md "wikilink")[中产阶级价值观](../Page/中产阶级.md "wikilink")。

### 纽约

在[一战期间](../Page/一战.md "wikilink")，美国[纽约也和](../Page/纽约.md "wikilink")[苏黎世一样成为大批流亡艺术家和作家栖身的场所](../Page/苏黎世.md "wikilink")。从[法国流亡美国的艺术家](../Page/法国.md "wikilink")[马塞尔·杜尚和](../Page/马塞尔·杜尚.md "wikilink")[弗朗西·毕卡比亚结识了美国艺术家](../Page/弗朗西·毕卡比亚.md "wikilink")[曼·雷](../Page/曼·雷.md "wikilink")。1916年，这三人成为[美国](../Page/美国.md "wikilink")“反艺术”运动的核心人物。曾经留学[法国的美国艺术家](../Page/法国.md "wikilink")[比阿特丽斯·伍德后来也加入了这一团体](../Page/比阿特丽斯·伍德.md "wikilink")。

[纽约的艺术家们并没有以](../Page/纽约.md "wikilink")“达达主义者”自居，他们也从未发表过任何宣言或组织过任何政治活动。然而他们却通过自己的出版物对旧的艺术和文化体系进行大肆的炮轰，这些出版物包括《[盲人](../Page/盲人.md "wikilink")》、《纽约达达》等。在这些刊物中，他们将旧的艺术讥称为“博物馆艺术”。

在这段时期内，[杜尚开始以现成品来进行自己的艺术创作](../Page/杜尚.md "wikilink")，并参加了“[独立艺术家社会](../Page/独立艺术家社会.md "wikilink")”组织。1917年，他发表了著名作品《[泉](../Page/泉.md "wikilink")》，是一个写有“R.
Mutt”字样的[小便池](../Page/小便.md "wikilink")。然而这幅作品却被“独立艺术家社会”组织拒绝。

[毕卡比亚对](../Page/毕卡比亚.md "wikilink")[欧洲的出访加强了](../Page/欧洲.md "wikilink")[纽约](../Page/纽约.md "wikilink")、[苏黎世和](../Page/苏黎世.md "wikilink")[巴黎的达达主义组织之间的联系](../Page/巴黎.md "wikilink")。他曾连续七年坚持出版达达主义期刊《391》。这本期刊从1917年至1924年在[纽约](../Page/纽约.md "wikilink")、[苏黎世和](../Page/苏黎世.md "wikilink")[巴黎出版](../Page/巴黎.md "wikilink")。

到1921年，大部分艺术家迁至[巴黎](../Page/巴黎.md "wikilink")。在那里，达达主义迎来了最后一个高峰并走向终结。

### 巴黎

[法国的先锋派艺术家一直和](../Page/法国.md "wikilink")[苏黎世的达达主义者保持着密切的联系](../Page/苏黎世.md "wikilink")。[查拉就和包括](../Page/查拉.md "wikilink")[阿波利奈尔](../Page/阿波利奈尔.md "wikilink")、[布勒东在内的](../Page/布勒东.md "wikilink")[法国作家](../Page/法国.md "wikilink")、评论家和艺术家长期保持通信。

[巴黎的达达主义运动高峰出现在](../Page/巴黎.md "wikilink")1920年。这一年，达达主义运动的很多元老来到[巴黎](../Page/巴黎.md "wikilink")。受[查拉的影响](../Page/查拉.md "wikilink")，巴黎的达达主义者们也发表宣言，组织大规模示威运动，进行舞台表演并出版了大量的刊物。

达达主义的作品第一次出现在[巴黎公众的视野内是在](../Page/巴黎.md "wikilink")1921年的“[独立艺术家沙龙](../Page/独立艺术家沙龙.md "wikilink")”上。[让·克罗蒂和其他达达主义者们展览了自己的作品](../Page/让·克罗蒂.md "wikilink")。

## 达达主义音乐

达达主义并不是一场严格意义上的[视觉艺术或](../Page/视觉艺术.md "wikilink")[文学思潮](../Page/文学.md "wikilink")，它也波及了[音乐和录音领域](../Page/音乐.md "wikilink")。[库尔特·施威特斯和](../Page/库尔特·施威特斯.md "wikilink")[阿尔伯特·萨维尼奥曾撰写](../Page/阿尔伯特·萨维尼奥.md "wikilink")《达达主义音乐》，一个名为“六人组”的乐队也曾在达达主义者的集会上演出过。

## 影响

[缩略图](https://zh.wikipedia.org/wiki/File:Lady_in_Blue.jpg "fig:缩略图")
尽管达达主义在很大范围内得到了传播，但它终究是一个很不稳定的文艺思潮。到1924年，达达主义基本被新生的[超现实主义所吞并](../Page/超现实主义.md "wikilink")，达达主义艺术家们也纷纷投奔其他流派，包括[社会现实主义以及其他](../Page/社会现实主义.md "wikilink")[现代艺术流派](../Page/现代主义.md "wikilink")。

[第二次世界大战前期](../Page/第二次世界大战.md "wikilink")，[欧洲的许多达达主义者再度流亡](../Page/欧洲.md "wikilink")[美国](../Page/美国.md "wikilink")，有一些则死于[希特勒的集中营之中](../Page/希特勒.md "wikilink")，原因是[希特勒不喜欢有颓废色彩的艺术](../Page/希特勒.md "wikilink")。二战之后，许多新的文学和艺术流派纷纷诞生，达达主义的影响更加微弱。

1967年，在[巴黎曾举办了一场大规模的对达达主义的追忆活动](../Page/巴黎.md "wikilink")。

[苏黎世的](../Page/苏黎世.md "wikilink")[伏尔泰酒店在达达主义运动殒灭后曾一度陷入沉寂](../Page/伏尔泰酒店.md "wikilink")，直到2002年，一群自称“新达达主义者”的艺术家们重新在此开始他们的活动。然而两个月后，这群人也逐渐消失。而伏尔泰酒店也被改建成一座[博物馆](../Page/博物馆.md "wikilink")，以纪念达达主义运动的历史。

总体上讲，达达主义并不是一个成熟的文艺流派，而只是一种过渡状态的文艺思维，其艺术理念不具任何建设性，而是建立在对旧秩序的毁灭的基础之上的，因此势必无法长久。但正因为达达主义激进的破旧立新观，20世纪大量的[现代及后现代流派得以催生并长足发展](../Page/现代主义.md "wikilink")。没有达达主义者的努力，这些是很难实现的。

## 代表人物及活动国家

  - [纪尧姆·阿波利奈尔](../Page/纪尧姆·阿波利奈尔.md "wikilink") – 法国
  - [汉斯·阿尔普](../Page/汉斯·阿尔普.md "wikilink") – 瑞士、法国、德国
  - [雨果·巴尔](../Page/雨果·巴尔.md "wikilink") – 瑞士
  - [约翰尼斯·巴德](../Page/约翰尼斯·巴德.md "wikilink") – 德国
  - [阿尔图尔·卡拉凡](../Page/阿尔图尔·卡拉凡.md "wikilink") – 法国
  - [让·克罗蒂](../Page/让·克罗蒂.md "wikilink") – 法国
  - [马塞尔·杜尚](../Page/马塞尔·杜尚.md "wikilink") – 法国、美国
  - [马克思·恩斯特](../Page/马克思·恩斯特.md "wikilink") – 德国
  - [罗尔·豪斯曼](../Page/罗尔·豪斯曼.md "wikilink") – 德国
  - [艾米·翰宁斯](../Page/艾米·翰宁斯.md "wikilink") – 瑞士
  - [理查德·胡森贝克](../Page/理查德·胡森贝克.md "wikilink") – 瑞士、德国
  - [马塞尔·詹可](../Page/马塞尔·詹可.md "wikilink") – 瑞士
  - [克勒蒙·邦撒耶](../Page/克勒蒙·邦撒耶.md "wikilink") – 比利时
  - [弗朗西·毕卡比亚](../Page/弗朗西·毕卡比亚.md "wikilink") – 法国、美国
  - [曼·雷](../Page/曼·雷.md "wikilink") – 法国、美国
  - [汉斯·里克特](../Page/汉斯·里克特.md "wikilink") – 瑞士
  - [库尔特·施威特斯](../Page/库尔特·施威特斯.md "wikilink") – 德国
  - [苏菲·托伯](../Page/苏菲·托伯.md "wikilink") – 瑞士
  - [特利斯坦·查拉](../Page/特利斯坦·查拉.md "wikilink") – 瑞士
  - [比阿特丽斯·伍德](../Page/比阿特丽斯·伍德.md "wikilink") – 法国、美国

## 参考文献

  - 理查德·胡森贝克，《达达主义鼓手的回忆录》，加利福尼亚大学出版社
  - 格雷·马库斯，《唇印》，哈佛大学出版社
  - 艾琳·霍夫曼
    [达达主义与超现实主义](http://www.artic.edu/reynolds/essays/hofmann.php)，
    芝加哥艺术学院

## 参见

  - [现代主义](../Page/现代主义.md "wikilink")
  - [超现实主义](../Page/超现实主义.md "wikilink")

## 外部链接

  - [巴尔于1916年发表的达达主义宣言](http://en.wikisource.org/wiki/Dada_Manifesto_%281916,_Hugo_Ball%29)
  - [查拉的达达主义宣言摘录](http://www.english.upenn.edu/~jenglish/English104/tzara.html)
  - [1918年查拉发表的达达主义宣言](http://www.artseensoho.com/Life/readings/tzara.html)
  - [1921年的达达主义宣言](http://www.ralphmag.org/AR/dada.html)
  - [国际主义的达达艺术家档案](http://www.lib.uiowa.edu/dada/index.html)
  - [2001年的达达主义宣言](https://web.archive.org/web/20050831181456/http://nwlink.com/~phoenix/dada-manifesto-2001.htm)
  - [达达在线](http://www.peak.org/~dadaist/)
      - [达达主义的定义](http://www.peak.org/~dadaist/English/Graphics/index.html)
      - [达达主义运动大事年表](http://www.peak.org/~dadaist/English/Graphics/chronology.html)
      - [达达主义艺术家](http://www.peak.org/~dadaist/English/Graphics/artists.html)
      - [达达主义艺术.](http://www.peak.org/~dadaist/Art/index.html)

[Category:现代主义](../Category/现代主义.md "wikilink")
[Category:艺术运动](../Category/艺术运动.md "wikilink")