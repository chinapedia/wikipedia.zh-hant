[Henry_Steel_Olcott.jpg](https://zh.wikipedia.org/wiki/File:Henry_Steel_Olcott.jpg "fig:Henry_Steel_Olcott.jpg")
**亨利·斯太尔·奥尔科特**，[神智學協會](../Page/神智學協會.md "wikilink")的创始人及首任主席，首位正式转向佛教信仰的欧美人。作为[灵智学会的主席](../Page/灵智学会.md "wikilink")，奥尔科特为佛教的复兴，作了许多工作，他在[斯里兰卡知名度颇高](../Page/斯里兰卡.md "wikilink")。

## 简介

奥尔科特在[美国](../Page/美国.md "wikilink")[新泽西州的农场长大](../Page/新泽西州.md "wikilink")。1860年，与Mary
Epplee
Morgan结婚，她是[新罗徹斯特三位一体教区教区长](../Page/新罗徹斯特.md "wikilink")的女儿，两人育有三子。1858年到1860年，奥尔科特是纽约Tribune的农业版编辑，但有时也向各种主题的报刊投稿。而后在[南北战争中服役](../Page/南北战争.md "wikilink")，接着进入[纽约市法院](../Page/纽约市.md "wikilink")。1863年到1866年，拥有上校军衔的奥尔科特，任美国战争与海军部的特派员。他出版了一本家谱，将自己家族上溯到了托马斯·奥尔科特，这位老奥尔科特是1636年[康乃狄克州](../Page/康乃狄克州.md "wikilink")[哈特福的创建者之一](../Page/哈特福.md "wikilink")。

奥尔科特的部分报道涉及了[通灵人士的活动](../Page/通灵人士.md "wikilink")。1874年，他正着手撰写[佛蒙特州Chittenden小镇](../Page/佛蒙特州.md "wikilink")[埃迪兄弟牵亡的系列文章](../Page/埃迪兄弟.md "wikilink")，在埃迪的农场，奥尔科特遇到了[海倫娜·布拉瓦茨基](../Page/海倫娜·布拉瓦茨基.md "wikilink")。1875年初期，他们都卷入了当时有名的[Katie
King和](../Page/Katie_King.md "wikilink")[John
King](../Page/John_King.md "wikilink")[精神控制的通灵洋相](../Page/精神控制.md "wikilink")。

1875年9月，亨利、海伦娜以及其他人，包括著名的[威廉·關·賈奇创建了神智學協會](../Page/威廉·關·賈奇.md "wikilink")。1878年12月，他们将学会的总部迁往[印度](../Page/印度.md "wikilink")，并安置在[金奈的](../Page/金奈.md "wikilink")[Adyar地区](../Page/Adyar.md "wikilink")。[布拉瓦茨基最后前往](../Page/海倫娜·布拉瓦茨基.md "wikilink")[伦敦居住](../Page/伦敦.md "wikilink")，并在那里去世，而亨利则一直旅居印度，继续学会的工作。灵智学会在Sri
Lanka建造了几所佛教的院校，比较著名的有[Ananda
College](../Page/Ananda_College.md "wikilink")、[Nalanda
College](../Page/Nalanda_College.md "wikilink")、[Dharmaraja
College以及](../Page/Dharmaraja_College.md "wikilink")[Visakha
Vidyalaya](../Page/Visakha_Vidyalaya.md "wikilink")。亨利去世后，学会由[安妮·贝赞特领导](../Page/安妮·贝赞特.md "wikilink")。

后来，[可伦坡的一条主干道](../Page/可伦坡.md "wikilink")，专门被命名为“奥尔科特路”。在Maradana也有他的一座雕像。奥尔科特仍然被许多斯里兰卡民众所怀念，特别是这些学校毕业后，成为了政治、经济界首脑的人。

## 注释

1.  奥尔科特，即Olcott，也被译作奥葛特。

2.  神智學協會，即Theosophical Society，也译作神智会、通神学会等。

3.  三位一体教区教区长，原文是the rector of Trinity parish

## 著作

1.  *Buddhist Catechism*, 1881
2.  *Theosophy, Religion, and Occult Science*, 1885
3.  *Old Diary Leaves* (6 volumes)
4.  *Descendents of Thomas Olcott*, 1872

## 引用

### 书籍

1.  "Priestess of the Occult" by Gertrude Williams; Alfred Knopf (1946)

### 网页

1.  关于奥尔科特的文章，<http://www.theosophy-nw.org/theosnw/theos/wqj-selc.htm#olcott>
2.  奥尔科特写的文章，<https://web.archive.org/web/20051001021751/http://www.theosophical.ca/OnLineDocs.htm>
3.  美国佛教史话，<https://web.archive.org/web/20060514221113/http://www.ebud.net/teach/special/history/teach_special_history_20020302_9.html>

[Category:美國佛教徒](../Category/美國佛教徒.md "wikilink")
[Category:上座部佛教徒](../Category/上座部佛教徒.md "wikilink")
[Category:北军军官](../Category/北军军官.md "wikilink")
[Category:南北戰爭人物](../Category/南北戰爭人物.md "wikilink")
[Category:新澤西州人](../Category/新澤西州人.md "wikilink")