**己内酰胺**（Caprolactam，簡稱CPL），化学式为(CH<sub>2</sub>)<sub>5</sub>C(O)NH的[有机化合物](../Page/有机化合物.md "wikilink")，是6-（ε-氨基己酸）的[内酰胺](../Page/内酰胺.md "wikilink")，也可看作[己酸的环状](../Page/己酸.md "wikilink")[酰胺](../Page/酰胺.md "wikilink")。纯净的己内酰胺是白色的固体。其年产量大约45万吨。

己内酰胺主要用作制取[聚合物](../Page/聚合物.md "wikilink")[尼龙6的](../Page/尼龙6.md "wikilink")[单体](../Page/单体.md "wikilink")。\[1\]

## 生产

己内酰胺最早于19世纪后期通过ε-氨基己酸（己内酰胺的水解产物）的成环反应来制备。由于尼龙-6含有较大的商业价值，现在已有许多可用于生产己内酰胺的方法。

现在己内酰胺可由[环己酮与](../Page/环己酮.md "wikilink")[硫酸羟胺发生成](../Page/硫酸羟胺.md "wikilink")[肟反应生成](../Page/肟.md "wikilink")，再经酸处理发生[贝克曼重排反应制得](../Page/贝克曼重排.md "wikilink")：

  -
    [Beckmann_rearrangement.png](https://zh.wikipedia.org/wiki/File:Beckmann_rearrangement.png "fig:Beckmann_rearrangement.png")

重排的直接产物是己内酰胺的硫酸氢盐，一般需要用[氨中和以得到游离的己内酰胺](../Page/氨.md "wikilink")，同时生成[硫酸铵](../Page/硫酸铵.md "wikilink")。

另一个主要的工业制备方法是通过[亚硝酰氯使](../Page/亚硝酰氯.md "wikilink")[环己烷形成肟](../Page/环己烷.md "wikilink")。该方法的优点是环己烷比环己酮价格更低廉。在较早的时代，也有通过用氨处理己内酯制备己内酰胺的方法。\[2\]

环己酮在[叠氮酸在有](../Page/叠氮酸.md "wikilink")[硫酸或](../Page/硫酸.md "wikilink")[路易斯酸作催化剂的情况下反应的制备方法也曾得到报道](../Page/路易斯酸.md "wikilink")。\[3\]

  -
    [Schmidt_ring_expansion.png](https://zh.wikipedia.org/wiki/File:Schmidt_ring_expansion.png "fig:Schmidt_ring_expansion.png")

## 使用

几乎生产的所有己内酰胺都用于生产[尼龙6](../Page/尼龙6.md "wikilink")。该反应是一个开环[聚合反应](../Page/聚合.md "wikilink")：

  -
    *n* (CH<sub>2</sub>)<sub>5</sub>C(O)NH →
    \[(CH<sub>2</sub>)<sub>5</sub>C(O)NH\]<sub>*n*</sub>

生产得到的尼龙-6广泛使用于[纤维和](../Page/纤维.md "wikilink")[塑料制品](../Page/塑料.md "wikilink")。

## 安全

己内酰胺具有刺激性，摄取、吸入或皮肤接触有毒，为1.1
g/kg（大鼠，口服）。1991年，己内酰胺被列入1990年“”的有害空气污染物清单，但于1996年被移除出名单。\[4\]

在[国际癌症研究机构对](../Page/国际癌症研究机构.md "wikilink")[致癌物质的分类中](../Page/致癌物质.md "wikilink")，己内酰胺被归为第4类致癌物“对人体基本无致癌作用”分类的唯一物质。\[5\]

目前，美国对于处理己内酰胺的工人暂未正式规定允许的接触限值。对于己内酰胺[粉尘和](../Page/粉尘.md "wikilink")[蒸气](../Page/蒸气.md "wikilink")，建议的长时间接触限值为1
mg/m<sup>3</sup>，短期接触限值为3 mg/m<sup>3</sup>。\[6\]

## 参见

  -
  - [环己酮](../Page/环己酮.md "wikilink")

  - [尼龙6](../Page/尼龙6.md "wikilink")

## 参考资料

[Category:内酰胺](../Category/内酰胺.md "wikilink")
[Category:单体](../Category/单体.md "wikilink")
[Category:高哌啶](../Category/高哌啶.md "wikilink")

1.

2.
3.

4.  [EPA - Modifications To The 112(b)1 Hazardous Air
    Pollutants](http://www.epa.gov/ttn/atw/pollutants/atwsmod.html)

5.

6.