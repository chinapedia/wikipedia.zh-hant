**龙与地下城Online**（[英文](../Page/英文.md "wikilink")：Dungeons & Dragons
Online:
Stormreach，缩写：DDO）是一款基于[龙与地下城规则](../Page/龙与地下城.md "wikilink")3.5版的一个[大型多人在线角色扮演游戏](../Page/大型多人在线角色扮演游戏.md "wikilink")。游戏由[美国](../Page/美国.md "wikilink")[Turbine公司开发](../Page/Turbine公司.md "wikilink")，在与现在掌握龙与地下城版权的[威世智经过约两年的合作后推出](../Page/威世智.md "wikilink")，游戏于2006年2月28日开始运行，由[雅达利发行](../Page/雅达利.md "wikilink")。在[中国大陆](../Page/中国大陆.md "wikilink")，游戏由[盛大代理](../Page/盛大网络.md "wikilink")，于2006年6月28日及7月28日分别开始第一及第二次内部测试，并于2006年8月28日开始进行公开测试。游戏于2006年10月10日荣获[英国电影和电视艺术学院](../Page/英国电影和电视艺术学院.md "wikilink")（BAFTA）最佳多人游戏奖。

## 游戏世界设定

龙与地下城Online基于龙与地下城规则3.5版，并稍做改动，游戏场景设定于[艾伯伦世界](../Page/艾伯伦.md "wikilink")（Eberron）的泽恩德瑞克大陆（Xen'drik
）北部的风暴湾附近。目前游戏开放的地图主要包括冒险者渡口（即俗称的新手岛），海港区，市场区及菲奥兰家族（House
Phiarlan）、德尼斯家族（House Deneith）、乔拉斯科家族（House
Jorasco）和昆达拉克家族（House
Kundarak）四个[龙纹家族](../Page/龙纹.md "wikilink")。此外，还有一些任务发生在上述地区附近的[岛屿或](../Page/岛屿.md "wikilink")[海湾等](../Page/海湾.md "wikilink")，如三桶湾、哀暮岛、陨梦岛等。

目前龙与地下城Online开放有6個種族、9種職業和6個陣營可供選擇

种族：

  - [人类](../Page/人类.md "wikilink")
  - [精灵](../Page/精灵.md "wikilink")
  - [半身人](../Page/半身人.md "wikilink")
  - [矮人](../Page/矮人.md "wikilink")
  - [机关人](../Page/机关人.md "wikilink")
  - [卓尔](../Page/卓尔.md "wikilink")

职业：

  - [野蠻人](../Page/野蠻人.md "wikilink")
  - [戰士](../Page/戰士.md "wikilink")
  - [聖騎士](../Page/聖騎士.md "wikilink")
  - [遊俠](../Page/遊俠.md "wikilink")
  - [牧師](../Page/牧師\(奇幻文學\).md "wikilink")
  - [盜賊](../Page/盜賊.md "wikilink")
  - [法師](../Page/法師.md "wikilink")
  - [術士](../Page/術士.md "wikilink")
  - [吟遊詩人](../Page/吟遊詩人.md "wikilink")

阵营(未開放邪惡陣營)：

  - 守序善良
  - 中立善良
  - 混乱善良
  - 守序中立
  - 绝对中立
  - 混乱中立

游戏尚未开放领域、动物伙伴、晋阶职业等设定，技能种类的开放也有限。

## 玩法

龙与地下城Online是一款具有明显动作要素的MMORPG，因此原来在桌面上由玩家自己掷[骰子改为了电脑自动投骰子来决定行动的效果](../Page/骰子.md "wikilink")，并且默认在游戏画面的右下角及战斗记录中显示结果。而地下城主的职责则是游戏预设，在适当的情况下以[文字或语音的形式加以提示](../Page/文字.md "wikilink")。在德里拉墓系列任务中，地下城主由龙与地下城的创始人[加里·吉盖克斯](../Page/加里·吉盖克斯.md "wikilink")（Gary
Gygax）配音。

龙与地下城Online与很多常见的MMORPG相同，靠获得经验值来提升角色等级，但获得经验值的主要方式是完成副本任务，只有极少数的情况下可以靠打败怪物获得微量经验值。游戏注重团队合作，绝大多数任务都能组队进行，一个小队最多可以有六名成员，而个别大型任务副本则可以团队形式（raid）进行，一个团队人数上限为十二。

游戏除了可以靠文字交流，还内置语音交流系统，在一个小队或者团队中，成员可以用语言交流。

游戏者可以从不同的非玩家角色（NPC）接受任务，大多数任务分为单人、正常、困难及精英四个难度，个别任务只能单人进行或者只能以队伍形式进行。

## 游戏中提及的神祇、宗教、团体

游戏中提及的各种[神祇](../Page/神祇.md "wikilink")、[宗教](../Page/宗教.md "wikilink")、团体等均来自于艾伯伦世界的设定。

  - **银焰教会**
  - **天命诸神**
  - **黑暗六邪神**
  - **沃尔血族**

此外，来自邪恶位面[佐瑞艾特及地下世界的](../Page/佐瑞艾特.md "wikilink")[黄泉巨龙信徒也在不断侵扰风暴湾](../Page/黄泉巨龙.md "wikilink")。

## 中国的运营

2008年7月16日盛大宣布，由于中国运营版权使用到期，将于2008年7月31日关闭所有游戏服务器，正式停止《龙与地下城》在中国的运营。[1](http://www.gamespot.com.cn/news/2008/0716/90128.shtml)

## 相關条目

  - [支持DirectX 10游戏列表](../Page/支持DirectX_10游戏列表.md "wikilink")

## 外部链接

  - [Dungeons & Dragons Online: Stormreach](http://www.ddo.com/)
  - [龙与地下城Online](http://www.ddo.com.cn/)
  - [艾伯伦碎片](http://www.ebecn.com/)
  - [封神公會遊戲網 - 龍與地下城網路版(Dungeons & Dragons
    Online)](http://fongshen.net/viewforum.php?f=99)

[D](../Category/龙与地下城电子游戏.md "wikilink")
[D](../Category/2006年电子游戏.md "wikilink")
[D](../Category/大型多人在线角色扮演游戏.md "wikilink")
[Category:雅达利游戏](../Category/雅达利游戏.md "wikilink")
[D](../Category/Codemasters遊戲.md "wikilink")
[Category:Windows游戏](../Category/Windows游戏.md "wikilink")
[Category:盛大游戏](../Category/盛大游戏.md "wikilink")