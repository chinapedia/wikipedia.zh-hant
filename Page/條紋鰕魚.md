**條紋蝦魚**，俗名為甲香魚、玻璃魚、刀片魚，為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[棘背魚目](../Page/棘背魚目.md "wikilink")[海龍亞目](../Page/海龍亞目.md "wikilink")[蝦魚科的其中一](../Page/蝦魚科.md "wikilink")[種](../Page/種.md "wikilink")。

## 分布

本於分布於[印度](../Page/印度.md "wikilink")[太平洋海域](../Page/太平洋.md "wikilink")，西起[非洲東部](../Page/非洲.md "wikilink")、北起[日本](../Page/日本.md "wikilink")、南至[澳洲等海域](../Page/澳洲.md "wikilink")。
[Aeoliscus_strigatus_Prague_2011_1.jpg](https://zh.wikipedia.org/wiki/File:Aeoliscus_strigatus_Prague_2011_1.jpg "fig:Aeoliscus_strigatus_Prague_2011_1.jpg")

## 深度

水深1\~25公尺。

## 特徵

本魚體側扁近透明，體外由脊柱擴展而成之薄骨枝，分節但密接；腹緣銳利如刀鋒。吻延長為管狀，無側線，體側有一條顯著之黑色縱帶。背鰭的第一硬棘看起來像是尾部，其他硬棘、第二背鰭、尾鰭都因而擠在腹面朝下方。本魚與[玻甲魚類似](../Page/玻甲魚.md "wikilink")，但其體型更短狀但腹鰭位置偏後。體長可達15公分。
[Aeoliscus_strigatus_Prague_2011_2.jpg](https://zh.wikipedia.org/wiki/File:Aeoliscus_strigatus_Prague_2011_2.jpg "fig:Aeoliscus_strigatus_Prague_2011_2.jpg")

## 生態

本魚常成群或單尾頭下尾上倒立停於水中，或插入[海膽棘或珊瑚叢尋求保護](../Page/海膽.md "wikilink")。游泳時可豎著游也可以橫著正常地頭部向前游，以其長吻吸食[浮游生物](../Page/浮游生物.md "wikilink")。

## 經濟利用

非[食用魚](../Page/食用魚.md "wikilink")，多作為[觀賞魚](../Page/觀賞魚.md "wikilink")。

## 參考資料

[台灣魚類資料庫](http://fishdb.sinica.edu.tw/)

[strigatus](../Category/鰕魚屬.md "wikilink")