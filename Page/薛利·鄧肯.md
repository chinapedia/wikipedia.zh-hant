**薛利·鄧肯**（**Shelley
Duncan**；）為[美國的棒球選手之一](../Page/美國.md "wikilink")，現為[坦帕灣魔鬼魚](../Page/坦帕灣魔鬼魚.md "wikilink")
小聯盟的外野手。。

## 職棒生涯

  - 2008年4月9日被送至[小聯盟](../Page/小聯盟.md "wikilink")。
  - 2008年4月26日被叫上[大聯盟](../Page/大聯盟.md "wikilink")。
  - 2008年6月13日再度被送至[小聯盟](../Page/小聯盟.md "wikilink")。
  - 2009年1月6日被[指定讓渡](../Page/指定讓渡.md "wikilink")，移出40人名單。
  - 2009年7月31日被[Purchased](../Page/Purchased.md "wikilink")，再度進入40人名單。
  - 2009年被送至[小聯盟](../Page/小聯盟.md "wikilink")。球季結束以後，因拒絕[紐約洋基將其下放至小聯盟](../Page/紐約洋基.md "wikilink")，而成為自由球員\[1\]。
  - 2010年1月4日，鄧肯和[克里夫蘭印地安人簽下](../Page/克里夫蘭印地安人.md "wikilink")1年的小聯盟合約\[2\]。
  - 2010年5月19日，因[格雷迪·塞斯摩爾受傷進入傷兵名單](../Page/格雷迪·塞斯摩爾.md "wikilink")，而被升上大聯盟。

## 參考資料

## 外部連結

  -
[Category:紐約洋基隊球員](../Category/紐約洋基隊球員.md "wikilink")
[Category:克里夫蘭印地安人隊球員](../Category/克里夫蘭印地安人隊球員.md "wikilink")
[Category:美國棒球選手](../Category/美國棒球選手.md "wikilink")

1.  <http://yankees.lhblogs.com/2009/11/20/shelley-not-coming-back-to-yankees/>
2.  <http://yankees.lhblogs.com/2010/01/04/shelley-duncan-signs-with-the-indians/>