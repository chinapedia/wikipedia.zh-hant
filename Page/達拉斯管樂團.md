**達拉斯管樂團**（[英文](../Page/英文.md "wikilink")：**Dallas Wind
Symphony**，縮寫DWS），[美國](../Page/美國.md "wikilink")[德克薩斯州](../Page/德克薩斯州.md "wikilink")[達拉斯市的一個職業](../Page/達拉斯.md "wikilink")[管樂團](../Page/管樂團.md "wikilink")。

## 概要

達拉斯管樂團由[金姆·坎貝爾](../Page/金姆·坎貝爾.md "wikilink")（[Kim J.
Campbell](../Page/:en:Kim_J._Campbell.md "wikilink")）\[1\]和[南方衛理公會大學音樂教授](../Page/南方衛理公會大學.md "wikilink")[侯活·鄧恩](../Page/侯活·鄧恩.md "wikilink")（[Howard
Dunn](../Page/:en:Howard_Dunn.md "wikilink")）\[2\]於1985年所創立。它是由當地音樂家（大部份是音樂教師以及樂團指揮）所組成，並且是在學期中練習，就如同他們在學時的時候練習管樂合奏。

在1990年2月6日，達拉斯管樂團在[達拉斯](../Page/達拉斯.md "wikilink")[麥耶生交響樂中心](../Page/麥耶生交響樂中心.md "wikilink")（[Morton
H. Meyerson Symphony
Center](../Page/:en:Morton_H._Meyerson_Symphony_Center.md "wikilink")）首次登場。達拉斯管樂團第一個樂季是在[南方衛理公會大學](../Page/南方衛理公會大學.md "wikilink")（Southern
Methodist University）的[卡魯夫堂](../Page/卡魯夫堂.md "wikilink")（Caruth
Auditorium）開始，

[芬奈爾](../Page/芬奈爾.md "wikilink")（Frederick
Fennell）從1990年代中期擔任過達拉斯管樂團的客席指揮，直到他2004年去世。

1991年，[侯活·鄧恩](../Page/侯活·鄧恩.md "wikilink")（[Howard
Dunn](../Page/:en:Howard_Dunn.md "wikilink")）教授去世，達拉斯管樂團積極的找尋新的藝術總監。在1993年，[德克薩斯州大學奧斯汀分校](../Page/德克薩斯州大學奧斯汀分校.md "wikilink")（[University
of Texas at
Austin](../Page/:en:University_of_Texas_at_Austin.md "wikilink")）的樂團指揮[傑利·鍾健](../Page/傑利·鍾健.md "wikilink")（[Jerry
Junkin](../Page/:en:Jerry_Junkin.md "wikilink")）被任命為達拉斯管樂團藝術總監及指揮。

達拉斯管樂團在1991年錄製了12張唱片，演奏極為精確，勘稱為示範錄音，其中10張被製成[高清晰度唱片](../Page/高清晰度唱片.md "wikilink")（[HDCD](../Page/:en:High_Definition_Compatible_Digital.md "wikilink")），樂團的《Trittico》和《Testament》唱片是當時世上第一批[高清晰度唱片](../Page/高清晰度唱片.md "wikilink")。

達拉斯管樂團所演奏的片段時常在[美國](../Page/美國.md "wikilink")[國家公眾電台的](../Page/National_Public_Radio.md "wikilink")《[今日演奏](../Page/今日演奏.md "wikilink")》（[Performance
Today](../Page/:en:Performance_Today.md "wikilink")）節目中聽到。

達拉斯管樂團與[東京佼成管樂團也被視為世界上知名的管樂團](../Page/東京佼成管樂團.md "wikilink")。

## 參考文獻

## 外部連結

  - [達拉斯管樂團官方網頁](http://www.dws.org)

[Category:管樂團](../Category/管樂團.md "wikilink")
[Category:美國管樂團](../Category/美國管樂團.md "wikilink")

1.  [Dallas Museum of Art Presents State of the Arts: Kim J. Campbell,
    Frances Bagley, and Tom
    Orr:](https://dfw.daybooknetwork.com/story/2009-12-16/27981-state-of-the-art-lecture)
2.  [Phi Beta Mu - Alpha
    Chapter:](http://www.pbmalpha.org/pbmhalloffamebio.php?HOF_Number=057)