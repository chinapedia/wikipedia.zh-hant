[Eight_regions_of_Japan_(zh-hant).svg](https://zh.wikipedia.org/wiki/File:Eight_regions_of_Japan_\(zh-hant\).svg "fig:Eight_regions_of_Japan_(zh-hant).svg")及[釣魚臺列嶼](../Page/釣魚臺列嶼.md "wikilink")）\]\]

**日本地理區劃**描述了[日本國內基於歷史人文與自然地理等因素所劃分成的地方或地區概念](../Page/日本.md "wikilink")。對於非日本當地的居民來說，一般較為熟悉的是根據地理上的概念，將日本分為[日本列島的四個大島及](../Page/日本列島.md "wikilink")[琉球群島這般的分法](../Page/琉球群島.md "wikilink")。但是對於日本當地居民來說，卻較常使用一被稱為「八地方區分」的劃分概念，依照各地因歷史、文化、經濟發展、交通建設等的不同而逐漸形成的當地居民意識，將日本全國劃分為[北海道](../Page/北海道.md "wikilink")、[東北](../Page/東北地方.md "wikilink")、[關東](../Page/關東地方.md "wikilink")、[中部](../Page/中部地方.md "wikilink")、[近畿](../Page/近畿地方.md "wikilink")、[中國](../Page/中國地方.md "wikilink")、[四國](../Page/四國.md "wikilink")、[九州等](../Page/九州_\(日本\).md "wikilink")8個「地方」\[1\]。

## 地方、地域名一覽

| 地方                                   | 地圖                                                                                                                                                                | 面積            | 人口（2017年）  | 人口密度       | 最大城市                                 |
| ------------------------------------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------- | ---------- | ---------- | ------------------------------------ |
| [北海道地方](../Page/北海道地方.md "wikilink") | [Japan_Hokkaido_large.png](https://zh.wikipedia.org/wiki/File:Japan_Hokkaido_large.png "fig:Japan_Hokkaido_large.png")                                          | 83,457.48 km² | 5,348,102  | 64.1/km²   | [札幌市](../Page/札幌市.md "wikilink")     |
| [东北地方](../Page/东北地方.md "wikilink")   | [Japan_Tohoku_Region_large.png](https://zh.wikipedia.org/wiki/File:Japan_Tohoku_Region_large.png "fig:Japan_Tohoku_Region_large.png")                          | 66,951.97 km² | 8,851,238  | 132.2/km²  | [仙台市](../Page/仙台市.md "wikilink")     |
| [關東地方](../Page/關東地方.md "wikilink")   | [Japan_Kanto_Region_large.png](https://zh.wikipedia.org/wiki/File:Japan_Kanto_Region_large.png "fig:Japan_Kanto_Region_large.png")                             | 32,423.90 km² | 42,598,300 | 1313.8/km² | [東京都區部](../Page/東京都區部.md "wikilink") |
| [中部地方](../Page/中部地方.md "wikilink")   | [Japan_Chubu_Region_large.png](https://zh.wikipedia.org/wiki/File:Japan_Chubu_Region_large.png "fig:Japan_Chubu_Region_large.png")                             | 72,572.34 km² | 23,575,736 | 324.9/km²  | [名古屋市](../Page/名古屋市.md "wikilink")   |
| [近畿地方](../Page/近畿地方.md "wikilink")   | [Japan_Kinki_Region_large.png](https://zh.wikipedia.org/wiki/File:Japan_Kinki_Region_large.png "fig:Japan_Kinki_Region_large.png")                             | 33,112.42 km² | 22,551,434 | 681.1/km²  | [大阪市](../Page/大阪市.md "wikilink")     |
| [中國地方](../Page/中國地方.md "wikilink")   | [Japan_Chugoku_Region_large.png](https://zh.wikipedia.org/wiki/File:Japan_Chugoku_Region_large.png "fig:Japan_Chugoku_Region_large.png")                       | 31,921.65 km² | 7,380,254  | 231.2/km²  | [廣島市](../Page/廣島市.md "wikilink")     |
| [四國地方](../Page/四國地方.md "wikilink")   | [Japan_Shikoku_Region_large.png](https://zh.wikipedia.org/wiki/File:Japan_Shikoku_Region_large.png "fig:Japan_Shikoku_Region_large.png")                       | 18,803.41 km² | 3,794,792  | 201.8/km²  | [松山市](../Page/松山市.md "wikilink")     |
| [九州地方](../Page/九州地方.md "wikilink")   | [Japan_Kyushu_Okinawa_Region_large.png](https://zh.wikipedia.org/wiki/File:Japan_Kyushu_Okinawa_Region_large.png "fig:Japan_Kyushu_Okinawa_Region_large.png") | 44,512.60 km² | 14,361,528 | 322.9/km²  | [福岡市](../Page/福岡市.md "wikilink")     |

### [北海道](../Page/北海道.md "wikilink")

  - 北海道地方：在過去曾一度被劃分為3個縣（[函館縣](../Page/函館縣.md "wikilink")、[札幌縣](../Page/札幌縣.md "wikilink")、[根室縣](../Page/根室縣.md "wikilink")），但在1886年設置[北海道廳時同步廢縣](../Page/北海道廳_\(1886年-1947年\).md "wikilink")。
      - [道北](../Page/道北.md "wikilink"):
          - [宗谷支廳](../Page/宗谷支廳.md "wikilink") -
            [稚內市](../Page/稚內市.md "wikilink")
          - [留萌支廳](../Page/留萌支廳.md "wikilink") -
            [留萌市](../Page/留萌市.md "wikilink")
          - [上川支廳](../Page/上川支廳.md "wikilink") -
            [旭川市](../Page/旭川市.md "wikilink")
      - [道東](../Page/道東.md "wikilink"):
          - [網走支廳](../Page/網走支廳.md "wikilink") -
            [網走市](../Page/網走市.md "wikilink")
          - [根室支廳](../Page/根室支廳.md "wikilink") -
            [根室市](../Page/根室市.md "wikilink")
          - [釧路支廳](../Page/釧路支廳.md "wikilink") -
            [釧路市](../Page/釧路市.md "wikilink")
          - [十勝支廳](../Page/十勝支廳.md "wikilink") -
            [帶廣市](../Page/帶廣市.md "wikilink")
      - [道央](../Page/道央.md "wikilink"):
          - [空知支廳](../Page/空知支廳.md "wikilink") -
            [岩見澤市](../Page/岩見澤市.md "wikilink")
          - [石狩支廳](../Page/石狩支廳.md "wikilink") -
            [札幌市](../Page/札幌市.md "wikilink")
          - [後志支廳](../Page/後志支廳.md "wikilink") -
            [虻田郡](../Page/虻田郡.md "wikilink")[俱知安町](../Page/俱知安町.md "wikilink")
          - [胆振支廳](../Page/胆振支廳.md "wikilink") -
            [室蘭市](../Page/室蘭市.md "wikilink")
          - [日高支廳](../Page/日高支廳.md "wikilink") -
            [浦河郡](../Page/浦河郡.md "wikilink")[浦河町](../Page/浦河町.md "wikilink")
      - [道南](../Page/道南.md "wikilink"):
          - [渡島支廳](../Page/渡島支廳.md "wikilink") -
            [函館市](../Page/函館市.md "wikilink")
          - [檜山支廳](../Page/檜山支廳.md "wikilink") -
            [檜山郡](../Page/檜山郡.md "wikilink")[江差町](../Page/江差町.md "wikilink")

### [本州](../Page/本州.md "wikilink")

本州島是日本列島中最大的島嶼，其上包含了習慣上被稱為東北、關東、中部、近畿與中國的五個地方，包括東京與大阪在內等日本最重要的都會區皆位於本州島上。

  - [東北地方](../Page/東北地方.md "wikilink")：
      - [北東北](../Page/北東北.md "wikilink")：
          - [青森縣](../Page/青森縣.md "wikilink") -
            [青森市](../Page/青森市.md "wikilink")
          - [岩手縣](../Page/岩手縣.md "wikilink") -
            [盛岡市](../Page/盛岡市.md "wikilink")
          - [秋田縣](../Page/秋田縣.md "wikilink") -
            [秋田市](../Page/秋田市.md "wikilink")
      - [南東北](../Page/南東北.md "wikilink")：
          - [宮城縣](../Page/宮城縣.md "wikilink") -
            [仙台市](../Page/仙台市.md "wikilink")
          - [山形縣](../Page/山形縣.md "wikilink") -
            [山形市](../Page/山形市.md "wikilink")
          - [福島縣](../Page/福島縣.md "wikilink") -
            [福島市](../Page/福島市.md "wikilink")
  - [關東地方](../Page/關東地方.md "wikilink")：
      - [北關東](../Page/北關東.md "wikilink")：
          - [茨城縣](../Page/茨城縣.md "wikilink") -
            [水戶市](../Page/水戶市.md "wikilink")
          - [栃木縣](../Page/栃木縣.md "wikilink") -
            [宇都宮市](../Page/宇都宮市.md "wikilink")
          - [群馬縣](../Page/群馬縣.md "wikilink") -
            [前橋市](../Page/前橋市.md "wikilink")
      - [南關東](../Page/南關東.md "wikilink")：
          - [埼玉縣](../Page/埼玉縣.md "wikilink") -
            [埼玉市](../Page/埼玉市.md "wikilink")
          - [千葉縣](../Page/千葉縣.md "wikilink") -
            [千葉市](../Page/千葉市.md "wikilink")
          - [東京都](../Page/東京都.md "wikilink") -
            [東京特別區](../Page/東京特別區.md "wikilink")（[新宿區](../Page/新宿區.md "wikilink")）
          - [神奈川縣](../Page/神奈川縣.md "wikilink") -
            [横濱市](../Page/横濱市.md "wikilink")、[川崎市](../Page/川崎市.md "wikilink")
  - [中部地方](../Page/中部地方.md "wikilink")：
      - [甲信越地方](../Page/甲信越地方.md "wikilink")：
          - [山梨縣](../Page/山梨縣.md "wikilink") -
            [甲府市](../Page/甲府市.md "wikilink")
          - [長野縣](../Page/長野縣.md "wikilink") -
            [長野市](../Page/長野市.md "wikilink")
          - [新潟縣](../Page/新潟縣.md "wikilink") -
            [新潟市](../Page/新潟市.md "wikilink")
      - [北陸地方](../Page/北陸地方.md "wikilink")：
          - [富山縣](../Page/富山縣.md "wikilink") -
            [富山市](../Page/富山市.md "wikilink")
          - [石川縣](../Page/石川縣.md "wikilink") -
            [金澤市](../Page/金澤市.md "wikilink")
          - [福井縣](../Page/福井縣.md "wikilink") -
            [福井市](../Page/福井市.md "wikilink")
      - [東海地方](../Page/東海地方.md "wikilink")：
          - [静岡縣](../Page/静岡縣.md "wikilink") -
            [靜岡市](../Page/靜岡市.md "wikilink")、[濱松市](../Page/濱松市.md "wikilink")
          - [愛知縣](../Page/愛知縣.md "wikilink") -
            [名古屋市](../Page/名古屋市.md "wikilink")
          - [岐阜縣](../Page/岐阜縣.md "wikilink") -
            [岐阜市](../Page/岐阜市.md "wikilink")
  - [近畿地方](../Page/近畿地方.md "wikilink")：
      - [滋賀縣](../Page/滋賀縣.md "wikilink") -
        [大津市](../Page/大津市.md "wikilink")
      - [京都府](../Page/京都府.md "wikilink") -
        [京都市](../Page/京都市.md "wikilink")
      - [大阪府](../Page/大阪府.md "wikilink") -
        [大阪市](../Page/大阪市.md "wikilink")、[堺市](../Page/堺市.md "wikilink")
      - [兵庫縣](../Page/兵庫縣.md "wikilink") -
        [神戶市](../Page/神戶市.md "wikilink")
      - [奈良縣](../Page/奈良縣.md "wikilink") -
        [奈良市](../Page/奈良市.md "wikilink")
      - [三重縣](../Page/三重縣.md "wikilink") -
        [津市](../Page/津市_\(日本\).md "wikilink")
      - [和歌山縣](../Page/和歌山縣.md "wikilink") -
        [和歌山市](../Page/和歌山市.md "wikilink")
  - [中國地方](../Page/中國地方.md "wikilink")
      - [山陰地方](../Page/山陰地方.md "wikilink")：
          - [鳥取縣](../Page/鳥取縣.md "wikilink") -
            [鳥取市](../Page/鳥取市.md "wikilink")
          - [島根縣](../Page/島根縣.md "wikilink") -
            [松江市](../Page/松江市.md "wikilink")
      - [山陽地方](../Page/山陽地方.md "wikilink")：
          - [岡山縣](../Page/岡山縣.md "wikilink") -
            [岡山市](../Page/岡山市.md "wikilink")
          - [廣島縣](../Page/廣島縣.md "wikilink") -
            [廣島市](../Page/廣島市.md "wikilink")
          - [山口縣](../Page/山口縣.md "wikilink") -
            [山口市](../Page/山口市.md "wikilink")

### [四國](../Page/四國.md "wikilink")

  - 四國地方：
      - [德島縣](../Page/德島縣.md "wikilink") -
        [德島市](../Page/德島市.md "wikilink")
      - [香川縣](../Page/香川縣.md "wikilink") -
        [高松市](../Page/高松市.md "wikilink")
      - [愛媛縣](../Page/愛媛縣.md "wikilink") -
        [松山市](../Page/松山市.md "wikilink")
      - [高知縣](../Page/高知縣.md "wikilink") -
        [高知市](../Page/高知市.md "wikilink")

### [九州](../Page/九州.md "wikilink")

九州島是日本列島四大島中位置最南的一座。另外，[琉球群島雖然在地理上被視為獨立的群島](../Page/琉球群島.md "wikilink")（全境分屬鹿兒島縣及沖繩縣管轄），但在日本的地方概念中通常被視為是九州地方的一部份。此外，有將九州和沖繩縣合稱為「九州・沖繩地方」的稱呼法，但是定義等同於「九州地方」。

  - 九州地方：
      - [福岡縣](../Page/福岡縣.md "wikilink") -
        [福岡市](../Page/福岡市.md "wikilink")、[北九州市](../Page/北九州市.md "wikilink")
      - [佐賀縣](../Page/佐賀縣.md "wikilink") -
        [佐賀市](../Page/佐賀市.md "wikilink")
      - [長崎縣](../Page/長崎縣.md "wikilink") -
        [長崎市](../Page/長崎市.md "wikilink")
      - [熊本縣](../Page/熊本縣.md "wikilink") -
        [熊本市](../Page/熊本市.md "wikilink")
      - [大分縣](../Page/大分縣.md "wikilink") -
        [大分市](../Page/大分市.md "wikilink")
      - [宮崎縣](../Page/宮崎縣.md "wikilink") -
        [宮崎市](../Page/宮崎市.md "wikilink")
      - [鹿兒島縣](../Page/鹿兒島縣.md "wikilink") -
        [鹿兒島市](../Page/鹿兒島市.md "wikilink")
      - [沖繩縣](../Page/沖繩縣.md "wikilink") -
        [那霸市](../Page/那霸市.md "wikilink")

## 「八地方區分」以外的劃分方法

  - 二區分

<!-- end list -->

  - [東日本與](../Page/東日本.md "wikilink")[西日本](../Page/西日本.md "wikilink")：根據過去日本的中央政權曾經位於不同區域的歷史淵源，而將日本（尤其是本州島）分為以東京與大阪為首的東西兩個主要地區。
  - [太平洋側與](../Page/太平洋.md "wikilink")[日本海側](../Page/日本海.md "wikilink")：根據[氣候與](../Page/氣候.md "wikilink")[海運的特性不同來劃分日本](../Page/海運.md "wikilink")。
  - [表日本與](../Page/表日本.md "wikilink")[裏日本](../Page/裏日本.md "wikilink")：根據日本在高度經濟成長的時代，各地區佔國家經濟發展比重的不同而劃分的概念。一般來說「表日本」的範圍涵蓋本州島太平洋岸一側，而「裏日本」則大致與日本海側的日本同義，涵蓋本州島大部分的日本海一側地區。

<!-- end list -->

  - 三區分

<!-- end list -->

  - 東日本、[中日本](../Page/中日本.md "wikilink")、西日本：根據交通特性與[三大都市圈為核心的經濟圈分佈不同來劃分](../Page/日本三大都市圈.md "wikilink")。
  - [北日本](../Page/北日本.md "wikilink")、東日本、西日本：主要是[氣象預報用的劃分方式](../Page/氣象預報.md "wikilink")，其中北日本包含北海道與東北，東日本包括了關東與包含三重縣在內的中部地方，西日本則指不包含三重縣在內的近畿與九州地方，但不包含沖繩與[奄美等地區](../Page/奄美群島.md "wikilink")。

<!-- end list -->

  - 其他輔助用法

通常是以日本國內某個地區為中心來看待其他區域的劃分方式，屬於較不綜觀性的分類。

  - [北日本](../Page/北日本.md "wikilink")、[南日本](../Page/南日本.md "wikilink")：以[畿內](../Page/畿內.md "wikilink")（日本古代的中央政權所在地）至東京這個位居日本中央位置的地區為中心來看待日本其他區域的觀點概念。其中，「北日本」的用法在播報氣象時經常被使用到，但「南日本」這稱呼卻甚少被提及。
  - [東國](../Page/東國.md "wikilink")、[西國](../Page/西國.md "wikilink")：以畿內為中心來看待日本全國其他地區的概念。

## 參考來源

## 参见

  - [五畿七道](../Page/五畿七道.md "wikilink")
      - [日本令制國列表](../Page/日本令制國列表.md "wikilink")
  - [日本地理](../Page/日本地理.md "wikilink")
  - [日本交通](../Page/日本交通.md "wikilink")
  - [日本鐵路](../Page/日本鐵路.md "wikilink")
  - [日本首都](../Page/日本首都.md "wikilink")
  - [政令指定都市](../Page/政令指定都市.md "wikilink")
  - [日本市町村列表](../Page/日本市町村列表.md "wikilink")
  - [道州制](../Page/日本道州制議論.md "wikilink")
  - [縣民經濟計算](../Page/縣民經濟計算.md "wikilink")

{{-}}

[Category:日本地理區劃](../Category/日本地理區劃.md "wikilink")
[Category:日本地方區分](../Category/日本地方區分.md "wikilink")
[Category:各國行政區劃列表](../Category/各國行政區劃列表.md "wikilink")

1.