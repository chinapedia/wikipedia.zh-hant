[Bungdan-gu_Seongnam.PNG](https://zh.wikipedia.org/wiki/File:Bungdan-gu_Seongnam.PNG "fig:Bungdan-gu_Seongnam.PNG")
**盆唐區**（）是[大韓民國](../Page/大韓民國.md "wikilink")[京畿道](../Page/京畿道.md "wikilink")[城南市南部的一個區](../Page/城南市.md "wikilink")，面積有69.44平方公里，是韓國首都圈內的一個新興城鎮。根據2006年的人口統計，區內有人口450,130人。

## 行政區劃

盆唐區共有19個行政洞，從18個法定洞劃分，分別如下：

<table>
<thead>
<tr class="header">
<th><p>法定洞名</p></th>
<th><p>行政洞名</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/盆唐洞.md" title="wikilink">盆唐洞</a> (분당동)</p></td>
<td><p><a href="../Page/盆唐洞.md" title="wikilink">盆唐洞</a> (분당동)</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/藪內洞.md" title="wikilink">藪內洞</a> (수내동)</p></td>
<td><p><a href="../Page/藪內1洞.md" title="wikilink">藪內1洞</a> (수내1동) / <a href="../Page/草林洞.md" title="wikilink">草林洞</a> (초림동)</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/藪內2洞.md" title="wikilink">藪內2洞</a> (수내2동) / <a href="../Page/內亭洞.md" title="wikilink">內亭洞</a> (내정동)</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/藪內3洞.md" title="wikilink">藪內3洞</a> (수내3동) / 藪內洞 (수내동)</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/亭子洞.md" title="wikilink">亭子洞</a> (정자동)</p></td>
<td><p><a href="../Page/亭子1洞.md" title="wikilink">亭子1洞</a> (정자1동) / <a href="../Page/新基洞.md" title="wikilink">新基洞</a> (신기동)</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/亭子2洞.md" title="wikilink">亭子2洞</a> (정자2동) / 亭子洞 (정자동)</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/亭子3洞.md" title="wikilink">亭子3洞</a> (정자3동) / <a href="../Page/佛亭洞.md" title="wikilink">佛亭洞</a> (불정동)</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/書硯洞.md" title="wikilink">書硯洞</a> (서현동)<br />
<a href="../Page/栗洞.md" title="wikilink">栗洞</a> (율동)</p></td>
<td><p><a href="../Page/書硯1洞.md" title="wikilink">書硯1洞</a> (서현1동) / 書硯洞 (서현동)</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/書硯2洞.md" title="wikilink">書硯2洞</a> (서현2동) / 書堂洞 (서당동)</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/二梅洞.md" title="wikilink">二梅洞</a> (이매동)</p></td>
<td><p><a href="../Page/二梅1洞.md" title="wikilink">二梅1洞</a> (이매1동) / 二梅洞 (이매동)</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/二梅2洞.md" title="wikilink">二梅2洞</a> (이매2동) / <a href="../Page/梅松洞.md" title="wikilink">梅松洞</a> (매송동)</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/野塔洞.md" title="wikilink">野塔洞</a> (야탑동)</p></td>
<td><p><a href="../Page/野塔1洞.md" title="wikilink">野塔1洞</a> (야탑1동) / 野塔洞 (야탑동)</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/野塔2洞.md" title="wikilink">野塔2洞</a> (야탑2동) / <a href="../Page/下塔洞.md" title="wikilink">下塔洞</a> (하탑동)</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/野塔3洞.md" title="wikilink">野塔3洞</a> (야탑3동) / <a href="../Page/中塔洞.md" title="wikilink">中塔洞</a> (중탑동)</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/板橋洞.md" title="wikilink">板橋洞</a> (판교동)</p></td>
<td><p><a href="../Page/板橋洞.md" title="wikilink">板橋洞</a> (판교동)</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/三坪洞.md" title="wikilink">三坪洞</a> (삼평동)</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/柏峴洞.md" title="wikilink">柏峴洞</a> (백현동)</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/金谷洞.md" title="wikilink">金谷洞</a> (금곡동)</p></td>
<td><p><a href="../Page/金谷1洞.md" title="wikilink">金谷1洞</a> (금곡1동) / 金谷洞 (금곡동)</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/宮內洞.md" title="wikilink">宮內洞</a> (궁내동)</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/東遠洞.md" title="wikilink">東遠洞</a> (동원동)</p></td>
<td><p><a href="../Page/金谷2洞.md" title="wikilink">金谷2洞</a> (금곡2동) / 金谷洞 (금곡동)</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/九美洞.md" title="wikilink">九美洞</a> (구미동)</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/九美洞.md" title="wikilink">九美洞</a> (구미동)</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>雲中洞 (운중동)</p></td>
<td><p><a href="../Page/雲中洞.md" title="wikilink">雲中洞</a> (운중동)</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/大庄洞.md" title="wikilink">大-{庄}-洞</a> (대장동)</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/石雲洞.md" title="wikilink">石雲洞</a> (석운동)</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/下山雲洞.md" title="wikilink">下山雲洞</a> (하산운동)</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 地理

東面是[廣州市](../Page/廣州市_\(京畿道\).md "wikilink")、東北面是[中院區](../Page/中院區.md "wikilink")、西北面是[壽井區](../Page/壽井區.md "wikilink")、西面是[義王市](../Page/義王市.md "wikilink")、南面是[龍仁市](../Page/龍仁市.md "wikilink")。

## 名稱爭議

盆唐區原來只是[廣州郡的兩個](../Page/廣州郡.md "wikilink")-{里}-，分別叫**盆店-{里}-**()及**唐隅-{里}-**()，今日的名稱是從[日韓合邦後的行政改組開始](../Page/日韓合邦.md "wikilink")。1914年，當時的日本殖民政府對全個朝鮮的行政單位重組，而盆店-{里}-及唐隅-{里}-亦是於當時合併成為**盆唐里**()。不過，後來有發現指在1906年[伊藤博文被任命為](../Page/伊藤博文.md "wikilink")[朝鮮總督之後](../Page/朝鮮總督.md "wikilink")，曾進行過另外一次土地改革，而根據當時的紀錄，「唐隅-{里}-」的漢字名稱被寫成「堂隅-{里}-」。因此，有陰謀論者認為這是殖民政府有意把自己比作消滅[百濟的](../Page/百濟.md "wikilink")[唐朝而更改當地的名稱](../Page/唐朝.md "wikilink")，因為盆唐區在三國時代屬於百濟的疆土。

現時在當地，有學者及政府官員建議把「盆唐區」改名為「盆堂區」。

## 歷史

一直以來，盆唐區都是一個人口稀少的農村，依靠種植稻物為業。

1989年，政府開展盆唐新市鎮計劃，使當地人口急增。90年代成為韓國第一個全人工新市鎮，2000年代起，成為[城南市人口最密](../Page/城南市.md "wikilink")，也是最富庶和地價第二高（全韓國第二高）的地區。

## 交通

### 鐵道

  - [韓國鐵道公社](../Page/韓國鐵道公社.md "wikilink")
      - [盆唐線](../Page/盆唐線.md "wikilink")

<!-- end list -->

  -

      -
        [野塔驛](../Page/野塔驛.md "wikilink") -
        [二梅驛](../Page/二梅驛.md "wikilink") -
        [書峴驛](../Page/書峴驛.md "wikilink") -
        [藪內驛](../Page/藪內驛.md "wikilink") -
        [亭子驛](../Page/亭子驛.md "wikilink") -
        [美金驛](../Page/美金驛.md "wikilink") -
        [梧里驛](../Page/梧里驛.md "wikilink")

<!-- end list -->

  - [仁川國際機場有高速巴士直達盆唐](../Page/仁川國際機場.md "wikilink")。

## 国際交流

## 教育

  - [SK電信Access研究院](../Page/SK電信Access研究院.md "wikilink")

## 著名人物

  - [李恩宙](../Page/李恩宙.md "wikilink")：女演員
  - [李秀英](../Page/李秀英.md "wikilink")：女歌手
  - [趙鍾煥](../Page/趙鍾煥.md "wikilink")：男歌手.[100%團體成員](../Page/:100%_\(組合\).md "wikilink")

## 其他

  - [板橋新都市](../Page/板橋新都市.md "wikilink")

## 外部連結

  - [城南市政府官方網頁](http://www.seongnam.go.kr/CN/main.do)
  - [盆唐區區政府官方網頁](https://web.archive.org/web/20060518220817/http://www.bundang-gu.or.kr/)


[Category:城南市](../Category/城南市.md "wikilink")