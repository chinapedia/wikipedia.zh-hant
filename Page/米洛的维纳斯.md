{{ infobox artwork | image_file = Venus_de_Milo_Louvre_Ma399.jpg |
painting_alignment = | image_size = 250px | title =米洛的維納斯 |
other_language_1 = | other_title_1 = | other_language_2 = |
other_title_2 = | artist =  | year = 西元前130到100年之間 | type =
[大理石](../Page/大理石.md "wikilink") | height_metric = 202 | city =
[巴黎](../Page/巴黎.md "wikilink") | museum =
[羅浮宮](../Page/羅浮宮.md "wikilink") }}

**断臂维纳斯**，也称**米洛的维纳斯**（，），是一座著名的[古希腊](../Page/古希腊.md "wikilink")[雕像](../Page/雕塑.md "wikilink")。這座雕像創作於西元前130到100年之間，表现的是[希腊神话中爱与美的女神](../Page/希腊神话.md "wikilink")[阿佛洛狄忒](../Page/阿佛洛狄忒.md "wikilink")（[罗马神话中与之对应的女神是](../Page/罗马神话.md "wikilink")[维纳斯](../Page/维纳斯.md "wikilink")）。這座[大理石雕成的雕像高](../Page/大理石.md "wikilink")202公分，略大於人體真實大小。\[1\]1820年發現於[希臘](../Page/希臘.md "wikilink")[米洛斯島](../Page/米洛斯島.md "wikilink")（現代希臘語稱作米洛），故被稱作米洛的維納斯。
過去它曾被誤認為雕刻家[普拉克西特列斯的作品](../Page/普拉克西特列斯.md "wikilink")，現在一般認為是的創作。目前收藏於[法國](../Page/法國.md "wikilink")[巴黎的](../Page/巴黎.md "wikilink")[羅浮宮](../Page/羅浮宮.md "wikilink")。\[2\]

## 來歷

1820年，农民伊奥尔科斯在[米洛斯岛上发现它](../Page/米洛斯岛.md "wikilink")。他试图将这尊[雕像藏起来](../Page/雕像.md "wikilink")，但后来还是被一个[土耳其军官发现了](../Page/土耳其.md "wikilink")。当时[法国驻土耳其的大使将它买下](../Page/法国.md "wikilink")。這座雕像於隔年作為禮物獻給法國國王[路易十八](../Page/路易十八.md "wikilink")，之後國王將其贈送給[羅浮宮](../Page/羅浮宮.md "wikilink")。

## 描述

米洛的维纳斯以其遺失的神祕雙臂而知名。\[3\]雕塑整體由两块大理石拼接而成，两块大理石连接处非常巧妙，在身躯裸露部分与裹巾的相邻处。这座维纳斯雕像是举世公认的女性人体美的典范，这是因为她完全符合[黄金分割的人体美比例关系](../Page/黄金分割.md "wikilink")。黄金分割的比例关系是1:1.618，把它用在人体上，就是将人体分为上下两个部分，其分界点正位于肚脐。匀称的人体上下两部分的比例正好是1:1.618。由于这个比例接近于8:5或5:3，而人体的总长为8头身，所以这个总长度可以分割成头、颈下至肚脐、肚脐至脚三段。这三段以头部为基准，分割为1:2:5的整数比，成为人体美的标准规则。维纳斯的造型魅力在于，无论从哪个角度去看，她都符合人体的黄金分割率，因此她能超越时代，成为女性永恒美的象征。

[File:Vindplaats_venus_van_milo.jpg|断臂维纳斯出土地点](File:Vindplaats_venus_van_milo.jpg%7C断臂维纳斯出土地点)

## 參見

  - [古希臘雕刻](../Page/古希臘雕刻.md "wikilink")
  - [雕塑](../Page/雕塑.md "wikilink")
  - [米洛斯岛](../Page/米洛斯岛.md "wikilink")
  - [古希腊](../Page/古希腊.md "wikilink")

## 参考文献

  - 《美学与美育》（王一川/主编、中央广播电视大学出版社）p.154 ISBN 978-7-304-03982-0

## 外部連結

  - [羅浮宮官方頁面](http://www.louvre.fr/oeuvre-notices/aphrodite-dite-venus-de-milo#)

[Category:古希臘雕塑作品](../Category/古希臘雕塑作品.md "wikilink")
[Category:神像](../Category/神像.md "wikilink")

1.
2.
3.