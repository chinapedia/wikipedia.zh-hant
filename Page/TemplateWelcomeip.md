<div style="width: 100%; margin: 0.2em 0; padding: 0.3em 0 0.3em 0; border-top:1px solid #DDD; border-bottom:1px solid #DDD; font-size: 115%;">

**您好！[欢迎来到维基百科](../Page/WP:WEL.md "wikilink")。**

</div>

<div style="font-size:95%;">

感謝您對維基百科的興趣與貢獻，希望您會喜歡這-{zh-cn:里; zh-hk:裏;
zh-tw:裡;}-。您可以继续以匿名身份參與，但不妨考虑**[-{zh-cn:注册;zh-tw:登記}-一個帳戶](../Page/Help:如何登录.md "wikilink")**以使用更多功能，例如你能夠：

1.  編輯受半保護頁面；
2.  选择一个属于您自己的用户名称，隐藏自己的IP地址；
3.  进行个性化的设置，添加增强工具；
4.  拥有自己完整的贡献记录；
5.  更方便地参加社群事务⋯⋯

<!-- end list -->

  -
    等等。除了[歡迎辭以外](../Page/Wikipedia:WELCOME.md "wikilink")，也請您了解以下重要文章：'''

<table>
<tbody>
<tr class="odd">
<td><figure>
<img src="https://zh.wikipedia.org/wiki/File:Nuvola_apps_korganizer.svg" title="Nuvola_apps_korganizer.svg" alt="Nuvola_apps_korganizer.svg" width="35" /><figcaption>Nuvola_apps_korganizer.svg</figcaption>
</figure>
<div style="margin-left:45px;">
<p><a href="../Page/WP:5P.md" title="wikilink"><strong>五大支柱</strong></a><br />
<small><a href="../Page/WP:NOT.md" title="wikilink">百科</a>、<a href="../Page/WP:NPOV.md" title="wikilink">中立</a>、<a href="../Page/WP:CRFAQ.md" title="wikilink">开放</a>、<a href="../Page/WP:CIV.md" title="wikilink">互重</a>、<a href="../Page/WP:IAR.md" title="wikilink">勇於创新</a></small></p>
</div></td>
<td><figure>
<img src="https://zh.wikipedia.org/wiki/File:Nuvola_apps_emacs.png" title="Nuvola_apps_emacs.png" alt="Nuvola_apps_emacs.png" width="45" /><figcaption>Nuvola_apps_emacs.png</figcaption>
</figure>
<div style="margin-left:55px;">
<p><a href="../Page/WP:CRFAQ.md" title="wikilink"><strong>版權問題解答</strong></a><br />
<small>貢獻內容必须是<strong>您所著或獲得授權</strong>、<br />
并同意在<a href="../Page/WP:C.md" title="wikilink">CC-by-sa-3.0和GFDL條款下發布</a></small></p>
</div></td>
<td><figure>
<img src="https://zh.wikipedia.org/wiki/File:Postscript-viewer.svg" title="Postscript-viewer.svg" alt="Postscript-viewer.svg" width="35" /><figcaption>Postscript-viewer.svg</figcaption>
</figure>
<div style="margin-left:45px;">
<p><a href="../Page/WP:T.md" title="wikilink"><strong>使用指南</strong></a><br />
<small><a href="../Page/Help:如何访问维基百科.md" title="wikilink">安全访问</a> / <a href="../Page/WP:SB.md" title="wikilink">測試編輯</a> / <a href="../Page/WP:HEP.md" title="wikilink">如何编辑</a><br />
<a href="../Page/WP:HELP.md" title="wikilink">使用手册</a> / <a href="../Page/Help:中文维基百科的繁简处理.md" title="wikilink">繁简处理</a> / <a href="../Page/WP:改进条目.md" title="wikilink">寫個好條目</a></small></p>
</div></td>
</tr>
<tr class="even">
<td><p><img src="https://zh.wikipedia.org/wiki/File:Icon_apps_query.svg" title="fig:Icon_apps_query.svg" alt="Icon_apps_query.svg" width="18" /><strong>有问题？</strong>請到<a href="../Page/WP:VP.md" title="wikilink"><strong>互助客栈</strong>询问</a>，或在<strong>我的对话页</strong>提出。别忘記：討論後要<a href="../Page/WP:SIG.md" title="wikilink"><strong>簽名</strong></a>，方式之一是留下4個波浪紋「 ~~~~ 」。</p>
<div lang="en" style="font-style:italic;font-size:90%;">
<p>If you have any questions about the Chinese Wikipedia, please leave a message <a href="../Page/Wikipedia_talk:Guestbook_for_non-Chinese-speakers.md" title="wikilink"><em>here</em></a>. Thank you for visiting!</p>
</div></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

  - 閱讀新手应该注意的七种[**常见错误**](../Page/Wikipedia:避免常见错误.md "wikilink")、理解[維基百科的立場與常見注意事項](../Page/WP:KID.md "wikilink")。
  - 有任何编辑上的问题？请试试[IRC即时讨论](../Page/WP:IRC.md "wikilink")。也可\[
    到此处提问\]，其他[维基人见到后就会来提供帮助](../Page/WP:维基人.md "wikilink")。
  - 不知道有甚麼可写？[条目请求](../Page/WP:RA.md "wikilink")、[最多语言版本的待撰条目](../Page/WP:SA.md "wikilink")、[缺少的传统百科全书条目和](../Page/WP:MEA.md "wikilink")[首页的缺失条目中列出了许多维基百科目前还没有的条目](../Page/WP:专题/首页的缺失条目.md "wikilink")，欢迎您来[撰寫](../Page/WP:NEW.md "wikilink")！
  - 希望您能享受共同编写百科的樂趣，成为一名充實的[维基百科人](../Page/WP:EDIANS.md "wikilink")。

<includeonly><span style="display:none">{{</includeonly><includeonly>subst:\#if:
|{{</includeonly><includeonly>subst:var1|\</}}{{</includeonly><includeonly>subst:var1|span\>}}我是欢迎您的维基人：{{</includeonly><includeonly>subst:var1|\<}}{{</includeonly><includeonly>subst:var1|1=span
style="display:none"\>}}
|{{</includeonly><includeonly>subst:var1|\</}}{{</includeonly><includeonly>subst:var1|span\>}}我是欢迎您的维基人：\~\~</includeonly><includeonly>\~\~{{</includeonly><includeonly>subst:var1|\<}}{{</includeonly><includeonly>subst:var1|1=span
style="display:none"\>}} }}</span></includeonly>

</div>

<noinclude> </noinclude>