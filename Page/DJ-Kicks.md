***DJ-Kicks***（在其所有唱片封面標示為***DJ-KiCKS***）是一系列[DJ](../Page/DJ.md "wikilink")[混音專輯](../Page/混音.md "wikilink")，由獨立唱片品牌[\!K7
Records](http://en.wikipedia.org/wiki/Studio_%21K7)旗下不同DJ所[混音而成](../Page/混音.md "wikilink")。在1995年首次發行時，還算是一張[電子音樂DJ](../Page/電子音樂.md "wikilink")[舞吧](../Page/舞吧.md "wikilink")（club）的合輯，風格為[科技](../Page/科技.md "wikilink")（Techno）或[浩室](../Page/浩室.md "wikilink")（House），之後轉以在家聆聽音樂的一般聽眾為對象。不久之後，以上兩種選編路線都獲得擴展，除了DJ之外，越來越多的製作人（如Terranova）、混音師（如Kruder
& Dorfmeister）、樂團（如The Stereo
MCs）與音樂家（如Nicolette）都加入了DJ-Kicks系列的陣容，音樂路線也變得更繁雜，風格範疇橫跨Trüby
Trio的[重拍爵士](../Page/重拍爵士.md "wikilink")（Downbeat Jazz）聲響到Kemistry &
Storm前衛的[鼓打貝斯](../Page/鼓打貝斯.md "wikilink")（Drum and
Bass），但所有作品仍可大致歸納於[電子音樂的範疇](../Page/電子音樂.md "wikilink")。

1995年發行的第一張DJ-Kicks系列唱片為C.J.
Bolland的作品，至今該系列仍在穩定擴展中，約一年推出二至三張作品，至2006年10月，DJ-Kicks系列已發行了28張作品，其中有一些非常受到歡迎，最著名的即是Kruder
& Dorfmeister的混音作品。DJ-Kicks系列曾被《Mixmag》雜誌譽為「最重要的DJ混音系列」，它第26張作品DJ-Kicks:
The Exclusives是一張特別精選集，收錄了該系列DJ最佳的原始混音單曲。

## *DJ-Kicks*發行的唱片

## 外部链接

  - [Official *DJ-Kicks* website](http://www.dj-kicks.com)
  - [\!K7 Records website](http://www.k7.com)

[\!](../Category/DJ-Kicks.md "wikilink") [Category:DJ mix
albums](../Category/DJ_mix_albums.md "wikilink") [Category:Compilation
album series](../Category/Compilation_album_series.md "wikilink")