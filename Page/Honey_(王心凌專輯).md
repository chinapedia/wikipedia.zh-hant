《**Honey**》是[臺灣](../Page/臺灣.md "wikilink")[歌手](../Page/歌手.md "wikilink")[王心凌的第三張](../Page/王心凌.md "wikilink")[錄音室專輯](../Page/錄音室專輯.md "wikilink")。由[愛貝克思](../Page/愛貝克思_\(台灣\).md "wikilink")（時為艾迴唱片）於2005年2月18日發行。該專輯於2005年3月25日發行改版「甜蜜慶功版」。

## 曲目

## 音樂錄影帶

<table style="width:25%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 15%" />
</colgroup>
<thead>
<tr class="header">
<th><p>歌名</p></th>
<th><p>導演</p></th>
<th><p>附註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Honey</p></td>
<td><p><a href="../Page/黃中平.md" title="wikilink">黃中平</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>翹翹板</p></td>
<td><p><a href="../Page/賴偉康.md" title="wikilink">賴偉康</a><br />
<a href="../Page/比爾賈.md" title="wikilink">比爾賈</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>翅膀</p></td>
<td><p>賴偉康<br />
比爾賈</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>失戀歌迷黨</p></td>
<td><p><a href="../Page/金卓.md" title="wikilink">金卓</a><br />
<a href="../Page/陳勇秀.md" title="wikilink">陳勇秀</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>飛吧</p></td>
<td><p><a href="../Page/張昀陽.md" title="wikilink">張昀陽</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>愛的滑翔翼</p></td>
<td></td>
<td><p>畫面剪輯自《Honey》專輯慶功演唱會</p></td>
</tr>
</tbody>
</table>

## 版本

  - 2005年2月18日：正式版（1片[CD](../Page/CD.md "wikilink")）
  - 2005年3月25日：甜蜜慶功版（1片CD、1片[VCD](../Page/VCD.md "wikilink")（收錄3支[音樂錄影帶](../Page/音樂錄影帶.md "wikilink")、〈Honey〉與〈翹翹板〉舞蹈版MV）及10張「Honey甜蜜」寫真[明信片](../Page/明信片.md "wikilink")）

[Category:流行音樂專輯](../Category/流行音樂專輯.md "wikilink")
[Category:台灣音樂專輯](../Category/台灣音樂專輯.md "wikilink")
[Category:2005年音樂專輯](../Category/2005年音樂專輯.md "wikilink")
[Category:王心凌音樂專輯](../Category/王心凌音樂專輯.md "wikilink")