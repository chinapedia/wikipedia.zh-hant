**扶西·黎刹**（，全名：，漢文譯为**厘沙路**\[1\]，又譯**黎薩**、**黎刹爾**、**黎薩爾**；）是[菲律賓的一位民族英雄](../Page/菲律賓.md "wikilink")，菲律宾天主徒，[華人](../Page/華人.md "wikilink")，[柯姓](../Page/柯姓.md "wikilink")[閩南人後裔](../Page/閩南人.md "wikilink")，常被華僑稱為**柯黎薩**。他的尊稱有“[馬來人之驕](../Page/馬來人.md "wikilink")”、“馬來偉人”、“第一個菲律賓人”、「菲律賓國父」、“革命[彌賽亞](../Page/彌賽亞.md "wikilink")”、“萬世英雄”和“救贖彌賽亞”等。

黎剎是一名[眼科](../Page/眼科.md "wikilink")[醫生](../Page/醫生.md "wikilink")，精通包括[英](../Page/英語.md "wikilink")、[法](../Page/法語.md "wikilink")、[德](../Page/德語.md "wikilink")、[拉丁](../Page/拉丁語.md "wikilink")、[閩南語以及](../Page/閩南語.md "wikilink")[官話等](../Page/官話.md "wikilink")22種[語言](../Page/語言.md "wikilink")，並在文理各方多才多藝，除医学外，还懂得绘画、雕刻、哲学和历史等。從年輕時就開始從事[西班牙統治下的改革工作](../Page/西班牙.md "wikilink")，曾在1882年至1892年旅居[歐洲](../Page/歐洲.md "wikilink")，出版曝露西班牙統治弊端叢生的小說，成為宣傳運動的領袖，發表了針對改革的文章、[雜誌和](../Page/雜誌.md "wikilink")[詩歌](../Page/詩歌.md "wikilink")。他同时也是[菲律宾共济会成员](../Page/共济会#.E8.8F.B2.E5.BE.8B.E5.AE.BE.md "wikilink")。

黎剎於1896年12月30日被西班牙殖民當局處決，今[菲律賓政府將此日定為國定假日](../Page/菲律賓.md "wikilink")[黎剎日](../Page/黎剎日.md "wikilink")。

## 家世

祖籍地为[泉州府](../Page/泉州府.md "wikilink")[晉江縣](../Page/晉江縣.md "wikilink")[罗山上郭村](../Page/罗山街道.md "wikilink")（[晋江市](../Page/晋江市.md "wikilink")[新塘街道上郭社区](../Page/新塘街道_（晋江市）.md "wikilink")）。第一世至第十七世为：塘邊叟、山翁、柯光震、柯雲從、柯植、柯松老、柯賜、柯萬卿、柯宗賢、柯礜、柯洪、柯檜、柯延佐、柯伯遐、柯和夫、柯才經、柯從佑。

  - 第18世（在菲第1世）：天祖父柯长豐，人称长豐哥（）或長哥／常哥／祥哥（）。
  - 第19世（在菲第2世）：高祖父柯仪南（）人称柯南哥，西班牙语名多明戈·南哥（），后改姓，称多明戈·梅尔卡多（），“梅尔卡多”意“市場”）
  - 第20世（在菲第3世）：曾祖父弗朗西斯科·梅尔卡多（）
  - 第21世（在菲第4世）：祖父胡安·梅尔卡多（）
  - 第22世（在菲第5世）：父弗朗西斯科·梅尔卡多（），后因其祖父同名在当地总督的建议下改姓，称弗朗西斯科·黎刹－梅尔卡多（）
  - 第23世（在菲第6世）：**扶西·黎刹**－梅尔卡多\[2\]

## 生平

1875年，年僅14岁的黎剎获得文学学士学位，隨後進入菲律賓[圣道顿马士皇家教会大学](../Page/圣道顿马士皇家教会大学.md "wikilink")（University
of Santo Tomas），主修哲学，兼修美术。後因母患眼疾，黎剎改读医科。

1879年，18岁的黎剎發表爱国诗篇《给菲律宾青年》，此詩获全国诗歌比赛一等奖，黎剎也因此被誉为“菲律宾青年诗人”。
1882年，21岁的黎刹潜赴歐洲加入[共济会](../Page/共济会.md "wikilink")，在西班牙[马德里康普顿斯大学](../Page/马德里康普顿斯大学.md "wikilink")、法国[巴黎大学以及德国](../Page/巴黎大学.md "wikilink")[海德堡大学深造](../Page/海德堡大学.md "wikilink")。1885年，他取得医学博士学位，并留校执教。

1887年，黎剎在執教期間，用西班牙文创作了《社会毒瘤》（另譯《不許犯我》）一书，揭露西班牙殖民统治的殘酷眞相。该书的续篇《起义者》也於1891年出版。

[Ophthalmologist_Business_Card_of_Doctor_Jose_Rizal_from_Hong_Kong_End_of_19_Century.jpg](https://zh.wikipedia.org/wiki/File:Ophthalmologist_Business_Card_of_Doctor_Jose_Rizal_from_Hong_Kong_End_of_19_Century.jpg "fig:Ophthalmologist_Business_Card_of_Doctor_Jose_Rizal_from_Hong_Kong_End_of_19_Century.jpg")在[香港使用的](../Page/香港.md "wikilink")[名片](../Page/名片.md "wikilink")\]\]
[HK_Central_Rednaxela_Terrace_Shelley_Street_Dr_Jose_Rizal_2012.JPG](https://zh.wikipedia.org/wiki/File:HK_Central_Rednaxela_Terrace_Shelley_Street_Dr_Jose_Rizal_2012.JPG "fig:HK_Central_Rednaxela_Terrace_Shelley_Street_Dr_Jose_Rizal_2012.JPG")故居的紀念牌匾\]\]

1891年12月至1892年6月，黎剎與家人居住在[香港島半山](../Page/香港島.md "wikilink")[列拿士地臺門牌第二號](../Page/列拿士地臺.md "wikilink")，他每天[下午](../Page/下午.md "wikilink")2時至[晚上](../Page/晚上.md "wikilink")6時在[中環](../Page/中環.md "wikilink")[德己立街](../Page/德己立街.md "wikilink")5號作[眼科醫生](../Page/眼科醫生.md "wikilink")。

1892年6月，黎剎返回[菲律賓後](../Page/菲律賓.md "wikilink")，建立一個非暴力的改革社團[菲律賓聯盟](../Page/菲律賓聯盟.md "wikilink")。同年7月6日被捕，随後被流放到[民答那峨島](../Page/民答那峨島.md "wikilink")（Mindanao，又叫[棉蘭老島](../Page/棉蘭老島.md "wikilink")）的小镇（又譯达皮丹，Dapitan）——世界上最荒芜的地方之一。他在那裡居住了四年，帮助小镇建设学校、医院等设施，并且在当地建立了全菲第一个灯光系统。期间，他还遭遇了来自香港的爱尔兰裔未婚妻[约瑟芬·布蕾肯](../Page/约瑟芬·布蕾肯.md "wikilink")（Josephine
Brackon）難產，孩子未能存活。

1896年，[卡蒂普南](../Page/卡蒂普南.md "wikilink")（Katipunan）祕密革命社團發動叛亂後，本來獲釋到古巴當義醫的黎刹，沒有接受好友**唐·佩德羅**（Don
Pedro，馬尼拉工商鉅子）的建議留在[新加坡接受英國法律庇護](../Page/新加坡.md "wikilink")，仍然坐船前往西班牙，準備轉往古巴。

果然菲律賓總督[羅曼·布蘭科](../Page/羅曼·布蘭科.md "wikilink")（Ramón
Blanco）食言，把黎剎視為危險的敵人，必須擔起菲律賓革命負責，暗中勾結國防部長和殖民部長要捉他回國。
10月3日船抵[巴塞隆納](../Page/巴塞隆納.md "wikilink")，黎剎馬上變成階下囚，抓他的人正是讓他流亡四年的前菲律賓總督狄思普耶將軍，當時擔任巴塞隆納的軍區司令。

黎刹被押回[马尼拉](../Page/马尼拉.md "wikilink")，囚禁在[圣地亚哥堡](../Page/圣地亚哥堡_\(菲律宾\).md "wikilink")。同年12月30日凌晨，西班牙當局以“非法结社和文字煽动叛乱”的罪名在[马尼拉将黎剎處決](../Page/马尼拉.md "wikilink")，即使他並未加入該社團，也未曾參加[叛亂](../Page/叛亂.md "wikilink")。

临刑前，应未婚妻要求，黎剎與她举行了刑场上的婚礼，最後写下绝命诗《永别了，我的祖国》。他的壯烈犧牲使[菲律賓人意識到除了脫離](../Page/菲律賓人.md "wikilink")[西班牙](../Page/西班牙.md "wikilink")[獨立外別無選擇](../Page/獨立.md "wikilink")，隨後爆發了[菲律賓革命](../Page/菲律賓革命.md "wikilink")。

## 主要著作

  - 《不许犯我》（或译「社会毒瘤」、「不要犯我」、「别碰我」）
  - 《起义者》
  - 《永别了，我的祖国》（绝命诗）：

<!-- end list -->

  -
    (台湾作家**[东方白](../Page/东方白.md "wikilink")**译)
    别了，祖国，
    艳阳之土， 南海之珠，失去的乐园，
    我乐于把这凋零之躯献给你，
    即使再年轻，再健壮，再幸福，
    我仍然毫不怜惜，把它献给你。
    在无情的沙场，激烈的战斗，
    有人把生命捐弃，既不犹豫也不在意，
    葬身之所岂有分别？
    在香杉在月桂在百合之下，
    在绞台在原野，
    在刀枪或殉道的途上，
    只要为了国家为了乡土，它都一般无二。
    我要在东方破晓的清晨死去，
    正当那旭日的光明冲破长夜的黑暗，
    如果曙光无色，
    就拿我的血漂染， 任你泼洒整个苍穹，
    把它染成一片瑰丽的鲜红。
    当生命之页在眼前展开，我做过梦，
    当青春之火在心中点燃，我做过梦，
    梦见可爱的脸，
    啊，你这南海之珠，
    不再悲伤，不再憔悴，眉头无结，眼眶无泪。
    我爱之梦，我生之欲，
    在悲泣，为你这将高飞的灵魂，
    在欢唱，因你终将远离这多愁的嚣尘，
    为国而死，不正是你日夜所望？
    然后躺在她的怀里，安然无恙。
    看见一枝羞放的花朵，
    有朝一日，在我墓地的草上，
    请把它拥到唇边深深吻我，
    也许我久卧墓穴，周身发抖，
    让我的眉梢感受你的轻触，你的热呼。
    让晨曦着我金色衣衫，
    让月光抚我无底创伤，
    让微风飘送饮泣悲叹。
    若有鸟儿栖在十字架上，
    让它为我低吟，如对国殇。
    让太阳把雾气提升上苍， 使夜幕在坟地重重下降，
    让善人来墓旁喟我不遇， 然后在静夜为我默默恳乞，
    求你，我的祖国，让我在天国安息。
    哀祷吧，为那些不幸早逝的故友，
    为那些满身伤痕的活人，
    为那些因儿女悲恸的母亲，
    为那些寡妇孤儿，那些受刑逼供的罪人，
    然后为你自己的救赎而祈祷。
    因为长夜漫漫只有尸身可依，
    然后对着苍穹倾诉我迟来的抗议，
    请别吵我清睡与深沉的神秘。
    也许你会听见圣颂在周遭悠扬，
    这是我啊，祖国，我在对你歌唱。
    既无十字架，也不见碑坊，
    当我的坟墓已被人遗忘， 让犁犁过，让锹翻过，
    在我骨灰被风吹散之前， 将它撒播，覆盖大地。
    当我飞过高山越过大海， 然后隐入无形我不在意，
    惊叹空间的浩瀚与时间的无际，
    我将带着光明与色彩， 传播我的信心，我的爱。
    我热爱的菲律宾啊，
    请听我最后的骊歌，
    我敬爱的祖国啊，令我忧伤的祖国，
    我把一切都献给你-我的父母以及我的兄弟，
    因为我将去之国，没有暴君也没有奴隶，
    那里亲善和睦，一切统于上帝。
    别了，被蹂躏的朋友自我的孩提，
    别了，从破碎的心底我呼唤你，
    感谢你，我的上帝，我走完了人生的劳途，
    也感谢你，我的导师，你照明了我的路，
    永别了，人类我爱，只在寂灭才有安息。

## 參見

  - [列拿士地臺](../Page/列拿士地臺.md "wikilink")
  - [黎刹公园](../Page/黎刹公园.md "wikilink")

## 注释

## 外部链接

  - [传奇黎刹：带有中国血统的菲律宾民族英雄.付志刚.光明日报.2011-08-16](http://www.wenming.cn/sjwm_pd/hwcf/201108/t20110816_285051.shtml)
  - [菲律宾国父黎刹](http://www.tma.tw/ltk/100540111.pdf)

[Category:菲律賓革命家](../Category/菲律賓革命家.md "wikilink")
[Category:菲律宾政治人物](../Category/菲律宾政治人物.md "wikilink")
[Category:菲律宾西班牙佔時期](../Category/菲律宾西班牙佔時期.md "wikilink")
[Category:菲律宾作家](../Category/菲律宾作家.md "wikilink")
[Category:菲律賓醫生](../Category/菲律賓醫生.md "wikilink")
[Category:马德里康普顿斯大学校友](../Category/马德里康普顿斯大学校友.md "wikilink")
[Category:巴黎大學校友](../Category/巴黎大學校友.md "wikilink")
[Category:馬尼拉阿德紐大學校友](../Category/馬尼拉阿德紐大學校友.md "wikilink")
[Category:聖道頓馬士大學校友](../Category/聖道頓馬士大學校友.md "wikilink")
[Category:被西班牙處決者](../Category/被西班牙處決者.md "wikilink")
[Category:被處決的菲律賓人](../Category/被處決的菲律賓人.md "wikilink")
[Category:菲律賓天主教徒](../Category/菲律賓天主教徒.md "wikilink")
[母系](../Category/西班牙裔菲律賓人.md "wikilink")
[母系](../Category/日本裔菲律賓人.md "wikilink")
[柯](../Category/菲律賓華人.md "wikilink")
[Category:华裔混血儿](../Category/华裔混血儿.md "wikilink")
[Category:柯姓](../Category/柯姓.md "wikilink")
[Category:西班牙裔混血兒](../Category/西班牙裔混血兒.md "wikilink")
[Category:日裔混血儿](../Category/日裔混血儿.md "wikilink")
[Category:马来族混血儿](../Category/马来族混血儿.md "wikilink")
[Category:菲律賓裔混血兒](../Category/菲律賓裔混血兒.md "wikilink")
[Category:亞洲紙幣上的人物](../Category/亞洲紙幣上的人物.md "wikilink")
[Category:亞洲硬幣上的人物](../Category/亞洲硬幣上的人物.md "wikilink")
[R](../Category/醫生出身的政治人物.md "wikilink")
[Category:被处决的作家](../Category/被处决的作家.md "wikilink")

1.  该名为其在[香港當眼科醫生時的名片上所印](../Page/香港.md "wikilink")。
2.  <http://shtong.gov.cn/newsite/node2/node70393/node70403/node72484/node72495/userobject1ai81191.html>