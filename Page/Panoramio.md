**Panoramio**是一个已經結束營運的地理位置[摄影分享网站](../Page/摄影.md "wikilink")。使用者可以上传包含[地理位置的照片](../Page/地理位置.md "wikilink")，並在每個月的月底展示在[Google地球和](../Page/Google地球.md "wikilink")[Google地图上](../Page/Google地图.md "wikilink")。Panoramio的宗旨是让Google地球的用户凭借观看其他用户的摄影作品对特定的区域更加地了解。該网站支持多种语言。

Panoramio的总部位于[瑞士](../Page/瑞士.md "wikilink")[苏黎世的Google的办公楼内](../Page/苏黎世.md "wikilink")\[1\]。网站在中国无法稳定访问，疑似受到了中国当局的封锁。

Google宣布Panoramio於2016年11月4日結束營運\[2\]，在[Google
Earth則可以存取Panoramio的相片至](../Page/Google_Earth.md "wikilink")2018年1月\[3\]\[4\]。現在仍可透過圖片網址直接存取Panoramio的相片。

## 历史

[Joaquín_Cuenca_-_Eduardo_Manchón.jpg](https://zh.wikipedia.org/wiki/File:Joaquín_Cuenca_-_Eduardo_Manchón.jpg "fig:Joaquín_Cuenca_-_Eduardo_Manchón.jpg")
2005年的夏天，两位[西班牙企业家华金](../Page/西班牙.md "wikilink")·昆卡·阿贝拉和爱德华多·曼琼·阿吉拉尔创立了Panoramio，不过正式发布时间则在2005年10月3日。至2007年3月19日，用户所提交的摄影作品数量超过了100万件\[5\]；3个月后，即2007年6月27日，已有共约200万張照片被上传\[6\]；4个月后的10月25日，这个数字攀升到了500万\[7\]。截至2015年12月25日，Panoramio照片上传数量超过了1.26亿。

2007年5月30日，[Google宣布其抱有收购Panoramio的意向](../Page/Google.md "wikilink")\[8\]，这个计划最终在該年7月实现\[9\]，两位创始人則分别在2010年1月和5月离去\[10\]\[11\]。

2014年9月16日，Google宣布打算关闭Panoramio，並将其并入[Google Maps
Views](../Page/Google街景服务.md "wikilink")。并入期间，“评论”、“最喜欢的照片”、“小组”等功能将取消。9月23日，Panoramio发起人阿贝拉、康德、阿吉拉尔提出诉求“Google：请继续保持Panoramio(Google:
Keep The Panoramio Community
Alive)”，请Panoramio用户签名連署。這項抗议和请求，迅速成为国际媒体的焦點。

2015年6月2日，Google宣布将继续保留Panoramio直至推出更佳的方案。

2016年10月8日，Google推出相片上傳工具以及[在地嚮導](../Page/在地嚮導.md "wikilink")。Google正式宣佈于2016年11月4日停止Panoramio服務\[12\]，預定2017年11月完全關閉Panoramio服務。

## 特色

用户上传的照片，都可以通过网站内置的功能实现地理定位，从而能在收录后展示与Google地球和Google地图相应的地理位置中。

Panoramio要求用户使用[标签](../Page/標籤_\(元數據\).md "wikilink")（[元数据的一种形式](../Page/元数据.md "wikilink")）来组织相片，这样搜索者可以通过某个特定的关键词，例如[地名或题材](../Page/地名.md "wikilink")，来找到相应的作品。Panoramio也是较早采用[标签云的网站](../Page/标签云.md "wikilink")。目前，上传的摄影作品以约每20天增加100万的数字增长\[13\]。用户还可以创建或加入[群组](../Page/Google网上论坛.md "wikilink")，并将自己的作品也添加进群组内。

## 参见

  - [EXIF](../Page/EXIF.md "wikilink")
  - [坐標](../Page/坐標.md "wikilink")[經緯](../Page/經緯.md "wikilink")
  - [Google地图](../Page/Google地图.md "wikilink")
  - [Google产品列表](../Page/Google产品列表.md "wikilink")

## 参考资料

## 外部連結

  - [Panoramio.com](http://www.panoramio.com)

[Category:Web 2.0](../Category/Web_2.0.md "wikilink")
[Category:Google](../Category/Google.md "wikilink")
[Category:電子地圖](../Category/電子地圖.md "wikilink")
[Category:网络相册](../Category/网络相册.md "wikilink")
[Category:被防火长城封锁的网站](../Category/被防火长城封锁的网站.md "wikilink")
[Category:2016年关闭的网站](../Category/2016年关闭的网站.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.