《**大城小聚**》（[英文](../Page/英语.md "wikilink")：），是[加拿大](../Page/加拿大.md "wikilink")[新時代電視製作的一個清談節目](../Page/新時代電視.md "wikilink")，逢星期一至五
[加拿大](../Page/加拿大.md "wikilink")[温哥華和](../Page/温哥華.md "wikilink")[多倫多時間晚上](../Page/多倫多.md "wikilink")18:20在[新時代電視播出](../Page/新時代電視.md "wikilink")。

節目在1998年8月啟播，內容是報導、邀請，內容包羅萬有。每集請來[温哥華和](../Page/温哥華.md "wikilink")[多倫多城中名人或藝人討論熱門話題](../Page/多倫多.md "wikilink")。前半年於[溫哥華錄影](../Page/溫哥華.md "wikilink")，而後半年則於[多倫多錄影](../Page/多倫多.md "wikilink")。

## 現任主持人

<table>
<tbody>
<tr class="odd">
<td><p><strong>姓名</strong></p></td>
<td><p><strong>所在地區</strong></p></td>
<td><p><strong>簡介</strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/林嘉華.md" title="wikilink">林嘉華</a></strong></p></td>
<td><p><a href="../Page/多倫多.md" title="wikilink">多倫多</a></p></td>
<td><ul>
<li>80及90年代時是<a href="../Page/TVB.md" title="wikilink">無綫電視當紅的合約藝員</a></li>
<li>也是<a href="../Page/加拿大中文電台.md" title="wikilink">加拿大中文電台AM</a>1430節目主持</li>
<li>間中回港拍攝<a href="../Page/無綫劇集.md" title="wikilink">無綫劇集</a></li>
<li>加港兩邊走。</li>
</ul></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/潘宗明.md" title="wikilink">潘宗明</a></strong></p></td>
<td><p><a href="../Page/多倫多.md" title="wikilink">多倫多</a></p></td>
<td><ul>
<li>80及90年代著名<a href="../Page/TVB.md" title="wikilink">無綫電視足球及體育報道員</a></li>
<li>曾為<a href="../Page/TVB.md" title="wikilink">無綫電視重量級司儀</a>。</li>
<li>也是<a href="../Page/加拿大中文電台.md" title="wikilink">加拿大中文電台AM</a>1430節目主持</li>
</ul></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/羅爵暉.md" title="wikilink">羅爵暉</a></strong></p></td>
<td><p><a href="../Page/多倫多.md" title="wikilink">多倫多</a></p></td>
<td><ul>
<li>也是<a href="../Page/加拿大中文電台.md" title="wikilink">加拿大中文電台AM</a>1430節目主持</li>
</ul></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/蕭嘉俊.md" title="wikilink">蕭嘉俊</a></strong></p></td>
<td><p><a href="../Page/多倫多.md" title="wikilink">多倫多</a></p></td>
<td><ul>
<li>2000年《<a href="../Page/新秀歌唱大賽多倫多選拔賽.md" title="wikilink">新秀歌唱大賽多倫多選拔賽</a>》入選決賽</li>
<li>曾在2005年加入<a href="../Page/無綫娛樂新聞台.md" title="wikilink">無綫娛樂新聞台</a></li>
<li>2006年回歸<a href="../Page/新時代電視.md" title="wikilink">新時代電視</a></li>
</ul></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/劉雨薇.md" title="wikilink">劉雨薇</a></strong></p></td>
<td><p><a href="../Page/多倫多.md" title="wikilink">多倫多</a></p></td>
<td><ul>
<li>也是<a href="../Page/加拿大中文電台.md" title="wikilink">加拿大中文電台AM</a>1430節目主持</li>
</ul></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/雷安娜.md" title="wikilink">雷安娜</a></strong></p></td>
<td><p><a href="../Page/溫哥華.md" title="wikilink">溫哥華</a></p></td>
<td><ul>
<li>80年代的著名前香港流行歌手</li>
<li>現為歌唱老師</li>
<li>為<a href="../Page/新秀歌唱大賽溫哥華選拔賽.md" title="wikilink">新秀歌唱大賽溫哥華選拔賽在幕後訓練參賽者</a></li>
</ul></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/盧玉鳳.md" title="wikilink">盧玉鳳</a></strong></p></td>
<td><p><a href="../Page/溫哥華.md" title="wikilink">溫哥華</a></p></td>
<td><ul>
<li>著名前<a href="../Page/香港.md" title="wikilink">香港唱片騎師</a></li>
<li>曾是<a href="../Page/華僑之聲.md" title="wikilink">華僑之聲的</a><a href="../Page/唱片騎師.md" title="wikilink">唱片騎師</a></li>
<li>現<a href="../Page/加拿大中文電台AM1470.md" title="wikilink">加拿大中文電台AM1470的唱片騎師</a></li>
</ul></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/張文謙_(加拿大).md" title="wikilink">張文謙</a></strong></p></td>
<td><p><a href="../Page/溫哥華.md" title="wikilink">溫哥華</a></p></td>
<td><ul>
<li>前<a href="../Page/加拿大中文電台.md" title="wikilink">加拿大中文電台</a><a href="../Page/唱片騎師.md" title="wikilink">唱片騎師</a></li>
</ul></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/何活權.md" title="wikilink">何活權</a></strong></p></td>
<td><p><a href="../Page/溫哥華.md" title="wikilink">溫哥華</a></p></td>
<td><ul>
<li>也是<a href="../Page/加拿大中文電台AM1470.md" title="wikilink">加拿大中文電台AM1470節目主持</a></li>
</ul></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 參看

  - [新時代電視](../Page/新時代電視.md "wikilink")

## 外部連結

  - [大城小聚節目重温](http://www.fairchildtv.com/programarchive.php)
  - [大城小聚節目介紹](http://www.fairchildtv.com/show_reel_popup.php?url=/images/data/show_reel/reel_46.wmv)

[Category:新時代電視](../Category/新時代電視.md "wikilink")
[Category:新時代電視節目](../Category/新時代電視節目.md "wikilink")
[Category:温哥華電影](../Category/温哥華電影.md "wikilink")
[Category:多倫多文化](../Category/多倫多文化.md "wikilink")