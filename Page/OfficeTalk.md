**OfficeTalk**是由[施乐帕洛阿尔托研究中心的Clarence](../Page/施乐帕洛阿尔托研究中心.md "wikilink")
A.Ellis和Gary J.
Nutt等人于1970年代后期至1980年代中期开发的系列试验系统。\[1\]OfficeTalk在早期的[办公自动化领域和](../Page/办公自动化.md "wikilink")[工作流领域中产生了深远的影响](../Page/工作流.md "wikilink")。OfficeTalk系统是我们已知最早的在个人计算机上采用桌面[隐喻的](../Page/隐喻.md "wikilink")[图形用户界面系统](../Page/图形用户界面.md "wikilink")；同时它也是最早的支持工作流的系统之一。

OfficeTalk系列包括：OfficeTalk-Zero, Backtalk, OfficeTalk-P和OfficeTalk-D。

OfficeTalk采用的流程建模工具称之为ICN（Information Control Net）。

## 脚注

## 参见

  -
  -
[Category:办公室自动化软件](../Category/办公室自动化软件.md "wikilink")

1.