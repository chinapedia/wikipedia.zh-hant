**大樹町**（）位於[北海道](../Page/北海道.md "wikilink")[十勝綜合振興局南部](../Page/十勝綜合振興局.md "wikilink")。名稱來自[阿伊努語](../Page/阿伊努語.md "wikilink")「tay-ki-us-i」，意思為許多大樹生長的地方\[1\]或[跳蚤很多的地方](../Page/跳蚤.md "wikilink")。是[迷你排球的發源地](../Page/迷你排球.md "wikilink")。此外也標榜為「通往宇宙的町」，目前積極招攬航空和太空相關領域的實驗和飛行測試。

## 地理

西部是[日高山脈](../Page/日高山脈.md "wikilink")，東部為平原，並面向[太平洋](../Page/太平洋.md "wikilink")；沿海區域有[沼澤區域](../Page/沼澤.md "wikilink")。

## 歷史

  - 1906年4月1日：廣尾郡茂寄村、[當緣郡大樹村](../Page/當緣郡.md "wikilink")、歷舟村以及當緣村的部份區域[合併為廣尾郡茂寄村](../Page/市町村合併.md "wikilink")，並成為二級村。\[2\]
  - 1926年6月1日：茂寄村改名為廣尾村。\[3\]
  - 1928年10月1日：大樹村從廣尾村分割獨立設置村。
  - 1930年：[國鐵](../Page/國鐵.md "wikilink")[廣尾線通車](../Page/廣尾線.md "wikilink")，大樹車站開始營運。
  - 1949年8月20日：[忠類村從大樹村分割獨立設置](../Page/忠類村.md "wikilink")。
  - 1951年4月1日：改制為大樹町。
  - 1955年4月1日：[十勝郡大津村廢村](../Page/十勝郡.md "wikilink")，部份區域被併入。
  - 1987年：廣尾線停駛。

## 產業

主要產業為[漁業和](../Page/漁業.md "wikilink")[酪農](../Page/酪農.md "wikilink")。

## 交通

### 機場

  - [帶廣機場](../Page/帶廣機場.md "wikilink")（位於[帶廣市](../Page/帶廣市.md "wikilink")）

### 鐵路

  - [北海道旅客鐵道](../Page/北海道旅客鐵道.md "wikilink")
      - [廣尾線](../Page/廣尾線.md "wikilink")
        ：[大樹車站](../Page/大樹車站.md "wikilink")，已於1987年停駛。

### 道路

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><dl>
<dt>一般國道</dt>

</dl>
<ul>
<li>國道236號</li>
<li>國道336號</li>
</ul>
<dl>
<dt><a href="../Page/主要地方道.md" title="wikilink">主要地方道</a></dt>

</dl>
<ul>
<li>北海道道15號幕別大樹線</li>
<li>北海道道55號清水大樹線</li>
</ul></td>
<td><dl>
<dt><a href="../Page/都道府縣道.md" title="wikilink">道道</a></dt>

</dl>
<ul>
<li>北海道道210號尾田豐頃停車場線</li>
<li>北海道道319號生花大樹線</li>
<li>北海道道501號旭濱大樹停車場線</li>
<li>北海道道622號幸德大樹停車場線</li>
<li>北海道道657號美成忠類停車場線</li>
<li>北海道道773號萌和大樹停車場線</li>
<li>北海道道881號線</li>
<li>北海道道1002號光地園尾田線</li>
<li>北海道道1037號廣尾大樹線</li>
</ul></td>
</tr>
</tbody>
</table>

## 觀光景點

[Taiki_Aerospace_Research_Field.jpg](https://zh.wikipedia.org/wiki/File:Taiki_Aerospace_Research_Field.jpg "fig:Taiki_Aerospace_Research_Field.jpg")公園暨[飛行運動廣場](../Page/飛行運動.md "wikilink")（[JAXA大樹航空](../Page/JAXA.md "wikilink")[宇宙](../Page/宇宙.md "wikilink")[實驗場](../Page/實驗.md "wikilink")）\]\]

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><h3 id="觀光">觀光</h3>
<ul>
<li>神居古潭公園露營地</li>
<li>挖砂金體驗地</li>
<li>萠和森林公園
<ul>
<li>晩成海岸</li>
<li>晩成温泉</li>
<li>晩成露營地</li>
<li>晩成原生花園</li>
</ul></li>
<li>大樹町多功能航空公園</li>
</ul></td>
<td><h3 id="文化遺產"><a href="../Page/文化遺產.md" title="wikilink">文化遺產</a></h3>
<ul>
<li>大樹遺跡出土遺物（北海道指定有形文化財）：大樹町圖書館收藏</li>
<li><p>（北海道指定史跡）</p></li>
</ul></td>
</tr>
</tbody>
</table>

## 教育

### 高等學校

  - 道立北海道大樹高等學校

### 中學校

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>大樹町立尾田學校</li>
</ul></td>
<td><ul>
<li>大樹町立大樹中學校</li>
</ul></td>
</tr>
</tbody>
</table>

### 小學校

<table style="width:90%;">
<colgroup>
<col style="width: 45%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>大樹町立石坂小學校</li>
<li>大樹町立尾田小學校</li>
</ul></td>
<td><ul>
<li>大樹町立大樹小學校</li>
<li>大樹町立中島小學校</li>
</ul></td>
</tr>
</tbody>
</table>

## 姊妹市・友好都市

### 日本

  - [相馬市](../Page/相馬市.md "wikilink")（[福島縣](../Page/福島縣.md "wikilink")）

## 參考資料

## 外部連結

1.
2.
3.