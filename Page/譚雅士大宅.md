[Jessville.JPG](https://zh.wikipedia.org/wiki/File:Jessville.JPG "fig:Jessville.JPG")128號的Jessville\]\]
[Jessville2.jpg](https://zh.wikipedia.org/wiki/File:Jessville2.jpg "fig:Jessville2.jpg")

**譚雅士大宅**（，又稱**薄扶林道128號**）位於[香港](../Page/香港.md "wikilink")[薄扶林](../Page/薄扶林.md "wikilink")[薄扶林道](../Page/薄扶林道.md "wikilink")128號，大宅連花園佔地逾3萬2,000多[平方呎](../Page/平方呎.md "wikilink")，由香港前首席大法官[楊鐵樑爵士的岳父](../Page/楊鐵樑.md "wikilink")[譚雅士於](../Page/譚雅士.md "wikilink")1929年購入地皮，於1931年建成\[1\]，大宅名稱「Jessville」取自譚雅士愛妻名字杜佩珍（）。譚雅士一直居住在大宅直到1970年左右為止。

譚雅士是執業[大律師](../Page/大律師.md "wikilink")，二戰前曾任[立法局和](../Page/立法局.md "wikilink")[市政局非官守議員](../Page/市政局.md "wikilink")、[東華醫院首總理](../Page/東華醫院.md "wikilink")、[保良局主席等職](../Page/保良局.md "wikilink")，戰後曾任[中央裁判司署裁判司和](../Page/中央裁判司署.md "wikilink")[香港大學終身校董](../Page/香港大學.md "wikilink")。\[2\]

## 外觀及景色

Jessville為一座兩層高建築，外觀呈灰色，建築具意大利文藝復興時代色彩，亦同時揉合了[裝飾派的藝術特色](../Page/裝飾派.md "wikilink")，包括具有意大利[文藝復興時期建築特色的圓拱頂](../Page/文藝復興.md "wikilink")，又有[新古典主義的門廊](../Page/新古典主義.md "wikilink")、欄杆、裝飾用扶手、三角牆、挑檐、基座，同時糅合[裝飾藝術風格的線條](../Page/裝飾藝術.md "wikilink")，在香港已相當少見。平面為[柏拉蒂奧式設計](../Page/帕拉第奧式建築.md "wikilink")，極為對稱。大宅樓底極高，建有寬闊的柱廊；屋頂上建有一座中式凉亭，風格中西合璧。大宅周圍的鐵欄上有雞隻以及犬隻的圖案。大宅前的花園，花草仍有工人打理，但噴泉則已沒有運作；旁邊則有一座L形的工人宿舍附屬建築。從大宅的閘口向南可遠眺[東博寮海峽以及](../Page/東博寮海峽.md "wikilink")[南丫島](../Page/南丫島.md "wikilink")。建築反映了戰前香港的建築風格和技術，及殖民地時代上流社會品味。\[3\]

## 未來發展

大宅現時由譚雅士身為[建築師的兒子](../Page/建築師.md "wikilink")[譚正擔任董事的一家私人公司擁有](../Page/譚正.md "wikilink")，早前先後兩次申請清拆，[屋宇署將此個案轉交](../Page/屋宇署.md "wikilink")[古物古蹟辦事處跟進](../Page/古物古蹟辦事處.md "wikilink")，研究需否列作古蹟保留\[4\]，於2007年4月20日宣佈列為[暫定古蹟](../Page/暫定古蹟.md "wikilink")，暫緩拆卸一年以便與業權擁有人共同協商未來用途。2008年1月，[發展局局長](../Page/發展局局長.md "wikilink")[林鄭月娥表示經諮詢後認為Jessville大宅的歷史價值未足以成為](../Page/林鄭月娥.md "wikilink")[法定古蹟](../Page/法定古蹟.md "wikilink")，將會撤銷該大宅暫定古蹟的地位，改列為[三級歷史建築](../Page/三級歷史建築.md "wikilink")。業主亦同意保留歷史建築物，重新向[城市規劃委員會提出發展方案](../Page/城市規劃委員會.md "wikilink")，包括在原本土地發展住宅及保留大宅。

同年10月，[行政會議通過Jessville保育方案](../Page/行政會議.md "wikilink")，業主可以在該地界內興建的兩幢分別為17層及21層樓高住宅樓宇，地積比率為2.1，較[薄扶林分區規劃大綱圖的許可上限](../Page/薄扶林.md "wikilink")3為低。兩棟樓宇合共提供72個住宅單位、83個私家車車位、4個電單車車位及2個上落客貨車車位。發展條件是Jessville必須保留下來，並會活化作私人住宅的住客會所，當局規定業主日後要至少每周免費向公眾開放一次，讓市民可以入內觀賞這座大宅。

## 參見

  - [楊鐵樑](../Page/楊鐵樑.md "wikilink")
  - [薄扶林道](../Page/薄扶林道.md "wikilink")

## 注釋

## 參考資料

  - [樓宇名稱](http://www.rvd.gov.hk/en/doc/urban_2007.pdf)
  - Antiquities and Monuments Office,[Brief Information on Proposed
    Grade 3
    Items](https://web.archive.org/web/20120321223119/http://www.amo.gov.hk/form/Brief_Information_on_proposed_Grade_III_Items.pdf),2009,Hong
    Kong.

[Category:薄扶林](../Category/薄扶林.md "wikilink")
[Category:香港獨立住宅屋](../Category/香港獨立住宅屋.md "wikilink")
[Category:香港三級歷史建築](../Category/香港三級歷史建築.md "wikilink")
[Category:香港西式建築](../Category/香港西式建築.md "wikilink")

1.  薄扶林隱蔽大宅 暫列古蹟延命（明報，2007年4月21日，A9版）
2.  《譚雅士老先生仙遊》(載《工商日報》 09-04-1976, 第7頁)
3.  [萬呎大宅重門深鎖](http://www.gcmt.com/new/content.asp?lang=bi&section=bf&conid=co0000022898cm&otherinfo=200407)
4.  [楊鐵樑岳父古宅或列古蹟](http://the-sun.on.cc/channels/news/20050502/20050502014347_0001_1.html)