**8月4日**是[阳历年的第](../Page/阳历.md "wikilink")216天（[闰年是](../Page/闰年.md "wikilink")217天），离一年的结束还有149天。

## 大事记

### 1世紀

  - [70年](../Page/70年.md "wikilink")：[罗马帝国军队毁掉](../Page/罗马帝国.md "wikilink")[第二圣殿](../Page/第二圣殿.md "wikilink")。

### 6世紀

  - [598年](../Page/598年.md "wikilink")：[隋朝发兵](../Page/隋朝.md "wikilink")30万大军[攻打高句丽](../Page/隋与高句丽的战争.md "wikilink")。

### 16世紀

  - [1532年](../Page/1532年.md "wikilink")：[布列塔尼公国与](../Page/布列塔尼公国.md "wikilink")[法兰西王国合并](../Page/法兰西王国.md "wikilink")。
  - [1578年](../Page/1578年.md "wikilink")：[葡萄牙國王](../Page/葡萄牙.md "wikilink")[塞巴斯蒂昂率领军队与](../Page/塞巴斯蒂昂_\(葡萄牙\).md "wikilink")[摩洛哥军队交战](../Page/摩洛哥.md "wikilink")，在撤退时溺死于[马哈赞河](../Page/马哈赞河.md "wikilink")。

### 18世紀

  - [1704年](../Page/1704年.md "wikilink")：在[西班牙王位继承战争中](../Page/西班牙王位继承战争.md "wikilink")，[英国军队占领](../Page/英国.md "wikilink")[西班牙的](../Page/西班牙.md "wikilink")[直布罗陀](../Page/直布罗陀.md "wikilink")。
  - [1789年](../Page/1789年.md "wikilink")：[法国废除](../Page/法国.md "wikilink")[封建制](../Page/封建制.md "wikilink")。
  - [1791年](../Page/1791年.md "wikilink")：[奥斯曼帝国根据](../Page/奥斯曼帝国.md "wikilink")[西斯托瓦条约](../Page/西斯托瓦条约.md "wikilink")，把[罗马尼亚南部的](../Page/罗马尼亚.md "wikilink")[奥尔肖瓦割让给](../Page/奥尔肖瓦.md "wikilink")[奥地利](../Page/奥地利.md "wikilink")。

### 19世紀

  - [1820年](../Page/1820年.md "wikilink")：[中国](../Page/中国.md "wikilink")[河南许州](../Page/河南.md "wikilink")（今[许昌](../Page/许昌.md "wikilink")）发生6级[地震](../Page/地震.md "wikilink")。
  - [1876年](../Page/1876年.md "wikilink")：[凯尔文学院创立](../Page/凯尔文学院.md "wikilink")。

### 20世紀

  - [1914年](../Page/1914年.md "wikilink")：在[第一次世界大战中](../Page/第一次世界大战.md "wikilink")，[德國入侵](../Page/德國.md "wikilink")[比利時](../Page/比利時.md "wikilink")，[英国随即向德国宣战](../Page/英国.md "wikilink")。
  - [1926年](../Page/1926年.md "wikilink")：中共中央扩大会议发布《坚决清理贪污腐化分子》的通告，这是迄今为止发现的中共最早的反贪文件。
  - [1949年](../Page/1949年.md "wikilink")：[中华民国湖南省政府主席](../Page/中华民国.md "wikilink")[程潜和长沙守军将领](../Page/程潜.md "wikilink")[陈明仁向中国共产党投降](../Page/陳明仁_\(中國軍事家\).md "wikilink")，中国共产党占领长沙。
  - [1964年](../Page/1964年.md "wikilink")：[美国两艘](../Page/美国.md "wikilink")[驱逐舰据称在](../Page/驱逐舰.md "wikilink")[北部湾受到北越的第二次攻击](../Page/北部湾.md "wikilink")，引发[越南战争全面升级的](../Page/越南战争.md "wikilink")[北部湾事件发生](../Page/北部湾事件.md "wikilink")。
  - [1967年](../Page/1967年.md "wikilink")：[駐港英軍和](../Page/駐港英軍.md "wikilink")[香港警察搜查位於](../Page/香港警察.md "wikilink")[北角](../Page/北角.md "wikilink")[英皇道三座大廈內的左派陣地](../Page/英皇道.md "wikilink")，拘捕約30人。
  - [1971年](../Page/1971年.md "wikilink")：[中华人民共和国与](../Page/中华人民共和国.md "wikilink")[土耳其共和国建交](../Page/土耳其共和国.md "wikilink")。
  - [1975年](../Page/1975年.md "wikilink")：[“八·四”海事](../Page/“八·四”海事.md "wikilink"):[珠江](../Page/珠江.md "wikilink")[容桂水道两艘客轮相撞](../Page/容桂水道.md "wikilink")，437人遇难。
  - [1975年](../Page/1975年.md "wikilink")：恐怖组织[日本赤军策划的](../Page/日本赤军.md "wikilink")[马来西亚](../Page/马来西亚.md "wikilink")[吉隆坡事件](../Page/吉隆坡事件.md "wikilink")。
  - [1977年](../Page/1977年.md "wikilink")：[美国能源部建立](../Page/美国能源部.md "wikilink")。
  - [1984年](../Page/1984年.md "wikilink")：上沃尔特改名为[布基纳法索](../Page/布基纳法索.md "wikilink")。
  - [1986年](../Page/1986年.md "wikilink")：[中華民國推出](../Page/中華民國.md "wikilink")[國家標準中文交換碼](../Page/國家標準中文交換碼.md "wikilink")。
  - [1990年](../Page/1990年.md "wikilink")：第三届[同志运动会开幕](../Page/同志运动会.md "wikilink")。
  - [1991年](../Page/1991年.md "wikilink")：在[香港](../Page/香港.md "wikilink")，一群國商銀行存戶遊行示威。
  - [1998年](../Page/1998年.md "wikilink")：中国[长江学者奖励计划全面启动](../Page/长江学者奖励计划.md "wikilink")。
  - [2018年](../Page/2018年.md "wikilink"): 中国一艘江蘇漁船在浙江海域遇險，11人失蹤，3人獲救。

### 21世紀

  - [2002年](../Page/2002年.md "wikilink"):[港鐵](../Page/港鐵.md "wikilink")[將軍澳綫](../Page/將軍澳綫.md "wikilink")[北角站至](../Page/北角站.md "wikilink")[油塘站路段通車](../Page/油塘站.md "wikilink")，同日[港鐵](../Page/港鐵.md "wikilink")[觀塘綫的東面總站遷至](../Page/觀塘綫.md "wikilink")[油塘站](../Page/油塘站.md "wikilink")。
  - [2003年](../Page/2003年.md "wikilink")：[中国](../Page/中国.md "wikilink")[齐齐哈尔挖出](../Page/齐齐哈尔.md "wikilink")[日本侵华战争期间日军遗弃的](../Page/日本侵华战争.md "wikilink")[化学武器](../Page/化学武器.md "wikilink")，造成[化学毒剂泄漏事件](../Page/2003年侵华日军遗弃在华化学毒剂泄漏事件.md "wikilink")。
  - [2006年](../Page/2006年.md "wikilink")：新西兰第18任总督[西尔维亚·卡特赖特卸任](../Page/西尔维亚·卡特赖特.md "wikilink")。
  - [2007年](../Page/2007年.md "wikilink")：[美國職棒球員](../Page/美國職棒.md "wikilink")[貝瑞·邦茲以](../Page/貝瑞·邦茲.md "wikilink")755支[全壘打追平](../Page/全壘打.md "wikilink")[漢克阿倫所保持的美國職棒最新記錄](../Page/漢克阿倫.md "wikilink")。
  - [2008年](../Page/2008年.md "wikilink")：中国[新疆](../Page/新疆.md "wikilink")[喀什市](../Page/喀什市.md "wikilink")[公安边防支队遭受两名男子的驾车袭击](../Page/公安.md "wikilink")，共造成警察16人死亡、16人受伤。
  - [2010年](../Page/2010年.md "wikilink")：最後一屆供日校生應考的[香港中學會考放榜](../Page/香港中學會考.md "wikilink")。

## 出生

  - [1281年](../Page/1281年.md "wikilink")：[元武宗海山](../Page/元武宗.md "wikilink")，[元朝皇帝](../Page/元朝.md "wikilink")。（逝于[1311年](../Page/1311年.md "wikilink")）
  - [1521年](../Page/1521年.md "wikilink")：[烏爾班七世](../Page/烏爾班七世.md "wikilink")，[教宗](../Page/教宗.md "wikilink")。（逝于[1590年](../Page/1590年.md "wikilink")）
  - [1792年](../Page/1792年.md "wikilink")：[雪莱](../Page/雪莱.md "wikilink")，[英國詩人](../Page/英國.md "wikilink")。（逝于[1822年](../Page/1822年.md "wikilink")）
  - [1805年](../Page/1805年.md "wikilink")：[哈密顿](../Page/哈密顿.md "wikilink")，[愛爾蘭數學家](../Page/愛爾蘭.md "wikilink")、物理學家及天文學家。（逝于[1865年](../Page/1865年.md "wikilink")）
  - [1821年](../Page/1821年.md "wikilink")：[路易·威登](../Page/路易·威登.md "wikilink")，法国时尚设计师。（逝于[1892年](../Page/1892年.md "wikilink")）
  - [1893年](../Page/1893年.md "wikilink")：[湯用彤](../Page/湯用彤.md "wikilink")，[中國近代國學大師](../Page/中國.md "wikilink")。（逝于[1964年](../Page/1964年.md "wikilink")）
  - [1900年](../Page/1900年.md "wikilink")：[伊麗莎白·鮑斯-萊昂](../Page/伊麗莎白·鮑斯-萊昂.md "wikilink")，英國皇太后。（逝于[2002年](../Page/2002年.md "wikilink")）
  - [1900年](../Page/1900年.md "wikilink")：[田島鍋](../Page/田島鍋.md "wikilink")，日本[超級人瑞](../Page/超級人瑞.md "wikilink")，最後一位出生於19世紀的人類。（逝于[2018年](../Page/2018年.md "wikilink")）
  - [1901年](../Page/1901年.md "wikilink")：[路易斯·阿姆斯壯](../Page/路易斯·阿姆斯壯.md "wikilink")，[美国](../Page/美国.md "wikilink")[爵士樂音乐家](../Page/爵士樂.md "wikilink")。（逝于[1971年](../Page/1971年.md "wikilink")）
  - [1920年](../Page/1920年.md "wikilink")：[海倫·湯瑪斯](../Page/海倫·湯瑪斯.md "wikilink")，美國白宮記者。（逝于[2013年](../Page/2013年.md "wikilink")）
  - [1928年](../Page/1928年.md "wikilink")：[卡达尔·弗洛拉](../Page/卡达尔·弗洛拉.md "wikilink")，[匈牙利](../Page/匈牙利.md "wikilink")[女演员](../Page/演员.md "wikilink")（[2002年逝世](../Page/2002年.md "wikilink")）
  - [1929年](../Page/1929年.md "wikilink")：[亚西尔·阿拉法特](../Page/亚西尔·阿拉法特.md "wikilink")，巴勒斯坦政治家。（逝于[2004年](../Page/2004年.md "wikilink")）
  - [1932年](../Page/1932年.md "wikilink")：[梁从诫](../Page/梁从诫.md "wikilink")，中国环保人士。（逝于[2010年](../Page/2010年.md "wikilink")）
  - [1953年](../Page/1953年.md "wikilink")：[魏翰楷](../Page/魏翰楷.md "wikilink")，德國外交官、前駐華[公使](../Page/公使.md "wikilink")。
  - [1956年](../Page/1956年.md "wikilink")：[土井美加](../Page/土井美加.md "wikilink")，[日本動畫](../Page/日本動畫.md "wikilink")[聲優](../Page/聲優.md "wikilink")
  - [1961年](../Page/1961年.md "wikilink")：[贝拉克·奥巴马](../Page/贝拉克·奥巴马.md "wikilink")，[美國總統](../Page/美國總統.md "wikilink")
  - [1962年](../Page/1962年.md "wikilink")：[羅傑·克萊門斯](../Page/羅傑·克萊門斯.md "wikilink")，美国棒球投手
  - [1981年](../Page/1981年.md "wikilink")：[萨塞克斯公爵夫人梅根](../Page/萨塞克斯公爵夫人梅根.md "wikilink")，英国皇室成员
  - [1982年](../Page/1982年.md "wikilink")：[盧卡·安東尼尼](../Page/盧卡·安東尼尼.md "wikilink")，[意大利足球運動員](../Page/意大利.md "wikilink")
  - [1987年](../Page/1987年.md "wikilink")：[張根碩](../Page/張根碩.md "wikilink")，[韓國演員](../Page/韓國.md "wikilink")
  - [1994年](../Page/1994年.md "wikilink")：[福田麻由子](../Page/福田麻由子.md "wikilink")，[日本演員](../Page/日本.md "wikilink")

## 逝世

  - [221年](../Page/221年.md "wikilink")：[文昭皇后甄氏](../Page/文昭甄皇后.md "wikilink")，[魏文帝曹丕的正妻](../Page/魏文帝.md "wikilink")，[魏明帝曹叡之生母](../Page/魏明帝.md "wikilink")。（[183年出生](../Page/183年.md "wikilink")）
  - [1060年](../Page/1060年.md "wikilink")：[亨利一世](../Page/亨利一世_\(法兰西\).md "wikilink")，[法國](../Page/法國.md "wikilink")[卡佩王朝國王](../Page/卡佩王朝.md "wikilink")。（[1008年出生](../Page/1008年.md "wikilink")）
  - [1578年](../Page/1578年.md "wikilink")：[塞巴斯蒂昂](../Page/塞巴斯蒂昂.md "wikilink")，[葡萄牙国王](../Page/葡萄牙.md "wikilink")。（[1554年出生](../Page/1554年.md "wikilink")）
  - [1875年](../Page/1875年.md "wikilink")：[安徒生](../Page/安徒生.md "wikilink")，[丹麦](../Page/丹麦.md "wikilink")[童话作家](../Page/童话.md "wikilink")。（[1805年出生](../Page/1805年.md "wikilink")）
  - [1941年](../Page/1941年.md "wikilink")：[许地山](../Page/许地山.md "wikilink")，现代[小说家](../Page/小說家_\(職業\).md "wikilink")、[散文家](../Page/散文家.md "wikilink")。（[1893年出生](../Page/1893年.md "wikilink")）
  - [1948年](../Page/1948年.md "wikilink")：[米列娃·馬利奇](../Page/米列娃·馬利奇.md "wikilink")，[南斯拉夫数学家](../Page/南斯拉夫.md "wikilink")，[阿尔伯特·爱因斯坦的第一位夫人](../Page/阿尔伯特·爱因斯坦.md "wikilink")。（[1875年出生](../Page/1875年.md "wikilink")）
  - [1966年](../Page/1966年.md "wikilink")：[曹汝霖](../Page/曹汝霖.md "wikilink")，清末民初的中國政治家。（[1877年出生](../Page/1877年.md "wikilink")）
  - [1997年](../Page/1997年.md "wikilink")：[雅娜·卡爾芒](../Page/雅娜·卡爾芒.md "wikilink")，已证的最长寿者。（[1875年出生](../Page/1875年.md "wikilink")）
  - [2008年](../Page/2008年.md "wikilink")：[阿卜杜勒·瓦哈博·巴](../Page/阿卜杜勒·瓦哈博·巴.md "wikilink")，[非洲田径协会秘书长](../Page/非洲田径协会.md "wikilink")。（[1942年出生](../Page/1942年.md "wikilink")）
  - [2013年](../Page/2013年.md "wikilink")：[雷納托·魯傑羅](../Page/雷納托·魯傑羅.md "wikilink")，義大利政治人物。（[1930年出生](../Page/1930年.md "wikilink")）
  - [2015年](../Page/2015年.md "wikilink")：[林家聲](../Page/林家聲.md "wikilink")，[香港](../Page/香港.md "wikilink")[粵劇演員](../Page/粵劇.md "wikilink")（[1933年出生](../Page/1933年.md "wikilink")）
  - [2016年](../Page/2016年.md "wikilink")：[徐三泰](../Page/徐三泰.md "wikilink")，[台灣企業家](../Page/台灣.md "wikilink")（[1983年出生](../Page/1983年.md "wikilink")）

## 节假日和习俗

  - [布基纳法索](../Page/布基纳法索.md "wikilink")：国庆日
  - [日本](../Page/日本.md "wikilink")：筷子节

## 參考資料

## 外部連結