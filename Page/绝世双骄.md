，又名《小雙俠闖江湖》，以[古龙](../Page/古龙.md "wikilink")[小说](../Page/小说.md "wikilink")《[绝代双骄](../Page/绝代双骄.md "wikilink")》为改編，主要讲述二十年后[花无缺与](../Page/花无缺.md "wikilink")[小鱼兒的后代们之间的故事](../Page/小鱼兒.md "wikilink")。[華視首播期間為](../Page/華視.md "wikilink")2002年1月18日至2002年3月15日的每週一至週五的[八點檔時段](../Page/八點檔.md "wikilink")，[香港](../Page/香港.md "wikilink")[亞洲電視以](../Page/亞洲電視.md "wikilink")《小雙俠闖江湖》名義於2003年5月5日首播，共40集\[1\]。由[賴水清](../Page/賴水清.md "wikilink")、[夏玉順導演](../Page/夏玉順.md "wikilink")，[林志颖](../Page/林志颖.md "wikilink")、[TAE](../Page/TAE.md "wikilink")、[徐錦江](../Page/徐錦江.md "wikilink")、[李小璐](../Page/李小璐.md "wikilink")、[鄭國霖](../Page/鄭國霖.md "wikilink")、[張瑞竹](../Page/張瑞竹.md "wikilink")、[蕭薔](../Page/蕭薔.md "wikilink")、[天心](../Page/天心_\(藝人\).md "wikilink")、[恬妞](../Page/恬妞.md "wikilink")、[吴启华](../Page/吴启华.md "wikilink")、[-{于}-莉](../Page/于莉.md "wikilink")、[马景涛](../Page/马景涛.md "wikilink")、[葛蕾主演](../Page/葛蕾.md "wikilink")。

## 劇情大綱

　　二十年前。江湖上出現了一名武功極為高強的東瀛刀客，為求武道而挑戰中土英雄，四大門派掌門皆斃於其刀下。武功已臻化境之江小魚及花無缺為息武林紛爭，決戰東瀛刀客，但雙雙失蹤，東瀛刀客亦銷聲江湖，傳聞二人為武林殉難，成了一代神話！同時，二人的兒子卻流落江湖，不知所終……

　　倏忽廿載……

　　二人的後人長大了，到底，他們會是怎麼樣的人？上一代的兩兄弟，從仇恨開始，卻以親情作結，到底，這代的兩兄弟，會否重蹈父親們的覆徹，成了宿世的仇敵？抑或成了一雙好友？一切一切，將慢慢的在這裡解開……

　　龍蛇混雜的地方，由江南【龍陽鎮】一小幫派魚老大所經營。魚老大育有一子，年約二十，人稱小魚。由於在這複雜環境下長大，形成小魚性格古惑佻脫，小魚武功雖只一般，人卻是聰明絕頂，更以捉弄人為樂，身邊之人皆怕了他，活像當年的江小魚一樣……

一座環境清幽的大山，山上巨木參天，人煙稀少，村民生性純樸，以樵獵為生。當中住了一對尋常樵夫，二人育有一子，名無忌。無忌外表雖是一名很平凡的樵夫，卻原來他身懷不俗的劍藝。半年前，無忌在家中雜物裡，偶然發現了一劍譜，無忌甫一接觸，便已著迷，夜間自己偷偷練起來，只半年間，劍術竟已有相當造詣，可見其天份之高！

　　平靜的日子不長久！一場無名的殺戮，降臨在這遺世的村莊上！無忌練劍回來，竟發現父母雙雙倒在血泊中。無忌痛不欲生，他撿起了一塊相信是兇手留下的玉珮，毅然挑起行囊，往尋找那殺父仇人！他卻不知，這一離開，將改變他的一生……無忌追查到鎮上，發現玉珮原屬魚所有，即要殺小魚報仇。一對素未謀面的年青人，甫一見面便作出生死鬥，是偶然，還命運，還是宿命的撥弄……

## 演员表

<table>
<tbody>
<tr class="odd">
<td><p><strong>演员</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>配音</strong></p></td>
<td><p><strong>备注</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/林志颖.md" title="wikilink">林志颖</a></p></td>
<td><p>江小鱼</p></td>
<td><p><a href="../Page/何志威.md" title="wikilink">何志威</a></p></td>
<td><p>晓风之丈夫，江飛鱼之父 。</p></td>
</tr>
<tr class="odd">
<td><p>江飞鱼</p></td>
<td><p>晓风與江小鱼之子</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/TAE.md" title="wikilink">TAE</a></p></td>
<td><p>无忌</p></td>
<td><p><a href="../Page/屈中恒.md" title="wikilink">屈中恒</a></p></td>
<td><p>误以为风无缺之子，实为岳龍軒之子</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李小璐.md" title="wikilink">李小璐</a></p></td>
<td><p>紫嫣</p></td>
<td><p><a href="../Page/林芳雪.md" title="wikilink">林芳雪</a></p></td>
<td><p>明月宫主侍女兼弟子。鶴雲天之女。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/吴天心.md" title="wikilink">吴天心</a></p></td>
<td><p>宇文霜</p></td>
<td><p><a href="../Page/刘小芸.md" title="wikilink">刘小芸</a></p></td>
<td><p>宇文普之女，不二庄大小姐</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/吴启华.md" title="wikilink">吴启华</a></p></td>
<td><p>常春</p></td>
<td><p><a href="../Page/康殿宏.md" title="wikilink">康殿宏</a></p></td>
<td><p>不二庄的御用大夫。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/于莉.md" title="wikilink">于莉</a></p></td>
<td><p>明月宫主</p></td>
<td><p><a href="../Page/王华怡.md" title="wikilink">王华怡</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/郑国霖.md" title="wikilink">郑国霖</a></p></td>
<td><p>小江</p></td>
<td><p><a href="../Page/康殿宏.md" title="wikilink">康殿宏</a></p></td>
<td><p>風無缺之子。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/张瑞竹.md" title="wikilink">张瑞竹</a></p></td>
<td><p>雪雨</p></td>
<td><p><a href="../Page/王华怡.md" title="wikilink">王华怡</a></p></td>
<td><p>岳龙轩之養女。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/徐锦江.md" title="wikilink">徐锦江</a></p></td>
<td><p>鱼老大</p></td>
<td><p><a href="../Page/雷威远.md" title="wikilink">雷威远</a></p></td>
<td><p>小鱼之养父，大赌坊老板。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/何嘉文.md" title="wikilink">何嘉文</a></p></td>
<td><p>豆豆</p></td>
<td><p><a href="../Page/林芳雪.md" title="wikilink">林芳雪</a></p></td>
<td><p>小鱼之青梅竹马。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/王道.md" title="wikilink">王道</a></p></td>
<td><p>岳龙轩</p></td>
<td><p><a href="../Page/徐健春.md" title="wikilink">徐健春</a><br />
<a href="../Page/杨少文.md" title="wikilink">杨少文</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/顾冠忠.md" title="wikilink">顾冠忠</a></p></td>
<td><p>宇文普</p></td>
<td><p><a href="../Page/沈光平.md" title="wikilink">沈光平</a></p></td>
<td><p>不二庄庄主，宇文霜之父。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黄仲昆.md" title="wikilink">黄仲昆</a></p></td>
<td><p>燕浩天</p></td>
<td><p><a href="../Page/王希华.md" title="wikilink">王希华</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黄淳隆.md" title="wikilink">黄淳隆</a></p></td>
<td><p>段天宝</p></td>
<td><p><a href="../Page/王希华.md" title="wikilink">王希华</a><br />
<a href="../Page/曹冀鲁.md" title="wikilink">曹冀鲁</a></p></td>
<td><p>于文普大弟子，不二庄大师兄。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陈观泰.md" title="wikilink">陈观泰</a></p></td>
<td><p>叶天照</p></td>
<td><p><a href="../Page/沈光平.md" title="wikilink">沈光平</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/刘致妤.md" title="wikilink">刘致妤</a></p></td>
<td><p>东维</p></td>
<td><p><a href="../Page/王华怡.md" title="wikilink">王华怡</a></p></td>
<td><p>明月宫主侍女兼弟子</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/杨泽中.md" title="wikilink">杨泽中</a></p></td>
<td><p>鹤云天</p></td>
<td><p><a href="../Page/王希华.md" title="wikilink">王希华</a><br />
<a href="../Page/曹冀鲁.md" title="wikilink">曹冀鲁</a></p></td>
<td><p>武林盟主，华山派掌门。紫嫣之父。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/恬妞.md" title="wikilink">恬妞</a></p></td>
<td><p>朱晓彤</p></td>
<td><p><a href="../Page/崔帼夫.md" title="wikilink">崔帼夫</a></p></td>
<td><p>易楼老板</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/葛蕾.md" title="wikilink">葛蕾</a></p></td>
<td><p>杜巧巧</p></td>
<td><p><a href="../Page/崔帼夫.md" title="wikilink">崔帼夫</a></p></td>
<td><p>十邪恶人之一</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/尤雅.md" title="wikilink">尤雅</a></p></td>
<td><p>墨冰</p></td>
<td><p><a href="../Page/王华怡.md" title="wikilink">王华怡</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/力牛.md" title="wikilink">力牛</a></p></td>
<td><p>马总管</p></td>
<td><p><a href="../Page/雷威远.md" title="wikilink">雷威远</a></p></td>
<td><p>不二庄总管</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/王正权.md" title="wikilink">王正权</a></p></td>
<td><p>元裘</p></td>
<td><p><a href="../Page/曹冀鲁.md" title="wikilink">曹冀鲁</a></p></td>
<td><p>不二庄弟子，天下第三庄少主，小鱼和无忌之友</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/朱晏.md" title="wikilink">朱晏</a></p></td>
<td><p>晓风</p></td>
<td><p><a href="../Page/王华怡.md" title="wikilink">王华怡</a></p></td>
<td><p>江飞鱼之妻，江小鱼之母</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/金巧巧.md" title="wikilink">金巧巧</a></p></td>
<td><p>郭若兰</p></td>
<td><p><a href="../Page/刘小芸.md" title="wikilink">刘小芸</a></p></td>
<td><p>风无缺之妻</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/岳跃利.md" title="wikilink">岳跃利</a></p></td>
<td><p>无忌养父</p></td>
<td><p><a href="../Page/陈宗岳.md" title="wikilink">陈宗岳</a></p></td>
<td><p>樵夫</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/戴春荣.md" title="wikilink">戴春荣</a></p></td>
<td><p>无忌养母</p></td>
<td><p><a href="../Page/崔帼夫.md" title="wikilink">崔帼夫</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/董志强.md" title="wikilink">董志强</a></p></td>
<td><p>李虎</p></td>
<td><p><a href="../Page/康殿宏.md" title="wikilink">康殿宏</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/金书贵.md" title="wikilink">金书贵</a></p></td>
<td><p>七叔</p></td>
<td><p><a href="../Page/康殿宏.md" title="wikilink">康殿宏</a></p></td>
<td><p>鱼家管家</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/吴强.md" title="wikilink">吴强</a></p></td>
<td><p>雷剑霹</p></td>
<td><p><a href="../Page/曹冀鲁.md" title="wikilink">曹冀鲁</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/王正佳.md" title="wikilink">王正佳</a></p></td>
<td><p>羽剑风</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/卢勇_(演员).md" title="wikilink">卢勇</a></p></td>
<td><p>宇文及</p></td>
<td><p><a href="../Page/徐健春.md" title="wikilink">徐健春</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/蒋毅.md" title="wikilink">蒋毅</a></p></td>
<td><p>周力</p></td>
<td><p><a href="../Page/曹冀鲁.md" title="wikilink">曹冀鲁</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/马景涛.md" title="wikilink">马景涛</a></p></td>
<td><p>霸刀</p></td>
<td><p><a href="../Page/王希华.md" title="wikilink">王希华</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/苏有朋.md" title="wikilink">苏有朋</a></p></td>
<td><p>风无缺</p></td>
<td><p><a href="../Page/郭如舜.md" title="wikilink">郭如舜</a></p></td>
<td><p>郭若兰之丈夫，江飞鱼兄弟</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/萧蔷.md" title="wikilink">萧蔷</a></p></td>
<td><p>燕飘香</p></td>
<td><p><a href="../Page/刘小芸.md" title="wikilink">刘小芸</a></p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 注釋

## 外部連結

  - [華視文化公司影片中心《絕世雙驕》簡介](https://web.archive.org/web/20110129004408/http://www.cts.com.tw/ctse/film2_15.asp?fm=b)

[J绝](../Category/2001年中國電視劇集.md "wikilink")
[J绝](../Category/2002年台灣電視劇集.md "wikilink")
[J绝](../Category/华视电视剧.md "wikilink")
[Category:絕代雙驕改編電視劇](../Category/絕代雙驕改編電視劇.md "wikilink")
[Category:電視劇續集](../Category/電視劇續集.md "wikilink")

1.  [華視《絕世雙驕》各集劇情介紹](http://www.cts.com.tw/ascx/History_Prog_Show.aspx?progno=051776)