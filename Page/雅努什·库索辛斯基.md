**雅努什·库索辛斯基**（，）是[波兰中長跑运动员](../Page/波兰.md "wikilink")，该国首枚[奥运会金牌得主](../Page/奥运会.md "wikilink")，民族英雄。

库索辛斯基出生于[华沙的一个铁路工人家庭](../Page/华沙.md "wikilink")，早年从事[足球运动](../Page/足球.md "wikilink")，1928年开始接受长跑训练，在其后的数年中，他包揽了波兰国内各项长跑冠军。1932年，他打破了男子3000米世界纪录，成绩是8:18.8。在[1932年夏季奥林匹克运动会上](../Page/1932年夏季奥林匹克运动会.md "wikilink")，库索辛斯基克服了鞋子磨脚带来的剧痛，险胜两位[芬兰选手](../Page/芬兰.md "wikilink")，获得了男子10000米长跑的金牌，打破了芬兰对这个项目的垄断。

1939年[纳粹德国入侵波兰后](../Page/纳粹德国.md "wikilink")，库索辛斯基志愿从军，先后两次负伤。波兰亡国后，库索辛斯基奉流亡政府之命在华沙潜伏下来开展破坏活动。1940年3月，他在[盖世太保的](../Page/盖世太保.md "wikilink")“[AB行动](../Page/AB行动.md "wikilink")”中被捕。库索辛斯基拒绝投降，于6月21日被枪决于华沙郊外。

## 參考資料

## 外部連結

  - [波兰奥委会资料](https://web.archive.org/web/20080514034002/http://www.olimpijski.pl/294_1861.html)

[Category:波兰奥运田径运动员](../Category/波兰奥运田径运动员.md "wikilink")
[Category:波蘭奧林匹克運動會金牌得主](../Category/波蘭奧林匹克運動會金牌得主.md "wikilink")
[Category:1932年夏季奥林匹克運動會田徑運動員](../Category/1932年夏季奥林匹克運動會田徑運動員.md "wikilink")
[Category:1932年夏季奧林匹克運動會獎牌得主](../Category/1932年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:奥林匹克运动会田径金牌得主](../Category/奥林匹克运动会田径金牌得主.md "wikilink")
[Category:前田徑項目世界記錄保持者](../Category/前田徑項目世界記錄保持者.md "wikilink")