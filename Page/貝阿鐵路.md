[3TE25K2M-0001_with_train.jpg](https://zh.wikipedia.org/wiki/File:3TE25K2M-0001_with_train.jpg "fig:3TE25K2M-0001_with_train.jpg")
[Map_Trans-Siberian_railway.png](https://zh.wikipedia.org/wiki/File:Map_Trans-Siberian_railway.png "fig:Map_Trans-Siberian_railway.png")，綠線為[貝阿鐵路](../Page/貝阿鐵路.md "wikilink")\]\]
**貝阿鐵路**，全名**貝加爾－阿穆爾鐵路**（），全長4,234公里。是[蘇聯為應二战前](../Page/蘇聯.md "wikilink")[远东的緊張局势以及](../Page/远东.md "wikilink")1970年代中苏重兵对峙局势，抢建的战略性鐵路
(因為原[西伯利亞鐵路的正線相當接近](../Page/西伯利亞鐵路.md "wikilink")[中國邊境](../Page/中國.md "wikilink"))。新線與原線相距610-770km
。

## 線路

由[西伯利亚铁路](../Page/西伯利亚铁路.md "wikilink")[泰舍特站出發](../Page/泰舍特站.md "wikilink")，在[布拉茨克過](../Page/布拉茨克.md "wikilink")[安加拉河](../Page/安加拉河.md "wikilink")，在[乌斯季库特过](../Page/乌斯季库特.md "wikilink")[勒拿河](../Page/勒拿河.md "wikilink")，在[北贝加尔斯克經過](../Page/北贝加尔斯克.md "wikilink")[貝加爾湖北端](../Page/貝加爾湖.md "wikilink")，在[阿穆尔河畔共青城過](../Page/阿穆尔河畔共青城.md "wikilink")[黑龍江](../Page/黑龍江.md "wikilink")。終點是[韃靼海峽上的](../Page/韃靼海峽.md "wikilink")[蘇維埃港](../Page/蘇維埃港.md "wikilink")。
21座隧道总长47
km；超过4,200座桥梁总长超过400km。从[泰舍特到](../Page/泰舍特.md "wikilink")1469km为电气化铁路。绝大部分线路为单轨，预留复线条件。

在[滕达与](../Page/滕达.md "wikilink")干线相交汇。该线与[西伯利亚铁路](../Page/西伯利亚铁路.md "wikilink")7273km处的Bamovskaya出岔至[滕达这一段](../Page/滕达.md "wikilink")，在1972年4月5日重建时曾被称为“小贝阿铁路”。

[贝阿铁路沿线穿越](../Page/贝阿铁路.md "wikilink")[外兴安岭等一系列大小山脉](../Page/外兴安岭.md "wikilink")，但新修段3145千米中[隧道长度仅](../Page/隧道.md "wikilink")26.3千米，而且这26.3千米仅由4条长[隧道组成](../Page/隧道.md "wikilink")。

开通了莫斯科直达[雅库茨克的铁路客运列车](../Page/雅库茨克.md "wikilink")。[泰舍特至](../Page/泰舍特.md "wikilink")[腾达运行](../Page/腾达.md "wikilink")48小时。

沿途有多条煤矿或水电站铁路支线。如埃尔加(Elga或Elginskoye)煤矿探明储量22亿吨。焦煤生产企业于2012年1月建成贝阿铁路Ulak站至埃尔加的320km支线铁路。

  - 575km: Khrebtovaya至[乌斯季伊利姆斯克](../Page/乌斯季伊利姆斯克.md "wikilink"),
    214 km: 1970年开通, 服务于
  - 1257km: Novy Uoyan至贝加尔湖。
  - 2364km:
    [滕达至](../Page/滕达.md "wikilink")[西伯利亚铁路的Bamovskaya站](../Page/西伯利亚铁路.md "wikilink"),180 km，1933年至1937年建设。1942年拆除铁轨运往前线。1972年至1975年重建，称为小贝阿铁路.
  - 2364km:
    [滕达至](../Page/滕达.md "wikilink")[雅库茨克](../Page/雅库茨克.md "wikilink")。
  - 3315km: Novy Urgal至西伯利亚铁路的Izvestovskaya站,328 km:
    位于[布列亚河盆地](../Page/布列亚河.md "wikilink")，二战结束后由日本战俘建设。有一条32 km支线从Novy
    Urgal向北至[切格多门煤矿](../Page/切格多门.md "wikilink")
  - 3837km: [共青城至哈巴罗夫斯克](../Page/伯力－阿穆尔河畔共青城铁路.md "wikilink"), 374 km;
    99 km向南至[博隆湖](../Page/博隆湖.md "wikilink").
  - 51km (自共青城起始计算的里程): Selikhin至Cherny Mys, 122 km: 1950年至1953年建设。

贝加尔山隧道长6.7km，替代了1979年至1984年的15km越岭铁路。

## 历史

1880年代，该线路是[西伯利亞鐵路远东段的選線方案之一](../Page/西伯利亞鐵路.md "wikilink")。

1930年代初期，[泰舍特至](../Page/泰舍特.md "wikilink")[布拉茨克動工建设](../Page/布拉茨克.md "wikilink")，当时这段铁路不称作贝阿铁路。在1933年至1935年，贝阿铁路是指[西伯利亚铁路在](../Page/西伯利亚铁路.md "wikilink")[贝加尔湖以东的复线建设工程](../Page/贝加尔湖.md "wikilink")。

1945年，确定了现今的贝阿铁路的选线设计，承担重轴原油列车运输任务。[贝阿铁路的](../Page/贝阿铁路.md "wikilink")[布拉茨克至](../Page/布拉茨克.md "wikilink")[乌斯季库特](../Page/乌斯季库特.md "wikilink")（用于沟通[勒拿河水运](../Page/勒拿河.md "wikilink")）、[阿穆尔河畔共青城至](../Page/阿穆尔河畔共青城.md "wikilink")[苏维埃港段是](../Page/苏维埃港.md "wikilink")[二战胜利后驱使](../Page/二战.md "wikilink")[日本与](../Page/日本.md "wikilink")[德国](../Page/德国.md "wikilink")[战俘修建的](../Page/战俘.md "wikilink")。分别于1954年和1947建成通车。

1953至1974年間停工。

1973年，[苏联设立](../Page/苏联.md "wikilink")[贝阿铁路建设总局](../Page/贝阿铁路建设总局.md "wikilink")。新建[乌斯季库特至](../Page/乌斯季库特.md "wikilink")[阿穆尔河畔共青城段长](../Page/阿穆尔河畔共青城.md "wikilink")3,145公里，其中65%建在永久冻土地带。铁路穿越[外兴安岭山脉](../Page/外兴安岭.md "wikilink")，需要开凿众多长大隧道。整个工程由[苏军铁道兵](../Page/苏军铁道兵.md "wikilink")[部隊承建](../Page/部隊.md "wikilink")，耗资达54.36亿卢布（西方称造價140億[美元](../Page/美元.md "wikilink")），每年投资额占到[苏联年度基础建设投资总额的](../Page/苏联.md "wikilink")1%。列为两大重点工程之一。被[苏联领导人](../Page/苏联.md "wikilink")[勃列日涅夫公开宣称为](../Page/勃列日涅夫.md "wikilink")“世纪工程”。1974年3月起开始建设。1974年4月召开的[苏联列宁共青团十七大宣布贝阿铁路为](../Page/苏联共青团.md "wikilink")之一，担任共青团贝阿铁路建设指挥部总指挥。1974年底，156
000名团员青年报名，其中5万人获得批准参加建设。于1984年10月27日底竣工，1985年通车。然而到目前為止，很多配套工程至今仍未完工，[复线和](../Page/复线.md "wikilink")[电气化建设也尚待进行](../Page/电气化.md "wikilink")。

1996年，贝阿铁路作为独立运营结束，以[哈尼站为界](../Page/哈尼站.md "wikilink")，分别划归[东西伯利亚铁路局与](../Page/东西伯利亚铁路局.md "wikilink")[远东铁路局](../Page/远东铁路局.md "wikilink")。

2003年12月5日，长度为15343m的经过27年建设，通车运营。埋深达到了1.5km。被替代的越岭线路长达54km，由于限坡太大需要使用补机机车。

## 图集

Image:Tynda rail.jpg|[滕达火车站](../Page/滕达.md "wikilink")
Image:Vikhorevka.jpg|[维霍列夫卡火车站](../Page/维霍列夫卡.md "wikilink")
Image:Fevralsk_train_station,_Amur_region,_Russia.jpg|[Fevralsk火车站](../Page/Fevralsk.md "wikilink")
Image:Tayshet_old.jpg|[泰舍特站](../Page/泰舍特站.md "wikilink")

## 參見

  - [瓦尼诺－霍尔姆斯克铁路轮渡](../Page/瓦尼诺－霍尔姆斯克铁路轮渡.md "wikilink")

## 链接

  - [穿越西伯利亚的无产阶级列车](http://cn.nytimes.com/travel/20121017/c17Siberian/zh-hant/)

[Category:俄羅斯鐵路線](../Category/俄羅斯鐵路線.md "wikilink")