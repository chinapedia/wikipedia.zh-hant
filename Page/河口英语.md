**河口英语**（，又译**港湾英语**）是一種現代的[英語口音](../Page/英語.md "wikilink")，广泛流传于[东南英格兰及](../Page/东南英格兰.md "wikilink")[东英格兰地区](../Page/东英格兰.md "wikilink")，特别是這兩個地點的交匯點[泰晤士河沿岸和其河口](../Page/泰晤士河.md "wikilink")。

## 歷史

这种英语是[英国皇室的标准发音和](../Page/英国.md "wikilink")[英格兰东南地区](../Page/英格兰.md "wikilink")，特别是[伦敦](../Page/伦敦.md "wikilink")、[肯特和](../Page/肯特.md "wikilink")[艾塞克斯](../Page/埃塞克斯郡.md "wikilink")[方言的一种混合体](../Page/方言.md "wikilink")，大約於1980年代成型。這種語言在1984年10月首度引起公眾的關注，原因是David
Rosewarne在《[泰晤士報教育副刊](../Page/泰晤士報教育副刊.md "wikilink")》（，縮寫作**TES**）發表了一篇文章，認為這種新的口音「最終可能會取代RP成為英語發音的新標準」（）\[1\]。

過往，英國的上流社會從19世紀開始一直通行俗稱「女王英語」的「[公认發音](../Page/公认發音.md "wikilink")」（），縮寫作**RP**，即來自[牛津郡及](../Page/牛津郡.md "wikilink")[劍橋郡的口音](../Page/劍橋郡.md "wikilink")；但另一方面，在首都[倫敦本土的口音卻被認為是低俗的](../Page/倫敦.md "wikilink")[勞動階層口音](../Page/勞動階層.md "wikilink")，被冠以「Cockney」這個外號。慢慢的，隨着社會發展，在兩者之間開始出現一種新的口音，而這種口音成為了中層階層的常用口音，成為當地英語的一股新勢力。

研究顯示：河口英語不是英語的一個唯一連貫形式；相反的，現實在修建之後包括工人階級倫敦講話一些（但並非所有）語音特點社會上傳播以各種各樣的率入中產階級講話和地理上入東南英國其它口音。

一些人认为（通常是开玩笑）“河口英语”的语源来自于其特征：像泥浆一样“清晰”，和像河流一样“自由流动”（"It's as clear as
mud and flows freely"）。

## 特色

河口英语有以下[伦敦方言](../Page/伦敦方言.md "wikilink")（）的发音特点：

  - 當說出兩個母音相連的連音時，會[出現侵入性的/r/音](../Page/連音.md "wikilink")（又稱「插入r」）。

## 參看

  - [地方英語口音](../Page/地方英語口音.md "wikilink")
  - [英語種類列表](../Page/英語種類列表.md "wikilink")

## 參考

## 外部链接

  - [Estuary English](http://www.phon.ucl.ac.uk/home/estuary/home.htm)

[Category:英国语言](../Category/英国语言.md "wikilink")

1.  [Rosewarne, David (1984年). *Estuary English*. Times Educational
    Supplement, 19 (October
    1984)](http://www.phon.ucl.ac.uk/home/estuary/rosew.htm)