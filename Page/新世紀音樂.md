**新世纪音乐**（[英語](../Page/英語.md "wikilink")：**New Age
music**）又译作**新纪元音乐**，是一种在1970年代出现的一种音乐形式，最早用於幫助[冥思及洁浄](../Page/冥思.md "wikilink")[心灵](../Page/心灵.md "wikilink")，但許多后期的创作者已不再抱有这种出发点。另一种说法是：由于其丰富多彩、富于变换，不同于以前任何一种音乐；它并非单指一个类别，而是一个[范畴](../Page/范畴.md "wikilink")，一切不同以往，象征时代更替诠释精神[内涵的改良音乐都可归于此内](../Page/内涵.md "wikilink")，所以被命名为New
Age，即新世纪音乐。

## 歷史

20世纪70年代至80年代，[新纪元运动开始](../Page/新纪元运动.md "wikilink")，並開始提倡用这類型的音乐洁浄心灵。

## 特点

虽然新纪元音乐的内容广泛变幻多端，但仍可以找到一些特点：

  - 很少具有强烈的节奏
  - 大多有著輕柔悅耳的漂亮旋律
  - 少有刺激和短促的声音，接近[轻音乐](../Page/轻音乐.md "wikilink")
  - 经常在音乐裡利用人声合音与谐音

## 主要分类

### 演奏形式分類

  - 器乐演奏
      - 乐器独奏（Solo Instrumental）
      - 新古典乐派（Neo-classical）
      - 新原音乐派（New Acoustic）
      - 環境音樂（Ambient Music）
  - 电子合成
      - 电子乐派（Progressive Electronic）
      - 电子-原音乐派（Electro-Acoustic）
      - 科技-原始部落音乐（Techno-tribal）
  - 人声演唱

### 形體分類

  - 休闲音乐
  - 靈修音樂
  - 恢復音樂
  - 催眠音樂

第一類是用自然樂器演奏的，包括一般聽眾熟悉的常規樂器，如鋼琴、小提琴、吉他等等，其中被用得最多的一方面是接近東方樂器音色的樂器，如雙簧管、英國管等，另一方面，民間性質的樂器，如曼多林、手風琴、排簫等也常可聽到；有些演奏者特別喜歡東方的打擊樂器，如小鈴、大鑼和鐘等，現在還有些新世紀音樂家熱衷于把那些非洲的戰鼓等原始的樂器摻入他們的音樂中。

第二類則是電聲樂器，它所制造的音響也偏重神秘的、飄渺的色彩，許多新世紀音樂家都是電腦音樂高手。

第三類是前二者的混合。有些唱片商在制作這三類演奏形式時，往往還會加上自然界的音響，如鳥鳴林嘯、風聲鶴唳等等。

除了大自然之外，久遠而神秘的古代、純樸而素雅的民謠，都是新世紀藝術家們所熱衷的題材。

### 地方分類

  - 印度电子音乐

## 知名艺术家和艺术团体

### 西方

  - 英国
      - Medwyn Goodall一般译为梅得溫·古铎
      - [莎拉·布萊曼](../Page/莎拉·布萊曼.md "wikilink")（Sarah Brightman）
      - [麦克·欧菲尔德](../Page/麦克·欧菲尔德.md "wikilink")（[Mike
        Oldfield](../Page/Mike_Oldfield.md "wikilink")）
      - [卡爾·詹肯斯](../Page/卡爾·詹肯斯.md "wikilink")（[Karl
        Jenkins](../Page/Karl_Jenkins.md "wikilink")）

<!-- end list -->

  - 爱尔兰/挪威
      - [神秘园](../Page/神秘园.md "wikilink")，又譯秘密花園（Secret Garden）\[1\]

<!-- end list -->

  - 爱尔兰
      - [恩雅](../Page/恩雅.md "wikilink")（Enya）\[2\]
      - 菲爾·寇特（Phil Coulter）
      - [Clannad](../Page/Clannad_\(樂團\).md "wikilink")
  - 瑞士
      - [班德瑞](../Page/班德瑞.md "wikilink")（Bandari）
  - 西班牙
      - Psicodreamics（心理梦境）
  - 美国
      - [乔治·温斯顿](../Page/乔治·温斯顿.md "wikilink")（George Winston）
      - [凯文·柯恩](../Page/凱文·柯恩.md "wikilink")（Kevin Kern）
      - [喬許·葛洛班](../Page/喬許·葛洛班.md "wikilink")（Josh Groban）
      - 麥克·鍾斯（Michael Jones）
      - 比爾·道格拉斯（Bill Douglas）
      - 尼克拉斯·根（Nicholas Gunn）
      - 蘇珊·絲雅妮（Suzanne Ciani）
      - 大衛·蘭茲（David Lanz）
      - 大衛·阿肯斯頓（David Arkenstone）
      - 史提夫·洛奇（Steve Roach）
      - 韋恩·格勒茲（Wayne Gratz）
      - 保羅·溫特（Paul Winter）
      - 麥克·芝特（Michael Gettel）
      - 麥克·窩蘭（Michael Whalen）
      - 奧特瑪·利伯特（Ottmar Liebert）
      - 第三勢力（3rd Force）
      - 傑絲·庫克（Jesse Cook）
      - 埃里克·廷斯達與南茜·蘭波（Eric Tingstad & Nancy Rumbel）
      - 布勒利·約瑟夫（Bradley Joseph）
      - 約翰·提什（John Tesh）
      - 2002（乐团）
      - Mehdi（梅迪）
      - Checkfield（）
      - Jonn Serrie（）
      - Jim Brickman
  - 加拿大
      - [雷恩·寇伯](../Page/雷恩·寇伯.md "wikilink")（[Ron
        Korb](../Page/Ron_Korb.md "wikilink")）
      - [罗琳娜·麦肯尼特](../Page/罗琳娜·麦肯尼特.md "wikilink")（Loreena McKennitt）
      - [馬修·連恩](../Page/馬修·連恩.md "wikilink")（Matthew Lien）
  - 希臘
      - [范吉利斯](../Page/范吉利斯.md "wikilink")（Vangelis）
      - [雅尼](../Page/雅尼.md "wikilink")（Yanni）\[3\]
      - Chris Spheeris
  - 德国
      - [謎](../Page/谜_\(音乐团体\).md "wikilink")（ENIGMA）
      - 丛林深处（Deep Forest）
      - 格林高利合唱团（Gregorian）
      - [银杏花园](../Page/银杏花园.md "wikilink")（Ginkgo Garden）
      - 橘梦乐团（Tangerine Dream）
      - Jean Ven Robert Hal
      - Klaus Schulze
      - Cusco
      - Oliver Shanti
      - [Karunesh](../Page/Karunesh.md "wikilink")
      - Mars Lasar
      - Aschera
      - Christopher Franke
      - Yara
      - Bella Sonus
      - Bernward Koch
  - 法国
      - [让-米歇尔·雅尔](../Page/让-米歇尔·雅尔.md "wikilink")
      - [ERA](../Page/ERA.md "wikilink")\[4\]
  - 其他
      - Earl Klugh
      - Cera & Poggi
      - Rasa

### 亞洲

  - 中國大陸
      - [央金拉姆](../Page/央金拉姆.md "wikilink")
      - [朱哲琴](../Page/朱哲琴.md "wikilink")
      - [何訓田](../Page/何訓田.md "wikilink")
      - [林海](../Page/林海_\(作曲家\).md "wikilink")
      - [女子十二樂坊](../Page/女子十二樂坊.md "wikilink")
      - [薩頂頂](../Page/薩頂頂.md "wikilink")
      - 陸昆侖（[澳洲](../Page/澳洲.md "wikilink")[華僑](../Page/華僑.md "wikilink")）
      - 郭思达
      - [薛汀哲](../Page/薛汀哲.md "wikilink")

<!-- end list -->

  - 台灣
      - [李欣芸](../Page/李欣芸.md "wikilink")
      - 范宗沛
      - [郭英男](../Page/郭英男.md "wikilink")
      - 阮丹青
      - 林少英
      - 希客\[5\]
      - [V.K克](../Page/V.K克.md "wikilink")
      - [張穆庭](../Page/張穆庭.md "wikilink")
      - [Cicada](../Page/Cicada.md "wikilink")
  - 日本
      - [坂本龍一](../Page/坂本龍一.md "wikilink")
      - [喜多郎](../Page/喜多郎.md "wikilink")（Kitaro）
      - [神思者](../Page/神思者.md "wikilink")（S.E.N.S.）\[6\]
      - [姫神](../Page/姫神.md "wikilink")（Himekami）
      - [和平之月](../Page/和平之月.md "wikilink")（Pacific Moon）\[7\]
      - 黃永燦（Wong Wing Tsan）
      - [西村由紀江](../Page/西村由紀江.md "wikilink")
      - [宗次郎](../Page/宗次郎.md "wikilink")
      - [中村由利子](../Page/中村由利子.md "wikilink")
      - [中村幸代](../Page/中村幸代.md "wikilink")
      - [久石讓](../Page/久石讓.md "wikilink")
  - 韓國
      - [李閏珉](../Page/李閏珉.md "wikilink")（YIRUMA）\[8\]
      - The Daydream
      - The Rain
  - 印度
      - Karsh Kale
      - Namaste Flowering

### 大洋洲

  - 澳大利亞
      - Tony O'Connor
      - Terry Oldfield
  - 紐西蘭
      - [海莉·薇思特拉](../Page/海莉·薇思特拉.md "wikilink")（Hayley Westenra）

## 参考文献

## 参见

  - [新纪元运动](../Page/新纪元运动.md "wikilink")
  - [雅尼](../Page/雅尼.md "wikilink")

[新世紀音樂](../Category/新世紀音樂.md "wikilink")
[Category:新紀元運動](../Category/新紀元運動.md "wikilink")

1.  <http://www.secretgarden.no/>
2.  <http://www.enya.com/>
3.  <http://www.yanni.com/>
    雅尼是否属于新纪元音乐家存在争议，他本人似乎更倾向于称自己的音乐为[现代器乐](../Page/现代器乐.md "wikilink")（Contemporary
    Instrumental）
4.
5.  <http://hichung.googlepages.com/>
6.  <http://www.sens-company.com/>
7.  <http://www.pacificmoon.com/>
8.  <http://www.yiruma.com/>