《**面纱**》（*The Painted
Veil*）是2006年出品的一部[美国电影](../Page/美国.md "wikilink")。该片由[约翰·卡兰导演](../Page/约翰·卡兰.md "wikilink")，男女主演分别为[爱德华·诺顿和](../Page/爱德华·诺顿.md "wikilink")[娜奥米·沃茨](../Page/娜奥米·沃茨.md "wikilink")，同时[黄秋生](../Page/黄秋生.md "wikilink")、[夏雨和模特](../Page/夏雨_\(大陸\).md "wikilink")[吕燕也在片中出演角色](../Page/吕燕.md "wikilink")。

影片根据[英国作家](../Page/英国.md "wikilink")[威廉·索默赛特·毛姆的](../Page/威廉·索默赛特·毛姆.md "wikilink")[同名小说改编](../Page/面纱_\(小说\).md "wikilink")，讲述1920年代英国细菌学家沃特·费恩医生（Dr.
Walter Fane）与妻子基蒂·费恩（Kitty Fane）来到[中国的故事](../Page/中国.md "wikilink")。

## 故事概要

20年代的倫敦，虛榮女子Kitty
（[娜奧美·屈絲飾演](../Page/妮奥米·瓦兹.md "wikilink")）為了逃避浮华的上流社會，甘願選擇一段沒有愛情的婚姻，與細菌學家Walter（[愛德華·諾頓飾演](../Page/爱德华·诺顿.md "wikilink")）結婚並移居上海。後來Kitty因寂寞而搭上英國外交官，令Walter決定舉家搬到霍亂肆虐的農鄉，在當地行醫，作為對她的懲罰。每天與死亡和絕望擦身而過，夫妻倆卻發現了另一片天地，在遙遠的異鄉第一次走近對方，只是一切來得太遲……

## 获奖情况

[亚历山大·德斯普拉获得](../Page/亚历山大·德斯普拉.md "wikilink")[金球奖和](../Page/金球奖.md "wikilink")[洛杉矶影评人奖最佳原创音乐奖](../Page/洛杉矶影评人奖.md "wikilink")。

Ron Nyswaner获得[美国国家评论协会奖最佳改编剧本奖](../Page/美国国家评论协会奖.md "wikilink")。

影片同时在[独立精神奖中获得最佳剧本和最佳男主角两项提名](../Page/独立精神奖.md "wikilink")。

虽然该片在金球奖中获得了最佳原创音乐奖，但并未在[奥斯卡中获得提名](../Page/奥斯卡.md "wikilink")。

## 其他

  - 该片主要场景取景于[广西宜州市祥貝鄉段的](../Page/广西.md "wikilink")[龙江河](../Page/龙江河.md "wikilink")。
  - 在片中扮演外遇情人的[娜奥米·沃茨和](../Page/娜奥米·沃茨.md "wikilink")[里夫·施雷伯在实际生活中确实已订婚](../Page/里夫·施雷伯.md "wikilink")。

## 外部链接

  - [英文官方网站](https://web.archive.org/web/20070406232755/http://wip.warnerbros.com/paintedveil/)

  - [中文官方网站](https://web.archive.org/web/20070323183853/http://www.thepaintedveilmovie.com.cn/paintedveil/index.html)

  - [官方预告片](http://pdl.warnerbros.com/wip/us/med/paintedveil/painted_veil_tlr1_qt_700.mov)
    (高分辨率)

  - [官方预告片](http://pdl.warnerbros.com/wip/us/med/paintedveil/painted_veil_tlr1_qt_300.mov)
    (中分辨率)

  -
[Category:2006年电影](../Category/2006年电影.md "wikilink")
[Category:美國浪漫劇情片](../Category/美國浪漫劇情片.md "wikilink")
[Category:2000年代劇情片](../Category/2000年代劇情片.md "wikilink")
[Category:小說改編電影](../Category/小說改編電影.md "wikilink")
[Category:華納獨立電影](../Category/華納獨立電影.md "wikilink")
[Category:倫敦背景電影](../Category/倫敦背景電影.md "wikilink")
[Category:上海背景電影](../Category/上海背景電影.md "wikilink")
[Category:中國背景電影](../Category/中國背景電影.md "wikilink")
[Category:中國取景電影](../Category/中國取景電影.md "wikilink")