[PackageKit_on_Fedora.png](https://zh.wikipedia.org/wiki/File:PackageKit_on_Fedora.png "fig:PackageKit_on_Fedora.png")目前是Fedora默认的包管理器前端\]\]
**Yum**（**Yellow dog Updater, Modified**）是由[Duke
University團隊修改](../Page/Duke_University.md "wikilink")[Yellow
Dog Linux的](../Page/Yellow_Dog_Linux.md "wikilink")[Yellow Dog
Updater開發而成](../Page/Yellow_Dog_Updater.md "wikilink")，是一个基于[RPM包管理的字符前端软件包管理器](../Page/RPM.md "wikilink")。能够从指定的服务器自动下载RPM包并且安装，可以处理依赖性关系，并且一次安装所有依赖的软件包，无须繁琐地一次次下载、安装。被[Yellow
Dog
Linux本身](../Page/Yellow_Dog_Linux.md "wikilink")，以及[Fedora](../Page/Fedora.md "wikilink")、[Red
Hat Enterprise Linux採用](../Page/Red_Hat_Enterprise_Linux.md "wikilink")。

## 软件包来源

可供Yum下载的软件包包括[Fedora本身的软件包以及源自](../Page/Fedora.md "wikilink")[rpmfusion](http://rpmfusion.org/)等非官方软件仓库的软件包，全部是由[Linux社区维护的](../Page/Linux.md "wikilink")，并且基本是[自由软件](../Page/自由软件.md "wikilink")。所有的包都有一个独立的GPG签名，主要是为了用户的系统安全。对于Fedora
core 4及更高版本的用户，来自新软件仓库的签名是自动导入并安装的。

## 图形化前端

Yum的图形化前端主要有Yumex和kyum（KDE）。它们并不是独立于Yum的，而是Yum的图形前端，也就是说在安装和使用Yumex和kyum同时，都是以Yum为基础；所以在用Yumex或kyum时，必须先安装配置Yum。

## 參見

  - [Yellow Dog Linux](../Page/Yellow_Dog_Linux.md "wikilink")
  - [Fedora](../Page/Fedora.md "wikilink")
  - [RPM](../Page/RPM套件管理員.md "wikilink")
  - [APT](../Page/高級包裝工具.md "wikilink")
  - [DNF](../Page/DNF_\(软件\).md "wikilink")

[Category:系統軟件](../Category/系統軟件.md "wikilink")