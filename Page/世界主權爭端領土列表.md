[Kashmir_region_2004.jpg](https://zh.wikipedia.org/wiki/File:Kashmir_region_2004.jpg "fig:Kashmir_region_2004.jpg")地区，争端方为[中华人民共和国](../Page/中华人民共和国.md "wikilink")、[印度和](../Page/印度.md "wikilink")[巴基斯坦三方](../Page/巴基斯坦.md "wikilink")。\]\]

**世界主權爭端領土列表**列出世界上存在主权争议的地区。边界争端根据成因可分为位置性的边界争端（Positional Boundary
Disputes）和领土性的边界争端（Territorial Boundary
Disputes），前者因已签订条约文本的缺陷而形成纷争，后者为在达成边界条约之前围绕边境领土主权发生的争论\[1\]。

## 国家之间的领土争端

[Ile_Europa.svg](https://zh.wikipedia.org/wiki/File:Ile_Europa.svg "fig:Ile_Europa.svg")
**粗体**表示該國現時實際控制該爭議地區的全部、''斜体
''表示該國現時實際控制該爭議地區的部分、正体表示該國現時並未控制該爭議地區的任何部分。

### [非洲](../Page/非洲.md "wikilink")

  - [艾卜耶](../Page/艾卜耶地區.md "wikilink")、及地区：**[苏丹](../Page/苏丹.md "wikilink")**、[南苏丹](../Page/南苏丹.md "wikilink")

  - ：*[马达加斯加](../Page/马达加斯加.md "wikilink")*、*[葛摩](../Page/葛摩.md "wikilink")*、*[法国](../Page/法国.md "wikilink")*

  - [莫桑比克海峡上的](../Page/莫桑比克海峡.md "wikilink")[印度礁](../Page/印度礁.md "wikilink")、[欧罗巴岛](../Page/欧罗巴岛.md "wikilink")、[新胡安岛](../Page/新胡安岛.md "wikilink")：
    **[法国](../Page/法国.md "wikilink")**、[马达加斯加](../Page/马达加斯加.md "wikilink")

      - 兩者皆被被法國統稱為[印度洋諸島](../Page/法属印度洋诸岛.md "wikilink")，並劃歸[法属南部和南极领地](../Page/法属南部和南极领地.md "wikilink")

  - [休达](../Page/休达.md "wikilink")、[梅利利亚及其他](../Page/梅利利亚.md "wikilink")[西属主权地](../Page/西属主权地.md "wikilink")：
    **[西班牙](../Page/西班牙.md "wikilink")**、[摩洛哥](../Page/摩洛哥.md "wikilink")

  - [查戈斯群岛](../Page/查戈斯群岛.md "wikilink")：
    **[英国](../Page/英国.md "wikilink")**、[毛里求斯](../Page/毛里求斯.md "wikilink")（英國將其劃歸[英屬印度洋領地](../Page/英屬印度洋領地.md "wikilink")）

  - 、：**[厄立特里亚](../Page/厄立特里亚.md "wikilink")**、[吉布提](../Page/吉布提.md "wikilink")

  - [格洛里厄斯群岛](../Page/格洛里厄斯群岛.md "wikilink")：
    **[法国](../Page/法国.md "wikilink")**、[马达加斯加](../Page/马达加斯加.md "wikilink")、[科摩罗](../Page/科摩罗.md "wikilink")（法國將其劃歸[法属南部和南极领地](../Page/法属南部和南极领地.md "wikilink")）

  - [哈拉伊卜三角区](../Page/哈拉伊卜三角区.md "wikilink")、[瓦迪哈勒法尖角](../Page/瓦迪哈勒法尖角.md "wikilink")：
    **[埃及](../Page/埃及.md "wikilink")**、[苏丹](../Page/苏丹.md "wikilink")（與前者接壤的[比尔泰维勒則為無主地](../Page/比尔泰维勒.md "wikilink")，意即兩國皆不實際控制與宣稱主權）

  - ：**[肯尼亚](../Page/肯尼亚.md "wikilink")**、[南苏丹](../Page/南苏丹.md "wikilink")

  - [卡恩格瓦尼及](../Page/卡恩格瓦尼.md "wikilink")：**[南非](../Page/南非.md "wikilink")**、[斯威士兰](../Page/斯威士兰.md "wikilink")

  - Koualou村落：[布基纳法索](../Page/布基纳法索.md "wikilink")、[贝宁](../Page/贝宁.md "wikilink")

  - Kpéaba村落（接近）：**[科特迪瓦](../Page/科特迪瓦.md "wikilink")**、[贝宁](../Page/贝宁.md "wikilink")

  - Logoba和[莫約區附近的地區](../Page/莫約區.md "wikilink")：**[南苏丹](../Page/南苏丹.md "wikilink")**、**[乌干达](../Page/乌干达.md "wikilink")**

  - [盧阿普拉附近的边境地区](../Page/盧阿普拉.md "wikilink")：**[赞比亚](../Page/赞比亚.md "wikilink")**、[刚果民主共和国](../Page/刚果民主共和国.md "wikilink")

  - [马约特](../Page/马约特.md "wikilink")：**[法国](../Page/法国.md "wikilink")**、[葛摩](../Page/葛摩.md "wikilink")

  - [马拉维湖中邻近](../Page/马拉维湖.md "wikilink")的岛屿：**[坦桑尼亚](../Page/坦桑尼亚.md "wikilink")**、[马拉维](../Page/马拉维.md "wikilink")

  - ：[加蓬](../Page/加蓬.md "wikilink")、[赤道几内亚](../Page/赤道几内亚.md "wikilink")

  - 、Remba岛、等[維多利亞湖沿岸小島](../Page/維多利亞湖.md "wikilink")：**[肯尼亚](../Page/肯尼亚.md "wikilink")**、**[乌干达](../Page/乌干达.md "wikilink")**

  - [刚果河沿岸各岛](../Page/刚果河.md "wikilink")：**[刚果共和国](../Page/刚果共和国.md "wikilink")**、**[刚果民主共和国](../Page/刚果民主共和国.md "wikilink")**

  - [恩特姆河沿岸各岛](../Page/恩特姆河.md "wikilink")：**[喀麦隆](../Page/喀麦隆.md "wikilink")**、**[赤道几内亚](../Page/赤道几内亚.md "wikilink")**

  - [奥克帕腊河沿岸村庄](../Page/奥克帕腊河.md "wikilink")：**[贝宁](../Page/贝宁.md "wikilink")**、**[奈及利亞](../Page/奈及利亞.md "wikilink")**

  - [奧蘭治河邊境線](../Page/奧蘭治河.md "wikilink")：**[纳米比亚](../Page/纳米比亚.md "wikilink")**、[南非](../Page/南非.md "wikilink")

  - 及Sabanerwa：
    **[卢旺达](../Page/卢旺达.md "wikilink")**、**[布隆迪](../Page/布隆迪.md "wikilink")**

  - 及[塞姆利基河流域](../Page/塞姆利基河.md "wikilink")：
    **[刚果民主共和国](../Page/刚果民主共和国.md "wikilink")**、**[乌干达](../Page/乌干达.md "wikilink")**

  - Sindabezi岛：**[赞比亚](../Page/赞比亚.md "wikilink")**、[津巴布韦](../Page/津巴布韦.md "wikilink")

  - [索科特拉岛](../Page/索科特拉岛.md "wikilink")：**[也门](../Page/也门.md "wikilink")**、**[索马里](../Page/索马里.md "wikilink")**

  - [特罗姆兰岛](../Page/特罗姆兰岛.md "wikilink")：**[法国](../Page/法国.md "wikilink")**、[毛里求斯](../Page/毛里求斯.md "wikilink")（法國將其劃歸[法属南部和南极领地](../Page/法属南部和南极领地.md "wikilink")）

  - [南喀麥隆](../Page/南喀麥隆.md "wikilink")：**[喀麥隆](../Page/喀麥隆.md "wikilink")**、**[亚巴佐尼亚](../Page/亚巴佐尼亚.md "wikilink")**

  - [西撒哈拉](../Page/西撒哈拉.md "wikilink")：*[摩洛哥](../Page/摩洛哥.md "wikilink")*、*[撒拉威阿拉伯民主共和國](../Page/撒拉威阿拉伯民主共和國.md "wikilink")*

### 美洲

  - [巴霍努埃沃礁](../Page/巴霍努埃沃礁.md "wikilink")（又称海燕岛）：**[哥伦比亚](../Page/哥伦比亚.md "wikilink")**、[美国](../Page/美国.md "wikilink")、[牙买加](../Page/牙买加.md "wikilink")、[洪都拉斯](../Page/洪都拉斯.md "wikilink")
  - 南部[伯利兹](../Page/伯利兹.md "wikilink")：**[伯利兹](../Page/伯利兹.md "wikilink")**、[危地马拉](../Page/危地马拉.md "wikilink")
  - [阿韦斯岛](../Page/阿韦斯岛.md "wikilink")：**[多米尼加](../Page/多米尼加.md "wikilink")**、[委内瑞拉](../Page/委內瑞拉.md "wikilink")
  - [巴西岛](../Page/巴西島.md "wikilink")：**[巴西](../Page/巴西.md "wikilink")**、[阿根廷](../Page/阿根廷.md "wikilink")、[乌拉圭](../Page/乌拉圭.md "wikilink")
  - [科内霍岛](../Page/科内霍岛_\(丰塞卡湾\).md "wikilink")：**[洪都拉斯](../Page/洪都拉斯.md "wikilink")**、[萨尔瓦多](../Page/萨尔瓦多.md "wikilink")
  - [卡萊羅島北端](../Page/卡萊羅島.md "wikilink")：**[哥斯达黎加](../Page/哥斯达黎加.md "wikilink")**、[尼加拉瓜](../Page/尼加拉瓜.md "wikilink")
  - [马尔维纳斯群岛](../Page/福克兰群岛.md "wikilink")（福克兰群岛）：**[英国](../Page/英国.md "wikilink")**和[阿根廷](../Page/阿根廷.md "wikilink")
  - [法属圭亚那](../Page/法属圭亚那.md "wikilink")[马罗尼河河西岸](../Page/马罗尼河.md "wikilink")：**[法国](../Page/法国.md "wikilink")**、[蘇利南](../Page/蘇利南.md "wikilink")
  - [科兰太因河东岸](../Page/科兰太因河.md "wikilink")：**[圭亚那](../Page/圭亚那.md "wikilink")**、[蘇利南](../Page/蘇利南.md "wikilink")
  - [埃塞奎博河西岸](../Page/埃塞奎博河.md "wikilink")（[西属圭亚那](../Page/西属圭亚那.md "wikilink")）：**[圭亚那](../Page/圭亚那.md "wikilink")**、[委内瑞拉](../Page/委内瑞拉.md "wikilink")
  - [汉斯岛](../Page/漢斯島.md "wikilink")：**[丹麦](../Page/丹麦.md "wikilink")**、[加拿大](../Page/加拿大.md "wikilink")
  - [納弗沙島](../Page/納弗沙島.md "wikilink")：**[美国](../Page/美国.md "wikilink")**、[海地](../Page/海地.md "wikilink")
  - [圣胡安河省](../Page/圣胡安河省.md "wikilink")：[哥斯达黎加](../Page/哥斯达黎加.md "wikilink")、**[尼加拉瓜](../Page/尼加拉瓜.md "wikilink")**
  - [萨波蒂约礁](../Page/萨波蒂约礁.md "wikilink")：**[伯利兹](../Page/伯利兹.md "wikilink")**、[危地马拉](../Page/危地马拉.md "wikilink")（历史上宣称对整个伯利兹拥有主权）、[洪都拉斯](../Page/洪都拉斯.md "wikilink")
  - [聖安德列斯-普羅維登西亞和聖卡塔利娜群島省](../Page/聖安德列斯-普羅維登西亞和聖卡塔利娜群島省.md "wikilink")：**[哥伦比亚](../Page/哥伦比亚.md "wikilink")**、[尼加拉瓜](../Page/尼加拉瓜.md "wikilink")、[洪都拉斯](../Page/洪都拉斯.md "wikilink")
  - [塞拉納島](../Page/塞拉納島.md "wikilink")（又称小塞拉纳岛）：**[哥伦比亚](../Page/哥伦比亚.md "wikilink")**、[美国](../Page/美国.md "wikilink")、[尼加拉瓜](../Page/尼加拉瓜.md "wikilink")、[洪都拉斯](../Page/洪都拉斯.md "wikilink")
  - [蘇亞雷斯島](../Page/蘇亞雷斯島.md "wikilink")：[玻利維亞](../Page/玻利維亞.md "wikilink")、[巴西](../Page/巴西.md "wikilink")
  - [南乔治亚和南桑威奇群岛](../Page/南乔治亚和南桑威奇群岛.md "wikilink")：**[英国](../Page/英国.md "wikilink")**、[阿根廷](../Page/阿根廷.md "wikilink")
  - [菲茨罗伊峰和](../Page/菲茨罗伊峰.md "wikilink")[穆拉永峰之间的](../Page/穆拉永峰.md "wikilink")[南巴塔哥尼亚冰原](../Page/南巴塔哥尼亚冰原.md "wikilink")：[阿根廷](../Page/阿根廷.md "wikilink")、[智利](../Page/智利.md "wikilink")
  - [加拿大-美国的领土争端](../Page/加拿大-美国的领土争端.md "wikilink")
      - [波弗特海](../Page/波弗特海.md "wikilink")（美国[阿拉斯加州](../Page/阿拉斯加州.md "wikilink")／加拿大[育空地區](../Page/育空地區.md "wikilink")）
      - [迪克森海峡](../Page/迪克森海峡.md "wikilink")（美国[阿拉斯加州](../Page/阿拉斯加州.md "wikilink")／加拿大[不列颠哥伦比亚省](../Page/不列颠哥伦比亚省.md "wikilink")）
      - [玛基亚斯海豹岛](../Page/玛基亚斯海豹岛.md "wikilink")（美国[缅因州](../Page/缅因州.md "wikilink")／**加拿大[新不倫瑞克省](../Page/新不倫瑞克省.md "wikilink")**）
      - [北岩岛](../Page/北岩岛.md "wikilink")（美国[缅因州](../Page/缅因州.md "wikilink")／加拿大[新不伦瑞克省](../Page/新不伦瑞克.md "wikilink")）
      - [胡安·德·富卡海峡](../Page/胡安·德·富卡海峡.md "wikilink")（美国[华盛顿州](../Page/华盛顿州.md "wikilink")／加拿大[不列颠哥伦比亚省](../Page/不列颠哥伦比亚省.md "wikilink")）
      - [西北水道](../Page/西北水道.md "wikilink")：美国宣称为国际水域

### 亚洲和大洋洲

[South_China_Sea_claims_map.jpg](https://zh.wikipedia.org/wiki/File:South_China_Sea_claims_map.jpg "fig:South_China_Sea_claims_map.jpg")的海權聲索\]\]
[Senkaku_Diaoyu_Tiaoyu_Islands.png](https://zh.wikipedia.org/wiki/File:Senkaku_Diaoyu_Tiaoyu_Islands.png "fig:Senkaku_Diaoyu_Tiaoyu_Islands.png")

  - [霍爾木茲海峽间的](../Page/霍爾木茲海峽.md "wikilink")[阿布穆萨岛](../Page/阿布穆萨岛.md "wikilink")、[大通布岛](../Page/大小通布岛.md "wikilink")、[小通布岛](../Page/大小通布岛.md "wikilink")：**[伊朗](../Page/伊朗.md "wikilink")**、[阿拉伯联合酋长国](../Page/阿拉伯联合酋长国.md "wikilink")
  - [阿克赛钦](../Page/阿克赛钦.md "wikilink")：**[中华人民共和国](../Page/中华人民共和国.md "wikilink")**、[中華民國](../Page/中華民國.md "wikilink")、[印度](../Page/印度.md "wikilink")
  - [巴里加斯](../Page/巴里加斯.md "wikilink")：[中华人民共和国](../Page/中华人民共和国.md "wikilink")、[中華民國](../Page/中華民國.md "wikilink")、[印度](../Page/印度.md "wikilink")
  - [藏南地区](../Page/藏南地區.md "wikilink")（[阿鲁纳恰尔邦大部分地区](../Page/阿鲁纳恰尔邦.md "wikilink")）：**印度**、中華民國、中华人民共和国
  - [联邦直辖部落地区](../Page/联邦直辖部落地区.md "wikilink")：**[巴基斯坦](../Page/巴基斯坦.md "wikilink")**、[阿富汗](../Page/阿富汗.md "wikilink")
  - [Bagys](../Page/Bagys.md "wikilink")、[突厥斯坦州](../Page/突厥斯坦_\(哈薩克斯坦\).md "wikilink")：**[乌兹别克斯坦](../Page/乌兹别克斯坦.md "wikilink")**、[哈萨克斯坦](../Page/哈萨克斯坦.md "wikilink")
  - [不丹北部](../Page/中不边界争议.md "wikilink")：'''[中华人民共和国](../Page/中华人民共和国.md "wikilink")
    '''、[中華民國](../Page/中華民國.md "wikilink")、[不丹](../Page/不丹.md "wikilink")
  - [吉大港山走廊](../Page/吉大港山走廊.md "wikilink")：**[孟加拉](../Page/孟加拉.md "wikilink")**、[印度](../Page/印度.md "wikilink")
  - [东耶路撒冷](../Page/东耶路撒冷.md "wikilink")：**[以色列](../Page/以色列.md "wikilink")**、[联合国](../Page/联合国.md "wikilink")、[巴勒斯坦民族权力机构](../Page/巴勒斯坦民族权力机构.md "wikilink")
  - [戈蘭高地](../Page/戈蘭高地.md "wikilink")：**[以色列](../Page/以色列.md "wikilink")**、[叙利亚](../Page/叙利亚.md "wikilink")

:\*[舍巴農場](../Page/舍巴農場.md "wikilink")：**[以色列](../Page/以色列.md "wikilink")**、[黎巴嫩](../Page/黎巴嫩.md "wikilink")、[叙利亚](../Page/叙利亚.md "wikilink")

  - [阿拉伯河](../Page/阿拉伯河.md "wikilink")：[伊朗](../Page/伊朗.md "wikilink")、[伊拉克](../Page/伊拉克.md "wikilink")

  - [沙特阿拉伯与阿拉伯联合酋长国之间的边界争端](../Page/沙特阿拉伯与阿拉伯联合酋长国之间的边界争端.md "wikilink")：[沙特阿拉伯](../Page/沙特阿拉伯.md "wikilink")、[阿拉伯联合酋长国](../Page/阿拉伯联合酋长国.md "wikilink")

  - [克什米尔](../Page/克什米尔.md "wikilink")：
    *[印度](../Page/印度.md "wikilink")*（[查谟-克什米尔邦](../Page/查谟-克什米尔邦.md "wikilink")）、*[巴基斯坦](../Page/巴基斯坦.md "wikilink")*

  - [卡拉帕尼](../Page/卡拉帕尼.md "wikilink")： *[印度](../Page/印度.md "wikilink")*
    、 *[尼泊尔](../Page/尼泊尔.md "wikilink")*

  -
  - [朝鲜半岛北部](../Page/朝鲜半岛.md "wikilink")：
    **[朝鲜](../Page/朝鲜民主主义人民共和国.md "wikilink")**、[韩国](../Page/大韩民国.md "wikilink")

  - [朝鲜半岛南部](../Page/朝鲜半岛.md "wikilink")：
    [朝鲜](../Page/朝鲜民主主义人民共和国.md "wikilink")、**[韩国](../Page/大韩民国.md "wikilink")**

      - [西海（黄海）五岛](../Page/西海五岛.md "wikilink")：**[大韩民国](../Page/大韩民国.md "wikilink")**、[朝鲜民主主义人民共和国](../Page/朝鲜民主主义人民共和国.md "wikilink")

  - [南千岛群岛](../Page/南千岛群岛.md "wikilink")（[北方四島](../Page/北方四島.md "wikilink")）：**[俄罗斯](../Page/俄罗斯.md "wikilink")**、[日本](../Page/日本.md "wikilink")

  - [林梦地区](../Page/林梦.md "wikilink")：**[马来西亚](../Page/马来西亚.md "wikilink")**、[汶莱](../Page/汶莱.md "wikilink")

  - [独岛](../Page/独岛.md "wikilink")（竹岛）：**[大韩民国](../Page/大韩民国.md "wikilink")**、[日本](../Page/日本.md "wikilink")、[朝鲜民主主义人民共和国](../Page/朝鲜民主主义人民共和国.md "wikilink")

  - [中国大陆](../Page/中国大陆.md "wikilink")：[中華民國](../Page/中華民國.md "wikilink")、**[中华人民共和国](../Page/中华人民共和国.md "wikilink")**

  - [台湾](../Page/台湾.md "wikilink")、[澎湖](../Page/澎湖.md "wikilink")：**[中華民國](../Page/中華民國.md "wikilink")**、[中华人民共和国](../Page/中华人民共和国.md "wikilink")

      - [金馬](../Page/金馬地區.md "wikilink")、[东沙](../Page/东沙群岛.md "wikilink")：**[中華民國](../Page/中華民國.md "wikilink")**、[中华人民共和国](../Page/中华人民共和国.md "wikilink")

  - [釣魚臺列嶼](../Page/釣魚臺列嶼.md "wikilink")：[日本](../Page/日本.md "wikilink")、[中华人民共和国](../Page/中华人民共和国.md "wikilink")、[中華民國](../Page/中華民國.md "wikilink")

  - [中沙群岛](../Page/中沙群島.md "wikilink")：**[中华人民共和国](../Page/中华人民共和国.md "wikilink")**、[中華民國](../Page/中華民國.md "wikilink")、[菲律宾](../Page/菲律宾.md "wikilink")

  - [黄岩岛](../Page/黄岩岛.md "wikilink")：**[中华人民共和国](../Page/中华人民共和国.md "wikilink")**、[中華民國](../Page/中華民國.md "wikilink")、[菲律宾](../Page/菲律宾.md "wikilink")

<!-- end list -->

  - [亨特岛](../Page/亨特岛_\(新喀里多尼亚\).md "wikilink")、[马修岛](../Page/马修岛.md "wikilink")：[瓦努阿图](../Page/瓦努阿图.md "wikilink")、[新喀里多尼亞](../Page/新喀里多尼亞.md "wikilink")（[法国](../Page/法国.md "wikilink")）

  - [密涅瓦群礁](../Page/密涅瓦群礁.md "wikilink")：**[汤加](../Page/汤加.md "wikilink")**、[斐濟](../Page/斐濟.md "wikilink")

  - [斯温斯岛](../Page/斯温斯岛.md "wikilink")：**[美国](../Page/美国.md "wikilink")**、[托克劳](../Page/托克劳.md "wikilink")

  - [南达尔帕蒂岛](../Page/南达尔帕蒂岛.md "wikilink")：[孟加拉国](../Page/孟加拉国.md "wikilink")、[印度](../Page/印度.md "wikilink")

  - [沃兹罗日杰尼耶岛](../Page/沃兹罗日杰尼耶岛.md "wikilink")（以前为[鹹海内一个小岛](../Page/鹹海.md "wikilink")，现已成为半岛）：[哈萨克斯坦](../Page/哈萨克斯坦.md "wikilink")、[乌兹别克斯坦](../Page/乌兹别克斯坦.md "wikilink")''

  - [西沙群岛](../Page/西沙群岛.md "wikilink")：**[中华人民共和国](../Page/中华人民共和国.md "wikilink")**、[中華民國](../Page/中華民國.md "wikilink")、

  - ：[印度](../Page/印度.md "wikilink")、[巴基斯坦](../Page/巴基斯坦.md "wikilink")

  - [南沙群島](../Page/南沙群島.md "wikilink")：*[中华人民共和国](../Page/中华人民共和国.md "wikilink")*、*[中華民國](../Page/中華民國.md "wikilink")*、**、*[菲律宾](../Page/菲律宾.md "wikilink")*、*[马来西亚](../Page/马来西亚.md "wikilink")*、*[汶萊](../Page/汶萊.md "wikilink")*、[印度尼西亚](../Page/印度尼西亚.md "wikilink")（中华人民共和国、中華民國和越南声称拥有全部主权）

  - [仁爱礁](../Page/仁爱礁.md "wikilink")：**[中华人民共和国](../Page/中华人民共和国.md "wikilink")**、[菲律宾](../Page/菲律宾.md "wikilink")、[中華民國](../Page/中華民國.md "wikilink")

### 欧洲

  - [布雷加纳](../Page/布雷加纳.md "wikilink")：[克罗地亚和](../Page/克罗地亚.md "wikilink")[斯洛文尼亚](../Page/斯洛文尼亚.md "wikilink")
  - [埃姆斯河河口和](../Page/埃姆斯河.md "wikilink")[多拉德湾西部](../Page/多拉德湾.md "wikilink")：[荷兰](../Page/荷兰.md "wikilink")、[德国](../Page/德国.md "wikilink")
  - [皮蘭灣](../Page/皮蘭灣.md "wikilink")：[斯洛文尼亚和](../Page/斯洛文尼亚.md "wikilink")[克罗地亚](../Page/克罗地亚.md "wikilink")
  - [费迪南德岛](../Page/费迪南德岛.md "wikilink")：**[意大利](../Page/意大利.md "wikilink")**、[英国](../Page/英国.md "wikilink")、[法国](../Page/法国.md "wikilink")、[西班牙](../Page/西班牙.md "wikilink")、[马耳他](../Page/马耳他.md "wikilink")、[阿尔及利亚](../Page/阿尔及利亚.md "wikilink")、[突尼斯](../Page/突尼斯.md "wikilink")、[利比亚和](../Page/利比亚.md "wikilink")[摩洛哥](../Page/摩洛哥.md "wikilink")
  - [直布罗陀](../Page/直布罗陀.md "wikilink")：**[英国](../Page/英国.md "wikilink")**和[西班牙](../Page/西班牙.md "wikilink")
  - [伊米亞群島](../Page/伊米亞群島.md "wikilink")：**[希腊](../Page/希腊.md "wikilink")**和[土耳其](../Page/土耳其.md "wikilink")
  - [伊万哥罗德](../Page/伊万哥罗德.md "wikilink")：**[俄罗斯](../Page/俄罗斯.md "wikilink")**和[爱沙尼亚](../Page/爱沙尼亚.md "wikilink")
  - [奥利文萨](../Page/奥利文萨.md "wikilink")：**[西班牙](../Page/西班牙.md "wikilink")**和[葡萄牙](../Page/葡萄牙.md "wikilink")
  - [羅科爾島](../Page/羅科爾島.md "wikilink")：**[英国](../Page/英国.md "wikilink")**、[冰岛](../Page/冰岛.md "wikilink")、[爱尔兰和](../Page/爱尔兰.md "wikilink")[丹麦](../Page/丹麦.md "wikilink")
  - [圖茲拉島和](../Page/圖茲拉島.md "wikilink")[刻赤海峽](../Page/刻赤海峽.md "wikilink")：[乌克兰](../Page/乌克兰.md "wikilink")、[俄罗斯](../Page/俄罗斯.md "wikilink")
  - [武科瓦尔](../Page/武科瓦尔.md "wikilink")：**[塞尔维亚](../Page/塞尔维亚.md "wikilink")**、[克罗地亚](../Page/克罗地亚.md "wikilink")

## 主权國家内部的爭端

  - ：[布拉克山](../Page/布拉克山.md "wikilink")（Black
    Hills）：[美國聯邦政府和](../Page/美國聯邦政府.md "wikilink")[印第安人](../Page/美洲原住民.md "wikilink")

  - ：[塞阿腊州](../Page/塞阿腊州.md "wikilink")（Ceará）和[皮奧伊州](../Page/皮奧伊州.md "wikilink")（Piauí）

  - [拉布拉多南方角落](../Page/拉布拉多.md "wikilink")：  、
    和[拉布拉多三者之间](../Page/拉布拉多.md "wikilink")。

  - ：  （Catalunya）、

  - ：  （Scotland）、

  - ：红河大桥：  和 [俄克拉荷马州](../Page/俄克拉荷马州.md "wikilink")

  - ：[托莱多西部](../Page/托莱多_\(俄亥俄州\).md "wikilink")：  和

  - ：[前三岛](../Page/前三岛.md "wikilink")、[微山湖](../Page/微山湖.md "wikilink")：[江苏省和](../Page/江苏省.md "wikilink")[山东省](../Page/山东省.md "wikilink")

  - ：[七星岛](../Page/七星岛.md "wikilink")：[浙江省和](../Page/浙江省.md "wikilink")[福建省](../Page/福建省.md "wikilink")

  - ：[加格达奇区](../Page/加格达奇区.md "wikilink")、[松岭区](../Page/松岭区.md "wikilink")：[黑龙江省和](../Page/黑龙江省.md "wikilink")[内蒙古自治区](../Page/内蒙古自治区.md "wikilink")

  - ：[雁石坪镇](../Page/雁石坪镇.md "wikilink")：[西藏自治区和](../Page/西藏自治区.md "wikilink")[青海省](../Page/青海省.md "wikilink")

  - ：[开山岛](../Page/开山岛.md "wikilink")：[江苏省](../Page/江苏省.md "wikilink")[连云港市](../Page/连云港市.md "wikilink")[灌云县和](../Page/灌云县.md "wikilink")[盐城市](../Page/盐城市.md "wikilink")[响水县](../Page/响水县.md "wikilink")

## 地位存在争议的地区

[Zhonghua_Minguo_Quhua_Fanti.svg](https://zh.wikipedia.org/wiki/File:Zhonghua_Minguo_Quhua_Fanti.svg "fig:Zhonghua_Minguo_Quhua_Fanti.svg")
**粗体**表示实际控制方。

  - [中國大陸](../Page/中国大陆.md "wikilink")、[西沙群岛](../Page/西沙群岛.md "wikilink")、[中沙群島](../Page/中沙群島.md "wikilink")：**[中华人民共和国](../Page/中华人民共和国.md "wikilink")**和
    [中華民國](../Page/中華民國.md "wikilink")
  - [南沙群岛](../Page/南沙群岛.md "wikilink")：[中华人民共和国](../Page/中华人民共和国.md "wikilink")、[中華民國和](../Page/中華民國.md "wikilink")[越南](../Page/越南.md "wikilink")、[菲律宾](../Page/菲律宾.md "wikilink")、[马来西亚](../Page/马来西亚.md "wikilink")、[汶萊](../Page/汶萊.md "wikilink")、[印度尼西亚](../Page/印度尼西亚.md "wikilink")（中华人民共和国和中華民國声称拥有全部主权）
  - [朝鲜半岛](../Page/朝鲜半岛.md "wikilink")：[朝鲜](../Page/朝鲜民主主义人民共和国.md "wikilink")
    和[韓國](../Page/大韩民国.md "wikilink")
  - [白頭山](../Page/长白山.md "wikilink")（長白山）南部：**[朝鮮](../Page/朝鲜民主主义人民共和国.md "wikilink")**、[中华人民共和国](../Page/中华人民共和国.md "wikilink")
  - [阿布哈兹](../Page/阿布哈兹.md "wikilink")：**[阿布哈茲](../Page/阿布哈茲.md "wikilink")**和[格鲁吉亚](../Page/格鲁吉亚.md "wikilink")
  - [北塞浦路斯土耳其](../Page/北賽普勒斯.md "wikilink")：**[北賽普勒斯](../Page/北賽普勒斯.md "wikilink")**和[賽普勒斯](../Page/賽普勒斯.md "wikilink")
  - [納戈爾諾-卡拉巴赫地區](../Page/納戈爾諾-卡拉巴赫.md "wikilink")：**[纳戈尔诺-卡拉巴赫](../Page/納戈爾諾-卡拉巴赫共和國.md "wikilink")**和[阿塞拜疆](../Page/阿塞拜疆.md "wikilink")
  - [西亞美尼亞](../Page/西亞美尼亞.md "wikilink")、[卡尔斯省](../Page/卡尔斯省.md "wikilink")、[凡城省](../Page/凡城省.md "wikilink")：**[土耳其](../Page/土耳其.md "wikilink")**和[亞美尼亞](../Page/亞美尼亞.md "wikilink")
  - [邦特兰](../Page/邦特兰.md "wikilink")：**[邦特兰](../Page/邦特兰.md "wikilink")**和[索马里](../Page/索马里.md "wikilink")
  - [索马里兰](../Page/索马里兰.md "wikilink")：**[索马里兰](../Page/索马里兰.md "wikilink")**和[索马里](../Page/索马里.md "wikilink")
  - [西南索马里](../Page/索馬里西南國.md "wikilink")：**[索馬里西南國](../Page/索馬里西南國.md "wikilink")**和[索马里](../Page/索马里.md "wikilink")
  - [南奥塞梯](../Page/南奥塞梯.md "wikilink")：
    **[南奥塞梯](../Page/南奥塞梯.md "wikilink")**和[格鲁吉亚](../Page/格鲁吉亚.md "wikilink")
  - [斯里兰卡东部和北部](../Page/斯里兰卡.md "wikilink")：**[斯里兰卡](../Page/斯里兰卡.md "wikilink")**和[泰米爾-伊拉姆猛虎解放組織](../Page/泰米爾-伊拉姆猛虎解放組織.md "wikilink")
  - [德涅斯特河沿岸](../Page/德涅斯特河沿岸.md "wikilink")：**[德涅斯特河沿岸共和国](../Page/德涅斯特河沿岸.md "wikilink")**和[摩尔多瓦](../Page/摩尔多瓦.md "wikilink")
  - [西撒哈拉](../Page/西撒哈拉.md "wikilink")：**[摩洛哥](../Page/摩洛哥.md "wikilink")**和**[撒拉威阿拉伯民主共和國](../Page/撒拉威阿拉伯民主共和國.md "wikilink")**
  - [約旦河西岸地區和](../Page/約旦河西岸地區.md "wikilink")[加沙地带](../Page/加沙地带.md "wikilink")：**[以色列](../Page/以色列.md "wikilink")**和**[巴勒斯坦國](../Page/巴勒斯坦國.md "wikilink")**
  - [科索沃和梅托希亞](../Page/科索沃和梅托希亚自治省.md "wikilink")：**[科索沃](../Page/科索沃.md "wikilink")**和[塞尔维亚](../Page/塞尔维亚.md "wikilink")

## 南極洲

《[南極條約體系](../Page/南极条约体系.md "wikilink")》於1961年6月23日生效，提供了框架管理[南極洲](../Page/南极洲.md "wikilink")，通過協議和會議。要求凍結所有對南極地區有領土要求的國家。

## 参考文献

## 参见

  - [残存国家列表](../Page/残存国家列表.md "wikilink")
  - [有限承认国家列表](../Page/有限承认国家列表.md "wikilink")

[Category:国际关系](../Category/国际关系.md "wikilink")
[\*](../Category/爭議地區.md "wikilink")
[Category:地理相關列表](../Category/地理相關列表.md "wikilink")

1.