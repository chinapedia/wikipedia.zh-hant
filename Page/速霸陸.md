**速霸陸公司**（，，）舊名為「富士重工業」，是[日本](../Page/日本.md "wikilink")[重工業製造商之一](../Page/重工業.md "wikilink")，產品範圍涵蓋航空太空、交通運輸、汽車、工業設備等。2017年4月起更名為與其汽車[品牌同名的](../Page/品牌.md "wikilink")「速霸陸」，以強調該公司對汽車產業的重視。

## 發展沿革

[Nissann_Diesel_and_FHI_Makers_plates_001.JPG](https://zh.wikipedia.org/wiki/File:Nissann_Diesel_and_FHI_Makers_plates_001.JPG "fig:Nissann_Diesel_and_FHI_Makers_plates_001.JPG")
[Builder's_plate_of_FHI_001.JPG](https://zh.wikipedia.org/wiki/File:Builder's_plate_of_FHI_001.JPG "fig:Builder's_plate_of_FHI_001.JPG")9000系電車的車內銘板（下）\]\]
1917年（大正6年）12月21日[中島知久平自](../Page/中島知久平.md "wikilink")[大日本帝國海軍退役後](../Page/大日本帝國海軍.md "wikilink")，回到故鄉[群馬縣新田郡尾島町](../Page/群馬縣.md "wikilink")（今群馬縣[太田市](../Page/太田市.md "wikilink")）和親兄弟中島喜代一、中島乙未平等人設立「飛機研究所」（飛行機研究所），是為日本第二家民營飛機製造商。1919年易名為**[中島飛機製作所](../Page/中島飛機.md "wikilink")**，後來在[第二次世界大戰期間研發製造出許多](../Page/第二次世界大戰.md "wikilink")[戰鬥機](../Page/戰鬥機.md "wikilink")、偵察機、[轟炸機等](../Page/轟炸機.md "wikilink")。[日本投降後](../Page/日本投降.md "wikilink")，1945年8月先易名成**富士產業株式會社**，12月[駐日盟軍總司令部](../Page/駐日盟軍總司令部.md "wikilink")（GHQ）下令解散，共拆分成12家公司。這些由各地工廠轉型的12家公司被GHQ禁止從事[軍需相關產業](../Page/軍需.md "wikilink")，為了餬口度日，各公司轉進汽車修理、自行車、[曳引機等行業](../Page/拖拉机.md "wikilink")，甚至生產鍋碗瓢盆、嬰兒車、收納箱等用具。1946年太田吞龍工廠和武藏野三鷹工廠參考美國鮑威爾公司（Powell
Motor
Company）開發出「[兔子速克達](../Page/富士兔子速克達.md "wikilink")」（ラビットスクーター），1949年開發日本第一輛單體結構的[巴士](../Page/巴士.md "wikilink")。兔子速克達受到市場的歡迎，甚至曾外銷美國，直到1968年才停產。

1950年6月25日爆發[韓戰](../Page/朝鲜战争.md "wikilink")，為此GHQ對日本的[佔領政策不變](../Page/同盟國軍事佔領日本.md "wikilink")。直至1952年4月28日《[舊金山和約](../Page/舊金山和約.md "wikilink")》生效後，富士工業（太田吞龍工廠和武藏野三鷹工廠）、富士汽車工業（伊勢崎工廠）等二家公司積極運作其他公司，1952年（[昭和](../Page/昭和.md "wikilink")27年）12月加上東京富士產業（本社）、大宮富士工業（大宮工廠）等共四家公司初步同意合併。在此同時，1953年保安廳（今[防衛省](../Page/防衛省.md "wikilink")）編列[預算擬採購教練機](../Page/財政預算案.md "wikilink")，獲得美國[比奇飛機公司授權在日本生產](../Page/比奇飛機公司.md "wikilink")[T-34教練機](../Page/T-34教練機.md "wikilink")，國內各家飛機製造商莫不摩拳擦掌，也間接加速促成原中島飛機各工廠的合併。1953年5月轉型成製造[鐵路車輛的宇都宮車輛](../Page/鐵路車輛.md "wikilink")（宇都宮工廠）決定加入合併，於是同年7月15日五家合併成立以製造[飛機為事業目標的](../Page/飛機.md "wikilink")**富士重工業株式會社**。當時富士重工業資本額為8億3千50萬[日圓](../Page/日圓.md "wikilink")，員工人數共5,643名。

原中島飛機的[引擎生產重地東京荻窪工廠與濱松工廠改組成](../Page/引擎.md "wikilink")「富士精密工業」，1950年與多摩電力汽車（たま電気自動車）簽訂契約供應[汽油引擎](../Page/汽油引擎.md "wikilink")。不過富士精密工業[股東之一](../Page/股東.md "wikilink")（[瑞穗金融集團前身](../Page/瑞穗金融集團.md "wikilink")）並不贊成富士精密工業和多摩汽車的合作案；多摩汽車大股東[石橋正二郎](../Page/石橋正二郎.md "wikilink")（[普利司通輪胎創辦人](../Page/普利司通.md "wikilink")）遂買下其持股，並當上富士精密工業董事會會長。日後兩家公司併成[王子汽車](../Page/王子汽車.md "wikilink")，1966年8月1日該公司又與[日產汽車合併](../Page/日產汽車.md "wikilink")。1966年（昭和41年），為了將每張股票面額自500日圓改成50日圓，富士重工業吸收合併[東邦化學](../Page/東邦化學.md "wikilink")\[1\]。

在汽車製造業務方面，1954年開發試作了代號「[P-1](../Page/速霸陸1500.md "wikilink")」的[次緊湊型](../Page/次緊湊型車.md "wikilink")[轎車](../Page/轎車.md "wikilink")，並經由首任社長北謙治命名為「速霸陸（スバル）」，可惜因故未能正式上市。富士重工業毫不氣餒，1958年在工程師[百瀨晉六的領導下開發出](../Page/百瀨晉六.md "wikilink")[速霸陸360](../Page/速霸陸360.md "wikilink")，由於彼時國民所得逐漸增加，這部被車迷暱稱為「[瓢蟲](../Page/瓢蟲.md "wikilink")（てんとう虫）\[2\]」的[輕型車推出後一炮而紅](../Page/輕型車.md "wikilink")，總計約生產了39萬2千部，奠定了該公司在汽車業立足的基礎。由於[日本首相](../Page/日本首相.md "wikilink")[佐藤榮作下令整合國內汽車產業](../Page/佐藤榮作.md "wikilink")，1968年[日產汽車取得富士重工業](../Page/日產汽車.md "wikilink")20%[股權](../Page/股票.md "wikilink")。

1970年代初期發表[速霸陸Leone](../Page/速霸陸Leone.md "wikilink")，並正式外銷至[北美洲市場](../Page/北美洲.md "wikilink")。縱使遭遇[石油危機](../Page/第一次石油危机.md "wikilink")、廢氣排放標準等事件，但是當時日圓走跌反而使車款價格低廉，在該市場比其他日系對手賣得更好。70年代中期開始，逐漸將銷售網擴及到[南美洲](../Page/南美洲.md "wikilink")、[大洋洲](../Page/大洋洲.md "wikilink")、[中東地區](../Page/中東.md "wikilink")、[歐洲等市場](../Page/歐洲.md "wikilink")，使得年度產能從10萬輛躍升至20萬輛。但是1985年9月22日[廣場協議生效後日圓大幅升值](../Page/廣場協議.md "wikilink")，該公司在北美市場的銷售反顯疲弱。1987年該公司和[五十鈴汽車在](../Page/五十鈴汽車.md "wikilink")[印第安那州](../Page/印第安那州.md "wikilink")[拉斐特合資建造](../Page/拉斐特_\(印第安纳州\).md "wikilink")「速霸陸五十鈴汽車廠（今改稱〔Subaru
of Indiana Automotive,
Inc.〕）」\[3\]，雙方以[換牌生產的方式互相補足產品陣容](../Page/換牌工程.md "wikilink")，譬如[第三代Leone五門](../Page/速霸陸Leone#第三代（1984年-1994年）.md "wikilink")[旅行車](../Page/旅行車.md "wikilink")[換牌成五十鈴Geminett](../Page/換牌工程.md "wikilink")
II、[第一代Legacy四門轎車換牌成五十鈴Aska](../Page/速霸陸Legacy#第一代BC/BF系（1989年-1993年）.md "wikilink")
CX、五十鈴Trooper（日規名稱五十鈴Bighorn）則換牌成[速霸陸Bighorn等](../Page/速霸陸Bighorn.md "wikilink")。由於缺乏具有產品力的主力車款，加上以該公司的規模和其他競爭對手相較，無法有效降低製造成本，造成1989年的營業赤字竟高達3百億日圓\[4\]。幸虧該公司力圖振作，導入開發主管制度以進行大規模的組織改革活動，推出的[第一代Legacy不論在日本或北美洲市場皆取得銷售佳績](../Page/速霸陸Legacy#第一代BC/BF系（1989年-1993年）.md "wikilink")；加上順逢「[泡沫經濟](../Page/日本泡沫经济.md "wikilink")」時期順利取得資金調度週轉，得以繼續經營。

1990年上任的第七任社長[川合勇積極地削減製造成本](../Page/川合勇.md "wikilink")，並以新推出的[Impreza進軍](../Page/速霸陸Impreza.md "wikilink")[世界拉力錦標賽](../Page/世界拉力錦標賽.md "wikilink")，提升品牌形象。可惜1990年代初期經濟泡沫破裂，[日產汽車陷入經營困頓](../Page/日產汽車.md "wikilink")，於是在2000年4月決定將富士重工業的約20%持股全數出清給美國[通用汽車](../Page/通用汽車.md "wikilink")。在後者的主導下，富士重工業曾換牌生產[紳寶9-2X](../Page/紳寶9-2X.md "wikilink")，同屬通用集團的[歐寶汽車也換牌生產](../Page/歐寶.md "wikilink")[速霸陸Traviq等車款](../Page/速霸陸Traviq.md "wikilink")。隨著通用汽車的財務出現問題，原本手中握有富士重工業20%的股權，2005年（平成17年）10月5日決定悉數拋售，[豐田汽車取得](../Page/豐田汽車.md "wikilink")8.7%股權，正式入主富士重工業\[5\]。因此，速霸陸[輕型車的零件部品供應商從原來屬於通用汽車集團的](../Page/輕型車.md "wikilink")[鈴木汽車](../Page/鈴木公司.md "wikilink")，變成同屬豐田汽車集團的[大發工業](../Page/大發工業.md "wikilink")。

豐田汽車一躍成為富士重工業的大股東後，實施一連串的改造計劃。首先2008年（平成20年）4月10日富士重工業宣布不再自行開發製造輕型車，2012年（平成24年）2月以降改以大發工業的輕型車款換牌上市，諸如[速霸陸Pleo](../Page/速霸陸Pleo.md "wikilink")、[速霸陸Pleo
Plus等車款](../Page/速霸陸Pleo_Plus.md "wikilink")。此外本著集團資源共享的前提，豐田汽車和富士重工業分別推出四人座雙門[跑車](../Page/跑車.md "wikilink")[豐田86](../Page/豐田86.md "wikilink")、賽揚FR-S、[速霸陸BRZ](../Page/速霸陸BRZ.md "wikilink")，而這些兄弟車皆在富士重工業的群馬工廠製造。

富士重工業原先位於[東京都](../Page/東京都.md "wikilink")[新宿區的總部大樓](../Page/新宿區.md "wikilink")（新宿スバルビル）日漸老舊，2010年（平成22年）8月30日遂決定出售給[小田急電鐵](../Page/小田急電鐵.md "wikilink")，另在[澀谷區](../Page/澀谷區.md "wikilink")[惠比壽新建大樓](../Page/惠比壽_\(東京都\).md "wikilink")；2014年完成遷入。2013年1月31日，富士重工業發生未將國家提供的[機器人開發補助金用於正當用途之情事](../Page/機器人.md "wikilink")，[經濟產業省決定停止補助](../Page/經濟產業省.md "wikilink")，並要求富士重工業必須退還8億250萬日圓補助金給經濟產業省、[新能源產業技術綜合開發機構等機關](../Page/新能源產業技術綜合開發機構.md "wikilink")\[6\]。

2016年8月9日，富士重工業聲明，全數拋售所持有的[鈴木公司之股份](../Page/鈴木公司.md "wikilink")578萬股\[7\]（占比為1.18%）；至於鈴木則在8月8日拋售其持有的富士重工業的1,369萬股（占比1.75%）。由於1990年代[美國](../Page/美國.md "wikilink")[通用汽車集團分別擁有富士重工業](../Page/通用汽车公司.md "wikilink")、鈴木及[五十鈴汽車的股權](../Page/五十鈴.md "wikilink")，基於集團資源共享的原則，早在1999年12月雙方便開始合作關係，以[第二代鈴木Cultus為基礎發展](../Page/鈴木Cultus#第二代（SF系_1988年-1999年）.md "wikilink")[第二代速霸陸Justy](../Page/速霸陸Justy#第二代（1994年-2003年）.md "wikilink")，委託鈴木位在[匈牙利的工廠](../Page/匈牙利.md "wikilink")[馬札爾鈴木公司製造供應](../Page/馬札爾鈴木公司.md "wikilink")。2005年[豐田汽車入主富士重工業](../Page/豐田汽車.md "wikilink")，2009年鈴木開始和[福斯汽車合作](../Page/大众汽车.md "wikilink")，雙方越行越遠。2015年6月[東京證券交易所通過](../Page/東京證券交易所.md "wikilink")「企業管理方針」的新規定，要求企業提供交叉持股的合理解釋；加上近年來鈴木的整體營收下滑，導致富士重工業決定出售手中的鈴木[股票](../Page/股票.md "wikilink")\[8\]。

2017年4月1日，富士重工業更名為**速霸陸公司**。為了將資源集中在汽車產業，2017年10月2日速霸陸宣佈，大宮工廠全面停產建設用機器、工業機器及農業機器的通用引擎及發電機\[9\]，售後服務則委託關係企業產業機器公司負責。

### 再度生產飛機

1953年（昭和28年）9月富士重工業與美國[比奇飛機公司簽約](../Page/比奇飛機公司.md "wikilink")，授權生產[T-34教練機](../Page/T-34教練機.md "wikilink")；1955年（昭和30年）10月完成第一架國產1號機，交予防衛廳（今[防衛省](../Page/防衛省.md "wikilink")）使用。接著在1957年（昭和32年）11月成功開發出[第二次世界大戰後首度由日本自行研發的軍用機](../Page/第二次世界大戰.md "wikilink")[T-1教練機](../Page/T-1教練機.md "wikilink")（初鷹），至1963年為止共製造了66架給防衛廳。1962年起獲得美國[貝爾直升機公司授權製造](../Page/貝爾直升機公司.md "wikilink")[UH-1B直升機](../Page/UH-1直升機.md "wikilink")，供給[陸上自衛隊使用](../Page/陸上自衛隊.md "wikilink")；至1972年為止共生產90架。此外，該公司改良UH-1H型，陸上自衛隊命名為UH-1J型。1965年8月民用輕型飛機FA-200
Aero Subaru試飛成功，翌年10月開始量產發售。因為採用低翼式機體設計，在低速飛行時相當平穩而獲得好評，共製造了298架。

富士重工業也參與了[YS-11螺旋槳民航機的開發行列](../Page/YS-11.md "wikilink")，主要負責[主翼及](../Page/机翼.md "wikilink")[尾翼的部分](../Page/尾翼.md "wikilink")。也由於這樣的經驗，1973年（昭和48年）12月獲得美國[波音公司旗下](../Page/波音.md "wikilink")[波音民用飛機集團的青睞](../Page/波音民用飛機集團.md "wikilink")，供應[波音747的零件](../Page/波音747.md "wikilink")；甚至在1974年參加新世代客機[波音767的國際共同開發計劃](../Page/波音767.md "wikilink")。1974年富士重工業和美國[羅克韋爾公司共同攜手](../Page/羅克韋爾.md "wikilink")，開發FA-300雙螺旋槳7人座商務客機，並1975年11月試飛成功，1977年開始上市。可惜羅克韋爾公司將飛機生產部門出售，最終FA-300僅製造了42架。

1978年（昭和53年）1月17日試飛成功，接著3月開始量產，共製造出50架。1990年起富士重工業、[川崎重工業](../Page/川崎重工業.md "wikilink")、[三菱重工業等共同參與](../Page/三菱重工業.md "wikilink")[波音777的開發製造行列](../Page/波音777.md "wikilink")，其中中央機翼的部分由該公司負責。2002年6月投入[空中巴士A380的開發計劃](../Page/空中客车A380.md "wikilink")，負責垂直尾翼前緣及翼端、整流罩等部分；同年7月9日T-3改教練機試飛成功，2003年4月所生產的獲得[航空自衛隊採用](../Page/航空自衛隊.md "wikilink")。2005年該公司另投入[波音787](../Page/波音787.md "wikilink")[廣體飛機的開發計劃](../Page/廣體飛機.md "wikilink")，2008年起派遣4名技術人員參加[三菱支線噴射機的設計工作](../Page/三菱支線噴射機.md "wikilink")。至於在[太空載具設備方面](../Page/航天器.md "wikilink")，富士重工業也參與了包括[H-IIA運載火箭](../Page/H-IIA運載火箭.md "wikilink")、小型[人造衛星等主要元件的製造](../Page/人造衛星.md "wikilink")。

## 歷任社長

| 姓名                                         | 在任時間             | 備考                                                                                                                                                                                                                                                                                                                                                       |
| ------------------------------------------ | ---------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 北謙治                                        | 1953年7月至1956年7月  | 原金融公庫（前身）理事，選定[昴宿星團的](../Page/昴宿星團.md "wikilink")「昴（すばる）」做為汽車品牌，六連星代表前[中島飛機旗下](../Page/中島飛機.md "wikilink")5家公司和吸收合併這5家公司的富士重工業。任內開發的[速霸陸1500因故遭到阻撓](../Page/速霸陸1500.md "wikilink")，無法正式上市。                                                                                                                                                               |
| 吉田孝雄                                       | 1956年7月至1963年5月  | 曾任[中島飛機小泉製作所所長](../Page/中島飛機.md "wikilink")，任內開發了[T-1教練機](../Page/T-1教練機.md "wikilink")。                                                                                                                                                                                                                                                                 |
| 橫田信夫                                       | 1963年5月至1970年5月  | 原[電電公社副總裁出身的他和飛機](../Page/日本電信電話公社.md "wikilink")、汽車製造業相距甚遠，故選擇向該公司主要往來銀行（今[瑞穗銀行](../Page/瑞穗銀行.md "wikilink")）行長中山素平靠攏，被譏為「金融界的鞍馬[天狗](../Page/天狗.md "wikilink")」\[10\]，也種下日後興銀派和日產派相互傾軋的導火線。                                                                                                                                                             |
| [大原榮一](../Page/大原榮一.md "wikilink")         | 1970年5月至1978年10月 | 日本興業銀行出身，在他任內放棄與[三菱汽車](../Page/三菱汽車.md "wikilink")、[五十鈴汽車等的合作關係](../Page/五十鈴.md "wikilink")，改投向[日產汽車的懷抱](../Page/日產汽車.md "wikilink")；主要的車款產品則有[速霸陸ff-1 1300G](../Page/速霸陸ff-1_1300G.md "wikilink")、[速霸陸Leone](../Page/速霸陸Leone.md "wikilink")、[速霸陸Rex等](../Page/速霸陸Rex.md "wikilink")。                                                                   |
| [佐佐木定道](../Page/佐佐木定道.md "wikilink")       | 1978年10月至1985年6月 | [日產汽車公司出身](../Page/日產汽車.md "wikilink")，含有技術底蘊的他將整家公司帶入技術主導型。                                                                                                                                                                                                                                                                                             |
| [田島敏弘](../Page/田島敏弘.md "wikilink")         | 1985年6月至1990年6月  | 出身自日本興業銀行的社長，任內開發了[速霸陸Legacy](../Page/速霸陸Legacy.md "wikilink")，並且和[五十鈴汽車在](../Page/五十鈴.md "wikilink")[美國](../Page/美國.md "wikilink")[印第安那州](../Page/印第安那州.md "wikilink")[拉斐特合資成立](../Page/拉斐特_\(印第安纳州\).md "wikilink")「速霸陸五十鈴汽車廠」（2002年12月起改稱〔Subaru of Indiana Automotive, Inc.〕）。                                                                       |
| [川合勇](../Page/川合勇.md "wikilink")           | 1990年6月至1996年6月  | 日產柴油工業（日產汽車子公司）出身的社長，1998年12月1日因為涉嫌在1996年向[海上自衛隊的](../Page/海上自衛隊.md "wikilink")[防衛廳政務次官](../Page/防衛省.md "wikilink")（富士重工業前身[中島飛機創辦人](../Page/中島飛機.md "wikilink")[中島知久平之孫](../Page/中島知久平.md "wikilink")）行賄，而遭到日本警方逮捕\[11\]。                                                                                                                               |
| [田中毅](../Page/田中毅_\(富士重工業\).md "wikilink") | 1996年6月至2001年6月  | [日產汽車出身](../Page/日產汽車.md "wikilink")，任內推出[速霸陸Forester等](../Page/速霸陸Forester.md "wikilink")。                                                                                                                                                                                                                                                              |
| [竹中恭二](../Page/竹中恭二.md "wikilink")         | 2001年6月至2006年6月  | 自第二任社長吉田孝雄以來，睽違38年的內部直升社長\[12\]\[13\]。                                                                                                                                                                                                                                                                                                                   |
| [森郁夫](../Page/森郁夫_\(富士重工業\).md "wikilink") | 2006年6月至2011年6月  | [豐田汽車對該公司的](../Page/豐田汽車.md "wikilink")[持股比例從](../Page/股票.md "wikilink")9.5%升至16.61%，繼續成為最大[股東](../Page/股東.md "wikilink")\[14\]；並且推出[速霸陸Stella](../Page/速霸陸Stella.md "wikilink")、[速霸陸Pleo](../Page/速霸陸Pleo.md "wikilink")、[速霸陸Lucra](../Page/速霸陸Lucra.md "wikilink")、[速霸陸Exiga](../Page/速霸陸Exiga.md "wikilink")、[速霸陸XV等車款](../Page/速霸陸XV.md "wikilink")。 |
| [吉永泰之](../Page/吉永泰之.md "wikilink")         | 2011年6月至2018年6月  | 2013年3月創下全球共出售72萬4千輛汽車的新高紀錄，且不論營業毛利、淨利皆創下該公司史上的新高\[15\]。任內推出[速霸陸Levorg等車款](../Page/速霸陸Levorg.md "wikilink")，並自[速霸陸Impreza中獨立出](../Page/速霸陸Impreza.md "wikilink")[速霸陸WRX一系](../Page/速霸陸WRX.md "wikilink")。2017年4月1日公司更名成速霸陸公司，2018年6月5日由於產品數據造假，召開記者會道歉\[16\]。                                                                                            |
| [中村知美](../Page/中村知美.md "wikilink")         | 2018年6月迄今        |                                                                                                                                                                                                                                                                                                                                                          |
|                                            |                  |                                                                                                                                                                                                                                                                                                                                                          |

## 旗下部門

### 速霸陸汽車部門

  - 群馬製作所（[群馬縣](../Page/群馬縣.md "wikilink")[太田市](../Page/太田市.md "wikilink")）
  - 群馬製作所矢島工廠（群馬縣太田市）
  - 群馬製作所太田北工廠（群馬縣太田市）
  - 群馬製作所大泉工廠（群馬縣[邑樂郡](../Page/邑樂郡.md "wikilink")）

### 航空宇宙部門

  - 宇都宮製作所（[栃木縣](../Page/栃木縣.md "wikilink")[宇都宮市](../Page/宇都宮市.md "wikilink")）
  - 宇都宮製作所半田工廠（[愛知縣](../Page/愛知縣.md "wikilink")[半田市](../Page/半田市.md "wikilink")）
  - 宇都宮製作所半田西工廠（[愛知縣](../Page/愛知縣.md "wikilink")[半田市](../Page/半田市.md "wikilink")）

### 產業機器部門

  - 埼玉製作所（[埼玉縣](../Page/埼玉縣.md "wikilink")[北本市](../Page/北本市.md "wikilink")）

### 環境技術部門

  - Eco宇都宮工廠（栃木縣宇都宮市）：專門生產[鐵路車輛](../Page/鐵路車輛.md "wikilink")、[垃圾車](../Page/垃圾車.md "wikilink")「」（フジマイティー）\[17\]等特殊車輛。

### 住宅事業部門

  - 伊勢崎事業所（[群馬縣](../Page/群馬縣.md "wikilink")[伊勢崎市](../Page/伊勢崎市.md "wikilink")）

### 海外關係企業

  - Subaru of America, Inc.
  - Fuji Heavy Industries U.S.A., Inc.
  - Subaru Research & Development, Inc.
  - Subaru of Indiana Automotive, Inc.
  - Subaru Canada, Inc.
  - Subaru Industrial Power Products
  - Subaru Europe N.V./S.A.
  - Subaru Italia S.p.A.
  - N.V. Subaru Benelux
  - Subaru Vehicle Distribution B.V.
  - Robin Europe GmbH Industrial Engine & Equipment
  - 斯巴魯汽車（中國）有限公司
  - 富士重工業技術（北京）有限公司
  - 斯巴魯動機械（上海）有限公司
  - 常州富士常柴羅賓汽油機有限公司
  - 斯巴魯汽車（香港）有限公司
  - Fuji Heavy Industries（Singapore）Pte.Ltd.
  - 台灣速霸陸股份有限公司

## 主要產品

### 汽車

[Subaru_Impreza_G4_front.JPG](https://zh.wikipedia.org/wiki/File:Subaru_Impreza_G4_front.JPG "fig:Subaru_Impreza_G4_front.JPG")
[Subaru_Legacy_B4_tS.jpg](https://zh.wikipedia.org/wiki/File:Subaru_Legacy_B4_tS.jpg "fig:Subaru_Legacy_B4_tS.jpg")

  - [速霸陸Sambar](../Page/速霸陸Sambar.md "wikilink")：2012年第七代起改以作原型車。
  - [速霸陸Legacy](../Page/速霸陸Legacy.md "wikilink")
  - [速霸陸Impreza](../Page/速霸陸Impreza.md "wikilink")
  - [速霸陸Outback](../Page/速霸陸Outback.md "wikilink")
  - [速霸陸Forester](../Page/速霸陸Forester.md "wikilink")
  - [速霸陸Pleo](../Page/速霸陸Pleo.md "wikilink")：第二代的原型車是。
  - [速霸陸Pleo Plus](../Page/速霸陸Pleo_Plus.md "wikilink")
  - [速霸陸Tribeca](../Page/速霸陸Tribeca.md "wikilink")
  - [速霸陸Stella](../Page/速霸陸Stella.md "wikilink")：第二代的原型車是[大發Move](../Page/大發Move.md "wikilink")。
  - [速霸陸Exiga](../Page/速霸陸Exiga.md "wikilink")
  - [速霸陸XV](../Page/速霸陸XV.md "wikilink")
  - [速霸陸Trezia](../Page/速霸陸Trezia.md "wikilink")
  - [速霸陸Lucra](../Page/速霸陸Lucra.md "wikilink")
  - [速霸陸BRZ](../Page/速霸陸BRZ.md "wikilink")

### 航空

#### 軍用

  - J/AQM-1 無人標靶機
  - FFOS（）
  - [飛行模擬器](../Page/飛行模擬器.md "wikilink")

##### 授權生產

  - [T-34教練機](../Page/T-34教練機.md "wikilink")（[比奇飛機公司](../Page/比奇飛機公司.md "wikilink")）
      - LM-1/2通信聯絡機
      - KM-2教練機/TL-1通信聯絡教練機
      - T-3教練機
      - T-5教練機
      - T-7教練機
  - [UH-1B/H/J直升機](../Page/UH-1直升機.md "wikilink")（[貝爾直升機公司](../Page/貝爾直升機公司.md "wikilink")）
  - [AH-1S眼鏡蛇直升機](../Page/AH-1眼鏡蛇直升機.md "wikilink")（貝爾直升機公司）
  - [AH-64D阿帕契直升機](../Page/AH-64阿帕契直昇機.md "wikilink")（[波音](../Page/波音.md "wikilink")）
  - [愛國者飛彈](../Page/愛國者飛彈.md "wikilink")（[雷神公司](../Page/雷神公司.md "wikilink")）

##### 分擔生產

  - [F-2戰鬥機](../Page/F-2戰鬥機.md "wikilink")：主翼、尾翼等。
  - [T-4教練機](../Page/T-4教練機.md "wikilink")：主翼、尾翼、座艙罩等。
  - [川崎OH-1偵查直升機](../Page/川崎OH-1.md "wikilink")：尾翼、座艙罩等。
  - US-1A改救難[飛行艇](../Page/飛行艇.md "wikilink")：主翼、尾翼、引擎機艙等。
  - [P-3獵戶座海上巡邏機](../Page/P-3獵戶座海上巡邏機.md "wikilink")：授權生產主翼。
  - U-125搜索救難機：配件等。

#### 民用

  - FA-200 Aero Subaru民用輕型飛機
  - FA-300商務噴射機
  - [飛行模擬器](../Page/飛行模擬器.md "wikilink")
  - FFOS遠隔操縱觀測系統：該公司也曾發售民用版的「RPH-2型」。

##### 分擔生產

  - [波音737](../Page/波音737.md "wikilink")：。
  - [波音747](../Page/波音747.md "wikilink")：[副翼](../Page/副翼.md "wikilink")、[擾流板等](../Page/扰流板_\(航空\).md "wikilink")。
  - [波音757](../Page/波音757.md "wikilink")：外側襟翼。
  - [波音767](../Page/波音767.md "wikilink")：翼身整流罩、[起落架門扉等](../Page/起落架.md "wikilink")。
  - [波音777](../Page/波音777.md "wikilink")：參與共同開發，主翼、翼身整流罩、[起落架門扉等](../Page/起落架.md "wikilink")。
  - [波音787](../Page/波音787.md "wikilink")：參與共同開發，主翼等。
  - [空中巴士A380](../Page/空中客车A380.md "wikilink")：垂直尾翼構造。
  - 霍克Horizon M4000：主翼系統\[18\]。
  - 貝爾·[奧古斯塔偉士蘭AW609傾轉旋翼機](../Page/奧古斯塔偉士蘭AW609傾轉旋翼機.md "wikilink")：機體構造開發。
  - 日蝕500：主翼。
  - [Dash 8](../Page/Dash_8.md "wikilink")

##### 授權生產

  - [貝爾204B](../Page/貝爾204/205直升機.md "wikilink")
  - [貝爾205B](../Page/貝爾204/205直升機.md "wikilink")

##### 太空載具

  - [H-IIA運載火箭](../Page/H-IIA運載火箭.md "wikilink")
  - 背負式衛星
  - 人工衛星

### 產業機械

富士重工業以「[羅賓](../Page/羅賓發動機.md "wikilink")」（、）為[品牌名稱](../Page/品牌.md "wikilink")，開發生產泛用型[發動機](../Page/發動機.md "wikilink")、[發電機](../Page/發電機.md "wikilink")、[幫浦](../Page/泵.md "wikilink")、[雪上摩托車](../Page/雪上摩托車.md "wikilink")、[卡丁車引擎](../Page/卡丁車.md "wikilink")、[照明裝置等產品](../Page/照明.md "wikilink")。生產工廠分別設於[中國及](../Page/中國.md "wikilink")[美國](../Page/美國.md "wikilink")，其中泛用型發動機在全球的市佔率排名第4。

### 房屋住宅

過去富士重工業曾生產小型[裝配式住宅](../Page/裝配式建築.md "wikilink")，並透過（）以郵購的方式銷售。

## 以前的產品

### 輕型三輪車

  - 富士工業曾推出輕型三輪車，名為「Dynaster」（），1950年代由大宮富士工業製造，在[摩托車後方加裝可以載貨的車斗](../Page/摩托車.md "wikilink")，符合戰後民生需求的產物\[19\]。

### 二輪車

  - Hurricane（）：由富士工業開發製造，1954年上市時原名「KingDyna」（），原廠代號T91型，配置排氣量250c.c.的引擎；兩年後改良版的T92型問世並更名成「Hurricane」，直到1957年停產。此款摩托車目前在日本的數量稀少，相當罕見\[20\]。

### 巴士車身

群馬縣伊勢崎工廠在1947年起製造輕量化的巴士車體，接著1949年仿效美國製巴士，獨自開發出日本汽車史上第一輛後置引擎、無框架單體結構（或稱[應力外皮結構](../Page/應力.md "wikilink")）的巴士，稱為「富士號（，富士TR014X-2）」。後來，日本國內主要的大型巴士製造商有四家，以日產柴油工業（今[UD卡車](../Page/UD卡車.md "wikilink")）為中心，皆是富士重工業的主要客戶。但是這些客戶逐漸將車身委託[底盤製造廠](../Page/底盤.md "wikilink")（大部分是客戶的子公司）內製化，巴士車體的需求量大減；加上1998年[三菱扶桑和](../Page/三菱扶桑卡客车.md "wikilink")[日野汽車陸續退出巴士製造的市場](../Page/日野汽車.md "wikilink")，1999年[五十鈴汽車停止供應底盤](../Page/五十鈴.md "wikilink")，日產柴油更在2002年1月轉單給，同年5月富士重工業判斷此業務難以延續，遂忍痛停止伊勢崎工廠的巴士車體業務。此外，該公司企劃、製造的、[雙節公車等業務也在](../Page/铰接客车.md "wikilink")2000年結束。後來該公司另成立[速霸陸客製工房](../Page/速霸陸客製工房.md "wikilink")（，今[桐生工業伊勢崎工廠](../Page/桐生工業.md "wikilink")），從事售後服務、維修保養等業務。
[FHI_Fuji-go_001.JPG](https://zh.wikipedia.org/wiki/File:FHI_Fuji-go_001.JPG "fig:FHI_Fuji-go_001.JPG")

  - 主要產品計有：
  - R13
      - 13
      - 3A/3B/3D/3E
      - R1/R2
  - R14
      - 14
      - 4B/4E
  - R15
      - 5B/5E
      - R1/R2/R3
      - HD1/HD2/HD3
  - R16
      - 6B/6E
      - H1
  - R17
      - 7B/7E
      - 7HD
      - 7S
  - R18
      - 8B/8E
  - R21
      - 1M/1S

[File:K-U31L-Hitachi-Dentetsu.jpg|3E](File:K-U31L-Hitachi-Dentetsu.jpg%7C3E)
日產柴油 K-U31L <File:Aritakotsu> P-RA52T FHI R3.jpg|R3 日產柴油 P-RA52T
[File:KL-UA452KAN-SotetsuBus-50.jpg|7E](File:KL-UA452KAN-SotetsuBus-50.jpg%7C7E)
日產柴油 KL-UA252KAN <File:IwateKenpokuBus> KL-RA552RBN No.613.jpg|1M 日產柴油
Space Arrow

### 鐵道車輛

從1950年代起，富士重工業為[日本國鐵](../Page/日本國鐵.md "wikilink")、[JR集團以及](../Page/JR集團.md "wikilink")[私鐵](../Page/私鐵.md "wikilink")、[第三部門鐵道等各鐵道事業體承造各種鐵道車輛](../Page/第三部門鐵道.md "wikilink")。但因訂單數量日漸下滑，富士重工業於2002年決定終止其栃木縣宇都宮市的鐵道車輛生產工廠，2003年由[新潟運輸系統](../Page/新潟運輸系統.md "wikilink")（）接手其鐵道車輛事業部門。

## 內部連結

  - [中島飛機](../Page/中島飛機.md "wikilink")
  - [富士兔子速克達](../Page/富士兔子速克達.md "wikilink")
  - [速霸陸汽車](../Page/速霸陸汽車.md "wikilink")
  - [大慶汽車](../Page/大慶汽車.md "wikilink")

## 參考資料

  - [富士重工業：沿革](http://www.fhi.co.jp/ir/report/pdf/fact/2011/fact_16.pdf)

## 外部連結

  - [日文官方網站](http://www.subaru.co.jp/)

  - [英文官方網站](https://www.subaru.co.jp/en/)

[Category:速霸陸](../Category/速霸陸.md "wikilink")
[Category:豐田集團](../Category/豐田集團.md "wikilink")
[Category:日本軍事工業](../Category/日本軍事工業.md "wikilink")
[Category:日本製造公司](../Category/日本製造公司.md "wikilink")
[Category:日本飛機公司](../Category/日本飛機公司.md "wikilink")
[Category:日本汽車公司](../Category/日本汽車公司.md "wikilink")
[Category:日本機車公司](../Category/日本機車公司.md "wikilink")
[Category:巴士生產商](../Category/巴士生產商.md "wikilink")
[Category:日本鐵路車輛製造商](../Category/日本鐵路車輛製造商.md "wikilink")
[Category:總部在日本的跨國公司](../Category/總部在日本的跨國公司.md "wikilink")
[Category:澀谷區公司](../Category/澀谷區公司.md "wikilink")
[Category:芙蓉集團](../Category/芙蓉集團.md "wikilink")
[Category:東京證券交易所上市公司](../Category/東京證券交易所上市公司.md "wikilink")
[Category:日經平均指數](../Category/日經平均指數.md "wikilink")
[Category:1953年成立的公司](../Category/1953年成立的公司.md "wikilink")

1.  請參見[企業情報@Wiki - 富士重工業](http://www4.atwiki.jp/sysd/pages/1513.html)。
2.  從桂木洋二所著之《**てんとう虫**が走った日―スバル360開発物語》書名可見一斑。
3.  直到2003年五十鈴汽車退出該工廠的經營。
4.  參看[経済社会総合研究所：第8章
    「失われた10年」の様相](http://www.esri.go.jp/jp/prj/sbubble/history/history_02/analysis_02_04_08.pdf)，頁11。
5.  [SUBARU: 富士重工業
    ＧＭとの提携関係を解消、トヨタ自動車との新たな関係構築へ](https://www.fhi.co.jp/news/05_10_12/05_10_05_6.pdf)。
6.  參看[関東経済産業局：富士重工業株式会社による補助金等の不正受給に対する措置](http://www.kanto.meti.go.jp/annai/hodo/data/20130131jinzai.pdf)
    。
7.  [Response:
    富士重工業、スズキ株を売却](http://response.jp/article/2016/08/10/279957.html)。
8.  [@niftyニュース：富士重工業と関係解消に動いたスズキに「富士重の成長に見切りをつけたため」との見方](https://news.nifty.com/article/item/neta/12111-26334/)。
9.  [株式会社SUBARU：汎用エンジン・発電機等の生産・販売を終了](https://www.subaru.co.jp/press/news/2017_10_02_4800/)。
10. 參考[月刊BOSSｘWizBiz:
    設立60年を経ても宿る　中島飛行機のDNA](http://boss.wizbiz.me/special/special1305_05.html)，「興銀支配で迷走」之段落。
11. 參看[日經BP社：富士重工・川合会長逮捕――防衛産業の罠に墜ちた「名経営者」](http://www.nikkeibp.co.jp/archives/047/47175.html)
    。
12. 參考[月刊BOSSｘWizBiz:
    設立60年を経ても宿る　中島飛行機のDNA](http://boss.wizbiz.me/special/special1305_05.html)，倒數第4段。
13. 富士重工業背後有兩大股東：日本興業銀行及日產汽車，自第二任以降的社長皆自前二者空降，直到竹中恭二才恢復社長由公司內部直升。
14. 參看[U-Car車壇新聞:借重Subaru資源，Toyota將增加對富士重工持股達16.61%](http://news.u-car.com.tw/8993.html)。
15. 參看[週刊エコノミスト：経営者：編集長インタビュー　吉永泰之　富士重工業社長](http://www.weekly-economist.com/2013/09/17/%E7%B5%8C%E5%96%B6%E8%80%85-%E7%B7%A8%E9%9B%86%E9%95%B7%E3%82%A4%E3%83%B3%E3%82%BF%E3%83%93%E3%83%A5%E3%83%BC-%E5%90%89%E6%B0%B8%E6%B3%B0%E4%B9%8B-%E5%AF%8C%E5%A3%AB%E9%87%8D%E5%B7%A5%E6%A5%AD%E7%A4%BE%E9%95%B7-2013%E5%B9%B49%E6%9C%8817%E6%97%A5%E7%89%B9%E5%A4%A7%E5%8F%B7/)，第一段。
16. [日本経済新聞：スバル、新たな燃費不正　吉永社長が代表権返上](https://www.nikkei.com/article/DGXMZO3139640005062018000000/)。
17. 可參考[塵芥収集車（フジマイティー）](http://www.fuji-toku.jp/product/product01.html)
    。
18. 請見[SUBARU: 富士重工業 新型ビジネスジェット機
    初号機の主翼を納入](http://www.fhi.co.jp/news/99_4_6/04_06.htm)。
19. 參考[草ヒロ探検隊：大宮富士工業ダイナスター（T73A）](http://kusahiroexploration.blog107.fc2.com/blog-entry-2558.html)。
20. 請看[バイク多聞天 画像ギャラリー：No.00702
    富士重工・ハリケーンＴ９２Ｃ](http://www.bike-tamonten.com/gallery/read.php?id=739)
    。