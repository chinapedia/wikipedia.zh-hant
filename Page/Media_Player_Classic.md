**Media Player Classic**是一個簡潔的[媒体播放器](../Page/媒体播放器.md "wikilink")。Media
Player Classic含有與[Windows Media
Player](../Page/Windows_Media_Player.md "wikilink")
6.4幾乎相同的介面。然而，這是與Windows Media Player
6.4完全不同的應用程式，它含有其它近代的播放器相似的功能及選項。

Media Player
Classic是由名為"Gabest"的程式員建立，現時他仍然有維護這個程式。Gabest原先是以不公開原始碼的方式開發Media
Player Classic，但後來他開放了Media Player
Classic的[原始碼](../Page/开放源代码.md "wikilink")。現時Media
Player
Classic是基於[GPL下發佈](../Page/GNU_General_Public_License.md "wikilink")，另外，在[SourceForge亦開發了名為](../Page/SourceForge.md "wikilink")*Guliverkli*的專案。

在*Guliverkli*的專案頁中亦有大量的編碼，閱讀器及解碼器可供下載。

MPC项目现在主要由Doom9论坛社群维护。现时活跃的分支为Black Edition（MPC-BE）。[Media Player
Classic Home
Cinema](../Page/Media_Player_Classic_Home_Cinema.md "wikilink")（MPC-HC）分支于2017年7月16日宣布终止。\[1\]

目前clsid2接替更新1.7.13後版本。

## 特色

### MPEG-1、MPEG-2及MPEG-4播放

能播放[VCD](../Page/VCD.md "wikilink")、[SVCD及](../Page/SVCD.md "wikilink")[DVD格式](../Page/DVD.md "wikilink")，無需安裝額外軟件及編碼／解碼器。Media
Player
Classic內含[MPEG-2影像支援字幕播放及](../Page/MPEG-2.md "wikilink")[LPCM](../Page/PCM.md "wikilink")、[MP2](../Page/MP2.md "wikilink")、[AC3及](../Page/AC3.md "wikilink")[DTS音效播放](../Page/DTS.md "wikilink")。亦包含改進的[MPEG解碼器以便支援SVCD字幕](../Page/MPEG.md "wikilink")，利用其VCD/SVCD/XCD閱讀器，可直接播放VCD及SVCD。2005年10月30日，Gabest加上[\*.mp4及](../Page/MPEG-4_Part_14.md "wikilink")[MPEG-4時間字幕支援](../Page/MPEG-4_Part_17.md "wikilink")。\[2\]另外，[AAC解碼器亦已在MPC上推行](../Page/AAC.md "wikilink")，這可令MPC可以播放以AAC壓縮的mp4檔案，成為[Winamp及](../Page/Winamp.md "wikilink")[iTunes代替品](../Page/iTunes.md "wikilink")。

### QuickTime及RealPlayer結構

由於這個播放器主要是基於[DirectShow結構運作](../Page/DirectShow.md "wikilink")，如果在您的電腦中已經安裝了[QuickTime及](../Page/QuickTime.md "wikilink")／或[RealPlayer](../Page/RealPlayer.md "wikilink")，MPC亦都可以兩種原先的播放編碼播放這些檔案。

### Matroska及Ogg容器

Media Player
Classic本身包含了對[OGM及](../Page/OGM.md "wikilink")[Matroska等](../Page/Matroska.md "wikilink")[容器格式的播放支援](../Page/容器格式.md "wikilink")。

### 電視調解器

如果已經安裝[電視調解卡的話](../Page/電視調解卡.md "wikilink")，MPC是可以支援[電視播放及錄影](../Page/電視.md "wikilink")。

### 圖片檢視器

Media Player
Classic內置數項圖片格式的支援，包括[JPEG](../Page/JPEG.md "wikilink")、[BMP](../Page/BMP.md "wikilink")、[GIF等常见图像格式檔案](../Page/GIF.md "wikilink")。

### 其他

对[流媒体的支持较差](../Page/流媒体.md "wikilink")，如mms或Realnetworks流媒体协议的文件基本无法播放。此外界面也较为朴素，仍然采用的是最古老的[Windows软件的标准界面](../Page/Windows.md "wikilink")，与多数媒体播放软件华丽的界面大异其趣。

## Media Player Classic與影音包

可能有人認為一些解碼器安裝包（例如[K-Lite Codec
Pack或](../Page/K-Lite.md "wikilink")[暴风影音](../Page/暴风影音.md "wikilink")*Storm
codec*等）就等同於Media Player
Classic。但其實事實上這是不正確的。這些的解碼器安裝包通常會綁上很多的解碼器模組，再加上Media
Player Classic推出。但是Media Player
Classic是一個獨立使用的程式，已經包含了大部份媒體格式檔的支援。在大部份的情況下，Media
Player Classic都能夠支援一般使用的檔案。

## GPL版權被侵害

  - 2005年4月23日，Gabest在專案的新聞頁面中發佈信息，指出有2個軟件被懷疑使用了該專案的原始碼卻未同樣以[GPL授權發佈](../Page/GPL.md "wikilink")，違反了GPL的授權條款，一個是[韓國人製作的免費軟件](../Page/韓國.md "wikilink")[The
    KMPlayer](../Page/The_KMPlayer.md "wikilink")，另一個是商業軟件[VX30](http://www.vx30.com/)。Gabest對這個事件表示沮喪，他不知道如何對有關已違反了授權的組織進行有關的行動。\[3\]
  - 2008年[The KMPlayer作者姜勇囍進入Daum](../Page/The_KMPlayer.md "wikilink")
    Communications，發表了改良版本[Daum
    PotPlayer](../Page/Daum_PotPlayer.md "wikilink")，因此PotPlayer繼承了KMPlayer侵害GPL授權條款。
  - 2009年[暴风影音和腾讯旗下的](../Page/暴风影音.md "wikilink")[QQ影音先后因未遵循GPL协议登上](../Page/QQ影音.md "wikilink")“耻辱堂”\[4\]。而[射手影音播放器也曾被指责有违反GPL协议的嫌疑](../Page/射手影音播放器.md "wikilink")，但软件作者否认\[5\]。
  - 2009年射手播放器获得MPC-HC赞赏并成为MPC-HC的代码贡献者。\[6\]

## 注释

## 參考文獻

## 外部連結

  - [Guliverkli](../Page/:sourceforge:projects/guliverkli/.md "wikilink")
    - 在[SourceForge.net的Media](../Page/SourceForge.net.md "wikilink")
    Player Classic專案頁
  - [Guliverkli2](../Page/:sourceforge:projects/guliverkli2/.md "wikilink")
    - 在SourceForge.net的Media Player Classic修正版專案頁
  - [MPC-HC](../Page/:sourceforge:projects/mpc-hc/.md "wikilink") -
    在SourceForge.net的MPC-HC專案頁
  - <span class=plainlinks></span> -
    在[GitHub的MPC](../Page/GitHub.md "wikilink")-HC專案頁(在1.7.13之後的新版本)

## 参见

  - [Real Alternative](../Page/Real_Alternative.md "wikilink")
  - [QuickTime Alternative](../Page/QuickTime_Alternative.md "wikilink")
  - [Media Player Classic
    Homecinema](../Page/Media_Player_Classic_Homecinema.md "wikilink")
  - [媒体播放器列表](../Page/媒体播放器列表.md "wikilink")

{{-}}

[Category:媒体播放器](../Category/媒体播放器.md "wikilink")
[Category:綠色軟件](../Category/綠色軟件.md "wikilink")
[Category:SourceForge專案](../Category/SourceForge專案.md "wikilink")

1.
2.  [在Doom9論壇Gabest釋出之最新CVS組建](http://forum.doom9.org/showthread.php?t=101835)
3.  <http://sourceforge.net/forum/forum.php?forum_id=462894>
4.
5.
6.  <http://www.cnbeta.com/articles/91973.htm>