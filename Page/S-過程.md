**S-過程**，或稱為[慢中子捕獲過程](../Page/中子捕獲.md "wikilink")，是發生在相對來說中子密度較低和溫度中等條件下的[恆星進行](../Page/恆星.md "wikilink")[核合成過程](../Page/核合成.md "wikilink")。在這樣的條件下，原子的[核心進行](../Page/原子核.md "wikilink")[中子捕獲的速率相較之下就低於](../Page/中子捕獲.md "wikilink")[β負衰變](../Page/β衰變.md "wikilink")。穩定的同位素捕獲中子；但是放射性同位素在另一次中子捕獲前就先衰變成為穩定的子核，這樣經由[β穩定的過程](../Page/β穩定.md "wikilink")，使同位素沿著[同位素列表的槽線移動](../Page/同位素列表.md "wikilink")。S-過程大約創造了另一半[比鐵重的元素](../Page/重元素.md "wikilink")，因此在[星系化學演化中扮演著很重要的角色](../Page/化學演化.md "wikilink")。S-過程與更快速的[r-過程中子捕獲不同的是它的低速率](../Page/r-過程.md "wikilink")。

## 歷史

**S-過程**似乎必須從重元素的同位素相對豐度和在1956年由和[哈羅德·尤里重新印製的](../Page/哈羅德·尤里.md "wikilink")[化學元素豐度表來觀察](../Page/化學元素豐度表.md "wikilink")，尤其是[鍶](../Page/鍶.md "wikilink")、[鋇](../Page/鋇.md "wikilink")、和[鉛這三種元素豐度的峰值](../Page/鉛.md "wikilink")。根據[量子力學和](../Page/量子力學.md "wikilink")[殼層模型](../Page/殼層模型.md "wikilink")，這三種是特別穩定的核子，行為很像[惰性氣體](../Page/稀有气体.md "wikilink")，在化學上是[無活性的](../Page/無活性的.md "wikilink")。這暗示了有些含量豐富的核子必須經由慢[中子捕獲來創造](../Page/中子捕獲.md "wikilink")，並且也只能估算哪些核子可以經歷這樣的過程。回顧1957年，即有一篇現稱為[B<sup>2</sup>FH的論文提出了S](../Page/B2FH.md "wikilink")-過程和[R-過程的分攤表](../Page/R-過程.md "wikilink")\[1\]，也認為S-過程會在[紅巨星內發生](../Page/紅巨星.md "wikilink")。
但是，直到1961年才有計算的模型指出，無論在任何時間要創造出比鐵重的元素，都要以鐵為種核。\[2\]這件工作顯示，天文學家在某些紅巨星觀察到異常豐度的鋇，如果中子的累積通量（每單位區間內中子的數量）適當的話，可能是以鐵為種核產生的。它也顯示唯一的累積通量不可能說明觀測到的s-過程豐度，但是大範圍的累積通量是必須的，鐵的種核暴露在特定累積通量下的數量必須隨著中子強度的增加而減少。這項工作也顯示，中子捕獲的截面積相對於豐度不是平滑的下降曲線，而是有著*懸崖絕壁*的構造。在1970年代，克萊頓在以指數下降的中子累積通量做為鐵種核的函數為基礎的假設下，這樣的結果被當成S-過程的標準模型，並維持到對[漸近巨星分支](../Page/漸近巨星分支.md "wikilink")（AGB）的核合成有了詳細的了解，獲得足夠的進展，才成為建立在恆星結構模型上的標準模型。
1965年，來自橡樹嶺國家實驗室一系列有關中子捕獲截面積測量的重要報告\[3\] 和其後在1982年來自卡爾斯魯厄核物理中心的，\[4\]
才使s-過程得以在今天獲得一定數量的穩固位置和享受它的地位。

## 恆星中的S-過程

S-過程被認為能發生在大部分的[漸近巨星分支恆星](../Page/漸近巨星分支.md "wikilink")；相對的，R-過程被認為發生在超新星爆炸環境的最初幾秒鐘內，而S-過程在這樣的環境下可以持續進行數千年。S-過程在同位素圖上使[質量數的增加](../Page/質量數.md "wikilink")，根本上取決於恆星能產生的[中子數量](../Page/中子.md "wikilink")，以及起初開始時鐵元素在恆星內豐度的分布。[鐵是系列的](../Page/鐵.md "wikilink")[中子捕獲或是](../Page/中子捕獲.md "wikilink")[β負衰變合成新元素的原始材料](../Page/β負衰變.md "wikilink")（或是種子）。

主要的中子源反應如下：

<font size= "-1"><sup>13</sup>[</font>C](../Page/碳13.md "wikilink") + α
→ <font size= "-1"><sup>16</sup>[</font>O](../Page/氧16.md "wikilink")
+ n

<font size= "-1"><sup>22</sup>[</font>Ne](../Page/氖22.md "wikilink") + α
→ <font size= "-1"><sup>25</sup>[</font>Mg](../Page/鎂25.md "wikilink") +
n

[S-process-elem-Ag-to-Sb.svg](https://zh.wikipedia.org/wiki/File:S-process-elem-Ag-to-Sb.svg "fig:S-process-elem-Ag-to-Sb.svg")到[Sb的範圍內的行為](../Page/銻.md "wikilink")。\]\]

主要的和微弱的S-過程有一個明顯的區別。主要的成分導致低金屬量恆星的[Sr和](../Page/鍶.md "wikilink")[Y](../Page/釔.md "wikilink")，成為[Pb](../Page/鉛.md "wikilink")，主要成分發生的場所在低質量的[漸近巨星分支恆星](../Page/漸近巨星分支.md "wikilink")。一篇發表在*Science*雜誌上的簡短而精彩的文章，討論和說明了兩顆在AGB階段的中子源如何經營之間複雜的脈動週期。\[5\]在另一方面，S-過程弱的成分，綜合了由鐵的族群成為[Sr和](../Page/鍶.md "wikilink")[Y](../Page/釔.md "wikilink")，並且發生的場所在[氦和](../Page/氦.md "wikilink")[碳燃燒結束階段的大質量恆星](../Page/碳.md "wikilink")。這些恆將成為超新星，並將這些同位素擴散致星際空間內。

S-過程經常使用數學上所謂的局部近似來處理，建立在恆星內的中子流量是常數的假設下，先假設一個元素自然豐度的理論模型，所以豐度的比率與產生不同的同位素中子捕獲截面積比率是相反的。這種局部近似－如同名稱所顯示的－只是局部的有效，意為著多數的同味素有著相同的原子量。

因為期待的S-過程發生在相對於低中子的通量（數量級在10<font size= "-1"><sup>5</sup></font>至10<font size= "-1"><sup>11</sup></font>中子每公分<font size= "-1"><sup>2</sup></font>每秒），這種過程不能產生如[釷或](../Page/釷.md "wikilink")[鈾這樣重的放射性同位素](../Page/鈾.md "wikilink")。

S-過程週期終止於：
<font size= "-1"><sup>209</sup>[</font>Bi](../Page/鉍209.md "wikilink")
+ n<font size= "-1"><sup>0</sup></font> →
<font size= "-1"><sup>210</sup>[</font>Bi](../Page/鉍210.md "wikilink") +
γ

<font size= "-1"><sup>210</sup></font>Bi →
<font size= "-1"><sup>210</sup>[</font>Po](../Page/釙210.md "wikilink") +
β<font size= "-1"><sup>-</sup></font>

<font size= "-1"><sup>210</sup></font>Po →
<font size= "-1"><sup>206</sup>[</font>Pb](../Page/鉛206.md "wikilink") +
α

鉛-206接著捕獲3個中子，產生[Pb-209](../Page/鉛209.md "wikilink")，然後由β負衰變成為鉍，重複這個循環。

## 在星際塵埃中測量S-過程

星際塵埃是[宇宙塵的一種成分](../Page/宇宙塵.md "wikilink")。在成為太陽系內被找到的隕石之前，這些單獨的固體顆粒是來自早已死亡的恆星，並被保存了下來。對這些顆粒的測量，在實驗室內顯示這些顆粒內的元素有著異常的同位素豐度，結果給予天體物理學新的認知。\[6\]
矽碳化物的顆粒在AGB
恆星的大氣中凝聚，而成為這些恆星會擁有這些同位素。因為AGB恆星是銀河系內進行S-過程的主要場所，因此重元素的矽碳化物顆粒實際上純粹是S-過程產生的同位素。這個事實經由濺射離子質譜儀對前太陽系顆粒的研究被一再的展示。\[7\]
有些濺射的結果顯示S-過程和R-過程的豐度比率和早先假設的有些不同，它也顯示了氪和氙在恆星大氣層的豐度會因S-過程而隨時間改變，或是因恆星而異，推測可能是中子濃度的累積通量或是溫度的影響。這是現今對S-過程研究的新領域。

## 參考資料

## 参见

  - [β衰变](../Page/β衰变.md "wikilink")
  - [R-过程](../Page/R-过程.md "wikilink")

[Category:原子核物理学](../Category/原子核物理学.md "wikilink")
[Category:中子](../Category/中子.md "wikilink")
[Category:核合成](../Category/核合成.md "wikilink")
[Category:天体物理学](../Category/天体物理学.md "wikilink")

1.

2.

3.

4.

5.

6.

7.