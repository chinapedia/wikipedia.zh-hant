**艾哈迈德三世**（1673年出生，1736年7月1日逝世于[君士坦丁堡](../Page/君士坦丁堡.md "wikilink")）是[奥斯曼帝国的苏丹](../Page/奥斯曼帝国.md "wikilink")，他的统治时期是从1703年到1730年，這段時期在奧斯曼帝國歷史上被稱作[鬱金香時期](../Page/鬱金香時期.md "wikilink")。

## 生平

[Jean-Baptiste_van_Mour_006.jpg](https://zh.wikipedia.org/wiki/File:Jean-Baptiste_van_Mour_006.jpg "fig:Jean-Baptiste_van_Mour_006.jpg")接見法國大使\]\]
[Koceks_-_Surname-i_Vehbi.jpg](https://zh.wikipedia.org/wiki/File:Koceks_-_Surname-i_Vehbi.jpg "fig:Koceks_-_Surname-i_Vehbi.jpg")

艾哈迈德三世是[穆罕默德四世的一个儿子](../Page/穆罕默德四世_\(奥斯曼帝国\).md "wikilink")，他的哥哥[穆斯塔法二世于](../Page/穆斯塔法二世.md "wikilink")1703年退位后他登基。由于当时奥斯曼帝国与[俄国的关系日趋紧张](../Page/俄国.md "wikilink")，艾哈迈德三世倾向于亲近[英国](../Page/英国.md "wikilink")。1709年瑞典国王[查理十二世在](../Page/查理十二世.md "wikilink")[波尔塔瓦战役战败后逃亡奥斯曼帝国](../Page/波尔塔瓦战役.md "wikilink")，受到艾哈迈德三世的保护。1711年他的军队彻底包围了俄军，这是奥斯曼帝国对俄作战的历史上最大的胜利。在俄国与奥斯曼帝国签署的条约中，俄国向奥斯曼帝国割让[亚速](../Page/亚速.md "wikilink")，摧毁所有俄国沿两国边境建立的要塞，保证今后不介入[波兰和](../Page/波兰.md "wikilink")[哥萨克的内政](../Page/哥萨克.md "wikilink")。虽然如此在奥斯曼帝国内部许多人觉得这些条件依然太宽松了，因此两国之间几乎重新爆发战争。

1715年奥斯曼帝国占领了[伯罗奔尼撒半岛上的](../Page/伯罗奔尼撒半岛.md "wikilink")[摩里亞](../Page/摩里亞.md "wikilink")。摩里亞本来是[威尼斯共和国的领地](../Page/威尼斯共和国.md "wikilink")，由于威尼斯与[奥地利之间有联盟](../Page/奥地利.md "wikilink")，因此奥斯曼帝国此举导致了它与奥地利之间的战争。在这场战争中奥斯曼帝国未能取胜。1716年夏奥斯曼海军围攻[科孚島](../Page/科孚島.md "wikilink")，但是被威尼斯击退。1717年奥地利占领[贝尔格莱德](../Page/贝尔格莱德.md "wikilink")。在英国和[荷兰的帮助下奥斯曼帝国于](../Page/荷兰.md "wikilink")1718年与奥地利达成了[巴沙洛维兹和约](../Page/巴沙洛维兹和约.md "wikilink")。奥斯曼帝国得以保存摩里亞半岛，但丧失了[匈牙利](../Page/匈牙利.md "wikilink")。

1727年艾哈迈德三世废除了在奥斯曼帝国境内不准印书的禁令。

在与[波斯的战争中奥斯曼帝国大败](../Page/波斯.md "wikilink")，苏丹的禁卫队暴动，1730年9月艾哈迈德三世被废黜囚禁至死。

[Category:奥斯曼帝国苏丹](../Category/奥斯曼帝国苏丹.md "wikilink")
[Category:1673年出生](../Category/1673年出生.md "wikilink")
[Category:1736年逝世](../Category/1736年逝世.md "wikilink")