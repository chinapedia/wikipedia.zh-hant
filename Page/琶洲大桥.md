**琶洲大桥**是[中国](../Page/中国.md "wikilink")[广州市的一座跨越](../Page/广州市.md "wikilink")[珠江桥梁](../Page/珠江.md "wikilink")，属于[广州国际会议展览中心配套工程](../Page/广州国际会议展览中心.md "wikilink")，前称**黄洲大桥**，於2003年8月30日通车。大桥位於市区东部[华南大桥和](../Page/华南大桥.md "wikilink")[东圃大桥之间](../Page/东圃大桥.md "wikilink")，北起[天河区](../Page/天河区.md "wikilink")[科韵路与](../Page/科韵路.md "wikilink")[黄埔大道的交叉口](../Page/黄埔大道.md "wikilink")，跨珠江，经琶洲，与新港东路相交，南止于[海珠区](../Page/海珠区.md "wikilink")[新滘南路](../Page/新滘南路.md "wikilink")，与新港东路交汇，是[城市快捷路的跨江桥梁](../Page/广州城市快捷路.md "wikilink")。全长3.6公里，行车道宽度60米，双向六车道。其中大桥长1205米，桥宽32米，通航高度20米，大桥属V形墩刚构—连续梁组合结构；通车后将可分流[广州大桥之车辆](../Page/广州大桥.md "wikilink")。

[P琶](../Category/广州桥梁.md "wikilink")
[Category:珠江](../Category/珠江.md "wikilink")
[P琶](../Category/天河区.md "wikilink")
[P琶](../Category/海珠区.md "wikilink")
[Category:2003年完工橋梁](../Category/2003年完工橋梁.md "wikilink")