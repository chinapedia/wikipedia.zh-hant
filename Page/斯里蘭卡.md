**斯里兰卡民主社会主义共和国**（；；），通称**斯里兰卡**（；；），1972年之前称**锡兰**，是一个位於[南亞](../Page/南亞.md "wikilink")－[印度次大陸東南方外海的](../Page/印度次大陸.md "wikilink")[島國](../Page/島國.md "wikilink")，属于[亚洲](../Page/亚洲.md "wikilink")。古代中国曾經稱之為[已程不](../Page/已程不.md "wikilink")、[獅子國](../Page/獅子國.md "wikilink")、師子國、[僧伽羅](../Page/僧伽羅.md "wikilink")、[楞伽島](../Page/楞伽島.md "wikilink")。

斯里兰卡是[單一制](../Page/單一制.md "wikilink")[共和國](../Page/共和國.md "wikilink")，其政治首都位於[斯里賈亞瓦德納普拉科特](../Page/斯里賈亞瓦德納普拉科特.md "wikilink")（简称为**科特**），经济首都位于[科伦坡](../Page/科伦坡.md "wikilink")。

## 名称

  - 斯里蘭卡梵语古名Simhalauipa，驯狮人，《[汉书](../Page/汉书.md "wikilink")·地理志》称「已程不国」。《[梁书](../Page/梁书.md "wikilink")》称「狮子国」。《大唐西域记》作「僧伽罗」，即梵语古名Simhalauipa的音译。斯里蘭卡的主要民族至今在漢語裏仍稱“僧伽罗人”，其語言稱“僧伽罗语”。

<!-- end list -->

  - 斯里蘭卡古阿拉伯语Sirandib，[宋代音译为](../Page/宋代.md "wikilink")“细兰”，[明代称](../Page/明代.md "wikilink")“锡兰”。

<!-- end list -->

  - 1815年起作爲皇家殖民地由[英國統治](../Page/英國.md "wikilink")，正式名稱為“Ceylon”，漢譯“錫蘭”。此英語名稱來自[葡萄牙語的](../Page/葡萄牙語.md "wikilink")“Ceilão”。1948年錫蘭獨立成爲“锡兰自治領”。

<!-- end list -->

  - 1972年锡兰廢除[君主制](../Page/君主制.md "wikilink")，改稱“斯里蘭卡共和國”。“斯里蘭卡”名稱來自僧伽罗语（），讀音//。在斯里蘭卡的另一官方語言[泰米尔语中則稱爲](../Page/泰米尔语.md "wikilink")（ilaṅkai），讀音//。“斯里蘭卡”在僧伽羅語裡義爲“美好神聖的土地”，其中的“蘭卡”實際上就是佛經中的“楞伽”，《[楞伽經](../Page/楞伽經.md "wikilink")》即講述佛陀到達楞伽島教化說法，故以地名爲經名。

<!-- end list -->

  - 由於歷史原因，斯里蘭卡的衆多機構以及產品都仍以“錫蘭”為名。2011年斯里蘭卡政府宣佈將在所有政府可以控制的機構名稱中將“錫蘭”改爲“斯里蘭卡”。\[1\]

## 历史

公元前5世紀，[僧伽羅人從印度遷移到斯里蘭卡](../Page/僧伽羅人.md "wikilink")。

公元前247年，[印度](../Page/印度.md "wikilink")[孔雀王朝的](../Page/孔雀王朝.md "wikilink")[阿育王派其子来岛](../Page/阿育王.md "wikilink")，从此僧伽罗人摈弃[婆罗门教而改信](../Page/婆罗门教.md "wikilink")[佛教](../Page/佛教.md "wikilink")。

公元前2世纪前后，南印度的[泰米尔人开始迁入斯里兰卡](../Page/泰米尔人.md "wikilink")。

311年左右，佛牙從印度傳入斯里蘭卡。

从公元5世纪直至16世纪，[僧伽罗王国和](../Page/僧伽罗族.md "wikilink")[泰米尔王国间征战不断](../Page/泰米尔人.md "wikilink")，直至1521年[葡萄牙船队在](../Page/葡萄牙.md "wikilink")[科伦坡附近登陸](../Page/科伦坡.md "wikilink")。

1656年5月12日，[荷蘭軍隊攻克](../Page/荷蘭.md "wikilink")[科伦坡](../Page/科伦坡.md "wikilink")。

1796年2月15日，英軍占領[科伦坡](../Page/科伦坡.md "wikilink")，荷蘭人統治時期結束。

1802年英法兩國簽訂了[亞眠條約](../Page/亞眠條約.md "wikilink")，斯里蘭卡被正式宣布為[英國的殖民地](../Page/英國.md "wikilink")。

1948年2月4日，斯里蘭卡正式宣布从英国[獨立](../Page/獨立.md "wikilink")，成為[英聯邦的自治領](../Page/英聯邦.md "wikilink")，定國名為[錫蘭](../Page/錫蘭.md "wikilink")。

1972年5月22日，改國名為斯里蘭卡共和國。

1978年8月16日，新[憲法頒布](../Page/憲法.md "wikilink")，改國名為**斯里蘭卡民主社會主義共和國**。
[缩略图](https://zh.wikipedia.org/wiki/File:Buda_de_Avukana_-_01.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Sigiriya.jpg "fig:缩略图")世界遺產\]\]
[缩略图](https://zh.wikipedia.org/wiki/File:Nelum_Pokuna_\(Lotus_Pond\)_Mahinda_Rajapaksa_Theatre.JPG "fig:缩略图")\]\]
[缩略图](https://zh.wikipedia.org/wiki/File:Petah2.jpg "fig:缩略图")
[Wtccolombo.jpg](https://zh.wikipedia.org/wiki/File:Wtccolombo.jpg "fig:Wtccolombo.jpg")
[缩略图](https://zh.wikipedia.org/wiki/File:Sri_Lanka_-_029_-_Kandy_Temple_of_the_Tooth.jpg "fig:缩略图")\]\]
1830年代，英国将印度南部的[泰米尔人大批迁至斯里兰卡](../Page/泰米尔人.md "wikilink")，并扶持其占据了各個方面的主导地位，由此與当地的主要民族僧伽罗人结怨。斯里蘭卡獨立後，泰米尔人於1972年成立了[猛虎组织](../Page/猛虎组织.md "wikilink")（1976年改稱“泰米尔—伊拉姆猛虎解放组织”），走上“獨立建国”的道路。

1983年，[猛虎组织與斯里兰卡政府开战](../Page/猛虎组织.md "wikilink")，战火一度蔓延到科伦坡。

1987年，印度出兵協助斯政府清剿猛虎组织，迫使其签订停火协议。1990年印军撤离后，猛虎组织重新反攻並迅速控制北部广大地区，在[贾夫纳半岛建立起](../Page/贾夫纳半岛.md "wikilink")“泰米尔政权”，此後雙方戰爭不斷，造成6萬餘人喪生。

在[挪威等國斡旋下](../Page/挪威.md "wikilink")，2000年起雙方開始和談，2002年2月，猛虎组织与斯里兰卡政府在斯德哥尔摩签署了一份永久性的停火协议，斯里兰卡政府也于当年9月4日解除了对猛虎组织的禁令，使其成为合法组织。同年9月16日至18日，斯里兰卡政府和猛虎组织在[泰国东南部春武里府的梭桃邑海军基地举行了首次和谈](../Page/泰国.md "wikilink")。6年来，尽管双方先后进行了8轮直接谈判，但他们之间的武装冲突一直不断，停火协议已名存实亡。

从2006年7月开始，政府军开始向猛虎组织控制区发动大规模军事进攻，在两年多的时间里收复了约1.5万平方公里的猛虎组织控制区。2008年3月，斯里兰卡政府指控猛虎组织领导人[普拉巴卡兰犯有谋杀罪](../Page/普拉巴卡兰.md "wikilink")。

2009年1月2日，斯里兰卡总统拉贾帕克萨宣布，政府军当天攻占了反政府武装猛虎组织的大本营基利诺奇。1月7日，斯里兰卡政府决定重新禁止猛虎组织活动，这表明在政府军不断取得军事胜利的情况下，斯里兰卡政府已不再把猛虎组织作为谈判对手。1月25日，政府军攻入猛虎组织控制的最后一个主要城镇——东北部的穆莱蒂武。2月5日，政府军攻克猛虎组织最后一个海上武装基地。4月初，政府军攻占了猛虎组织在北部地区的最后一个主要据点普杜库迪伊鲁普，部分猛虎组织成员转移到位于斯里兰卡北部穆莱蒂武地区的约20平方公里的“安全区”内。4月20日，斯里兰卡政府军攻入猛虎组织最后据守的“安全区”。26日，斯里兰卡政府拒绝了猛虎组织当天的单方面停火声明。5月15日斯里兰卡总统马欣达·拉贾帕克萨已宣布，将在48小时内解救出猛虎组织控制区内的所有被困平民，并将收复所有被该组织控制的领土。5月16日斯里兰卡政府军收复斯里兰卡反政府武装泰米尔伊拉姆猛虎解放组织（猛虎组织）控制的最后一段海岸线。同日马欣达·拉贾帕克萨总统在约旦访问时说，斯里兰卡政府军已经击败泰米尔伊拉姆猛虎解放组织（猛虎组织）。5月17日猛虎组织承认与政府军长达25年的战争失败，宣布放下武器，结束与政府军的战斗。斯里兰卡政府方面则表示无法相信猛虎组织“放下武器”的声明，政府军将继续进攻猛虎组织控制的最后一片丛林，以收复“每一寸国土”。5月18日，斯里兰卡政府军在穆莱蒂武区击毙反政府武装泰米尔伊拉姆猛虎解放组织最高领导人普拉巴卡兰后，宣布[斯里兰卡内战结束](../Page/斯里兰卡内战.md "wikilink")。

## 地理

Sri Lanka-CIA WFB Map.png|斯里蘭卡地圖 Sri Lanka provinces.png|斯里蘭卡省分圖 Sri
Lanka Districts.png|斯里蘭卡分區圖

斯里蘭卡島是梨形，又与眼泪的形状相似，因此有“印度洋上的一滴眼泪”的雅称。整个岛屿位于[印度洋之中](../Page/印度洋.md "wikilink")，東北邊是[孟加拉灣](../Page/孟加拉灣.md "wikilink")；中部、南部是高原，多山地，北部和沿海是平原；有[亚当峰](../Page/亚当峰.md "wikilink")。

斯里兰卡北部属[热带草原气候](../Page/热带草原气候.md "wikilink")，南部属[热带雨林气候](../Page/热带雨林气候.md "wikilink")，全年炎热；西部年降雨量2000－3000毫米，东北部较干燥，年降雨量约1000毫米。

## 政治

斯里蘭卡的[總統是](../Page/斯里蘭卡總統.md "wikilink")[國家元首](../Page/國家元首.md "wikilink")。總統任期6年。

斯里蘭卡的[一院制議會有](../Page/一院制.md "wikilink")225人，普選，任期6年。

獨立之後的斯里蘭卡仍然是[大英國協成员](../Page/大英國協.md "wikilink")。

## 行政區

斯里蘭卡劃分為9個省和25个行政区：

<table>
<tbody>
<tr class="odd">
<td><p><strong>省（පලාත）/區（දිස්ත්‍රි‌ක්‌ක）名稱</strong></p></td>
<td><p><strong>COK</strong></p></td>
<td style="text-align: right;"><p><strong>人口</strong></p></td>
<td><p>面積k㎡</p></td>
<td style="text-align: left;"><p><strong>省會和主要城市</strong></p></td>
<td style="text-align: right;"><p><strong>人口</strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/西部省_(斯里蘭卡).md" title="wikilink">西部省Western</a></strong></p></td>
<td><p>1</p></td>
<td style="text-align: right;"><p>5,361,185</p></td>
<td><p>3,684</p></td>
<td style="text-align: left;"><p><strong><a href="../Page/科伦坡.md" title="wikilink">科伦坡<span lang="zh-tw">Colombo</a></strong></p></td>
<td style="text-align: right;"><p>　</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/可倫坡區.md" title="wikilink">科伦坡<span lang="zh-tw"></a> Colombo</p></td>
<td><p>11</p></td>
<td style="text-align: right;"><p>2,234,289</p></td>
<td><p>699</p></td>
<td style="text-align: left;"><p><a href="../Page/科伦坡.md" title="wikilink">科伦坡<span lang="zh-tw"></a> Colombo<br />
<a href="../Page/芒特拉維尼亞.md" title="wikilink">芒特拉維尼亞Mount</a> Lavinia<br />
<a href="../Page/莫勒圖沃.md" title="wikilink">莫勒圖沃Moratuwa</a><br />
<a href="../Page/科特.md" title="wikilink">科特Kotte</a></p></td>
<td style="text-align: right;"><p>642,163<br />
209,787<br />
177,190<br />
115,826</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/加姆珀哈區.md" title="wikilink">加姆珀哈區Gampaha</a></p></td>
<td><p>12</p></td>
<td style="text-align: right;"><p>2,066,096</p></td>
<td><p>1,387</p></td>
<td style="text-align: left;"><p><a href="../Page/加姆珀哈.md" title="wikilink">加姆珀哈Gampaha</a><br />
<a href="../Page/尼甘布.md" title="wikilink">尼甘布Negombo</a></p></td>
<td style="text-align: right;"><p>9,438<br />
121,933</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/卡盧特勒區.md" title="wikilink">卡盧特勒區Kalutara</a></p></td>
<td><p>13</p></td>
<td style="text-align: right;"><p>1,060,800</p></td>
<td><p>1,598</p></td>
<td style="text-align: left;"><p><a href="../Page/卡盧特勒.md" title="wikilink">卡盧特勒Kalutara</a></p></td>
<td style="text-align: right;"><p>37,081</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/中央省_(斯里蘭卡).md" title="wikilink">中央省Central</a></strong></p></td>
<td><p>2</p></td>
<td style="text-align: right;"><p>2,414,973</p></td>
<td><p>5,674</p></td>
<td style="text-align: left;"><p><strong><a href="../Page/康提.md" title="wikilink">康提Kandy</a></strong>　</p></td>
<td style="text-align: right;"><p>　</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/康提區.md" title="wikilink">康提區Kandy</a></p></td>
<td><p>21</p></td>
<td style="text-align: right;"><p>1,272,463</p></td>
<td><p>1,940</p></td>
<td style="text-align: left;"><p><a href="../Page/康提.md" title="wikilink">康提Kandy</a></p></td>
<td style="text-align: right;"><p>110,049</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/馬特萊區.md" title="wikilink">馬特萊區Matale</a></p></td>
<td><p>22</p></td>
<td style="text-align: right;"><p>442,427</p></td>
<td><p>1,993</p></td>
<td style="text-align: left;"><p><a href="../Page/馬特萊.md" title="wikilink">馬特萊Matale</a></p></td>
<td style="text-align: right;"><p>36,352</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/努沃勒埃利耶區.md" title="wikilink">努沃勒埃利耶區Nuwara</a> Eliya</p></td>
<td><p>23</p></td>
<td style="text-align: right;"><p>700,083</p></td>
<td><p>1,741</p></td>
<td style="text-align: left;"><p><a href="../Page/努沃勒埃利耶.md" title="wikilink">努沃勒埃利耶Nuwara</a> Eliya</p></td>
<td style="text-align: right;"><p>25,049</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/南部省_(斯里蘭卡).md" title="wikilink">南方省Southern</a></strong></p></td>
<td><p>3</p></td>
<td style="text-align: right;"><p>2,277,145</p></td>
<td><p>5,444</p></td>
<td style="text-align: left;"><p><strong><a href="../Page/加勒.md" title="wikilink">加勒Galle</a></strong></p></td>
<td style="text-align: right;"><p>　</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/加勒區.md" title="wikilink">加勒區Galle</a></p></td>
<td><p>31</p></td>
<td style="text-align: right;"><p>990,539</p></td>
<td><p>1,652</p></td>
<td style="text-align: left;"><p><a href="../Page/加勒.md" title="wikilink">加勒Galle</a></p></td>
<td style="text-align: right;"><p>90,934</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/馬特勒區.md" title="wikilink">馬特勒區Matara</a></p></td>
<td><p>32</p></td>
<td style="text-align: right;"><p>761,236</p></td>
<td><p>1,283</p></td>
<td style="text-align: left;"><p><a href="../Page/馬特勒.md" title="wikilink">馬特勒Matara</a></p></td>
<td style="text-align: right;"><p>42,756</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/漢班托特區.md" title="wikilink">漢班托特區Hambantota</a></p></td>
<td><p>33</p></td>
<td style="text-align: right;"><p>525,370</p></td>
<td><p>2,609</p></td>
<td style="text-align: left;"><p><a href="../Page/漢班托特.md" title="wikilink">漢班托特Hambantota</a></p></td>
<td style="text-align: right;"><p>11,213</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/北部省_(斯里蘭卡).md" title="wikilink">北方省Northern</a></strong></p></td>
<td><p>4</p></td>
<td style="text-align: right;"><p>2,277,145</p></td>
<td><p>5,444</p></td>
<td style="text-align: left;"><p><strong><a href="../Page/賈夫納.md" title="wikilink">賈夫納Jaffna</a></strong></p></td>
<td style="text-align: right;"><p>　</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/賈夫納區.md" title="wikilink">賈夫納區Jaffna</a></p></td>
<td><p>41</p></td>
<td style="text-align: right;"><p>490,621</p></td>
<td><p>1,025</p></td>
<td style="text-align: left;"><p><a href="../Page/賈夫納.md" title="wikilink">賈夫納Jaffna</a></p></td>
<td style="text-align: right;"><p>118,224</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/基利諾奇區.md" title="wikilink">基利諾奇區Kilinochchi</a></p></td>
<td><p>42</p></td>
<td style="text-align: right;"><p>127,263</p></td>
<td><p>1,279</p></td>
<td style="text-align: left;"><p><a href="../Page/基裡諾奇.md" title="wikilink">基裡諾奇Kilinochchi</a></p></td>
<td style="text-align: right;"><p>　</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/馬納爾區.md" title="wikilink">馬納爾區Mannar</a></p></td>
<td><p>43</p></td>
<td style="text-align: right;"><p>151,577</p></td>
<td><p>1,996</p></td>
<td style="text-align: left;"><p><a href="../Page/馬納爾.md" title="wikilink">馬納爾Mannar</a></p></td>
<td style="text-align: right;"><p>　</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/瓦武尼亞區.md" title="wikilink">瓦武尼亞區Vavuniya</a></p></td>
<td><p>44</p></td>
<td style="text-align: right;"><p>149,835</p></td>
<td><p>1,967</p></td>
<td style="text-align: left;"><p><a href="../Page/瓦武尼亞.md" title="wikilink">瓦武尼亞Vavuniya</a></p></td>
<td style="text-align: right;"><p><em>53,237</em></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/穆萊蒂武區.md" title="wikilink">穆萊蒂武區Mullathivu</a></p></td>
<td><p>45</p></td>
<td style="text-align: right;"><p>121,667</p></td>
<td><p>2,617</p></td>
<td style="text-align: left;"><p><a href="../Page/穆萊蒂武.md" title="wikilink">穆萊蒂武Mullathivu</a></p></td>
<td style="text-align: right;"><p>　</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/東部省_(斯里蘭卡).md" title="wikilink">東方省Eastern</a></strong></p></td>
<td><p>5</p></td>
<td style="text-align: right;"><p>1,415,949</p></td>
<td><p>10,472</p></td>
<td style="text-align: left;"><p>　 <strong><a href="../Page/拜蒂克洛.md" title="wikilink">拜蒂克洛Batticaloa</a></strong></p></td>
<td style="text-align: right;"><p>　</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/拜蒂克洛區.md" title="wikilink">拜蒂克洛區Batticaloa</a></p></td>
<td><p>51</p></td>
<td style="text-align: right;"><p>486,447</p></td>
<td><p>2,854</p></td>
<td style="text-align: left;"><p><a href="../Page/拜蒂克洛.md" title="wikilink">拜蒂克洛Batticaloa</a></p></td>
<td style="text-align: right;"><p>78,480</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/安帕賴區.md" title="wikilink">安帕賴區Ampara</a></p></td>
<td><p>52</p></td>
<td style="text-align: right;"><p>589,344</p></td>
<td><p>4,415</p></td>
<td style="text-align: left;"><p><a href="../Page/安帕賴.md" title="wikilink">安帕賴Ampara</a></p></td>
<td style="text-align: right;"><p>17,965</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/亭可馬里區.md" title="wikilink">亭可馬里區Trincomalee</a></p></td>
<td><p>53</p></td>
<td style="text-align: right;"><p>340,158</p></td>
<td><p>2,727</p></td>
<td style="text-align: left;"><p><a href="../Page/亭可馬里.md" title="wikilink">亭可馬里Trincomalee</a></p></td>
<td style="text-align: right;"><p>44,313</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/西北省_(斯里蘭卡).md" title="wikilink">西北省North</a> Western</strong></p></td>
<td><p>6</p></td>
<td style="text-align: right;"><p>2,157,711</p></td>
<td><p>7,888</p></td>
<td style="text-align: left;"><p><strong><a href="../Page/庫魯內格勒.md" title="wikilink">庫魯內格勒Kurunegala</a></strong></p></td>
<td style="text-align: right;"><p>　</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/庫魯內格勒區.md" title="wikilink">庫魯內格勒區Kurunegala</a></p></td>
<td><p>61</p></td>
<td style="text-align: right;"><p>1,452,369</p></td>
<td><p>4,816</p></td>
<td style="text-align: left;"><p><a href="../Page/庫魯內格勒.md" title="wikilink">庫魯內格勒Kurunegala</a></p></td>
<td style="text-align: right;"><p>28,337</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/普塔勒姆區.md" title="wikilink">普塔勒姆區Puttalam</a></p></td>
<td><p>62</p></td>
<td style="text-align: right;"><p>705,342</p></td>
<td><p>3,072</p></td>
<td style="text-align: left;"><p><a href="../Page/普塔勒姆.md" title="wikilink">普塔勒姆Puttalam</a></p></td>
<td style="text-align: right;"><p>40,967</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/北中省_(斯里蘭卡).md" title="wikilink">北中省North</a> Central</strong></p></td>
<td><p>7</p></td>
<td style="text-align: right;"><p>1,415,949</p></td>
<td><p>10,472</p></td>
<td style="text-align: left;"><p><strong><a href="../Page/阿努拉德普勒.md" title="wikilink">阿努拉德普勒Anuradhapura</a></strong></p></td>
<td style="text-align: right;"><p>　</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿努拉德普勒區.md" title="wikilink">阿努拉德普勒區Anuradhapura</a></p></td>
<td><p>71</p></td>
<td style="text-align: right;"><p>746,466</p></td>
<td><p>7,179</p></td>
<td style="text-align: left;"><p><a href="../Page/阿努拉德普勒.md" title="wikilink">阿努拉德普勒Anuradhapura</a></p></td>
<td style="text-align: right;"><p>56,632</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/波隆纳鲁瓦区.md" title="wikilink">波隆纳鲁瓦区Polonnaruwa</a></p></td>
<td><p>72</p></td>
<td style="text-align: right;"><p>359,197</p></td>
<td><p>3,293</p></td>
<td style="text-align: left;"><p><a href="../Page/波隆纳鲁瓦.md" title="wikilink">波隆纳鲁瓦Polonnaruwa</a></p></td>
<td style="text-align: right;"><p>　</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/烏沃省.md" title="wikilink">烏沃省Uva</a></strong></p></td>
<td><p>8</p></td>
<td style="text-align: right;"><p>1,170,728</p></td>
<td><p>8,500</p></td>
<td style="text-align: left;"><p><strong><a href="../Page/巴杜勒.md" title="wikilink">巴杜勒Badulla</a></strong></p></td>
<td style="text-align: right;"><p>　</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/巴杜勒區.md" title="wikilink">巴杜勒區Badulla</a></p></td>
<td><p>81</p></td>
<td style="text-align: right;"><p>774,555</p></td>
<td><p>2,861</p></td>
<td style="text-align: left;"><p><a href="../Page/巴杜勒.md" title="wikilink">巴杜勒Badulla</a></p></td>
<td style="text-align: right;"><p>40,920</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/莫訥勒格勒區.md" title="wikilink">莫訥勒格勒區Monaragala</a></p></td>
<td><p>82</p></td>
<td style="text-align: right;"><p>396,173</p></td>
<td><p>5,639</p></td>
<td style="text-align: left;"><p><a href="../Page/莫訥勒格勒.md" title="wikilink">莫訥勒格勒Monaragala</a></p></td>
<td style="text-align: right;"><p>　</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/薩伯勒格穆沃省.md" title="wikilink">薩伯勒格穆沃省Sabaragamuwa</a></strong></p></td>
<td><p>9</p></td>
<td style="text-align: right;"><p>1,787,938</p></td>
<td><p>4,968</p></td>
<td style="text-align: left;"><p><strong><a href="../Page/拉特納普勒.md" title="wikilink">拉特納普勒Ratnapura</a></strong></p></td>
<td style="text-align: right;"><p>　</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/拉特納普勒區.md" title="wikilink">拉特納普勒區Ratnapura</a></p></td>
<td><p>91</p></td>
<td style="text-align: right;"><p>1,008,164</p></td>
<td><p>3,275</p></td>
<td style="text-align: left;"><p><a href="../Page/拉特納普勒.md" title="wikilink">拉特納普勒Ratnapura</a></p></td>
<td style="text-align: right;"><p>46,309</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/凱格勒區.md" title="wikilink">凱格勒區Kegalle</a></p></td>
<td><p>92</p></td>
<td style="text-align: right;"><p>779,774</p></td>
<td><p>1,693</p></td>
<td style="text-align: left;"><p><a href="../Page/凱格勒.md" title="wikilink">凱格勒Kegalle</a></p></td>
<td style="text-align: right;"><p>17,430</p></td>
</tr>
</tbody>
</table>

## 經濟

[缩略图](https://zh.wikipedia.org/wiki/File:Sri_Lanka_Military_0274.jpg "fig:缩略图")戰機\]\]
斯里蘭卡的經濟以寶石出口和[農業為主](../Page/農業.md "wikilink")，主要農產出口如[稻米](../Page/稻米.md "wikilink")、[橡膠](../Page/橡膠.md "wikilink")、[椰子](../Page/椰子.md "wikilink")、[咖啡等許多](../Page/咖啡.md "wikilink")[熱帶地區代表性的](../Page/熱帶.md "wikilink")[經濟作物](../Page/經濟作物.md "wikilink")；而該國最重要的出口產品是[錫蘭紅茶](../Page/錫蘭紅茶.md "wikilink")，斯里蘭卡是世界三大產茶國之一，也因此國內經濟深受產茶情況的影響。斯里蘭卡的觀光資源也相當豐富，但自[2004年印度洋大地震所引起的](../Page/2004年印度洋大地震.md "wikilink")[大海嘯以來](../Page/大海嘯.md "wikilink")，該國的海岸線被嚴重破壞，觀光業也因此受到了一定程度的影響。

出於戰略考量[中華人民共和國和斯里蘭卡長期有多種合作關係密切](../Page/中華人民共和國.md "wikilink")，中國企業投資建造的科倫坡南港國際集裝箱(貨櫃)碼頭於2013年啟用，更早的2008年斯里蘭卡就在距離首都科倫坡50公里處為中國投資者建立了一個佔地150公頃的經濟特區，中國企業在此可享受許多優惠，也開始洽談[自由貿易協定](../Page/自由貿易協定.md "wikilink")。南部城市漢班托塔的工業園區也幾乎交由中國大陸的國企和民企來開發基礎建設，並開發酒店度假村。

## 人口及宗教

根據1981年的統計，斯里蘭卡人口約14,850,001人，平均每平方公里有226.3人，主要的有兩個民族[僧伽羅族約佔](../Page/僧伽羅.md "wikilink")73.8%、[泰米爾族佔](../Page/泰米爾.md "wikilink")18%（其中[印度移民佔](../Page/印度.md "wikilink")5.1%）、[摩爾人佔](../Page/摩爾人.md "wikilink")8.3%。77%屬於[上座部佛教徒](../Page/上座部佛教.md "wikilink")、15%屬於[印度教徒](../Page/印度教.md "wikilink")、7.5%屬於[穆斯林及](../Page/穆斯林.md "wikilink")[基督徒](../Page/基督徒.md "wikilink")。

| [佛教](../Page/佛教.md "wikilink")                                                                                     | [印度教](../Page/印度教.md "wikilink")                                                                                   | [伊斯蘭教](../Page/伊斯蘭教.md "wikilink")                                                                        | [基督教](../Page/基督教.md "wikilink")                                                                                         |
| ------------------------------------------------------------------------------------------------------------------ | ------------------------------------------------------------------------------------------------------------------ | --------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------ |
| [Sri_Lanka_Buddhism.svg](https://zh.wikipedia.org/wiki/File:Sri_Lanka_Buddhism.svg "fig:Sri_Lanka_Buddhism.svg") | [Sri_Lanka_Hinduism.svg](https://zh.wikipedia.org/wiki/File:Sri_Lanka_Hinduism.svg "fig:Sri_Lanka_Hinduism.svg") | [Sri_Lanka_Islam.svg](https://zh.wikipedia.org/wiki/File:Sri_Lanka_Islam.svg "fig:Sri_Lanka_Islam.svg") | [Sri_Lanka_Christians.svg](https://zh.wikipedia.org/wiki/File:Sri_Lanka_Christians.svg "fig:Sri_Lanka_Christians.svg") |
| 主要宗教信仰。普通字体的百分比来源于2010年人口普查；斜体字的百分比来源于1981年人口普查，这些地区在1981年后发生的人口流动尚无精确统计。\[2\]                                     |                                                                                                                    |                                                                                                           |                                                                                                                          |

| [僧伽羅族](../Page/僧伽罗.md "wikilink")                                                                                     | [泰米爾](../Page/泰米爾.md "wikilink")[原住民](../Page/原住民.md "wikilink")                                                                | [斯里兰卡摩爾人](../Page/斯里兰卡摩爾人.md "wikilink")                                                               | [印度](../Page/印度.md "wikilink")[泰米爾](../Page/泰米爾.md "wikilink")[移民](../Page/移民.md "wikilink")                                    |
| --------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------ | ------------------------------------------------------------------------------------------------------------------------------- |
| [Sri_Lanka_Sinhalese.svg](https://zh.wikipedia.org/wiki/File:Sri_Lanka_Sinhalese.svg "fig:Sri_Lanka_Sinhalese.svg") | [Sri_Lanka_Native_Tamil.svg](https://zh.wikipedia.org/wiki/File:Sri_Lanka_Native_Tamil.svg "fig:Sri_Lanka_Native_Tamil.svg") | [Sri_Lanka_Moor.svg](https://zh.wikipedia.org/wiki/File:Sri_Lanka_Moor.svg "fig:Sri_Lanka_Moor.svg") | [Sri_Lanka_Indian_Tamil.svg](https://zh.wikipedia.org/wiki/File:Sri_Lanka_Indian_Tamil.svg "fig:Sri_Lanka_Indian_Tamil.svg") |
| 斯里兰卡的主要民族构成。百分比来源于2001年或1981年（斜体）人口普查。\[3\]                                                                           |                                                                                                                                 |                                                                                                        |                                                                                                                                 |

## 文化

[世界文化遺產](../Page/世界文化遺產.md "wikilink")：

  - [阿怒拉德普勒聖城](../Page/阿怒拉德普勒聖城.md "wikilink")
  - [加勒古城及其堡壘](../Page/加勒古城及其堡壘.md "wikilink")
  - [丹布勒金寺](../Page/丹布勒金寺.md "wikilink")
  - [聖城康提](../Page/聖城康提.md "wikilink")
  - [波隆纳鲁瓦古城](../Page/波隆纳鲁瓦古城.md "wikilink")
  - [錫吉里耶古城](../Page/錫吉里耶古城.md "wikilink")

## 外部链接

  - [GOI目錄](https://web.archive.org/web/20150810070644/http://www.priu.gov.lk/)
    - 政府網站目錄

  - [斯里蘭卡議會](http://www.parliament.lk) - 議會官方網站

  - [維客旅行上的](../Page/維客旅行.md "wikilink")[斯里蘭卡](http://wikitravel.org/en/Sri_Lanka)

  -
  -
  - \[//maps.google.com/maps?q=斯里蘭卡 谷歌地圖\]

## 参考文献

{{-}}

[斯里蘭卡](../Category/斯里蘭卡.md "wikilink")
[Category:亞洲岛国](../Category/亞洲岛国.md "wikilink")
[Category:南亚国家](../Category/南亚国家.md "wikilink")
[Category:印度洋島嶼](../Category/印度洋島嶼.md "wikilink")
[Category:印度洋國家](../Category/印度洋國家.md "wikilink")
[Category:泰米爾語國家地區](../Category/泰米爾語國家地區.md "wikilink")
[Category:前英國殖民地](../Category/前英國殖民地.md "wikilink")
[Category:1948年建立的國家或政權](../Category/1948年建立的國家或政權.md "wikilink")
[Category:社会主义国家](../Category/社会主义国家.md "wikilink")

1.  [斯里蘭卡除舊名「錫蘭」　紅茶例外](http://www.nownews.com/2011/01/02/91-2678719.htm)，今日傳媒BBC，2011-1-2
2.  [Department of Census and Statistics](http://www.statistics.gov.lk/)
3.  [Department of Census and Statistics](http://www.statistics.gov.lk/)