**李時勉**（），名懋，字時勉，[以字行](../Page/以字行.md "wikilink")，號古廉。[江西](../Page/江西.md "wikilink")[吉安府](../Page/吉安府.md "wikilink")[安福县人](../Page/安福县.md "wikilink")。明朝政治人物。

## 生平

[永樂二年](../Page/永乐_\(明朝\).md "wikilink")（1404年）進士。預修《[太祖實錄](../Page/太祖實錄.md "wikilink")》，書成後，升翰林侍讀。永樂十九年，上書反對[永樂遷都](../Page/永樂遷都.md "wikilink")。

[洪熙元年](../Page/洪熙.md "wikilink")（1425年），上書勸仁宗“所謂節民力者此也”，“所謂謹嗜欲者此也”，“所謂勤政事者此也”，時[明仁宗生病](../Page/明仁宗.md "wikilink")，被其激怒，令武士對時勉擊打以金瓜，[肋骨折斷三根](../Page/肋骨.md "wikilink")，並將他拘捕入獄。但是仁宗氣極敗壞，數日後病崩。\[1\]

[宣德初年](../Page/宣德.md "wikilink")，復官，官至[國子監](../Page/國子監.md "wikilink")[祭酒](../Page/祭酒.md "wikilink")，禁《[剪燈新話](../Page/剪燈新話.md "wikilink")》。[景泰元年](../Page/景泰.md "wikilink")（1450年）卒，諡文毅，成化五年（1469年）改諡忠文，追贈禮部侍郎。著有《[古廉集](../Page/古廉集.md "wikilink")》。\[2\]\[3\]

## 参考文献

<div class="references-small">

<references>

</references>

</div>

[Category:明朝翰林](../Category/明朝翰林.md "wikilink")
[Category:明朝國子監祭酒](../Category/明朝國子監祭酒.md "wikilink")
[Category:安福人](../Category/安福人.md "wikilink")
[S](../Category/李姓.md "wikilink")
[Category:諡忠文](../Category/諡忠文.md "wikilink")
[Category:諡文毅](../Category/諡文毅.md "wikilink")

1.  《明史·李时勉传》：“仁宗大渐，谓夏原吉曰：‘时勉廷辱我。’言已，勃然怒，原吉慰解之。其夕，帝崩。”《夏忠靖公遺事》謂:“五月，李時勉廷諍過激，上怒，欲刑之。晚，諭公等曰:李時勉當朝辱朕。諭巳，天顏大變。公進曰:時勉小臣之言，豈能傷損聖德，願陛下少霽天威，下法司議定罪之未晚也。從之。”
2.
3.  《[明史](../Page/明史.md "wikilink")》列传第五十一