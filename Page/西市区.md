**西市区**是[中国](../Page/中国.md "wikilink")[营口市所辖的一个](../Page/营口市.md "wikilink")[市辖区](../Page/市辖区.md "wikilink")，是营口城市发展的起步区。

## 行政区划

西市区辖8（-1）个街道：五台子街道 渔市街道 得胜街道 清华街道 河北街道 胜利街道 滨海街道、沿海街道。 沿海街道办事处交由沿海新区代管。

## 外部链接

[营口市西市区政府网站](http://www.ykxs.gov.cn/)

[西市区](../Category/西市区.md "wikilink") [区](../Category/营口区市.md "wikilink")
[营口](../Category/辽宁市辖区.md "wikilink")