在[信息论中](../Page/信息论.md "wikilink")，两个等长[字符串之间的](../Page/字符串.md "wikilink")**汉明距离**（）是两个字符串对应位置的不同字符的个数。换句话说，它就是将一个字符串变换成另外一个字符串所需要*替换*的字符个数。

**[汉明重量](../Page/汉明重量.md "wikilink")**是字符串相对于同样长度的零字符串的汉明距离，也就是说，它是字符串中非零的元素个数：对于[二进制](../Page/二进制.md "wikilink")[字符串来说](../Page/字符串.md "wikilink")，就是1的个数，所以11101的汉明重量是4。

## 範例

例如：

  - **10<font color=blue>1</font>1<font color=blue>1</font>01**与**10<font color=red>0</font>1<font color=red>0</font>01**之间的汉明距离是2。
  - **2<font color=blue>14</font>3<font color=blue>8</font>96**与**2<font color=red>23</font>3<font color=red>7</font>96**之间的汉明距离是3。
  - "**<font color=blue>t</font>o<font color=blue>n</font>e<font color=blue>d</font>**"与"**<font color=red>r</font>o<font color=red>s</font>e<font color=red>s</font>**"之间的汉明距离是3。

## 特性

对于固定的长度*n*，汉明距离是该长度字符向量空间上的[度量](../Page/度量.md "wikilink")，很显然它满足非负、唯一及对称性，并且可以很容易地通过[完全归纳法证明它满足](../Page/数学归纳法.md "wikilink")[三角不等式](../Page/三角不等式.md "wikilink")。

两个字*a*与*b*之间的汉明距离也可以看作是特定运算−的*a*−*b*的汉明重量。

对于二进制字符串*a*与*b*来说，它等于*a*
[异或](../Page/异或.md "wikilink")*b*以后所得二进制字符串中“1”的个数。另外二进制字符串的汉明距离也等于*n*维[超正方体两个顶点之间的](../Page/超正方体.md "wikilink")[曼哈顿距离](../Page/曼哈顿距离.md "wikilink")，其中*n*是两个字串的长度。

## 历史及应用

汉明距离是以[理查德·衛斯里·漢明的名字命名的](../Page/理查德·衛斯里·漢明.md "wikilink")，汉明在*误差检测与校正码*的基础性论文中首次引入这个概念。在[通信中累计定长二进制字中发生翻转的错误数据位](../Page/通信.md "wikilink")，所以它也被称为**信号距离**。汉明重量分析在包括[信息论](../Page/信息论.md "wikilink")、[编码理论](../Page/编码理论.md "wikilink")、[密码学等领域都有应用](../Page/密码学.md "wikilink")。但是，如果要比较两个不同长度的字符串，不仅要进行替换，而且要进行插入与删除的运算，在这种场合下，通常使用更加复杂的[編輯距離等算法](../Page/編輯距離.md "wikilink")。

## 参考文献

*部分摘自[Federal Standard
1037C](../Page/Federal_Standard_1037C.md "wikilink").*

[理查德·衛斯里·漢明](../Page/理查德·衛斯里·漢明.md "wikilink")，误差检测与[纠错码](../Page/纠错码.md "wikilink")（Error-detecting
and [error-correcting
codes](../Page/error-correcting_code.md "wikilink")）, Bell System
Technical Journal 29 (2):147-160, 1950.

## 参见

  - [汉明重量](../Page/汉明重量.md "wikilink")
  - [杰卡德指数](../Page/杰卡德指数.md "wikilink")
  - [編輯距離](../Page/編輯距離.md "wikilink")，一般化的汉明距离
  - [相似](../Page/相似.md "wikilink")
  - [数值分类学中的](../Page/数值分类学.md "wikilink")[相似空间](../Page/相似空间.md "wikilink")

[Category:字符串相似性度量](../Category/字符串相似性度量.md "wikilink")
[Category:编码理论](../Category/编码理论.md "wikilink")