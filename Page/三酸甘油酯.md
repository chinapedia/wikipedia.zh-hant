[Fat_triglyceride_shorthand_formula.svg](https://zh.wikipedia.org/wiki/File:Fat_triglyceride_shorthand_formula.svg "fig:Fat_triglyceride_shorthand_formula.svg"),
右側部分從上到下：[棕櫚酸](../Page/棕櫚酸.md "wikilink")，[油酸](../Page/油酸.md "wikilink")，[α-亞麻酸](../Page/α-亞麻酸.md "wikilink")。化學式:
C<sub>55</sub>H<sub>98</sub>O<sub>6</sub>\]\]

**-{zh-hans:甘油三酸酯;zh-hk:甘油三酸酯;zh-sg:三酸甘油酯;zh-tw:三酸甘油酯;}-**（triglyceride,
TG, triacylglycerol, TAG, or
triacylglyceride），亦作**-{zh-hans:三酸甘油酯;zh-hk:三酸甘油酯;zh-sg:甘油三酸酯;zh-tw:甘油三酸酯;}-**，常稱為**油脂**，為動物性油脂與[植物性油脂的主要成分](../Page/植物性油脂.md "wikilink")，一種由一個[甘油分子和三個](../Page/甘油.md "wikilink")[脂肪酸分子組成的](../Page/脂肪酸.md "wikilink")[酯類有機化合物](../Page/酯.md "wikilink")，可以透過日常飲食攝取。

熔點則取決於其[脂肪酸部分的種類](../Page/脂肪酸.md "wikilink")，由碳數較多的[飽和脂肪酸所形成的](../Page/飽和脂肪酸.md "wikilink")-{zh-hans:甘油三酸酯;zh-hk:甘油三酸酯;zh-sg:三酸甘油酯;zh-tw:三酸甘油酯;}-在[常溫下多為固體](../Page/常溫.md "wikilink")（如[牛油](../Page/牛油.md "wikilink")、[豬油](../Page/豬油.md "wikilink")），即稱為[脂肪](../Page/脂肪.md "wikilink")（fat）。由碳數較少的飽和脂肪酸（[椰子油](../Page/椰子油.md "wikilink")）或[雙鍵的](../Page/雙鍵.md "wikilink")[不飽和脂肪酸](../Page/不飽和脂肪酸.md "wikilink")（[花生油](../Page/花生油.md "wikilink")）所形成的-{zh-hans:甘油三酸酯;zh-hk:甘油三酸酯;zh-sg:三酸甘油酯;zh-tw:三酸甘油酯;}-在常溫下多為液體，即稱為[油](../Page/油.md "wikilink")（oils）。市上販售的固態[植物奶油是將植物油加氫成為飽和脂肪酸後加上牛奶與人工色素而得](../Page/植物奶油.md "wikilink")。

## 化學結構

三酸甘油酯的化学式是**CH<sub>2</sub>**COOR-**CH**COOR'-**CH**<sub>2</sub>COOR"，其中R、R'、R"為[烷基](../Page/烷基.md "wikilink")（alkyl）長鏈。三個脂肪酸RCOOH、R'COOH、R"COOH可能為相同、相異或部份相異的烷基。

自然界裡的三酸甘油酯的链长差异很大，但是16个、18个和20个[碳原子的链最常见](../Page/碳.md "wikilink")。自然界裡常见的植物和动物三酸甘油酯的长链只有双数碳原子的，其原因来自[乙酰辅酶A生物合成的过程](../Page/乙酰辅酶A.md "wikilink")。[细菌也能够产生单数碳原子的或者分叉的链](../Page/细菌.md "wikilink")。由于[反刍动物在](../Page/反刍动物.md "wikilink")[反刍过程中有](../Page/反刍.md "wikilink")[细菌介入](../Page/细菌.md "wikilink")，因此它们的三酸甘油酯也包含单数碳原子（比如15个碳原子）的链。

天然的[脂肪包含许多不同的三酸甘油酯](../Page/脂肪.md "wikilink")，因此它们没有固定的熔点，而是有一个非常宽的熔化温度带。[可可脂是少数的仅有数种三酸甘油酯的脂肪](../Page/可可脂.md "wikilink")，包含[棕櫚酸](../Page/棕櫚酸.md "wikilink")、[油酸和](../Page/油酸.md "wikilink")[硬脂酸](../Page/硬脂酸.md "wikilink")。因此可可脂的熔点比较固定，导致[巧克力在嘴里熔化时没有颗粒的感觉](../Page/巧克力.md "wikilink")。

在[细胞裡](../Page/细胞.md "wikilink")，三酸甘油酯可以自由穿过[细胞膜](../Page/细胞膜.md "wikilink")，原因是其[无极性](../Page/极性.md "wikilink")，与组成细胞膜的类脂双层不产生反应。

## 新陈代谢

三酸甘油酯是极低密度脂蛋白和乳糜微粒的主要组成部分，在[新陈代谢过程中它作为能源和食物中的脂肪的运输工具起了一个重要的作用](../Page/新陈代谢.md "wikilink")。其能量密度为[糖类和](../Page/糖类.md "wikilink")[蛋白质的两倍](../Page/蛋白质.md "wikilink")（9[大卡每克](../Page/卡路里.md "wikilink")）。在[小肠内](../Page/小肠.md "wikilink")，三酸甘油酯在[脂肪酶和](../Page/脂肪酶.md "wikilink")[胆汁的作用下被分解为甘油和脂肪酸后进入](../Page/胆汁.md "wikilink")[血管](../Page/血管.md "wikilink")。在[血液内它重组](../Page/血液.md "wikilink")，形成[脂蛋白的组成部分](../Page/脂蛋白.md "wikilink")。它的作用包括向细胞运输脂肪酸。不同的组织可以释放脂肪酸或者吸收脂肪酸作为能源。[脂肪细胞可以产生和储藏三酸甘油酯](../Page/脂肪细胞.md "wikilink")。假如身体需要脂肪酸作为能源时[胰高血糖素会促使脂肪酶分解三酸甘油酯](../Page/胰高血糖素.md "wikilink")，释放自由脂肪酸。由于[脑无法使用脂肪酸作为能源](../Page/脑.md "wikilink")，三酸甘油酯中的[丙三醇会被转化为](../Page/丙三醇.md "wikilink")[葡萄糖供脑作为能源使用](../Page/葡萄糖.md "wikilink")，而脂肪酸也可在[肝中转化为大脑可利用的](../Page/肝.md "wikilink")[酮体](../Page/酮体.md "wikilink")。假如脑对葡萄糖的需要大于身体内的含量时，脂肪细胞也会将三酸甘油酯分解。

## 疾病

[代謝症候群是指由於](../Page/代謝症候群.md "wikilink")[內臟](../Page/內臟.md "wikilink")[脂肪型肥胖所引起各式疾病狀態之統稱](../Page/脂肪.md "wikilink")\[1\]。膽固醇是造成動脈硬化之危險因子，而三酸甘油酯則容易轉化為內臟脂肪\[2\]。膽固醇是製造細胞膜物質之一，也是製造[荷爾蒙或](../Page/荷爾蒙.md "wikilink")[膽汁酸之原料](../Page/膽汁酸.md "wikilink")\[3\]。加上[高血压会提高](../Page/高血压.md "wikilink")[冠状动脉性心脏病与](../Page/冠状动脉性心脏病.md "wikilink")[中風的可能性](../Page/中風.md "wikilink")。不过三酸甘油酯含量的影响比[低密度脂蛋白](../Page/低密度脂蛋白.md "wikilink")（LDL）與[高密度脂蛋白](../Page/高密度脂蛋白.md "wikilink")（HDL）比率的影响要低。主要是由高密度脂蛋白和低密度脂蛋白運送膽固醇\[4\]。其原因可能是因为三酸甘油酯含量与HDL含量之间的反正比例关系。三酸甘油酯一旦過度增加，良性高密度脂蛋白會減少；而惡性低密度脂蛋白則會增加被氧化，進而引起動脈硬化\[5\]。導致[高血脂症之最主要原因](../Page/高血脂症.md "wikilink")，就是因[運動不足引起](../Page/運動.md "wikilink")[肥胖或因飲](../Page/肥胖.md "wikilink")[酒過度而讓三酸甘油酯增加](../Page/酒.md "wikilink")，進而引起高三酸甘油脂症；因過度攝取含有多量膽固醇或[飽和脂肪酸之食品](../Page/飽和脂肪酸.md "wikilink")，而引起高膽固醇血脂症\[6\]。其次，也有因為遺傳，導致融入血液之膽固醇之受容體不足或受損而發病之家族性高血脂症，特別是以三酸甘油酯值變高為特徵，多數是在[成人後發病](../Page/成人.md "wikilink")，因[飲食過量造成三酸甘油酯值漸漸變高](../Page/飲食.md "wikilink")\[7\]。

除[糖尿病或](../Page/糖尿病.md "wikilink")[甲狀線功能降低症](../Page/甲狀腺機能低下症.md "wikilink")、閉塞性[黃疸或](../Page/黃疸.md "wikilink")[肝癌等](../Page/肝癌.md "wikilink")[肝臟疾病](../Page/肝臟.md "wikilink")、[腎病變症候群或](../Page/腎病症候群.md "wikilink")[腎衰竭等](../Page/腎衰竭.md "wikilink")[腎臟疾病之外](../Page/腎臟.md "wikilink")，[高血壓或](../Page/高血壓.md "wikilink")[避孕藥等藥劑](../Page/避孕藥.md "wikilink")，[女性在](../Page/女性.md "wikilink")[更年期也會容易演變成高血脂症](../Page/更年期.md "wikilink")\[8\]。

三酸甘油酯值高，就容易引起心肌梗塞\[9\]。高血脂症也有併發高血壓症、糖尿病、腎臟病等可能性\[10\]。

高三酸甘油酯指数导致的另一个疾病是[胰腺炎](../Page/胰腺炎.md "wikilink")。

### 改善方法

第一、不要不吃[早餐](../Page/早餐.md "wikilink")，隔5至6小時進餐；第二、慢慢嘴嚼，建議每一口都要嘴嚼15次以上；第三、每天飲食適量且攝取均衡營養素；第四、[晚餐要少量](../Page/晚餐.md "wikilink")；第五、不吃[宵夜](../Page/宵夜.md "wikilink")，就寢3小時前不要飲食；第六、身邊不要擺放食物\[11\]。

### 指标

[美国心脏协会就三酸甘油酯指数给出了以下指标](../Page/美国心脏协会.md "wikilink")：\[12\]

|               |               |           |
| ------------- | ------------- | --------- |
| **指数**（毫克／分升） | **指数**（毫摩尔／升） | **注释**    |
| \<150         | \<1.69        | 一般，低危险    |
| 150-199       | 1.70-2.25     | 临界        |
| 200-498       | 2.25-5.63     | 高危险       |
| \>500         | \>5.65        | 非常高，危险性增高 |

请注意这些值是在空腹的情况下化验出来的。吃过饭后三酸甘油酯的值会暂时提高一段时间。

### 降低三酸甘油酯指数

有人建议通过帶氧運動和低糖类的食物来降低三酸甘油酯指数。鱼肉中含的ω-3脂肪酸（每天约5克）、每天一至数克[菸鹼酸](../Page/菸鹼酸.md "wikilink")（维他命B3）以及[羟甲戊二酰辅酶A还原酶抑制剂被用来降低三酸甘油酯指数](../Page/羟甲戊二酰辅酶A还原酶抑制剂.md "wikilink")。[二甲苯氧庚酸被用在极高水平的三酸甘油酯情况下](../Page/二甲苯氧庚酸.md "wikilink")。但是由于它们无效，在有些情况下甚至提高死亡率，因此它们被撤出北美市场。酗酒会提高三酸甘油酯指数。

### 工业加工

工厂在大量处理和使用食用油时需要油的组成分子的成分基本一致。但是植物油因季节和产地各不相同。因此这些油在加工前需要进行处理。首先易挥发成分被分离，这样在煮的时候不会散发气味。通过给油加温以及让蒸汽通过油可以达到这个目的。这个过程的效率由接触时间和温度决定。接触时间可以通过扩建工厂达到，但是这需要一定的投资。通过加温可以降低接触时间。但是加温可能导致不良的反应发生。比如在190ºC的温度下分子会变形，而且在温度降低后不会恢复其原来的形状。一定的分子链可能会改变其位置，在人体内这样的不自然的分子可能会导致癌症。

每天处理100至700吨的大型加工工厂将廉价油中含有的少数稀有油分子分离出来。比如[油菜等廉价油中也包含少数高价油](../Page/油菜.md "wikilink")（比如可可的油）的成分。这些被分离出来的分子被当作[可可脂取代品出售](../Page/可可脂.md "wikilink")。这样的油也往往会经过高温处理。

## 工业使用

在[生物柴油的制造过程中](../Page/生物柴油.md "wikilink")，甘油三酯也通过[酯交换反应分解成为它们的组成部分](../Page/酯交换反应.md "wikilink")。
所得脂肪酸[酯可用作](../Page/酯.md "wikilink")[柴油发动机中的燃料](../Page/柴油发动机.md "wikilink")。
[甘油具有许多用途](../Page/甘油.md "wikilink")，例如在食品的制造和药物的生产中。肥皂工廠也常[使用三酸甘油酯製造肥皂](../Page/皂化反應.md "wikilink")。

## 染色

通过使用（脂溶性染料）进行脂肪酸，甘油三酯，脂蛋白和其他脂质的[染色](../Page/染色_\(生物學\).md "wikilink")。
这些染料可以通过将材料染成特定颜色来限定某种感兴趣的脂肪。
一些例子：[蘇丹四號](../Page/蘇丹四號.md "wikilink")，[油红O和](../Page/油红O.md "wikilink")[苏丹黑B](../Page/苏丹黑B.md "wikilink").

另外，使用[脂肪染色剂可以给三酸甘油酯染色](../Page/脂肪染色剂.md "wikilink")。

## 互动的代谢途径图

## 参阅

  - [脂類](../Page/脂類.md "wikilink")

## 参考资料

[三酰甘油](../Category/三酰甘油.md "wikilink")
[Category:血脂紊亂](../Category/血脂紊亂.md "wikilink")

1.

2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12. <http://www.americanheart.org/presenter.jhtml?identifier=183>