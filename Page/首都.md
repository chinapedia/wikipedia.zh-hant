[Esplanada_dos_Ministérios,_Brasília_DF_04_2006_(modificada).jpg](https://zh.wikipedia.org/wiki/File:Esplanada_dos_Ministérios,_Brasília_DF_04_2006_\(modificada\).jpg "fig:Esplanada_dos_Ministérios,_Brasília_DF_04_2006_(modificada).jpg")*'
[巴西的首都](../Page/巴西.md "wikilink")\]\]
[Canberra_parliamentary_axis.JPG](https://zh.wikipedia.org/wiki/File:Canberra_parliamentary_axis.JPG "fig:Canberra_parliamentary_axis.JPG")*'
[澳大利亞的首都](../Page/澳大利亞.md "wikilink")\]\]
[KL_view_from_Skybridge.jpg](https://zh.wikipedia.org/wiki/File:KL_view_from_Skybridge.jpg "fig:KL_view_from_Skybridge.jpg")*'
[马来西亚的首都](../Page/马来西亚.md "wikilink")\]\]
[Ankara_(16290014657).jpg](https://zh.wikipedia.org/wiki/File:Ankara_\(16290014657\).jpg "fig:Ankara_(16290014657).jpg")*'
[土耳其的首都](../Page/土耳其.md "wikilink")\]\]
[Panorama_of_Hanoi.jpg](https://zh.wikipedia.org/wiki/File:Panorama_of_Hanoi.jpg "fig:Panorama_of_Hanoi.jpg")'''
[越南的首都](../Page/越南.md "wikilink")\]\]

**首都**、**國都**或**京**（**[京師](../Page/京師.md "wikilink")／城／都**），以[現代](../Page/現代.md "wikilink")[政治角度而言](../Page/政治.md "wikilink")，通常指一個[國家的](../Page/國家.md "wikilink")[中央政府所在地的首要](../Page/中央政府.md "wikilink")[行政中心](../Page/行政中心.md "wikilink")[城市](../Page/城市.md "wikilink")\[1\]\[2\]，也是[政治活动的中心城市](../Page/政治.md "wikilink")、各类[国家级](../Page/国家.md "wikilink")[机关集中駐紮地](../Page/机关.md "wikilink")、国家[主权的象徵城市](../Page/主权.md "wikilink")。

在大部分国家，首都是國家最大的城市，如[英國](../Page/英國.md "wikilink")[倫敦](../Page/倫敦.md "wikilink")、[法國](../Page/法國.md "wikilink")[巴黎等](../Page/巴黎.md "wikilink")；大部份的國家也未在[憲法和](../Page/憲法.md "wikilink")[法律內訂定首都地點](../Page/法律.md "wikilink")\[3\]，而在部分国家，政治中心与经济中心分离，例如美国首都[華盛頓哥倫比亞特區](../Page/華盛頓哥倫比亞特區.md "wikilink")、土耳其首都[安卡拉等](../Page/安卡拉.md "wikilink")。有时一个国家有多个首都，如**行政首都**、**司法首都**，分别是该国的行政和司法中心，例如[南非](../Page/南非.md "wikilink")。

在[國際政治及](../Page/國際政治.md "wikilink")[外交上](../Page/外交.md "wikilink")，首都的名稱可以直接代表其所在的國家及其中央政府。常見的例子有以“[莫斯科](../Page/莫斯科.md "wikilink")”取代[俄羅斯](../Page/俄羅斯.md "wikilink")、“[北京](../Page/北京.md "wikilink")”取代[中華人民共和國](../Page/中華人民共和國.md "wikilink")、“[台北](../Page/台北.md "wikilink")”取代[中華民國](../Page/中華民國.md "wikilink")、“[平壤](../Page/平壤.md "wikilink")”取代[朝鮮](../Page/朝鮮.md "wikilink")、“[華府](../Page/華盛頓哥倫比亞特區.md "wikilink")”取代[美國](../Page/美國.md "wikilink")、“[倫敦](../Page/倫敦.md "wikilink")”取代[英國](../Page/英國.md "wikilink")、“[巴黎](../Page/巴黎.md "wikilink")”取代[法國等](../Page/法國.md "wikilink")。

在[歷史上](../Page/歷史.md "wikilink")，國家或地區的主要[經濟中心往往成為](../Page/經濟.md "wikilink")[政權的焦點](../Page/政權.md "wikilink")，並透過佔領或合併成為首都。[倫敦和](../Page/倫敦.md "wikilink")[莫斯科正是這樣](../Page/莫斯科.md "wikilink")。首都無形中吸引有志投身政治的人才，例如[律師](../Page/律師.md "wikilink")、[新聞媒體及](../Page/新聞媒體.md "wikilink")[公共政策的研究員](../Page/公共政策.md "wikilink")，協助政府實施有效的管治。如果首都兼是首要的經濟、[文化或](../Page/文化.md "wikilink")[知識中心](../Page/知識.md "wikilink")，有時會被稱為「**首要城市**」，如[倫敦和](../Page/倫敦.md "wikilink")[布宜諾斯艾利斯](../Page/布宜諾斯艾利斯.md "wikilink")。

首都的設立有時會阻礙原有的主要城市的進一步發展。由於舊都[里約熱內盧和巴西東南部的人口過於稠密](../Page/里約熱內盧.md "wikilink")，新都[巴西利亞遂設於](../Page/巴西利亞.md "wikilink")[巴西](../Page/巴西.md "wikilink")[內陸](../Page/內陸.md "wikilink")，而[德國首都則設於](../Page/德國.md "wikilink")[柏林](../Page/柏林.md "wikilink")。

[政治與](../Page/政治.md "wikilink")[經濟或](../Page/經濟.md "wikilink")[文化權力會合的情況並不普遍](../Page/文化.md "wikilink")。傳統首都經濟上會被競爭者超越，[朝代或文化的衰落](../Page/朝代.md "wikilink")，也意味著首都的破滅，[巴比倫就是一個例子](../Page/巴比倫.md "wikilink")。此外，很多現代首都，例如[阿布賈](../Page/阿布賈.md "wikilink")、[坎培拉和](../Page/坎培拉.md "wikilink")[渥太華](../Page/渥太華.md "wikilink")，特意設於原有的經濟地區以外，而且至今仍未發展成新的[工](../Page/工業.md "wikilink")[商業中心](../Page/商業.md "wikilink")。

首都不一定會設立在面積較大的國土上，如[赤道幾內亞的](../Page/赤道幾內亞.md "wikilink")[馬拉博](../Page/馬拉博.md "wikilink")、[丹麥的](../Page/丹麥.md "wikilink")[哥本哈根](../Page/哥本哈根.md "wikilink")、[甘比亞的](../Page/甘比亞.md "wikilink")[班竹](../Page/班竹.md "wikilink")，這些都不是[島國](../Page/島國.md "wikilink")，首都卻是位於較小的離島。

一般來說首都通常會特意設在易守難攻的地理位置上，以防其他國家的侵略，但也有[國家的首都是設於鄰國附近的邊界旁或是相鄰的地理位置上](../Page/國家.md "wikilink")，如[韓國的](../Page/韓國.md "wikilink")[首爾](../Page/首爾.md "wikilink")、[寮國的](../Page/寮國.md "wikilink")[永珍](../Page/永珍.md "wikilink")、[查德的](../Page/查德.md "wikilink")[恩將納](../Page/恩將納.md "wikilink")、[斯洛伐克的](../Page/斯洛伐克.md "wikilink")[布拉提斯拉瓦](../Page/布拉提斯拉瓦.md "wikilink")、[波札那的](../Page/波札那.md "wikilink")[嘉柏隆里](../Page/嘉柏隆里.md "wikilink")、[烏茲別克的](../Page/烏茲別克.md "wikilink")[塔什干](../Page/塔什干.md "wikilink")。

## 特殊情況

每一個[國家通常只設立一個首都](../Page/國家.md "wikilink")，因為[政府通常會將其重要機關集中在首都地區](../Page/政府.md "wikilink")，以方便政府高層[行政和管理](../Page/行政.md "wikilink")，但亦有例外。一些國家有多個首都，一些甚至沒有。有時候，實際的首都和法定的首都由於某些原因並不在同一個[城市](../Page/城市.md "wikilink")。譬如，一個稱為「首都」的城市，實際上並非[中央政府所在地](../Page/中央政府所在地.md "wikilink")。反之，所謂的正式「首都」雖然是中央政府的所在地，但可能不是[政治決策的](../Page/政治.md "wikilink")[地理中心](../Page/地理.md "wikilink")。故此，「行政首都」一般被認定為是該國的「國家首都」。

### 多首都

<center>

| 國家                                                    | 首都                                                                             | 詳情                               |
| ----------------------------------------------------- | ------------------------------------------------------------------------------ | -------------------------------- |
| ** [貝南](../Page/貝南.md "wikilink")**                   | [波多諾伏](../Page/波多諾伏.md "wikilink")                                             | [法定首都](../Page/憲法.md "wikilink") |
| [科托努](../Page/科托努.md "wikilink")                      | 政府所在地                                                                          |                                  |
| ** [玻利維亞](../Page/玻利維亞.md "wikilink")**               | [蘇克雷](../Page/蘇克雷.md "wikilink")                                               | [法定首都](../Page/憲法.md "wikilink") |
| [拉巴斯](../Page/拉巴斯.md "wikilink")                      | 國家行政與立法機構所在地                                                                   |                                  |
| ** [智利](../Page/智利.md "wikilink")**                   | [聖地亚哥](../Page/聖地亞哥_\(智利\).md "wikilink")                                      | [法定首都](../Page/憲法.md "wikilink") |
| [瓦爾帕萊索](../Page/瓦爾帕萊索.md "wikilink")                  | [國會所在地](../Page/國會.md "wikilink")                                              |                                  |
| ** [象牙海岸](../Page/象牙海岸.md "wikilink")**               | [亞穆蘇克羅](../Page/亞穆蘇克羅.md "wikilink")                                           | [法定首都](../Page/憲法.md "wikilink") |
| [阿比讓](../Page/阿比讓.md "wikilink")                      | 政府所在地                                                                          |                                  |
| ** [馬來西亞](../Page/馬來西亞.md "wikilink")**               |                                                                                |                                  |
| [吉隆坡](../Page/吉隆坡.md "wikilink")                      | [法定首都](../Page/憲法.md "wikilink")、经济与金融首都、[国会所在地](../Page/马来西亚国会.md "wikilink") |                                  |
| [布城](../Page/布城.md "wikilink")                        | 行政首都、政府及國家司法機構的所在地                                                             |                                  |
| ** [荷蘭](../Page/荷蘭.md "wikilink")**                   |                                                                                |                                  |
| [阿姆斯特丹](../Page/阿姆斯特丹.md "wikilink")                  | [法定首都](../Page/憲法.md "wikilink")                                               |                                  |
| [海牙](../Page/海牙.md "wikilink")                        | 政府所在地                                                                          |                                  |
| ** [韓國](../Page/大韩民国.md "wikilink")**                 | [世宗](../Page/世宗市.md "wikilink")                                                | 行政首都                             |
| [首爾](../Page/首爾.md "wikilink")                        | 中央政府所在地                                                                        |                                  |
| ** [南非](../Page/南非.md "wikilink")**                   | [比勒陀利亞](../Page/比勒陀利亞.md "wikilink")                                           | 行政首都                             |
| [開普敦](../Page/開普敦.md "wikilink")                      | 立法首都                                                                           |                                  |
| [布隆方丹](../Page/布隆方丹.md "wikilink")                    | 司法首都                                                                           |                                  |
| ** [斯威士蘭](../Page/斯威士蘭.md "wikilink")**               | [姆巴巴納](../Page/姆巴巴納.md "wikilink")                                             | 行政首都                             |
| [洛班巴](../Page/洛班巴.md "wikilink")                      | 立法首都、皇宮所在地                                                                     |                                  |
| ** [坦桑尼亞](../Page/坦桑尼亞.md "wikilink")**               | [多多馬](../Page/多多馬.md "wikilink")                                               | [法定首都](../Page/憲法.md "wikilink") |
| [達累斯薩拉姆](../Page/達累斯薩拉姆.md "wikilink")                | 政府所在地                                                                          |                                  |
| ** [阿拉伯撒哈拉民主共和國](../Page/阿拉伯撒哈拉民主共和國.md "wikilink")** | [阿尤恩](../Page/阿尤恩.md "wikilink")                                               | 宣称的首都                            |
| [提法里提](../Page/提法里提.md "wikilink")                    | 事实上的首都                                                                         |                                  |

</center>

### 一些特殊的首都

  -
一些日本人認為[東京和](../Page/東京.md "wikilink")[京都都是](../Page/京都.md "wikilink")[日本首都](../Page/日本首都.md "wikilink")。

  -
[以色列以](../Page/以色列.md "wikilink")[耶路撒冷為首都](../Page/耶路撒冷.md "wikilink")，大部分政府機構都駐守此處。但[耶路撒冷地位問題未完全解決之前](../Page/耶路撒冷#耶路撒冷地位問題.md "wikilink")，大部分國家和[聯合國都以](../Page/聯合國.md "wikilink")[特拉維夫為以色列首都](../Page/特拉維夫.md "wikilink")，因此大多數[大使館設館於特拉維夫而非耶路撒冷](../Page/大使館.md "wikilink")。特拉維夫自1948年12月起成為以色列的「[實質](../Page/De_facto.md "wikilink")」首都。而[巴勒斯坦民族權力機構亦聲稱耶路撒冷是](../Page/巴勒斯坦民族權力機構.md "wikilink")[巴勒斯坦國的首都](../Page/巴勒斯坦國.md "wikilink")，但現時的「實質」首都位於[拉姆安拉](../Page/拉姆安拉.md "wikilink")，而[哈馬斯政府則駐守](../Page/哈馬斯.md "wikilink")[加薩市](../Page/加薩.md "wikilink")。

  -
[德國的行政首都是](../Page/德國.md "wikilink")[柏林](../Page/柏林.md "wikilink")，但依聯邦法律有部份政府部門仍設址於前西德首都[波恩](../Page/波恩.md "wikilink")，波恩仍被稱為「聯邦市」（）。[德國聯邦最高法院和](../Page/德國聯邦最高法院.md "wikilink")[德國聯邦憲法法院則位於](../Page/德國聯邦憲法法院.md "wikilink")[卡爾斯魯厄](../Page/卡爾斯魯厄.md "wikilink")。

  -
[波士尼亞赫塞哥維納的](../Page/波士尼亞赫塞哥維納.md "wikilink")[塞族共和國](../Page/塞族共和國.md "wikilink")，根據[岱頓協定](../Page/岱頓協定.md "wikilink")，以[塞拉耶佛為法理首都](../Page/塞拉耶佛.md "wikilink")（塞拉耶佛同時是[波士尼亞的首都](../Page/波士尼亞.md "wikilink")），但事實上其政府位於[巴尼亞盧卡](../Page/巴尼亞盧卡.md "wikilink")。

### 國際組織

在如英语在内的一些外文中，[首府和首都往往意义相近或者相同](../Page/首府.md "wikilink")，部分国际组织所在地也会被称为首都。

  - ：雖然歐盟不是一個「國家」，但[比利時的](../Page/比利時.md "wikilink")[布魯塞爾被廣泛認為是歐盟的](../Page/布魯塞爾.md "wikilink")「首都」\[4\]。因為歐盟的主要行政機構、[歐盟委員會](../Page/歐盟委員會.md "wikilink")、[歐洲理事會](../Page/歐洲理事會.md "wikilink")、[歐盟理事會](../Page/歐盟理事會.md "wikilink")（也有部分立法機能）都駐在此地，而[歐洲議會大部分會議亦在此進行](../Page/歐洲議會.md "wikilink")（常規會議和正式投票在[斯特拉斯堡進行](../Page/斯特拉斯堡.md "wikilink")）。歐盟沒有正式指定一個「首都」，但《[阿姆斯特丹條約](../Page/阿姆斯特丹條約.md "wikilink")》規定了大部分機構駐紮布魯塞爾。此外，[歐洲法院位於](../Page/歐洲法院.md "wikilink")[盧森堡](../Page/盧森堡.md "wikilink")，而[歐洲中央銀行駐於](../Page/歐洲中央銀行.md "wikilink")[法蘭克福](../Page/法蘭克福.md "wikilink")。因此，歐盟也可以被看作像南非般的「多首都」狀況。不過對一個國際實體而言「首都」概念有爭議，因為「首都」或「首府」通常分別用於主權國和國內的行政區劃。

### 不明定首都

：[中華民國於](../Page/中華民國.md "wikilink")1927年定國都於[南京市](../Page/南京市_\(中華民國\).md "wikilink")。1937年11月因為[八年抗戰日軍勢力已威脅到中央京畿而遷都](../Page/八年抗戰.md "wikilink")[重慶市](../Page/重慶市_\(中華民國\).md "wikilink")（[陪都](../Page/重慶陪都時期.md "wikilink")），而在勝利後於1946年5月還都南京市。然而在1946年11月召開的[制憲國民大會中](../Page/制憲國民大會.md "wikilink")，對於國都設於[南京或](../Page/南京.md "wikilink")[北平相持不下](../Page/北平.md "wikilink")，經時任[國民政府主席](../Page/國民政府主席.md "wikilink")[蔣中正主張國都地點不必定於](../Page/蔣中正.md "wikilink")[憲法](../Page/中華民國憲法.md "wikilink")，始將[制憲國民大會的第一讀會通過的](../Page/制憲國民大會.md "wikilink")「國都定於北平」一條予以刪除\[5\]，中央政府仍設在南京，南京仍是首都。而後[第二次国共内战失利](../Page/第二次国共内战.md "wikilink")，[中國人民解放軍於](../Page/中國人民解放軍.md "wikilink")1949年4月23日攻佔南京城。中央政府在幾次遷移後，於1949年12月9日遷往[臺北市](../Page/臺北市.md "wikilink")，之後該市成為[中央政府所在地或實質上的首都](../Page/中央政府所在地.md "wikilink")。

然而，在1990年代及以前，[中華民國政府從未放棄以武力光復中國大陸並還都南京的目標](../Page/中華民國政府.md "wikilink")，因此習稱臺北為「戰時首都」（類似於[中國歷史的](../Page/中國歷史.md "wikilink")「[行在](../Page/行在.md "wikilink")」），並在教科書上記載南京市為首都\[6\]\[7\]。2000年後[民進黨政府執政期間](../Page/民進黨政府.md "wikilink")，官方多次表示台北市是中華民國首都\[8\]\[9\]
\[10\]。2002年起，[中華民國教育部決議修改教科書的相關記載為](../Page/中華民國教育部.md "wikilink")「中華民國中央政府位於台北」或「中華民國中央部会在台北」\[11\]\[12\]。

但如果以民意來說，目前台灣絕大多數民眾(特別是年輕世代)多普遍認定中華民國首都是[台北市而非南京市](../Page/台北市.md "wikilink")。

### 沒有首都

  - [城市國家](../Page/城市國家.md "wikilink")，如[新加坡](../Page/新加坡.md "wikilink")、[梵-{}-蒂岡等](../Page/梵蒂岡.md "wikilink")，並沒有下設首都，但是基於國家與城市的雙重身份，人們有時會視之為「首都」。
  - [諾魯共和國是一個只有](../Page/諾魯共和國.md "wikilink")21[平方公里](../Page/平方公里.md "wikilink")（8[平方英里](../Page/平方英里.md "wikilink")）的[島國](../Page/島國.md "wikilink")，沒有首都，行政管理中心設在[雅連](../Page/雅連.md "wikilink")。
  - [日本](../Page/日本.md "wikilink")，在《》废止后，\[13\]取代其的《》则定义了[首都圈](../Page/首都圈_\(日本\).md "wikilink")。
  - [瑞士](../Page/瑞士.md "wikilink")，沒有法律上的首都，但是事实上首都为[伯尔尼](../Page/伯尔尼.md "wikilink")。

## 政治象徵意義

隨著現代[帝國及](../Page/帝國.md "wikilink")[民族國家崛起](../Page/民族國家.md "wikilink")，首都已成為[國家及](../Page/國家.md "wikilink")[政府的](../Page/政府.md "wikilink")[象徵](../Page/象徵.md "wikilink")，而且滲透著政治意義。在古代，多數國家行[君主專制](../Page/君主專制.md "wikilink")，「首都」一般指[君主理政和居住的地方城市](../Page/君主.md "wikilink")，所謂「天子腳下，首善之區」，首善之名，出自《[漢書](../Page/漢書.md "wikilink")·儒林傳序》：“故教化之行也，建首善自京師始。”[南宋耐得翁](../Page/南宋.md "wikilink")《都城紀勝序》說：杭州“車書混一，人物繁盛，風俗繩厚”，“為今日四方之標准”。不同的是，現代首都的選址、建立，以至遷移，均取決於人為因素。舉例：

  - 首都重置：曾經是廢墟的[雅典](../Page/雅典.md "wikilink")，在獨立後被定為首都，以恢復昔日的傳奇光彩。另外，經歷[冷戰及](../Page/冷戰.md "wikilink")[兩德統一](../Page/兩德統一.md "wikilink")，[柏林再度成為](../Page/柏林.md "wikilink")[德國的首都](../Page/德國.md "wikilink")。首都重置的例子還有[十月革命後](../Page/十月革命.md "wikilink")、[蘇聯以](../Page/蘇聯.md "wikilink")[莫斯科作為首都](../Page/莫斯科.md "wikilink")，以區別象徵沙皇統治及由[彼得大帝興建的位處](../Page/彼得大帝.md "wikilink")[芬蘭灣的](../Page/芬蘭灣.md "wikilink")[聖彼德堡](../Page/聖彼德堡.md "wikilink")。
  - 首都象徵式遷移至地理或人口較次要的選址，可能基於[經濟或](../Page/經濟地理.md "wikilink")[戰略因素](../Page/軍事地理.md "wikilink")（或稱為「前鋒首都」或「先鋒首都」）。[彼得大帝把政府由內陸的](../Page/彼得大帝.md "wikilink")[莫斯科遷至位處](../Page/莫斯科.md "wikilink")[芬蘭灣的](../Page/芬蘭灣.md "wikilink")[聖彼德堡](../Page/聖彼德堡.md "wikilink")，為[俄羅斯帝國確立作為](../Page/俄羅斯帝國.md "wikilink")[歐洲及](../Page/歐洲.md "wikilink")[西方國家為定位](../Page/西方國家.md "wikilink")；而[穆斯塔法·凱末爾·阿塔土克則把政府東遷至](../Page/穆斯塔法·凱末爾·阿塔土克.md "wikilink")[安卡拉](../Page/安卡拉.md "wikilink")，保衛[奧斯曼](../Page/奧斯曼帝國.md "wikilink")[伊斯坦堡](../Page/伊斯坦堡.md "wikilink")。[明成祖把國都由中央的](../Page/明成祖.md "wikilink")[應天府](../Page/應天府.md "wikilink")（今[南京](../Page/南京.md "wikilink")）遷至[順天府](../Page/順天府.md "wikilink")（後改稱[北京](../Page/北京.md "wikilink")），以防[滿](../Page/滿族.md "wikilink")[蒙等](../Page/蒙古族.md "wikilink")[明長城以北的外族侵擾邊疆](../Page/明長城.md "wikilink")。其他例子有[東京](../Page/東京.md "wikilink")、[阿布賈](../Page/阿布賈.md "wikilink")、[努尔苏丹](../Page/努尔苏丹.md "wikilink")、[巴西利亞](../Page/巴西利亞.md "wikilink")、[赫爾辛基](../Page/赫爾辛基.md "wikilink")、[伊斯蘭馬巴德](../Page/伊斯蘭馬巴德.md "wikilink")、[奈比多及](../Page/奈比多.md "wikilink")[亞穆蘇克羅](../Page/亞穆蘇克羅.md "wikilink")。
  - 首都的選址、建立不受到地方及政治因素妨礙，以代表新國家的統一。例子有[伯爾尼](../Page/伯爾尼.md "wikilink")、[坎培拉](../Page/坎培拉.md "wikilink")、[馬德里](../Page/馬德里.md "wikilink")、[渥太華](../Page/渥太華.md "wikilink")、[巴西利亞及](../Page/巴西利亞.md "wikilink")[華盛頓哥倫比亞特區](../Page/華盛頓哥倫比亞特區.md "wikilink")。[南北戰爭時期](../Page/南北戰爭.md "wikilink")，接壤[美利堅聯盟國的](../Page/美利堅聯盟國.md "wikilink")[華盛頓哥倫比亞特區面臨邦聯的進攻](../Page/華盛頓哥倫比亞特區.md "wikilink")，聯邦軍用了大量資源來保衛該地。當時[鐵路和](../Page/鐵路.md "wikilink")[電報發達](../Page/電報.md "wikilink")，[聯邦政府基於弱勢可選擇遷移](../Page/美國聯邦政府.md "wikilink")[費城或](../Page/費城.md "wikilink")[紐約](../Page/紐約.md "wikilink")，但他們選擇了死守首都。
  - [中華民國政府在](../Page/中華民國政府.md "wikilink")[國民政府](../Page/國民政府.md "wikilink")[抗日戰爭中遷移到](../Page/抗日戰爭.md "wikilink")[重慶](../Page/重慶.md "wikilink")，並定重慶為[陪都](../Page/陪都.md "wikilink")，戰後始遷回[南京](../Page/南京.md "wikilink")，而在[國共內戰失利後再遷往](../Page/國共內戰.md "wikilink")[台北](../Page/台北.md "wikilink")。直至1990年代期間，實質首都雖在[台北市](../Page/台北市.md "wikilink")，卻在教科書上記載[南京市為首都以呼應其](../Page/南京市_\(中華民國\).md "wikilink")「光復大陸」之政策，但現今絕大多數台灣人已將[台北市視為首都](../Page/台北市.md "wikilink")，中華民國政府以[台北市作為](../Page/台北市.md "wikilink")「政府所在地」，作為首都的地位。
  - 有時氣候因素也可能成為遷移首都的原因，中美洲國家[貝里斯的舊首都](../Page/貝里斯.md "wikilink")[貝里斯市由於靠海的關係](../Page/貝里斯市.md "wikilink")，曾遭到颶風毀壞，最後貝國政府將首都遷到位處內陸的[貝爾墨邦以躲避颶風](../Page/貝爾墨邦.md "wikilink")。

## 戰略意義

首都長久以來是[戰爭的首要目標](../Page/戰爭.md "wikilink")，佔領首都等同取得敵方的政府，是戰勝的關鍵。

首都，作為國家政治、經濟、文化的會聚並不是永恆不變的。在古代，國家一般採取[中央集權政策](../Page/中央集權.md "wikilink")，地方勢力有限；首都一旦淪陷，就意味著[朝代的覆亡](../Page/朝代.md "wikilink")。[中國](../Page/中國.md "wikilink")[三國時代](../Page/三國時代.md "wikilink")，[漢](../Page/蜀漢.md "wikilink")、[吳因失去各自的首都](../Page/孫吳.md "wikilink")──[成都和](../Page/成都.md "wikilink")[建業](../Page/建業.md "wikilink")（今[南京](../Page/南京.md "wikilink")）而亡國。

過去，[社會經濟傾向地方權力](../Page/社會經濟.md "wikilink")，這戰略方式在[封建制度發展後尤其盛行](../Page/封建制度.md "wikilink")，其後又在民主和資本主義的發展中再獲肯定。對於[世界其他地方](../Page/世界.md "wikilink")，包括[西方而言](../Page/西方國家.md "wikilink")，首都在[軍事上是較次要的](../Page/軍事.md "wikilink")。1205年，[拉丁人在](../Page/拉丁人.md "wikilink")[第四次十字軍東征後佔領了](../Page/第四次十字軍東征.md "wikilink")[拜占庭的首都](../Page/拜占庭帝國.md "wikilink")[君士坦丁堡](../Page/君士坦丁堡.md "wikilink")，然而，拜占庭勢力成功在數個[省份中重組](../Page/省份.md "wikilink")；地方[貴族在](../Page/貴族.md "wikilink")60年後重奪首都，並把國祚延長了200年。[英國勢力先後在](../Page/大英帝國.md "wikilink")[美國獨立戰爭和](../Page/美國獨立戰爭.md "wikilink")[第二次英美戰爭中劫掠了](../Page/第二次英美戰爭.md "wikilink")[美國首都](../Page/美國.md "wikilink")[華盛頓特區](../Page/華盛頓特區.md "wikilink")，但美國勢力得到地方政府和傳統上獨立的邊遠地區的平民的支援，仍足以在地方開展戰鬥。例外的是，高度[中央集權國家](../Page/中央集權.md "wikilink")，例如[法國](../Page/法國.md "wikilink")，其中央政體有效地協調偏遠地區的資源，為國家帶來絕對的優勢。但是，假如首都被佔，國家會面臨崩潰的邊緣；其他戰略方面，法國的傳統敵人[德國](../Page/德國.md "wikilink")，於兩次[世界大戰時把目標集中於佔領](../Page/世界大戰.md "wikilink")[巴黎](../Page/巴黎.md "wikilink")。

## 遷都

「遷都」指一個國家將原來設立的首都，由另一個城市取代，主要行動是將政府高層重要機關總部遷移至新首都，而背後亦涉及很多原因。在古代，多數國家面對不穩定的政治環境，因而遷都的機會較多。也有可能是舊的首都由於某些原因（例如經濟）而逐漸喪失其重要性。朝代的交替或領土的變更或擴大亦影響首都的遷移。

在中國，[明朝從](../Page/明朝.md "wikilink")[應天府](../Page/應天府.md "wikilink")（今南京）遷都[北平府](../Page/北平府.md "wikilink")（稱[北京](../Page/北京.md "wikilink")），加強北方边防，以防[滿](../Page/滿洲人.md "wikilink")[蒙兩族侵擾邊疆](../Page/蒙古人.md "wikilink")。[李自成佔領首都後](../Page/李自成.md "wikilink")，明朝大致滅亡（[南明](../Page/南明.md "wikilink")──幾個由明朝[皇室在南方建立的流亡政權](../Page/皇室.md "wikilink")──則繼續聲稱作為明室正統而存在）。而[滿洲人在崇禎十七年五月初二](../Page/滿洲人.md "wikilink")(1644年6月6日)進入北京，将首都从[沈阳迁往北京](../Page/沈阳.md "wikilink")，則代表了正式的權力轉移。這樣的格局在[中國歷史屢見不鮮](../Page/中國歷史.md "wikilink")，及至20世紀傳統[儒家政體垮臺](../Page/儒家.md "wikilink")。[滿清覆亡後](../Page/滿清.md "wikilink")，基於地方分權及交通技術的改進，[國民政府得以在](../Page/國民政府.md "wikilink")[抗日戰爭時期迅速遷都](../Page/中國抗日戰爭.md "wikilink")，並維持其原有的領導架構。1949年因政权移手后，[中华民国政府由南京遷至](../Page/中华民国政府.md "wikilink")[臺北](../Page/臺北.md "wikilink")。

在其他國家例子有[蘇聯](../Page/蘇聯.md "wikilink")/[俄罗斯](../Page/俄罗斯.md "wikilink")（[聖彼得堡](../Page/聖彼得堡.md "wikilink")—[莫斯科](../Page/莫斯科.md "wikilink")）、[土耳其](../Page/土耳其.md "wikilink")（[伊斯坦堡](../Page/伊斯坦堡.md "wikilink")—[安卡拉](../Page/安卡拉.md "wikilink")）和[東西統一後的](../Page/德國統一.md "wikilink")[德國](../Page/德國.md "wikilink")（[波恩](../Page/波恩.md "wikilink")—[柏林](../Page/柏林.md "wikilink")）。或是像[巴西或](../Page/巴西.md "wikilink")[澳大利亞等重新建造城市移轉首都的例子](../Page/澳大利亞.md "wikilink")。

然而，因為古代國家與開發中國家（例如於2005年遷都的[緬甸](../Page/緬甸.md "wikilink")）行政架構較為簡單，遷都相對於已開發或相對發達國家來說是容易得多。現代的國家遷都要顧及將龐大複雜的政府部門有序不紊地調遷的步驟，以及巨大的國家經濟開支，故此一次完整的遷都可能要花上近10年的時間。

但也有其變通的方式讓國家資源不至於太集中於首都，但又不用花費巨資遷移首都，有些國家便設立所謂的副首都、行政副中心或是[陪都來分散其風險](../Page/陪都.md "wikilink")。

## 最大首都

[缩略图](https://zh.wikipedia.org/wiki/File:Capital_not_largest_city.svg "fig:缩略图")
一些国家的最大城市並非首都，如圖上所示。

以下列出各大洲的最大首都，以[市區](../Page/市區.md "wikilink")／[都會區人口計算](../Page/都會區.md "wikilink")：

  - [非洲](../Page/非洲.md "wikilink")：[開羅](../Page/開羅.md "wikilink")（11,146,000）
  - [亞洲](../Page/亞洲.md "wikilink")：[東京](../Page/東京.md "wikilink")（35,237,000）
  - [歐洲](../Page/歐洲.md "wikilink")：[莫斯科](../Page/莫斯科.md "wikilink")（10,400,000）
  - [北美洲](../Page/北美洲.md "wikilink")：[墨西哥城](../Page/墨西哥城.md "wikilink")（17,809,471）
  - [大洋洲](../Page/大洋洲.md "wikilink")：[惠灵顿](../Page/惠灵顿.md "wikilink")（445,400）
  - [南美洲](../Page/南美洲.md "wikilink")：[布宜諾斯艾利斯](../Page/布宜諾斯艾利斯.md "wikilink")（14,230,000）

## 首都列表

  - [各国首都列表](../Page/各国首都列表.md "wikilink")

      - （兼最大城市）

      -
  -
  -
  -
  -
  -
  -
## 參考文獻

## 外部連結

## 参见

  - [帝都](../Page/帝都.md "wikilink")
  - [京師](../Page/京師.md "wikilink")
  - [中国古都](../Page/中国古都.md "wikilink")
  - [首都圈](../Page/首都圈.md "wikilink")
  - [行政中心](../Page/行政中心.md "wikilink")
  - [中央政府所在地](../Page/中央政府所在地.md "wikilink")
  - [行在](../Page/行在.md "wikilink")
  - [陪都](../Page/陪都.md "wikilink")
  - [首府](../Page/首府.md "wikilink")
  - [遷都](../Page/遷都.md "wikilink")

{{-}}

[Category:行政中心](../Category/行政中心.md "wikilink")
[首都](../Category/首都.md "wikilink")

1.
2.
3.
4.
5.  荊知仁，《中國立憲史》，461頁
6.  [蔣中正於](../Page/蔣中正.md "wikilink")1967年6月27日主持總統府[國父紀念月會訓詞](../Page/國父紀念月會.md "wikilink")：「現在我們是處於戰時戰地，時時刻刻都在備戰狀態之中，臺北市為中央政府所在地，亦即為戰時之首都。以臺北改為院轄市的目的，即在建設其成為一個現代的都市，並以適應戰時之需要。此後一切規畫、組織、制度、行動即皆應以此一認識，為其準據」。《先總統蔣公全集》，第三冊，4070頁，[中國文化大學出版部](../Page/中國文化大學.md "wikilink")，1984年4月，[張其昀主編](../Page/張其昀.md "wikilink")。
7.  中華民國中等學校歷史教科書。1996年出版。國立編譯館。
8.  [近日修改教科書　游揆：中華民國首都在台北市](http://www.ettoday.com/2002/03/29/328-1282773.htm)
9.  [台巴聯合公報](http://www7.www.gov.tw/todaytw/2005/TWtaiwan/ch04/2-4-11-0.html)，其中最後的部分記載：“本聯合公報於中華民國93年5月24日即西元2004年5月24日在中華民國首都台北市簽署。”
10. [近日修改教科書　游揆：中華民國首都在台北市](http://www.nownews.com/2002/03/29/328-1282773.htm)
11. 中華民國中等學校歷史教科書。2007年出版。國立編譯館。
12. 中華民國原依[訓政時期約法第五條定](../Page/訓政時期約法.md "wikilink")[南京市為法定首都](../Page/南京市_\(中華民國\).md "wikilink")，1947年[中華民國憲法實行後](../Page/中華民國憲法.md "wikilink")，憲法中沒有法定首都的規定。詳見[中華民國憲法與](../Page/s:中華民國憲法.md "wikilink")[訓政時期約法](../Page/s:訓政時期約法.md "wikilink")
13.