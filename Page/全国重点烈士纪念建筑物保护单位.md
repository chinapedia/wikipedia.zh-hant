**全国重点烈士纪念建筑物保护单位**，是为纪念在[中国各个革命历史时期的重大事件](../Page/中国.md "wikilink")、重要战役和主要革命根据地斗争中牺牲的烈士或在全国有重要影响的著名烈士和为中国革命牺牲的知名国际友人而修建的，或者在对外开放的重点地区、少数民族地区修建的规模较大的，经确定的烈士纪念碑、馆和烈士陵园。列为全国重点保护的单位，由[民政部提出](../Page/中华人民共和国民政部.md "wikilink")，报[国务院批准后由民政部公布](../Page/中华人民共和国国务院.md "wikilink")。

迄今在中国（大陆）境内共有174处全国重点烈士纪念建筑物保护单位（见注1）。

  - 第一批（批次I），共32处，1986年10月15日批准，1986年10月28日公布；
  - 第二批（批次II），共36处，1989年8月20日批准，1989年8月31日公布；
  - 增补一处（增列I），1990年5月24日批准，1990年6月14日公布；
  - 增补3处（增列II），1992年6月17日批准（或公布）；
  - 第三批（批次III），共18处，1996年3月31日批准，1996年4月12日公布；
  - 第四批（批次IV），共20处，2001年4月30日批准，2001年5月17日公布；
  - 第五批（批次V），共64处，2009年3月2日批准，2009年3月23日公布。

## 北京

  - [李大钊](../Page/李大钊.md "wikilink")[烈士陵园](../Page/李大钊烈士陵园.md "wikilink")（[海淀区](../Page/海淀区.md "wikilink")，批次I）
  - [平西抗日烈士陵园](../Page/平西抗日烈士陵园.md "wikilink")（[房山区](../Page/房山区.md "wikilink")，批次III）
  - [平北抗日烈士纪念园](../Page/平北抗日烈士纪念园.md "wikilink")（[延庆县](../Page/延庆县.md "wikilink")，批次V〕

## 天津

  - [盘山烈士陵园](../Page/盘山烈士陵园.md "wikilink")（[蓟县](../Page/蓟县.md "wikilink")，批次II）
  - [天津市烈士陵园](../Page/天津市烈士陵园.md "wikilink")（[北辰区](../Page/北辰区.md "wikilink")，批次V〕

## 河北

  - [晋冀鲁豫烈士陵园](../Page/晋冀鲁豫烈士陵园.md "wikilink")（[邯郸市](../Page/邯郸市.md "wikilink")，批次I）
  - [华北军区烈士陵园](../Page/华北军区烈士陵园.md "wikilink")（[石家庄市](../Page/石家庄市.md "wikilink")，批次I）
  - [狼牙山](../Page/狼牙山.md "wikilink")[五勇士纪念塔](../Page/狼牙山五勇士纪念塔.md "wikilink")（[易县](../Page/易县.md "wikilink")，批次I）
  - [董存瑞](../Page/董存瑞.md "wikilink")[烈士陵园](../Page/董存瑞烈士陵园.md "wikilink")（[隆化县](../Page/隆化县.md "wikilink")，批次I）
  - [冀南烈士陵园](../Page/冀南烈士陵园.md "wikilink")（[南宫市](../Page/南宫市.md "wikilink")，批次II）
  - [冀东烈士陵园](../Page/冀东烈士陵园.md "wikilink")（[唐山市](../Page/唐山.md "wikilink")，批次II）
  - [察哈尔烈士陵园](../Page/察哈尔烈士陵园.md "wikilink")（[张家口市](../Page/张家口市.md "wikilink")，批次V）
  - [热河烈士纪念馆](../Page/热河烈士纪念馆.md "wikilink")（[承德市](../Page/承德市.md "wikilink")，批次V）
  - [保定烈士陵园](../Page/保定烈士陵园.md "wikilink")（[保定市](../Page/保定市.md "wikilink")，批次V）
  - [晋察冀烈士陵园](../Page/晋察冀烈士陵园.md "wikilink")（[唐县](../Page/唐县.md "wikilink")，批次V）

## 山西

  - [太行](../Page/太行山.md "wikilink")[太岳烈士陵园](../Page/太岳烈士陵园.md "wikilink")（[长治市](../Page/长治市.md "wikilink")，批次I）
  - [刘胡兰](../Page/刘胡兰.md "wikilink")[纪念馆](../Page/刘胡兰纪念馆.md "wikilink")（[文水县](../Page/文水县.md "wikilink")，批次I）
  - [晋绥烈士陵园](../Page/晋绥烈士陵园.md "wikilink")（[兴县](../Page/兴县.md "wikilink")，批次II）
  - [临汾烈士陵园](../Page/临汾烈士陵园.md "wikilink")（[临汾市](../Page/临汾市.md "wikilink")，批次IV）
  - [牛驼寨烈士陵园](../Page/牛驼寨烈士陵园.md "wikilink")（[太原市](../Page/太原市.md "wikilink")，批次V）
  - [李林烈士陵园](../Page/李林烈士陵园.md "wikilink")（[朔州市](../Page/朔州市.md "wikilink")[平鲁区](../Page/平鲁区.md "wikilink")，批次V）

## 内蒙古

  - [大青山](../Page/大青山.md "wikilink")[革命英雄纪念碑](../Page/大青山革命英雄纪念碑.md "wikilink")（[武川县](../Page/武川县.md "wikilink")，批次II）
  - [内蒙古烈士陵园](../Page/内蒙古烈士陵园.md "wikilink")（[呼和浩特市](../Page/呼和浩特市.md "wikilink")，批次IV）
  - [乌兰浩特市烈士陵园](../Page/乌兰浩特市烈士陵园.md "wikilink")（[乌兰浩特市](../Page/乌兰浩特市.md "wikilink")，批次IV）
  - [集宁烈士陵园](../Page/集宁烈士陵园.md "wikilink")（[乌兰察布市](../Page/乌兰察布市.md "wikilink")，批次V）

## 辽宁

  - [辽沈战役](../Page/辽沈战役.md "wikilink")[烈士陵园](../Page/辽沈战役烈士陵园.md "wikilink")（[锦州市](../Page/锦州市.md "wikilink")，批次I）
  - [抗美援朝](../Page/朝鲜战争.md "wikilink")[烈士陵园](../Page/抗美援朝烈士陵园.md "wikilink")（[沈阳市](../Page/沈阳市.md "wikilink")，批次I）
  - [雷锋](../Page/雷锋.md "wikilink")[纪念馆](../Page/抚顺市雷锋纪念馆.md "wikilink")（[抚顺市](../Page/抚顺市.md "wikilink")，批次I）
  - [本溪烈士陵园](../Page/本溪烈士陵园.md "wikilink")（[本溪市](../Page/本溪市.md "wikilink")，批次V）
  - [解放锦州烈士陵园](../Page/解放锦州烈士陵园.md "wikilink")（[锦州市](../Page/锦州市.md "wikilink")，批次V）

## 吉林

  - [四平市烈士陵园](../Page/四平市烈士陵园.md "wikilink")（[四平市](../Page/四平市.md "wikilink")，批次II）
  - [“四保临江”](../Page/四保临江.md "wikilink")[烈士陵园](../Page/四保临江烈士陵园.md "wikilink")（[临江市](../Page/临江市.md "wikilink")，批次II）
  - [杨靖宇](../Page/杨靖宇.md "wikilink")[烈士陵园](../Page/杨靖宇烈士陵园.md "wikilink")（[通化市](../Page/通化市.md "wikilink")，批次I）
  - [延边烈士陵园](../Page/延边烈士陵园.md "wikilink")（[延吉市](../Page/延吉市.md "wikilink")，批次III）
  - [吉林市革命烈士陵园](../Page/吉林市革命烈士陵园.md "wikilink")（[吉林市](../Page/吉林市.md "wikilink")，批次IV）
  - [白城烈士陵园](../Page/白城烈士陵园.md "wikilink")（[白城市](../Page/白城市.md "wikilink")，批次V）

## 黑龙江

  - [饶河](../Page/饶河.md "wikilink")[抗日游击队纪念碑](../Page/饶河抗日游击队纪念碑.md "wikilink")（[饶河县](../Page/饶河县.md "wikilink")，批次I）
  - [哈尔滨烈士陵园](../Page/哈尔滨烈士陵园.md "wikilink")（[哈尔滨市](../Page/哈尔滨市.md "wikilink")，批次I）
  - [“八女投江”](../Page/八女投江.md "wikilink")[烈士群雕](../Page/八女投江烈士群雕.md "wikilink")（[牡丹江市](../Page/牡丹江市.md "wikilink")，批次II）
  - [西满烈士陵园](../Page/西满烈士陵园.md "wikilink")（[齐齐哈尔市](../Page/齐齐哈尔市.md "wikilink")，批次III）
  - [杨子荣](../Page/杨子荣.md "wikilink")[烈士陵园](../Page/杨子荣烈士陵园.md "wikilink")（[海林市](../Page/海林市.md "wikilink")，批次IV）
  - [珍宝岛](../Page/珍宝岛.md "wikilink")[革命烈士陵园](../Page/珍宝岛革命烈士陵园.md "wikilink")（[宝清县](../Page/宝清县.md "wikilink")，批次IV）
  - [佳木斯烈士陵园](../Page/佳木斯烈士陵园.md "wikilink")（[佳木斯市](../Page/佳木斯市.md "wikilink")，批次V）
  - [尚志烈士陵园](../Page/尚志烈士陵园.md "wikilink")（[尚志市](../Page/尚志市.md "wikilink")，批次V）

## 上海

  - [上海市龙华烈士陵园](../Page/上海市龙华烈士陵园.md "wikilink")（[徐汇区](../Page/徐汇区.md "wikilink")，批次II，见注2）
  - [高桥烈士陵园](../Page/高桥烈士陵园.md "wikilink")
    （[浦东新区](../Page/浦东新区.md "wikilink")，批次V）

## 江苏

  - [淮海战役](../Page/淮海战役.md "wikilink")[烈士纪念塔](../Page/淮海战役烈士纪念塔.md "wikilink")（[徐州市](../Page/徐州市.md "wikilink")，批次I）
  - [抗日山](../Page/抗日山.md "wikilink")[烈士陵园](../Page/抗日山烈士陵园.md "wikilink")（[赣榆县](../Page/赣榆县.md "wikilink")，批次II）
  - [常州烈士陵园](../Page/常州烈士陵园.md "wikilink")（[常州市](../Page/常州市.md "wikilink")，批次III)
  - [镇江市烈士陵园](../Page/镇江市烈士陵园.md "wikilink")（[镇江市](../Page/镇江市.md "wikilink")[北固山](../Page/北固山.md "wikilink")，批次IV）
  - [杨根思](../Page/杨根思.md "wikilink")[烈士陵园](../Page/杨根思烈士陵园.md "wikilink")（[泰兴市](../Page/泰兴市.md "wikilink")，批次V）
  - [王杰烈士陵园](../Page/王杰烈士陵园.md "wikilink")（[邳州市](../Page/邳州市.md "wikilink")，批次V）
  - [泗洪烈士陵园](../Page/泗洪烈士陵园.md "wikilink")（[泗洪县](../Page/泗洪县.md "wikilink")，批次V）
  - [扬州烈士陵园](../Page/扬州烈士陵园.md "wikilink")（[扬州市](../Page/扬州市.md "wikilink")，批次V）

## 浙江

  - [浙江革命烈士纪念馆](../Page/浙江革命烈士纪念馆.md "wikilink")（[杭州市](../Page/杭州市.md "wikilink")，增列II）
  - [四明山烈士陵园](../Page/四明山烈士陵园.md "wikilink")（[余姚市](../Page/余姚市.md "wikilink")，批次III）
  - [解放一江山岛烈士陵园](../Page/解放一江山岛烈士陵园.md "wikilink")（[台州市](../Page/台州市.md "wikilink")，批次III）　
  - [温州市革命烈士纪念馆](../Page/温州市革命烈士纪念馆.md "wikilink")（[温州市](../Page/温州市.md "wikilink")，批次IV）

## 安徽

  - [皖西烈士陵园](../Page/皖西烈士陵园.md "wikilink")（[六安市](../Page/六安市.md "wikilink")，批次II）
  - [安徽革命烈士事迹陈列馆](../Page/安徽革命烈士事迹陈列馆.md "wikilink")（[合肥市](../Page/合肥市.md "wikilink")，批次II）
  - [皖南事变烈士纪念馆](../Page/皖南事变烈士纪念馆.md "wikilink")（[泾县](../Page/泾县.md "wikilink")，增列I)
  - [金寨县烈士陵园](../Page/金寨县烈士陵园.md "wikilink")（[金寨县](../Page/金寨县.md "wikilink")，批次III)
  - [宿州烈士陵园](../Page/宿州烈士陵园.md "wikilink")（[宿州市](../Page/宿州市.md "wikilink")，批次V）
  - [皖东烈士陵园](../Page/皖东烈士陵园.md "wikilink")（[来安县](../Page/来安县.md "wikilink")，批次V）
  - [双堆集烈士陵园](../Page/双堆集烈士陵园.md "wikilink")（[濉溪县](../Page/濉溪县.md "wikilink")，批次V）
  - [大别山烈士陵园](../Page/大别山烈士陵园.md "wikilink")（[岳西县](../Page/岳西县.md "wikilink")，批次V）

## 福建

  - [瞿秋白](../Page/瞿秋白.md "wikilink")[烈士纪念碑](../Page/瞿秋白烈士纪念碑.md "wikilink")（[长汀县](../Page/长汀县.md "wikilink")，批次I）
  - [闽西革命烈士陵园](../Page/闽西革命烈士陵园.md "wikilink")（[龙岩市](../Page/龙岩市.md "wikilink")，批次II）
  - [林祥谦](../Page/林祥谦.md "wikilink")[烈士陵园](../Page/林祥谦烈士陵园.md "wikilink")（[闽侯县](../Page/闽侯县.md "wikilink")，批次II）
  - [闽中革命烈士陵园](../Page/闽中革命烈士陵园.md "wikilink")（[莆田市](../Page/莆田市.md "wikilink")，批次IV）
  - [东山战斗](../Page/东山战斗.md "wikilink")[烈士陵园](../Page/东山战斗烈士陵园.md "wikilink")（[东山县](../Page/东山县.md "wikilink")，批次V）
  - [厦门烈士陵园](../Page/厦门烈士陵园.md "wikilink")（[厦门市](../Page/厦门市.md "wikilink")，批次V）

## 江西

  - [红军](../Page/红军.md "wikilink")[烈士纪念塔](../Page/红军烈士纪念塔.md "wikilink")（[瑞金市](../Page/瑞金市.md "wikilink")，批次I）
  - [井冈山](../Page/井冈山.md "wikilink")[革命烈士纪念塔](../Page/井冈山革命烈士纪念塔.md "wikilink")（[井冈山市](../Page/井冈山市.md "wikilink")，批次I）
  - [江西省](../Page/江西.md "wikilink")[革命烈士纪念堂](../Page/江西省革命烈士纪念堂.md "wikilink")（[南昌市](../Page/南昌市.md "wikilink")，批次I）
  - [方志敏](../Page/方志敏.md "wikilink")[烈士墓](../Page/方志敏烈士墓.md "wikilink")（[南昌市](../Page/南昌市.md "wikilink")，批次I）
  - [茅家岭](../Page/茅家岭.md "wikilink")[烈士陵园](../Page/茅家岭烈士陵园.md "wikilink")（[上饶市](../Page/上饶市.md "wikilink")，批次II）
  - [兴国县烈士陵园](../Page/兴国县烈士陵园.md "wikilink")（[兴国县](../Page/兴国县.md "wikilink")，批次III）
  - [于都烈士纪念馆](../Page/于都烈士纪念馆.md "wikilink")（[于都县](../Page/于都县.md "wikilink")，批次V）
  - [卢德铭](../Page/卢德铭.md "wikilink")[烈士陵园](../Page/卢德铭烈士陵园.md "wikilink")（[萍乡市](../Page/萍乡市.md "wikilink")，批次V）
  - [吉安烈士纪念馆](../Page/吉安烈士纪念馆.md "wikilink")（[吉安县](../Page/吉安县.md "wikilink")，批次V）

## 山东

  - [华东](../Page/华东.md "wikilink")[革命烈士陵园](../Page/华东革命烈士陵园.md "wikilink")（[临沂市](../Page/临沂市.md "wikilink")，批次I）
  - [济南革命烈士陵园](../Page/济南革命烈士陵园.md "wikilink")（[济南市](../Page/济南市.md "wikilink")，批次II）
  - [胶东](../Page/胶东.md "wikilink")[革命烈士陵园](../Page/胶东革命烈士陵园.md "wikilink")（[栖霞县](../Page/栖霞县.md "wikilink")，批次II）
  - [孟良崮战役](../Page/孟良崮战役.md "wikilink")[烈士陵园](../Page/孟良崮战役烈士陵园.md "wikilink")（[蒙阴县](../Page/蒙阴县.md "wikilink")，批次II）
  - [青岛市革命烈士纪念馆](../Page/青岛市革命烈士纪念馆.md "wikilink")（[青岛市](../Page/青岛市.md "wikilink")，增列II）
  - [羊山革命烈士陵园](../Page/羊山革命烈士陵园.md "wikilink")（[金乡县](../Page/金乡县.md "wikilink")，批次IV）
  - [湖西](../Page/湖西專區.md "wikilink")[革命烈士陵园](../Page/湖西革命烈士陵园.md "wikilink")（[单县](../Page/单县.md "wikilink")，批次IV）
  - [莱芜战役](../Page/莱芜战役.md "wikilink")[纪念馆](../Page/莱芜战役纪念馆.md "wikilink")（[莱芜市](../Page/莱芜市.md "wikilink")，批次IV）
  - [冀鲁豫边区](../Page/冀鲁豫边区.md "wikilink")[纪念馆](../Page/冀鲁豫边区纪念馆.md "wikilink")（[菏泽市](../Page/菏泽市.md "wikilink")，批次V）
  - [渤海革命烈士陵园](../Page/渤海革命烈士陵园.md "wikilink")（[滨州市](../Page/滨州市.md "wikilink")，批次V）
  - [青岛莱西革命烈士陵园](../Page/青岛莱西革命烈士陵园.md "wikilink")（[莱西市](../Page/莱西市.md "wikilink")，批次V）
  - [淄博革命烈士陵园](../Page/淄博革命烈士陵园.md "wikilink")（[淄博市](../Page/淄博市.md "wikilink")，批次V）
  - [泰安革命烈士陵园](../Page/泰安革命烈士陵园.md "wikilink")（[泰安市](../Page/泰安市.md "wikilink")，批次V）
  - [潍坊革命烈士陵园](../Page/潍坊革命烈士陵园.md "wikilink")（[潍坊市](../Page/潍坊市.md "wikilink")，批次V）
  - [荣成革命烈士陵园](../Page/荣成革命烈士陵园.md "wikilink")（[荣成市](../Page/荣成市.md "wikilink")，批次V）

## 河南

  - [竹沟](../Page/竹沟.md "wikilink")[革命烈士陵园](../Page/竹沟革命烈士陵园.md "wikilink")（[确山县](../Page/确山县.md "wikilink")，批次I）
  - [鄂豫皖苏区](../Page/鄂豫皖苏区.md "wikilink")[革命烈士陵园](../Page/鄂豫皖苏区革命烈士陵园.md "wikilink")（[新县](../Page/新县.md "wikilink")，批次II）
  - [焦裕禄烈士陵园](../Page/焦裕禄烈士墓.md "wikilink")（[兰考县](../Page/兰考县.md "wikilink")，增列II）
  - [郑州烈士陵园](../Page/郑州烈士陵园.md "wikilink")（[郑州市](../Page/郑州市.md "wikilink")，批次III）
  - [淮海战役陈官庄烈士陵园](../Page/淮海战役陈官庄烈士陵园.md "wikilink")（[永城市](../Page/永城市.md "wikilink")，批次III）
  - [开封市烈士陵园](../Page/开封市烈士陵园.md "wikilink")（[开封市](../Page/开封市.md "wikilink")，批次IV）
  - [洛阳烈士陵园](../Page/洛阳烈士陵园.md "wikilink")（[洛阳市](../Page/洛阳市.md "wikilink")，批次V）
  - [林州烈士陵园](../Page/林州烈士陵园.md "wikilink")（[林州市](../Page/林州市.md "wikilink")，批次V）

## 湖北

  - [湘鄂西苏区](../Page/湘鄂西苏区.md "wikilink")[革命烈士陵园](../Page/湘鄂西苏区革命烈士陵园.md "wikilink")（[洪湖市](../Page/洪湖市.md "wikilink")，批次I）
  - [鄂豫边区](../Page/鄂豫边区.md "wikilink")[革命烈士陵园](../Page/鄂豫边区革命烈士陵园.md "wikilink")（[大悟县](../Page/大悟县.md "wikilink")，批次I）
  - [黄麻起义和鄂豫皖苏区革命烈士陵园](../Page/黄麻起义和鄂豫皖苏区革命烈士陵园.md "wikilink")（[红安县](../Page/红安县.md "wikilink")，批次II）
  - [湘鄂赣边区鄂东南革命烈士陵园](../Page/湘鄂赣边区鄂东南革命烈士陵园.md "wikilink")（[阳新县](../Page/阳新县.md "wikilink")，批次II）
  - [向警予](../Page/向警予.md "wikilink")[烈士陵园](../Page/向警予烈士陵园.md "wikilink")（[武汉市](../Page/武汉市.md "wikilink")，批次II）
  - [施洋](../Page/施洋.md "wikilink")[烈士陵园](../Page/施洋烈士陵园.md "wikilink")（[武汉市](../Page/武汉市.md "wikilink")，批次II）
  - [湘鄂边苏区](../Page/湘鄂边苏区.md "wikilink")[革命烈士陵园](../Page/湘鄂边苏区鹤峰革命烈士陵园.md "wikilink")（[鹤峰县](../Page/鹤峰县.md "wikilink")，批次IV）
  - [麻城烈士陵园](../Page/麻城烈士陵园.md "wikilink")（[麻城市](../Page/麻城市.md "wikilink")，批次V）
  - [宜昌烈士陵园](../Page/宜昌烈士陵园.md "wikilink")（[宜昌市](../Page/宜昌市.md "wikilink")，批次V）
  - [孝感烈士陵园](../Page/孝感烈士陵园.md "wikilink")（[孝感市](../Page/孝感市.md "wikilink")，批次V）

## 湖南

  - [湖南烈士公园](../Page/湖南烈士公园.md "wikilink")（[长沙市](../Page/长沙市.md "wikilink")，批次II）
  - [韶山烈士陵园](../Page/韶山烈士陵园.md "wikilink")（[韶山市](../Page/韶山市.md "wikilink")，批次III）
  - [华容烈士陵园](../Page/华容烈士陵园.md "wikilink")（[华容县](../Page/华容县.md "wikilink")，批次V）
  - [平江烈士陵园](../Page/平江烈士陵园.md "wikilink")（[平江县](../Page/平江县.md "wikilink")，批次V）
  - [湘西剿匪](../Page/湘西剿匪.md "wikilink")[烈士纪念园](../Page/湘西剿匪烈士纪念园.md "wikilink")（[沅陵县](../Page/沅陵县.md "wikilink")，批次V）
  - [醴陵烈士陵园](../Page/醴陵烈士陵园.md "wikilink")（[醴陵市](../Page/醴陵市.md "wikilink")，批次V）
  - [衡阳烈士陵园](../Page/衡阳烈士陵园.md "wikilink")（[衡阳市](../Page/衡阳市.md "wikilink")，批次V）
  - [湖南革命陵园](../Page/湖南革命陵园.md "wikilink")（[长沙市](../Page/长沙市.md "wikilink")，批次V）

## 广东

  - [广州起义](../Page/广州起义.md "wikilink")[烈士陵园](../Page/广州起义烈士陵园.md "wikilink")（[广州市](../Page/广州市.md "wikilink")，批次I）
  - [海丰县烈士陵园](../Page/海丰县烈士陵园.md "wikilink")（[海丰县](../Page/海丰县.md "wikilink")，批次II）　　　　　　　　　
  - [十九路军](../Page/十九路军.md "wikilink")[淞沪抗日阵亡将士陵园](../Page/十九路军淞沪抗日阵亡将士陵园.md "wikilink")（[广州市](../Page/广州市.md "wikilink")，批次II）
    　　
  - [“八一”起义军](../Page/八一起义军.md "wikilink")[三河坝战役](../Page/三河坝战役.md "wikilink")[烈士纪念碑](../Page/“八一”起义军三河坝战役烈士纪念碑.md "wikilink")（[大埔县](../Page/大埔县.md "wikilink")，批次IV）
  - [河源烈士陵园](../Page/河源烈士陵园.md "wikilink")（[河源市](../Page/河源市.md "wikilink")，批次V）

## 广西

  - [广西壮族自治区烈士陵园](../Page/广西壮族自治区烈士陵园.md "wikilink")（[南宁市](../Page/南宁市.md "wikilink")，批次I）
  - [东兰烈士陵园](../Page/东兰烈士陵园.md "wikilink")（[东兰县](../Page/东兰县.md "wikilink")，批次I）
  - [百色起义](../Page/百色起义.md "wikilink")[烈士碑园](../Page/百色起义烈士碑园.md "wikilink")（[百色市](../Page/百色市.md "wikilink")，批次III）
  - [突破湘江](../Page/突破湘江.md "wikilink")[烈士纪念碑园](../Page/突破湘江烈士纪念碑园.md "wikilink")（[兴安县](../Page/兴安县.md "wikilink")，批次III）
  - [柳州烈士陵园](../Page/柳州烈士陵园.md "wikilink")（[柳州市](../Page/柳州市.md "wikilink")，批次V）

## 海南

  - [六连岭](../Page/六连岭.md "wikilink")[烈士纪念碑阵亡将士陵园](../Page/六连岭烈士纪念碑阵亡将士陵园.md "wikilink")（[万宁县](../Page/万宁县.md "wikilink")，批次II）
  - [李硕勋](../Page/李硕勋.md "wikilink")[烈士纪念亭](../Page/李硕勋烈士纪念亭.md "wikilink")（[海口市](../Page/海口市.md "wikilink")，批次II）
  - [白沙起义](../Page/白沙起义.md "wikilink")[烈士纪念碑园](../Page/白沙起义烈士纪念碑园.md "wikilink")（[琼中黎族苗族自治县](../Page/琼中黎族苗族自治县.md "wikilink")，批次V）
  - [解放海南岛战役](../Page/解放海南岛战役.md "wikilink")[烈士陵园](../Page/解放海南岛战役烈士陵园.md "wikilink")（[海口市](../Page/海口市.md "wikilink")，批次V）

## 重庆

  - [张自忠](../Page/张自忠.md "wikilink")[烈士陵园](../Page/张自忠烈士陵园.md "wikilink")（[北碚区](../Page/北碚区.md "wikilink")，批次I）
  - [邱少云](../Page/邱少云.md "wikilink")[烈士纪念馆](../Page/邱少云烈士纪念馆.md "wikilink")（[铜梁县](../Page/铜梁县.md "wikilink")，批次III）
  - [杨暗公](../Page/杨暗公.md "wikilink")[烈士陵园](../Page/杨暗公烈士陵园.md "wikilink")（[潼南县](../Page/潼南县.md "wikilink")，批次III）
  - [万州革命烈士陵园](../Page/万州革命烈士陵园.md "wikilink")（[万州区](../Page/万州区.md "wikilink")，批次IV）
  - [库里申科](../Page/库里申科.md "wikilink")[烈士墓园](../Page/库里申科烈士墓园.md "wikilink")（[万州区](../Page/万州区.md "wikilink")，批次V）

## 四川

  - [自贡市烈士陵园](../Page/自贡市烈士陵园.md "wikilink")（[自贡市](../Page/自贡市.md "wikilink")，批次I）
  - [王坪](../Page/王坪.md "wikilink")[烈士陵园](../Page/王坪烈士陵园.md "wikilink")（[通江县](../Page/通江县.md "wikilink")，批次II）
  - [南充烈士陵园](../Page/南充烈士陵园.md "wikilink")（[南充市](../Page/南充市.md "wikilink")，批次IV）
  - [遂宁烈士陵园](../Page/遂宁烈士陵园.md "wikilink")（[遂宁市](../Page/遂宁市.md "wikilink")，批次V）
  - [雅安烈士陵园](../Page/雅安烈士陵园.md "wikilink")（[雅安市](../Page/雅安市.md "wikilink")，批次V）

## 贵州

  - [遵义红军烈士陵园](../Page/遵义红军烈士陵园.md "wikilink")（[遵义市](../Page/遵义市.md "wikilink")，批次I）
  - [毕节市烈士陵园](../Page/毕节市烈士陵园.md "wikilink")（[毕节市](../Page/毕节市.md "wikilink")，批次IV）
  - [黎平烈士陵园](../Page/黎平烈士陵园.md "wikilink")（[黎平县](../Page/黎平县.md "wikilink")，批次V）

## 云南

  - [威信扎西红军烈士陵园](../Page/威信扎西红军烈士陵园.md "wikilink")（[威信县](../Page/威信县.md "wikilink")，批次III）
  - [屏边烈士陵园](../Page/屏边烈士陵园.md "wikilink")（[屏边苗族自治县](../Page/屏边苗族自治县.md "wikilink")，批次III）
  - [虎头山烈士陵园](../Page/虎头山烈士陵园.md "wikilink")（[宣威市](../Page/宣威市.md "wikilink")，批次V）

## 西藏

  - [山南烈士陵园](../Page/山南烈士陵园.md "wikilink")（[乃东县](../Page/乃东县.md "wikilink")，批次II）

## 陕西

  - [“四八”烈士陵园](../Page/四八烈士陵园.md "wikilink")（[延安市](../Page/延安市.md "wikilink")，批次I）
  - [刘志丹](../Page/刘志丹.md "wikilink")[烈士陵园](../Page/刘志丹烈士陵园.md "wikilink")（[志丹县](../Page/志丹县.md "wikilink")，批次I）
  - [子长革命烈士纪念馆](../Page/子长革命烈士纪念馆.md "wikilink")（[子长县](../Page/子长县.md "wikilink")，批次II）
  - [杨虎城](../Page/杨虎城.md "wikilink")[烈士陵园](../Page/杨虎城烈士陵园.md "wikilink")（[长安区](../Page/长安区_\(西安市\).md "wikilink")，批次II）
  - [永丰革命烈士陵园](../Page/永丰革命烈士陵园.md "wikilink")（[蒲城县](../Page/蒲城县.md "wikilink")，批次IV）
  - [扶眉战役](../Page/扶眉战役.md "wikilink")[烈士陵园](../Page/扶眉战役烈士陵园.md "wikilink")（[眉县](../Page/眉县.md "wikilink")，批次V）
  - [西安烈士陵园](../Page/西安烈士陵园.md "wikilink")（[西安市](../Page/西安市.md "wikilink")，批次V）
  - [瓦子街战役](../Page/瓦子街战役.md "wikilink")[烈士陵园](../Page/瓦子街战役烈士陵园.md "wikilink")（[黄龙县](../Page/黄龙县.md "wikilink")，批次V）

## 甘肃

  - [兰州市烈士陵园](../Page/兰州市烈士陵园.md "wikilink")（[兰州市](../Page/兰州市.md "wikilink")，批次II）
  - [高台烈士陵园](../Page/高台烈士陵园.md "wikilink")（[高台县](../Page/高台县.md "wikilink")，批次II）
  - [红西路军](../Page/红西路军.md "wikilink")[临泽烈士陵园](../Page/红西路军临泽烈士陵园.md "wikilink")（[临泽县](../Page/临泽县.md "wikilink")，批次V）

## 青海

  - [西宁市烈士陵园](../Page/西宁市烈士陵园.md "wikilink")（[西宁市](../Page/西宁市.md "wikilink")，批次II）
  - [循化烈士陵园](../Page/循化烈士陵园.md "wikilink")（[循化县](../Page/循化县.md "wikilink")，批次V）

## 宁夏

  - [任山河](../Page/任山河.md "wikilink")[烈士陵园](../Page/任山河烈士陵园.md "wikilink")（[彭阳县](../Page/彭阳县.md "wikilink")，批次III）
  - [吴忠涝河桥烈士陵园](../Page/吴忠涝河桥烈士陵园.md "wikilink")（[吴忠市](../Page/吴忠市.md "wikilink")，批次V）
  - [同心烈士陵园](../Page/同心烈士陵园.md "wikilink")（[同心县](../Page/同心县.md "wikilink")，批次V）

## 新疆

  - [乌鲁木齐烈士陵园](../Page/乌鲁木齐烈士陵园.md "wikilink")（[乌鲁木齐市](../Page/乌鲁木齐市.md "wikilink")，批次I）
  - [伊宁烈士陵园](../Page/伊宁烈士陵园.md "wikilink")（[伊宁市](../Page/伊宁市.md "wikilink")，批次II）
  - [伊吾县烈士陵园](../Page/伊吾县烈士陵园.md "wikilink")（[伊吾县](../Page/伊吾县.md "wikilink")，批次IV
    ）
  - [叶城烈士陵园](../Page/叶城烈士陵园.md "wikilink")（[叶城县](../Page/叶城县.md "wikilink")，批次V）

### 新疆生产建设兵团

  - [孙龙珍](../Page/孙龙珍.md "wikilink")[军垦烈士陵园](../Page/孙龙珍军垦烈士陵园.md "wikilink")（[农九师](../Page/新疆生产建设兵团#所属各师.md "wikilink")，批次V）

## 备注

  - 注1：根据民发〔2009〕36号文[1](https://web.archive.org/web/20150603045749/http://www.mca.gov.cn/article/zwgk/fvfg/yfaz/200906/20090600031449.shtml)，正式公布的单位仅有64处，但民政部另一说法为71处[2](https://web.archive.org/web/20150603045806/http://www.mca.gov.cn/article/zwgk/mzyw/200903/20090300029002.shtml)，单位总数也据此增至181处，请自行判断。
  - 注2：原“上海市烈士陵园”迁“龙华烈士陵园”建“上海市龙华烈士陵园”，仍为全国重点保护单位。

## 参见

  - [中国抗日战争阵亡将士纪念建筑物](../Page/中国抗日战争阵亡将士纪念建筑物.md "wikilink")

{{-}}

[中华人民共和国烈士纪念设施](../Category/中华人民共和国烈士纪念设施.md "wikilink")
[Category:中文地名索引](../Category/中文地名索引.md "wikilink")
[Category:中国建筑物列表](../Category/中国建筑物列表.md "wikilink")
[Category:中华人民共和国列表](../Category/中华人民共和国列表.md "wikilink")