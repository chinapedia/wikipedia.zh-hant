[TaoMountain.jpg](https://zh.wikipedia.org/wiki/File:TaoMountain.jpg "fig:TaoMountain.jpg")

**桃山**位於[武陵農場北方](../Page/武陵農場.md "wikilink")，屬[雪山山脈](../Page/雪山山脈.md "wikilink")，海拔高度3,325公尺，為[武陵四秀](../Page/武陵四秀.md "wikilink")、[台灣百岳之一](../Page/台灣百岳.md "wikilink")，編號第48；稜線是[新竹縣](../Page/新竹縣.md "wikilink")[尖石鄉與](../Page/尖石鄉.md "wikilink")[台中市](../Page/台中市.md "wikilink")[和平區的分界嶺](../Page/和平區_\(臺中市\).md "wikilink")，山頂有三等[三角點](../Page/三角點.md "wikilink")6327號。在泰雅語中稱「**Babo
Kaba**」，意為「拳頭山」\[1\]，而山形自西南側展望狀似[桃子](../Page/桃子.md "wikilink")，故名。

## 攀登路線

要攀登桃山，可經由武陵四秀路線往品田山及池有山登山口開始攀登，在三岔營地後右轉攀登桃山，之後可順登武陵四秀中的喀拉業山，另一路線可由詩崙溪和桃山西溪之登山口起攀，約4.5k到達桃山山頂，可順登詩崙山及喀拉業山，自桃山山頂至喀拉業山輕裝來回約四至五小時。

## 參考資料

<div class="references-small">

<references />

</div>

## 相關條目

  - [雪霸國家公園](../Page/雪霸國家公園.md "wikilink")
  - [台灣百岳](../Page/台灣百岳.md "wikilink")
  - [聖稜線](../Page/聖稜線.md "wikilink")
  - [登山](../Page/登山.md "wikilink")
  - [桃山瀑布](../Page/桃山瀑布.md "wikilink")

[Category:新竹縣山峰](../Category/新竹縣山峰.md "wikilink")
[Category:台中市山峰](../Category/台中市山峰.md "wikilink")
[Category:雪霸國家公園](../Category/雪霸國家公園.md "wikilink")

1.  第二世代台灣百岳全集上冊，戶外生活圖書股份有限公司，陳遠見主編，2007年8月31日初版