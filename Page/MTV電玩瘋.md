《**MTV電玩瘋**》原是[MTV中文台與](../Page/音樂電視網.md "wikilink")[巴哈姆特電玩資訊站合作推出的](../Page/巴哈姆特電玩資訊站.md "wikilink")[ACG資訊節目](../Page/ACG.md "wikilink")《MTV巴哈姆特電玩瘋》，已於2007年10月27日第28集播畢。第29集起改為MTV獨立製作，與巴哈姆特電玩資訊站停止合作，名稱改為《**MTV電玩瘋**》繼續播出；此時期[臺灣電視公司購買播映權後於](../Page/臺灣電視公司.md "wikilink")[台視主頻播出](../Page/台視主頻.md "wikilink")，但標題畫面以圓形「TTV」字樣覆蓋「電玩瘋」三字左邊的MTV[台標](../Page/台標.md "wikilink")。內容結合Anime([動畫](../Page/動畫.md "wikilink"))、Comic([漫畫](../Page/漫畫.md "wikilink"))及Game([遊戲](../Page/遊戲.md "wikilink"))資訊，提供玩家最多元的電玩訊息。已播出43集以及多集精華版，目前已停播。

## 播出時間

  - 2007年4月21日-10月27日：《MTV巴哈姆特電玩瘋》

首播:MTV每週六17:00-18:00
重播:MTV每週六22:00-23:00、每週日14:00-15:00、每週二13:00-14:00、每週四22:00-23:00

  - 2007年：《MTV電玩瘋》

首播:[台視主頻每週六](../Page/台視主頻.md "wikilink")16:30-17:30
重播:MTV每週六18:00-19:00、每週日14:00-15:00、每週日17:00-18:00

## 主持人

[2008TaipeiGameShow_Day3_Insrea_MTVGamer_Akina_Flora.jpg](https://zh.wikipedia.org/wiki/File:2008TaipeiGameShow_Day3_Insrea_MTVGamer_Akina_Flora.jpg "fig:2008TaipeiGameShow_Day3_Insrea_MTVGamer_Akina_Flora.jpg")的《MTV電玩瘋》主持人，左起：小貓、子寧\]\]

  - [王子寧](../Page/王子寧.md "wikilink") 節目中暱稱:子寧 (電玩小天使)
  - [陳思瑾](../Page/陳思瑾.md "wikilink") 節目中暱稱:小貓 (電玩小可愛)
  - [盧宛辰](../Page/盧宛辰.md "wikilink") 節目中暱稱:小蜜 (電玩小甜心)
  - [俞曉帆](../Page/俞曉帆.md "wikilink") 節目中暱稱:毛毛 (電玩小精靈)

### 主持人票選過程

  - 2006年底進行報名
  - 2007年初進行第一波刪選
  - 2007年1月底舉辦試鏡選出12名候選人
  - 2007年[農曆過年期間進行網路決賽票選](../Page/農曆過年.md "wikilink")，票選期間MTV播出票選之訊息[廣告](../Page/廣告.md "wikilink")
  - 2007年農曆過年後網路決賽票選岀四位主持人

### 主持培訓課程

| 培訓課程   | 培訓講師                                                                      |
| ------ | ------------------------------------------------------------------------- |
| 電玩知識   | 【巴哈姆特電玩資訊站副站長】Linger                                                      |
| 棚內實習主持 | 【MTV中文台節目製作人】安東尼                                                          |
| 舞蹈肢體訓練 | 【Unity舞道館】藍波                                                              |
| 英語正音   | 【[中廣音樂網專業](../Page/中廣音樂網.md "wikilink")[DJ](../Page/DJ.md "wikilink")】鄭開來 |
| 國語正音   | 【[Hit FM專業DJ](../Page/Hit_FM.md "wikilink")】ELSA                          |
| 美姿美儀訓練 | 【[伊林模特兒經紀公司教育訓練經理](../Page/伊林模特兒經紀公司.md "wikilink")】蔡玉治                   |

## 節目單元

  - 新GAME特區
  - 電玩突擊隊
  - 巴哈神人榜
  - 電玩名人堂
  - 八大系列遊戲
  - 達人攻略
  - 名作特輯
  - 特別企劃

## 節目中使用音效

  - TM Revolution - Invoke(Gundam Seed Opening) 00:11至00:17為各單元開頭所播放之音效

## 活動

  - 名稱:MTV電玩瘋美少女見面會\[1\]

時間:2008/1/26 下午2點25分至2點55分
地點:[台北世貿一館](../Page/台北世貿.md "wikilink")[因思銳遊戲總局攤位](../Page/因思銳遊戲總局.md "wikilink")

  - 名稱:MTV電玩瘋美少女超級任務\[2\]

時間:2008/1/24\~2008/1/28
地點:台北世貿一館

## 參考文獻

## 外部連結

  - [MTV巴哈姆特電玩瘋](http://prj.gamer.com.tw/gamecrazy/)
  - [MTV電玩瘋](https://web.archive.org/web/20071015051758/http://www.mtvchinese.com/Channel/Show/MTVGamecrazy/Index.html)

[MTV](../Category/停播台灣綜藝節目.md "wikilink")
[MTV](../Category/MTV電視節目.md "wikilink")
[MTV](../Category/台視外購電視節目.md "wikilink")
[MTV](../Category/電玩節目.md "wikilink")

1.
2.