[Italian_provinces_no_regions.svg](https://zh.wikipedia.org/wiki/File:Italian_provinces_no_regions.svg "fig:Italian_provinces_no_regions.svg")

**省**（[意大利语](../Page/意大利语.md "wikilink")：****）是[意大利的第二级行政区划](../Page/意大利.md "wikilink")，義大利共有110個省\[1\]。

## 歷史

## 分區

  - [阿布鲁佐大区](../Page/阿布鲁佐大区.md "wikilink")（Abruzzo）
      - [基耶蒂省](../Page/基耶蒂省.md "wikilink") Chieti
      - [阿奎拉省](../Page/阿奎拉省.md "wikilink") L'Aquila
      - [佩斯卡拉省](../Page/佩斯卡拉省.md "wikilink") Pescara
      - [泰拉莫省](../Page/泰拉莫省.md "wikilink") Teramo
  - [瓦莱达奥斯塔大区](../Page/瓦莱达奥斯塔大区.md "wikilink")（Valle d'Aosta）
      - [奥斯塔省](../Page/奥斯塔省.md "wikilink") Aosta
  - [普利亚大区](../Page/普利亚大区.md "wikilink")（Puglia）
      - [巴-{里}-省](../Page/巴里省.md "wikilink") Bari
      - [巴列塔-安德里亚-特拉尼省](../Page/巴列塔-安德里亚-特拉尼省.md "wikilink")
        Barletta-Andria-Trani
      - [布林迪西省](../Page/布林迪西省.md "wikilink") Brindisi
      - [福贾省](../Page/福贾省.md "wikilink") Foggia
      - [莱切省](../Page/莱切省.md "wikilink") Lecce
      - [塔兰托省](../Page/塔兰托省.md "wikilink") Taranto
  - [巴西利卡塔大区](../Page/巴西利卡塔大区.md "wikilink")（Basilicata）
      - [马泰拉省](../Page/马泰拉省.md "wikilink") Matera
      - [波坦察省](../Page/波坦察省.md "wikilink") Potenza
  - [卡拉布里亚大区](../Page/卡拉布里亚大区.md "wikilink")（Calabria）
      - [卡坦扎罗省](../Page/卡坦扎罗省.md "wikilink") Catanzaro
      - [科森扎省](../Page/科森扎省.md "wikilink") Cosenza
      - [克罗托内省](../Page/克罗托内省.md "wikilink") Crotone
      - [雷焦卡拉布里亚省](../Page/雷焦卡拉布里亚省.md "wikilink") Reggio Calabria
      - [维博瓦伦蒂亚省](../Page/维博瓦伦蒂亚省.md "wikilink") Vibo Valentia
  - [坎帕尼亚大区](../Page/坎帕尼亚大区.md "wikilink")（Campania）
      - [阿韦利诺省](../Page/阿韦利诺省.md "wikilink") Avellino
      - [贝内文托省](../Page/贝内文托省.md "wikilink") Benevento
      - [卡塞塔省](../Page/卡塞塔省.md "wikilink") Caserta
      - [那不勒斯省](../Page/那不勒斯省.md "wikilink") Napoli
      - [萨莱诺省](../Page/萨莱诺省.md "wikilink") Salerno
  - [艾米利亚-罗马涅大区](../Page/艾米利亚-罗马涅大区.md "wikilink")（Emilia-Romagna）
      - [博洛尼亚省](../Page/博洛尼亚省.md "wikilink") Bologna
      - [费拉拉省](../Page/费拉拉省.md "wikilink") Ferrara
      - [弗利-切塞纳省](../Page/弗利-切塞纳省.md "wikilink") Forlì-Cesena
      - [摩德纳省](../Page/摩德纳省.md "wikilink") Modena
      - [帕尔马省](../Page/帕尔马省.md "wikilink") Parma
      - [皮亚琴察省](../Page/皮亚琴察省.md "wikilink") Piacenza
      - [拉韦纳省](../Page/拉韦纳省.md "wikilink") Ravenna
      - [雷焦艾米利亚省](../Page/雷焦艾米利亚省.md "wikilink") Reggio Emilia
      - [里米尼省](../Page/里米尼省.md "wikilink") Rimini
  - [弗留利-威尼斯朱利亚大区](../Page/弗留利-威尼斯朱利亚大区.md "wikilink")（Friuli-Venezia
    Giulia）
      - [戈里齐亚省](../Page/戈里齐亚省.md "wikilink") Gorizia
      - [波代诺内省](../Page/波代诺内省.md "wikilink") Pordenone
      - [的里雅斯特省](../Page/的里雅斯特省.md "wikilink") Trieste
      - [乌迪内省](../Page/乌迪内省.md "wikilink") Udine
  - [拉齐奥大区](../Page/拉齐奥大区.md "wikilink")（Lazio）
      - [弗罗西诺内省](../Page/弗罗西诺内省.md "wikilink") Frosinone
      - [拉蒂纳省](../Page/拉蒂纳省.md "wikilink") Latina
      - [列蒂省](../Page/列蒂省.md "wikilink") Rieti
      - [罗马省](../Page/罗马省.md "wikilink") Roma
      - [维泰博省](../Page/维泰博省.md "wikilink") Viterbo
  - [利古里亚大区](../Page/利古里亚大区.md "wikilink")（Liguria）
      - [热那亚省](../Page/热那亚省.md "wikilink") Genova
      - [因佩里亚省](../Page/因佩里亚省.md "wikilink") Imperia
      - [拉斯佩齐亚省](../Page/拉斯佩齐亚省.md "wikilink") Provincia della Spezia
      - [萨沃纳省](../Page/萨沃纳省.md "wikilink") Savona
  - [伦巴第大区](../Page/伦巴第大区.md "wikilink")（Lombardia）
      - [贝加莫省](../Page/贝加莫省.md "wikilink") Bergamo
      - [-{布}-雷西亚省](../Page/布雷西亚省.md "wikilink") Brescia
      - [科莫省](../Page/科莫省.md "wikilink") Como
      - [克雷莫纳省](../Page/克雷莫纳省.md "wikilink") Cremona
      - [莱科省](../Page/莱科省.md "wikilink") Lecco
      - [洛迪省](../Page/洛迪省.md "wikilink") Lodi
      - [曼图亚省](../Page/曼图亚省.md "wikilink") Mantova
      - [米兰省](../Page/米兰省.md "wikilink") Milano
      - [蒙扎和布里安扎省](../Page/蒙扎和布里安扎省.md "wikilink") Monza e della Brianza
      - [帕维亚省](../Page/帕维亚省.md "wikilink") Pavia
      - [松德里奥省](../Page/松德里奥省.md "wikilink") Sondrio
      - [瓦雷泽省](../Page/瓦雷泽省.md "wikilink") Varese
  - [马尔凯大区](../Page/马尔凯大区.md "wikilink")（Marche）
      - [安科纳省](../Page/安科纳省.md "wikilink") Ancona
      - [阿斯科利皮切诺省](../Page/阿斯科利皮切诺省.md "wikilink") Ascoli Piceno
      - [费尔莫省](../Page/费尔莫省.md "wikilink") Fermo
      - [马切拉塔省](../Page/马切拉塔省.md "wikilink") Macerata
      - [佩萨罗和乌尔比诺省](../Page/佩萨罗和乌尔比诺省.md "wikilink") Pesaro e Urbino
  - [莫利塞大区](../Page/莫利塞大区.md "wikilink")（Molise）
      - [坎波巴索省](../Page/坎波巴索省.md "wikilink") Campobasso
      - [伊塞尔尼亚省](../Page/伊塞尔尼亚省.md "wikilink") Isernia
  - [皮埃蒙特大区](../Page/皮埃蒙特大区.md "wikilink")（Piemonte）
      - [亚历山德里亚省](../Page/亚历山德里亚省.md "wikilink") Alessandria
      - [阿斯蒂省](../Page/阿斯蒂省.md "wikilink") Asti
      - [比耶拉省](../Page/比耶拉省.md "wikilink") Biella
      - [库内奥省](../Page/库内奥省.md "wikilink") Cuneo
      - [诺瓦拉省](../Page/诺瓦拉省.md "wikilink") Novara
      - [都灵省](../Page/都灵省.md "wikilink") Torino
      - [韦尔巴诺-库西奥-奥索拉省](../Page/韦尔巴诺-库西奥-奥索拉省.md "wikilink") Verbano
        Cusio Ossola
      - [韦尔切利省](../Page/韦尔切利省.md "wikilink") Vercelli
  - [撒丁大区](../Page/撒丁大区.md "wikilink")（Sardegna）
      - [卡利亚里省](../Page/卡利亚里省.md "wikilink") Cagliari
      - [卡尔博尼亚-伊格莱西亚斯省](../Page/卡尔博尼亚-伊格莱西亚斯省.md "wikilink")
        Carbonia-Iglesias
      - [梅迪奥坎皮达诺省](../Page/梅迪奥坎皮达诺省.md "wikilink") Medio Campidano
      - [努奥罗省](../Page/努奥罗省.md "wikilink") Nuoro
      - [奥利亚斯特拉省](../Page/奥利亚斯特拉省.md "wikilink") Ogliastra
      - [奥尔比亚-滕皮奥省](../Page/奥尔比亚-滕皮奥省.md "wikilink") Olbia-Tempio
      - [奥-{里}-斯塔诺省](../Page/奥里斯塔诺省.md "wikilink") Oristano
      - [萨萨里省](../Page/萨萨里省.md "wikilink") Sassari
  - [西西里大区](../Page/西西里大区.md "wikilink")（Sicilia）
      - [阿格-{里}-真托省](../Page/阿格里真托省.md "wikilink") Agrigento
      - [卡尔塔尼塞塔省](../Page/卡尔塔尼塞塔省.md "wikilink") Caltanissetta
      - [卡塔尼亚省](../Page/卡塔尼亚省.md "wikilink") Catania
      - [恩纳省](../Page/恩纳省.md "wikilink") Enna
      - [墨西拿省](../Page/墨西拿省.md "wikilink") Messina
      - [巴勒莫省](../Page/巴勒莫省.md "wikilink") Palermo
      - [拉古萨省](../Page/拉古萨省.md "wikilink") Ragusa
      - [锡拉库萨省](../Page/锡拉库萨省.md "wikilink") Siracusa
      - [特拉帕尼省](../Page/特拉帕尼省.md "wikilink") Trapani
  - [特伦蒂诺-上阿迪杰大区](../Page/特伦蒂诺-上阿迪杰大区.md "wikilink")（Trentino-Alto
    Adige）
      - [特伦托省](../Page/特伦托省.md "wikilink") Trento
      - [博尔扎诺-博曾省](../Page/博尔扎诺-博曾省.md "wikilink") Bolzano
  - [托斯卡纳大区](../Page/托斯卡纳大区.md "wikilink")（Toscana）
      - [阿雷佐省](../Page/阿雷佐省.md "wikilink") Arezzo
      - [佛罗伦萨省](../Page/佛罗伦萨省.md "wikilink") Firenze
      - [格罗塞托省](../Page/格罗塞托省.md "wikilink") Grosseto
      - [里窝那省](../Page/里窝那省.md "wikilink") Livorno
      - [卢卡省](../Page/卢卡省.md "wikilink") Lucca
      - [马萨-卡拉拉省](../Page/马萨-卡拉拉省.md "wikilink") Massa-Carrara
      - [比萨省](../Page/比萨省.md "wikilink") Pisa
      - [皮斯托亚省](../Page/皮斯托亚省.md "wikilink") Pistoia
      - [普拉托省](../Page/普拉托省.md "wikilink") Prato
      - [锡耶纳省](../Page/锡耶纳省.md "wikilink") Siena
  - [翁布里亚大区](../Page/翁布里亚大区.md "wikilink")（Umbria）
      - [佩鲁贾省](../Page/佩鲁贾省.md "wikilink") Perugia
      - [特尔尼省](../Page/特尔尼省.md "wikilink") Terni
  - [威尼托大区](../Page/威尼托大区.md "wikilink")（Veneto）
      - [贝卢诺省](../Page/贝卢诺省.md "wikilink") Belluno
      - [帕多瓦省](../Page/帕多瓦省.md "wikilink") Padova
      - [特雷维索省](../Page/特雷维索省.md "wikilink") Treviso
      - [罗维戈省](../Page/罗维戈省.md "wikilink") Rovigo
      - [威尼斯省](../Page/威尼斯省.md "wikilink") Venezia
      - [维罗纳省](../Page/维罗纳省.md "wikilink") Verona
      - [维琴察省](../Page/维琴察省.md "wikilink") Vicenza

## 參考

[\*](../Category/意大利省份.md "wikilink")
[Category:義大利相關列表](../Category/義大利相關列表.md "wikilink")

1.