**圭亞那合作共和国**（\[1\]\[2\]\[3\]），通称**圭亚那**，位於[南美洲北部](../Page/南美洲.md "wikilink")，是南美洲唯一以[英語為](../Page/英語.md "wikilink")[官方語言的](../Page/官方語言.md "wikilink")[国家](../Page/国家.md "wikilink")，也是[英联邦成员國](../Page/英联邦.md "wikilink")。東鄰[蘇利南](../Page/蘇利南.md "wikilink")，南臨[巴西](../Page/巴西.md "wikilink")，西鄰[委内瑞拉](../Page/委内瑞拉.md "wikilink")，北鄰[大西洋](../Page/大西洋.md "wikilink")。圭亞那與蘇利南和委内瑞拉有國界爭議。尤其委内瑞拉一再聲稱[埃塞奎博河以西的土地](../Page/埃塞奎博河.md "wikilink")─[西屬圭亞那](../Page/西屬圭亞那.md "wikilink")（約佔圭亞那面積的三分之二）屬於委內瑞拉。圭亞那雖地處南美洲，為[南美洲國家聯盟的成員國](../Page/南美洲國家聯盟.md "wikilink")，但傳統上及歷史上與加勒比海諸島的關係較为密切。

## 中文譯名

在南美洲北部的[圭亚那地区有兩個名稱相近的地區与國家](../Page/圭亚那_\(地区\).md "wikilink")，分別是獨立的國家「」以及法國海外屬地「」。[中國大陆將獨立的國家譯為](../Page/中華人民共和國.md "wikilink")「**圭-{}-亚那**」，法屬的領地譯為「[法属圭-{}-亚那](../Page/法屬圭亞那.md "wikilink")」。[台灣則將獨立國家譯為](../Page/中華民國.md "wikilink")「**蓋-{}-亞納**」或「蓋-{}-亞那」，法屬的領地譯為「法屬圭-{}-亞那」<ref name="外交部領事事務局法屬圭亞那">{{
Cite web

`|url = `<http://www.boca.gov.tw/content.asp?CuItem=1021&BaseDSD=13&CtUnit=14&mp=1>
`|title = 法屬圭亞那 - 外交部領事事務局全球資訊網`
`|author =`
`|date =`
`|publisher = 外交部領事事務局`
`|language = zh-tw`
`|accessdate = 2014-12-30`
`|format =`
`|quote = 法屬圭-{}-亞那`
`|archiveurl = `<https://web.archive.org/web/20141230202752/http://www.boca.gov.tw/content.asp?CuItem=1021&BaseDSD=13&CtUnit=14&mp=1>
`|archivedate= 2014-12-30`

}}</ref>或「法屬蓋-{}-亞那」。

## 历史

1498年[克里斯托弗·哥伦布發現蓋亞那及其鄰近地區後](../Page/克里斯托弗·哥伦布.md "wikilink")，歐洲移民紛至沓來。隨後三個世紀該地區輪流由英國、荷蘭、法國佔領，直到19世紀初1814年英、法、荷三國訂立界約，蓋亞那地區正式由英國殖民統治，稱為英屬圭亞那（British
Guiana）。1961年獲得自治，改稱圭亞那（Guyana）。1966年5月26日獨立，1970年2月23日成立圭亚那合作共和国\[4\]。

## 人口

蓋亞那山深林密，人口稀少約80萬人，英國殖民者最初利用由非洲購得之奴隸來開墾，1834年奴隸制度廢除，殖民當局轉由南亞的印度與東南亞地區，以契約模式引進廉價勞工，使蓋亞那的農業及礦業能持續發展，此舉也使蓋亞那成為多民族地區。735,554人（2014年）
印度裔約佔49%，黑非裔32%，印非混血12%，餘為土著、華裔及歐洲裔(約7%)。

LethemBridge.jpg|連結巴西的陸橋 Mabaruma Supply Boat from Georgetown.JPG|喬治港
Providence_Stadium_outside.jpg|國家體育場 Parliament building,
Guyana.jpg|议会大楼 Stabroek Market, Georgetown.jpg|農貿市場 Entrance to
Kabakaburi Village, Guyana.jpg|卡巴卡布瑞 Port Kaituma1.jpg|卡蒂瑪港
Lethem-Guyana.jpg|南部地區私人農莊

## 地理

位於南美大陸北緣，北濱大西洋，東為蘇利南，南與巴西接壤，西接委內瑞拉。

## 經濟

財政收支：税入2億8千7百萬美元；税出3億7百萬美元（2004年）。輸出總值：4.994億美元（2007年）；輸入總值：8.358億美元（2007年）。主要輸出項目：鐵礬土、糖、蝦、木材、黃金及米等。主要輸入項目：資本財、燃料、機械、化學品、建材及消費品等。主要出口國：除[加勒比共同體之會員國外](../Page/加勒比共同體.md "wikilink")，另為美、英及加拿大等國。主要進口國：美、英及加拿大等國。

## 其他

圭亞那是唯一仍舊視[同性戀為非法地位的](../Page/同性戀.md "wikilink")[南美洲國家](../Page/南美洲.md "wikilink")。

## 參考文獻

<references />

## 外部連結

  -

  -

  -

  -
[Category:南美洲國家](../Category/南美洲國家.md "wikilink")
[\*](../Category/圭亚那.md "wikilink")
[Category:南美洲國家聯盟成員國](../Category/南美洲國家聯盟成員國.md "wikilink")
[Category:社会主义国家](../Category/社会主义国家.md "wikilink")
[Category:英格兰前殖民地](../Category/英格兰前殖民地.md "wikilink")

1.  [圭亚那国家概况](http://www.fmprc.gov.cn/mfa_chn/gjhdq_603914/gj_603916/nmz_608635/1206_608832/)

2.

3.  [圭亚那驻华大使馆](http://www.guyanaembassybeijing.cn/indexch.asp)

4.