**柯受良**（），生于[浙江](../Page/浙江.md "wikilink")[渔山列岛](../Page/渔山列岛.md "wikilink")，籍贯[浙江](../Page/浙江.md "wikilink")[宁波](../Page/宁波.md "wikilink")，以挑战极限著称的[台灣著名艺人](../Page/台灣.md "wikilink")，綽號**小黑**、**亚洲飞人**。

他曾經駕[摩托车飛越](../Page/摩托车.md "wikilink")[萬里長城及](../Page/萬里長城.md "wikilink")[西藏](../Page/西藏.md "wikilink")[布達拉宮](../Page/布達拉宮.md "wikilink")，1997年6月1日駕汽車飛越[黃河的](../Page/黃河.md "wikilink")[壺口瀑布以慶祝](../Page/壺口瀑布.md "wikilink")[香港回歸](../Page/香港回歸.md "wikilink")，有“亞洲第一飛人”的稱號。生前曾計劃飛越[長江三峽](../Page/長江三峽.md "wikilink")，但未能如願。

## 早年

柯受良在1953年出生在[國共內戰末期時的中國大陸](../Page/國共內戰.md "wikilink")，[美軍](../Page/美軍.md "wikilink")[第七艦隊與](../Page/第七艦隊_\(美國海軍\).md "wikilink")[國軍的協助下](../Page/國軍.md "wikilink")，1955年2月[大陳島撤退時](../Page/大陳島撤退.md "wikilink")，兩歲的柯受良隨父親追隨[國軍到](../Page/國軍.md "wikilink")[台灣](../Page/台灣.md "wikilink")\[1\]\[2\]。在大風大浪中，與父母從大陳島一路顛沛流離，并定居[台東縣](../Page/台東縣.md "wikilink")。

柯家在浙江沿海一帶有200年的捕魚歷史，屬於[閩南民系福佬人](../Page/閩南民系.md "wikilink")，語言與[臺灣閩南人略同](../Page/臺灣閩南人.md "wikilink")。1955年剛滿3歲的柯受良隨著一家人和整個家族及村子共20戶的人搬遷到臺東縣卑南鄉富岡（今[臺東市](../Page/臺東市.md "wikilink")），父執輩的人則是在台灣持續了捕魚的生活。其[閩南語講得比國語流利](../Page/閩南語.md "wikilink")，與[本省人沒有文化的隔閡](../Page/本省人.md "wikilink")，這也是他奠定在台灣演藝圈親和力及地位的基礎。

14歲時，家境貧困的柯受良中斷了學業，並開始幫家裡的人從事漁業，天光未亮就跟著父親出海。16歲時獨自一人上了台北工作，隔年考進了電影明星學校，並擔任場記、武師的工作多年，專長是特技表演。

## 生平

1970年，報考電影明星學校被錄取，充任臨記、武師工作，首次參加特技表演。

1971年，《》裡，一路由月世界山頂滾到山下的灰頭土臉替身。

1975年，參加臺灣摩托車障礙賽，奪得冠軍。

1977年，進入香港影視圈，擔任武術指導驚險片演員和導演。

1980年，《血濺冷鷹堡》騰空攀越香港太平山纜車的高難度動作，柯受良終於熬成台灣第一把交椅的特技演員。

1982年，參與香港電影《最佳拍檔》，以特技演出一舉成名，當時一幕機車以時速200公里從大廈三樓破窗而出，還要停留三秒再落海的驚險鏡頭，所有香港的特技演員，都視為「不可能的任務」。但其自告奮勇，親自上陣。結果這一躍成為柯受良奠立香江影壇地位的經典畫面，被稱“柯大膽”。

1984年，擔任成龍電影《快餐車》特技指導，並親自參加飛車特技演出。

1985年，首部個人電影連續劇《飛越萬縷情》在[華視上映](../Page/華視.md "wikilink")。

1986年，擔任成龍電影《[龍兄虎弟](../Page/龍兄虎弟_\(1987年電影\).md "wikilink")》的特技指導，在南斯拉夫親自駕駛跑車飛越六車道高速公路（長度達75米）。

1987年，7月26日駕駛汽車飛越了80米寬素有“死亡谷”之稱的臺灣桃園海湖峽谷。

1988年，應藝能影業公司邀請執導《壯志豪情》，奠定導演地位。

1992年11月15日，飛越萬里長城，為中國體育基金會酬款，在北京金山嶺長城完成摩托車特技凌空飛越38米，列入[健力士世界紀錄大全](../Page/健力士世界紀錄大全.md "wikilink")。

1993年，成立柯受良電影創作公司，創業作品《芝士火腿》；發行第一張閩南語唱片《正港男兒》；參加韓國舉行的世界博覽會，擔任飛車表演特邀嘉賓。

1994年，參加台灣區越野汽車長途對抗賽，獲得第三名。

1997年6月1日，迎香港回歸，駕駛跑車成功飛越黃河天塹 -
壺口瀑布（長度55米），全程由[中國中央電視台](../Page/中國中央電視台.md "wikilink")、[鳳凰衛視中文台](../Page/鳳凰衛視中文台.md "wikilink")[現場直播](../Page/現場直播.md "wikilink")，獲得“亞洲第一飛人”的稱號。　　　

1998年，柯受良與劉德華、吳宗憲合唱國語歌曲〈笨小孩〉，成為一時金曲。

1999年，為新加坡仁善醫院籌款義演，在新加坡體育館成功飛越42米。同年6月發行個人國語大碟《再次征服》；11月廣告：台灣康利諾洗髮精12月廣告、三洋維士比。

2000年，2月廣告-[光陽工業](../Page/光陽工業.md "wikilink")「光陽三冠王」機車，3月擔綱主持[AXN](../Page/AXN.md "wikilink")《飛越生死線》
；4月與吳宗憲聯合主持[中視](../Page/中視.md "wikilink")《[週日八點黨](../Page/周日八點黨.md "wikilink")》。

2002年，駕駛汽車成功飛越布達拉宮廣場（28米）。

2003年12月9日，因飲酒過量引發[氣喘](../Page/氣喘.md "wikilink")，送往[上海市第六人民醫院搶救無效身亡](../Page/上海市第六人民醫院.md "wikilink")，終年50歲。

## 家庭

藝人[柯有倫為其子](../Page/柯有倫.md "wikilink")。其女兒[柯美麗於](../Page/柯美麗.md "wikilink")2006年5月14日嫁給了前[東華三院主席](../Page/東華三院.md "wikilink")[周振基兒子周博軒](../Page/周振基.md "wikilink")。

## 评价

柯受良連續以凌空飛車特技讓人津津樂道，他先後飛越萬里長城、黃河壺口瀑布、布達拉宮等知名標的的狀舉，讓他在中國大陸贏得「中國特技之王」、「亞洲飛人」的封號，聲名更享譽全球華人社群。

除了醉心演藝事業以外，亦對社會事務十分關心。1990年代初期，香港[尖沙咀海旁出現俗稱](../Page/尖沙咀.md "wikilink")“[老泥妹](../Page/老泥妹.md "wikilink")”的離家出走少女，他當時拍了一部電影《老泥妹》反映她們的生活，引起社會關注。1997年香港主權移交前，[中華人民共和國政府決意另起爐灶成立](../Page/中華人民共和國.md "wikilink")[香港臨時立法會](../Page/香港臨時立法會.md "wikilink")，他與[柯俊雄是報名參加選舉的唯二](../Page/柯俊雄.md "wikilink")[中国国民党籍人士](../Page/中国国民党.md "wikilink")，結果雙雙落敗。

柯受良在圈內人的眼中口碑亦很好，他的好友包括[刘德华](../Page/刘德华.md "wikilink")、[周星馳](../Page/周星馳.md "wikilink")、[张学友](../Page/张学友.md "wikilink")、[梁朝伟](../Page/梁朝伟.md "wikilink")、[曾志偉](../Page/曾志偉_\(藝人\).md "wikilink")、[吳宗憲](../Page/吳宗憲.md "wikilink")、[王傑等人](../Page/王傑.md "wikilink")。

2014年1月12日，時隔14年，被稱為柯受良關門弟子的[中國大陸籍特技車手謝雨均選擇在黑龍江一段冰封的江面上飛車距離超過](../Page/中國大陸.md "wikilink")40米，向師傅當年飛越壺口瀑布的壯舉致敬。

## 唱片

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>專輯資料</p></th>
<th style="text-align: left;"><p>唱片公司</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>語言</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>《正港男兒》</p></td>
<td style="text-align: left;"><p>名冠唱片公司發行</p></td>
<td style="text-align: left;"><p>1993年12月</p></td>
<td style="text-align: left;"><p><a href="../Page/台語.md" title="wikilink">台語</a></p></td>
<td style="text-align: left;"><ol>
<li>正港的男兒</li>
<li>妳不是我妳未知影</li>
<li>思念</li>
<li>黃昏的故鄉</li>
<li>心事誰人知</li>
<li>我不是放蕩的人</li>
<li>我不瞭解[我]</li>
<li>何必相送</li>
<li>暗淡的月</li>
<li>心所愛的人</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>《再次征服》</p></td>
<td style="text-align: left;"><p>BMG唱片公司發行</p></td>
<td style="text-align: left;"><p>1999年6月</p></td>
<td style="text-align: left;"><p><a href="../Page/中華民國國語.md" title="wikilink">國語</a></p></td>
<td style="text-align: left;"><ol>
<li>大哥</li>
<li>錢不夠用</li>
<li>征服</li>
<li>我要拼</li>
<li>男人眼淚</li>
<li>溫柔壞男人</li>
<li>粗線條</li>
<li>天和地</li>
<li>笨小孩</li>
<li>憨人情歌</li>
</ol></td>
</tr>
</tbody>
</table>

## 電影

  - 1981年 《[瘋狂大發財](../Page/瘋狂大發財.md "wikilink")》飾 鄉巴佬
  - 1985年 《[八番坑口的新娘](../Page/八番坑口的新娘.md "wikilink")》
  - 1986年《[神勇雙響炮續集](../Page/神勇雙響炮續集.md "wikilink")》
  - 1987年 《[衛斯理傳氣](../Page/衛斯理傳氣.md "wikilink")》飾 兩頭蛇
  - 1989年 《[英雄無膽](../Page/英雄無膽.md "wikilink")》飾 連長
  - 1989年 《[壯志豪情](../Page/壯志豪情.md "wikilink")》導演
  - 1990年 《[咖哩辣椒](../Page/咖哩辣椒.md "wikilink")》飾 鮑魚刷
  - 1990年 《[賭俠](../Page/賭俠.md "wikilink")》飾 黑豹
  - 1991年 《[夜生活女王之霞姐傳奇](../Page/夜生活女王之霞姐傳奇.md "wikilink")》飾 阿光
  - 1992年 《[戰龍在野](../Page/戰龍在野.md "wikilink")》飾 黑哥
  - 1992年 《[逃學威龍2](../Page/逃學威龍2.md "wikilink")》飾 犯人（客串）
  - 1992年 《[少年吔，安啦！](../Page/少年吔，安啦！.md "wikilink")》飾 老董
  - 1993年 《[重案組](../Page/重案組_\(電影\).md "wikilink")》飾 台灣警察（客串）
  - 1993年 《[芝士火腿](../Page/芝士火腿.md "wikilink")》飾 殺手龍
  - 1994年 《[賭神2](../Page/賭神2.md "wikilink")》飾 海岸
  - 1995年 《[給爸爸的信](../Page/給爸爸的信.md "wikilink")》飾 李連傑好友
  - 1996年 《[古惑仔2之猛龍過江](../Page/古惑仔2之猛龍過江.md "wikilink")》 飾 柯志華
  - 1996年 《[古惑仔3之隻手遮天](../Page/古惑仔3之隻手遮天.md "wikilink")》 飾 柯志華
  - 1997年 《[俠盜正傳](../Page/俠盜正傳.md "wikilink")》 飾 石班主
  - 1997年 《[黑玫瑰之義結金蘭](../Page/黑玫瑰之義結金蘭.md "wikilink")》飾 紅毛丹
  - 1999年 《[烈火戰車2極速傳說](../Page/烈火戰車2極速傳說.md "wikilink")》飾 黑鬼東
  - 1999年 《[條子阿不拉](../Page/條子阿不拉.md "wikilink")》飾 阿不拉
  - 2000年 《[勝者為王](../Page/勝者為王.md "wikilink")》飾 柯志華
  - 2000年 《[BAD BOY 特攻](../Page/勝者為王.md "wikilink")》飾 黑鬼東
  - 2000年 《[笨小孩](../Page/笨小孩.md "wikilink")》 飾 譚耀文父
  - 2001年 《[欲望之城](../Page/欲望之城.md "wikilink") 》
  - 2001年 《[童黨](../Page/童黨.md "wikilink") 》 飾 猛哥
  - 2003年 《[生死速遞](../Page/生死速遞.md "wikilink")》 飾 吳信勇

## 電視劇

  - 2017年 《[反黑](../Page/反黑.md "wikilink")》 飾
    黑哥(此角色是致敬已故藝人柯受良，劇中於陳鳳翔的回憶有出現陳小春與柯受良生前的合照)

## 參考資料

[B](../Category/臺灣特技表演者.md "wikilink")
[B](../Category/世界之最.md "wikilink")
[B](../Category/中國電影男演員.md "wikilink")
[B](../Category/香港電影男演員.md "wikilink")
[B](../Category/香港電影導演.md "wikilink")
[B](../Category/臺灣電影男演員.md "wikilink")
[B](../Category/台灣男歌手.md "wikilink")
[B](../Category/臺灣電視男演員.md "wikilink")
[B](../Category/動作片演員.md "wikilink")
[B](../Category/中國國民黨黨員.md "wikilink")
[B](../Category/藝人出身的政治人物.md "wikilink")
[B](../Category/象山人.md "wikilink")
[B](../Category/大陳島人.md "wikilink")
[B](../Category/台灣戰後寧波移民.md "wikilink")
[S受良](../Category/柯姓.md "wikilink")

1.  [逃難離鄉半世紀
    村民陪同如意娘娘風光返鄉](http://www.epochtimes.com/b5/8/9/8/n2256254p.htm)
2.  [彭副縣長率團參與大陸象山開漁節活動收穫豐
    並希加強雙方農漁業等交流](http://www.taitung.gov.tw/tw/information/CountyNewsDetail.aspx?SN=13303)