**ソフト・オン・デマンド株式会社**（），通稱**SOD**，是一個[日本的](../Page/日本.md "wikilink")[成人影片集團公司](../Page/成人视频制造商.md "wikilink")，總部設在東京的[中野區](../Page/中野區.md "wikilink")，成立於1995年12月。\[1\]該公司是日本最大的成人影片公司之一，因其對成人影片的創造性內容而聞名。\[2\]

## 概要

自1996年脫離傳統AV片商的[映畫倫理委員會以來](../Page/映畫倫理委員會.md "wikilink")，SOD一直是成長最快與表現最好的片商之一，並設立與吸引不少子公司加盟，近年來與擁有[MOODYZ與](../Page/MOODYZ.md "wikilink")[S1的](../Page/S1_\(AV公司\).md "wikilink")[北都集團被視為日本成人映像業界的兩大勢力](../Page/北都集團.md "wikilink")，2006年，SOD集合日本數家成人片商舉辦「AV
OPEN」作品競賽，並集合500位男女集體性愛拍攝成作品，同時也積極和西洋成人片商合作，受到很多業界觀察家注目。

2010年舉辦挑戰「世界做愛最久」的企劃，由女優[新垣聖那與男優](../Page/新垣聖那.md "wikilink")[川本英雄以連續做愛十小時](../Page/川本英雄.md "wikilink")，打破[金氏世界紀錄](../Page/金氏世界紀錄.md "wikilink")。\[3\]

2010年12月舉辦「SOD2010年大賞」頒獎當年度優秀的AV作品。\[4\]

## 著名作品

  - SOD女子社員系列
  - SOD STAR
  - 本物人妻
  - 童貞狩
  - [透明人](../Page/透明人.md "wikilink")
  - 時間停止系列
  - 魔術鏡系列
  - 我的皮可系列

## 主要子公司與相關公司

  - **（SOD Create）**

:\*Soft·on·demand母公司的主要製作部門。分支有SOD Star、SOD ACE、SOD Cinderella、Senz等。

  - **（Natural High）**

:\*1999年8月加入旗下。影片內容以[癡漢](../Page/癡漢.md "wikilink")，擬真性犯罪等內容為大宗。

  - **（DEEP'S）**

:\*[1999年10月加入旗下](../Page/1999年10月.md "wikilink")。

  - **（IEnergy）**

:\*[2000年12月加入](../Page/2000年12月.md "wikilink")。在網路討論區熱門的「[中出し](../Page/體內射精.md "wikilink")20連発」系列。

  - ****　

:\*著名AV導演志摩紫光設立。以[SM內容為主](../Page/SM.md "wikilink")。

  - **（Hibino）**

:\*著名AV導演日比野正明設立。以劇情片為主。

  - **（V\&R Products）**

:\*[2004年4月加入](../Page/2004年4月.md "wikilink")。以各種妄想奇想系內容為主，代表作有「時間停止」、「透明人間」等。

  - **（AKNR）**

:\*[2006年1月加入旗下](../Page/2006年1月.md "wikilink")。以多段式情境劇為主。

  - **DANDY**

:\*[2006年8月加入](../Page/2006年8月.md "wikilink")。

  - **LADYxLADY**

:\*[2007年1月加入](../Page/2007年1月.md "wikilink")。女同性戀題材。

  - **ハンター（Hunter）**

:\*[2007年4月加入](../Page/2007年4月.md "wikilink")。

  - **ギャルソン（GARCON）**　

:\*[2007年6月加入](../Page/2007年6月.md "wikilink")。

  - **サディスティックヴィレッジ（[Sadistic
    Village](../Page/Sadistic_Village.md "wikilink")）**

:\*[2007年7月加入](../Page/2007年7月.md "wikilink")。

  - **ロケット（ROCKET）**

:\*[2008年1月加入](../Page/2008年1月.md "wikilink")。 「処女喪失」、「究極の妄想発明シリーズ」系列。

  - **人間考察**

:\*[2008年9月加入](../Page/2008年9月.md "wikilink")。

  - **KEU（稀有）**

:\*[2009年9月加入](../Page/2009年9月.md "wikilink")。

  - **（NEW SEXUAL）**

:\*[2010年5月加入](../Page/2010年5月.md "wikilink")。以[同志㚻片為主](../Page/同志㚻片.md "wikilink")。

  - **ATOM**

:\*[2010年11月加入](../Page/2010年11月.md "wikilink")。

  - **SWITCH**

:\*[2011年12月加入](../Page/2011年12月.md "wikilink")。以情境劇為主。

  - **new girl**

:\*[2011年12月加入](../Page/2011年12月.md "wikilink")。素人片形式。

  - **SOSORU（ソソル）**

:\*[2013年3月加入](../Page/2013年3月.md "wikilink")。以情境劇為主。

## 其他

甲斐正明事務所已於2007年離開SOD公司，獨立經營事務所業務。

「SOD都是真的」是一句台灣網路的流行用語，多用在與性有關、難以令人置信會發生在現實的事物。在新聞媒體中也常見以此句做為標題的性相關新聞。

## 參考資料

## 外部連結

  - [SOD官方網站](http://www.sod.co.jp/)

  - [SOD官方youtube](https://www.youtube.com/channel/UCWTv_HSFmceRzDVb6p4U7kw/featured)

  -   - [Natural High](http://www.naturalhigh.co.jp/)
      - [DEEP'S](http://www.deeps.net/)
      - [IEnergy](http://www.ienergy1.com/)
      - [Hibino](https://web.archive.org/web/20120510011735/http://www.av-hibino.com/system/index.php)
      - [V\&R Products](http://www.vr-produce.co.jp/)
      - [AKNR](https://web.archive.org/web/20120503020901/http://www.aknr.com/top.html)
      - [DANDY](http://www.choi-waru.com/)
      - [Hunter](http://www.hunter-pp.jp/)
      - [GARCON](http://www.garcon-web.com/)
      - [ROCKET](http://www.rocket-inc.net/)
      - [人間考察](http://www.ningenkousatsu.com/)
      - [KEU](http://keu-av.com/index02.html)
      - [NEW
        SEXUAL](https://web.archive.org/web/20131228010757/http://www.new-sexual.jp/)
      - [ATOM](http://www.a-t-o-m.jp/)
      - [SWITCH](http://switch1.jp/web/)
      - [new
        girl](https://web.archive.org/web/20120505130443/http://www.newgirl.jp/top.html)
      - [SOSORU](http://sosoru.jp/web/)

[Category:日本色情片公司](../Category/日本色情片公司.md "wikilink")
[Category:中野區公司](../Category/中野區公司.md "wikilink")
[Category:1995年成立的公司](../Category/1995年成立的公司.md "wikilink")

1.
2.
3.
4.