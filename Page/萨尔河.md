**萨尔河**（[法语](../Page/法语.md "wikilink")：****、[德语](../Page/德语.md "wikilink")：****）发源于[法国](../Page/法国.md "wikilink")[阿尔萨斯和](../Page/阿尔萨斯.md "wikilink")[洛林边境的](../Page/洛林.md "wikilink")[孚日山脉](../Page/孚日山脉.md "wikilink")，向西流经德国，最终在[孔茨注入](../Page/孔茨.md "wikilink")[摩泽尔河](../Page/摩泽尔河.md "wikilink")，全长246公里。

萨尔河流经的主要城镇有：

  - 法国：
      - [摩泽尔省](../Page/摩泽尔省.md "wikilink")：[阿布雷什维莱](../Page/阿布雷什维莱.md "wikilink")、[萨尔堡](../Page/萨尔堡_\(法国\).md "wikilink")、[萨尔格米讷](../Page/萨尔格米讷.md "wikilink")
      - [下莱茵省](../Page/下莱茵省.md "wikilink"):
  - 德国
      - [萨尔州](../Page/萨尔州.md "wikilink")：[萨尔布吕肯](../Page/萨尔布吕肯.md "wikilink")、[萨尔路易](../Page/萨尔路易.md "wikilink")、[梅尔齐希](../Page/梅尔齐希.md "wikilink")
      - [莱茵兰-普法尔茨州](../Page/莱茵兰-普法尔茨州.md "wikilink")：[萨尔堡](../Page/萨尔堡_\(德国\).md "wikilink")、[孔茨](../Page/孔茨.md "wikilink")

[联合国教科文组织](../Page/联合国教科文组织.md "wikilink")[世界文化遗产](../Page/世界文化遗产.md "wikilink")[弗尔克林根钢铁厂就位于萨尔河畔](../Page/弗尔克林根钢铁厂.md "wikilink")。

## 参见

支流:

  - [布利斯河](../Page/布利斯河.md "wikilink")

[S](../Category/法国河流.md "wikilink") [S](../Category/德国河流.md "wikilink")
[Category:摩泽尔河支流](../Category/摩泽尔河支流.md "wikilink")