**AMD
K8**（研發代號：Hammer）是[美商](../Page/美国.md "wikilink")[超微](../Page/超微.md "wikilink")（AMD）研發並推出市場的[中央處理器微架構](../Page/中央處理器.md "wikilink")，用以接替之前的[K7微架構](../Page/AMD_K7.md "wikilink")。K8架構是第一個引入[64位x86擴充指令集的](../Page/X86-64.md "wikilink")[x86處理器架構](../Page/x86.md "wikilink")，是為『[AMD64](../Page/AMD64.md "wikilink")』。

基於K8架構的超微處理器：

  - [Athlon 64](../Page/Athlon_64.md "wikilink")
  - [Athlon 64 X2](../Page/Athlon_64_X2.md "wikilink")
  - [Athlon 64 FX](../Page/Athlon_64_FX.md "wikilink")
  - [Sempron](../Page/Sempron.md "wikilink")
  - [Opteron](../Page/Opteron.md "wikilink")
  - [Turion 64](../Page/Turion_64.md "wikilink")

K8架構的核心和K7架構的非常相似，兩者最主要的變化在於K8架構中引入AMD64指令集（64位x86）以及處理器晶片內建[記憶體控制器](../Page/記憶體.md "wikilink")。由於記憶體控制器內建於處理器內，處理器和記憶體之間直接進行資料傳送，不再需要繞道[北橋晶片](../Page/北橋.md "wikilink")，因而有更低的傳送[延時和更快的響應速率](../Page/延遲.md "wikilink")，相較於以前的K7架構和對手[英特爾當時的一眾處理器擁有更佳的記憶體存取效能](../Page/英特爾.md "wikilink")。此外K8架構後來還支援[DDR2-SDRAM記憶體](../Page/DDR2_SDRAM.md "wikilink")。

[AMD_K8.PNG](https://zh.wikipedia.org/wiki/File:AMD_K8.PNG "fig:AMD_K8.PNG")

## 參見

  - [AMD K9](../Page/AMD_K9.md "wikilink")
  - [AMD K10](../Page/AMD_K10.md "wikilink")

[Category:X86架構](../Category/X86架構.md "wikilink")
[Category:AMD处理器](../Category/AMD处理器.md "wikilink")