**藤原浩**（日語： /ふじわら ひろし<sup>Fujiwara
Hiroshi</sup>，），是一位[日本](../Page/日本.md "wikilink")[時裝設計師](../Page/時裝設計師.md "wikilink")、[饒舌歌手](../Page/饒舌歌手.md "wikilink")、[DJ](../Page/DJ.md "wikilink")，也被稱為[裏原宿之父](../Page/裏原宿.md "wikilink")。是設計單位[Fragment
Design的主理人](../Page/Fragment_Design.md "wikilink")。

## 介紹

藤原浩的身份很多，像是DJ、音樂製作人、設計師等等，是一位全方位的人才。被譽為日本的潮流教父[裏原宿之父](../Page/裏原宿.md "wikilink")，不止在[日本只要是亞洲地區的人也深受著藤原浩的影響](../Page/日本.md "wikilink")。其實早期藤原浩從[英国留學歸來以後](../Page/英国.md "wikilink")，是從事著[DJ的工作](../Page/唱片騎師.md "wikilink")，投身於音事業與現在同為原宿系教父級的DJ[高木完創立了](../Page/高木完.md "wikilink")[TINNIE
PUNKS這個組合作為正式出道的團體](../Page/TINNIE_PUNKS.md "wikilink"),隔年將團名改名為[TINY
PANX](../Page/TINY_PANX.md "wikilink").當時引起極大迴響，1988年成立Major
Force音樂製作公司後1990年開始藤原浩開始接觸了服裝領域，並創辦了自己的第一個品牌[Good
Enough](../Page/Good_Enough.md "wikilink")。

## 著作

  - PERSONAL EFFECTS
  - Hiroshi Fujiwara: Fragment

## 經歷

  - 1979年11月，Plastics於[英國獨立音樂廠牌](../Page/英國.md "wikilink") Rough
    trade，發行首張單曲 COPY/ROBOT。
  - 1982年，[中西俊夫和](../Page/中西俊夫.md "wikilink")[YMO團員](../Page/YMO.md "wikilink")[細野晴臣](../Page/細野晴臣.md "wikilink")，[高橋幸宏以及](../Page/高橋幸宏.md "wikilink")[土屋昌巳](../Page/土屋昌巳.md "wikilink")(後長居英國)，[桑原茂一等人合作專輯](../Page/桑原茂一.md "wikilink")
    "Do You Like Japan?"。
  - 1986年，藤原浩和高木完兩人組成饒舌&龐克團體TINNIE PUNKS。
  - 1987年，TSUBAKI HOUSE（樁館）閉店，LONDON NITE仍持續舉行。
  - 1988年，與高木完、[中西俊夫](../Page/中西俊夫.md "wikilink")、[工藤昌之](../Page/工藤昌之.md "wikilink")（K.U.D.O）和[屋敷豪太等人創設首個日本傳奇](../Page/屋敷豪太.md "wikilink")[Hip-hop廠牌](../Page/Hip-hop.md "wikilink")[MAJOR
    FORCE](../Page/MAJOR_FORCE.md "wikilink")。
  - 1990年，創立街頭品牌[Good
    Enough](../Page/Good_Enough.md "wikilink")，品牌服裝數量極為稀少造成當時一股風潮。
  - 1995年，創辦[Electric
    Cottage](../Page/Electric_Cottage.md "wikilink")（EC）服飾品牌，Electric
    Cottage為當時藤原浩私人工作室的名字。
  - 1998年，創設[Head
    Porter背包品牌](../Page/Head_Porter.md "wikilink")，藤原浩負責監製設計而背包本身由[吉田吉藏先生的YOSHIDA](../Page/吉田吉藏.md "wikilink")
    & CO.LTD負責生產。
  - 2002年，藤原浩(Fujiwara
    Hiroshi)與[Nike創意概念副總裁Tinker](../Page/Nike.md "wikilink")
    Hatfield以及Nike公司總裁兼首席執行長Mark Parke共同發展出Nike
    HTM系列，HTM取自於三個人的名字頭文字所組成。
  - 2004年，和[倉石一樹成立](../Page/倉石一樹.md "wikilink")[Fragment
    Design工作室](../Page/Fragment_Design.md "wikilink")(前身為Electric
    Cottage)。
  - 2006年，與[NEIGHBORHOOD的主理人瀧澤伸介](../Page/NEIGHBORHOOD.md "wikilink")，共同創立平價服飾品牌[Base
    Control](../Page/Base_Control.md "wikilink")。
  - 2008年，SOPHNET.主理人清永浩文與藤原浩共同創立時裝品牌[Uniform
    Experiment](../Page/Uniform_Experiment.md "wikilink")，概念為對“實驗性”的創新性改良，將許多元素融入時裝之中，創造出更多適合現代生活中穿著的服飾。
  - 2009年，由blender株式会社所開發的日本香水品牌[retaW](../Page/retaW.md "wikilink")，藤原浩為策劃人之一負責策畫和創意。品牌名稱取自「Water」水的反寫。
  - 2018年，在1998年創設的 [Head Porter](../Page/Head_Porter.md "wikilink")
    背包品牌宣佈將於2019年春夏後結束，但也同時宣佈2019年9月將會有新品牌誕生。\[1\]

## 外部連結

  - [fujiwarahiroshi](https://www.instagram.com/fujiwarahiroshi/)的[Instagram](../Page/Instagram.md "wikilink")
  - [藤原 ヒロシ](https://ringofcolour.com/archives/author/h-fujiwara)的[Ring
    of colour](../Page/Ring_of_colour.md "wikilink")
  - [Fragment Design](http://www.fragment.jp/)
  - [Head Porter](http://store.headporter.co.jp/en/)

[Category:日本企业家](../Category/日本企业家.md "wikilink")
[Category:日本服装设计师](../Category/日本服装设计师.md "wikilink")
[Category:日本音樂製作人](../Category/日本音樂製作人.md "wikilink")
[Category:三重縣出身人物](../Category/三重縣出身人物.md "wikilink")

1.