[A_small_cup_of_coffee.JPG](https://zh.wikipedia.org/wiki/File:A_small_cup_of_coffee.JPG "fig:A_small_cup_of_coffee.JPG")，一种存在于咖啡和许多饮料中的物质，是使用最广泛的精神药物。90%的美国人每天都摄入咖啡因。\[1\]\]\]
[Liquor_store_in_Breckenridge_Colorado.jpg](https://zh.wikipedia.org/wiki/File:Liquor_store_in_Breckenridge_Colorado.jpg "fig:Liquor_store_in_Breckenridge_Colorado.jpg")是最常用的药物。2013年，全球酒精饮料产业的产值将近一兆美元。\[2\]啤酒是世界第三流行的饮料，排在[饮用水和](../Page/饮用水.md "wikilink")[茶之后](../Page/茶.md "wikilink")。\[3\]\]\]
[Papierosa_1_ubt_0069.jpeg](https://zh.wikipedia.org/wiki/File:Papierosa_1_ubt_0069.jpeg "fig:Papierosa_1_ubt_0069.jpeg")，含有[尼古丁](../Page/尼古丁.md "wikilink")，是世界上使用最广泛的药物之一。\[4\]\]\]

**药物**（）广义上指可以对[人或其他](../Page/人类.md "wikilink")[动物产生已知生物效应的](../Page/动物.md "wikilink")[物质](../Page/物质.md "wikilink")\[5\]。[食物通常不适用于这个定义](../Page/食物.md "wikilink")，尽管它们也可以对生物物种产生生理效应\[6\]\[7\]\[8\]。

[药理学上](../Page/药理学.md "wikilink")，药物指用于预防、治疗、诊断疾病或增强体格或改善精神状态的化学物质\[9\]。[药品可以短期使用或定期使用来治疗](../Page/药物治疗.md "wikilink")[慢性疾病](../Page/慢性疾病.md "wikilink")\[10\]。

[精神药物是可以影响](../Page/精神药物.md "wikilink")[神经系统功能](../Page/神经系统.md "wikilink")，改变感觉、情绪、意识的化学物质\[11\]。酒精、尼古丁和咖啡因是世界上使用最广泛的精神药物\[12\]。

[娱乐性药物是没有医疗用途](../Page/娱乐性药物.md "wikilink")，而是用来获得快感的药物\[13\]。常见的娱乐性药物包括酒精、尼古丁和咖啡因，也包括其他物质如鸦片类药物和安非他命。

有的药物可以产生[依赖性和习惯性](../Page/药物依赖.md "wikilink")，并且所有的药物均有[副作用](../Page/副作用.md "wikilink")\[14\]\[15\]。许多药物被禁止用作娱乐用途，国际条约如[麻醉品单一公约的存在意在从法律上禁止某些物质的使用](../Page/麻醉品单一公约.md "wikilink")。

## 药品

[Nexium_(esomeprazole_magnesium)_pills.JPG](https://zh.wikipedia.org/wiki/File:Nexium_\(esomeprazole_magnesium\)_pills.JPG "fig:Nexium_(esomeprazole_magnesium)_pills.JPG")镁胶囊，是一种[质子泵抑制剂](../Page/质子泵抑制剂.md "wikilink")（PPI），用来减少[胃酸的产生](../Page/胃酸.md "wikilink")。\]\]

药品（pharmaceutical drug, medicinal drug 或
medication）是一种可以治疗和（或）改善疾病症状或医疗条件的药物，也可以作为预防用药来改善将来的情况，而不是用来治疗已存在或曾经存在的症状。

药品的配给通常由政府监管并分为三类：[OTC类药物](../Page/非处方药.md "wikilink")（可以出售于[药店或](../Page/药店.md "wikilink")[超市并由消费者自行选购](../Page/超市.md "wikilink")，没有任何特殊限制）、[BTC类药物](../Page/BTC.md "wikilink")（见于欧美，可以由[药剂师配给且不需要](../Page/药剂师.md "wikilink")[医师处方](../Page/医师.md "wikilink")）和[处方药](../Page/处方药.md "wikilink")（POM，只可由执业医疗专业人员处方，通常为执业医师）。

在英国，BTC类药物又被称为药房药品，只可在药店出售，并且需要药剂师监管。这些药品的包装上会印有字母P\[16\]。无需处方的药物种类在不同国家间往往不尽相同。

药品通常由制药公司生产，开发商往往被授予专利权并可以独家进行生产。非专利（或是专利已过期）的药品称为generic
drug，其他制药公司无需向开发商取得授权或许可便可以生产它们，非专利持有者生产的药品称为仿制药，中国的制药厂多以生产仿制药为主。

## 给药方式

药物，无论是药品还是娱乐性药物，都可以有许多的给药方法或途径。许多药物的给药方式不止一种。

  - 是给药方式的一种，使药物或其他化合物在[血液内达到有效的浓度水平](../Page/血液.md "wikilink")。推注可以在静脉内，肌内，鞘内或皮下进行。

  - [吸入](../Page/吸入.md "wikilink")（通过[呼吸进入](../Page/呼吸.md "wikilink")[肺部](../Page/肺.md "wikilink")），通常为气雾剂或干粉末（也包括吸入物质）。

  - [注射悬浮液或乳液](../Page/注射.md "wikilink")，方式有[肌内注射](../Page/肌内注射.md "wikilink")（肌注）、[静脉注射](../Page/静脉注射.md "wikilink")（静注，又分为静脉滴注和静脉推注）、[腹腔内注射](../Page/腹腔内注射.md "wikilink")、[骨内注射](../Page/骨内注射.md "wikilink")。

  - [喷雾](../Page/喷雾.md "wikilink")，也可以喷入[鼻腔内](../Page/鼻腔.md "wikilink")。

  - [口服液体或固体](../Page/口.md "wikilink")，通过[肠道吸收](../Page/肠道.md "wikilink")。

  - [直肠](../Page/直肠.md "wikilink")[栓剂](../Page/栓剂.md "wikilink")，通过[直肠或](../Page/直肠.md "wikilink")[结肠吸收](../Page/结肠.md "wikilink")。

  - [舌下含服](../Page/舌下含服.md "wikilink")，通过舌下组织扩散到血液中。

  - [外用](../Page/外用.md "wikilink")，通常为乳膏或软膏，可以局部也可以全身给药。\[17\]

  - [阴道](../Page/阴道.md "wikilink")[栓剂](../Page/栓剂.md "wikilink")，主要用来治疗[阴道感染](../Page/阴道感染.md "wikilink")。

## 文言釋義

  - [东晉](../Page/东晉.md "wikilink")[葛洪](../Page/葛洪.md "wikilink")《[抱朴子](../Page/抱朴子.md "wikilink")·道意》：“屢值疫癘，當得藥物之力。”
  - [嚴復](../Page/嚴復.md "wikilink")《[原強續篇](../Page/原強.md "wikilink")》：“蓋察病而知致病之原，則其病將愈，唯病原真而後藥物得，藥物得而後其病乃有瘳，此不易之理也。”

## 参考文献

  - 引用

## 参见

  - [药品](../Page/药品.md "wikilink")
  - [保健品](../Page/保健品.md "wikilink")
  - [处方药](../Page/处方药.md "wikilink")、[非处方药（OTC）](../Page/非处方药.md "wikilink")、[中药](../Page/中药.md "wikilink")
  - [安慰剂](../Page/安慰剂.md "wikilink")
  - [联合国毒品和犯罪问题办公室](../Page/联合国毒品和犯罪问题办公室.md "wikilink")

## 外部链接

  - [Drugs.com](http://drugs.com/)
  - [MIMS](http://www.mims.com/)
  - [DrugBank](http://drugbank.ca/)

[药物](../Category/药物.md "wikilink")
[Category:药学](../Category/药学.md "wikilink")

1.

2.  <http://www.forbes.com/sites/robertlaura/2013/12/26/will-your-retirement-home-have-a-liquor-license/>

3.

4.  According to the
    [statistic](http://faostat.fao.org/site/567/DesktopDefault.aspx?PageID=567)
     of the [Food and Agriculture
    Organization](../Page/Food_and_Agriculture_Organization.md "wikilink")
    the production quantity in 2006 of coffee was 7.8 million tonnes and
    of tobacco was 6.7 million tonnes.

5.  ["Drug."](http://www.merriam-webster.com/concise/drug)  *Merriam
    Webster: Concise Encyclopedia*

6.  ["Drug."](http://dictionary.reference.com/browse/drug)
    *Dictionary.com Unabridged (v 1.1)*, [Random House,
    Inc.](../Page/Random_House.md "wikilink"), via dictionary.com.
    Retrieved on 20 September 2007.

7.

8.

9.
10. ["Drug."](http://dictionary.reference.com/browse/drug) *The American
    Heritage Science Dictionary*, [Houghton Mifflin
    Company](../Page/Houghton_Mifflin_Company.md "wikilink"), via
    dictionary.com. Retrieved on 20 September 2007.

11.

12. <http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3181622/>

13.

14.

15. ["MHRA Side Effects of
    Medicines."](http://www.mhra.gov.uk/Safetyinformation/Generalsafetyinformationandadvice/Adviceandinformationforconsumers/Sideeffectsofmedicines/)
    *MHRA Side Effects of Medicines*,

16.

17.