\(\)

**HK
MP5**系列是由[德國軍械廠](../Page/德國.md "wikilink")[黑克勒-科赫所設計及製造的](../Page/黑克勒-科赫.md "wikilink")[衝鋒槍](../Page/衝鋒槍.md "wikilink")，是黑克勒-科赫最著名及製造量最多的槍械產品。由於該系列衝鋒槍獲多國的軍隊、保安部隊、警隊選擇作為制式槍械使用，因此具有極高的知名度。

## 設計

[US_Patent_3283435_8-Nov-1966_BREECH_CLOSURE_Theodor_Koch.png](https://zh.wikipedia.org/wiki/File:US_Patent_3283435_8-Nov-1966_BREECH_CLOSURE_Theodor_Koch.png "fig:US_Patent_3283435_8-Nov-1966_BREECH_CLOSURE_Theodor_Koch.png")的專利圖片。\]\]
[Visierlinie.jpg](https://zh.wikipedia.org/wiki/File:Visierlinie.jpg "fig:Visierlinie.jpg")

早年[衝鋒槍普遍採用自由後坐式](../Page/衝鋒槍.md "wikilink")，以便於大量生產，但由於槍機品質較差，射擊時槍口跳動較大，準確性不佳。而MP5採用[HK
G3系列結構複雜的閉鎖槍機](../Page/HK_G3自動步槍.md "wikilink")，且採用傳統滾柱閉鎖機構來延遲開鎖，射擊時槍口跳動較小，準確性大大提高。

MP5所採用的[滾輪延遲反衝式技術源自](../Page/反沖作用_\(槍械\)#滾輪延遲反沖.md "wikilink")1940年代中期德國的[StG45(M)及後來的](../Page/StG45突擊步槍.md "wikilink")[CETME自動步槍](../Page/CETME自動步槍.md "wikilink")（G3的原型），這些設計亦影響後來衝鋒槍市場的定位。

標準型的MP5發射[9×19毫米魯格弹](../Page/9×19mm魯格彈.md "wikilink")，採用塑料固定[槍托或金屬伸縮槍托](../Page/槍托.md "wikilink")，配15或30發彈匣，它的扳機有多种發射選擇摸式，包括連發、單發、兩或[三發點放](../Page/三發點放.md "wikilink")。

雖然有高命中精度、可靠、後座力低及威力適中的優點，但MP5結構複雜，容易故障，單價高昂且空槍較新一代的衝鋒槍重。MP5使用手槍子彈雖然在可能發生的混戰或匪徒脅持人質的場面中防止誤殺隊友或人質，但無法有效貫穿[防彈衣](../Page/防彈衣.md "wikilink")，而且射程不遠，難以應付較遠距離著防彈衣的敵人。

## 歷史

MP5的原設計來自在1964年[黑克勒-科赫以](../Page/黑克勒-科赫.md "wikilink")[HK
G3的設計縮小而成HK](../Page/HK_G3自動步槍.md "wikilink")54衝鋒槍項目（Project
64）。

HK54其中5為H\&K的第五代衝鋒槍，4解為使用[9×19毫米子槍子彈](../Page/9×19mm鲁格弹.md "wikilink")，西德政府採用後正式命名為MP5，[瑞士同年成為第一个德國以外用MP](../Page/瑞士.md "wikilink")5的國家。由於高質素設計及高可靠性，MP5在推出後便成為了多國軍隊、警隊及保安部隊的制式冲锋枪，而黑克勒-科赫亦不斷改良及開發更多、共120多种版本。在1990年後期，更推出了為特定用戶開發的[10毫米Auto及](../Page/10mm_Auto.md "wikilink")[.40
S\&W版本](../Page/.40_S&W.md "wikilink")（MP5/10及MP5/40），在1970年代至2000年，MP5系列更一直保持其用戶數量及衝鋒槍的領導地位。

MP5現今在[土耳其](../Page/土耳其.md "wikilink")、[希臘](../Page/希臘.md "wikilink")、[巴基斯坦](../Page/巴基斯坦.md "wikilink")、[緬甸](../Page/緬甸.md "wikilink")、[伊朗等部分國家及一些小廠都獲授權生產或私自仿製及使用](../Page/伊朗.md "wikilink")，有些國家更為其產品重新命名（但槍枝本身沒有改變），[中華人民共和國非授權仿製的](../Page/中華人民共和國.md "wikilink")[NR-08](../Page/NR-08衝鋒槍.md "wikilink")／[CS/LS3型冲锋枪及](../Page/CS/LS3型冲锋枪.md "wikilink")[美國小廠所拼裝的](../Page/美國.md "wikilink")[SWA5衝鋒槍就是個私自仿製品的好例子](../Page/SWA5衝鋒槍.md "wikilink")。但獲授權生產或私自仿製品大都是早期A2、A3型，無三發點放裝置，並且手工較粗糙、內部結構不同及可靠性較差，無HK原廠製造的細膩。

## 型號

[Mp5_4.JPG](https://zh.wikipedia.org/wiki/File:Mp5_4.JPG "fig:Mp5_4.JPG")
[MP5A3_Marinir.jpg](https://zh.wikipedia.org/wiki/File:MP5A3_Marinir.jpg "fig:MP5A3_Marinir.jpg")
[MP3K_ImgID1.jpg](https://zh.wikipedia.org/wiki/File:MP3K_ImgID1.jpg "fig:MP3K_ImgID1.jpg")
[MP5SD.JPG](https://zh.wikipedia.org/wiki/File:MP5SD.JPG "fig:MP5SD.JPG")
[HK94k_on_Finnish_camo.jpg](https://zh.wikipedia.org/wiki/File:HK94k_on_Finnish_camo.jpg "fig:HK94k_on_Finnish_camo.jpg")
[Hkmp5k.jpg](https://zh.wikipedia.org/wiki/File:Hkmp5k.jpg "fig:Hkmp5k.jpg")
<small>註：「SEF」代表可連發、單發、保險扳機'''，伸縮[槍托為金屬製造](../Page/槍托.md "wikilink")、固定及摺疊槍托使用塑料製造。</small>

  - 原廠型號：

<!-- end list -->

  - A型號：

<!-- end list -->

  - MP5A1：可安裝附件的槍口；海軍版[扳機](../Page/扳機.md "wikilink")／「SEF」，使用直型彈匣。
  - MP5A2：固定槍托，海軍版扳機／「SEF」。
  - MP5A3：伸縮槍托，海軍版扳機／「SEF」，最廣泛使用的MP5型號。
  - MP5A4：固定槍托，A2的可[三點發扳機版本](../Page/三點發.md "wikilink")。
  - MP5A5：伸縮槍托，A3的可三點發扳機版本。

<!-- end list -->

  - SF（代表「單發」）型號：

<!-- end list -->

  - MP5SFA2：A2移除連發功能的半自動卡賓槍型號，固定槍托。
  - MP5SFA3：A3移除連發功能的半自動卡賓槍型號，伸縮槍托。

<!-- end list -->

  - 特製型號：

<!-- end list -->

  - MP5N：（「N」代表「Navy」，[海軍](../Page/海軍.md "wikilink")）專為[美國海軍製造的型號](../Page/美國海軍.md "wikilink")，裝有海軍版專用「SEF」扳機、前護木有金屬防滑紋，防海水腐蝕，三叉型槍口可裝上[消聲器](../Page/消聲器.md "wikilink")、伸縮槍托。
  - MP5F：（「F」代表「French」，[法國](../Page/法國.md "wikilink")）專為[法國軍隊及警隊](../Page/法國.md "wikilink")，為了減低射擊時後坐力帶來的不適，特別在槍托底板裝有軟塑料護板、前護木上有防滑紋。
  - MP5J：（「J」代表「Japan」，[日本](../Page/日本.md "wikilink")）專為[日本警隊製造的MP](../Page/日本.md "wikilink")5，最大特色就是可以單槍同時使用SEF及三點發扳機。
  - MP5/10：專為美國[聯邦調查局製造的型號](../Page/聯邦調查局.md "wikilink")（一些執法部門也有採用），發射[10毫米Auto](../Page/10mm_Auto.md "wikilink")，有A2、A3和N的版本，已停產但原來的用户可以享有HK公司的售後服務。
  - MP5/40：專為美國[聯邦調查局製造的型號](../Page/聯邦調查局.md "wikilink")，發射[.40
    S\&W](../Page/.40_S&W.md "wikilink")，已停產。
  - MP5 RAS：（「RAS」代表「Rail Attachment System」，導軌系統）裝上導軌系統的MP5。
  - MP5公事包型號:內藏MP5K的公事包，可由公事包提把上的按制連動扳機發射。

<!-- end list -->

  - K（代表「短」）型號：

<!-- end list -->

  - MP5K：（「K」代表「短」，德文稱為「」）超短型的MP5；裝有前握把、全長只有325毫米、「SEF」。
  - MP5KA1：裝有簡易片形照門的MP5K；「SEF」。
  - MP5KA4：MP5K的可三點發版本，全MP5系列最小的型號。
  - MP5KA5：MP5KA1的可三點發版本。
  - MP5K-N：專為[美國海軍製造的型號](../Page/美國海軍.md "wikilink")，裝有海軍版專用「SEF」扳機、防海水腐蝕，三叉型槍口可裝上[消聲器](../Page/消聲器.md "wikilink")，沒有槍托。
  - MP5K-PDW：MP5K的[個人防衛武器版本](../Page/個人防衛武器.md "wikilink")；加裝摺疊槍托、可裝上[消聲器的三叉型槍口及](../Page/消聲器.md "wikilink")[三點發](../Page/三點發.md "wikilink")，於1991年首次推出。

<!-- end list -->

  - SD（代表「消聲」）型號：

<!-- end list -->

  - MP5SD1：（「SD」），原廠製造的整體槍管[消聲器型號](../Page/消聲器.md "wikilink")；海軍版[扳機](../Page/扳機.md "wikilink")／「SEF」。
  - MP5SD2：同樣裝有整體槍管[消聲器](../Page/消聲器.md "wikilink")、固定槍托；海軍版[扳機](../Page/扳機.md "wikilink")／「SEF」。
  - MP5SD3：裝有整體槍管[消聲器](../Page/消聲器.md "wikilink")、伸縮槍托；海軍版[扳機](../Page/扳機.md "wikilink")／「SEF」。
  - MP5SD4：MP5SD1的可三點發扳機版本。
  - MP5SD5：MP5SD2的可三點發扳機版本。
  - MP5SD6：MP5SD3的可三點發扳機版本。
  - MP5SD-N：MP5SD的海軍版本、KAC護木、[不鏽鋼整體槍管](../Page/不鏽鋼.md "wikilink")[消聲器](../Page/消聲器.md "wikilink")；伸縮槍托。

<!-- end list -->

  - 民用型號：

<!-- end list -->

  - HK94：美國本土發售的民用型版本，配有單發/保險扳機及16寸槍管，有三種不同版本-A2、A3及SG1，已停產但至今還可以用5,000美元或以上的高價買到。值得一提的是，大量半自動的HK94A2/A3被割短長管和改裝過，在1980年代至1990年代的[動作片出現以代表全自動的MP](../Page/動作片.md "wikilink")5。
  - SP89：（M1989運動手槍「）。民用型MP5K、特別製造的前護木以符合美國在1989年的法例，已停產。

<!-- end list -->

  - 其他國家的授權生產或私自仿製型號：

<!-- end list -->

  - [SWA5衝鋒槍](../Page/SWA5衝鋒槍.md "wikilink")\[1\]\[2\]
  - [MP-10衝鋒槍](../Page/MP-10衝鋒槍.md "wikilink")：Special
    Weapons生產的另一版本的MP5，另有民用型SP-10。
  - [EAS
    MP5](../Page/EAS_MP5.md "wikilink")：由[希臘EAS工廠合法生產的版本](../Page/希臘.md "wikilink")。
  - [PK-9](../Page/PK-9.md "wikilink"):由[巴基斯坦POF工廠生產的版本](../Page/巴基斯坦.md "wikilink")。
  - [緬甸仿製的MP](../Page/緬甸.md "wikilink")5
  - [DIO
    MP5](../Page/DIO_MP5.md "wikilink")：由[伊朗](../Page/伊朗.md "wikilink")[DIO工廠仿製的MP](../Page/DIO.md "wikilink")5。
  - [Tihraga](../Page/Tihraga衝鋒槍.md "wikilink"):
    [蘇丹製MP](../Page/蘇丹.md "wikilink")5，由MIC工廠生產。
  - [SEDENA
    MP5](../Page/SEDENA_MP5.md "wikilink")：由[墨西哥國營兵工廠](../Page/墨西哥.md "wikilink")[SEDENA生產的版本](../Page/SEDENA.md "wikilink")。
  - [INDEP
    MP5](../Page/INDEP_MP5.md "wikilink")：[葡萄牙](../Page/葡萄牙.md "wikilink")[INDEP公司生產的版本](../Page/INDEP.md "wikilink")。
  - [沙地阿拉伯製MP](../Page/沙地阿拉伯.md "wikilink")5
  - [MAS
    MP5F](../Page/MAS_MP5F.md "wikilink")：由[法國](../Page/法國.md "wikilink")[聖艾蒂安兵工廠授權生產的MP](../Page/聖艾蒂安.md "wikilink")5F。
  - [T94](../Page/T94衝鋒槍.md "wikilink")：[土耳其MKE工廠合法生產的MP](../Page/土耳其.md "wikilink")5，有MP5A3、A2、K等部分型號。
  - [塞爾維亞仿製的MP](../Page/塞爾維亞.md "wikilink")5K
  - [MPT9衝鋒槍](../Page/MPT9衝鋒槍.md "wikilink")：[伊朗DIO工廠的MP](../Page/伊朗.md "wikilink")5、MP5K仿制版本，有MPT9S/9
    mm LIGHT其他型號。\[3\]。
  - [NR-08](../Page/NR-08衝鋒槍.md "wikilink")：[中華人民共和國的非授權仿制版](../Page/中華人民共和國.md "wikilink")，由[北方工業生產](../Page/北方工業.md "wikilink")，目前已知裝備在[重慶閃電反恐突擊隊](../Page/重慶閃電反恐突擊隊.md "wikilink")。
  - [CS/LS3](../Page/CS/LS3型冲锋枪.md "wikilink")：同樣是[中華人民共和國的非授權仿制版](../Page/中華人民共和國.md "wikilink")，由[中国兵器装备集团生產](../Page/中国兵器装备集团.md "wikilink")，目前已知裝備在[广西](../Page/广西壮族自治区.md "wikilink")[公安](../Page/公安机关.md "wikilink")[特警队](../Page/特種警察.md "wikilink")。
  - [Brügger & Thomet
    MP5](../Page/B&T_MP5衝鋒槍.md "wikilink")：由[瑞士Brügger](../Page/瑞士.md "wikilink")
    & Thomet公司生產的MP5。
  - [美國版HK](../Page/美國.md "wikilink")
    MP5：由美國的[黑克勒-科赫公司生產的MP](../Page/黑克勒-科赫.md "wikilink")5，當中包含了許多不同的型號。
  - [GSG-5卡賓槍](../Page/GSG-5卡賓槍.md "wikilink")：由德國運動槍有限公司研製及生產的一種.22口徑半自動民用型卡賓槍。

## 使用國家和地區

[Police.gun.1.london.arp.jpg](https://zh.wikipedia.org/wiki/File:Police.gun.1.london.arp.jpg "fig:Police.gun.1.london.arp.jpg")槍械授權警員使用MP5SFA3\]\]
[US_Navy_SEALs_in_from_water.jpg](https://zh.wikipedia.org/wiki/File:US_Navy_SEALs_in_from_water.jpg "fig:US_Navy_SEALs_in_from_water.jpg")裝備的MP5N\]\]
[UTK_PGK_on_standby.JPG](https://zh.wikipedia.org/wiki/File:UTK_PGK_on_standby.JPG "fig:UTK_PGK_on_standby.JPG")
[GIGN4_Domenjod_160316.jpg](https://zh.wikipedia.org/wiki/File:GIGN4_Domenjod_160316.jpg "fig:GIGN4_Domenjod_160316.jpg")隊員，他們分別手持[HK416突擊步槍和HK](../Page/HK416突擊步槍.md "wikilink")
MP5\]\]
[Romanian_soldiers_on_parade.jpg](https://zh.wikipedia.org/wiki/File:Romanian_soldiers_on_parade.jpg "fig:Romanian_soldiers_on_parade.jpg")
[A_group_of_Chilean_Navy_special_unit.jpg](https://zh.wikipedia.org/wiki/File:A_group_of_Chilean_Navy_special_unit.jpg "fig:A_group_of_Chilean_Navy_special_unit.jpg")
[US_special_forces_playing_pool_at_a_village_in_Afghanistan.jpg](https://zh.wikipedia.org/wiki/File:US_special_forces_playing_pool_at_a_village_in_Afghanistan.jpg "fig:US_special_forces_playing_pool_at_a_village_in_Afghanistan.jpg")
[AKM_and_MP5K.JPEG](https://zh.wikipedia.org/wiki/File:AKM_and_MP5K.JPEG "fig:AKM_and_MP5K.JPEG")\]\]

  -
  -   -
  -   - 空降突擊師
      - 國家安全局特別干預組

  -   - 警察干預小組

  -   - [阿根廷武裝部隊](../Page/阿根廷武裝部隊.md "wikilink")
      - 特警單位

  -   - [澳大利亞皇家空軍](../Page/澳大利亞皇家空軍.md "wikilink")

      - [澳大利亞國防軍](../Page/澳大利亞國防軍.md "wikilink")

      -
  -   - [奧地利聯邦內政部](../Page/奧地利聯邦內政部.md "wikilink")[眼鏡蛇作戰司令部](../Page/眼鏡蛇作戰司令部.md "wikilink")

  -   - 內部警衛隊

  -
  -
  -   -
      -
      -
      -
  -   - 白羅斯內務部[鑽石反恐特別單位](../Page/鑽石反恐特別單位.md "wikilink")\[4\]\[5\]
      - [白羅斯共和國國家安全委員會](../Page/白羅斯共和國國家安全委員會.md "wikilink")[阿爾法小組](../Page/阿爾法小組_\(白羅斯\).md "wikilink")（A3）\[6\]

  -   - 特警單位

  -
  -   - [巴西武裝部隊特種部隊單位](../Page/巴西軍事.md "wikilink")

      - [巴西聯邦警察](../Page/巴西聯邦警察.md "wikilink")

      - [特別警察行動營](../Page/特別警察行動營.md "wikilink")

  -   - 特警單位

  -
  -   - [加拿大皇家騎警](../Page/加拿大皇家騎警.md "wikilink")
      - 特警單位
      - [加拿大陸軍](../Page/加拿大陸軍.md "wikilink")[第二聯合特遣部隊](../Page/第二聯合特遣部隊.md "wikilink")
      - Bruce Power核能反應組

  -
  -   - 公安特警單位（其仿製型[NR-08與](../Page/NR-08衝鋒槍.md "wikilink")[CS/LS3型衝鋒鎗](../Page/CS/LS3型衝鋒鎗.md "wikilink")）
      - [武警特警学院](../Page/中國人民武裝警察部隊特種警察學院.md "wikilink")

  -   - 特警單位

  -
  -   -
  -   - 特種部隊\[7\]

  -
  -   -
      -
  -   - [德國人民警察](../Page/德國人民警察.md "wikilink")[第9連](../Page/第9勤務部隊.md "wikilink")\[8\]

  -   -
      - [丹麥國防軍](../Page/丹麥軍事.md "wikilink")

  -
  -   - 總統衛隊

  -   -
  -   -
      - 特警單位

  -
  -
  -
  -   - [芬蘭國防軍](../Page/芬蘭國防軍.md "wikilink")

      -
      -
  -   - [法國武裝部隊](../Page/法國軍事.md "wikilink")

      - [法國外籍兵團](../Page/法國外籍兵團.md "wikilink")

      -
  -
  -   - [德國聯邦警察](../Page/德國聯邦警察.md "wikilink")

      - [特別行動突擊隊](../Page/特別行動突擊隊.md "wikilink")

      -
      - [德國聯邦國防軍特種部隊單位](../Page/德國聯邦國防軍.md "wikilink")

  -
  -   - [希臘警察](../Page/希臘警察.md "wikilink")
      - [希臘武裝部隊](../Page/希臘軍事.md "wikilink")

  -
  - （2017年11月16日，根据消息人士透露，制造商黑克勒-科赫公司拒絕了香港警方在年初购买一批MP5衝鋒槍和配件的请求。有报道指出，德國媒體DPA于2016年11月曾引用一名黑克勒-科赫不具名高層的话，他说公司希望只向[民主且非](../Page/民主.md "wikilink")[腐敗的政府售賣槍械](../Page/政治腐败.md "wikilink")，而它們必須是[北約成員國或其盟友](../Page/北約.md "wikilink")。一些观点认为，这或与香港近年[人权和和平](../Page/人权.md "wikilink")[集会权利受损有关](../Page/集会.md "wikilink")。[香港保安局局長李家超拒绝就报道发布评论](../Page/香港保安局.md "wikilink")，并表示市场仍有很多选择，此事不会影响执法机构运作。\[9\]\[10\]\[11\]\[12\]）

      - [香港警務處](../Page/香港警務處.md "wikilink")（[衝鋒隊](../Page/衝鋒隊_\(香港\).md "wikilink")、[水警總區](../Page/水警總區.md "wikilink")、[小艇分區](../Page/小艇分區.md "wikilink")、[有組織罪案及三合會調查科](../Page/有組織罪案及三合會調查科.md "wikilink")、[毒品調查科](../Page/毒品調查科.md "wikilink")、[刑事情報科](../Page/刑事情報科.md "wikilink")、[跟蹤支援隊](../Page/跟蹤支援隊.md "wikilink")、[保護證人組](../Page/保護證人組.md "wikilink")、[要員保護組](../Page/要員保護組.md "wikilink")、[機場特警組](../Page/機場特警組.md "wikilink")、[反恐特勤隊及](../Page/反恐特勤隊.md "wikilink")[特別任務連](../Page/特別任務連.md "wikilink")）
      - [香港海關](../Page/香港海關.md "wikilink")

  -   - [匈牙利國防軍](../Page/匈牙利軍事.md "wikilink")
      - 特警單位

  -   -
      - 危機應變部隊

      -
  -   - [印度陸軍](../Page/印度陸軍.md "wikilink")

      - [印度海軍](../Page/印度海軍.md "wikilink")

      -
      -
      - 地方警察的特警單位

      -
  -   - [印尼陸軍](../Page/印尼陸軍.md "wikilink")

      -
      - [印尼空軍](../Page/印尼空軍.md "wikilink")

      -
  -   - [伊朗伊斯蘭共和國武裝部隊](../Page/伊朗軍事.md "wikilink")（使用DIO仿製版本）

      - （使用DIO仿製版本）

  -
  -
  -   - [愛爾蘭陸軍遊騎兵](../Page/愛爾蘭陸軍遊騎兵.md "wikilink")

      -
      - [愛爾蘭和平衛隊](../Page/愛爾蘭和平衛隊.md "wikilink")

      -
  -   - [卡賓槍騎兵](../Page/卡賓槍騎兵.md "wikilink")[特別干預組](../Page/特別干預組.md "wikilink")

      -
      - [義大利武裝部隊特種部隊單位](../Page/義大利軍事.md "wikilink")

  -   -
  -   - [海上自衛隊](../Page/海上自衛隊.md "wikilink")
      - [日本警察廳多個部門](../Page/日本警察廳.md "wikilink")（包括：[特殊急襲部隊](../Page/特殊急襲部隊.md "wikilink")、特殊搜查班、特殊犯捜査系及各都道府縣槍械對策部隊）
      - [日本警察廳](../Page/日本警察廳.md "wikilink")[皇宮警察本部](../Page/皇宮警察本部.md "wikilink")
      - [陸上自衛隊特殊作戰群](../Page/陸上自衛隊特殊作戰群.md "wikilink")
      - [海上保安廳](../Page/海上保安廳.md "wikilink")[特殊警備隊](../Page/特殊警備隊.md "wikilink")

  -
  -   - 特種部隊單位\[13\]

  -   -
  -
  -
  -   - 特種部隊單位

  -   -
  -
  -   - 特警單位
      - 保安部隊

  -   -
      -
  -   -
  -   - [治安警察局多個部門](../Page/治安警察局.md "wikilink")（包括：[特警隊](../Page/特警隊_\(澳門\).md "wikilink")、[保護重要人物及設施組](../Page/保護重要人物及設施組.md "wikilink")、[特別行動組](../Page/特別行動組_\(澳門\).md "wikilink")、[特別巡邏組](../Page/特別巡邏組.md "wikilink")）
      - [司法警察局](../Page/司法警察局.md "wikilink")
      - [懲教管理局](../Page/懲教管理局.md "wikilink")

  -   - 特別任務單位

      - 特別行動團

  -   - [馬來西亞武裝部隊](../Page/馬來西亞武裝部隊.md "wikilink")

      - [馬來西亞皇家警察](../Page/馬來西亞皇家警察.md "wikilink")

      - [馬來西亞監獄署](../Page/馬來西亞監獄署.md "wikilink")

      -
  -   -
      - 特警單位

  -
  -   - [墨西哥武裝部隊特種部隊單位](../Page/墨西哥武裝部隊.md "wikilink")

      -
  -
  -   -
      -
      -
  -
  -   - [荷蘭軍隊](../Page/荷蘭軍隊.md "wikilink")

      - [荷蘭皇家憲兵隊](../Page/荷蘭皇家憲兵隊.md "wikilink")

      -
  -
  -
  -   -
      -
  -
  -
  -
  -   - [挪威國防軍](../Page/挪威國防軍.md "wikilink")（被[MP7所取代](../Page/HK_MP7衝鋒槍.md "wikilink")\[14\]）

      -
  -
  -   - [巴基斯坦武裝部隊](../Page/巴基斯坦軍事.md "wikilink")（採用合法生產版本）
      - 機場保安部隊
      - 要員保護單位

  -
  -
  -   - [菲律賓武裝部隊](../Page/菲律賓軍事.md "wikilink")
      - [菲律賓國家警察](../Page/菲律賓國家警察.md "wikilink")

  -   - [波蘭武裝部隊特種部隊單位](../Page/波蘭武裝部隊.md "wikilink")

      -
      -
  -   - [特別行動組](../Page/特別行動組_\(葡萄牙\).md "wikilink")

      - [葡萄牙武裝部隊](../Page/葡萄牙軍事.md "wikilink")

  -
  -   - [中華民國陸軍](../Page/中華民國陸軍.md "wikilink")[空降勤務中隊](../Page/涼山特勤隊.md "wikilink")
      - [中華民國海軍陸戰隊特種勤務中隊](../Page/中華民國海軍陸戰隊.md "wikilink")
      - [憲兵特勤隊](../Page/憲兵特勤隊.md "wikilink")
      - [維安特勤隊](../Page/維安特勤隊.md "wikilink")（但是其中有一部分是[SWA5衝鋒槍](../Page/SWA5衝鋒槍.md "wikilink")）
      - [中華民國特種警察](../Page/中華民國特種警察.md "wikilink")
      - 台北市刑事警察大隊特勤中隊
      - 高雄市刑事警察大隊特勤中隊
      - [國家安全局特勤中心](../Page/國家安全局.md "wikilink")

  -   - 特種作戰營

      -
  -   - [俄羅斯聯邦安全局](../Page/俄羅斯聯邦安全局.md "wikilink")[阿爾法小組](../Page/阿爾法小組.md "wikilink")（A3）\[15\]\[16\]
      - [俄羅斯聯邦武裝力量特種作戰部隊](../Page/俄羅斯聯邦武裝力量特種作戰部隊.md "wikilink")\[17\]

  -   - 特種部隊單位 （採用合法生產版本）

  -   - [塞爾維亞內務部](../Page/塞爾維亞內務部.md "wikilink")

      - [塞爾維亞內務部](../Page/塞爾維亞內務部.md "wikilink")

      - [塞爾維亞武裝部隊](../Page/塞爾維亞武裝部隊.md "wikilink")

      -
  -   -
      - [新加坡警察部隊](../Page/新加坡警察部隊.md "wikilink")

  -   -
  -   -
      -
  -   - [特別任務部隊](../Page/南非警察服務特別任務部隊.md "wikilink")

  -   - [大韓民國陸軍](../Page/大韓民國陸軍.md "wikilink")[第707特殊任務營](../Page/第707特殊任務營.md "wikilink")
      - [大韓民國海軍特戰戰團](../Page/大韓民國海軍特戰戰團.md "wikilink")
      - 特警單位

  -   -
      -
      - [西班牙海軍](../Page/西班牙海軍.md "wikilink")

  - \[18\]

  - ：採用[合法生產版本](../Page/合法生產.md "wikilink")

  -   -
      - [瑞典國防軍特種部隊單位](../Page/瑞典國防軍.md "wikilink")

  -   - [瑞士陸軍](../Page/瑞士陸軍.md "wikilink")
      - [瑞士空軍](../Page/瑞士空軍.md "wikilink")
      - 特警單位

  -   - [泰國皇家警察](../Page/泰國皇家警察.md "wikilink")

      -
  -
  -
  -   - [土耳其武裝部隊](../Page/土耳其武裝部隊.md "wikilink")（採用合法生產版本）

      - （採用合法生產版本）

  -   - 總統衛隊
      - 內務部歐米茄特種部隊
      - [烏克蘭安全局](../Page/烏克蘭安全局.md "wikilink")[阿爾法小組](../Page/阿爾法小組_\(烏克蘭\).md "wikilink")\[19\]（A3）

  -
  -   - 安全服務隊

  -   - [英國特種部隊](../Page/英國特種部隊.md "wikilink")（A3、SD、K）

      - [倫敦警察廳](../Page/倫敦警察廳.md "wikilink")[專業槍械司令部](../Page/專業槍械司令部.md "wikilink")（SF）

      - 槍械授權警員（SF）

      - 武裝警察單位

      -
  -   - [美國武裝部隊](../Page/美軍.md "wikilink")
      - [美國特種作戰司令部](../Page/美國特種作戰司令部.md "wikilink")
      - [聯邦調查局](../Page/聯邦調查局.md "wikilink")
      - [美國特勤局](../Page/美國特勤局.md "wikilink")
      - [美國緝毒局](../Page/美國緝毒局.md "wikilink")
      - 多個聯邦及地方執法部門

  -
  -   - [瑞士近衛隊](../Page/瑞士近衛隊.md "wikilink")
      - [梵蒂岡憲兵](../Page/梵蒂岡憲兵.md "wikilink")

  -
  -   - 武裝警察（A3、A4、K）

  -
## 軼事

[RAF-Logo.svg](https://zh.wikipedia.org/wiki/File:RAF-Logo.svg "fig:RAF-Logo.svg")
MP5出現在[恐怖組織](../Page/恐怖組織.md "wikilink")[赤軍旅的標誌當中](../Page/紅軍派.md "wikilink")，其生存遊戲槍也非常的普遍並受到某些玩家的喜愛。

在[第一人稱射擊遊戲之中](../Page/第一人称射击游戏.md "wikilink")，自MP5於[維爾福軟體公司推出的](../Page/維爾福.md "wikilink")《半條命》遊戲出現以後，不少射擊遊戲都會出現其身影。

在[尼泊爾王室槍擊事件當中](../Page/尼泊爾王室槍擊事件.md "wikilink")，MP5為王儲[狄潘德拉發動襲擊時所使用的武器之一](../Page/狄潘德拉.md "wikilink")。

## 流行文化

MP5系列也出現在許多[電影](../Page/電影.md "wikilink")、[電視劇](../Page/電視劇.md "wikilink")、[電子遊戲和](../Page/電子遊戲.md "wikilink")[動畫當中](../Page/動畫.md "wikilink")，並常常作為作品中登場的[特種部隊隊員或精兵所持有的武器](../Page/特種部隊.md "wikilink")，不過有時也會落入平民及[恐怖份子手上](../Page/恐怖份子.md "wikilink")。當中部份作品亦展示了該槍的特殊上膛方式“HK拍擊”（HK
Slap）。

### [電影](../Page/電影.md "wikilink")

  - 1985年—《》（Rambo First Blood: Part
    II）：型號為HK94，改裝至類似於MP5A3並裝上瞄準鏡，約翰.藍波（[席維斯·史特龍飾演](../Page/席維斯·史特龍.md "wikilink")）的任務武器，會使用“HK拍擊”上膛，然而在著陸期間連同一半裝備丢失了。
  - 1986年—《》（The Fly II）：型號為HK94A3，改裝至類似於MP5A3，被研究所的守衞所使用。
  - 1991年—《[逃學威龍](../Page/逃學威龍.md "wikilink")》：型號為MP5A3，被周星星（[周星馳飾演](../Page/周星馳.md "wikilink")）在內的[特別任務連隊員所使用](../Page/特別任務連.md "wikilink")。
  - 1991年—《[-{zh-hk:未來戰士續集;zh-cn:终结者2：审判日;zh-tw:魔鬼終結者2;}-](../Page/终结者2：审判日.md "wikilink")》（Terminator
    2: Judgement
    Day）：型號為HK94，改裝至類似於MP5A3，被[特種武器和戰術部隊隊員所使用](../Page/特種武器和戰術部隊.md "wikilink")。
  - 1996年—《[-{zh-hk:石破天驚;zh-cn:勇闯夺命岛;zh-tw:絕地任務;}-](../Page/絕地任務.md "wikilink")》：被[美國海軍](../Page/美國海軍.md "wikilink")[海豹突擊隊所使用](../Page/海豹突擊隊.md "wikilink")。
  - 1997年—《[空軍一號](../Page/空軍一號_\(電影\).md "wikilink")》（Air Force
    One）：型號為MP5A3，被[美國總統及劫機的](../Page/美國總統.md "wikilink")[恐怖份子所使用](../Page/恐怖份子.md "wikilink")。
  - 1999年—《[-{zh-hk:廿二世紀殺人網絡;zh-cn:黑客帝国;zh-tw:駭客任務;}-](../Page/駭客任務.md "wikilink")》（The
    Matrix）：型號為MP5K，被湯瑪斯·安德森/尼奧（[基努李維飾演](../Page/基努李維.md "wikilink")）[雙持使用](../Page/雙持.md "wikilink")，會使用“HK拍擊”上膛。
  - 2001年—《[-{zh-hk:黑鷹十五小時;zh-cn:黑鹰坠落;zh-tw:黑鷹計劃;}-](../Page/黑鷹計劃.md "wikilink")》（Black
    Hawk
    Down）：型號為MP5A3，[黑鷹直升機超級](../Page/UH-60黑鷹直升機.md "wikilink")64號駕駛員麥可·杜蘭特（朗·艾達飾演）所使用。
  - 2002年—《[-{zh-hk:未來殺人網絡;zh-cn:撕裂的末日;zh-tw:重裝任務;}-](../Page/重裝任務.md "wikilink")》（Equilibrium）：型號為MP5A3和MP5K，被“清道夫”們所使用。
  - 2002年ㄧ《[雙瞳](../Page/雙瞳_\(電影\).md "wikilink")》：型號為MP5A5，被一群從記者身後經過的[中華民國特種警察所使用](../Page/中華民國特種警察.md "wikilink")。
  - 2002年—《[-{zh-cn:生化危机; zh-tw:惡靈古堡;
    zh-hk:生化危機之變種生還者}-](../Page/惡靈古堡_\(電影\).md "wikilink")》（Resident
    Evil）：型號為MP5K，裝上[紅點鏡](../Page/紅點鏡.md "wikilink")，被[保護傘公司的突擊隊員所使用](../Page/安布雷拉.md "wikilink")。
  - 2003年—《[-{zh-hk:廿二世紀殺人網絡2：決戰未來;zh-cn:黑客帝国2：重装上阵;zh-tw:駭客任務：重裝上陣;}-](../Page/黑客帝國2：重裝上陣.md "wikilink")》（The
    Matrix
    Reloaded）：型號為MP5A3及MP5K，被墨洛溫（[藍柏·威爾森飾演](../Page/藍柏·威爾森.md "wikilink")）的手下所使用，會使用“HK拍擊”上膛。
  - 2003年—《[-{zh-hk:廿二世紀殺人網絡3：驚變世紀;zh-cn:黑客帝国3：矩阵革命;zh-tw:駭客任務完結篇：最後戰役;}-](../Page/黑客帝國3：矩陣革命.md "wikilink")》（The
    Matrix
    Revolutions）：型號為MP5K，裝上[紅點鏡並移除了](../Page/紅點鏡.md "wikilink")[前握把](../Page/輔助握把.md "wikilink")，被莫菲斯（[勞倫斯·費許朋飾演](../Page/勞倫斯·費許朋.md "wikilink")）[雙持使用](../Page/雙持.md "wikilink")。
  - 2003年—《[-{zh-hk:未來戰士3：殲滅者TX;zh-cn:终结者3：机器的崛起;zh-tw:魔鬼終結者3;}-](../Page/魔鬼終結者3.md "wikilink")》（The
    Terminator: Rise of the Machines）
  - 2003年—《[-{zh-hk:特警雄風;zh-cn:反恐特警组;zh-tw:反恐特警組;}-](../Page/反恐特警组.md "wikilink")》（S.W.A.T.）：MP5A4被[特種武器和戰術部隊隊員所使用](../Page/特種武器和戰術部隊.md "wikilink")，而MP5K則被暴徒所使用。
  - 2004年ㄧ《[-{zh-cn:谍影重重2; zh-tw:神鬼認證：神鬼疑雲;
    zh-hk:叛諜追擊2：機密圈套;}-](../Page/神鬼認證：神鬼疑雲.md "wikilink")》（The
    Bourne
    Supremacy）：型號為MP5A3與MP5SD3，被[德國聯邦警察第九國境守備隊所使用](../Page/德國聯邦警察第九國境守備隊.md "wikilink")。
  - 2004年—《[-{zh-cn:生化危机2：启示录; zh-tw:惡靈古堡2：啟示錄;
    zh-hk:生化危機之殲滅生還者}-](../Page/惡靈古堡2：啟示錄.md "wikilink")》（Resident
    Evil:
    Apocalypse）：[兩支鍍銀](../Page/雙持.md "wikilink")[上機匣的MP](../Page/上機匣.md "wikilink")5K被[艾莉絲](../Page/艾莉絲_\(惡靈古堡\).md "wikilink")（[蜜拉·喬娃薇琪飾演](../Page/蜜拉·喬娃薇琪.md "wikilink")）所使用；MP5A3被S.T.A.R.S.隊員所使用。
  - 2005年—《[-{zh-hans:战争之王;zh-hant:軍火之王;}-](../Page/軍火之王.md "wikilink")》（Lord
    of War）：型號為MP5A2/A3/K，被[國際刑警探員所使用](../Page/國際刑警.md "wikilink")。
  - 2005年—《[-{zh-hk:恐懼鬥室2死亡困局;zh-cn:电锯惊魂2;zh-tw:奪魂鋸2;}-](../Page/奪魂鋸2.md "wikilink")》（Saw
    II）：型號為MP5A3，被[特種武器和戰術部隊隊員所使用](../Page/特種武器和戰術部隊.md "wikilink")。
  - 2006年—《[-{zh-hk:職業特工隊3;zh-cn:碟中谍3;zh-tw:不可能的任務3;}-](../Page/不可能的任務3.md "wikilink")》（Mission:
    Impossible 3）：型號為MP5K
    PDW，被[伊森·韓特](../Page/伊森·韓特.md "wikilink")（[湯·告魯斯飾演](../Page/湯·告魯斯.md "wikilink")）所使用。
  - 2006年—《[-{zh-hk:恐懼鬥室3死神在齒;zh-cn:电锯惊魂3;zh-tw:奪魂鋸3;}-](../Page/奪魂鋸3.md "wikilink")》（Saw
    III）：型號為MP5A3，裝上[電筒和](../Page/電筒.md "wikilink")[激光指示器](../Page/激光指示器.md "wikilink")，被[特種武器和戰術部隊隊員所使用](../Page/特種武器和戰術部隊.md "wikilink")。
  - 2006年—《[-{zh-tw:快克殺手;
    zh-cn:怒火攻心;}-](../Page/快克殺手.md "wikilink")》（Crank）：型號為MP5A3及MP5A4，前者裝上[紅點鏡](../Page/紅點鏡.md "wikilink")，被唐·金的手下所使用，後者被[特種武器和戰術部隊隊員所使用](../Page/特種武器和戰術部隊.md "wikilink")。
  - 2007年—《[-{zh-hant:精銳部隊;
    zh-hans:精英部队;}-](../Page/精銳部隊.md "wikilink")》（Tropa
    de
    Elite）：型號為MP5A3，被[巴西](../Page/巴西.md "wikilink")[特別警察行動營隊長納西門圖所使用](../Page/特別警察行動營.md "wikilink")。
  - 2008年—《[-{zh-hk:叛諜同謀;zh-cn:谎言之躯;zh-tw:謊言對決;}-](../Page/謊言對決.md "wikilink")》（Body
    of
    Lies）：型號為MP5A3和MP5K-PDW，前者被[英國](../Page/英國.md "wikilink")[武裝特警隊隊員所使用](../Page/武裝特警隊.md "wikilink")，後者被[約旦](../Page/約旦.md "wikilink")[特種部隊隊員所使用](../Page/特種部隊.md "wikilink")。
  - 2009年—《[-{zh-hk:未來戰士2018;zh-cn:终结者2018;zh-tw:魔鬼終結者：未來救贖;}-](../Page/魔鬼終結者：未來救贖.md "wikilink")》（Terminator
    Salvation）：型號為MP5SD6，被一名生還者所使用。
  - 2010年—《》（L'assaut）：型號為MP5A3，被[法國國家憲兵干預隊隊員所使用](../Page/法國國家憲兵干預隊.md "wikilink")。
  - 2010年—《[-{zh-hk:生化危機3D：戰神再生;zh-cn:生化危机4：来生;zh-tw:惡靈古堡4：陰陽界;}-](../Page/惡靈古堡4：陰陽界.md "wikilink")》（Resident
    Evil:
    Afterlife）：MP5K被艾莉絲（[蜜拉·喬娃薇琪飾演](../Page/蜜拉·喬娃薇琪.md "wikilink")）所使用。
  - 2010年—《[-{zh-hk:轟天猛將; zh-tw:浴血任務;
    zh-cn:敢死队;}-](../Page/浴血任務.md "wikilink")》（The
    Expendables）：型號為MP5K和MP5K-PDW，前者被托爾·羅德（[蘭迪·寇楚飾演](../Page/蘭迪·寇楚.md "wikilink")）所使用，後者被李·聖誕（[傑森·史塔森飾演](../Page/傑森·史塔森.md "wikilink")）所使用。
  - 2011年—《》（Forces
    spéciales）：型號為MP5SD3、MP5KA4及MP5A5，裝上各種戰術配件，被[法國海軍](../Page/法國海軍.md "wikilink")隊員所使用。
  - 2012年—《[-{zh-hk:海豹突擊隊; zh-tw:海豹神兵：英勇行動;
    zh-cn:勇者行动;}-](../Page/海豹神兵：英勇行動.md "wikilink")》（Act
    of
    Valor）：型號分別為MP5A2及MP5A4。MP5A2被多名在[哥斯達黎加的營救行動中的敵方守衛所使用](../Page/哥斯達黎加.md "wikilink")；MP5A4則被[墨西哥特種部隊隊員所使用](../Page/墨西哥.md "wikilink")。
  - 2012年—《[-{zh-hk:追擊拉登行動; zh-tw:00:30凌晨密令;
    zh-cn:猎杀本·拉登;}-](../Page/00:30凌晨密令.md "wikilink")》（Zero
    Dark Thirty）：型號為MP5A2，被一名槍手所使用。
  - 2012年—《[寒戰](../Page/寒戰.md "wikilink")》（Cold
    War）：型號為MP5A3和MP5A2，分別被[特別任務連和](../Page/特別任務連.md "wikilink")[香港警察所使用](../Page/香港警務處.md "wikilink")。
  - 2013年—《[-{zh-hk:白宮淪陷; zh-tw:全面攻佔：倒數救援;
    zh-cn:奥林匹斯的陷落;}-](../Page/全面攻佔：倒數救援.md "wikilink")》（Olympus
    Has
    Fallen）：型號為MP5A3，裝上瞄準鏡和[戰術燈](../Page/戰術燈.md "wikilink")，被[朝鮮](../Page/朝鮮.md "wikilink")[恐怖份子所使用](../Page/恐怖份子.md "wikilink")，亦曾被主角邁克·班寧（[傑拉德·巴特勒飾演](../Page/傑拉德·巴特勒.md "wikilink")）所繳獲。
  - 2014年—《[國定殺戮日：無法無天](../Page/國定殺戮日：無法無天.md "wikilink")》（The Purge:
    Anarchy）：型號為MP5A3和MP5K，被殺戮者所使用，其中一支A3亦被主角李歐·巴恩斯（[法蘭克·葛里洛飾演](../Page/法蘭克·葛里洛.md "wikilink")）所繳獲。
  - 2014年—《[-{zh-hk:轟天猛將3; zh-tw:浴血任務3;
    zh-cn:敢死队3;}-](../Page/敢死队3.md "wikilink")》（The
    Expendables 3）：型號為MP5A3、MP5K、MP5KA1和MP5KA5：
      - MP5A3被「陰陽」（[李連杰飾演](../Page/李連杰.md "wikilink")）、「死亡醫生」（[衛斯里·史奈普飾演](../Page/衛斯里·史奈普.md "wikilink")）和敵軍所使用。
      - MP5K在[布加勒斯特行動期間被敵方守衛所使用](../Page/布加勒斯特.md "wikilink")。
      - MP5KA1被「死亡醫生」和敵軍所使用。
      - MP5KA5裝上MP5K-PDW式槍托和低視度[照準器並被索恩](../Page/照準器.md "wikilink")（[格倫·鮑威爾飾演](../Page/格倫·鮑威爾.md "wikilink")）所使用。
  - 2014年—《[-{zh-cn:伸冤人; zh-hk:叛諜裁判;
    zh-tw:私刑教育;}-](../Page/私刑教育.md "wikilink")》：為MP5A3，配備[戰術導軌](../Page/皮卡汀尼導軌.md "wikilink")[護木](../Page/護木.md "wikilink")，裝上[抑制器](../Page/抑制器.md "wikilink")、EOTech
    XPS2[全息瞄準鏡和](../Page/全息瞄準鏡.md "wikilink")[前握把等配件](../Page/輔助握把.md "wikilink")，於故事後期被泰迪（[馬頓·索柯斯飾演](../Page/馬頓·索柯斯.md "wikilink")）所使用。
  - 2015年—《[黑帽駭客](../Page/黑帽駭客.md "wikilink")》：型號為MP5A3，被執行圍捕行動的[香港警務處](../Page/香港警務處.md "wikilink")[特別任務連隊員所使用](../Page/特別任務連.md "wikilink")。
  - 2015年—《[赤道](../Page/赤道_\(電影\).md "wikilink")》：型號為MP5A5，被[香港警務處](../Page/香港警務處.md "wikilink")[反恐特勤隊隊員所使用](../Page/反恐特勤隊.md "wikilink")。
  - 2015年—《[-{zh-hans:边境杀手; zh-hk:毒裁者;
    zh-tw:怒火邊界;}-](../Page/毒裁者.md "wikilink")》（Sicario）：型號為MP5A3和MP5SD3，兩者均被亞歷杭德羅·吉利克（[班尼西歐·狄奧·托羅飾演](../Page/班尼西歐·狄奧·托羅.md "wikilink")）所使用，並被裝上[紅點鏡](../Page/紅點鏡.md "wikilink")，前者更換了戰術導軌[護木](../Page/護木.md "wikilink")。一支裝上[電筒的MP](../Page/電筒.md "wikilink")5A3亦被一名毒犯所使用。
  - 2015年—《[-{zh-hk:猛火鎗;zh-tw:全面逃殺;zh-cn:使命召唤;}-](../Page/全面逃殺.md "wikilink")》（The
    Gunman）：型號為MP5A3，被故事尾段的西班牙特警隊所使用。
  - 2015年—《[-{zh-cn:王牌特工：特工学院; zh-hk:皇家特工：間諜密令;
    zh-tw:金牌特務;}-](../Page/皇家特工：間諜密令.md "wikilink")》（Kingsman:
    The Secret Service）：型號為MP5D3、MP5KA1和MP5K PDW。
      - MP5SD3於故事開頭被蘭斯洛特（[傑克·達文波特飾演](../Page/傑克·達文波特.md "wikilink")）所使用。
      - MP5KA1噴上了雪地塗裝並裝上ASG Super Xenon
        16085瞄準鏡，普遍地被里奇蒙·范倫坦（[山謬·傑克森飾演](../Page/山謬·傑克森.md "wikilink")）的手下所使用，其中一枝被主角蓋瑞·「伊格西」·安文（[泰隆·艾格頓飾演](../Page/泰隆·艾格頓.md "wikilink")）所繳獲。
      - MP5K PDW同樣噴上了雪地塗裝並被里奇蒙·范倫坦的手下所使用。
  - 2015年—《[魔鬼終結者：創世契機](../Page/魔鬼終結者：創世契機.md "wikilink")》（Terminator
    Genisys）型號為MP5K-PDW被莎拉康納所使用
  - 2016年—《[-{zh-cn:伦敦陷落; zh-hk:白宮淪陷2：倫敦淪陷;
    zh-tw:全面攻佔2：倫敦救援;}-](../Page/全面攻佔2：倫敦救援.md "wikilink")》（London
    Has Fallen）：型號為MP5A3及MP5K：
      - MP5A3被[倫敦警察和恐怖份子所使用](../Page/倫敦.md "wikilink")。
      - MP5K被恐怖份子所使用，也被[美國總統班傑明](../Page/美國總統.md "wikilink")·亞瑟（[艾倫·艾克哈特飾演](../Page/艾倫·艾克哈特.md "wikilink")）所繳獲。
  - 2016年—《[-{zh-hans:硬核大战; zh-hk:爆機特攻;
    zh-tw:超狂亨利;}-](../Page/硬核大戰.md "wikilink")》（Hardcore
    Henry）：型號為MP5K，被吉米（[沙托·科普利飾演](../Page/沙托·科普利.md "wikilink")）在[妓院裡所使用](../Page/妓院.md "wikilink")。
  - 2016年—《[樹大招風](../Page/樹大招風.md "wikilink")》：型號為MP5A3，被[特別任務連所使用](../Page/特別任務連.md "wikilink")。
  - 2016年—《[湄公河行動](../Page/湄公河行動.md "wikilink")》：型號為MP5K
    PDW，被郭冰（馮文娟飾演）所使用。
  - 2016年—《[寒戰II](../Page/寒戰II.md "wikilink")》（Cold War
    II）：型號為MP5A3和MP5A2，分別被[香港警察](../Page/香港警務處.md "wikilink")[特別任務連所使用](../Page/特別任務連.md "wikilink")。
  - 2017年—《[殺手保鏢](../Page/殺手保鏢.md "wikilink")》：型號為MP5A5，有裝[皮卡汀尼導軌與](../Page/皮卡汀尼導軌.md "wikilink")[全像武器照準器](../Page/全像武器照準器.md "wikilink")，被[法國國家警察干預隊與](../Page/法國國家警察干預隊.md "wikilink")[國際刑警組織探員使用](../Page/國際刑警組織.md "wikilink")，後來其中一支被達留士金凱德繳獲。
  - 2018年—《[红海行动](../Page/红海行动.md "wikilink")》（Operation Red
    Sea）：型号为MP5A3，分别被[中国人民解放軍海军陸戰隊](../Page/中国人民解放軍海军陸戰隊.md "wikilink")“蛟龙突击队”队长杨锐（[张译饰](../Page/张译.md "wikilink")）在追截核原料期间以及伊维亚共和国政府军所使用。

### [電視劇](../Page/電視劇.md "wikilink")

  - 《[極限權利系列](../Page/極限權利.md "wikilink")》（Ultimate Force）
      - 第一季：型號為MP5A3、MP5A2和MP5KA1。
          - MP5A3普遍地被[英國](../Page/英國.md "wikilink")[特種空勤團的隊員和武裝特警隊所使用](../Page/特種空勤團.md "wikilink")。
          - MP5A2於第5集由皇家北愛爾蘭警察所持有。
          - MP5KA1於第5集被卡羅琳所使用。
      - 第二季：型號為MP5A3、MP5SD3和MP5KA1。
          - MP5A3普遍地被[英國](../Page/英國.md "wikilink")[特種空勤團的隊員和武裝特警隊所使用](../Page/特種空勤團.md "wikilink")。
          - MP5SD3於第5集被英國特種空勤團的隊員所使用。
          - MP5KA1於第1集被[法國](../Page/法國.md "wikilink")[特種部隊的隊員所使用](../Page/特種部隊.md "wikilink")。
  - 《[-{zh-hans:反击; zh-hk:絕地反擊;
    zh-tw:勇者逆襲;}-系列](../Page/勇者逆襲.md "wikilink")》（Strike
    Back）
  - 《[-{zh-hans:行尸走肉; zh-hk:行屍;
    zh-tw:陰屍路;}-系列](../Page/陰屍路.md "wikilink")》（The
    Walking Dead）
  - 2016年—《[末日孤艦第三季](../Page/末日孤艦.md "wikilink")》（The Last Ship Season
    3）
      - MP5SD3於第四集被美國海軍内森.詹姆斯號驅逐艦滲透鵬主席官邸的艦長及陸戰隊員使用。

### 電子遊戲

  - 1998年—《[-{zh-hans:半条命;
    zh-hant:戰慄時空;}-](../Page/半条命_\(游戏\).md "wikilink")》（Half-Life）及其资料片《[-{zh-hans:针锋相对;
    zh-hant:正面交鋒;}-](../Page/半条命：针锋相对.md "wikilink")》与《[-{zh-hans:蓝色行动;
    zh-hant:關鍵時刻;}-](../Page/半条命：蓝色行动.md "wikilink")》：型号为MP5SD，命名為「9毫米衝鋒槍」，使用30发弹匣但奇怪的装填50发弹药，并与[9毫米手枪共用备弹](../Page/格洛克17.md "wikilink")，奇怪的换弹时不需要上膛。裝上了[M203榴彈發射器并最多可携带](../Page/M203榴彈發射器.md "wikilink")10发榴弹。造型上缺失了拉机柄、枪托以及榴弹发射器的扳机座与扳机。
  - 1998年—《[-{zh-cn:古墓丽影III：劳拉的冒险; zh-tw:古墓奇兵III：蘿拉的冒險;
    zh-hk:盜墓者羅拉III：羅拉的冒險;}-](../Page/古墓奇兵III：蘿拉的冒險.md "wikilink")》（Tomb
    Raider III: Adventures of Lara Croft）：在遊戲進行中可取得的武器以及所需彈藥。
  - 1999年—《[-{zh-hans:反恐精英;
    zh-hant:絕對武力;}-](../Page/反恐精英.md "wikilink")》（Counter-Strike）：
    型號為MP5N， 命名為「K\&M衝鋒槍」，武器模組採用鏡像佈局（與拋殼口位於同一邊）
  - 2000年—《[-{zh-hans:秘密潜入;
    zh-hant:核武浩劫;}-](../Page/秘密潜入.md "wikilink")》（Project
    IGI）：命名為「MP5 SD3」，是最初始的枪支。
  - 2002年—《》（Global Operations）：可選購額外彈匣或是瞄準鏡以及滅音器。
  - 2002年—《[Soldat](../Page/Soldat.md "wikilink")》：第2件主要武器。
  - 2002年—《[-{zh-hans:命运战士2：双重螺旋;
    zh-hant:傭兵戰場2：雙重螺旋;}-](../Page/傭兵戰場2：雙重螺旋.md "wikilink")》（Soldier
    of Fortune II: Double Helix）：為MP5SD，僅出現於連線版。
  - 2002年—《[-{zh-hans:秘密潜入2：隐秘行动;
    zh-hant:核武浩劫2;}-](../Page/秘密潜入2.md "wikilink")》（IGI2
    Covert Strike）：命名為「MP5 A3」，同样是最初始的枪支。
  - 2003年—《[-{zh-hans:反恐精英：零点行动;
    zh-hant:絕對武力：一觸即發;}-](../Page/絕對武力：一觸即發.md "wikilink")》（Counter-Strike:
    Condition Zero）： 型號為MP5N， 命名為「K\&M衝鋒槍」， 武器模組採用鏡像佈局（拉機柄與拋殼口位於同一邊）。
    刪除片段中具有可使用的[戰術燈](../Page/戰術燈.md "wikilink")。
  - 2004年—《[-{zh-hans:反恐精英：起源;
    zh-hant:絕對武力：次世代;}-](../Page/絕對武力：次世代.md "wikilink")》（Counter-Strike:
    Source）：
    型號為MP5N，命名為「K\&M衝鋒槍」，與前作最大區別為把抛殼口修正至在機匣右邊（左手持槍時為左邊），不再採用鏡像佈局。
  - 2004年—《[俠盜獵車手：聖安地列斯](../Page/俠盜獵車手：聖安地列斯.md "wikilink")》（Grand Theft
    Auto: San Andreas）：命名為「衝鋒槍（SMG）」。
  - 2004年—《[-{zh-hans:孤岛惊魂;
    zh-hant:極地戰嚎;}-](../Page/孤岛惊魂_\(游戏\).md "wikilink")》（Far
    Cry）
  - 2005年—《[真實計劃](../Page/真實計劃.md "wikilink")》（Project
    Reality）：型號為MP5A3。
  - 2007年—《[-{zh-hans:反恐精英Online;
    zh-hant:絕對武力Online;}-](../Page/絕對武力Online.md "wikilink")》（Counter-Strike
    Online）：型號為MP5N与MP5K-PDW：
      - MP5N为隨遊戲登場武器，命名為「K\&M衝鋒槍」，由於模型是繼承自CS1.6版本因此武器模組採用鏡像佈局（拉機柄與拋殼口位於同一邊）。另有為迎接虎年而設的改型、戰損版及C-Box限定的黃金槍身型。
      - MP5K-PDW以“Balrog-III”的虛構武器登场，枪身上拥有魔怪装饰。加装全息瞄准镜但奇怪的反向安装。武器模組不再採用鏡像佈局。使用30发弹匣供弹，持续射击超过15发后将不会再消耗弹匣内的弹药而是消耗备弹进行射击，伤害减半但是射速大幅提高，同时在弹着点会产生小型爆炸。停止射击时效果重置。
  - 2007年—《[-{zh:決勝時刻4：現代戰爭; zh-hans:使命召唤4：现代战争; zh-hant:決勝時刻4：現代戰爭;
    zh-cn:使命召唤4：现代战争; zh-tw:決勝時刻4：現代戰爭;
    zh-hk:使命召喚4：現代戰爭;}-](../Page/決勝時刻4：現代戰爭.md "wikilink")》（Call
    of Duty 4: Modern Warfare）： 型號為MP5N及MP5SD3（使用消音器後），
    命名為「MP5」，在單人戰役關卡“Crew
    Expendable”開頭拔槍時會使用“HK拍擊”上膛。故事模式時被[英國陸軍](../Page/英國陸軍.md "wikilink")[特種空勤團](../Page/特種空勤團.md "wikilink")、[美國空軍](../Page/美國空軍.md "wikilink")、[俄羅斯](../Page/俄羅斯.md "wikilink")[極端民族主義黨武裝勢力和中東某國反對勢力使用](../Page/極端民族主義.md "wikilink")。在聯機模式時於等級1解鎖，並能加裝[紅點鏡](../Page/紅點鏡.md "wikilink")、[ACOG瞄準鏡和](../Page/ACOG瞄準鏡.md "wikilink")[消音器](../Page/抑制器.md "wikilink")。
  - 2007年—《[-{zh-hans:潜行者：切尔诺贝利的阴影;
    zh-hant:浩劫殺陣：車諾比之影;}-](../Page/浩劫殺陣：車諾比之影.md "wikilink")》（S.T.A.L.K.E.R:
    Shadow of Chernobyl）：型號為MP5A3，命名為「Viper 5」。武器模組採用鏡像佈局（拋殼口及槍機拉柄都在左邊）。
  - 2008年—《[穿越火線](../Page/穿越火線.md "wikilink")》（Cross
    Fire）：型号为MP5A3与MP5KA4：
      - MP5A3命名为“MP5”，使用30发弹匣装填，无军衔限制，可使用9000GP购买并永久使用，并拥有多种改型。
      - MP5KA4命名为“MP5K
        A4”：使用30发弹匣装填，无军衔限制，可使用9000GP购买并永久使用，會使用“HK拍擊”上膛，奇怪的打空弹匣后或更换弹匣时拉机柄会自动锁定。
  - 2008年—《[-{zh-cn:孤岛惊魂2;
    zh-tw:極地戰嚎2;}-](../Page/孤島驚魂2.md "wikilink")》：型號為MP5SD6，命名為“MP5
    SD”。武器模組採用鏡像佈局（拋殼口跟拉機柄都在左邊）。
  - 2008年—《[-{zh-hans:镜之边缘;
    zh-hant:靚影特務;}-](../Page/镜之边缘.md "wikilink")》（Mirror's
    Edge）：型号为MP5K，被城市警察（CPF）所属的SWAT所使用，同时亦可以由女主角绯丝·康纳斯（配音：Jules de
    Jongh）于敌人手中所缴获。
  - 2008年—《[戰地之王](../Page/戰地之王.md "wikilink")》（Alliance of Valiant
    Arms）：型號為MP5A3，以基本武器供應，適當的穩定度，適中的準確度，扎實的傷害力，評價優良。另外还有euro购买的MP5K-PDW，可升级成可改造的MP5K
    Rail。随后推出升级版MP5K MOD 0。另外还有一个消音版MP5SD。
  - 2009年—《[-{zh:決勝時刻：現代戰爭2; zh-hans:使命召唤：现代战争2; zh-hant:決勝時刻：現代戰爭2;
    zh-cn:使命召唤：现代战争2; zh-tw:決勝時刻：現代戰爭2;
    zh-hk:使命召喚：現代戰爭2;}-](../Page/決勝時刻：現代戰爭2.md "wikilink")》（Call
    of Duty: Modern Warfare 2）：型號為MP5K，
    命名為「MP5K」，拔槍時會使用“HK拍擊”上膛，故事模式時被141特遣部隊、
    [俄羅斯](../Page/俄羅斯.md "wikilink")[極端民族主義黨武裝力量](../Page/極端民族主義.md "wikilink")、[聯邦安全局](../Page/聯邦安全局.md "wikilink")、[俄羅斯內衞部隊及](../Page/俄羅斯內衞部隊.md "wikilink")[巴西武裝團匪所使用](../Page/巴西.md "wikilink")。在聯機模式時於等級4解鎖，並能使用[紅點鏡](../Page/紅點鏡.md "wikilink")、[ACOG瞄準鏡](../Page/ACOG瞄準鏡.md "wikilink")、[消音器](../Page/抑制器.md "wikilink")、射速增加、[全息瞄準鏡](../Page/全息瞄準鏡.md "wikilink")、、延長彈匣（增至45發）、雙持和[熱能探測式瞄具](../Page/熱輻射.md "wikilink")。
  - 2009年—《[-{zh-cn:求生之路2;
    zh-tw:惡靈勢力2;}-](../Page/求生之路2.md "wikilink")》（Left
    4 Dead 2）：繼承自[-{zh-hans:反恐精英：起源;
    zh-hant:絕對武力：次世代;}-](../Page/絕對武力：次世代.md "wikilink")，型號為MP5N，命名為「H\&K
    MP5」，只在德國版出現，50發[彈匣](../Page/彈匣.md "wikilink")，附有[手電筒](../Page/手電筒.md "wikilink")。
  - 2009年—《[-{zh-hans:生化危机5; zh-hk:生化危機5;
    zh-hant:惡靈古堡5;}-](../Page/生化危機5.md "wikilink")》（Resident
    Evil 5）：型號為MP5A3。
  - 2010年—《[-{zh:決勝時刻：黑色行動; zh-hans:使命召唤：黑色行动; zh-hant:決勝時刻：黑色行動;
    zh-cn:使命召唤：黑色行动; zh-tw:決勝時刻：黑色行動;
    zh-hk:使命召喚：黑色行動;}-](../Page/決勝時刻：黑色行動.md "wikilink")》（Call
    of Duty: Black Ops）：型號為MP5K原型，
    命名為「MP5K」，會使用“HK拍擊”上膛，奇怪地會在[1960年代出現](../Page/1960年代.md "wikilink")。故事模式時被[美國中央情報局](../Page/中央情报局.md "wikilink")[特別活動分部和](../Page/中央情報局特別行動科.md "wikilink")所使用。在聯機模式時於等級1解鎖，並能使用[紅點鏡](../Page/紅點鏡.md "wikilink")、[ACOG瞄準鏡](../Page/ACOG瞄準鏡.md "wikilink")、[消音器](../Page/抑制器.md "wikilink")、射速增加、反射式瞄準鏡和延長彈匣（增至45發）。
  - 2011年—《[-{zh:決勝時刻：現代戰爭3; zh-hans:使命召唤：现代战争3; zh-hant:決勝時刻：現代戰爭3;
    zh-cn:使命召唤：现代战争3; zh-tw:決勝時刻：現代戰爭3;
    zh-hk:使命召喚：現代戰爭3;}-](../Page/決勝時刻：現代戰爭3.md "wikilink")》（Call
    of Duty: Modern Warfare 3）：型號為MP5A2，
    命名為「MP5」，拔槍時會使用“HK拍擊”上膛，在故事模式時被[英國陸軍](../Page/英國陸軍.md "wikilink")[特種空勤團](../Page/特種空勤團.md "wikilink")、[美國陸軍](../Page/美國陸軍.md "wikilink")[三角洲部隊合金分隊](../Page/三角洲部隊.md "wikilink")、[法國](../Page/法國.md "wikilink")[國家憲兵干預組和](../Page/國家憲兵干預組.md "wikilink")141特遣部隊所使用。在聯機模式時於等級1解鎖，並能使用[紅點鏡](../Page/紅點鏡.md "wikilink")、[ACOG瞄準鏡](../Page/ACOG瞄準鏡.md "wikilink")、[消音器](../Page/抑制器.md "wikilink")、射速增加、[全息瞄準鏡](../Page/全息瞄準鏡.md "wikilink")、混合式瞄準鏡、延長彈匣（增至45發）和[熱能探測式瞄具](../Page/熱輻射.md "wikilink")。此槍在生存模式中也是最早解鎖的武器之一，價格為$2,000。
  - 2011年— 《[-{zh-hans:战地3 ;
    zh-hant:戰地風雲3;}-](../Page/戰地風雲3.md "wikilink")》（Battlefield
    3）：型號為MP5KA4，命名為“M5K”，會使用“HK拍擊”上膛，為聯機模式中的工程兵可用武器。
  - 2012年—《[-{zh-hans:反恐精英：全球攻势;
    zh-hant:絕對武力：全球攻勢;}-](../Page/反恐精英：全球攻势.md "wikilink")》（Counter-Strike:
    Global Offensive）：与2018年8月16日加入游戏，型号为MP5SD3，命名为“MP5
    SD”，30发弹匣，换弹时会手动锁定枪机，更换弹匣后会使用“HK拍击”上膛。需要在仓库内替换[MP7方可使用](../Page/HK_MP7衝鋒槍.md "wikilink")。
  - 2012年—《[-{zh:決勝時刻：黑色行動II; zh-hans:使命召唤：黑色行动II; zh-hant:決勝時刻：黑色行動II;
    zh-cn:使命召唤：黑色行动II; zh-tw:決勝時刻：黑色行動II;
    zh-hk:使命召喚：黑色行動II;}-](../Page/決勝時刻：黑色行動II.md "wikilink")》（Call
    of Duty: Black Ops
    II）：型號為MP5A3和MP5SD3（裝備[抑制器後](../Page/抑制器.md "wikilink")），
    命名為「MP5A3」，拔槍時會使用“HK拍擊”上膛，只在故事模式和殭屍模式中登場，故事模式中被[美國中央情報局](../Page/美國中央情報局.md "wikilink")[特別活動分部和](../Page/特別活動分部.md "wikilink")[巴拿馬國防軍所使用](../Page/巴拿馬國防軍.md "wikilink")，並是玩家一開始就能使用的武器之一。能夠使用：[反射式瞄準鏡](../Page/紅點鏡.md "wikilink")、[抑制器](../Page/抑制器.md "wikilink")、長槍管、延長彈匣（增至45發）、[快速重裝彈匣](../Page/彈匣並聯.md "wikilink")、[槍托](../Page/槍托.md "wikilink")、射速增加。
  - 2012年—《[战争前线](../Page/战争前线.md "wikilink")》（Warface）：型號為MP5A4与MP5A5，均为工程兵专用武器：
      - MP5A4命名为“H\&K MP5”，采用MP5K
        PDW的折叠式枪托并折叠，同时去除前护木改为战术导轨，枪械顶部加装导轨以安装瞄准镜。使用30发弹匣，作为工程兵初始武器，可以改装枪口配件（[通用消音器](../Page/抑制器.md "wikilink")、冲锋枪消音器）、战术导轨配件（[通用握把](../Page/辅助握把.md "wikilink")）以及瞄准镜（[EoTech
        553全息瞄准镜](../Page/全息瞄准镜.md "wikilink")、绿点全息瞄准镜、冲锋枪普通瞄准镜）。拥有雪地迷彩涂装版本，强化威力与射程，可以改装枪口配件（[通用消音器](../Page/抑制器.md "wikilink")、冲锋枪消音器、[冲锋枪制退器](../Page/炮口制动器.md "wikilink")、[冲锋枪刺刀](../Page/刺刀.md "wikilink")）、战术导轨配件（[通用握把](../Page/辅助握把.md "wikilink")、[冲锋枪握把](../Page/宽型前握把.md "wikilink")、冲锋枪握把架）以及瞄准镜（[EoTech
        553全息瞄准镜](../Page/全息瞄准镜.md "wikilink")、绿点全息瞄准镜、[红点瞄准镜](../Page/红点瞄准镜.md "wikilink")、冲锋枪普通瞄准镜、冲锋枪高级瞄准镜、[冲锋枪专家瞄准镜](../Page/Leupold_Mark_4_CQ/T光学瞄准镜.md "wikilink")），配件自带迷彩涂装。
          - 雪地迷彩版本可以通过抽奖、生存模式系列地图“冷锋行动”通关奖励箱以及活动获得
      - MP5A5命名为“H\&K MP5A5
        Custom”，沙色涂装，使用战术导轨护木，并将枪托更换为[Magpul生产的CTR枪托](../Page/馬格普工業公司.md "wikilink")。使用30发弹匣，大师级解锁，可以改装枪口配件（[通用消音器](../Page/抑制器.md "wikilink")、冲锋枪消音器、[冲锋枪制退器](../Page/炮口制动器.md "wikilink")、[冲锋枪刺刀](../Page/刺刀.md "wikilink")）、战术导轨配件（[通用握把](../Page/辅助握把.md "wikilink")、[冲锋枪握把](../Page/宽型前握把.md "wikilink")、冲锋枪握把架）以及瞄准镜（[EoTech
        553全息瞄准镜](../Page/全息瞄准镜.md "wikilink")、绿点全息瞄准镜、[红点瞄准镜](../Page/红点瞄准镜.md "wikilink")、冲锋枪普通瞄准镜、冲锋枪高级瞄准镜、[冲锋枪专家瞄准镜](../Page/Leupold_Mark_4_CQ/T光学瞄准镜.md "wikilink")、[RAMBO
        SP-6反射式瞄准镜](../Page/RAMBO_SP-6反射式瞄准镜.md "wikilink")）。
  - 2012年—《[-{zh-hans:孤岛惊魂;
    zh-hant:極地戰嚎;}-3](../Page/極地戰嚎3.md "wikilink")》（Far
    Cry 3）：型號為MP5A3，會使用“HK拍擊”上膛，奇怪的是射擊時拉機柄會隨槍機一同復進，並在打空彈匣後會自動鎖定。
  - 2013年—《[-{zh-hans:收获日;
    zh-hant:劫薪日;}-2](../Page/劫薪日2.md "wikilink")》（Payday
    2）：型號為MP5A4（預設），命名為「Compact-5」，可在黑市購買後並另外花費自行組裝，會使用“HK拍擊”上膛。
  - 2013年—《[-{zh-hans:武装突袭;
    zh-hant:武裝行動;}-3](../Page/武装突袭3.md "wikilink")》（ArmA
    3）：型號為MP5K，命名为“Protecter 9mm"，會使用“HK拍擊”上膛。
  - 2014年—《[-{zh-hans:孤岛惊魂;
    zh-hant:極地戰嚎;}-4](../Page/極地戰嚎4.md "wikilink")》（Far
    Cry
    4）：型號為MP5N，命名為「MP5」，會使用“HK拍擊”上膛，奇怪的是在射擊時拉機柄會隨槍機一同復進，並在打空彈匣後會自動鎖定。
  - 2014年—《》（Insurgency）：型號為MP5KA4，命名為“MP5K”，安全部隊專用武器，會使用“HK拍擊”上膛。
  - 2015年—《[-{zh-hans:战地：硬仗 ;
    zh-hant:戰地風雲：強硬派系;}-](../Page/戰地風雲：強硬派系.md "wikilink")》（Battlefield
    Hardline）：型號為MP5KA4、MP5A4和MP5SD5：
      - MP5KA4（裝上[槍托後會變成MP](../Page/槍托.md "wikilink")5K
        PDW）命名為「MP5K」，歸類為衝鋒槍，20+1發彈匣，預設配備Trijicon SRS02綠點鏡、延長彈匣（25發）及[抑制器](../Page/抑制器.md "wikilink")，設定為警匪雙方機械師（Mechanic）起始武器。可加裝各種瞄準鏡（[反射](../Page/紅點鏡.md "wikilink")、眼鏡蛇、[全息瞄準鏡](../Page/全息瞄準鏡.md "wikilink")（放大1倍）、PKA-S（放大1倍）、Micro
        T1、SRS 02、[Comp
        M4S](../Page/Aimpoint_Comp_M4紅點鏡.md "wikilink")、[M145](../Page/C79光學瞄準鏡.md "wikilink")（放大3.4倍）、PO（放大3.5倍）
        、[ACOG](../Page/先進戰鬥光學瞄準鏡.md "wikilink")（放大4倍）、[PSO-1](../Page/PSO-1光學瞄準鏡.md "wikilink")（放大4倍）、[IRNV](../Page/夜視儀.md "wikilink")（放大1倍）、[FLIR](../Page/熱成像儀.md "wikilink")（放大2倍））、附加配件（傾斜式[機械瞄具](../Page/機械瞄具.md "wikilink")、傾斜式紅點鏡、延長彈匣（增至25+1發）、[電筒](../Page/電筒.md "wikilink")、戰術燈、[槍托](../Page/槍托.md "wikilink")、[激光瞄準器](../Page/激光指示器.md "wikilink")）及槍口零件（[槍口制退器](../Page/砲口制動器.md "wikilink")、補償器、重[槍管](../Page/槍管.md "wikilink")、[抑制器](../Page/抑制器.md "wikilink")、消焰器）。劇情模式當中則能夠被主角尼古拉斯·門多薩所使用。
      - MP5A4命名為「M5
        Navy」，會使用“HK拍擊”上膛，為資料片「大逃亡」新增的資料片獨有武器，歸類為衝鋒槍，30+1發彈匣（造型上為15發彈匣，使用“延長彈匣”改裝後更換為30發彈匣的造型），被警方機械師（Mechanic）所使用，價格為$60,000。可加裝各種瞄準鏡（[反射](../Page/紅點鏡.md "wikilink")、眼鏡蛇、[全息瞄準鏡](../Page/全息瞄準鏡.md "wikilink")（放大1倍）、PKA-S（放大1倍）、Micro
        T1、SRS 02、[Comp
        M4S](../Page/Aimpoint_Comp_M4紅點鏡.md "wikilink")、[M145](../Page/C79光學瞄準鏡.md "wikilink")（放大3.4倍）、PO（放大3.5倍）
        、[ACOG](../Page/先進戰鬥光學瞄準鏡.md "wikilink")（放大4倍）、[PSO-1](../Page/PSO-1光學瞄準鏡.md "wikilink")（放大4倍）、[IRNV](../Page/夜視儀.md "wikilink")（放大1倍）、[FLIR](../Page/熱成像儀.md "wikilink")（放大2倍））、附加配件（傾斜式[機械瞄具](../Page/機械瞄具.md "wikilink")、傾斜式紅點鏡、延長彈匣、[電筒](../Page/電筒.md "wikilink")、戰術燈、[激光瞄準器](../Page/激光指示器.md "wikilink")）、槍口零件（[槍口制退器](../Page/砲口制動器.md "wikilink")、補償器、重[槍管](../Page/槍管.md "wikilink")、[抑制器](../Page/抑制器.md "wikilink")、消焰器）及[握把](../Page/輔助握把.md "wikilink")（垂直握把、粗短握把、[直角握把](../Page/直角前握把.md "wikilink")）。
      - MP5SD5命名為「M5SD」，會使用“HK拍擊”上膛，為資料片「大逃亡」新增武器，歸類為衝鋒槍，30+1發彈匣（造型上為15發彈匣，使用“延長彈匣”改裝後更換為30發彈匣的造型），被雙方所有職階所使用，價格為$60,000。可加裝各種瞄準鏡（[反射](../Page/紅點鏡.md "wikilink")、眼鏡蛇、[全息瞄準鏡](../Page/全息瞄準鏡.md "wikilink")（放大1倍）、PKA-S（放大1倍）、Micro
        T1、SRS 02、[Comp
        M4S](../Page/Aimpoint_Comp_M4紅點鏡.md "wikilink")、[M145](../Page/C79光學瞄準鏡.md "wikilink")（放大3.4倍）、PO（放大3.5倍）
        、[ACOG](../Page/先進戰鬥光學瞄準鏡.md "wikilink")（放大4倍）、[PSO-1](../Page/PSO-1光學瞄準鏡.md "wikilink")（放大4倍）、[IRNV](../Page/夜視儀.md "wikilink")（放大1倍）、[FLIR](../Page/熱成像儀.md "wikilink")（放大2倍））、附加配件（傾斜式[機械瞄具](../Page/機械瞄具.md "wikilink")、傾斜式紅點鏡、延長彈匣、[電筒](../Page/電筒.md "wikilink")、戰術燈、[激光瞄準器](../Page/激光指示器.md "wikilink")）及[握把](../Page/輔助握把.md "wikilink")（垂直握把、粗短握把、[直角握把](../Page/直角前握把.md "wikilink")）。
  - 2015年—《[未轉變者](../Page/未轉變者.md "wikilink")》:命名為「Viper」，，分類為卡賓槍，等級為稀有等級槍枝，使用25發專用彈匣，可以改瞄準鏡、戰術導軌配件以及槍管。
  - 2015年—《[-{zh-hans:彩虹六号：围攻 ;
    zh-hant:虹彩六號：圍攻行動;}-](../Page/虹彩六號：圍攻行動.md "wikilink")》（Rainbow
    Six: Siege）：登場型號為MP5 MLI，MP5K PDW和MP5SD2，會使用“HK拍擊”上膛：
      - MP5 MLI命名為「MP5」，被[國家憲兵干預組所使用](../Page/國家憲兵干預組.md "wikilink")。
      - MP5K PDW命名為「MP5K」，被[特種空勤團所使用](../Page/特種空勤團.md "wikilink")
      - MP5SD2命名為「MP5SD」，被[特殊急襲部隊所使用](../Page/特殊急襲部隊.md "wikilink")。
  - 2015年—《[-{zh-hans:黑山：起源;zh-hant:黑色高地：起源;}-](../Page/黑色高地.md "wikilink")》（Black
    Mesa）：型号为MP5A3，但奇怪的使用MP5SD的枪管，命名为「9毫米冲锋枪」，使用30发弹匣装填并与[格洛克手枪共用备弹](../Page/格洛克17.md "wikilink")，换弹时会手动锁定枪机，更换弹匣后会使用“HK拍击”上膛。裝上了[M203榴彈發射器并最多可携带](../Page/M203榴彈發射器.md "wikilink")3发榴弹，但奇怪的不需要扣动扳机即可击发。MOD版本加装[全息瞄准镜](../Page/全息瞄准镜.md "wikilink")，独立版本则不安装瞄准镜。
  - 2016年—《[少女前線](../Page/少女前線.md "wikilink")》(Girls'
    Frontline):在少女前線中，MP5被設為四星衝鋒戰術人形，可透過工廠製造獲得，外觀為手持MP5的女孩。
  - 2016年—《[殺戮空間2](../Page/殺戮空間2.md "wikilink")》（Killing Floor
    2）:型號為MP5A2，命名為「MP5RAS SMG」，配備戰術導軌護木並裝上前握把，以30發彈匣供彈卻有40發。
  - 2016年—《[-{zh:決勝時刻：現代戰爭; zh-hans:使命召唤：现代战争; zh-hant:決勝時刻：現代戰爭;
    zh-cn:使命召唤：现代战争; zh-tw:決勝時刻：現代戰爭;
    zh-hk:使命召喚：現代戰爭;}-重製版](../Page/決勝時刻4：現代戰爭.md "wikilink")》（Call
    of Duty: Modern Warfare Remastered）：型號為MP5N及MP5SD3，外觀比前作更為精細。
  - 2018年—《[-{zh-hans:孤岛惊魂;
    zh-hant:極地戰嚎;}-5](../Page/極地戰嚎5.md "wikilink")》（Far
    Cry 5）：型號為MP5A3、MP5SD3和MP5K，會使用「HK拍擊」上膛。
  - 2019年—《[-{zh-hans:孤岛惊魂：新曙光;
    zh-hant:極地戰嚎：破曉;}-](../Page/孤岛惊魂：新曙光.md "wikilink")》（Far
    Cry: New Dawn）

### [動畫](../Page/動畫.md "wikilink")

  - 2002年—《[神槍少女](../Page/神槍少女.md "wikilink")》
  - 2006年—《[-{zh-hans:黑礁;
    zh-hant:企業傭兵;}-](../Page/企業傭兵.md "wikilink")》：型號為MP5A3，被國家社會主義白人團結黨成員所使用。
  - 2007年—《[DARKER THAN BLACK](../Page/DARKER_THAN_BLACK.md "wikilink")》
  - 2008年—《[圖書館戰爭](../Page/圖書館戰爭.md "wikilink")》
  - 2009年—《[幻靈鎮魂曲](../Page/幻靈鎮魂曲.md "wikilink")》
  - 2011年—《[未來日記](../Page/未來日記.md "wikilink")》
  - 2012年—《[軍火女王](../Page/軍火女王.md "wikilink")》
  - 2014年—《[WIZARD
    BARRISTERS～弁魔士賽希爾](../Page/WIZARD_BARRISTERS～弁魔士賽希爾.md "wikilink")》：型號為MP5A2，被[特殊急襲部隊所使用](../Page/特殊急襲部隊.md "wikilink")。

### [小说](../Page/小说.md "wikilink")

  - 2007年—《[SCP基金会](../Page/SCP基金会.md "wikilink")》：型号为MP5K-PDW，以编号“SCP-127”的名义出现，为一只由活体组织构成能够自动生成牙齿状弹药的冲锋枪，编级为SAFE级。
  - 2014年—《[刀劍神域外傳Gun Gale
    Online](../Page/刀劍神域外傳Gun_Gale_Online.md "wikilink")》：登場型號為MP5A3及MP5K，被個别GGO玩家所使用。

## 参见

  - [HK UMP](../Page/HK_UMP衝鋒槍.md "wikilink")
  - [HK MP7](../Page/HK_MP7衝鋒槍.md "wikilink")
  - [HK G3](../Page/HK_G3自動步槍.md "wikilink")
  - [MC-51](../Page/MC-51卡賓槍.md "wikilink")
  - [幽靈M4型](../Page/幽靈M4型衝鋒槍.md "wikilink")
  - [GSG-5](../Page/GSG-5卡賓槍.md "wikilink")

## 註釋

## 參考文獻

  - [heckler-koch.de-HK衝鋒槍](http://www.heckler-koch.de/HKWeb/show/frameContent/20/4/20)
  - [heckler-koch.de-MP5A](http://www.heckler-koch.de/HKWebText/detailProd/1926/83/4/20)
  - [heckler-koch.de-MP5SD](http://www.heckler-koch.de/HKWebText/detailProd/1926/129/4/20)
  - [heckler-koch.de-MP5K](http://www.heckler-koch.de/HKWebText/detailProd/1926/128/4/20)
  - [heckler-koch.de-MP5SFA](http://www.heckler-koch.de/HKWebText/detailProd/1926/126/4/20)
  - [HKPRO-MP5](https://web.archive.org/web/20071011221834/http://hkpro.com/mp5.htm)
  - [remtek.com-MP5](https://web.archive.org/web/20070929071426/http://remtek.com/arms/hk/mil/mp5/choice/coang.htm)

## 外部連結

  - —[黑克勒-科赫官方網頁](http://www.heckler-koch.de/)

  - —[Modern Firearms—Heckler und Koch
    MP-5](http://world.guns.ru/smg/de/hk-mp5-e.html)

      - [Heckler und Koch
        MP-5k](http://world.guns.ru/smg/de/hk-mp5k-e.html)

  - —[槍炮世界—MP5系列冲锋枪](https://web.archive.org/web/20120424050323/http://www.gunsworld.net/german/hk/mp5/mp5.htm)

[Category:衝鋒槍](../Category/衝鋒槍.md "wikilink")
[Category:黑克勒-科赫](../Category/黑克勒-科赫.md "wikilink")
[Category:9毫米魯格彈槍械](../Category/9毫米魯格彈槍械.md "wikilink")
[MP5/10](../Category/10毫米Auto槍械.md "wikilink")
[MP5/40](../Category/.40_S&W口徑槍械.md "wikilink")
[Category:德國槍械](../Category/德國槍械.md "wikilink")
[Category:美國海軍陸戰隊裝備](../Category/美國海軍陸戰隊裝備.md "wikilink")
[Category:HK G3衍生槍](../Category/HK_G3衍生槍.md "wikilink")

1.  [tactical-weapons.com-Comparison between SWA5 and Third world
    country MP5 weapons](http://www.tactical-weapons.com/SWA5comp.htm)
2.  [tactical-weapons.com-SWA5](http://www.tactical-weapons.com/swa5.htm)
3.  [diomil.ir](http://www.diomil.ir/en/aig.aspx)
4.  <http://vitaly.livejournal.com/165234.html>
5.  <http://www.youtube.com/watch?v=_ihVozn0BeE>
6.  <http://www.youtube.com/watch?v=ltn_pTE7rrA>
7.  [Cuban Special Forces - MININT and Black
    Wasps](https://m.youtube.com/watch?v=h0RGvgrz4CU)
8.  <https://www.youtube.com/watch?t=610&v=RwK1wa6la5Y>
9.
10.
11.
12.
13. <http://www.youtube.com/watch?v=mz-_AVC-tV4>
14. [*En liten røver med trøkk i (Norwegian article on the Heckler &
    Koch
    deal)*](http://www.mil.no/haren/start/article.jhtml?articleID=141475)

15. <http://scienceport.ru/galleries/Kalendar-Alfy-na-2014-god-9046.html>
16. <http://lenta.ru/news/2009/12/11/glock/>
17. <http://i-korotchenko.livejournal.com/662520.html>
18. [priu.gov.lk-Naval surveillance is the millstone around LTTE's
    neck](http://www.priu.gov.lk/news_update/features/20031017naval_surveillance_millstone_LTTE.htm)

19. <https://sites.google.com/site/worldinventory/wiw_eu_ukraine>