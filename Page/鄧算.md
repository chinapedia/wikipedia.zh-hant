**鄧算**（，），原名**鄧金算**（），字**節甫**（），[越南](../Page/越南.md "wikilink")[阮朝官員](../Page/阮朝.md "wikilink")。

## 生平

鄧算是[南定省](../Page/南定省.md "wikilink")[春長府](../Page/春長府.md "wikilink")[膠水縣行善總行善社](../Page/膠水縣_\(越南\).md "wikilink")（今屬南定省[春長縣春鴻社行善村](../Page/春長縣.md "wikilink")）人，[嘉隆六年](../Page/嘉隆.md "wikilink")（1807年）出生。[紹治七年](../Page/紹治.md "wikilink")（1847年）考中[舉人](../Page/舉人.md "wikilink")\[1\]。[嗣德元年](../Page/嗣德.md "wikilink")（1848年）會試，考中副榜\[2\]。授[翰林院檢討](../Page/翰林院.md "wikilink")，補[南策府同知府](../Page/南策府.md "wikilink")，後改任[靜嘉府知府](../Page/靜嘉府.md "wikilink")。當時[珥河決堤](../Page/珥河.md "wikilink")，鄧算奉命前往勘探水道，擔任堤政員外郎一職。他借著南定瀕臨大海，有沙地的條件，招集流民開墾荒地，立為樂善總，歸膠水縣管轄。後來升任侍講學士，領[北寧省按察使一職](../Page/北寧省.md "wikilink")，後又改任[寧平省按察使](../Page/寧平省.md "wikilink")。嗣德二十年（1867年），代理諒平巡撫。當時清朝廣西省有土匪滋擾邊境，鄧算籌畫處置辦法。嗣德二十三年（1870年），賊寇攻陷諒山省城，鄧算因失職降為翰林院侍講，擔任商辦諒山省軍次事務。不久憑藉雲池之戰獲勝，開復為鴻臚寺卿，擔任[高平省布政使一職](../Page/高平省.md "wikilink")。嗣德二十七年（1874年），擔任寧平巡撫，不久又陞為安靜總督，但未及赴任，就因病去世，壽六十八歲。

鄧算有8子。[鄧箲蔭授翰林院檢討](../Page/鄧箲.md "wikilink")。

## 注釋

## 參考資料

  - 《[大南實錄](../Page/大南實錄.md "wikilink")》正編列傳二集 卷三十七 鄧算傳

[Category:南定省人](../Category/南定省人.md "wikilink")
[Category:阮朝巡撫](../Category/阮朝巡撫.md "wikilink")
[Category:嗣德元年戊申恩科副榜](../Category/嗣德元年戊申恩科副榜.md "wikilink")
[Category:阮朝布政使](../Category/阮朝布政使.md "wikilink")
[Category:阮朝總督](../Category/阮朝總督.md "wikilink")
[Category:阮朝知府](../Category/阮朝知府.md "wikilink")
[Category:阮朝按察使](../Category/阮朝按察使.md "wikilink")
[Category:紹治七年丁未恩科南定場舉人](../Category/紹治七年丁未恩科南定場舉人.md "wikilink")
[Category:阮朝鴻臚寺卿](../Category/阮朝鴻臚寺卿.md "wikilink")

1.  [《國朝鄉科錄》](http://lib.nomfoundation.org/collection/1/volume/340/page/81)
2.  [《國朝科榜錄》](http://hannom.nlv.gov.vn/hannom/cgi-bin/hannom?a=d&d=BNTwEHieahcNp1894.1.41)