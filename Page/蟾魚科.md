**蟾魚科**（[学名](../Page/学名.md "wikilink")：），為**蟾魚目**（）的唯一一科，属[脊索动物门](../Page/脊索动物门.md "wikilink")[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")。

## 分類

本目属于[正真骨鱼群](../Page/正真骨鱼群.md "wikilink")、[栉鳞派](../Page/栉鳞派.md "wikilink")、[棘鳍类](../Page/棘鳍类.md "wikilink")、[鲈形亚类](../Page/鲈形亚类.md "wikilink")，独自组成**蟾鱼系**，与其它系的演化关系如下：

### 内部分类

蟾魚科下分4个亚科23個屬，如下：

  - [蟾鱼亚科](../Page/蟾鱼亚科.md "wikilink") Batrachoidinae

<!-- end list -->

  - [雙蟾魚屬](../Page/雙蟾魚屬.md "wikilink") *Amphichthys*
      - [吻雙蟾魚](../Page/吻雙蟾魚.md "wikilink") *Amphichthys cryptocentrus*
      - [雙蟾魚](../Page/雙蟾魚.md "wikilink") *Amphichthys rubigenes*

<!-- end list -->

  - [蟾魚屬](../Page/蟾魚屬.md "wikilink") *Batrachoides*
      - [布氏蟾魚](../Page/布氏蟾魚.md "wikilink") *Batrachoides boulengeri*
      - [吉氏蟾魚](../Page/吉氏蟾魚.md "wikilink") *Batrachoides gilberti*
      - [戈氏蟾魚](../Page/戈氏蟾魚.md "wikilink") *Batrachoides goldmani*
      - [絨頭蟾魚](../Page/絨頭蟾魚.md "wikilink") *Batrachoides liberiensis*
      - [芒蟾魚](../Page/芒蟾魚.md "wikilink") *Batrachoides manglae*
      - [太平洋蟾魚](../Page/太平洋蟾魚.md "wikilink") *Batrachoides pacifici*
      - [蘇里南蟾魚](../Page/蘇里南蟾魚.md "wikilink") *Batrachoides surinamensis*
      - [沃克氏蟾魚](../Page/沃克氏蟾魚.md "wikilink") *Batrachoides walkeri*
      - [沃爾特氏蟾魚](../Page/沃爾特氏蟾魚.md "wikilink") *Batrachoides waltersi*

<!-- end list -->

  - [豹蟾魚屬](../Page/豹蟾魚屬.md "wikilink") *Opsanus*
      - [海灣豹蟾魚](../Page/海灣豹蟾魚.md "wikilink") *Opsanus beta*
      - [巴西豹蟾魚](../Page/巴西豹蟾魚.md "wikilink") *Opsanus brasiliensis*
      - [雙色豹蟾魚](../Page/雙色豹蟾魚.md "wikilink") *Opsanus dichrostomus*
      - [豹蟾魚](../Page/豹蟾魚.md "wikilink") *Opsanus pardus*
      - [瘦體豹蟾魚](../Page/瘦體豹蟾魚.md "wikilink") *Opsanus phobetron*
      - [毒棘豹蟾魚](../Page/毒棘豹蟾魚.md "wikilink") *Opsanus tau*

<!-- end list -->

  - [河蟾魚屬](../Page/河蟾魚屬.md "wikilink") *Potamobatrachus*
      - [三棘河蟾魚](../Page/三棘河蟾魚.md "wikilink") *Potamobatrachus
        trispinosus*

<!-- end list -->

  - [礁蟾魚屬](../Page/礁蟾魚屬.md "wikilink") *Sanopus*
      - [珊穴礁蟾魚](../Page/珊穴礁蟾魚.md "wikilink") *Sanopus astrifer*
      - [鬚礁蟾魚](../Page/鬚礁蟾魚.md "wikilink") *Sanopus barbatus*
      - [格林菲爾德礁蟾魚](../Page/格林菲爾德礁蟾魚.md "wikilink") *Sanopus
        greenfieldorum*
      - [科蘇梅爾礁蟾魚](../Page/科蘇梅爾礁蟾魚.md "wikilink") *Sanopus johnsoni*
      - [網紋礁蟾魚](../Page/網紋礁蟾魚.md "wikilink") *Sanopus reticulatus*
      - [礁蟾魚](../Page/礁蟾魚.md "wikilink") *Sanopus splendidus*

<!-- end list -->

  - [弗拉特蟾魚屬](../Page/弗拉特蟾魚屬.md "wikilink") *Vladichthys*
      - [弗拉特蟾魚](../Page/弗拉特蟾魚.md "wikilink") *Vladichthys gloverensis*

<!-- end list -->

  - [小孔蟾鱼亚科](../Page/小孔蟾鱼亚科.md "wikilink") Halophryninae

<!-- end list -->

  - [奇蟾魚屬](../Page/奇蟾魚屬.md "wikilink") *Allenbatrachus*
      - [印尼奇蟾魚](../Page/印尼奇蟾魚.md "wikilink") *Allenbatrachus
        grunniens*：又稱真蟾魚、裸蟾魚。
      - [南方奇蟾魚](../Page/南方奇蟾魚.md "wikilink") *Allenbatrachus
        meridionalis*
      - [網紋奇蟾魚](../Page/網紋奇蟾魚.md "wikilink") *Allenbatrachus
        reticulatus*

<!-- end list -->

  - [澳洲蟾魚屬](../Page/澳洲蟾魚屬.md "wikilink") *Austrobatrachus*
      - [南方澳洲蟾魚](../Page/南方澳洲蟾魚.md "wikilink") *Austrobatrachus foedus*

<!-- end list -->

  - [鬚蟾魚屬](../Page/鬚蟾魚屬.md "wikilink") *Barchatus*
      - [紅海鬚蟾魚](../Page/紅海鬚蟾魚.md "wikilink") *Barchatus
        cirrhosus*：又稱紅海似海蟾魚。

<!-- end list -->

  - [擬蟾魚屬](../Page/擬蟾魚屬.md "wikilink") *Batrachomoeus*
      - [達氏擬蟾魚](../Page/達氏擬蟾魚.md "wikilink") *Batrachomoeus dahli*
      - [擬蟾魚](../Page/擬蟾魚.md "wikilink") *Batrachomoeus dubius*
      - [西澳擬蟾魚](../Page/西澳擬蟾魚.md "wikilink") *Batrachomoeus
        occidentalis*
      - [紅頭擬蟾魚](../Page/紅頭擬蟾魚.md "wikilink") *Batrachomoeus
        rubricephalus*
      - [三刺擬蟾魚](../Page/三刺擬蟾魚.md "wikilink") *Batrachomoeus trispinosus*

<!-- end list -->

  - [裸蟾魚屬](../Page/裸蟾魚屬.md "wikilink") *Batrichthys*
      - [白紋裸蟾魚](../Page/白紋裸蟾魚.md "wikilink") *Batrichthys albofasciatus*
      - [蛇首裸蟾魚](../Page/蛇首裸蟾魚.md "wikilink") *Batrichthys apiatus*

<!-- end list -->

  - [蛙蟾魚屬](../Page/蛙蟾魚屬.md "wikilink") *Bifax*
      - [阿曼蛙蟾魚](../Page/阿曼蛙蟾魚.md "wikilink") *Bifax lacinia*
  - [叉鼻蟾魚屬](../Page/叉鼻蟾魚屬.md "wikilink") *Chatrabus*
      - [南非叉鼻蟾魚](../Page/南非叉鼻蟾魚.md "wikilink") *Chatrabus
        felinus*：又稱南非裸蟾魚
      - [享氏叉鼻蟾魚](../Page/享氏叉鼻蟾魚.md "wikilink") *Chatrabus hendersoni*
      - [四帶叉鼻蟾魚](../Page/四帶叉鼻蟾魚.md "wikilink") *Chatrabus
        melanurus*：又稱叉鼻蟾魚。

<!-- end list -->

  - [科利特蟾魚屬](../Page/科利特蟾魚屬.md "wikilink") *Colletteichthys*
      - [杜氏科利特蟾魚](../Page/杜氏科利特蟾魚.md "wikilink") *Colletteichthys
        dussumieri*：又名杜氏澳洲蟾魚
      - [黃鰭科利特蟾魚](../Page/黃鰭科利特蟾魚.md "wikilink") *Colletteichthys
        flavipinnis*
      - [阿拉伯科利特蟾魚](../Page/阿拉伯科利特蟾魚.md "wikilink") *Colletteichthys
        occidentalis*

<!-- end list -->

  - [孔蟾魚屬](../Page/孔蟾魚屬.md "wikilink") *Halobatrachus*
      - [腋孔蟾魚](../Page/腋孔蟾魚.md "wikilink") *Halobatrachus
        didactylus*：又稱斑紋真蟾魚。

<!-- end list -->

  - [小孔蟾魚屬](../Page/小孔蟾魚屬.md "wikilink") *Halophryne*
      - [橫帶小孔蟾魚](../Page/橫帶小孔蟾魚.md "wikilink") *Halophryne diemensis*
      - [哈氏小孔蟾魚](../Page/哈氏小孔蟾魚.md "wikilink") *Halophryne hutchinsi*
      - [白斑小孔蟾魚](../Page/白斑小孔蟾魚.md "wikilink") *Halophryne ocellatus*
      - [昆士蘭小孔蟾魚](../Page/昆士蘭小孔蟾魚.md "wikilink") *Halophryne
        queenslandiae*

<!-- end list -->

  - [袋蟾魚屬](../Page/袋蟾魚屬.md "wikilink") *Perulibatrachus*
      - [鋒牙袋蟾魚](../Page/鋒牙袋蟾魚.md "wikilink") *Perulibatrachus
        aquilonarius*
      - [幾內亞袋蟾魚](../Page/幾內亞袋蟾魚.md "wikilink") *Perulibatrachus
        elminensis*：又稱副蟾魚。
      - [東非袋蟾魚](../Page/東非袋蟾魚.md "wikilink") *Perulibatrachus kilburni*
      - [單鼻袋蟾魚](../Page/單鼻袋蟾魚.md "wikilink") *Perulibatrachus
        rossignoli*

<!-- end list -->

  - [飾眼蟾魚屬](../Page/飾眼蟾魚屬.md "wikilink") *Riekertia*
      - [飾眼蟾魚](../Page/飾眼蟾魚.md "wikilink") *Riekertia ellisi*

<!-- end list -->

  - [三海蟾魚屬](../Page/三海蟾魚屬.md "wikilink") *Triathalassothia*
      - [阿根廷三海蟾魚](../Page/阿根廷三海蟾魚.md "wikilink") *Triathalassothia
        argentinus*
      - [蘭氏三海蟾魚](../Page/蘭氏三海蟾魚.md "wikilink") *Triathalassothia
        lambaloti*

<!-- end list -->

  - [光蟾鱼亚科](../Page/光蟾鱼亚科.md "wikilink") Porichthyinae

<!-- end list -->

  - [無光蟾魚屬](../Page/無光蟾魚屬.md "wikilink") *Aphos*
      - [頭孔無光蟾魚](../Page/頭孔無光蟾魚.md "wikilink") *Aphos porosus*

<!-- end list -->

  - [光蟾魚屬](../Page/光蟾魚屬.md "wikilink") *Porichthys*
      - [軟光蟾魚](../Page/軟光蟾魚.md "wikilink") *Porichthys analis*
      - [深水光蟾魚](../Page/深水光蟾魚.md "wikilink") *Porichthys bathoiketes*
      - [上光蟾魚](../Page/上光蟾魚.md "wikilink") *Porichthys ephippiatus*
      - [格氏光蟾魚](../Page/格氏光蟾魚.md "wikilink") *Porichthys greenei*
      - [巴西光蟾魚](../Page/巴西光蟾魚.md "wikilink") *Porichthys kymosemeum*
      - [珠光蟾魚](../Page/珠光蟾魚.md "wikilink") *Porichthys margaritatus*
      - [類光蟾魚](../Page/類光蟾魚.md "wikilink") *Porichthys mimeticus*
      - [細條光蟾魚](../Page/細條光蟾魚.md "wikilink") *Porichthys myriaster*
      - [斑光蟾魚](../Page/斑光蟾魚.md "wikilink") *Porichthys notatus*
      - [多眼光蟾魚](../Page/多眼光蟾魚.md "wikilink") *Porichthys oculellus*
      - [眼韁光蟾魚](../Page/眼韁光蟾魚.md "wikilink") *Porichthys oculofrenum*
      - [少輻光蟾魚](../Page/少輻光蟾魚.md "wikilink") *Porichthys pauciradiatus*
      - [淺水光蟾魚](../Page/淺水光蟾魚.md "wikilink") *Porichthys plectrodon*
      - [大西洋光蟾魚](../Page/大西洋光蟾魚.md "wikilink") *Porichthys
        porosissimus*：又稱孔蟾魚。

<!-- end list -->

  - [海蟾鱼亚科](../Page/海蟾鱼亚科.md "wikilink") Thalassophryninae

<!-- end list -->

  - [兇蟾魚屬](../Page/兇蟾魚屬.md "wikilink") *Daector*
      - [道氏兇蟾魚](../Page/道氏兇蟾魚.md "wikilink") *Daector dowi*
      - [傑氏兇蟾魚](../Page/傑氏兇蟾魚.md "wikilink") *Daector gerringi*
      - （*Daector quadrizonatus*
      - [網紋兇蟾魚](../Page/網紋兇蟾魚.md "wikilink") *Daector reticulata*
      - [施氏兇蟾魚](../Page/施氏兇蟾魚.md "wikilink") *Daector schmitti*

<!-- end list -->

  - [海蟾魚屬](../Page/海蟾魚屬.md "wikilink") *Thalassophryne*
      - [亞馬遜海蟾魚](../Page/亞馬遜海蟾魚.md "wikilink") *Thalassophryne
        amazonica*
      - [加勒比海蟾魚](../Page/加勒比海蟾魚.md "wikilink") *Thalassophryne maculosa*
      - [大眼海蟾魚](../Page/大眼海蟾魚.md "wikilink") *Thalassophryne megalops*
      - [蒙的維海蟾魚](../Page/蒙的維海蟾魚.md "wikilink") *Thalassophryne
        montevidensis*
      - [納氏海蟾魚](../Page/納氏海蟾魚.md "wikilink") *Thalassophryne nattereri*
      - [斑紋海蟾魚](../Page/斑紋海蟾魚.md "wikilink") *Thalassophryne punctata*

## 参考文献

[\*](../Category/蟾魚科.md "wikilink")