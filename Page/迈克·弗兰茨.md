**迈克·弗兰茨**（，），是一位[德国](../Page/德国.md "wikilink")[足球](../Page/足球.md "wikilink")[运动员](../Page/运动员.md "wikilink")，擔任[後衛](../Page/後衛_\(足球\).md "wikilink")。

## 生平

2001年之前在马格德堡队效力。后转会至[沃尔夫斯堡队](../Page/沃尔夫斯堡足球俱乐部.md "wikilink")。其特点为一对一能力较强。2006年转会至[卡尔斯鲁厄队](../Page/卡尔斯鲁厄体育会.md "wikilink")，并签了3年的合同。在卡队的后防线上起着至关重要的作用。在球隊降級後，於2009年夏季以70萬[歐元轉投](../Page/歐元.md "wikilink")[法蘭克福](../Page/法兰克福足球俱乐部.md "wikilink")，合約有效期至2013年6月30日\[1\]。

2011年6月24日隨著法蘭克福降班[德乙後](../Page/德乙.md "wikilink")，免費轉投剛回升[德甲的](../Page/德甲.md "wikilink")[哈化柏林](../Page/柏林赫塔足球俱乐部.md "wikilink")，簽約至2014年夏季\[2\]。

## 參考資料

## 外部連結

  - [卡爾斯魯厄
    迈克·弗兰茨資料](https://web.archive.org/web/20081224030312/http://www.ksc.de/profis/team/0607/maik-franz.html)

  - [Weltfussball.de
    迈克·弗兰茨資料](https://web.archive.org/web/20080520235329/http://www.weltfussball.de/spieler_profil.php?id=572)


[Category:德国足球运动员](../Category/德国足球运动员.md "wikilink")
[Category:足球後衛](../Category/足球後衛.md "wikilink")
[Category:禾夫斯堡球員](../Category/禾夫斯堡球員.md "wikilink")
[Category:卡爾斯魯厄球員](../Category/卡爾斯魯厄球員.md "wikilink")
[Category:法蘭克福球員](../Category/法蘭克福球員.md "wikilink")
[Category:哈化柏林球員](../Category/哈化柏林球員.md "wikilink")
[Category:德甲球員](../Category/德甲球員.md "wikilink")

1.
2.