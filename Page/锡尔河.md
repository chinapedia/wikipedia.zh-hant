[thumb](../Page/image:Khujand.jpg.md "wikilink")[苦盞](../Page/苦盞.md "wikilink")\]\]
[Syr_Darya_River_Floodplain,_Kazakhstan,_Central_Asia.JPG](https://zh.wikipedia.org/wiki/File:Syr_Darya_River_Floodplain,_Kazakhstan,_Central_Asia.JPG "fig:Syr_Darya_River_Floodplain,_Kazakhstan,_Central_Asia.JPG")
**锡尔河**（[英語](../Page/英語.md "wikilink"): Syr
Darya，，，，），中国古称**药杀水**\[1\]，也即**叶河**，中、下游流经沙漠地区，是[中亚著名](../Page/中亚.md "wikilink")[内流河](../Page/内流河.md "wikilink")。

锡尔河发源于[天山山脉](../Page/天山山脉.md "wikilink")，分两源，右源[纳伦河和左源](../Page/纳伦河.md "wikilink")[卡拉达里亚河](../Page/卡拉达里亚河.md "wikilink")，纳伦河为正源，发源于[吉尔吉斯斯坦](../Page/吉尔吉斯斯坦.md "wikilink")。两河在[纳曼干附近汇合后向西流入](../Page/纳曼干.md "wikilink")[费尔干纳谷地](../Page/费尔干纳谷地.md "wikilink")，在[塔吉克斯坦](../Page/塔吉克斯坦.md "wikilink")[苦盏出谷](../Page/苦盏.md "wikilink")，流至[别卡巴德后转而向西北](../Page/别卡巴德.md "wikilink")，流经[乌兹别克斯坦和](../Page/乌兹别克斯坦.md "wikilink")[哈萨克斯坦后](../Page/哈萨克斯坦.md "wikilink")，注入[咸海](../Page/咸海.md "wikilink")。
[Aral_map.png](https://zh.wikipedia.org/wiki/File:Aral_map.png "fig:Aral_map.png")

全长2212公里，流域面积21.9万平方公里，河口多年平均流量500立方米/秒，年均径流量336亿立方米。

锡尔河主要支流有[阿汉加兰河](../Page/阿汉加兰河.md "wikilink")、[奇尔奇克河](../Page/奇尔奇克河.md "wikilink")、[克列斯河和](../Page/克列斯河.md "wikilink")[阿雷西河](../Page/阿雷西河.md "wikilink")。

## 參考文獻

## 参见

  - [阿姆河](../Page/阿姆河.md "wikilink")，中亞最長之內流河，注入鹹海南部。

[Category:鹹海](../Category/鹹海.md "wikilink")
[\*](../Category/錫爾河.md "wikilink")
[Category:内流河](../Category/内流河.md "wikilink")
[Category:中亞地理](../Category/中亞地理.md "wikilink")
[Category:亚洲跨國河流](../Category/亚洲跨國河流.md "wikilink")
[Category:吉爾吉斯斯坦河流](../Category/吉爾吉斯斯坦河流.md "wikilink")
[Category:塔吉克斯坦河流](../Category/塔吉克斯坦河流.md "wikilink")
[Category:烏茲別克斯坦河流](../Category/烏茲別克斯坦河流.md "wikilink")
[Category:哈萨克斯坦河流](../Category/哈萨克斯坦河流.md "wikilink")
[Category:塔吉克斯坦-烏茲別克斯坦邊界](../Category/塔吉克斯坦-烏茲別克斯坦邊界.md "wikilink")
[Category:哈薩克斯坦-烏茲別克斯坦邊界](../Category/哈薩克斯坦-烏茲別克斯坦邊界.md "wikilink")
[Category:絲綢之路地點](../Category/絲綢之路地點.md "wikilink")

1.  <https://zh.wikisource.org/wiki/新唐書/卷221下> 西南有藥殺水，入中國謂之真珠河，亦曰質河。