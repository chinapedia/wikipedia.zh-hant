**銀行控股公司**（），又稱為**金融控股公司**（；簡稱「金控」），係透過成立一間[控股公司的間接方式來進行](../Page/控股公司.md "wikilink")[金融業跨業整合](../Page/金融業.md "wikilink")。金融控股公司並不能直接從事金融業務或其他商業，但它可投資控股的範圍則包括了[銀行業](../Page/銀行.md "wikilink")、[票券金融業](../Page/票券.md "wikilink")、[信用卡業](../Page/信用卡.md "wikilink")、[信託業](../Page/信託.md "wikilink")、[保險業](../Page/保險.md "wikilink")、[證券業](../Page/證券.md "wikilink")、[期貨業](../Page/期貨.md "wikilink")、[創投業](../Page/創投.md "wikilink")、外國金融機構等。整合後可透過交叉行銷，在同一個[行銷通路上販賣保險](../Page/行銷通路.md "wikilink")、證券、債券、信用卡等各種[金融商品以產生](../Page/金融商品.md "wikilink")[綜效的](../Page/綜效.md "wikilink")[組織型態](../Page/組織型態.md "wikilink")。

## 概況

在[經濟大恐慌後](../Page/經濟大恐慌.md "wikilink")，[美系銀行間所建立禁止](../Page/美系銀行.md "wikilink")[銀行跨業的銀行業](../Page/銀行.md "wikilink")[防火牆機制](../Page/防火牆.md "wikilink")，在歐系的綜合銀行強力競爭之下欠缺競爭力，故在金融業[百貨公司化的市場要求下](../Page/百貨公司.md "wikilink")，二十世紀八十年代在美國出現以銀行控股公司經營不同金融行業。

金控的各個關係企業間可藉此緊密連結後，突破以往金融分業的藩籬，透過金融控股公司的[交叉行銷](../Page/交叉行銷.md "wikilink")（Cross-Selling），在同一個[行銷通路上販賣保險](../Page/行銷通路.md "wikilink")、證券、債券、信用卡等各種金融商品，並藉此提升各個金控子公司對客戶效率化的服務，進而降低[管銷費用成本](../Page/管銷.md "wikilink")，提高競爭力，以提供「一次購足、一站到底」的全方位金融服務。

### 問題

如何藉由財務面的分析找出彼此的優勢而能夠彼此[互補](../Page/互補.md "wikilink")，再者就是合併成為金控後落實原先預期的相關計畫，使金控的[綜效發會到極致](../Page/綜效.md "wikilink")，成為名副其實的「金控」，這才是金控組成後的重要工作，否則，一切為合併而合併，並沒有事先做過審慎評估，而盲目改制，一旦[掛牌上市後](../Page/掛牌.md "wikilink")，投資人很快就會從股價加以反應。

## 世界各國的銀行控股公司

###

  - [臺灣金融控股公司](../Page/臺灣金融控股公司.md "wikilink")
  - [華南金融控股公司](../Page/華南金融控股公司.md "wikilink")
  - [富邦金融控股公司](../Page/富邦金融控股公司.md "wikilink")
  - [國泰金融控股公司](../Page/國泰金融控股公司.md "wikilink")
  - [中華開發金融控股公司](../Page/中華開發金融控股公司.md "wikilink")
  - [玉山金融控股公司](../Page/玉山金融控股公司.md "wikilink")
  - [元大金融控股公司](../Page/元大金融控股公司.md "wikilink")
  - [兆豐金融控股公司](../Page/兆豐金融控股公司.md "wikilink")
  - [台新金融控股公司](../Page/台新金融控股公司.md "wikilink")
  - [新光金融控股公司](../Page/新光金融控股公司.md "wikilink")
  - [國票金融控股公司](../Page/國票金融控股公司.md "wikilink")
  - [永豐金融控股公司](../Page/永豐金融控股公司.md "wikilink")
  - [中國信託金融控股公司](../Page/中國信託金融控股公司.md "wikilink")
  - [第一金融控股公司](../Page/第一金融控股公司.md "wikilink")
  - [日盛金融控股公司](../Page/日盛金融控股公司.md "wikilink")
  - [合作金庫金融控股公司](../Page/合作金庫金融控股公司.md "wikilink")

###

  - [三菱UFJ金融控股集團](../Page/三菱UFJ金融集團.md "wikilink")
  - [索尼金融控股集團](../Page/索尼金融控股.md "wikilink")
  - [三井住友金融控股集團](../Page/三井住友金融集團.md "wikilink")
  - [九州親和控股集團](../Page/九州親和控股.md "wikilink")
  - [福岡金融控股集團](../Page/福岡金融集團股份.md "wikilink")
  - [札幌北洋控股集團](../Page/札幌北洋控股股份.md "wikilink")
  - [瑞穗金融控股集團](../Page/瑞穗金融集團.md "wikilink")
  - [福岡金融集團](../Page/福岡金融集團股份.md "wikilink")
  - [山口金融控股集團](../Page/山口金融集團股份.md "wikilink")
  - [葉種控股股份](../Page/葉種控股股份.md "wikilink")
  - [思佰益](../Page/思佰益.md "wikilink")

###

###

  - [中信国际金融](../Page/中信国际金融.md "wikilink")

## 參考資料

[+](../Category/銀行.md "wikilink")
[金融控股公司](../Category/金融控股公司.md "wikilink")