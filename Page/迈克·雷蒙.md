**迈克·雷蒙**（，），[美国职业](../Page/美国.md "wikilink")[围棋棋手](../Page/围棋.md "wikilink")，[日本棋院职业九段](../Page/日本棋院.md "wikilink")。迈克·雷蒙是世界上唯一一位非[东亚裔的围棋九段](../Page/东亚.md "wikilink")。

## 生平

雷蒙出生于美国[加利福尼亚州](../Page/加利福尼亚州.md "wikilink")[圣巴巴拉](../Page/圣巴巴拉.md "wikilink")，1974年在父亲的影响下开始学习围棋，很快显露出天分。1976年前往[日本学棋](../Page/日本.md "wikilink")，次年拜[大枝雄介为师](../Page/大枝雄介.md "wikilink")。1981年入段，正式成为职业棋手，2000年升为九段。

雷蒙曾获得过1985年留园杯冠军、1990年俊英战亚军、1992年[日本新人王战亚军](../Page/日本新人王战.md "wikilink")，并在2003年打入[棋圣战四强](../Page/棋圣战.md "wikilink")，最后败于[依田纪基](../Page/依田纪基.md "wikilink")。

除了做為[母語的](../Page/母語.md "wikilink")[英语](../Page/英语.md "wikilink")，雷蒙還能说流利的[日语](../Page/日语.md "wikilink")，2011年参加第27届[美国围棋大会](../Page/美国围棋大会.md "wikilink")。

2016年3月，[Google的子公司](../Page/Google.md "wikilink")[Google
DeepMind所開發電腦圍棋程式](../Page/Google_DeepMind.md "wikilink")[AlphaGo與世界冠军](../Page/AlphaGo.md "wikilink")[南韓籍九段棋士](../Page/南韓.md "wikilink")[李世乭](../Page/李世乭.md "wikilink")（）進行[五番棋对战](../Page/AlphaGo李世乭五番棋.md "wikilink")，这个程序使用谷歌位于美国的[云计算服务器](../Page/云计算.md "wikilink")，并透過光缆网络连接韩国\[1\]。五場比賽地點為[南韓](../Page/南韓.md "wikilink")[首爾](../Page/首爾.md "wikilink")[四季酒店](../Page/四季酒店.md "wikilink")，分別於2016年3月9日、10日、12日、13日和15日舉行\[2\]\[3\]
，由Deepmind团队在[YouTube全球直播并由邁克](../Page/YouTube.md "wikilink")·雷蒙擔任英语解说。

## 家庭

其妻牛娴娴是[中国棋院职业围棋三段](../Page/中国棋院.md "wikilink")。牛娴娴的姐姐职业围棋五段[牛力力是](../Page/牛力力.md "wikilink")[吴清源生前的秘书](../Page/吴清源.md "wikilink")。牛力力的丈夫是[中国象棋特级大师](../Page/中国象棋.md "wikilink")[赵国荣](../Page/赵国荣.md "wikilink")。

迈克·雷蒙和牛娴娴有两个女儿。

## 參考資料

## 外部連結

  - [楚天都市报-中国女婿迈克·雷蒙-世界第一个西洋九段](http://www.cnhubei.com/200603/ca1023440.htm)

  - [日本棋院迈克·雷蒙介绍](http://www.nihonkiin.or.jp/player/htm/ki000174.htm)

[Category:日本围棋棋手](../Category/日本围棋棋手.md "wikilink")
[Category:美国围棋棋手](../Category/美国围棋棋手.md "wikilink")

1.
2.
3.