**貢德人**（），又稱岡德人，南亞[印度少數民族](../Page/印度.md "wikilink")，主要分佈於印度中部。多數使用，屬[達羅毗荼語系貢德語族](../Page/達羅毗荼語系.md "wikilink")，也有許多人使用周圍其他民族的語言。主要信仰為[印度教及](../Page/印度教.md "wikilink")[泛靈信仰](../Page/泛靈信仰.md "wikilink")。主要從事農業，栽培玉米、水稻、[油菜](../Page/油菜.md "wikilink")、棉花等作物。20世紀尚行[刀耕火種](../Page/刀耕火種.md "wikilink")，兼事[狩獵採集](../Page/狩獵採集.md "wikilink")。擁有豐富的傳統藝術及歷史悠久的[口傳文學](../Page/口頭文學.md "wikilink")。盛行、[交換婚和](../Page/交換婚.md "wikilink")[私奔婚](../Page/私奔.md "wikilink")；[交表婚也相當普遍](../Page/平行從表與交錯從表.md "wikilink")。

## 民族分佈、人口與語言

### 民族分佈

貢德人主要分布於[印度中部地區](../Page/印度.md "wikilink")，橫跨[中央邦](../Page/中央邦.md "wikilink")、[北方邦](../Page/北方邦.md "wikilink")、[比哈爾邦](../Page/比哈爾邦.md "wikilink")、[賈坎德邦](../Page/賈坎德邦.md "wikilink")、[奧里薩邦](../Page/奧里薩邦.md "wikilink")、[安得拉邦](../Page/安得拉邦.md "wikilink")、[泰倫加納邦](../Page/泰倫加納邦.md "wikilink")、[馬哈拉施特拉邦](../Page/馬哈拉施特拉邦.md "wikilink")、[恰蒂斯加爾邦](../Page/恰蒂斯加爾邦.md "wikilink")，少數分布於東部[西孟加拉邦與](../Page/西孟加拉邦.md "wikilink")[阿薩姆邦](../Page/阿薩姆邦.md "wikilink")、南部[卡納塔克邦以及西部](../Page/卡納塔克邦.md "wikilink")[古吉拉特邦等地區](../Page/古吉拉特邦.md "wikilink")。

<table>
<tbody>
<tr class="odd">
<td></td>
<td></td>
<td><ol>
<li><a href="../Page/安得拉邦.md" title="wikilink">安得拉邦</a></li>
<li><a href="../Page/阿魯納恰爾邦.md" title="wikilink">阿魯納恰爾邦</a></li>
<li><a href="../Page/阿薩姆邦.md" title="wikilink">阿薩姆邦</a></li>
<li><a href="../Page/比哈爾邦.md" title="wikilink">比哈爾邦</a></li>
<li><a href="../Page/恰蒂斯加爾邦.md" title="wikilink">恰蒂斯加爾邦</a></li>
<li><a href="../Page/果阿邦.md" title="wikilink">果阿邦</a></li>
<li><a href="../Page/古吉拉特邦.md" title="wikilink">古吉拉特邦</a></li>
<li><a href="../Page/哈里亞納邦.md" title="wikilink">哈里亞納邦</a></li>
<li><a href="../Page/喜馬偕爾邦.md" title="wikilink">喜馬偕爾邦</a></li>
<li><a href="../Page/查谟-克什米尔邦.md" title="wikilink">查谟-克什米尔邦</a></li>
<li><a href="../Page/賈坎德邦.md" title="wikilink">賈坎德邦</a></li>
<li><a href="../Page/卡納塔克邦.md" title="wikilink">卡納塔克邦</a></li>
<li><a href="../Page/喀拉拉邦.md" title="wikilink">喀拉拉邦</a></li>
<li><a href="../Page/中央邦.md" title="wikilink">中央邦</a></li>
</ol></td>
<td><ol start="15">
<li><a href="../Page/馬哈拉施特拉邦.md" title="wikilink">馬哈拉施特拉邦</a></li>
<li><a href="../Page/曼尼普爾邦.md" title="wikilink">曼尼普爾邦</a></li>
<li><a href="../Page/梅加拉亞邦.md" title="wikilink">梅加拉亞邦</a></li>
<li><a href="../Page/米佐拉姆邦.md" title="wikilink">米佐拉姆邦</a></li>
<li><a href="../Page/那加蘭邦.md" title="wikilink">那加蘭邦</a></li>
<li><a href="../Page/奧里薩邦.md" title="wikilink">奧里薩邦</a></li>
<li><a href="../Page/旁遮普邦.md" title="wikilink">旁遮普邦</a></li>
<li><a href="../Page/拉賈斯坦邦.md" title="wikilink">拉賈斯坦邦</a></li>
<li><a href="../Page/錫金邦.md" title="wikilink">錫金邦</a></li>
<li><a href="../Page/泰米爾納德邦.md" title="wikilink">泰米爾納德邦</a></li>
<li><a href="../Page/泰倫迦納邦.md" title="wikilink">泰倫迦納邦</a></li>
<li><a href="../Page/特里普拉邦.md" title="wikilink">特里普拉邦</a></li>
<li><a href="../Page/北方邦.md" title="wikilink">北方邦</a></li>
<li><a href="../Page/北阿坎德邦.md" title="wikilink">北阿坎德邦</a></li>
<li><a href="../Page/西孟加拉邦.md" title="wikilink">西孟加拉邦</a></li>
</ol></td>
<td><ul>
<li>A. <a href="../Page/安达曼-尼科巴群岛.md" title="wikilink">安达曼-尼科巴群岛</a></li>
<li>B. <a href="../Page/昌迪加尔.md" title="wikilink">昌迪加尔</a>（<a href="../Page/旁遮普邦.md" title="wikilink">旁遮普邦和</a><a href="../Page/哈里亚纳邦.md" title="wikilink">哈里亚纳邦首府</a>）</li>
<li>C. <a href="../Page/达德拉-纳加尔哈维利.md" title="wikilink">达德拉-纳加尔哈维利</a></li>
<li>D. <a href="../Page/达曼-第乌.md" title="wikilink">达曼-第乌</a></li>
<li>E. <a href="../Page/拉克沙群岛.md" title="wikilink">拉克沙群岛</a></li>
<li>F. <a href="../Page/新德里.md" title="wikilink">新德里</a>（<a href="../Page/印度.md" title="wikilink">印度首都</a>）</li>
<li>G. <a href="../Page/本地治里.md" title="wikilink">本地治里</a></li>
</ul></td>
<td></td>
</tr>
</tbody>
</table>

### 人口

根據人口普查報告，1971年貢德人口為4,728,796人\[1\]，1991年約930萬人\[2\]，2001將近1100萬人，迄今已超過1400萬人。\[3\]

### 語言

貢德人主要使用的語言為，但有各種不同的貢德方言，其中一部分彼此不易聽懂；另一些貢德人已經喪失自己的語言，轉而使用當地佔統治地位的語言，如[印地語和](../Page/印地語.md "wikilink")[馬拉提語](../Page/馬拉提語.md "wikilink")，少數則使用[泰盧固語](../Page/泰盧固語.md "wikilink")、[歐利亞語及](../Page/歐利亞語.md "wikilink")[阿薩姆語等](../Page/阿薩姆語.md "wikilink")。

## 地理環境

[India_topo_big.jpg](https://zh.wikipedia.org/wiki/File:India_topo_big.jpg "fig:India_topo_big.jpg")
 貢德人主要居住於印度中部的[德干高原](../Page/德干高原.md "wikilink")。

### 地形

德干高原為世界最大的熔岩台地，屬於[岡瓦那大陸的一部分](../Page/岡瓦那大陸.md "wikilink")。地勢西高東低，發源於此的河流多向東南方流，注入[孟加拉灣](../Page/孟加拉灣.md "wikilink")。

### 氣候

德干高原屬於典型[熱帶季風氣候](../Page/熱帶季風氣候.md "wikilink")，東西兩側雨量豐富，但高原東西緣的水氣分別受到[東高止山脈及](../Page/東高止山脈.md "wikilink")[西高止山脈阻擋](../Page/西高止山脈.md "wikilink")，高原內部高溫少雨，以灌木和高草為主要地貌。

## 歷史沿革

貢德人9至13世紀定居於[岡瓦那大陸](../Page/岡瓦那大陸.md "wikilink")（今日[中央邦東部](../Page/中央邦.md "wikilink")），16至18世紀建立貢德王朝，統治印度中部的四個王國，葛哈-曼德拉（Garha-Mandla）、卡爾拉（Kherla）、德沃加爾（）、昌達（Chanda），並建造為數眾多的堡壘、宮殿、寺廟、池塘。1690年，蒙兀兒王朝衰落後，貢德人也掌控了[摩臘婆地區](../Page/摩臘婆.md "wikilink")。[英國殖民時期貢德人為了挑戰殖民政權](../Page/英屬印度.md "wikilink")，與英國發生多次戰爭。\[4\]

## 社會、家庭與婚姻

貢德人有明確的父系及族長氏族制度，施行[氏族外婚](../Page/外婚制.md "wikilink")，並視族內通婚為亂倫。貢德人會以動植物為氏族命名，亦即所謂的圖騰制度。除了氏族外婚規定之外，氏族並無其他重要功能。
貢德人的婚姻型態普遍為[交表婚及](../Page/平行從表與交錯從表.md "wikilink")[一夫多妻制](../Page/一夫多妻制.md "wikilink")，並施行[妻姊妹婚和](../Page/妻姊妹婚.md "wikilink")[夫兄弟婚](../Page/夫兄弟婚.md "wikilink")。\[5\]

## 產業與生活

### 產業

貢德人主要的產業型態為農業。貢德人最初為[狩獵採集者](../Page/狩獵採集.md "wikilink")，後來轉為採用[輪耕技術](../Page/輪耕.md "wikilink")，它不只是一種農業型態，更是一種生活方式。輪耕不需要利用役畜，同時也讓耕種者有較多閒暇時間進行捕魚狩獵等活動。但政府因為輪耕會破壞森林，而迫使多數貢德人轉為犁田或梯田耕作。\[6\]

## 信仰與習俗

### 信仰

貢德人主要信仰[印度教](../Page/印度教.md "wikilink")，部份則屬於[泛靈信仰](../Page/泛靈信仰.md "wikilink")。\[7\]

### 習俗

貢德人的節慶多半與農業及生命週期（出生、結婚、生病、死亡）相關。祭典的[犧牲與](../Page/犧牲.md "wikilink")[祭品由村裡](../Page/祭品.md "wikilink")[祭司或家族長老負責](../Page/祭司.md "wikilink")，犧牲與祭品的種類則視祭拜的神而定。\[8\]

## 藝術與文學

### 藝術

[_Gond_Painting_of_MP1.JPG](https://zh.wikipedia.org/wiki/File:_Gond_Painting_of_MP1.JPG "fig:_Gond_Painting_of_MP1.JPG")
貢德人擁有豐富的傳統藝術，包含陶藝、籃子編織、身體刺青、地板漆畫、壁畫等。\[9\]

貢德傳統繪畫風格可追溯至[中石器時代的洞窟壁畫](../Page/中石器時代.md "wikilink")，靈感多源自其[泛靈信仰](../Page/泛靈信仰.md "wikilink")，無論山河石頭或樹，皆有靈魂存在，因此，一切都是神聖的，貢德人藉由繪畫表達對萬物的敬畏之心。雖然大多數貢德繪畫靈感取於大自然，但繪畫題材也汲取自神話和印度傳說及部落的日常生活。\[10\]

### 文學

貢德人擁有歷史悠久的[口傳文學](../Page/口頭文學.md "wikilink")。貢德世襲吟遊詩人稱為普拉漢（Pardhan），他們傳承著貢德人的傳說故事和神話，以及稱頌民族英雄凌果（Lingo）的英雄史詩
\[11\]

## 著名人士

  - – 統治者

  - – 政治家，曾任[喜馬偕爾邦首長](../Page/喜馬偕爾邦.md "wikilink")

  - – 政治家

  - – 政治家

  - – 政治家

  - – 政治家

  - – 自由鬥士

  - – 藝術家

  - – 藝術家

  - – 藝術家

## 參考資料

[Category:亞洲民族](../Category/亞洲民族.md "wikilink")
[Category:印度民族](../Category/印度民族.md "wikilink")

1.  Stephen Fuchs [Culture Summary:
    Gond](http://ehrafworldcultures.yale.edu.autorpa.lib.nccu.edu.tw/ehrafe/azCultures.do?thisChar=G#thisChar=G)
    eHRAF World Cultures　瀏覽日期：2016-03-01

2.  Indian Tribes ISBN 81-230-0328-5

3.  [Joshua Project: Gond in
    India](https://joshuaproject.net/people_groups/16855/IN)　瀏覽日期：2016-03-19

4.  [Eco India: Gonds
    Tribe](http://www.ecoindia.com/tribes/gonds.html)　瀏覽日期：2016-07-09

5.
6.
7.  [PG India:
    Gond](http://www.peoplegroupsindia.com/profiles/gond/)　瀏覽日期：2016-03-19

8.
9.  [Countries and Their Cultures:
    Gonds](http://www.everyculture.com/wc/Germany-to-Jamaica/Gonds.html)　瀏覽日期：2016-03-19

10. [Utsavpedia: Gond
    Painting](http://www.utsavpedia.com/motifs-embroideries/gond-painting/)　瀏覽日期：2016-06-20

11.