[May_Shing_Court_Access_2008.jpg](https://zh.wikipedia.org/wiki/File:May_Shing_Court_Access_2008.jpg "fig:May_Shing_Court_Access_2008.jpg")及美林邨，但到2016年加裝圍欄後，此通道只限美城苑居民使用\]\]
[May_Shing_Court_Kwai_Shing_House_2017.jpg](https://zh.wikipedia.org/wiki/File:May_Shing_Court_Kwai_Shing_House_2017.jpg "fig:May_Shing_Court_Kwai_Shing_House_2017.jpg")
[May_Shing_Court_Fitness_Facilities_2008.jpg](https://zh.wikipedia.org/wiki/File:May_Shing_Court_Fitness_Facilities_2008.jpg "fig:May_Shing_Court_Fitness_Facilities_2008.jpg")
**美城苑**（）是[香港房屋委員會的](../Page/香港房屋委員會.md "wikilink")[居屋屋苑之一](../Page/居屋.md "wikilink")，位於[新界](../Page/新界.md "wikilink")[沙田區的](../Page/沙田區.md "wikilink")[大圍](../Page/大圍.md "wikilink")[美田路](../Page/美田路.md "wikilink")34號，[美林邨隔鄰](../Page/美林邨.md "wikilink")，屬居屋出售分期的第六期甲屋苑，於1984至1985年間入伙。房委會當年為屋苑命名時，「美」字的英文音譯為
"May"，與區內附近其他「美」字開頭的屋邨及屋苑採用的「Mei」有所不同，較為特別。

## 重大事件

  - 2016年6月26日下午五時許，逸城閣一部升降機由6樓急降至地面，兩名女乘客遇上極大[離心力](../Page/離心力.md "wikilink")，失去平衡跌倒地下，其中一名外籍女子腳傷送院。事發後，不少屋苑住戶要求管理處交代事件。\[1\]
  - 2016年，屋苑法團以加強保安為理由，在行人通道加裝圍欄，附近居民批評法團罔顧鄰近居民使用體育館、學校以及行人通道的權利。
  - 2018年5月，其中一幢大廈入口的密碼設為「8964」，唯有住戶去信投訴指政治敏感，屋苑管理公司宜居服務有限公司發通告表示，為免造成誤會及不和諧，最後重新設定密碼。\[2\]

## 當區議員

  - [董健莉](../Page/董健莉.md "wikilink")

## 樓宇

| 樓宇名稱    | 樓宇類型                                                                          | 落成年份\[3\] |
| ------- | ----------------------------------------------------------------------------- | --------- |
| 貴城閣（A座） | 風車型                                                                           | 1984      |
| 逸城閣（B座） | [Y2型](../Page/Y2型.md "wikilink")（原先為[美林邨未命名的](../Page/美林邨.md "wikilink")5及6座） |           |
| 暉城閣（C座） | 1985                                                                          |           |

## 交通

[Mei_Lam_Estate_Bus_Terminal_2017.jpg](https://zh.wikipedia.org/wiki/File:Mei_Lam_Estate_Bus_Terminal_2017.jpg "fig:Mei_Lam_Estate_Bus_Terminal_2017.jpg")

<div class="NavFrame collapsed" style="color: black; background: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background: #FFFF00; margin: 0 auto; padding: 0 10px; font-weight:normal;">

交通路線列表

</div>

<div class="NavContent" style="background: #FFFF00; margin: 0 auto; padding: 0 10px;">

  - [美田路](../Page/美田路.md "wikilink")

<!-- end list -->

  - 美松苑專線小巴總站

</div>

</div>

## 鄰近建築

  - [美林邨美槐樓](../Page/美林邨.md "wikilink")
  - [康文署美林體育館](../Page/康樂及文化事務署.md "wikilink")
  - [樂道中學](../Page/樂道中學.md "wikilink")
  - [香港聖瑪加利女書院](../Page/香港聖瑪加利女書院.md "wikilink")
  - [循理會美林小學](../Page/循理會美林小學.md "wikilink")
  - [博雅山莊](../Page/博雅山莊.md "wikilink")
  - [美松苑](../Page/美松苑.md "wikilink")
  - [美柏苑](../Page/美柏苑.md "wikilink")

## 外部連結

  - [香港房屋委員會 -
    美城苑簡介](http://www.housingauthority.gov.hk/tc/global-elements/estate-locator/detail.html?propertyType=2&id=3006)

[Category:大圍](../Category/大圍.md "wikilink")

1.  [美城苑升降機急墜6層樓　事主親述恐怖經過：跌倒在地　心諗死啦
    《香港01》2016-06-26](http://www.hk01.com/%E6%B8%AF%E8%81%9E/28318/%E7%BE%8E%E5%9F%8E%E8%8B%91%E5%8D%87%E9%99%8D%E6%A9%9F%E6%80%A5%E5%A2%9C6%E5%B1%A4%E6%A8%93-%E4%BA%8B%E4%B8%BB%E8%A6%AA%E8%BF%B0%E6%81%90%E6%80%96%E7%B6%93%E9%81%8E-%E8%B7%8C%E5%80%92%E5%9C%A8%E5%9C%B0-%E5%BF%83%E8%AB%97%E6%AD%BB%E5%95%A6)
2.
3.  <http://www.rvd.gov.hk/doc/tc/nt_201504.pdf>