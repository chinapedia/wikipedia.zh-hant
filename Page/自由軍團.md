**自由军团**（[德语](../Page/德语.md "wikilink")：Freikorps，亦即[民兵](../Page/民兵.md "wikilink")）原指志愿军。首批自由军团是在[七年战争中由](../Page/七年战争.md "wikilink")[腓特烈二世招募](../Page/腓特烈二世.md "wikilink")。其他後起之例包括[拿破仑战争中](../Page/拿破仑战争.md "wikilink")，[普鲁士将领](../Page/普鲁士.md "wikilink")[路德维希·阿道夫·威廉·冯·吕佐夫对抗](../Page/路德维希·阿道夫·威廉·冯·吕佐夫.md "wikilink")[法军的](../Page/法军.md "wikilink")[吕佐夫志愿军](../Page/吕佐夫志愿军.md "wikilink")。自由军一般被[正规军认为不太可靠](../Page/正规军.md "wikilink")，因此通常只作为[哨兵及执行小型任务](../Page/哨兵.md "wikilink")。

然而該德语词语之義随後有所变化。1918年後，自由军团指[第一次世界大战後](../Page/第一次世界大战.md "wikilink")，[德国军队退出战场後组成之](../Page/德国.md "wikilink")[準軍事组织](../Page/準軍事组织.md "wikilink")。它是威玛时代半军事组织之一。很多德国战场老手有感与文官政府脱节，故此加入军团以求在该军事单位寻找安稳生活。也有其他军人不满战败，故想一起打倒[共产党的起义或报复](../Page/德国共产党.md "wikilink")（详见[刀刺在背传说](../Page/刀刺在背传说.md "wikilink")）。在獲得[国防部长](../Page/德國國防部.md "wikilink")[古斯塔夫·诺斯克大力支持下](../Page/古斯塔夫·诺斯克.md "wikilink")，军团镇压共产党起事，包括於1919年1月15日谋杀[卡尔·李卜克内西与](../Page/卡尔·李卜克内西.md "wikilink")[罗莎·卢森堡](../Page/罗莎·卢森堡.md "wikilink")。军团也在同年推翻了[巴伐利亚苏维埃共和国](../Page/巴伐利亚苏维埃共和国.md "wikilink")。

某些军团成员於[波罗的海](../Page/波罗的海.md "wikilink")、[西里西亚与蘇聯作战](../Page/西里西亚.md "wikilink")，有时候甚至能战胜正规军。军团在1920年被正式解散。某些军团成员其後於该年3月发动[卡普政变](../Page/卡普政变.md "wikilink")，但以失败收場。

部分军团成员為後起之[纳粹党要员](../Page/纳粹党.md "wikilink")，包括[衝鋒隊首领](../Page/衝鋒隊.md "wikilink")[恩斯特·罗姆和](../Page/恩斯特·罗姆.md "wikilink")[纳粹党副元首](../Page/纳粹党.md "wikilink")[鲁道夫·赫斯](../Page/鲁道夫·赫斯.md "wikilink")。

1920年，[阿道夫·希特勒方才开始其政治事业](../Page/阿道夫·希特勒.md "wikilink")，僅是加入名不经传之[德意志國家社會主義工人党](../Page/德意志國家社會主義工人党.md "wikilink")（後之纳粹党）。多數自由军团成员於[纳粹时代期间僅為局外人](../Page/纳粹德国.md "wikilink")。因此军团老手一直有争议之话题即為：「我们与共产党战斗时，当时希特勒究竟在哪儿？」。

## 參見

（有待補充）

  -
  - [馬丁·鮑曼](../Page/馬丁·鮑曼.md "wikilink")

  - [萊因哈特·海德里希](../Page/萊因哈特·海德里希.md "wikilink")

  - [海因里希·希姆莱](../Page/海因里希·希姆莱.md "wikilink")

  - [漢斯·法郎克](../Page/漢斯·法郎克.md "wikilink")

  -
  -
  - [恩斯特·罗姆](../Page/恩斯特·罗姆.md "wikilink")

  -
  - [胡戈·施佩勒](../Page/胡戈·施佩勒.md "wikilink")

  - [格里哥·斯特拉瑟](../Page/格里哥·斯特拉瑟.md "wikilink")

  - [奥托·斯特拉瑟](../Page/奥托·斯特拉瑟.md "wikilink")

  -
  -
  -
  - [钢盔团](../Page/钢盔团.md "wikilink")

  -
## 相關條目

  - [準軍事組織](../Page/準軍事組織.md "wikilink")

## 外部連結

  - [1918-23年德国自由军团之历史](https://web.archive.org/web/20151224114127/http://www.reitergenosten.de/)
  - [Axis History Factbook;
    Freikorps章节](http://www.axishistory.com/index.php?id=5773) -
    Marcus Wendel等

[Category:反共主义](../Category/反共主义.md "wikilink")
[Category:民兵](../Category/民兵.md "wikilink")
[Category:威瑪共和國軍事](../Category/威瑪共和國軍事.md "wikilink")