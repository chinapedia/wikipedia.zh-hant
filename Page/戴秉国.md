**戴秉国**（），[土家族](../Page/土家族.md "wikilink")，[贵州](../Page/贵州.md "wikilink")[印江人](../Page/印江县.md "wikilink")，[中国职业](../Page/中华人民共和国.md "wikilink")[外交官](../Page/外交官.md "wikilink")；[四川大学外语系俄语专业毕业](../Page/四川大学.md "wikilink")，曾在[外交学院进修](../Page/外交学院.md "wikilink")。1973年6月加入[中国共产党](../Page/中国共产党.md "wikilink")；是中共第十五、十六、十七届中央委员。曾任[中华人民共和国](../Page/中华人民共和国.md "wikilink")[国务委员](../Page/国务委员.md "wikilink")、[中央外事工作领导小组办公室主任](../Page/中央外事工作委员会办公室.md "wikilink")，[中共中央对外联络部部长](../Page/中共中央对外联络部.md "wikilink")，[外交部副部长等职](../Page/中华人民共和国外交部.md "wikilink")，现任[暨南大学校董会董事长](../Page/暨南大学.md "wikilink")。

## 成长背景

戴秉国出生在贫困偏僻的[贵州](../Page/贵州.md "wikilink")[印江的一个山村](../Page/印江.md "wikilink")。家境贫寒的戴秉国，幼年时就开始学做农活；但父亲还是坚持让他在村里的[私塾读书](../Page/私塾.md "wikilink")；[中华人民共和国成立后](../Page/中华人民共和国成立.md "wikilink")，依靠国家助学金读完中学。1959年，戴秉国考入[四川大学外语系](../Page/四川大学.md "wikilink")。毕业前，受学校推荐，参加国家[外交学院研究生班考试](../Page/外交学院.md "wikilink")，成为被录取的50人之一。

## 外交生涯

在[外交学院短暂的进修后](../Page/外交学院.md "wikilink")，25岁的戴秉国被选调到[中华人民共和国外交部苏联东欧司工作](../Page/中华人民共和国外交部苏联东欧司.md "wikilink")，开始了近半个世纪的职业外交生涯。先后担任外交部苏欧司科员，驻[苏联使馆随员](../Page/苏联.md "wikilink")，外交部苏欧司副处长、处长等职。在[文化大革命期间](../Page/文化大革命.md "wikilink")，曾被下放[五七干校劳动](../Page/五七干校.md "wikilink")。

1985年1月，44岁的戴秉国“大器晚成”，终获提拔，升任苏欧司副司长；一年后，接任司长。因此，戴秉国在1980年代中期，参与了中苏实现关系正常化的进程。1989年10月，被任命为[驻匈牙利大使](../Page/中国驻匈牙利大使列表.md "wikilink")；两年后，升任[外交部部长助理](../Page/中华人民共和国外交部.md "wikilink")；1993年12月，晋升副部长。

### 红色联络官

戴秉国长期从事对[苏联](../Page/苏联.md "wikilink")、[东欧等前](../Page/东欧.md "wikilink")“社会主义国家”的外交事务。1995年6月，戴秉国由隶属[国务院的](../Page/中华人民共和国国务院.md "wikilink")“外交系统”转到隶属[中直机关的](../Page/中共中央直属机构.md "wikilink")“外联系统”，出任[中共中央对外联络部副部长](../Page/中共中央对外联络部.md "wikilink")；同一些“红色国家”、或曾经是“共产主义阵营”国家的原执政党保持联系和沟通。1997年8月，在[中共十五大召开前夕](../Page/中共十五大.md "wikilink")，戴秉国升任中联部部长。

### 处理朝核危机

2002年底，[朝鲜半岛核危机再度爆发](../Page/朝鲜半岛.md "wikilink")。2003年1月10日，[朝鲜公开宣布退出](../Page/朝鲜.md "wikilink")《[不扩散核武器条约](../Page/不扩散核武器条约.md "wikilink")》。为了应对危机，当年5月，戴秉国返回[外交部](../Page/中华人民共和国外交部.md "wikilink")，继续出任副部长（至2008年4月），掛帅斡旋“[朝鲜核危机](../Page/朝鲜核危机.md "wikilink")”；2005年3月，接替[刘华秋](../Page/刘华秋.md "wikilink")，兼任[中央外事办主任](../Page/中央外事办.md "wikilink")。

在任职期间，戴秉国一直扮演着类似“[国家安全顾问](../Page/国家安全顾问.md "wikilink")”的角色，代表兼任[中央外事工作领导小组组长的中共中央总书记](../Page/中央外事工作委员会.md "wikilink")[胡锦涛处理外交大事](../Page/胡锦涛.md "wikilink")，是[中美](../Page/中美战略对话.md "wikilink")、[中日](../Page/中日战略对话.md "wikilink")、[中俄](../Page/中俄战略对话.md "wikilink")、[中印](../Page/中印战略对话.md "wikilink")[战略对话的领军人物](../Page/战略对话.md "wikilink")\[1\]。

### 主管外交事务

2008年3月，在[第十一届全国人民代表大会第一次会议上](../Page/第十一届全国人民代表大会第一次会议.md "wikilink")，戴秉国被国务院总理[温家宝提名为](../Page/温家宝.md "wikilink")[国务委员](../Page/国务委员.md "wikilink")，并继续兼任[中央外事工作领导小组办公室主任和](../Page/中央外事工作委员会办公室.md "wikilink")[中央国家安全工作领导小组办公室主任的工作](../Page/中央国家安全委员会办公室.md "wikilink")。2013年3月[第十二届全国人大上届满退休](../Page/第十二届全国人大.md "wikilink")，同年11月出任[暨南大学董事长](../Page/暨南大学.md "wikilink")。

## 参考文献

{{-}}

[Category:外交學院校友](../Category/外交學院校友.md "wikilink")
[Category:四川大学校友](../Category/四川大学校友.md "wikilink")
[Category:中华人民共和国外交部副部长](../Category/中华人民共和国外交部副部长.md "wikilink")
[Category:中国共产党第十五届中央委员会委员](../Category/中国共产党第十五届中央委员会委员.md "wikilink")
[Category:中国共产党第十六届中央委员会委员](../Category/中国共产党第十六届中央委员会委员.md "wikilink")
[Category:中国共产党第十七届中央委员会委员](../Category/中国共产党第十七届中央委员会委员.md "wikilink")
[Category:印江人](../Category/印江人.md "wikilink")
[Category:土家族人](../Category/土家族人.md "wikilink")
[B秉](../Category/戴姓.md "wikilink")

1.  [戴秉国将成为中国外交掌门人](http://www.tycool.com/2007/08/13/00004.html)  美国中文在线