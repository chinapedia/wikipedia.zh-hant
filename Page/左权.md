**左权**（），原名**左纪传**\[1\]，字**孳麟**，号**叔仁**，[湖南](../Page/湖南.md "wikilink")[醴陵人](../Page/醴陵.md "wikilink")。[中国共产党军事将领](../Page/中国共产党.md "wikilink")，[中国人民解放军军事家之一](../Page/中国人民解放军军事家.md "wikilink")。

左权毕业于[黄埔军校第一期](../Page/黄埔军校.md "wikilink")，1925年加入[中国共产党](../Page/中国共产党.md "wikilink")，后到[莫斯科中山大学和](../Page/莫斯科中山大学.md "wikilink")[伏龙芝军事学院学习](../Page/伏龙芝军事学院.md "wikilink")。回国后，担任[红一军团参谋长](../Page/红一军团.md "wikilink")，参加[长征](../Page/长征.md "wikilink")。1936年，他担任红一军团代理军团长。[抗日战争爆发后](../Page/抗日战争.md "wikilink")，他担任[八路军副参谋长](../Page/八路军.md "wikilink")，同[彭德怀指挥了](../Page/彭德怀.md "wikilink")[百团大战](../Page/百团大战.md "wikilink")。1942年5月，日军对太行八路军发动扫荡，左权指挥部队掩护[中共中央北方局和八路军总部等机关突围转移时阵亡](../Page/中共中央北方局.md "wikilink")。

## 生平

[Zuo_Quan1925.jpg](https://zh.wikipedia.org/wiki/File:Zuo_Quan1925.jpg "fig:Zuo_Quan1925.jpg")

### 早年投身革命

1905年3月12日，左权出生于湖南醴陵[平侨乡](../Page/平侨乡.md "wikilink")[黄茅岭的一个贫民家庭](../Page/黄茅岭.md "wikilink")。左权8岁入私塾读书，后入小学，中间几次辍学。17岁考入县立中学。在县中读书时，与[蔡申熙](../Page/蔡申熙.md "wikilink")、[宋时轮等组织](../Page/宋时轮.md "wikilink")“社会问题研究社”。1923年12月，左权奔赴[广州](../Page/广州.md "wikilink")；次年3月，考入[广州陆军讲武学校](../Page/广州陆军讲武学校.md "wikilink")，同年11月转入[黄埔军校](../Page/黄埔军校.md "wikilink")，编为第1期第6队，并参加“中国青年军人联合会”\[2\]。

1925年1月，经[陈赓](../Page/陈赓.md "wikilink")、[周逸群介绍加入](../Page/周逸群.md "wikilink")[中国共产党](../Page/中国共产党.md "wikilink")。同年2月，左权任[党军教导团排](../Page/国民革命军.md "wikilink")、连长，参加国民革命军[第一次东征](../Page/第一次东征.md "wikilink")。6月回师广州后，左权又参加了平定[杨希闵](../Page/杨希闵.md "wikilink")、[刘震寰的作战](../Page/刘震寰.md "wikilink")。7月，在[程潜攻鄂军卫队任连长](../Page/程潜.md "wikilink")，参加国民革命军[第二次东征](../Page/第二次东征.md "wikilink")。同年，被中共党组织派往苏联留学，入[莫斯科中山大学学习](../Page/莫斯科中山大学.md "wikilink")。1927年9月，入[伏龙芝军事学院](../Page/伏龙芝军事学院.md "wikilink")，时逢[苏联肃反](../Page/大清洗.md "wikilink")，左权被批评“行为不检”，受到党内警告处分。

### 第一次国共内战时期

1930年6月，左权毕业回国到[上海](../Page/上海.md "wikilink")。9月，经[厦门](../Page/厦门.md "wikilink")、[龙岩进入闽西苏区](../Page/龙岩.md "wikilink")，任红军军官学校第1分校教育长。12月初，任新[红十二军軍長](../Page/红十二军.md "wikilink")。1931年初，任[红一方面军总司令部作战参谋](../Page/红一方面军.md "wikilink")，6月升为参谋处长。12月，受中央军委派遣前往宁都附近的固村圩，协同[王稼祥](../Page/王稼祥.md "wikilink")、[刘伯坚等筹划国民革命军第二十六路军倒戈事宜](../Page/刘伯坚.md "wikilink")；该路军[成功倒戈后](../Page/宁都起义.md "wikilink")，左权任[红五军团](../Page/红五军团.md "wikilink")[第十五军政委](../Page/中国工农红军第十五军.md "wikilink")\[3\]。

1932年3月，与军长[黄中岳率部参加](../Page/黄中岳.md "wikilink")[赣州战役](../Page/赣州战役.md "wikilink")，掩护主力红军撤退\[4\]。不久，红十五军编入东路军，左权奉命率部东进，参加[漳州战役](../Page/漳州战役.md "wikilink")。5月，担任红十五军军长兼政委；不久后，在肃反运动中，有人告发称左权收藏了“[托陈取消派](../Page/托陈取消派.md "wikilink")”的文件；左权因而被撤职，受到留党察看半年的处分，随后调至[红军学校任教官](../Page/红军学校.md "wikilink")。在担任教官期间，先后翻译了《苏联国内战争中之红军》《苏联红军中党的工作原则》等文章，以配合教学工作。

1933年1月，调任总参谋部第一局作战参谋，参加中央苏区第四次反围剿战争。5月，升任军委总司令部第一局副局长；同年9月，调任粤赣军区司令员。1933年12月，左权调任[红一军团参谋长](../Page/红一军团.md "wikilink")，参加[第五次反围剿战争](../Page/第五次反围剿战争.md "wikilink")。1934年8月，左权协助[林彪](../Page/林彪.md "wikilink")、[聂荣臻指挥](../Page/聂荣臻.md "wikilink")[温坊战斗](../Page/温坊战斗.md "wikilink")，采用运动战，毙伤国军2000余人，俘2400余人。

1934年10月，因为反围剿作战失败，左权跟随红军主力[长征](../Page/长征.md "wikilink")，参加指挥[湘江战役](../Page/湘江战役.md "wikilink")，掩护中央机关和后续部队顺利渡过湘江。1935年1月，参与指挥红一军团攻占[遵义](../Page/遵义.md "wikilink")。接着又参加指挥红一军团[四渡赤水](../Page/四渡赤水.md "wikilink")，抢占[娄山关等战斗](../Page/娄山关.md "wikilink")。5月，参与指挥强渡[大渡河的作战](../Page/大渡河.md "wikilink")。同年10月，率部到达陕西[吴起镇](../Page/吴起镇.md "wikilink")。同年11月，他率部参加[直罗镇战役](../Page/直罗镇战役.md "wikilink")。1936年2月，他与林彪、聂荣臻率红一军团渡过黄河，进攻[山西](../Page/山西.md "wikilink")[阎锡山等部](../Page/阎锡山.md "wikilink")，歼灭国军约7个团，俘4000余人\[5\]。同年5月，由于林彪调任[红军大学校长](../Page/红军大学.md "wikilink")，左权代理红一军团军团长，率领部队参加西征战役；11月，率部参加[山城堡战役](../Page/山城堡战役.md "wikilink")。1937年2月，任红军前敌总指挥部参谋长。

### 抗日战争时期

[1938eightleaders.jpg](https://zh.wikipedia.org/wiki/File:1938eightleaders.jpg "fig:1938eightleaders.jpg")[洪洞县](../Page/洪洞县.md "wikilink")[马牧村](../Page/马牧村.md "wikilink")[八路军总部合影](../Page/八路军.md "wikilink")，自左起分别为左权、[彭德怀](../Page/彭德怀.md "wikilink")、[朱德](../Page/朱德.md "wikilink")、[彭雪枫](../Page/彭雪枫.md "wikilink")、[萧克](../Page/萧克.md "wikilink")、[邓小平](../Page/邓小平.md "wikilink")\]\]
1937年7月，[七七事变爆发后](../Page/七七事变.md "wikilink")，[第二次国共合作开始](../Page/第二次国共合作.md "wikilink")。同年8月，[中国工农红军主力改编为](../Page/中国工农红军.md "wikilink")[八路军](../Page/八路军.md "wikilink")，左权任副参谋长，进入华北作战\[6\]。9月，他协助[朱德](../Page/朱德.md "wikilink")、[彭德怀部署](../Page/彭德怀.md "wikilink")[平型关战役和八路军配合](../Page/平型关战役.md "wikilink")[忻口战役等行动](../Page/忻口战役.md "wikilink")。1938年初，率八路军总部特务团二营在[安泽阻击日军进攻](../Page/安泽.md "wikilink")。其后，参与指挥东路军各部粉碎日军的[九路围攻](../Page/晋东南1938年战役.md "wikilink")，消灭日军4000余人，收复县城18座。同年12月，任八路军前方指挥部参谋长，加强司令部工作建设\[7\]。1939年7月，指挥八路军总部机关直属队强渡[漳河](../Page/漳河.md "wikilink")，粉碎日军的第二次九路围攻。随后，他选址在[山西省](../Page/山西省.md "wikilink")[黎城县西北部赤峪西端的](../Page/黎城县.md "wikilink")[黄崖洞建军工厂](../Page/黄崖洞.md "wikilink")，后成为八路军在抗战期间重要的兵工厂。

1940年2月，兼任八路军第二纵队司令员，率部进攻国军[朱怀冰部](../Page/朱怀冰.md "wikilink")。同年8月，与[彭德怀等八路軍領導人一同指挥针对日本军队的](../Page/彭德怀.md "wikilink")[百团大战](../Page/百团大战.md "wikilink")，突破日軍“囚籠政策”，取得巨大战果\[8\]。1941年11月，日军向[黄崖洞兵工厂进攻](../Page/黄崖洞兵工厂.md "wikilink")，左权直接指挥总部特务团在黄崖洞展开保卫战，八路军最终取得胜利\[9\]。1942年5月，日军向太行山区发动大扫荡，对八路军总部完成合围；5月25日，左权率部掩护总部突围，在[山西辽县的十字岭战斗中](../Page/山西.md "wikilink")，左权被炮弹击中而阵亡。

## 纪念与评价

[Zuoquan1942.jpg](https://zh.wikipedia.org/wiki/File:Zuoquan1942.jpg "fig:Zuoquan1942.jpg")
彭德懷在《左權同志碑誌》中寫道：“壯志未成，遺恨太行。露冷風淒，慟失全民優秀之指揮。”\[10\]他本人也成为[八路军在抗日战争中阵亡的最高级别指挥官](../Page/八路军.md "wikilink")。著有《論堅持華北抗戰》、《埋伏戰術》、《襲擊戰術》、《論軍事思想的原理》等軍事論著。[毛泽东在抗日战争时期中的](../Page/毛泽东.md "wikilink")《左权军事文选》里说：“左权他吃的洋面包都消化了，这个人硬是个‘两杆子’都硬的将才。”\[11\][周恩来称左权](../Page/周恩来.md "wikilink")“足以为党之模范”，[朱德赞誉他是](../Page/朱德.md "wikilink")“中国军事界不可多得的人才”。

左权去世后，刘志兰多次要求中共中央取消左权的留党察看处分，一直到1982年，中共中央终于写出书面文件，对左权予以平反、取消处分\[12\]。1942年9月8日，晋冀鲁豫边区政府将辽县改为[左权县](../Page/左权县.md "wikilink")。1950年，左权迁葬于[邯郸](../Page/邯郸.md "wikilink")[晋冀鲁豫烈士陵园](../Page/晋冀鲁豫烈士陵园.md "wikilink")。[中華民國國防部在](../Page/中華民國國防部.md "wikilink")2014年12月25日，公布以纪念[抗战胜利七十周年为主题的](../Page/中國抗日戰爭.md "wikilink")2015年“勇士国魂”[月历](../Page/月历.md "wikilink")。有别以往，这份月历将八路军左权列[国军抗战牺牲将领名单内](../Page/中華民國國軍.md "wikilink")。[中華民國國防部称](../Page/中華民國國防部.md "wikilink")，不必刻意抹煞共军在抗战中的表现，把八路军“唯一阵亡”\[13\]的将领左权，也将其列入牺牲将领名单内\[14\]。

## 家庭

1939年4月16日，经朱德介绍，左权与[北平来的女大学生](../Page/北平.md "wikilink")[刘志兰](../Page/刘志兰.md "wikilink")（1917-1992）结婚。左权与[刘志兰育有一女](../Page/刘志兰.md "wikilink")[左太北](../Page/左太北.md "wikilink")，1940年出生，曾任[中国航空工业总公司计划司副司长](../Page/中国航空工业总公司.md "wikilink")\[15\]。

## 参考文献

{{-}}

[Category:八路军将领](../Category/八路军将领.md "wikilink")
[Category:中華民國大陸時期軍事人物](../Category/中華民國大陸時期軍事人物.md "wikilink")
[Category:中華民國少將](../Category/中華民國少將.md "wikilink")
[Category:中华民国大陆时期中共烈士](../Category/中华民国大陆时期中共烈士.md "wikilink")
[Category:中国抗日战争牺牲者](../Category/中国抗日战争牺牲者.md "wikilink")
[Category:伏龙芝军事学院校友](../Category/伏龙芝军事学院校友.md "wikilink")
[Category:莫斯科中山大学校友](../Category/莫斯科中山大学校友.md "wikilink")
[Category:黄埔军校第一期](../Category/黄埔军校第一期.md "wikilink")
[Category:醴陵人](../Category/醴陵人.md "wikilink")
[Q权](../Category/左姓.md "wikilink")
[Category:中国工农红军将领](../Category/中国工农红军将领.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13. 以国民政府正式委任为准
14.
15.