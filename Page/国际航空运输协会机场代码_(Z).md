## Z

| IATA代碼                    | ICAO代碼 | 機場名稱                                           | 城市                                           | 國家和地區                                    |
| ------------------------- | ------ | ---------------------------------------------- | -------------------------------------------- | ---------------------------------------- |
|                           |        |                                                |                                              |                                          |
| <cite id="ZA"></cite>-ZA- |        |                                                |                                              |                                          |
| ZAA                       |        | Alice Arm Airport                              | Alice Arm                                    | [加拿大](../Page/加拿大.md "wikilink")         |
| ZAC                       | CZAC   | York Landing Airport                           | York Landing                                 | [加拿大](../Page/加拿大.md "wikilink")         |
| ZAD                       | LDZD   | Zadar Airport                                  | [扎達爾](../Page/扎達爾.md "wikilink")             | [克羅埃西亞](../Page/克羅埃西亞.md "wikilink")     |
| ZAG                       | LDZA   | [薩格勒布機場](../Page/薩格勒布機場.md "wikilink")         | [薩格勒布](../Page/薩格勒布.md "wikilink")           | [克羅埃西亞](../Page/克羅埃西亞.md "wikilink")     |
| ZAH                       | OIZH   | Zahedan Airport                                | [扎黑丹](../Page/扎黑丹.md "wikilink")             | [伊朗](../Page/伊朗.md "wikilink")           |
| ZAJ                       | OAZJ   | Zaranj Airport                                 | [扎蘭季](../Page/扎蘭季.md "wikilink")             | [阿富汗](../Page/阿富汗.md "wikilink")         |
| ZAL                       | SCVD   | Pichoy Airport                                 | [瓦爾迪維亞](../Page/瓦爾迪維亞.md "wikilink")         | [智利](../Page/智利.md "wikilink")           |
| ZAM                       | RPMZ   | Zamboanga International Airport                | [三寶顏](../Page/三寶顏.md "wikilink")             | [菲律賓](../Page/菲律賓.md "wikilink")         |
| ZAO                       | LFCC   | Laberandie Airport                             | [卡奧爾](../Page/卡奧爾.md "wikilink")             | [法國](../Page/法國.md "wikilink")           |
| ZAR                       | DNZA   | Zaria Airport                                  | [扎里亞](../Page/扎里亞.md "wikilink")             | [奈及利亞](../Page/奈及利亞.md "wikilink")       |
| ZAT                       | ZPZT   | [昭通機場](../Page/昭通機場.md "wikilink")             | [昭通市](../Page/昭通市.md "wikilink")             | [中國](../Page/中華人民共和國.md "wikilink")      |
| <cite id="ZB"></cite>-ZB- |        |                                                |                                              |                                          |
| ZBE                       | LKZA   | Dolní Benesov Airport                          | Zábreh                                       | [捷克](../Page/捷克.md "wikilink")           |
| ZBF                       | CZBF   | Bathurst Airport                               | Bathurst                                     | [加拿大](../Page/加拿大.md "wikilink")         |
| ZBK                       |        | Žabljak Airport                                | [扎布利亞克](../Page/扎布利亞克.md "wikilink")         | [蒙特內哥羅](../Page/蒙特內哥羅.md "wikilink")     |
| ZBM                       | CZBM   | Bromont Airport                                | Bromont                                      | [加拿大](../Page/加拿大.md "wikilink")         |
| ZBO                       | YBWN   | Bowen Airport                                  | Bowen                                        | [澳大利亚](../Page/澳大利亚.md "wikilink")       |
| ZBR                       | OIZC   | Konarak Airport                                | Chabahar                                     | [伊朗](../Page/伊朗.md "wikilink")           |
| ZBY                       | VLSB   | Sayaboury Airport                              | Sainyabuli                                   | [寮國](../Page/寮國.md "wikilink")           |
| <cite id="ZC"></cite>-ZC- |        |                                                |                                              |                                          |
| ZCL                       | MMZC   | General Leobardo C. Ruiz International Airport | [薩卡特卡斯](../Page/薩卡特卡斯.md "wikilink")         | [墨西哥](../Page/墨西哥.md "wikilink")         |
| ZCN                       | ETHC   | Celle Air Base                                 | [策勒](../Page/策勒_\(德國\).md "wikilink")        | [德國](../Page/德國.md "wikilink")           |
| ZCO                       | SCTC   | Maquehue Airport                               | [特木科](../Page/特木科.md "wikilink")             | [智利](../Page/智利.md "wikilink")           |
| <cite id="ZE"></cite>-ZE- |        |                                                |                                              |                                          |
| ZEC                       | FASC   | Secunda Airport                                | Secunda                                      | [南非](../Page/南非.md "wikilink")           |
| ZEG                       |        | Senggo Airport                                 | Senggo                                       | [印尼](../Page/印尼.md "wikilink")           |
| ZEL                       | CYJQ   | Denny Island Aerodrome                         | Bella Bella                                  | [加拿大](../Page/加拿大.md "wikilink")         |
| ZEM                       | CZEM   | Eastmain River Airport                         | Eastmain                                     | [加拿大](../Page/加拿大.md "wikilink")         |
| ZER                       | VEZO   | Zero Airport (Ziro Airport)                    | Ziro                                         | [印度](../Page/印度.md "wikilink")           |
| <cite id="ZF"></cite>-ZF- |        |                                                |                                              |                                          |
| ZFA                       | CZFA   | Faro Airport                                   | Faro                                         | [加拿大](../Page/加拿大.md "wikilink")         |
| ZFB                       |        | Old Fort Bay Airport                           | Bonne-Espérance                              | [加拿大](../Page/加拿大.md "wikilink")         |
| ZFD                       | CZFD   | Fond-du-Lac Airport                            | Fond-du-Lac                                  | [加拿大](../Page/加拿大.md "wikilink")         |
| ZFL                       |        | South Trout Lake Airport                       | South Trout Lake                             | [加拿大](../Page/加拿大.md "wikilink")         |
| ZFM                       | CZFM   | Fort McPherson Airport                         | Fort McPherson                               | [加拿大](../Page/加拿大.md "wikilink")         |
| ZFN                       | CZFN   | Tulita Airport                                 | Tulita                                       | [加拿大](../Page/加拿大.md "wikilink")         |
| <cite id="ZG"></cite>-ZG- |        |                                                |                                              |                                          |
| ZGC                       | ZLLL   | [蘭州中川機場](../Page/蘭州中川機場.md "wikilink")         | [蘭州市](../Page/蘭州市.md "wikilink")             | [中國](../Page/中華人民共和國.md "wikilink")      |
| ZGF                       | CZGF   | Grand Forks Airport                            | Grand Forks                                  | [加拿大](../Page/加拿大.md "wikilink")         |
| ZGI                       | CZGI   | Gods River Airport                             | Gods River                                   | [加拿大](../Page/加拿大.md "wikilink")         |
| ZGL                       | YSGW   | South Galway Airport                           | South Galway                                 | [澳大利亚](../Page/澳大利亚.md "wikilink")       |
| ZGM                       | FLNA   | Ngoma Airport                                  | Ngoma                                        | [尚比亞](../Page/尚比亞.md "wikilink")         |
| ZGR                       | CZGR   | Little Grand Rapids Airport                    | Little Grand Rapids                          | [加拿大](../Page/加拿大.md "wikilink")         |
| ZGS                       |        | Gethsemani Airport                             | Gethsemani                                   | [加拿大](../Page/加拿大.md "wikilink")         |
| <cite id="ZH"></cite>-ZH- |        |                                                |                                              |                                          |
| ZHA                       | ZGZJ   | [湛江機場](../Page/湛江機場.md "wikilink")             | [湛江市](../Page/湛江市.md "wikilink")             | [中國](../Page/中華人民共和國.md "wikilink")      |
| ZHM                       | VGSH   | Shamshernagar Airport                          | Shamshernagar                                | [孟加拉](../Page/孟加拉國.md "wikilink")        |
| ZHP                       | CZHP   | High Prairie Airport                           | High Prairie                                 | [加拿大](../Page/加拿大.md "wikilink")         |
| ZHY                       | ZLZW   | [中衛沙坡头機場](../Page/中衛沙坡头機場.md "wikilink")       | [中衛市](../Page/中衛市.md "wikilink")             | [中國](../Page/中華人民共和國.md "wikilink")      |
| <cite id="ZI"></cite>-ZI- |        |                                                |                                              |                                          |
| ZIC                       | SCTO   | Victoria Airport                               | Victoria                                     | [智利](../Page/智利.md "wikilink")           |
| ZIG                       | GOGG   | Ziguinchor Airport                             | [濟金紹爾](../Page/濟金紹爾.md "wikilink")           | [塞內加爾](../Page/塞內加爾.md "wikilink")       |
| ZIH                       | MMZH   | Ixtapa-Zihuatanejo International Airport       | [锡瓦塔内霍](../Page/锡瓦塔内霍.md "wikilink")         | [墨西哥](../Page/墨西哥.md "wikilink")         |
| <cite id="ZJ"></cite>-ZJ- |        |                                                |                                              |                                          |
| ZJG                       | CZJG   | Jenpeg Airport                                 | Jenpeg                                       | [加拿大](../Page/加拿大.md "wikilink")         |
| ZJN                       | CZJN   | Swan River Airport                             | [天鵝河](../Page/天鵝河.md "wikilink")             | [加拿大](../Page/加拿大.md "wikilink")         |
| <cite id="ZK"></cite>-ZK- |        |                                                |                                              |                                          |
| ZKB                       | FLKY   | Kasaba Bay Airport                             | Kasaba Bay                                   | [尚比亞](../Page/尚比亞.md "wikilink")         |
| ZKE                       | CZKE   | Kashechewan Airport                            | Kashechewan                                  | [加拿大](../Page/加拿大.md "wikilink")         |
| ZKG                       |        | Kegaska Airport                                | Kegaska                                      | [加拿大](../Page/加拿大.md "wikilink")         |
| ZKL                       | WASB   | Bintuni Airport                                | Bintuni                                      | [印尼](../Page/印尼.md "wikilink")           |
| ZKM                       | FOOS   | Sette Cama Airport                             | Akieni                                       | [加彭](../Page/加彭.md "wikilink")           |
| ZKP                       | FLKE   | Kasompe Airport                                | [欽戈拉](../Page/欽戈拉.md "wikilink")             | [尚比亞](../Page/尚比亞.md "wikilink")         |
| <cite id="ZL"></cite>-ZL- |        |                                                |                                              |                                          |
| ZLG                       |        | El Gouera Airport                              | El Gouera                                    | [茅利塔尼亞](../Page/茅利塔尼亞.md "wikilink")     |
| ZLO                       | MMZO   | Playa de Oro International Airport             | [曼薩尼約](../Page/曼薩尼約.md "wikilink")           | [墨西哥](../Page/墨西哥.md "wikilink")         |
| ZLT                       |        | La Tabatière Airport                           | La Tabatière                                 | [加拿大](../Page/加拿大.md "wikilink")         |
| <cite id="ZM"></cite>-ZM- |        |                                                |                                              |                                          |
| ZMD                       | SWSN   | Sena Madureira Airport                         | Sena Madureira                               | [巴西](../Page/巴西.md "wikilink")           |
| ZMH                       | CZML   | South Cariboo Regional Airport                 | 108 Mile Ranch                               | [加拿大](../Page/加拿大.md "wikilink")         |
| ZMM                       | MMZM   | Zamora Airport                                 | [薩莫拉-德伊達爾戈](../Page/薩莫拉-德伊達爾戈.md "wikilink") | [墨西哥](../Page/墨西哥.md "wikilink")         |
| ZMT                       | CZMT   | Masset Airport                                 | Masset                                       | [加拿大](../Page/加拿大.md "wikilink")         |
| <cite id="ZN"></cite>-ZN- |        |                                                |                                              |                                          |
| ZNA                       |        | Nanaimo Harbour Water Airport                  | [納奈莫](../Page/納奈莫.md "wikilink")             | [加拿大](../Page/加拿大.md "wikilink")         |
| ZNC                       |        | Nyac Airport                                   | Nyac                                         | [美國](../Page/美國.md "wikilink")           |
| ZND                       | DRZR   | Zinder Airport                                 | [津德爾](../Page/津德爾.md "wikilink")             | [尼日尔](../Page/尼日尔.md "wikilink")         |
| ZNE                       | YNWN   | Newman Airport                                 | Newman                                       | [澳大利亚](../Page/澳大利亚.md "wikilink")       |
| ZNG                       |        | Negginan Airport                               | Negginan                                     | [加拿大](../Page/加拿大.md "wikilink")         |
| ZNU                       |        | Namu Airport                                   | Namu                                         | [加拿大](../Page/加拿大.md "wikilink")         |
| ZNZ                       | HTZA   | Zanzibar Airport                               | [桑吉巴](../Page/桑吉巴.md "wikilink")             | [坦尚尼亞](../Page/坦尚尼亞.md "wikilink")       |
| <cite id="ZO"></cite>-ZO- |        |                                                |                                              |                                          |
| ZOF                       |        | Ocean Falls Airport                            | Ocean Falls                                  | [加拿大](../Page/加拿大.md "wikilink")         |
| ZOS                       | SCJO   | Cañal Bajo Carlos Hott Siebert Airport         | Osorno                                       | [智利](../Page/智利.md "wikilink")           |
| <cite id="ZP"></cite>-ZP- |        |                                                |                                              |                                          |
| ZPB                       | CZPB   | Sachigo Lake Airport                           | Sachigo Lake First Nation                    | [加拿大](../Page/加拿大.md "wikilink")         |
| ZPC                       | SCPC   | Pucón Airport                                  | [普孔](../Page/普孔.md "wikilink")               | [智利](../Page/智利.md "wikilink")           |
| ZPH                       | KZPH   | Zephyrhills Municipal Airport                  | Zephyrhills                                  | [美國](../Page/美國.md "wikilink")           |
| ZPO                       | CZPO   | Pinehouse Lake Airport                         | Pinehouse Lake                               | [加拿大](../Page/加拿大.md "wikilink")         |
| ZPQ                       | ETHE   | Rheine Air Base                                | Rheine                                       | [德國](../Page/德國.md "wikilink")           |
| <cite id="ZQ"></cite>-ZQ- |        |                                                |                                              |                                          |
| ZQN                       | NZQN   | Queenstown Airport                             | [皇后鎮](../Page/皇后鎮.md "wikilink")             | [紐西蘭](../Page/紐西蘭.md "wikilink")         |
| ZQS                       |        | Queen Charlotte Island Airport                 | Haida Gwaii                                  | [加拿大](../Page/加拿大.md "wikilink")         |
| ZQW                       |        | Zweibrücken Airport                            | [茨魏布呂肯](../Page/茨魏布呂肯.md "wikilink")         | [德國](../Page/德國.md "wikilink")           |
| ZQZ                       | ZBZJ   | [张家口宁远机场](../Page/张家口宁远机场.md "wikilink")       | [张家口](../Page/张家口.md "wikilink")             | [中国](../Page/中国.md "wikilink")           |
| <cite id="ZR"></cite>-ZR- |        |                                                |                                              |                                          |
| ZRH                       | LSZH   | [蘇黎世機場](../Page/蘇黎世機場.md "wikilink")           | [蘇黎世](../Page/蘇黎世.md "wikilink")             | [瑞士](../Page/瑞士.md "wikilink")           |
| ZRI                       | WABO   | Serui Airport                                  | Serui                                        | [印尼](../Page/印尼.md "wikilink")           |
| ZRJ                       | CZRJ   | Round Lake (Weagamow Lake) Airport             | Round Lake                                   | [加拿大](../Page/加拿大.md "wikilink")         |
| ZRM                       | WAJI   | Sarmi Airport                                  | Sarmi                                        | [印尼](../Page/印尼.md "wikilink")           |
| <cite id="ZS"></cite>-ZS- |        |                                                |                                              |                                          |
| ZSA                       | MYSM   | San Salvador Airport (Cockburn Town Airport)   | [聖薩爾瓦多島](../Page/聖薩爾瓦多島.md "wikilink")       | [巴哈馬](../Page/巴哈馬.md "wikilink")         |
| ZSE                       | FMEP   | Pierrefonds Airport                            | [聖皮埃爾](../Page/聖皮埃爾_\(留尼旺\).md "wikilink")   | [留尼旺](../Page/留尼旺.md "wikilink")         |
| ZSJ                       | CZSJ   | Sandy Lake Airport                             | Sandy Lake                                   | [加拿大](../Page/加拿大.md "wikilink")         |
| ZSP                       |        | St. Paul Aerodrome                             | St. Paul                                     | [加拿大](../Page/加拿大.md "wikilink")         |
| ZSS                       | DISS   | Sassandra Airport                              | [薩桑德拉](../Page/薩桑德拉.md "wikilink")           | [象牙海岸](../Page/象牙海岸.md "wikilink")       |
| ZST                       | CZST   | Stewart Aerodrome                              | Stewart                                      | [加拿大](../Page/加拿大.md "wikilink")         |
| ZSW                       | CZSW   | Prince Rupert/Seal Cove Water Airport          | [魯珀特王子港](../Page/魯珀特王子港.md "wikilink")       | [加拿大](../Page/加拿大.md "wikilink")         |
| <cite id="ZT"></cite>-ZT- |        |                                                |                                              |                                          |
| ZTA                       | NTGY   | Tureira Airport                                | Tureira                                      | [法屬玻里尼西亞](../Page/法屬玻里尼西亞.md "wikilink") |
| ZTB                       |        | Tête-à-la-Baleine Airport                      | Tête-à-la-Baleine                            | [加拿大](../Page/加拿大.md "wikilink")         |
| ZTH                       | LGZA   | Zakynthos International Airport                | [扎金索斯](../Page/扎金索斯.md "wikilink")           | [希臘](../Page/希臘.md "wikilink")           |
| ZTM                       | CZTM   | Shamattawa Airport                             | Shamattawa                                   | [加拿大](../Page/加拿大.md "wikilink")         |
| ZTR                       |        | Zhitomir Airport                               | [日托米爾](../Page/日托米爾.md "wikilink")           | [烏克蘭](../Page/烏克蘭.md "wikilink")         |
| ZTS                       |        | Tahsis Water Aerodrome                         | Tahsis Water                                 | [加拿大](../Page/加拿大.md "wikilink")         |
| <cite id="ZU"></cite>-ZU- |        |                                                |                                              |                                          |
| ZUC                       | CZUC   | Ignace Municipal Airport                       | Ignace Municipal                             | [加拿大](../Page/加拿大.md "wikilink")         |
| ZUD                       | SCAC   | Itauba Airport                                 | Ancud                                        | [智利](../Page/智利.md "wikilink")           |
| ZUE                       |        | Zuenoula Airport                               | Zuenoula                                     | [象牙海岸](../Page/象牙海岸.md "wikilink")       |
| ZUH                       | ZGSD   | [珠海金湾機場](../Page/珠海金湾機場.md "wikilink")         | [珠海市](../Page/珠海市.md "wikilink")             | [中國](../Page/中華人民共和國.md "wikilink")      |
| ZUL                       | OEZL   | Zilfi Airport                                  | Zilfi                                        | [沙烏地阿拉伯](../Page/沙烏地阿拉伯.md "wikilink")   |
| ZUM                       | CZUM   | Churchill Falls Airport                        | Churchill Falls                              | [加拿大](../Page/加拿大.md "wikilink")         |
| <cite id="ZV"></cite>-ZV- |        |                                                |                                              |                                          |
| ZVA                       | FMMN   | Miandrivazo Airport                            | [米安德里瓦祖](../Page/米安德里瓦祖.md "wikilink")       | [馬達加斯加](../Page/馬達加斯加.md "wikilink")     |
| ZVG                       |        | Springvale Airport                             | Springvale                                   | [澳大利亚](../Page/澳大利亚.md "wikilink")       |
| ZVK                       | VLSK   | Savannakhet Airport                            | [凱山豐威漢市](../Page/凱山豐威漢市.md "wikilink")       | [寮國](../Page/寮國.md "wikilink")           |
| <cite id="ZW"></cite>-ZW- |        |                                                |                                              |                                          |
| ZWA                       | FMND   | Andapa Airport                                 | [安達帕](../Page/安達帕.md "wikilink")             | [馬達加斯加](../Page/馬達加斯加.md "wikilink")     |
| ZWL                       | CZWL   | Wollaston Lake Airport                         | Wollaston Lake                               | [加拿大](../Page/加拿大.md "wikilink")         |
| ZWS                       |        | [斯圖加特中央車站](../Page/斯圖加特中央車站.md "wikilink")     | [斯圖加特](../Page/斯圖加特.md "wikilink")           | [德國](../Page/德國.md "wikilink")           |
| <cite id="ZY"></cite>-ZY- |        |                                                |                                              |                                          |
| ZYI                       | ZUZY   | [遵义新舟机场](../Page/遵义新舟机场.md "wikilink")         | [遵義市](../Page/遵義市.md "wikilink")             | [中國](../Page/中華人民共和國.md "wikilink")      |
| ZYL                       | VGSY   | Osmani International Airport                   | Sylhet                                       | [孟加拉](../Page/孟加拉國.md "wikilink")        |
| <cite id="ZZ"></cite>-ZZ- |        |                                                |                                              |                                          |
| ZZU                       | FWUU   | Mzuzu Airport                                  | [姆祖祖](../Page/姆祖祖.md "wikilink")             | [馬拉威](../Page/馬拉威.md "wikilink")         |
| ZZV                       | KZZV   | Zanesville Municipal Airport                   | [曾斯維爾](../Page/曾斯維爾_\(俄亥俄州\).md "wikilink")  | [美國](../Page/美國.md "wikilink")           |

[Category:航空代碼列表](../Category/航空代碼列表.md "wikilink")