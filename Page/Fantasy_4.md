**Fantasy
4**，通稱**女F4**，是一個臺灣[女子團體](../Page/女子團體.md "wikilink")，其成員包括[劉樂妍](../Page/劉樂妍.md "wikilink")（Fanny）、[安真佑](../Page/安真佑.md "wikilink")（Amy）、[謝孟恬](../Page/謝孟恬.md "wikilink")（Tiffany）以及[周均諺](../Page/周均諺.md "wikilink")（Stacy）。主持的節目有[亞洲旅遊台](../Page/亞洲旅遊台.md "wikilink")《F4遊台灣》，電影《[幫幫我愛神](../Page/幫幫我愛神.md "wikilink")》。及後改組為**[超女F4](../Page/超女F4.md "wikilink")**，在新成員加入後一周解散。

## 簡介

### 成立與解散

Fantasy
4與因為演出《[流星花園](../Page/流星花園_\(台灣電視劇\).md "wikilink")》一劇而成功的[男子團體](../Page/男子團體.md "wikilink")[F4沒有關係](../Page/F4_\(男子團體\).md "wikilink")。Fantasy
4在成立前有傳聞指出有四位胸圍擁有F罩杯的女藝人將打著「女F4」的名號進軍演藝圈，而最後在2005年6月27日當天，團長[劉樂妍在經紀人的陪同下開始拍攝宣傳照](../Page/劉樂妍.md "wikilink")。\[1\]值得注意的是當時Fantasy
4的成員只有劉樂妍、安真佑和謝孟恬三人，第四人尚未決定、結果Fantasy 4的最後一名成員由周均諺獲得，至此團體已變相解散。

### 超女F4成員

#### 周宥伶

  -
    英文名：Stacy
    生日：1984年6月26日（歲）
    學校：[國立台灣戲曲專科學校歌仔戲科](../Page/國立台灣戲曲專科學校.md "wikilink")
    身高：163公分
    體重：48公斤
    三圍：32F.25.36

#### 林有綝

  -
    英文名：Elva
    生日：1986年2月4日（歲）
    學校：[國立岡山高中](../Page/國立岡山高中.md "wikilink")
    身高：168公分
    體重：50 公斤
    三圍：34 C.24.38

#### 李梅瑄

  -
    英文名：CoCo
    生日：1982年6月19日（歲）
    學校：[台灣藝術學院肄業](../Page/台灣藝術學院.md "wikilink")
    身高：166公分
    體重：49公斤
    三圍：32B.24.35

#### 李霓

  -
    英文名：Maggie Qu (李霓)
    生日：1982年9月26日（歲）
    學校：[國立台灣藝術大學戲劇系畢業](../Page/國立台灣藝術大學.md "wikilink")
    身高：165公分
    體重：45公斤
    三圍：32C.23.33

## 批評

Fantasy
4因為團名和藉由物化女性來換取知名度行為的從成立前就一直受到批評。一手打造男子團體F4的製作人[柴智屏在得知有關Fantasy](../Page/柴智屏.md "wikilink")
4的傳言時表示驚訝，而當時的第一夫人[吳淑珍也對Fantasy](../Page/吳淑珍.md "wikilink")
4所造成的效應說「真是社會亂源」。 \[2\] Fantasy
4的表演能力在於綜藝節目《[快樂星期天](../Page/快樂星期天.md "wikilink")》演出時被評審[包小柏批評](../Page/包小柏.md "wikilink")：「妳們的表演，動作沒動作，戲劇沒戲劇，沒有看頭。」\[3\]

## 注釋

<div class="references-small" style="-moz-column-count:2; column-count:2;">

<references />

</div>

[Category:臺灣女子演唱團體](../Category/臺灣女子演唱團體.md "wikilink")
[Category:已解散的女子演唱團體](../Category/已解散的女子演唱團體.md "wikilink")

1.  [波霸F4來勢「胸胸」
    柴智屏感嘆沒創意只想搭順風車](http://www.ettoday.com/2005/06/28/34t0-1809740.hm)2005年6月27日
2.  [媒體愛播女F4...　吳淑珍：那真是社會亂源](http://www.ettoday.com/2005/09/13/340-1844067.htm)
    2005年09月13日
3.  [包小松包小柏再把女F4批哭](http://stars.zaobao.com/pages2/yutian051228.html)
    2005年12月28日