**13號星期五**（**Friday the 13th: The
Series**）是一部[美國的電視劇](../Page/美國.md "wikilink")，總共製作了三季，從1987年9月28日開播，至1990年5月26日結束。

敘述女主角蜜姬與表弟萊恩，從過世的叔叔那裡繼承了一間古董店，對於古董生意毫無概念的他們，隨即賣掉了店內不少古董，直到一位自稱是他們叔叔的朋友，傑克出現。傑克告知表姐弟倆關於店內古董被詛咒的事情，也因此揭開他們找尋這些古董的歷險……。

[台灣的](../Page/台灣.md "wikilink")[台灣電視公司曾於](../Page/台灣電視公司.md "wikilink")1990年3月13日至1991年8月14日之間\[1\]，每[週二晚間](../Page/週二.md "wikilink")11點30分至12點30分的深夜時段播出。

## 產品

台灣的[協和影視曾發行錄影帶](../Page/協和影視.md "wikilink")，但並非收錄完整的一季，而是其中的某幾集。其中包含萊恩離開古董店，新男主角強尼加入的第三季第一集。

美國於2008年九月發行第一季DVD，第二季DVD預計於2009年發行。

## 参考文献

<div class="references-small">

<references />

</div>

[Category:美國電視劇](../Category/美國電視劇.md "wikilink")
[Category:1987年電視劇集](../Category/1987年電視劇集.md "wikilink")
[Category:電影改編電視劇](../Category/電影改編電視劇.md "wikilink")
[Category:CBS電視工作室製作的電視節目](../Category/CBS電視工作室製作的電視節目.md "wikilink")
[Category:恐怖電視劇](../Category/恐怖電視劇.md "wikilink")
[Category:科幻電視劇](../Category/科幻電視劇.md "wikilink")
[Category:台視外購電視劇](../Category/台視外購電視劇.md "wikilink")
[Category:電視小作品](../Category/電視小作品.md "wikilink")

1.  [台灣電視資料庫](http://tv.nccu.edu.tw/radioProgram_list.htm?STATION=%A5x%B5%F8&PROGRAMNAME=%A4Q%A4T%B8%B9%ACP%B4%C1%A4%AD&CATALOG=&COUNTRY=)