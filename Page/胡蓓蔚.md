**胡蓓蔚**（**Paisley Hu Pui
Wei**，），[香港女](../Page/香港.md "wikilink")[歌手](../Page/歌手.md "wikilink")、[演員](../Page/演員.md "wikilink")、[主持人](../Page/主持人.md "wikilink")。現為[無綫電視](../Page/無綫電視.md "wikilink")（TVB）基本藝人合約女藝員。

## 簡歷

胡蓓蔚於[上海長大](../Page/上海.md "wikilink")，後來去[美國讀書](../Page/美國.md "wikilink")，畢業後回到香港。小時候在上海學[歌劇](../Page/歌劇.md "wikilink")。到了美國以後也學習了[R\&B和](../Page/R&B.md "wikilink")[Jazz](../Page/Jazz.md "wikilink")。\[1\]

在[劉以達的帶領下](../Page/劉以達.md "wikilink")，參加了一個在上海舉辦的歌唱比賽從而進入樂壇。\[2\]1990年代加入樂壇，初出道時有「翻版王菲」之稱。她在1994年推出過爵士專輯，1996年加盟[非池中唱片](../Page/非池中唱片.md "wikilink")\[3\]，之後參與過劉以達，[林海峰和](../Page/林海峰.md "wikilink")[陳冠希的專輯](../Page/陳冠希.md "wikilink")。

2007年，胡蓓蔚再次復出推出新個人EP《Don't Think Just
Do》，亦擔任TVB音樂節目《[勁歌金曲](../Page/勁歌金曲.md "wikilink")》主持。

與[胡杏兒](../Page/胡杏兒.md "wikilink")、[胡定欣](../Page/胡定欣.md "wikilink")、[姚子羚](../Page/姚子羚.md "wikilink")、[黃智雯](../Page/黃智雯.md "wikilink")、[李施嬅成立](../Page/李施嬅.md "wikilink")「胡說八道會」\[4\]。

## 感情生活

胡蓓蔚與音樂人及演員[單立文於](../Page/單立文.md "wikilink")1996年年底開始交往。2008年3月，二人結婚\[5\]\[6\]。

## 音樂作品

### 專輯列表

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>專輯 #</p></th>
<th style="text-align: left;"><p>專輯名稱</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>1st</p></td>
<td style="text-align: left;"><p><a href="../Page/一生守候_(胡蓓蔚專輯).md" title="wikilink">一生守候</a>（國語）</p></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"><ol>
<li>愛上一個不回家的人</li>
<li>我和我的愛情說再見</li>
<li>滾滾紅塵</li>
<li>情關</li>
<li>一生守候</li>
<li>我不是對你演戲</li>
<li>你熟悉的世界多了一個我</li>
<li>哭砂</li>
<li>讓我看看愛情的樣子</li>
<li>愛短短的就好</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>2nd</p></td>
<td style="text-align: left;"><p><a href="../Page/今天明天後天.md" title="wikilink">今天明天後天</a></p></td>
<td style="text-align: left;"><p>1994年</p></td>
<td style="text-align: left;"><p><strong>Side A</strong></p>
<ol>
<li>不要問我</li>
<li>不要說愛我</li>
<li>Put The Blame On 上海</li>
<li>歇斯底裡（過場音樂）</li>
<li>未来的光明</li>
<li>在一樣的天空裡</li>
<li>La Vie En Rose（過場音樂）</li>
<li>這個下午</li>
</ol>
<p><strong>Side B</strong></p>
<ol>
<li>一丁點兒</li>
<li>Look Me Over Closely（過場音樂）</li>
<li>我在印度等着你回來</li>
<li>今天明天後天大後天</li>
<li>這是我最後的一天</li>
<li>夜沒有夜（過場音樂）</li>
<li>不要唱吧</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>3rd</p></td>
<td style="text-align: left;"><p><a href="../Page/就係胡蓓蔚.md" title="wikilink">就係胡蓓蔚</a></p></td>
<td style="text-align: left;"><p>1997年</p></td>
<td style="text-align: left;"><ol>
<li>滿天飛</li>
<li>多多</li>
<li>假使今晚睡了</li>
<li>為什麼(國語)</li>
<li>鐘擺</li>
<li>壞習慣</li>
<li>Near Me</li>
<li>自說自話</li>
<li>抹掉</li>
<li>全因你</li>
<li>滿天飛</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>4th</p></td>
<td style="text-align: left;"><p><a href="../Page/停格愛情.md" title="wikilink">停格愛情</a>（國語）</p></td>
<td style="text-align: left;"><p>2002年</p></td>
<td style="text-align: left;"><ol>
<li>停格愛情</li>
<li>紀念旅行</li>
<li>愛夢想</li>
<li>他是誰</li>
<li>不明飛行物</li>
<li>兩難</li>
<li>愛過的人去了哪裡</li>
<li>會員專利(粵語)</li>
<li>不必剎掣(粵語)</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>5th</p></td>
<td style="text-align: left;"><p><a href="../Page/Don&#39;t_Think_Just_Do.md" title="wikilink">Don't Think Just Do</a>（EP）</p></td>
<td style="text-align: left;"><p>2007年</p></td>
<td style="text-align: left;"><ol>
<li>Don't Think Just Do</li>
<li>算了吧</li>
<li>Letting Go</li>
<li>慾望的規律</li>
<li>Love Is</li>
<li>算了吧 (remix)</li>
<li>Don'tThink Just Do (國語)</li>
</ol></td>
</tr>
</tbody>
</table>

### 派台歌曲成績

| **派台歌曲成績**                               |
| ---------------------------------------- |
| 唱片                                       |
| **1996年**                                |
| [麻木](../Page/麻木_\(劉以達專輯\).md "wikilink") |
| [就係胡蓓蔚](../Page/就係胡蓓蔚.md "wikilink")     |
| 就係胡蓓蔚                                    |
| **1997年**                                |
| 就係胡蓓蔚                                    |
| 就係胡蓓蔚                                    |
| 神偷諜影電影原聲大碟                               |
| 就係胡蓓蔚                                    |
| [的士夠格](../Page/的士夠格.md "wikilink")       |
| **2004年**                                |
| Please Steal This Album                  |
| **2007年**                                |
| Don't Think Just Do                      |
| Don't Think Just Do                      |
| **2008年**                                |
| Don't Think Just Do                      |

| **各台冠軍歌總數** |
| ----------- |
| 903         |
| **0**       |

## 演出作品

### 電視劇（[無綫電視](../Page/無綫電視.md "wikilink")）

|                                                        |                                                    |                 |        |
| ------------------------------------------------------ | -------------------------------------------------- | --------------- | ------ |
| **首播**                                                 | **劇名**                                             | **角色**          | **性質** |
| 2010年                                                  | [天天天晴](../Page/天天天晴.md "wikilink")                 | 汪小姐             | 客串     |
| [情人眼裏高一D](../Page/情人眼裏高一D.md "wikilink")               | 牛美君                                                |                 |        |
| 2012年                                                  | [On Call 36小時](../Page/On_Call_36小時.md "wikilink") | 屠家敏             | 女配角    |
| [怒火街頭2](../Page/怒火街頭2.md "wikilink")                   | Nancy                                              | 單元女主角           |        |
| 2013年                                                  | [衝上雲霄II](../Page/衝上雲霄II.md "wikilink")             | Hana            | 客串     |
| [On Call 36小時II](../Page/On_Call_36小時II.md "wikilink") | 屠家敏                                                | 女配角             |        |
| 2016年                                                  | [愛情食物鏈](../Page/愛情食物鏈.md "wikilink")               | 莎蓮娜             |        |
| 2017年                                                  | [誇世代](../Page/誇世代.md "wikilink")                   | 尚可清（Jacqueline） |        |

### 節目主持（[無綫電視](../Page/無綫電視.md "wikilink")）

  - [TVB8金曲榜](../Page/TVB8.md "wikilink")
  - [TVB8翡翠音樂幹線](../Page/TVB8.md "wikilink")
  - [TVB8火爆音樂空間](../Page/TVB8.md "wikilink")
  - [TVB8超級樂壇全接觸](../Page/TVB8.md "wikilink")
  - TVB金曲榜頒獎典禮
  - 2002年：[SMS話點就點](../Page/SMS話點就點.md "wikilink")
  - 2004年至今：[無間音樂](../Page/無間音樂.md "wikilink")
  - 2007-2009年：[勁歌金曲](../Page/勁歌金曲.md "wikilink")
  - 2012年至今：TVB8 [娛樂最前線](../Page/娛樂最前線.md "wikilink")
  - 2016年：[為食3姊妹](../Page/為食3姊妹.md "wikilink")
  - 2017年：[1+1的幸福](../Page/1+1的幸福.md "wikilink")
  - 2017年：[胡說八道真情假期](../Page/胡說八道真情假期.md "wikilink")
  - 2018年：[2018勁歌金曲優秀選第二回](../Page/2018勁歌金曲優秀選第二回.md "wikilink")

### 綜藝節目（無綫電視）

  - 2008年：[鐵甲無敵獎門人](../Page/鐵甲無敵獎門人.md "wikilink")
  - 2010年：[金曲擂台](../Page/金曲擂台.md "wikilink")
  - 2010年：[超級遊戲獎門人](../Page/超級遊戲獎門人.md "wikilink")
  - 2011年：[變身男女Chok Chok Chok](../Page/變身男女Chok_Chok_Chok.md "wikilink")
  - 2011年：[姊妹淘 (電視節目)](../Page/姊妹淘_\(電視節目\).md "wikilink")
  - 2011年：[千奇百趣省港澳](../Page/千奇百趣省港澳.md "wikilink")
  - 2012年：[五覺大戰](../Page/五覺大戰.md "wikilink")
  - 2012年：[千奇百趣東南亞](../Page/千奇百趣東南亞.md "wikilink")
  - 2012年：[登登登對](../Page/登登登對.md "wikilink")
  - 2013年：[超級無敵獎門人 終極篇](../Page/超級無敵獎門人_終極篇.md "wikilink")
  - 2015年：[今晚睇李](../Page/今晚睇李.md "wikilink")（第7集嘉賓）
  - 2016年：[2015年度TVB8金曲榜頒獎典禮](../Page/2015年度TVB8金曲榜頒獎典禮得獎名單.md "wikilink")
  - 2016年：[Do姐有問題](../Page/Do姐有問題.md "wikilink")
  - 2016年：[男人食堂](../Page/男人食堂.md "wikilink")
  - 2016年：[Sunday好戲王](../Page/Sunday好戲王.md "wikilink")
  - 2017年：[流行經典50強](../Page/流行經典50強.md "wikilink")
  - 2017年：[千奇百趣特工隊](../Page/千奇百趣特工隊.md "wikilink")
  - 2017年：[胡說八道真情假期](../Page/胡說八道真情假期.md "wikilink")
  - 2018年：[美女廚房](../Page/美女廚房_\(第三輯\).md "wikilink")
  - 2019年：[娛樂大家](../Page/娛樂大家_\(無綫電視節目\).md "wikilink")

## 參考

## 外部連結

  - [pinkwork訪問聲音及video](http://www.pinkwork.com/paisley/hu1.htm)

  - [胡蓓蔚相片及video](http://www.pinkmessage.com/talk/viewforum.php?f=23)

  -
[pui](../Category/胡姓.md "wikilink")
[Category:香港女歌手](../Category/香港女歌手.md "wikilink")
[Category:無綫電視女藝員](../Category/無綫電視女藝員.md "wikilink")
[Category:香港電視演員](../Category/香港電視演員.md "wikilink")
[Category:香港主持人](../Category/香港主持人.md "wikilink")
[Category:上海人](../Category/上海人.md "wikilink")

1.
2.
3.
4.  [「胡說八道會」澳洲Trip正式出發](http://hk.on.cc/hk/bkn/cnt/entertainment/20170730/bkn-20170730104321202-0730_00862_001.html)
5.
6.