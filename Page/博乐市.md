**博乐市**（；[托忒](../Page/托忒字母.md "wikilink")；，[拉丁维文](../Page/拉丁维文.md "wikilink")：Börtala）是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[新疆维吾尔自治区](../Page/新疆维吾尔自治区.md "wikilink")[博尔塔拉蒙古自治州的](../Page/博尔塔拉蒙古自治州.md "wikilink")[首府](../Page/行政中心.md "wikilink")，為一[縣級市](../Page/縣級市.md "wikilink")。也是[新疆生产建设兵团农五师师部](../Page/新疆生产建设兵团农五师.md "wikilink")、博尔塔拉军分区机关所在地。

博乐市的[气候为](../Page/气候.md "wikilink")[温带干旱气候](../Page/温带干旱气候.md "wikilink")，年均[气温](../Page/气温.md "wikilink")5.6℃，年均[降水](../Page/降水.md "wikilink")181mm。

## 行政区划

下辖4个[街道办事处](../Page/街道办事处.md "wikilink")、4个[镇](../Page/行政建制镇.md "wikilink")、1个[乡](../Page/乡级行政区.md "wikilink")：

。

## 参考资料

## 外部链接

  - [新疆博乐政务网](http://www.xjbl.gov.cn/)

[博乐市](../Category/博乐市.md "wikilink")
[市](../Category/博尔塔拉县市.md "wikilink")
[博尔塔拉](../Category/新疆县级市.md "wikilink")