**BMC Software,
Inc.**，是一家美国[企业管理软件提供商](../Page/企业战略管理.md "wikilink")，致力于IT基础设施应用软件。BMC创建于1980年，总部在[得克萨斯的](../Page/得克萨斯.md "wikilink")[休斯顿](../Page/休斯顿.md "wikilink")。

BMC自称为业务服务管理（[BSM](../Page/BSM.md "wikilink")）概念的先导者，企业通过BSM提供软件解决方案，从而可以使企业从业务视角管理他们的IT基础设施。\[1\]

BMC的组织分为两个主要的业务部门，主机服务管理（Mainframe Service Management
MSM）和企业服务管理（Enterprise Service Management ESM）.
MSM部门包括现在的主机管理部门和他的企业工作计划与产出管理产品线。ESM部门包括包括他的服务管理和[身份管理业务](../Page/身份管理.md "wikilink")，分布式系统管理，和BMC性能管理产品线，还有2006年5月因合并[Identify
Software,
Ltd.而出现的事务管理产品线](../Page/Identify_Software,_Ltd..md "wikilink")\[2\]

## BMC概览

### 公司使命

  - 通过帮助客户把他们的IT基础设施和业务有机结合来实现使BMC成为企业管理软件解决方案供应商的领导者。\[3\]

### 公司背景

  - 1980年9月成立。

<!-- end list -->

  - 1988年开始提供公共服务。

<!-- end list -->

  - 公司总部在得克萨斯州，休斯顿。

<!-- end list -->

  - 国际总部在荷兰的阿姆斯特丹和新加坡。

<!-- end list -->

  - 在阿根廷、澳大利亚、奥地利、比利时、巴西、加拿大、中国、丹麦、英国、芬兰、法国、德国、香港、匈牙利、印度、印度尼西亚、爱尔兰、以色列、意大利、日本、韩国、马来西亚、墨西哥、荷兰、新西兰、挪威、菲律宾、葡萄牙、苏格兰、新加坡、南非、西班牙、瑞典、瑞士、台湾、泰国、土耳其和阿拉伯联合酋长国运作着国际办公室。

<!-- end list -->

  - 市场覆盖世界上50多个国家。
  - 研究和开发办公室分布在得克萨斯的休斯顿和奥斯汀；乔治亚州的圣。琼斯；乔治亚州的亚特兰大；马萨诸塞州的沃尔瑟姆；弗吉尼亚的亨顿；法国的Aix-en-Provence；新加坡；特拉维夫和Tel
    Hai，以色列和印度的Pune。

<!-- end list -->

  - 在世界上共有6000多员工。

<!-- end list -->

  - 标准普尔500指数（S\&P 500）成员。

<!-- end list -->

  - 产品销售方式有直销和间接渠道包括分销商，系统集成商，原始设备制造商。

### 公司财务信息

  - 2006年3月财政年末的关键财务数字。
      - 销售额：$1,498.4M
      - 年增长率：2.4%

<!-- end list -->

  - 净收益：$102.0M
      - 收益增长率：35.5%

### 公司现状

  - 拥有72项软件技术专利。

<!-- end list -->

  - 号称是业务服务管理概念的先锋。

<!-- end list -->

  - 第一个通过PATROL提供应用管理解决方案

<!-- end list -->

  - 第一个给为IBM的IMS和[DB2数据库提供工具的独立软件供应商](../Page/IBM_DB2.md "wikilink")（ISV）

<!-- end list -->

  - 第一个为B2B交易和扩展的企业提供广泛的管理解决方案的软件供应商

<!-- end list -->

  - 创造了数据流优化

## 简史

1970年代末这段时间，BMC Software的创始人 Scott Boulett, [John
Moores](../Page/John_Moores.md "wikilink")，和 Dan Cloer
建立了一个合伙的编程伙伴，运作在得克萨斯的休斯顿附近。直到1980年，这个公司才正式依照法律成立，并正式命名为
BMC Software, Inc.
Moores是公司的第一任CEO，用他在壳牌石油做程序员的经验来改善为IBM主机提供的软件--当时的工业标准。
1988年7月，BMC在[NASDAQ首次公开募股](../Page/NASDAQ.md "wikilink")（[IPO](../Page/IPO.md "wikilink")）.

Moores在1987年从执行董事长和CEO的位置上卸任，由Max Watson继任。Moores
1992年从BMC主席的位置上退休下来，那时候Watson也被认为会是主席的位置。

在Watson's担任CEO和Chairman期间，BMC从500人发展到7000人。同时，销售业绩从1987年的小于100，000，000$增长到1，700，000，000，000$.

Watson的继任者是BMC前负责产品管理和开发的高级副董事长Robert Beauchamp.
在他任BMC的董事长和CEO期间，预见了很多重要的商业变革，例如把BMC的股票转移到纽约证券交易所[New
York Stock
Exchange](../Page/New_York_Stock_Exchange.md "wikilink")（[NYSE](../Page/NYSE.md "wikilink")），把BMC重组成11个业务单元；
精简销售和市场过程的策略，还有提出了业务服务管理（Business Service
Management）的概念；从业务的角度来管理IT的方法。\[4\]

## 聚焦

在1980年代的大部分时间里，BMC的焦点都集中在用于改进现有主机结构的产品。在1980年代的晚期和1990年代的早期，BMC致力于一系列的战略并购，为了在竞争激烈的网络软件市场上站稳脚跟。到1996年BMC已经成为在主机软件和网络计算软件业界的领导者。

## 产品

为了提供BSM, BMC 将其产品解决方案集中于4个不同的类别。\[5\]

  - **服务影响管理**

服务影响管理扩展了高级事件管理并提高了IT服务发布的有效性。产品被设计用于帮助业务实现“IT组织以业务优先级作为他们努力的方向”。
通过把IT基础设施与它所支持的关键业务服务关联起来，SIM解决方案改进了IT组织解决问题并以业务内容来划分优先级的能力。

  - **IT服务管理**

这一类软件提供，改进，并最大化驱动业务的IT服务的交付，包括[操作请求系统和Remedy](../Page/操作请求系统.md "wikilink")
Help Desk服务支持解决方案（原来是由[Remedy
Corporation开发的](../Page/Remedy_Corporation.md "wikilink")）和BMC的服务交付解决方案如BMC事件管理器和企业性能保证。

  - **应用管理**

BMC应用管理解决方案把用户放到一个应用的环境里，而不用去考虑平台和应用。应用管理产品帮助控制运作费用，减少应用复杂性，改进可靠性，从业务服务的视野去管理IT。

  - **IT运作，数据库和基础设施管理**

这一部分的产品为一个与第三方的企业管理技术交互的成功的业务和开放解决方案保证一个坚实的平台。
产品包括BMC数据库管理，安全管理和基础设施管理解决方案。

## 合并

  - RealOps, Inc.－2009年7月
  - ProactiveNet, Inc.－2009年6月
  - [Identify Software
    Ltd.](../Page/Identify_Software_Ltd..md "wikilink")－2009年5月
  - [KMXperts](../Page/KMXperts.md "wikilink")－2009年8月
  - [OpenNetwork](../Page/OpenNetwork.md "wikilink")－2009年3月
  - [Calendra](../Page/Calendra.md "wikilink")－2009年1月
  - Viadyne－2009年7月
  - Marimba－2009年7月
  - [Magic Solutions](../Page/Magic_Solutions.md "wikilink")－2009年2月
  - [ASA Knowledge](../Page/ASA_Knowledge.md "wikilink")－2009年1月
  - [IT Masters](../Page/IT_Masters.md "wikilink")－2009年3月
  - [Action Request
    System](../Page/Action_Request_System.md "wikilink")，assets from
    Peregrine Systems－2009年11月
  - Simulus Limited－2009年4月
  - AgentSpring－2009年3月
  - [Perform, SA](../Page/Perform,_SA.md "wikilink")－2009年2月
  - [Sylvain Faust,
    Inc.](../Page/Sylvain_Faust,_Inc..md "wikilink")－2009年10月
  - [OptiSystems
    Solutions](../Page/OptiSystems_Solutions.md "wikilink")－2009年8月
  - [Evity Inc.](../Page/Evity_Inc..md "wikilink")－2009年4月
  - OTL Software－1999年11月
  - [New Dimension
    Software](../Page/New_Dimension_Software.md "wikilink")－1999年4月
  - [Boole & Babbage,
    Inc.](../Page/Boole_&_Babbage,_Inc..md "wikilink")－1999年3月
  - [BGS Systems](../Page/BGS_Systems.md "wikilink")－1998年3月
  - [DataTools](../Page/DataTools.md "wikilink")－1997年5月
  - HawkNet, Inc.－1996年1月
  - Peer Networks, Inc.－1995年11月
  - [PATROL
    Software](../Page/PATROL_Software.md "wikilink")－1994年1月\[6\]

## 参考

  - [CONTROL-M](../Page/CONTROL-M.md "wikilink")
  - [Remedy Corporation](../Page/Remedy_Corporation.md "wikilink")

## 参考资料

<references/>

## 外部链接

  - [BMC](http://www.bmc.com)

[Category:美國軟體公司](../Category/美國軟體公司.md "wikilink")
[Category:1980年成立的公司](../Category/1980年成立的公司.md "wikilink")
[Category:納斯達克已除牌公司](../Category/納斯達克已除牌公司.md "wikilink")

1.
2.
3.
4.
5.
6.