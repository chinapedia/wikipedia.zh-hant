**马摩拉铁路**（又稱**馬摩拉計畫**；）是一项链接[伊斯坦堡亚洲和欧洲部分的](../Page/伊斯坦堡.md "wikilink")[鐵路](../Page/鐵路.md "wikilink")[海底隧道計畫](../Page/海底隧道.md "wikilink")，採取[沉管式興建](../Page/沉管式隧道.md "wikilink")，由[日本與](../Page/日本.md "wikilink")[土耳其的財團共同建設](../Page/土耳其.md "wikilink")（以[大成建設為首](../Page/大成建設.md "wikilink")）。该计划在2004年5月9日動工，原計2009年完工\[1\]，之後由于挖到一些[拜占庭帝国的古蹟而延誤](../Page/拜占庭帝国.md "wikilink")，2011年2月26日完成貫通，2013年10月29日完工通車。\[2\]\[3\]

## 资金

[日本国际协力银行和](../Page/日本国际协力银行.md "wikilink")[欧洲投资银行资助了这一计划](../Page/欧洲投资银行.md "wikilink")。

## 參考資料

## 外部連結

  - [马尔马拉铁路工程官方網站](http://www.marmaray.com.tr/)

[Category:2013年建立](../Category/2013年建立.md "wikilink")
[Category:伊斯坦堡交通](../Category/伊斯坦堡交通.md "wikilink")
[Category:海底隧道](../Category/海底隧道.md "wikilink")
[Category:土耳其鐵路](../Category/土耳其鐵路.md "wikilink")
[Category:2013年啟用的鐵路線](../Category/2013年啟用的鐵路線.md "wikilink")

1.  [Rails under the
    Bosporus](http://www.railwaygazette.com/news/single-view/view//rails-under-the-bosporus.html)
    , [Railway Gazette
    International](../Page/Railway_Gazette_International.md "wikilink")
    2009-02-23
2.
3.