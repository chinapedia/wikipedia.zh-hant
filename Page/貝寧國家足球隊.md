**貝寧國家足球隊**是[貝寧男子](../Page/貝寧.md "wikilink")[足球國家代表隊](../Page/足球.md "wikilink")，由[貝寧足球協會負責管轄](../Page/貝寧足球協會.md "wikilink")。1975年前貝寧前身為*達荷美國家足球隊*，貝寧從未打入[世界盃足球賽決賽周](../Page/世界盃足球賽.md "wikilink")，只是曾經於2004年參加[非洲國家盃](../Page/2004年非洲國家盃.md "wikilink")。

## 世界盃成績

  - [1930年](../Page/1930年世界杯足球赛.md "wikilink") -
    [1970年](../Page/1970年世界杯足球赛.md "wikilink") - *沒有參賽*
  - [1974年](../Page/1974年世界杯足球赛.md "wikilink") - *外圍賽出局*
  - [1978年](../Page/1978年世界杯足球赛.md "wikilink") -
    [1982年](../Page/1982年世界杯足球赛.md "wikilink") - *沒有參賽*
  - [1986年](../Page/1986年世界杯足球赛.md "wikilink") - *外圍賽出局*
  - [1990年](../Page/1990年世界杯足球赛.md "wikilink") - *沒有參賽*
  - [1994年](../Page/1994年世界杯足球赛.md "wikilink") - *外圍賽出局*
  - [1998年](../Page/1998年世界杯足球赛.md "wikilink") - *沒有參賽*
  - [2002年](../Page/2002年世界杯足球赛.md "wikilink") -
    [2014年](../Page/2014年世界杯足球赛.md "wikilink") - *外圍賽出局*

## 非洲國家盃成績

  - [1957年](../Page/1957年非洲國家盃.md "wikilink") 至
    [1970年](../Page/1970年非洲國家盃.md "wikilink") - *沒有參賽*
  - [1972年](../Page/1972年非洲國家盃.md "wikilink") - *外圍賽出局*
  - [1974年](../Page/1974年非洲國家盃.md "wikilink") -
    [1976年](../Page/1976年非洲國家盃.md "wikilink") - *退出*
  - [1978年](../Page/1978年非洲國家盃.md "wikilink") - *沒有參賽*
  - [1980年](../Page/1980年非洲國家盃.md "wikilink") - *外圍賽出局*
  - [1982年](../Page/1982年非洲國家盃.md "wikilink") - *沒有參賽*
  - [1984年](../Page/1984年非洲國家盃.md "wikilink") 至
    [1986年](../Page/1986年非洲國家盃.md "wikilink") - *外圍賽出局*
  - [1988年](../Page/1988年非洲國家盃.md "wikilink") 至
    [1990年](../Page/1990年非洲國家盃.md "wikilink") - *沒有參賽*
  - [1992年](../Page/1992年非洲國家盃.md "wikilink") 至
    [1994年](../Page/1994年非洲國家盃.md "wikilink") - *外圍賽出局*
  - [1996年](../Page/1996年非洲國家盃.md "wikilink") - *退出*
  - [1998年](../Page/1998年非洲國家盃.md "wikilink") 至
    [2002年](../Page/2002年非洲國家盃.md "wikilink")- *外圍賽出局*
  - [2004年](../Page/2004年非洲國家盃.md "wikilink") - 第一圈
  - [2006年](../Page/2006年非洲國家盃.md "wikilink") - *外圍賽出局*
  - [2008年](../Page/2008年非洲國家盃.md "wikilink") - 第一圈
  - [2010年](../Page/2010年非洲國家盃.md "wikilink") - 第一圈
  - [2012年](../Page/2012年非洲國家盃.md "wikilink") 至
    [2013年](../Page/2013年非洲國家盃.md "wikilink") - *外圍賽出局*

## 近賽成績

### [2014年世界盃](../Page/2014年世界盃足球賽.md "wikilink")

  - [外圍賽第二輪H組](../Page/2014年世界盃外圍賽非洲區第二輪#H組.md "wikilink")

## 著名球員

  - Romuald Boco - 現時貝寧國家足球隊主力
  - Laurent D'Jaffo - 曾效力多間英格蘭及蘇格蘭球會的貝寧球員
  - Moussa Latoundji - 曾效力德甲球會[科特布斯](../Page/科特布斯足球會.md "wikilink")

[Category:非洲足球代表隊](../Category/非洲足球代表隊.md "wikilink")
[Category:貝寧體育](../Category/貝寧體育.md "wikilink")