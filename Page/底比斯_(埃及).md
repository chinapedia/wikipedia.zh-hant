**底比斯**是[上埃及古城](../Page/上埃及.md "wikilink")，瀕臨[尼羅河](../Page/尼羅河.md "wikilink")，位於今埃及中部，即今天的[卢克索附近](../Page/卢克索.md "wikilink")。作為皇室居地和宗教膜拜的宗教中心，它從公元前22世紀中期到公元前18世紀曾繁榮一時。它的建築遺迹包括許多輝煌的廟宇和[帝王谷附近的法老陵墓](../Page/帝王谷.md "wikilink")。

## 興起

在[古王國時期](../Page/古王國時期.md "wikilink")，底比斯只是一個寂寂無名的小型商道中心。它作為通往[西奈半島和](../Page/西奈半島.md "wikilink")[彭特的水路及通往](../Page/彭特.md "wikilink")[努比亞的陸路的樞紐](../Page/努比亞.md "wikilink")。但正因為這個優勢，[第十一王朝法老](../Page/第十一王朝.md "wikilink")[曼图霍特普二世決定把首都定在底比斯](../Page/曼图霍特普二世.md "wikilink")，並在底比斯為[阿蒙神大興土木](../Page/阿蒙.md "wikilink")，興建很多神廟，底比斯從此成為古埃及的聖地。

根据历史年表，底比斯可以算是埃及的九朝古都。自十一王朝第一位国王塞赫塔尼-安特夫（Sehertani
Antef，前2120-前2118年）决定迁都至此并开始建设，以后在此建都的王朝有：十二王朝（前1991）、十三王朝（前1785）、十六王朝（前1622）、十八王朝（前1580）、十九王朝（前1314）、二十王朝（前1200）、二十一王朝（前1085）、和二十五王朝（前716）。\[1\]

## 重新興起

在[新王國時期](../Page/新王國時期.md "wikilink")，底比斯再次作為埃及首都和的宗教中心。在東底比斯，法老為阿蒙神和他們自己建立很多壯觀的神廟和宮殿。與此同時，法老亦在西底比斯建設了一系列華麗的陵墓，當中最為著名的是[拉美西斯二世墓和](../Page/拉美西斯二世.md "wikilink")[圖坦卡蒙墓](../Page/圖坦卡蒙.md "wikilink")。

## 登录基准

## 参考文献

## 外部連結

  - [神奇古都底比斯](https://web.archive.org/web/20090401203651/http://www.lusin.cn/book/world5000a/07.htm)
  - [联合国教科文组织世界遗产中心](http://whc.unesco.org/en/list/87/)
  - [底比斯城定位計劃](http://www.thebanmappingproject.com/)

[Category:古埃及](../Category/古埃及.md "wikilink")
[Category:埃及城市](../Category/埃及城市.md "wikilink")

1.  《ART AND HISTORY OF EGYPT》 by ALBERTO CARLO CARPICECI，BONECHI
    PUBLICATION