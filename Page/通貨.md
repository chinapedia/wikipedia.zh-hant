[AltechinesischeMuenzen.jpg](https://zh.wikipedia.org/wiki/File:AltechinesischeMuenzen.jpg "fig:AltechinesischeMuenzen.jpg")[戰國時代的](../Page/戰國時代.md "wikilink")[刀錢](../Page/刀錢.md "wikilink")、[布錢與](../Page/布錢.md "wikilink")[孔方錢](../Page/孔方錢.md "wikilink")\]\]
**通货**，是指在社会经济活动中作为流通手段的货币。\[1\][货币的一个作为交换媒介形式的部分](../Page/货币.md "wikilink")，通货是各国[货币供应量中无需](../Page/货币.md "wikilink")[背书的部分](../Page/背书.md "wikilink")，只包括[纸币](../Page/纸币.md "wikilink")、[硬币](../Page/硬币.md "wikilink")\[2\]\[3\]，不包括货币的存储功能以及其他支付手段。各国通货根据[汇率在通货领域进行交换](../Page/汇率.md "wikilink")，由于不同的汇率而分为固定通货和浮动通货。各国一般都是控制自己本国通货的供需状态，唯一例外的是[欧盟](../Page/欧盟.md "wikilink")，由[欧洲中央银行统一控制欧盟各国的通货政策](../Page/欧洲中央银行.md "wikilink")。

各国由中央银行或财政部控制本国的通货，来调整[经济状况](../Page/经济.md "wikilink")，有的几个国家统一实行一种通货，有的国家允许外国通货在本国流通，但大部分国家发行自己本国的通货，并不允许外国通货在本国流通。在发达国家，由于支票和电子货币的大量使用，通货已经只占[货币供应量中的小部分](../Page/货币.md "wikilink")。
一般每一种通货都有二级或三级单位，通常为百进制，如一[美圆等于](../Page/美圆.md "wikilink")100[美分组成](../Page/美分.md "wikilink")；一[英镑等于](../Page/英镑.md "wikilink")100[便士](../Page/便士.md "wikilink")；一[人民币元等于](../Page/人民币.md "wikilink")10角等于100分等。

## 現代的通貨

[2002_currency_exchange_AIGA_euro_money.png](https://zh.wikipedia.org/wiki/File:2002_currency_exchange_AIGA_euro_money.png "fig:2002_currency_exchange_AIGA_euro_money.png")
[流通貨幣列表中列出所有使用中的貨幣](../Page/流通貨幣列表.md "wikilink")。

通貨的使用是基於的概念，也就是由國家政府決定要使用哪一種貨幣。許多貨幣的名稱都叫[法郎](../Page/法郎.md "wikilink")、[元或是](../Page/元_\(貨幣\).md "wikilink")[鎊](../Page/鎊.md "wikilink")，而各國的幣值也有所不同。為了避免混淆，[國際標準組織有針對各國貨幣定義一個三個英文字的識別系統](../Page/國際標準組織.md "wikilink")（[ISO
4217](../Page/ISO_4217.md "wikilink")）。三個英文字的編碼會用[ISO
3166-1的國家做為前二個字母](../Page/ISO_3166-1.md "wikilink")，用貨幣名稱的第一個字母（像dollar則用D代表）做為第三個字母。例如[美元的編碼為USD](../Page/美元.md "wikilink")。此系統中也有不屬於特定國家的網路貨幣或是電子貨幣，例如[比特币](../Page/比特币.md "wikilink")\[4\]、。

[國際貨幣基金組織有另一個系統列出所有國家的貨幣](../Page/國際貨幣基金組織.md "wikilink")。

## 货币兑换性

货币兑换性是指個人、組織或是政府是否可以在不受央行或政府干預的情形下將地區貨幣兌換為外幣，或是將地區貨幣兌換為外幣其。货币可依其兑换性分為以下三種：

  - 充分可兑换：是指國際市場上可交易的貨幣總值沒有任何限制．政府也沒有人為的訂定[匯率為一固定值或是限制其不低於某特定值](../Page/匯率.md "wikilink")。美元就是充分可兑换的貨幣．因此美元也是[外汇市场的主要貨幣之一](../Page/外汇市场.md "wikilink")。

<!-- end list -->

  - 部份可兑换：中央銀行控管國際投資的流進及流出．但對國內大部份的小額交易沒有任何限制。一般只會對國際投資有限制．若國際投資需要轉換為外幣，需专项审批。像[印度卢比及](../Page/印度卢比.md "wikilink")[人民幣即為部份可兑换的货币](../Page/人民幣.md "wikilink")。

<!-- end list -->

  - 不可兑换：即沒有參與外汇市场，也不允許個人或組織進行合法的貨幣兌換，這種貨幣一般會稱為「凍結貨幣」，像[古巴比索及](../Page/古巴比索.md "wikilink")[朝鮮圓都是不可兑换的貨幣](../Page/朝鮮圓.md "wikilink")。

## 硬通货

[经济学中的硬通货](../Page/经济学.md "wikilink")（ 或
），是指某种在全球范围交易的[货币](../Page/货币.md "wikilink")，其可以作为可靠且稳定的[贮藏手段](../Page/贮藏手段.md "wikilink")。保证一种货币足够“硬”的因素有多种，其中包括政治稳定，低[通货膨胀](../Page/通货膨胀.md "wikilink")，稳定的[货币政策和](../Page/货币政策.md "wikilink")[财政政策](../Page/财政政策.md "wikilink")，有足够的[贵金属储备支持以及同其他货币比价长期保持稳定或攀升](../Page/贵金属.md "wikilink")。

## 参见

[Jiao_zi.jpg](https://zh.wikipedia.org/wiki/File:Jiao_zi.jpg "fig:Jiao_zi.jpg")的[交子被認為是世界上最早發行的紙質流通貨幣](../Page/交子.md "wikilink")\]\]
 **相關概念**

  - [假幣](../Page/假幣.md "wikilink")

  -
  - [外汇市场](../Page/外汇市场.md "wikilink")

  - [外汇储备](../Page/外汇储备.md "wikilink")

  -
  - [最优货币区](../Page/最优货币区.md "wikilink")

  -
  - [货币](../Page/货币.md "wikilink")

  - [通货膨胀](../Page/通货膨胀.md "wikilink")

  - [通货紧缩](../Page/通货紧缩.md "wikilink")

**衡算单位**

  -
  - [货币符号](../Page/货币符号.md "wikilink")

  -
  - [歐洲通貨單位](../Page/歐洲通貨單位.md "wikilink")

  - [虛擬貨幣](../Page/虛擬貨幣.md "wikilink")

  -
  - [区域货币](../Page/区域货币.md "wikilink")

  -
  - [特别提款权](../Page/特别提款权.md "wikilink")

**列表**

  - [货币列表](../Page/货币列表.md "wikilink")
  - [流通货币列表](../Page/流通货币列表.md "wikilink")

## 参考文献

## 外部链接

  - [各国货币](http://tomchao.com/)

[Category:经济学](../Category/经济学.md "wikilink")
[Category:通貨](../Category/通貨.md "wikilink")

1.
2.  （See definition 1）
3.
4.