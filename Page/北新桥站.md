[Exit_B_of_Beixinqiao_Station_(20160315153203).jpg](https://zh.wikipedia.org/wiki/File:Exit_B_of_Beixinqiao_Station_\(20160315153203\).jpg "fig:Exit_B_of_Beixinqiao_Station_(20160315153203).jpg")
**北新桥站**位于[北京市](../Page/北京市.md "wikilink")[东城区](../Page/东城区_\(北京市\).md "wikilink")，是[北京地铁](../Page/北京地铁.md "wikilink")[5号线的一个车站](../Page/北京地铁5号线.md "wikilink")。車站色調為淺綠色。未来本站将成为[5号线与](../Page/北京地铁5号线.md "wikilink")[机场线轉乘站](../Page/北京地铁机场线.md "wikilink")。

## 位置

这个站位于北新桥路口，[东直门内大街和](../Page/东直门内大街.md "wikilink")[雍和宫大街](../Page/雍和宫大街.md "wikilink")、[东四北大街交汇处](../Page/东四北大街.md "wikilink")，并因此得名。

## 结构

这座车站为地下车站，[5号线站台为](../Page/北京地铁5号线.md "wikilink")[岛式站台设计](../Page/岛式站台.md "wikilink")。

<table>
<tbody>
<tr class="odd">
<td><p><strong>地面</strong></p></td>
<td><p>出入口</p></td>
<td><p>A—D出入口</p></td>
</tr>
<tr class="even">
<td><p><strong>地下一层</strong></p></td>
<td><p>站厅</p></td>
<td><p>票务服务、自动售票机、一卡通充值</p></td>
</tr>
<tr class="odd">
<td><p><strong>地下二层</strong></p></td>
<td><p>轨道（东）</p></td>
<td><p>列车往方向 <small>（）</small></p></td>
</tr>
<tr class="even">
<td><center>
<p><small><a href="../Page/岛式站台.md" title="wikilink">岛式站台</a>，左边车门将会开启</small></p>
</center></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>轨道（西）</p></td>
<td><p>　列车往<a href="../Page/宋家庄站_(北京).md" title="wikilink">宋家庄方向</a> <small>（）</small> </p></td>
<td></td>
</tr>
</tbody>
</table>

## 出口

这座车站有4个出入口：A（西北）、B（东北）、C（东南）、D（西南）。

<table>
<thead>
<tr class="header">
<th><p>编号</p></th>
<th><p>建议前往的目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>交道口东大街、北京市第六医院、东城区文化文物局</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>北新桥头条、<a href="../Page/东直门内大街.md" title="wikilink">东直门内大街</a>（簋街）、<a href="../Page/中国新闻社.md" title="wikilink">中国新闻社</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/东直门内大街.md" title="wikilink">东直门内大街</a>（簋街）、北新桥医院</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>东四北大街、嘻哈包袱铺、交东小区、东城社保中心、<a href="../Page/北京市第五中学.md" title="wikilink">北京市第五中学</a>、田汉故居</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 临近车站

[Category:2007年启用的铁路车站](../Category/2007年启用的铁路车站.md "wikilink")
[Category:北京市东城区地铁车站](../Category/北京市东城区地铁车站.md "wikilink")