**王竹溪**（），名**治淇**，号**竹溪**，中国[物理学家](../Page/物理学.md "wikilink")、[教育家](../Page/教育家.md "wikilink")、[文字学家](../Page/文字学家.md "wikilink")。

## 生平

王竹溪1911年6月7日生於[湖北](../Page/湖北.md "wikilink")[公安縣](../Page/公安縣.md "wikilink")，1933年[清华大学物理系毕业](../Page/清华大学.md "wikilink")，同年入该校研究院。1935年公费留英，1938年获得[英国](../Page/英国.md "wikilink")[剑桥大学博士学位](../Page/剑桥大学.md "wikilink")，随即回国任清华大学物理系教授，先后任教于国立[西南联大](../Page/西南联大.md "wikilink")、[清华大学](../Page/清华大学.md "wikilink")。1952年后任[北京大学教授](../Page/北京大学.md "wikilink")，1955年当选为[中国科学院](../Page/中国科学院.md "wikilink")[数学物理学部委员](../Page/数学物理.md "wikilink")，并任北京大学副校长等职。[文化大革命期间被扣上](../Page/文化大革命.md "wikilink")“反动学术权威”帽子，下放到[江西](../Page/江西.md "wikilink")[鄱阳湖鲤鱼洲北大](../Page/鄱阳湖.md "wikilink")[五七干校牧牛](../Page/五七干校.md "wikilink")，感染[血吸虫病](../Page/血吸虫病.md "wikilink")。

王竹溪著《热力学》和《统计物理学导论》等教科书。他还曾任[中国物理学会名词委员会主任](../Page/中国物理学会.md "wikilink")，创造了一批物理学新名词的汉译称呼。在研究物理学的同时，他还潜心研究文字学，著有二百五十万字《[新部首大字典](../Page/新部首大字典.md "wikilink")》，该词典将《[康熙字典](../Page/康熙字典.md "wikilink")》中的214个部首简化为
56个，并且将五万余个汉字按笔划上下左右次序编排，检索方便，收字齐全。

1983年1月30日，因急性肝坏死，在[北京友谊医院病逝](../Page/北京友谊医院.md "wikilink")。

诺贝尔物理学奖获得者[杨振宁](../Page/杨振宁.md "wikilink")、[中国科学院院长](../Page/中国科学院.md "wikilink")[周光召等都是王竹溪的学生](../Page/周光召.md "wikilink")，杨振宁说：“我对统计物理的兴趣即是受了竹溪师的影响。”2003年王竹溪铜像在北京大学校园落成。

## 著作

### 《热力学》

上世纪40年代王竹溪教授在[西南联大讲授热力学](../Page/西南联大.md "wikilink")，讲稿最初用英文写出，经过十多年的修改，在1955年首次以中文出版\[1\]，在北京大学物理系用作大三的热力学课程教材，由王竹溪讲授亲自讲授，为期一学期。此后多次重印，在1987年出第二版，获得全国高等院校教材特等奖。2005年由北京大学出版社出版简体字版\[2\]。王竹溪教授的热力学一书系统严密，内容丰富，使此书能屹立至今，超过半个世纪。

### 其他著作

  - 《[特殊函数概论](../Page/特殊函数概论.md "wikilink")》王竹溪、[郭敦仁合著](../Page/郭敦仁.md "wikilink")。
  - 《统计物理学导论》，王竹溪著，高等教育出版社
  - 《[新部首大字典](../Page/新部首大字典.md "wikilink")》，王竹溪编著，上海翻译出版公司，1988，ISBN
    7-5053-0013-X/H2
  - Special Functions, Wang Z X, Guo D R, Singapore World Scientific
    Publishing C. 1989 ISBN 997150667X

## 參考資料

  - 王正行 《严谨与简洁之美-王竹溪一生的物理追求》 北京大学出版社 2008.4 ISBN 978-7-301-13538-9
  - [王竹溪简历（中国科学技术协会）](https://web.archive.org/web/20050407010217/http://www.cpst.net.cn/kxj/zgkxjszj/cx/lxb/pe/wl05055001.htm)

[Category:中国物理学家](../Category/中国物理学家.md "wikilink")
[Category:中国语言学家](../Category/中国语言学家.md "wikilink")
[Category:公安人](../Category/公安人.md "wikilink")
[Category:1911年出生](../Category/1911年出生.md "wikilink")
[Category:1983年逝世](../Category/1983年逝世.md "wikilink")
[Category:清华大学校友](../Category/清华大学校友.md "wikilink")
[Category:剑桥大学校友](../Category/剑桥大学校友.md "wikilink")
[Category:国立西南联合大学教授](../Category/国立西南联合大学教授.md "wikilink")
[Category:清华大学教授](../Category/清华大学教授.md "wikilink")
[Category:北京大学教授](../Category/北京大学教授.md "wikilink")
[ZHU](../Category/王姓.md "wikilink")

1.  《热力学》，王竹溪著 高等教育出版社 1955
2.  《热力学》，王竹溪著，北京大学出版社 2005 ISBN 7-301-08493-1