[Miles Away](../Page/Miles_Away.md "wikilink"){{〉}} | 拍攝地 =  | 執行製片 = |
製片人 = 後藤博幸、清水一幸 | 國家 =  | 語言 = [日語](../Page/日語.md "wikilink") | 頻道 =
[富士電視台](../Page/富士電視台.md "wikilink") | 播出國家 =  | 開始 = 2008年5月12日 | 結束
= 2008年7月14日 | 集數 = 10 | 官方網站 =
<https://web.archive.org/web/20101005231007/http://wwwz.fujitv.co.jp/change/>
| imdb_id = | tv_com_id = }}

《**CHANGE**》是自2008年5月12日起至2008年7月14日，由[日本](../Page/日本.md "wikilink")[富士電視台](../Page/富士電視台.md "wikilink")[及其聯播網於每星期一](../Page/FNS.md "wikilink")21:00-21:54（日本時間）播出的[電視連續劇](../Page/日本電視劇.md "wikilink")，由[木村拓哉](../Page/木村拓哉.md "wikilink")、[深津繪里](../Page/深津繪里.md "wikilink")、[阿部寬](../Page/阿部寬.md "wikilink")、[加藤羅莎等人主演](../Page/加藤羅莎.md "wikilink")。首回的播放時間延長30分鐘到22:24。

## 劇情

朝倉啟太原本是一個與政治八竿子打不著的鄉下小學老師，性格溫吞，連在學校都會被學生惡整。他的父親政友黨眾議員朝倉誠連同接班的大哥[空難身亡之後](../Page/空難.md "wikilink")，朝倉誠的妻子勸說身為次子的啟太辭去小學教師投入參選，從未從政的他當選了眾議員。而政友黨鑑於低迷的支持率，黨內提議由朝倉啟太競選黨總裁以出任[內閣總理大臣](../Page/日本內閣總理大臣.md "wikilink")，出任內閣官房長官的神林把朝倉當成傀儡，意在支持率提高後逼退朝倉以換自己出任總理大臣。

## 登場人物

### 主角關係人物

  - 朝倉啟太（35）-
    [木村拓哉](../Page/木村拓哉.md "wikilink")（[SMAP](../Page/SMAP.md "wikilink")）（香港配音：[蘇強文](../Page/蘇強文.md "wikilink")）

<!-- end list -->

  -
    1973年3月5日出生於福岡縣。眾議員朝倉誠之二子，18年前因父親收受獻金而對政治反感，從小離家往長野縣，就讀於信州大學，並在當地擔任國小老師。自中學時代已喜歡天文觀察。但在父親空難死去後被美山理香說服，出馬競選父親所遺留的福岡縣第12選區眾議員之位，此後踏入政界，其後出任內閣總理大臣，成為日本憲政史上最年輕的總理大臣。

<!-- end list -->

  - 美山理香（35）-
    [深津繪里](../Page/深津繪里.md "wikilink")（香港配音：[鄭麗麗](../Page/鄭麗麗.md "wikilink")）

<!-- end list -->

  -
    5年前本是財務省的官僚，後被神林議員啟發走上政治道路。對憲政運作非常熟識，最先为神林議員之秘書，在朝倉誠死後，受神林議員指派說服其子朝倉啟太參加競選，並一路輔佐他。在朝倉啟太成為總理大臣之後，擔任總理大臣首席秘書官，並領導旗下四位分別從財務省、經濟產業省、外務省和警察廳官僚借調來的總理大臣事務秘書官。雖是政友黨舊人，但對黨內利用朝倉的計畫不知情。在朝倉啟太請辭後及重新參加眾議院大選時，已成為朝倉啟太之妻。

<!-- end list -->

  - 韮澤勝利（44）-
    [阿部寬](../Page/阿部寬.md "wikilink")（香港配音：[陳欣](../Page/陳欣.md "wikilink")）

<!-- end list -->

  -
    超級選舉專家，主張「選舉是法律上唯一合法的戰爭」。選舉戰績199勝1敗，唯一敗戰是第1次自己參選時。幫助朝倉啟太打贏之後許多選戰。當朝倉啟太成為總理大臣之後，擔任總理大臣輔助官，後來因為神林議員聲稱被總理罷免而辭去內閣官房長官之職，韮澤被委任為內閣官房長官。

<!-- end list -->

  - 宮本光（23）-
    [加藤羅莎](../Page/加藤羅莎.md "wikilink")（香港配音：[程文意](../Page/程文意.md "wikilink")）

<!-- end list -->

  -
    朝倉啟太地方後援會會長姪女，幫助朝倉啟太處理事務。對朝倉啟太有淡淡的好感。

<!-- end list -->

  - 月丘瑠美子（35）-
    [堀内敬子](../Page/堀内敬子.md "wikilink")（香港配音：[林芷筠](../Page/林芷筠.md "wikilink")）

<!-- end list -->

  -
    內閣總理官邸的醫師。

<!-- end list -->

  - 松谷 -
  - 檀原段（36） - （香港配音：[李錦綸](../Page/李錦綸_\(配音員\).md "wikilink")）
  - 羽鳥 -
  - 朝倉貴江（59）-
    [富司純子](../Page/富司純子.md "wikilink")（香港配音：[袁淑珍](../Page/袁淑珍.md "wikilink")）

<!-- end list -->

  -
    朝倉啟太之母。在其夫意外死亡後，以自己出馬參選的苦肉計，逼得啟太不得不投身眾議員補選。

<!-- end list -->

  - 朝倉誠 -
  - 朝倉昌也 -

### 日本政友黨關係人物

支持度處於有史以來最低的危險情況。為避免下屆選舉輸給在野黨，黨內高層展開一連串的權力運作。黨內最高層為神林正一、小野田朝雄、二瓶榮、垣内達彦等人。此數人對朝倉一派持著既想利用又想扯後腿的態度，而他們之間也在暗鬥黨內第一人之位，權衡關係複雜。

  - 神林正一（59）-
    [寺尾聰](../Page/寺尾聰.md "wikilink")（香港配音：[朱子聰](../Page/朱子聰.md "wikilink")）

<!-- end list -->

  -
    日本政友黨總務會長（1-3話），內閣官房長官（4話以後）。動用黨的力量大力支持朝倉啟太，讓他一夜之間登上總理大臣大位。但他其實有著自己的計劃：「讓有清廉形象的朝倉當上總理以提升黨的支持度，之後迫朝倉下台，由各大派系領袖輪流當總理」。朝倉就職後，被美國總統親自致電向神林道賀，並稱他為「實際上的總理」。主導內閣人事及政策，企圖把朝倉控制成為傀儡。其餘三位大老對未能入閣表示不滿時，他表示「將來各位也可以入閣」。不滿朝倉脫離控制的措施，決定藉補助預算案攻防戰與散布收賄醜聞迫朝倉下台。

<!-- end list -->

  - 近藤光輝（54）- [風間杜夫](../Page/風間杜夫.md "wikilink")
    （香港配音：[招世亮](../Page/招世亮.md "wikilink")）

<!-- end list -->

  -
    神林正一的主任秘書，彼此極度信任。

<!-- end list -->

  - 小野田朝雄（67）-
    [中村敦夫](../Page/中村敦夫.md "wikilink")（香港配音：[陳曙光](../Page/陳曙光.md "wikilink")）

<!-- end list -->

  -
    日本政友黨幹事長，前國土交通大臣。任內因興建水壩引發副作用引致該區居民漁獲大跌，基層民代出身，因看見朝倉總理努力推動個案為民請命受感動，於是對朝倉態度大為轉變。在第6話補助預算攻防戰中，動員三十五位眾議員支持議案。

<!-- end list -->

  - 二瓶 榮（72）- （香港配音：[黃子敬](../Page/黃子敬.md "wikilink")）

<!-- end list -->

  -
    日本政友黨長老議員，二瓶派首領，對權力的追求非常執著。

<!-- end list -->

  - 垣內達彦（65）-  （香港配音：[林保全](../Page/林保全.md "wikilink")）

<!-- end list -->

  -
    衆議院議員。黨內大老之一，與二瓶類似，非常關注自己的事業。

<!-- end list -->

  - 生方恒男（38）- [石黑賢](../Page/石黑賢.md "wikilink")
    （香港配音：[翟耀輝](../Page/翟耀輝.md "wikilink")）

<!-- end list -->

  -
    衆議院議員，年紀較輕。原本為證券公司職員不想從政，因父親意外死去而繼承衣缽，而選舉也是韮澤勝利策劃，許多情況都與朝倉類似，故是國會裡少數對朝倉持友善態度者。

<!-- end list -->

  - 鴨志田 - [東根作壽英](../Page/東根作壽英.md "wikilink")
    （香港配音：[劉昭文](../Page/劉昭文.md "wikilink")）
  - 高柳 - （香港配音：[陳廷軒](../Page/陳廷軒.md "wikilink")）
  - 森重利一 - [隈部洋平](../Page/隈部洋平.md "wikilink")
  - 鵜飼武彥（77）- （特別演出；香港配音：[譚炳文](../Page/譚炳文.md "wikilink")）

<!-- end list -->

  -
    第91任內閣總理大臣（1-3話）。為歷代評價最差的總理大臣，故事一開始即因自身緋聞而宣告下台，由朝倉啟太接任。

<!-- end list -->

  - 五十川秀樹 -
    [兒玉謙次](../Page/兒玉謙次.md "wikilink")（香港配音：[譚炳文](../Page/譚炳文.md "wikilink")）
  - 綿見大一郎 - （香港配音：[盧國權](../Page/盧國權.md "wikilink")）
  - 小松崎和也 -
    [久富惟晴](../Page/久富惟晴.md "wikilink")（香港配音：[林國雄](../Page/林國雄.md "wikilink")）
  - 中村勘 -
  - 下山三郎 - [渡邊憲吉](../Page/渡邊憲吉.md "wikilink")
  - 大龜順三郎 - [戶澤佑介](../Page/戶澤佑介.md "wikilink")
  - 關根 - [唐澤民賢](../Page/唐澤民賢.md "wikilink")
  - 河原崎 -
  - 眾議院預算委員長 -
  - 門倉 -

### 總理大臣事務秘書官

  - 百坂哲也（48）-
    [西村雅彥](../Page/西村雅彥.md "wikilink")（香港配音：[葉振聲](../Page/葉振聲.md "wikilink")）

<!-- end list -->

  -
    事務秘書官，原美山理香在財務省的前輩，原只是消極任官態度，後受啟太拼命工作的態度打動而協助與各個負責局長為了更改做交涉。

<!-- end list -->

  - 郡司敏夫（56）-
    [平泉成](../Page/平泉成.md "wikilink")（香港配音：[梁志達](../Page/梁志達.md "wikilink")）
  - 西誠二（42）- （香港配音：[雷霆](../Page/雷霆.md "wikilink")）
  - 秋山太郎勘介（34）-
    [鈴木浩介](../Page/鈴木浩介_\(演員\).md "wikilink")（香港配音：[陳卓智](../Page/陳卓智.md "wikilink")）

### 高峰町立清澤小學學生

  - 小島美久 -
    [山田夏海](../Page/山田夏海.md "wikilink")（香港配音：[林元春](../Page/林元春.md "wikilink")）
  - 上野茜 -
    [小野花梨](../Page/小野花梨.md "wikilink")（香港配音：[何璐怡](../Page/何璐怡.md "wikilink")）
  - 林和人 -
    [一之瀨聖崇](../Page/一之瀨聖崇.md "wikilink")（香港配音：[黃鳳英](../Page/黃鳳英.md "wikilink")）
  - 村田淳之介 -
    [岩沼佑亮](../Page/岩沼佑亮.md "wikilink")（香港配音：[雷碧娜](../Page/雷碧娜.md "wikilink")）
  - 桑名琢磨 -
    [五十畑哉耶](../Page/五十畑哉耶.md "wikilink")（香港配音：[謝潔貞](../Page/謝潔貞.md "wikilink")）
  - 其他演員 -
    [伊藤龍彌](../Page/伊藤龍彌.md "wikilink")、[今泉野乃香](../Page/今泉野乃香.md "wikilink")、[入江姫加](../Page/入江姫加.md "wikilink")、[兼尾瑞穗](../Page/兼尾瑞穗.md "wikilink")、[越川禮菜](../Page/越川禮菜.md "wikilink")、[田邊未佳](../Page/田邊未佳.md "wikilink")、[千葉理奈](../Page/千葉理奈.md "wikilink")、[東郷晉也](../Page/東郷晉也.md "wikilink")、[新見紗央](../Page/新見紗央.md "wikilink")、[西浦璃奈](../Page/西浦璃奈.md "wikilink")、[服部唯人](../Page/服部唯人.md "wikilink")、[遼花](../Page/遼花.md "wikilink")、[山谷花純](../Page/山谷花純.md "wikilink")

### 其他

  - 外木場清光 -
    [上田耕一](../Page/上田耕一.md "wikilink")（香港配音：[陳永信](../Page/陳永信.md "wikilink")）
  - 熊田 -
    [田窪一世](../Page/田窪一世.md "wikilink")（香港配音：[梁志達](../Page/梁志達.md "wikilink")）
  - 新保 久子 -
    [福松美](../Page/福松美.md "wikilink")（香港配音：[黃鳳英](../Page/黃鳳英.md "wikilink")）
  - 播報員 -
    [並樹史朗](../Page/並樹史朗.md "wikilink")（香港配音：[張炳強](../Page/張炳強.md "wikilink")）
  - 村山嘉津男（57） -
    [泉谷茂](../Page/泉谷茂.md "wikilink")（香港配音：[張炳強](../Page/張炳強.md "wikilink")）
  - 電視節目主持人 -
    [吹田明日香](../Page/吹田明日香.md "wikilink")（香港配音：[梁少霞](../Page/梁少霞.md "wikilink")）
  - JBS電視台人員 -
    [UNJASH](../Page/UNJASH.md "wikilink")（[兒嶋一哉](../Page/兒嶋一哉.md "wikilink")、[渡部建](../Page/渡部建.md "wikilink")）（香港配音：[張錦江](../Page/張錦江.md "wikilink")）
  - 食堂的夫婦 -
    [菊池均也](../Page/菊池均也.md "wikilink")、[宮地雅子](../Page/宮地雅子.md "wikilink")（香港配音：[陳廷軒](../Page/陳廷軒.md "wikilink")、[伍秀霞](../Page/伍秀霞.md "wikilink")）
  - 久野正次郎（68） -
    [森本雷歐](../Page/森本雷歐.md "wikilink")（香港配音：[招世亮](../Page/招世亮.md "wikilink")）
  - 老奶奶 - [森康子](../Page/森康子.md "wikilink")
  - 國土交通省事務次官 - [淺見小四郎](../Page/淺見小四郎.md "wikilink")
  - 哈利·賓漢通商代表 -
    [尼可拉斯·佩塔斯](../Page/尼可拉斯·佩塔斯.md "wikilink")（香港配音：[潘文柏](../Page/潘文柏.md "wikilink")）
  - 喬治·達加特 - [賽恩·卡繆](../Page/賽恩·卡繆.md "wikilink")
  - 早川奈津子 -
    [上原美佐](../Page/上原美佐.md "wikilink")（香港配音：[梁少霞](../Page/梁少霞.md "wikilink")）
  - 小柳健介 -
    [忍成修吾](../Page/忍成修吾.md "wikilink")（香港配音：[梁偉德](../Page/梁偉德.md "wikilink")）
  - 野呂勘三郎（63） -
    [高橋英樹](../Page/高橋英樹.md "wikilink")（香港配音：[張炳強](../Page/張炳強.md "wikilink")）
  - 多湖議員 - [齊藤曉](../Page/齊藤曉.md "wikilink")
  - 松井尚子 -
    [高橋由美子](../Page/高橋由美子.md "wikilink")（香港配音：[沈小蘭](../Page/沈小蘭.md "wikilink")）
  - 松井亮介 -
    [廣田亮平](../Page/廣田亮平.md "wikilink")（香港配音：[陸惠玲](../Page/陸惠玲.md "wikilink")）
  - 財務省會計局長 -
    [田口主將](../Page/田口主將.md "wikilink")（香港配音：[翟耀輝](../Page/翟耀輝.md "wikilink")）
  - 財務省幹部 -
    [神崎智孝](../Page/神崎智孝.md "wikilink")（香港配音：[林保全](../Page/林保全.md "wikilink")）
  - 法國大使夫妻 -
    [伊恩·摩爾](../Page/伊恩·摩爾.md "wikilink")（香港配音：[翟耀輝](../Page/翟耀輝.md "wikilink")）、[黛莎](../Page/黛莎.md "wikilink")
  - 櫻木事務次官 - [ト字高雄](../Page/ト字高雄.md "wikilink")
  - 野野村三（55）內閣府事務次官 -
    [大和田伸也](../Page/大和田伸也.md "wikilink")（香港配音：[潘文柏](../Page/潘文柏.md "wikilink")）

## 製作人員

  - 編劇：[福田靖](../Page/福田靖.md "wikilink")
  - 音樂：[延近輝之](../Page/延近輝之.md "wikilink")
  - 協力：エスブイエス、[VIDEO
    STAFF](../Page/VIDEO_STAFF.md "wikilink")、[八峯電視台](../Page/八峯電視台.md "wikilink")、、、Halftone
    Music、、アクティス、エルエーカンパニー、ジャニーズファミリークラブ
  - 構成協力：[酒井雅秋](../Page/酒井雅秋.md "wikilink")、[山本あかり](../Page/山本あかり.md "wikilink")、[木下草介](../Page/木下草介.md "wikilink")、[山岡潤平](../Page/山岡潤平.md "wikilink")
  - 監修：[田崎史郎](../Page/田崎史郎.md "wikilink")、[飯島勲](../Page/飯島勲.md "wikilink")、[福田達夫](../Page/福田達夫.md "wikilink")、[佐伯耕三](../Page/佐伯耕三.md "wikilink")
  - 選舉指導：[三浦博史](../Page/三浦博史.md "wikilink")
  - 政治指導：[田中良幸](../Page/田中良幸.md "wikilink")、[川上和久](../Page/川上和久.md "wikilink")、[中原由利](../Page/中原由利.md "wikilink")
  - 助理製作：[石原隆](../Page/石原隆.md "wikilink")
  - 導演：[澤田鎌作](../Page/澤田鎌作.md "wikilink")（第1-2、5、8、10話）、[平野眞](../Page/平野眞.md "wikilink")（第3-4、6-7、9話）
  - 制作：[富士電視台電視劇制作部](../Page/富士電視台.md "wikilink")

## 播放日期、副題、收視率

<table>
<thead>
<tr class="header">
<th><p>集數</p></th>
<th><p>播放日期</p></th>
<th><p>原文副題</p></th>
<th><p>中文副題</p></th>
<th><p>收視率</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>關東</p></td>
<td><p>關西</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第1回</p></td>
<td><p>2008年5月12日</p></td>
<td><p>{{lang|ja|小学校教師が日本を変える!?<br />
政治の素人が最年少総理大臣に</p></td>
<td><p>}}</p></td>
<td><p>小學教師即將改變日本!?<br />
政治門外漢變身最年輕總理大臣</p></td>
</tr>
<tr class="odd">
<td><p>第2回</p></td>
<td><p>2008年5月19日</p></td>
<td></td>
<td><p>國會王子的初體驗</p></td>
<td><p>23.0%</p></td>
</tr>
<tr class="even">
<td><p>第3回</p></td>
<td><p>2008年5月26日</p></td>
<td><p>{{lang|ja|今夜、総理誕生</p></td>
<td><p>}}</p></td>
<td><p>今夜，總理誕生</p></td>
</tr>
<tr class="odd">
<td><p>第4回</p></td>
<td><p>2008年6月2日</p></td>
<td></td>
<td><p>被欺負的總理</p></td>
<td><p>19.3%</p></td>
</tr>
<tr class="even">
<td><p>第5回</p></td>
<td><p>2008年6月9日</p></td>
<td></td>
<td><p>總理休息日的大事件</p></td>
<td><p>19.5%</p></td>
</tr>
<tr class="odd">
<td><p>第6回</p></td>
<td><p>2008年6月16日</p></td>
<td></td>
<td><p>戀愛醜聞</p></td>
<td><p><span style="color: green;">19.1%</span></p></td>
</tr>
<tr class="even">
<td><p>第7回</p></td>
<td><p>2008年6月23日</p></td>
<td><p>{{lang|ja|15分拡大SP・衝撃の結末</p></td>
<td><p>消えた総理}}</p></td>
<td><p>15分鐘延長特别篇·沖擊性的結局</p></td>
</tr>
<tr class="odd">
<td><p>第8回</p></td>
<td><p>2008年6月30日</p></td>
<td></td>
<td><p>離別。然後逆襲</p></td>
<td><p>19.5%</p></td>
</tr>
<tr class="even">
<td><p>第9回</p></td>
<td><p>2008年7月7日</p></td>
<td><p>{{lang|ja|衝撃</p></td>
<td><p>総理倒れる}}</p></td>
<td><p>衝擊</p></td>
</tr>
<tr class="odd">
<td><p>最終回</p></td>
<td><p>2008年7月14日</p></td>
<td></td>
<td><p>最終回擴大90分鐘SP<br />
再見了，朝倉總理淚的最後訊息<br />
22分鐘的全國實況轉播</p></td>
<td><p><span style="color: red;">27.4%</span></p></td>
</tr>
<tr class="even">
<td><p>平均收視率</p></td>
<td><p>22.1%</p></td>
<td><p>26.72%</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 其他

本劇劇情與漫畫《[政治最前線](../Page/政治最前線.md "wikilink")》頗為類似。

## 外部連結

  - [富士電視台官方網站](https://web.archive.org/web/20101005231007/http://wwwz.fujitv.co.jp/change/)

  - [日劇 Change -
    新生代的政治家風範：紀錄朝倉啟太部份演說內容。](http://timespacewalker.blogspot.com/2009/01/change.html)

  - [緯來日本台官方網站](http://japan.videoland.com.tw/channel/change/)

  - [無綫電視官方網站](http://programme.tvb.com/drama/change)

## 作品的變遷

[Category:2008年日本電視劇集](../Category/2008年日本電視劇集.md "wikilink")
[Category:永田町背景作品](../Category/永田町背景作品.md "wikilink")
[Category:福岡縣背景作品](../Category/福岡縣背景作品.md "wikilink")
[Category:長野縣背景作品](../Category/長野縣背景作品.md "wikilink")
[Category:政治電視劇](../Category/政治電視劇.md "wikilink")
[Category:緯來電視外購日劇](../Category/緯來電視外購日劇.md "wikilink")
[Category:無綫電視外購劇集](../Category/無綫電視外購劇集.md "wikilink")
[Category:福田靖劇本電視劇](../Category/福田靖劇本電視劇.md "wikilink")
[Category:日語電視劇](../Category/日語電視劇.md "wikilink")
[Category:香港有線電視外購劇集](../Category/香港有線電視外購劇集.md "wikilink")
[Category:木村拓哉](../Category/木村拓哉.md "wikilink")