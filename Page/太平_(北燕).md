**太平**（409年十月—430年十二月）是[十六國時期](../Page/十六國.md "wikilink")[北燕政權](../Page/北燕.md "wikilink")，北燕太祖文成帝[冯跋的](../Page/冯跋.md "wikilink")[年號](../Page/年號.md "wikilink")，共計21年餘。

太平二十二年九月[馮弘沿用](../Page/馮弘.md "wikilink")。

## 纪年

| 太平                               | 元年                             | 二年                             | 三年                             | 四年                             | 五年                             | 六年                             | 七年                             | 八年                             | 九年                             | 十年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 409年                           | 410年                           | 411年                           | 412年                           | 413年                           | 414年                           | 415年                           | 416年                           | 417年                           | 418年                           |
| [干支](../Page/干支纪年.md "wikilink") | [己酉](../Page/己酉.md "wikilink") | [庚戌](../Page/庚戌.md "wikilink") | [辛亥](../Page/辛亥.md "wikilink") | [壬子](../Page/壬子.md "wikilink") | [癸丑](../Page/癸丑.md "wikilink") | [甲寅](../Page/甲寅.md "wikilink") | [乙卯](../Page/乙卯.md "wikilink") | [丙辰](../Page/丙辰.md "wikilink") | [丁巳](../Page/丁巳.md "wikilink") | [戊午](../Page/戊午.md "wikilink") |
| 太平                               | 十一年                            | 十二年                            | 十三年                            | 十四年                            | 十五年                            | 十六年                            | 十七年                            | 十八年                            | 十九年                            | 二十年                            |
| [公元](../Page/公元纪年.md "wikilink") | 419年                           | 420年                           | 421年                           | 422年                           | 423年                           | 424年                           | 425年                           | 426年                           | 427年                           | 428年                           |
| [干支](../Page/干支纪年.md "wikilink") | [己未](../Page/己未.md "wikilink") | [庚申](../Page/庚申.md "wikilink") | [辛酉](../Page/辛酉.md "wikilink") | [壬戌](../Page/壬戌.md "wikilink") | [癸亥](../Page/癸亥.md "wikilink") | [甲子](../Page/甲子.md "wikilink") | [乙丑](../Page/乙丑.md "wikilink") | [丙寅](../Page/丙寅.md "wikilink") | [丁卯](../Page/丁卯.md "wikilink") | [戊辰](../Page/戊辰.md "wikilink") |
| 太平                               | 二十一年                           | 二十二年                           |                                |                                |                                |                                |                                |                                |                                |                                |
| [公元](../Page/公元纪年.md "wikilink") | 429年                           | 430年                           |                                |                                |                                |                                |                                |                                |                                |                                |
| [干支](../Page/干支纪年.md "wikilink") | [己巳](../Page/己巳.md "wikilink") | [庚午](../Page/庚午.md "wikilink") |                                |                                |                                |                                |                                |                                |                                |                                |

## 参看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
      - 其他时期使用[太平的年号](../Page/太平.md "wikilink")
  - 同期存在的其他政权年号
      - [义熙](../Page/义熙_\(晋安帝\).md "wikilink")（405年-418年十二月）：東晉皇帝[晋安帝司马德宗的年号](../Page/晋安帝.md "wikilink")
      - [元熙](../Page/元熙_\(晋恭帝\).md "wikilink")（419年正月-420年六月）：東晉皇帝[晋恭帝司马德文的年号](../Page/晋恭帝.md "wikilink")
      - [永初](../Page/永初_\(刘裕\).md "wikilink")（420年六月—423年十二月）：[南朝宋皇帝宋武帝](../Page/南朝宋.md "wikilink")[刘裕的年号](../Page/刘裕.md "wikilink")
      - [景平](../Page/景平.md "wikilink")（423年正月—424年八月）：[南朝宋皇帝](../Page/南朝宋.md "wikilink")[宋少帝刘义符的年号](../Page/宋少帝.md "wikilink")
      - [元嘉](../Page/元嘉_\(南朝宋文帝\).md "wikilink")（424年八月—453年十二月）：[南朝宋皇帝](../Page/南朝宋.md "wikilink")[宋文帝刘义隆的年号](../Page/宋文帝.md "wikilink")
      - [龙升](../Page/龙升.md "wikilink")（407年六月-413年二月）：[夏国政权](../Page/夏国.md "wikilink")[赫连勃勃年号](../Page/赫连勃勃.md "wikilink")
      - [凤翔](../Page/凤翔_\(赫连勃勃\).md "wikilink")（413年三月-418年十月）：[夏国政权](../Page/夏国.md "wikilink")[赫连勃勃年号](../Page/赫连勃勃.md "wikilink")
      - [昌武](../Page/昌武.md "wikilink")（418年十一月-419年正月）：[夏国政权](../Page/夏国.md "wikilink")[赫连勃勃年号](../Page/赫连勃勃.md "wikilink")
      - [真兴](../Page/真兴.md "wikilink")（419年二月-425年七月）：[夏国政权](../Page/夏国.md "wikilink")[赫连勃勃年号](../Page/赫连勃勃.md "wikilink")
      - [承光](../Page/承光_\(赫连昌\).md "wikilink")（425年八月-428年二月）：[夏国政权](../Page/夏国.md "wikilink")[赫连昌年号](../Page/赫连昌.md "wikilink")
      - [勝光](../Page/胜光.md "wikilink")（428年二月-431年六月）：[夏国政权](../Page/夏国.md "wikilink")[赫连定年号](../Page/赫连定.md "wikilink")
      - [弘始](../Page/弘始.md "wikilink")（399年九月-416年正月）：[后秦政权](../Page/后秦.md "wikilink")[姚兴年号](../Page/姚兴.md "wikilink")
      - [永和](../Page/永和_\(姚泓\).md "wikilink")（416年二月-417年八月）：[后秦政权](../Page/后秦.md "wikilink")[姚泓年号](../Page/姚泓.md "wikilink")
      - [更始](../Page/更始_\(乞伏乾歸\).md "wikilink")（409年七月-412年八月）：[西秦政权](../Page/西秦.md "wikilink")[乞伏乾归年号](../Page/乞伏乾归.md "wikilink")
      - [永康](../Page/永康_\(乞伏熾磐\).md "wikilink")（412年八月-419年十二月）：[西秦政权](../Page/西秦.md "wikilink")[乞伏熾磐年号](../Page/乞伏熾磐.md "wikilink")
      - [建弘](../Page/建弘.md "wikilink")（420年正月-428年五月）：[西秦政权](../Page/西秦.md "wikilink")[乞伏熾磐年号](../Page/乞伏熾磐.md "wikilink")
      - [永弘](../Page/永弘.md "wikilink")（428年五月—431年正月）：[西秦政权](../Page/西秦.md "wikilink")[乞伏暮末年号](../Page/乞伏暮末.md "wikilink")
      - [太上](../Page/太上.md "wikilink")（405年十一月-410年二月）：[南燕政权](../Page/南燕.md "wikilink")[慕容超年号](../Page/慕容超.md "wikilink")
      - [嘉平](../Page/嘉平_\(秃发傉檀\).md "wikilink")（408年十一月-414年七月）：[南凉政权](../Page/南凉.md "wikilink")[秃发傉檀年号](../Page/秃发傉檀.md "wikilink")
      - [建初](../Page/建初_\(李暠\).md "wikilink")（405年正月—417年二月）：[西涼政权](../Page/西涼.md "wikilink")[李暠年号](../Page/李暠.md "wikilink")
      - [嘉兴](../Page/嘉兴_\(李歆\).md "wikilink")（417年二月—420年七月）：[西涼政权](../Page/西涼.md "wikilink")[李歆年号](../Page/李歆.md "wikilink")
      - [永建](../Page/永建_\(李恂\).md "wikilink")（420年十月-421年三月）：[西凉政权](../Page/西凉.md "wikilink")[李恂年号](../Page/李恂.md "wikilink")
      - [永安](../Page/永安_\(北凉\).md "wikilink")（401年六月-412年十月）：[北凉政权](../Page/北凉.md "wikilink")[沮渠蒙逊年号](../Page/沮渠蒙逊.md "wikilink")
      - [玄始](../Page/玄始.md "wikilink")（412年十一月-428年十二月）：[北凉政权](../Page/北凉.md "wikilink")[沮渠蒙逊年号](../Page/沮渠蒙逊.md "wikilink")
      - [承玄](../Page/承玄.md "wikilink")（428年六月-431年）：[北凉政权](../Page/北凉.md "wikilink")[沮渠蒙逊年号](../Page/沮渠蒙逊.md "wikilink")
      - [天赐](../Page/天赐_\(北魏\).md "wikilink")（404年十月-409年十月）：[北魏政权](../Page/北魏.md "wikilink")[拓跋珪年号](../Page/拓跋珪.md "wikilink")
      - [永兴](../Page/永兴_\(北魏明元帝\).md "wikilink")（409年闰十月-413年）：[北魏政权](../Page/北魏.md "wikilink")[拓跋嗣年号](../Page/拓跋嗣.md "wikilink")
      - [神瑞](../Page/神瑞.md "wikilink")（414年正月-416年四月）：[北魏政权](../Page/北魏.md "wikilink")[拓跋嗣年号](../Page/拓跋嗣.md "wikilink")
      - [泰常](../Page/泰常.md "wikilink")（416年四月-423年十二月）：[北魏政权](../Page/北魏.md "wikilink")[拓跋嗣年号](../Page/拓跋嗣.md "wikilink")
      - [始光](../Page/始光.md "wikilink")（424年正月-428年正月）：[北魏政权](../Page/北魏.md "wikilink")[北魏太武帝拓跋燾年号](../Page/北魏太武帝.md "wikilink")
      - [神䴥](../Page/神䴥.md "wikilink")（428年二月-431年十二月）：[北魏政权](../Page/北魏.md "wikilink")[北魏太武帝拓跋燾年号](../Page/北魏太武帝.md "wikilink")

## 参考文献

1.  李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129

[Category:北燕年號](../Category/北燕年號.md "wikilink")
[Category:5世纪](../Category/5世纪.md "wikilink")