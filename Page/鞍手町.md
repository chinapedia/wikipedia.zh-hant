**鞍手町**（）是位于[北九州市西南部的一個](../Page/北九州市.md "wikilink")[城鎮](../Page/城鎮.md "wikilink")，距離北九州市市中心約20公里，為[北九州都市圈的一部份](../Page/北九州都市圈.md "wikilink")，
屬[鞍手郡](../Page/鞍手郡.md "wikilink")。

## 歷史

### 年表

  - 1889年4月1日：實施[町村制](../Page/町村制.md "wikilink")，現在的轄區在當時分屬：劍村、西川村、古月村。
  - 1908年7月1日：官營鐵道（後來的[國鐵](../Page/國鐵.md "wikilink")）室木線開始營運，轄區內設有三站。
  - 1939年4月5日：室木線古月車站開始營運。
  - 1952年8月1日：劍村改制為劍町。
  - 1955年1月1日：劍町、西川村、古月村[合併為](../Page/市町村合併.md "wikilink")**鞍手町**\[1\]，合併時人口為30,794人。
  - 1985年4月1日：室木線停止營運。
  - 1987年7月1日：[JR九州](../Page/JR九州.md "wikilink")[筑豐本線設置](../Page/筑豐本線.md "wikilink")[鞍手車站](../Page/鞍手車站.md "wikilink")。

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1889年以前</p></th>
<th><p>1889年4月1日</p></th>
<th><p>1889年 - 1926年</p></th>
<th><p>1926年 - 1944年</p></th>
<th><p>1945年 - 1954年</p></th>
<th><p>1955年 - 1988年</p></th>
<th><p>1989年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>劍村</p></td>
<td><p>1952年8月1日<br />
劍町</p></td>
<td><p>1955年1月1日<br />
鞍手町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>西川村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>古月村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 交通

### 鐵路

  - [九州旅客鐵道](../Page/九州旅客鐵道.md "wikilink")
      - [筑豐本線](../Page/筑豐本線.md "wikilink")：[鞍手車站](../Page/鞍手車站.md "wikilink")

<!-- end list -->

  - [西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")
      - [山陽新幹線](../Page/山陽新幹線.md "wikilink")：轄區內無設置車站，僅設有鞍手信號場

過去曾有[國鐵](../Page/國鐵.md "wikilink")[室木線](../Page/室木線.md "wikilink")，並設有[古月車站](../Page/古月車站.md "wikilink")、[鞍手車站](../Page/鞍手車站_\(國鐵\).md "wikilink")、[八尋車站](../Page/八尋車站.md "wikilink")、[室木車站](../Page/室木車站.md "wikilink")，但已於1985年停駛。

### 道路

  - 高速道路

<!-- end list -->

  - [九州自動車道](../Page/九州自動車道.md "wikilink")：[鞍手交流道](../Page/鞍手交流道.md "wikilink")
    - [鞍手休息區](../Page/鞍手休息區.md "wikilink")

## 教育

### 高等學校

  - [福岡縣立鞍手高等學校鞍手町立鞍手分校](../Page/福岡縣立鞍手高等學校鞍手町立鞍手分校.md "wikilink")

### 專門學校

  - [九州工業技術專門學校](../Page/九州工業技術專門學校.md "wikilink")

## 本地出身之名人

  - [次原隆二](../Page/次原隆二.md "wikilink")：[漫畫家](../Page/漫畫家.md "wikilink")

## 參考資料

## 外部連結

1.