**史料**即可據以研究或討論[歷史的東西](../Page/歷史.md "wikilink")。史料的形式和分類非常廣泛，也沒有一定的形式，某事物是否為史料、或其史料價值高低，通常必須由[歷史學家依其專業知識自行判斷](../Page/歷史學家.md "wikilink")。

## 史料分類

史料的分類方法很多，較常見的區分方式有以原始程度、和以史料形式區分兩種。如以原始程度區分，可分為一手史料和二手史料；如以史料形式區分，則可分為文字史料和非文字史料。

### 以原始程度區分

#### 一手史料

[一手史料](../Page/一手史料.md "wikilink")（primary
source），有關[歷史事件的](../Page/歷史事件.md "wikilink")[文物和](../Page/文物.md "wikilink")[當事人直接記錄的史料](../Page/當事人.md "wikilink")。

#### 二手史料

[二手史料](../Page/二手史料.md "wikilink")（secondary
source），有關歷史事件，由後人轉述記載而非由當事人直接記錄的史料。

### 以史料形式區分

#### 文字史料

  - [史書](../Page/史書.md "wikilink")：距離要研究的時代較近的人所寫的史書，是研究當時的重要史料，例如《[漢書](../Page/漢書.md "wikilink")》之於[漢代歷史](../Page/漢代.md "wikilink")，《[通典](../Page/通典.md "wikilink")》對於[唐代制度的歷史](../Page/唐代.md "wikilink")。明清的[地方志對各地區歷史的研究等](../Page/地方志.md "wikilink")。
  - [檔案文書類](../Page/檔案.md "wikilink")：尤其對[政治史及](../Page/政治史.md "wikilink")[社會史研究頗有幫助](../Page/社會史.md "wikilink")。例如[長沙](../Page/長沙.md "wikilink")《[走馬樓吳簡](../Page/走馬樓吳簡.md "wikilink")》中的[戶籍資料](../Page/戶籍.md "wikilink")、[唐代的](../Page/唐代.md "wikilink")[法律條文](../Page/法律.md "wikilink")、[明清檔案](../Page/明清檔案.md "wikilink")、[南京](../Page/南京.md "wikilink")[國民政府的檔案等](../Page/國民政府.md "wikilink")。
  - [思想或](../Page/思想.md "wikilink")[學術著作](../Page/學術.md "wikilink")：反映當時人的思想、觀念、以及學術的發展。如由《[孟子](../Page/孟子.md "wikilink")》中可看到[戰國時代人的思想觀念](../Page/戰國時代.md "wikilink")，藉由《[黃帝內經](../Page/黃帝內經.md "wikilink")》可了解中國古代人的[醫學以及對人的身體的認識](../Page/醫學.md "wikilink")。《[皇朝經世文篇](../Page/皇朝經世文篇.md "wikilink")》反映了[清代人的政治社會思想以及政治社會上的問題等等](../Page/清代.md "wikilink")。
  - [文學作品](../Page/文學.md "wikilink")：文學作品的內容雖有[虛構之處](../Page/虛構.md "wikilink")，但常可反映當時人的生活、想法以及觀念等，對於[文化史](../Page/文化史.md "wikilink")、[思想史](../Page/思想史.md "wikilink")、[社會生活史的研究有所助益](../Page/社會生活史.md "wikilink")。如從《[世說新語](../Page/世說新語.md "wikilink")》看[六朝人的思想及生活](../Page/六朝.md "wikilink")，由元[雜劇看](../Page/雜劇.md "wikilink")[元代人的觀念及生活](../Page/元代.md "wikilink")，從《[金瓶梅](../Page/金瓶梅.md "wikilink")》觀察[晚明](../Page/晚明.md "wikilink")[文化及生活](../Page/文化.md "wikilink")。[司馬光](../Page/司馬光.md "wikilink")《通鉴》卷一九五贞观十三年末尾傅奕临终前描述：“上命僧咒奕，奕初无所觉，须臾，僧忽僵仆，若为物所击，遂不复苏。又有婆罗门僧言得佛齿，所击前无坚物。长安士女辐凑如市。”，来源则是唐人[李伉的](../Page/李伉.md "wikilink")《独异志》、[韦绚的](../Page/韦绚.md "wikilink")《刘宾客嘉话录》和宋初[王傥的](../Page/王傥.md "wikilink")《唐语林》。
  - 日常生活中的文字遺留：包括如古代的[農民曆](../Page/農民曆.md "wikilink")、商店的[帳薄](../Page/帳薄.md "wikilink")、土地[契約書](../Page/契約.md "wikilink")，以及私人來往的[書信等](../Page/書信.md "wikilink")。由於這些大多不是刻意留傳下來的東西，常能更真實地反映當時的實際生活及想法。
  - [報刊](../Page/報紙.md "wikilink")[雜誌](../Page/雜誌.md "wikilink")：為[近](../Page/近代史.md "wikilink")[現代史的重要史料](../Page/現代史.md "wikilink")，一般性報紙有助了解一些事件的發展，以及當時人從不同觀點對此事的了解，娛樂性的報刊則可以觀察當時人的文化和生活。
  - [口述史料](../Page/口述史料.md "wikilink")：多用於現代史的研究，藉由對尚在人世的當時人訪問口述而獲得對歷史更直接深入的了解。
  - 其他：包括如[碑刻](../Page/碑刻.md "wikilink")、[墓誌](../Page/墓誌.md "wikilink")、[族譜等等](../Page/族譜.md "wikilink")。

#### 非文字史料

  - [圖像類](../Page/圖像.md "wikilink")：如[繪畫](../Page/繪畫.md "wikilink")、[壁畫](../Page/壁畫.md "wikilink")、[刺繡](../Page/刺繡.md "wikilink")[圖案及](../Page/圖案.md "wikilink")[漫畫等](../Page/漫畫.md "wikilink")，有助於了解當時人的生活及[審美觀等](../Page/審美觀.md "wikilink")，圖像史料中最有名的是有助於研究[宋代城市生活的的](../Page/宋代.md "wikilink")《[清明上河圖](../Page/清明上河圖.md "wikilink")》。
  - [實物類](../Page/實物.md "wikilink")：包括古代[建築](../Page/建築.md "wikilink")、[家具](../Page/家具.md "wikilink")、[衣物](../Page/衣物.md "wikilink")、[器物](../Page/器物.md "wikilink")、[飾品](../Page/飾品.md "wikilink")、[錢幣](../Page/錢幣.md "wikilink")、[墓葬](../Page/墓葬.md "wikilink")，用以探討當時人的生活、及觀念等等。這類的史料，有很大部分是由[考古發掘的](../Page/考古.md "wikilink")。
  - [風俗類](../Page/風俗.md "wikilink")：藉由對於當今流傳[風俗文化的觀察](../Page/風俗.md "wikilink")，作為討論歷史現像的依據，例如由現今對遺留的[民間信仰儀式研究為線索](../Page/民間信仰.md "wikilink")，來了解其在古代的情形。或由現在仍存在的[原始民族的研究](../Page/原始民族.md "wikilink")，來推斷早期古代人的[生活方式](../Page/生活方式.md "wikilink")。

## 参考文献

## 参见

  - [歷史](../Page/歷史.md "wikilink")
  - [歷史學](../Page/歷史學.md "wikilink")
  - [史學方法](../Page/史學方法.md "wikilink")
  - [文獻](../Page/文獻.md "wikilink")
  - [遺跡](../Page/遺跡.md "wikilink")
  - [遺物](../Page/遺物.md "wikilink")

[Category:歷史學](../Category/歷史學.md "wikilink")