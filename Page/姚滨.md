[Yao_Bin_2.jpg](https://zh.wikipedia.org/wiki/File:Yao_Bin_2.jpg "fig:Yao_Bin_2.jpg")
**姚滨**（），[中国](../Page/中国.md "wikilink")[黑龙江](../Page/黑龙江.md "wikilink")[哈尔滨](../Page/哈尔滨.md "wikilink")[花样滑冰教练](../Page/花样滑冰.md "wikilink")。他是[申雪](../Page/申雪.md "wikilink")/[赵宏博](../Page/赵宏博.md "wikilink")、[龐清](../Page/龐清.md "wikilink")/[佟健](../Page/佟健.md "wikilink")、[張丹](../Page/張丹.md "wikilink")/[张昊和](../Page/张昊.md "wikilink")[张悦](../Page/张悦.md "wikilink")/[王磊的教练](../Page/王磊.md "wikilink")。

在[1980年和](../Page/1984年冬季奥林匹克运动会.md "wikilink")[1984年](../Page/1984年冬季奥林匹克运动会.md "wikilink")[冬季奥林匹克运动会](../Page/冬季奥林匹克运动会.md "wikilink")，姚滨与[栾波搭档竞争花样滑冰](../Page/栾波.md "wikilink")，结果排名最末。

2008年，当选第十一届全国政协委员\[1\]，代表体育界，分入第四十三组。\[2\]

## 外部链接

  - [姚滨个人网站](https://web.archive.org/web/20060220233720/http://star.sports.cn/yaob/index.html)
  - [Shen Xue's poetry in motion on the
    rink](http://www2.chinadaily.com.cn/english/doc/2004-03/25/content_317931.htm)
    [中国日报](../Page/中国日报.md "wikilink")（英文）

## 参考

[Y](../Category/哈尔滨人.md "wikilink")
[Yao](../Category/中國花樣滑冰教練.md "wikilink")
[Yao](../Category/中国教练.md "wikilink")
[Category:第十一届全国政协委员](../Category/第十一届全国政协委员.md "wikilink")
[Category:中国花样滑冰协会名誉顾问](../Category/中国花样滑冰协会名誉顾问.md "wikilink")

1.
2.