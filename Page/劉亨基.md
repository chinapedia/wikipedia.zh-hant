**劉亨基**，[字](../Page/表字.md "wikilink")**少圃**，[湖南](../Page/湖南.md "wikilink")[湘潭人](../Page/湘潭.md "wikilink")，[中國](../Page/中國.md "wikilink")[清朝官員](../Page/清朝.md "wikilink")。

劉亨基為[乾隆十五年](../Page/乾隆.md "wikilink")（1750年）庚午科[舉人](../Page/舉人.md "wikilink")，曾任[台灣府](../Page/台灣府.md "wikilink")[鳳山縣知縣](../Page/鳳山縣知縣.md "wikilink")。[台灣府海防兼南路理番同知](../Page/台灣府海防兼南路理番同知.md "wikilink")。[乾隆四十七年](../Page/乾隆.md "wikilink")（1782年）攝[台灣縣](../Page/台灣縣.md "wikilink")[知縣](../Page/知縣.md "wikilink")，次年正月署[台灣府知府](../Page/台灣府知府.md "wikilink")，年底再署。[乾隆四十九年](../Page/乾隆.md "wikilink")（1784年）九月兼理[彰化縣](../Page/彰化縣_\(清朝\).md "wikilink")[知縣](../Page/知縣.md "wikilink")，[乾隆五十一年十一月](../Page/乾隆.md "wikilink")（1787年元月）再攝彰化縣事。

## 參考文獻

  - 劉寧顏編，《重修台灣省通志》，台北市，台灣省文獻委員會，1994年。

{{-}}

[Category:乾隆十五年庚午科舉人](../Category/乾隆十五年庚午科舉人.md "wikilink")
[Category:清朝鳳山縣知縣](../Category/清朝鳳山縣知縣.md "wikilink")
[Category:清朝彰化縣知縣](../Category/清朝彰化縣知縣.md "wikilink")
[Category:清朝臺灣縣知縣](../Category/清朝臺灣縣知縣.md "wikilink")
[Category:台灣府海防兼南路理番同知](../Category/台灣府海防兼南路理番同知.md "wikilink")
[Category:臺灣府知府](../Category/臺灣府知府.md "wikilink")
[L](../Category/湘潭人.md "wikilink")
[H](../Category/劉姓.md "wikilink")