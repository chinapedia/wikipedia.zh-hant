**米卡埃尔·西尔韦斯特**（，），是一名[法國](../Page/法國.md "wikilink")[足球](../Page/足球.md "wikilink")[運動員](../Page/運動員.md "wikilink")，司職中堅或左後衛，目前為自由身，但早前被發現於[曼聯參與集訓](../Page/曼聯.md "wikilink")\[1\]。擅長後上接應角球攻門，曾效力[雷恩](../Page/雷恩足球俱乐部.md "wikilink")、[國際米蘭](../Page/國際米蘭.md "wikilink")、[曼聯及](../Page/曼联足球俱乐部.md "wikilink")[阿仙奴](../Page/阿森纳足球俱乐部.md "wikilink")。

## 球員生涯

### 球會

施維斯達出身[法甲的](../Page/法甲.md "wikilink")[雷恩](../Page/雷恩.md "wikilink")，曾上陣49場賽事，其後轉會到[意甲球會](../Page/意甲.md "wikilink")[國際米蘭](../Page/國際米蘭.md "wikilink")，但上陣機會不多，卻被[費格遜看中](../Page/費格遜.md "wikilink")，在1999年9月10日以350萬[英鎊轉會到](../Page/英鎊.md "wikilink")[曼聯](../Page/曼聯.md "wikilink")。

加盟曼联后的西尔韦斯特一直是队伍中的重要成员，主要出任左后卫，有时也担任中后卫。虽然在球队失利或战绩不佳时经常成为被批评的对象，但是历年的出场次数一直名列前茅，其中2002-03赛季和2003-04赛季表现尤为出色。2006-07赛季，由于左后卫位置[軒斯与](../Page/加布里埃尔·海因策.md "wikilink")[艾夫拿竞争激烈](../Page/帕特里斯·埃夫拉.md "wikilink")，中后卫新加盟的[維迪坐稳主力位置](../Page/内马尼亚·维迪奇.md "wikilink")，西尔韦斯特第一次失去主力位置。

2007年9月17日，西尔韦斯特在比赛中拉伤膝部十字韧带，被宣布无缘赛季剩余比赛。2008年8月20日，西尔韦斯特75萬英鎊轉投[阿仙奴](../Page/阿仙奴.md "wikilink")，並簽約兩年<small>\[2\]</small>，穿上18號球衣，成為自1974年[布莱恩·基德後第一位由曼聯轉會至阿仙奴的球員](../Page/布莱恩·基德.md "wikilink")<small>\[3\]</small>。西尔韦斯特共为曼联在正式比赛中出场361次，攻入10球。

2010年8月30日約滿阿仙奴的施維斯達轉戰[德甲](../Page/德甲.md "wikilink")，以[自由身加盟](../Page/自由球員.md "wikilink")[雲達不萊梅](../Page/不来梅沙洲体育俱乐部.md "wikilink")，簽約兩年<small>\[4\]</small>。

### 國家隊

施維斯達早於1997年已代表法國青年軍出席[世青盃](../Page/世界青年足球錦標賽.md "wikilink")，亦40次代表[國家隊](../Page/法國國家足球隊.md "wikilink")。但在[2004年歐洲國家杯中多次犯錯導致被罰十二碼](../Page/2004年歐洲國家杯.md "wikilink")，而法國亦於次圈被[希臘淘汰出局](../Page/希臘國家足球隊.md "wikilink")。不過在[2006年世界杯](../Page/2006年世界杯.md "wikilink")，施維斯達卻仍能入選國家隊，上陣過一場\[5\]。

## 參考資料

## 外部連結

  - [足球資料庫：施維斯達](http://www.soccerbase.com/players_details.sd?playerid=16494)
  - [manutdplayers.co.uk：施維斯達連結網站](https://web.archive.org/web/20070312031704/http://www.manutdplayers.co.uk/mikael-silvestre.htm)
  - [施維斯達球迷網站](https://web.archive.org/web/20070110154450/http://www.golaccio.com/mikaelsilvestre/)
  - [u-soccer.com：施維斯達](http://www.u-soccer.com/home/players.php?fixture_type=EPL&pid=37)

[Category:法国足球运动员](../Category/法国足球运动员.md "wikilink")
[Category:足球後衛](../Category/足球後衛.md "wikilink")
[Category:雷恩球員](../Category/雷恩球員.md "wikilink")
[Category:國際米蘭球員](../Category/國際米蘭球員.md "wikilink")
[Category:曼聯球員](../Category/曼聯球員.md "wikilink")
[Category:阿仙奴球員](../Category/阿仙奴球員.md "wikilink")
[Category:雲達不萊梅球員](../Category/雲達不萊梅球員.md "wikilink")
[Category:法甲球員](../Category/法甲球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:德甲球員](../Category/德甲球員.md "wikilink")
[Category:意甲球員](../Category/意甲球員.md "wikilink")
[Category:2001年洲際國家盃球員](../Category/2001年洲際國家盃球員.md "wikilink")
[Category:2002年世界盃足球賽球員](../Category/2002年世界盃足球賽球員.md "wikilink")
[Category:2003年洲際國家盃球員](../Category/2003年洲際國家盃球員.md "wikilink")
[Category:2004年歐洲國家盃球員](../Category/2004年歐洲國家盃球員.md "wikilink")
[Category:2006年世界盃足球賽球員](../Category/2006年世界盃足球賽球員.md "wikilink")
[Category:洲際國家盃冠軍隊球員](../Category/洲際國家盃冠軍隊球員.md "wikilink")
[Category:瓜德羅普裔法國人](../Category/瓜德羅普裔法國人.md "wikilink")

1.  <http://www.goal.com/hk/news/491/%E8%8B%B1%E6%A0%BC%E8%98%AD/2012/10/02/3417652/%E8%B2%BBsir%E9%87%8D%E5%8F%AC%E6%96%BD%E7%B6%AD%E6%96%AF%E9%81%94%E8%A7%A3%E6%B1%BA%E4%B8%AD%E5%A0%85%E8%8D%92>
2.  [Arsenal sign Man Utd's
    Silvestre](http://news.bbc.co.uk/sport2/hi/football/teams/a/arsenal/7573520.stm)
3.
4.
5.  [多哥0-2法國](http://www.soccerbase.com/results3.sd?gameid=485996)