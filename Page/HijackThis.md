**HijackThis**，有时称为**HJT**，是一款由Merijn Bellekom编写，在[Microsoft
Windows平台上运行的](../Page/Microsoft_Windows.md "wikilink")[间谍软件移除工具](../Page/间谍软件.md "wikilink")。

HijackThis原是一款免费软件，2012年起在GNU通用公共许可证下成为开源软件。现在它的版本是2.0.4。

该软件可以快速扫描电脑并对比无间谍软件环境创建一个与区别列表。这个列表包括如下各项：

  - [ActiveX模块](../Page/ActiveX.md "wikilink")
  - [BHOs](../Page/Browser_Helper_Object.md "wikilink")（浏览器帮助对象）
  - 浏览器[工具条](../Page/工具条.md "wikilink")
  - 浏览器[主语以及默认使用的](../Page/主语.md "wikilink")[搜索引擎](../Page/搜索引擎.md "wikilink")
  - [Internet
    Explorer的](../Page/Internet_Explorer.md "wikilink")[插件](../Page/插件.md "wikilink")
  - *layered service providers*
  - 开机时启动的程序与服务
  - [代理服务器](../Page/代理服务器.md "wikilink")

HijackThis对搜集到的信息即可以保存为[纯文本日志格式也可以禁用发现项](../Page/纯文本.md "wikilink")。HijackThis可以禁用正在运行的重要程序，这可能导致系统或外围设备停止工作。所以HijackThis允许创建文件和注册表备份以便当发生问题时可以恢复系统。

一个常见的做法是把日志文件发布在[电子公告牌上](../Page/电子公告牌.md "wikilink")，这样以方便高级用户检视并提供移除恶意软件建议。

HijackThis的后继版本将包括更多的工具，如[任务管理器](../Page/任务管理器.md "wikilink")、[hosts文件编辑器以及](../Page/hosts文件.md "wikilink")[alternate
data stream扫描工具](../Page/alternate_data_stream.md "wikilink")。

  -
    于2007年3月12日，Merijn在其首页中指出其HijackThis已經出售予[趋势科技](../Page/趋势科技.md "wikilink")（Trend
    Micro），以后HJT由趋势科技负责维护。


\== 系統需求 == **作業系統**
Microsoft Windows™ XP
Microsoft Windows™ 2000
Microsoft Windows™ Me
Microsoft Windows™ 98
Microsoft Windows™ Vista
Microsoft Windows™ 7

**瀏覽器**
Microsoft Internet Explorer 6.0 (或更新版本)
Mozilla Firefox™ 1.5 (或更新版本)

\== 参见 ==

  - [System Repair
    Engineer](../Page/System_Repair_Engineer.md "wikilink")


\== 外部链接 ==

  - [Trend Micro Hijack
    This（官方网站）](http://free.antivirus.com/hijackthis/)
  - [Hijack This 意見回覆](http://free.antivirus.com/feedback/)

<!-- end list -->

  - [自动日志文件分析程序 - HijackThis.de Security](http://www.hijackthis.de/)
  - [自动日志文件分析程序 - I Am Not A Geek](http://hjt.iamnotageek.com/)



[Category:反间谍软件](../Category/反间谍软件.md "wikilink")
[Category:免費軟件](../Category/免費軟件.md "wikilink")