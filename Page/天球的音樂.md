**天球的音樂**是[牧野由依的第一張專輯](../Page/牧野由依.md "wikilink")。2006年12月6日由victor
entertainment發售。初回特典收錄24集的寫真集「TAKE ME...」。

## 收錄曲

1.    - 作詞：Gabriela Robin、作曲：[菅野洋子](../Page/菅野洋子.md "wikilink")、編曲：菅野洋子

    <!-- end list -->

      -
        keyboards:菅野洋子
        programming:浦田恵司、坂本俊介
        backing vocal:[牧野由依](../Page/牧野由依.md "wikilink")
        recording & mixing engineer:薮原正史
        recorded &　mixed at VICTOR AOYAMA STUDIO

    <!-- end list -->

      - 《[-{zh-cn:创圣的大天使;zh-tw:創聖機械天使;
        zh-hk:創聖大天使;}-](../Page/創聖的亞庫艾里翁.md "wikilink")》（）片尾曲

2.    - 作詞：[河井英里](../Page/河井英里.md "wikilink")、作曲：[窪田ミナ](../Page/窪田ミナ.md "wikilink")、編曲：窪田ミナ

    <!-- end list -->

      -
        acoustic piano:窪田ミナ
        programming:[吉田健二](../Page/吉田健二.md "wikilink")
        bouzouki & bandolim:田代耕一郎
        percussion:菅原裕紀
        strings:杉野裕ストリングス
        backing vocal:牧野由依
        recording & mixing engineer:花島攻武
        recorded &　mixed at EPICRUS STUDIO

    <!-- end list -->

      - 《[ARIA The
        ANIMATION](../Page/ARIA_The_ANIMATION.md "wikilink")》片頭曲

3.  1.  作詞：[牧野由依](../Page/牧野由依.md "wikilink")、作曲：牧野由依、編曲：[河野伸](../Page/河野伸.md "wikilink")

    <!-- end list -->

      -
        wurlitzer & backing vocal:牧野由依
        electric bass:河野伸
        electric & acoustic guitar:[古川昌義](../Page/古川昌義.md "wikilink")
        tenor sax:[山本拓夫](../Page/山本拓夫.md "wikilink")
        flugel horn:西村浩二
        handclapping:ゆいとゆかいな仲間たち
        recording & mixing engineer:廣瀬修
        recorded at SUNRISE STUDIO,SUNRISE STUDIO TOWERSIDE
        mixed at SUNRISE STUDIO

4.    - 作詞：[佐々倉有吾](../Page/佐々倉有吾.md "wikilink")、作曲：佐々倉有吾、編曲：島田昌典

    <!-- end list -->

      -
        acoustic piano,hammond organ,programing & tambourine:島田昌典
        drums:佐野康夫
        electric bass:スティング宮本
        electric & acoustic guitar:狩野良昭
        strings:[弦一徹ストリングス](../Page/弦一徹ストリングス.md "wikilink")
        backing vocal:牧野由依
        recording & mixing engineer:茨木直樹
        record at BS & T STUDIO,MIT STUDIO
        mixed at MIT STUDIO

    <!-- end list -->

      - 《[歡迎來到N·H·K！](../Page/歡迎來到NHK！.md "wikilink")》（）片尾曲

5.    - 作詞：[伊藤利恵子](../Page/ROUND_TABLE.md "wikilink")、作曲：[北川勝利](../Page/ROUND_TABLE.md "wikilink")、編曲：桜井康史

    <!-- end list -->

      -
        acoustic piano & backing vocal:牧野由依
        programming:桜井康史
        drums:宮田繁男
        electric bass & tambourine:北川勝利
        electric & acoustic guitar:稲葉政裕
        strings:金原千恵子グループ
        recording & mixing engineer:薮原正史
        record at SUNRISE STUDIO TOWERSIDE
        mixed at MIT STUDIO

    <!-- end list -->

      - 《[ARIA The
        ANIMATION](../Page/ARIA_The_ANIMATION.md "wikilink")》插入曲

6.    - 作詞：河井英里、作曲：窪田ミナ、編曲：窪田ミナ

    <!-- end list -->

      -
        acoustic piano & programming:窪田ミナ
        acoustic guitar & ukulele:松宮幹彦
        percussion:[岡部洋一](../Page/岡部洋一.md "wikilink")
        strings:杉野裕ストリングス
        backing vocal:牧野由依
        recording & mixing engineer:花島功武
        record at CRESCENT STUDIO,MIT STUDIO
        mixed at MIT STUDIO

    <!-- end list -->

      - 《[ARIA The NATURAL](../Page/ARIA_The_NATURAL.md "wikilink")》片頭曲

7.    - 作詞：田口俊、作曲：SONIC DOVE、編曲：SONIC DOVE

    <!-- end list -->

      -
        acoustic piano & backing vocal:SONIC DOVE
        electric & acoustic guitar:[鈴木俊介](../Page/鈴木俊介.md "wikilink")
        backing vocal:牧野由依
        recording & mixing engineer:北城浩志
        record at WESTSIDE STUDIO
        mixed at HAKASE STUDIO

8.    - 作詞：西直樹、作曲：村下雅俊、編曲：河野伸

    <!-- end list -->

      -
        acoustic piano & programming:河野伸
        drums:波多江健
        electric bass:沢田浩史
        electric & acoustic guitar:[古川昌義](../Page/古川昌義.md "wikilink")
        tenor sax:[山本拓夫](../Page/山本拓夫.md "wikilink")
        trumpet:西村浩二
        strings:[金原千恵子ストリングス](../Page/金原千恵子ストリングス.md "wikilink")
        backing vocal:牧野由依
        recording & mixing engineer:廣瀬修
        record at SUNRISE STUDIO,SUNRISE STUDIO TOWERSIDE
        mixed at SUNRISE STUDIO

9.    - 作詞：高橋舞、作曲：F.GIRAUD、編曲：窪田ミナ

    <!-- end list -->

      -
        acoustic piano & programming:窪田ミナ
        acoustic piano:遠山哲郎
        percussion:菅原裕紀
        flute:赤木りえ
        strings:杉野裕ストリングス
        backing vocal:牧野由依
        recording & mixing engineer:花島功武
        record at CRESCENTE STUDIO,MIT STUDIO
        mixed at MIT STUDIO

    <!-- end list -->

      - 《[ARIA The NATURAL](../Page/ARIA_The_NATURAL.md "wikilink")》插入曲

10.   - 作詞：[梶浦由記](../Page/梶浦由記.md "wikilink")、作曲：梶浦由記、編曲：梶浦由記

    <!-- end list -->

      -
        keyboards & programming:梶浦由記
        drums:[野崎真助](../Page/野崎真助.md "wikilink")
        electric bass:スティング宮本
        electric guitar:[西川進](../Page/西川進.md "wikilink")
        electric piano:松田真人
        backing vocal:貝田由里子
        recording & mixing engineer:山田直樹
        record at WARNER MUSIC RECORDING STUDIO
        mixed at PRIME SOUND STUDIO FORM

    <!-- end list -->

      - 《[TSUBASA翼](../Page/TSUBASA翼.md "wikilink")》（）插入曲

11.   - 作詞：牧野由依、作曲：[五島良子](../Page/五島良子.md "wikilink")、編曲：吉澤瑛師

    <!-- end list -->

      -
        keyboads & programming:吉澤瑛師
        electric & acoustic guitar:伊丹雅博
        strings:伊藤加奈子ストリングス
        backing vocal:五島良子、牧野由依
        recording & mixing engineer:森田聖司
        record at SUNRISE STUDIO TOWERSIDE, MIT STUDIO
        mixed at MIT STUDIO

    <!-- end list -->

      - 《[TSUBASA翼](../Page/TSUBASA翼.md "wikilink")》（ツバサ・クロニクル）小櫻的登場人物角色歌曲

12. CESTREE

      - 作詞：[かの香織](../Page/かの香織.md "wikilink")、作曲：かの香織、編曲：かの香織

    <!-- end list -->

      -
        acoustic piano & programming:かの香織
        electric bass:松永孝義
        electric guitar:[窪田晴男](../Page/パール兄弟.md "wikilink")
        backing vocal:牧野由依
        recording & mixing engineer:中山佳敬
        record at VICTOR AOYAMA STUDIO
        mixed at MIT STUDIO

    <!-- end list -->

      - 《[Zegapain](../Page/Zegapain.md "wikilink")》（）插入曲

13.   - 作詞：牧野由依、作曲：F.GIRAUD、編曲：河野伸

    <!-- end list -->

      -
        rhodes & programming:河野伸
        drums:江口信夫
        electric bass:[種子田健](../Page/種子田健.md "wikilink")
        electric & acoustic guitar:石成正人
        strings:金原千恵子ストリングス
        backing vocal:牧野由依
        recording & mixing engineer:廣瀬修
        recorded &　mixed at MIT STUDIO

    <!-- end list -->

      - 《[ARIA The
        NATURAL](../Page/ARIA_The_NATURAL.md "wikilink")》第17話插入曲

14.   - 作詞：かの香織、編曲：かの香織、編曲：藤田哲司、ストリングス編曲：河野伸

    <!-- end list -->

      -
        programming:藤田哲司
        electric guitar:石成正人
        electric piano:河野伸
        strings:弦一徹ストリングス
        backing vocal:牧野由依
        recording & mixing engineer:薮原正史
        record at SUNRISE STUDIO TOWESIDE
        mixed at MIT STUDIO

    <!-- end list -->

      - 《[TSUBASA翼劇場版
        鳥籠國的公主](../Page/TSUBASA翼劇場版_鳥籠國的公主.md "wikilink")》（）主題曲

[Category:2006年音樂專輯](../Category/2006年音樂專輯.md "wikilink")
[Category:日本配音員音樂專輯](../Category/日本配音員音樂專輯.md "wikilink")