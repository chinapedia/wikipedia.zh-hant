[Schleiden.JPG](https://zh.wikipedia.org/wiki/File:Schleiden.JPG "fig:Schleiden.JPG")

**馬蒂亞斯·雅各布·許萊登**（，）是一位[德國](../Page/德國.md "wikilink")[植物學家](../Page/植物學家.md "wikilink")，[細胞學說的建立者之一](../Page/細胞學說.md "wikilink")。他出生於德國[漢堡](../Page/漢堡.md "wikilink")，大學時原本研讀法律，但後來轉向其興趣[植物學](../Page/植物學.md "wikilink")。

他喜歡使用[顯微鏡來觀察植物的結構](../Page/顯微鏡.md "wikilink")，他在擔任[耶拿大學教授時](../Page/耶拿大學.md "wikilink")，紀錄了植物不同部位是由細胞所構成的現象。後來他也辨識出1831年時由植物學家[羅伯特·布朗所發現的](../Page/羅伯特·布朗.md "wikilink")[細胞核](../Page/細胞核.md "wikilink")\[1\]。

## 參考文獻

[Category:德國植物學家](../Category/德國植物學家.md "wikilink")
[Category:科學插畫家](../Category/科學插畫家.md "wikilink")
[Category:耶拿大學教師](../Category/耶拿大學教師.md "wikilink")
[Category:海德堡大學校友](../Category/海德堡大學校友.md "wikilink")
[Category:漢堡人](../Category/漢堡人.md "wikilink")

1.