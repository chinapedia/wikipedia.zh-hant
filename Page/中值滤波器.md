在[图像处理中](../Page/图像处理.md "wikilink")，在进行如[边缘检测这样的进一步处理之前](../Page/边缘检测.md "wikilink")，通常需要首先进行一定程度的[降噪](../Page/降噪.md "wikilink")。**中值滤波**是一种非线性[数字滤波器技术](../Page/数字滤波器.md "wikilink")，经常用于去除图像或者其它信号中的[雜訊](../Page/雜訊_\(通訊學\).md "wikilink")。这个设计思想就是检查输入信号中的采样并判断它是否代表了信号，使用奇数个采样组成的观察窗实现这项功能。观察窗口中的数值进行排序，位于观察窗中间的[中值作为输出](../Page/中值.md "wikilink")。然后，丢弃最早的值，取得新的采样，重复上面的计算过程。

中值滤波是[图像处理中的一个常用步骤](../Page/图像处理.md "wikilink")，它对于[斑点噪声和](../Page/斑点噪声.md "wikilink")[椒盐噪声来说尤其有用](../Page/椒盐噪声.md "wikilink")。保存边缘的特性使它在不希望出现边缘模糊的场合也很有用。

## 例子

为了演示中值滤波器的工作过程，我们给下面的数组加上观察窗 3 ，重复边界的数值：

x = \[2 80 6 3\]

y\[1\] = Median\[2 2 80\] = 2
y\[2\] = Median\[2 80 6\] = Median\[2 6 80\] = 6
y\[3\] = Median\[80 6 3\] = Median\[3 6 80\] = 6
y\[4\] = Median\[6 3 3\] = Median\[3 3 6\] = 3
于是
y = \[2 6 6 3\]

其中 y 是 x 的中值滤波输出。

## 在GIMP中进行中值滤波

在GIMP 2.8.2中操作如下：

滤镜-\>增强-\>去除斑点

**不要勾选**适应和递归，设置黑色水平为-1，白色水平为256。\[1\]根据需要调节半径。半径越大，细节越少。

|                                                                                                                              |                                                                                                                                                             |
| ---------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [Noise_salt_and_pepper.png](https://zh.wikipedia.org/wiki/File:Noise_salt_and_pepper.png "fig:Noise_salt_and_pepper.png") | [Median_filtered_salt_and_pepper.png](https://zh.wikipedia.org/wiki/File:Median_filtered_salt_and_pepper.png "fig:Median_filtered_salt_and_pepper.png") |

## 参考

## 参见

  - [噪声](../Page/噪声.md "wikilink")
  - [中值](../Page/中值.md "wikilink")
  - [信号处理](../Page/信号处理.md "wikilink")
  - [数字图像处理](../Page/数字图像处理.md "wikilink")

## 外部链接

  - [中值滤波器](https://web.archive.org/web/20060711221219/http://www.cee.hw.ac.uk/hipr/html/median.html)
  - [快速二维中值滤波器](http://www.shellandslate.com/fastmedian.html)

<references/>

[Category:非线性滤波器](../Category/非线性滤波器.md "wikilink")
[Category:信号处理](../Category/信号处理.md "wikilink")
[Category:图像处理](../Category/图像处理.md "wikilink")

1.