**巴特纳查巴伯奧雷斯足球俱乐部**（简称：**CA
Batna**,[法语](../Page/法语.md "wikilink")：**Chabab Aurès de
Batna**,[阿拉伯语](../Page/阿拉伯语.md "wikilink")：شباب أوريه
باتنة）是位于[阿尔及利亚](../Page/阿尔及利亚.md "wikilink")[巴特纳的足球俱乐部](../Page/巴特纳.md "wikilink")，球队创立于1932年。俱乐部颜色为红色和蓝色。

球队的主体育场为可以容纳30,000名球迷的塞弗赫体育场（Seffouhi
Stadium），球队在2005年从第二档的乙级联赛升入阿尔及利亚国内顶级联赛的国家锦标赛。

## 球队荣誉

  - **[阿尔及利亚杯](../Page/阿尔及利亚杯.md "wikilink")**
      - 亚军：1997年

## 外部链接

  - [Site de
    fan](https://web.archive.org/web/20070311022459/http://cab.rf.lv/)

[Category:阿尔及利亚足球俱乐部](../Category/阿尔及利亚足球俱乐部.md "wikilink")