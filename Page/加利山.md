**加利山**為[台灣知名山峰](../Page/台灣.md "wikilink")，也是[台灣百岳之一](../Page/台灣百岳.md "wikilink")，排名第86。加利山高達3,112公尺，屬於[雪山山脈](../Page/雪山山脈.md "wikilink")，[行政區劃屬於](../Page/台灣行政區劃.md "wikilink")[苗栗縣](../Page/苗栗縣.md "wikilink")。

## 參考文獻

  - 楊建夫，《台灣的山脈》，2001年，臺北，遠足文化公司

## 相關條目

  - [雪霸國家公園](../Page/雪霸國家公園.md "wikilink")
  - [大霸尖山](../Page/大霸尖山.md "wikilink")
  - [聖稜線](../Page/聖稜線.md "wikilink")
  - [登山](../Page/登山.md "wikilink")

## 外部連結

  - [大霸/聖稜線段](http://trail.forest.gov.tw/NationalTrailSystem/TR_G_01.htm)
    - 國家步道系統
  - [中華民國山岳協會](https://web.archive.org/web/20070414235108/http://www.mountaineering.org.tw/index.asp)
    - 最初為「台灣山岳會」
  - [雪霸聖稜線](https://web.archive.org/web/20070927193452/http://www.beautymountain88.com.tw/snownmountainj/syuebanationparkj1-2.htm)
  - [聖稜線](http://www.ntut.edu.tw/~s8370037/%B3%B7-%B8t%B3%AE.htm) -
    [聖稜線步道稜脈圖](http://www.ntut.edu.tw/~s8370037/images/%B8t%B3%AEmap-hl.gif)
  - [聖稜線](https://web.archive.org/web/20070926212836/http://csm01.csu.edu.tw/0150/49002134/index-12.htm)
    - 連峰照片
  - [雪山山脈的聖稜、南稜與西南稜線](https://web.archive.org/web/20070926212842/http://csm01.csu.edu.tw/0150/49002134/index-11-1.htm)
    - 連峰照片
  - [山情點滴─山與人的故事](http://www.tces.chc.edu.tw/alan/taiwan/g/g1/g16.htm)

[Category:苗栗縣山峰](../Category/苗栗縣山峰.md "wikilink")
[Category:台灣百岳](../Category/台灣百岳.md "wikilink")
[Category:雪霸國家公園](../Category/雪霸國家公園.md "wikilink")