**百子蓮屬**（[学名](../Page/学名.md "wikilink")：**）是一個生長在[南非的](../Page/南非.md "wikilink")[屬](../Page/屬_\(生物\).md "wikilink")，長青或落葉多年生草本植物。它有大約10個不同的種類。

雷頓（Leighton）在1965年發表的文獻裡，將百子蓮屬分類為下列十個種：百子蓮、鈴花百子蓮、具莖百子蓮、蔻第百子蓮、德拉肯斯堡百子蓮、早花百子蓮、康普頓百子蓮、戴爾百子蓮、垂花百子蓮、瓦許百子蓮。宗納維爾與鄧肯（Zonneveld
and
Duncan）在2003年發表的研究報告裡，將上述十種百子蓮屬植物重新分類，只留下最前面的六個種（百子蓮～早花百子蓮），而將其餘的四個種（康普頓百子蓮～瓦許百子蓮）改分類為[亞種或同種異名](../Page/亞種.md "wikilink")。

先前的分類系統將百子蓮屬分類為[石蒜科或](../Page/石蒜科.md "wikilink")[蔥科的一個屬](../Page/蔥科.md "wikilink")，最近則有人認為應將百子蓮屬分類為[百子蓮科的一個屬](../Page/百子蓮科.md "wikilink")，而百子蓮科內只有百子蓮屬一個屬。（參見：*Indices
Nominum Supragenericorum Plantarum Vascularium*）

百子蓮屬植物的花序為聚繖花序，[花漏斗形](../Page/花.md "wikilink")，藍色，著生於花莖頂端；花莖直立，可以生長至一[公尺長](../Page/公尺.md "wikilink")。[葉基生](../Page/葉.md "wikilink")，披針形，長度可達六十[公分左右](../Page/公分.md "wikilink")。

非洲類百子蓮在美國第7至第11植物生長區域也能夠順利生長。([USDA plant hardiness
zones](../Page/USDA_plant_hardiness_zones.md "wikilink") 7 through 11).
在低數字的美國植物生長區域種植，需要將根部植入更深的土壤裏，土壤亦需要更濕潤和足夠的腐土。百子蓮屬植物可以用分球繁殖或是用種子繁殖。

## 種類

[Agapanthus_Prebloom.jpg](https://zh.wikipedia.org/wiki/File:Agapanthus_Prebloom.jpg "fig:Agapanthus_Prebloom.jpg")

  - [百子蓮](../Page/百子蓮.md "wikilink")（*Agapanthus africanus*）
  - [鈴花百子蓮](../Page/鈴花百子蓮.md "wikilink")（*Agapanthus campanulatus*）
  - [具莖百子蓮](../Page/具莖百子蓮.md "wikilink")（*Agapanthus caulescens*）
  - [蔻第百子蓮](../Page/蔻第百子蓮.md "wikilink")（*Agapanthus coddii*）
  - [康普頓百子蓮](../Page/康普頓百子蓮.md "wikilink")（*Agapanthus comptonii*）
  - [戴爾百子蓮](../Page/戴爾百子蓮.md "wikilink")（*Agapanthus dyeri*）
  - [德拉肯斯堡百子蓮](../Page/德拉肯斯堡百子蓮.md "wikilink")（*Agapanthus inapertus*）
  - [垂花百子蓮](../Page/垂花百子蓮.md "wikilink")（*Agapanthus nutans*）
  - [早花百子蓮](../Page/早花百子蓮.md "wikilink")（*Agapanthus praecox*）
  - [瓦許百子蓮](../Page/瓦許百子蓮.md "wikilink")（*Agapanthus walshii*）

[File:agapantha_closeup.jpg|百子蓮的花](File:agapantha_closeup.jpg%7C百子蓮的花)
- 特寫 [File:agapantha_pot.jpg|百子蓮](File:agapantha_pot.jpg%7C百子蓮) - 盤栽
[File:aga20_blossom.jpeg|百子蓮花](File:aga20_blossom.jpeg%7C百子蓮花)
[File:aga20_blossom2.jpeg|百子蓮花](File:aga20_blossom2.jpeg%7C百子蓮花) (側面)
[File:aga20_blossomtop.jpeg|百子蓮花](File:aga20_blossomtop.jpeg%7C百子蓮花)
(鳥瞰) [File:aga20_shoot.jpeg|百子蓮花莖](File:aga20_shoot.jpeg%7C百子蓮花莖)

[Category:百子莲属](../Category/百子莲属.md "wikilink")
[Category:花卉](../Category/花卉.md "wikilink")