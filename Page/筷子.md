**筷子**是一種起源於[古中國的食具](../Page/古中國.md "wikilink")，古漢語稱箸、或梜提\[1\]。用於将食物夾起並送入口中。在[東亞](../Page/東亞.md "wikilink")、[東南亞地區](../Page/東南亞.md "wikilink")（[越南](../Page/越南.md "wikilink")、[寮國](../Page/寮國.md "wikilink")、[柬埔寨](../Page/柬埔寨.md "wikilink")、[泰國](../Page/泰國.md "wikilink")、[緬甸](../Page/緬甸.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")）被作為主要餐具，而受傳統中華文化影響的[新疆](../Page/新疆.md "wikilink")、[西藏及部分](../Page/西藏.md "wikilink")[蒙古的少數民族也有廣泛使用](../Page/蒙古.md "wikilink")，以及世界各地的東亞裔移民群體所遍布的國家中常見。

## 歷史

人類使用筷子的歷史可以追溯到四千年以前，筷子是何人發明，何時誕生，歷代雖有很多傳說，但至今仍無得而知。中國人從古至今都視筷子為自然沿襲下來的家常用品，對其的由來和發展只有少量的記載。由考古學提供的證據而言，筷子的出現晚於[匙羹](../Page/匙.md "wikilink")，同樣原因，由於人們想做某種工具以方便取得燙熱的熟食，所以筷子的發明跟原始農業和陶器的運用和發展有著直接關係。《韓非子·喻志》中古筷稱**櫡**。從這個字從木從竹來看，可以證明中國最早的筷子多為木質筷和竹筷。**櫡**的簡写為**箸**。因為古代最早的烹飪方法，肉類多用烤的方式，菜則多用煮的；筷子最早是從煮沸的湯羹中撈菜食用，所以古筷名**箸**實在非常貼切\[2\]。筷子有[竹製](../Page/竹.md "wikilink")，亦有[木頭](../Page/木頭.md "wikilink")、[金属](../Page/金属.md "wikilink")、[象牙與](../Page/象牙.md "wikilink")[塑料等材质](../Page/塑料.md "wikilink")。

  - 《韩非子·喻老》曾記載：“昔者纣为象箸，而箕子怖。”可見當時商朝末期已經有用象牙製品來做食具。
  - 《[禮記](../Page/禮記.md "wikilink")·曲禮上》也有「飯黍母以箸」和「羹之有菜者用梜」的記載。
  - 筷子有多种名称，先秦时期称“挟”，也作“荚”。郑玄注释：“挟，犹箸也，今人谓箸为挟提。”
  - 汉代[司马迁](../Page/司马迁.md "wikilink")《史记》时，称商纣时期的筷子为“箸”，古写为“木箸”。两汉又出现了“筯”字。
  - 在隋唐時期，人們流行將飯菜擺在桌子，坐在椅凳上來享用，一般都用筷子夾菜，用羹子勺飯。當時民間用[銀制筷子很普遍](../Page/銀.md "wikilink")，[宮廷多用](../Page/宮廷.md "wikilink")[金製的](../Page/金.md "wikilink")。筷子從古代就流传至鄰近国家，當今已成为[東亞和](../Page/東亞.md "wikilink")[東南亞多個民族常用的飲食工具](../Page/東南亞.md "wikilink")。
  - 隋唐时李白《行路难》诗曰：“停杯投筯不能食”；杜甫《丽人行》诗云：“犀箸厌饮久来下，鍪刀镂切空纷纶。”。
  - [明朝](../Page/明朝.md "wikilink")[陸容的](../Page/陸容.md "wikilink")《[菽園雜記](../Page/s:菽園雜記.md "wikilink")》說：「起於吳中。凡舟行諱住諱翻，故呼箸為快子」（因为吴中船民和渔民特别忌讳“箸”，他们最怕船“住”，船停住了，行船者也就没生意，他们更怕船“蛀”，木船“蛀”了漏水如何捕鱼。在这种迷信谐音的思想指导下，故见了“箸”反其道叫“快子”，以图吉利。）。
  - [明朝](../Page/明朝.md "wikilink")[李豫亨在](../Page/李豫亨.md "wikilink")《推蓬寤语》中说得更明白：“世有讳恶字而呼为美字者，如立箸讳滞，呼为快子。今因流传已久，至有士大夫间亦呼箸为快子者，忘其始也。”
  - 《[康熙字典](../Page/康熙字典.md "wikilink")》中仅收录“箸”而不承认“筷”字。
  - 曹雪芹《红楼梦》四十回，在贾母宴请刘姥姥一段中曹雪芹三处称“箸”，两次呼“筯”，而四次直接写明“筷子”。

## 各地唸法

  - [閩南語泉漳片地區](../Page/泉漳話.md "wikilink")、仍稱筷子為箸，唸法各地有差異，老泉腔唸\[tīr\]（[IPA](../Page/IPA.md "wikilink")：\[tɨ˨˨\]或\[tɯ˨˨\]）；新泉腔唸\[tū\]（[IPA](../Page/IPA.md "wikilink")：\[tu˧˧\]）；漳腔唸\[tī\]（[IPA](../Page/IPA.md "wikilink")：\[ti˧˧\]）。
  - [福州話地區](../Page/福州話.md "wikilink")、仍稱筷子為箸，唸法的拼音為\[tɛu(242)\]。
  - [廣東省](../Page/廣東省.md "wikilink")[和平縣](../Page/和平縣.md "wikilink")、仍稱筷子為箸，唸法的拼音為\[chī\]。
  - [廣東省](../Page/廣東省.md "wikilink")[客家地區](../Page/客家地區.md "wikilink")、仍稱筷子為箸，唸法的拼音為\[chu\]。
  - [廣東省](../Page/廣東省.md "wikilink")[潮汕地區](../Page/潮汕地區.md "wikilink")、仍稱筷子為箸，唸法的拼音為\[du\]。
  - [溫州市区的當地人](../Page/溫州.md "wikilink")、仍稱筷子為箸 ，又叫梜，唸法的拼音為\[dzei\]。

## 各國特色

[Many-chopsticks.jpg](https://zh.wikipedia.org/wiki/File:Many-chopsticks.jpg "fig:Many-chopsticks.jpg")
[漢字文化圈一直保持使用筷子的傳統](../Page/漢字文化圈.md "wikilink")，但各地的筷子也具備當地特色，表現出不同的文化特徵。筷子除了用來吃飯，在民間的婚、喪、喜慶等禮俗都有廣泛運用。

[中国筷子的形狀大多为近似的](../Page/中国.md "wikilink")[长方体或](../Page/长方体.md "wikilink")[圆柱体](../Page/圆柱体.md "wikilink")，或头圆尾方，只在头部略细与尾部分別，近年尖头筷子也流行起来。由于竹的生长比较快和广泛，竹筷的使用率很高，也有用传统的红木和象牙等名贵材料制作的筷子。
[Chopsticks.jpg](https://zh.wikipedia.org/wiki/File:Chopsticks.jpg "fig:Chopsticks.jpg")
[Korean_Metal_Chopsticks.png](https://zh.wikipedia.org/wiki/File:Korean_Metal_Chopsticks.png "fig:Korean_Metal_Chopsticks.png")
[Black_and_Gold_Chopsticks_-_The_Bund.jpg](https://zh.wikipedia.org/wiki/File:Black_and_Gold_Chopsticks_-_The_Bund.jpg "fig:Black_and_Gold_Chopsticks_-_The_Bund.jpg")
[日本繼承中國的固有制筷工藝](../Page/日本.md "wikilink")，又別出心裁地將其本土化。日本流行極尖头的筷子，由於海島環境，通常都是[木質制作](../Page/木.md "wikilink")，方便夾取魚刺，同時款式十分繁多，也有包[漆的](../Page/漆.md "wikilink")，名称沿用中国古语「箸」（[訓讀](../Page/訓讀.md "wikilink")、*hashi*），也有特别为烹调用的巨型长筷“”，台灣也常見。

[朝鲜半岛是最早引入筷子文化的東亞區域](../Page/朝鲜半岛.md "wikilink")，約有一千多年，他們把餐具的功能分得較細，吃飯用匙、夾菜用筷，视端起饭碗吃饭的习惯是不正规的行为（他們認為這是乞丐討飯才會做的事），而且也不能用嘴接触饭碗。因为战乱关系，为使筷子更耐用，原來的木製扁筷改為不銹鋼，既合乎當地飲食又方便清洗使用。傳統韓國的房屋結構，廚房只是做菜的地方，沒有空間在此吃飯，習慣將食物、碗筷放在矮桌再端到各自的房間用餐，故設計為搬動時不易滾動的扁筷。所以他们的筷子都是扁平的两片[金属製成](../Page/金属.md "wikilink")，称之「」。語源為[漢字](../Page/漢字.md "wikilink")「箸」發音「」（*jeo*）加上固有語「」（*garak*，意為條棒）而成。加上「」是因為[中世朝鮮語時標準語中發生了](../Page/中世朝鮮語.md "wikilink")「」與「」的合流。在某些朝鮮語方言中筷子的名稱仍然是單音節的「」。

[越南很早就使用筷子](../Page/越南.md "wikilink")，雖然曾被法國長期殖民統治，受過西方文化的衝擊，但依然保持用筷子的傳統，是東南亞少數用筷子的國家。越南筷整體平直沒有粗細之分，對筷子的名稱“đũa”源自漢語的「箸」（[中古音](../Page/中古漢語.md "wikilink")
*ɖɨʌ*H, *ʈɨʌ*H），但不是標準[漢越音](../Page/漢越音.md "wikilink")，而是古漢越音。

[琉球也使用筷子](../Page/琉球.md "wikilink")，他們的筷子形狀與中式筷子相似，通常為竹製，但上面部份會塗上紅漆，代表太陽，下方則會塗上黃漆，代表月亮，也有殺菌作用。[琉球語把筷子稱為](../Page/琉球語.md "wikilink")「」。[琉球人使用與日本本土形制不同的傳統筷子](../Page/琉球人.md "wikilink")，更与中国的相似。

### 傳播

較早把筷子介紹到歐洲的是義大利人[利瑪竇](../Page/利瑪竇.md "wikilink")，在他的著作《[中國札記](../Page/中國札記.md "wikilink")》一書也有描述到筷子文化和用法。隨著華人不斷地移居到歐美國家，西方對筷子也不陌生，很多家庭都備有筷子。法國旅遊協會制定了一項「金筷獎」以表彰出色的中餐行業。

## 使用的礼节

[Chopsticks_usage.png](https://zh.wikipedia.org/wiki/File:Chopsticks_usage.png "fig:Chopsticks_usage.png")

  - 夹起食物之后，不应该放回盘碟。
  - 不能用筷子對著人或用餐時拿筷子指手畫腳。
  - 不能将筷子插入一碗米或飯。这是祖先奉献物安置方法，參見[腳尾飯](../Page/腳尾飯.md "wikilink")。
  - 不要用筷子敲打碗盆。逢到家里请客吃饭时，尤其不可将筷子胡乱地敲打碗盆。
  - 不与他人的筷子相争。
  - 不能顛倒使用筷子。
  - 不該旋轉（戲耍）筷子。
  - 赴宴的时候，不应提前于主人喊「起筷」前动筷子（[汤匙亦然](../Page/汤匙.md "wikilink")），之後要等客人或在座最年長者動作才開始吃飯。
  - 在用餐过程中，已经举起筷子，但不知道该吃哪道菜，这时不可将筷子在各碟菜中来回移动或在空中游弋，在菜盘上来回的转而却又不夹菜。
  - 与其他就餐者从同一食物容器取菜时，不应用筷子在其中翻动寻找，而应提前选好要取用的区域后再夹起。
  - 在日本，使用筷子必須要用雙手放至筷架上或是其它可以放至的地方。

## 演变

  - 在中國，筷子相当于手指的延伸。经过不断的改进，筷子才成为今天的形状——夹食物的那段是圆柱形，另一端是四方形，上端粗下端细。
  - 在韓國，傳統以小木桌盛飯食，圓筷易滾動，遂演變為扁平的筷子，不易於托運時滾落。

## 公筷

[HK_Vegetarian_Poonchoi.JPG](https://zh.wikipedia.org/wiki/File:HK_Vegetarian_Poonchoi.JPG "fig:HK_Vegetarian_Poonchoi.JPG")
中式合菜為多人同桌，共用幾盤菜餚，如果大家都用自己的筷子夾取食物，[口水容易沾染](../Page/口水.md "wikilink")，或許會有衛生上的問題。為了提倡飲食衛生、避免疾病傳染，現在部分的家庭或餐廳，除了個人的碗筷外，每一道菜都會另外備有一雙公共的筷子，稱為公筷。

[台灣早年一項政策](../Page/台灣.md "wikilink")，為防治幾乎百分之百盛行率的[A型肝炎而致力推行公筷母匙與免洗餐具](../Page/A型肝炎.md "wikilink")，儘管公筷母匙只能防治死亡率極低的[A型肝炎](../Page/A型肝炎.md "wikilink")，對後來盛行的[B型肝炎毫無助益](../Page/B型肝炎.md "wikilink")，但將A肝傳染降至極低水準。惟在大量使用[保麗龍](../Page/保麗龍.md "wikilink")、塑膠製品後，所造成的環境破壞，正努力著手補救。目前政策已更正為停用免洗餐具（一次性餐具），改用一般餐具，並獎勵自備餐具。至於[自助餐本來就使用公筷母匙](../Page/自助餐.md "wikilink")，而未受影響

尤其在2003年[SARS疫症後](../Page/SARS.md "wikilink")，[香港衞生署更播放了一齣](../Page/香港衞生署.md "wikilink")[公益廣告](../Page/公益廣告.md "wikilink")[「食得衞生、活得健康」](http://www.isd.gov.hk/chi/tvapi/teamcleanc-lai.html)，提倡挑選乾淨的餐廳進餐和使用公筷兩項衞生飲食觀念。

[香港](../Page/香港.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")、[台湾和](../Page/台湾.md "wikilink")[日本等国家和地區](../Page/日本.md "wikilink")，民眾合餐有普遍使用公筷的習慣。但是[華人在餐厅与友人聚餐时](../Page/華人.md "wikilink")，常会使用公筷；而親人聚餐时，大多不用公筷。

## 環保筷

為了方便及節省成本等理由，過往的食肆為食客提供一次性的[便利筷](../Page/便利筷.md "wikilink")。但是，其耗用量已經達到環保人士的關注，因此現在自己攜帶筷子的人多了。據估計，全球每年消耗800億對即棄木筷，[中國大陸及](../Page/中國大陸.md "wikilink")[日本分別佔](../Page/日本.md "wikilink")450億及250億對，佔整體近九成，台灣及香港亦是高用量地區，韓國則因為政府規定改用回收金屬箸。有文章認為要生產800億對筷子，須砍伐264萬棵樹\[3\]，但這數據有爭議性，因為除了品質較優之木筷，許多街頭攤販提供的是竹筷，竹子為極快速生長之植物，為永續性之材料。

便利筷除了對環境造成傷害外，在製作過程中，生產商會使用食品添加劑[亞硫酸鹽來漂白及防腐](../Page/亞硫酸鹽.md "wikilink")，有可能令筷子含有過量的二氧化硫，誘發用者出現[過敏](../Page/過敏.md "wikilink")、呼吸困難及[嘔吐等徵狀](../Page/嘔吐.md "wikilink")\[4\]。亦有不法商人，回收使用過的即棄木筷，經清洗漂白後，再包裝出售\[5\]。因此，基於環保及健康之考慮，環保團體積極鼓勵民眾自備[環保筷](../Page/環保筷.md "wikilink")。

[File:Wegwerf-Esstäbchen.jpg|垃圾桶裡大量廢棄的便利筷](File:Wegwerf-Esstäbchen.jpg%7C垃圾桶裡大量廢棄的便利筷)
<File:Reusable> metal chopsticks.JPG|滾珠定位式及鎖螺絲式的鐵製環保筷

## 筷子文化

### 中國

  - 筷子谜：《[魏书](../Page/魏书.md "wikilink")》中有一则筷子的谜语：“眠则俱眠，起则俱起，贪如豺狼，赃不入己。”
  - 筷子联：“玲珑自竹制来，古今饮誉神州萃；典雅由筷托出，中外扬名世界钦”。
  - 筷子德：筷子外形直而不弯，被古人寓以种种美满。据《开元天宝遗事》记载，[唐玄宗把自己用的一双金筷子赐给](../Page/唐玄宗.md "wikilink")[宰相](../Page/宰相.md "wikilink")[宋怀](../Page/宋怀.md "wikilink")，表彰他的耿直，以筷象征人格。
  - 筷子卜：用筷子筹划谋事，问卜吉凶在古代不胜枚举。如[汉代名将](../Page/汉代.md "wikilink")[张良曾](../Page/张良.md "wikilink")“借箸”为[刘邦筹划消灭](../Page/刘邦.md "wikilink")[项羽的战略](../Page/项羽.md "wikilink")；[韩凝礼用筷子预卜](../Page/韩凝礼.md "wikilink")[唐玄宗平叛](../Page/唐玄宗.md "wikilink")。
  - 筷子俚語：
      - 一根筷子易折斷，十根筷子難折斷：比喻團結力量大。
  - 筷子习俗：
      - [供奉亡靈饭](../Page/腳尾飯.md "wikilink")，筷子要竖在饭碗头上，以示恭敬，因为“筷”与“快”同音；與此同時，在平常吃飯時，長輩都會禁止小朋友把筷子插在飯碗或菜上，以免引起不好的聯想。
      - 苏北农村闹新房时，将整红筷子穿进窗户，还要唱几曲“穿筷歌”，以示庆贺和祝愿。

### 漢字文化圈

  - 筷子節：11月11日，由11形同一雙筷子故。[韓國](../Page/韓國.md "wikilink")[清州市在](../Page/清州市.md "wikilink")2015年11月10日舉辦首屆筷子節，該國「東亞文化之都」組委會宣布將11月11日定為筷子日\[6\]，第二屆筷子節同樣在[韓國](../Page/韓國.md "wikilink")[清州市舉辦](../Page/清州市.md "wikilink")。\[7\]
  - 日本筷子節：8月4日，由日語「箸」(はし，hashi)諧音「八」(ha)和「四」(shi)故。日本筷子節(箸の日)於1975年正式訂立，當日有感謝筷子之儀式(箸感謝祭)\[8\]等慶祝活動。

## 筷子形象應用

### 祝福之禮

  - 贈送筷子给搬新家的人，寓意快樂起家、吃食無虞。
  - 贈送筷子给新婚夫妻，諧音「快子」，祝早生貴子。
  - 贈送筷子给朋友、情侶、夫妻，因筷子成雙成對，寓意朋友友誼長存、相伴不離；情侶、夫妻長相廝守 、永結同心。
  - [韓國筷子常有](../Page/韓國.md "wikilink")「福」、「壽」或[鶴等吉祥的字或圖](../Page/鶴.md "wikilink")，故有贈送筷子表示祝福之禮。

### 舞蹈

在[蒙古有筷子舞](../Page/蒙古.md "wikilink")，舞者單手或雙手持著一雙或一束筷子，以筷子敲打身體不同部位或左右手筷子互擊敲出節奏感，伴隨音樂起舞。

### 武技表演

以筷子射擊（穿）木板、鋁鍋等物，成為一種武技的表演。

## 參閱

  - [便利筷](../Page/便利筷.md "wikilink")，用完即丟的筷子，又稱免洗筷、衛生筷、即棄筷、一次性筷子。
  - [羹子筷](../Page/羹子筷.md "wikilink")，筷子尖為[匙形](../Page/匙.md "wikilink")。
  - 银筷，古代皇帝用来测试饭食中是否有毒的筷子。

## 註釋

## 参考文獻

1.  戶倉恆信:
    [\<「筷子」在日本食膳中扮演的角色\>，台灣《自由時報》2014年10月9日](http://news.ltn.com.tw/news/opinion/paper/820120)
2.  科學出版社 劉雲 王仁湘 木芹《中國箸文化大觀》
3.  戶倉恆信：[〈筷子，如何擺放才妥當?〉，台灣《自由時報》2014年7月13日](http://news.ltn.com.tw/news/opinion/paper/795580)
4.  藍翔，《筷子，不只是筷子》，麥田，2011年。ISBN 9789861207179。
5.  戶倉恆信: [<生魚片與無垢箸>
    ，台灣《自由時報》2014年6月22日](http://news.ltn.com.tw/news/opinion/paper/789653)

## 外部連結

  - [Chinese Chopstick
    Etiquette](http://www.culture-4-travel.com/chopstick-etiquette.html)
  - [Japanese etiquette for chopsticks](http://wandco.com/?p=13)
  - [A gallery of chopstick images](http://www.ichizen.com/chopsticks/)
  - [Bring Your Own Chopsticks Movement Gains Traction in
    Asia](http://www.treehugger.com/files/2006/01/bring_your_own.php),
    article examining the link between chopsticks and deforestation
  - [How to use
    Chopsticks?](https://web.archive.org/web/20070323002329/http://www.makemysushi.com/chopstiks.html)
  - [Article about chopsticks from
    MrsLinsKitchen.com](http://www.mrslinskitchen.com/nlmar01.html),
    March 2001

[分類:飲食工具](../Page/分類:飲食工具.md "wikilink")

[Category:東亞傳統](../Category/東亞傳統.md "wikilink")

1.  《禮記》．〈曲禮上〉「羹之有菜者用梜，其無菜者不用梜」鄭玄．注：「梜，猶箸也。今人或謂箸為梜提。」
2.
3.  全球年耗800億雙免洗筷。香港明報，2008年8月3日。轉載自苦勞網。[1](http://www.coolloud.org.tw/node/24790)
4.  香港和民連鎖木筷轉膠筷
    年省30萬。香港明報，2008年8月4日。轉載自苦勞網。[2](http://www.coolloud.org.tw/node/24790)
5.  柳南工商查獲200多萬雙“問題筷”。新華網，2010年12月25日。[3](http://www.gx.xinhuanet.com/dtzx/2010-12/25/content_21728024.htm)
6.  [自由時報-這項生活用品……有望成為世界文化遺產](http://news.ltn.com.tw/news/world/breakingnews/1510321)
7.  [新浪新聞中心-第二屆中日韓筷子節在韓國舉辦](http://news.sina.com.cn/o/2016-11-10/doc-ifxxsmuu5273778.shtml)
8.  [日本箸文化協會- 箸感謝祭](http://hashi-bunka.jp/pg228.html)