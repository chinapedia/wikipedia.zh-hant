**酒見寺**是一位於[日本](../Page/日本.md "wikilink")[兵庫縣](../Page/兵庫縣.md "wikilink")[加西市的](../Page/加西市.md "wikilink")[佛寺](../Page/佛寺.md "wikilink")，屬於[高野山](../Page/高野山.md "wikilink")[真言宗一派](../Page/真言宗.md "wikilink")。酒見寺是在[天平](../Page/天平_\(聖武天皇\).md "wikilink")17年（745年）時，由[行基菩薩建立](../Page/行基菩薩.md "wikilink")。酒見寺的[山號為](../Page/山號.md "wikilink")**泉生山**，祀奉的本尊主神為[十一面觀音](../Page/十一面觀音.md "wikilink")。

## 歷史

根據該寺寺記記載，行基在走訪到當地時收到[酒見明神的旨意](../Page/酒見明神.md "wikilink")，要在此地建造佛寺。行基向當時的[聖武天皇奏報此事](../Page/聖武天皇.md "wikilink")，並獲准設寺，定號為酒見寺。酒見寺是聖武天皇的[勅願寺](../Page/勅願寺.md "wikilink")（[天皇為了祈願或還願所設的佛寺](../Page/天皇.md "wikilink")），之後的歷代天皇與[將軍也持續地敕使來此參拜](../Page/幕府將軍.md "wikilink")，其中包括[德川家光](../Page/德川家光.md "wikilink")，在酒見寺的護摩堂內放有歷代天皇與德川家的將軍們之牌位。在[天平年間所建的寺院堂塔](../Page/天平_\(聖武天皇\).md "wikilink")，在接下來的幾個世紀中遭到至少兩次的嚴重損壞，其中包括在[平治之亂](../Page/平治之亂.md "wikilink")（1159年）時遭大火全毀，並在之後奉[二條院之敕重建](../Page/二條院.md "wikilink")。[天正時代間因兵亂](../Page/天正_\(日本\).md "wikilink")（[三木合戰](../Page/三木合戰.md "wikilink")，1578年）又遭到火焚，整個[伽藍全燬](../Page/伽藍.md "wikilink")。[慶安](../Page/慶安_\(年號\).md "wikilink")3年（1650年）時，在當時[姬路城城主](../Page/姬路城.md "wikilink")[本多忠政的援助之下](../Page/本多忠政.md "wikilink")，重建了伽藍。之後江戶幕府第三代將軍德川家光將酒見寺定為「朱印寺」，此後歷代將軍皆繼續賜[狀授地](../Page/朱印狀.md "wikilink")。酒見寺可以說自開基將來便一直受到歷代天皇、幕府與[藩主豐厚的保護與重視](../Page/藩主.md "wikilink")。

[File:Sagamiji01s3200.jpg|楼門](File:Sagamiji01s3200.jpg%7C楼門)
<File:Sagamiji> Hondo.jpg|本堂
[File:Sagamiji08s3200.jpg|本堂前](File:Sagamiji08s3200.jpg%7C本堂前)
[File:Sagamiji03s3200.jpg|多宝塔](File:Sagamiji03s3200.jpg%7C多宝塔)
[File:Sagamiji04s3200.jpg|多宝塔軒下](File:Sagamiji04s3200.jpg%7C多宝塔軒下)
[File:Sagamiji05s3200.jpg|鐘楼](File:Sagamiji05s3200.jpg%7C鐘楼)
[File:Sagamiji06s3200.jpg|鐘楼軒下](File:Sagamiji06s3200.jpg%7C鐘楼軒下)
[File:Sagamiji07s3200.jpg|本坊](File:Sagamiji07s3200.jpg%7C本坊)
[File:Sagamiji09s3200.jpg|引聲堂](File:Sagamiji09s3200.jpg%7C引聲堂)

## 參考文獻

  - [酒見寺（兵庫縣加西市官方網站）](https://web.archive.org/web/20070824173348/http://www.city.kasai.hyogo.jp/02kank/09bunk/sitei/sakam/sagam.htm)

  - [泉生山
    酒見寺（新西國靈場官方網站）](http://www.shin-saigoku.jp/temple/34_sagamiji_01.html)

  - [酒見寺](http://www.y-morimoto.com/s_saigoku/s_saigoku29.html)

[Category:兵庫縣佛寺](../Category/兵庫縣佛寺.md "wikilink")
[Category:高野山真言宗](../Category/高野山真言宗.md "wikilink")
[Category:勅願寺](../Category/勅願寺.md "wikilink")
[Category:加西市](../Category/加西市.md "wikilink")