**天主教道明學校財團法人高雄市道明高級中學**，簡稱**道明中學**(Saint Dominic's Catholic High
School)，位於[臺灣](../Page/臺灣.md "wikilink")[高雄市](../Page/高雄市.md "wikilink")[苓雅區的一所私立](../Page/苓雅區.md "wikilink")[完全中學](../Page/完全中學.md "wikilink")。隔鄰為高雄市有名的教會醫院[聖功醫院和高雄市有名的啓智學校](../Page/聖功醫院.md "wikilink")[高雄啓智](../Page/高雄啓智.md "wikilink")。

## 校史

[Saint_Dominic_High_School.jpg](https://zh.wikipedia.org/wiki/File:Saint_Dominic_High_School.jpg "fig:Saint_Dominic_High_School.jpg")

天主教[中華道明會於](../Page/中華道明會.md "wikilink")1955年8月4日在今日校址動土開工，定校名為“道明”以紀念道明會會祖[聖道明亦寓有](../Page/聖道明.md "wikilink")“弘道明德”之義
。1958年9月正式招生，招收第一屆高一兩班、初一兩班，各一百一十人，為男子完全中學。2012年9月，更名為**天主教道明學校財團法人高雄市道明高級中學**。現為男女合班完全中學。道明中學秉持優良教育品質，自詡為南台灣第一私校，年年有醫科，當之無愧。更在許多教育評鑑上獲得佳績，實在是南台灣的棟樑，高雄的中流砥柱。

## 歷任校長

  - 第一任：[鄭天祥](../Page/鄭天祥.md "wikilink") 主教
    1955年至1961年7月（1961年5月21日在羅馬祝聖為高雄教區首任主教，乃於同年7月辭校長職。）
  - 第二任：[馮觀濤](../Page/馮觀濤.md "wikilink") 神父 1961年5月至1990年1月
  - 第三任：[潘清德](../Page/潘清德.md "wikilink") 神父
    1990年2月至1991年1月、1991年10月至1992年11月
  - 第四任：[曾仰如](../Page/曾仰如.md "wikilink") 神父 1991年2月至1991年9月
  - 第六任：[黃龍章](../Page/黃龍章.md "wikilink") 1992年12月至1995年7月
  - 第七任：[宋弘茂](../Page/宋弘茂.md "wikilink") 1995年8月至2009年7月
  - 第八任：[張艷華](../Page/張艷華.md "wikilink") 2009年8月至2013年7月
  - 第九任：[林耀隆](../Page/林耀隆.md "wikilink") 2013年8月迄今

## 學校慶典

學校內部主要慶典有四個，分別為12月底的耶誕節園遊會。，3月底的校慶運動會，還有復活節會有找彩蛋活動

## 校友

  - [吳克羣](../Page/吳克羣.md "wikilink") 藝人，曾在校內歌唱比賽得獎。
  - [周定緯](../Page/周定緯.md "wikilink") 藝人，曾在校內歌唱比賽得獎。
  - [李濤](../Page/李濤.md "wikilink")，曾任華視主播，現任TVBS有線電視台的政論節目主持人，因婚姻出軌事件備受非議。
  - [洪曉慧](../Page/洪曉慧.md "wikilink") 震驚社會之清大殺人溶屍案凶手.
  - [余政憲](../Page/余政憲.md "wikilink") 民進黨立委，曾任內政部長，［南台灣媽祖婆］余陳月瑛之子
  - [邱議瑩](../Page/邱議瑩.md "wikilink") 民進黨立委
  - [陳其邁](../Page/陳其邁.md "wikilink") 民進黨立委
  - [馮翊綱](../Page/馮翊綱.md "wikilink")
    藝人，[相聲瓦舍創辦人之一](../Page/相聲瓦舍.md "wikilink")
  - [吳聖皓](../Page/吳聖皓.md "wikilink")
    藝人，第25屆金曲獎最佳樂團獎[麋先生主唱](../Page/麋先生.md "wikilink")。
  - [林喆安](../Page/林喆安.md "wikilink")
    藝人，第25屆金曲獎最佳樂團獎[麋先生木吉他手](../Page/麋先生.md "wikilink")。
  - [張以諾](../Page/張以諾.md "wikilink")
    藝人，第25屆金曲獎最佳樂團獎[麋先生貝斯手](../Page/麋先生.md "wikilink")。
  - [許維芳](../Page/許維芳.md "wikilink")
    網路紅人，105學年度畢業歌[夢想藍圖女主唱](../Page/夢想藍圖.md "wikilink")
  - [謝嘉全](../Page/謝嘉全.md "wikilink")
    網路紅人，105學年度畢業歌[夢想藍圖男主唱](../Page/夢想藍圖.md "wikilink")
  - [陳尚志](../Page/陳尚志.md "wikilink") 大學教授，社會民主黨召集人

## 校服

道明中學女生冬季制服.jpg|女生冬季制服

## 參見

  - [夢想藍圖](../Page/夢想藍圖.md "wikilink")

## 外部連結

  -
[Category:高雄市中學](../Category/高雄市中學.md "wikilink")
[Category:台灣私立高級中學](../Category/台灣私立高級中學.md "wikilink")
[Category:台灣天主教會學校](../Category/台灣天主教會學校.md "wikilink")
[Category:台灣完全中學](../Category/台灣完全中學.md "wikilink")
[Category:苓雅區](../Category/苓雅區.md "wikilink")
[Category:1955年創建的教育機構](../Category/1955年創建的教育機構.md "wikilink")
[高](../Category/台灣冠以人名的教育機構.md "wikilink") [Category:五塊厝
(苓雅區)](../Category/五塊厝_\(苓雅區\).md "wikilink")
[Category:高雄市私立道明中學](../Category/高雄市私立道明中學.md "wikilink")