**詹保羅·帕齊尼**（，）是一名[意大利的職業足球員](../Page/意大利.md "wikilink")。現時被[意甲球隊](../Page/意甲.md "wikilink")[維羅納外借至](../Page/維羅納足球俱樂部.md "wikilink")[西甲](../Page/西甲.md "wikilink")[利雲特](../Page/萊萬特足球俱樂部.md "wikilink")。2009–2012年曾[意大利国家队的现役成员](../Page/意大利国家足球队.md "wikilink")。他曾获选[意甲最佳青年球员](../Page/意甲最佳青年球员.md "wikilink")，绰号“帕佐”（意大利语Pazzo，意为“疯子”）。

## 生涯

### 球會生涯

#### 亚特兰大

帕齐尼于2003年在[亞特蘭大开始了自己的足球职业生涯](../Page/亞特蘭大足球俱樂部.md "wikilink")。起初他是一个左后卫，后来才改打前锋。在为亚特兰大效力的两个赛季，他出场51次攻入12球，成为希望之星。

#### 費倫天拿

他在2005年1月自[亞特蘭大以](../Page/亞特蘭大足球俱樂部.md "wikilink")650万欧元的价格轉會到[費倫天拿](../Page/費倫天拿.md "wikilink")。他是一名強而有力的[前鋒](../Page/前鋒_\(足球\).md "wikilink")，並在[-{zh-hk:盧卡·東尼;zh-hans:卢卡·托尼;zh-tw:盧卡·托尼;}-離隊後取代其重要位置](../Page/盧卡·東尼.md "wikilink")。詹保羅·帕齊尼在球隊時，在一次對陣[祖雲達斯時攻入兩球](../Page/祖雲達斯.md "wikilink")，擊敗世界頂級[門將](../Page/門將.md "wikilink")[-{zh-hk:保方;zh-hans:布冯;zh-tw:布馮;}-](../Page/詹路易吉·布冯.md "wikilink")，是他生涯中的其中一份佳作（最终佛罗伦萨与尤文图斯3:3打平）。可是，在其後帕齐尼被排除在了时任球队教练[普兰德利的计划外](../Page/切萨雷·普兰德利.md "wikilink")，加上球會從[AC米蘭買入射手](../Page/AC米蘭.md "wikilink")[-{zh-hk:基拿甸奴;zh-hans:吉拉迪诺;zh-tw:吉拉迪諾;}-](../Page/阿尔贝托·吉拉迪诺.md "wikilink")，令他失去正選地位。最終他被球會以800萬歐元賣到[森多利亞](../Page/森多利亞.md "wikilink")。

#### 森多利亞

柏仙尼在2009年1月轉會到森多利亞。轉會後，他以驚人的速度融入球隊，於意大利盃八強對[烏甸尼斯一戰先開紀錄](../Page/烏甸尼斯.md "wikilink")，首次上陣便助球隊打進八強，其后在意大利杯半决赛中攻入一球，帮助球队3:0击败意甲豪门[国际米兰闯入决赛](../Page/国际米兰.md "wikilink")。其更在之後的12場比賽中入11球，漸漸站穩正選位置，更與隊中前鋒[-{zh-hk:卡斯辛奴;zh-hans:卡萨诺;zh-tw:卡薩諾;}-組成強大的攻擊線](../Page/安東尼奧·卡斯辛奴.md "wikilink")。在2009/10赛季，帕齐尼在联赛中攻入了19球，在当季的意甲联赛最佳射手奖项中排名第三。

#### 國際米蘭

2011年1月29日，[國際米蘭官方宣佈正式從](../Page/國際米蘭.md "wikilink")[森多利亞引進意大利國腳柏仙尼](../Page/森多利亞.md "wikilink")，據報章報道轉會費高達2000萬歐元。加盟國米之後，柏仙尼將穿上昔日[-{zh-hk:費高;zh-hans:菲戈;zh-tw:菲戈;}-所穿過的](../Page/路易斯·菲戈.md "wikilink")7號球衣，根據柏仙尼和國米雙方所簽署的合約，柏仙尼將效力到2015年6月30日。1月30日，帕齐尼在意甲联赛国际米兰主场对阵[巴勒莫的比赛中替补出场](../Page/巴勒莫足球俱乐部.md "wikilink")，首次代表国际米兰亮相。他在比赛中打进2个进球并创造出1个点球，帮助国际米兰3比2逆转对手。隨後柏仙尼迅速成為國際米蘭在下半季的意甲比賽中追趕死敵[AC米蘭榜首位置的重要棋子](../Page/AC米蘭.md "wikilink")，在半季比賽中共為國際米蘭攻入11個聯賽入球，雖然最終國際米蘭仍失落聯賽錦標，但柏仙尼的能力已得到一眾國際米蘭球迷的肯定，同時，柏仙尼亦幫助球隊成功贏得該屆的意大利盃冠軍。

2011/12赛季，國際米蘭队中另一名中锋[-{zh-hans:迭戈·米利托;zh-hk:迪亞高·米列圖;zh-tw:迭戈·米利托;}-的出色表现使得柏仙尼失去首发位置](../Page/迪亞高·米列圖.md "wikilink")，虽然他仍以替补的身份参加国际米兰的大部分联赛比赛，但在有限的出场时间里仅有5球进账。

### AC米兰

2012年8月22日，國際米蘭和同城球队[AC米兰宣布柏仙尼和](../Page/AC米兰.md "wikilink")[-{zh-hans:安东尼奥·卡萨诺;zh-hk:安東尼奧·卡斯辛奴;zh-tw:安東尼奧·卡薩諾;}-互换球队](../Page/安東尼奧·卡斯辛奴.md "wikilink")，柏仙尼正式加盟AC米兰。据报道AC米兰在这次交易中支付给国际米兰700万欧元。\[1\]

### 維羅納

2015年夏天，柏仙尼與AC米蘭的合約結束，柏仙尼，以自由身球員身份加盟維羅納。[2015/16意甲球季](../Page/2015年至2016年意大利足球甲级联赛.md "wikilink")，維羅納只取名第20名，降落乙組。2016/17意乙球季，柏仙尼打進23球，成為乙組神射手，並協助維羅納取得亞軍，重返意甲。

### 國家隊生涯

#### U-21

在2004-2007年間，詹保羅·帕齊尼曾是意大利U21的隊員，為球隊上陣22次和攻入5球，更創造在[溫布萊球場的第一個入球](../Page/溫布萊球場.md "wikilink")。

#### 意大利國家隊

在2009年，柏仙尼憑在森多利亞出色的表現，吸引[意大利國家隊教練](../Page/意大利国家足球队.md "wikilink")[-{zh-hk:納比;zh-hans:里皮;zh-tw:里皮;}-的注意](../Page/马尔切洛·里皮.md "wikilink")，入選國家隊成為大國腳,並在首次上陣、對[黑山的比賽中射入一球](../Page/黑山国家足球队.md "wikilink")。在4天後，帕齊尼進入了意大利對[爱尔兰的世界杯預選賽收發](../Page/爱尔兰国家足球队.md "wikilink")，不過開場2分鐘時就收到紅牌。帕齊尼也參加了2009年6月6日意大利對[北爱尔兰的友誼賽](../Page/北爱尔兰足球代表队.md "wikilink")。

## 職業數據統計

| 球队                                                                  | 赛季  | 联赛 | 杯赛 | 洲际 | 总计 |
| ------------------------------------------------------------------- | --- | -- | -- | -- | -- |
| 出场                                                                  | 进球  | 出场 | 进球 | 出场 | 进球 |
| [亚特兰大](../Page/亚特兰大足球俱乐部.md "wikilink")                             |     |    |    |    |    |
| \!colspan="2"|[2003–04](../Page/2003年至2004年意大利足球甲级联赛.md "wikilink") | 39  | 9  | \- | \- | \- |
| \!colspan="2"|[2004–05](../Page/2004年至2005年意大利足球甲级联赛.md "wikilink") | 12  | 3  | \- | \- | \- |
| \!colspan|**总计**                                                    | 51  | 12 | \- | \- | \- |
| [佛罗伦萨](../Page/佛罗伦萨足球俱乐部.md "wikilink")                             |     |    |    |    |    |
| \!colspan="2"|[2004–05](../Page/2004年至2005年意大利足球甲级联赛.md "wikilink") | 14  | 3  | 0  | 0  | \- |
| \!colspan="2"|[2005–06](../Page/2005年至2006年意大利足球甲级联赛.md "wikilink") | 27  | 5  | 0  | 0  | \- |
| \!colspan="2"|[2006–07](../Page/2006年至2007年意大利足球甲级联赛.md "wikilink") | 24  | 7  | 0  | 0  | \- |
| \!colspan="2"|[2007–08](../Page/2006年至2007年意大利足球甲级联赛.md "wikilink") | 31  | 9  | 0  | 0  | 10 |
| \!colspan="2"|[2008–09](../Page/2007年至2008年意大利足球甲级联赛.md "wikilink") | 12  | 1  | 3  | 0  | 2  |
| \!colspan="2"|**总计**                                                | 108 | 25 | 12 | 8  | 14 |
| [桑普多利亚](../Page/桑普多利亚.md "wikilink")                                |     |    |    |    |    |
| \!colspan="2"|[2008–09](../Page/2008年至2009年意大利足球甲级联赛.md "wikilink") | 19  | 11 | 4  | 4  | \- |
| \!colspan="2"|[2009–10](../Page/2009年至2010年意大利足球甲级联赛.md "wikilink") | 37  | 19 | 1  | 2  | \- |
| \!colspan="2"|[2010–11](../Page/2010年至2011年意大利足球甲级联赛.md "wikilink") | 19  | 6  | 2  | 1  | 5  |
| \!colspan="2"|**总计**                                                | 75  | 36 | 7  | 7  | 5  |
| [国际米兰](../Page/国际米兰.md "wikilink")                                  |     |    |    |    |    |
| \!colspan="2"|[2010–11](../Page/2010年至2011年意大利足球甲级联赛.md "wikilink") | 17  | 11 | 3  | 0  | 0  |
| \!colspan="2"|[2011–12](../Page/2011年至2012年意大利足球甲级联赛.md "wikilink") | 33  | 5  | 1  | 1  | 6  |
| 总计                                                                  | 50  | 16 | 4  | 1  | 6  |
| [AC米兰](../Page/AC米兰.md "wikilink")                                  |     |    |    |    |    |
| \!colspan="2"|[2012–13](../Page/2012年至2013年意大利足球甲级联赛.md "wikilink") | 19  | 10 | 2  | 1  | 4  |
| 总计                                                                  | 19  | 10 | 2  | 1  | 4  |
| 职业生涯总计                                                              | 303 | 99 | 13 | 9  | 28 |

## 其他

### 技術特點

司職中鋒的柏仙尼的足球風格簡單且具力量性，有人更拿他與前國際米蘭球星[韋利比較](../Page/韋利.md "wikilink")，柏仙尼不止擁有上佳射術和門前觸角，對於接應隊友傳送球的控球技巧亦不俗，更別具一格的是他對射門的想像力。就例如柏仙尼為國際米蘭上演的首場意甲球賽時，為球隊於落後[巴勒莫](../Page/巴勒莫.md "wikilink")0:2的情況下扳回的第一球，柏仙尼於對手禁區背身接應隊友[卡查的傳中球](../Page/卡查.md "wikilink")，在背後有敵方球員緊迫的情況下，不瞄準便瞬速180度轉身抽射，盡顯柏仙尼門前的想像力和直覺。

### 紀錄

他保持在新[溫布萊球場的最快入球](../Page/溫布萊球場.md "wikilink")，新[溫布萊球場的第一個入球及第一次](../Page/溫布萊球場.md "wikilink")[帽子戲法的紀錄](../Page/帽子戲法.md "wikilink")，这几个记录都发生在意大利U21在客场3-3逼平了英格兰U21代表队的比赛里。

## 外部連結

  - [意大利國家隊官方網頁](http://213.92.78.197/nazionali/DettaglioConvocato?codiceConvocato=2492&squadra=NAZIONALI)

  - [柏仙尼個人檔案](http://www.gazzetta.it/Speciali/serie_a_2007/giocatori/pazzini_gia.shtml)

  - [費倫天拿的新聞](http://www.violanews.com)

## 參考資料

[Category:足球前鋒](../Category/足球前鋒.md "wikilink")
[Category:亞特蘭大球員](../Category/亞特蘭大球員.md "wikilink")
[Category:費倫天拿球員](../Category/費倫天拿球員.md "wikilink")
[Category:意大利足球運動員](../Category/意大利足球運動員.md "wikilink")
[Category:森多利亞球員](../Category/森多利亞球員.md "wikilink")
[Category:國際米蘭球員](../Category/國際米蘭球員.md "wikilink")
[Category:AC米蘭球員](../Category/AC米蘭球員.md "wikilink")
[Category:維羅納球員](../Category/維羅納球員.md "wikilink")
[Category:意甲球員](../Category/意甲球員.md "wikilink")
[Category:2010年世界盃足球賽球員](../Category/2010年世界盃足球賽球員.md "wikilink")

1.