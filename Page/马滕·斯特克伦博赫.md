**马滕·斯特克伦博赫**（，），[荷蘭足球運動員](../Page/荷蘭.md "wikilink")，司職門將，乃[荷甲傳統勁旅](../Page/荷甲.md "wikilink")[阿積士青訓產品](../Page/阿積士.md "wikilink")，現效力[英超球會](../Page/英超.md "wikilink")[愛華頓](../Page/愛華頓足球會.md "wikilink")。曾效力[意甲球會](../Page/意甲.md "wikilink")[羅馬](../Page/羅馬足球俱樂部.md "wikilink")，2013年6月轉投[英超球會](../Page/英超.md "wikilink")[富勒姆](../Page/富勒姆足球俱乐部.md "wikilink")。他曾被廣泛認為是荷蘭著名門將[雲達沙的接班人](../Page/雲達沙.md "wikilink")。

## 生平

### 球會

  - 阿積士

史迪基倫堡早年是在荷蘭一些小型球會接受訓練，十五歲時始加盟荷蘭班霸[阿積士](../Page/阿積士.md "wikilink")。他於2002年2月24日一場聯賽中正式在一隊登場。其後他出場漸多，其間比較重要的獎項，乃2004年[荷甲聯賽冠軍](../Page/荷甲.md "wikilink")。

  - 羅馬

2011年8月2日，羅馬以600萬[歐元從阿積士簽入](../Page/歐元.md "wikilink")[荷蘭國家隊門將史迪基倫堡](../Page/荷蘭國家足球隊.md "wikilink")，合約為期四年。該協議包括附加條款，如史迪基倫堡再轉球會，阿積士會再獲款項支付，及如果羅馬得到[歐洲冠軍聯賽參賽資格](../Page/歐洲冠軍聯賽.md "wikilink")，阿積士會再有獎金\[1\]。

  - 富咸

2013年6月5日，富咸從羅馬簽入史迪基倫堡，合約為期四年，轉會費金額並無透露。是次令史迪基倫堡與富咸領隊[马丁·乔尔繼](../Page/马丁·乔尔.md "wikilink")2009/10球季於[阿積士後再次合作](../Page/阿積士.md "wikilink")\[2\]。

  - 修咸頓

2015年6月23日，修咸頓從富咸借入史迪基倫堡，借約一年。

### 國家隊

他第一次代表國家隊乃2004年9月3日對賽[列支敦士登大勝](../Page/列支敦士登国家足球队.md "wikilink")3-0的賽事。[2006年世界杯他獲國家隊選中出賽決賽週](../Page/2006年世界杯.md "wikilink")，但不過是副選。2008年9月對[澳洲的一場](../Page/澳洲國家足球隊.md "wikilink")[友誼賽](../Page/友誼賽.md "wikilink")，史迪基倫堡因侵犯[乔舒亚·肯尼迪被逐離場](../Page/乔舒亚·肯尼迪.md "wikilink")，是歷來首領[紅牌的荷蘭國家隊門將](../Page/紅牌.md "wikilink")。在[雲達沙宣佈退出國家隊後](../Page/雲達沙.md "wikilink")，史迪基倫堡成為荷蘭首席門將，獲選參加[2010年世界盃](../Page/2010年世界盃足球賽.md "wikilink")。

史迪基倫堡在2010年世界盃成名，帶領球隊殺入决赛。2010年世界盃後，史迪基倫堡一度和紅魔扯上關係。

## 榮譽

  - 阿積士

<!-- end list -->

  - [荷甲聯賽冠軍](../Page/荷甲.md "wikilink")：2004；
  - [荷蘭盃冠軍](../Page/荷蘭盃.md "wikilink")：2006年、2007年、2010年；
  - [告魯夫盾冠軍](../Page/告魯夫盾.md "wikilink")：2003年、2007年；

<!-- end list -->

  - 荷蘭國家隊

<!-- end list -->

  - [世界盃亞軍](../Page/世界盃足球賽.md "wikilink")：[2010年](../Page/2010年世界盃足球賽.md "wikilink")；

## 參考資料

## 外部連結

  -
  - [Official Ajax
    profile](https://web.archive.org/web/20110720093219/http://english.ajax.nl/web/show/id=48420/dbid=35/team=1/typeofpage=55719)

  - [Wereld van oranje.nl
    Profile](http://www.wereldvanoranje.nl/profielen/profiel.php?id=474)

  - [National Football
    Teams](http://www.national-football-teams.com/v2/player.php?id=8485)

[Category:北荷蘭省人](../Category/北荷蘭省人.md "wikilink")
[Category:荷蘭足球運動員](../Category/荷蘭足球運動員.md "wikilink")
[Category:足球守門員](../Category/足球守門員.md "wikilink")
[Category:阿積士球員](../Category/阿積士球員.md "wikilink")
[Category:羅馬球員](../Category/羅馬球員.md "wikilink")
[Category:富咸球員](../Category/富咸球員.md "wikilink")
[Category:摩納哥球員](../Category/摩納哥球員.md "wikilink")
[Category:修咸頓球員](../Category/修咸頓球員.md "wikilink")
[Category:愛華頓球員](../Category/愛華頓球員.md "wikilink")
[Category:荷甲球員](../Category/荷甲球員.md "wikilink")
[Category:意甲球員](../Category/意甲球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:法甲球員](../Category/法甲球員.md "wikilink")
[Category:義大利外籍足球運動員](../Category/義大利外籍足球運動員.md "wikilink")
[Category:英格蘭外籍足球運動員](../Category/英格蘭外籍足球運動員.md "wikilink")
[Category:荷蘭旅外足球運動員](../Category/荷蘭旅外足球運動員.md "wikilink")
[Category:在義大利的荷蘭人](../Category/在義大利的荷蘭人.md "wikilink")
[Category:荷蘭國家足球隊球員](../Category/荷蘭國家足球隊球員.md "wikilink")
[Category:2010年世界盃足球賽球員](../Category/2010年世界盃足球賽球員.md "wikilink")
[Category:2006年世界盃足球賽球員](../Category/2006年世界盃足球賽球員.md "wikilink")
[Category:2012年歐洲國家盃球員](../Category/2012年歐洲國家盃球員.md "wikilink")
[Category:2008年歐洲國家盃球員](../Category/2008年歐洲國家盃球員.md "wikilink")

1.
2.