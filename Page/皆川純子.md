**皆川純子**是[日本的](../Page/日本.md "wikilink")[女性](../Page/女性.md "wikilink")[聲優](../Page/聲優.md "wikilink")。[茨城縣出身](../Page/茨城縣.md "wikilink")。[血型A型](../Page/血型.md "wikilink")。

以特別的聲音和精彩的演技成名，擅長少年的聲線，並獲得不少女性Fans的支持。在成為聲優之前是一名上班族，後來進入了聲優養成學校，一邊上班一邊工作，漸漸投入了聲優的工作。由[博英社以](../Page/博英社.md "wikilink")[網球王子聲優的身份邀請](../Page/網球王子.md "wikilink")，於2005年8月與[甲斐田幸](../Page/甲斐田幸.md "wikilink")（）小姐共同出席[台灣](../Page/台灣.md "wikilink")[漫畫博覽會](../Page/漫畫博覽會.md "wikilink")，並且舉辦了簽名會活動。

## 主要參與作品

### 電視動畫

主要角色以**粗體**表示

  - 2001年

<!-- end list -->

  - [網球王子](../Page/網球王子_\(動畫\).md "wikilink")（**越前龍馬**、越前倫子、若人啦啦隊之7）
  - [熱帶雨林的爆笑生活](../Page/熱帶雨林的爆笑生活.md "wikilink")（由米）

<!-- end list -->

  - 2002年

<!-- end list -->

  - [我們這一家](../Page/我們這一家.md "wikilink")（山田）
  - [東京喵喵](../Page/東京喵喵.md "wikilink")（白金稜〈小時候〉）
  - [哈姆太郎](../Page/哈姆太郎.md "wikilink")（小愛迪生）
  - [戰鬥陀螺v代](../Page/戰鬥陀螺v代.md "wikilink")（傑克）

<!-- end list -->

  - 2003年

<!-- end list -->

  - [聖槍修女](../Page/聖槍修女.md "wikilink")（約書亞·克里斯多福）
  - [貯金大冒險](../Page/貯金大冒險.md "wikilink")（維也納腸）
  - [成惠的世界](../Page/成惠的世界.md "wikilink")（七瀨香奈花）
  - [火影忍者](../Page/火影忍者.md "wikilink")（白的母親）
  - [星球流浪記](../Page/星球流浪記.md "wikilink")（**申吾**）
  - [名偵探柯南](../Page/名侦探柯南_\(动画\).md "wikilink")（平良伊江）
  - [神魂合體](../Page/神魂合體.md "wikilink")（猿渡忍）
  - [京極夏彦 巷說百物語](../Page/京極夏彦_巷說百物語.md "wikilink")（柳女）
  - [.hack//黃昏的腕輪傳說](../Page/.hack/黃昏的腕輪傳說.md "wikilink")（**國崎秀悟**）

<!-- end list -->

  - 2004年

<!-- end list -->

  - [奇幻旅程](../Page/奇幻旅程_\(動畫\).md "wikilink")（**多馬**）
  - [真珠美人魚](../Page/真珠美人魚.md "wikilink")（米凱爾）
  - [洛克人EXE AXESS](../Page/洛克人EXE_AXESS.md "wikilink")（）
  - [神魂合體](../Page/神魂合體.md "wikilink") 第二季（猿渡忍）
  - [北方戀曲](../Page/北方戀曲.md "wikilink")（森永真冬）
  - [Duel Masters -
    卡片決鬥高手](../Page/Duel_Masters_-_卡片決鬥高手.md "wikilink")（**白凰**、白凰之母）

<!-- end list -->

  - 2005年

<!-- end list -->

  - [ARIA The ANIMATION](../Page/水星領航員.md "wikilink")（**晃·E·法拉利**）
  - [草莓棉花糖](../Page/草莓棉花糖.md "wikilink")（笹塚、安娜的媽媽）
  - [玻璃面具](../Page/玻璃面具.md "wikilink")（金-{谷}-英美）
  - [我的主人](../Page/我的主人.md "wikilink")（**中林義貴**）
  - [灼眼的夏娜](../Page/灼眼的夏娜.md "wikilink")（「盛裝騎手」卡姆辛·雷布哈烏）
  - [魔界女王候補生](../Page/魔界女王候補生.md "wikilink")（少年、獨角獸）
  - [TSUBASA翼](../Page/TSUBASA翼.md "wikilink")（龍王）
  - [聖魔之血](../Page/聖魔之血.md "wikilink")（以恩·法透納）
  - [雙子公主](../Page/雙子公主.md "wikilink")（**夏恩特**）
  - [黑貓](../Page/黑貓.md "wikilink")（-{里}-昂·艾略特）
  - [怪醫黑傑克](../Page/怪醫黑傑克.md "wikilink")（）
  - [魔法老師](../Page/魔法老師_\(動畫\).md "wikilink")（**[雪廣綾香](../Page/雪廣綾香.md "wikilink")**）
  - [Loveless](../Page/Loveless.md "wikilink")（**青柳立夏**）
  - [Jinki:EXTEND](../Page/Jinki:EXTEND.md "wikilink")（****）
  - [格鬥美神 武龍](../Page/格鬥美神_武龍.md "wikilink")（**曹春揚**）

<!-- end list -->

  - 2006年

<!-- end list -->

  - [ARIA The NATURAL](../Page/水星領航員.md "wikilink")（**晃·E·法拉利**）
  - [Code Geass
    反叛的魯路修](../Page/Code_Geass_反叛的魯路修.md "wikilink")（柯內莉亞·Li·不列顛尼亞）
  - [地獄少女 二籠](../Page/地獄少女.md "wikilink")（天城志津子）(第11話)
  - [史上最強弟子兼一](../Page/史上最強弟子兼一.md "wikilink")（白鳥）
  - [貓狗寵物街](../Page/貓狗寵物街.md "wikilink")（）
  - [少年陰陽師](../Page/少年陰陽師.md "wikilink")（玄武）
  - [奏光之Strain](../Page/奏光之Strain.md "wikilink")（**梅路奇薩路克**）
  - [魔法老師\!?](../Page/魔法老師_\(動畫\).md "wikilink")（**雪廣綾香**）
  - [蜂蜜幸運草第二季](../Page/蜂蜜幸運草.md "wikilink")（森田馨〈小時候〉）
  - [雙子公主](../Page/雙子公主.md "wikilink") Gyu\!（**夏恩特**）
  - [武裝鍊金](../Page/武裝鍊金.md "wikilink")（圓山圓）
  - 名偵探柯南（）\#452
  - [完美小姐進化論](../Page/完美小姐進化論.md "wikilink")（）
  - [格鬥美神 武龍 REBIRTH](../Page/格鬥美神_武龍_REBIRTH.md "wikilink")（**曹春揚**）
  - [咆哮戰爭](../Page/咆哮戰爭.md "wikilink")（島原看取·李彩霞）
  - [妖逆門](../Page/妖逆門.md "wikilink")（華院三月）
  - [MÄR](../Page/MÄR.md "wikilink")（幼時艾倫）

<!-- end list -->

  - 2007年

<!-- end list -->

  - [多啦A夢](../Page/多啦A夢.md "wikilink")（）

  - [驅魔少年](../Page/驅魔少年.md "wikilink")（）

  - [Zero決鬥大師](../Page/決鬥大師.md "wikilink")（**白凰**）

  - [地球防衛少年](../Page/地球防衛少年.md "wikilink")（**宇白順**、古茂田孝美之母）

  - （**範人**）

  - [灼眼的夏娜II](../Page/灼眼的夏娜.md "wikilink")（「盛裝騎手」卡姆辛·雷布哈烏）

  - [決鬥大師Zero](../Page/決鬥大師.md "wikilink")（白凰）

<!-- end list -->

  - 2008年

<!-- end list -->

  - [ARIA The ORIGINATION](../Page/水星領航員.md "wikilink")（**晃·E·法拉利**）
  - [吸血鬼騎士](../Page/吸血鬼騎士.md "wikilink")（早園瑠佳）
  - [吸血鬼騎士 Guilty](../Page/吸血鬼騎士.md "wikilink")（早園瑠佳）
  - [BLASSREITER](../Page/BLASSREITER.md "wikilink")（馬列克·威爾納）
  - [泰坦尼亚](../Page/泰坦尼亚.md "wikilink")（米兰达·卡西米尔）
  - [Code Geass 反叛的魯路修
    R2](../Page/Code_Geass_反叛的魯路修_R2.md "wikilink")（柯內莉亞·Li·不列顛尼亞）
  - [游戏王5D's](../Page/游戏王5D's.md "wikilink")（米丝蒂）
  - [守護甜心 心跳\!](../Page/守護甜心.md "wikilink")（水野守）\#56

<!-- end list -->

  - 2009年

<!-- end list -->

  - [潘朵拉之心](../Page/潘朵拉之心.md "wikilink")（**奧茲·貝薩流士**）
  - [戰場女武神](../Page/戰場女武神.md "wikilink")（羅潔〈布利姬多·修羅塔〉）
  - [BASQUASH\!](../Page/BASQUASH!.md "wikilink")（蘇拉修·肯茲）
  - [夏目友人帳](../Page/夏目友人帳.md "wikilink")（少女）\#3
  - 名偵探柯南（渡邊友紀子）\#544
  - [CANAAN](../Page/CANAAN.md "wikilink")（夏目百合）

<!-- end list -->

  - 2010年

<!-- end list -->

  - [科學超電磁砲](../Page/科學超電磁砲.md "wikilink")（鴻野江遥希）\#17
  - [無法掙脫的背叛](../Page/無法掙脫的背叛.md "wikilink")（艾蕾姬）
  - [心靈偵探 八雲](../Page/心靈偵探_八雲.md "wikilink")（宮川惠理子）

<!-- end list -->

  - 2011年

<!-- end list -->

  - [玛莉亚狂热 Alive](../Page/玛莉亚狂热.md "wikilink")（花房桃太）
  - [SKET DANCE](../Page/SKET_DANCE.md "wikilink")（惠大五郎）
  - [灼眼的夏娜Final](../Page/灼眼的夏娜.md "wikilink")（**卡姆辛．奈夫哈維**）
  - [肯普法](../Page/肯普法.md "wikilink")（皆川瞳美）

<!-- end list -->

  - 2012年

<!-- end list -->

  - [數碼獸合體戰爭](../Page/數碼獸合體戰爭.md "wikilink")（左輪獸）\#12-14、18
  - [男子高中生的日常](../Page/男子高中生的日常.md "wikilink")（奈古、名護）
  - [黃昏乙女×失憶幽靈](../Page/黃昏乙女×失憶幽靈.md "wikilink")（新谷貞一）
  - [新網球王子](../Page/網球王子_\(動畫\).md "wikilink")（**越前龍馬**）
  - [Campione 弒神者！](../Page/Campione_弒神者！.md "wikilink")（烏魯斯拉格納）
  - [Battle Spirits Sword
    Eyes](../Page/Battle_Spirits_Sword_Eyes.md "wikilink")（**劍·帶刀**）
  - [魔奇少年](../Page/魔奇少年.md "wikilink")（扎米爾〈少年〉）
  - [Smile 光之美少女！](../Page/Smile_光之美少女！.md "wikilink")（豐島英一）

<!-- end list -->

  - 2013年

<!-- end list -->

  - [寶石寵物 Kira☆Deco！](../Page/寶石寵物_Kira☆Deco！.md "wikilink")（古哈科）
  - [八犬傳－東方八犬異聞－](../Page/八犬傳－東方八犬異聞－.md "wikilink")（琥珀）
  - [革神語](../Page/革神語.md "wikilink")（金手）
  - [魔笛MAGI第二季](../Page/魔奇少年.md "wikilink")（扎米爾〈幼年〉）

<!-- end list -->

  - 2014年

<!-- end list -->

  - [美食獵人TORIKO](../Page/美食獵人TORIKO.md "wikilink")（三虎〈少年時代〉）
  - [極黑的布倫希爾德](../Page/極黑的布倫希爾德.md "wikilink")（伊尼夏雷茲）
  - [噗嗶啵～來自未來～](../Page/噗嗶啵～來自未來～.md "wikilink")（哈魯）
  - [晨曦公主](../Page/晨曦公主.md "wikilink")（**悠**）

<!-- end list -->

  - 2015年

<!-- end list -->

  - [金田一少年之事件簿R 第2期](../Page/金田一少年之事件簿_\(动画\).md "wikilink")（冬野八重姬）
  - 名侦探柯南（千田直美 \#801-802）

<!-- end list -->

  - 2016年

<!-- end list -->

  - [新我們這一家](../Page/我們這一家.md "wikilink")（山田）
  - [美少女戰士Crystal](../Page/美少女戰士Crystal.md "wikilink") Season III
    死亡毀滅者篇（**天王遙／水手天王星**）
  - [SUPER LOVERS](../Page/SUPER_LOVERS.md "wikilink")（**海棠零**）
  - [龍族拼圖X](../Page/龍族拼圖X.md "wikilink")（達芙尼斯）
  - [超心動！文藝復興](../Page/超心動！文藝復興.md "wikilink")（近松珠里）
  - [漂流武士](../Page/漂流武士.md "wikilink")（貞德·達魯克）
  - 名侦探柯南（木船染花 \#836-837）

<!-- end list -->

  - 2017年

<!-- end list -->

  - [SUPER LOVERS 2](../Page/SUPER_LOVERS_2.md "wikilink")（**海棠零**）
  - [ID-0](../Page/ID-0.md "wikilink")（**阿曼札·沃爾奇科娃**\[1\]）
  - [KiraKira☆光之美少女 A La
    Mode](../Page/KiraKira☆光之美少女_A_La_Mode.md "wikilink")（**皮卡利奧／朱利奧／黑樹利奧**）
  - [活擊 刀劍亂舞](../Page/刀劍亂舞.md "wikilink")（**審神者**）
  - [悠久持有者：魔法老師！ 第2部](../Page/悠久持有者.md "wikilink")（雪廣綾香）
  - [寶石之國](../Page/寶石之國.md "wikilink")（**黃鑽石**）
  - [如果有妹妹就好了。](../Page/如果有妹妹就好了。.md "wikilink")（三田洞）

<!-- end list -->

  - 2018年

<!-- end list -->

  - [狐狸之聲](../Page/狐狸之聲.md "wikilink")（主持人）

### OVA

**2002年**

  - [熱帶雨林的爆笑生活DELUXE](../Page/熱帶雨林的爆笑生活.md "wikilink")（由米）

**2003年**

  - 熱帶雨林的爆笑生活FINAL（由米）

**2005年**

  - （**伊萊斯**）

**2006年**

  - [網球王子OVA 全國大會篇](../Page/網球王子_\(動畫\).md "wikilink")（**越前龍馬**）
  - [魔法老師！？ 春](../Page/魔法老師_\(動畫\).md "wikilink")（**雪廣綾香**）
  - 魔法老師！？ 夏（**雪廣綾香**）
  - [瑪莉亞的凝望](../Page/瑪莉亞的凝望.md "wikilink")（有栖川金太郎）

**2007年**

  - [草莓棉花糖](../Page/草莓棉花糖.md "wikilink")（笹塚）
  - [櫻花大戰 紐約篇](../Page/櫻花大戰系列.md "wikilink")（Sagiitta Weinberg）
  - [ICE](../Page/ICE_\(動畫\).md "wikilink")（**ヒトミ**）
  - 網球王子OVA 全国大会篇 Semifinal（**越前龍馬**）
  - [ARIA The OVA 〜ARIETTA〜](../Page/水星領航員.md "wikilink")（晃·E·費拉利）

**2008年**

  - 網球王子OVA 全国大会篇 final（**越前龍馬**）
  - 魔法老師 白之翼 ALA ALBA（**雪廣綾香**）

**2009年**

  - 草莓棉花糖 encore（笹塚）
  - 網球王子OVA ANOTHER STORY 〜過去與未來的訊息（**越前龍馬**）
  - 魔法老師 另一个世界（**雪廣綾香**）

**2010年**

  - [DARKER THAN BLACK -黑之契約者-
    外傳](../Page/DARKER_THAN_BLACK.md "wikilink")（**小姐**）
  - 魔法老師 另一個世界Extra 魔法少女夕映（**雪廣綾香**）

**2011年**

  - （則武薰子）

  - 網球王子OVA ANOTHER STORY II 〜那時的我們（**越前龍馬**）

**2012年**

  - [史上最強弟子兼一 闇黑襲擊](../Page/史上最強弟子兼一.md "wikilink")（白鳥）
  - [Code Geass 反叛的魯路修
    娜娜莉夢遊仙境](../Page/Code_Geass_反叛的魯路修.md "wikilink")（柯內莉亞·Li·不列顛尼亞）

**2013年**

  - [鋼鐵人：噬甲病毒崛起](../Page/漫威日本動畫.md "wikilink")（瑪麗亞·希爾）

**2014年**

  - （瑪麗亞·希爾）

  - [戰敗女王](../Page/女王之刃_\(動畫\).md "wikilink")（維爾貝利亞）

  - [最近，妹妹的樣子有點怪？](../Page/最近，妹妹的樣子有點怪？.md "wikilink")（島津）

  - [新網球王子OVA vs Genius10Vol](../Page/網球王子_\(動畫\).md "wikilink").
    1（**越前龍馬**）

  - 新網球王子OVA vs Genius10 Vol. 2（**越前龍馬**）

**2015年**

  - 新網球王子OVA vs Genius10 Vol. 3（**越前龍馬**）
  - 新網球王子OVA vs Genius10 Vol. 4（**越前龍馬**）
  - 新網球王子OVA vs Genius10 Vol. 5（**越前龍馬**）
  - [晨曦公主](../Page/晨曦公主.md "wikilink")（**悠**）

**2017年**

  - [SUPER LOVERS](../Page/SUPER_LOVERS.md "wikilink") OAD1（**海棠零**）
  - SUPER LOVERS OAD2（**海棠零**）

### 剧场版

  - [網球王子劇場版：二人武士 The First
    Game](../Page/網球王子_\(動畫\).md "wikilink")：**越前龍馬**
  - [網球王子劇場版：跡部的禮物 獻給你的網球祭](../Page/網球王子_\(動畫\).md "wikilink")：**越前龍馬**
  - [剧场版 魔法老师！ ANIME
    FINAL](../Page/剧场版_魔法老师！_ANIME_FINAL.md "wikilink")（**雪廣綾香**）
  - [網球王子劇場版：決戰英國網球城！](../Page/網球王子_\(動畫\).md "wikilink")：**越前龍馬**

### 遊戲

  - [英雄傳說 軌跡系列](../Page/英雄傳說_軌跡系列.md "wikilink")（**瓦奇·赫密士菲亞**）
      - \[PSP\][英雄傳說 零之軌跡](../Page/英雄傳說_零之軌跡.md "wikilink")
      - \[PSP\][英雄傳說 碧之軌跡](../Page/英雄傳說_碧之軌跡.md "wikilink")
      - \[PS VITA\] [英雄傳說 零之軌跡
        Evolution](../Page/英雄傳說_零之軌跡.md "wikilink")
      - \[PS VITA\] [英雄傳說 碧之軌跡
        Evolution](../Page/英雄傳說_碧之軌跡.md "wikilink")
  - [無雙☆群星大會串](../Page/無雙☆群星大會串.md "wikilink") - 米蕾妮亞
  - [信賴鈴音～蕭邦之夢～](../Page/信賴鈴音～蕭邦之夢～.md "wikilink")（Waltz）
  - [魔法老师系列](../Page/魔法老师.md "wikilink")（**[雪廣綾香](../Page/雪廣綾香.md "wikilink")**）
  - [時空幻境心靈傳奇R](../Page/時空幻境心靈傳奇R.md "wikilink") - 茵卡洛斯
  - [櫻花大戰V](../Page/櫻花大戰.md "wikilink") -
  - [NANA](../Page/NANA.md "wikilink") - 大崎娜娜
  - [心跳回忆](../Page/心跳回忆.md "wikilink")3 -
  - [零～刺青之聲](../Page/零_\(遊戲\).md "wikilink") - 黑澤怜、久世零華
  - [龍影魔咒](../Page/龍影魔咒.md "wikilink") - ジェフティ
  - [ARIA The NATURAL～遙遠記憶之幻象～](../Page/水星領航員.md "wikilink") - 晃・E・法拉利
  - [Remember11 -the age of
    infinity-](../Page/Remember11_-the_age_of_infinity-.md "wikilink") -
    楠田 由仁
  - [任俠傳 渡世人一代記](../Page/任俠傳_渡世人一代記.md "wikilink") - 蘭太夫
  - [召喚夜響曲 X ～TEARS CRONW～](../Page/召喚夜響曲_X_～TEARS_CRONW～.md "wikilink")
    - 璐加(ルーか)
  - [嘉芙蓮 (遊戲)](../Page/嘉芙蓮_\(遊戲\).md "wikilink") - 謎の聲（亞斯塔羅特）
  - [發明工坊2](../Page/發明工坊2.md "wikilink") - 費兒麗．哈謝斐勒
  - [生死格鬥4](../Page/生死格鬥4.md "wikilink") - 伊利歐特
  - [鬼武者魂](../Page/鬼武者魂.md "wikilink") - 小姓、武將
  - [生化危機6](../Page/生化危機6.md "wikilink") - 艾達·王（Ada Wong）
  - [神奇寶貝黑白版2宣傳片](../Page/神奇寶貝黑白版2宣傳片.md "wikilink") - 勁敵
  - [烈火之炎ps2](../Page/烈火之炎ps2.md "wikilink") - 亞希 (動畫版為緒方惠美配音)
  - [網球王子ps](../Page/網球王子.md "wikilink")2 - **越前龍馬**
  - [網球王子gba](../Page/網球王子.md "wikilink") - **越前龍馬**
  - [網球王子nds](../Page/網球王子.md "wikilink") - **越前龍馬**
  - 新網球王子Rising Beat-**越前龍馬**
  - 夢幻遊戲朱雀異聞 - 柳宿
  - 為了誰的鍊金術師(手機遊戲) - 卡莉絲

### 演出

  - [鋼之鍊金術師漫畫CD集](../Page/鋼之鍊金術師.md "wikilink") - 愛德華‧愛力克

### 廣播劇

  - [惱殺ジャンキー(色誘中毒)](../Page/惱殺ジャンキー\(色誘中毒\).md "wikilink") -
    **梶原海(梢原海)**
  - [潘朵拉之心](../Page/潘朵拉之心.md "wikilink") - **オズ=ベザリウス (奧茲=貝薩流士)**
  - [少年同盟](../Page/少年同盟.md "wikilink") - **淺羽祐希**
  - 紅心王子-**櫻紅次郎**
  - 黃昏乙女×失憶幽靈-**新谷貞一**
  - 微憂青春日記 - 籐縞寶

#### 廣播連續劇

  - [VOMIC](../Page/VOMIC.md "wikilink")
      - [境界觸發者](../Page/境界觸發者.md "wikilink") - **空閑遊真**

## 參考資料

## 外部連結

  - [個人網站](https://web.archive.org/web/20080730191423/http://www.junko-minagawa.com/)

[Category:日本女性配音員](../Category/日本女性配音員.md "wikilink")
[Category:茨城縣出身人物](../Category/茨城縣出身人物.md "wikilink")

1.