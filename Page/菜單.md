[Businesslunchmenu.sergeyrest.moscow.jpg](https://zh.wikipedia.org/wiki/File:Businesslunchmenu.sergeyrest.moscow.jpg "fig:Businesslunchmenu.sergeyrest.moscow.jpg")一家餐館的商務套餐菜單\]\]
[Yungkee-menu.jpg](https://zh.wikipedia.org/wiki/File:Yungkee-menu.jpg "fig:Yungkee-menu.jpg")的菜牌\]\]

**菜單**，粵語稱為**餐牌**、**菜牌**，是[餐廳供顧客在進餐時選擇所進食菜色的工具](../Page/餐廳.md "wikilink")，一般分[套餐或](../Page/套餐.md "wikilink")[散餐兩種](../Page/散餐.md "wikilink")，然後視乎時間和情況再分門別類。例如西方早餐的菜單可以分為[歐陸式早餐](../Page/歐陸式早餐.md "wikilink")
（[麵包](../Page/麵包.md "wikilink")、[咖啡或](../Page/咖啡.md "wikilink")[茶](../Page/茶.md "wikilink")）和[英式早餐](../Page/英式早餐.md "wikilink")
（歐陸式早餐再加上[雞蛋](../Page/雞蛋.md "wikilink")、[肉類](../Page/肉類.md "wikilink")、[果汁](../Page/果汁.md "wikilink")、[麥片](../Page/麥片.md "wikilink")、[牛奶等](../Page/牛奶.md "wikilink")）兩大類。

有時餐廳買到新鮮的食材
（特別是[海鮮](../Page/海鮮.md "wikilink")）或者有當日特色或特價菜，會在當眼處列出或靠[侍應生口耳相傳](../Page/侍應生.md "wikilink")，提醒食客。

## 歷史

在[中國](../Page/中國.md "wikilink")，[宋朝](../Page/宋朝.md "wikilink")（西元960-1279年）初期，[商人中产阶级中在城市开店的业主](../Page/四民.md "wikilink")，由于常常在家吃饭的时间不多，所以他们便大胆地在各式各样场所，如庙宇、客栈、茶馆、熟食档、以及做附近的妓院、歌屋、[戏班场饮食生意的](../Page/宋朝文化#戏曲.md "wikilink")[酒家吃饭](../Page/餐馆.md "wikilink")；这些[旅居中土的外国人](../Page/宋朝社会#本地、外国、宗教等少数人口.md "wikilink")，以及那些自全国各地聚居城市的中国人由于烹调方式间的差异，促使做饮食业生意的想尽办法满足种种不同的口味，由此而造成菜单的崛起。\[1\]\[2\]

至於在[歐洲](../Page/歐洲.md "wikilink")，菜单最初并非为向客人提供菜肴内容而制作的，而是厨师为了备忘而写的单子，英文为**menu**。16世纪由法国皇家厨师为了记住意大利菜肴而发明。

於香港，身兼設計師與收藏家John
Wu更喜歡收藏舊時的�餐牌包括1970年代的中環希爾頓酒店餐牌、半島酒店Gaddi’s第一代的餐牌、1960年代的手寫晚宴菜單等等\[3\]。

## 程序

食客看[餐牌](../Page/餐牌.md "wikilink")，選擇點菜餚，[侍應為客人寫下](../Page/侍應.md "wikilink")，成為「菜單」，之後交給[廚房](../Page/廚房.md "wikilink")，[廚師按菜單的指示](../Page/廚師.md "wikilink")[烹飪食物](../Page/烹飪.md "wikilink")。

## 相關

  - [食譜是教授烹飪的](../Page/食譜.md "wikilink")[書籍](../Page/書籍.md "wikilink")，不应与「菜單」或「菜牌」相混淆。

## 参考资料

[Category:飲食](../Category/飲食.md "wikilink")
[Category:文件](../Category/文件.md "wikilink")

1.  West (1997), 70–76.
2.  Gernet (1962), 133–134, 137.
3.  [珍藏香港舊餐牌　收藏家追憶黃金年代](https://bkb.mpweekly.com/%E9%A3%B2%E9%A3%9F/20180713-78292)