**吉本興業株式会社**（Yoshimoto Kogyo Co.,
Ltd.），通稱**「吉本」**、**「」（yoshimoto）**，[日本的大型](../Page/日本.md "wikilink")[藝人](../Page/藝人.md "wikilink")[經紀公司](../Page/經紀公司.md "wikilink")、[電視節目製作公司](../Page/電視節目.md "wikilink")；創業於1912年（明治45年）4月1日，擁有超過百年歷史，是日本最古老的藝能事務所，也是[吉本興業集團的](../Page/吉本興業集團.md "wikilink")[母公司](../Page/母公司.md "wikilink")。總公司設置於[大阪府](../Page/大阪府.md "wikilink")[大阪市](../Page/大阪市.md "wikilink")[中央區以及](../Page/中央區_\(大阪市\).md "wikilink")[東京都](../Page/東京都.md "wikilink")[千代田區](../Page/千代田區.md "wikilink")[神田神保町](../Page/神田神保町.md "wikilink")，吉本方面自2002年以來稱呼大阪總公司為**大阪本部**、東京總公司為**東京本部**。為[日本經濟團體聯合會與](../Page/日本經濟團體聯合會.md "wikilink")[日本商品化權協會會員](../Page/日本商品化權協會.md "wikilink")。

## 主要幹事

<table>
<thead>
<tr class="header">
<th><p>職務</p></th>
<th><p>幹事名</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>董事會<a href="../Page/董事長.md" title="wikilink">董事長代表</a></p></td>
<td><p><a href="../Page/吉野伊佐男.md" title="wikilink">吉野伊佐男</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>副董事長代表</p></td>
<td><p><a href="../Page/大崎洋.md" title="wikilink">大崎洋</a></p></td>
<td><p>原<a href="../Page/DOWNTOWN.md" title="wikilink">DOWNTOWN的經理</a></p></td>
</tr>
<tr class="odd">
<td><p>董事管理人代表</p></td>
<td><p>河村靜也（<a href="../Page/桂文枝_(6代目).md" title="wikilink">桂文枝</a>）</p></td>
<td><p><a href="../Page/落語.md" title="wikilink">落語家</a>・兼任<a href="../Page/上方落語協會.md" title="wikilink">上方落語協會會長</a></p></td>
</tr>
<tr class="even">
<td><p>常務董事</p></td>
<td><p>若林正裕</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>董事</p></td>
<td><p>橋爪健康</p></td>
<td><p>ROJAM Entertainment Holdings Limited董事會董事長代表<br />
</p></td>
</tr>
<tr class="even">
<td><p>董事</p></td>
<td><p>藤原茂樹</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>董事</p></td>
<td><p>平島治</p></td>
<td><p>大成建設（藝人團體，與大型施工承包商<a href="../Page/大成建設.md" title="wikilink">大成建設無關</a>）協商</p></td>
</tr>
<tr class="even">
<td><p>董事</p></td>
<td><p>原田裕</p></td>
<td><p><a href="../Page/律師.md" title="wikilink">律師</a></p></td>
</tr>
<tr class="odd">
<td><p>執行委員</p></td>
<td><p>竹中功</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/監察人.md" title="wikilink">監察人</a></p></td>
<td><p>山田有人</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>監察人</p></td>
<td><p>蔭山幸夫</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>監察人</p></td>
<td><p>沖津嘉昭</p></td>
<td><p><a href="../Page/岩井證券.md" title="wikilink">岩井證券董事會董事長代表</a></p></td>
</tr>
<tr class="odd">
<td><p>監察人</p></td>
<td><p>水戸重之</p></td>
<td><p>律師</p></td>
</tr>
<tr class="even">
<td><p>特別顧問</p></td>
<td><p>岡本武士（笑福亭仁鶴）</p></td>
<td><p>落語家</p></td>
</tr>
<tr class="odd">
<td><p>文藝顧問</p></td>
<td><p>竹本浩三</p></td>
<td><p><a href="../Page/廣播劇.md" title="wikilink">廣播劇作家</a></p></td>
</tr>
<tr class="even">
<td><p>協商</p></td>
<td><p>石元正春</p></td>
<td><p>原董事</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 概要

能親自進行製作電視節目、劇場演出、以及培訓藝人的事務，尤其在搞笑藝人的經營管理部份展現出了壓倒性的強勢。原先只是一間演出公司，但現在成為以藝能事務所為中心的複合企業。被戲稱為是「搞笑综合商社」、「日本最大的藝能企業」。

公司名稱採用當初創業者自報的姓氏「吉本」，但現在公司的所有者並不是這個姓氏。

現時吉本興業旗下有大約700多名藝人。

另外，吉本興業現時亦是在[香港創業板上市的](../Page/香港創業板.md "wikilink")[寰亞傳媒](../Page/ROJAM.md "wikilink")（ROJAM
Entertainment Holdings
Limited；2000年2月29日成立；）的主要股東；寰亞傳媒由日本著名音樂人[小室哲哉及其歌手前妻](../Page/小室哲哉.md "wikilink")[吉田麻美共同在](../Page/吉田麻美.md "wikilink")[香港創立](../Page/香港.md "wikilink")，成立初期在大中華地區從事全面娛樂事業，並曾簽下多位女新人歌手（其中兩位當時的歌手[陳敏之及](../Page/陳敏之.md "wikilink")[譚凱琪現時分別是](../Page/譚凱琪.md "wikilink")[香港無綫電視藝員及](../Page/電視廣播有限公司.md "wikilink")[驕陽音樂旗下歌手](../Page/驕陽音樂.md "wikilink")）。2002年9月30日，ROJAM收購[上海龍傑娛樂有限公司](../Page/上海龍傑娛樂有限公司.md "wikilink")（原[上海智鴻娛樂有限公司](../Page/上海智鴻娛樂有限公司.md "wikilink")）90%註冊股本，現時業務主要集中在[中國大陸](../Page/中國大陸.md "wikilink")。2004年12月24日，ROJAM成為吉本興業與[Fandango](../Page/Fandango.md "wikilink")（Fandango,
Inc.）之附屬公司。2007年10月31日，ROJAM完成收購[深圳樂酷信息技術有限公司之全部權益](../Page/深圳樂酷信息技術有限公司.md "wikilink")。2008年3月31日，ROJAM於[台灣](../Page/台灣.md "wikilink")[台北市成立了全資](../Page/台北市.md "wikilink")[子公司](../Page/子公司.md "wikilink")「香港商龍傑多媒體股份有限公司台灣分公司」，在台灣銷售吉本興業[授權影音製品](../Page/授權.md "wikilink")。2010年12月，吉本興業於首爾和台北市成立海外分公司，於台北市成立「台北吉本娛樂事業股份有限公司」。

## 沿革

[Yoshimoto_kogyo_tokyo_office_2009.JPG](https://zh.wikipedia.org/wiki/File:Yoshimoto_kogyo_tokyo_office_2009.JPG "fig:Yoshimoto_kogyo_tokyo_office_2009.JPG")

  - 1912年4月1日，[吉本吉兵衛](../Page/吉本吉兵衛.md "wikilink")（本名[吉本吉次郎](../Page/吉本吉次郎.md "wikilink")，通稱[泰三](../Page/泰三.md "wikilink")）及其妻[吉本勢收購位於](../Page/吉本勢.md "wikilink")[大阪市北區](../Page/大阪市.md "wikilink")[天神橋的](../Page/天神橋.md "wikilink")「第二文藝館」，經營[寄席](../Page/寄席.md "wikilink")。
  - 1913年1月，吉本夫婦在大阪市南區笠屋町（今大阪市中央區東[心齋橋](../Page/心齋橋.md "wikilink")）設立「吉本興行部」。
  - 1922年1月，吉本夫婦收購位於[東京都](../Page/東京都.md "wikilink")[千代田區](../Page/千代田區.md "wikilink")[神田的寄席](../Page/神田_\(千代田區\).md "wikilink")「川竹亭」，改名為「神田花月」。
  - 1922年5月，吉本夫婦收購位於[橫濱市](../Page/橫濱市.md "wikilink")[伊勢佐木町的寄席](../Page/伊勢佐木町.md "wikilink")「新富亭」。
  - 1923年，新富亭改名為「橫濱花月」。
  - 1924年，吉本吉兵衛因急性[心肌梗塞逝世](../Page/心肌梗塞.md "wikilink")。
  - 1932年3月1日，吉本興行部改組為「吉本興業合名會社」並設立東京支社，[林弘高擔任第一屆支社長](../Page/林弘高.md "wikilink")。
  - 1933年，吉本興業成立[電影部](../Page/電影.md "wikilink")。
  - 1935年11月，吉本興業東京支社的大本營「淺草花月劇場」開幕。
  - 1941年，[情報局與](../Page/情報局.md "wikilink")[大政翼贊會主導成立](../Page/大政翼贊會.md "wikilink")「[移動演劇聯盟](../Page/移動演劇聯盟.md "wikilink")」，吉本興業跟進成立「吉本移動演劇隊」展開全日本巡演。
  - 1943年2月13日，當時吉本興業持有的大阪市[通天閣](../Page/通天閣.md "wikilink")（第一代）雖已被大火燒毀，卻停止重建，而被[大阪府廳以](../Page/大阪府廳.md "wikilink")「獻納」名目拆除，拆除所得的鋼材等原料投入軍工生產中。
  - 1945年3月10日，由於[東京大轟炸](../Page/東京大轟炸.md "wikilink")，神田花月與江東花月被燒毀，吉本興業在[神奈川縣的劇場也遭波及](../Page/神奈川縣.md "wikilink")。
  - 1946年10月，吉本興業東京支社正式脫離吉本興業，獨立為「吉本株式會社」。
  - 1946年11月，吉本株式會社成立「太泉映畫株式會社」（[東映前身之一](../Page/東映.md "wikilink")）。
  - 1948年1月7日，吉本興業合名會社改組而成的「吉本興業株式會社」開業，但最後因業績下滑而聲請[企業重整](../Page/企業重整.md "wikilink")。
  - 1949年5月14日，吉本興業在[大阪證券交易所](../Page/大阪證券交易所.md "wikilink")[股票上市](../Page/股票上市.md "wikilink")。
  - 1950年3月14日，吉本勢逝世。
  - 1959年3月1日，吉本興業將其持有的電影院「梅田Ground劇場」改裝而成的演藝場「梅田花月」開幕。
  - 1961年10月2日，吉本興業在[東京證券交易所股票上市](../Page/東京證券交易所.md "wikilink")。
  - 1962年，「京都花月」開幕。
  - 1963年，「難波花月」開幕。
  - 2005年，吉本興業、Faith、Fandango與日本[英特爾組成](../Page/英特爾.md "wikilink")[策略聯盟](../Page/策略聯盟.md "wikilink")，在美國設立[Bellrock
    Media](../Page/Bellrock_Media.md "wikilink")。
  - 2007年10月1日，吉本興業實施[控股公司制度](../Page/控股公司.md "wikilink")。
  - 2008年，吉本興業東京總公司從東京都[新宿區](../Page/新宿區.md "wikilink")[新宿](../Page/新宿.md "wikilink")5丁目搬遷至東京都千代田區神田神保町。
  - 2009年，吉本興業與美國第一大演藝經紀公司[Creative Artists
    Agency](../Page/Creative_Artists_Agency.md "wikilink")（CAA）締結合作關係。
  - 2010年2月24日，吉本興業在[東京證券交易所及](../Page/東京證券交易所.md "wikilink")[大阪證券交易所](../Page/大阪證券交易所.md "wikilink")[股票下市](../Page/股票下市.md "wikilink")。
  - 2010年12月，吉本興業於首爾及台北市成立海外分社，於台北市成立「台北吉本娛樂事業股份有限公司」。
  - 2011年，吉本興業台灣分社除了經紀事務之外，也執行了許多大型專案，如東映「[假面騎士](../Page/假面騎士.md "wikilink")」2011台北公演與日本最大大阪燒[道頓堀御好燒第一家台北分店開幕](../Page/道頓堀御好燒.md "wikilink")。

## 搞笑藝人

  - [西川清](../Page/西川清.md "wikilink")
  - [桂三枝](../Page/桂三枝.md "wikilink")
  - [笑福亭仁鶴](../Page/笑福亭仁鶴.md "wikilink")
  - [コメディNo.1](../Page/コメディNo.1.md "wikilink")
      - [坂田利夫](../Page/坂田利夫.md "wikilink")
      - [前田五郎](../Page/前田五郎.md "wikilink")
  - [明石家秋刀魚](../Page/明石家秋刀魚.md "wikilink")
  - [島田紳助](../Page/島田紳助.md "wikilink")
  - [オール阪神・巨人](../Page/オール阪神・巨人.md "wikilink")
  - [間寛平](../Page/間寛平.md "wikilink")
  - [B\&B](../Page/B&B.md "wikilink")
      - [西川のりお](../Page/西川のりお.md "wikilink")
      - [上方よしお](../Page/上方よしお.md "wikilink")
  - [桂文珍](../Page/桂文珍.md "wikilink")
  - [月亭八方](../Page/月亭八方.md "wikilink")
  - [中田カウス・ボタン](../Page/中田カウス・ボタン.md "wikilink")
  - [大木こだま・ひびき](../Page/大木こだま・ひびき.md "wikilink")
  - [大平サブロー](../Page/大平サブロー.md "wikilink")
  - [大平シロー](../Page/大平シロー.md "wikilink")
  - [DOWN TOWN](../Page/DOWN_TOWN.md "wikilink")
      - [松本人志](../Page/松本人志.md "wikilink")
      - [浜田雅功](../Page/浜田雅功.md "wikilink")
  - [130R](../Page/130R.md "wikilink")
      - [板尾創路](../Page/板尾創路.md "wikilink")
      - [藏野孝洋](../Page/藏野孝洋.md "wikilink")
  - [今田耕司](../Page/今田耕司.md "wikilink")
  - [東野幸治](../Page/東野幸治.md "wikilink")
  - [木村祐一](../Page/木村祐一.md "wikilink")
  - [小米良啓太](../Page/小米良啓太.md "wikilink")
  - [リットン調査団](../Page/リットン調査団_\(お笑いコンビ\).md "wikilink")
      - [水野透](../Page/水野透.md "wikilink")
      - [藤原光博](../Page/藤原光博.md "wikilink")
  - [山崎邦正](../Page/山崎邦正.md "wikilink")
  - [山田花子](../Page/山田花子.md "wikilink")
  - [NINETY-NINE](../Page/NINETY-NINE.md "wikilink")
      - [岡村隆史](../Page/岡村隆史.md "wikilink")
      - [矢部浩之](../Page/矢部浩之.md "wikilink")
  - [Razor Ramon](../Page/Razor_Ramon.md "wikilink")
      - [HG(住谷正樹)](../Page/HG\(住谷正樹\).md "wikilink")
      - [RG(出渕誠)](../Page/RG\(出渕誠\).md "wikilink")
  - [雨后敢死队](../Page/雨后敢死队.md "wikilink")
      - [宮迫博之](../Page/宮迫博之.md "wikilink")
      - [萤原徹](../Page/萤原徹.md "wikilink")
  - [鐵公雞二人組](../Page/鐵公雞二人組.md "wikilink")
      - [遠藤章造](../Page/遠藤章造.md "wikilink")
      - [田中直樹](../Page/田中直樹.md "wikilink")
  - [藤井隆](../Page/藤井隆.md "wikilink")
  - [河内家菊水丸](../Page/河内家菊水丸.md "wikilink")
  - [千原兄弟](../Page/千原兄弟.md "wikilink")
      - [千原靖史](../Page/千原靖史.md "wikilink")
      - [千原浩史](../Page/千原浩史.md "wikilink")
  - [ジャリズム](../Page/ジャリズム.md "wikilink")
      - [渡辺鐘](../Page/渡辺鐘.md "wikilink")
      - [山下しげのり](../Page/山下しげのり.md "wikilink")
  - [FUJIWARA](../Page/FUJIWARA.md "wikilink")
      - [原西孝幸](../Page/原西孝幸.md "wikilink")
      - [藤本敏史](../Page/藤本敏史.md "wikilink")
  - [水牛吾郎](../Page/水牛吾郎.md "wikilink")
      - [木村明浩](../Page/木村明浩.md "wikilink")
      - [竹若元博](../Page/竹若元博.md "wikilink")
  - [池乃](../Page/池乃.md "wikilink")
  - [倫敦靴子](../Page/倫敦靴子.md "wikilink")
      - [田村淳](../Page/田村淳.md "wikilink")
      - [田村亮](../Page/田村亮.md "wikilink")
  - [東方收音機](../Page/東方收音機.md "wikilink")
      - [藤森慎吾](../Page/藤森慎吾.md "wikilink")
      - [中田敦彦](../Page/中田敦彦.md "wikilink")
  - [中川家](../Page/中川家.md "wikilink")
      - [中川剛](../Page/中川剛.md "wikilink")
      - [中川礼二](../Page/中川礼二.md "wikilink")
  - [加藤浩次](../Page/加藤浩次.md "wikilink")
  - [DonDokoDon](../Page/DonDokoDon_\(お笑いコンビ\).md "wikilink")
      - [山口智充](../Page/山口智充.md "wikilink")
      - [平畠啓史](../Page/平畠啓史.md "wikilink")
  - [車庫拍賣](../Page/車庫拍賣.md "wikilink")
      - [猩爺](../Page/猩爺.md "wikilink")
      - [川田広樹](../Page/川田広樹.md "wikilink")
  - [ペナルティ](../Page/ペナルティ.md "wikilink")
  - [ライセンス](../Page/ライセンス_\(お笑い芸人\).md "wikilink")
  - [陣内智則](../Page/陣内智則.md "wikilink")
  - [ランディーズ](../Page/ランディーズ.md "wikilink")
      - [高井俊彦](../Page/高井俊彦.md "wikilink")
      - [中川貴志](../Page/中川貴志.md "wikilink")
  - [シャンプーハット](../Page/シャンプーハット_\(お笑い芸人\).md "wikilink")
  - [アップダウン](../Page/アップダウン.md "wikilink")
  - [宮川大助・花子](../Page/宮川大助・花子.md "wikilink")
  - [今いくよ・くるよ](../Page/今いくよ・くるよ.md "wikilink")
  - [ハイヒール](../Page/ハイヒール_\(お笑い\).md "wikilink")
      - [ハイヒールモモコ](../Page/ハイヒールモモコ.md "wikilink")
      - [ハイヒールリンゴ](../Page/ハイヒールリンゴ.md "wikilink")
  - [村上ショージ](../Page/村上ショージ.md "wikilink")
  - [Mr.オクレ](../Page/Mr.オクレ.md "wikilink")
  - [ネゴシックス](../Page/ネゴシックス.md "wikilink")
  - [トミーズ](../Page/トミーズ.md "wikilink")
      - [トミーズ雅](../Page/トミーズ雅.md "wikilink")
      - [トミーズ健](../Page/トミーズ健.md "wikilink")
  - [品川庄司](../Page/品川庄司.md "wikilink")
      - [品川祐](../Page/品川祐.md "wikilink")
      - [庄司智春](../Page/庄司智春.md "wikilink")
  - [足球時間](../Page/足球時間.md "wikilink")
      - [後藤輝基](../Page/後藤輝基.md "wikilink")
      - [岩尾望](../Page/岩尾望.md "wikilink")
  - [ハリガネロック](../Page/ハリガネロック.md "wikilink")
  - [ケンドーコバヤシ](../Page/ケンドーコバヤシ.md "wikilink")
  - [ザ・プラン9](../Page/ザ・プラン9.md "wikilink")
  - [2丁拳銃](../Page/2丁拳銃.md "wikilink")
      - [小堀裕之](../Page/小堀裕之.md "wikilink")
      - [川谷修士](../Page/川谷修士.md "wikilink")
  - [次長課長](../Page/次長課長.md "wikilink")
  - [ルート33](../Page/ルート33.md "wikilink")
  - [ロザン](../Page/ロザン.md "wikilink")
      - [宇治原史規](../Page/宇治原史規.md "wikilink")
      - [菅廣史](../Page/菅廣史.md "wikilink")
  - [TUTORIAL](../Page/Tutorial.md "wikilink")
      - [德井義實](../Page/德井義實.md "wikilink")
      - [福田充德](../Page/福田充德.md "wikilink")
  - [代田光](../Page/代田光.md "wikilink")
  - [友近](../Page/友近.md "wikilink")
  - [なかやまきんに君](../Page/なかやまきんに君.md "wikilink")
  - [キングコング](../Page/キングコング_\(お笑い\).md "wikilink")
      - [梶原雄太](../Page/梶原雄太.md "wikilink")
      - [西野亮廣](../Page/西野亮廣.md "wikilink")
  - [三瓶](../Page/三瓶.md "wikilink")
  - [森三中](../Page/森三中.md "wikilink")
  - [羅伯特](../Page/ロバート_\(お笑い\).md "wikilink")
  - [インパルス](../Page/インパルス_\(お笑い\).md "wikilink")
  - [タカアンドトシ](../Page/タカアンドトシ.md "wikilink")
  - [かつみ♥さゆり](../Page/かつみ・さゆり.md "wikilink")
  - [なるみ](../Page/なるみ.md "wikilink")
  - [笑い飯](../Page/笑い飯.md "wikilink")
  - [千鳥](../Page/千鳥_\(搞笑組合\).md "wikilink")
  - [麒麟](../Page/麒麟_\(お笑いコンビ\).md "wikilink")
      - [川島明](../Page/川島明.md "wikilink")
      - [田村裕](../Page/田村裕.md "wikilink")
  - [南海甜心](../Page/南海甜心.md "wikilink")
      - [小山](../Page/山里亮太.md "wikilink")
      - [小靜](../Page/山崎静代.md "wikilink")
  - [ダイノジ](../Page/ダイノジ.md "wikilink")
  - [松本竜助](../Page/松本竜助.md "wikilink")
  - [POISON GIRL BAND](../Page/POISON_GIRL_BAND.md "wikilink")
  - [おはよう。](../Page/おはよう。.md "wikilink")
  - [たいぞう](../Page/たいぞう.md "wikilink")
  - [石田靖](../Page/石田靖.md "wikilink")（*本来は新喜劇座長だが、主に東京で活動中のためこのカテゴリーに入れた*）
  - [大山英雄](../Page/大山英雄.md "wikilink")
  - [中西喜美恵](../Page/中西喜美恵.md "wikilink")
  - [長原成樹](../Page/長原成樹.md "wikilink")
  - [紫SHIKIBU](../Page/紫SHIKIBU.md "wikilink")
      - [宮迫博之](../Page/宮迫博之.md "wikilink")
      - [螢原徹](../Page/螢原徹.md "wikilink")
      - [渡邊鐘](../Page/渡邊鐘.md "wikilink")
      - [田中直樹](../Page/田中直樹.md "wikilink")
      - [ガレッジセール](../Page/ガレッジセール.md "wikilink")
      - [山下しげのり](../Page/山下しげのり.md "wikilink")

## 新喜劇

**座長**

  - [内場勝則](../Page/内場勝則.md "wikilink")
  - [辻本茂雄](../Page/辻本茂雄.md "wikilink")
  - [すっちー](../Page/すっちー.md "wikilink")
  - [小籔千豊](../Page/小籔千豊.md "wikilink")
  - [川畑泰史](../Page/川畑泰史.md "wikilink")
  - [酒井藍](../Page/酒井藍.md "wikilink")

**NGKレギュラー**

  - [青野敏行](../Page/青野敏行.md "wikilink")
  - [秋田久美子](../Page/秋田久美子.md "wikilink")
  - [浅香あき恵](../Page/浅香あき恵.md "wikilink")
  - [伊賀健二](../Page/伊賀健二.md "wikilink")
  - [五十嵐サキ](../Page/五十嵐サキ.md "wikilink")
  - [池乃めだか](../Page/池乃めだか.md "wikilink")
  - [井上竜夫](../Page/井上竜夫.md "wikilink")
  - [今別府直之](../Page/今別府直之.md "wikilink")
  - [井村勝](../Page/井村勝.md "wikilink")
  - [烏川耕一](../Page/烏川耕一.md "wikilink")
  - [宇都宮まき](../Page/宇都宮まき.md "wikilink")
  - [川畑泰史](../Page/川畑泰史.md "wikilink")
  - [桑原和男](../Page/桑原和男.md "wikilink")
  - [小米良啓太](../Page/小米良啓太.md "wikilink")
  - [島木譲二](../Page/島木譲二.md "wikilink")
  - [島田一之介](../Page/島田一之介.md "wikilink")
  - [清水キョウイチ郎](../Page/清水キョウイチ郎.md "wikilink")
  - [末成由美](../Page/末成由美.md "wikilink")
  - [たいぞう](../Page/たいぞう.md "wikilink")
  - [高橋靖子](../Page/高橋靖子.md "wikilink")
  - [中條健一](../Page/中條健一.md "wikilink")
  - [チャーリー浜](../Page/チャーリー浜.md "wikilink")
  - [ぢゃいこ](../Page/ぢゃいこ.md "wikilink")
  - [中恭太](../Page/中恭太.md "wikilink")
  - [中田はじめ](../Page/中田はじめ.md "wikilink")
  - [なかやまきんに君](../Page/なかやまきんに君.md "wikilink")
  - [中山美保](../Page/中山美保.md "wikilink")
  - [西科仁](../Page/西科仁.md "wikilink")
  - [平山昌雄](../Page/平山昌雄.md "wikilink")
  - [別所清一](../Page/別所清一.md "wikilink")
  - [松下笑一](../Page/松下笑一.md "wikilink")
  - [未知やすえ](../Page/未知やすえ.md "wikilink")
  - [南出一葉](../Page/南出一葉.md "wikilink")
  - [宮崎高章](../Page/宮崎高章.md "wikilink")
  - [村上斉範](../Page/村上斉範.md "wikilink")
  - [安尾信乃助](../Page/安尾信乃助.md "wikilink")
  - [山田亮](../Page/山田亮.md "wikilink")
  - [山本奈臣実](../Page/山本奈臣実.md "wikilink")

以上。

  - [その他吉本新喜劇メンバー](../Page/その他吉本新喜劇メンバー.md "wikilink")

## 藝人與演员

  - [小室哲哉（已離開）](../Page/小室哲哉.md "wikilink")
  - [池脇千鶴](../Page/池脇千鶴.md "wikilink")
  - [小川菜摘](../Page/小川菜摘.md "wikilink")
  - [名高達男](../Page/名高達男.md "wikilink")
  - [酒井美紀](../Page/酒井美紀.md "wikilink")
  - [鈴木美智子](../Page/鈴木美智子.md "wikilink")
  - [大阪プロレス](../Page/大阪プロレス.md "wikilink")
  - [デーモン小暮閣下](../Page/デーモン小暮閣下.md "wikilink")
  - [ちすん](../Page/ちすん.md "wikilink")
  - [西興一朗](../Page/西興一朗.md "wikilink")
  - [岡村靖幸](../Page/岡村靖幸.md "wikilink")
  - [風花舞](../Page/風花舞.md "wikilink")（[宝冢歌剧团出身](../Page/宝冢歌剧团.md "wikilink")）
  - [竹中繪里](../Page/竹中繪里.md "wikilink")
  - [齊藤林子](../Page/齊藤林子.md "wikilink")
  - [Vivian Hsu（已離開）](../Page/徐若瑄.md "wikilink")
  - [佐藤麻衣](../Page/佐藤麻衣.md "wikilink")
  - [小林優美](../Page/小林優美.md "wikilink")
  - [李亦捷](../Page/李亦捷.md "wikilink")
  - [郭葦昀](../Page/郭葦昀.md "wikilink")
  - [黃泰安](../Page/黃泰安.md "wikilink")
  - [元斌](../Page/元斌.md "wikilink")
  - [Lulu](../Page/Lulu.md "wikilink")
  - [MP魔幻力量（已離開）](../Page/MP魔幻力量.md "wikilink")

## 藝術家

  - [明和電機](../Page/明和電機.md "wikilink")
  - [ジミー大西](../Page/ジミー大西.md "wikilink")
  - [樋口あゆ子](../Page/樋口あゆ子.md "wikilink")

## 相關條目

  - [讀賣巨人](../Page/讀賣巨人.md "wikilink")（參與前身[大日本東京棒球俱樂部的出資](../Page/大日本東京棒球俱樂部.md "wikilink")、經營）

## 外部連結

  - [吉本興業官方網站](http://www.yoshimoto.co.jp/)
  - [YouTube吉本興業官方頻道](http://jp.youtube.com/yoshimotokogyo)

[\*](../Category/吉本興業.md "wikilink")
[Category:日本藝人經紀公司](../Category/日本藝人經紀公司.md "wikilink")
[Category:大阪府公司](../Category/大阪府公司.md "wikilink")
[Category:東京證券交易所已除牌公司](../Category/東京證券交易所已除牌公司.md "wikilink")
[Category:新宿區公司](../Category/新宿區公司.md "wikilink")
[Category:1912年成立的公司](../Category/1912年成立的公司.md "wikilink")
[Y](../Category/日本控股公司.md "wikilink")