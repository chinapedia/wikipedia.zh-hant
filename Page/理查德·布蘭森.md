**理查德·查爾斯·-{zh-cn:尼古拉斯;zh-tw:尼可拉斯;zh-hk:尼古拉斯;}-·布蘭森**爵士（Sir **Richard
Charles Nicholas
Branson**，）是英國[維珍集團的](../Page/維珍集團.md "wikilink")[董事长](../Page/董事长.md "wikilink")。

## 生平

1950年出生在英国[伦敦](../Page/伦敦.md "wikilink")[布莱克希斯](../Page/布莱克希斯.md "wikilink")，从小患有严重的[阅读障碍症](../Page/阅读障碍.md "wikilink")，从唱片生意赚到人生第一桶金，1984年成立[维珍航空](../Page/维珍航空.md "wikilink")。后来建立庞大的商业帝国，[集團旗下包括](../Page/公司集團.md "wikilink")：[維珍航空](../Page/維珍航空.md "wikilink")（[英國航空的主要競爭對手](../Page/英國航空.md "wikilink")）、[維珍鐵路](../Page/維珍鐵路.md "wikilink")、[維珍電訊](../Page/維珍電訊.md "wikilink")、[維珍可樂](../Page/維珍可樂.md "wikilink")、[維珍能源](../Page/維珍能源.md "wikilink")，[連鎖](../Page/連鎖店.md "wikilink")[零售店](../Page/零售店.md "wikilink")[維珍唱片行](../Page/維珍唱片行.md "wikilink")，以及維珍[金融服務](../Page/金融服務.md "wikilink")。他創立的[维珍电影](../Page/维珍电影.md "wikilink")，已被賣給[UGC
Cinemas](../Page/UGC_Cinemas.md "wikilink")，而布蘭森唯一的真正的[媒体方面的尝试](../Page/传播媒体.md "wikilink")：[维珍电台](../Page/维珍电台.md "wikilink")，现在是[苏格兰电台集团的一部分](../Page/苏格兰电台集团.md "wikilink")。\[1\]

## 参考来源

## 外部連結

  - [維珍集團](http://www.virgin.co.uk)

[Category:英國億萬富豪](../Category/英國億萬富豪.md "wikilink")
[Category:英国慈善家](../Category/英国慈善家.md "wikilink")
[Category:英國飛行員](../Category/英國飛行員.md "wikilink")
[Category:维珍集团人物](../Category/维珍集团人物.md "wikilink")
[Category:下級勳位爵士](../Category/下級勳位爵士.md "wikilink")
[Category:TED演讲人](../Category/TED演讲人.md "wikilink")
[Category:失读症患者](../Category/失读症患者.md "wikilink")
[Category:英国动力运动人物](../Category/英国动力运动人物.md "wikilink")
[Category:伦敦作家](../Category/伦敦作家.md "wikilink")
[Category:伦敦企业家](../Category/伦敦企业家.md "wikilink")

1.  [布兰森：最会享受生活的富翁](http://www.people.com.cn/GB/paper2086/13397/1200729.html)，人民网。