[Kwangtung_Provincial_Bank_1949.jpg](https://zh.wikipedia.org/wiki/File:Kwangtung_Provincial_Bank_1949.jpg "fig:Kwangtung_Provincial_Bank_1949.jpg")
[KaiTak_CX_Boeing_747_Final_Approach.jpg](https://zh.wikipedia.org/wiki/File:KaiTak_CX_Boeing_747_Final_Approach.jpg "fig:KaiTak_CX_Boeing_747_Final_Approach.jpg")附近的廣東省銀行分行。\]\]
[Kwangtung_Provincial_Bank_BOC_HK.jpg](https://zh.wikipedia.org/wiki/File:Kwangtung_Provincial_Bank_BOC_HK.jpg "fig:Kwangtung_Provincial_Bank_BOC_HK.jpg")封。\]\]
**廣東省銀行**（），原稱**中央銀行**，在民國十三年（1924年）8月，由[孫中山](../Page/孫中山.md "wikilink")[小舅](../Page/小舅.md "wikilink")[宋子文領頭籌辦成立](../Page/宋子文.md "wikilink")，孫中山當時也曾經在[開幕儀式上發表演說](../Page/開幕儀式.md "wikilink")。廣東中央銀行是[中華民國第一間以](../Page/中華民國.md "wikilink")[中央銀行為名](../Page/中央銀行.md "wikilink")，在[中國](../Page/中國.md "wikilink")[金融](../Page/金融.md "wikilink")[歷史上有一重要的角色](../Page/歷史.md "wikilink")。廣州中央銀行成立後負責發行紙幣\[1\]。後由於各軍閥間混戰，廣東紙幣信用趨於下降\[2\]。1920年代尾開始，[港幣重新取得在廣東金融市場的壟斷地位](../Page/港幣.md "wikilink")。

至民國十八年（1929）三月，兩廣政治分會令飭中央銀行改名為**廣東中央銀行**，以區別南京中央銀行\[3\]，並在[香港開設分行](../Page/香港.md "wikilink")。\[4\]到1931年9月，廣東中央銀行改組為**廣東省銀行**。總行位於廣州市長堤西濠口，該處同時是**廣東省省庫**（）的所在地。省府規定不再發行銀質硬幣，改以毫洋為本位發行紙鈔，即毫洋券\[5\]，又叫做**銀毫券**。1935年11月6日[廣東省財政廳出臺廣東管理貨幣辦法](../Page/广东财政厅旧址.md "wikilink")，指定廣東省銀行所發鈔票，以及廣州市立銀行所發鈔票，為廣東法定貨幣，抗衡中央法幣入粵流通\[6\]。

1949年中共建政後，廣東省銀行在中國大陸的分行皆被關閉。

2001年10月1日，廣東省銀行香港分行被[合併入](../Page/合併.md "wikilink")[中銀香港](../Page/中銀香港.md "wikilink")。\[7\]\[8\]\[9\]

  - 參見：[銀毫](../Page/銀毫.md "wikilink")

## 参考文献

## 外部链接

  -
{{-}}

[Category:廣州歷史](../Category/廣州歷史.md "wikilink")
[Category:广东省政府](../Category/广东省政府.md "wikilink")
[Category:广东经济史](../Category/广东经济史.md "wikilink")
[Category:中國已結業銀行](../Category/中國已結業銀行.md "wikilink")
[Category:香港已結業銀行](../Category/香港已結業銀行.md "wikilink")
[Category:中國銀行](../Category/中國銀行.md "wikilink")
[Category:中华民国大陆时期政府机构](../Category/中华民国大陆时期政府机构.md "wikilink")
[D](../Category/中央銀行_\(中華民國\).md "wikilink")
[Category:1924年廣州建立](../Category/1924年廣州建立.md "wikilink")

1.  蔣祖緣、方志欽主編：《簡明廣東史》，廣東人民出版社
2.  《當代港澳》1995年第2期，第62頁
3.
4.  [民国时期的中央银行与广东省银行](http://www.gzzxws.gov.cn/gzws/cg/cgml/cg8/200808/t20080826_3811.htm)
5.
6.
7.  [民国时期的中央银行与广东省银行](http://www.gzzxws.gov.cn/gzws/cg/cgml/cg8/200808/t20080826_3811.htm)
8.  [什么是中央银行](http://www.shenmeshi.com/Business/Business_20081103152319_4.html)

9.  [国民革命时期中央银行的纸币论析](http://www.cqvip.com/QK/82728X/2005002/15457086.html)