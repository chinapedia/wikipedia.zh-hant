**格律**，指[韻文在创作时的格式](../Page/韻文.md "wikilink")、音律等方面所应遵守的准则。[中国古代](../Page/中国.md "wikilink")[近体诗](../Page/近体诗.md "wikilink")、[词在格律上要求严格](../Page/词_\(文学\).md "wikilink")，其他如[古体诗](../Page/古体诗.md "wikilink")、[现代诗歌](../Page/现代诗歌.md "wikilink")、[欧化诗歌等没有确定的](../Page/欧化诗歌.md "wikilink")、严格的格律要求。

## 格律的要素

诗词格律一般有四大要素：用韵、[平仄](../Page/平仄.md "wikilink")、[对仗](../Page/对仗.md "wikilink")、字数。其中律诗最为严格，必须满足全部要素。近体诗中的绝句以及词、散曲一般不需要对仗。古体诗相对最为宽松，一般只有不严格的用韵的概念。

## 律诗的格律

[律诗共可分为四种不同的](../Page/律诗.md "wikilink")[音韵格式](../Page/音韵.md "wikilink")，即仄起平收式、仄起仄收式、平起仄收式和平起平收式。它们均由四种句式组成：仄仄脚、平平脚、仄平脚和平仄脚。除仄平脚外，其余句式首字的平仄均可更换。其余如[变格](../Page/变格.md "wikilink")、[变体](../Page/变体.md "wikilink")、[拗救等](../Page/拗救.md "wikilink")，不再详述。

律诗在[押韵上](../Page/押韵.md "wikilink")，只允许押平韵，且一般要求押《[平水韵](../Page/平水韵.md "wikilink")》，比词、[散曲的韵严格](../Page/散曲.md "wikilink")。

律诗在[格式上](../Page/格式.md "wikilink")，保留了[骈体文章](../Page/骈体.md "wikilink")、诗歌的特点，要求[颔联](../Page/颔联.md "wikilink")（第二联）、[颈联](../Page/颈联.md "wikilink")（第三联）两联严格[对仗](../Page/对仗.md "wikilink")。

## 词、散曲的格律

词和散曲的格式比较复杂，在不同的[词牌名和](../Page/词牌名.md "wikilink")[曲牌名下](../Page/曲牌名.md "wikilink")，字数、句式、平仄和韵各不相同，具体可查找各[词牌和](../Page/词牌.md "wikilink")[曲牌](../Page/曲牌.md "wikilink")，如：《[定风波](../Page/定风波.md "wikilink")》。押韵上，词依《[词林正韵](../Page/词林正韵.md "wikilink")》，散曲依《[中原音韵](../Page/中原音韵.md "wikilink")》。

詩不比詞、散曲的韻嚴格，其實近體詩僅粗略區分平、仄二聲韻，詞和散曲卻必須細分陰平、陽平，及仄之上去入。其中詞還可常見古人在依譜填詞時有「借入聲為平聲」的通用法則﹙請參宋詞﹚。

## 格律的来由

格律本来自[音乐](../Page/音乐.md "wikilink")，在音乐散佚后，经研究者总结古诗歌的共同规律，便形成了今天看到的格律。

## 对格律的看法

自[白话文运动以来](../Page/白话文运动.md "wikilink")，文学界提倡创作现代诗歌，对格律逐渐不再注意，[新月派的代表人物](../Page/新月派.md "wikilink")[闻一多提出现代诗歌也应遵守一定的格律](../Page/闻一多.md "wikilink")。相较[散文](../Page/散文.md "wikilink")、[小说等](../Page/小说.md "wikilink")[文学体裁](../Page/文学体裁.md "wikilink")，诗歌，尤其是[旧体诗歌](../Page/旧体诗歌.md "wikilink")，越来越遭到冷落。这与全盘西化思潮提倡的[西化](../Page/西化.md "wikilink")[生活方式有密切关系](../Page/生活方式.md "wikilink")。但也有复兴[传统文化的声音](../Page/传统文化.md "wikilink")。

## 参见

  - [平仄](../Page/平仄.md "wikilink")
  - [押韻](../Page/押韻.md "wikilink")
  - [平起式](../Page/平起式.md "wikilink")
  - [仄起式](../Page/仄起式.md "wikilink")

[category:韻文](../Page/category:韻文.md "wikilink")