**羟甲基戊二酸单酰辅酶A还原酶抑制剂**（，简称“**HMG-CoA还原酶抑制剂**”或“**他汀类药物**”）是一类[抗高血脂药](../Page/抗高血脂药.md "wikilink")。因他汀类有降低低密度脂蛋白和高胆固醇的功效。教授Colin
Baigent和他的同事们针对老龄人高血脂引起的心血管疾病预防做了研究，调查接近十九万的样本，实验的结果显示因他汀类有降低心血管疾病的作用\[1\]。

## 药物

  - [阿托伐他汀](../Page/阿托伐他汀.md "wikilink")(Atorvastatin)
  - [西立伐他汀](../Page/西立伐他汀.md "wikilink")(Cerivastatin) 因不良反應已停售
  - [氟伐他汀](../Page/氟伐他汀.md "wikilink")(Fluvastatin)
  - [洛伐他汀](../Page/洛伐他汀.md "wikilink")(Lovastatin)
  - [美伐他汀](../Page/美伐他汀.md "wikilink")(Mevastatin)
  - [匹伐他汀](../Page/匹伐他汀.md "wikilink")(Pitavastatin)
  - [普伐他汀](../Page/普伐他汀.md "wikilink")(Pravastatin)
  - [瑞舒伐他汀](../Page/瑞舒伐他汀.md "wikilink")(Rosuvastatin)
  - [辛伐他汀](../Page/辛伐他汀.md "wikilink")(Simvastatin)

## 作用

  - 血浆中的[胆固醇来源有外源性和内源性两种途径](../Page/胆固醇.md "wikilink")。外源性胆固醇主要来自[食物](../Page/食物.md "wikilink")，可通过调节[食物结构来控制摄入量](../Page/食物结构.md "wikilink")；内源性的则在[肝脏中合成](../Page/肝脏.md "wikilink")。在[肝细胞的](../Page/肝细胞.md "wikilink")[细胞质中](../Page/细胞质.md "wikilink")，由[乙酸经](../Page/乙酸.md "wikilink")26步[生物合成步骤合成内源性胆固醇](../Page/生物合成.md "wikilink")。
  - 其中[羟甲戊二酰辅酶A还原酶](../Page/羟甲戊二酰辅酶A还原酶.md "wikilink")(3-羟基-3-甲基戊二酰辅酶A(HMG-CoA)还原酶)是该合成过程中的[限速酶](../Page/限速酶.md "wikilink")，能催化HMG-CoA还原为[甲羟戊酸](../Page/甲羟戊酸.md "wikilink")。此步骤是内源性胆固醇合成中关键一步，若抑制HMG-CoA还原酶，则内源性胆固醇合成减少。

[MevalonatePathway_zh.png](https://zh.wikipedia.org/wiki/File:MevalonatePathway_zh.png "fig:MevalonatePathway_zh.png")

[Atorvastatin_binding.png](https://zh.wikipedia.org/wiki/File:Atorvastatin_binding.png "fig:Atorvastatin_binding.png")
entry
\[2\]\]\][HMG-CoA_reductase_pathway.png](https://zh.wikipedia.org/wiki/File:HMG-CoA_reductase_pathway.png "fig:HMG-CoA_reductase_pathway.png")

## 相互作用

  - 胆固醇可反馈抑制肝胆固醇的合成。它主要抑制HMG-CoA还原酶的合成。
  - 胰岛素及甲状腺素能诱导肝HMG-CoA还原酶的合成，从而增加胆固醇的合成。

## 药理学

## 安全性

  - 他汀类药物共同存在[肌毒性](../Page/肌毒性.md "wikilink")。
  - 当他汀与[非诺贝特](../Page/非诺贝特.md "wikilink")（Fenofibrate）、[吉非贝特](../Page/吉非贝特.md "wikilink")（Gemfibrozil）等[贝特类](../Page/贝特类药物.md "wikilink")（Fibrate）调血脂药连用时，致[横纹肌溶解症的危险性会增加](../Page/横纹肌溶解症.md "wikilink")。

## 历史

[Pleurotus_ostreatus_JPG7.jpg](https://zh.wikipedia.org/wiki/File:Pleurotus_ostreatus_JPG7.jpg "fig:Pleurotus_ostreatus_JPG7.jpg")，一种可食用的[蘑菇](../Page/蘑菇.md "wikilink")，含有天然的[洛伐他汀](../Page/洛伐他汀.md "wikilink")\]\]

  - 1971年，日本科學家[遠藤章在研製抗](../Page/遠藤章.md "wikilink")[膽固醇藥物時](../Page/膽固醇.md "wikilink")，發現HMG-CoA還原酶與膽固醇之間的關係，因此推斷[微生物可製成膽固醇抑制劑](../Page/微生物.md "wikilink")。1973年，遠藤團隊從[桔青霉菌](../Page/桔青霉菌.md "wikilink")（*Penicillium
    citrinum*）中分离出了一种抑制剂[美伐他汀](../Page/美伐他汀.md "wikilink")（mevastatin，Compactin，ML-236B）\[3\]。
  - 由[拜尔公司开发的西立伐他汀](../Page/拜尔公司.md "wikilink")，上市仅8个月，销售额达7亿美元，全世界约有600万人使用过西立伐他汀，有40例病人死亡与其严重的[肌损伤不良反应有关](../Page/肌损伤.md "wikilink")，因此拜尔公司不得不于2001年8月在全球范围内停止销售西立伐他汀的所有制剂。

## 参见

  - [甲羟戊酸途径](../Page/甲羟戊酸途径.md "wikilink")

## 外部連結

  - [Statin
    page](http://www.medicine.ox.ac.uk/bandolier/booth/booths/statin.html)
    at *[Bandolier](../Page/:en:Bandolier_\(journal\).md "wikilink")*,
    an [evidence-based medicine](../Page/循证医学.md "wikilink") journal
    (little content after 2004)

[他汀类](../Category/他汀类.md "wikilink")

1.
2.
3.  Liao and Laufs. Pleiotropic Effects of Statins.(2005) Annu. Rev.
    Pharmacol. Toxicol:45:89-118.