**紅頰獴**（**''Herpestes
javanicus**''）又叫**紅臉獴**、**印度獴**或**爪哇獴**，是[灵猫科](../Page/灵猫科.md "wikilink")[獴属](../Page/獴属.md "wikilink")\]的一個[品種](../Page/品種.md "wikilink")，廣泛分佈[亞洲多個不同的地方](../Page/亞洲.md "wikilink")，分為不同[亞種](../Page/亞種.md "wikilink")。獴在世界很多地方均可找到，但紅頰獴的體型比[非洲的亞種細型](../Page/非洲.md "wikilink")。

## 分佈及習性

紅頰獴主要分佈於[亞洲南部](../Page/亞洲.md "wikilink")，由[伊朗到](../Page/伊朗.md "wikilink")[中國](../Page/中國.md "wikilink")[華南地區](../Page/華南.md "wikilink")，再分佈至[印尼](../Page/印尼.md "wikilink")。這個品種的適應能力很強，可以於不同的自然環境生存，包括[濕地](../Page/濕地.md "wikilink")、[草原](../Page/草原.md "wikilink")、[森林等](../Page/森林.md "wikilink")，但較偏向空曠地方。

该物种的模式产地在爪哇。\[1\]

## 獵食

紅頰獴是[機會主義者](../Page/機會主義.md "wikilink")，任何有能力捉到的小動物均成為牠們的食物，例如[昆蟲](../Page/昆蟲.md "wikilink")、[兩棲類](../Page/兩棲類.md "wikilink")、[蝸牛](../Page/蝸牛.md "wikilink")、[蜥蜴](../Page/蜥蜴.md "wikilink")、[雀](../Page/雀.md "wikilink")、[老鼠等](../Page/老鼠.md "wikilink")，甚至[蛇虱可以捕食到](../Page/蛇.md "wikilink")。

## 習性

紅頰獴一般為獨行性，但到了繁殖季節時均會以2至4隻群居。牠們為[晝行性動物](../Page/晝行性.md "wikilink")，喜歡玩耍。

## 外來入侵種

紅頰獴曾被認為是[人類刻意引入](../Page/人類.md "wikilink")[農田以防治](../Page/農田.md "wikilink")[鼠害](../Page/鼠.md "wikilink")，但獴亦有捕捉[雀及其](../Page/雀.md "wikilink")[蛋](../Page/蛋.md "wikilink")，對部份地方的生態環境帶來嚴重威脅。[國際自然保護聯盟](../Page/國際自然保護聯盟.md "wikilink")[物種存續委員會的入侵物種專家小組](../Page/物種存續委員會.md "wikilink")（ISSG）列為[世界百大外來入侵種](../Page/世界百大外來入侵種.md "wikilink")。

## 亚种

红颊獴包括以下亚种\[2\]

  - （[学名](../Page/学名.md "wikilink")： (Hodgson, 1836)）
  - （[学名](../Page/学名.md "wikilink")： (Gervais, 1841)）
  - **红颊獴指名亚种**（[学名](../Page/学名.md "wikilink")： (É. Geoffroy
    Saint-Hilaire, 1818)）
  - （[学名](../Page/学名.md "wikilink")： (Sody, 1936)）
  - （[学名](../Page/学名.md "wikilink")： (Blyth, 1845)）
  - （[学名](../Page/学名.md "wikilink")： (Ghose, 1965)）
  - **红颊獴云南亚种**（[学名](../Page/学名.md "wikilink")：），Schwarz于1910年命名。分布于泰国以及[中国大陆的云南](../Page/中国大陆.md "wikilink")（南部）等地。该物种的模式产地在[曼谷](../Page/曼谷.md "wikilink")。\[3\]
  - （[学名](../Page/学名.md "wikilink")： (Kloss, 1917)）
  - （[学名](../Page/学名.md "wikilink")： (Anderson, 1875)）
  - **红颊獴海南亚种**（[学名](../Page/学名.md "wikilink")：），J.
    Allen于1909年命名。在[中国大陆](../Page/中国大陆.md "wikilink")，分布于[海南](../Page/海南.md "wikilink")、[广西](../Page/广西.md "wikilink")、[广东等地](../Page/广东.md "wikilink")。该物种的模式产地在海南岛[五指山](../Page/五指山.md "wikilink")。\[4\]
  - （[学名](../Page/学名.md "wikilink")： (Kloss, 1917)）
  - （[学名](../Page/学名.md "wikilink")： (Sody, 1949)）

## 參考文獻

[Category:獴属](../Category/獴属.md "wikilink")
[Category:入侵哺乳動物種](../Category/入侵哺乳動物種.md "wikilink")
[Category:廣東動物](../Category/廣東動物.md "wikilink")

1.
2.

3.

4.