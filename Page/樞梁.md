**枢梁**為[日本](../Page/日本.md "wikilink")[女性](../Page/女性.md "wikilink")[漫畫家](../Page/漫畫家.md "wikilink")。

## 人物簡介

出生於日本[埼玉縣](../Page/埼玉縣.md "wikilink")[蕨市](../Page/蕨市.md "wikilink")。現居[神奈川縣](../Page/神奈川縣.md "wikilink")[横浜市](../Page/横浜市.md "wikilink")。血型O型。2004年，以刊載於《[月刊GFantasy](../Page/月刊GFantasy.md "wikilink")》（[SQUARE
ENIX](../Page/SQUARE_ENIX.md "wikilink")）上的《9th》而正式出道。目前主要活躍於《月刊GFantasy》。

過去曾以****（、Yanao
Rock）作為筆名創作[BL](../Page/BL_\(和制英语\).md "wikilink")[同人誌](../Page/同人誌.md "wikilink")，現時已經停止著作BL作品。

## 作品一覽

###

  - [9th](../Page/9th.md "wikilink")（2004年、月刊GFantasy12月號刊載）
  - [聖槍吸血鬼](../Page/聖槍吸血鬼.md "wikilink")（2005年 - 2006年、月刊GFantasy）
  - [黑執事](../Page/黑執事.md "wikilink")（2006年 - 連載中、月刊GFantasy）

###

  - [Glamorous
    Lip](../Page/Glamorous_Lip.md "wikilink")（2006年、[光彩書房](../Page/光彩書房.md "wikilink")）ISBN
    4-86093-224-2

## 外部連結

  - [樞梁官方網站](http://d-6th.com/)

[Category:日本漫画家](../Category/日本漫画家.md "wikilink")
[Category:日本女性漫画家](../Category/日本女性漫画家.md "wikilink")
[Category:埼玉縣出身人物](../Category/埼玉縣出身人物.md "wikilink")