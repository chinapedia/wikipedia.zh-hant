**古尔省**（）位於[阿富汗中部偏西](../Page/阿富汗.md "wikilink")，與[巴德吉斯省](../Page/巴德吉斯省.md "wikilink")、[巴米揚省](../Page/巴米揚省.md "wikilink")、[戴孔迪省](../Page/戴孔迪省.md "wikilink")、[赫拉特省](../Page/赫拉特省.md "wikilink")、[赫爾曼德省](../Page/赫爾曼德省.md "wikilink")、[法拉省](../Page/法拉省.md "wikilink")、[法利亞布省](../Page/法利亞布省.md "wikilink")、[烏魯茲甘省](../Page/烏魯茲甘省.md "wikilink")、[薩爾普勒省等省份相鄰](../Page/薩爾普勒省.md "wikilink")。该省总面积36,479
km²（14,085 sq
mi），2002年总人口485,000\[1\]，主要语言为[波斯语](../Page/波斯语.md "wikilink")。

古尔省辖有11个县。

## 参考文献

<references/>

## 外部链接

  - [Encyclopaedia Britannica (Online Edition) - Ghurid
    Sultanate](http://www.britannica.com/eb/article-9036708/Ghurid-Sultanate)
  - [Encyclopaedia Britannica (Online Edition) - Muizz-ud-Din-Muhammad
    a.k.a. Mohammad of
    Ghor](http://www.britannica.com/eb/article-9054185/Muizz-ud-Din-Muhammad-ibn-Sam)
  - [Columbia Encyclopedia (Sixth Edition) - Muhammad of
    Ghor](http://www.infoplease.com/ce6/people/A0834353.html)
  - [Historical Guide to Afghanistan - Chakhcharan to Herat (Ghaznavids,
    Ghorids, and Mongols)](http://www.zharov.com/dupree/chapter32.html)

<!-- end list -->

1.  [Provinces of Afghanistan](http://www.statoids.com/uaf.html)