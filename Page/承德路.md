**承德路**，是[臺北市的一條南北向的主要幹道之一](../Page/臺北市.md "wikilink")，為台北市通往[士林](../Page/士林區.md "wikilink")[北投和](../Page/北投區.md "wikilink")[淡水的重要道路](../Page/淡水區.md "wikilink")，車流量在交通尖峰時刻極大。曾經與鄰近的[中山北路齊名](../Page/中山北路_\(台北市\).md "wikilink")，有一種說法是：當中山北路堵車時，可以改走承德路；反之，若承德路堵車，則走中山北路或許比較順暢。

## 路名起源

名字源於[熱河省](../Page/熱河省.md "wikilink")[省會](../Page/省會.md "wikilink")[承德市](../Page/承德市.md "wikilink")。

## 沿革

  - 1974年，[交通部臺灣區高速公路工程局](../Page/交通部臺灣區高速公路工程局.md "wikilink")[徵收附近](../Page/徵收.md "wikilink")[重慶國中與明倫國中](../Page/臺北市立重慶國民中學.md "wikilink")（今[明倫高中](../Page/明倫高中.md "wikilink")）的運動場外圍，開始進行敦煌路、承德路等路段的拓寬工程，同時開始興建[承德橋](../Page/承德橋_\(臺北市\).md "wikilink")。
  - 1979年以前，承德橋仍在興建中，當時的承德路僅至明倫國中為止，且不分段，[門牌號碼高達上千號](../Page/門牌號碼.md "wikilink")。隔著[基隆河之對岸則為士林](../Page/基隆河.md "wikilink")[後港墘地區](../Page/後港.md "wikilink")。
  - 1979年7月1日，後港墘地區的道路被整編併入承德路。
  - 1979年10月30日，承德橋通車，承德路可以連結[圓山及後港墘地區](../Page/圓山.md "wikilink")。
  - 1991年8月18日，承德路開始分段，後港墘地區的承德路被編為承德路四段；至於過[中正路之路段](../Page/中正路.md "wikilink")，原為百齡三路、百齡四路、百齡五路，1992年4月1日更名為承德路五至七段，此路段屬省道[台2乙線](../Page/台2乙線.md "wikilink")。

## 行經行政區域

（由北至南）

  - [北投區](../Page/北投區.md "wikilink")：（六段、七段）
  - [士林區](../Page/士林區.md "wikilink")：（四段、五段）
  - [大同區](../Page/大同區_\(臺北市\).md "wikilink")：（一段、二段、三段）

## 分段

承德路共分七段。

  - 七段：南於[文林北路與承德路六段相接](../Page/文林北路.md "wikilink")，東於[大業路與大度路相接](../Page/大業路.md "wikilink")。
  - 六段：南於[洲美街與承德路五段相接](../Page/洲美街.md "wikilink")，北於[文林北路與承德路七段相接](../Page/文林北路.md "wikilink")。
  - 五段：南於[中正路與承德路四段相接](../Page/中正路_\(臺北市\).md "wikilink")，北於[洲美街與承德路六段相接](../Page/洲美街.md "wikilink")。

[承德路四段.JPG](https://zh.wikipedia.org/wiki/File:承德路四段.JPG "fig:承德路四段.JPG")

  - 四段：南於[承德橋與承德路三段相接](../Page/承德橋_\(臺北市\).md "wikilink")，北於[中正路與承德路五段相接](../Page/中正路.md "wikilink")。
  - 三段：南於[民權西路與承德路二段相接](../Page/民權西路_\(臺北市\).md "wikilink")，北於[承德橋與承德路四段相接](../Page/承德橋.md "wikilink")。
  - 二段：南於[南京西路與承德路一段相接](../Page/南京西路_\(臺北市\).md "wikilink")，北於[民權西路與承德路三段相接](../Page/民權西路_\(臺北市\).md "wikilink")。

[Sec._1_of_5th_Ave.in_Taipei.JPG](https://zh.wikipedia.org/wiki/File:Sec._1_of_5th_Ave.in_Taipei.JPG "fig:Sec._1_of_5th_Ave.in_Taipei.JPG")後）\]\]

  - 一段：南於[市民大道相接](../Page/市民大道.md "wikilink")，北於[南京西路與承德路二段相接](../Page/南京西路_\(臺北市\).md "wikilink")。

## 沿線設施（由南至北）

<table style="width:10%;">
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
</colgroup>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><ul>
<li>一段
<ul>
<li><a href="../Page/市民大道.md" title="wikilink">市民大道口</a>
<ul>
<li><a href="../Page/臺北車站.md" title="wikilink">臺北車站</a>（<a href="../Page/市民大道.md" title="wikilink">市民大道口</a>）</li>
<li><a href="../Page/臺北轉運站.md" title="wikilink">臺北轉運站</a>（市民大道口）（市民大道一段209號）</li>
<li><a href="../Page/台北市公共自行車租賃系統.md" title="wikilink">台北市公共自行車租賃系統</a><a href="../Page/臺北轉運站.md" title="wikilink">臺北轉運站</a>（市民大道一段209號）</li>
</ul></li>
<li><a href="../Page/京站大樓.md" title="wikilink">京站大樓</a>
<ul>
<li><a href="../Page/京站時尚廣場.md" title="wikilink">京站時尚廣場</a>（1號）</li>
<li><a href="../Page/君品酒店.md" title="wikilink">君品酒店</a>（3號）</li>
</ul></li>
<li><a href="../Page/蔡合源宅第.md" title="wikilink">蔡合源宅第</a>（72號）</li>
<li><a href="../Page/臺北市政府警察局.md" title="wikilink">臺北市政府警察局大同分局建成派出所</a>（80號）</li>
<li>貴族大廈（105號）
<ul>
<li><a href="../Page/聯邦商業銀行.md" title="wikilink">聯邦商業銀行台北分行</a>（105號1樓）</li>
</ul></li>
</ul></li>
</ul>
<ul>
<li>二段
<ul>
<li><a href="../Page/建成公園.md" title="wikilink">建成公園</a>（平陽街口）</li>
<li><a href="../Page/建成公園.md" title="wikilink">建成公園地下停車場</a></li>
<li>建成大樓（33號，2017年下半年拆除）</li>
<li><a href="../Page/臺北市政府消防局.md" title="wikilink">臺北市政府消防局建成分隊</a>（35號）</li>
<li><a href="../Page/臺北市大同區日新國民小學.md" title="wikilink">臺北市大同區日新國民小學</a>（正門位於太原路151號）</li>
<li>首府經貿大廈
<ul>
<li><a href="../Page/上海商業儲蓄銀行.md" title="wikilink">上海商業儲蓄銀行承德分行</a>（77號1樓）</li>
<li><a href="../Page/中國生產力中心.md" title="wikilink">中國生產力中心臺北承德教育訓練中心</a>（81號B1樓）</li>
<li><a href="../Page/東立出版社.md" title="wikilink">東立出版社</a>（81號10樓）</li>
<li><a href="../Page/台灣東方出版社.md" title="wikilink">台灣東方出版社</a>（81號12樓之2）</li>
</ul></li>
<li><a href="../Page/臺北市立成淵高級中學.md" title="wikilink">臺北市立成淵高級中學</a>（235號）</li>
<li><a href="../Page/臺北市立成淵高級中學.md" title="wikilink">臺北市立成淵高級中學地下停車場</a></li>
<li><a href="../Page/臺灣銀行.md" title="wikilink">臺灣銀行承德大樓</a>（206、208號）</li>
<li>臺灣銀行民權大樓（239號）</li>
</ul></li>
</ul>
<ul>
<li>三段
<ul>
<li><a href="../Page/台北市公共自行車租賃系統.md" title="wikilink">台北市公共自行車租賃系統</a><a href="../Page/捷運圓山站.md" title="wikilink">捷運圓山站</a>(2號出口)</li>
<li><a href="../Page/三德大飯店.md" title="wikilink">三德大飯店</a>（49號）</li>
<li><a href="../Page/金車大塚.md" title="wikilink">金車大塚</a>（97號）</li>
<li>臺北市大同區明倫國民小學（285號，2013年8月廢校，現正改建<a href="../Page/臺北市大同區明倫公共住宅.md" title="wikilink">臺北市大同區明倫公共住宅</a>）</li>
<li><a href="../Page/臺北數位產業園區.md" title="wikilink">臺北數位產業園區</a>（287號）</li>
<li><a href="../Page/臺北市立明倫高級中學.md" title="wikilink">臺北市立明倫高級中學</a>（336號）</li>
</ul></li>
</ul></td>
<td><ul>
<li>四段
<ul>
<li><a href="../Page/臺北市立百齡高級中學.md" title="wikilink">臺北市立百齡高級中學</a>（177號）</li>
<li><a href="../Page/臺北市立百齡高級中學.md" title="wikilink">臺北市立百齡高級中學地下停車場</a></li>
<li><a href="../Page/臺北表演藝術中心.md" title="wikilink">臺北表演藝術中心</a>[興建中]</li>
</ul></li>
<li><a href="../Page/士商路.md" title="wikilink">士商路口</a>
<ul>
<li><a href="../Page/士林運動中心.md" title="wikilink">士林運動中心</a>（<a href="../Page/士商路.md" title="wikilink">士商路</a>1號）</li>
<li><a href="../Page/台北市公共自行車租賃系統.md" title="wikilink">台北市公共自行車租賃系統</a><a href="../Page/士林運動中心.md" title="wikilink">士林運動中心站</a></li>
<li>承德公園地下停車場</li>
<li>承德公園</li>
</ul></li>
</ul>
<ul>
<li>五段<a href="https://zh.wikipedia.org/wiki/File:TW_PHW2b.svg" title="fig:TW_PHW2b.svg">TW_PHW2b.svg</a>
<ul>
<li><a href="../Page/臺北市立士林高級商業職業學校.md" title="wikilink">臺北市立士林高級商業職業學校</a>（正門位於<a href="../Page/士商路.md" title="wikilink">士商路</a>150號）</li>
<li><a href="../Page/交通部公路總局臺北市區監理所.md" title="wikilink">交通部公路總局臺北市區監理所士林監理站</a>（80號）</li>
<li><a href="../Page/臺北市立兒童新樂園.md" title="wikilink">臺北市立兒童新樂園</a>(55號)</li>
</ul></li>
</ul>
<ul>
<li>六段<a href="https://zh.wikipedia.org/wiki/File:TW_PHW2b.svg" title="fig:TW_PHW2b.svg">TW_PHW2b.svg</a>
<ul>
<li>大台北花市聯合廣場（449-14號）</li>
</ul></li>
</ul>
<ul>
<li>七段<a href="https://zh.wikipedia.org/wiki/File:TW_PHW2b.svg" title="fig:TW_PHW2b.svg">TW_PHW2b.svg</a>
<ul>
<li>漢諾威馬術俱樂部（143號）</li>
<li><a href="../Page/臺北市中華基督教青年會.md" title="wikilink">臺北市中華基督教青年會唭哩岸會館</a>（310號）</li>
<li>立農公園地下停車場</li>
<li>立農公園</li>
<li><a href="../Page/富邦人壽.md" title="wikilink">富邦承德大樓</a>（400號，原<a href="../Page/大同公司.md" title="wikilink">大同公司北投廠</a>）</li>
</ul></li>
</ul></td>
</tr>
</tbody>
</table>

## 道路設計

### 車道數

  - 一段\~四段：雙向各四車道，寬度為40米。
  - 五段：雙向各六車道，北往南其中一線為機車道，60米寬。
  - 六段：雙向各六車道，北往南其中一線為機車道，60米寬。
  - 七段（[文林北路](../Page/文林北路.md "wikilink")-[石牌路](../Page/石牌路.md "wikilink")）：雙向各六車道，60米寬，其中一線為機車道。
  - 七段（[石牌路](../Page/石牌路.md "wikilink")-[大度路](../Page/大度路.md "wikilink")）：雙向各五車道，60米寬。

### 號碼

  - 一段：單號1\~105，雙號2\~88。
  - 二段：單號1\~239，雙號2\~208。
  - 三段：單號1\~287，雙號2\~336。
  - 四段：單號1\~333，雙號2\~364。
  - 五段：單號55 ，雙號2\~80。
  - 六段：單號1\~459，雙號2\~452。
  - 七段：單號1\~401，雙號2\~400。

## 參見

  - 參見[臺北市主要道路列表](../Page/臺北市主要道路列表.md "wikilink")

## 外部連結

[C承](../Category/臺北市街道.md "wikilink")