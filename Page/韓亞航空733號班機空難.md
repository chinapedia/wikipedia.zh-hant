**韓亞航空733號班機**是[韓亞航空一班由韓國](../Page/韓亞航空.md "wikilink")[首爾飛往](../Page/首爾.md "wikilink")[全羅南道的](../Page/全羅南道.md "wikilink")[木浦的航班](../Page/木浦.md "wikilink")。1993年7月26日，一架註冊編號HL-7229的[波音737-5L9型客機執行此航班時在木浦市因天氣惡劣墜毀](../Page/波音737#737-500.md "wikilink")，造成機上116人中的68人死亡。

## 經過

韓亞航空的733號班機於當地時間下午2時37分於[首爾](../Page/首爾.md "wikilink")[金浦國際機場起飛](../Page/金浦國際機場.md "wikilink")，前往西南方的[木浦市](../Page/木浦市.md "wikilink")（Mokpo）。當時的報告指能見度高，可是在空難發生前，天氣卻瞬間轉壞，並刮起強風和下大雨。該航機曾兩次嘗試著陸都未能成功，到了第三次，機長嘗試強行降落[木浦機場的](../Page/木浦機場.md "wikilink")06跑道前，卻意外撞向離跑道4公里外、高1,050尺高的Mount
Ungeo的800尺山脊處。飛機即時爆炸起火，機上66名乘客及2名機組人員死亡。

## 事後

這是波音737-500第一次發生墜機事故，亦是歷來涉及737-500型客機的第二最嚴重事故，僅次於2008年9月14日發生的[北俄羅斯航空821號班機空難](../Page/北俄羅斯航空821號班機空難.md "wikilink")。在當時，這亦是韓國本土史上第二傷亡最慘重的空難（韓國境內傷亡最慘重的空難是2002年4月15日的[中國國際航空129號班機空難](../Page/中國國際航空129號班機空難.md "wikilink")），也是韓亞航空首宗致命空難。

雖然韓亞航空在事發後將來往首爾及木浦的航線取消\[1\]，但OZ733這個航班編號現時仍在使用中，由[首爾](../Page/首爾.md "wikilink")[仁川國際機場飛往](../Page/仁川國際機場.md "wikilink")[河內](../Page/河內.md "wikilink")，使用[空中巴士A330-300或](../Page/空中客车A330.md "wikilink")[波音777-200ER型客機飛行](../Page/波音777.md "wikilink")。

## 參考

  - [Aviation Safety
    Network](http://aviation-safety.net/database/record.php?id=19930726-1&lang=en)

[Category:1993年航空事故](../Category/1993年航空事故.md "wikilink")
[Category:1993年韩国](../Category/1993年韩国.md "wikilink")
[Category:韓國航空事故](../Category/韓國航空事故.md "wikilink")
[Category:可控飛行撞地](../Category/可控飛行撞地.md "wikilink")
[Category:全罗南道历史](../Category/全罗南道历史.md "wikilink")
[Category:韩亚航空公司航空事故](../Category/韩亚航空公司航空事故.md "wikilink")
[Category:天氣因素觸發的航空事故](../Category/天氣因素觸發的航空事故.md "wikilink")
[Category:波音737航空事故](../Category/波音737航空事故.md "wikilink")
[Category:1993年7月](../Category/1993年7月.md "wikilink")

1.  [박삼구 아시아나항공 사장, 서울-목포 운항 무기한
    중단](http://imnews.imbc.com/20dbnews/history/1993/1758030_3833.html)
    - MBC 뉴스데스크 1993年7月29日