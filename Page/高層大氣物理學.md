**高層大氣物理學**（）是[大氣物理學的一個分支](../Page/大氣物理學.md "wikilink")，主要研究中層與高層（[平流層](../Page/平流層.md "wikilink")、[中間層](../Page/中間層.md "wikilink")、[熱成層和](../Page/熱成層.md "wikilink")[磁層等](../Page/磁層.md "wikilink")）[大氣的成分和](../Page/大氣.md "wikilink")[物理性質](../Page/物理性質.md "wikilink")，分子[离解和](../Page/离解.md "wikilink")[電離是該學科重要的部分](../Page/電離.md "wikilink")\[1\]。另外還研究[臭氧層的分佈與變化](../Page/臭氧層.md "wikilink")。高層大氣物理學主要發展於20世紀和21世紀。

「Aeronomy」這個詞是於1946年在《[自然](../Page/自然_\(期刊\).md "wikilink")》期刊中的發表的文章《Some
Thoughts on Nomenclature》中首次提及\[2\]。

今日這項學科研究範圍也包含了其他行星大氣層的對應區域。高層大氣物理學的相關研究必須透過氣球、衛星和[探空火箭以獲得高層大氣的有用資料](../Page/探空火箭.md "wikilink")。其他屬於高層大氣物理學的大氣現象則是[中高层大气闪电](../Page/中高层大气闪电.md "wikilink")，例如[紅電光閃靈](../Page/紅電光閃靈.md "wikilink")、精靈光暈、藍色噴流和精靈\[3\]。

## 大氣潮汐

大氣潮汐是將能量從高層大氣送往低層的重要機制，也主導了中間層和較低熱層的動力狀態；因此，了解大氣潮汐對於了解整個大氣層狀態是必須的。對於地球大氣層的監測和預測變化必須要進行大氣潮汐的模型建立和觀測。

## 中高层大气闪电

[Upperatmoslight1.jpg](https://zh.wikipedia.org/wiki/File:Upperatmoslight1.jpg "fig:Upperatmoslight1.jpg")

中高层大气闪电或中高層大氣放電是指一系列發生在高度高於尋常[閃電以上區域的放電現象](../Page/閃電.md "wikilink")。這個用語現在主要是指在較高層大氣層中發生的多種被稱為「瞬态发光现象」（Transient
Luminous
Events，TLEs）的放電現象。瞬態發光現象包含[紅電光閃靈](../Page/紅電光閃靈.md "wikilink")、精靈光暈（Sprite
halos）、藍色噴流（Blue jets）和精靈（Elves）。

## 參考資料

## 參見

  - [大氣物理學](../Page/大氣物理學.md "wikilink")

## 外部連結

  - [比利時太空與高層大氣物理學研究所](http://www.aeronomie.be/en/index.htm)
  - [Aeronomy, Department of Atmospheric, Oceanic and Space Science,
    University of
    Michigan](http://aoss.engin.umich.edu/pages/space/aeronomy)

[Category:大气科学](../Category/大气科学.md "wikilink")
[Category:电现象](../Category/电现象.md "wikilink")
[Category:太空電漿](../Category/太空電漿.md "wikilink")

1.
2.
3.  [Palmer Station measurements of radio atmospherics associated with
    upward electrodynamic coupling phenomena between thunderstorms and
    the mesosphere/lower
    ionosphere](http://www.nsf.gov/geo/plr/antarct/ajus/nsf9828/9828pdf/nsf9828l.pdf)