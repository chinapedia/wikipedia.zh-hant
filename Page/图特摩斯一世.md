**图特摩斯一世** （英语文献中一般写作：*Thutmose
I*，?—约前1493年）[古埃及](../Page/古埃及.md "wikilink")[第十八王朝](../Page/埃及第十八王朝.md "wikilink")[法老](../Page/法老.md "wikilink")（约前1506年—约前1493年在位）。

图特摩斯一世是法老[阿蒙霍特普一世的主要军事统帅](../Page/阿蒙霍特普一世.md "wikilink")，也是他的妹夫。在阿蒙霍特普一世无嗣而终后，图特摩斯一世取得了王位。他在位时期征服了[尼罗河第三瀑布以北的](../Page/尼罗河.md "wikilink")[努比亚](../Page/努比亚.md "wikilink")，并向[亚洲的](../Page/亚洲.md "wikilink")[叙利亚及](../Page/叙利亚.md "wikilink")[巴勒斯坦地区发展了埃及的势力](../Page/巴勒斯坦.md "wikilink")。一般认为，图特摩斯一世是埃及历史上最辉煌的第十八王朝的真正奠基者。

图特摩斯一世是第一个葬在[帝王谷的法老](../Page/帝王谷.md "wikilink")。

## 名字

像其他所有第十八王朝的法老一样，图特摩斯一世有两个名字。其中一个是[王衔](../Page/王衔.md "wikilink")（praenomen，或者叫假名），在第十八王朝的法老中一般是拉名；另一个是真名，即出生时的名字。图特摩斯一世的真名是图特摩斯（Thutmose），意思是“[托特](../Page/托特.md "wikilink")（神）的儿子”。王衔是阿克珀卡拉（Akheperkare），
这是一个与埃及太阳神[拉有关的名字](../Page/拉.md "wikilink")。

## 家庭

[缩略图](https://zh.wikipedia.org/wiki/File:Thutmose_I_Family-83d40m-highContrast.jpg "fig:缩略图")
图特摩斯一世的母亲是[仙塞妮普](../Page/仙塞妮普.md "wikilink")，其母亲家世不详，或为[阿蒙霍特普一世的侧室或姐妹](../Page/阿蒙霍特普一世.md "wikilink")，但仍有争议，其父亲身份有待考察，或许是阿蒙霍特普一世，仍有争议。
图特摩斯一世拥有两位皇后:

  - [雅赫摩斯王后](../Page/雅赫摩斯王后.md "wikilink")，家世不明，有待考察，为女皇[哈特谢普苏特和](../Page/哈特谢普苏特.md "wikilink")[妮斐鲁碧提的母亲](../Page/妮斐鲁碧提.md "wikilink")，也有可能是[阿蒙摩斯和](../Page/阿蒙摩斯.md "wikilink")[娃德杰摩斯的母亲](../Page/娃德杰摩斯.md "wikilink")。
  - [姆特诺费列特](../Page/姆特诺费列特.md "wikilink")，为[雅赫摩斯一世与](../Page/雅赫摩斯一世.md "wikilink")[雅赫摩斯-纳菲尔泰丽之女](../Page/雅赫摩斯-纳菲尔泰丽.md "wikilink")，阿蒙霍特普一世的姐妹，[图特摩斯二世的母亲](../Page/图特摩斯二世.md "wikilink")，或为[阿蒙摩斯与](../Page/阿蒙摩斯.md "wikilink")[娃德杰摩斯的母亲](../Page/娃德杰摩斯.md "wikilink")。

图特摩斯一世也有几个孩子:

  - [阿蒙摩斯](../Page/阿蒙摩斯.md "wikilink")，王子，母或许是[雅赫摩斯王后或者](../Page/雅赫摩斯王后.md "wikilink")[姆特诺费列特](../Page/姆特诺费列特.md "wikilink")，与兄弟[娃德杰摩斯于家庭教师](../Page/娃德杰摩斯.md "wikilink")[巴赫里的墓室壁画中出现](../Page/巴赫里\(埃及人\).md "wikilink")。
  - [娃德杰摩斯](../Page/娃德杰摩斯.md "wikilink")，王子，母或许是[雅赫摩斯王后或者](../Page/雅赫摩斯王后.md "wikilink")[姆特诺费列特](../Page/姆特诺费列特.md "wikilink")，与兄弟[阿蒙摩斯于家庭教师](../Page/阿蒙摩斯.md "wikilink")[巴赫里的墓室壁画中出现](../Page/巴赫里\(埃及人\).md "wikilink")。
  - [图特摩斯二世](../Page/图特摩斯二世.md "wikilink")，王子，后为法老，母[姆特诺费列特](../Page/姆特诺费列特.md "wikilink")
  - [哈特谢普苏特](../Page/哈特谢普苏特.md "wikilink")，公主，后为女皇，母[雅赫摩斯王后](../Page/雅赫摩斯王后.md "wikilink")
  - [妮斐鲁碧提](../Page/妮斐鲁碧提.md "wikilink")，公主，母[雅赫摩斯王后](../Page/雅赫摩斯王后.md "wikilink")，于父母亲出现于一副壁画中。后失去记载，可能是早夭。

## 死亡和埋葬

图特摩斯一世的棺材后来被21王朝的一位法老重用。他的木乃伊一度被认为丢失，可是埃及学家在很大程度上取决于与图特摩斯二世和三世的家族相似性而相信无标记木乃伊\#5283是图特摩斯一世。\[1\]该鉴定已被后续检查所支持，这检查揭示了符合时间的所使用的防腐技术，并几乎肯定这木乃伊在十八王朝雅赫摩斯一世之后做成。\[2\]

一直被认为是图特摩斯的木乃伊能够在[开罗的](../Page/开罗.md "wikilink")[埃及博物馆中看到](../Page/埃及博物馆.md "wikilink")。然而，[札希·哈瓦斯于](../Page/札希·哈瓦斯.md "wikilink")2007年宣布之前被认为是图特摩斯一世的木乃伊是一个死于胸口箭伤的30岁男子。根据其死亡的岁数和死因，这具木乃伊可能不是图特摩斯一世本人。\[3\]

## 注释

<references/>

[Category:第十八王朝法老](../Category/第十八王朝法老.md "wikilink")

1.  Maspero, Gaston. *History Of Egypt, Chaldaea, Syria, Babylonia, and
    Assyria, Volume 4 (of 12),* Project Gutenberg EBook, Release Date:
    December 16, 2005. EBook \#17324.
    <https://www.gutenberg.org/dirs/1/7/3/2/17324/17324-h/v4c.htm#image-0047>
2.  Smith (2000) p.25-28
3.