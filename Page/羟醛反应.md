[aldol-46-CHSP.png](https://zh.wikipedia.org/wiki/File:aldol-46-CHSP.png "fig:aldol-46-CHSP.png")

**羟醛反应**（）是[有机化学及](../Page/有机化学.md "wikilink")[生物化学中构建](../Page/生物化学.md "wikilink")[碳－碳键最重要的反应手段之一](../Page/碳－碳键.md "wikilink")。该反应由[查尔斯·阿道夫·武兹](../Page/查尔斯·阿道夫·武兹.md "wikilink")\[1\]\[2\]\[3\]和[亞歷山大·波菲里耶維奇·鮑羅丁于](../Page/亞歷山大·波菲里耶維奇·鮑羅丁.md "wikilink")1872年分别独立发现\[4\]，它是指具有α氢原子的[醛或](../Page/醛.md "wikilink")[酮在一定条件下形成](../Page/酮.md "wikilink")[烯醇负离子](../Page/烯醇负离子.md "wikilink")，再与另一分子[羰基化合物发生](../Page/羰基.md "wikilink")[加成反应](../Page/加成反应.md "wikilink")，并形成[β-羟基羰基化合物的一类有机化学反应](../Page/β-羟基羰基化合物.md "wikilink")。\[5\]\[6\]\[7\]
反应连接了两个羰基底物（最初反应使用醛）合成的β-产物，其命名取用了醇羟基的“羟”（ol）字和醛类化合物的“醛”（ald）字，也称作“羟醛”（aldol）化合物。

  -

一个典型的现代羟醛[加成反应如上图所示](../Page/加成反应.md "wikilink")，即酮的烯醇负离子对醛的[亲核加成](../Page/亲核加成反应.md "wikilink")。反应发生后，羟醛产物在一定条件下可以[失一分子水形成](../Page/脱水.md "wikilink")[α,β-不饱和羰基化合物](../Page/α,β-不饱和羰基化合物.md "wikilink")，这时的羟醛反应及脱水过程可称作「羟醛缩合反应」。在羟醛反应中可以参与反应的亲核试剂有：[烯醇](../Page/烯醇.md "wikilink")、烯醇负离子、酮的[烯醇醚](../Page/烯醇醚.md "wikilink")、醛和其他[羰基化合物](../Page/羰基.md "wikilink")。与之反应的[亲电试剂通常是醛或酮](../Page/亲电体.md "wikilink")（包括许多反应变种，如[曼尼希反应](../Page/曼尼希反应.md "wikilink")）。若亲核试剂和亲电试剂不同，反应称作「交叉羟醛反应」；若亲核试剂和亲电试剂相同则称作「羟醛二聚化反应」。

[aldolrxnpic.jpg](https://zh.wikipedia.org/wiki/File:aldolrxnpic.jpg "fig:aldolrxnpic.jpg")（LDA）[四氢呋喃](../Page/四氢呋喃.md "wikilink")（THF）溶液。左边反应瓶内是叔丁基丙酸酯的烯醇锂负离子（由二异丙基氨基锂和叔丁基丙酸酯形成）。醛可以被加入到含烯醇离子的反应瓶中引发羟醛加成反应。
两个反应瓶都浸没于一个干冰/丙酮[冷浴](../Page/冷浴.md "wikilink")（−78℃）中，热电偶（左边的电线）用于监测反应温度。\]\]

## 发现与发展历程

羟醛反应首先由法国化学家[查尔斯-阿道夫·武尔茨](../Page/查尔斯-阿道夫·武尔茨.md "wikilink")\[8\]和俄国化学家[亞歷山大·波菲里耶維奇·鮑羅丁](../Page/亞歷山大·波菲里耶維奇·鮑羅丁.md "wikilink")\[9\]\[10\]于1872年分别独立发现。最初的反应为乙醛在氢氧化钠条件下进行加成反应，产物为含有羟基的醛类化合物，羟醛即由此得名。由于羟醛反应的产物控制方法学还未出现，交叉羟醛反应总会产生大量无任何合成价值的副产物，除了利用简单醛酮分子的羟醛缩合反应可用于合成[共轭不饱和醛酮之外](../Page/共轭体系.md "wikilink")，该反应在发现后的近一个世纪内一直没有良好的应用\[11\]。

羟醛反应第一个具里程碑意义的事件出现于1957年，当时美国西北大学的H.E.齐默曼（Zimmerman）和M.D.特拉克斯勒（Traxler）为解释[格氏试剂介导的](../Page/格氏試劑.md "wikilink")[Ivanov反应中反式产物占优势的问题](../Page/Ivanov反应.md "wikilink")，提出了著名的六元环[过渡态模型](../Page/过渡态.md "wikilink")，后人常称之齐默曼-特拉克斯勒模型。\[12\]
该模型首次从[立体化学角度对羟醛反应进行剖析](../Page/立体化学.md "wikilink")，指出烯醇盐构型与产物立体化学之间的对应关系，成为羟醛反应历史上的第一个理论突破并在很长一段时间内成为后续研究的关键性指导性理论。以后众多的实验结果也证明这个模型的正确性，依据模型设计的实验大多得到了模型所预期的立体化学结果\[13\]。

二十世纪六十年代，[核磁共振和X单晶衍射技术大力推动了立体化学的发展](../Page/核磁共振.md "wikilink")\[14\]，也为羟醛反应研究带来了极大便利。通过[氢谱中获得的大量积分](../Page/核磁共振氢谱.md "wikilink")、[化学位移](../Page/化学位移.md "wikilink")、[偶合常数等数据](../Page/偶合常数.md "wikilink")，可方便快捷地分析鉴定产物的立体化学结构；X单晶衍射可以通过晶体衍射来分析化合物的绝对构型，而在此背景下羟醛反应的研究也开始逐步升温\[15\]\[16\]\[17\]
。

1973年[日本](../Page/日本.md "wikilink")[北里大学教授](../Page/北里大学.md "wikilink")发现了使用[硅醚形态稳定烯醇以进行羟醛反应的新方法](../Page/硅醚.md "wikilink")\[18\]\[19\]，该发现为羟醛反应研究揭开了新的一页。[向山羟醛反应通过预先制备](../Page/向山羟醛反应.md "wikilink")[烯醇硅醚与醛酮混合后在路易斯酸催化下反应得到羟醛产物](../Page/烯醇硅醚.md "wikilink")。由于产物的立体化学与烯醇硅醚的构型之对应关系不再符合齐默曼-特拉克斯勒模型，因此向山开链过渡态理论被提出并用以解释相关反应结果。通过[路易斯酸在向山羟醛反应中的研究](../Page/路易斯酸.md "wikilink")，羟醛反应立体化学控制技术得以发展，并成为该羟醛反应立体化学三大控制方向之一\[20\]\[21\]。

二十世纪六十年代到八十年代是[有机化学发展史上的重要时期](../Page/有机化学.md "wikilink")，出现了众多不对称合成技术和著名的有机化学家，如：[野依良治](../Page/野依良治.md "wikilink")，[巴里·夏普莱斯和](../Page/巴里·夏普莱斯.md "wikilink")[威廉·斯坦迪什·诺尔斯](../Page/威廉·斯坦迪什·诺尔斯.md "wikilink")。羟醛反应立体控制技术同样在这段期间发生了重大的突破\[22\]。1981年哈佛大学教授\[23\]\[24\]发明了手性配体介导的不对称羟醛反应技术，第一次实现了高对映选择性的不对称羟醛反应。此外日本的向山光昭、克里明斯等都在这段时间对羟醛反应立体控制技术做出了重大的贡献。

近年来，化学家对于羟醛反应的研究集中在[催化剂的发现领域](../Page/催化剂.md "wikilink")\[25\]。许多更新颖的催化剂被发现，包括：[酶类与](../Page/酶.md "wikilink")[抗体型催化剂](../Page/抗体.md "wikilink")\[26\]
\[27\] \[28\] \[29\] \[30\] \[31\]
\[32\]，手性金属络合物催化剂\[33\]，以及众多的小分子催化剂\[34\]\[35\]\[36\]。

## 有机合成中的地位和意义

羟醛结构单元存在于许多分子（包括天然产物和合成分子）中\[37\]\[38\]\[39\]。
例如，通过羟醛反应大规模合成的日用化学品[季戊四醇](../Page/季戊四醇.md "wikilink")\[40\]及心脏病药物[阿托伐他汀](../Page/阿托伐他汀.md "wikilink")\[41\]\[42\]\[43\]。羟醛反应之所以应用广泛，是因为它将两个相对简单的分子结合成一个较复杂的分子，且通过形成两个新的[手性中心](../Page/手性.md "wikilink")（于羟醛产物的α-碳原子上，在下述分子式当中标注）增加了分子复杂性。现代化学方法学不仅可以做到高收率的羟醛反应，而且能够控制反应产物的相对和绝对立体化学构型（見下文的立體選擇性一節）。选择性合成特定的手性异构体对于现代[药物化学非常重要](../Page/药物化学.md "wikilink")，因为不同的手性异构体可能具有完全不同的临床[药理学特性](../Page/药理学.md "wikilink")\[44\]
。

手性羟醛单元在[聚酮化合物中较常见](../Page/聚酮.md "wikilink")，聚酮是一种在生物有机体中发现的分子。在大自然中，聚酮通过酶进行多重[克莱森缩合反应](../Page/克莱森缩合反应.md "wikilink")。这些反应产生的1,3-二羰基化合物可衍生出各式各样有趣的结构，其中一些具有强效的生物活性，如强效免疫抑制剂\[45\]
\[46\]、抗癌药物\[47\]
\[48\]及抗真菌药物[两性霉素B](../Page/两性霉素B.md "wikilink")\[49\]\[50\]。不对称羟醛反应在合成方法学中的应用，使得许多传统的天然产物分子合成成为可能，且在许多合成中成为关键步骤，如：littoralisone\[51\]，福司曲星\[52\]\[53\]，及callipeltoside的合成\[54\]\[55\]。

## 反应机理

羟醛反应的具体机理还无法确定，但现今被广泛认可的有两种机理\[56\] \[57\] \[58\] \[59\]：
羰基化合物如醛或酮可转化为烯醇或烯醇醚。它们在[α-碳原子上都具有亲核性](../Page/α-碳原子.md "wikilink")，可以进攻一些活泼的质子化羰基化合物，如质子化的醛，称为“烯醇机理”\[60\]\[61\]。

羰基化合物或碳原子上含活泼氢的有机物，可于羰基α位去质子化形成烯醇负离子，而该离子形态比烯醇和烯醇醚更具亲核性，可直接进攻亲电试剂。常见的亲电试剂为醛类化合物，而酮的活性相对较低，这类反应机理称为“烯醇负离子机理”\[62\]
\[63\] \[64\]。

若反应条件特别剧烈，如：甲醇钠作碱，甲醇为溶剂的[回流条件下](../Page/回流.md "wikilink")，则会发生缩合反应；为了避免这种情况可以降低温度并使用温和碱如[二异丙基氨基锂](../Page/二异丙基氨基锂.md "wikilink")，并使用[四氢呋喃为溶剂](../Page/四氢呋喃.md "wikilink")，在−78 °C下反应。虽然羟醛加成反应通常能进行完全，却并非不可逆。用强碱来处理羟醛加成产物，可导致逆向-羟醛裂解而得到起始原料。羟醛缩合通常认为是不可逆反应，但交叉羟醛反应动力学研究表明其实际上是可逆反应\[65\]。

  -

### 烯醇机理

在酸催化条件下，[反应机理的起始步骤是羰基在酸](../Page/反应机理.md "wikilink")-催化下[异构化为烯醇](../Page/异构化.md "wikilink")，并通过质子化活化另一分子羰基，使其具有高度的[亲电性](../Page/亲电体.md "wikilink")。烯醇在α-碳原子上具[亲核性](../Page/亲核体.md "wikilink")，能进攻质子化的羰基化合物，而后[去质子化形成羟醛](../Page/去质子化.md "wikilink")。羟醛产物最后通常还会继续[脱水得到不饱和羰基化合物](../Page/脱水.md "wikilink")\[66\]。以下展示了典型的酸-催化下的醛自身缩合反应\[67\]：

#### 酸催化的羟醛反应机理

  -

#### 酸催化的脱水反应

  -

### 烯醇负离子机理

若[催化剂是温和的碱](../Page/催化剂.md "wikilink")，如[氢氧根离子或](../Page/氢氧根.md "wikilink")[醇负离子](../Page/醇盐.md "wikilink")，则羟醛反应可通过形成[共振-稳定的烯醇负离子而亲核进攻另一分子羰基化合物](../Page/共振_\(化学\).md "wikilink")，形成羟醛产物[醇盐](../Page/醇盐.md "wikilink")，而后自身脱水得到不饱和羰基化合物\[68\]。反应式展示了碱-催化下的醛自身进行羟醛反应的机理\[69\]：

#### 碱催化的羟醛反应

图例使用[<sup>−</sup>OCH<sub>3</sub>做碱](../Page/甲醇钠.md "wikilink")

  -

#### 碱催化的脱水反应

此反应通常被错写为简单一步，见[E1cB消除反应](../Page/E1cB反应.md "wikilink")

  -

### 齐默曼-特拉克斯勒反应过渡态模型

有机反应过渡态是控制反应的关键要素之一。羟醛反应的第一个[过渡态模型是](../Page/过渡态.md "wikilink")1957年由霍华德·齐默曼和马乔里·特拉克斯勒提出的六元环过渡态模型\[70\]，即如下图所示齐默曼-特拉克斯勒模型，M是由碱带入的金属离子。运用建立在[环己烷上的](../Page/环己烷.md "wikilink")[构象分析理论可以看出](../Page/构象分析.md "wikilink")，醛分子在受烯醇进攻时的构象形态主要是使羰基两侧较大的取代基处于六元环平伏键的位置，因为可以使各官能团之间空间位阻最小，过渡态的能量也最小，而导致优先形成相应立体构型的产物。\[71\]

  -

## 交叉-羟醛反应的反应物控制

羟醛反应的一个主要研究方向是如何“控制”反应产物，这可以通过一个实例来说明，如以下假定反应中，两个不对称酮在[乙醇钠条件下进行缩合反应](../Page/乙醇钠.md "wikilink")。乙醇钠的碱性不能将其中任意一个酮的邻位氢完全去质子化，但可以将两种酮都形成少量的烯醇钠盐。这意味着两种酮分子不仅都是潜在的亲电试剂，而且都可以通过形成烯醇负离子成为亲核试剂。最后反应的结果是两种亲电试剂和两种亲核试剂形成的四种产物\[72\]。

  -

### 酸性

  -

丙二酸酯非常易于去质子化形成烯醇负离子，这是因为它的羰基α位和另外一个羰基连接。1,
3-二羰基形式能让形成的烯醇负离子更稳定，所以不需要很强的碱就能让其烯醇化。此效应可以引申出另外一种情况：即使羰基旁有两种α氢原子，却仍然可以控制形成哪种烯醇式。若两种α氢原子酸性具有较大差异，就会导致其中一个较强酸性质子被碱中和形成烯醇，而另一个酸性较弱α氢原子则不被碱影响\[73\]
\[74\]。这种类型的控制方法，只存在于酸性区别足够大而碱不过量的情况下。其应用在亚甲基临近于两个羰基或者氰基的分子中较常见，如[Knoevenagel缩合反应和](../Page/Knoevenagel缩合反应.md "wikilink")[丙二酸酯合成的第一步](../Page/丙二酸酯合成.md "wikilink")\[75\]
\[76\]\[77\]。

### 加料顺序

另一种常用的选择性控制的解决方案是让其中一个羰基化合物先形成烯醇负离子，然后在下加入另一个羰基化合物。\[78\]
动力学控制决定了正向羟醛加成反应必须明显快于反向羟醛反应。要达成这个目的必须满足其他两个条件：1、量化形成其中一种烯醇负离子；2、正向羟醛反应明显快于烯醇离子的交换过程。常见的动力学控制操作是使用[二异丙基氨基锂在](../Page/二异丙基氨基锂.md "wikilink")−78 °C下和酮反应得到烯醇负离子，而后缓慢加入醛\[79\]。

## 烯醇和烯醇负离子

### 互变异构

[烯醇是醛或酮的异构体](../Page/烯醇.md "wikilink")\[80\]
。在存在少量酸或碱的条件下烯醇和醛酮可以相互转化，属于一种[互变异构](../Page/互变异构体.md "wikilink")\[81\]\[82\]\[83\]，或称之为烯醇和醛酮互为互变异构体。酸性或中性条件下烯醇不稳定，互变异构的反应平衡有利于醛酮而不利于烯醇，所以醛酮中只含有微量的烯醇异构体\[84\]
。如下图所示：

[Aldol-10-CHSP.png](https://zh.wikipedia.org/wiki/File:Aldol-10-CHSP.png "fig:Aldol-10-CHSP.png")

### 形成

相比酮式，通常烯醇式是一种不稳定的互变异构形态，有三种较为稳定烯醇化合物：1) 形成烯醇的双键与分子内其他双键共轭\[85\]。2)
形成的烯醇式与较大的芳香基团连接\[86\]。3)
形成的烯醇式的双键碳原子直接连接多个氟原子\[87\]。而烯醇负离子可通过两种方法获得：1、强碱（“硬条件”）
2、[路易斯酸和弱碱](../Page/路易斯酸.md "wikilink")（“软条件”）\[88\]。

  -

要进行[去质子化](../Page/去质子化.md "wikilink")，立体电化学需要α-C-H[σ键必须能够和羰基的π轨道重叠](../Page/σ键.md "wikilink")\[89\]。

  -

### 立体化学

#### 烯醇化的区域选择性

。例如：

  -

三取代烯醇被认为是「动力学控制」的烯醇，而四取代烯醇则被认为是热力学控制的烯醇。α氢原子被去质子化后形成的动力学烯醇相对位阻更小，去质子化过程更快。总体来说，四取代烯烃由于超共轭，稳定性比三取代的烯烃更好。烯醇区域异构体的比例很大程度上受到反应中碱的影响。以上述为例，动力学控制可以通过二异丙基氨基锂在−78 °C下得到，且具有99：1的动力学选择性；而热力学烯醇可以通过[三苯基甲基锂在](../Page/有机锂试剂.md "wikilink")[室温下得到](../Page/室温.md "wikilink")10：90的热力学选择性\[90\]。一般来说，动力学烯醇更倾向于在低温下形成相对离子化的金属-氧键，而反应中快速去质子化需要通过使用略过量的碱，该碱需是强且高位阻的碱。体积大的碱只会中和位阻小的氢原子，低温条件和过量的碱能防止在初步烯醇化时平衡转化为更稳定的烯醇式。热力学烯醇更倾向于长的平衡时间与更高的温度，这种条件形成了相对共价的金属-氧碱而且使用少量的强碱。不足量的碱能够同时去质子化所有的羰基分子，不同的烯醇负离子和羰基之间进行质子交换并形成平衡，从而形成更稳定构型。这种选择性控制可以通过挑选金属离子和溶剂来进行\[91\]。

关于不同条件下烯醇化的扩展研究已经开始。当代有机化学在大多数情况下，已经可以得到想要的烯醇构型：\[92\]

  -

#### 烯醇化方法

醛酮羰基旁的α氢一般pKa为23左右。为了避免自身发生羟醛加成则需要使用如[二异丙基氨基锂的强碱](../Page/二异丙基氨基锂.md "wikilink")，才可迅速地使整个反应体系完全烯醇化。由于使用强碱，且提高去质子化的选择性，反应通常在[丙酮](../Page/丙酮.md "wikilink")[乾冰浴的低温下进行](../Page/乾冰.md "wikilink")。对于反应溶剂，则通常挑选[四氢呋喃](../Page/四氢呋喃.md "wikilink")、[二氯甲烷等非质子类极性溶剂](../Page/二氯甲烷.md "wikilink")。由于二异丙基氨基锂（LDA）这类强碱属于硬碱，所以这种方法属于硬式烯醇化。如果使用[路易斯酸先与醛酮羰基络合](../Page/路易斯酸.md "wikilink")，则能极大地提高α氢的酸性，就可以使用诸如[三乙胺的弱碱进行反应](../Page/三乙胺.md "wikilink")，从而实现脱质子烯醇化，这种方法就属于软式烯醇化\[93\]。

#### 烯醇盐的顺反异构

。见图：

[Z-E_isomers_cn.gif](https://zh.wikipedia.org/wiki/File:Z-E_isomers_cn.gif "fig:Z-E_isomers_cn.gif")

该烯醇的命名法与烯烃体系有所区别，规则是：只要是羰基氧原子烯醇化后与双键另一端的大基团处在一端的都认为是顺式，而与另一个R'的基团大小无关\[94\]。如：

[Thioenolate_cn.gif](https://zh.wikipedia.org/wiki/File:Thioenolate_cn.gif "fig:Thioenolate_cn.gif")

图例之烯醇盐按照普通规则属于反式，因硫的原子序数比氧大而基团优先，但在羟醛反应的研究中属于顺式。而对于酮类化合物，大多数的烯醇化条件都是得到Z构型的烯醇。而对于[酯类化合物](../Page/酯.md "wikilink")，大多数的烯醇化条件得到的是E式的烯醇。而在加入[六甲基磷酰胺后](../Page/六甲基磷酰胺.md "wikilink")，该去质子化过程的立体选择性可以与上述相反\[95\]\[96\]。

[aldol-43-CHSP.png](https://zh.wikipedia.org/wiki/File:aldol-43-CHSP.png "fig:aldol-43-CHSP.png")

烯醇形成的立体选择性可用Ireland模型进行解释，\[97\]\[98\]\[99\]\[100\]

  -

## 立体选择性

羟醛反应非常重要，因为其过程能产生两个手性中心。许多相关研究已经了解了反应机理并能通过不同的条件改进反应的选择性。顺式/反式的转化通常使用α-
和β-碳原子上的相对构型来表示。

[4_isomers_cn.png](https://zh.wikipedia.org/wiki/File:4_isomers_cn.png "fig:4_isomers_cn.png")

文献也曾经使用过[赤式/苏式来命名一些碳水化合物的立体构型](../Page/非对映异构#赤式苏式_/_苏式.md "wikilink")。而有机化学家们一直感兴趣的问题是如何控制羟醛反应，使四种可能产物中的一种成为主要产物，并尽量减少其他异构体的生成。

### 烯醇双键构型的影响

每一个烯醇的双键构型都确定了主要产物的相对立体构型，*E*型双键得到反式产物；*Z*型双键得到顺式产物：\[101\]

  -

因为烯醇盐的顺反异构对羟醛产物的立体化学有着直接的影响，所以需要对烯醇盐的顺反异构进行控制，以期获得特定结构的烯醇盐。在胺类碱的作用下，烯醇化的结果倾向于生成反式烯醇盐，如果在反应体系内添加[六甲基磷酰胺](../Page/六甲基磷酰胺.md "wikilink")（HMPA），则可以使上述结果相反。此现象可以用「Ireland模型」来解释：

[Ireland_mode_v2_cn.gif](https://zh.wikipedia.org/wiki/File:Ireland_mode_v2_cn.gif "fig:Ireland_mode_v2_cn.gif")

生成反式烯醇盐的六元环过渡态大取代基间相互积压程度小、能量低，容易形成过渡态，进而得到反式烯醇盐；同理，生成顺式的过渡态存在大取代基1,3-竖键相互左右，挤压程度高、能量上不利于形成过渡态，故顺式含量低。

羟醛反应研究的发展产生了更为可靠的烯醇化技术，通过选择不同的碱和路易斯酸催化剂，可以有效地产生97%以上的单一烯醇盐。基本上而言，使用小位阻硼硬路易斯酸[三氟甲磺酸二正丁基化硼](../Page/三氟甲磺酸二正丁基化硼.md "wikilink")（Bu<sub>2</sub>BOTf）和大位阻碱[N,N-二异丙基乙基胺](../Page/N,N-二异丙基乙基胺.md "wikilink")（DIPEA）有利于生成顺式烯醇盐；反之，大位阻软路易斯酸[氯代二环己基硼烷](../Page/氯代二环己基硼烷.md "wikilink")（Cy<sub>2</sub>BCl）和小位阻碱的搭配则有利于生成反式烯醇盐。见下图：

  -

### 金属离子的影响

烯醇金属离子的在确定羟醛反应的立体选择性上具有很重要的作用。[硼试剂](../Page/硼.md "wikilink")\[102\]\[103\]就常被用于立体选择性的羟醛反应，因为其[键长远比其他的金属离子](../Page/键长.md "wikilink")（如[锂](../Page/锂.md "wikilink")，[铝或者](../Page/铝.md "wikilink")[镁](../Page/镁.md "wikilink")）要短。

  -

硼-碳和硼-氧键的键长分别为1.4–1.5[Å和](../Page/埃格斯特朗.md "wikilink")1.5–1.6Å。而典型的金属-碳和金属-氧键的键长分别为1.9–2.2Å和2.0–2.2Å。硼试剂让金属原子“收紧”[过渡态而让反应具有更高的立体选择性](../Page/过渡态.md "wikilink")。\[104\]
这样，上述反应使用烯醇锂负离子得到的“顺：反”比例为80：20；而使用二丁基硼烯醇则得到了97：3的高选择性。

### 烯醇α-手性中心的影响

羟醛反应可以发生“底物介导的立体化学控制”，也就是存在[手性的底物发生反应可以影响到反应产物的手性](../Page/手性.md "wikilink")。若烯醇底物含有一个α-位的[手性中心](../Page/手性.md "wikilink")，则可以达到完美的立体化学控制。

  -

E式的烯醇，其主要控制因素为[1,3-烯丙位张力](../Page/1,3-烯丙位张力.md "wikilink")；而对于Z式烯醇，主要控制因素则是防止1,3-位的双直立键相互作用。总模型如下所示：

  -

为了让图解更加清晰易懂，烯醇分子的立体化学被[差向异构化](../Page/差向异构体.md "wikilink")；而在实际的反应中，醛的差向异构面也是可以发生亲核进攻反应的。在两个例子中，1,3-顺式的非对映体都是有利的。有许多此类立体化学控制的例子如图：\[105\]

[Aldol-23-CHSP.png](https://zh.wikipedia.org/wiki/File:Aldol-23-CHSP.png "fig:Aldol-23-CHSP.png")

### 亲电试剂α-手性中心的影响

当烯醇进攻的底物醛具有一个α-位的手性中心，同样可以达到完美的立体化学控制。大多数的研究表明*E*式烯醇参与了非对映体的选择；*Z*式烯醇参与了反-Felkin的选择性。总模型\[106\]\[107\]如下所示：

  -

由于*Z*式烯醇必须通过一种含有不稳定的顺式-戊烷中间体或者反-Felkin[旋转异构体进行反应](../Page/構象異構.md "wikilink")，所以*Z*式烯醇在这个例子当中降低了非对映选择性。相关例子如下：\[108\]\[109\]

  -

### 立体诱导的统一模型

若烯醇和醛分子本来就是手性分子，则可使用统一的立体化学模型以预测该“双重手性区分”的羟醛反应，而该模型需要同时考虑两个因素：烯醇分子的空间位阻和立体化学的对于反应的影响，以及醛的空间位阻对于反应的影响。\[110\]
一些关于此模型的应用如下所示：\[111\]

  -

## 埃文斯噁唑烷酮化学

现代有机合成化学需要合成光学纯的化合物。然而羟醛反应创造出两个手性中心，即形成四种手性异构体：

  -

现今已经发展出许多用于控制相对手性（如：顺式或反式）和绝对手性化学（如：*R* *S* 构型）的方法。

  -

一个广泛使用的方法为埃文斯[酰基噁唑烷酮法](../Page/酰基.md "wikilink")。\[112\]\[113\]
其由和同事于二十世纪七八十年代发现，这种方法通过添加一个[手性助剂来暂时建立手性烯醇](../Page/手性助剂.md "wikilink")。这种手性助剂通过非对映选择性反应将“手性”转移至产物。而后将助剂脱除，得到需要的手性异构体。

  -

在埃文斯的方法中，引入的手性助剂为噁唑烷酮，而形成的羰基化合物是一种[酰亚胺](../Page/酰亚胺.md "wikilink")。许多噁唑烷酮试剂现在都已商品化，且可买到两种对映体，其售价约为每克10至20美元。

  -

噁唑烷酮的[酰化反应操作过程简单](../Page/酰化反应.md "wikilink")。Z型烯醇可以通过「硼-介导的软性烯醇化」得到顺式-羟醛加合物。\[114\]

  -

通常单个[非对映体可以通过羟醛加成物的](../Page/非对映异构.md "wikilink")[结晶操作获得](../Page/结晶.md "wikilink")。尽管成本高且只能得到“顺式”产物
，埃文斯羟醛由于可靠性高被广泛使用。一些断裂助剂的方法如下：\[115\]

  -

通过构建酰亚胺结构，顺式和反式选择性的羟醛加成反应都可进行，其允许形成四种当中的三种手性组合：顺式选择性\[116\]
和反式选择性：\[117\]

  -

在顺式-选择性反应中，两种烯醇化方法都如预期的得到*Z*型烯醇，然而反应的手性化学并非受到噁唑烷酮而是甲基手性中心的控制。该法还允许手性选择的组建聚酮（一类具有反合成子的天然产物）。

## 现代方法与变化

羟醛反应的现代化学方法学发展出了许多不对称羟醛反应，这些不对称反应常使用催化量的。当反应使用少量的[光学纯配体](../Page/对映异构.md "wikilink")，即可诱导形成光学纯的羟醛反应产物，该反应也属于一种“催化的非对称”反应。根据催化剂的不同可以将非对称反应分类为以下几种羟醛反应类型：

### 乙酰羟醛反应

前文所述手性助剂的主要局限性在于：N-乙酰[酰亚胺反应不具选择性](../Page/酰亚胺.md "wikilink")。早期的解决方法是暂时引入[硫醚基团](../Page/硫醚.md "wikilink")：\[118\]\[119\]

  -

### 向山羟醛反应

[向山羟醛反应是在](../Page/mukaiyama羟醛反应.md "wikilink")[路易斯酸](../Page/路易斯酸.md "wikilink")，如[三氟化硼或](../Page/三氟化硼.md "wikilink")[四氯化钛的催化下](../Page/四氯化钛.md "wikilink")，[硅烯醇醚对](../Page/烯醇硅醚.md "wikilink")[醛的](../Page/醛.md "wikilink")[亲核加成反应](../Page/亲核加成反应.md "wikilink")。
\[120\]\[121\]
向山羟醛反应并不符合齐默曼-特拉克斯勒模型。后来卡雷拉（Carreira）发现了一种具实用价值的利用硅烯酮缩醛的不对称合成法：使用含钛的西弗碱催化反应（见下图）。这种催化剂被发现具有有高度的对映选择性及广泛的底物适用性，在优化条件下，只需0.5%摩尔比例的催化剂便可以得到高收率且高ee的加成产物\[122\]
\[123\]\[124\]。该方法常应用于脂肪族醛，由于在[对映面两侧的电性和手性差别较小](../Page/对映面.md "wikilink")，该底物的[亲电性对于催化不对称反应的通常较弱](../Page/亲电体.md "wikilink")。
\[125\]

  -

[乙烯类似物](../Page/乙烯类似物.md "wikilink")（Vinylogous）的向山羟醛过程还可以发生催化非对称性反应。下图展示了仅对芳香醛的有效反应，其机理被认为与具手性且金属-键合的二烯醇有关。\[126\]\[127\]

  -

### 克里明斯噻唑硫酮羟醛反应

近期发现一种新型埃文斯助剂：按其发现化学家命名为克里明斯（Crimmins）噻唑硫酮催化剂\[128\]\[129\]，虽然不及埃文斯的反应实例中的数据，但这种催化剂能够使反应的[收率](../Page/收率.md "wikilink")、[非对映选择性和对映选择性都大大提高](../Page/非对映选择性.md "wikilink")。而优于早期的埃文斯助剂，噻唑硫酮对于乙酰羟醛反应同样有效，\[130\]
且克里明斯发现可以通过使用少量的得到“埃文斯顺式”或“非-埃文斯顺式”的加成产物。该反应的反应机理认为是通过形成钛-键合六元环[过渡态进行的](../Page/过渡态.md "wikilink")，而该过渡态机理类似于埃文斯助剂。

  -

### 有机催化法

羟醛反应的另一个最新进展发现了可使用手性二级[胺催化剂进行反应](../Page/胺.md "wikilink")。这种二级胺和酮反应会先形成[烯胺中间体](../Page/烯胺.md "wikilink")，并与合适的醛进行对映选择性加成。\[131\]\[132\]\[133\]\[134\]\[135\]胺与羰基化合物反应得到的烯胺（类似于烯醇）可作为亲核试剂参与反应，而胺又会从产物上脱除重新进入催化循环。烯胺催化法大多基于一些有机小分子底物，因此属于[有机催化的一种](../Page/有机催化.md "wikilink")。在下图中，[脯氨酸对于催化三酮的环化反应非常有效](../Page/脯氨酸.md "wikilink")\[136\]\[137\]：

  -

该反应也称为[Hajos-Parrish反应](../Page/Hajos-Parrish反应.md "wikilink")\[138\]\[139\]
（同样还称为Hajos-Parrish-Eder-Sauer-Wiechert反应）\[140\]
Hajos-Parrish条件仅需催化量的[脯氨酸](../Page/脯氨酸.md "wikilink")（3 mol%）。由于过渡态的烯胺中间体比酮（前体）的烯醇式更具亲核性，因此该反应没有诸如非手性反应的问题。这种策略很具实用价值，因为它提供了一种简单进行对映选择性反应的方法，而且不需要使用昂贵或有毒性的过渡金属催化剂。在2000年BarbasIII和同事在前人的基础优化了该方法，发现在30 mol%[脯氨酸的催化下](../Page/脯氨酸.md "wikilink")，可以使羟醛反应的ee值达到99%以上；而羟基丙酮底物则可以得到高选择性的顺式双羟基产物\[141\]\[142\]。

值得一提的是脯氨酸-催化的羟醛反应不具有任何「非线性效应」。（线性效应是指：产物对映选择性的比例直接与催化剂的光学纯度相关）。结合[同位素标记得到的证据及](../Page/同位素标记.md "wikilink")[计算化学研究](../Page/计算化学.md "wikilink")，假定的脯氨酸-催化的羟醛反应机理如下所示：\[143\]

  -

这种策略允许竞争性的双醛交叉羟醛反应。通常两醛之间的交叉-羟醛反应都是具有竞争性的，因此它们很容易[聚合或进行非选择性反应而得到混合产物](../Page/聚合.md "wikilink")。第一个例子如下：\[144\]

  -

基于烯醇的羟醛加成反应会优先形成顺式产物，而有机催化形成的加成产物是反式-选择性的。在许多实例中，有机催化条件可以非常温以防止多聚化。然而由于两种亲电试剂都能产生烯醇化质子，所以为了提高选择性就必须使用注射泵缓慢滴加亲电试剂加以控制。如果其中的一种原料不具烯醇化的α氢原子或β侧链，就可以实现可控制的加成反应。

麦克米伦和同事于2004年发现了一个很好的展示不对称有机催化羟醛反应的例子，他们通过不同的方法保护[碳水化合物](../Page/醣類.md "wikilink")（糖类）。传统合成[己糖的方法是使用多重](../Page/己糖.md "wikilink")[保护-脱保护策略](../Page/保护基.md "wikilink")，共需要8至14步完成，而有机催化法能够作用于许多相同底物，且仅用两步就可顺利完成。该反应使用了脯氨酸-催化的α羟基醛的二聚反应，然后进行几步向山羟醛环化反应完成合成。

  -

α羟醛的二聚化反应要防止羟醛加成物进一步发生加成反应。\[145\]
早期的研究表明能够耐受α-[烷氧](../Page/烷氧基.md "wikilink")，或者α-[硅氧的](../Page/硅氧.md "wikilink")[保护基适用于此反应](../Page/保护基.md "wikilink")，因为能耐受[吸电子基团](../Page/吸电子基团.md "wikilink")（如[乙酰基](../Page/乙酰基.md "wikilink")）的醛都不具反应性。保护后的[赤藓糖产物通过向山羟醛加成](../Page/赤藓糖.md "wikilink")，而后进行化转化为四种不同的糖分子。这需要在向山加成中进行适当的非对映体控制，并且产物[硅氧碳正离子会优先环化](../Page/碳正离子.md "wikilink")，而不是进一步进行羟醛反应。例如，[葡萄糖](../Page/葡萄糖.md "wikilink")、[甘露糖](../Page/甘露糖.md "wikilink")、[阿洛糖都可通过此法制备](../Page/阿洛糖.md "wikilink")：

  -

### “直接”羟醛加成

普通的羟醛反应中，羰基化合物都是进行去质子化形成烯醇。烯醇加入到醛或者酮化合物中形成醇盐，然后进行酸化和后处理。有一种更好的方法，理论上能够不需要多步操作，而可“直接”进行一步反应。其想法是利用金属[催化剂在羟醛加成的过程中释放](../Page/催化剂.md "wikilink")。而问题在于反应产生的醇盐比起始原料具有更强的碱性。产物和金属离子紧密键合，以致阻碍了它与羰基原料继续进行反应\[146\]\[147\]\[148\]。

  -

由埃文斯发现的一种方法，使用了硅基化的羟醛加成物进行反应。\[149\]\[150\]
例如一种硅试剂[三甲基氯硅烷参与反应用于代替金属离子结合醇盐](../Page/三甲基氯硅烷.md "wikilink")，并允许金属催化剂的循环利用。该法减少了反应步数，降低了试剂消耗，使反应更经济且适用于工业领域。

  -

一种最近由Shair发现的[仿生合成法](../Page/仿生学.md "wikilink")，使用β-硫[酮酸作为亲核试剂](../Page/酮酸.md "wikilink")。\[151\]
其中一半的酮酸在反应中进行了[脱羧](../Page/脱羧反应.md "wikilink")。其过程非常类似于通过来应用[丙二-辅酶A](../Page/丙二-辅酶A.md "wikilink")。示例中的手性配体为。有趣的是，芳香族和支链脂肪族醛在此例中无法很好的进行反应。

  -

## 生物的羟醛反应

[ALDO_reaction.png](https://zh.wikipedia.org/wiki/File:ALDO_reaction.png "fig:ALDO_reaction.png")
生物化学中的羟醛反应，如[糖酵解反应的第二阶段](../Page/糖酵解.md "wikilink")：[1,6-二磷酸果糖分解成](../Page/果糖-1,6-雙磷酸.md "wikilink")[3-磷酸甘油醛和](../Page/甘油醛-3-磷酸.md "wikilink")[二羟基丙酮](../Page/二羟基丙酮.md "wikilink")，该反应是由[醛缩酶A催化的逆向羟醛反应](../Page/醛缩酶A.md "wikilink")\[152\]。又如：萜类在生物中间体[甲瓦龙酸中的合成](../Page/甲羟戊酸.md "wikilink")\[153\]等。

## 参见

  - [羟醛-Tishchenko反应](../Page/羟醛-Tishchenko反应.md "wikilink")
  - [贝里斯-希尔曼反应](../Page/贝里斯－希尔曼反应.md "wikilink")
  - [Ivanov反应](../Page/Ivanov反应.md "wikilink")
  - [雷福尔马茨基反应](../Page/雷福尔马茨基反应.md "wikilink")

## 註解

<references group="註解"/>

## 参考文献

## 外部連結

  - [Chem 206 Lecture Notes
    (Fall 2003)](https://ia801505.us.archive.org/29/items/EvansD.A.HarvardsAdvancedOrganicChemistry2003/Evans%20D.%20A.%20Harvard's%20Advanced%20Organic%20Chemistry%20\(2003\).pdf)
    by [D. A. Evans](../Page/David_A._Evans.md "wikilink") *et al.*,
    Harvard University (pp. 345–372)
  - [Chem 106 Aldol Reaction
    Notes](http://isites.harvard.edu/fs/docs/icb.topic1146159.files/JJB%20Aldol%20Lecture%2021%20Final.pdf)

[Category:加成反应](../Category/加成反应.md "wikilink")
[Category:缩合反应](../Category/缩合反应.md "wikilink")
[Category:碳－碳键形成反应](../Category/碳－碳键形成反应.md "wikilink")

1.

2.

3.

4.  鮑羅丁觀察到乙醛在酸性環境下會二聚化，形成3-羥基丁醛

5.

6.

7.

8.

9.

10. .

11.

12.

13.

14.

15.

16.

17.

18.

19. 3-Hydroxy-3-Methyl-1-Phenyl-1-Butanone by Crossed Aldol Reaction
    Teruaki Mukaiyama and Koichi Narasaka [Organic
    Syntheses](../Page/有机合成_\(期刊\).md "wikilink"), Coll. Vol. 8,
    p.323 (1993); Vol. 65, p.6 (1987)
    [Link](http://www.orgsynth.org/orgsyn/pdfs/CV8P0323.pdf)

20.

21.

22.

23.

24.

25.

26.

27. Bednarski, M. D. Applications of enzymic aldol reactions in organic
    synthesis. Applied Biocatalysis 1991, 1, 87-116.

28. Fessner, W. D. Enzyme-catalyzed aldol additions in asymmetric
    synthesis. Part 1. Kontakte (Darmstadt) 1992, 3-9.

29. Fessner, W. D. Enzyme-catalyzed aldol additions in asymmetric
    synthesis. Part 2. Kontakte (Darmstadt) 1993, 23-34.

30. Petersen, M., Zannetti, M. T., Fessner, W.-D. Tandem asymmetric C-C
    bond formations by enzyme catalysis. Top. Curr. Chem. 1997, 186,
    87-117.

31. Takayama, S., McGarvey, G. J., Wong, C.-H. Enzymes in organic
    synthesis: recent developments in aldol reactions and
    glycosylations. Chem. Soc. Rev. 1997, 26, 407-415.

32. W.-D. Fessner, in Modern Aldol Reactions, ed. R. Mahrwald,
    Wiley-VCH, Berlin, 2004, vol. 1, ch. 5, pp. 201–272.

33.

34.

35. M. Shibasaki, S. Matsunaga and N. Kumagai, in Modern Aldol
    Reactions, ed. R. Mahrwald, Wiley-VCH, Berlin, 2004, vol. 2, ch. 6,
    pp. 197–227.

36. A. Yanagisawa, in Modern Aldol Reactions, ed. R. Mahrwald,
    Wiley-VCH, Berlin, 2004, vol. 2, ch. 1, pp. 1–23.

37.

38.

39.

40.

41.

42.

43.

44. E. J. Ariëns: [Stereochemistry, a basis for sophisticated nonsense
    in pharmacokinetics and clinical
    pharmacology](http://www.springerlink.com/content/ng657086x6u73p44/)
    European Journal of Clinical Pharmacology 26 (1984) 663-668.  預覽：

45. Journal of the American Chemical Society, 1990 , vol. 112, \# 14 p.
    5583 - 5601

46. Journal of Organic Chemistry, 1996 , vol. 61, \# 20 p. 6856 - 6872.

47. Nerenberg, J. B.; Hung, D. T.; Somers, P. K.; Schreiber, S. L. J.
    Am. Chem. Soc. 1993, 115, 12621–12622.

48. Harried, S. S.; Yang, G.; Strawn, M. A.; Myles, D. C. J. Org. Chem.
    1997, 62, 6098–6099.

49. Journal of the American Chemical Society, 1987 , vol. 109, \# 9 p.
    2821 - 2822.

50. \<Bioorganic and Medicinal Chemistry Letters, 2007 , vol. 17, \# 9
    p. 2554 - 2557.

51. I. K. Mangion and D. W. C. MacMillan, J. Am. Chem. Soc., 2005, 127,
    3696.

52. B. M. Trost, A. Fettes and B. T. Shireman, J. Am. Chem. Soc., 2004,
    126, 2660.

53. B. M. Trost, M. U. Frederiksen, J. P. N. Papillon, P. E. Harrington,
    S. Shin and B. T. Shireman, J. Am. Chem. Soc., 2005, 127, 3666.

54. P. M. Pihko and A. Erkkila¨ , Tetrahedron Lett., 2003, 44, 7607.

55. J. Carpenter, A. B. Northrup, D. Chung, J. J. M. Wiener, S.-G. Kim
    and D. W. C. MacMillan, Angew. Chem., Int. Ed., 2008, 47, 3568.

56. Gennari, C. In Comprehensive Organic Synthesis; Trost, B.
    M.,Ed.;Pergamon: Oxford, 1993; Vol. 2, Chapter 2.4, p 629.

57. Lefour, J.-M.; Loupy, A. Tetrahedron 1978, 34, 2597.

58. Loupy, A.; Meyer, G.; Tchoubar, B. Tetrahedron 1978, 34, 1333.

59. Murthy, A. S. N.; Bhardwaj, A. P. J. J. Chem. Soc., Perkin Trans
    1984, 2, 727.

60. Patai, S. The Chemistry of the Carbonyl Group, Wiley, London, 1966;

61. Kresge, A.J. Chem. Soc. Rev. 1996, 25, 275;

62. Reetz, M. T.; Hu¨ llmann, M.; Seitz, T. Angew. Chem., Int. Ed. Engl.
    1987, 26, 477.

63. Shambayati, S.; Schreiber, S. L. in Comprehensive Organic Synthesis;
    Trost, B. M., Ed.; Pergamon: Oxford, 1993; Vol. 1, Chapter 1.10, p
    283.

64. Gennari, C. In Comprehensive Organic Synthesis; Trost, B. M., Ed.;
    Pergamon: Oxford, 1993; Vol. 2, Chapter 2.4, p 629.

65.

66. Heathcock, C. H. The Aldol Reaction: Group I and II Enolates. in
    Comp. Org. Synth. (eds. Trost, B. M.,Fleming, I.), 1, 181-231
    (Pergamon Press, Oxford, 1991).

67. Mukaiyama, T. The directed aldol reaction. Org. React. 1982, 28,
    203-331.

68. Heathcock, C. H. The Aldol Reaction: Acid and General Base
    Catalysis. in Comp. Org. Synth. (eds. Trost, B. M.,Fleming, I.), 1,
    133-179(Pergamon Press, Oxford, 1991).

69. Mukaiyama, T. The directed aldol reaction. Org. React. 1982, 28,
    203-331.

70.
71.

72. Palomo, C., Oiarbide, M., Garcia, J. M. The aldol addition reaction:
    an old transformation at constant rebirth. Chem.-- Eur. J. 2002, 8,
    36-44.

73. Franklin, A. S., Paterson, I. Recent developments in asymmetric
    aldol methodology. Contemp. Org. Synth. 1994, 1, 317-338.

74. Heathcock, C. H. The aldol condensation as a tool for
    stereoselective organic synthesis. Curr. Trends Org. Synth., Proc.
    Int. Conf., 4th 1983, 27-43.

75. E. Knoevenagel. Ueber eine Darstellungsweise des
    Benzylidenacetessigesters. Ber. Dtsch. Chem. Ges. 1896, 29 (1):
    172–174. <doi:10.1002/cber.18960290133>.

76. Emil Knoevenagel. Condensation von Malonsäure mit Aromatiachen
    Aldehyden durch Ammoniak und Amine. Ber. Dtsch. Chem. Ges. 1898, 31:
    2596–2619. <doi:10.1002/cber.18980310308>

77. House, Herbert O. Modern Synthetic Reactions. Menlo Park, CA.: W. A.
    Benjamin. 1972. ISBN 978-0-8053-4501-8.

78. Bal, B.; Buse, C. T.; Smith, K.; Heathcock, C. H. *Org. Syn.*, Coll.
    Vol. 7, p.185 (1990); Vol. 63, p.89 (1985).
    ([Article](http://www.orgsyn.org/orgsyn/prep.asp?prep=cv7p0185))

79. Evans, D. A., Nelson, J. V., Taber, T. R. Stereoselective aldol
    condensations. Top. Stereochem. 1982, 13, 1-115.

80.

81.

82.

83.

84.

85. Hart, H.; Rappoport, Z.; Biali, S.E., in Rappoport, Z. The Chemistry
    of Enols, Wiley, NY, 1990, pp. 481–589;

86. Rappoport, Z.; Biali, S.E. Acc. Chem. Res. 1988, 21, 442.

87. Bekker, R.A.; Knunyants, I.L. Sov. Sci. Rev. Sect. B 1984, 5, 145.

88. Iglesias, E. J. Org. Chem, 2003, 68, 2680.

89. Kresge, A.J. Pure Appl. Chem. 1991, 63, 213; Capon, B., in
    Rappoport, Z. The Chemistry of Enols, Wiley, NY, 1990, pp. 307–322

90. Heathcock, C. H. Science 1981, 214, 395.

91. Takacs, J. M.; McGee, L. R.; Ennis, M. D.; Mathre, D. J.; Bartroli,
    J. Pure Appl. Chem. 1981, 53, 1109

92.

93. Braun, M. Methoden Org. Chem. (Houben Weyl), 4th Ed. 1952-1986, E21,
    1603.

94. IUPAC Commission on the Nomenclature of Organic Chemistry and
    Commission on Physical Organic Chemistry, Glossary of Class Names of
    Organic Compounds and Reactive Intermediates Based on Structure.
    IUPAC Recommendations 1995. Prepared for publication by G.P. Moss,
    P.A.S. Smith, D. Tavernier. Pure Appl. Chem. 67, 1307-1375 (1995).

95. Paterson, I. Org. React. 1997, 51, 1.

96. Gennari, C. In Comprehensive Organic Synthesis; Trost, B. M., Ed.;
    Pergamon: Oxford, 1993; Vol. 2, Chapter 2.4, p 629.

97.

98.

99.

100.

101.
102. Cowden, C. J.; Paterson, I. *Org. React.* 1997, *51*, 1. (doi:
     [10.1002/0471264180.or051.01](http://dx.doi.org/10.1002/0471264180.or051.01))

103.

104.

105.

106. Evans, D. A. *et al.* *Top. Stereochem.* 1982, *13*, 1–115.
     (Review)

107.

108.

109.

110.

111.
112.
113.
114.

115.

116.

117.

118.
119. 该反应中，亲核试剂为一硼烯醇分子，该分子由[二丁基硼三氟甲磺酸盐](../Page/二丁基硼三氟甲磺酸盐.md "wikilink")
     （nBu<sub>2</sub>BOTf）反应衍生，碱则是[N,N-二异丙基乙基胺](../Page/N,N-二异丙基乙基胺.md "wikilink")。硫醚在第二步中用[雷尼镍](../Page/雷尼镍.md "wikilink")/氢气[还原法脱除](../Page/有机氧化还原反应.md "wikilink")。

120.

121.

122.

123. E. M. Carreira, in Comprehensive Asymmetric Catalysis, Vol. 3, Eds.
     E.N. Jacobsen, A. Pfaltz and H. Yamamoto, Springer, Heildelberg,
     1999, pp. 997–1065.

124. D. Machajewski and C.-H. Wong, Angew. Chem. Int. Ed., 2000, 39,
     1352.

125.

126.

127.

128.

129.

130. Crimmins, Org. Lett. 2007, 9(1), 149–152.

131.

132. B. Alcaide and P. Almendros, Angew. Chem., Int. Ed., 2003, 42, 858.

133. G. Guillena, C. Najera and D. J. Ramon, Tetrahedron: Asymmetry,
     2007, 18, 2249.

134. C. Palomo, M. Oiarbide and J. M. Garcia, Chem. Soc. Rev., 2004, 33,
     65.

135. D. W. C. MacMillan, Nature, 2008, 455, 304.

136. C. Pidathala, L. Hoang, N. Vignola and B. List, Angew. Chem., Int.
     Ed., 2003, 42, 2785.

137. C. L. Chandler and B. List, J. Am. Chem. Soc., 2008, 130, 6737.

138. Z. G. Hajos, D. R. Parrish, German Patent DE 2102623 1971

139.

140.

141. W. Notz and B. List, J. Am. Chem. Soc., 2000, 122, 7386.

142. K. Sakthivel, W. Notz, T. Bui and C. F. Barbas III, J. Am. Chem.
     Soc., 2001, 123, 5260.

143.

144.

145.

146. M. Braun, in Stereoselective Synthesis, Houben-Weyl, Vol. E21/3,
     Eds. G. Helmchen, R. W. Hoffmann, J. Mulzer and E. Schaumann,
     Thieme, Stuttgart, 1996, p. 1603.

147. C. J. Cowden and I. Paterson, Org. React., 1997, 51, 1.

148.

149.

150.

151.

152.

153. Brady RO. THE ENZYMATIC SYNTHESIS OF FATTY ACIDS BY ALDOL
     CONDENSATION. *Proceedings of the National Academy of Sciences of
     the United States of America*. 1958;44(10):993-998.