**安东尼**（[法语](../Page/法语.md "wikilink")：****）是[法国](../Page/法国.md "wikilink")[法兰西岛大区](../Page/法兰西岛大区.md "wikilink")[上塞纳省的](../Page/上塞纳省.md "wikilink")[副省会](../Page/副省会.md "wikilink")，位于[巴黎南部郊区](../Page/巴黎.md "wikilink")，面积9.56平方公里，人口59,855人（1999年）。

## 名人

  - [洛朗·拉福格](../Page/洛朗·拉福格.md "wikilink")：法国数学家、[菲尔兹奖获得者](../Page/菲尔兹奖.md "wikilink")

## 交通

  - [安东尼站](../Page/安东尼站.md "wikilink")
  - [安东尼城市公共交通](../Page/安东尼城市公共交通.md "wikilink")

## 友好城市

  - [希腊](../Page/希腊.md "wikilink")[埃莱夫塞鲁波利](../Page/埃莱夫塞鲁波利.md "wikilink")

  - [以色列](../Page/以色列.md "wikilink")[斯德罗特](../Page/斯德罗特.md "wikilink")

  - [美国](../Page/美国.md "wikilink")[列克星敦](../Page/列克星敦_\(马萨诸塞州\).md "wikilink")

  - [意大利](../Page/意大利.md "wikilink")[科莱尼奥](../Page/科莱尼奥.md "wikilink")

  - [突尼斯](../Page/突尼斯.md "wikilink")[哈马姆利夫](../Page/哈马姆利夫.md "wikilink")

  - [德国](../Page/德国.md "wikilink")[赖尼肯多夫](../Page/赖尼肯多夫.md "wikilink")

  - [意大利](../Page/意大利.md "wikilink")[都灵](../Page/都灵.md "wikilink")

  - [俄罗斯](../Page/俄罗斯.md "wikilink")[普罗特维诺](../Page/普罗特维诺.md "wikilink")

  - [英国](../Page/英国.md "wikilink")[刘易舍姆](../Page/刘易舍姆.md "wikilink")

  - [捷克](../Page/捷克.md "wikilink")[奥洛穆茨](../Page/奥洛穆茨.md "wikilink")

## 外部链接

  - [Antony city council website](http://www.ville-antony.fr/)
  - [Paris-Sud
    Community](https://web.archive.org/web/20070202044919/http://parissud.wordpress.com/)

[A](../Category/上塞纳省市镇.md "wikilink")