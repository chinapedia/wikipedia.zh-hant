**PortalPlayer**曾是一家著名的為[數字音頻播放器供應](../Page/數字音頻播放器.md "wikilink")[SoC半導體](../Page/系統單晶片.md "wikilink")、固件與軟體的[無廠半導體公司](../Page/無廠半導體公司.md "wikilink")。該公司負責半導體設計與固件開發，[半導體的實際](../Page/半導體.md "wikilink")[生產會外判給商業](../Page/生產.md "wikilink")[代工廠](../Page/代工.md "wikilink")。2007年1月5日[NVIDIA宣佈以](../Page/NVIDIA.md "wikilink")3億5700萬收購PortalPlayer\[1\]。

## 產品介紹

### PortalPlayer 5002

[PP5002規格](https://web.archive.org/web/20061202104706/http://www.portalplayer.com/products/documents/5002_brief_0108_Public.pdf)

由下列裝置使用：

  - [iPod](../Page/iPod.md "wikilink")：第一、二與第三代
    [1](https://web.archive.org/web/20050829181744/http://www.ipodlinux.org/PP5002)
    [2](http://www.rockbox.org/twiki/bin/view/Main/PortalPlayer)

### PortalPlayer 5003

[PP5003規格](https://web.archive.org/web/20061212093505/http://www.portalplayer.com/products/documents/5003_brief_0108_Public.pdf)

由下列裝置使用：

  - [Rio](../Page/Rio.md "wikilink") Karma
    [3](https://web.archive.org/web/20060813210834/http://www.gizmodo.com/archives/ogg-on-ipod-why-the-ipod-may-not-have-the-horsepower-for-ogg-015607.php)

### PortalPlayer 5020

[PP5020規格](https://web.archive.org/web/20061202104042/http://www.portalplayer.com/products/documents/5020_Brief_0108_Public.pdf)

包含兩個[ARM](../Page/ARM_architecture.md "wikilink")[處理器的](../Page/處理器.md "wikilink")[SoC](../Page/系統單晶片.md "wikilink")，分別以75[兆赫運行](../Page/兆赫.md "wikilink")。

由下列裝置使用：

  - [iPod](../Page/iPod.md "wikilink")：第四代、iPod Photo與第一代iPod Mini
    [4](https://web.archive.org/web/20050828045346/http://www.ipodlinux.org/PP5020)[5](http://www.rockbox.org/twiki/bin/view/Main/PortalPlayer)
  - [iriver](../Page/iriver.md "wikilink") H10 系列
    [6](http://www.rockbox.org/twiki/bin/view/Main/IriverH10Info#Portable_Player_5020)[7](http://www.rockbox.org/twiki/bin/view/Main/PortalPlayer)
  - Philips HDD100/120（未經證實，檔案格式及規格與下方的YH-925相符）
  - ROCdigital rocbox model 14003
  - Samsung YH-925 [8](http://www.samsungplay.com/en/925.htm)
    中指出"Platform: PP5020"
  - Samsung YEPP YH-820MC（根據
    [9](https://web.archive.org/web/20070103095617/http://www.tgdaily.com/2005/11/29/teardown_iriver_h10/)）
  - [Tatung Elio
    M310](https://web.archive.org/web/20070114074056/http://en.europe.eliohome.com/product_m310_01.html)（system/pp5020.mi4
    包含"PP5020AF-05.11-TG01-11.40-TG01-11.40-DT"一行與"Copyright(c) 1999 -
    2003 PortalPlayer, Inc."）
  - Virgin player 5GB（根據
    [10](https://web.archive.org/web/20060113021847/http://www.virginelectronics.com/faq-player_5gb.htm)）
  - [MSI
    Megaplayer 540](http://www.msi.com.tw/program/support/download/dld/spt_dld_detail.php?UID=622&kind=6)，固件system/pp5020.mi4包含"PP5020AF..."
    一行；在德國由ALDI以Medion MD81034的名字發售。

### PortalPlayer 5022

[PP5022規格](https://web.archive.org/web/20061114012208/http://www.portalplayer.com/products/documents/5022_Brief_Mar05.pdf)

由下列裝置使用：

  - [iPod](../Page/iPod.md "wikilink")：第二代iPod Mini與第四代視頻iPod
    [11](https://web.archive.org/web/20050830032742/http://www.ipodlinux.org/PP5022)
    [12](http://www.rockbox.org/twiki/bin/view/Main/PortalPlayer)

### PortalPlayer 5024

[PP5024規格](http://www.nvidia.com/page/pp_5024.html)

最新的PortalPlayer 音頻[芯片組](../Page/晶元組.md "wikilink")，用於現時的播放器。

由下列裝置使用：

  - [Sandisk](../Page/Sandisk.md "wikilink")
    [Sansa](../Page/Sansa.md "wikilink") e200系列
    [13](http://www.anythingbutipod.com/archives/2006/03/sandisk-sansa-e200-series-review.php)

## 參考文獻

[Category:無廠半導體公司](../Category/無廠半導體公司.md "wikilink")
[Category:納斯達克已除牌公司](../Category/納斯達克已除牌公司.md "wikilink")
[Category:已結業電腦公司](../Category/已結業電腦公司.md "wikilink")
[Category:聖塔克拉拉公司](../Category/聖塔克拉拉公司.md "wikilink")

1.  [NVIDIA to Acquire
    PortalPlayer](http://www.nvidia.com/object/IO_37040.html)