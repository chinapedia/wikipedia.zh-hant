**2024年**是一個[閏年](../Page/閏年.md "wikilink")，它的第一天從[星期一開始](../Page/星期一.md "wikilink")。2024年又是：

## 預言

  - [推背圖第四十三象](../Page/推背圖.md "wikilink")，「頌曰：黑兔走入青龍穴，欲盡不盡不可說。唯有外邊根樹上，三十年中子孫結。」
  - [印度人口超过](../Page/印度人口.md "wikilink")[中国](../Page/中国.md "wikilink")，成为[世界人口最多的国家](../Page/世界人口.md "wikilink")\[1\]
  - 俄罗斯发射[联邦号载人飞船](../Page/联邦号载人飞船.md "wikilink")\[2\]。

## 大事記

| 月  | 日 | 內容                                                                                                                     |
| -- | - | ---------------------------------------------------------------------------------------------------------------------- |
| 4  | 8 | [日全蝕](../Page/日全蝕.md "wikilink")（中太平洋、墨西哥北部、東部、西南部和美國中部、加拿大東南部和大西洋北部海域）。                                               |
| 8  | 2 | [第33屆夏季奧林匹克運動會將在](../Page/2024年夏季奧林匹克運動會.md "wikilink")[法國](../Page/法國.md "wikilink")[巴黎舉行](../Page/巴黎.md "wikilink")。 |
| 10 | 4 | [UNIX時間第](../Page/UNIX時間.md "wikilink")20000紀念日。                                                                       |
|    |   |                                                                                                                        |

### 時間未定

  - 期望[火星一號能將第一批](../Page/火星一號.md "wikilink")[人類送上](../Page/人類.md "wikilink")[火星](../Page/火星.md "wikilink")\[3\]
  - [PLATO推出第三](../Page/PLATO_\(spacecraft\).md "wikilink")[ESA宇宙願景的M級任務](../Page/ESA.md "wikilink")\[4\]
  - [台鐵](../Page/台鐵.md "wikilink")
    [台南鐵路地下化](../Page/台南鐵路地下化.md "wikilink")
    [台北捷運](../Page/台北捷運.md "wikilink") [三鶯線](../Page/三鶯線.md "wikilink")
    [台北捷運民生汐止線第一階段](../Page/台北捷運民生汐止線.md "wikilink")
    [高雄捷運](../Page/高雄捷運.md "wikilink")
    [岡山路竹延伸線第二階段](../Page/岡山路竹延伸線.md "wikilink")
    [基隆輕軌第一階段預計於本年完工](../Page/基隆輕軌.md "wikilink")
  - [中華民國第](../Page/中華民國.md "wikilink")16屆總統選舉

## 出生

## 逝世

## [節氣](../Page/節氣.md "wikilink")

## [諾貝爾獎](../Page/諾貝爾獎.md "wikilink")

  - [物理學](../Page/諾貝爾物理學獎.md "wikilink")
  - [化學](../Page/諾貝爾化學獎.md "wikilink")
  - [醫學](../Page/諾貝爾醫學獎.md "wikilink")
  - [文學](../Page/諾貝爾文學獎.md "wikilink")
  - [和平](../Page/諾貝爾和平獎.md "wikilink")
  - [經濟學](../Page/諾貝爾經濟學獎.md "wikilink")

## [奧斯卡金像獎](../Page/奧斯卡金像獎.md "wikilink")

  -
## 參考文獻

[4](../Category/2020年代.md "wikilink")
[2](../Category/21世纪各年.md "wikilink")

1.
2.
3.  <http://mars-one.com/>
4.