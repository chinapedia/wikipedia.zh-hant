**薑科**（[學名](../Page/學名.md "wikilink")：）是[單子葉植物](../Page/單子葉植物.md "wikilink")[薑目的一](../Page/薑目.md "wikilink")[科](../Page/科_\(生物\).md "wikilink")。本科約有47[屬](../Page/屬.md "wikilink")，700[種](../Page/種.md "wikilink")，主要分佈在[熱帶地區](../Page/熱帶.md "wikilink")。[中国有](../Page/中国.md "wikilink")17属约120种，主要分布在南方各地。姜科是多年生[草本](../Page/草本.md "wikilink")，通常具有芳香，[茎短](../Page/茎.md "wikilink")，有匍匐的或块状的[根莖](../Page/根莖.md "wikilink")，[叶基部具有叶鞘](../Page/叶.md "wikilink")；[花两性](../Page/花.md "wikilink")，两侧对称，[花瓣](../Page/花瓣.md "wikilink")3，下部合生成管；[果实为](../Page/果实.md "wikilink")[蒴果](../Page/蒴果.md "wikilink")，[种子常具有假种皮](../Page/种子.md "wikilink")。本科[植物很多種類是重要的](../Page/植物.md "wikilink")[調味料和](../Page/調味料.md "wikilink")[藥用植物](../Page/藥用植物.md "wikilink")。本科重要的成員有[薑](../Page/薑.md "wikilink")，[砂仁](../Page/砂仁.md "wikilink")、[薑黃](../Page/薑黃.md "wikilink")、[鬱金](../Page/鬱金.md "wikilink")、[蘘荷](../Page/蘘荷.md "wikilink")、[大荳蔻](../Page/大荳蔻.md "wikilink")、[小荳蔻](../Page/小荳蔻.md "wikilink")、[草荳蔻](../Page/草荳蔻.md "wikilink")、[紅荳蔻](../Page/紅荳蔻.md "wikilink")、[山奈](../Page/山奈.md "wikilink")、[月桃](../Page/月桃.md "wikilink")…等。

## 屬

  - 非洲豆蔻屬 *Aframomum*
      - [天堂椒](../Page/天堂椒.md "wikilink") （Aframomum melegueta）
  - [山薑屬](../Page/山薑屬.md "wikilink")（*Alpinia*），[月桃屬](../Page/月桃屬.md "wikilink")。
      - [距花山薑](../Page/距花山薑.md "wikilink")（Alpinia calcarata）
      - [大高良薑](../Page/大高良薑.md "wikilink")（Alpinia galanga）
      - [高良薑](../Page/高良薑.md "wikilink")（Alpinia officinarum）
      - [艷山薑](../Page/艷山薑.md "wikilink")（Alpinia
        zerumbet），[月桃](../Page/月桃.md "wikilink")。
      - [草荳蔻](../Page/草荳蔻.md "wikilink")（Alpinia katsumadai）
  - [豆蔻属](../Page/豆蔻属.md "wikilink")（*Amomum*）
      - [白豆蔻](../Page/白豆蔻.md "wikilink")（*Amomum cardamomum*）
      - [砂仁](../Page/砂仁.md "wikilink")（*Amomum villosum*）
      - [草果](../Page/草果.md "wikilink")（*Amomum tsao-ko*）
  - *Aulotandra*
  - [凹唇姜属](../Page/凹唇姜属.md "wikilink")（*Boesenbergia*）
  - *Burbigdea*
  - *Camptandra*
  - *Caulokaempferia*
  - [距药姜属](../Page/距药姜属.md "wikilink")（*Cautleya*）
  - [薑黃屬](../Page/薑黃屬.md "wikilink")（*Curcuma*），[鬱金屬](../Page/鬱金.md "wikilink")（日文），130多種。
      - [薑荷花](../Page/薑荷花.md "wikilink")（*Curcuma alsimatifolia*）
      - [郁金](../Page/郁金.md "wikilink")（*Curcuma aromatica*）
      - [广西莪术](../Page/广西莪术.md "wikilink")（*Curcuma kwangsiensis*）
      - [薑黃](../Page/薑黃.md "wikilink")（*Curcuma longa*）
      - [莪术](../Page/莪术.md "wikilink")（*Curcuma zedoaria*）
  - *Curcumorpha*
  - *Cyphostigma*
  - [小豆蔻屬](../Page/小豆蔻屬.md "wikilink")（*Elettaria*）
      - [小豆蔻](../Page/小豆蔻.md "wikilink")（*Elettaria caramomum*）
  - *Elettariopsis*
  - *Etlingera*
  - *Gagnepainia*
  - *Geocharis*
  - [舞花姜属](../Page/舞花姜属.md "wikilink")（*Globba*）
  - *Haniffia*
  - *Haplochorema*
  - [薑花屬](../Page/薑花屬.md "wikilink")（*Hedychium*），[蝴蝶薑屬](../Page/蝴蝶薑.md "wikilink")。
      - [野薑花](../Page/野薑花.md "wikilink")（*Hedychium coronarium*）
  - *Hemiorchis*
  - *Hitchenia*
  - [大豆蔻属](../Page/大豆蔻属.md "wikilink")（*Hornstedtia*）
  - [山柰屬](../Page/山柰屬.md "wikilink")（*Kaempferia*），[孔雀薑屬](../Page/孔雀薑.md "wikilink")。
      - [山柰](../Page/山柰.md "wikilink")（*Kaempferia galanga*）
  - *Leptosolena*
  - *Mantisia*
  - *Nanochilus*
  - *Paracautleya*
  - *Parakaempferia*
  - *Plagiostachys*
  - *Pleuranthodium*
  - [直唇姜属](../Page/直唇姜属.md "wikilink")（*Pommereschea*）
  - *Renealmia*
  - [喙花姜属](../Page/喙花姜属.md "wikilink")（*Rhynchanthus*）
  - *Riedelia*
  - [象牙参属](../Page/象牙参属.md "wikilink")（*Roscoea*）
  - *Scaphochlamys*
  - [长果姜属](../Page/长果姜属.md "wikilink")（*Siliquamomum*）
  - *Siphonochilus*
  - *Stadiochilus*
  - [土田七属](../Page/土田七属.md "wikilink")（*Stahlianthus*）
  - *Vanoverberghia*
  - [薑屬](../Page/薑屬.md "wikilink")（*Zingiber*）
      - [薑](../Page/薑.md "wikilink")（*Zingiber officinale*）
      - [蘘荷](../Page/蘘荷.md "wikilink")（*Zingiber mioga*）
      - [球薑](../Page/球薑.md "wikilink")（*Zingiber zerumbeta*）

## 外部連結

  - \[<http://libproject.hkbu.edu.hk/was40/search?lang=ch&channelid=1288&searchword=family_latin='Zingiberaceae>'
    薑科Zingiberaceae\] 藥用植物圖像數據庫 (香港浸會大學中醫藥學院)

[\*](../Category/薑科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")