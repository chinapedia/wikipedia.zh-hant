**GA文庫**是的[輕小說文庫系列](../Page/輕小說.md "wikilink")。2006年1月創刊。GA文庫官方網站並未說明「GA」是什麼的縮寫；但從廣告詞「超世代Adventure」以及作品募集中的文案\[1\]中推測，「G」為「*Generation*」的縮寫，「A」為「*Adventure*」的縮寫。\[2\]

GA文库出版了相當多原創作品。創刊一年來，主力作品只有《[神曲奏界](../Page/神曲奏界.md "wikilink")》系列是改編作品，直到2007年《[魔塔大陸](../Page/魔塔大陸.md "wikilink")》出版為止。此外它也曾再版其他出版社的作品。2008年2月成立新人賞「[GA文庫大賞](../Page/GA文庫大賞.md "wikilink")」及「GA文庫主題大賞」。

## 作品一覽

### 尖端出版

  - [下課後防衛隊－戰爭世代](../Page/下課後防衛隊－戰爭世代.md "wikilink")（[柿沼秀樹](../Page/柿沼秀樹.md "wikilink")／[放電映像](../Page/放電映像.md "wikilink")）
  - [高校女僕警察](../Page/高校女僕警察.md "wikilink")（[澤上水也](../Page/澤上水也.md "wikilink")／[西脇ゆぅり](../Page/西脇ゆぅり.md "wikilink")）
  - [神曲奏界](../Page/神曲奏界.md "wikilink")
      - 神曲奏界
        紅（[榊一郎](../Page/榊一郎.md "wikilink")／[神奈月昇](../Page/神奈月昇.md "wikilink")）
      - 神曲奏界
        黑（[大迫純一](../Page/大迫純一.md "wikilink")／[BUNBUN](../Page/BUNBUN.md "wikilink")）
      - 神曲奏界
        藍（[築地俊彥](../Page/築地俊彥.md "wikilink")／[兎塚エイジ](../Page/兎塚エイジ.md "wikilink")）
      - 神曲奏界
        白（[高殿円](../Page/高殿円.md "wikilink")／[きなこひろ](../Page/きなこひろ.md "wikilink")）
  - [菩提樹莊的闇狩姬─Nachtjager](../Page/菩提樹莊的闇狩姬─Nachtjager.md "wikilink")（[涼元悠一](../Page/涼元悠一.md "wikilink")／[一美](../Page/一美.md "wikilink")）
  - [汪汪地獄犬](../Page/汪汪地獄犬.md "wikilink")（[蕪木統文](../Page/蕪木統文.md "wikilink")／[狐印](../Page/狐印.md "wikilink")）
  - [女僕刑事](../Page/女僕刑事.md "wikilink")（[早見裕司](../Page/早見裕司.md "wikilink")／[はいむらきよたか](../Page/灰村キヨタカ.md "wikilink")）
  - [聲音×魔法](../Page/聲音×魔法.md "wikilink")（[白瀨修](../Page/白瀨修.md "wikilink")／[ヤス](../Page/ヤス.md "wikilink")）
  - [隔鄰的魔法師](../Page/隔鄰的魔法師.md "wikilink")（[篠崎砂美](../Page/篠崎砂美.md "wikilink")／[尾谷おさむ](../Page/尾谷おさむ.md "wikilink")）
  - [惡魔召喚使](../Page/惡魔召喚使.md "wikilink")（[中里融司](../Page/中里融司.md "wikilink")／[浦積理野](../Page/浦積理野.md "wikilink")）
  - [伊佐與雪](../Page/伊佐與雪.md "wikilink")（[友谷蒼](../Page/友谷蒼.md "wikilink")／[三日月かける](../Page/三日月かける.md "wikilink")）
  - [EX\!](../Page/EX!.md "wikilink")（[織田兄第](../Page/織田兄第.md "wikilink")／[うき](../Page/うき.md "wikilink")）
  - [同居五重奏！](../Page/同居五重奏！.md "wikilink")（[越後屋鐵舟](../Page/越後屋鐵舟.md "wikilink")／[せんむ](../Page/せんむ.md "wikilink")）
  - [淑女騎士團](../Page/淑女騎士團.md "wikilink")（[千田誠行](../Page/千田誠行.md "wikilink")／[しらゆき昭士郎](../Page/しらゆき昭士郎.md "wikilink")）
  - [快樂的輕小說寫作方式](../Page/快樂的輕小說寫作方式.md "wikilink")（[本田透](../Page/本田透.md "wikilink")／[桐野霞](../Page/桐野霞.md "wikilink")）
  - [閒狼作家是美少女妖怪?](../Page/閒狼作家是美少女妖怪?.md "wikilink")（[杉井光](../Page/杉井光.md "wikilink")／[赤人](../Page/赤人.md "wikilink")）
  - [我的紫苑](../Page/我的紫苑.md "wikilink")（[本田透](../Page/本田透.md "wikilink")／[百瀨壽](../Page/百瀨壽.md "wikilink")）
  - [襲來！美少女邪神](../Page/襲來！美少女邪神.md "wikilink")（[逢空萬太](../Page/逢空萬太.md "wikilink")／[狐印](../Page/狐印.md "wikilink")）
  - [妹妹x殺手x宅配便](../Page/妹妹x殺手x宅配便.md "wikilink")（[九邊健二](../Page/九邊健二.md "wikilink")／[鶴崎貴大](../Page/鶴崎貴大.md "wikilink")）
  - [深山家的蓓兒汀](../Page/深山家的蓓兒汀.md "wikilink")（逢空萬太／[七](../Page/七.md "wikilink")）
  - [我女友與青梅竹馬的慘烈修羅場](../Page/我女友與青梅竹馬的慘烈修羅場.md "wikilink")（[裕時悠示](../Page/裕時悠示.md "wikilink")／[るろお](../Page/るろお.md "wikilink")）
  - [農林](../Page/農林.md "wikilink")（[白鳥士郎](../Page/白鳥士郎.md "wikilink")／[切符](../Page/切符.md "wikilink")）
  - [舞星灑落的雷涅席庫爾](../Page/舞星灑落的雷涅席庫爾.md "wikilink")（[裕時悠示](../Page/裕時悠示.md "wikilink")／[たかやKi](../Page/たかやKi.md "wikilink")）
  - [你的侍奉只有這種程度嗎?](../Page/你的侍奉只有這種程度嗎?.md "wikilink")（[森田季節](../Page/森田季節.md "wikilink")／[尾崎弘宜](../Page/尾崎弘宜.md "wikilink")）
  - [BOY‧MEETS‧HEART
    \!](../Page/BOY‧MEETS‧HEART_!.md "wikilink")（[鳥羽徹](../Page/鳥羽徹.md "wikilink")／[H2SO4](../Page/H2SO4.md "wikilink")）
  - [雙胞胎與青梅竹馬的四人命案](../Page/雙胞胎與青梅竹馬的四人命案.md "wikilink")（[森田陽一](../Page/森田陽一.md "wikilink")／[saitom](../Page/saitom.md "wikilink")）
  - [也許是現在進行式的黑歷史](../Page/也許是現在進行式的黑歷史.md "wikilink")（[あわむら赤光](../Page/あわむら赤光.md "wikilink")／[refeia](../Page/refeia.md "wikilink")）
  - [落第騎士英雄譚](../Page/落第騎士英雄譚.md "wikilink")（[海空陸](../Page/海空陸.md "wikilink")／[WON](../Page/WON.md "wikilink")）
  - [日常生活中的異能戰鬥](../Page/日常生活中的異能戰鬥.md "wikilink")（[望公太](../Page/望公太.md "wikilink")／[029](../Page/029.md "wikilink")）
  - [那麼，來攻略異世界吧](../Page/那麼，來攻略異世界吧.md "wikilink")（[おかざき登](../Page/おかざき登.md "wikilink")／[Peco](../Page/Peco.md "wikilink")）

### 傑克魔豆

  - [歡迎光臨！廢墟飯店](../Page/歡迎光臨！廢墟飯店.md "wikilink")（[松殿理央](../Page/松殿理央.md "wikilink")／[がんぽん](../Page/がんぽん.md "wikilink")）

### 青文出版社

  - [額頭輕觸](../Page/額頭輕觸.md "wikilink")（[野島けんじ](../Page/野島けんじ.md "wikilink")／[しゅがーピコラ](../Page/しゅがーピコラ.md "wikilink")）
  - [月見月理解的偵探殺人](../Page/月見月理解的偵探殺人.md "wikilink")（[明月千里](../Page/明月千里.md "wikilink")／[mebae](../Page/mebae.md "wikilink")）
  - [織田信奈的野望](../Page/織田信奈的野望.md "wikilink")（[春日みかげ](../Page/春日みかげ.md "wikilink")／[みやま零](../Page/みやま零.md "wikilink")）

### 東立出版社

  - [let's
    party\!來組隊吧！](../Page/let's_party!來組隊吧！.md "wikilink")（[小金井ゴル](../Page/小金井ゴル.md "wikilink")／[鍋島テツヒロ](../Page/鍋島テツヒロ.md "wikilink")）
  - [龍王的工作！](../Page/龍王的工作！.md "wikilink")（[白鳥士郎](../Page/白鳥士郎.md "wikilink")
    / しらび）

### 测绘出版社

  - [潜行吧！奈亚子](../Page/襲來！美少女邪神.md "wikilink")（[逢空万太](../Page/逢空万太.md "wikilink")／[狐印](../Page/狐印.md "wikilink")）

### 安徽少年儿童出版社

  - [织田信奈的野望](../Page/织田信奈的野望.md "wikilink")（[春日御影](../Page/春日御影.md "wikilink")／[美山零](../Page/美山零.md "wikilink")）

### 新星出版社

  - [断罪的EXCEED](../Page/断罪的EXCEED.md "wikilink")（[海空陆](../Page/海空陆.md "wikilink")／[纯珪一](../Page/纯珪一.md "wikilink")）

### 小多文化

  - [我女友和青梅竹马的各种修罗场](../Page/我女友和青梅竹马的各种修罗场.md "wikilink")（裕时悠示／[LLO](../Page/LLO.md "wikilink")）
  - 舞星散落的雷涅西库尔（裕时悠示／TakayaKi）

## 註解

<references />

## 外部連結

  - [GA文庫](http://ga.sbcr.jp/novel/)

[\*](../Category/GA文庫.md "wikilink")

1.  [GAでライトノベルレーベル刊行決定\!
    小説&イラストを大募集\!\!](http://ga.sbcr.jp/novel/information/)
2.  與設立出版社的情報站《[GA
    Graphic](../Page/GA_Graphic.md "wikilink")》中的「GA」意義相同與否，則不明。