**金勝友**（，），[韓國](../Page/韓國.md "wikilink")[男演員](../Page/男演員.md "wikilink")。前任妻子是[李美妍](../Page/李美妍.md "wikilink")，現任妻子為[金南珠](../Page/金南珠_\(1971年\).md "wikilink")（2005年5月25日結婚），育有1子1女。

## 演出作品

### 電視劇

  - 1995年：[MBC](../Page/文化廣播_\(韓國\).md "wikilink")《[戀愛的基礎](../Page/戀愛的基礎.md "wikilink")》飾演
    韓秀
  - 1996年：MBC《[他們的擁抱](../Page/他們的擁抱.md "wikilink")》飾演 柳貞仁
  - 1996年：MBC《[蘋果花飄香](../Page/蘋果花飄香.md "wikilink")》飾演 崔正延
  - 1997年：MBC《[灰姑娘](../Page/灰姑娘.md "wikilink")》飾演 徐俊熙
  - 1998年：MBC《[回憶](../Page/回憶.md "wikilink")》飾演 韓正浩
  - 2000年：MBC《[新貴公子](../Page/新貴公子.md "wikilink")》飾演 金雍男
  - 2001年：MBC《[情定大飯店](../Page/情定大飯店.md "wikilink")》飾演 韓泰俊
  - 2003年：[KBS](../Page/韓國放送公社.md "wikilink")《[迷迭香](../Page/迷迭香_\(電視劇\).md "wikilink")》飾演
    崔英濤
  - 2006年：MBC《[我人生的Special](../Page/我人生的Special.md "wikilink")》飾演 朴江浩
  - 2007年：[SBS](../Page/SBS_\(韓國\).md "wikilink")《[遇上完美鄰居的方法](../Page/遇上完美鄰居的方法.md "wikilink")》飾演
    白秀燦
  - 2009年：MBC《[賢內助女王](../Page/賢內助女王.md "wikilink")》飾演 警察（客串）
  - 2009年：KBS《[IRIS](../Page/IRIS_\(電視劇\).md "wikilink")》飾演 朴哲榮
  - 2010年：MBC《[逆轉女王](../Page/逆轉女王.md "wikilink")》飾演 保安（客串）
  - 2010年：SBS《[雅典娜：戰爭女神](../Page/雅典娜：戰爭女神.md "wikilink")》飾演 朴哲榮
  - 2011年：MBC《[Miss Ripley](../Page/Miss_Ripley.md "wikilink")》飾演 張明勳
  - 2012年：KBS《[順藤而上的你](../Page/順藤而上的你.md "wikilink")》飾演 考生
  - 2012年：[tvN](../Page/tvN.md "wikilink")《[第三醫院](../Page/第三醫院.md "wikilink")》飾演
    金斗賢
  - 2013年：KBS《[IRIS 2](../Page/IRIS_2.md "wikilink")》飾演 朴哲榮
  - 2015年：SBS《[深夜食堂](../Page/深夜食堂_\(韓國電視劇\).md "wikilink")》飾演 老闆

### 電影

  - 1990年：《[將軍的兒子](../Page/將軍的兒子.md "wikilink")》
  - 1990年：《[小組頭癟三](../Page/小組頭癟三.md "wikilink")》
  - 1991年：《[年青時節的肖像](../Page/年青時節的肖像.md "wikilink")》
  - 1991年：《[將軍的兒子2](../Page/將軍的兒子2.md "wikilink")》
  - 1992年：《[不能離婚的女子](../Page/不能離婚的女子.md "wikilink")》
  - 1992年：《[將軍的兒子3](../Page/將軍的兒子3.md "wikilink")》
  - 1993年：《[101次求婚](../Page/101次求婚\(韓國電影\).md "wikilink")》
  - 1994年：《[結婚](../Page/結婚_\(電影\).md "wikilink")》
  - 1995年：《[狗一樣生活的午後](../Page/狗一樣生活的午後.md "wikilink")》
  - 1995年：《[捲款逃吧](../Page/捲款逃吧.md "wikilink")》
  - 1996年：《[內衣秀](../Page/內衣秀.md "wikilink")》
  - 1996年：《[你們相信命嗎？](../Page/你們相信命嗎？.md "wikilink")》
  - 1996年：《[鬼媽媽](../Page/鬼媽媽_\(韓國電影\).md "wikilink")》
  - 1997年：《[拿著花的男人](../Page/拿著花的男人.md "wikilink")》
  - 1997年：《[深度憂傷](../Page/深度憂傷.md "wikilink")》
  - 1997年：《[愛上姐姐的朋友](../Page/愛上姐姐的朋友.md "wikilink")》
  - 1998年：《[男人的香氣](../Page/男人的香氣.md "wikilink")》
  - 1999年：《[新裝開業](../Page/新裝開業.md "wikilink")》
  - 2000年：《[秘密](../Page/秘密_\(電影\).md "wikilink")》
  - 2002年：《[Yesterday](../Page/Yesterday_\(韓國電影\).md "wikilink")》
  - 2002年：《[撻著火機](../Page/撻著火機.md "wikilink")》
  - 2003年：《[春風](../Page/春風.md "wikilink")》
  - 2003年：《[命運的逆轉](../Page/命運的逆轉.md "wikilink")》
  - 2003年：《[黃山伐](../Page/黃山伐.md "wikilink")》
  - 2003年：《[偷歡嫁期](../Page/偷歡嫁期.md "wikilink")》
  - 2005年：《[天軍](../Page/天軍_\(韓國電影\).md "wikilink")》
  - 2006年：《[海邊的女人](../Page/海邊的女人.md "wikilink")》
  - 2006年：《[愛，不是忍受](../Page/愛，不是忍受.md "wikilink")》
  - 2007年：《[Curling Love](../Page/Curling_Love.md "wikilink")》
  - 2009年：《[肚臍](../Page/肚臍_\(電影\).md "wikilink")》
  - 2010年：《[走進炮火中](../Page/走進炮火中.md "wikilink")》
  - 2010年：《[IRIS](../Page/IRIS_\(電影\).md "wikilink")》
  - 2011年：《[我是爸爸](../Page/我是爸爸.md "wikilink")》
  - 2013年：《[臍](../Page/臍_\(電影\).md "wikilink")》
  - 2015年：《[抓住才能活](../Page/抓住才能活.md "wikilink")》
  - 2016年：《[第二個二十](../Page/第二個二十.md "wikilink")》

### MV

  - 1998年：[曹誠模](../Page/曹成模.md "wikilink")《不朽的愛情》【[李炳憲](../Page/李炳憲.md "wikilink")、[黃秀貞](../Page/黃秀貞.md "wikilink")、[金諪恩](../Page/金諪恩.md "wikilink")】
  - 2001年：朱永勳《願望》【[李恩宙](../Page/李恩宙.md "wikilink")】
  - 2008年：KCM《Classic》【[金素妍](../Page/金素妍.md "wikilink")】
  - 2008年：KCM《一天日記》+《激動》【[金素妍](../Page/金素妍.md "wikilink")】
  - 2008年：KCM《因爲愛》【[金素妍](../Page/金素妍.md "wikilink")】
  - 2009年：[December](../Page/December.md "wikilink")《女人喜歡壞男人》【[李珉廷](../Page/李珉廷.md "wikilink")】

## 綜藝節目

  - 2010年2月2日－2013年1月15日：KBS《[乘勝長驅](../Page/乘勝長驅.md "wikilink")》MC之一
  - 2012年3月4日－2013年3月31日：KBS《[兩天一夜](../Page/兩天一夜.md "wikilink")》MC之一

## 獲獎

  - 1998年：[MBC演技大賞](../Page/MBC演技大賞.md "wikilink")－最優秀演技獎
  - 2003年：[KBS演技大賞](../Page/KBS演技大賞.md "wikilink")－優秀演技獎、人氣獎、最佳情侶獎
  - 2006年：第14屆[春史電影賞](../Page/春史電影賞.md "wikilink")－人氣獎
  - 2009年：KBS演技大賞－中篇電視劇部門 男子優秀演技獎
  - 2010年：[KBS演藝大賞](../Page/KBS演藝大賞.md "wikilink")－Show娛樂部門 男新人獎
  - 2011年：KBS演藝大賞－Show娛樂部門 男子優秀獎
  - 2012年：KBS演藝大賞－Show娛樂部門 男子最優秀獎（乘勝長驅、Happy Sunday-兩天一夜）

## 外部連結

  - [EPG](https://web.archive.org/web/20070830195935/http://epg.epg.co.kr/Star/Profile/index.asp?actor_id=630)

  - [Yahoo
    Korea](http://kr.search.yahoo.com/search?p=%EA%B9%80%EC%8A%B9%EC%9A%B0&ret=1&fr=kr-search_top&x=34&y=12)


[K](../Category/韓國電視演員.md "wikilink")
[K](../Category/韓國電影演員.md "wikilink")
[K](../Category/水原大學校友.md "wikilink")
[K](../Category/首爾特別市出身人物.md "wikilink")
[K](../Category/金姓.md "wikilink")