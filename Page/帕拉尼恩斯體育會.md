**帕拉尼恩斯體育會**（**Clube Atlético
Paranaense**），是一間位於[巴西](../Page/巴西.md "wikilink")[巴拉那州](../Page/巴拉那州.md "wikilink")[庫里奇巴的球會](../Page/庫里奇巴.md "wikilink")，成立於1924年3月26日。

## 歷史

帕拉尼恩斯由兩支位於庫里奇巴的傳統球會組成。這個合併於1924年3月21日發出，5天後完成。球會換上新的球衣和名稱，還有新的董事會，而球會當時就由Água
Verde擔任董事

球會第一場比賽於4月6日進行，而第一場正式競逐性的比賽則於4月20日進行，當時球會以2-0擊敗[哥列迪巴](../Page/哥列迪巴.md "wikilink")。經常於比賽亮相，令球會累積不少經驗，令球會早於1925年就首次奪得州聯賽冠軍，奠定了球會未來的地位。在1934年，球會正式使用[拜沙達體育場作為主場館](../Page/拜沙達體育場.md "wikilink")。

在1949年，球會贏得第九次的州聯賽冠軍，令球會得到Furacão（颶風）這個暱稱。

在1995年，球會以1-5大敗給哥列迪巴後，新的董事會接替了舊的，並實施了一個新的策略叫“Atlético Total”。

在2001年，球會擊敗[聖卡坦奴後](../Page/聖卡坦奴.md "wikilink")，首次贏得[巴西足球甲級聯賽冠軍](../Page/巴西足球甲級聯賽.md "wikilink")。2004年，他們再贏得亞軍。而射手Washington以射入34球，打破了[巴西足球甲級聯賽改制後的紀錄](../Page/巴西足球甲級聯賽.md "wikilink")。

而球會也三次參加了[南美自由杯](../Page/南美自由杯.md "wikilink")，分別在2000年、2002年和2005年。2000年在次圈被[明尼路淘汰](../Page/明尼路.md "wikilink")，2005年在決賽被[聖保羅擊敗而得亞軍](../Page/聖保羅足球會.md "wikilink")。

## 成就

### 國內

  - *州聯賽冠軍*(21): 1925, 1929, 1930, 1934, 1936, 1940, 1943, 1945, 1949,
    1958, 1970, 1982, 1983, 1985, 1988, 1990, 1998, 2000, 2001, 2002,
    2005.
      - [巴西足球甲級聯賽冠軍](../Page/巴西足球甲級聯賽.md "wikilink")(1): 2001
      - [巴西足球甲級聯賽亞軍](../Page/巴西足球甲級聯賽.md "wikilink")(1): 2004
      - [巴西足球乙級聯賽冠軍](../Page/巴西足球乙級聯賽.md "wikilink")(1): 1995

### 國際

  - [南美球會盃冠軍](../Page/南美球會盃.md "wikilink")(1): 2018
  - [南美自由盃亞軍](../Page/南美自由盃.md "wikilink")(1): 2005

## 球場

球會的主場位於[拜沙達體育場](../Page/拜沙達體育場.md "wikilink")，能容納32,864人。

## 著名球員

  - Alfredo Gottardi Júnior
  - [Alex Mineiro](../Page/Alex_Mineiro.md "wikilink")
  - Assis
  - Bellini
  - Caju
  - [Cocito](../Page/Cocito.md "wikilink")
  - [Djalma Santos](../Page/Djalma_Santos.md "wikilink")
  - Jackson do Nascimento
  - Lucas
  - Nilson Borges
  - Oséas
  - [Paulo Rink](../Page/Paulo_Rink.md "wikilink")
  - Sicupira
  - Washington
  - [-{zh-hans:克勒贝尔森;zh-hk:基巴臣;zh-tw:克勒貝爾森;}-](../Page/克勒贝尔森.md "wikilink")（Kleberson）
  - [Jadson](../Page/Jadson.md "wikilink")
  - Dagoberto

## 外部連結

  - [官方網站](http://www.atleticopr.com.br/)
  - [非官方網站](http://www.furacao.com/)

[Category:巴西足球俱樂部](../Category/巴西足球俱樂部.md "wikilink")
[Category:1924年建立的足球俱樂部](../Category/1924年建立的足球俱樂部.md "wikilink")