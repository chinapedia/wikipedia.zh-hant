**羅伯特·伯恩斯**（，）是著名[苏格兰](../Page/苏格兰.md "wikilink")[詩人](../Page/詩人.md "wikilink")。

羅伯特·伯恩斯从小熟悉[苏格兰民谣和古老传说](../Page/苏格兰民谣.md "wikilink")，并曾搜集、整理[民歌](../Page/民歌.md "wikilink")，主要用[苏格兰语写作](../Page/低地苏格兰语.md "wikilink")，所作诗歌受民歌影响，通俗流畅，便于吟唱，在民间广为流传，被認為是蘇格蘭的[民族詩人](../Page/民族.md "wikilink")。

羅伯特·伯恩斯被視為[浪漫主義運動的先驅](../Page/浪漫主義.md "wikilink")，在死後成為[自由主義和](../Page/自由主義.md "wikilink")[社會主義的靈感來源](../Page/社會主義.md "wikilink")，也是蘇格蘭和散居在世界各地蘇格蘭人的一個[文化偶像](../Page/魅力型權威.md "wikilink")。慶祝羅伯特·伯恩斯的活動在19世紀和20世紀幾乎風靡全蘇格蘭，他對[蘇格蘭文學影響深遠](../Page/蘇格蘭文學.md "wikilink")。2009年，羅伯特·伯恩斯獲[蘇格蘭](../Page/蘇格蘭.md "wikilink")[電視台](../Page/電視台.md "wikilink")[STV選為歷史上最偉大的蘇格蘭人](../Page/STV.md "wikilink")\[1\]\[2\]。

## 生平

### 早期

[The_burns_family.JPG](https://zh.wikipedia.org/wiki/File:The_burns_family.JPG "fig:The_burns_family.JPG")

羅伯特·伯恩斯出生於[蘇格蘭艾爾以南](../Page/蘇格蘭.md "wikilink")2英里（3公里）的[艾爾郡](../Page/艾爾郡.md "wikilink")，是[威廉·伯恩斯](../Page/威廉·伯恩斯.md "wikilink")（1721年至1784年）的七個孩子中的長子，他是一位[佃農](../Page/佃農.md "wikilink")，妻子則是艾格尼絲·布恩（1732年至1820年），為艾爾郡佃農的女兒\[3\]\[4\]
。

他出生在父親所搭建的房子（現在為伯恩斯農舍博物館）直到1766年。他七歲時，威廉·伯恩斯賣掉房子，買下[奧利芬特山](../Page/奧利芬特山.md "wikilink")[農場](../Page/農場.md "wikilink")70英畝（280,000[平方公尺](../Page/平方公尺.md "wikilink")）土地，伯恩斯後來在這裡長大。

羅伯特·伯恩斯獲得很少正規學校[教育](../Page/教育.md "wikilink")，他的父親教授孩子們的[閱讀](../Page/閱讀.md "wikilink")、寫作、[算術](../Page/算術.md "wikilink")、[地理](../Page/地理.md "wikilink")、[歷史](../Page/歷史.md "wikilink")，還為他們寫一本[基督教信仰手冊](../Page/基督教.md "wikilink")。羅伯特和[弟弟吉爾伯特](../Page/弟弟.md "wikilink")（1760年至1827年）於1765年至1768年之間在一間學校，學習[拉丁語](../Page/拉丁語.md "wikilink")、[法語](../Page/法語.md "wikilink")、[數學](../Page/數學.md "wikilink")。經過了幾年的家庭教育，羅伯特·伯恩斯被送往達爾林普爾教區學校，直到1773年才回到家鄉。

羅伯特·伯恩斯15歲時在奧利芬特山勞動。在1774年，[莉基爾·帕特里克](../Page/莉基爾·帕特里克.md "wikilink")（1759年至1820年）啟發了他在詩歌上的興趣。

威廉·伯恩斯在1777年從奧利芬特山舉家搬到Lochlea農場，羅伯特·伯恩斯居住在此直到威廉·伯恩斯在1784年去世為止。1781年12月，伯恩斯暫時搬到北艾爾郡[爾灣](../Page/爾灣.md "wikilink")，試圖成為[亞麻工人](../Page/亞麻.md "wikilink")，然而在1782年新年，亞麻工廠卻起火燒毀。羅伯特·伯恩斯回到家鄉後，結識了[理查德·布朗](../Page/理查德·布朗.md "wikilink")[船長](../Page/船長.md "wikilink")，他鼓勵羅伯特·伯恩斯成為一個詩人。

他繼續寫詩歌和歌曲，並在1783年開始寫[書](../Page/書.md "wikilink")，而當時他的父親與房東發生法律糾紛。案件後來進入[蘇格蘭高等民事法院](../Page/蘇格蘭高等民事法院.md "wikilink")（Court
of Session）審理。

羅伯特·伯恩斯和[弟弟羅伯特](../Page/弟弟.md "wikilink")·吉爾伯特後來搬到了[莫克林附近的Mossgiel](../Page/莫克林.md "wikilink")。羅伯特·伯恩斯在當地與Jean
Armour發生親密關係。

### 戀愛

[Jeanarmour.jpg](https://zh.wikipedia.org/wiki/File:Jeanarmour.jpg "fig:Jeanarmour.jpg")

伊麗莎白·佩頓·伯恩斯（1785年至1817年）是羅伯特·伯恩斯的第一個孩子，母親是[伊麗莎白·佩頓](../Page/伊麗莎白·佩頓.md "wikilink")（1760年至約1799年），伊麗莎白·佩頓於1786年3月懷上了[雙胞胎](../Page/雙胞胎.md "wikilink")。伯恩斯簽署了一紙證明他與Jean
Armour的婚姻關係，於是她的父母把伊麗莎白·伯恩斯送到住在[佩斯利的叔叔家](../Page/佩斯利.md "wikilink")。雖然Armour的父親反對，他們最終還是在1788年結婚\[5\]。Jean
Armour為他生了九個孩子，只有其中三名倖存下來。

為了養家活口，羅伯特·伯恩斯在[牙買加工作](../Page/牙買加.md "wikilink")，薪水每年30[英鎊](../Page/英鎊.md "wikilink")\[6\]\[7\]。伯恩斯擔任[奴隸種植園簿記員](../Page/奴隸.md "wikilink")。在1786年，很少獲得公眾注意的[廢奴運動大約在那個時候開始](../Page/廢奴運動.md "wikilink")\[8\]\[9\]。

大約在同一時間，羅伯特·伯恩斯與[瑪麗·坎貝爾](../Page/瑪麗·坎貝爾.md "wikilink")（1763年至1786年）陷入熱戀，他曾在[教堂裡遇見她](../Page/教堂.md "wikilink")。1786年5月14日，他們交換了“聖經”、[宣誓成為夫妻](../Page/宣誓.md "wikilink")。此後不久，瑪麗·坎貝爾離開艾爾郡，從[摩海港乘船返回了父母在坎貝爾鎮的家](../Page/摩海港.md "wikilink")\[10\]\[11\]。

1786年10月，瑪麗·坎貝爾和她的父親從坎貝爾至格里諾克看望她的哥哥。她的哥哥罹患[斑疹傷寒](../Page/斑疹傷寒.md "wikilink")，她也因此被感染。1786年10月21日（或20日），她死於斑疹傷寒，埋葬於當地。

### 基爾馬諾克

[Robert_Burns_statue,_Bernard_Street.jpg](https://zh.wikipedia.org/wiki/File:Robert_Burns_statue,_Bernard_Street.jpg "fig:Robert_Burns_statue,_Bernard_Street.jpg")

因為羅伯特·伯恩斯沒有足夠的資金來支付旅費，前往[西印度群島](../Page/西印度群島.md "wikilink")，[加文·漢密爾頓認為羅伯特](../Page/加文·漢密爾頓.md "wikilink")·伯恩斯應該平均發表他的詩，可以從中獲得一點金錢，提供他更寬裕的生活。4月3日，羅伯特·伯恩斯發送他的蘇格蘭詩歌給基爾馬諾克一個本地出版商約翰·威爾遜，建議在1786年4月發表，同一天，Jean
Armour的父親撕毁伯恩斯的婚姻證書。7月22日，他轉移了在Mossgiel農場的份額給弟弟吉爾伯特。7月30日，羅伯特·伯恩斯寫信告訴他的朋友約翰·里士滿說，「Jean
Armour把我扔在[監獄裡](../Page/監獄.md "wikilink")，直到我能獲得巨富。我從一個朋友家流浪到另一個朋友家。」

1786年7月31日，約翰·威爾遜發表羅伯特·伯恩斯的詩，主要使用蘇格蘭[方言](../Page/方言.md "wikilink")。詩集銷售情形很好，羅伯特·伯恩斯很快就舉國聞名。

9月1日，羅伯特·伯恩斯推遲了他的牙買加移民計劃，兩天後他得知Jean
Armour生下雙胞胎。苏格兰詩人[托馬斯·布萊克洛克於](../Page/托馬斯·布萊克洛克.md "wikilink")9月4日寫了一封信，表示欽佩羅伯特·伯恩斯的詩歌，並建議擴充第二版\[12\]
。

### 愛丁堡

1786年11月27日，羅伯特·伯恩斯租借了小馬，前往[愛丁堡](../Page/愛丁堡.md "wikilink")。12月14日，威廉·克里奇買下《Poems,
Chiefly in the Scottish
dialect》詩集版權，該詩集主要刊登於1787年4月17日，獲得100[基尼](../Page/基尼.md "wikilink").\[13\]。在愛丁堡，他受到許多人邀請，也參加[貴族聚會](../Page/貴族.md "wikilink")。他在這裡遇到16歲的[沃尔特·司各特](../Page/沃尔特·司各特.md "wikilink")，並留下深刻印象。

羅伯特·伯恩斯留在這個城市，發展出一些終身友誼，其中包含[格倫·凱恩勳爵和](../Page/格倫·凱恩勳爵.md "wikilink")[弗朗西絲·安娜·鄧洛普](../Page/弗朗西絲·安娜·鄧洛普.md "wikilink")（1730年至1815年），弗朗西絲·安娜·鄧洛普成為他的贊助商多年。他與Agnes
Maclehose（1758年至1841年）、南希的僕人[珍妮·克洛](../Page/珍妮·克洛.md "wikilink")（1766至1792年）等人發展出親密關係，珍妮·克洛在1788年為他生了一個兒子羅伯特·伯恩斯·克洛。他也與女僕[瑪格麗特·卡梅隆有染](../Page/瑪格麗特·卡梅隆.md "wikilink")。

1787年初，羅伯特·伯恩斯在愛丁堡會見了詹姆斯·約翰遜，他是一個喜愛老蘇格蘭人歌曲的銷售員。羅伯特·伯恩斯完成的作品是《蘇格蘭音樂博物館》。《蘇格蘭音樂博物館》第一冊於1787年出版，包括三首羅伯特·伯恩斯歌曲。第二冊他貢獻了40首歌曲，最終冊於1803年出版。

### 鄧弗里斯

[Robert_Burns'_House,_Dumfries.jpg](https://zh.wikipedia.org/wiki/File:Robert_Burns'_House,_Dumfries.jpg "fig:Robert_Burns'_House,_Dumfries.jpg")

[Ellisland_Farm,_Dumfries,_c_1900.jpg](https://zh.wikipedia.org/wiki/File:Ellisland_Farm,_Dumfries,_c_1900.jpg "fig:Ellisland_Farm,_Dumfries,_c_1900.jpg")

1788年2月18日，羅伯特·伯恩斯返回[艾爾郡](../Page/艾爾郡.md "wikilink")，恢復他與Jean
Armour的關係，並租下鄧弗里斯附近的艾利斯蘭農場（定居於6月11日）。他在1789年被任命為海關人員，並在1791年放棄經營農場。同時在1790年11月，他拒絕擔任倫敦《星報》的工作人員，並拒絕成為[愛丁堡大學農業專家](../Page/愛丁堡大學.md "wikilink"),\[14\]。

羅伯特·伯恩斯放棄他的農場後，搬到鄧弗里斯。正是在這個時候，羅伯特·伯恩斯被要求撰寫蘇格蘭歌詞，之後貢獻了超過100首歌曲。他對[喬治·湯姆森撰寫的蘇格蘭歌曲合集作出了重大貢獻](../Page/喬治·湯姆森.md "wikilink")。

羅伯特·伯恩斯還致力於收集和保存蘇格蘭民歌，有時也加以修改內容。

### 逝世

[Death_room_robert_burns.jpg](https://zh.wikipedia.org/wiki/File:Death_room_robert_burns.jpg "fig:Death_room_robert_burns.jpg")

羅伯特·伯恩斯於1795年3月加入了皇家鄧弗里斯志願軍\[15\]。由於他的健康狀況不佳，他開始過早衰老，心情因此沮喪。放縱的生活習慣加劇了他的風濕性心臟病情\[16\]。他在1795年冬天[拔牙後去世](../Page/拔牙.md "wikilink")。

1796年7月21日上午，37歲的羅伯特·伯恩斯於鄧弗里斯逝世。葬禮於1796年7月25日舉行，他的兒子麥克斯韋出生於當天。他首先葬於鄧弗里斯偏僻角落的聖邁克爾墓地，他的墓碑為簡單的砂石板\[17\]。他的屍體在1815年9月最終被轉移到伯恩斯墓地。他的遺孀Jean
Armour在1834年葬在此地.\[18\]。

Jean Armour採取步驟，以確保他的個人財產，金額為15英鎊.\[19\]
。他的家人於1798年出版他的全集\[20\]。1787年6月4日，他死亡9年前，羅伯特·伯恩斯被追授為鄧弗里斯鎮[榮譽市民](../Page/榮譽市民.md "wikilink")\[21\]。

羅伯特·伯恩斯育有十二個孩子，擁有超過600名後裔\[22\]。

## 作品

羅伯特·伯恩斯創作的著名诗篇包含《自由树》歌颂[法国大革命](../Page/法国大革命.md "wikilink")；《苏格兰人》歌颂反抗英国侵略的民族英雄，号召人民争取自由；《[友誼萬歲](../Page/友誼萬歲.md "wikilink")》（Auld
Lang Syne）；《[羊肚膾頌](../Page/羊肚膾頌.md "wikilink")》（Address To a Haggis）等。

羅伯特·伯恩斯創作的著名抒情诗有《一朵朵红红的玫瑰》、《高原玛丽》、《往昔时光》等。

## 參考資料

## 文獻

  -
  -
  - ([p.57](http://www.gutenberg.org/catalog/world/readfile?fk_files=89429&pageno=57))

  - Dietrich Hohmann: *Ich, Robert Burns*, Biographical Novel, Neues
    Leben, Berlin 1990 (in German)

## 外部連結

  - [Modern English translations of poems by Robert
    Burns](http://www.thehypertexts.com/Robert%20Burns%20Translations%20Modern%20English.htm)

  - [site of Burns Cottage
    Museum](http://www.burnsmuseum.org.uk/Official)

  -
  - [National Library of Scotland's Burns
    site](https://web.archive.org/web/20100604015038/http://www.nls.uk/burns/index.htm)

  -
  - [Legacy of Robert Burns – National Archives of Scotland
    website](http://www.nas.gov.uk/about/090717.asp)

  - [A recital of Tam O'Shanter a
    Tale](http://soundcloud.com/kevin-brown-50/tam-oshanter-robert-burns)

  - [A recital Address to the Unco
    Guid](http://soundcloud.com/kevin-brown-50/address-to-the-unco-guid-or-1?utm_campaign=timeline&utm_content=http%3A%2F%2Fsoundcloud.com%2Fkevin-brown-50%2Faddress-to-the-unco-guid-or-1&utm_medium=facebook&utm_source=soundcloud)

  - [A recital of Address to a
    Haggis](http://soundcloud.com/kevin-brown-50/address-to-a-haggis-robert)

  - [Video recital of Tam o'
    Shanter](http://www.youtube.com/watch?v=fP_aFSuLeT0)

  - [Video recital Address to the Unco Guid or the rigidly
    righteous](http://www.youtube.com/watch?v=afOAbMV1rNU)

  - [A Recital Holly Willie's Prayer inclding Epitaph on Holly
    Willie](http://soundcloud.com/kevin-brown-50/holly-willies-prayer-epitaph)

[Category:共濟會會員](../Category/共濟會會員.md "wikilink")
[Category:蘇格蘭文學](../Category/蘇格蘭文學.md "wikilink")
[Category:啟蒙運動](../Category/啟蒙運動.md "wikilink")
[Category:浪漫主義詩人](../Category/浪漫主義詩人.md "wikilink")
[Category:蘇格蘭詩人](../Category/蘇格蘭詩人.md "wikilink")
[Category:蘇格蘭作詞家](../Category/蘇格蘭作詞家.md "wikilink")
[Category:南艾爾郡人](../Category/南艾爾郡人.md "wikilink")

1.  [Robert Burns voted Greatest
    Scot](http://scotland.stv.tv/greatest-scot/) 2013年10月25日查閱

2.  [The Greatest
    Scot](http://news.stv.tv/scotland/141018-robert-burns-voted-greatest-scot/)
    2013年10月25日查閱

3.

4.

5.

6.

7.

8.

9.

10.
11.
12.

13.
14. Robert Burns: "[Poetry – Poems –
    Poets](http://www.zeugma.co.uk/RobertBurnsScottishPoet.htm) ."
    2013年10月25日查閱

15. [Robert Burns Birthplace Museum - MS: 'The Dumfries
    Volunteers'](http://www.burnsmuseum.org.uk/collections/object_detail/3.6201)

16. Hogg, PS (2008). *Robert Burns. The Patriot Bard*. Edinburgh :
    Mainstream Publishing. ISBN 978-1-84596-412-2. p. 321.

17. <https://sites.google.com/site/joerocksresearchpages/thomas-hamilton-architect>

18.
19.

20.

21. Hogg, PS (2008). *Robert Burns. The Patriot Bard*. Edinburgh :
    Mainstream Publishing. ISBN 978-1-84596-412-2. p. 154.

22. [George Burns descendents family tree, living descendents,
    etc.](http://www.burness.ca/p133.htm#i1323)