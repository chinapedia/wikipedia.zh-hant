**微机电系统**（，縮寫為
MEMS）是将[微电子技术与](../Page/微电子技术.md "wikilink")[机械工程融合到一起的一种工业技术](../Page/机械工程.md "wikilink")，它的操作范围在[微米范围内](../Page/微米.md "wikilink")。比它更小的，在[纳米范围的类似技术被称为](../Page/纳米.md "wikilink")[纳机电系统](../Page/纳机电系统.md "wikilink")（nanoelectromechanical
systems，NEMS）。微机电系统在日本被称作**微机械**（micromachines），在欧洲被称作**微系统技術**（Micro
Systems Technology，MST）。

微机电系统与或的超前概念不同。微机电系统由尺寸为1至100微米（0.001至0.1毫米)的部件组成，而且微机电设备的尺寸通常在20微米到一毫米之间。它们内部通常包含一个微处理器和若干获取外界信息的微型传感器。\[1\]在这种尺寸范围下，[经典物理基本定律通常不适用](../Page/经典物理.md "wikilink")。而且由于微机电系统相当大的表面积/体积比，诸如[静电和](../Page/静电.md "wikilink")[浸润等表面效应要比惯性和比热等体效应大很多](../Page/浸润.md "wikilink")。

微机电系统的加工技术由[半导体加工技术改造而来](../Page/半导体器件制造.md "wikilink")，使其可以应用到实际当中，而后者一般用来制造[电子设备](../Page/电子设备.md "wikilink")。其加工方式包含了molding
and
plating，[湿法刻蚀](../Page/刻蝕.md "wikilink")（[氢氧化钾](../Page/氢氧化钾.md "wikilink")，[四甲基氢氧化铵](../Page/四甲基氢氧化铵.md "wikilink")）和[乾法刻蚀](../Page/刻蝕.md "wikilink")（[RIE和DRIE](../Page/反应离子刻蚀.md "wikilink")），[电火花加工](../Page/电火花加工.md "wikilink")（EDM），和其他一些能够制造小型设备的加工方式。

[蘋果公司的設備以MEMS時鐘代替](../Page/蘋果公司.md "wikilink")[石英鐘](../Page/石英鐘.md "wikilink")。氦原子會破壞MEMS[積體電路](../Page/積體電路.md "wikilink")，使氦氣影響[iPhone](../Page/iPhone.md "wikilink")、[Apple
Watch和](../Page/Apple_Watch.md "wikilink")[iPad等設備](../Page/iPad.md "wikilink")。\[2\]\[3\]

## 历史

微型机械的概念在相应的加工技术出现之前就被提出了。1959年，[理查德·费曼在](../Page/理查德·费曼.md "wikilink")[加州理工学院进行题为](../Page/加州理工学院.md "wikilink")《》的演讲。费曼在演讲中提出了在[原子尺度上操纵物质的可能性以及将面临的挑战](../Page/原子.md "wikilink")。
1964年，[西屋公司的一支团队制造出了第一批微机电设备](../Page/西屋公司.md "wikilink")。\[4\]\[5\]这种设备名叫[谐振栅极晶体管](../Page/谐振栅极晶体管.md "wikilink")（）

## 简介

微机电系统是微米大小的机械系统，其中也包括不同形状的三维[平板印刷产生的系统](../Page/平板印刷.md "wikilink")。这些系统的大小一般在微米到毫米之间。在这个大小范围中日常的[物理经验往往不适用](../Page/物理.md "wikilink")。比如由于微机电系统的面积对体积比比一般日常生活中的机械系统要小得多，其表面现象如[静电](../Page/静电.md "wikilink")、[润湿等比体积现象如](../Page/润湿.md "wikilink")[惯性或](../Page/惯性.md "wikilink")[热容量等要重要](../Page/热容量.md "wikilink")。它们一般是由类似于生产[半导体的技术如](../Page/半导体.md "wikilink")[表面微加工](../Page/表面微加工.md "wikilink")、[体型微加工等技术制造的](../Page/体型微加工.md "wikilink")。其中包括更改的[硅加工方法如压延](../Page/硅.md "wikilink")、电镀、湿蚀刻、干蚀刻、电火花加工等等。

生产微机电系统的公司的大小各不相同。大的公司主要集中于为汽车、生物医学或电子工业生产大批量的便宜的系统。成功的小公司则集中于生产创新的技术。所有这些公司都致力于[研究开发](../Page/研究开发.md "wikilink")。随着[传感器的发展微机电系统的复杂性和效率不断提高](../Page/传感器.md "wikilink")。

常见的应用有：

  - 在[喷墨打印机里作为](../Page/打印机#喷墨打印机.md "wikilink")[压电元件](../Page/压电效应.md "wikilink")
  - 在汽车里作为[加速规来控制碰撞时](../Page/加速规.md "wikilink")[安全气囊防护系统的施用](../Page/安全气囊防护系统.md "wikilink")
  - 在汽车里作为[陀螺来测定汽车倾斜](../Page/陀螺.md "wikilink")，控制[动态稳定控制系统](../Page/动态稳定控制系统.md "wikilink")
  - 在[轮胎里作为](../Page/轮胎.md "wikilink")[压力传感器](../Page/压力传感器.md "wikilink")，在医学上测量[血压](../Page/血压.md "wikilink")
  - [数字微镜芯片](../Page/数字微镜芯片.md "wikilink")
  - 微型麦克风阵列
  - MEMS微型投影仪
  - 在[计算机网络中充当](../Page/计算机网络.md "wikilink")[光交换系统](../Page/光交换.md "wikilink")，这是一个与[智能灰尘技术的融合](../Page/智能灰尘.md "wikilink")

设计微机电系统最重要的工具是[有限元分析](../Page/有限元分析.md "wikilink")。

## 技术

微机电系统有多种原材料和制造技术，选择条件是系统的应用、市场等等。

### 硅

硅是用来制造[集成电路的主要原材料](../Page/集成电路.md "wikilink")。由于在电子工业中已经有许多实用硅制造极小的结构的经验，硅也是微机电系统非常常用的原材料。硅的物质特性也有一定的优点。单晶体的硅遵守[胡克定律](../Page/胡克定律.md "wikilink")，几乎没有[弹性滞后的现象](../Page/弹性滞后.md "wikilink")，因此几乎不耗能，其运动特性非常可靠。此外硅不易折断，因此非常可靠，其使用周期可以达到上兆次。一般微机电系统的生产方式是在基质上堆积物质层，然后使用平板印刷和蚀刻的方法来让它形成各种需要的结构。

#### 表面微加工

表面微加工是在硅[晶片上沉积](../Page/晶片.md "wikilink")[多晶硅然后进行加工](../Page/多晶硅.md "wikilink")。

#### 深层刻蚀

深层刻蚀如[深层反应离子刻蚀技术向硅晶片内部刻蚀](../Page/深层反应离子刻蚀.md "wikilink")。刻蚀到晶片内部的一个牺牲层。这个牺牲层在刻蚀完成后被腐蚀掉，这样本来埋在晶片内部的结构就可以自由运动了。

#### 体型微加工

体型微加工与深层刻蚀类似，是另一种去除硅的方法。一般体型微加工使用碱性溶液如[氢氧化钾来腐蚀平板印刷后留下来的硅](../Page/氢氧化钾.md "wikilink")。这些碱溶液腐蚀时的[相对各向异性非常强](../Page/相对各向异性.md "wikilink")，沿一定的晶体方向的腐蚀速度比其它的高1000倍。这样的过程往往用来腐蚀v状的沟。假如选择的原材料的[晶向足够精确的话这样的沟的边可以非常平](../Page/晶向.md "wikilink")。

### 高分子材料

虽然电子工业对硅加工的经验是非常丰富和宝贵的，并提供了很大的经济性，但是纯的硅依然是非常昂贵的。高分子材料非常便宜，而且其性能各种各样。使用[注射成形](../Page/注射成形.md "wikilink")、[压花](../Page/压花.md "wikilink")、[立体光固化成形等技术也可以使用高分子材料制造微机电系统](../Page/立体光固化成形.md "wikilink")，这样的系统尤其有利于微液体应用，比如可携测血装置等。

### 金属

金属也可以用来制造微机电系统。虽然比起硅来金属缺乏其良好的机械特性，但是在金属的适用范围内它非常可靠。

## 研究和开发

MEMS研究人员使用一系列的工程软件工具来对他们的设计进行仿真和原型测试。MEMS设计中经常用到[有限元分析](../Page/有限元分析.md "wikilink")。对动态力，热度等等的仿真可以通过[ANSYS](../Page/ANSYS.md "wikilink")，[COMSOL](../Page/COMSOL.md "wikilink")、[IntelliSuite和](../Page/IntelliSuite.md "wikilink")[CoventorWare](../Page/CoventorWare.md "wikilink")-ANALYZER等软件来实现。其他软件，比如ConvertorWare-ARCHITECT和MEMS-PRO，被用来开发更适合加工制造的产品布局，甚至用来仿真嵌入型的MEMS系统。当原型机开发完成后，研究人员能够用各种仪器比如激光多普勒扫描振動计，显微镜，频闪观测仪等来对它们进行测试。

## 参考文献

[Category:机电一体化](../Category/机电一体化.md "wikilink")
[Category:电子工程](../Category/电子工程.md "wikilink")
[Category:机械工程](../Category/机械工程.md "wikilink")
[Category:纳米技术](../Category/纳米技术.md "wikilink")

1.
2.
3.
4.  Electromechanical monolithic resonator,[US
    patent 3614677](http://www.google.com/patents/about?id=hpcBAAAAEBAJ&dq=ELECTROMECHANICAL+MONOLITHIC+RESONATOR),
    Filed April 29, 1966; Issued October 1971
5.