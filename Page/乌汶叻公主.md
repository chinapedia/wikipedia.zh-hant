**乌汶叻公主**（，；），生于[瑞士](../Page/瑞士.md "wikilink")[洛桑的瑞士山醫院](../Page/洛桑.md "wikilink")，泰國長[公主及](../Page/公主.md "wikilink")[女演員](../Page/女演員.md "wikilink")，[泰国国王](../Page/泰国.md "wikilink")[普密蓬和](../Page/普密蓬·阿杜德.md "wikilink")[诗丽吉王后的长女](../Page/诗丽吉王后.md "wikilink")、現任泰王[瑪哈·瓦集拉隆功](../Page/瑪哈·瓦集拉隆功.md "wikilink")（[拉瑪十世](../Page/拉瑪十世.md "wikilink")）之胞姊。

## 生平

乌汶叻公主畢業於[美国](../Page/美国.md "wikilink")[麻省理工学院數學理科](../Page/麻省理工学院.md "wikilink")[學士學位](../Page/學士.md "wikilink")、[加州大學洛杉磯分校公共衛生學](../Page/加州大學洛杉磯分校.md "wikilink")[碩士學位](../Page/碩士.md "wikilink")。毕业后与美国人（Peter
Ladd
Jensen）结婚，放棄公主頭銜并移居美国。婚后育有三个子女。1999年2月和丈夫离婚，2001年正式携子女回到泰国長住，並重新執行王室職務，但未恢復「[殿下](../Page/殿下.md "wikilink")」稱呼；另外她亦開始投身慈善事業。

公主曾於2008年电影《》和2010年电影《》中演出。

2019年2月，烏汶叻公主接受被視為支持[他信·西那瓦的政黨](../Page/他信·西那瓦.md "wikilink")邀請，在下月大選中競逐[宰相的](../Page/宰相.md "wikilink")[寶座](../Page/寶座.md "wikilink")，打破泰國[王族不參與政治的慣例](../Page/王族.md "wikilink")。而作為[立憲](../Page/立憲.md "wikilink")[虛君王室成員](../Page/虛君.md "wikilink")，此直接參政並投入選舉的舉動在國內外皆極不尋常，亦欠先例。\[1\]烏汶叻公主聲稱自身已放棄王室頭銜，與所有泰國人民一樣並無任何特權，亦不應受皇室傳統掣肘。\[2\]

其胞弟現任泰王[瑪哈·瓦集拉隆功](../Page/瑪哈·瓦集拉隆功.md "wikilink")（[拉瑪十世](../Page/拉瑪十世.md "wikilink")）代表泰國王室發表聲明，其表示王室高級成員參政無論如何都是藐視國家傳統、慣例和文化的舉動，極之不當。公主則回應自己只是以平民身分參選\[3\]。

在泰王的影響下，表示该党将遵守国王[敕令阻止乌汶叻公主竞选泰国总理](../Page/敕令.md "wikilink")\[4\]。

2月11日，泰国选举委员会认定乌汶叻不符合竞选总理的资格\[5\]。

3月7日，[泰國憲法法院認定](../Page/泰國憲法法院.md "wikilink")違反[君主立憲制而裁定解散](../Page/君主立憲制.md "wikilink")，該黨常務委員會成員未來10年不得再從事政治活動。

## 頭銜

  - 1951年-1972年：乌汶叻公主殿下
  - 1972年-1998年：乌汶叻·詹森女士
  - 1998年-：乌汶叻公主

## 图库

<File:Royal> Monogram of Princess Ubolratana Rajakanya.svg|王家标志
<File:Royal> Flag of Princess Ubolratana Rajakanya.svg|王家旗帜

## 参考资料

[Category:泰国公主](../Category/泰国公主.md "wikilink")
[Category:扎克里王朝](../Category/扎克里王朝.md "wikilink")
[Category:麻省理工學院校友](../Category/麻省理工學院校友.md "wikilink")
[Category:洛杉磯加州大學校友](../Category/洛杉磯加州大學校友.md "wikilink")

1.
2.
3.
4.  [反转！泰爱国党将阻止乌汶叻公主竞选泰国总理](http://world.huanqiu.com/exclusive/2019-02/14269089.html)
5.  [外媒：泰选举委员会认定乌汶叻不符竞选总理资格](http://www.chinanews.com/gj/2019/02-11/8751157.shtml)