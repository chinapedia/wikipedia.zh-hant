《**OK牧場大決鬥**》（）為集合眾多[好萊塢大卡司](../Page/好萊塢.md "wikilink")[动作片演員年輕時拍攝的熱鬧](../Page/动作片演員.md "wikilink")、[娛樂性極高之](../Page/娛樂.md "wikilink")[1957年](../Page/1957年電影.md "wikilink")[西部片](../Page/西部片.md "wikilink")，也是數位[影星早年成名作](../Page/影星.md "wikilink")；此片在[票房賣座之成功部份可歸功於導演](../Page/票房.md "wikilink")[約翰·司圖加巧思](../Page/約翰·司圖加.md "wikilink")。

## 演員陣容

## 外部連結

  -
  -
[Category:1957年電影](../Category/1957年電影.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:1950年代動作片](../Category/1950年代動作片.md "wikilink")
[Category:美國動作片](../Category/美國動作片.md "wikilink")
[Category:西部片](../Category/西部片.md "wikilink")
[Category:亞利桑那州背景電影](../Category/亞利桑那州背景電影.md "wikilink")
[Category:真人真事改編電影](../Category/真人真事改編電影.md "wikilink")
[Category:亞利桑那州取景電影](../Category/亞利桑那州取景電影.md "wikilink")
[Category:派拉蒙影業電影](../Category/派拉蒙影業電影.md "wikilink")