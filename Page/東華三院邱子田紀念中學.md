[TWGH_Yau_Tsz_Tin_Memorial_College.jpg](https://zh.wikipedia.org/wiki/File:TWGH_Yau_Tsz_Tin_Memorial_College.jpg "fig:TWGH_Yau_Tsz_Tin_Memorial_College.jpg")
**東華三院邱子田紀念中學**（；簡稱），是[東華三院開辦的第十間中學](../Page/東華三院.md "wikilink")，故又名**東華第十中學**，學校創校於1982年，是[香港](../Page/香港.md "wikilink")[屯門區一所全日制中文中學](../Page/屯門區.md "wikilink")，校內三樓的圖書館叫作景賢閣。

學校座落於[新界](../Page/新界.md "wikilink")[屯門](../Page/屯門.md "wikilink")[兆康苑北面](../Page/兆康苑.md "wikilink")，鄰近[西鐵綫及](../Page/西鐵綫.md "wikilink")[輕鐵](../Page/輕鐵.md "wikilink")[兆康站](../Page/兆康站_\(西鐵綫\).md "wikilink")。創校時曾借用[東華三院辛亥年總理中學作課室之用](../Page/東華三院辛亥年總理中學.md "wikilink")。學校正門的校名由邱木城（[景賢里業主](../Page/景賢里.md "wikilink")）題字。邱田元和邱子田是親戚，同期第九間中學是[沙田區的](../Page/沙田區.md "wikilink")[東華三院邱金元中學](../Page/東華三院邱金元中學.md "wikilink")。

## 教學語言

配合[母語教學政策](../Page/母語教學.md "wikilink")，該校初中以中文作教學語言，高中亦以中文為主；物理、化學、生物科以英文作教學語言。

## 學校組織

<table>
<thead>
<tr class="header">
<th><p>|學術學會</p></th>
<th><p>|興趣學會</p></th>
<th><p>|服務隊伍</p></th>
<th><p>|運動學會</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><ul>
<li>中文學會</li>
<li>英文學會</li>
<li>數學學會</li>
<li>電腦學會</li>
<li>科學學會</li>
<li>歷史學會</li>
<li>通識學會</li>
</ul></td>
<td><ul>
<li>音樂學會</li>
<li>美術學會</li>
<li>設計與科技學會</li>
<li>家政學會</li>
<li>普通話學會</li>
<li>戲劇學會</li>
<li>棋藝學會</li>
<li>攝影學會</li>
<li>足毽學會</li>
<li>國術學會</li>
<li>園藝學會</li>
<li>文藝學會</li>
<li>橋牌學會</li>
<li>羽毛球學會</li>
</ul></td>
<td><ul>
<li>童軍</li>
<li>女童軍</li>
<li>社會服務團</li>
</ul></td>
<td><ul>
<li>體育學會</li>
<li>舞蹈學會</li>
</ul></td>
</tr>
</tbody>
</table>

## 四社

東華三院邱子田紀念中學以校訓勤、儉、忠、信分社。

## 著名校友

  - [陳國邦](../Page/陳國邦.md "wikilink")：演員
  - [梁浩賢](../Page/梁浩賢_\(歌手\).md "wikilink")：歌手
  - [黎諾懿](../Page/黎諾懿.md "wikilink")：演員
  - [梁嘉琪](../Page/梁嘉琪.md "wikilink")：演員
  - [張國鈞](../Page/張國鈞.md "wikilink")：[民建聯副主席](../Page/民建聯.md "wikilink")、[行政會議成員](../Page/香港特別行政區行政會議.md "wikilink")、[立法會議員（香港島）](../Page/第六屆香港立法會.md "wikilink")、[中西區區議員](../Page/中西區區議會.md "wikilink")（[西環選區](../Page/西環.md "wikilink")）\[1\]
  - [陳永業](../Page/陳永業.md "wikilink")：唱片騎師、演員（1988年中六畢業)
  - [林若寧](../Page/林若寧.md "wikilink")：本名龐健章，香港男填詞人，2001年開始填詞，亦是商台叱咤903總監。著名填詞人林夕的徒弟

## 相關條目

  - [景賢里](../Page/景賢里.md "wikilink")

## 參考來源

## 外部連結

  - [東華三院邱子田紀念中學官方網址](http://www.ytt.edu.hk/)
  - [Facebook group](http://www.facebook.com/group.php?gid=2348129872)

[Category:屯門](../Category/屯門.md "wikilink")
[Category:東華三院中學](../Category/東華三院中學.md "wikilink")
[T](../Category/屯門區中學.md "wikilink")
[Category:1982年創建的教育機構](../Category/1982年創建的教育機構.md "wikilink")

1.