**何啟**爵士，[CMG](../Page/CMG.md "wikilink")，[JP](../Page/太平紳士.md "wikilink")，MRCS（Sir
**Kai
Ho**，），原名**何神啟**，[字](../Page/表字.md "wikilink")**迪之**，[號](../Page/號.md "wikilink")**沃生**。[香港出生](../Page/香港.md "wikilink")，[籍貫](../Page/籍貫.md "wikilink")[廣東](../Page/廣東.md "wikilink")[南海](../Page/南海.md "wikilink")，基督教徒，[何福堂家族成員](../Page/何福堂家族.md "wikilink")，是香港第一位獲封為[爵士的](../Page/爵士.md "wikilink")[華人](../Page/華人.md "wikilink")，同時是著名[醫生](../Page/醫生.md "wikilink")、[大律師](../Page/大律師.md "wikilink")、[商人暨](../Page/商人.md "wikilink")[政治家](../Page/政治家.md "wikilink")。

## 簡歷

何啟的父親是[何福堂](../Page/何福堂.md "wikilink")[牧師](../Page/牧師.md "wikilink")，胞姐為[何妙齡](../Page/何妙齡.md "wikilink")，姐夫為首名華人[立法局非官守議員](../Page/香港立法會.md "wikilink")[伍才](../Page/伍才.md "wikilink")（[伍廷芳](../Page/伍廷芳.md "wikilink")）\[1\]。何啟在[中央書院畢業](../Page/中央書院.md "wikilink")，於1871年9月到[英國留學](../Page/英國.md "wikilink")，先後在[鴨巴甸大學取得醫科碩士學位及在](../Page/鴨巴甸大學.md "wikilink")[林肯律師學院取得高級法律學士學位](../Page/林肯律師學院.md "wikilink")，於[倫敦與](../Page/倫敦.md "wikilink")[雅麗氏](../Page/雅麗氏.md "wikilink")（Alice
Walkden）結婚。於1882年返回香港，成為[大律師](../Page/大律師.md "wikilink")。於1887年與[倫敦傳道會及有熱帶醫學之父之稱的](../Page/倫敦傳道會.md "wikilink")[白文信醫生共同創辦香港](../Page/白文信.md "wikilink")[雅麗氏利濟醫院](../Page/雅麗氏利濟醫院.md "wikilink")
(雅麗氏何妙齡那打素醫院)，附設一所[香港華人西醫書院](../Page/香港華人西醫書院.md "wikilink")，是[香港大學的前身](../Page/香港大學.md "wikilink")。於1890年獲得委任為[定例局非官守議員](../Page/香港立法局.md "wikilink")。1909年出任香港大學助捐董事會主席。由於在香港社會貢獻良多，於1912年，何啟獲得[英國政府頒授爵士勳銜](../Page/英國政府.md "wikilink")。何啟於1914年逝世\[2\]，安葬於[跑馬地](../Page/跑馬地.md "wikilink")[香港墳場](../Page/香港墳場.md "wikilink")。

## 家庭

**何啟**回港初期受兄姐[何添及](../Page/何添_\(何福堂子\).md "wikilink")[何妙齡地產投資失利牽連而陷入財困](../Page/何妙齡.md "wikilink")。到晚年何啟因投資不善陷於債務\[3\]，需要姻親[區德和好友](../Page/區德.md "wikilink")[曹善允等人接濟](../Page/曹善允.md "wikilink")。於1914年逝世後由於生活拮据，區德和曹善允等又各自出資750港元，作為殮葬何啟和接濟其家人的費用\[4\]。香港政府則提供學費資助予在學的永乾、永元、永利、永安、永康及好友[韋寶珊接濟其家庭](../Page/韋寶珊.md "wikilink")。1918年何啟子女一家遷居上海投靠姑丈[伍廷芳](../Page/伍廷芳.md "wikilink")。

**何啟**於[倫敦期間與英國人](../Page/倫敦.md "wikilink")[雅麗氏](../Page/雅麗氏.md "wikilink")（Alice
Walkden）結婚，於1882年返回香港。誕下一女後於1884年因[傷寒去世](../Page/傷寒.md "wikilink")。後續弦娶父親為美國人的[黎玉卿為妻](../Page/黎玉卿.md "wikilink")。**何啟**育有十子八女，除長女外皆為黎氏所出。\[5\]。

  - 何女（1884－？）－雅麗氏之女，1884年母親逝世後送回英國親人撫養，早逝未婚
  - 長子[何永貞](../Page/何永貞.md "wikilink")（？－？）－妻子區氏為[區德之女](../Page/區德.md "wikilink")，1909年曾肄業於[香港西醫書院](../Page/香港西醫書院.md "wikilink")，後任職上海南京鐵路技監，有一子[何鸿铎](../Page/何鸿铎.md "wikilink")、孫女[何纪青](../Page/何纪青.md "wikilink")\[6\]
  - 次子[何永亨](../Page/何永亨.md "wikilink")（？－？）－已婚，1909年曾肄業於[香港西醫書院](../Page/香港西醫書院.md "wikilink")，後任職上海南京鐵路
  - 三子[何永乾](../Page/何永乾.md "wikilink")（1892－？）－1916年畢業於[香港大學](../Page/香港大學.md "wikilink")[工程學院](../Page/工程學院.md "wikilink")，同學為妹夫[傅秉常](../Page/傅秉常.md "wikilink")，後任Hall
    and Hall Associates建築師行合夥人，有一子Arnold Hall
  - 四子[何永元](../Page/何永元.md "wikilink")（1894－？）－畢業於[香港大學](../Page/香港大學.md "wikilink")[工程學院](../Page/工程學院.md "wikilink")，後任Hall
    and Hall Associates建築師行合夥人
  - 五子[何永利](../Page/何永利.md "wikilink")（1895－？）－1914年獲[香港大學取錄](../Page/香港大學.md "wikilink")，後任職上海浦東鐵路
  - 六子[何永安](../Page/何永安.md "wikilink")（1900－？）－1917年畢業於[聖保羅書院](../Page/聖保羅書院.md "wikilink")，後任職上海南京鐵路
  - 七子[何永康](../Page/何永康.md "wikilink")（1903－1997）－1914-18年間肄業於[聖保羅書院](../Page/聖保羅書院.md "wikilink")，1918年隨家人移居上海學習
  - 八子[何永感](../Page/何永感.md "wikilink")（1909－？）－1914-18年間在香港預備班學習，1918年隨家人移居上海學習
  - 九子[何永德](../Page/何永德.md "wikilink")（1910－？）－1918年隨家人移居上海學習
  - 十子[何永謝](../Page/何永謝.md "wikilink")（1913－？）－1918年隨家人移居上海學習
  - 長女[何瑞金](../Page/何瑞金.md "wikilink")（？－？）－丈夫為表哥[伍朝樞](../Page/伍朝樞.md "wikilink")，隨[伍廷芳到民國政府從政](../Page/伍廷芳.md "wikilink")
  - 次女[何瑞銀](../Page/何瑞銀.md "wikilink")（？－？）－
  - 三女[何瑞銅](../Page/何瑞銅.md "wikilink")（？－？）－已婚
  - 四女[何瑞鐵](../Page/何瑞鐵.md "wikilink")（？－？）－丈夫香港牙醫Herbert To
  - 五女[何瑞錫](../Page/何瑞錫.md "wikilink")（1898－？）－丈夫[傅秉常為](../Page/傅秉常.md "wikilink")[何永乾同學兼](../Page/何永乾.md "wikilink")[香港大學首位一級榮譽畢業生](../Page/香港大學.md "wikilink")，初投[伍廷芳門下](../Page/伍廷芳.md "wikilink")，後官至外交部部長\[7\]，[何瑞錫於](../Page/何瑞錫.md "wikilink")1914-18年間在香港預備班學習
  - 六女[何瑞華](../Page/何瑞華.md "wikilink")（1901－？）－1914-18年間在香港預備班學習
  - 七女[何瑞美](../Page/何瑞美.md "wikilink")（？－？）－

另有孫輩如下： 孫：

  - Arnold
    Hall－上海出生，曾於香港[英皇書院及上海](../Page/英皇書院.md "wikilink")[聖約翰大學就讀](../Page/聖約翰大學_\(上海\).md "wikilink")，後在香港和美國生活
  - [何鸿铎](../Page/何鸿铎.md "wikilink")－定居佛山
  - [何鴻鈞](../Page/何鴻鈞.md "wikilink")
  - [何鴻威](../Page/何鴻威.md "wikilink")
  - [何鴻鑄](../Page/何鴻鑄.md "wikilink")
  - [何鴻銳](../Page/何鴻銳.md "wikilink")
  - [何鴻志](../Page/何鴻志.md "wikilink")
  - [何鴻星](../Page/何鴻星.md "wikilink")
  - [何鴻信](../Page/何鴻信.md "wikilink")
  - [何鴻震](../Page/何鴻震.md "wikilink")
  - [何鴻飛](../Page/何鴻飛.md "wikilink")
  - [何鴻光](../Page/何鴻光.md "wikilink")
  - [何鴻鈺](../Page/何鴻鈺.md "wikilink")

孫女：

  - [何鴻群](../Page/何鴻群.md "wikilink")
  - [何鴻卿](../Page/何鴻卿_\(何福堂家族\).md "wikilink")
  - [何鴻珠](../Page/何鴻珠.md "wikilink")
  - [何婉鈴](../Page/何婉鈴.md "wikilink")
  - [何逸雲](../Page/何逸雲.md "wikilink")
  - [何逸霞](../Page/何逸霞.md "wikilink")
  - [何逸平](../Page/何逸平.md "wikilink")
  - [何月華](../Page/何月華.md "wikilink")

## 何啟與中國

何啟對當時的中國國勢十分關心。相關的論述有：

  - 1887年：《曾論書後》：批評[曾紀澤的](../Page/曾紀澤.md "wikilink")《中國先睡後醒論》。
  - 1895年：《新政論議》：受[甲午戰爭的失敗所影響](../Page/甲午戰爭.md "wikilink")，提出改革中國的建議。
  - 1898年：《康說書後》：反對[康有為鼓動中國人的排外情緒](../Page/康有為.md "wikilink")，認為應以和平的態度處理國際關係。
  - 1899年：《勸學篇書後》：批評[張之洞](../Page/張之洞.md "wikilink")「中體西用」的方法。

何啟曾擔任[孫中山的老師](../Page/孫中山.md "wikilink")，亦曾支持其革命事業。孫中山曾說過：「自己受惠於何啟之教。」\[8\]

## 何啟與香港醫療

1887年，何啟為紀念在1884年去世的亡妻[雅麗氏而出資於](../Page/雅麗氏.md "wikilink")[上環](../Page/上環.md "wikilink")[荷李活道興建雅麗氏紀念醫院](../Page/荷李活道.md "wikilink")（Alice
Memorial
Hospital），成為本港首間為貧苦華人提供西醫治療的醫院，醫院後來與那打素醫院合併。1904年，何啟又創立雅麗氏紀念產科醫院（Alice
Memorial Maternity
Hospital），是香港第一所產科醫院，也是中國最早的產科醫院之一；1906年，由於雅麗氏醫院和那打素合併後，病床仍出現不足情況，何啟之胞姐[何妙齡捐款興建新醫院](../Page/何妙齡.md "wikilink")，起名何妙齡醫院（Ho
Miu Ling
Hospital）。後至1954年，那打素醫院、雅麗氏紀念產科醫院及何妙齡醫院三家醫院合併為[雅麗氏何妙齡那打素醫院](../Page/雅麗氏何妙齡那打素醫院.md "wikilink")，後來醫院遷至今[大埔現址](../Page/大埔_\(香港\).md "wikilink")。

何啟亦倡建[廣華醫院的其中一人](../Page/廣華醫院.md "wikilink")，更曾為該醫院的董事局主席。廣華醫院最終於1911年成立。

## 何啟與[香港大學](../Page/香港大學.md "wikilink")

本身是西醫的何啟，眼見本地的醫療培訓不足，便和志同道合的好友合辦[香港西醫書院](../Page/香港西醫書院.md "wikilink")，並親任講師，培訓本地醫療人才。1907年，[香港總督](../Page/香港總督.md "wikilink")[盧吉提出興辦本地大學的主張](../Page/盧吉.md "wikilink")。何啟表示讚成，更提出「三院制」，即主張將已有的[香港西醫書院](../Page/香港西醫書院.md "wikilink")、[香港工程學院](../Page/香港工程學院.md "wikilink")，以及新增香港文學院合成一所大學。1909年，香港大學籌備委員會成立，何啟更出任捐助董事會主席，香港各界均有捐助。至1912年3月，香港大學正式揭幕。\[9\]

## 何啟與「啟德」

19世紀末期，有人在[宋王臺聖山採石作建築材料](../Page/宋王臺.md "wikilink")，對宋王臺古跡做成威脅。何啟支持九龍城居民發起的保存宋王臺運動，1898年8月15日在立法局提出動議，要求政府立法保存宋王臺古跡。1899年，立法局通過《保存宋王臺條例》，禁止在宋王臺聖山採石。

在19世紀80年代九龍半島平地多用作住宅區，人口因商業繁盛而急劇增加，居民住宅擠迫之狀非常惡劣，何啟姐夫[伍才](../Page/伍才.md "wikilink")（[伍廷芳](../Page/伍廷芳.md "wikilink")）提出於九龍灣填海辟建住宅區的構思\[10\]。清末至辛亥革命期間，大量移民由國內湧入香港，令住房供不應求\[11\]
\[12\]
\[13\]，1914年初何啟與姻親[區德](../Page/區德.md "wikilink")（長子[何永貞岳丈](../Page/何永貞.md "wikilink")）牽頭組成「啟德營業有限公司」，在[九龍寨城對開的九龍灣北岸進行填海工程](../Page/九龍寨城.md "wikilink")，計劃建造一個名為[啟德濱的花園城市](../Page/啟德濱.md "wikilink")。但此計劃在何啟逝世後，因1920年代的[省港大罷工及](../Page/省港大罷工.md "wikilink")[海員大罷工之影響而失敗](../Page/海員大罷工.md "wikilink")，土地其後被政府以近100萬港元購回，最後被發展成為[啟德機場](../Page/啟德機場.md "wikilink")\[14\]。

## 參見

  - [啟德濱](../Page/啟德濱.md "wikilink")
  - [啟德機場](../Page/啟德機場.md "wikilink")
  - [啟德隧道](../Page/啟德隧道.md "wikilink")
  - [胡礼垣](../Page/胡礼垣.md "wikilink")

## 外部連結

  - [博物館開辦墓之旅-
    太陽報](http://the-sun.on.cc/channels/news/20051017/20051017024839_0001.html)
  - [何啟 -
    宋王臺](http://www2.hkedcity.net/citizen_files/ab/aw/tc6628/public_html/new_page_3.htm)

## 参考文献

  - 《中國知識分子與近代民主思想》，林啟彥著，中華書局，1989年，ISBN 978-962-231-624-9
  - Carroll, JM 'Ho Kai: A Chinese Reformer in Colonial Hong Kong'In The
    Human Tradition in Modern China, p. 55-72. Lanham, Maryland: Rowman
    and Littlefield, 2008
  - [Choa, G. H. (2000) *.The Life and Times of Sir Kai Ho*, Chinese
    University Press, ISBN
    978-962-201-873-0](http://books.google.co.uk/books?id=nzWs6_BUBPoC&printsec=frontcover&dq=%22The+Life+and+Times+of+Sir+Kai+Ho%22&source=bl&ots=i7Pu00gZOT&sig=vEamlLgMKLebFPf65a_9ozUXqOg&hl=en&ei=y7SrS4GMNaT-0gTxv_CXDg&sa=X&oi=book_result&ct=result&resnum=1&ved=0CAYQ6AEwAA#v=onepage&q=&f=false)

## 注釋

<div class="references-small">

<references />

[K](../Category/何姓.md "wikilink")
[Category:香港南海人](../Category/香港南海人.md "wikilink")
[H](../Category/皇仁書院校友.md "wikilink")
[H](../Category/阿伯丁大学校友.md "wikilink")
[H](../Category/香港醫學界人士.md "wikilink")
[H](../Category/香港法律界人士.md "wikilink")
[H](../Category/前香港立法局議員.md "wikilink")
[H](../Category/下級勳位爵士.md "wikilink")
[Category:CMG勳銜](../Category/CMG勳銜.md "wikilink")
[Category:啟德](../Category/啟德.md "wikilink")

1.

2.
3.

4.

5.
6.

7.

8.  [郭位](../Page/郭位.md "wikilink")《「香港各界慶祝[雙十節暨](../Page/雙十節.md "wikilink")[辛亥革命](../Page/辛亥革命.md "wikilink")101周年大會」演講稿》，[香港](../Page/香港.md "wikilink")，《[明報](../Page/明報.md "wikilink")》，2012年10月7日。

9.

10.

11.

12.

13.

14.