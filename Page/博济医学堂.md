[Sun_Yat-sen_Medical_College_of_Lingnan_Univ.jpg](https://zh.wikipedia.org/wiki/File:Sun_Yat-sen_Medical_College_of_Lingnan_Univ.jpg "fig:Sun_Yat-sen_Medical_College_of_Lingnan_Univ.jpg")
[Sun_Yat-sen_Medical_College_of_Lingnan_University.jpg](https://zh.wikipedia.org/wiki/File:Sun_Yat-sen_Medical_College_of_Lingnan_University.jpg "fig:Sun_Yat-sen_Medical_College_of_Lingnan_University.jpg")。\]\]
**博济医学堂**成立于1866年，由[广州](../Page/广州.md "wikilink")[博济医院](../Page/博济医院.md "wikilink")（The
Canton
Hospital）创办，是一所[教会](../Page/教会.md "wikilink")[医学专业学校](../Page/医学.md "wikilink")，首任校长是[嘉约翰医生](../Page/嘉约翰.md "wikilink")；博济医学堂也是[中国的第一所](../Page/中国.md "wikilink")[西医教育机构](../Page/西医.md "wikilink")。1879年，博济医学堂改名为博济医院南华医学校（South
China Medical
College）\[1\]。1936年，博济医学堂发展成为[岭南大学医学院](../Page/岭南大学_\(广州\).md "wikilink")；1949年以后，与[中山大学医学院及](../Page/中山大学医学院.md "wikilink")[光华医学院合并](../Page/光华医学院.md "wikilink")，成为[中山医学院](../Page/中山医学院.md "wikilink")，再改名为[中山医科大学](../Page/中山医科大学.md "wikilink")。后又与[中山大学合并](../Page/中山大学.md "wikilink")，现为[中山大学中山医学院](../Page/中山大学中山医学院.md "wikilink")。

## 历史

[博济医院由嘉约翰经营](../Page/博济医院.md "wikilink")10年后，已具相当规模，医院设备良好，医师力量增强，医疗水平在广州已属顶级。经过历届收受生徒，特别是1861年和1863年两届生徒培训，已经具备开办医学班的条件。1865年博济医院建院30周年时成立博济医校，首届招生8人，由嘉约翰掌教。每周逢周三、六进行课题讲授，星期一、五出门诊学习诊治，周二、四在手术室学习手术割治。学生参与医院日常事物、施药、通常手术割治等助手工作，三年毕业。1868年学生增至12人，嘉约翰教授药物学、化学，[黄宽教授解剖学](../Page/黄宽.md "wikilink")、生理学和外科学，关韬教授临床各科。开班第二年，曾于院中示范解剖尸体一具，由[黄宽执刀](../Page/黄宽.md "wikilink")。

1879年，博济医校应[真光女校学生的请求](../Page/广州市真光中学.md "wikilink")，接受2名女生入学，成为该校招收女生之始，同时也成为中国培训女医师之始。

1885年，医校增加讲课和实习时间，充实教学内容，仍为三年制。孙中山于1886年秋入该校学习，次年转入香港华人西医书院（现为[香港大学李嘉诚医学院](../Page/香港大学李嘉诚医学院.md "wikilink")）。时有男生12人，女生4人。至1897年，有男生25人，女生6人。同年学制改为4年。

1899年，博济医院和医校交由关约翰主掌。1901年博济医院建设成为正规医校，建设独立校舍。新校舍于1903年建成，为当时广州的新式楼宇，命名为南华医学堂。成为了中国历史上开办的第一所现代医学院。1911年学堂停办。从博济医校到南华医学堂，共办学46年，先后共培养毕业生120多人。

## 校友

  - [孙中山](../Page/孙中山.md "wikilink")：于1886年入读。
  - [陈垣](../Page/陈垣.md "wikilink")：于1907年入读，[宗教](../Page/宗教.md "wikilink")[历史学家](../Page/历史学家.md "wikilink")
  - [康廣仁](../Page/康廣仁.md "wikilink")：[康有為之弟](../Page/康有為.md "wikilink")，[戊戌六君子](../Page/戊戌六君子.md "wikilink")

## 参见

  - [廣州地名](../Page/广州地名.md "wikilink")
  - [博濟醫院](../Page/博濟醫院.md "wikilink")
  - [中山大学中山医学院](../Page/中山大学中山医学院.md "wikilink")

[中山医学院](../Category/中山医学院.md "wikilink")
[B博](../Category/广州历史.md "wikilink")
[Category:广州教育史](../Category/广州教育史.md "wikilink")
[B博](../Category/中国教会大学.md "wikilink")
[Category:1866年創建的教育機構](../Category/1866年創建的教育機構.md "wikilink")
[Category:1911年廢除](../Category/1911年廢除.md "wikilink")

1.  [中山大学馆藏近代医学档案情况简介](http://ulib.iupui.edu/wmicproject/sites/default/files/SYSU_Medical_School_Report.pdf)
    IUPUI University Library