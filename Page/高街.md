[西營盤早期街道規劃.png](https://zh.wikipedia.org/wiki/File:西營盤早期街道規劃.png "fig:西營盤早期街道規劃.png")

**高街**（），是[香港](../Page/香港.md "wikilink")[西營盤的一條街道](../Page/西營盤.md "wikilink")，單線行車，可容[小巴及私家車由東至西走向](../Page/小巴.md "wikilink")，有一行[收費咪表路邊泊車位](../Page/收費咪錶.md "wikilink")，兩邊有行人道，西端連接[薄扶林道](../Page/薄扶林道.md "wikilink")，東端則連接[般咸道](../Page/般咸道.md "wikilink")，鄰近[香港大學西閘](../Page/香港大學.md "wikilink")。另外，在[英国的](../Page/英国.md "wikilink")[牛津大學亦有一條同樣是東西走向的同名街道](../Page/牛津大學.md "wikilink")[高街
(牛津)](../Page/高街_\(牛津\).md "wikilink")。

## 高街鬼屋

高街有一座建於1892年的[精神病院](../Page/精神病院.md "wikilink")。二次大戰時，位於[醫院前面的佐治五世公園則曾經是一個](../Page/醫院.md "wikilink")[亂葬崗](../Page/萬人塚.md "wikilink")，因此在這裏不時會有一些靈異傳聞。戰後繼續成為精神病院，因為當時醫術沒有今天那麼先進，[精神病人通常都是進院](../Page/精神病.md "wikilink")「等[死](../Page/死.md "wikilink")」的，使這座建築物變得更為可怕。現時已改建為[西營盤社區綜合大樓](../Page/西營盤社區綜合大樓.md "wikilink")。

## 沿路著名地標

  - [戴麟趾康復中心](../Page/戴麟趾康復中心.md "wikilink")（原址為[前半山警署](../Page/前半山警署.md "wikilink")）
  - [般咸道官立小學](../Page/般咸道官立小學.md "wikilink")
  - [西營盤社區綜合大樓](../Page/西營盤社區綜合大樓.md "wikilink")，以前是[精神病院及](../Page/精神病.md "wikilink")[麻瘋病院](../Page/麻瘋病.md "wikilink")(高街鬼屋)
  - [香港佐治五世紀念公園](../Page/香港佐治五世紀念公園.md "wikilink")，現設有[港鐵](../Page/港鐵.md "wikilink")[港島綫](../Page/港島綫.md "wikilink")[西營盤站出口](../Page/西營盤站.md "wikilink")
  - [正街街市及](../Page/正街街市.md "wikilink")[西營盤街市](../Page/西營盤街市.md "wikilink")
  - [香港崇真會救恩堂及](../Page/香港崇真會救恩堂.md "wikilink")[救恩學校](../Page/救恩學校.md "wikilink")
  - [李陞小學](../Page/李陞小學.md "wikilink")
  - 西區牙科診所
  - [石牆樹](../Page/香港石牆樹.md "wikilink")

<File:HK> High Street Crime Wing HK Island Reg HQ
1.jpg|位於高街一號的[前半山警署](../Page/前半山警署.md "wikilink")
<File:HK> Li Sing Primary School 119 High
Street.JPG|高街119號[李陞小學](../Page/李陞小學.md "wikilink")
<File:HK> Sai Ying Pun High Street 9 Ko Nga
Court.jpg|高街9號，東望[香港佐治五世紀念公園老](../Page/香港佐治五世紀念公園.md "wikilink")[榕樹](../Page/榕樹.md "wikilink")

## 參考

<references/>

[Category:西營盤街道](../Category/西營盤街道.md "wikilink")