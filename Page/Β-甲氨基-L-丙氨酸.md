**β-甲氨基-L-丙氨酸**（英文**β-methylamino
L-alanine**，簡稱**BMAA**），是一種[神經毒素](../Page/神經毒素.md "wikilink")，能在[蘇鐵科植物種子中找到](../Page/蘇鐵科.md "wikilink")。這一種[非蛋白胺基酸](../Page/非蛋白胺基酸.md "wikilink")，與非必要胺基酸[丙氨酸](../Page/丙氨酸.md "wikilink")（alanine）十分相似，是由[念珠藻屬的](../Page/念珠藻屬.md "wikilink")[藍藻產生](../Page/藍藻.md "wikilink")，而這些藍藻大都生長於植物的根部。

## 神經毒性

根據一些研究，BMAA
被視為導致[關島型](../Page/關島.md "wikilink")「[肌肉萎縮性側索硬化症](../Page/肌肉萎縮性側索硬化症.md "wikilink")
/
[帕金徵群](../Page/帕金森氏症.md "wikilink")[癡呆綜合症](../Page/失智症.md "wikilink")」（*ALS/PDC*）\[1\]的可能成因，在[關島的](../Page/關島.md "wikilink")[查莫羅人罹患這症狀的機率十分高](../Page/查莫羅人.md "wikilink")\[2\]，當地人稱之*Lytico-boding*，「Lytico」意指肌肉萎縮，「Boding」則是肌肉僵硬的意思。\[3\][香港中文大學](../Page/香港中文大學.md "wikilink")[生物化學系副教授](../Page/生物化學.md "wikilink")[陳竟明指](../Page/陳竟明.md "wikilink")，人體會從食物吸收[蛋白胺基酸](../Page/蛋白胺基酸.md "wikilink")，代謝物會被排出體外，但神經細胞可能未能識別胺基酸的代謝物BMAA，遺留體內而損害神經系統。\[4\]
但中大腦神經科主任黃家星指，尚未確定BMAA對人類有害，難以估計安全的食用量。\[5\]

### BMAA的來源

1950年代，在[羅塔島和](../Page/羅塔島.md "wikilink")[關島](../Page/關島.md "wikilink")[查莫羅人](../Page/查莫羅人.md "wikilink")，ALS/PDC的患病率和[死亡率](../Page/死亡.md "wikilink")，皆高於[已發展國家的](../Page/已發展國家.md "wikilink")50至100倍，包括[美國](../Page/美國.md "wikilink")。當時亦不能確實証明，是[遺傳和](../Page/遺傳.md "wikilink")[病毒引致居物發病](../Page/病毒.md "wikilink")，但1955年後，關島患ALS/PDC的比率卻不明地減少，這引起學者和環保組織的關注和調查。研究發現[查莫羅人以](../Page/查莫羅人.md "wikilink")[蘇鐵科植物種子來製造傳統藥物](../Page/蘇鐵科.md "wikilink")，但因為[第二次世界大戰後](../Page/第二次世界大戰.md "wikilink")，關島由美國統治，引進現代化的醫療藥物，令[查莫羅人降低對傳統藥物的依賴](../Page/查莫羅人.md "wikilink")，間接使居民減少吸收[蘇鐵種子中的BMAA](../Page/蘇鐵.md "wikilink")。\[6\]

其後的研究指，BMAA能夠透過[生物放大作用累積](../Page/生物放大作用.md "wikilink")，被人體大量吸收。因為[查莫羅人有食用](../Page/查莫羅人.md "wikilink")[狐蝠的](../Page/狐蝠科.md "wikilink")[風俗](../Page/風俗.md "wikilink")，而狐蝠則以蘇鐵種子為主要食糧，故BMAA大量累積在狐蝠體內，食用至一定份量便會產生毒性。1950年代在關島收集的蝙蝠樣本顯示，蝙蝠體內含有的BMAA，比每克的蘇鐵[種子高出數百倍](../Page/種子.md "wikilink")。\[7\]

已有研究指出，[鯊魚翅中含有高含量的BMAA](../Page/魚翅.md "wikilink")。\[8\]其製品魚翅羹及軟骨膠囊恐對健康造成威脅。
\[9\]

BMAA目前可藉由已知檢驗法檢測出，包括：液相色譜法、高效液相色譜法、氨基酸分析、毛細管電泳、核磁共振光譜法。\[10\]

### 神經毒性效應

一些動物食用[蘇鐵科植物而出現的機能退化](../Page/蘇鐵.md "wikilink")，令植物和ALS/PDC病源的可能關聯得以肯定，其後終在[實驗室發現β](../Page/實驗室.md "wikilink")-甲氨基-L-丙氨酸的存在。而β-甲氨基-L-丙氨酸就在[恆河猴身上產生強烈的毒性](../Page/恆河猴.md "wikilink")，症狀包括\[11\]：

  - 四肢肌肉[萎縮](../Page/萎縮.md "wikilink")。
  - [脊髓的](../Page/脊髓.md "wikilink")[前角細胞產生非反應性退化](../Page/前角.md "wikilink")。
  - [大腦](../Page/大腦.md "wikilink")[皮質和](../Page/皮質.md "wikilink")[錐體細胞](../Page/錐體細胞.md "wikilink")（*pyramidal
    neuron*）退化或流失。
  - 皮質的貝茲細胞（*Betz cell*）產生神經病理學上的異變。
  - 中樞傳導路徑（*central motor pathway*）的傳導能力不足。
  - 行為機能障礙。

亦有研究將老鼠脊髓抽出培植，發現小量β-甲氨基-L-丙氨酸積累能選擇性殺死老鼠的神經元。β-甲氨基-L-丙氨酸活化「AMP-KA[谷氨酸受體](../Page/谷氨酸.md "wikilink")」（AMPA-Kainate
glutamate Receptor）並加速體內生產[自由基](../Page/自由基.md "wikilink")。\[12\]

## 國際關注

國際間對食用[藍藻門生物開始關注](../Page/藍藻門.md "wikilink")，因為不論土生和水生的藍藻生物皆含有的BMAA，並可能透過[食物鏈不斷累積產生](../Page/食物鏈.md "wikilink")[生物放大作用](../Page/生物放大作用.md "wikilink")，對人類的損害將逐漸增加。香港中文大學亦呼籲大眾停止食用同屬藍藻門的[髮菜](../Page/髮菜.md "wikilink")，減輕患上[柏金遜症和](../Page/柏金遜症.md "wikilink")[老人癡呆症的風險](../Page/老人癡呆症.md "wikilink")。\[13\]\[14\]

## 相關條目

  - [肌肉萎縮性側索硬化症](../Page/肌肉萎縮性側索硬化症.md "wikilink")

## 參考

<div class="references-small">

<references />

</div>

[分類:α-胺基酸](../Page/分類:α-胺基酸.md "wikilink")

1.  注：ALS/PDC 的英文全名是「*Amyotrophic lateral sclerosis / Parkinsonism
    dementia complex of Guam*」

2.  Cox PA, Banack SA, Murch SJ, Rasmussen U, Tien G, Bidigare RR,
    Metcalf JS, Morrison LF, Codd GA, Bergman B. Diverse taxa of
    cyanobacteria produce β-N-methylamino-L-alanine, a neurotoxic amino
    acid. *Proceedings of the National Academy of Sciences of the United
    States of America* **2005**, *102*, 5074-5078.
    [1](http://dx.doi.org/10.1073/pnas.0501526102)

3.  Greg Miller. Guam's Deadly Stalker: On the Loose Worldwide?
    *Science* **July 2006**, *28* (313), 428-431.
    [2](http://dx.doi.org/10.1126/science.313.5786.428)

4.  [髮菜無益 調查指三成假貨真貨含神經毒素,
    香港明報, 30/1/07](http://www.mingpaonews.com/20070130/gba1.htm)

5.  *中大引述文獻：食髮菜會癡呆*, 香港《[蘋果日報](../Page/蘋果日報.md "wikilink")》,30/1/07

6.  PS Spencer, PB Nunn, J Hugon, AC Ludolph, SM Ross, DN Roy, and RC
    Robertson. Guam amyotrophic lateral sclerosis-parkinsonism-dementia
    linked to a plant excitant neurotoxin. *Science* **July 1987**, *31*
    (237), 517-522.

7.
8.  [3](http://www.mdpi.com/1660-3397/10/2/509)

9.  [4](http://www.sciencedaily.com/releases/2012/02/120223182516.htm)

10. [5](https://dx.doi.org/10.1039%2Fc2an16250d)

11.
12.
13.
14.