**卡尔·卡斯滕斯**（****，）不來梅人，[德国](../Page/德国.md "wikilink")[政治家](../Page/政治家.md "wikilink")，[德国基督教民主联盟人](../Page/德国基督教民主联盟.md "wikilink")。

年輕時曾參與[納粹黨](../Page/納粹黨.md "wikilink")，1976年至1979年任[德国联邦议院议长](../Page/德国联邦议院.md "wikilink")，1979年至1984年任[德国联邦总统](../Page/德国联邦总统.md "wikilink")。

[Category:德国联邦总统](../Category/德国联邦总统.md "wikilink")
[Category:德國聯邦議院議長](../Category/德國聯邦議院議長.md "wikilink")
[Category:纳粹党党员](../Category/纳粹党党员.md "wikilink")
[Category:耶魯大學校友](../Category/耶魯大學校友.md "wikilink")
[Category:法蘭克福大學校友](../Category/法蘭克福大學校友.md "wikilink")
[Category:慕尼黑大學校友](../Category/慕尼黑大學校友.md "wikilink")
[Category:柯尼斯堡大學校友](../Category/柯尼斯堡大學校友.md "wikilink")
[Category:漢堡大學校友](../Category/漢堡大學校友.md "wikilink")
[Category:不來梅人](../Category/不來梅人.md "wikilink")
[Category:查理曼獎得主](../Category/查理曼獎得主.md "wikilink")