**许衡**（1209年5月8日-1281年3月23日），
字**仲平**，又称**鲁斋先生**，[怀州](../Page/怀州.md "wikilink")[河内](../Page/河内.md "wikilink")（今[河南](../Page/河南.md "wikilink")[沁阳](../Page/沁阳.md "wikilink")）人，[元代](../Page/元代.md "wikilink")[理学家](../Page/理学.md "wikilink")、[教育家](../Page/教育.md "wikilink")。

## 生平

许衡生于[金](../Page/金国.md "wikilink")[卫绍王大安元年](../Page/卫绍王.md "wikilink")（[宋宁宗嘉定二年](../Page/宋宁宗.md "wikilink")、[元太祖四年](../Page/元太祖.md "wikilink")）九月丙寅\[1\]。十六岁时，下决心求学，一心研究[儒家经典](../Page/儒家.md "wikilink")。

### 仕元

1232年，[蒙古军攻破金国](../Page/蒙古.md "wikilink")[新郑](../Page/新郑.md "wikilink")，许衡被俘，但是后来获释。6年后[科举中选](../Page/科举.md "wikilink")，遂以教学为业，与当时著名的[隐士](../Page/隐士.md "wikilink")[窦默一起谈论学问](../Page/窦默.md "wikilink")。1242年，许衡得[程颐的](../Page/程颐.md "wikilink")《[易传](../Page/易传.md "wikilink")》、[朱熹的](../Page/朱熹.md "wikilink")《[四书章句集注](../Page/四书章句集注.md "wikilink")》、《[小学](../Page/小学.md "wikilink")》等书，从此以此传授门徒。1254年，[忽必烈置](../Page/忽必烈.md "wikilink")[宣抚司](../Page/宣抚司.md "wikilink")，以许衡为[京兆](../Page/京兆.md "wikilink")[教授](../Page/教授.md "wikilink")。1258年还居河内。[中统元年](../Page/中统.md "wikilink")（1260），元世祖忽必烈即位，遂召许衡北上。次年，官[国子](../Page/国子.md "wikilink")[祭酒](../Page/祭酒.md "wikilink")，不久辞职还乡。中统三年，复入朝，但因病[燕京](../Page/燕京.md "wikilink")，[至元元年](../Page/至元.md "wikilink")（1264）归乡。二年，忽必烈再召，许衡奉命即赴，四年，告病还，不久复召入。七年，官[中书左丞](../Page/中书左丞.md "wikilink")。许衡劾[阿合马专权](../Page/阿合马.md "wikilink")，世祖不听，于是又请求解职。八年，官[集贤大学士兼国子祭酒](../Page/集贤大学士.md "wikilink")，置[国子学](../Page/国子学.md "wikilink")。许衡致力教学，用[小学](../Page/小学.md "wikilink")、[四书](../Page/四书.md "wikilink")，及所著《[大学直解](../Page/大学直解.md "wikilink")》、《[中庸直解](../Page/中庸直解.md "wikilink")》、《[大学要略](../Page/大学要略.md "wikilink")》、《[编年歌括](../Page/编年歌括.md "wikilink")》、《[稽古千字文](../Page/稽古千字文.md "wikilink")》等篇作教材。十年，许衡辞职归怀州。十三年，再召至[大都](../Page/大都.md "wikilink")，命与[王恂](../Page/王恂.md "wikilink")、[郭守敬等人商定历法](../Page/郭守敬.md "wikilink")。十七年致仕还乡，十八年三月初三（1281年3月23日）去世。

## 影响

许衡五进五-{出}-，但从未被重用过。他的主要业绩是奠定元朝国子学基础和阐扬程朱学说。所以元代有不少人推崇他是朱熹的继承者。他的著作收在《[鲁斋遗书](../Page/鲁斋遗书.md "wikilink")》中。今人有王成儒點校本《[許衡集](../Page/許衡集.md "wikilink")》，東方出版社2007年出版。

## 参考文献

[H](../Page/category:元朝政治人物.md "wikilink")
[H](../Page/category:1209年出生.md "wikilink")
[H](../Page/category:1281年逝世.md "wikilink")

[H](../Category/元朝儒學學者.md "wikilink")
[X](../Category/中国思想家.md "wikilink")
[Category:中國理學家](../Category/中國理學家.md "wikilink")
[Category:西廡先賢先儒](../Category/西廡先賢先儒.md "wikilink")
[Category:许姓](../Category/许姓.md "wikilink")

1.  《许鲁斋先生年谱》(清)郑士范编