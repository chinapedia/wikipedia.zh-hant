**細So**（，），本名**蘇耀宗**，[香港商業電台](../Page/香港商業電台.md "wikilink")[唱片騎師](../Page/唱片騎師.md "wikilink")、[素食主義者](../Page/素食主義者.md "wikilink")。

## 簡歷

細So早年就讀[番禺會所華仁小學和](../Page/番禺會所華仁小學.md "wikilink")[香港華仁書院](../Page/香港華仁書院.md "wikilink")\[1\]，畢業於香港理工大學\[2\]。他是2009年[飢饉三十饑饉之星](../Page/飢饉三十.md "wikilink")，其他分別是[張敬軒和](../Page/張敬軒.md "wikilink")[蔡卓妍](../Page/蔡卓妍.md "wikilink")；2014年[飢饉三十饑饉之星](../Page/飢饉三十.md "wikilink")，其他的有[RubberBand](../Page/RubberBand.md "wikilink")\[3\]\[4\]。他亦是商台節目《[森美移動](../Page/森美移動.md "wikilink")》成員之一，當中成員包括[森美](../Page/森美.md "wikilink")、[詹志文及](../Page/詹志文.md "wikilink")[陳強](../Page/陳強.md "wikilink")，另外，他曾擔任單元廣播劇《[黃金少年](../Page/黃金少年.md "wikilink")
Season 1, 3,
4》的製作及[監製](../Page/監製.md "wikilink")。除了連續8年參與[森美小儀歌劇團外](../Page/森美小儀歌劇團.md "wikilink")，他也有參與其他舞台劇。

## 感情生活

2006年8月6日與[商業電台](../Page/商業電台.md "wikilink")[叱吒903](../Page/叱吒903.md "wikilink")
DJ同事[張詠妍](../Page/張詠妍.md "wikilink")（阿Wing）結婚。2009年年初，因性格不合，兩人低調協議離婚，女方翌年嫁給商台高層[林若寧](../Page/林若寧.md "wikilink")。2011年11月1日，細so與旅遊記者[陳穎楠結婚](../Page/陳穎楠.md "wikilink")。2013年9月7日，其妻於[聖保祿醫院誕下](../Page/聖保祿醫院.md "wikilink")8磅重兒子蘇浪仁。2016年7月9日則再剖腹誕下次子蘇㟍仁\[5\]。

## 演出作品

### 現主持電台節目

  - 《[生活日常](../Page/生活日常.md "wikilink")》 （逢星期一至五晚上7時至9時）
  - 《[好醒晨](../Page/好醒晨.md "wikilink")》（逢星期六早上6時至8時）
  - 《[突然好想聽](../Page/突然好想聽.md "wikilink")》（逢星期日晚上7時至9時，暫代[黃君慧](../Page/黃君慧.md "wikilink")（Vani）《國語類》節目）

### 曾主持電台節目

  - 《[兄弟做野](../Page/兄弟做野.md "wikilink")》
  - 《[搜尋音樂](../Page/搜尋音樂.md "wikilink")》
  - 《[gege獸](../Page/gege獸.md "wikilink")》
  - 《[MOTO 903 新樂潮](../Page/MOTO_903_新樂潮.md "wikilink")》 （逢星期五黃昏6時至6時半）
  - 《[大鐵人123](../Page/大鐵人123.md "wikilink")》
  - 《[903 豁達推介](../Page/903_豁達推介.md "wikilink")》
  - 《[好明天](../Page/好明天.md "wikilink")》
  - 《[蘇耀一個鐘](../Page/蘇耀一個鐘.md "wikilink")》
  - 《[蘇耀兩個鐘](../Page/蘇耀兩個鐘.md "wikilink")》
  - 《[宗慇同學會](../Page/宗慇同學會.md "wikilink")》
  - 《[五天精華遊](../Page/五天精華遊.md "wikilink")》
  - 《[森美移動](../Page/森美移動.md "wikilink")》
    （逢星期六晚上11時至凌晨1時，與[森美](../Page/森美.md "wikilink")、[詹志民及](../Page/詹志民.md "wikilink")[陳強主持節目](../Page/陳強.md "wikilink")）
  - 《[組黨私竇](../Page/組黨私竇.md "wikilink")》 （逢星期日黃昏6時至8時）
  - 《[903 爆籃週末](../Page/903_爆籃週末.md "wikilink")》
  - 《[朋咤咤](../Page/朋咤咤.md "wikilink")》
  - 《[903 idclub到處流行](../Page/903_idclub到處流行.md "wikilink")》
  - 《[好回家](../Page/好回家.md "wikilink")》 （逢星期一至五黃昏6時至8時）
  - 《[903專業推介](../Page/903專業推介.md "wikilink")》（逢星期六上午11時至下午1時）
  - 《[903豁達推介](../Page/903豁達推介.md "wikilink")》（逢星期六下午1時至2時）
  - 《[好出奇](../Page/好出奇.md "wikilink")》（逢星期一至五下午4時至6時，與[朱薰及](../Page/朱薰.md "wikilink")[孔慶慇](../Page/孔慶慇.md "wikilink")（Marco）主持節目）

### 曾主持電視台節目

  - 《超低能特攻隊》（[ViuTV](../Page/ViuTV.md "wikilink")，與[羅伊婷共同主持](../Page/羅伊婷.md "wikilink")）
  - 《[晚吹](../Page/晚吹.md "wikilink")-[Daddy
    Kingdom](../Page/Daddy_Kingdom.md "wikilink")》（ViuTV）
  - 《[飯聚現場](../Page/飯聚現場.md "wikilink")》（ViuTV）
  - 《大同·大不同》（ViuTV）

### 曾參與廣播劇

  - 《I Love You Boyz - 時裝世界》
  - 《I Love You Boyz - 大瘋人院》
  - 《[黃金少年](../Page/黃金少年.md "wikilink")》Season1、2、3、4（飾演：蘇耶 - 萻提俠）
  - 《[妹妹妹妹實況劇場](../Page/妹妹妹妹_#妹妹妹妹實況劇場.md "wikilink")》（飾演：細so）
  - 《爆炸糖劇場》（飾演：石叔）
  - 《[街車王](../Page/街車王.md "wikilink")》
  - 《[大衛營](../Page/大衛營.md "wikilink")》
  - 《[大鄉里](../Page/大鄉里.md "wikilink")》
  - 《[爆籃無敵](../Page/爆籃無敵.md "wikilink")》
  - 《[I Love You
    Boyz劇場](../Page/I_Love_You_Boyz.md "wikilink")》（飾演：司徒歐陽司徒太太及司徒歐陽司徒先生）
  - 《[大瘋人院](../Page/大瘋人院.md "wikilink")》
  - 《[小火花](../Page/小火花.md "wikilink")》
  - 《[地板對我說](../Page/地板對我說.md "wikilink")》(客串)
  - 《[最好的時光-第一章](../Page/最好的時光-第一章.md "wikilink")》，《[最好的時光-第三章](../Page/最好的時光-第三章.md "wikilink")》（飾演：蘇SIR）
  - 《[森美移動](../Page/森美移動.md "wikilink")[音樂機械特勤](../Page/音樂機械特勤.md "wikilink")》（飾演：細So）（逢星期六[森美移動時段內播放](../Page/森美移動.md "wikilink")，通常於11點半至12點播放）
  - 《[最好的時光-第二章](../Page/最好的時光-第二章.md "wikilink")》（監製）
  - 《[3厘米](../Page/3厘米.md "wikilink")》（聲演：李若信）（故事）（製作）
  - 《[森美幻想劇](../Page/森美幻想劇.md "wikilink")》（逢星期六[森美移動時段內播放](../Page/森美移動.md "wikilink")，時間不定，通常於[森美移動中段播放](../Page/森美移動.md "wikilink")，晚上11點半至凌晨12點）
  - 《[見習朋友計劃](../Page/毒檸王國#2016冬季限定廣播劇：《見習朋友計劃》.md "wikilink")》（飾演：友達計劃企劃人）（2016年11月28日至2016年12月30日，逢星期一及星期五[毒檸王國時段內播放](../Page/毒檸王國.md "wikilink")）

## 音樂作品

### 專輯

  - 2007年：《[Don't Be
    Scared](../Page/Don't_Be_Scared.md "wikilink")》（[森美移動](../Page/森美移動.md "wikilink")）

### 作曲

  - 月球上的人（[陳奕迅](../Page/陳奕迅.md "wikilink")）
  - 兒歌（[容祖兒](../Page/容祖兒.md "wikilink")）
  - 差不多先生（[EO2](../Page/EO2.md "wikilink")）
  - 給愛惜的人（[吳浩康](../Page/吳浩康.md "wikilink")）
  - 一天（EO2）
  - 叮叮車（[薛凱琪](../Page/薛凱琪.md "wikilink")）
  - 戀後感（[古巨基](../Page/古巨基.md "wikilink")）
  - 惜愛（[張敬軒](../Page/張敬軒.md "wikilink")）
  - 完美口味（[洪卓立](../Page/洪卓立.md "wikilink")）
  - 我想真的愛你（[許志安](../Page/許志安.md "wikilink")）
  - 冷笑話（[薛凱琪](../Page/薛凱琪.md "wikilink")）
  - It's All About Timing（[陳慧琳](../Page/陳慧琳.md "wikilink")）
  - 假裝愉快（[張紋嘉](../Page/張紋嘉.md "wikilink")）
  - 緊張你（[洪卓立](../Page/洪卓立.md "wikilink")）
  - \-{拼}-命虛耗（[J.Arie](../Page/J.Arie.md "wikilink")）
  - 惺惺相惜（[C AllStar](../Page/C_AllStar.md "wikilink") Feat.
    [許志安](../Page/許志安.md "wikilink")）
  - 你是你（[許志安](../Page/許志安.md "wikilink")）
  - 逆風速遞（[林海峰](../Page/林海峰_\(香港\).md "wikilink")）
  - 五十路（[李克勤](../Page/李克勤.md "wikilink")）

### 作詞

  - 差利先生（[Yellow\!](../Page/Yellow!.md "wikilink")）
  - 1st date（[Gin Lee](../Page/Gin_Lee.md "wikilink")）
  - 芭比說（[石詠莉](../Page/石詠莉.md "wikilink")）
  - Killer（[Gin Lee](../Page/Gin_Lee.md "wikilink")）
  - 回去哪要時光機器（[周國賢](../Page/周國賢.md "wikilink")）
  - 溫馨提示（[官恩娜](../Page/官恩娜.md "wikilink")）
  - 我只不過是我（[江若琳](../Page/江若琳.md "wikilink")）
  - 嫌小姐（[Yellow\!](../Page/Yellow!.md "wikilink")）

## 其他工作

  - 廣告
      - 與[周麗淇擔任體育用品平面廣告主角](../Page/周麗淇.md "wikilink")。
  - 電影
      - [我要做Model](../Page/我要做Model.md "wikilink") 飾 模特兒公司職員
      - [算吧啦，老豆\!](../Page/算吧啦，老豆!.md "wikilink") 飾 聶人王
      - [等.我爱你\!](../Page/等.我爱你!.md "wikilink") 飾 施彥祖（媽池）
  - 舞台劇
      - [BigBigWorld大世界](../Page/BigBigWorld大世界.md "wikilink") 飾
        偉業（2009年4月公演）
      - [跳飛機](../Page/跳飛機_\(舞台劇\).md "wikilink") Hopsotch
        參演及參與創作（2009年3月於[葵青劇院黑盒劇場公演](../Page/葵青劇院.md "wikilink")）
      - [拾香紀](../Page/拾香紀.md "wikilink") 飾 四海
      - [勁金歌曲](../Page/勁金歌曲.md "wikilink") 飾 八十年代電台DJ蘇耀宗（首演，重演及第3度公演）
      - [黐膠花園](../Page/黐膠花園.md "wikilink")
      - [親愛的,維多利亞](../Page/親愛的,維多利亞.md "wikilink")
      - [森美小儀歌劇團](../Page/森美小儀歌劇團.md "wikilink")
  - 電視演出
      - 《[森之愛情](../Page/森之愛情.md "wikilink")》第九集《十五年的約會》飾 細SO
  - 其他
      - [Love Union](../Page/Love_Union.md "wikilink")
      - 2009[飢饉三十](../Page/飢饉三十.md "wikilink")——饑饉之星
      - 2014[飢饉三十](../Page/飢饉三十.md "wikilink")——饑饉之星

## 參考文獻

## 外部連結

  - [商業電台主持簡介－細So](http://www.881903.com/Page/ZH-TW/DJ903_22.aspx)

  -
  -
[Category:蘇姓](../Category/蘇姓.md "wikilink")
[Category:香港廣播主持人](../Category/香港廣播主持人.md "wikilink")
[Category:香港作曲家](../Category/香港作曲家.md "wikilink")
[Category:香港填詞人](../Category/香港填詞人.md "wikilink")
[S](../Category/香港華仁書院校友.md "wikilink")
[S](../Category/香港理工大學校友.md "wikilink")
[Category:香港素食主義者](../Category/香港素食主義者.md "wikilink")
[Category:前ViuTV電視藝員‎](../Category/前ViuTV電視藝員‎.md "wikilink")

1.  [我是誰
    靠森美小儀響朵](http://hk.apple.nextmedia.com/entertainment/art/20110219/14989796)，[蘋果日報](../Page/蘋果日報_\(香港\).md "wikilink")，2011年2月19日

2.
3.  [第17屆校園記者·非洲體會 RubberBand：開飯不簡單 津國人尊重食物
    一口一感恩](https://web.archive.org/web/20171106160930/http://studentreporter.mingpao.com/cfm/com_OldContent2.cfm?Category=activitystar&Content_ID=1780&Year=17)，[明報校園記者網](../Page/明報.md "wikilink")，2014年

4.  [阿Sa割草餵牛變村姑](http://orientaldaily.on.cc/archive/20090207/ent/ent_a15cnt.html)，[東方日報](../Page/東方日報_\(香港\).md "wikilink")，2009年2月7日

5.  [DJ細So 搬去離島避
    傳統教育](http://www.sundaykiss.com/magazine/sunday-family/dj%e7%b4%b0so-%e6%90%ac%e5%8e%bb%e9%9b%a2%e5%b3%b6%e9%81%bf%e5%82%b3%e7%b5%b1%e6%95%99%e8%82%b2/)，[Sundaykiss](../Page/Sundaykiss.md "wikilink")，2016年11月15日