愛爾蘭皇家外科醫學院，英文名Royal College of Surgeons in
Ireland，簡稱RCSI。成立於1784年，是所私立醫學院。愛爾蘭皇家外科醫學院位於[愛爾蘭的](../Page/愛爾蘭.md "wikilink")[都柏林市](../Page/都柏林.md "wikilink")。由於[愛爾蘭共和國曾經是](../Page/愛爾蘭共和國.md "wikilink")[大英帝國的一部份](../Page/大英帝國.md "wikilink")，名稱上還保有【皇家】二字。愛爾蘭皇家外科醫學院下分為[醫學部](../Page/醫學.md "wikilink")，[物理治療部和](../Page/物理治療.md "wikilink")[藥學部](../Page/藥學.md "wikilink")。物理治療學士課程為期三年。[藥學部學士課程為期四年](../Page/藥學.md "wikilink")。醫學部分為學士前和學士後入學。[高中畢業者可依學士前申請辦法辦理入學申請](../Page/高中.md "wikilink")，為期五至六年。[大學畢業者可依學士後入學申請辦法辦理入學申請](../Page/大學.md "wikilink")，為期四至五年。愛爾蘭皇家外科醫學院醫學部的[美國大學畢業申請者是以](../Page/美國.md "wikilink")[美國醫學院入學考試](../Page/美國醫學院入學考試.md "wikilink")【MCAT】，[大學成績及教授](../Page/大學成績.md "wikilink")[推薦函為入學審查標準](../Page/推薦函.md "wikilink")。再類似的情況下，[澳洲大學畢業申請者是以](../Page/澳洲.md "wikilink")[澳洲學士後醫學院入學考試](../Page/澳洲學士後醫學院入學考試.md "wikilink")【GAMSAT】及[大學成績為入學審查標準](../Page/大學成績.md "wikilink")。愛爾蘭共和國屬英語系國家。愛爾蘭皇家外科醫學院授課語言為英語。

## 創辦歷史

在醫學院1784年創辦之前，[愛爾蘭的醫學教育及醫療監督管理是由](../Page/愛爾蘭.md "wikilink")[理髮醫師公會](../Page/理髮.md "wikilink")【Barber
Surgeons'
Guild】負責。在十八世紀的[歐洲](../Page/歐洲.md "wikilink")，醫療及衛生體系常在沒有細分的情況下與理髮公會，[牙醫公會統籌管理](../Page/牙醫.md "wikilink")。由於[工業革命的影響](../Page/工業革命.md "wikilink")，使得[醫學上有明顯的進步](../Page/醫學.md "wikilink")。這也導致西方社會逐漸把醫學以更專門的學科來加以歸類。在1784年的二月十一日，愛爾蘭皇家外科醫學院在[都柏林市的羅頓達醫院宣布成立並同時爭取到了大英帝國皇家](../Page/都柏林.md "wikilink")[特許狀](../Page/特許狀.md "wikilink")。成立之初，愛爾蘭皇家外科醫學院並不是一所醫學教育機構，而是類似於[公會一樣的組織](../Page/公會.md "wikilink")。皇家特許狀於1844年由[英女皇維多利亞擴增其權限並把原有的畢業醫學生二分為醫學士及醫學研究員](../Page/维多利亚女王.md "wikilink")。在這之後，歷經了[愛爾蘭自由邦與](../Page/愛爾蘭自由邦.md "wikilink")[愛爾蘭共和國的成立醫學院才慢慢的衍生成為一個](../Page/愛爾蘭共和國.md "wikilink")[私立的醫學教育機構](../Page/私立.md "wikilink")。自1980年代以來RCSI醫學訓練的中心是一直以貝歐蒙特醫院為主。

## 國際醫學上的合作及貢獻

愛爾蘭皇家外科醫學院除了在學員的錄取上主張多國籍地球村的概念之外，在世界各地也都有設立常駐據點。同時，早在[南非還實行](../Page/南非.md "wikilink")[種族隔離政策時](../Page/南非种族隔离制度.md "wikilink")，醫學院就已開始幫助[弱勢族群接受醫學教育](../Page/弱勢族群.md "wikilink")。愛爾蘭皇家外科醫學院[阿聯酋的](../Page/阿拉伯联合酋长国.md "wikilink")[杜拜醫學大學於](../Page/杜拜.md "wikilink")2005年時成立，目前並有醫療管理的碩士課程。在[巴林方面](../Page/巴林.md "wikilink")，愛爾蘭皇家外科醫學院也致力於醫學教育。[巴林的RCSI醫學大學並在](../Page/巴林.md "wikilink")2004年10月開始營運招生。愛爾蘭皇家外科醫學院與世界其他醫學院也有深入的學術聯繫。[馬來西亞](../Page/馬來西亞.md "wikilink")[檳城醫學院學生可以選擇性的到愛爾蘭皇家外科醫學院選修基本醫學課程](../Page/檳城醫學院.md "wikilink")。在實習部分，愛爾蘭皇家外科醫學院有與其他世界各地大學合作，包括[美國](../Page/美國.md "wikilink")[哥倫比亞大學](../Page/哥倫比亞大學.md "wikilink")，[賓州大學及](../Page/賓州大學.md "wikilink")[塔夫次大學](../Page/塔夫次大學.md "wikilink")。非正式性的交流也包含了[約翰·霍普金斯大學及](../Page/约翰斯·霍普金斯大学.md "wikilink")[馬優醫學院](../Page/馬優醫學院.md "wikilink")。

## 榮譽會員

  - [法國](../Page/法國.md "wikilink")[微生物學家](../Page/微生物.md "wikilink")[路易·巴斯德博士](../Page/路易·巴斯德.md "wikilink")
    (1886)
  - [美國](../Page/美國.md "wikilink")[明尼蘇達州](../Page/明尼蘇達州.md "wikilink")[馬優醫院創辦人威廉馬優及查爾斯馬優兄弟](../Page/馬優醫院.md "wikilink")
    (1921)
  - [沙烏地阿拉伯](../Page/沙烏地阿拉伯.md "wikilink")[阿卜杜拉·本·阿卜杜勒·阿齊茲親王](../Page/阿卜杜拉·本·阿卜杜勒·阿齊茲.md "wikilink")
    (1988)
  - [馬來西亞前最高](../Page/馬來西亞.md "wikilink")[元首](../Page/元首.md "wikilink")[蘇丹](../Page/苏丹_\(称谓\).md "wikilink")[拉賈·圖恩·阿茲蘭·沙阿](../Page/拉賈·圖恩·阿茲蘭·沙阿.md "wikilink")
    (1991)
  - [馬來西亞前首相](../Page/馬來西亞.md "wikilink")[馬哈迪](../Page/馬哈迪.md "wikilink")
    (1991)
  - [愛爾蘭前](../Page/愛爾蘭.md "wikilink")[總統](../Page/總統.md "wikilink")[瑪麗·羅賓遜](../Page/瑪麗·羅賓遜.md "wikilink")
    (1994)
  - [巴林前最高](../Page/巴林.md "wikilink")[元首](../Page/元首.md "wikilink")[蘇爾曼·佳里發](../Page/蘇爾曼·佳里發.md "wikilink")
    (1995)
  - [印度](../Page/印度.md "wikilink")[諾貝爾和平獎得主](../Page/諾貝爾.md "wikilink")[德蕾莎修女](../Page/德蕾莎修女.md "wikilink")
    (1995)
  - [南非前](../Page/南非.md "wikilink")[總統](../Page/總統.md "wikilink")[曼德拉](../Page/曼德拉.md "wikilink")
    (1996)
  - [英國外科](../Page/英國.md "wikilink")[教授](../Page/教授.md "wikilink")[亞福列得·庫協力爵士](../Page/亞福列得·庫協力.md "wikilink")
    (1996)
  - [英國](../Page/英國.md "wikilink")[諾貝爾文學獎得主](../Page/諾貝爾.md "wikilink")[西馬斯·韓力](../Page/西馬斯·韓力.md "wikilink")[教授](../Page/教授.md "wikilink")
    (1998)
  - [愛爾蘭](../Page/愛爾蘭.md "wikilink")[總統](../Page/總統.md "wikilink")[瑪麗·麥亞烈斯](../Page/瑪麗·麥亞烈斯.md "wikilink")
    (1998)
  - [馬來西亞前最高](../Page/馬來西亞.md "wikilink")[元首](../Page/元首.md "wikilink")[蘇丹沙拉胡丁](../Page/蘇丹沙拉胡丁.md "wikilink")
    (2000)
  - [阿聯前](../Page/阿聯.md "wikilink")[副總統](../Page/副總統.md "wikilink")[馬可通](../Page/馬可通.md "wikilink")
    (2004)

## 外部链接

  - [愛爾蘭皇家外科醫學院網址](http://www.rcsi.ie)
  - [貝歐蒙特醫院網址](http://www.beaumont.ie)

[Category:爱尔兰国立大学](../Category/爱尔兰国立大学.md "wikilink")
[Category:1780年代創建的教育機構](../Category/1780年代創建的教育機構.md "wikilink")
[Category:醫學院校](../Category/醫學院校.md "wikilink")