**有性生殖**是[生殖的一种类型](../Page/繁殖.md "wikilink")，它导致了[后代加强](../Page/后代.md "wikilink")[基因多样化](../Page/基因多样化.md "wikilink")。它可以用两个进程刻画。第一个是[减数分裂](../Page/减数分裂.md "wikilink")，涉及将[染色体个数减半](../Page/染色体.md "wikilink")。第二个是[受精](../Page/受精.md "wikilink")，这个过程中两个[配偶子融合](../Page/配偶子.md "wikilink")，并恢复原来的染色体个数。在减数分裂时，每对染色体通常[交叉以达到](../Page/染色体交叉.md "wikilink")[基因重组](../Page/基因重组.md "wikilink")。

[性的演变是现代](../Page/性的演变.md "wikilink")[演化生物学的重大谜团](../Page/演化生物学.md "wikilink")。最早的有性繁殖的[生物的](../Page/生物.md "wikilink")[化石证据是来自](../Page/化石.md "wikilink")[狭带纪的](../Page/狭带纪.md "wikilink")[真核细胞](../Page/真核细胞.md "wikilink")，距今约12到10亿年。有性生殖是绝大多数可见生命体的繁殖形式，包括几乎所有的[动物和](../Page/动物.md "wikilink")[植物](../Page/植物.md "wikilink")。[细菌接合](../Page/细菌接合.md "wikilink")（bacterial
conjugation），也就是两个[细菌之间的](../Page/细菌.md "wikilink")[DNA转移](../Page/DNA.md "wikilink")，有时被错误地视为有性生殖，因为机理其实很相似。

当代进化论观点提出了为何虽然[单性生殖在有些方面是更强的](../Page/单性生殖.md "wikilink")[生殖形式](../Page/生殖.md "wikilink")，但有性生殖依然持续存在的一些理由。有性生殖可能是因为在[进化树本身上的压力而保持下来](../Page/进化树.md "wikilink")
-
因为通过有性重组比单性繁殖更能产生适应变化的环境的分支，並有效處理[突變](../Page/突變.md "wikilink")\[1\]與寄生蟲\[2\]而将物种散布出去。或者，有性生殖可能像棘轮那样控制了进化发展的速度，因为一个进化枝会和另一个竞争有限的资源。

[Sexual_cycle.svg](https://zh.wikipedia.org/wiki/File:Sexual_cycle.svg "fig:Sexual_cycle.svg")

## 原生生物和真菌的有性生殖

很多[原生生物和單細胞的](../Page/原生生物.md "wikilink")[真菌會有性繁殖](../Page/真菌.md "wikilink")。虽然它们是单细胞，但在繁殖的时候，会出現“父细胞”和“母细胞”结合起来的現象。然后，它们的基因信息合成一个新的形式，接著透过[減數分裂产生了后代](../Page/減數分裂.md "wikilink")。

## 植物繁殖

在[开花植物中](../Page/开花植物.md "wikilink")，[雄蕊产生称为](../Page/雄蕊.md "wikilink")[花粉的配偶子](../Page/花粉.md "wikilink")，它附着于[雌蕊上](../Page/雌蕊.md "wikilink")，而雌蕊中有雌性配偶子（胚珠）。在这里，雌性配偶子受精，并发展成为种子。而产生了配偶子的子房则长成了一个[果实](../Page/果实.md "wikilink")，它包裹了种子。植物可以[自花授粉或者](../Page/自花授粉.md "wikilink")[异花授粉](../Page/异花授粉.md "wikilink")。

## 祖龙（爬行动物和鸟类）的繁殖

雄性和雌性鸟以及爬行动物都有[泄殖腔](../Page/泄殖腔.md "wikilink")，排出卵子，精子，和排泄物。交配是通过将泄殖腔的唇压在一起进行的，在此期间雄性将精子转入雌性体内。雌性将孕育了年轻生命的带[羊膜的蛋产下](../Page/羊膜.md "wikilink")。但是，有一些种类，包括多数[涉禽和](../Page/涉禽.md "wikilink")[鸵鸟](../Page/鸵鸟.md "wikilink")，有一个类似哺乳动物阴茎的[交接器官](../Page/交接器官.md "wikilink")。

## 哺乳动物的生殖

对于[胎生哺乳动物](../Page/胎生.md "wikilink")，[后代出生时为幼体](../Page/后代.md "wikilink")：带有[性器官的完整个体](../Page/性器官.md "wikilink")，但性器官功能还未健全。几个月或几年后，性器官发育成熟，个体开始[性成熟](../Page/性成熟.md "wikilink")。多数雌性哺乳动物只在特定周期[可受孕](../Page/可受孕.md "wikilink")，在那些时候它们称为"发情"。这是，个体已经可以交配。单独的雄性和雌性哺乳动物接触并开始[交配](../Page/交配.md "wikilink")。对于多数哺乳动物，雄性和雌性在成年生命中会交换性伴侣。

### 哺乳动物雄性

雄性生殖系统包含两个主要部分：[阴茎](../Page/阴茎.md "wikilink")，用于在[交配時將](../Page/交配.md "wikilink")[生殖细胞](../Page/生殖细胞.md "wikilink")（精子）导入[发情期的雌性生殖系統](../Page/发情周期.md "wikilink")，而[睾丸用于产生生殖细胞](../Page/睾丸.md "wikilink")（精子）。对于人类，两个器官都位于[腹腔以外](../Page/腹腔.md "wikilink")，但是对于其它动物，它们可以主要位于腹部（譬如，对于[狗](../Page/狗.md "wikilink")，阴茎在非交配时主要位于体内）。睾丸位于体外有利于精子的[温度调节](../Page/温度调节.md "wikilink")，因为它们需要特定的温度存活。

精子比卵子小，通常非常短命，这要求雄性从[性成熟至死亡前不停的生产精子](../Page/性成熟.md "wikilink")。它们是活动的并且通过[趋化性游动](../Page/趋化性.md "wikilink")。

### 哺乳动物雌性

雌性生殖系统同样包含两个主要部分：[阴道和](../Page/阴道.md "wikilink")[子宫](../Page/子宫.md "wikilink")，主要用于接受雄性在[交配](../Page/交配.md "wikilink")[射精时产生的精子](../Page/射精.md "wikilink")，而[卵巢](../Page/卵巢.md "wikilink")，产生雌性的[卵子](../Page/卵子.md "wikilink")。所有这些部分总是体内的。阴道通过[子宫颈连接于子宫](../Page/子宫颈.md "wikilink")，而子宫通过[输卵管连接到卵巢](../Page/输卵管.md "wikilink")。在特定时间段，卵巢排出卵子，通过输卵管到达子宫。

如果，在那个传输中，它遇到了精子，精子会穿入并和卵子结合完成[授精](../Page/授精.md "wikilink")。授精通常发生在输卵管，但也可以在子宫中发生。受精卵将自己植入子宫壁，然后就开始[胚形成和](../Page/胚形成.md "wikilink")[器官形成的过程](../Page/器官形成.md "wikilink")。当发展到可以在子宫外成活的阶段，子宫颈扩张，子宫的宫缩将胎儿退出产道，也就是阴道。

卵子比精子更大，通常在出生时就全部生成了。它们大部分时候是静态的，除了它们到子宫的传输，它们也包含了将来[受精卵和](../Page/受精卵.md "wikilink")[胚胎所需的营养](../Page/胚胎.md "wikilink")。在一个规则的时间段，[卵形成的进程将一个卵子变为成熟](../Page/卵形成.md "wikilink")，并送到连接着卵巢的输卵管以接受可能的授精。如果未受精，该卵子通过人类和[类人猿的](../Page/类人猿.md "wikilink")[月经周期排出体外或者在其它动物的](../Page/月经.md "wikilink")[动情周期中吸收](../Page/动情周期.md "wikilink")。

### 妊娠

妊娠，对于人类也就是怀孕，是胎儿通过在母体内有丝分裂生长的时间段。这段时间内，胎儿通过母体获得所有的[营养和带氧血液](../Page/营养.md "wikilink")，通过胎盘来过滤，胎盘通过[脐带连接到胎儿的](../Page/脐带.md "wikilink")[腹部](../Page/腹部.md "wikilink")。这个[养料的索取对于母体负担相当大](../Page/养料.md "wikilink")，她需要摄入高的多的[卡路里](../Page/卡路里.md "wikilink")。而且，某些[维生素和其它养料比平常需要程度高很多](../Page/维生素.md "wikilink")，经常导致异常的饮食习惯。妊娠的长度，称为[妊娠期](../Page/妊娠期.md "wikilink")，每个种类有很大不同，对于人类是38星期，对于[长颈鹿是](../Page/长颈鹿.md "wikilink")55－60星期，而对于[仓鼠则是](../Page/仓鼠.md "wikilink")16天。

### 分娩

一旦胎儿足够成熟，分娩过程的化学信号就开始了，它从子宫的收缩和子宫颈的扩张开始。胎儿下降到子宫颈，然后推入阴道，最后退出母体。新生儿通常在出生-{zh-hans:后;zh-hant:後}-立刻开始[呼吸](../Page/呼吸.md "wikilink")。不久之后，胎盘也排出。多数哺乳动物将它吃掉，因为它是用于抚育幼体的很好的蛋白质和其它重要养分的来源。连接到幼体的腹部的脐带最后会自行脱落。

### 单孔目动物

[单孔目动物现在只剩](../Page/单孔目动物.md "wikilink")5个物种，全部生活于[澳大利亚和](../Page/澳大利亚.md "wikilink")[新几内亚](../Page/新几内亚.md "wikilink")，它们是排卵的。它们有一个用于排泄和生殖的口称为[泄殖腔](../Page/泄殖腔.md "wikilink")。它们将蛋存于体内几个星期，提供养料，然后将他们产出，然後像鸟类一样孵化。不到两个星期，幼体孵化并爬入母体的袋子。和有袋动物相似，它需要喂养几个星期。

### 有袋动物

[有袋动物基本上以一般哺乳动物的相同方式生殖](../Page/有袋动物.md "wikilink")，但是他们的幼体在生长的相当早的阶段就分娩了。出生-{zh-hans:后;zh-hant:後}-[幼袋鼠爬入母体的袋中](../Page/幼袋鼠.md "wikilink")，并连到一个[乳头](../Page/乳头.md "wikilink")，从那里接受养料并完成发展到完全独立的动物的过程。

## 参看

  - [生物繁殖](../Page/生物繁殖.md "wikilink")
  - [單性生殖](../Page/單性生殖.md "wikilink")
  - [鳥](../Page/鳥.md "wikilink")
  - [開花植物](../Page/開花植物.md "wikilink")
  - [哺乳動物](../Page/哺乳動物.md "wikilink")
  - [爬行動物](../Page/爬行動物.md "wikilink")
  - [性器官](../Page/性器官.md "wikilink")
  - [性交](../Page/性交.md "wikilink")

## 注释

## 参考文獻

  - Pang， K。 "Certificate Biology: New Mastering Basic Concepts"，
    香港，2004年
  - [生殖生物学期刊](http://www。biolreprod。org/)， 2005年6月。

[Category:發育生物學](../Category/發育生物學.md "wikilink")
[Category:繁殖](../Category/繁殖.md "wikilink")
[Category:性學](../Category/性學.md "wikilink")
[有性生殖](../Category/有性生殖.md "wikilink")

1.  [穆勒認為這就要拜「性」所賜。性不但能把不同個體中好用的那部分基因聚集在一起傳下去，還能把差的基因也聚集在一起然後用自然選擇的力量一腳踢飛。](http://songshuhui.net/archives/42077.html)
2.  [性所創造的多樣性能夠抵擋寄生蟲](http://songshuhui.net/archives/42427.html)