[PAL-NTSC-SECAM.svg](https://zh.wikipedia.org/wiki/File:PAL-NTSC-SECAM.svg "fig:PAL-NTSC-SECAM.svg")
**SECAM制式**（），又称**塞康制**，意为"按顺序传送彩色与存储"，1966年由法国研制成功，它属于同时顺序制。

在[信号传输过程中](../Page/信号.md "wikilink")，[亮度信号每行传送](../Page/亮度.md "wikilink")，而两个[色差信号则逐行依次传送](../Page/色差.md "wikilink")，即用行错开传输时间的办法来避免同时传输时所产生的串色以及由其造成的彩色失真。

SECAM制式特点是不怕[干扰](../Page/干扰.md "wikilink")，彩色效果好，但[兼容性差](../Page/兼容性.md "wikilink")。

[帧频为每秒](../Page/帧频.md "wikilink")25帧，扫描线625行，[隔行扫描](../Page/隔行扫描.md "wikilink")，画面比例4:3，[分辨率](../Page/分辨率.md "wikilink")720×576。

采用SECAM制的国家主要为大部分[獨聯體國家](../Page/獨聯體.md "wikilink")（例：[俄罗斯](../Page/俄罗斯.md "wikilink")）、[法国](../Page/法国.md "wikilink")、[埃及以及非洲的一些法語系國家等等](../Page/埃及.md "wikilink")。

另外，有人暱稱[NTSC為](../Page/NTSC制式.md "wikilink")**N**ever **T**he **S**ame
**C**olor（不會重現一樣的色彩）、稱[PAL為](../Page/PAL制式.md "wikilink")**P**erfect
**A**t **L**ast（最終達到完美）、稱SECAM為**S**ystem **E**ssentially **C**ontrary
to **A**merican **M**ethod（本質上有別與美國的系統）或**S**hows **E**very **C**olor
**A**ll **M**urky（把每一個顏色顯示得模糊）。

## 使用SECAM制式的國家和地區

### 歐洲

  - ，於2011年停播，改以[DVB播出](../Page/DVB.md "wikilink")。

  -
  -
### 南美洲

  -
### 中北美洲

  -
  -
  -
### 亞洲

### 非洲

### 太平洋島嶼

  -
  - （包括[塔希提島](../Page/塔希提島.md "wikilink")）

  -
## 参考文献

## 外部链接

## 参见

  - [NTSC制式](../Page/NTSC制式.md "wikilink")
  - [PAL制式](../Page/PAL制式.md "wikilink")
  - [彩色電視廣播標準](../Page/彩色電視廣播標準.md "wikilink")

{{-}}

[Category:電視技術](../Category/電視技術.md "wikilink")
[Category:通信技术](../Category/通信技术.md "wikilink")