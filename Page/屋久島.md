**屋久島**（）是座位於[日本](../Page/日本.md "wikilink")[九州](../Page/九州_\(日本\).md "wikilink")[大隅半島南方](../Page/大隅半島.md "wikilink")60[公里的一個島嶼](../Page/公里.md "wikilink")，與鄰近的[種子島和](../Page/種子島.md "wikilink")[口永良部島同屬](../Page/口永良部島.md "wikilink")[大隅群島](../Page/大隅群島.md "wikilink")。島上目前約有14000位居民，行政區屬於[鹿兒島縣](../Page/鹿兒島縣.md "wikilink")[熊毛郡](../Page/熊毛郡_\(鹿兒島縣\).md "wikilink")[屋久島町](../Page/屋久島町.md "wikilink")。

## 特色

[Yakushima_Panorama.jpg](https://zh.wikipedia.org/wiki/File:Yakushima_Panorama.jpg "fig:Yakushima_Panorama.jpg")
[Senpiro_Fall_Yaku_Island.jpg](https://zh.wikipedia.org/wiki/File:Senpiro_Fall_Yaku_Island.jpg "fig:Senpiro_Fall_Yaku_Island.jpg")
屋久島[面積約為](../Page/面積.md "wikilink")504[平方公里](../Page/平方公里.md "wikilink")，位於[北緯](../Page/北緯.md "wikilink")30度。島的形狀屬於完整的圓菱形。是一個多山之島，山地占全島面積約75%，平均高度超過1000[公尺](../Page/公尺.md "wikilink")。氣候上，屋久島是以日本降雨最多的地方聞名，當地有一句俗語「一個月下35天的雨」，在日本降雨紀錄上，屋久島一天的降雨量曾與北海道全年的降雨量相同。

1993年，屋久島與[白神山地一起被列入](../Page/白神山地.md "wikilink")[世界遺產名冊](../Page/世界遺產.md "wikilink")，是日本最早列入世界遺產的自然景觀，但並非整個島都屬於世界遺產範圍，而是中央的[宮之浦岳和西邊的神木林區](../Page/宮之浦岳.md "wikilink")（1000年以上）共約107.47平方公里地區被列入世界遺產範圍。

宮之浦岳是屋久島上的最高峰（1935公尺），屬屋久島山脈的一部份，宮之浦岳同時也是日本南部，九州地方的最高峰。整個屋久島山脈都由豐富的森林植被覆蓋，而最主要的樹種為[柳杉](../Page/柳杉.md "wikilink")。在1993年列入世界遺產的神木－[繩文杉最為著名](../Page/繩文杉.md "wikilink")。

近年來由於[宮崎駿的動畫](../Page/宮崎駿.md "wikilink")「[-{zh-hk:幽靈公主; zh-mo:幽靈公主;
zh-tw:魔法公主; zh-cn:魔法公主;
zh-sg:魔法公主;}-](../Page/魔法公主.md "wikilink")」正是以屋久島取景，島上開始盛行觀光業。知名的景點包括有白谷雲水峽，千尋瀑布等；此外日本最南端的高[濕地景觀](../Page/濕地.md "wikilink")，就在屋久島的[小花之江河](../Page/小花之江河.md "wikilink")、[花之江河上](../Page/花之江河.md "wikilink")。

### 溫泉

屋久島上有不少[溫泉](../Page/溫泉.md "wikilink")，還有特別的[海中溫泉](../Page/海中溫泉.md "wikilink")，稱為[平內海中溫泉](../Page/平內海中溫泉.md "wikilink")，此外還有[尾之間溫泉和湯泊溫泉都相當有名](../Page/尾之間溫泉.md "wikilink")。

### 野生動物

[Yakusaru_In_Yaku_Island_Japan001a.jpg](https://zh.wikipedia.org/wiki/File:Yakusaru_In_Yaku_Island_Japan001a.jpg "fig:Yakusaru_In_Yaku_Island_Japan001a.jpg")
屋久島上由於森林密布，而且高山地帶受到保護沒有開發，野生動物還相當多，包括有[屋久鹿和](../Page/屋久鹿.md "wikilink")[屋久猿都是常見的野生動物](../Page/屋久猿.md "wikilink")。

在島的北邊則有世界少數幾個[海龜產卵的棲息地](../Page/海龜.md "wikilink")。在每年五月到八月時，海龜會回岸產卵，2004年時學者調查統計約有7300多隻海龜登陸，這些海龜近年來受到觀光業開發的汙染和遊客影響，引起了一些保育團體的注意。因此屋久島有幾個海灘是不對外開放的，是為了保護海龜的生態。

### 繩文杉

繩文杉一詞的由來是基於日本歷史而來，日本在石器時代後期曾發展出[繩文文化](../Page/繩文時代.md "wikilink")，距今約有6000多年，繩文杉的年齡據考察之後，約定為有6000多年，由於時期相近，加上屋久島島上發現不少繩文時代的文物，就以此命名。

## 交通

屋久島對外交通包括飛航與乘船：可從[鹿兒島機場搭乘飛機抵達](../Page/鹿兒島機場.md "wikilink")[屋久島機場](../Page/屋久島機場.md "wikilink")，約需40分鐘；屋久島機場位於島上的東北邊，跑道長度約1500公尺。另外也可乘坐高速船，從鹿兒島本港出發，前往屋久島的宮之浦港或安房港，一日有四個船次；或是乘普通汽船，也是從鹿兒島本港航往宮之浦港，一日有一個船次。

## 外部連結

  - [屋久島照片集](https://archive.is/20130426195953/http://www.j-foto.com/everyone/tags/%E7%BE%8E%E3%81%97%E3%81%84%E5%B1%8B%E4%B9%85%E5%B3%B6/)

  - [屋久島官方網站](http://yakushima.co.jp/)

  - [屋久島觀光協會](http://yakukan.jp/)

  - [屋久島地區生態旅遊推進協會](http://www.yakushima-eco.com/)

  - [屋久島環境文化財團](http://www.yakushima.or.jp/)

  - [千杉萬水屋久島](https://web.archive.org/web/20050228215736/http://www.libertytimes.com.tw/2001/new/may/7/life/travel-1.htm)

  -
[Category:大隅群島](../Category/大隅群島.md "wikilink")
[\*02](../Category/日本世界遗产.md "wikilink")
[Category:屋久島町](../Category/屋久島町.md "wikilink")
[Category:日本自然保育](../Category/日本自然保育.md "wikilink")
[Category:拉姆薩公約登錄地](../Category/拉姆薩公約登錄地.md "wikilink")
[Category:日本地質百選](../Category/日本地質百選.md "wikilink")
[Category:IUCN分类II](../Category/IUCN分类II.md "wikilink")