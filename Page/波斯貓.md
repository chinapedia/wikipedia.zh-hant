**波斯貓**（Persian
cat）是最古老的貓種之一，而其中一種廣為人知的波斯貓為**[金吉拉](../Page/金吉拉貓.md "wikilink")**（Chinchilla）.

## 品種起源

波斯貓起源於[波斯](../Page/波斯.md "wikilink")（即今天的[伊朗](../Page/伊朗.md "wikilink")），16世紀首次出現在[英國](../Page/英國.md "wikilink")，一般認為首先將波斯貓帶入[歐洲的是](../Page/歐洲.md "wikilink")[義大利的旅行家](../Page/義大利.md "wikilink")[Pietro
Della
Valle](../Page/Pietro_Della_Valle.md "wikilink")，雪白蓬鬆的長毛被視為高貴的象徵，在歐洲一出現便很受歡迎，經繁殖至今品種愈來愈多。但今天所見的長毛波斯貓的品種則是19世紀時所產生
正確起源不詳。

## 特徵

展示級的波斯貓有極長且蓬鬆的毛、相對較短的腿、較寬扁的頭及距離較開的兩耳、大眼，以及較扁的臉部。原本波斯貓的品種外觀並非完全如此，但隨著時間及展示，這些特徵漸漸被人類所強調，特別是在[北美地區](../Page/北美.md "wikilink")。過度平坦的臉部常引起許多的健康問題，尤其是呼吸道方面。但是一般而言，現在繁殖貓的飼主通常會挑選較適當且健康的貓進行繁殖以減少後代呼吸道的問題。

波斯貓有各種不同的顏色，常見的有白色、虎斑、藍色和褐色。

金吉拉底層的毛色為白色，毛尖依花色的不同為黑色、銀灰色或是金色，毛尖為黑色及銀灰色就是所謂的「[銀白色金吉拉](../Page/銀白色金吉拉.md "wikilink")」，毛尖為金色的則是「[黃金金吉拉](../Page/黃金金吉拉.md "wikilink")」，由毛尖到底層絨毛的顏色是逐漸變白的

## 金吉拉

金吉拉是人類以波斯貓和安哥拉貓繁殖培育而成的波斯貓的一支。體型中型到大型都有，特徵跟波斯貓一樣全身都是蓬鬆的長毛，但是臉部不像波斯貓那樣扁，而是像一般常見貓種的外貌。

[File:lyonofmaomar.jpg|Blue](File:lyonofmaomar.jpg%7CBlue) Colourpoint
Persian <File:Persian> (cat) silver.jpg|白色波斯貓
[File:金吉拉.jpg|金吉拉](File:金吉拉.jpg%7C金吉拉)

## 參見

  - [家貓品種列表](../Page/家貓品種列表.md "wikilink")

## 外部連結

  - [PersianKittenEmpire.com - GLOBAL Breeder
    Directory](http://www.PersianKittenEmpire.com)
  - [Traditional Persian
    Cats](http://www.traditionalpersian.worldofdani.com)
  - [The Blue Persian Cat
    Society](http://www.bluepersian.ndirect.co.uk/) (founded 1901)
  - [Cat and Kitten Central's Persian Cat Breed Information
    Guide](https://web.archive.org/web/20070206073838/http://cat-breeds.cats-central.com/persian-cat-breed-guide.html)
  - [Traditional doll face Persian breed
    description](http://www.boutiquekittens.com/index.php?id=132)
  - [Cat and Kitten Central's Persian Cat Breeders
    Directory](https://web.archive.org/web/20070216013332/http://cat-breeds.cats-central.com/kitten-for-sale/persian-kittens.html)
  - [Purelee Persians and
    Himalayans](https://web.archive.org/web/20070216055113/http://www.catpage.us/purelee/)
  - [The Persian and Exotic Cat Club](http://www.enpcc.com/)

[P](../Category/貓品種.md "wikilink")