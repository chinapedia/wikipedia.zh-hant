**帕薩爾加德**（[波斯語](../Page/波斯語.md "wikilink")：
**پاسارگاد**、[英語](../Page/英語.md "wikilink")：**Pasargadae**）位於[伊朗境內](../Page/伊朗.md "wikilink")，為[波斯](../Page/波斯.md "wikilink")[阿契美尼德帝國的首都之一](../Page/阿契美尼德帝國.md "wikilink")，目前被列為[世界遺產](../Page/世界遺產.md "wikilink")，是伊朗目前8個世界遺產的其中之一\[1\]。

## 位置與歷史

帕薩爾加德的遺跡位於[波斯波利斯東北方](../Page/波斯波利斯.md "wikilink")87[公里](../Page/公里.md "wikilink")（54[英里](../Page/英里.md "wikilink")），也就是現在[伊朗的](../Page/伊朗.md "wikilink")[法爾斯省境內](../Page/法爾斯省.md "wikilink")，也是[阿契美尼德帝國第一個首都](../Page/阿契美尼德帝國.md "wikilink")。[居魯士二世從前](../Page/居魯士二世.md "wikilink")546年（或更晚）開始這個城市的建設，不過因為居魯士二世於前530或529年去世而並沒有完成。不過帕薩爾加德仍然是阿契美尼德帝國的首都，直到[大流士一世計畫在波斯波利斯建都為止](../Page/大流士一世.md "wikilink")。帕薩爾加德這個名稱是從[希臘語而來的](../Page/希臘語.md "wikilink")，不過也可能是從阿契美尼德帝國時期一個比較早期的名稱（Pâthragâda，意為波斯的花園）演化來的。

帕薩爾加德的遺跡範圍為1.6平方公里，包括一個普遍被認為是居魯士二世[陵墓的建築](../Page/陵墓.md "wikilink")、坐落在附近山丘上的堡壘與2座皇宮與花園的遺址。這些花園也包括已知最早的四重的花園設計。

在帕薩爾加德建築結構工程的最新研究顯示：阿契美尼德帝國的工程師为這座城市設計的建築可以抵擋劇烈的[地震](../Page/地震.md "wikilink")，能承受大約是目前[芮氏](../Page/芮氏地震規模.md "wikilink")7.0級的地震。而地基相当于今天的[耐震補強設計](../Page/耐震補強設計.md "wikilink")，这种设计不久之前在許多國家被採用，例如[核能發電這種需要減低地震影響的建築](../Page/核能.md "wikilink")。
[Cyrus_tomb.jpg](https://zh.wikipedia.org/wiki/File:Cyrus_tomb.jpg "fig:Cyrus_tomb.jpg")

帕薩爾加德最重要的遺蹟是居魯士二世的陵墓，外側總共有6階寬闊的階梯，墓室長3.17公尺、寬2.11公尺、高2.11公尺，並且有一個低而狹窄的入口。雖然沒有明確的證據證明這是居魯士二世的陵墓，不過根據[希臘](../Page/希臘.md "wikilink")[歷史學家的紀錄](../Page/歷史學家.md "wikilink")，[亞歷山大大帝將它視為是居魯士二世的陵墓](../Page/亞歷山大大帝.md "wikilink")。亞歷山大大帝劫掠與破壞了波斯波利斯，並曾造訪這座陵墓。在[阿利安的著作中](../Page/阿利安.md "wikilink")，紀錄亞歷山大大帝曾經命令一位[士兵進入這座陵墓](../Page/士兵.md "wikilink")，並發現一具[黃金打造的床鋪](../Page/黃金.md "wikilink")、一張擺著器具的桌子、一具黃金棺材、一些用珍貴珠寶裝飾的擺飾與陵墓的銘文，不過這些銘文並沒有保存到現在。

根據一些古典學者的看法，這座陵墓的形式與建築與相同時期[烏拉爾圖的陵墓有密切的關係](../Page/烏拉爾圖.md "wikilink")\[2\]。尤其，帕薩爾加德的陵墓幾乎跟[里底亞國王](../Page/里底亞.md "wikilink")[阿利亞特](../Page/阿利亞特.md "wikilink")（[克羅索斯的父親](../Page/克羅索斯.md "wikilink")）的陵墓大小相同，不過許多學者並不認同這個看法。一些學者認為居魯士二世可能「引進」里底亞的工匠來建造這座陵墓。陵墓主要的設計則是位於[山牆內部門扉上的](../Page/山牆.md "wikilink")[玫瑰設計](../Page/玫瑰設計.md "wikilink")\[3\]。大體來說，帕薩爾加德可以當成許多波斯傳統文化的集合體（承襲[埃蘭](../Page/埃蘭.md "wikilink")、[巴比倫](../Page/巴比倫.md "wikilink")、[亞述與](../Page/亞述.md "wikilink")[古埃及](../Page/古埃及.md "wikilink")），並受到一些里底亞影響。

在阿拉伯人統治伊朗期間，他們的軍隊曾經試圖破壞這座陵墓，認為它違反[伊斯蘭教的教義](../Page/伊斯蘭教.md "wikilink")。不過陵墓的管理人則讓阿拉伯統治者相信這座建築不是為了居魯士二世而建的，而是作為[所羅門母親的墳墓](../Page/所羅門_\(君王\).md "wikilink")，因此這座建築免於被毀。

## 參見

  - [伊朗歷史](../Page/伊朗歷史.md "wikilink")
  - [伊朗建築](../Page/伊朗建築.md "wikilink")
  - [居魯士二世](../Page/居魯士二世.md "wikilink")

## 注释

## 參考资料

  - [Sivand Dam’s Inundation Postponed for 6
    Months](https://web.archive.org/web/20070312035619/http://www.chnpress.com/news/?Section=2&id=5981),
    Cultural Heritage News Agency, 29 November 2005, *Accessed Sept. 15,
    2006*.
  - Nazila Fathi, [A Rush to Excavate Ancient Iranian
    Sites](http://select.nytimes.com/gst/abstract.html?res=F60C1FFB3E550C748EDDA80994DD404482),
    *The New York Times*, November 27, 2005; also accessible in full
    [here](http://www.sfgate.com/cgi-bin/article.cgi?f=/c/a/2005/11/27/MNGEJFUORL1.DTL&feed=rss.news).
  - Ali Mousavi, [Cyrus can rest in peace: Pasargadae and rumors about
    the dangers of Sivand
    Dam](http://www.iranian.com/History/2005/September/Heritage6/index.html),
    Iranian.com, September 16, 2005
  - [Pasargadae Will Never
    Drown](https://web.archive.org/web/20070312115633/http://www.chnpress.com/news/?section=2&id=5615),
    Cultural Heritage News Agency, 12 September 2005, *Accessed Sept.
    15, 2006*.
  - *Persia: An Archaeological Guide*, by Sylvia A. Matheson

## 外部連結

  - “帕薩爾加德 -
    波斯帝国的宝座”([YouTube](http://www.youtube.com/watch?v=yJCw2ArVHKI)).
  - [Pictures of Tall_e
    Takht](https://web.archive.org/web/20070118140346/http://www.irantooth.com/iranpics/tall_e_takht.htm)
  - [联合国教科文组织世界遗产中心](http://whc.unesco.org/pg.cfm?cid=31&id_site=1106)
  - [帕薩爾加德，伊朗商会协会](http://www.iranchamber.com/history/pasargadae/pasargadae.php)
  - [Persepolis 官方网站](http://www.persepolis.ir/)
  - [从毁灭中保存帕薩爾加德](http://www.savepasargad.com/)
  - [Pasargad](http://landofaryan.atspace.com/passargad.htm)

[Category:伊朗世界遺產](../Category/伊朗世界遺產.md "wikilink")
[Category:波斯](../Category/波斯.md "wikilink")
[Category:古都](../Category/古都.md "wikilink")

1.  [古帕薩爾加德受到大坝施工威胁](http://www.mehrnews.com/en/NewsDetail.aspx?NewsID=107603)
    , 伊朗迈赫尔通讯社, 2004年8月28日, *Accessed Sept. 15, 2006*
2.  [C. Michael Hogan, *Tomb of Cyrus*, The Megalithic Portal, ed. A.
    Burnham, Jan 19,
    2008](http://www.megalithic.co.uk/article.php?sid=18264)
3.  罗纳德·费里尔（1989年）的波斯艺术，耶鲁大学出版社，344页 ISBN 0-300-03987-5