EditGrid，是一個[Web
2.0](../Page/Web_2.0.md "wikilink")[試算表服式](../Page/試算表.md "wikilink")，由[香港創業企業](../Page/香港.md "wikilink")[Team
and
Concepts開發](../Page/Team_and_Concepts.md "wikilink")，於2007年2月14日推出首個正式版本（首個公眾[預覽版本於](../Page/預覽版本.md "wikilink")2006年4月7日推出）。除英語版外，EditGrid備有八個語文版本，包括[德文](../Page/德文.md "wikilink")、[西班牙文](../Page/西班牙文.md "wikilink")、[法文](../Page/法文.md "wikilink")、[日文](../Page/日文.md "wikilink")、[荷蘭文](../Page/荷蘭文.md "wikilink")、[巴西](../Page/巴西.md "wikilink")[葡萄牙文及](../Page/葡萄牙文.md "wikilink")[繁](../Page/繁體中文.md "wikilink")、[簡體中文](../Page/簡體中文.md "wikilink")。個人用戶可免費登錄。

除開傳統試算表應有之功能外，EditGrid更具備一系列專為網上[協作及共享而設之功能](../Page/群組軟體.md "wikilink")。其『即時更新』（RTU）功能讓多個用戶同時在單一試算表上進行編輯，而『自訂匯出格式』（MDF）功能則允許用戶使用[XSLT將試算表轉化成其他格式](../Page/XSLT.md "wikilink")，包括供[Google
Earth讀取的](../Page/Google_Earth.md "wikilink")[KML格式](../Page/KML.md "wikilink")。其他功能包括多層權限配置、修訂記錄、圖表、即時通訊、永久連結及超過伍佰個試算表函數等。

用戶也可藉著EditGrid的刊登網誌功能從第三方網站瀏覽EditGrid試算表。開發人員可使用EditGrid的[API開發自訂應用程式](../Page/應用程序編程介面.md "wikilink")。在EditGrid上提供的Grid2Map外掛應用程式就是其一個將EditGrid上的經緯點轉化為[Google
Maps上的坐標的API應用程式](../Page/Google_Maps.md "wikilink")。

2013年10月5日04:34某用户收到邮件，称2014年5月1日结束时EditGrid将停止服务。邮件全文如下：

Dear EditGrid user, After keeping EditGrid.com up and running for a few
years as a hobby, it's about time for EditGrid.com to turn its lights
off. On May 1, 2014 at 11:59:59 pm PST, EditGrid.com services will be
discontinued. After May 1, 2014, users will no longer be able to log in
to EditGrid.com. Content stored on EditGrid.com will be deleted and no
longer be accessible. We recommend that you sign in to EditGrid.com
before May 1, 2014 and download all your spreadsheets to your computer.
Thank you for using and supporting EditGrid. David The EditGrid Team

## 外部連結

  - [官方網頁](http://www.editgrid.com/)
  - [香港外銷網站三巨頭，力抗胚胎宿命](http://mr6.cc/?p=701) – Mr. 6
  - [Editgrid：越来越出色的电子表格处理服务与令人尊敬的团队](https://web.archive.org/web/20061113154633/http://www.wangtam.com/50226711/editgrid_eeeccecaeaeaece_46343.php)
    – 網談
  - [EditGrid =\> XML + XSLT =\>
    KML](http://www.ogleearth.com/2006/08/editgrid_xml_xs.html) – Ogle
    Earth
  - [EditGrid, an excellent Web 2.0
    spreadsheet](http://www.networkworld.com/newsletters/web/2006/0717web1.html)
    – Network World

[Category:电子制表](../Category/电子制表.md "wikilink")
[Category:中文软件](../Category/中文软件.md "wikilink")
[Category:AJAX](../Category/AJAX.md "wikilink")
[Category:应用程序接口](../Category/应用程序接口.md "wikilink")
[Category:免費軟件](../Category/免費軟件.md "wikilink")
[Category:应用软件](../Category/应用软件.md "wikilink") [Category:Web
2.0](../Category/Web_2.0.md "wikilink")