**鍬鱗龍屬**（屬名：*Stagonolepis*）是種已滅絕[主龍類](../Page/主龍類.md "wikilink")[爬行動物](../Page/爬行動物.md "wikilink")，屬於[堅蜥目](../Page/堅蜥目.md "wikilink")，生存於[三疊紀晚期的](../Page/三疊紀.md "wikilink")[卡尼階](../Page/卡尼階.md "wikilink")。鍬鱗龍的化石已在[蘇格蘭與](../Page/蘇格蘭.md "wikilink")[波蘭發現](../Page/波蘭.md "wikilink")。

鍬鱗龍是種四足、[草食性動物](../Page/草食性.md "wikilink")，身體覆蓋者厚重鱗甲。鍬鱗龍行動緩慢，牠們使用厚重骨板來抵抗同時代[鑲嵌踝類主龍掠食動物的攻擊](../Page/鑲嵌踝類主龍.md "wikilink")。鍬鱗龍身長3公尺，頭部長達25公分；與身體相比，鍬鱗龍的頭部非常小頭部少於身體長度的10%。牠們的嘴部前段沒有牙齒，嘴部後段有適合咀嚼的釘狀牙齒，但喙狀嘴的前端往上拱起，可讓鍬鱗龍拔起[植物](../Page/植物.md "wikilink")（類似現在的[豬](../Page/豬.md "wikilink")），包括[木賊](../Page/木賊.md "wikilink")、[蕨類植物](../Page/蕨類植物.md "wikilink")、以及剛演化出現的[蘇鐵](../Page/蘇鐵.md "wikilink")\[1\]。

## 種

[MEPAN_aetozaur_(stagolepsis).jpg](https://zh.wikipedia.org/wiki/File:MEPAN_aetozaur_\(stagolepsis\).jpg "fig:MEPAN_aetozaur_(stagolepsis).jpg")演化博物館\]\]
[模式種是](../Page/模式種.md "wikilink")*S.
robertsoni*，化石發現於[蘇格蘭的](../Page/蘇格蘭.md "wikilink")[洛西茅斯砂岩層](../Page/洛西茅斯砂岩.md "wikilink")；第二個種是*S.
olenkae*，化石發現於[波蘭](../Page/波蘭.md "wikilink")，地質年代比模式種還年輕\[2\]。

某些古生物學家認為，發現於[南美洲的](../Page/南美洲.md "wikilink")*Aetosauroides*，是鍬鱗龍的[次異名](../Page/次異名.md "wikilink")。*Aetosauroides*目前有兩個種：*A.
scagliai*、*A. subsulcatus*。在2002年，史賓賽·盧卡斯（Spencer G.
Lucas）將體形較大的種，歸類於鍬鱗龍的*S.
robertsoni*，較小的種則歸類於*S. wellesi*\[3\]。

一些發現於[美國的化石](../Page/美國.md "wikilink")，生存同年也是三疊紀晚期，原本是獨立屬*Calyptosuchus*，曾被改歸類成鍬鱗龍的第三個種*S.
wellesi*。大部分研究認為鍬鱗龍、*Calyptosuchus*是兩個[單系源](../Page/單系源.md "wikilink")，是兩個有效的獨立屬\[4\]\[5\]\[6\]\[7\]。

## 參考文獻

## 外部連結

  - [鍬鱗龍簡介](http://www.dinoruss.com/de_4/5c5e97a.htm) Dinoruss網站
  - [鍬鱗龍](https://web.archive.org/web/20070611042809/http://www.fmnh.helsinki.fi//users/haaramo/Metazoa/Deuterostoma/Chordata/Archosauria/Pseudosuchia/history_Stagonolepis.html)
    Mikko's Phylogeny Archive網站

[Category:堅蜥目](../Category/堅蜥目.md "wikilink")

1.
2.
3.
4.
5.
6.
7.