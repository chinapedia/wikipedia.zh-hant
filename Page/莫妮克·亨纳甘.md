**莫妮克·亨纳甘**（，），生于[南卡罗来纳州](../Page/南卡罗来纳州.md "wikilink")[哥伦比亚](../Page/哥伦比亚_\(南卡罗来纳州\).md "wikilink")，是一名[美国女子](../Page/美国.md "wikilink")[田径运动员](../Page/田径.md "wikilink")。她曾夺得两枚奥林匹克运动会金牌，均来自于女子4×400米接力。

## 參考資料

## 外部連結

  -
[Category:美国田径运动员](../Category/美国田径运动员.md "wikilink")
[Category:美國奧運田徑運動員](../Category/美國奧運田徑運動員.md "wikilink")
[Category:美國奧林匹克運動會金牌得主](../Category/美國奧林匹克運動會金牌得主.md "wikilink")
[Category:2000年夏季奧林匹克運動會金牌得主](../Category/2000年夏季奧林匹克運動會金牌得主.md "wikilink")
[Category:2004年夏季奧林匹克運動會金牌得主](../Category/2004年夏季奧林匹克運動會金牌得主.md "wikilink")
[Category:2000年夏季奧林匹克運動會田徑運動員](../Category/2000年夏季奧林匹克運動會田徑運動員.md "wikilink")
[Category:2004年夏季奧林匹克運動會田徑運動員](../Category/2004年夏季奧林匹克運動會田徑運動員.md "wikilink")
[Category:奥林匹克运动会田径金牌得主](../Category/奥林匹克运动会田径金牌得主.md "wikilink")