**荔湾区**，是[中国](../Page/中国.md "wikilink")[广东省](../Page/广东省.md "wikilink")[广州市的一个](../Page/广州市.md "wikilink")[市辖区](../Page/市辖区.md "wikilink")，位於广州市西部，與[芳村區合併之前是广州老四区之一](../Page/芳村區.md "wikilink")，被廣州市府定位為“最具岭南特色的中心城区”。2005年與芳村区合并后的荔湾区的总面积為59.10平方公里，总人口73.35万人，辖22个街道办事处。“荔湾”得名自“[荔枝湾](../Page/荔枝湾.md "wikilink")”。

## 歷史

荔湾区原为古城广州之西郊，旧称西园、[西关](../Page/西关.md "wikilink")，因区内有著名的“荔枝湾”而得名。廣州有一句老說話是：「東村、西俏、南富、北貧」，那個“西俏”就是指西關，這區的特色是富商巨賈聚居之處，和眾多娛樂場所的立足地。西關風情就是形容清朝時期廣州富人的生活。

[秦始皇时代起曾先后隶属于](../Page/秦始皇.md "wikilink")[番禺](../Page/番禺.md "wikilink")、[南海等县管辖](../Page/南海.md "wikilink")，反复更变。至[宋朝宋太祖开宝五年](../Page/宋朝.md "wikilink")，荔湾地段属南海县管辖，此后一直未变。[明朝时](../Page/明朝.md "wikilink")，这里已是中国对外通商与文化交流的重要口岸；[清](../Page/清朝.md "wikilink")[康](../Page/康熙.md "wikilink")[-{乾}-时為中國唯一的外贸商埠](../Page/乾隆.md "wikilink")───十三行的所在地。清[咸丰九年](../Page/咸丰_\(年号\).md "wikilink")，沙面被迫租借给[英](../Page/英國.md "wikilink")、[法作](../Page/法國.md "wikilink")[租界](../Page/租界.md "wikilink")。清末，荔湾地区西关為南海县衙直辖区，西村、泮塘一带农村属南海县恩洲堡管辖。

廣州清初所產棉花,以輕暖出名,號“廣花”；“廣之線紗”，於[十三行之財富積累也有助推](../Page/十三行.md "wikilink")。由於本埠棉布價廉物美於[英國布](../Page/英國.md "wikilink")，出口甚多。在巨大利益驅使下，棉花來料加工迅速興起，西關農田被大量開發，修築廠房街道，錦華、經綸、麻紗等地方便於此時期出現。廣州紡織業帶之形成，帶動了印染、機具、漿緞、制衣、制帽、鞋襪、絨線等行業的興盛。下西關湧郊區也被發展為高尚住宅區，業主有不少是因紡織業大旺而獲益的洋行買辦。西關在當時有所謂“八橋之盛”（匯源、蓬萊、三聖、志喜、永寧、牛乳、大觀和德興）。

[辛亥革命后](../Page/辛亥革命.md "wikilink")，西关工商业区劃歸广州市区范围。民国17至19年，原属南海县管辖的南岸、澳口、增埗、西场等农业区划入广州市区范围。民国34年抗日战争胜利后，国民政府重新把广州划为28个行政区，收回的租界沙面也新设成区。此时荔湾区域内设有长寿、西禅、逢源、黄沙、陈塘、沙面各区，南岸、西山、太平区有部分地段属今荔湾范围。1949年10月14日，中共攻佔广州後，在以上各区成立人民政府。

1950年6月，西禅区与长寿区合并称为长寿区，逢源区与黄沙区合并称为荔湾区，南岸区改名西村区。逢源区的三沙、泮塘农业区划归西村区。陈塘区并入太平区（后并入中区）。各区人民政府相应成立。而沙面区則改为市直辖沙面街办事处，後改辖于太平区。1952年9月，长寿区、荔湾区及西村区的彩虹、西村、泮塘、西增、小梅街和南源镇合并成立西区，并成立西区人民政府，後改称西区人民委员会。1959年4月，泮塘、三沙等农业社加入三元里人民公社，划出西区。1960年4至7月，原属中区的清平人民公社（含清平、岭南、沙面三街地段）与秀丽、光扬、华林、宝华四街并入西区。西区同时接管了三元里人民公社的同德、横沙、沙凤、三沙、西郊等5个大队和石井人民公社7个大队。1960年8月，西区改名荔湾区。1961年复建18个街道办事处。同年9月，区辖内农村大队全部划回郊区管辖。2002年1月18日，荔湾区正式接管了原轄[白云区的大坦沙岛](../Page/白云区_\(广州市\).md "wikilink")。2005年5月，广州市调整行政区划，將原[芳村区的行政区域划归荔湾区管辖](../Page/芳村区.md "wikilink")。

## 轄區

新荔灣区下辖22个街道，即原来荔湾区13个街道和原来芳村区9个街道。

  - 属原荔灣区的街道：[华林街道](../Page/华林街道.md "wikilink")（因街道属地有一佛教名寺“[华林寺](../Page/华林寺_\(广州\).md "wikilink")”而得名）、[逢源街道](../Page/逢源街道.md "wikilink")、[龙津街道](../Page/龙津街道.md "wikilink")、[彩虹街道](../Page/彩虹街道.md "wikilink")、[多宝街道](../Page/多宝街道.md "wikilink")、[岭南街道](../Page/岭南街道.md "wikilink")、[西村街道](../Page/西村街道.md "wikilink")、[南源街道](../Page/南源街道.md "wikilink")、[金花街道](../Page/金花街道.md "wikilink")、[昌华街道](../Page/昌华街道.md "wikilink")、[桥中街道](../Page/桥中街道.md "wikilink")、[站前街道](../Page/站前街道.md "wikilink")、[沙面街道](../Page/沙面街道.md "wikilink")；
  - 属原芳村区的街道：[石围塘街道](../Page/石围塘街道.md "wikilink")、[花地街道](../Page/花地街道.md "wikilink")、[茶滘街道](../Page/茶滘街道.md "wikilink")、[冲口街道](../Page/冲口街道.md "wikilink")、[白鹤洞街道](../Page/白鹤洞街道.md "wikilink")、[东漖街道](../Page/东漖街道.md "wikilink")、[东沙街道](../Page/东沙街道.md "wikilink")、[中南街道](../Page/中南街道.md "wikilink")、[海龙街道](../Page/海龙街道.md "wikilink")。

## 经济

现在荔湾区是广州市唯一一个拥有一河两岸美丽景色的辖区。西岸为[芳村](../Page/芳村.md "wikilink")，经济主要以[花卉](../Page/花卉.md "wikilink")、[蔬菜种植](../Page/蔬菜.md "wikilink")、[水产养殖为主](../Page/水产.md "wikilink")，近十年主要发展花鸟鱼虫及茶叶、家具批发如[南方茶叶市场及](../Page/南方茶叶市场.md "wikilink")[花鸟鱼虫批发市场](../Page/花鸟鱼虫批发市场.md "wikilink")。而东岸则为传统商业繁华地段，有[上下九步行街](../Page/上下九步行街.md "wikilink")、[南方大厦](../Page/南方大厦.md "wikilink")、[华林玉器街等经济特色](../Page/华林玉器街.md "wikilink")。

## 景點

荔湾区的景点有[陈家祠](../Page/陈家祠.md "wikilink")、[沙面](../Page/沙面.md "wikilink")、[上下九步行街](../Page/上下九步行街.md "wikilink")、[流花湖公园](../Page/流花湖公园.md "wikilink")、[西苑](../Page/西苑.md "wikilink")、[荔灣湖公園](../Page/荔灣湖公園.md "wikilink")、[西關大屋](../Page/西關大屋.md "wikilink")、[黄大仙祠](../Page/黄大仙祠_\(广州\).md "wikilink")、[广州花卉博览园](../Page/广州花卉博览园.md "wikilink")、[聚龙村](../Page/聚龙村.md "wikilink")、[荔枝湾](../Page/荔枝湾.md "wikilink")、[坑口观音庙](../Page/坑口观音庙.md "wikilink"),
[白鹅潭酒吧街](../Page/白鹅潭酒吧街.md "wikilink")，[华林寺](../Page/华林寺.md "wikilink")，[仁威庙](../Page/仁威庙.md "wikilink")，[恩宁路骑楼街](../Page/恩宁路骑楼街.md "wikilink")，[广州文化公园](../Page/广州文化公园.md "wikilink")，[醉观公园](../Page/醉观公园.md "wikilink")，[大沙河湿地公园等](../Page/大沙河湿地公园.md "wikilink")。

## 著名大廈

荔湾区的著名大厦有[白天鹅宾馆](../Page/白天鹅宾馆.md "wikilink")、[南方大厦](../Page/南方大厦.md "wikilink")、[荔灣廣場](../Page/荔灣廣場.md "wikilink")、[广州汇丰大厦](../Page/广州汇丰大厦.md "wikilink"),
广州地标性建筑物[广州圆大厦](../Page/广州圆大厦.md "wikilink")（位于荔湾区东沙大道）等。

## 交通

### 主干道

荔湾区的主干道有[东风路](../Page/東風路_\(广州\).md "wikilink")、[中山路](../Page/中山路_\(广州\).md "wikilink")、[環市路](../Page/環市路_\(广州\).md "wikilink")、[六二三路](../Page/六二三路.md "wikilink")、[龍溪大道](../Page/龍溪大道.md "wikilink")、[花地大道](../Page/花地大道.md "wikilink")、[鶴洞路](../Page/鶴洞路.md "wikilink")、[南岸路](../Page/南岸路.md "wikilink")、[黄沙大道](../Page/黄沙大道.md "wikilink")、[康王路](../Page/康王路.md "wikilink")、[荔湾路](../Page/荔湾路.md "wikilink")、[人民路](../Page/人民路.md "wikilink")、[芳村大道](../Page/芳村大道.md "wikilink")。

### [高速公路](../Page/高速公路.md "wikilink")

荔湾区的高速公路有[廣州環城高速公路](../Page/廣州環城高速公路.md "wikilink")、[廣珠西線高速公路](../Page/廣珠西線高速公路.md "wikilink"),
[东新高速公路](../Page/东新高速公路.md "wikilink")。

### 橋樑及隧道

荔湾区的橋樑及隧道有：

[珠江大橋](../Page/珠江大橋.md "wikilink")、[珠江隧道](../Page/珠江隧道.md "wikilink")：连接荔湾区与荔湾区芳村片区。

[人民桥](../Page/人民橋_\(广州\).md "wikilink")、[鶴洞大橋](../Page/鶴洞大橋.md "wikilink")、[丫髻沙大橋](../Page/丫髻沙大橋.md "wikilink")、[洲头咀隧道](../Page/洲头咀隧道.md "wikilink")：连接荔湾区和海珠区。

[五丫口大橋](../Page/五丫口大橋.md "wikilink")、[沙尾大橋](../Page/沙尾大橋.md "wikilink")：连接广州市荔湾区与佛山市南海区。

[东沙桥](../Page/东沙桥.md "wikilink")：连接荔湾区与番禺区。

### 地鐵

  - [{{广州地铁线路标志](../Page/广州地铁1号线.md "wikilink")：[陳家祠站](../Page/陳家祠站.md "wikilink")、[長壽路站](../Page/長壽路站.md "wikilink")、[黄沙站](../Page/黄沙站.md "wikilink")、[芳村站](../Page/芳村站.md "wikilink")、[花地灣站](../Page/花地灣站.md "wikilink")、[坑口站](../Page/坑口站_\(广州\).md "wikilink")、[西塱站](../Page/西塱站.md "wikilink")
  - [{{广州地铁线路标志](../Page/广州地铁5号线.md "wikilink")：[滘口站](../Page/滘口站.md "wikilink")、[坦尾站](../Page/坦尾站.md "wikilink")、[中山八站](../Page/中山八站.md "wikilink")、[西場站](../Page/西場站.md "wikilink")、[西村站](../Page/西村站.md "wikilink")
  - [{{广州地铁线路标志](../Page/广州地铁6号线.md "wikilink")：[河沙站](../Page/河沙站.md "wikilink")、[坦尾站](../Page/坦尾站.md "wikilink")、[如意坊站](../Page/如意坊站.md "wikilink")、[黃沙站](../Page/黃沙站.md "wikilink")、[文化公園站](../Page/文化公園站.md "wikilink")
  - [{{广州地铁线路标志](../Page/廣佛地鐵.md "wikilink")：[龍溪站](../Page/龍溪站.md "wikilink")、[菊樹站](../Page/菊樹站.md "wikilink")、[西塱站](../Page/西塱站.md "wikilink")、[鶴洞站](../Page/鶴洞站.md "wikilink")、[沙涌站](../Page/沙涌站.md "wikilink")

<!-- end list -->

  - [{{广州地铁线路标志](../Page/广州地铁8号线.md "wikilink")：[文化公园站](../Page/文化公园站.md "wikilink")、[华林寺站](../Page/华林寺站.md "wikilink")、[陈家祠站](../Page/陈家祠站.md "wikilink")、[彩虹桥站](../Page/彩虹桥站.md "wikilink")、[西村站](../Page/西村站.md "wikilink")
  - [{{广州地铁线路标志](../Page/广州地铁11号线.md "wikilink")：[彩虹橋站](../Page/彩虹橋站.md "wikilink")、[中山八站](../Page/中山八站.md "wikilink")、[如意坊站](../Page/如意坊站.md "wikilink")、[石圍塘站](../Page/石圍塘站.md "wikilink")、[芳村站](../Page/芳村站.md "wikilink")、[芳村大道東站](../Page/芳村大道東站.md "wikilink")、[沙涌站](../Page/沙涌站.md "wikilink")、[鶴洞東站](../Page/鶴洞東站.md "wikilink")
  - [{{广州地铁线路标志](../Page/广州地铁13号线.md "wikilink")：[西場站](../Page/西場站.md "wikilink")、[彩虹橋站](../Page/彩虹橋站.md "wikilink")
  - [{{广州地铁线路标志](../Page/广州地铁22号线.md "wikilink")：[东沙工业园站](../Page/东沙工业园站.md "wikilink")、[西朗站](../Page/西朗站.md "wikilink")、[白鹅潭站](../Page/白鹅潭站.md "wikilink")

同時，亦有規劃[10号线](../Page/广州地铁10号线.md "wikilink")、[13号线](../Page/广州地铁13号线.md "wikilink")、[19号线經過荔灣區內](../Page/广州地铁19号线.md "wikilink")。

## 教育

### 中学

荔湾区的中学有[广东实验中学](../Page/广东实验中学.md "wikilink")（高中部）、[广雅中学](../Page/广雅中学.md "wikilink")、[广州市第一中学](../Page/广州市第一中学.md "wikilink")、[广州市第四中学](../Page/广州市第四中学.md "wikilink")、[广州市协和高级中学](../Page/广州市协和高级中学.md "wikilink")、[广州市真光中学](../Page/广州市真光中学.md "wikilink")、[广州市培英中学](../Page/广州市培英中学.md "wikilink")（校本部）、[广州市西关外国语学校](../Page/广州市西关外国语学校.md "wikilink")、[西关培英中学等](../Page/西关培英中学.md "wikilink")。

### 小学

荔湾区的小学有[沙面小学](../Page/沙面小学.md "wikilink")、[西关培正小学](../Page/西关培正小学.md "wikilink")、[华侨小学](../Page/华侨小学.md "wikilink")、[真光中英文小学等](../Page/真光中英文小学.md "wikilink")。

## 参考文献

  - 　广州市地方志编纂委员会（1998）.《广州市志·卷二 自然地理志、建置志、人口志、区县概况》.廣州市：廣州出版社 ISBN
    7-80592-607-7

## 外部链接

  - [荔湾区政府网站](http://www.lw.gov.cn/)
  - [荔湾区人大网站](http://rd.lw.gov.cn/)
  - [荔湾区政府招商网站](https://web.archive.org/web/20110606045718/http://zsw.lw.gov.cn/)
  - [荔湾区科技园网站](https://web.archive.org/web/20101231024513/http://kjy.lw.gov.cn/)

[\*](../Category/荔湾区.md "wikilink") [区](../Category/广州区市.md "wikilink")
[Category:广州市辖区](../Category/广州市辖区.md "wikilink")