[南投縣草屯鎮炎峰國民小學.jpg](https://zh.wikipedia.org/wiki/File:南投縣草屯鎮炎峰國民小學.jpg "fig:南投縣草屯鎮炎峰國民小學.jpg")
[Jiujiufeng,_Nantou_County_,Taiwan.jpg](https://zh.wikipedia.org/wiki/File:Jiujiufeng,_Nantou_County_,Taiwan.jpg "fig:Jiujiufeng,_Nantou_County_,Taiwan.jpg")
[Jiujiufeng_after_Chichi_Earthquake_20061007.jpg](https://zh.wikipedia.org/wiki/File:Jiujiufeng_after_Chichi_Earthquake_20061007.jpg "fig:Jiujiufeng_after_Chichi_Earthquake_20061007.jpg")
**九九峰**，亦稱**九十九尖峰**，是[臺灣](../Page/臺灣.md "wikilink")[中部地區擁有火炎山特殊地理景觀的地區](../Page/中臺灣.md "wikilink")，諸峰獨立，鮮有稜脈相連，九九峰全境最高峰為海拔779.4公尺（[921地震前高度](../Page/921地震.md "wikilink")777.5公尺），與[三義](../Page/三義鄉_\(台灣\).md "wikilink")[火炎山](../Page/火炎山_\(苗栗縣\).md "wikilink")、[六龜](../Page/六龜區.md "wikilink")[十八羅漢山並稱為](../Page/十八羅漢山.md "wikilink")「台灣三大火炎山地形」。九九峰地質含有[赭土](../Page/赭土.md "wikilink")，赭土本身具有[鐵](../Page/鐵.md "wikilink")、[鋁等](../Page/鋁.md "wikilink")[氧化物成份](../Page/氧化物.md "wikilink")，在晨間、黃昏之時，遠觀九九峰，宛如火紅般的山色在燃燒，故得火炎山之美譽。

九九峰於921地震之後，植被隨之土石崩落，使山頭失去植被而裸露土壤。由[國道6號沿](../Page/國道六號_\(中華民國\).md "wikilink")[烏溪向東橫越九九峰南側](../Page/烏溪.md "wikilink")，規劃草屯服務區可供旅客觀賞九九峰。九九峰對於草屯是別具意義的指標，因此炎峰國小前校長簡金間對九九峰比喻：「尖峰宛如筆架，能鍾聚靈氣，培才毓秀。」取「炎峰毓秀」命名[炎峰國小](../Page/南投縣草屯鎮炎峰國民小學.md "wikilink")，是象徵[草屯第一名景](../Page/草屯鎮.md "wikilink")。

## 自然保留區

經歷921地震後，[農委會依照](../Page/農委會.md "wikilink")[文化資產保存法第](../Page/文化資產保存法.md "wikilink")49條細則第72條規定，以地震造成崩坍、斷崖等特殊地景為保育對象，因此由[林務局於](../Page/林務局.md "wikilink")1999年10月提出九九峰嚴重崩坍區域劃為[自然保留區](../Page/自然保留區.md "wikilink")，同年12月完成規劃報告書，將一千兩百[公頃所屬嚴重崩坍地且](../Page/公頃.md "wikilink")[人類不易到達之區劃設](../Page/人類.md "wikilink")，供學術研究與戶外教學之用，翌年5月22日正式成立「**九九峰自然保留區**」。

九九峰自然保留區設立後，已明確地將九九峰範圍納定，位於[南投縣](../Page/南投縣.md "wikilink")[草屯鎮](../Page/草屯鎮.md "wikilink")、[國姓鄉及](../Page/國姓鄉.md "wikilink")[臺中市](../Page/臺中市.md "wikilink")[霧峰區](../Page/霧峰區.md "wikilink")、[太平區](../Page/太平區_\(臺中市\).md "wikilink")，面積達1198.4466[公頃](../Page/公頃.md "wikilink")，屬於台灣[文化資產保存法中的](../Page/文化資產保存法.md "wikilink")「自然地景」，由[農委會林務局](../Page/農委會林務局.md "wikilink")[埔里事業處第](../Page/埔里鎮.md "wikilink")8～13與15～20林班管轄。

## 成因

受到古烏溪的上游帶來的岩塊、土壤等沉積物質，經由搬運與堆積的作用，在現今九九峰位置形成沖積扇，加上長時間堆積、擠壓，將地層內的礫石彼此膠結，逐漸地形成卵礫石岩層，這便是火炎山層地質構造，但此時仍是處於埋沒在地表之下，經過造山運動後，這些卵礫石構成的岩層得以露出地表。九九峰除了地質構造是火炎山層，還挾帶著[香山砂岩與](../Page/香山砂岩.md "wikilink")[頁岩](../Page/頁岩.md "wikilink")，後來長時間經歷雨水沖刷，將脆弱的頁岩沖蝕流失，成了現在看到黃山群起，諸峰挺立的火炎山景觀，九九峰與鄰近[頭嵙山同屬頭嵙山層地質](../Page/頭嵙山.md "wikilink")。

## 地理環境

[烏溪自九九峰東側](../Page/烏溪.md "wikilink")、南側流經，境內由東向西有仙人指坑溪、田尾坑溪、油車坑溪、乾溪等四條河川貫穿。依[中央氣象局雙冬測候站](../Page/中央氣象局.md "wikilink")，自1995年－2002年記錄顯示雨季在5月～8月，此時才會顯現大河之勢，相對地在10月～翌年1月是為乾季，這期間大河變成細流，當地農民為了適應此處環境而發展出鑿井灌溉，故可見得許多交錯水管穿越烏溪上方。

九九峰河谷景觀相當陡峭、開闊，平均坡度皆有60～70度，最陡達到85度峭壁，因此區地質屬於卵礫石岩層，受到風化、雨襲是顯得地表狀況不穩定，許多河谷都呈現V字形侵蝕溝，如此山景美色，卻無法種植耕地，在[地理學喻為](../Page/地理學.md "wikilink")「[惡地](../Page/惡地.md "wikilink")」，與[田寮月世界](../Page/田寮區.md "wikilink")、[利吉月世界屬不同地質構造](../Page/利吉月世界.md "wikilink")。

## 地震影響

九九峰周邊有雙冬斷層、龜蒲斷層，但並未由這兩斷層所觸發地震，而是受到車籠埔斷層引發921地震而使表土裸露。在自然保留區境內崩坍面積總達729.36公頃，其中以第15林班崩坍區域最為廣泛，相對位居國姓鄉第20林班，受到地震影響並不大，是全部崩坍面積最小的林班地。實際上，多數崩坍地分布於山頭，平均海拔皆在400公尺以上，而且所造成地形景觀宛如一根根玉筍佇立，因此地震之後，對於有立即危害之區域進行整治、植栽，避免[土石流危害周邊民居與損害範圍擴大](../Page/土石流.md "wikilink")，在九九峰境內的河川可見得有箱涵工法或蛇籠築壩整治。

## 周邊景點

  - [青桐林生態產業園區](../Page/青桐林生態產業園區.md "wikilink")
  - [國立臺灣工藝研究發展中心](../Page/國立臺灣工藝研究發展中心.md "wikilink")[九九峰園區](../Page/九九峰園區.md "wikilink")
  - [土生自然農產-九九峰蝶豆花](../Page/土生自然農產-九九峰蝶豆花.md "wikilink")

## 參考文獻

## 延伸閱讀

  -
  -
## 外部連結

  - [【走讀台灣
    草屯鎮】九九峰自然保護區](https://web.archive.org/web/20090227015749/http://readtw.ncl.edu.tw/readtw/town_html/10008/1000803/scenic.htm)
  - [【行政院農委會林務局
    自然保育網】九九峰自然保留區](https://web.archive.org/web/20071116094837/http://conservation.forest.gov.tw/ct.asp?xItem=3213&ctNode=174&mp=10)
  - [【農委會林務局 自然資源與生態資料庫】
    九九峰自然保留區](http://ngis.zo.ntu.edu.tw/nr/nr19_nine.htm)
  - \[<http://ngis.zo.ntu.edu.tw/PDF/biodiversity/150dpi/3_èªç>¶ä¿çå.pdf
    台灣十九個自然保留區位置圖\]

[category:霧峰區](../Page/category:霧峰區.md "wikilink")

[Category:臺灣區域步道](../Category/臺灣區域步道.md "wikilink")
[Category:南投縣山峰](../Category/南投縣山峰.md "wikilink")
[Category:台中市山峰](../Category/台中市山峰.md "wikilink")
[Category:南投縣旅遊景點](../Category/南投縣旅遊景點.md "wikilink")
[Category:台中市旅遊自然景點](../Category/台中市旅遊自然景點.md "wikilink")
[Category:台灣自然保留區](../Category/台灣自然保留區.md "wikilink")
[Category:臺中市文化資產](../Category/臺中市文化資產.md "wikilink") [Category:太平區
(臺灣)](../Category/太平區_\(臺灣\).md "wikilink")
[Category:草屯鎮](../Category/草屯鎮.md "wikilink")
[Category:國姓鄉](../Category/國姓鄉.md "wikilink")
[Category:台灣自然地景](../Category/台灣自然地景.md "wikilink")
[Category:南投縣文化資產](../Category/南投縣文化資產.md "wikilink")