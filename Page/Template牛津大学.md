<noinclude>  </noinclude>{{Navbox |name = 牛津大学 |title =
[<span style="color: #ffffff;">牛津大學</span>](../Page/牛津大學.md "wikilink")
|state =  |bodyclass = hlist |basestyle = background:\#002147;
color:white; |groupstyle= text-align:left; |image =
![Oxford_University_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:Oxford_University_Coat_Of_Arms.svg
"Oxford_University_Coat_Of_Arms.svg")

|group1 = 領導層 |list1 =

  -
    [彭定康](../Page/彭定康.md "wikilink")

  -
    [露易丝·理查德森](../Page/露易丝·理查德森.md "wikilink")

  -

|group2 =
[<span style="color: #ffffff;">成員學院</span>](../Page/牛津大學學院.md "wikilink")
|list2 =

![All-Souls_College_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:All-Souls_College_Oxford_Coat_Of_Arms.svg
"All-Souls_College_Oxford_Coat_Of_Arms.svg")
[萬靈](../Page/牛津大學萬靈學院.md "wikilink")
![Balliol_College_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:Balliol_College_Oxford_Coat_Of_Arms.svg
"Balliol_College_Oxford_Coat_Of_Arms.svg")
[利奧爾](../Page/牛津大學貝利奧爾學院.md "wikilink")
![Brasenose_College_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:Brasenose_College_Oxford_Coat_Of_Arms.svg
"Brasenose_College_Oxford_Coat_Of_Arms.svg")
[布雷齊諾斯](../Page/牛津大學布雷齊諾斯學院.md "wikilink")
![Christ_Church_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:Christ_Church_Oxford_Coat_Of_Arms.svg
"Christ_Church_Oxford_Coat_Of_Arms.svg")
[基督堂](../Page/牛津大學基督堂學院.md "wikilink")
![Corpus-Christi_College_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:Corpus-Christi_College_Oxford_Coat_Of_Arms.svg
"Corpus-Christi_College_Oxford_Coat_Of_Arms.svg")
[基督聖體](../Page/牛津大學基督聖體學院.md "wikilink")
![Exeter_College_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:Exeter_College_Oxford_Coat_Of_Arms.svg
"Exeter_College_Oxford_Coat_Of_Arms.svg")
[埃克塞特](../Page/牛津大學埃克塞特學院.md "wikilink")
![Green-Templeton_College_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:Green-Templeton_College_Oxford_Coat_Of_Arms.svg
"Green-Templeton_College_Oxford_Coat_Of_Arms.svg")
[格林坦普頓](../Page/牛津大學格林坦普頓學院.md "wikilink")
![Harris-Manchester_College_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:Harris-Manchester_College_Oxford_Coat_Of_Arms.svg
"Harris-Manchester_College_Oxford_Coat_Of_Arms.svg")
[哈里斯·曼徹斯特](../Page/牛津大學哈里斯·曼徹斯特學院.md "wikilink")
![Hertford_College_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:Hertford_College_Oxford_Coat_Of_Arms.svg
"Hertford_College_Oxford_Coat_Of_Arms.svg")
[赫特福德](../Page/牛津大學赫特福德學院.md "wikilink")
![Jesus_College_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:Jesus_College_Oxford_Coat_Of_Arms.svg
"Jesus_College_Oxford_Coat_Of_Arms.svg")
[耶穌](../Page/牛津大學耶穌學院.md "wikilink")
![Keble_College_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:Keble_College_Oxford_Coat_Of_Arms.svg
"Keble_College_Oxford_Coat_Of_Arms.svg")
[基布爾](../Page/牛津大學基布爾學院.md "wikilink")
![Kellogg_College_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:Kellogg_College_Oxford_Coat_Of_Arms.svg
"Kellogg_College_Oxford_Coat_Of_Arms.svg")
[凱洛格](../Page/牛津大學凱洛格學院.md "wikilink")
![Lady-Margaret-Hall_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:Lady-Margaret-Hall_Oxford_Coat_Of_Arms.svg
"Lady-Margaret-Hall_Oxford_Coat_Of_Arms.svg")
[瑪格麗特夫人](../Page/牛津大學瑪格麗特夫人學堂.md "wikilink")
![Linacre_College_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:Linacre_College_Oxford_Coat_Of_Arms.svg
"Linacre_College_Oxford_Coat_Of_Arms.svg")
[李納克](../Page/牛津大學李納克爾學院.md "wikilink")
![Lincoln_College_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:Lincoln_College_Oxford_Coat_Of_Arms.svg
"Lincoln_College_Oxford_Coat_Of_Arms.svg")
[林肯](../Page/牛津大學林肯學院.md "wikilink")
![Magdalen_College_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:Magdalen_College_Oxford_Coat_Of_Arms.svg
"Magdalen_College_Oxford_Coat_Of_Arms.svg")
[莫德林](../Page/牛津大學莫德林學院.md "wikilink")
![Mansfield_College_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:Mansfield_College_Oxford_Coat_Of_Arms.svg
"Mansfield_College_Oxford_Coat_Of_Arms.svg")
[曼斯菲爾德](../Page/牛津大學曼斯菲爾德學院.md "wikilink")
![Merton_College_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:Merton_College_Oxford_Coat_Of_Arms.svg
"Merton_College_Oxford_Coat_Of_Arms.svg")
[墨頓](../Page/牛津大學墨頓學院.md "wikilink")
![New_College_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:New_College_Oxford_Coat_Of_Arms.svg
"New_College_Oxford_Coat_Of_Arms.svg")
[新學院](../Page/牛津大學新學院.md "wikilink")
![Nuffield_College_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:Nuffield_College_Oxford_Coat_Of_Arms.svg
"Nuffield_College_Oxford_Coat_Of_Arms.svg")
[納菲爾德](../Page/牛津大學納菲爾德學院.md "wikilink")
![Oriel_College_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:Oriel_College_Oxford_Coat_Of_Arms.svg
"Oriel_College_Oxford_Coat_Of_Arms.svg")
[奧里爾](../Page/牛津大學奧里爾學院.md "wikilink")
![Pembroke_College_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:Pembroke_College_Oxford_Coat_Of_Arms.svg
"Pembroke_College_Oxford_Coat_Of_Arms.svg")
[彭布羅克](../Page/牛津大學彭布羅克學院.md "wikilink")
![Queens_College_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:Queens_College_Oxford_Coat_Of_Arms.svg
"Queens_College_Oxford_Coat_Of_Arms.svg")
[王后](../Page/牛津大學王后學院.md "wikilink")
![Somerville_College_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:Somerville_College_Oxford_Coat_Of_Arms.svg
"Somerville_College_Oxford_Coat_Of_Arms.svg")
[薩默維爾](../Page/牛津大學薩默維爾學院.md "wikilink")
![St-Anne's_College_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:St-Anne's_College_Oxford_Coat_Of_Arms.svg
"St-Anne's_College_Oxford_Coat_Of_Arms.svg")
[聖安妮](../Page/牛津大學聖安妮學院.md "wikilink")
![St-Antony's_College_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:St-Antony's_College_Oxford_Coat_Of_Arms.svg
"St-Antony's_College_Oxford_Coat_Of_Arms.svg")
[聖安東尼](../Page/牛津大學聖安東尼學院.md "wikilink")
![St-Catherines_College_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:St-Catherines_College_Oxford_Coat_Of_Arms.svg
"St-Catherines_College_Oxford_Coat_Of_Arms.svg")
[聖凱瑟琳](../Page/牛津大學聖凱瑟琳學院.md "wikilink")
![St-Cross_College_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:St-Cross_College_Oxford_Coat_Of_Arms.svg
"St-Cross_College_Oxford_Coat_Of_Arms.svg")
[聖十字](../Page/牛津大學聖十字學院.md "wikilink")
![St-Edmund-Hall_College_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:St-Edmund-Hall_College_Oxford_Coat_Of_Arms.svg
"St-Edmund-Hall_College_Oxford_Coat_Of_Arms.svg")
[聖艾德蒙](../Page/牛津大學聖艾德蒙學堂.md "wikilink")
![St-Hilda's_College_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:St-Hilda's_College_Oxford_Coat_Of_Arms.svg
"St-Hilda's_College_Oxford_Coat_Of_Arms.svg")
[聖希爾達](../Page/牛津大學聖希爾達學院.md "wikilink")
![St-Hughs_College_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:St-Hughs_College_Oxford_Coat_Of_Arms.svg
"St-Hughs_College_Oxford_Coat_Of_Arms.svg")
[聖休](../Page/牛津大學聖休學院.md "wikilink")
![St-John's_College_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:St-John's_College_Oxford_Coat_Of_Arms.svg
"St-John's_College_Oxford_Coat_Of_Arms.svg")
[聖約翰](../Page/牛津大學聖約翰學院.md "wikilink")
![St-Peters_College_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:St-Peters_College_Oxford_Coat_Of_Arms.svg
"St-Peters_College_Oxford_Coat_Of_Arms.svg")
[聖彼得](../Page/牛津大學聖彼得學院.md "wikilink")
![Trinity_College,_Oxford.svg](https://zh.wikipedia.org/wiki/File:Trinity_College,_Oxford.svg
"Trinity_College,_Oxford.svg") [三一](../Page/牛津大學三一學院.md "wikilink")
![University_College_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:University_College_Oxford_Coat_Of_Arms.svg
"University_College_Oxford_Coat_Of_Arms.svg")
[大學學院](../Page/牛津大學大學學院.md "wikilink")
![Wadham_College_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:Wadham_College_Oxford_Coat_Of_Arms.svg
"Wadham_College_Oxford_Coat_Of_Arms.svg")
[瓦德漢](../Page/牛津大學瓦德漢學院.md "wikilink")
![Wolfson_College_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:Wolfson_College_Oxford_Coat_Of_Arms.svg
"Wolfson_College_Oxford_Coat_Of_Arms.svg")
[沃弗森](../Page/牛津大學沃弗森學院.md "wikilink")
![Worcester_College_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:Worcester_College_Oxford_Coat_Of_Arms.svg
"Worcester_College_Oxford_Coat_Of_Arms.svg")
[伍斯特](../Page/牛津大學伍斯特學院.md "wikilink")

</ol>

|group3 =
[<span style="color: #ffffff;">私人學堂</span>](../Page/永久私人學堂.md "wikilink")
|list3 =

![Blackfriars_Hall_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:Blackfriars_Hall_Oxford_Coat_Of_Arms.svg
"Blackfriars_Hall_Oxford_Coat_Of_Arms.svg")
[黑衣修士](../Page/牛津大學黑衣修士院.md "wikilink")
![Campion_Hall_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:Campion_Hall_Oxford_Coat_Of_Arms.svg
"Campion_Hall_Oxford_Coat_Of_Arms.svg")
[康平](../Page/牛津大學康平學堂.md "wikilink")
![Regent's_Park_College_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:Regent's_Park_College_Oxford_Coat_Of_Arms.svg
"Regent's_Park_College_Oxford_Coat_Of_Arms.svg")
[攝政公園](../Page/牛津大學攝政公園學院.md "wikilink")
![St_Benet's_Hall_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:St_Benet's_Hall_Oxford_Coat_Of_Arms.svg
"St_Benet's_Hall_Oxford_Coat_Of_Arms.svg")
[聖貝納](../Page/牛津大學聖貝納學堂.md "wikilink")
![St-Stephen's_Hall_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:St-Stephen's_Hall_Oxford_Coat_Of_Arms.svg
"St-Stephen's_Hall_Oxford_Coat_Of_Arms.svg")
[聖史蒂芬](../Page/牛津大學聖史蒂芬院.md "wikilink")
![Wycliffe_Hall_Oxford_Coat_Of_Arms.svg](https://zh.wikipedia.org/wiki/File:Wycliffe_Hall_Oxford_Coat_Of_Arms.svg
"Wycliffe_Hall_Oxford_Coat_Of_Arms.svg")
[威克里夫](../Page/牛津大學威克里夫學堂.md "wikilink")

</ol>

|group4 =
[<span style="color: #ffffff;">學術學院</span>](../Page/牛津大學學術學院.md "wikilink")
|list4 =

  - 人文學院
  - 社會科學院
  - 數學、物理與生命科學院
  - 醫學院

|group5 = <span style="color: #ffffff;">認可研究
中心</span> |list5 =

  - [佛教研究](../Page/牛津大學佛教研究中心.md "wikilink")

  - [希伯來及猶太研究](../Page/牛津大學希伯來及猶太研究中心.md "wikilink")

  -
  - [伊斯蘭研究](../Page/牛津大學伊斯蘭研究中心.md "wikilink")

|group6 = 體育 |list6 =

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
|below =

  - '''\[\[:commons:Category:牛津大學|<noinclude>

</noinclude>