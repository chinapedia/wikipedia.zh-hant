**蘇聯五年計劃**（）是[蘇聯在](../Page/蘇聯.md "wikilink")[史太林統治時期全國性的經濟計劃](../Page/史太林.md "wikilink")，目標是令蘇聯的經濟迅速發展。該計劃是由史太林推展，由[苏联国家计划委员会](../Page/苏联国家计划委员会.md "wikilink")（）在生产理论的基礎上制訂細節及執行，成為[蘇聯共產黨經濟發展的總方針](../Page/蘇聯共產黨.md "wikilink")。自1928年发起至1991年[苏联解体共实行了十三个五年计划](../Page/苏联解体.md "wikilink")。

## 背景

長久以來，俄國相比其他的歐洲鄰國，在經濟發展上都處於落後形勢，包括在工業生產上處於甚低水平；農業的生產方式仍處於落伍、欠缺機械化的小農生產（在二十世紀初期仍使用木犁或石犁），反映俄國在經濟上的嚴重衰弱，加上蘇聯立國後搖擺不定的經濟生產政策，[白軍和](../Page/俄国白军.md "wikilink")[紅軍的內戰](../Page/苏联红军.md "wikilink")、對外貿易的停止，都使蘇聯的經濟更進一步受到破壞。而面對立國初期國際環境針對蘇聯的孤立、三十年代西鄰的[德國及東鄰的](../Page/德國.md "wikilink")[日本](../Page/日本.md "wikilink")[極右](../Page/極右派.md "wikilink")[反共主義興起](../Page/反共主义.md "wikilink")，[英](../Page/英国.md "wikilink")[法的](../Page/法国.md "wikilink")[綏靖政策及](../Page/绥靖主义.md "wikilink")[美國的](../Page/美國.md "wikilink")[孤立主義均使蘇聯對外的憂患意識加強](../Page/孤立主義.md "wikilink")，希望培養足夠的經濟實力，以應付外部威脅。

1929年，苏联全国年产汽车不到100辆，可见即使其国内城市内部交通，也只能依靠[有轨电车](../Page/有轨电车.md "wikilink")，没有公交汽车的来源。这一年苏联开始建设高尔基汽车厂（GAZ）。史太林在1931年曾表示：

可見五年計劃的推行背後有強烈的自強意識。

## 內容

### 第一個五年計劃

第一個五年計劃的主要目標，是農業集體化。史達林認為工業的進步須由農業基礎上加以支持。故此史達林將大量小農莊合併成大型「集體農場」，以推動現代化耕作。

於1929年，集體農場數目有57000個；翌年則增加至8萬多個；自1931年開始，集體農場數一直維持至20萬個以上。

### 第二個五年計劃

第二個五年計劃在1933年至1937年推行，其主要目標則由農業轉為發展重工業，史太林為[煤](../Page/煤.md "wikilink")、[鐵](../Page/鐵.md "wikilink")、[鋼等工業生產定下高額生產目標](../Page/鋼.md "wikilink")，及下令興建發電站、礦井及油田，又改進運輸設施。結果在短短數年間工業產量有大幅度提升，以鋼產量為例，1938年的鋼產量比一次大戰前增加近三倍、比1920年的最低潮高出百多倍，僅次於[美國和](../Page/美國.md "wikilink")[德國](../Page/德國.md "wikilink")，比[英國和](../Page/英國.md "wikilink")[法國的產量總和高](../Page/法國.md "wikilink")（參看-{表}-三）；同年蘇聯在世界製造業產量的佔有率僅次於[美國](../Page/美國.md "wikilink")、[德國及](../Page/德國.md "wikilink")[英國](../Page/英國.md "wikilink")，位列第四（參看-{表}-四）；工業產量的提升帶動能源耗用量比一次大戰前增加三倍有餘、比1920年的最低潮高出十二倍，同樣僅次於[美國](../Page/美國.md "wikilink")、[德國及](../Page/德國.md "wikilink")[英國](../Page/英國.md "wikilink")（參看-{表}-五）。而[烏拉爾及](../Page/烏拉爾地區.md "wikilink")[西伯利亞地帶則建設了一批新興的工業城市](../Page/西伯利亞.md "wikilink")，推動城市化的發展（參看-{表}-六）。在“二五”计划期间，苏联总共有4500个大企业建成投入生产；工业总产值增长了120%，其中重工业增长了139%，轻工业增长了100%，农业总产值由第一个五年计划期间的负增长，增长了54%；国民收入增长了109%，人民的工资基金增加了1.5倍，集体农庄农民收入增长了2倍多，到1937年，大工业总产值比1932年增加了1倍，比1913年增加了7倍，大工业特别是机器制造工业的增长，使国民经济各部门获得了技术上的重新装备。1937年苏联的的工业总产值占世界的10.6%，仅次于美国。超过德国、英国、法国跃居欧洲第一位，世界第二位。在整个国民经济中，社会主义成分已取得了彻底胜利，国家所有制和集体所有制成为苏联社会的经济基础，公有经济在国民经济的比例占到99.8%。

| 年份  | 1913年 | 1920年 | 1930年 | 1938年 | 1940年 |
| --- | ----- | ----- | ----- | ----- | ----- |
| 苏联  | 480   | 16    | 590   | 1800  | 1770  |
| 美國  | 3180  | 4230  | 4140  | 2880  | 6080  |
| 德國  | 1400  | 760   | 1150  | 2320  | 1900  |
| 英國  | 650   | 920   | 740   | 1050  | 數據不詳  |
| 法國  | 350   | 270   | 940   | 610   | 440   |
| 意大利 | 93    | 73    | 170   | 230   | 100   |
| 日本  | 25    | 84    | 230   | 700   | 數據不詳  |

表三：**1913年至1940年蘇聯和其他列強鋼產量之比較**（單位：萬噸）

| 年份  | 1928年 | 1938年 |
| --- | ----- | ----- |
| 苏联  | 5.3%  | 9.0%  |
| 美國  | 39.3% | 31.4% |
| 德國  | 11.6% | 12.7% |
| 英國  | 9.9%  | 10.7% |
| 法國  | 6.0%  | 4.4%  |
| 意大利 | 2.7%  | 2.8%  |

表四：**1928年和1938年蘇聯和其他列強在世界製造業產量佔有率之比較**

| 年份  | 1913年 | 1920年 | 1930年 | 1938年 |
| --- | ----- | ----- | ----- | ----- |
| 苏联  | 5400  | 1430  | 6500  | 17700 |
| 美國  | 54100 | 69400 | 76200 | 69700 |
| 英國  | 19500 | 21200 | 18400 | 19600 |
| 德國  | 18700 | 15900 | 17700 | 22800 |
| 法國  | 6250  | 6500  | 9750  | 8400  |
| 日本  | 2300  | 3400  | 5580  | 9650  |
| 意大利 | 1100  | 1430  | 2400  | 2780  |

表五：**1913年至1940年蘇聯和其他列強能源耗用量之比較**（單位：煤萬噸）

| 年份  | 1920年 | 1928年 | 1938年 |
| --- | ----- | ----- | ----- |
| 苏联  | 3.1%  | 7.1%  | 20.2% |
| 美國  | 25.9% | 28.7% | 32.8% |
| 英國  | 37.3% | 38.2% | 39.2% |
| 德國  | 35.7% | 34.4% | 30.2% |
| 法國  | 15.1% | 15.3% | 15.0% |
| 意大利 | 13.2% | 16.1% | 18.2% |
| 日本  | 11.6% | 15.6% | 28.6% |

表六：**1920年至1938年蘇聯和其他列強城市化比率之比較**

### 第三個五年計劃

面對德國、日本及意大利的擴軍及隨後國際局勢的升溫，1938年開始的第三個五年計劃則集中發展軍事工業。史太林通過發展一些和軍工業近似的民用工業，以為即將到來的戰爭作準備，譬如擴大農用履帶拖拉機的生產，以便在戰時可於短時間內利用其生產線來生產輕型坦克，又或以擴大飛機生產以準備生產戰機。1938年開始，蘇聯的飛機生產較上年度增加一倍有餘，達7500架，翌年開始更保持在10000架以上（參看-{表}-七）；1940年，蘇聯的武器生產額僅次於[德國](../Page/德國.md "wikilink")，達五十億美元，已等同於英美兩國武器生產額的總和（當時蘇聯並未參戰，而英德正在開戰），1941年更為列強之首（參看-{表}-八）。第三個五年計劃在1941年因德軍入侵而中斷。

| 年份   | 1937年 | 1938年 | 1939年 | 1940年 | 1941年 | 1942年 |
| ---- | ----- | ----- | ----- | ----- | ----- | ----- |
| 飛機產量 | 3578  | 7500  | 10382 | 10565 | 15735 | 25436 |

表七：**1937年至1942年蘇聯飛機生產量**（單位：架）

| 年份  | 1940年 | 1941年 |
| --- | ----- | ----- |
| 苏联  | 50    | 85    |
| 美國  | 15    | 45    |
| 英國  | 35    | 65    |
| 德國  | 60    | 60    |
| 日本  | 10    | 20    |
| 意大利 | 7.5   | 10    |

表八：**1940年和1941年蘇聯和其他列強武器生產額之比較**（單位：億美元）

### 整體特徵

史太林模式的五年計劃存在著對經濟系統的高度控制，在產業所有權及管理方面，1936年國有制單位佔有全國工業產值的百分之九十七，農業產值的百分之七十六（集體農莊佔有另外的百分之二十）；中央對國有企業佔有百分之九十；地方則只佔有百分之十，存在著高度集中化。另外在實際生產上，中央的指令也貫徹整個生產過程，企業主管由中央委派、[國有企業的](../Page/國有企業.md "wikilink")[財政預算及年度計劃由中央審批](../Page/財政預算.md "wikilink")、國家指令代替[市場原則](../Page/市場.md "wikilink")、按政府需要生產及分配資源，譬如以[重工業優先](../Page/重工業.md "wikilink")，指令性經濟計劃完全體現其中。

## 結果

顯然，五年計劃成功令俄國迅速進行[工業化](../Page/工業化.md "wikilink")，在1940年，鋼、煤、石油、電力產量都達至新高（之後才因德軍入侵而有所回落），蘇聯已經成為繼美國之後世界第二強工業國；而在東部興建新的工業城市則有助[西伯利亞及](../Page/西伯利亞.md "wikilink")[烏拉山脈地區的開發](../Page/烏拉山脈.md "wikilink")。在优先发展重工业的同时，轻工业也得到一定发展，人民生活水平有得到改善。提前一年完成的苏联1929～1932年第一个五年计划，工业中生产资料生产年均增长28.5%，消费品生产（第二部类）年均增长速度也达11.7%，（《经济史》第3卷第285页）。1933～1937年第二个五年计划，苏联工业的消费品生产又比1932年增长100%，年均增长高达14.7%。1935年首先取消了面包和面粉、米的配给制，后来又取消了按定额发放的其它食品的配给制。1936年初，工业品也采取了类似的措施。与1932年相比，苏联1937年通过零售商品流转渠道供应的重要日用工业品和食品增长情况如下：针织外衣增长2.86倍，针织内衣3.08倍，砂糖1.92倍，动物脂肪1.59倍，糖果点心0.81倍，灌肠和熏制食品3.90倍，靴子1.18倍，长、短裤子0.97倍，自行车3.2倍，留声机10.7倍，手表7.2倍。消费品的销售额有了很大的增长。（《经济史》第4卷第611页、第610页）。

| 年份  | 1913年 | 1928年 | 1938年 |
| --- | ----- | ----- | ----- |
| 苏联  | 20    | 20    | 38    |
| 美國  | 126   | 182   | 167   |
| 英國  | 115   | 122   | 157   |
| 德國  | 85    | 128   | 144   |
| 法國  | 39    | 59    | 82    |
| 意大利 | 26    | 44    | 61    |
| 日本  | 20    | 30    | 51    |

表九：**1913年至1938年蘇聯和其他列強人均工業化指數之比較**（以1900年的英國為100）

## 其他国家实施的五年計劃

  - [中华人民共和国五年计划](../Page/中国五年计划.md "wikilink")
  - [尼泊爾五年計劃](../Page/尼泊爾五年計劃.md "wikilink")
  - [白俄羅斯五年計劃](../Page/白俄羅斯五年計劃.md "wikilink")（由[卢卡申科提出](../Page/卢卡申科.md "wikilink")）
  - [越南五年計劃](../Page/越南五年計劃.md "wikilink")
  - [韓國五年計劃](../Page/韓國五年計劃.md "wikilink")
  - [印度五年計劃](../Page/印度五年計劃.md "wikilink")
  - [馬來西亞五年計劃](../Page/馬來西亞五年計劃.md "wikilink")

## 其他国家实施的經濟計劃

  - [納粹德國四年計劃](../Page/納粹德國四年計劃.md "wikilink")

{{-}}

[cs:Pětiletka](../Page/cs:Pětiletka.md "wikilink")
[da:Femårsplan](../Page/da:Femårsplan.md "wikilink")
[de:Fünfjahresplan](../Page/de:Fünfjahresplan.md "wikilink")
[et:Viisaastakuplaan](../Page/et:Viisaastakuplaan.md "wikilink")
[is:Fimm ára áætlun](../Page/is:Fimm_ára_áætlun.md "wikilink")
[ja:五カ年計画](../Page/ja:五カ年計画.md "wikilink")
[nl:Vijfjarenplan](../Page/nl:Vijfjarenplan.md "wikilink")
[sk:Päťročnica](../Page/sk:Päťročnica.md "wikilink")
[sv:Femårsplan](../Page/sv:Femårsplan.md "wikilink")

[\*](../Category/苏联五年计划.md "wikilink")