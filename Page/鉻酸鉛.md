**鉻酸鉛**（俗稱**鉻黃**），[化學式](../Page/化學式.md "wikilink")[Pb](../Page/鉛.md "wikilink")[CrO{{sub](../Page/鉻酸根.md "wikilink")。常溫時為黃色粉末，可用作黃色顏料或是塗於馬路上的分隔線。

將[鉻酸鉀加於鉛](../Page/鉻酸鉀.md "wikilink")[鹽可以制成鉻酸鉛](../Page/鹽.md "wikilink")。它的反應式是：

  -
    Pb<sup>2+</sup> + CrO<sub>4</sub><sup>2−</sup> → PbCrO<sub>4</sub>

这一反应常用来鉴定Pb<sup>2+</sup>或
CrO<sub>4</sub><sup>2−</sup>。它可在[氢氧化钠溶液中溶解](../Page/氢氧化钠.md "wikilink")：

  -
    PbCrO<sub>4</sub> + 3 OH<sup>−</sup>   →  
    \[Pb(OH)<sub>3</sub>\]<sup>−</sup> + CrO<sub>4</sub><sup>2−</sup>

故可用来区别其他黄色的难溶铬酸盐（如BaCrO<sub>4</sub>）。

[C.I.77600.jpg](https://zh.wikipedia.org/wiki/File:C.I.77600.jpg "fig:C.I.77600.jpg")

[Category:铅化合物](../Category/铅化合物.md "wikilink")
[Category:铬酸盐](../Category/铬酸盐.md "wikilink")
[Y](../Category/顏色.md "wikilink") [L](../Category/黃色系.md "wikilink")
[L](../Category/色彩系.md "wikilink")