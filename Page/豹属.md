**豹属**（）包括四种著名的大型貓科动物：

  - [狮](../Page/狮.md "wikilink") - *Panthera leo* <small>(Linnaeus,
    1758)</small>：分布於非洲和亞洲。
  - [虎](../Page/虎.md "wikilink") - *Panthera tigris* <small>(Linnaeus,
    1758)</small>：分布於亞洲。
  - [美洲豹](../Page/美洲豹.md "wikilink") - *Panthera onca* <small>(Linnaeus,
    1758)</small>：分布於北美洲和南美洲。
  - [豹](../Page/豹.md "wikilink") - *Panthera pardus* <small>(Linnaeus,
    1758)</small>：分布於非洲和亞洲。

和其他貓科動物比較，只有這四種貓科動物有吼嘯的能力。

其他豹属的大型貓科动物，尚有：

  - [雪豹](../Page/雪豹.md "wikilink") - *Panthera uncia* 或 *Uncia uncia*
    <small>(Linnaeus, 1775)</small>

## 分类

（†符号表示已经[绝灭](../Page/绝灭.md "wikilink")）

  - **豹属 *Panthera***
      - [美洲拟狮](../Page/美洲拟狮.md "wikilink")（北美穴狮） *Panthera atrox*

      - [布氏豹](../Page/布氏豹.md "wikilink") *Panthera blytheae*

      - *Panthera carassidens*

      - [古中华豹](../Page/古中华豹.md "wikilink") *Panthera palaeosinensis*

      - *Panthera schreuderi*

      - *Panthera shawi*

      - [穴狮](../Page/穴狮.md "wikilink") （欧亚穴狮） *Panthera spelaea*

      - [托斯卡那山地狮](../Page/托斯卡那山地狮.md "wikilink")（托斯卡那美洲豹） *Panthera
        toscana*

      - [杨氏虎](../Page/杨氏虎.md "wikilink") *Panthera youngi*

      - [龙担虎](../Page/龙担虎.md "wikilink") *Panthera zdanskyi*

      - [虎](../Page/虎.md "wikilink") *Panthera tigris*

          - 古董虎 *Panthera tigris acutidens*

          - [峇里虎](../Page/峇里虎.md "wikilink") *Panthera tigris balica*

          - [爪哇虎](../Page/爪哇虎.md "wikilink") *Panthera tigris sondaica*

          - [特里尼尔虎](../Page/特里尼尔虎.md "wikilink") *Panthera tigris
            trinilensis*

          - [里海虎](../Page/里海虎.md "wikilink") *Panthera tigris virgata*

          - [西伯利亞虎](../Page/西伯利亞虎.md "wikilink") *Panthera tigris
            altaica*

          - [华南虎](../Page/华南虎.md "wikilink") *Panthera tigris amoyensis*

          - [印度支那虎](../Page/印度支那虎.md "wikilink") *Panthera tigris
            corbetti*

          - [马来亚虎](../Page/马来亚虎.md "wikilink") *Panthera tigris
            jacksoni*

          - [苏门答腊虎](../Page/苏门答腊虎.md "wikilink") *Panthera tigris
            sumatrae*

          - [孟加拉虎](../Page/孟加拉虎.md "wikilink") *Panthera tigris tigris*
            <small>（虎的指名亚种）</small>

      - [狮](../Page/狮.md "wikilink") *Panthera leo*

          - [原始狮](../Page/原始狮.md "wikilink")（中更新世欧洲穴狮） *Panthera leo
            fossilis*

          - [开普狮](../Page/开普狮.md "wikilink") *Panthera leo melanochaita*

          - [斯里兰卡狮](../Page/斯里兰卡狮.md "wikilink")（锡兰狮） *Panthera leo
            sinhaleyus*

          - [东北刚果狮](../Page/东北刚果狮.md "wikilink")（刚果狮） *Panthera leo
            azandica*

          - [西南非洲狮](../Page/西南非洲狮.md "wikilink")（加丹加狮） *Panthera leo
            bleyenberghi*

          - [德兰士瓦狮](../Page/德兰士瓦狮.md "wikilink") *Panthera leo krugeri*

          - [东非狮](../Page/东非狮.md "wikilink") *Panthera leo nubica*

          - [亚洲狮](../Page/亚洲狮.md "wikilink") *Panthera leo persica*

          - [西非狮](../Page/西非狮.md "wikilink") *Panthera leo senegalensis*

          - （野外灭绝）[巴巴里狮](../Page/巴巴里狮.md "wikilink")（北非狮） *Panthera leo
            leo* <small>（狮的指名亚种）</small>

      - [美洲豹](../Page/美洲豹.md "wikilink") *Panthera onca*

          - [更新世北美洲美洲豹](../Page/更新世北美洲美洲豹.md "wikilink") *Panthera onca
            augusta*

          - [欧洲美洲豹](../Page/欧洲美洲豹.md "wikilink") *Panthera onca
            gombaszoegensis*

          - [更新世南美洲美洲豹](../Page/更新世南美洲美洲豹.md "wikilink") *Panthera onca
            mesembrina*

          - [南美洲豹](../Page/南美洲豹.md "wikilink") *Panthera onca palustris*

          - [沿海美洲豹](../Page/沿海美洲豹.md "wikilink") *Panthera onca onca*
            <small>（美洲豹的指名亚种）</small>

      - [雪豹](../Page/雪豹.md "wikilink") *Panthera uncia*

      - [豹](../Page/豹.md "wikilink") *Panthera pardus*

          - [印度支那豹](../Page/印度支那豹.md "wikilink") *Panthera pardus
            ddelacouri*
          - [印度花豹](../Page/印度花豹.md "wikilink") *Panthera pardus fusca*
          - [华北豹](../Page/华北豹.md "wikilink") *Panthera pardus
            japonensis*
          - [斯里兰卡豹](../Page/斯里兰卡豹.md "wikilink") *Panthera pardus
            kotiya*
          - [爪哇豹](../Page/爪哇豹.md "wikilink") *Panthera pardus melas*
          - [阿拉伯豹](../Page/阿拉伯豹.md "wikilink") *Panthera pardus nimr*
          - [远东豹](../Page/远东豹.md "wikilink") *Panthera pardus
            orientalis*
          - [波斯豹](../Page/波斯豹.md "wikilink") *Panthera pardus saxicolor*
          - [非洲豹](../Page/非洲豹.md "wikilink") "Panthera pardus pardus"
            <small>（豹的指名亚种）</small>

## 参见

  - [猫科动物的标誌](../Category/猫科动物的标誌.md "wikilink")

## 參考

  -
[Category:豹亞科](../Category/豹亞科.md "wikilink")
[\*](../Category/豹属.md "wikilink")