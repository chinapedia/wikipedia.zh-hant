**凯雷集团**（Carlyle
Group），舊譯為**卡萊爾集團**，成立於1987年，[創始人為](../Page/創始人.md "wikilink")[大卫·鲁宾斯坦](../Page/大卫·鲁宾斯坦.md "wikilink")（David
M.
Rubenstein）等人，是一家位于[美國](../Page/美國.md "wikilink")[华盛顿的](../Page/华盛顿.md "wikilink")[私募股权投资公司](../Page/私募股权.md "wikilink")，主要業務包括企業[購併](../Page/購併.md "wikilink")、[房地產](../Page/房地產.md "wikilink")、槓桿財務以及創投，投資產業範圍遍及航太國防、消費零售、能源、醫療保健、科技、電信、媒體與運輸業。管理的资产超过800亿[美元](../Page/美元.md "wikilink")，给投资者的年均[回报率高达](../Page/回报率.md "wikilink")35%，被称为“总统俱乐部”。美国前任总统[老布什](../Page/老布什.md "wikilink")、英国前首相[梅杰](../Page/梅杰.md "wikilink")、菲律宾前总统[菲德尔·拉莫斯](../Page/菲德尔·拉莫斯.md "wikilink")、美国前[证券与交易委员会](../Page/美國證券交易委員會.md "wikilink")（SEC）主席[阿瑟·列维特](../Page/阿瑟·列维特.md "wikilink")、金融大鳄[乔治·索罗斯都曾在其中挂职](../Page/乔治·索罗斯.md "wikilink")，拥有较深的政治资源。在这个“俱乐部”的投资者中间，也包括美国前任总统[布什](../Page/布什.md "wikilink")、[本·拉登家族成员](../Page/本·拉登家族.md "wikilink")，只是在“9.11”恐怖袭击事件之后，出于政治上的考虑，凯雷集团结束了与[賓拉登家族表面的合作关系](../Page/賓拉登.md "wikilink")。

## 历史

凯雷最早的发起人是[史蒂芬·诺里斯](../Page/史蒂芬·诺里斯.md "wikilink")（Stephen
Norris），他在担任收购兼并公司的税务负责人时，发现收购阿拉斯加爱斯基摩人的公司能够合理避税，这让他由此投身于私人股权投资业务。通过前总统卡特的助理大卫·鲁宾斯坦的政治背景和人脉募集了第一批投资人，这便是凯雷的起点。凯雷早期并不算成功，直到在[老布什任美国总统期间](../Page/老布什.md "wikilink")，[老布什家族实际控制了凯雷](../Page/老布什.md "wikilink")，当时担任国务卿的[詹姆斯·貝克曾担任凯雷高级顾问并为其大股东之一](../Page/詹姆斯·貝克.md "wikilink")，美国前国防部长[弗兰克·卡路奇](../Page/弗兰克·卡路奇.md "wikilink")（Frank
Carlucci）曾任其董事长，前白宫预算主任迪克·达尔曼也曾担任其顾问。尽管凯雷独特的政府背景给予业务上诸多便利，但是当凯雷完成早期的一系列交易之后，其最大的基金也不过只有1亿美元。为解决此问题，1990年代中期请来了金融投资界最富有盛名的[乔治·索罗斯成为了凯雷的有限责任合伙人](../Page/乔治·索罗斯.md "wikilink")，在他的号召之下，筹集资金突然变得令人惊奇的容易。并于1990年通过政治和金融的完美结合，表面上通过前国防部长[弗兰克·卡路奇促成了凯雷在国防工业中的一项重大投资](../Page/弗兰克·卡路奇.md "wikilink")——从[美国陆军那里赢得了](../Page/美国陆军.md "wikilink")200亿美元的军火合同，凯雷集团才真正起飞，标志着[布什家族财富通过金融工具迅速成世界巨富](../Page/布什.md "wikilink")。

发起人史蒂芬·诺里斯，于1995年，虽然公司前途似锦，但在各大世界豪强的阴影下，还是选择了就离开了自己创办的公司另起炉灶。

公司的取名源自二人第一次见面讨论成立公司的酒店名，位於纽约上东区的凯雷酒店（Carlyle Hotel）。

2007年，阿布扎比投資公司Mubadala以13.5億美元，購入凱雷7.5%股權。

## 旗下全资公司

  - 凯雷资產管理有限公司（Carlyle Capital Corporation）

<!-- end list -->

  -
    成立于2006年8月，专门用于投资次级贷款，由于危机爆发，无法支付166亿美元的次级贷款，公司已经在08年3月在媒体上宣布进入破产清算。

<!-- end list -->

  - 华尔街英语（Wall street english）

<!-- end list -->

  -
    著名的意大利公司[华尔街英语就是凯雷旗下](../Page/华尔街英语.md "wikilink")100%控股公司，07年初，[老布什到访中国的一站就是参观东方广场的](../Page/老布什.md "wikilink")[华尔街英语旗舰店](../Page/华尔街英语.md "wikilink")，可见凯雷对于布什家族的重要性。

<!-- end list -->

  - Dunkin' Donuts和Baskin-Robbins。

## 收購公司

2006年，[凱雷集團併購](../Page/凱雷集團.md "wikilink")[東森電視台](../Page/東森電視台.md "wikilink")\[1\]。

凯雷目前在中国大肆收购重要国有企业如[太平洋保险](../Page/太平洋保险.md "wikilink")、[徐工集团等](../Page/徐工集团.md "wikilink")，但其出价往往大大低于企业的实际资产，引起不少争议。

在台灣曾試圖收購[日月光半導體](../Page/日月光半導體.md "wikilink")，但因出價過低及政府不支持等因素，於2007年4月宣告破局。\[2\]

後來於2007年11月9日入主[東森媒體集團旗下的](../Page/東森媒體集團.md "wikilink")[東森媒體科技](../Page/東森媒體科技.md "wikilink")。

[東森媒體科技系統台改名為](../Page/東森媒體科技.md "wikilink")[凱擘股份有限公司](../Page/凱擘股份有限公司.md "wikilink")，已於2009年9月16日被[台灣大哥大併購](../Page/台灣大哥大.md "wikilink")。

另外於2017年11月14日[東森電視台出售給茂德國際投資公司](../Page/東森電視台.md "wikilink")。\[3\]

## 在亚洲管理的基金

  - 亚洲技术投资基金
  - 凯雷亚洲合伙人，750百万美元，企业收购基金
  - 凯雷亚洲创投I，159百万美元，创业基金
  - 凯雷亚洲创投II，164百万美元，创业基金
  - 凯雷亚洲房地产基金，410百万美元

## 集团相关重要人物

### 商界

  - [G. Allen Andreas](../Page/G._Allen_Andreas.md "wikilink")，[Archer
    Daniels Midland
    Company董事长](../Page/Archer_Daniels_Midland_Company.md "wikilink")。
  - [Daniel Akerson](../Page/Daniel_Akerson.md "wikilink")，美国多家大公司董事。
  - [Joaquin Avila](../Page/Joaquin_Avila.md "wikilink")，美国著名投资银行家。
  - [Lou
    Gerstner](../Page/Lou_Gerstner.md "wikilink")，现任凯雷集团董事长，曾任IBM董事会主席和著名的食品企业Nabisco的CEO。
  - [Laurent
    Beaudoin](../Page/Laurent_Beaudoin.md "wikilink")，加拿大著名重工业巨无霸[彭巴迪](../Page/彭巴迪.md "wikilink")[BombardierCEO](../Page/Bombardier.md "wikilink")（1979年至今）。
  - [Paul
    Desmarais](../Page/Paul_Desmarais.md "wikilink")，[加拿大电力公司董事长](../Page/加拿大电力.md "wikilink")。
  - [Arthur Levitt](../Page/Arthur_Levitt.md "wikilink")，前任美国证监会主席。
  - [David M.
    Moffett](../Page/David_M._Moffett.md "wikilink")，美国最大房贷机构[房地美](../Page/房地美.md "wikilink")（[Freddie
    Mac](../Page/Freddie_Mac.md "wikilink")）的CEO，由[财政部长](../Page/财政部长.md "wikilink")[Henry
    M. Paulson
    Jr.亲自任命](../Page/Henry_M._Paulson_Jr..md "wikilink")。（2008年9月7日）
  - [Karl Otto
    Pöhl](../Page/Karl_Otto_Pöhl.md "wikilink")，前任[Bundesbank总经理](../Page/Bundesbank.md "wikilink")。
  - [Olivier
    Sarkozy](../Page/Olivier_Sarkozy.md "wikilink")（前任法国总统[尼古拉·薩科齊同父异母兄弟](../Page/尼古拉·薩科齊.md "wikilink")），凯雷财务咨询管理董事和总经理（2008年3月至今）\[4\]。
  - [Jeffrey
    Chen](../Page/Jeffrey_Chen.md "wikilink")，ASE-Taiwan台湾路透社CEO。
  - [Jason Chang](../Page/Jason_Chang.md "wikilink")，ASE Group台湾路透社董事长。

### 政界

#### 北美

  - [詹姆斯·貝克](../Page/詹姆斯·貝克.md "wikilink")，前任老布什美国国务卿，[里根和](../Page/里根.md "wikilink")[小布什总统班子成员](../Page/小布什.md "wikilink")，1993年至2005年期間擔任凯雷高级顾问。
  - [Frank C.
    Carlucci](../Page/Frank_C._Carlucci.md "wikilink")，前任美国[国防部长](../Page/国防部长.md "wikilink")（1987－1989）；国防部长[Donald
    Rumsfeld的大学摔跤伙伴](../Page/Donald_Rumsfeld.md "wikilink")，曾擔任凯雷集团董事长（1989－2005）。
  - [Richard
    Darman](../Page/Richard_Darman.md "wikilink")，老布什预算办公室部长，1993年至今擔任凯雷高级管理董事和顾问。
  - [Randal K.
    Quarles](../Page/Randal_Quarles.md "wikilink")，前任小布什秘书团成员。
  - [Allan
    Gotlieb](../Page/Allan_Gotlieb.md "wikilink")，加拿大驻美国大使（1981－1989），凯雷加拿大顾问团成员。
  - [Arthur
    Levitt](../Page/Arthur_Levitt.md "wikilink")，前任美国证监会主席，從2001年至今擔任凯雷高级顾问。
  - [Dan Senor](../Page/Dan_Senor.md "wikilink")，政治顾问
  - [Peter
    Lougheed](../Page/Peter_Lougheed.md "wikilink")，加拿大阿尔伯特（Alberta）省长（1971年－1985年）。
  - [Luis Téllez
    Kuenzler](../Page/Luis_Téllez_Kuenzler.md "wikilink")，墨西哥经济学家，现任交通部长，前任能源部长。
  - [Frank
    McKenna](../Page/Frank_McKenna.md "wikilink")，加拿大驻美国大使，前任凯雷加拿大顾问团成员。

#### 欧洲

  - [梅傑](../Page/梅傑.md "wikilink")，[英国前任](../Page/英国.md "wikilink")[总理](../Page/总理.md "wikilink")，曾擔任凯雷欧洲总经理（2002－2005）。

#### 澳大利亚

  - [Peter
    Cornelius](../Page/Peter_Cornelius.md "wikilink")，澳大利亚尼尔森新闻调查公司CEO。

#### 亚洲

  - [刘鸿儒](../Page/刘鸿儒.md "wikilink")，前任[中国证监会主席](../Page/中国证监会.md "wikilink")。
  - [Anand
    Panyarachun](../Page/Anand_Panyarachun.md "wikilink")，[泰国](../Page/泰国.md "wikilink")（两任）前总理，2004年以前凯雷亚洲顾问团成员。
  - [Fidel V.
    Ramos](../Page/Fidel_V._Ramos.md "wikilink")，[菲律宾前总统](../Page/菲律宾.md "wikilink")，2004年以前凯雷亚洲顾问团成员。
  - [他信](../Page/他信.md "wikilink")，泰国前总理，2001年以前凯雷亚洲顾问团成员。

#### 中东

  - 沙飞格·本拉登（[Shafig bin
    Laden](../Page/Shafig_bin_Laden.md "wikilink")），头号恐怖分子[賓拉登的亲哥哥](../Page/賓拉登.md "wikilink")。

### 媒体

  - [Norman
    Pearlstine](../Page/Norman_Pearlstine.md "wikilink")，[时代主编](../Page/时代杂志.md "wikilink")（1995－2005）。

## 參考文獻

## 外部連結

  - [Carlyle
    Group](https://web.archive.org/web/20070816200019/http://www.thecarlylegroup.com/)
  - [Carlyle Group](http://www.carlyle.com)
  - [凯雷投资集团](http://www.carlyle.cn)
  - [カーライル・グループ](http://www.carlyle.jp)
  - [凱雷投資集團](http://www.carlyle.tw)

[凯雷集团](../Category/凯雷集团.md "wikilink")
[Category:1987年成立的公司](../Category/1987年成立的公司.md "wikilink")
[Category:美国金融公司](../Category/美国金融公司.md "wikilink")
[Category:私人股权投资公司](../Category/私人股权投资公司.md "wikilink")

1.  <http://nccur.lib.nccu.edu.tw/handle/140.119/59905>
2.  [1](https://tw.appledaily.com/finance/daily/20070418/23404603)
3.  [2](https://www.businesstoday.com.tw/article/category/80392/post/201711080029/東森電視新老闆張高祥%20500億身家解密)
4.  Nick Clarck, [Carlyle poaches Olivier
    Sarkozy](http://www.independent.co.uk/news/business/news/carlyle-poaches-olivier-sarkozy-790939.html),
    *[The Independent](../Page/The_Independent.md "wikilink")*, 4 March
    2008