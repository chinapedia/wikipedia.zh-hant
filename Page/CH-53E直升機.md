**西科斯基CH-53E「超級种马」**（Sikorsky CH-53E Super
Stallion），是[美國](../Page/美國.md "wikilink")[西科斯基飞行器公司所出品的軍用](../Page/西科斯基飞行器公司.md "wikilink")[直升機](../Page/直升機.md "wikilink")，是美军最大和最重的[直升机](../Page/直升机.md "wikilink")，海军官兵们戏称它为「飓风制造者」，主要为[美国海军陆战队服务](../Page/美国海军陆战队.md "wikilink")，充当运输重型货物及吊掛重型機具等之任务。

**西科斯基MH-53E「海龍」**（Sea
Dragon）是超級种马的改進版，作為美國海軍的長程[水雷掃蕩版](../Page/水雷.md "wikilink")，專門特化進行空中掃蕩水雷的海上任務。加裝大型[適型油箱取代外掛油箱](../Page/適型油箱.md "wikilink")、後逃生艙蓋、AN/APN-217雷達，改良燃油管理系統、尾旋翼。特色是加裝了很多掃雷具的掛點和匯流排接口，通常海龍佈署於兩棲攻擊艦和航母上，作為應對水雷的手段，也能負擔一些搜救和運輸任務。

目前另有研發中的**[CH-53K](../Page/CH-53K直升機.md "wikilink")**，具有3部7500轴马力的[涡轴发动机](../Page/涡轴发动机.md "wikilink")，新型[旋翼材料和更宽敞的机舱](../Page/旋翼.md "wikilink")。
[缩略图](https://zh.wikipedia.org/wiki/File:US_Navy_111031-N-JL506-553_An_MH-53E_Super_Stallion_helicopter_from_the_Japan_Maritime_Self-Defense_Force_\(JMSDF\)_Helicopter_Mine_Squadron_111_land.jpg "fig:缩略图")

## 使用者

  -

<!-- end list -->

  - [美國海軍陸戰隊](../Page/美國海軍陸戰隊.md "wikilink")
  - [美國海軍](../Page/美國海軍.md "wikilink")

<!-- end list -->

  -

<!-- end list -->

  - [日本海上自衛隊](../Page/日本海上自衛隊.md "wikilink")

## 規格（CH-53E）

[MH-53E_HM-15_MK-103_mine_sweeping_mission.jpg](https://zh.wikipedia.org/wiki/File:MH-53E_HM-15_MK-103_mine_sweeping_mission.jpg "fig:MH-53E_HM-15_MK-103_mine_sweeping_mission.jpg")
[CH-53_Sea_Stallion.jpg](https://zh.wikipedia.org/wiki/File:CH-53_Sea_Stallion.jpg "fig:CH-53_Sea_Stallion.jpg")
[Aerial_refueling_CH-53_DF-SD-06-02984.JPG](https://zh.wikipedia.org/wiki/File:Aerial_refueling_CH-53_DF-SD-06-02984.JPG "fig:Aerial_refueling_CH-53_DF-SD-06-02984.JPG")

## 參與救災

2009年[中華民國發生因](../Page/中華民國.md "wikilink")[莫拉克颱風引起的](../Page/莫拉克颱風.md "wikilink")[八八風災時](../Page/八八風災.md "wikilink")，為了運輸工程機具進入對外交通受阻的災區救災，但因為[中華民國陸軍的](../Page/中華民國陸軍.md "wikilink")[CH-47無法吊掛大噸位的工程機具](../Page/CH-47.md "wikilink")，故向外國爭取更大運載能力的運輸直升機以利救災。美國與中國大陆曾表示要派遣運輸直升機赴台救災，中國大陆方面曾提出派遣民間使用或向俄羅斯租借[Mi-26赴台救災](../Page/Mi-26.md "wikilink")，但因台灣內部有所爭議而予以婉拒；而台灣媒體一度盛傳美軍會派遣比台灣CH-47運載能力更佳的CH-53「超級種馬」來台救災，但最後由[美國海軍陸戰隊派出比](../Page/美國海軍陸戰隊.md "wikilink")「超級種馬」的運載能力略低的同型機「海龍」飛抵台灣救災。

## 参考文献

<div class="references-small">

<references />

</div>

## 另見

  - [夏威夷直升机坠毁事件](../Page/夏威夷直升机坠毁事件.md "wikilink")
  - [MH-53直升機](../Page/MH-53直升機.md "wikilink")
  - [直-8](../Page/直-8.md "wikilink")
  - [直-9](../Page/直-9.md "wikilink")
  - [Mi-24](../Page/Mi-24.md "wikilink")
  - [CH-53直升機](../Page/CH-53直升機.md "wikilink")

[Category:美國軍用直升機](../Category/美國軍用直升機.md "wikilink")
[Category:重型直升機](../Category/重型直升機.md "wikilink")
[Category:賽考斯基飛行器](../Category/賽考斯基飛行器.md "wikilink")