**黄河夺淮**指的是[黃河在](../Page/黃河.md "wikilink")[南宋](../Page/南宋.md "wikilink")[建炎二年](../Page/建炎.md "wikilink")（1128年）至清[咸豐五年](../Page/咸豐.md "wikilink")（1855年）間以[淮河的河道作出海口的历史](../Page/淮河.md "wikilink")。

[北宋之前](../Page/北宋.md "wikilink")，黃河與淮河大抵相安無事。雖自[西汉以来](../Page/西汉.md "wikilink")，淮河多为黃河所侵，“多在泗（州）、凤（阳）以上”，“颍（上）、亳（县）、怀远之间”，但黃河少有入淮之事。

北宋末年，金兵南侵，東京留守[杜充](../Page/杜充.md "wikilink")，決黃河，自[泗入淮](../Page/泗水_\(河流\).md "wikilink")，以阻金兵。\[1\]南宋控制黃河奪得黃河的水源。[紹定五年](../Page/紹定.md "wikilink")(金[天興元年](../Page/天興.md "wikilink")、1232年)，蒙古軍取[金國睢州](../Page/睢州_\(金朝\).md "wikilink")，圍[歸德府](../Page/歸德府.md "wikilink")。蒙古大軍在今商丘縣西北決河，河水奪[濉水入泗](../Page/濉水.md "wikilink")。稍後蒙古軍又在今開封市北決河，河水奪渦水入淮。元世祖至元二十三年（1286年），黃河在開封、祥符、陳留、杞、太康、通許、鄢陵、扶溝、洧川、尉氏、陽武、延津、中牟、原武、睢州等縣15處決口\[2\]，河水共侵奪穎、泗、渦、淮四條河流的河道入海。“每淮水盛时，西风激浪，白波如山，淮扬数百里中，公私惶惶，莫敢安枕者，数百年矣”，明隆慶年間，[潘季馴治河](../Page/潘季馴.md "wikilink")，黃河方固定於明清河道達三百年。破釜塘、白水塘、富陵湖、泥墩湖、万家湖等陂塘和小湖连接成[洪泽湖](../Page/洪泽湖.md "wikilink")。[淮河长期不再有入海口](../Page/淮河.md "wikilink")，改在[三江營匯入](../Page/三江營.md "wikilink")[長江](../Page/長江.md "wikilink")。

[清朝](../Page/清朝.md "wikilink")[咸豐五年](../Page/咸豐.md "wikilink")（1855年），黃河再度在[河南銅瓦廂決口](../Page/河南.md "wikilink")[改道](../Page/咸丰黄河大改道.md "wikilink")，此後黃河大致以[山東](../Page/山東.md "wikilink")[济水](../Page/济水.md "wikilink")（古代山东大清河乃济水，并非今天大清河）河道為入海口至今。\[3\]

在此前和此後，統治者也以扒開堤岸的方式迫使黃河改道，但都不稱為黄河夺淮。

## 黃河故道

今日，黃河故道在江苏省穿过[徐州市](../Page/徐州市.md "wikilink")、[宿迁市和](../Page/宿迁市.md "wikilink")[淮安市](../Page/淮安市.md "wikilink")、[盐城市](../Page/盐城市.md "wikilink")，其中，穿过徐州、宿迁的黄河故道即早年泗水河道，而穿过淮安、盐城的河道即淮河故道（古淮河），曾作为[南宋与](../Page/南宋.md "wikilink")[金国的分界线](../Page/金国.md "wikilink")，2008年，淮安在市区北面的古淮河河道上建筑[中国南北分界线标志物](../Page/秦嶺淮河線.md "wikilink")。

## 注釋

## 相關條目

  - [杜充](../Page/杜充.md "wikilink")
  - [蓄清刷黄](../Page/蓄清刷黄.md "wikilink")
  - [洪澤湖](../Page/洪澤湖.md "wikilink")
  - [黄河改道](../Page/黄河改道.md "wikilink")
  - [蔣介石炸花園口](../Page/花园口决堤事件.md "wikilink")

[Category:黃河](../Category/黃河.md "wikilink")
[Category:淮河水系](../Category/淮河水系.md "wikilink")
[Category:江苏水利](../Category/江苏水利.md "wikilink")
[Category:河南水利](../Category/河南水利.md "wikilink")
[Category:江苏历史](../Category/江苏历史.md "wikilink")
[Category:河南历史](../Category/河南历史.md "wikilink")
[Category:金朝](../Category/金朝.md "wikilink")
[Category:元朝](../Category/元朝.md "wikilink")
[Category:明朝水利](../Category/明朝水利.md "wikilink")
[Category:清朝水利](../Category/清朝水利.md "wikilink")
[Category:中国水灾](../Category/中国水灾.md "wikilink")

1.  脫脫， 阿魯圖 《宋史本紀》
2.  畢沅《續資治通鑑》
3.