**XnView**為一款支持圖片瀏覽、轉換和編輯的[多平台軟體](../Page/多平台軟體.md "wikilink")。该软件支持改變[用戶界面](../Page/用戶界面.md "wikilink")[語言以及自定义](../Page/語言.md "wikilink")[工具栏](../Page/工具栏.md "wikilink")[按鈕与](../Page/按鈕.md "wikilink")[面板](../Page/面板.md "wikilink")。它支援讀取超過500種的[圖檔格式及部份](../Page/圖檔格式.md "wikilink")[音频文件格式及](../Page/音频文件格式.md "wikilink")[视频文件格式](../Page/视频文件格式.md "wikilink")，亦支援寫入50種圖片格式。提供免費的個人使用及簡易操作。可將壓縮檔以資料夾方式開啟、依[EXIF及](../Page/EXIF.md "wikilink")[IPTC作圖片搜尋](../Page/IPTC.md "wikilink")、圖片相似比對、對圖片作[批次命名](../Page/批次命名.md "wikilink")、格式轉換及套用影像調整，並支援[無損耗旋轉](../Page/JPEG#無損耗旋轉_\(lossless_JPEG_rotation\).md "wikilink")。類似功能之[軟體有](../Page/軟體.md "wikilink")[ACDSee等](../Page/ACDSee.md "wikilink")。

## XnView MP

XnView
MP是该软件的跨平台版本，支持[Windows](../Page/Windows.md "wikilink")、[Linux和](../Page/Linux.md "wikilink")[Mac平台](../Page/Mac.md "wikilink")。它使用了[Qt库进行开发](../Page/Qt.md "wikilink")，目前正在公開測試，版本是0.92。相比起其旧版程序和其它的免费图像查看器，它的特点是能在默认码表为[ASCII的](../Page/ASCII.md "wikilink")[Unicode操作系统上识别包含有Unicode字符的路径和文件名](../Page/Unicode.md "wikilink")，并在全平台上支持多语言界面。XnView社区的成员mezich和thibaud为这款软件贡献了新的工具条按钮和图标。\[1\]

## 特性

XnView兼容了著名看图软件[ACDSee的图片注释方法](../Page/ACDSee.md "wikilink")，即在图片文件夹下新建一个隐藏的文本文件，名为
[Descript.ion](../Page/Descript.ion.md "wikilink")，将逐个图片的注释存储在这一文件，并能够在图片浏览和查看时显示。

格式如下：

` 文件名1.jpg 注释内容`
` 文件名2.png 文件2注释`

## 参见

  - [ACDSee](../Page/ACDSee.md "wikilink")
  - [FastStone Image
    Viewer](../Page/FastStone_Image_Viewer.md "wikilink")
  - [IrfanView](../Page/IrfanView.md "wikilink")
  - [Picasa](../Page/Picasa.md "wikilink")

## 參考

## 外部連結

  - [XnView 網站](https://www.xnview.com/)

[Category:免費軟體](../Category/免費軟體.md "wikilink")
[Category:綠色軟體](../Category/綠色軟體.md "wikilink")
[Category:圖像檢視器](../Category/圖像檢視器.md "wikilink")

1.