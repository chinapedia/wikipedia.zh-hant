**臺23線**，是[臺灣一條連接](../Page/臺灣.md "wikilink")[花東縱谷平原與花東海岸的](../Page/花東縱谷平原.md "wikilink")[省道](../Page/台灣省道.md "wikilink")，中途穿越[泰源盆地](../Page/泰源盆地.md "wikilink")。西起[花蓮縣](../Page/花蓮縣.md "wikilink")[富里鄉](../Page/富里鄉.md "wikilink")[臺9線處](../Page/臺9線.md "wikilink")，東至[臺東縣](../Page/臺東縣.md "wikilink")[東河鄉連接](../Page/東河鄉_\(臺灣\).md "wikilink")[臺11線](../Page/臺11線.md "wikilink")，故又稱**富東公路**或**東富公路**，全長45.503公里。

## 簡介

早期與[TW_PHW11a.svg](https://zh.wikipedia.org/wiki/File:TW_PHW11a.svg "fig:TW_PHW11a.svg")[台11甲線](../Page/台11線#甲線.md "wikilink")(光豐公路)、[瑞港公路](../Page/瑞港公路.md "wikilink")（花64線）分屬三條橫貫[海岸山脈的重要公路之一](../Page/海岸山脈.md "wikilink")。但自從[玉長公路](../Page/玉長公路.md "wikilink")（花蓮玉里至台東長濱）通車之後，重要性已大不如前。

## 經過鄉鎮市區、里程數

  - [花蓮縣](../Page/花蓮縣.md "wikilink")

<!-- end list -->

  - [富里鄉](../Page/富里鄉.md "wikilink"):富里0.0k（**起點**，岔路）→ 富里大橋 → 無毛山 →
    永豐4.948k → 豐南6.648k →
    石門橋（[小天祥](../Page/小天祥_\(花蓮縣富里鄉\).md "wikilink")）→
    望通嶺10.648k → 第十三號橋 → 花東縣界16.563k（海岸山脈稜線）

<!-- end list -->

  - [臺東縣](../Page/臺東縣.md "wikilink")

<!-- end list -->

  - [東河鄉](../Page/東河鄉_\(臺灣\).md "wikilink"):縣界 → 公路最高點（海拔636.6m）18.5k→
    第十四號橋 → 東河農場26.000k → 德高（斷層景觀區）→ [北溪](../Page/北源村.md "wikilink")
    →
    [美蘭](../Page/北源村.md "wikilink")→[北源](../Page/北源村.md "wikilink")（[泰源監獄](../Page/泰源監獄.md "wikilink")）→
    泰源（本部落）39.600k →
    [登仙峽](../Page/登仙峽.md "wikilink")（[泰源幽谷](../Page/泰源幽谷.md "wikilink")）
    → 隧道部落（[馬武窟溪谷](../Page/馬武窟溪.md "wikilink")） →
    [新東河橋](../Page/新東河橋.md "wikilink")45.965k（**終點**，岔路）

## 沿線景點

  - 泰源遺址
  - [泰源幽谷](../Page/泰源幽谷.md "wikilink")（[小天祥](../Page/小天祥.md "wikilink")）
  - [東河橋遊憩區](../Page/舊東河橋.md "wikilink")
  - [小馬隧道](../Page/小馬隧道.md "wikilink")
  - [登仙峽](../Page/登仙峽.md "wikilink")
  - [登仙橋休憩區](../Page/登仙橋休憩區.md "wikilink")
  - [國軍退除役官兵輔導委員會東河休閒農場](../Page/國軍退除役官兵輔導委員會.md "wikilink")
  - 北源農場
  - 美蘭休閒農場
  - 四維橋
  - 五號橋頭
  - 豐南小田莊
  - [台糖東山舊糖廠](../Page/台糖.md "wikilink")

## 參考文獻

## 外部連結

[Category:花蓮縣省道](../Category/花蓮縣省道.md "wikilink")
[Category:台東縣省道](../Category/台東縣省道.md "wikilink")