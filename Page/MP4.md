**MP4**或稱**[MPEG-4第](../Page/MPEG-4.md "wikilink")14部分**（英語：MPEG-4 Part
14）是一種標準的數位[多媒體容器格式](../Page/多媒體.md "wikilink")。MPEG-4第14部分的擴充名為**`.mp4`**，以儲存[數位音訊及](../Page/數位音訊.md "wikilink")[數位影片為主](../Page/數位影片.md "wikilink")，但也可以儲存[字幕和静止图像](../Page/字幕.md "wikilink")。因其可容納支持[位元流的視頻流](../Page/位元流.md "wikilink")（如[高級視頻編碼](../Page/H.264/MPEG-4_AVC.md "wikilink")），MP4可以在網路傳輸時使用[串流式傳輸](../Page/串流媒體.md "wikilink")。
雖然攜帶式媒體播放器有時會被稱作"MP4播放器"，但並不僅限於播放MP4格式的影片，有些也可以播放[AMV](../Page/AMV.md "wikilink")、[MKV或其他影片格式](../Page/MKV.md "wikilink")
擴展名為`.flv`或`.f4v`并同樣基於的[Flash視頻格式與MPEG](../Page/FLV.md "wikilink")-4第14部分極為相似，多數情況下直接變更擴展名為`.mp4`也能夠正常播放

## 版本

| 版本         | 發佈日期  | 標準                            | 描述                            |
| ---------- | ----- | ----------------------------- | ----------------------------- |
| MP4文件格式第1版 | 2001年 | link-en|ISO/IEC 14496-1:2001  | MPEG-4 第1部分（Systems）          |
| MP4文件格式第2版 | 2003年 | link-en|ISO/IEC 14496-14:2003 | MPEG-4 第14部分（MP4 file format） |

## 擴展名

雖然被官方標準定義的唯一擴展名是`.mp4`，但第三方通常會使用各種擴展名來指示文件的內容：

  - 同時擁有音訊視訊的MPEG-4文件通常使用標準擴展名`.mp4`
  - 僅有音頻的MPEG-4文件會使用`.m4a`擴展名，對於不受保護的內容更是如此
      - 通过iTunes
        Store销售的被[数字版权管理加密的僅擁有音频流的MPEG](../Page/数字版权管理.md "wikilink")-4文件會使用`.m4p`作為扩展名
      - 包含章节标记，图像和[超链接的](../Page/超链接.md "wikilink")[有声读物](../Page/有声读物.md "wikilink")、[播客文件或是](../Page/播客.md "wikilink")[元數據會使用](../Page/元數據.md "wikilink")`.m4b`作為擴展名，但有時候也會使用`.m4a`作為擴展名。使用`.m4a`擴展名的文件不能使用書籤來記錄播放位置，而使用`.m4b`擴展名的就可以做到這一點
      - [蘋果公司的](../Page/蘋果公司.md "wikilink")[iPhone手機使用MPEG](../Page/iPhone.md "wikilink")-4音頻作為其電話鈴聲，但擴展名為`.m4r`而不是`.m4a`
  - 原始的MPEG-4視頻流會使用`.m4v`擴展名，但該擴展名也被用於僅有視頻流的MPEG-4文件
  - 移動電話使用3GP視頻格式（MPEG-4
    第12部分，又被稱為MPEG-4/JPEG2000基本媒體文件格式），它類似於MP4格式但使用`.3gp`或是`.3g2`擴展名，該格式還存儲非MPEG-4數據（如[AMR](../Page/AMR.md "wikilink")，TX3G）

## 數據流

大部分數據可以通過專用數據流嵌入到MP4文件中，因此MP4文件中包含了一個單獨的用於儲存流信息的軌道。已註冊的用於MPEG-4
第14部分的編解碼器已經在MP4註冊機構\[1\]的網站上列出，但其中依舊有部分格式沒有得到大多數MP4播放器的支持，目前得到廣泛支持的編解碼器或數據流格式有：

  - 視頻格式：[高效率視訊編碼（H.265/HEVC/MPEG-H第2部分）](../Page/H.265.md "wikilink")、[高級視訊編碼（H.264/AVC/MPEG-4第10部分）和](../Page/H.264.md "wikilink")

<!-- end list -->

  -
    新加入的视频格式：[RV60（RealVideo
    HD/RMHD）](../Page/RealVideo.md "wikilink")、[VP9等](../Page/VP9.md "wikilink")
    被淘汰的視頻格式：[H.263](../Page/H.263.md "wikilink")、[H.262（MPEG-2第2部分）](../Page/H.262.md "wikilink")、[MPEG-1和](../Page/MPEG-1.md "wikilink")[H.261](../Page/H.261.md "wikilink")

<!-- end list -->

  - 音頻格式：[高級音頻編碼（AAC）](../Page/AAC.md "wikilink")

<!-- end list -->

  -
    同時也兼容所有中指定的音頻對象類型，例如[音頻無損編碼（MPEG-4
    ALS）](../Page/MPEG-4_ALS.md "wikilink")、、[码激励线性预测（CELP）](../Page/码激励线性预测.md "wikilink")、[MPEG-1或MPEG-2
    音频层III（MP3）](../Page/MP3.md "wikilink")、[MPEG-1
    音频层II（MP2）](../Page/MPEG-1_Audio_Layer_II.md "wikilink")、
    此外也可兼容在MP4注册机构网站上所注册过的音频格式，如[蘋果無損音頻編碼（ALAC）](../Page/ALAC.md "wikilink")、[Opus](../Page/Opus.md "wikilink")、[AC-3等](../Page/AC-3.md "wikilink")

<!-- end list -->

  - 字幕格式：和DVD字幕（`.sub`和`.idx`）

## 元數據

MP4文件不僅可以包含由格式标准定义的元数据，还可包含遵從[可扩展元数据平台（XMP）标准的元数据](../Page/XMP.md "wikilink")。

## 外部連結

  - RFC 4337 - *MIME Type Registration for MPEG-4*

[Category:MPEG](../Category/MPEG.md "wikilink")
[Category:視頻文件格式](../Category/視頻文件格式.md "wikilink")
[Category:音频格式](../Category/音频格式.md "wikilink")

1.  [MP4註冊機構（The 'MP4' Registration
    Authority）](http://mp4ra.org/index.html)