`|photo2c = TabrizBazar.jpg{{!}}`
`|photo3a = 20160313-IMG 0359.jpg{{!}}`
`|photo4a = Arg-e Tabriz.JPG{{!}}`
`|photo4b = Poets Mausoleum in Tabriz.jpg{{!}}Sacramento Memorial Auditorium`
`|foot_montage =}}`

\-----------\> |image_map = |mapsize = |map_caption = |pushpin_map =
Iran |pushpin_label_position = bottom |pushpin_mapsize =
|pushpin_map_caption = |pushpin_relief = 1 |coordinates =
|subdivision_type = 國家 |subdivision_name =
[伊朗](../Page/伊朗.md "wikilink") |subdivision_type1 = 行政區
|subdivision_type2 = [省](../Page/伊朗省份.md "wikilink")
|subdivision_type3 = [縣](../Page/縣_\(伊朗\).md "wikilink")
|subdivision_type4 = [區](../Page/區_\(伊朗\).md "wikilink")
|subdivision_name1 = 3 |subdivision_name2 =
[東阿塞拜疆省](../Page/東阿塞拜疆省.md "wikilink")
|subdivision_name3 = 大不里士縣 |subdivision_name4 = 大不里士中心區
|established_title = |established_date = |government_footnotes =
|leader_title = 市長 |leader_name = Iraj Shahin-Baher |leader_title1 =
議會主席 |leader_name1 = Shakur Akbarnejad |unit_pref = |area_footnotes
= |area_magnitude = |area_total_km2 =324 |area_land_km2 =
|area_water_km2 = |area_water_percent = |area_urban_km2 = 2356
|area_metro_km2 = |elevation_footnotes = |elevation_m = 1351.4
|elevation_ft = |population_urban = 1,558,693\[1\] |population_metro
= 1,773,023\[2\] |population_as_of = 2016 Census |population_est =
|population_est_as_of = |population_footnotes =
|population_density_km2 = auto |population_density_sq_mi = auto
|population_density_urban_km2 = |population_blank1_title = 排名
|population_blank1 = 第六名 |population_demonym = Tabrizian, Təbrizli,
Tabrizi |postal_code_type = [郵區編號](../Page/郵區編號.md "wikilink")
|postal_code = 51368 |area_code = 041 |blank_name =
[氣候](../Page/柯本氣候分類法.md "wikilink") |blank_info =
[Dsa](../Page/溫帶大陸性濕潤氣候.md "wikilink") |website = [Tabriz
municipality](http://tabriz.ir/) |image_size = |leader_title2 = 國會
|leader_name2 = Alirezabeighi, Saei, Farhanghi, Bimegdar, Pezeshkian &
Saeidi |timezone = [伊朗標準時間](../Page/伊朗標準時間.md "wikilink") |utc_offset =
+3:30 |timezone_DST = [伊朗夏令時](../Page/伊朗標準時間.md "wikilink")
|utc_offset_DST = +4:30 }}

**大不里士**，（；），中国古称**桃里寺**，[伊朗西北部城市](../Page/伊朗.md "wikilink")，是[东阿塞拜疆省的首府](../Page/东阿塞拜疆省.md "wikilink")。位于[库赫·塞汉特高原之上](../Page/库赫·塞汉特高原.md "wikilink")。海拔1350米。大不里士位于一个山谷内。冬季寒冷，夏季温和，被认为是一个避暑胜地。

## 历史

据传说，791年大不里士曾是[阿拔斯王朝的](../Page/阿拔斯王朝.md "wikilink")[哈里发](../Page/哈里发.md "wikilink")[哈伦·拉希德的妻子的住地](../Page/哈伦·拉希德.md "wikilink")。这座城市曾是13、14世纪時期[伊兒汗國及](../Page/伊兒汗國.md "wikilink")17世紀時期[薩法維王朝的](../Page/薩法維王朝.md "wikilink")[首都](../Page/首都.md "wikilink")。

在[第二次世界大战期间](../Page/第二次世界大战.md "wikilink")，大不里士是[苏联实际控制的](../Page/苏联.md "wikilink")[阿塞拜疆独立共和国的首府](../Page/阿塞拜疆独立共和国.md "wikilink")。

## 氣候

## 经济

大不里士拥有[石油工业](../Page/石油工业.md "wikilink")，这里有[输油管通往](../Page/输油管.md "wikilink")[德黑兰](../Page/德黑兰.md "wikilink")。

## 各高等学府网站

1.  [Sahand University of
    Technology](https://web.archive.org/web/20061212222721/http://www.sut.ac.ir/)
2.  [Tabriz University of Medical Sciences](http://www.tbzmed.ac.ir/)
3.  [Tabriz University of Tarbiat Moallem](http://www.azaruniv.edu/)
4.  [University of Tabriz](http://www.tabrizu.ac.ir/)
5.  [Islamic Azad University of Tabriz](http://www.iaut.ac.ir/)
6.  [Tabriz Islamic Arts
    University](https://web.archive.org/web/20040611123309/http://www.tiartuni.ac.ir/)
7.  [University College of Nabi Akram](http://www.ucna.ac.ir)

## 外部链接

  - [大不里士](http://lexicorient.com/e.o/tabriz.htm)

[Category:东阿塞拜疆省](../Category/东阿塞拜疆省.md "wikilink")
[Category:伊朗城市](../Category/伊朗城市.md "wikilink")

1.
2.