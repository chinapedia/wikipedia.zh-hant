[Maxwell_Perkins_NYWTS.jpg](https://zh.wikipedia.org/wiki/File:Maxwell_Perkins_NYWTS.jpg "fig:Maxwell_Perkins_NYWTS.jpg")
**麥克斯威爾‧柏金斯** (**Maxwell Evarts Perkins**,)
是[美国出版史上一位傳奇人物和](../Page/美国.md "wikilink")[編輯](../Page/編輯.md "wikilink")。

他曾為[佛蘭西斯·史考特·基·費茲傑羅](../Page/佛蘭西斯·史考特·基·費茲傑羅.md "wikilink")(F. Scott
Fitzgerald)、[歐內斯特·海明威](../Page/歐內斯特·海明威.md "wikilink")(Ernest
Hemingway)、[湯瑪斯·伍爾夫](../Page/湯瑪斯·伍爾夫.md "wikilink")(Thomas
Wolfe)、[瑪喬麗·金楠·勞林斯](../Page/瑪喬麗·金楠·勞林斯.md "wikilink")(Marjorie
Kinnan Rawlings)等著名作家編書。

柏金斯出生於[紐約](../Page/紐約.md "wikilink")。1907年從[哈佛大學畢業後](../Page/哈佛大學.md "wikilink")，他的第一份工作是在[紐約時報當記者](../Page/紐約時報.md "wikilink")；1910年，他進入[斯克裡布納之子公司擔任廣告經理一職](../Page/斯克裡布納之子公司.md "wikilink")；自1914年起轉戰編輯部，從此展開他一生最重要的編輯事業。

1917年，柏金斯開始和一位有志創作的年輕人通信，為了能順利出版他的作品，柏金斯歷經一番奮戰，不顧出版社文學顧問布羅奈爾的反對，堅持買下這位年輕人處女作的版權。最後經過數次的改寫，終於在1920年－《[塵世樂園](../Page/塵世樂園.md "wikilink")》(*This
Side of
Paradise*)一書進了印刷廠。此書出版後深獲好評，被認為是反映時代的一部佳作，這個年僅24歲、原本默默無聞的年輕人－[佛蘭西斯·史考特·基·費茲傑羅](../Page/佛蘭西斯·史考特·基·費茲傑羅.md "wikilink")，自此一舉成名，也是當時[斯克裡布納之子公司出版記錄上最年輕的作家](../Page/斯克裡布納之子公司.md "wikilink")。柏金斯與費茲傑羅，兩人聯手宣告年輕爵士時代的來臨；也從此展開柏金斯與多位重要作家的共事經驗。

1925年，費茲傑羅在[巴黎認識一位剛出道的窮作家](../Page/巴黎.md "wikilink")，該作家正在撰寫自己的第一本長篇小說，於是費茲傑羅將他推薦給柏金斯；他就是日後在文學領域佔有舉足輕重地位、並獲得[諾貝爾文學獎的](../Page/諾貝爾文學獎.md "wikilink")[歐內斯特·海明威](../Page/歐內斯特·海明威.md "wikilink")。1926年，柏金斯幫助海明威出版他的成名作《[太陽照樣升起](../Page/太陽照樣升起.md "wikilink")》(台譯《妾似朝陽又照君》*The
Sun Also Rises*)，正式將這位小說家引入美國文壇。

另一位著名小說家[湯瑪斯·伍爾夫](../Page/湯瑪斯·伍爾夫.md "wikilink")，亦是藉由柏金斯的挖掘與琢磨，始在文壇大放光芒。他曾在作品的扉頁上寫著，要把書獻給「一位偉大的編輯和一位勇敢而誠實的人。當作者陷入極度的失望和懷疑中時，他不吝於給予堅定的支持，不讓作者在絕望中輕易放棄。」藉此肯定柏金斯的貢獻。伍爾夫並在自己《你再也無法回家》(*You
can't Go Home Again*)一書中，藉一個虛擬的人物愛德華(Foxhall
Edwards)生動地刻畫這位編輯，使他的傳奇形象躍然紙上。

在柏金斯長達25年的編輯生涯中，他引導無數才華洋溢的作家開創寫作顛峰；他不僅是一位專業的編輯，同時身兼作者的精神支柱、忠實摯友、資金贊助者及事業生涯的引導者等多重身份。柏金斯認為編輯工作要盡己所能地挖掘、栽培作者的才華，並用各種方式協助一部優秀作品的完成；儘管他在編輯著作上花費許多的心血，但柏金斯自始至終都堅持自己的信念－「書是屬於作者的」。

柏金斯與費茲傑羅的信件往來，被收集成《*Dear Scott, Dear Max: The Fitzgerald-Perkins
Correspondence*》(John Kuehl /Jackson Bryer,1991)一書出版，另一本性質類似的作品《*The
Only Thing That Counts*》(Matthew J. Bruccoli/Robert W.
Trogdon)，則是談論有關柏金斯和海明威之間的關係。A. Scott
Berg在1978年出版了一本頗暢銷的柏金斯傳記《*Max Perkins: Editor of
Genius*》，當中記錄了他的一生與經歷。在《[編輯寫給作者的信：柏金斯信件集](../Page/編輯寫給作者的信：柏金斯信件集.md "wikilink")》(*Editor
to Author: The Letters of Maxwell
Perkins*)一書中，集結了*柏金*斯有關編輯工作的信件，充分流露他卓越的能力與品味、領導文化潮流的強烈風格，更加奠定這位編輯巨匠的地位；此書是對編輯行業或角色有興趣的人，都該一讀的佳作。

1947年6月17日，柏金斯逝世。他生前在[康乃迪克州的](../Page/康乃迪克州.md "wikilink")[新卡納的住處](../Page/新卡納.md "wikilink")，目前已被列入[國家歷史古蹟管理處](../Page/國家歷史古蹟管理處.md "wikilink")。

## 外部連結

  - [Thomas Wolfe Memorial - Maxwell
    Perkins](https://web.archive.org/web/20080419005106/http://www.ah.dcr.state.nc.us/sections/hs/wolfe/perkins.htm)
  - [Family tree of William Maxwell
    Perkins](http://worldconnect.rootsweb.com/cgi-bin/igm.cgi?op=PED&db=glencoe&id=I40088)

[P](../Category/美国编辑.md "wikilink") [P](../Category/美国作家.md "wikilink")
[P](../Category/哈佛大學校友.md "wikilink")