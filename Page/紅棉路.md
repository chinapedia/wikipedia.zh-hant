[Cotton_Tree_Drive_at_night.JPG](https://zh.wikipedia.org/wiki/File:Cotton_Tree_Drive_at_night.JPG "fig:Cotton_Tree_Drive_at_night.JPG")
[Hong_Kong_Squash_Centre.jpg](https://zh.wikipedia.org/wiki/File:Hong_Kong_Squash_Centre.jpg "fig:Hong_Kong_Squash_Centre.jpg")
[HK_Cotton_Tree_Drive_Lippo_Ctr_fire_station.JPG](https://zh.wikipedia.org/wiki/File:HK_Cotton_Tree_Drive_Lippo_Ctr_fire_station.JPG "fig:HK_Cotton_Tree_Drive_Lippo_Ctr_fire_station.JPG")以西的一條行人天橋上，北望山下的紅棉路，有坐落於東邊的消防局及[力寶中心](../Page/力寶中心.md "wikilink")\]\]
**紅棉路**（英文：**Cotton Tree
Drive**），是由[香港](../Page/香港.md "wikilink")[港島](../Page/港島.md "wikilink")[金鐘前往](../Page/金鐘.md "wikilink")[半山區的一條交通幹線](../Page/半山區.md "wikilink")。它起始自山下東行線車道的[金鐘道交匯處](../Page/金鐘道.md "wikilink")，及金鐘道西行線車道，而山上尾段分叉接駁到[堅尼地道西段](../Page/堅尼地道.md "wikilink")，及[花園道的南段](../Page/花園道.md "wikilink")（近[香港動植物公園東面出口](../Page/香港動植物公園.md "wikilink")）。

紅棉路舊稱**木棉徑**（英文：**Kapok
Drive**），其名字是因為該處的[木棉樹而得名](../Page/木棉.md "wikilink")。

## 沿路著名景點

紅棉路因為途經的[紅棉路婚姻註冊處而著名](../Page/紅棉路婚姻註冊處.md "wikilink")，在香港幾近成為[結婚勝地的代名詞](../Page/結婚.md "wikilink")。這處的政府婚姻註冊處是粉紅色的，它在[香港公園的西面](../Page/香港公園.md "wikilink")，有露天停泊花車區。

其他景點：

  - [香港公園](../Page/香港公園.md "wikilink")
  - [花旗銀行大廈](../Page/花旗銀行大廈.md "wikilink")
  - [美利大廈](../Page/美利大廈.md "wikilink")
  - [美國銀行中心](../Page/美國銀行中心.md "wikilink")
  - [中銀大廈](../Page/中銀大廈.md "wikilink")
  - [聖若瑟書院](../Page/聖若瑟書院.md "wikilink")
  - [力寶中心](../Page/力寶中心.md "wikilink")
  - [山頂纜車](../Page/山頂纜車.md "wikilink")[花園道總站](../Page/花園道站.md "wikilink")
  - [遠東金融中心](../Page/遠東金融中心.md "wikilink")

<File:HK> Park Cotton Tree Drive Marriage Registry
Olympia.JPG|紅棉路婚姻註冊處在[香港公園內](../Page/香港公園.md "wikilink")
Cotton Tree Drive Flyover towards Mid-levels.jpg|紅棉路往中半山的天橋

## 霆鋒彎

霆鋒彎，指紅棉路[美利大廈對開上斜路段](../Page/美利大廈.md "wikilink")，名字沿自2002年3月藝人[謝霆鋒曾於該彎位駕駛](../Page/謝霆鋒.md "wikilink")[法拉利F](../Page/法拉利.md "wikilink")360
Modena跑車時發生車禍，並引發哄動一時的[頂包案](../Page/謝霆鋒#頂包案.md "wikilink")。

過去就有多次交通意外發生於霆鋒彎，大部份都是涉及高級、高性能跑車、轎車、電單車。另一方面，由於該處轉彎後為一段斜而闊之上斜路，不少駕駛愛好者都愛趁深夜少車時以高速入彎之後加速上斜，以表現其車之性能\[1\]\[2\]

## 公共交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px;">

</div>

</div>

## 參考資料

<div class="references-small">

<references />

</div>

[Category:金鐘街道](../Category/金鐘街道.md "wikilink")
[Category:半山區街道](../Category/半山區街道.md "wikilink")

1.  [壹車搜雙甩](http://motor.atnext.com/moDspContent.cfm?article_ID='%23\\G5Q_K!+0++%0A&CAT_ID=23)，壹蘋果車網
2.  [法拉利掃欄
    醉司機被捕](http://paper.wenweipo.com/2006/06/27/HK0606270068.htm)，
    2006-06-27，文匯報