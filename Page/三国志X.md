是[日本光榮公司於](../Page/日本光榮公司.md "wikilink")2004年7月推出的一款[三國志系列的](../Page/三國志系列.md "wikilink")[策略](../Page/策略.md "wikilink")[電子遊戲](../Page/電子遊戲.md "wikilink")。三國志X再度融合了[角色養成的成分](../Page/养成游戏.md "wikilink")，賦予高自由度扮演一名[文官或](../Page/文官.md "wikilink")[武將的可能性](../Page/武將.md "wikilink")，與[三國志VII](../Page/三國志VII.md "wikilink")、[三國志VIII相似](../Page/三國志VIII.md "wikilink")，相對於[三國志IX有著相當大的不同](../Page/三國志IX.md "wikilink")。

## 內容

遊戲加入了大量[歷史事件](../Page/歷史.md "wikilink")，[戰爭模式也大幅改變了](../Page/戰爭.md "wikilink")。[內政方面](../Page/內政.md "wikilink")，[國力的增強不僅是](../Page/國力.md "wikilink")[農業](../Page/農業.md "wikilink")、[商業](../Page/商業.md "wikilink")、[城市防禦度](../Page/城市.md "wikilink")、[治安的提升](../Page/治安.md "wikilink")，還導入了[技術值與城市規模](../Page/技術.md "wikilink")。技術愈高，城市規模愈大，[建築物也能因此增建](../Page/建築物.md "wikilink")，對於[兵種與](../Page/兵種.md "wikilink")[攻城武器之開發有一定程度的影響](../Page/攻城武器.md "wikilink")，也能抵擋某些[天災](../Page/天災.md "wikilink")。

[軍事方面](../Page/軍事.md "wikilink")，強調兵種相剋的原理，以[騎兵剋](../Page/騎兵.md "wikilink")[步兵](../Page/步兵.md "wikilink")，步兵剋[弓兵](../Page/弓兵.md "wikilink")，弓兵剋騎兵的[原則](../Page/原則.md "wikilink")，再衍生其他特殊的兵種如[衝車](../Page/衝車.md "wikilink")、[木獸和](../Page/木獸.md "wikilink")[鬥艦等等](../Page/鬥艦.md "wikilink")，且建立兵種憑藉[經驗值升級的制度](../Page/經驗.md "wikilink")。例如，步兵為基本[單位](../Page/單位.md "wikilink")，隨著經驗值上升可以強化為[重步兵與近衛兵](../Page/重步兵.md "wikilink")，而[青州兵則需要經歷特別事件始可徵用](../Page/青州兵.md "wikilink")。

武將方面，三國志X再度偏重角色養成，透過經驗值或[人際關係](../Page/人際關係.md "wikilink")、鍛鍊等方式，可以提升個人的[特技與](../Page/特技.md "wikilink")[能力](../Page/能力.md "wikilink")。同時，遊戲中某些[城池具有相當獨特的特性](../Page/城池.md "wikilink")，透過該城池的[發展可以完全展現其獨特性](../Page/發展.md "wikilink")。例如[洛陽的](../Page/洛陽.md "wikilink")[太學](../Page/太學.md "wikilink")
(增加鍛鍊效果)、[鄴的觀星所](../Page/鄴.md "wikilink")
(可進行[預知與](../Page/預知.md "wikilink")[占卜](../Page/占卜.md "wikilink"))、[長安的大](../Page/長安.md "wikilink")[商家](../Page/商家.md "wikilink")
(可[買賣](../Page/買賣.md "wikilink")[寶物](../Page/寶物.md "wikilink"))、[襄陽的](../Page/襄陽.md "wikilink")[智力](../Page/智力.md "wikilink")[學問所及](../Page/學問.md "wikilink")[永昌的](../Page/永昌.md "wikilink")[象兵](../Page/象兵.md "wikilink")[編制所等等](../Page/編制.md "wikilink")。

[戰略方面](../Page/戰略.md "wikilink")，可以透過建設[陣](../Page/陣.md "wikilink")、[砦](../Page/砦.md "wikilink")、[城塞](../Page/城塞.md "wikilink")、石兵陣等設施來阻止敵人的進犯，也能以支配關隘的方式來抵擋敵方軍勢。整體來看，戰略傾向大[地圖即時戰略](../Page/地圖.md "wikilink")，戰爭時又比較偏向類回合制，軍隊移動順序以士氣高低決定。

三國志X還納入了[舌戰系統](../Page/舌戰.md "wikilink")，其意義近似於武將之[單挑系統](../Page/單挑.md "wikilink")，能力數值並非決定勝負的唯一要素。[舌戰由](../Page/舌戰.md "wikilink")2名「文臣」透過簡易精闢的臨場出招、指令施展和策略連線，在[拱橋上進行角力](../Page/拱橋.md "wikilink")，意圖展現[舌戰不輸於](../Page/舌戰.md "wikilink")[單挑的激烈臨場感](../Page/單挑.md "wikilink")。

## 遊戲劇本

### 本體

  - 1 184年2月 黃巾興於亂世 英雄胸懷青雲（[黃巾之亂](../Page/黃巾之亂.md "wikilink")）
  - 2 189年9月 虎狼君臨朝堂 帝都暗雲飄蕩（[反董卓聯盟](../Page/董卓討伐戰.md "wikilink")）
  - 3 194年3月 飛將馳騁天下 武威轟動中原（[下邳之戰](../Page/下邳之戰.md "wikilink")）
  - 4 200年1月 華北兩強相爭 官渡一決雌雄（[官渡之戰](../Page/官渡之戰.md "wikilink")）
  - 5 207年5月 臥龍飛出茅廬 周郎火燒赤壁（[赤壁之戰](../Page/赤壁之戰.md "wikilink")）
  - 6 217年7月 皇叔大器晚成 漢中戰慄電馳（[漢中攻防戰](../Page/漢中之戰.md "wikilink")）
  - 7 227年4月 丞相上表出師 宿敵五度對決（[諸葛亮北伐](../Page/諸葛亮北伐.md "wikilink")）

### 威力加強版

  - PK1 211年1月 義臣壯志未酬 鐵騎強渡渭水（[潼關之戰](../Page/潼關之戰.md "wikilink")）
  - PK2 221年6月 書生嶄露頭角 蜀主墜落夷陵（[夷陵之戰](../Page/夷陵之戰.md "wikilink")）
  - PK3 253年2月 繼承武侯遺志 漢將孤軍奮戰（[姜維北伐](../Page/姜維北伐.md "wikilink")）
  - PK4 250年1月 英雄群聚一堂 同爭天下霸業（假想劇本，本劇本無法參加網路排行榜）

## 推薦武將

本遊戲共有12位的官方推薦武將，選擇時有專屬故事介紹。

| 武將名稱                             | 起始身分    | 起始勢力                           | 起始所在 | 使用劇本                   |
| -------------------------------- | ------- | ------------------------------ | ---- | ---------------------- |
| [趙雲](../Page/趙雲.md "wikilink")   | 一般      | 劉備                             | 新野   | 5 207年5月 臥龍飛出茅廬 周郎火燒赤壁 |
| [關羽](../Page/關羽.md "wikilink")   | 太守      | 劉備                             | 下邳   | 4 200年1月 華北兩強相爭 官渡一決雌雄 |
| [張遼](../Page/張遼.md "wikilink")   | 一般（流浪軍） | 呂布                             | 河內   | 3 194年3月 飛將馳騁天下 武威轟動中原 |
| [諸葛亮](../Page/諸葛亮.md "wikilink") | 在野      | \----                          | 襄陽   | 5 207年5月 臥龍飛出茅廬 周郎火燒赤壁 |
| [周瑜](../Page/周瑜.md "wikilink")   | 都督      | [孫權](../Page/孫權.md "wikilink") | 柴桑   | 5 207年5月 臥龍飛出茅廬 周郎火燒赤壁 |
| [陸遜](../Page/陸遜.md "wikilink")   | 都督      | 孫權                             | 江陵   | 7 227年4月 丞相上表出師 宿敵五度對決 |
| [劉備](../Page/劉備.md "wikilink")   | 君主      | 劉備                             | 新野   | 5 207年5月 臥龍飛出茅廬 周郎火燒赤壁 |
| [曹操](../Page/曹操.md "wikilink")   | 君主      | 曹操                             | 許昌   | 4 200年1月 華北兩強相爭 官渡一決雌雄 |
| [姜維](../Page/姜維.md "wikilink")   | 一般      | [曹叡](../Page/曹叡.md "wikilink") | 天水   | 7 227年4月 丞相上表出師 宿敵五度對決 |
| [呂布](../Page/呂布.md "wikilink")   | 君主（流浪軍） | 呂布                             | 河內   | 3 194年3月 飛將馳騁天下 武威轟動中原 |
| [張飛](../Page/張飛.md "wikilink")   | 太守      | 劉備                             | 梓潼   | 6 217年7月 皇叔大器晚成 漢中戰慄電馳 |
| [馬超](../Page/馬超.md "wikilink")   | 一般      | 劉備                             | 梓潼   | 6 217年7月 皇叔大器晚成 漢中戰慄電馳 |

## 外部連結

  - [三國志X繁體中文官方網站](http://www.gamecity.com.tw/sangokushi/10/)
  - [三國志X威力加強版繁體中文官方網站](http://www.gamecity.com.tw/sangokushi/10puk/)

[10](../Category/三國志系列.md "wikilink")
[R](../Category/Windows遊戲.md "wikilink")