**罗姆语**（羅姆語：，又称**吉卜赛语**或**茨冈语**）是[罗姆人和信德](../Page/罗姆人.md "wikilink")（Sinti）社群的语言。罗姆语属于[印度-雅利安语支语言](../Page/印度-雅利安语支.md "wikilink")。

分析罗姆语得知，它与[印度北部的语言相近](../Page/印度.md "wikilink")，尤其是[旁遮普语](../Page/旁遮普语.md "wikilink")。在语言学上的关系，相信可引证[罗姆人的地理源头](../Page/罗姆人.md "wikilink")。罗姆语的外来词，可追查到罗姆人向西迁移的模式。罗姆人来自[印度次大陆](../Page/印度次大陆.md "wikilink")（或现在印度北部或[巴基斯坦地区](../Page/巴基斯坦.md "wikilink")）。

罗姆语、旁遮普语和Pothohari语有一些相同词和语法系统。

[罗姆人没有固定的居住点](../Page/罗姆人.md "wikilink")，亦没有国家以罗姆语作为官方语言。现在，罗姆语分成为不同的方言。

## 使用者分布

  - [罗马尼亚](../Page/罗马尼亚.md "wikilink") - 273,500 (1.2%)
  - [斯洛伐克](../Page/斯洛伐克.md "wikilink") - 253,943 (4.8%)
  - [捷克共和国](../Page/捷克共和国.md "wikilink") - 200,000 (1.7%)
  - [保加利亚](../Page/保加利亚.md "wikilink") - 187,900 (2.48%)
  - [匈牙利](../Page/匈牙利.md "wikilink") - 150,000 (1.5%)
  - [阿尔巴尼亚](../Page/阿尔巴尼亚.md "wikilink") - 60,000 (1.67%)
  - [希腊](../Page/希腊.md "wikilink") - 40,000 (0.36%)
  - [瑞典](../Page/瑞典.md "wikilink") - 20,000 (0.22%)
  - [土耳其](../Page/土耳其.md "wikilink") - 10,633 (1960年普查)

## 參考文獻

  -
  -
### 引用

## 外部链接

  - [Romani language
    tree](http://www.ethnologue.com/show_family.asp?subid=92037)
  - [A part of the works of Ian Hancock](http://radoc.net)
  - [Detailed discussion of the
    language](http://web.quipo.it/minola/romani/language9.htm)
  - [Romany - English
    Dictionary](https://web.archive.org/web/20060209212729/http://www.websters-online-dictionary.org/definition/Romany-english/)
  - [Partial Romany/English Dictionary : Compiled by Angela Ba'Tal Libal
    and Will
    Strain](https://web.archive.org/web/20000106170716/http://www.geocities.com/SoHo/3698/rom.htm)
  - [Romani project @ Karl-Franzens-University in
    Graz](http://www-gewi.kfunigraz.ac.at/romani/index.en.shtml)
  - [Romani project @ Manchester
    University](http://www.llc.manchester.ac.uk/Research/Projects/romani/index.html)

[Category:罗姆人](../Category/罗姆人.md "wikilink")
[Category:印度-雅利安语支](../Category/印度-雅利安语支.md "wikilink")