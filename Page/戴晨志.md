**戴晨志**（），台灣知名作家，以心靈、勵志等書籍著作出名。

## 背景資料

[Cheng-chi_Dai_speech_note_20130907.jpg](https://zh.wikipedia.org/wiki/File:Cheng-chi_Dai_speech_note_20130907.jpg "fig:Cheng-chi_Dai_speech_note_20130907.jpg")

  - 1959年生於[花蓮縣](../Page/花蓮縣.md "wikilink")，成長於台南、嘉義、雲林、台中、台北等地。
  - [大學聯考兩次失利](../Page/大學聯考.md "wikilink")，後來唸了[國立藝專廣播電視科](../Page/國立藝專.md "wikilink")，為了訓練表達能力，要求自己每天都要寫[日記](../Page/日記.md "wikilink")、練習上台[演講](../Page/演講.md "wikilink")。
  - 1990年，就讀美國[俄勒岡大學博士班](../Page/俄勒岡大學.md "wikilink")，越洋投稿，於《[中國時報](../Page/中國時報.md "wikilink")》開闢專欄〈隔海播報‧戴晨志頻道〉。
  - 1992年，擔任世新大學口語傳播系系主任。
  - 1994年，推出首本著作《你是說話高手嗎？》，至今已有四十六本書問市，總銷售量已逾四百萬冊。

## 學歷

  - 美國[俄勒岡大學口語傳播博士](../Page/俄勒岡大學.md "wikilink")
  - 美國[威斯康辛州](../Page/威斯康辛州.md "wikilink")[馬凱大學廣播電視碩士](../Page/馬凱大學.md "wikilink")
  - [國立藝專廣播電視科畢業](../Page/國立藝專.md "wikilink")

## 經歷

  - [世新大學口語傳播系創系主任](../Page/世新大學.md "wikilink")
  - [中華電視公司](../Page/中華電視公司.md "wikilink")[新聞部記者](../Page/華視新聞.md "wikilink")、編譯
  - 中華電視公司《[華視新聞雜誌](../Page/華視新聞雜誌.md "wikilink")》[執行製作](../Page/執行製作.md "wikilink")

## 外部連結

  - [戴晨志博士的快樂網站](http://drdai.com)

  -
  - [時報出版名家檔案：戴晨志的勵志館](http://www.readingtimes.com.tw/authors/dai/)

  - [時報出版--戴晨志作者專區](http://www.readingtimes.com.tw/ReadingTimes/site/604/Default.aspx)

  - [戴晨志 說故事高手](http://blog.yam.com/DRDAI)

## 參考來源

  - [戴晨志博士的快樂網站](http://www.drdai.com/)

[Category:台灣作家](../Category/台灣作家.md "wikilink")
[Category:台湾记者](../Category/台湾记者.md "wikilink")
[Category:台灣傳播學者](../Category/台灣傳播學者.md "wikilink")
[Category:臺中市私立衛道高級中學校友](../Category/臺中市私立衛道高級中學校友.md "wikilink")
[Chen晨](../Category/戴姓.md "wikilink")
[Category:世新大學教授](../Category/世新大學教授.md "wikilink")
[Category:國立臺灣藝術專科學校校友](../Category/國立臺灣藝術專科學校校友.md "wikilink")
[Category:俄勒岡大學校友](../Category/俄勒岡大學校友.md "wikilink")
[Category:花蓮人](../Category/花蓮人.md "wikilink")