**酒酒井町**（）是[千葉縣北部中央](../Page/千葉縣.md "wikilink")，[印旛郡的一](../Page/印旛郡.md "wikilink")[町](../Page/町.md "wikilink")。

## 概要

  - 昭和40年代起，此地開發住宅團地，近年[JR](../Page/東日本旅客鐵道.md "wikilink")[成田線的快速電車停靠JR](../Page/成田線.md "wikilink")[酒酒井站](../Page/酒酒井站.md "wikilink")，[京成電鐵特急電車也停靠](../Page/京成電鐵.md "wikilink")[京成酒酒井站](../Page/京成酒酒井站.md "wikilink")、[宗吾參道站](../Page/宗吾參道站.md "wikilink")，交通便利性大幅提升。
  - 此外，京成宗吾參道站附近有[京成電鐵](../Page/京成電鐵.md "wikilink")。

## 地理

  - 1級河川：高崎川

### 鄰接自治體

  - [佐倉市](../Page/佐倉市.md "wikilink")
  - [成田市](../Page/成田市.md "wikilink")
  - [富里市](../Page/富里市.md "wikilink")
  - [八街市](../Page/八街市.md "wikilink")
  - [印西市](../Page/印西市.md "wikilink")

## 人口

## 歷史

  - 從各遺跡出土的[石器推測](../Page/石器.md "wikilink")，此地約在2萬4000年前就已有人類活動。
  - [室町時代至](../Page/室町時代.md "wikilink")[戰國時代是](../Page/戰國時代_\(日本\).md "wikilink")[千葉氏據點](../Page/千葉氏.md "wikilink")，此地的是[北總地域的](../Page/北總.md "wikilink")[政治中心](../Page/政治.md "wikilink")，也是水運的要衝。[江戶時代](../Page/江戶時代.md "wikilink")，[土井利勝築](../Page/土井利勝.md "wikilink")[佐倉城](../Page/佐倉城.md "wikilink")，本佐倉城廢城，此地轉變為[成田山詣與](../Page/成田山.md "wikilink")詣的[宿場町](../Page/宿場町.md "wikilink")。本地除了有[幕府直轄野馬會所](../Page/幕府.md "wikilink")（馬[市場](../Page/市場.md "wikilink")），受惠於便利的水上交通，也是物資的集散地。[農業與釀酒業興盛](../Page/農業.md "wikilink")。
  - [近代](../Page/近代.md "wikilink")，1897年（明治30年）起[酒酒井站等車站](../Page/酒酒井站.md "wikilink")、鐵道陸續開通。現在是[千葉](../Page/千葉市.md "wikilink")、[成田的](../Page/成田市.md "wikilink")[臥城之一](../Page/臥城.md "wikilink")。
  - 明治大合併後，本町是少數未合併的自治體之一。

### 地名由來

傳聞過去有個孝子的水井湧出酒來，因而稱為酒之井戶，後演變為地名。

### 年表

  - 2萬4000年前 -
    從[石器與](../Page/石器.md "wikilink")[貝塚推測此地已有人類居住](../Page/貝塚.md "wikilink")。
  - 1486年（文明18年） - 築本佐倉城，為其據點。
  - 1521年（[永正](../Page/永正.md "wikilink")18年） -
    開始整備[城下町](../Page/城下町.md "wikilink")（酒酒井宿）。
  - 1590年（[天正](../Page/天正.md "wikilink")18年） -
    [小田原征伐](../Page/小田原征伐.md "wikilink")，[千葉氏滅亡](../Page/千葉氏.md "wikilink")。
  - 1592年（天正20年） -
    [德川家康五男](../Page/德川家康.md "wikilink")[武田信吉入府](../Page/武田信吉.md "wikilink")。
  - 1610年（[慶長](../Page/慶長.md "wikilink")15年） -
    [土井利勝築](../Page/土井利勝.md "wikilink")[佐倉城](../Page/佐倉城.md "wikilink")。
  - 1889年（明治22年）4月1日 -
    [町村制施行](../Page/町村制.md "wikilink")，酒酒井町、下台村、馬橋村、墨村、飯積村、尾上村、中川村、伊篠村、伊篠新田、篠山新田、今倉新田、下岩橋村、柏木村、上岩橋村、本佐倉村、本佐倉町合併成立[印旛郡](../Page/印旛郡.md "wikilink")**酒酒井町**。\[1\]
  - 1897年（明治30年） - [鐵道開通](../Page/鐵道.md "wikilink")。（現JR成田線）
  - 1971年（昭和46年） - 開發住宅團地。
  - 2002年（[平成](../Page/平成.md "wikilink")14年） -
    與[佐倉市合併公投未通過](../Page/佐倉市.md "wikilink")。

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>酒酒井町町域變遷表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1868年<br />
以前</p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
</tbody>
</table>

## 行政

  - 町長：小坂泰久

## 企業

  -
  - 壽司麵處大京（すしめん処大京）本社

## 經濟

### 主要商業設施

  - 酒酒井店

  - 酒酒井店

  - 酒酒井店

  - 酒酒井店

  - 酒酒井店

  -
## 地域

### 北部

[宗吾參道站](../Page/宗吾參道站.md "wikilink")、、酒酒井兒童天國（酒々井ちびっ子天国）、伊篠白幡遺跡、上岩橋貝層等。

### 中部

主要車站[酒酒井站](../Page/酒酒井站.md "wikilink")、[京成酒酒井站所在地](../Page/京成酒酒井站.md "wikilink")，車站周邊人口密集。
東部[國道51號舊道](../Page/國道51號.md "wikilink")（）可見過去[宿場町風貌的懷舊景色](../Page/宿場町.md "wikilink")。
國道繞道旁[購物中心與大型回收商店等大型商業設施聚集](../Page/購物中心.md "wikilink")。

### 南部

、總合運動公園、藥草園、[酒酒井PA](../Page/酒酒井PA.md "wikilink")、酒酒井曲家（まがり家）等。

## 教育

**高等學校**

  -
**中學校**

  -
**小學校**

  -
  -
## 交通

### 鐵路

  - [東日本旅客鐵道](../Page/東日本旅客鐵道.md "wikilink")（JR東日本）

<!-- end list -->

  - [成田線](../Page/成田線.md "wikilink")：[酒酒井站](../Page/酒酒井站.md "wikilink")
  - [總武本線](../Page/總武本線.md "wikilink")：

<!-- end list -->

  - [京成電鐵](../Page/京成電鐵.md "wikilink")

<!-- end list -->

  - [Number_prefix_Keisei.PNG](https://zh.wikipedia.org/wiki/File:Number_prefix_Keisei.PNG "fig:Number_prefix_Keisei.PNG")
    [京成本線](../Page/京成本線.md "wikilink")：[京成酒酒井站](../Page/京成酒酒井站.md "wikilink")
    - [宗吾參道站](../Page/宗吾參道站.md "wikilink")

### 道路

  - **[高速道路](../Page/高速道路.md "wikilink")**
      - [東關東自動車道](../Page/東關東自動車道.md "wikilink")
          - [酒酒井PA](../Page/酒酒井PA.md "wikilink")
          - [酒酒井IC](../Page/酒酒井IC.md "wikilink")
  - **[一般國道](../Page/一般國道.md "wikilink")**
      - [國道51號](../Page/國道51號.md "wikilink")

      -
  - **[主要地方道](../Page/主要地方道.md "wikilink")**
      -
      -
  - **[一般縣道](../Page/都道府縣道.md "wikilink")**
      -
### 路線巴士

  -
  -
## 名勝、古蹟、觀光地、祭典、活動

  - 藥草園「藥草之丘」

  - 酒酒井兒童天國（酒々井ちびっこ天国）（）

  - 酒酒井曲家（酒々井まがり家）

      - 酒酒井新酒祭（11月）

  -
  - 螢之里

  - 酒酒井故鄉祭（11月中旬）

  - （骨加[史跡](../Page/史跡.md "wikilink")、房總魅力500選）

  -   - 墨之獅子舞（7月15日）（千葉縣指定無形民俗文化財、房總魅力500選）

  - 馬橋地區獅子舞（7月第3個星期六）（酒酒井町指定無形民俗文化財）

  - 上岩橋地區獅子舞（4月第1個星期日）（酒酒井町指定無形民俗文化財）

  - [順天堂大學裸祭](../Page/順天堂大學.md "wikilink")（6月上旬）

  - 酒酒井之森公園高爾夫球場

  -
<File:Shisui-P.A.-higashi-kanto-expressway-japan.JPG>|[酒酒井服務區](../Page/酒酒井PA.md "wikilink")
<File:Motosakura>
Castle.jpg|坐落於與[佐倉市交界處的本佐倉城遺址](../Page/佐倉市.md "wikilink")
<File:JNR> C58 179.jpg|60年代的南酒酒井站 <File:Keisei> Sogosando sta
001.jpg|宗吾參道站

## 備註

## 外部連結

  - [酒酒井町](http://www.town.shisui.chiba.jp/)

<!-- end list -->

1.  角川日本地名大辞典編纂委員会『[角川日本地名大辞典](../Page/角川日本地名大辞典.md "wikilink") 12
    千葉県』、[角川書店](../Page/角川書店.md "wikilink")、1984年 ISBN
    4040011201より