**儲備**（**Reserve**，或稱**精算儲備金**），等於某[偶發事件的預期](../Page/偶發事件.md "wikilink")[現金流](../Page/現金流.md "wikilink")[現值](../Page/現值.md "wikilink")（即[精算現值](../Page/精算現值.md "wikilink")），是其擁有者（通常是[保險公司](../Page/保險公司.md "wikilink")）的一種[債項](../Page/債項.md "wikilink")。在保險業中，儲備指[保單未來的現金流現值](../Page/保單.md "wikilink")，而保險人的總債項是其承諾賠付的所有保單之儲備的總和。保險人必須保持其[資產水平](../Page/資產.md "wikilink")（尤其是[流動資產](../Page/流動資產.md "wikilink")），以滿足偶發事件產生的賠付額。

## 損失[隨機變數](../Page/隨機變數.md "wikilink")

無論計算哪種保險產品的精算儲備金，計算者必須先定義損失隨機變數。[精算師通常以](../Page/精算師.md "wikilink")
\(K(x)\) 代表一名 \(x\)
歲的[受保人剩餘生命期的縮減隨機變數](../Page/受保人.md "wikilink")，而某份死亡賠付為一元及每年保費
\(P\) 元的保單的損失隨機變數則可寫成

\[L = v^{K(x)+1} - P\ddot{a}_{\overline{K(x)+1}|}\]

從上述算式，可知若某人於保單生效後 \(t\)
年內死亡，則保險公司於保單生效時損失的現值，必等於死亡賠付的現值減保費收入的現值。然而此算式無法求出未來任一時間的損失現值，故在時間
\(t\) 的損失隨機變數定義成

\[{}_{t}L = v^{K(x)+1-t} - P\ddot{a}_{\overline{K(x)+1-t|}}\]

## 賠付儲備

賠付儲備，又稱淨等額[保費儲備金](../Page/保費.md "wikilink")，只包含兩種現金流，主要用於根據[GAAP準則製作財務報告](../Page/GAAP.md "wikilink")。賠付儲備中保費的計算原理是假設
\(t=0\)
時，儲備金等於零，而計算方法主要分為預期式及過去式兩種。以未來保費收入的精算現值減去未來偶發賠付額的精算現值，謂之預期式；以過去偶發賠付額累積價值減去過去已付保費的累積價值，謂之過去式。兩種方式計算出的儲備值是相同的。

以終身[人壽保險單為例](../Page/人壽保險.md "wikilink")，假設此單對於某 \(x\)
歲受保人的死亡賠付為一元，年繳保費需於每年年初繳交，而死亡賠付則於受保人死亡年的年終交付[受益人](../Page/受益人.md "wikilink")，則可以下列算式計算出每年等額保費：

\[\operatorname{E}[L] = \operatorname{E}[v^{K(x)+1} - P\ddot{a}_{\overline{K(x)+1|}}]\]

\[\operatorname{E}[L] = \operatorname{E}[v^{K(x)+1}] - P\operatorname{E}[\ddot{a}_{\overline{K(x)+1|}}]\]

\[{}_0\!V_x=A_{x}-P\cdot\ddot{a}_{x}\]

最後一條算式中的 \(V\) 代表儲備值。當 \(t=0\)，即於保單生效時，根據上述定義，儲備值應為零，故可得

\[P=\frac{A_{x}}{\ddot{a}_{x}}\]

\(t\) 年後的儲備值可以下列算式計算出：

\[{}_{t}L = v^{K(x)+1-t} - P_{x}\ddot{a}_{\overline{K(x)+1-t|}}\]

\[\operatorname{E}[{}_{t}L|K(x)>t] = \operatorname{E}[v^{K(x)+1-t}|K(x)>t] - P_{x}\operatorname{E}[\ddot{a}_{\overline{K(x)+1-t|}}|K(x)>t]\]

\[{}_t\!V_x=A_{x+t}-P_x\cdot\ddot{a}_{x+t}\] Where
\({ }P_x=\frac{A_{x}}{\ddot{a}_{x}}\)

## 參看

  - [精算學](../Page/精算學.md "wikilink")
  - [生命表](../Page/生命表.md "wikilink")
  - [责任准备金](../Page/责任准备金.md "wikilink")

[Category:保險學](../Category/保險學.md "wikilink")
[Category:精算](../Category/精算.md "wikilink")