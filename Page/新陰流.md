**新陰流兵法**（）是[日本](../Page/日本.md "wikilink")[戰國時期劍術名家](../Page/戰國_\(日本\).md "wikilink")[上泉伊勢守秀綱學習了](../Page/上泉信綱.md "wikilink")[鎌倉的](../Page/鎌倉.md "wikilink")[念流](../Page/念流.md "wikilink")、[愛洲移香齋的](../Page/愛洲移香齋.md "wikilink")[陰流](../Page/陰流.md "wikilink")（又稱愛洲陰流）、以及杉本備前守紀政的[神道流的竅門後](../Page/神道流.md "wikilink")，於1560年代成立的[劍術](../Page/劍術.md "wikilink")[流派](../Page/流派.md "wikilink")。新陰流這名字，是為對應陰流。

上泉伊勢守的弟子柳生宗嚴繼承了「新陰流」後，由柳生家所傳承、發展的稱為「柳生新陰流」。

## 柳生新陰流

### 系譜

當[柳生宗嚴被上泉伊勢守交代研究](../Page/柳生宗嚴.md "wikilink")「**無刀取**」後，[柳生氏的柳生新陰流之名開始廣為流傳](../Page/柳生氏.md "wikilink")，本來被冠上「柳生」的是傳到[福岡藩稱著的弟子筋流派](../Page/福岡藩.md "wikilink")「柳生新影流」。但，[武道學上](../Page/武道學.md "wikilink")，上泉信綱跟柳生氏傳授内容之差異，是為了分開柳生的和其他以外的新陰流。「**新陰流**」和「**柳生新陰流**」的分別是傳授者是柳生氏的為「柳生新陰流」。

同時，與新陰流有交流的的原流陰流，愛洲移香齋出身地附近的，不少人會報上柳生的名號。

[柳生宗嚴以後](../Page/柳生宗嚴.md "wikilink")，分派成：五男[柳生宗矩的](../Page/柳生宗矩.md "wikilink")[江戶柳生](../Page/江戶柳生.md "wikilink")；其孫[柳生利嚴](../Page/柳生利嚴.md "wikilink")（宗嚴嫡子[柳生嚴勝次男](../Page/柳生嚴勝.md "wikilink")）的[尾張柳生](../Page/尾張柳生.md "wikilink")。特別的，是稱呼柳生宗嚴時代的為[大和柳生](../Page/大和柳生.md "wikilink")。
新陰流的道統子孫，尾張柳生的利嚴於[慶長](../Page/慶長.md "wikilink")10年6月成為第三代傳人。
江戸柳生的[柳生三嚴](../Page/柳生十兵衛.md "wikilink")（柳生十兵衛），尾張柳生的[柳生嚴包](../Page/柳生嚴包.md "wikilink")（柳生連也齋）亦是其中的天才劍士。

## 關連項目

  - [陰流](../Page/陰流.md "wikilink")
  - [柳生流](../Page/柳生流.md "wikilink")
  - [柳生氏](../Page/柳生氏.md "wikilink")
  - [柳生藩](../Page/柳生藩.md "wikilink")
  - [尾張柳生家](../Page/尾張柳生家.md "wikilink")
  - [柳生大太刀](../Page/柳生大太刀.md "wikilink")
  - [兵法三大源流](../Page/日本兵法三大源流.md "wikilink")
  - [渡邊幸庵](../Page/渡邊幸庵.md "wikilink")

## 外部連結

  -   -
  -
  -
  -
  -
\<\!---\>\<---\>

[Category:日本劍術流派](../Category/日本劍術流派.md "wikilink")