**峰峰矿区**位于[中国](../Page/中国.md "wikilink")[河北省](../Page/河北省.md "wikilink")[邯郸市](../Page/邯郸市.md "wikilink")，西依[太行山脉](../Page/太行山.md "wikilink")，东临[冀南平原](../Page/冀南平原.md "wikilink")，北有[洺水涓涓](../Page/洺水.md "wikilink")，南系[漳河东渐](../Page/漳河.md "wikilink")，城中心有[滏阳河穿过](../Page/滏阳河.md "wikilink")，是南北经济交流中转要地和区域性经济的腹心，是该市的一个距离主城区较远的行政区，经济以[采矿业为主](../Page/采矿.md "wikilink")，陶瓷业和电力业发展迅速。

## 历史

峰峰矿区始建于1950年，1952年归[河北省直属领导](../Page/河北省.md "wikilink")，1954年改为省辖[峰峰市](../Page/峰峰市.md "wikilink")，1956年与[邯郸市合并](../Page/邯郸.md "wikilink")，后改为峰峰矿区至今，是邯郸市不在主城区内的[市辖区](../Page/市辖区.md "wikilink")。

## 人口

2009年普查人口数89.75万

## 行政区划

下辖1个[街道办事处](../Page/街道办事处.md "wikilink")、9个[镇](../Page/行政建制镇.md "wikilink")：

。

## 旅游

  - [无梁阁](../Page/无梁阁.md "wikilink")
  - [北宋抗金古地道](../Page/北宋抗金古地道.md "wikilink")
  - [宋代磁州窑遗址](../Page/宋代磁州窑遗址.md "wikilink")
  - [响堂山风景区](../Page/响堂山风景区.md "wikilink")
  - [响堂山石窟](../Page/响堂山石窟.md "wikilink")
  - [元宝山风景区](../Page/元宝山风景区.md "wikilink")

## 官方网站

  - [峰峰矿区人民政府](http://www.ff.gov.cn/)

[峰峰矿区](../Page/category:峰峰矿区.md "wikilink")
[区](../Page/category:邯郸区县市.md "wikilink")
[邯郸](../Page/category:河北市辖区.md "wikilink")