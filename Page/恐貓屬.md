**恐貓**（*Dinofelis*）是[後貓族下的一](../Page/後貓族.md "wikilink")[屬](../Page/屬.md "wikilink")。牠們廣泛分佈在[歐洲](../Page/歐洲.md "wikilink")、[亞洲](../Page/亞洲.md "wikilink")、[非洲及](../Page/非洲.md "wikilink")[北美洲](../Page/北美洲.md "wikilink")，估計屬於最少500-120萬年前。在亦有發現非常似恐貓的[化石](../Page/化石.md "wikilink")，可以追溯至800萬年前的[中新世末期](../Page/中新世.md "wikilink")。

## 物種

現時已知的恐貓[物種如下](../Page/物種.md "wikilink")：

  - *D. aronoki*：於[東非發現](../Page/東非.md "wikilink")，從巴羅刀齒恐貓中分裂出來。
  - [巴羅刀齒恐貓](../Page/巴羅刀齒恐貓.md "wikilink")（*Dinofelis
    barlowi*）：於[南非發現](../Page/南非.md "wikilink")。
  - *D.
    cristata*：於[中國發現](../Page/中國.md "wikilink")，包括了[阿氏恐貓](../Page/阿氏恐貓.md "wikilink")（*D.
    abeli*）。
  - *D. darti*：於南非發現。
  - *D. diastemata*：於[歐洲發現](../Page/歐洲.md "wikilink")。
  - *D. paleoonca*：於[北美洲發現](../Page/北美洲.md "wikilink")。
  - *D. petteri*：於東非發現。
  - [皮氏恐貓](../Page/皮氏恐貓.md "wikilink")（*D. piveteaui*）：於南非發現。
  - *Dinofelis* sp. "Langebaanweg"
  - *Dinofelis* sp. "Lothagam"

## 描述

恐貓的體型介於現存的[獅子與](../Page/獅子.md "wikilink")[豹之間](../Page/豹.md "wikilink")，多數成員大小接近[美洲豹](../Page/美洲豹.md "wikilink")（肩高約
70cm，體重約 120kg），具有劍齒以及較現今貓科粗壯許多的前足。

由[古生物學家](../Page/古生物學家.md "wikilink")與針對兩個樣本所估算的[體重](../Page/體重.md "wikilink")。第一個樣本體重約
，第二個樣本體重約 \[1\]。

恐貓的[犬齒較現存的](../Page/犬齒.md "wikilink")[貓科長而扁平](../Page/貓科.md "wikilink")，但沒有[劍齒虎這麼長](../Page/劍齒虎.md "wikilink")。因此恐貓與[獵虎的犬齒常被稱為](../Page/獵虎.md "wikilink")「偽劍齒」（不過恐貓與獵虎並非近親，分別屬於不同的科別）。恐貓的[裂肉齒並沒有如獅子或現存其他貓科發達](../Page/裂肉齒.md "wikilink")\[2\]。

恐貓可能偏好棲息於森林，[動物行為學家William](../Page/動物行為學.md "wikilink")
Allen等人認為牠們身上可能具有如[豹的斑點或是如](../Page/豹.md "wikilink")[虎的條紋](../Page/虎.md "wikilink")\[3\]。

## 古生物學

恐貓壯碩的身體代表牠們可能跟[美洲豹一樣適合生存在多種棲息地中](../Page/美洲豹.md "wikilink")，包括[森林或是較開闊的](../Page/森林.md "wikilink")[濕地等](../Page/濕地.md "wikilink")。

恐貓的獵物包括[猛瑪象幼崽](../Page/猛瑪象.md "wikilink")、[乳齒象以及](../Page/乳齒象.md "wikilink")[巧人](../Page/巧人.md "wikilink")\[4\]。不過針對出土化石的碳同位素分析顯示，恐貓仍然以狩獵大型草食動物為主。當時巧人主要的天敵可能為[豹和](../Page/豹.md "wikilink")[巨頦虎](../Page/巨頦虎.md "wikilink")，同樣來自於碳同位素分析的結果\[5\]。

在[南非](../Page/南非.md "wikilink")，恐貓和[狒狒的化石常常一起發現](../Page/狒狒.md "wikilink")。恐貓與[非洲南方古猿的化石也在數個考古地點一起被發現](../Page/非洲南方古猿.md "wikilink")。另外，在恐貓的化石旁也曾發現過[傍人的顱骨化石](../Page/傍人.md "wikilink")，其上有犬齒造成的傷痕。這代表恐貓可能以當時這些[靈長類為食](../Page/靈長類.md "wikilink")，然而碳同位素的分析認為這些只是少數特例\[6\]。

[冰河期開始造成的森林衰退可能是恐貓絕種的主要原因](../Page/冰河期.md "wikilink")。

## 大眾文化

出現在[英國](../Page/英國.md "wikilink")[BBC紀錄片](../Page/BBC.md "wikilink")《[與野獸共舞](../Page/與野獸共舞.md "wikilink")》第4集中，作為[非洲南方古猿的主要天敵](../Page/非洲南方古猿.md "wikilink")。

## 參考

## 其他文獻

  - Haines, Tom & Chambers, Paul (2006): *The Complete Guide to
    Prehistoric Life*: 181. Firefly Books Ltd., Canada.
  - Werdelin, Lars & Lewis, Margaret E. (2001): A revision of the genus
    *Dinofelis* (Mammalia, Felidae). *Zool. J. Linn. Soc.* **132**(2):
    147–258. <small></small> (HTML abstract)

[\*](../Category/恐貓屬.md "wikilink")
[Category:更新世哺乳類](../Category/更新世哺乳類.md "wikilink")
[Category:上新世動物](../Category/上新世動物.md "wikilink")
[Category:更新世動物](../Category/更新世動物.md "wikilink")
[Category:歐洲動物](../Category/歐洲動物.md "wikilink")
[Category:亞洲動物](../Category/亞洲動物.md "wikilink")
[Category:非洲動物](../Category/非洲動物.md "wikilink")
[Category:北美洲動物](../Category/北美洲動物.md "wikilink")
[Category:已滅絕動物](../Category/已滅絕動物.md "wikilink")
[Category:古動物](../Category/古動物.md "wikilink")

1.

2.

3.

4.

5.

6.