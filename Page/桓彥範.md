**桓彥範**（），字**士則**，[潤州](../Page/润州_\(隋朝\).md "wikilink")[丹阳县](../Page/丹阳县.md "wikilink")（今[江苏省](../Page/江苏省.md "wikilink")[丹阳市](../Page/丹阳市.md "wikilink")）人，是[唐朝政治家](../Page/唐朝.md "wikilink")。曾与[张柬之](../Page/张柬之.md "wikilink")、[崔玄暐](../Page/崔玄暐.md "wikilink")、[敬仲曄等發動](../Page/敬仲曄.md "wikilink")[神龍革命](../Page/神龍革命.md "wikilink")，迫[武則天退位](../Page/武則天.md "wikilink")，[唐中宗因而](../Page/唐中宗.md "wikilink")[復辟](../Page/復辟.md "wikilink")。后得罪[韋皇后](../Page/韋皇后.md "wikilink")、[武三思](../Page/武三思.md "wikilink")，因而被殺。

## 生平

曾祖桓子玉，隋散骑常侍。祖父[桓法嗣曾任弘文館學士](../Page/桓法嗣.md "wikilink")。父桓思敏。因家世關係，桓彥範被調補為右翊衛。宰相[狄仁傑賞識其才](../Page/狄仁傑.md "wikilink")，推薦給[武則天](../Page/武則天.md "wikilink")，累遷官为[御史中丞](../Page/御史中丞.md "wikilink")。

武后病重，宰相[張柬之謀誅張氏兄弟](../Page/張柬之.md "wikilink")，桓彥範、[崔玄暐](../Page/崔玄暐.md "wikilink")、[敬仲曄等參與策劃](../Page/敬仲曄.md "wikilink")。後以[禁軍攻入](../Page/禁軍.md "wikilink")[玄武門](../Page/玄武门_\(大明宫\).md "wikilink")，掌控[大內](../Page/皇城.md "wikilink")，誅[張昌宗](../Page/張昌宗.md "wikilink")、[張易之](../Page/張易之.md "wikilink")，[武則天退位](../Page/武則天.md "wikilink")，[唐中宗](../Page/唐中宗.md "wikilink")[復辟](../Page/復辟.md "wikilink")，史稱[神龍革命](../Page/神龍革命.md "wikilink")，又稱[神龍政變](../Page/神龍政變.md "wikilink")。五名大臣各有封賞；[张柬之封为](../Page/张柬之.md "wikilink")[汉阳郡王](../Page/汉阳郡_\(唐朝\).md "wikilink")；[崔玄暐](../Page/崔玄暐.md "wikilink")[博陵郡王](../Page/博陵郡_\(东汉\).md "wikilink")；[敬晖](../Page/敬晖.md "wikilink")[平阳郡王](../Page/平阳郡.md "wikilink")；[袁恕己](../Page/袁恕己.md "wikilink")[南阳郡王](../Page/南阳郡.md "wikilink")；[桓彦範](../Page/桓彦範.md "wikilink")[扶阳郡王](../Page/扶阳.md "wikilink")。彦範[赐姓](../Page/赐姓.md "wikilink")[韦](../Page/韦姓.md "wikilink")，与[韦皇后合譜](../Page/韦皇后.md "wikilink")[聯宗](../Page/聯宗.md "wikilink")。

彥範看唐中宗懦弱，大權由韋皇后與男寵[武三思等人掌握](../Page/武三思.md "wikilink")，又上書唐中宗，勸其勿大權旁落。唐中宗不聽。於是武三思集團與桓彥範等结下仇恨，武三思以「五大臣誣陷韋皇后」為由，誣賴，唐中宗頒布詔令，將五大臣[流放邊疆](../Page/流放.md "wikilink")。彥範剥夺韦姓，流放[貴州](../Page/貴州_\(廣西\).md "wikilink")（在今[广西壮族自治区](../Page/广西壮族自治区.md "wikilink")[貴縣](../Page/貴縣.md "wikilink")）。據《[舊唐書](../Page/舊唐書.md "wikilink")·桓彥範傳》記載，武三思派[周利貞逮捕桓彥範](../Page/周利貞.md "wikilink")，“乃令左右執縛，曳於竹槎之上，肉盡至骨，然後[杖殺](../Page/杖殺.md "wikilink")。”

最初誅張氏兄弟時，洛州長史[薛季昶向張柬之](../Page/薛季昶.md "wikilink")、敬仲曄說：“二凶雖除，[產](../Page/呂產.md "wikilink")[祿猶在](../Page/呂祿.md "wikilink")，去草不去根，終當復生。”彥範不欲廣殺，曰：“大事已定，彼猶機上肉耳，夫何能為！所誅已多，不可復益也。”季昶歎曰：“吾不知死所矣！”後來桓彥範果然被武三思所害。

中宗死后，[太平公主與臨淄王](../Page/太平公主.md "wikilink")[李隆基發動了](../Page/李隆基.md "wikilink")[唐隆之變](../Page/唐隆之變.md "wikilink")，[唐睿宗](../Page/唐睿宗.md "wikilink")[復辟](../Page/復辟.md "wikilink")，[平反追复彦範等人官爵](../Page/平反.md "wikilink")，谥**忠烈**。

## 參考書目

  - 《旧唐书·桓彦范传》

[Y](../Page/category:桓姓.md "wikilink")
[category:譙國桓氏](../Page/category:譙國桓氏.md "wikilink")

[Category:唐朝宰相](../Category/唐朝宰相.md "wikilink")
[Category:唐朝异姓郡王](../Category/唐朝异姓郡王.md "wikilink")
[废](../Category/唐朝赐姓氏者.md "wikilink")
[Category:唐朝被杀害人物](../Category/唐朝被杀害人物.md "wikilink")
[Category:丹陽人](../Category/丹陽人.md "wikilink")
[Category:諡忠烈](../Category/諡忠烈.md "wikilink")