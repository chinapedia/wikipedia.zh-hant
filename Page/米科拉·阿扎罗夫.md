**米科拉·阿扎罗夫**（，，），[乌克兰](../Page/乌克兰.md "wikilink")[政治家](../Page/政治家.md "wikilink")。前任[乌克兰总理](../Page/乌克兰总理.md "wikilink")。2004年12月至2005年1月被[乌克兰总统](../Page/乌克兰总统.md "wikilink")[列昂尼德·库奇马任命为代总理](../Page/列昂尼德·库奇马.md "wikilink")。[维克多·亚努科维奇在](../Page/维克多·亚努科维奇.md "wikilink")2010年成为总统之后，即被议会选举成为总理。2014年1月28日，阿扎罗夫辞去乌克兰总理职务。

## 早期生活

阿扎羅夫出生於蘇聯時代的[卡盧加](../Page/卡盧加.md "wikilink")，畢業於[莫斯科國立大學](../Page/莫斯科國立大學.md "wikilink")，取得[地質學及](../Page/地質學.md "wikilink")[礦物學博士学位](../Page/礦物學.md "wikilink")。他自1984年移居[烏克蘭](../Page/烏克蘭.md "wikilink")。

## 政治生涯

在[維克多·亞努科維奇擔任總理的兩次任期下](../Page/維克多·亞努科維奇.md "wikilink")，阿扎羅夫擔任財相掌管烏國經濟，不過他似乎並不擅於使用[烏克蘭語](../Page/烏克蘭語.md "wikilink")。

在[烏克蘭總統](../Page/烏克蘭總統.md "wikilink")[庫奇馬任內](../Page/庫奇馬.md "wikilink")，他主張與[俄羅斯](../Page/俄羅斯.md "wikilink")、[白俄羅斯](../Page/白俄羅斯.md "wikilink")、[哈薩克斯坦合作](../Page/哈薩克斯坦.md "wikilink")，成立單一經濟區，但此舉被批評阻礙乌克兰融合[世貿及](../Page/世貿.md "wikilink")[歐洲經濟](../Page/歐洲.md "wikilink")。

2014年1月28日辞去乌克兰总理职务，他表示希望自己的辭職能讓烏克蘭衝突各方達成妥協方案并結束衝突。\[1\]同日总统亚努科维奇接受辞职申请，拟任[谢尔盖·阿尔布佐夫担任代总理](../Page/谢尔盖·阿尔布佐夫.md "wikilink")。\[2\]

## 参考来源

[Category:乌克兰总理](../Category/乌克兰总理.md "wikilink")
[Category:愛沙尼亞裔烏克蘭人](../Category/愛沙尼亞裔烏克蘭人.md "wikilink")
[Category:愛沙尼亞裔俄羅斯人](../Category/愛沙尼亞裔俄羅斯人.md "wikilink")
[Category:俄羅斯裔烏克蘭人](../Category/俄羅斯裔烏克蘭人.md "wikilink")
[A](../Category/莫斯科国立大学校友.md "wikilink")

1.
2.