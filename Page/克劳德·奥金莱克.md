**克劳德·约翰·埃尔·奥金莱克**（Field Marshal **Claude John Eyre
Auchinleck**）（），是英國陆军元帅，在北非戰場聞名。丘吉爾無法等待，而發出命令立刻進攻北非的[納粹德軍](../Page/納粹德軍.md "wikilink")，但奥金莱克認為不是進攻時候而拒絕進攻而被[丘吉爾免職](../Page/丘吉爾.md "wikilink")。由[蒙哥馬利將軍代替他的位置](../Page/伯納德·蒙哥馬利.md "wikilink")，最後蒙哥馬利用他部署的軍力打敗了[隆美爾](../Page/隆美爾.md "wikilink")。

## 早年生涯

奥金莱克出生于英国奥尔德肖特市。他在贫困中长大，由于艰苦工作和奖学金得以从惠灵顿学院和[桑赫斯特皇家军事学院毕业](../Page/桑赫斯特皇家军事学院.md "wikilink")。他在印度开始他的军事生涯。1904年被派往英属印度军队服役。参加过[第一次世界大战](../Page/第一次世界大战.md "wikilink")。1929年后历任印军营长、教官、旅长。1935年升少将。1936年任印度陆军副参谋长。

## 二战

### 挪威

[Auchinleck.jpg](https://zh.wikipedia.org/wiki/File:Auchinleck.jpg "fig:Auchinleck.jpg")
[第二次世界大战开始后他调回英国](../Page/第二次世界大战.md "wikilink")，1940年5月被派往[挪威北部指挥英法联军对德军作战](../Page/挪威.md "wikilink")。12月任英南部军区司令，升中将。1941年1月任驻印英军总司令。

### 北非

[Monty,_wavvel,_auk.jpg](https://zh.wikipedia.org/wiki/File:Monty,_wavvel,_auk.jpg "fig:Monty,_wavvel,_auk.jpg")
1941年7月奥金莱克任中东英军总司令，指挥英军在北非向德意军队发起进攻，攻占[利比亚昔兰尼加地区](../Page/利比亚.md "wikilink")，取得北非战场的胜利。1942年8月他因抵制英国首相[丘吉尔尽早重新进攻的指示](../Page/丘吉尔.md "wikilink")，被免职。

### 印度

1943年奥金莱克复任驻印英军总司令。1946年晋升为**[英国陆军元帅](../Page/英国陆军元帅.md "wikilink")**军衔。1947年印度独立后退休。

## 战后生活

1981年3月23日在[摩洛哥](../Page/摩洛哥.md "wikilink")[马拉喀什去世](../Page/马拉喀什.md "wikilink")。

[Category:英国陆军元帅](../Category/英国陆军元帅.md "wikilink")
[A](../Category/OBE勳銜.md "wikilink")
[Category:杰出服务勋章得主](../Category/杰出服务勋章得主.md "wikilink")
[Category:阿尔斯特－苏格兰人](../Category/阿尔斯特－苏格兰人.md "wikilink")