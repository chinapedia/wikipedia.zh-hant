**日本文化**，是指在[日本列島上形成的在關於思想](../Page/日本.md "wikilink")、行為、生活、教育、信仰及價值觀等等的一系列實體或非實體的事物或象徵。

公元四世紀至九世紀，朝鲜半島的移民（[渡來人](../Page/渡來人.md "wikilink")）為日本帶來[東亞文化](../Page/東亞文化.md "wikilink")，這包括其書寫系統—[漢字](../Page/漢字.md "wikilink")。往后日本的[遣隋使和](../Page/遣隋使.md "wikilink")[遣唐使為日本帶來了](../Page/遣唐使.md "wikilink")[漢傳佛教文化](../Page/漢傳佛教.md "wikilink")，如[花道](../Page/花道.md "wikilink")、[茶道和](../Page/茶道.md "wikilink")[香道都是伴隨著](../Page/香道.md "wikilink")[漢傳佛教傳到日本的](../Page/漢傳佛教.md "wikilink")，是日本傳統藝術的重要一環。十世紀左右，日本與東亞大陸的交流變少，開始孕育出[國風文化](../Page/國風文化.md "wikilink")。十六世紀中葉，歐陸文化傳到日本，後來因貿易保護政策和基督教禁令，使歐陸文化在日本的傳播停滯。直至十九世紀，日本在美國的外交壓力下签署[日美神奈川条约](../Page/日美神奈川条约.md "wikilink")（日美和親条約），开放了[下田及](../Page/下田.md "wikilink")[箱馆两港口通商](../Page/箱馆.md "wikilink")，歐陸文化在日本才得以重新復興，後來更成為日本文化的重要一員。

由於日本之於東亞大陸的地理位置，日本文化常常被劃入東亞或[儒家文化圈文化之中](../Page/儒家文化圈.md "wikilink")，但始终存在爭議。[美国保守派政治学家](../Page/美国.md "wikilink")[塞缪尔·P·亨廷顿在](../Page/塞缪尔·P·亨廷顿.md "wikilink")1993年發表的[文明衝突論中](../Page/文明衝突論.md "wikilink")，認為日本文化獨立於東亞自成一體，應並列為世界九大文明之一。而日本历史学家[與那霸潤則認為日本文化是東亞大陸文化的延伸](../Page/與那霸潤.md "wikilink")，並著重於唐朝文化的色彩，只在些許方面有差異。

## 历史

日本為一[島國](../Page/島國.md "wikilink")，地處[東亞大陸的東北面](../Page/東亞.md "wikilink")，與[朝鮮半島](../Page/朝鮮半島.md "wikilink")、[滿洲及](../Page/满洲地区.md "wikilink")[西伯利亞相隔着](../Page/西伯利亞.md "wikilink")[日本海](../Page/日本海.md "wikilink")，特殊的地理位置使其文化一直與東亞大陸文化保持着自身獨特性。日本一方面不斷吸收外來文化，同時有自身的特色。自公元4世紀到9世紀，就有[渡來人帶來](../Page/渡來人.md "wikilink")[東亞文化](../Page/東亞文化.md "wikilink")。往后日本的[遣隋使和](../Page/遣隋使.md "wikilink")[遣唐使為日本帶來了](../Page/遣唐使.md "wikilink")[漢傳佛教文化](../Page/漢傳佛教.md "wikilink")，如[花道](../Page/花道.md "wikilink")、[茶道和](../Page/茶道.md "wikilink")[香道都是伴隨著](../Page/香道.md "wikilink")[漢傳佛教傳到日本的](../Page/漢傳佛教.md "wikilink")，是日本傳統藝術的重要一環，並稱為日本的「雅道」\[1\]。隨後到十世紀左右，日本與東亞大陸的交流變少，開始發展具有獨自特色的[國風文化](../Page/國風文化.md "wikilink")，而[京都則成為日本的文化中心](../Page/京都.md "wikilink")\[2\]。16世紀中葉，歐陸文化傳到日本，後來因貿易保護政策和基督教禁令，使歐陸文化在日本的傳播停滯。直至十九世紀，日本在美國的外交壓力下签署[日美神奈川条约](../Page/日美神奈川条约.md "wikilink")（日美和親条約），开放了下田及箱馆两港口通商，歐陸文化在日本才得以重新復興，後來更成為日本文化的重要一員。

[明治维新時期](../Page/明治维新.md "wikilink")，實施多項現代化改革，在掌握當時[先進國家的學術及文化的同時](../Page/先進國家.md "wikilink")，全國亦在進行大規模[工業化和](../Page/工業化.md "wikilink")[都市化](../Page/都市化.md "wikilink")，國力大幅攀升。伴随着經濟好景，國內文化生態亦有所起色，有大批傑出的文學作家及藝術家的出现。大正時期經濟景氣，這段時期日本引進了美國不少流行文化，如音樂、電影等。1920年代以後，日本法西斯政體确立，對外來文化實施嚴格的限制。戰後，[同盟國在日本實行](../Page/同盟國.md "wikilink")[民主化](../Page/民主化.md "wikilink")，放寬了日本對外來文化的限制，維持了其多元性。\[3\]\[4\]\[5\]近年來日本的文化邁向國際化，[動漫和](../Page/動漫.md "wikilink")[電子遊戲在海外擁有很大的影響力](../Page/電子遊戲.md "wikilink")\[6\]\[7\]。日本目前共有21項[世界遺產](../Page/世界遺產.md "wikilink")，其中17項是文化遺產、4項是自然遺產\[8\]。日本與[英國和](../Page/英國.md "wikilink")[美國一同被譽為](../Page/美國.md "wikilink")「世界文化大國」\[9\]。

## 音樂

日本的傳統音樂被稱為[邦樂](../Page/邦樂.md "wikilink")，包括了[雅樂](../Page/日本雅樂.md "wikilink")、聲明、[神樂](../Page/神樂.md "wikilink")、[演歌等](../Page/演歌.md "wikilink")。邦樂的演奏樂器種類繁多如[三味線](../Page/三味線.md "wikilink")、[尺八](../Page/尺八.md "wikilink")、能管（橫笛）、[箏](../Page/日本箏.md "wikilink")、[笙](../Page/笙.md "wikilink")、[和太鼓等](../Page/和太鼓.md "wikilink")，[琵琶由中國傳入日本後](../Page/琵琶.md "wikilink")，也發展出不同的形式。十九世紀末期，西方音樂進入日本，給日本音樂帶來了很大的影響，昭和後期開始產生了融合西方元素與日本特色的[卡拉OK和](../Page/卡拉OK.md "wikilink")[J-POP](../Page/J-POP.md "wikilink")（日本流行音樂）\[10\]。

現代日本流行音樂基本上從[日本傳統音樂演變而成](../Page/日本傳統音樂.md "wikilink")，當中的轉變當中最早可以追溯到西方[搖滾音樂的興起](../Page/搖滾音樂.md "wikilink")。1960年代，[披頭四和](../Page/披頭四.md "wikilink")[海灘男孩等組合啟發了日本音樂界](../Page/海灘男孩.md "wikilink")，於1969年組成的就嘗試把搖滾音樂和日本傳統音樂混合\[11\]。1970年代晚期，[新浪潮開始](../Page/新浪潮.md "wikilink")，將[流行音樂和](../Page/流行音樂.md "wikilink")[搖滾樂合二為一的](../Page/搖滾樂.md "wikilink")[南方之星開啓了J](../Page/南方之星.md "wikilink")-POP的新一頁\[12\]，最終在1980年代末，J-POP取代[歌謠曲成為日本流行音樂的主流](../Page/歌謠曲.md "wikilink")\[13\]。1999年，16歲的新人歌手[宇多田光發表了出道專輯](../Page/宇多田光.md "wikilink")《[First
Love](../Page/First_Love_\(宇多田光专辑\).md "wikilink")》，銷量達到了765萬，打破了歷史紀錄，也帶動了日本[R\&B曲風](../Page/R&B.md "wikilink")\[14\]。同年新人歌手[滨崎步发表的第一张原创专辑](../Page/滨崎步.md "wikilink")《[A
Song for
××](../Page/A_Song_for_××.md "wikilink")》连获三周销量冠军，知名度迅速上升，與宇多田光稱霸了整個市場。在[倉木麻衣出道後](../Page/倉木麻衣.md "wikilink")，三人並稱為「平成三大[歌姬](../Page/歌姬_\(日語\).md "wikilink")」，在2000年代初席捲亞洲歌壇\[15\]。踏入2010年代，各個[偶像團體均在日本音樂界獲得成功](../Page/日本偶像.md "wikilink")，尤其以[嵐和](../Page/嵐.md "wikilink")[AKB48最為突出](../Page/AKB48.md "wikilink")。不同偶像團體所獲得的大量獎項均令團體之間的盛況和激烈競爭達到前所未有的境界，因而被稱為「偶像團體的[戰國時代](../Page/戰國時代_\(日本\).md "wikilink")」\[16\]\[17\]。另外隨著[電腦及](../Page/電腦.md "wikilink")[軟件技術的發展](../Page/軟件.md "wikilink")，[VOCALOID和](../Page/VOCALOID.md "wikilink")[初音未來等虛擬歌手應運而生](../Page/初音未來.md "wikilink")，並隨著[互聯網而逐漸變得流行](../Page/互聯網.md "wikilink")\[18\]，甚至出現了虛擬歌手[翻唱歌曲的演唱會](../Page/翻唱.md "wikilink")\[19\]。

截至2011年，日本是世界第一大實體音樂市場，市場年產值達31億[美元](../Page/美元.md "wikilink")，佔世界市場30%\[20\]，而包括數位販售在內的總唱片市場年產值也達到41億美元，佔世界市場之25%，僅次於[美國排名第二](../Page/美國.md "wikilink")\[21\]。

## 文學

[Ch20_asago.jpg](https://zh.wikipedia.org/wiki/File:Ch20_asago.jpg "fig:Ch20_asago.jpg")》第20帖
朝顏
[土佐光起繪](../Page/土佐光起.md "wikilink")\]\]

影響日本文學的類型[和歌](../Page/和歌.md "wikilink")、[短歌](../Page/短歌.md "wikilink")、[俳句](../Page/俳句.md "wikilink")、[川柳](../Page/川柳_\(日本文學\).md "wikilink")、[連歌](../Page/連歌.md "wikilink")、[詩句](../Page/詩句.md "wikilink")，此外[歌舞伎對文學影響甚深](../Page/歌舞伎.md "wikilink")。日本最早的典籍，如[古事記和](../Page/古事記.md "wikilink")[日本書紀等都是采用](../Page/日本書紀.md "wikilink")[漢字書寫](../Page/漢字.md "wikilink")，直到平安時代才出現了日本特有的[假名文字](../Page/假名.md "wikilink")。[紫式部的著作](../Page/紫式部.md "wikilink")《[源氏物語](../Page/源氏物語.md "wikilink")》是世界上第一部長篇小說，以散文为主体叙事，织入近八百首和歌抒情、状物，使歌与文融为一体，行文典雅，被誉为日本物语文学的高峰之作\[22\]。[和歌是日本特有](../Page/和歌.md "wikilink")、以假名寫作的一種詩歌，通常由5韻、31個假名組成，形式短小、精炼，意蕴含蓄而悠远，追求表现[禅的审美理想和美的境界](../Page/禅.md "wikilink")\[23\]，《[萬葉集](../Page/萬葉集.md "wikilink")》就是日本現存最早的詩歌總集，享有「日本[詩經](../Page/詩經.md "wikilink")」的美譽，詩集共二十卷，收錄詩歌四千五百餘首\[24\]。[俳句也是日本獨特的一種文學體裁](../Page/俳句.md "wikilink")，使用簡約的文字抒發豐富情感，並喚起更多的聯想\[25\]，有「俳聖」之稱的知名俳人[松尾芭蕉](../Page/松尾芭蕉.md "wikilink")\[26\]所寫的《[奧之細道](../Page/奧之細道.md "wikilink")》記載了他自[江戶城到](../Page/江戶城.md "wikilink")[岐阜縣的旅程所見所聞及沿途景色](../Page/岐阜縣.md "wikilink")，被視為日本文學史上的經典作品\[27\]。明治以後，[日本文學受西方影響](../Page/日本文學.md "wikilink")，出現了[夏目漱石](../Page/夏目漱石.md "wikilink")、[森鷗外](../Page/森鷗外.md "wikilink")、[芥川龍之介](../Page/芥川龍之介.md "wikilink")、[谷崎潤一郎](../Page/谷崎潤一郎.md "wikilink")、[三島由紀夫](../Page/三島由紀夫.md "wikilink")、[川端康成](../Page/川端康成.md "wikilink")、[太宰治](../Page/太宰治.md "wikilink")，以及有「雙村上」之稱的[村上春樹及](../Page/村上春樹.md "wikilink")[村上龍等知名作家](../Page/村上龍.md "wikilink")。目前[輕小說作品在日本也受到相當程度的注目](../Page/輕小說.md "wikilink")。

日本有兩位[諾貝爾文學獎得主](../Page/諾貝爾文學獎.md "wikilink")，分別是川端康成（1968年）和[大江健三郎](../Page/大江健三郎.md "wikilink")（1994年）\[28\]。

## 建築

[Itukusimajinja-sea-side01.jpg](https://zh.wikipedia.org/wiki/File:Itukusimajinja-sea-side01.jpg "fig:Itukusimajinja-sea-side01.jpg")創建於公元593年左右\]\]

日本建築擁有久遠的歷史，最早在6世紀受到[中國建築的影響](../Page/中國建築.md "wikilink")，[佛教在傳入日本時同時帶入了中國](../Page/佛教.md "wikilink")[隋](../Page/隋.md "wikilink")[唐的建築技術與風格](../Page/唐.md "wikilink")，大量興建[佛寺和](../Page/佛寺.md "wikilink")[宮殿](../Page/宮殿.md "wikilink")，隨後慢慢發展出屬於日本的獨特風格。自16世紀起，府邸和城樓取代佛寺成為主要建築活動，[城堡在江戶時代已演變為地方的政治與經濟中心](../Page/城堡.md "wikilink")，各大城市的富有人家也開始興建不同規模與風格的府邸\[29\]。

日本在[明治時代開始引入西方建築技巧](../Page/明治時代.md "wikilink")、材料和風格，建造與傳統風格有極大差別的[鋼鐵和](../Page/鋼鐵.md "wikilink")[水泥建築](../Page/水泥.md "wikilink")\[30\]。二战前最早的日本建筑受到西方影响，材料、功能、结构和比例之间的关系得到最大限度的发展，[二戰後](../Page/第二次世界大戰.md "wikilink")，重建的城市與舊有的建築樣貌完全不同，逐而發展出現代的日本城市建築風格。1950至1960年代奠定了日本建筑发展的基调，日本建築師開始在早期基礎上寻找日本建筑文化与西方建筑理念之间重叠的部分，互相替換\[31\]，得出的成品既和西方建筑融为一体，但也有日本自己的特色\[32\]。同一時期，日本經濟快速成長，許多城市建築（例如[東京鐵塔](../Page/東京鐵塔.md "wikilink")）都是在20世紀中後期興建的，當時是日本建築[粗野](../Page/粗野主义.md "wikilink")[现代主义建築的最興盛時期](../Page/现代主义建筑.md "wikilink")，代表建築包括在1961年落成的[東京文化會館](../Page/東京文化會館.md "wikilink")\[33\]。

日本建筑对自然环境的关注、日本传统文化的坚持以及东西方建築風格之间的平衡是其較為重要的特點\[34\]。1991年，具[後現代風格的](../Page/后现代主义.md "wikilink")[東京都廳舍完工](../Page/東京都廳舍.md "wikilink")，掀起了日本的[摩天大樓風潮](../Page/摩天大樓.md "wikilink")，其後[東京國際論壇大樓](../Page/東京國際論壇大樓.md "wikilink")、[六本木新城](../Page/六本木新城.md "wikilink")、[東京晴空塔等地標性建築逐一落成](../Page/東京晴空塔.md "wikilink")，標誌著日本建築已走出一条与西方建筑体系不同、充滿本土色彩的风格，正式踏入一個新時代\[35\]\[36\]。

## 藝術

[Kabuki_performance-J._M._W._Silver.jpg](https://zh.wikipedia.org/wiki/File:Kabuki_performance-J._M._W._Silver.jpg "fig:Kabuki_performance-J._M._W._Silver.jpg")演出\]\]

日本傳統的表演藝術有[歌舞伎](../Page/歌舞伎.md "wikilink")、[能劇](../Page/能劇.md "wikilink")、[狂言](../Page/狂言.md "wikilink")、[文樂](../Page/文樂.md "wikilink")、[漫才](../Page/漫才.md "wikilink")、[落語等](../Page/落語.md "wikilink")，當中歌舞伎起源於[战国时代末期](../Page/战国时代_\(日本\).md "wikilink")，擁有悠久的歷史，是[聯合國](../Page/聯合國.md "wikilink")[非物質文化遺產之一](../Page/非物質文化遺產.md "wikilink")，內容多關於歷史事件和戀愛關係的道德衝突\[37\]，其特色在於演員只有喬扮女裝的男性，在演出時扮演小姐、姑娘等年輕女性，連聲音、姿態、感情也必須女性化，是日本最受歡迎的舞台藝術之一\[38\]，也比[能劇更大眾化](../Page/能劇.md "wikilink")\[39\]。

同樣為世界非物質文化遺產的文樂是一種[木偶戲](../Page/木偶戲.md "wikilink")，又名「人形淨琉璃」，在1684年發祥於[大阪](../Page/大阪.md "wikilink")[道頓堀](../Page/道頓堀.md "wikilink")，並曾在18世紀掀起熱潮，受歡迎程度一度壓過歌舞伎\[40\]。文樂的表演內容是以描寫嚴肅的內心故事為中心而發展出的長篇戲劇，有些作品需花費一整天上演。文樂的特別之處在於世界上大部份木偶劇都會隱藏操縱木偶的人，但在文樂中操縱木偶的人可以光明正大地出現在觀眾面前\[41\]。

[The_Great_Wave_off_Kanagawa.jpg](https://zh.wikipedia.org/wiki/File:The_Great_Wave_off_Kanagawa.jpg "fig:The_Great_Wave_off_Kanagawa.jpg")的[浮世繪作品](../Page/浮世繪.md "wikilink")「神奈川沖浪裏」\]\]

日本繪畫有[單色畫和雙色畫之分](../Page/單色畫.md "wikilink")，而[浮世繪是日本畫中最廣為人知的一種](../Page/浮世繪.md "wikilink")。[浮世繪是](../Page/浮世繪.md "wikilink")[江戶時代以](../Page/江戶時代.md "wikilink")[江戶為中心發展的版畫藝術](../Page/江戶.md "wikilink")，初期的浮世繪多為多色印刷木版畫，直到十八世紀中葉以後才發展到精緻的多色套印\[42\]。江戶末期，日本人對西方世界的興趣上升，描寫外國人日常生活的浮世繪數量急升。當時交通和通訊均不發達，浮世繪還發揮了向各個地方傳遞資訊的作用：[歌舞伎演員去世](../Page/歌舞伎.md "wikilink")、自然災害、犯罪等社會新聞都成為浮世繪的創作題材，描繪大名鼎鼎的[武士](../Page/武士.md "wikilink")、[怪談裏的](../Page/怪談.md "wikilink")[幽靈和](../Page/幽靈.md "wikilink")[妖怪的浮世繪也十分受歡迎](../Page/妖怪.md "wikilink")。\[43\]浮世繪對西方的[印象派藝術產生了不少影響](../Page/印象派.md "wikilink")，江戶時期的畫作是目前各國收藏者的熱門\[44\]。

日本[漆器工藝在世界享負盛名](../Page/漆器.md "wikilink")，「japan」一字除瞭解作「日本」，亦為漆器之意\[45\]。日本漆器的特色是以[金](../Page/金.md "wikilink")[銀作為裝飾花紋](../Page/銀.md "wikilink")，即所謂的「[蒔繪](../Page/蒔繪.md "wikilink")」，以金、銀屑嵌貼於漆液中，乾後推光處理，表現出極盡華貴的金銀色澤，有時並以[螺鈿](../Page/螺鈿.md "wikilink")、銀絲、沈金嵌出花紋花鳥草蟲或吉祥圖案，具有極高的藝術價值\[46\]。日本的现代漆器内容丰富多彩，表现手法创新，融入了不少[抽象的艺术技法](../Page/抽象.md "wikilink")，也能夠反映日本人精致细腻的民族特点。漆器在日本由建筑装饰到家具和[餐具都經常使用得到](../Page/餐具.md "wikilink")，其工藝技術也廣泛用于[陶瓷](../Page/陶瓷.md "wikilink")、[液晶电视面板和电源开关等不同的領域上](../Page/液晶电视.md "wikilink")，在日本人的生活中佔有很重要的地位。\[47\]日本較有名的漆器包括[越前漆器](../Page/越前.md "wikilink")、[木曾漆器](../Page/木曾郡.md "wikilink")、[輪島涂和](../Page/輪島市.md "wikilink")[镰仓彫等](../Page/镰仓.md "wikilink")\[48\]。

[花道](../Page/花道.md "wikilink")、[茶道和](../Page/茶道.md "wikilink")[香道都是伴隨著](../Page/香道.md "wikilink")[漢傳佛教進入日本的](../Page/漢傳佛教.md "wikilink")，現在已在日本扎根，成為了日本藝術的重要組成部分，並稱為日本的「雅道」\[49\]。

## 庭園

[Kenrokuen01-r.jpg](https://zh.wikipedia.org/wiki/File:Kenrokuen01-r.jpg "fig:Kenrokuen01-r.jpg")為日本三名園之一\]\]

日本傳統的庭園又稱為和風庭園，可見於日本寺院、大名屋敷的庭園跡、政治家・實業家邸宅跡、公共施設的敷地造型。[兼六園](../Page/兼六園.md "wikilink")、[後樂園與](../Page/後樂園.md "wikilink")[偕樂園被稱為](../Page/偕樂園.md "wikilink")[日本三名園](../Page/日本三名園.md "wikilink")。

日本庭園是依循日本神道、[佛教或](../Page/佛教.md "wikilink")[禪宗中自然與人的關係之哲學所創造的庭園](../Page/禪宗.md "wikilink")。普遍具有簡單、不對稱設計的特色，以修剪過的常綠樹為主，通常包括橋樑、岩石、踏腳石、有耙紋的砂礫地以及石燈籠，以營造一個用來冥想和沉思的環境。

## 宗教

[神道與](../Page/神道.md "wikilink")[佛教是日本的主要宗教](../Page/日本佛教.md "wikilink")，目前最多日本人自認信奉神道教，佛教排第二\[50\]。現在大多數[日本人心理上](../Page/日本人.md "wikilink")，并沒有虔誠的宗教信仰，文化上，多數日本人同時崇奉神道與[佛教](../Page/佛教.md "wikilink")，這兩個宗教的儀式已與日本人生活融為一體，如[婚禮和](../Page/婚禮.md "wikilink")[葬禮](../Page/葬禮.md "wikilink")。多數日本人同時信仰神道與[佛教](../Page/佛教.md "wikilink")。根據[美國](../Page/美國.md "wikilink")[中央情報局](../Page/中央情報局.md "wikilink")（CIA）的調查，[日本神道教約佔](../Page/日本.md "wikilink")[日本人口比率](../Page/日本人口.md "wikilink")99%，[佛教徒約佔人口的](../Page/佛教徒.md "wikilink")80%\[51\]\[52\]《[日本書紀](../Page/日本書紀.md "wikilink")》記述，佛教于552年傳入日本\[53\]，其後在[圣德太子的積極推广下在日本迅速传播开来](../Page/圣德太子.md "wikilink")\[54\]。神道教則是在日本本土所發展出來的宗教，[神社是祭神的場所](../Page/神社.md "wikilink")，神道教認為自然界[萬物皆為神](../Page/泛靈論.md "wikilink")\[55\]。

[天主教在](../Page/天主教.md "wikilink")1549年進入日本，至17世紀初大約有75萬名教徒。然而後來[德川幕府實行禁教政策](../Page/德川幕府.md "wikilink")，直到19世紀後期，[美軍強制日本開放貿易與外交的](../Page/美軍.md "wikilink")[黑船來航事件以後](../Page/黑船來航.md "wikilink")，傳教活動才再次興盛於日本，現在日本經過正式[受洗的](../Page/受洗.md "wikilink")[基督宗教教徒不超過總人口的](../Page/基督宗教.md "wikilink")1%\[56\]。

## 競技運動

[Seibu_Dome.JPG](https://zh.wikipedia.org/wiki/File:Seibu_Dome.JPG "fig:Seibu_Dome.JPG")

日本自古以來即重視各種武術，[相撲是日本的傳統運動](../Page/相撲.md "wikilink")，在《[古事記](../Page/古事記.md "wikilink")》和《[日本書紀](../Page/日本書紀.md "wikilink")》中已有記載，且有「國技」之稱\[57\]。現在日本每年舉行6次[大相撲競賽](../Page/大相撲.md "wikilink")；相撲運動員被稱為「[力士](../Page/力士.md "wikilink")」，其中等級最高者為「[橫綱](../Page/橫綱.md "wikilink")」。此外[柔道](../Page/柔道.md "wikilink")、[空手道](../Page/空手道.md "wikilink")、[劍道和](../Page/劍道.md "wikilink")[弓道也是日本的傳統體育項目](../Page/弓道.md "wikilink")，擁有很高的地位。[柔道](../Page/柔道.md "wikilink")、[空手道甚至影響了不少外國](../Page/空手道.md "wikilink")[武術](../Page/武術.md "wikilink")，如[韓國的](../Page/韓國.md "wikilink")[跆拳道就是來自](../Page/跆拳道.md "wikilink")[空手道](../Page/空手道.md "wikilink")\[58\]，[柔道則影響了](../Page/柔道.md "wikilink")[巴西柔術](../Page/巴西柔術.md "wikilink")\[59\]和[俄羅斯擒拿術](../Page/俄羅斯擒拿術.md "wikilink")\[60\]。

[明治維新後](../Page/明治維新.md "wikilink")，西方體育運動透過教育系統進入日本并普及\[61\]，其中以[棒球最為發達盛行](../Page/棒球.md "wikilink")，始于1936年的[日本職業棒球聯賽](../Page/日本棒球.md "wikilink")\[62\]孕育了[王貞治](../Page/王貞治.md "wikilink")、[長島茂雄](../Page/長島茂雄.md "wikilink")、[鈴木一朗等棒球巨星](../Page/鈴木一朗.md "wikilink")，在日本享有“國球”地位\[63\]。除了職業棒球之外，由高中生參加的[選拔高等學校野球大會](../Page/選拔高等學校野球大會.md "wikilink")（[春季甲子園大賽](../Page/春季甲子園.md "wikilink")）與[全國高等學校野球選手權大會](../Page/全國高等學校野球選手權大會.md "wikilink")（[夏季甲子園大賽](../Page/夏季甲子園.md "wikilink")）也頗負盛名，全國超過4000所高中當中只有獲得各地區冠軍的49所學校能夠突圍而出，在兩周的比賽中決出冠軍，因此在甲子園登場代表了高中生的“終極榮耀”，[甲子園亦被視為日本體育乃至日本精神的一種象徵](../Page/甲子園.md "wikilink")\[64\]。

日本的[職業足球聯賽成立于](../Page/日本職業足球聯賽.md "wikilink")1992年，平均每年有30萬人次入場觀賽\[65\]，而日本也在2002年與南韓共同舉辦了[韓日世界盃](../Page/2002年世界盃足球賽.md "wikilink")。[日本國家足球隊是亞洲實力最強的足球隊之一](../Page/日本國家足球隊.md "wikilink")\[66\]，總計贏得了4次[亞洲盃](../Page/亞洲盃足球賽.md "wikilink")，並連續5屆殺入[世界盃決賽周](../Page/世界盃.md "wikilink")，水平與歐洲次級國家看齊\[67\]；[日本國家女子足球隊也在](../Page/日本國家女子足球隊.md "wikilink")2011年[女足世界杯决赛取得冠军](../Page/女足世界杯.md "wikilink")，成为首支赢得女足世界冠军的亚洲球队\[68\]。

時至今日，西方的[高爾夫球](../Page/高爾夫球.md "wikilink")\[69\]、[網球](../Page/網球.md "wikilink")、[滑雪等運動在日本也頗為流行](../Page/滑雪.md "wikilink")。

| 日本在大型国际综合运动会的成绩                                    |
| -------------------------------------------------- |
| **賽事**                                             |
| [夏季奥运会](../Page/夏季奥林匹克运动会.md "wikilink")           |
| [冬季奥运会](../Page/冬季奥林匹克运动会.md "wikilink")           |
| [亚洲运动会](../Page/亚洲运动会.md "wikilink")               |
| [世界运动会](../Page/世界运动会.md "wikilink")               |
| [夏季大运会](../Page/世界大學生運動會#夏季世界大学生运动会.md "wikilink") |
| [冬季大运会](../Page/世界大學生運動會#冬季大学生运动会.md "wikilink")   |
| [室内运动会](../Page/亞洲室內運動會.md "wikilink")             |
| [夏季青奥会](../Page/青年奧林匹克運動會.md "wikilink")           |
| [冬季青奧会](../Page/青年奧林匹克運動會.md "wikilink")           |
| [亚洲冬运会](../Page/亚洲冬季运动会.md "wikilink")             |
| [亚洲武运会](../Page/亞洲室內暨武藝運動會.md "wikilink")          |
| [世界武运会](../Page/世界武搏运动会.md "wikilink")             |
| [沙滩运动会](../Page/亚洲沙滩运动会.md "wikilink")             |
| [亚洲青运会](../Page/亚洲青年运动会.md "wikilink")             |

## 政治

日本為[君主立憲國](../Page/君主立憲.md "wikilink")，[日本國憲法訂明](../Page/日本國憲法.md "wikilink")“[主權在民](../Page/主權在民.md "wikilink")”，而[天皇則為](../Page/天皇.md "wikilink")“日本國及人民團結的象徵”\[70\]。如同世界上多數君主立憲制度一样，天皇於日本只有元首名義，並無政治實權\[71\]。

日本政府的行政機關是指《國家行政組織法》以及《內閣府設置法》中規定的內閣府、省及其外局，並以[內閣總理大臣](../Page/內閣總理大臣.md "wikilink")（首相）為首的「1府12省廳」。2001年實施的「中央省廳再編」機構改革以大部門體制為重點，按照職能優化的原則將原本的1府22省廳精簡成為[內閣府](../Page/內閣府.md "wikilink")、[總務省](../Page/總務省.md "wikilink")、[法務省](../Page/法務省.md "wikilink")、[外務省](../Page/外務省.md "wikilink")、[財務省](../Page/財務省.md "wikilink")、[文部科學省](../Page/文部科學省.md "wikilink")、[厚生勞動省](../Page/厚生勞動省.md "wikilink")、[農林水產省](../Page/農林水產省.md "wikilink")、[經濟產業省](../Page/經濟產業省.md "wikilink")、[國土交通省](../Page/國土交通省.md "wikilink")、[環境省](../Page/環境省.md "wikilink")、[防衛省和](../Page/防衛省.md "wikilink")[警察廳的](../Page/警察廳.md "wikilink")「1府12省廳」體制。經過改革的日本行政機關核心機構數量屬主要發達國家中最少\[72\]。

[Goshichi_no_kiri.svg](https://zh.wikipedia.org/wiki/File:Goshichi_no_kiri.svg "fig:Goshichi_no_kiri.svg")

日本古代的法律體制深受中國的[中華法系影響](../Page/中華法系.md "wikilink")，[江戶時代制定的國家最高法律](../Page/江戶幕府.md "wikilink")《》就是建基於其基礎之上\[73\]。
然而自18世紀末期，日本大部份的法律則是在歐洲法系的基礎上編寫，例如[明治政府在](../Page/明治政府.md "wikilink")1896年通過的民事法則參考了[德國的](../Page/德意志帝國.md "wikilink")《[德国民法典](../Page/德国民法典.md "wikilink")》，此法律至今依然是日本民事法律的骨幹\[74\]。日本最高的法院是[最高裁判所](../Page/最高裁判所.md "wikilink")，下分三個不同等級的法院\[75\]。日本的主要法律被統稱為《[六法](../Page/六法全書.md "wikilink")》\[76\]。

東京大學博士[与那霸润認為日本政經體制來自中國思想](../Page/与那霸润.md "wikilink")，尤其是唐宋時期的色彩，但島嶼地形和中國的大陸型複雜人口組成加眾多鄰國環境不同，所以演化了些許不同處，其著作《中國化的日本：日中「文明衝突」千年史》(2011)書中提出中華文明與日本文明的不同只在五方面的些許對立。\[77\]在中華文明：

  - [權威與](../Page/權威.md "wikilink")[權力一致](../Page/權力.md "wikilink")（[皇帝是名義上的權威](../Page/皇帝.md "wikilink")，也掌握實權）
  - [政治與](../Page/政治.md "wikilink")[道德一體化](../Page/道德.md "wikilink")（在[儒家思想下](../Page/儒家思想.md "wikilink")，政治的正確性與道德的正確性一致）
  - 人民地位一貫上升（[科舉取士使平民百姓可以向社會上層流動](../Page/科舉取士.md "wikilink")）
  - 由市場形成流動性（自給自足的[農村社會解體](../Page/農村.md "wikilink")，出現自由的[工商業者](../Page/工商業.md "wikilink")）
  - [人際關係網絡化](../Page/人際關係.md "wikilink")（有利訊息流通）

在日本文明：

  - 權威與權力分離（在古代，權威者如[天皇與權力者如](../Page/天皇.md "wikilink")[幕府將軍各有其人](../Page/幕府將軍.md "wikilink")；至今在政治[經濟領域](../Page/經濟.md "wikilink")，露面者不掌權，掌實權者居幕後）
  - 政治與道德分離（協調利益是[政治人物的主要任務](../Page/政治人物.md "wikilink")，毋須向局外人講道德理念）
  - 人民地位一貫低下（從古至今有能者未必有權力地位；除與那霸潤以外，日本誰也不覺得「[知識分子地位低下](../Page/知識分子.md "wikilink")」這事有問題）
  - 農村模式靜止不變（區域社會牢不可破，抗拒[自由競爭帶來的](../Page/自由競爭.md "wikilink")[社會流動性](../Page/社會流動性.md "wikilink")）
  - 人際關係[共同體化](../Page/共同體.md "wikilink")（某個時點歸屬於某一個「集體」〔如[企業](../Page/企業.md "wikilink")〕的意識，優先於歸屬於[家庭或](../Page/家庭.md "wikilink")[宗族](../Page/宗族.md "wikilink")）

與那霸潤在書中認為，現代日本的原型在相當程度上從[日本戰國時代就形成了](../Page/日本戰國時代.md "wikilink")，[江戶時代是把那些特徵全部繼承](../Page/江戶時代.md "wikilink")、深化、定型的時代，[明治維新沒有改變這些特徵](../Page/明治維新.md "wikilink")，[自由民主黨與](../Page/自由民主黨_\(日本\).md "wikilink")[民主黨](../Page/民主党_\(1998年\).md "wikilink")「都是[戰國大名的後裔](../Page/戰國大名.md "wikilink")」。戰國大名的使命不是統一天下，而是保衛[領地](../Page/領地.md "wikilink")、保護把自身安危「委託給上面」的民眾：戰國大名的城堡是「公共建築」，一旦大名之間發生[戰爭](../Page/戰爭.md "wikilink")，城堡是大名子民的避難所。至今，日本政治人物為自己的[選區建設](../Page/選區.md "wikilink")[公共設施是理所當然的事](../Page/公共設施.md "wikilink")，屬於社會共識。[福島核災後](../Page/福島核災.md "wikilink")，災民乖乖到「現代城堡」（[小學](../Page/小學.md "wikilink")、[體育館之類](../Page/體育館.md "wikilink")）等待救援，井然有序，這其實是大名傳統的遺傳。

日本現行的[憲法是於](../Page/憲法.md "wikilink")1947年5月3日由當時佔領日本的[美軍草拟](../Page/美軍.md "wikilink")，經過日本國會的審議後再由[天皇颁行](../Page/天皇.md "wikilink")。日本國憲法最重要的三大原則是主權在民、基本人權的尊重以及和平主義，日本政治以這三大原則及其中最基本的、對個人尊嚴的尊重運行。日本實行[三權分立的政治體制](../Page/三權分立.md "wikilink")，[立法權歸兩院制](../Page/立法權.md "wikilink")[國會](../Page/日本國會.md "wikilink")，[司法權歸裁判所](../Page/司法權.md "wikilink")（即[法院](../Page/法院.md "wikilink")），[行政權則由](../Page/行政權.md "wikilink")[內閣](../Page/日本內閣.md "wikilink")、地方公共團體及[中央省廳分別處理](../Page/中央省廳.md "wikilink")\[78\]。

[Kokkaigijido.jpg](https://zh.wikipedia.org/wiki/File:Kokkaigijido.jpg "fig:Kokkaigijido.jpg")[千代田区的日本](../Page/千代田区.md "wikilink")[國會議事堂](../Page/國會議事堂.md "wikilink")，左侧为众议院，右侧为参议院。\]\]

憲法規定國家最高權力機構為國會。日本實施兩院制，[眾議院有](../Page/日本眾議院.md "wikilink")480席，[參議院有](../Page/日本參議院.md "wikilink")242席。選民為20歲以上的國民\[79\]。眾議院議員任期為四年，但由於眾議院可以在任期結束之前中途解散，所以實質上眾議院議員任期平均只有兩年多\[80\]。參議院議員任期為6年，每3年改選一半，不能中途解散\[81\]。

## 語言

在日本，大部分日本人和在日外國人都通[日語](../Page/日語.md "wikilink")，現行的公用語言稱為標準語（）或共通語（），以[江戶山手地區](../Page/江戶.md "wikilink")（今[東京中心一帶](../Page/東京.md "wikilink")）的中流階層方言為基礎\[82\]。在17世纪前，[京都作為日本的政治和文化中心](../Page/京都.md "wikilink")，當地的语言曾經是日本的标准语言。但是自從[德川幕府统治日本後](../Page/德川幕府.md "wikilink")，[江户](../Page/江户.md "wikilink")（即今天的[東京](../Page/東京.md "wikilink")）的语言便成为日本的标准语。\[83\]

此外，日本各地也存在不同的方言，主要可分为和\[84\]，而兩者中較具代表性的包括了[近畿方言](../Page/近畿方言.md "wikilink")（關西腔）、和混合了琉球語口音和詞彙的[沖繩日語等](../Page/沖繩日語.md "wikilink")。還有一些少數民族語言，主要有在[琉球群島地區](../Page/琉球群島.md "wikilink")[琉球人使用的](../Page/琉球人.md "wikilink")[琉球語及母語使用者不足](../Page/琉球語.md "wikilink")100人的[阿伊努人語言](../Page/阿伊努人.md "wikilink")[愛努語](../Page/愛努語.md "wikilink")\[85\]。

[英語是日本國民教育中標準的項目之一](../Page/英語.md "wikilink")，但因日語[音標一定會帶](../Page/音標.md "wikilink")[a](../Page/a.md "wikilink")、[i](../Page/i.md "wikilink")、[u](../Page/u.md "wikilink")、[e](../Page/e.md "wikilink")、[o五個](../Page/o.md "wikilink")[母音音標的特性](../Page/母音.md "wikilink")，與日語中缺乏[捲舌音等因素](../Page/捲舌音.md "wikilink")，加上日本人通常直接以[假名音譯外來詞](../Page/日語假名.md "wikilink")，導致日本人在英語[發音上面](../Page/發音.md "wikilink")（尤其是[r和](../Page/r.md "wikilink")[l音](../Page/l.md "wikilink")）普遍有一定程度上的不準確\[86\]\[87\]。

## 料理

[Meshi.JPG](https://zh.wikipedia.org/wiki/File:Meshi.JPG "fig:Meshi.JPG")

传统的日本料理主食是[米饭](../Page/米饭.md "wikilink")，然后再配上其他菜肴，例如鱼，肉，蔬菜，酱菜，以及汤。料理的名称则是用这些菜肴的数目来命名。最简单的日本餐膳为[一汁一菜](../Page/一汁一菜.md "wikilink")：「菜」指一碟酱菜（通常是醃黄萝卜）；「汁」則指一碗汤，最後加上一碗白饭。传统的日式早餐通常也是[味噌汤](../Page/味噌汤.md "wikilink")，米饭，和一碟酱菜（或[納豆](../Page/納豆.md "wikilink")）\[88\]。而最常见的料理为[一汁三菜](../Page/一汁三菜.md "wikilink")，即汤，米饭、一碟主菜和兩碟副菜，當中这三碟菜通常是一碟生鱼片，一碟烤菜，和一碟水煮菜，有的则是蒸菜，炸菜，醋菜，或是淋上酱料的菜，另外附上绿茶和[梅乾等醬菜](../Page/梅乾.md "wikilink")\[89\]。

由于日本是海岛型国家，日本人相当喜好海产品，例如鱼，贝类，章鱼，[虾](../Page/虾.md "wikilink")[蟹类和](../Page/蟹.md "wikilink")[海草等](../Page/海草.md "wikilink")。日本人在明治时代前有一段頗長的时间除武士在外會進食打獵得來的野獸外並不進食獸肉，直至1872年1月24日，[明治天皇宣布廢除肉食禁令並親自食用牛肉](../Page/明治天皇.md "wikilink")，才終結了日本人不常吃獸肉的歷史\[90\]\[91\]。现在的日本料理大多含有獸肉，如[牛肉](../Page/牛肉.md "wikilink")、[猪肉和](../Page/猪肉.md "wikilink")[鸡肉等都是日常料理中经常出现的食材](../Page/鸡肉.md "wikilink")。

源自中国的[麵條是日本料理很重要的一部份](../Page/麵條.md "wikilink")，當中[荞麦麵和](../Page/荞麦麵.md "wikilink")[烏冬麵這两种传统的麵條比較受歡迎](../Page/烏冬麵.md "wikilink")，汤底通常是用鱼类煮成的高湯加入酱油调味以及加上不同种类的蔬菜。另一種很受欢迎的麵类是在20世纪早期由中国传入的[拉麵](../Page/日本拉麵.md "wikilink")（）。拉麵使用的汤底有许多种，比如用鱼类和酱油煮成的高汤，或是猪肉和奶油煮成的高汤。

日本饮食文化的其中一項特色是生食，大部份食物如[鮪魚](../Page/鮪魚.md "wikilink")、[鲑鱼](../Page/鲑鱼.md "wikilink")、[河豚](../Page/河豚.md "wikilink")、[章鱼](../Page/章鱼.md "wikilink")、[牛肉](../Page/牛肉.md "wikilink")、[雞肉](../Page/雞肉.md "wikilink")、[雞蛋等都可以生食入菜](../Page/雞蛋.md "wikilink")\[92\]。日本料理中较為具標誌性食物包括[寿司](../Page/寿司.md "wikilink")、[刺身](../Page/刺身.md "wikilink")、[清酒](../Page/日本清酒.md "wikilink")、[便当](../Page/便当.md "wikilink")、[納豆](../Page/納豆.md "wikilink")、[天妇罗](../Page/天妇罗.md "wikilink")、[章魚燒](../Page/章魚燒.md "wikilink")、[竹輪](../Page/竹輪.md "wikilink")、[荞麦麵](../Page/荞麦面条.md "wikilink")、[壽喜燒和](../Page/壽喜燒.md "wikilink")[牛丼等](../Page/牛丼.md "wikilink")。

和食一向被認為能體現\[93\]，因此在2013年被[聯合國教科文組織以](../Page/聯合國教科文組織.md "wikilink")「和食—日本人的傳統飲食文化」的名稱列為[世界非物質文化遺產](../Page/非物質文化遺產.md "wikilink")\[94\]。

日本人進食的食具通常是小碗和改造过的[筷子](../Page/筷子.md "wikilink")（）\[95\]。

## 流行文化

日本的現今流行文化在世界各地有不同的影響，尤其是对东亚的影響，以电影、电视以及流行曲（[J-POP](../Page/J-POP.md "wikilink")）的影响力最大。另外，日本的科技、电器是世界领先水平，流行時裝也影響了不少人的衣著習慣。

### 美少女文化

日本人的美少女文化和少女崇拜情结无论是在远东还是在西方都已经是十分知名。

[Central_Asian_Girl.jpg](https://zh.wikipedia.org/wiki/File:Central_Asian_Girl.jpg "fig:Central_Asian_Girl.jpg")

日本的美少女文化一是表现在[选美比赛的风行](../Page/选美.md "wikilink")。据不完全统计，日本全国拥有1000多种规模不等、评判标准不一的选美比赛。从区域看，除了全国性的“日本国际小姐”、“日本小姐”和“日本环球小姐”三项顶级选美赛事以外，还有北方的“冰河小姐”、南方的“芙蓉小姐”等。国有国选，县有县选，市有市选，甚至两三百人的小村庄里，还有“最美村姑”评比。从年龄段看，小学里有“甜美小天使”，中学里有“最美少女”，大学里有“校花”，人到中年的家庭主妇亦有“美少妇大赛”。日本最有人气的偶像组合[AKB48](../Page/AKB48.md "wikilink")，就由全国海选出的几百名二十岁左右的青春美少女组成，被誉为“日本第一国民女子天团”。

不过，如同现今中国大陆的国家级选美比赛结果常引起网民不满一样\[96\]，日本网民亦认为现今的“日本小姐”长相实在令人不敢恭维，严重背离日本年轻一代的审美观\[97\]。

日本的美少女文化的另一表现就是日本时常出现十几岁便出道走红的未成年女星，[佐佐木希](../Page/佐佐木希.md "wikilink")、[渡边麻友](../Page/渡边麻友.md "wikilink")、[石原里美都是其中的佼佼者](../Page/石原里美.md "wikilink")；火了十数年的“[早安少女](../Page/早安少女.md "wikilink")”，每期成员都是十几岁的女孩；2013年被美国电影情报网站“TC
Candler”评为全球最美100人第12位的女星[桐谷美玲](../Page/桐谷美玲.md "wikilink")，早在高中一年级时便已夺得[千叶县美少女选美比赛冠军](../Page/千叶县.md "wikilink")。

日本美少女文化的第三点表现，同时也是最突出的表现，就是在独一无二的动漫文化方面。常接触日本ACG文化作品的人，往往会惊异其中的美少女文化，即这类作品中充斥着各种美少女的形象和元素。她们会改头换面，出现在架空、玄幻、情色、打斗、魔法等诸多平台。她们或是作为具有特殊属性或超能力的女主角，在虚拟的时空中尽情冒险，或是作为男主角身边不可或缺的重要参谋、助手奔前跑后，或是作为情节、画面中引人瞩目的“重要道具”被着意渲染。她们是女学生、女[忍者](../Page/忍者.md "wikilink")、豪门浮浪子弟、贵族淑女、魔法师，甚至黑帮首脑、国家元首、军队统帅等其他奇怪的身份。御宅族的出现、吸引女性观众欣赏同性之美的百合文化的流行、独特的[秋叶原色情产业](../Page/秋叶原.md "wikilink")，也都对这种美少女现象推波助澜。

[偶像剧这一电视剧类型首先出现于日本与日本的](../Page/偶像剧.md "wikilink")[少女漫画脱不了干系](../Page/少女漫画.md "wikilink")，韩国和台湾的不少偶像剧即改编自日本的少女漫画。此外，美国动画《[飞天小女警](../Page/飞天小女警.md "wikilink")》、大陆的许多网络[穿越小说](../Page/穿越小说.md "wikilink")、台湾恶搞漫画《[大民国](../Page/大民国.md "wikilink")》《[中华萌国传](../Page/中华萌国传.md "wikilink")》等的人物设定皆可追溯到日本美少女文化的根子，甚至连抗日题材的大陆电视剧《雅典娜女神》也明显的照搬照抄日本[某经典动漫中的美少女角色设定](../Page/圣斗士.md "wikilink")。

## 次文化

[次文化即由小众发起的文化](../Page/次文化.md "wikilink")，當中[日本](../Page/日本.md "wikilink")[ACG](../Page/ACG.md "wikilink")（動漫遊戲）对世界各地最具影響力。最近几十年，各種語言亦有不少用語受到了日語的影響，包括了「tsunami」（即[海嘯](../Page/海嘯.md "wikilink")，世界大多數語言以tsunami標示，日語原文為）、卡哇伊（，「可愛」之意）、[暴走族](../Page/暴走族.md "wikilink")、[御宅族](../Page/御宅族.md "wikilink")（，但較常使用片假名「」，簡寫「」表記
）等等，有些已经在社会广泛使用。

### 漫画

[MangaStoreJapan.jpg](https://zh.wikipedia.org/wiki/File:MangaStoreJapan.jpg "fig:MangaStoreJapan.jpg")

高度發展的日本漫畫已經成為世界漫畫當中具有獨特風格以及龐大影響力的流派。

日本的漫畫讀者包括了所有的年齡層\[98\]，因此日本漫畫的題材非常廣泛。1950年代以後，漫畫逐漸成為日本出版業的主要部分\[99\]。2006年漫畫市值更達到4810亿[日圓](../Page/日圓.md "wikilink")\[100\]。除了一小部分漫畫是彩色的\[101\]
之外，大多數日本漫畫採用黑白印刷\[102\]。

在日本，漫畫一般在[漫畫雜誌上連載](../Page/漫畫雜誌.md "wikilink")，毎期漫畫雜誌都包含多個漫畫系列，毎個系列只刊登一個章節，留待下期繼續\[103\]\[104\]。如果某個漫畫系列已連載一段時期並且受到讀者歡迎，那個系列的漫畫章節通常會集結成[單行本出版](../Page/單行本.md "wikilink")\[105\]\[106\]。在單行本中可收集多個（與漫畫雜誌不同，一本單行本仅限於一個漫畫系列）。
[Mahuri.svg](https://zh.wikipedia.org/wiki/File:Mahuri.svg "fig:Mahuri.svg")
日本擁有大量的漫畫讀者。為了滿足讀者的需求，日本設有專門的漫畫[咖啡廳](../Page/咖啡廳.md "wikilink")（），在那里讀者可以一邊喝[咖啡一邊看漫畫](../Page/咖啡.md "wikilink")，許多人還在漫畫咖啡廳過夜\[107\]。

日本漫畫向海外輸出始於1970年代末，並隨之向全球擴張，日本漫畫也在世界各地越來越普及，不少被各國出版商翻譯和發行外文版\[108\]\[109\]。

大多数日式漫画有一些共同的特征，例如漫畫的人物身體比較接近真人，[頭和](../Page/頭.md "wikilink")[眼睛比較大](../Page/眼睛.md "wikilink")\[110\]，[鼻子和](../Page/鼻子.md "wikilink")[嘴巴比真人小](../Page/嘴巴.md "wikilink")。日式漫畫向來不刻意描繪人種特徵，有時連[性別也難以分辨](../Page/性別.md "wikilink")\[111\]。另外，日本動漫裡的人物性格通常較誇張，但非常注重細節\[112\]。

### 動畫

日本动画最早开始于1917年\[113\]\[114\]，早期的動畫製作人包括了[下传凹天](../Page/下传凹天.md "wikilink")、
和[北山清太郎](../Page/北山清太郎.md "wikilink")，被合称为“日本动画之父”。\[115\]此後直至1980年代，日本的動畫才開始在日本被接納為主流，製作量隨之迅速增長，[GUNDAM系列作品的製作亦於這個年代開始](../Page/GUNDAM系列作品.md "wikilink")\[116\]。日本動畫於1990年代及2000年代迎來了它被海外市場接受程度的提昇。動畫連續劇諸如《[新世紀福音戰士](../Page/新世紀福音戰士.md "wikilink")》和《[攻殼機動隊](../Page/攻殼機動隊.md "wikilink")》於日本大受歡迎，亦吸引了海外觀眾的注意。《[七龍珠](../Page/七龍珠.md "wikilink")》、《[美少女戰士](../Page/美少女戰士.md "wikilink")》和《[神奇寶貝](../Page/神奇寶貝.md "wikilink")》的周邊商品在歐美地區更是搶手貨。《[神隱少女](../Page/神隱少女.md "wikilink")》於2002年[德國](../Page/德國.md "wikilink")[柏林影展中得到](../Page/柏林影展.md "wikilink")[金熊獎](../Page/金熊獎.md "wikilink")，並在第七十六屆[奧斯卡電影頒獎典禮中得到](../Page/奧斯卡金像獎.md "wikilink")「最佳動畫長片」一獎項\[117\]。而《[攻殼機動隊](../Page/攻殼機動隊.md "wikilink")》亦在2004年[坎城影展上映](../Page/坎城影展.md "wikilink")\[118\]。

日本動畫業中較為重要的製作公司包括了[東映動畫](../Page/東映動畫.md "wikilink")、[GAINAX](../Page/GAINAX.md "wikilink")、[MADHOUSE](../Page/MADHOUSE.md "wikilink")、[GONZO](../Page/GONZO.md "wikilink")、[日升動畫等等](../Page/日昇動畫.md "wikilink")\[119\]。日本大部分的動畫製作公司都加入了[日本動畫協會](../Page/日本動畫協會.md "wikilink")，而不同製作室在一些較為複雜和造價較高的動畫製作項目上也會互相合作，例如由吉卜力工作室製作的《[千與千尋](../Page/千與千尋.md "wikilink")》就邀請了MADHOUSE、GAINAX、[Production
I.G和](../Page/Production_I.G.md "wikilink")[雲雀工作室等合作夥伴協助製片](../Page/雲雀工作室.md "wikilink")\[120\]。每部動畫大概需要10萬至30萬美元的製作費\[121\]。2001年，動畫片佔據了日本電影市場的7%收入\[122\]，而日本動畫的成功也反映在DVD銷售上，日本近七成的DVD銷售均為動畫\[123\]。

隨著動畫市場的迅速擴張，大量在1990年代成立，並帶動了[動畫展覽會的興起](../Page/動畫展覽會.md "wikilink")\[124\]。這些展覽會主要展示了日本以至世界各國的動漫作品，並包含了[cosplay比賽等其他元素](../Page/cosplay.md "wikilink")\[125\]。另一方面，日本的動漫文化亦創造了不少獨特的名稱，例如用於指稱熱衷及博精於[ACG的](../Page/ACG.md "wikilink")「[御宅族](../Page/御宅族.md "wikilink")」\[126\]。

### 电子游戏

电子游戏，亦叫电玩，是动漫的延伸。有别于美国游戏着重操作感，日本游戏更着重游戏细节的表现，小游戏简单易玩，大游戏掺杂极多的小游戏以增加娱乐性，当然也有同时具有日美元素的作品。日本游戏机生产商占了三大游戏机商的两个，包括索尼（SONY）、任天堂，也有已经退出的世嘉（SEGA）。

## 参考文献

## 外部連結

  - [](https://web.archive.org/web/20091231055207/http://www.nova.ne.jp/mamechishiki/index.htm)
  - [ JapanType](http://www.japantype.com/)
  - [](http://www.geocities.jp/sugiiteruo/index.htm)
  - [Hanami Web - Inside Japan](http://www.hanamiweb.com)

{{-}}

[Japan](../Category/亚洲各国文化.md "wikilink")
[日本文化](../Category/日本文化.md "wikilink")
[Category:東亞文化圈](../Category/東亞文化圈.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.

9.

10.

11.

12.
    ([Translation](http://translate.google.co.uk/translate?hl=en&sl=ja&tl=en&u=http://web.archive.org/web/20090603164729/http://idol.who.ne.jp/modules/page05/content/index.php?id=10))

13.

14.

15.
16.

17.

18.

19.

20.

21.

22.

23.

24.

25.

26.

27.

28.

29.

30.

31.
32.

33.

34.

35.
36.

37.

38.

39.

40.

41.

42.

43.

44.

45.

46.

47.

48.

49.

50.

51. [中央情報局](../Page/中央情報局.md "wikilink")[2012年調査](../Page/2012年.md "wikilink")

52.

53.

54.

55.

56.

57.

58.

59.

60.

61.

62.

63.

64.
65.

66.

67.

68.

69.

70.

71.

72.

73.

74.

75.

76.

77. 《中國化的日本：日中「文明衝突」千年史》 與那霸潤.2011

78.

79.

80.

81.

82.

83.

84.
85.

86.

87.

88.

89.

90.

91.

92.

93.

94.

95.

96. [中国选美小姐遭“吐槽”最美OR最丑](http://www.fh21.com.cn/meirong/tpgs/332101.html)


97. [日國際小姐代表長這樣？21歲本鄉李來奪冠遭批「臉短」](http://www.ettoday.net/news/20140126/320104.htm)

98. Gravett, Paul. 2004. *Manga: Sixty Years of Japanese Comics.* NY:
    Harper Design. ISBN 978-1-85669-391-2. p. 8.

99. Schodt, Frederik L. 1996. *Dreamland Japan: Writings on Modern
    Manga.* Berkeley, CA: Stone Bridge Press. ISBN 978-1880656235.

100. "Japanese Manga Market Drops Below 500 Billion Yen." (2007-03-10)
     <http://comipress.com/news/2007/03/10/1622> Accessed 2007-09-14.

101. Kishi, Torajiro. 1998. *Colorful*. Tokyo: Shueisha. ISBN
     4-08-782556-6.

102. Katzenstein, Peter. J. & Takashi Shiraishi 1997. '' Network Power:
     Japan in Asia.'' Ithaca, NY: Cornell University Press. ISBN
     978-0801483738.

103.
104.

105.
106.
107.

108.

109.

110.

111.

112.

113.

114.

115.

116.

117.

118.

119.

120.
121.

122.
123.
124.

125.
126.