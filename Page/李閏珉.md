**李閏珉**（**YIRUMA**，，），是一位出身[韩国](../Page/大韓民國.md "wikilink")[首爾的](../Page/首爾.md "wikilink")[新世纪音樂](../Page/新世紀音樂.md "wikilink")[钢琴家與](../Page/钢琴家.md "wikilink")[作曲家](../Page/作曲家.md "wikilink")。

他对传统的[古典音乐十分关心](../Page/古典音乐.md "wikilink")，也对[电影音乐十分关注](../Page/电影.md "wikilink")，所以在他的音乐中更富有感性。他的音乐在[中国大陆](../Page/中国大陆.md "wikilink")、[香港](../Page/香港.md "wikilink")、[台湾](../Page/台湾.md "wikilink")、[日本以及](../Page/日本.md "wikilink")[欧美都受到很大程度的欢迎](../Page/欧美.md "wikilink")。他最著名的作品《雨的印记》（）和《在你身上流動的河》（）更是确立他在[新世纪音乐的地位](../Page/新世纪音乐.md "wikilink")。

雨的印记于2003年收录在其钢琴曲专辑《From The Yellow
Room》中，据说是星夜中的一场骤雨激发了李闰珉的灵感，从而创作出此曲。\[1\]

## 专辑

| 专辑名称                                        | 发行日期       |
| ------------------------------------------- | ---------- |
| **Love Scene**                              | 2001-05-16 |
| **First Love**                              | 2001-12-01 |
| **Oasis & Yiruma**                          | 2002-06-20 |
| **Doggy Poo**                               | 2002-12-17 |
| **From The Yellow Room**                    | 2003-10-23 |
| **Nocturnal lights... they scatter**        | 2004-08-05 |
| **Destiny of Love**                         | 2005-04-19 |
| **First Love \[Re-packaged\]**              | 2005-05-19 |
| **YIRUMA Live at HOAM Art Hall**            | 2005-07-12 |
| **POEMUSIC**                                | 2005-11-29 |
| **h.i.s. monologue**                        | 2006-11-02 |
| **P.N.O.N.I.**                              | 2008-10-30 |
| '''Missing You '''                          | 2009-01-01 |
| **Movement on a theme 1,2**                 | 2009-10-15 |
| **The Very Best Of Yiruma：Yiruma & Piano**  | 2011-11-13 |
| **The Best - Reminiscent 10th Anniversary** | 2011-12-09 |
| **Global Project Kiss the Rain**            | 2012-01-28 |
| **Prenatal Education Music**                | 2012-03-20 |
| **Stay In Memory**                          | 2012-05-24 |
| **Blind Film**                              | 2013-10-08 |
| '''Healing Piano '''                        | 2013-11-06 |

## 家庭、感情生活

外界对于李闰珉的感情生活知道的很少。

李閏珉于2007年與孫慧林结婚，韩国女星[孫泰英是其妹妹](../Page/孫泰英.md "wikilink")。[孫泰英及丈夫](../Page/孫泰英.md "wikilink")（韩国著名男星[权相佑](../Page/权相佑.md "wikilink")）曾一同参加过Yiruma的音乐会\[2\]。

## 參考資料

## 外部链接

  - [風潮唱片](https://web.archive.org/web/20051109041336/http://www.wind-records.com.tw/product/newslink/TCD-9106.htm)

[L](../Category/韓國音樂家.md "wikilink")
[L](../Category/新世纪音乐家.md "wikilink")
[L](../Category/首爾特別市出身人物.md "wikilink")
[Category:韩国电影音乐作曲家](../Category/韩国电影音乐作曲家.md "wikilink")

1.
2.