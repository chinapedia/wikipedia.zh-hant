[缩略图](https://zh.wikipedia.org/wiki/File:Hortobágy.jpg "fig:缩略图")\]\]
**匈牙利大平原**（[匈牙利語](../Page/匈牙利语.md "wikilink")：Alföld）是[東歐的平原](../Page/東歐.md "wikilink")，北面和東面是[喀爾巴阡山脈](../Page/喀爾巴阡山脈.md "wikilink")，面積約100,000平方公里，其中約56%面積由[匈牙利負責管轄](../Page/匈牙利.md "wikilink")，最高點海拔高度183米，[蒂薩河是該地區最重要的河流](../Page/蒂薩河.md "wikilink")。
在[匈牙利语](../Page/匈牙利语.md "wikilink")，这个平原被称为Alföld
\[ˈɒlføld\]，意为“低地”。\[1\][斯洛伐克语为](../Page/斯洛伐克语.md "wikilink")
Veľká dunajská kotlina，[罗马尼亚语叫](../Page/罗马尼亚语.md "wikilink") Câmpia
Tisei 或者 Câmpia de Vest, [克罗地亚语](../Page/克罗地亚语.md "wikilink") Panonska
nizina, [塞尔维亚语](../Page/塞尔维亚语.md "wikilink") Panonska
nizija，[乌克兰语](../Page/乌克兰语.md "wikilink") Тисо-Дунайська
низовина。

## 参考来源

## 外部連結

  - [Körös Regional Archaeological
    Project](https://web.archive.org/web/20070211073956/http://www.anthro.fsu.edu/research/koros/overview/arch_background/arch_background.html):
    Neolithic and Copper Age archaeology in the Great Hungarian Plain

[Category:欧洲平原](../Category/欧洲平原.md "wikilink")
[Category:匈牙利地形](../Category/匈牙利地形.md "wikilink")

1.