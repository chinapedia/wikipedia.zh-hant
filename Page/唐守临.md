**唐守临**（），又名**唐醒**，是[倪柝声的](../Page/倪柝声.md "wikilink")[同工之一](../Page/同工.md "wikilink")，1950年至1958年[上海地方教会](../Page/上海地方教会.md "wikilink")[长老之一](../Page/长老.md "wikilink")。1956年以后带领一部分[地方教会信徒参加](../Page/地方教会.md "wikilink")[三自爱国运动](../Page/三自爱国运动.md "wikilink")，并长期担任[中国基督教三自爱国运动委员会副主席](../Page/中国基督教三自爱国运动委员会.md "wikilink")。

## 生平

1906年，唐守临出生在[苏州的一个](../Page/苏州.md "wikilink")[牧师家庭](../Page/牧师.md "wikilink")，1927年毕业于[东吴大学](../Page/东吴大学.md "wikilink")，毕业后在[东吴大学附属第二中学](../Page/东吴大学附属第二中学.md "wikilink")（位于上海虹口昆山路，[景林堂附近](../Page/景林堂.md "wikilink")）等校執教。1931年脱离[监理会](../Page/监理会.md "wikilink")，转入[地方教会](../Page/地方教会.md "wikilink")（同他的学生[江守道和](../Page/江守道.md "wikilink")[周行义](../Page/周行义.md "wikilink")\[1\]）。

后来，唐守临经历了属灵的复兴，奉献做全时间傳道人。先在[上海福音书房任編輯](../Page/上海福音书房.md "wikilink")。1936年起，他在浙江、江蘇、上海等地各教會参与事奉。在1930年代，兴起了苏州地方教会，在大锏银巷10号恢复了中断近10年的南京地方教会的聚会\[2\]。抗战期间曾去长沙、重庆等地开展。1950年设立为[上海地方教会的](../Page/上海地方教会.md "wikilink")[长老之一](../Page/长老.md "wikilink")，兼任[上海福音书房经理](../Page/上海福音书房.md "wikilink")。参与编写《诗歌》（增订本及选本），并编著《圣经提要》。

1942年，他是参与革除[倪柝声的](../Page/倪柝声.md "wikilink")[上海地方教会的](../Page/上海地方教会.md "wikilink")11位同工、长老之一。（另外10位是[杜忠臣](../Page/杜忠臣.md "wikilink")、[俞成华](../Page/俞成华.md "wikilink")、[许达微](../Page/许达微.md "wikilink")、[张光荣](../Page/张光荣.md "wikilink")、[朱臣](../Page/朱臣.md "wikilink")、[江守道](../Page/江守道.md "wikilink")、[张愚之](../Page/张愚之.md "wikilink")、[李渊如](../Page/李渊如.md "wikilink")、[张耆年](../Page/张耆年.md "wikilink")、[缪韵春](../Page/缪韵春.md "wikilink")）。此后去[莫干山](../Page/莫干山.md "wikilink")。1948年他们与[倪柝声恢复了交通](../Page/倪柝声.md "wikilink")。

1939年，唐守临曾编译[考门夫人的灵修著作](../Page/考门.md "wikilink")《[荒漠甘泉](../Page/荒漠甘泉.md "wikilink")》，由[上海福音书房出版](../Page/上海福音书房.md "wikilink")，至今广为流传。

唐守臨很有诗歌的[恩赐](../Page/恩赐.md "wikilink")。他写的一些[诗歌被选入地方教会使用的](../Page/诗歌.md "wikilink")《诗歌》中，传唱至今，其中一首在[孹饼聚会中经常被信徒选唱](../Page/孹饼聚会.md "wikilink"):“咒诅他受，祝福我享，苦杯他饮，爱筵我尝，如此恩爱举世无双，我的心哪，永志不忘。”许多基督徒表示，他们从唐守临所写的诗歌得到帮助。

1952年[倪柝声失踪被捕后](../Page/倪柝声.md "wikilink")，唐守临、[任钟祥](../Page/任钟祥.md "wikilink")、[张愚之三位长老代表](../Page/张愚之.md "wikilink")[上海教会和](../Page/上海教会.md "wikilink")[灵粮堂的周福庆](../Page/灵粮堂.md "wikilink")、华美熙、徐天民、陆传芳；[乌鲁木齐北路聚会所的](../Page/新恩堂.md "wikilink")[杨绍唐等负责人](../Page/杨绍唐.md "wikilink")，一同祷告交通，达成共识：在爱国的政治运动上与「三自」团结；但在发生干涉信仰与传道自由和打击信仰事件时就与「三自」斗争。这就是＂又团结，又斗争″的双轨路线。

1956年1月29日，[上海教会一批主要负责人](../Page/上海教会.md "wikilink")[俞成华](../Page/俞成华.md "wikilink")、[张愚之](../Page/张愚之.md "wikilink")、[蓝志一](../Page/蓝志一.md "wikilink")、[汪佩真](../Page/汪佩真.md "wikilink")、[李渊如等均被](../Page/李渊如.md "wikilink")[逮捕或](../Page/逮捕.md "wikilink")[隔离审查](../Page/隔离审查.md "wikilink")，打成[反革命分子](../Page/反革命分子.md "wikilink")。在压力之下，唐守临、[任钟祥](../Page/任钟祥.md "wikilink")、[左弗如代表上海教会再次革除](../Page/左弗如.md "wikilink")[倪柝声](../Page/倪柝声.md "wikilink")，并重新参加三自组织，欲以此保住公开的聚会和[南阳路145号聚会所](../Page/南阳路145号聚会所.md "wikilink")。但到了1958年“献堂献庙”、[联合礼拜的运动中](../Page/联合礼拜.md "wikilink")，被迫交出[南阳路聚会所](../Page/南阳路聚会所.md "wikilink")，到怀恩堂小房间维持聚会，此后再也没能收回会所。此後，唐守临在上海[懷恩堂擔任静安区联合礼拜传道](../Page/懷恩堂.md "wikilink")，并且任中國基督教三自愛國運動委員會史料組編譯員。

1980年担任[中国基督教三自爱国运动委员会副主席](../Page/中国基督教三自爱国运动委员会.md "wikilink")，1982年，唐守临擔任中國基督教聖詩委員會委員。1988年後，歷任[中國基督教協會第二](../Page/中國基督教協會.md "wikilink")、三屆顧問。

1980年代初，[唐守臨](../Page/唐守臨.md "wikilink")、[周行义和一位弟兄](../Page/周行义.md "wikilink")恢复上海[地方教会的聚会](../Page/地方教会.md "wikilink")。1985年1月，经批准得以在怀恩堂小房间内恢复擘饼聚会。

1983年4月，唐守臨和[任钟祥二人配合](../Page/任钟祥.md "wikilink")[中华人民共和国政府取缔](../Page/中华人民共和国政府.md "wikilink")[李常受](../Page/李常受.md "wikilink")[召会在](../Page/召会.md "wikilink")[中国大陆的分支](../Page/中国大陆.md "wikilink")“[呼喊派](../Page/呼喊派.md "wikilink")”的有关行动，為[金陵協和神學院編寫函授教材](../Page/金陵協和神學院.md "wikilink")《坚决抵[李常受的](../Page/李常受.md "wikilink")[异端邪说](../Page/异端.md "wikilink")》。据认为這份教材的內容是根據美國被高等法院判定為誹謗的兩本書《神人》和《彎曲心思者》（後來被美國高等法院判定為誹謗）。

## 著作

  - 编译《[荒漠甘泉](../Page/荒漠甘泉.md "wikilink")》（[上海福音书房](../Page/上海福音书房.md "wikilink")1939年出版）
  - 《基督徒聚會所的自養經驗》（《天風》，1951年3月10日，总第254期，第4页）
  - 《“荒漠甘泉”是反革命分子利用的工具》（《天風》，1959年，总第584期，第19页）
  - 《根據聖經真理認識[三自](../Page/三自.md "wikilink")》（《天風》，1981年9月30日，总第584期，第19页）
  - 与[任鍾祥合著](../Page/任鍾祥.md "wikilink")《堅決抵[李常受的](../Page/李常受.md "wikilink")[異端邪說](../Page/異端.md "wikilink")》（《景風》
    ，1983年9月，总第75期，第15-38页）
  - 《唐守临讲道集》（江苏省基督教协会1985年出版）

## 参考文献

[Category:中国基督教三自爱国运动委员会第三届副主席](../Category/中国基督教三自爱国运动委员会第三届副主席.md "wikilink")
[Category:中华人民共和国基督教新教人物](../Category/中华人民共和国基督教新教人物.md "wikilink")
[Category:上海地方教会人物](../Category/上海地方教会人物.md "wikilink")
[Category:1906年出生](../Category/1906年出生.md "wikilink")
[Category:1993年逝世](../Category/1993年逝世.md "wikilink")
[Category:唐姓](../Category/唐姓.md "wikilink")
[Category:苏州人](../Category/苏州人.md "wikilink")

1.  张锡康：《张锡康回忆录—上海地方教会六十年来的回顾》，光荣出版社，2012，19-31页
2.  [南京地方志](http://njdfz.nje.cn/HTMLNEWS/1111/2009629220743.htm)