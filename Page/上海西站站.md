**上海西站站**是[上海轨道交通一座车站](../Page/上海轨道交通.md "wikilink")，位于[上海市](../Page/上海市.md "wikilink")[普陀区](../Page/普陀区_\(上海市\).md "wikilink")[真如镇街道](../Page/真如镇街道.md "wikilink")[交通路](../Page/交通路.md "wikilink")[铁路上海西站候车室西侧](../Page/上海西站.md "wikilink")，为[上海轨道交通11号线一期的地下车站](../Page/上海轨道交通11号线.md "wikilink")。于2009年12月31日启用。未来，[15號線以及上海軌道交通](../Page/上海軌道交通15號線.md "wikilink")16號線亦将经过本站。

## 車站構造

[島式月台](../Page/島式月台.md "wikilink")1面2線地下車站。

  - 月台配置

<table>
<thead>
<tr class="header">
<th><p>上行</p></th>
<th><p>11号线</p></th>
<th><p><a href="../Page/徐家汇站.md" title="wikilink">徐家汇</a>・<a href="../Page/东方体育中心站.md" title="wikilink">东方体育中心</a>・<a href="../Page/迪士尼站_(上海).md" title="wikilink">迪士尼方向</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>下行</p></td>
<td><p>11号线</p></td>
<td><p><a href="../Page/嘉定北站.md" title="wikilink">嘉定北</a>・<a href="../Page/花桥站_(地铁).md" title="wikilink">花桥方向</a></p></td>
</tr>
</tbody>
</table>

## 公交换乘

01、106、107、117、129、319、724、742、744、762、768、822、859、869、944、966、北嘉线
沪嘉专线（往成都北路方向单向停靠，下客站不上客）、沪唐专线、新嘉专线

## 车站出口

  - 1号口：桃浦路曹杨路口
  - 4号口：交通路南侧，上海西站处

## 参考文献

<references />

  - [上海西站建设用地规划许可证](https://web.archive.org/web/20070927192647/http://www.shghj.gov.cn/Ct_4.aspx?ct_id=00070629D10196)

## 外部链接

## 参见

  - [上海轨道交通11号线](../Page/上海轨道交通11号线.md "wikilink")
  - [上海西站](../Page/上海西站.md "wikilink")
  - 远景规划中，[SML15.svg](https://zh.wikipedia.org/wiki/File:SML15.svg "fig:SML15.svg")[15号线以及](../Page/上海轨道交通15号线.md "wikilink")[SML16.svg](https://zh.wikipedia.org/wiki/File:SML16.svg "fig:SML16.svg")[16号线将途径上海西站](../Page/上海轨道交通16号线.md "wikilink")

[Category:上海市普陀区地铁车站](../Category/上海市普陀区地铁车站.md "wikilink")
[Category:2009年启用的铁路车站](../Category/2009年启用的铁路车站.md "wikilink")
[Category:以建築物命名的鐵路車站](../Category/以建築物命名的鐵路車站.md "wikilink")