**鲟科**是[鲟形目下的一個科](../Page/鲟形目.md "wikilink")，其下有4屬、27种魚類，其中4种可能已經滅絕\[1\]。鱘魚生活在[海洋和大的](../Page/海洋.md "wikilink")[河流](../Page/河流.md "wikilink")、[湖泊中](../Page/湖泊.md "wikilink")，体长最大的超過9米。其化石記錄可追溯到2.45至2.08億年前的[三疊紀時期](../Page/三疊紀.md "wikilink")\[2\]。[匙吻鲟和](../Page/匙吻鲟.md "wikilink")[白鱘雖然與鱘科魚類很像](../Page/白鱘.md "wikilink")，但實際屬於不同的科。不過所有的這些鱘魚因爲化石記錄久遠，都被稱為[活化石](../Page/活化石.md "wikilink")\[3\]\[4\]。鱘生活在歐亞大陸和北美洲的熱帶、溫帶及亞寒帶水域中\[5\]。

鲟科的鱼口前有四须和口排长一横列，生活在海中的一般春季向大江河中洄游产卵。性成熟较迟，因为被人类捕获的多，面临绝种的危险，现在许多国家都加以保护。

## 参考文献

## 外部链接

  - [世界鲟保护协会网页(英文)](http://www.wscs.info)

[\*](../Category/鲟科.md "wikilink")

1.
2.  Birstein, V.J., R. Hanner, and R. DeSalle. 1997. Phylogeny of the
    Acipenseriformes: cytogenic and molecular approaches. Environmental
    Biology of Fishes 48: 127-155.
3.
4.
5.