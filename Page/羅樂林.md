**羅樂林**（；），香港知名甘草演員，曾于1970年代先后加入[佳藝電視及](../Page/佳藝電視.md "wikilink")[亞洲電視](../Page/亞洲電視.md "wikilink")，[無綫電視](../Page/無綫電視.md "wikilink")（TVB）資深藝員（現為基本藝人合約），曾為無綫電視監製[梁材遠的常用演員](../Page/梁材遠.md "wikilink")。

## 背景

羅樂林於[廣東省](../Page/廣東省.md "wikilink")[汕頭市出生](../Page/汕頭市.md "wikilink")，在[澳門與](../Page/澳門.md "wikilink")[廣州成長與就學](../Page/廣州.md "wikilink")。他是家中獨子，在廣州讀小學時參與過廣東省體育運動學校的業餘乒乓球訓練。而在澳門讀至中二年級便到香港生活。在學期間，羅樂林因親戚為[泰國華僑的關係而接觸](../Page/泰國.md "wikilink")[泰拳運動](../Page/泰拳.md "wikilink")。亦會學習[書法和彈奏](../Page/書法.md "wikilink")[小提琴](../Page/小提琴.md "wikilink")。他及後隨父親定居父親友人位於[香港島](../Page/香港島.md "wikilink")[北角的家中](../Page/北角.md "wikilink")。羅樂林來港發展時在父親的旅行社做旅遊領隊，又在親戚的塑膠廠做跟車工人。於1969年與朋友一同報考[邵氏兄弟南國實驗劇團第](../Page/邵氏兄弟.md "wikilink")9期訓練班。當時與他同期的同學有只有9歲的香港女演員兼女歌手[吳香倫](../Page/吳香倫.md "wikilink")。羅樂林就讀訓練班之時，曾以[現代舞舞蹈演員身份參演](../Page/現代舞.md "wikilink")1970年由[張徹導演的電影](../Page/張徹.md "wikilink")《小煞星》。同年因獲得張徹的賞識而被推薦到[邵氏電影的製片部工作](../Page/邵氏電影.md "wikilink")。他在未轉往電視方面發展前參演了一些文藝片的拍攝如1972年電影《年輕人》「小羅」一角。

由於羅樂林希望演出武俠片，所以在1972年與公司提早解除為期5年的電影合約，並轉投富國影業電影公司。不久富國影業突然倒閉。羅樂林以特約演員身份參演一些粵語片如和電影劇集《少年十五二十時》（1976年無綫電視作品的其中一集）。至1975年正式於電視界發展。其首部電視劇作品為1975年的《[射雕英雄傳](../Page/射雕英雄傳_\(1976年電視劇\).md "wikilink")》。當時他所飾演[魯有腳一角的表現不錯](../Page/魯有腳.md "wikilink")。結果獲電視台的員工引薦給佳藝電視的高層如[樂易玲](../Page/樂易玲.md "wikilink")、[蕭笙](../Page/蕭笙.md "wikilink")、[李惠民等人認識](../Page/李惠民.md "wikilink")，且得到《[神鵰俠侶](../Page/神鵰俠侶.md "wikilink")》一劇的試鏡機會。雖然最終出演劇中的男主角，但羅樂林自覺有鄉音和感到力不從心。因此認為自己演得很差。他早於1976年在[佳藝電視演出電視劇](../Page/佳藝電視.md "wikilink")《[神鵰俠侶](../Page/神鵰俠侶.md "wikilink")》，為香港首位飾演男主角[楊過的演員](../Page/楊過.md "wikilink")\[1\]，[李通明則飾演](../Page/李通明.md "wikilink")[小龍女](../Page/小龍女.md "wikilink")。自從佳藝電視倒閉後，羅樂林得知無綫電視有足夠的人材，遂轉投演員數目不多、[亞洲電視的前身麗的電視](../Page/亞洲電視.md "wikilink")，演出過《[天蠶變](../Page/天蠶變.md "wikilink")》、《[大內群英](../Page/大內群英.md "wikilink")》、《[太極張三豐](../Page/太極張三豐_\(電視劇\).md "wikilink")》、《[秦始皇](../Page/秦始皇.md "wikilink")》等。一直到了1990年，他獲[馮美基邀請轉投](../Page/馮美基.md "wikilink")[無綫電視](../Page/無綫電視.md "wikilink")\[2\]。羅樂林入行多年最為人熟悉的演出是在《[大時代](../Page/大時代.md "wikilink")》中飾演炒股票致精神失常的「股神」葉天，曾把股票心得傳授予主角方展博（[劉青雲](../Page/劉青雲.md "wikilink")），臨死前傳授方一首稀奇古怪的詩，稱為「股票必勝法」\[3\]。在2014年，羅樂林在《[萬千星輝頒獎典禮2014](../Page/萬千星輝頒獎典禮2014.md "wikilink")》榮獲頒發「專業演員大獎」\[4\]；藉此肯定及嘉許他在演藝圈中的貢獻及付出。

## 家庭與婚姻

  - 羅樂林曾有兩段婚姻。其中第二位前妻為息影演員[陳寶儀](../Page/陳寶儀.md "wikilink")（與[陳寶珠同為粵劇名伶](../Page/陳寶珠.md "wikilink")[宮粉紅養女](../Page/宮粉紅.md "wikilink")）。他於2011年5月31日接受[查小欣主持的](../Page/查小欣.md "wikilink")[商業電台節目訪問中承認與妻子陳寶儀已離婚](../Page/商業電台.md "wikilink")，但仍一起同住。羅樂林有一個與[陳寶儀生下的女兒](../Page/陳寶儀.md "wikilink")；2012年10月4日羅苡之與男演員[陳志健結婚](../Page/陳志健.md "wikilink")。另外，羅樂林與第一任妻子尚育有兩名女兒\[5\]。

## 趣聞

  - 在2011年4月6日20:30起，羅樂林分別在以下五齣[翡翠台電視劇集](../Page/翡翠台.md "wikilink")：
      - [洪武三十二](../Page/洪武三十二.md "wikilink")
      - [女拳](../Page/女拳.md "wikilink")
      - [七號差館](../Page/七號差館_\(電視劇\).md "wikilink")（非黃金時段）
      - [布衣神相](../Page/布衣神相_\(電視劇\).md "wikilink")（非黃金時段）
      - [皆大歡喜](../Page/皆大歡喜_\(古裝電視劇\).md "wikilink")（非黃金時段）

以及4月8日14:05在[無綫經典台播出的](../Page/無綫經典台.md "wikilink")《[烏金血劍](../Page/烏金血劍_\(電視劇\).md "wikilink")》巧合地在劇內死去，令他在20小時內連「死5次」，44小時內連「死6次」\[6\]。

對於羅樂林連「死5次」，除了引來香港[網民一時熱話外](../Page/網民.md "wikilink")\[7\]，英國[每日電訊報](../Page/每日電訊報.md "wikilink")、美國AllVoices、意大利AGINewsOn、匈牙利BorsodOnline、[澳大利亚广播公司及](../Page/澳大利亚广播公司.md "wikilink")[法新社德文版合共](../Page/法新社.md "wikilink")6個國家均有報道，成為國際娛樂新聞\[8\]。

## 演出作品

### 電視劇（[佳藝電視](../Page/佳藝電視.md "wikilink")）

|                                                   |                                                   |                                    |
| ------------------------------------------------- | ------------------------------------------------- | ---------------------------------- |
| **首播**                                            | **劇名**                                            | **角色**                             |
| 1975年                                             | [射雕英雄傳](../Page/射雕英雄傳_\(1976年電視劇\).md "wikilink") | [魯有腳](../Page/魯有腳.md "wikilink")   |
| 1976年                                             | [神鵰俠侶](../Page/神鵰俠侶_\(1976年電視劇\).md "wikilink")   | **[楊過](../Page/楊過.md "wikilink")** |
| 1977年                                             | [紅樓夢](../Page/紅樓夢_\(1977年電視劇\).md "wikilink")     | [焙茗](../Page/焙茗.md "wikilink")     |
| [白髮魔女傳](../Page/白髮魔女傳_\(1977年電視劇\).md "wikilink") | '''[岳鳴珂](../Page/岳鳴珂.md "wikilink")               |                                    |
| [仙鶴神針](../Page/仙鶴神針_\(1977年電視劇\).md "wikilink")   | 馬君武                                               |                                    |
| [雪山飛狐](../Page/雪山飛狐_\(電視劇\).md "wikilink")        | 田歸農                                               |                                    |
| 1978年                                             | [流星蝴蝶劍](../Page/流星蝴蝶劍_\(1978年電視劇\).md "wikilink") | [孟星魂](../Page/孟星魂.md "wikilink")   |
| [金刀情俠](../Page/金刀情俠_\(電視劇\).md "wikilink")        |                                                   |                                    |
| 未完成拍攝                                             | 風雷第一刀                                             |                                    |

### 電視劇（[麗的電視](../Page/麗的電視.md "wikilink")/[亞洲電視](../Page/亞洲電視.md "wikilink")）

|                                               |                                              |                                |
| --------------------------------------------- | -------------------------------------------- | ------------------------------ |
| **首播**                                        | **劇名**                                       | **角色**                         |
| 1979年                                         | [情人箭](../Page/情人箭_\(電視劇\).md "wikilink")     | 展夢白                            |
| [俠盜風流](../Page/俠盜風流.md "wikilink")            | 江湖一點紅                                        |                                |
| [天蠶變](../Page/天蠶變.md "wikilink")              | 陸丹                                           |                                |
| [天龍訣](../Page/天龍訣.md "wikilink")              | 陸丹                                           |                                |
| 1980年                                         | [湖海爭霸錄](../Page/湖海爭霸錄.md "wikilink")         | 鐵角                             |
| [大內群英](../Page/大內群英.md "wikilink")            | 無名                                           |                                |
| [大地恩情](../Page/大地恩情.md "wikilink")            | 容昌                                           |                                |
| [大內群英續集](../Page/大內群英續集.md "wikilink")        | 白眉道长（单翼蜻蜓/十四皇爷）                              |                                |
| [太極張三豐](../Page/太極張三豐_\(電視劇\).md "wikilink")  | 朱重八                                          |                                |
| 1981年                                         | [少年黃飛鴻](../Page/少年黃飛鴻_\(電視劇\).md "wikilink") | 納蘭貴福                           |
| [蕩寇誌](../Page/蕩寇誌_\(電視劇\).md "wikilink")      | 馬青海                                          |                                |
| 1982年                                         | [大將軍](../Page/大將軍.md "wikilink")             |                                |
| [老婆越老越可愛](../Page/老婆越老越可愛.md "wikilink")      |                                              |                                |
| [烽火情仇](../Page/烽火情仇.md "wikilink")            |                                              |                                |
| [再生戀](../Page/再生戀.md "wikilink")              |                                              |                                |
| 1983年                                         | [101拘捕令](../Page/101拘捕令.md "wikilink")       | 徐大華                            |
| [誓不低頭](../Page/誓不低頭_\(亞洲電視劇集\).md "wikilink") | 杜五                                           |                                |
| [誓不低頭續集](../Page/誓不低頭續集.md "wikilink")        | 杜五                                           |                                |
| [劍仙李白](../Page/劍仙李白.md "wikilink")            | 李璘                                           |                                |
| [阿SIR阿SIR](../Page/阿SIR阿SIR.md "wikilink")    |                                              |                                |
| 1984年                                         | [醉拳王無忌](../Page/醉拳王無忌.md "wikilink")         | 劍仙/雷劍飛                         |
| [毋忘我](../Page/毋忘我_\(電視劇\).md "wikilink")      | 莊平                                           |                                |
| [十二金牌](../Page/十二金牌.md "wikilink")            |                                              |                                |
| [101拘捕令2之熱線999](../Page/101拘捕令.md "wikilink") | [蕭邦](../Page/蕭邦.md "wikilink")               |                                |
| [101拘捕令3之勇敢新世界](../Page/101拘捕令.md "wikilink") |                                              |                                |
| 1985年                                         | [四大名捕重出江湖](../Page/四大名捕重出江湖.md "wikilink")   | 冷血                             |
| [住家男人](../Page/住家男人.md "wikilink")            |                                              |                                |
| [天涯明月刀](../Page/天涯明月刀.md "wikilink")          | [燕南飛](../Page/燕南飛.md "wikilink")             |                                |
| [諜網迷情](../Page/諜網迷情.md "wikilink")            |                                              |                                |
| [諸葛亮](../Page/諸葛亮_\(亞視電視劇集\).md "wikilink")   | [周瑜](../Page/周瑜.md "wikilink")               |                                |
| 1986年                                         | [冒險家樂園](../Page/冒險家樂園.md "wikilink")         |                                |
| [哪吒](../Page/哪吒_\(1986年電視劇\).md "wikilink")   | 李靖                                           |                                |
| [秦始皇](../Page/秦始皇_\(香港電視劇\).md "wikilink")    | [韓非](../Page/韓非.md "wikilink")               |                                |
| 1987年                                         | [母親](../Page/母親.md "wikilink")               | 父親                             |
| [獅子之王](../Page/獅子之王.md "wikilink")            |                                              |                                |
| [包公](../Page/包公.md "wikilink")                |                                              |                                |
| [香港情](../Page/香港情.md "wikilink")              |                                              |                                |
| [武林故事](../Page/武林故事.md "wikilink")            |                                              |                                |
| [滿清十三皇朝](../Page/滿清十三皇朝.md "wikilink")        | [多爾袞](../Page/多爾袞.md "wikilink")             |                                |
| 1988年                                         | [鍾馗捉鬼](../Page/鍾馗捉鬼.md "wikilink")           | [鍾馗](../Page/鍾馗.md "wikilink") |
| [聊齋誌異](../Page/聊齋誌異.md "wikilink")            | [王生](../Page/王生.md "wikilink")               |                                |
| [十月風暴](../Page/十月風暴.md "wikilink")            |                                              |                                |
| [俠女傳奇](../Page/俠女傳奇.md "wikilink")            |                                              |                                |
| 1989年                                         | [龍鳳喜迎春](../Page/龍鳳喜迎春.md "wikilink")         |                                |
| [夜琉璃](../Page/夜琉璃.md "wikilink")              |                                              |                                |
| [皇家檔案之黑五類](../Page/皇家檔案.md "wikilink")        | 高偉健                                          |                                |
| [男大當差](../Page/男大當差.md "wikilink")            |                                              |                                |
|                                               |                                              |                                |

### 電視劇（[無綫電視](../Page/無綫電視.md "wikilink")）

<table>
<tbody>
<tr class="odd">
<td><p><strong>首播</strong></p></td>
<td><p><strong>劇名</strong></p></td>
<td><p><strong>角色</strong></p></td>
</tr>
<tr class="even">
<td><p>1976年</p></td>
<td><p><a href="../Page/少年十五二十時.md" title="wikilink">少年十五二十時</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1990年</p></td>
<td><p><a href="../Page/劍魔獨孤求敗.md" title="wikilink">劍魔獨孤求敗</a></p></td>
<td><p>獨孤天峰</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/我本善良_(香港電視劇).md" title="wikilink">我本善良</a></p></td>
<td><p>蔣定邦</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/奇幻人間世.md" title="wikilink">奇幻人間世</a></p></td>
<td><p>石頭道人</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/烏金血劍_(電視劇).md" title="wikilink">烏金血劍</a></p></td>
<td><p>宗單</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/飛越官場.md" title="wikilink">飛越官場</a></p></td>
<td><p>韋玉冠</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1991年</p></td>
<td><p><a href="../Page/蜀山奇俠之仙侶奇緣.md" title="wikilink">蜀山奇俠之仙侶奇緣</a></p></td>
<td><p>軒轅法王</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/怒劍嘯狂沙.md" title="wikilink">怒劍嘯狂沙</a></p></td>
<td><p>烏恩</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/藍色風暴.md" title="wikilink">藍色風暴</a></p></td>
<td><p>雷軍</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1992年</p></td>
<td><p><a href="../Page/壹號皇庭.md" title="wikilink">壹號皇庭</a></p></td>
<td><p>李大成</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/血璽金刀.md" title="wikilink">血璽金刀</a></p></td>
<td><p>張真</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/大時代.md" title="wikilink">大時代</a></p></td>
<td><p>葉天</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/風之刀.md" title="wikilink">風之刀</a></p></td>
<td><p>洪震</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/出位江湖.md" title="wikilink">出位江湖</a></p></td>
<td><p>毛語問</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1993年</p></td>
<td><p><a href="../Page/如來神掌再戰江湖.md" title="wikilink">如來神掌再戰江湖</a></p></td>
<td><p><a href="../Page/龍劍飛.md" title="wikilink">龍劍飛</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/射鵰英雄傳之九陰真經.md" title="wikilink">射鵰英雄傳之九陰真經</a></p></td>
<td><p><a href="../Page/歐陽峰.md" title="wikilink">歐陽峰</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/武尊少林.md" title="wikilink">武尊少林</a></p></td>
<td><p><a href="../Page/雍正.md" title="wikilink">雍正</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/馬場大亨_(電視劇).md" title="wikilink">馬場大亨</a></p></td>
<td><p>倪冠鶴</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/魔刀俠情.md" title="wikilink">魔刀俠情</a></p></td>
<td><p>逆天唯我</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1994年</p></td>
<td><p><a href="../Page/清宮氣數錄.md" title="wikilink">清宮氣數錄</a></p></td>
<td><p>廉抱山</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/生死訟.md" title="wikilink">生死訟</a></p></td>
<td><p>馬錦超</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黃飛鴻系列之鐵膽梁寬.md" title="wikilink">黃飛鴻系列之鐵膽梁寬</a></p></td>
<td><p>陸志剛（惡霸）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1995年</p></td>
<td><p><a href="../Page/包青天_(無綫電視劇).md" title="wikilink">包青天</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/萬里長情.md" title="wikilink">萬里長情</a></p></td>
<td><p>周金榮</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/O記實錄.md" title="wikilink">O記實錄</a></p></td>
<td><p>羅金勝</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/忘情闊少爺.md" title="wikilink">忘情闊少爺</a></p></td>
<td><p>高振豪</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/大捕快.md" title="wikilink">大捕快</a></p></td>
<td><p>何化成</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/刀馬旦_(無綫電視劇).md" title="wikilink">刀馬旦</a></p></td>
<td><p>沈鐵蘭</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1996年</p></td>
<td><p><a href="../Page/武當張三丰.md" title="wikilink">武當張三丰</a></p></td>
<td><p>火龍真人</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/笑傲江湖_(1996年电视剧).md" title="wikilink">笑傲江湖</a></p></td>
<td><p><a href="../Page/任我行.md" title="wikilink">任我行</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1997年</p></td>
<td><p><a href="../Page/大刺客_(電視劇).md" title="wikilink">大刺客</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/保護證人組_(電視劇).md" title="wikilink">保護證人組</a></p></td>
<td><p>陳忠義</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/大鬧廣昌隆.md" title="wikilink">大鬧廣昌隆</a></p></td>
<td><p>車月亭</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/刑事偵緝檔案III.md" title="wikilink">刑事偵緝檔案III</a></p></td>
<td><p>方崇業（檔案九：爸爸對不起）（司徒莎莎之老闆）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1998年</p></td>
<td><p><a href="../Page/乾隆大帝.md" title="wikilink">乾隆大帝</a></p></td>
<td><p><a href="../Page/雍正.md" title="wikilink">雍正</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/西遊記（貳）.md" title="wikilink">西遊記（貳）</a></p></td>
<td><p>陳光蕊/<a href="../Page/東海龍王.md" title="wikilink">東海龍王</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1999年</p></td>
<td><p><a href="../Page/雪山飛狐_(1999年電視劇).md" title="wikilink">雪山飛狐</a></p></td>
<td><p><a href="../Page/吳三桂.md" title="wikilink">吳三桂</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/寵物情緣.md" title="wikilink">寵物情緣</a></p></td>
<td><p>戴忠</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/刑事偵緝檔案IV.md" title="wikilink">刑事偵緝檔案IV</a></p></td>
<td><p>文國泰（檔案七：父女同心）（文婉蘭之父）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/狀王宋世傑（貳）.md" title="wikilink">狀王宋世傑（貳）</a></p></td>
<td><p>鐵威（鐵頭將軍）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/十三密殺令.md" title="wikilink">十三密殺令</a></p></td>
<td><p><a href="../Page/嚴世藩.md" title="wikilink">嚴世藩</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/反黑先鋒.md" title="wikilink">反黑先鋒</a></p></td>
<td><p>常國棟</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/騙中傳奇.md" title="wikilink">騙中傳奇</a></p></td>
<td><p>錢方孔</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/非常保鑣.md" title="wikilink">非常保鑣</a></p></td>
<td><p>傅百堅</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/雙面伊人.md" title="wikilink">雙面伊人</a></p></td>
<td><p>林耀安</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/布袋和尚_(1999年電視劇).md" title="wikilink">布袋和尚</a></p></td>
<td><p><a href="../Page/魏徵.md" title="wikilink">魏徵</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/人龍傳說.md" title="wikilink">人龍傳說</a></p></td>
<td><p>周傲天（第1、4-12、14-20集）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/創世紀_(電視劇).md" title="wikilink">創世紀</a></p></td>
<td><p>馬成才</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2000年</p></td>
<td><p><a href="../Page/雷霆第一关.md" title="wikilink">雷霆第一关</a></p></td>
<td><p>冯永发</p></td>
</tr>
<tr class="odd">
<td><p>2001年</p></td>
<td><p><a href="../Page/封神榜_(2001年電視劇).md" title="wikilink">封神榜</a></p></td>
<td><p><a href="../Page/姬昌.md" title="wikilink">姬昌</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/皆大歡喜_(古裝電視劇).md" title="wikilink">皆大歡喜（古裝版）</a></p></td>
<td><p>金華</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/七號差館_(電視劇).md" title="wikilink">七號差館</a></p></td>
<td><p>孫全（在2011年於<a href="../Page/翡翠台.md" title="wikilink">翡翠台播映</a>）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2002年</p></td>
<td><p><a href="../Page/無頭東宮.md" title="wikilink">無頭東宮</a></p></td>
<td><p>張伯常</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/再生緣_(2002年電視劇).md" title="wikilink">再生緣</a></p></td>
<td><p>劉捷</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/點指賊賊賊捉賊.md" title="wikilink">點指賊賊賊捉賊</a></p></td>
<td><p>巢思故（在2006年於<a href="../Page/翡翠台.md" title="wikilink">翡翠台播映</a>）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2003年</p></td>
<td><p><a href="../Page/洗冤錄II.md" title="wikilink">洗冤錄II</a></p></td>
<td><p>展堅</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/帝女花.md" title="wikilink">帝女花</a></p></td>
<td><p><a href="../Page/周興.md" title="wikilink">周興</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/皆大歡喜_(時裝電視劇).md" title="wikilink">皆大歡喜（時裝版）</a></p></td>
<td><p>金華</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/金牌冰人.md" title="wikilink">金牌冰人</a></p></td>
<td><p>秦太蔚</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/英雄·刀·少年.md" title="wikilink">英雄·刀·少年</a></p></td>
<td><p>譚繼洵 御史台（<a href="../Page/譚嗣同.md" title="wikilink">譚嗣同之父</a>）</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>Romeo</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2004年</p></td>
<td><p><a href="../Page/烽火奇遇結良緣.md" title="wikilink">烽火奇遇結良緣</a></p></td>
<td><p><a href="../Page/樊洪.md" title="wikilink">樊洪</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/皆大歡喜_(時裝電視劇).md" title="wikilink">皆大歡喜（時裝版）</a></p></td>
<td><p>岑豪（喪坤）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/血薦軒轅.md" title="wikilink">血薦軒轅</a></p></td>
<td><p><a href="../Page/司馬信.md" title="wikilink">司馬信</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/大唐雙龍傳.md" title="wikilink">大唐雙龍傳</a></p></td>
<td><p>宋缺</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/心理心裏有個謎.md" title="wikilink">心理心裏有個謎</a></p></td>
<td><p>萬正良（在2006年於<a href="../Page/翡翠台.md" title="wikilink">翡翠台播映</a>）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/楚漢驕雄.md" title="wikilink">楚漢驕雄</a></p></td>
<td><p><a href="../Page/范增.md" title="wikilink">范增</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2005年</p></td>
<td><p><a href="../Page/學警雄心.md" title="wikilink">學警雄心</a></p></td>
<td><p>壽　哥</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/秀才遇著兵.md" title="wikilink">秀才遇著兵</a></p></td>
<td><p>邵正罡</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/胭脂水粉.md" title="wikilink">胭脂水粉</a></p></td>
<td><p>林唯本</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/本草藥王.md" title="wikilink">本草藥王</a></p></td>
<td><p><a href="../Page/嚴嵩.md" title="wikilink">嚴　嵩</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/布衣神相.md" title="wikilink">布衣神相</a></p></td>
<td><p>無相子（在2011年於<a href="../Page/翡翠台.md" title="wikilink">翡翠台播映</a>）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2006年</p></td>
<td><p><a href="../Page/人生馬戲團.md" title="wikilink">人生馬戲團</a></p></td>
<td><p>何永佳</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鐵血保鏢.md" title="wikilink">鐵血保鏢</a></p></td>
<td><p>駱秉章（<a href="../Page/杭州.md" title="wikilink">杭州衙門大官</a>(第16集出現)）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/飛短留長父子兵.md" title="wikilink">飛短留長父子兵</a></p></td>
<td><p>彭　根（<a href="../Page/陳松伶.md" title="wikilink">陳松伶父親</a>）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/男人之苦.md" title="wikilink">男人之苦</a></p></td>
<td><p>高　威</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/滙通天下_(電視劇).md" title="wikilink">滙通天下</a></p></td>
<td><p>徐永懷(吏部尚書)</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/賭場風雲.md" title="wikilink">賭場風雲</a></p></td>
<td><p>翁漢昌</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/凶城計中計.md" title="wikilink">凶城計中計</a></p></td>
<td><p>屠一夫（屠四海父）（客串）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2007年</p></td>
<td><p><a href="../Page/突圍行動.md" title="wikilink">突圍行動</a></p></td>
<td><p>童展鵬</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/緣來自有機.md" title="wikilink">緣來自有機</a></p></td>
<td><p>蔡積喜</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/同事三分親.md" title="wikilink">同事三分親</a></p></td>
<td><p>戴　金（大舊金）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鐵咀銀牙.md" title="wikilink">鐵咀銀牙</a></p></td>
<td><p>段天虎</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2008年</p></td>
<td><p><a href="../Page/原來愛上賊.md" title="wikilink">原來愛上賊</a></p></td>
<td><p>劉宇超</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/師奶股神.md" title="wikilink">師奶股神</a></p></td>
<td><p>司徒聯輝</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/少年四大名捕.md" title="wikilink">少年四大名捕</a></p></td>
<td><p>鐵　斧</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/法證先鋒2.md" title="wikilink">法證先鋒2</a></p></td>
<td><p>馬錦濤</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/疑情別戀.md" title="wikilink">疑情別戀</a></p></td>
<td><p>凌兆銘</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/與敵同行.md" title="wikilink">與敵同行</a></p></td>
<td><p>曾樹樑</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2009年</p></td>
<td><p><a href="../Page/蔡鍔與小鳳仙.md" title="wikilink">蔡鍔與小鳳仙</a></p></td>
<td><p><a href="../Page/袁世凱.md" title="wikilink">袁世凱</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/畢打自己人.md" title="wikilink">畢打自己人</a></p></td>
<td><p>龍　波（肥波）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2010年</p></td>
<td><p><a href="../Page/秋香怒點唐伯虎.md" title="wikilink">秋香怒點唐伯虎</a></p></td>
<td><p>夏侯純</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/談情說案.md" title="wikilink">談情說案</a></p></td>
<td><p>徐漢飛（飛Sir）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蒲松齡_(電視劇).md" title="wikilink">蒲松齡</a></p></td>
<td><p>令狐吉祥</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/施公奇案II.md" title="wikilink">施公奇案II</a></p></td>
<td><p>丁仁川</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/摘星之旅.md" title="wikilink">摘星之旅</a></p></td>
<td><p>張　華</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/刑警_(無綫電視劇).md" title="wikilink">刑警</a></p></td>
<td><p>高立仁（大Sir）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2011年</p></td>
<td><p><a href="../Page/女拳.md" title="wikilink">女拳</a></p></td>
<td><p>莫　平（莫桂蘭之叔父、游三水之養父）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/誰家灶頭無煙火.md" title="wikilink">誰家灶頭無煙火</a></p></td>
<td><p>高　仁</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/洪武三十二.md" title="wikilink">洪武三十二</a></p></td>
<td><p><strong><a href="../Page/朱元璋.md" title="wikilink">朱元璋</a></strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/點解阿Sir係阿Sir.md" title="wikilink">點解阿Sir係阿Sir</a></p></td>
<td><p>蔣　洪</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/紫禁驚雷.md" title="wikilink">紫禁驚雷</a></p></td>
<td><p>納蘭·明珠</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/法證先鋒III.md" title="wikilink">法證先鋒III</a></p></td>
<td><p>趙大龍</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2012年</p></td>
<td><p><a href="../Page/盛世仁傑.md" title="wikilink">盛世仁傑</a></p></td>
<td><p>曹展鏗</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/護花危情.md" title="wikilink">護花危情</a></p></td>
<td><p>翁瑞邦</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/飛虎_(電視劇).md" title="wikilink">飛虎</a></p></td>
<td><p>俞世棠</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/回到三國.md" title="wikilink">回到三國</a></p></td>
<td><p><strong><a href="../Page/曹操.md" title="wikilink">曹　操</a>（曹孟德）</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/雷霆掃毒.md" title="wikilink">雷霆掃毒</a></p></td>
<td><p>鄭志成</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/幸福摩天輪.md" title="wikilink">幸福摩天輪</a></p></td>
<td><p>許明德/許玉安</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2013年</p></td>
<td><p><a href="../Page/初五啟市錄.md" title="wikilink">初五啟市錄</a></p></td>
<td><p>汪繼堯</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/戀愛星求人.md" title="wikilink">戀愛星求人</a></p></td>
<td><p>張　旺</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/心路GPS.md" title="wikilink">心路GPS</a></p></td>
<td><p>張　旺</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/仁心解碼II.md" title="wikilink">仁心解碼II</a></p></td>
<td><p>朱學禮</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2014年</p></td>
<td><p><a href="../Page/單戀雙城.md" title="wikilink">單戀雙城</a></p></td>
<td><p>麥貫天（Sky哥）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/叛逃_(電視劇).md" title="wikilink">叛逃</a></p></td>
<td><p>喬劍亨</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/廉政行動2014.md" title="wikilink">廉政行動2014</a></p></td>
<td><p>梁浩天（单元一）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/點金勝手.md" title="wikilink">點金勝手</a></p></td>
<td><p>冼祖偉</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/寒山潛龍.md" title="wikilink">寒山潛龍</a></p></td>
<td><p>熊　峰</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/我們的天空.md" title="wikilink">我們的天空</a></p></td>
<td><p>Frankie</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/使徒行者.md" title="wikilink">使徒行者</a></p></td>
<td><p>郭　正</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/老表，你好hea！.md" title="wikilink">老表，你好hea！</a></p></td>
<td><p>綺珊父</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/八卦神探.md" title="wikilink">八卦神探</a></p></td>
<td><p>夏文值</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2015年</p></td>
<td><p><a href="../Page/天眼_(香港電視劇).md" title="wikilink">天眼</a></p></td>
<td><p>司徒國強</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/水髮胭脂.md" title="wikilink">水髮胭脂</a></p></td>
<td><p>連其祥</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/張保仔_(2015年電視劇).md" title="wikilink">張保仔</a></p></td>
<td><p>鄭　一</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/無雙譜_(2015年電視劇).md" title="wikilink">無雙譜</a></p></td>
<td><p>洪太史</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/刀下留人.md" title="wikilink">刀下留人</a></p></td>
<td><p>花在山</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2016年</p></td>
<td><p><a href="../Page/EU超時任務.md" title="wikilink">EU超時任務</a></p></td>
<td><p>凌　海</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/火線下的江湖大佬.md" title="wikilink">火線下的江湖大佬</a></p></td>
<td><p>廖國安</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/愛·回家之八時入席.md" title="wikilink">愛·回家之八時入席</a></p></td>
<td><p>細　父（第132集）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2017年</p></td>
<td><p><a href="../Page/愛·回家之開心速遞.md" title="wikilink">愛·回家之開心速遞</a></p></td>
<td><p>龍敢威</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/與諜同謀.md" title="wikilink">與諜同謀</a></p></td>
<td><p>陸一凡</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/心理追兇_Mind_Hunter.md" title="wikilink">心理追兇 Mind Hunter</a></p></td>
<td><p>胡貴成</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蘭花刼.md" title="wikilink">蘭花刼</a></p></td>
<td><p>張應景</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/踩過界.md" title="wikilink">踩過界</a></p></td>
<td><p>文根鷹</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/同盟.md" title="wikilink">同盟</a></p></td>
<td><p>郭勇军</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/溏心風暴3.md" title="wikilink">溏心風暴3</a></p></td>
<td><p>彭世雄</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2018年</p></td>
<td><p><a href="../Page/天命_(無綫電視劇).md" title="wikilink">天命</a></p></td>
<td><p>朱　珪</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/BB來了.md" title="wikilink">BB來了</a></p></td>
<td><p>唐　秋</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/跳躍生命線.md" title="wikilink">跳躍生命線</a></p></td>
<td><p>蔣世聰</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>未播映</p></td>
<td><p><a href="../Page/包青天再起風雲.md" title="wikilink">包青天再起風雲</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/解決師.md" title="wikilink">解決師</a></p></td>
<td><p>蔣　滔</p></td>
<td></td>
</tr>
</tbody>
</table>

### 電視電影（[無綫電視](../Page/無綫電視.md "wikilink")）

|        |                                          |        |
| ------ | ---------------------------------------- | ------ |
| **首播** | **劇名**                                   | **角色** |
| 1990年  | [綫人](../Page/綫人.md "wikilink")           |        |
| 1995年  | [胭花淚](../Page/胭花淚.md "wikilink")         |        |
| 2003年  | [咫尺疑魂](../Page/咫尺疑魂.md "wikilink")       |        |
| 2003年  | [天降橫財心驚驚](../Page/天降橫財心驚驚.md "wikilink") |        |
| 2004年  | [懸案追兇](../Page/懸案追兇.md "wikilink")       |        |

### 電影

|        |                                                  |        |
| ------ | ------------------------------------------------ | ------ |
| **首播** | **片名**                                           | **角色** |
| 1970年  | 小煞星                                              |        |
| 1971年  | 拳擊                                               |        |
| 1972年  | [年輕人](../Page/年輕人\(1972年電影\).md "wikilink")      | 小羅     |
| 惡客     | 小樂                                               |        |
| 1973年  | 大刀王五                                             | 四君子    |
| 1975年  | 蕩寇志                                              | 小黃門    |
| 1993年  | [白髮魔女傳](../Page/白髮魔女傳_\(1993年電影\).md "wikilink") | 白雲道長   |

### 綜藝節目

  - 2015年：[網絡挑機](../Page/網絡挑機.md "wikilink")
  - 2015年：[刀下留人](../Page/刀下留人.md "wikilink") 訪問(上)\[9\]
  - 2015年：[刀下留人](../Page/刀下留人.md "wikilink") 訪問(下)\[10\]

## 注釋

## 參考來源

## 外部連結

  -
[lok](../Category/羅姓.md "wikilink")
[Category:香港甘草演員](../Category/香港甘草演員.md "wikilink")
[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[Category:前佳藝電視藝員](../Category/前佳藝電視藝員.md "wikilink")
[Category:前亞洲電視藝員](../Category/前亞洲電視藝員.md "wikilink")
[Category:無綫電視男藝員](../Category/無綫電視男藝員.md "wikilink")
[Category:潮州人](../Category/潮州人.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.  [\[刀下留人](https://www.facebook.com/TVBmytv/videos/vb.849567251776114/939094829490022/?type=2&theater)
    羅樂林曾經憑一日死五次成為電視界一時佳話\]
10. [\[刀下留人](https://www.facebook.com/TVBmytv/videos/vb.849567251776114/940054612727377/?type=2&theater)
    羅樂林曾經憑一日死五次成為電視界一時佳話 2\]