**宇陀郡**（）為[奈良縣轄下的](../Page/奈良縣.md "wikilink")[郡](../Page/郡.md "wikilink")。人口4,761人、面積127.47
km²。（2005年）

現有以下兩村。

  - [曾爾村](../Page/曾爾村.md "wikilink")（そにむら）
  - [御杖村](../Page/御杖村.md "wikilink")（みつえむら）

在2006年[宇陀市成立之前](../Page/宇陀市.md "wikilink")，曾經還包括以下四町村

  - [菟田野町](../Page/菟田野町.md "wikilink")（うたのちょう）
  - [大宇陀町](../Page/大宇陀町.md "wikilink")（おおうだちょう）
  - [榛原町](../Page/榛原町.md "wikilink")（はいばらちょう）
  - [室生村](../Page/室生村.md "wikilink")（むろうむら）

## 沿革

1889年4月1日町村制施行以來沿革

<table>
<thead>
<tr class="header">
<th><p>1889年以前</p></th>
<th><p>1889年4月1日</p></th>
<th><p>1889年 - 1944年</p></th>
<th><p>1945年 - 1954年</p></th>
<th><p>1956年 - 2005年</p></th>
<th><p>2006年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>松山町</p></td>
<td><p>1942年2月11日<br />
大宇陀町</p></td>
<td><p>大宇陀町</p></td>
<td><p>大宇陀町</p></td>
<td><p>2006年1月1日<br />
宇陀市</p></td>
<td><p><a href="../Page/宇陀市.md" title="wikilink">宇陀市</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>神戸村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>政始村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>1890年9月23日<br />
<a href="../Page/吉野郡.md" title="wikilink">吉野郡</a><br />
上龍門村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>宇太村</p></td>
<td><p>1935年4月29日<br />
町制</p></td>
<td><p>宇太町</p></td>
<td><p>1956年8月1日<br />
菟田野町</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>宇賀志村</p></td>
<td><p>宇賀志村</p></td>
<td><p>宇賀志村</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>榛原村</p></td>
<td><p>1951年9月27日<br />
町制</p></td>
<td><p>榛原町</p></td>
<td><p>榛原町</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>伊那佐村</p></td>
<td><p>伊那佐村</p></td>
<td><p>1954年7月1日<br />
被併入榛原町</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>内牧村</p></td>
<td><p>内牧村</p></td>
<td><p>内牧村</p></td>
<td><p>1955年4月1日<br />
被併入榛原町</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>室生村</p></td>
<td><p>室生村</p></td>
<td><p>室生村</p></td>
<td><p>1955年2月11日<br />
室生村</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>三本松村</p></td>
<td><p>三本松村</p></td>
<td><p>三本松村</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/山邊郡.md" title="wikilink">山邊郡</a><br />
東里村</p></td>
<td><p>山邊郡<br />
東里村</p></td>
<td><p>山邊郡<br />
東里村</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>曾爾村</p></td>
<td><p>曾爾村</p></td>
<td><p>曾爾村</p></td>
<td><p>曾爾村</p></td>
<td><p>曾爾村</p></td>
<td><p>曾爾村</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>御杖村</p></td>
<td><p>御杖村</p></td>
<td><p>御杖村</p></td>
<td><p>御杖村</p></td>
<td><p>御杖村</p></td>
<td><p>御杖村</p></td>
</tr>
</tbody>
</table>