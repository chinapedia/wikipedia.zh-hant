**吸口虫纲**（學名：）是[环节动物门下的一个纲](../Page/环节动物门.md "wikilink")，生活於海洋。物種数量较少，其身体扁平，呈圆形状，腹部有数对刚毛，因此有学者将之列为[多毛纲下的一个](../Page/多毛纲.md "wikilink")[目](../Page/目.md "wikilink")。从外表看不出吸口虫有分节现象，但其神经系却有分节的构造，它们多寄生在各种[棘皮动物身上](../Page/棘皮动物.md "wikilink")。因为吸口虫生活习性和身体的特殊分节现象，而将之从多毛纲中独立出来。這個奇怪的物種由[Friedrich
Sigismund
Leuckart於](../Page/Friedrich_Sigismund_Leuckart.md "wikilink")1827年首度發現。

本分類有部份物種為[寄生蟲](../Page/寄生蟲.md "wikilink")。

## 分類

  - [Asteriomyzostomidae科](../Page/Asteriomyzostomidae.md "wikilink")
    <small>Jägersten, 1940</small>
  - [Asteromyzostomidae科](../Page/Asteromyzostomidae.md "wikilink")
    <small>Wagin, 1954</small>
  - [Eenymeenymyzostomatidae科](../Page/Eenymeenymyzostomatidae.md "wikilink")
    <small>Summers & Rouse, 2015</small>
  - [Endomyzostomatidae科](../Page/Endomyzostomatidae.md "wikilink")
    <small>Perrier, 1897</small>
  - [Myzostomatidae科](../Page/Myzostomatidae.md "wikilink")
    <small>Benham, 1896</small>
  - [Protomyzostomidae科](../Page/Protomyzostomidae.md "wikilink")
    <small>Stummer-Traunfels, 1926</small>
  - Myzostomida incertae sedis (suborder)

## 參考文獻

## 延伸閱讀

  - Bleidorn C. et al. 2009. *On the phylogenetic position of
    Myzostomida: can 77 genes get it wrong?* [BMC Evolutionary
    Biology](../Page/BMC_Evolutionary_Biology.md "wikilink") 2009,
    9:150.

## 外部連結

[Category:寄生蟲](../Category/寄生蟲.md "wikilink")
[Category:环节动物门](../Category/环节动物门.md "wikilink")