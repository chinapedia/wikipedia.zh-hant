[YingYaiShengLan.jpg](https://zh.wikipedia.org/wiki/File:YingYaiShengLan.jpg "fig:YingYaiShengLan.jpg")》\]\]
**马欢**，[字](../Page/表字.md "wikilink")**宗道**、**汝钦**，[号](../Page/号.md "wikilink")**会稽山樵**，[浙江](../Page/浙江.md "wikilink")[会稽](../Page/会稽.md "wikilink")（今[绍兴](../Page/绍兴.md "wikilink")）人，是一名[穆斯林](../Page/穆斯林.md "wikilink")，通晓[阿拉伯语及](../Page/阿拉伯语.md "wikilink")[波斯语](../Page/波斯语.md "wikilink")\[1\]。[明朝通事](../Page/明朝.md "wikilink")（翻译官）、航海家，曾随[郑和三次](../Page/郑和.md "wikilink")[下西洋](../Page/鄭和下西洋.md "wikilink")。为了纪念马欢，[南沙群岛中有一岛名为](../Page/南沙群岛.md "wikilink")[马欢岛](../Page/马欢岛.md "wikilink")。

## 隨鄭和下西洋

  - [永乐十一年](../Page/永乐_\(明\).md "wikilink")（1413年，第四次下西洋），到访[占城](../Page/占城.md "wikilink")、[爪哇](../Page/爪哇.md "wikilink")、[旧港](../Page/巨港.md "wikilink")、[暹罗](../Page/暹罗.md "wikilink")、[古里](../Page/古里.md "wikilink")、[忽鲁谟斯等国](../Page/忽鲁谟斯.md "wikilink")
  - 永乐十九年（1421年，第六次下西洋），到访[满剌加](../Page/满剌加.md "wikilink")、[亚鲁国](../Page/亚鲁国.md "wikilink")、[苏门答剌](../Page/苏门答剌.md "wikilink")、[锡兰](../Page/锡兰.md "wikilink")、[小葛兰](../Page/小葛兰.md "wikilink")、[柯枝](../Page/柯枝.md "wikilink")、[古里](../Page/古里.md "wikilink")、[祖法儿和](../Page/祖法儿.md "wikilink")[忽鲁谟斯等国](../Page/忽鲁谟斯.md "wikilink")。
  - [宣德六年](../Page/宣德.md "wikilink")（1431年，第七次下西洋），[太监](../Page/太监.md "wikilink")[洪保派遣马欢等七位使者到](../Page/洪保.md "wikilink")[天方](../Page/麥加.md "wikilink")[朝圣](../Page/朝圣.md "wikilink")。

马欢将下西洋时亲身经历的二十国国王、政治、风土、地理、人文、经济状况纪录下来在[景泰二年成书的](../Page/景泰_\(明\).md "wikilink")《[瀛涯胜览](../Page/瀛涯胜览.md "wikilink")》。

## 參考文獻

<div class="references-small">

<references />

  -
  -

</div>

[Category:中国旅游作家](../Category/中国旅游作家.md "wikilink")
[M](../Category/中国航海家.md "wikilink")
[M](../Category/中國旅行家.md "wikilink")
[M](../Category/绍兴人.md "wikilink")
[H欢](../Category/马姓.md "wikilink")
[M](../Category/回族人.md "wikilink")
[M](../Category/明朝穆斯林.md "wikilink")
[Category:中外交通史人名](../Category/中外交通史人名.md "wikilink")
[M](../Category/郑和下西洋.md "wikilink")

1.