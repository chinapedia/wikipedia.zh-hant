[ESPN_STAR_Sports_logo.png](https://zh.wikipedia.org/wiki/File:ESPN_STAR_Sports_logo.png "fig:ESPN_STAR_Sports_logo.png")
**ESPN卫视体育台**（，簡稱ESS）是由时属[新聞集團旗下的](../Page/新聞集團.md "wikilink")[星空傳媒有限公司與](../Page/星空傳媒有限公司.md "wikilink")[華特迪士尼旗下的](../Page/華特迪士尼.md "wikilink")[ESPN各佔一半而成立的媒體公司](../Page/ESPN.md "wikilink")，是[亞洲一家](../Page/亞洲.md "wikilink")[體育頻道供應者](../Page/體育.md "wikilink")。ESS在亞洲拥有多个频道，包括："ESPN
亞洲"、"ESPN 印度"、"ESPN 台灣""、"ESPN 新加坡""、"ESPN 菲律賓"、、"ESPN 香港"、"衛視體育台
香港"、"衛視體育台 亚洲"、"衛視體育台 印度"、"衛視體育台 台灣"、"衛視體育台 東南亞"和"衛視體育台
新加坡"。全部頻道主要播放世界主要體育賽事，配上當地的評述。[ESPN覆蓋超過](../Page/ESPN.md "wikilink")1億1000萬的家庭，而[衛視體育台則覆蓋超過](../Page/衛視體育台.md "wikilink")5400萬家庭。

ESPN STAR
Sports的對象是15至54歲的觀眾，24小時播放全球不同的體育賽事，包括[籃球](../Page/籃球.md "wikilink")、[羽毛球](../Page/羽毛球.md "wikilink")、[足球](../Page/足球.md "wikilink")、[游泳等節目](../Page/游泳.md "wikilink")，包括國際性和地區性的體育節目。ESPN
STAR
Sports主要經加密收費網絡或免費電視廣播服務把節目傳送給亞洲觀眾，節目則由在新加坡的廣播中心和製作中心完成和發放至不同的網絡播放。ESPN
STAR Sports在整個地區擁有500個員工。

隨著2012年ESPN把持有的ESPN STAR
Sports所有股份售予新聞集團，ESPN正式退出亞洲，其在亞洲的ESPN頻道已陸續更名，ESPN
STAR Sports一詞也順勢走入歷史。\[1\]

## ESPN卫视体育台節目管理組

ESPN卫视体育台節目管理組主要負責管理、宣傳、商量和製作體育節目和賽事，曾獲多個獎項，包括1998年及1999年"ATP"的"最佳宣傳獎項"；1998年及1999年"[香港體育發展局](../Page/香港.md "wikilink")"的"最佳體育獎項"；2000年"[新加坡旅遊局](../Page/新加坡.md "wikilink")"的"最佳節目管理獎項"。ESPN卫视体育台節目管理組於11個國家曾參於舉辦超過100個體育節目，包括"亞洲極限運動巡迴賽"、"世界室內五人制足球賽"、"亞洲保齡球巡迴賽"等，於2003年亦籌辦了官方[英格蘭超級聯賽季前錦標賽](../Page/英格蘭超級聯賽.md "wikilink")，在[馬來西亞](../Page/馬來西亞.md "wikilink")[吉隆坡舉辦的](../Page/吉隆坡.md "wikilink")[英國](../Page/英國.md "wikilink")[足球協會聯賽亞洲盃](../Page/足球.md "wikilink")。ESPN卫视体育台節目管理組也創辦了"生力亞洲9號球巡迴賽"，一系列亞洲最佳桌球員選舉。這些節目及賽事也由[ESPN及](../Page/ESPN.md "wikilink")[衛視體育台播放](../Page/衛視體育台.md "wikilink")。

## ESPN卫视体育台網上業務

在網上，ESS成立了"espnstar.com"、"espnstar.com.cn"、"espnstar.com.hk和"espnstar.com.tw"，主要提供深入體育新聞、賽事結局和比賽。網站與其頻道和不同賽事的網絡有緊密聯繫。

## ESPN卫视体育台的特式

每年的比分牌、廣告商、顯示模式、聲音都不同。

## ESPN卫视体育台的節目

### [羽毛球](../Page/羽毛球.md "wikilink")

  - [南韓羽毛球公開賽](../Page/南韓羽毛球公開賽.md "wikilink")（衛視體育）
  - [馬來西亞羽毛球公開賽](../Page/馬來西亞羽毛球公開賽.md "wikilink")（衛視體育）

### [板球](../Page/板球.md "wikilink")（香港區只在[ESPN STAR Sports Cricket Live播放](../Page/now寬頻電視體育平臺#ESPN_STAR_Sports_Cricket_Live.md "wikilink")）

  - [印度板球隊](../Page/印度.md "wikilink")（Indian cricket team）國外巡迴賽
  - [巴基斯坦板球隊](../Page/巴基斯坦.md "wikilink")（Indian cricket team）國外巡迴賽
  - [斯里蘭卡板球隊](../Page/斯里蘭卡.md "wikilink")（Indian cricket team）國外巡迴賽
  - [西印度群島板球隊](../Page/西印度群島.md "wikilink")（West Indies cricket
    team）國外巡迴賽
  - [英國本土木球](../Page/英國.md "wikilink")（UK Domestic Cricket）比賽

### [高爾夫球](../Page/高爾夫球.md "wikilink")

  - [亞洲高爾夫球巡迴賽](../Page/亞洲高爾夫球巡迴賽.md "wikilink")（衛視體育）
  - [歐盟高爾夫球巡迴賽](../Page/歐盟高爾夫球巡迴賽.md "wikilink")（ESPN）
  - [米高佐敦高爾夫球名人邀請賽](../Page/米高佐敦高爾夫球名人邀請賽.md "wikilink")（ESPN）

### [賽車](../Page/賽車.md "wikilink")

  - [A1GP汽車大奖赛](../Page/A1GP汽車大奖赛.md "wikilink")（A1 Grand Prix）（衛視體育）
  - [一级方程式大奖赛](../Page/一级方程式.md "wikilink")（F1）
  - [印第賽車聯賽](../Page/印第賽車聯賽.md "wikilink")（Indy Racing League）
  - [MotoGP 賽車](../Page/世界摩托車錦標賽.md "wikilink")
  - [德國房車大師賽](../Page/德國房車大師賽.md "wikilink")（衛視體育）
  - [世界耐力锦标赛](../Page/世界耐力锦标赛.md "wikilink")（衛視體育）
  - [亞洲房車錦標賽](../Page/亞洲房車錦標賽.md "wikilink")（衛視體育）
  - [亞洲V6方程式](../Page/亞洲V6方程式.md "wikilink")（衛視體育）

### [棒球](../Page/棒球.md "wikilink")

  - [美国職棒大聯盟](../Page/美国職棒大聯盟.md "wikilink")（Major League Baseball）

### [籃球](../Page/籃球.md "wikilink")

  - [NBA](../Page/NBA.md "wikilink") (香港除外)
  - [NCAA籃球賽](../Page/NCAA籃球賽.md "wikilink")
  - [ACC美國大學籃球賽](../Page/ACC美國大學籃球賽.md "wikilink")（ESPN）
  - [超級籃球聯賽](../Page/超級籃球聯賽.md "wikilink")（SBL，只在台湾台播放）

### [極限運動](../Page/極限運動.md "wikilink")

  - [X Games](../Page/X_Games.md "wikilink") (ESPN)
  - [刺激體育](../Page/刺激體育.md "wikilink")（衛視體育）

### [英式足球](../Page/足球.md "wikilink")

  - [英格兰超级足球联赛](../Page/英格兰超级足球联赛.md "wikilink")（印尼、东帝汶、柬埔寨、老挝、缅甸、马来西亚、汶莱、菲律宾、朝鲜、南韩、蒙古国印度、孟加拉、不丹、马尔代夫、尼泊尔、巴基斯坦、斯里兰卡）
  - [英格蘭足總盃](../Page/英格蘭足總盃.md "wikilink")（泰國除外、香港增加STAR Sports
    Plus台分流直播賽事）
  - [歐洲冠軍聯賽](../Page/歐洲冠軍聯賽.md "wikilink")（香港、中國、台湾除外）
  - [欧足联欧洲联赛](../Page/欧足联欧洲联赛.md "wikilink")（香港、中國、台湾除外）
  - [世界盃沙灘足球賽](../Page/世界盃沙灘足球賽.md "wikilink") (ESPN)（香港除外）

### [摩托车比賽](../Page/摩托车.md "wikilink")

  - [FIM 世界摩托车錦標賽](../Page/世界摩托车锦标赛.md "wikilink")（MotoGP）

### [乒乓球](../Page/乒乓球.md "wikilink")

  - [美國乒乓球名人賽](../Page/美國乒乓球名人賽.md "wikilink")（ESPN）
  - [ENERGIS盃](../Page/ENERGIS盃.md "wikilink")（ESPN）

### [橄榄球](../Page/橄榄球.md "wikilink")

  - [嘉士伯盃](../Page/嘉士伯盃.md "wikilink") - (Carlsberg Cup)
  - [健力士欖球超級聯賽](../Page/健力士欖球超級聯賽.md "wikilink") - (Guinness
    Premiership)
  - [六國錦標賽](../Page/六國錦標賽.md "wikilink") - 英國、法國、義大利、愛爾蘭、蘇格蘭、威爾斯 (Six
    Nations Championship)
  - [超級橄欖球聯賽](../Page/超級橄欖球聯賽.md "wikilink")
  - [橄欖球冠軍錦標賽](../Page/橄欖球冠軍錦標賽.md "wikilink") - 紐西蘭、澳洲、南非 (Tri Nations
    Series)

### [網球](../Page/網球.md "wikilink")

  - [ATP 網球大師巡迴賽](../Page/ATP大師系列賽.md "wikilink")（ATP Masters Series）
  - [温布顿网球公开赛](../Page/温布顿网球公开赛.md "wikilink")
  - [澳洲网球公开赛](../Page/澳洲网球公开赛.md "wikilink")

### [摔角](../Page/摔角.md "wikilink")

  - [TNA iMPACT\!](../Page/TNA_iMPACT!.md "wikilink")

### [撞球](../Page/撞球.md "wikilink")

  - [亞洲9號球巡迴賽](../Page/亞洲9號球巡迴賽.md "wikilink")（衛視體育）
  - [世界盃花式桌球賽](../Page/世界盃花式桌球賽.md "wikilink")（ESPN）
  - [UPA美式桌球職業巡迴錦標賽](../Page/UPA美式桌球職業巡迴錦標賽.md "wikilink")（ESPN）

### [新聞](../Page/新聞.md "wikilink")

  - [世界體育中心](../Page/世界體育中心.md "wikilink")（ESPN）
  - [世界體育中心快報](../Page/世界體育中心快報.md "wikilink")（ESPN）
  - [世界體育中心(周末版)](../Page/世界體育中心\(周末版\).md "wikilink") (ESPN)
  - [加德士今日賽果](../Page/加德士今日賽果.md "wikilink")（衛視體育）
  - [吉列世界體壇](../Page/吉列世界體壇.md "wikilink")（衛視體育）

### 其他赛事

  - [奥运会](../Page/奥运会.md "wikilink")（仅台湾，部分转播权）
  - [亚运会](../Page/亚运会.md "wikilink")（仅台湾，部分转播权）

## TNA iMPACT\!

ESPN卫视体育台是[TNA
iMPACT\!亞洲地區的電視提供者](../Page/TNA_iMPACT!.md "wikilink")，一個由[Total
Nonstop Action
Wrestling製作的專業的摔角電視節目](../Page/Total_Nonstop_Action_Wrestling.md "wikilink")。
在2005年9月，3個TNA摔角手(the Indian American Sonjay Dutt, Shark Boy and Simon
Diamond) 在印度用了2個星期，他們去了很多的城市，宣傳即將在ESPN卫视体育台播放的iMPACT.
在9月28日，波帕爾發生了一場暴動，那時觀眾超過預期的數目，所以1000人被趕走，三個TNA摔跤手受傷。

## 参考资料

## 內部連結

  - [ESPN](../Page/ESPN.md "wikilink")
  - [衛視體育台](../Page/衛視體育台.md "wikilink")
  - [FOX體育台](../Page/FOX體育台.md "wikilink")：ESPN退出經營後時期的新頻道名稱。

## 外部連結

  - ESPN STAR Sports
      - ESPNSTAR中文網→ESPNSTAR.com.hk
      - ESPNSTAR.COM.TW
      - ESPNSTAR.COM.CN
      - MBC ESPN

[Category:国际媒体](../Category/国际媒体.md "wikilink")
[Category:體育電視台](../Category/體育電視台.md "wikilink")
[Category:ESPN](../Category/ESPN.md "wikilink")
[Category:1994年成立的公司](../Category/1994年成立的公司.md "wikilink")
[Category:2013年結業公司](../Category/2013年結業公司.md "wikilink")

1.  <http://money.163.com/12/0607/10/83D270OO002526O3.html>
    新闻集团买断迪士尼在ESPN Star Sports的股份