**国立[图卢兹应用科学学院](../Page/图卢兹.md "wikilink")**〔[INSA图卢兹](../Page/INSA.md "wikilink"),[法语](../Page/法语.md "wikilink"):INSA
DE
TOULOUSE〕是一所位于法国西南城市[图卢兹的工程师类](../Page/图卢兹.md "wikilink")[大学校](../Page/大学校.md "wikilink")，属于[法国国立应用科学学院](../Page/法国国立应用科学学院.md "wikilink")（[INSA](../Page/INSA.md "wikilink")）集团五所学校的一所。

## 历史

学校创立于1963年，属[图卢兹大学](../Page/图卢兹大学.md "wikilink")（Université de
Toulouse）联盟学校之一。国立图卢兹应用科学学院是一所工程师教育及科学研究为一体的[大学校](../Page/大学校.md "wikilink")。该校由法国高等教育部以及负责颁发[工程师文凭的](../Page/工程师文凭.md "wikilink")[法国工程师教育协会授权](../Page/法国工程师教育协会.md "wikilink")，负责工程师培养和科学研究工作。

在校学生2740名（其中2730名工程师学生, 23%为外国学生），教职员工450人。学校现已有12000名工程师毕业。

[INSA图卢兹共设有](../Page/INSA.md "wikilink")7个院系和2个教育中心，共十个专业。

## 课程设置

INSA图卢兹的工程师学位课程学制五年。2002年以前，学校采用传统的“2+3”学制，即前两年为理工科基础课阶段，此阶段内所有学生课程相同，不区分专业；后三年为工程师专业阶段，课程集中在学生所选择的专业领域。自2002年起，在欧洲高等教育一体化的框架内，学校进行了学制改革，开始采用“1+2+2”新学制，将理工科公共课阶段缩短为一年；以第二、三年为预备专业阶段，该阶段内学生根据个人意愿选择一个大概的专业方向，课程设置也因此而有所倾向；以第四、五年为专业课阶段，学生应当在第二阶段已选定的大专业方向内再次筛选，确定最终的专业。

## 专业设置

INSA图卢兹目前共有十个专业,七个院系：

  - **[生物化学系](../Page/生物化学.md "wikilink")**：
      - [生物化学专业](../Page/生物化学.md "wikilink");
  - **[土木工程系](../Page/土木工程.md "wikilink")**：
      - [土木工程专业](../Page/土木工程.md "wikilink")；
  - **[电子与](../Page/电子.md "wikilink")[信息工程系](../Page/信息工程.md "wikilink")**:
      - [自动化](../Page/自动化.md "wikilink")、[电子与](../Page/电子.md "wikilink")[计算机科学专业](../Page/计算机科学.md "wikilink")
        ;
      - [网络与](../Page/网络.md "wikilink")[通讯科学专业](../Page/通讯.md "wikilink")
        ;
      - [计算机科学专业](../Page/计算机科学.md "wikilink");
  - **[机械工程系](../Page/机械工程.md "wikilink")**：
      - [机械工程专业](../Page/机械工程.md "wikilink")
      - 系统工程专业
  - **[数学工程系](../Page/数学工程.md "wikilink")**：
      - 数学工程与[数学建模专业](../Page/数学建模.md "wikilink")；
  - **物理工程系**：
      - [物理工程专业](../Page/物理工程.md "wikilink")（即[材料工程与](../Page/材料工程.md "wikilink")[纳米技术专业](../Page/纳米技术.md "wikilink")）；
  - **过程控制与环境科学系**：
      - [工业过程与](../Page/工业过程.md "wikilink")[环境工程专业](../Page/环境工程.md "wikilink").

其中，系统工程专业在2006年仍隶属于机械工程系，然而作为一门集合了机械、电子、信息等多种学科的交叉性专业，其单独成系已只是一个时间的问题。该专业所有课程均为英文授课。

校园内[材料力学](../Page/材料力学.md "wikilink")、[生物芯片](../Page/生物芯片.md "wikilink")、[磁学](../Page/磁学.md "wikilink")、[光电子学](../Page/光电子学.md "wikilink")、[微观液体力学](../Page/微观液体力学.md "wikilink")、[数学建模等多领域研究室为各学系教学提供了支援](../Page/数学建模.md "wikilink")。

学校与企业关系紧密，与[空中客车](../Page/空中客车.md "wikilink")、[Assystem](../Page/Assystem.md "wikilink")、[Sogeclair等工业企业签订了合作协议](../Page/Sogeclair.md "wikilink")。

## 学校位置

INSA[图卢兹位于](../Page/图卢兹.md "wikilink") Rangueil
大学区内，与[图卢兹三大](../Page/图卢兹三大.md "wikilink")（Universite
Paul Sabatier Toulouse
III），[法国高等航空航天学院](../Page/法国高等航空航天学院.md "wikilink")（SUPAERO-ENSICA）毗邻，乘地铁B线，或23号公交线路INSA站可到达。夜间线路N2也将校区与市中心连为一体。

学校与市区间的交通非常便利。

## 外部链接

  - [INSA Toulouse](http://www.insa-toulouse.fr/)
  - [AIIT](http://aiit.insa-toulouse.fr/)
  - [LA
    LENTILLE](https://etud.insa-toulouse.fr/~contact/lentille/index.php)
  - [Amicale de l'INSA de
    Toulouse](http://etud.insa-toulouse.fr/~amicale/)

[category:法國大學](../Page/category:法國大學.md "wikilink")

[Category:1963年創建的教育機構](../Category/1963年創建的教育機構.md "wikilink")