**加拿大歷史**可追溯至大約33,000年前。當地的[原住民由](../Page/原住民.md "wikilink")[印第安人和](../Page/印第安人.md "wikilink")[因紐特人組成](../Page/因紐特人.md "wikilink")，大約在33,000年前移居到[北美洲](../Page/北美洲.md "wikilink")。[北歐的](../Page/北歐.md "wikilink")[斯堪地納維亞人](../Page/斯堪地納維亞.md "wikilink")（Viking）
在大約公元986年在現为[紐芬蘭的地方暫居](../Page/紐芬蘭.md "wikilink")。斯堪地納維亞人是第一批踏足北美的歐洲人。但是在16和17世紀才有歐洲人到北美作永久性定居。來北美定居的欧洲人主要來自[英國和](../Page/英國.md "wikilink")[法國](../Page/法國.md "wikilink")。

在1763年結束的[七年戰爭](../Page/七年戰爭.md "wikilink")，英法簽訂的[巴黎條約令法國放棄它們在北美殖民地的主權](../Page/巴黎條約.md "wikilink")。[美國革命戰爭](../Page/美國革命.md "wikilink")（1775年－1783年）以後，大規模的親英分子北上移居到[英屬北美](../Page/英屬北美.md "wikilink")。在1812年至1815年，[美國和英國在北美展開](../Page/美國.md "wikilink")[1812年戰爭](../Page/1812年戰爭.md "wikilink")。

在1867年，英國政府通過《[英屬北美法案](../Page/英屬北美法案.md "wikilink")》，在1867年7月1日將在北美的三個殖民地[合併成爲一個聯邦制的殖民地](../Page/加拿大联邦化.md "wikilink")——加拿大自治領。1867年，四個省份，[安大略](../Page/安大略.md "wikilink")、[魁北克](../Page/魁北克.md "wikilink")、[-{zh-hans:新斯科舍;zh-hk:諾華士高沙;zh-tw:諾瓦斯科西亞;}-](../Page/新斯科舍.md "wikilink")（新蘇格蘭）和[-{zh-hans:新不伦瑞克;zh-hk:紐賓士域;zh-tw:紐布朗斯維克;}-](../Page/紐不倫瑞克.md "wikilink")（其中安大略和魁北克已作爲一個殖民地管轄，所以四省是三個殖民地），同意加入聯邦政府，組成英帝國內的加拿大自治領。到1910年,加拿大已有9個省份（[紐芬蘭省是在](../Page/紐芬蘭.md "wikilink")1949年加入聯邦的）。1931年的《[西敏法令](../Page/1931年西敏法令.md "wikilink")》（**Statute
of
Westminster**）擴大加拿大政府自主權，包括外交。[英国议会在](../Page/英国议会.md "wikilink")1982年通過的《[加拿大法案](../Page/加拿大法案.md "wikilink")》把修改憲法的權力從英國议会移交給[加拿大议会](../Page/加拿大议会.md "wikilink")。

## 殖民前时期

### 原住民时期

按[考古發現](../Page/考古.md "wikilink")，加拿大[育空地區至少在西元](../Page/育空.md "wikilink")24,500年前，即有人類活動；而南[安大略的人類活動](../Page/安大略.md "wikilink")，則可追溯至西元前7,500年\[1\]
\[2\]。[舊克羅平地和](../Page/舊克羅平地.md "wikilink")[藍魚洞穴的](../Page/藍魚洞穴.md "wikilink")[印地安人遺跡](../Page/印地安人.md "wikilink")，是加拿大境內最古老的人類聚居點\[3\]\[4\]\[5\]。

[加拿大原住民的特點包括](../Page/加拿大原住民.md "wikilink")：成立永久居住點、從事農耕、複雜的社會等級，以及貿易網路等\[6\]\[7\]。一部分部落在[歐洲人抵達後已告瓦解](../Page/歐洲人.md "wikilink")，如今只能通過考古作研究\[8\]。據估計，加拿大原住民人口在15世紀晚期，約在20萬到200萬之間\[9\]。由於缺乏相應的[免疫系統](../Page/免疫系統.md "wikilink")，加拿大原住民被歐洲人帶來的[傳染病襲擊](../Page/傳染病.md "wikilink")，造成約40%-80%人口銳減\[10\]\[11\]\[12\]。

今日剩下的原住民，包括[第一民族](../Page/第一民族.md "wikilink")、[因紐特人](../Page/因紐特人.md "wikilink")、[梅蒂人](../Page/梅蒂人.md "wikilink")，其中梅蒂人是原住民和[法裔加拿大人的](../Page/法裔加拿大人.md "wikilink")[混血後代](../Page/混血.md "wikilink")。總的來說，因紐特人和歐洲殖民人的交流較少。

### 欧洲接触

[Benjamin_West_005.jpg](https://zh.wikipedia.org/wiki/File:Benjamin_West_005.jpg "fig:Benjamin_West_005.jpg")的名画沃夫将军之死，反映1759年在魁北克发生的[亚伯拉罕平原战役](../Page/亚伯拉罕平原战役.md "wikilink")\]\]
早於公元1000年左右，已有第一批歐洲人前來加拿大──[北歐](../Page/北歐.md "wikilink")[維京人領袖](../Page/維京人.md "wikilink")[萊弗·艾瑞克森](../Page/萊弗·艾瑞克森.md "wikilink")（[紅鬍子埃裡克之子](../Page/紅鬍子埃裡克.md "wikilink")）曾在加拿大[紐芬蘭一帶建立村莊](../Page/紐芬蘭.md "wikilink")\[13\]，但不久該村被淹沒在印第安人之中。此後很長一段時間，歐洲沒人與加拿大接觸，直到1497年，[義大利人](../Page/義大利人.md "wikilink")[喬瓦尼·卡博托為](../Page/喬瓦尼·卡博托.md "wikilink")[英格蘭探索](../Page/英格蘭.md "wikilink")[加拿大大西洋沿岸](../Page/加拿大大西洋省份.md "wikilink")\[14\]。16世紀早期，[巴斯克及](../Page/巴斯克.md "wikilink")[葡萄牙海員在大西洋沿岸](../Page/葡萄牙.md "wikilink")，建立季節性捕魚點。

## 法国统治时期（1534–1763年）

法国对于新世界的兴趣始于[弗朗索瓦一世](../Page/弗朗索瓦一世_\(法兰西\).md "wikilink")。他于1524年资助[乔瓦尼·达韦拉扎诺](../Page/乔瓦尼·达韦拉扎诺.md "wikilink")，去探索在佛罗里达至新探索地区来寻找一条通往太平洋的路线。\[15\]

1534年，[法國人雅各](../Page/法國人.md "wikilink")·卡蒂爾探索[聖羅倫斯河](../Page/聖羅倫斯河.md "wikilink")，並按法國國王之名，將這片土地命名「[弗朗索瓦一世](../Page/弗朗索瓦一世.md "wikilink")」\[16\]。1583年，英國在今日[紐芬蘭與拉布拉多的](../Page/紐芬蘭與拉布拉多.md "wikilink")[聖約翰斯建立定居點](../Page/聖約翰斯.md "wikilink")，並宣稱這是英國在北美的第一塊殖民地\[17\]。1603年，法國探險家[薩繆爾·德·尚普蘭在如今](../Page/薩繆爾·德·尚普蘭.md "wikilink")[新斯科舍省的](../Page/新斯科舍省.md "wikilink")[皇家港和](../Page/皇家港.md "wikilink")[魁北克省](../Page/魁北克省.md "wikilink")[魁北克市的地方建立北美第一個歐洲人的永久定居點](../Page/魁北克市.md "wikilink")\[18\]。在新法蘭西的領域內，[法裔加拿大人開始沿著](../Page/法裔加拿大人.md "wikilink")[聖羅倫斯河一帶擴張](../Page/聖羅倫斯河.md "wikilink")，[阿卡迪亞人開始在如今的大西洋省份定居](../Page/阿卡迪亞_\(加拿大\).md "wikilink")。與此同時，[傳教士和皮毛交易擴展到](../Page/傳教士.md "wikilink")[五大湖及](../Page/五大湖.md "wikilink")[法屬路易斯安那](../Page/法屬路易斯安那.md "wikilink")。為了爭奪皮毛交易的控制權，[阿爾岡金部落和](../Page/阿爾袞琴人.md "wikilink")[易洛魁部落分別在英國](../Page/易洛魁聯盟.md "wikilink")、荷蘭和法國的支持下發生[海狸戰爭](../Page/海狸戰爭.md "wikilink")。\[19\]

至18世纪初期，新法兰西殖民者已在沿着[圣劳伦斯河海岸以及新斯克尔特部分区域广泛建立据点](../Page/圣劳伦斯河.md "wikilink")。这些据点共有大约16000人。\[20\]然而在接下来数十年，来自法国的新移民停滞不前，这个导致在纽芬兰和斯科科舍和[十三殖民地的来自英格兰和苏格兰的人口比例大增](../Page/十三殖民地.md "wikilink")。至1750年代，法裔人口占比约为十个人中一个。\[21\]

## 英国统治时期（1763–1931年）

1713年，[西班牙王位繼承戰爭的](../Page/西班牙王位繼承戰爭.md "wikilink")[烏德勒支和約把如今](../Page/烏德勒支和約.md "wikilink")[新斯科舍的大陸部分割讓予英國](../Page/新斯科舍.md "wikilink")，而隨後的[七年戰爭的](../Page/七年戰爭.md "wikilink")[巴黎和約更是讓新法蘭西絕大部分領土都讓給英國](../Page/巴黎和約_\(1763年\).md "wikilink")，自此英國獲得如今加拿大東部的控制權\[22\]。

英國於1763頒佈的[公告將新法蘭西分離建立魁北克省](../Page/1763年公告.md "wikilink")，並將[佈雷頓角島合併到新斯科舍](../Page/佈雷頓角島.md "wikilink")。1769年，聖約翰斯島（即如今的[爱德华王子島](../Page/艾德華王子島.md "wikilink")）成為單獨的殖民地\[23\]。為了避免魁北克境內法裔的不滿，英國頒佈1774年魁北克法案，將魁北克地區延伸至五大湖和[俄亥俄河](../Page/俄亥俄河.md "wikilink")，並在魁北克地區重新確立[法語](../Page/法語.md "wikilink")、[天主教信仰和法國民法的地位](../Page/天主教.md "wikilink")。

1783年签署的[巴黎条约承认美国独立](../Page/巴黎条约_\(1783年\).md "wikilink")，并将五大湖南部区域割让予美国\[24\]。[新不伦瑞克从](../Page/新不伦瑞克.md "wikilink")[新斯科舍划分出来成立皇室置地](../Page/新斯科舍.md "wikilink")。为了安抚魁北克境内的的英裔保皇派，英国将魁北克分为讲英语的[上加拿大和讲法语的](../Page/上加拿大.md "wikilink")[下加拿大两部分](../Page/下加拿大.md "wikilink")，赋予两者各自的选举立法权，上下加拿大随后成为如今的[安大略和魁北克](../Page/安大略.md "wikilink")\[25\]。1812年，英国和美国[交战](../Page/1812年战争.md "wikilink")，加拿大为主要战场，战火一度燃烧到美国本土，但最后的和约又将边境恢复至战前状况。1812战争之后，大量[英国人和](../Page/英国人.md "wikilink")[爱尔兰人移民到加拿大](../Page/爱尔兰人.md "wikilink")。在1825年到1846年间，共有626,628欧洲移民合法进入加拿大，但是其中大约四分之一到三分之一的移民在1891年前死于传染病。

出於對責任政府的渴望，加拿大人掀起[1837年起義](../Page/1837年起義.md "wikilink")。為了平息叛亂和同化法裔，英國頒佈[1840年聯合法案](../Page/1840年聯合法案.md "wikilink")，重新將上下加拿大合併成立[加拿大省](../Page/加拿大省.md "wikilink")。1849年，代表所有[英屬北美的責任政府建立](../Page/英屬北美.md "wikilink")\[26\]。1846年，英美簽定[俄勒岡條約](../Page/俄勒岡條約.md "wikilink")，規定美國和英屬北美在西部的邊境以北緯49度為界。這一條約解決長久以來英屬北美和美國的邊境爭端，也為英國建立溫哥華島殖民地和英屬哥倫比亞殖民地鋪平道路\[27\]。

### 联邦的组成及扩张

[Canada_provinces_evolution_2.gif](https://zh.wikipedia.org/wiki/File:Canada_provinces_evolution_2.gif "fig:Canada_provinces_evolution_2.gif")
数次立宪会议之后，[1867年宪法法案于](../Page/1867年宪法法案.md "wikilink")1867年7月1日通过，[魁北克](../Page/魁北克.md "wikilink")、[安大略](../Page/安大略.md "wikilink")、[新不伦瑞克与](../Page/新不伦瑞克.md "wikilink")[新斯科舍四个英属北美殖民地组成加拿大联邦](../Page/新斯科舍.md "wikilink")\[28\]\[29\]\[30\]。宪法法案在[爱德华王子岛首府](../Page/爱德华王子岛.md "wikilink")[夏洛特顿签署](../Page/夏洛特顿.md "wikilink")，因此这座城市被认为是加拿大的诞生地，但是该省并没有在联邦成立之初加入。同时加拿大获得[罗伯特领地和](../Page/罗伯特领地.md "wikilink")[西北部地区的控制权](../Page/西北部地区.md "wikilink")，并将两者合并成[西北地区](../Page/西北地区_\(加拿大\).md "wikilink")。出于不满，[梅蒂人发起](../Page/梅蒂人.md "wikilink")[红河起义](../Page/红河起义.md "wikilink")，这一事件直接导致[马尼托巴省于](../Page/马尼托巴省.md "wikilink")1870年从[西北地区分离](../Page/西北地区.md "wikilink")。1871年，[不列颠哥伦比亚](../Page/不列颠哥伦比亚.md "wikilink")（不列颠哥伦比亚殖民地与温哥华岛殖民地于1866年合并）加入联邦；两年后，[爱德华王子岛也加入联邦](../Page/爱德华王子岛.md "wikilink")\[31\]。

为了促进西部的开发，加拿大政府资助三项横加铁路项目(包括[加拿大太平洋铁路](../Page/加拿大太平洋铁路.md "wikilink"))，在领地法案之下开启对[大平原的开发](../Page/大平原.md "wikilink")。为了维护这片新开发区域的治安，加拿大政府设立西北骑警警察局\[32\]\[33\]。1898年掀起的[克朗代克淘金热为加拿大西北领地带来大批移民](../Page/克朗代克淘金热.md "wikilink")，这一事件直接促进这片区域从西北领地分离并建立[育空地区](../Page/育空.md "wikilink")。在[加拿大自由党首相](../Page/加拿大自由党.md "wikilink")[威尔弗里德·劳雷尔的领导下](../Page/威尔弗里德·劳雷尔.md "wikilink")，欧洲大陆民族开始向大平原地区移民，因此促成[艾伯塔省和](../Page/艾伯塔省.md "wikilink")[萨斯喀彻温省的建立](../Page/萨斯喀彻温省.md "wikilink")。\[34\]

### 20世纪早期

由于联邦法案，作为自治领的加拿大仍被英国控制着外交事务。1914年，加拿大因为[英国的参战而自动对](../Page/英国.md "wikilink")[德宣战](../Page/德国.md "wikilink")，因而卷入[第一次世界大战](../Page/第一次世界大战.md "wikilink")。加拿大志愿兵被派往欧洲战场上的[西方战线](../Page/西方战线.md "wikilink")，随后这些志愿兵组成加拿大军。这支军队在[维米岭战役和其它主要战役中承担相当重要的任务](../Page/维米岭战役.md "wikilink")\[35\]。参与一战的650,000加拿大人中，约60,000人战死，173,000人负伤\[36\]。1917年，保守派首相[罗伯特·莱尔德·博登对法裔魁北克人实行强制征兵](../Page/罗伯特·莱尔德·博登.md "wikilink")，征兵危机爆发。1919年，加拿大独立于英国加入[国际联盟](../Page/国际联盟.md "wikilink")，随后的[1931年威斯敏斯特法案肯定加拿大的独立性](../Page/1931年威斯敏斯特法案.md "wikilink")，自此加拿大在国际事务上受英国的干预越来越少。20世纪30年代的[大萧条给加拿大经济带来沉重打击](../Page/大萧条.md "wikilink")。[二战期间](../Page/二战.md "wikilink")，在自由党首相[威廉·莱昂·麦肯齐·金的领导下](../Page/威廉·莱昂·麦肯齐·金.md "wikilink")，加拿大向[德国宣战](../Page/德国.md "wikilink")，第一支加拿大军队于1939年12月抵达英国。加拿大军队参与许多重要战役，包括[迪耶普战役](../Page/迪耶普战役.md "wikilink")、[入侵意大利](../Page/入侵意大利.md "wikilink")、[诺曼底登陆](../Page/诺曼底登陆.md "wikilink")、[霸王行动和](../Page/霸王行动.md "wikilink")[斯海尔德河战役等](../Page/斯海尔德河战役.md "wikilink")\[37\]。在[荷兰被德国占领期间](../Page/荷兰.md "wikilink")，加拿大为流亡的荷兰王室提供庇护\[38\]。在二战期间，加拿大为本国军队以及英国、[中国和](../Page/中国.md "wikilink")[苏联制造许多军事装备](../Page/苏联.md "wikilink")，使得加拿大的经济飞速发展\[39\]。

## 现代加拿大（1931年-至今）

[Flag_of_Canada_1921.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Canada_1921.svg "fig:Flag_of_Canada_1921.svg")
1949年，[纽芬兰自治领加入加拿大联邦](../Page/1948年紐芬蘭公民投票.md "wikilink")\[40\]。[二战之后](../Page/二战.md "wikilink")，加拿大经济快速增长，随之带来加拿大人更强烈的自我认同，标志事件为1967年[枫叶旗的采用](../Page/加拿大国旗.md "wikilink")\[41\]、1969年[双语政策的实施](../Page/双语政策.md "wikilink")\[42\]以及1971年多元文化机构的成立\[43\]。20世纪60年代，[魁北克掀起的](../Page/魁北克.md "wikilink")[寂静革命对该省的多方面产生深刻影响](../Page/寂静革命.md "wikilink")，主要表现在[魁北克人身份认同的转变及](../Page/魁北克人.md "wikilink")[天主教势力的没落](../Page/天主教.md "wikilink")。魁北克人身份认同的转变带来[魁北克人独立的诉求](../Page/魁北克独立运动.md "wikilink")。信仰马克思主义的极左翼恐怖组织[魁北克解放阵线在](../Page/魁北克解放阵线.md "wikilink")60年代策划多起恐怖袭击，给加拿大社会造成深远的伤痛。最严重的事件为[十月危机](../Page/十月危机.md "wikilink")，恐怖分子绑架并杀害时任劳工部长皮埃尔·拉波特\[44\]。1976年，主张[魁北克独立的](../Page/魁北克独立运动.md "wikilink")[魁北克人党赢得选举](../Page/魁北克人党.md "wikilink")，并分别在1980年及1995年就魁北克是否独立命题进行全民公投，但獨立派分别以40%及49.4％的得票率失败。随后，加拿大国会通过[清晰法案](../Page/清晰法案.md "wikilink")，规定在魁北克全民公决中所提出的問題足夠明確且得到明顯多數的支持時，魁北克可以與加拿大政府進行脫離聯邦的協商程序\[45\]\[46\]。由于1995年公投导致魁北克险些独立，很多原本将总部设在蒙特利尔的加拿大本土企业和跨国公司纷纷将总部迁往[安大略省](../Page/安大略省.md "wikilink")[多伦多](../Page/多伦多.md "wikilink")，导致魁北克经济大不如前。在时任加拿大总理的[皮埃尔·特鲁多的努力下](../Page/皮埃尔·特鲁多.md "wikilink")，1982年4月17日，英国国会通过加拿大法案，将加拿大宪法的修宪权由英国国会移交加拿大，至此加拿大完全脱离英国独立，但加拿大的国家元首依然为[伊丽莎白二世女王](../Page/伊丽莎白二世.md "wikilink")，且加拿大依然有代替女王行使权力的[加拿大总督](../Page/加拿大总督.md "wikilink")，只不过在加拿大宪法中不称其为英国女王，而称[加拿大女王](../Page/加拿大女王.md "wikilink")。新版1982年宪法法案也于修宪权移交后在加拿大国会通过，但至今未受到[魁北克省的承认](../Page/魁北克省.md "wikilink")。1999年，[努纳武特脱离西北地区](../Page/努纳武特.md "wikilink")，成为加拿大第三个地区。

## 参考来源

[Category:加拿大](../Category/加拿大.md "wikilink")
[Category:加拿大歷史](../Category/加拿大歷史.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.

9.
10.

11.

12.

13.

14.

15.

16.

17.

18.

19.

20.

21.

22.

23.

24.

25.

26.

27.

28.

29.

30.

31.

32.

33.

34.
35.

36.

37.
38.

39.
40.

41.

42.

43.

44.

45.

46.