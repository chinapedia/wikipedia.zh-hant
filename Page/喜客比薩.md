**喜客比薩**（）是[美國一家連鎖餐廳品牌](../Page/美國.md "wikilink")，主要提供比薩餅。集團現時共有400多間分店，其中63間分店設在美國，其餘則是亞洲分店。

## 歷史

**喜客比薩**由 Sherwood Johnson 及 愛迪龐馬
於1954年4月30日於[加州首府](../Page/加州.md "wikilink")[薩克拉門托成立](../Page/薩克拉門托.md "wikilink")。餐廳在第一週剛剛開業時，只有依靠售賣[啤酒來獲取利潤](../Page/啤酒.md "wikilink")。在接着的星期一，喜客利用[啤酒所賺取的利潤來購買](../Page/啤酒.md "wikilink")[比薩的原料](../Page/比薩.md "wikilink")。原本在[薩克拉門托的分店營業至](../Page/薩克拉門托.md "wikilink")90年代末便結業。該處的喜客比薩特別引入[迪克西蘭爵士樂來吸引食客](../Page/迪克西蘭爵士樂.md "wikilink")。喜客比薩逐漸在[薩克拉門托外變得著名](../Page/薩克拉門托.md "wikilink")，並不是比薩，只是從當地的電台網絡贊助[爵士樂節目才成名](../Page/爵士樂.md "wikilink")。Shakey
Johnson在[奧克拉荷馬州](../Page/奧克拉荷馬州.md "wikilink")[加斯里得到榮譽](../Page/加斯里.md "wikilink")，藉此表揚他長期在餐廳內演奏五弦琴。其他現場演奏音樂，包括鋼琴也是以前喜客比薩主要活動。

[HKNTPShakeyPizza_20070806.jpg](https://zh.wikipedia.org/wiki/File:HKNTPShakeyPizza_20070806.jpg "fig:HKNTPShakeyPizza_20070806.jpg")[新城市廣場分店](../Page/新城市廣場.md "wikilink")\]\]
[HKShakeysign1_20070806.jpg](https://zh.wikipedia.org/wiki/File:HKShakeysign1_20070806.jpg "fig:HKShakeysign1_20070806.jpg")

第二間喜客比薩餐廳分店在1956年於[奧勒岡州](../Page/奧勒岡州.md "wikilink")[波特蘭開業](../Page/波特蘭.md "wikilink")。餐廳在1957年開始採用[加盟連鎖方式開設更多分店](../Page/加盟連鎖.md "wikilink")。根據創辦人的資料，喜客比薩當時自己只做極少的市場研究，而基本選擇跟著去[Kinney
Shoes](../Page/Kinney_Shoes.md "wikilink")（即現今的[Foot
Locker廉價鞋連鎖店](../Page/Foot_Locker.md "wikilink")）開設分店的城市開業。到
1967年的時候 Johnson
出售股權，同時在[美國已有](../Page/美國.md "wikilink")272間喜客比薩分店。首間在[美國境外開設的加盟店在](../Page/美國.md "wikilink")1968年於[加拿大](../Page/加拿大.md "wikilink")[溫尼伯開業](../Page/溫尼伯.md "wikilink")。到了
1973年，公司擴展業務到[亞太地區](../Page/亞太地區.md "wikilink")，包括[日本](../Page/日本.md "wikilink")、[菲律賓及](../Page/菲律賓.md "wikilink")[馬來西亞](../Page/馬來西亞.md "wikilink")。在2007年8月，首次在[香港開設分店](../Page/香港.md "wikilink")。現今喜客比薩在[菲律賓的連鎖店更大於](../Page/菲律賓.md "wikilink")[美國](../Page/美國.md "wikilink")。

喜客比薩創辦人Shakey Johnson在1967年以三百萬美金出售其持有的公司一半的股權給[Colorado Milling and
Elevator](../Page/Colorado_Milling_and_Elevator.md "wikilink")，而該公司在次年以九百萬美金買下公司的另一半股權。喜客比薩於1974年再次出售至[Hunt
International
Resources](../Page/Hunt_International_Resources.md "wikilink")
。兩位特許經營人在1984年收購公司，之後在1989年賣給[新加坡的Inno](../Page/新加坡.md "wikilink")-Pacific
Holdings。在Inno-Pacific Holdings
管理喜客比薩時，大部份位於[美國的分店因此結業](../Page/美國.md "wikilink")。部份剩餘的特許經營人於2003年控訴
Inno-Pacific
。在開審前，喜客比薩已於2004年將公司出售給位於[加州](../Page/加州.md "wikilink")[阿罕布拉市的](../Page/阿罕布拉市.md "wikilink")
Jacmar Companies 。Jacmar 最後擁有喜客比薩其中19間分店特許經營權。

喜客比薩從1974年當初遍佈在[美國境內的](../Page/美國.md "wikilink")325分店，變成於2008年只有63間分店(當中有55間分店在[加州](../Page/加州.md "wikilink")，只有兩間分店在[密西西比河以東](../Page/密西西比河.md "wikilink")：包括位於[喬治亞州的](../Page/喬治亞州.md "wikilink")
Warner Robins
，[阿拉巴馬州的Auburn](../Page/阿拉巴馬州.md "wikilink")。[威斯康辛州的](../Page/威斯康辛州.md "wikilink")
West
Allis分店於2008年6月30日結業。此外，在美西的[加州以外只有三家分店](../Page/加州.md "wikilink")，包括位於[亞利桑那州的](../Page/亞利桑那州.md "wikilink")[诺加莱斯](../Page/诺加莱斯.md "wikilink")、及[华盛顿州](../Page/华盛顿州.md "wikilink")[西雅圖](../Page/西雅圖.md "wikilink")
近郊兩間。

## 海外分店

### 日本

[1973年](../Page/1973年.md "wikilink")，喜客披薩與[三菱商事](../Page/三菱商事.md "wikilink")、[麒麟麥酒](../Page/麒麟麥酒.md "wikilink")（現在是[麒麟](../Page/麒麟啤酒.md "wikilink")）等日本公司共同合資設立**日本シェーキーズ**株式會社，並在東京都港區赤阪成立日本一號店（目前是閉店狀態）\[1\]。截至2017年12月，日本國內還存在22家分店，主要分佈在東京都會區（7家）、關東近郊地區（6家）、沖繩縣4家，其他如岩手縣、靜岡縣、京都府、大阪府、福岡縣則各1家。

### 台灣

1980年代，台灣也曾經引進過喜客披薩，
當時以口味獨特的三大類披薩(每種口味有自己的編號，分別是厚披薩、薄脆披薩、鐵鍋披薩)為號召、加上特殊醃製的炸雞、新鮮厚切的現炸薯片、以及類似美式餐廳Friday's的活潑服務，在臺北市吉林路、金山南路、國父紀念館旁各有3家分店，曾掀起一股旋風。此後數年才陸續有[必勝客](../Page/必勝客.md "wikilink")、[達美樂](../Page/達美樂.md "wikilink")、[拿波里等外送品牌出現](../Page/拿波里.md "wikilink")。目前，喜客披薩已退出台灣，僅剩國父紀館旁光復南路上的蘇阿姨PIZZA更名後繼續經營，好味道依舊不變，生意興隆。

### 菲律賓

目前除美國之外的海外分店，尚有菲律賓\[2\]。現今喜客比薩在[菲律賓的連鎖店更大於](../Page/菲律賓.md "wikilink")[美國](../Page/美國.md "wikilink")。

### 香港

2007年8月，在[香港開設首家分店](../Page/香港.md "wikilink")。

## 餐廳對流行文化的重要性

  - 在第五季的*[衰仔樂園](../Page/衰仔樂園.md "wikilink")*中， **碌葛** the government
    puts a ban on [stem cell](../Page/stem_cell.md "wikilink") research;
    在同一時候，**蛋散** replicates 一份全新的喜客比薩，包含生物製幹細胞。喜客比薩也有在之後的新一輯發生同樣事件，包括
    "Up the Down Steroid" 及 "Stanley's Cup".
  - 在電影[咪走堂中](../Page/咪走堂.md "wikilink")，有一間奇妙餐廳的名稱命名為*Chez
    Quis*，發音被信以為"Shakey's"。
  - 在'電影*Wayne's World*'中，男主角[Rob
    Lowe在喜客比薩店內吃比薩](../Page/Rob_Lowe.md "wikilink")。
  - 在 Frank Zappa 的歌曲"The Blue Light"中，他熱情奔放出對喜客比薩中食客的感受。
  - [雷朋三世其中一集命名為](../Page/雷朋三世.md "wikilink") Shaky Pisa。

## 參考書目

  - Wilson, Burt. *Shakey & Me*. Sacramento, CA: Paloria Press, 2001年.
    ISBN 0-9676-5752-0

## 相關連結

  - [喜客比薩官方網頁](http://www.shakeys.com)
  - [Obituary](http://groups.google.com/groups?selm=363F4F3E.467D%40csus.edu&output=gplain)
    of Sherwood "Shakey" Johnson
  - [喜客比薩香港](http://www.shakeys-pizza.com/)
  - [喜客比薩菲律賓](http://www.shakeyspizza.ph/)
  - [喜客比薩日本 (R & K 餐飲企業，日文版)](http://shakeys.jp/)

[Category:美國餐厅](../Category/美國餐厅.md "wikilink")
[Category:美國食品公司](../Category/美國食品公司.md "wikilink")
[Category:總部在美國的跨國公司](../Category/總部在美國的跨國公司.md "wikilink")
[Category:美國連鎖餐廳](../Category/美國連鎖餐廳.md "wikilink")
[Category:日本連鎖餐廳](../Category/日本連鎖餐廳.md "wikilink")
[Category:菲律賓連鎖餐廳](../Category/菲律賓連鎖餐廳.md "wikilink")
[Category:1954年成立的公司](../Category/1954年成立的公司.md "wikilink")

1.  [HISTORY](http://shakeys.jp/history/) シェーキーズ公式サイト

2.