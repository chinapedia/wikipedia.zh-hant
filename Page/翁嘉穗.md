**翁嘉穗**（Virginia
Yung，），掛[夫姓作](../Page/夫姓.md "wikilink")**鄔翁嘉穗**，前香港藝人，2018/19年度[東華三院總理](../Page/東華三院.md "wikilink")\[1\]、[香港海洋公園保育基金籌委會委員](../Page/香港海洋公園.md "wikilink")\[2\]、[慧妍雅集會員](../Page/慧妍雅集.md "wikilink")\[3\]。

翁嘉穗在1997年以[溫哥華海外佳麗身份參選](../Page/溫哥華.md "wikilink")[香港小姐競選](../Page/香港小姐競選.md "wikilink")，參選期間曾獲得電視台高層[何麗全出口力讚](../Page/何麗全.md "wikilink")。最終翁嘉穗贏得第25屆香港小姐冠軍，同時獲得「最上鏡小姐」、「東方美態大獎」、「都會魅力小姐」。1999年1月5日在[美國](../Page/美國.md "wikilink")[拉斯維加斯嫁給比她年長](../Page/拉斯維加斯.md "wikilink")20年的[七海化工集團有限公司主席](../Page/七海化工集團有限公司.md "wikilink")[鄔友正為妻](../Page/鄔友正.md "wikilink")，現時育有兩子。前男友為2000年度香港小姐冠軍[劉慧蘊的老公](../Page/劉慧蘊.md "wikilink")[林孝基](../Page/林孝基.md "wikilink")。

## 參與節目

  - [超級無敵獎門人之再戰江湖](../Page/超級無敵獎門人之再戰江湖.md "wikilink")

## 参考文献

[Category:前無綫電視女藝員](../Category/前無綫電視女藝員.md "wikilink")
[Category:1997年度香港小姐競選參賽者](../Category/1997年度香港小姐競選參賽者.md "wikilink")
[Category:協恩中學校友](../Category/協恩中學校友.md "wikilink")
[Category:不列顛哥倫比亞大學校友](../Category/不列顛哥倫比亞大學校友.md "wikilink")
[Ka](../Category/翁姓.md "wikilink")

1.  [鄔翁嘉穗總理](http://www.tungwah.org.hk/wp-content/uploads/2018/03/14.pdf)

2.
3.