**ISO 3166-2:2002-05-21**是ISO第二號更新通訊，於2002年5月21日發佈。

這次宣告[ISO 3166-2更新下列國家或地區的主要行政區](../Page/ISO_3166-2.md "wikilink")：

|                                           |                                            |
| ----------------------------------------- | ------------------------------------------ |
| [AE](../Page/ISO_3166-2:AE.md "wikilink") | [阿拉伯聯合大公國](../Page/阿拉伯聯合大公國.md "wikilink") |
| [AL](../Page/ISO_3166-2:AL.md "wikilink") | [阿爾巴尼亞](../Page/阿爾巴尼亞.md "wikilink")       |
| [AO](../Page/ISO_3166-2:AO.md "wikilink") | [安哥拉](../Page/安哥拉.md "wikilink")           |
| [AZ](../Page/ISO_3166-2:AZ.md "wikilink") | [亞塞拜然](../Page/亞塞拜然.md "wikilink")         |
| [BD](../Page/ISO_3166-2:BD.md "wikilink") | [孟加拉](../Page/孟加拉.md "wikilink")           |
| [BG](../Page/ISO_3166-2:BG.md "wikilink") | [保加利亞](../Page/保加利亞.md "wikilink")         |
| [BJ](../Page/ISO_3166-2:BJ.md "wikilink") | [貝南](../Page/貝南.md "wikilink")             |
| [CA](../Page/ISO_3166-2:CA.md "wikilink") | [加拿大](../Page/加拿大.md "wikilink")           |
| [CD](../Page/ISO_3166-2:CD.md "wikilink") | [剛果](../Page/剛果.md "wikilink")             |
| [CN](../Page/ISO_3166-2:CN.md "wikilink") | [中國](../Page/中國.md "wikilink")             |
| [CV](../Page/ISO_3166-2:CV.md "wikilink") | [維德角](../Page/維德角.md "wikilink")           |
| [CZ](../Page/ISO_3166-2:CZ.md "wikilink") | [捷克](../Page/捷克.md "wikilink")             |
| [ES](../Page/ISO_3166-2:ES.md "wikilink") | [西班牙](../Page/西班牙.md "wikilink")           |
| [FR](../Page/ISO_3166-2:FR.md "wikilink") | [法國](../Page/法國.md "wikilink")             |
| [GB](../Page/ISO_3166-2:GB.md "wikilink") | [英國](../Page/英國.md "wikilink")             |
| [GE](../Page/ISO_3166-2:GE.md "wikilink") | [喬治亞](../Page/喬治亞.md "wikilink")           |
| [GN](../Page/ISO_3166-2:GN.md "wikilink") | [幾內亞](../Page/幾內亞.md "wikilink")           |
| [GT](../Page/ISO_3166-2:GT.md "wikilink") | [瓜地馬拉](../Page/瓜地馬拉.md "wikilink")         |
| [HR](../Page/ISO_3166-2:HR.md "wikilink") | [克羅埃西亞](../Page/克羅埃西亞.md "wikilink")       |
| [ID](../Page/ISO_3166-2:ID.md "wikilink") | [印尼](../Page/印尼.md "wikilink")             |
| [IN](../Page/ISO_3166-2:IN.md "wikilink") | [印度](../Page/印度.md "wikilink")             |
| [IR](../Page/ISO_3166-2:IR.md "wikilink") | [伊朗](../Page/伊朗.md "wikilink")             |
| [KZ](../Page/ISO_3166-2:KZ.md "wikilink") | [哈薩克](../Page/哈薩克.md "wikilink")           |
| [LA](../Page/ISO_3166-2:LA.md "wikilink") | [寮國](../Page/寮國.md "wikilink")             |
| [MA](../Page/ISO_3166-2:MA.md "wikilink") | [摩洛哥](../Page/摩洛哥.md "wikilink")           |
| [MD](../Page/ISO_3166-2:MD.md "wikilink") | [摩爾多瓦](../Page/摩爾多瓦.md "wikilink")         |
| [MW](../Page/ISO_3166-2:MW.md "wikilink") | [馬拉威](../Page/馬拉威.md "wikilink")           |
| [NI](../Page/ISO_3166-2:NI.md "wikilink") | [尼加拉瓜](../Page/尼加拉瓜.md "wikilink")         |
| [PH](../Page/ISO_3166-2:PH.md "wikilink") | [菲律賓](../Page/菲律賓.md "wikilink")           |
| [TR](../Page/ISO_3166-2:TR.md "wikilink") | [土耳其](../Page/土耳其.md "wikilink")           |
| [UZ](../Page/ISO_3166-2:UZ.md "wikilink") | [烏茲別克](../Page/烏茲別克.md "wikilink")         |
| [VN](../Page/ISO_3166-2:VN.md "wikilink") | [越南](../Page/越南.md "wikilink")             |

## 外部連結

  - [複本下載](http://www.iso.org/iso/en/prods-services/iso3166ma/03updates-on-iso-3166/nli-2.pdf)（ISO網站）

[\#03166-2-2002-05-21](../Category/ISO_3166.md "wikilink")