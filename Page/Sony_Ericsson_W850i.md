**Sony Ericsson
W850i**為[索尼愛立信於](../Page/索尼愛立信.md "wikilink")2006年10月推出的第二部[Walkman](../Page/Walkman.md "wikilink")[3G音樂手提電話](../Page/3G.md "wikilink")，設有黑白兩種顏色。此外更備有200萬像素相機功能。長方形搭配四周圓弧設計，看起來簡單有形。此機種也是索愛第一款滑蓋式手機。

## 特色

W850i 內建最新版本的[Walkman](../Page/Walkman.md "wikilink") player
2.0音樂播放器，將手機音樂的檔案類型、播放清單、個別歌曲或音樂專輯的搜尋工作簡化不少。此外，新增的圖型化功能讓玩家體驗
W850i 更趨直覺化，同時也提供了完整的音樂曲目詳細資料與專輯封面影像。

## 內容

W850i有「動感音樂閃燈」，可隨著音樂節奏的變化點亮控制鍵。W850i支援空中音樂下載服務
(OTA)，可播放[MP3](../Page/MP3.md "wikilink")、[AAC](../Page/AAC.md "wikilink")、[AAC+](../Page/AAC+.md "wikilink")、[eAAC+](../Page/eAAC+.md "wikilink")、[MPEG4格式檔案](../Page/MPEG4.md "wikilink")，而且還隨機附贈1GB
[MS Pro Duo記憶卡](../Page/MS_Pro_Duo.md "wikilink")，相當於可收藏
1000首[eAAC+檔案的音樂](../Page/eAAC+.md "wikilink")。別以為只有如此，W850i
最高可擴充達4GB的容量，充分享受聆聽音樂的愉悅。

全新[TrackID服務可讓玩家透過麥克風或手機內建的](../Page/TrackID.md "wikilink") FM
收音機錄製歌曲。更方便的是，W850i只要一個步驟，就可傳送檔案到Gracenote的音樂庫，而Gracenote會執行自動識別，然後再將曲目資訊傳回發送訊息的W850i。

## 參考條目

  - [索尼愛立信](../Page/索尼愛立信.md "wikilink")

## 參考連結

[W850i資料](https://web.archive.org/web/20060901180650/http://www.sonyericsson.com/spg.jsp?cc=hk&lc=zh&ver=4000&template=pp1_loader&php=php1_10447&zone=pp&lm=pp1&pid=10447)

[W850i](../Category/索尼愛立信手機.md "wikilink")
[Category:2006年面世的手機](../Category/2006年面世的手機.md "wikilink")