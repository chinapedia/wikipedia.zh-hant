**馬森**，[華人戲劇家與](../Page/華人.md "wikilink")[小說家](../Page/小說家.md "wikilink")。1932年10月3日生於[山東省](../Page/山東省.md "wikilink")[齊河縣](../Page/齊河縣.md "wikilink")。父親馬超群，母親孫希然。少年時期曾於[濟南](../Page/濟南.md "wikilink")、[北京](../Page/北京.md "wikilink")、[淡水](../Page/淡水區.md "wikilink")、[宜蘭等地就讀中學](../Page/宜蘭市.md "wikilink")。畢業於[國立台灣師範大學國文系及國文研究所](../Page/國立台灣師範大學.md "wikilink")，1961年赴[法國巴黎電影高級研究院](../Page/法國.md "wikilink")（Institut
des hautes études
cinématographiques）研究[電影與](../Page/電影.md "wikilink")[戲劇](../Page/戲劇.md "wikilink")，並在[巴黎大學漢學院博士班肄業](../Page/巴黎大學.md "wikilink")。1965年在巴黎創辦、主編《歐洲雜誌》。然後繼續赴[加拿大研究](../Page/加拿大.md "wikilink")[社會學](../Page/社會學.md "wikilink")，獲[不列顛哥倫比亞大學](../Page/不列顛哥倫比亞大學.md "wikilink")（University
of British
Columbia）[哲學](../Page/哲學.md "wikilink")[博士學位](../Page/博士.md "wikilink")。現旅居[加拿大](../Page/加拿大.md "wikilink")。

## 參考資料

<references/>

## 外部連結

  - [馬森老師 佛光大學](http://www.fgu.edu.tw/~literary/MaSen.htm)
  - [馬森的部落格](http://wwwmasen.blogspot.com/)

[M](../Category/台灣小說家.md "wikilink")
[M](../Category/台灣劇作家.md "wikilink")
[M](../Category/國立臺灣師範大學校友.md "wikilink")
[M](../Category/不列顛哥倫比亞大學校友.md "wikilink")
[M](../Category/國立臺灣師範大學教授.md "wikilink")
[M](../Category/佛光大學教授.md "wikilink")
[M](../Category/國立成功大學教授.md "wikilink")
[M](../Category/齐河人.md "wikilink") [S森](../Category/馬姓.md "wikilink")