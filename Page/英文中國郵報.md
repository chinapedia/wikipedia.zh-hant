《**The China
Post**》，中文名稱為《**英文中國郵報**》，是在[台灣發行的英文報紙](../Page/台灣.md "wikilink")，公司為**中國郵報社股份有限公司**，由[黃遹霈](../Page/黃遹霈.md "wikilink")、[余夢燕夫婦創辦於](../Page/余夢燕.md "wikilink")1952年9月3日。並交由第二代黃致祥接任，2016年再轉由第三任董事長與發行人為前立委[謝國樑](../Page/謝國樑.md "wikilink")。是台灣首家以報紙型態發行的英文報紙\[1\]。

該報2016年6月移交[華聯國際多媒體股份有限公司為旗下子公司](../Page/華聯國際.md "wikilink")。謝國樑於入主該報一年一個月後，於2017年4月15日宣布該報將於5月15日停止紙本發行，只保留網路版。內部人力也將相應調整，但無法對外確認調整幅度。2017年10月1日華聯國際正式將英文中國郵報移轉至網路媒體[今日新聞網](../Page/今日新聞網.md "wikilink")（NOWNEWS），多以轉載美聯社（AP）與中央社（CNA）等通訊社所發出稿件，轉型經營英文新聞頻道網站。

## 特殊紀錄

金氏世界紀錄博物館 於 民國八十五年九月一日 發給世界紀錄證明書 認證項目: 英文中國郵報為台灣第一份英文早報 認證內容 :
民國41年9月3日創刊 創辦人是一對記者夫婦 -- 黃遹霈.余夢燕

## 業務範圍

除日報以外，還有海外航空版發行至歐、美、亞洲共十餘國，並有《中英雙語週報》（Student Post，雙語學生郵報），是大學入學考試可參考刊物。

自1997年起代理《[亞洲華爾街日報](../Page/亞洲華爾街日報.md "wikilink")》，並在2008年成為（ANN）會員，是ANN唯一台灣新聞媒體代表，將台灣的新聞傳播至全[亞洲](../Page/亞洲.md "wikilink")17個會員國家。

## 參見

  - [台灣報紙列表](../Page/台灣報紙列表.md "wikilink")

## 註釋

## 參考資料

## 外部連結

  - [1](http://chinapost.nownews.com)
  - [China Post Facebook](https://www.facebook.com/Chinapost/)
  - [英文中國郵報
    將停止發行紙本](http://www.cna.com.tw/news/firstnews/201704165006-1.aspx/)
  - [今日新聞+英文中國郵報
    開啟台灣媒體不凡的一頁](https://www.nownews.com/news/20171010/2622467/)

[Category:台灣報紙](../Category/台灣報紙.md "wikilink")
[Category:英文報紙](../Category/英文報紙.md "wikilink")
[Category:日报](../Category/日报.md "wikilink")
[Category:1952年建立的出版物](../Category/1952年建立的出版物.md "wikilink")
[Category:1952年台灣建立](../Category/1952年台灣建立.md "wikilink")

1.