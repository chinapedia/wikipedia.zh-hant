<table>
<tbody>
<tr class="odd">
<td></td>
</tr>
</tbody>
</table>

**愛爾溫**（**Elwing**），是[英國作家](../Page/英國.md "wikilink")[約翰·羅納德·鲁埃爾·托爾金](../Page/約翰·羅納德·鲁埃爾·托爾金.md "wikilink")（J.R.R.
Tolkien）的史詩式奇幻小說《[精靈寶鑽](../Page/精靈寶鑽.md "wikilink")》中的人物。

她是半精靈，埃蘭迪爾之妻、[愛隆](../Page/愛隆.md "wikilink")（Elrond）與愛洛斯的母親，持有[精靈寶鑽](../Page/精靈寶鑽_\(寶物\).md "wikilink")（Silmaril），並屬於[登丹人](../Page/登丹人.md "wikilink")（Dúnedain）皇族的祖先。

## 名字語源

**愛爾溫**（Elwing）一名是[辛達林語](../Page/辛達林語.md "wikilink")，意思是「星光四射」*Star
spray*，源自她出生的夜晚，星光照射在她家旁的泉水，散發燦爛光芒。\[1\]

她又被稱為「白羽愛爾溫」（Elwing the White），可能因為她有化身白色大海鳥的能力。

## 總覽

愛爾溫是一名[半精靈](../Page/半精靈.md "wikilink")（half-elven），不過半精靈這稱號最常用於她的兩個兒子身上。

她的父親是半精靈、貝倫與露西安的獨子[迪歐·埃盧希爾](../Page/迪歐·埃盧希爾.md "wikilink")（Dior
Eluchíl），母親是[辛達族精靈](../Page/辛達族.md "wikilink")、凱勒鵬的親屬[寧羅絲](../Page/寧羅絲.md "wikilink")（Nimloth）。\[2\]她的兄弟是雙胞胎[埃盧瑞](../Page/埃盧瑞.md "wikilink")（Eluréd）及[埃盧林](../Page/埃盧林.md "wikilink")（Elurín）。\[3\]她的丈夫是航海家[埃蘭迪爾](../Page/埃蘭迪爾.md "wikilink")（Eärendil），誕下了雙胞胎兒子[愛洛斯](../Page/愛洛斯.md "wikilink")（Elros）及愛隆。\[4\]

愛爾溫擁有變形的能力，這是她在逃離第三次親族殘殺後獲得的，因為[烏歐牟](../Page/烏歐牟.md "wikilink")（Ulmo）將她變成一隻白色的大海鳥，\[5\]讓她避過淹死大海的結局。

## 生平

### 早年

[第一紀元](../Page/第一紀元.md "wikilink")503年，\[6\]愛爾溫出生於[歐西瑞安](../Page/歐西瑞安.md "wikilink")（Ossiriand）的藍希爾·拉瑪斯（Lanthir
Lamath）瀑布。

她童年時，其父迪歐帶著家人前去[多瑞亞斯](../Page/多瑞亞斯.md "wikilink")（Doriath）繼承外祖父[埃盧·庭葛](../Page/埃盧·庭葛.md "wikilink")（Elu
Thingol）的王權。\[7\]後來費諾眾子發動攻擊，要奪回[精靈寶鑽](../Page/精靈寶鑽_\(寶物\).md "wikilink")，殺了愛爾溫的所有家人，是為[第二次親族殘殺](../Page/第二次親族殘殺.md "wikilink")（Second
Kinslaying）。\[8\]

愛爾溫隻身倖免，一小群灰精靈保護她和[精靈寶鑽逃到](../Page/精靈寶鑽_\(寶物\).md "wikilink")[西瑞安河口](../Page/西瑞安河口.md "wikilink")（Mouth
of
Sirion）。\[9\]愛爾溫長大成人，美貌聞名，並繼續保有[精靈寶鑽](../Page/精靈寶鑽_\(寶物\).md "wikilink")。後來埃蘭迪爾與她結婚，誕下雙胞胎兒子愛洛斯和愛隆。在埃蘭迪爾四處遠航時，她就留在西瑞安河口港等待他的歸來。

### 第三次親族殘殺

費諾長子[梅斯羅斯](../Page/梅斯羅斯.md "wikilink")（Maedhros）得知[精靈寶鑽的位置](../Page/精靈寶鑽_\(寶物\).md "wikilink")，但他卻因後悔而節制自己。\[10\]不過之後由於他們立下的誓言驅使，他派使禮貌地索回[精靈寶鑽](../Page/精靈寶鑽_\(寶物\).md "wikilink")。愛爾溫拒絕，因為那顆[精靈寶鑽是她祖父母](../Page/精靈寶鑽_\(寶物\).md "wikilink")[貝倫](../Page/貝倫.md "wikilink")（Beren）與[露西安](../Page/露西安.md "wikilink")（Lúthien）嬴得，加上她的父親迪歐因寶石命喪，另外更不該是費諾眾子在埃蘭迪爾遠航時來搶奪[精靈寶鑽](../Page/精靈寶鑽_\(寶物\).md "wikilink")。於是梅斯羅斯遂發起攻擊，為[第三次親族殘殺](../Page/第三次親族殘殺.md "wikilink")（Third
Kinslaying）。\[11\]河口居民打了敗仗，他們當中許多人被殺，愛爾溫帶著[精靈寶鑽投海](../Page/精靈寶鑽_\(寶物\).md "wikilink")，她的兒子都被擄獲。\[12\]

愛爾溫逃過殞命的結局，維拉烏歐牟將她化為一隻白色的大海鳥，[精靈寶鑽鑲嵌在她胸口中](../Page/精靈寶鑽_\(寶物\).md "wikilink")。\[13\]她飛快地衝向埃蘭迪爾，跌落在[威基洛特](../Page/威基洛特.md "wikilink")（Vingilot）的甲板上，幾乎斷氣。第二天早晨，愛爾溫回復原狀，並告知他發生的一切。悲傷的二人放棄回頭，前去[維林諾](../Page/維林諾.md "wikilink")（Valinor）。因著她對埃蘭迪爾的愛，她踏上阿門洲，不怕可能引來[維拉](../Page/維拉.md "wikilink")（Valar）的憤怒，自此她和埃蘭迪爾就沒有回到中土大陸。

### 航向維林諾

埃蘭迪爾離開後，愛爾溫就沿海岸線走到[帖勒瑞族](../Page/帖勒瑞族.md "wikilink")（Teleri）的[澳闊隆迪](../Page/澳闊隆迪.md "wikilink")（Alqualondë），遇到她的帖勒瑞親族。她講述許多貝爾蘭的故事給他們知道，\[14\]不久埃蘭迪爾就匯合她去等候維拉的判決。維拉們給予愛爾溫、埃蘭迪爾及他們後裔選擇成為精靈或人類的權利。想到露西安的命運，她就決定成為精靈。\[15\]　

埃蘭迪爾也跟從她的選擇，並駕著威基洛特與[精靈寶鑽翱翔空中](../Page/精靈寶鑽_\(寶物\).md "wikilink")，愛爾溫也是如往昔一樣等待他回來。[貝烈蓋爾海](../Page/貝烈蓋爾海.md "wikilink")（Belegaer）岸邊有座屬於她的高塔，海鳥都在那裡築巢，愛爾溫從牠們身上學懂飛翔的本領和各種鳥語。\[16\]有時他遠航回歸時，她會化身成為白鳥，飛去迎接埃蘭迪爾。在[憤怒之戰](../Page/憤怒之戰.md "wikilink")（War
of
Wrath）爆發前，她說服了帖勒瑞族駕駛白船乘載西方大軍前往[中土大陸](../Page/中土大陸.md "wikilink")。\[17\]

她應該從此永遠居住在[阿門洲](../Page/阿門洲.md "wikilink")（Aman），沒有返回過中土大陸。

## 半精靈家系

## 參考

  - 《[精靈寶鑽](../Page/精靈寶鑽.md "wikilink")》
  - 《[中土世界的歷史](../Page/中土世界的歷史.md "wikilink")》
      - 《[珠寶之戰](../Page/珠寶之戰.md "wikilink")》

## 資料來源

[de:Figuren in Tolkiens Welt\#Earendil und
Elwing](../Page/de:Figuren_in_Tolkiens_Welt#Earendil_und_Elwing.md "wikilink")
[pl:Półelf
(Śródziemie)\#Elwinga](../Page/pl:Półelf_\(Śródziemie\)#Elwinga.md "wikilink")

[Category:中土大陸的角色](../Category/中土大陸的角色.md "wikilink")
[Category:中土大陸的精靈](../Category/中土大陸的精靈.md "wikilink")
[Category:精靈寶鑽中的人物](../Category/精靈寶鑽中的人物.md "wikilink")

1.  《精靈寶鑽》 2002年 聯經初版翻譯 第二十二章《多瑞亞斯的毀滅》

2.
3.
4.  《精靈寶鑽》 2002年 聯經初版翻譯 第二十四章《埃蘭迪爾的遠航與憤怒之戰》

5.
6.
7.
8.
9.
10.
11.
12.
13.
14.
15.
16.
17.