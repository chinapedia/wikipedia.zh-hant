<div style="text-align: center;">

<div style="font-size: larger;">

![Nuvola_apps_bookcase.svg](Nuvola_apps_bookcase.svg
"Nuvola_apps_bookcase.svg") **[主題首頁](Portal:首頁.md "wikilink")**

</div>

<div style="font-size: smaller;">

目前中文維基百科共有項主題首頁。

</div>

</div>

<div>

<div style="float: left; width: 49%;">

<div style="text-align: center; background-color: gainsboro;">

![Nuvola_apps_edu_miscellaneous.svg](Nuvola_apps_edu_miscellaneous.svg
"Nuvola_apps_edu_miscellaneous.svg")
**[藝術與](Portal:藝術.md "wikilink")[文化](Portal:文化.md "wikilink")**

</div>

<div style="font-size: 95%; margin: 0.5em 1em 0.5em 1em;">

</div>

<div style="text-align: center; background-color: gainsboro;">

![Sports_icon.png](Sports_icon.png "Sports_icon.png")
**娛樂與[體育](Portal:體育.md "wikilink")**

</div>

<div style="font-size: 95%; margin: 0.5em 1em 0.5em 1em;">

</div>

<div style="text-align: center; background-color: gainsboro;">

![Nuvola_apps_edu_science.svg](Nuvola_apps_edu_science.svg
"Nuvola_apps_edu_science.svg") **[科學](Portal:科學.md "wikilink")**

</div>

<div style="font-size: 95%; margin: 0.5em 1em 0.5em 1em;">

</div>

<div style="text-align: center; background-color: gainsboro;">

![Nuvola_apps_kweather.svg](Nuvola_apps_kweather.svg
"Nuvola_apps_kweather.svg") **[氣象](Portal:氣象.md "wikilink")**

</div>

<div style="font-size: 95%; margin: 0.5em 1em 0.5em 1em;">

</div>

<div style="text-align: center; background-color: gainsboro;">

![Nuvola_apps_kcmsystem.svg](Nuvola_apps_kcmsystem.svg
"Nuvola_apps_kcmsystem.svg")
**[技術](Portal:技術.md "wikilink")**与**[工程](Portal:工程.md "wikilink")**

</div>

<div style="font-size: 95%; margin: 0.5em 1em 0.5em 1em;">

</div>

<div style="text-align: center; background-color: gainsboro;">

![Sinnbild_Radfahrer,_StVO_1992.svg](Sinnbild_Radfahrer,_StVO_1992.svg
"Sinnbild_Radfahrer,_StVO_1992.svg") **交通**

</div>

<div style="font-size: 95%; margin: 0.5em 1em 0.5em 1em;">

</div>

<div style="text-align: center; background-color: gainsboro;">

![Nuvola_apps_kuser.svg](Nuvola_apps_kuser.svg
"Nuvola_apps_kuser.svg") **[社會科學](Portal:社會科學.md "wikilink")**

</div>

<div style="font-size: 95%; margin: 0.5em 1em 0.5em 1em;">

</div>

</div>

<div style="float: right; width: 49%;">

<div style="text-align: center; background-color: gainsboro;">

![Bluetank.png](Bluetank.png "Bluetank.png")
**[军事及战争](Portal:军事.md "wikilink")**

</div>

<div style="font-size: 95%; margin: 0.5em 1em 0.5em 1em;">

</div>

<div style="text-align: center; background-color: gainsboro;">

![Monobook_icon.svg](Monobook_icon.svg "Monobook_icon.svg") **人文**

</div>

<div style="font-size: 95%; margin: 0.5em 1em 0.5em 1em;">

</div>

<div style="text-align: center; background-color: gainsboro;">

![P_religion_world.svg](P_religion_world.svg "P_religion_world.svg")
**[宗教](Portal:宗教.md "wikilink")**

</div>

<div style="font-size: 95%; margin: 0.5em 1em 0.5em 1em;">

</div>

<div style="text-align: center; background-color: gainsboro;">

![Nuvola_apps_kworldclock.svg](Nuvola_apps_kworldclock.svg
"Nuvola_apps_kworldclock.svg") **[歷史](Portal:歷史.md "wikilink")**

</div>

<div style="font-size: 95%; margin: 0.5em 1em 0.5em 1em;">

</div>

<div style="text-align: center; background-color: gainsboro;">

![Gnome-globe.svg](Gnome-globe.svg "Gnome-globe.svg") **地理**

</div>

<div style="font-size: 95%; margin: 0.5em 1em 0.5em 1em;">

  -
  - 亚洲

<!-- end list -->

  -

      -

<!-- end list -->

  - [歐洲](Portal:歐洲.md "wikilink")

<!-- end list -->

  -

      -

<!-- end list -->

  - 美洲

<!-- end list -->

  -

      -

<!-- end list -->

  - 大洋洲

<!-- end list -->

  -

      -

<!-- end list -->

  - [非洲](Portal:非洲.md "wikilink")

<!-- end list -->

  -

      -

</div>

</div>

</div>

<noinclude>  </noinclude>