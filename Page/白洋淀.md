**白洋淀**位于中国[河北省](../Page/河北省.md "wikilink")[雄安新区](../Page/雄安新区.md "wikilink")[安新县](../Page/安新县.md "wikilink")、[雄县和](../Page/雄县.md "wikilink")[容城县之间](../Page/容城县.md "wikilink")，是[华北最大的](../Page/华北.md "wikilink")[淡水湖](../Page/淡水湖.md "wikilink")，属于[华北平原北部](../Page/华北平原.md "wikilink")[海河流域](../Page/海河.md "wikilink")。

## 概述

白洋淀汇集了上游自[太行山麓发源的](../Page/太行山.md "wikilink")9条河流之水，形成一片由3700多条沟渠、河道连接的146个大小湖泊群，总面积有366平方千米，湖群中岛屿和湖畔分布有36个村庄。湖中除了沟渠中可以行船，其他地方水位不深，都是苇田和荷花荡，[芦苇高大密集](../Page/芦苇.md "wikilink")，质量在全[中国最好](../Page/中国.md "wikilink")，湖区渔民以芦苇编织苇席为副业。晚秋芦苇收获后，淀水已结冰。夏季芦苇密集，水道形成苇墙中的迷宫，其景色非常独特，宜人，所以成为著名的旅游胜地。

湖水自枣林口通过[大清河流入](../Page/大清河.md "wikilink")[海河](../Page/海河.md "wikilink")。近些年由于上游城市用水巨增，在山区修建了不少水库，白洋淀补水缺乏，所以在枣林庄也修建了大闸，除了雨季很少向外泻水。

由于气候干旱和地下水过度开采，以及80年代以来的工业废水问题，白洋淀面临着缺水与污染的生态问题的考验。保定市政府计划用十年时间、耗资80亿元人民币来解决这一问题。\[1\]

[1965-9_抗日战争期间的白洋淀雁翎队.jpg](https://zh.wikipedia.org/wiki/File:1965-9_抗日战争期间的白洋淀雁翎队.jpg "fig:1965-9_抗日战争期间的白洋淀雁翎队.jpg")
冬季淀上结冰，也是冰上旅游的好季节。只有入冬和冰化季节，船和冰排都不能行走。湖区岛上居民经常需要储备一些必须的[燃料和](../Page/燃料.md "wikilink")[食物](../Page/食物.md "wikilink")，基本和外界隔绝，称为“潺期”。
在[抗日战争期间](../Page/抗日战争.md "wikilink")，著名的抗日游击队“雁翎队”在芦苇迷宫和荷花荡中和[日本侵略军捉迷藏](../Page/日本.md "wikilink")，经常将侵略军打得焦头烂额，由此也产生了[中国](../Page/中国.md "wikilink")[文学以](../Page/文学.md "wikilink")[孙犁为代表的](../Page/孙犁.md "wikilink")“荷花-{淀}-派”。

白洋淀由于地处[北京](../Page/北京.md "wikilink")、[天津和](../Page/天津.md "wikilink")[保定三个大城市的中心](../Page/保定市.md "wikilink")，[旅游业发展很快](../Page/旅游.md "wikilink")，2003年被评为4A级旅游风景地，2007年，白洋淀又被评为全国首批5A级景区之一。
[Baiyangdian_Lake.JPG](https://zh.wikipedia.org/wiki/File:Baiyangdian_Lake.JPG "fig:Baiyangdian_Lake.JPG")

## 相关文学及影视作品

  - [荷花-{淀}-](../Page/荷花淀.md "wikilink")（[孙犁](../Page/孙犁.md "wikilink")）
  - [小兵张嘎](../Page/小兵张嘎.md "wikilink")（[徐光耀](../Page/徐光耀.md "wikilink")）
  - [新儿女英雄传](../Page/新儿女英雄传.md "wikilink")（[孔厥](../Page/孔厥.md "wikilink")、[袁静](../Page/袁静.md "wikilink")）

## 参考文献

  -
[B](../Category/河北湖泊.md "wikilink") [B](../Category/中国淡水湖.md "wikilink")
[Category:雄安新区](../Category/雄安新区.md "wikilink")

1.  [水体污染每况愈下 80亿规划求解白洋淀二十载之渴](http://news.xinhuanet.com/politics/2006-01/23/content_4086456.htm)