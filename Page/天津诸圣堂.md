**天津诸圣堂**又名**安里甘教堂**、**浙江路教堂**，建于1936年，是为天津英国侨民服务的圣公会教堂。位于原[天津英租界马厂道](../Page/天津英租界.md "wikilink")（Race
Course
Road）（今[和平区浙江路](../Page/和平区_\(天津市\).md "wikilink")2号），该建筑目前是[天津市文物保护单位](../Page/天津市文物保护单位.md "wikilink")\[1\]和[重点保护等级历史风貌建筑](../Page/天津历史风貌建筑.md "wikilink")\[2\]。

## 历史

1880年，[英国圣公会设立华北教区](../Page/英国圣公会.md "wikilink")，起初活动中心在[山东](../Page/山东.md "wikilink")[泰安](../Page/泰安.md "wikilink")，后来移驻[北京](../Page/北京.md "wikilink")。主教史嘉乐常从北京到天津来为英国圣公会信徒举行礼拜，地点在[合众会堂](../Page/合众会堂.md "wikilink")。1893年，[天津英租界工部局将咪哆士道](../Page/天津英租界工部局.md "wikilink")（今[泰安道](../Page/泰安道.md "wikilink")）与马厂道东北端（今浙江路）的一片沼泽地赠予教会，建成一座容纳60人的小教堂。不久即感觉需要建造较大的教堂。1900年6月举行奠基仪式不久，就因为[义和团事变而停工](../Page/义和团事变.md "wikilink")。1901年事变平息后重新开工，到1903年教堂正式落成。1935年，原教堂因火灾烧毁，次年进行重建。此外，圣公会在九江路另有一处教堂，供中国教徒们做礼拜。\[3\]。

1958年，天津市基督教各教派实行联合礼拜，浙江路堂为保留下来的四处教堂之一。[文化大革命中教堂关闭](../Page/文化大革命.md "wikilink")，被工厂占用。1997年，教堂被列为天津市第二批[市级文物保护单位](../Page/天津市文物保护单位.md "wikilink")。2009年6月，诸圣堂进行修复，恢复原貌，年底竣工\[4\]。

## 建筑

天津诸圣堂为明显的哥特式风格，有高耸的钟楼和细长的门窗，能容纳300人。目前，天津诸圣堂及配楼外檐、结构已修复完毕，但未投入使用，也未恢复宗教活动，而该建筑的产权单位为天津市宗教房地产公司。

## 参考文献

## 参见

  - [中华圣公会](../Page/中华圣公会.md "wikilink")
  - [天津市开放宗教活动场所列表](../Page/天津市开放宗教活动场所列表.md "wikilink")

{{-}}

[Category:天津新教教堂](../Category/天津新教教堂.md "wikilink")
[Category:中國前聖公會教堂](../Category/中國前聖公會教堂.md "wikilink")
[Category:天津租界建筑物](../Category/天津租界建筑物.md "wikilink")
[Category:天津英租界](../Category/天津英租界.md "wikilink")

1.  [天津市人民政府](../Page/天津市人民政府.md "wikilink")，一九九七年六月二日公布
2.  [天津市人民政府](../Page/天津市人民政府.md "wikilink")，二零零九年四月三日公布
3.
4.