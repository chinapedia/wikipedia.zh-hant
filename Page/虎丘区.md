__NOTOC__
**虎丘区**是[中国](../Page/中国.md "wikilink")[江苏省](../Page/江苏省.md "wikilink")[苏州市所辖的一个](../Page/苏州市.md "wikilink")[市辖区](../Page/市辖区.md "wikilink")，总面积为258平方公里，2010年人口为57.23万。**[苏州高新技术产业开发区](../Page/苏州高新技术产业开发区.md "wikilink")**位于辖区内，与虎丘区政府机构合署办公。但區名的來源[虎丘山风景名胜区並非位於本區](../Page/虎丘山.md "wikilink")，而是位於相鄰的[姑蘇區內](../Page/姑蘇區.md "wikilink")。

## 历史

[苏州市](../Page/苏州市.md "wikilink")**虎丘区**始建于1951年，当时称郊区，由[吴县划出城东](../Page/吴县.md "wikilink")、城西两区组成，2000年9月8日被批准改名为虎丘区，下辖横塘、虎丘、浒墅关3个镇和白洋湾街道、浒墅关经济开发区。2002年9月，苏州市委、市政府对新区、**虎丘区**、[相城区](../Page/相城区.md "wikilink")、[吴中区等进行了区划调整](../Page/吴中区.md "wikilink")，将虎丘区虎丘镇和白洋湾街道以及横塘镇的部分村划出，由相城区和吴中区划入通安镇和东渚镇、镇湖街道，建立[苏州高新区](../Page/苏州高新区.md "wikilink")、虎丘区。

## 行政区划

虎丘区辖[枫桥](../Page/枫桥街道.md "wikilink")、[狮山](../Page/狮山街道_\(苏州市\).md "wikilink")、[横塘](../Page/横塘街道.md "wikilink")、[镇湖](../Page/镇湖街道.md "wikilink")、[东渚](../Page/东渚.md "wikilink")5个街道及[浒墅关](../Page/浒墅关.md "wikilink")、[通安](../Page/通安镇.md "wikilink")2个镇。

## 参见

  - [虎丘山风景名胜区](../Page/虎丘山.md "wikilink")

## 外部链接

  - [苏州市虎丘区政府网站](http://www.snd.gov.cn/)

[虎丘区](../Category/虎丘区.md "wikilink") [区](../Category/苏州区市.md "wikilink")
[苏州市](../Category/江苏市辖区.md "wikilink")