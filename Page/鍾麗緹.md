**鍾麗緹**（，），香港女演員，於[加拿大](../Page/加拿大.md "wikilink")[蒙特利爾出生](../Page/蒙特利爾.md "wikilink")，為[中](../Page/中國.md "wikilink")[越混血兒](../Page/越南.md "wikilink")，父亲是[越南華裔](../Page/越南華裔.md "wikilink")
，母亲是[越南人](../Page/越南人.md "wikilink")。

鍾麗緹於加拿大[魁北克大學畢業](../Page/魁北克大學.md "wikilink")，主修[行銷](../Page/行銷.md "wikilink")，精通五種語言（[英語](../Page/英語.md "wikilink")、[法語](../Page/法語.md "wikilink")、[粵語](../Page/粵語.md "wikilink")、[普通話](../Page/普通話.md "wikilink")、[越南語](../Page/越南語.md "wikilink")）。其後到香港參選1993年度[國際華裔小姐得到冠軍而入行](../Page/國際華裔小姐.md "wikilink")，夫婿為演員[張倫碩](../Page/張倫碩.md "wikilink")。

## 家庭

1998年，與外籍夫婿Glen Ross結婚，有一段幸福的婚姻生活，育有一女Yasmine Ross，但婚姻於2002年結束。

2004年，鍾麗緹與其第二任丈夫台灣音樂製作人[嚴錚步入禮堂](../Page/嚴錚.md "wikilink")，並2008年在[北京生下長女](../Page/北京.md "wikilink")，取名嚴稚晴（Jaden）
，2010年在[北京誕下次女嚴紫凝](../Page/北京.md "wikilink")（Cayla，後改嚴稚稜），但2011年兩人離婚。

2015年7月，與[張倫碩合作真人秀節目](../Page/張倫碩.md "wikilink")《如果愛》日久生情，6月初更被媒體拍到在港穿情侶裝，蜜遊山頂，鍾麗緹與張倫碩在港會見傳媒時，大方確認戀愛關係
，2016年9月，鍾麗緹和張倫碩拍京劇婚紗照 。
11月8日，張倫碩和鍾麗緹於北京舉辦婚禮。\[1\]2017年11月，兩人結婚一周年，鍾麗緹決定讓三名女兒跟張姓，改名為張敏鈞、張思捷和張凱琳。

2017年11月，鍾麗緹接受訪問時透露近日在量子大學（Quantum
University）修讀自然及綜合醫學學士，畢業後可以考取醫生，亦有興趣可以修讀
MBA，這些是網上學習，在線獲得學位，有時間先學好方便。而做中醫是她十五歲的夢想，希望可以開設一個健康中心\[2\]。

## 演出作品

### 電影

<table>
<tbody>
<tr class="odd">
<td><p><strong>年份</strong></p></td>
<td><p><strong>片名</strong></p></td>
<td><p><strong>角色</strong></p></td>
</tr>
<tr class="even">
<td><p>1993年</p></td>
<td><p><a href="../Page/至尊三十六計之偷天換日.md" title="wikilink">至尊三十六計之偷天換日</a></p></td>
<td><p>莉莉</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/白髮魔女2.md" title="wikilink">白髮魔女2</a></p></td>
<td><p>凌月兒</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1994年</p></td>
<td><p><a href="../Page/珠光寶氣_(香港電影).md" title="wikilink">珠光寶氣</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/第六感奇緣之人魚傳說.md" title="wikilink">第六感奇緣之人魚傳說</a></p></td>
<td><p>小美</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/中南海保鑣.md" title="wikilink">中南海-{zh-hans:保镖; zh-hant:保鑣;}-</a></p></td>
<td><p>楊倩兒</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/戀愛的天空.md" title="wikilink">戀愛的天空</a>(中譯:四个好色的女人)</p></td>
<td><p>Ady</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/神探磨轆.md" title="wikilink">神探磨轆</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/九品芝麻官之白面包青天.md" title="wikilink">九品芝麻官之白-{面}-包青天</a></p></td>
<td><p>吳好緹（<a href="../Page/台灣.md" title="wikilink">台譯</a>：莫再緹）</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/破壞之王.md" title="wikilink">破壞之王</a></p></td>
<td><p>阿麗</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1995年</p></td>
<td><p><a href="../Page/叛逆情緣.md" title="wikilink">叛逆情緣</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/虎猛威龍.md" title="wikilink">虎猛威龍</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/旺角的天空.md" title="wikilink">旺角的天空</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/冒險遊戲_(電影).md" title="wikilink">冒險遊戲</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/流氓醫生.md" title="wikilink">流氓醫生</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1996年</p></td>
<td><p><a href="../Page/太極拳2.md" title="wikilink">太極拳2</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/運財智叻星_(電影).md" title="wikilink">運財智叻星</a></p></td>
<td><p>九天玄女</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/食神_(電影).md" title="wikilink">食神</a></p></td>
<td><p>客串</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/功夫小子闖情關_(電影).md" title="wikilink">功夫小子闖情關</a></p></td>
<td><p>曹央雲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1997年</p></td>
<td><p><a href="../Page/最佳拍檔之醉街拍檔.md" title="wikilink">最佳拍檔之醉街拍檔</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陰陽路.md" title="wikilink">陰陽路</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/97家有喜事.md" title="wikilink">97家有喜事</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2000年</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/冷戰.md" title="wikilink">冷戰</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/中華賭俠.md" title="wikilink">中華賭俠</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/偷吻_(2000年電影).md" title="wikilink">偷吻</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2001年</p></td>
<td><p><a href="../Page/晚孃_(2001年電影).md" title="wikilink">晚孃</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2002年</p></td>
<td><p><a href="../Page/色戒_(2001年電影).md" title="wikilink">色戒</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2003年</p></td>
<td><p><a href="../Page/鬼馬狂想曲.md" title="wikilink">鬼馬狂想曲</a></p></td>
<td><p>狄娜</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/飛龍再生.md" title="wikilink">飛龍再生</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2004年</p></td>
<td><p><a href="../Page/驚心動魄_(2004年電影).md" title="wikilink">驚心動魄</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2005年</p></td>
<td><p><a href="../Page/凶男寡女.md" title="wikilink">凶男寡女</a></p></td>
<td><p>Moon</p></td>
</tr>
<tr class="odd">
<td><p>2010年</p></td>
<td><p><a href="../Page/李小龍_(電影).md" title="wikilink">李小龍</a></p></td>
<td><p>何愛榆</p></td>
</tr>
<tr class="even">
<td><p>2011年</p></td>
<td><p><a href="../Page/親密敵人.md" title="wikilink">親密敵人</a></p></td>
<td><p>Lucy</p></td>
</tr>
<tr class="odd">
<td><p>2013年</p></td>
<td><p><a href="../Page/人间蒸发_(2013年电影).md" title="wikilink">人間蒸發</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2014年</p></td>
<td><p><a href="../Page/壞姐姐之拆婚聯盟.md" title="wikilink">壞姐姐之拆婚聯盟</a></p></td>
<td><p>前妻</p></td>
</tr>
<tr class="odd">
<td><p>2015年</p></td>
<td><p><a href="../Page/將錯就錯.md" title="wikilink">將錯就錯</a></p></td>
<td><p>新加坡女警</p></td>
</tr>
<tr class="even">
<td><p>2019年</p></td>
<td><p><a href="../Page/一吻定情_(電影).md" title="wikilink">一吻定情</a></p></td>
<td><p>阿利嫂</p></td>
</tr>
</tbody>
</table>

### 電視劇

|        |                                                 |        |
| ------ | ----------------------------------------------- | ------ |
| **年份** | **劇名**                                          | **角色** |
| 2003年  | [天龍八部](../Page/天龍八部_\(2003年電視劇\).md "wikilink") | 马夫人康敏  |

### 綜藝節目

|        |                                                |
| ------ | ---------------------------------------------- |
| **年份** | **節目名**                                        |
| 2014年  | 《[人生第一次第二季](../Page/人生第一次第二季.md "wikilink")》   |
| 2015年  | 《[如果愛第二季](../Page/如果愛_\(電視節目\).md "wikilink")》 |
| 2016年  | 《[如果愛第三季](../Page/如果愛_\(電視節目\).md "wikilink")》 |

## 参考资料

  - [鍾麗緹首次携三女拍写真
    大女儿美貌出众](http://ent.163.com/photoview/00AJ0003/527928.html).网易娱乐.2014-05-06
  - [為老公博盡！46歲鍾麗緹甘做高齡產婦](http://hk.on.cc/hk/bkn/cnt/entertainment/20161018/bkn-20161018203932721-1018_00862_001.html).東網.2016-10-18

## 外部链接

  -
  -
  -
  -
  -
[Category:华裔加拿大人](../Category/华裔加拿大人.md "wikilink")
[Category:越南裔加拿大人](../Category/越南裔加拿大人.md "wikilink")
[Category:京族裔混血兒](../Category/京族裔混血兒.md "wikilink")
[Category:蒙特婁人](../Category/蒙特婁人.md "wikilink")
[Category:魁北克大學校友](../Category/魁北克大學校友.md "wikilink")
[Category:香港電影女演員](../Category/香港電影女演員.md "wikilink")
[Category:香港電視女演員](../Category/香港電視女演員.md "wikilink")
[L](../Category/鍾姓.md "wikilink")
[Category:越南裔香港人](../Category/越南裔香港人.md "wikilink")
[Category:华裔混血儿](../Category/华裔混血儿.md "wikilink")

1.
2.  [【專訪】鍾麗緹收粉紅鑽戒興奮尖叫　調理身體計劃生B](http://bka.mpweekly.com/interview/%E5%A8%9B%E6%A8%82123/20171118-91104).明周娛樂.2017-11-18