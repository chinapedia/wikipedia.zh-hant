**新-{zh-cn:;zh-tw:;zh-hk:}-县**，旧名**新**，位于[中华人民共和国](../Page/中华人民共和国.md "wikilink")[江西省中部](../Page/江西省.md "wikilink")，是[吉安市北部下辖的一个](../Page/吉安市.md "wikilink")[县](../Page/县_\(中华人民共和国\).md "wikilink")。

## 名称

本称“新淦”，1957年中国大陆推行[简化字时更名为](../Page/简体中文.md "wikilink")“-{新干}-”，繁体写法也由“-{新淦}-”变为“-{新幹}-”。

## 历史

新干是江西十八古县之一，[秦始皇二十六年](../Page/秦始皇.md "wikilink")（公元前221年）置新淦县，治所在淦阳（今[樟树市区](../Page/樟树市.md "wikilink")）；[隋时](../Page/隋.md "wikilink")，县治迁到南市村（今[金川镇](../Page/金川镇.md "wikilink")）；1957年改为新干县。

## 地理

东邻[乐安](../Page/乐安.md "wikilink")，南毗[永丰](../Page/永丰.md "wikilink")、[峡江](../Page/峡江.md "wikilink")，西接[新余](../Page/新余.md "wikilink")，北联[樟树](../Page/樟树市.md "wikilink")、[丰城](../Page/丰城.md "wikilink")；[赣江纵贯](../Page/赣江.md "wikilink")。

## 行政区划

全县辖

  - 7镇：金川镇、三湖镇、大洋洲镇、七琴镇、麦斜镇、界埠镇、溧江镇
  - 6乡：桃溪乡、城上乡、潭丘乡、神政桥乡、沂江乡、荷浦乡

[XinganStation.jpg](https://zh.wikipedia.org/wiki/File:XinganStation.jpg "fig:XinganStation.jpg")

## 交通

  - 铁路：[京九铁路](../Page/京九铁路.md "wikilink")
  - 公路：[105国道](../Page/105国道.md "wikilink")、[赣粤高速公路](../Page/大广高速公路.md "wikilink")
  - 水运：[赣江](../Page/赣江.md "wikilink")

## 风景名胜

  - [大洋洲程家遺址](../Page/大洋洲程家遺址.md "wikilink")
  - [玉华山](../Page/玉华山.md "wikilink")
  - [上寨](../Page/上寨.md "wikilink")

## 参考文献

## 外部链接

  - [新干县人民政府网](http://www.xingan.gov.cn/)

{{-}}

[吉安](../Category/江西省县份.md "wikilink")
[新干县](../Category/新干县.md "wikilink")
[县](../Category/吉安区县市.md "wikilink")