**犍陀羅語佛教原稿**是已發現最早的佛教文獻及[印度文獻](../Page/印度.md "wikilink")。原稿是以[犍陀羅語](../Page/犍陀羅語.md "wikilink")[佉盧文書寫](../Page/佉盧文.md "wikilink")，代表了來自於的文獻。1994年，[大英圖書館獲得](../Page/大英圖書館.md "wikilink")8份從1世紀到2世紀的犍陀羅語原稿。這些原稿全部被寫在[樺樹樹皮上及泥陶罐上](../Page/樺樹.md "wikilink")。之後陸續發現更多原稿，目前共計77份，分藏於[大英圖書館](../Page/大英圖書館.md "wikilink")、[美國國會圖書館](../Page/美國國會圖書館.md "wikilink")、[西雅圖華盛頓大學等地](../Page/西雅圖華盛頓大學.md "wikilink")。\[1\]\[2\]\[3\]

研究該領域的專家邵瑞祺（Richard
Salomon）認定其所研究的那批[犍陀羅語文獻所屬部派為](../Page/犍陀羅語.md "wikilink")[法藏部](../Page/法藏部.md "wikilink")\[4\]
，他並稱[大英圖書館的藏本](../Page/大英圖書館.md "wikilink")「代表了保存在那揭羅曷國（Nagarāhāra）[法藏部寺院圖書館的龐大經卷中](../Page/法藏部.md "wikilink")，隨機但相當典型的一部分」。\[5\]

## 寫本

### [-{于}-闐法句經](../Page/于闐.md "wikilink")

19世紀末在中國新疆[-{于}-闐地區出土的](../Page/于闐.md "wikilink")[樺樹皮寫卷](../Page/樺樹.md "wikilink")，後來被分別收藏在法國[巴黎和俄國](../Page/巴黎.md "wikilink")[聖彼得堡](../Page/聖彼得堡.md "wikilink")。英國劍橋大學教授John
Brough在1962年出版的《犍陀羅語法句經》（The Gandhari
Dharmapada）為該[-{于}-闐寫卷最重要的學術校勘文本](../Page/于闐.md "wikilink")\[6\]。

其抄寫的具體時間，據林梅村先生考證，應該是在二世紀末到三世紀初之一段內（公元175-230），並判定該本法句經為[法藏部的誦本](../Page/法藏部.md "wikilink")。在犍陀羅語《法句經》的這些偈頌中，大約有225到230首偈頌可以在[巴利本中找到對應](../Page/巴利语.md "wikilink")\[7\]。

### [大英圖書館藏品](../Page/大英圖書館.md "wikilink")

1994年9月，倫敦的英國圖書館得到由一位匿名的捐贈者所捐贈的一批古代[樺樹皮寫卷](../Page/樺樹.md "wikilink")。寫卷來自阿富汗東部。約含有二十種主要為[初期佛教的寫本](../Page/初期佛教.md "wikilink")，包括《[眾集經](../Page/眾集經.md "wikilink")》、《》、《無熱惱池偈頌》（Anavatapta-gāthā\[8\]）以及一些《法句經》的片段等等，非常可能是來自於法藏部。其中的一個陶罐，壁上寫有[佉盧文的銘記](../Page/佉盧文.md "wikilink")，講到這是屬於法藏部僧的物品\[9\]\[10\]。

### Senior藏品

Robert
Senior的收藏品，內容以《[相應部](../Page/相應部.md "wikilink")》最多\[11\]，如《》；還包括《[中部](../Page/中部.md "wikilink")》的87、120、128經，《[長阿含經](../Page/長阿含經.md "wikilink")·[沙門果經](../Page/沙門果經.md "wikilink")》、《[長部](../Page/長部.md "wikilink")·[大般涅槃經](../Page/大般涅槃经_\(上座部\).md "wikilink")》等。\[12\]原稿有嗢陀南（攝頌）或是目錄，列舉經名說有55部經。這種經文合集的方式，過去有[安世高所譯](../Page/安世高.md "wikilink")《七處三觀經》附上從《[增一阿含經](../Page/增一阿含經.md "wikilink")》選出的《雜經四十四篇》\[13\]，以及單卷本《雜阿含經》收錄二十七經。\[14\]\[15\]此藏品出土地點不明，據推測可能來自[阿富汗東部](../Page/阿富汗.md "wikilink")（屬於古代[犍陀羅國](../Page/健馱邏國.md "wikilink")，位於今日[賈拉拉巴德以南十公里](../Page/賈拉拉巴德.md "wikilink")）。原稿藏在地宮的陶罐中，罐身與罐蓋上的銘文顯示供養者將這當[法身](../Page/法身.md "wikilink")[舍利藏在](../Page/舍利.md "wikilink")[塔中崇拜](../Page/塔.md "wikilink")。綜合銘文的日期與對樹皮[放射性碳定年的結果](../Page/放射性碳定年法.md "wikilink")，原稿的年代是公元130-140年。經文屬於[法藏部所傳](../Page/法藏部.md "wikilink")。\[16\]

### 斯柯延藏品

主要由挪威收藏家所收藏，一小部份則為日本人所持有。由[慕尼黑大學哈特曼](../Page/慕尼黑大学.md "wikilink")（Jens‐Uwe
Hartmann）、鮑姆斯（Stefan Baums）為代表的一批學者在加以研究\[17\]。

中國僧人[玄奘記錄了](../Page/玄奘.md "wikilink")[巴米揚大眾部](../Page/巴米揚.md "wikilink")[說出世部寺院的存在](../Page/說出世部.md "wikilink")\[18\]。20年前出土於巴米揚，現為挪威斯柯延(Schøyen)搜集品的斷簡中包含有大量大乘經典和大眾部律典，其中一部分是用佉盧文字體書寫的犍陀羅語經典，寫於2\~4世紀；其他部分是用[婆羅米文字書寫的梵文經典](../Page/婆罗米文.md "wikilink")，寫於2\~8世紀\[19\]。

其犍陀羅語部份的藏品則包括屬於[大乘佛教的](../Page/大乘佛教.md "wikilink")《賢劫經》（Bhadrakalpika-Sūtra）、《集一切福德三昧經》（Sarvapuṇyasamuccayasāmādhi–Sūtra）、《菩薩藏經》（Bodhisattvapiṭaka–Sūtra）等等\[20\]。

### 華盛頓大學藏品

2002年華盛頓大學圖書館從英國的私人收藏家手中買到一份手稿。它是已知最早的論書，論題是苦諦，沒有經過編輯修改，與其他原稿經過大量修改不同。\[21\]\[22\]\[23\]

### 巴扎爾藏品

[柏林自由大學福爾克](../Page/柏林自由大學.md "wikilink")（Harry
Falk）為核心的團隊所研究的[巴扎爾蒐集品](../Page/巴焦尔特区.md "wikilink")（The
Bajaur
collection）。此一蒐集品是以其可能的來源地命名，共有18個樺樹皮寫卷，包括諸多的[初期佛教](../Page/初期佛教.md "wikilink")[偈頌](../Page/偈頌.md "wikilink")、零碎的譬喻經、片段的[法句經](../Page/法句經.md "wikilink")、[波羅提木叉](../Page/波羅提木叉.md "wikilink")，並保存了很長的大乘佛經，如《[般若八千頌](../Page/八千頌般若經.md "wikilink")》的第一到第五品殘片、《[阿閦佛國經](../Page/阿閦佛國經.md "wikilink")》等等。這個寫本群最早是由兩位巴基斯坦白沙瓦大學（University
of Peshawar）考古系學者納西姆•汗（Nasim Khan）與索黑爾•汗（Sohail Khan）刊布\[24\]。

該《八千頌般若經》殘片，其年代為公元後一世紀後半葉至二世紀中葉，為目前所能找到「存世大乘佛教經典」文本最早的確鑿年代。將之與[支婁迦讖的](../Page/支婁迦讖.md "wikilink")《[道行般若經](../Page/道行般若經.md "wikilink")》進行對讀，可發現這個犍陀羅語本比支譯所據版本略早，並不是同經梵語本的略譯，支譯所據版本比犍陀羅語本稍加擴充。

## 已出版的資料

華盛頓大學和大英圖書館收藏的文本的學術評論版本已經由華盛頓大學出版社出版為《Gandhāran Buddhist
Texts》叢書，第一冊是對於《》從[語音學](../Page/語音學.md "wikilink")、[構詞學](../Page/構詞學.md "wikilink")、[正寫法](../Page/正寫法.md "wikilink")、[古文字學等角度詳細分析](../Page/古文字學.md "wikilink")，第二冊是三篇《增一阿含經》，第三冊是犍陀羅文《法句經》的修訂版與本生故事，第四冊是四篇《雜阿含經》，第五冊是[伽陀](../Page/伽陀.md "wikilink")，第六冊是21篇。\[25\]來自斯柯延藏品的資料由[挪威](../Page/挪威.md "wikilink")[奧斯陸Hermes出版社出版](../Page/奧斯陸.md "wikilink")。

下列學者出版了犍陀羅手稿的片段: [Mark Allon](../Page/Mark_Allon.md "wikilink")、[Richard
Salomon](../Page/Richard_Salomon.md "wikilink")、[Timothy
Lenz](../Page/Timothy_Lenz.md "wikilink") 和 [Jens
Braarvig](../Page/Jens_Braarvig.md "wikilink")。下面列出一些出版了的資料:

  - 1999年 - 《Ancient Buddhist Scrolls from Gandhara: The British Library
    Kharosthi Fragments》, [Richard
    Salomon](../Page/Richard_Salomon.md "wikilink"), [F. Raymond
    Allchin](../Page/F._Raymond_Allchin.md "wikilink") 和 Mark Barnard 著。
  - 2000年 - 《Manuscripts in the Schøyen Collection I, Buddhist
    Manuscripts, Vol. 1.》, Braarvig, Jens. 著。Oslo: Hermes Publishing 出版。
  - 2000年 - 《A Gandhari Version of the Rhinoceros Sutra: British Library
    Kharosthi Fragment 5B (Gandharan Buddhist Texts, 1)》, Andrew Glass 和
    Richard Salomon 著。
  - 2001年 - 《Three Gandhari Ekottarikagama-Type Sutras: British Library
    Kharosthi Fragments 12 and 14 (GBT Vol 2)》 Mark Allon (Author,
    Editor), Andrew Glass (Editor) 著。Seattle: University of Washington
    Press 出版。
  - 2003年 - 《A New Version of the Gandhari Dharmapada and a Collection
    of Previous-Birth Stories: British Library Karosthi Fragments 16 +
    25 (GBT vol. 3)》, Timothy Lenz (Author), Andrew Glass (Author),
    Bhikshu Dharmamitra (Author)著。Seattle: University of Washington
    Press 出版。
  - 2008年 - 《Four Gandhari Samyuktagama Sutras: Senior Kharosthi
    Fragment 5 (GBT, Vol. 4)》 Andrew Glass (Author), Mark Allon
    (Contributor) 著 Seattle: University of Washington Press 出版。
  - 2009年 - 《Two Gandhari Manuscripts of the Songs of Lake Anavtapta
    (Anavatapta-gatha): British Library Kharosthi Fragment 1 and Senior
    Scroll 14 (GBT vol 5)》 Richard Salomon (Author), Andrew Glass
    (Contributor) 著。Seattle: University of Washington Press 出版。

## 註釋

## 參考

  - Salomon, Richard. *Ancient Buddhist Scrolls from Gandhāra*,
    University of Washington Press, Seattle, 1999, ISBN
    978-0-295-97769-0.
  - Salomon, Richard. *A Gāndhārī Version of the Rhinoceros Sutra:
    British Library  Fragment 5B* Univ. of Washington Press: Seattle and
    London, 2000.
  - Allon, Mark. *Wrestling with Kharosthi Manuscripts*, BDK Fellowship
    Newsletter, No 7, 2004.

## 外部連結

  - [早期佛教手稿計劃](https://web.archive.org/web/20110510020253/http://www.ebmp.org/)
  - [犍陀羅語言和文獻](https://gandhari.org/)
  - [Schøyen收藏品](http://www.schoyencollection.com/Buddhism.htm)
  - [犍陀羅語《法句經》](http://titus.uni-frankfurt.de/texte/etcs/ind/mind/gandhpkt/dhpgpkt/dhpgp.htm)

[Category:佛经](../Category/佛经.md "wikilink")
[Category:大英图书馆](../Category/大英图书馆.md "wikilink")
[Category:佛教之最](../Category/佛教之最.md "wikilink")
[Category:初期佛教典籍](../Category/初期佛教典籍.md "wikilink")

1.  [早期佛教手稿計劃](http://ebmp.org/)

2.

3.

4.  "The Discovery of 'the Oldest Buddhist Manuscripts'" Review article
    by Enomoto Fumio. *The Eastern Buddhist*, Vol NS32 Issue I, 2000, pg
    160

5.  Richard Salomon. *Ancient Buddhist Scrolls from Gandhāra: The
    British Library Kharosthī Fragments*, with contributions by Raymond
    Allchin and Mark Barnard. Seattle: University of Washington Press;
    London: The British Library, 1999. pg 181

6.

7.

8.

9.
10.
11.
12.
13.

14.

15.

16.

17.
18. 《大唐西域記》：「梵衍那國。東西二千餘里。南北三百餘里。在雪山之中也。……伽藍數十所。僧徒數千人。宗學小乘說出世部」

19.

20.
21.
22.

23.

24.
25.