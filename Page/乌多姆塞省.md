**乌多姆塞省**（）是[老挝西北部的一个](../Page/老挝.md "wikilink")[省](../Page/老挝行政区划.md "wikilink")，首府[芒赛](../Page/芒赛.md "wikilink")。

這裡最早的居民是[克木族](../Page/克木族.md "wikilink")，在1260年來自西雙版納的泰人征服該地，並建立一條名為Ban
Luang Cheng的村落。

## 行政区划

  - [赛县](../Page/赛县.md "wikilink")（芒赛县、孟赛县，ເມືອງໄຊ）
  - [拉县](../Page/拉县.md "wikilink")（ເມືອງຫຼາ）
  - [纳莫县](../Page/纳莫县.md "wikilink")（ເມືອງນາໝໍ້）
  - [雅县](../Page/雅县.md "wikilink")（ເມືອງງາ）
  - [本县](../Page/本县.md "wikilink")（孟本县，ເມືອງແບ່ງ）
  - [洪县](../Page/洪县.md "wikilink")（孟昏县，ເມືອງຮຸນ）
  - [巴本县](../Page/巴本县.md "wikilink")（孟巴本县，ເມືອງປາກແບ່ງ）

## 参考资料

[Oudomxai](../Category/老挝省份.md "wikilink")