**A Great Big
Sled**是一首由來自[拉斯維加斯的](../Page/拉斯維加斯.md "wikilink")[殺手樂團](../Page/殺手樂團.md "wikilink")（The
Killers）所演唱的歌曲。這首歌是在2006年12月中旬錄製，並作為樂團的[聖誕節](../Page/聖誕節.md "wikilink")[單曲](../Page/單曲.md "wikilink")。製作人愛倫·莫德（Alan
Moulder）的妻子東妮·荷莉岱（Toni
Halliday），同時也是[英格蘭樂團](../Page/英格蘭.md "wikilink")「Curve」的成員，在歌曲中擔任背景合音。本單曲在2006年12月5日以
[iTunes Store](../Page/iTunes_Store.md "wikilink")
數位下載的方式發行\[1\]。所有的收入都會捐給[波諾](../Page/波諾.md "wikilink")（[U2樂團主唱](../Page/U2.md "wikilink")）「RED
campaign」計劃之一的[愛滋病](../Page/愛滋病.md "wikilink")（AIDS）慈善機構。歌曲也拍攝了[音樂錄影帶](../Page/音樂錄影帶.md "wikilink")，其中包含了一些在樂團成員慶祝各種節日時的「偷拍」畫面。

## 排行榜成績

「A Great Big Sled」在2006年12月18日登上英國官方下載排行榜（UK Official Download
Chart）第11名\[2\]。這首歌曲也在[告示牌](../Page/告示牌_\(雜誌\).md "wikilink")[公告牌百强单曲榜登上第](../Page/公告牌百强单曲榜.md "wikilink")54名\[3\]。

## 注釋與資料來源

## 外部連結

  - [Great Big Sled
    歌曲](http://mp.aol.com/audio.index.adp?mode=0&pmmsid=1778869)（[AOL](../Page/美國線上.md "wikilink")
    Music）
  - [Great Big Sled
    音樂錄影帶](http://us.rd.yahoo.com/launch/promotions/xmas/killersvideo/*http://music.yahoo.com/video/?37661773&redirectURL=http://music.yahoo.com/promos/xmas/)（Yahoo
    Music）

[Category:殺手樂團](../Category/殺手樂團.md "wikilink")
[Category:2006年單曲](../Category/2006年單曲.md "wikilink")
[Category:圣诞歌曲](../Category/圣诞歌曲.md "wikilink")

1.
2.
3.