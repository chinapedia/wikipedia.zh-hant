**海宁市**位于[长江三角洲南翼](../Page/长江三角洲.md "wikilink")、[浙江省北部](../Page/浙江省.md "wikilink")，南濒[钱塘江](../Page/钱塘江.md "wikilink")，为[嘉兴市代管的一个](../Page/嘉兴市.md "wikilink")[县级市](../Page/县级市.md "wikilink")。下辖4个街道，8个镇。海宁市经济发达，为全国最具实力的[县市之一](../Page/县级行政区.md "wikilink")，2016年[GDP总量](../Page/国内生产总值.md "wikilink")744.09亿元，其综合竞争能力[2010年位居浙江省县（市）的第十一位，全国百强县（市）的第四十五位](../Page/中国百强县#县域经济论坛评价.md "wikilink")。全市辖域面积为731.03平方公里，2009年末户籍总人口655,049人。

## 历史

海宁是[良渚文化发源地之一](../Page/良渚文化.md "wikilink")。据考古资料证明，距今6000-7000年间，在海宁土地上已有先民生息。在[春秋](../Page/春秋.md "wikilink")[战国时](../Page/战国.md "wikilink")，海宁是[越](../Page/越.md "wikilink")、[吴](../Page/吴.md "wikilink")、[楚武原乡](../Page/楚.md "wikilink")、李乡、禦兒乡属地。[秦时在](../Page/秦朝.md "wikilink")[海盐县](../Page/海盐县.md "wikilink")、[由拳县境内](../Page/由拳县.md "wikilink")。[东汉](../Page/东汉.md "wikilink")[建安八年](../Page/建安_\(东汉\).md "wikilink")（203年）[陆逊在此任海昌屯田都尉并领县事](../Page/陆逊.md "wikilink")。[三国吴](../Page/三国吴.md "wikilink")[黄武二年](../Page/黄武.md "wikilink")（223年），析海盐、由拳置[盐官县](../Page/盐官县.md "wikilink")，属[吴郡](../Page/吴郡.md "wikilink")，隶[扬州](../Page/扬州_\(古代\).md "wikilink")，为海宁建县之始。[唐朝](../Page/唐朝.md "wikilink")[武德七年](../Page/武德.md "wikilink")（624年）并入[钱塘县](../Page/钱塘县.md "wikilink")，[贞观四年](../Page/貞觀_\(唐朝\).md "wikilink")（630年）复置盐官县。[元](../Page/元朝.md "wikilink")[元贞元年](../Page/元贞.md "wikilink")（1295年）升盐官州，[天历二年](../Page/天历_\(元朝\).md "wikilink")（1329年）改名[海宁州](../Page/海宁州.md "wikilink")。

[明](../Page/明朝.md "wikilink")[洪武二年](../Page/洪武.md "wikilink")（1369年）降为[海宁县](../Page/海宁县_\(明朝\).md "wikilink")，属[杭州府](../Page/杭州府.md "wikilink")。[清](../Page/清朝.md "wikilink")[乾隆三十八年](../Page/乾隆.md "wikilink")（1773年）复升为州。[民国元年](../Page/民国.md "wikilink")（1912年）改州为县，直属[浙江省](../Page/浙江省_\(中華民國\).md "wikilink")。历史上，海宁县治长期在[盐官镇](../Page/盐官镇_\(海宁市\).md "wikilink")。[抗日战争期间](../Page/抗日战争.md "wikilink")，曾先后迁移袁花一带乡间及县境以外，抗战胜利后设于硖石镇。民国37年（1948年）属[第一行政督察区](../Page/第一行政督察区.md "wikilink")。

1949年5月被[中国人民解放军攻占](../Page/中国人民解放军.md "wikilink")，建海宁县，属[嘉兴专区](../Page/嘉兴专区.md "wikilink")。1949年6月，县人民政府移驻[硖石镇](../Page/硖石街道.md "wikilink")。1958年10月海盐县并入海宁县，至1961年12月复建海盐县。1986年11月，撤海宁县，设海宁市，属嘉兴市。
[Haining_Indoor_Stadium.jpg](https://zh.wikipedia.org/wiki/File:Haining_Indoor_Stadium.jpg "fig:Haining_Indoor_Stadium.jpg")
[Haining_Swimming_Center.jpg](https://zh.wikipedia.org/wiki/File:Haining_Swimming_Center.jpg "fig:Haining_Swimming_Center.jpg")

## 行政区划

下辖4个街道、8个镇：

  - 街道：硖石街道、海洲街道、海昌街道、马桥街道
  - 镇：[许村镇](../Page/许村镇.md "wikilink")、[长安镇](../Page/长安镇_\(海宁市\).md "wikilink")、[周王庙镇](../Page/周王庙镇.md "wikilink")、[盐官镇](../Page/盐官镇_\(海宁市\).md "wikilink")、[斜桥镇](../Page/斜桥镇.md "wikilink")、[丁桥镇](../Page/丁桥镇.md "wikilink")、[袁花镇](../Page/袁花镇.md "wikilink")、[黄湾镇](../Page/黄湾镇.md "wikilink")

## 人口

根据2016年5‰人口抽样调查推算，海宁市2016年末常住人口83.50万人。

## 文化与生活

  - 海宁三大文化：[海宁潮文化](../Page/海宁潮.md "wikilink")、[灯彩文化](../Page/灯彩.md "wikilink")、名人文化

## 教育

境内主要院校：

  - [浙江财经大学东方学院](../Page/浙江财经大学#独立学院.md "wikilink")（海宁长安）
  - [浙江大学国际校区](../Page/浙江大学国际校区.md "wikilink")
  - [浙江机电职业技术学院长安校区](../Page/浙江机电职业技术学院.md "wikilink")
  - [海宁市高级中学](../Page/海宁市高级中学.md "wikilink")
  - [海宁市第一中学](../Page/海宁市第一中学.md "wikilink")
  - [海宁市南苑中学](../Page/海宁市南苑中学.md "wikilink")
  - [海宁市紫微高级中学](../Page/海宁市紫微高级中学.md "wikilink")
  - [海宁市紫微小学](../Page/海宁市紫微小学.md "wikilink")
  - [海宁市紫微初级中学](../Page/海宁市紫微初级中学.md "wikilink")

## 经济

海宁市2016年实现[国内生产总值](../Page/国内生产总值.md "wikilink")744.09亿元，比上年增长6.4%；[财政总收入](../Page/财政.md "wikilink")123.88亿元，比上年增长2.2%；城市居民人均可支配收入51954元，增长7.5%；农村居民人均纯收入30200元，增长7.8%；城镇登记失业率2.9%。\[1\]

2012福布斯中国大陆最佳商业城市位列第90名。

## 交通

  - （[沪杭高速公路](../Page/沪杭高速公路.md "wikilink")）

  -
  -
  - （[嘉绍大桥](../Page/嘉绍大桥.md "wikilink")）

  -
  - [杭州到海宁城际铁路](../Page/杭州到海宁城际铁路.md "wikilink")

[嘉绍大桥北起](../Page/嘉绍大桥.md "wikilink")[海宁](../Page/海宁.md "wikilink")，南接[绍兴](../Page/绍兴.md "wikilink")[上虞](../Page/上虞.md "wikilink")，是继[杭州湾跨海大桥后](../Page/杭州湾跨海大桥.md "wikilink")，又一座横跨[杭州湾的大桥](../Page/杭州湾.md "wikilink")。2008年12月14日正式开工建设，于2012年底建成通车。

## 旅游

  - 城市名片：观潮胜地、皮革之都、轮滑星城
  - 旅游特色：[海宁潮](../Page/海宁潮.md "wikilink")、[海宁中国皮革城](../Page/海宁中国皮革城.md "wikilink")、盐官古城
  - [全国重点文物保护单位](../Page/全国重点文物保护单位.md "wikilink")：[盐官海塘及](../Page/盐官海塘.md "wikilink")[海神庙](../Page/海神庙.md "wikilink")、[安国寺经幢](../Page/安国寺经幢.md "wikilink")、[王国维故居](../Page/王国维故居.md "wikilink")
  - [浙江省文物保护单位](../Page/浙江省文物保护单位.md "wikilink")：[郭家石桥遗址](../Page/郭家石桥遗址.md "wikilink")、[长安镇画像石墓](../Page/长安镇画像石墓.md "wikilink")、[惠力寺经幢](../Page/惠力寺经幢.md "wikilink")、[盛家埭遗址](../Page/盛家埭遗址.md "wikilink")、[陈阁老宅](../Page/陈阁老宅.md "wikilink")、[崔家场遗址](../Page/崔家场遗址.md "wikilink")、[荷叶地遗址](../Page/荷叶地遗址.md "wikilink")、[施家墩遗址](../Page/施家墩遗址.md "wikilink")、[达泽庙遗址](../Page/达泽庙遗址.md "wikilink")、[安澜园遗址](../Page/安澜园遗址.md "wikilink")、[长安闸](../Page/长安闸.md "wikilink")、[占鳌塔](../Page/占鳌塔.md "wikilink")、[衍芬草堂](../Page/衍芬草堂.md "wikilink")、[许村奉宪严禁盐枭扳害碑](../Page/许村奉宪严禁盐枭扳害碑.md "wikilink")、[张宗祥故居](../Page/张宗祥故居.md "wikilink")、[徐志摩旧居](../Page/徐志摩旧居.md "wikilink")

## 名人

  - [顾况](../Page/顾况.md "wikilink")：唐代诗人
  - [许远](../Page/许远.md "wikilink")：[开元年间进士](../Page/开元.md "wikilink")，[睢阳太守](../Page/睢阳.md "wikilink")，将军，[唐朝杭州](../Page/唐朝.md "wikilink")[盐官人](../Page/盐官.md "wikilink")
  - [朱淑真](../Page/朱淑真.md "wikilink")：[南宋女词人](../Page/南宋.md "wikilink")
  - [王国维](../Page/王国维.md "wikilink")：近代著名[学者](../Page/学者.md "wikilink")，[国学大师](../Page/国学.md "wikilink")
  - [李善兰](../Page/李善兰.md "wikilink")：清末[数学家](../Page/数学家.md "wikilink")
  - [蔣百里](../Page/蔣百里.md "wikilink")：近代[军事家](../Page/军事家.md "wikilink")
  - [蔣英](../Page/蔣英.md "wikilink"):
    [錢學森之妻](../Page/錢學森.md "wikilink")，演唱家
  - [陳世倌](../Page/陳世倌.md "wikilink")：清[乾隆朝重臣](../Page/乾隆.md "wikilink")
  - [谈迁](../Page/谈迁.md "wikilink")：明末[历史学家](../Page/历史学家.md "wikilink")
  - [穆旦](../Page/穆旦.md "wikilink")：原名查良铮，民国诗人、翻译家
  - [张宗祥](../Page/张宗祥.md "wikilink")：近现代[书画家](../Page/书画家.md "wikilink")
  - [徐志摩](../Page/徐志摩.md "wikilink")：近现代[诗人](../Page/诗人.md "wikilink")
  - [金庸](../Page/金庸.md "wikilink")：原名查良镛，现代[作家](../Page/作家.md "wikilink")
  - [陆宗舆](../Page/陆宗舆.md "wikilink")：民国[政治家](../Page/政治家.md "wikilink")
  - [郑晓沧](../Page/郑晓沧.md "wikilink")：现代[教育家](../Page/教育家.md "wikilink")
  - [史东山](../Page/史东山.md "wikilink")：原名史匡韶，近现代[电影艺术家](../Page/电影艺术家.md "wikilink")
  - [米谷](../Page/米谷.md "wikilink")：原名朱禄庆，现代[漫画家](../Page/漫画家.md "wikilink")
  - [查济民](../Page/查济民.md "wikilink")：当代[香港](../Page/香港.md "wikilink")[实业家](../Page/实业家.md "wikilink")
  - [沙可夫](../Page/沙可夫.md "wikilink")：原名陈维敏，现代教育家、[艺术家](../Page/艺术家.md "wikilink")
  - [章克标](../Page/章克标.md "wikilink")：现代作家
  - [印順長老](../Page/印順長老.md "wikilink")：俗名張鹿芹，近代著名的佛教大思想家，為台灣比丘界首位博士
  - [许国璋](../Page/许国璋.md "wikilink")：现代教育家
  - [查慎行](../Page/查慎行.md "wikilink")：詩人
  - [查繼佐](../Page/查繼佐.md "wikilink"):
    《[罪惟錄](../Page/罪惟錄.md "wikilink")》作者
  - [张效祥](../Page/张效祥.md "wikilink")：[计算机专家](../Page/计算机.md "wikilink")，中国科学院院士
  - [沈鸿](../Page/沈鸿.md "wikilink")：[机械工程学家](../Page/机械工程.md "wikilink")，中国科学院院士
  - [顾正澄](../Page/顾正澄.md "wikilink"): 物理学家

## 参考资料

<references />

[海宁市](../Category/海宁市.md "wikilink")
[市](../Category/嘉兴区县市.md "wikilink")
[嘉兴市](../Category/浙江省县级市.md "wikilink")
[H](../Category/东海沿海城市.md "wikilink")
[浙](../Category/中国中等城市.md "wikilink")
[浙](../Category/国家卫生城市.md "wikilink")
[Category:浙江省省级历史文化名城](../Category/浙江省省级历史文化名城.md "wikilink")
[5](../Category/全国文明城市.md "wikilink")

1.  <http://tjj.haining.gov.cn/tjsj/tjgb/201703/t20170321_620827.htm>
    2016年海宁市国民经济和社会发展统计公报