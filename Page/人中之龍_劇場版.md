《**人中之龍 劇場版**》（日文：，英文：*Like a Dragon*）是[PlayStation
2遊戲](../Page/PlayStation_2.md "wikilink")《[人中之龍](../Page/人中之龍.md "wikilink")》寫實影像化的作品，由《[鬼來電](../Page/鬼來電.md "wikilink")》及《[妖怪大戰爭](../Page/妖怪大戰爭.md "wikilink")》的電影導演[三池崇史擔任導演](../Page/三池崇史.md "wikilink")，日本於2007年3月3日在東映系電影院上映。

## 角色

  - 桐生一馬：[北村一輝](../Page/北村一輝.md "wikilink")
  - 真島吾朗：[岸谷五朗](../Page/岸谷五朗.md "wikilink")
  - 悟：[鹽谷瞬](../Page/鹽谷瞬.md "wikilink")
  - 唯：[沙耶子](../Page/沙耶子.md "wikilink")
  - 澤村遥：[夏緒](../Page/夏緒.md "wikilink")
  - 一輝：[加藤晴彦](../Page/加藤晴彦.md "wikilink")
  - 澤村由美/美月：[高岡早紀](../Page/高岡早紀.md "wikilink")
  - 野口刑事：[哀川翔](../Page/哀川翔.md "wikilink")
  - 朴：[孔侑](../Page/孔侑.md "wikilink")
  - 伊達真：[松重豊](../Page/松重豊.md "wikilink")
  - 錦山彰：[真木藏人](../Page/真木藏人.md "wikilink")
  - 神宮京平：[名越稔洋](../Page/名越稔洋.md "wikilink")
  - 風間新太郎：[鹽見三省](../Page/鹽見三省.md "wikilink")

## 製作人員

  - 導演：[三池崇史](../Page/三池崇史.md "wikilink")
  - 編劇：[十川誠志](../Page/十川誠志.md "wikilink")
  - 製作人：[名越稔洋](../Page/名越稔洋.md "wikilink")、[梅村宗宏](../Page/梅村宗宏.md "wikilink")
  - 製作：[SEGA](../Page/SEGA.md "wikilink")
  - 配給：[東映](../Page/東映.md "wikilink")

## 主題曲

  - [CRAZY KEN BAND](../Page/CRAZY_KEN_BAND.md "wikilink")

## 其他

由於三池導演認為「要將其他媒體題材拍成電影，就需要深入知道原作內容」，因此本作決定電影化後，他就立即購買[人中之龍的遊戲](../Page/人中之龍.md "wikilink")，並在三日不眠不休的情況下以「EASY」的難度破關，並藉此細細咀嚼消化整部遊戲的內容。

## 相關條目

  - 遊戲作品

<!-- end list -->

  - [人中之龍](../Page/人中之龍.md "wikilink")
  - [人中之龍2](../Page/人中之龍2.md "wikilink")
  - [人中之龍 登場\!](../Page/人中之龍_登場!.md "wikilink")
  - [人中之龍3](../Page/人中之龍3.md "wikilink")
  - [人中之龍4 傳說的繼承者](../Page/人中之龍4_傳說的繼承者.md "wikilink")
  - [人中之龍 OF THE END](../Page/人中之龍_OF_THE_END.md "wikilink")
  - [人中之龍5 夢 實踐者](../Page/人中之龍5_夢_實踐者.md "wikilink")

<!-- end list -->

  - 影像作品

<!-- end list -->

  - [人中之龍 〜序章〜](../Page/人中之龍_〜序章〜.md "wikilink")

<!-- end list -->

  -
    由三池擔任導演，[宮坂武志負責監製](../Page/宮坂武志.md "wikilink")。

## 外部連結

  - [人中之龍 電影版
    官方網站](https://web.archive.org/web/20091126021329/http://www.ryu-movie.com/)

  - [人中之龍 電影版 DVD網站](http://www.amuse-s-e.co.jp/ryu-dvd/)

[Category:日本電影作品](../Category/日本電影作品.md "wikilink")
[Category:2007年電影](../Category/2007年電影.md "wikilink")
[Category:日本極道](../Category/日本極道.md "wikilink")
[Category:人中之龍系列](../Category/人中之龍系列.md "wikilink")
[Category:新宿背景電影](../Category/新宿背景電影.md "wikilink")
[Category:三池崇史電影](../Category/三池崇史電影.md "wikilink")
[Category:歌舞伎町背景作品](../Category/歌舞伎町背景作品.md "wikilink")
[Category:电子游戏改编真人电影](../Category/电子游戏改编真人电影.md "wikilink")