**尼羅魚科**（學名*Kneriidae*）為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鼠鱚目的其中一科](../Page/鼠鱚目.md "wikilink")。

## 分類

**尼羅魚科**下分4個屬：

### 尼羅魚屬（*Cromeria*）

  -   - [尼羅魚](../Page/尼羅魚.md "wikilink")（*Cromeria nilotica*）
      - [西方尼羅魚](../Page/西方尼羅魚.md "wikilink")（*Cromeria occidentalis*）

### 油奈氏魚屬（*Grasseichthys*）

  -   - [嘉寶油奈氏魚](../Page/嘉寶油奈氏魚.md "wikilink")（*Grasseichthys
        gabonensis*）

### 克奈魚屬（*Kneria*）

  -   - [安哥拉克奈魚](../Page/安哥拉克奈魚.md "wikilink")（*Kneria angolensis*）
      - [安氏克奈魚](../Page/安氏克奈魚.md "wikilink")（*Kneria ansorgii*）
      - [大耳克奈魚](../Page/大耳克奈魚.md "wikilink")（*Kneria auriculata*）
      - [卡坦克奈魚](../Page/卡坦克奈魚.md "wikilink")（*Kneria katangae*）
      - [海氏克奈魚](../Page/海氏克奈魚.md "wikilink")（*Kneria maydelli*）
      - [少鱗克奈魚](../Page/少鱗克奈魚.md "wikilink")（*Kneria paucisquamata*）
      - [波氏克奈魚](../Page/波氏克奈魚.md "wikilink")（*Kneria polli*）
      - [魯氏克奈魚](../Page/魯氏克奈魚.md "wikilink")（*Kneria ruaha*）
      - [魯夸湖克奈魚](../Page/魯夸湖克奈魚.md "wikilink")（*Kneria rukwaensis*）
      - [肖氏克奈魚](../Page/肖氏克奈魚.md "wikilink")（*Kneria sjolandersi*）
      - [史氏克奈魚](../Page/史氏克奈魚.md "wikilink")（*Kneria stappersii*）
      - [烏魯克奈魚](../Page/烏魯克奈魚.md "wikilink")（*Kneria uluguru*）
      - [威氏克奈魚](../Page/威氏克奈魚.md "wikilink")（*Kneria wittei*）

### 副克奈氏魚屬（*Parakneria*）

  -   - [短副克奈氏魚](../Page/短副克奈氏魚.md "wikilink")（*Parakneria abbreviata*）
      - [金馬倫副克奈氏魚](../Page/金馬倫副克奈氏魚.md "wikilink")（*Parakneria
        cameronensis*）
      - [副克奈氏魚](../Page/副克奈氏魚.md "wikilink")（*Parakneria damasi*）
      - [福圖副克奈氏魚](../Page/福圖副克奈氏魚.md "wikilink")（*Parakneria fortuita*）
      - [野副克奈氏魚](../Page/野副克奈氏魚.md "wikilink")（*Parakneria kissi*）
      - [拉迪副克奈氏魚](../Page/拉迪副克奈氏魚.md "wikilink")（*Parakneria ladigesi*）
      - [勒非副克奈氏魚](../Page/勒非副克奈氏魚.md "wikilink")（*Parakneria lufirae*）
      - [大頭副克奈氏魚](../Page/大頭副克奈氏魚.md "wikilink")（*Parakneria malaissei*）
      - [雲斑副克奈氏魚](../Page/雲斑副克奈氏魚.md "wikilink")（*Parakneria marmorata*）
      - [莫桑比克副克奈氏魚](../Page/莫桑比克副克奈氏魚.md "wikilink")（*Parakneria
        mossambica*）
      - [斯氏副克奈氏魚](../Page/斯氏副克奈氏魚.md "wikilink")（*Parakneria spekii*）
      - [坦贊副克奈氏魚](../Page/坦贊副克奈氏魚.md "wikilink")（*Parakneria tanzaniae*）
      - [小眼副克奈氏魚](../Page/小眼副克奈氏魚.md "wikilink")（*Parakneria thysi*）
      - [維爾副克奈氏魚](../Page/維爾副克奈氏魚.md "wikilink")（*Parakneria vilhenae*）

[de:Ohrenfische](../Page/de:Ohrenfische.md "wikilink")

[\*](../Category/尼羅魚科.md "wikilink")