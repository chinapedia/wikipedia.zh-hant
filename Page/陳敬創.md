**陳敬創**，洋名: **Chan King
Chong**，，[廣東省](../Page/廣東省.md "wikilink")[中山縣人](../Page/中山縣.md "wikilink")，[香港](../Page/香港.md "wikilink")[九龍](../Page/九龍.md "wikilink")[深水埗長大](../Page/深水埗.md "wikilink")，[培正中學校友](../Page/培正中學.md "wikilink")。[香港中文大學](../Page/香港中文大學.md "wikilink")[地理](../Page/地理.md "wikilink")[學士](../Page/學士.md "wikilink")，[加拿大](../Page/加拿大.md "wikilink")[約克大學學過](../Page/約克大學.md "wikilink")[電影](../Page/電影.md "wikilink")。陳敬創[愛稱](../Page/花名.md "wikilink"):
**阿創、肥仔**，前[香港電台](../Page/香港電台.md "wikilink")[節目主持人](../Page/節目主持人.md "wikilink")。

## 簡歷

陳敬創在1993年加入[香港電台](../Page/香港電台.md "wikilink")，至2000年接任節目編導工作。期間曾主持《[晨光第一線](../Page/晨光第一線.md "wikilink")》、《[智慧公民](../Page/智慧公民.md "wikilink")》、《[知識會社](../Page/知識會社.md "wikilink")》、《[創KaPo夫](../Page/創KaPo夫.md "wikilink")》、《[創Ka童話](../Page/創Ka童話.md "wikilink")》等節目。

2008年，陳敬創因涉詐騙港台被捕。控罪指，他於2001年2月至2002年2月間製作《晨光第一線》期間，曾墊支報紙及嘉賓的茶點費；其後，他聽從上級吩咐以9A表格，以其母王玉芳曾撰稿的名義，領回41,300元節目開支。另外，他又曾為主持人[彭晴申領的士費](../Page/彭晴.md "wikilink")，串同她以彭母李慧雲名義申領49,600元費用，以及串謀前港台助理[廖倩怡支取](../Page/廖倩怡.md "wikilink")《[春風伴我行](../Page/春風伴我行.md "wikilink")》的3,000元演出酬勞。法官認為，陳敬創屬技術上犯錯，亦接納他只是錯誤地走捷徑申領費用，考慮到其正面背景，遂判他接受160小時[社會服務令](../Page/社會服務令.md "wikilink")\[1\]。

陳敬創在2008年10月與港台約滿後，未獲續約。後在2009年1月，親自向上訴庭申請上訴以推翻定罪，獲上訴庭受理\[2\]。最終，上訴庭只撤回其中1項串謀詐騙罪名，其餘11項罪名維持原判\[3\]。

## 作品

  - 《店小二的點心》作者：曾淑儀、陳敬創
    [1](https://web.archive.org/web/20080621220050/http://www.hvpoint.com/search.php?authurID=13)
  - 《[創KaPo夫](../Page/創KaPo夫.md "wikilink")》主題曲《[最難忘初戀六月天](../Page/最難忘初戀六月天.md "wikilink")》填詞
  - 《[情場妙女郎](../Page/情場妙女郎.md "wikilink")》主題曲《情愛就算是假》填詞、作曲，片尾曲《每天等著每一天》填詞

## 參考資料

## 外部參考

  - [2](http://www.rthk.org.hk/channel/radio2/r2/06_profile.htm)
  - [陳敬創 facebook](http://www.facebook.com/chankingchong)
  - [陳敬創----no air-time (YAHOO \!
    BLOG)](http://hk.myblog.yahoo.com/jw!mctSqSuRGBiyjnumKvsBCExFkJnWrzZ6wQNz)
  - [陳敬創----no air-time
    (Xuite)](http://blog.xuite.net/chongchan922/hkblog)
  - [創Ka童話](http://www.rthk.org.hk/rthk/radio2/ckstory/)
  - [香港電台第二台節目主持——陳敬創](http://www.rthk.org.hk/home/djprofiles/r2/r2_chankc.htm)
  - [香港電台DJ
    wallpaper](http://rthk27.rthk.org.hk/homepage/special/djwallpaper)
  - [陳敬創
    youtube頻道](https://www.youtube.com/channel/UCLTejtjGWExcsm5GNv-FNyQ)

[Category:前香港電台人物](../Category/前香港電台人物.md "wikilink")
[K](../Category/陳姓.md "wikilink")
[Category:香港廣播主持人](../Category/香港廣播主持人.md "wikilink")
[Category:香港中山人](../Category/香港中山人.md "wikilink")
[C](../Category/香港培正中學校友.md "wikilink")
[Category:香港中文大學校友](../Category/香港中文大學校友.md "wikilink")
[C](../Category/加拿大約克大學校友.md "wikilink")

1.
2.
3.