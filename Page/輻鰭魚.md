**輻鰭魚**又名**条鳍鱼**，为**辐鳍鱼总纲**（[學名](../Page/學名.md "wikilink")：）鱼类的通称，是一类[鰭呈](../Page/鰭.md "wikilink")[放射狀的](../Page/放射狀.md "wikilink")[硬骨魚](../Page/硬骨魚.md "wikilink")。輻鰭魚是[脊椎動物中種類最多的](../Page/脊椎動物.md "wikilink")，种数几乎占现存3万多种鱼类的99%，遍及[淡水及](../Page/淡水.md "wikilink")[海水環境](../Page/海水.md "wikilink")。

在不同的[分类系统中](../Page/分类系统.md "wikilink")，辐鳍鱼的[分类层级从](../Page/分类级别.md "wikilink")[下纲](../Page/下纲.md "wikilink")、[亚纲](../Page/亚纲.md "wikilink")、[纲到](../Page/纲.md "wikilink")[总纲不等](../Page/总纲.md "wikilink")。基本上，广义的辐鳍鱼（）包含了[腕鳍鱼](../Page/腕鳍鱼.md "wikilink")，而[狭义的辐鳍鱼](../Page/辐鳍鱼纲.md "wikilink")（）则成为腕鳍鱼的[姐妹群](../Page/姐妹群.md "wikilink")。

## 种系发生学

依照2017年《[硬骨鱼支序分类法](../Page/硬骨鱼支序分类法.md "wikilink")》，本总纲与其它[硬骨鱼的演化关系如下](../Page/硬骨鱼.md "wikilink")：

## 分類

傳統上，輻鰭魚分為三個類群：[軟骨硬鱗附類](../Page/軟骨硬鱗附類.md "wikilink")、[全骨附類和](../Page/全骨附類.md "wikilink")[真骨附類](../Page/真骨附類.md "wikilink")。一些形態學上的證據認為第二類為[併系群](../Page/併系群.md "wikilink")，所以應該廢除；然而，最近從完整粒線體基因中來分析DNA序列的研究卻傾向支持第二類的存在。幾乎所有現存的硬骨魚類皆為[真骨附類](../Page/真骨附類.md "wikilink")。

以下是輻鰭魚綱分類的列表，各階層的排列順序均依演化順序排列：

### [腕鰭魚綱](../Page/腕鰭魚綱.md "wikilink") Cladistia

  - [多鰭魚目](../Page/多鰭魚目.md "wikilink") Polypteriformes
  - [吉尔戴鱼目](../Page/吉尔戴鱼目.md "wikilink") Guildayichthyiformes †

### [輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink") Actinopteri

#### [軟骨硬鱗亞綱](../Page/軟骨硬鱗亞綱.md "wikilink") Chondrostei

  - [鱘形目](../Page/鱘形目.md "wikilink") Acipenseriformes
  - [盧加諾魚目](../Page/盧加諾魚目.md "wikilink") Luganoiiformes †
  - [古鳕目](../Page/古鳕目.md "wikilink") Palaeonisciformes †
  - [侧鳞鱼目](../Page/侧鳞鱼目.md "wikilink") Pholidopleuriformes †
  - [裂齿鱼目](../Page/裂齿鱼目.md "wikilink") Perleidiformes †
  - [肋鳞鱼目](../Page/肋鳞鱼目.md "wikilink") Peltopleuriformes †
  - [褶鳞鱼目](../Page/褶鳞鱼目.md "wikilink") Ptycholepiformes †
  - [龙鱼目](../Page/龙鱼目.md "wikilink") Saurichthyiformes †
  - [鳗鳕目](../Page/鳗鳕目.md "wikilink") Tarrasiiformes †
  - [硬齿鱼目](../Page/硬齿鱼目.md "wikilink") Pycnodontiformes †
  - [比耶鱼目](../Page/比耶鱼目.md "wikilink") Birgeriiformes †

#### [新鰭亞綱](../Page/新鰭亞綱.md "wikilink") Neopterygii

##### [全骨下綱](../Page/全骨下綱.md "wikilink") Holostei

  - [雀鱔目](../Page/雀鱔目.md "wikilink") Lepisosteiformes
  - [弓鰭魚目](../Page/弓鰭魚目.md "wikilink") Amiiformes
  - [预言鱼目](../Page/预言鱼目.md "wikilink") Ionoscopiformes †
  - [半椎鱼目](../Page/半椎鱼目.md "wikilink") Semionotiformes †

##### [真骨下綱](../Page/真骨下綱.md "wikilink") Teleostei

[銀龍001.JPG](https://zh.wikipedia.org/wiki/File:銀龍001.JPG "fig:銀龍001.JPG")\]\]

  - [薄鳞鱼目](../Page/薄鱗魚目.md "wikilink") Leptolepiformes †
  - [厚茎鱼目](../Page/厚茎鱼目.md "wikilink") Pachycormiformes †
  - [针吻鱼目](../Page/针吻鱼目.md "wikilink") Aspidorhynchiformes †
  - [叉鳞鱼目](../Page/叉鳞鱼目.md "wikilink") Pholidophoriformes †
  - Crossognathiformes †

###### [骨舌魚總目](../Page/骨舌魚總目.md "wikilink") Osteoglossomorpha

  - [骨舌魚目](../Page/骨舌魚目.md "wikilink") Osteoglossiformes
  - [月眼魚目](../Page/月眼魚目.md "wikilink") Hiodontiformes
  - [狼鰭魚目](../Page/狼鰭魚目.md "wikilink") Lycopteriformes †
  - [乞丐魚目](../Page/乞丐魚目.md "wikilink") Ichthyodectiformes †

###### [海鰱總目](../Page/海鰱總目.md "wikilink") Elopomorpha

  - [海鰱目](../Page/海鰱目.md "wikilink") Elopiformes
  - [北梭魚目](../Page/北梭魚目.md "wikilink") Albuliformes
  - [背棘鱼目](../Page/背棘鱼目.md "wikilink") Notacanthiformes
  - [鰻鱺目](../Page/鰻鱺目.md "wikilink") Anguilliformes
  - [囊鰓鰻目](../Page/囊鰓鰻目.md "wikilink") Saccopharyngiformes

###### [鯡形總目](../Page/鯡形總目.md "wikilink") Clupeomorpha

  - [鯡形目](../Page/鯡形目.md "wikilink") Clupeiformes
  - [埃笠姆鲱目](../Page/埃笠姆鲱目.md "wikilink") Ellimmichthyiformes †

###### [骨鰾總目](../Page/骨鰾總目.md "wikilink") Ostariophysi

  - [鼠鱚目](../Page/鼠鱚目.md "wikilink") Gonorynchiformes
  - [鯉形目](../Page/鯉形目.md "wikilink") Cypriniformes
  - [脂鯉目](../Page/脂鯉目.md "wikilink") Characiformes
  - [電鰻目](../Page/電鰻目.md "wikilink") Gymnotiformes
  - [鯰形目](../Page/鯰形目.md "wikilink") Siluriformes
  - [黑头鱼目](../Page/黑头鱼目.md "wikilink") Alepocephaliformes ？

###### [原棘鰭總目](../Page/原棘鰭總目.md "wikilink") Protacanthopterygii

  - [鮭形目](../Page/鮭形目.md "wikilink") Salmoniformes
  - [狗魚目](../Page/狗魚目.md "wikilink") Esociformes
  - [水珍魚目](../Page/水珍魚目.md "wikilink") Actinopterygii
  - [南乳鱼目](../Page/南乳魚科.md "wikilink") Galaxiiformes

######  Stenopterygii

  - [辮魚目](../Page/辮魚目.md "wikilink") Ateleopodiformes
  - [胡瓜魚目](../Page/胡瓜魚目.md "wikilink") Osmeriformes
  - [巨口魚目](../Page/巨口魚目.md "wikilink") Stomiiformes

###### [圓鱗總目](../Page/圓鱗總目.md "wikilink") Cyclosquamata

  - [仙女魚目](../Page/仙女魚目.md "wikilink") Aulopiformes

###### [燈籠魚總目](../Page/燈籠魚總目.md "wikilink") Scopelomorpha

  - [燈籠魚目](../Page/燈籠魚目.md "wikilink") Myctophiformes

###### [月魚總目](../Page/月魚總目.md "wikilink") Lampridiomorpha

  - [月魚目](../Page/月魚目.md "wikilink") Lampriformes

###### [鬚鰃總目](../Page/鬚鰃總目.md "wikilink") Polymyxiomorpha

  - [鬚鰃目](../Page/鬚鰃目.md "wikilink") Polymixiiformes

###### [副棘鰭總目](../Page/副棘鰭總目.md "wikilink") Paracanthopterygii

  - [鮭鱸目](../Page/鮭鱸目.md "wikilink") Percopsiformes
  - [蟾魚目](../Page/蟾魚目.md "wikilink") Batrachoidiformes
  - [鮟鱇目](../Page/鮟鱇目.md "wikilink") Lophiiformes
  - [鱈形目](../Page/鱈形目.md "wikilink") Gadiformes
  - [鼬魚目](../Page/鼬魚目.md "wikilink") Ophidiiformes

###### [棘鰭總目](../Page/棘鰭總目.md "wikilink") Acanthopterygii

  - [鯔形目](../Page/鯔形目.md "wikilink") Mugiliformes
  - [銀漢魚目](../Page/銀漢魚目.md "wikilink") Atheriniformes
  - [鶴鱵目](../Page/鶴鱵目.md "wikilink") Beloniformes
  - [鯉齒目](../Page/鯉齒目.md "wikilink") Cyprinodontiformes
  - [奇鯛目](../Page/奇鯛目.md "wikilink") Stephanoberyciformes
  - [鯨頭魚目](../Page/鯨頭魚目.md "wikilink") Cetomimiformes
  - [金眼鯛目](../Page/金眼鯛目.md "wikilink") Beryciformes
  - [背棘鱼目](../Page/背棘鱼目.md "wikilink") Notacanthiformes
  - [海魴目](../Page/海魴目.md "wikilink") Zeiformes
  - [刺魚目](../Page/刺魚目.md "wikilink") Gasterosteiformes
  - [合鰓目](../Page/合鰓目.md "wikilink") Synbranchiformes
  - [魨形目](../Page/魨形目.md "wikilink") Tetraodontiformes
  - [鰈形目](../Page/鰈形目.md "wikilink") Pleuronectiformes
  - [鮋形目](../Page/鮋形目.md "wikilink") Scorpaeniformes
  - [鱸形目](../Page/鱸形目.md "wikilink") Perciformes

注：[鱸形目是輻鰭魚綱中最多種的目](../Page/鱸形目.md "wikilink")，佔大約40%。

## 参考文献

## 外部連結

  - [NCBI分類學輻鰭魚綱入口](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?name=actinopterygii)
  - [臺灣魚類資料庫](http://fishdb.sinica.edu.tw/)
  - [延陵魚類志](http://www.ngensis.com/pscies.htm)
  - [FishBase](http://fishbase.sinica.edu.tw/search.php?lang=Chinese)

{{-}}

[辐鳍鱼总纲](../Category/辐鳍鱼总纲.md "wikilink")