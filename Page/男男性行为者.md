[Wiki-anal_missionary.png](https://zh.wikipedia.org/wiki/File:Wiki-anal_missionary.png "fig:Wiki-anal_missionary.png")
**男男性行為者**（英文：men who have sex with
men，缩写**MSM**，又称**男男性接触者**、**男男性行为人群**）是指与同性发生[性关系的男性](../Page/性关系.md "wikilink")，而不管他们自我认定为何种[性向](../Page/性取向.md "wikilink")。这个术语主要用于美国，用以划分出一个[艾滋病防治政策的關鍵群體](../Page/艾滋病.md "wikilink")。因为该术语通常只用于确定[性别的人士身上](../Page/性别.md "wikilink")，所以除非有证据显示性别非确定者发生类似的行为或者具有相同危险性的性行为，否则应该避免使用这个术语来描述[跨性别者](../Page/跨性别者.md "wikilink")、[变性者或](../Page/变性者.md "wikilink")[雌雄间性者](../Page/雌雄间性.md "wikilink")。

男男性行为不僅僅發生在同性戀群體，事實上，这种[境遇性性行為可能在包括同性戀](../Page/境遇性性行为.md "wikilink")、双性向、部分異性戀在内的人群中，[监狱](../Page/监狱.md "wikilink")\[1\]、[军队](../Page/军队.md "wikilink")、单一性别的[寄宿学校或者其他性别隔离的场所中出现](../Page/寄宿学校.md "wikilink")。甚至部分商業行為中以男性保健為招牌，實施男男性行為以促進盈利。[前列腺快感條目敘述了前列腺按摩在商業行為中實施現狀](../Page/前列腺快感.md "wikilink")。

男男性行為除愛撫、[口交](../Page/口交.md "wikilink")、[手交](../Page/手交.md "wikilink")、[肛交](../Page/肛交.md "wikilink")、[施虐與受虐以外](../Page/虐恋.md "wikilink")，还有一些引起性快感、達到高潮的性刺激行為，如男男手交及其各種演化形式，其中部分演化為[男男商業性行為](../Page/男男商業性行為.md "wikilink")。

## 男男性接触者包括：

  - 性活跃期的[男同性戀者](../Page/男同性戀者.md "wikilink")；
  - 与其他男性发生性行为的[雙性戀男性](../Page/雙性戀.md "wikilink")；
  - 为男性提供性服务的[男性性工作者](../Page/男妓.md "wikilink")；
  - 境遇性情況下与其他男性發生性行為的異性戀男性；
  - 與男性發生性行為的[跨性別男性](../Page/跨性別男性.md "wikilink")。

## 世界各地限制男男性行為的最低年齡

男性行為的合法年齡：

  - [中華民國](../Page/中華民國.md "wikilink")、[香港](../Page/香港.md "wikilink")：16歲以上；
  - [菲律賓](../Page/菲律賓.md "wikilink")、[西班牙](../Page/西班牙.md "wikilink")、[荷蘭](../Page/荷蘭.md "wikilink")、[阿根廷](../Page/阿根廷.md "wikilink")：12岁；
  - [越南](../Page/越南.md "wikilink")、[韓國](../Page/韓國.md "wikilink")：13岁；
  - [冰島](../Page/冰島.md "wikilink")、[德國](../Page/德國.md "wikilink")，[義大利](../Page/義大利.md "wikilink")、[奧地利](../Page/奧地利.md "wikilink")、[巴西](../Page/巴西.md "wikilink")、[加拿大](../Page/加拿大.md "wikilink")：14岁；
  - [丹麥](../Page/丹麥.md "wikilink")、[法國](../Page/法國.md "wikilink")、[捷克](../Page/捷克.md "wikilink")、[瑞典](../Page/瑞典.md "wikilink")、[波蘭](../Page/波蘭.md "wikilink")、[希臘](../Page/希臘.md "wikilink")：15岁；
  - [芬蘭](../Page/芬蘭.md "wikilink")、[挪威](../Page/挪威.md "wikilink")、[比利時](../Page/比利時.md "wikilink")、[葡萄牙](../Page/葡萄牙.md "wikilink")、[新西蘭](../Page/新西蘭.md "wikilink")：16岁；
  - [英國](../Page/英國.md "wikilink")：16歲，但[北愛爾蘭為](../Page/北愛爾蘭.md "wikilink")17歲；
  - [日本](../Page/日本.md "wikilink")：17岁；
  - [匈牙利](../Page/匈牙利.md "wikilink")：18岁；
  - [南非](../Page/南非.md "wikilink")：19岁；
  - [美國](../Page/美國.md "wikilink")：各州规定不同。

## 参见

  - [攻受](../Page/攻受.md "wikilink")
  - [男男商业性行为](../Page/男男商业性行为.md "wikilink")
  - [境遇性性行为](../Page/境遇性性行为.md "wikilink")
  - [女女性行为者](../Page/女女性行为者.md "wikilink")

## 脚注

[Category:性學](../Category/性學.md "wikilink")
[x](../Category/男男性行為.md "wikilink")
[Category:性取向和社会](../Category/性取向和社会.md "wikilink")

1.