[Black-tea.jpg](https://zh.wikipedia.org/wiki/File:Black-tea.jpg "fig:Black-tea.jpg")
**红茶**是一种全[发酵](../Page/发酵.md "wikilink")[茶](../Page/茶.md "wikilink")，是西方茶文化中的主要茶品。明朝時產於福建武夷山的[正山小種為紅茶鼻祖](../Page/正山小種.md "wikilink")\[1\]，1610年[荷兰人透過](../Page/荷兰人.md "wikilink")[印度尼西亚殖民地将小种红茶运往欧洲](../Page/印度尼西亚.md "wikilink")\[2\]開始了西方紅茶文化。红茶与绿茶不同，[绿茶会随着时间而失去味道](../Page/绿茶.md "wikilink")，但红茶能够保存相当长的时间而味道不變，因此红茶能适应长途运输，这也许是红茶传到西方的重要原因之一。所有紅茶都是由灌木（小树）山茶的叶子制成，目前分支的两个大類為小叶中华品种（C.
sinensis subsp.sinensis）和大叶阿萨姆品种（Anchimus subsp.assamica）。

红茶是经过采摘、[萎凋](../Page/萎凋.md "wikilink")、[揉捻](../Page/揉捻.md "wikilink")、发酵、干燥等步骤生产出来的；比[绿茶多了发酵的过程](../Page/绿茶.md "wikilink")。发酵是指茶叶在空气中氧化，发酵作用使得茶叶中的[茶多酚和](../Page/茶多酚.md "wikilink")[鞣质酸减少](../Page/鞣质.md "wikilink")，产生了[茶黄素](../Page/茶黄素.md "wikilink")、[茶红素等新的成分和醇类](../Page/茶红素.md "wikilink")、醛类、酮类、酯类等[芳香物质](../Page/芳香物质.md "wikilink")。因此，红茶的茶叶呈黑色，或黑色中参杂着嫩芽的橙黄色；茶汤呈深红色；香气扑鼻；由于少了苦涩味，因而味道更香甜、醇厚、回甘。

目前红茶的产地主要有[中国](../Page/中国.md "wikilink")、[斯里兰卡](../Page/斯里兰卡.md "wikilink")、[印度](../Page/印度.md "wikilink")、[肯尼亚](../Page/肯尼亚.md "wikilink")、[印度尼西亞等](../Page/印度尼西亞.md "wikilink")。

## 红茶的較著名品种

<table>
<thead>
<tr class="header">
<th><p>國別</p></th>
<th><p>茶</p></th>
<th><p>通稱</p></th>
<th><p>來源</p></th>
<th><p>內容</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/中國.md" title="wikilink">中國</a></p></td>
<td><p>功夫红茶</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>坦洋工夫</p></td>
<td><p><em>坦洋</em></p></td>
<td><p>福建福安市坦洋村</p></td>
<td><p>三大工夫茶之首</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>政和工夫</p></td>
<td><p><em>政和</em></p></td>
<td><p>福建政和县</p></td>
<td><p>三大工夫茶之一.轻微的蜂蜜味</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>白琳工夫</p></td>
<td><p><em>白琳</em></p></td>
<td><p>福建福鼎市白琳鎮</p></td>
<td><p>三大工夫茶之一.</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/立山小种.md" title="wikilink">立山小种</a></p></td>
<td><p>立山小种(Lapsang souchong)</p></td>
<td><p><em>正山小种</em></p></td>
<td><p>福建武夷山區</p></td>
<td><p>乾枯而发出强烈的烟熏味，歷史上的紅茶鼻祖。</p></td>
</tr>
<tr class="even">
<td><p>银骏眉 (Silver Steed Eyebrow)</p></td>
<td><p><em>小银眉</em></p></td>
<td><p>高一等的正山小种，目前市面稱立山小种的多為银骏眉。</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>金骏眉 (Golden Steed Eybrow)</p></td>
<td><p><em>小金眉</em></p></td>
<td><p>最高等而少見的正山小种。</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/祁门红茶.md" title="wikilink">祁门红茶</a></p></td>
<td><p>祁门工夫</p></td>
<td><p><em>祁门紅茶</em></p></td>
<td><p>安徽祁门茗洲村</p></td>
<td><p>祁门传统红茶。光绪年间創製，具有酒香和果味。</p></td>
</tr>
<tr class="odd">
<td><p>祁红香螺</p></td>
<td><p><em>红香螺</em></p></td>
<td><p>安徽省农业科学院採用国家级茶树良种培育而成，加上黄山松萝的制作工艺。</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>祁红毛峰</p></td>
<td><p><em>红毛峰</em></p></td>
<td><p>采制工艺更为精细，采用一芽一叶、一芽二叶进行全发酵，加上葆和堂独创的木质揉捻技术。</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>雲南大葉茶</p></td>
<td><p><em>云南红茶/滇红</em></p></td>
<td><p>云南</p></td>
<td><p>分黑芽和金芽兩種.</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>英德茶</p></td>
<td><p><em><a href="../Page/英德红茶.md" title="wikilink">英德红茶</a></em></p></td>
<td><p>廣東英德</p></td>
<td><p>可可與胡椒的混合口味. 茶色紅艷。</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>浙茶</p></td>
<td><p><em>九曲红梅</em></p></td>
<td><p>浙江杭州</p></td>
<td><p>具有光泽黑的紧凑鱼钩状叶子，茶水呈淡红色，回味悠长故名.</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/印度.md" title="wikilink">印度</a></p></td>
<td><p><a href="../Page/阿薩姆紅茶.md" title="wikilink">阿薩姆紅茶</a></p></td>
<td><p><em>Ôxôm cah</em> ()</p></td>
<td><p>印度<a href="../Page/阿薩姆邦.md" title="wikilink">阿薩姆邦</a></p></td>
<td><p>阿薩姆茶以濃稠、濃烈、麥芽香、清透鮮亮而出名。在歷史上阿薩姆是繼中國以後第二個商業紅茶栽種地區。[3]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/大吉嶺茶.md" title="wikilink">大吉嶺茶</a></p></td>
<td><p><em>Dārjiliṁ cā</em> ()</p></td>
<td><p>印度<a href="../Page/西孟加拉邦.md" title="wikilink">西孟加拉邦</a></p></td>
<td><p>蘇格蘭植物學家<a href="../Page/福鈞.md" title="wikilink">羅伯特·福瓊也於</a>1849年受東印度公司派遣，將武夷山茶移植於印度栽種而成.</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>坎格拉茶</p></td>
<td><p><em>Kāngada cāy</em> ()</p></td>
<td><p>印度喜馬偕爾邦<br />
<a href="../Page/坎格拉縣.md" title="wikilink">坎格拉縣</a></p></td>
<td><p>具有微小的刺激性味道和植物香气。[4][5]</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>木納茶</p></td>
<td><p><em>Mūnnār cāya</em> ()</p></td>
<td><p>印度喀拉拉邦<br />
伊都基縣木納村</p></td>
<td><p>这品种产生强烈的金黄色茶水，有清爽的一丝水果味。 聞起來有中等色调的香味，类似于麦芽饼干。[6]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>藍山紅茶</p></td>
<td><p><em>Nīlakiri tēnīr</em> ()</p></td>
<td><p>印度泰米爾納德邦<br />
<a href="../Page/尼爾吉利斯縣.md" title="wikilink">尼爾吉利斯縣</a></p></td>
<td><p>僅次於阿薩姆的第二大產茶區.茶味濃重劇烈。</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/台灣.md" title="wikilink">台灣</a></p></td>
<td><p>台灣紅茶</p></td>
<td><p><em>日月潭紅茶/台茶21號</em></p></td>
<td><p>台灣南投<a href="../Page/日月潭.md" title="wikilink">日月潭</a></p></td>
<td><p>蜂蜜色並帶有桂和薄荷味.</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/韓國.md" title="wikilink">韓國</a></p></td>
<td><p>雀舌茶</p></td>
<td><p><em>jaekseol-cha</em> ()</p></td>
<td><p>慶尚南道<br />
<a href="../Page/河東郡_(韓國).md" title="wikilink">河東郡 (韓國)</a></p></td>
<td><p>金黃色茶水略甜.[7]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/尼泊爾.md" title="wikilink">尼泊爾</a></p></td>
<td><p>尼泊爾茶</p></td>
<td><p><em>Nēpālī ciyā</em> ()</p></td>
<td><p>尼泊爾</p></td>
<td><p>類似大吉嶺茶，果香較重.</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/斯里蘭卡.md" title="wikilink">斯里蘭卡</a></p></td>
<td><p><a href="../Page/錫蘭高地紅茶.md" title="wikilink">錫蘭高地紅茶</a></p></td>
<td><p><em>Silōn tē</em> ()</p></td>
<td><p>斯里蘭卡</p></td>
<td><p>特徵是濃烈、香醇，因此常被拿來當早餐茶。適合泡奶茶。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/土耳其.md" title="wikilink">土耳其</a></p></td>
<td><p>里澤紅茶</p></td>
<td><p><em>Rize çayı</em></p></td>
<td><p>土耳其<a href="../Page/里澤省.md" title="wikilink">里澤省</a></p></td>
<td><p>酿造时是桃红色的颜色，传统上配甜菜糖飲用。</p></td>
</tr>
</tbody>
</table>

## 规格和等级

[缩略图](https://zh.wikipedia.org/wiki/File:Pure_Ceylon_Single_Estate_Black_Tea_in_Australia.jpg "fig:缩略图")

按照西方的分类，红茶根据茶叶的在茶树上的部位和完成后茶叶的形状分为不同规格：

  - 白毫（Pekoe 简称P）：白毫
  - 碎白毫（Broken Pekoe 简称BP）：切碎或不完整的白毫
  - 片茶（Fannings 简称F）：是指比碎白毫更小的细片。
  - 小种（Souchong 简称S）：小种茶
  - 茶粉（Dust
    简称D）：[茶粉或](../Page/茶粉.md "wikilink")[抹茶](../Page/抹茶.md "wikilink")
  - 切碎-撕裂-卷曲红茶（Crush Tear Curl 简称CTC）：

白毫根据采摘部位的不同，又可以分为不同的等级，等级主要有：

[`Black_tea_grading.jpg`](https://zh.wikipedia.org/wiki/File:Black_tea_grading.jpg "fig:Black_tea_grading.jpg")` `

  - 花橙白毫（Flowery Orange Pekoe 简称FOP）：茶枝最頂尖的新芽(芯芽)，非常罕見。因為通常新葉都會一對出現。
  - 橙白毫（Orange Pekoe 简称OP）：茶枝最頂起數的第二片葉。若新葉以一對形式出現，則兩片亦算是op。
  - 白毫（Pekoe 简称P）：茶枝最頂起數的第三片葉。葉片通常比較短。
  - 正小種（Pure Souchong 简称PS）：茶枝最頂起數的第四片葉。葉片通常短而闊
  - 小種（Souchong 简称S）：茶枝最頂起數的第五片葉。正山小种に使われる。葉片通常比較大。
  - 碎橙白毫（Broken Orange Peoke 简称BOP）：切碎了的橙白毫。
  - 碎白毫（Broken Pekoe 简称BP）：切碎了的白毫。
  - 碎正小種（Broken Pure Souchong 简称BPS）：切碎了的白毫小種。
  - 碎橙白毫片茶（Broken Orange Pekoe Fannings 简称BOPF）：切碎了的橙白毫細片。
  - 茶粉（Dust 简称D）：切得像粉一般細的茶葉。主要用來做茶包。

### 台灣的紅茶混種

台灣紅茶現齊備大葉種及小葉種，民國60年代起由中華民國農委會茶葉改良場進行混種，所得茶種包括：

  - [台茶八號](../Page/台茶八號.md "wikilink") （阿薩姆紅茶齋浦爾品系（Jaipuri）單株培育）
  - [台茶十二號](../Page/台茶12號.md "wikilink") （福建硬枝紅心與台農八號雜交）
  - [台茶十八號](../Page/台茶十八號.md "wikilink") （台灣野生山茶與緬甸大葉種雜交）
  - [台茶二十一號](../Page/台茶二十一號.md "wikilink") （祁門紅茶和印度坎格拉雜交）
  - [青心烏龍](../Page/青心烏龍.md "wikilink")、[青心大冇](../Page/青心大冇.md "wikilink")
    （清朝福建移栽的矮腳烏龍後經日本技師改良）
  - [台茶原生山茶](../Page/台灣山茶.md "wikilink")

## 饮用方法

[NCI_iced_tea.jpg](https://zh.wikipedia.org/wiki/File:NCI_iced_tea.jpg "fig:NCI_iced_tea.jpg")\]\]
除了单一品种的红茶以外，还有混合調配茶（blended tea）和混合调味茶（flavoured
tea）。混合茶是把不同品种红茶的搭配起来制成的。

混合调味茶是在红茶中加入水果、花、香草的香味制成的，如加入了[佛手柑香味的英国的](../Page/佛手柑.md "wikilink")[伯爵茶](../Page/伯爵茶.md "wikilink")，加入了[荔枝香味的中国岭南的](../Page/荔枝.md "wikilink")[荔枝红茶等等](../Page/荔枝红茶.md "wikilink")。
混合調配茶則是將不同產區的茶葉為基礎混合調配出的獨特風味的茶。最知名、歷史悠久的混合調配茶為英國早餐茶，各個品牌之口味有不同，適合搭配牛奶。

### 一般的沖飲器具

  - 沖泡：可以用[紫砂茶壶](../Page/紫砂壶.md "wikilink")、白瓷瓷盖碗及陶瓷茶壺等茶壺，也可以用咖啡杯等杯沖泡。
  - 沖泡輔助：量茶匙（度量茶葉的匙、濾茶匙（架在杯上濾去茶渣，有濾網狀與打洞型等）
  - 飲用：中式[茶杯](../Page/茶杯.md "wikilink")，歐式茶杯、咖啡杯、馬克杯等皆可。

### 清饮法

  - [熱紅茶](../Page/熱紅茶.md "wikilink")
  - [冰鎮紅茶](../Page/冰鎮紅茶.md "wikilink")
  - [冰滴紅茶](../Page/冰滴紅茶.md "wikilink")

### 混饮法

冲泡后可以按个人喜好，如加[牛奶](../Page/牛奶.md "wikilink")（即奶茶）和[砂糖饮用](../Page/砂糖.md "wikilink")。还可以加入[奶酪](../Page/奶酪.md "wikilink")、[肉桂](../Page/肉桂.md "wikilink")、[果醬](../Page/果醬.md "wikilink")、[蜂蜜等](../Page/蜂蜜.md "wikilink")[香料與](../Page/香料.md "wikilink")[调味品](../Page/调味品.md "wikilink")，還有在[奶茶中加入](../Page/奶茶.md "wikilink")[酒類的喝法](../Page/酒.md "wikilink")。

### 红茶饮品

红茶还衍生出了其他的引用方法和饮品。

  - [泡沫红茶](../Page/泡沫红茶.md "wikilink")：紅茶加上果糖糖漿後放在調酒器中和冰塊一起搖勻，在搖的過程中會產生細緻的泡沫，故稱為泡沫紅茶。亦有泡沫奶茶。
  - [檸檬茶](../Page/檸檬茶.md "wikilink")：红茶加上[檸檬汁](../Page/檸檬.md "wikilink")。
  - [古早味紅茶](../Page/古早味紅茶.md "wikilink")：台灣早期常見，於紅茶加入焙炒過的[決明子調味而成](../Page/決明子.md "wikilink")。因為焙炒過的決明子味道和咖啡有些相似，所以也俗稱[咖啡紅茶](../Page/咖啡紅茶.md "wikilink")。
  - [麥香紅茶](../Page/麥香紅茶.md "wikilink")：紅茶加入[麥茶與糖](../Page/麥茶.md "wikilink")。
  - [奶茶](../Page/奶茶.md "wikilink")：常為紅茶加上[牛奶](../Page/牛奶.md "wikilink")（或[奶精](../Page/奶精.md "wikilink")、[奶粉](../Page/奶粉.md "wikilink")），簡稱**奶紅**。有別於以[綠茶為基礎的](../Page/綠茶.md "wikilink")**奶綠**。
  - [印度奶茶](../Page/印度奶茶.md "wikilink")：奶茶加上[香辛料](../Page/香辛料.md "wikilink")
  - [珍珠奶茶](../Page/珍珠奶茶.md "wikilink")：奶茶加上[木薯粉圓](../Page/木薯.md "wikilink")
  - [絲襪奶茶](../Page/絲襪奶茶.md "wikilink")：錫蘭紅茶透過[英國傳入](../Page/英國.md "wikilink")[香港后](../Page/香港.md "wikilink")，發展成具香港地道特色的飲料：[絲襪奶茶及](../Page/絲襪奶茶.md "wikilink")[鴛鴦](../Page/鴛鴦_\(飲料\).md "wikilink")。
  - [水果茶](../Page/水果.md "wikilink")：紅茶與水果熬煮，或是茶同水果一起冷泡。

## 栽培和加工

### 栽培

适合[茶树栽培的地域需要满足下面的条件](../Page/茶树.md "wikilink")：

  - [热带或](../Page/热带.md "wikilink")[亚热带](../Page/亚热带.md "wikilink")。
  - 气温高的季节有足够的降水。
  - 弱酸性土壤。
  - 土壤的排水性良好。

在收获季节，在干燥的日子一日内温差较大能够够厚或具有芳香的优质茶叶。

此外，需要廉价的，高质量的劳动力也是很重要的，这样，从茶树的栽培到茶叶的采摘都有足够的人手照料。

### 加工

[Tea_bags.jpg](https://zh.wikipedia.org/wiki/File:Tea_bags.jpg "fig:Tea_bags.jpg")

1.  采摘后，新鲜的叶片首先要放到空气中[萎凋](../Page/萎凋.md "wikilink")。
2.  红茶将被按照两种方法之一进行加工：CTC方法（碾碎（Crush）、撕裂（Tear）、卷起（Curl））或者传统方法。CTC方法一般用于生产[袋茶的低质茶叶并且使用机器加工](../Page/袋茶.md "wikilink")。在使用中低质茶叶生产更好一些的成品方面，这种方法非常有效。传统方法是通过机器或手工完成的。手工处理用于高质量的茶叶。传统方法会根据不同的茶叶采用不同的手法，这种加工的方式最终产生许多鉴赏家所追求的高质量散茶。
3.  叶片在控制的[温度和](../Page/温度.md "wikilink")[湿度下进行](../Page/湿度.md "wikilink")[氧化](../Page/氧化.md "wikilink")。这一过程也称为发酵，虽然没有[发酵发生](../Page/发酵.md "wikilink")。氧化的程度决定着茶的质量。由于氧化在揉捻阶段就开始了，因此两个阶段之间的时间长短对于茶叶的质量来说也是至关紧要的因素。
4.  叶片将被干燥，以阻止氧化过程。
5.  叶片将按照大小（整叶，碎叶，茶末，茶粉）分成不同的“等级”，这种分类通常是使用筛子进行的。茶叶将进一步根据标准进行子等级分类。之後即可進行包裝。

## 西方茶文化

红茶是西方喜爱的茶，几百年的饮茶史使得西方发展出了不同于东方的茶文化。而最早紅茶起源自1675年的英國，在當時紅茶被稱做"治病的萬能藥"。1662年，英格蘭國王[查理二世和葡萄牙國王](../Page/查理二世_\(英格蘭\).md "wikilink")[若昂四世之女凱薩琳結婚](../Page/若昂四世_\(葡萄牙\).md "wikilink")，也順便把當時中國顯得珍貴的砂糖引入西方國家，宮廷內也開始有喝茶的習慣。自紅茶帶入西方後，在紅茶內加入方糖已經成為當時最奢侈的行為。在之後，紅茶也從宮廷括及到貴族階層。17-19世紀左右，紅茶被當時荷蘭籍東印度公司給壟斷，此龐大利益也創造出當時的大英帝國。\[8\]

### 英国茶文化

  - [上午茶](../Page/上午茶.md "wikilink")
  - [英式早餐](../Page/英式早餐.md "wikilink")
  - [下午茶](../Page/下午茶.md "wikilink")

## 國際品牌

國際行銷常見牌主要包括：

  - [立顿](../Page/立顿.md "wikilink")（Lipton）

  - [-{zh-hans:川宁;zh-hk:敦寧;zh-tw:唐寧;}-](../Page/川宁.md "wikilink")（Twinings）

  - [PG Tips](../Page/PG_Tips.md "wikilink")

  - [布魯克邦德](../Page/布魯克邦德.md "wikilink")（Brooke Bond）

  - [泰特莱](../Page/泰特莱.md "wikilink")（Tetley）

  - [瑋緻活](../Page/瑋緻活.md "wikilink")（Wedgwood，著名[陶瓷](../Page/陶瓷.md "wikilink")[茶具廠自有品牌](../Page/茶具.md "wikilink")）

  - [福南梅森](../Page/福南梅森.md "wikilink")（Fortnum &
    Mason，[伦敦著名百貨自有品牌](../Page/伦敦.md "wikilink")）

  - [威塔](../Page/威塔.md "wikilink")（Whittard，著名茶叶和[咖啡公司](../Page/咖啡.md "wikilink")）

  - [哈洛德](../Page/哈洛德.md "wikilink")（Harrods，[伦敦著名百貨公司自有品牌](../Page/伦敦.md "wikilink")）

  - [約克夏茶](../Page/約克夏茶.md "wikilink")（Yorkshire Tea）

  - （Lyons）

  - [瑪黑兄弟](../Page/瑪黑兄弟.md "wikilink")（Mariage Freres）

  - （Dilmah）

## 参考文献

## 外部链接

  - [维多利亚时期的红茶品牌](https://web.archive.org/web/20141225072521/http://casanouva.blog.163.com/blog/static/4486336220105112138834)

## 参见

  - [红茶菌](../Page/红茶菌.md "wikilink")

{{-}}

[Category:茶](../Category/茶.md "wikilink")
[红茶](../Category/红茶.md "wikilink")
[Category:中國發明](../Category/中國發明.md "wikilink")

1.  [正山小種改寫歷史](http://www.chinatimes.com/realtimenews/20170105002744-260404)
2.
3.
4.
5.  <https://www.teabox.com/blog/tea101/kangra-tea>
6.
7.
8.  <http://www.tea-a.gr.jp/knowledge/tea_history/index.html>