**鼠龍屬**（屬名：*Mussaurus*）意為「[老鼠](../Page/老鼠.md "wikilink")[蜥蜴](../Page/蜥蜴.md "wikilink")」，是種[草食性](../Page/草食性.md "wikilink")[原蜥腳類](../Page/原蜥腳類.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")。鼠龍是種非常早期的恐龍，生存年代為晚[三疊紀的](../Page/三疊紀.md "wikilink")[阿根廷南部](../Page/阿根廷.md "wikilink")，約2億1500萬年前。

鼠龍的化石來自於未成年個體與幼體，長度為20到37公分；科學家估計成年個體的身長可達3公尺，重量約70公斤\[1\]。鼠龍可能是種過渡物種，鼠龍在[科學分類法中的狀態是](../Page/科學分類法.md "wikilink")[蜥腳形亞目的分類未位屬](../Page/蜥腳形亞目.md "wikilink")，可能屬於原蜥腳下目，也可能屬於早期[蜥腳下目](../Page/蜥腳下目.md "wikilink")。

## 發現

鼠龍的化石是由古生物學家[何塞·波拿巴](../Page/何塞·波拿巴.md "wikilink")（Jose
Bonaparte）的挖掘團隊在1970年代所發現，出土於阿根廷的El
Tranquilo組\[2\]。除了幼體標本以外，還發現鼠龍的蛋巢、蛋殼，使科學家可以研究鼠龍與其他原蜥腳類的繁衍方式\[3\]。雖然標本的年齡小，但從四肢古[骨盆可以辨認出鼠龍是種原蜥腳類恐龍](../Page/骨盆.md "wikilink")\[4\]。

## 繁衍

古生物學家在[阿根廷發現了鼠龍的蛋巢](../Page/阿根廷.md "wikilink")、蛋殼、以及剛孵化幼體的化石，蛋巢內有多顆蛋。鼠龍的幼體身長為20到37公分，約為小型[蜥蜴的長度](../Page/蜥蜴.md "wikilink")。鼠龍的幼體化石有短頭部、短頸部、長尾巴、以及大型眼眶。幼年與成年個體的身體比例通常不同，成年鼠龍可能有較長口鼻部與頸部，外表較類似原蜥腳類恐龍。

## 大眾文化

鼠龍曾在[麥可·克萊頓](../Page/麥可·克萊頓.md "wikilink")（Michael
Crichton）的[科幻小說](../Page/科幻小說.md "wikilink")《[侏羅紀公園2：失落的世界](../Page/侏羅紀公園2：失落的世界.md "wikilink")》中短暫出現。

## 參考資料

<div class="references-small">

<references />

</div>

## 外部链接

  - [恐龙.NET](https://web.archive.org/web/20170527222928/http://dinosaur.net.cn/)——中国古生物门户网站。
  - [鼠龍](http://www.dinodata.org/index.php) DinoData

[Category:原蜥腳下目](../Category/原蜥腳下目.md "wikilink")
[Category:南美洲恐龍](../Category/南美洲恐龍.md "wikilink")
[Category:三疊紀恐龍](../Category/三疊紀恐龍.md "wikilink")

1.  Montague, Jeremy R. (2006). "Estimates of body size and geological
    time of origin for 612 dinosaur genera (Saurischia, Ornithischia)",
    *Florida Scientist*. 69(4): 243-257.

2.  "Mussaurus." In: Dodson, Peter & Britt, Brooks & Carpenter, Kenneth
    & Forster, Catherine A. & Gillette, David D. & Norell, Mark A. &
    Olshevsky, George & Parrish, J. Michael & Weishampel, David B. *The
    Age of Dinosaurs*. Publications International, LTD. p. 40. ISBN
    0-7853-0443-6.

3.
4.