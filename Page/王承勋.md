**王承勋**，[字](../Page/字.md "wikilink")**叔元**，[号](../Page/号.md "wikilink")**瑞楼**，[浙江](../Page/浙江.md "wikilink")[余姚](../Page/余姚.md "wikilink")（今[宁波](../Page/宁波.md "wikilink")[余姚市](../Page/余姚市.md "wikilink")）人，曾擔任[漕运总兵长达二十年](../Page/漕运总督.md "wikilink")，是明朝最后一任和任职时间最长的漕运总兵。

## 简介

王承勋是明朝[哲学家](../Page/哲学家.md "wikilink")、功臣[王守仁之孙](../Page/王守仁.md "wikilink")，[王正亿之子](../Page/王正亿.md "wikilink")，王正亿去世后，王承勋承袭新建伯[爵位](../Page/爵位.md "wikilink")；娶妻吴氏。

[万历二十年](../Page/万历.md "wikilink")（1592年）8月，王承勋充任[漕运总兵](../Page/漕运总督.md "wikilink")（或称漕运总兵官），镇守[淮安督理漕运](../Page/淮安.md "wikilink")（后与[李三才搭档](../Page/李三才.md "wikilink")），此后总督漕运长达二十年之久（参见《[明史](../Page/明史.md "wikilink")》）。

据清朝[乾隆年間成书的](../Page/乾隆.md "wikilink")《淮安府志·历代漕运》记载，在[明朝前中期](../Page/明朝.md "wikilink")，全国漕运事务上，“武尊文卑”，即漕运总兵的权力在漕运文官之上。[李三才到任后](../Page/李三才.md "wikilink")，逐渐扭转这一局势。李三才对王承勋“以气凌驾之”，王忍耐并“移坐其下”，致使此后“武尊文卑”传统逐渐改变，文官总理漕运事务，武官逐渐空闲。武官仅负责督运，最后连督运的任务也减免了。[万历四十年](../Page/万历.md "wikilink")（1612年），王承勋请辞回乡，明廷便撤销漕运总兵官一职，王承勋遂成为明朝最后一任漕运总兵。[1](http://szb.huaian.gov.cn/szb/jsp/content/content.jsp?articleId=50379)

王承勋的长子名叫王先进，无子嗣，故引发后来的新建伯爵位世袭之争，争讼旷日持久，长达数十年。

## 参见条目

  - [王守仁](../Page/王守仁.md "wikilink")
  - [余姚王氏](../Page/余姚王氏.md "wikilink")
  - [李三才](../Page/李三才.md "wikilink")

## 参考

  - 《明史》
  - [明清漕运总督与淮安](http://szb.huaian.gov.cn/szb/jsp/content/content.jsp?articleId=50379)
  - [在尘封的历史中走近王阳明家世](http://www.yynews.com.cn/gb/node2/whty/userobject1ai40145.html)
  - [第二章
    古代的淮安（下）](http://www.czzx.gov.cn/Lishi/Print.asp?ArticleID=763&Page=4)

[承勋](../Page/category:余姚王氏.md "wikilink")
[承勋](../Page/category:新建伯.md "wikilink")

[W王](../Category/明朝漕運總督.md "wikilink")
[W王](../Category/余姚人.md "wikilink")
[C](../Category/王姓.md "wikilink")
[Category:明朝伯爵](../Category/明朝伯爵.md "wikilink")