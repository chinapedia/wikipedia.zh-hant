## 大事记

  - 全球[人口數量到達](../Page/人口.md "wikilink")50億
  - 中华人民共和国[物价闯关](../Page/物价闯关.md "wikilink")

<!-- end list -->

  - [1月1日](../Page/1月1日.md "wikilink")——[北京](../Page/北京.md "wikilink")[天安門城樓正式向中外遊客開放](../Page/天安門城樓.md "wikilink")。
  - [1月1日](../Page/1月1日.md "wikilink")——[中华民国](../Page/中华民国.md "wikilink")[报禁正式完全解除](../Page/报禁.md "wikilink")，新闻媒体拥有[新闻自由](../Page/新闻自由.md "wikilink")。
  - [1月2日](../Page/1月2日.md "wikilink")——[廉政公署拘捕](../Page/廉政公署_\(香港\).md "wikilink")[香港聯合交易所多名高層](../Page/香港聯合交易所.md "wikilink")，包括前主席[李福兆](../Page/李福兆.md "wikilink")。
  - [1月13日](../Page/1月13日.md "wikilink")——[中華民國總統](../Page/中華民國總統.md "wikilink")[蔣經國去世](../Page/蔣經國.md "wikilink")，副總統[李登輝依照憲法在司法院長](../Page/李登輝.md "wikilink")[林洋港監誓下繼任總統](../Page/林洋港.md "wikilink")。

<!-- end list -->

  - [2月25日](../Page/2月25日.md "wikilink")——在[大韓民國](../Page/大韓民國.md "wikilink")，[盧泰愚就任總統](../Page/盧泰愚.md "wikilink")，展開五年單一任期。

<!-- end list -->

  - [3月5日](../Page/3月5日.md "wikilink")——在[台灣股市飆漲期間發行量曾高達](../Page/台灣.md "wikilink")40餘萬份的《[中時晚報](../Page/中時晚報.md "wikilink")》創刊。
  - [3月10日](../Page/3月10日.md "wikilink")——中國第一名[試管嬰兒在北京醫科大學第三醫院出生](../Page/試管嬰兒.md "wikilink")，是一名女嬰，重3.9公斤。
  - [3月14日](../Page/3月14日.md "wikilink")——[中华人民共和国与](../Page/中华人民共和国.md "wikilink")[越南社会主义共和国在](../Page/越南.md "wikilink")[南中国海](../Page/南中国海.md "wikilink")[赤瓜礁发生](../Page/赤瓜礁.md "wikilink")[海战](../Page/赤瓜礁海戰.md "wikilink")。
  - [3月24日](../Page/3月24日.md "wikilink")——[上海市](../Page/上海市.md "wikilink")[匡巷站发生](../Page/匡巷站.md "wikilink")[旅客列车相撞重大事故](../Page/1988年沪杭铁路列车相撞事故.md "wikilink")，导致29人死亡，其中28人为日本游客。

<!-- end list -->

  - [3月24日](../Page/3月24日.md "wikilink")-[4月10日](../Page/4月10日.md "wikilink")——七屆[全國政协一次会议举行](../Page/中国人民政治协商会议全国委员会.md "wikilink")，选举产生：政協第七屆全國委員會主席：[李先念](../Page/李先念.md "wikilink")；秘书长[周绍铮](../Page/周绍铮.md "wikilink")。
  - [3月25日](../Page/3月25日.md "wikilink")-[4月13日](../Page/4月13日.md "wikilink")——七屆[全國人大一次会议举行](../Page/全国人民代表大会.md "wikilink")，会议通过[赵紫阳关于请辞](../Page/赵紫阳.md "wikilink")[国务院总理一职的决定](../Page/中华人民共和国国务院总理.md "wikilink")，4月8日选举产生：
    [國家主席](../Page/中华人民共和国主席.md "wikilink")：[楊尚昆](../Page/杨尚昆.md "wikilink")；[國家副主席](../Page/中华人民共和国副主席.md "wikilink")：[王震](../Page/王震.md "wikilink")；第七屆[全國人大常委會委員長](../Page/全国人民代表大会常务委员会.md "wikilink")：[萬里](../Page/万里.md "wikilink")；[中央軍事委員會主席](../Page/中华人民共和国中央军事委员会.md "wikilink")：[鄧小平](../Page/邓小平.md "wikilink")；[國務院總理](../Page/中华人民共和国国务院总理.md "wikilink")：[李鵬](../Page/李鹏.md "wikilink")。
  - [4月10日晚上](../Page/4月10日.md "wikilink")——[第七届香港电影金像奖在香港演艺学院歌剧院举行](../Page/第7屆香港電影金像獎.md "wikilink")
  - [4月13日](../Page/4月13日.md "wikilink")——七屆全國人大一次會議通過關於設立[海南省的決定及建立](../Page/海南省.md "wikilink")[海南經濟特區的決議](../Page/海南省.md "wikilink")。
  - [4月17日](../Page/4月17日.md "wikilink")——[民主進步黨四一七決議文](../Page/民主進步黨.md "wikilink")：全國黨員代表大會決議，主張台灣國際主權獨立，不屬於以北京為首都之中華人民共和國；並通過「四個如果」決議文：如果國共片面和談、如果國民黨出賣台灣人民利益、如果中共統一台灣、如果國民黨不實施真正的民主憲政，則民進黨主張台灣獨立。
  - [4月28日](../Page/4月28日.md "wikilink")——[阿羅哈航空243號班機事故](../Page/阿羅哈航空243號班機事故.md "wikilink")

<!-- end list -->

  - [5月1日](../Page/5月1日.md "wikilink")——[台灣鐵路管理局的火車駕駛以集體休假名義發動](../Page/台灣鐵路管理局.md "wikilink")[大罷工](../Page/台灣鐵路管理局司機員罷工事件.md "wikilink")。
  - [5月8日](../Page/5月8日.md "wikilink")——[密特朗在第二輪投票再次當選](../Page/密特朗.md "wikilink")[法國總統](../Page/法國總統.md "wikilink")。
  - [5月14日](../Page/5月14日.md "wikilink")——全世界最長的超級油輪[諾克·耐維斯號在行經](../Page/諾克·耐維斯號.md "wikilink")[霍尔木兹海峡時](../Page/霍尔木兹海峡.md "wikilink")，遭[伊拉克戰機以](../Page/伊拉克.md "wikilink")[飛魚式反艦飛彈擊沉](../Page/飞鱼反舰导弹.md "wikilink")。
  - [5月15日](../Page/5月15日.md "wikilink")——[蘇聯開始從](../Page/苏联.md "wikilink")[阿富汗撤军](../Page/阿富汗.md "wikilink")。
  - [5月20日](../Page/5月20日.md "wikilink")——[中華民國爆發](../Page/中華民國.md "wikilink")[520事件](../Page/520事件.md "wikilink")，解嚴以來示威遊行活動衍生的最激烈衝突。「雲林縣農民權益促進會」發起農民遊行，抗議政府開放外國果菜、火雞肉進口，使農民生存權益受損，來自雲林、嘉義等十個縣市農民五千人，五月二十日集結台北街頭遊行抗議，先後在[立法院](../Page/立法院.md "wikilink")、城中分局等地與警方發生激烈衝突，警民共數百人受傷。
  - [5月30日](../Page/5月30日.md "wikilink")——中共中央决定创办《[求是](../Page/求是.md "wikilink")》杂志。

<!-- end list -->

  - [6月16日](../Page/6月16日.md "wikilink")——[香港政府開始對抵港的](../Page/港英政府.md "wikilink")[越南船民實施](../Page/越南船民.md "wikilink")「[甄別政策](../Page/甄別政策.md "wikilink")」。

<!-- end list -->

  - [7月2日](../Page/7月2日.md "wikilink")——《[临沂广播影视报](../Page/临沂广播影视报.md "wikilink")》创刊，4升4版，铅字印刷。
  - [7月3日](../Page/7月3日.md "wikilink")——[伊朗航空655號班機從](../Page/伊朗航空655號班機空難.md "wikilink")[伊朗的Bandar](../Page/伊朗.md "wikilink")
    Abbas飛往[阿拉伯聯合酋長國的](../Page/阿拉伯联合酋长国.md "wikilink")[杜拜](../Page/杜拜.md "wikilink")，在[波斯灣上空的國際航道被](../Page/波斯湾.md "wikilink")[美國海軍軍艦](../Page/美國海軍.md "wikilink")[文森號](../Page/文森號.md "wikilink")（USS
    Vincennes）誤當成伊朗空軍的[F-14雄貓式戰鬥機而遭飛彈擊落](../Page/F-14雄貓式戰鬥機.md "wikilink")，機上的274名旅客及16名機組員全數罹難。

<!-- end list -->

  - [8月8日](../Page/8月8日.md "wikilink")——[緬甸民主運動展開](../Page/8888民主運動.md "wikilink")。
  - [8月20日](../Page/8月20日.md "wikilink")——[两伊战争结束](../Page/两伊战争.md "wikilink")。
  - [8月22日](../Page/8月22日.md "wikilink")——[南非正式同](../Page/南非.md "wikilink")[安哥拉](../Page/安哥拉.md "wikilink")、[古巴签订停火协议](../Page/古巴.md "wikilink")，结束了长达13年的[安哥拉内战](../Page/安哥拉内战.md "wikilink")。
  - [8月28日](../Page/8月28日.md "wikilink")——[拉姆斯泰因航展事故](../Page/拉姆斯泰因航展事故.md "wikilink")，3名飛行員和67名觀眾死亡。
  - [8月31日](../Page/8月31日.md "wikilink")——一架從廣州飛抵香港[啟德機場的](../Page/啟德機場.md "wikilink")[中國民航](../Page/中国民用航空局.md "wikilink")[301號班機在降落時墮海](../Page/中國民航301號班機空難.md "wikilink")，釀成7死14傷。

<!-- end list -->

  - 9月──取消23年的[中國人民解放軍軍銜制度恢復](../Page/中国人民解放军军衔.md "wikilink")。
  - [9月17日](../Page/9月17日.md "wikilink")——[第24届奥林匹克运动会在](../Page/1988年夏季奥林匹克运动会.md "wikilink")[韓國](../Page/大韩民国.md "wikilink")[汉城](../Page/首爾.md "wikilink")（今首爾）开幕。
  - [9月18日](../Page/9月18日.md "wikilink")——連接[屯門及](../Page/屯門.md "wikilink")[元朗的](../Page/元朗.md "wikilink")[香港輕鐵第一期正式通車](../Page/香港輕鐵.md "wikilink")。
  - [9月18日](../Page/9月18日.md "wikilink")——緬甸軍政府強硬鎮壓抗議者。一個星期之內，約有1000名學生、僧侶和兒童被殺。
  - [9月24日](../Page/9月24日.md "wikilink")——[中華民國九二四證所稅事件](../Page/中華民國.md "wikilink")，中華民國證券史上最慘重的波段跌幅開啟。財政部長郭婉蓉週六中午，宣布1989年元旦起恢復課徵證所稅，出售股票超過三百萬元且有所得者，應納入綜所稅申報。隔天為假期過後首日交易，股市無量下跌19天，共計下跌3174.45點，跌幅37%。
  - [9月24日](../Page/9月24日.md "wikilink")——[緬甸的](../Page/缅甸.md "wikilink")[昂山素姬創立反對派政黨](../Page/翁山蘇姬.md "wikilink")[全國民主聯盟](../Page/全國民主聯盟.md "wikilink")。
  - [9月26日](../Page/9月26日.md "wikilink")——奪得[漢城奧運會男子](../Page/漢城奧運會.md "wikilink")100米短跑金牌的加拿大田徑運動員[賓·莊遜被驗出服用興奮劑](../Page/賓·莊遜.md "wikilink")，被褫奪金牌資格，翌日更被罰停賽兩年。
  - [9月29日](../Page/9月29日.md "wikilink")——[中國进行了首枚](../Page/中華人民共和國.md "wikilink")[中子弹实弹爆炸试验](../Page/中子弹.md "wikilink")，中国成为继[美国](../Page/美国.md "wikilink")、[法国](../Page/法国.md "wikilink")、[苏联后第四个拥有中子弹技术的国家](../Page/苏联.md "wikilink")。

<!-- end list -->

  - [11月15日](../Page/11月15日.md "wikilink")——[巴勒斯坦國在](../Page/巴勒斯坦国.md "wikilink")[阿爾及利亞首都](../Page/阿尔及利亚.md "wikilink")[阿爾及爾宣佈成立](../Page/阿爾及爾.md "wikilink")

<!-- end list -->

  - [12月1日](../Page/12月1日.md "wikilink")——[日本](../Page/日本.md "wikilink")[JR](../Page/JR.md "wikilink")[京葉線](../Page/京葉線.md "wikilink")[舞濱站啟用](../Page/舞濱站.md "wikilink")。
  - [12月1日](../Page/12月1日.md "wikilink")——中華人民共和國外交部長[錢其琛訪問蘇聯](../Page/錢其琛.md "wikilink")，中蘇關係解凍。
  - [12月7日](../Page/12月7日.md "wikilink")——前蘇聯[亞美尼亞北部發生](../Page/亞美尼亞.md "wikilink")[強烈地震](../Page/亞美尼亞大地震.md "wikilink")，2萬5千人死亡。
  - [12月21日](../Page/12月21日.md "wikilink")——[泛美航空103航班遇上炸彈襲擊](../Page/泛美航空103航班.md "wikilink")，在[蘇格蘭邊境小鎮](../Page/蘇格蘭.md "wikilink")[洛克比上空爆炸](../Page/洛克比.md "wikilink")，270人罹難。

## 出生

### 1月

  - [1月4日](../Page/1月4日.md "wikilink")——[釋小龍](../Page/释小龙.md "wikilink")，[中國内地](../Page/中國.md "wikilink")[演員](../Page/演員.md "wikilink")。
  - [1月7日](../Page/1月7日.md "wikilink")——[洪詩涵](../Page/洪詩涵_\(藝人\).md "wikilink")，[台灣](../Page/台灣.md "wikilink")[女艺人](../Page/女艺人.md "wikilink")，前《[我愛黑澀會](../Page/我愛黑澀會.md "wikilink")》[美眉](../Page/黑澀會美眉.md "wikilink")、現任[Popu
    Lady成員](../Page/Popu_Lady.md "wikilink")**洪詩**。
  - [1月9日](../Page/1月9日.md "wikilink")——[李海娜](../Page/李海娜.md "wikilink")，泰國藝人。
  - [1月15日](../Page/1月15日.md "wikilink")——[金閔俊](../Page/金閔俊.md "wikilink")，[韓國歌手](../Page/韓國.md "wikilink")、組合[2PM成員](../Page/2PM.md "wikilink")**Jun.K**。
  - [1月22日](../Page/1月22日.md "wikilink")——[格雷格·奥登](../Page/格雷格·奥登.md "wikilink")，美国著名篮球运动员。
  - [1月27日](../Page/1月27日.md "wikilink")——[林柏宏](../Page/林柏宏.md "wikilink")，台湾演员

### 2月

  - [2月1日](../Page/2月1日.md "wikilink")——[東出昌大](../Page/東出昌大.md "wikilink")，[日本](../Page/日本.md "wikilink")[演員](../Page/演員.md "wikilink")。
  - [2月3日](../Page/2月3日.md "wikilink")——[曺圭賢](../Page/圭賢.md "wikilink")，[韓國著名](../Page/韓國.md "wikilink")[男艺人](../Page/男艺人.md "wikilink")，[韓國男子團體](../Page/韓國.md "wikilink")[Super
    Junior團員](../Page/Super_Junior.md "wikilink")**圭賢**。
  - [2月3日](../Page/2月3日.md "wikilink")——[許展榮](../Page/許展榮.md "wikilink")，[台灣著名](../Page/台灣.md "wikilink")[網路紅人](../Page/網路紅人.md "wikilink")，[台灣網路團體](../Page/台灣.md "wikilink")[這群人TGOP團員](../Page/這群人TGOP.md "wikilink")**展榮**。
  - [2月3日](../Page/2月3日.md "wikilink")——[許展瑞](../Page/許展瑞.md "wikilink")，[台灣著名](../Page/台灣.md "wikilink")[網路紅人](../Page/網路紅人.md "wikilink")，[台灣網路團體](../Page/台灣.md "wikilink")[這群人TGOP團員](../Page/這群人TGOP.md "wikilink")**展瑞**。
  - [2月7日](../Page/2月7日.md "wikilink")——[李準
    (藝人)](../Page/李準_\(藝人\).md "wikilink")，韓國艺人。
  - 2月7日——[加護亞依](../Page/加護亞依.md "wikilink")，[日本](../Page/日本.md "wikilink")[歌手](../Page/歌手.md "wikilink")。
  - [2月8日](../Page/2月8日.md "wikilink")——[佐佐木希](../Page/佐佐木希.md "wikilink")，[日本](../Page/日本.md "wikilink")[演員](../Page/演員.md "wikilink")。
  - [2月11日](../Page/2月11日.md "wikilink")——[李純](../Page/李純_\(歌手\).md "wikilink")，[湖南](../Page/湖南.md "wikilink")[歌手](../Page/歌手.md "wikilink")。
  - [2月13日](../Page/2月13日.md "wikilink")——[林更新](../Page/林更新.md "wikilink")，中国内地男演员
  - [2月15日](../Page/2月15日.md "wikilink")——[草野博紀](../Page/草野博紀.md "wikilink")，[日本艺人](../Page/日本.md "wikilink")。
  - [2月16日](../Page/2月16日.md "wikilink")——[金秀賢](../Page/金秀賢_\(男演員\).md "wikilink")，韓國艺人。
  - [2月16日](../Page/2月16日.md "wikilink")——[张继科](../Page/张继科.md "wikilink")，[中國](../Page/中國.md "wikilink")[乒乓球運動員](../Page/乒乓球.md "wikilink")。
  - [2月18日](../Page/2月18日.md "wikilink")——[沈昌珉](../Page/沈昌珉.md "wikilink")，[韓國著名](../Page/韓國.md "wikilink")[男艺人](../Page/男艺人.md "wikilink")，[韓國男子團體](../Page/韓國.md "wikilink")[東方神起團員](../Page/東方神起.md "wikilink")**最強昌珉**。
  - [2月19日](../Page/2月19日.md "wikilink")——[入野自由](../Page/入野自由.md "wikilink")，日本配音員。
  - [2月20日](../Page/2月20日.md "wikilink")——[蕾哈娜](../Page/蕾哈娜.md "wikilink")，[巴貝多女歌手](../Page/巴貝多.md "wikilink")。
  - [2月25日](../Page/2月25日.md "wikilink")——[TASTY](../Page/TASTY.md "wikilink")，韓國雙胞胎團體，在中國出生。

### 3月

  - [3月4日](../Page/3月4日.md "wikilink")——[陳曼青](../Page/陳曼青.md "wikilink")，[台灣女歌手](../Page/台灣.md "wikilink")、街頭藝人。
  - [3月4日](../Page/3月4日.md "wikilink")——[陳韋利](../Page/陳韋利.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[模特兒](../Page/模特兒.md "wikilink")。
  - [3月5日](../Page/3月5日.md "wikilink")——[施幸余](../Page/施幸余.md "wikilink")，[香港游泳運動員](../Page/香港.md "wikilink")。
  - [3月11日](../Page/3月11日.md "wikilink")——[高以愛](../Page/高以愛.md "wikilink")，[台灣女歌手](../Page/台灣.md "wikilink")。
  - [3月12日](../Page/3月12日.md "wikilink")——[王啸坤](../Page/王啸坤.md "wikilink")，[中国内地](../Page/中国.md "wikilink")[歌手](../Page/歌手.md "wikilink")。
  - [3月14日](../Page/3月14日.md "wikilink")——[马思纯](../Page/马思纯.md "wikilink")，[中国内地女演员](../Page/中国.md "wikilink")

### 4月

  - [4月1日](../Page/4月1日.md "wikilink")——[丁海寅](../Page/丁海寅.md "wikilink")，[韓國](../Page/韓國.md "wikilink")[男演員](../Page/男演員.md "wikilink")。
  - [4月7日](../Page/4月7日.md "wikilink")——[蘇晏霈](../Page/蘇晏霈.md "wikilink")，台灣演員
  - [4月7日](../Page/4月7日.md "wikilink")——[梁証嘉](../Page/梁証嘉.md "wikilink")，[香港](../Page/香港.md "wikilink")[演员](../Page/演员.md "wikilink")。
  - [4月9日](../Page/4月9日.md "wikilink")——[UIE](../Page/UIE.md "wikilink")，[南韓歌手](../Page/南韓.md "wikilink")，演員。
  - [4月11日](../Page/4月11日.md "wikilink")——[詹子晴](../Page/詹子晴.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[女藝人](../Page/女藝人.md "wikilink")，前[黑Girl成員](../Page/黑Girl.md "wikilink")**丫頭**。
  - [4月16日](../Page/4月16日.md "wikilink")——[朱一龙](../Page/朱一龙.md "wikilink")，中国演員
  - [4月17日](../Page/4月17日.md "wikilink")——[森田貴寛](../Page/森田貴寛.md "wikilink")，[日本搖滾樂團](../Page/日本.md "wikilink")
    [ONE OK ROCK](../Page/ONE_OK_ROCK.md "wikilink") 主唱。
  - [4月19日](../Page/4月19日.md "wikilink")——[小嶋陽菜](../Page/小嶋陽菜.md "wikilink")，[日本艺人](../Page/日本.md "wikilink")。
  - [4月23日](../Page/4月23日.md "wikilink")——[嚴爵](../Page/嚴爵.md "wikilink")，[台灣歌手](../Page/台灣.md "wikilink")，創作人。
  - [4月26日](../Page/4月26日.md "wikilink")——[賴鴻誠](../Page/賴鴻誠.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[棒球](../Page/棒球.md "wikilink")[運動員](../Page/運動員.md "wikilink")，[富邦悍將](../Page/富邦悍將.md "wikilink")[投手](../Page/投手.md "wikilink")。
  - [4月28日](../Page/4月28日.md "wikilink")——[胡安·馬塔](../Page/桑·馬達.md "wikilink")，[西班牙](../Page/西班牙.md "wikilink")[足球](../Page/足球.md "wikilink")[運動員](../Page/運動員.md "wikilink")。
  - [4月29日](../Page/4月29日.md "wikilink")——[許廷鏗](../Page/許廷鏗.md "wikilink")，[香港](../Page/香港.md "wikilink")[歌手](../Page/歌手.md "wikilink")。

### 5月

  - [5月5日](../Page/5月5日.md "wikilink")——[愛黛兒](../Page/愛黛兒.md "wikilink")，[英國](../Page/英國.md "wikilink")[歌手](../Page/歌手.md "wikilink")。
  - [5月6日](../Page/5月6日.md "wikilink")——[亚历克西斯·阿金萨](../Page/亚历克西斯·阿金萨.md "wikilink")，法国著名篮球运动员。
  - [5月16日](../Page/5月16日.md "wikilink")——[-{于}-朦胧](../Page/于朦胧.md "wikilink")，[中国](../Page/中国.md "wikilink")[歌手](../Page/歌手.md "wikilink")[导演](../Page/导演.md "wikilink")。
  - [5月18日](../Page/5月18日.md "wikilink")——[董永裴](../Page/太陽_\(歌手\).md "wikilink")，韓國組合[BIGBANG成員](../Page/BIGBANG.md "wikilink")**太陽**。
  - [5月18日](../Page/5月18日.md "wikilink")——[瀨戶康史](../Page/瀨戶康史.md "wikilink")，[日本](../Page/日本.md "wikilink")[演員](../Page/演員.md "wikilink")。
  - [5月21日](../Page/5月21日.md "wikilink")——[朴奎利](../Page/朴奎利.md "wikilink")，[韓國](../Page/韓國.md "wikilink")[女藝人](../Page/女藝人.md "wikilink")，團體[Kara成員](../Page/Kara.md "wikilink")**奎利**。
  - [5月29日](../Page/5月29日.md "wikilink")——[程菲](../Page/程菲.md "wikilink")，[中國](../Page/中國.md "wikilink")[體操](../Page/體操.md "wikilink")[運動員](../Page/運動員.md "wikilink")。
  - [5月29日](../Page/5月29日.md "wikilink")——[柳下大](../Page/柳下大.md "wikilink")，[日本](../Page/日本.md "wikilink")[演員](../Page/演員.md "wikilink")。

### 6月

  - [6月1日](../Page/6月1日.md "wikilink")——[玉置成實](../Page/玉置成實.md "wikilink")，[日本](../Page/日本.md "wikilink")[歌手](../Page/歌手.md "wikilink")。
  - [6月3日](../Page/6月3日.md "wikilink")——[三浦翔平](../Page/三浦翔平.md "wikilink")，[日本](../Page/日本.md "wikilink")[演員](../Page/演員.md "wikilink")。
  - [6月11日](../Page/6月11日.md "wikilink")——[新垣結衣](../Page/新垣結衣.md "wikilink")，[日本](../Page/日本.md "wikilink")[演员](../Page/演员.md "wikilink")、[歌手](../Page/歌手.md "wikilink")、[模特兒](../Page/模特兒.md "wikilink")。
  - [6月13日](../Page/6月13日.md "wikilink")——[古屋敬多](../Page/古屋敬多.md "wikilink")，[日本](../Page/日本.md "wikilink")[歌手](../Page/歌手.md "wikilink")。
  - [6月13日](../Page/6月13日.md "wikilink")——[生田龍聖](../Page/生田龍聖.md "wikilink")，[日本新聞](../Page/日本.md "wikilink")[主播](../Page/主播.md "wikilink")。
  - [6月15日](../Page/6月15日.md "wikilink")——[于朦胧](../Page/于朦胧.md "wikilink")，[中國](../Page/中國.md "wikilink")[歌手](../Page/歌手.md "wikilink")、[演員](../Page/演員.md "wikilink")、[模特](../Page/模特.md "wikilink")。
  - [6月16日](../Page/6月16日.md "wikilink")——[G.Soul](../Page/G.Soul.md "wikilink")，[韓國](../Page/韓國.md "wikilink")[歌手](../Page/歌手.md "wikilink")。
  - [6月20日](../Page/6月20日.md "wikilink")——[May
    J.](../Page/May_J..md "wikilink")，[日本](../Page/日本.md "wikilink")[歌手](../Page/歌手.md "wikilink")。
  - [6月22日](../Page/6月22日.md "wikilink")——[欧米·卡斯比](../Page/欧米·卡斯比.md "wikilink")，以色列著名篮球运动员。
  - [6月23日](../Page/6月23日.md "wikilink")——[梁洛施](../Page/梁洛施.md "wikilink")，[香港](../Page/香港.md "wikilink")[演员](../Page/演员.md "wikilink")。
  - [6月24日](../Page/6月24日.md "wikilink")——[Nichkhun Buck
    Horvejkul](../Page/Nichkhun.md "wikilink")，[韓國](../Page/韓國.md "wikilink")[男藝人](../Page/男藝人.md "wikilink")，[韓國男子團體](../Page/韓國.md "wikilink")[2PM成員](../Page/2PM.md "wikilink")**Nichkhun（尼坤）**。
  - [6月24日](../Page/6月24日.md "wikilink")——[李曼筠](../Page/李曼筠.md "wikilink")，[香港](../Page/香港.md "wikilink")[模特兒](../Page/模特兒.md "wikilink")、[演員](../Page/演員.md "wikilink")。
  - [6月26日](../Page/6月26日.md "wikilink")——[中西里菜](../Page/中西里菜.md "wikilink")，[日本](../Page/日本.md "wikilink")[歌手](../Page/歌手.md "wikilink")。
  - [6月26日](../Page/6月26日.md "wikilink")——[加樂](../Page/鄧加樂.md "wikilink")，[臺灣流行](../Page/臺灣.md "wikilink")[男子演唱組合](../Page/男子音樂組合.md "wikilink")[GTM成員之一](../Page/GTM.md "wikilink")
  - [6月28日](../Page/6月28日.md "wikilink")——[濱田岳](../Page/濱田岳.md "wikilink")，[日本](../Page/日本.md "wikilink")[演員](../Page/演員.md "wikilink")
    /
    [林師傑](../Page/林師傑.md "wikilink")，[香港歌手](../Page/香港歌手.md "wikilink")，[演員](../Page/演員.md "wikilink")。
  - [6月30日](../Page/6月30日.md "wikilink")——[郭雪芙](../Page/郭雪芙.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[女藝人](../Page/女藝人.md "wikilink")，女子組合[Dream
    Girls成員](../Page/Dream_Girls.md "wikilink")。

### 7月

  - [7月1日](../Page/7月1日.md "wikilink")——[黃得生](../Page/黃得生.md "wikilink")，[香港](../Page/香港.md "wikilink")[演員](../Page/演員.md "wikilink")。
  - [7月11日](../Page/7月11日.md "wikilink")——[裕美](../Page/裕美.md "wikilink")，[香港](../Page/香港.md "wikilink")[歌手](../Page/歌手.md "wikilink")。
  - [7月17日](../Page/7月17日.md "wikilink")——[郭跃](../Page/郭跃.md "wikilink")，[中国内地女子](../Page/中国.md "wikilink")[乒乓球运动员](../Page/乒乓球.md "wikilink")。
  - [7月21日](../Page/7月21日.md "wikilink")——[德安德鲁·乔丹](../Page/德安德鲁·乔丹.md "wikilink")，美国著名篮球运动员。
  - [7月22日](../Page/7月22日.md "wikilink")——[吉高由里子](../Page/吉高由里子.md "wikilink")，[日本](../Page/日本.md "wikilink")[演員](../Page/演員.md "wikilink")。
  - [7月24日](../Page/7月24日.md "wikilink")——[韓昇延](../Page/韓昇延.md "wikilink")，[韓國](../Page/韓國.md "wikilink")[歌手團體](../Page/歌手.md "wikilink")[KARA](../Page/KARA.md "wikilink")。
  - [7月26日](../Page/7月26日.md "wikilink")——[秋元才加](../Page/秋元才加.md "wikilink")，日本前[AKB48成員](../Page/AKB48.md "wikilink")、艺人。
  - [7月28日](../Page/7月28日.md "wikilink")——[韓雨潔](../Page/韓雨潔.md "wikilink")，[台灣女演員](../Page/台灣.md "wikilink")。
  - [7月29日](../Page/7月29日.md "wikilink")——[黃姵嘉](../Page/黃姵嘉.md "wikilink")，[台灣女演員](../Page/台灣.md "wikilink")。
  - [7月30日](../Page/7月30日.md "wikilink")——[朴世榮](../Page/朴世榮.md "wikilink")，[韓國女演員](../Page/韓國.md "wikilink")。

### 8月

  - [8月1日](../Page/8月1日.md "wikilink")——[王若琳](../Page/王若琳.md "wikilink")，[台灣](../Page/台灣.md "wikilink")、[歌手](../Page/歌手.md "wikilink")。
  - [8月2日](../Page/8月2日.md "wikilink")——[周湯豪](../Page/周湯豪.md "wikilink")，[台灣](../Page/台灣.md "wikilink")、[歌手](../Page/歌手.md "wikilink")。
  - [8月6日](../Page/8月6日.md "wikilink")——[窪田正孝](../Page/洼田正孝.md "wikilink")，[日本](../Page/日本.md "wikilink")[演员](../Page/演员.md "wikilink")。
  - [8月8日](../Page/8月8日.md "wikilink")——[劉品言](../Page/劉品言.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[演员](../Page/演员.md "wikilink")、[歌手](../Page/歌手.md "wikilink")。
  - [8月8日](../Page/8月8日.md "wikilink")——[陈志忠](../Page/陈志忠_\(马来西亚\).md "wikilink")，[马来西亚](../Page/马来西亚.md "wikilink")[青年领袖](../Page/青年领袖.md "wikilink")、[企业家](../Page/企业家.md "wikilink")
  - [8月8日](../Page/8月8日.md "wikilink")——[达尼罗·加里纳利](../Page/达尼罗·加里纳利.md "wikilink")，意大利著名篮球运动员。
  - [8月17日](../Page/8月17日.md "wikilink")——[戶田惠梨香](../Page/戶田惠梨香.md "wikilink")，[日本](../Page/日本.md "wikilink")[演员](../Page/演员.md "wikilink")。
  - [8月18日](../Page/8月18日.md "wikilink")——[G-Dragon](../Page/G-Dragon.md "wikilink")，[韓國](../Page/韓國.md "wikilink")[歌手團體](../Page/歌手.md "wikilink")[BIGBANG](../Page/BIGBANG.md "wikilink")。
  - [8月19日](../Page/8月19日.md "wikilink")——[馬詩婷](../Page/馬詩婷.md "wikilink")，[台灣女歌手](../Page/台灣.md "wikilink")，藝名**夏宇童（舊藝名：小米）**。
  - [8月19日](../Page/8月19日.md "wikilink")——[蘇暐淇](../Page/蘇暐淇.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[演員](../Page/演員.md "wikilink")、[模特兒](../Page/模特兒.md "wikilink")。
  - [8月20日](../Page/8月20日.md "wikilink")——[鍵本輝](../Page/鍵本輝.md "wikilink")，[日本](../Page/日本.md "wikilink")[歌手](../Page/歌手.md "wikilink")。
  - [8月23日](../Page/8月23日.md "wikilink")——[林書豪](../Page/林書豪.md "wikilink")，[美國](../Page/美國.md "wikilink")[籃球員](../Page/籃球員.md "wikilink")。
  - [8月24日](../Page/8月24日.md "wikilink")——[魯伯特·葛林](../Page/魯伯特·葛林.md "wikilink")，[英國男演員](../Page/英國.md "wikilink")
    / [张若昀](../Page/张若昀.md "wikilink")，内地男演员。
  - [8月25日](../Page/8月25日.md "wikilink")——[黄光熙](../Page/黃光熙.md "wikilink")，男歌手，[韩国团体](../Page/韩国.md "wikilink")[帝国之子团员](../Page/ZE:A.md "wikilink")。
  - [8月29日](../Page/8月29日.md "wikilink")——[方語昕](../Page/方語昕.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[歌手](../Page/歌手.md "wikilink")、[演員](../Page/演員.md "wikilink")。

### 9月

  - [9月2日](../Page/9月2日.md "wikilink")——[加藤慶祐](../Page/加藤慶祐.md "wikilink")，[日本藝人](../Page/日本.md "wikilink")。
  - [9月14日](../Page/9月14日.md "wikilink")——[陈凯旋](../Page/陈凯旋_\(歌手\).md "wikilink")，马来西亚艺人
  - [9月20日](../Page/9月20日.md "wikilink")——[大本彩乃](../Page/大本彩乃.md "wikilink")，[日本歌手](../Page/日本.md "wikilink")。
  - [9月21日](../Page/9月21日.md "wikilink")——[林哲瑄](../Page/林哲瑄.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[棒球](../Page/棒球.md "wikilink")[運動員](../Page/運動員.md "wikilink")，[富邦悍將](../Page/富邦悍將.md "wikilink")[中外野手](../Page/中外野手.md "wikilink")。
  - [9月21日](../Page/9月21日.md "wikilink")——[鄺兆昌](../Page/鄺兆昌.md "wikilink")，香港音乐人。
  - [9月21日](../Page/9月21日.md "wikilink")——[比拉瓦爾·布托·扎爾達裡](../Page/比拉瓦尔·布托·扎尔达里.md "wikilink")，[巴基斯坦政治人物](../Page/巴基斯坦.md "wikilink")。
  - [9月23日](../Page/9月23日.md "wikilink")——[木村了](../Page/木村了.md "wikilink")，[日本演員](../Page/日本.md "wikilink")。
  - [9月27日](../Page/9月27日.md "wikilink")——[何傲兒](../Page/何傲兒.md "wikilink")，[香港演員](../Page/香港.md "wikilink")。
  - [9月29日](../Page/9月29日.md "wikilink")——[凯文·杜兰特](../Page/凯文·杜兰特.md "wikilink")，美国著名篮球明星，最年轻的得分王。

### 10月

  - [10月4日](../Page/10月4日.md "wikilink")——[金婑斌](../Page/金婑斌.md "wikilink")，[韓國](../Page/韓國.md "wikilink")[歌手](../Page/歌手.md "wikilink")[Wonder
    Girls成員](../Page/Wonder_Girls.md "wikilink")。
  - [10月6日](../Page/10月6日.md "wikilink")——[堀北真希](../Page/堀北真希.md "wikilink")，[日本](../Page/日本.md "wikilink")[演員](../Page/演員.md "wikilink")。
  - [10月11日](../Page/10月11日.md "wikilink")——[泉里香](../Page/泉里香.md "wikilink")，日本演員
  - [10月16日](../Page/10月16日.md "wikilink")——[簡嫚書](../Page/簡嫚書.md "wikilink")，[台灣演員](../Page/台灣.md "wikilink")。
  - [10月17日](../Page/10月17日.md "wikilink")——[大島優子](../Page/大島優子.md "wikilink")，日本前[AKB48成員](../Page/AKB48.md "wikilink")、艺人
  - [10月17日](../Page/10月17日.md "wikilink")——[松坂桃李](../Page/松坂桃李.md "wikilink")，日本[模特兒](../Page/模特兒.md "wikilink")、演員
  - [10月20日](../Page/10月20日.md "wikilink")——[马龙
    (乒乓球运动员)](../Page/马龙_\(乒乓球运动员\).md "wikilink")，中国内地乒乓球运动员。
  - [10月20日](../Page/10月20日.md "wikilink")——[新垣里沙](../Page/新垣里沙.md "wikilink")，日本歌手。
  - [10月21日](../Page/10月21日.md "wikilink")——[劉欣宜](../Page/劉欣宜.md "wikilink")，[香港模特兒](../Page/香港.md "wikilink")。
  - [10月22日](../Page/10月22日.md "wikilink")——[張書豪](../Page/張書豪.md "wikilink")，台灣演員。
  - [10月25日](../Page/10月25日.md "wikilink")——[郭嚴文](../Page/郭嚴文.md "wikilink")，台灣[棒球](../Page/棒球.md "wikilink")[Lamigo桃猿隊選手](../Page/Lamigo桃猿.md "wikilink")。
  - [10月28日](../Page/10月28日.md "wikilink")——[菜菜緒](../Page/菜菜緒.md "wikilink")，日本模特兒、演員。
  - [10月31日](../Page/10月31日.md "wikilink")——[邱鋒澤](../Page/邱鋒澤.md "wikilink")，[新加坡](../Page/新加坡.md "wikilink")[男歌手](../Page/男歌手.md "wikilink")、[男演員](../Page/男演員.md "wikilink")。

### 11月

  - [11月1日](../Page/11月1日.md "wikilink")——[林雨宣](../Page/林雨宣.md "wikilink")，台湾演员。
  - [11月1日](../Page/11月1日.md "wikilink")——[福原愛](../Page/福原愛.md "wikilink")，[日本](../Page/日本.md "wikilink")[乒乓球選手](../Page/乒乓球.md "wikilink")。
  - [11月1日](../Page/11月1日.md "wikilink")——[陳俊秀](../Page/陳俊秀.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[棒球](../Page/棒球.md "wikilink")[Lamigo桃猿隊選手](../Page/Lamigo桃猿.md "wikilink")。
  - [11月5日](../Page/11月5日.md "wikilink")——[山葵](../Page/山葵.md "wikilink")，[日本](../Page/日本.md "wikilink")[和樂器樂團鼓手](../Page/和樂器樂團.md "wikilink")
  - [11月6日](../Page/11月6日.md "wikilink")——[艾瑪·史東](../Page/艾瑪·史東.md "wikilink")，[美國女](../Page/美國.md "wikilink")[演員](../Page/演員.md "wikilink")。
  - [11月6日](../Page/11月6日.md "wikilink")——[楊小黎](../Page/楊小黎.md "wikilink")，[台灣女](../Page/台灣.md "wikilink")[演員](../Page/演員.md "wikilink")，早期為童星。
  - [11月12日](../Page/11月12日.md "wikilink")——[陳以桐](../Page/陳以桐.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[歌手](../Page/歌手.md "wikilink")。
  - [11月17日](../Page/11月17日.md "wikilink")——[曾之喬](../Page/曾之喬.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[演员](../Page/演员.md "wikilink")、[歌手](../Page/歌手.md "wikilink")。
  - [11月22日](../Page/11月22日.md "wikilink")——[任容萱](../Page/任容萱.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[演员](../Page/演员.md "wikilink")、[歌手](../Page/歌手.md "wikilink")。
  - [11月26日](../Page/11月26日.md "wikilink")——[小林优美](../Page/小林優美_\(模特兒\).md "wikilink")，[日本](../Page/日本.md "wikilink")[模特儿](../Page/模特儿.md "wikilink")
    |
    [與真司郎](../Page/與真司郎.md "wikilink")，[日本](../Page/日本.md "wikilink")[歌手](../Page/歌手.md "wikilink")。

### 12月

  - [12月1日](../Page/12月1日.md "wikilink")——[任雄宰](../Page/時完.md "wikilink")，[韩国](../Page/韩国.md "wikilink")[歌手](../Page/歌手.md "wikilink")、[演员](../Page/演员.md "wikilink")。
  - [12月6日](../Page/12月6日.md "wikilink")
    [島崎信長](../Page/島崎信長.md "wikilink")，[日本](../Page/日本.md "wikilink")[男性](../Page/男性.md "wikilink")[聲優](../Page/聲優.md "wikilink")
  - [12月7日](../Page/12月7日.md "wikilink")——[山下亨](../Page/山下亨.md "wikilink")，[日本搖滾樂團](../Page/日本.md "wikilink")[ONE
    OK ROCK](../Page/ONE_OK_ROCK.md "wikilink") 吉他手。
  - [12月10日](../Page/12月10日.md "wikilink")——[賴文飛](../Page/賴文飛.md "wikilink")，香港足球運動員
  - [12月12日](../Page/12月12日.md "wikilink")——[咸𤨒晶](../Page/咸𤨒晶.md "wikilink")，[韓國](../Page/韓國.md "wikilink")[女藝人](../Page/女藝人.md "wikilink")，[韓國女子團體](../Page/韓國.md "wikilink")[T-ara成員](../Page/T-ara.md "wikilink")**𤨒晶**。
  - [12月14日](../Page/12月14日.md "wikilink")——[尼古拉斯·巴图姆](../Page/尼古拉·巴通姆.md "wikilink")，法国著名篮球运动员。
  - [12月14日](../Page/12月14日.md "wikilink")——[凡妮莎·哈金斯](../Page/凡妮莎·哈金斯.md "wikilink")，美國演員。
  - [12月16日](../Page/12月16日.md "wikilink")——[朴敘俊](../Page/朴敘俊.md "wikilink")，[韓國](../Page/韓國.md "wikilink")[男演員](../Page/男演員.md "wikilink")
  - [12月20日](../Page/12月20日.md "wikilink")——[山下翔央](../Page/山下翔央.md "wikilink")，[日本艺人](../Page/日本.md "wikilink")。
  - [12月23日](../Page/12月23日.md "wikilink")——[龜井繪里](../Page/龜井繪里.md "wikilink")，[日本](../Page/日本.md "wikilink")[歌手](../Page/歌手.md "wikilink")。
  - [12月21日](../Page/12月21日.md "wikilink")——[狄以達](../Page/狄以達.md "wikilink")，[香港](../Page/香港.md "wikilink")[歌手](../Page/歌手.md "wikilink")。
  - [12月23日](../Page/12月23日.md "wikilink")——[樫野有香](../Page/樫野有香.md "wikilink")，[日本艺人](../Page/日本.md "wikilink")。
  - [12月27日](../Page/12月27日.md "wikilink")——[玉澤演](../Page/玉澤演.md "wikilink")，[韓國](../Page/韓國.md "wikilink")[演員](../Page/演員.md "wikilink")、[歌手](../Page/歌手.md "wikilink")
    [2PM成員](../Page/2PM.md "wikilink")
  - [12月27日](../Page/12月27日.md "wikilink")——[娄艺潇](../Page/娄艺潇.md "wikilink")，[中国内地](../Page/中国.md "wikilink")[演員](../Page/演員.md "wikilink")，
  - [12月29日](../Page/12月29日.md "wikilink")——[文詠珊](../Page/文詠珊.md "wikilink")，[香港](../Page/香港.md "wikilink")[模特兒](../Page/模特兒.md "wikilink")、[演員](../Page/演員.md "wikilink")。

## 逝世

## 诺贝尔奖

  - [物理](../Page/诺贝尔物理学奖.md "wikilink")：[傑克·施泰因貝格爾](../Page/杰克·施泰因贝格尔.md "wikilink")、[利昂·萊德曼](../Page/利昂·萊德曼.md "wikilink")、[梅爾文·施瓦茨](../Page/梅尔文·施瓦茨.md "wikilink")
  - [化学](../Page/诺贝尔化学奖.md "wikilink")：[約翰·戴森霍費爾](../Page/约翰·戴森霍费尔.md "wikilink")、[羅伯特·胡貝爾](../Page/羅伯特·胡貝爾.md "wikilink")、[哈特穆特·米歇爾](../Page/哈特穆特·米歇尔.md "wikilink")
  - [生理和医学](../Page/诺贝尔医学奖.md "wikilink")：[喬治·H·希欽斯](../Page/喬治·H·希欽斯.md "wikilink")、[詹姆士·W·布拉克](../Page/詹姆士·W·布拉克.md "wikilink")、[格特魯德·B·埃利恩](../Page/格特魯德·B·埃利恩.md "wikilink")
  - [文学](../Page/诺贝尔文学奖.md "wikilink")：[納吉布·馬哈福茲](../Page/納吉布·馬哈福茲.md "wikilink")
  - [和平](../Page/诺贝尔和平奖.md "wikilink")：[聯合國維持和平部隊](../Page/聯合國維持和平部隊.md "wikilink")
  - [经济](../Page/诺贝尔经济学奖.md "wikilink")：[莫里斯·阿萊](../Page/莫里斯·阿莱.md "wikilink")

[\*](../Category/1988年.md "wikilink")
[8年](../Category/1980年代.md "wikilink")
[8](../Category/20世纪各年.md "wikilink")