**許書揚**（）是一名企業家與專欄作家，畢業於[臺灣科技大學](../Page/臺灣科技大學.md "wikilink")\[1\]，擁有美國[南伊利諾州立大學電腦科學碩士](../Page/南伊利諾州立大學.md "wikilink")。

## 簡介

許書揚出生於[新北市](../Page/新北市.md "wikilink")，父親來自[福建省](../Page/福建省.md "wikilink")[晉江縣](../Page/晉江縣.md "wikilink")，早年隨兄長旅居菲律賓，後來響應政府號召，加入[黃埔軍校](../Page/黃埔軍校.md "wikilink")，軍旅生涯近二十年。退役後，定居[台灣](../Page/台灣.md "wikilink")。許母是家庭主婦，出生於[台北市](../Page/台北市.md "wikilink")。許為父母的第三個兒女，上有兩位姊姊。

許書揚大學時受父親鼓舞，前往美國就讀，在分不清[電腦科學與](../Page/電腦科學.md "wikilink")[計算機工程的情形下](../Page/計算機工程.md "wikilink")，選擇了有獎學金的計算機科學\[2\]。

許書揚擔任天下Cheers雜誌、[工商時報及管理雜誌等平面媒體的專欄作家](../Page/工商時報.md "wikilink")。他擁有美國電腦科學[碩士](../Page/碩士.md "wikilink")，卻涉足[人力資源管理領域](../Page/人力資源管理.md "wikilink")。

## 學歷

  - [美國](../Page/美國.md "wikilink")[南伊利諾大學](../Page/南伊利諾大學.md "wikilink")
    [電腦科學](../Page/電腦科學.md "wikilink")[碩士](../Page/碩士.md "wikilink")（1988）
  - [國立台灣科技大學](../Page/國立台灣科技大學.md "wikilink")
    工業管理[學士](../Page/學士.md "wikilink")（1984）

## 經歷

許書揚於[美國南](../Page/美國.md "wikilink")[伊利諾大學畢業後](../Page/伊利諾大學.md "wikilink")，加入[美國運通](../Page/美國運通.md "wikilink")（American
Express）擔任人事經理。

1991年，加入保聖那（Pasona）管理顧問公司，擔任台灣分公司負責人。除了負責PASONA與MGR台灣分公司的營運外，亦曾擔任保聖那集團（PASONA
Group）亞太地區執行長以及中國、香港、泰國等地分公司總經理。

目前為[經緯智庫](../Page/經緯智庫.md "wikilink")（MGR
Consulting）管理顧問公司及保聖那（Pasona）台灣分公司總經理。

許書揚曾獲頒Pasona Group全球海外分公司績效最佳經理人獎。

## 著作列表

  - 你可以更搶手（1999/奧林文化）
  - Top100 面談題目排行榜（1999/奧林文化）
  - e時代跳槽高手（2000/奧林文化）
  - 獵人觀點（2007/商訊文化）
  - CEO最在乎的事（2013/天下雜誌）

## 外部連結

  - [MGR 經緯智庫](http://www.mgr.com.tw)
  - [Pasona Taiwan 保聖那台灣](http://www.pasona.com.tw)

## 參考文獻

[Category:國立臺灣科技大學校友](../Category/國立臺灣科技大學校友.md "wikilink")

1.
2.