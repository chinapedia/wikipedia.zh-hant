**保罗·瓦兹拉威克**（，）是一位出生於[奥地利的美籍家庭治療師](../Page/奥地利.md "wikilink")、心理學家、傳播理論學家與哲學家，是的领军人物。在[家庭治療和一般心理治療上也有很高的成就](../Page/家庭治療.md "wikilink")。

[Category:美国心理学家](../Category/美国心理学家.md "wikilink")
[Category:奧地利心理學家](../Category/奧地利心理學家.md "wikilink")
[Category:史丹佛大學醫學院教師](../Category/史丹佛大學醫學院教師.md "wikilink")
[Category:威尼斯卡福斯卡里大學校友](../Category/威尼斯卡福斯卡里大學校友.md "wikilink")