[Sha_Tin_Government_Secondary_School.jpg](https://zh.wikipedia.org/wiki/File:Sha_Tin_Government_Secondary_School.jpg "fig:Sha_Tin_Government_Secondary_School.jpg")的沙田官立中學\]\]
[HK_ShaTinGovSecSchool.jpg](https://zh.wikipedia.org/wiki/File:HK_ShaTinGovSecSchool.jpg "fig:HK_ShaTinGovSecSchool.jpg")
**沙田官立中學**（，簡稱沙官、沙田官中、STGSS），是[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")[沙田區的第一間官立中學](../Page/沙田區.md "wikilink")。位於[沙田文禮路](../Page/沙田.md "wikilink")11-17號，處於[城門河畔](../Page/城門河.md "wikilink")，創辦於1972年9月。首任校長為岳文林（J.
Ormerod），附近的[文林路亦以其為名](../Page/文林路.md "wikilink")。創校初年曾因為沙田校舍尚未建成，暫以[黃大仙](../Page/黃大仙.md "wikilink")[龍翔官立中學為校址](../Page/龍翔官立中學.md "wikilink")。沙田官立中學是沙田區最早期的一批學校，亦是區內第一間官立中學，其校舍架構與同期建成的[荃灣官立中學](../Page/荃灣官立中學.md "wikilink")、[新界鄉議局元朗區中學及](../Page/新界鄉議局元朗區中學.md "wikilink")[何文田官立中學非常相近](../Page/何文田官立中學.md "wikilink")。經過多年的發展，由1970年代寂寂無聞的地區學校發展為一間有逾千位學生的中學。

沙田官立中學的[校訓是](../Page/校訓.md "wikilink")「智、仁、勇」（Wisdom, Love,
Vigour），出自《[論語](../Page/論語.md "wikilink")》的《論仁、論君子》：「智者不惑、仁者不憂、勇者不懼」。第二任校長黃景添先生在建校初年除了在校舍中央種植了老[杉樹之外](../Page/杉樹.md "wikilink")，也成立了以[花卉命名四](../Page/花卉.md "wikilink")[社](../Page/社.md "wikilink")：[杜鵑花](../Page/杜鵑花.md "wikilink")（Azalea）、[洋紫荊](../Page/洋紫荊.md "wikilink")（Bauhinia）、[山茶花](../Page/山茶花.md "wikilink")（Camellia）及[藍花楹](../Page/藍花楹.md "wikilink")（Jacaranda），其中藍花楹社的英文名字以J字開頭，是特別為紀念首任校長岳文林。

沙田官立中學以理科成績較佳，在1995、1997、2001及2006各產生一名[會考十優狀元](../Page/香港中學會考.md "wikilink")，而文科在2006年及2009年亦有九優狀元。課外活動則以[排球隊](../Page/排球.md "wikilink")、[手球隊](../Page/手球.md "wikilink")、[劍擊](../Page/劍擊.md "wikilink")、[中國舞及](../Page/中國舞.md "wikilink")[中樂團較為著名](../Page/中樂團.md "wikilink")。該校中樂團在過往12年，曾八次奪得香港校際音樂節30-60人合奏組別之冠軍，成績輝煌。

沙田官立中學亦是香港[環境保護署的沙田空氣質素監測站所在地](../Page/環境保護署_\(香港\).md "wikilink")\[1\]。

## 學校設施

  - 地下 -
    大堂、有蓋[操場](../Page/操場.md "wikilink")、小食部、[飯堂](../Page/飯堂.md "wikilink")、多用途[球場](../Page/球場.md "wikilink")、[自修室](../Page/自修室.md "wikilink")（30號）、[更衣室](../Page/更衣室.md "wikilink")、[圖書館](../Page/圖書館.md "wikilink")、[校務處](../Page/校務處.md "wikilink")、校長及副校長室、教員室、[學生會室](../Page/學生會.md "wikilink")、領袖生室、體育室、醫療室、茶水室、花圃、[停車場](../Page/停車場.md "wikilink")
  - 一樓 -
    學校[禮堂](../Page/禮堂.md "wikilink")（下層）、美術室、地理室、教員室、[中樂團室](../Page/中樂團.md "wikilink")
  - 二樓 -
    學校[禮堂](../Page/禮堂.md "wikilink")（上層）、[課室](../Page/課室.md "wikilink")（1-6及28-29號）、新翼課室（E21及E22）、電腦室（202室、204室）、英文室（E23）、廣播室、社工室（203室）
  - 三樓 -
    [物理實驗室](../Page/物理實驗室.md "wikilink")、[生物實驗室](../Page/生物實驗室.md "wikilink")、[課室](../Page/課室.md "wikilink")（7-12號）、新翼活動室（E31及E32）、[會議室](../Page/會議室.md "wikilink")（E33）、實驗預備室
  - 四樓 -
    普通[科學實驗室](../Page/科學實驗室.md "wikilink")、[化學實驗室](../Page/化學實驗室.md "wikilink")、[課室](../Page/課室.md "wikilink")（13-18號）、新翼課室（E41及E42）、[音樂室](../Page/音樂室.md "wikilink")（E43）、實驗預備室
  - 五樓 - [課室](../Page/課室.md "wikilink")（19-24號）、新翼課室（E51-52）、新翼電腦室（E54）
  - 六樓 - [天台](../Page/天台.md "wikilink")

地下至五樓各層均設有男女[洗手間](../Page/洗手間.md "wikilink")，地下至六樓各層新翼設有傷殘人士洗手間。一樓的洗手間為教師專用洗手間。

學校設有[升降機通往](../Page/升降機.md "wikilink")1-6樓各層。

## 歷史

  - 1973年，沙田官立中學校舍正式啟用。
  - 1993年，沙田官立中學[學生會正式成立](../Page/學生會.md "wikilink")。
  - 1995年，一度中斷的沙田官立中學舊生會在高葉慧嫻校長大力支持下重新組織成為沙田官立中學校友會，首任主席為阮卓輝博士。同年，懸掛於禮堂上的[英女皇像被除下](../Page/伊莉莎白二世_\(英國\).md "wikilink")，準備[香港主權移交](../Page/香港主權移交.md "wikilink")。
  - 1998年，沙田官立中學率先加入了教育統籌局的資訊科技教育試驗計劃，是全港獲選加入計劃的10間中學之一，並獲政府撥款600萬港元作為開發費用，建成學校光纖內聯網及自行管理伺服器，其後有[新加坡國防部](../Page/新加坡.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")[華僑中學](../Page/華僑中學.md "wikilink")、及[中國國內各單位前來參觀其基建設施](../Page/中國.md "wikilink")。同年，該校第一屆畢業生[龐愛蘭小姐獲選為](../Page/龐愛蘭.md "wikilink")[香港十大傑出青年](../Page/香港十大傑出青年.md "wikilink")。
  - 2001年，香港中學收生分開男女編排的制度被投訴為不公平，受法院裁決影響，該年沙官中一需要加開一班以接收在原來制度下未能獲編入該校的女生。
  - 2005年，根據[教統局的](../Page/教統局.md "wikilink")「校舍擴建計劃」，沙田官立中學完成在舊有停車場上蓋加建新翼的工程，達到2000年的校舍標準，並使校舍成為「[四合院](../Page/四合院.md "wikilink")」，學生從實驗室走到對面後樓梯的課室不再需要兜圈子\[2\]。
  - 2010年，沙田官立中學更新其官方網站版面，引起部份同學及校友不滿，於[Facebook上建立群組](../Page/Facebook.md "wikilink")《永遠懷念STGSS舊網頁
    (2004-2010)》，直至2011年10月，網站始改為較新的版面，唯網站只設英文版本。
  - 2011年，沙田官立中學因屬官校，必須參與教育局提倡的[自願優化班級結構計劃](../Page/自願優化班級結構計劃.md "wikilink")，該年度起的中一由原本5班（1A-1E）改為4班（1A-1D），直至現在

## 歷任校長

1.  岳文林先生（1972年－1978年）
2.  黃景添先生（1978年－1979年）
3.  戚懿璧女士（1979年－1986年）
4.  高葉慧嫻女士（1986年－1995年）
5.  馬紹良先生（1995年－2000年）
6.  張歡鴻先生（2000年－2001年）
7.  周金祥先生（2001年－2012年）
8.  黃廣榮先生（2012年－現在）

## 會考佳績

沙田官立中學於歷屆[香港中學會考出產了](../Page/香港中學會考.md "wikilink")4位十優狀元：周勝馥（1995）、楊作衡（1997）、李文俊（2001）、陳婷婷（2006）

## 香港傑出學生選舉

直至2018年(第33屆)，沙田官立中學在[香港傑出學生選舉共出產](../Page/香港傑出學生選舉.md "wikilink")6名傑出學生，排名全港第15。

## 著名/傑出校友

  - [文志賢](../Page/文志賢.md "wikilink")：[雅麗氏何妙齡那打素醫院及](../Page/雅麗氏何妙齡那打素醫院.md "wikilink")[大埔醫院行政總監](../Page/大埔醫院.md "wikilink")
  - [鄭夏恩](../Page/鄭夏恩.md "wikilink")：[雅丽氏何妙龄那打素医院醫生](../Page/雅丽氏何妙龄那打素医院.md "wikilink")，2003年因感染[SARS病逝](../Page/SARS.md "wikilink")
  - [方迪培](../Page/方迪培.md "wikilink")：[香港中文大學醫學院矯形外科及創傷學系研究助理教授](../Page/香港中文大學.md "wikilink")
  - [龐愛蘭](../Page/龐愛蘭.md "wikilink")：[沙田區議員](../Page/沙田區議會.md "wikilink")（[火炭](../Page/火炭.md "wikilink")）
  - [張穎茵](../Page/張穎茵.md "wikilink")：[新城電台節目主持](../Page/新城電台.md "wikilink")
  - [方健儀](../Page/方健儀.md "wikilink")：前[無綫新聞首席記者](../Page/無綫新聞.md "wikilink")／主播（1996年中七畢業）
  - [范綺明](../Page/范綺明.md "wikilink")：前[亞視新聞記者](../Page/亞視新聞.md "wikilink")／主播
  - [史可為](../Page/史可為.md "wikilink")：前[亞視新聞記者](../Page/亞視新聞.md "wikilink")，現任[中國中央電視台記者](../Page/中國中央電視台.md "wikilink")
  - [劉舜文](../Page/劉舜文.md "wikilink")：[有線電視足球評述員](../Page/有線電視.md "wikilink")
  - [孫耀威](../Page/孫耀威.md "wikilink")：歌手，曾在[香港中學會考中奪得](../Page/香港中學會考.md "wikilink")5A1C共28分佳績\[3\]
  - [古巨基](../Page/古巨基.md "wikilink")：歌手，曾就讀夜校部
  - [游思行](../Page/游思行.md "wikilink")：前[商業電台唱片騎師及填詞人](../Page/商業電台.md "wikilink")
  - [麥曦茵](../Page/麥曦茵.md "wikilink")：香港其中一位最年輕的導演及編劇，著名作品有《烈日當空》及《前度》等。2011年，她憑《志明與春嬌》榮獲香港電影金像獎最佳編劇一獎。
  - [顏卓靈](../Page/顏卓靈.md "wikilink")：女演員，曾憑電影《[大追捕](../Page/大追捕.md "wikilink")》榮獲2012年香港導演會新人獎，並以電影《[狂舞派](../Page/狂舞派.md "wikilink")》入圍[第50屆金馬獎](../Page/第50屆金馬獎.md "wikilink")[最佳女主角及](../Page/金馬獎最佳女主角.md "wikilink")[第33屆香港電影金像獎](../Page/第33屆香港電影金像獎.md "wikilink")[最佳女主角獎](../Page/香港電影金像獎最佳女主角.md "wikilink")。
  - [陳偉武](../Page/陳偉武.md "wikilink")：香港年輕導演、亦是創作團隊『YAMANYAMO
    也文也武』的創辦人之一；曾為容祖兒、方大同等製作 MV 音樂錄像短片。
  - [區永權](../Page/區永權.md "wikilink")：香港節目主持
  - [李潔瑩](../Page/李潔瑩.md "wikilink")：香港馬評人
  - [方保僑](../Page/方保僑.md "wikilink")：時事評論員
  - [周勝馥](../Page/周勝馥.md "wikilink")：新界區首位十優生，[Lalamove創始人暨執行長](../Page/啦啦快送.md "wikilink")
  - [黃璿僑](../Page/黃璿僑.md "wikilink")：香港中樂團首席琵琶樂師
  - [黃文意](../Page/黃文意.md "wikilink")：無綫娛樂新聞台主播
  - [梁港蘭](../Page/梁港蘭.md "wikilink")：永安旅遊董事兼行政總裁
  - [鄧啟泰](../Page/鄧啟泰.md "wikilink")：[明愛專上學院商業及款待管理學院助理教授](../Page/明愛專上學院.md "wikilink")
  - [德格](../Page/德格_\(香港\).md "wikilink")：網絡作家，在2012-2013年期間曾任《晴報》專欄作家，曾在2013年4月29日專欄提及母校的「ABCJ社」，並說「它們不是隨手可拈的梅蘭菊竹，而是經過精心設計，下過心思的好出品。」

## 事件

  - 2008年4月16日，該校一名15歲中三女生陳綺文在其住所[馬鞍山](../Page/馬鞍山_\(香港市鎮\).md "wikilink")[恆安邨恆星樓墮樓身亡](../Page/恆安邨.md "wikilink")\[4\]。
  - 2010年3月28日，該校一名16歲來自[綜援家庭的姓蔡中四男學生潛入低年級班房偷去](../Page/綜援.md "wikilink")2名中一女生合共341元及一部手機。校方得悉事件後報警處理。該男生其後在[沙田法院承認兩項盜竊罪](../Page/沙田.md "wikilink")\[5\]。
  - 2014年11月17日，因沙田官中早前一份中二級中文測驗卷，以「[9·28催淚彈驅散行動](../Page/9·28催淚彈驅散行動.md "wikilink")」為題材，要學生在文章中填上適當成語，形容警方「＿
    ＿ 人
    ＿」，引起反[佔中人士的指摘](../Page/佔領中環.md "wikilink")，《[人民日報](../Page/人民日報.md "wikilink")》海外版文章批評該測驗卷引導學生要形容警方「草菅人命」\[6\]。不過沙田官中回應傳媒查詢時指，該試卷的正確答案是「不近人情」而非「草菅人命」\[7\]。11月25日，沙官一群校友以「一群沙田官立中學舊生」為署名，在《[明報](../Page/明報.md "wikilink")》港聞版內頁落全版廣告，批評這些騷擾師生的行為和連串失實的報道和抹黑，並呼籲「全港市民，本著良知和公義，守護香港的民主自由，使下一代可在免於恐懼的環境下學習和成長」\[8\]。
  - 2017年6月12日，該校一名24歲葉姓女教學助理疑不堪工作壓力困擾，在港鐵[大圍站跳軌亡](../Page/大圍站.md "wikilink")。葉剛畢業即獲聘為合約制教學助理，於2016年9月入職，主要負責協助教師準備教案、準備教材、做成績及數據統計等工作\[9\]。

## 參考資料

<references />

## 外部連結

  - [沙田官立中學](http://www.stgss.edu.hk/)
  - [沙田官立中學校友會](https://web.archive.org/web/20061105112255/http://alumni.stgss.edu.hk/)
  - [永遠懷念STGSS舊網頁
    (2004-2010)](http://www.facebook.com/home.php?sk=group_164417003585736&ap=1/)

[S](../Category/香港官立中學.md "wikilink") [學](../Category/大圍.md "wikilink")
[S](../Category/沙田區中學.md "wikilink")
[Category:香港英文授課中學](../Category/香港英文授課中學.md "wikilink")
[Category:1972年創建的教育機構](../Category/1972年創建的教育機構.md "wikilink")

1.
2.  沙田官立中學學生會刊物 校園縱橫 第6期 1999年7月 新校舍
3.
4.  [性格文靜，剛受洗成教徒，15歲名校女生離奇墮樓亡](https://hk.news.appledaily.com/local/daily/article/20080417/10998095)蘋果日報,
    2008-4-17
5.  [稱零用錢少，中四生盜竊候判](http://orientaldaily.on.cc/cnt/news/20100424/00176_062.html)
    東方日報, 2010-3-29
6.  [香港中学试卷公然为“占中”唱赞歌](http://paper.people.com.cn/rmrbhwb/html/2014-11/17/content_1499749.htm),人民日報海外版,
    2014-11-17
7.  [誤指填充題「草菅人命」，《人民日報》斥「沙官」試題歌頌佔領](https://hk.news.appledaily.com/local/realtime/article/20141117/53137355)
    蘋果日報, 2014-11-17
8.  [沙官校友回應《人民日報》，全版廣告反擊試卷風](http://www.post852.com/67579/%E6%B2%99%E5%AE%98%E6%A0%A1%E5%8F%8B%E5%9B%9E%E6%87%89%E3%80%8A%E4%BA%BA%E6%B0%91%E6%97%A5%E5%A0%B1%E3%80%8B%E3%80%80%E5%85%A8%E7%89%88%E5%BB%A3%E5%91%8A%E5%8F%8D%E6%93%8A%E8%A9%A6%E5%8D%B7%E9%A2%A8/)
    852郵報, 2014-11-25
9.  [女教學助理跳軌亡，校方指其工作表現出色](http://hk.on.cc/hk/bkn/cnt/news/20170612/bkn-20170612120332455-0612_00822_001.html)
    東網, 2017-06-12