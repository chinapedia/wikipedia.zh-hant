**罗贝尔二世（虔诚者）**（**Robert II le
Pieux**，）是[卡佩王朝的第二位国王](../Page/卡佩王朝.md "wikilink")（996年—1031年在位）。

他是法蘭克人的國王[雨果·卡佩的儿子](../Page/雨果·卡佩.md "wikilink")，母为[阿基坦的阿德莱德](../Page/阿基坦.md "wikilink")。出生于[奥尔良](../Page/奥尔良.md "wikilink")。由于卡佩王朝早期极為脆弱，為了罗贝尔在其父王尚在位时即已被加冕为國王。當雨果·卡佩於987年7月當選成為法蘭克人的國王並加冕後，於同年12月羅貝爾被加冕成為共治國王\[1\]\[2\]。

996年，由于与自己的表妹[伯莎结婚](../Page/布戈涅之貝爾特.md "wikilink")，罗贝尔二世被[教宗](../Page/教宗.md "wikilink")[格里高利五世处以绝罚](../Page/額我略五世.md "wikilink")\[3\]，經過與格里高利五世的繼承人教皇[思維二世進行漫長的談判](../Page/思維二世.md "wikilink")，這段婚姻最終被廢除。

1003年，羅貝爾二世入侵[勃艮第公國](../Page/勃艮第公國.md "wikilink")。但是直至1016年，罗贝尔二世才得到教會支持承認合并勃艮第公國。他在1017年即已为長子[于格二世加冕称王](../Page/于格大帝.md "wikilink")。

羅貝爾二世晚年與眾兒子因爭奪權力及財產而爆發內戰，于格二世於1025年或1026年，在準備發動推翻他父王的叛亂時，在[貢比涅摔下馬來而駕崩](../Page/貢比涅.md "wikilink")\[4\]。次子[亨利一世於](../Page/亨利一世_\(法蘭西\).md "wikilink")1027年5月被羅貝爾二世安排加冕成為共治國王\[5\]。羅貝爾二世在與兒子亨利一世及[羅貝爾的內戰中被擊敗被迫退至](../Page/羅貝爾一世_\(勃艮第\).md "wikilink")[博讓西](../Page/博讓西.md "wikilink")，他於1031年7月20日在此內戰中死於[默倫](../Page/默倫.md "wikilink")，由亨利一世繼承法國及勃艮第公國。

## 家庭

子女：

1.  阿历克斯
2.  [于格二世](../Page/于格大帝.md "wikilink")
3.  [亨利一世](../Page/亨利一世_\(法兰西\).md "wikilink")
4.  阿德莱德
5.  [罗贝尔](../Page/羅貝爾一世_\(勃艮第\).md "wikilink")
6.  欧多
7.  康斯坦丝

## 注釋

[R](../Category/法国君主.md "wikilink") [R](../Category/卡佩王朝.md "wikilink")

1.  Andrew W. Lewis, "Anticipatory Association of the Heir in Early
    Capetian France" *The American Historical Review* **83**.4 (October
    1978:906–927) p. 907.
2.  Robert Fawtier, *The Capetian Kings of France*, transl. Lionel
    Butler and R.J. Adam, (Macmillan, 1989), 48.
3.  James Palmer, *The Apocalypse in the Early Middle Ages*, (Cambridge
    University Press, 2014), 215.
4.  *New Cambridge Medieval History*, IV:124.
5.  William W. Clark, *Medieval Cathedrals*, (Greenwood Publishing,
    2006), 87.