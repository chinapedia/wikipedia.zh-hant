**派西·衛斯理**(**Percy Weasley**，1976年8月22日-)，全名**派西·伊格內修斯·衛斯理**(**Percy
Ignatius Weasley**)
，虛構人物，是[英國](../Page/英國.md "wikilink")[作家](../Page/作家.md "wikilink")[J·K·羅琳的兒童](../Page/J·K·羅琳.md "wikilink")[奇幻小說](../Page/奇幻小說.md "wikilink")《[哈利·波特](../Page/哈利·波特.md "wikilink")》[系列](../Page/系列.md "wikilink")《[哈利波特](../Page/哈利波特.md "wikilink")》中的角色。

## 家庭

派西的[父親](../Page/父親.md "wikilink")[亞瑟·衛斯理是](../Page/亞瑟·衛斯理.md "wikilink")[魔法部](../Page/魔法部.md "wikilink")[麻瓜人工製品濫用局的](../Page/麻瓜.md "wikilink")[員工](../Page/員工.md "wikilink")；他的[母親是](../Page/母親.md "wikilink")[茉莉·衛斯理](../Page/茉莉·衛斯理.md "wikilink")。派西有五名兄弟和一個妹妹，分別是[比爾·衛斯理](../Page/比爾·衛斯理.md "wikilink")、[查理·衛斯理](../Page/查理·衛斯理_\(小說角色\).md "wikilink")、[弗雷·衛斯理](../Page/弗雷和喬治·衛斯理.md "wikilink")、[喬治·衛斯理](../Page/弗雷和喬治·衛斯理.md "wikilink")、[榮恩·衛斯理和](../Page/榮恩·衛斯理.md "wikilink")[金妮·衛斯理](../Page/金妮·衛斯理.md "wikilink")。在衛斯理家排行第三。　

## 工作

派西崇尚權力，就读于[霍格華茲魔法與巫術學院](../Page/霍格華茲魔法與巫術學院.md "wikilink")5年級時擔任級長（當時哈利等人還在唸一年級），6年級時（第二集）與潘妮·清水交往（被金妮無意撞見），7年級時（第三集）成為男學生主席。畢業後在[魔法部國際魔法交流合作部工作](../Page/魔法部.md "wikilink")，擔任巴堤·柯羅奇的助理（他本人非常崇拜遵守規則的[巴堤·柯羅奇](../Page/巴堤·柯羅奇.md "wikilink")），後來又成為魔法部部長康尼留斯·夫子的助理。

由於魔法部不想相信[佛地魔已回來](../Page/佛地魔.md "wikilink")，不停的宣傳[鄧不利多和哈利已經癲瘋了](../Page/鄧不利多.md "wikilink")，而派西則因為權力而與父親立場不同，和衛斯理家決裂不回衛斯理家，而到倫敦居住。最後於第二次[霍格華茲大戰時因後悔而返回參戰](../Page/霍格華茲.md "wikilink")。

佛地魔被擊潰後，因潘妮·清水下落不明，而與另一女子奧黛麗正式結婚成家，育有兩名女兒，分別為茉莉·衛斯理和露西·衛斯理。

## 外部連結

  - [J·K·羅琳官方網頁](https://web.archive.org/web/20070926224412/http://origin.jkrowling.com/fr/)

[Category:哈利波特人物](../Category/哈利波特人物.md "wikilink")