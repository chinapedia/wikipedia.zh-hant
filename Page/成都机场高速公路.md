**成都机场高速公路**是一条连接[成都市区和](../Page/成都.md "wikilink")[成都双流国际机场的城市快速通道](../Page/成都双流国际机场.md "wikilink")，全长12公里，絕大部分路段采用了[高架桥的形式修建](../Page/高架桥.md "wikilink")，1999年7月建成通车。

現時由[香港和](../Page/香港.md "wikilink")[上海上市公司的](../Page/上海.md "wikilink")[四川成渝高速公路股份有限公司](../Page/四川成渝高速公路股份有限公司.md "wikilink")（）管理。

## 出入口与沿线设施

## 参考资料

[高](../Category/成都交通.md "wikilink")
[S](../Category/四川省高速公路.md "wikilink")
[Category:招商局公路權益](../Category/招商局公路權益.md "wikilink")