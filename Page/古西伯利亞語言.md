**古西伯利亞語言**（Paleosiberian
languages、Paleosiberian），或**古亞細亞語言**（Paleoasian
languages、Palaeo-Asiatic），並不是一種語言，也不是一個正式的[語系](../Page/語系.md "wikilink")，而是[語言學家用來指涉某些位於](../Page/語言學家.md "wikilink")[西伯利亞偏遠地區的孤立](../Page/西伯利亞.md "wikilink")[語言和小語系語言的稱謂](../Page/語言.md "wikilink")，作為這些語言的總稱。古西伯利亞語言的情況十分複雜，各語言之間的系屬關係目前也不十分明確。因此，古西伯利亞語言基本上只是為了描述上的便利而創造出來的，並不意味這些語言之間有歷史發展上的承傳關係。

說古西伯利亞語言的人口總數約為23,000人。\[1\]

## 分類支系

此外，有人認為目前語言分類存在爭議的[朝鮮語](../Page/朝鮮語.md "wikilink")、[日本語和](../Page/日本語.md "wikilink")[阿伊努語與一些古西伯利亞語言之間存在某種聯繫](../Page/阿伊努語.md "wikilink")，不過尚未被證實或普遍接受。

## 參見

  - [美洲原住民語言](../Page/美洲原住民語言.md "wikilink")
  - [併合 (語言學)](../Page/併合_\(語言學\).md "wikilink")
  - [黏著法構詞](../Page/黏著法構詞.md "wikilink")

## 註釋

## 延伸閱讀

  -
## 外部連結

  -

{{-}}

[Category:語系](../Category/語系.md "wikilink")
[Category:古西伯利亞語言](../Category/古西伯利亞語言.md "wikilink")

1.  [Number of Speakers of the Major Language Families of the
    World](http://www.freelang.net/families/index.php) (Note—numbers
    given for the Niger-Congo languages on the chart are about 50 years
    out of date as of 2011, and for some of the other languages
    families, the numbers are 15-20 years out of date as of 2011)