在数学中，**泰勒级数**（）用无限项连加式——[级数来表示一个函数](../Page/级数.md "wikilink")，这些相加的项由函数在某一点的[导数求得](../Page/导数.md "wikilink")。泰勒级数是以于1715年发表了[泰勒公式的](../Page/泰勒公式.md "wikilink")[英國](../Page/英國.md "wikilink")[数学家](../Page/数学家.md "wikilink")[布魯克·泰勒](../Page/布魯克·泰勒.md "wikilink")（）来命名的。通过函数在自变量零点的导数求得的泰勒级数又叫做**麦克劳林级数**，以苏格兰数学家[科林·麦克劳林的名字命名](../Page/科林·麦克劳林.md "wikilink")。

[拉格朗日在](../Page/拉格朗日.md "wikilink")1797年之前，最先提出帶有餘項的現在形式的泰勒定理。实际应用中，泰勒级数需要截断，只取有限项，可以用[泰勒定理估算这种近似的误差](../Page/泰勒定理.md "wikilink")。一个函数的有限项的泰勒级数叫做[泰勒多项式](../Page/泰勒多项式.md "wikilink")。一个函数的泰勒级数是其泰勒多项式的[极限](../Page/极限.md "wikilink")（如果存在极限）。即使泰勒级数在每点都收敛，函数与其泰勒级数也可能不相等。在开区间（或[复平面上的开区间](../Page/复平面.md "wikilink")）上，与自身泰勒级数相等的函数称为[解析函数](../Page/解析函数.md "wikilink")。

## 定义

在数学上，对于一个在[实数或](../Page/实数.md "wikilink")[复数](../Page/复数.md "wikilink")\(a\)[邻域上](../Page/邻域.md "wikilink")，[以实数作为变量或](../Page/实数.md "wikilink")[以复数作为变量的](../Page/複數.md "wikilink")[函数](../Page/函数.md "wikilink")，并且是[无穷可微的](../Page/导数.md "wikilink")[函数](../Page/函数.md "wikilink")\(f(x)\)，它的**泰勒级数**是以下这种形式的[幂级数](../Page/幂级数.md "wikilink")：

\[\sum_{n=0}^{\infin} \frac{f^{(n)}(a)}{n!} (x-a)^{n}\]

这里，\(n!\)表示\(n\)的[阶乘](../Page/阶乘.md "wikilink")，而\(f^{(n)}(a)\,\!\)表示函数\(f\)在点\(a\)处的\(n\)阶[导数](../Page/导数.md "wikilink")。如果\(a=0\)，也可以把这个级数称为**[麦克劳林级数](../Page/科林·麦克劳林.md "wikilink")**。

## 解析函數

[Exp_neg_inverse_square.svg](https://zh.wikipedia.org/wiki/File:Exp_neg_inverse_square.svg "fig:Exp_neg_inverse_square.svg")在1823年指出函數\(\exp \left(- \frac{1}{x^2} \right)\)在\(x=0\)无法被解析。\]\]
如果泰勒级数对于区间\((a-r,a+r)\)中的所有\(x\)都收敛并且级数的和等于\(f(x)\)，那么我们就称函数\(f(x)\)为**[解析形的函数](../Page/解析函数.md "wikilink")**（analytic）。一个函数[当且仅当](../Page/当且仅当.md "wikilink")（简单地说，“只有在”）能够被表示为[幂级数的形式时](../Page/幂级数.md "wikilink")，才是解析形的函数。通常会用**[泰勒定理](../Page/泰勒定理.md "wikilink")**来估计级数的[餘项](../Page/餘项.md "wikilink")，这样就能够确定级数是否收敛于\(f(x)\)。上面给出的幂级数展开式中的系数正好是泰勒级数中的系数。

以下三个事实可以说明为什么泰勒级数是十分重要的：

1.  可以逐项对[幂级数的计算微分和积分](../Page/幂级数.md "wikilink")，因此求[和函数相对比较容易](../Page/和函数.md "wikilink")。
2.  数学家因此能够在[复数平面上研究函数](../Page/复分析.md "wikilink")，因为一个[解析函数](../Page/解析函数.md "wikilink")，也可以被定义为在[复平面中一个开放的区间内的](../Page/复数.md "wikilink")[解析函数](../Page/解析函数.md "wikilink")(在区间内每一个点上都能被微分的函数)。
3.  可用泰勒级数估计，在某一点上函数会计算出什么值。

对于一些[无穷的](../Page/无穷.md "wikilink")[可以被微分](../Page/导数.md "wikilink")[函数](../Page/函数.md "wikilink")\(f(x)\)，虽然它们的展开式会收敛，但是并不等于\(f(x)\)。例如，[分段函数](../Page/分段函数.md "wikilink")\(f(x) = \exp \left(- \frac{1}{x^2} \right)\)，如果\(x \ne 0\)并且\(f(0)=0\)，则\(x=0\)时所有的导数都为零，所以这个\(f(x)\)的泰勒级数为零，且其[收敛半径为无穷大](../Page/收敛半径.md "wikilink")，不过函数\(f(x)\)仅在\(x=0\)处为零。但是，在[以复数作为变量的函数中这个问题并不存在](../Page/复数.md "wikilink")，因为当\(z\)沿虚轴趋于零，\(\exp \left(- \frac{1}{z^2} \right)\)并不趋于零。

如果一个函数在某处引发一个奇点，它就无法被展开为泰勒级数，不过如果变量\(x\)是负指数幂的话，我们仍然可以将其展开为一个级数。例如，虽然在\(x=0\)的时候，\(f(x) = \exp \left(- \frac{1}{x^2} \right)\)会引发奇点，但仍然能够把这个函数展开为一个[洛朗级数](../Page/洛朗级数.md "wikilink")。

最近，专家们发现了一个用泰勒级数来求解[微分方程的方法](../Page/微分方程.md "wikilink")——\[1\]。用[皮卡反覆運算便可以推导出这个方法](../Page/柯西-利普希茨定理.md "wikilink")。

## 常用的函数的麦克劳林序列

[TaylorCosCosEnhanced.svg](https://zh.wikipedia.org/wiki/File:TaylorCosCosEnhanced.svg "fig:TaylorCosCosEnhanced.svg")上餘弦函數的實數部分。\]\]
[TaylorCosPolSVG.svg](https://zh.wikipedia.org/wiki/File:TaylorCosPolSVG.svg "fig:TaylorCosPolSVG.svg")上餘弦函數的第八度逼近\]\]
[TaylorCosAllSVG.svg](https://zh.wikipedia.org/wiki/File:TaylorCosAllSVG.svg "fig:TaylorCosAllSVG.svg")
下面我们给出了几个重要的泰勒级数。当变量\(x\)是复数时，这些等式依然成立。

### 几何级数

[几何级数](../Page/几何级数.md "wikilink")

\[\frac{1}{1-x} = \sum^{\infin}_{n=0} x^n\quad \forall x: \left| x \right| < 1\]

### 二项式定理

[二项式定理](../Page/二项式定理.md "wikilink")

\[(1+x)^\alpha = \sum^{\alpha}_{n=0} C(\alpha,n) x^n\quad \forall x: \left| x \right| < 1, \forall \alpha \in \mathbb{C}\]

  -
    二项式展开中的\(C(\alpha,n)\)是[二项式系数](../Page/二项式系数.md "wikilink")。

### 指数函数和自然对数

以\(e\)为底数的[指数函数的麦克劳林序列是](../Page/指数函数.md "wikilink")

\[e^{x} = \sum^{\infin}_{n=0} \frac{x^n}{n!}\quad \forall x\] （对所有X都成立）

以\(e\)为底数的[自然对数的麦克劳林序列是](../Page/自然对数.md "wikilink")

\[\ln(1+x) = \sum^{\infin}_{n=1} \frac{(-1)^{n+1}}n x^n\quad \forall x\in (-1,1]\]
（对于在区间(-1,1\]内所有的X都成立）

### 三角函数

常用的[三角函数可以被展开为以下的麦克劳林序列](../Page/三角函数.md "wikilink")：

\[\begin{align}
\sin x &= \sum^{\infty}_{n=0} \frac{(-1)^n}{(2n+1)!} x^{2n+1} &&= x - \frac{x^3}{3!} + \frac{x^5}{5!} - \cdots && \forall x\\[6pt]
\cos x &= \sum^{\infty}_{n=0} \frac{(-1)^n}{(2n)!} x^{2n} &&= 1 - \frac{x^2}{2!} + \frac{x^4}{4!} - \cdots && \forall x\\[6pt]
\tan x &= \sum^{\infty}_{n=1} \frac{B_{2n} (-4)^n \left(1-4^n\right)}{(2n)!} x^{2n-1} &&= x + \frac{x^3}{3} + \frac{2 x^5}{15} + \cdots && \forall x:|x| < \frac{\pi}{2}\\[6pt]
\sec x &= \sum^{\infty}_{n=0} \frac{(-1)^n E_{2n}}{(2n)!} x^{2n} &&=1+\frac{x^2}{2}+\frac{5x^4}{24}+\cdots && \forall x:|x| < \frac{\pi}{2}\\[6pt]
\arcsin x &= \sum^{\infty}_{n=0} \frac{(2n)!}{4^n (n!)^2 (2n+1)} x^{2n+1} &&=x+\frac{x^3}{6}+\frac{3x^5}{40}+\cdots && \forall x:|x| \le 1\\[6pt]
\arccos x &=\frac{\pi}{2}-\arcsin x\\&=\frac{\pi}{2}- \sum^{\infty}_{n=0} \frac{(2n)!}{4^n (n!)^2 (2n+1)} x^{2n+1}&&=\frac{\pi}{2}-x-\frac{x^3}{6}-\frac{3x^5}{40}+\cdots&& \forall x:|x| \le 1\\[6pt]
\arctan x &= \sum^{\infty}_{n=0} \frac{(-1)^n}{2n+1} x^{2n+1} &&=x-\frac{x^3}{3} + \frac{x^5}{5}-\cdots && \forall x:|x| \le 1,\ x\neq\pm i
\end{align}\]

  -
    在\(\tan(x)\)展开式中的B<sub>k</sub>是[伯努利数](../Page/伯努利数.md "wikilink")。在\(\sec(x)\)展开式中的*E*<sub>*k*</sub>是[欧拉数](../Page/欧拉数.md "wikilink")。

### 双曲函数

[双曲函数](../Page/双曲函数.md "wikilink")

\[\sinh x = \sum^{\infin}_{n=0} \frac{1}{(2n+1)!} x^{2n+1}\quad \forall x\]

\[\cosh x = \sum^{\infin}_{n=0} \frac{1}{(2n)!} x^{2n}\quad \forall x\]

\[\tanh x = \sum^{\infin}_{n=1} \frac{B_{2n} 4^n (4^n-1)}{(2n)!} x^{2n-1}\quad \forall x: \left| x \right| < \frac{\pi}{2}\]

\[\sinh^{-1} x = \sum^{\infin}_{n=0} \frac{(-1)^n (2n)!}{4^n (n!)^2 (2n+1)} x^{2n+1}\quad \forall x: \left| x \right| < 1\]

\[\tanh^{-1} x = \sum^{\infin}_{n=0} \frac{1}{2n+1} x^{2n+1}\quad \forall x: \left| x \right| < 1\]

\[\tanh(x)\]展开式中的*B*<sub>*k*</sub>是[伯努利数](../Page/伯努利数.md "wikilink")。

### 朗伯W函数

[朗伯W函数](../Page/朗伯W函数.md "wikilink")

\[W_0(x) = \sum^{\infin}_{n=1} \frac{(-n)^{n-1}}{n!} x^n\quad \forall x: \left| x \right| < \frac{1}{e}\]

## 多元函数的展开

泰勒级数可以推广到有多个[变量的](../Page/变量.md "wikilink")[函数](../Page/函数.md "wikilink")：
\(\sum_{n_1=0}^{\infin} \cdots \sum_{n_d=0}^{\infin}
\frac{\partial^{n_1+\cdots+n_d}}{\partial x_1^{n_1}\cdots\partial x_d^{n_d}}
\frac{f(a_1,\cdots,a_d)}{n_1!\cdots n_d!}
(x_1-a_1)^{n_1}\cdots (x_d-a_d)^{n_d}\)

## 历史

希腊哲学家[芝诺在考虑了利用无穷级数求和来得到有限结果的问题](../Page/埃利亚的芝诺.md "wikilink")，得出不可能的结论 -
[芝诺悖论](../Page/芝诺悖论.md "wikilink")。后来，[亚里士多德对芝诺悖论在哲学上进行了反驳](../Page/亚里士多德.md "wikilink")，但[德谟克利特以及后来的](../Page/德谟克利特.md "wikilink")[阿基米德进行研究](../Page/阿基米德.md "wikilink")，此部分数学内容才得到解决。
正是用了阿基米德的[穷竭法才使得一个无穷级数被逐步的细分](../Page/穷竭法.md "wikilink")，得到了有限的结果。\[2\].几个世纪之后，中国数学家[刘徽也独立提出了类似的方法](../Page/刘徽.md "wikilink")。\[3\]

进入14世纪，最早使用了泰勒级数以及相关的方法\[4\]。尽管他的数学著作没有流传下来，但后来印度数学家的著作表明他发现了一些特殊的泰勒级数，这些级数包括[正弦](../Page/正弦.md "wikilink")、[余弦](../Page/余弦.md "wikilink")、[正切](../Page/正切.md "wikilink")、和[反正切三角函数等等](../Page/反正切.md "wikilink")。之后，在他的基础上进行了一系列的延伸与合理逼近，这些工作一直持续到16世纪。

到了17世纪，[詹姆斯·格雷果里同样继续着这方面的研究并且发表了若干](../Page/詹姆斯·格雷果里.md "wikilink")[麦克劳林级数](../Page/麦克劳林级数.md "wikilink")。但是直到1715年，[布鲁克·泰勒](../Page/布鲁克·泰勒.md "wikilink")
\[5\] 提出了一个通用的方法来构建适用于所有函数的此类列级数。这就是后来被人们所熟知的泰勒级数。
[麦克劳林级数是泰勒级数的特例](../Page/麦克劳林级数.md "wikilink")，是[爱丁堡大学的](../Page/爱丁堡大学.md "wikilink")[科林·麦克劳林教授在](../Page/科林·麦克劳林.md "wikilink")18世纪发表的，并以其名字命名。

## 與牛頓插值公式的淵源

[Principia1846-466.png](https://zh.wikipedia.org/wiki/File:Principia1846-466.png "fig:Principia1846-466.png")》的第三編“宇宙體系”的引理五的图例。這裡在橫坐標上有6個點H,I,K,L,M,N，對應著6個值A,B,C,D,E,F，生成一個多項式函數對這6個點上有對應的6個值，計算任意點S對應的值R。牛頓給出了間距為單位值和任意值的兩種情況。\]\]
**牛頓插值公式**也叫做**[牛頓級數](../Page/牛頓多項式.md "wikilink")**，由“牛頓前向[差分方程](../Page/差分方程.md "wikilink")”的項組成，得名於[伊薩克·牛頓爵士](../Page/伊薩克·牛頓.md "wikilink")，最早发表为他在1687年出版的《[自然哲學的數學原理](../Page/自然哲學的數學原理.md "wikilink")》中第三編“宇宙體系”的引理五\[6\]，此前[詹姆斯·格雷果里於](../Page/詹姆斯·格雷果里.md "wikilink")1670年和牛頓於1676年已經分別獨立得出這個成果。一般稱其為連續“泰勒展開”的離散對應。

### 差分

對於x值間隔為非一致步長，牛頓計算[均差](../Page/均差.md "wikilink")，對*x*值間隔為單位步長1或一致但非單位量的情況，計算[差分](../Page/差分.md "wikilink")，前向差分的定義為：

\[\begin{align}
\Delta_h^1[f](x) &=  f(x + h) - f(x) \\
\Delta^n_h[f](x) &= \Delta_h^{n-1}[f](x+h) -\Delta_h^{n-1}[f](x) \\
\end{align}\]

### 插值公式

牛頓前向差分插值公式為：

  -
    <math>

\\begin{align} f(x) &= f(a) + \\frac {x-a} {h} \\left(
\\Delta_h^1\[f\](a) + \\frac {x-a-h} {2h}\\left(\\Delta_h^2\[f\](a) +
\\cdots \\right) \\right) \\\\

`&= f(a) + \sum_{k=1}^n \frac{\Delta_h^k[f](a)}{k!h^k} \prod_{i=0}^{k-1} ((x-a)-ih) \\`

\\end{align} </math>

這成立於任何[多項式函數和大多數但非全部](../Page/多項式.md "wikilink")[解析函數](../Page/解析函數.md "wikilink")。

### 無窮級數

[牛頓在](../Page/伊薩克·牛頓.md "wikilink")1665年得出並在1671年寫的《流數法》中發表了\(\ln(1+x)\)的[無窮級數](../Page/無窮級數.md "wikilink")，在1666年得出了\(\arcsin(x)\)和\(\arctan(x)\)的無窮級數，在1669年的《分析學》中發表了\(\sin(x)\)、\(\cos(x)\)、\(\arcsin(x)\)和\(e^x\)的無窮級數；[萊布尼茨在](../Page/萊布尼茨.md "wikilink")1673年大概也得出了\(\sin(x)\)、\(\cos(x)\)和\(\arctan(x)\)的無窮級數。[布魯克·泰勒在](../Page/布魯克·泰勒.md "wikilink")1715年著作《[Methodus
Incrementorum Directa et
Inversa](http://www.17centurymaths.com/contents/taylorscontents.html)》中研討了[有限差分方法](../Page/有限差分.md "wikilink")，其中論述了他在1712年得出的[泰勒定理](../Page/泰勒定理.md "wikilink")，這個成果此前[詹姆斯·格雷果里在](../Page/詹姆斯·格雷果里.md "wikilink")1670年和[萊布尼茨在](../Page/萊布尼茨.md "wikilink")1673年已經得出，而[約翰·伯努利在](../Page/約翰·伯努利.md "wikilink")1694年已經在《教師學報》發表。

他對牛頓的均差分的步長取趨於\(0\)的[極限](../Page/極限_\(數學\).md "wikilink")，得出：

  -
    <math>

\\begin{align} f(x) &= f(a) + \\lim_{h \\to 0}\\sum_{k=1}^\\infty
\\frac{\\Delta_h^k\[f\](a)}{k\!h^k} \\prod_{i=0}^{k-1} ((x-a)-ih) \\\\

`&= f(a) + \sum_{k=1}^\infty \frac{d^k}{dx^k}f(a) \frac{(x-a)^k}{k!} \\`

\\end{align} </math>

## 参考文献

## 參見

  - [無窮級數](../Page/無窮級數.md "wikilink")
  - [牛頓多項式](../Page/牛頓多項式.md "wikilink")
  - [冪級數](../Page/冪級數.md "wikilink")
  - [光滑函數](../Page/光滑函數.md "wikilink")
  - [帕德近似](../Page/帕德近似.md "wikilink")
  - [泰勒公式](../Page/泰勒公式.md "wikilink")

[pl:Wzór Taylora\#Szereg
Taylora](../Page/pl:Wzór_Taylora#Szereg_Taylora.md "wikilink")

[Category:级数](../Category/级数.md "wikilink")
[Category:光滑函数](../Category/光滑函数.md "wikilink")
[Category:微积分](../Category/微积分.md "wikilink")

1.
2.  Kline, M. (1990) *Mathematical Thought from Ancient to Modern
    Times*. Oxford University Press. pp. 35-37
3.  [吴文俊](../Page/吴文俊.md "wikilink") 《中国数学史大系》第三卷 367页
4.
5.  Taylor, Brook, *Methodus Incrementorum Directa et Inversa* \[Direct
    and Reverse Methods of Incrementation\] (London, 1715), pages 21-23
    (Proposition VII, Theorem 3, Corollary 2). Translated into English
    in D. J. Struik, *A Source Book in Mathematics 1200-1800*
    (Cambridge, Massachusetts: Harvard University Press, 1969), pages
    329-332.
6.  Newton, Isaac, (1687). [*Principia*, Book III, Lemma V, Case
    1](http://books.google.com/books?id=KaAIAAAAIAAJ&dq=sir%20isaac%20newton%20principia%20mathematica&as_brr=1&pg=PA466#v=onepage&q&f=false)