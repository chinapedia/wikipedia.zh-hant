[Touch_Panel.jpg](https://zh.wikipedia.org/wiki/File:Touch_Panel.jpg "fig:Touch_Panel.jpg")
[Samsung_Android_Smartphone.jpg](https://zh.wikipedia.org/wiki/File:Samsung_Android_Smartphone.jpg "fig:Samsung_Android_Smartphone.jpg")

\-{zh-hans;zh-hant|**觸控螢幕**}-（），是可以接收觸頭（包括[手指或者膠筆頭等](../Page/手指.md "wikilink")）等輸入訊號的感應式[液晶顯示](../Page/液晶顯示.md "wikilink")、[平板螢幕或](../Page/平板顯示器.md "wikilink")[电子墨水裝置](../Page/电子墨水.md "wikilink")。當接觸了[螢幕上的圖形按鈕時](../Page/螢幕.md "wikilink")，螢幕上的[觸覺](../Page/觸覺.md "wikilink")[反饋系統可根據預先編程的程式驅動各種連結裝置](../Page/反饋.md "wikilink")，可用以取代機械式的按鈕面板，並藉由顯示畫面製造出生動的影音效果。
觸控螢幕的用途非常廣泛，從常見的[提款機](../Page/提款機.md "wikilink")、[PDA](../Page/PDA.md "wikilink")、到工業用的觸控電腦，因為觸控螢幕為親切且生動的[人機介面](../Page/人機介面.md "wikilink")。2007年以後，愈來愈多[-{zh-cn:智能手机;zh-tw:智慧型手機;zh-hk:智能手機;}-也採用了觸控式螢幕](../Page/智能手机.md "wikilink")，典型的例子如[iPhone](../Page/iPhone.md "wikilink")。

## 種類

通常是在[半反射式液晶面板上覆蓋一層](../Page/半反射式液晶面板.md "wikilink")[壓力板](../Page/壓力板.md "wikilink")，其對壓力有高敏感度，當物體施壓於其上時會有電流訊號產生以定出壓力源位置，並可動態追蹤。
現亦有In cell
Touch觸控元件整合於顯示面板之內，使面板本身就具有觸控功能，不需另外進行與觸控面板的貼合與組裝即可達到觸控的效果與應用。

按[传感器工作原理](../Page/传感器.md "wikilink")，觸控螢幕大致上可分為︰

  - [電容式](../Page/电容式感应.md "wikilink")
  - [電阻式](../Page/电阻器.md "wikilink")
  - [紅外線式](../Page/紅外線.md "wikilink")
  - [聲波式](../Page/聲波.md "wikilink")

## 應用裝置

  - [餐饮點餐系統](../Page/餐饮點餐系統.md "wikilink")
  - [会议展示](../Page/会议展示.md "wikilink")
  - [醫療系統](../Page/醫療系統.md "wikilink")
  - [POS](../Page/POS.md "wikilink")
  - [Kiosk](../Page/Kiosk.md "wikilink")
  - [GPS](../Page/GPS.md "wikilink")
  - [提款機](../Page/提款機.md "wikilink")
  - [遊樂器](../Page/遊樂器.md "wikilink")
  - 工業用[人機介面](../Page/人機介面.md "wikilink")
  - 停車場停車繳費系統
  - 數位家電
  - 汽車中央控制面板
  - PC手寫系統
  - 录音设备用开关
  - [筆記型電腦](../Page/筆記型電腦.md "wikilink")[觸控板](../Page/觸控板.md "wikilink")
  - [個人數位助理](../Page/個人數位助理.md "wikilink")
  - [手機](../Page/手機.md "wikilink")
  - [Microsoft Surface](../Page/Microsoft_Surface.md "wikilink")
  - [iPhone](../Page/iPhone.md "wikilink")

## 參見

  - [多點觸控](../Page/多點觸控.md "wikilink")
  - [光筆](../Page/光筆.md "wikilink")
  - [觸控筆](../Page/觸控筆.md "wikilink")
  - [IPS液晶](../Page/IPS液晶.md "wikilink")
  - [手寫運算](../Page/手寫運算.md "wikilink")
  - [单点触摸屏](https://www.vicpas.com)

[Category:硬件](../Category/硬件.md "wikilink")
[Category:電子工業](../Category/電子工業.md "wikilink")
[Category:自動控制](../Category/自動控制.md "wikilink")