**尼尼微省**（[阿拉米語](../Page/阿拉米語.md "wikilink")：****；[阿拉伯語](../Page/阿拉伯語.md "wikilink")：****；[庫爾德語](../Page/庫爾德語.md "wikilink")：）是[伊拉克十八省之一](../Page/伊拉克.md "wikilink")。面積37,323平方公里，2011年人口3,270,400。首府[摩蘇爾](../Page/摩蘇爾.md "wikilink")。1976年以前稱為**摩蘇爾省**，並包括了今日的[杜胡克省](../Page/杜胡克省.md "wikilink")。该省的居民主要为[库尔德人和](../Page/库尔德人.md "wikilink")[亚述人](../Page/亚述人.md "wikilink")。其中[亚述人信仰东方礼的](../Page/亚述人.md "wikilink")[基督教](../Page/基督教.md "wikilink")，讲**新亚述语**（现代[阿拉米语的方言](../Page/阿拉米语.md "wikilink")）。[亚述人在](../Page/亚述人.md "wikilink")[尼尼微地区生活了几千年之久](../Page/尼尼微.md "wikilink")，是名符其实的土著民族。该省东部的三角地带现在依然是当代[亚述人的集中聚居地和文化中心](../Page/亚述人.md "wikilink")。

2014年6月，[伊拉克和黎凡特伊斯蘭國的武裝分子攻陷摩蘇爾](../Page/伊拉克和黎凡特伊斯蘭國.md "wikilink")，並控制全個尼尼微省。\[1\]2017年8月，伊拉克政府宣佈已完全收復尼尼微省。\[2\]

## 地理

尼尼微省位于[伊拉克北部](../Page/伊拉克.md "wikilink")，与[土耳其](../Page/土耳其.md "wikilink")、[叙利亚接壤](../Page/叙利亚.md "wikilink")。北连[杜胡克省和土耳其](../Page/杜胡克省.md "wikilink")[舍尔纳克省](../Page/舍尔纳克省.md "wikilink")，东邻[埃尔比勒省](../Page/埃尔比勒省.md "wikilink")，南接[萨拉赫丁省](../Page/萨拉赫丁省.md "wikilink")、[安巴尔省](../Page/安巴尔省.md "wikilink")，西北部是叙利亚[代尔祖尔省与](../Page/代尔祖尔省.md "wikilink")[哈塞克省](../Page/哈塞克省.md "wikilink")。

## 行政区划

尼尼微省下辖10个县。

  - 摩苏尔县(Al-Mosul)
  - 阿格拉县(Aqrah)
  - Shekhan 县
  - Al-Shekhan 县
  - Al-Hamdaniya 县
  - 泰勒凯夫县(Tel Kaif)
  - 泰勒阿费尔县(Tel Afar)
  - 辛贾尔县(Sinjar)
  - Al-Ba'aj 县
  - 哈塔拉县(Al-Hadar)

[Ninevehdistricts.jpg](https://zh.wikipedia.org/wiki/File:Ninevehdistricts.jpg "fig:Ninevehdistricts.jpg")

## 参考文献

## 延伸閱讀

  -
[\*](../Category/尼尼微省.md "wikilink")
[Category:伊拉克省份](../Category/伊拉克省份.md "wikilink")

1.
2.