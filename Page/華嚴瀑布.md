**華嚴瀑布**是位於[日本](../Page/日本.md "wikilink")[栃木縣](../Page/栃木縣.md "wikilink")[日光國立公園內的一條瀑布](../Page/日光國立公園.md "wikilink")，出自[中禪寺湖](../Page/中禪寺湖.md "wikilink")，其下接駁至[鬼怒川的其中一條支流](../Page/鬼怒川.md "wikilink")[大谷川](../Page/大谷川.md "wikilink")。華嚴瀑布形成的原因是中禪寺湖[水壓很大](../Page/水壓.md "wikilink")，造成湖底漏水，水便流下山谷而形成瀑布。

華嚴瀑-{布}-有「日本三大瀑布」之稱。1931年被定為日本國家名勝，2007年入圍[日本地質百選](../Page/日本地質百選.md "wikilink")。

## 交通

搭乘[栃木縣](../Page/栃木縣.md "wikilink")[日光市的](../Page/日光市.md "wikilink")[東武鐵道](../Page/東武鐵道.md "wikilink")[日光線到](../Page/日光線.md "wikilink")[東武日光站轉乘東武バス日光公司的大巴在中禅寺温泉站下車](../Page/東武日光站.md "wikilink")，步行五分鐘。

## 自殺勝地

在1903年（明治36年）5月22日，[藤村操在瀑布旁邊的樹上留下](../Page/藤村操.md "wikilink")「巌頭之感」（がんとうのかん）後自殺。

由於厭世的菁英學生自殺，帶給當時以「出人頭地」為美德的當時社會極大的影響。之後陸續出現許多追隨他自殺的人。即使在警察的嚴格監視下，依然有為數不少的人自殺。在藤村操死後的四年內，在華嚴瀑布企圖自殺的人高達185位（包括既遂的有40位），日本著名的出版商巖波茂雄也受到時為其師弟的藤村操的影響，萌生了了却此生的念頭，幸好被同窗阻攔。華嚴瀑布現在之所以成為自殺的有名地點，是由於藤村操的緣故。

藤村留下遺言的水楢樹之後遭[警方砍伐](../Page/警察.md "wikilink")。不過有保留當時的照片、現在在華嚴的瀑布被當作華嚴瀑布的土產販賣。

## 相關條目

  - [男體山](../Page/男體山.md "wikilink")
  - [中禪寺湖](../Page/中禪寺湖.md "wikilink")
  - [日本三大列表](../Page/日本三大列表.md "wikilink")
  - [日本瀑布百選](../Page/日本瀑布百選.md "wikilink")
  - [日本地質百選](../Page/日本地質百選.md "wikilink")

## 照片一覽

<File:Kegon> falls 2007-09-09.jpg|雨季水量大增
(2007年9月) <File:Lake> chuzenji and kegon
waterfall.jpg|華嚴瀑布源出[中禪寺湖](../Page/中禪寺湖.md "wikilink")
<File:Ganto> no kan.JPG|水楢樹上的巌頭之感

## 外部連結

  - [Nikko
    Falls](http://www.johnharveyphoto.com/Japan2/Nikko%20Falls/index.html)

[Category:栃木縣地理](../Category/栃木縣地理.md "wikilink")
[Category:日本瀑布](../Category/日本瀑布.md "wikilink")
[Category:日光市](../Category/日光市.md "wikilink")