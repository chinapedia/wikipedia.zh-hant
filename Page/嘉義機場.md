**嘉義航空站**（）是一座位於[臺灣](../Page/臺灣.md "wikilink")[嘉義縣](../Page/嘉義縣.md "wikilink")[水上鄉的](../Page/水上鄉.md "wikilink")[機場](../Page/機場.md "wikilink")，俗稱**嘉義水上機場**，簡稱**水上機場**，為軍民合用機場。[民用部分的管轄單位為](../Page/民用航空.md "wikilink")[中華民國](../Page/中華民國.md "wikilink")[交通部民用航空局](../Page/交通部民用航空局.md "wikilink")**嘉義航空站**，[軍用部分則為](../Page/軍用航空.md "wikilink")[中華民國](../Page/中華民國.md "wikilink")[空軍](../Page/中華民國空軍.md "wikilink")**嘉義基地**,駐紮有**空軍第四戰術戰鬥機聯隊(前第455戰術戰鬥機聯隊)**。空軍基地位於嘉義縣[水上鄉](../Page/水上鄉.md "wikilink")、[太保市與](../Page/太保市.md "wikilink")[嘉義市交界](../Page/嘉義市.md "wikilink")，航空站出口位於[嘉義縣](../Page/嘉義縣.md "wikilink")[水上鄉](../Page/水上鄉.md "wikilink")，距離[嘉義市大約有八公里](../Page/嘉義市.md "wikilink")，距離[中山高速公路](../Page/中山高速公路.md "wikilink")[水上交流道則大約有三公里](../Page/水上交流道.md "wikilink")。
[thumbF](../Page/file:16th_Fighter-Interceptor_Squadron_North_American_F-86D-35-NA_Sabre_51-6214_1955.jpg.md "wikilink")-86D戰機\]\]

## 歷史

水上機場興建於[台灣日治時期](../Page/台灣日治時期.md "wikilink")，時為軍方之航空基地。戰後成為中華民國空軍及[美國空軍](../Page/美國空軍.md "wikilink")[在台部隊基地](../Page/駐台美軍.md "wikilink")。

  - 1947年3月，[二二八事件波及當地及其鄰近村落](../Page/嘉義民兵.md "wikilink")。\[1\]
  - 1976年5月19日，成立嘉義民航候機室。
  - 1977年7月，航站大廈開工至同年12月完工。
  - 1978年1月1日，航站大廈正式啟用。
  - 1993年1月1日，升格為嘉義民航輔助站。
  - 1995年2月，航站大廈重新擴建，於11月完工啟用。
  - 1997年11月1日，升格為民航丙種航空站。
  - 2013年5月25日-29日，[中華航空包機直飛](../Page/中華航空.md "wikilink")[日本](../Page/日本.md "wikilink")[靜岡機場](../Page/靜岡機場.md "wikilink")。
  - 2014年4月30日，[春秋航空包機直飛](../Page/春秋航空.md "wikilink")[上海浦東國際機場](../Page/上海浦東國際機場.md "wikilink")。

## 航空公司與航點

## 公車資訊

北回歸線站/北回站

  - [嘉義客運](../Page/嘉義客運.md "wikilink")
      - 【7205】嘉義-朴子(經太保)
      - 【7206】嘉義-塭港
      - 【7209】嘉義-布袋(經朴子)
      - 【7229】嘉義-柳營
  - [嘉義縣公車](../Page/嘉義縣公車.md "wikilink")
      - 【7324】嘉義-朴子(經水上)
      - 【7327】嘉義-布袋(經朴子)

## 相關條目

  - [嘉義市](../Page/嘉義市.md "wikilink")
  - [水上鄉](../Page/水上鄉.md "wikilink")
  - [日本](../Page/日本.md "wikilink")[靜岡縣](../Page/靜岡縣.md "wikilink")[靜岡機場](../Page/靜岡機場.md "wikilink")
  - [上海浦東國際機場](../Page/上海浦東國際機場.md "wikilink")
  - [飛行第50戰隊 (日本陸軍)](../Page/飛行第50戰隊_\(日本陸軍\).md "wikilink")

## 參考文獻

  -
## 外部連結

  - [嘉義航空站](http://www.cya.gov.tw/)
  - [電子式飛航指南，CIVIL AERONAUTICS ADMINISTRATION MOTC
    R.O.C.](https://web.archive.org/web/20080704115523/http://eaip.caa.gov.tw/eaip/user_docs.aspx)

[分類:1976年啟用的機場](../Page/分類:1976年啟用的機場.md "wikilink")

[Category:嘉義市交通](../Category/嘉義市交通.md "wikilink")
[Category:嘉義縣交通](../Category/嘉義縣交通.md "wikilink")
[Category:台灣機場](../Category/台灣機場.md "wikilink")
[Category:台灣軍用機場](../Category/台灣軍用機場.md "wikilink")
[Category:水上鄉](../Category/水上鄉.md "wikilink")
[台](../Category/前美國空軍基地.md "wikilink")

1.