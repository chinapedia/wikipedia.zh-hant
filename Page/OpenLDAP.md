**OpenLDAP**是[轻型目录访问协议](../Page/轻型目录访问协议.md "wikilink")（Lightweight
Directory Access
Protocol，LDAP）的自由和[开源的实现](../Page/开源.md "wikilink")，在其OpenLDAP许可证下发行，并已经被包含在众多流行的[Linux发行版中](../Page/Linux.md "wikilink")。

它主要包括下述4个部分：

  - slapd - 独立LDAP守护服务
  - slurpd - 独立的LDAP更新复制守护服务
  - 实现LDAP协议的库
  - 工具软件和示例客户端

## 外部链接

  - [OpenLDAP.org](http://openldap.org)
  - [OpenLDAP许可证](http://www.openldap.org/doc/admin22/license.html)
  - [Lucas Bergman的hacks: Windows
    OpenLDAP](https://web.archive.org/web/20050717022252/http://lucas.bergmans.us/hacks/openldap/)

[Category:开放源代码](../Category/开放源代码.md "wikilink")
[Category:互聯網](../Category/互聯網.md "wikilink")