[Macrophage.jpg](https://zh.wikipedia.org/wiki/File:Macrophage.jpg "fig:Macrophage.jpg")體內的巨噬細胞，正在延伸其[假足以吞沒兩粒可能是](../Page/假足.md "wikilink")[病原體的顆粒](../Page/病原體.md "wikilink")。\]\]

**巨噬細胞**（，縮寫為**mφ**\[1\]）是一種位於[組織內的](../Page/組織_\(生物學\).md "wikilink")[白血球](../Page/白血球.md "wikilink")，源自[單核球](../Page/單核球.md "wikilink")，而單核球又來源於[骨髓中的前體细胞](../Page/骨髓.md "wikilink")。巨噬細胞和單核球皆為[吞噬細胞](../Page/吞噬細胞.md "wikilink")，在[脊椎動物體內參與非特異性防衛](../Page/脊椎動物.md "wikilink")（[先天性免疫](../Page/先天性免疫.md "wikilink")）和特異性防衛（[细胞免疫](../Page/细胞免疫.md "wikilink")）。它們的主要功能是以固定細胞或游離細胞的形式，對死亡细胞、細胞残片及[病原體進行](../Page/病原體.md "wikilink")[噬菌作用](../Page/噬菌作用.md "wikilink")（吞噬與消化），并激活[淋巴球或其他免疫細胞](../Page/淋巴球.md "wikilink")，加快其對病原體作出反應的時間。

## 生命週期

當單核球經[血管的](../Page/血管.md "wikilink")[內皮細胞層進入一已受損的組織時](../Page/內皮細胞層.md "wikilink")（這過程被稱為[白血球外滲作用](../Page/白血球外滲作用.md "wikilink")），它經過一連串轉變以成為巨噬細胞。單核球會因[化學趨向性而被化學物質的刺激吸引至受損處](../Page/化學趨向性.md "wikilink")，这些刺激包括受傷細胞、[病原體](../Page/病原體.md "wikilink")、由[肥大細胞和](../Page/肥大細胞.md "wikilink")[嗜鹼性球所釋放的](../Page/嗜鹼性細胞.md "wikilink")[組織胺](../Page/組織胺.md "wikilink")，以及由已於該處的巨噬細胞釋出的[細胞因子](../Page/細胞因子.md "wikilink")。在某些地方，如[睪丸](../Page/睪丸.md "wikilink")，巨噬細胞已被證實會透過增殖以移殖於此。

与壽命較短的[嗜中性球不同](../Page/嗜中性細胞.md "wikilink")，其壽命可达數個月至數年不等。\[2\]

## 功能

[**巨噬細胞攝取病原體之步驟：**
**a.**透過吞噬作用攝取[病原體](../Page/病原体.md "wikilink")，形成一[吞噬體](../Page/吞噬體.md "wikilink")
**b.**
[溶酶體融入吞噬體並形成](../Page/溶酶體.md "wikilink")[吞解體](../Page/吞解體.md "wikilink")；病原體被酶所分解
**c.**廢料被排出或[同化](../Page/同化作用.md "wikilink")（後者則未被繪成圖象）
**部分：**
**1.**病原體
**2.**吞噬體
**3.**溶酶體
**4.**廢料
**5.** [細胞質](../Page/細胞質.md "wikilink")
**6.**
[細胞膜](../Page/細胞膜.md "wikilink")](https://zh.wikipedia.org/wiki/File:Phagocytosis.png "fig:巨噬細胞攝取病原體之步驟： a.透過吞噬作用攝取病原體，形成一吞噬體 b. 溶酶體融入吞噬體並形成吞解體；病原體被酶所分解 c.廢料被排出或同化（後者則未被繪成圖象） 部分： 1.病原體 2.吞噬體 3.溶酶體 4.廢料 5. 細胞質 6. 細胞膜")

### 吞噬作用

巨噬細胞的其中一個重要角色是移除[肺中的](../Page/肺.md "wikilink")[壞疽碎片及](../Page/壞疽.md "wikilink")[塵埃](../Page/塵埃.md "wikilink")。另外，在慢性[炎症中](../Page/炎症.md "wikilink")，移除已死細胞亦為重要。在炎症的早期，大量的[嗜中性球會佔據患處](../Page/嗜中性細胞.md "wikilink")。當這些細胞死去時，就會被巨噬細胞所攝取。

塵埃及壞疽組織的移除大部份由固定巨噬細胞負責，它們會駐守在一些戰略位置如肺臟、[肝臟](../Page/肝臟.md "wikilink")、神經中樞的組織、[骨](../Page/骨.md "wikilink")、[脾臟及結締組織](../Page/脾臟.md "wikilink")，以攝取外來物質如塵埃和病原體。在有需要時，还会召集游離巨噬細胞。

當病原體被巨噬細胞吞噬後，病原體會被困於食物泡中並稍後與[溶酶體融合](../Page/溶酶體.md "wikilink")。在[溶酶體中](../Page/溶酶體.md "wikilink")，[酶和有毒物質如](../Page/酶.md "wikilink")[過氧化物會把侵入者消化](../Page/過氧化物.md "wikilink")。但一些[細菌如](../Page/細菌.md "wikilink")[結核分枝桿菌及](../Page/結核分枝桿菌.md "wikilink")[利什曼原虫等病原體則對此消化方法產生抵抗及規避](../Page/利什曼原蟲.md "wikilink")。巨噬細胞可以同時消化多過一百个細菌，然而，它們最終會死於自己分泌的消化混合物。

### 在特異性免疫中的角色

巨噬細胞是一種具多用途多功能的細胞。作為體內的「清道夫」，它們會去除體內那些壞掉死去的細胞以及其他廢料。它們是眾多個細胞中首個「[呈递](../Page/抗原呈递.md "wikilink")」抗原的，所以它其中有一個重要的功能就是啟動一個免疫反應。另外，作為一個分泌細胞，單核球及巨噬細胞對於免疫反應的調整和炎症的發展尤為必需。這是因為它們會大量地產生出一系列強勁的化學物質（[單核因子](../Page/單核因子.md "wikilink")），其中包括[酶](../Page/酶.md "wikilink")、[補體蛋白和調節因子如](../Page/補體.md "wikilink")[白細胞介素1](../Page/白细胞介素1族.md "wikilink")。同時，因为它們身上有[淋巴因子感受器](../Page/淋巴因子.md "wikilink")，因此可以被激活为仅追击单一的入侵微生物或者[肿瘤细胞的状态](../Page/肿瘤.md "wikilink")。

在病原體被消化後，巨噬細胞會把相应[抗原](../Page/抗原.md "wikilink")（一個分子，常為病原體表面的蛋白質並被免疫系統作辨別之用）呈递予对應的[輔助型T細胞](../Page/輔助型T細胞.md "wikilink")。呈递过程中，它們将這些抗原整合至[細胞膜](../Page/細胞膜.md "wikilink")，并把抗原结合在[主要組織相容性複合體II型分子](../Page/主要組織相容性複合體.md "wikilink")（MHC
II）之上以告之其他白血球，雖然它有抗原在其表面，但它並非病原體。

随后，抗原呈递导致了[抗體的產生](../Page/抗體.md "wikilink")。产生的抗體會依附在病原體的抗原上，這使病原體更易被巨噬細胞的細胞膜黏著，甚至被巨噬細胞所吞噬。在一些情况下，有部份病原體能夠有效抵抗巨噬細胞的黏附。抗體覆蓋著抗原的情形，就如[魔鬼氈附著在某物的有毛表面上一样](../Page/魔鬼氈.md "wikilink")。

在[淋巴结中](../Page/淋巴结.md "wikilink")，吞噬了病原体的巨噬細胞的表面所呈现的抗原（MHC
II），會刺激I型[T輔助細胞](../Page/T輔助細胞.md "wikilink")（TH1）的增值，而其主因是巨噬細胞分泌的[白細胞介素12](../Page/白細胞介素12.md "wikilink")。当處於淋巴核的[B細胞通过其表面的抗体来识别位於細菌表面的相同但未处理的抗原時](../Page/B細胞.md "wikilink")，这些抗原則會被吞噬以及處理。其後，已被處理的抗原會通过B細胞表面上的MHCII进行呈递。已经增值的TH1上的受体开始對抗原—MHCII複合物进行辨認（在共刺激因子[CD40和CD](../Page/CD40.md "wikilink")40L存在的情况下），并导致B細胞产生可以幫助抗原[调理作用的抗體](../Page/调理作用.md "wikilink")，使得[吞噬細胞可以更好地清除細菌](../Page/吞噬細胞.md "wikilink")。

巨噬細胞亦會針對腫瘤細胞和一些受[菌類或](../Page/菌類.md "wikilink")[寄生物所感染的細胞作出防衛](../Page/寄生物.md "wikilink")。當一個T細胞辨認出異常細胞表面的獨特抗原時，T細胞會變成一個活躍的效應細胞以釋放一種被稱之為淋巴因子的化學介質，而這種物質會刺激巨噬細胞令其變得更具侵略性。其後，這些被激活的或「加强的」巨噬細胞則更容易去吞噬及消化這些已受感染的細胞。\[3\]這種巨噬細胞是不會對抗原產生特異性防衛反應，反而會攻擊所有在它被激活之地周围的細胞。\[4\]

## 固定巨噬細胞

大部份的巨噬細胞會聚集在一些易於被微生物入侵或囤積塵埃的组织器官中。每一種固定巨噬細胞都會因所在部位的不同而得到不同的特定名稱。

[macrophage.png](https://zh.wikipedia.org/wiki/File:macrophage.png "fig:macrophage.png")

|                                        |                                                             |
| -------------------------------------- | ----------------------------------------------------------- |
| **細胞名稱**                               | **位置**                                                      |
| [肺泡巨噬細胞](../Page/肺泡巨噬細胞.md "wikilink") | [肺的](../Page/肺.md "wikilink")[肺泡](../Page/肺泡.md "wikilink") |
| [組織細胞](../Page/組織細胞.md "wikilink")     | [結締組織](../Page/結締組織.md "wikilink")                          |
| [庫佛氏细胞](../Page/庫佛氏细胞.md "wikilink")   | [肝](../Page/肝.md "wikilink")                                |
| [神經膠質細胞](../Page/神經膠質細胞.md "wikilink") | [神經組織](../Page/神經組織.md "wikilink")                          |
| [上皮樣細胞](../Page/上皮樣細胞.md "wikilink")   | [肉芽腫](../Page/肉芽腫.md "wikilink")                            |
| [蝕骨細胞](../Page/蝕骨細胞.md "wikilink")     | [骨](../Page/骨.md "wikilink")                                |
| [竇内皮细胞](../Page/竇内皮细胞.md "wikilink")   | [脾](../Page/脾.md "wikilink")                                |
| [系膜細胞](../Page/腎小球內系膜細胞.md "wikilink") | [腎](../Page/腎.md "wikilink")                                |

[庫佛氏细胞的研究受到限制](../Page/庫佛氏细胞.md "wikilink")，因为只有来自活檢或屍檢的人類庫佛氏细胞可以用于[免疫組織化學分析](../Page/免疫組織化學分析.md "wikilink")。而要在老鼠中把之分離是非常困難的，而且在提純以後，亦只能從一隻鼠身上獲得大約五百萬個細胞。

巨噬細胞在[器官中亦有](../Page/器官.md "wikilink")[旁泌性功能](../Page/旁泌性.md "wikilink")，這也是為該器官的功能而獨有的。以[睪丸為例](../Page/睪丸.md "wikilink")，巨噬細胞能夠分泌[氧類固醇以及](../Page/氧類固醇.md "wikilink")[25-羥基膽固醇](../Page/25-羥基膽固醇.md "wikilink")（可以在[睪丸間質細胞附近轉換為](../Page/睪丸間質細胞.md "wikilink")[睾丸酮](../Page/睾丸酮.md "wikilink")）来与[睪丸間質細胞相互作用](../Page/睪丸間質細胞.md "wikilink")。而且，在[睪丸中睪丸巨噬細胞能够参与形成一個免疫特化的環境](../Page/睪丸.md "wikilink")，以及在睪丸發炎期間介导[不育](../Page/不育.md "wikilink")。

## 涉入病症的症狀

因為它的功能—吞噬，巨噬細胞亦涉入許多免疫系統的症狀。譬如它們會參與形成[肉芽瘤的過程](../Page/肉芽瘤.md "wikilink")，及可由多種病症引起的[炎症病變](../Page/炎症病變.md "wikilink")。

下文中會形容一些有關巨噬細胞功能和非有效吞噬的疾病，而大部份亦為罕見。

巨噬細胞亦被涉入動脈硬化症愈趨嚴重的血小板病變的具能力細胞。

當與[流行性感冒展開](../Page/流行性感冒.md "wikilink")「戰爭」時，巨噬細胞會被派至[喉嚨](../Page/喉嚨.md "wikilink")。但是在[殺手T細胞將感冒病毒找出前](../Page/细胞毒性T细胞.md "wikilink")，巨噬細胞是破壞多於幫忙。它們不只破壞受感冒病毒感染的喉嚨細胞，亦會破壞數個位處附近且未被感染的細胞。

當人體受到[人體免疫缺損病毒](../Page/人體免疫缺損病毒.md "wikilink")（HIV）感染時，如[T細胞般](../Page/T細胞.md "wikilink")，巨噬細胞也會受到感染，更在身體之中被變成持續複製的病毒的倉庫。

再者，巨噬細胞亦被相信它會幫助癌細胞增生。它們會被吸引到缺氧（[低氧](../Page/低氧.md "wikilink")）的腫瘤細胞附近并促进[慢性炎症](../Page/慢性炎症.md "wikilink")。巨噬細胞會釋出致炎物質如[腫瘤壞死因子](../Page/腫瘤壞死因子.md "wikilink")（TNF）从而活化[NF-κB的](../Page/NF-κB.md "wikilink")[基因](../Page/基因.md "wikilink")。其後，NF-κB會進入腫瘤細胞核並啟動多種可以停止[細胞凋亡及促進細胞增生及發炎的](../Page/細胞凋亡.md "wikilink")[蛋白質的产生](../Page/蛋白質.md "wikilink")。\[5\]

## 多媒體

## 相關細胞

  - [樹狀細胞](../Page/樹狀細胞.md "wikilink")（包括[郎格罕細胞](../Page/郎格罕細胞.md "wikilink")）
  - [泡沫細胞](../Page/泡沫細胞.md "wikilink")——）載[脂巨噬細胞](../Page/脂.md "wikilink")
  - [多核巨細胞](../Page/多核巨細胞.md "wikilink")

## 参见

  - [褐藻素](../Page/褐藻素.md "wikilink")
  - [免疫調節劑](../Page/免疫調節劑.md "wikilink")
  - [巨噬細胞極化](../Page/巨噬細胞極化.md "wikilink")

## 参考文献

## 外部連結

  - [*HIV and the
    Macrophage*](http://www.ressign.com/UserBookDetail.aspx?bkid=560&catid=161)《HIV與巨噬細胞》一本關於巨噬細胞對愛滋病病毒吞噬過程的書（ISBN
    81-7895-271-8）
  - [在HIV吞噬過程中的角色](http://hiv-mac.fedde.org/)

{{-}}

[巨噬细胞](../Category/巨噬细胞.md "wikilink")
[Category:吞噬细胞](../Category/吞噬细胞.md "wikilink")
[Category:细胞生物学](../Category/细胞生物学.md "wikilink")
[Category:免疫系統](../Category/免疫系統.md "wikilink")
[Category:人体细胞](../Category/人体细胞.md "wikilink")
[Category:结缔组织细胞](../Category/结缔组织细胞.md "wikilink")

1.  [Kidney International - Abstract of article:
    P-selection-dependent...](http://www.nature.com/ki/journal/v62/n1/abs/4493079a.html)

2.  [Macrophages and the Pathogenesis of
    COPD\*（巨噬細胞與COPD的病因）](http://www.chestjournal.org/cgi/content/full/121/5_suppl/156S)

3.

4.
5.  Gary Stix: "A Malignant Flame", *科學人雜誌*, July 2007, pages 46-49