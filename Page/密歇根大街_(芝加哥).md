[Michigan_Avenue_bridge_over_Chicago_River.jpg](https://zh.wikipedia.org/wiki/File:Michigan_Avenue_bridge_over_Chicago_River.jpg "fig:Michigan_Avenue_bridge_over_Chicago_River.jpg")

**密歇根大街**（**Michigan
Avenue**）是美国[芝加哥的一条南北向干道](../Page/芝加哥.md "wikilink")，跨越[芝加哥河並途經](../Page/芝加哥河.md "wikilink")[芝加哥水塔](../Page/芝加哥水塔.md "wikilink")、[芝加哥艺术学院](../Page/芝加哥艺术学院.md "wikilink")（Art
Institute of Chicago）、[千禧年公园](../Page/千禧公園.md "wikilink")（Millennium
Park）等地標，北邊止於[約翰漢考克中心](../Page/約翰漢考克中心.md "wikilink")（John Hancock
Center），是该市著名的[商业街](../Page/商业街.md "wikilink")。而芝加哥河北岸一段因其繁華景象拥有「[华丽一英里](../Page/华丽一英里.md "wikilink")」的美名。

## 历史

密歇根大街最早形成的部分是现在接近[格兰特公园](../Page/格兰特公园.md "wikilink")（Grant
Park）的一段。它因密歇根湖而得名。1871年以前，密歇根湖紧邻密歇根大街的东侧。当时这条街北到芝加哥河，南到市界。起初密歇根大街基本上是住宅区。在1860年代，昂贵的大住宅主宰着密歇根大街。1871年[芝加哥大火以后](../Page/芝加哥大火.md "wikilink")，密歇根大街上从议会街到芝加哥河的所有建筑全被烧毁。大火之后密歇根大街立即得到重建，仍然保持了住宅区的特色，但是已经不再直接濒临密歇根湖

## 北密歇根大街与华丽一英里

密歇根大街起初没有桥，终止于芝加哥河。现在密歇根大街在芝加哥河以北的部分原名松树街，最初附近散布松树。1909年Daniel
Burnham在芝加哥规划中提议连接密歇根大街和松树街，到1917年，开始修建桥梁。当桥梁建成时，松树街改名为密歇根大街。北面终止于[湖滨大道](../Page/湖滨大道.md "wikilink")（Lake
Shore Drive）近[約翰漢考克中心](../Page/約翰漢考克中心.md "wikilink")（John Hancock
Center）。

今天，密歇根大街在芝加哥河以北的部分称为“[华丽一英里](../Page/华丽一英里.md "wikilink")”，拥有高档百货公司、餐馆、写字楼和旅馆。

[MgnificentMile_Chicago.jpg](https://zh.wikipedia.org/wiki/File:MgnificentMile_Chicago.jpg "fig:MgnificentMile_Chicago.jpg")

## 密歇根大街河南段

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  - [芝加哥的华丽一英里](http://www.themagnificentmile.com)

[Category:芝加哥](../Category/芝加哥.md "wikilink")
[Category:芝加哥街道](../Category/芝加哥街道.md "wikilink")