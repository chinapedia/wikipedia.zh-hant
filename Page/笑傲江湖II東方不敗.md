是1992年根据[金庸小说](../Page/金庸.md "wikilink")《[笑傲江湖](../Page/笑傲江湖.md "wikilink")》改编的一部电影，由[李连杰和](../Page/李连杰.md "wikilink")[林青霞分别扮演](../Page/林青霞.md "wikilink")[令狐冲与](../Page/令狐冲.md "wikilink")[東方不敗](../Page/東方不敗.md "wikilink")，其他演员还有[關之琳](../Page/關之琳.md "wikilink")、[李嘉欣等](../Page/李嘉欣.md "wikilink")。

## 劇情

華山大弟子令狐沖與[岳靈珊前往任盈盈所在野店與大暴牙](../Page/岳靈珊.md "wikilink")、傻屁股、陸大有等六個師兄弟相會，欲一同退出江湖。卻受任盈盈之託尋找其失蹤的父親，日月神教前教主任我行，終卷入血腥的江湖紛爭。
[東方不敗取任我行而代之為教主](../Page/東方不敗.md "wikilink")，野心勃勃的他獲得武林秘笈《[葵花寶典](../Page/葵花寶典.md "wikilink")》後，不惜自宮苦練，望一統天下。其身心因為葵花寶典之故卻越發女性化。一日，令狐與東方相遇，將其誤認為女性，旋即互生情意。
東方礙於自己是男兒身，遂命愛妾詩詩在黑暗中與令狐纏綿，而伺機夜襲野店欲殺任氏父女，一眾華山弟子被殺。令狐決心上黑木崖復仇，與東方決一生死……
天下風雲出我輩，一入江湖歲月催，皇圖霸業談笑中，不勝人生一場醉。—— **令狐沖（李連杰）**與**東方不敗（林青霞）**飲酒時所朗誦。
提劍跨騎揮鬼雨，白骨如山鳥驚飛，塵事如潮人如水，只嘆江湖幾人回。—— **岳靈珊（李嘉欣）**將劍留在馬墳時所讀。

## 演員表

### 主演演員

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>粵語配音</strong></p></td>
<td><p><strong>備註</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/李連杰.md" title="wikilink">李連杰</a></p></td>
<td><p><a href="../Page/令狐沖.md" title="wikilink">令狐沖</a></p></td>
<td><p><a href="../Page/朱子聰.md" title="wikilink">朱子聰</a></p></td>
<td><p>華山派大弟子</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/林青霞.md" title="wikilink">林青霞</a></p></td>
<td><p><a href="../Page/東方不敗.md" title="wikilink">東方不敗</a></p></td>
<td><p>男：<a href="../Page/紹子逸.md" title="wikilink">紹子逸及女</a>：<a href="../Page/廖靜妮.md" title="wikilink">廖靜妮</a><br />
(粵語：<a href="../Page/何錦華.md" title="wikilink">何錦華</a>)</p></td>
<td><p>日月神教教主，詩詩之夫，半男半女，令狐沖好友，後忘恩負義，暗中与倭寇勾結想成為倭寇首領，後來為東西方不敗，劇中的大反派。參考<a href="../Page/東方不敗之風云再起.md" title="wikilink">東方不敗之風云再起</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/關之琳.md" title="wikilink">關之琳</a></p></td>
<td><p><a href="../Page/任盈盈.md" title="wikilink">任盈盈</a></p></td>
<td></td>
<td><p>任我行之女</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李嘉欣.md" title="wikilink">李嘉欣</a></p></td>
<td><p><a href="../Page/岳靈珊.md" title="wikilink">岳靈珊</a></p></td>
<td><p><a href="../Page/陸惠玲.md" title="wikilink">陸惠玲</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/李子雄.md" title="wikilink">李子雄</a></p></td>
<td><p>服部千軍</p></td>
<td></td>
<td><p>東瀛忍者，也是東方不敗身邊唯一的忠誠者，在聽從東方不敗命令執行圍殺任我行計劃時，被任我行用吸星大法吸乾頭顱而死</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/袁潔瑩.md" title="wikilink">袁潔瑩</a></p></td>
<td><p><a href="../Page/藍鳳凰.md" title="wikilink">藍鳳凰</a></p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 合演演員

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>粵語配音</strong></p></td>
<td><p><strong>備註</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/劉洵_(香港演員).md" title="wikilink">劉　洵</a></p></td>
<td><p><a href="../Page/向問天.md" title="wikilink">向問天</a>（向左使）</p></td>
<td><p><a href="../Page/高翰文.md" title="wikilink">高翰文</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/任世官.md" title="wikilink">任世官</a></p></td>
<td><p><a href="../Page/任我行.md" title="wikilink">任我行</a></p></td>
<td></td>
<td><p>日月神教教主<br />
任盈盈之父</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/余安安.md" title="wikilink">余安安</a></p></td>
<td><p>詩詩</p></td>
<td><p><a href="../Page/黃鳳英.md" title="wikilink">黃鳳英</a></p></td>
<td><p>東方不敗愛妾<br />
（特別客串）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/錢嘉樂.md" title="wikilink">錢嘉樂</a></p></td>
<td><p>猿飛日月</p></td>
<td><p><a href="../Page/謝君豪.md" title="wikilink">謝君豪</a></p></td>
<td><p>東瀛忍者，為了對付東瀛大將軍豐臣秀吉而屈就東方不敗，最後卻因一窺葵花寶典之秘而被東方不敗所殺（友情客串）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/張國樑.md" title="wikilink">張國樑</a></p></td>
<td></td>
<td></td>
<td><p>（友情客串）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/金揚樺.md" title="wikilink">金揚樺</a></p></td>
<td></td>
<td></td>
<td><p>（友情客串）</p></td>
</tr>
</tbody>
</table>

## 工作人員

  - 武術指導：[程小東](../Page/程小東.md "wikilink")、[元彬](../Page/元彬.md "wikilink")、[馬玉成](../Page/馬玉成.md "wikilink")、[張耀星](../Page/張耀星.md "wikilink")
  - 原著：[金庸](../Page/金庸.md "wikilink")
  - 服裝指導：[張叔平](../Page/張叔平.md "wikilink")、[余家安](../Page/余家安.md "wikilink")
  - 策劃：[程小東](../Page/程小東.md "wikilink")
  - 武師：[藍海翰](../Page/藍海翰.md "wikilink")、[余偉強](../Page/余偉強.md "wikilink")、[陳耀倫](../Page/陳耀倫.md "wikilink")、[王偉順](../Page/王偉順.md "wikilink")、[何賽舟](../Page/何賽舟.md "wikilink")、[汪禹](../Page/汪禹.md "wikilink")
  - 特技：[新視覺特技工作室](../Page/新視覺特技工作室.md "wikilink")
  - 執行製片：[佘偉森](../Page/佘偉森.md "wikilink")、[張志偉](../Page/張志偉.md "wikilink")
  - 助理製片：[劉思樂](../Page/劉思樂.md "wikilink")
  - 副導演：[朱偉光](../Page/朱偉光.md "wikilink")、[姚佑雄](../Page/姚佑雄.md "wikilink")、[許安](../Page/許安.md "wikilink")
  - 助理美指：[陳國仁](../Page/陳國仁.md "wikilink")
  - 助理服指：[劉世運](../Page/劉世運.md "wikilink")、[郭澤潔](../Page/郭澤潔.md "wikilink")、[陳秀婷](../Page/陳秀婷.md "wikilink")、[吳翠華](../Page/吳翠華.md "wikilink")
  - 助理攝影：[姜國文](../Page/姜國文.md "wikilink")、[馮偉倫](../Page/馮偉倫.md "wikilink")、[馮遠文](../Page/馮遠文.md "wikilink")
  - 後期製作：[林安兒](../Page/林安兒.md "wikilink")、[陳志偉](../Page/陳志偉.md "wikilink")
  - 場記：[鍾侃爕](../Page/鍾侃爕.md "wikilink")
  - 燈光：[陳偉年](../Page/陳偉年.md "wikilink")
  - 助理燈光：[王永志](../Page/王永志.md "wikilink")
  - 劇務：[吳錦超](../Page/吳錦超.md "wikilink")
  - 事務：[曾招兒](../Page/曾招兒.md "wikilink")
  - 場務：[陳梓存](../Page/陳梓存.md "wikilink")、[胡振強](../Page/胡振強.md "wikilink")
  - 化妝：[文潤玲](../Page/文潤玲.md "wikilink")、[賴家碧](../Page/賴家碧.md "wikilink")
  - 梳頭：[仇小梅](../Page/仇小梅.md "wikilink")、[溫玉梅](../Page/溫玉梅.md "wikilink")
  - 服裝管理：[楊蓮妹](../Page/楊蓮妹.md "wikilink")、[陳寶娟](../Page/陳寶娟.md "wikilink")、[邵靜儀](../Page/邵靜儀.md "wikilink")、[郭美玲](../Page/郭美玲.md "wikilink")
  - 道具：[李坤龍](../Page/李坤龍.md "wikilink")
  - 劇照：[林金鈴](../Page/林金鈴.md "wikilink")
  - 茶水：[羅麗香](../Page/羅麗香.md "wikilink")
  - 粵語對白：[林安兒](../Page/林安兒.md "wikilink")
  - 國語對白：[林曉萍](../Page/林曉萍.md "wikilink")
  - 效果：[鄺偉雄](../Page/鄺偉雄.md "wikilink")
  - 錄音／混音：[周錦榮](../Page/周錦榮.md "wikilink")
  - 字幕：[晶藝字幕公司](../Page/晶藝字幕公司.md "wikilink")
  - 錄音室：[九龍灣電影後期製作有限公司](../Page/九龍灣電影後期製作有限公司.md "wikilink")
  - 原著音樂：[袁卓繁](../Page/袁卓繁.md "wikilink")
  - 器材：[Salon Films (HK)
    Ltd.](../Page/Salon_Films_\(HK\)_Ltd..md "wikilink")
  - 沖印：[東方電影沖印有限公司](../Page/東方電影沖印有限公司.md "wikilink")

## 電影音樂

|        |         |                                |                                |                                  |                                                                           |                  |
| ------ | ------- | ------------------------------ | ------------------------------ | -------------------------------- | ------------------------------------------------------------------------- | ---------------- |
| **曲別** | **歌名**  | **作詞**                         | **作曲**                         | **編曲**                           | **演唱者**                                                                   | **備註**           |
| 主題曲    | 〈滄海一聲笑〉 | [黃霑](../Page/黃霑.md "wikilink") | [黃霑](../Page/黃霑.md "wikilink") | [顧嘉煇](../Page/顧嘉煇.md "wikilink") | [羅文](../Page/羅文.md "wikilink")、[周小君](../Page/周小君.md "wikilink")           | BMG Pacific Ltd. |
| 插曲     | 〈只記今朝笑〉 | [黃霑](../Page/黃霑.md "wikilink") | [黃霑](../Page/黃霑.md "wikilink") | [顧嘉煇](../Page/顧嘉煇.md "wikilink") | [林青霞](../Page/林青霞.md "wikilink")(國語版)、[呂珊](../Page/呂珊.md "wikilink")(粵語版) | BMG Pacific Ltd. |

## 得獎紀錄

<table>
<thead>
<tr class="header">
<th><p>頒獎典禮</p></th>
<th><p>獎項</p></th>
<th><p>名字</p></th>
<th><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/1993年香港電影金像獎.md" title="wikilink">第12屆香港電影金像獎</a></p></td>
<td><p>最佳女主角</p></td>
<td><p><a href="../Page/林青霞.md" title="wikilink">林青霞</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳剪接</p></td>
<td><p><a href="../Page/麥子善.md" title="wikilink">麥子善</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳美術指導</p></td>
<td><p><a href="../Page/梁華生.md" title="wikilink">梁華生</a>、<a href="../Page/鍾移風.md" title="wikilink">鍾移風</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳服裝造型設計</p></td>
<td><p><a href="../Page/張叔平.md" title="wikilink">張叔平</a>、<a href="../Page/余家安.md" title="wikilink">余家安</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳動作指導</p></td>
<td><p><a href="../Page/程小東.md" title="wikilink">程小東</a>、<a href="../Page/元彬.md" title="wikilink">元彬</a>、<a href="../Page/馬玉成.md" title="wikilink">馬玉成</a>、<a href="../Page/張耀星.md" title="wikilink">張耀星</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳電影配樂</p></td>
<td><p><a href="../Page/袁卓凡.md" title="wikilink">袁卓凡</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳電影歌曲</p></td>
<td><p>〈只記今朝笑〉作曲：<a href="../Page/黃霑.md" title="wikilink">黃霑</a>、作詞：<a href="../Page/黃霑.md" title="wikilink">黃霑</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/第29屆金馬獎.md" title="wikilink">第29屆金馬獎</a></p></td>
<td><p>最佳改編劇本</p></td>
<td><p><a href="../Page/徐克.md" title="wikilink">徐克</a>、<a href="../Page/陳天璇.md" title="wikilink">陳天璇</a>、<a href="../Page/鄧碧燕.md" title="wikilink">鄧碧燕</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳造型設計</p></td>
<td><p><a href="../Page/張叔平.md" title="wikilink">張叔平</a>、<a href="../Page/余家安.md" title="wikilink">余家安</a></p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 角色原型

徐克多次提及東方不敗的角色靈感為新蜀山劍俠中林青霞的血魔造型。
徐克：「我接觸東方不敗的第一印象就即時聯想起蜀山裡林青霞的妖女造型，一個結合邪惡與美麗的造型。」
香港電影類型論 羅卡 p54

他在永遠的林青霞一書，偶像來了我們的偶像林青霞特輯中曾重複類似的故事。我們的偶像中尤其詳細。
另外，林青霞和施南生也曾在著作和正式的訪問中提及此事，足見東方不敗一角令演員和創造者皆再三回味。

## 電影影響

認為，電影東方不敗出乎意料的影響深遠。

  - 掀起香港新派武俠電影浪潮
    作為一部里程碑式的武俠電影，《東方不敗》可以說是“新派武俠”這個時尚名詞的代言和奠基者。片中美倫美奐的場景；瀟灑飄逸的動作；動人心魄的樂曲令所有一代人為之動容、為之記憶留心。

<!-- end list -->

  - 創造了亞洲電影的一位完美偶像
    東方不敗這個角色使林青霞成為亞洲電影的一位完美偶像，讓她完成了由台灣瓊瑤電影中純情女生到一代電影女神的蛻變，再創事業高峰，成為90年代初期香港影壇最炙手可熱的女星，亦在南韓多次打入優先雜誌最受歡迎外國明星的排行榜。\[1\]

<!-- end list -->

  - 至今影響網路流行文化：當網絡文學興起時，東方不敗成為大受歡迎的同人小說主角之一。
    據2012年媒體粗略統計，在著名的華語原創文學網站起點網上有218部以東方不敗為主角的同人小說，而晉江原創網上東方不敗做主角的小說數量達到了442部，其中很多作品的總字數超過10萬字。Cosplay中，也出現的模仿電影造型，或高度原創造型的東方不敗，其共同點是高度美化和唯持紅色系為基調。大大有別原作中被令狐冲諷為“老旦”的東方不敗形象。

<!-- end list -->

  - 流行用語
    隨著電影《笑傲江湖之東方不敗》票房告捷，東方不敗一詞忽然也“一夜成名”，成為媒體流行語新寵。由聯合報系的標題查找系統看來，1951年到1992年，東方不敗一詞出現過11次，皆為電影笑傲江湖之東方不敗的相關消息，而1992年到2006年，東方不敗出現在標題出現407次，已不必然和電影東方不敗，或金庸原作中的東方不敗相關，例如“東方不敗張清芳”“「東方不敗」沒被電暈暴龍逆轉雷霆奪5連勝”等等。
    現在提東方不敗，可以是指林青霞，也可能是《[機動武鬥傳G高達](../Page/機動武鬥傳G高達.md "wikilink")》同名角色、個性別曖昧的角色，亦可能是指某檔強勁的股票。

## 參見

  - [笑傲江湖【吕颂贤】版](http://www.03ka.com/dianshiju/index12036.html)

## 其他

有趣的是，後來的觀眾開始把《笑傲江湖II東方不敗》和《東方不敗之風雲再起》等港產武俠片視作「[邪典電影](../Page/邪典電影.md "wikilink")」（Cult
Film）來欣賞。

日本動畫《[機動武鬥傳G鋼彈](../Page/機動武鬥傳G鋼彈.md "wikilink")》導演[今川泰宏受香港電影影響甚深](../Page/今川泰宏.md "wikilink")，動畫中主角的師父名為「東方不敗」，為電影「笑傲江湖二之東方不敗」同名主角；東方不敗的愛馬取名「風雲再起」，名稱來自電影東方不敗之風雲再起，所以東方不敗的愛駒就起名為風雲再起。

## 外部連結

  - {{@movies|fCcmb9092053}}

  -
  -
  -
  -
  -
  -
## 參考資料

[悅博副刊第一期 東方不敗二十年](http://blog.sina.com.cn/lm/z/Swordsman2/)

[Category:程小東電影](../Category/程小東電影.md "wikilink")
[2](../Category/1990年代香港電影作品.md "wikilink")
[Category:續集電影](../Category/續集電影.md "wikilink")
[Category:笑傲江湖改編影片](../Category/笑傲江湖改編影片.md "wikilink")
[Category:香港动作片](../Category/香港动作片.md "wikilink")
[Category:香港電影金像獎最佳服裝造型設計獲獎電影](../Category/香港電影金像獎最佳服裝造型設計獲獎電影.md "wikilink")

1.