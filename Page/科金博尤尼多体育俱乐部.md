**科金博尤尼多体育俱乐部**（**Coquimbo
Unido**）是智利足球俱乐部，位于[科金博](../Page/科金博.md "wikilink")。俱乐部创建于1957年8月15日，现为[智利足球甲级联赛球队](../Page/智利足球甲级联赛.md "wikilink")。球队主体育场为拥有15,000个座位的弗朗西斯科·桑切斯·拉莫罗索体育场（Estadio
Francisco Sánchez Rumoroso）。

## 球队荣誉

  - [智利足球乙级联赛](../Page/智利足球乙级联赛.md "wikilink") champions: 1962, 1977,
    1990

## 球队荣誉

  - [马赛罗·科拉莱斯](../Page/马赛罗·科拉莱斯.md "wikilink")

  -  [贾维尔·迪·格雷戈里奥](../Page/贾维尔·迪·格雷戈里奥.md "wikilink")

  - [路易斯·弗恩特斯](../Page/路易斯·弗恩特斯.md "wikilink")

  - [Patricio Galaz](../Page/Patricio_Galaz.md "wikilink")

  - [加马迪埃尔·加西亚](../Page/加马迪埃尔·加西亚.md "wikilink")

  - [Ali Manouchehri](../Page/Ali_Manouchehri.md "wikilink")

  - [Pedro González Vera](../Page/Pedro_González_Vera.md "wikilink")

  - [Rubén Gómez](../Page/Rubén_Gómez_\(footballer\).md "wikilink")

  - [Pablo Lenci](../Page/Pablo_Lenci.md "wikilink")

  -  [胡安·马努·卢塞罗](../Page/胡安·马努·卢塞罗.md "wikilink")

  - [劳尔·帕拉西奥斯](../Page/劳尔·帕拉西奥斯.md "wikilink")

  - [埃克托尔·罗夫莱斯](../Page/埃克托尔·罗夫莱斯.md "wikilink")

  - [维克托里诺·洛佩斯·加西亚](../Page/维克托里诺·洛佩斯·加西亚.md "wikilink")

  - [亚历克斯·瓦拉斯](../Page/亚历克斯·瓦拉斯.md "wikilink")

  - [卡梅洛·维加](../Page/卡梅洛·维加.md "wikilink")

## 外部链接

  - [Coquimbo News Website](http://www.coquimbounido.cl/)

{{-}}

[Category:智利足球俱乐部](../Category/智利足球俱乐部.md "wikilink")