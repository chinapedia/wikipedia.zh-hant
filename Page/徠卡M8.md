**Leica M8**是一部[Leica
M系列的数码旁轴相机](../Page/莱卡.md "wikilink")，于2006年9月14日发布。M8配备由[柯达制造的](../Page/柯达.md "wikilink")27mm
x
18mm[CCD](../Page/CCD.md "wikilink")，具备1030万像素[分辨率](../Page/分辨率.md "wikilink")。使用[SD卡进行存储](../Page/SD卡.md "wikilink")。

发售之后有部分用户反应M8拍摄的图像具有带状条纹，同时，黑色物体显示为紫色。[Leica公司于](../Page/莱卡.md "wikilink")2006年11月28日宣布M8数码[旁轴相机将延后发售](../Page/旁轴相机.md "wikilink")，并且还将在2007年2月前免费赠送每位用户2个M镜头专用的
UV/IR 滤镜。同时免费修理相关的故障。

<File:Leica-M8-IMG_0092.JPG> <File:Leica_M8_img_0742.jpg>
<File:Leica_M8_IMG_0483.JPG> <File:Leica_M8_IMG_0671.JPG>

## 参见

  - [徕卡](../Page/徕卡.md "wikilink")

## 外部链接

  - [徕卡M8——数字化的M型相机](http://www.xitek.com/info/showarticle.php?id=4419)－色影无忌
  - [Leica
    M8延迟销售](http://www.xitek.com/info/showarticle.php?id=4646)－色影无忌

[Category:数码照相机](../Category/数码照相机.md "wikilink")
[Category:数码旁轴相机](../Category/数码旁轴相机.md "wikilink")
[Category:2006年面世的相機](../Category/2006年面世的相機.md "wikilink")