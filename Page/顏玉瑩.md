**顏玉瑩**（**Gan
Geok-Eng**，），是一位出生於[福建省](../Page/福建省.md "wikilink")[漳州](../Page/漳州.md "wikilink")[海澄縣的](../Page/海澄縣.md "wikilink")[商人](../Page/商人.md "wikilink")。他是[白花油的始創人](../Page/白花油.md "wikilink")。

## 生平

### 家族與早年生活

[顏玉瑩的家族散布於](../Page/顏玉瑩家族.md "wikilink")[金門](../Page/金門.md "wikilink")、[廈門等地](../Page/廈門.md "wikilink")。顏玉瑩的[父親是一位](../Page/父親.md "wikilink")[中醫師](../Page/中醫師.md "wikilink")；顏玉瑩在家中排行第三，上頭有[兄長與](../Page/兄長.md "wikilink")[姊姊各一位](../Page/姊姊.md "wikilink")。\[1\]

### 南洋謀生

顏玉瑩在七歲時同父親離開廈門，前往[南洋謀求新生活](../Page/南洋.md "wikilink")。隨後，他在[新加坡由基層小工做起](../Page/新加坡.md "wikilink")，也當過[小販](../Page/小販.md "wikilink")。在經營多年後，顏玉瑩在[檳城開設](../Page/檳城.md "wikilink")[糖果](../Page/糖果.md "wikilink")[工廠並經營](../Page/工廠.md "wikilink")[橡膠事業](../Page/橡膠.md "wikilink")，也在新加坡經營一些小生意。不幸的是，他的事業大多在[二次世界大戰毀於戰火之中](../Page/二次世界大戰.md "wikilink")。\[2\]\[3\]

### 白花油事業

1927年，顏玉瑩在經過多年[研究和](../Page/研究.md "wikilink")[試驗發現利用](../Page/試驗.md "wikilink")[薄荷](../Page/薄荷.md "wikilink")、[樟腦](../Page/樟腦.md "wikilink")、[薰衣草](../Page/薰衣草.md "wikilink")、[桉葉等天然植物調製而成之藥油及](../Page/桉树.md "wikilink")[藥膏極具](../Page/藥膏.md "wikilink")[療效](../Page/療效.md "wikilink")；該[配方可用於緩解](../Page/配方.md "wikilink")[感冒](../Page/感冒.md "wikilink")[頭痛](../Page/頭痛.md "wikilink")、[中暑](../Page/中暑.md "wikilink")[暈眩及](../Page/暈眩.md "wikilink")[蚊蟲叮咬等日常疾病](../Page/蚊蟲叮咬.md "wikilink")。因該藥油之氣味近似[水仙之氣味](../Page/水仙.md "wikilink")，且他的[妻子也喜愛該種](../Page/妻子.md "wikilink")[花卉](../Page/花卉.md "wikilink")（該種花卉在[南洋又名](../Page/南洋.md "wikilink")「白花」），他於是將之命名為「[白花油](../Page/白花油.md "wikilink")」。

1935年，他在[檳城及](../Page/檳城.md "wikilink")[新加坡註冊](../Page/新加坡.md "wikilink")「白花」[商標](../Page/商標.md "wikilink")，同時在新加坡設廠生產。

1951年，在白花油事業於檳城及新加坡取得成功之後，顏玉瑩前往[香港成立](../Page/香港.md "wikilink")[工廠](../Page/工廠.md "wikilink")，並在當地採取一系列策略性[市場推廣活動](../Page/市場推廣.md "wikilink")；其中，他利用了獨特的大型[戶外廣告](../Page/户外广告机.md "wikilink")，並聘請知名[粵劇](../Page/粵劇.md "wikilink")[藝人協助宣傳](../Page/藝人.md "wikilink")。

1953年起，他積極參與由[香港中華廠商聯合會主辦的](../Page/香港中華廠商聯合會.md "wikilink")「[香港工業產品展覽會](../Page/工展會.md "wikilink")」，以逐步確立「白花油」的[市場地位](../Page/市場佔有率.md "wikilink")。此後，當「白花油」正式在香港量產後，顏玉瑩便將在[新加坡的廠房結束營運](../Page/新加坡.md "wikilink")。

1962年，他在[臺灣設立](../Page/臺灣.md "wikilink")「和興白花油廠」量產「白花油」、「白花膏」及「可白牙膏」等產品，並將之銷往新加坡、香港及當地[市場](../Page/市場.md "wikilink")。\[4\]\[5\]\[6\]\[7\]

### 與蔣氏政權的聯繫

[顏玉瑩與蔣宋美齡.jpg](https://zh.wikipedia.org/wiki/File:顏玉瑩與蔣宋美齡.jpg "fig:顏玉瑩與蔣宋美齡.jpg")\]\]
戰後，顏玉瑩被[新加坡僑界推為代表前往](../Page/新加坡華人.md "wikilink")[南京祝賀抗戰勝利](../Page/南京.md "wikilink")，並首次與當時擔任[軍事委員會委員長的](../Page/軍事委員會.md "wikilink")[蔣介石見面](../Page/蔣介石.md "wikilink")。

在[國民政府撤退至臺灣後](../Page/臺灣戰後時期.md "wikilink")，顏玉瑩更年年前往[臺灣向蔣介石](../Page/臺灣.md "wikilink")[祝壽並參與島內的各項](../Page/祝壽.md "wikilink")[慈善事業及](../Page/慈善事業.md "wikilink")[官方慶祝活動](../Page/官方.md "wikilink")，又不斷投資於當地；此時，他也多次獲得[當局頒給](../Page/蔣中正政府.md "wikilink")[獎牌](../Page/獎牌.md "wikilink")、[匾額及](../Page/匾額.md "wikilink")[勳章](../Page/勳章.md "wikilink")，並屢次獲得高層接見。\[8\]\[9\]

### 公益事業

他曾在[香港辦理](../Page/香港.md "wikilink")「白花油慈善皇后」[義賣活動](../Page/義賣.md "wikilink")，並積極響應[中國大陸災胞救濟總會](../Page/中國大陸災胞救濟總會.md "wikilink")、[紅十字會空飄](../Page/紅十字會.md "wikilink")[物資等](../Page/物資.md "wikilink")[慈善活動](../Page/慈善.md "wikilink")，且因此多次獲得[中華民國政府表揚](../Page/中華民國政府.md "wikilink")。此外，他也全力支持[臺灣省警察廣播電臺的](../Page/警察廣播電臺.md "wikilink")「雪中送炭」節目，並多次親身參與發送[棉被及](../Page/棉被.md "wikilink")[慰問金的工作](../Page/慰問金.md "wikilink")。\[10\]

他更籌得[經費開辦](../Page/經費.md "wikilink")[西環鐘聲小學](../Page/西環.md "wikilink")，又設立香港白花油慈善會，並將每年所得[盈餘捐與相關教育機構及其它](../Page/盈餘.md "wikilink")[慈善組織](../Page/慈善組織.md "wikilink")。

他曾擔任[東華三院副主席及總理](../Page/東華三院.md "wikilink")，也擔任過[保良局總理及主席等職務](../Page/保良局.md "wikilink")。\[11\]\[12\]

1977年，顏玉瑩參與[颱風災後](../Page/1977年太平洋颱風季.md "wikilink")[賑濟活動](../Page/人道援助.md "wikilink")，並因此成為第一位獲得[褒揚令的](../Page/褒揚令.md "wikilink")[華僑](../Page/華僑.md "wikilink")。\[13\]

## 身後

顏福成於2010年在臺灣成立「顏玉瑩和興化工廠有限公司」，又於2014年在香港成立「玉瑩有限公司」，並將「顏玉瑩」[商標於二十多個](../Page/商標.md "wikilink")[國家完成登記註冊](../Page/國家.md "wikilink")。\[14\]\[15\]

## 參見

  - [顏福偉](../Page/顏福偉.md "wikilink")
  - [顏玉瑩家族](../Page/顏玉瑩家族.md "wikilink")

## 參考資料

  - 呂志華，《香港企業大王》，明報出版社
  - 吳文明，《香港企業家的成功之道》

[分類:閩南裔新加坡人](../Page/分類:閩南裔新加坡人.md "wikilink")
[G](../Page/category:顏姓.md "wikilink")

[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:新加坡企業家](../Category/新加坡企業家.md "wikilink")
[Category:龙海人](../Category/龙海人.md "wikilink")

1.

2.

3.
4.

5.
6.

7.

8.

9.
10.
11.

12.
13.
14.
15.