**莫罗氏反射**（**Moro
reflex**）是一种[新生儿反射](../Page/新生儿反射.md "wikilink")。由[奥地利儿科专家](../Page/奥地利.md "wikilink")[欧内斯特·莫罗](../Page/欧内斯特·莫罗.md "wikilink")（Ernst
Moro，1874-1951）首先发现和描述。當變換嬰兒的位置或姿勢時，便會出現出雙手迅速向外伸張，然後再復原作擁抱狀。

该[反射在以下情形中出现](../Page/反射.md "wikilink")：

1.  突然朝着婴儿大喊一声
2.  当婴儿感觉到正在下落。（以水平姿势抱住婴儿，将其头的一端向下移动）

据相信，人类在新生儿时尚未习得恐惧。根据[小艾伯特实验](../Page/小艾伯特实验.md "wikilink")（little
Albert）的研究，在著名的[古典条件反射实验中](../Page/古典条件反射.md "wikilink")，用惊跳反射使他对毛茸茸的白色物体产生恐惧。

该[反射的最重要的意义是评估](../Page/反射.md "wikilink")[中枢神经系统](../Page/中枢神经系统.md "wikilink")（CNS）的整合情况，因为该反射包括4个组成部分：

1.  惊跳
2.  双臂向两边伸展（[外展肌](../Page/外展肌.md "wikilink"))
3.  双臂向向胸前合拢，作出拥抱姿势（[内收肌](../Page/内收肌.md "wikilink")）
4.  [哭喊](../Page/哭喊.md "wikilink")（通常）

该反射从出生起就开始出现。对于[早产儿](../Page/早产儿.md "wikilink")，可以在产后第28周观察到莫罗氏反射的完全形式；对于足月出生的新生儿，通常是在第34周观察到莫罗氏反射的完全形式。在正常情形下，莫罗氏反射会持续到[产后第](../Page/产后.md "wikilink")4-5个月时消失。

对于新生儿而言，出现以上每个组成部分\[1\]或运动中的每个[不对称现象都是正常的](../Page/不对称现象.md "wikilink")，但如果该反射一直持续到婴儿后期、儿童和[成人](../Page/成人.md "wikilink")，则属于反常现象。

## 参见

  - [神经系统](../Page/神经系统.md "wikilink")
  - [反射](../Page/反射_\(生理學\).md "wikilink")
  - [达尔文反射](../Page/达尔文反射.md "wikilink")（Darwinian
    reflex）、[罗曼尼斯反射或](../Page/达尔文反射.md "wikilink")[抓握反射](../Page/达尔文反射.md "wikilink")　
  - [巴宾斯基反射](../Page/巴宾斯基反射.md "wikilink")（Babinski reflex）
  - [游泳反射](../Page/游泳反射.md "wikilink")（Swimming reflex）
  - [瞳孔反射](../Page/瞳孔反射.md "wikilink")
  - [吮吸反射](../Page/吮吸反射.md "wikilink")
  - [定向反射](../Page/定向反射.md "wikilink")
  - [巴布金反射](../Page/巴布金反射.md "wikilink")
  - [行走反射](../Page/行走反射.md "wikilink")
  - [蜷缩反射](../Page/蜷缩反射.md "wikilink")
  - [击剑反射](../Page/击剑反射.md "wikilink")

## 注释

<references />

## 外部链接

  - [Medline Plus](http://medlineplus.gov/):
  - [莫罗氏反射](http://www.nlm.nih.gov/medlineplus/ency/article/003293.htm)

[Category:神经系统](../Category/神经系统.md "wikilink")
[Category:儿童心理学](../Category/儿童心理学.md "wikilink")

1.  除了哭喊