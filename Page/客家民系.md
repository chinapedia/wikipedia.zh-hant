**客家民系**，俗稱「**客家族群**」或「**客家人**」，在[西方](../Page/西方.md "wikilink")[人類學上被簡稱作](../Page/人類學.md "wikilink")「」（），是[漢族的一個分支](../Page/漢族.md "wikilink")，也是[漢族影響深遠的](../Page/漢族.md "wikilink")[民系之一](../Page/民系.md "wikilink")，為[漢族唯一不以](../Page/漢族.md "wikilink")[地域命名之漢族](../Page/地域.md "wikilink")[民系](../Page/民系.md "wikilink")。

該民系分佈於[南方各地](../Page/南方.md "wikilink")，如[廣東東部](../Page/廣東.md "wikilink")（[粵東](../Page/粵東.md "wikilink")）、北部（[粵北](../Page/粵北.md "wikilink")）和西部（[粵西](../Page/粵西.md "wikilink")）片區，以及[廣西東南部和](../Page/廣西.md "wikilink")[福建西部](../Page/福建.md "wikilink")（[閩西](../Page/閩西.md "wikilink")）、[江西南部](../Page/江西.md "wikilink")（[贛南](../Page/贛南.md "wikilink")）、[台湾西北部](../Page/台湾.md "wikilink")（[桃竹苗](../Page/桃竹苗.md "wikilink")）及南部（[高屏](../Page/高屏.md "wikilink")）片區為主要集中地，在[四川](../Page/四川.md "wikilink")、[湖南](../Page/湖南.md "wikilink")、[海南](../Page/海南.md "wikilink")、[浙江](../Page/浙江.md "wikilink")、[香港等地亦有部分客家人聚居](../Page/香港.md "wikilink")，近代亦有些回遷[陜西](../Page/陜西.md "wikilink")、[河南定居](../Page/河南.md "wikilink")。\[1\]主要由南遷的漢族組成，也融合了一些南方少數民族，形成了一种兼具[漢族文化與少数民族](../Page/漢族.md "wikilink")[文化的獨特](../Page/文化.md "wikilink")[民系](../Page/民系.md "wikilink")。

## 族群形成

關於客家渊源的看法主要有：

  - 中原[族群純血說](../Page/族群.md "wikilink")：客家籍歷史學家[羅香林於](../Page/羅香林.md "wikilink")1933年出版《客家研究導論》一書，主張客家民系是古代中原移民的後裔，歷經多次戰亂才從黃河流域逐漸移居到[漢地南部](../Page/九州_\(神州\).md "wikilink")。\[2\]\[3\]

<!-- end list -->

  - 中原[族群主體說](../Page/族群.md "wikilink")：客家人是[秦朝末至](../Page/秦朝.md "wikilink")[宋朝初](../Page/宋朝.md "wikilink")，[黃河流域漢族居民數次从](../Page/黃河流域.md "wikilink")[中原南遷](../Page/中原.md "wikilink")，抵達[粵](../Page/廣東.md "wikilink")、[閩](../Page/福建.md "wikilink")、[贛三地交界處](../Page/江西.md "wikilink")，與當地少數[族群血緣](../Page/族群.md "wikilink")、文化交流融合，經過千年演化最終形成相對穩定的客家民系。李輝等人在2003年發表針對福建長汀客家人所進行的遺傳分析\[4\]，顯示當地人父系結構中漢族約80.2%、畬族約13%、侗族約6.8%。

<!-- end list -->

  - 土著[族群主體說](../Page/族群.md "wikilink")：以[廣東](../Page/廣東.md "wikilink")[梅州](../Page/梅州.md "wikilink")[嘉应学院](../Page/嘉应学院.md "wikilink")[教授房學嘉的](../Page/教授.md "wikilink")《客家源流探奥》一書為主要論點。作者通過對客家地區（主要是作者所熟悉的廣東省梅州地區）歷史時期人文事象的探討，以及對客家文化與歷史上[百越文化的比較研究](../Page/百越.md "wikilink")，認為長期生活在贛閩粵邊的客家人，是南遷的中原人與閩、粵、贛三角地區的古越人遺民混化以後産生的共同體，其主體是生活在這片土地上的古越人，而不是少數流落於這一地區的中原人。

客家人從廣東的梅州、惠州、河源、福建和江西南部大量外遷到[華南各省乃至世界各地](../Page/華南.md "wikilink")。客家四州爲[梅州](../Page/梅州.md "wikilink")、[惠州](../Page/惠州.md "wikilink")、[汀州](../Page/汀州.md "wikilink")、[贛州](../Page/贛州.md "wikilink")。而[福建](../Page/福建省.md "wikilink")[宁化縣](../Page/宁化縣.md "wikilink")[石壁村是客家傳說民系形成的中心地域](../Page/石壁村.md "wikilink")，「石壁」被成稱爲“客家祖地”。[廣東的](../Page/廣東.md "wikilink")[梅州市則因其是客家人最主要聚居區之一而被宣傳爲](../Page/梅州市.md "wikilink")“[世界客都](../Page/世界客都.md "wikilink")”，廣東的河源則被稱爲千年“客家古邑”。由于海外客家华侨祖籍地最多的是原惠州府，所以惠州又被成为“客家侨都”。赣州是客家人南迁最早的集中地，因而被称为“客家摇篮”。福建省长汀县在漫长的客家人迁徙的历史上汀州作为第一个府治行政机关而存在，所以被海内外的客家人称为“客家首府”。\[5\]

## 人口與分佈

1994年，中國大陸梅州市为在梅州举行"[世界客属第十二次恳亲大会](../Page/世界客屬懇親大會#歷屆世客會.md "wikilink")"，通过多渠道，包括国内外函调、采访、采摘报刊材料等方面汇总统计，客家人在国内外分布人数共6562.429万人，其中在中國大陸6107.8万人，海外454.629万人\[6\]\[7\]。

### 中國大陸

[hakkapopulationguangdong.png](https://zh.wikipedia.org/wiki/File:hakkapopulationguangdong.png "fig:hakkapopulationguangdong.png")
[hakkapopulationjiangxi.png](https://zh.wikipedia.org/wiki/File:hakkapopulationjiangxi.png "fig:hakkapopulationjiangxi.png")
[hakkapopulationfujian.png](https://zh.wikipedia.org/wiki/File:hakkapopulationfujian.png "fig:hakkapopulationfujian.png")
[hakkapopulationguangxi.png](https://zh.wikipedia.org/wiki/File:hakkapopulationguangxi.png "fig:hakkapopulationguangxi.png")
[Taiwan_Hakka.svg](https://zh.wikipedia.org/wiki/File:Taiwan_Hakka.svg "fig:Taiwan_Hakka.svg")
[客家地区根据过去和近年各地学者的调查资料排列出来](../Page/客家地区.md "wikilink")。下列县市是中国客家人分布的县市。

  - [广东省](../Page/广东省.md "wikilink")：
      - [梅州市全境](../Page/梅州市.md "wikilink")，[梅江区](../Page/梅江区.md "wikilink")、[梅县区](../Page/梅县区.md "wikilink")、[大埔县](../Page/大埔县.md "wikilink")、[蕉岭县](../Page/蕉岭县.md "wikilink")、[平远县](../Page/平远县.md "wikilink")、[兴宁市](../Page/兴宁市.md "wikilink")、[五华县](../Page/五华县.md "wikilink")、[豐順县](../Page/豐順县.md "wikilink")
      - [河源市全境](../Page/河源市.md "wikilink")，[源城区](../Page/源城区.md "wikilink")、[东源县](../Page/东源县.md "wikilink")、[紫金县](../Page/紫金县.md "wikilink")、[龙川县](../Page/龙川县.md "wikilink")、[和平县](../Page/和平县.md "wikilink")、[连平县](../Page/连平县.md "wikilink")
      - [惠州市部分区域](../Page/惠州市.md "wikilink")，[惠阳区全境](../Page/惠阳区.md "wikilink")、[惠东县全境](../Page/惠东县.md "wikilink")、[博罗县部分区域](../Page/博罗县.md "wikilink")、[龙门县部分区域](../Page/龙门县.md "wikilink")、[惠城区部分区域](../Page/惠城区.md "wikilink")
      - [揭阳市部分区域](../Page/揭阳市.md "wikilink")，[揭西县部分区域](../Page/揭西县.md "wikilink")、[普宁市部分区域](../Page/普宁市.md "wikilink")
      - [汕尾市部分区域](../Page/汕尾市.md "wikilink")，[陆河县全境](../Page/陆河县.md "wikilink")、[陆丰市部分区域](../Page/陆丰市.md "wikilink")
      - [深圳市部分区域](../Page/深圳市.md "wikilink")，[龙岗区](../Page/龙岗区.md "wikilink")、[盐田区](../Page/盐田区.md "wikilink")、[石岩街道](../Page/石岩街道.md "wikilink")、[龍華區](../Page/龍華區_\(深圳市\).md "wikilink")[观澜街道](../Page/观澜街道.md "wikilink")、[龍華街道](../Page/龙华街道_\(深圳市\).md "wikilink")、[南山区](../Page/南山区.md "wikilink")[西丽街道](../Page/西丽街道.md "wikilink")、[宝安区](../Page/宝安区.md "wikilink")[西乡街道](../Page/西乡街道.md "wikilink")、[福永街道等部分区域](../Page/福永街道.md "wikilink")
      - [东莞市部分区域](../Page/东莞市.md "wikilink")，[樟木头镇](../Page/樟木头镇.md "wikilink")、[清溪鎮](../Page/清溪镇_\(东莞市\).md "wikilink")、[凤崗鎮部分区域](../Page/凤岗镇_\(东莞市\).md "wikilink")
      - [韶关市部分区域](../Page/韶关市.md "wikilink")，[新丰县](../Page/新丰县.md "wikilink")、[翁源县](../Page/翁源县.md "wikilink")、[始兴县和](../Page/始兴县.md "wikilink")[南雄市全境](../Page/南雄市.md "wikilink")、[仁化县](../Page/仁化县.md "wikilink")、縣级市[乐昌市](../Page/乐昌市.md "wikilink")、[曲江区等部分区域](../Page/曲江区.md "wikilink")
      - [清远市北部](../Page/清远市.md "wikilink")、东部、縣级市[英德市等部分区域](../Page/英德市.md "wikilink")
      - [广州市部分区域](../Page/广州市.md "wikilink")，[花都区](../Page/花都区.md "wikilink")、[从化区](../Page/从化区.md "wikilink")、[增城区等部分区域](../Page/增城区.md "wikilink")
      - [中山市](../Page/中山市.md "wikilink")[五桂山地区](../Page/五桂山.md "wikilink")；东区、[板芙镇](../Page/板芙镇.md "wikilink")、[南朗镇等部分村落](../Page/南朗镇.md "wikilink")
      - [江門市縣级市](../Page/江門市.md "wikilink")[台山市](../Page/台山市.md "wikilink")[赤溪鎮全境](../Page/赤溪镇_\(台山市\).md "wikilink")
      - [潮州市](../Page/潮州市.md "wikilink")[饶平县部分区域](../Page/饶平县.md "wikilink")
      - 另外，[云浮市](../Page/云浮市.md "wikilink")、[肇庆市](../Page/肇庆市.md "wikilink")、[茂名市](../Page/茂名市.md "wikilink")、[阳江市](../Page/阳江市.md "wikilink")、[湛江市](../Page/湛江市.md "wikilink")、[汕头市等各地均有客家村镇分布](../Page/汕头市.md "wikilink")。
  - [福建省](../Page/福建省.md "wikilink")：
      - [龍巖市部分区域](../Page/龍巖市.md "wikilink")，[长汀縣](../Page/长汀縣.md "wikilink")、[连城縣](../Page/连城縣.md "wikilink")、[上杭縣](../Page/上杭縣.md "wikilink")、[武平縣](../Page/武平縣.md "wikilink")、[永定区](../Page/永定区_\(龍巖市\).md "wikilink")
      - [三明市部分区域](../Page/三明市.md "wikilink")，[宁化縣](../Page/宁化縣.md "wikilink")、[清流縣](../Page/清流縣.md "wikilink")、[明溪縣](../Page/明溪縣.md "wikilink")、[建寧縣](../Page/建寧縣.md "wikilink")、[将乐縣](../Page/将乐縣.md "wikilink")、[泰寧縣等](../Page/泰寧縣.md "wikilink")11个。
      - 非纯客家县市：[漳州市](../Page/漳州市.md "wikilink")[詔安縣](../Page/詔安縣.md "wikilink")、[雲霄縣](../Page/雲霄縣.md "wikilink")、[南靖縣](../Page/南靖縣.md "wikilink")、[平和縣等](../Page/平和縣.md "wikilink")。
  - [江西省](../Page/江西省.md "wikilink")
      - [赣州市部分区域](../Page/赣州市.md "wikilink")，[南康区](../Page/南康区.md "wikilink")、[宁都縣](../Page/宁都縣.md "wikilink")、[石城縣](../Page/石城縣.md "wikilink")、[雩都縣](../Page/于都縣.md "wikilink")、[上犹縣](../Page/上犹縣.md "wikilink")、[大余縣](../Page/大余縣.md "wikilink")、[崇义縣](../Page/崇义縣.md "wikilink")、[安远縣](../Page/安远縣.md "wikilink")、[信丰縣](../Page/信丰縣.md "wikilink")、[龙南縣](../Page/龙南縣.md "wikilink")、[全南縣](../Page/全南縣.md "wikilink")、[定南縣](../Page/定南縣.md "wikilink")、[兴国縣](../Page/兴国縣.md "wikilink")、縣级市[瑞金市](../Page/瑞金市.md "wikilink")、[会昌縣](../Page/会昌縣.md "wikilink")、[寻乌縣](../Page/寻乌縣.md "wikilink")
      - [宜春市的](../Page/宜春市.md "wikilink")[铜鼓县等等](../Page/铜鼓县.md "wikilink")
  - [湖南省](../Page/湖南省.md "wikilink")：
      - 湖南東南部的[郴州市](../Page/郴州市.md "wikilink")[汝城县](../Page/汝城县.md "wikilink")、[桂东县](../Page/桂东县.md "wikilink")、[株洲市](../Page/株洲市.md "wikilink")[炎陵县三县](../Page/炎陵县.md "wikilink")。
      - 非纯客家县市：[岳陽市](../Page/岳陽市.md "wikilink")[平江县](../Page/平江县.md "wikilink")、[長沙市縣级市](../Page/長沙市.md "wikilink")[浏阳市](../Page/浏阳市.md "wikilink")、[株洲市](../Page/株洲市.md "wikilink")[攸县](../Page/攸县.md "wikilink")、[茶陵县等](../Page/茶陵县.md "wikilink")
  - [广西壯族自治区](../Page/广西壯族自治区.md "wikilink")：
      - 非纯客家县市：[玉林市](../Page/玉林市.md "wikilink")[博白县](../Page/博白县.md "wikilink")、[陆川县](../Page/陆川县.md "wikilink")、縣级市[北流市](../Page/北流市.md "wikilink")、[防城港市](../Page/防城港市.md "wikilink")、[北海市縣级市](../Page/北海市.md "wikilink")[合浦县](../Page/合浦县.md "wikilink")、[欽州市](../Page/欽州市.md "wikilink")[浦北县](../Page/浦北县.md "wikilink")、[贺州市等](../Page/贺州市.md "wikilink")。
  - [海南省](../Page/海南省.md "wikilink")：
      - 非纯客家县市：[儋州市](../Page/儋州市.md "wikilink")、[临高县](../Page/临高县.md "wikilink")、[澄迈县](../Page/澄迈县.md "wikilink")、[瓊中黎族苗族自治縣](../Page/瓊中黎族苗族自治縣.md "wikilink")、縣级市[万宁市](../Page/万宁市.md "wikilink")、[陵水黎族自治縣](../Page/陵水黎族自治縣.md "wikilink")、[屯昌县](../Page/屯昌县.md "wikilink")、[海口市等](../Page/海口市.md "wikilink")
  - [四川省](../Page/四川省.md "wikilink")：
      - 非纯客家县市：[資陽市](../Page/資陽市.md "wikilink")[安岳县](../Page/安岳县.md "wikilink")、[眉山市](../Page/眉山市.md "wikilink")[仁寿縣](../Page/仁寿縣.md "wikilink")、[成都市地级市](../Page/成都市.md "wikilink")[简阳市](../Page/简阳市.md "wikilink")、[成都市](../Page/成都市.md "wikilink")、[自貢市](../Page/自貢市.md "wikilink")[富顺縣](../Page/富顺縣.md "wikilink")、[泸州市](../Page/泸州市.md "wikilink")[泸县](../Page/泸县.md "wikilink")、[宜宾市](../Page/宜宾市.md "wikilink")、[涼山彝族自治州](../Page/涼山彝族自治州.md "wikilink")[西昌市](../Page/西昌市.md "wikilink")、[內江市县级市](../Page/內江市.md "wikilink")[隆昌市等](../Page/隆昌市.md "wikilink")。
  - [浙江省](../Page/浙江省.md "wikilink") :
      - 非純客家县市：[紹興市](../Page/紹興市.md "wikilink")、[金華市](../Page/金華市.md "wikilink")、[衢州市](../Page/衢州市.md "wikilink")、[丽水市](../Page/丽水市.md "wikilink")[青田縣等](../Page/青田縣.md "wikilink")。
  - [重庆市](../Page/重庆市.md "wikilink")：
      - 非纯客家县市：[荣昌区](../Page/荣昌区.md "wikilink")
  - [陜西省](../Page/陜西省.md "wikilink")：
      - 非纯客家县市：[商洛市](../Page/商洛市.md "wikilink")
  - [河南省](../Page/河南省.md "wikilink")：
      - 非纯客家县市：[信陽市](../Page/信陽市.md "wikilink")

### [香港](../Page/香港.md "wikilink")

[香港歷史上原屬](../Page/香港.md "wikilink")[廣東省的](../Page/廣東省.md "wikilink")[寶安縣](../Page/寶安縣.md "wikilink")，是客家人的傳統聚居地之一。17世紀末，客家人就開始向香港遷移，[香港仍未開埠前客家人是](../Page/香港開埠初期歷史.md "wikilink")[香港原居民最早之一](../Page/香港原居民.md "wikilink")，該地區因而被人叫做「客家村」。在[英國殖民](../Page/英國.md "wikilink")[香港以前](../Page/香港.md "wikilink")，香港336個村落中（例如[赤柱村](../Page/八間屋.md "wikilink")），客家人的村落多達128個\[8\]。2010年時，香港的客家人約有100萬人\[9\]，當中包括「原居」\[10\]和「非原居」的客家人。在新界[大埔](../Page/大埔.md "wikilink")、[北區](../Page/北區_\(香港\).md "wikilink")、[沙田](../Page/沙田.md "wikilink")、[西貢](../Page/西貢市.md "wikilink")、[荃灣](../Page/荃灣.md "wikilink")、[屯門](../Page/屯門.md "wikilink")、[九龍及](../Page/九龍.md "wikilink")[香港島的村落皆可見客家人的足跡](../Page/香港島.md "wikilink")。客家人在港數量並不少，但由於遭[標準粵語](../Page/標準粵語.md "wikilink")（即[廣府話或稱廣州話](../Page/廣府話.md "wikilink")）取代的影響，現時[客家語在香港已不佔優](../Page/客家語.md "wikilink")，不少客家新一代都不會講客家話，或者只懂聽不懂講。（詳見[香港語言條](../Page/香港語言.md "wikilink")）

### [澳門](../Page/澳門.md "wikilink")

[澳門的客家人約有](../Page/澳門.md "wikilink")10萬人，可分為三個部分來源：一是澳門原居民，居住在[路環](../Page/路環.md "wikilink")[九澳村和](../Page/九澳村.md "wikilink")[黑沙村](../Page/黑沙村.md "wikilink")，主要是在[清朝咸豐年間自廣東](../Page/清朝.md "wikilink")[鶴山](../Page/鶴山.md "wikilink")、[開平](../Page/開平.md "wikilink")、[恩平](../Page/恩平.md "wikilink")、[高要一帶遷徙到澳門](../Page/高要.md "wikilink")；二是上世紀六、七十年代，從東南亞（柬埔寨、緬甸、越南、印度尼西亞等）的歸國華人；三是改革開放以後，由內地遷往澳門的居民。\[11\]

### [臺灣](../Page/臺灣.md "wikilink")

[臺灣客家人口比例較高地區為](../Page/臺灣.md "wikilink")[桃竹苗](../Page/桃竹苗.md "wikilink")（俗稱北客）、[高屏](../Page/高屏.md "wikilink")（俗稱南客或[六堆](../Page/六堆.md "wikilink")）、[花東縱谷](../Page/花東縱谷.md "wikilink")\[12\]\[13\]。

### [東南亞](../Page/東南亞.md "wikilink")、[南亞和](../Page/南亞.md "wikilink")[西方世界](../Page/歐美.md "wikilink")

主要分布于[东南亚一些国家如](../Page/东南亚.md "wikilink")[越南](../Page/越南.md "wikilink")、[泰国](../Page/泰国.md "wikilink")、[马来西亚](../Page/马来西亚.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[新加坡等](../Page/新加坡.md "wikilink")，还有部分分布在[印度](../Page/印度.md "wikilink")、[孟加拉国](../Page/孟加拉国.md "wikilink")，[毛里求斯](../Page/毛里求斯.md "wikilink")。[孟加拉国](../Page/孟加拉国.md "wikilink")[客家裔](../Page/客家.md "wikilink")[华人](../Page/华人.md "wikilink")
分布最多的[民族](../Page/民族.md "wikilink")[地区是在](../Page/地区.md "wikilink")[达卡](../Page/达卡.md "wikilink")（參見[华裔孟加拉人](../Page/华裔孟加拉人.md "wikilink")），此外在[美洲也有少量分布](../Page/美洲.md "wikilink")，如[加拿大第三大城](../Page/加拿大.md "wikilink")[溫哥華有大量](../Page/溫哥華.md "wikilink")[香港移民](../Page/香港.md "wikilink")，該處[客家人比例亦隨之增長](../Page/客家人.md "wikilink")。[南美洲的](../Page/南美洲.md "wikilink")[圭亞那亦將](../Page/圭亞那.md "wikilink")[客語定為官方語言之一](../Page/客語.md "wikilink")。\[14\]\[15\]

### [太平洋和](../Page/太平洋.md "wikilink")[印度洋](../Page/印度洋.md "wikilink")

如[留尼旺島有部分客家人](../Page/留尼旺島.md "wikilink")。\[16\]\[17\]

## 母語

客家人使用的母語「[客話](../Page/客話.md "wikilink")」（）俗稱[客語](../Page/客語.md "wikilink")、[客家話或](../Page/客家話.md "wikilink")[土广东話](../Page/土广东話.md "wikilink")，在广东西部和广西地区也叫作「[涯話](../Page/涯話.md "wikilink")」、「嘛介話」（）或「[艾話](../Page/艾話.md "wikilink")」。\[18\]

### 臺灣

中華民國過去推動的[國語運動打壓包含客家話的台灣本土語言](../Page/國語運動.md "wikilink")，年輕一輩的臺灣客家裔民眾能聽說客家話的人數大幅降低。

在[臺灣省和旁邊的六個直轄市](../Page/臺灣省.md "wikilink")，[教育部將之命名為](../Page/中華民國教育部.md "wikilink")「[臺灣客家語](../Page/臺灣客家語.md "wikilink")」\[19\]。客家話在客家族群認同上扮演着很重要的作用，客家人素有「寧賣祖宗田，莫忘祖宗言」的祖訓\[20\]，不會講客家話又自稱「客家人」曾被譏諷是「假客家人」\[21\]。\[22\]

1970年代，臺灣省府訂定「台灣省加強推行國語實施計劃」，規定交通機構包括火車及公民營汽車一律使用國語、人民團體開會均應使用國語；各級官員列席民意代表會議必須使用國語、
嚴加勸導商店及流動小販，不得使用外文商標、招牌；禁止電影院對外播放方言、外語，嚴加勸導街頭宣傳勿用方言；各級運動會嚴禁使用方言報告。\[23\]

近年則開始正視語言流失，2000年通過的《大眾運輸工具播音語言平等保障法》規定公車、捷運等交通工具，需播報閩南語、客家語。

### 香港

[香港原屬](../Page/香港.md "wikilink")[廣東省](../Page/廣東省.md "wikilink")[寶安縣](../Page/寶安縣.md "wikilink")，是客家人的傳統聚居地之一。[香港仍未開埠至](../Page/香港開埠初期歷史.md "wikilink")[1898年英國租借新九龍及「新界」前](../Page/展拓香港界址專條.md "wikilink")，客家人已在今天的香港立足多年，屬香港原居民之一。在新界631個原住民村落裏（例如[赤柱村](../Page/八間屋.md "wikilink")、[荃灣](../Page/荃灣.md "wikilink")[三棟屋](../Page/三棟屋村.md "wikilink")），以客家人為主的村落有341個，佔54%\[24\]\[25\]。根據2012年的《中國語言地圖集》，香港客家語被歸類為[粵台片](../Page/粵台片.md "wikilink")[梅惠小片](../Page/粵台片.md "wikilink")\[26\]。

2010年，香港客家原居民约为20到30万左右。假若包括客家原居民和非原居\[27\]的客家人，则香港的客家人約有100萬人\[28\]。雖然客家人（包括[原居民及非原居民的客家人](../Page/新界原居民.md "wikilink")）在港數量不少，但現時多數是中、老年香港客家人仍使用[客家語](../Page/香港客家語.md "wikilink")\[29\]，不少香港客家新一代都不會講客家話，或者只懂聽不懂講。這是由於1970年代初，來自[中國內地移民帶來了不同的](../Page/中國內地.md "wikilink")[漢語變體](../Page/漢語變體.md "wikilink")（比如[廣州話](../Page/廣州話.md "wikilink")、[四邑話](../Page/四邑話.md "wikilink")、[客家話](../Page/客家話.md "wikilink")、[潮州話](../Page/潮州話.md "wikilink")、[閩南話](../Page/閩南話.md "wikilink")、[福州話](../Page/福州話.md "wikilink")、[上海話](../Page/上海話.md "wikilink")、[山東話等](../Page/山東話.md "wikilink")），[港英政府遂選擇了](../Page/港英政府.md "wikilink")[粵語作爲香港](../Page/粵語.md "wikilink")[華人的統一](../Page/華人.md "wikilink")[中文](../Page/中文.md "wikilink")[口語](../Page/口語.md "wikilink")。在香港，現時以客家話為母語的僅有62,340人，佔香港人口的0.9%\[30\]\[31\]；報稱通曉客家話的香港人，亦只有259,738人，佔香港人口的4.7%\[32\]\[33\]。以上數據都顯示以客家語為母語的香港人口比例正逐年下降，情況不容樂觀\[34\]。

## 文化

客家文化是南方文化的重要組成部分，特別是廣東文化的重要支流。客家人非常團結合作，在中國大陸多居于閩粵贛地區，故有濃厚的丘陵文化。[中華民國](../Page/中華民國.md "wikilink")[客家委員會在廣徵客家人士意見後](../Page/客家委員會.md "wikilink")，宣布將把具客家獨特性的「[天穿日](../Page/天穿日.md "wikilink")」（[農曆](../Page/農曆.md "wikilink")[正月二十](../Page/正月二十.md "wikilink")），訂為[中華民國](../Page/中華民國.md "wikilink")「[全國客家日](../Page/全國客家日.md "wikilink")」\[35\]。

### 圍屋古鎮

客家圍屋是漢代[塢堡的活化石](../Page/塢堡.md "wikilink")，有方形、圓形、八角形和橢圓形等形狀的土樓共有8000餘座，規模之大，造型之美，既科學實用，又有特色。而客家土樓的[夯土版築技術](../Page/夯土版築技術.md "wikilink")，更是[中華縱貫古今建城造牆積累下來的結晶](../Page/中華.md "wikilink")，塢堡，最初是一種民間防衛性建築，大約形成[王莽天鳳年間](../Page/王莽.md "wikilink")，當時北方大饑，社會動盪不安，士家大族為求自保，紛紛構築塢堡營壁。從[漢墓出土的陶屋模型可見在秦磚漢瓦](../Page/漢墓.md "wikilink")，宮闕萬間都作了土的今天，仍能得以一窺塢堡的真容。[東漢學者](../Page/東漢.md "wikilink")[許慎的](../Page/許慎.md "wikilink")《[說文解字](../Page/說文解字.md "wikilink")》「隖，小障也，一曰庳城也」[北宋政治家](../Page/北宋.md "wikilink")，史學家[司馬光的](../Page/司馬光.md "wikilink")《[資治通鑑](../Page/資治通鑑.md "wikilink")》「永嘉四年七月條，胡三省註釋塢壁道：城之小者曰塢，天下兵爭，聚眾築塢以自守，未有朝命，故自為塢主」近代史學家[陳寅恪在](../Page/陳寅恪.md "wikilink")《[桃花源記旁證](../Page/桃花源記旁證.md "wikilink")》一文中認為「[西晉末年戎狄盜賊並起](../Page/西晉.md "wikilink")，當時中原避難之人民......其不能遠離本土遷至他鄉者，則大抵糾合宗族鄉黨，屯聚堡塢，據險自守，以避戎狄寇盜之難」\[36\]\[37\]

  - 廣東省：[圍龍屋](../Page/圍龍屋.md "wikilink") -
    [花萼樓](../Page/花萼樓.md "wikilink") -
    [榮槐樓](../Page/榮槐樓.md "wikilink") -
    [棣華圍](../Page/棣華圍.md "wikilink") -
    [磐安圍](../Page/磐安圍.md "wikilink") -
    [滿堂客家大圍](../Page/滿堂客家大圍.md "wikilink") -
    [四角樓](../Page/四角樓.md "wikilink") -
    [承德樓](../Page/承德樓.md "wikilink") -
    [人境廬](../Page/人境廬.md "wikilink") -
    [南華又廬](../Page/南華又廬.md "wikilink") -
    [原膴隆基](../Page/原膴隆基.md "wikilink") -
    [東昇圍](../Page/東昇圍.md "wikilink") -
    [馨梓圍](../Page/馨梓圍.md "wikilink") -
    [崇林世居](../Page/崇林世居.md "wikilink") -
    [善述圍](../Page/善述圍.md "wikilink") -
    [濟濟樓](../Page/濟濟樓.md "wikilink") -
    [泰安樓](../Page/泰安樓.md "wikilink") -
    [德先樓等](../Page/德先樓.md "wikilink")。圍龍屋以[廣東](../Page/廣東.md "wikilink")[梅州為代表](../Page/梅州.md "wikilink")；於[臺灣](../Page/臺灣.md "wikilink")[南部較多](../Page/南臺灣.md "wikilink")，而[中部的](../Page/中臺灣.md "wikilink")[石岡](../Page/石岡區.md "wikilink")、[東勢地區客家人則稱其作](../Page/東勢區.md "wikilink")「圍屋」\[38\]

[_Guanxi_Xinwei,_Longnan,_Jiangxi.JPG](https://zh.wikipedia.org/wiki/File:_Guanxi_Xinwei,_Longnan,_Jiangxi.JPG "fig:_Guanxi_Xinwei,_Longnan,_Jiangxi.JPG")

  - 江西省：[東生圍](../Page/東生圍.md "wikilink") -
    [關西新圍](../Page/關西新圍.md "wikilink") -
    [燕翼圍](../Page/燕翼圍.md "wikilink") -
    [栗園圍](../Page/栗園圍.md "wikilink") -
    [漁子潭圍](../Page/漁子潭圍.md "wikilink") -
    [龍光圍](../Page/龍光圍.md "wikilink") -
    [雅溪圍](../Page/雅溪圍.md "wikilink") -
    [西昌圍](../Page/西昌圍.md "wikilink") -
    [烏石圍等](../Page/烏石圍.md "wikilink")。江西龍南縣有全世界最多的客家圍屋。\[39\]

[_Zhenchenglou.JPG](https://zh.wikipedia.org/wiki/File:_Zhenchenglou.JPG "fig:_Zhenchenglou.JPG")

  - 福建省：[福建土樓](../Page/福建土樓.md "wikilink")：即[福建](../Page/福建.md "wikilink")[龍岩和](../Page/龍岩.md "wikilink")[廣東](../Page/廣東.md "wikilink")[大埔一帶的客家人所居住的大型](../Page/大埔縣.md "wikilink")[民居群](../Page/民居.md "wikilink")，以[永定](../Page/永定縣.md "wikilink")[土樓為代表](../Page/土樓.md "wikilink")，已被列入[世界文化遺產](../Page/世界文化遺產.md "wikilink")\[40\]。[振成樓](../Page/振成樓.md "wikilink")
    - [齊雲樓](../Page/齊雲樓.md "wikilink") -
    [二宜樓](../Page/二宜樓.md "wikilink") -
    [善慶樓](../Page/善慶樓.md "wikilink") -
    [步雲樓](../Page/步雲樓.md "wikilink") -
    [和昌樓](../Page/和昌樓.md "wikilink") -
    [振昌樓](../Page/振昌樓.md "wikilink")-
    [瑞雲樓等](../Page/瑞雲樓.md "wikilink")。\[41\]

<!-- end list -->

  - 深圳客家圍：[鶴湖新居](../Page/鶴湖新居.md "wikilink") -
    [大田世居](../Page/大田世居.md "wikilink") -
    [大萬世居](../Page/大萬世居.md "wikilink") -
    [茂盛世居](../Page/茂盛世居.md "wikilink") -
    [豐田世居](../Page/豐田世居.md "wikilink") -
    [龍田世居](../Page/龍田世居.md "wikilink") -
    [新喬世居](../Page/新喬世居.md "wikilink") -
    [西埔新居](../Page/西埔新居.md "wikilink") -
    [正埔嶺世居](../Page/正埔嶺世居.md "wikilink") -
    [璇慶新居](../Page/璇慶新居.md "wikilink") -
    [吉坑世居](../Page/吉坑世居.md "wikilink") -
    [梅岡世居](../Page/梅岡世居.md "wikilink") -
    [環水樓](../Page/環水樓.md "wikilink") -
    [洪圍](../Page/洪圍.md "wikilink") -
    [貴湖塘老圍](../Page/貴湖塘老圍.md "wikilink") -
    [廻龍世居等](../Page/廻龍世居.md "wikilink")。\[42\]

<!-- end list -->

  - 香港客家圍：[沙田圍](../Page/沙田圍.md "wikilink") -
    [曾大屋](../Page/曾大屋.md "wikilink") -
    [三棟屋](../Page/三棟屋.md "wikilink") -
    [企嶺下新圍](../Page/企嶺下新圍.md "wikilink") -
    [蕉徑老圍](../Page/蕉徑老圍.md "wikilink") -
    [谷埔老圍等](../Page/谷埔老圍.md "wikilink")。\[43\]

<!-- end list -->

  - [家廟](../Page/家廟.md "wikilink")（[公廳](../Page/公廳.md "wikilink")）：祭祀共同祖先的建築物。

<!-- end list -->

  - [伙房](../Page/伙房.md "wikilink")（夥房）：夥房為「ㄇ」字型設計，正中央則是「廳下」﹙祭祀祖先的廳堂﹚，以屋頂高低作為輩份之區分，廚房共用，共用一口灶，一起伙食。

<!-- end list -->

  - [走馬樓](../Page/走馬樓.md "wikilink")：有走廊可通行的樓屋，甚至騎馬可以在裡面暢行無阻，外形一字形、凹形或曲尺形，分上下兩層。

<!-- end list -->

  - [五鳳樓](../Page/五鳳樓.md "wikilink")：為土樓的一種形式，其樓依山形而建，呈層層高姿態，一般有五個層次，形如雁翅，故稱“五鳳樓”。

<!-- end list -->

  - [四點金](../Page/四點金.md "wikilink")：主要特點為房屋四角升建有如近代砲台樓式的高層建築，樓四角均有槍眼。

<!-- end list -->

  - [混合式](../Page/混合式.md "wikilink")：這是客家僑鄉民居建築形式的一種特殊風格，是將客家傳統的房屋結構結合西洋的建築藝術裝飾建造的混合型民居建築。較突出的有[梅縣白宮鎮的](../Page/梅縣.md "wikilink")“[聯芳樓](../Page/聯芳樓.md "wikilink")”、程江鎮的“[萬秋樓](../Page/萬秋樓.md "wikilink")”等。

<!-- end list -->

  - [敬字亭](../Page/敬字亭.md "wikilink")：用於燒毀書有文字，受[科舉制度影響](../Page/科舉.md "wikilink")，古人認為文字是神聖和崇高的，寫在紙上的文字，不能隨意褻瀆，即使是廢字紙，也必須誠心敬意地燒掉。

<!-- end list -->

  - 客家古鎮：[成都洛帶古鎮](../Page/成都洛帶古鎮.md "wikilink")\[44\] -
    [梅州百侯古鎮](../Page/梅州百侯古鎮.md "wikilink")\[45\] -
    [邵武和平古鎮](../Page/邵武和平古鎮.md "wikilink")\[46\] -
    [永定客家古鎮](../Page/永定客家古鎮.md "wikilink")\[47\] -
    [梅州松口古鎮](../Page/梅州松口古鎮.md "wikilink")\[48\] -
    [長汀古鎮](../Page/長汀古鎮.md "wikilink")\[49\] -
    [深圳甘坑客家小鎮](../Page/深圳甘坑客家小鎮.md "wikilink")\[50\]-
    [深圳觀瀾文化小鎮](../Page/深圳觀瀾文化小鎮.md "wikilink")\[51\]等。

<!-- end list -->

  - 客家古村：[贛縣白鷺古村](../Page/贛縣白鷺古村.md "wikilink")\[52\]-[河源南園古村](../Page/河源南園古村.md "wikilink")\[53\]-[河源蘇家圍村](../Page/河源蘇家圍村.md "wikilink")\[54\]-[河源林寨古村](../Page/河源林寨古村.md "wikilink")\[55\]-[韶關石塘古村](../Page/韶關石塘古村.md "wikilink")\[56\]-[連城縣培田古村](../Page/連城縣培田古村.md "wikilink")<ref>民間故宮-培田客家古村

<!-- end list -->

  -
    <https://travel.qunar.com/p-pl3579952></ref>-[唐江盧屋古村](../Page/唐江盧屋古村.md "wikilink")\[57\]-[梅州僑溪古村](../Page/梅州僑溪古村.md "wikilink")\[58\]等。

### 音樂

#### 傳統音樂

  - [崖山哀](http://www.youtube.com/watch?v=-3sc8QqQUW0)
  - [挑簾](https://www.youtube.com/watch?v=DlHN2wx_zF8)
  - [出水蓮](https://www.youtube.com/watch?v=k3iFyIZLP-Q)
  - [錦上添花](https://www.youtube.com/watch?v=jr8NbH1OitQ)
  - [小桃紅](https://www.youtube.com/watch?v=IFTQ45CQcfo)
  - [琵琶詞](https://www.youtube.com/watch?v=Fx0eLkaciGc)
  - [情纏呂布](https://www.youtube.com/watch?v=ja_6Rbg_J14)
  - [昭君怨](https://www.youtube.com/watch?v=_ewRQlgmDSY)
  - [雪雁南飛](https://www.youtube.com/watch?v=N1LMoetIlmM)

#### 流行音樂

近年來，客家音樂有了新面貌，加入了年輕一輩的創意，更活潑生動；混合了民謠、爵士、流行、搖滾等風格。

代表作品：

  - [遠方的鼓聲](http://www.youtube.com/watch?v=-hHg8GBSvOc&feature=channel_page)
  - [地獄浮沉錄](http://www.youtube.com/watch?v=acPWLejrZDs)
  - [青鳥詞](http://www.youtube.com/watch?v=PbJhgD5WGjI)
  - [末代客的最後一場戲](http://www.youtube.com/watch?v=aj04XGnpGRc&feature=relmfu)
  - [客家世界](http://www.youtube.com/watch?v=S2hWK9fkb0Y)
  - [客家本色](http://www.youtube.com/watch?v=ZBlOGxVDpxg)
  - [風神125](http://www.youtube.com/watch?v=xiSDTtZMclM)
  - [月光光](http://www.youtube.com/watch?v=8-SgvL8SOgg)
  - [天光](https://www.youtube.com/watch?v=jCss5ZDXLM8&list=PLZ1vWvhmSaIccac49OKRpEWl6p7sFFJhv&index=87&t=0s)
  - [芙蓉花開](https://www.youtube.com/watch?v=-dpF8a20T2w&list=PLZ1vWvhmSaIccac49OKRpEWl6p7sFFJhv&index=176)
  - [光源](https://www.youtube.com/watch?v=3cAdMircr_Q&index=177&list=PLZ1vWvhmSaIccac49OKRpEWl6p7sFFJhv)

### 飲食

  - 因為客家族群多遷徙且居住[華中](../Page/華中.md "wikilink")、[華南](../Page/華南.md "wikilink")[丘陵的丘陵山地地區](../Page/丘陵.md "wikilink")，勞動出汗多，需補充[鹽分以維持體力](../Page/鹽.md "wikilink")，因此飲食傾向多油多鹹的重口味菜（香油鹹為特色），為下飯以應付大量勞力，並且好用各式處理過的醃製菜類（如[酸菜](../Page/酸菜.md "wikilink")、[梅乾菜](../Page/梅乾菜.md "wikilink")、[芥菜](../Page/芥菜.md "wikilink")、[福菜](../Page/福菜.md "wikilink")）作為食材入菜。著名的客家菜有[梅菜扣肉](../Page/梅菜扣肉.md "wikilink")、[酿豆腐](../Page/酿豆腐.md "wikilink")。传统的客家招牌菜是[盐焗鸡](../Page/盐焗鸡.md "wikilink")、[客家酿豆腐](../Page/客家酿豆腐.md "wikilink")、[猪肚鸡](../Page/猪肚鸡.md "wikilink")、[酿苦瓜](../Page/酿苦瓜.md "wikilink")、[梅菜扣肉](../Page/梅菜扣肉.md "wikilink")、[三杯鸭](../Page/三杯鸭.md "wikilink")、[白斩河田鸡](../Page/白斩河田鸡.md "wikilink")、[兜汤](../Page/兜汤.md "wikilink")、[汀州泡猪腰](../Page/汀州泡猪腰.md "wikilink")、[仙人冻](../Page/仙人冻.md "wikilink")、[麒麟脱胎](../Page/麒麟脱胎.md "wikilink")、客家[盆菜](../Page/盆菜.md "wikilink")、[四星望月](../Page/四星望月.md "wikilink")、[芋子包](../Page/芋子包.md "wikilink")、[芋子饺等与潮州菜比较](../Page/芋子饺.md "wikilink")，客家菜的口感偏重“肥、咸、熟”，在粤菜或闽菜系中独树一帜。<ref>客家飲食文化根在中原

<!-- end list -->

  -
    <http://www.youngchina.cn/live/20161014/140866.html></ref>\[59\]

<!-- end list -->

  - 客家菜源於[中原](../Page/中原.md "wikilink")，有些菜餚帶有中原烹調技法的影子，富有北方風味的菜餚特徵，有些菜餚雖然在原料選擇與運用上完全不同，但在制法上大致相同，作為[豫菜的發祥地](../Page/豫菜.md "wikilink")，[北宋時期](../Page/北宋.md "wikilink")[開封的飲食文化](../Page/開封.md "wikilink")，被視為[中國飲食文化史上輝煌的里程碑](../Page/中國.md "wikilink")，對客家飲食文化產生了深遠影響。<ref>客家飲食文化-古代中原飲食文化的活化石

<!-- end list -->

  -
    <http://sh.qihoo.com/pc/9bf518bd3dbc14594?cota=1&sign=360_e39369d1></ref>

<!-- end list -->

  - 像客家菜餚紫蓋肉與代表[黃河中下游菜餚風味特色的](../Page/黃河.md "wikilink")[魯菜中的炸脂蓋](../Page/魯菜.md "wikilink")，在烹調技法上完全相同，但客家菜選用的是[五花肉](../Page/五花肉.md "wikilink")，魯菜選用的是羊花肉。客家菜的酥燒肉與魯菜的琉璃肉，客家菜的紅燒肉和魯菜的壇子肉，客家菜的燒鯉與魯菜的糖醋鯉魚，客家菜的糯米酥雞，八寶全雞（鴨）和魯菜的布袋雞（鴨）等在烹調技法的運用上都十分相似，只不過在原料運用，方法處理上稍有差別。\[60\]

<!-- end list -->

  - 客家菜和中原菜一脈相承，完整保留中原飲食特色的又屬廣東地區的東江客家菜，東江多山川地貌，氣候和物產條件與中原相似，因此在食俗中得以最大程度的保留。東江菜用料以[家畜](../Page/家畜.md "wikilink")、[家禽的肉類為主](../Page/家禽.md "wikilink")，所謂「無雞不清，無鴨不香，無肉不鮮，無肘不濃」所重皆為陸生肉類。菜餚風格上也和中原一樣，講求主料突出，造型古樸，以鹽定味，以湯提鮮，力求酥爛香濃。烹調方法多樣，尤以北方常見的煮、燉、熬、釀、燜等技法見長，鄉土氣息濃郁，頗有中原遺風。\[61\]

<!-- end list -->

  - [河南開封飲食文化博物館館長](../Page/河南.md "wikilink")[孫潤田指出客家飲食保留了大量的中州古味](../Page/孫潤田.md "wikilink")，是古代飲食文化的「活化石」中原情結是客家飲食的根基源泉，除了日常飲食習慣，客家人的習俗也與中原地區一脈相承。\[62\]\[63\]

<!-- end list -->

  - 如客家人待客方面講究「六碗八盆十樣」菜餚實惠量足，盛器多用盆、缽、大碗，有古民遺風，客家人尊老知禮，設宴依輩分排座次，席間禮規繁多，上座留空位於已故[先祖](../Page/先祖.md "wikilink")，以示敬禮，席間小輩給長輩敬酒，敬菜等，客家飲食文化依然保持著中原文化的傳統習俗和用餐禮儀，也算是對中原文化的傳承與發展。\[64\]

### 表演藝術

  - [採茶戲](../Page/採茶戲.md "wikilink")（三腳採茶戲）
  - [平安戲](../Page/平安戲.md "wikilink")（收冬戲）
  - [木偶戏](../Page/木偶戏.md "wikilink")
  - [广东汉剧](../Page/广东汉剧.md "wikilink")
  - [山歌剧](../Page/山歌剧.md "wikilink")
  - [客家](../Page/客家.md "wikilink")[布袋戲](../Page/布袋戲.md "wikilink")（以客語發音的布袋戲，其中以臺灣[雲林縣為最著名](../Page/雲林縣.md "wikilink")）
  - [詔安客家開口獅](../Page/詔安客家開口獅.md "wikilink")
  - [九腔十八調](../Page/九腔十八調.md "wikilink")
  - [客家嗩吶](../Page/客家嗩吶.md "wikilink")
  - [榮興客家採茶劇團](../Page/榮興客家採茶劇團.md "wikilink")

### 客家電視台

[台灣](../Page/台灣.md "wikilink")[客家電視台是目前全球首家以](../Page/客家電視台.md "wikilink")[客語為主要播音語言的](../Page/客語.md "wikilink")[電視台](../Page/電視台.md "wikilink")，該電視台提供網路節目觀賞服務([Hakka
Television](http://www.hakkatv.org.tw))。

### 梅州電視台

2006年，[廣東](../Page/廣東.md "wikilink")[梅州電視台把旗下的](../Page/梅州電視台.md "wikilink")「公共[頻道](../Page/電視頻道.md "wikilink")」（即「梅视二套」或「[梅州](../Page/梅州.md "wikilink")-2」）由原先的[普通話](../Page/普通話.md "wikilink")[廣播改組為全天候](../Page/廣播.md "wikilink")[客語播出](../Page/客語.md "wikilink")，並更名為「[客家公共頻道](../Page/梅州電視台客家公共頻道.md "wikilink")」。

  - 大陸唯一一部[客家話電視連續劇](../Page/客家話.md "wikilink")：[《圍屋人家》](https://web.archive.org/web/20150212000034/http://www.gdmztv.com/zhuanti/weiwurenjia/)，至今已播出四季，可在線收看[《圍屋人家》](https://web.archive.org/web/20150212000034/http://www.gdmztv.com/zhuanti/weiwurenjia/)。

### 其他客家地區電視台

  - [惠州市广播电视台](../Page/惠州市广播电视台.md "wikilink")[《客家》](https://web.archive.org/web/20150204220728/http://www.hzrtv.cn/geography/kj/)節目，每週一期，可在線收看（[《客家》](https://web.archive.org/web/20150204220728/http://www.hzrtv.cn/geography/kj/)）。
  - [賀州市广播电视台](../Page/賀州市广播电视台.md "wikilink")[《客家》](http://i.youku.com/u/UMjg5OTQ4NDY0/videos/)節目，可在線收看（[《客家》](http://i.youku.com/u/UMjg5OTQ4NDY0/videos/)）。
  - [龙岩市广播电视台](../Page/龙岩市广播电视台.md "wikilink")[《客家风》](https://web.archive.org/web/20150207143350/http://www.lytv.net.cn/kjf.asp)節目，可在線收看（[《客家风》](https://web.archive.org/web/20150207143350/http://www.lytv.net.cn/kjf.asp)）。
  - [博白县广播电视台](../Page/博白县广播电视台.md "wikilink")[《白州和韵》](http://www.gxbbtv.cn/e/action/ListInfo/?classid=4)節目，可在線收看（[《白州和韵》](http://www.gxbbtv.cn/e/action/ListInfo/?classid=4)）。
  - [大埔县广播电视台](../Page/大埔县广播电视台.md "wikilink")[《客家话新闻》](https://web.archive.org/web/20150207143251/http://www.gddbtv.com/index.aspx?lanmuid=65&sublanmuid=733)節目，可在線收看（[《客家话新闻》](https://web.archive.org/web/20150207143251/http://www.gddbtv.com/index.aspx?lanmuid=65&sublanmuid=733)）。
  - [兴宁市广播电视台](../Page/兴宁市广播电视台.md "wikilink")[《宁江风情》](http://www.gdxntv.com/portal.php?mod=list&catid=8)節目，可在線收看（[《宁江风情》](http://www.gdxntv.com/portal.php?mod=list&catid=8)）。
  - [梅县区广播电视台](../Page/梅县区广播电视台.md "wikilink")[《梅县新闻》](http://www.gdmxbtv.com/index.php?m=content&c=index&a=lists&catid=32)節目，可在線收看（[《梅县新闻》](http://www.gdmxbtv.com/index.php?m=content&c=index&a=lists&catid=32)）。
  - [蕉岭县广播电视台](../Page/蕉岭县广播电视台.md "wikilink")[《民生视点》](http://www.gdjltv.cn/vid.asp?vidcat_id=44)節目，可在線收看（[《民生视点》](http://www.gdjltv.cn/vid.asp?vidcat_id=44)）。
  - [五华县广播电视台](../Page/五华县广播电视台.md "wikilink")[《风土人情》](https://web.archive.org/web/20150207144442/http://www.gdwhtv.com//ftrq/index.html)節目，可在線收看（[《风土人情》](https://web.archive.org/web/20150207144442/http://www.gdwhtv.com//ftrq/index.html)）。

## 參考文獻

### 引用資料

### 研究書目

  - 張正田，〈從1926年臺灣漢人籍貫調查資料看「臺灣客家傳統地域」〉，《客家研究》(桃園中壢：國立中央大學客家學院)，3:
    2，2009.12，頁165-210。

## 另見

  - [客家地區](../Page/客家地區.md "wikilink")
  - [客家文化](../Page/客家文化.md "wikilink")
  - [客家方言](../Page/客家方言.md "wikilink")
  - [客家基因族譜](../Page/客家基因族譜.md "wikilink")
  - [台灣客家人](../Page/台灣客家人.md "wikilink")
  - [臺灣客家語](../Page/臺灣客家語.md "wikilink")
  - [客家基本法](../Page/s:客家基本法.md "wikilink")
  - [渡臺悲歌](../Page/渡臺悲歌.md "wikilink")
  - [福佬客](../Page/福佬客.md "wikilink")

## 外部链接

  - [行政院客家委員會](http://www.hakka.gov.tw)
  - [世界客屬總商會](http://www.whkba.org/zh/)
  - [世界客屬懇親大會](https://www.hakka2017.com/index)

### 學術網站

  - [國立中央大學客家學院](http://140.115.170.1/Hakkacollege/)
  - [國立中央大學客家研究中心](http://www.cc.ncu.edu.tw/~ncu3008/)
  - [國立交通大學國際客家研究中心](http://hakkacenter.nctu.edu.tw/)
  - [國立交通大學客家文化學院](http://hakka.nctu.edu.tw/)
  - [國立聯合大學全球客家研究中心](http://www2.nuu.edu.tw/~hakkacenter/ch/index.html)
  - [嘉应学院客家研究所](https://web.archive.org/web/20100505175005/http://www.jyu.edu.cn/kejia/)
  - [國立高雄師範大學客家文化研究所](http://www.nknu.edu.tw/~hakka/)
  - [客語教師進修網](https://web.archive.org/web/20110510145416/http://www2.nknu.edu.tw/thakka/i_main.htm)
  - [台灣客家數位圖書館](http://hakkalib.ncl.edu.tw/index.jsp)

[Category:漢族支系](../Category/漢族支系.md "wikilink")
[客家](../Category/客家.md "wikilink")
[Category:福建族群](../Category/福建族群.md "wikilink")
[Category:广东族群](../Category/广东族群.md "wikilink")

1.

2.

3.  客家人紅細胞分佈:<http://www.ivpp.cas.cn/cbw/rlxxb/xbwzxz/201205/P020120522348922735138.pdf>

4.  李輝 等人（2003）客家人起源的遗传学分析，遺傳學報30(9)873-880。
    <http://www.ebiotrade.com/emagazine/content/2/2003_9_30_9/E261AD36-F5E0-4EAD-B9EC-01422096B73D/pdf/3913.pdf>

5.  客家源流新探:<http://www.chiapowsoohakka.org.sg/HakkaCulture.html>

6.  《中国大陆客家人居的空间分布及群体特征》 广西民族研究2007年第四期（总第90期） 钟声宏、黄德权著

7.

8.  [鐘山，《走向海洋的優秀民係—漫話港澳臺的客家人和客屬組織》，你好台灣客家鄉親網站，中央人民廣播電臺對臺灣廣播中心](http://big5.am765.com/hakka/kjwh/lsyy/200705/t20070531_2946.htm)

9.  [香港客家人](http://www.cnki.com.cn/Article/CJFDTotal-DWDC199711025.htm)

10. 指1898年7月1日租借新界和新九龍之前，已在香港存在的村落的居民後代。

11. 澳門客家人約占五分之一:<http://www.epochtimes.com/b5/6/5/28/n1332666.htm>

12. [客家-{庄}-分佈圖](http://www2.nknu.edu.tw/thakka/item08/c8_1.htm#)

13. [客家人口分佈圖](http://www.nknu.edu.tw/~hakka/hakkamap/04.htm)

14. 東南亞客家的變貌-新加坡與馬來西亞:<http://isbn.ncl.edu.tw/NEW_ISBNNet/C00_index.php?&Pfile=2346&KeepThis=true&TB_iframe=true&width=900&height=650>

15. 論泰國客家華人的歷史:<http://hk.crntt.com/doc/1002/4/6/1/100246156.html?coluid=160&kindid=6363&docid=100246156&mdate=0923234603>

16. 跨洋尋夢-法國留尼旺的客家人:<http://www.kejialianmeng.com/portal.php?mod=view&aid=3694>

17. 客家的定義-源流和分布:<http://www.hakkausa.org/Welcome%20to%20AmericaHakkaAssociation!_files/main_04.htm>

18. 我的母語是艾話/涯話/客家話:<http://blog.sina.com.cn/s/blog_4e70d8230101clg2.html>

19.

20.

21.

22. 淺論閩西與台灣客家的關係:<http://www.huaxia.com/zhwh/whrd/whrdwz/2010/08/2048956.html>

23.

24.

25. [鐘山，《走向海洋的優秀民係—漫話港澳臺的客家人和客屬組織》，你好台灣客家鄉親網站，中央人民廣播電臺對臺灣廣播中心](http://big5.am765.com/hakka/kjwh/lsyy/200705/t20070531_2946.htm)

26.

27. 原居係指1898年7月1日租借新界和新九龍之前，已在香港存在的村落的居民後代

28. [香港客家人](http://www.cnki.com.cn/Article/CJFDTotal-DWDC199711025.htm)

29.

30.

31.

32.
33.

34.
35. [農曆正月廿日 全國客家日](http://udn.com/NEWS/NATIONAL/NAT5/5842857.shtml)

36. 客家圍屋-漢代塢堡的活化石:<http://bbs.tianya.cn/post-free-6047444-1.shtml>

37. 圍屋-塢堡的活化石:<http://bj.sina.com.cn/t/2007-08-13/1150148155.shtml>

38. 圍龍屋:<https://www.chiculture.net/30047/c25.html>

39. 人民日報海外版-關西新圍:<http://www.people.com.cn/BIG5/paper39/4980/533192.html>

40. 《客家源流新論：誰是客家人》/陳支平 第一版 台北市：臺原 民87頁146 承啟樓

41. 中國文化研究院-福建土樓:<https://www.chiculture.net/index.php?file=topic_description&old_id=30025>

42. 文匯報-大萬世居:<http://sp.wenweipo.com/2015wan/?action-viewnews-itemid-23>

43. 灼見名家-客家大型圍村曾大屋:<https://www.master-insight.com/%E5%AE%A2%E5%AE%B6%E5%A4%A7%E5%9E%8B%E5%9C%8D%E6%9D%91%E6%9B%BE%E5%A4%A7%E5%B1%8B/>

44. 平凡人遊記-成都洛帶古鎮:<http://arho25.blogspot.com/2013/11/blog-post_23.html>

45. 客家小麗江-梅州百侯古鎮:<http://www.lotour.com/zhengwen/2/lg-jc-20912.shtml>

46. 閩北千年古鎮-邵武和平古鎮:<http://lj6210.blog.sohu.com/277169117.html>

47. 人民網-永定客家古鎮開街:<http://fj.people.com.cn/BIG5/n2/2018/0519/c181466-31600418.html>

48. 嶺南四大古鎮-鬆口古鎮:<https://3g.163.com/dy/article/D505VOJC0524DKDH.html>

49. 中國最美的十大古鎮-長汀古鎮:<https://www.zhongguofeng.com/guzhen/9208.html>

50. 甘坑客家小鎮-步步尋往跡，有處特依依:<http://blackmomo.tw/2015-05-05-554/>

51. 觀瀾文化小鎮納入省級特色小鎮培育庫:<http://www.sohu.com/a/277546177_669715>

52. 贛縣白鷺古村老屋印象:<http://kuaibao.qq.com/s/20181229A0AMRP00?refer=spider>

53. Sunny 旅遊足跡-河源南園古村

54. 河源蘇家圍是蘇東坡後裔的聚居地:<https://zh.meet99.com/jingdian-sujiawei.html>

55. 河源林寨古村再獲國際殊榮:<http://www.hkcd.com/content/2017-06/07/content_1051587.html>

56. 廣東韶關／歷史文化名村-石塘古村:<https://udn.com/news/story/7332/3518287>

57. 千年客家古村－江西南康唐江镇卢屋村:<http://blog.sina.com.cn/s/blog_7ed2b8de0100r3a3.html>

58. 客家民居坐落「世外桃源」-梅州橋溪古村:<http://paper.wenweipo.com/2018/11/09/GS1811090010.htm>

59. 每一道客家菜，都有不為人知的故事來歷:<http://www.hkcd.com/content_p/2017-06/20/content_45793.html>

60. 客家飲食文化根在中原:<http://dumpling.webcom8.com/news_detail.asp?id=166>

61. 客家菜的南遷路:<http://big5.sinopec.com/group/qywh/shwy/20130625/news_20130625_512840000000.shtml>

62. 客家菜-中州古味:<https://zhuanlan.zhihu.com/p/58239780>

63. 中國烹飪大師陳鋼文-論“客家菜吃得有根有據”-客家菜源於中原，不忘本根:<http://www.mzta.gov.cn/info/news/hakka/2018/13322.html>

64. 中州古味客家菜:<http://blog.sina.com.cn/s/blog_484879d9010001ku.html>