**-{zh-hans:佛拉多·巴金斯;
zh-hant:佛羅多·巴金斯;}-**（），[英國](../Page/英國.md "wikilink")[作家](../Page/作家.md "wikilink")[J·R·R·托爾金的](../Page/J·R·R·托爾金.md "wikilink")《[魔戒](../Page/魔戒.md "wikilink")》小說和[新线电影公司同名电影中的人物](../Page/新线电影公司.md "wikilink")。他是居住在[夏爾](../Page/夏爾_\(中土大陸\).md "wikilink")[袋底洞的一名](../Page/袋底洞.md "wikilink")-{zh-hans:霍比特人;
zh-hant:哈比人;}-，於[第三紀元](../Page/第三紀元.md "wikilink")2968年9月22日出生。-{zh-hans:佛拉多·巴金斯;
zh-hant:佛羅多·巴金斯;}-是魔戒整个故事的一条主线也是最主要的人物之一。

## 主要情节

以下是以-{zh-hans:佛拉多·巴金斯;
zh-hant:佛羅多·巴金斯;}-为主线的《[魔戒三部曲](../Page/魔戒電影三部曲.md "wikilink")》電影情节，以其他人为主线的情节请参看相应条目内容。

### 夏爾

\-{zh-hans:佛拉多·巴金斯; zh-hant:佛羅多·巴金斯;}-原本只是一個普通的-{zh-hans:霍比特人;
zh-hant:哈比人;}-，与叔叔[比爾博·巴金斯一起生活](../Page/比爾博·巴金斯.md "wikilink")。在[夏尔他还有几个相当不错的邻居](../Page/夏尔.md "wikilink")，包括他的园丁-{zh-hans:[山姆·甘姆齐](../Page/山姆怀斯.md "wikilink");
zh-hant:山姆·衛斯詹吉;}-，好朋友-{zh-hans:梅利; zh-hant:梅里;}-和-{zh-hans:皮平;
zh-hant:皮聘;}-。在比爾博111岁生日的时候，灰袍巫师[-{zh-hans:甘道夫;
zh-hant:甘道夫;}-到来为比尔博庆祝](../Page/甘道夫.md "wikilink")，不料在[生日宴会上发生了意想不到的事情](../Page/生日宴会.md "wikilink")，佛羅多才知道叔叔有一个神奇戒指（根据后来的[霍比特人电影结尾提及](../Page/霍比特人电影系列.md "wikilink")，甘道夫已经知道有个戒指，但是不知道是万戒之王，所以没怎么重视）。當時-{zh-hans:甘道夫;
zh-hant:甘道夫;}-也不知道比尔博到底有一个怎么样的戒指，从戒指释放出的力量他判断，这不是一个普通的东西，经过一番查证，他断定这就是索倫的戒指。因为[魔戒的力量过于强大](../Page/至尊魔戒.md "wikilink")，-{zh-hans:甘道夫;
zh-hant:甘道夫;}-自己无法控制魔戒的力量，于是他命令-{zh-hans:佛拉多;
zh-hant:佛羅多;}-带着魔戒去[瑞文戴爾寻求办法](../Page/瑞文戴爾.md "wikilink")。

### 往瑞文戴爾途中

[甘道夫建議](../Page/甘道夫.md "wikilink")（或命令）他與園丁山姆以及兩位友人-{zh-hans:梅利;
zh-hant:梅里;}-與-{zh-hans:皮平;
zh-hant:皮聘;}-出發前往瑞文戴爾，途中遇上[戒靈追捕](../Page/戒靈.md "wikilink")，後因[-{zh-hans:阿拉贡;
zh-hant:亞拉岡;}-](../Page/亞拉岡_\(魔戒\).md "wikilink")（即之後的人皇）幫助而順利抵達，但在途中-{zh-hans:佛拉多;
zh-hant:佛羅多;}-於風雲頂已遭到戒靈之首的巫王刺傷，而後經由瑞文戴爾的精靈公主[亞玟](../Page/亞玟.md "wikilink")（在原著小說是精靈领主[格洛芬戴尔](../Page/格洛芬戴尔.md "wikilink")）接应救往瑞文戴爾。

### 远征队的建立和解散

在到達瑞文戴爾後，精靈王[愛隆招開了一場會議](../Page/愛隆.md "wikilink")，佛羅多與其他三名-{zh-hans:霍比特人;
zh-hant:哈比人;}-（山姆、-{zh-hans:梅利; zh-hant:梅里;}-與-{zh-hans:皮平;
zh-hant:皮聘;}-）、[-{zh-hans:阿拉贡;
zh-hant:亞拉岡;}-](../Page/亞拉岡_\(魔戒\).md "wikilink")、[波羅莫](../Page/波羅莫.md "wikilink")、甘道夫、精靈[勒苟拉斯](../Page/勒苟拉斯.md "wikilink")、矮人[金靂被賦予前往末日火山摧毀魔戒的使命成為](../Page/金靂.md "wikilink")[魔戒遠征隊](../Page/魔戒遠征隊.md "wikilink")，但他們選錯了捷徑，誤入了已成為[炎魔及大量](../Page/摩瑞亞炎魔.md "wikilink")[半獸人盤據之地的](../Page/半獸人.md "wikilink")[摩瑞亞](../Page/摩瑞亞.md "wikilink")，-{zh-hans:甘道夫;
zh-hant:甘道夫;}-為確保眾人安危而與炎魔雙雙墜入[都靈深淵中](../Page/都靈.md "wikilink")。

遠征隊剩餘成員前往[羅斯洛利安](../Page/羅斯洛立安.md "wikilink")，在獲得充分補給後決定走水路前往[魔多](../Page/魔多.md "wikilink")，但被由白轉黑的[巫師](../Page/巫師_\(中土大陸\).md "wikilink")[-{zh-hans:萨茹曼;
zh-hant:薩魯曼;}-從中阻擾](../Page/薩魯曼.md "wikilink")，派出[強獸人追殺遠征隊](../Page/強獸人.md "wikilink")，波羅莫因而喪生，-{zh-hans:梅利;
zh-hant:梅里;}-與-{zh-hans:皮平; zh-hant:皮聘;}-遭強獸人俘虜，-{zh-hans:佛拉多;
zh-hant:佛羅多;}-與山姆渡船前往魔多。

### 與-{zh-hans:古魯姆; zh-hant:咕嚕;}-在一起

[-{zh-hans:阿拉貢;
zh-hant:亞拉岡;}-](../Page/亞拉岡_\(魔戒\).md "wikilink")，金靂和精靈王子勒苟拉斯以為-{zh-hans:佛拉多;
zh-hant:佛羅多;}-被半獸人抓走，因此他們尾隨前往拯救他們。其實-{zh-hans:佛拉多;
zh-hant:佛羅多;}-與山姆已經乘坐小船離開了。

之後他們帶著精靈族給的乾糧繼續往厄運山谷的方向前進，由於魔戒的力量越來越強大，半獸人的隊伍也不時地出現在他們面前。

不久之後他們發現有什麼東西一直尾隨他們，這就是魔戒的第二個半身人主人，已經被魔戒的力量改變得面目全非的-{zh-hans:古魯姆;
zh-hant:咕嚕;}-。-{zh-hans:佛拉多;
zh-hant:佛羅多;}-自認馴服了他，希望他幫他們帶路前往末日火山，但是山姆自開始就認定-{zh-hans:古魯姆;
zh-hant:咕嚕;}-並不可靠，對其懷有戒心。也許-{zh-hans:古魯姆;
zh-hant:咕嚕;}-本來真的存心向善，但魔戒的力量實在太過強大—他最後背叛了兩名-{zh-hans:霍比特人;
zh-hant:哈比人;}-。

在[米那斯魔窟的山谷](../Page/米那斯魔窟.md "wikilink")，[咕嚕首先使用離間計讓](../Page/咕嚕.md "wikilink")-{zh-hans:佛拉多;
zh-hant:佛羅多;}-趕走了山姆，再暗地裡將精靈乾糧掉下深谷，然後把-{zh-hans:佛拉多;
zh-hant:佛羅多;}-騙入蜘蛛怪[屍羅的巢穴](../Page/屍羅_\(魔戒\).md "wikilink")，-{zh-hans:佛拉多;
zh-hant:佛羅多;}-因而被屍羅注入毒液。另一方面，山姆在山腳下看到了被-{zh-hans:古魯姆;
zh-hant:咕嚕;}-扔下的精靈乾糧，義無反顧的返回，當發現-{zh-hans:佛拉多;
zh-hant:佛羅多;}-遇害時，悲痛萬分，山姆毅然決然撿起魔戒繼續旅程，但在途中聽見半獸人說其實-{zh-hans:佛拉多;
zh-hant:佛羅多;}-只是被注入毒液的呈現假死狀態時，就計劃去拯救-{zh-hans:佛拉多; zh-hant:佛羅多;}-。

山姆成功從毫無戒備的半獸人士兵手下救出了-{zh-hans:佛拉多;
zh-hant:佛羅多;}-，而兩人也打扮成半獸人，潛入魔多到達[末日火山](../Page/末日火山.md "wikilink")。

### 末日山谷

當兩人終於抵達末日火山中的烈火之廳時，-{zh-hans:佛拉多;
zh-hant:佛羅多;}-已不再有足夠的意志力來對抗魔戒，他被擊垮了，在他戴上魔戒隐身准备逃走的时候，-{zh-hans:古鲁姆;
zh-hant:咕嚕;}-突然出现。已经被魔戒的黑暗力量完全吸引的-{zh-hans:古鲁姆;
zh-hant:咕嚕;}-跳到-{zh-hans:佛拉多;
zh-hant:佛羅多;}-的身上抢夺魔戒。他將魔戒連同-{zh-hans:佛拉多;
zh-hant:佛羅多;}-的手指一同咬下，但是随后在與佛羅多搏鬥中失身落入[熔岩中](../Page/熔岩.md "wikilink")，同魔戒一起在熔岩中燃烧殆尽。佛羅多也掉下，幸好一手緊握著懸崖，最後被山姆所拯救。魔戒释放的巨大力量导致了[火山的喷发](../Page/火山.md "wikilink")。巨大的熔岩流追赶着两人，并把他们最终包围在一块巨石之上。

看着身边滚滚的炙热熔岩流和满天飞舞的火山灰和落下的石块，二人充满了绝望，只是等待着死亡的到来。好在很快[-{zh-hans:甘道夫;
zh-hant:甘道夫;}-派来的救兵两只](../Page/甘道夫.md "wikilink")[巨鷹将他们救起](../Page/巨鷹.md "wikilink")。

### 国王归来

经过多天的修养，-{zh-hans:佛拉多; zh-hant:佛羅多;}-终于醒来，他的朋友们和[-{zh-hans:刚多尔夫;
zh-hant:甘道夫;}-都来看望他](../Page/甘道夫.md "wikilink")。魔戒被彻底摧毁，索倫的黑暗力量再也没有复苏的可能，中土世界暂时回到了和平的状态。由于四个-{zh-hans:霍比特人;
zh-hant:哈比人;}-在整个摧毁魔戒的过程中起到了举足轻重的作用，所以在[-{zh-hans:阿拉贡;
zh-hant:亞拉岡;}-的登基仪式上受到了来自各族的最崇高的敬意](../Page/亞拉岡_\(魔戒\).md "wikilink")。

### 返回夏尔

[缩略图| 電影裡的佛羅多·巴金斯（[伊利亞·伍德](../Page/伊利亞·伍德.md "wikilink") 飾演）
](https://zh.wikipedia.org/wiki/File:Frodo_Baggins.jpg "fig:缩略图| 電影裡的佛羅多·巴金斯（伊利亞·伍德 飾演） ")
-{zh-hans:佛拉多;
zh-hant:佛羅多;}-和他的朋友在阿拉贡的王国住了一段时间后向西返回夏尔。经过这一系列的洗礼，他们都变化了很多。-{zh-hans:佛拉多;
zh-hant:佛羅多;}-试着把这一切都记录下来，但是寫到一半，他的任务就已经结束了，由于在風雲頂受過戒靈之王[安格瑪巫王的一劍](../Page/安格馬巫王.md "wikilink")，當時的傷口仍然不時作痛，他必须跟从老[比尔博·巴金斯](../Page/比尔博·巴金斯.md "wikilink")，[-{zh-hans:刚多尔夫;
zh-hant:甘道夫;}-等乘着精灵的大船驶往不死之地](../Page/甘道夫.md "wikilink")[瓦林诺](../Page/瓦林诺.md "wikilink")，於是剩下的内容就由山姆来完成。

## 原著情节

佛羅多和他的朋友在返回夏爾後，發現萨魯曼秘密的來到了夏爾施行暴虐統治，在經歷了[臨水之戰後](../Page/臨水之戰.md "wikilink")，薩魯曼被[葛力馬·巧言所殺](../Page/葛力馬·巧言.md "wikilink")。

## 名字来源

佛罗多（原文：Frodo）是[托尔金用古英语frod](../Page/托尔金.md "wikilink")（睿智）和常见的阳性词尾-o组合而成\[1\]。

托尔金为他设想的真实名字是Maura，是一个哈比人的传统名字，由字根Maur-和哈比人常见的男性人名词尾-a构成，Maur-或许和[洛汗语的形容词Maur](../Page/洛汗语.md "wikilink")-（睿智）同源，因此，在托尔金的设定中，Frodo是Maura的译名。

## 脚注

<references />

## 参看

  - [魔戒](../Page/魔戒.md "wikilink")
  - [甘道夫](../Page/甘道夫.md "wikilink")
  - [亞拉岡 (魔戒)](../Page/亞拉岡_\(魔戒\).md "wikilink")

## 外部链接

[de:Figuren in Tolkiens Welt\#Frodo
Beutlin](../Page/de:Figuren_in_Tolkiens_Welt#Frodo_Beutlin.md "wikilink")
[simple:Middle-earth characters\#Frodo
Baggins](../Page/simple:Middle-earth_characters#Frodo_Baggins.md "wikilink")

[Category:魔戒的角色](../Category/魔戒的角色.md "wikilink")
[Category:魔戒](../Category/魔戒.md "wikilink")
[Category:中土大陸的哈比人](../Category/中土大陸的哈比人.md "wikilink")

1.  《中土的人民》