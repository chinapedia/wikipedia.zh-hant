**梶山静六**（），[日本政治家](../Page/日本.md "wikilink")，[日本自由民主党成员](../Page/日本自由民主党.md "wikilink")，曾任自民党干事长、法务大臣、通商产业大臣、自治大臣、国家公安委员会委员长和[桥本龙太郎内阁官房长官](../Page/桥本龙太郎.md "wikilink")。

[Category:日本内阁官房长官](../Category/日本内阁官房长官.md "wikilink")
[Category:日本自由民主党干事长](../Category/日本自由民主党干事长.md "wikilink")
[Category:日本自由民主黨國會對策委員長](../Category/日本自由民主黨國會對策委員長.md "wikilink")
[Category:日本法务大臣](../Category/日本法务大臣.md "wikilink")
[Category:日本通商产业大臣](../Category/日本通商产业大臣.md "wikilink")
[Category:日本自治大臣](../Category/日本自治大臣.md "wikilink")
[Category:日本国家公安委员会委员长](../Category/日本国家公安委员会委员长.md "wikilink")
[Category:竹下內閣閣僚](../Category/竹下內閣閣僚.md "wikilink")
[Category:宇野內閣閣僚](../Category/宇野內閣閣僚.md "wikilink")
[Category:第二次海部內閣閣僚](../Category/第二次海部內閣閣僚.md "wikilink")
[Category:第一次橋本內閣閣僚](../Category/第一次橋本內閣閣僚.md "wikilink")
[Category:第二次橋本內閣閣僚](../Category/第二次橋本內閣閣僚.md "wikilink")
[Category:日本大學校友](../Category/日本大學校友.md "wikilink")
[Category:茨城縣出身人物](../Category/茨城縣出身人物.md "wikilink")
[Category:朝鮮綁架日本人問題相關人物](../Category/朝鮮綁架日本人問題相關人物.md "wikilink")
[Category:日本眾議院議員
1969–1972](../Category/日本眾議院議員_1969–1972.md "wikilink")
[Category:日本眾議院議員
1972–1976](../Category/日本眾議院議員_1972–1976.md "wikilink")
[Category:日本眾議院議員
1979–1980](../Category/日本眾議院議員_1979–1980.md "wikilink")
[Category:日本眾議院議員
1980–1983](../Category/日本眾議院議員_1980–1983.md "wikilink")
[Category:日本眾議院議員
1983–1986](../Category/日本眾議院議員_1983–1986.md "wikilink")
[Category:日本眾議院議員
1986–1990](../Category/日本眾議院議員_1986–1990.md "wikilink")
[Category:日本眾議院議員
1990–1993](../Category/日本眾議院議員_1990–1993.md "wikilink")
[Category:日本眾議院議員
1993–1996](../Category/日本眾議院議員_1993–1996.md "wikilink")
[Category:日本眾議院議員
1996–2000](../Category/日本眾議院議員_1996–2000.md "wikilink")
[Category:勳一等旭日大綬章獲得者](../Category/勳一等旭日大綬章獲得者.md "wikilink")
[Category:茨城縣選出日本眾議院議員](../Category/茨城縣選出日本眾議院議員.md "wikilink")