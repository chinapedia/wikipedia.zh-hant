《**Nothing Really
Matters**》是[香港](../Page/香港.md "wikilink")[歌手](../Page/歌手.md "wikilink")[陳奕迅的](../Page/陳奕迅.md "wikilink")[粵語音樂專輯](../Page/粵語.md "wikilink")，於2000年7月21日發行。

## 專輯簡介

此乃陳奕迅於[華星唱片時期的最後一張錄音室專輯](../Page/華星唱片.md "wikilink")，因推出大碟後不久便轉簽[英皇娛樂](../Page/英皇娛樂.md "wikilink")，這專輯並沒有獲得足夠宣傳。然而陳奕迅在這專輯嘗試了多種曲風，例如《Made
In Hong Kong》的重型[搖滾](../Page/搖滾.md "wikilink")、《美麗有罪》的[Techno
Music等](../Page/鐵克諾.md "wikilink")，均為Eason 首次嘗試的曲風。另外，這專輯亦收錄了Eason
一首廣為人知的歌曲《黑夜不再來》，此曲其後被[容祖兒](../Page/容祖兒.md "wikilink")、[蘇永康等歌手翻唱](../Page/蘇永康.md "wikilink")。

## 曲目

## 派台歌曲成績

《黑夜不再來》、《美麗有罪》及《當這地球沒有花》

| 歌曲      | 903 專業推介 | RTHK 中文歌曲龍虎榜 | 997 勁爆流行榜 | TVB 勁歌金榜 |
| ------- | -------- | ------------ | --------- | -------- |
| 黑夜不再來   | **1**    | 17           | 16        | \-       |
| 美麗有罪    | 3        | 17           | 5         | \-       |
| 當這地球沒有花 | 5        | \-           | \-        | \-       |

## 相關獎項或提名

### 歌曲《黑夜不再來》

  - [2001年香港電影金像獎](../Page/2001年香港電影金像獎.md "wikilink")－最佳原創電影歌曲（提名）

[Category:陳奕迅音樂專輯](../Category/陳奕迅音樂專輯.md "wikilink")
[Category:陳奕迅粵語專輯](../Category/陳奕迅粵語專輯.md "wikilink")
[Category:香港音樂專輯](../Category/香港音樂專輯.md "wikilink")
[Category:2000年音樂專輯](../Category/2000年音樂專輯.md "wikilink")