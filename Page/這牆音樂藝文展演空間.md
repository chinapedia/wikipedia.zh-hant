**這牆音樂藝文展演空間**（The Wall Live House），簡稱**The
Wall**，是位於[台北市的](../Page/台北市.md "wikilink")[音樂展演空間](../Page/音樂展演空間.md "wikilink")（Livehouse）。

## 簡介

這牆音樂藝文展演空間由[傅鉛文](../Page/傅鉛文.md "wikilink")、[祝驪雯經營](../Page/祝驪雯.md "wikilink")。原本由[春天吶喊主辦人Jimi](../Page/春天吶喊.md "wikilink")（牟齊民）、[董事長樂團](../Page/董事長樂團.md "wikilink")[阿吉](../Page/吳永吉.md "wikilink")、[閃靈樂團主唱](../Page/閃靈樂團.md "wikilink")[Freddy於](../Page/Freddy.md "wikilink")2003年創立，因與傅鉛文經營理念不合於2013年撤出，將持股轉售予傅鉛文及其經營團隊。這牆音樂藝文展演空間亦曾新增[高雄市](../Page/高雄市.md "wikilink")[駁二藝術特區](../Page/駁二藝術特區.md "wikilink")[THE
WALL
駁二](../Page/THE_WALL_駁二.md "wikilink")(2010年)、[宜蘭縣](../Page/宜蘭縣.md "wikilink")[舊宜蘭菸酒賣捌所](../Page/舊宜蘭菸酒賣捌所.md "wikilink")[THE
WALL
賣捌所](../Page/THE_WALL_賣捌所.md "wikilink")(2011年)等空間，後高雄市政府與宜蘭縣政府相繼與其解約結束[駁二藝術特區及](../Page/駁二藝術特區.md "wikilink")[舊宜蘭菸酒賣捌所之委託經營](../Page/舊宜蘭菸酒賣捌所.md "wikilink")。這牆音樂藝文展演空間於2009年開始轉型為[THE
WALL
MUSIC延伸其組織功能目標](../Page/THE_WALL_MUSIC.md "wikilink")，主辦各種台灣樂團全國巡迴、中小型[演唱會](../Page/演唱會.md "wikilink")，並邀請歐美日多國演出團體。2010\~2012年曾經承辦高雄一年一度的[大港開唱](../Page/大港開唱.md "wikilink")，2013年曾舉辦[野台開唱](../Page/野台開唱.md "wikilink")。

這牆佔地300餘[平方米](../Page/平方米.md "wikilink")，可容納量約600人，提供[搖滾樂團](../Page/搖滾樂團.md "wikilink")、[獨立樂團與](../Page/獨立樂團.md "wikilink")[創作歌手的表演場地](../Page/創作歌手.md "wikilink")，並曾被《[紐約雜誌](../Page/紐約雜誌.md "wikilink")》（*New
York
Magazine*）選為台北十大景點之一，並獲《[紐約時報](../Page/紐約時報.md "wikilink")》、《[Monocle](../Page/Monocle.md "wikilink")》等國際媒體介紹報導。

## 演唱樂團

這牆音樂藝文展演空間採取與樂團合作，本身提供場地和樂器租用，樂團負責宣傳和票務。開幕至今有以下國外樂團演出：

  - 2008：
      - 美國：Explosions in The sky、Her Space Holiday、Steve Aoki、Yo La
        Tengo、Plus Minus
      - 英國：Epic45、Camera Obscura
      - 日本：LM.C、te'、Hyakkei、miyavi、4 bonjour's
        parties、[toe](../Page/Toe_\(樂團\).md "wikilink")、Mondialito、清春、Bloodthirsty
        Butchers、nhhmbase、moools、Plastic Tree
      - 德國：Maximilian Hecker
      - 法國：Keren Ann、Guns N' Bombs、Uffie、Vicarious Bliss、Tahiti 80
      - 捷克：The Plastic People of the Universe
      - 加拿大：Broken Social Scene

<!-- end list -->

  - 2007：
      - 美國：The Appleseed Cast、The Apples in Stereo、Album Leaf、NOFX、Mice
        Parade
      - 日本：Luminous
        Orange、[長谷川俊](../Page/長谷川俊.md "wikilink")、quizmaster、Plastic
        Tree、Six o' minus、[下川美娜](../Page/下川美娜.md "wikilink")
      - 英國：Yndi Halda
      - 德國：Maximilian Hecker
      - 瑞典：Pernilla Andersson

<!-- end list -->

  - 2006年：
      - 美國：Plus Minus、The Microphones、Caroline、The Meeting Places、RAMBO
      - 日本：Clione-index、Honey's、Piana、Advantage
        Lucy、[Ra:IN](../Page/Ra:IN.md "wikilink")、The
        shaverz、[福山芳樹](../Page/福山芳樹.md "wikilink")、[清春](../Page/清春.md "wikilink")、envy
      - 英國：Four Tet
      - 澳洲：Art of Fighting
      - 德國：Maximilian Hecker
      - 瑞典：Pelle Carlberg
      - 挪威：Norther

<!-- end list -->

  - 2005年：
      - 英國：Black Rebel Motorcycle Club、Transition
      - 日本：Boom Boom Satellite、Bloodthirsty butchers、Endzweck、Goofy
        Style、quizmaster、Roly Poly Rag Bear、World's End Girlfriend
      - 香港：My Little Airport、JoyTrendySound、A Company
      - 美國：Michelle Shocked、Plus Minus、Stuart Hamm、Vincent Gallo
      - 冰島：[múm](../Page/múm.md "wikilink")

<!-- end list -->

  - 2004年：
      - 日本：Akiakane、Impiety
      - 瑞典：Club 8
      - 美國：American Analog Set、Cat Power
      - 英國：Four Tet
      - 西班牙：Mus

<!-- end list -->

  - 2003年：
      - 英國: Mogwai

## 外部連結

  - [The Wall Live House官方網站](http://www.TheWall.tw)
  - [紐約時報報導](https://web.archive.org/web/20070309022130/http://travel2.nytimes.com/2006/02/12/travel/12going.html?pagewanted=2)
  - [紐約雜誌：台北十大景點](http://www.nymetro.com/travel/travelcolumn/16090/)

[Category:台灣音樂場地](../Category/台灣音樂場地.md "wikilink") [Category:大安區
(臺北市)](../Category/大安區_\(臺北市\).md "wikilink")