**中沙群島**，1946年之前[中華民國官方称](../Page/中華民國.md "wikilink")**南沙群岛**，是[南海上的四大](../Page/南海.md "wikilink")[群島之一](../Page/南海諸島.md "wikilink")，位於[西沙群島東南約](../Page/西沙群島.md "wikilink")100[公里](../Page/公里.md "wikilink")、[東沙群島西南遠方](../Page/東沙群島.md "wikilink")、[南沙群島北方](../Page/南沙群島.md "wikilink")，主要島嶼為黃岩島。但嚴格來說，除了[中华人民共和国所控之](../Page/中华人民共和国.md "wikilink")[黃岩島外](../Page/黃岩島.md "wikilink")，中沙群島大部分只是一群沒有露出水面的珊瑚礁石。

中沙群島正如南海上其他島嶼一樣，鄰近海域有豐富漁產，還可能有[石油及](../Page/石油.md "wikilink")[天然氣等資源](../Page/天然氣.md "wikilink")。因為淺灘，所以鄰近海域航行困難。目前中沙群島沒有常住居民，但[中華人民共和國](../Page/中華人民共和國.md "wikilink")、[中華民國及](../Page/中華民國.md "wikilink")[菲律賓都聲稱擁有中沙群島的主權](../Page/菲律賓.md "wikilink")，而目前中華人民共和國於2012年起實際控制中沙群島唯一露出水面的[黃岩島](../Page/黃岩島.md "wikilink")\[1\]\[2\]。中華民國之中沙群島管理權責單位為[海巡署](../Page/海巡署.md "wikilink")。而[菲律賓則歸屬](../Page/菲律賓.md "wikilink")[三描禮士省管理](../Page/三描禮士省.md "wikilink")。中華人民共和國的中沙群島行政上由[海南省](../Page/海南省.md "wikilink")[三沙市管轄](../Page/三沙市.md "wikilink")。

## 歷史

[宋](../Page/宋朝.md "wikilink")[明以來](../Page/明朝.md "wikilink")，中沙群島被稱為「**千里長沙」**。明代《[鄭和航海圖](../Page/鄭和航海圖.md "wikilink")》把中沙群島稱為「石星石塘」。[清代](../Page/清朝.md "wikilink")[谢清高在](../Page/谢清高.md "wikilink")《[海录](../Page/海录.md "wikilink")》中把中沙群岛称作「红毛浅」。

1946年9月13日，[中華民國政府](../Page/中華民國政府.md "wikilink")[行政院將三十年代命名的](../Page/行政院.md "wikilink")“南沙群島”改名為“中沙群島”，而“團沙群島”則改名為[南沙群島](../Page/南沙群島.md "wikilink")。內政部編制《南海諸島新舊名稱對照表》，於1947年11月正式公佈\[3\]，並在官方地圖上標示。

中沙群島及附近水域目前由[中華人民共和國控制](../Page/中華人民共和國.md "wikilink")，並由海軍巡邏及駐守。

## 分布

<onlyinclude>

<table>
<thead>
<tr class="header">
<th><p>官方名称</p></th>
<th><p>英语名称</p></th>
<th><p>菲律宾官方名称</p></th>
<th><p>实际控制方</p></th>
<th><p>位置</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong><a href="../Page/中沙群島.md" title="wikilink">中沙群島</a></strong><br />
</p></td>
<td><p><a href="../Page/中沙大环礁.md" title="wikilink">中沙大环礁</a></p></td>
<td><p>外缘</p></td>
<td><p><a href="../Page/西门暗沙.md" title="wikilink">西门暗沙</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/本固暗沙.md" title="wikilink">本固暗沙</a></p></td>
<td></td>
<td><p>—</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/美滨暗沙.md" title="wikilink">美滨暗沙</a></p></td>
<td></td>
<td><p>—</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鲁班暗沙.md" title="wikilink">鲁班暗沙</a></p></td>
<td></td>
<td><p>—</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/中北暗沙.md" title="wikilink">中北暗沙</a></p></td>
<td></td>
<td><p>—</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/比微暗沙.md" title="wikilink">比微暗沙</a></p></td>
<td><p><br />
（）</p></td>
<td><p>—</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/隐矶滩.md" title="wikilink">隐矶滩</a></p></td>
<td><p><br />
（）</p></td>
<td><p>—</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/武勇暗沙.md" title="wikilink">武勇暗沙</a></p></td>
<td></td>
<td><p>—</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/济猛暗沙.md" title="wikilink">济猛暗沙</a></p></td>
<td></td>
<td><p>—</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/海鸠暗沙.md" title="wikilink">海鸠暗沙</a></p></td>
<td></td>
<td><p>—</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/安定连礁.md" title="wikilink">安定连礁</a></p></td>
<td></td>
<td><p>—</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/美溪暗沙.md" title="wikilink">美溪暗沙</a></p></td>
<td></td>
<td><p>—</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/布德暗沙.md" title="wikilink">布德暗沙</a></p></td>
<td></td>
<td><p>—</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/波洑暗沙.md" title="wikilink">波洑暗沙</a></p></td>
<td></td>
<td><p>—</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/排波暗沙.md" title="wikilink">排波暗沙</a></p></td>
<td></td>
<td><p>—</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/果淀暗沙.md" title="wikilink">果淀暗沙</a></p></td>
<td></td>
<td><p>—</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/排洪滩.md" title="wikilink">排洪滩</a></p></td>
<td></td>
<td><p>—</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/涛静暗沙.md" title="wikilink">涛静暗沙</a></p></td>
<td></td>
<td><p>—</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/控湃暗沙.md" title="wikilink">控湃暗沙</a></p></td>
<td></td>
<td><p>—</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/华夏暗沙.md" title="wikilink">华夏暗沙</a></p></td>
<td><p><br />
（）</p></td>
<td><p>—</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>内部</p></td>
<td><p><a href="../Page/石塘连礁.md" title="wikilink">石塘连礁</a></p></td>
<td></td>
<td><p>—</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/指掌暗沙.md" title="wikilink">指掌暗沙</a></p></td>
<td></td>
<td><p>—</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/南扉暗沙.md" title="wikilink">南扉暗沙</a></p></td>
<td></td>
<td><p>—</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/漫步暗沙.md" title="wikilink">漫步暗沙</a></p></td>
<td></td>
<td><p>—</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/乐西暗沙.md" title="wikilink">乐西暗沙</a></p></td>
<td></td>
<td><p>—</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/屏南暗沙.md" title="wikilink">屏南暗沙</a></p></td>
<td></td>
<td><p>—</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>北部大陆架</p></td>
<td><p><a href="../Page/一统暗沙.md" title="wikilink">一统暗沙</a></p></td>
<td></td>
<td><p>—</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/管事滩.md" title="wikilink">管事滩</a></p></td>
<td></td>
<td><p>—</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/神狐暗沙.md" title="wikilink">神狐暗沙</a></p></td>
<td></td>
<td><p>—</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黄岩海山.md" title="wikilink">黄岩海山</a></p></td>
<td></td>
<td><p>—</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/石星海山.md" title="wikilink">石星海山</a></p></td>
<td></td>
<td><p>—</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/宪北海山.md" title="wikilink">宪北海山</a></p></td>
<td></td>
<td><p>—</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/宪南海山.md" title="wikilink">宪南海山</a></p></td>
<td></td>
<td><p>—</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/涨中海山.md" title="wikilink">涨中海山</a></p></td>
<td></td>
<td><p>—</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/珍贝海山.md" title="wikilink">珍贝海山</a></p></td>
<td></td>
<td><p>—</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>中央海山</p></td>
<td><p><a href="../Page/黄岩岛.md" title="wikilink">黄岩岛</a><br />
（民主礁）</p></td>
<td></td>
<td><p><br />
（）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/宪法暗沙.md" title="wikilink">宪法暗沙</a></p></td>
<td></td>
<td><p>—</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/中南暗沙.md" title="wikilink">中南暗沙</a></p></td>
<td></td>
<td><p>—</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/中南海山.md" title="wikilink">中南海山</a></p></td>
<td></td>
<td><p>—</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/龙南海山.md" title="wikilink">龙南海山</a></p></td>
<td></td>
<td><p>—</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/长龙海山.md" title="wikilink">长龙海山</a></p></td>
<td></td>
<td><p>—</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

</onlyinclude>

## 参考文献

## 外部链接

  - [中沙群島分布](https://www.openstreetmap.org/relation/3959723) —
    在[OpenStreetMap](../Page/OpenStreetMap.md "wikilink")

{{-}}

[Category:南海諸島](../Category/南海諸島.md "wikilink")
[中沙群島](../Category/中沙群島.md "wikilink")
[Category:太平洋群島](../Category/太平洋群島.md "wikilink")
[Category:三沙岛屿](../Category/三沙岛屿.md "wikilink")
[Category:有爭議的島嶼](../Category/有爭議的島嶼.md "wikilink")
[Category:中華民國領海基點](../Category/中華民國領海基點.md "wikilink")
[Category:中華民國爭議地區](../Category/中華民國爭議地區.md "wikilink")
[Category:中華人民共和國爭議地區](../Category/中華人民共和國爭議地區.md "wikilink")
[Category:菲律賓爭議地區](../Category/菲律賓爭議地區.md "wikilink")

1.
2.
3.