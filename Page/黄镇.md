**黄镇**（），[字](../Page/表字.md "wikilink")**白知**（一作**百知**），原名**黄士元**，[安徽省](../Page/安徽省.md "wikilink")[桐城县东乡人](../Page/桐城县.md "wikilink")（现为[枞阳县](../Page/枞阳县.md "wikilink")[横埠镇](../Page/横埠镇.md "wikilink")），中華人民共和國退休外交官、艺术家。

## 生平

黄鎮1925年考入[上海美术专科学校学习绘画](../Page/上海美术专科学校.md "wikilink")，但次年即因参加学生运动而被迫退学，后转入[新华艺术大学](../Page/新华艺术大学.md "wikilink")，毕业后回乡任浮山中学美术教员。1929年，黄因支持学生运动而被解职，次年加入西北军[孙连仲部任中尉参谋](../Page/孙连仲.md "wikilink")。

1931年，随部参加[宁都暴动](../Page/宁都暴动.md "wikilink")，加入[中国工农红军](../Page/中国工农红军.md "wikilink")，次年加入[中国共产党](../Page/中国共产党.md "wikilink")，任[红五军团政治部宣传科科长](../Page/红五军团.md "wikilink")，创作画作《粉碎敌人的围剿》等。1934年，黄镇随部参加[长征](../Page/长征.md "wikilink")，在途中创作了话剧《一只破草鞋》、歌曲《打骑兵歌》和画作《[长征画册](../Page/长征画册.md "wikilink")》等作品。

[抗战爆发后](../Page/抗日战争_\(中国\).md "wikilink")，黄镇任[八路军](../Page/八路军.md "wikilink")[129师政治部副主任](../Page/129师.md "wikilink")\[1\]。1943年5月，[河南北部的国民政府军队在](../Page/河南.md "wikilink")[庞炳勋和](../Page/庞炳勋.md "wikilink")[孙殿英的率领下投敌](../Page/孙殿英.md "wikilink")，黄镇遂立即调任豫北工作委员会书记，负责在新沦陷区域创建中共根据地，不久转任[太行军区副政委兼政治部主任](../Page/太行军区.md "wikilink")，指挥中共军事力量在河南北部的行动。

1948年，黄镇被调往[西柏坡](../Page/西柏坡.md "wikilink")[中共中央](../Page/中共中央.md "wikilink")，任[中国人民解放军总政治部研究室副主任](../Page/中国人民解放军总政治部.md "wikilink")、第一研究室主任等职务，主持制定了[中国人民解放军军旗图案以及一些军事条例](../Page/中国人民解放军军旗.md "wikilink")。

[中华人民共和国成立后](../Page/中华人民共和国.md "wikilink")，黄镇于1950年被任命为[中国驻匈牙利大使](../Page/中国驻匈牙利大使.md "wikilink")。1954年，调任[驻印度尼西亚大使](../Page/中国驻印尼大使列表.md "wikilink")，陪同[周恩来总理出席](../Page/周恩来.md "wikilink")[万隆会议](../Page/万隆会议.md "wikilink")。1961年，黄镇升任[中华人民共和国外交部副部长](../Page/中华人民共和国外交部.md "wikilink")，次年又被任命为[中印边界问题特使](../Page/中印边界问题.md "wikilink")，负责[中印边界战争后的外交善后事宜](../Page/中印边界战争.md "wikilink")。1964年，黄镇出任首任[驻法国大使](../Page/中国驻法国大使列表.md "wikilink")。

1971年起，黄镇奉命秘密同[美国展开外交沟通](../Page/美国.md "wikilink")，为中华人民共和国和美国外交正常化作出了推动作用。1973年，黄镇被任命为首任[中华人民共和国驻美国联络处主任](../Page/中华人民共和国驻美国联络处.md "wikilink")，成为驻[华盛顿的中共方面首席外交代表](../Page/华盛顿哥伦比亚特区.md "wikilink")。1977年，黄镇调任[中共中央宣传部第一副部长兼](../Page/中共中央宣传部.md "wikilink")[中华人民共和国文化部部长](../Page/中华人民共和国文化部.md "wikilink")。

1982年，黄镇退休，任中共中央顾问委员会常委。1989年12月10日，黄镇逝世于[北京](../Page/北京.md "wikilink")。

## 参考

[Category:上海美术专科学校校友](../Category/上海美术专科学校校友.md "wikilink")
[Category:中華人民共和國文化部部長](../Category/中華人民共和國文化部部長.md "wikilink")
[Category:中华人民共和国外交部副部长](../Category/中华人民共和国外交部副部长.md "wikilink")
[Category:中華人民共和國駐法國大使](../Category/中華人民共和國駐法國大使.md "wikilink")
[Category:中華人民共和國駐印度尼西亞大使](../Category/中華人民共和國駐印度尼西亞大使.md "wikilink")
[Category:中華人民共和國駐匈牙利大使](../Category/中華人民共和國駐匈牙利大使.md "wikilink")
[Category:中華民國畫家](../Category/中華民國畫家.md "wikilink")
[Category:中華民國劇作家](../Category/中華民國劇作家.md "wikilink")
[Category:國共戰爭人物](../Category/國共戰爭人物.md "wikilink")
[Category:中國第二次世界大戰人物](../Category/中國第二次世界大戰人物.md "wikilink")
[Category:中国共产党第九届中央委员会委员](../Category/中国共产党第九届中央委员会委员.md "wikilink")
[Category:中国共产党第十届中央委员会委员](../Category/中国共产党第十届中央委员会委员.md "wikilink")
[Category:中国共产党第十一届中央委员会委员](../Category/中国共产党第十一届中央委员会委员.md "wikilink")
[Category:中共中央顾问委员会常务委员](../Category/中共中央顾问委员会常务委员.md "wikilink")
[Category:樅陽人](../Category/樅陽人.md "wikilink")
[Z](../Category/黃姓.md "wikilink")

1.