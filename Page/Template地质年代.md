<table>
<thead>
<tr class="header">
<th><p><span style="font-size:110%;"><a href="../Page/地质年代.md" title="wikilink">地质年代</a></span></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/冥古宙.md" title="wikilink">冥古宙</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/隱生代.md" title="wikilink">隱<br />
生<br />
代</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/成铁纪.md" title="wikilink">成<br />
鐵<br />
紀</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/古新世.md" title="wikilink">古<br />
新<br />
世</a></p></td>
</tr>
</tbody>
</table>

<noinclude>

## 参见

  - [Template:地质年代](../Page/Template:地质年代.md "wikilink")

:\*[Template:Hadean
Footer](../Page/Template:Hadean_Footer.md "wikilink")

:\*[Template:Archean
Footer](../Page/Template:Archean_Footer.md "wikilink")

:\*[Template:Proterozoic
Footer](../Page/Template:Proterozoic_Footer.md "wikilink")

:\*[Template:Phanerozoic
Footer](../Page/Template:Phanerozoic_Footer.md "wikilink")

::\*[Template:Paleozoic
Footer](../Page/Template:Paleozoic_Footer.md "wikilink")

:::\*[Template:Cambrian
Footer](../Page/Template:Cambrian_Footer.md "wikilink")

:::\*[Template:Ordovician
Footer](../Page/Template:Ordovician_Footer.md "wikilink")

:::\*[Template:Silurian
Footer](../Page/Template:Silurian_Footer.md "wikilink")

:::\*[Template:Devonian
Footer](../Page/Template:Devonian_Footer.md "wikilink")

:::\*[Template:Carboniferous
Footer](../Page/Template:Carboniferous_Footer.md "wikilink")

:::\*[Template:Permian
Footer](../Page/Template:Permian_Footer.md "wikilink")

::\*[Template:Mesozoic
Footer](../Page/Template:Mesozoic_Footer.md "wikilink")

:::\*[Template:Triassic
Footer](../Page/Template:Triassic_Footer.md "wikilink")

:::\*[Template:Jurassic
Footer](../Page/Template:Jurassic_Footer.md "wikilink")

:::\*[Template:Cretaceous
Footer](../Page/Template:Cretaceous_Footer.md "wikilink")

::\*[Template:Cenozoic
Footer](../Page/Template:Cenozoic_Footer.md "wikilink")

:::\*[Template:Paleogene
Footer](../Page/Template:Paleogene_Footer.md "wikilink")

:::\*[Template:Neogene
Footer](../Page/Template:Neogene_Footer.md "wikilink")

:::\*[Template:Quaternary
Footer](../Page/Template:Quaternary_Footer.md "wikilink")

## 参考文献

  - <http://www.stratigraphy.org/cheu.pdf>
  - <http://www.stratigraphy.org/codeu.pdf>

</noinclude>

[D](../Category/歷史分期模板.md "wikilink")
[Category:地質資訊框](../Category/地質資訊框.md "wikilink")
[D](../Category/地质模板.md "wikilink") [D](../Category/地史學.md "wikilink")
[Category:地质年代](../Category/地质年代.md "wikilink")