**唐睿宗李旦**（），[唐高宗](../Page/唐高宗.md "wikilink")[李治和](../Page/李治.md "wikilink")[武則天之子](../Page/武則天.md "wikilink")，[唐朝的第五和第八任](../Page/唐朝.md "wikilink")[皇帝](../Page/皇帝.md "wikilink")，曾用名**李旭轮**、**李轮**，他一生兩繼[大統](../Page/大統.md "wikilink")，兩度[禪位](../Page/禪位.md "wikilink")。两次登基，第一次為[天后武氏](../Page/天后.md "wikilink")（登基前的武則天）廢[唐中宗](../Page/唐中宗.md "wikilink")[李顯而繼位](../Page/李顯.md "wikilink")，在位时间是文明元年至載初二年（684年2月27日－690年10月16日），後上表自行退位，禪讓予母親武則天；第二次是在[唐隆之變誅除](../Page/唐隆之變.md "wikilink")[韋皇后及其黨羽後復辟](../Page/韋皇后.md "wikilink")，在位时间是景雲元年至延和元年（710年7月25日-712年9月8日），後退位，[內禪予其子](../Page/內禪.md "wikilink")[李隆基](../Page/李隆基.md "wikilink")（[唐玄宗](../Page/唐玄宗.md "wikilink")）。李旦為[唐高宗](../Page/唐高宗.md "wikilink")[李治諸子之中排行第八](../Page/李治.md "wikilink")，母为[武则天](../Page/武则天.md "wikilink")，[李弘](../Page/李弘_\(唐朝\).md "wikilink")、[李贤](../Page/章懷太子.md "wikilink")、[唐中宗](../Page/唐中宗.md "wikilink")[李顯都是其同母兄长](../Page/李顯.md "wikilink")，[太平公主則為其同母妹妹](../Page/太平公主.md "wikilink")。

## 生平

[龍朔二年](../Page/龍朔.md "wikilink")（662年）六月一日生於[長安蓬萊宮含涼殿](../Page/長安.md "wikilink")\[1\]。当年，封殷王，[遥领冀州](../Page/遥领.md "wikilink")[大都督](../Page/大都督.md "wikilink")、单于[大都护](../Page/大都护.md "wikilink")、右金吾卫[大将军](../Page/大將軍_\(中國\).md "wikilink")。史載“謙恭孝友，好學，工草隸，尤愛文字[訓詁之書](../Page/訓詁.md "wikilink")”。[乾封元年](../Page/乾封.md "wikilink")（666年），徙封豫王。[总章二年](../Page/總章.md "wikilink")（669年），徙封冀王。[上元二年](../Page/上元_\(唐高宗\).md "wikilink")（675年），徙封相王，拜右卫大将军。根据史料显示，仪凤元年（676年），十四岁的李轮即已纳[豆卢氏为](../Page/豆卢贵妃.md "wikilink")[孺人](../Page/孺人.md "wikilink")（[妾室](../Page/妾室.md "wikilink")）。儀鳳三年（678年），迁洛[牧](../Page/牧.md "wikilink")；改名李旦，徙封豫王。在七月的一次宴会上，父亲[唐高宗对叔祖父霍王](../Page/唐高宗.md "wikilink")[李元轨说](../Page/李元軌.md "wikilink")：因为他是最小的儿子，所以最为喜爱\[2\]。679年，王妃[刘氏生下他的长子](../Page/肅明皇后.md "wikilink")[李成器](../Page/李憲_\(寧王\).md "wikilink")。

[嗣聖元年](../Page/嗣聖.md "wikilink")（684年）二月七日，[武则天废其兄](../Page/武则天.md "wikilink")[中宗帝位](../Page/唐中宗.md "wikilink")，立他为帝，改元[文明](../Page/文明_\(唐睿宗\).md "wikilink")。不过，由於是武则天[執政](../Page/執政.md "wikilink")，“政事決于太后”\[3\]，睿宗毫无实权，甚至連干预国家大政的权力都沒有，淪為[傀儡](../Page/傀儡.md "wikilink")。[載初元年](../Page/载初_\(唐朝\).md "wikilink")（690年），武则天废除睿宗後自登[帝位](../Page/帝位.md "wikilink")，改國號[周](../Page/武周.md "wikilink")，睿宗被貶为「皇嗣」（候補性質的皇位繼承人。具儀一比[皇太子](../Page/皇太子.md "wikilink")，卻不給皇太子的名分），改名**武轮**，迁居东宫。

武则天[聖曆元年](../Page/聖曆.md "wikilink")（698年），武則天又改立[中宗為](../Page/唐中宗.md "wikilink")[儲君](../Page/儲君.md "wikilink")。睿宗則從「皇嗣」再被貶為[親王](../Page/親王.md "wikilink")，封號**相王**，他的五個兒子（[李成器](../Page/李成器.md "wikilink")、[李成義](../Page/李成義.md "wikilink")、[李隆基](../Page/李隆基.md "wikilink")、[李隆範](../Page/李隆範.md "wikilink")、[李隆業](../Page/李隆業.md "wikilink")）被封為[郡王](../Page/郡王.md "wikilink")，唐睿宗從此重獲自由，擁有干预国家大政的权力。

[神龍元年](../Page/神龍.md "wikilink")（705年），[宰相](../Page/宰相.md "wikilink")[張柬之等五人發動](../Page/張柬之.md "wikilink")[神龍革命](../Page/神龍革命.md "wikilink")，殺[张易之](../Page/张易之.md "wikilink")、[张昌宗兄弟](../Page/张昌宗.md "wikilink")，逼武則天[退位](../Page/退位.md "wikilink")，迎中宗[復辟](../Page/復辟.md "wikilink")，不久武则天去世。此後中宗封其為**安國相王**，隨即辭去。[景雲元年](../Page/景雲.md "wikilink")（710年），中宗駕崩（傳說是被[韋皇后毒杀](../Page/韋皇后_\(唐中宗\).md "wikilink")），（後在韋皇后矯詔下）由中宗幼子[李重茂登位](../Page/李重茂.md "wikilink")，[改元](../Page/改元.md "wikilink")[唐隆](../Page/唐隆.md "wikilink")，是為少帝。

睿宗的三子[李隆基與](../Page/唐玄宗.md "wikilink")[太平公主等聯絡](../Page/太平公主.md "wikilink")[禁軍](../Page/禁軍.md "wikilink")[將領](../Page/將領.md "wikilink")，擁兵入宮，將韦-{后}-誅殺，迫少帝[李重茂遜位](../Page/李重茂.md "wikilink")，史曰[唐隆之變](../Page/唐隆之變.md "wikilink")。六月二十四日睿宗[復辟於承天門樓](../Page/復辟.md "wikilink")，[大赦](../Page/大赦.md "wikilink")[天下](../Page/天下.md "wikilink")，与其子李隆基一起铲除了[韋皇后一黨的势力](../Page/韋皇后_\(唐中宗\).md "wikilink")。

[延和元年](../Page/延和.md "wikilink")（712年）七月二十五日，唐睿宗無法面對[李隆基與](../Page/唐玄宗.md "wikilink")[太平公主的爭端](../Page/太平公主.md "wikilink")，於是[禪讓](../Page/禪讓.md "wikilink")[帝位於李隆基](../Page/皇帝.md "wikilink")，是為唐玄宗，自称「[太上皇](../Page/太上皇.md "wikilink")」，每五天在太極殿接受群臣的朝賀，仍自称“朕”，三品已上除授及大刑狱仍然自决，命令称诰、令，而玄宗每日受朝于武德殿，自称“予”，决定三品已下除授及徒罪，命令称制、敕。后来玄宗发动[先天政变消灭太平公主一党](../Page/先天政变.md "wikilink")，睿宗才不得不彻底放权。

[開元四年六月二十日](../Page/開元.md "wikilink")（716年7月13日）李旦病逝，享年五十五歲。

## 安国相王

唐中宗于神龙元年正月复位后，李旦加号称安国相王。下列安国相王府官员。

  - 开府仪同三司左千牛卫大将军上柱国安国相王旦
  - 特进中书令兼检校安国相王府长史上柱国南阳郡王[袁恕己](../Page/袁恕己.md "wikilink")（神龙元年正月—五月）
  - 特进行尚书左仆射兼检校安国相王府长史平章军国重事上柱国芮国公[豆卢钦望](../Page/豆卢钦望.md "wikilink")（神龙元年五月—
  - 大中大夫行安国相王府司马护军[皇甫忠](../Page/皇甫忠.md "wikilink")
  - 朝散大夫守安国相王府谘议上柱国邢国公[王温](../Page/王温.md "wikilink")
  - 朝议大夫行安国相王府记室参军事[丘悦](../Page/丘悦.md "wikilink")
  - 朝议郎行安国相王府文学[韦利器](../Page/韦利器.md "wikilink")
  - 行安国相王府文学[高仲舒](../Page/高仲舒.md "wikilink")
  - 朝议大夫行安国相王府仓曹参军[辛道瑜](../Page/辛道瑜.md "wikilink")
  - 行安国相王府功曹参军[窦希瑊](../Page/窦希瑊.md "wikilink")
  - 行安国相王府属[韦慎名](../Page/韦慎名.md "wikilink")
  - 行安国相王府掾[丘知几](../Page/丘知几.md "wikilink")
  - 行安国相王府典军[丘琬](../Page/丘琬.md "wikilink")
  - 行安国相王府典军[卫日新](../Page/卫日新.md "wikilink")
  - 行安国相王府典签[裴耀卿](../Page/裴耀卿.md "wikilink")
  - 安国相王品官行内侍省奚官局令[引叁目](../Page/引叁目.md "wikilink")
  - 安国相王品官行内侍省掖庭局令[戴思恭](../Page/戴思恭.md "wikilink")

## 家庭

### 家世

<div style="clear: both; width: 100%; padding: 0; text-align: left; border: none;" class="NavFrame">

<div style="background: #ccddcc; text-align: center; border: 1px solid #667766" class="NavHead">

**唐睿宗李旦的祖先**

</div>

<div class="NavContent" style="display:none;">

<center>

</center>

</div>

</div>

### 后妃

#### 皇后

  - [刘皇后](../Page/刘皇后_\(唐睿宗\).md "wikilink")，结发之妻，正室；生太子[李憲](../Page/李成器.md "wikilink")、寿昌公主、代国公主李华婉，被武则天勒令处死，追谥为肃明順聖皇后
  - [窦德妃](../Page/昭成皇后.md "wikilink")，侧室；生唐玄宗李隆基、金仙公主、玉真公主，被武则天勒令处死，追谥为昭成顺圣皇后

#### 妃嫔

  - [豆卢贵妃](../Page/豆卢贵妃_\(唐睿宗\).md "wikilink")
  - [崔贵妃](../Page/崔贵妃_\(唐睿宗\).md "wikilink")，生鄎国公主
  - [王德妃](../Page/王德妃_\(唐睿宗\).md "wikilink")，惠宣太子李業母
  - [王賢妃](../Page/王芳媚.md "wikilink")，名芳媚，王德妃之妹，李業養母

#### 庶妃

  - [崔孺人](../Page/崔孺人.md "wikilink")，生惠文太子[李范](../Page/李范.md "wikilink")
  - [唐孺人](../Page/唐孺人.md "wikilink")（672年—692年12月15日），[唐俭孙女](../Page/唐俭.md "wikilink")，考古发现唐孺人与崔孺人墓
  - 崔孺人。因崔孺人墓志铭缺失，不知崔孺人与李范母是否同一人\[4\]

#### 姬侍

  - [柳宫人](../Page/柳宫人.md "wikilink")，前宰相[柳奭孙女](../Page/柳奭.md "wikilink")，生惠庄太子[李撝](../Page/李撝.md "wikilink")

### 子

1.  讓皇帝[李憲](../Page/李宪_\(唐朝宗室\).md "wikilink")，母刘皇后
2.  惠莊太子[李撝](../Page/李撝.md "wikilink")，母宮人柳氏
3.  玄宗[李隆基](../Page/李隆基.md "wikilink")，母窦德妃
4.  惠文太子[李範](../Page/李范_\(唐朝\).md "wikilink")，母崔孺人
5.  惠宣太子[李業](../Page/李業.md "wikilink")，母王德妃
6.  隋王[李隆悌](../Page/李隆悌.md "wikilink")

### 女

《新唐书·诸帝公主列传》，记载唐睿宗共有11位女儿\[5\]。具体的排行，出自对个人记载：

1.  长女，[壽昌公主](../Page/寿昌公主_\(唐睿宗\).md "wikilink")（母[刘皇后](../Page/肅明皇后.md "wikilink")，下嫁[崔真](../Page/崔真.md "wikilink")）
2.  次女，[安興昭懷公主](../Page/安興昭懷公主.md "wikilink")（早薨）
3.  某女，[荊山公主](../Page/荊山公主.md "wikilink")（下嫁[薛伯阳](../Page/薛伯阳.md "wikilink")，存疑，可能是凉国公主、鄎国公主两个公主的混记）
4.  三女，[淮陽公主](../Page/李花山.md "wikilink")（名花山，母王德妃，下嫁[王承庆](../Page/王承庆.md "wikilink")）
5.  四女，[代國公主](../Page/李华_\(公主\).md "wikilink")（名华，字華婉（墓誌銘記為花婉），母刘皇后，下嫁[郑万钧](../Page/郑万钧.md "wikilink")）
6.  五女，[涼國公主](../Page/涼國公主.md "wikilink")（字華莊，母王德妃，先封为仙源公主，下嫁[薛伯阳](../Page/薛伯阳.md "wikilink")）
7.  某女，[薛國公主](../Page/薛國公主.md "wikilink")（先封为清阳公主，下嫁[王守一](../Page/王守一.md "wikilink")，又嫁[裴巽](../Page/裴巽.md "wikilink")）
8.  七女，[鄎國公主](../Page/鄎國公主.md "wikilink")（母[崔贵妃](../Page/崔贵妃_\(唐睿宗\).md "wikilink")，先封为荆山公主，下嫁[薛儆](../Page/薛儆.md "wikilink")，又嫁[郑孝义](../Page/郑孝义.md "wikilink")）
9.  八女，[金仙公主](../Page/金仙公主.md "wikilink")（母窦德妃，先封为西城县主）
10. 九女，[玉真公主](../Page/玉真公主.md "wikilink")（字玄玄，号持盈，母窦德妃，始封崇昌县主）
11. 某女，[霍國公主](../Page/霍國公主.md "wikilink")（下嫁[裴虚己](../Page/裴虚己.md "wikilink")）

## 注释

1.  从684年－690年唐睿宗仍然在位期间，还有[光宅](../Page/光宅.md "wikilink")、[垂拱](../Page/垂拱.md "wikilink")、[永昌](../Page/永昌_\(唐朝\).md "wikilink")、[载初几个年号](../Page/载初_\(唐朝\).md "wikilink")。虽然有文献\[6\]把这几个年号算作唐睿宗的，但是这段期间，武则天实际操纵朝政，睿宗毫无实权。因此大多文献把这几个年号算在武则天名下\[7\]\[8\]。

## 参考文献

{{-}}       |-   |-    |-

[Category:唐朝皇帝](../Category/唐朝皇帝.md "wikilink")
[8](../Category/唐高宗皇子.md "wikilink")
[L](../Category/662年出生.md "wikilink")
[L](../Category/716年逝世.md "wikilink")
[L李](../Category/唐朝三公.md "wikilink")
[Category:中国太上皇](../Category/中国太上皇.md "wikilink")
[唐](../Category/入祀历代帝王庙景德崇圣殿.md "wikilink")
[Category:李姓](../Category/李姓.md "wikilink")

1.  《酉陽雜俎》：「睿宗初生含涼殿，則天乃於殿內造佛氏。」
2.  《[旧唐书](../Page/旧唐书.md "wikilink")·本纪第五·高宗下》（仪凤）三年......秋七月丁巳，宴近臣诸亲于咸亨殿。上谓霍王元轨曰：“......又男轮最小，特所留爱，......”
3.  《資治通鑒》卷二○三后光宅元年
4.  [洛阳发现两座唐代壁画墓](http://tech.sina.com.cn/d/2005-05-28/1312620603.shtml)
5.  [《新唐书·诸帝公主列传》](http://guoxue.baidu.com/page/d0c2ccc6cae9/95.html)
6.  陈光，中国历代帝王年号手册，北京燕山出版社 第225-226页 ISBN 7540210311
7.  徐红岚，中日朝三国历史纪年表，辽宁教育出版社，1998年 第150页 ISBN 7538276193
8.  李崇智，中国历代年号考，中华书局，2004年 第100页 ISBN 7101025129