**唐麗球**（，），綽號“波波”，1984年參與[香港小姐競選奪得季軍](../Page/香港小姐競選.md "wikilink")。85年開始與[杜德偉交往兩年](../Page/杜德偉.md "wikilink")，最後分手收場。之後與藝人[吳毅將拍拖十年](../Page/吳毅將.md "wikilink")，於2003年5月25日註冊結婚，兩人婚後育有一女。\[1\]

她的[姨甥女](../Page/姨甥女.md "wikilink")[嘉碧儀也是](../Page/嘉碧儀.md "wikilink")[香港小姐出身](../Page/香港小姐.md "wikilink")。

## 演出作品

### 電視劇（[無綫電視](../Page/無綫電視.md "wikilink")）

|                                             |                                      |        |
| ------------------------------------------- | ------------------------------------ | ------ |
| **年份**                                      | **劇名**                               | **角色** |
| 1985年                                       | [中四丁班](../Page/中四丁班.md "wikilink")   | 中四女學生  |
| [楊家將](../Page/楊家將_\(無綫電視劇\).md "wikilink")  | 碧蓮公主                                 |        |
| 1986年                                       | [神勇CID](../Page/神勇CID.md "wikilink") | 馬思敏    |
| [小島風雲](../Page/小島風雲.md "wikilink")          | 柳小眉                                  |        |
| [城市故事](../Page/城市故事.md "wikilink")          | 琪琪（Suzanne）                          |        |
| [哥哥的女友](../Page/哥哥的女友.md "wikilink")（台慶單元劇） | 鍾子欣                                  |        |

### 電視劇（[亞洲電視](../Page/亞洲電視.md "wikilink")）

|                                      |                                          |        |
| ------------------------------------ | ---------------------------------------- | ------ |
| **年份**                               | **劇名**                                   | **角色** |
| 1989年                                | [生娘不及養娘大](../Page/生娘不及養娘大.md "wikilink") |        |
| [望父成龍](../Page/望父成龍.md "wikilink")   |                                          |        |
| 1990年                                | [鴻運迫人來](../Page/鴻運迫人來.md "wikilink")     |        |
| [發達智多星](../Page/發達智多星.md "wikilink") | 王安妮                                      |        |
| 1991年                                | [八婆會館](../Page/八婆會館.md "wikilink")       | 賈玉芬    |
| 1994年                                | [岳飛傳](../Page/岳飛傳.md "wikilink")         | 完顏彩雁   |

### 電影（1980年代）

|                                          |                                      |                                                                                                                                                                      |                                                                 |                                    |
| ---------------------------------------- | ------------------------------------ | -------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------- | ---------------------------------- |
| **年 份**                                  | **片 名**                              | **角 色**                                                                                                                                                              | **合 作**                                                         | **備註**                             |
| 1988年                                    | [標錯參](../Page/標錯參.md "wikilink")     | 歪妹                                                                                                                                                                   |                                                                 | 客串，台：綁錯票                           |
| [吉屋藏嬌](../Page/吉屋藏嬌.md "wikilink")       | 丫鬟阿芝                                 | [利智](../Page/利智.md "wikilink")、[爾冬陞](../Page/爾冬陞.md "wikilink")、[鄭裕玲](../Page/鄭裕玲.md "wikilink")、[鄭丹瑞](../Page/鄭丹瑞.md "wikilink")                                    |                                                                 |                                    |
| [最佳損友](../Page/最佳損友.md "wikilink")       | Happy                                | [邱淑貞](../Page/邱淑貞.md "wikilink")、[劉德華](../Page/劉德華.md "wikilink")、[馮淬帆](../Page/馮淬帆.md "wikilink")、[陳百祥](../Page/陳百祥.md "wikilink")、[曹查理](../Page/曹查理.md "wikilink") |                                                                 |                                    |
| [最佳損友闖情關](../Page/最佳損友闖情關.md "wikilink") | 客串                                   |                                                                                                                                                                      |                                                                 |                                    |
| [褲甲天下](../Page/褲甲天下.md "wikilink")       | 阿富的妹妹                                | 爾冬陞、沈殿霞、吳耀漢、張艾嘉、金燕玲、盧冠廷、陸劍明、瑪利亞、吳君如                                                                                                                                  | 客串 |-----、                                                      | [愛的逃兵](../Page/愛的逃兵.md "wikilink") |
| [江湖接班人](../Page/江湖接班人.md "wikilink")     | 吳天真                                  |                                                                                                                                                                      |                                                                 |                                    |
| 1989年                                    | [福星闖江湖](../Page/福星闖江湖.md "wikilink") | Mina                                                                                                                                                                 | [盧海鵬](../Page/盧海鵬.md "wikilink")、[王青](../Page/王青.md "wikilink") |                                    |
| [壯志雄心](../Page/壯志雄心.md "wikilink")       | 唐淑儀                                  |                                                                                                                                                                      |                                                                 |                                    |
| [富貴兵團](../Page/富貴兵團.md "wikilink")       |                                      |                                                                                                                                                                      |                                                                 |                                    |
|                                          |                                      |                                                                                                                                                                      |                                                                 |                                    |

### 電影（1990年代）

|                                                |                                        |                                                                                                    |                                                                                                                                     |        |
| ---------------------------------------------- | -------------------------------------- | -------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------- | ------ |
| **年 份**                                        | **片 名**                                | **角 色**                                                                                            | **合 作**                                                                                                                             | **備註** |
| 1990年                                          | [脂粉雙雄](../Page/脂粉雙雄.md "wikilink")     |                                                                                                    | [洪金寶](../Page/洪金寶.md "wikilink")、[譚詠麟](../Page/譚詠麟.md "wikilink")                                                                   |        |
| [安樂戰場](../Page/安樂戰場.md "wikilink")             | 林秀麗                                    | [曾志偉](../Page/曾志偉.md "wikilink")、[溫碧霞](../Page/溫碧霞.md "wikilink")                                  |                                                                                                                                     |        |
| [捉鬼合家歡2麻衣傳奇](../Page/捉鬼合家歡2麻衣傳奇.md "wikilink") | 芙蓉                                     | [鄭則仕](../Page/鄭則仕.md "wikilink")、[何家勁](../Page/何家勁.md "wikilink")、[張國強](../Page/張國強.md "wikilink") | 台：五鬼運財2人鬼戀                                                                                                                          |        |
| 1991年                                          | [五虎將之決裂](../Page/五虎將之決裂.md "wikilink") |                                                                                                    | [劉德華](../Page/劉德華.md "wikilink")、[梁朝偉](../Page/梁朝偉.md "wikilink")、[湯鎮業](../Page/湯鎮業.md "wikilink")                                  |        |
| [莎莎嘉嘉站起來](../Page/莎莎嘉嘉站起來.md "wikilink")       |                                        | [張艾嘉](../Page/張艾嘉.md "wikilink")、[葉倩文](../Page/葉倩文.md "wikilink")                                  |                                                                                                                                     |        |
| 1992年                                          | [越軌迷情](../Page/越軌迷情.md "wikilink")     |                                                                                                    | [尹揚明](../Page/尹揚明.md "wikilink")                                                                                                    |        |
| 1993年                                          | [情陷珠江橋](../Page/情陷珠江橋.md "wikilink")   |                                                                                                    | [吳啟華](../Page/吳啟華.md "wikilink")                                                                                                    |        |
| [臥底風雲](../Page/臥底風雲.md "wikilink")             |                                        | [林俊賢](../Page/林俊賢.md "wikilink")、[潘宏彬](../Page/潘宏彬.md "wikilink")                                  |                                                                                                                                     |        |
| [梟雄未路](../Page/梟雄未路.md "wikilink")             |                                        | [湯鎮業](../Page/湯鎮業.md "wikilink")、[潘宏彬](../Page/潘宏彬.md "wikilink")、[黃錦燊](../Page/黃錦燊.md "wikilink") |                                                                                                                                     |        |
| [魔鬼情人](../Page/魔鬼情人.md "wikilink")             |                                        | [吳啟華](../Page/吳啟華.md "wikilink")、[陳曉瑩](../Page/陳曉瑩.md "wikilink")                                  |                                                                                                                                     |        |
| [夢斷危情](../Page/夢斷危情.md "wikilink")             |                                        | [林俊賢](../Page/林俊賢.md "wikilink")、[關詠荷](../Page/關詠荷.md "wikilink")                                  |                                                                                                                                     |        |
| 1994年                                          | [新邊緣人](../Page/新邊緣人.md "wikilink")     |                                                                                                    | [張學友](../Page/張學友.md "wikilink")、[梁家輝](../Page/梁家輝.md "wikilink")                                                                   |        |
| 1995年                                          | [狂情殺手](../Page/狂情殺手.md "wikilink")     |                                                                                                    | [劉永](../Page/劉永.md "wikilink")、[任達華](../Page/任達華.md "wikilink")                                                                     |        |
| 1997年                                          | [夜半2點鐘](../Page/夜半2點鐘.md "wikilink")   |                                                                                                    | [陳小春](../Page/陳小春.md "wikilink")、[周文健](../Page/周文健.md "wikilink")                                                                   |        |
| 1998年                                          | [勇探](../Page/勇探.md "wikilink")         |                                                                                                    | [楊玉梅](../Page/楊玉梅.md "wikilink")、[徐少強](../Page/徐少強.md "wikilink")                                                                   |        |
| 1999年                                          | [陽光警察](../Page/陽光警察.md "wikilink")     | 高級女督察                                                                                              | [馮德倫](../Page/馮德倫.md "wikilink")、[李心潔](../Page/李心潔.md "wikilink")                                                                   |        |
| 2001年                                          | [女人那話兒](../Page/女人那話兒.md "wikilink")   |                                                                                                    | [黃真真](../Page/黃真真.md "wikilink")、[許鞍華](../Page/許鞍華.md "wikilink")、[邵音音](../Page/邵音音.md "wikilink")、[周美鳳](../Page/周美鳳.md "wikilink") |        |

### MV

  - [譚詠麟](../Page/譚詠麟.md "wikilink")：《酒紅色的心》《卡拉永遠OK（國語）》

### 節目嘉賓

  - 2018年：[奇妙電視](../Page/奇妙電視.md "wikilink")《喜出望外》

## 參考資料

[Category:香港電影演員](../Category/香港電影演員.md "wikilink")
[Category:前無綫電視女藝員](../Category/前無綫電視女藝員.md "wikilink")
[Category:前亞洲電視藝員](../Category/前亞洲電視藝員.md "wikilink")
[Category:1984年度香港小姐競選參賽者](../Category/1984年度香港小姐競選參賽者.md "wikilink")
[Lai](../Category/唐姓.md "wikilink")

1.  [長情請看：舊女友多覓好歸宿](https://hk.entertainment.appledaily.com/entertainment/daily/article/20121028/18050481)