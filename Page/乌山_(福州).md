**乌山**（[闽东语](../Page/闽东语.md "wikilink")[平话字](../Page/平话字.md "wikilink")：），[福州三山之一](../Page/福州.md "wikilink")，又名乌石山、道山，海拔84米，高度为三山之首。相传[汉](../Page/汉.md "wikilink")[何氏九仙](../Page/何氏九仙.md "wikilink")[重阳登高射](../Page/重陽節.md "wikilink")[乌鸦于此](../Page/乌鸦.md "wikilink")，故得此山名。

自[唐代起](../Page/唐代.md "wikilink")，乌山即为游览胜地，以山石幽奇受到人们喜爱。[道山亭](../Page/道山亭.md "wikilink")、黎公亭一带是全山风景最佳处，同时也是摩崖石刻最集中的地方。这里岩石嵯峨，挺拔竞秀；古榕参天，流泉淙淙，既有险象环生的天生桥——雀舌桥，也有下临绝壁，视野开阔的天然大石坪——[天章台](../Page/天章台.md "wikilink")，可谓移步换景，步步幽奇。

乌山不仅风景秀丽，而且摩崖石刻遍布全山。其中最著名的有唐代大书法家[李阳冰的](../Page/李阳冰.md "wikilink")[篆书](../Page/篆书.md "wikilink")“般若台”，它是福州现存最古老也是最珍贵的[石刻](../Page/石刻.md "wikilink")，被誉为“人间至宝”。此外，还有[李纲](../Page/李纲.md "wikilink")、[朱熹](../Page/朱熹.md "wikilink")、[程师孟等历代名人的题刻](../Page/程师孟.md "wikilink")。

## 著名景点

  - 乌山摩岩石刻——数目众多，以唐书法家李阳冰的《[般若台记](../Page/般若台记.md "wikilink")》堪称一绝。

<!-- end list -->

  - [乌塔](../Page/乌塔.md "wikilink")

<!-- end list -->

  - [道山亭](../Page/道山亭.md "wikilink")

[唐宋八大家之一](../Page/唐宋八大家.md "wikilink")[曾巩所作](../Page/曾巩.md "wikilink")《道山亭记》

  - 凌霄台

凌霄台曾是旧时[福州人重阳登高](../Page/福州人.md "wikilink")、放风筝的地方，在台上可北观[屏山](../Page/屏山.md "wikilink")[镇海楼](../Page/镇海楼.md "wikilink")，东望于山九日台，南见[烟台山](../Page/烟台山.md "wikilink")，周围有天秀岩、双峰梦、薛老峰等，登高远眺，景色极佳。[宋代书法家](../Page/宋代.md "wikilink")[蔡襄曾写下](../Page/蔡襄.md "wikilink")《登凌霄台诗》，其中有句“缔结青云上，登临沧海滨”，流传甚广。
福州市政府还计划重建当年陈宝琛所建的“[海天阁](../Page/海天阁.md "wikilink")”，与[屏山](../Page/屏山.md "wikilink")[镇海楼遥遥相望](../Page/镇海楼.md "wikilink")\[1\]。

## 其他景点

  - 天章台、霹雳岩、天香台、天台桥

<!-- end list -->

  - 石林园、天秀岩、清冷台

<!-- end list -->

  - 沈公祠、道山观、弥陀寺、吕祖宫

## 参见

  - [三山两塔](../Page/三山两塔.md "wikilink")

[Category:福州地理](../Category/福州地理.md "wikilink")
[W](../Category/福建山峰.md "wikilink")

1.