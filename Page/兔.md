**兔**，又称**兔子**，在[汉语中是](../Page/汉语.md "wikilink")[哺乳类](../Page/哺乳纲.md "wikilink")[兔形目](../Page/兔形目.md "wikilink")**兔科**（[学名](../Page/学名.md "wikilink")：）物种的总称。

## 習性

[Großes_Hauskaninchen_beim_Dösen.JPG](https://zh.wikipedia.org/wiki/File:Großes_Hauskaninchen_beim_Dösen.JPG "fig:Großes_Hauskaninchen_beim_Dösen.JPG")
[Jack_Rabbit_(14398629508).jpg](https://zh.wikipedia.org/wiki/File:Jack_Rabbit_\(14398629508\).jpg "fig:Jack_Rabbit_(14398629508).jpg")
兔子沒有[汗腺](../Page/汗腺.md "wikilink")，所以不會[流汗](../Page/流汗.md "wikilink")，[耳朵可以](../Page/耳朵.md "wikilink")[散熱](../Page/散熱.md "wikilink")。兔子的[排尿機制屬濃縮性](../Page/排尿.md "wikilink")，對[水分的需求比其他動物來的少](../Page/水分.md "wikilink")，有些野兔只要靠[青草上的](../Page/青草.md "wikilink")[露水即可](../Page/露水.md "wikilink")，或是從[蔬菜攝的水分](../Page/蔬菜.md "wikilink")，故很少看到兔子到河邊喝水。兔子的腸胃道內有相當多的各式菌種以維持體內腸胃消化的平衡，野外的兔子若喝過量的生水有可能因其內部細菌破壞腸胃內的菌種平衡導致身體不適，甚至引起死亡。但认为兔子不喝水则是误解。因食用乾式飼料與牧草的關係，需要給予豢養的[寵物兔足量乾淨的飲水](../Page/寵物.md "wikilink")，以維持身體的必要運作功能。

兔子有时会进食自己的粪便，但其自身特殊的消化系统可以帮助它们再次消化这些粪便，通常来说进食粪便对于它们自身是无害的。

兔子在自然界能生存的原因來自於逃跑時能快速轉彎變換逃生方向，兔在全世界除了[大洋洲以外都是原生的](../Page/大洋洲.md "wikilink")[動物](../Page/動物.md "wikilink")。澳洲因為以前兔子入侵，現在成了一大物種災難。

有人认為兔子只喜歡吃[胡蘿蔔](../Page/胡蘿蔔.md "wikilink")、[高麗菜](../Page/高麗菜.md "wikilink")，所以只需給牠這些[食物](../Page/食物.md "wikilink")；還有兔子不喝水，否則會造成[肚瀉](../Page/肚瀉.md "wikilink")；這些都是嚴重的錯誤觀念。寵物兔的正常健康食物應是無限量牧草、自身的大便、適量乾糧和潔淨的飲用水。

現在飼養的兔子大部分出生時體內带有球蟲菌，球蟲菌可令幼兔[致命](../Page/致命.md "wikilink")，对成年兔則無害。所以在幼兔成年前需要餵食球蟲藥。可将球蟲藥0.5／ml滴入幼兔飲用水中，讓幼兔随意飲用，幼兔成年前每个月一次，一次療程为七天。

另外，兔子的耳朵有很多微血管與感知神經，捕捉時不可抓取耳部，長期可能會使其部位受創，應該要以手持腹部或臀部為主要施力點。

兔子生下小寶寶後有時會拔下自己的毛來給寶寶做窩。

## 分类

  - [琉球兔屬](../Page/琉球兔屬.md "wikilink") *Pentalagus*
      - [琉球兔](../Page/琉球兔.md "wikilink")（奄美短耳兔） *Pentalagus furnessi*
  - [山兔屬](../Page/山兔屬.md "wikilink") *Bunolagus*
      - [南非山兔](../Page/南非山兔.md "wikilink")（河兔） *Bunolagus monticularis*
  - [蘇門兔屬](../Page/蘇門兔屬.md "wikilink") *Nesolagus*
      - [蘇門答臘短耳兔](../Page/蘇門答臘短耳兔.md "wikilink") *Nesolagus netscheri*
  - [火山兔屬](../Page/火山兔屬.md "wikilink") *Romerolagus*
      - [火山兔](../Page/火山兔.md "wikilink")（墨西哥兔） *Romerolagus diazi*
  - [侏兔屬](../Page/侏兔屬.md "wikilink") *Brachylagus*
      - [侏兔](../Page/侏兔.md "wikilink") *Brachylagus idahoensis*
  - *Sylvilagus*
      - [森林兔](../Page/森林兔.md "wikilink") *Sylvilagus brasiliensis*

      - *Sylvilagus dicei*

      - *Sylvilagus bachmani*

      - [馴棉尾兔](../Page/馴棉尾兔.md "wikilink") *Sylvilagus mansuetus*

      - [水兔](../Page/水兔.md "wikilink")（沼澤兔） *Sylvilagus aquaticus*

      - [澤兔](../Page/澤兔.md "wikilink") *Sylvilagus palustris*

      - [東部棉尾兔](../Page/東部棉尾兔.md "wikilink")（佛羅里達棉尾兔） *Sylvilagus
        floridanus*

      - [新英格蘭棉尾兔](../Page/新英格蘭棉尾兔.md "wikilink") *Sylvilagus
        transitionalis*

      - [山棉尾兔](../Page/山棉尾兔.md "wikilink") *Sylvilagus nuttallii*

      - [沙漠棉尾兔](../Page/沙漠棉尾兔.md "wikilink") *Sylvilagus audubonii*

      - [中墨林兔](../Page/中墨林兔.md "wikilink") *Sylvilagus insonus*

      - [南墨林兔](../Page/南墨林兔.md "wikilink")（墨西哥棉尾兔） *Sylvilagus
        cunicularis*

      - [島林兔](../Page/島林兔.md "wikilink")（格雷森棉尾兔） *Sylvilagus graysoni*
  - [穴兔屬](../Page/穴兔屬.md "wikilink") *Oryctolagus*
      - [穴兔](../Page/穴兔.md "wikilink")（[家兔](../Page/家兔.md "wikilink")）
        *Oryctolagus cuniculus*
  - [藪岩兔屬](../Page/藪岩兔屬.md "wikilink") *Poelagus*
      - [藪岩兔](../Page/藪岩兔.md "wikilink") *Poelagus marjorita*
  - [岩兔屬](../Page/岩兔屬.md "wikilink") *Pronolagus*
      - [厚尾岩兔](../Page/厚尾岩兔.md "wikilink") *Pronolagus crassicaudatus*
      - [蘭德岩兔](../Page/蘭德岩兔.md "wikilink") *Pronolagus randensis*
      - [史氏岩兔](../Page/史氏岩兔.md "wikilink") *Pronolagus rupestris*
  - [粗毛兔屬](../Page/粗毛兔屬.md "wikilink") *Caprolagus*
      - [粗毛兔](../Page/粗毛兔.md "wikilink")（阿薩密兔） *Caprolagus hispidus*
  - [兔屬](../Page/兔屬.md "wikilink") *Lepus*
      - [北極兔](../Page/北極兔.md "wikilink") *Lepus arcticus*
      - [藪兔](../Page/藪兔.md "wikilink") *Lepus saxatilis*
      - [草兔](../Page/草兔.md "wikilink") *Lepus capensis*
      - [衣索比亞野兔](../Page/衣索比亞野兔.md "wikilink") *Lepus habessinicus*
      - *Lepus starcki*
      - [歐洲野兔](../Page/歐洲野兔.md "wikilink") *Lepus europaeus*
      - [緬甸野兔](../Page/緬甸野兔.md "wikilink") *Lepus peguensis*
      - [印度野兔](../Page/印度野兔.md "wikilink")（黑頸兔） *Lepus nigricollis*
      - [高原兔](../Page/高原兔.md "wikilink")（灰尾兔） *Lepus oiostolus*
      - [雲南兔](../Page/雲南兔.md "wikilink") *Lepus comus*
      - [雪兔](../Page/雪兔.md "wikilink") *Lepus timidus*
      - [白靴兔](../Page/白靴兔.md "wikilink") *Lepus americanus*
      - *Lepus othus*
      - [草原兔](../Page/草原兔.md "wikilink")（湯森氏兔） *Lepus townsendii*
      - [加利福尼亞兔](../Page/加利福尼亞兔.md "wikilink") *Lepus californicus*
      - *Lepus insularis*
      - *Lepus callotis*
      - [黃喉兔](../Page/黃喉兔.md "wikilink") *Lepus flavigularis*
      - [羚羊兔](../Page/羚羊兔.md "wikilink") *Lepus alleni*
      - [中國野兔](../Page/中國野兔.md "wikilink") *Lepus sinensis*
      - [日本兔](../Page/日本兔.md "wikilink") *Lepus brachyurus*
      - [東北兔](../Page/東北兔.md "wikilink")（滿洲兔） *Lepus mandschuricus*
      - [塔里木兔](../Page/塔里木兔.md "wikilink")（莎車兔、南疆兔） *Lepus yarkandensis*
      - *Lepus castrovieoi*
      - [韓國野兔](../Page/韓國野兔.md "wikilink") *Lepus coreanus*
      - [科西嘉島野兔](../Page/科西嘉島野兔.md "wikilink") *Lepus corsicanus*
      - [大草原野兔](../Page/大草原野兔.md "wikilink") *Lepus crawshayi*
      - *Lepus fagani*
      - [格拉納達野兔](../Page/格拉納達野兔.md "wikilink") *Lepus granatensis*
      - [海南兔](../Page/海南兔.md "wikilink") *Lepus hainanus*
      - [蒙古兔](../Page/蒙古兔.md "wikilink")（托氏兔） *Lepus tolai*
      - [非洲大草原野兔](../Page/非洲大草原野兔.md "wikilink") *Lepus victoriae*
      - [馬拉威野兔](../Page/馬拉威野兔.md "wikilink") *Lepus whytei*
      - [台灣野兔](../Page/台灣野兔.md "wikilink") *Lepus sinensis formosus*

## 文化

[KCityParkRabbit.JPG](https://zh.wikipedia.org/wiki/File:KCityParkRabbit.JPG "fig:KCityParkRabbit.JPG")十二生肖像之兔\]\]

### 東方文化

  - 兔是[中国的](../Page/中国.md "wikilink")[十二生肖之一](../Page/十二生肖.md "wikilink")，排名第四，对应[地支中的](../Page/地支.md "wikilink")[卯](../Page/卯.md "wikilink")。
  - 中国现代动画/漫画作品《那年那兔那些事》以“兔子”形象代表中国人，在现实中，也会有人用“兔子”指代“中国人”或“中华人民共和国”。
  - [月兔的记载](../Page/月兔.md "wikilink")，首见于[屈原的](../Page/屈原.md "wikilink")《[天问](../Page/天问.md "wikilink")》：“夜光何德，死而又育？厥利维何，而顾菟在腹。”[中國神話中的](../Page/中國神話.md "wikilink")[月兔在](../Page/月兔.md "wikilink")[月球上搗藥](../Page/月球.md "wikilink")，在[嫦娥奔月后](../Page/嫦娥.md "wikilink")，一直在月球上陪伴她，甚至成爲傳説中消除瘟疫的[兔兒爺](../Page/兔兒爺.md "wikilink")。[日本神話中的月兔則是在上面製造](../Page/日本神話.md "wikilink")[麻糬](../Page/麻糬.md "wikilink")。朝鮮童謠《[小白船](../Page/小白船.md "wikilink")》（原名《半月》，[諺文](../Page/諺文.md "wikilink")：）也提到月中有兔。
  - [兔儿神是中国古代传说掌管男同性恋爱情的神祇](../Page/兔儿神.md "wikilink")。

### 西方文化

  - 在美洲大陸，有著認為兔子的腳能帶來幸運，請參閱：[幸運兔腳](../Page/幸運兔腳.md "wikilink")。在沙盒游戏[我的世界](../Page/我的世界.md "wikilink")（Minecraft）中，兔子脚是一种与酿造有关的道具。
  - 西方以兔子作為復活節的象徵之一：[復活兔](../Page/復活兔.md "wikilink")。
  - [花花公子是美国一份以兔子商標的男性成人雜誌](../Page/花花公子.md "wikilink")。

### 體育界

  - 野兔在[體育界的某些項目中被認為是速度的象征](../Page/體育.md "wikilink")，一些以速度見長的運動員被冠以野兔的花名，如澳洲著名網球運動員[萊頓·休伊特](../Page/萊頓·休伊特.md "wikilink")\[1\]。
  - 在某些跑步赛事（比如[马拉松](../Page/马拉松.md "wikilink")）中，兔子是[定速员的俗称](../Page/定速员.md "wikilink")。一般是比赛赛会组委会聘请来控制比赛节奏，帮助参赛选手更好地发挥，甚至破纪录的陪跑人员，多在中长跑比赛中出现。

### 传说生物

  - [鹿角兔](../Page/鹿角兔.md "wikilink")

### 以兔命名的人造物

  - [白兔号列车](../Page/白兔号列车.md "wikilink")

### 作品中的虛構兔

  - [龜兔賽跑](../Page/龜兔賽跑.md "wikilink")
  - [賓尼兔](../Page/賓尼兔.md "wikilink")
  - [賤兔](../Page/賤兔.md "wikilink")
  - [蘿拉兔](../Page/蘿拉兔.md "wikilink")
  - [監獄兔](../Page/監獄兔.md "wikilink")
  - [全力兔](../Page/全力兔.md "wikilink")
  - [米飛兔](../Page/米飛兔.md "wikilink")
  - [美樂蒂](../Page/美樂蒂.md "wikilink")
  - [露比](../Page/寶石寵物.md "wikilink")
  - [揍揍兔](../Page/蠟筆小新.md "wikilink")
  - [彼得兔](../Page/彼得兔.md "wikilink")
  - [瘋狂兔子](../Page/瘋狂兔子.md "wikilink")

## 参见

  - [寵物](../Page/寵物.md "wikilink")

## 相關條目

  - [大久野島](../Page/大久野島.md "wikilink") -兔子的棲息地之一。

## 參考文献

### 引用

### 来源

  - Windling, Terri. [*The Symbolism of Rabbits and
    Hares*](https://www.webcitation.org/67AkEBAkB?url=http://www.endicott-studio.com/rdrm/rrRabbits.html)
  - [全球最巨兔
    年啃4千根胡蘿蔔](http://www.cna.com.tw/news/aopl/201404210360-1.aspx)

## 外部链接

  - [American Rabbit Breeders Association](http://www.arba.net/)
    organization which promotes all phases of rabbit keeping
  - [House Rabbit Society](http://www.rabbit.org/) an activist
    organization which promotes keeping rabbits indoors.
  - [RabbitShows.com](https://web.archive.org/web/20080601034923/http://www.rabbitshows.com/)
    an informational site on the hobby of showing rabbits.
  - [The (mostly) silent language of
    rabbits](http://www.muridae.com/rabbits/rabbittalk.html)
  - [World Rabbit Science Association](http://world-rabbit-science.com/)
    an international rabbit-health science-based organization
  - [The Year of the
    Rabbit](http://www.life.com/image/first/in-gallery/55521/the-year-of-the-rabbit#index/0)
    - slideshow by *[Life
    magazine](../Page/Life_magazine.md "wikilink")*
  - [House Rabbit Society - FAQ:
    Aggression](http://www.rabbit.org/faq/sections/aggression.html#basics)

{{-}}

[R](../Category/兔科.md "wikilink") [R](../Category/生肖.md "wikilink")
[R](../Category/寵物.md "wikilink")

1.