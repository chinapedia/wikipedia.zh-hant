[Altan_Khan.jpg](https://zh.wikipedia.org/wiki/File:Altan_Khan.jpg "fig:Altan_Khan.jpg")
[Altan_Khan.png](https://zh.wikipedia.org/wiki/File:Altan_Khan.png "fig:Altan_Khan.png")
**俺答汗**（，名“俺答”，，意思是朋友），也称**阿勒坦汗**（，“阿勒坦”意思是金子），16世纪后期[蒙古](../Page/蒙古.md "wikilink")[土默特部重要](../Page/土默特部.md "wikilink")[首领](../Page/首领.md "wikilink")，[孛儿只斤氏](../Page/孛儿只斤氏.md "wikilink")，[成吉思汗](../Page/成吉思汗.md "wikilink")[黄金家族后裔](../Page/黄金家族.md "wikilink")，[达延汗孙](../Page/达延汗.md "wikilink")。

## 生平

[明朝](../Page/明朝.md "wikilink")[嘉靖年间崛起](../Page/嘉靖.md "wikilink")，其部落初期游牧于今[内蒙古](../Page/内蒙古.md "wikilink")[呼和浩特一带](../Page/呼和浩特.md "wikilink")，后逐渐强盛，逐原草原霸主[察哈尔部于辽东](../Page/察哈尔部.md "wikilink")，成为右翼蒙古首领，基本上管冶[韃靼大部份地區](../Page/鞑靼_\(蒙古\).md "wikilink")。控制范围东起[宣化](../Page/宣化府.md "wikilink")、[大同以北](../Page/大同府.md "wikilink")，西至[河套](../Page/河套.md "wikilink")，北抵[戈壁沙漠](../Page/戈壁沙漠.md "wikilink")，南临[长城](../Page/长城.md "wikilink")。后他为开辟牧场，把[瓦刺赶到](../Page/瓦刺.md "wikilink")[科布多](../Page/科布多.md "wikilink")，征服[青海](../Page/青海.md "wikilink")，甚至一度用兵[西藏](../Page/西藏.md "wikilink")。

[嘉靖二十九年](../Page/嘉靖.md "wikilink")（1550年），俺答在多次遣使要求开放[朝贡贸易未果后](../Page/朝贡贸易.md "wikilink")，兵临[明朝的首都](../Page/明朝.md "wikilink")[北京](../Page/北京.md "wikilink")，以武力要求明朝政府开放邊境貿易，史称[庚戌之变](../Page/庚戌之变.md "wikilink")。嘉靖三十年（1551年），明朝被迫开放[宣府](../Page/宣府.md "wikilink")、[大同等地与蒙古进行](../Page/大同市.md "wikilink")[马匹交易](../Page/马匹.md "wikilink")。不久，明朝拒绝蒙古方面以牛羊交易的要求，单方关闭马市，双方再次开战。隆慶四年（1570年），俺答為救孫兒[把漢那吉](../Page/把漢那吉.md "wikilink")，與明朝开始和谈，次年达成协议，明朝封俺答为顺义王，开放十一处边境贸易口岸，史稱[俺答封貢](../Page/俺答封貢.md "wikilink")。隆慶六年（1572年），俺答汗與其妻三娘子築庫庫和屯城，萬曆年間，明朝賜名歸化城，即今內蒙古自治區首府呼和浩特市。他的土默特十二部建立了汗国，后被[林丹汗消灭](../Page/林丹汗.md "wikilink")。

俺答晚年皈依[藏传佛教](../Page/藏传佛教.md "wikilink")，也是第一位独尊藏传佛教[格魯派的可汗](../Page/格魯派.md "wikilink")，他禁制了被視為[迷信的](../Page/迷信.md "wikilink")[萨满教活动](../Page/萨满教.md "wikilink")。1578年，他赴[青海与](../Page/青海.md "wikilink")[格鲁派](../Page/格鲁派.md "wikilink")（黄教）首领[索南嘉措](../Page/索南嘉措.md "wikilink")[喇嘛会面](../Page/喇嘛.md "wikilink")（之前[切盡黃台吉發兵](../Page/切盡黃台吉.md "wikilink")[裕固族](../Page/裕固族.md "wikilink")，俘獲了一些喇嘛），并为其修建了[仰华寺](../Page/仰华寺.md "wikilink")。在会面中，俺答为索南嘉错上尊号为“圣识一切瓦齐尔达喇达赖喇嘛”，为[觀音菩薩的化身](../Page/觀音菩薩.md "wikilink")，此为[达赖喇嘛一词的出处](../Page/达赖喇嘛.md "wikilink")。同时，索南嘉措为俺答上尊号为“转千金法轮咱克喇瓦尔第彻辰汗”，承认他为[成吉思汗的](../Page/成吉思汗.md "wikilink")[化身](../Page/化身.md "wikilink")，为全[蒙古的大汗](../Page/蒙古.md "wikilink")。但是他手下曾經對[格魯派有懷疑](../Page/格魯派.md "wikilink")。《[蒙古源流](../Page/蒙古源流.md "wikilink")》记载，在俺答病危時，有些人意圖殺害喇嘛，直到俺答与满珠锡里[呼图克图反覆開導才作罷](../Page/呼图克图.md "wikilink")。

## 家庭

妻妾，有史可查两人

  - 乞庆哈母，被称为[哈屯](../Page/哈屯.md "wikilink")（皇后）
  - [三娘子](../Page/三娘子.md "wikilink")，外孙女，原为其孙[把漢那吉妻](../Page/把漢那吉.md "wikilink")

九子

  - [乞庆哈辛爱黄台吉](../Page/乞庆哈.md "wikilink")
      - 长子[-{zh-hans:扯力克;zh-hant:撦力克}-](../Page/扯力克.md "wikilink")
          - [晁兔台吉](../Page/晁兔台吉.md "wikilink")
              - [卜失兔](../Page/卜失兔.md "wikilink")
      - 五子松木儿台吉
          - 四子[云丹嘉措](../Page/云丹嘉措.md "wikilink")
  - 不彦台吉
      - 擺腰把都兒台吉
  - 铁背台吉
      - [把漢那吉](../Page/把漢那吉.md "wikilink")
  - 丙兔台吉
  - 把林台吉
  - 哥力各台吉
  - 不他失礼台吉
  - 沙赤星台吉
  - 倚儿将逊台吉
  - 恰台吉（义子）

## 注释

## 参考文献

  - 《[明史](../Page/明史.md "wikilink")·列传第二百十五·外国八鞑靼》

## 参见

  - [俺答封貢](../Page/俺答封貢.md "wikilink")

{{-}}     |-

[Category:蒙古君主](../Category/蒙古君主.md "wikilink")
[Category:顺义王](../Category/顺义王.md "wikilink")