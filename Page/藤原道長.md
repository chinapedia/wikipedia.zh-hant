[Fujiwara_Michinaga.jpg](https://zh.wikipedia.org/wiki/File:Fujiwara_Michinaga.jpg "fig:Fujiwara_Michinaga.jpg")

**藤原道長**（\[1\]）日本[平安時代的](../Page/平安時代.md "wikilink")[公卿](../Page/公卿.md "wikilink")，[藤原北家](../Page/藤原北家.md "wikilink")[藤原兼家的第五子](../Page/藤原兼家.md "wikilink")。官至[從一位](../Page/從一位.md "wikilink")[攝政](../Page/攝政.md "wikilink")[太政大臣](../Page/太政大臣.md "wikilink")，[准三宮](../Page/准三宮.md "wikilink")。出家後法名**行觀**，後改名**行覺**。別名**法成寺攝政**、**御堂殿**\[2\]
，或稱**御堂關白**。

道長是[攝關政治](../Page/攝關政治.md "wikilink")、[外戚掌權的最具代表性人物](../Page/外戚.md "wikilink")。他最為有名的事跡是「一家立三-{后}-」。藉著姻親關係，道長掌握了極大的權勢，其在權勢達到頂峰的時期，曾寫下一首和歌「此世即吾世，如月滿無缺」來描述自己的心境。

## 生平

### 早年

980年，敘[從五位下](../Page/從五位下.md "wikilink")。後漸次升遷，至991年任[權大納言](../Page/權大納言.md "wikilink")，992年敘[從二位](../Page/從二位.md "wikilink")。

[藤原兼家去世](../Page/藤原兼家.md "wikilink")，舉行喪禮之際，曾出現一陣騷動，在大家驚惶之際道長不為所動，表示：「我已經派人去了解狀況了，只是馬受到驚嚇罷了。」
當時[清和源氏猛將的](../Page/清和源氏.md "wikilink")[源賴光也在場](../Page/源賴光.md "wikilink")，感嘆道：「此公妥帖，已堪為主將。」\[3\]

### 與伊周的爭執

道長與姪兒[藤原伊周不合](../Page/藤原伊周.md "wikilink")。伊周之父是[關白](../Page/關白.md "wikilink")[藤原道隆](../Page/藤原道隆.md "wikilink")、伊周妹[藤原定子為](../Page/藤原定子.md "wikilink")[一條天皇](../Page/一條天皇.md "wikilink")[中宮](../Page/中宮.md "wikilink")，因為這些關係，伊周在994年任[內大臣](../Page/內大臣.md "wikilink")，官位在時任權大[納言的道長之上](../Page/納言.md "wikilink")。

995年，道隆、[道兼相繼去世](../Page/藤原道兼.md "wikilink")。此時應由何人執掌政務，伊周與道長各有擁護者：一條天皇屬意伊周；輿論及[太后](../Page/太后.md "wikilink")[東三條院](../Page/藤原詮子.md "wikilink")（道長之姊，一條天皇生母）則屬意道長。最後在東三條院的堅持下，由道長以本官（權大納言）任[內覽](../Page/內覽.md "wikilink")、[藤氏長者](../Page/藤氏長者.md "wikilink")，並於不久之後拜[右大臣](../Page/右大臣.md "wikilink")。\[4\]

之後，伊周在996年因弟[隆家放箭恐嚇](../Page/藤原隆家.md "wikilink")[花山法皇而中其衣袖](../Page/花山天皇.md "wikilink")、且被發現有詛咒[東三條院之舉動](../Page/藤原詮子.md "wikilink")，因而被流放[播磨](../Page/播磨.md "wikilink")；定子中宮因伊周遭遠流，憂慮之餘剃髮為尼。而道長則轉任[左大臣](../Page/左大臣.md "wikilink")，進[正二位](../Page/正二位.md "wikilink")。\[5\]

### 一條天皇時代

999年，道長的長女[彰子入內](../Page/藤原彰子.md "wikilink")，成為[一條天皇的](../Page/一條天皇.md "wikilink")[女御](../Page/女御.md "wikilink")。1000年，彰子立為[中宮](../Page/中宮.md "wikilink")，原來的中宮[定子則升為](../Page/藤原定子.md "wikilink")[皇后](../Page/皇后.md "wikilink")。不久皇后定子因難產而去世。

一條天皇與彰子相處的不壞，然而道長掌權之後玩弄威權，令一條天皇很不愉快。1011年，一條天皇去世，據聞曾留下「叢蘭欲茂，秋風吹破，王事欲寄，讒臣亂國。」這樣的文字，道長看到這段話之後，認為是指自己，很生氣的把這段文字湮滅了。
\[6\]

### 三條天皇時代

[三條天皇是](../Page/三條天皇.md "wikilink")[一條天皇的](../Page/一條天皇.md "wikilink")[堂兄](../Page/堂亲.md "wikilink")。即位之前，已有東宮妃[藤原娍子](../Page/藤原娍子.md "wikilink")，並有多名皇子女。即位之後，道長納女[妍子入宮](../Page/藤原妍子.md "wikilink")，而以一條天皇與彰子之子[敦成親王為皇太子](../Page/後一條天皇.md "wikilink")。妍子先立為中宮，之後娀子立為皇后。
\[7\]

道長表面上勸三條天皇立娀子為-{后}-，然而內心卻不願意；以至立-{后}-當日，群臣忌憚道長的威權，而不敢來賀。因此三條天皇對道長非常不滿。\[8\]

而後三條天皇患目疾，道長於1015年任[准攝政](../Page/攝政.md "wikilink")。之後對天皇施加了很大的壓力，逼迫他退位。最後以立三條天皇的兒子[敦明親王為皇太子作交換條件](../Page/敦明親王.md "wikilink")，三條天皇退位，敦成親王即位，是為[後一條天皇](../Page/後一條天皇.md "wikilink")。\[9\]

### 後一條天皇時代

1016年，[後一條天皇即位時年僅](../Page/後一條天皇.md "wikilink")9歲，道長[攝政](../Page/攝政.md "wikilink")。
\[10\]

1017年，讓[攝政給兒子賴通](../Page/攝政.md "wikilink")，敘[從一位](../Page/從一位.md "wikilink")。不久[三條天皇去世](../Page/三條天皇.md "wikilink")，道長廢皇太子[敦明親王](../Page/敦明親王.md "wikilink")（尊之為小一條院太上天皇），
而改立後一條天皇之同母弟[敦良親王為皇太子](../Page/後朱雀天皇.md "wikilink")。同年，道長官拜[太政大臣](../Page/太政大臣.md "wikilink")。
\[11\]

1018年，道長辭[太政大臣](../Page/太政大臣.md "wikilink")。其後納女[威子作為後一條天皇的中宮](../Page/藤原威子.md "wikilink")。至此，道長「**一家立三-{后}-**」，因而在宴席間詠和歌「此世即吾世，如月滿無缺」（この世をば　わが世とぞ思ふ　望月の　欠けたることも　なしと思へば）來描述自己的心境。\[12\]

藤原道長一家三-{后}-列表：\[13\]

<table>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><strong>三-{后}-</strong></p></td>
<td style="text-align: center;"><p><strong>太皇太后</strong></p></td>
<td style="text-align: center;"><p><strong>皇太后</strong></p></td>
<td style="text-align: center;"><p><strong>皇后</strong><br />
<sub>（中宮）</sub></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>人物</p></td>
<td style="text-align: center;"><p><strong><a href="../Page/藤原彰子.md" title="wikilink">藤原彰子</a></strong></p></td>
<td style="text-align: center;"><p><strong><a href="../Page/藤原妍子.md" title="wikilink">藤原妍子</a></strong></p></td>
<td style="text-align: center;"><p><strong><a href="../Page/藤原威子.md" title="wikilink">藤原威子</a></strong></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>出身</p></td>
<td style="text-align: center;"><p>父：<strong>藤原道長</strong>、母<a href="../Page/源倫子.md" title="wikilink">源倫子</a>。<br />
988年出生、1074年去世。</p></td>
<td style="text-align: center;"><p>父：<strong>藤原道長</strong>、母<a href="../Page/源倫子.md" title="wikilink">源倫子</a>。<br />
994年出生、1027年去世。</p></td>
<td style="text-align: center;"><p>父：<strong>藤原道長</strong>、母<a href="../Page/源倫子.md" title="wikilink">源倫子</a>。<br />
1000年出生、1036年去世。</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>冊立時間</p></td>
<td style="text-align: center;"><p>1018年</p></td>
<td style="text-align: center;"><p>1018年</p></td>
<td style="text-align: center;"><p>1018年</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>備考</p></td>
<td style="text-align: center;"><p>1000年為<a href="../Page/一條天皇.md" title="wikilink">一條天皇中宮</a>、1012年立皇太后。<br />
1026年彰子出家，詔停-{后}-號，為<strong>上東門院</strong>。</p></td>
<td style="text-align: center;"><p>1012年為<a href="../Page/三條天皇.md" title="wikilink">三條天皇中宮</a>。</p></td>
<td style="text-align: center;"><p><a href="../Page/後一條天皇.md" title="wikilink">後一條天皇中宮</a>。<br />
威子立中宮時，<a href="../Page/三條天皇.md" title="wikilink">三條天皇皇后宮</a><a href="../Page/藤原娍子.md" title="wikilink">娍子尚在</a>。<br />
（<a href="../Page/藤原娍子.md" title="wikilink">娍子</a>972年出生、1012年立皇后、1025年去世。）</p></td>
</tr>
</tbody>
</table>

1019年，因病出家，法名**行觀**，又改名**行覺**。 \[14\]

1021年，道長最小的女兒[嬉子立為皇太子](../Page/藤原嬉子.md "wikilink")[敦良親王的東宮妃](../Page/後朱雀天皇.md "wikilink")。[嬉子於](../Page/藤原嬉子.md "wikilink")1025年生下[親仁親王後去世](../Page/後冷泉天皇.md "wikilink")。

1027年，外孫女[禎子內親王入皇太弟](../Page/禎子內親王.md "wikilink")[敦良親王宮](../Page/後朱雀天皇.md "wikilink")。（禎子內親王之父為[三條天皇](../Page/三條天皇.md "wikilink")，之母為[妍子](../Page/藤原妍子.md "wikilink")；敦良親王之父為[一條天皇](../Page/一條天皇.md "wikilink")，母為[彰子](../Page/藤原彰子.md "wikilink")。此二人均為道長的外孫。二人於1034年生下皇子尊仁；尊仁即[後三條天皇](../Page/後三條天皇.md "wikilink")，從他開始[攝關政治出現衰退](../Page/攝關政治.md "wikilink")。）同年年底道長去世\[15\]。

## 人物

道長出家後正式的稱呼是「**入道前太政大臣藤原朝臣道長公**」，或稱「**入道大相國**」\[16\]。

道長早年為人豪爽，而不願居於人下。遇事能鎮定，被猛將[源賴光形容](../Page/源賴光.md "wikilink")「堪為主將」。\[17\]

篤信佛教。建[法成寺](../Page/法成寺.md "wikilink")，欲與[東大寺媲美](../Page/東大寺.md "wikilink")，建築進行時，道長已得病，子[賴通催促加快建築腳步](../Page/藤原賴通.md "wikilink")：「寧可荒廢公事，也不能荒廢建築法成寺。」於是很快的就建成了。
\[18\] （法成寺建於1021年，費時兩年而成。1058年燒毀後重建，1219年又再次燒毀。\[19\]）

道長被視為[藤原氏的象徵](../Page/藤原氏.md "wikilink")，而他的去世則被視為藤原氏沒落的開始\[20\]。儘管如此，號稱「與藤原氏無直接關係」\[21\]的[後三條天皇](../Page/後三條天皇.md "wikilink")，是道長的曾孫；[後三條的大力支持者](../Page/後三條天皇.md "wikilink")[藤原能信](../Page/藤原能信.md "wikilink")，是道長的第四個兒子（母為源明子）；而其東宮妃藤原茂子（[白河天皇生母](../Page/白河天皇.md "wikilink")），則是[能信的養女](../Page/藤原能信.md "wikilink")。此外，平安朝後期崛起的公卿[村上源氏](../Page/村上源氏.md "wikilink")，其始祖[源師房也是道長的女婿](../Page/源師房.md "wikilink")（其妻藤原尊子為道長與源明子之女），其嫡子[俊房](../Page/源俊房.md "wikilink")、[顯房均為道長外孫](../Page/源顯房.md "wikilink")。由上述，與其說「道長的去世是藤原氏沒落的開始」，改成「道長去世後，其嫡系與旁系的子孫，各結派系互相鬥爭」，或許更貼切些。

## 家庭

道長有兩位地位較高的妻妾，分別是[源雅信之女](../Page/源雅信.md "wikilink")[源倫子](../Page/源倫子.md "wikilink")、與[源高明之女](../Page/源高明.md "wikilink")[源明子](../Page/源明子.md "wikilink")。由岳家的背景、本身的敘位、以及子女當時的地位來看，源倫子應是正妻（[准三宮](../Page/准三宮.md "wikilink")[從一位](../Page/從一位.md "wikilink")，源雅信時任[左大臣](../Page/左大臣.md "wikilink")，[賴通](../Page/藤原賴通.md "wikilink")、[教通分別為藤氏長者](../Page/藤原教通.md "wikilink")）；相對的源明子僅敘[從三位](../Page/從三位.md "wikilink")。
\[22\] \[23\]

### 系譜

\[24\]\[25\]

  - 父：[藤原兼家](../Page/藤原兼家.md "wikilink")
  - 母：[藤原時姬](../Page/藤原時姬.md "wikilink") -
    [藤原中正之女](../Page/藤原中正.md "wikilink")
  - 正室：[源倫子](../Page/源倫子.md "wikilink") -
    [左大臣](../Page/左大臣.md "wikilink")[源雅信之女](../Page/源雅信.md "wikilink")，母為[穆子](../Page/藤原穆子.md "wikilink")
      - 長女：[藤原彰子](../Page/藤原彰子.md "wikilink")（988-1074） -
        [一條天皇中宮](../Page/一條天皇.md "wikilink")
      - 長男：[藤原賴通](../Page/藤原賴通.md "wikilink")（992-1074） - 攝政、關白
      - 次女：[藤原妍子](../Page/藤原妍子.md "wikilink")（993-1027） -
        [三條天皇中宮](../Page/三條天皇.md "wikilink")
      - 五男：[藤原教通](../Page/藤原教通.md "wikilink")（996-1075） - 關白
      - 四女：[藤原威子](../Page/藤原威子.md "wikilink")（1000-1036） -
        [後一條天皇中宮](../Page/後一條天皇.md "wikilink")
      - 六女：[藤原嬉子](../Page/藤原嬉子.md "wikilink")（1007-1025） -
        [後朱雀天皇東宮妃](../Page/後朱雀天皇.md "wikilink")
  - 側室：[源明子](../Page/源明子.md "wikilink") -
    左大臣[源高明之女](../Page/源高明.md "wikilink")，母為[愛宮](../Page/愛宮.md "wikilink")。養父為[盛明親王](../Page/盛明親王.md "wikilink")
      - 次男：[藤原賴宗](../Page/藤原賴宗.md "wikilink")（993-1065） -
        [右大臣](../Page/右大臣.md "wikilink")，[中御門家之祖](../Page/中御門家.md "wikilink")
      - 三男：[藤原顯信](../Page/藤原顯信.md "wikilink")（994-1027） -
        入道前[右馬頭](../Page/右馬頭.md "wikilink")（出家）
      - 四男：[藤原能信](../Page/藤原能信.md "wikilink")（995-1065） -
        [權大納言](../Page/大納言.md "wikilink")
      - 三女：[藤原寛子](../Page/藤原寛子.md "wikilink")（999-1025） -
        [敦明親王之](../Page/敦明親王.md "wikilink")[女御](../Page/女御.md "wikilink")
      - 五女：[藤原尊子](../Page/藤原尊子.md "wikilink")（1003?-1087?） -
        [源師房之妻](../Page/源師房.md "wikilink")
      - 六男：[藤原長家](../Page/藤原長家.md "wikilink")（1005-1064） -
        權大納言，[御子左家之祖](../Page/御子左家.md "wikilink")
  - 妾：[源重光女](../Page/源重光.md "wikilink")
      - 七男：[長信](../Page/長信.md "wikilink")（1014-1072）
  - 生母不明
      - 女子：[藤原盛子](../Page/藤原盛子.md "wikilink") -
        [三條天皇尚侍](../Page/三條天皇.md "wikilink")
  - 其他

### 道長與天皇家的姻親關係示意

數字表天皇代數，點線表示姻親關係。

## 著作

日記14本，被稱為法成寺攝政記或[御堂關白記](../Page/御堂關白記.md "wikilink")，被指定為日本的國寶\[26\]。

## 相關流行文化

  - [陰陽師](../Page/陰陽師.md "wikilink")：日本小說，[夢枕貘著](../Page/夢枕貘.md "wikilink")。其中有提到藤原道長，書中有與[安倍晴明的關係](../Page/安倍晴明.md "wikilink")。
  - [少年陰陽師](../Page/少年陰陽師.md "wikilink")：日本輕小說、動漫。藤原道長成為主要配角，其女兒[藤原璋子為女主角](../Page/藤原璋子.md "wikilink")。

## 參考資料

  - [歴史コラム
    藤原道長](https://web.archive.org/web/20070712233150/http://www.loops.jp/~asukaclub/hmemo/hmemo037.html)

|-    |-  |-    |-    |-    |-    |-  |-    |-

[Category:平安時代中期貴族](../Category/平安時代中期貴族.md "wikilink")
[Category:御堂流](../Category/御堂流.md "wikilink")
[Category:攝關](../Category/攝關.md "wikilink")
[Category:平安時代外戚](../Category/平安時代外戚.md "wikilink")

1.  道長去世於[萬壽](../Page/萬壽.md "wikilink")4年12月。萬壽4年是1027年，但月份則採[舊曆](../Page/舊曆.md "wikilink")，換算成西元則屬1028年。道長去世見[日本紀略](http://miko.org/~uraki/kuon/furu/text/kiryaku/b13.htm#manjyu)

2.  大日本史[藤原道長傳](http://miko.org/~uraki/kuon/furu/text/dainihonsi/dns138.htm#01)

3.  [日本國史略](http://miko.org/~uraki/kuon/furu/text/siryaku/siryaku03-066.htm)

4.
5.
6.
7.
8.  [大日本史藤原娀子傳](http://miko.org/~uraki/kuon/furu/text/dainihonsi/dns080.htm)

9.
10.
11.
12.
13. 「三-{后}-」指[太皇太后](../Page/太皇太后.md "wikilink")、[皇太后](../Page/皇太后.md "wikilink")、[皇后三者](../Page/皇后.md "wikilink")。對道長家而言，亦可有**三代天皇之-{后}-**的意思。自[彰子以降](../Page/藤原彰子.md "wikilink")，常有「中宮」與「皇后宮」並立的狀況，且中宮常有很強的後台，其勢往往凌駕於皇后宮之上，是實質的皇后。中宮與皇后可參見[日本皇后列表](../Page/日本皇后列表.md "wikilink")。道長家-{三后}-冊立時間可參見[日本紀略](http://miko.org/~uraki/kuon/furu/text/kiryaku/b13.htm)

14.
15.
16.
17.
18.
19. 參見日文法成寺條目

20. 日本史（鄭學稼著，黎明文化，臺北，民66）

21.
22. [公卿類別譜](http://www.geocities.jp/okugesan_com/fujiwaranomichinaga.htm#a11)

23. 參照日文版[源明子條目](../Page/源明子.md "wikilink")

24.
25. 日文版藤原道長條目

26. [國寶御堂關白記](http://www.bunka.go.jp/bsys/maindetails.asp?register_id=201&item_id=816)