是由动作影星-{[李連杰](../Page/李連杰.md "wikilink")}-主演的一部影片，拍摄于1992年，导演为[徐克](../Page/徐克.md "wikilink")。

以「黃飛鴻」為主題的系列影片有數部，本影片是該系列影片的第三部。

## 劇情大要

黃飛鴻到[京師探望父親](../Page/京師.md "wikilink")[黃麒英](../Page/黃麒英.md "wikilink")，卻得知[俄國人要在](../Page/俄國人.md "wikilink")[舞獅獅王大賽時刺殺](../Page/舞獅.md "wikilink")[宰相](../Page/宰相.md "wikilink")[李鴻章](../Page/李鴻章.md "wikilink")，黃飛鴻雖不喜歡李鴻章，卻為了民族大義，還是決定出手相救。最後，黃飛鴻在[舞獅獅王大賽中](../Page/舞獅.md "wikilink")，打败了惡棍赵天霸，也乘機拯救了李鴻章。

## 演員表

### 主演演員

|                                          |                                       |                                  |                       |
| ---------------------------------------- | ------------------------------------- | -------------------------------- | --------------------- |
| **演員**                                   | **角色**                                | **粵語配音**                         | **備註**                |
| [李連杰](../Page/李連杰.md "wikilink")         | [黃飛鴻](../Page/黃飛鴻.md "wikilink")（黃師傅） | [黃志成](../Page/黃志成.md "wikilink") |                       |
| [關之琳](../Page/關之琳.md "wikilink")         | 十三姨（少筠）                               | [馮蔚衡](../Page/馮蔚衡.md "wikilink") |                       |
| [莫少聰](../Page/莫少聰.md "wikilink")         | 梁　寬（鐵釘寬）                              | [譚漢華](../Page/譚漢華.md "wikilink") | 黃飛鴻徒弟                 |
| [劉　洵](../Page/劉洵_\(香港演員\).md "wikilink") | [黃麒英](../Page/黃麒英.md "wikilink")      | [源家祥](../Page/源家祥.md "wikilink") | 黃飛鴻之父                 |
| [熊欣欣](../Page/熊欣欣.md "wikilink")         | 鬼腳七                                   | [謝君豪](../Page/謝君豪.md "wikilink") | 原為趙天霸手下，後被黃飛鴻所救，成為其徒弟 |
| [趙　箭](../Page/趙箭.md "wikilink")          | 趙天霸                                   |                                  |                       |

### 合演演員

|                                                        |                                  |                                        |                |
| ------------------------------------------------------ | -------------------------------- | -------------------------------------- | -------------- |
| **演員**                                                 | **角色**                           | **粵語配音**                               | **備註**         |
| [葛存壯](../Page/葛存壯.md "wikilink")                       | [李鴻章](../Page/李鴻章.md "wikilink") | [劉丹](../Page/劉丹.md "wikilink")         | 清末重臣           |
| [John Wakefield](../Page/John_Wakefield.md "wikilink") | 杜文奇                              | [陳欣](../Page/陳欣_\(配音員\).md "wikilink") | 俄國人，十三姨同學，日本間諜 |
| [孟瑾](../Page/孟瑾.md "wikilink")                         |                                  |                                        |                |
| [黃德仁](../Page/黃德仁.md "wikilink")                       |                                  |                                        |                |

## 工作人員

  - 出品人：[鄒文懷](../Page/鄒文懷.md "wikilink")
  - 策劃：[麥子善](../Page/麥子善.md "wikilink")
  - 武術指導：[元彬](../Page/元彬.md "wikilink")
  - 美術指導：[葉錦添](../Page/葉錦添.md "wikilink")
  - 服裝指導：[程天嬌](../Page/程天嬌.md "wikilink")
  - 原作音樂：[胡偉立](../Page/胡偉立.md "wikilink")、[徐克](../Page/徐克.md "wikilink")
  - 執行製片：[趙桂松](../Page/趙桂松.md "wikilink")
  - 助理製片：[劉小娟](../Page/劉小娟.md "wikilink")、[陳慧芝](../Page/陳慧芝.md "wikilink")
  - 副導演：[陶雲](../Page/陶雲.md "wikilink")、[錢文錡](../Page/錢文錡.md "wikilink")、[李保章](../Page/李保章.md "wikilink")
  - 舞獅指導組：[凌錦華](../Page/凌錦華.md "wikilink")、[孔祥德](../Page/孔祥德.md "wikilink")、[葉承健](../Page/葉承健.md "wikilink")、[郭雅長](../Page/郭雅長.md "wikilink")、[鄒長德](../Page/鄒長德.md "wikilink")、[鄭浩榮](../Page/鄭浩榮.md "wikilink")、[鄭帶勝](../Page/鄭帶勝.md "wikilink")
  - 武術指導組：[易天雄](../Page/易天雄.md "wikilink")、[藍海瀚](../Page/藍海瀚.md "wikilink")、[何漢洲](../Page/何漢洲.md "wikilink")、[張炳全](../Page/張炳全.md "wikilink")、[張力](../Page/張力.md "wikilink")、[鄭義](../Page/鄭義.md "wikilink")、[黃德仁](../Page/黃德仁.md "wikilink")
  - 助理美指：[劉志健](../Page/劉志健.md "wikilink")、[馮繼輝](../Page/馮繼輝.md "wikilink")
  - 助理服指：[陳秀嫻](../Page/陳秀嫻.md "wikilink")
  - 第二組攝影：[曹萬強](../Page/曹萬強.md "wikilink")
  - 助理攝影：[黎耀輝](../Page/黎耀輝.md "wikilink")、[何寶榮](../Page/何寶榮.md "wikilink")
  - 燈光：[黃志明](../Page/黃志明.md "wikilink")
  - 燈光助理：[關永祥](../Page/關永祥.md "wikilink")
  - 劇務：[吳錦超](../Page/吳錦超.md "wikilink")
  - 化妝：[關莉娜](../Page/關莉娜.md "wikilink")
  - 梳頭：[鍾偉誠](../Page/鍾偉誠.md "wikilink")
  - 服裝管理：[劉美珍](../Page/劉美珍.md "wikilink")
  - 道具：[李坤龍](../Page/李坤龍.md "wikilink")
  - 道具助手：[鍾劍偉](../Page/鍾劍偉.md "wikilink")
  - 劇照：[陳錦泉](../Page/陳錦泉.md "wikilink")
  - 後期製作：[陳志偉](../Page/陳志偉.md "wikilink")、[周志超](../Page/周志超.md "wikilink")、[莊永雄](../Page/莊永雄.md "wikilink")
  - 粵語對白：[林安兒](../Page/林安兒.md "wikilink")
  - 國語對白：[馮雪銳](../Page/馮雪銳.md "wikilink")
  - 效果：[程小龍](../Page/程小龍.md "wikilink")
  - 錄音：[嘉禾錄音室](../Page/橙天嘉禾.md "wikilink")、[聲藝錄音室](../Page/聲藝錄音室.md "wikilink")
  - 混音：[周錦榮](../Page/周錦榮.md "wikilink")
  - 字幕：[晶藝電影字幕公司](../Page/晶藝電影字幕公司.md "wikilink")
  - 沖印：[天工彩色沖印有限公司](../Page/天工彩色沖印有限公司.md "wikilink")
  - 器材：[Filmed In
    Panavision](../Page/Filmed_In_Panavision.md "wikilink")

## 電影音樂

| 曲別  | 歌名      | 作詞                             | 作曲 | 編曲                               | 演唱者                                                                                                                                                                  |
| --- | ------- | ------------------------------ | -- | -------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 主題曲 | 〈男兒當自強〉 | [黃霑](../Page/黃霑.md "wikilink") | 佚名 | [雷頌德](../Page/雷頌德.md "wikilink") | [譚錫禧](../Page/譚錫禧.md "wikilink")、[張偉文](../Page/張偉文.md "wikilink")、[雷有曜](../Page/雷有曜.md "wikilink")、[雷有輝](../Page/雷有輝.md "wikilink")、[王路明](../Page/王路明.md "wikilink") |

  - 主題曲混音：[梁家麟](../Page/梁家麟.md "wikilink")

## 得獎紀錄

<table>
<thead>
<tr class="header">
<th><p>頒獎典禮</p></th>
<th><p>獎項</p></th>
<th><p>名單</p></th>
<th><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/1994年香港電影金像獎.md" title="wikilink">第13屆香港電影金像獎</a></p></td>
<td><p>最佳動作指導</p></td>
<td><p><a href="../Page/元彬.md" title="wikilink">元彬</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳剪接</p></td>
<td><p><a href="../Page/麥子善.md" title="wikilink">麥子善</a>、<a href="../Page/林安兒.md" title="wikilink">林安兒</a></p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 音樂原聲帶

1.  \[黄飞鸿III之狮王争霸\]主题
2.  苍生
3.  赵天霸
4.  十三姨
5.  鬼脚七
6.  悲悯
7.  俄国军官
8.  鬼脚七和黄飞鸿
9.  含情脉脉
10. 阴谋
11. 快乐的钢琴－阴影
12. 忧国
13. 俄国军官2
14. 俄国人的失落
15. 俄国人过场
16. 乐观
17. 蓄势
18. 趣
19. 磨炼
20. 狮阵

## 外部連結

  - {{@movies|fenO80108592}}

  -
  -
  -
  -
  -
  -
[Category:香港續集電影](../Category/香港續集電影.md "wikilink")
[Category:黃飛鴻電影](../Category/黃飛鴻電影.md "wikilink")
[3](../Category/1990年代香港電影作品.md "wikilink")
[Category:徐克電影](../Category/徐克電影.md "wikilink")
[Category:嘉禾电影](../Category/嘉禾电影.md "wikilink")
[Category:胡伟立配乐电影](../Category/胡伟立配乐电影.md "wikilink")