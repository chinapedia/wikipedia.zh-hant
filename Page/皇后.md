**皇后**，在[中國史籍中常簡稱為](../Page/中國.md "wikilink")-{**后**}-，是[世界歷史上](../Page/世界歷史.md "wikilink")[帝國最高統治者](../Page/帝國.md "wikilink")——[皇帝](../Page/皇帝.md "wikilink")[正配的稱號](../Page/嫡妻.md "wikilink")。目前唯一仍有皇后存在的[國家及](../Page/國家.md "wikilink")[皇室是](../Page/皇室.md "wikilink")[日本](../Page/日本.md "wikilink")[皇室](../Page/日本皇室.md "wikilink")，為[明仁天皇](../Page/明仁天皇.md "wikilink")（亦是现存唯一的[皇帝](../Page/皇帝.md "wikilink")）的皇后[美智子](../Page/美智子.md "wikilink")，其他國家君主的正配一般只稱為“[王后](../Page/王后.md "wikilink")”。

## 概述

如前文所述，只有帝國最高統治者的正配才能被稱為、冊立為皇后，如[東亞](../Page/東亞.md "wikilink")[漢字文化圈內的](../Page/漢字文化圈.md "wikilink")[皇帝](../Page/皇帝.md "wikilink")、[天皇](../Page/天皇.md "wikilink")，或[歐洲的](../Page/歐洲.md "wikilink")[俄羅斯帝國](../Page/俄羅斯帝國.md "wikilink")[沙皇](../Page/沙皇.md "wikilink")、[神聖羅馬帝國皇帝](../Page/神聖羅馬帝國.md "wikilink")、[中東及](../Page/中東.md "wikilink")[北非的](../Page/北非.md "wikilink")[萬王之王等君主的正配](../Page/萬王之王.md "wikilink")，因此，皇后的級別與使用和君王的稱號息息相關，只有在君王稱號為皇帝（包括天皇、沙皇、萬王之王等，下文以皇帝簡略之）等等時，其正配才能稱皇后；若僅為[王](../Page/王.md "wikilink")、[國王者](../Page/國王.md "wikilink")，則其正配只能稱[王后](../Page/王后.md "wikilink")、甚至再次等的[王妃](../Page/王妃.md "wikilink")。

皇后的冊立與人選，古來都是帝國的大事，尤其在中國，皇后的廢立有時常關係著政治鬥爭甚至[皇后家族的存亡](../Page/外戚.md "wikilink")。原則上，皇帝是帝國的最高統治者，而皇后則是皇帝所有妻妾之首、內宮之長，有權力統御管理所有的[皇族與](../Page/皇.md "wikilink")[貴族女性](../Page/貴族.md "wikilink")，甚至是帝國內母儀及婦女風範的表率。

## 东亚

### 中國

皇后的-{后}-，最早在中國的[夏朝](../Page/夏朝.md "wikilink")，是君主生前的稱號，如-{[后羿](../Page/后羿.md "wikilink")}-、-{[后稷](../Page/后稷.md "wikilink")}-等等，君主在死後才改稱為[帝](../Page/帝.md "wikilink")；至於君王正配則稱為[妃](../Page/妃.md "wikilink")，[嬪則是在](../Page/嬪.md "wikilink")[周朝才開始使用的漢字](../Page/周朝.md "wikilink")。[商朝開始](../Page/商朝.md "wikilink")，君主生前稱王，死後稱帝，原本的**-{后}-**則轉降為君王正配的稱號，稱**王后**，而妃也隨之降為君王[側室的稱號](../Page/側室.md "wikilink")。

現代為人所熟悉與使用的皇后一號，則是在[秦始皇確立了](../Page/秦始皇.md "wikilink")**皇帝**一詞作為國家最高統治者的稱號之後才出現，因為有了皇帝的出現，將原本的王后升格為級別更高的皇后，成為帝國內唯一能與皇帝平起平坐的地位與角色。現存的史料文獻無法確定秦始皇是否有冊立皇后，而秦二世临死前哀求咸阳县令阎乐说：“愿与妻子为黔首，比诸公子。”[秦二世皇后除此之外别无记载](../Page/秦二世皇后.md "wikilink")。因此[中國歷史上的第一位正式記載的皇后是](../Page/中國歷史.md "wikilink")[漢高祖](../Page/漢高祖.md "wikilink")[劉邦的妻子](../Page/劉邦.md "wikilink")[呂雉](../Page/呂雉.md "wikilink")。

#### 別稱

  - **[椒房](../Page/椒房.md "wikilink")**：[漢朝皇后的宮殿多以](../Page/漢朝.md "wikilink")**椒**塗壁，用以取暖避邪，也有“多子”之意，因此椒房可別稱皇后、或用以代稱皇后寢宮。
      - 漢朝哀帝寵愛美男子[董賢](../Page/董賢.md "wikilink")，將董賢之妹冊封為[董昭儀](../Page/董昭儀.md "wikilink")，並將之稱為**次皇后**，又在宮中賜董昭儀宮室名為**椒風舍**，和皇后所居之椒房相似。
  - **中宮**、**正宮**：古代皇后所居的寢宮多位於後宮正中央，因此皇后寢宮可稱為**正宮**或**中宮**，<span style="font-size:smaller;">詳細解釋請參閱[中宮](../Page/中宮.md "wikilink")</span>。
  - **元-{后}-**、**元嫡**：皇帝的元配皇后可稱元-{后}-，以和繼位皇后（繼后）有所區別。
  - **梓童**：皇帝對皇后的稱呼，如現今的**老婆**。
  - **慈壺**、**慈闈**：對皇后或皇帝生母（多為[皇太后](../Page/皇太后.md "wikilink")）的尊稱，或是用以代稱這兩者的寢宮。
  - **天下母**、**天地母**：皆為對皇后的敬稱。
  - **坤極**、**坤位**：皇后的別稱。
  - **聖人**：宋朝皇后尊稱。

#### 册立

[“皇后之玺”玉印上部,_2015-09-22.jpg](https://zh.wikipedia.org/wiki/File:“皇后之玺”玉印上部,_2015-09-22.jpg "fig:“皇后之玺”玉印上部,_2015-09-22.jpg")，被认为是西汉第一位皇后[吕雉的信物](../Page/吕雉.md "wikilink")。\]\]
皇后的產生需經過皇帝冊立。冊立皇后是一件非常重要的國家事件，要詔告天下，普天同慶，同時還有一項正式隆重的立-{后}-儀式。除了正式的[诏书](../Page/诏书.md "wikilink")（亦或以[金册形式](../Page/金册.md "wikilink")）外，颁发[印章做为](../Page/印章.md "wikilink")[信物](../Page/信物.md "wikilink")。

若是皇帝在登基前以有[正室](../Page/嫡妻.md "wikilink")，則通常不另擇人選，直接將正室納入[後宮](../Page/後宮.md "wikilink")，並冊立為皇后，但也有皇帝是先將正室納入後宮，冊封為[嬪妃](../Page/嬪妃.md "wikilink")，待日後在進行擢昇為皇后，如[汉宣帝的妻子](../Page/汉宣帝.md "wikilink")[许平君先被封为](../Page/许平君.md "wikilink")[婕妤](../Page/婕妤.md "wikilink")，后立为皇后。不是所有正室都能被立为皇后，如[曹丕的妻子](../Page/曹丕.md "wikilink")[甄氏](../Page/文昭皇后.md "wikilink")。另一些得不到皇后之位的正室则会被册封为妃嫔，如[唐宪宗的发妻](../Page/唐宪宗.md "wikilink")[郭氏](../Page/懿安皇后.md "wikilink")，[后唐莊宗的发妻](../Page/李存勗.md "wikilink")[韩氏](../Page/韓淑妃_\(後唐莊宗\).md "wikilink")。

#### 數量

中國歷代皇帝在原則上，都保持著同時期只有一名皇后在位，但中國歷史上仍有部分由少數民族所建立的政權，其君主打破了一帝一-{后}-的規範，從而有一帝多-{后}-的局面，比較有名的是匈奴人所建立的前趙，其第二任皇帝劉聰，一生親自冊立過九位皇后，其中有多位皇后是同時存在；而鮮卑人所建立的北周，第四任皇帝宇文贇，先後冊立了共五位皇后，同一時間五-{后}-並立。另外，元朝的多位皇帝也同時有數位皇后並存，而由漢族建立之北齊，其第五任皇帝後主高緯也曾左右皇后並立，這也是漢族建立之政權中，唯一同時立-{后}-多於一人者。

#### 宫廷生活

皇后在禮儀上與皇帝平等，出同車、入同座。在元旦、皇帝以及本人生日要接受百官的朝賀。皇后擁有自己的官署（如[漢朝的](../Page/漢朝.md "wikilink")[皇后三卿](../Page/皇后三卿.md "wikilink")），負責管理後宮，理論上皇帝的所有[嬪御](../Page/嬪御.md "wikilink")、後宮的[宮女](../Page/宮女.md "wikilink")、[女官等](../Page/女官.md "wikilink")，都是她的臣屬。

當一名女子為皇后時沒有[徽號](../Page/徽號.md "wikilink")（除去[懿安皇后的情形](../Page/孝哀悊皇后.md "wikilink")），只有在皇后升為[皇太后](../Page/皇太后.md "wikilink")、[太皇太后時才有徽號](../Page/太皇太后.md "wikilink")，或是她在當[嬪妃的時期才會有徽號](../Page/嬪妃.md "wikilink")。

#### 尊封

皇帝去世（稱[大行皇帝](../Page/大行皇帝.md "wikilink")）的話，皇后一般獲嗣皇帝尊為-{[皇太后](../Page/皇太后.md "wikilink")}-，並同時恭上皇太后徽號，但嗣皇帝並不總是皇后親生子，有可能是前任皇后或[妃嫔所生的儿子以及大行皇帝的兄弟](../Page/妃嫔.md "wikilink")。

若嗣皇帝为妃嫔所生的[庶子](../Page/庶子.md "wikilink")，会将皇后和生母一并尊为皇太后，有的朝代只将皇后尊为皇太后，而皇帝生母得不到尊封或仅尊封為[太妃的](../Page/太妃.md "wikilink")。如果生母早逝，皇帝通常会追尊她为皇后或皇太后甚至[太皇太后](../Page/太皇太后.md "wikilink")，例如[南明](../Page/南明.md "wikilink")[弘光帝追尊祖母](../Page/弘光帝.md "wikilink")[鄭貴妃為太皇太后](../Page/鄭貴妃.md "wikilink")。

若嗣皇帝與去世的皇帝[輩分相當](../Page/輩分.md "wikilink")，甚至是大行皇帝的尊長的時候，大行皇帝的皇后就只能稱○○皇后（○○為大行皇帝的最後一個年號，或是由繼任皇帝上徽號給該皇后，如[明朝](../Page/明.md "wikilink")[懿安皇后](../Page/孝哀悊皇后.md "wikilink")）。

### 越南

独立前的[越南君主](../Page/越南.md "wikilink")[李南帝](../Page/李南帝.md "wikilink")、[梅黑帝没有关于他们皇后的记载](../Page/梅黑帝.md "wikilink")。越南的皇帝一般一次可以设置多个皇后。[丁朝的](../Page/丁朝.md "wikilink")[丁先皇立了](../Page/丁先皇.md "wikilink")5个皇后，[前黎朝的](../Page/前黎朝.md "wikilink")[黎大行立了](../Page/黎大行.md "wikilink")5个皇后，[李太祖立了](../Page/李太祖.md "wikilink")3个皇后，[李太宗立了](../Page/李太宗.md "wikilink")7个皇后。不过，在[越南陳朝](../Page/越南陳朝.md "wikilink")、[后黎朝刚建立时只立一个皇后](../Page/后黎朝.md "wikilink")。

[阮朝不设皇后](../Page/阮朝.md "wikilink")，最高是[皇貴妃](../Page/皇貴妃.md "wikilink")。当他的儿子成为皇帝后，她被尊为[皇太后](../Page/皇太后.md "wikilink")，死后得到皇后谥号（除[南芳皇后例外](../Page/南芳皇后.md "wikilink")）。

### 日本

[日本天皇之正妻也和中國一般稱之為皇后](../Page/日本天皇.md "wikilink")，而自[平安時代的](../Page/平安時代.md "wikilink")[一條天皇以後](../Page/一條天皇.md "wikilink")，因權臣使然，開了**一帝二-{后}-**的先例，而自一條天皇以後的歷代天皇，在冊立皇后時大多先冊為[中宮](../Page/中宮.md "wikilink")，之後立第二位皇后時，便將此前的中宮升為名義上地位較高的皇后，而新冊的第二位皇后仍稱中宮。
<small>（詳細解釋請參閱 **[中宮](../Page/中宮.md "wikilink")**）</small>

自一條天皇首開兩-{后}-的先河後，後世的[後冷泉天皇](../Page/後冷泉天皇.md "wikilink")，甚至**一帝三-{后}-**，此時除去前兩位稱之為皇后與中宮之外，第三位稱之為[皇太后](../Page/皇太后.md "wikilink")。而自平安時代至[鐮倉時代初期](../Page/鐮倉時代.md "wikilink")，皇后成為一種榮銜，不時用以封給天皇的姑母、叔母、准母與同母未婚姐妹，且受封者不需與天皇有夫妻之實。

由於冊立皇后或中宮，都需要大量的金錢來操辦冊封典禮，且皇后或中宮的生活開銷及歲奉，也都是可觀的數目，而皇室自[室町時代以後逐漸式微](../Page/室町時代.md "wikilink")、財政窮困，很多天皇的登基儀式——[大嘗祭](../Page/大嘗祭.md "wikilink")，都是在極為困窘的狀況下舉行，因此多位天皇都不再冊立中宮或皇后，直至[江戶幕府二代將軍](../Page/江戶幕府.md "wikilink")[德川秀忠之女](../Page/德川秀忠.md "wikilink")[德川和子](../Page/德川和子.md "wikilink")、成為[後水尾天皇皇后以後](../Page/後水尾天皇.md "wikilink")，為維持德川和子在宮內體面的生活，幕府增加了每年對朝廷的歲貢，但仍有幾位天皇只設[女御](../Page/女御.md "wikilink")、不設皇后或中宮，主因還是皇室財政匱乏。

日本自[二戰之後取消了](../Page/二戰.md "wikilink")[女院制](../Page/女院.md "wikilink")，而出現了首位以皇后之號為[諡號的皇后](../Page/諡號.md "wikilink")，為第123代[大正天皇之皇后](../Page/大正天皇.md "wikilink")[貞明皇后](../Page/貞明皇后.md "wikilink")。目前在位的日本皇后是第125代今上天皇[明仁的皇后](../Page/明仁.md "wikilink")[美智子](../Page/美智子.md "wikilink")，首開先例地以平民之身成為皇后。

### 朝鮮

[朝鮮王朝為中國之](../Page/朝鮮王朝.md "wikilink")[藩國](../Page/藩國.md "wikilink")，因此其[君主只能稱](../Page/君主.md "wikilink")[國王而非](../Page/國王.md "wikilink")[皇帝](../Page/皇帝.md "wikilink")，相對的，國王之正妻也只能稱**王妃**而不是皇后。

國王正妻稱王妃，如下列：

  - 當朝國王正妻：王妃
  - 前任國王正妻：[王大妃](../Page/王大妃.md "wikilink")
  - 前前任國王正妻：[大王大妃](../Page/大王大妃.md "wikilink")

但若是宮中有三代以上前代王妃共存時；

  - 當朝國王正妻：王妃
  - 前任國王正妻：大妃
  - 前前任國王正妻：[王大妃](../Page/王大妃.md "wikilink")
  - 前前前任國王正妻：大王大妃

朝鮮王妃對於管理後宮有很高的決定權，通常國王或王大妃亦不得干涉，而後宮[嬪御的](../Page/嬪御.md "wikilink")[徽號和](../Page/徽號.md "wikilink")[牒紙也是由王妃擬定](../Page/牒紙.md "wikilink")、頒下。王妃死後以**王后**之名為諡號。
最後一位生前以王妃之名登位的國王正室是高宗的王妃——明成皇后，[大韓帝國成立以後](../Page/大韓帝國.md "wikilink")，開國皇帝[高宗](../Page/朝鮮高宗.md "wikilink")，追諡了部分先祖為[皇帝及](../Page/皇帝.md "wikilink")**皇后**，而大韓帝國獨立期間，只有一位皇后，為[純宗的继室](../Page/朝鮮純宗.md "wikilink")[純貞孝皇后尹氏](../Page/純貞孝皇后.md "wikilink")。

## 欧洲

欧洲国家的[语言中](../Page/语言.md "wikilink")，皇后与[女皇帝是同一个单词](../Page/女皇帝.md "wikilink")。例如[英语中为了区别两者](../Page/英语.md "wikilink")，在empress加上consort（王配）表示是男皇帝的配偶，即皇后（empress
consort）。加上regnant（佔统治地位的）表示女皇帝（empress
regnant）。[法语中](../Page/法语.md "wikilink")，则是在“皇帝”（empereur）一词前加femme（[女性](../Page/女性.md "wikilink")）以示区别。

## 廢后、再婚

歷史上有許多皇后被廢。在[一夫多妻國家中](../Page/一夫多妻.md "wikilink")，因為某些原因，如君主對皇后感情淡薄、皇后無子、皇后族屬犯罪、皇后本人犯罪等等原因，被廢的皇后或授予較低級別的位號、或軟禁在宮中而沒有任何稱號、或[出家](../Page/出家.md "wikilink")（實際上不能在宮外居住，大多在宮中落髮修行），也有被賜死的例子。通常废后没有再次结婚的自由。但在国家动乱时，废后会因种种原因而再婚。[东晋](../Page/东晋.md "wikilink")[南北朝时](../Page/南北朝.md "wikilink")，时局动荡，皇后和[妃嫔再婚并不罕见](../Page/妃嫔.md "wikilink")。[羊献容是](../Page/羊献容.md "wikilink")[晋惠帝的第二任皇后](../Page/晋惠帝.md "wikilink")，后成为[前赵](../Page/前赵.md "wikilink")[末帝刘曜的皇后](../Page/刘曜.md "wikilink")。也有的废后因政局动荡等原因重新获得皇后地位，如[北宋](../Page/北宋.md "wikilink")[宋哲宗废后](../Page/宋哲宗.md "wikilink")[孟氏曾被小叔子宋徽宗恢复皇后地位](../Page/孟皇后.md "wikilink")，后又因政局变动被再废，但在[南宋初年](../Page/南宋.md "wikilink")[金朝南侵之际](../Page/金朝.md "wikilink")，又作为[宋高宗的伯母被尊为皇太后](../Page/宋高宗.md "wikilink")。

有的皇后则并非在丈夫生前被废，而是在成为皇太后之后，因政治斗争失势等原因被废。

在[一夫一妻制國家](../Page/一夫一妻.md "wikilink")，也有一些皇后或王后被君主強制[離婚或於死後褫奪皇后或王后銜頭](../Page/離婚.md "wikilink")。

## 参考文献

## 參見

  - [女皇](../Page/女皇.md "wikilink")
  - [王后](../Page/王后.md "wikilink")
  - [正妃](../Page/正妃.md "wikilink")
  - [中宮](../Page/中宮.md "wikilink")
  - [尊稱皇后](../Page/尊稱皇后.md "wikilink")
  - [妃](../Page/妃.md "wikilink")
  - [中国皇后及妃嫔列表](../Page/中国皇后及妃嫔列表.md "wikilink")

## 相關

  - [皇帝](../Page/皇帝.md "wikilink")
  - [国王](../Page/国王.md "wikilink")
  - \-{[后](../Page/后.md "wikilink")}-
  - [王后](../Page/王后.md "wikilink")
  - [嬪妃](../Page/嬪妃.md "wikilink")
  - [皇太后](../Page/皇太后.md "wikilink")
  - [太皇太后](../Page/太皇太后.md "wikilink")
  - [女皇帝](../Page/女皇帝.md "wikilink")
  - [女王](../Page/女王.md "wikilink")
  - [國母](../Page/國母.md "wikilink")

[en:Empress of China](../Page/en:Empress_of_China.md "wikilink")

[皇后](../Category/皇后.md "wikilink")