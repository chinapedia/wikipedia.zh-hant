[Jung_Chang,_2010_(cropped).jpg](https://zh.wikipedia.org/wiki/File:Jung_Chang,_2010_\(cropped\).jpg "fig:Jung_Chang,_2010_(cropped).jpg")

**張戎**（英文名：，），原名**张二鸿**，是[英國籍](../Page/英國.md "wikilink")[華裔](../Page/華裔.md "wikilink")[作家](../Page/作家.md "wikilink")，現居[倫敦](../Page/倫敦.md "wikilink")。

## 生平

1952年生於[中國](../Page/中國.md "wikilink")[四川](../Page/四川.md "wikilink")[宜賓](../Page/宜賓.md "wikilink")，外祖父是军事将领[薛之珩](../Page/薛之珩.md "wikilink")，父母是共產黨幹部，[文革时是](../Page/文革.md "wikilink")[红卫兵](../Page/红卫兵.md "wikilink")、农民、[赤脚医生](../Page/赤脚医生.md "wikilink")、[翻砂工](../Page/翻砂工.md "wikilink")、[电工](../Page/电工.md "wikilink")。26歲時出國到英國留學，曾於[四川大學擔任助理講師](../Page/四川大學.md "wikilink")。1971年，被调入[成都公私合營工厂](../Page/成都.md "wikilink")。1973年，进入[四川大学外语系成为](../Page/四川大学.md "wikilink")[工农兵大学生](../Page/工农兵大学生.md "wikilink")。毕业后留校任教。1978年，获得公费资助到英国留学。她在[英國約克大學取得博士學位](../Page/英國約克大學.md "wikilink")，是[中華人民共和國建國後首位獲英國大學博士學位的中国公民](../Page/中華人民共和國.md "wikilink")。

2018年，作家張戎被禁止出席澳門文學節\[1\]。

## 著作

張戎著作有傳記作品《[慈禧：開啟現代中國的皇太后](../Page/慈禧：開啟現代中國的皇太后.md "wikilink")》（2013年）、《[毛澤東：鮮為人知的故事](../Page/毛澤東：鮮為人知的故事.md "wikilink")》（2005年，與丈夫[喬·哈利戴合著](../Page/喬·哈利戴.md "wikilink"))、《孫逸仙夫人宋慶齡傳》（1986年），和得獎自傳《[鸿：三代中国女人的故事](../Page/鸿：三代中国女人的故事.md "wikilink")》（1991年）。

## 外部連結

  - 英国广播电台BBC中文网《中国丛谈》特辑（[上](http://news.bbc.co.uk/chinese/simp/hi/newsid_4630000/newsid_4638100/4638107.stm)／[下](http://news.bbc.co.uk/chinese/simp/hi/newsid_4650000/newsid_4658500/4658505.stm)）
  - BBCChinese
    采访录音(使用RealPlayer打开)（[1/2](http://news.bbc.co.uk/media/audio/41246000/rm/_41246539_28.ram)／[2/2](http://news.bbc.co.uk/media/audio/41246000/rm/_41246435_29.ram)）
  - [希望之声](https://web.archive.org/web/20070927185043/http://soundofhope.org/programs/525/42719-1.asp)
  - [张戎法拉盛图书馆演讲原声(mp3)](http://media2.soundofhope.org/16K-mp3/audio01/2006/7/14/zhangrong-total.mp3)
  - [德国之声：张戎“一块块拆卸毛泽东神话”](http://www.dw-world.de/dw/article/0,1564,1616803,00.html)

[category:宜宾人](../Page/category:宜宾人.md "wikilink")

[Category:英国约克大学校友](../Category/英国约克大学校友.md "wikilink")
[Category:西伦敦大学校友](../Category/西伦敦大学校友.md "wikilink")
[Category:中国现代作家](../Category/中国现代作家.md "wikilink")
[Category:英国华人作家](../Category/英国华人作家.md "wikilink")
[Category:归化英国公民的中华人民共和国人](../Category/归化英国公民的中华人民共和国人.md "wikilink")
[Category:四川大學校友](../Category/四川大學校友.md "wikilink")
[Category:中華人民共和國歷史學家](../Category/中華人民共和國歷史學家.md "wikilink")
[Category:英国历史学家](../Category/英国历史学家.md "wikilink")
[R](../Category/張姓.md "wikilink")

1.