**黃仲圖**（），字河軒，生於[日治臺灣](../Page/日治臺灣.md "wikilink")[斗六廳](../Page/斗六廳.md "wikilink")[林圮埔支廳羌仔藔區](../Page/林圮埔支廳.md "wikilink")（今[南投縣](../Page/南投縣.md "wikilink")[鹿谷鄉](../Page/鹿谷鄉.md "wikilink")），[台灣政治人物](../Page/台灣.md "wikilink")，為[中國國民黨籍](../Page/中國國民黨.md "wikilink")。於1946年接替[連謀派任擔任](../Page/連謀.md "wikilink")[高雄市市長一職](../Page/高雄市_\(省轄市\).md "wikilink")。

## 生平

其父[黃錫三](../Page/黃錫三.md "wikilink")，清朝時期[秀才](../Page/秀才.md "wikilink")，出身南投竹山，為地方領袖。其兄[黃式鴻](../Page/黃式鴻.md "wikilink")。

黃仲圖畢業於[臺南師範](../Page/臺南師範.md "wikilink")（今[國立臺南大學的前身](../Page/國立臺南大學.md "wikilink")），之後就讀日本[東洋大學](../Page/東洋大學.md "wikilink")。畢業後，與其兄黃式鴻赴中國浙江，任職浙江嘉興民眾教育館嘉善分部。

1937年，對日抗戰爆發，隨國民政府後撤至重慶。曾任[浙江大學教授](../Page/浙江大學.md "wikilink")、第八戰區幹訓團總教官、政治部部附等職。

1945年，第二次世界大戰結束，黃仲圖隨國民政府回到台灣，從事接收工作。後被指派為高雄市長。

1947年[二二八事件時](../Page/二二八事件.md "wikilink")，黃仲圖曾在3月6日與議長[彭清靠](../Page/彭清靠.md "wikilink")、[涂光明與苓雅區長林界台灣電力公司高雄辦事處主任](../Page/涂光明.md "wikilink")[李佛續等人前往壽山的要塞司令部](../Page/李佛續.md "wikilink")，會見要塞司令[彭孟緝](../Page/彭孟緝.md "wikilink")，希望禁止巡邏隊再射擊民眾。然而卻全數遭到逮捕，只有李佛續、黃仲圖、彭清靠三人獲釋下山。其他人先後遇害。

同年6月，受聘為[國立臺灣大學外文系副教授](../Page/國立臺灣大學.md "wikilink")，教授日文，並出任系主任。8月，黃強接任高雄市長。

1950年，黃仲圖卸下系主任職務，任台灣大學總務長，直到1953年卸任。1958年，升任教授。1976年，自台灣大學退休。1988年去世。

## 參見

  - [高雄市](../Page/高雄市.md "wikilink")

## 參考文獻

  - 劉寧顏編，《重修台灣省通志》，台北市，台灣省文獻委員會，1994年。

[Category:國立臺灣大學總務長](../Category/國立臺灣大學總務長.md "wikilink")
[Category:國立臺灣大學教授](../Category/國立臺灣大學教授.md "wikilink")
[Category:高雄市市長](../Category/高雄市市長.md "wikilink")
[Category:中國國民黨黨員](../Category/中國國民黨黨員.md "wikilink")
[Category:二二八事件相關人物](../Category/二二八事件相關人物.md "wikilink")
[Category:浙江大學教授](../Category/浙江大學教授.md "wikilink")
[Category:東洋大學校友](../Category/東洋大學校友.md "wikilink")
[Category:國立臺南大學校友](../Category/國立臺南大學校友.md "wikilink")
[Category:半山](../Category/半山.md "wikilink")
[Category:鹿谷人](../Category/鹿谷人.md "wikilink")
[Zhong仲圖](../Category/黃姓.md "wikilink")