**六四天网**是[黄琦创办的一家关注中国](../Page/黄琦.md "wikilink")[人权状况的](../Page/人权.md "wikilink")[网站](../Page/网站.md "wikilink")，于1999年上线，总部位于中国[四川](../Page/四川.md "wikilink")[成都](../Page/成都.md "wikilink")，被视为[中国大陆网络异见的先驱](../Page/中国大陆.md "wikilink")。\[1\]由于网站刊登的内容涉及政治敏感和批评时局的信息，引发中国政府不满，因而黄琦曾多次被中国公安拘捕，带走询问。\[2\]

## 简介

20世纪末，六四天网率先在网络上公开发布反映[法轮功人士人权遭到侵犯的报道](../Page/法轮功.md "wikilink")\[3\]，率先在网上公开反映六四死难者的案例[《11年来，孩子依旧半睁着双眼看着世界》](http://64tianwang.com/Article/Class1/200603/20060306052312.html)等大量人权案件。

，六四天网由海外天网义工维护。

2006年，黄琦出狱后重建六四天网。2006年4月，天网恢复运营，但遭到数十次攻击。8月18日，[北京时间凌晨](../Page/北京时间.md "wikilink")1点30分左右，网站突然无法浏览，10小时后接到通知“永久关闭服务器”。

目前，六四天网在境外的服务器独立运行，每天报道大量人权案件与新闻。同时，网站也独家发布[中国天网人权事务中心在中国大陆介入的上千起维权案件](../Page/中国天网人权事务中心.md "wikilink")。

2016年11月28日，黄琦在家中被警方带走，有访民对[自由亚洲电台表示黄琦已被刑事拘留](../Page/自由亚洲电台.md "wikilink")，并被羁押于[绵阳看守所](../Page/绵阳.md "wikilink")。\[4\]

## 参见

  - [中华人民共和国被封锁网站列表](../Page/中华人民共和国被封锁网站列表.md "wikilink")

## 参考资料

## 外部链接

  - [六四天网官方网站](http://64tianwang.com/)
  - [专访六四天网负责人黄琦](http://www.rfa.org/mandarin/zhuanlan/butongdeshengyin/2006/08/31/huangqi/)
    - RFA

[Category:六四事件](../Category/六四事件.md "wikilink")
[Category:被防火长城封锁的网站](../Category/被防火长城封锁的网站.md "wikilink")
[Category:中華人民共和國人權](../Category/中華人民共和國人權.md "wikilink")
[Category:中国维权运动](../Category/中国维权运动.md "wikilink")
[Category:中国网站](../Category/中国网站.md "wikilink")
[Category:1999年建立的网站](../Category/1999年建立的网站.md "wikilink")

1.  [谷歌:中國不改言論審查就不會重返中國](http://www.voachinese.com/content/google-cheif-describe-china-internet-control-20131105/1783683.html),
    [美國之音](../Page/美國之音.md "wikilink"), 11.05.2013
2.  [美国之音 |
    中国两维权网站负责人被带走维稳](http://chinadigitaltimes.net/chinese/2016/10/%E7%BE%8E%E5%9B%BD%E4%B9%8B%E9%9F%B3-%E4%B8%AD%E5%9B%BD%E4%B8%A4%E7%BB%B4%E6%9D%83%E7%BD%91%E7%AB%99%E8%B4%9F%E8%B4%A3%E4%BA%BA%E8%A2%AB%E5%B8%A6%E8%B5%B0%E7%BB%B4%E7%A8%B3/)
3.  [北京，目击死亡](http://64tianwang.com/Article/Class1/200603/20060305235923.html)
4.