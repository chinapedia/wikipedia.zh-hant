**傣族**（[傣仂语](../Page/傣仂语.md "wikilink")：
；[傣那语](../Page/傣那语.md "wikilink")：ᥖᥭᥰ
；或），可以指称**泰老民族**，又可指称中国境内的泰老民族。傣族在[民族识别以前又被稱作](../Page/民族识别.md "wikilink")**泰族**。傣族是中国跨境民族之一，與[百濮及](../Page/百濮.md "wikilink")[百越中的滇越有關](../Page/百越.md "wikilink")，与[缅甸的](../Page/缅甸.md "wikilink")[掸族](../Page/掸族.md "wikilink")、[坎底傣](../Page/坎底傣.md "wikilink")、[老挝的主体民族](../Page/老挝.md "wikilink")[佬族和](../Page/佬族.md "wikilink")[泰国的主体民族](../Page/泰国.md "wikilink")[泰族](../Page/泰族.md "wikilink")（
），印度[阿萨姆](../Page/阿萨姆邦.md "wikilink")[阿豪姆人系出同源](../Page/阿豪姆人.md "wikilink")。

中国境内的傣族，包括有[傣泐](../Page/傣泐.md "wikilink")（云南西双版纳、普洱）、[傣阮](../Page/傣阮.md "wikilink")（云南西双版纳，有几个村寨是傣阮村寨，而傣阮的主体主要位于泰国北部，此外还有老挝北部、缅甸掸邦）、[傣龙](../Page/傣龙.md "wikilink")（即掸族，缅甸掸邦云南德宏）、[黑泰](../Page/黑泰.md "wikilink")（云南金平）、（云南普洱，的主体主要位于缅甸掸邦景栋）等等。[佤族对傣族称呼为](../Page/佤族.md "wikilink")“Siam”\[1\]。

泰老民族按照地区和文化的不同，有各自的称呼，可以划分为30多个群体，主要的是：
Shan（[掸族](../Page/掸族.md "wikilink")/傣龙）、Tai Ya（傣亚/花腰傣）、Tai
Nüa（傣讷/花腰傣）；Khamti、Tai Laing、Tai Phake、Tai
Aiton、Khamyang、Ahom、Turung（以上为7支为印度和印缅边境）
Laos（老族，老族又有6个支系老龙、老允、老康等等）、Lao
Isan（泰国伊桑地区老族）、Nyaw（泰国伊桑地区）、Phu Thai（普泰，泰国伊桑地区和老挝中部） Tai Dam（黑傣）、Tai
Dón（[白泰](../Page/白泰.md "wikilink")）、Tai
Daeng（[红泰](../Page/红泰.md "wikilink")）、Phuan（傣Phuan）、Thai
Song（松） Tai Yuan（[傣沅](../Page/傣沅.md "wikilink")）、Tai
Lü（[傣泐](../Page/傣泐.md "wikilink")）、Khun（） Siamese（暹罗泰族）、Southern
Thai（泰国南部泰族）、Khorat（克拉泰族）

## 分布

根據[中華人民共和國2010年人口普查](../Page/中华人民共和国第五次全国人口普查.md "wikilink")，傣族人口有1,261,311人，内地31个省份均有分布，列第19大民族。傣族主要聚居於[雲南省](../Page/雲南省.md "wikilink")[西双版纳傣族自治州和](../Page/西双版纳傣族自治州.md "wikilink")[德宏傣族景颇族自治州以及](../Page/德宏傣族景颇族自治州.md "wikilink")[耿马](../Page/耿马.md "wikilink")、[孟连](../Page/孟连.md "wikilink")、[新平](../Page/新平.md "wikilink")、[元江](../Page/元江哈尼族彝族傣族自治县.md "wikilink")、[景谷](../Page/景谷.md "wikilink")、[金平](../Page/金平.md "wikilink")、[双江等县](../Page/双江拉祜族佤族布朗族傣族自治县.md "wikilink")，在[保山](../Page/保山.md "wikilink")、[鎮沅](../Page/鎮沅.md "wikilink")、[瀾滄](../Page/澜沧拉祜族自治县.md "wikilink")、[元陽](../Page/元陽縣.md "wikilink")、[彌勒](../Page/彌勒縣.md "wikilink")、[馬關等地也有傣族民眾散居](../Page/馬關縣.md "wikilink")。\[2\]另外[四川省](../Page/四川省.md "wikilink")[凉山州](../Page/凉山彝族自治州.md "wikilink")[会理县](../Page/会理县.md "wikilink")[新安乡也是傣族聚居地](../Page/新安傣族乡.md "wikilink")。一部分南下到[越南](../Page/越南.md "wikilink")、[緬甸與](../Page/緬甸.md "wikilink")[老撾北部](../Page/老撾.md "wikilink")。

因他们需要暖湿气候种植[水稻](../Page/水稻.md "wikilink")，其聚居地位于海拔二千米以下，主要分布於雲南西南的河谷平原（[大盈江](../Page/大盈江.md "wikilink")、[瑞麗江](../Page/瑞麗江.md "wikilink")）。

### 傣族自治地方

  - [德宏傣族景颇族自治州](../Page/德宏傣族景颇族自治州.md "wikilink")
  - [西双版纳傣族自治州](../Page/西双版纳傣族自治州.md "wikilink")
  - [元江哈尼族彝族傣族自治县](../Page/元江哈尼族彝族傣族自治县.md "wikilink")
  - [新平彝族傣族自治县](../Page/新平彝族傣族自治县.md "wikilink")
  - [景谷傣族彝族自治县](../Page/景谷傣族彝族自治县.md "wikilink")
  - [孟连傣族拉祜族佤族自治县](../Page/孟连傣族拉祜族佤族自治县.md "wikilink")
  - [双江拉祜族佤族布朗族傣族自治县](../Page/双江拉祜族佤族布朗族傣族自治县.md "wikilink")

### 傣族乡

|                                                  |    |                                      |                                          |
| :----------------------------------------------: | :-: | :----------------------------------: | :--------------------------------------: |
|                       傣族乡                        | 省  |  [市州](../Page/地级行政区.md "wikilink")   |    [县区](../Page/县级行政区.md "wikilink")     |
|  [勐角傣族彝族拉祜族乡](../Page/勐角傣族彝族拉祜族乡.md "wikilink")  | 云南 |   [临沧市](../Page/临沧市.md "wikilink")   | [沧源佤族自治县](../Page/沧源佤族自治县.md "wikilink") |
|       [湾甸傣族乡](../Page/湾甸傣族乡.md "wikilink")       | 云南 |   [保山市](../Page/保山市.md "wikilink")   |     [昌宁县](../Page/昌宁县.md "wikilink")     |
|    [湾碧傣族傈僳族乡](../Page/湾碧傣族傈僳族乡.md "wikilink")    | 云南 | [楚雄州](../Page/楚雄彝族自治州.md "wikilink") |     [大姚县](../Page/大姚县.md "wikilink")     |
|    [船房傈僳族傣族乡](../Page/船房傈僳族傣族乡.md "wikilink")    | 云南 |   [丽江市](../Page/丽江市.md "wikilink")   |     [华坪县](../Page/华坪县.md "wikilink")     |
|    [石龙坝彝族傣族乡](../Page/石龙坝彝族傣族乡.md "wikilink")    | 云南 |   [丽江市](../Page/丽江市.md "wikilink")   |     [华坪县](../Page/华坪县.md "wikilink")     |
|    [新庄傈僳族傣族乡](../Page/新庄傈僳族傣族乡.md "wikilink")    | 云南 |   [丽江市](../Page/丽江市.md "wikilink")   |     [华坪县](../Page/华坪县.md "wikilink")     |
|     [平村彝族傣族乡](../Page/平村彝族傣族乡.md "wikilink")     | 云南 |   [临沧市](../Page/临沧市.md "wikilink")   |     [临翔区](../Page/临翔区.md "wikilink")     |
|     [芒宽彝族傣族乡](../Page/芒宽彝族傣族乡.md "wikilink")     | 云南 |   [保山市](../Page/保山市.md "wikilink")   |     [隆阳区](../Page/隆阳区.md "wikilink")     |
|     [龙潭彝族傣族乡](../Page/龙潭彝族傣族乡.md "wikilink")     | 云南 |   [普洱市](../Page/普洱市.md "wikilink")   |     [思茅区](../Page/思茅区.md "wikilink")     |
|     [荷花傣族佤族乡](../Page/荷花傣族佤族乡.md "wikilink")     | 云南 |   [保山市](../Page/保山市.md "wikilink")   |     [腾冲县](../Page/腾冲县.md "wikilink")     |
|       [东坡傣族乡](../Page/东坡傣族乡.md "wikilink")       | 云南 | [楚雄州](../Page/楚雄彝族自治州.md "wikilink") |     [武定县](../Page/武定县.md "wikilink")     |
| [大雪山彝族拉祜族傣族乡](../Page/大雪山彝族拉祜族傣族乡.md "wikilink") | 云南 |   [临沧市](../Page/临沧市.md "wikilink")   |     [永德县](../Page/永德县.md "wikilink")     |
|       [永兴傣族乡](../Page/永兴傣族乡.md "wikilink")       | 云南 | [楚雄州](../Page/楚雄彝族自治州.md "wikilink") |     [永仁县](../Page/永仁县.md "wikilink")     |
|     [栗树彝族傣族乡](../Page/栗树彝族傣族乡.md "wikilink")     | 云南 |   [临沧市](../Page/临沧市.md "wikilink")   |      [云县](../Page/云县.md "wikilink")      |
|     [高大傣族彝族乡](../Page/高大傣族彝族乡.md "wikilink")     | 云南 |   [玉溪市](../Page/玉溪市.md "wikilink")   |     [通海县](../Page/通海县.md "wikilink")     |
|       [新安傣族乡](../Page/新安傣族乡.md "wikilink")       | 四川 | [凉山州](../Page/凉山彝族自治州.md "wikilink") |     [会理县](../Page/会理县.md "wikilink")     |

### 中国傣族人口分布

<table>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><font size="2">位次</font><br />
</p></td>
<td style="text-align: center;"><p><font size="2">地区</font><br />
</p></td>
<td style="text-align: center;"><p><font size="2">总人口</font><br />
</p></td>
<td style="text-align: center;"><p><font size="2">傣族</font><br />
</p></td>
<td style="text-align: center;"><p><font size="2">占傣族<br />
人口比例（%）</font><br />
</p></td>
<td style="text-align: center;"><p><font size="2">占地区<br />
少数民族<br />
人口比例（%）</font><br />
</p></td>
<td style="text-align: center;"><p><font size="2">占地区<br />
人口比例（%）</font><br />
</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"></td>
<td style="text-align: center;"><p><font size="2">合计</font></p></td>
<td style="text-align: center;"><p><font size="2">1,245,110,826</font></p></td>
<td style="text-align: center;"><p><font size="2">1,159,231 </font></p></td>
<td style="text-align: center;"><p><font size="2">100</font></p></td>
<td style="text-align: center;"><p><font size="2">1.10049</font></p></td>
<td style="text-align: center;"><p><font size="2">0.09310</font></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"></td>
<td style="text-align: center;"><p><font size="2">31省份合计</font></p></td>
<td style="text-align: center;"><p><font size="2">1,242,612,226</font></p></td>
<td style="text-align: center;"><p><font size="2">1,158,989</font></p></td>
<td style="text-align: center;"><p><font size="2">100</font></p></td>
<td style="text-align: center;"><p><font size="2">1.10143</font></p></td>
<td style="text-align: center;"><p><font size="2">0.09327</font></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><font size="2">G1</font></p></td>
<td style="text-align: center;"><p><font size="2"><a href="../Page/中国西南地区.md" title="wikilink">西南地区</a></font></p></td>
<td style="text-align: center;"><p><font size="2">193,085,172</font></p></td>
<td style="text-align: center;"><p><font size="2">1,150,154</font></p></td>
<td style="text-align: center;"><p><font size="2">99.217</font></p></td>
<td style="text-align: center;"><p><font size="2">3.19090</font></p></td>
<td style="text-align: center;"><p><font size="2">0.59567</font></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><font size="2">G2</font></p></td>
<td style="text-align: center;"><p><font size="2"><a href="../Page/华东地区.md" title="wikilink">华东地区</a></font></p></td>
<td style="text-align: center;"><p><font size="2">358,849,244</font></p></td>
<td style="text-align: center;"><p><font size="2">4,283</font></p></td>
<td style="text-align: center;"><p><font size="2">0.369</font></p></td>
<td style="text-align: center;"><p><font size="2">0.17137</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00119</font></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><font size="2">G3</font></p></td>
<td style="text-align: center;"><p><font size="2"><a href="../Page/中南地区.md" title="wikilink">中南地区</a></font></p></td>
<td style="text-align: center;"><p><font size="2">350,658,477</font></p></td>
<td style="text-align: center;"><p><font size="2">3,528</font></p></td>
<td style="text-align: center;"><p><font size="2">0.304</font></p></td>
<td style="text-align: center;"><p><font size="2">0.01193</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00101</font></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><font size="2">G4</font></p></td>
<td style="text-align: center;"><p><font size="2"><a href="../Page/华北地区.md" title="wikilink">华北地区</a></font></p></td>
<td style="text-align: center;"><p><font size="2">145,896,933</font></p></td>
<td style="text-align: center;"><p><font size="2">682</font></p></td>
<td style="text-align: center;"><p><font size="2">0.059</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00782</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00047</font></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><font size="2">G5</font></p></td>
<td style="text-align: center;"><p><font size="2"><a href="../Page/中国东北地区.md" title="wikilink">东北地区</a></font></p></td>
<td style="text-align: center;"><p><font size="2">104,864,179</font></p></td>
<td style="text-align: center;"><p><font size="2">187</font></p></td>
<td style="text-align: center;"><p><font size="2">0.016</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00171</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00018</font></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><font size="2">G6</font></p></td>
<td style="text-align: center;"><p><font size="2"><a href="../Page/中国西北地区.md" title="wikilink">西北地区</a></font></p></td>
<td style="text-align: center;"><p><font size="2">89,258,221</font></p></td>
<td style="text-align: center;"><p><font size="2">155</font></p></td>
<td style="text-align: center;"><p><font size="2">0.013</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00089</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00017</font></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><font size="2">1</font></p></td>
<td style="text-align: center;"><p><font size="2"><a href="../Page/云南省.md" title="wikilink">云南</a></font></p></td>
<td style="text-align: center;"><p><font size="2">42,360,089</font></p></td>
<td style="text-align: center;"><p><font size="2">1,142,139</font></p></td>
<td style="text-align: center;"><p><font size="2">98.526</font></p></td>
<td style="text-align: center;"><p><font size="2">8.06663</font></p></td>
<td style="text-align: center;"><p><font size="2">2.69626</font></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><font size="2">2</font></p></td>
<td style="text-align: center;"><p><font size="2"><a href="../Page/四川省.md" title="wikilink">四川</a></font></p></td>
<td style="text-align: center;"><p><font size="2">82,348,296</font></p></td>
<td style="text-align: center;"><p><font size="2">6,642</font></p></td>
<td style="text-align: center;"><p><font size="2">0.573</font></p></td>
<td style="text-align: center;"><p><font size="2">0.16127</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00807</font></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><font size="2">3</font></p></td>
<td style="text-align: center;"><p><font size="2"><a href="../Page/山东省.md" title="wikilink">山东</a></font></p></td>
<td style="text-align: center;"><p><font size="2">89,971,789</font></p></td>
<td style="text-align: center;"><p><font size="2">1,797</font></p></td>
<td style="text-align: center;"><p><font size="2">0.155</font></p></td>
<td style="text-align: center;"><p><font size="2">0.28400</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00200</font></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><font size="2">4</font></p></td>
<td style="text-align: center;"><p><font size="2"><a href="../Page/广东省.md" title="wikilink">广东</a></font></p></td>
<td style="text-align: center;"><p><font size="2">85,225,007</font></p></td>
<td style="text-align: center;"><p><font size="2">1,256</font></p></td>
<td style="text-align: center;"><p><font size="2">0.108</font></p></td>
<td style="text-align: center;"><p><font size="2">0.09896</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00147</font></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><font size="2">5</font></p></td>
<td style="text-align: center;"><p><font size="2"><a href="../Page/江苏省.md" title="wikilink">江苏</a></font></p></td>
<td style="text-align: center;"><p><font size="2">73,043,577</font></p></td>
<td style="text-align: center;"><p><font size="2">1,083</font></p></td>
<td style="text-align: center;"><p><font size="2">0.093</font></p></td>
<td style="text-align: center;"><p><font size="2">0.41669</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00148</font></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><font size="2">6</font></p></td>
<td style="text-align: center;"><p><font size="2"><a href="../Page/广西壮族自治区.md" title="wikilink">广西</a></font></p></td>
<td style="text-align: center;"><p><font size="2">43,854,538</font></p></td>
<td style="text-align: center;"><p><font size="2">847</font></p></td>
<td style="text-align: center;"><p><font size="2">0.073</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00503</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00193</font></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><font size="2">7</font></p></td>
<td style="text-align: center;"><p><font size="2"><a href="../Page/贵州省.md" title="wikilink">贵州</a></font></p></td>
<td style="text-align: center;"><p><font size="2">35,247,695</font></p></td>
<td style="text-align: center;"><p><font size="2">755</font></p></td>
<td style="text-align: center;"><p><font size="2">0.065</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00566</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00214</font></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><font size="2">8</font></p></td>
<td style="text-align: center;"><p><font size="2"><a href="../Page/重庆市.md" title="wikilink">重庆</a></font></p></td>
<td style="text-align: center;"><p><font size="2">30,512,763</font></p></td>
<td style="text-align: center;"><p><font size="2">604</font></p></td>
<td style="text-align: center;"><p><font size="2">0.052</font></p></td>
<td style="text-align: center;"><p><font size="2">0.03060</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00198</font></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><font size="2">9</font></p></td>
<td style="text-align: center;"><p><font size="2"><a href="../Page/湖南省.md" title="wikilink">湖南</a></font></p></td>
<td style="text-align: center;"><p><font size="2">63,274,173</font></p></td>
<td style="text-align: center;"><p><font size="2">587</font></p></td>
<td style="text-align: center;"><p><font size="2">0.051</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00916</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00093</font></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><font size="2">10</font></p></td>
<td style="text-align: center;"><p><font size="2"><a href="../Page/浙江省.md" title="wikilink">浙江</a></font></p></td>
<td style="text-align: center;"><p><font size="2">45,930,651</font></p></td>
<td style="text-align: center;"><p><font size="2">540</font></p></td>
<td style="text-align: center;"><p><font size="2">0.047</font></p></td>
<td style="text-align: center;"><p><font size="2">0.13658</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00118</font></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><font size="2">11</font></p></td>
<td style="text-align: center;"><p><font size="2"><a href="../Page/河南省.md" title="wikilink">河南</a></font></p></td>
<td style="text-align: center;"><p><font size="2">91,236,854</font></p></td>
<td style="text-align: center;"><p><font size="2">500</font></p></td>
<td style="text-align: center;"><p><font size="2">0.043</font></p></td>
<td style="text-align: center;"><p><font size="2">0.04372</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00055</font></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><font size="2">12</font></p></td>
<td style="text-align: center;"><p><font size="2"><a href="../Page/安徽省.md" title="wikilink">安徽</a></font></p></td>
<td style="text-align: center;"><p><font size="2">58,999,948</font></p></td>
<td style="text-align: center;"><p><font size="2">373</font></p></td>
<td style="text-align: center;"><p><font size="2">0.032</font></p></td>
<td style="text-align: center;"><p><font size="2">0.09376</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00063</font></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><font size="2">13</font></p></td>
<td style="text-align: center;"><p><font size="2"><a href="../Page/北京市.md" title="wikilink">北京</a></font></p></td>
<td style="text-align: center;"><p><font size="2">13,569,194</font></p></td>
<td style="text-align: center;"><p><font size="2">265</font></p></td>
<td style="text-align: center;"><p><font size="2">0.023</font></p></td>
<td style="text-align: center;"><p><font size="2">0.04526</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00195</font></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><font size="2">14</font></p></td>
<td style="text-align: center;"><p><font size="2"><a href="../Page/上海市.md" title="wikilink">上海</a></font></p></td>
<td style="text-align: center;"><p><font size="2">16,407,734</font></p></td>
<td style="text-align: center;"><p><font size="2">246</font></p></td>
<td style="text-align: center;"><p><font size="2">0.021</font></p></td>
<td style="text-align: center;"><p><font size="2">0.23683</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00150</font></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><font size="2">15</font></p></td>
<td style="text-align: center;"><p><font size="2"><a href="../Page/河北省.md" title="wikilink">河北</a></font></p></td>
<td style="text-align: center;"><p><font size="2">66,684,419</font></p></td>
<td style="text-align: center;"><p><font size="2">229</font></p></td>
<td style="text-align: center;"><p><font size="2">0.020</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00789</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00034</font></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><font size="2">16</font></p></td>
<td style="text-align: center;"><p><font size="2"><a href="../Page/海南省.md" title="wikilink">海南</a></font></p></td>
<td style="text-align: center;"><p><font size="2">7,559,035</font></p></td>
<td style="text-align: center;"><p><font size="2">197</font></p></td>
<td style="text-align: center;"><p><font size="2">0.017</font></p></td>
<td style="text-align: center;"><p><font size="2">0.01500</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00261</font></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><font size="2">17</font></p></td>
<td style="text-align: center;"><p><font size="2"><a href="../Page/福建省.md" title="wikilink">福建</a></font></p></td>
<td style="text-align: center;"><p><font size="2">34,097,947</font></p></td>
<td style="text-align: center;"><p><font size="2">169</font></p></td>
<td style="text-align: center;"><p><font size="2">0.015</font></p></td>
<td style="text-align: center;"><p><font size="2">0.02895</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00050</font></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><font size="2">18</font></p></td>
<td style="text-align: center;"><p><font size="2"><a href="../Page/湖北省.md" title="wikilink">湖北</a></font></p></td>
<td style="text-align: center;"><p><font size="2">59,508,870</font></p></td>
<td style="text-align: center;"><p><font size="2">141</font></p></td>
<td style="text-align: center;"><p><font size="2">0.012</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00543</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00024</font></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><font size="2">19</font></p></td>
<td style="text-align: center;"><p><font size="2"><a href="../Page/辽宁省.md" title="wikilink">辽宁</a></font></p></td>
<td style="text-align: center;"><p><font size="2">41,824,412</font></p></td>
<td style="text-align: center;"><p><font size="2">104</font></p></td>
<td style="text-align: center;"><p><font size="2">0.009</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00155</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00025</font></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><font size="2">20</font></p></td>
<td style="text-align: center;"><p><font size="2"><a href="../Page/内蒙古自治区.md" title="wikilink">内蒙</a></font></p></td>
<td style="text-align: center;"><p><font size="2">23,323,347</font></p></td>
<td style="text-align: center;"><p><font size="2">96</font></p></td>
<td style="text-align: center;"><p><font size="2">0.008</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00198</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00041</font></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><font size="2">21</font></p></td>
<td style="text-align: center;"><p><font size="2"><a href="../Page/江西省.md" title="wikilink">江西</a></font></p></td>
<td style="text-align: center;"><p><font size="2">40,397,598</font></p></td>
<td style="text-align: center;"><p><font size="2">75</font></p></td>
<td style="text-align: center;"><p><font size="2">0.006</font></p></td>
<td style="text-align: center;"><p><font size="2">0.05966</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00019</font></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><font size="2">22</font></p></td>
<td style="text-align: center;"><p><font size="2"><a href="../Page/山西省.md" title="wikilink">山西</a></font></p></td>
<td style="text-align: center;"><p><font size="2">32,471,242</font></p></td>
<td style="text-align: center;"><p><font size="2">68</font></p></td>
<td style="text-align: center;"><p><font size="2">0.006</font></p></td>
<td style="text-align: center;"><p><font size="2">0.06592</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00021</font></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><font size="2">23</font></p></td>
<td style="text-align: center;"><p><font size="2"><a href="../Page/陕西省.md" title="wikilink">陕西</a></font></p></td>
<td style="text-align: center;"><p><font size="2">35,365,072</font></p></td>
<td style="text-align: center;"><p><font size="2">68</font></p></td>
<td style="text-align: center;"><p><font size="2">0.006</font></p></td>
<td style="text-align: center;"><p><font size="2">0.03854</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00019</font></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><font size="2">24</font></p></td>
<td style="text-align: center;"><p><font size="2"><a href="../Page/吉林省.md" title="wikilink">吉林</a></font></p></td>
<td style="text-align: center;"><p><font size="2">26,802,191</font></p></td>
<td style="text-align: center;"><p><font size="2">60</font></p></td>
<td style="text-align: center;"><p><font size="2">0.005</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00245</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00022</font></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><font size="2">25</font></p></td>
<td style="text-align: center;"><p><font size="2"><a href="../Page/新疆维吾尔自治区.md" title="wikilink">新疆</a></font></p></td>
<td style="text-align: center;"><p><font size="2">18,459,511</font></p></td>
<td style="text-align: center;"><p><font size="2">59</font></p></td>
<td style="text-align: center;"><p><font size="2">0.005</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00054</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00032</font></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><font size="2">26</font></p></td>
<td style="text-align: center;"><p><font size="2"><a href="../Page/天津市.md" title="wikilink">天津</a></font></p></td>
<td style="text-align: center;"><p><font size="2">9,848,731</font></p></td>
<td style="text-align: center;"><p><font size="2">24</font></p></td>
<td style="text-align: center;"><p><font size="2">0.002</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00899</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00024</font></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><font size="2">27</font></p></td>
<td style="text-align: center;"><p><font size="2"><a href="../Page/黑龙江省.md" title="wikilink">黑龙江</a></font></p></td>
<td style="text-align: center;"><p><font size="2">36,237,576</font></p></td>
<td style="text-align: center;"><p><font size="2">23</font></p></td>
<td style="text-align: center;"><p><font size="2">0.002</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00130</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00006</font></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><font size="2">28</font></p></td>
<td style="text-align: center;"><p><font size="2"><a href="../Page/西藏自治区.md" title="wikilink">西藏</a></font></p></td>
<td style="text-align: center;"><p><font size="2">2,616,329</font></p></td>
<td style="text-align: center;"><p><font size="2">14</font></p></td>
<td style="text-align: center;"><p><font size="2">0.001</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00057</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00054</font></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><font size="2">29</font></p></td>
<td style="text-align: center;"><p><font size="2"><a href="../Page/甘肃省.md" title="wikilink">甘肃</a></font></p></td>
<td style="text-align: center;"><p><font size="2">25,124,282</font></p></td>
<td style="text-align: center;"><p><font size="2">13</font></p></td>
<td style="text-align: center;"><p><font size="2">0.001</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00059</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00005</font></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><font size="2">30</font></p></td>
<td style="text-align: center;"><p><font size="2"><a href="../Page/宁夏回族自治区.md" title="wikilink">宁夏</a></font></p></td>
<td style="text-align: center;"><p><font size="2">5,486,393</font></p></td>
<td style="text-align: center;"><p><font size="2">11</font></p></td>
<td style="text-align: center;"><p><font size="2">0.001</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00058</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00020</font></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><font size="2">31</font></p></td>
<td style="text-align: center;"><p><font size="2"><a href="../Page/青海省.md" title="wikilink">青海</a></font></p></td>
<td style="text-align: center;"><p><font size="2">4,822,963</font></p></td>
<td style="text-align: center;"><p><font size="2">4</font></p></td>
<td style="text-align: center;"><p><font size="2">0.000</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00018</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00008</font></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"></td>
<td style="text-align: center;"><p><font size="2">现役军人</font></p></td>
<td style="text-align: center;"><p><font size="2">2,498,600</font></p></td>
<td style="text-align: center;"><p><font size="2">242</font></p></td>
<td style="text-align: center;"><p><font size="2">0.021</font></p></td>
<td style="text-align: center;"><p><font size="2">0.21664</font></p></td>
<td style="text-align: center;"><p><font size="2">0.00969</font></p></td>
</tr>
</tbody>
</table>

## 族源與歷史

傣族源于中國南方的[百越族群](../Page/百越.md "wikilink")，[西漢称其先民為](../Page/西漢.md "wikilink")[滇國](../Page/滇國.md "wikilink")，[東漢稱為](../Page/東漢.md "wikilink")“[掸](../Page/掸.md "wikilink")”，[唐](../Page/唐.md "wikilink")、[宋称黑齒蠻](../Page/宋.md "wikilink")、“
金齿蛮”、“銀齿蛮”、“繡面蛮”、茫蠻、繡脚，“白衣”等，[元](../Page/元.md "wikilink")、[明](../Page/明.md "wikilink")、[清及](../Page/清.md "wikilink")[民國時期称](../Page/民國.md "wikilink")“白夷”、“摆夷”、“百夷”。

他们在宋朝曾有自己的国家,叫[勐泐](../Page/勐泐.md "wikilink")(即車里宣慰司)，首都在[景洪](../Page/景洪.md "wikilink")。国主叫[叭真](../Page/叭真.md "wikilink")（帕雅真）。版圖至[蘭納](../Page/蘭納.md "wikilink")、[老撾與越南西北](../Page/老撾.md "wikilink")。有四子，小儿子管西双版纳，其他三人分别管泰国的清迈、越南西北、老挝的[丰沙里省](../Page/丰沙里省.md "wikilink")，他的王国生存至1950年。

而德宏一帶則有被稱為[麓川思氏的](../Page/麓川.md "wikilink")[德宏傣族地方政權](../Page/德宏傣族.md "wikilink")，由元末生存至明英宗時代。

## 語言文字

[1962-07_1962年_云南德宏傣族家庭.jpg](https://zh.wikipedia.org/wiki/File:1962-07_1962年_云南德宏傣族家庭.jpg "fig:1962-07_1962年_云南德宏傣族家庭.jpg")
[1962-07_1962年_云南德宏傣族老人.jpg](https://zh.wikipedia.org/wiki/File:1962-07_1962年_云南德宏傣族老人.jpg "fig:1962-07_1962年_云南德宏傣族老人.jpg")
[1962-07_1962年_云南德宏傣族青年庆祝开门节.jpg](https://zh.wikipedia.org/wiki/File:1962-07_1962年_云南德宏傣族青年庆祝开门节.jpg "fig:1962-07_1962年_云南德宏傣族青年庆祝开门节.jpg")
傣族说[德宏傣语](../Page/德宏傣语.md "wikilink")（傣那语）、[西双版纳傣语](../Page/西双版纳傣语.md "wikilink")（傣仂语）、[红金傣语](../Page/红金傣语.md "wikilink")、[金平傣语](../Page/金平傣语.md "wikilink")（傣端语）等多种傣语，都属于[壯侗語系的](../Page/壯侗語系.md "wikilink")[台语支](../Page/台语支.md "wikilink")。

傣族有自己的文字，分為四種形式：在[西雙版納等地通行的稱為](../Page/西雙版納.md "wikilink")**[傣仂文](../Page/傣仂文.md "wikilink")**﹐又稱**西雙版納傣文**﹔在[德宏等地通行的稱為](../Page/德宏.md "wikilink")**[傣哪文](../Page/傣哪文.md "wikilink")**﹐又稱**德宏傣文**﹔在[瑞麗](../Page/瑞麗.md "wikilink")﹑[瀾滄](../Page/瀾滄.md "wikilink")﹑[耿馬等縣市的部分地區使用的稱為](../Page/耿馬.md "wikilink")**[傣繃文](../Page/傣繃文.md "wikilink")**﹔在[金平使用的稱為](../Page/金平.md "wikilink")**[傣端文](../Page/傣端文.md "wikilink")**，又稱**金平傣文**。這四種傣文都是從[印度的](../Page/印度.md "wikilink")[婆羅米字母演變而來的](../Page/婆羅米字母.md "wikilink")﹐與[老撾文](../Page/老撾文.md "wikilink")﹑[泰文](../Page/泰文.md "wikilink")﹑[緬甸文](../Page/緬甸文.md "wikilink")﹑[高棉文屬于同一體系](../Page/高棉文.md "wikilink")。均為自左向右書寫﹐自上而下換行﹐但形體結構有所差異。\[3\]

## 民族服饰

傣族妇女多数[束髮](../Page/束髮.md "wikilink")，着窄袖短衫和长筒裙，但在[芒市等地婚前着短衫](../Page/芒市.md "wikilink")，束小围腰，婚后改为穿对襟短衫和黑色筒裙。男子多用白布或青布包头，上着短衫，下穿长裤，冬天冷时披毛毯。过去傣族人有[纹身的习俗](../Page/纹身.md "wikilink")。

## 风俗

  - [泼水节](../Page/泼水节.md "wikilink")
  - 傣族人结婚时要杀牛，并请全村人来到自家吃饭。酒席持续三天。贺者为示感谢，会说一些吉祥话，或送上红包。三天酒席结束后，新郎新娘正式结为夫妻。他們只種一次糧食在冬天结束農耕逸居。

## 参考文献

## 外部链接

  - [Photos related to Dai Theravada
    Buddhism](https://web.archive.org/web/20091224155102/http://picasaweb.google.com/walter.stanish/DaiTheravada)
  - [Site including information on some endangered Tai
    scripts](http://www.seasite.niu.edu/Tai/TaiDehong/)
  - [Xishuangbanna Tropical Botanical
    Garden](http://www.kepu.net.cn/english/banna/folk/fol107.html)

{{-}}

[傣族](../Category/傣族.md "wikilink")
[Category:台语民族](../Category/台语民族.md "wikilink")

1.
2.  中國少數民族分布圖集.中國地圖出版社，2002年：119-124頁. ISBN 7-5031-3001-6
3.  [中國大百科全書智慧藏——傣文條目](http://203.68.243.199/cpedia/Content.asp?ID=49964)