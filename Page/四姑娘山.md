**四姑娘山**位于[四川省西部](../Page/四川省.md "wikilink")[小金](../Page/小金县.md "wikilink")、[汶川两县间](../Page/汶川县.md "wikilink")，地处[邛崃山脉中段](../Page/邛崃山.md "wikilink")，毗邻[卧龙国家级自然保护区和](../Page/卧龙自然保护区.md "wikilink")[米亚罗自然保护区](../Page/米亚罗自然保护区.md "wikilink")，先后于1994年、1996年被[国务院批准建立](../Page/国务院.md "wikilink")“四姑娘山国家重点风景名胜区”和“小金四姑娘山国家级自然保护区”，属世界自然遗产“[四川大熊猫栖息地](../Page/四川大熊猫栖息地.md "wikilink")”的重要组成部分。

## 地理

四姑娘山由四座相连的山峰组成，主峰幺妹峰[海拔](../Page/海拔.md "wikilink")6,250米，有东方的[阿尔卑斯之称](../Page/阿尔卑斯.md "wikilink")，位于东经102.9度，北纬31.1度，是邛崃山脉最高峰。么妹峰南侧毗连的有三姑娘峰（海拔5,355米），二姑娘峰（海拔5,276米），大姑娘峰（海拔5,025米）三座山峰。

四姑娘山四座山峰中以[幺妹峰攀登难度最大](../Page/幺妹峰.md "wikilink")，1981年一支日本登山队完成首登。二峰和三峰属于登山入门级山峰，需具备一定的攀登技术。大峰攀登较为简单。登山者一般以从[日隆镇出发](../Page/日隆镇.md "wikilink")，经海子沟或长坪沟前往各山峰的大本营。

“四姑”本来是藏语的音译，后来当地的汉族人错以为“四姑”是汉语，并把这个名字改成“四姑娘”。

小金四姑娘山是[国家AAAA级风景旅游区](../Page/中国国家4A级旅游景区.md "wikilink")。整个景区由双桥沟、长坪沟、海子沟组成。
[Shuangqiaogou_overlook.jpg](https://zh.wikipedia.org/wiki/File:Shuangqiaogou_overlook.jpg "fig:Shuangqiaogou_overlook.jpg")

## 登山事故

  - 2008年9月，一搜狐网户外频道雇员和一当地向导在骆驼峰遇难。
  - 2009年6月，一中国职业登山向导、攀冰教练入长坪沟后失踪。
  - 2009年7月，一登山者在骆驼峰遭遇落石遇难。
  - 2009年10月，4名俄罗斯人遭遇雪崩，2人被埋。
  - 2010年8月，一名日本人在野人峰被落石击中。
  - 2011年10月，14名登山者在穿越时与外界失去联系长达13天。
  - 2012年1月，一名福建驴友在攀登三峰时被大风刮下坠崖遇难。
  - 2014年1月，一名广州驴友在双桥沟尖山子登山活动下撤途中，因岩钉脱落坠崖身亡。

## 参考文献

## 外部链接

  -
{{-}}

[Category:四川大熊猫栖息地](../Category/四川大熊猫栖息地.md "wikilink")
[川](../Category/中国世界遗产.md "wikilink")
[Category:四川旅游景点](../Category/四川旅游景点.md "wikilink")
[Category:四川山峰](../Category/四川山峰.md "wikilink")
[Category:阿坝旅游](../Category/阿坝旅游.md "wikilink")
[Category:阿坝地理](../Category/阿坝地理.md "wikilink")
[Category:小金县](../Category/小金县.md "wikilink")
[Category:汶川县](../Category/汶川县.md "wikilink")
[Category:国家4A级旅游景区](../Category/国家4A级旅游景区.md "wikilink")