[伯特倫·凡·芒斯特](伯特倫·凡·芒斯特.md "wikilink")（創作人、導演）
**季度：** [1](极速前进1.md "wikilink"){{·}} [2](极速前进2.md "wikilink"){{·}}
[3](极速前进3.md "wikilink"){{·}} [4](极速前进4.md "wikilink"){{·}}
[5](极速前进5.md "wikilink"){{·}} [6](极速前进6.md "wikilink"){{·}}
[7](极速前进7.md "wikilink"){{·}} [8](极速前进8.md "wikilink"){{·}}
[9](极速前进9.md "wikilink"){{·}} [10](极速前进10.md "wikilink"){{·}}
[11](极速前进11.md "wikilink"){{·}} [12](极速前进12.md "wikilink"){{·}}
[13](极速前进13.md "wikilink"){{·}} [14](极速前进14.md "wikilink"){{·}}
[15](极速前进15.md "wikilink"){{·}} [16](极速前进16.md "wikilink"){{·}}
[17](极速前进17.md "wikilink"){{·}} [18](极速前进18.md "wikilink"){{·}}
[19](极速前进19.md "wikilink"){{·}} [20](极速前进20.md "wikilink") {{·}}
[21](极速前进21.md "wikilink") {{·}} [22](极速前进22.md "wikilink"){{·}}
[23](极速前进23.md "wikilink"){{·}} [24](极速前进24.md "wikilink"){{·}}
[25](极速前进25.md "wikilink") {{·}}[26](极速前进26.md "wikilink"){{·}}
[27](极速前进27.md "wikilink"){{·}} [28](极速前进28.md "wikilink") {{·}}
[29](极速前进29.md "wikilink"){{·}} [30](极速前进30.md "wikilink")

| group2 = [亞洲版](极速前进亚洲版.md "wikilink") | list2 = **人物：**
[吳振天](吳振天.md "wikilink")（主持）
**季度：**
[1](极速前进亚洲版1.md "wikilink"){{·}}[2](极速前进亚洲版2.md "wikilink"){{·}}[3](极速前进亚洲版3.md "wikilink"){{·}}[4](极速前进亚洲版4.md "wikilink"){{·}}[5](极速前进亚洲版5.md "wikilink")

| group3 = 中國版 | list3 = [冲刺！中国](极速前进：冲刺！中国1.md "wikilink")
**人物：**吳振天（主持）
**季度：**[1](极速前进：冲刺！中国1.md "wikilink"){{·}}
[2](极速前进：冲刺！中国2.md "wikilink"){{·}}
[3](极速前进：冲刺！中国3.md "wikilink")
**[全明星中国版](极速前进中国版.md "wikilink")**
**人物：**吳振天、[安志杰](安志杰.md "wikilink")（主持）
**季度：**[1](极速前进中国版1.md "wikilink"){{·}} [2](极速前进中国版2.md "wikilink"){{·}}
[3](极速前进中国版3.md "wikilink"){{·}} [4](极速前进中国版4.md "wikilink")

| group4 = 其他版本 | list4 = [巴西版](极速前进巴西版.md "wikilink"){{·}}
[以色列版](极速前进以色列版.md "wikilink")（[1](极速前进以色列版.md "wikilink"){{·}}
[2](极速前进以色列版2.md "wikilink"){{·}}[3](极速前进以色列版3.md "wikilink"){{·}}[4](极速前进以色列版4.md "wikilink")）{{·}}
[拉丁美洲版](极速前进拉丁美洲版.md "wikilink")（[1](极速前进拉丁美洲版.md "wikilink"){{·}}
[2](极速前进拉丁美洲版2.md "wikilink"){{·}}
[3](极速前进拉丁美洲版3.md "wikilink"){{·}}
[4](极速前进拉丁美洲版4.md "wikilink")）{{·}}
[-{zh-hans:澳大利亚版;zh-hk:澳洲版;zh-tw:澳洲版;}-](极速前进澳洲版.md "wikilink")（[1](极速前进澳洲版.md "wikilink"){{·}}
[2](极速前进澳洲版2.md "wikilink"){{·}}
[-{zh-hans:澳大利亚对战新西兰;zh-hk:3;zh-tw:3;}-](极速前进澳洲版3.md "wikilink")）{{·}}
[挪威版](极速前进挪威版.md "wikilink")（[1](极速前进挪威版.md "wikilink"){{·}}
[2](极速前进挪威版2.md "wikilink")）{{·}}
[菲律賓版](极速前进菲律宾版.md "wikilink"){{·}}[法国版](极速前进法国版.md "wikilink")
{{·}} [越南版](极速前进越南版.md "wikilink") {{·}}
[烏克蘭版](极速前进乌克兰版.md "wikilink"){{·}}[加拿大版](驚險大挑戰加拿大版.md "wikilink")（[1](极速前进加拿大版.md "wikilink"){{·}}[2](惊险大挑战加拿大版2.md "wikilink"){{·}}[3](极速前进加拿大版3.md "wikilink"){{·}}[4](极速前进加拿大版4.md "wikilink"){{·}}[5](极速前进加拿大版5.md "wikilink")）

| group5 = 相關節目 | list5 = [衝刺上海](衝刺上海.md "wikilink")
}}<noinclude></noinclude>

[Category:极速前进](../Category/极速前进.md "wikilink")
[Category:电视节目模板](../Category/电视节目模板.md "wikilink")