**PunBB**是个快速、轻量的[PHP架构](../Page/PHP.md "wikilink")[网络论坛](../Page/网络论坛.md "wikilink")，以[GNU的标准释放](../Page/GNU.md "wikilink")。它的首要原则为快速、轻巧、与其它论坛相比更少量的图像。PunBB比起其它论坛功能较少，但也因此更快速、更小巧，并正确的支持[XHTML与](../Page/XHTML.md "wikilink")[CSS架构页面](../Page/CSS.md "wikilink")。

此外，PunBB可在任何支持[PHP语言的](../Page/PHP.md "wikilink")[作业系统上运作](../Page/作业系统.md "wikilink")，PunBB可以在[MySQL](../Page/MySQL.md "wikilink")、[PostgreSQL](../Page/PostgreSQL.md "wikilink")、[SQLite等数据库下储存信息](../Page/SQLite.md "wikilink")。

## 历史

PunBB的领航者Rickard
Andersson，在他发现并没有任何论坛软件支持[XHTML之后](../Page/XHTML.md "wikilink")，开始撰写了此计划。速度、简洁是PunBB的首要宗旨，所以Rickard并不使它内建一些功能如个人信息、投票，以及档案附加等。这些功能已经以模组的方式释放。

PunBB最初的认知是'Pun'，因为pun是个双关语且意味着在布告栏发生什么事的意思。[1](https://web.archive.org/web/20060315015020/http://www.theadminzone.com/forums/showthread.php?t=16370)
这也确定了使用这名字，不会让PunBB被视为“简化的[phpBB](../Page/phpBB.md "wikilink")”。增加的'BB'是布告栏（Bulletin
Board）的意思。

PunBB 1.0在2003年8月释出。

2008年，PunBB被[俄羅斯公司Informer收購](../Page/俄羅斯.md "wikilink")，原團隊相繼離開并成立新的[FluxBB](../Page/FluxBB.md "wikilink")。

## PunBB 1.3

最新的版本是1.3，已經推出，此版本改进1.2.x的源代码，并有些新功能，如下列：

  - 拓展系統建立在以掛鉤技術，按一下就能安裝，不再需要修改討論區的程式碼。
  - 完整[UTF-8支援](../Page/UTF-8.md "wikilink")。
  - 可更加自定的風格、模板系統。
  - 新界面：[結構](../Page/XHTML.md "wikilink")、[CSS](../Page/CSS.md "wikilink")、語言包皆更新，結構助手也被新增。
  - 良好的設想連結。
  - 更佳地分割、合併主題。
  - 允許多重版主群組。
  - 搜尋機制不再遲鈍，extern.php改進成更全面性，並有針對每個群組、會員的選項。
  - 某些“壞”字眼會被安靜地剔除。
  - 免除額外的資料夾，README新增了。
  - 基於拓展系統的修正（hotfix）系統被新增用於快速修復漏洞。

## 链接

### 官方

  - [PunBB](http://www.punbb.org/)
  - [PunBB支持论坛](http://forums.punbb.org/)
  - [PunBB开发](https://web.archive.org/web/20060404230813/http://dev.punbb.org/)

### 非官方

  - [PunRes –
    模组与风格](https://web.archive.org/web/20060402215556/http://punres.org/)
  - [SpinkBB – PunBB界面](http://www.jsand.net/spinkbb/?lang=en)
  - [MyPunBB – 免费的PunBB空间](http://www.mypunbb.com)
  - [ezDIY.org - 中文PunBB支持站台](http://www.ezdiy.org)

[Category:PHP](../Category/PHP.md "wikilink")
[Category:自由网络论坛软件](../Category/自由网络论坛软件.md "wikilink")