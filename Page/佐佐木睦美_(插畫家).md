**佐佐木睦美**\[1\]（，），是出身於日本[北海道的男性](../Page/北海道.md "wikilink")[插圖畫家](../Page/插圖畫家.md "wikilink")。

主要為[美少女遊戲作](../Page/美少女遊戲.md "wikilink")[人物設計](../Page/人物設計.md "wikilink")、繪製[原畫](../Page/原畫.md "wikilink")。現時為自由身狀態。

## 主要作品

### 人物設計

  - [秋之回憶](../Page/秋之回憶.md "wikilink")
  - [秋之回憶2](../Page/秋之回憶2.md "wikilink")
  - [HAPPY☆LESSON](../Page/歡樂課程.md "wikilink")
  - [雙戀](../Page/雙戀.md "wikilink")
  - [Myself ; Yourself](../Page/Myself_;_Yourself.md "wikilink")
  - [Chaos;HEAd](../Page/Chaos;HEAd.md "wikilink")
  - [Chaos;HEAd
    LoveChu☆Chu\!](../Page/Chaos;HEAd_LoveChu☆Chu!.md "wikilink")
  - [マジカ★マジカ](../Page/マジカ★マジカ.md "wikilink")
  - [L@ve once](../Page/L@ve_once.md "wikilink")
  - [CHAOS;CHILD](../Page/CHAOS;CHILD.md "wikilink")

### 繪製插圖

  - [水瓶戰記](../Page/水瓶戰記.md "wikilink")

  -
  - （著：[清涼院流水](../Page/清涼院流水.md "wikilink")）

  - （著：清涼院流水）

### 畫集

  -
    [MediaWorks](../Page/MediaWorks.md "wikilink")2006年初版，ISBN
    978-4-8402-3295-1
    [台灣角川](../Page/台灣角川.md "wikilink")2007年8月9日初版第1刷發行，ISBN
    978-986-174-429-2
    該畫集是集合了由1998 - 2005年主要所畫的原畫，主要分為四部份：
      - [雙戀](../Page/雙戀.md "wikilink")
      - [秋之回憶與](../Page/秋之回憶.md "wikilink")[秋之回憶2](../Page/秋之回憶2.md "wikilink")
      - [HAPPY☆LESSON](../Page/歡樂課程.md "wikilink")
      - 其它原畫，包括其[同人原畫](../Page/同人.md "wikilink")。
    而當中的附錄中有標明所有原畫的出處。

<!-- end list -->

  -
    MediaWorks2008年初版，ISBN 978-4-8401-2200-9
    收錄上一畫集未收錄的插畫。

### 其他

  -
## 註釋

## 外部連結

  - [](http://www.ne.jp/asahi/hp/belfacs/) （本人營運的網站）

  -
[Category:日本插畫家](../Category/日本插畫家.md "wikilink")
[Category:電子遊戲繪圖及原畫家](../Category/電子遊戲繪圖及原畫家.md "wikilink")
[Category:同人創作者](../Category/同人創作者.md "wikilink")
[Category:北海道出身人物](../Category/北海道出身人物.md "wikilink")

1.  遊戲相關作品中文版代理商，如[智冠](../Page/智冠.md "wikilink")、[光譜及](../Page/光譜資訊.md "wikilink")[尖端等均採用的譯名](../Page/尖端.md "wikilink")。