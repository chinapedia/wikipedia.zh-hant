[Fushing_Township_luo-fu_bridge,TAIWAN.jpg](https://zh.wikipedia.org/wiki/File:Fushing_Township_luo-fu_bridge,TAIWAN.jpg "fig:Fushing_Township_luo-fu_bridge,TAIWAN.jpg")路段的羅浮橋（遠）及復興吊橋（近）\]\]
[Sanmin_River,TAIWAN.jpg](https://zh.wikipedia.org/wiki/File:Sanmin_River,TAIWAN.jpg "fig:Sanmin_River,TAIWAN.jpg")河谷\]\]
[Northern_Highway_Bai-Ji_Tunnel,TAIWAN.jpg](https://zh.wikipedia.org/wiki/File:Northern_Highway_Bai-Ji_Tunnel,TAIWAN.jpg "fig:Northern_Highway_Bai-Ji_Tunnel,TAIWAN.jpg")
[thumb](../Page/檔案:2014-08-13蘇樂橋側面.JPG.md "wikilink")
[遠眺北橫大曼橋.JPG](https://zh.wikipedia.org/wiki/File:遠眺北橫大曼橋.JPG "fig:遠眺北橫大曼橋.JPG")
**北橫公路**，全名**北部橫貫公路**，常簡稱為**北橫**，是[臺灣北部一條連接起東](../Page/北臺灣.md "wikilink")、西岸的橫貫公路，與[中橫](../Page/中橫.md "wikilink")、[南橫同為負擔起](../Page/南橫.md "wikilink")「東西橫貫公路」之運輸功能，並列為「台灣三大橫貫公路」。

## 路線

北橫公路在[公路總局編制之中](../Page/公路總局.md "wikilink")，被納編為[台七線](../Page/台7線.md "wikilink")。西起於[桃園市](../Page/桃園市.md "wikilink")[大溪區](../Page/大溪區.md "wikilink")，東迄於[宜蘭縣](../Page/宜蘭縣.md "wikilink")[大同鄉](../Page/大同鄉.md "wikilink")，全長約八十二公里。

## 歷史

北橫公路的前身為[日治時代的](../Page/台灣日治時期.md "wikilink")「角板山三星警備道」，或稱為「角板山三星間道路」。戰後，1963年5月，北部橫貫公路根據原本的舊道拓寬。

1966年5月28日，於當時的三峽鎮大埔村舉行通車典禮，由當時的副總統[嚴家淦親臨剪綵](../Page/嚴家淦.md "wikilink")，省政府主席[黃杰主持以及訓詞](../Page/黃杰.md "wikilink")，內容如下：

「本省因中央山脈橫亙南北，使交通受阻，六年以前[台灣省政府與退役官兵就業輔導委員會合作](../Page/台灣省政府.md "wikilink")，善用榮民的力量完成中部橫貫公路....五十二年五月，又開始建築北部橫貫公路之大同線，自台北大埔至宜蘭大同全長九十二公里，歷時三載，於本年四月完成。」\[1\]

## 沿線景點

  - [大溪陵寢](../Page/大溪陵寢.md "wikilink")
  - [慈湖陵寢](../Page/慈湖陵寢.md "wikilink")
  - [石門水庫](../Page/石門水庫_\(台灣\).md "wikilink")
  - [角板山](../Page/角板山.md "wikilink")
  - [小烏來瀑布](../Page/小烏來瀑布.md "wikilink")
  - [巴陵](../Page/巴陵_\(台灣\).md "wikilink")
  - [拉拉山](../Page/拉拉山.md "wikilink")
  - [明池](../Page/明池.md "wikilink")
  - [棲蘭](../Page/棲蘭.md "wikilink")

## 沿線地理資訊

\[2\]

<table>
<thead>
<tr class="header">
<th><p>行政區</p></th>
<th><p>地名</p></th>
<th><p>台7線<br />
里程數(公里)</p></th>
<th><p>海拔<br />
(公尺)</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/桃園市.md" title="wikilink">桃園市</a><a href="../Page/大溪區.md" title="wikilink">大溪區</a></p></td>
<td><p>頭寮</p></td>
<td><p>4.940</p></td>
<td></td>
<td><p>接桃59線處</p></td>
</tr>
<tr class="even">
<td><p>慈湖</p></td>
<td><p>7.648</p></td>
<td><p>247</p></td>
<td><p>接桃118線處</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>阿姆坪</p></td>
<td><p>9.593</p></td>
<td></td>
<td><p>接桃63線處</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/桃園市.md" title="wikilink">桃園市</a><a href="../Page/復興區.md" title="wikilink">復興區</a></p></td>
<td><p>三民</p></td>
<td><p>12.051</p></td>
<td></td>
<td><p>接台7乙線處</p></td>
</tr>
<tr class="odd">
<td><p>水源地</p></td>
<td><p>16.480</p></td>
<td><p>458</p></td>
<td><p>接桃117線處</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>羅浮</p></td>
<td><p>22.845</p></td>
<td><p>345</p></td>
<td><p>接縣118線處</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>榮華橋</p></td>
<td><p>32.694</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>高義橋</p></td>
<td><p>37.804</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>巴陵</p></td>
<td><p>48.029</p></td>
<td><p>648</p></td>
<td><p>接桃116線處</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>四稜</p></td>
<td><p>56.216</p></td>
<td><p>1167</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>西村</p></td>
<td><p>61.881</p></td>
<td><p>1106</p></td>
<td><p>桃市宜縣界處</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/宜蘭縣.md" title="wikilink">宜蘭縣</a><a href="../Page/大同鄉.md" title="wikilink">大同鄉</a></p></td>
<td><p>明池森林遊樂區</p></td>
<td><p>66.1</p></td>
<td><p>1150</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>棲蘭百韜橋</p></td>
<td><p>86.650</p></td>
<td><p>320</p></td>
<td><p>接台7甲線處</p></td>
<td></td>
</tr>
</tbody>
</table>

## 相關條目

  - [中橫公路](../Page/中橫公路.md "wikilink")
  - [新中橫公路](../Page/新中橫公路.md "wikilink")
  - [南橫公路](../Page/南橫公路.md "wikilink")

## 參考文獻

## 外部連結

[Category:桃園市道路](../Category/桃園市道路.md "wikilink")
[Category:宜蘭縣道路](../Category/宜蘭縣道路.md "wikilink")

1.  《聯合報》，1966年5月29日，第二版。
2.