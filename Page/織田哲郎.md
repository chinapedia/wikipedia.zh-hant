**織田
哲郎**，日本[作曲家](../Page/作曲家.md "wikilink")。本名**濱田哲郎**。[東京都出生](../Page/東京都.md "wikilink")。13歲的時候由於父親的工作的關係搬到[英國](../Page/英國.md "wikilink")[倫敦](../Page/倫敦.md "wikilink")，受到西洋樂極深影響。15歲回國，回國後住在父母的出生地[高知縣](../Page/高知縣.md "wikilink")[高知市](../Page/高知市.md "wikilink")。[東京都立大學附屬高級中學畢業](../Page/東京都.md "wikilink")，[明治學院大學除籍](../Page/明治學院大學.md "wikilink")。

## 人物

  - 大4歲的哥哥，在29歲時因[心臟病過世](../Page/心臟病.md "wikilink")。
  - 90年代日本流行音樂重要的人物，和[長戶大幸一起創立](../Page/長戶大幸.md "wikilink")[Being,Inc.](../Page/Being,Inc..md "wikilink")。
  - 自英國回日本後由於留學身份和由於父親工作關係經常轉學而受到欺負，一直抱有自殺傾向和患有憂鬱症，但在一次決定割腕自殺的時候聽到了[艾爾頓·約翰的](../Page/艾爾頓·約翰.md "wikilink")《YOUR
    SONG》而打消念頭。
  - 1979年和[長戶秀介](../Page/長戶秀介.md "wikilink")（[長戶大幸的弟弟](../Page/長戶大幸.md "wikilink")），吉他演奏家北島健二組成"WHY"。
  - 2000年在西班牙遇劫，當時他被搶匪勒頸造成聲帶疼痛，結果發現聲帶骨出現扭曲而影響發聲。後來雖然經過持續復健而恢復，但也僅能達到全盛期40%左右的聲量。而通常多以作曲家身分活動的織田，卻在這次事件之後，重新燃起了對歌唱的熱情。

## 專輯

  - Voices（1983年6月22日）
  - New Morning（1984年5月21日）
  - Night Waves（1985年8月21日）
  - Life（1986年4月21日）
  - Wildlife（1987年2月26日）
  - Ships（1987年8月26日）
  - Season（1988年5月21日）
  - Candle In The Rain（1989年3月21日）
  - いつかすべての閉ざされた扉が開かれる日まで（1990年4月21日）
  - Endless Dream（1992年6月24日）
  - Songs（1993年12月23日）
  - T（1993年12月23日）
  - Melodies（2006年9月20日）
  - One Night（2007年5月23日）
  - W FACE（2013年10月30日）

## 作曲

曾參與作曲給[ZARD](../Page/ZARD.md "wikilink")、[DEEN](../Page/DEEN.md "wikilink")、[TUBE](../Page/TUBE.md "wikilink")、[WANDS](../Page/WANDS.md "wikilink")、[V6](../Page/V6.md "wikilink")、[Dream](../Page/Dream.md "wikilink")、[Fairies](../Page/Fairies.md "wikilink")、[大黒摩季](../Page/大黒摩季.md "wikilink")、[Being,Inc.](../Page/Being,Inc..md "wikilink")、[近畿小子](../Page/近畿小子.md "wikilink")、[AKB48](../Page/AKB48.md "wikilink")、[鄧麗君](../Page/鄧麗君.md "wikilink")、[鄉廣美](../Page/鄉廣美.md "wikilink")、[上戶彩](../Page/上戶彩.md "wikilink")、[相川七瀨](../Page/相川七瀨.md "wikilink")、[都春美](../Page/都春美.md "wikilink")、[中村雅俊](../Page/中村雅俊.md "wikilink")、[近藤真彥](../Page/近藤真彥.md "wikilink")、[酒井法子](../Page/酒井法子.md "wikilink")、[渡邊謙](../Page/渡邊謙.md "wikilink")、[柳時元](../Page/柳時元.md "wikilink")、[深田恭子](../Page/深田恭子.md "wikilink")、[倖田來未等團體](../Page/倖田來未.md "wikilink")、歌手演唱。

## 匿名演唱作品

1983年，他曾以匿名歌手「TETSU」的名義，演唱過動畫《[裝甲騎兵](../Page/裝甲騎兵.md "wikilink")》的主題歌「」，後來並於2009年，為同作品的電影版推出了翻唱版本。

## 官方網站

  - [TETSURO ODA Official Web Site](http://www.t-oda.jp/)
  - [官方部落格](http://gree.jp/oda_tetsuro/)

[Category:日本音樂製作人](../Category/日本音樂製作人.md "wikilink")
[Category:日本男歌手](../Category/日本男歌手.md "wikilink")
[Category:日本創作歌手](../Category/日本創作歌手.md "wikilink")
[Category:日本作曲家](../Category/日本作曲家.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")
[Category:明治學院大學校友](../Category/明治學院大學校友.md "wikilink")