**TechCrunch**是一个聚焦[信息技术公司报道的新闻网站](../Page/信息技术.md "wikilink")，他们关注的公司大小各异，从创业公司到[纳斯达克指数所包含的顶尖企业都有涉及](../Page/纳斯达克.md "wikilink")。这个网站在2005年由[迈克尔·阿灵顿创始](../Page/迈克尔·阿灵顿.md "wikilink")。2010年9月28日，在美国旧金山举行的年度TechCrunch
Disrupt会议上，[美国在线宣布他们将会收购该网站](../Page/美国在线.md "wikilink")，收购金额据传是在2500-4000万美元之间。

2011年，由于美国在线对阿灵顿的一个新建投资基金CrunchFund进行了注资\[1\]，涉嫌违反新闻伦理，阿灵顿和一些创始的编辑退出了该网站。此后还有其他作者包括Paul
Carr\[2\]和Sarah Lacy\[3\]对此事件提起诉讼。

2013年8月8日，TechCrunch推出新的中文版网站，域名为http://techcrunch.cn。中文版的国内运营商是[动点科技](../Page/动点科技.md "wikilink")\[4\]。

## TechCrunch 網絡

TechCrunch 有數個附屬網站，故被稱為TechCrunch網絡，自2004年12月14日起包括以下網站：

  - [TechCrunch 法國](../Page/TechCrunch_法國.md "wikilink") - 由[Ouriel
    Ohayon](../Page/Ouriel_Ohayon.md "wikilink")
    編寫，2006年6月建立，主要翻譯TechCrunch 內容或一些原創內容。
  - [TechCrunch 日本](../Page/TechCrunch_日本.md "wikilink") - 翻譯自美國及一些原創內容。
  - TechCrunch UK - 現已關站。由Sam Sethi 編寫，內容主要針對英國的Web 2.0 服務，因為Sethi
    與Arrinton 和[Loic Le
    Meur之間的爭論而於](../Page/Loic_Le_Meur.md "wikilink")2006年12月13日關站。
  - [CrunchNotes](../Page/CrunchNotes.md "wikilink") - Michael Arrington
    的個人網誌，內容主要關於Web 2.0.
  - [MobileCrunch](../Page/MobileCrunch.md "wikilink") - 主要針對流動電訊工業的網誌。
  - [CrunchGear](../Page/CrunchGear.md "wikilink") -
    針對電子現意和電腦硬件的網誌，由John Biggs 編寫。
  - [TalkCrunch](../Page/TalkCrunch.md "wikilink") - 關於Web 2.0
    的播客，內容包裝訪問Web 2.0 公司或新產品發佈。
  - [CrunchBoard](../Page/CrunchBoard.md "wikilink") - Web 2.0 公司的招聘網站。
  - [CrunchBase](../Page/CrunchBase.md "wikilink") - 人人可编辑的Web
    2.0创业公司，人物及投资者的数据库

<!-- end list -->

  - [TechCrunch Disrupt](../Page/TechCrunch_Disrupt.md "wikilink") -
    是由TechCrunch每年举办的大会，始于2011年，分别在[旧金山](../Page/旧金山.md "wikilink"),\[5\]
    [纽约](../Page/纽约.md "wikilink")，\[6\]
    [北京](../Page/北京.md "wikilink"),\[7\]
    和[柏林](../Page/柏林.md "wikilink")\[8\]
    举办。在[风险投资商](../Page/风险投资.md "wikilink")，媒体和感兴趣的人们前，重点介绍一些新兴科技[创业公司](../Page/创业.md "wikilink")。\[9\]

## 中文版情况

2013年8月开始，中文版网站techcrunch.cn开始运营，其业务由动点科技承担\[10\]。除了翻译TC本身英文版内容，亦进行中国本土企业报道。

### 受侵权事件

由于中国大陆相对后发的科技媒体氛围，TechCrunch遭遇了一些侵权情况：

  - [36Kr](../Page/36氪.md "wikilink") 未授权翻译\[11\]
  - 虎嗅 在其应用内构造了一个未授权账号，标注为TechCrunch，具有误导性，并且存在其他侵权行为\[12\]
  - 腾讯科技 未授权翻译\[13\]\[14\]\[15\]

其中关于36Kr的未授权翻译行为，亦一度引发话题\[16\]。

### 平台审查

中文版TechCrunch除网页版，亦在其他社交媒体平台登录，如中国大陆流行的[微信公众号](../Page/微信.md "wikilink")。

2019年2月，[Reddit接受腾讯领投的融资](../Page/Reddit.md "wikilink")。TechCrunch英文版发表文章
\[17\]，次日中文版更新《腾讯不是 Reddit 的审查噩梦》\[18\]。

文章提及了中国大陆的审查机制，和大陆互联网巨头的实际运行，如微信对公众号的管理。该文在TechCrunch自己的微信公众号同步时，出现问题，运营者以大量字母缩写替代（可能的）[敏感词](../Page/敏感词.md "wikilink")（如SC-审查、ZF-政府、6FVV-[GFW等](../Page/GFW.md "wikilink")）\[19\]。由于内容与实际形成反差，被调侃为“TechCrunch中文版
的《腾讯不是Reddit的审查噩梦》一文正在接受腾讯审查”\[20\]。

## 參考資料

## 外部連結

  - [TechCrunch](http://www.techcrunch.com/)
  - [TechCrunch 中国](http://www.techcrunch.cn/)
  - [TechCrunch France](http://fr.techcrunch.com/)
  - [TechCrunch Japan](http://jp.techcrunch.com/)
  - [CrunchNotes](http://www.crunchnotes.com/)
  - [MobileCrunch](http://mobilecrunch.com/)
  - [CrunchGear](http://www.crunchgear.com/)
  - [CrunchBoard](http://www.crunchboard.com/)

[Category:2005年建立的网站](../Category/2005年建立的网站.md "wikilink")
[Category:技術網站](../Category/技術網站.md "wikilink")
[Category:網誌](../Category/網誌.md "wikilink") [Category:Web
2.0](../Category/Web_2.0.md "wikilink")
[Category:美國線上公司](../Category/美國線上公司.md "wikilink")

1.  <http://www.nytimes.com/2011/09/05/business/media/michael-arringtons-audacious-venture.html/>

2.

3.

4.

5.

6.

7.

8.

9.  <http://www.pcmag.com/article2/0,2817,2421359,00.asp>

10.
11.

12.

13.

14.

15.

16.

17.

18.

19.

20.