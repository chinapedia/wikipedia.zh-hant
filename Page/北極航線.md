[PolarRoute.png](https://zh.wikipedia.org/wiki/File:PolarRoute.png "fig:PolarRoute.png")
**北極航線**指的是往來[歐洲和](../Page/歐洲.md "wikilink")[亞洲或者](../Page/亞洲.md "wikilink")[北美洲和亞洲之間](../Page/北美洲.md "wikilink")，因為地表的曲面形狀，以飛越[北極上空能最為節省飛行時間的方式](../Page/北極.md "wikilink")。

## 概說

如果從北美洲到亞洲，或者是從歐洲到亞洲，最短的「直接距離」航線即是沿著以地球半径为半径的圓周而行的航線（通稱「[大圓航線](../Page/大圓航線.md "wikilink")」），一般會經過北極地區，或是俄羅斯北部，但無論是哪一條航線都需要通過俄羅斯上空，由於俄羅斯領空在前[蘇聯時期基本上處於封鎖狀態](../Page/蘇聯.md "wikilink")，因此一般飞機無法利用這樣的直接航線來往歐、亞、北美等三洲，如1983年的[大韓航空007號班機就是因為誤進蘇聯領空而被擊落](../Page/大韓航空007號班機.md "wikilink")。

此外，1990年代以前的一般民航機的續航能力並不足以飛越北極以及俄羅斯北部的一大片荒蕪地區，而且該處也沒有可供緊急情況降落的備降機場，因此當時來往亞洲與北美洲的飛機會繞道北太平洋，並在[阿拉斯加](../Page/阿拉斯加.md "wikilink")[安克拉治中停加油](../Page/安克雷奇.md "wikilink")。

直到1991年[蘇聯解體後](../Page/蘇聯.md "wikilink")[俄羅斯開放](../Page/俄羅斯.md "wikilink")[領空](../Page/領空.md "wikilink")，再加上[波音747-400等等續航距離超過](../Page/波音747.md "wikilink")13,000公里的飛機投入服務，一般民航機才可能取道北極航線，因而大幅拉近了美洲與南亞之間的距離。

## 好處

由於北極地區的盛行西風一般強度較弱，再加上北極航線更接近[大圓航線](../Page/大圓航線.md "wikilink")、航程較短，因此，越來越多往西飛（例如從美國東岸到北京）的航班，都會取道北極航線，節省時間之餘，還可以節省[燃料](../Page/燃料.md "wikilink")，並增加有效負載。

## 不使用北極航線的情況

不過，從亞洲向東飛往北美洲的航班，一般並不使用北極航線，而取道較南的北太平洋，再從[加拿大或](../Page/加拿大.md "wikilink")[美國西岸進入北美洲](../Page/美國.md "wikilink")。這是因為北太平洋有較強的盛行西風（即[噴射氣流](../Page/噴射氣流.md "wikilink")），飛機可以順著西風飛行，節省的燃料與時間優於使用北極航線。

另外，即使是現在，純貨機一般會盡量載運負載，所攜帶的燃料並不足於飛越北極地區，故此仍然需要在阿拉斯加的[安克拉治中途停站加油](../Page/安克雷奇.md "wikilink")。

## 飛行北極航線的特別要求

由於北極地區接近北磁極，羅盤可能會無法正常顯示方向，在[全球定位系統](../Page/全球定位系統.md "wikilink")（GPS）未開放為民用以前，飛機的導航系統並沒有足夠能力，引導飛機飛越北極地區（該處並沒有雷達站一類的設施）。導航系統的可靠性，對於在北極航線飛行的飛機來說，就會變得非常重要。

另外，考慮到北極地區的寒冷天氣，飛機上必備有寒冷地區專用的求生裝備（例如煤油爐、寒衣、毛毯、高熱量的乾糧等），以備飛機萬一發生嚴重故障，需要在北極地區緊急降落之用。為了避免此等事故發生，飛機的所有裝置，包括發動機、[輔助動力裝置](../Page/輔助動力系統.md "wikilink")、電力、液壓系統等，都需要更為嚴格的防護性維修。

## 南極航線

目前南半球的民航飛行並沒有利用南極航線，[阿根廷航空的航線最南可以達到約南緯](../Page/阿根廷航空.md "wikilink")55°。

## 另見

  - [西北航道](../Page/西北航道.md "wikilink")
  - [北方海路](../Page/北方海路.md "wikilink")

[Category:航空术语](../Category/航空术语.md "wikilink")
[Category:航空通路](../Category/航空通路.md "wikilink")