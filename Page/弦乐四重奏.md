[String_quartet.JPG](https://zh.wikipedia.org/wiki/File:String_quartet.JPG "fig:String_quartet.JPG")
**弦樂四重奏**是一種[音樂演奏形式](../Page/音樂.md "wikilink")，或是說一種音樂作品，由4把弦樂器負責，通常是2把小提琴，一把中提琴和一把大提琴。

## 背景

[Violin_VL100.jpg](https://zh.wikipedia.org/wiki/File:Violin_VL100.jpg "fig:Violin_VL100.jpg")
雖然任何四種弦樂器組合都可以在文字上被稱為“弦樂四重奏”，但實際上該名詞指的是一組由兩把[小提琴](../Page/小提琴.md "wikilink")，（第一小提琴通常演奏高位旋律，第二小提琴則相應的演奏低位旋律），一把[中提琴和一把](../Page/中提琴.md "wikilink")[大提琴組成的演奏家團體](../Page/大提琴.md "wikilink")。如果作曲家為其它的四種弦樂器配寫音樂，如三把小提琴和一低音提琴，或是小提琴，中提琴，一大提琴和一把[吉他](../Page/吉他.md "wikilink")，他應在[配器上特別注明](../Page/配器.md "wikilink")。標準的弦樂四重奏是一種常見的[室內樂](../Page/室內樂.md "wikilink")，自18世紀以後，大多數的作曲家都寫有弦樂四重奏。

單單說為四位弦樂演奏家而寫的作品，還可以有很多種可能的形式。但如果說到是“弦樂四重奏”，則指的通常是四[樂章的作品](../Page/樂章.md "wikilink")（小部份的弦樂四重奏為三樂章的，代表有[莫扎特早期的弦樂四重奏](../Page/莫扎特.md "wikilink")。），和[交響曲一樣有著複雜的結構](../Page/交響曲.md "wikilink")。首尾兩樂章通常是快板，中間的樂章，在古典四重奏中是慢板和舞曲（如[小步舞曲或](../Page/小步舞曲.md "wikilink")[諧謔曲](../Page/諧謔曲.md "wikilink")）。

其它的室內樂演奏組可看成是弦樂四重奏的改編版本，如[鋼琴五重奏](../Page/鋼琴五重奏.md "wikilink")，即弦樂四重奏加一[鋼琴](../Page/鋼琴.md "wikilink")(也有例外,如舒伯特的"鱒魚",把一般鋼琴五重奏的一把小提琴改成了低音提琴)，弦樂五重奏，弦樂四重奏加一中提琴，大提琴或是低音提琴。[弦樂三重奏](../Page/弦樂三重奏.md "wikilink")，由一把小提琴，一中提琴和一大提琴擔任。還有[鋼琴四重奏](../Page/鋼琴四重奏.md "wikilink")，用鋼琴代替弦樂四重奏中的一把小提琴。

## 歷史

弦樂四重奏這種音樂形式在18世紀中後出現。[海頓的第一首弦樂四重奏有五個樂章](../Page/海頓.md "wikilink")，與[嬉遊曲或是](../Page/嬉遊曲.md "wikilink")[小夜曲相似](../Page/小夜曲.md "wikilink")，但是1769至70年間創作的第9號，則是以日後成為標準的形式出現的:四樂章，一個快板，一個慢板，一個小步舞曲和三重奏，還有一個快的終曲。海頓將源於[巴洛克時期的組曲形式化為弦樂四重奏的標準形式](../Page/巴洛克.md "wikilink")，被後世尊為“弦樂四重奏之父”。海頓也在社交場合和臨時組成的弦樂四重奏團演出他自己的作品，就連[莫扎特也當過他的搭檔](../Page/莫扎特.md "wikilink")。

早在海頓的時代，弦樂四重奏就被視為對一個作曲家功力的考驗。這是由於弦樂四重奏不如[管弦樂那樣有這麼多樂器可供表現之用](../Page/管弦樂.md "wikilink")，音樂只能以自身，而非音色去取悅聽眾，可說是十九世紀「絕對音樂」的代表性曲式。

弦樂四重奏在[古典主義時期開花](../Page/古典主義時期.md "wikilink")，[莫扎特和](../Page/莫扎特.md "wikilink")[貝多芬都沿著海頓的作品寫下了多部有名的弦樂四重奏作品](../Page/貝多芬.md "wikilink")。19世紀弦樂四重奏陷入低潮，一個奇怪的現象，一些作曲家只寫下一首弦樂四重奏，可能他們認為自己已經駕馭了這種曲式，無可突破之處。當現代音樂來臨之際，弦樂四重奏又得以重生。
.....

## 著名的弦樂四重奏作品

下面是一些18世紀到20世紀80年代弦樂四重奏名曲：

  - [海頓的](../Page/海頓.md "wikilink")68首弦樂四重奏，特別是晚期的[艾爾德迪四重奏](../Page/艾爾德迪四重奏.md "wikilink")，作品76。
  - [莫扎特的](../Page/莫扎特.md "wikilink")23首弦樂四重奏：他獻給海頓的六首（K.
    387，421，428，458，464，465，作品10）被普遍認為是古典弦樂四重奏的頂峰之作。C大調第19號《不和諧音》，K.
    465以其不調和的前奏著名。
  - [貝多芬的](../Page/貝多芬.md "wikilink")16首弦樂四重奏聲譽甚隆。弦樂四重奏1-6號，作品18展示了他自[海頓和](../Page/海頓.md "wikilink")[莫扎特以來對弦樂四重奏發展的總結](../Page/莫扎特.md "wikilink")。之後三首，[弦樂四重奏7-9號，作品59也非常流行而甚至今天也是](../Page/弦樂四重奏7-9號，作品59.md "wikilink")，因為這些作品大大地擴展其表現形式和吸收了新的情感和戲劇性。接下來也有弦樂四重奏10-11號，作品74《Harp》和作品95《Serioso》。最後，[弦樂四重奏12-16c和大賦格，作品127，130-135](../Page/弦樂四重奏12-16c和大賦格，作品127，130-135.md "wikilink")，這最後五首弦樂四重奏和大賦格為作曲家最後的作品。雖然這些作品充滿著不妥協的鬥志，被認為是最偉大的作品，但是相比起貝多芬中期寫下的浪漫詩篇，諸如拉祖莫夫斯基四重奏，普及程度還是稍遜一籌。
  - [舒伯特的](../Page/舒伯特.md "wikilink")[c小調第12號弦樂四重奏《四重奏樂章》](../Page/c小調第12號弦樂四重奏《四重奏樂章》.md "wikilink")，[d小調第14號弦樂四重奏《死與少女》](../Page/d小調第14號弦樂四重奏《死與少女》.md "wikilink")。還有[A小調第13號弦樂四重奏《Rosamunde》和他最後的](../Page/A小調第13號弦樂四重奏《Rosamunde》.md "wikilink")[G大調第15號弦樂四重奏](../Page/G大調第15號弦樂四重奏.md "wikilink")。
  - [孟德爾頌的六首弦樂四重奏](../Page/孟德爾頌.md "wikilink")。
  - [史麥塔納的](../Page/史麥塔納.md "wikilink")[e小調第1號弦樂四重奏《我的生涯》](../Page/e小調第1號弦樂四重奏《我的生涯》.md "wikilink")。
  - [布拉姆斯的三首弦樂四重奏](../Page/布拉姆斯.md "wikilink")。
  - [德弗札克的](../Page/安東尼·德弗札克.md "wikilink")[F大調第12號弦樂四重奏《美國》](../Page/F大調第12號弦樂四重奏《美國》.md "wikilink")。
  - [柴可夫斯基的](../Page/柴可夫斯基.md "wikilink")[第1號弦樂四重奏，作品11](../Page/第1號弦樂四重奏，作品11.md "wikilink")，特別是第二樂章"如歌的行板"。
  - [鮑羅丁的](../Page/鮑羅丁.md "wikilink")[D大調第2號弦樂四重奏](../Page/D大調第2號弦樂四重奏.md "wikilink")，特別是第三樂章"夜曲"。
  - [德布西的](../Page/德布西.md "wikilink")[g小調弦樂四重奏，作品10](../Page/g小調弦樂四重奏，作品10.md "wikilink")。
  - [荀白格的四首弦樂四重奏](../Page/荀白格.md "wikilink")。
  - [拉威爾的](../Page/拉威爾.md "wikilink")[F大調第弦樂四重奏](../Page/F大調第弦樂四重奏.md "wikilink")。
  - [楊納傑克的](../Page/楊納傑克.md "wikilink")[第1號弦樂四重奏《克羅采》](../Page/第1號弦樂四重奏《克羅采》.md "wikilink")，靈感來自[托爾斯泰的小說](../Page/托爾斯泰.md "wikilink")[克羅采奏鳴曲](../Page/克羅采奏鳴曲.md "wikilink")，其小說靈感來自[貝多芬的](../Page/貝多芬.md "wikilink")[小提琴](../Page/小提琴.md "wikilink")[奏鳴曲No](../Page/奏鳴曲.md "wikilink").
    9，[克羅采奏鳴曲](../Page/克羅采奏鳴曲.md "wikilink")。
  - [Frank
    Bridge的](../Page/Frank_Bridge.md "wikilink")[第3號弦樂四重奏](../Page/第3號弦樂四重奏.md "wikilink")。
  - [巴爾托克的六首弦樂四重奏](../Page/巴爾托克.md "wikilink")。
  - [貝爾格的](../Page/貝爾格.md "wikilink")[抒情組曲](../Page/抒情組曲.md "wikilink")，作給弦樂四重奏。
  - [馬提努的](../Page/馬提努.md "wikilink")[協奏曲給弦樂四重奏和管弦樂團](../Page/協奏曲給弦樂四重奏和管弦樂團.md "wikilink")
  - [魏拉-羅伯士的十七首弦樂四重奏](../Page/魏拉-羅伯士.md "wikilink")。
  - [蕭斯達高維契的十五首弦樂四重奏](../Page/蕭斯達高維契.md "wikilink")，特別是[c小調第8號弦樂四重奏，作品110](../Page/c小調第8號弦樂四重奏，作品110.md "wikilink")。
  - [艾略特·卡特的五首弦樂四重奏和近年受到讚揚的係列](../Page/艾略特·卡特.md "wikilink")。
  - [菅野茂的六首弦樂四重奏](../Page/菅野茂.md "wikilink")《Dietro il Pontecello》。
  - [武滿徹的](../Page/武滿徹.md "wikilink")[第1號四重奏給弦樂《A Way a
    Lone》](../Page/第1號四重奏給弦樂《A_Way_a_Lone》.md "wikilink")。

## 弦樂四重奏團

為了演出，一班的弦樂樂師會組成弦樂四重奏團。有時，他們會一起合奏多年，有時則會替改成員但保留名稱。以下是一些出名的弦樂四重奏團：（名稱以外文為主）

  - [艾歐里安弦樂四重奏團](../Page/艾歐里安弦樂四重奏團.md "wikilink")（Aeolian Quartet）
  - [阿爾班·貝爾格弦樂四重奏團](../Page/阿爾班·貝爾格弦樂四重奏團.md "wikilink")（Alban Berg
    Quartet）
  - [艾爾伯尼弦樂四重奏團](../Page/艾爾伯尼弦樂四重奏團.md "wikilink")（Alberni Quartet）
  - [阿雷格里弦樂四重奏團](../Page/阿雷格里弦樂四重奏團.md "wikilink")（Allegri Quartet）
  - [阿瑪迪斯弦樂四重奏團](../Page/阿瑪迪斯弦樂四重奏團.md "wikilink")（Amadeus Quartet）
  - [阿馬蒂弦樂四重奏團](../Page/阿馬蒂弦樂四重奏團.md "wikilink")（Amati Quartet）
  - [阿迪蒂弦樂四重奏團](../Page/阿迪蒂弦樂四重奏團.md "wikilink")（Arditti Quartet）
  - [澳大利亞弦樂四重奏團](../Page/澳大利亞弦樂四重奏團.md "wikilink")（Australian String
    Quartet）
  - [阿維夫弦樂四重奏團](../Page/阿維夫弦樂四重奏團.md "wikilink")（Aviv String Quartet）
  - [Balanescu弦樂四重奏團](../Page/Balanescu弦樂四重奏團.md "wikilink")（Balanescu
    Quartet）
  - [貝爾琪亞弦樂四重奏團](../Page/貝爾琪亞弦樂四重奏團.md "wikilink")（Belcea Quartet）
  - [鮑羅定弦樂四重奏團](../Page/鮑羅定弦樂四重奏團.md "wikilink")（Borodin Quartet）
  - [辣妹弦樂四重奏團](../Page/辣妹弦樂四重奏團.md "wikilink")（Bond（band））
  - [布蘭迪斯四重奏](../Page/布蘭迪斯四重奏.md "wikilink")（Brandis Quartet）
  - [布蘭塔諾弦樂四重奏團](../Page/布蘭塔諾弦樂四重奏團.md "wikilink")（Brentano Quartet）
  - [柏洛斯基弦樂四重奏團](../Page/柏洛斯基弦樂四重奏團.md "wikilink")（Brodsky Quartet）
  - [布達佩斯弦樂四重奏團](../Page/布達佩斯弦樂四重奏團.md "wikilink")（Budapest
    Quartet），以及後來的[新布達佩斯弦樂四重奏團](../Page/新布達佩斯弦樂四重奏團.md "wikilink")（New
    Budapest Quartet）
  - [布希弦樂四重奏團](../Page/布希弦樂四重奏團.md "wikilink")（Busch Quartet）
  - [Carmine弦樂四重奏團](../Page/Carmine弦樂四重奏團.md "wikilink")（Carmine
    Quartet）
  - [Cavani弦樂四重奏團](../Page/Cavani弦樂四重奏團.md "wikilink")（Cavani Quartet）
  - [Claring Chamber
    Players](../Page/Claring_Chamber_Players.md "wikilink")
  - [克利夫蘭弦樂四重奏團](../Page/克利夫蘭弦樂四重奏團.md "wikilink")（Cleveland Quartet）
  - [奇夌季里安弦樂四重奏團](../Page/奇夌季里安弦樂四重奏團.md "wikilink")（Chilingirian
    Quartet）
  - [科羅拉多弦樂四重奏團](../Page/科羅拉多弦樂四重奏團.md "wikilink")（Colorado Quartet）
  - [科爾維努斯弦樂四重奏團](../Page/科爾維努斯弦樂四重奏團.md "wikilink")（Corvinus Quartet）
  - [卡溫頓弦樂四重奏團](../Page/卡溫頓弦樂四重奏團.md "wikilink")（Covington Quartet）
  - [庫爾弦樂四重奏團](../Page/庫爾弦樂四重奏團.md "wikilink")（Coull Quartet）
  - [絲柏弦樂四重奏團](../Page/絲柏弦樂四重奏團.md "wikilink")（Cypress Quartet）
  - [達文西弦樂四重奏團](../Page/達文西弦樂四重奏團.md "wikilink")（Da Vinci Quartet）
  - [代爾姆弦樂四重奏團](../Page/代爾姆弦樂四重奏團.md "wikilink")（Delme Quartet）
  - [太陽弦樂四重奏團](../Page/太陽弦樂四重奏團.md "wikilink")（Del Sol Quartet）
  - [公爵弦樂四重奏團](../Page/公爵弦樂四重奏團.md "wikilink")（Duke Quartet）
  - [安德里昂弦樂四重奏團](../Page/安德里昂弦樂四重奏團.md "wikilink")（Endellion Quartet）
  - [愛默生弦樂四重奏團](../Page/愛默生弦樂四重奏團.md "wikilink")（Emerson String Quartet）
  - [菲斯特提弦樂四重奏團](../Page/菲斯特提弦樂四重奏團.md "wikilink")（Festetics String
    Quartet）
  - [四弦弦樂四重奏團](../Page/四弦弦樂四重奏團.md "wikilink")（The Four Strings Quartet）
  - [費兹威廉弦樂四重奏團](../Page/費兹威廉弦樂四重奏團.md "wikilink")（Fitzwilliam Quartet）
  - [弗路克斯四重奏](../Page/弗路克斯四重奏.md "wikilink")（Flux Quartet）
  - [加布里埃利弦樂四重奏團](../Page/加布里埃利弦樂四重奏團.md "wikilink")（Gabrieli Quartet）
  - [布商大厦弦乐四重奏团](../Page/布商大厦弦乐四重奏团.md "wikilink")（Gewandhaus Quartet）
  - [葛利勒弦樂四重奏團](../Page/葛利勒弦樂四重奏團.md "wikilink")（Griller Quartet）
  - [瓜奈里弦樂四重奏團](../Page/瓜奈里弦樂四重奏團.md "wikilink")（Guarneri Quartet）
  - [哈根弦樂四重奏團](../Page/哈根弦樂四重奏團.md "wikilink")（Hagen Quartet）
  - [好萊塢弦樂四重奏團](../Page/好萊塢弦樂四重奏團.md "wikilink")（Hollywood Quartet）
  - [沃爾夫弦樂四重奏團](../Page/沃爾夫弦樂四重奏團.md "wikilink")（Hugo Wolf Quartet）
  - [匈牙利弦樂四重奏團](../Page/匈牙利弦樂四重奏團.md "wikilink")（Hungarian Quartet）
  - [四方克雷莫纳](../Page/四方克雷莫纳.md "wikilink")（Quartetto di Cremona）
  - [義大利弦樂四重奏團](../Page/義大利弦樂四重奏團.md "wikilink")（Quartetto Italiano）
  - [四方瓜达尼尼](../Page/四方瓜达尼尼.md "wikilink")（Quartetto Guadagnini）
  - [四威尼斯](../Page/四威尼斯.md "wikilink")（Quartetto di Venezia）
  - [姚阿幸弦樂四重奏團](../Page/姚阿幸弦樂四重奏團.md "wikilink")（Joachim Quartet）
  - [茱莉亚弦乐四重奏团](../Page/茱莉亚弦乐四重奏团.md "wikilink")（Juilliard String
    Quartet）
  - [高大宜弦樂四重奏團](../Page/高大宜弦樂四重奏團.md "wikilink")（Kodály Quartet）
  - [柯波曼弦樂四重奏團](../Page/柯波曼弦樂四重奏團.md "wikilink")（Kopelman Quartet）
  - [克羅諾斯弦樂四重奏團](../Page/克羅諾斯弦樂四重奏團.md "wikilink")（Kronos Quartet）
  - [Kuchl弦樂四重奏團](../Page/Kuchl弦樂四重奏團.md "wikilink")（Kuchl Quartet）
  - [拉薩葉弦樂四重奏團](../Page/拉薩葉弦樂四重奏團.md "wikilink")（Lasalle Quartet）
  - [林夕弦樂四重奏團](../Page/林夕弦樂四重奏團.md "wikilink")（Lindsay Quartet）
  - [馬奇尼弦樂四重奏團](../Page/馬奇尼弦樂四重奏團.md "wikilink")（Maggini Quartet）
  - [梅洛斯弦樂四重奏團](../Page/梅洛斯弦樂四重奏團.md "wikilink")（Melos Quartet）
  - [紐西蘭弦樂四重奏團](../Page/紐西蘭弦樂四重奏團.md "wikilink")（New Zealand String
    Quartet）
  - [歐弗德弦樂四重奏團](../Page/歐弗德弦樂四重奏團.md "wikilink")（Orford Quartet）
  - [奧蘭多弦樂四重奏團](../Page/奧蘭多弦樂四重奏團.md "wikilink")（Orlando Quartet）
  - [彼德森弦樂四重奏團](../Page/彼德森弦樂四重奏團.md "wikilink")（Peterson Quartet）
  - [布拉格弦樂四重奏團](../Page/布拉格弦樂四重奏團.md "wikilink")（Prague Quartet）
  - [Quartet-X](../Page/Quartet-X.md "wikilink")
  - [阿貝鳩奈弦樂四重奏團](../Page/阿貝鳩奈弦樂四重奏團.md "wikilink")（Quatuor Arpeggione）
  - [馬賽克弦樂四重奏團](../Page/馬賽克弦樂四重奏團.md "wikilink")（Quatuor Mosaïques）
  - [羅莎蒙德弦樂四重奏團](../Page/Rosamunde_Quartet_\(ensemble\).md "wikilink")（Rosamunde
    Quartet）
  - [沙羅門弦樂四重奏團](../Page/沙羅門弦樂四重奏團.md "wikilink")（Salomon Quartet）
  - [上海弦樂四重奏團](../Page/上海弦樂四重奏團.md "wikilink")（Shanghai String Quartet）
  - [西里西亞弦樂四重奏團](../Page/西里西亞弦樂四重奏團.md "wikilink")（Silesian String
    Quartet）
  - [史麥塔納弦樂四重奏團](../Page/史麥塔納弦樂四重奏團.md "wikilink")（Smetana Quartet）
  - [Skampa弦樂四重奏團](../Page/Skampa弦樂四重奏團.md "wikilink")（Skampa Quartet）
  - [史密斯弦樂四重奏團](../Page/史密斯弦樂四重奏團.md "wikilink")（Smith Quartet）
  - [塔卡克斯弦樂四重奏團](../Page/塔卡克斯弦樂四重奏團.md "wikilink")（Takács Quartet）
  - [塔利希弦樂四重奏團](../Page/塔利希弦樂四重奏團.md "wikilink")（Talich Quartet）
  - [柴可夫斯基弦樂四重奏團](../Page/柴可夫斯基弦樂四重奏團.md "wikilink")（Tchaikovsky
    Quartet）
  - [東京弦樂四重奏團](../Page/東京弦樂四重奏團.md "wikilink")（Tokyo String Quartet）
  - [RTÉ Vanbrugh Quartet](../Page/RTÉ_Vanbrugh_Quartet.md "wikilink")
  - [維格弦樂四重奏團](../Page/維格弦樂四重奏團.md "wikilink")（Vegh Quartet）
  - [維梅爾弦樂四重奏團](../Page/維梅爾弦樂四重奏團.md "wikilink")（Vermeer Quartet）
  - [維爾塔弗弦樂四重奏團](../Page/維爾塔弗弦樂四重奏團.md "wikilink")（Vertavo String
    Quartet）
  - [伊薩依弦樂四重奏團](../Page/伊薩依弦樂四重奏團.md "wikilink")（Ysaÿe Quartet）
  - [扎格萊布弦樂四重奏團](../Page/扎格萊布弦樂四重奏團.md "wikilink")（Zagreb Quartet）
  - [Zorian Quartet](../Page/Zorian_Quartet.md "wikilink")

## 延伸阅读

  - David Blum (1986). *The Art of Quartet Playing: The Guarneri Quartet
    in Conversation with David Blum*, New York: Alfred A. Knopf Inc.
    ISBN 0-394-53985-0,
  - [Arnold Steinhardt](../Page/Arnold_Steinhardt.md "wikilink")
    (1998).*Indivisible by four*, Farrar, Straus Giroux. ISBN
    0-374-52700-8
  - Edith Eisler (2000). *21st-Century String Quartets*, String Letter
    Publishing. ISBN 1-890490-15-6
  - Paul Griffiths (1983). *The String Quartet: A History*, New York:
    Thames and Hudson. ISBN 0-500-01311-X
  - David Rounds (1999), *The Four & the One: In Praise of String
    Quartets*, Fort Bragg, CA: Lost Coast Press. ISBN 1-882897-26-9.
  - Francis Vuibert (2009), *Répertoire universel du quatuor à cordes*,
    [ProQuartet-CEMC](http://www.proquartet.fr/nl/lettre/rudaq.html),
    ISBN 978-2-9531544-0-5

## 外部链接

  - [Lewis Morton's The String Quartet
    Page](http://www.lmconsult.com/q_head.htm)
  - [Greg Sandow - Introducing String
    Quartets](https://web.archive.org/web/20110718200859/http://www.gregsandow.com/quartint.htm)
  - [A brief history of the development of the String Quartet up to
    Beethoven](http://www.raptusassociation.org/stringprehist_e.html)
  - [Beethoven's string
    quartets](http://www.all-about-beethoven.com/stringquartet.html),
    considered to be some of the best quartets ever written
  - [Art of the States: string
    quartet](http://artofthestates.org/cgi-bin/instsearch.pl?inst=string%20quartet)
    works for string quartet by American composers
  - [Carmine String Quartet webportal](http://www.vonosnegyes.hu), fine
    chamber music from Hungary

[弦樂四重奏](../Category/弦樂四重奏.md "wikilink")
[Category:室內樂](../Category/室內樂.md "wikilink")