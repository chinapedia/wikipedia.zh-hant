**联合国大会决议**由[联合国全体成员国投票决定](../Page/联合国.md "wikilink")，大部分只要获得参与投票（即排除那些未投票或投弃权票的成员国）的**过半数以上**成员国的支持即获得通过。但是凡涉及“维持国际和平及安全之建议，安全理事会[非常任理事国之选举](../Page/非常任理事国.md "wikilink")，经济及社会理事会理事国之选举，托管理事会理事国之选举，对于新会员国加入联合国之准许，会员国权利及特权之停止，会员国之除名，关于施行托管制度之问题，以及预算问题”等“重要问题之决议”，则需要获得参与投票的**三分之二以上**成员国的支持才可通过。

## 重要的联合国决议

  - 1946年的[联合国大会66号决议](../Page/联合国大会66号决议.md "wikilink")：依照宪章第七十三条(辰)款项递送情报书；
  - 1947年的[联合国大会172号决议](../Page/联合国大会172号决议.md "wikilink")：条约与国际协定的登记及公布；
  - 1947年的[联合国大会181号决议](../Page/联合国大会181号决议.md "wikilink")：分割[英属巴勒斯坦的决定](../Page/英属巴勒斯坦.md "wikilink")，成立[巴勒斯坦與](../Page/巴勒斯坦.md "wikilink")[以色列](../Page/以色列.md "wikilink")；
  - 1951年的[聯合國大會498號決議](../Page/聯合國大會第498號決議.md "wikilink")：譴責[中華人民共和國入侵](../Page/中華人民共和國.md "wikilink")[朝鮮半島並大規模攻擊聯合國部隊的](../Page/朝鮮半島.md "wikilink")[侵略與敵對行動](../Page/侵略.md "wikilink")。\[1\]\[2\]\[3\]
  - 1951年的[聯合國大會500號決議](../Page/聯合國大會第500號決議.md "wikilink")：就[中華人民共和國與](../Page/中華人民共和國.md "wikilink")[北韓在](../Page/北韓.md "wikilink")[朝鮮半島的](../Page/朝鮮半島.md "wikilink")[侵略實施貿易](../Page/侵略.md "wikilink")[禁運](../Page/禁運.md "wikilink")。\[4\]
  - 1952年的[聯合國大會505號決議](../Page/聯合國大會505號決議.md "wikilink")：[蘇聯違反](../Page/蘇聯.md "wikilink")1945年8月14日[中蘇友好同盟條約及](../Page/中蘇友好同盟條約.md "wikilink")[聯合國憲章](../Page/聯合國憲章.md "wikilink")，以致威脅[中國](../Page/中國.md "wikilink")（[中華民國](../Page/中華民國.md "wikilink")）政治獨立與領土完整及[遠東和平案](../Page/遠東.md "wikilink")
  - 1960年的[联合国大会1514号决议](../Page/联合国大会1514号决议.md "wikilink")：[关于准许殖民地国家及民族独立的宣言](../Page/关于准许殖民地国家及民族独立的宣言.md "wikilink")；
  - 1960年的[联合国大会1541号决议](../Page/联合国大会1541号决议.md "wikilink")：会员国确定是否负有义务递送宪章第七十三条(辰)款规定的情报所应遵循的原则；
  - 1961年的[联合国大会1654号决议](../Page/联合国大会1654号决议.md "wikilink")：准许殖民地国家及民族独立宣言的实施情形；
  - 1962年的[联合国大会1761号决议](../Page/联合国大会1761号决议.md "wikilink")：建议对施行[种族隔离政策的](../Page/种族隔离制度.md "wikilink")[南非实施制裁](../Page/南非.md "wikilink")；
  - 1971年的[联合国大会2758号决议](../Page/联合国大会2758号决议.md "wikilink")：恢復[中华人民共和国在联合国组织中的合法权利问题](../Page/中华人民共和国.md "wikilink")；
  - 1975年的[联合国大会3379号决议](../Page/联合国大会3379号决议.md "wikilink")：认为[錫安主義是一种](../Page/錫安主義.md "wikilink")[种族主义和](../Page/种族主义.md "wikilink")[种族歧视政策](../Page/种族歧视.md "wikilink")；
  - 1991年的[联合国大会46/86号决议](../Page/联合国大会46/86号决议.md "wikilink")：反对种族歧视、种族主义。

## 参考文献

## 外部链接

  - [联合国大会决议](http://www.un.org/chinese/documents/resga.htm)：查阅所有的过往大会决议文本

## 参见

  - [联合国安全理事会决议](../Page/联合国安全理事会决议.md "wikilink")

{{-}}

[聯合國大會決議](../Category/聯合國大會決議.md "wikilink")
[Category:国际法](../Category/国际法.md "wikilink")

1.  [UNITED NATIONS GENERAL ASSEMBLY
    RESOLUTION 498(V)](http://digitalarchive.wilsoncenter.org/document/116196)

2.  [Defining International
    Aggression](http://www.derechos.org/peace/dia/doc/dia54.html)

3.  John Kuo-Chang Wang, [United Nations voting on Chinese
    representation: An analysis of General Assembly
    roll-calls, 1950-1971](https://shareok.org/bitstream/handle/11244/4401/7815387.PDF?sequence=1)

4.