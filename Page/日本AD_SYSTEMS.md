**株式会社日本AD
SYSTEMS**（、），簡稱**NAS**，為[Asatsu-DK全資擁有的](../Page/Asatsu-DK.md "wikilink")[动画](../Page/动画.md "wikilink")[公司](../Page/公司.md "wikilink")，成立於1975年；業務是動畫企劃製作及周邊商品的經營管理。

## 作品一覽

  - [Keroro軍曹](../Page/Keroro軍曹.md "wikilink")（）
  - [不可思議星球的雙胞胎公主](../Page/不可思議星球的雙胞胎公主.md "wikilink")（）
  - [新世纪福音战士](../Page/新世纪福音战士.md "wikilink")（）
  - [這裡是葛飾區龜有公園前派出所](../Page/這裡是葛飾區龜有公園前派出所.md "wikilink") 又譯
    [烏龍派出所](../Page/烏龍派出所.md "wikilink")（，劇場版）
  - [妖怪人間貝姆](../Page/妖怪人間貝姆.md "wikilink") 又譯 妖怪人類貝姆（）
  - [網球王子](../Page/網球王子.md "wikilink")（）
  - [爆鬥宣言](../Page/爆鬥宣言.md "wikilink")（）
  - [遊戲王怪獸之決鬥](../Page/遊戲王怪獸之決鬥.md "wikilink")（）
  - [小紅帽恰恰](../Page/小紅帽恰恰.md "wikilink")（）
  - [龍鳴](../Page/龍鳴.md "wikilink")（、DRAGONAUT THE RESONANCE）
  - [鋼鐵三國志](../Page/鋼鐵三國志.md "wikilink")（）
  - [神秘的世界](../Page/神秘的世界.md "wikilink")（）
  - [光速蒙面俠21](../Page/光速蒙面俠21.md "wikilink")（）
  - [吸血鬼騎士](../Page/吸血鬼騎士.md "wikilink")（）
  - [夏目友人帳](../Page/夏目友人帳.md "wikilink")（）
  - [玩偶遊戲](../Page/玩偶遊戲.md "wikilink")（）
  - [單蠢女孩](../Page/單蠢女孩.md "wikilink")（）
  - [徒然喜歡你](../Page/徒然喜歡你.md "wikilink")（）
  - [學園奶爸](../Page/學園奶爸.md "wikilink")（）

## 關連項目

  - [日本動畫工作室列表](../Page/日本動畫工作室列表.md "wikilink")

## 外部連結

  - [NAS官方網站](http://www.nasinc.co.jp)

[Category:1975年成立的公司](../Category/1975年成立的公司.md "wikilink")
[Category:中央區公司 (東京都)](../Category/中央區公司_\(東京都\).md "wikilink")
[Category:動畫產業公司](../Category/動畫產業公司.md "wikilink")