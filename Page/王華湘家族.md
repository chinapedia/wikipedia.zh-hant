**王華湘家族**，祖籍[江西](../Page/江西.md "wikilink")[南昌](../Page/南昌.md "wikilink")，以電子業起家，號稱電子大王，坐擁兩間上市公司。

## 家族特色

王華湘家族中之各種特色：

  - 與香港娛樂圈關係密切
      - 王華湘的長孫[王賢誌為前](../Page/王賢誌.md "wikilink")[香港無綫電視藝員](../Page/香港無綫電視.md "wikilink")
      - 2006年香港無綫電視最佳男主角得主[鄭嘉穎及](../Page/鄭嘉穎.md "wikilink")[1998年度香港小姐競選冠軍](../Page/1998年度香港小姐競選.md "wikilink")[向海嵐均與此家族有姻親關係](../Page/向海嵐.md "wikilink")
      - 王華湘的孫女[張王賢慧其丈夫](../Page/張王賢慧.md "wikilink")[張瑞燊與著名歌影視三棲藝人](../Page/張瑞燊.md "wikilink")[周麗淇](../Page/周麗淇.md "wikilink")（表妹）和名模[周汶錡](../Page/周汶錡.md "wikilink")（表姐）是親戚
      - [張瑞燊之母親和](../Page/張瑞燊.md "wikilink")[周汶錡](../Page/周汶錡.md "wikilink")、[周麗淇之母親是姊妹](../Page/周麗淇.md "wikilink")
  - 熱心公益
      - 家族的第二代有兩人歷任慈善機構的主席，而家族第三代亦已有人踏入總理的門檻
      - 慈善機構轄下的三所學校及一所育嬰院以王華湘家族成員命名
  - 自由發展
      - 家族第三代的兩位公子在畢業後均沒有參與打理[家族生意](../Page/家族生意.md "wikilink")，此在華商家族中份屬罕見
  - 熱愛賽馬運動
      - 家族第二代的王忠桐、王忠秣及第三代的[王賢敏](../Page/王賢敏.md "wikilink")、[王賢信](../Page/王賢信.md "wikilink")、[王賢誌](../Page/王賢誌.md "wikilink")、[張王賢慧均為馬會會員](../Page/張王賢慧.md "wikilink")
      - 王忠秣育有以安字為首的馬包括：安賦、安銳、安駒、安荃(此駒與王賢信共同擁有)、安嚮、安喜、安打、安驥、安陶、安鞍、安駿、安晉、安翹、[安畋](../Page/安畋.md "wikilink")、安利、安騰、安威、安達、安驊、安御、安進、安圖(此駒與[王賢敏](../Page/王賢敏.md "wikilink")、[王賢信共同擁有](../Page/王賢信.md "wikilink"))、安培、安勝(上手馬主為潘永楷)
      - 王忠桐擁有的名下馬匹包括大將風馳(此駒與[王賢誌](../Page/王賢誌.md "wikilink")、[張王賢慧等人以](../Page/張王賢慧.md "wikilink")「大將之家賽馬團體」名義共同擁有)、大將風雲、大將風勝(此駒與[王賢誌共同擁有](../Page/王賢誌.md "wikilink"))、大將風範(此駒與[張王賢慧共同擁有](../Page/張王賢慧.md "wikilink"))、大將福星、大將雄心、大統帥、山中王、大將風魔
          - [王賢誌的名下馬匹包括大將風騷](../Page/王賢誌.md "wikilink")、大將風速、大將風雷
          - [王賢敏的名下馬匹包括安競](../Page/王賢敏.md "wikilink")、安趣、安穩、安賞、安琛、安友、安騏、安定
          - [王賢信的名下馬匹包括安瑪](../Page/王賢信.md "wikilink")、安甲、安闖、安迅
          - [張王賢慧與丈夫張瑞燊的名下馬匹包括大將風鑽](../Page/張王賢慧.md "wikilink")、大將風彩

## 興起方法

以[山寨廠起家](../Page/山寨廠.md "wikilink")，其後將[王氏國際及](../Page/王氏國際.md "wikilink")[王氏港建在](../Page/王氏港建.md "wikilink")[香港交易所上市](../Page/香港交易所.md "wikilink")，現市值超過十四億元

## 家族成員

  - [王華湘](../Page/王華湘.md "wikilink")（電子大王兼大慈善家） =
    [余家潔](../Page/余家潔.md "wikilink") |
    [龐流流](../Page/龐流流.md "wikilink")（看護）
      - [王忠椏](../Page/王忠椏.md "wikilink") =
        [陸潔貞](../Page/陸潔貞.md "wikilink")
          - [王賢詠](../Page/王賢詠.md "wikilink")
      - [王忠桐](../Page/王忠桐.md "wikilink")（王氏港建主席
        歷任[東華三院主席](../Page/東華三院.md "wikilink")）
        = [胡麗明](../Page/胡麗明.md "wikilink")
          - [王賢誌](../Page/王賢誌.md "wikilink")（前無綫電視藝員、現任王氏港建執行董事、東華三院主席）
            = 周思聰
          - [張王賢慧](../Page/張王賢慧.md "wikilink")（東華三院2006/2007總理）
          - [王賢德](../Page/王賢德.md "wikilink") =
            [陳志堅](../Page/陳志堅.md "wikilink")（新興紙品廠主席陳鐵生之幼子）
      - [王忠梴](../Page/王忠梴.md "wikilink") =
        [胡倩明](../Page/胡倩明.md "wikilink")（歷任東華三院總理）
      - [王忠秣](../Page/王忠秣.md "wikilink")（王氏國際主席
        歷任[仁濟醫院主席](../Page/仁濟醫院.md "wikilink")）
        = [呂珍娣](../Page/呂珍娣.md "wikilink")
          - [王賢敏](../Page/王賢敏.md "wikilink")
          - [王賢訊](../Page/王賢訊.md "wikilink")（原名王賢信） =
            [朱穎詩](../Page/朱穎詩.md "wikilink")
      - [艾王忠椒](../Page/艾王忠椒.md "wikilink")
      - [王忠恩](../Page/王忠恩.md "wikilink")

## 參見

  - [仁濟醫院王華湘中學](../Page/仁濟醫院王華湘中學.md "wikilink")

## 設施命名

  - [仁濟醫院王華湘中學](http://www.ychwwsss.edu.hk/)
  - [東華三院王余家潔紀念小學](http://www.wyjjmps.edu.hk/)
  - [東華三院王胡麗明幼稚園](http://www.twghwwlmkg.edu.hk/)
  - [香港科技大學王忠秣科技講堂](https://web.archive.org/web/20051004035407/http://www.ust.hk/terminology/named_facilities.htm)
  - [仁濟艾王忠椒育嬰院](http://www.baby-kingdom.com/modules/newbb/viewtopic.php?topic_id=982231&forum=279&7)

## 外部連結

  - [王氏國際(股票編號:0099)](http://www.wongswec.com/)
  - [王氏港建(股票編號:0532)](http://www.wkkintl.com/)
  - [WKKINTL (HOLD)(0532) - 公告及復牌
    (02)](http://hk.biz.yahoo.com/060807/263/1r0y0.html)
  - [東華三院2007/2008總理
    張王賢慧](https://web.archive.org/web/20070927011926/http://www.tungwah.org.hk/upload/Board/2007/board-ac.pdf)
  - [图片：王贤志、王贤德，郑嘉颖、向海岚](https://web.archive.org/web/20070125080222/http://bbs.house.focus.cn/msgview/2710/40029728.html)
  - [10萬擺檔大賺遊客錢](https://web.archive.org/web/20070930210457/http://education.atnext.com/index.cfm?fuseaction=Article.View&articleID=6380781&issueID=20061006)
  - [王忠桐上市王國告終](http://appledaily.atnext.com/template/apple/art_main.cfm?iss_id=20060808&sec_id=15307&subsec_id=15320&art_id=6199498)
  - [香港馬會](http://www.hkjc.com/chinese/racing/ownersearch.asp?horseowner=王忠)

[Category:香港家族](../Category/香港家族.md "wikilink")
[Category:中國王姓家族](../Category/中國王姓家族.md "wikilink")
[王華湘家族](../Category/王華湘家族.md "wikilink")