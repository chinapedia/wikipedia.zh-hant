**直腹足亞綱**（[学名](../Page/学名.md "wikilink")：），又稱**正腹足亞綱**，是過往[蝸牛及](../Page/蝸牛.md "wikilink")[蛞蝓在](../Page/蛞蝓.md "wikilink")[分類學的兩個主要分類中的其中一個](../Page/分類學.md "wikilink")，但現時[已不再使用](../Page/棄用生物分類.md "wikilink")。直腹足亞綱為龐大的[腹足綱下一個極大的](../Page/腹足綱.md "wikilink")[亞綱](../Page/亞綱.md "wikilink")\[1\]。（腹足綱為所有在海水、淡水及陸地上的蝸牛及蛞蝓。而腹足綱則為包含所有軟體動物的[軟體動物門的一個分類](../Page/軟體動物門.md "wikilink")）。

根據[布歇特和洛克羅伊的腹足類分類
(2005年)](../Page/布歇特和洛克羅伊的腹足類分類_\(2005年\).md "wikilink")，腹足綱的兩個亞綱會取消，而兩個亞綱旗下的各個目會變成直屬於腹足綱的不分級支序\[2\]。

## 分類的歷史

根據[大衛·R·林德柏格](../Page/大衛·R·林德柏格.md "wikilink")（David R.
Lindberg）及[溫斯頓·旁得](../Page/溫斯頓·旁得.md "wikilink")（W.F.
Ponder）在1996年出版的[腹足纲分类表
(1997年)](../Page/腹足纲分类表_\(1997年\).md "wikilink")，「直腹足類」動物被歸為腹足綱的其中的一個亞綱\[3\]；而根據二人其後為澳洲動物系列撰寫的*The
Southern
Synthesis*，本亞綱包含原來的较早的[前鳃亚纲](../Page/前鳃亚纲.md "wikilink")（Prosobranchia）和[后鳃亚纲](../Page/后鳃亚纲.md "wikilink")（Opisthobranchia）的部分物種。另一個亞綱[真腹足亞綱](../Page/真腹足亞綱.md "wikilink")（*Eogastropoda*）則比直腹足亞綱細小很多，只有5個[科的真](../Page/科_\(生物\).md "wikilink")[帽貝](../Page/帽貝.md "wikilink")（limpets）。然而，分子生物學的結果並不認同這種將腹足綱二分的分類\[4\]\[5\]。

## 真正的蝸牛

此直腹足亞綱，部分人稱為**真正的蝸牛**。而被簡潔地定義為所有腹足類中不是**真正的帽貝**，即所有不屬於[笠形腹足目](../Page/笠形腹足目.md "wikilink")（*Patellogastropoda*）的腹足綱成員\[6\]。

## 分支的衍徵

直腹足亞綱形成在[支序分類學上的一個分支](../Page/支序分類學.md "wikilink")，由清楚的[衍徵](../Page/衍徵.md "wikilink")（synapomorphies）所支持。那些衍徵（即特徵只在其成員出現而非其分支的形態）為其分支的特點。

部分特點如下：

  - 眼睛在眼柄上，有玻璃體的主體。
  - 有一對下頜，其位置不受（buccal mass）所限制。
  - 在[心包右邊有一個](../Page/心包.md "wikilink")[腎臟](../Page/腎臟.md "wikilink")。
  - 一個靈活的[齒舌](../Page/齒舌.md "wikilink")（radula）膜。齒舌為蝸牛的[舌頭](../Page/舌頭.md "wikilink")，作為[銼一類的](../Page/銼.md "wikilink")[工具](../Page/工具.md "wikilink")。
  - 單一的嗅覺器官（osphradium）。
  - 嗅覺器官有橫向纖毛的區域。
  - 單一在左方的鰓下腺（在鰓的器官，會釋放分泌物，例如紅色的染料[泰爾紫](../Page/泰爾紫.md "wikilink")（Tyrian
    purple））。
  - 單一的[櫛鰓](../Page/櫛鰓.md "wikilink")（ctenidium）（部分軟體動物擁有蜂巢狀的[呼吸系統](../Page/呼吸系統.md "wikilink")）

## 參考文獻

[直腹足亞綱](../Category/直腹足亞綱.md "wikilink")
[Category:棄用生物分類](../Category/棄用生物分類.md "wikilink")

1.
2.
3.
4.
5.
6.