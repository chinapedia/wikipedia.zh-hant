**宝安体育馆**，位于[中国](../Page/中国.md "wikilink")[广东省](../Page/广东省.md "wikilink")[深圳市](../Page/深圳市.md "wikilink")[宝安区](../Page/宝安区.md "wikilink")[罗田路](../Page/罗田路.md "wikilink")[宝安体育中心](../Page/宝安体育中心.md "wikilink")。於2002年9月開館，占地面积8万平方米，建筑面积4.74万平方米。宝安体育馆曾承办[第26届世界大学生夏季运动会的水球](../Page/2011年夏季世界大学生运动会.md "wikilink")、体操与艺术体操等赛事
\[1\]、另外也举办過廣東省第十一屆運動會的[散打](../Page/散打.md "wikilink")，競技[體操等比賽](../Page/體操.md "wikilink")\[2\]和各类演唱会。

## 交通

[深圳地鐵1號線在體育館西側設有](../Page/深圳地鐵1號線.md "wikilink")[寶體站](../Page/寶體站.md "wikilink")，[深圳地鐵11號線在體育場西側設有](../Page/深圳地鐵11號線.md "wikilink")[寶安站](../Page/寶安站.md "wikilink")，而附近亦有多線公交途經。

## 参考资料

<references />

[H](../Category/深圳體育場館.md "wikilink")
[H](../Category/中國籃球場.md "wikilink")
[H](../Category/2002年中國建立.md "wikilink")
[H](../Category/2002年完工體育場館.md "wikilink")
[H](../Category/演唱會場地.md "wikilink")
[H](../Category/寶安區.md "wikilink")

1.
2.