**羅姆**（全稱：**羅姆株式會社**，）是1958年於[日本成立的跨國電子公司](../Page/日本.md "wikilink")。羅姆半導體（ROHM
SEMICONDUCTOR）前稱R.ohm，品牌和商標的「R」代表公司初始產品項目，同時代表可靠性（Reliability），「OHM」代表電阻測量單位歐姆（Ω）。

## 參考文獻

## 外部連結

  - [羅姆全球](https://web.archive.org/web/20180224130117/http://www.rohm.com/web/global/)

  - [羅姆日本](https://web.archive.org/web/20180218082046/http://www.rohm.co.jp/web/japan/)

  - [羅姆中國大陸](https://web.archive.org/web/20180210064446/http://www.rohm.com.cn/web/china/)

  - [羅姆台灣](https://web.archive.org/web/20180210065251/http://www.rohm.com.tw/web/taiwan/)

[Category:日本電子公司](../Category/日本電子公司.md "wikilink")
[Category:總部在日本的跨國公司](../Category/總部在日本的跨國公司.md "wikilink")
[Category:大阪证券交易所上市公司](../Category/大阪证券交易所上市公司.md "wikilink")
[Category:1958年成立的公司](../Category/1958年成立的公司.md "wikilink")