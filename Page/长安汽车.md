**中国长安汽车集团股份有限公司**（），简称**中国长安汽车集团**、**中国长安**，是[中国南方工业集团公司](../Page/中国南方工业集团公司.md "wikilink")、[中国航空工业集团公司两大](../Page/中国航空工业集团公司.md "wikilink")[世界500强](../Page/世界500强.md "wikilink")[中国中央企业对旗下汽车产业进行战略重组](../Page/中国中央企业.md "wikilink")、共同成立的一家特大型企业集团，是中国四大汽车集团之一\[1\]，成立于2005年12月26日。中国长安现拥有长安汽车、[江铃汽车](../Page/江铃汽车.md "wikilink")、[东安动力](../Page/东安动力.md "wikilink")、[济南轻骑](../Page/济南轻骑.md "wikilink")4家上市公司。2012年，中国长安以122万辆的自主品牌汽车销量，位列中国汽车企业第一、全球汽车企业第十四，连续七年蝉联中国第一自主品牌。同时，中国长安也是唯一自主品牌车型产销过百万辆的中国车企。\[2\]

其旗下核心上市公司**重庆长安汽车股份有限公司**（），简称**长安汽车**、**重庆长安**，其历史可以追溯至1862年成立的[上海洋炮局](../Page/上海.md "wikilink")，经过多次改制后改为现名，1983年进入汽车领域，为中国汽车行业第一阵营、第一自主品牌、第一研发实力企业，现有资产1033亿元人民币，员工8万余人。长安汽车现生产[微客](../Page/微客.md "wikilink")、[乘用车等产品](../Page/乘用车.md "wikilink")。\[3\]

## 历史沿革

长安汽车的前身是[上海洋炮局](../Page/上海洋炮局.md "wikilink")，由[洋务运动发起人](../Page/洋务运动.md "wikilink")[李鸿章于](../Page/李鸿章.md "wikilink")1862年（清[同治元年](../Page/同治.md "wikilink")）12月授命英国人[马格里和中国官员](../Page/马格里.md "wikilink")[刘佐禹在上海](../Page/刘佐禹.md "wikilink")[松江府城外一所庙宇中创办](../Page/松江府.md "wikilink")。1865年因李鸿章升任[两江总督](../Page/两江总督.md "wikilink")，而迁到[南京更名为](../Page/南京.md "wikilink")[金陵制造局](../Page/金陵制造局.md "wikilink")，主要生产各种枪炮。1929年更名为[金陵兵工厂](../Page/金陵兵工厂.md "wikilink")。\[4\]

1937年“八·一三”上海事件爆发，[金陵兵工厂被轰炸数次](../Page/金陵兵工厂.md "wikilink")。为准备抗战，兵工厂西迁[重庆簸箕石和](../Page/重庆.md "wikilink")[南岸铜元局](../Page/南岸区.md "wikilink")（今长江电工集团有限公司），并在1938年3月1日恢复生产，在[抗日战争中提供了弹药](../Page/抗日战争.md "wikilink")3000余吨，[手榴弹三十万余发和各类枪械约五十万支](../Page/手榴弹.md "wikilink")，是整个抗战期间最大的兵工企业，在[军统授意下于](../Page/军统.md "wikilink")1942年在[九龙坡区的](../Page/九龙坡.md "wikilink")[杨家坪](../Page/杨家坪.md "wikilink")**绿瓦楼**中（原[国民革命军第](../Page/国民革命军.md "wikilink")8战区司令部所在地，现为[重庆理工大学行政楼](../Page/重庆理工大学.md "wikilink")，**重庆市文物保护单位**）秘密开办了最早的"**士继公学**"，名义上为工厂的私立技工学校，实际是暗中培养抗战武器装备设计人才，为抗战做出了巨大贡献。抗战结束后1946年改名为21厂技校，后来成为[重庆理工大学的前身](../Page/重庆理工大学.md "wikilink")。

中华人民共和国建立后，划归[兵器工业部和](../Page/兵器工业部.md "wikilink")[国防科工委](../Page/国防科工委.md "wikilink")，为保密，以长安代号50开头，编为代号5077的一个研发单位，实际是一所军事工业高等学校，后解密成为[重庆理工大学后](../Page/重庆理工大学.md "wikilink")，电台、校办工厂乃至校园BBS皆一直保留5077的呼号至今。

1957年改制为重庆兵工厂，开始试制长江牌46型吉普车，并在1958年试制成功，并参加了1959年的国庆阅兵仪式。1963年底停产，累计生产1390辆，停产后将技术资料转交[北京汽车制造厂](../Page/北京汽车制造厂.md "wikilink")，成为闻名于世的[北京吉普](../Page/北京吉普.md "wikilink")。

1981年开始长安厂开始尝试生产微车，通过仿制和与[铃木的技术合作成功生产了以铃木ST](../Page/铃木.md "wikilink")90K微型汽车为原型车的微型面包车，俗称“[长安车](../Page/长安车.md "wikilink")”。又在重庆、上海和[意大利](../Page/意大利.md "wikilink")[都灵](../Page/都灵.md "wikilink")、美国[密歇根成立研发机构](../Page/密歇根.md "wikilink")，推出[CM8](../Page/CM8.md "wikilink")，[长安之星](../Page/长安之星.md "wikilink")，奔奔等微型面包车和轿车，并推出自主研发的新车如[长安锐翔](../Page/长安锐翔.md "wikilink")、[长安志翔](../Page/长安志翔.md "wikilink")、[氢混和动力车等产品](../Page/氢混和动力车.md "wikilink")。

2006年1月15日，在长安汽车的主管单位[中国兵器装备集团公司的牵头下以重庆长安为主体成立了中国南方工业汽车股份有限公司](../Page/中国兵器装备集团公司.md "wikilink")，[尹家绪任](../Page/尹家绪.md "wikilink")[中国兵器装备集团公司党组副书记](../Page/中国兵器装备集团公司.md "wikilink")、同时担任[中国南方工业汽车股份有限公司总经理和长安汽车董事长职务](../Page/中国南方工业汽车股份有限公司.md "wikilink")。中国兵器装备集团公司希望通过重组实现旗下汽车和零部件业务在[香港交易所上市](../Page/香港交易所.md "wikilink")。

2009年7月1日，中国南方工业汽车股份有限公司更名为中国长安汽车集团。2009年11月10日，在中国政府的主导下，中国兵器装备集团公司和中国航空工业集团公司对旗下的汽车业务兼并重组，中航汽车全部并入中国长安汽车集团，形成了新的中国长安汽车集团股份有限公司。\[5\]\[6\]

2016年4月17日兩輛長安無人測試車，從[重慶行駛到達北京](../Page/重慶.md "wikilink")，途經2千多公里行駛了六天。\[7\]

2018年9月5日[長安汽車與](../Page/長安汽車.md "wikilink")[鈴木
(公司)及鈴木](../Page/鈴木_\(公司\).md "wikilink")(中國)投資有限公司達成協議，以作價1元人民幣，收購[鈴木
(公司)及鈴木中國分別持有的](../Page/鈴木_\(公司\).md "wikilink")[長安鈴木](../Page/長安鈴木.md "wikilink")40%股權及10%股權，公告顯示，長安鈴木在2018年4月30日的總資產帳面價值約45.3億元（人民幣‧下同），總負債帳面值約48億元，淨資產帳面價值約為負2.7億元。

## 子公司

### 中国长安汽车集团旗下整车制造企业

  - 哈尔滨哈飞汽车工业集团有限公司

### 长安汽车旗下合资公司

[Chang'an_Suzuki_SC7081A_City_Baby_(Suzuki_Alto).jpg](https://zh.wikipedia.org/wiki/File:Chang'an_Suzuki_SC7081A_City_Baby_\(Suzuki_Alto\).jpg "fig:Chang'an_Suzuki_SC7081A_City_Baby_(Suzuki_Alto).jpg")

1993年在重庆市[巴南区鱼洞成立](../Page/巴南区.md "wikilink")[长安铃木](../Page/长安铃木.md "wikilink")，生产长安[奥拓](../Page/鈴木Alto.md "wikilink")、[羚羊](../Page/鈴木Cultus.md "wikilink")、[雨燕](../Page/鈴木Swift.md "wikilink")、[天語SX4等车型](../Page/鈴木SX4.md "wikilink")。由于油耗、售价等诸多方面与其它轿车相比具有绝对的竞争力，大量进入家庭非常合适，有“民众轿车”之称。\[8\]

[CNpolicecarfocus.jpg](https://zh.wikipedia.org/wiki/File:CNpolicecarfocus.jpg "fig:CNpolicecarfocus.jpg")\]\]
2001年4月25日，[福特汽车公司和长安汽车集团在](../Page/福特汽车.md "wikilink")[重庆北部新区成立了](../Page/重庆北部新区.md "wikilink")[长安福特](../Page/长安福特.md "wikilink")，双方各拥有50%的股份，生产[蒙迪欧](../Page/蒙迪欧.md "wikilink")、[嘉年华](../Page/嘉年华.md "wikilink")、[福克斯等车型](../Page/福克斯.md "wikilink")。2005年底和2008年初，[马自达3與](../Page/马自达3.md "wikilink")[馬自達2分別开始在长安福特重庆工厂生产](../Page/馬自達2.md "wikilink")。2006年3月，[马自达汽车公司参股长安福特](../Page/马自达汽车公司.md "wikilink")，股权重组后的长安福特正式更名为“长安福特马自达汽车有限公司（长安福特马自达汽车）”，三方持股比例为：长安50%，福特35%，马自达15%。2012年11月长安福特马自达拆分为长安福特和[长安马自达](../Page/长安马自达.md "wikilink")。\[9\]

2006年瑞典[沃尔沃以技术转让的形式授权给长安福特马自达汽车公司以福克斯生产线生产](../Page/沃尔沃.md "wikilink")[沃尔沃S40](../Page/沃尔沃S40.md "wikilink")、S60和S80车型。长安[江铃汽车位于](../Page/江铃汽车.md "wikilink")[江西](../Page/江西.md "wikilink")[南昌](../Page/南昌.md "wikilink")，以生产自主设计的SUV"陆风"闻名，出口到欧盟。

[重慶長安民生物流](../Page/重慶長安民生物流.md "wikilink")，长安集团与著名的[民生集团的合资企业](../Page/民生集团.md "wikilink")，主要经营成品车和零件的运输。

2010年7月9日，长安与[标致雪铁龙集团成立其合资公司](../Page/标致雪铁龙集团.md "wikilink")[长安标致雪铁龙](../Page/长安标致雪铁龙.md "wikilink")，总部设在[深圳](../Page/深圳.md "wikilink")，生产DS系列车型。\[10\]

## 事业分布

长安汽车在重庆、北京、河北、江苏、江西、浙江等设立9大国内生产基地，在[巴西](../Page/巴西.md "wikilink")、[俄罗斯](../Page/俄罗斯.md "wikilink")、[尼日利亚](../Page/尼日利亚.md "wikilink")、[马来西亚设立国外生产基地](../Page/马来西亚.md "wikilink")；在中国、日本、英国、意大利和美国设立研发中心。\[11\]

## 车系

### 乘用车系（长安汽车）

[缩略图](https://zh.wikipedia.org/wiki/File:Chang'an_Alsvin_sedan_01_China_2012-05-05.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Chang'an_Ben_Ben_01_China_2012-04-29.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Chang'an_CX20_01_China_2012-06-08.JPG "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Chang'an_Eado_01_China_2012-05-27.JPG "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Chang'an_CS35_rear_Auto_Chongqing_2012-06-07.JPG "fig:缩略图")

  - 睿骋
  - CS35
  - CS75
  - 致尚XT
  - 逸动
  - 悦翔V5
  - 悦翔V3
  - CX20
  - CX30（未来）
  - 奔奔
  - 奔奔Mini

### 商用车系（长安欧尚）

[缩略图](https://zh.wikipedia.org/wiki/File:ChangAn.gif "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Changan_SC6378.jpg "fig:缩略图")
[Local_bus_of_Miyun_AF6075_(20150106160559).JPG](https://zh.wikipedia.org/wiki/File:Local_bus_of_Miyun_AF6075_\(20150106160559\).JPG "fig:Local_bus_of_Miyun_AF6075_(20150106160559).JPG")地方公交使用的长安客车，由保定长安客车制造有限公司制造\]\]

  - 欧力威
  - 欧诺
  - 金牛星
  - 新长安之星
  - 长安之星S460
  - 长安之星2
  - 长安星光4500
  - 星光小卡
  - 星卡
  - CX70/CX70T
  - X70A

### 商用车系（长安轻型车）

  - 睿行S50
  - 尊行
  - 睿行M90
  - 睿行M80
  - 神骐F50
  - 神骐F30
  - 神骐T20

### 已停售车系

  - 杰勋
  - 志翔
  - CM8
  - 长安之星

## 争议事件

2016年1月，北京市环保局抽查长安汽车在北京销售的车型时发现，旗下的CS75
1.8T和睿骋因不符合环保生产一致性要求，排放超标而被开出378万元人民币的罚单，并没收在北京市场销售所得金额1260.5万元，累计为1638.5万元\[12\]。

## 参考文献

## 外部链接

  - [中国长安汽车集团](http://www.ccag.cn)

  - [长安汽车](http://www.changan.com.cn)

  -
  -
  - [长安汽车搜狐微博](http://t.sohu.com/chana)

  - [长安汽车网易微博](https://web.archive.org/web/20140413145012/http://t.163.com/chana_auto)

## 参见

  - [胡連精密](../Page/胡連精密.md "wikilink")
  - [中國汽車製造業](../Page/中國汽車製造業.md "wikilink")
  - [世界汽車工業國際協會](../Page/世界汽車工業國際協會.md "wikilink")

{{-}}

[长安汽车集团](../Category/长安汽车集团.md "wikilink")
[Category:中国汽车公司](../Category/中国汽车公司.md "wikilink")
[Category:中国兵器装备集团子公司](../Category/中国兵器装备集团子公司.md "wikilink")
[Category:中国航空工业集团子公司](../Category/中国航空工业集团子公司.md "wikilink")
[Category:总部位于北京市的中华人民共和国国有企业](../Category/总部位于北京市的中华人民共和国国有企业.md "wikilink")
[Category:总部在重庆的中华人民共和国国有公司](../Category/总部在重庆的中华人民共和国国有公司.md "wikilink")
[Category:1862年成立的公司](../Category/1862年成立的公司.md "wikilink")
[Category:2005年成立的公司](../Category/2005年成立的公司.md "wikilink")

1.  [New policy to encourage China's carmaker
    consolidation](http://news.xinhuanet.com/english2010/china/2010-02/22/c_13182876.htm)
    xinhuanet.com, 2010-02-22 10:27:20

2.  [中国长安企业简介](http://www.ccag.cn/about.do?action=company)

3.  [公司介绍](http://www.changan.com.cn/lovely.do?id=201011100549356535&m=1)


4.  [发展历程](http://www.changan.com.cn/lovely.do?action=read&cid=201011100338433377&m=1)


5.  [New policy to encourage China's carmaker
    consolidation](http://news.xinhuanet.com/english2010/china/2010-02/22/c_13182876.htm)
    xinhuanet.com, 2010-02-22 10:27:20;[Year in review: A memorable 2009
    saw China's ascent to top of global vehicle
    market](http://www.chinadaily.com.cn/cndy/2010-04/24/content_9769992.htm)
    chinadaily.com.cn, 2010-04-24

6.  [Milestone merger reshapes
    Suzuki](http://www.chinadaily.com.cn/bizchina/2010-03/29/content_9655056.htm)
    chinadaily.com.cn, 2010-03-29 09:26

7.  [长安无人驾驶汽车历经6天行驶千里顺利进京](https://www.youtube.com/watch?v=tsxXTnNRt04&list=PLB3Ii-4FPd04xkCkJEX_VScJuhXMljNrf&index=10)

8.
9.

10. [Peugeot and Changan Automotive finalise joint
    venture](http://news.bbc.co.uk/1/hi/business/10569058.stm)
    bbc.co.uk, 11:04 GMT, Friday, 9 July 2010

11. [生产布局](http://www.changan.com.cn/lovely.do?action=read&cid=201011100343040000&m=1)

12.