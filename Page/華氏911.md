《**華氏911**》（），[美國導演](../Page/美國.md "wikilink")[麥可·摩爾於](../Page/麥可·摩爾.md "wikilink")2004年拍攝的[紀錄片](../Page/紀錄片.md "wikilink")。影片从批判的角度描述了[美國總統](../Page/美國總統.md "wikilink")[喬治·沃克·布希](../Page/喬治·沃克·布希.md "wikilink")、[反恐戰爭和对新媒体的控制](../Page/反恐戰爭.md "wikilink")，也是[美國票房记录最高的纪录片](../Page/美國.md "wikilink")。

在影片中，麦克·摩尔指美国的各新闻集团在2003年美国出兵伊拉克的战争中扮演了啦啦队的角色，而没有客观准确地反应战争造成的伤亡和损失。影片激起了激烈争论，包括电影中的细节是否准确的争论。

2004年本片在[坎城影展首映](../Page/坎城影展.md "wikilink")，影片结束后获得了观众20分钟的起立鼓掌，并獲得[金棕櫚獎](../Page/金棕櫚獎.md "wikilink")。

影片名称源于Ray
Bradbury1945年出版的[反乌托邦小说](../Page/反乌托邦.md "wikilink")《[华氏451](../Page/華氏451度.md "wikilink")》，小说描述了在未来的美国，书籍被焚烧，华氏451度是纸张的自燃温度。“华氏911”取了911事件的日期，副标题是：自由燃烧的温度。

## 延伸閱讀

  -
## 外部連結

  -
  -   - [Notes and
        sources](https://web.archive.org/web/20040715020956/http://www.michaelmoore.com/warroom/)

  -
  -
  -
[Category:2004年電影](../Category/2004年電影.md "wikilink")
[Category:美國紀錄片](../Category/美國紀錄片.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:麥可·摩爾電影](../Category/麥可·摩爾電影.md "wikilink")
[Category:金棕櫚獎獲獎電影](../Category/金棕櫚獎獲獎電影.md "wikilink")