**[清朝](../Page/清朝.md "wikilink")**版圖遼闊，民族眾多，在**[行政區劃](../Page/行政區劃.md "wikilink")**上「因時順地、變通斟酌」\[1\]。大體上，清朝在[漢地實行](../Page/内地十八省.md "wikilink")[行省制](../Page/行省.md "wikilink")，在[東北地區和](../Page/中国东北地区.md "wikilink")[新疆實行](../Page/新疆省_\(清\).md "wikilink")[軍府制](../Page/駐劄大臣.md "wikilink")，在[蒙古實行](../Page/清代蒙古.md "wikilink")[盟旗制](../Page/清代蒙古盟部旗列表.md "wikilink")，在[西藏遵循元朝以來的行政區劃設置](../Page/清朝治藏歷史.md "wikilink")。

清末，在列強蠶食鯨吞的形勢下，邊疆各地依靠舊有的行政體制已無法維持有效的統治。[光绪年間](../Page/光绪.md "wikilink")，[新疆](../Page/新疆省_\(清\).md "wikilink")、[台灣與](../Page/福建臺灣省.md "wikilink")[奉天](../Page/奉天省.md "wikilink")、[吉林](../Page/吉林省_\(清\).md "wikilink")、[黑龍江相繼建省](../Page/黑龍江省_\(清\).md "wikilink")，實行與[漢地相同的行政體制](../Page/内地十八省.md "wikilink")。[蒙古](../Page/清代蒙古.md "wikilink")、[西藏](../Page/清朝治藏歷史.md "wikilink")、[海南也有建省之議](../Page/海南岛.md "wikilink")，但在[清朝滅亡之前未能實行](../Page/清朝.md "wikilink")。

## 疆域

[乾隆二十四年](../Page/乾隆.md "wikilink")（1759年）之后的清朝疆域：北起[蒙古](../Page/蒙古高原.md "wikilink")[唐努乌梁海地区及](../Page/唐努乌梁海.md "wikilink")[西伯利亚](../Page/西伯利亚.md "wikilink")，南至[南海](../Page/南海.md "wikilink")，包括“千里石塘、万里长沙、曾母暗沙”（今[南海诸岛](../Page/南海诸岛.md "wikilink")），西南达[西藏的](../Page/西藏自治区.md "wikilink")[达旺地区](../Page/达旺地区.md "wikilink")、[云南省的](../Page/云南省.md "wikilink")[南坎](../Page/南坎.md "wikilink")、[江心坡地区等](../Page/江心坡.md "wikilink")[缅甸北部](../Page/缅甸.md "wikilink")，西尽[鹹海与](../Page/鹹海.md "wikilink")[帕米爾高原地区](../Page/帕米爾高原.md "wikilink")，包括今[新疆以及](../Page/新疆维吾尔自治区.md "wikilink")[中亚的](../Page/中亚.md "wikilink")[巴尔喀什湖](../Page/巴尔喀什湖.md "wikilink")，东北抵[外兴安岭](../Page/外兴安岭.md "wikilink")，包括[库页岛](../Page/库页岛.md "wikilink")，东南包括[臺灣](../Page/臺灣.md "wikilink")、[澎湖群島](../Page/澎湖群島.md "wikilink")。

清朝最大國土面積達1300多萬平方公里，在中國歷代為第二，并保持了一百多年。晚清國土面積縮減至約1107萬平方公里，不過還是屬俄國之外的世界第二大國，清朝國土之遼闊，由此可以想見。

## 沿革

### 漢地

清代[山海關以內](../Page/山海关.md "wikilink")、[长城以南的漢族地區被稱為](../Page/长城.md "wikilink")「漢地」、「關內」或「內地」。漢地的行政區劃承襲明代「省—府（州）—縣」的層級體制。一級政區為[布政使司](../Page/承宣布政使司.md "wikilink")，通稱“[行省](../Page/行省.md "wikilink")”或“省”。二級政區為府、[直隸州](../Page/直隸州.md "wikilink")。府管轄的州（散州、屬州）不再領縣，形成了單式的三級制。清代初年，原為臨時差官的[巡撫取代](../Page/巡撫.md "wikilink")[布政使](../Page/布政使.md "wikilink")，成為一省的長官。在一些民族雜居之處及戰略要地，設置了新型政區「[廳](../Page/厅_\(行政区划\).md "wikilink")」，分為省直轄的[直隸廳和府轄的](../Page/直隸廳.md "wikilink")[散廳](../Page/厅_\(行政区划\).md "wikilink")。少數直隸廳下轄縣。

明代布政使司、[按察使司派出的差官](../Page/提刑按察司.md "wikilink")「[道員](../Page/道員.md "wikilink")」，在清代也保留下來。道員的統轄區域是「道」，介於省與府之間，有分巡道、分守道、糧儲道、鹽法道、兵備道等名目。清初的道並不是行政區，道員亦無品級。乾隆以後，定道員秩品為正四品，分巡道、分守道的職權也漸趨一致。有的道下直接領縣。有人認為清末的道實際上已成為省、府之間的行政區劃\[2\]。

清代漢地政區的層級關係為：



#### 行省

在行省設置方面，基本沿襲明代所置的兩京與十三布政使司，即南北二京及山東、山西、河南、陝西、浙江、福建、江西、廣東、廣西、湖廣、四川、雲南、貴州。順治元年（1644年）定鼎北京，以[盛京為留都](../Page/沈阳市.md "wikilink")\[3\]。二年（1645年）改[北直隶為](../Page/北直隶.md "wikilink")[直隶省](../Page/直隶省.md "wikilink")，改[南直隸為](../Page/南直隸.md "wikilink")[江南省](../Page/江南省.md "wikilink")。康熙三年（1664年），分湖廣為湖北、湖南二省。康熙六年（1667年），江南省正式分為江蘇、安徽二省。康熙七年，陝西省正式分為陝西、甘肅二省，自此形成了所謂「漢地十八省」的格局。

  - [直隶省](../Page/直隶省.md "wikilink")
  - [江蘇省](../Page/江蘇省_\(清\).md "wikilink")
  - [安徽省](../Page/安徽省_\(清\).md "wikilink")
  - [山西省](../Page/山西省_\(清\).md "wikilink")
  - [山東省](../Page/山東省_\(清\).md "wikilink")
  - [河南省](../Page/河南省_\(清\).md "wikilink")

<!-- end list -->

  - [陝西省](../Page/陝西省_\(清\).md "wikilink")
  - [甘肅省](../Page/甘肅省_\(清\).md "wikilink")
  - [浙江省](../Page/浙江省_\(清\).md "wikilink")
  - [江西省](../Page/江西省_\(清\).md "wikilink")
  - [湖北省](../Page/湖北省_\(清\).md "wikilink")
  - [湖南省](../Page/湖南省_\(清\).md "wikilink")

<!-- end list -->

  - [四川省](../Page/四川省_\(清\).md "wikilink")
  - [福建省](../Page/福建省_\(清\).md "wikilink")
  - [廣東省](../Page/廣東省_\(清\).md "wikilink")
  - [廣西省](../Page/廣西省_\(清\).md "wikilink")
  - [雲南省](../Page/云南省_\(清\).md "wikilink")
  - [貴州省](../Page/貴州省_\(清\).md "wikilink")

光緒十一年（1885年），分福建省[臺灣府置](../Page/臺灣府.md "wikilink")[臺灣省](../Page/福建臺灣省.md "wikilink")。兩年後臺灣正式建省，稱「[福建臺灣省](../Page/福建臺灣省.md "wikilink")」。光緒二十一年（1895年），因[甲午战争戰敗](../Page/甲午战争.md "wikilink")，臺灣省被割讓予[日本](../Page/日本.md "wikilink")，遂被廢除。光緒三十年十二月（1905年1月），分江蘇江寧、淮安、揚州、徐州四府及通州、海州二直隸州置[江淮省](../Page/江淮省.md "wikilink")，旋即裁撤。此後至清末，內地仍為十八省，與[東三省](../Page/東三省.md "wikilink")、[新疆省合為二十二省](../Page/新疆省_\(清\).md "wikilink")。

#### 衝繁疲難

清代的府、州、廳、縣，按照「[衝、繁、疲、難](../Page/衝繁疲難.md "wikilink")」的考語分為不同等次。考語字數越多，地位就越重要。一般以四字俱全者為「最要缺」，三字者（衝繁難、衝疲難、繁疲難）為「要缺」，二字者（衝繁、繁難、繁疲、疲難、衝難、衝疲）為「中缺」、一字或無字者為「簡缺」\[4\]。

  - 衝：地當孔道者為衝
  - 繁：政務紛紜者為繁
  - 疲：賦多逋欠者為疲
  - 難：民刁俗悍，命盜案多者為難

#### 土司

雲南、貴州、廣西、四川、湖南、湖北、甘肅等省設有[土司](../Page/土司.md "wikilink")，分為[宣慰司](../Page/宣慰使司都元帅府.md "wikilink")、宣撫司、招討司、安撫司和長官司（長官為武職），與土府、土州、土縣（長官為文職）。土司的長官以當地各族頭人充任，可以世襲，由朝廷或地方官府頒給印信，歸所在地方之督撫、[駐紮大臣管轄](../Page/駐紮大臣.md "wikilink")。宣慰等司的長官隸屬於[兵部](../Page/兵部.md "wikilink")、土知府、土知州等官隸屬於[吏部](../Page/吏部.md "wikilink")\[5\]。雍正年間，雲南、貴州、廣西等省的土司開始改行流官制，史稱[改土歸流](../Page/改土归流.md "wikilink")。光緒、宣統之際，[赵尔丰出任川滇邊務大臣](../Page/赵尔丰.md "wikilink")，四川西部的藏人土司、西藏東部的宗也開始改土歸流。

### 東三省

東北為清朝龍興之地。[顺治年間](../Page/顺治.md "wikilink")[入關後](../Page/入關戰爭.md "wikilink")，以駐防八旗留守盛京[瀋陽](../Page/沈阳市.md "wikilink")。康熙至乾隆年間，逐漸形成三個將軍轄區：**盛京**（奉天）、**吉林**、**黑龍江**，地位異於內地之行省。將軍之下設[專城副都統分駐各城](../Page/都統.md "wikilink")，並管理各城的臨近地區。副都統下有[總管統領各旗](../Page/总管.md "wikilink")。在漢民聚居之處，置府、州、縣、廳，如同關內。居於[黑龍江](../Page/黑龙江.md "wikilink")、[嫩江中上游的](../Page/嫩江.md "wikilink")[巴爾虎](../Page/巴爾虎.md "wikilink")、[達斡爾](../Page/达斡尔族.md "wikilink")、[索倫](../Page/索伦部.md "wikilink")（鄂溫克）、[鄂倫春](../Page/鄂伦春族.md "wikilink")、[錫伯等族](../Page/锡伯族.md "wikilink")，編入八旗，由[布特哈總管](../Page/布特哈總管.md "wikilink")、[呼倫貝爾總管管轄](../Page/呼倫貝爾總管.md "wikilink")。黑龍江、[烏蘇里江下游及](../Page/烏蘇里江.md "wikilink")[庫頁島的](../Page/庫頁島.md "wikilink")[赫哲](../Page/赫哲.md "wikilink")、[費雅喀](../Page/尼夫赫人.md "wikilink")、庫頁、奇楞等漁獵部落則分設姓長、鄉長，由[三姓副都統管轄](../Page/三姓副都統.md "wikilink")。

  - [盛京](../Page/盛京.md "wikilink")，由[盛京将军統轄](../Page/盛京将军.md "wikilink")
      - [奉天府](../Page/奉天府.md "wikilink") / 盛京副都統
      - [锦州府](../Page/锦州府.md "wikilink") / 錦州副都統
      - [熊岳副都統](../Page/熊岳驻防八旗.md "wikilink")
      - 金州副都統
      - 興京副都統

<!-- end list -->

  - [吉林](../Page/吉林.md "wikilink")，由[吉林將軍統轄](../Page/吉林將軍.md "wikilink")
      - 寧古塔副都統
      - 伯都訥副都統
      - 三姓副都統
      - 吉林副都統
      - 阿勒楚喀副都統
      - 拉林副都統
      - 琿春副都統

<!-- end list -->

  - [黑龍江](../Page/黑龍江.md "wikilink")，由[黑龍江將軍統轄](../Page/黑龍江將軍.md "wikilink")
      - 黑龍江副都統
      - 墨爾根副都統
      - 齊齊哈爾副都統
      - 布特哈副都統
      - 呼蘭副都統
      - 呼倫貝爾副都統
      - 通肯副都統

光緒末年的[甲午戰爭](../Page/甲午戰爭.md "wikilink")、[八國聯軍之役與](../Page/八國聯軍之役.md "wikilink")[日俄戰爭嚴重動搖了清朝在東北地區的統治](../Page/日俄戰爭.md "wikilink")，迫使其廢除滿洲的旗民分治制度，設立行省。光緒三十三年（1907年），廢除盛京、吉林、黑龍江三地將軍衙門，改設[奉天省](../Page/奉天省.md "wikilink")、[吉林省](../Page/吉林省.md "wikilink")、[黑龍江省](../Page/黑龍江省.md "wikilink")；隨後裁撤各城副都統、總管，改為府、廳、州、縣。宣統三年（1911年），奉天省領八府、八廳、六州、三十三縣；吉林省領十一府、一州、五廳、十八縣；黑龍江省領七府、六廳、一州、七縣\[6\]。

### 藩部

清代蒙古、西藏、青海、新疆与黑龙江[布特哈](../Page/布特哈.md "wikilink")（[达呼尔](../Page/达斡尔族.md "wikilink")、[索伦](../Page/索伦部.md "wikilink")、[鄂伦春等族](../Page/鄂伦春族.md "wikilink")）被称为[藩部](../Page/藩部.md "wikilink")，由[理藩院管理](../Page/理藩院.md "wikilink")。

#### 蒙古

明清之際，蒙古分為眾多部落（蒙古語稱為“艾馬克”），部落首領為“部長”（鄂拓克）或“汗”。[清太宗時](../Page/皇太極.md "wikilink")，依照滿洲[八旗的組織形式](../Page/八旗制度.md "wikilink")，將蒙古各部落編為[旗](../Page/旗_\(行政区划\).md "wikilink")，完成[建旗划界后](../Page/建旗划界.md "wikilink")，因各旗牧地固定，“旗”成为蒙古地区的基本行政單位，其長官為[扎薩克或](../Page/扎薩克.md "wikilink")[總管](../Page/總管.md "wikilink")。旗下設“佐”（[蘇木](../Page/苏木_\(行政区划\).md "wikilink")），相當於[鄉](../Page/乡级行政区.md "wikilink")。自此蒙古各部落被納入統一的行政體系之中。在地域上，蒙古地區大致分為[察哈爾](../Page/察哈尔部.md "wikilink")、[內蒙古](../Page/內蒙古.md "wikilink")、[西套蒙古](../Page/西套蒙古.md "wikilink")、[外蒙古](../Page/外蒙古.md "wikilink")（包括[土謝圖汗部](../Page/土謝圖汗部.md "wikilink")、[賽音諾顏部](../Page/賽音諾顏部.md "wikilink")、[車臣汗部](../Page/車臣汗部.md "wikilink")、[札薩克圖汗部](../Page/札薩克圖汗部.md "wikilink")）、[科布多與](../Page/科布多.md "wikilink")[唐努乌梁海](../Page/唐努乌梁海.md "wikilink")。

清代蒙古又分為[內屬蒙古與](../Page/內屬蒙古.md "wikilink")[外藩蒙古](../Page/外藩蒙古.md "wikilink")。內屬蒙古包括察哈爾、[歸化城土默特](../Page/歸化城土默特.md "wikilink")、唐努烏梁海、[阿爾泰烏梁海等部](../Page/阿爾泰烏梁海.md "wikilink")，各旗由朝廷派遣官員（一般為總管）治理，與內地的[州](../Page/州.md "wikilink")、[縣無異](../Page/縣.md "wikilink")。外藩蒙古各旗則由當地的世襲[札薩克管理](../Page/札薩克.md "wikilink")，處於半自治狀態。在外藩蒙古，以若干旗合為一[盟](../Page/盟.md "wikilink")，設正、副盟長，掌管會盟事宜，對各旗札薩克進行監管。清代的盟是監察機構，並不能視為一級政區。

外藩蒙古又按其歸附清朝的先後分為[內札薩克蒙古與](../Page/內札薩克蒙古.md "wikilink")[外札薩克蒙古](../Page/外札薩克蒙古.md "wikilink")。內札薩克蒙古又被稱為**內蒙古**，於天命至康熙初年陸續歸附清朝。乾隆以後定為二十四部，共四十九旗，設六盟。內札薩克各旗不但政治地位很高，還保留了一定的兵權。康熙中期以後歸附清朝的各部落稱為外札薩克蒙古，包括漠北的喀爾喀四部、[西套蒙古二旗](../Page/西套蒙古.md "wikilink")、青海蒙古各部、[科布多各札薩克旗](../Page/科布多.md "wikilink")、新疆[舊土爾扈特部及](../Page/舊土爾扈特部.md "wikilink")[中路和碩特部](../Page/中路和碩特部.md "wikilink")。外札薩克各旗無兵權，隸屬於當地的將軍、[都統](../Page/都統.md "wikilink")、[駐紮大臣](../Page/駐紮大臣.md "wikilink")（西套蒙古二旗除外）。其中喀爾喀四部後來演變為**外蒙古**。

#### 青海

清代的青海不包括今[西寧](../Page/西宁市.md "wikilink")、[海東](../Page/海东市.md "wikilink")、[黃南以及](../Page/黄南藏族自治州.md "wikilink")[青海省邊緣的部分地區](../Page/青海省.md "wikilink")。統轄青海地方的官員為[西寧辦事大臣](../Page/西寧辦事大臣.md "wikilink")，常駐西寧（屬[甘肅省](../Page/甘肅省.md "wikilink")）。青海大致以[黃河為界](../Page/黄河.md "wikilink")，分為**青海蒙古**和**玉樹等四十族土司**。黃河以北主要為蒙古人，有[和碩特](../Page/和硕特.md "wikilink")、[輝特](../Page/輝特部.md "wikilink")、[綽羅斯](../Page/绰罗斯.md "wikilink")（準噶爾）、[土爾扈特](../Page/土尔扈特.md "wikilink")、[喀爾喀五大部落](../Page/喀尔喀蒙古.md "wikilink")。雍正三年（1725年），編青海蒙古為二十七旗，後增至二十九旗，由西寧辦事大臣主持會盟。另有[察漢諾門罕牧地](../Page/夏茸尕布活佛.md "wikilink")，實際上單獨為一喇嘛旗。[道光三年](../Page/道光.md "wikilink")（1823年），分黃河以北二十四旗為左、右翼二[盟](../Page/盟.md "wikilink")，每盟設正、副盟長各一人。

黃河以南主要為藏人，設有四十個土司，其中以[玉樹土司最大](../Page/玉樹等四十族.md "wikilink")，故稱玉樹等四十族土司。土司以下有土千戶、土百戶。嘉慶、道光年間，藏人不斷越過黃河向北遷徙，形成了環[青海湖一帶的](../Page/青海湖.md "wikilink")**[環海八族](../Page/環海八族.md "wikilink")**。

#### 西藏

西藏在清代又稱“唐古忒”、“圖伯特”，分為**[衛](../Page/衛藏.md "wikilink")**、**喀木**（[康](../Page/康區.md "wikilink")）、**[藏](../Page/衛藏.md "wikilink")**、**[阿里](../Page/阿里地区.md "wikilink")**四部，以及**[霍爾三十九族](../Page/霍爾三十九族.md "wikilink")**地區。西藏地方的行政長官為[駐藏大臣](../Page/駐藏大臣.md "wikilink")，駐[喇薩](../Page/拉萨市.md "wikilink")，會同達賴喇嘛、班禪額爾德尼辦理藏內政務。其政令由[噶廈](../Page/噶廈.md "wikilink")（西藏官府）執行。西藏的基層政區是[宗](../Page/宗_\(行政區\).md "wikilink")，大致相當於內地的縣，但規模很小。一些貴族、寺廟的莊園領地稱為“谿卡”，地位比宗低或者平級。宗的長官為“宗本”，谿的長官為“谿堆”，一般由噶廈委派，也有的由特定寺廟委任。後藏[札什倫布附近的幾個宗](../Page/扎什伦布寺.md "wikilink")，由班禪直接管理。

今[那曲地區](../Page/那曲地區.md "wikilink")、[昌都地區北部的各部落統稱](../Page/昌都地區.md "wikilink")[霍爾三十九族](../Page/霍爾三十九族.md "wikilink")，簡稱三十九族，為蒙古人後裔，由駐藏大臣的屬員夷情章京管轄。駐紮於達木（今[當雄](../Page/当雄县.md "wikilink")）的[達木蒙古八旗](../Page/達木蒙古.md "wikilink")，每旗設一佐領，不設總管，直屬於駐藏大臣。

#### 新疆

清代[新疆分為](../Page/新疆维吾尔自治区.md "wikilink")[天山北路的](../Page/天山山脉.md "wikilink")[準部和天山南路的](../Page/准噶尔部.md "wikilink")[回部](../Page/回部.md "wikilink")，統屬於[伊犁將軍](../Page/伊犁將軍.md "wikilink")。其中的蒙古遊牧地區實行盟旗制。[維吾爾](../Page/维吾尔族.md "wikilink")、[布魯特](../Page/柯尔克孜族.md "wikilink")、[塔吉克等族地區則實行](../Page/塔吉克族.md "wikilink")[伯克制](../Page/伯克制.md "wikilink")。蒙古[舊土爾扈特部與](../Page/舊土爾扈特部.md "wikilink")[中路和碩特部設立旗](../Page/巴圖塞特奇勒圖盟.md "wikilink")、盟：舊土爾扈特部為南北東西四路[烏訥恩素珠克圖盟](../Page/舊土爾扈特部.md "wikilink")，和碩特部為[巴圖塞特奇勒圖盟](../Page/巴圖塞特奇勒圖盟.md "wikilink")。凖部地方設[烏魯木齊都統](../Page/烏魯木齊都統.md "wikilink")，統轄烏魯木齊（[迪化州](../Page/迪化州.md "wikilink")）、[庫爾喀喇烏蘇](../Page/庫爾喀喇烏蘇.md "wikilink")、[吐魯番](../Page/吐鲁番厅.md "wikilink")、[哈密](../Page/哈密廳.md "wikilink")、[古城](../Page/奇台县.md "wikilink")、[巴里坤](../Page/巴里坤哈萨克自治县.md "wikilink")（[鎮西府](../Page/镇西府_\(清朝\).md "wikilink")）等城。其中迪化州、鎮西府由新疆與[甘肅省雙重管轄](../Page/甘肅省.md "wikilink")。[塔爾巴哈臺由塔爾巴哈臺參贊大臣管轄](../Page/塔爾巴哈臺.md "wikilink")。伊犁及其以西地方由伊犁參贊大臣、領隊大臣管理。回部設總理回疆事務大臣（喀什噶爾參贊大臣），統轄[喀什噶爾](../Page/喀什市.md "wikilink")、[葉爾羌](../Page/葉爾羌.md "wikilink")、[和闐](../Page/和闐.md "wikilink")、[阿克蘇](../Page/阿克苏市.md "wikilink")、[烏什](../Page/乌什县.md "wikilink")、[庫車](../Page/庫車直隸州.md "wikilink")、[喀喇沙爾等城](../Page/焉耆府.md "wikilink")。光緒十年（1884年），[新疆建省](../Page/新疆省_\(清\).md "wikilink")，實行與內地相同的府、廳、州、縣體制。

<table>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><strong>新疆建省前的區域劃分</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>將軍</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p><a href="../Page/伊犁將軍.md" title="wikilink">伊犁將軍</a></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>伊犁參贊大臣</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>塔爾巴哈臺參贊大臣</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/烏魯木齊都統.md" title="wikilink">烏魯木齊都統</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>哈密辦事大臣[7]</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>吐魯番領隊大臣</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>巴里坤領隊大臣</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>古城領隊大臣</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>庫爾喀喇烏蘇領隊大臣</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>總理回疆事務參贊大臣<br />
（喀什噶爾參贊大臣[8]）</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>英吉沙爾領隊大臣</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>葉爾羌辦事大臣[9]</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>和闐辦事大臣[10]</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>阿克蘇辦事大臣</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>烏什辦事大臣</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>庫車辦事大臣</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>喀喇沙爾辦事大臣</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

## 屬國

清朝曾有的[屬國主要包含](../Page/附庸國.md "wikilink")（非同時期）：[朝鲜](../Page/李氏朝鲜.md "wikilink")、[琉球](../Page/琉球国.md "wikilink")、[安南](../Page/阮朝.md "wikilink")（越南）、[南掌](../Page/琅勃拉邦王国.md "wikilink")（老挝）、[暹罗](../Page/泰国.md "wikilink")（泰國）、[缅甸](../Page/贡榜王朝.md "wikilink")、[廓尔喀](../Page/尼泊爾王國.md "wikilink")（尼泊尔）、[哲孟雄](../Page/锡金王国.md "wikilink")（锡金）、[浩罕](../Page/浩罕汗国.md "wikilink")、[哈萨克](../Page/哈萨克汗国.md "wikilink")、[安集延](../Page/安集延.md "wikilink")、[玛尔噶朗](../Page/玛尔噶朗.md "wikilink")、[那木干](../Page/那木干.md "wikilink")、[塔什干](../Page/塔什干.md "wikilink")、[巴达克山](../Page/巴达克山.md "wikilink")、[博罗尔](../Page/博罗尔.md "wikilink")、[阿富汗](../Page/杜蘭尼王朝.md "wikilink")、[坎巨提](../Page/坎巨提.md "wikilink")、[蘇祿國](../Page/蘇祿蘇丹國.md "wikilink")。

屬國在列強勢力開始進入遠東後盡數獨立出清朝或是遭列強佔領；中亞的屬國先後被[俄罗斯帝国佔領](../Page/俄罗斯帝国.md "wikilink")，南亞的屬國及緬甸則成為[大英帝国的](../Page/大英帝国.md "wikilink")[保护国](../Page/保护国.md "wikilink")，安南、南掌則成為法國的保護國，而暹羅後也脫離屬國；東亞部份，[琉球被日本占领](../Page/琉球.md "wikilink")；[朝鲜則在](../Page/李氏朝鲜.md "wikilink")[甲午戰爭後宣布獨立為](../Page/甲午戰爭.md "wikilink")[大韓帝國](../Page/大韓帝國.md "wikilink")。

## 領土變遷

### 領土獲取

### 領土割讓

參見：[沙俄和苏联割占中国领土列表](../Page/沙俄和苏联割占中国领土列表.md "wikilink")，[七子之歌](../Page/七子之歌.md "wikilink")

注：本表僅列出在清朝時**開始**的領土變遷，民國及之後**開始**的變遷（如[黑瞎子島](../Page/黑瞎子岛.md "wikilink")，見本章節最下）不列入在內。若清朝開始的領土變遷在之後的朝代中亦有變遷，僅列出截止**最後**與清朝繼承國（[中華民國](../Page/中華民國.md "wikilink")、[中華人民共和國](../Page/中华人民共和国.md "wikilink")）有關的變遷。如香港/澳門主權移交列入於內，而外東北割讓予[沙皇俄國後不再追蹤](../Page/沙皇俄国.md "wikilink")（因之後此地不再與中國有關聯）。另外兩岸皆知的歷史事件，比如1949年[大陸地區政權變遷亦不列入於內](../Page/三大战役.md "wikilink")。

<table>
<thead>
<tr class="header">
<th><p>地區</p></th>
<th><p>今屬於</p></th>
<th><p>事件日期</p></th>
<th><p>獲取方</p></th>
<th><p>事件</p></th>
<th><p>類型</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/香港島.md" title="wikilink">香港島</a></p></td>
<td><p><a href="../Page/香港.md" title="wikilink">香港特區香港島全境</a></p></td>
<td><p>1841年 1941年</p>
<p>1945年</p>
<p>1997年7月1日</p></td>
<td><p>大英帝國 大日本帝國</p>
<p>大英帝國</p>
<p>中華人民共和國</p></td>
<td><p>《<a href="../Page/南京条约.md" title="wikilink">南京条约</a>》 <a href="../Page/第二次世界大战.md" title="wikilink">二戰</a><a href="../Page/香港保衛戰.md" title="wikilink">香港保衛戰</a></p>
<p><a href="../Page/第二次世界大战.md" title="wikilink">二戰日本戰敗</a></p>
<p><a href="../Page/香港回歸.md" title="wikilink">香港主權移交</a></p></td>
<td><p>割讓 佔領</p>
<p>佔領</p>
<p>回歸</p></td>
<td><p>1841-1941割讓予英，1941-1945日佔，1945-1997再次歸英，1997年7月1日主權移交至中華人民共和國。 香港島原屬<a href="../Page/广东省.md" title="wikilink">廣東省</a><a href="../Page/寶安縣.md" title="wikilink">寶安縣</a>，乃<a href="../Page/英屬香港.md" title="wikilink">英屬香港之最初部分</a>。</p>
<p><a href="../Page/闻一多.md" title="wikilink">聞一多的</a><a href="../Page/七子之歌.md" title="wikilink">七子之歌中包括該地</a>。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黑龙江.md" title="wikilink">黑龍江以北</a></p></td>
<td><p>俄羅斯<a href="../Page/阿穆尔州.md" title="wikilink">阿穆爾州大部分地區</a> 俄羅斯<a href="../Page/哈巴罗夫斯克边疆区.md" title="wikilink">哈巴羅夫斯克邊疆區部分</a></p>
<p>俄羅斯<a href="../Page/犹太自治州.md" title="wikilink">猶太自治州</a></p></td>
<td><p>1858年</p></td>
<td><p>沙皇俄國</p></td>
<td><p>《<a href="../Page/璦琿條約.md" title="wikilink">璦琿條約</a>》</p></td>
<td><p>割讓</p></td>
<td><p>原清朝黑龍江省北部，清朝吉林省北部。 北界為<a href="../Page/外兴安岭.md" title="wikilink">外興安嶺</a>，原為清朝與沙皇俄國最東段的邊界。包括<a href="../Page/海兰泡.md" title="wikilink">海蘭泡</a>，<a href="../Page/庙街_(俄罗斯).md" title="wikilink">廟街</a>。</p>
<p>不包括<a href="../Page/黑瞎子岛.md" title="wikilink">黑瞎子島</a>（見最下之註解），<a href="../Page/江東六十四屯.md" title="wikilink">江東六十四屯</a>（1900年俄佔，見後）。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/九龍.md" title="wikilink">九龍</a></p></td>
<td><p><a href="../Page/香港.md" title="wikilink">香港特區九龍全境</a>，以及<a href="../Page/昂船洲.md" title="wikilink">昂船洲</a></p></td>
<td><p>1860年 1941年</p>
<p>1945年</p>
<p>1997年7月1日</p></td>
<td><p>大英帝國 大日本帝國</p>
<p>大英帝國</p>
<p>中華人民共和國</p></td>
<td><p>《<a href="../Page/南京条约.md" title="wikilink">南京条约</a>》 <a href="../Page/第二次世界大战.md" title="wikilink">二戰</a><a href="../Page/香港保衛戰.md" title="wikilink">香港保衛戰</a></p>
<p><a href="../Page/第二次世界大战.md" title="wikilink">二戰日本戰敗</a></p>
<p><a href="../Page/香港回歸.md" title="wikilink">香港主權移交</a></p></td>
<td><p>割讓 佔領</p>
<p>佔領</p>
<p>回歸</p></td>
<td><p>1860-1941割讓予英，1941-1945日佔，1945-1997再次歸英，1997年7月1日主權移交至中華人民共和國。 九龍原屬<a href="../Page/广东省.md" title="wikilink">廣東省</a><a href="../Page/寶安縣.md" title="wikilink">寶安縣</a>，乃<a href="../Page/英屬香港.md" title="wikilink">英屬香港之拓展</a>。</p>
<p>包括<a href="../Page/昂船洲.md" title="wikilink">昂船洲</a>，<a href="../Page/昂船洲.md" title="wikilink">昂船洲原為</a><a href="../Page/九龍半島.md" title="wikilink">九龍半島西面的</a><a href="../Page/香港島嶼.md" title="wikilink">島嶼</a>，但經過1995年<a href="../Page/填海.md" title="wikilink">填海後已與</a><a href="../Page/九龍半島.md" title="wikilink">九龍半島連成一片成為當中一部份</a>。</p>
<p><a href="../Page/闻一多.md" title="wikilink">聞一多的</a><a href="../Page/七子之歌.md" title="wikilink">七子之歌中包括該地</a>。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/乌苏里江.md" title="wikilink">烏蘇里江以東</a></p></td>
<td><p>俄羅斯<a href="../Page/滨海边疆区.md" title="wikilink">濱海邊疆區全境</a> 俄羅斯<a href="../Page/哈巴罗夫斯克边疆区.md" title="wikilink">哈巴羅夫斯克邊疆區部分</a></p>
<p>俄羅斯<a href="../Page/萨哈林州.md" title="wikilink">薩哈林州之</a><a href="../Page/库页岛.md" title="wikilink">庫頁島</a></p></td>
<td><p>1860年</p></td>
<td><p>沙皇俄國</p></td>
<td><p>《<a href="../Page/北京条约.md" title="wikilink">北京条约</a>》</p></td>
<td><p>割讓</p></td>
<td><p>原<a href="../Page/吉林省_(清).md" title="wikilink">清朝吉林省大部分</a>。包括<a href="../Page/海參崴.md" title="wikilink">海參崴</a>，<a href="../Page/伯力.md" title="wikilink">伯力以及</a><a href="../Page/库页岛.md" title="wikilink">庫頁島</a>。 不包括<a href="../Page/黑瞎子岛.md" title="wikilink">黑瞎子島</a>（見最下之註解）以及<a href="../Page/南千岛群岛.md" title="wikilink">南千島群島</a>。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/外西北.md" title="wikilink">外西北</a></p></td>
<td><p><a href="../Page/哈萨克斯坦.md" title="wikilink">哈薩克斯坦部分地區</a> <a href="../Page/吉尔吉斯斯坦.md" title="wikilink">吉爾吉斯斯坦部分地區</a></p>
<p><a href="../Page/塔吉克斯坦.md" title="wikilink">塔吉克斯坦部分地區</a></p>
<p><a href="../Page/土库曼苏维埃社会主义共和国.md" title="wikilink">土庫曼斯坦部分地區</a></p>
<p><a href="../Page/乌兹别克苏维埃社会主义共和国.md" title="wikilink">烏茲別克斯坦部分地區</a></p></td>
<td><p>1864年</p></td>
<td><p>沙皇俄國</p></td>
<td><p>《<a href="../Page/中俄勘分西北界约记.md" title="wikilink">中俄勘分西北界约记</a>》</p></td>
<td><p>割讓</p></td>
<td><p>包括<a href="../Page/哈萨克斯坦.md" title="wikilink">哈薩克斯坦最大城市兼前首都</a><a href="../Page/阿拉木图.md" title="wikilink">阿拉木圖</a>。 <a href="../Page/斋桑泊.md" title="wikilink">齋桑泊</a>、<a href="../Page/阿拉湖.md" title="wikilink">阿拉湖</a>、<a href="../Page/伊塞克湖.md" title="wikilink">伊塞克湖</a>、<a href="../Page/巴尔喀什湖.md" title="wikilink">巴爾喀什湖等湖泊也位於這一地區</a>。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/葡屬澳門.md" title="wikilink">澳門</a></p></td>
<td><p><a href="../Page/澳門.md" title="wikilink">澳門特區全境</a></p></td>
<td><p>1887年 1999年12月20日</p></td>
<td><p>葡萄牙 中華人民共和國</p></td>
<td><p>《<a href="../Page/中葡和好通商條約.md" title="wikilink">中葡和好通商條約</a>》 <a href="../Page/澳門回歸.md" title="wikilink">澳門主權移交</a></p></td>
<td><p>租借 回歸</p></td>
<td><p>1557年<a href="../Page/葡萄牙人.md" title="wikilink">葡萄牙人向</a><a href="../Page/明朝.md" title="wikilink">明朝</a><a href="../Page/租借.md" title="wikilink">租借</a>，直至1887年，葡萄牙與清朝簽訂有效期為40年的《中葡和好通商條約》，1999年12月20日葡萄牙將<a href="../Page/澳門主權移交.md" title="wikilink">澳門主權移交</a><a href="../Page/中華人民共和國政府.md" title="wikilink">中華人民共和國政府</a>。</p>
<p><a href="../Page/闻一多.md" title="wikilink">聞一多的</a><a href="../Page/七子之歌.md" title="wikilink">七子之歌中包括該地</a>。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/臺灣省_(清朝).md" title="wikilink">台灣省</a></p></td>
<td><p><a href="../Page/臺灣.md" title="wikilink">台灣島全境及所有附屬各島嶼</a> <a href="../Page/澎湖群島.md" title="wikilink">澎湖列島全境</a></p></td>
<td><p>1895年4月17日 1945年9月2日</p></td>
<td><p>大日本帝國 中華民國</p></td>
<td><p>《<a href="../Page/马关条约.md" title="wikilink">馬關條約</a>》 <a href="../Page/第二次世界大战.md" title="wikilink">二戰日本戰敗</a></p></td>
<td><p>割讓 回歸</p></td>
<td><p>包括<a href="../Page/东亚.md" title="wikilink">東亞爭議島嶼</a><a href="../Page/釣魚臺列嶼.md" title="wikilink">釣魚島及其附屬島嶼</a>（日本稱<a href="../Page/釣魚臺列嶼.md" title="wikilink">尖閣諸島</a>，琉球稱<strong>魚根久場島</strong>）。</p>
<p>包括今中華民國絕大部分領土，但不包括<a href="../Page/金門縣.md" title="wikilink">金門</a><a href="../Page/馬祖列島.md" title="wikilink">馬祖地區以及</a><a href="../Page/東沙群島.md" title="wikilink">東沙群島</a>。</p>
<p>1895年，清政府因在<a href="../Page/甲午战争.md" title="wikilink">甲午戰爭戰敗</a>，與日本簽訂<a href="../Page/马关条约.md" title="wikilink">馬關條約</a>，臺灣割讓。1945年，<a href="../Page/日本二戰投降.md" title="wikilink">日本二戰投降</a>，喪失臺灣的<a href="../Page/主權.md" title="wikilink">主權</a>，台灣光復。</p>
<p><a href="../Page/闻一多.md" title="wikilink">聞一多的</a><a href="../Page/七子之歌.md" title="wikilink">七子之歌中包括該地</a>。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/关东州.md" title="wikilink">關東州</a>（<a href="../Page/旅順.md" title="wikilink">旅順</a>，<a href="../Page/大连市.md" title="wikilink">大連</a>）</p></td>
<td><p><a href="../Page/辽宁省.md" title="wikilink">遼寧省</a><a href="../Page/大连市.md" title="wikilink">大連市南部</a></p></td>
<td><p>1895年4月17日 1895年4月23日</p>
<p>1898年3月27日</p>
<p>1905年</p>
<p>1945年</p>
<p>1955年</p></td>
<td><p>大日本帝國 清朝</p>
<p>沙皇俄國</p>
<p>大日本帝國</p>
<p>蘇聯</p>
<p>中華人民共和國</p></td>
<td><p>《<a href="../Page/马关条约.md" title="wikilink">馬關條約</a>》 <a href="../Page/三国干涉.md" title="wikilink">三國干涉還遼</a></p>
<p>《<a href="../Page/旅大租地条约.md" title="wikilink">旅大租地條約</a>》</p>
<p>《<a href="../Page/朴茨茅斯和约.md" title="wikilink">朴茨茅斯和約</a>》</p>
<p><a href="../Page/第二次世界大战.md" title="wikilink">二戰日本戰敗</a></p>
<p>赫魯曉夫1954年訪華</p></td>
<td><p>割讓 回歸</p>
<p>佔領</p>
<p>佔領</p>
<p>佔領</p>
<p>回歸</p></td>
<td><p>1895日佔，1895-1898清朝收回，1898-1905俄佔， 1905-1945再次日佔，1945-1955蘇佔。</p>
<p>1895年4月17日甲午戰爭清朝戰敗，《馬關條約》割讓遼東半島予大日本帝國。</p>
<p>6日後，<a href="../Page/俄羅斯帝國.md" title="wikilink">俄羅斯</a>、<a href="../Page/德意志帝國.md" title="wikilink">德國與</a><a href="../Page/法蘭西第三共和國.md" title="wikilink">法國以提供</a>「友善勸告」為藉口，使日本把遼東半島交還給大清帝國（<a href="../Page/三國干涉還遼.md" title="wikilink">三國干涉還遼</a>）。</p>
<p>1898年3月27日，<a href="../Page/沙俄.md" title="wikilink">沙俄以</a><a href="../Page/三國干涉還遼.md" title="wikilink">三國干涉還遼有功</a>，迫使清政府與之簽訂了《旅大租地條約》，1904日俄戰爭，日軍奪取<a href="../Page/旅順口.md" title="wikilink">旅順口軍港</a>，<a href="../Page/1945年.md" title="wikilink">1945年</a>，根據<a href="../Page/雅尔塔协定.md" title="wikilink">雅爾達協定</a>，旅順港由<a href="../Page/蘇聯.md" title="wikilink">蘇聯占用</a>，1955年，蘇聯將旅順主權移交<a href="../Page/中華人民共和國.md" title="wikilink">中華人民共和國</a>。</p>
<p>此「關東」乃「<a href="../Page/山海关.md" title="wikilink">山海關以東</a>」，與日本<a href="../Page/關東地方.md" title="wikilink">關東地區無關</a>。</p>
<p>包括今<a href="../Page/大连市.md" title="wikilink">大連</a><a href="../Page/中山区_(大连市).md" title="wikilink">中山区</a>、<a href="../Page/西岗区.md" title="wikilink">西岗区</a>、<a href="../Page/沙河口区.md" title="wikilink">沙河口区</a>、<a href="../Page/甘井子区.md" title="wikilink">甘井子区</a>、<a href="../Page/旅顺口区.md" title="wikilink">旅顺口区</a>、<a href="../Page/金州区.md" title="wikilink">金州区以及</a><a href="../Page/长海县.md" title="wikilink">长海县</a>。</p>
<p>不包括<a href="../Page/瓦房店市.md" title="wikilink">瓦房店市</a>、<a href="../Page/普兰店区.md" title="wikilink">普兰店区和</a><a href="../Page/庄河市.md" title="wikilink">庄河市</a>。</p>
<p><a href="../Page/闻一多.md" title="wikilink">聞一多的</a><a href="../Page/七子之歌.md" title="wikilink">七子之歌中包括該地</a>。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/新界.md" title="wikilink">新界</a></p></td>
<td><p><a href="../Page/香港.md" title="wikilink">香港特區新界全境</a></p></td>
<td><p>1898年 1941年</p>
<p>1945年</p>
<p>1997年7月1日</p></td>
<td><p>大英帝國 大日本帝國</p>
<p>大英帝國</p>
<p>中華人民共和國</p></td>
<td><p>《<a href="../Page/南京条约.md" title="wikilink">南京条约</a>》 二戰<a href="../Page/香港保衛戰.md" title="wikilink">香港保衛戰</a></p>
<p><a href="../Page/第二次世界大战.md" title="wikilink">二戰日本戰敗</a></p>
<p><a href="../Page/香港回歸.md" title="wikilink">香港回歸</a></p></td>
<td><p>割讓 佔領</p>
<p>佔領</p>
<p>回歸</p></td>
<td><p>1898-1941租借予英，1941-1945日佔，1945-1997再次歸英，1997年7月1日主權移交至中華人民共和國。 新界原屬<a href="../Page/广东省.md" title="wikilink">廣東省</a><a href="../Page/寶安縣.md" title="wikilink">寶安縣</a>，乃<a href="../Page/英屬香港.md" title="wikilink">英屬香港之二度拓展</a>，包括<a href="../Page/大嶼山.md" title="wikilink">大嶼山</a>。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/英租威海卫.md" title="wikilink">威海卫</a></p></td>
<td><p><a href="../Page/山东省.md" title="wikilink">山东省</a><a href="../Page/威海市.md" title="wikilink">威海市全境</a></p></td>
<td><p>1898年 1930年10月1日</p>
<p>（除劉公島，見右）</p></td>
<td><p>大英帝國 中華民國</p></td>
<td><p>《<a href="../Page/订租威海卫专条.md" title="wikilink">威海衛組界專條</a>》 （需補充）</p></td>
<td><p>租借</p></td>
<td><p>1898年，<a href="../Page/英租威海卫.md" title="wikilink">英國強租威海衛</a>。1900年，設威海衛行政長官署，屬英國殖民部。1930年10月，中國收回除<a href="../Page/刘公岛.md" title="wikilink">劉公島外的威海衛</a>，置威海衛行政區，直屬國民政府行政院。 劉公島為英國海軍續租10年，1940年劉公島準備收回前被日軍占領。而中國政府再次控制劉公島則要到1945年<a href="../Page/二战.md" title="wikilink">二戰結束後才得以實現</a>。</p>
<p><a href="../Page/闻一多.md" title="wikilink">聞一多的</a><a href="../Page/七子之歌.md" title="wikilink">七子之歌中包括該地</a>。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/胶州湾租借地.md" title="wikilink">胶澳</a></p></td>
<td><p><a href="../Page/山东省.md" title="wikilink">山东省</a><a href="../Page/青岛市.md" title="wikilink">青岛市市區</a></p></td>
<td><p>1898年 1914年11月</p>
<p>1922年12月10日</p></td>
<td><p>德國 大日本帝國</p>
<p>中華民國</p></td>
<td><p>《<a href="../Page/胶澳租借条约.md" title="wikilink">膠澳租借條約</a>》 <a href="../Page/青島戰役.md" title="wikilink">青島戰役</a></p>
<p><a href="../Page/《解決山東懸案條約》.md" title="wikilink">《解決山東懸案條約》</a></p></td>
<td><p>租借 佔領</p>
<p>回歸</p></td>
<td><p>1898-1914租借予德、1914-1922日佔。 即環<a href="../Page/胶州湾.md" title="wikilink">膠州灣地區</a>。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/广州湾.md" title="wikilink">廣州湾</a></p></td>
<td><p><a href="../Page/广东省.md" title="wikilink">廣東省</a><a href="../Page/湛江市.md" title="wikilink">湛江市市區</a></p></td>
<td><p>1899年 1945年10月19日</p></td>
<td><p>法國 中華民國</p></td>
<td><p>《<a href="../Page/中法互订广州湾租界条约.md" title="wikilink">中法互訂廣州灣租界條約</a>》 <a href="../Page/第二次世界大战.md" title="wikilink">二戰日本戰敗</a></p></td>
<td><p>租借 回歸</p></td>
<td><p>1899年由<a href="../Page/法國.md" title="wikilink">法國向清政府租借</a>，直到<a href="../Page/二次大戰.md" title="wikilink">二次大戰時的</a>1943年被<a href="../Page/日軍.md" title="wikilink">日軍佔領</a>，1945年<a href="../Page/日本投降.md" title="wikilink">日本投降後</a>，由法國主權移交中華民國。 與<a href="../Page/广东省.md" title="wikilink">廣東省省會</a><a href="../Page/广州市.md" title="wikilink">廣州市無關</a>。</p>
<p><a href="../Page/闻一多.md" title="wikilink">聞一多的</a><a href="../Page/七子之歌.md" title="wikilink">七子之歌中包括該地</a>。</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/江東六十四屯.md" title="wikilink">江東六十四屯</a></p></td>
<td><p>俄羅斯<a href="../Page/阿穆尔州.md" title="wikilink">阿穆爾州</a><a href="../Page/海兰泡.md" title="wikilink">海蘭泡</a></p></td>
<td><p>1900年 1991年</p></td>
<td><p>沙皇俄國 俄羅斯聯邦</p></td>
<td><p><a href="../Page/义和团运动.md" title="wikilink">庚子拳亂</a> 《<a href="../Page/中华人民共和国和苏维埃社会主义共和国联盟关于中苏国界东段的协定.md" title="wikilink">中蘇國界東段的協定</a>》</p></td>
<td><p>佔領</p>
<p>放棄治權</p></td>
<td><p>原<a href="../Page/黑龙江省.md" title="wikilink">黑龍江省</a><a href="../Page/黑河市.md" title="wikilink">黑河市主城區</a>，今黑河市僅餘黑龍江南岸部分，北岸成為<a href="../Page/俄罗斯.md" title="wikilink">俄羅斯遠東地區</a><a href="../Page/阿穆爾州.md" title="wikilink">阿穆爾州</a><a href="../Page/首府.md" title="wikilink">首府</a><a href="../Page/海兰泡.md" title="wikilink">海蘭泡</a>。 1900年<a href="../Page/义和团运动.md" title="wikilink">義和團運動俄國趁中國無暇兼顧東北情勢</a>，派兵予以佔領。</p>
<p>1991年<a href="../Page/中华人民共和国.md" title="wikilink">中華人民共和國和</a><a href="../Page/苏联.md" title="wikilink">蘇聯劃定邊界</a>，簽訂《<a href="../Page/中华人民共和国和苏维埃社会主义共和国联盟关于中苏国界东段的协定.md" title="wikilink">中蘇國界東段的協定</a>》，中華人民共和國正式放棄該地之治權。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/清代蒙古.md" title="wikilink">蒙古</a></p></td>
<td><p><a href="../Page/蒙古国.md" title="wikilink">蒙古國全境</a> 俄羅斯<a href="../Page/图瓦共和国.md" title="wikilink">圖瓦共和國全境</a></p></td>
<td><p>1911年</p></td>
<td><p>俄國</p></td>
<td><p>《<a href="../Page/俄蒙协约.md" title="wikilink">俄蒙协约</a>》</p></td>
<td><p>失去控制權</p></td>
<td><p>但當時「名义上」仍然是<a href="../Page/中國.md" title="wikilink">中國领土</a>。包括<a href="../Page/唐努乌梁海.md" title="wikilink">唐努烏拉海</a>。</p></td>
</tr>
</tbody>
</table>

注：[黑瞎子島](../Page/黑瞎子岛.md "wikilink")：1929年[中東路事件時](../Page/中东路事件.md "wikilink")，蘇聯軍隊進佔該島，屬民國時期之領土變遷，故不列入在內。2004年，中俄兩國重新勘定國界，將黑瞎子島一分為二，其中西部回歸中國。

  -
## 与领土相关的条约

  - 1689年（康熙二十八年），中俄签订《[尼布楚條約](../Page/尼布楚條約.md "wikilink")》，划定外东北边境。
  - 1727年（雍正五年），中俄签订《[恰克圖界約](../Page/恰克圖界約.md "wikilink")》、《[恰克图条约](../Page/恰克图条约.md "wikilink")》，划定[乌里雅苏台](../Page/烏里雅蘇臺將軍.md "wikilink")（外蒙古地区）边境。
  - 1842年（道光二十二年），中英签订《[南京条约](../Page/南京条约.md "wikilink")》，将香港岛割让给英国<small>（1997年主權移交，《南京條約》擱置。）</small>。
  - 1858年（咸丰八年），中俄签订《[璦琿條約](../Page/璦琿條約.md "wikilink")》，中方割让按《尼布楚条约》规定原属中国的[黑龙江以北约](../Page/黑龙江.md "wikilink")60万平方公里土地。
  - 1860年（咸丰十年），英法联军攻入北京，火烧[圆明园](../Page/圆明园.md "wikilink")，后签订中英、中法《[北京条约](../Page/北京条约.md "wikilink")》，将[九龍半島南部割让给英国](../Page/九龍半島.md "wikilink")<small>（1997年主權移交，《北京條約》擱置）</small>。同年中俄《北京条约》签订，将[乌苏里江以东](../Page/乌苏里江.md "wikilink")，包括[库页岛在内的约](../Page/库页岛.md "wikilink")40万平方千米的土地割让给俄国。
  - 1864年（同治三年），中国与俄国签定《[中俄勘分西北界约记](../Page/中俄勘分西北界约记.md "wikilink")》，将[外西北](../Page/外西北.md "wikilink")（包括[巴尔喀什湖之东南](../Page/巴尔喀什湖.md "wikilink")、[伊犁以西](../Page/伊犁.md "wikilink")、以及[喷赤河以东的](../Page/噴赤河.md "wikilink")[新疆](../Page/伊犁將軍.md "wikilink")[帕米尔等地区](../Page/帕米爾高原.md "wikilink")）的44万平方公里割让给俄国。
  - 1881年（光绪七年），中国与俄国签定《[伊犁條約](../Page/伊犁條約.md "wikilink")》。
  - 1885年（光绪十一年），中国与法国签定《[中法新约](../Page/中法新约.md "wikilink")》，划定滇越边境。
  - 1886年（光绪十二年），中国与英国签定《[中英缅甸条款](../Page/中英缅甸条款.md "wikilink")》，划定滇缅边境。
  - 1887年（光绪十三年），《[中葡和好通商條約](../Page/中葡和好通商條約.md "wikilink")》簽訂，中國同意[葡萄牙](../Page/葡萄牙.md "wikilink")“永居、管理[澳門](../Page/澳門.md "wikilink")”<small>（1999年政權移交）</small>。
  - 1895年（光绪二十一年），中国与[日本签定](../Page/日本.md "wikilink")《[马关条约](../Page/马关条约.md "wikilink")》，将[臺灣以及](../Page/臺灣.md "wikilink")[澎湖列岛割让给日本](../Page/澎湖群島.md "wikilink")。[二戰后由中華民國政府實際統治至今](../Page/第二次世界大战.md "wikilink")。
  - 1898年（光绪二十四年），中国与[英国签定](../Page/英国.md "wikilink")《[展拓香港界址專條](../Page/展拓香港界址專條.md "wikilink")》，将[香港附近](../Page/香港.md "wikilink")[新界等地租借给英国](../Page/新界.md "wikilink")99年<small>（1997年租約期滿，收回。）</small>。
  - 1911年（宣统三年），[辛亥革命爆发后](../Page/辛亥革命.md "wikilink")，大清[外蒙古当局与俄罗斯签订](../Page/外蒙古.md "wikilink")《俄蒙协约》，[中國对外蒙古失去控制权](../Page/中國.md "wikilink")，但「名义上」仍然是[中國领土](../Page/中國.md "wikilink")」。

## 參考文獻

  - 《[清史稿](../Page/清史稿.md "wikilink")》
  - 《[中国历史地图集](../Page/中国历史地图集.md "wikilink")》

## 注釋

## 参见

  - [明朝行政区划](../Page/明朝行政区划.md "wikilink")
  - [清朝](../Page/清朝.md "wikilink")

{{-}}

[清朝行政区划](../Category/清朝行政区划.md "wikilink")
[Category:中国各朝代行政区划](../Category/中国各朝代行政区划.md "wikilink")

1.  道光皇帝《御製[大清一統志序](../Page/大清一統志.md "wikilink")》

2.  《清代政區沿革綜表》王鐘翰序

3.  《大清一統志》盛京

4.  《中國歷代行政區劃的變遷》，147頁

5.  《清史稿》土司傳

6.  《清史稿》地理志二、三、四

7.  設協辦大臣一員。

8.
9.
10.