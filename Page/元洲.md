**元洲**（[英語](../Page/英語.md "wikilink")：****）是[香港地方](../Page/香港地方.md "wikilink")，位於現時[九龍](../Page/九龍.md "wikilink")[深水埗北部](../Page/深水埗.md "wikilink")，大約在[長沙灣道](../Page/長沙灣道.md "wikilink")、[大埔道](../Page/大埔道.md "wikilink")、[南昌街及](../Page/南昌街.md "wikilink")[桂林街之內的範圍](../Page/桂林街.md "wikilink")。未有市區發展之前，元洲原本是[鄉村](../Page/鄉村.md "wikilink")，稱為**元洲村**，位於深水埗北部，其附近有三條村，分別是東南的[田寮村](../Page/田寮村_\(深水埗區\).md "wikilink")、西面的[菴由村](../Page/菴由村.md "wikilink")，以及西北的[馬龍坑村](../Page/馬龍坑村.md "wikilink")。然而現時的[元州邨一帶與當時的元洲村的位置並不一樣](../Page/元州邨.md "wikilink")。

## 歷史

元洲這個名稱據稱是由**圓洲**而來，原本是[圓形的](../Page/圓形.md "wikilink")[島嶼](../Page/島嶼.md "wikilink")。元洲東面有[溪流](../Page/溪流.md "wikilink")，流往原深水埗的北面，這條[涌最後變成今日的](../Page/涌.md "wikilink")[南昌街渠](../Page/南昌街.md "wikilink")。而元洲村的北面，則有另一條溪流。元洲地形平坦，加上有兩條溪流的水源，所以適合種[田](../Page/田.md "wikilink")\[1\]。1924年，元洲以南的深水埗已經發展近元洲村邊\[2\]，最後元洲村也進入了市區範圍。[香港政府就將大埔道和長沙灣道之間的三條街](../Page/香港殖民地時期#香港政府.md "wikilink")，分別稱為菴由街、田寮街和元洲街\[3\]。前兩者後來分別改稱為[福華街及](../Page/福華街.md "wikilink")[福榮街](../Page/福榮街.md "wikilink")\[4\]，元洲街中的「洲」字亦改為「州」字，即[元州街](../Page/元州街.md "wikilink")。

元洲街隨著[長沙灣填海而被延長](../Page/長沙灣.md "wikilink")。1965年，香港政府於[長沙灣新填地興建元洲街政府廉租屋邨](../Page/長沙灣.md "wikilink")，即是現在的元州邨。不過這條屋邨與本來元洲的位置相距甚遠。除了元州邨，[元州街公共圖書館以及](../Page/元州街公共圖書館.md "wikilink")[香港扶幼會元洲宿舍的名稱](../Page/香港扶幼會.md "wikilink")，都是間接來自元洲。

現時元洲現址最著名的是[高登電腦中心與黃金電腦商場](../Page/高登電腦中心與黃金電腦商場.md "wikilink")，而深水埗則位於元洲以南。不過現時已經甚少人知道該處是元洲。由於深水埗不少街道均使用[中國城市命名](../Page/中國.md "wikilink")，甚至有人誤會元洲是元州的筆誤，以為元州是中國大陸某處的地名。

## 參見

  - [元州街](../Page/元州街.md "wikilink")
  - [元州邨](../Page/元州邨.md "wikilink")

## 參考資料

<div class="references-small">

<references />

</div>

[Category:深水埗](../Category/深水埗.md "wikilink")

1.
2.
3.
4.