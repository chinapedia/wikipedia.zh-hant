[Butterfly_theorem.svg](https://zh.wikipedia.org/wiki/File:Butterfly_theorem.svg "fig:Butterfly_theorem.svg")
**蝴蝶定理**（Butterfly
theorem），是古典[欧氏平面几何的最精彩的结果之一](../Page/欧氏平面几何.md "wikilink")。蝴蝶定理最先是作为一个征求证明的问题，这个命题最早出现在1815年，而“蝴蝶定理”这个名称最早出现在《[美国数学月刊](../Page/美国数学月刊.md "wikilink")》1944年2月号，题目的几何图形象一只[蝴蝶](../Page/蝴蝶.md "wikilink")，便以此命名。这个定理的证法多得不胜枚举，至今仍然被数学热爱者研究，在考试中时有出现各种变形。

最基本的叙述为：设*M*为圆内[弦](../Page/弦.md "wikilink")*PQ*的中点，过*M*作弦*AB*和*CD*。设AD和*BC*各相交*PQ*于点*X*和*Y*，则*M*是*XY*的中点。

这个命题最早作为一个征解问题出现在公元1815年[英国的一本杂志](../Page/英国.md "wikilink")《男士日记》（Gentleman's
Diary）39-40页上。登出的当年,英国一个自学成才的中学数学教师[W.G.霍纳](../Page/W.G.霍纳.md "wikilink")（他发明了[多项式方程近似根的](../Page/多项式.md "wikilink")[霍纳法](../Page/霍纳法.md "wikilink")）给出了第一个证明，完全是初等的；另一个证明由理查德·泰勒（Richard
Taylor）给出。一种早期的证明由M.布兰德（Miles
Bland）在《几何问题》（1827年）一书中给出。最为简洁的证法是[射影几何的证法](../Page/射影几何.md "wikilink")，由英国的[J·开世在](../Page/约翰·开世.md "wikilink")"A
Sequel to the First Six Books of the Elements of
Euclid"（中译：近世几何学初编，[李俨译](../Page/李俨_\(现代学者\).md "wikilink")，上海[商务印书馆](../Page/商务印书馆.md "wikilink")
1956
）给出，只有一句话，用的是线束的[交比](../Page/交比.md "wikilink")。1981年，[Crux杂志刊登了K](../Page/Crux杂志.md "wikilink").萨蒂亚纳拉亚纳（Kesirajn
Satyanarayana）用[解析几何的一种比较简单的方法](../Page/解析几何.md "wikilink")（利用直线束，二次曲线束）。

该定理实际上是[射影几何中一个定理的特殊情况](../Page/射影几何.md "wikilink")，有多种推广：

1.  M，作为圆内弦是不必要的，可以移到圆外。
2.  圆可以改为任意[圆锥曲线](../Page/圆锥曲线.md "wikilink")。
3.  将圆变为一个[完全四角形](../Page/完全四角形.md "wikilink")，M为对角线交点。
4.  去掉中点的条件，结论变为一个一般关于有向线段的比例式，称为“[坎迪定理](../Page/坎迪定理.md "wikilink")”，
    \(M \,\)不为中点时满足:
    \({1 \over MY}-{1 \over MX} = {1 \over MQ}-{1 \over MP}\),这对2，3均成立。

## 证明

从\(X\,\)向\(AM\,\)和\(DM\,\)作垂线，设垂足分别为\(X'\,\)和\(X''\,\)。类似地，从\(Y\,\)向\(BM\,\)和\(CM\,\)作垂线，设垂足分别为\(Y'\,\)和\(Y''\,\)。

[Butterfly1.svg](https://zh.wikipedia.org/wiki/File:Butterfly1.svg "fig:Butterfly1.svg")

现在，由于

  -

      -
        \(\triangle MXX' \sim \triangle MYY',\,\)

    \({MX \over MY} = {XX' \over YY'},\)

<!-- end list -->

  -

      -
        \(\triangle MXX'' \sim \triangle MYY'',\,\)

    \({MX \over MY} = {XX'' \over YY''},\)

<!-- end list -->

  -

      -
        \(\triangle AXX' \sim \triangle CYY'',\,\)

    \({XX' \over YY''} = {AX \over CY},\)

<!-- end list -->

  -

      -
        \(\triangle DXX'' \sim \triangle BYY',\,\)

    \({XX'' \over YY'} = {DX \over BY},\)

从这些等式，可以很容易看出：

  -
    \(\left({MX \over MY}\right)^2 = {XX' \over YY' } {XX'' \over YY''},\)

<!-- end list -->

  -
    \({} = {AX.DX \over CY.BY},\)

<!-- end list -->

  -
    \({} = {PX.QX \over PY.QY},\)

<!-- end list -->

  -
    \({} = {(PM-XM).(MQ+XM) \over (PM+MY).(QM-MY)},\)

由于\(PM \,\) = \(MQ \,\)

现在，

\[{ (MX)^2 \over (MY)^2} = {(PM)^2 - (MX)^2 \over (PM)^2 - (MY)^2}.\]

因此，我们得出结论： \(MX = MY \,\)，也就是说，\(M \,\)是\(XY \,\)的中点。

证毕。

[Category:几何定理](../Category/几何定理.md "wikilink")
[Category:圆](../Category/圆.md "wikilink")