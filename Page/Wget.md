**GNU
Wget**（常简称为**Wget**）是一个在网络上进行下载的简单而强大的[自由软件](../Page/自由软件.md "wikilink")，其本身也是[GNU计划的一部分](../Page/GNU计划.md "wikilink")。它的名字是「****」和「****」的结合，同时也隐含了软件的主要功能。目前它支持通过、，以及这三个最常见的[TCP/IP协议协议下载](../Page/TCP/IP协议.md "wikilink")。

## 特点

[thumb](../Page/file:Gwget-1.0.4.png.md "wikilink") 它的主要特点包括：

  - 支持递归下载
  - 恰当的转换页面中的链接
  - 生成可在本地浏览的页面镜像
  - 支持代理服务器

1996年，随着互联网的爆发，Wget出现了。并广泛的被[Unix和主要的](../Page/Unix.md "wikilink")[Linux发行版用户所使用](../Page/Linux.md "wikilink")。由于使用移植性非常良好的[C语言](../Page/C语言.md "wikilink")，所以Wget可以轻松的在任何类似Unix的系统以及其他Unix变种操作系统上[編譯使用](../Page/編譯.md "wikilink")，如[Mac
OS X](../Page/Mac_OS_X.md "wikilink")，[Microsoft
Windows](../Page/Microsoft_Windows.md "wikilink")\[1\]，和[OpenVMS](../Page/OpenVMS.md "wikilink")\[2\]。

由于没有交互式界面，在类Unix平台上Wget可在后台运行，截获并忽略HANGUP信号，因此在用户退出登录以后，仍可继续运行。通常，Wget用于成批量地下载Internet网站上的文件，或制作远程网站的镜像。

在其之上的图形界面应用程序有：[GNOME下面的](../Page/GNOME.md "wikilink")**gwget**[2](http://gnome.org/projects/gwget/index.html)。Windows系统下面的**wGetGUI**[3](http://www.jensroesner.de/wgetgui/)。

## 命令

Wget的命令格式如下：

> ` wget [options] [URL]`

详细的命令和参数可以参照后面的外部链接中的内容。

## 缺點

  - 支持的协议较少，特别是[cURL相比](../Page/cURL.md "wikilink")。流行的流媒体协议mms和rtsp没有得到支持，还有广泛使用各种的P2P协议也没有涉及。
  - 支持协议过老。目前HTTP还是使用1.0版本，而HTML中通过JavaScript和CSS引用的文件不能下载。
  - 灵活性不强，扩展性不高。面对复杂的镜像站会出现问题。
  - 命令过于复杂，可选的设置项有上百个。

## 参见

  - [FTP客户端列表](../Page/FTP客户端列表.md "wikilink")
  - [cURL](../Page/cURL.md "wikilink")
  - [Teleport](../Page/Teleport.md "wikilink")

## 参考文献

## 外部链接

  -
  - [win32 binary
    wget-1.13.4](http://opensourcepack.blogspot.com/2010/05/wget-112-for-windows.html)

<!-- end list -->

  - [很老的一个Wget
    FTP站点](ftp://gnjilux.srk.fer.hr/pub/unix/util/wget/)，里面包含了各个历史时期的发布版本，包括在1.0版前发布的Geturl。

[Category:GNU計劃軟體](../Category/GNU計劃軟體.md "wikilink")
[Category:FTP客户端](../Category/FTP客户端.md "wikilink")
[Category:HTTP客户端](../Category/HTTP客户端.md "wikilink")
[Category:下載工具](../Category/下載工具.md "wikilink")
[Category:免費軟件](../Category/免費軟件.md "wikilink")

1.  [1](http://xoomer.virgilio.it/hherold/)
2.