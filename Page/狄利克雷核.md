在[数学分析中](../Page/数学分析.md "wikilink")，**狄利克雷核**是指[函数列](../Page/函数.md "wikilink")：

\[D_n(x)=\sum_{k=-n}^n
e^{ikx}=1+2\sum_{k=1}^n\cos(kx)=\frac{\sin\left(\left(n +\frac{1}{2}\right)x\right)}{\sin(x/2)}.\]

狄利克雷核的名称得自[約翰·彼得·狄利克雷](../Page/約翰·彼得·狄利克雷.md "wikilink")。

狄利克雷核的主要应用是在[傅里叶级数中](../Page/傅里叶级数.md "wikilink")。*D<sub>n</sub>*(*x*)与任何以2π为[周期的函数](../Page/周期函数.md "wikilink")
*f* 的[卷积是](../Page/卷积.md "wikilink") *f* 的第 *n* 阶傅里叶级数逼近，也就是说：

\[(D_n*f)(x)=\frac{1}{2\pi}\int_{-\pi}^\pi f(y)D_n(x-y)\,dy=\sum_{k=-n}^n \hat{f}(k)e^{ikx},\]

其中

\[\hat{f}(k)=\frac{1}{2\pi}\int_{-\pi}^\pi f(x)e^{-ikx}\,dx\]

是 *f* 的第 *k*
个傅里叶系数。因此，为了研究傅里叶级数的收敛性质，只需研究相应的狄利克雷核的性质。狄利克雷核的一个重要特征是当*n*趋于正[无穷时](../Page/无穷大.md "wikilink")，
*D<sub>n</sub>*
的[*L*<sup>1</sup>](../Page/Lp空间.md "wikilink")[范数](../Page/范数.md "wikilink")
也趋于正无穷，并且有：

\[\| D_n \| _{L^1} \approx \log n\]

其中\(\approx\)表示两者为“同等级别”的无穷大。狄利克雷核的缺乏一致收敛性是导致很多傅里叶级数发散的原因。比如，运用狄利克雷核与[一致有界原理我们可以证明连续函数的傅里叶级数甚至不一定](../Page/一致有界原理.md "wikilink")[逐点收敛](../Page/逐点收敛.md "wikilink")。参见[傅里叶级数的收敛性](../Page/傅里叶级数的收敛性.md "wikilink")。

## 与[狄拉克δ函数的关系](../Page/狄拉克δ函数.md "wikilink")

[狄拉克δ函数并不是严格意义上的函数](../Page/狄拉克δ函数.md "wikilink")，而更多地是一个“[广义函数](../Page/广义函数.md "wikilink")”，或者说“分布”。将[周期](../Page/周期函数.md "wikilink")[狄拉克δ函数乘以](../Page/狄拉克δ函数.md "wikilink")2π，就可以得到关于周期为2π的[卷积运算的](../Page/卷积.md "wikilink")[单位元](../Page/单位元.md "wikilink")，即对于2π为周期的函数*f*，有：

\[f*(2\pi \delta)=f \,\]

这个“函数”的[傅立叶级数为](../Page/傅立叶级数.md "wikilink")：

\[2\pi \delta(x)\sim\sum_{k=-\infty}^\infty e^{ikx}=\left(1 +2\sum_{k=1}^\infty\cos(kx)\right).\]

于是，作为此级数的一个部分和，狄利克雷核可以看作一个“[恒等逼近](../Page/恒等逼近.md "wikilink")”。然而，这个恒等逼近并不是“正项”的（不是正值函数），因此有上述的局限。

## 三角恒等式的证明

上文中的三角恒等式

\[\sum_{k=-n}^n e^{ikx}
=\frac{\sin\left(\left(n+\frac{1}{2}\right)x\right)}{\sin(x/2)}\]

可以用[等比数列的求和公式得到](../Page/等比数列.md "wikilink")：首先

\[\sum_{k=0}^n a r^k=a\frac{1-r^{n+1}}{1-r}.\]

因此有：

\[\sum_{k=-n}^n r^k=r^{-n}\cdot\frac{1-r^{2n+1}}{1-r}.\]

在式中将分子和分母各乘 *r*<sup>−1/2</sup>，便有：

\[\frac{r^{-n-1/2}}{r^{-1/2}}\cdot\frac{1-r^{2n+1}}{1-r} =\frac{r^{-n-1/2}-r^{n+1/2}}{r^{-1/2}-r^{1/2}}.\]

当*r* = *e*<sup>*ix*</sup> 时就有：

\[\sum_{k=-n}^n e^{ikx}=\frac{e^{-(n+1/2)ix}-e^{(n+1/2)ix}}{e^{-ix/2}-e^{ix/2}} =\frac{-2i\sin((n+1/2)x)}{-2i\sin(x/2)}\]

等式当 \(e^{ix}\neq 1\) 时，即对于不是\(2\pi\)整数倍的*x* 成立。

对于为\(2\pi\)整数倍的*x*，由于 \(\frac{\sin((n+1/2)x)}{\sin(x/2)}\) 在对应点的极限是2n+1

\[\lim\limits_{x\to 2k\pi} \frac{\sin((n+1/2)x)}{\sin(x/2)} = 2n+1\]

因此可以将表达式延伸为连续函数，使得等式对任意*x*都成立。

## 狄利克雷核的性质

  - 狄利克雷核是一个[三角多项式](../Page/三角多项式.md "wikilink")，因此是无穷阶可导的周期函数；
  - 狄利克雷核是[偶函数](../Page/偶函数.md "wikilink")；
  - 狄利克雷核的[平均值是](../Page/平均值.md "wikilink")1；
  - 在正无穷处的平均值为：

\[\|D_n\|_1=\frac1{2\pi}\int_{-\pi}^{\pi} |D_n(t)| d t =\frac4{\pi^2}\ln n+O(1)\]

[category:数学分析](../Page/category:数学分析.md "wikilink")

[Category:逼近理论](../Category/逼近理论.md "wikilink")
[Category:傅里叶级数](../Category/傅里叶级数.md "wikilink")