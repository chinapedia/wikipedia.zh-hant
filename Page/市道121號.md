**縣道121號
南勢－日南**是位於臺灣中部海岸平原的[縣道](../Page/縣道.md "wikilink")，北起[苗栗縣](../Page/苗栗縣.md "wikilink")[通霄鎮南勢](../Page/通霄鎮.md "wikilink")，南至[臺中市](../Page/臺中市.md "wikilink")[大甲區日南](../Page/大甲區.md "wikilink")，全長共計22.988公里。\[1\]

## 行經鄉鎮里程一覽

  - [苗栗縣](../Page/苗栗縣.md "wikilink")

<!-- end list -->

  - [通霄鎮](../Page/通霄鎮.md "wikilink")：通霄南勢0.0k（**起點**，岔路）→通霄外環（岔路）→南和國小7.525k→福興（左【苗48線】鄉道岔路）
  - [苑裡鎮](../Page/苑裡鎮.md "wikilink")：水柳坡14.95k（右【苗42線】鄉道岔路）→山腳14.731k（岔路）→山柑20.063k（岔路近[苑裡交流道](../Page/苑裡交流道.md "wikilink")）

<!-- end list -->

  - [臺中市](../Page/臺中市.md "wikilink")

<!-- end list -->

  - [大甲區](../Page/大甲區.md "wikilink")：日南22.156k（左【中6線】鄉道岔路）→日南22.988k（**終點**，岔路）

## 交流道

  - [通霄二交流道](../Page/通霄二交流道.md "wikilink")

## 沿線風景

  - [台鐵](../Page/台鐵.md "wikilink")[海線](../Page/海岸線_\(臺鐵\).md "wikilink")[通霄車站](../Page/通霄車站.md "wikilink")
  - 通霄[市區](http://cxz9001.pixnet.net/blog)
  - [飛牛牧場](../Page/飛牛牧場.md "wikilink")

## 參考

<references />

## 相關連結

[Category:台灣縣道](../Category/台灣縣道.md "wikilink")
[Category:苗栗縣道路](../Category/苗栗縣道路.md "wikilink")
[Category:台中市道路](../Category/台中市道路.md "wikilink")
[Category:台灣市道](../Category/台灣市道.md "wikilink")

1.  公路總局