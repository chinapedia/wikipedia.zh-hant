**愛媛足球會**（FC
Ehime，一般稱作**FC愛媛**或**愛媛FC**），是[日本一支職業足球球隊](../Page/日本.md "wikilink")，現時於[日本職業足球聯賽乙級部](../Page/日本職業足球聯賽.md "wikilink")（J2）比賽。

## 簡介

球隊的前身是松山足球俱樂部，於1970年成立。根據地在[愛媛縣](../Page/愛媛縣.md "wikilink")[松山市以及整個愛媛縣](../Page/松山市.md "wikilink")，球隊的主場是[愛媛縣綜合運動公園陸上競技場](../Page/愛媛縣綜合運動公園陸上競技場.md "wikilink")。1995年改名為愛媛足球俱樂部（簡稱愛媛FC）。2001年球隊開始在日本足球聯賽（JFL）比賽。2005年球隊奪得JFL的冠軍後，昇級到乙級（J2）。

## 歷年成績紀錄

| 賽計                                            | 層級 | Tms. | Pos.   | Attendance/G | [日本聯賽盃](../Page/日本聯賽盃.md "wikilink") | [天皇盃](../Page/天皇盃.md "wikilink") |
| --------------------------------------------- | -- | ---- | ------ | ------------ | ------------------------------------ | -------------------------------- |
| [2006](../Page/2006年日本職業足球聯賽.md "wikilink")   | J2 | 13   | **9**  | 4,139        | \-                                   | 4th Round                        |
| [2007](../Page/2007年日本職業足球聯賽.md "wikilink")   | J2 | 13   | **10** | 3,317        | \-                                   | Quarter-final                    |
| [2008](../Page/2008年日本職業足球聯賽.md "wikilink")   | J2 | 15   | **14** | 3,704        | \-                                   | 4th Round                        |
| [2009](../Page/2009年日本職業足球聯賽.md "wikilink")   | J2 | 18   | **15** | 3,694        | \-                                   | 2nd Round                        |
| [2010](../Page/2010年日本職業足球聯賽.md "wikilink")   | J2 | 19   | **11** | 4,386        | \-                                   | 2nd Round                        |
| [2011](../Page/2011年日本職業足球聯賽.md "wikilink")   | J2 | 20   | **15** | 3,475        | \-                                   | 4th Round                        |
| [2012](../Page/2012年日本職業足球乙級聯賽.md "wikilink") | J2 | 22   | **16** | 3,629        | \-                                   | 2nd Round                        |

  - Key

<!-- end list -->

  - Tms. = 球隊數量
  - Pos. = 聯賽排名
  - Attendance/G = 平均觀眾數

## 球員名單

## 參考資料

  - [球隊介紹](https://web.archive.org/web/20070817112644/http://www.ehimefc.com/p/team_profile.html)

## 外部連結

  - [官方網站](http://www.ehimefc.com/)

[Category:日本足球俱樂部](../Category/日本足球俱樂部.md "wikilink")
[Category:松山市](../Category/松山市.md "wikilink")
[Category:1970年建立的足球俱乐部](../Category/1970年建立的足球俱乐部.md "wikilink")