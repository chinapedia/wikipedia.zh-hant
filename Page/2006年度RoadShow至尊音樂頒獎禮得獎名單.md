**RoadShow至尊音樂頒獎禮2006**於2007年1月4日假座[香港會議展覽中心舉行](../Page/香港會議展覽中心.md "wikilink")，當晚共頒發16個項目，總共51個獎項，司儀為[高皓正](../Page/高皓正.md "wikilink")、[張達明](../Page/張達明.md "wikilink")、[俞詠文](../Page/俞詠文.md "wikilink")，以下為當晚的得獎名單：

## 歌曲獎項

### 至尊歌曲

  - 《華麗邂逅》 ——[容祖兒](../Page/容祖兒.md "wikilink")
  - 《熱浪假期》 ——[Twins](../Page/Twins.md "wikilink")
  - 《紅綠燈》 ——[鄭融](../Page/鄭融.md "wikilink")
  - 《在你遙遠的附近》 ——[方力申](../Page/方力申.md "wikilink")
  - 《愛得太遲》 ——[古巨基](../Page/古巨基.md "wikilink")
  - 《光明會》 ——[何韻詩](../Page/何韻詩.md "wikilink")
  - 《張開眼睛》 ——[劉德華](../Page/劉德華.md "wikilink")
  - 《成魔之路》 ——[麥浚龍](../Page/麥浚龍.md "wikilink")
  - 《糖不甩》 ——[薛凱琪](../Page/薛凱琪.md "wikilink")
  - 《情歌》 ——[側田](../Page/側田.md "wikilink")
  - 《離家出走》 ——[衛蘭](../Page/衛蘭.md "wikilink")
  - 《晏》 ——[Eric Kwok](../Page/Eric_Kwok.md "wikilink")
  - 《愛愛愛》 ——[方大同](../Page/方大同.md "wikilink")
  - 《阿博二世》 ——[黃貫中](../Page/黃貫中.md "wikilink")
  - 《阿姆斯特丹》 ——[黃耀明](../Page/黃耀明.md "wikilink")

### 至尊合唱歌曲

  - 《大喊包》
    ——[關楚耀](../Page/關楚耀.md "wikilink")、[譚詠麟](../Page/譚詠麟.md "wikilink")
  - 《十分愛》
    ——[方力申](../Page/方力申.md "wikilink")、[鄧麗欣](../Page/鄧麗欣.md "wikilink")
  - 《Messenger》
    ——[李逸朗](../Page/李逸朗.md "wikilink")、[蔣雅文](../Page/蔣雅文.md "wikilink")

### 至尊舞臺演繹（排名不分先後）

  - 《Honey》 ──[鄭融](../Page/鄭融.md "wikilink")
  - 《Up and down》 ──[鄭希怡](../Page/鄭希怡.md "wikilink")
  - 《Happy Hour》 ──[吳日言](../Page/吳日言.md "wikilink")

### 至尊手機鈴聲

  - 《明知做戲》 ——[吳雨霏](../Page/吳雨霏.md "wikilink")
  - 《感應》 ——[泳兒](../Page/泳兒.md "wikilink")
  - 《速剪速決》 ——[戴夢夢](../Page/戴夢夢.md "wikilink")

## 新人獎項

### 至尊新人（排名不分先後）

  - [泳兒](../Page/泳兒.md "wikilink")
  - [關楚耀](../Page/關楚耀.md "wikilink")
  - [衛詩](../Page/衛詩.md "wikilink")

### 至尊潛力新人（排名不分先後）

  - [胡琳](../Page/胡琳.md "wikilink")
  - [許懷欣](../Page/許懷欣.md "wikilink")

### 至尊新組合樂隊（排名不分先後）

  - [Zarahn](../Page/Zarahn.md "wikilink")
  - [Sun Boy'z](../Page/Sun_Boy'z.md "wikilink")

## 歌手獎項

### 至尊創作歌手（排名不分先後）

  - [黃耀明](../Page/黃耀明.md "wikilink")
  - [黃貫中](../Page/黃貫中.md "wikilink")
  - [劉德華](../Page/劉德華.md "wikilink")

### 至尊潛力創作歌手（排名不分先後）

  - [鄧麗欣](../Page/鄧麗欣.md "wikilink")
  - [方大同](../Page/方大同.md "wikilink")

### 至尊人氣歌手（排名不分先後）

  - [薛凱琪](../Page/薛凱琪.md "wikilink")
  - [周麗淇](../Page/周麗淇.md "wikilink")
  - [關智斌](../Page/關智斌.md "wikilink")

## 至尊男歌手

  - [古巨基](../Page/古巨基.md "wikilink")
  - [側田](../Page/側田.md "wikilink")
  - [劉德華](../Page/劉德華.md "wikilink")

## 至尊女歌手

  - [容祖兒](../Page/容祖兒.md "wikilink")
  - [何韻詩](../Page/何韻詩.md "wikilink")
  - [衛蘭](../Page/衛蘭.md "wikilink")

## 至尊組合樂隊

  - [Twins](../Page/Twins.md "wikilink")
  - [Soler](../Page/Soler.md "wikilink")
  - [EO2](../Page/EO2.md "wikilink")

## 至尊歌曲累積票數最高男歌手

  - [古巨基](../Page/古巨基.md "wikilink")

## 至尊歌曲累積票數最高女歌手

  - [容祖兒](../Page/容祖兒.md "wikilink")

## 至尊榮譽大獎

  - [溫拿樂隊](../Page/溫拿樂隊.md "wikilink")

## 媒體播放

由RoadShow[路訊通將於其節目內播出精華片段](../Page/路訊通.md "wikilink")。[無線電視翡翠台於](../Page/無線電視翡翠台.md "wikilink")2007年2月20日（年初三）下午14:30-15:30播出一小時精裝版，這是該頒獎典禮首次在電視台播放。另外，本年度之賽果並不計算入[四台聯頒音樂大獎內](../Page/四台聯頒音樂大獎.md "wikilink")。

## 參見

  - [路訊通](../Page/路訊通.md "wikilink")

[Category:RoadShow至尊音樂頒獎禮](../Category/RoadShow至尊音樂頒獎禮.md "wikilink")