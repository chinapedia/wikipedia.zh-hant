[Xiangqi_Advisor_(Trad).svg](https://zh.wikipedia.org/wiki/File:Xiangqi_Advisor_\(Trad\).svg "fig:Xiangqi_Advisor_(Trad).svg")
「**仕**」、「**士**」是[中國象棋的棋子](../Page/中國象棋.md "wikilink")，每方各有兩枚。紅方為「仕」，黑方為「士」。

仕、士每次可以沿“九宫”内的斜线前進或後退一格，但不能走出“九宮”，也不能平移。仕、士前進一步稱作“上仕”或“撐仕”，后退一步稱作“落仕”。

仕為仕女，即服侍元帥的人。士為士大夫，幫將軍想戰略的人。

[Category:象棋棋子](../Category/象棋棋子.md "wikilink")