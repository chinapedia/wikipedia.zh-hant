[Calgary_CTrain_Map.png](https://zh.wikipedia.org/wiki/File:Calgary_CTrain_Map.png "fig:Calgary_CTrain_Map.png")
**卡尔加里轻铁**（）是[加拿大](../Page/加拿大.md "wikilink")[艾伯塔省](../Page/艾伯塔省.md "wikilink")[卡尔加里市的](../Page/卡尔加里.md "wikilink")[轻型铁路系統](../Page/輕軌運輸.md "wikilink")，首段线路于1981年5月25日正式通車，由市營的卡尔加里交通局营运。现有系統[軌道全长约](../Page/軌道.md "wikilink")60[公里](../Page/公里.md "wikilink")，營運上分為2條路綫（201号红线和202号蓝线）。直至2017年，卡加利輕鐵以每週306,900
位乘客，成為北美洲其中一個最繁忙的輕型鐵路系統\[1\]。

## 歷史

卡尔加里輕鐵第一條路綫歷經3年的建設後，在1981年5月25日通車，成為[北美首個](../Page/北美.md "wikilink")[輕鐵系統](../Page/輕軌運輸.md "wikilink")。該路綫稱為「南綫」（South
Line，屬201綫/红线路段），由[市中心](../Page/卡加利市中心.md "wikilink")（[西南8街站](../Page/西南8街站.md "wikilink")）前往南部的[安達臣站](../Page/安達臣站.md "wikilink")（Anderson）站。早期系統中餘下部份－東北綫（屬202綫/蓝线路段）和西北綫（屬201綫/红线路段）也分別在1985年4月27日和1987年9月17日通車。此后这几条路线时有延伸并有新车站启用︰

  - 1990年8月31日 - 布伦特伍德（Brentwood，西北線）
  - 2001年10月9日 - 谷原及魚溪-拉科姆（Canyon Meadows / Fish Creek-Lacombe，南線）
  - 2003年12月15日 - 代豪斯（Dalhousie，西北線）
  - 2004年6月28日 - 蕭納西及索默斯特-布莱特伍德（Shawnessy / Somerset-Bridlewood South
    Line，南线）
  - 2009年6月15日 - 鸦爪（Crowfoot，西北線）
  - 2012年8月27日 - 马丁代尔及鞍镇（Martindale / Saddletowne，东北线）
  - 2014年8月25日 - 托斯卡尼（Tuscany，西北线）

2012年12月10日，蓝线（202线）的延伸线——8.2公里长，设有6座车站的西线（West
Line）开通营运，成为25年来整个系统中新增的首条新线路。

## 收費

|            |              |
| ---------- | ------------ |
| 收費種類       | 價格 (加元)\[2\] |
| 現金票價       | $3.25        |
| 青年票價       | $2.25        |
| 十張普通票      | $32.50       |
| 十張青年票      | $22.50       |
| 全日通        | $10.00       |
| 青年全日通      | $7.00        |
| 全月通        | $101.00      |
| 青年全月通      | $65.00       |
| 全月通（優惠價）   | $44.00       |
| 長者全年通      | $95.00       |
| 長者全年通（優惠價） | $15.00       |

  - 5歲以下之小童可免費乘車。
  - 青年票適用於就讀第1至12班之學生或6-17歲之小童。
  - 憑輕鐵車票可於購票後指定時限內轉乘卡加利交通營辦之巴士路綫。而憑登上巴士時車長發出之轉車票（Transfer）亦可免費轉乘輕鐵。
  - 位于市中心南7大道上的车站（自蓝线专属的市中心西/柯尔比站起始向东至市政厅站止，包括所有红线蓝线共用的车站）构成“市中心免费区”（Downtown
    free fare zone），这些车站之间的行程无需付费。

## 路綫及車站列表

截至2018年，卡加利輕鐵兩條營運路線上共有四十五個車站。除市中心數個車站以外，大多數乘客往返[市郊車站之時](../Page/郊區.md "wikilink")，都會選擇乘搭接駁巴士或於以泊車轉乘方式，將自己的私家車停放在車站鄰近的停車場並轉乘公共交通。

  - **红綫** （201号线，托斯卡尼至索默斯特-布莱特伍德
    *Crowfoot/Somerset-Bridlewood*，行走西北线-市中心南7大道公交走廊-南线）
  - **蓝綫** （202号线，鞍镇至西南69街 *Saddletowne/69th St.
    SW*，行走东北线-市中心南7大道公交走廊-西线）。

### 红线（201线）沿途各站

<table>
<thead>
<tr class="header">
<th><p>中文譯名</p></th>
<th><p>英文站名</p></th>
<th><p>所屬路綫</p></th>
<th><p>线区</p></th>
<th><p>通車年份</p></th>
<th><p>位置座標</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/托斯卡尼站.md" title="wikilink">托斯卡尼站</a></p></td>
<td><p>Tuscany</p></td>
<td><p>201</p></td>
<td><p>西北线</p></td>
<td><p>2014</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鴉爪站.md" title="wikilink">鸦爪站</a></p></td>
<td><p>Crowfoot</p></td>
<td><p>201</p></td>
<td><p>西北线</p></td>
<td><p>2009</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/代豪斯站.md" title="wikilink">代豪斯站</a></p></td>
<td><p>Dalhousie</p></td>
<td><p>201</p></td>
<td><p>西北线</p></td>
<td><p>2003</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/布伦特伍德站.md" title="wikilink">布伦特伍德站</a></p></td>
<td><p>Brentwood</p></td>
<td><p>201</p></td>
<td><p>西北线</p></td>
<td><p>1990</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/大學站_(卡加利).md" title="wikilink">大學站</a></p></td>
<td><p>University</p></td>
<td><p>201</p></td>
<td><p>西北线</p></td>
<td><p>1987</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/班夫徑站.md" title="wikilink">班夫徑站</a></p></td>
<td><p>Banff Trail</p></td>
<td><p>201</p></td>
<td><p>西北线</p></td>
<td><p>1987</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/獅子園站.md" title="wikilink">獅子園站</a></p></td>
<td><p>Lions Park</p></td>
<td><p>201</p></td>
<td><p>西北线</p></td>
<td><p>1987</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/南亞省理工學院／亞省藝術設計學院／銀禧站.md" title="wikilink">南亞省理工學院／亞省藝術設計學院／銀禧站</a></p></td>
<td><p>SAIT/ACAD/Jubilee</p></td>
<td><p>201</p></td>
<td><p>西北线</p></td>
<td><p>1987</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陽邊站.md" title="wikilink">陽邊站</a></p></td>
<td><p>Sunnyside</p></td>
<td><p>201</p></td>
<td><p>西北线</p></td>
<td><p>1987</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/西南8街站.md" title="wikilink">西南8街站</a></p></td>
<td><p>8 Street SW</p></td>
<td><p>201、202</p></td>
<td><p>市中心</p></td>
<td><p>1981</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/西南6街／西南7街站.md" title="wikilink">西南6街站<br />
西南7街站</a></p></td>
<td><p>6 Street SW<br />
7 Street SW</p></td>
<td><p>201、202</p></td>
<td><p>市中心</p></td>
<td><p>1981</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/西南3街／西南4街站.md" title="wikilink">西南3街站<br />
西南4街站</a></p></td>
<td><p>3 Street SW<br />
4 Street SW</p></td>
<td><p>201、202</p></td>
<td><p>市中心</p></td>
<td><p>1981</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/西南1街／中央大街站.md" title="wikilink">西南1街站<br />
中央街站</a></p></td>
<td><p>1 Street SW<br />
Centre Street</p></td>
<td><p>201、202</p></td>
<td><p>市中心</p></td>
<td><p>1981</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/市政厅站.md" title="wikilink">市政厅站</a></p></td>
<td><p>City Hall</p></td>
<td><p>201、202</p></td>
<td><p>市中心</p></td>
<td><p>1981</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/維多利亞公園／牛仔節站.md" title="wikilink">維多利亞公園／牛仔節站</a></p></td>
<td><p>Victoria Park/Stampede</p></td>
<td><p>201</p></td>
<td><p>南线</p></td>
<td><p>1981</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/愛爾頓／牛仔節站.md" title="wikilink">愛爾頓／牛仔節站</a></p></td>
<td><p>Erlton/Stampede</p></td>
<td><p>201</p></td>
<td><p>南线</p></td>
<td><p>1981</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/39大道站.md" title="wikilink">39大道站</a></p></td>
<td><p>39th Avenue</p></td>
<td><p>201</p></td>
<td><p>南线</p></td>
<td><p>1981</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/奇努克站.md" title="wikilink">奇努克站</a></p></td>
<td><p>Chinook</p></td>
<td><p>201</p></td>
<td><p>南线</p></td>
<td><p>1981</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/傳統站.md" title="wikilink">傳統站</a></p></td>
<td><p>Heritage</p></td>
<td><p>201</p></td>
<td><p>南线</p></td>
<td><p>1981</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/南地站.md" title="wikilink">南地站</a></p></td>
<td><p>Southland</p></td>
<td><p>201</p></td>
<td><p>南线</p></td>
<td><p>1981</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/安達臣站.md" title="wikilink">安達臣站</a></p></td>
<td><p>Anderson</p></td>
<td><p>201</p></td>
<td><p>南线</p></td>
<td><p>1981</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/谷原站.md" title="wikilink">谷原站</a></p></td>
<td><p>Canyon Meadows</p></td>
<td><p>201</p></td>
<td><p>南线</p></td>
<td><p>2001</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/魚溪-拉科姆站.md" title="wikilink">魚溪-拉科姆站</a></p></td>
<td><p>Fish Creek-Lacombe</p></td>
<td><p>201</p></td>
<td><p>南线</p></td>
<td><p>2001</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/蕭納西站.md" title="wikilink">蕭納西站</a></p></td>
<td><p>Shawnessy</p></td>
<td><p>201</p></td>
<td><p>南线</p></td>
<td><p>2004</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/索默斯特-布莱特伍德.md" title="wikilink">索默斯特-布莱特伍德</a></p></td>
<td><p>Somerset-Bridlewood</p></td>
<td><p>201</p></td>
<td><p>南线</p></td>
<td><p>2004</p></td>
<td><p><small></small></p></td>
</tr>
</tbody>
</table>

### 蓝线（202线）沿途各站

<table>
<thead>
<tr class="header">
<th><p>中文譯名</p></th>
<th><p>英文站名</p></th>
<th><p>所屬路綫</p></th>
<th><p>线区</p></th>
<th><p>通車年份</p></th>
<th><p>位置座標</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/西南69街站.md" title="wikilink">西南69街站</a></p></td>
<td><p>69th St. SW</p></td>
<td><p>202</p></td>
<td><p>西线</p></td>
<td><p>2012</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/史洛科站.md" title="wikilink">史洛科站</a></p></td>
<td><p>Sirocco</p></td>
<td><p>202</p></td>
<td><p>西线</p></td>
<td><p>2012</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/西南45街站.md" title="wikilink">西南45街站</a></p></td>
<td><p>45th St. SW</p></td>
<td><p>202</p></td>
<td><p>西线</p></td>
<td><p>2012</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/西溪站.md" title="wikilink">西溪站</a></p></td>
<td><p>Westbrook</p></td>
<td><p>202</p></td>
<td><p>西线</p></td>
<td><p>2012</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/沙加纳皮峰站.md" title="wikilink">沙加纳皮峰站</a></p></td>
<td><p>Shaganappi Point</p></td>
<td><p>202</p></td>
<td><p>西线</p></td>
<td><p>2012</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/苏纳塔站.md" title="wikilink">苏纳塔站</a></p></td>
<td><p>Sunalta</p></td>
<td><p>202</p></td>
<td><p>西线</p></td>
<td><p>2012</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/市中心西／柯尔比站.md" title="wikilink">市中心西／柯尔比站</a></p></td>
<td><p>Downtown West – Kerby</p></td>
<td><p>202</p></td>
<td><p>市中心</p></td>
<td><p>2012</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/西南8街站.md" title="wikilink">西南8街站</a></p></td>
<td><p>8 Street SW</p></td>
<td><p>201、202</p></td>
<td><p>市中心</p></td>
<td><p>1981</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/西南6街／西南7街站.md" title="wikilink">西南6街站<br />
西南7街站</a></p></td>
<td><p>6 Street SW<br />
7 Street SW</p></td>
<td><p>201、202</p></td>
<td><p>市中心</p></td>
<td><p>1981</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/西南3街／西南4街站.md" title="wikilink">西南3街站<br />
西南4街站</a></p></td>
<td><p>3 Street SW<br />
4 Street SW</p></td>
<td><p>201、202</p></td>
<td><p>市中心</p></td>
<td><p>1981</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/西南1街／中央大街站.md" title="wikilink">西南1街站<br />
中央大街站</a></p></td>
<td><p>1 Street SW<br />
Centre Street</p></td>
<td><p>201、202</p></td>
<td><p>市中心</p></td>
<td><p>1981</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/市政厅站.md" title="wikilink">市政厅站</a></p></td>
<td><p>City Hall</p></td>
<td><p>201、202</p></td>
<td><p>市中心</p></td>
<td><p>1981</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/橋地／紀念路站.md" title="wikilink">橋地／紀念路站</a></p></td>
<td><p>Bridgeland/Memorial</p></td>
<td><p>202</p></td>
<td><p>东北线</p></td>
<td><p>1985</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/動物園站_(卡加利).md" title="wikilink">動物園站</a></p></td>
<td><p>Zoo</p></td>
<td><p>202</p></td>
<td><p>东北线</p></td>
<td><p>1985</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/巴洛／麥斯貝站.md" title="wikilink">巴洛／麥斯貝站</a></p></td>
<td><p>Barlow/Max Bell</p></td>
<td><p>202</p></td>
<td><p>东北线</p></td>
<td><p>1985</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/富蘭克林站.md" title="wikilink">富蘭克林站</a></p></td>
<td><p>Franklin</p></td>
<td><p>202</p></td>
<td><p>东北线</p></td>
<td><p>1985</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/馬爾堡站.md" title="wikilink">馬爾堡站</a></p></td>
<td><p>Marlborough</p></td>
<td><p>202</p></td>
<td><p>东北线</p></td>
<td><p>1985</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/倫度站.md" title="wikilink">倫度站</a></p></td>
<td><p>Rundle</p></td>
<td><p>202</p></td>
<td><p>东北线</p></td>
<td><p>1985</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/白角站_(卡加利).md" title="wikilink">白角站 (卡加利)</a></p></td>
<td><p>Whitehorn</p></td>
<td><p>202</p></td>
<td><p>东北线</p></td>
<td><p>1985</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/麥禮特－西風站.md" title="wikilink">麥禮特－西風站</a></p></td>
<td><p>McKnight-Westwinds</p></td>
<td><p>202</p></td>
<td><p>东北线</p></td>
<td><p>2007</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/马丁代尔站.md" title="wikilink">马丁代尔站</a></p></td>
<td><p>Martindale</p></td>
<td><p>202</p></td>
<td><p>东北线</p></td>
<td><p>2012</p></td>
<td><p><small></small></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鞍镇站.md" title="wikilink">鞍镇站</a></p></td>
<td><p>Saddletowne</p></td>
<td><p>202</p></td>
<td><p>东北线</p></td>
<td><p>2012</p></td>
<td><p><small></small></p></td>
</tr>
</tbody>
</table>

## 未来发展

### 既有线路延伸

目前红线的南端和蓝线的两端均有进一步的延伸计划。红线计划向南延伸两站至南210大道；蓝线西端计划延伸一站至西85街，东北端计划至少延伸4站至北128大道红石社区（Redstone）附近。

### 绿线

卡尔加里市政府规划修建两条新的放射线路：东南线（Southeast Line）和北线（North
Line），并于市中心以修建地下线形式将两条线路连接，形成一条新的线路（绿线/203线）。目前绿线规划已初步通过市议会的审批，首期工程（北16大道至雪柏，*16
Avenue North / Shepard*）计划于2020年开工。

### 连接机场计划

规划从东北线（蓝线）修建一条支线连接至[卡尔加里国际机场](../Page/卡尔加里国际机场.md "wikilink")。

## 參考文獻

## 外部連結

  - [卡加利交通局](http://www.calgarytransit.com)
  - [LRT in
    Calgary](https://web.archive.org/web/20120327231319/http://www.lrtincalgary.ca/)
    (非官方網頁，含有關卡加利輕鐵的豐富資訊)
  - [www.ctrainmap.com](https://web.archive.org/web/20090804042136/http://www.ctrainmap.com/)
    (加入輕鐵車站標籤的[谷歌地圖](../Page/谷歌.md "wikilink"))

[Category:加拿大輕鐵](../Category/加拿大輕鐵.md "wikilink")
[Category:加拿大鐵路](../Category/加拿大鐵路.md "wikilink")

1.
2.  [Calgary Transit -
    Fares](http://www.calgarytransit.com/html/fares.html)