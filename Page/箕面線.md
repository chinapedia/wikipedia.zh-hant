**箕面線**（）是由[大阪府](../Page/大阪府.md "wikilink")[池田市石橋站至同府](../Page/池田市.md "wikilink")[箕面市箕面站間的](../Page/箕面市.md "wikilink")[阪急電鐵鐵道路線](../Page/阪急電鐵.md "wikilink")。

主要是通往以紅葉與瀑布聞名的[箕面公園和箕面溫泉的行樂路線](../Page/箕面公園.md "wikilink")、亦是往大阪的上班、學路線。

## 路線資料

  - 路線距離（）：4.0公里
  - [軌距](../Page/軌距.md "wikilink")：1435毫米
  - 站數：4個（包括起終點站）
  - 複線路段：全線
  - 電氣化路段：全線電氣化（直流1500V）
  - [閉塞方式](../Page/閉塞_\(鐵路\).md "wikilink")：自動閉塞式
  - 最高速度：80公里/小時
  - 車輛基地：

## 運行形態

基本為線内折返運行、平日朝夕有往梅田站直通的通勤準急和普通列車。通勤準急在箕面線内於各站停車。

## 历史

阪急電鐵的前身箕面有馬電氣軌道、於1910年開始營運。

  - 1910年（明治43年）
      - 3月10日 石橋 - 箕面間開始營運。
      - 4月12日 櫻井站開始營運。
  - 1921年（大正10年）12月30日 牧落站開始營運。
  - 1969年（昭和44年）8月24日 電車線電壓由600伏特昇壓到1500伏特。
  - 1978年（昭和53年）3月10日
    全線由基於[軌道法的軌道變更成基於](../Page/軌道法.md "wikilink")[地方鐵道法的鐵道](../Page/地方鐵道法.md "wikilink")。

## 車站列表

  - 所有車站都位於[大阪府](../Page/大阪府.md "wikilink")。
  - 線內折返列車，只限往梅田直通列車與普通列車，停靠所有車站。
  - [車站編號自](../Page/車站編號.md "wikilink")2013年12月21日起引入\[1\]\[2\]。

<table>
<thead>
<tr class="header">
<th><p>車站編號</p></th>
<th><p>中文站名</p></th>
<th><p>日文站名</p></th>
<th><p>英文站名</p></th>
<th><p>站間營業距離</p></th>
<th><p>累計營業距離</p></th>
<th><p>接續路線</p></th>
<th><p>所在地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>HK-48</p></td>
<td><p><a href="../Page/石橋站_(大阪府).md" title="wikilink">石橋</a></p></td>
<td></td>
<td></td>
<td><p>-</p></td>
<td><p>0.0</p></td>
<td><p><a href="../Page/阪急電鐵.md" title="wikilink">阪急電鐵</a>：<a href="https://zh.wikipedia.org/wiki/File:Number_prefix_Hankyū_Takarazuka_line.png" title="fig:Number_prefix_Hankyū_Takarazuka_line.png">Number_prefix_Hankyū_Takarazuka_line.png</a> <a href="../Page/寶塚本線.md" title="wikilink">寶塚本線</a>（一部分直通運行至<a href="../Page/梅田站_(阪急).md" title="wikilink">梅田站</a>）</p></td>
<td><p><a href="../Page/池田市.md" title="wikilink">池田市</a></p></td>
</tr>
<tr class="even">
<td><p>HK-57</p></td>
<td><p><a href="../Page/櫻井站_(大阪府).md" title="wikilink">櫻井</a></p></td>
<td></td>
<td></td>
<td><p>1.6</p></td>
<td><p>1.6</p></td>
<td><p> </p></td>
<td><p><a href="../Page/箕面市.md" title="wikilink">箕面市</a></p></td>
</tr>
<tr class="odd">
<td><p>HK-58</p></td>
<td><p><a href="../Page/牧落站.md" title="wikilink">牧落</a></p></td>
<td></td>
<td></td>
<td><p>1.1</p></td>
<td><p>2.7</p></td>
<td><p> </p></td>
<td></td>
</tr>
<tr class="even">
<td><p>HK-59</p></td>
<td><p><a href="../Page/箕面站.md" title="wikilink">箕面</a></p></td>
<td></td>
<td></td>
<td><p>1.3</p></td>
<td><p>4.0</p></td>
<td><p> </p></td>
<td></td>
</tr>
</tbody>
</table>

  - 石橋車站至櫻井車站間通過[豐中市](../Page/豐中市.md "wikilink")（飛地的石橋麻田町），但市內未有設車站。

## 相關條目

  - [日本鐵路線列表](../Page/日本鐵路線列表.md "wikilink")

## 參考資料

[Minoo](../Category/近畿地方鐵路路線.md "wikilink") [線
Minoo](../Category/阪急電鐵路線.md "wikilink")
[Category:1910年啟用的鐵路線](../Category/1910年啟用的鐵路線.md "wikilink")

1.
2.