**胡瓜魚**，英文: Smelt
(fish)，是[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[胡瓜魚目](../Page/胡瓜魚目.md "wikilink")[胡瓜魚總科的其中一科](../Page/胡瓜魚總科.md "wikilink")，**胡瓜魚總科**又名**胡瓜魚上科**。

## 分布

本[科廣泛分布於北半球之](../Page/科.md "wikilink")[溫帶及](../Page/溫帶.md "wikilink")[寒帶水域](../Page/寒帶.md "wikilink")，包括北[太平洋](../Page/太平洋.md "wikilink")、北[大西洋及](../Page/大西洋.md "wikilink")[北極海](../Page/北極海.md "wikilink")。少數種類分布於陸地之淡水域。

## 深度

水深0至300公尺。

## 特徵

本[科魚口大](../Page/科.md "wikilink")，口裂達眼下，下頷稍長於上頷，兩頷、舌頭及口內骨骼皆長有細齒。體被細小圓鱗，但頭部無鱗片。背鰭後方有一枚脂鰭。各鰭皆無硬棘，臀鰭稍延長。背鰭短，位於體背中央或稍後方。腹鰭位於背鰭下之腹緣，體型均小且不易區分。

## 分類

**胡瓜魚科**下分7個屬，如下：

### 異胡瓜魚屬(*Allosmerus*)

  - [長身異胡瓜魚](../Page/長身異胡瓜魚.md "wikilink")(*Allosmerus elongatus*)

### 公魚屬(*Hypomesus*)

  - [日本公魚](../Page/日本公魚.md "wikilink")(*Hypomesus japonicus*)
  - [西太公魚](../Page/西太公魚.md "wikilink")(*Hypomesus nipponensis*)
  - [池沼公魚](../Page/池沼公魚.md "wikilink")(*Hypomesus olidus*)
  - [海公魚](../Page/海公魚.md "wikilink")(*Hypomesus pretiosus*)
  - [越洋公魚](../Page/越洋公魚.md "wikilink")(*Hypomesus transpacificus*)

### 毛鱗魚屬(*Mallotus*)

  - [毛鱗魚](../Page/毛鱗魚.md "wikilink")(*Mallotus villosus*)

### 胡瓜魚屬(*Osmerus*)

  - [胡瓜魚](../Page/胡瓜魚.md "wikilink")(*Osmerus eperlanus*)
  - [亞洲胡瓜魚](../Page/亞洲胡瓜魚.md "wikilink")(*Osmerus dentex*)
  - [美洲胡瓜魚](../Page/美洲胡瓜魚.md "wikilink")(*Osmerus mordax*)
  - [野胡瓜魚](../Page/野胡瓜魚.md "wikilink")(*Osmerus spectrum*)

### 油胡瓜魚屬(*Spirinchus*)

  - [長體油胡瓜魚](../Page/柳葉魚.md "wikilink")(*Spirinchus lanceolatus*)
  - [施氏油胡瓜魚](../Page/施氏油胡瓜魚.md "wikilink")(*Spirinchus starksi*)
  - [油胡瓜魚](../Page/油胡瓜魚.md "wikilink")(*Spirinchus thaleichthys*)

### 細齒鮭屬(蠟魚屬)(*Thaleichthys*)

  - [太平洋細齒鮭](../Page/太平洋細齒鮭.md "wikilink")(*Thaleichthys
    pacificus*)：又稱蠟魚。

## 生態

本[科魚類多數為海水魚](../Page/科.md "wikilink")，少數為溯河洄游，或為純淡水魚。常成群活動，部分種類在海岸附近產卵外，部分魚種如[鮭魚一般](../Page/鮭魚.md "wikilink")，須溯河至淡水域產卵。生殖期的雄魚體型與體色稍有改變，產卵量高，屬肉食性，以小型[無脊椎動物為食](../Page/無脊椎動物.md "wikilink")。

## 經濟利用

為著名的食用魚，以鮮魚或乾製品販賣。

[Category:食用鱼](../Category/食用鱼.md "wikilink")
[\*](../Category/胡瓜魚科.md "wikilink")