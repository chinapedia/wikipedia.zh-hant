**杨德智**（2019年1月26日），籍贯[福建](../Page/福建.md "wikilink")[林森](../Page/林森.md "wikilink")，生於[台灣日治時期](../Page/台灣日治時期.md "wikilink")[臺北州](../Page/臺北州.md "wikilink")[宜蘭郡](../Page/宜蘭郡.md "wikilink")，中華民國[陸軍軍官學校第](../Page/陸軍軍官學校.md "wikilink")33期毕业，陸軍二級上將。
曾任第十九任陸軍軍官學校校长，國防部參謀本部副參謀總長，聯合勤務總司令部總司令，[行政院退除役官兵輔導委員會主任委員](../Page/行政院退除役官兵輔導委員會.md "wikilink")。

## 外部連結

[Category:中華民國行政院國軍退除役官兵輔導委員會主任委員](../Category/中華民國行政院國軍退除役官兵輔導委員會主任委員.md "wikilink")
[Category:中華民國陸軍二級上將](../Category/中華民國陸軍二級上將.md "wikilink")
[Category:中華民國聯勤總司令](../Category/中華民國聯勤總司令.md "wikilink")
[Category:中華民國陸軍軍官學校校長](../Category/中華民國陸軍軍官學校校長.md "wikilink")
[Category:中華民國陸軍軍官學校校友](../Category/中華民國陸軍軍官學校校友.md "wikilink")
[Category:國立宜蘭高級中學校友](../Category/國立宜蘭高級中學校友.md "wikilink")
[Category:閩侯裔台灣人](../Category/閩侯裔台灣人.md "wikilink")
[D德智](../Category/楊姓.md "wikilink")
[Category:宜蘭人](../Category/宜蘭人.md "wikilink")
[Category:中華民國參謀本部人物](../Category/中華民國參謀本部人物.md "wikilink")
[Category:國防大學校友](../Category/國防大學校友.md "wikilink")