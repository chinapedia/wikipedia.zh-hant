[최현.JPG](https://zh.wikipedia.org/wiki/File:최현.JPG "fig:최현.JPG")
**崔贤**（[朝鮮語](../Page/朝鮮語.md "wikilink")：**최현**，），[朝鲜劳动党中央政治局委员](../Page/朝鲜劳动党.md "wikilink")、[朝鲜人民军](../Page/朝鲜人民军.md "wikilink")[大将](../Page/朝鲜次帅列表.md "wikilink")。

## 生平

原名**崔得权**，生于中国吉林[珲春县](../Page/珲春县.md "wikilink")，原籍朝鲜[咸镜北道](../Page/咸镜北道.md "wikilink")[惠山郡](../Page/惠山.md "wikilink")。1924年因进行反日活动被捕入狱，1926年被判处无期徒刑，后被减刑为6年徒刑。1932年7月出狱，参加[延吉县](../Page/延吉县.md "wikilink")[太阳帽赤卫队](../Page/太阳帽赤卫队.md "wikilink")，成为负责人之一，旋加入[中国共产党](../Page/中国共产党.md "wikilink")。1933年1月任延吉抗日游击大队([東北抗日聯軍](../Page/東北抗日聯軍.md "wikilink"))第1中队[政治指导员](../Page/政治指导员.md "wikilink")。1934年3月，任[东北人民革命军第](../Page/东北抗日联军.md "wikilink")2军独立师1团一连政治指导员。1935年底任第一连连长。1936年2月升为第2军1师1团团长。1938年7月任[抗联第一路军第三方面军第](../Page/抗联第一路军.md "wikilink")13团团长。1941年1月中旬进入在苏联的[南野营](../Page/南野营.md "wikilink")。1942年8月抗联余部在苏联缩编后，任苏军[远东方面军第](../Page/远东方面军.md "wikilink")88步兵旅（抗联教导旅）第1营第1连连长。

朝鲜解放后回国。1948年任内务省“三八线”警备旅旅长。1949年2月任人民军第2师师长。1951年2月任人民军第2军团军团长。1956年，崔贤任[朝鲜民族保卫副相](../Page/朝鮮民主主義人民共和國.md "wikilink")，为上将军衔，1958年4月—1962年9月任递信相，1964年当选中央军事委员会副委员长，1968年12月—1972年12月任民族保卫相，大将军衔，1972年12月—1976年5月任国防委员会副委员长、人民武装力量部部长，1976年5月单任国防委员会副委员长。1980年当选中央政治局委员、军委委员。1982年4月9日病逝。

其子[崔龍海](../Page/崔龍海.md "wikilink")，曾任[朝鮮勞動黨中央軍事委員會副委員長](../Page/朝鮮勞動黨中央軍事委員會.md "wikilink")、[朝鮮國防委員會委員](../Page/朝鮮國防委員會.md "wikilink")，现为[朝鲜劳动党中央政治局常委](../Page/朝鲜劳动党中央政治局常委.md "wikilink")、[朝鲜国务委员会第一副委员长](../Page/朝鲜国务委员会.md "wikilink")、[最高人民会议常任委员会委员长](../Page/最高人民会议常任委员会委员长.md "wikilink")。

## 参考资料

  - 辭海編纂委員會. 《辭海》（1999年版） (M). 1. 上海:
    [上海辭書出版社](../Page/上海辭書出版社.md "wikilink").
    2000. ISBN 7-5326-0630-9.

[Category:朝鮮國防委員會副委員長](../Category/朝鮮國防委員會副委員長.md "wikilink")
[Category:朝鮮國防部長](../Category/朝鮮國防部長.md "wikilink")
[Category:朝鮮民主主義人民共和國政治人物](../Category/朝鮮民主主義人民共和國政治人物.md "wikilink")
[Category:朝鲜人民军大将](../Category/朝鲜人民军大将.md "wikilink")
[Category:朝鮮勞動黨中央軍事委員會副委員長](../Category/朝鮮勞動黨中央軍事委員會副委員長.md "wikilink")
[Category:朝鮮勞動黨中央政治局委員](../Category/朝鮮勞動黨中央政治局委員.md "wikilink")
[Category:朝鮮勞動黨黨員](../Category/朝鮮勞動黨黨員.md "wikilink")
[Category:朝鮮半島籍中國共產黨黨員](../Category/朝鮮半島籍中國共產黨黨員.md "wikilink")
[Category:朝鮮朝鮮戰爭人物](../Category/朝鮮朝鮮戰爭人物.md "wikilink")
[Category:朝鮮第二次世界大戰人物](../Category/朝鮮第二次世界大戰人物.md "wikilink")
[최현](../Category/咸鏡北道出身人物.md "wikilink")
[Category:琿春人](../Category/琿春人.md "wikilink")
[Hyun](../Category/崔姓.md "wikilink")