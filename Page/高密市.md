**高密市**位于[中国](../Page/中国.md "wikilink")[山东省东部](../Page/山东省.md "wikilink")，是[潍坊市代管的一个县级市](../Page/潍坊市.md "wikilink")。地处[胶东半岛和](../Page/胶东半岛.md "wikilink")[山东内陆的中间](../Page/山东.md "wikilink")，东临山东省最大城市[青岛](../Page/青岛.md "wikilink")。高密市西北与[安丘市](../Page/安丘市.md "wikilink")，西南与[诸城市](../Page/诸城市.md "wikilink")，东部与[胶州市](../Page/胶州市.md "wikilink")，北部与[平度市接壤](../Page/平度市.md "wikilink")。

高密市是[国务院最早批准的沿海地区对外开放县](../Page/国务院.md "wikilink")（市）之一，也是[山东省肉类生产第四大、中国第13大县市](../Page/中国肉类生产百强县列表.md "wikilink")。

## 地理

### 气候

高密地处暖温带东部季风区，气候宜人，四季分明，雨量集中，雨热同期。年均气温12.7℃，无霜期平均212天，日照时数2453小时，年均降雨量619.6毫米。

## 历史

[秦置高密县](../Page/秦朝.md "wikilink")，治今高密市西北。属[胶东郡](../Page/胶东郡.md "wikilink")。《漢武故事》言漢武帝数歲，封膠東王。[西汉建元](../Page/西汉.md "wikilink")，為[高密国治](../Page/高密国.md "wikilink")。[唐](../Page/唐.md "wikilink")[武德六年](../Page/武德.md "wikilink")（623年）移治今高密市，属[密州](../Page/密州.md "wikilink")。元属[胶州](../Page/胶州.md "wikilink")，明初属[青州府](../Page/青州府.md "wikilink")，[洪武九年](../Page/洪武.md "wikilink")（1376年）后属[莱州府](../Page/莱州府.md "wikilink")，清光绪三十一年，县属[胶州府](../Page/胶县.md "wikilink")。[抗日战争及](../Page/抗日战争.md "wikilink")[國共內戰时期](../Page/國共內戰.md "wikilink")，县境曾先后析置[胶高](../Page/胶高县.md "wikilink")、[胶河等县](../Page/胶河县.md "wikilink")。1994年5月18日撤销高密县，改设高密市。

### 高密事件

清[光绪二十五年](../Page/光绪.md "wikilink")（1899年），德国强占民地民房，修筑[胶济铁路](../Page/胶济铁路.md "wikilink")；6月高密县大吕庄民众阻修铁路，被德军枪杀20余人；1900年1月，高密武生李金榜等率众阻止铁路，要求改线，时任山东巡抚[袁世凯下令逮捕](../Page/袁世凯.md "wikilink")；9月高密数百民众在南流阻修铁路，又被德军枪杀3人。

## 行政区划

下辖3个街道、7个镇，993个行政村：

  - 街道办事处：[朝阳街道](../Page/朝阳街道.md "wikilink")、[醴泉街道](../Page/醴泉街道.md "wikilink")、[密水街道](../Page/密水街道.md "wikilink")。
  - 镇：[柏城镇](../Page/柏城镇.md "wikilink")、[夏庄镇](../Page/夏庄镇.md "wikilink")、[姜庄镇](../Page/姜庄镇.md "wikilink")、[大牟家镇](../Page/大牟家镇.md "wikilink")、[阚家镇](../Page/阚家镇.md "wikilink")、[井沟镇](../Page/井沟镇.md "wikilink")、[柴沟镇](../Page/柴沟镇.md "wikilink")。

## 经济

2009年GDP达到274亿元，列[濰坊市第四](../Page/濰坊市.md "wikilink")。产业以纺织、化纤、建材、机械制造、农产品加工为主。

## 人口

2010年[中国第六次人口普查时](../Page/中国第六次人口普查.md "wikilink")，高密市共有人口895582人。共有家庭262670户，平均每户3.23人。14岁以下的少年儿童共143702人，占总人口16.04%；15-64岁人口共653610人，占总人口72.98%；65岁及以上的老年人共98270人，占总人口的10.97%。男性共计447238人，占总人口49.93%；女性共计448344，占总人口50.06%。本地居住的人口中，拥有本地户籍的人口为778962人，占86.97%。\[1\]

## 名人

  - [晏婴](../Page/晏婴.md "wikilink")，東周時期諸侯國齊國的行政官員，侍奉景公，是著名的思想家、外交家。曾多次出使楚國，與楚王有精辟的對話。
  - [郑玄](../Page/郑玄.md "wikilink")（127年－200年）字康成，北海高密（今山東省高密市）人，東漢經學家。鄭益（字益恩）之父，鄭小同（字子真）祖父。
  - [刘墉](../Page/刘墉.md "wikilink")，清朝乾隆時期政治人物。
  - [莫言](../Page/莫言.md "wikilink")，原名管謨業，中国当代作家，2012年[诺贝尔文学奖得主](../Page/诺贝尔文学奖.md "wikilink")，作品有《[红高粱](../Page/红高粱家族.md "wikilink")》等。
  - [刘世文](../Page/刘世文.md "wikilink")，人称“欧洲哥”，香港著名欧洲旅游专家顾问、专栏作家。
  - [林建华](../Page/林建华.md "wikilink")，2015年出任[北京大学校长](../Page/北京大学校长列表.md "wikilink")。

## 参考文献

{{-}}

[高密市](../Category/高密市.md "wikilink")
[市](../Category/潍坊区县市.md "wikilink")
[潍坊](../Category/山东县级市.md "wikilink")

1.