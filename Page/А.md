__NOTOC__
[Cyrillic_A.png](https://zh.wikipedia.org/wiki/File:Cyrillic_A.png "fig:Cyrillic_A.png")
**А**, **а**（称呼为 а,
*a*）是一个广泛用在斯拉夫语言的[西里尔字母](../Page/西里尔字母.md "wikilink")，由希腊字母
[Α](../Page/Α.md "wikilink") 演变而成。

## 字母的次序

在[俄语](../Page/俄语.md "wikilink")、[乌克兰语](../Page/乌克兰语.md "wikilink")、[白俄罗斯语](../Page/白俄罗斯语.md "wikilink")、[保加利亚语](../Page/保加利亚语.md "wikilink")、[马其顿语](../Page/马其顿语.md "wikilink")、[塞尔维亚语等字母中都是排第](../Page/塞尔维亚语.md "wikilink")1位。

## 音值

通常为 。

在俄语字中有**部份**单字的/a/不是重音时， /a/会弱化为 и 音，例如  的发音是  /cɕiˈsof/。

在[车臣语或](../Page/车臣语.md "wikilink")[印古什语等语言亦代表](../Page/印古什语.md "wikilink")
 或 。

## 字符编码

| 字符编码                               | [Unicode](../Page/Unicode.md "wikilink") | [ISO 8859-5](../Page/ISO/IEC_8859-5.md "wikilink") | [KOI-8](../Page/KOI-8.md "wikilink") | [GB 2312](../Page/GB_2312.md "wikilink") | [HKSCS](../Page/香港增補字符集.md "wikilink") |
| ---------------------------------- | ---------------------------------------- | -------------------------------------------------- | ------------------------------------ | ---------------------------------------- | -------------------------------------- |
| [大写](../Page/大寫字母.md "wikilink") А | U+0410                                   | B0                                                 | E1                                   | A7A1                                     | C7F3                                   |
| [小写](../Page/小寫字母.md "wikilink") а | U+0430                                   | D0                                                 | C1                                   | A7D1                                     | C855                                   |

## 参考资料

  -
## 参看

  - <span lang="el" style="font-size:120%;">[Α
    α](../Page/Α.md "wikilink")</span>（[希腊字母](../Page/希腊字母.md "wikilink")）
  - <span lang="en" style="font-size:120%;">[A
    a](../Page/A.md "wikilink")</span>（[拉丁字母](../Page/拉丁字母.md "wikilink")）

## 外部連結

  -
  -
  - [Буква А](http://graphemica.com/А) на сайте graphemica.com

  - [Буква а](http://graphemica.com/а) на сайте graphemica.com

[Category:西里尔字母](../Category/西里尔字母.md "wikilink")