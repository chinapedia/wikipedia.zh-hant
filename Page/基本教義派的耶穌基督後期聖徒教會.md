[FLDS_Eldorado_hi.jpg](https://zh.wikipedia.org/wiki/File:FLDS_Eldorado_hi.jpg "fig:FLDS_Eldorado_hi.jpg")[艾多拉多市](../Page/艾多拉多市.md "wikilink")（Eldorado）的FLDS教堂\]\]


**基本教義派的耶穌基督後期聖徒教會**（**Fundamentalist Church of Jesus Christ of Latter
Day
Saints**，常簡稱為**FLDS**）是[摩門教基本教義派](../Page/摩門教基本教義派.md "wikilink")（Mormon
fundamentalist）最大的一個分支教派\[1\]\[2\]，也是目前美國最大的奉行[一夫多妻制的團體](../Page/一夫多妻制.md "wikilink")\[3\]。一群創始教徒在1930年代離開[耶穌基督後期聖徒教會](../Page/耶穌基督後期聖徒教會.md "wikilink")（The
Church of Jesus Christ of Latter-day
Saints，LDS）後創立了FLDS。造成這次分裂主要起因為LDS放棄[多配偶制](../Page/多配偶制.md "wikilink")，並且將南[犹他州和北](../Page/犹他州.md "wikilink")[亞利桑那州實行多妻制的教徒逐出教會](../Page/亞利桑那州.md "wikilink")。基本教義派的耶穌基督後期聖徒教會（FLDS）和耶穌基督後期聖徒教會（LDS）是兩個完全不同的宗教派別，兩者之間也沒有任何的正式關係。

FLDS教會於1930年代成立後，其總部便一直設於美國[猶他州](../Page/猶他州.md "wikilink")[希爾達爾市](../Page/希爾達爾市.md "wikilink")（Hildale,
Colorado City,
UT），該市與[亞利桑那州](../Page/亞利桑那州.md "wikilink")[科羅拉多市互組為](../Page/科羅拉多市.md "wikilink")[雙城](../Page/雙城.md "wikilink")（twin
city）。而2004年有新聞媒體指出教會可能將總部遷往[德州](../Page/德克萨斯州.md "wikilink")[艾多拉多市](../Page/艾多拉多市.md "wikilink")（Eldorado），有FLDS的教徒在該地建造了大型的[教堂](../Page/教堂.md "wikilink")\[4\]。[沃倫·杰弗斯](../Page/沃倫·杰弗斯.md "wikilink")（Warren
Jeffs）在2002年繼承父親[魯倫·杰弗斯](../Page/魯倫·杰弗斯.md "wikilink")（Rulon
Jeffs）成為該教派的領袖。沃倫·杰弗斯曾因性侵犯罪案件遭通緝約兩年。從2006年5月開始，至他遭逮捕的2006年8月間，沃倫·杰弗斯名列美國[聯邦調查局](../Page/聯邦調查局.md "wikilink")（FBI）的[十大通緝要犯](../Page/美國聯邦調查局十大通緝要犯.md "wikilink")\[5\]。在2007年9月25日，杰弗斯因兩起性侵案件被認定有罪\[6\]\[7\]，並遭求處十年以上的有期徒刑至終身監禁\[8\]。

沃倫·杰弗斯在2007年11月20日正式辭去FLDS教會的領袖職務\[9\]。

## 教會總部和會友

由於FLDS教會內部封閉，外界無法得知該教會的會友總數。但是一般估計總數約在6千到1萬人之間，分布在[亞利桑那州的](../Page/亞利桑那州.md "wikilink")[科羅拉多市和](../Page/科羅拉多市.md "wikilink")[猶他州的](../Page/猶他州.md "wikilink")[希爾戴爾聯合起來的](../Page/希爾戴爾.md "wikilink")[雙城的社區中](../Page/雙城.md "wikilink")\[10\]。在買下位於德州[Eldorado的這個稱為](../Page/Eldorado,_Texas.md "wikilink")[Yearning
for Zion
Ranch的農莊後](../Page/Yearning_for_Zion_Ranch.md "wikilink")，似乎有將總部與大多數忠信信眾搬遷到此處的意圖。該教會長久以來在[加拿大也有一個分部](../Page/加拿大.md "wikilink")，位於[卑詩省的](../Page/卑詩省.md "wikilink")[Bountiful（滿地富）](../Page/Bountiful,_British_Columbia.md "wikilink")\[11\]。

### 財務

FLDS的教會擁有一間製造飛機零組件的工廠，賣零組件給美國政府。從1998年到2007年為止，總共交付了約170萬美元的訂單\[12\]。

## 歷史

### 緣起

## 特殊教義

### 服裝規定

該教會的所有成員必須遵守嚴格的服裝穿著規定。女性不得穿著裤子或任何露出膝蓋的裙子。男性則穿著簡單的衣物，通常是有顏色的襯衫和一般的长褲。短袖、短裤无论男女都不得穿着。男性不得在身上刺青或穿洞，但女性可以有對邊耳洞。女性和女童通常穿著自家縫製的單色長袖洋裝，約長及小腿，並搭配長襪，而頭髮則是向上梳起或藏在[科伊夫帽](../Page/科伊夫帽.md "wikilink")（Coif）帽下\[13\]。

## 資料來源

[Category:摩爾門教](../Category/摩爾門教.md "wikilink")

1.  Krakauer, Jon. *Under the Banner of Heaven*. New York:Random House,
    2003. ISBN 1400032806
2.  該教會目前約有8000名信徒
3.  [Principle Voices - Polygamist Census: LDS Splinter Groups
    Growing](http://www.principlevoices.org/article.php?story=20050828124239994)

4.  [The Eldorado
    Success](http://www.myeldorado.net/YFZ%20Aerial%20Tour/yfztour1.html)

5.  [HAVE YOU SEEN THIS MAN? FBI Announces New Top
    Tenner](http://www.fbi.gov/page2/may06/jeffs050606.htm) , from the
    [FBI](../Page/FBI.md "wikilink") Headline
    Archives，2006年5月6日，於2008年4月9日查閱。
6.
7.
8.  [Polygamist 'prophet' to serve at least 10 years in prison -
    CNN.com](http://www.cnn.com/2007/US/law/11/20/jeffs.sentence/index.html)
9.
10. 見美國政府普查網頁[2005年](http://www.census.gov/popest/cities/tables/SUB-EST2005-04-04.csv)、[2006年](http://www.census.gov/popest/cities/tables/SUB-EST2006-04-49.csv)
11. [在加拿大的一夫多妻制的摩門教](http://www.religioustolerance.org/lds_poly1.htm)
12. 見[2008年4月17日CNN報導](http://www.cnn.com/2008/US/04/17/polygamy.pentagon/index.html)
13. [The polygamist women of Colorado
    City](http://www.rickross.com/reference/polygamy/polygamy10.html)