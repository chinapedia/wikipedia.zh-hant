**漢語音節結構**，指的是從不同角度出發採取不同方法對[漢字字音的構成所做的分析](../Page/漢字.md "wikilink")\[1\]。漢語音節在通常情況下指的就是一個漢字的[字音](../Page/字音.md "wikilink")，如「上、中、下、天、地、人」每個漢字就是一個[音節](../Page/音節.md "wikilink")。特殊的情況，如[北京話](../Page/北京話.md "wikilink")[兒化韻的](../Page/兒化韻.md "wikilink")「門兒」、「聲兒」等，是兩個漢字構成一個音節，又如後起的單位字「瓩、兛」等，是一個漢字包含兩個音節\[2\]\[3\]。

## 傳統分析法

從傳統的漢字注音方法[反切的角度出發](../Page/反切.md "wikilink")，一個漢語音節可分成[聲](../Page/聲.md "wikilink")、[韻兩部分](../Page/韻.md "wikilink")，如「東，『德紅切』」，[反切上字](../Page/反切上字.md "wikilink")「德」表[聲母](../Page/聲母.md "wikilink")「[端](../Page/端母.md "wikilink")」，[反切下字](../Page/反切下字.md "wikilink")「紅」表[韻母](../Page/韻母.md "wikilink")\[uŋ\]
\[4\]和[聲調之](../Page/聲調.md "wikilink")[平聲](../Page/平聲.md "wikilink")——韻母、聲調合稱爲“韻”。

一個字音又可分爲「聲」、「韻」、「調」三部分，或者叫做「聲母」、「韻母」和「聲調」\[5\]。如：「流連」二字聲母和聲調相同，韻母不同；「抖擻」二字韻母和聲調相同，聲母不同；「買賣」二字，聲母和韻母相同，聲調不同\[6\]。

## 五音位

從[音位的角度出發](../Page/音位.md "wikilink")，對[現代漢語諸方言和宋](../Page/現代漢語.md "wikilink")、元以來的近古漢語，可將一個漢字的字音分成五個音位\[7\]。[劉復把一個字音比喻成一種生物](../Page/刘半农.md "wikilink")，按發音的先後順序將這五個音位命名爲：頭、頸、腹、尾、神。將其翻譯成現代[音韻學習慣的說法就是](../Page/音韻學.md "wikilink")：[聲母](../Page/聲母.md "wikilink")（頭）、[介音或](../Page/介音.md "wikilink")[韻頭](../Page/韻頭.md "wikilink")（頸）、[韻腹](../Page/韻腹.md "wikilink")（腹）、[韻尾](../Page/韻尾.md "wikilink")（尾）和[聲調](../Page/聲調.md "wikilink")（神）。

漢語音節構成中的五個音位，只有韻腹和聲調是必需的，其他音位可有可無。也就是說：

  - 有的漢字的字音可以沒有聲母，如[北京話的](../Page/北京話.md "wikilink")「彎」字，介音爲-u-，韻腹爲-a-,韻尾爲-n，聲調爲[陰平](../Page/陰平.md "wikilink")，沒有聲母。對沒有聲母的字音有時候也說成帶[零聲母](../Page/零聲母.md "wikilink")；
  - 有的漢字的字音沒有介音，如[梅州話的](../Page/梅州話.md "wikilink")「塔」字，聲母爲
    tʰ-，韻腹爲-a-，韻尾爲-p，聲調爲[入聲](../Page/入聲.md "wikilink")，對有[四呼的方言來說](../Page/四呼.md "wikilink")，無介音的字屬於[開口呼](../Page/開口呼.md "wikilink")。
  - 有的漢字的字音沒有韻尾，如[上海話的](../Page/上海話.md "wikilink")「回」字，聲母爲
    ɦ-，介音爲-u-，韻腹爲-e-，聲調爲陽平。

竝不是所有漢語方言的字音結構都完整地包含了「聲、介、腹、尾、調」五個構成部分\[8\]，比如粵語的音節結構中就沒有真正意義上的「介音」，其「開」「合」兩呼是用不同的聲母來區分的，而不是靠介音\[9\]。又如，[朝鮮語的漢字發音也可以被看成是一種漢語的方言音](../Page/朝鮮語.md "wikilink")，但現代[朝鮮語是沒有聲調的](../Page/朝鮮語.md "wikilink")。另外，有的方言的音節結構要比這個五位的模式複雜，如[福州話的](../Page/福州話.md "wikilink")「卓」字讀如\[tauk\]，韻腹爲-a-，韻尾卻是由/u/、/k/兩個音位構成的。

漢語音節結構示例（[IPA注音](../Page/IPA.md "wikilink")）

<table>
<thead>
<tr class="header">
<th><p>方言</p></th>
<th><p>例字</p></th>
<th><p>聲母</p></th>
<th><p>介音</p></th>
<th><p>韻腹</p></th>
<th><p>韻尾</p></th>
<th><p>聲調</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>北京話</p></td>
<td><p>裝</p></td>
<td></td>
<td><p>u</p></td>
<td><p>a</p></td>
<td></td>
<td><p>陰平</p></td>
</tr>
<tr class="even">
<td><p>梅州話</p></td>
<td><p>爵</p></td>
<td><p>ts</p></td>
<td><p>i</p></td>
<td></td>
<td><p>k</p></td>
<td><p>入聲</p></td>
</tr>
<tr class="odd">
<td><p>廣府話</p></td>
<td><p>險</p></td>
<td><p>h</p></td>
<td><p>-</p></td>
<td><p>i</p></td>
<td><p>m</p></td>
<td><p>陰上</p></td>
</tr>
<tr class="even">
<td><p>上海話</p></td>
<td><p>腳</p></td>
<td></td>
<td><p>i</p></td>
<td><p>a</p></td>
<td></td>
<td><p>入聲</p></td>
</tr>
<tr class="odd">
<td><p>朝鮮語</p></td>
<td><p>決</p></td>
<td><p>k</p></td>
<td><p>i</p></td>
<td></td>
<td><p>l</p></td>
<td><p>-</p></td>
</tr>
</tbody>
</table>

## 注释

[Category:語音學](../Category/語音學.md "wikilink")
[Category:漢語音韻學](../Category/漢語音韻學.md "wikilink")

1.
2.
3.
4.  [王力擬音](../Page/王力_\(语言学家\).md "wikilink")
5.
6.
7.
8.
9.