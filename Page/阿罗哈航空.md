**阿罗哈航空公司**（英文：Aloha
Airlines）是一家总部设在[美国](../Page/美国.md "wikilink")[夏威夷](../Page/夏威夷.md "wikilink")[檀香山的航空公司](../Page/檀香山.md "wikilink")。它在夏威夷群岛内拥有众多的航线，还有许多夏威夷和[美国西海岸之间的航线](../Page/美国.md "wikilink")。它的主要基地是[檀香山国际机场](../Page/檀香山国际机场.md "wikilink")。

## 历史

2008年3月20日，阿羅哈航空公司因為抵受不住激烈的市場競爭和燃油價格持續上升的壓力，向夏威夷法院第二次申請破產保護。3月30日，由於找不到適合的買家，宣布在3月31日後停止提供所有客運服務。

## 意外事件

1988年4月28日，一架阿羅哈航空的[波音737-200型客機在飛往檀香山國際機場途中發生爆裂性失壓事故](../Page/波音737.md "wikilink")，導致約頭等艙部位的上半部外殼完全破損，機上一名機組人員墮海失蹤，飛機最後奇蹟地安全迫降[茂宜島的](../Page/茂宜島.md "wikilink")[卡富魯伊機場](../Page/卡富魯伊機場.md "wikilink")。

## 通航城市

阿罗哈航空公司可通达下列城市（截至2008年3月）\[1\]：

  - 加利福尼亚州
      - [奥克兰国际机场 (美国)](../Page/奥克兰国际机场_\(美国\).md "wikilink")
      - [圣塔安那](../Page/圣塔安那.md "wikilink")[約翰·韋恩機場](../Page/約翰·韋恩機場.md "wikilink")
      - [圣迭戈国际机场](../Page/圣迭戈国际机场.md "wikilink")
      - [薩克拉門托國際機場](../Page/薩克拉門托國際機場.md "wikilink")
  - 夏威夷州
      - [希洛國際機場](../Page/希洛國際機場.md "wikilink")（[夏威夷岛](../Page/夏威夷岛.md "wikilink")）
      - [科纳](../Page/科纳.md "wikilink")（夏威夷岛）
      - [檀香山國際機場](../Page/檀香山國際機場.md "wikilink")（[瓦胡岛](../Page/瓦胡岛.md "wikilink")）
      - [卡富魯伊機場](../Page/卡富魯伊機場.md "wikilink")（[茂宜岛](../Page/茂宜岛.md "wikilink")）
      - [里呼依](../Page/里呼依.md "wikilink")（[考艾岛](../Page/考艾岛.md "wikilink")）
  - 内华达州
      - [拉斯维加斯](../Page/拉斯维加斯.md "wikilink")[麥卡倫國際機場](../Page/麥卡倫國際機場.md "wikilink")
      - [里诺](../Page/里诺_\(内华达州\).md "wikilink")

## 机队

[Aloha_732.jpg](https://zh.wikipedia.org/wiki/File:Aloha_732.jpg "fig:Aloha_732.jpg")
阿罗哈航空公司机队组成如下（截至2008年3月）:

  - 10架[波音737-200](../Page/波音737.md "wikilink")
  - 3架波音737-200C货机
  - 8架波音737-700
  - 1架波音737-800（從TransAvia租借）

## 參考資料

<div class="references-small">

<references />

</div>

[Category:檀香山公司](../Category/檀香山公司.md "wikilink")
[Category:美國已結業航空公司](../Category/美國已結業航空公司.md "wikilink")
[Category:1946年成立的航空公司](../Category/1946年成立的航空公司.md "wikilink")
[Category:2008年結業航空公司](../Category/2008年結業航空公司.md "wikilink")

1.