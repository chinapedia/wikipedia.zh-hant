**大有**（928年三月-942年三月）是[南汉高祖](../Page/南汉.md "wikilink")[劉龑的](../Page/劉龑.md "wikilink")[年号](../Page/年号.md "wikilink")，共计15年。

## 纪年

| 大有                               | 元年                             | 二年                             | 三年                             | 四年                             | 五年                             | 六年                             | 七年                             | 八年                             | 九年                             | 十年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 928年                           | 929年                           | 930年                           | 931年                           | 932年                           | 933年                           | 934年                           | 935年                           | 936年                           | 937年                           |
| [干支](../Page/干支纪年.md "wikilink") | [戊子](../Page/戊子.md "wikilink") | [己丑](../Page/己丑.md "wikilink") | [庚寅](../Page/庚寅.md "wikilink") | [辛卯](../Page/辛卯.md "wikilink") | [壬辰](../Page/壬辰.md "wikilink") | [癸巳](../Page/癸巳.md "wikilink") | [甲午](../Page/甲午.md "wikilink") | [乙未](../Page/乙未.md "wikilink") | [丙申](../Page/丙申.md "wikilink") | [丁酉](../Page/丁酉.md "wikilink") |
| 大有                               | 十一年                            | 十二年                            | 十三年                            | 十四年                            | 十五年                            |                                |                                |                                |                                |                                |
| [公元](../Page/公元纪年.md "wikilink") | 938年                           | 939年                           | 940年                           | 941年                           | 942年                           |                                |                                |                                |                                |                                |
| [干支](../Page/干支纪年.md "wikilink") | [戊戌](../Page/戊戌.md "wikilink") | [己亥](../Page/己亥.md "wikilink") | [庚子](../Page/庚子.md "wikilink") | [辛丑](../Page/辛丑.md "wikilink") | [壬寅](../Page/壬寅.md "wikilink") |                                |                                |                                |                                |                                |

## 參看

  - [大有卦](../Page/周易六十四卦列表#大有.md "wikilink")
  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 同期存在的其他政权年号
      - [天成](../Page/天成_\(李嗣源\).md "wikilink")（926年四月至930年二月）：後唐—[李嗣源之年號](../Page/李嗣源.md "wikilink")
      - [長興](../Page/長興_\(李嗣源\).md "wikilink")（930年二月至933年十二月）：後唐—李嗣源之年號
      - [應順](../Page/應順.md "wikilink")（934年正月至四月）：後唐—[李從厚之年號](../Page/李從厚.md "wikilink")
      - [清泰](../Page/清泰.md "wikilink")（934年四月至936閏十一月）：後唐—[李從珂之年號](../Page/李從珂.md "wikilink")
      - [天福](../Page/天福_\(石敬瑭\).md "wikilink")（936年十一月至944年六月）：[後晉](../Page/後晉.md "wikilink")—[石敬瑭之年號](../Page/石敬瑭.md "wikilink")
      - [乾貞](../Page/乾貞.md "wikilink")（927年十一月至929年十月）：吳—楊溥之年號
      - [大和](../Page/大和_\(楊溥\).md "wikilink")（929年十月至935年八月）：吳—楊溥之年號
      - [天祚](../Page/天祚.md "wikilink")（935年九月至937年十月）：吳—楊溥之年號
      - [昇元](../Page/昇元.md "wikilink")（937年十月至943年二月）：[南唐](../Page/南唐.md "wikilink")—[李昪之年號](../Page/李昪.md "wikilink")
      - [龍啟](../Page/龍啟.md "wikilink")（933年至934年）：[閩](../Page/闽_\(十国\).md "wikilink")—[王延鈞之年號](../Page/王延鈞.md "wikilink")
      - [永和](../Page/永和_\(王延鈞\).md "wikilink")（935年正月至936年二月）：閩—王延鈞之年號
      - [通文](../Page/通文.md "wikilink")（936年三月至939年七月）：閩—[王繼鵬之年號](../Page/王繼鵬.md "wikilink")
      - [永隆](../Page/永隆_\(王延羲\).md "wikilink")（939年閏七月至943年正月）：閩—[王延羲之年號](../Page/王延羲.md "wikilink")
      - [明德](../Page/明德_\(孟知祥\).md "wikilink")（934年四月至937年）：[後蜀](../Page/後蜀.md "wikilink")—[孟知祥之年號](../Page/孟知祥.md "wikilink")
      - [廣政](../Page/廣政_\(孟昶\).md "wikilink")（938年正月至965年正月）：後蜀—[孟昶之年號](../Page/孟昶.md "wikilink")
      - [宝正](../Page/宝正.md "wikilink")（926年至931年）：吳越—錢鏐之年號
      - [尊聖](../Page/尊聖.md "wikilink")（928年至929年）：[大天興](../Page/大天興.md "wikilink")—[趙善政之年號](../Page/趙善政.md "wikilink")
      - [大明](../Page/大明_\(楊干真\).md "wikilink")（931年至937年）：[大義寧](../Page/大義寧.md "wikilink")—[楊干真之年號](../Page/楊干真.md "wikilink")
      - [文德](../Page/文德_\(段思平\).md "wikilink")（938年起）：[大理國](../Page/大理國.md "wikilink")—[段思平之年號](../Page/段思平.md "wikilink")
      - [天顯](../Page/天顯.md "wikilink")（926年二月至938年）：[契丹](../Page/遼朝.md "wikilink")—[耶律阿保機](../Page/耶律阿保機.md "wikilink")、[耶律德光之年號](../Page/耶律德光.md "wikilink")
      - [會同](../Page/會同.md "wikilink")（938年十一月至947年正月）：契丹—耶律德光之年號
      - [甘露](../Page/甘露_\(耶律倍\).md "wikilink")（926年至936年）：[東丹](../Page/東丹.md "wikilink")—[耶律倍之年號](../Page/耶律倍.md "wikilink")
      - [同慶](../Page/同慶_\(尉遲烏僧波\).md "wikilink")（912年至966年）：[于闐](../Page/于闐.md "wikilink")—[尉遲烏僧波之年號](../Page/尉遲烏僧波.md "wikilink")
      - [天授](../Page/天授_\(高麗太祖\).md "wikilink")（918年—933年）：[高麗](../Page/高麗.md "wikilink")—[高麗太祖王建之年號](../Page/高麗太祖.md "wikilink")
      - [延長](../Page/延長_\(醍醐天皇\).md "wikilink")（923年閏四月十一日至931年四月二十六日）：日本[醍醐天皇](../Page/醍醐天皇.md "wikilink")、[朱雀天皇年号](../Page/朱雀天皇.md "wikilink")
      - [承平](../Page/承平_\(朱雀天皇\).md "wikilink")（931年四月二十六日至938年五月二十二日）：日本朱雀天皇年号
      - [天慶](../Page/天慶_\(朱雀天皇\).md "wikilink")（938年五月二十二日至947年四月二十二日）：日本朱雀天皇、[村上天皇年号](../Page/村上天皇.md "wikilink")

[Category:南汉年号](../Category/南汉年号.md "wikilink")
[Category:920年代中国政治](../Category/920年代中国政治.md "wikilink")
[Category:930年代中国政治](../Category/930年代中国政治.md "wikilink")
[Category:940年代中国政治](../Category/940年代中国政治.md "wikilink")