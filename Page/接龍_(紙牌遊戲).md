[Klondike_(solitare).png](https://zh.wikipedia.org/wiki/File:Klondike_\(solitare\).png "fig:Klondike_(solitare).png")中的接龍\]\]
**接龍**（Klondike）是一個單人玩的經典紙牌遊戲。游戏于19世纪晚期开始流行。\[1\]\[2\]

## 規則

將一副標準52張[撲克牌](../Page/撲克牌.md "wikilink")（不包括鬼牌）分堆成七組，並依照數字由一至七張紙牌分堆從左排至右，堆中最上方的紙牌以翻開顯示，其餘以覆蓋的方式堆疊，剩下未分組的紙牌同樣以覆蓋的方式堆疊在左上角甲板，這就是遊戲一開始的排列方式。

移動撲克牌的方式：您只能移動已翻開的紙牌，並且從大到小，以紅黑花色交錯的方式將紙牌移動到另一已翻開的紙牌上，而原本翻開顯示的紙牌移走後，就可將同堆的最上的覆蓋紙牌掀開。另外有
A 紙牌可先移動到右上角當成收集堆的基礎，並依照花色收集成四堆，由小排到大的方式收集。如果某個牌堆是空的，則您可以將
K（以及其牌堆中所有的紙牌）移到該牌堆。

另外在左上角甲板的牌堆有許多不同的使用方式，遊戲時只能擇其一：

  - 一次發一張牌，若有可移動的紙牌可移動至該牌堆，若沒有可移動的紙牌則可繼續發下一張紙牌，發完時可重新由第一張發牌。
  - 一次發三張牌，三張牌中只有最上的牌可移動，移動一張牌後可繼續移動第二張，不可以逆向發牌，若沒有可移動的紙牌則可繼續發下三張紙牌，發完時不可洗牌，重新發三張牌，所以得到的三張牌會跟上一次所發的紙牌的組合方式相差甚遠，難度較高。

將所有撲克牌完全收集完畢時遊戲結束。

## 电子游戏

  - Michael A.
    Casteel的[共享软件版本于](../Page/共享软件.md "wikilink")1984年在[Macintosh上发布](../Page/Macintosh.md "wikilink")，至今仍在更新，支持[iPhone](../Page/iPhone.md "wikilink")。
  - [纸牌](../Page/纸牌_\(电子游戏\).md "wikilink")（Solitaire）是随着[Windows
    3.0发布的接龙游戏](../Page/Windows_3.0.md "wikilink")，出现在之后多数版本的[Windows中](../Page/Windows.md "wikilink")，可在『開始』-\>『執行』中鍵入**sol**執行遊戲。
  - 在Windows 8和Windows 8.1中,Klondike 包含於[Microsoft Solitaire
    Collection中](../Page/Microsoft_Solitaire_Collection.md "wikilink"),並不再隨附於Windows
    8和Windows 8.1,而是在Windows Store上提供下載。
  - [GNOME和](../Page/GNOME.md "wikilink")[KDE很早就有此游戏](../Page/KDE.md "wikilink")（sol和kpat）。

## 参考资料

[K](../Page/分類:撲克牌遊戲.md "wikilink")

[K](../Category/Windows遊戲.md "wikilink")
[K](../Category/電子桌上遊戲.md "wikilink")
[Category:Windows组件](../Category/Windows组件.md "wikilink")

1.
2.