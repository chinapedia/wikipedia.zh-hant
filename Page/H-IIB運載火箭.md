**H-IIB運載火箭**（）是液體低溫的火箭系列，提供一個不可回收的發射系統，用來發射[H-II運送載具至](../Page/H-II運送載具.md "wikilink")[國際太空站](../Page/国际空间站.md "wikilink")。由[三菱重工業為日本航空太空探險代辦處](../Page/三菱重工業.md "wikilink")（[宇宙航空研究開發機構](../Page/日本宇宙航空研究開發機構.md "wikilink")）製造。發射將在種子島太空中心。第一次試飛於2009年（平成21年）9月11日發射成功。

## 技術諸元

|                                     |                                                                          |                                           |                                                                   |                                                                                       |
| ----------------------------------- | ------------------------------------------------------------------------ | ----------------------------------------- | ----------------------------------------------------------------- | ------------------------------------------------------------------------------------- |
| 各節                                  | 第1節                                                                      | 固體助推火箭                                    | 第2節                                                               | 衛星酬載艙                                                                                 |
| 全長                                  | 38.2[公尺](../Page/公尺.md "wikilink")                                       | 15.2[公尺](../Page/公尺.md "wikilink")        | 10.7[公尺](../Page/公尺.md "wikilink")                                | 15.0[公尺](../Page/公尺.md "wikilink")（5S-H型）16.0[公尺](../Page/公尺.md "wikilink")（4/4D-LC型） |
| 直徑                                  | 5.2[公尺](../Page/公尺.md "wikilink")                                        | 2.5[公尺](../Page/公尺.md "wikilink")         | 4.0[公尺](../Page/公尺.md "wikilink")                                 | 5.1[公尺](../Page/公尺.md "wikilink")（5S-H型）4.0[公尺](../Page/公尺.md "wikilink")（4/4D-LC型）   |
| 各段質量                                | 202[噸](../Page/噸.md "wikilink")（包括分離段）                                   | 308[噸](../Page/噸.md "wikilink")（4枚）       | 20[噸](../Page/噸.md "wikilink")                                    | 3.2[噸](../Page/噸.md "wikilink")（包括分離段、酬載艙）                                            |
| 引擎                                  | LE-7A引擎                                                                  | SRB-A3                                    | LE-5B引擎                                                           | －                                                                                     |
| 推進劑重量                               | 175.8[噸](../Page/噸.md "wikilink")                                        | 262.2[噸](../Page/噸.md "wikilink")（4枚）     | 16.6[噸](../Page/噸.md "wikilink")                                  | －                                                                                     |
| 推進劑種類                               | [液態氧](../Page/液態氧.md "wikilink")／[液態氫](../Page/液態氫.md "wikilink")        | HTPB                                      | [液態氧](../Page/液態氧.md "wikilink")／[液態氫](../Page/液態氫.md "wikilink") | －                                                                                     |
| [真空中推力](../Page/真空.md "wikilink")   | 2196000[牛頓](../Page/牛頓.md "wikilink")（2[引擎加總](../Page/引擎.md "wikilink")） | 9140000[牛頓](../Page/牛頓.md "wikilink")（4枚） | 137000[牛頓](../Page/牛頓.md "wikilink")                              | －                                                                                     |
| 真空中[比推力](../Page/比推力.md "wikilink") | 440.0秒                                                                   | 281.1秒                                    | 447.0秒                                                            | －                                                                                     |
| 推進時間                                | 345秒                                                                     | 120秒                                      | 530秒                                                              | －                                                                                     |

## 設計

[Mitsubishi_LE-7A.JPG](https://zh.wikipedia.org/wiki/File:Mitsubishi_LE-7A.JPG "fig:Mitsubishi_LE-7A.JPG")
H-IIB運載火箭，第一次發射在2009年9月11日發射成功，能運載8
[噸酬載至](../Page/噸.md "wikilink")[地球同步軌道](../Page/地球同步軌道.md "wikilink")，比[H-IIA運載火箭](../Page/H-IIA運載火箭.md "wikilink")
4
到6[噸酬載至](../Page/噸.md "wikilink")[地球同步軌道為大](../Page/地球同步軌道.md "wikilink")。它將提供充足的酬載能力使[H-II運載飛船到達](../Page/H-II運載飛船.md "wikilink")[低地軌道](../Page/低地軌道.md "wikilink")（16,500
[公斤](../Page/公斤.md "wikilink")）。H-IIB運載火箭是早期的[H-IIA運載火箭的衍生型](../Page/H-IIA運載火箭.md "wikilink")。H-IIB
將有二個LE-7A引擎，代替[H-IIA運載火箭](../Page/H-IIA運載火箭.md "wikilink")
的一個，和四枚主要助推火箭（SRB-A），代替[H-IIA運載火箭](../Page/H-IIA運載火箭.md "wikilink")
的二枚。這個新發射器的研發自2004
年以來，預算大約200億[日元](../Page/日元.md "wikilink")（$1.8億[美元](../Page/美元.md "wikilink")；1.5億[歐元](../Page/歐元.md "wikilink")）。在H-IIB運載火箭
以後，宇宙航空研究開發機構欲開發可載人的H-IIC運載火箭 - 有時並提議H-IIX運載火箭，是為了在2025 年以前到達發射載人太空船的宗旨。

H2B的LEO發射能力是19噸，H2B以傾角51.6度發射HTV到近地[橢圓軌道的發射能力是](../Page/橢圓軌道.md "wikilink")16.5噸。

## 发射紀錄

<table>
<thead>
<tr class="header">
<th><p>班次 #</p></th>
<th><p>型號</p></th>
<th><p>圖片</p></th>
<th><p>發射日期</p></th>
<th><p>發射地點</p></th>
<th><p>酬載衛星</p></th>
<th><p>結果</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>F1</p></td>
<td><p>H-IIB</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:H-IIB_TF1_launching_HTV_Demo.jpg" title="fig:H-IIB_TF1_launching_HTV_Demo.jpg">H-IIB_TF1_launching_HTV_Demo.jpg</a></p></td>
<td><p>2009年9月10日 (17:01 UTC)</p></td>
<td><p><a href="../Page/吉信發射場.md" title="wikilink">LA-Y</a>, <a href="../Page/種子島.md" title="wikilink">種子島</a></p></td>
<td><p><a href="../Page/H-II傳送載具-1.md" title="wikilink">H-II傳送載具-1</a></p></td>
<td></td>
<td><p>H-IIB運載火箭首次發射</p></td>
</tr>
<tr class="even">
<td><p>F2</p></td>
<td><p>H-IIB</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:H-IIB_F2_launching_HTV2.jpg" title="fig:H-IIB_F2_launching_HTV2.jpg">H-IIB_F2_launching_HTV2.jpg</a></p></td>
<td><p>2011年1月22日 (05:37 UTC)</p></td>
<td><p><a href="../Page/吉信發射場.md" title="wikilink">LA-Y</a>, <a href="../Page/種子島.md" title="wikilink">種子島</a></p></td>
<td><p><a href="../Page/H-II傳送載具-2.md" title="wikilink">H-II傳送載具-2</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>F3</p></td>
<td><p>H-IIB</p></td>
<td><p><a href="../Page/file:H-IIB_F3_launching_HTV3.jpg.md" title="wikilink">100px</a></p></td>
<td><p>2012年7月21日 (02:06 UTC)</p></td>
<td><p><a href="../Page/吉信發射場.md" title="wikilink">LA-Y</a>, <a href="../Page/種子島.md" title="wikilink">種子島</a></p></td>
<td><p><a href="../Page/H-II傳送載具-3.md" title="wikilink">H-II傳送載具-3</a><br />
 <a href="../Page/Raiko.md" title="wikilink">Raiko</a><br />
 <a href="../Page/We-Wish.md" title="wikilink">We Wish</a><br />
 <a href="../Page/Niwaka.md" title="wikilink">Niwaka</a><br />
<br />
 <a href="../Page/F-1_(satellite).md" title="wikilink">F-1</a></p></td>
<td></td>
<td><p><a href="../Page/CubeSat.md" title="wikilink">CubeSats</a> carried aboard HTV,on 4th October, 2012 deployed from the ISS</p></td>
</tr>
<tr class="even">
<td><p>F4</p></td>
<td><p>H-IIB</p></td>
<td></td>
<td><p>2013年8月4日 (19:48 UTC)</p></td>
<td><p><a href="../Page/吉信發射場.md" title="wikilink">LA-Y</a>, <a href="../Page/種子島.md" title="wikilink">種子島</a></p></td>
<td><p><a href="../Page/Kounotori_4.md" title="wikilink">H-II傳送載具-4</a><br />
  <a href="../Page/Pico_Dragon.md" title="wikilink">Pico Dragon</a><br />
 <a href="../Page/Ardusat-1.md" title="wikilink">Ardusat-1</a><br />
 <a href="../Page/Ardusat-X.md" title="wikilink">Ardusat-X</a><br />
</p></td>
<td></td>
<td><p><a href="../Page/CubeSat.md" title="wikilink">CubeSats</a> carried aboard HTV for deployment from the ISS</p></td>
</tr>
<tr class="odd">
<td><p>F5</p></td>
<td><p>H-IIB</p></td>
<td></td>
<td><p>2015年8月19日<br />
(11:50:49 UTC)</p></td>
<td><p><a href="../Page/吉信發射場.md" title="wikilink">LA-Y2</a>, <a href="../Page/種子島.md" title="wikilink">種子島</a></p></td>
<td><p><a href="../Page/Kounotori_5.md" title="wikilink">H-II傳送載具-5</a><br />
 <a href="../Page/SERPENS.md" title="wikilink">SERPENS</a><br />
 <a href="../Page/S-CUBE.md" title="wikilink">S-CUBE</a><br />
 <a href="../Page/Flock-2b.md" title="wikilink">Flock-2b</a> x 14<br />
 <a href="../Page/GOMX-3.md" title="wikilink">GOMX-3</a><br />
 <a href="../Page/AAUSAT5.md" title="wikilink">AAUSAT5</a></p></td>
<td></td>
<td><p><a href="../Page/CubeSat.md" title="wikilink">CubeSats</a> carried aboard HTV for deployment from the ISS</p></td>
</tr>
<tr class="even">
<td><p>F6</p></td>
<td><p>H-IIB</p></td>
<td></td>
<td><p>2016年12月9日<br />
(13:26:47 UTC)</p></td>
<td><p><a href="../Page/吉信發射場.md" title="wikilink">LA-Y2</a>, <a href="../Page/種子島.md" title="wikilink">種子島</a></p></td>
<td><p><a href="../Page/Kounotori_6.md" title="wikilink">H-II傳送載具-6</a><br />
 AOBA-Velox III<br />
   TuPOD<br />
 EGG<br />
 ITF-2<br />
 STARS-C<br />
 FREEDOM<br />
 WASEDA-SAT3</p></td>
<td></td>
<td><p><a href="../Page/CubeSat.md" title="wikilink">CubeSats</a> carried aboard HTV for deployment from the ISS</p></td>
</tr>
</tbody>
</table>

## 现役几种主力大型火箭性能一览

## 參見

  - [H-IIA運載火箭](../Page/H-IIA運載火箭.md "wikilink")

[Category:运载火箭](../Category/运载火箭.md "wikilink")
[Category:化學火箭](../Category/化學火箭.md "wikilink")
[Category:日本火箭](../Category/日本火箭.md "wikilink")
[Category:一次性火箭](../Category/一次性火箭.md "wikilink")
[Category:三菱重工業](../Category/三菱重工業.md "wikilink")