[缩略图](https://zh.wikipedia.org/wiki/File:Kumata_Crossing01.JPG "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Headquarter_of_Maikon_no_Kohara_Co.,Ltd..JPG "fig:缩略图")
**東住吉區**（）是[大阪市的](../Page/大阪市.md "wikilink")24個區之一，位於大阪市的東南部，南以[大和川與](../Page/大和川.md "wikilink")[松原市接鄰](../Page/松原市_\(日本\).md "wikilink")，但在大和川南岸的周邊及過去屬於其[參道的區域](../Page/參道.md "wikilink")，由於神社歷史的緣故，現屬於大阪市東住吉區，形成在松原市轄區內有一台細長的帶狀區域屬於大阪市。

## 历史

東住吉郡的轄區在過去屬於[攝津國](../Page/攝津國.md "wikilink")[住吉郡及](../Page/住吉郡.md "wikilink")[河內國](../Page/河內國.md "wikilink")[丹北郡](../Page/丹北郡.md "wikilink")，在1889年實施町村制時，分屬、南百濟村、田邊村、四個行政區劃\[1\]；1925年北百濟村、南百濟村、田邊村被併入大阪市，連同當時其他屬於舊[住吉的區域一同被劃入](../Page/住吉.md "wikilink")[住吉郡](../Page/住吉郡.md "wikilink")，1943年住吉郡被拆分成為西北部、西南部、東部三個新的區，位於東部的區域即被命名為「**東住吉郡**」。

1955年[中河內郡的加美村](../Page/中河內郡.md "wikilink")、長吉村、瓜破村、矢田村被併入大阪市，並劃入東住吉郡，此時期的東住吉郡範圍還包含現在的[平野區](../Page/平野區.md "wikilink")；直到1974年將位於當時東住吉郡東部的平野、喜連、長吉、瓜破、加美地區分出設立為[平野区](../Page/平野区.md "wikilink")，才形成現在的東住吉郡範圍。

## 交通

### 鐵路

  - [西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")

<!-- end list -->

  - [關西本線](../Page/關西本線.md "wikilink")（
    [大和路線](../Page/大和路線.md "wikilink")）：

  - [阪和線](../Page/阪和線.md "wikilink")： -  -
    （通過鄰接與[阿倍野區的邊界附近](../Page/阿倍野區.md "wikilink")，登記上沒有東住吉區的車站）

<!-- end list -->

  -
    ※南田邊站、鶴丘站在地上線時代屬於東住吉區，阪和線高架化（2006年完成）後站舍與軌道向西（阿倍野區側）移動後，這兩個車站的住所也變成阿倍野區。地上線時代的阪和線軌道西端是與阿倍野區的邊界。

  - [日本貨物鐵道](../Page/日本貨物鐵道.md "wikilink")

<!-- end list -->

  - 關西本線貨物支線

<!-- end list -->

  - [近畿日本鐵道](../Page/近畿日本鐵道.md "wikilink")

<!-- end list -->

  - [南大阪線](../Page/南大阪線.md "wikilink")： -  -  -

<!-- end list -->

  - [大阪市高速電氣軌道](../Page/大阪市高速電氣軌道.md "wikilink")（大阪地下鐵）

<!-- end list -->

  - [1M.png](https://zh.wikipedia.org/wiki/File:1M.png "fig:1M.png")
    [御堂筋線](../Page/御堂筋線.md "wikilink")：[長居站](../Page/長居站.md "wikilink")（此站是區內距離最近的車站，東住吉區域只限1號出口、3號出口、月台的一部分。車站所在地是[住吉區](../Page/住吉區.md "wikilink")）
  - [2T-2.png](https://zh.wikipedia.org/wiki/File:2T-2.png "fig:2T-2.png")
    [谷町線](../Page/谷町線_\(大阪市高速電氣軌道\).md "wikilink")：[田邊站](../Page/田邊站.md "wikilink")
    - [駒川中野站](../Page/駒川中野站.md "wikilink")

※
上述的都是[高架站和](../Page/高架站.md "wikilink")[地下站](../Page/地下站.md "wikilink")，區內沒有[平交道](../Page/平交道.md "wikilink")。

  -   - [谷町線開業前有](../Page/谷町線_\(大阪市高速電氣軌道\).md "wikilink")（廢線）營業。

## 本地出身之名人

  - [高村薰](../Page/高村薰.md "wikilink")：作家
  - [筒井康隆](../Page/筒井康隆.md "wikilink")：說家、科幻作家和演員
  - [岩尾望](../Page/岩尾望.md "wikilink")：搞笑藝人
  - [影山浩宣](../Page/影山浩宣.md "wikilink")：歌手
  - [小林劍道](../Page/小林劍道.md "wikilink")：搞笑藝人、演員
  - [高崎晃](../Page/高崎晃.md "wikilink")：[響度樂團的吉他手](../Page/響度樂團.md "wikilink")
  - [谷村新司](../Page/谷村新司.md "wikilink")：音樂家及歌手

## 參考資料

## 外部連結

  -
  -
  -
<!-- end list -->

1.