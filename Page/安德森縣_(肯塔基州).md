**安德森县**（**Anderson County,
Kentucky**）是[美國](../Page/美國.md "wikilink")[肯塔基州的一個縣](../Page/肯塔基州.md "wikilink")，位於[外藍草地區](../Page/外藍草地區.md "wikilink")。面積529平方公里。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，共有人口19,111人。縣治[劳伦斯堡](../Page/劳伦斯堡_\(肯塔基州\).md "wikilink")
(Lawrenceburg)。

成立於1827年1月16日。縣名紀念[聯邦眾議員](../Page/美國眾議院.md "wikilink")[小R·C·安德森](../Page/小R·C·安德森.md "wikilink")
(Richard Clough Anderson, Jr.)。

[A](../Category/肯塔基州行政区划.md "wikilink")