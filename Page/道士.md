**道士**一词，在古代可以指的是一般学道、务道之士，是學[道之人的泛稱](../Page/道.md "wikilink")，並非完全指向[道教的信徒](../Page/道教.md "wikilink")。在宋朝以後，道士逐漸成為[道教](../Page/道教.md "wikilink")[神職人員的專有名詞](../Page/神職人員.md "wikilink")。初步统计，截至2011年，中国大陸有道士近10万人\[1\]。

## 起源

道家思想最早可追溯到[老子](../Page/老子.md "wikilink")、[莊子](../Page/莊子.md "wikilink")\[2\]。

《[春秋繁露](../Page/春秋繁露.md "wikilink")‧循天之道》有「古之道士」一詞，但不是指道教徒。真正的道士之稱可能始於漢代，[張道陵創立](../Page/張道陵.md "wikilink")「五斗米道」，奉[老子為始祖](../Page/老子.md "wikilink")。東漢[漢靈帝時](../Page/漢靈帝.md "wikilink")，[张角創立](../Page/张角.md "wikilink")[太平道之後](../Page/太平道.md "wikilink")，始有道教之名。

晉代[葛洪](../Page/葛洪.md "wikilink")《抱朴子·內篇‧明本》亦有“惟道家之教，使人精神專一，動合無形。”《[太霄琅書經](../Page/太霄琅書經.md "wikilink")》稱：“人行大道，號為道士。……身心順理，唯道是從，從道為事，故稱道士。道士一词是[寇谦时才开始广泛使用](../Page/寇谦.md "wikilink")，不过早期道士专指出家求真理之人，不指在家修道士。

但一直到唐代，道士與方士的稱呼仍然可以混用。

## 歷史

[道教符.jpg](https://zh.wikipedia.org/wiki/File:道教符.jpg "fig:道教符.jpg")
[南北朝时代又多以道人稱佛教](../Page/南北朝.md "wikilink")[沙門](../Page/沙門.md "wikilink")。“[真人](../Page/真人.md "wikilink")”又是道士的一種尊稱。南北朝的道士尊称[张陵为](../Page/张陵.md "wikilink")“[正一真人](../Page/正一真人.md "wikilink")”，[许谧为](../Page/许谧.md "wikilink")“上清真人”。[東晉](../Page/東晉.md "wikilink")《太極真人敷靈寶齋戒威儀諸經要訣》云：“夫先生者，道士也。於此學[仙](../Page/仙.md "wikilink")，道成曰真人；體道大法，謂之真人矣。”[陸修靜稱道士以道德為父](../Page/陸修靜.md "wikilink")，神明為母，清靜為師，太和為友\[3\]。

《太上洞玄灵宝出家因缘经》真正定義了道士的職務：“所以名为道士者，谓行住坐卧，举心运意，唯道为务；持斋礼拜，奉诫诵经，烧香散花，然灯忏悔，布施愿念，讲说大乘，教导众生，发大道心，造诸功德，普为一切，后己先人，不杂尘劳，唯行道业”\[4\]。

到了[唐朝道人又專指道士](../Page/唐朝.md "wikilink")。即使是到了唐朝，道士、道人、[方士等名詞還是經常混用](../Page/方士.md "wikilink")\[5\]。

《[唐六典](../Page/唐六典.md "wikilink")》卷四云：“道士修行有三号：其一曰法师，其二曰威仪师，其三曰律师。其德高思精，谓之炼师。”

## 成型

[Taopriest.jpg](https://zh.wikipedia.org/wiki/File:Taopriest.jpg "fig:Taopriest.jpg")

  - 男性的道士称为“**-{乾道}-**”，也称**道人**、**羽士**、**羽客**、**黄冠**等，
  - 資歷深的道士又會被尊稱為[道長](../Page/道長.md "wikilink")。
  - **女道士**稱為**坤道**，又稱**女冠**，俗稱**道姑**。
  - 古代[佛門弟子亦可稱道士](../Page/佛門.md "wikilink")。

## 分類

按地域，道士又可分為茅山道士、羅浮道士等。《三洞奉道科誡》將道士分為六階:“一、天真道士，二、神仙道士，三、山居道士，四、出家道士，五、在家道士，六、祭酒道士。”，道士中被認為道功最高者，稱為“高功”。

閔智亭道長《道教雜講隨筆》述：“全真高功一定須得到上一代高功的「撥職」才是合格高功。”北魏人[寇謙之改革](../Page/寇謙之.md "wikilink")[天師道](../Page/天師道.md "wikilink")，自稱天師，自此道教徒可在家立壇。

## 特徵

道士的服装称为[道袍](../Page/道袍.md "wikilink")，是一种[中国古代的](../Page/中国.md "wikilink")[汉服](../Page/汉服.md "wikilink")，还要戴古代的[冠巾](../Page/冠巾.md "wikilink")，脚上穿的鞋叫做[云履](../Page/云履.md "wikilink")(雲鞋)。道士进行宗教活动的场所称为[宫](../Page/宫.md "wikilink")、[观](../Page/观.md "wikilink")、[道院或者](../Page/道院.md "wikilink")[庙](../Page/庙.md "wikilink")，他们大多住在这些宫观里面。当然现在內襯多半是[满清式](../Page/满清.md "wikilink")[马褂的道服](../Page/马褂.md "wikilink")。

现代道教主要分為[全真派與](../Page/全真派.md "wikilink")[正一派兩派](../Page/正一派.md "wikilink")，一個傳統的[全真派道士](../Page/全真派.md "wikilink")，需受戒[出家](../Page/出家.md "wikilink")，居住於宮觀之內，大部分[素食](../Page/素食.md "wikilink")，不許娶妻，類似[佛教](../Page/佛教.md "wikilink")[僧侶](../Page/僧侶.md "wikilink")。道人時常[留鬚蓄髮](../Page/鬍鬚.md "wikilink")，头顶挽[髻](../Page/髻.md "wikilink")，强调[打坐](../Page/打坐.md "wikilink")、[冥想](../Page/冥想.md "wikilink")。另外還有[全真南派](../Page/南宗_\(道教\).md "wikilink")，可以娶妻生子。

[正一派的道士可以](../Page/正一派.md "wikilink")[结婚](../Page/结婚.md "wikilink")，可以居住家中，也称「火居道士」，不[齋戒時可喫](../Page/齋戒.md "wikilink")[荤食肉](../Page/葷菜#道家五葷.md "wikilink")（除了[雁](../Page/雁.md "wikilink")、[狗](../Page/狗.md "wikilink")、[龜](../Page/龜.md "wikilink")、[鱉等肉](../Page/鱉.md "wikilink")），很少人出家，髮型大多也是一般的現代髮型。集會時方赴宮觀，主要从事道教仪式活动，尤其强调[畫符](../Page/符箓.md "wikilink")、[念咒](../Page/念咒.md "wikilink")，驅使鬼神。现代正一弟子须经过授箓才成为有资格的道士，
[缩略图](https://zh.wikipedia.org/wiki/File:Taoist_ceremony_at_Xiao_ancestral_temple_in_Chaoyang,_Shantou,_Guangdong_\(daoshi\)_\(1\).jpg "fig:缩略图")

## 参见

  - [道長](../Page/道長.md "wikilink")
  - [道场](../Page/道场.md "wikilink")
  - [炼丹](../Page/炼丹.md "wikilink")
  - [茅山道士](../Page/茅山道士.md "wikilink")
  - [居士](../Page/居士.md "wikilink")

## 参考文献

## 延伸閱讀

  - 吉岡義豐：〈[道士的道院生活](http://www.his.ncku.edu.tw/chinese/uploadeds/1-08.pdf)〉。

{{-}}

[Category:道教人物](../Category/道教人物.md "wikilink")
[道士](../Category/道士.md "wikilink")
[Category:聖職者](../Category/聖職者.md "wikilink")

1.
2.  《[隋書](../Page/隋書.md "wikilink")·經籍志》云：“[元始天尊開劫人](../Page/元始天尊.md "wikilink")，經四十一億萬載，所度皆諸天，仙乃命又真皇人，傳授世上”
    ；[張君房輯](../Page/張君房.md "wikilink")《雲笈七籤》說：“道教起源於無始以前的天真皇人，於[峨嵋山授](../Page/峨嵋山.md "wikilink")《靈寶經》於軒轅[黃帝](../Page/黃帝.md "wikilink")，又授帝嚳於牧德之台，師資相承，蟬聯不絕”，而“今人學多浮淺，唯誦道德，不識真經即謂道教起自莊周，始乎柱下，則謬言矣”。[葛洪](../Page/葛洪.md "wikilink")《神仙传》更谓：“自伏羲至三代，显名道士，世世有之，其老子者，盖得道尤精者也。”
3.  《初學記》：「陸法師曰：凡道士，道德為父，神明為母，清靜為師，太和為友。大戒三百，以度未兆之禍；威儀二千，以興自然之神。」
4.  《道藏》第6册139页
5.  《南齊書‧顧歡傳》載“道士與道人戰儒墨﹐道人與道士獄是非”。