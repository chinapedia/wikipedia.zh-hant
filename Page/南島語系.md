**南島語系**（）是主要由[南岛民族所使用的语言](../Page/南岛民族.md "wikilink")，是世界現今唯一主要分布在島嶼上的一個[語系](../Page/語系.md "wikilink")，包括約1300種語言。其分布主要位于南太平洋群岛，包括[臺灣](../Page/臺灣.md "wikilink")、[海南](../Page/海南.md "wikilink")、[越南南部](../Page/越南.md "wikilink")、[菲律宾](../Page/菲律宾.md "wikilink")、[马来群岛](../Page/马来群岛.md "wikilink")，東達[南美洲西方的](../Page/南美洲.md "wikilink")[復活節島](../Page/復活節島.md "wikilink")，西到東[非洲外海的](../Page/非洲.md "wikilink")[馬達加斯加島](../Page/馬達加斯加島.md "wikilink")，南抵[紐西蘭](../Page/紐西蘭.md "wikilink")。[臺灣是南島語系內部分化的源頭](../Page/臺灣.md "wikilink")，也是目前南島語系最北部的地區\[1\]，居住在[新北市](../Page/新北市.md "wikilink")[烏來區的](../Page/烏來區.md "wikilink")[泰雅族聚落](../Page/泰雅族.md "wikilink")，目前是全世界南島語系居住地中，是人口分布上地理最北端的聚落；東西的延伸距離，超過地球圓周的一半，總人口數大約兩億五千萬之多，絕大多數居住在東南亞的菲律賓\[2\]\[3\]、印尼、馬來西亞，至於[新畿內亞以東的太平洋島嶼有一百多萬人](../Page/新畿內亞.md "wikilink")。

## 主要分佈區域

所謂的「南島」（Austronesia），是1899年由學者Wilhelm
Schmidt自[拉丁文字的字根](../Page/拉丁文.md "wikilink")「auster」與[希臘文](../Page/希臘文.md "wikilink")「nêsos」所合組而成\[4\]，前者為南風之意，後者意指島，所以日本語將「Austronesia」翻譯成「南島」。中文在近代沿用了這個日本語詞彙。

使用南島語系的區域包括[臺灣](../Page/臺灣.md "wikilink")、[海南](../Page/海南.md "wikilink")、[菲律賓](../Page/菲律賓.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[馬達加斯加](../Page/馬達加斯加.md "wikilink")、[新畿內亞](../Page/新畿內亞.md "wikilink")、[紐西蘭](../Page/紐西蘭.md "wikilink")、[夏威夷](../Page/夏威夷.md "wikilink")、[密克羅尼西亞](../Page/密克羅尼西亞.md "wikilink")、[美拉尼西亞](../Page/美拉尼西亞.md "wikilink")、[玻里尼西亞等各地島嶼的語言](../Page/玻里尼西亞.md "wikilink")，外加[馬來半島上的](../Page/馬來半島.md "wikilink")[馬來語](../Page/馬來語.md "wikilink")、[中南半島上](../Page/中南半島.md "wikilink")[越南與](../Page/越南.md "wikilink")[高棉的](../Page/高棉.md "wikilink")[佔語和](../Page/佔語.md "wikilink")[泰國的](../Page/泰國.md "wikilink")[莫肯語](../Page/莫肯語.md "wikilink")（）及[莫克倫語](../Page/莫克倫語.md "wikilink")（）\[5\]。北起臺灣，南抵紐西蘭，西至馬達加斯加，東至智利復活節島。

南島語系分佈的區域很廣，語言的數目也頗多，根據《[民族語](../Page/民族語.md "wikilink")》這個世界語言資料庫的資料，其總數有1262種之多（Ethnologue:
Languages of the World 2004）。
由於移民及近代帝國主義殖民，在劃分為南島語系的區域內，部分的島上也有使用非南島語族的語言。以臺灣為例，除了有隸屬於南島語族的[原住民族族語以外](../Page/台灣原住民.md "wikilink")，由於17與18世紀時，來自大明帝國、大清帝國的移民而同時存在著不同的漢語，包括了[閩南語](../Page/閩南語.md "wikilink")（[臺灣話](../Page/臺灣話.md "wikilink")）和[客家話](../Page/客家話.md "wikilink")（[臺灣客家語](../Page/臺灣客家語.md "wikilink")）。台灣於19世紀末至20世紀中期的[台灣日治時期則以](../Page/台灣日治時期.md "wikilink")[日語為官方語言](../Page/日語.md "wikilink")；[第二次世界大戰後時期](../Page/臺灣戰後時期.md "wikilink")，[中華民國政府統一使用](../Page/中華民國.md "wikilink")[現代標準漢語](../Page/現代標準漢語.md "wikilink")（[國語](../Page/中華民國國語.md "wikilink")）。\[6\]

其他如菲律賓相繼受到西班牙與美國殖民、夏威夷受到美國殖民、紐西蘭受英國殖民、印尼受到荷蘭殖民、馬來西亞受到英國殖民、越南與柬埔寨受到法國殖民等，其殖民母國的語言在其現今語言系統內仍造成不同影響。

## 分類

[Austronesian_languages.PNG](https://zh.wikipedia.org/wiki/File:Austronesian_languages.PNG "fig:Austronesian_languages.PNG")被視為南島語系的主要源頭\]\]

根據《民族語》的分類，**南島語系**可以再分為兩大語族，第一是「**[台灣南島語](../Page/台灣南島語.md "wikilink")**」，第二為「**[馬來-玻里尼西亞語](../Page/馬來-玻里尼西亞語.md "wikilink")**」。前者有23種語言，可以再分成[泰雅語群](../Page/泰雅語群.md "wikilink")、[排灣語群](../Page/排灣語群.md "wikilink")、和[鄒語群三大系統](../Page/鄒語群.md "wikilink")。後者則有1239種語言，可以再分成「[中-東部馬來-玻里尼西亞語族](../Page/中-東部馬來-玻里尼西亞語族.md "wikilink")」（Central-Eastern
Malayo-Polynesian）、「[西部馬來-玻里尼西亞語族](../Page/西部馬來-玻里尼西亞語族.md "wikilink")」、以及兩種尚無法歸類的語言\[7\]。

關於「南島語」（Austronesian
languages）和「[馬來-玻里尼西亞語](../Page/馬來-玻里尼西亞語.md "wikilink")」的關係，還有其他的說法。其說法是「南島語」有十種次語系，其中有九種是在台灣，最後一種則是「馬來-玻里尼西亞語」\[8\]。也就是說，雖然其分類方式和《民族語》所列舉的並不一致，但一樣是將「馬來-玻里尼西亞語」當成是「南島語系」的一個次語系。

台灣[中央研究院](../Page/中央研究院.md "wikilink")（1996）明白陳述：「南島語系（Austronesian）又稱馬來-玻里尼西亞語系（Malay-Polynesian），是世界上唯一主要分布在島嶼上的一個大語系」。中央研究院平埔文化資訊網也說，「生活在台灣的土著民族，即平埔族和高山族，他們所使用的語言，不僅自己相互之間有很深的關係，而且跟太平洋、印度洋島嶼上的許多種語言都有親屬關係，形成學術界所稱的「南島語系」（Austronesian）
或 （Malayopolynesian）」\[9\]。

某些語言學者相信[泰語可能也可以算是擴大定義之](../Page/泰語.md "wikilink")「南島語系」的一部份，雖然多數學者似乎仍舊將其歸類為「[壯侗語族](../Page/壯侗語族.md "wikilink")」。學者[白保羅認為](../Page/白保羅.md "wikilink")「南島語系」和「[南亞語系](../Page/南亞語系.md "wikilink")」以及「壯侗語族」可能有血緣關係，正研究[南方大語系](../Page/南方大語系.md "wikilink")，由此成為語言學家的另一項研究專題。

## 特征

南岛语系各语言都是[黏著语](../Page/黏著语.md "wikilink")，語音上辅音有[清辅音和](../Page/清音.md "wikilink")[浊辅音](../Page/浊音.md "wikilink")；語法上有[格](../Page/格_\(语法\).md "wikilink")，但沒有[性的区别](../Page/性_\(语法\).md "wikilink")；广泛利用[词缀和詞幹](../Page/词缀.md "wikilink")[元音音变来表达](../Page/元音.md "wikilink")[语法意义](../Page/语法.md "wikilink")；[动词有](../Page/动词.md "wikilink")[时态](../Page/时态.md "wikilink")、[语态和](../Page/语态.md "wikilink")[语体的变化](../Page/语体.md "wikilink")，[主语和动词在变化中互相呼应](../Page/主语.md "wikilink")。南岛语系各语言的词都有[重音](../Page/重音.md "wikilink")，另外除了長期受到周圍漢語、黎語等有聲調語言影響的[回輝話以外](../Page/回輝話.md "wikilink")，其他南島諸語沒有聲調系統。

## 歷史研究

### 起源和擴散

[Polynesian_Migration.svg](https://zh.wikipedia.org/wiki/File:Polynesian_Migration.svg "fig:Polynesian_Migration.svg")是南島語系[玻里尼西亞人的原鄉](../Page/玻里尼西亞.md "wikilink")\]\]

關於南島語的起源問題，南島語系的起源，與[南島民族的起源有緊密相關](../Page/南島民族.md "wikilink")。随着分子人类学与基因组学的Y单倍型研究，[汉-南岛同源祖语得到正视](../Page/汉-南岛同源祖语.md "wikilink")。祖语的语音形式以
CVCV 、CVCVC 两种为绝大多数。这些语言里面，南岛语、尤其是台湾和菲律宾的南岛语，最为接近这个「大O超语系」祖语的原貌。

臺灣[原住民與](../Page/原住民.md "wikilink")[菲律賓](../Page/菲律賓.md "wikilink")、[印度尼西亞各島原住民皆屬於南島民族且有密切關係](../Page/印度尼西亞.md "wikilink")。這些臺灣史前移民的語言因而與上述其他地區具有共通性。比較早期的說法，包括該語言起源於[麥克羅尼西亞](../Page/麥克羅尼西亞.md "wikilink")、[華南](../Page/華南.md "wikilink")、[中南半島](../Page/中南半島.md "wikilink")、西[新畿內亞等論點](../Page/新畿內亞.md "wikilink")。\[10\]

### 與其他语系的关系

关于南岛语系和其他语系的同源关系，学者议论纷纷，目前有五个不同假设：

1.  德国传教士Wilhelm Schmidt
    1899年提出南岛语系和[南亚语系同源](../Page/南亚语系.md "wikilink")。
2.  美国学者Dahl 1970年指出南岛语系和[印欧语系同源](../Page/印欧语系.md "wikilink")。
3.  法国漢学家[沙加尔](../Page/沙加尔.md "wikilink")1990年提出南岛语系和[汉藏语系同源](../Page/汉藏语系.md "wikilink")。
4.  日本一些學者認為南島語系與[阿爾泰語系構成一種混合語言就是](../Page/阿爾泰語系.md "wikilink")[日語](../Page/日語.md "wikilink")。
5.  美国人潘乃德提出泰语（[壮侗語族](../Page/壮侗語族.md "wikilink")）就是南岛语系。

## 南島語系語言分類系譜

《[民族語](../Page/民族語.md "wikilink")》基於[白樂思](../Page/白樂思.md "wikilink")（Robert
Blust）1999 年的新分類，將 1256 種南島語言，分為 10
大分支，是[民族語有列出語系中](../Page/民族語.md "wikilink")，語言數目第二多的語系。

此新分類將原來[台灣南島語言](../Page/台灣南島語言.md "wikilink")（Formosan）17 種語言及婆羅語群的 139
種語言分拆成多個新的語族，並提升至與[馬來-玻里尼西亞語族的地位相同](../Page/馬來-玻里尼西亞語族.md "wikilink")，詳見[台灣南島語言](../Page/台灣南島語言.md "wikilink")。最後面的數字是該語族或語群所包括之語言的數目\[11\]：

**南島語系**（Austronesian，1256）

  - [泰雅語群](../Page/泰雅語群.md "wikilink")（Atayalic，2）
      - [泰雅語](../Page/泰雅語.md "wikilink")（Atayal，）
      - [賽德克語](../Page/賽德克語.md "wikilink")（Seediq，)
  - 布農語（Bunun，1）
      - [布農語](../Page/布農語.md "wikilink")（Bunun，)
  - [東台灣南島語族](../Page/東台灣南島語族.md "wikilink")（East Formosan，5）
      - 中部語群（2）
          - [阿美語](../Page/阿美語.md "wikilink")（Amis，)
          - [撒奇萊雅語](../Page/撒奇萊雅語.md "wikilink")（Sakizaya，)
      - 北部語群（2）
          - [巴賽語](../Page/巴賽語.md "wikilink")（Basay，)
          - [噶瑪蘭語](../Page/噶瑪蘭語.md "wikilink")（Kbalan，)
      - 西南部語群（1）
          - [西拉雅語](../Page/西拉雅語.md "wikilink")（Siraya，)
  - [西北台灣南島語族](../Page/北台灣南島語族.md "wikilink")（Northwest Formosan，2）
      - [龜崙-巴宰語](../Page/巴宰語.md "wikilink")（Kulon-Pazeh，)
      - [賽夏語](../Page/賽夏語.md "wikilink")（Saisiyat，)
  - [排灣語群](../Page/排灣語.md "wikilink")（Paiwan，1）
      - [排灣語](../Page/排灣語.md "wikilink")（Paiwan，)

<!-- end list -->

  - 卑南語（Puyuma，1）
      - [卑南語](../Page/卑南語.md "wikilink")（Puyuma，)
  - [魯凱語群](../Page/魯凱語.md "wikilink")（Rukai，1）
      - [魯凱語](../Page/魯凱語.md "wikilink")（Rukai，)
  - [鄒語群](../Page/鄒語群.md "wikilink")（Tsouic，3）
      - [卡那卡那富語](../Page/卡那卡那富語.md "wikilink")（Kanakanabu，)
      - [沙阿魯阿語](../Page/沙阿魯阿語.md "wikilink")（Saaroa，)
      - [鄒語](../Page/鄒語.md "wikilink")（Tsou，)
  - 未分類南島語言（1）
      - [凱達格蘭語](../Page/凱達格蘭語.md "wikilink")（Ketangalan，)
  - 西部平原台灣南島語族（Western Plains，3）
      - 中西部平原台灣南島語群（Central Western Plains，2）
          - [巴布薩語](../Page/巴布薩語.md "wikilink")（Babuza，)
          - [巴布拉語](../Page/巴布拉語.md "wikilink")－[洪雅語](../Page/洪雅語.md "wikilink")（Papora-Hoanya，)
      - [邵語群](../Page/邵語.md "wikilink")（Thaw，1）
          - [邵語](../Page/邵語.md "wikilink")（Thaw，)
  - [馬來-玻里尼西亞語族](../Page/馬來-玻里尼西亞語族.md "wikilink")（Malayo-Polynesian，1237）
      - [達悟語](../Page/達悟語.md "wikilink")（Tao，)
      - 語言學家通常將「馬來-玻里尼西亞語族」分為「[婆羅-菲律賓語群](../Page/婆羅-菲律賓語群.md "wikilink")」和「[核心馬來-玻里尼西亞語群](../Page/核心馬來-玻里尼西亞語群.md "wikilink")」兩大類。請參閱相關條目。

## 南島語系語言舉隅

  - 台灣
      - [排灣語](../Page/排灣語.md "wikilink")
      - [阿美語](../Page/阿美語.md "wikilink")
      - [賽夏語](../Page/賽夏語.md "wikilink")
      - [布農語](../Page/布農語.md "wikilink")
      - [噶瑪蘭語](../Page/噶瑪蘭語.md "wikilink")
      - [賽德克語](../Page/賽德克語.md "wikilink")
      - [巴宰語](../Page/巴宰語.md "wikilink")
      - [噶哈巫語](../Page/噶哈巫語.md "wikilink")
      - [道卡斯語](../Page/道卡斯語.md "wikilink")
      - [卑南語](../Page/卑南語.md "wikilink")
      - [泰雅語](../Page/泰雅語.md "wikilink")
      - [西拉雅語](../Page/西拉雅語.md "wikilink")
      - [鄒語](../Page/鄒語.md "wikilink")
      - [達悟語](../Page/達悟語.md "wikilink")（屬北菲律賓語支[巴丹語群](../Page/巴丹語群.md "wikilink")）
  - 海南
      - [回輝語](../Page/回輝語.md "wikilink")（海南[占語](../Page/占語.md "wikilink")）
  - 菲律賓
      - [塔加洛語](../Page/塔加洛語.md "wikilink")、[菲律賓語](../Page/菲律賓語.md "wikilink")

      - [伊洛卡諾語](../Page/伊洛卡諾語.md "wikilink")

      - [希利蓋農語](../Page/希利蓋農語.md "wikilink")

      - [宿霧語](../Page/宿霧語.md "wikilink")

      - （[巴丹語群](../Page/巴丹語群.md "wikilink")）

      - （[巴丹語群](../Page/巴丹語群.md "wikilink")）

      - （[巴丹語群](../Page/巴丹語群.md "wikilink")）
  - 越南
      -
      - [占語](../Page/占語.md "wikilink")

      -
      -
      -
  - 泰國
      -
      -
  - 馬來西亞、印度尼西亞、汶萊、東帝汶
      - [馬來語](../Page/馬來語.md "wikilink")

      - [印尼語](../Page/印尼語.md "wikilink")

      - [爪哇語](../Page/爪哇語.md "wikilink")

      - [巽他語](../Page/巽他語.md "wikilink")

      - [馬都拉語](../Page/馬都拉語.md "wikilink")

      - [米南佳保語](../Page/米南佳保語.md "wikilink")

      - [巴厘语](../Page/巴厘语.md "wikilink")

      - [德頓語](../Page/德頓語.md "wikilink")

      -
  - 琉球
      - [宮古語](../Page/宮古語.md "wikilink")
      - [與那國語](../Page/與那國語.md "wikilink")
      - [八重山語](../Page/八重山語.md "wikilink")
  - 大洋洲
      - [斐濟語](../Page/斐濟語.md "wikilink")
      - [夏威夷語](../Page/夏威夷語.md "wikilink")
      - [毛利語](../Page/毛利語.md "wikilink")
      - [大溪地語](../Page/大溪地語.md "wikilink")
      - [薩摩亞語](../Page/薩摩亞語.md "wikilink")
      - [湯加語](../Page/湯加語.md "wikilink")
      - [吉里巴斯語](../Page/吉里巴斯語.md "wikilink")
      - [馬紹爾語](../Page/馬紹爾語.md "wikilink")
      - [楚克語](../Page/楚克語.md "wikilink")
      - [拉帕努伊語](../Page/拉帕努伊語.md "wikilink")
      - [希里摩圖語](../Page/希里摩圖語.md "wikilink")
  - 非洲 馬達加斯加
      - [馬拉加斯語](../Page/馬拉加斯語.md "wikilink")

## 維基百科語言列表中的南島語系語言

統計至2017年4月10日止：

Fijian 斐濟語

Marshallese 馬紹爾語

Nauruan 諾魯語

Tetum 特塔姆語、德頓語

Samoan 薩摩亞語

Tahitian 塔希提語、大溪地語

Tonga 東加語

Māori 毛利語

Malagasy 馬拉加斯語

Chamorro 查莫羅語

Hawaiian 夏威夷語

Malay 馬來語

Indonesian 印尼語、印度尼西亞語

Minangkabau 米南佳保語

Javanese 爪哇語

Sundanese 巽他語

Buginese 布吉語

Banyumasan 班尤馬山語

Acehnese 亞齊語

Banjar 班查語

Cebuano 宿霧語

Wara 瓦賴語

Tagalog 他加祿語

Ilokano 伊羅肯諾語

Kapampangan 邦板牙語

Central Bicolano 中比科爾語

Pangasinan 邦阿西楠語

Hiri Motu 希里摩圖語 （暫停使用）

## 參見

  - [臺灣原住民](../Page/臺灣原住民.md "wikilink")
  - [漢本遺址](../Page/漢本遺址.md "wikilink")
  - [南岛民族](../Page/南岛民族.md "wikilink")
  - [南方大語系](../Page/南方大語系.md "wikilink")

## 参考文献

### 引用

### 来源

  - Bellwood, Peter. 1991. The Austronesian Dispersal and the Origins of
    Languages. *Scientific American* July: 88-93.
  - Bellwood, Peter (1997). Prehistory of the Indo-Malaysian
    archipelago. Honolulu: University of Hawai'i Press. .
  - Ethnologue: Languages of the World. 2004. Language Family Trees:
    Austronesian \[online\]. Dallas, Tex.: Ethnologue: Languages of the
    World, \[cited 28 October 2004\]. Available from World Wide Web:
    [1](http://www.ethnologue.com/show_family.asp?subid=106).
  - 李壬癸，1996，臺灣平埔族的種類及其相互關係，見張炎憲、李筱峰、戴寶村編，臺灣史論文精選(上)，頁43-68。臺北：玉山社。
  - Microsoft. 2004. Austronesian languages \[online\]. np: Microsoft
    Encarta Online Encyclopedia , \[cited 28 October 2004\]. Available
    from World Wide Web:
    [2](https://web.archive.org/web/20051213212630/http://au.encarta.msn.com/encyclopedia_761553922/Austronesian_Languages.html).
  - Shutler, Richard Jr., and Jeff Marck. 1975. On the Dispersal of the
    Austronesian Horticulturalists. *Archaeology and Physical
    Anthropology in Oceania* 10: 81-113.
  - Wikipedia. 2004. Austronesian languages \[online\]. np: Wikipedia,
    the Free Encyclopedia, 23 October \[cited 28 October 2004\].
    Available from World Wide Web:
    [3](http://en.wikipedia.org/wiki/Austronesian_languages).
  - 中央研究院，1996，南島語系民族
    \[online\]。臺北：中央研究院。\[引用於2004年10月28日\]。全球資訊網網址：[4](http://www.sinica.edu.tw/~tibe/1-culture/pre/Austronesian.html)。
  - 中央研究院平埔文化資訊網，nd, a，使用南島語言的區域 \[online\]。臺北：中央研究院平埔文化資訊網。\[引用於
    2004年10月28日\]。全球資訊網網址：[5](http://www.sinica.edu.tw/~pingpu/museum/linguistics/htm1/AN_1_1_1.htm)。
  - 中央研究院平埔文化資訊網，nd, b，平埔族隸屬的大家庭：南島語族
    \[online\]。臺北：中央研究院平埔文化資訊網。\[引用於2004年10月28日\]。全球資訊網網址：[6](http://www.sinica.edu.tw/~pingpu/museum/linguistics/htm1/AN_1_1.htm)。
  - 中央研究院平埔文化資訊網，nd, c，南島民族起源地的學說 \[online\]。臺北：中央研究院平埔文化資訊網。\[引用於
    2004年10月28日\]。全球資訊網網址：[7](http://www.sinica.edu.tw/~pingpu/museum/linguistics/htm1/AN_1_2_2_1.htm)。

## 外部連結

  - [Language Family
    Trees](http://www.ethnologue.com/show_family.asp?subid=89851)
  - [Jared M.
    Diamond：臺灣給世界的禮物](http://www.tyjh.kh.edu.tw/elelim/%E7%89%A9%E7%90%86/%E9%80%A0%E8%88%B9.htm)
  - [經典雜誌：南島新世界](https://web.archive.org/web/20050415011502/http://taipei.tzuchi.org.tw/rhythms/subject/south%20island/south.htm#%C3D%A5%D8)
  - [南島語系播遷圖](http://www.nature.com/nature/journal/v405/n6790/fig_tab/4051052a0_F1.html)
  - [Ethnologue:
    Austronesian](http://www.ethnologue.com/show_family.asp?subid=89851)
  - [Austronesian
    Vocabulary](http://www.gbarto.com/languages/austronesian.html)
  - [中央研究院數位典藏資源網：南島語數位典藏](http://digiarch.sinica.edu.tw/7.html)
  - [Langues et civilisations à tradition
    orale(LACITO)/(UMR7107)/南島語語音語料](http://lacito.vjf.cnrs.fr/archivage/)

  - 臧振華：〈[再論南島語族的起源與擴散問題](https://web.archive.org/web/20161020173254/http://www.nmp.gov.tw/uploads/tiny_mce/books2/3-4.pdf)〉。

{{-}}

[Category:語系](../Category/語系.md "wikilink")
[南島語系](../Category/南島語系.md "wikilink")

1.  [紐西蘭研究 玻里尼西亞人來自臺灣](http://udn.com/NEWS/NATIONAL/NAT5/4706748.shtml)
2.  [巴丹人 也源自臺灣原住民](http://udn.com/NEWS/NATIONAL/NAT5/4706751.shtml)
3.  [臺灣原民、南島語族 同條文化臍帶](http://udn.com/NEWS/NATIONAL/NAT5/4706746.shtml)
4.  [國立臺灣大學原住民族研究中心－－南島文化](http://www.cip.ntu.edu.tw/main06.htm)
5.  中央研究院平埔文化資訊網 nd, a
6.  中央研究院平埔文化資訊網 nd, a
7.  《民族語》*Ethnologue: Languages of the World 2004*
8.  Diamond(2000)
9.
10.
11. Gordon, Raymond G., Jr.（編），(2009年)，[南島語系
    (Austronesian)](../Page/ethnologuefamily:243-16.md "wikilink")《[民族語](../Page/民族語.md "wikilink")》（Ethnologue:
    Languages of the
    World），第16版。[美國](../Page/美國.md "wikilink")[德薩斯州](../Page/德薩斯州.md "wikilink")[達拉斯](../Page/達拉斯.md "wikilink")：[SIL國際](../Page/SIL國際.md "wikilink")。線上版本：http://www.ethnologue.com/，於2009年8月19日存取。