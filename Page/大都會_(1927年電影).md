《**大都會**》（）是[德國知名](../Page/德國.md "wikilink")[電影導演](../Page/電影導演.md "wikilink")[弗里茨·朗所執導的](../Page/弗里茨·朗.md "wikilink")[表現主義](../Page/表現主義.md "wikilink")[科幻](../Page/科幻.md "wikilink")[默片](../Page/默片.md "wikilink")，也是電影史上最重要的作品之一，於1927年1月10日於[德國](../Page/德國.md "wikilink")[柏林首映](../Page/柏林.md "wikilink")。本片的製作成本達五百萬[帝國馬克](../Page/帝國馬克.md "wikilink")，是最昂貴的默片電影。2001年，被[聯合國教科文組織列為](../Page/聯合國教科文組織.md "wikilink")[世界記憶項目](../Page/世界記憶項目.md "wikilink")。

## 概要

，德國柏林首映版本長達153分鐘（菲林長度為4189米，以每秒廿四格放映）\[1\]\[2\]，後由發行商剪輯成約兩小時的版本於德國以及海外上映\[3\]。不過基於票房收益與政治審查等因素，過去於世界各地上映的版本均為刪除了四分之一內容，片長不一的剪輯版。且自[二次世界大戰後](../Page/二次世界大戰.md "wikilink")，本片的原始母片散佚世界各地難以尋齊，過去雖曾多次重映，但均非原本的完整版本。

2001年，[穆瑙基金會主導的修復版本](../Page/F·W·穆瑙.md "wikilink")，在[柏林影展面世](../Page/柏林影展.md "wikilink")\[4\]，經多年發掘及考證後，約四分之一的片段在當時被認為可能永久散佚。同年，本片被[聯合國教科文組織列為](../Page/聯合國教科文組織.md "wikilink")[世界文獻遺產](../Page/世界記憶項目.md "wikilink")。

2008年，在[阿根廷](../Page/阿根廷.md "wikilink")[布宜諾斯艾利斯的電影博物館中](../Page/布宜諾斯艾利斯.md "wikilink")，發現了本片的16米釐版拷貝，並找到其中約23分鐘失落片段畫面。影片經最新數位技術修復，搭配現場演奏原始配樂的147分鐘（長度為3945米）復元版本，於2010年[柏林影展中重新上映](../Page/柏林影展.md "wikilink")\[5\]，此版本目前已經推出[藍光光碟及DVD](../Page/藍光光碟.md "wikilink")。

## 劇情簡介

故事设置在2026年，电影完成的100年后，人类被分为两个阶层，生活在两个截然不同的世界——权贵和富人都住在梦幻般的富丽大厦，每天过着享乐的生活；而贫穷的工薪阶层则长期被困在幽暗的地下城市，与冰冷的机器相伴，过着劳碌辛苦的人生。

大都会统治者菲達遜（*Fredersen*）的儿子法迪（*Freder*）有一次看見地下城的漂亮女子瑪利亞（*Maria*）时，令他著迷，之後尾隨她去看地下城，目睹一次致命工業意外，看到工作者们干活苦況，巨变便开始来临。

菲達遜發現兒子有異，擔心工人發難，找[科學家洛宏](../Page/科學家.md "wikilink")（*Rotwang*）幫忙。瑪利亞在地下城的[教堂有很大的影響力](../Page/教堂.md "wikilink")，菲達遜要求洛宏製造貌似瑪利亞的[機械人](../Page/機械人.md "wikilink")，用它來煽惑工人[暴動](../Page/暴動.md "wikilink")，便可藉口鎮壓。原來洛宏的舊情人凱爾（*Hel*）是法迪的母親，他一早已做了個凱爾樣貌的機械人，可是洛宏不懷好意，意圖摧毀菲達遜的整個都會作報復。他捉了瑪利亞，把機械人換成瑪利亞的樣子。

法迪知道科學家的詭計，欲充當調停者，可是機械人挑起暴動，工人搗毀機器，導致洪水泛濫。洪水把地下城淹浸機器失靈，於是法迪與瑪利亞千辛萬苦把地下城的小孩救出來。與此同時工人以為他們的孩子困在地下城被淹，憤怒的工人把機械人捉往當作[女巫燒死](../Page/女巫.md "wikilink")。這時科學家洛宏發了瘋，追着真正的瑪利亞，洛宏與法迪打起上來，洛宏從屋頂上跌斃了。最後，管工帶領工人到教堂，瑪利亞促調停者法迪帶着管工與城主菲達遜一起握手和解，代表智慧與勞動要用心結合起來。

## 主要製作人員

  - 導演：[弗烈茲·朗](../Page/弗烈茲·朗.md "wikilink")（Fritz Lang）
  - 原作、編劇：蒂亞·馮·哈伯（Thea von Harbou）
  - 攝影：卡爾·佛洛因特（Karl Freund）、鈞特·李陶（Günther Rittau）
  - 特殊攝影：尤根·雪夫坦（Eugen Schüfftan）
  - 美術：奧圖·芬特（Otto Hunte）、艾瑞克·凱特胡（Eric Ketelhut）、卡爾·佛布雷西（Karl Vollbrecht）

## 評價

本片乃[默片末期](../Page/默片.md "wikilink")[科幻片的经典之作](../Page/科幻片.md "wikilink")，艺术价值非常之高，至今依舊極受推崇。片中犀利的思想性（以[马克思主义为起点](../Page/马克思主义.md "wikilink")，以[基督教精神为终点](../Page/基督教.md "wikilink")）和壮观的[表现主义布景设计](../Page/表现主义.md "wikilink")（基于导演对[纽约的印象](../Page/纽约.md "wikilink")）及特效，还有用人体组成的[几何图形](../Page/几何图形.md "wikilink")，使得影片成为[德国电影黄金时代的代表作](../Page/德国电影.md "wikilink")。

## 影響

本片可謂是[科幻电影的類型鼻祖](../Page/科幻电影.md "wikilink")，包括《[星際大战](../Page/星際大战.md "wikilink")》等著名现代科幻影片，都有许多场景借鉴本片的创意。2001年日本动画电影《[大都会](../Page/大都會_\(2001年電影\).md "wikilink")》靈感也源於本片。

## 幕后花絮

  - [希特勒非常喜欢本片](../Page/希特勒.md "wikilink")，多年后试图说服导演为[纳粹效劳](../Page/纳粹.md "wikilink")，但遭到拒绝。導演弗烈茲·朗本人具[猶太](../Page/猶太.md "wikilink")[血統](../Page/血統.md "wikilink")，而他當時擔任本片編劇的妻子蒂亞·馮·哈伯，則因後來政治立場傾向納粹，而造成兩人離異。
  - 本片初映时遭到广泛排斥，其中一个原因是[大萧条时期的德国最大的问题不是工人累死累活](../Page/大萧条.md "wikilink")，而是找不到工作。

## 註解

<div class="references-small">

<references />

</div>

## 外部連結

  -
  - [Metropolis: A Film Far Ahead of its
    Time](http://www.brokenprojector.com/wordpress/?p=56)

  - [2010年復元版官網](http://www.kino.com/metropolis/)

  - [Digital restoration at ALPHA-OMEGA - About restoring
    'Metropolis'](https://web.archive.org/web/20120118131605/http://www.alpha-omega.de/doku.php?id=en:showroom:metropolis)

[Category:1927年電影](../Category/1927年電影.md "wikilink")
[Category:德國電影作品](../Category/德國電影作品.md "wikilink")
[Category:德國科幻片](../Category/德國科幻片.md "wikilink")
[Category:德语电影](../Category/德语电影.md "wikilink")
[Category:特摄电影](../Category/特摄电影.md "wikilink")
[Category:弗里茨·朗电影](../Category/弗里茨·朗电影.md "wikilink")
[Category:德国默片](../Category/德国默片.md "wikilink")
[Category:黑白電影](../Category/黑白電影.md "wikilink")
[Category:德国表现主义电影](../Category/德国表现主义电影.md "wikilink")
[Category:反烏托邦電影](../Category/反烏托邦電影.md "wikilink")
[Category:科幻小說改編電影](../Category/科幻小說改編電影.md "wikilink")
[Category:史诗电影](../Category/史诗电影.md "wikilink")
[Category:世界记忆名录](../Category/世界记忆名录.md "wikilink")
[Category:机器人電影](../Category/机器人電影.md "wikilink")
[Category:機械人主角題材作品](../Category/機械人主角題材作品.md "wikilink")
[Category:2026年背景電影](../Category/2026年背景電影.md "wikilink")
[Category:柏林取景电影](../Category/柏林取景电影.md "wikilink")

1.
2.

3.
4.

5.