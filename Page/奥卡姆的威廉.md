[William_of_Ockham_-_Logica_1341.jpg](https://zh.wikipedia.org/wiki/File:William_of_Ockham_-_Logica_1341.jpg "fig:William_of_Ockham_-_Logica_1341.jpg")
[Guillelmus_-_Quaestiones_in_quattuor_libros_sententiarum_-_4417303_Carta_25r.tif](https://zh.wikipedia.org/wiki/File:Guillelmus_-_Quaestiones_in_quattuor_libros_sententiarum_-_4417303_Carta_25r.tif "fig:Guillelmus_-_Quaestiones_in_quattuor_libros_sententiarum_-_4417303_Carta_25r.tif")
**奧卡姆的威廉**（William of
Ockham/Occam，約1285年－1349年），又譯為**奧坎**、**奧康**，出生於[英格蘭的](../Page/英格蘭.md "wikilink")[薩里郡](../Page/薩里郡.md "wikilink")[奧卡姆](../Page/奧坎_\(薩里郡\).md "wikilink")（Ockham），在大學註冊為奧卡姆的威廉。14世紀[邏輯學家](../Page/邏輯學.md "wikilink")、[聖方濟各會修士](../Page/聖方濟各會.md "wikilink")。

奧坎早年就加入了方濟會。据记载，他于1309－1321年间在[牛津大學学习神学](../Page/牛津大學.md "wikilink")，但一直未获得硕士（Master）学位（相当于现在的[学士](../Page/学士.md "wikilink")）\[1\]。之后，又在[巴黎大學求學](../Page/巴黎大學.md "wikilink")，能言善辯，被人稱為“駁不倒的博士”。

1322年左右他發表一些言論，主张教會不應擁有私人財產，與當時的[羅馬教廷不合](../Page/羅馬教廷.md "wikilink")，被[教皇](../Page/教皇.md "wikilink")[約翰二十二世宣称为](../Page/約翰二十二世.md "wikilink")“异端”，1324年囚禁在[法國的](../Page/法國.md "wikilink")[亚威农教皇監獄](../Page/亚威农.md "wikilink")。教会聘請六位神学家专门研究其著作，有51篇被判为“异端邪说”。1328年5月他在夜裡越獄，逃往[意大利](../Page/意大利.md "wikilink")[比萨城](../Page/比萨.md "wikilink")。[神圣罗马帝国皇帝](../Page/神圣罗马帝国.md "wikilink")[路易四世收留他](../Page/路易四世_\(神圣罗马帝国\).md "wikilink")，作為回報威廉撰書支持路易四世擁有控制所有在神聖羅馬帝國內的教會和領土的權利。\[2\]
後定居[慕尼黑](../Page/慕尼黑.md "wikilink")。

1347年路易四世死後，教會與他和解，此時欧洲爆發[黑死病](../Page/黑死病.md "wikilink")，奥坎病死。

他在《箴言書注》2卷15題說「切勿浪費較多東西去做用較少的東西同樣可以做好的事情。」因为他是英国奥卡姆人，人们就把这句话称为「**[奥卡姆剃刀](../Page/奥卡姆剃刀.md "wikilink")**」（Occam's
razor）。主要著作有《箴言書注》、《邏輯大全》、《辯論集7篇》等。

## 参考文献

{{-}}

[Category:英國哲學家](../Category/英國哲學家.md "wikilink")
[Category:逻辑学家](../Category/逻辑学家.md "wikilink")
[Category:牛津大学墨顿学院校友](../Category/牛津大学墨顿学院校友.md "wikilink")
[Category:1349年逝世](../Category/1349年逝世.md "wikilink")

1.  Spade, Paul Vincent (ed.). *The Cambridge Companion to Ockham*.
    Cambridge University Press, 1999, p. 20.
2.  Cf. the online British Academy edition at
    <http://www.britac.ac.uk/pubs/dialogus/ockdial.html>