**椿姬 彩菜**（），本名中村
有里\[1\]\[2\]，[日本变性藝人](../Page/日本.md "wikilink")、時裝[模特兒](../Page/模特兒.md "wikilink")，出生於東京都，[身高](../Page/身高.md "wikilink")165公分，[血型為](../Page/血型.md "wikilink")[O型](../Page/ABO血型系统.md "wikilink")，畢業於[青山學院大學](../Page/青山學院大學.md "wikilink")[法語文學系](../Page/法語文學.md "wikilink")。所屬經紀公司為[Horipro](../Page/Horipro.md "wikilink")。

## 概要

  - 原本是男性，曾患上[性別不安症](../Page/性別不安症.md "wikilink")。2004年，當時20歲的椿姬彩菜選擇從大學休學（2007年復學）。及後為籌募經費進行[性別重置手術](../Page/性別重置手術.md "wikilink")，曾於[新宿區](../Page/新宿區.md "wikilink")[歌舞伎町的酒廊](../Page/歌舞伎町.md "wikilink")（ショーパブ，showpub）上班。
  - 2006年單身遠赴[泰國接受手術转为女性](../Page/泰國.md "wikilink")\[3\]，而此事則由在酒廊任職的「領班」（ママ）向其家長透露。
  - 休學期間，在酒廊工作時開始起用椿姬彩菜一名，並沿用至今

## 簡歷

  - 2005年，[辛酸なめ子出版](../Page/:ja:辛酸なめ子.md "wikilink")《新譯小王子》（[日语](../Page/日语.md "wikilink")：「新」訳
    星の王子さま』），椿姬協助法語翻譯。
  - 2006年，時裝[雜誌](../Page/雜誌.md "wikilink")《[小惡魔ageha](../Page/小惡魔ageha.md "wikilink")》刊載由椿姬擔任「跨性別模特兒」（[ニューハーフ](../Page/:ja:ニューハーフ.md "wikilink")[モデル](../Page/:ja:モデル.md "wikilink")）的照片。另外，椿姬在[FM東京電臺節目](../Page/FM東京.md "wikilink")《[WONDERFUL
    WORLD](../Page/:ja:WONDERFUL_WORLD.md "wikilink")》中定期參與演出。
  - 2007年至2008年，曾於《[爽快情報バラエティー
    スッキリ\!\!](../Page/:ja:爽快情報バラエティー_スッキリ!!.md "wikilink")》、《[FNNスーパーニュース](../Page/:ja:FNNスーパーニュース.md "wikilink")》及《[サタデースクランブル](../Page/:ja:サタデースクランブル.md "wikilink")》等電視節目亮相，因而獲得公眾認識。2007年8月，《別冊ageha》刊載以其半生為題的專輯「椿姫彩菜物語」。
  - 2008年，椿姬參與多項ageha模特兒以外的工作，包括，
      - 2月，為アメイズビューティー（Amaze Beauty）公司旗下產品《ヒートジュエル》（Heat
        Jewel）唇彩（lipgloss）擔任模特兒。\[4\]
      - 4月17日，[富士電視臺節目](../Page/富士電視臺.md "wikilink")《近未来予報ツギクル》加入「椿姫彩菜の花予報」環節，椿姬首次獲得定期出演機會，並從此以藝人身份亮相綜藝節目。
      - 4月，與[:菅野結以及武藤静香兩位模特兒聯合零食制造商](../Page/:菅野結以.md "wikilink")「ちぼり」共同開發糖果品牌「Gateau
        de Fee」，並一同制作糖果，第一項推出之商品為「メレンゲハートショコラ」（Meringue Heart
        Chocolate，一種心型的酥皮巧克力糖果）。\[5\]
      - 5月12日，發表限量發行歌曲「Heartsleep」，並負責曲中鋼琴演奏及心臟聲取樣混音（[sampling](../Page/:en:sampling\(music\).md "wikilink")）部分。
      - 6月10日，自傳《わたし、男子校出身です。》（中文譯名：我是男校畢業的女生）出版。

## 參考資料

<div class="references-small">

<references />

</div>

## 外部連結

  - [椿姫彩菜官方網站](https://web.archive.org/web/20090214012407/http://tsubaki-ayana.jp/)
  - [椿姫彩菜Official Blog「きっとLa vie en
    rose」](http://ameblo.jp/tsubaki-ayana/)

[Category:日本變性人](../Category/日本變性人.md "wikilink")
[Category:日本女性電視藝人](../Category/日本女性電視藝人.md "wikilink")
[Category:埼玉縣出身人物](../Category/埼玉縣出身人物.md "wikilink")
[Category:Horipro](../Category/Horipro.md "wikilink")
[Category:日本女性模特兒](../Category/日本女性模特兒.md "wikilink")
[Category:跨性別模特兒](../Category/跨性別模特兒.md "wikilink")
[Category:青山學院大學校友](../Category/青山學院大學校友.md "wikilink")
[Category:小惡魔ageha模特兒](../Category/小惡魔ageha模特兒.md "wikilink")

1.  椿姫彩菜自傳『わたし、男子校出身です。』、[ポプラ社](../Page/:ja:ポプラ社.md "wikilink")、2008年6月10日初版。
2.  [日本電視臺節目](../Page/日本電視臺.md "wikilink")[ザ\!世界仰天ニュース](../Page/:ja:ザ!世界仰天ニュース.md "wikilink")』2008年6月18日中公開本名。
3.  [日本：变性名模除偏见](http://www.sinchew.com.my/node/225998).星洲网.
4.
5.