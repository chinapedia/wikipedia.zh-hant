[2003年](../Page/2003年.md "wikilink")[10月的新闻事件](../Page/10月.md "wikilink")：

**请参看：**

  - [中国载人航天计划](../Page/中国载人航天计划.md "wikilink")
  - [中东和平路线图](../Page/中东和平路线图.md "wikilink")
  - [欧盟扩大](../Page/欧盟.md "wikilink")
  - [北朝鲜核危机](../Page/北朝鲜核危机.md "wikilink")
  - [中俄日输油管线争端](../Page/中俄日输油管线争端.md "wikilink")
  - [恐怖主义](../Page/恐怖主义.md "wikilink")
  - [美军占领伊拉克](../Page/美军占领伊拉克.md "wikilink")
  - [钓鱼岛](../Page/钓鱼岛.md "wikilink")
  - [2003年女足世界杯](../Page/2003年女足世界杯.md "wikilink")

## [10月31日](../Page/10月31日.md "wikilink")

  - [马来西亚首相](../Page/马来西亚.md "wikilink")[马哈迪下午正式辞职](../Page/马哈迪·莫哈末.md "wikilink")。[1](http://news.xinhuanet.com/world/2003-11/01/content_1154379.htm)

## [10月29日](../Page/10月29日.md "wikilink")

  - [北朝鲜核危机](../Page/北朝鲜核危机.md "wikilink")：[吴邦国抵达](../Page/吴邦国.md "wikilink")[朝鲜访问](../Page/朝鲜.md "wikilink")。[2](http://www.people.com.cn/GB/shizheng/1024/2158982.html)

## [10月26日](../Page/10月26日.md "wikilink")

  - 中国自行研制的[小鹰500轻型飞机首飞成功](../Page/小鹰500.md "wikilink")。

## [10月24日](../Page/10月24日.md "wikilink")

  - [协和飞机退出飞行](../Page/协和飞机.md "wikilink")。[3](http://news.xinhuanet.com/world/2003-10/25/content_1142137.htm)
  - [中国足球协会甲级联赛](../Page/中国足球协会甲级联赛.md "wikilink")（简称“中甲联赛”）将在[2004年正式启动](../Page/2004年.md "wikilink")。[4](http://news.xinhuanet.com/nsports/2003-10/24/content_1141537.htm)
  - [中印边界问题特别代表首次会晤在](../Page/中印边界问题.md "wikilink")[新德里结束](../Page/新德里.md "wikilink")。[5](http://news.sina.com.cn/c/2003-10-24/2205985818s.shtml)

## [10月23日](../Page/10月23日.md "wikilink")

  - 中国国务院总理[温家宝和日本首相](../Page/温家宝.md "wikilink")[小泉纯一郎相互致电](../Page/小泉纯一郎.md "wikilink")，祝贺《[中日和平友好条约](../Page/中日和平友好条约.md "wikilink")》缔结25周年。[6](http://news.xinhuanet.com/world/2003-10/23/content_1137543.htm)

## [10月21日](../Page/10月21日.md "wikilink")

  - [中国月球探测计划首席科学家称中国五年内将探测月球](../Page/中国月球探测计划.md "wikilink")。[7](http://www.chinanews.com.cn/n/2003-10-22/26/359641.html)

## [10月20日](../Page/10月20日.md "wikilink")

  - [北朝鲜核危机](../Page/北朝鲜核危机.md "wikilink")：[美国总统首次正式承诺对](../Page/美国总统.md "wikilink")[朝鲜安全保证](../Page/朝鲜.md "wikilink")。[8](http://news.xinhuanet.com/world/2003-10/21/content_1135309.htm)

## [10月18日](../Page/10月18日.md "wikilink")

  - [云南](../Page/云南.md "wikilink")[大姚发生](../Page/大姚县.md "wikilink")[地震](../Page/地震.md "wikilink")：3死46伤

## [10月17日](../Page/10月17日.md "wikilink")

  - [西湖博览会](../Page/西湖博览会.md "wikilink")（[9](https://web.archive.org/web/20180823175414/http://www.xh-expo.com/)）第五届杭州西湖博览会今日开幕，盛况空前。

## [10月16日](../Page/10月16日.md "wikilink")

  - [中国载人航天计划](../Page/中国载人航天计划.md "wikilink")：[神舟五号航天飞船成功在](../Page/神舟五号.md "wikilink")[内蒙古着陆](../Page/内蒙古.md "wikilink")。（[BBC](http://news.bbc.co.uk/2/hi/asia-pacific/3194892.stm)）
  - [天主教](../Page/天主教.md "wikilink")：100,000天主教徒庆祝教宗[若望·保禄二世登基](../Page/若望·保禄二世.md "wikilink")25周年。[10](http://www.voanews.com/article.cfm?objectID=07A89899-88F9-4DEE-9EC10332441DBE80)
  - [美国众议院通过法案](../Page/美国众议院.md "wikilink")，决定制裁[叙利亚](../Page/叙利亚.md "wikilink")。（[凤凰网](http://www.phoenixtv.com/home/news/world/200310/16/121776.html)）
  - [美军占领伊拉克](../Page/美军占领伊拉克.md "wikilink")：[美方提出的对伊新方案在](../Page/美国.md "wikilink")[联合国通过](../Page/联合国.md "wikilink")。[俄罗斯](../Page/俄罗斯.md "wikilink")、[德国和](../Page/德国.md "wikilink")[法国表示支持](../Page/法国.md "wikilink")，但拒绝提供军事或经济援助。[11](http://www.rte.ie/aertel/p104.htm)[12](http://news.bbc.co.uk/2/hi/middle_east/3197688.stm)

## [10月15日](../Page/10月15日.md "wikilink")

  - [中国载人航天计划](../Page/中国载人航天计划.md "wikilink")：[神舟五号载人飞船发射成功](../Page/神舟五号载人飞船.md "wikilink")，[航天员为](../Page/航天员.md "wikilink")[杨利伟](../Page/杨利伟.md "wikilink")。（[Reuters](https://web.archive.org/web/20031025193226/http://www.reuters.co.uk/newsPackageArticle.jhtml?type=topNews&storyID=388028&section=news)
    | [CNN](http://www.cnn.com/2003/TECH/space/10/14/space.race/) |
    [新华社](http://www.xinhuanet.com/video/szwh/shipin.htm)）
  - [阿塞拜疆](../Page/阿塞拜疆.md "wikilink")：总统选举。（[CNN](http://www.cnn.com/WORLD/election.watch/europe/azerbaijan3.html)）

## [10月14日](../Page/10月14日.md "wikilink")

  - [德国科学家发现的第](../Page/德国.md "wikilink")111号[元素获得认可](../Page/元素.md "wikilink")，缩写为[Uuu](../Page/錀.md "wikilink")。[13](http://news.xinhuanet.com/st/2003-10/15/content_1124909.htm)

## [10月12日](../Page/10月12日.md "wikilink")

  - [中国载人航天计划](../Page/中国载人航天计划.md "wikilink")：[中国表示将于](../Page/中国.md "wikilink")[10月15日至](../Page/10月15日.md "wikilink")[17日间发射第一艘载人航天飞船](../Page/10月17日.md "wikilink")。这艘太空船计划绕地球14圈。[14](http://news.bbc.co.uk/2/hi/asia-pacific/3180618.stm)
  - [利比里亚外交关系](../Page/利比里亚外交关系.md "wikilink")：[利比里亚宣布于](../Page/利比里亚.md "wikilink")[台湾中断外交关系](../Page/台湾.md "wikilink")，重新与[中华人民共和国建交](../Page/中华人民共和国.md "wikilink")。台湾外交部长[简又新请辞](../Page/简又新.md "wikilink")。（[凤凰网](http://www.phoenixtv.com/home/news/taiwan/200310/13/119245.html)
    |
    [MSNBC](http://famulus.msnbc.com/FamulusIntl/reuters10-12-054050.asp?reg=PACRIM)）

## [10月10日](../Page/10月10日.md "wikilink")

  - [雙十節](../Page/雙十節.md "wikilink")92周年紀念
  - 第８屆[釜山國際電影節閉幕](../Page/釜山國際電影節.md "wikilink")

## [10月8日](../Page/10月8日.md "wikilink")

  - [瑞典皇家科学院宣布](../Page/瑞典皇家科学院.md "wikilink")，把2003年[诺贝尔经济学奖授予](../Page/诺贝尔经济学奖.md "wikilink")[美国经济学家](../Page/美国.md "wikilink")[罗伯特·恩格尔和](../Page/罗伯特·恩格尔.md "wikilink")[英国经济学家](../Page/英国.md "wikilink")[克莱夫·格兰杰](../Page/克莱夫·格兰杰.md "wikilink")，以表彰他们在“[分析经济时间数列](../Page/分析经济时间数列.md "wikilink")”研究领域所作出的突破性贡献。

## [10月7日](../Page/10月7日.md "wikilink")

  - [美国加州州长](../Page/美国.md "wikilink")[戴维斯在州长罢免选举中被该州选民投票罢免](../Page/戴维斯.md "wikilink")，[施瓦辛格获得州长职位](../Page/施瓦辛格.md "wikilink")。[15](http://news.xinhuanet.com/ent/2003-10/09/content_1114634.htm)
  - “[人类基因组外遗传计划](../Page/人类基因组外遗传计划.md "wikilink")（Human Epigenome
    Project）”正式启动。[16](http://news.xinhuanet.com/st/2003-10/09/content_1114511.htm)

## [10月5日](../Page/10月5日.md "wikilink")

  - 数十名来自[以色列和其它国家的和平人士表示将以](../Page/以色列.md "wikilink")[人体盾牌保护](../Page/人体盾牌.md "wikilink")[阿拉法特](../Page/阿拉法特.md "wikilink")。[17](http://news.xinhuanet.com/world/2003-10/05/content_1110881.htm)
  - 据报道，[印度军队](../Page/印度.md "wikilink")4日晚炮击了克什米尔地区的尼拉姆谷地，至少造成12人死亡，15人受伤。[18](http://news.xinhuanet.com/world/2003-10/05/content_1111014.htm)
  - [联合国](../Page/联合国.md "wikilink")、[欧盟以及一些国家领导人强烈谴责](../Page/欧盟.md "wikilink")10月4日发生在[以色列海法的自杀式炸弹袭击](../Page/以色列.md "wikilink")。[19](http://news.xinhuanet.com/world/2003-10/05/content_1110871.htm)
  - [以色列军机袭击了位于](../Page/以色列.md "wikilink")[叙利亚境内被军方称为](../Page/叙利亚.md "wikilink")[伊斯兰圣战训练基地的](../Page/伊斯兰圣战.md "wikilink")[巴勒斯坦人营地](../Page/巴勒斯坦人.md "wikilink")。[20](https://web.archive.org/web/20041028074141/http://story.news.yahoo.com/news?tmpl=story&cid=540&e=1&u=%2Fap%2F20031005%2Fap_on_re_mi_ea%2Fisrael_syria)[21](http://news.xinhuanet.com/world/2003-10/05/content_1111015.htm)

## [10月4日](../Page/10月4日.md "wikilink")

  - [俄罗斯暂不与](../Page/俄罗斯.md "wikilink")[中国和](../Page/中国.md "wikilink")[日本讨论东](../Page/日本.md "wikilink")[西伯利亚输油管道具体走向](../Page/西伯利亚.md "wikilink")。（评论）东亚战略石油问题将继续。[22](http://news.sohu.com/72/63/news214076372.shtml)
  - [朝鲜宣称](../Page/朝鲜.md "wikilink")：“[朝鲜民主主义人民共和国将维持并逐步加强其核威慑力量](../Page/朝鲜民主主义人民共和国.md "wikilink")。”[23](http://www.zaobao.com/gj/gj005_041003.html)
  - [以巴冲突](../Page/以巴冲突.md "wikilink")：一名巴勒斯坦人发动自杀式爆炸袭击，至少18人死亡，其中5名儿童。[24](https://web.archive.org/web/20050204055125/http://www.msnbc.com/news/801833.asp?vts=100420030702&cp1=1)

## [10月2日](../Page/10月2日.md "wikilink")

  - [中国](../Page/中国.md "wikilink")[女足在](../Page/女足.md "wikilink")[美国举行的第四届](../Page/美国.md "wikilink")[女足世界杯上不敌](../Page/女足世界杯.md "wikilink")[加拿大队](../Page/加拿大.md "wikilink")，比分为1：0。
  - 第８屆[釜山國際電影節在韓國釜山海雲臺開幕](../Page/釜山國際電影節.md "wikilink")
    [25](https://web.archive.org/web/20150107153917/http://www.piff.org/)

## [10月1日](../Page/10月1日.md "wikilink")

  - [湖北省一中年男子上访](../Page/湖北省.md "wikilink")[北京](../Page/北京.md "wikilink")，在[天安门广场自焚受伤](../Page/天安门广场.md "wikilink")。
  - [日本](../Page/日本.md "wikilink")[冲绳警方逮捕了两名酒后滋事的](../Page/冲绳.md "wikilink")[美国士兵](../Page/美国.md "wikilink")。[26](http://www.phoenixtv.com/home/news/world/200310/01/116178.html)
  - [日本官房长官](../Page/日本.md "wikilink")[福田康夫](../Page/福田康夫.md "wikilink")9月30日表示法院对侵华日军遗留毒气问题的赔偿十分严厉，他将对此进一步商讨以决定是否上诉。
  - [朝鲜外务部表示朝鲜已经对举行新一轮谈判](../Page/朝鲜.md "wikilink")“失去了兴趣”。[27](http://www.phoenixtv.com/home/news/world/200310/01/116036.html)
  - [韩国举行大型阅兵仪式](../Page/韩国.md "wikilink")。[28](http://www.phoenixtv.com/home/news/world/200310/01/116159.html)
  - [梵蒂冈](../Page/梵蒂冈.md "wikilink")[约翰·保罗二世的顾问表示教皇目前的身体情况很差](../Page/约翰·保罗二世.md "wikilink")。希望全世界的[天主教徒为其祈祷](../Page/天主教.md "wikilink")。[29](http://www.phoenixtv.com/home/news/world/200310/01/116144.html)
  - [日本](../Page/日本.md "wikilink")[北海道发生的强烈](../Page/北海道.md "wikilink")[地震使得其地理位置发生明显位移](../Page/地震.md "wikilink")。[30](http://www.phoenixtv.com/home/news/world/200310/01/116179.html)