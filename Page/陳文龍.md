**陳文龍**（；）原名**子龍**，字**君贲**、**德刚**，南宋末年[興化軍](../Page/興化軍.md "wikilink")[莆田縣](../Page/莆田縣.md "wikilink")（今属福建省莆田市）人。是[南宋的抗元名將](../Page/南宋.md "wikilink")。

## 簡介

出身兴化军的[玉瑚陈氏家族](../Page/玉瑚陈氏.md "wikilink")，为[陈俊卿五世从孙](../Page/陳俊卿_\(南宋\).md "wikilink")。早年隨父陈粢定居[連江](../Page/連江.md "wikilink")[長樂](../Page/長樂.md "wikilink")（今[阜山](../Page/阜山.md "wikilink")）。幼颖悟，苦学不厌。

[淳祐十一年](../Page/淳祐.md "wikilink")（1251年），入[乡学](../Page/乡学.md "wikilink")。[宝祐四年](../Page/宝祐.md "wikilink")（1256年），入[太学](../Page/太学.md "wikilink")。宋咸淳四年（1268年）戊辰科[進士](../Page/進士.md "wikilink")，龙飞射策第一，[宋度宗赐名文龍](../Page/宋度宗.md "wikilink")。历任镇东军节度判官、历崇政殿说书，七年（1271年）又官至[秘书省校书郎](../Page/秘书省.md "wikilink")。当时朝政大权由[賈似道把持](../Page/賈似道.md "wikilink")，賈似道爱其文，雅礼重之。翌年，在賈似道的提拔下升为[监察御史](../Page/监察御史.md "wikilink")。但陈文龙刚正不阿，不仅不讨好贾似道，反而多次忤逆贾似道的意愿。

当时[元军](../Page/元.md "wikilink")[包围襄阳](../Page/襄樊之战.md "wikilink")，形式万分危急。贾似道表面上准备亲自支援[襄阳](../Page/襄阳.md "wikilink")，暗地里却指使自己的党羽极力反对，最终襄阳在1273年被元军攻陷。陈文龙上書彈劾賈似道的过失。[范文虎丢失了襄阳](../Page/范文虎.md "wikilink")，贾似道不处罚他，反而让他镇守[安庆](../Page/安庆.md "wikilink")；又派[赵溍知](../Page/赵溍.md "wikilink")[建康](../Page/建康.md "wikilink")、[黄万石知京城](../Page/黄万石.md "wikilink")[临安](../Page/临安.md "wikilink")。陈文龙认为赵溍没有经验、黄万石是个昏官；范文虎兵败见用，是赏罚不明，要求将三人全部罢免。贾似道大怒，将贬陈文龙为[抚州知州](../Page/抚州.md "wikilink")。賈似道怒犹未尽，又指使右正言[李可](../Page/李可.md "wikilink")[弹劾陈文龙](../Page/弹劾.md "wikilink")，文龙憤而辭官\[1\]，[刘埙驰马出桥南](../Page/刘埙.md "wikilink")，为文龙饯别。不久，[吕文焕领导元军东下](../Page/吕文焕.md "wikilink")，范文虎第一个投降，并引导元军侵略。贾似道大败于鲁港，赵溍得知后率先逃跑。此时才后悔没有采纳陈文龙的建议，将他任命为左司谏，后迁侍御史。元军不断侵占南宋领土，形式万分危急。宰相[王爚](../Page/王爚.md "wikilink")、[陈宜中二人不能出一策](../Page/陈宜中.md "wikilink")，却天天在朝堂上争私意。王爚支持的，往往陈宜中就反对。陈宜中支持的，往往王爚就反对。[张世杰诸将分四道出师](../Page/张世杰.md "wikilink")，而大臣不监护，被台谏弹劾。王爚请求行边，下公卿杂议。陈宜中请求担任督师，又下公卿杂议。陈文龙上书称：“《书》言‘三后协心，同底于道。’北兵今日取某城，明日筑某堡，而我以文相逊，以迹相疑，譬犹拯溺救焚，而为安步徐行之仪也。请诏大臣同心图治，无滋虚议。”然而朝廷没有采纳他的意见。王爚最终被罢免官职，但此时南宋的局势已经无法挽回了。

德祐元年（1275年）冬季，陈文龙被提拔为[参知政事](../Page/参知政事.md "wikilink")。不久朝廷讨论向元军投降之事，陈文龙辞官归家。但刚出国门就后悔了，上疏请求官复原职，朝廷没有理会他，于是回到了兴化。不久，南宋朝廷向元军投降。[景炎元年](../Page/景炎.md "wikilink")（1276年），[张世杰在](../Page/张世杰.md "wikilink")[福州拥立](../Page/福州.md "wikilink")[宋端宗继位](../Page/宋端宗.md "wikilink")，下詔让陈文龙官复原职，仍任参知政事。[漳州发生叛乱](../Page/漳州.md "wikilink")，朝廷任命文龍為[闽](../Page/福建.md "wikilink")[广](../Page/广东.md "wikilink")[宣抚使前去征讨](../Page/宣抚使.md "wikilink")。听说[黄恮镇守漳州时有恩信](../Page/黄恮.md "wikilink")，便引为参谋官。陈文龙驻军[泉州](../Page/泉州.md "wikilink")，派黄恮前去招抚，不费一兵一卒就平定了叛乱。[兴化军有一支名叫](../Page/兴化军.md "wikilink")[石手军的部队](../Page/石手军.md "wikilink")，能够掷石中人，朝廷认为没有用处，将其裁撤。石手军便发动叛乱。朝廷派陈文龙知兴化军，将叛乱平定。

十一月，元兵在降将[王世强的引导下攻打福州](../Page/王世强.md "wikilink")，[宋端宗逃奔广州](../Page/宋端宗.md "wikilink")，留陳文龍守興化。当时[建宁](../Page/建宁.md "wikilink")、泉州、[福州皆降](../Page/福州.md "wikilink")，陈文龙母黄氏及幼子陈璥被元军扣为人质。元军令福州知军[王刚中持书到兴化招降](../Page/王刚中.md "wikilink")，文龙怒斩正使，令其副使持回信归福州，怒斥王刚中叛国。元朝又派其姻家持书招降，陈文龙焚其书信，怒斩来使。當時城中驻兵不到千人，陳文龍在兴化城樓上立起“生为宋臣，死为宋鬼”的大旗，傾盡家產，招募义兵，抗击元军。他在[田囊山设伏](../Page/田囊山.md "wikilink")，数次击败元军。有风其纳款者，文龙曰：“诸君特畏死耳，未知此生能不死乎？”德佑二年（1276年）十二月，飭令部將林華、陈渊至福州侦察敌情。林華等暗中與降将王世强勾结，反引元兵萬人至城下，通判[曹澄孫開門迎敵](../Page/曹澄孫.md "wikilink")，最後城陷被俘，被押至福州元将[董文炳军中](../Page/董文炳.md "wikilink")。董文柄令左右招降之，不从。董文炳试图胁迫文龙投降，文龙以手指腹道：“此皆节义文章也，可相逼邪？”押送[杭州途中開始](../Page/杭州.md "wikilink")[絕食](../Page/絕食.md "wikilink")，經杭州謁拜岳飛廟時，氣絕而死，葬於杭州西湖智果寺旁。文龙之母被执於福州尼姑庵，闻讯，亦绝食亡。[宋端宗聞之](../Page/宋端宗.md "wikilink")，追贈[太師銜](../Page/太師.md "wikilink")，賜[諡](../Page/諡.md "wikilink")**忠肅**。

[蒲寿庚以泉州降元](../Page/蒲寿庚.md "wikilink")，告其民曰：“陈文龙非不忠义，如民何？”听闻此话的人都嘲笑他。不久元军归福州。陈文龙的堂叔[陳瓚](../Page/陳瓚.md "wikilink")，倾家财300万缗渡海至广东，支持张世杰抗元。\[2\]。闻元军归福州，乃率民兵攻兴化，杀林华。元兵复来攻，兵败被执，车裂而死。陈瓒子陈若水被南宋封为督府架阁，陈文龙的子孙陈八宣、陈汝楫，亦以义兵南下，至广东跟随宋帝。[崖山海战之后](../Page/崖山海战.md "wikilink")，南宋灭亡。陈八宣、陈汝楫所率的众多义兵散居到广东南部的[新会](../Page/新会.md "wikilink")、[吴川](../Page/吴川.md "wikilink")、[化州](../Page/化州.md "wikilink")、[雷州一带以及](../Page/雷州.md "wikilink")[海南岛沿海地区](../Page/海南岛.md "wikilink")。明末抗清将领[陈上川就是陈文龙的后裔](../Page/陈上川.md "wikilink")。

## 後世尊崇

明朝建立后，[明太祖封各地的城隍神](../Page/明太祖.md "wikilink")。陳文龍為[福州府](../Page/福州府.md "wikilink")[城隍](../Page/城隍.md "wikilink")，[陈瓒则被封为](../Page/陈瓒.md "wikilink")[兴化府城隍](../Page/兴化府.md "wikilink")。永樂六年（1409年），又其以能保佑航運、漁民的緣故，加封陈文龙为「水部尚書」；清乾隆四十六年（1782年），晉封「鎮海王」。\[3\]

清代[林則徐為陳文龍廟題書對聯](../Page/林則徐.md "wikilink")-{云}-：「節鎮守鄉邦，縱景炎殘局難支，一代忠貞垂史傳；英靈昭海噬，與信國隆名並峙，十州清晏仗神庥。」認為其與[文天祥齊名](../Page/文天祥.md "wikilink")。

陳文龍受到[興化民系和](../Page/興化民系.md "wikilink")[福州民系的尊崇](../Page/福州民系.md "wikilink")。在[莆田](../Page/莆田.md "wikilink")，陈文龙、陈瓒被称为“兴化二忠”，各地建祠造庙，世代供祀。\[4\]福州人稱陳文龍為「尚書公」，將其尊奉為水神。根據清代[齊鯤](../Page/齊鯤.md "wikilink")、[費錫章所著的](../Page/費錫章.md "wikilink")《[續琉球國志畧](../Page/續琉球國志畧.md "wikilink")》記載，在清朝時期，每當[冊封船隊出使](../Page/琉球冊封使列表.md "wikilink")[琉球時](../Page/琉球國.md "wikilink")，都要將[媽祖和](../Page/媽祖.md "wikilink")[拿公的神像奉安頭號封舟](../Page/拿公.md "wikilink")，陳尚書公的神像奉安二號封舟，以祈求海路平安。\[5\]在[福州有](../Page/福州.md "wikilink")「官船拜尚書，民船拜[媽祖](../Page/媽祖.md "wikilink")」\[6\]的說法，顯示二神傳統受祀的情形，現則民間亦崇拜尚書公，非限於官方了。

今日福州各地都有祭祀陳文龍的「尚書廟」。在福州市區共有十餘座尚書廟，僅[台江區和](../Page/台江區.md "wikilink")[倉山區就有尚書廟五處](../Page/倉山區.md "wikilink")，其中以台江區[上下杭一帶](../Page/上下杭.md "wikilink")[萬壽尚書廟最為出名](../Page/萬壽尚書廟.md "wikilink")。陳尚書的信仰後來被傳到了[台灣和](../Page/台灣.md "wikilink")[東南亞國家](../Page/東南亞.md "wikilink")。[馬祖](../Page/馬祖.md "wikilink")[北竿鄉](../Page/北竿鄉.md "wikilink")[塘岐村](../Page/塘岐村.md "wikilink")[北竿機場對面的](../Page/北竿機場.md "wikilink")「水部尚書公府」，主祀陳文龍，奉台江的萬壽尚書廟為祖廟。\[7\]\[8\]

## 诗作

## 注釋

## 參考書目

  - 《宋史·卷四百五十一·陈文龙传》

[Category:莆田狀元](../Category/莆田狀元.md "wikilink")
[Category:南宋军事人物](../Category/南宋军事人物.md "wikilink")
[Category:咸淳四年戊辰科進士](../Category/咸淳四年戊辰科進士.md "wikilink")
[W文](../Category/宋朝狀元.md "wikilink")
[Category:死于绝食的南宋人](../Category/死于绝食的南宋人.md "wikilink")
[Category:莆田人](../Category/莆田人.md "wikilink")
[W文](../Category/陳姓.md "wikilink")
[Category:福建民間信仰](../Category/福建民間信仰.md "wikilink")
[Category:台灣民間信仰](../Category/台灣民間信仰.md "wikilink")
[C陳](../Category/東亞民間信仰.md "wikilink")
[Category:中国人物神](../Category/中国人物神.md "wikilink")
[Category:城隍](../Category/城隍.md "wikilink")
[Category:海神](../Category/海神.md "wikilink")
[Category:馬祖文化](../Category/馬祖文化.md "wikilink")
[Category:諡忠肅](../Category/諡忠肅.md "wikilink")
[Category:葬于杭州](../Category/葬于杭州.md "wikilink")
[Category:中國餓死人物](../Category/中國餓死人物.md "wikilink")

1.  一說賈似道唆使监察御史李可，以“催科峻急”为罪名，劾罢陈文龙。
2.  弘治《兴化府志》卷四十三《陈瓒传》及《福建史稿》引《莆田玉瑚陈氏家乘》稱，陈瓒是陈文龙从叔。《宋史·陈文龙传》和《福建通纪》称陈瓒为陈文龙从子。
3.  [陈文龙：海上男神和妈祖齐名](http://fj.qq.com/a/20150317/022510.htm)
4.  [陈瓒陈文龙二忠城隍祖庙落户莆田壶公山](http://news.xinhuanet.com/local/2015-11/24/c_128461185.htm)
5.  （清）[齊鯤](../Page/齊鯤.md "wikilink")、[費錫章](../Page/費錫章.md "wikilink")，《[續琉球國志畧·卷三](../Page/:s:續琉球國志畧_\(齊鯤、費錫章\)/卷三.md "wikilink")》：「國朝冊封琉球，向例：請天后、拏公神像，供奉頭號貢船；請尚書神像，供奉二號船。」
6.  [福州陈文龙信俗文化节活动
    颂扬精忠报国佑民精神](http://www.citure.net/info/2017215/2017215110820.shtml)

7.  [陈文龙](http://www.mdwhyjh.com/mdwh/wenhuamingren/850.html)
8.  [陈文龙：闽台一脉相承的尚书神缘](http://lilun.fznews.com.cn/mddjt/mdwh/2012-8-2/201282tn_AmatHjj15392.shtml)