**沙巴團結黨**（），
成立於1985年1月，於1985年3月5日註冊，同年4月開始在[沙巴執政](../Page/沙巴.md "wikilink")，是[馬來西亞政黨之一](../Page/馬來西亞.md "wikilink")。\[1\]該黨是以[卡達山人為主的多民族政黨](../Page/卡達山人.md "wikilink")，由主席[拿督](../Page/拿督.md "wikilink")[百林吉丁岸](../Page/百林吉丁岸.md "wikilink")（Datuk
Joseph PAIRIN
Kitingan）所領導，並且從[沙巴人民联盟](../Page/沙巴人民联盟.md "wikilink")（BERJAYA）分裂出來。\[2\]1986年6月加入[國民陣線](../Page/國民陣線.md "wikilink")（簡稱"[國陣](../Page/國陣.md "wikilink")"），1990年10月退出国阵后，又于2002年1月重返国阵。[2018年马来西亚大选国阵败选后](../Page/2018年马来西亚大选.md "wikilink")，沙巴团结党退出国阵而另组[沙巴团结联盟](../Page/沙巴团结联盟.md "wikilink")。

## 参考文献

## 外部链接

  - [沙巴團結黨](http://www.pbs-sabah.org)

{{-}}

[Category:马来西亚政党](../Category/马来西亚政党.md "wikilink")
[Category:區域主義政黨](../Category/區域主義政黨.md "wikilink")
[Category:沙巴](../Category/沙巴.md "wikilink")

1.
2.