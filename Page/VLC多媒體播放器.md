**VLC多媒體播放器**（），最初名為，是[VideoLAN計劃的](../Page/VideoLAN.md "wikilink")[开放源代码](../Page/开放源代码.md "wikilink")[多媒體播放器](../Page/多媒體播放器.md "wikilink")。支援眾多音訊與視訊解碼器及檔案格式，並支援[DVD影音光碟](../Page/DVD.md "wikilink")、[VCD影音光碟及各類](../Page/Video_CD.md "wikilink")[串流協定](../Page/串流.md "wikilink")。它也能作為[单播或](../Page/单播.md "wikilink")[多播的串流伺服器在](../Page/多播.md "wikilink")[IPv4或](../Page/IPv4.md "wikilink")[IPv6的高速網路連線下使用](../Page/IPv6.md "wikilink")。调用[FFmpeg計劃的解碼器與](../Page/FFmpeg.md "wikilink")[程式庫使其有播放多媒體檔案及加密DVD影碟的功能](../Page/程式庫.md "wikilink")。

## 历史

VideoLAN最初作为大学的研究项目于1996年启动。VLC原指VideoLAN客户端（），但VLC已不再是个简单的客户端\[1\]\[2\]。该软件包含了客户端及服务器以通过校园网播放视频流。VLC作为VideoLAN计划所开发的客户端，最初是[巴黎中央理工學院學生的專題計畫](../Page/巴黎中央理工學院.md "wikilink")，現在贡献者已经遍及世界，并由非营利组织VideoLAN开发。

1998年开始重新编写，2001年1月1日通过[GNU通用公共许可协议发布](../Page/GNU通用公共许可协议.md "wikilink")，获得了巴黎中央理工學院校长的许可。[服务器版软件VideoLan服务器](../Page/服务器.md "wikilink")（，）的功能早已集成进VLC并停止开发\[3\]。由于软件已不再是客户端/服务器基础装备，因此被更名为VLC
media player。

VLC的图标是源自于[交通錐](../Page/交通錐.md "wikilink")，由Ecole Centrale's Networking
Students'
Association创作\[4\]。2006年由手繪的低解析度圖示改成\[5\]高解析度的[CGI](../Page/電腦成像.md "wikilink")，作者是Richard
Øiestad\[6\]。

经过13年的开发，VLC多媒体播放器1.0.0版于2009年7月7日发布\[7\]。VLC多媒体播放器2.0.0版于2012年2月18日发布\[8\]\[9\]。

2011至2012年，大部分VLC组件重新以[GNU通用公共许可协议发布](../Page/GNU通用公共许可协议.md "wikilink")\[10\]\[11\]。

VLC是最早被[SourceForge完全统计下载的软件](../Page/SourceForge.md "wikilink")\[12\]，下载量超过13亿次。\[13\]

VLC现在可通过苹果的[App
Store下载于](../Page/App_Store_\(iOS\).md "wikilink")[iPad](../Page/iPad.md "wikilink")、[iPhone及](../Page/iPhone.md "wikilink")[iPod
Touch](../Page/iPod_Touch.md "wikilink")。早期因为GPL和iTunes
Store许可的冲突而被下架\[14\]，后来改用[Mozilla公共许可证发布](../Page/Mozilla公共许可证.md "wikilink")。

2014年3月13日开始发布[Windows
Store的版本](../Page/Windows_Store.md "wikilink")。支持[Windows
Phone及可能支持](../Page/Windows_Phone.md "wikilink")[Xbox
One的版本也在开发中](../Page/Xbox_One.md "wikilink")。\[15\]

## 特色

VLC自建的動態核心模組，使所有的介面（interfaces）、視訊和音訊輸出（video and audio
outputs）、控制（controls）、定標器（scalers）、解碼器（codecs）、音訊/視訊濾波器（audio/video
filters）包含於統一的模組之內，便於使用。在播放媒體檔時，無需使用者干預，VLC會根據不同的情況自行調度輸入協定（input
protocol）、輸入檔的格式（input file format）、輸入轉碼器（input codec）、視訊卡功能（video card
capabilities）和其他參數。

VLC media
player具有[跨平台的特性](../Page/跨平台.md "wikilink")，可用于[Windows](../Page/Windows.md "wikilink")、[macOS](../Page/macOS.md "wikilink")、[GNU/Linux](../Page/GNU/Linux.md "wikilink")、[FreeBSD](../Page/FreeBSD.md "wikilink")、[NetBSD](../Page/NetBSD.md "wikilink")、[OpenBSD](../Page/OpenBSD.md "wikilink")、[Solaris](../Page/Solaris.md "wikilink")、[Android](../Page/Android.md "wikilink")、[iOS](../Page/iOS.md "wikilink")、[QNX](../Page/QNX.md "wikilink")、及[OS/2](../Page/OS/2.md "wikilink")。

開啟實驗性功能：使用[GPU加速解碼](../Page/GPU.md "wikilink")（需顯卡及配合驅動程式支援），用於大幅降低[CPU佔用率](../Page/CPU.md "wikilink")。

在Windows、Linux以及某些平台，VLC提供了一個[Mozilla擴充套件](../Page/Mozilla.md "wikilink")，使得某些網站上附帶的[QuickTime及](../Page/QuickTime.md "wikilink")[Windows
Media多媒體檔案](../Page/Windows_Media.md "wikilink")，可以在非[微軟或](../Page/微軟.md "wikilink")[蘋果電腦的作業系統中](../Page/蘋果電腦.md "wikilink")，正常顯示於Mozilla的[瀏覽器下](../Page/瀏覽器.md "wikilink")。

從版本0.8.2開始，VLC亦提供了一個[ActiveX的擴充套件](../Page/ActiveX.md "wikilink")，使用戶可以在[Internet
Explorer下](../Page/Internet_Explorer.md "wikilink")，正常顯示某些網站上附帶的QuickTime及[Windows
Media多媒體檔案](../Page/Windows_Media.md "wikilink")。

VLC支援播放某些沒有下載完成的視訊檔案部份內容。

VLC支援協力廠商面板。

## 彩蛋

VLC在[聖誕節的前後各一週](../Page/聖誕節.md "wikilink")（12月18日至翌年1月1日）会自动把软件執行中的[圖示设置为一个戴](../Page/圖示.md "wikilink")[圣诞帽的交通錐](../Page/圣诞帽.md "wikilink")。\[16\]

## 參見

  - [媒體播放器列表](../Page/媒體播放器列表.md "wikilink")
  - [媒體播放器比較](../Page/媒體播放器比較.md "wikilink")

## 參考資料

## 外部連結

  -
  - [VLC Media Player
    Portable官方網站](http://portableapps.com/apps/music_video/vlc_portable)

  - [VideoLAN論壇](https://forum.videolan.org)

  - [支援特性列表](https://www.videolan.org/vlc/features.html)

  - [\#videolan IRC頻道](irc://irc.freenode.net/videolan)

### 中文翻譯社群

  - [VLC media player國際化說明](https://www.videolan.org/developers/i18n/)
  - [VLC media
    player翻譯討論區](https://forum.videolan.org/viewforum.php?f=17)
  - [VLC media
    player翻譯計劃首頁](https://web.archive.org/web/20140303153926/https://www.transifex.com/projects/p/vlc-trans/language/zh_TW/)——VLC官方使用[Transifex翻譯平台](../Page/Transifex.md "wikilink")（[相關說明](http://chakra-zh.blogspot.tw/2012/11/chakra.html)）

[Category:免費軟體](../Category/免費軟體.md "wikilink")
[Category:媒體播放器](../Category/媒體播放器.md "wikilink")
[Category:自由軟體](../Category/自由軟體.md "wikilink")
[Category:跨平台軟體](../Category/跨平台軟體.md "wikilink")
[Category:自由跨平台軟體](../Category/自由跨平台軟體.md "wikilink")
[Category:使用Qt的軟體](../Category/使用Qt的軟體.md "wikilink")
[Category:苹果设计奖获奖作品](../Category/苹果设计奖获奖作品.md "wikilink")
[Category:多媒体框架](../Category/多媒体框架.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.
9.

10.

11.

12.

13.

14.

15. [vlc-for-the-new-windows-8-user-experience-metro](https://www.kickstarter.com/projects/1061646928/vlc-for-the-new-windows-8-user-experience-metro/posts)
    on kickstarter.com

16.