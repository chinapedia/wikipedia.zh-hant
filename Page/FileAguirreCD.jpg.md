This image is being linked here; although the picture is subject to
copyright I (Hal Raglan) feel it is covered by the U.S. fair use laws
for the articles [阿基尔，上帝的愤怒](../Page/阿基尔，上帝的愤怒.md "wikilink") because:

According to the Wikipedia:Fair use tag for music album covers, "It is
believed that the use of low-resolution images of album or single
covers, solely to illustrate the album or single in question, qualifies
as fair use under United States copyright law." The image has been added
to the articles to assist in critical commentary/discussion of the
Aguirre film and music soundtrack. It illustrates articles about the
film and album soundtrack from which the cover illustration was taken.
The image is used as the primary means of visual identification of the
soundtrack album. It is a low resolution image. The image is only a
small portion of the commercial product. It is not replaceable with an
uncopyrighted or freely copyrighted image of comparable educational
value. The use of the cover will not affect the value of the original
work or limit the copyright holder's rights or ability to distribute the
original. The same illustration is used on many other
websites.-[<span style="color: orange">-{冰热海风}-</span>](../Page/User:冰热海风.md "wikilink")([<span style="color: Aqua;">talk</span>](../Page/User_talk:冰热海风.md "wikilink"))