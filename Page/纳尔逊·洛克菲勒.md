**纳尔逊·奥尔德里奇·洛克菲勒**（，），[美国慈善家](../Page/美国.md "wikilink")、商人、政治家，曾任[美国副总统](../Page/美国副总统.md "wikilink")。

## 經歷

他是美國[洛克菲勒家族成員](../Page/洛克菲勒.md "wikilink")，也是[共和黨的溫和派領袖人物之一](../Page/共和黨_\(美國\).md "wikilink")，曾於1959年擔任第49任[紐約州州長](../Page/紐約州州長.md "wikilink")，一直到1973年，以主持大量建設計劃贏得稱譽。

1974年[傑拉爾德·福特接任總統後](../Page/傑拉爾德·福特.md "wikilink")，他獲國會提名為[美國副總統](../Page/美國副總統.md "wikilink")。1975年4月曾代表美國政府赴臺參加前總統蔣中正的喪禮\[1\]。1976年他不獲共和黨提名為副總統候選人，至1977年1月任期屆滿後退休。

1979年1月，因心臟病逝世於[紐約](../Page/紐約.md "wikilink")，享年71歲。

## 後續

1979年1月30日，宋美齡函電[中華民國總統](../Page/中華民國總統.md "wikilink")[蔣經國](../Page/蔣經國.md "wikilink")：「……前副總統洛克菲勒昨晚逝世彼在三年半前來台代表[美國政府弔喪](../Page/美國政府.md "wikilink")-{余}-意應有一團體在台北發起一盛大追悼會另[台北市政府及市議會應命名大通衢為洛副總統路以誌我對其弟大通銀行董事長與](../Page/台北市政府.md "wikilink")『匪』建商之別以誌我必友之此乃我中國傳統之美德也母」\[2\]2月2日，蔣經國函電宋美齡：「……洛克菲勒逝世當日已即馳電慰唁中美文經協會將於近日在台北集會追悼以表示國人只計道義不計其他之傳統美德至於更改路名台北市議會根據民眾廣泛反應久在醞釀取銷[羅斯福路並有以後不再用外國人名字命名道路之決定此事可否暫緩交辦謹聞兒經國跪叩福安二月二日](../Page/羅斯福路.md "wikilink")」\[3\]

## 參考

<references/>

[Category:纽约州州长](../Category/纽约州州长.md "wikilink")
[Category:美國慈善家](../Category/美國慈善家.md "wikilink")
[Category:美國企業家](../Category/美國企業家.md "wikilink")
[Category:達特茅斯學院校友](../Category/達特茅斯學院校友.md "wikilink")
[Category:德國裔美國人](../Category/德國裔美國人.md "wikilink")
[Category:緬因州人](../Category/緬因州人.md "wikilink")
[Category:紐約市人](../Category/紐約市人.md "wikilink")
[Category:洛克斐勒家族](../Category/洛克斐勒家族.md "wikilink")
[Category:共和党美国副总统](../Category/共和党美国副总统.md "wikilink")
[Category:商人出身的政治人物](../Category/商人出身的政治人物.md "wikilink")
[Category:苏格兰－爱尔兰裔美国人](../Category/苏格兰－爱尔兰裔美国人.md "wikilink")

1.
2.  [周美華](../Page/周美華.md "wikilink")、[蕭李居編](../Page/蕭李居.md "wikilink")，《蔣經國書信集——與宋美齡往來函電》(下)，台北「[國史館](../Page/國史館.md "wikilink")」出版，2009年，第9頁
3.  周美華、蕭李居編，《蔣經國書信集——與宋美齡往來函電》(下)，台北「國史館」出版，2009年，第10頁