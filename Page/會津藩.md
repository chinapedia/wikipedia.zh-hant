**會津[藩](../Page/藩.md "wikilink")**（）為[日本古](../Page/日本.md "wikilink")[陸奧國](../Page/陸奧國.md "wikilink")[會津郡](../Page/會津郡.md "wikilink")，範圍包含了現在的[福島縣西部](../Page/福島縣.md "wikilink")[會津地區](../Page/會津.md "wikilink")。當時的藩廳為[會津若松城](../Page/會津若松城.md "wikilink")（今[會津若松市](../Page/會津若松市.md "wikilink")）。

[Fukushima_Aizu_Map.jpg](https://zh.wikipedia.org/wiki/File:Fukushima_Aizu_Map.jpg "fig:Fukushima_Aizu_Map.jpg")，西部連接[新潟縣](../Page/新潟縣.md "wikilink")，西南部連接[群馬縣](../Page/群馬縣.md "wikilink")，南部連接[栃木縣](../Page/栃木縣.md "wikilink")；中間空白處為[豬苗代湖](../Page/豬苗代湖.md "wikilink")。\]\]

## 會津藩主的歷史

### 關原之戰以前

會津地區最早屬於[常陸國的領地](../Page/常陸國.md "wikilink")，從[七世紀以後被劃入成為](../Page/七世紀.md "wikilink")[陸奧國的領地](../Page/陸奧國.md "wikilink")。到了[戰國時代](../Page/戰國_\(日本\).md "wikilink")，由於[平安時代末期](../Page/平安時代.md "wikilink")[鎌倉時代初期的武將](../Page/鎌倉時代.md "wikilink")[佐原義連在](../Page/佐原義連.md "wikilink")[奧州戰爭中有功於](../Page/奧州戰爭.md "wikilink")[源賴朝](../Page/源賴朝.md "wikilink")，被賜予[會津郡](../Page/會津郡.md "wikilink")。

佐原義連原來姓[三蒲氏](../Page/三蒲氏.md "wikilink")，由於居住在[相模國](../Page/相模國.md "wikilink")[衣笠城的東南方佐原地區](../Page/衣笠城.md "wikilink")（現在的[神奈川縣](../Page/神奈川縣.md "wikilink")[横須賀市佐原](../Page/横須賀市.md "wikilink")），就以「佐原氏」自稱（橫須賀市自古為三蒲氏的[根據地](../Page/根據地.md "wikilink")）。義連除了兼任[京都扶持眾之外](../Page/京都扶持眾.md "wikilink")，因為領有會津郡的關係，也就自稱“會津守護”。義連本身並沒有居住在會津郡，而是後代才移居到此。由於今天的橫須賀市自古又稱為「蘆名」，所以義連的子孫自第三代開始才開始以地名自稱為
[蘆名氏](../Page/蘆名氏.md "wikilink")，並以領地作為發展依據，成為戰國[大名之一](../Page/大名_\(稱謂\).md "wikilink")。

原來會津地區有個規模不大的「東黑川館」，[至德元年](../Page/至德.md "wikilink")（1384年）的時候，蘆名氏第七代大名[蘆名直盛將他擴大維修](../Page/蘆名直盛.md "wikilink")，興建一座新的[黑川城](../Page/黑川城.md "wikilink")。天正17年（1589年）時，由於蘆名氏與[伊達政宗爭奪會津地區](../Page/伊達政宗.md "wikilink")，結果敗於[摺上原之戰](../Page/摺上原之戰.md "wikilink")，[蘆名義廣戰敗](../Page/蘆名義廣.md "wikilink")，只好棄黑川城逃往[常陸國](../Page/常陸國.md "wikilink")。然而，在[天正](../Page/天正.md "wikilink")18年（1590年）時[豐臣秀吉以](../Page/豐臣秀吉.md "wikilink")「違反惣無事令」為由，以及推行[奧州仕置的政策](../Page/奧州仕置.md "wikilink")，沒收了政宗在會津一帶的[領地](../Page/領地.md "wikilink")，也包括黑川城。從此黑川城則交由[蒲生氏鄉管理](../Page/蒲生氏鄉.md "wikilink")（氏鄉俸祿42萬石，檢地後增至92萬石）。氏鄉進駐會津後大力擴建黑川城，並且將擴建的黑川城更名為「鶴ヶ城」（「鶴城」的意思），同時從[京都招集](../Page/京都.md "wikilink")[商人等](../Page/商人.md "wikilink")，帶動會津地區的[經濟](../Page/經濟.md "wikilink")，也將領地的經營建立起不錯的成效。

氏鄉死後，在[慶長](../Page/慶長.md "wikilink")3年（1598年）時，其子[蒲生秀行由於年輕](../Page/蒲生秀行.md "wikilink")，加上[氏族中發生騷動而被](../Page/氏族.md "wikilink")[豐臣秀吉移封到](../Page/豐臣秀吉.md "wikilink")[宇都宮](../Page/宇都宮藩.md "wikilink")，同時遭到減俸變成18萬石的處分。因此會津籓主變成從[越後來的](../Page/越後.md "wikilink")[五大老之一](../Page/五大老.md "wikilink")[上杉景勝](../Page/上杉景勝.md "wikilink")（俸祿達120萬石）。不過景勝因為[關原之戰的時候參加](../Page/關原之戰.md "wikilink")[石田三成的西軍](../Page/石田三成.md "wikilink")，其後又敗於伊達氏與最上氏，所以在慶長6年（1601年）時被[德川家康移封到](../Page/德川家康.md "wikilink")[米澤](../Page/米澤藩.md "wikilink")，[俸祿減少到](../Page/俸祿.md "wikilink")30萬石。

### 蒲生時代

慶長6年（1601年），在[關原之戰時支持德川方面的](../Page/關原之戰.md "wikilink")[蒲生秀行因功受賞再次入封會津](../Page/蒲生秀行.md "wikilink")（60萬石）。不過第二代的[蒲生忠鄉在寬永](../Page/蒲生忠鄉.md "wikilink")4年（1627年）時不幸英年早逝，更糟糕的是他無嗣可繼承家業，蒲生氏突然要面對改易的問題。忠鄉的弟弟[蒲生忠知也因為無子嗣](../Page/蒲生忠知.md "wikilink")（僅有7個女兒）而被幕府易封到[伊予國](../Page/伊予國.md "wikilink")[松山藩](../Page/松山藩.md "wikilink")，俸祿被減為24萬石。

### 加藤時代

寬永4年（1627年），[加藤嘉明由於累積了](../Page/加藤嘉明.md "wikilink")[文祿之役](../Page/文祿之役.md "wikilink")、[關原之戰以及](../Page/關原之戰.md "wikilink")[大阪](../Page/大阪.md "wikilink")[夏之陣的戰功而入封會津](../Page/夏之陣.md "wikilink")（俸祿40萬石）。由於慶長16年8月21日（1611年9月27日）發生了震度高達六級的，原來的天守被震垮而傾杞，於是嘉明的兒子加藤明成重新修建，改為今日所見的層塔型天守。儘管明成有魄力，或許有[建築上的天份](../Page/建築.md "wikilink")，但是他也是個相當糟糕的藩主，缺乏凝聚人心的團結力量，結果家臣一致與他鬧對立，發生[會津騷動](../Page/會津騷動.md "wikilink")，甚至有家臣出走，明成還對出走的家臣進行追殺。於是幕府又裁定明成移封到[石見國](../Page/石見國.md "wikilink")[吉永藩](../Page/吉永藩.md "wikilink")（今天的[島根縣](../Page/島根縣.md "wikilink")[大田市](../Page/大田市.md "wikilink")）隱居，並且老死於當地。

### 松平時代

[Aizuwakamatsu_Castle_05.jpg](https://zh.wikipedia.org/wiki/File:Aizuwakamatsu_Castle_05.jpg "fig:Aizuwakamatsu_Castle_05.jpg")\]\]
寬永20年（1643年），加藤氏改易，會津籓由[出羽國](../Page/出羽國.md "wikilink")[山形藩](../Page/山形藩.md "wikilink")，第二代將軍[秀忠的庶子](../Page/德川秀忠.md "wikilink")[保科正之以](../Page/保科正之.md "wikilink")23萬石俸祿入封。保科正之原為德川氏（「松平」氏），幼年被將軍[過繼予](../Page/過繼.md "wikilink")[保科正光](../Page/保科正光.md "wikilink")，由於正之是第三代將軍[家光的同父異母弟](../Page/德川家光.md "wikilink")，個性忠誠低調，因此受到家光極大的信賴與倚重。正之在世時一直不肯歸宗，恢復松平姓氏，因為正之是個忠義的人，保留姓氏以紀念保科氏對他的養育之恩；會津藩一直到第三代藩主正容才歸宗為「松平」氏。以後，保科氏的子孫就以[會津松平家成為固定的家氏與血脈](../Page/會津松平家.md "wikilink")。松平家門第的[家徽也是採用德川家的圓形三葉葵](../Page/家徽.md "wikilink")（「丸に三つ葵」）[1](http://ja.wikipedia.org/wiki/%E7%94%BB%E5%83%8F:Mitsubaaoi2.jpg)，旗號則是採用正體漢字「會」字型號。

### 幕末

最後的藩主的九代[容保](../Page/松平容保.md "wikilink")，是由八代藩主松平容敬的義子，由於容敬是[美濃國](../Page/美濃國.md "wikilink")[高須藩第九代藩主松平義健的親弟](../Page/高須藩.md "wikilink")，加上膝下猶虛，所以才會被[過繼來到會津籓的](../Page/過繼.md "wikilink")。[文久](../Page/文久.md "wikilink")2年（1862年），容保成為[京都守護職](../Page/京都守護職.md "wikilink")，將麾下的[新撰組](../Page/新撰組.md "wikilink")（也是爾後奧州會津戰爭的幕府軍部隊之一）與會津直轄隊一起擔負了取締尊攘派[志士和京都的治安維持](../Page/志士.md "wikilink")。並且在[禁門之變](../Page/禁門之變.md "wikilink")，擊退了打算攻擊[孝明天皇的](../Page/孝明天皇.md "wikilink")[長州藩陣營](../Page/長州藩.md "wikilink")。此後容保受到孝明天皇極度的信任並且賜予「御宸翰」（ごしんかん；日本[天皇的親筆文件](../Page/天皇.md "wikilink")）一篇。

在慶應2年12月（1867年1月30日），[孝明天皇不幸駕崩](../Page/孝明天皇.md "wikilink")（有一說是遭到暗殺），失去天皇支持的[江戶幕府與跟已經簽訂](../Page/江戶幕府.md "wikilink")[薩長同盟的](../Page/薩長同盟.md "wikilink")[薩摩藩與](../Page/薩摩藩.md "wikilink")[長州藩的對立白熱化](../Page/長州藩.md "wikilink")。隨著[大政奉還以及](../Page/大政奉還.md "wikilink")[王政復古兩次政治事件之後](../Page/王政復古.md "wikilink")，[戊辰戰爭的首戰](../Page/戊辰戰爭.md "wikilink")[鳥羽伏見之戰爆發了](../Page/鳥羽伏見之戰.md "wikilink")。會津藩很自然地被看作舊幕府勢力中心之一，成為了新政府軍的仇敵。鳥羽伏見之戰後，[明治天皇更加倚賴新政府軍](../Page/明治天皇.md "wikilink")。不過會津藩以[奧羽越列藩同盟的方式獲得其他籓主人力物力方面的支援](../Page/奧羽越列藩同盟.md "wikilink")，作為抵抗新政府軍的條件。不過，在長達一個月的[會津若松城的戰鬥中](../Page/會津若松城.md "wikilink")（[會津戰爭](../Page/會津戰爭.md "wikilink")），會津藩還是敗北投降了。

投降之後，會津松平氏在會津藩的領地被明治政府沒收了。藩主容保美其名被移封到[鳥取藩](../Page/鳥取藩.md "wikilink")，實則遭到軟禁。明治2年（1869年），明治政府容許容保的嫡子[容大繼續保有氏名](../Page/松平容大.md "wikilink")，但是不得再居住在若松城中，加上一年前舊領地豬苗代城已經遭到焚毀，因此藩士們只好選擇帶著九個月大的容大移居到陸奧國斗南藩安身立命（今[青森縣](../Page/青森縣.md "wikilink")[陸奧市](../Page/陸奧市.md "wikilink")）。另一方面，會津地方[廢藩置縣前](../Page/廢藩置縣.md "wikilink")，成為了由於明治政府文官管理政治局的直轄地，此後雖然成為了[若松縣](../Page/若松縣.md "wikilink")，不過在明治9年（1876年）被[福島縣合併](../Page/福島縣.md "wikilink")。

### 现代

[明治维新后日本政局长期由](../Page/明治维新.md "wikilink")[长州藩等南方出身者把持](../Page/长州藩.md "wikilink")，會津藩作为维新时期的保守派在政治和经济上遭受打压（例如虽然[會津若松是交通要冲](../Page/會津若松.md "wikilink")，但至今没有[新干线和建造新干线的计划](../Page/新干线.md "wikilink")），致使经济落后于周围地区，当地居民对此颇有微词。2011年3月11日[東日本大震災后](../Page/東日本大震災.md "wikilink")[福島縣才被重新重視](../Page/福島縣.md "wikilink")，爲了激勵當地居民重建家園2013年的[大河劇改为](../Page/大河劇.md "wikilink")《[八重之櫻](../Page/八重之櫻.md "wikilink")》，主角是會津藩出身的[新島八重](../Page/新島八重.md "wikilink")。

## 歴代藩主

[Wakamatsujo_the_marks_of_all_Daimyōs.jpg](https://zh.wikipedia.org/wiki/File:Wakamatsujo_the_marks_of_all_Daimyōs.jpg "fig:Wakamatsujo_the_marks_of_all_Daimyōs.jpg")

### 近江蒲生氏

[外様](../Page/外様.md "wikilink")　91万9千石（1590年－1598年）

1.  [氏鄉](../Page/蒲生氏鄉.md "wikilink")〔正四位下・飛驒守、左近衛少將〕（1590年－1595年）
2.  [秀行](../Page/蒲生秀行.md "wikilink")〔從三位・飛驒守、参議〕（1595年－1598年）

### 上杉氏或山内上杉家

外様　120万石（1598年－1601年）

1.  [景勝](../Page/上杉景勝.md "wikilink")〔從三位・中納言、彈正大弼、参議〕

### 蒲生氏

再封　60万石（1601年－1627年）

1.  [秀行](../Page/蒲生秀行.md "wikilink")〔從三位・飛驒守、参議〕
2.  [忠鄉](../Page/蒲生忠鄉.md "wikilink")〔從三位・下野守、参議〕

### 加藤氏（利仁流）

外様　40万石（1627年－1643年）

1.  [嘉明](../Page/加藤嘉明.md "wikilink")〔從四位下・左馬頭〕
2.  [明成](../Page/加藤明成.md "wikilink")〔從四位下・式部少補、侍從〕

### 会津松平家〔保科氏〕

親藩　23万石（1643年－1868年）

1.  [正之](../Page/保科正之.md "wikilink")〔正四位下・肥後守、左近衛中將〕
2.  [正經](../Page/保科正經.md "wikilink")〔從四位下・筑前守〕
3.  [正容](../Page/松平正容.md "wikilink")〔正四位下・肥後守、左近衛中將〕；從此歸宗回到**松平**姓氏。
4.  [容貞](../Page/松平容貞.md "wikilink")〔從四位下・肥後守、左少將〕
5.  [容頌](../Page/松平容頌.md "wikilink")〔正四位下・肥後守、左中將〕
6.  [容住](../Page/松平容住.md "wikilink")〔從四位下・肥後守〕
7.  [容衆](../Page/松平容衆.md "wikilink")〔從四位下・肥後守、左少將〕
8.  [容敬](../Page/松平容敬.md "wikilink")〔正四位下・肥後守、左中將〕
9.  [容保](../Page/松平容保.md "wikilink")〔正三位・肥後守、參議〕
10. [喜德](../Page/松平喜德.md "wikilink")〔從四位下・若狹守〕

## 斗南藩

會津藩還有另一個關係甚深的藩領是**斗南藩**；明治2年（1869年）容保的嫡子容大因為被明治政府容許繼續保存姓氏，因此在翌年（1870年）設置成立斗南藩（「斗南」出自「北斗以南皆帝州」乙句）。明治政府在這一點表面上表現出懷柔，實際上卻有著陰險；由於會津松平家在會津藩的資產都被明治政府沒收了，以避免松平家有不滿政府的藩士又將「佐幕」化為行動，於是明治政府將松平容保移封到鳥取藩，擔任家督年僅13歲的養子喜德被遷移到[久留米藩](../Page/久留米藩.md "wikilink")；明治政府另外給予松平家兩條遷居上的選擇：一個是回到舊領地之一的猪苗代（今天的[猪苗代町](../Page/猪苗代町.md "wikilink")），或者遷移到斗南藩（現在的[青森縣](../Page/青森縣.md "wikilink")[陸奧市](../Page/陸奧市.md "wikilink")）。

原則上回到豬苗代是個不錯的選擇，距離若松城只有一湖之遙（[豬苗代湖](../Page/豬苗代湖.md "wikilink")）。但是在[慶應](../Page/慶應.md "wikilink")4年（1868年）戊辰戦争之際、於母成峠之戦中，西軍（也就是[薩摩藩與](../Page/薩摩藩.md "wikilink")[長州藩同盟](../Page/長州藩.md "wikilink")）擊破東軍（[会津藩與](../Page/会津藩.md "wikilink")[新撰組聯軍](../Page/新撰組.md "wikilink")），會津領地遭到入侵，當時的[城代高橋權大夫就把豬苗代城](../Page/城代.md "wikilink")（也稱為「龜城」）一把火給燒了，然後向若松城撤退，城裡的建築物在大火中最後十去八九，僅存廢墟；火燒猪苗代城雖然不僅僅結束了戰役，也終結了一年後松平家移居的機會。

因此藩士們議論紛紛後決定前往更北方的斗南藩。同年4月18日，從南部移居的第一波兩百多名人士在八戶登陸了。成為斗南藩主的松平容大當時才九個月大，被藩士[冨田重光抱在懷裡](../Page/冨田重光.md "wikilink")，乘坐轎子，後面跟著五戶人家出發了。

現在的[下北半島陸奧市的圓通寺是原來斗南藩廳的暫時辦公處](../Page/下北半島.md "wikilink")。而北海道的[歌棄郡](../Page/歌棄郡.md "wikilink")、[瀨棚郡](../Page/瀨棚郡.md "wikilink")、[太櫓郡以及](../Page/太櫓郡.md "wikilink")[山越郡](../Page/山越郡.md "wikilink")4個郡成為斗南藩的支配地。實際上真正落腳大約50多戶，不過220多人左右。順便一提原來明治政府給予斗南藩是3萬石，但是到了當地才知道藩領是不毛的貧瘠地，所以俸祿只不過是大約7千石而已。

雖然說[森林豐富](../Page/森林.md "wikilink")，但是無法像其他藩國那樣有效發展[林業](../Page/林業.md "wikilink")。當地的生活實在非常辛苦，移居的藩士中不幸因為寒冷而失去性命的人也不少，存活下來的人在苛刻的天然條件中過著饑餓的生活。

後來斗南藩在明治4年（1871年）的廢藩置縣政策中規劃成為斗南縣，同年9月斗南縣與弘前縣、黑石縣、七戶縣、八戶縣等上述藩縣合併為弘前縣，後隨即將縣廳移至青森，改稱青森縣。同時，作為其中一部份的二戶郡，被[岩手縣編入了](../Page/岩手縣.md "wikilink")。容大在明治17年（1884年）成為[子爵](../Page/子爵.md "wikilink")，被列在[華族中](../Page/華族.md "wikilink")。

  - 藩主：[松平容大](../Page/松平容大.md "wikilink")〔從五位　藩知事〕

## 相關條目

  - [會津若松城](../Page/會津若松城.md "wikilink")
  - [藩列表](../Page/藩列表.md "wikilink")
  - [白虎隊](../Page/白虎隊.md "wikilink")
  - [雍仁親王妃勢津子](../Page/雍仁親王妃勢津子.md "wikilink")
  - [大東流](../Page/大東流.md "wikilink")
  - [小原庄助](../Page/小原庄助.md "wikilink")

## 参考文献

  - [星亮一著](../Page/星亮一.md "wikilink")『幕末の会津藩——運命を決めた上洛』[中央公論新社](../Page/中央公論新社.md "wikilink")［中公新書］。ISBN
    412101619X

## 外部連結

  - [会津藩の総合サイト：「幕末会津藩」](https://web.archive.org/web/20060110193319/http://bakuai.main.jp/)

## 相关条目

  - [藩列表](../Page/藩列表.md "wikilink")

[Category:藩](../Category/藩.md "wikilink")
[\*](../Category/蒲生氏.md "wikilink")
[\*](../Category/保科氏.md "wikilink")
[Category:會津藩](../Category/會津藩.md "wikilink")
[Category:陸奧國](../Category/陸奧國.md "wikilink")
[Category:米澤上杉氏](../Category/米澤上杉氏.md "wikilink")
[Category:三河加藤氏](../Category/三河加藤氏.md "wikilink")
[Category:會津松平家](../Category/會津松平家.md "wikilink")