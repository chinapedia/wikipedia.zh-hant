**PyMOL**是一个[开放源码](../Page/开放源码.md "wikilink")，由使用者贊助的分子三维结构显示[软件](../Page/软件.md "wikilink")。由[Warren
Lyford DeLano编写](../Page/w:Warren_Lyford_DeLano.md "wikilink")，並且由DeLano
Scientific LLC將它商業化。DeLano Scientific
LLC是一個私人的軟體公司，它致力於創造讓普遍的科學與教育社群都能取得的好用軟體工具。

PyMOL适用于创作高品質的小分子或是[生物大分子](../Page/生物大分子.md "wikilink")（特别是[蛋白質](../Page/蛋白質.md "wikilink")）的三维结构圖像。軟體的作者宣称，在所有正式发表的科學文獻中的蛋白質結構圖像中，有四分之一是使用PyMOL來製作。

PyMOL是少數可以用在[結構生物学领域的开放源代码視覺化工具](../Page/結構生物学.md "wikilink")。

軟體以**Py**+**MOL**命名：“Py”表示它是由一种计算机语言[Python所衍生出來的](../Page/Python.md "wikilink")，“MOL”表示它是用于显示[分子](../Page/分子.md "wikilink")（英文为molecule）结构的软件。

## 商业化举措

自2006年8月1日起，DeLano Scientific對於事先編譯過的PyMOL執行程式（包括beta版）的取得採取限定下載的措施。
目前，完全无限制的可執行程序只有付費用戶可以取得。学生和教师可以通过一项声明来取得教育版PyMOL以用于教学和其它受限制的领域。用户还可以在一定时间内免费使用评估版本，以便于决定是否购买该软件。

PyMOL的源代碼目前仍可以免費下载，供使用者编译。對於[Linux](../Page/Linux.md "wikilink")、[Unix以及](../Page/Unix.md "wikilink")[Mac
OS
X等](../Page/Mac_OS_X.md "wikilink")[操作系统](../Page/操作系统.md "wikilink")，非付费用户可以通过自行编译源代碼来获得PyMOL執行程式；而对于[Windows的使用者](../Page/Windows.md "wikilink")，如果不安装第三方软件，则无法编译源代碼。這表示使用[Windows的用户取得新版PyMOL](../Page/Windows.md "wikilink")（PyMOL1.0及之后的版本）執行程式的方式就是成為付費會員，或者使用第三方软件（如[Cygwin](../Page/Cygwin.md "wikilink")）进行编译\[1\]。

## 类似软件

  - [VMD](../Page/w:Visual_Molecular_Dynamics.md "wikilink")
  - [Gabedit](../Page/w:Gabedit.md "wikilink")
  - [Molden](../Page/w:Molden.md "wikilink")
  - [Moleke](../Page/w:Molekel.md "wikilink")
  - [Molecular modelling](../Page/w:Molecular_modelling.md "wikilink")
  - [Jmol](../Page/w:Jmol.md "wikilink")
  - [RasMol](../Page/w:RasMol.md "wikilink")
  - [WHAT IF software](../Page/w:WHAT_IF_software.md "wikilink")
  - [Cn3D](../Page/w:Cn3D.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

  - [PyMOL主页](http://pymol.sourceforge.net/)
  - [PyMOL Wiki](http://www.pymolwiki.org/index.php/Main_Page)
  - [DeLano Scientific LLC](http://www.delanoscientific.com/)
  - [Protein **alignment** program coupled with
    PyMOL](http://3d-alignment.eu)
  - [Molecular movie-making with
    PyMOL](https://web.archive.org/web/20071025054743/http://www.weizmann.ac.il/ISPC/eMovie.html)

[Category:分子模擬軟體](../Category/分子模擬軟體.md "wikilink")
[Category:生物資訊軟體](../Category/生物資訊軟體.md "wikilink")
[Category:化學軟件](../Category/化學軟件.md "wikilink")

1.