[Tyler_Hansbrough.jpg](https://zh.wikipedia.org/wiki/File:Tyler_Hansbrough.jpg "fig:Tyler_Hansbrough.jpg")
**安德鲁·泰勒·汉斯布鲁**（，），[美国](../Page/美国.md "wikilink")[NBA](../Page/NBA.md "wikilink")[篮球运动员](../Page/篮球.md "wikilink")，曾效力于[ACC联盟的](../Page/ACC.md "wikilink")[北卡罗来纳大学教堂山分校](../Page/北卡罗来纳大学教堂山分校.md "wikilink")。汉斯布鲁身高2米06，体重111公斤，场上位置[大前锋](../Page/大前锋.md "wikilink")。

## 大学生涯

他是ACC联盟2006年，2007年和2008年无可争议的全明星队的成员，2006年最佳新秀，2008年最佳球员。2008年，泰勒席卷了几乎所有可以拿到的大学篮球个人荣誉。为了他的50号球衣可以退役，他需要等到他退役之后，并且获得一次以下六个全国性最佳球员的奖项：[美联社](../Page/美联社.md "wikilink")，美国篮球记者协会，全国篮球教练协会，体育新闻杂志，[奈史密斯学院年度最佳球员和](../Page/奈史密斯学院年度最佳球员.md "wikilink")[约翰伍登奖](../Page/约翰伍登奖.md "wikilink")。
而他却得到了所有六座奖杯。因此当他大学四年毕业后，他身穿的50号球衣将从[北卡大学篮球队退役](../Page/北卡大学.md "wikilink")。

他在2009年的[NBA選秀中第](../Page/NBA選秀.md "wikilink")1輪第13順位被[印第安納溜馬選中](../Page/印第安納溜馬.md "wikilink")。

## 参见

  - [北卡罗来纳柏油脚跟男子篮球队](../Page/北卡罗来纳柏油脚跟男子篮球队.md "wikilink")

## 参考资料

## 外部链接

  - [University of North Carolina player biography of Tyler
    Hansbrough](http://tarheelblue.collegesports.com/sports/m-baskbl/mtt/hansbrough_tyler00.html)
  - [Recruiting profile of Tyler Hansbrough by
    Scout.com](http://scout.scout.com/a.z?s=78&p=8&c=1&nid=722801)
  - [Sports Illustrated Blog interview with
    Hansbrough](http://sportsillustrated.cnn.com/si_blogs/basketball/ncaa/2006/08/blog-qa-with-uncs-tyler-hansbrough.html)
  - [Article of Hansbrough training for 2006-07
    season](http://tarheelblue.cstv.com/sports/m-baskbl/spec-rel/110906aaa.html)
  - [Sports Illustrated Article about Hansbrough being player of the
    year](http://sportsillustrated.cnn.com/2008/writers/grant_wahl/03/04/march.madman0310/index.html)
  - [Riverfront Times article on Hansbrough brothers from
    Fall 2004](http://www.riverfronttimes.com/2004-09-01/news/tallboys)

[Category:美国男子篮球运动员](../Category/美国男子篮球运动员.md "wikilink")
[Category:夏洛特黃蜂隊球員](../Category/夏洛特黃蜂隊球員.md "wikilink")
[Category:印第安纳步行者队球员](../Category/印第安纳步行者队球员.md "wikilink")
[Category:多伦多猛龙队球员](../Category/多伦多猛龙队球员.md "wikilink")