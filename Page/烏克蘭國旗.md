**烏克蘭國旗**
，（）長方形，長：寬=3:2。最早是[烏克蘭人民共和國的國旗](../Page/烏克蘭人民共和國.md "wikilink")，但在蘇聯時代被禁。1992年1月28日重新立法成為國旗。

[UkraineFlag.png](https://zh.wikipedia.org/wiki/File:UkraineFlag.png "fig:UkraineFlag.png")

國旗含義：藍色代表天空與海洋，象徵自由與主權；黃色代表麥田，象徵烏克蘭悠久的農業歷史。黃藍二色為[烏克蘭的傳統顏色](../Page/烏克蘭.md "wikilink")，源自该国藍盾黃獅徽章

根據《烏克蘭憲法》第20條規定：

  -

    *烏克蘭國旗是一面由面積相等的藍黃兩色橫條組成的旗幟。*

## 历代國旗

| 國旗                                                                                                                                                                                                                                         | 國家                                                   | 使用              |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ---------------------------------------------------- | --------------- |
| [Flag_of_Ukrainian_People's_Republic.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Ukrainian_People's_Republic.svg "fig:Flag_of_Ukrainian_People's_Republic.svg")                                                                    | [烏克蘭人民共和國](../Page/烏克蘭人民共和國.md "wikilink")           | 1917-1918；比例2:3 |
| [Flag_of_Ukrainian_People's_Republic_1917.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Ukrainian_People's_Republic_1917.svg "fig:Flag_of_Ukrainian_People's_Republic_1917.svg")                                                    | [烏克蘭人民共和國](../Page/烏克蘭人民共和國.md "wikilink")           | 1918—1920；比例2:3 |
| [Flag_of_Ukraine.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Ukraine.svg "fig:Flag_of_Ukraine.svg")                                                                                                                                  | [西烏克蘭人民共和國](../Page/西烏克蘭人民共和國.md "wikilink")         | 1918-1919；比例2:3 |
| [Flag_of_Ukraine.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Ukraine.svg "fig:Flag_of_Ukraine.svg")                                                                                                                                  | [烏克蘭國](../Page/烏克蘭國.md "wikilink")                   | 1918；比例2:3      |
| [Flag_of_Ukrainian_People's_Republic_(non-official,_1917).svg](https://zh.wikipedia.org/wiki/File:Flag_of_Ukrainian_People's_Republic_\(non-official,_1917\).svg "fig:Flag_of_Ukrainian_People's_Republic_(non-official,_1917).svg") | [烏克蘭人民共和國](../Page/烏克蘭人民共和國.md "wikilink")           | 1917-1918；比例2:3 |
| [Flag_of_Ukrainian_People's_Republic_of_the_Soviets.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Ukrainian_People's_Republic_of_the_Soviets.svg "fig:Flag_of_Ukrainian_People's_Republic_of_the_Soviets.svg")                    | 烏克蘭苏维埃人民共和國                                          | 1917–1918；比例2:3 |
| [Flag_of_the_Ukrainian_SSR_(1923-1927).svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_Ukrainian_SSR_\(1923-1927\).svg "fig:Flag_of_the_Ukrainian_SSR_(1923-1927).svg")                                                           | [烏克蘭苏维埃社会主义共和國](../Page/烏克蘭苏维埃社会主义共和國.md "wikilink") | 1922-1927；比例1:2 |
| [Flag_of_the_Ukrainian_SSR_(1927-1937).svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_Ukrainian_SSR_\(1927-1937\).svg "fig:Flag_of_the_Ukrainian_SSR_(1927-1937).svg")                                                           | [烏克蘭蘇維埃社會主義共和國](../Page/烏克蘭蘇維埃社會主義共和國.md "wikilink") | 1927-1937；比例1:2 |
| [Flag_of_Ukrainian_SSR_(1937-1949).svg](https://zh.wikipedia.org/wiki/File:Flag_of_Ukrainian_SSR_\(1937-1949\).svg "fig:Flag_of_Ukrainian_SSR_(1937-1949).svg")                                                                        | [烏克蘭蘇維埃社會主義共和國](../Page/烏克蘭蘇維埃社會主義共和國.md "wikilink") | 1937-1949；比例1:2 |
| [Flag_of_the_Ukrainian_Soviet_Socialist_Republic.svg](https://zh.wikipedia.org/wiki/File:Flag_of_the_Ukrainian_Soviet_Socialist_Republic.svg "fig:Flag_of_the_Ukrainian_Soviet_Socialist_Republic.svg")                              | [烏克蘭蘇維埃社會主義共和國](../Page/烏克蘭蘇維埃社會主義共和國.md "wikilink") | 1949-1991；比例1:2 |
| [Flag_of_Ukraine.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Ukraine.svg "fig:Flag_of_Ukraine.svg")                                                                                                                                  | [烏克蘭](../Page/烏克蘭.md "wikilink")                     | 1991 -；比例2:3    |

### 烏克蘭獨立後國旗時間軸

<table>
<tbody>
<tr class="odd">
<td><table>
<tbody>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Ukrainian_People&#39;s_Republic.svg" title="fig:Flag_of_Ukrainian_People&#39;s_Republic.svg">Flag_of_Ukrainian_People's_Republic.svg</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Ukrainian_People&#39;s_Republic_(non-official,_1917).svg" title="fig:Flag_of_Ukrainian_People&#39;s_Republic_(non-official,_1917).svg">Flag_of_Ukrainian_People's_Republic_(non-official,_1917).svg</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Ukrainian_People&#39;s_Republic_of_the_Soviets.svg" title="fig:Flag_of_Ukrainian_People&#39;s_Republic_of_the_Soviets.svg">Flag_of_Ukrainian_People's_Republic_of_the_Soviets.svg</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_the_Ukrainian_SSR_(1923-1927).svg" title="fig:Flag_of_the_Ukrainian_SSR_(1923-1927).svg">Flag_of_the_Ukrainian_SSR_(1923-1927).svg</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Ukrainian_SSR_(1937-1949).svg" title="fig:Flag_of_Ukrainian_SSR_(1937-1949).svg">Flag_of_Ukrainian_SSR_(1937-1949).svg</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_the_Ukrainian_Soviet_Socialist_Republic.svg" title="fig:Flag_of_the_Ukrainian_Soviet_Socialist_Republic.svg">Flag_of_the_Ukrainian_Soviet_Socialist_Republic.svg</a></p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Ukraine.svg" title="fig:Flag_of_Ukraine.svg">Flag_of_Ukraine.svg</a></p></td>
</tr>
<tr class="even">
<td><p>1917–1920</p></td>
<td><p>1917–1918</p></td>
<td><p>1917–1918</p></td>
<td><p>1922–1937</p></td>
<td><p>1937–1949</p></td>
<td><p>1949–1991</p></td>
<td><p>1991-至今</p></td>
</tr>
</tbody>
</table></td>
</tr>
</tbody>
</table>

## 其他旗帜

[W](../Category/國旗.md "wikilink")
[Category:乌克兰国家象征](../Category/乌克兰国家象征.md "wikilink")
[Category:乌克兰旗帜](../Category/乌克兰旗帜.md "wikilink")