**德川宗堯**（），常陸[水戶藩的第四代](../Page/水戶藩.md "wikilink")[藩主](../Page/藩主.md "wikilink")，[水戶德川家第四代](../Page/水戶德川家.md "wikilink")[當主](../Page/當主.md "wikilink")。父親是[水戶支藩](../Page/水戶.md "wikilink")[高松藩](../Page/高松藩.md "wikilink")[藩主](../Page/藩主.md "wikilink")[松平賴豐](../Page/松平賴豐.md "wikilink")，母親是湯淺氏，宗堯是長子，[入贅](../Page/入贅.md "wikilink")[水戶德川家](../Page/水戶德川家.md "wikilink")。正室是德川吉孚的女兒美代姬，側室岡崎氏。官職是從三位左近衛權中將、參議。

## 生涯

寶永2年（1705年）7月11日出生，乳名是輕麻呂。正德元年（1711年），輕麻呂的父親的伯父，水戶藩藩主[德川綱條將孫女美代姬許配給輕麻呂](../Page/德川綱條.md "wikilink")，輕麻呂成為綱條的[婿養子](../Page/婿養子.md "wikilink")，乳名改為鶴千代。享保元年（1716年）將軍[德川吉宗賜予](../Page/德川吉宗.md "wikilink")「宗」一字，改名為宗堯。宗堯少年時代是個英明傑出的人物，享保3年（1718年）綱條去世，宗堯繼任成為藩主。

宗堯也十分的盡力重整藩中財政。不過，享保15年（1730年）4月7日宗堯以二十六歲的年齡早逝，改革因此以失敗結束。藩主之位由宗堯的兒子，年僅二歲的[德川宗翰繼承](../Page/德川宗翰.md "wikilink")。

宗堯是個極有才能的出色文人，甚至被稱讚為[德川光圀再世](../Page/德川光圀.md "wikilink")。著有「成公文集」。

墓地:茨城縣常陸太田市瑞龍山。

## 家系

  - 父親：[松平賴豐](../Page/松平賴豐.md "wikilink")
  - 母親：湯淺氏

### 姊妹

  - 清玉院
  - 松平春姬
  - 松平友姬
  - 女子
  - 女子

### 妻妾

  - 正室：美代姬（德川氏，泰俊院、長松院）
  - 側室：岡島氏

### 子

  - 長子：[德川宗翰](../Page/德川宗翰.md "wikilink")
  - 次子：松平賴順（實為長子，但由於是庶出子，故列為次子）

### 養女

  - 養女：（從一位[關白](../Page/關白.md "wikilink")[近衛內前室](../Page/近衛內前.md "wikilink")，實父[松平賴豐](../Page/松平賴豐.md "wikilink")）
  - 養女：猗（[陸奧國](../Page/陸奧國.md "wikilink")[守山藩第三代藩主](../Page/守山藩.md "wikilink")[松平賴亮室](../Page/松平賴亮.md "wikilink")，實父[松平賴順](../Page/松平賴順.md "wikilink")）

[Category:水戶德川家](../Category/水戶德川家.md "wikilink")
[Category:高松松平家](../Category/高松松平家.md "wikilink")
[Category:水戶藩](../Category/水戶藩.md "wikilink")
[Category:親藩大名](../Category/親藩大名.md "wikilink")