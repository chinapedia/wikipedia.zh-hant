****（Melete）是第56颗被人类发现的[小行星](../Page/小行星.md "wikilink")，于1857年9月9日发现。的[直径为](../Page/直径.md "wikilink")113.2千米，[质量为](../Page/质量.md "wikilink")1.5×10<sup>18</sup>千克，[公转周期为](../Page/公转周期.md "wikilink")1526.839天。

## 參考文獻

[Category:小行星带天体](../Category/小行星带天体.md "wikilink")
[Category:1857年发现的小行星](../Category/1857年发现的小行星.md "wikilink")