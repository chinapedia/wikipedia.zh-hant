**人吉市**（）是位於[日本](../Page/日本.md "wikilink")[熊本縣南部的](../Page/熊本縣.md "wikilink")[城市](../Page/城市.md "wikilink")，地處[九州山地內的](../Page/九州山地.md "wikilink")[人吉盆地](../Page/人吉盆地.md "wikilink")。主要產業包括觀光業、農業、釀酒業。

過去是熊本、宮崎、鹿兒島之間的交通要衝，「人吉」的名稱即是起源於來往的人們須在此住宿的「舍」\[1\]，但隨著快速道路以及新幹線的通車，使得人們不須在停留在此第，逐漸人潮減少。

## 歷史

「人吉」的地名最早出現在[和名抄](../Page/和名抄.md "wikilink")，其中記載[球磨郡下轄六鄉](../Page/球磨郡.md "wikilink")，人吉即為其中之一。\[2\]

自[鎌倉時代初期的](../Page/鎌倉時代.md "wikilink")1193年以，人吉地區開始由來自遠州（現在的[靜岡縣](../Page/靜岡縣.md "wikilink")）[相良氏統治](../Page/相良氏.md "wikilink")，直到[明治時代實施](../Page/明治時代.md "wikilink")[廢藩置縣為止](../Page/廢藩置縣.md "wikilink")。廢藩置縣後，一度設置為[人吉縣](../Page/人吉縣.md "wikilink")，後又先後改隸屬[八代縣和](../Page/八代縣.md "wikilink")[白川縣](../Page/白川縣.md "wikilink")，最後成為[熊本縣的一個城市](../Page/熊本縣.md "wikilink")。

### 年表

  - 1889年4月1日：實施[町村制](../Page/町村制.md "wikilink")，現在的轄區在當時分屬：球磨郡人吉町、西瀨村、中原村、藍田村、大村。
  - 1933年4月1日：大村被併入人吉町。
  - 1942年2月11日：人吉町、西瀨村、中原村和藍田村[合併為](../Page/市町村合併.md "wikilink")**人吉市**。\[3\]

### 變遷表

<table>
<thead>
<tr class="header">
<th><p>1889年以前</p></th>
<th><p>1889年4月1日</p></th>
<th><p>1889年 - 1944年</p></th>
<th><p>1945年 - 1954年</p></th>
<th><p>1955年 - 1964年</p></th>
<th><p>1965年 - 1988年</p></th>
<th><p>1989年 - 現在</p></th>
<th><p>現在</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>人吉町</p></td>
<td><p>1932年2月11日<br />
人吉市</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>大村</p></td>
<td><p>1933年4月1日<br />
併入人吉町</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>藍田村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>西瀬村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>中原村</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 交通

### 鐵路

  - [九州旅客鐵道](../Page/九州旅客鐵道.md "wikilink")
      - [肥薩線](../Page/肥薩線.md "wikilink")：[西人吉車站](../Page/西人吉車站.md "wikilink")
        - [人吉車站](../Page/人吉車站.md "wikilink") -
        [大畑車站](../Page/大畑車站.md "wikilink") -
        [矢岳車站](../Page/矢岳車站.md "wikilink")
  - [球磨川鐵道](../Page/球磨川鐵道.md "wikilink")
      - [湯前線](../Page/球磨川鐵道湯前線.md "wikilink")：人吉車站 -
        [相良藩願成寺車站](../Page/相良藩願成寺車站.md "wikilink")

### 道路

  - 高速道路

<!-- end list -->

  - [九州自動車道](../Page/九州自動車道.md "wikilink")：[人吉交流道](../Page/人吉交流道.md "wikilink")

## 觀光資源

[Hitoyoshi_Castle_air.jpg](https://zh.wikipedia.org/wiki/File:Hitoyoshi_Castle_air.jpg "fig:Hitoyoshi_Castle_air.jpg")
[Hitoyoshijou001.jpg](https://zh.wikipedia.org/wiki/File:Hitoyoshijou001.jpg "fig:Hitoyoshijou001.jpg")河畔的[人吉城三之丸](../Page/人吉城.md "wikilink")[石牆遺跡](../Page/石牆.md "wikilink")\]\]

### 景點

  - [人吉溫泉](../Page/人吉溫泉.md "wikilink")
  - [鹿目瀑布](../Page/鹿目瀑布.md "wikilink")：[日本瀑布百選之一](../Page/日本瀑布百選.md "wikilink")
  - [人吉城遺跡](../Page/人吉城.md "wikilink")：相良氏前後共35代的居城。
  - [大村橫穴古墳群](../Page/大村橫穴古墳群.md "wikilink")
  - [大畑梅園](../Page/大畑梅園.md "wikilink")

### 祭典、活動

  - 人吉球磨はひな祭：每年2月1日至3月31日期間舉行
  - 梅祭：3月舉行
  - 人吉溫泉球磨燒酎祭：5月舉行
  - 人吉花火大會：8月舉行
  - 鹿目瀑布祭：8月舉行
  - おくんち祭（青井阿蘇神社例祭）：10月舉行

## 教育

### 高等學校

  - [熊本縣立人吉高等學校](../Page/熊本縣立人吉高等學校.md "wikilink")
  - [熊本縣立球磨工業高等學校](../Page/熊本縣立球磨工業高等學校.md "wikilink")

## 本地出身之名人

  - [日野熊藏](../Page/日野熊藏.md "wikilink")：[日本帝國陸軍軍人](../Page/日本帝國陸軍.md "wikilink")
  - [高木惣吉](../Page/高木惣吉.md "wikilink")：[日本帝國海軍軍人](../Page/日本帝國海軍.md "wikilink")
  - [犬童球溪](../Page/犬童球溪.md "wikilink")：[作詞家](../Page/作詞家.md "wikilink")
  - [とり・みき](../Page/とり・みき.md "wikilink")：[漫畫家](../Page/漫畫家.md "wikilink")
  - [轟悠](../Page/轟悠.md "wikilink")：[寶塚歌劇團理事](../Page/寶塚歌劇團.md "wikilink")
  - [川上哲治](../Page/川上哲治.md "wikilink")：[職業棒球選手](../Page/職業棒球.md "wikilink")
  - [末次利光](../Page/末次利光.md "wikilink")：職業棒球選手

## 參考資料

## 外部連結

  - [人吉市](https://www.city.hitoyoshi.lg.jp/)

  - [人吉物産振興協會](http://www.hitoyoshi-bussan.jp/)

  - [人吉球磨Clean
    Plaza](https://web.archive.org/web/20090423080747/http://www.cleanplaza.jp/)

  -
<!-- end list -->

1.

2.
3.