<table style="width:10%;">
<colgroup>
<col style="width: 1%" />
<col style="width: 8%" />
</colgroup>
<thead>
<tr class="header">
<th><p>描述摘要</p></th>
<th><p>魔戒戰略遊戲裡出現的克哈穆爾。</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>來源</p></td>
<td><p><a href="http://en.wikipedia.org/wiki/Image:Khamul_lotrsbg.jpg">http://en.wikipedia.org/wiki/Image:Khamul_lotrsbg.jpg</a></p></td>
</tr>
<tr class="even">
<td><p>日期</p></td>
<td><p>6/9/2006</p></td>
</tr>
<tr class="odd">
<td><p>作者</p></td>
<td><p>Major Despard</p></td>
</tr>
<tr class="even">
<td><p>許可</p></td>
<td><p>{{#switch: 檔案其他版本（可留空）</p></td>
</tr>
</tbody>
</table>

## 合理使用

  - 本圖片是從[遊戲工場官方網站](http://us.games-workshop.com/games/lotr/catalog/easterlings/khamul.htm)而來的，作為描述遊戲工場（Games
    Workshop）的產品，這是合理使用的。
  - 本圖片已被縮小，出自遊戲工場，維基百科是非商營的網站，相信這是合理使用的圖像。[1](http://uk.games-workshop.com/legal/canandcant/1/)
  - 本圖僅用以說明遊戲工場出產的一款模型，該模型是依據托爾金原著及電影《魔戒》系列。
  - 沒有其他自由版權及公有領域圖片可代替本圖。