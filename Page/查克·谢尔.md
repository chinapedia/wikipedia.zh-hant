**查克·謝尔**（，），生于[俄亥俄州亚克朗](../Page/俄亥俄州.md "wikilink")，美國职业籃球員。他是[NBA歷史上第一位](../Page/NBA.md "wikilink")[選秀狀元](../Page/NBA選秀狀元.md "wikilink")（之前为BAA）。

## NBA生涯

谢尔在1950年被[波士顿凯尔特人队在第一轮第一顺位选中](../Page/波士顿凯尔特人队.md "wikilink")，但是没有为凯尔特人队打过一场比赛，他的职业生涯开始于[韦恩堡活塞队](../Page/底特律活塞队.md "wikilink")，总共效力了NBA9个赛季，此后来效力过[密尔沃基／圣路易斯老鹰队和](../Page/亚特兰大老鹰队.md "wikilink")[明尼阿波利斯湖人队](../Page/洛杉矶湖人队.md "wikilink")。

他在9个赛季的职业生涯中总共出场596次，取得场均8.3分、8.4个篮板和1.4次助攻。

## 参考资料

## 外部链接

  - [Career
    statistics](http://www.basketball-reference.com/players/s/sharech01.html)

[Category:美国男子篮球运动员](../Category/美国男子篮球运动员.md "wikilink")
[Category:韦恩堡活塞队球员](../Category/韦恩堡活塞队球员.md "wikilink")
[Category:密尔沃基老鹰队球员](../Category/密尔沃基老鹰队球员.md "wikilink")
[Category:圣路易斯老鹰队球员](../Category/圣路易斯老鹰队球员.md "wikilink")
[Category:明尼阿波利斯湖人队球员](../Category/明尼阿波利斯湖人队球员.md "wikilink")