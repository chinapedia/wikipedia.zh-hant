**Lokamochi
Mamiba**為2007年[森美小儀歌劇團的主題](../Page/森美小儀歌劇團.md "wikilink")，於9月21日至30日公演。Lokamochi
Mamiba是「來嫁無情（無情乃森美飾演之角色）莫問吧」的諧音（亦有說是「落街冇錢買麵包」），是因為這次的舞台劇是強調[中西合璧並設定在](../Page/中西合璧.md "wikilink")[-{意}-大利](../Page/意大利.md "wikilink")\[1\]。演員除[森美](../Page/森美.md "wikilink")[小儀外](../Page/小儀.md "wikilink")，還有嘉賓[鄧麗欣](../Page/鄧麗欣.md "wikilink")、[何韻詩](../Page/何韻詩.md "wikilink")、[周麗淇及](../Page/周麗淇.md "wikilink")[容祖兒](../Page/容祖兒.md "wikilink")，[王菀之等](../Page/王菀之.md "wikilink")。2007年8月13日為[森美小儀歌劇團第一次公開售票](../Page/森美小儀歌劇團.md "wikilink")，並於半[小時內全部售罄](../Page/小時.md "wikilink")，創下[香港最快售罄全部](../Page/香港.md "wikilink")[門票的](../Page/門票.md "wikilink")[歷史](../Page/歷史.md "wikilink")[紀錄](../Page/紀錄.md "wikilink")\[2\]。森美表示有關場館檔期只能讓歌劇團加開日場，所以希望日後可以重演\[3\]。

## 售票詳情

  - 日期：2007年9月21日至30日（21日不公開售票）
  - 時間：[晚上](../Page/晚上.md "wikilink")8時
  - 地點：[理工大學](../Page/理工大學.md "wikilink")[賽馬會綜藝館](../Page/賽馬會.md "wikilink")
  - 訂購票價：$280 / $180
  - 位置是分上下層。下層$280，上層$180

首場門票可於8月26日、9月2日及9月9日在[青衣城購物換取](../Page/青衣城.md "wikilink")。
用電子貨幣購物滿港幣$1000可換$180門票兩張，滿$1500則可換$280門票兩張。\[4\]

另外8月16日在電台廣播及網站證實加開9月26日和30日日場，由[林海峰做嘉賓](../Page/林海峰.md "wikilink")，並於8月20日售票。

## 演員

### 商業電台演員

  - [森美](../Page/森美.md "wikilink")
  - [小儀](../Page/小儀.md "wikilink")
  - [細蘇](../Page/細蘇.md "wikilink")
  - [詹志民](../Page/詹志民.md "wikilink")
  - [E](../Page/13E.md "wikilink")
  - [Wing](../Page/99Wing.md "wikilink")
  - [朱薰](../Page/朱薰.md "wikilink")
  - [余迪偉](../Page/余迪偉.md "wikilink")
  - [有耳非文](../Page/有耳非文.md "wikilink")

### 演出嘉賓

  - [何韻詩](../Page/何韻詩.md "wikilink")：9月21日至22日
  - [王菀之](../Page/王菀之.md "wikilink")：9月23日至24日
  - [周麗淇](../Page/周麗淇.md "wikilink")：9月25日至26日
  - [鄧麗欣](../Page/鄧麗欣.md "wikilink")：9月27日至28日
  - [容祖兒](../Page/容祖兒.md "wikilink")：9月29日至30日
  - [林海峰](../Page/林海峰.md "wikilink")：9月26日和30日（日場）

## 資料來源

<references/>

## 外部連結

  - [Lokamochi Mamiba
    網頁](https://web.archive.org/web/20070817230603/http://www.my903.com/my903/SKDrama2007/index.jsp)

[Category:香港舞台劇](../Category/香港舞台劇.md "wikilink")
[Category:香港音樂劇](../Category/香港音樂劇.md "wikilink")
[Category:香港戲劇作品](../Category/香港戲劇作品.md "wikilink")
[Category:森美小儀歌劇團作品](../Category/森美小儀歌劇團作品.md "wikilink")
[Category:2007年音樂劇](../Category/2007年音樂劇.md "wikilink")

1.  [朱薰](../Page/朱薰.md "wikilink")，朱Fun E - Yahoo\!
    BLOG，http://hk.myblog.yahoo.com/chu_fun/article?mid=11835
    ，2007年8月14日更新。
2.  [香港文匯報](../Page/香港文匯報.md "wikilink")，《[軒仔捧硬](../Page/軒仔.md "wikilink")[王菀之場](../Page/王菀之.md "wikilink")》，http://paper.wenweipo.com/2007/08/14/EN0708140004.htm
    ，2007年8月14日更新。
3.  Yahoo\! 新聞，《[森美呻冇錢賺](../Page/森美.md "wikilink")
    [歌舞劇門票兩](../Page/歌舞劇.md "wikilink")[小時售罄](../Page/小時.md "wikilink")》，
    ，2007年8月14日更新。
4.  青衣城，推廣活動， <http://www.maritimesquare.com/chi/promo.asp#1>