**以挪士**（[希伯来语](../Page/希伯来语.md "wikilink"):
**אֱנוֹשׁ**，英语：**Enosh**），天主教译“厄诺士”，是個旧约[聖經人物](../Page/聖經.md "wikilink")，意为“软弱必死的人”或“人”。在[基督教](../Page/基督教.md "wikilink")《聖經》及《[塔納赫](../Page/塔納赫.md "wikilink")》裡，是[亞當的第三子](../Page/亞當.md "wikilink")[塞特的兒子](../Page/塞特.md "wikilink")，[该南的父亲](../Page/该南.md "wikilink")，在[路加福音中他被列为](../Page/路加福音.md "wikilink")[耶稣的先祖之一](../Page/耶稣.md "wikilink")\[1\]。他的寿命是905岁\[2\]。**以挪士**是[挪亞的七世祖](../Page/挪亞.md "wikilink")<ref>創世記5章22-29節；塞特活到一百零五歲，生了以挪士。創五:6。
以挪士活到九十歲，生了該南。創五:9 該南活到七十歲，生了瑪勒列。創五:12 瑪勒列活到六十五歲，生了雅列。創五:15
雅列活到一百六十二歲，生了以諾。創五:18 以諾活到六十五歲，生了瑪土撒拉。創五:21
瑪土撒拉活到一百八十七歲，生了拉麥。創五:25 拉麥活到一百八十二歲，生了一個兒子，(創五:28) 給他起名叫挪亞，(創五:29)
挪亞五百歲生了閃、含、雅弗。創五:32

為挪亞的祖先</ref>。 **以挪士**活到90歲生了[該南](../Page/該南.md "wikilink")\[3\]。

在整部《[塔納赫](../Page/塔納赫.md "wikilink")》中對以挪士再無其它描述。从**以挪士**开始，人类开始求告[雅赫维](../Page/雅赫维.md "wikilink")（[耶和華](../Page/耶和華.md "wikilink")）的名。\[4\]

## 參看

  - [阿當](../Page/阿當.md "wikilink") - [阿當與夏娃
    (摩門教)](../Page/阿當與夏娃_\(摩門教\).md "wikilink")
  - [伊斯蘭教中的以諾 (易德立斯)](../Page/易德立斯.md "wikilink") (Idris；)

## 外部連結

  - [*Dictionary of the History of
    Ideas*:](https://web.archive.org/web/20050211084959/http://etext.lib.virginia.edu/cgi-local/DHI/dhi.cgi?id=dv1-65)
    Cosmic Voyages
  - [*Catholic Encyclopedia
    Entry*](http://www.newadvent.org/cathen/07218a.htm)
  - [*Jewish Encyclopedia
    Entry*](http://www.jewishencyclopedia.com/view.jsp?artid=383&letter=E)
  - [*Mackey
    Encyclopedia*](https://web.archive.org/web/20041227132415/http://users.1st.net/fischer/MacEncE2.HTM)
  - ["Encyclopedia of
    Mormonism](https://web.archive.org/web/20060118162529/http://www.lightplanet.com/mormons/basic/bible/people/enoch_eom.htm)
  - [Comparison of Masonic legends of Enoch and Mormon scriptures
    description of
    Enoch](https://web.archive.org/web/20110308052441/http://www.cephasministry.com/mormon_is_there_no_help.html)

## 參考文獻

[E](../Category/聖經人物.md "wikilink") [E](../Category/古蘭經人物.md "wikilink")

1.  路加福音三：37-38
2.  創五:11
3.  創五:9
4.  創四:26