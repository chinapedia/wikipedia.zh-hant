**欧巴涅**（[法语](../Page/法语.md "wikilink")：****）是[法国](../Page/法国.md "wikilink")[普罗旺斯-阿尔卑斯-蓝色海岸大区](../Page/普罗旺斯-阿尔卑斯-蓝色海岸大区.md "wikilink")[罗讷河口省的一个小镇](../Page/罗讷河口省.md "wikilink")，距离大区首府[马赛约](../Page/马赛.md "wikilink")17公里，人口42,900（2005年）。

欧巴涅是[法国外籍军团的总部所在](../Page/法国外籍军团.md "wikilink")。

## 名人

  - [阿兰·贝尔纳](../Page/阿兰·贝尔纳.md "wikilink")：游泳运动员
  - [马塞尔·帕尼奥尔](../Page/马塞尔·帕尼奥尔.md "wikilink")：小说家、剧作家、电影制片人

## 人口

欧巴涅人口变化图示

## 参见

  - [罗讷河口省市镇列表](../Page/罗讷河口省市镇列表.md "wikilink")

## 参考文献

## 外部链接

  - <https://web.archive.org/web/20090227030245/http://www.aubagne.com/>

[A](../Category/罗讷河口省市镇.md "wikilink")