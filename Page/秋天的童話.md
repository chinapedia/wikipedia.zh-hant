《**秋天的童話**》（；台灣上映片名《**流氓大亨**》）是一部於1987年上映的[香港電影](../Page/香港電影.md "wikilink")，由[張婉婷擔任執導](../Page/張婉婷.md "wikilink")，以及由[周潤發](../Page/周潤發.md "wikilink")、[鍾楚紅](../Page/鍾楚紅.md "wikilink")、[陳百強等擔任主演](../Page/陳百強.md "wikilink")。本片于1987年获第七届香港电影金像奖最佳电影奖、最佳编剧奖、最佳摄影奖。

## 故事情节

美丽而骄纵的年轻姑娘李琪（[钟楚红飾](../Page/钟楚红.md "wikilink")）只身从香港赴纽约攻读大学，并探望先她赴美的男友（[陈百强飾](../Page/陈百强.md "wikilink")）。前来机场接她的，是一个在唐人街打工的远房亲戚船头尺（[周润发飾](../Page/周润发.md "wikilink")）。满口脏话、粗俗不堪的船头尺，被人看作“烂鬼”。船头尺初時對李琪感到厭煩，並稱她為茶煲(即是英文的trouble
，麻煩的意思)。

李琪发现男友移情别恋，心灰意冷，把自己关在房里。一次因家中煤气泄漏而中毒，幸亏住楼下的船头尺及時拯救，得以很快康复，并逐渐建立信心，专心读书和工作。船頭尺对李琪日久生情，开始戒烟戒赌。某日，船頭尺聽聞朋友遇事，並決定為他出頭，李琪阻止不果，更險出現意外。

秋天时，李琪決定接受一份在[長島的新工作](../Page/長島_\(紐約\).md "wikilink")，離別時，船头尺將自己的二手車賣掉，買了一條錶帶送給她配襯其手錶機芯，卻發現她將手錶機芯送了給自己。

一段时间之后，当李琪旧地重游时，发现在海边开了一间喚作「舢板」（Sampan）的餐厅。之後船头尺現身，以店主的身份招待她。

## 人物

|                   |                                  |                                                                                                                                                                                                                                                                                                                                                                 |
| ----------------- | -------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **角色**            | **演員**                           | **備註**                                                                                                                                                                                                                                                                                                                                                          |
| **李琪**（十三妹）       | [鍾楚紅](../Page/鍾楚紅.md "wikilink") | 英文名是Jennifer。一名年輕女子，隻身到[美國](../Page/美國.md "wikilink")[紐約留學](../Page/紐約.md "wikilink")，並打算探望已在美國留學的[男友](../Page/男友.md "wikilink")。初抵步即由一名遠親（船頭尺）接機。李琪人生路不熟，得船頭尺照顧。後來發現原來男友已移情別戀，而船頭尺對琪竟然日久生情，但二人生活世界截然不同，理性的在保持距離，又在感性的接近……                                                                                                                                      |
| **船頭尺**           | [周潤發](../Page/周潤發.md "wikilink") | 英文名是Samuel Pang。一名在[唐人街生活](../Page/唐人街.md "wikilink")，在[餐館任](../Page/餐館.md "wikilink")[服務生的老粗男子](../Page/服務生.md "wikilink")。平時好[賭](../Page/賭.md "wikilink")，生活亦不修邊幅，講[義氣](../Page/義氣.md "wikilink")。發現對琪日久生情後，於是[暗戀令他嘗試改變發奮](../Page/暗戀.md "wikilink")，卻又自覺配不上琪而不敢表露。「船頭尺」是[雙關語](../Page/雙關語.md "wikilink")，意思是(量)「度水」，香港俗語中，[借錢的意思](../Page/借錢.md "wikilink")。 |
| **Vincent**       | [陳百強](../Page/陳百強.md "wikilink") | 李琪前度男友。                                                                                                                                                                                                                                                                                                                                                         |
| **李琪母**           | [潘迪華](../Page/潘迪華.md "wikilink") | 李琪的媽媽。                                                                                                                                                                                                                                                                                                                                                          |
| **Mrs. Sherwood** | [黃淑儀](../Page/黃淑儀.md "wikilink") | 李琪在其富宅任褓姆的僱主。                                                                                                                                                                                                                                                                                                                                                   |

## 製作人員

  - 美術指導：Cristy Addis，[黃仁逵](../Page/黃仁逵.md "wikilink")
  - 造型設計：[張叔平](../Page/張叔平.md "wikilink")
  - 剪輯：[蔣國權](../Page/蔣國權.md "wikilink")，[李炎海](../Page/李炎海.md "wikilink")，[陳祺和](../Page/陳祺和.md "wikilink")，[朱晨傑](../Page/朱晨傑.md "wikilink")，[鄺志良](../Page/鄺志良.md "wikilink")
  - 策劃：[俞琤](../Page/俞琤.md "wikilink")

## 製作

張婉婷透露在《秋天的童話》開拍前，男主角周潤發憑《[英雄本色](../Page/英雄本色.md "wikilink")》而人氣急升，令未與對方簽約的她十分擔憂對方要求增加片酬，幸好發哥有義氣兌現承諾接拍。\[1\]

## 主題曲

陳百強專誠為《秋天的童話》作了主題曲《夢囈》，更找來好友[俞琤和](../Page/俞琤.md "wikilink")[陳少琪按劇本而寫上了歌詞](../Page/陳少琪.md "wikilink")。但其後導演張婉婷和羅啟銳改用了一個沒那麼傷感的結局。所以主題曲臨時換了由[呂方主唱的](../Page/呂方.md "wikilink")《別了秋天》，而《夢囈》則慘被棄用。\[2\]

## 獎項

<table>
<thead>
<tr class="header">
<th><p>頒獎典禮</p></th>
<th><p>獎項</p></th>
<th><p>名單</p></th>
<th><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/1988年香港電影金像獎.md" title="wikilink">香港電影金像獎（第7屆）</a></p></td>
<td><p>最佳電影</p></td>
<td><p>秋天的童話</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳導演</p></td>
<td><p><a href="../Page/張婉婷.md" title="wikilink">張婉婷</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳編劇</p></td>
<td><p><a href="../Page/羅啟銳.md" title="wikilink">羅啟銳</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳男主角</p></td>
<td><p><a href="../Page/周潤發.md" title="wikilink">周潤發</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳女主角</p></td>
<td><p><a href="../Page/鍾楚紅.md" title="wikilink">鍾楚紅</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳攝影</p></td>
<td><p><br />
鍾志文</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳音樂</p></td>
<td><p><a href="../Page/盧冠廷.md" title="wikilink">盧冠廷</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/第24屆金馬獎.md" title="wikilink">金馬獎（第24屆）</a></p></td>
<td><p>最佳劇情片</p></td>
<td><p>秋天的童話</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳導演</p></td>
<td><p><a href="../Page/張婉婷.md" title="wikilink">張婉婷</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳男主角</p></td>
<td><p><a href="../Page/周潤發.md" title="wikilink">周潤發</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳女主角</p></td>
<td><p><a href="../Page/鍾楚紅.md" title="wikilink">鍾楚紅</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>最佳原著劇本</p></td>
<td><p><a href="../Page/羅啟銳.md" title="wikilink">羅啟銳</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>最佳攝影</p></td>
<td><p><a href="../Page/鍾志文.md" title="wikilink">鍾志文</a></p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

《秋天的童話》獲選為[百部不可不看的香港電影的其中一部](../Page/百部不可不看的香港電影.md "wikilink")。\[3\]

## 粗口爭議

[無線電視有限公司在](../Page/無線電視有限公司.md "wikilink")2006年10月播映《秋天的童話》時，因為片中的「F\*\*k」字與「冚家\*」兩個粗口和船頭尺的其他粗俗說話而遭市民投訴，不應安排在星期日下午重播，\[4\]
最終[廣管局裁定投訴成立](../Page/廣管局.md "wikilink")，無線被勸喻。\[5\]

無線在2007年3月12日凌晨12時45分播出時，除了「F\*\*k」字與「冚家\*」兩個粗口之外，男主角船頭尺的其他粗俗用語依然會保留。\[6\]

## 參考資料

<references/>

## 外部連結

  -
  - {{@movies|fahk40093426|流氓大亨}}

  -
  -
  -
  -
  -
  - [《秋天的童話》日本版劇照](http://www3.telus.net/public/lchor/japan-version.pdf)

[7](../Category/1980年代香港電影作品.md "wikilink")
[Category:粤语電影](../Category/粤语電影.md "wikilink")
[Category:香港浪漫剧情片](../Category/香港浪漫剧情片.md "wikilink")
[Category:1980年代爱情片](../Category/1980年代爱情片.md "wikilink")
[Category:纽约市背景电影](../Category/纽约市背景电影.md "wikilink")
[Category:张婉婷电影](../Category/张婉婷电影.md "wikilink")
[Category:身分差異戀情題材電影](../Category/身分差異戀情題材電影.md "wikilink")
[Category:纽约市取景電影](../Category/纽约市取景電影.md "wikilink")
[Category:香港電影金像獎最佳電影](../Category/香港電影金像獎最佳電影.md "wikilink")
[Category:香港電影金像獎最佳編劇獲獎電影](../Category/香港電影金像獎最佳編劇獲獎電影.md "wikilink")
[Category:香港電影金像獎最佳攝影獲獎電影](../Category/香港電影金像獎最佳攝影獲獎電影.md "wikilink")
[Category:金馬獎最佳男主角獲獎電影](../Category/金馬獎最佳男主角獲獎電影.md "wikilink")
[Category:卢冠廷配乐电影](../Category/卢冠廷配乐电影.md "wikilink")

1.
2.
3.
4.
5.
6.