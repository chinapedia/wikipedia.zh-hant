**維京金屬**是[黑金屬音樂的另一個分支流派](../Page/黑金屬音樂.md "wikilink")。此種樂風是以黑金屬為架構，一樣使用失真吉他、雙踏、黑腔、鍵盤等等的演奏方式，不過編曲上比黑金屬要注重旋律，曲風也比較沒有那麼凶狠、肅殺、反而多了一些較為抒情的感覺。有些樂團甚至完全沒有黑金屬的樣子，乍聽之下反而像是旋律金屬一樣，歌詞則較關注於祖先情懷等等。

## 維京金屬之發展史

維京金屬的定位或多或少可追溯至1988年，當瑞典樂隊[Bathory發表他們第四張大碟](../Page/Bathory.md "wikilink")"Blood
Fire
Death"。他們滲入了[挪威及](../Page/挪威.md "wikilink")[日耳曼色彩的藝術](../Page/日耳曼.md "wikilink")，尤其是歌詞，因此而變得富有[史詩色彩](../Page/史詩.md "wikilink")。

維京金屬廣泛借用了傳統[黑金屬音樂的演繹方式](../Page/黑金屬.md "wikilink")，而歌詞方面，則較為史詩，講述[維京戰士與](../Page/維京.md "wikilink")[基督徒的鬥爭](../Page/基督徒.md "wikilink")；英勇的挪威人對抗入侵過來的基督徒。近期該類音樂發展至較接近[斯堪的納維亞](../Page/斯堪的納維亞.md "wikilink")[神話學](../Page/神話學.md "wikilink")。

最為突出的維京金屬樂隊有Enslaved、Bathory(中期時)、Thyrfing、Falkenbach、Windir和Moonsorrow.

## 具代表性的作品

  - [Amon Amarth](../Page/Amon_Amarth.md "wikilink") - Once Sent From
    the Golden Hall, The Avenger
  - [Ásmegin](../Page/Ásmegin.md "wikilink") - Hin Vordende Sod & Sø
  - [Bathory](../Page/Bathory.md "wikilink") - Hammerheart, Twilight of
    the Gods, Nordland I, Nordland II
  - [Einherjer](../Page/Einherjer.md "wikilink") - Dragons of the North,
    Blot
  - [Ensiferum](../Page/Ensiferum.md "wikilink") - Ensiferum, Iron
  - [Enslaved](../Page/Enslaved.md "wikilink") - Vikingligr Veldi
  - [Falkenbach](../Page/Falkenbach.md "wikilink") - Ok Nefna Tysvar Ty
  - [Finntroll](../Page/Finntroll.md "wikilink") - Jaktens Tid
  - [Månegarm](../Page/Månegarm.md "wikilink") - Havets Vargar
  - [Mithotyn](../Page/Mithotyn.md "wikilink") - In the Sign of the
    Ravens, King of the Distant Forest, Gathered Around the Oaken Table
  - [Moonsorrow](../Page/Moonsorrow.md "wikilink") - Kivenkantaja
  - [Thyrfing](../Page/Thyrfing.md "wikilink") - Vansinnesvisor
  - [Turisas](../Page/Turisas.md "wikilink") - Battle Metal
  - [Twin Obscenity](../Page/Twin_Obscenity.md "wikilink") - For Blood,
    Honour And Soil
  - [Vintersorg](../Page/Vintersorg.md "wikilink") - Till Fjälls
  - [Windir](../Page/Windir.md "wikilink") - Arntor, Sóknardalr
  - [Wintersun](../Page/Wintersun.md "wikilink") - Wintersun

[category:重金屬音樂](../Page/category:重金屬音樂.md "wikilink")