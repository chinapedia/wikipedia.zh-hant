border |flag_caption= |anthem |anthem_ref |holiday |holiday_ref
|political_status=州 |political_status_link=州 (俄羅斯)
|federal_district=[烏拉爾聯邦管區](../Page/烏拉爾聯邦管區.md "wikilink")
|economic_region=[乌拉尔经济地区](../Page/乌拉尔经济地区.md "wikilink")
|adm_ctr_type=行政中心
|adm_ctr_name=[葉卡捷琳堡](../Page/葉卡捷琳堡.md "wikilink")
|adm_ctr_ref |pop_2010census=4297747 |pop_2010census_rank=5th
|urban_pop_2010census=83.9% |rural_pop_2010census=16.1%
|pop_2010census_ref=\[1\] |pop_density |pop_density_as_of
|pop_density_ref |pop_latest |pop_latest_date |pop_latest_ref
|area_km2=194800 |area_km2_rank=17th |area_km2_ref
|established_date=January 17, 1934 |established_date_ref
|license_plates=66, 96, 196 |ISO=RU-SVE |gov_as_of=March 2011
|leader_title= |leader_title_ref |leader_name=[Yevgeny
Kuyvashev](../Page/Yevgeny_Kuyvashev.md "wikilink")
|leader_name_ref=\[2\] |legislature= |legislature_ref
|website=<http://www.midural.ru/> |date=March 2011 }}
**斯維爾德洛夫斯克州**（）是[俄羅斯聯邦](../Page/俄羅斯聯邦.md "wikilink")[主體之一](../Page/俄羅斯聯邦主體.md "wikilink")，屬於[乌拉尔联邦管区](../Page/乌拉尔联邦管区.md "wikilink")，首府[葉卡捷琳堡](../Page/葉卡捷琳堡.md "wikilink")。大部份位於[烏拉爾山脈東麓](../Page/烏拉爾山脈.md "wikilink")，面積194,800平方公里，人口
4,297,747 (2010年)\[3\]。

## 地理

本州主要散落於[烏拉爾山脈的中部和北部向東的山麓及](../Page/烏拉爾山脈.md "wikilink")[西西伯利亞平原](../Page/西西伯利亞平原.md "wikilink")。只有西南面延伸至烏拉爾山脈的西麓。

州內最高的山峰均位於烏拉爾山脈的北部。山脈的中部雖然多山，但少有突尖起來的高峰。平均海拔為海平面上300到500公尺。

## 姊妹地區

  - [巴地頭頓省](../Page/巴地頭頓省.md "wikilink")

  - [黑龍江省](../Page/黑龍江省.md "wikilink")[哈爾濱市](../Page/哈爾濱市.md "wikilink")

## 著名人物

  - [葉利欽](../Page/葉利欽.md "wikilink")—[俄羅斯聯邦第一任總統](../Page/俄羅斯聯邦.md "wikilink")
    (出生)。

<!-- end list -->

  - [雅科夫·米哈伊洛维奇·斯维尔德洛夫](../Page/雅科夫·米哈伊洛维奇·斯维尔德洛夫.md "wikilink")：共產黨的革命領袖，本州以他的姓來命名。

## 著名事件

  - [佳特洛夫事件](../Page/佳特洛夫事件.md "wikilink")，苏联1959年乌拉尔登山者遇难事件

## 注释

## 參考資料

## 外部連結

  -
  - [Investment portal of Sverdlovsk Oblast](http://invest.midural.ru/)

  - [Official website of the Government of Sverdlovsk
    Oblast](http://www.midural.ru/midural-new)

[\*](../Category/斯維爾德洛夫斯克州.md "wikilink")
[Category:乌拉尔联邦管区](../Category/乌拉尔联邦管区.md "wikilink")

1.
2.  Official website of the Governor of Sverdlovsk Oblast. [Alexander
    Sergeyevich Misharin](http://www.amisharin.ru/)

3.