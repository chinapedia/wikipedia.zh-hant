[Togo_Deutsches_Koloniallexikon,_Verlag_von_Quelle_&_Meyer_Leipzig.jpg](https://zh.wikipedia.org/wiki/File:Togo_Deutsches_Koloniallexikon,_Verlag_von_Quelle_&_Meyer_Leipzig.jpg "fig:Togo_Deutsches_Koloniallexikon,_Verlag_von_Quelle_&_Meyer_Leipzig.jpg")
**多哥蘭**（****）是[德國在](../Page/德意志帝國.md "wikilink")19世紀到[第一次世界大戰結束時在](../Page/第一次世界大戰.md "wikilink")[非洲西部的](../Page/非洲.md "wikilink")[保護國](../Page/保護國.md "wikilink")。其國土除了包括現時之[多哥全境以外](../Page/多哥.md "wikilink")，尚包括今日[-{zh-hans:加纳;zh-hk:加納;zh-tw:迦納;}-東部的部份土地](../Page/加納.md "wikilink")。[第一次世界大戰之後](../Page/第一次世界大戰.md "wikilink")，德國戰敗，國土被[英國及](../Page/英國.md "wikilink")[法國瓜分](../Page/法國.md "wikilink")，分別被稱作「英屬多哥蘭」及「法屬多哥蘭」。第二次世界大戰後，非洲各國尋求獨立，多哥蘭的前途及統一問題一度成為焦點。最後英屬多哥蘭決定與英屬黃金海岸合併成為加纳，而法屬多哥蘭則決定獨立，成為今日的[多哥共和國](../Page/多哥共和國.md "wikilink")。

## 地理

多哥蘭位於[非洲西部](../Page/非洲.md "wikilink")，東連[達荷美](../Page/達荷美.md "wikilink")（[貝寧](../Page/貝寧.md "wikilink")），北界[上沃爾特](../Page/上沃爾特.md "wikilink")（[布基纳法索](../Page/布基纳法索.md "wikilink")），西至[沃爾特河下游東岸](../Page/沃爾特河.md "wikilink")，與[英屬黃金海岸相對](../Page/英屬黃金海岸.md "wikilink")，南方是[几内亚湾](../Page/几内亚湾.md "wikilink")。多哥土地乾燥，產[柚木](../Page/柚木.md "wikilink")、[橡皮](../Page/橡皮.md "wikilink")、[椰子等](../Page/椰子.md "wikilink")，主要作出口之用。

## 人口

多哥蘭的人口以[敏納族](../Page/敏納族.md "wikilink")（Mina）為主，其次是[埃維族](../Page/埃維族.md "wikilink")（Ewe）。

## 歷史

多哥蘭位處於非洲大陸歷史上兩大勢力的夾縫：來自今日[尼日利亞及](../Page/尼日利亞.md "wikilink")[貝寧的](../Page/貝寧.md "wikilink")[埃維族](../Page/埃維族.md "wikilink")（Ewe）與來自加納的[敏納族](../Page/敏納族.md "wikilink")（Mina）及[谷因族](../Page/谷因族.md "wikilink")（Guin）。西方歷史並未有記錄在[葡萄牙人在](../Page/葡萄牙.md "wikilink")15世紀來到這裡之前，原住民的生活如何。不過，根據之後的記載，這兩大勢力在當時西非沿岸及流出海的各條河流兩岸居住，族群間戰鬥頻仍，而戰敗者很多時都被當作奴隸賣給歐洲國家。
在16世紀[奴隸貿易最興盛之時](../Page/奴隸貿易.md "wikilink")，敏納族是最大的得益者：他們成為了歐洲奴隸商的買手，不斷把北方的Kabye等民族販賣與歐洲人。由於多哥維爾一帶缺乏天然良港，歐洲人只在鄰近的Elmina（位於今日的迦納）及Ouidah（位於今日的貝寧）設置哨站。在往後的二百年，幾內亞灣沿岸一直都是歐洲人販賣奴隸的熱點，而這片海岸亦贏得了「[奴隸海岸](../Page/奴隸海岸.md "wikilink")」的惡名。

### 多哥蘭的成立

多哥蘭成立於1884年7月5日，由[俾斯麥親王派遣到](../Page/俾斯麥.md "wikilink")[多哥維爾的特使](../Page/多哥維爾.md "wikilink")[古斯塔夫·納赫蒂加爾](../Page/古斯塔夫·納赫蒂加爾.md "wikilink")（Gustav
Nachtigal）與當地原來的統治者[姆拉巴三世簽訂的保護條約確認](../Page/姆拉巴三世.md "wikilink")。在這次前，其實在多哥維爾附近地區已陸續出現來自歐洲的移民，主要來自[葡萄牙及](../Page/葡萄牙.md "wikilink")[丹麥](../Page/丹麥.md "wikilink")，不過這段移民史缺乏足夠的歷史紀錄。1885年[柏林會議正式承認為其勢力範圍之後](../Page/柏林西非會議.md "wikilink")，德國不斷於北部擴張保護國的領土，與其他歐洲國家繼續在非洲的圈地。但另一方面，德國透過本身較為先進的科學化農業技術，提高當地的[可可](../Page/可可.md "wikilink")、[咖啡豆及](../Page/咖啡豆.md "wikilink")[棉花的產量](../Page/棉花.md "wikilink")，作出口之用。當時多哥蘭的基建及生產水平在一戰前的非洲來說，算是數一數二的。當時的多哥蘭除了向德國出口農產品以外，其經濟尚能自給自足，成為了非洲殖民地的榜樣；相反，鄰近的其他歐洲殖民地卻要繼續靠向歐洲出口[奴隸而維生](../Page/奴隸.md "wikilink")。

### 一戰與英法瓜分

[Togoland_Map_Chinese.png](https://zh.wikipedia.org/wiki/File:Togoland_Map_Chinese.png "fig:Togoland_Map_Chinese.png")
由於多哥蘭的富庶，在一戰後期它成為了協約國的入侵對象。當時多哥蘭立處於[英國殖民地黃金海岸及](../Page/英國.md "wikilink")[法國殖民地上沃爾特和達荷美之間](../Page/法國.md "wikilink")。1916年8月26日，英法聯軍入侵多哥蘭，境內德國人只支持了五日，就向聯軍投降，結束了多哥蘭32年的歷史。同年12月27日，兩國把多哥蘭瓜分：東部連同首都[洛梅及海岸線歸法代管](../Page/洛梅.md "wikilink")，是為「法屬多哥蘭」；西部歸英代管，是為「英屬多哥蘭」。1920年，[國際聯盟承認兩國對多哥蘭的佔領](../Page/國際聯盟.md "wikilink")，並把兩個多哥蘭列為[國際聯盟委任管治國](../Page/國際聯盟委任管治國.md "wikilink")，並委任英、法英國管治兩個多哥蘭。而從這段時間開始，英國把英屬多哥蘭與原來的殖民地一起管理，是為[英屬黃金海岸](../Page/英屬黃金海岸.md "wikilink")。

[Stamp_Togo_1916_5c.jpg](https://zh.wikipedia.org/wiki/File:Stamp_Togo_1916_5c.jpg "fig:Stamp_Togo_1916_5c.jpg")

### 統一與分治的爭論

[第二次世界大戰之後](../Page/第二次世界大戰.md "wikilink")，[聯合國取代了國際聯盟的地位](../Page/聯合國.md "wikilink")，原來國際聯盟的委任統治國都成為了聯合國的託管地。1946年12月13日，聯合國取消了英、法兩國的委任統治，但仍把國境交給兩國託管。居住在沿岸的敏納族，由於其地理因素及長期以來與歐洲人的關係，在殖民期間掌屋了政治及經濟命脈，對於統一或分治並沒有太多的意見；相反的，散居於兩個多哥蘭的埃維族卻希望在多哥蘭統一後，能夠加強自身民族的實力。

### 獨立

然而，1956年12月，英屬多哥蘭的居民投票，加入從原來的黃金海岸獨立的新國家[加納](../Page/加納.md "wikilink")，粉碎了多哥蘭統一的希望。隨着聯合國託管法屬多哥蘭與法國的修約於1960年4月27日失效，法屬多哥蘭宣布獨立，取名為「[多哥](../Page/多哥.md "wikilink")」。

東部在聯合國監管之下進行全民投票，支持從法國完全獨立的[多哥國家聯盟黨得到大多數人的支持](../Page/多哥國家聯盟黨.md "wikilink")，黨魁[斯尔法纳斯·奥林匹欧就任國家首任總理](../Page/斯尔法纳斯·奥林匹欧.md "wikilink")。1960年2月，奥林匹欧拒絕了加納總統[克瓦米·恩克魯瑪的合併建議](../Page/克瓦米·恩克魯瑪.md "wikilink")，並於同年4月27日宣告獨立，成立[多哥共和國](../Page/多哥共和國.md "wikilink")。新成立的多哥共和國在同年9月加入聯合國。

## 影響

雖然多哥蘭曾是德國的保護國，但由於40年的分裂，使兩國對當地的影響較32年的德國保護來得更大。所以現時獨立後的多哥依然以[法語而非德語作官方語言](../Page/法語.md "wikilink")。同樣地，加入了加納的英屬多哥蘭舊地亦以英語作官方語言，而非德語。

## 參考書目

  - 《辭海》1947年版，第340/4頁
  - Funk & Magnalls New Encyclopedia Vol. 25, pp.385
  - [聯合國大會第十屆會](../Page/聯合國大會.md "wikilink")：據第四委員會報告書通過之決議案[944(10)：多哥蘭統一問題及英管多哥蘭託管領土之前途](http://daccess-ods.un.org/access.nsf/Get?OpenAgent&DS=A/RES/944\(X\)&Lang=C)

## 參看

  - [多哥歷史](../Page/多哥歷史.md "wikilink")
  - [德國殖民地](../Page/德國殖民地.md "wikilink")
  - [前德國殖民地列表](../Page/前德國殖民地列表.md "wikilink")

## 外部連結

  - [1912年多哥蘭地圖](http://www.deutsche-schutzgebiete.de/webpages/Togo_Karte_1912.jpg)
  - [多哥蘭原定的國旗](http://www.crwflags.com/fotw/images/d/de!tgl13.gif)（由於第一次世界大戰發生，這面旗從未使用過。）
  - [中国驻多哥共和国大使馆经济商务参赞处：多哥庆祝与德国120年友好关系](http://tg.mofcom.gov.cn/aarticle/jmxw/200407/20040700250268.html)

[Category:前德國殖民地](../Category/前德國殖民地.md "wikilink")
[Category:已不存在的非洲國家](../Category/已不存在的非洲國家.md "wikilink")
[Category:多哥歷史](../Category/多哥歷史.md "wikilink")