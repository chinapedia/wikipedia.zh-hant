[BhutanThimphu.png](https://zh.wikipedia.org/wiki/File:BhutanThimphu.png "fig:BhutanThimphu.png")

**廷布宗**（）( ཐིམ་ཕུ་རྫོང་ཁག་
)是[不丹二十個宗](../Page/不丹.md "wikilink")(dzongkhag)之一，面積達1,943km<sup>2</sup>，人口大約95,059。首府為[廷布](../Page/廷布.md "wikilink")
(Thimpu)，也是[不丹最大城市及首都](../Page/不丹.md "wikilink")。

**廷布宗**下有十個格窩(Gewog)：

  - [Bapbi
    格窩](https://web.archive.org/web/20050817050219/http://www.dop.gov.bt/fyp/09/tp_Bapbi.pdf)
  - [Chang
    格窩](https://web.archive.org/web/20050816131205/http://www.dop.gov.bt/fyp/09/tp_Chang.pdf)
  - [Dagala
    格窩](https://web.archive.org/web/20050816195528/http://www.dop.gov.bt/fyp/09/tp_Dagala.pdf)
  - [Genyekha
    格窩](https://web.archive.org/web/20050816121857/http://www.dop.gov.bt/fyp/09/tp_Genyekha.pdf)
  - [Kawang
    格窩](https://web.archive.org/web/20041022220207/http://www.dop.gov.bt/fyp/09/tp_Kawang.pdf)
  - [Lingzhi
    格窩](https://web.archive.org/web/20041022223841/http://www.dop.gov.bt/fyp/09/tp_Lingzhi.pdf)
  - [Mewang
    格窩](https://web.archive.org/web/20050817102831/http://www.dop.gov.bt/fyp/09/tp_Mewang.pdf)
  - [Naro
    格窩](https://web.archive.org/web/20050824101355/http://www.dop.gov.bt/fyp/09/tp_Naro.pdf)
  - [Soe
    格窩](https://web.archive.org/web/20050817015955/http://www.dop.gov.bt/fyp/09/tp_Soe.pdf)
  - [Toepisa
    格窩](https://web.archive.org/web/20050818022808/http://www.dop.gov.bt/fyp/09/tp_Toepisa.pdf)

## 參見

  - [不丹行政區劃](../Page/不丹行政區劃.md "wikilink")

## 外部链接

  - [Dzongkhag profile with a map of
    gewogs](http://www.dop.gov.bt/gpis/DzProfile/dzoProfilemain.asp?dzoCode=14&profYear=2003)
  - [Thimphu
    Twon](http://windhorsetours.com/bhutan/bhutan_sights_category.php?sidm=e65a0ed0e39dd14a9e5af441efb09b37&seid=15&gname=Thimphu&com=Wind%20Horse%20Tours,%20www.windhorsetours.com)

[Category:不丹行政區劃](../Category/不丹行政區劃.md "wikilink")