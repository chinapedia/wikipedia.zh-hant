**䓛**（），又名**1，2-苯并菲**（）。

  - [分子式](../Page/分子式.md "wikilink")：[C](../Page/C.md "wikilink")<sub>18</sub>[H](../Page/H.md "wikilink")<sub>12</sub>，[分子量](../Page/分子量.md "wikilink")228.29。
  - 白色或银灰色、黄绿色片状[晶体或平斜方八面体晶体](../Page/晶体.md "wikilink")。
  - 有红紫色荧光。
  - 熔点258.2℃，沸点448℃，密度1.274g/cm<sup>3</sup>。
  - 不溶于水，微溶于[乙醇](../Page/乙醇.md "wikilink")、[乙醚](../Page/乙醚.md "wikilink")、[二硫化碳](../Page/二硫化碳.md "wikilink")、[冰醋酸](../Page/冰醋酸.md "wikilink")，溶于热[甲苯](../Page/甲苯.md "wikilink")。
  - 有毒、易燃，在真空中易[升华](../Page/升华.md "wikilink")。
  - 从[蒽馏分中精馏而得](../Page/蒽.md "wikilink")。
  - 用作[紫外线过滤剂](../Page/紫外线.md "wikilink")、非磁性金属表面擦伤荧光剂、合成染料中间体。

## 註解

## 参考文献

[Category:多环芳香烃](../Category/多环芳香烃.md "wikilink")