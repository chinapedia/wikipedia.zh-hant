[缩略图](https://zh.wikipedia.org/wiki/File:"Don't_forget_that_everthing_will_be_forgotten",_Bieniek-Text_von_Sebastian_Bieniek_\(B1EN1EK\),_2014.jpg "fig:缩略图")
(Sebastian Bieniek), 2014.\]\]
[缩略图](https://zh.wikipedia.org/wiki/File:Programmed_Machines_installation_by_Maurizio_Bolognini.jpg "fig:缩略图")
[缩略图](https://zh.wikipedia.org/wiki/File:Distribution_1,_2013.jpg "fig:缩略图")
**觀念藝術**（）發生於1960年代的[美國](../Page/美國.md "wikilink")，不但已經在[美术或](../Page/美术.md "wikilink")[視覺藝術領域中佔一席之地](../Page/視覺藝術.md "wikilink")，也對[當代藝術教育及其他藝術相關活動產生啟發性影響](../Page/當代藝術.md "wikilink")\[1\]。觀念藝術是藝術的一種，主張作品所牽涉的意念比當中的物質性甚至傳統[美學更為重要](../Page/美學.md "wikilink")。不少人反對觀念藝術，當中的代表者為[反觀念藝術主義者](../Page/反觀念藝術.md "wikilink")。

## 理論

觀念艺术作品重要的部分是其概念，而有形的表现并不重要。艺术家[索爾·勒維特曾有过著名的论断](../Page/索爾·勒維特.md "wikilink")：“观念是创造艺术作品的机器”；这标志了[工业化最繁盛时期的](../Page/工业化.md "wikilink")[手工艺贬值](../Page/手工艺.md "wikilink")，以及[信息作为](../Page/信息.md "wikilink")[生产手段的时代的到来](../Page/生产手段.md "wikilink")。

## 历史

“观念艺术”这个[术语最早松散地隶属于](../Page/术语.md "wikilink")[激浪派](../Page/激浪派.md "wikilink")[作家和](../Page/作家.md "wikilink")[音乐家](../Page/音乐家.md "wikilink")[亨利·弗林特](../Page/亨利·弗林特.md "wikilink")，他在1961年写道：这类艺术的素材就是“观念”，就像[声音是](../Page/声音.md "wikilink")[音乐的素材一样](../Page/音乐.md "wikilink")。1968年，“观念艺术”这个[术语真正确立起来](../Page/术语.md "wikilink")。

## 評論

台灣的藝評家[高千惠表示](../Page/高千惠.md "wikilink")，儘管遠溯到古文化，可以找到觀念藝術所欲探討和發現的哲學空間，但藝術被視為一門自然科學和社會科學的哲學問題來研究，還是在19世紀後期[印象派畫家崛興之後](../Page/印象派.md "wikilink")，而在這之前，有關現代性的思潮則已間接地影響藝術的發展了。事實上，藝術的觀念發展與現代性的發展極為貼近。在時間點上，如何界定現代性之浮潛，亦可以用來界定觀念藝術潛出的歷史背景。從現代性的發展角度看，藝術的觀念衍變，一樣可以分為：古代、中古代、前現代、現代、後現代等時期。這樣的分法，是以社會思想條件來看觀念藝術的潛出，而每個前期階段，其歷史和生活型態，也都提供了藝術的觀念發展條件\[2\]。

## 代表人物與作品

  - [艺术与语言](../Page/艺术与语言.md "wikilink")
  - [约瑟夫·科苏斯](../Page/约瑟夫·科苏斯.md "wikilink")
  - [布鲁斯·瑙曼](../Page/布鲁斯·瑙曼.md "wikilink")

## 參考文獻

<references/>

[Category:藝術運動](../Category/藝術運動.md "wikilink")
[Category:後現代藝術](../Category/後現代藝術.md "wikilink")
[Category:當代藝術](../Category/當代藝術.md "wikilink")
[Category:艺术媒体](../Category/艺术媒体.md "wikilink")
[Category:美学](../Category/美学.md "wikilink")
[Category:概念論](../Category/概念論.md "wikilink")

1.
2.