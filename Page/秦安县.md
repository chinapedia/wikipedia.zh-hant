**秦安县**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[甘肃省](../Page/甘肃省.md "wikilink")[天水市下辖的一个](../Page/天水市.md "wikilink")[县](../Page/县.md "wikilink")，位于甘肃东南部。面积1601平方公里，2004年人口60万。[邮政编码](../Page/邮政编码.md "wikilink")
741600，县政府驻兴国镇。

## 历史沿革

### 历史

历史悠久，旅游资源丰富。秦安古称成纪，素有“羲里娲乡”之称。县内有大地湾、兴国寺、文庙大成殿等3处国家重点文物保护单位，已发现仰韶、马家窑、齐家文化等新石器时代文化遗址68处，省、市、县级文物保护单位53处。

秦安历史上就是古“丝绸之路”的要冲，三国时期的街亭战场就在县内陇城一带。这里名人辈出，唐太宗李世民，飞将军李广，前秦帝王苻坚，诗仙李白，明朝山东巡抚、著名书法家胡缵宗，清代“陇上铁汉”安维峻祖籍或出生地都在秦安。

其中陇城乡是中国[三国时代](../Page/三国时代.md "wikilink")[街亭之戰發生地](../Page/街亭之戰.md "wikilink")，由[蜀汉](../Page/蜀汉.md "wikilink")[丞相](../Page/丞相.md "wikilink")[诸葛亮北伐时派](../Page/诸葛亮.md "wikilink")[马谡和](../Page/马谡.md "wikilink")[曹魏主帅](../Page/曹魏.md "wikilink")[張郃在](../Page/張郃.md "wikilink")[街亭作战](../Page/街亭.md "wikilink")。

### 区划沿革

[宋时今县境内有秦寨](../Page/宋朝.md "wikilink")、鸡川寨、陇城寨。[金](../Page/金朝.md "wikilink")[正隆二年](../Page/正隆.md "wikilink")（1157年）在今县境内设秦安、[鸡川](../Page/鸡川县.md "wikilink")、[陇城三县](../Page/陇城县.md "wikilink")，在原寨名“秦”字上加一“安”字，是为秦安县名之始。[元至元七年](../Page/元朝.md "wikilink")（1270年）鸡川、陇城二县并入秦安县，属[秦州](../Page/秦州_\(古代\).md "wikilink")。明清沿之，属[巩昌府秦州](../Page/巩昌府.md "wikilink")。

## 行政区划

下辖17个[镇](../Page/镇.md "wikilink")：

。

## 特产

秦安是中国[蜜桃之乡](../Page/蜜桃.md "wikilink")。由于秦安特殊的气候和土壤，秦安蜜桃色泽艳丽，甘甜可口。

  - [秦安县政府网页](https://web.archive.org/web/20170730060315/http://qinan.gov.cn/)

[甘/甘肃](../Page/category:国家级贫困县.md "wikilink")

[秦安县](../Category/秦安县.md "wikilink") [县](../Category/天水区县.md "wikilink")
[天水](../Category/甘肃省县份.md "wikilink")