**奧迪A4**是[德國汽車製造商](../Page/德國.md "wikilink")[奧迪自](../Page/奧迪.md "wikilink")1994年末起生產的入門級豪華中级轿车，為[奧迪80和](../Page/奧迪80.md "wikilink")[奧迪90的入替車型](../Page/奧迪90.md "wikilink")。在市場上的主要競爭對手包括[BMW
3系列](../Page/BMW_3系列.md "wikilink")，[梅赛德斯-奔驰C级](../Page/梅赛德斯-奔驰C级.md "wikilink")，[凌志IS](../Page/凌志IS.md "wikilink")，[謳歌TSX以及](../Page/謳歌TSX.md "wikilink")[英菲尼迪G級](../Page/英菲尼迪G級.md "wikilink")。奧迪A4採用縱置式前置引擎，驅動方式為[前輪驅動](../Page/前輪驅動.md "wikilink")，某些型號搭配有quattro全時[四輪驅動系統](../Page/四輪驅動.md "wikilink")。迄今為止，奧迪A4總共開發了5代，全部基於[福斯的B系列平台開發](../Page/福斯.md "wikilink")。

奧迪A4的車型通常為常見的四門四座[轎車](../Page/轎車.md "wikilink")，某些國家的市場有上市[休旅車版本](../Page/休旅車.md "wikilink")。第2代以及第3代奧迪A4曾經發布過[敞篷車的版本](../Page/敞篷車.md "wikilink")，但都停產並被[奧迪A5以及](../Page/奧迪A5.md "wikilink")[奧迪S5的敞篷版代替](../Page/奧迪S5.md "wikilink")。

奧迪有開發高性能版本的A4，並命名為奧迪S4，而限量版奧迪S4又被稱為奧迪RS4。 __TOC__{{-}}

## 第一代奧迪A4 - B5 (1994-2001)

[奧迪於](../Page/奧迪.md "wikilink")1995年發表了在[福斯](../Page/福斯.md "wikilink")[B5平台基礎上開發的A](../Page/B5.md "wikilink")4
(內部編號為*Typ 8D*)；此平台與第4代
[大众帕萨特是共用的](../Page/大众帕萨特.md "wikilink")，不同的是，奧迪A4為縱置式[前置引擎](../Page/前置引擎.md "wikilink")。奧迪A4是首先以四門房車的型態示人的，於一年後才增加發布了[休旅車型](../Page/休旅車.md "wikilink")
(稱為 *Avant*)。

這一代的A4是[福斯集團旗下第一個使用其全新設計的](../Page/福斯集團.md "wikilink")1.8公升
20氣門（每缸5氣門）[直列四缸引擎的車型](../Page/直列四缸.md "wikilink")，[渦輪增壓的版本壓榨出](../Page/渦輪增壓.md "wikilink")148bhp的峰值馬力，並將峰值扭力提升到210Nm。同時，奧迪也在這一代A4中引入了當時全新開發的Tiptronic[手自一體變速箱](../Page/手自一體變速箱.md "wikilink")。

### B5 改良款 (1998-2001)

這一款改良的奧迪A4 B5首先亮相在1997年法蘭克福車展上，並在第二年在歐洲市場全面上市。這一改良款使用2.8公升
30氣門V6引擎入替了原來的2.8公升
12氣門V6引擎，主要是因為當時福斯將每缸5氣門的技術延伸到了V6引擎中。

奧迪同期還發布了基於A4開發的高性能版本奧迪S4。
[Audi_s4_b5_nogaroblau.jpg](https://zh.wikipedia.org/wiki/File:Audi_s4_b5_nogaroblau.jpg "fig:Audi_s4_b5_nogaroblau.jpg")
[2001_Audi_RS4_B5_Avant_-_Flickr_-_The_Car_Spy_(23).jpg](https://zh.wikipedia.org/wiki/File:2001_Audi_RS4_B5_Avant_-_Flickr_-_The_Car_Spy_\(23\).jpg "fig:2001_Audi_RS4_B5_Avant_-_Flickr_-_The_Car_Spy_(23).jpg")

### 奧迪A4 B5產品線所使用的引擎

| A4 引擎類型                                                                                                                       | 峰值馬力   | 峰值扭力  | 最高時速    | 0-100公里加速時間 | 生產年份      |
| ----------------------------------------------------------------------------------------------------------------------------- | ------ | ----- | ------- | ----------- | --------- |
| 1.6公升 8氣門 單[頂置凸輪軸](../Page/頂置凸輪軸.md "wikilink") [直列四缸發動機](../Page/直列四缸.md "wikilink")                                         | 100bhp | 140Nm | 191km/h | 11.9 s      | 1994-2001 |
| 1.8公升 20氣門 雙[頂置凸輪軸](../Page/頂置凸輪軸.md "wikilink") [直列四缸發動機](../Page/直列四缸.md "wikilink")                                        | 123bhp | 173Nm | 205km/h | 10.5 s      | 1994-2001 |
| 1.8公升 20氣門 雙[頂置凸輪軸](../Page/頂置凸輪軸.md "wikilink") [渦輪增壓](../Page/渦輪增壓.md "wikilink") [直列四缸發動機](../Page/直列四缸.md "wikilink")     | 148bhp | 210Nm | 222km/h | 8.3 s       | 1994-2001 |
| 1.8公升 20氣門 雙[頂置凸輪軸](../Page/頂置凸輪軸.md "wikilink") [渦輪增壓](../Page/渦輪增壓.md "wikilink") [直列四缸發動機](../Page/直列四缸.md "wikilink")     | 178bhp | 235Nm | 233km/h | 7.9 s       | 1997-2001 |
| 2.4公升 30氣門 雙[頂置凸輪軸](../Page/頂置凸輪軸.md "wikilink") [V6發動機](../Page/V6發動機.md "wikilink")                                         | 163bhp | 230Nm | 225km/h | 8.4 s       | 1997-2001 |
| 2.6公升 12氣門 單[頂置凸輪軸](../Page/頂置凸輪軸.md "wikilink") [V6發動機](../Page/V6發動機.md "wikilink")                                         | 148bhp | 225Nm | 220km/h | 9.1 s       | 1994-1997 |
| S4 2.7公升 30氣門 雙[頂置凸輪軸](../Page/頂置凸輪軸.md "wikilink") 雙[渦輪增壓](../Page/渦輪增壓.md "wikilink") [V6發動機](../Page/V6發動機.md "wikilink")  | 261bhp | 400Nm | 250km/h | 5.7 s       | 1997-2001 |
| RS4 2.7公升 30氣門 雙[頂置凸輪軸](../Page/頂置凸輪軸.md "wikilink") 雙[渦輪增壓](../Page/渦輪增壓.md "wikilink") [V6發動機](../Page/V6發動機.md "wikilink") | 376bhp | 440Nm | 262km/h | 4.9 s       | 1999-2001 |
| 2.8公升 12氣門 單[頂置凸輪軸](../Page/頂置凸輪軸.md "wikilink") [V6發動機](../Page/V6發動機.md "wikilink")                                         | 172bhp | 245Nm | 230km/h | 8.2 s       | 1994-1997 |
| 2.8公升 30氣門 雙[頂置凸輪軸](../Page/頂置凸輪軸.md "wikilink") [V6發動機](../Page/V6發動機.md "wikilink")                                         | 190bhp | 280Nm | 240km/h | 7.4 s       | 1997-2001 |
| 1.9公升 8氣門 單[頂置凸輪軸](../Page/頂置凸輪軸.md "wikilink") 柴油直噴 [直列四缸發動機](../Page/直列四缸.md "wikilink")                                    | 74bhp  | 150Nm | 158km/h |             | 1996-2001 |
| 1.9公升 8氣門 單[頂置凸輪軸](../Page/頂置凸輪軸.md "wikilink") 柴油直噴 [渦輪增壓](../Page/渦輪增壓.md "wikilink") [直列四缸發動機](../Page/直列四缸.md "wikilink") | 89bhp  | 202Nm | 168km/h | 13.3 s      | 1994-1997 |
| 1.9公升 8氣門 單[頂置凸輪軸](../Page/頂置凸輪軸.md "wikilink") 柴油直噴 [渦輪增壓](../Page/渦輪增壓.md "wikilink") [直列四缸發動機](../Page/直列四缸.md "wikilink") | 89bhp  | 210Nm | 168km/h | 13.3 s      | 1997-2001 |
| 1.9公升 8氣門 單[頂置凸輪軸](../Page/頂置凸輪軸.md "wikilink") 柴油直噴 [渦輪增壓](../Page/渦輪增壓.md "wikilink") [直列四缸發動機](../Page/直列四缸.md "wikilink") | 108bhp | 225Nm | 183km/s | 11.3 s      | 1994-1997 |
| 1.9公升 8氣門 單[頂置凸輪軸](../Page/頂置凸輪軸.md "wikilink") 柴油直噴 [渦輪增壓](../Page/渦輪增壓.md "wikilink") [直列四缸發動機](../Page/直列四缸.md "wikilink") | 108bhp | 235Nm | 183km/h | 11.3 s      | 1997-2000 |
| 1.9公升 8氣門 單[頂置凸輪軸](../Page/頂置凸輪軸.md "wikilink") 柴油直噴 [渦輪增壓](../Page/渦輪增壓.md "wikilink") [直列四缸發動機](../Page/直列四缸.md "wikilink") | 113bhp | 285Nm | 185km/h | 10.5 s      | 2000-2001 |
| 2.5公升 24氣門 雙[頂置凸輪軸](../Page/頂置凸輪軸.md "wikilink") [V6發動機](../Page/V6發動機.md "wikilink")                                         | 148bhp | 310Nm | 210km/h | 9.0 s       | 1997-2001 |

__TOC__{{-}}

## 第二代奧迪A4 - B6 (2001-2005)

[奧迪於](../Page/奧迪.md "wikilink")2000年末發布了基於[福斯B](../Page/福斯.md "wikilink")6平台開發的第2代奧迪A4，內部編號為Typ
8E/8H。受同代[奧迪A6的影響](../Page/奧迪A6.md "wikilink")，這一代的奧迪A4上市後獲得了好評。除1.6公升[直列四缸引擎外](../Page/直列四缸.md "wikilink")，其它型號的引擎均獲得了[擴缸](../Page/擴缸.md "wikilink")，或調校出更高的動力輸出。這一代A4的quattro全時[四輪驅動系統默認的前後扭力分佈被調校至](../Page/四輪驅動.md "wikilink")50:50，此外[博世集團開發的ESP](../Page/博世.md "wikilink")
5.7[電子穩定程序](../Page/電子穩定程序.md "wikilink")，包括ABS[防鎖死剎車系統](../Page/防鎖死剎車系統.md "wikilink")，[剎車輔助以及EBD](../Page/剎車輔助.md "wikilink")[電子制動力分配系統也在這代A](../Page/電子制動力分配系統.md "wikilink")4中成為了標準配置。

至到2002年，奧迪又引用了其全新設計的2.0公升燃油缸內直噴直列四缸引擎，並將一款名為Multitronic的CVT無段自動變速器引入到產品線中，且在一年後發布了新款的奧迪S4。

這一代的奧迪A4通過了歐盟NCAP的碰撞測試，其對乘客的保護成績較上一代奧迪A4獲得的三星成績提高了一顆星至四星，但卻忽略了對行人的衝撞保護，從上一代A4獲得的兩星成績降至一星。
[2003-2005_Audi_S4_(B6)_sedan_03.jpg](https://zh.wikipedia.org/wiki/File:2003-2005_Audi_S4_\(B6\)_sedan_03.jpg "fig:2003-2005_Audi_S4_(B6)_sedan_03.jpg")
[A4_Avant_B6_in_Poland.jpg](https://zh.wikipedia.org/wiki/File:A4_Avant_B6_in_Poland.jpg "fig:A4_Avant_B6_in_Poland.jpg")

__TOC__{{-}}

## 第三代奧迪A4 - B7 (2005-2008)

2004年末，[奧迪正式發布了此款代號B](../Page/奧迪.md "wikilink")7但實際依然沿用了[福斯B](../Page/福斯.md "wikilink")6平台開發的第三代奧迪A4，所以內部編號仍然是Typ
8E/8H。雖然這一代的奧迪A4仍然基於舊的福斯B6平台而開發，但此代奧迪A4較上一代奧迪A4的改進之處還是非常多。外表上，從這一代奧迪A4開始，前進氣格柵的鍍鉻外框被設計為一直延伸到接近前保險槓的底部，誇張的表現手法使之格外引人注目。基於整個懸吊系統的改進，駕駛時轉動方向盤的手感讓人更覺沉穩，加上[博世集團全新的ESP](../Page/博世.md "wikilink")
8.0[電子穩定程序系統的加持](../Page/電子穩定程序.md "wikilink")，此代奧迪A4的[操控性有了明顯的提高](../Page/操控.md "wikilink")。

在這個時期，或許是因為[福斯的燃油缸內直噴技術慢慢地趨於成熟](../Page/福斯.md "wikilink")，奧迪將此技術延伸到了幾乎奧迪A4全線的引擎型號當中，使之這一代的奧迪A4在動力輸出表現以及燃油經濟性方面都有了上佳的表現。
[Audi_S4_Cabriolet.jpg](https://zh.wikipedia.org/wiki/File:Audi_S4_Cabriolet.jpg "fig:Audi_S4_Cabriolet.jpg")
[2005-03-04_Motorshow_Geneva_052.JPG](https://zh.wikipedia.org/wiki/File:2005-03-04_Motorshow_Geneva_052.JPG "fig:2005-03-04_Motorshow_Geneva_052.JPG")
__TOC__{{-}}

## 第四代奧迪A4 - B8 (2008-2015)

[2012_Audi_S4_--_02-29-2012.JPG](https://zh.wikipedia.org/wiki/File:2012_Audi_S4_--_02-29-2012.JPG "fig:2012_Audi_S4_--_02-29-2012.JPG")
[2012-03-07_Motorshow_Geneva_4621.JPG](https://zh.wikipedia.org/wiki/File:2012-03-07_Motorshow_Geneva_4621.JPG "fig:2012-03-07_Motorshow_Geneva_4621.JPG")
__TOC__{{-}}

## 第五代奧迪A4-B9 2015–今

## 中国市场

奥迪A4
B6、B7曾在长春生产。如[奥迪A6L一样](../Page/奥迪A6.md "wikilink")，09年在[长春](../Page/长春.md "wikilink")[一汽大众生产的奥迪A](../Page/一汽大众.md "wikilink")4
B8为专为中国市场设计的加长版，命名为[奥迪A4L](../Page/奥迪A4.md "wikilink")。

## 相關條目

  - [奧迪S4](../Page/奧迪S4.md "wikilink")
  - [奧迪RS4](../Page/奧迪RS4.md "wikilink")

## 外部連結

  - [德國官方網站](https://web.archive.org/web/20061004183719/http://www.audi.de/audi/de/de2/neuwagen/a4.html)
  - [美國官方網站](https://web.archive.org/web/20070612164621/http://www.audiusa.com/audi/us/en2/new_cars/Audi_A4.html)
  - [英國官方網站](https://web.archive.org/web/20150925222802/http://a4.audi-cars-online.co.uk/)

[Category:1994年面世的汽車](../Category/1994年面世的汽車.md "wikilink")
[Category:奧迪車輛](../Category/奧迪車輛.md "wikilink")
[Category:敞篷車](../Category/敞篷車.md "wikilink")
[Category:中型車](../Category/中型車.md "wikilink")
[Category:旅行車](../Category/旅行車.md "wikilink")
[Category:CVT變速系統車輛](../Category/CVT變速系統車輛.md "wikilink")