**Boehm-Demers-Weiser garbage collector**，也就是著名的**Boehm
GC**，是計算機應用在C/C++語言上的一個保守的[垃圾回收器](../Page/垃圾回收器.md "wikilink")，可應用於許多經由C/C++開發的專案，同時也適用於其它執行環境的各類程式語言，包括了[GNU版Java編譯器執行環境](../Page/GNU.md "wikilink")，以及[Mono的](../Page/Mono_\(software\).md "wikilink")[Microsoft
.NET移植平台](../Page/Microsoft_.NET.md "wikilink")。同時支援許多的作業平台，如各種[Unix作業系統](../Page/Unix.md "wikilink")，微軟的作業系統（[Microsoft
Windows](../Page/Microsoft_Windows.md "wikilink")），以及麥金塔上的作業系統（[Mac OS
X](../Page/Mac_OS_X.md "wikilink")），還有更進一步的功能，例如：漸進式收集（incremental
collection），平行收集（parallel collection）以及終結語意的變化（variety of
[finalizer](../Page/finalizer.md "wikilink") semantics）。

## 範例

垃圾收集器作用於未變性的（unmodified）C程式，只要簡單的將malloc呼叫用GC_malloc取代，將realloc取代為GC_realloc呼叫，如此一來便不需要使用到free的函式。下列的程式碼展示出如何用Boehm取代傳統的[malloc以及free](../Page/malloc.md "wikilink")。[1](http://www.hpl.hp.com/personal/Hans_Boehm/gc/simple_example.html).

``` c
 #include "gc.h"
 #include <assert.h>
 #include <stdio.h>

 int main()
 {
     int i;

     GC_INIT();
     for(i = 0; i < 10000000; I)
     {
         int **p = (int **) GC_MALLOC(sizeof (int *));
         int *q = (int *) GC_MALLOC_ATOMIC(sizeof (int));

         assert(*p == 0);
         *p = (int *) GC_REALLOC(q, 2 * sizeof (int));
         if(i % 100000 == 0)
             printf("Heap size = %d\n", GC_get_heap_size());
     }

     return 0;
 }
```

## 外部链接

  -
  -
  - [Git repo for BoehmGC development](https://github.com/ivmai/bdwgc/)

  - [Transparent Programmer-Directed Garbage Collection for C++, Hans-J.
    Boehm and Michael
    Spertus](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2310.pdf)

  - [Using the C/C++ Garbage Collection
    Library](https://www.assembla.com/spaces/hito1/documents/biqNQMOhur3AwCab7jnrAJ/download/UsingtheC_CGarbageCollectionLibrarylibgc.pdf)

  - [Dr. Dobbs The Boehm Collector for C and C++, Gene Michael Stover,
    March 01, 2003](http://www.drdobbs.com/the-boehm-collector-for-c-and-c/184401632)

{{-}}

[Category:自动内存管理](../Category/自动内存管理.md "wikilink")
[Category:C++函式庫](../Category/C++函式庫.md "wikilink")
[Category:C函式庫](../Category/C函式庫.md "wikilink")
[Category:自由编译器和解释器](../Category/自由编译器和解释器.md "wikilink")
[Category:内存管理软件](../Category/内存管理软件.md "wikilink")