**凱神星**（**12
Victoria**）是位於小行星帶上的一顆[小行星](../Page/小行星.md "wikilink")。由[约翰·罗素·欣德發現於](../Page/约翰·罗素·欣德.md "wikilink")1850年9月13日。

凱神星的名字来自[罗马神话中的胜利女神](../Page/罗马神话.md "wikilink")[维多利亚](../Page/维多利亚_\(神话\).md "wikilink")。

## 參考文獻

[Category:小行星带天体](../Category/小行星带天体.md "wikilink")
[Category:1850年发现的小行星](../Category/1850年发现的小行星.md "wikilink")