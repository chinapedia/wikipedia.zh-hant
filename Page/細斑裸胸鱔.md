**細斑裸胸鱔**，又名**花鰭裸胸鯙**，俗名薯鰻、錢鰻，為[輻鰭魚綱](../Page/輻鰭魚綱.md "wikilink")[鰻鱺目](../Page/鰻鱺目.md "wikilink")[鯙亞目](../Page/鯙亞目.md "wikilink")[鯙科的其中一個](../Page/鯙科.md "wikilink")[種](../Page/種.md "wikilink")。

## 分布

本魚分布於[印度](../Page/印度.md "wikilink")[太平洋區](../Page/太平洋.md "wikilink")，包括[紅海](../Page/紅海.md "wikilink")、[馬達加斯加](../Page/馬達加斯加.md "wikilink")、[模里西斯](../Page/模里西斯.md "wikilink")、[塞席爾群島](../Page/塞席爾群島.md "wikilink")、[印度](../Page/印度.md "wikilink")、[緬甸](../Page/緬甸.md "wikilink")、[泰國](../Page/泰國.md "wikilink")、[越南](../Page/越南.md "wikilink")、[中國](../Page/中國.md "wikilink")、[台灣](../Page/台灣.md "wikilink")、南[日本](../Page/日本.md "wikilink")、[印尼](../Page/印尼.md "wikilink")、[菲律賓](../Page/菲律賓.md "wikilink")、[帛琉](../Page/帛琉.md "wikilink")、[關島](../Page/關島.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")、[密克羅尼西亞](../Page/密克羅尼西亞.md "wikilink")、[新喀里多尼亞](../Page/新喀里多尼亞.md "wikilink")、[馬紹爾群島](../Page/馬紹爾群島.md "wikilink")、[薩摩亞群島](../Page/薩摩亞群島.md "wikilink")、[東加](../Page/東加.md "wikilink")、[法屬波里尼西亞等海域](../Page/法屬波里尼西亞.md "wikilink")。

## 深度

水深0至30公尺。

## 特徵

本魚體呈米色，體表散佈許多大小不一的不規則黑斑，身體前半部的黑斑較稀疏，黑斑較小，大多小於斑間距，頭部則只散佈著一些小黑點。後半部黑斑較大且較密，大多大於斑間距。有時黑斑會三兩連成帶狀。斑點無淡色邊緣。表皮厚且光滑無鱗片，能分泌黏液，無胸鰭，背鰭、臀鰭與尾鰭相連呈皮膜狀，尾部側扁，體長可達80公分。牙齒尖銳，需小心咬傷。

## 生態

本魚性情兇猛，為晝伏夜出的魚類，棲息在珊瑚礁或岩礁的縫隙處，幼魚則棲息在淺水處，嗅覺靈敏但視力不佳，具領域性，游泳能力不強，以魚和[甲殼類為食](../Page/甲殼類.md "wikilink")。

## 經濟利用

食用魚，可飼養以供觀賞。

## 参考文献

  - [台灣魚類資料庫](http://fishdb.sinica.edu.tw/)

  -
  - {{ cite book | title = 觀賞魚圖鑑 | publisher = 貓頭鷹出版社 | date = 1996年6月
    }}

  -
[Category:食用鱼](../Category/食用鱼.md "wikilink")
[Category:觀賞魚](../Category/觀賞魚.md "wikilink")
[fimbriatus](../Category/裸胸鱔屬.md "wikilink")