[Scomber_scombrus.png](https://zh.wikipedia.org/wiki/File:Scomber_scombrus.png "fig:Scomber_scombrus.png")
[Scomber_scombrus.jpg](https://zh.wikipedia.org/wiki/File:Scomber_scombrus.jpg "fig:Scomber_scombrus.jpg")
**鯖魚**（學名：、），又名**青花魚**、**花飛**、**油胴魚**，古名**鮐**、**𩼅**。是[鯖屬的模式種](../Page/鯖屬.md "wikilink")，是一種很常見的可[食用魚類](../Page/食用魚.md "wikilink")，出沒於西[太平洋及](../Page/太平洋.md "wikilink")[大西洋的海岸附近](../Page/大西洋.md "wikilink")，喜群居。在[中文语境下要注意区别于](../Page/中文.md "wikilink")[中国的](../Page/中国.md "wikilink")[四大家鱼之一的](../Page/四大家鱼.md "wikilink")[青鱼](../Page/青鱼.md "wikilink")，在浙江沿海地区，此鱼被称为**青占鱼**，也常常写做**青鱼**。鲭鱼平均身长30至50厘米，寿命最长可至11年，它以吞噬浮游生物及[鲟鱼](../Page/鲟鱼.md "wikilink")、[鳕鱼和](../Page/鳕鱼.md "wikilink")[鲱鱼所产的卵为生](../Page/鲱鱼.md "wikilink")。\[1\]\[2\]。

## 生態

本魚生活在溫帶及寒帶的[大陸棚](../Page/大陸棚.md "wikilink")，成群活動，冬季時會游至深水域越冬，春天時游至沿岸海域，屬肉食性，以小魚及浮游動物為食。

## 食用方式

因鯖魚易腐難藏，且具有特殊腥味，故少有生食，多數情況下都是以[香料或](../Page/香料.md "wikilink")[醋醃漬鯖魚](../Page/醋.md "wikilink")，以達成保存與去腥的效果。另外由于它的肉质中含有较多脂肪因此特别适合熏烤\[3\]。和[沙丁鱼及](../Page/沙丁鱼.md "wikilink")[金枪鱼一样](../Page/金枪鱼.md "wikilink")，鲭鱼还常被人制成[鱼罐头食用](../Page/罐头.md "wikilink")\[4\]。

同時[日本或](../Page/日本.md "wikilink")[台灣皆有出產](../Page/台灣.md "wikilink")[鹽漬鯖魚](../Page/鹽漬鯖魚.md "wikilink")。以[番茄汁為主要原料製造的茄汁鯖魚罐頭在](../Page/番茄醬.md "wikilink")[台灣非常受歡迎](../Page/台灣.md "wikilink")\[5\]。

鯖魚也是[舟山等](../Page/舟山.md "wikilink")[浙江沿海较为常见的食用鱼](../Page/浙江.md "wikilink")，最常见烹调方式为[红烧](../Page/红烧.md "wikilink")，或添加[梅干菜红烧](../Page/梅干菜.md "wikilink")。

## 註解

## 參考文獻

[SS](../Category/IUCN無危物種.md "wikilink")
[SS](../Category/食用鱼.md "wikilink")
[scombrus](../Category/鯖屬.md "wikilink")
[Category:大西洋鱼类](../Category/大西洋鱼类.md "wikilink")
[Category:太平洋鱼类](../Category/太平洋鱼类.md "wikilink")

1.  [宜蘭南方澳](http://www.epochtimes.com/b5/7/3/17/n1649215.htm)
2.  [趙生健康網](http://www.chiusang.com.hk/seafood/seafood_1_09.htm)
3.  [烤鯖魚](http://www.wretch.cc/blog/bearwifelove/14527308)
4.  [可口網鲭鱼的3种用法](http://www.keko.com.cn/material/material_1134626516000_884.html)
5.  [鯖魚的出路](http://www.ctnet.com.tw/nan/page10-5.htm)