**威廉斯堡大橋**是一條位於[美國](../Page/美國.md "wikilink")[紐約州](../Page/紐約州.md "wikilink")[紐約市橫跨](../Page/紐約市.md "wikilink")[東河的](../Page/東河_\(紐約市\).md "wikilink")[懸索橋](../Page/懸索橋.md "wikilink")，連接[曼哈頓的](../Page/曼哈頓.md "wikilink")[下東城與](../Page/下東城.md "wikilink")[布魯克林的](../Page/布魯克林.md "wikilink")[威廉斯堡](../Page/威廉斯堡.md "wikilink")，在曼哈頓與[地兰西街](../Page/地兰西街.md "wikilink")（Delancey
Street）交匯，在布魯克林與[布魯克林-皇后區快速公路和Broadway交會點附近交匯](../Page/布魯克林-皇后區快速公路.md "wikilink")。過去曾被給予紐約州道27A與[78號州際公路的編號](../Page/78號州際公路.md "wikilink")，但是因為[下曼哈頓快速公路沒有付諸實行](../Page/下曼哈頓快速公路.md "wikilink")，I-78也從未真正的成為威廉斯堡大橋的編號。

威廉斯堡大橋是在1896年開工，其總工程師為（Leffert L. Buck），建築師為（Henry
Hornbostel），最後在1903年12月19日通車，而總經費為一千兩百萬美元。威廉斯堡大橋是在當時世界上最大的懸索橋，直到[熊山大橋在](../Page/熊山大橋.md "wikilink")1924年通車之後才退位。橋本身的結構並不像傳統的懸索橋一樣，而是中間主跨距部分是由懸索來支撐，但是兩邊連接陸地與中間主跨的橋段卻是以桁架所構成。主跨距約為長，橋全長為，橋面寬，在中間點的橋高為，橋塔高。

威廉斯堡大橋與[曼哈頓大橋是紐約市兩條仍舊載有汽車與鐵路的橋](../Page/曼哈頓大橋.md "wikilink")。除了現在連接[紐約地鐵的](../Page/紐約地鐵.md "wikilink")[BMT納蘇街線與](../Page/BMT納蘇街線.md "wikilink")[BMT牙買加線之外](../Page/BMT牙買加線.md "wikilink")，過去曾經有過兩條街車鐵軌。橋的開通使得原本位於河兩岸的渡輪逐漸消失。威廉斯堡大橋自1980年代開始便一直有不停的維修工程，這是主要因為數十年來一直推遲應有的維修所造成經年累月的損壞。

位於威廉斯堡大橋中央的鐵路軌道源本是由[布魯克林-曼哈頓運輸股份有限公司](../Page/布魯克林-曼哈頓運輸股份有限公司.md "wikilink")（）所使用，而今天是由[紐約地鐵](../Page/紐約地鐵.md "wikilink")[J](../Page/紐約地鐵J線與Z線.md "wikilink")[M](../Page/紐約地鐵M線.md "wikilink")[Z線所使用](../Page/紐約地鐵J線與Z線.md "wikilink")。

## 圖片

[File:WilliamsburgBridge1903.jpg|1903年時通車日當晚的煙火](File:WilliamsburgBridge1903.jpg%7C1903年時通車日當晚的煙火)。
<File:USA> williamsburg bridge NY.jpg|自曼哈頓望威廉斯堡大橋。
[File:NYCSub_JMZ_Williamsburg_2.jpg|正行經威廉斯堡大橋的J線列車](File:NYCSub_JMZ_Williamsburg_2.jpg%7C正行經威廉斯堡大橋的J線列車)。
[File:Above_Williamsburg_Bridge.jpg|自曼哈頓的下東城望威廉斯堡大橋](File:Above_Williamsburg_Bridge.jpg%7C自曼哈頓的下東城望威廉斯堡大橋)。

## 相关条目

  - [世界悬索桥列表](../Page/世界悬索桥列表.md "wikilink")

## 其他資訊

  - [Bridges at New York City
    DOT](https://web.archive.org/web/20071020045422/http://www.nyc.gov/html/dot/html/motorist/bridges.html)

  - 裡有關[威廉斯堡大橋](http://en.structurae.de/structures/data/index.cfm?ID=s0000514)的資料

  - \[<http://memory.loc.gov/cgi-bin/query/r?ammem/aap,aaeo,aasm,magbell,papr,gottscho,bbcards,bbpix,ncr,cowellbib,calbkbib,gmd,cwar,consrvbib,bdsbib,coolbib,dag,musdibib,detr,mcc,papr,aep,wpa,dcm,fsaall,mgw,raelbib,hh,gottlieb,alad,hlaw,gmd,lomaxbib,gmd,papr,fpnas,gmd,papr,ngp,gmd,pan,vv,presp,rbpebib,qlt,gmd,nfor,relpet,papr,papr,mussm,dukesm,scsm,amss,runyon,papr,denn,mtj,lhbumbib,varstg,toddbib,nawbib,suffrg,horyd,papr,wtc>,:@field(DOCID+@lit(NY1263))
    HAER\] [美國國會圖書館有關威廉斯堡大橋的頁面](../Page/美國國會圖書館.md "wikilink")。

  - [nycroads.com](http://www.nycroads.com/crossings/williamsburg/)
    威廉斯堡大橋頁

  -
[分類:桁架橋](../Page/分類:桁架橋.md "wikilink")

[Category:美国悬索桥](../Category/美国悬索桥.md "wikilink")
[W](../Category/紐約市橋樑.md "wikilink")
[W](../Category/布魯克林建築物.md "wikilink")
[Category:1903年完工橋梁](../Category/1903年完工橋梁.md "wikilink")
[Category:美国公路铁路两用桥](../Category/美国公路铁路两用桥.md "wikilink")
[Category:桥梁之最](../Category/桥梁之最.md "wikilink")
[Category:曼哈顿建筑物](../Category/曼哈顿建筑物.md "wikilink")
[Category:曼哈頓交通](../Category/曼哈頓交通.md "wikilink")