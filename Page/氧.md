**氧**（[IUPAC名](../Page/IUPAC.md "wikilink")：）是一種[化學元素](../Page/化學元素.md "wikilink")，符號為**O**，[原子序為](../Page/原子序.md "wikilink")8，在[元素週期表中屬於](../Page/元素週期表.md "wikilink")[氧族](../Page/氧族元素.md "wikilink")。氧屬於[非金屬](../Page/非金屬.md "wikilink")，是具有高[反應性的](../Page/化學反應.md "wikilink")[氧化劑](../Page/氧化劑.md "wikilink")，能夠與大部分元素以及其他[化合物形成](../Page/化合物.md "wikilink")[氧化物](../Page/氧化物.md "wikilink")。\[1\]氧在宇宙中的總質量在所有元素中位列第三，僅居[氫和](../Page/氫.md "wikilink")[氦之下](../Page/氦.md "wikilink")。\[2\]在[標準溫度和壓力下](../Page/標準溫度和壓力.md "wikilink")，兩個氧原子会自然[鍵合](../Page/化學鍵.md "wikilink")，形成無色無味的[氧氣](../Page/氧氣.md "wikilink")，即雙原子氧（<chem>O2</chem>）。氧氣是[地球大氣層的主要成分之一](../Page/地球大氣層.md "wikilink")，在體積上佔20.8%。\[3\][地球地殼中近一半的質量都是由氧和氧化物所組成](../Page/地球地殼.md "wikilink")。\[4\]

氧是細胞[呼吸作用中重要的元素](../Page/呼吸作用.md "wikilink")。在[生物體中](../Page/生物體.md "wikilink")，主要[有機分子](../Page/有機分子.md "wikilink")，如[蛋白質](../Page/蛋白質.md "wikilink")、[核酸](../Page/核酸.md "wikilink")、[碳水化合物和](../Page/碳水化合物.md "wikilink")[脂肪等](../Page/脂肪.md "wikilink")，還有組成動物外殼、牙齒和骨骼的[無機化合物](../Page/無機化合物.md "wikilink")，都含有氧原子。生物體絕大部分的質量都由含氧原子的水組成。[光合作用利用陽光的能量把水和二氧化碳轉化為氧氣](../Page/光合作用.md "wikilink")。氧氣的化學反應性強，容易與其他元素結合，所以大氣層中的氧氣成分只能通過生物的光合作用持續補充。[臭氧](../Page/臭氧.md "wikilink")（<chem>O3</chem>）是氧元素的另一種[同素異構體](../Page/同素異構體.md "wikilink")，能夠較好地吸收中[紫外線輻射](../Page/紫外線.md "wikilink")。位於高海拔的[臭氧層有助阻擋紫外線](../Page/臭氧層.md "wikilink")，從而保護生物圈。不過，在地表上的臭氧屬於污染物，為[霧霾的副產品之一](../Page/霧霾.md "wikilink")。在[低地球軌道高度的單原子氧足以對航天器造成](../Page/低地球軌道.md "wikilink")[腐蝕](../Page/腐蝕.md "wikilink")。\[5\]

[卡爾·威廉·舍勒於](../Page/卡爾·威廉·舍勒.md "wikilink")1773年或之前在[烏普薩拉最早發現氧元素](../Page/烏普薩拉.md "wikilink")。[約瑟夫·普利斯特里亦於](../Page/約瑟夫·普利斯特里.md "wikilink")1774年在[威爾特郡獨立發現氧](../Page/威爾特郡.md "wikilink")，因為其成果的發表日期較舍勒早，所以一般被譽為氧的發現者。1777年，[安東萬-羅倫·德·拉瓦節進行了一系列有關氧的實驗](../Page/安東萬-羅倫·德·拉瓦節.md "wikilink")，推翻了當時用於解釋[燃燒和](../Page/燃燒.md "wikilink")[腐蝕的](../Page/腐蝕.md "wikilink")[燃素說](../Page/燃素說.md "wikilink")。他也提出了氧的現用[IUPAC名稱](../Page/IUPAC.md "wikilink")「oxygen」，\[6\]源自[希臘語中的](../Page/希臘語.md "wikilink")「ὀξύς」（oxys，尖銳，指[酸](../Page/酸.md "wikilink")）和「-γενής」（-genes，產生者）。這是因為命名之時，人們曾以為所有酸都必須含有氧。許多化學詞彙都在清末由[徐壽等人的翻譯書籍傳入中國](../Page/徐壽.md "wikilink")，其中原法文元素名「oxygène」被譯為「養」\[7\]，後譯為「氱」\[8\]，最終演變為今天的中文名「氧」。

氧的應用包括[暖氣](../Page/暖通空調.md "wikilink")、[內燃機](../Page/內燃機.md "wikilink")、[鋼鐵](../Page/鋼鐵.md "wikilink")、[塑料和](../Page/塑料.md "wikilink")[布料的生產](../Page/布料.md "wikilink")、金屬[氣焊和氣割](../Page/氣焊和氣割.md "wikilink")、[火箭推進劑](../Page/火箭推進劑.md "wikilink")、及[航空器](../Page/航空器.md "wikilink")、[潛艇](../Page/潛艇.md "wikilink")、[載人航天器和](../Page/載人航天.md "wikilink")[潛水所用的](../Page/潛水.md "wikilink")[生命保障系統](../Page/生命保障系統.md "wikilink")。

## 歷史

### 早期實驗

[Philos_experiment_of_the_burning_candle.PNG](https://zh.wikipedia.org/wiki/File:Philos_experiment_of_the_burning_candle.PNG "fig:Philos_experiment_of_the_burning_candle.PNG")所做的實驗啟發了後世對燃燒與空氣間關係的研究。\]\]

人類最早研究[燃燒和空氣之間的關係](../Page/燃燒.md "wikilink")，可追溯至前2世紀[古希臘學者](../Page/古希臘.md "wikilink")[拜占庭的費隆所做的實驗](../Page/費隆_\(拜占庭\).md "wikilink")。費隆在著作《氣動學》（）中寫道，如果將點燃的蠟燭置於水中，再用瓶子蓋住蠟燭至水面，水面就會在瓶頸內上升。\[9\]費隆錯誤地推論，有一部分空氣經燃燒轉換成[火元素](../Page/古典元素.md "wikilink")，因此可以經玻璃中的小孔逃逸出去。過千年以後，[李安納度·達文西在費隆的基礎上又觀察到](../Page/李安納度·達文西.md "wikilink")，燃燒和[呼吸都會消耗部分空氣](../Page/呼吸_\(生理學\).md "wikilink")。\[10\]

17世紀，[羅拔·波義耳證明空氣是燃燒過程必需的物質](../Page/羅拔·波義耳.md "wikilink")。進一步證明，燃燒只需要空氣中的一種成分，他稱之為「銅硝石之靈」（）。\[11\]在其中一個實驗中，他發現把點燃的蠟燭或老鼠放在蓋著水面的密封容器內，水面都會上升，而且在蠟燭熄滅或老鼠死亡時，容器內空氣的四分之一容量會被水取代。\[12\]他得出的結論是，燃燒和呼吸都會消耗「銅硝石之靈」。

梅奧發現[銻在燃燒之後重量有所提升](../Page/銻.md "wikilink")，由此論證「銅硝石之靈」一定是在燃燒過程中和銻結合了。他也認為，肺臟能將「銅硝石之靈」從空氣中分離出來，運送到血液之中，而動物的熱量和肌肉運動都是「銅硝石之靈」與體內某些其他物質反應的結果。\[13\]梅奧在1668年《兩篇論文》（）中的〈論呼吸〉（）一文中發表了這一系列實驗和發現。\[14\]

### 燃素說

[Georg_Ernst_Stahl.png](https://zh.wikipedia.org/wiki/File:Georg_Ernst_Stahl.png "fig:Georg_Ernst_Stahl.png")推動了燃素說的發展和普及\]\]

[羅伯特·虎克](../Page/羅伯特·虎克.md "wikilink")、、[米哈伊爾·瓦西里耶維奇·羅蒙諾索夫和](../Page/米哈伊爾·瓦西里耶維奇·羅蒙諾索夫.md "wikilink")在17、18世紀都在實驗中產生了氧氣，但無一發現氧是一種[化學元素](../Page/化學元素.md "wikilink")。\[15\]由於有當時盛行的燃素說來解釋燃燒和腐蝕過程，因此他們都沒有發現氧氣在這些過程中重要的作用。

煉金術士[約翰·約阿希姆·貝歇爾在](../Page/約翰·約阿希姆·貝歇爾.md "wikilink")1667年建立燃素說，化學家[格奧爾格·恩斯特·斯塔爾又於](../Page/格奧爾格·恩斯特·斯塔爾.md "wikilink")1731年對燃素說作進一步修改。\[16\]根據燃素說，所有可燃物質都由兩部分組成。一部分為「燃素」，會在物質燃燒過程中釋放出來。不含燃素的部分則是物質的「純態」。\[17\]人們認為，木材、煤等可燃性高、燃燒殘留物少的物質主要都由燃素所組成，鐵等不可燃、會受腐蝕的物質則含有很少的燃素。空氣在燃素說中並無任何作用，此學說最初也未經量化實驗的驗證。大部分常見物質在燃燒過程中都會變輕，所以似乎失去了某種成分，這成為了燃素說的主要觀察基礎。\[18\]

### 發現

[Carl_Wilhelm_Scheele_from_Familj-Journalen1874.png](https://zh.wikipedia.org/wiki/File:Carl_Wilhelm_Scheele_from_Familj-Journalen1874.png "fig:Carl_Wilhelm_Scheele_from_Familj-Journalen1874.png")
瑞典藥劑師[卡爾·威廉·舍勒在](../Page/卡爾·威廉·舍勒.md "wikilink")1771至1772年間最早發現氧。他對[氧化汞和不同](../Page/氧化汞.md "wikilink")[硝酸鹽一同加熱](../Page/硝酸鹽.md "wikilink")，產生出氧氣。\[19\]\[20\]\[21\]舍勒把這種氣體產物稱為「火之氣」，因為它能支持燃燒過程。手稿《論空氣與火的化學》（）記錄了這項發現，1775年寄往出版商，1777年出版。\[22\]

[PriestleyFuseli.jpg](https://zh.wikipedia.org/wiki/File:PriestleyFuseli.jpg "fig:PriestleyFuseli.jpg")一般被譽為氧的發現者\]\]

1774年8月1日，英國教士[約瑟夫·普利斯特里把陽光聚焦到裝有](../Page/約瑟夫·普利斯特里.md "wikilink")[氧化汞的玻璃管中](../Page/氧化汞.md "wikilink")，從而釋放出一種氣體，他將其稱為「無燃素氣」（）。\[23\]他注意到，蠟燭在這一氣體中燃燒得更加旺盛，老鼠在呼吸此氣時顯得更加活躍，存活的時間也更長。在自己吸入「無燃素氣」後，普利斯特里寫道：「它在肺中的感覺和普通空氣無異，但一段時間內我感到胸腔格外輕盈舒適，頗感良好。」\[24\]這些發現在1775年發表的論文《對空氣的進一步發現》（）中有所記載。\[25\]\[26\]由於普利斯特里比舍勒更早發表論文，因此常被稱為氧氣的最早發現者。

法國化學家[安東萬-羅倫·德·拉瓦節之後也聲稱獨立發現了氧氣](../Page/安東萬-羅倫·德·拉瓦節.md "wikilink")。普利斯特里在1774年10月拜訪拉瓦節時向他講解過這項實驗如何釋放出某種新的氣體，又於1774年9月30日致信拉瓦節，講述他所發現的新物質。拉瓦節並未提及收到過此信。舍勒去世之後，人們在他的遺物之中發現了這封信的副本。\[27\]

### 拉瓦節的成果

雖然拉瓦節並沒有獨自發現氧氣，但他卻是對[氧化過程進行量化實驗並正確解釋燃燒過程的第一人](../Page/氧化還原反應.md "wikilink")。\[28\]自1774起，他利用一系列實驗推翻了燃素說，又證明普利斯特里和舍勒所生成的物質是一種[化學元素](../Page/化學元素.md "wikilink")。

[Antoine_lavoisier.jpg](https://zh.wikipedia.org/wiki/File:Antoine_lavoisier.jpg "fig:Antoine_lavoisier.jpg")推翻了燃素說\]\]

拉瓦節觀察到，在密封容器中對[錫和空氣加熱後](../Page/錫.md "wikilink")，總重量並沒有增加。在打開容器時，空氣會迅速湧入。以此可推論，加熱過程消耗了容器中的部分空氣。他也注意到，錫的重量有所提升，且重量之差和湧入的空氣之重量相同。與此類似的實驗都記錄在拉瓦節1777年出版的著作《燃燒總論》（）中。他證明空氣是兩種氣體的混合物：燃燒和呼吸過程所必需的「活氣」（），以及無助燃燒和呼吸的「死氣」（，取自希臘語，意為「無生命」），也就是[氮氣](../Page/氮氣.md "wikilink")。法語等某些歐洲語言至今仍稱氮氣為「azote」。\[29\]

1777年，拉瓦節將「活氣」改名為「oxygène」，結合希臘語詞根「」（尖銳，指酸味）和「-γενής」（產生者）。之所以詞源提到酸味，是因為他誤以為所有酸都必須含有氧。\[30\]儘管如此，「oxygen」已成為氧的[IUPAC元素名](../Page/IUPAC.md "wikilink")，在國際上通用。

英國的普利斯特里比拉瓦節更早研究氧氣，「oxygen」一詞也受到了英國科學家的反對。不過在1791年，[伊拉斯謨斯·達爾文](../Page/伊拉斯謨斯·達爾文.md "wikilink")（[查理斯·達爾文的祖父](../Page/查理斯·達爾文.md "wikilink")）的科普詩集《植物園》（）出版，其中一首名為*Oxygen*的詩以歌頌氧氣為主題，使「oxygen」進入英語詞彙之中。\[31\]

### 後期歷史

[Goddard_and_Rocket.jpg](https://zh.wikipedia.org/wiki/File:Goddard_and_Rocket.jpg "fig:Goddard_and_Rocket.jpg")，身旁為液氧汽油火箭\]\]

[約翰·道耳頓的](../Page/約翰·道耳頓.md "wikilink")[原子理論主張](../Page/原子理論#原子-分子學說.md "wikilink")，所有元素都是單原子物質，而在化合物中，不同元素的原子數都呈最簡單的整數比。例如，道耳頓假定水的公式為HO（1個氫原子對1個氧原子），從而推論氧的[原子量為氫的](../Page/原子量.md "wikilink")8倍。今天所知的實際數值約為16。\[32\]1805年，[約瑟夫·路易·蓋-呂薩克和](../Page/約瑟夫·路易·蓋-呂薩克.md "wikilink")[亞歷山大·馮·洪保德證明水是由兩份氫](../Page/亞歷山大·馮·洪保德.md "wikilink")、一份氧所組成。事實上，氫氣和氧氣都是雙原子分子。1811年，[阿莫迪歐·阿佛加德羅利用這一點](../Page/阿莫迪歐·阿佛加德羅.md "wikilink")，通過[阿佛加德羅定律推導出水的正確成分](../Page/阿佛加德羅定律.md "wikilink")。\[33\]

19世紀末，科學家發現可以通過壓縮和冷卻使空氣變為液體，再分離其中的各種成分。瑞士化學家、物理學家[拉烏爾·皮克泰利用](../Page/拉烏爾·皮克泰.md "wikilink")[串級法](../Page/串級系統.md "wikilink")，通過[蒸發液態](../Page/蒸發.md "wikilink")[二氧化硫使](../Page/二氧化硫.md "wikilink")[二氧化碳液化](../Page/二氧化碳.md "wikilink")，再通過蒸發液態二氧化碳使氧氣液化。1877年12月22日，他向[法國科學院發送電報](../Page/法國科學院.md "wikilink")，宣佈發現[液氧](../Page/液氧.md "wikilink")。\[34\]兩天後，法國物理學家[路易·保羅·卡耶泰宣佈用自己的方法](../Page/路易·保羅·卡耶泰.md "wikilink")，同樣成功製成液氧。\[35\]兩人都只產生了幾滴液氧，無法進行進一步的分析。1883年3月29日，波蘭科學家和終於製成穩定狀態下的液氧。\[36\]1891年，蘇格蘭化學家[詹姆斯·杜瓦產生了足夠的液氧做進一步研究](../Page/詹姆斯·杜瓦.md "wikilink")。\[37\]1895年，德國工程師和英國工程師分別研發出商業可行的氧氣液化過程。兩者都先降低空氣溫度，使其液化，再用[蒸餾法將不同成分逐一氣化](../Page/蒸餾法.md "wikilink")，分開捕獲。\[38\]

1901年，科學家使[乙炔和壓縮氧氣的混合物燃燒](../Page/乙炔.md "wikilink")，首次進行了[氧炔焊接](../Page/氣焊.md "wikilink")。氧炔焊接之後成為了焊接、切割金屬的常用方法。\[39\]1923年，美國科學家[羅伯特·戈達德研發出首支消耗液態推進劑的](../Page/羅伯特·戈達德.md "wikilink")[火箭推進器](../Page/火箭推進器.md "wikilink")。該火箭分別以[汽油和液氧作為燃料和](../Page/汽油.md "wikilink")[氧化劑](../Page/氧化劑.md "wikilink")。1926年3月16日，戈達德在美國[麻薩諸塞州奧本成功發射一支小型液態推進火箭](../Page/麻薩諸塞州.md "wikilink")。火箭達到每小時97公里的速度，並飛至56米的高度。\[40\]\[41\]

地球大氣中的氧氣含量在全球範圍內有稍稍下滑的趨勢，這可能和燃燒化石燃料有關。\[42\]

## 性質

### 分子結構

[Oxygen_molecule_orbitals_diagram.JPG](https://zh.wikipedia.org/wiki/File:Oxygen_molecule_orbitals_diagram.JPG "fig:Oxygen_molecule_orbitals_diagram.JPG")從低至高順序填入分子軌域，其中最高兩個電子不成對，是氧氣諸多性質的根源。\]\]

氧氣分子<chem>O2</chem>由兩個氧原子[鍵合組成](../Page/化學鍵.md "wikilink")，又稱雙原子氧。[分子軌域理論能夠很好地解釋氧氣分子的鍵合和性質](../Page/分子軌域理論.md "wikilink")（見圖）。兩個氧原子各自的[s軌域和](../Page/s軌域.md "wikilink")[p軌域結合後](../Page/p軌域.md "wikilink")，形成一系列[成鍵與](../Page/成鍵軌道.md "wikilink")[反鍵](../Page/反鍵軌道.md "wikilink")[分子軌域](../Page/分子軌域.md "wikilink")。\(1s\)和\(2s\)原子軌域分別結合，形成\(\sigma_s\)成鍵分子軌域和\({\sigma_s}^*\)反鍵分子軌域。\(2p\)原子軌域結合後，成為6個能級不同的分子軌域──\(\sigma_p\)、\(\pi_x\)和\(\pi_y\)成鍵軌域，以及對應的\({\sigma_p}^*\)、\({\pi_x}^*\)和\({\pi_y}^*\)反鍵軌域，其中兩個\(\pi\)軌域及兩個π\*的能量分別相同。\[43\]

電子按照[構造原理](../Page/構造原理.md "wikilink")，從低能量至高能量順序填入分子軌域。\(2p\)電子共有8個，其中兩個填入\(\sigma_p\)，四個分別成對填入兩個[π軌域](../Page/π軌域.md "wikilink")，餘下兩個不成對地分別填入兩個\(\pi^*\)軌域。從成鍵軌域電子數和反鍵軌域電子數可得出，氧氣分子的[鍵級為](../Page/鍵級.md "wikilink")\(\frac{6-2}{2}=2\)。\[44\]這兩個不成對電子是氧氣分子的[價電子](../Page/價電子.md "wikilink")，它們決定了氧氣的性質。

根據[洪德規則](../Page/洪德規則.md "wikilink")，在[基態下兩個價電子的](../Page/基態.md "wikilink")[自旋互相平行](../Page/自旋.md "wikilink")，因此氧氣分子的最低能態為[三重態](../Page/三重態.md "wikilink")，即有三個能量相同而自旋不同的量子態。由於兩個價電子不成對，所以兩個\(\pi^*\)軌域均處於半滿的狀態。這使得氧氣有雙[自由基的性質](../Page/自由基.md "wikilink")，還可以解釋氧氣的[順磁性](../Page/順磁性.md "wikilink")。（氧氣分子之間的負[交換能也導致一部分的順磁性](../Page/交換作用.md "wikilink")。）\[45\]\[46\]由於含不成對電子，所以氧氣與多數[有機分子的反應較慢](../Page/有機分子.md "wikilink")，有機物因而不會自發燃燒。\[47\]

氧氣分子除了有能量最低的三重態（\(^3\textstyle \sum_{g}\)）以外，還有兩種能量高得多的[單態](../Page/單態.md "wikilink")。在這兩個[激發態下](../Page/激發態.md "wikilink")，兩個價電子的自旋互相反平行，違反洪德規則。這兩種單態的差別在於，兩個價電子是位於同一個\(\pi^*\)軌域中（\(^1\Delta_g\)），還是分開佔據兩個\(\pi^*\)軌域（\(^1\textstyle \sum_{g}\)）。\(^1\textstyle \sum_{g}\)在能量上不穩定，會迅速變為更穩定的\(^1\Delta_g\)。\(^1\textstyle \sum_{g}\)狀態下的氧氣有[抗磁性](../Page/抗磁性.md "wikilink")，而\(^1\Delta_g\)狀態下的氧氣則因為既有的軌道[磁矩而具有順磁性](../Page/磁矩.md "wikilink")，其磁強度與三重態氧相約。\[48\]\[49\]

單態氧對於有機物的反應性比普通氧氣分子強得多。短波長光在分解[對流層中的臭氧時會產生單態氧](../Page/對流層.md "wikilink")。\[50\]在[免疫系統中](../Page/免疫系統.md "wikilink")，單態氧是活性氧的來源之一。\[51\][光合作用會利用陽光的能量](../Page/光合作用.md "wikilink")，從水產生出單態氧。\[52\]在進行光合作用的生物中，[類胡蘿蔔素有助吸收單態氧的能量](../Page/類胡蘿蔔素.md "wikilink")，並將它轉換成基態氧，從而避免單態氧對組織造成損壞。\[53\]

### 同素異形體

[Ozone-1,3-dipole.png](https://zh.wikipedia.org/wiki/File:Ozone-1,3-dipole.png "fig:Ozone-1,3-dipole.png")是地球上一種較為稀有的氣體，主要分佈在[平流層](../Page/平流層.md "wikilink")。圖為臭氧的分子結構示意圖\]\]
[Oxygen_molecule.png](https://zh.wikipedia.org/wiki/File:Oxygen_molecule.png "fig:Oxygen_molecule.png")\]\]
雙原子氧（<chem>O2</chem>），亦即氧氣，是氧元素在地球上最常見的[同素異形體](../Page/同素異形體.md "wikilink")，地球大氣氧的主要部分（見[存在形式一節](../Page/#存在形式.md "wikilink")）。雙原子氧的鍵長為121[pm](../Page/皮米.md "wikilink")，鍵能為498[kJ·mol<sup>−1</sup>](../Page/焦耳每摩爾.md "wikilink")。\[54\]與生物圈內其它分子的雙鍵或兩個單鍵相比，雙原子氧的鍵能更低，所以它與任何有機分子的反應都會[釋放熱能](../Page/放熱反應.md "wikilink")。\[55\]\[56\]這一性質是[生物體呼吸作用的主要原理](../Page/生物體.md "wikilink")（見[生物學上的作用一節](../Page/#生物學上的作用.md "wikilink")）。

三氧（<chem>O3</chem>），常用名為[臭氧](../Page/臭氧.md "wikilink")，是一種高反應性的氧同素異形體。\[57\]在上層大氣中，<chem>O2</chem>經[紫外線分解成獨立的氧原子](../Page/紫外線.md "wikilink")。這些氧原子再與<chem>O2</chem>結合後，形成臭氧。\[58\]由於臭氧能較強地吸收紫外線範圍內的輻射，所以[臭氧層能阻擋一部分輻射抵達地球表面](../Page/臭氧層.md "wikilink")，有屏障的作用。\[59\]在地球表面出現的臭氧是汽車廢氣所產生的一種[空氣污染物](../Page/空氣污染.md "wikilink")。\[60\]

[四聚氧](../Page/四聚氧.md "wikilink")（<chem>O4</chem>）分子為[亞穩態](../Page/亞穩態.md "wikilink")，2001年發現。\[61\]\[62\]科學家曾認為，將<chem>O2</chem>加壓至20[GPa所形成的](../Page/帕斯卡.md "wikilink")[固態氧](../Page/固態氧.md "wikilink")，是由四聚氧所組成。但2006年的一項研究證明，此固體相態實際上是由<chem>O8</chem>[原子簇所組成](../Page/原子簇.md "wikilink")，結構為[三方晶系](../Page/三方晶系.md "wikilink")。\[63\]這種同素異形體很有可能是比<chem>O2</chem>和<chem>O3</chem>更強得多的[氧化劑](../Page/氧化劑.md "wikilink")，可做[火箭燃料](../Page/火箭燃料.md "wikilink")。\[64\]\[65\]

1990年，科學家將固態氧加壓至96GPa以上，發現了氧的金屬相態；\[66\]1998年，又證明該相態在極低溫下具[超導性](../Page/超導性.md "wikilink")。\[67\]

### 物理性質

[Oxygen_discharge_tube.jpg](https://zh.wikipedia.org/wiki/File:Oxygen_discharge_tube.jpg "fig:Oxygen_discharge_tube.jpg")中的綠色相似\]\]

氧氣在水中的[溶解性比在氮中高](../Page/溶解性.md "wikilink")，在[淡水中的溶解性又比在](../Page/淡水.md "wikilink")[海水中高](../Page/海水.md "wikilink")。在大氣中氧氣和氮氣的分子比例為1:4，而在水和空氣達致平衡的狀態下，該比例為1:2。氧氣在水中的溶解度會隨溫度改變：在0°C下的溶解度（14.6mg·L<sup>−1</sup>）約為在20°C下的雙倍（7.6mg·L<sup>−1</sup>）。\[68\]\[69\]當空氣處於25°C及時，每[公升淡水含](../Page/公升.md "wikilink")6.04毫升氧氣，而每公升海水則含4.95毫升氧氣。\[70\]

|    | 5 °C   | 25 °C   |
| -- | ------ | ------- |
| 淡水 | 9.0 mL | 6.04 mL |
| 海水 | 7.2 mL | 4.95 mL |

海平面上水的溶氧量

氧氣在90.20[K](../Page/開爾文.md "wikilink")（−182.95°C）凝結，在54.36K（−218.79°C）凝固。\[71\][液氧和](../Page/液氧.md "wikilink")[固氧](../Page/固氧.md "wikilink")（<chem>O2</chem>）都是呈淡天藍色的透明物質，這是由於氧分子會吸收紅光。（天空呈藍色是由於藍光的[瑞利散射](../Page/瑞利散射.md "wikilink")，而非因氧氣的吸收光譜所致。）高純度液氧一般是通過液化空氣[分餾法萃取的](../Page/分餾法.md "wikilink")；\[72\]亦可利用液氮的低溫使空氣中的氧氣凝結。\[73\]

[極光和](../Page/極光.md "wikilink")[氣輝](../Page/氣輝.md "wikilink")（夜輝）的部分顏色來自於氧氣分子的[光譜](../Page/光譜.md "wikilink")。\[74\]氧氣分子會吸收[赫茨貝格連續區和](../Page/赫茨貝格連續區.md "wikilink")[舒曼–龍格帶內的紫外輻射](../Page/舒曼–龍格帶.md "wikilink")，形成原子氧。這一過程對大氣中層的化學有重要的作用。\[75\]在激發態下的單態氧在溶液中會化學發光，呈紅色。\[76\]

[Liquid_oxygen_in_a_magnet_2.jpg](https://zh.wikipedia.org/wiki/File:Liquid_oxygen_in_a_magnet_2.jpg "fig:Liquid_oxygen_in_a_magnet_2.jpg")
氧氣具有[順磁性](../Page/順磁性.md "wikilink")。在實驗室中，用強力磁鐵可以使液氧懸浮在兩個磁極之間。\[77\]通過分析某個氧氣樣本的順磁性，可得出樣本中氧氣的純度。\[78\]

### 同位素與來源

[Evolved_star_fusion_shells.svg](https://zh.wikipedia.org/wiki/File:Evolved_star_fusion_shells.svg "fig:Evolved_star_fusion_shells.svg")
自然界中的氧元素由<chem>^16O</chem>、<chem>^17O</chem>及<chem>^18O</chem>三種穩定[同位素組成](../Page/同位素.md "wikilink")，其中<chem>^16O</chem>的[豐度最高](../Page/豐度.md "wikilink")（99.762%）。\[79\]

大部分<chem>^16O</chem>是在大質量[恆星](../Page/恆星.md "wikilink")[氦聚變過程晚期](../Page/3氦過程.md "wikilink")[合成](../Page/核合成.md "wikilink")，也有一部分源於[氖燃燒過程](../Page/氖燃燒過程.md "wikilink")。\[80\]<chem>^17O</chem>主要是在[碳氮氧循環中從氫至氦的聚變過程中產生](../Page/碳氮氧循環.md "wikilink")，因此分佈在恆星內部的氫聚變區域。\[81\][<sup>14</sup>N在捕獲一個](../Page/氮的同位素.md "wikilink")[<sup>4</sup>He原子核後變為](../Page/氦的同位素.md "wikilink")<chem>^18O</chem>，因此<chem>^18O</chem>分佈在大質量恆星的氦區域。\[82\]

在穩定同位素以外，氧還有14種[放射性同位素](../Page/放射性同位素.md "wikilink")。最穩定的為<chem>^15O</chem>和<chem>^14O</chem>，[半衰期分別為](../Page/半衰期.md "wikilink")122.24秒和70.606秒。\[83\]其餘的放射性同位素半衰期都在27秒以內，大部分甚至低於83毫秒。\[84\]質量低於<chem>^16O</chem>的同位素的最常見衰變模式為[β<sup>+</sup>](../Page/正電子發射.md "wikilink")，\[85\]\[86\]\[87\]產物為[氮](../Page/氮.md "wikilink")；質量高於<chem>^18O</chem>的同位素則主要進行\(\beta^-\)衰變，產物為[氟](../Page/氟.md "wikilink")。\[88\]

### 存在形式

| [Z](../Page/原子序.md "wikilink") | 元素                           | 質量比例（百萬分率）        |
| ------------------------------ | ---------------------------- | ----------------- |
| 1                              | [氫](../Page/氫.md "wikilink") | 739,000           |
| 2                              | [氦](../Page/氦.md "wikilink") | 240,000           |
| 8                              | 氧                            | {{bartable|10,400 |
| 6                              | [碳](../Page/碳.md "wikilink") | {{bartable| 4,600 |
| 10                             | [氖](../Page/氖.md "wikilink") | {{bartable| 1,340 |
| 26                             | [鐵](../Page/鐵.md "wikilink") | {{bartable| 1,090 |
| 7                              | [氮](../Page/氮.md "wikilink") | {{bartable| 960   |
| 14                             | [矽](../Page/矽.md "wikilink") | {{bartable| 650   |
| 12                             | [鎂](../Page/鎂.md "wikilink") | {{bartable| 580   |
| 16                             | [硫](../Page/硫.md "wikilink") | {{bartable| 440   |

通過光譜學估算的[銀河系元素豐度表](../Page/銀河系.md "wikilink")\[89\]

在全宇宙中，氧的豐度排在第三位，僅在[氫和](../Page/氫.md "wikilink")[氦之後](../Page/氦.md "wikilink")。\[90\]太陽總質量的0.9%為氧元素。\[91\]按質量算，氧是地球海洋中和陸地上最常見的化學元素：在[地球地殼中佔](../Page/地球地殼.md "wikilink")49.2%，\[92\]在海洋中佔88.8%。\[93\]氧氣是[地球大氣層中含量第二高的成分](../Page/地球大氣層.md "wikilink")，佔總體積的20.8%，總質量的23.1%（共10<sup>15</sup>噸）。\[94\]\[95\]\[96\]在[太陽系中](../Page/太陽系.md "wikilink")，地球大氣的氧含量是獨一無二的：氧氣只佔[火星大氣體積的](../Page/火星.md "wikilink")0.1%，[金星的大氣氧含量則更低](../Page/金星.md "wikilink")。這些行星的氧氣是含氧分子（如二氧化碳）在紫外線輻射下所釋放出來的。

地球之所以有異常高的氧氣，是因為[氧循環](../Page/氧循環.md "wikilink")。地球上的氧主要在大氣、[生物圈及](../Page/生物圈.md "wikilink")[岩石圈之間流動](../Page/岩石圈.md "wikilink")，是為[生物地球化學循環](../Page/生物地球化學循環.md "wikilink")。[光合作用是氧循環的主要推動力](../Page/光合作用.md "wikilink")，它決定了目前的地球大氣成分。氧氣通過光合作用釋放到大氣之中，再經[呼吸作用](../Page/呼吸作用.md "wikilink")、[分解作用和燃燒離開大氣](../Page/分解作用.md "wikilink")。今天的大氣氧含量處於平衡，氧氣的生成率和消耗率相同，約為每年大氣氧氣總量的二千分之一。

[WOA09_sea-surf_O2_AYool.png](https://zh.wikipedia.org/wiki/File:WOA09_sea-surf_O2_AYool.png "fig:WOA09_sea-surf_O2_AYool.png")
氧氣也出現在海洋中。<chem>O2</chem>在低溫海水中的溶解量更高（見[物理性質一節](../Page/#物理性質.md "wikilink")），所以兩極附近海洋的生物密度比其他海洋高許多。\[97\]受[硝酸鹽](../Page/硝酸鹽.md "wikilink")、[磷酸鹽等植物養分所](../Page/磷酸鹽.md "wikilink")[污染的水可促使](../Page/水污染.md "wikilink")[藻類生長](../Page/藻類.md "wikilink")（稱為[富營養化過程](../Page/富營養化.md "wikilink")）。這些藻類以及其他生物物質在分解後，會降低水的氧含量。水體的[生化需氧量](../Page/生化需氧量.md "wikilink")，即把含氧量復原至正常水平所需的氧氣量，是水質的一項量化指標。\[98\]

### 分析

[Phanerozoic_Climate_Change.png](https://zh.wikipedia.org/wiki/File:Phanerozoic_Climate_Change.png "fig:Phanerozoic_Climate_Change.png")的趨勢圖\]\]

[古氣候學家可通過測量海洋動物](../Page/古氣候學.md "wikilink")[外殼和](../Page/外骨骼.md "wikilink")[骨骼中](../Page/骨骼.md "wikilink")<chem>^18O</chem>和<chem>^16O</chem>同位素之比，推算百萬年前的地球氣候
。同位素<chem>^18O</chem>的質量比<chem>^16O</chem>高出12%，因此海水中含<chem>^16O</chem>的水分子會比含<chem>^18O</chem>的水分子蒸發得更快。海水溫度越低，蒸發率的差異更大。\[99\]當氣候寒冷的時期，蒸發的水形成的雨雪會有較高的<chem>^16O</chem>，剩餘的海水則含較高的<chem>^18O</chem>。因此在寒冷氣候下，海洋生物的外殼和骨骼會包含更多的<chem>^18O</chem>同位素。\[100\]

行星地質學家已分析過[地球](../Page/地球.md "wikilink")、[月球](../Page/月球.md "wikilink")、[火星上及](../Page/火星.md "wikilink")[隕石中氧同位素的相對含量](../Page/隕石.md "wikilink")。[起源號衛星所攜帶的](../Page/起源號.md "wikilink")[矽晶片暴露在](../Page/矽.md "wikilink")[太陽風中](../Page/太陽風.md "wikilink")，於2004年隨衛星返回地球。科學家在分析晶片後發現，太陽中的<chem>^16O</chem>比例比地球上高，意味著有某種未知的過程在太陽系[原行星盤坍縮形成地球之前](../Page/原行星盤.md "wikilink")，消耗了部分的<chem>^16O</chem>。\[101\]

氧在[分光光度計下有兩條](../Page/分光光度計.md "wikilink")[吸收帶](../Page/吸收帶.md "wikilink")，峰值分別位於687和760[納米](../Page/納米.md "wikilink")。這兩條光譜帶能用於區分植物的[反射和](../Page/反射率.md "wikilink")[熒光](../Page/熒光.md "wikilink")。利用此原理，有[遙感科學家提議用衛星測量植被冠層在這兩條帶的輻射](../Page/遙感.md "wikilink")，監測植物的健康和全球範圍內的[碳循環](../Page/碳循環.md "wikilink")。監測的[信噪比較低](../Page/信噪比.md "wikilink")，再加上植物的結構複雜，因此技術難度較大。\[102\]

## 生物學上的作用

### 光合作用及呼吸作用

在自然界的[光合作用過程中](../Page/光合作用.md "wikilink")，水分子經過[光分解作用后會釋放出氧氣](../Page/光分解作用.md "wikilink")。具其中一項估計，地球上有70%的氧氣由水生[綠藻及](../Page/綠藻.md "wikilink")[藍綠菌產生](../Page/藍綠菌.md "wikilink")，其餘的氧氣則來自陸地植物。\[103\]根據另一項估計，大氣氧氣每年有45%來自於海洋。\[104\]

整個光合作用的簡化公式為：\[105\]

  -
    <chem>6CO2 + 6H2O +</chem>[光子](../Page/光子.md "wikilink")<chem>-\>
    C6H12O6 + 6O2</chem>

亦即

  -

      -
        [二氧化碳](../Page/二氧化碳.md "wikilink") + 水 + 光 →
        [葡萄糖](../Page/葡萄糖.md "wikilink") + 氧氣

在進行光合作用的生物體內，產生氧氣的反應發生在[類囊體膜上](../Page/類囊體.md "wikilink")，需四個[光子的能量](../Page/光子.md "wikilink")。\[106\]此反應有許多步驟，最終會在類囊體膜上產生質子梯度，以此經反應合成[三磷酸腺苷](../Page/三磷酸腺苷.md "wikilink")（ATP）。\[107\]在生成水分子之後所剩餘的<chem>O2</chem>會釋放到大氣之中。\[108\]

[線粒體中產生ATP的](../Page/線粒體.md "wikilink")[氧化磷酸化過程需氧](../Page/氧化磷酸化.md "wikilink")。有氧呼吸作用的整體反應類似於反向的光合作用，其簡化公式為：

  -
    <chem>C6H12O6 + 6O2 -\> 6CO2 + 6H2O + 2880</chem>kJ·mol<sup>−1</sup>

在[脊椎動物中](../Page/脊椎動物.md "wikilink")，<chem>O2</chem>經[擴散作用](../Page/擴散作用.md "wikilink")，通過[肺內的膜進入](../Page/肺.md "wikilink")[紅血球](../Page/紅血球.md "wikilink")。<chem>O2</chem>與紅血球中的[血紅蛋白結合](../Page/血紅蛋白.md "wikilink")，後者從紫藍色變為亮紅色。\[109\]（<chem>CO2</chem>會通過[玻爾效應從血紅蛋白的另一個部分釋放出來](../Page/玻爾效應.md "wikilink")。）[軟體動物及某些](../Page/軟體動物.md "wikilink")[節肢動物利用](../Page/節肢動物.md "wikilink")[血藍蛋白承載氧氣](../Page/血藍蛋白.md "wikilink")，[蜘蛛及](../Page/蜘蛛.md "wikilink")[海螯蝦則利用](../Page/海螯蝦.md "wikilink")[蚯蚓血紅蛋白](../Page/蚯蚓血紅蛋白.md "wikilink")。\[110\]含血紅蛋白的血液每公升可溶解200立方厘米的<chem>O2</chem>。\[111\]

在發現[厭氧動物之前](../Page/厭氧生物.md "wikilink")，\[112\]人們曾以為氧氣是所有複雜生物所必要的物質。\[113\]

生物體消耗氧氣之後所產生的[活性氧類有](../Page/活性氧類.md "wikilink")[超氧化物](../Page/超氧化物.md "wikilink")（<chem>O2^-</chem>）、[過氧化氫](../Page/過氧化氫.md "wikilink")（<chem>H2O2</chem>）等。\[114\]複雜生物[免疫系統中有一些部分會生成過氧化物](../Page/免疫系統.md "wikilink")、超氧化物和單態氧來攻擊入侵的微生物。在抵禦病原攻擊的[植物過敏反應之中](../Page/植物過敏反應.md "wikilink")，活性氧類也起著重要的作用。\[115\]氧對[專性厭氧生物有破壞性](../Page/專性厭氧菌.md "wikilink")。這類生物在地球[生命演化歷程早期處於統治性地位](../Page/生命演化歷程.md "wikilink")，直至專性厭氧生物出現後10億年，即25億年前的大氣[大氧化事件為止](../Page/大氧化事件.md "wikilink")。\[116\]\[117\]

| 單位                                 | [肺泡氣體](../Page/肺泡.md "wikilink") | [動脈氣體](../Page/動脈.md "wikilink") | [靜脈氣體](../Page/靜脈.md "wikilink") |
| ---------------------------------- | -------------------------------- | -------------------------------- | -------------------------------- |
| [kPa](../Page/帕斯卡.md "wikilink")   | 14.2                             | 11\[118\]-13\[119\]              | 4.0\[120\]-5.3\[121\]            |
| [mmHg](../Page/mmHg.md "wikilink") | 107                              | 75\[122\]-100\[123\]             | 30\[124\]-40\[125\]              |
|                                    |                                  |                                  |                                  |

人體內氧氣的[氣體分壓](../Page/氣體分壓.md "wikilink")（pO<sub>2</sub>）

### 大氣氧氣的積聚

[Oxygenation-atm.svg](https://zh.wikipedia.org/wiki/File:Oxygenation-atm.svg "fig:Oxygenation-atm.svg")

大概在35億年前，地球上出現會進行光合作用的[古菌和](../Page/古菌.md "wikilink")[細菌](../Page/細菌.md "wikilink")。在此之前，[地球大氣中幾乎不存在游離氧氣](../Page/地球大氣.md "wikilink")。到了[古元古代](../Page/古元古代.md "wikilink")，即30至23億年前，地球上第一次出現大量的游離氧氣。\[126\]地球形成後的首10億年內，游離氧氣會與溶於海洋中的[鐵結合](../Page/鐵.md "wikilink")，形成[條狀鐵層](../Page/條狀鐵層.md "wikilink")。30至27億年前，此類氧氣槽達到飽和，氧氣開始從海洋[釋出](../Page/釋氣.md "wikilink")。17億年前，氧氣水平達到今天的10%。\[127\]\[128\]

24億年前的[大氧化事件](../Page/大氧化事件.md "wikilink")（「氧氣大災難」）期間，海洋和大氣的含氧量大幅上漲，大多數的[厭氧生物很可能因此絕種](../Page/厭氧生物.md "wikilink")。[好氧生物利用](../Page/好氧生物.md "wikilink")<chem>O2</chem>進行[呼吸作用](../Page/呼吸作用.md "wikilink")，可以比厭氧生物產生多許多的[ATP](../Page/三磷酸腺苷.md "wikilink")。\[129\]所有[真核生物](../Page/真核生物.md "wikilink")，包括動植物等複雜多細胞生物，都用<chem>O2</chem>進行呼吸作用。

5.4億年前，地球進入[寒武紀](../Page/寒武紀.md "wikilink")，這段時期的大氣含氧量（依體積）在15%和30%間波動。\[130\]約3億年前[石炭紀進入末期時](../Page/石炭紀.md "wikilink")，大氣含氧量達到35%的最高值，\[131\]這有可能是當時[昆蟲和](../Page/昆蟲.md "wikilink")[兩棲類動物體型巨大的原因](../Page/兩棲類.md "wikilink")。\[132\]

含氧量的波動影響遠古氣候：含氧量下降時，大氣密度一同下降，表面蒸發率因而上升，進而提高溫度和降水程度。\[133\]

以目前光合作用的速率，要生成目前整個大氣中所有的氧氣需2千年。\[134\]

## 工業生產

[Hofmann_voltameter_fr.svg](https://zh.wikipedia.org/wiki/File:Hofmann_voltameter_fr.svg "fig:Hofmann_voltameter_fr.svg")\]\]

每年從空氣萃取的工業用<chem>O2</chem>有1億噸，萃取方法有二。\[135\]液態空氣[分餾法蒸餾出氮氣](../Page/分餾法.md "wikilink")，留下液氧，為最常用方法。\[136\]

另一常用方法主要利用[沸石做成兩個相同的分子篩](../Page/沸石.md "wikilink")。當有不含雜質和水份的空氣通過分子篩時，分子篩會吸收氮氣，讓90%至94%純度的氧氣通過。\[137\]當其中一個分子篩上的氮氣達到飽和時，可通過降低氣壓或使氧氣反向通過分子篩，使氮氣釋出。兩個分子篩交替吸附、釋放氮氣，達到連續供應氧氣的目的。這種方法稱為。\[138\]

氧氣可通過電解水來生成。直流電通過水，在正負兩極處會分別形成兩份氫氣和一份氧氣。亦可用電作為催化劑，從氧化物和[含氧酸中萃取氧氣](../Page/含氧酸.md "wikilink")。化學氧氣發生器和氧燭都利用化學催化劑萃取氧氣，應用包括潛水艇維生系統以及客運飛機上突發降壓時的後備維生系統。還有一種生成方法，用高壓或電流使空氣穿過基於[二氧化鋯的](../Page/二氧化鋯.md "wikilink")[陶瓷膜](../Page/陶瓷.md "wikilink")。這樣生成的氧氣有極高的純度。\[139\]

## 儲藏

氧氣的儲藏方法包括高壓[氧氣筒](../Page/氧氣筒.md "wikilink")、低溫儲藏及化合物儲藏。由於1公升液氧在大氣壓力和20攝氏度下相等於840公升氧氣，因此為降低成本，氧氣一般是在液態下用特殊罐車運輸的。此類氧氣罐車可以為醫院等需大量純氧的機構補充氧氣罐。在進入大樓之前，低溫液氧須先經過[換熱器轉化為氣體](../Page/換熱器.md "wikilink")。小型壓縮氧氣罐可用於便攜式醫療器材及[氣焊和氣割等](../Page/氣焊和氣割.md "wikilink")。\[140\]

## 應用

### 醫療

[Home_oxygen_concentrator.jpg](https://zh.wikipedia.org/wiki/File:Home_oxygen_concentrator.jpg "fig:Home_oxygen_concentrator.jpg")患者家中的一台\]\]

[呼吸的主要目的是從空氣中吸取氧氣](../Page/呼吸.md "wikilink")。在醫療上，病人吸入額外的氧氣，不但能增加其血氧量，還能降低許多種病態肺組織對血流的阻力，減輕心臟的負荷。[氧療可應用於](../Page/氧療.md "wikilink")[慢性阻塞性肺病](../Page/慢性阻塞性肺病.md "wikilink")、[肺炎](../Page/肺炎.md "wikilink")、某些心臟疾病（充血性心臟衰竭）、某些導致[肺動脈血壓增高的疾病等等](../Page/肺動脈.md "wikilink")，以及任何使身體吸入及使用氧氣能力降低的疾病。\[141\]

氧氣療法有較大的靈活性，可用於醫院及病人家中，甚至還有越來越多的便攜式醫用氧氣設備。曾經常見的[氧氣帳篷](../Page/氧氣帳篷.md "wikilink")，現已大多被[氧氣面罩和](../Page/氧氣面罩.md "wikilink")[鼻插管所取代](../Page/鼻插管.md "wikilink")。\[142\]

[高壓氧治療利用高壓氧艙增加氧氣](../Page/高壓氧治療.md "wikilink")[分壓](../Page/氣體分壓.md "wikilink")，治療艙內的病人，\[143\]可用於治療[一氧化碳中毒](../Page/一氧化碳中毒.md "wikilink")、[氣性壞疽及](../Page/氣性壞疽.md "wikilink")[減壓症等](../Page/減壓症.md "wikilink")。\[144\]增加肺內的氧氣濃度有助於從[血紅蛋白的](../Page/血紅蛋白.md "wikilink")[血基質上移除](../Page/血基質.md "wikilink")[一氧化碳](../Page/一氧化碳.md "wikilink")。\[145\]\[146\]較高的氧氣分壓能夠毒死造成氣性壞疽的[厭氧菌](../Page/厭氧菌.md "wikilink")。\[147\]\[148\]當潛水員上升過快，環境壓力迅速降低時，血液裡會形成由氮和氦等氣體所組成的氣泡。若盡早增加氧氣壓力，可使這些氣泡重新溶解於血液之中，多餘的氣體從而經肺部自然呼出。\[149\]\[150\]\[151\]

### 維生及娛樂

[Wisoff_on_the_Arm_-_GPN-2000-001069.jpg](https://zh.wikipedia.org/wiki/File:Wisoff_on_the_Arm_-_GPN-2000-001069.jpg "fig:Wisoff_on_the_Arm_-_GPN-2000-001069.jpg")使用低壓純氧\]\]

現代[宇航服內充滿近純氧](../Page/宇航服.md "wikilink")，壓力為大氣壓的三分之一左右，這使得宇航員血液裡可以會有正常的氧氣分壓。\[152\]\[153\]

[水肺式和](../Page/水肺潛水.md "wikilink")[水面供氣式潛水員以及](../Page/水面供氣式.md "wikilink")[潛水艇都需要人工供應氧氣](../Page/潛水艇.md "wikilink")。潛水艇、潛水器和[大氣壓潛水服中的呼吸氣體一般處於大氣壓力](../Page/大氣壓潛水服.md "wikilink")。呼出的氣體在經化學方法萃取出二氧化碳之後，再補回氧氣，使分壓保持不變。在環境壓潛水員所呼吸的混合氣體之中，氧氣的比例須依身處深度而定。純氧或近純氧的應用一般僅限於[循環呼吸器](../Page/循環呼吸器.md "wikilink")、深度較淺（約6米以內）的減壓過程\[154\]\[155\]以及2.8[巴壓力以內的](../Page/巴.md "wikilink")[加壓艙治療](../Page/加壓艙.md "wikilink")。在加壓艙中之所以能用較高的壓力，是因為急性[氧氣中毒的徵狀可以即時控制](../Page/氧氣中毒.md "wikilink")，而不存在溺死的危險。更深的潛水則需要在呼吸氣體中摻入其他氣體，如氮氣和氦氣，目的是大大降低氧氣分壓，以避免氧氣中毒。\[156\]

[登山或乘坐不加壓](../Page/登山.md "wikilink")[航空器的人士往往也會須要補充氧氣](../Page/航空器.md "wikilink")。在低壓空氣中增加氧氣的比例，能夠使氧氣的分壓達到海平面水平。載客飛機都為每個乘客備有緊急供氧設備，以應對機艙失壓的情況。機艙突然失壓時，每個座位上方的化學氧發生器隨即啟動，氧氣面罩掉下。當乘客拉下面罩時，會使鐵屑與一個罐子裡的[氯酸鈉混合](../Page/氯酸鈉.md "wikilink")，\[157\]兩者的[放熱反應便會持續產生氧氣](../Page/放熱反應.md "wikilink")。

氧氣可使人產生微[欣快感](../Page/欣快.md "wikilink")，其在[氧吧和運動中的娛樂性使用有一定歷史](../Page/氧吧.md "wikilink")。1990年代末起，氧吧在日本、美國加州及內華達州[拉斯維加斯等地興起](../Page/拉斯維加斯.md "wikilink")。顧客可以付費呼吸使用氧氣比例比一般更高的氣體。\[158\]職業運動員，特別是[美式足球員](../Page/美式足球.md "wikilink")，有時會在場外戴上氧氣面罩，提高體能。然而，這種做法的實際功效卻存疑，任何體能上的提升更可能是因為[安慰劑效應](../Page/安慰劑.md "wikilink")。\[159\]有研究指出，只有在[有氧運動期間吸入高含氧量氣體](../Page/有氧運動.md "wikilink")，才會有體能上的提升。\[160\]

### 工業

[Clabecq_JPG01.jpg](https://zh.wikipedia.org/wiki/File:Clabecq_JPG01.jpg "fig:Clabecq_JPG01.jpg")\]\]

商業生產的氧氣之中，有55%都用於[煉鋼](../Page/煉鋼.md "wikilink")。過程中，氧氣經一長槍型物注入熔融鐵中，將其中的[硫和](../Page/硫.md "wikilink")[碳雜質分別轉化為](../Page/碳.md "wikilink")[二氧化硫和](../Page/二氧化硫.md "wikilink")[二氧化碳而釋出](../Page/二氧化碳.md "wikilink")。此為放熱反應，溫度上升至1,700 °C。\[161\]

另有25%氧氣用於化工。[乙烯與氧氣反應](../Page/乙烯.md "wikilink")，產生[環氧乙烷](../Page/環氧乙烷.md "wikilink")，再轉化為[乙二醇](../Page/乙二醇.md "wikilink")。乙二醇是許多物質生產過程中的主要材料，包括[防凍劑和](../Page/防凍劑.md "wikilink")[聚酯](../Page/聚酯.md "wikilink")（即許多[塑料和](../Page/塑料.md "wikilink")[布料的原料](../Page/布料.md "wikilink")）等等。\[162\]

剩餘的20%氧氣則用於醫療、金屬[氣焊和氣割](../Page/氣焊和氣割.md "wikilink")、[火箭推進劑中之氧化劑以及](../Page/火箭推進劑.md "wikilink")[水處理](../Page/水處理.md "wikilink")。\[163\]氧和[乙炔的燃燒過程會產生高溫火焰](../Page/乙炔.md "wikilink")，可用於[氧炔焊接](../Page/氣焊.md "wikilink")：厚度在60厘米以內的金屬先用較小的氧乙炔火焰加熱，然後用大量氧氣噴射，進行快速切割。\[164\]

## 化合物

[Stilles_Mineralwasser.jpg](https://zh.wikipedia.org/wiki/File:Stilles_Mineralwasser.jpg "fig:Stilles_Mineralwasser.jpg")（）是常見的氧化合物\]\]
氧在大多數化合物中的[氧化態為](../Page/氧化態.md "wikilink")−2，在[過氧化物等幾種化合物中則為](../Page/過氧化物.md "wikilink")−1。\[165\]其他氧化態的氧化合物非常罕見，有：[超氧化物](../Page/超氧化物.md "wikilink")（−1/2）、[臭氧化物](../Page/臭氧化物.md "wikilink")（−1/3）、[元素氧和](../Page/氧的同素異形體.md "wikilink")[次氟酸](../Page/次氟酸.md "wikilink")（0）、[二氧基盐](../Page/二氧基盐.md "wikilink")（+1/2）、[二氟化二氧](../Page/二氟化二氧.md "wikilink")（+1）和[二氟化氧](../Page/二氟化氧.md "wikilink")（+2）。

### 氧化物及其他無機化合物

[水](../Page/水.md "wikilink")（<chem>H2O</chem>）是一種[氫的氧化物](../Page/氫.md "wikilink")。水分子中，兩個氫原子與氧形成[共價鍵](../Page/共價鍵.md "wikilink")，每個氫原子又與另一個水分子中的氧原子相吸，是為[氫鍵](../Page/氫鍵.md "wikilink")（每氫原子鍵能為23.3 kJ·mol<sup>−1</sup>）。\[166\]由於有這些氫鍵，所以水分子間的距離比純粹以[范德華力相吸的情況短](../Page/范德華力.md "wikilink")15%。\[167\]

[Rust_screw.jpg](https://zh.wikipedia.org/wiki/File:Rust_screw.jpg "fig:Rust_screw.jpg")，如組成[鐵鏽的](../Page/鐵鏽.md "wikilink")[鐵氧化物](../Page/鐵氧化物.md "wikilink")\]\]
由於氧的[電負性高](../Page/電負性.md "wikilink")，所以會與幾乎所有元素形成[化學鍵](../Page/化學鍵.md "wikilink")，形成相應的[氧化物](../Page/氧化物.md "wikilink")。包括[鋁和](../Page/鋁.md "wikilink")[鈦在內的大部分金屬](../Page/鈦.md "wikilink")，在空氣之中都會在表面形成一層薄薄的氧化物。這使金屬[鈍化](../Page/鈍化.md "wikilink")，有助減慢[腐蝕作用](../Page/腐蝕.md "wikilink")。大多數[過渡金屬的氧化物都屬於](../Page/過渡金屬.md "wikilink")[非整比化合物](../Page/非整比化合物.md "wikilink")，其金屬比例低於[化學式中所示](../Page/化學式.md "wikilink")。例如，[氧化亞鐵](../Page/氧化亞鐵.md "wikilink")（[方鐵礦](../Page/方鐵礦.md "wikilink")）的化學式為<chem>Fe_{1-x}O</chem>，其中<chem>x</chem>通常為0.05左右。\[168\]

[二氧化碳](../Page/二氧化碳.md "wikilink")（<chem>CO2</chem>）是地球大氣中的微量氣體。[地球地殼](../Page/地球地殼.md "wikilink")[岩石有一大部分由各種金屬的氧化物組成](../Page/岩石.md "wikilink")，包括：[二氧化硅](../Page/二氧化硅.md "wikilink")（<chem>SiO2</chem>），可見於[花崗岩和](../Page/花崗岩.md "wikilink")[石英等](../Page/石英.md "wikilink")；[氧化铝](../Page/氧化铝.md "wikilink")（<chem>Al2O3</chem>），可見於[鋁土礦和](../Page/鋁土礦.md "wikilink")[剛玉等](../Page/剛玉.md "wikilink")；[氧化铁](../Page/氧化铁.md "wikilink")（<chem>Fe2O3</chem>），可見於[赤鐵礦和](../Page/赤鐵礦.md "wikilink")[鐵鏽等](../Page/鐵鏽.md "wikilink")；[碳酸鈣](../Page/碳酸鈣.md "wikilink")（<chem>CaCO3</chem>），可見於[石灰岩等](../Page/石灰岩.md "wikilink")。除此之外，還有由各種複雜[矽酸鹽組成的](../Page/矽酸鹽.md "wikilink")[矽酸鹽礦物](../Page/矽酸鹽礦物.md "wikilink")。質量遠比地殼大的[地幔主要由](../Page/地幔.md "wikilink")[鎂和](../Page/鎂.md "wikilink")[鐵的各種矽酸鹽所組成](../Page/鐵.md "wikilink")。

[可水溶稀酸鹽](../Page/溶解性.md "wikilink")，包括<chem>Na4SiO4</chem>、<chem>Na2SiO3</chem>和<chem>Na2Si2O5</chem>，可用作[清潔劑和](../Page/清潔劑.md "wikilink")[黏合劑](../Page/黏合劑.md "wikilink")。\[169\]

氧可以做過渡金屬的[配體](../Page/配體.md "wikilink")，形成過渡金屬[雙氧配合物](../Page/雙氧配合物.md "wikilink")（化學式為「金屬–」）。含[血基質的蛋白質](../Page/血基質.md "wikilink")──[血紅蛋白和](../Page/血紅蛋白.md "wikilink")[肌紅蛋白](../Page/肌紅蛋白.md "wikilink")──都屬於此類化合物。\[170\][六氟化鉑](../Page/六氟化鉑.md "wikilink")（<chem>PtF6</chem>）會和氧發生特殊的反應，把氧氧化成<chem>O2+PtF6-</chem>。\[171\]

### 有機化合物及生物分子

[Acetone-3D-vdW.png](https://zh.wikipedia.org/wiki/File:Acetone-3D-vdW.png "fig:Acetone-3D-vdW.png")是重要的化工原料。圖為丙酮分子的[空間填充模型](../Page/空間填充模型.md "wikilink")
   \]\]
[ATP_structure.svg](https://zh.wikipedia.org/wiki/File:ATP_structure.svg "fig:ATP_structure.svg")分子的[分子量中](../Page/分子量.md "wikilink")，氧元素佔超過40%\]\]

許多重要的有機化合物類別都含有氧原子，包括（「<chem>R</chem>」代表有機官能團）：[醇](../Page/醇.md "wikilink")（<chem>R
-OH</chem>）、[醚](../Page/醚.md "wikilink")（<chem>R -O
-R</chem>）、[酮](../Page/酮.md "wikilink")（<chem>R -CO
-R</chem>）、[醛](../Page/醛.md "wikilink")（<chem>R -CO
-H</chem>）、[羧酸](../Page/羧酸.md "wikilink")（<chem>R
-COOH</chem>）、[酯](../Page/酯.md "wikilink")（<chem>R -COO
-R</chem>）、[酸酐](../Page/酸酐.md "wikilink")（<chem>R -CO -O -CO
-R</chem>）及[酰胺](../Page/酰胺.md "wikilink")（<chem>R -C(O)
-NR2</chem>）。許多重要的有機[溶劑也含有氧](../Page/溶劑.md "wikilink")，包括：[丙酮](../Page/丙酮.md "wikilink")、[甲醇](../Page/甲醇.md "wikilink")、[乙醇](../Page/乙醇.md "wikilink")、[2-丙醇](../Page/2-丙醇.md "wikilink")、[呋喃](../Page/呋喃.md "wikilink")、[THF](../Page/四氫呋喃.md "wikilink")、[乙醚](../Page/乙醚.md "wikilink")、[1,4-二噁烷](../Page/1,4-二噁烷.md "wikilink")、[乙酸乙酯](../Page/乙酸乙酯.md "wikilink")、[DMF](../Page/二甲基甲醯胺.md "wikilink")、[DMSO](../Page/二甲基亞碸.md "wikilink")、[乙酸及](../Page/乙酸.md "wikilink")[甲酸](../Page/甲酸.md "wikilink")。其他含氧的重要有機化合物有：[甘油](../Page/甘油.md "wikilink")、[甲醛](../Page/甲醛.md "wikilink")、[戊二醛](../Page/戊二醛.md "wikilink")、[檸檬酸](../Page/檸檬酸.md "wikilink")、[乙酸酐及](../Page/乙酸酐.md "wikilink")[乙酰胺](../Page/乙酰胺.md "wikilink")。[環氧化合物是含有氧三元環的](../Page/環氧化合物.md "wikilink")[醚類化合物](../Page/醚.md "wikilink")。

氧可以和很多有機化合物在室溫或更低溫度發生自發反應，這稱為[自然氧化](../Page/自然氧化.md "wikilink")。\[172\]大部分含氧有機化合物都可以通過直接與<chem>O2</chem>反應來合成。在工業和商業上以這種方法製成的有機化合物包括[環氧乙烷和](../Page/環氧乙烷.md "wikilink")[過氧乙酸等](../Page/過氧乙酸.md "wikilink")。\[173\]

幾乎所有[生物分子都含有氧原子](../Page/生物分子.md "wikilink")。只有少數複雜生物分子不含氧，如[鯊烯和](../Page/鯊烯.md "wikilink")[胡蘿蔔素](../Page/胡蘿蔔素.md "wikilink")。在生物學中起到重要作用的有機化合物中，[碳水化合物的含氧質量比例最高](../Page/碳水化合物.md "wikilink")。所有[脂肪](../Page/脂肪.md "wikilink")、[脂肪酸](../Page/脂肪酸.md "wikilink")、[氨基酸及](../Page/氨基酸.md "wikilink")[蛋白質都含氧原子](../Page/蛋白質.md "wikilink")（因為均含有[羰基](../Page/羰基.md "wikilink")）。氧還出現在[磷酸鹽](../Page/磷酸鹽.md "wikilink")（<chem>PO4^{3-}</chem>）中，是承載能量的[ATP和](../Page/三磷酸腺苷.md "wikilink")[ADP分子的組成部分](../Page/二磷酸腺苷.md "wikilink")；在[嘌呤](../Page/嘌呤.md "wikilink")（[腺嘌呤除外](../Page/腺嘌呤.md "wikilink")）和[嘧啶中](../Page/嘧啶.md "wikilink")，形成[RNA和](../Page/核糖核酸.md "wikilink")[DNA](../Page/脱氧核糖核酸.md "wikilink")；以及在[磷酸鈣和](../Page/磷酸鈣.md "wikilink")[羥磷灰石中](../Page/羥磷灰石.md "wikilink")，形成骨骼。

## 安全

美國[NFPA
704標準將壓縮氧氣評為一種對健康無害](../Page/NFPA_704.md "wikilink")、非易燃、反應活性低的氧化劑。由於凝結的氧氣會增加患[高氧血症的風險](../Page/高氧血症.md "wikilink")，低溫液體自身也有造成凍傷的危險，因此低溫液氧的健康危害評級為3；其他方面的評級則與壓縮氧氣相同。

### 毒性

[Scuba-diving.jpg](https://zh.wikipedia.org/wiki/File:Scuba-diving.jpg "fig:Scuba-diving.jpg")時發生\]\]

當氧氣（<chem>O2</chem>）的[氣體分壓較高時](../Page/氣體分壓.md "wikilink")，會導致[氧氣中毒](../Page/氧氣中毒.md "wikilink")，造成痙攣等症狀。\[174\]由於氧氣的分壓是氧氣比例和總氣壓之積，所以在呼吸氣體中增加氧氣比例，或者增加正常呼吸氣體的壓力，都會提高氧氣分壓。\[175\]氧氣中毒一般在氧氣分壓超過50[kPa時發生](../Page/帕斯卡.md "wikilink")，相等於在標準壓力下含氧量為50%（在海平面上正常空氣中的氧氣分壓為21kPa）。醫用氧氣面罩所提供的氧氣體積比例在30%和50%之間（在標準壓力下分壓約為30kPa），所以除使用[機械呼吸機的情況以外](../Page/機械呼吸機.md "wikilink")，都不會有氧氣中毒的危險。\[176\]

[早產嬰兒曾經會被放進富含氧氣的保溫箱](../Page/早產.md "wikilink")，但因含氧量過高，一些嬰兒因此失明。自此，醫院已不再使用這種治療方法。\[177\]

一些現代宇航服以及早期航天器（如[阿波羅太空船](../Page/阿波羅太空船.md "wikilink")）都以純氧作為呼吸氣體。由於總氣壓較低，所以這並無健康危害。\[178\]\[179\]宇航服中的氧氣分壓約為30kPa（正常水平的1.4倍），而宇航員動脈血氧分壓則比在海平面時的水平略高。

[水肺潛水和](../Page/水肺潛水.md "wikilink")[水面供氣潛水時](../Page/水面供氣潛水.md "wikilink")，也有可能發生氧氣中毒，對肺部和[中樞神經系統造成危害](../Page/中樞神經系統.md "wikilink")。\[180\]\[181\]持續呼吸氧氣分壓超過60kPa的氣體，會導致永久性[肺纖維化](../Page/肺纖維化.md "wikilink")。\[182\]當氧氣分壓超過160kPa（約1.6倍大氣壓）時，會引致痙攣，這對潛水員來說通常是致命的。在66米或更深的水中呼吸含21%氧氣的空氣，或在6米深處呼吸100%純氧，即會引致急性氧氣中毒。\[183\]\[184\]\[185\]\[186\]

### 燃燒及其他危害

[Apollo_1_fire.jpg](https://zh.wikipedia.org/wiki/File:Apollo_1_fire.jpg "fig:Apollo_1_fire.jpg")指令艙。一次火花點燃了艙內壓力高於正常水平的純氧，引發火災。全體宇航員喪生\]\]
高濃度氧氣可助燃。氧化劑和[燃料聚集](../Page/燃料.md "wikilink")，便有起火或爆炸的危險。要觸發燃燒反應，須有熱源或火花。\[187\]\[188\]雖然氧氣是氧化劑，而不是燃料，但在燃燒過程中，它所釋放的化學能卻最多。\[189\]\[190\]氧化電位較高的氧化合物可以在反應過程中提供氧原子，因此也有燃燒的危險，如[過氧化物](../Page/過氧化物.md "wikilink")、[氯酸鹽](../Page/氯酸鹽.md "wikilink")、[硝酸鹽](../Page/硝酸鹽.md "wikilink")、[高氯酸鹽及](../Page/高氯酸鹽.md "wikilink")[過氧化物等](../Page/過氧化物.md "wikilink")。

儲存及運輸氧氣及液氧的鋼製管道及容器也可以做燃燒反應的燃料，所以須經過特殊訓練才能設計和生產氧氣系統，把產生火花的可能性降到最低。\[191\]1967年，美國[阿波羅1號在進行發射台測試時發生火災](../Page/阿波羅1號.md "wikilink")，導致全體宇航員喪生。當時艙內充滿的純氧，壓力並非一般任務所用的正常壓力三分之一，而是稍高於大氣壓力，促使火焰快速蔓延，直至失控。\[192\]\[193\]

在處理液氧時，一旦液氧濺起並滲入[木材](../Page/木材.md "wikilink")、石化製品、[瀝青等有機物中](../Page/瀝青.md "wikilink")，這些物質會在碰撞下有不可預見的爆炸反應。\[194\]液氧與其他低溫液體一樣，若皮膚、眼睛等器官與之直接接觸，會有凍傷的危險。

## 參見

  - [缺氧](../Page/缺氧.md "wikilink")
  - [氧化合物](../Category/氧化合物.md "wikilink")

## 注釋

## 參考資料

## 引用

  -
  -
  -
## 外部連結

  - [Oxygen](http://www.periodicvideos.com/videos/008.htm) at *The
    Periodic Table of Videos*（[諾丁漢大學](../Page/諾丁漢大學.md "wikilink")）
  - [Oxidizing Agents \>
    Oxygen](http://www.organic-chemistry.org/chemicals/oxidations/oxygen.shtm)（以氧氣為氧化劑的化學反應彙編）
  - [WebElements.com –
    Oxygen](http://www.webelements.com/webelements/elements/text/O/index.html)

[氧](../Category/氧.md "wikilink") [2F](../Category/化学元素.md "wikilink")
[Category:氧族元素](../Category/氧族元素.md "wikilink")
[2F](../Category/第2周期元素.md "wikilink")
[Category:非金属](../Category/非金属.md "wikilink")
[Category:氧化剂](../Category/氧化剂.md "wikilink")
[Category:生命化学元素](../Category/生命化学元素.md "wikilink")

1.

2.  [Emsley 2001](../Page/#Reference-idEmsley2001.md "wikilink"), p.297

3.
4.
5.

6.
7.

8.

9.

10. [Cook & Lauer 1968](../Page/#Reference-idCook1968.md "wikilink"),
    p.499.

11.

12.

13.
14.
15. [Emsley 2001](../Page/#Reference-idEmsley2001.md "wikilink"), p.299

16.

17.
18.
19.

20.
21.
22. [Emsley 2001](../Page/#Reference-idEmsley2001.md "wikilink"), p.300

23. [Cook & Lauer 1968](../Page/#Reference-idCook1968.md "wikilink"),
    p.500

24.
25.
26.

27.
28.
29.
30.

31.
32.

33.

34.

35.
36. \[<http://www.poland.gov.pl/Karol,Olszewski,and,Zygmunt,Wroblewski>:,condensation,of,oxygen,and,nitrogen,1987.html
    Poland – Culture, Science and Media. Condensation of oxygen and
    nitrogen\]. Retrieved on 2008-10-4.

37. [Emsley 2001](../Page/#Reference-idEmsley2001.md "wikilink"), p.303

38.

39.
40.
41.

42.

43.

44.
45.
46.

47.

48. Keisuke Hasegawa: *Direct measurements of absolute concentration and
    lifetime of singlet oxygen in the gas phase by electron paramagnetic
    resonance.* In: *Chemical Physics Letters.* 457 (4–6), 2008,
    S. 312–314;
    [<doi:10.1016/j.cplett.2008.04.031>](../Page/doi:10.1016/j.cplett.2008.04.031.md "wikilink").

49. N. V. Shinkarenko, V. B. Aleskovskiji: *Singlet Oxygen: Methods of
    Preparation and Detection.* In: *Russian Chemical Reviews.* 50,
    1981, S. 320–231;
    [<doi:10.1070/RC1981v050n03ABEH002587>](../Page/doi:10.1070/RC1981v050n03ABEH002587.md "wikilink").

50.

51.

52.

53.

54.

55.
56.

57.

58.
59.
60.
61.

62.

63.

64.
65.
66.

67.

68.
69.

70.

71.

72.

73.

74.

75.

76.

77.

78.

79.

80.

81.
82.
83.
84.
85.

86.

87.

88.
89.

90.
91.
92.

93.
94.
95. [Emsley 2001](../Page/#Reference-idEmsley2001.md "wikilink"), p.298

96. 只考慮距離地表80公里以內的大氣

97. 取自H.W. Harvey, The Chemistry and Fertility of Sea Waters,
    1955，引用C.J.J. Fox, "On the coefficients of absorption of
    atmospheric gases in sea water", Publ. Circ. Cons. Explor. Mer, no.
    41, 1907。Harvey注釋稱，根據《自然》雜誌上的論文，該數值比正確數值高出3%左右。

98. [Emsley 2001](../Page/#Reference-idEmsley2001.md "wikilink"), p.301

99. [Emsley 2001](../Page/#Reference-idEmsley2001.md "wikilink"), p.304

100.
101.

102.

103.

104.

105.

106. 在藻類和植物中，類囊體為[葉綠體的一部分](../Page/葉綠體.md "wikilink")；在藍綠菌中，類囊體只是眾多膜結構中的一種。事實上，科學家認為，曾經與植物及藻類的始祖形成共生關係的藍綠菌，經過演化後成為了它們體內的葉綠體。

107. [Raven 2005](../Page/#Reference-idRaven2005.md "wikilink"), 115–27

108. 水的氧化作用由一種含[錳的](../Page/錳.md "wikilink")[酶](../Page/酶.md "wikilink")[配合物所催化](../Page/配合物.md "wikilink")。這種酶配合物稱為「」（OEC）或「分水配合物」，位於類囊體膜上靠體腔的一面。錳是一種重要的[輔因子](../Page/輔因子.md "wikilink")；反應另需[鈣和](../Page/鈣.md "wikilink")[氯](../Page/氯.md "wikilink")。(Raven
     2005)

109.
110.
111.
112.

113.

114.
115.
116.

117.

118. 用0.133322 kPa/mmHg的數值換算而得

119.
120.
121.
122. [Normal Reference Range
     Table](http://pathcuric1.swmed.edu/PathDemo/nrrt.htm)  from The
     University of Texas Southwestern Medical Center at Dallas. Used in
     Interactive Case Study Companion to Pathologic basis of disease.

123.
124.
125. [The Medical Education Division of the Brookside Associates--\> ABG
     (Arterial Blood
     Gas)](http://www.brooksidepress.org/Products/OperationalMedicine/DATA/operationalmed/Lab/ABG_ArterialBloodGas.htm)
     Retrieved on 2009-12-6

126.

127.
128.

129.

130.

131.
132.

133.

134.

135.
136.
137.
138.

139.
140.
141. [Cook & Lauer 1968](../Page/#Reference-idCook1968.md "wikilink"),
     p.510

142.

143.

144.

145.

146.

147.

148.

149.
150.

151.

152.

153.

154.

155.

156.
157.
158.

159.
160.

161.
162.
163.
164. [Cook & Lauer 1968](../Page/#Reference-idCook1968.md "wikilink"),
     p.508

165. ,p. 28

166.

167.

168.

169. [Cook & Lauer 1968](../Page/#Reference-idCook1968.md "wikilink"),
     p.507

170.

171. [Cook & Lauer 1968](../Page/#Reference-idCook1968.md "wikilink"),
     p.505

172. [Cook & Lauer 1968](../Page/#Reference-idCook1968.md "wikilink"),
     p.506

173.
174.
175. [Cook & Lauer 1968](../Page/#Reference-idCook1968.md "wikilink"),
     p.511

176.
177.
178.
179.

180.
181.
182.

183.
184.

185.

186.

187.
188.

189.
190.
191.
192. 不過，事後調查並未找到單個起火點，亦有證據顯示火花來自電弧。

193.

194.