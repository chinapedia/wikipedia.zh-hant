**T14突擊坦克**（Assault Tank
T14）是[二戰時期美英的一個聯合開發項目](../Page/二戰.md "wikilink")，T14的開發主旨是為美英雙方提供能同用的[重型坦克](../Page/重型坦克.md "wikilink")。
1941年，美國軍方人員前往英國商討硏制一種重型裝甲的戰鬥車輛，甚至比[邱吉爾Mk
IV坦克（A22）的裝甲更強](../Page/邱吉爾坦克.md "wikilink")。
於1942年5月至次年5月間研發，經測試後，原批准量產，但最終仍被取消，停留在試驗階段，製造過兩輛原型車。

## 開發歷史

1942年，英國最初預訂了8,500輛，但直至1944年T14的原型仍未完成，測驗指出車輛在實際使用時嚴重過重，此時英軍已大量采用[邱吉爾坦克及其他坦克](../Page/邱吉爾坦克.md "wikilink")。

T14開發項目在二戰完結時被終止。只有2輛製成；一輛在美國測試而另一輛送往英國。\[1\]
英國的存放在[博文頓坦克博物館](../Page/博文頓坦克博物館.md "wikilink")\[2\]

## 參考文獻

<div class="references-small">

<references>

</references>

</div>

## 外部連結

  - \-[二戰戰鬥車輛介紹](http://www.wwiivehicles.com/usa/tanks_heavy/t14_assault.html)

  - \-[美軍中型坦克介紹](https://web.archive.org/web/20060907090300/http://mailer.fsu.edu/~akirk/tanks/UnitedStates/mediumtanks/MediumTanks.html)

[Category:重型坦克](../Category/重型坦克.md "wikilink")
[Category:美國二戰坦克](../Category/美國二戰坦克.md "wikilink")

1.

2.