**通信工程**（也作**信息工程**、**电信工程**，旧称**远距离通信工程**、**弱电工程**）是[电子工程的重要分支](../Page/电子工程.md "wikilink")，同时也是其中一个基础学科。该学科关注的是通信过程中的信息传输和信号处理的原理和应用。

通信工程研究的是，以[电磁波](../Page/电磁学.md "wikilink")、[声波或](../Page/声波.md "wikilink")[光波的形式把](../Page/光波.md "wikilink")[信息通过电脉冲](../Page/信息.md "wikilink")，从发送端（[信源](../Page/信源.md "wikilink")）传输到一个或多个接受（[信宿](../Page/信宿.md "wikilink")）。接受端能否正确辨认信息，取决于传输中的损耗高低。[信号处理是通信工程中一个重要环节](../Page/信号处理.md "wikilink")，其包括[过滤](../Page/过滤.md "wikilink")，[编码和](../Page/编码.md "wikilink")[解码等](../Page/解码.md "wikilink")。

通信工程所关注的频段涉及甚广。低频段，亦即低[赫兹](../Page/赫兹.md "wikilink")，关心的是[技术声学或](../Page/技术声学.md "wikilink")[低频技术](../Page/低频技术.md "wikilink")。高频段中关注的范围从[微波或](../Page/微波.md "wikilink")[雷达系统到](../Page/雷达.md "wikilink")[可见光的](../Page/可见光.md "wikilink")[激光或](../Page/激光.md "wikilink")[镭射系统](../Page/镭射.md "wikilink")。微波到可见光中间的频段几乎都是通信工程的研究对象。除此之外，通信过程中所应用的媒介和技术，包括通信系统在陆上、水下、空中和宇宙空间中的应用，也是相当丰富的。

通信工程的基础建立于[应用数学中的](../Page/应用数学.md "wikilink")[数理方程](../Page/数理方程.md "wikilink")。其理论起点是物质与波在[傅里叶](../Page/傅里叶变换.md "wikilink")[热扩散和](../Page/热扩散.md "wikilink")[麦克斯韦电动力条件下观察到的传播现象](../Page/麦克斯韦方程组.md "wikilink")。

世界上由人类创造的最大的通信系统是[公共交换电话网](../Page/公共交换电话网.md "wikilink")（）。另外一个正在迅速发展的大型通信系统——[網際網路](../Page/網際網路.md "wikilink")——正逐步形成电话网的规模，并终将有一天取而代之。

不可否认的是，如今通信工程正在转变为[信息工程](../Page/信息工程.md "wikilink")，英特网就是一个很好的例子。一方面通信系统常常是信息与应用技术的计算系统的一个重要组成部分，另一方面现代通信系统给信息工程予以理论和方法指导，并处于计算系统的核心位置。

## 起源

## 电信协会与组织

  - [IEEE](../Page/IEEE.md "wikilink")
  - [IEE](../Page/IEE.md "wikilink")
  - [ITU](../Page/ITU.md "wikilink")

## 参见

  - [Portal:电子工程](../Page/Portal:电子工程.md "wikilink")
  - [电子工程](../Page/电子工程.md "wikilink")
  - [电子工程\#通信工程](../Page/电子工程#通信工程.md "wikilink")
  - [复用技术](../Page/复用技术.md "wikilink")
  - [无线与电视技术](../Page/无线与电视技术.md "wikilink")
  - [信号处理](../Page/信号处理.md "wikilink")
  - [电信工程](../Page/电信工程.md "wikilink")
  - [传输技术](../Page/传输技术.md "wikilink")
  - [交换技术](../Page/交换技术.md "wikilink")

[Category:电子工程](../Category/电子工程.md "wikilink")
[Category:通信](../Category/通信.md "wikilink")