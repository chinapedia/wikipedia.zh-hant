《**美国家庭滑稽录像**》（，英文简称**''AFV***或***AFHV**''，为[美國廣播公司](../Page/美國廣播公司.md "wikilink")（ABC）的一个[综艺节目](../Page/综艺节目.md "wikilink")，1990年1月14日開播。中文直譯全稱《**美洲最搞笑家庭影像**》，源自[日本](../Page/日本.md "wikilink")[TBS電視台綜藝節目](../Page/TBS電視台.md "wikilink")《》，虽然中文直譯名为《美洲最搞笑家庭影像》，但是大部分影像来源于[北美](../Page/北美.md "wikilink")（例如[加拿大](../Page/加拿大.md "wikilink")、[美国本地](../Page/美国.md "wikilink")）。现任主持人为[阿方索·里贝罗](../Page/阿方索·里贝罗.md "wikilink")（Alfonso
Ribeiro），現行播映時間為每周日19：00～20：00。

## 簡介

《美国家庭滑稽录像》一般为由主持人首先进行开场性的小幽默，之后播放一些幽默、搞笑的影像。所有影像均接收自美洲地区的人民自行以家用[攝影機拍攝後投稿的家庭影像](../Page/攝影機.md "wikilink")。影像大部分包括糗事。该节目目前在其中也穿插了很多互动环节。另外，该节目定期对优秀视频进行不等奖金的奖励，也是该节目较受欢迎的原因之一。

《美国家庭滑稽录像》的台灣公開播映權由[中國電視公司率先取得](../Page/中國電視公司.md "wikilink")，譯名為《歡笑-{}-一籮筐》，首播期間為1990年8月19日至1995年4月1日，以[雙語播映](../Page/雙語.md "wikilink")（主聲道為[國語](../Page/國語.md "wikilink")[配音](../Page/配音.md "wikilink")，副聲道為英語原音），共158集，曾設有主持人[王麗玲與](../Page/王麗玲.md "wikilink")[李方](../Page/李方_\(藝人\).md "wikilink")。至於台灣[有線電視](../Page/有線電視.md "wikilink")，[新視國際頻道](../Page/新視國際頻道.md "wikilink")、[國寶衛視太陽台](../Page/國寶衛視太陽台.md "wikilink")、[國寶衛視](../Page/國寶衛視.md "wikilink")、[超級太陽台](../Page/超級太陽台.md "wikilink")、[太陽衛視](../Page/太陽衛視.md "wikilink")、[春暉電影台都曾播映該節目](../Page/春暉電影台.md "wikilink")，國寶衛視以英語原音播出。

《美国家庭滑稽录像》在香港由[无线电视取得播放权](../Page/无线电视.md "wikilink")，譯名為《笑笑-{}-小電影》，于每周六晚8:00在[明珠台播映英語原音版](../Page/明珠台.md "wikilink")，[翡翠台亦曾播映過](../Page/翡翠台.md "wikilink")[香港粵語配音版](../Page/香港粵語.md "wikilink")；亦於[myTV
SUPER提供節目重溫](../Page/myTV_SUPER.md "wikilink")，集數上傳7天後會刪除。

## 参考文献

## 外部連結

  - [美國廣播公司《美国家庭滑稽录像》官方網站](http://abc.go.com/primetime/afv/)
  - [IMDB收錄的《美国家庭滑稽录像》相关资料](http://imdb.com/title/tt0098740/)
  - [滑稽搞笑影片 網路特搜](http://www.funplus.org/)

{{-}}

[Category:美国电视节目](../Category/美国电视节目.md "wikilink")
[Category:美國廣播公司電視節目](../Category/美國廣播公司電視節目.md "wikilink")
[Category:福斯电视公司制作的电视节目](../Category/福斯电视公司制作的电视节目.md "wikilink")
[Category:無綫電視外購節目](../Category/無綫電視外購節目.md "wikilink")
[Category:中視外購電視節目](../Category/中視外購電視節目.md "wikilink")
[Category:英语电视节目](../Category/英语电视节目.md "wikilink")
[Category:超視電視節目](../Category/超視電視節目.md "wikilink")