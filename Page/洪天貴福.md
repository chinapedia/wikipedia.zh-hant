**洪天貴福**（），[洪秀全长子](../Page/洪秀全.md "wikilink")，初名**天贵**，后加**福**字。生母[賴蓮英](../Page/賴蓮英.md "wikilink")、[清代](../Page/清代.md "wikilink")[廣東省](../Page/廣東省.md "wikilink")[廣州府](../Page/廣州府.md "wikilink")[花縣官祿村人](../Page/花縣.md "wikilink")。

## 生平

洪秀全在1851年建立[太平天國時](../Page/太平天國.md "wikilink")，立洪天貴福為[王世子](../Page/王世子.md "wikilink")，尊稱為幼主萬歲。

洪秀全死後，洪天貴福在1864年（[同治三年](../Page/同治.md "wikilink")）6月6日在[天京](../Page/天京.md "wikilink")（今[江蘇](../Page/江蘇.md "wikilink")[南京](../Page/南京.md "wikilink")）即位，成为“幼天王”。

1864年7月天京失陷，洪天贵福逃出天京，到達[浙江](../Page/浙江.md "wikilink")[湖州](../Page/湖州.md "wikilink")。8月末洪天貴福與當地太平軍從湖州出走，打算會合[江西的](../Page/江西.md "wikilink")[太平軍後北上](../Page/太平軍.md "wikilink")[中原](../Page/中原.md "wikilink")。部隊沿途被清軍緊追，10月9日部隊在江西被擊潰，洪天貴福在附近地區流浪多日，10月25日在江西[石城荒山之中被清军俘获](../Page/石城.md "wikilink")，11月18日在[南昌被](../Page/南昌.md "wikilink")[沈葆禎下令](../Page/沈葆禎.md "wikilink")[凌遲處死](../Page/凌遲.md "wikilink")，年仅[實歲十四岁](../Page/實歲.md "wikilink")。

## \-{后}-妃

四位后妃都稱為幼娘娘

1.  黃氏、廣西人
2.  黃氏、廣西人
3.  侯氏、安慶人
4.  張氏、湖北人

[Category:太平天国天王](../Category/太平天国天王.md "wikilink")
[Category:清朝死于凌迟者](../Category/清朝死于凌迟者.md "wikilink")
[Category:中国被处决的君主](../Category/中国被处决的君主.md "wikilink")
[Category:被處決的兒童](../Category/被處決的兒童.md "wikilink")
[Category:花縣人](../Category/花縣人.md "wikilink")
[T天貴福](../Category/洪姓.md "wikilink")
[Category:广东客家人](../Category/广东客家人.md "wikilink")
[Category:广州人](../Category/广州人.md "wikilink")