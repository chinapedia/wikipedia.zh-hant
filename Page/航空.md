[Airbus_A380_blue_sky.jpg](https://zh.wikipedia.org/wiki/File:Airbus_A380_blue_sky.jpg "fig:Airbus_A380_blue_sky.jpg")，目前人类建造的最大的客运航空器翱翔蓝天。\]\]

**航空**（）狭义上则指的是载人或非载人的飞行器在大气层中的航行活动，广义上航空一词也指进行航空活动所必须的科学，同时也泛指研究开发[航空器所涉及的各种技术](../Page/航空器.md "wikilink")\[1\]\[2\]。人类自古以来便有像鸟儿一样翱翔天空的愿望，但直到18世纪后期[载人热气球在欧洲升空后才首度实现](../Page/气球_\(航空器\).md "wikilink")。20世纪初随着工业革命带来的科技进步，人类的航空事业得以迅速发展。1903年12月17日，[美国人](../Page/美国人.md "wikilink")[莱特兄弟成功试飞人类第一架重于空气](../Page/莱特兄弟.md "wikilink")、带有动力、受控并可持续滞空的[飞机](../Page/飞机.md "wikilink")\[3\]，开启了现代航空的新纪元。航空是21世纪最活跃和最具影响力的科学技术领域，该领域取得的重要成就标志着人类文明的发展水平，也体现着一个国家的综合国力及科学技术的水平。

## 历史

[First_flight2.jpg](https://zh.wikipedia.org/wiki/File:First_flight2.jpg "fig:First_flight2.jpg")驾驶自行研制的[飞行者一号完成了人类史上首次](../Page/飞行者一号.md "wikilink")[飞机的受控持续飞行](../Page/飞机.md "wikilink")。\]\]

人类航空事业的发展已经有200多年的历史。人类最初从神话传说中表达自己对[飞行的渴望](../Page/飞行.md "wikilink")，随后他们发明了[风筝](../Page/风筝.md "wikilink")、[天灯等可以飞入空中的人造物](../Page/天灯.md "wikilink")。文艺复兴时[达·芬奇等人开始以科学的方法探求并进行](../Page/达·芬奇.md "wikilink")[航空器的设计](../Page/航空器.md "wikilink")。18世纪末期开始的[工业革命给自然科学技术的发展注入了强大动力](../Page/工业革命.md "wikilink")，人类的航空技术开始迅速发展，[热气球](../Page/热气球.md "wikilink")、[飞艇](../Page/飞艇.md "wikilink")、[滑翔机和](../Page/滑翔机.md "wikilink")[飞机等在一个多世纪的时间里相继被发明](../Page/飞机.md "wikilink")、发展成熟，而其中的一些又被淘汰。两次世界大战更是刺激了飞机制造业的进步，其后人类更进一步踏入了[航天领域](../Page/航天.md "wikilink")。现在航空业已经成为人类最前沿、最具影响力的领域。

## 民用航空

民航指一切非軍事航空活動，包括[航空運輸及](../Page/航空公司.md "wikilink")[通用航空](../Page/通用航空.md "wikilink")。

### 航空公司

[Embraer_175_(Air_Canada)_091.jpg](https://zh.wikipedia.org/wiki/File:Embraer_175_\(Air_Canada\)_091.jpg "fig:Embraer_175_(Air_Canada)_091.jpg")的ERJ-170\]\]
[Cessna170B_orange.jpg](https://zh.wikipedia.org/wiki/File:Cessna170B_orange.jpg "fig:Cessna170B_orange.jpg")\]\]

目前全球有五大主要客機生產商：

  - [空中巴士公司](../Page/空中巴士公司.md "wikilink")，總部在[法國](../Page/法國.md "wikilink")[圖盧茲](../Page/圖盧茲.md "wikilink")
  - [波音公司](../Page/波音公司.md "wikilink")，總部在[美國](../Page/美國.md "wikilink")[西雅圖](../Page/西雅圖.md "wikilink")
  - [圖波列夫](../Page/圖波列夫.md "wikilink")，總部在[俄羅斯](../Page/俄羅斯.md "wikilink")[莫斯科](../Page/莫斯科.md "wikilink")
  - [龐巴迪公司](../Page/龐巴迪公司.md "wikilink")，總部在[加拿大](../Page/加拿大.md "wikilink")[蒙特婁](../Page/蒙特婁.md "wikilink")
  - [巴西航空工業](../Page/巴西航空工業.md "wikilink")，總部在[巴西](../Page/巴西.md "wikilink")

空中巴士、波音和圖波列夫這三家公司都是集中生產窄體和寬體客機，而龐巴迪和巴西航空工業這二家公司則主要生產區域客機，而中國的[中國商用飛機有限責任公司所研發的](../Page/中國商用飛機有限責任公司.md "wikilink")[ARJ-21將會加入市場](../Page/翔鳳.md "wikilink")\[4\]。飛機生產商多數只負責設計和進行最後組裝及試飛，而飛機零件是由世界各地不同機構所提供的。

直至70年代，大部份大型航空公司都是[國家航空公司](../Page/國家航空公司.md "wikilink")，受到政府保護，免受對手威脅。但自[開放天空政策推行之後](../Page/開放天空.md "wikilink")，不少航空公司紛紛成立，使機票價格下滑，再加上高燃油價格，高薪金及2001年所發生的[九一一事件和](../Page/九一一事件.md "wikilink")2003年所爆发的[SARS瘟疫](../Page/SARS.md "wikilink")，不少較年長的航空公司紛紛申請破產保護、政府保護或合併，但同時間，[廉價航空公司如](../Page/廉價航空公司.md "wikilink")[西南航空則成為這段時期的得益者](../Page/西南航空.md "wikilink")。

  - 空中巴士(Airbus)曾和勞斯萊斯公司合作生產[Concorde](../Page/Concorde.md "wikilink")(協和式客機)，為英法兩國合作的第一款也是最後一款飛機，這款飛機也是史上第一架超音速客機，巡航速度可達2.02馬赫

### 通用航空

通用航空指所有非定期航班，包括私人和商業性飛機。通用航空可以為公務航班、不定期航班、私人航班、飛行訓練、降落傘、[熱氣球](../Page/熱氣球.md "wikilink")、滑翔飛行、空中攝影、救護航班、特技飛行、空中巡邏警察或森林消防員。每個國家對通用航空都有不同規範，其規範則視符是私人性質還是商業性質。

目前全球有不少飛機製造商生產小型飛機，如[塞斯納](../Page/塞斯納.md "wikilink")、[莫拉凡等](../Page/莫拉凡.md "wikilink")，其市場主要是針對希望擁有私人飛機的顧客或飛行校的教練機。隨著航電系統的改善，以往只有大型飛機的航電也能加裝在小型飛機上，例如GPS，再加上複合材料的應用使飛機更輕且飛得更快，成為發展小型飛機的推動力。近年，超輕型飛機和自製飛機的普及，使許多國家允許私人飛行，能使政府節省金錢和減少規範去發出適航證書給於飛機。

## 軍用航空

[USAF_B-2_Spirit.jpg](https://zh.wikipedia.org/wiki/File:USAF_B-2_Spirit.jpg "fig:USAF_B-2_Spirit.jpg")\]\]

早在18世紀，人類已經發展沒有動力的監察熱氣球\[5\]。到了現今，[軍用飛機被大量需求和生產](../Page/軍用航空飛行器.md "wikilink")，軍機生產商之間是相互的競爭者，因為只有其中一方能取得某國的合約，提供軍機給於該國，而政府的考量主要是在飛機價錢、性能和速度。

**軍用飛機種類**

  - [戰鬥機是用來摧毀敵方軍機](../Page/戰鬥機.md "wikilink")，例如[蘇愷-27戰鬥機](../Page/蘇愷-27戰鬥機.md "wikilink")。
  - [攻擊機是用來攻擊地面目標](../Page/攻擊機.md "wikilink")，實施火力支援，例如[A-10雷霆二式攻擊機](../Page/A-10雷霆二式攻擊機.md "wikilink")。
  - [轟炸機是用來進行戰略轟炸](../Page/轟炸機.md "wikilink")，例如[B-2幽靈隱形戰略轟炸機](../Page/B-2幽靈隱形戰略轟炸機.md "wikilink")。
  - [運輸機是用來進行人力或物資運輸](../Page/運輸機.md "wikilink")，例如[安-124運輸機](../Page/安-124運輸機.md "wikilink")。
  - [偵察機是用來資料蒐集](../Page/偵察機.md "wikilink")，例如[U-2偵察機](../Page/U-2偵察機.md "wikilink")。
  - [直升機是用來進行火力支援和運輸之用](../Page/直升機.md "wikilink")。

## 航空交通管制

[HKIA_Control_Tower.JPG](https://zh.wikipedia.org/wiki/File:HKIA_Control_Tower.JPG "fig:HKIA_Control_Tower.JPG")的主塔台及航空交通管制大樓\]\]
航空交通管制負責與飛行員溝通和維持飛機距離，確保飛機不會因為太接近而導致相撞。而航管員要得知飛機位置，需要由飛行員提供，或是在較大型機場裡的[雷達中看到飛機位置](../Page/雷達.md "wikilink")。

航空交通管制的種類：

  - 中央管制員
  - 控制台，負責管制機場範圍內的飛機
  - 海洋管制員，負責管制飛機，大多是國際航班，大多數管制員沒有雷達
  - 終端管制員，負責管制機場範圍外飛機，約50-80公里

在儀表飛行中，航空交通管制十分重要，因為飛行員可能因天氣問題而看不到其它飛機或機場。縱使在較大型的機場裡，飛機能夠目視飛行，但飛行員都需要聽命於管制員，以維持空中秩序。而管制員會因為飛行員的工作量而提供不同情報去分隔飛機，例如[天氣廣播](../Page/天氣.md "wikilink")、地形、飛行輔助等資料。然而，管制員不可能控制所有航班，[北美洲所流行的目視飛行不需要時時刻刻留意管制員的命令](../Page/北美洲.md "wikilink")；而在某些地區中，例如[加拿大北部](../Page/加拿大.md "wikilink")，因為沒有航空交通管制服務，因此該地方不能夠進行低空飛行。

## 環境影響

就像[燃燒一樣](../Page/燃燒.md "wikilink")，有动力的飛機都會釋放[溫室氣體如](../Page/溫室氣體.md "wikilink")[二氧化碳](../Page/二氧化碳.md "wikilink")、[煤煙及其它物質](../Page/煤煙.md "wikilink")，其環境影響如下：

  - 當飛機接近[對流層頂時](../Page/對流層.md "wikilink")，會釋出[飛機雲和](../Page/飛機雲.md "wikilink")[氣溶膠](../Page/氣溶膠.md "wikilink")，會增加天空中的[卷雲](../Page/卷雲.md "wikilink")，最多能增加高達0.2%的雲層\[6\]。
  - 當飛機接近[對流層頂時](../Page/對流層.md "wikilink")，會產生化學藥品，會互相影響該高度的溫室氣體，尤其是[氮氧化物](../Page/氮氧化物.md "wikilink")，會增加[臭氧的濃度](../Page/臭氧.md "wikilink")。\[7\]\[8\]
  - 大多數小型活塞飛機所燃燒的汽油，都含有[四乙基鉛](../Page/四乙基鉛.md "wikilink")，是一種高毒性物質，能夠污染土地和機場。

## 参看

  - [航空工程](../Page/航空工程.md "wikilink")

## 參考資料

### 參考書目

  -
[航空](../Category/航空.md "wikilink")

1.
2.
3.
4.  [China's Aircraft Industry Gets Off the
    Ground](http://www.time.com/time/magazine/article/0,9171,1670256,00.html)
5.  中國大百科，[氣球](../Page/氣球.md "wikilink")
6.  [Aviation and the Global Atmosphere
    (IPCC)](http://www.grida.no/climate/ipcc/aviation/032.htm)
7.
8.