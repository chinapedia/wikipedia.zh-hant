**比罗达特查巴伯瑞阿迪赫足球俱乐部**（**Chabab Riadhi de
Belouizdad**；[阿拉伯语](../Page/阿拉伯语.md "wikilink")：الشباب
الرياضي
بيلوزيداد）是位于[阿尔及利亚首都](../Page/阿尔及利亚.md "wikilink")[阿尔及尔贝尔考特区的足球俱乐部](../Page/阿尔及尔.md "wikilink")。创建于1962年，俱乐部的主体育场为1955年8月20日体育场（Stade
20 Août 1955）。

## 球队荣誉

  - **[阿爾及利亞足球甲級聯賽](../Page/阿爾及利亞足球甲級聯賽.md "wikilink")**: 6次

1965, 1966, 1969, 1970, 2000, 2001

  - **[阿尔及利亚盃](../Page/阿尔及利亚盃.md "wikilink")**: 5次

1966, 1969, 1970, 1978, 1995

  - **[阿尔及利亚联赛盃](../Page/阿尔及利亚联赛盃.md "wikilink")**: 1次

2000

  - **[马格里布锦标赛](../Page/马格里布锦标赛.md "wikilink")**: 3次

1970, 1971 ;1972

## 外部链接

  - [Official
    website](https://web.archive.org/web/20070612031625/http://www.cr-belouizdad.com/)

[Category:阿尔及利亚足球俱乐部](../Category/阿尔及利亚足球俱乐部.md "wikilink")