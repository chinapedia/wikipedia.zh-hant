**Libwww**﹝*[**Lib**rary](../Page/函式庫.md "wikilink") [**W**orld **W**ide
**W**eb](../Page/全球資訊網.md "wikilink")*﹞，是一個高度模組化用戶端的網路[应用程序接口](../Page/应用程序接口.md "wikilink")，用[C語言寫成](../Page/C語言.md "wikilink")，可在[UNIX操作系统和](../Page/UNIX操作系统.md "wikilink")[Windows操作系统上運行](../Page/Windows操作系统.md "wikilink")。

Libwww的目的是作為[協定實驗的測試平台](../Page/網路傳輸協定.md "wikilink")\[1\]，使軟件開發人員不必“重新發明車輪”\[2\]。

## 歷史

1991年和1992年，[蒂姆·伯納斯-李和](../Page/蒂姆·伯納斯-李.md "wikilink")[CERN的一名學生](../Page/CERN.md "wikilink")[揚-弗朗索瓦·格羅夫利用](../Page/Jean-Francois_Groff.md "wikilink")[可攜式](../Page/移植_\(軟體\).md "wikilink")[C代碼重寫了](../Page/C語言.md "wikilink")[NeXTSTEP作業系統原始的](../Page/NeXTSTEP.md "wikilink")[WorldWideWeb瀏覽器各種組件](../Page/WorldWideWeb.md "wikilink")，用於展示[全球資訊網的潛力](../Page/全球資訊網.md "wikilink")\[3\]。最初，Libwww被稱為*Common
Library*，無法作為單獨的產品使用\[4\]。在Libwww普及使用之前，整合在CERN程式庫中。1993年5月伯納斯-李宣布，*Common
Library*稱為Libwww，並被授權為[公有領域](../Page/公有領域.md "wikilink")，以鼓勵[網頁瀏覽器的開發](../Page/網頁瀏覽器.md "wikilink")\[5\]。他最初考慮在[GNU通用公共授權條款下發佈軟體](../Page/GNU通用公共授權條款.md "wikilink")，而不是公有領域，但是由於擔心像[IBM這樣的大型公司會被GPL的限制而不敢使用](../Page/IBM.md "wikilink")，所以決定不這麼做\[6\]\[7\]。函式庫早期的快速發展導致了[羅伯特·卡里奧整合到他的](../Page/羅伯特·卡里奧.md "wikilink")[MacWWW瀏覽器中出現了問題](../Page/MacWWW.md "wikilink")\[8\]。

從1994年11月25日（版本2.17）開始[亨利克·弗里斯蒂克·尼耳森負責Libwww](../Page/亨利克·弗里斯蒂克·尼耳森.md "wikilink")\[9\]。1995年3月21日，隨著版本3.0發布，CERN將Libwww移至[全球資訊網協會](../Page/全球資訊網協會.md "wikilink")（W3C）管理\[10\]。從1995年起，[Line
Mode
Browser不再單獨發布](../Page/Line_Mode_Browser.md "wikilink")，而是Libwww組件的一部分\[11\]。

W3C建立了[Arena網頁瀏覽器作為](../Page/Arena_\(瀏覽器\).md "wikilink")[HTML3](../Page/HTML.md "wikilink")、[CSS](../Page/CSS.md "wikilink")、[PNG和其他特性如Libwww的試驗平台和測試工具](../Page/PNG.md "wikilink")\[12\]，但在[beta](../Page/軟體版本週期.md "wikilink")
3之後，Arena被[Amaya取代](../Page/Amaya.md "wikilink")\[13\]。2003年9月2日，W3C由於缺乏資源而停止了Libwww的開發。基於其為開放原始碼的特性，任何人都能為Libwww付出一點心力，這也確保了Libwww能一直進步，成為更有用的軟體\[14\]。

使用Libwww的應用程式，如被廣泛使用的[Lynx及](../Page/Lynx.md "wikilink")[Mosaic即是用Libwww所寫成的](../Page/Mosaic.md "wikilink")\[15\]。

## 参考文献

## 外部連結

  -
  - [Libwww Hackers](http://www.w3.org/Library/Collaborators.html)

  - [Libwww
    Architecture](http://www.w3.org/Library/User/Architecture/Overview.html)

[Category:应用程序接口](../Category/应用程序接口.md "wikilink")
[Category:C函式庫](../Category/C函式庫.md "wikilink")
[Category:HTTP客户端](../Category/HTTP客户端.md "wikilink")
[Category:用C編程的自由軟體](../Category/用C編程的自由軟體.md "wikilink")
[Category:自由跨平台軟體](../Category/自由跨平台軟體.md "wikilink")

1.

2.

3.

4.

5.

6.

7.

8.

9.

10.
11.

12.

13.

14.

15.