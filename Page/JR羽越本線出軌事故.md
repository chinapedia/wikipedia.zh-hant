[Inaho_express_2.jpg](https://zh.wikipedia.org/wiki/File:Inaho_express_2.jpg "fig:Inaho_express_2.jpg")
[Inaho_14_derailed_in_Yamagata.png](https://zh.wikipedia.org/wiki/File:Inaho_14_derailed_in_Yamagata.png "fig:Inaho_14_derailed_in_Yamagata.png")
**JR羽越本線出軌事故**（****）是一宗在2005年12月25日時，發生於[日本](../Page/日本.md "wikilink")[山形縣](../Page/山形縣.md "wikilink")[最上川的](../Page/最上川.md "wikilink")[鐵路事故](../Page/鐵路事故.md "wikilink")，一列[JR東日本](../Page/東日本旅客鐵道.md "wikilink")[羽越本線的](../Page/羽越本線.md "wikilink")[特急電車因為出軌撞擊路旁小屋](../Page/特急電車.md "wikilink")，造成5死33傷。此事故是繼[JR福知山線出軌事故後](../Page/JR福知山線出軌事故.md "wikilink")，同年內日本第二宗重大鐵路事故。\[1\]

## 事故概述

JR東日本一列由[秋田車站經](../Page/秋田車站.md "wikilink")[酒田車站開往](../Page/酒田車站.md "wikilink")[新潟車站的](../Page/新潟車站.md "wikilink")[稻穗14號](../Page/稻穗號列車.md "wikilink")（<span lang="ja">いなほ
Inaho</span>）特急電車（車款為[485系3000番台](../Page/日本國鐵485系電力動車組.md "wikilink")，全車由6輛車廂組成），於當地時間晚上7時14分，駛經[北余目站](../Page/北余目站.md "wikilink")─[砂越站之間的第](../Page/砂越站.md "wikilink")2最上川橋樑後，突然發出怪聲，電車疑被強風吹起而向左浮起，之後便在風雪中發生出軌事故。最先頭的3輛（6號車─4號車）墮下幾米下的小屋旁，後2輛（3號車及2號車）則只有出軌，未有墮下小屋旁，最後一輛（1號車）沒有出軌。

出軌事故造成5人死亡，33人受傷，而死者全為最頭1輛（1號車，自由席）的乘客，出事特急電車的司機鈴木高司受輕傷（電車司機資料：29歲／1997年入職／資歷約4─5年）。

據調查報告，該班特急電車約有14人購入指定席車票，自由席乘搭人數約30人。全車共約有44名乘客。電車出事前，曾在酒田站因大雪影響，延遲68分鐘，至19時08分才開出。

電車出軌位置的最高速度限制為120km/h，而估計當時電車的行走速度約100km/h。

## 原因

原因尚在查明中，懷疑當時電車駛經第2號最上川橋樑時，遇烈風吹起，因而發生出軌事故。

## 後續處理

肇事列車由於損毀嚴重，最終會被拆解報銷。

2006年9月19日，JR東日本公司宣佈會於同年12月，即事故發生一週年，於事發地點建造一[紀念碑](../Page/紀念碑.md "wikilink")，以悼念此次意外的5名死者。

## 参考資料

<references/>

[Category:日本鐵路事故](../Category/日本鐵路事故.md "wikilink")
[Category:2005年日本](../Category/2005年日本.md "wikilink")
[Category:2005年鐵路事故](../Category/2005年鐵路事故.md "wikilink")
[Category:山形縣歷史](../Category/山形縣歷史.md "wikilink")
[Category:庄內町](../Category/庄內町.md "wikilink")
[Category:2005年12月](../Category/2005年12月.md "wikilink")

1.