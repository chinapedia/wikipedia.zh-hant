[Pyrite_sample.jpg](https://zh.wikipedia.org/wiki/File:Pyrite_sample.jpg "fig:Pyrite_sample.jpg")

**黄铁矿**，主要成分是[二硫化亚铁FeS](../Page/二硫化亚铁.md "wikilink")<sub>2</sub>，是提取[硫](../Page/硫.md "wikilink")、制造[硫酸的主要矿物原料](../Page/硫酸.md "wikilink")。其特殊的形态色泽，有观赏价值。一些黄铁矿磨制成[宝石也很受欢迎](../Page/宝石.md "wikilink")。

黃鐵礦可經由岩漿分結作用、熱水溶液或[昇華作用中生成](../Page/昇華.md "wikilink")，也可於[火成岩](../Page/火成岩.md "wikilink")、[沉積岩中生成](../Page/沉積岩.md "wikilink")。在工業上，黃鐵礦用作[硫和](../Page/硫.md "wikilink")[二氧化硫生成的原料](../Page/二氧化硫.md "wikilink")。

## 性状

[Pyrite_elbe.jpg](https://zh.wikipedia.org/wiki/File:Pyrite_elbe.jpg "fig:Pyrite_elbe.jpg")

  - 成份：46.67%的Fe和53.33%的S。
  - 颜色：浅黄铜色 ，表面带有黄褐的锖色，颇似[黄金](../Page/黄金.md "wikilink")。
  - 光泽：强金属光泽
  - [晶体形态](../Page/晶体.md "wikilink")：等軸晶系或立方晶系，通常呈[立方体](../Page/立方体.md "wikilink")、五角[十二面体](../Page/十二面体.md "wikilink")，较少呈[八面体](../Page/八面体.md "wikilink")。
  - [条痕](../Page/条痕.md "wikilink")：绿黑色
  - [硬度](../Page/硬度.md "wikilink")：6-6.5 性脆
  - 比重：5
  - 断口：参差状

其颜色为淡金褐色，容易让人以为是黄金，故有“愚人金”之称。看它在不带[釉的白](../Page/釉.md "wikilink")[瓷板上划出的条痕可以检验出来](../Page/瓷.md "wikilink")，金矿的条痕是金黄色的，黄铁矿的条痕是绿黑色的。

## 参看

  - [矿物列表](../Page/矿物列表.md "wikilink")
  - [宝石](../Page/宝石.md "wikilink")

## 參考資料

## 參考

  - [黃鐵礦 - Webmineral](http://webmineral.com/data/Pyrite.shtml)
  - [黃鐵礦 - Mindat.org](http://www.mindat.org/min-3314.html)

[Category:宝石](../Category/宝石.md "wikilink")
[Category:含铁矿物](../Category/含铁矿物.md "wikilink")
[Category:硫化物礦物](../Category/硫化物礦物.md "wikilink")