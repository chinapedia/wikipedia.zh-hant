**吕丽萍**（），[中国電影](../Page/中国.md "wikilink")、電視劇[女演员](../Page/女演员.md "wikilink")，[中国共产党党员](../Page/中国共产党.md "wikilink")，出生于[北京市](../Page/北京市.md "wikilink")。

## 事业

  - 1984年毕业于[中央戏剧学院表演系](../Page/中央戏剧学院.md "wikilink")，即被分配至[上海电影制片厂](../Page/上海电影制片厂.md "wikilink")。同年，在[黄蜀芹导演的影片](../Page/黄蜀芹.md "wikilink")《[童年的朋友](../Page/童年的朋友.md "wikilink")》中饰演罗姐，这也是吕丽萍的[电影处女作](../Page/电影.md "wikilink")。此后参与演出了许多影视作品，并获得观众的好评。

<!-- end list -->

  - 1988年，在[吴天明导演的影片](../Page/吴天明.md "wikilink")《[老井](../Page/老井.md "wikilink")》中饰演段凤喜（旺泉媳妇），与饰演孙旺泉的[张艺谋演对手戏](../Page/张艺谋.md "wikilink")，获得了成功。因此获得了当年度的中国电影[金鸡奖和](../Page/金鸡奖.md "wikilink")《大众电影》[百花奖的最佳女配角](../Page/百花奖.md "wikilink")，以及中国电影表演艺术学会奖。同年的《[龙年警官](../Page/龙年警官.md "wikilink")》一片，吕丽萍与当时还是自己丈夫的[张丰毅共同出演](../Page/张丰毅.md "wikilink")。张丰毅是吕丽萍的第一任丈夫，
    1989年3月，儿子张博宇出世。1991年，吕丽萍与张丰毅离婚。但两人离婚之后就不愿在公众面前提到此片。

<!-- end list -->

  - 1990年参与演出了[夏钢导演的](../Page/夏钢.md "wikilink")、被称为是“（20世纪）90年代中国都市电影开先河之作”的《遭遇激情》。

同年还参与演出[冯小宁导演的](../Page/冯小宁.md "wikilink")、反映[环境问题的](../Page/环境问题.md "wikilink")[科幻电影](../Page/科幻电影.md "wikilink")《[大气层消失](../Page/大气层消失.md "wikilink")》。

  - 1990年可谓是吕丽萍高产的一年，她除了参与了多部电影演出之外，还在[黄蜀芹导演的](../Page/黄蜀芹.md "wikilink")[电视剧](../Page/电视剧.md "wikilink")《[围城](../Page/围城.md "wikilink")》中饰演孙柔嘉，开始被电视观众认识并认可。并且因此再次获得了表演学会奖。

<!-- end list -->

  - 1991年参与演出的电视剧《[编辑部的故事](../Page/编辑部的故事.md "wikilink")》使吕丽萍获得了电视观众的广泛好评。在这部由[北京电视艺术中心制作的长篇电视连续剧中](../Page/北京电视艺术中心.md "wikilink")，吕丽萍扮演的是一名普通的杂志社编辑。生活化的对白和剧情让观众在观看当中获得了共鸣。吕丽萍也因此获得了[中国电视飞天奖最佳女演员奖](../Page/中国电视飞天奖.md "wikilink")。

<!-- end list -->

  - 1992年，在[周晓文导演的影片](../Page/周晓文.md "wikilink")《[青春无悔](../Page/青春无悔.md "wikilink")》中饰演刘洁。再次获得了[《大众电影》百花奖最佳女配角](../Page/《大众电影》百花奖.md "wikilink")。

<!-- end list -->

  - 1993年，在[田壮壮导演的影片](../Page/田壮壮.md "wikilink")《[蓝风筝](../Page/蓝风筝.md "wikilink")》中扮演了陈樹娟（妈妈）一角，与多位著名演员共同完成了这部讲述[文革及文革前普通人生活的电影](../Page/文革.md "wikilink")。但后来本片在中国大陆地区被禁映，所以很多人没能欣赏到吕丽萍在本片中的表演。吕丽萍凭本片获得了第六届[东京国际电影节最佳女演员奖和第一届](../Page/东京国际电影节.md "wikilink")[新加坡国际电影节最佳女演员奖](../Page/新加坡国际电影节.md "wikilink")。

<!-- end list -->

  - 1993年吕丽萍的演艺生涯达到了一个高峰，这一年她获得了首届“中国十大影视明星”的称号。并且在当年创办了“[北京群星表演艺术学校](../Page/北京群星表演艺术学校.md "wikilink")”。此后一段时间，吕丽萍逐渐淡出了演艺圈，把主要精力放在了学校的教学和管理上。

<!-- end list -->

  - 1996年吕丽萍重新开始演出影视剧。1996年出演了大连电视台摄制的电视剧《雷锋的死与我有关》，在其中扮演了[乔安山的妻子](../Page/乔安山.md "wikilink")。此后，吕丽萍参与演出了多部独立制作电影。1997年参与演出[路学长的电影](../Page/路学长.md "wikilink")《[长大成人](../Page/长大成人.md "wikilink")》，但并未引起广泛反响。同年参与演出了[张扬导演的电影](../Page/张扬.md "wikilink")《[爱情麻辣烫](../Page/爱情麻辣烫.md "wikilink")》。在这部由五部分故事组成的电影中，吕丽萍在第三部分“十三香”里扮演了一名处于“[中年危机](../Page/中年危机.md "wikilink")”的普通女性，与[濮存昕扮演的丈夫进行着离婚](../Page/濮存昕.md "wikilink")“拉锯战”。

<!-- end list -->

  - 1996年春天，吕丽萍带着儿子在北京先农坛观看篮球赛，认识了从四川全兴队退役的足球运动员陶伟。1999年12月16日，吕丽萍和[陶伟在洛杉矶华清信会教堂举行了婚礼](../Page/陶伟_\(1966年出生\).md "wikilink")。\[1\]

<!-- end list -->

  - 1999年在[香港独立导演](../Page/香港.md "wikilink")[余力为的影片](../Page/余力为.md "wikilink")《[天上人间](../Page/天上人間_\(1999年電影\).md "wikilink")》中饰演电梯小姐阿燕。吕丽萍因此片获得了1999年台湾电影[金马奖的最佳女配角提名](../Page/金马奖.md "wikilink")。在此期间，吕丽萍也参演了多部电视剧和贺岁电视短剧。

<!-- end list -->

  - 2001年无疑是吕丽萍的又一个演艺高峰。在[黄建新导演的轻喜剧电影](../Page/黄建新.md "wikilink")《[谁说我不在乎](../Page/谁说我不在乎.md "wikilink")》中，扮演了一名因为找不到结婚证而对生活、婚姻产生怀疑的中年母亲谢雨婷。同年在[康洪雷导演的长篇电视连续剧](../Page/康洪雷.md "wikilink")《[激情燃烧的岁月](../Page/激情燃烧的岁月.md "wikilink")》中，扮演了石光荣的妻子褚琴。《激情燃烧的岁月》的热播让吕丽萍再次获得人们的关注。也有人认为，这部剧真正引起人们注意的，是它的剧情和[孙海英](../Page/孙海英.md "wikilink")（饰演石光荣）的表演，吕丽萍的演出并没有很好的表现。但无论如何，吕丽萍因本片获得了第三届[中国电视金鹰奖最受欢迎女演员奖](../Page/中国电视金鹰奖.md "wikilink")。

<!-- end list -->

  - 2002年出演《[激情燃烧的岁月](../Page/激情燃烧的岁月.md "wikilink")》之后，吕丽萍与孙海英合作演出[彭小莲导演的影片](../Page/彭小莲.md "wikilink")《[假装没感觉](../Page/假装没感觉.md "wikilink")》，并拍摄纪录片《[心中的节日](../Page/心中的节日.md "wikilink")》。

<!-- end list -->

  - 2010年，凭《玩酷青春》中单身母亲一角获得[第47届金马奖最佳女主角奖](../Page/第47届金马奖.md "wikilink")。

## 家庭

2001年8月2日，吕丽萍因性格不合和陶伟结束婚姻。\[2\]
2002年，吕丽萍与[孙海英在沈阳登记结婚](../Page/孙海英.md "wikilink")，
孙海英当时是沈阳话剧团的演员。关于他们的婚姻生活, 吕丽萍曾在访谈节目《影视风云》中透露说, 她的丈夫孙海英是个急性子, 凡事都 ‘反应过快’，
常常使她 ‘跟不上节奏’。 另外她说最不能容忍的是孙海英爱上网的习惯。

## 爭議

2011年6月29日，吕丽萍在她的[新浪微博上转发某位牧师反对](../Page/新浪微博.md "wikilink")[同性恋者的言论](../Page/同性恋.md "wikilink")\[3\]，并注转发理由“[给力](../Page/给力.md "wikilink")”、“兄弟姊妹转起来”等内容，遭到中国同性恋群体抵制以及中国社会活动人士的反对，中国社会学家[李银河也在](../Page/李銀河_\(社會學家\).md "wikilink")[她自己的微博](http://weibo.com/liyinhe)表示“如果是个普通老百姓也就罢了，有人开放，有人保守，是很自然的。可是作为公众人物发表这样反动的言论就不可原谅。”\[4\]。吕丽萍反同志言论也很快便引起媒体热议，[搜狐](../Page/搜狐.md "wikilink")\[5\]，[网易](../Page/网易.md "wikilink")\[6\]，以及台湾媒体都大肆报道并批评了吕丽萍的反同志言论。[阿信](../Page/阿信.md "wikilink")（[蘇見信](../Page/蘇見信.md "wikilink")）更是飙脏话愤怒回应吕丽萍\[7\]。甚至连一向立场保守的官媒[央视也在节目中称应](../Page/央视.md "wikilink")“尊重每个群体的自我选择。”\[8\]

金馬影后呂麗萍的反同志言論，風波繼續延燒。依金馬獎慣例，上屆得主將獲邀次年出席頒獎，但金馬獎7月1日在知會主席[侯孝賢之後](../Page/侯孝賢.md "wikilink")，由執委會作出回應：「金馬不能控制得主的發言，但不支持也不認同任何歧視言論。關於邀請呂小姐來台頒獎一事，金馬會暫緩進行。」

## 作品

### 电影

<table>
<thead>
<tr class="header">
<th><p>年</p></th>
<th><p>英文名</p></th>
<th><p>中文名</p></th>
<th><p>角色</p></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1984</p></td>
<td></td>
<td><p>《童年的朋友》</p></td>
<td><p>罗姐</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1985</p></td>
<td></td>
<td><p>《张家少奶奶》</p></td>
<td><p>客串</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>《大树底下》</p></td>
<td><p>五月</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1986</p></td>
<td></td>
<td><p>《<a href="../Page/老井_(电影).md" title="wikilink">老井</a>》</p></td>
<td><p>段喜凤</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>《嘿！哥儿们》</p></td>
<td><p>客串</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1987</p></td>
<td></td>
<td><p>《危险的蜜月旅行》</p></td>
<td><p>客串</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1988</p></td>
<td></td>
<td><p>《龙年警官》</p></td>
<td><p>妻子</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>《荒火》</p></td>
<td><p><a href="../Page/景颇族.md" title="wikilink">景颇族女子</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1990</p></td>
<td></td>
<td><p>《大气层消失》</p></td>
<td><p>妇女</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>《<a href="../Page/围城_(电影).md" title="wikilink">围城</a>》</p></td>
<td><p>孙柔嘉</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>《<a href="../Page/遭遇激情.md" title="wikilink">遭遇激情</a>》</p></td>
<td><p>梁小清</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1991</p></td>
<td></td>
<td><p>《<a href="../Page/青春无悔_(电影).md" title="wikilink">青春无悔</a>》</p></td>
<td><p>前妻</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>《独身女人》</p></td>
<td><p>客串</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1997</p></td>
<td></td>
<td><p>《<a href="../Page/爱情麻辣烫.md" title="wikilink">爱情麻辣烫</a>》</p></td>
<td><p>淑慧</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1997</p></td>
<td></td>
<td><p>《编辑部的故事之万事如意》</p></td>
<td><p>戈玲</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1998</p></td>
<td></td>
<td><p>《北京人》</p></td>
<td><p>增思婴</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1999</p></td>
<td></td>
<td><p>《天上人间》</p></td>
<td><p>阿燕</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2000</p></td>
<td></td>
<td><p>《好日子一块过》</p></td>
<td><p>陆姐</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2001</p></td>
<td></td>
<td><p>《谁说我不在乎》</p></td>
<td><p>谢雨婷</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>《<a href="../Page/假装没感觉.md" title="wikilink">假装没感觉</a>》</p></td>
<td><p>妈妈</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2002</p></td>
<td></td>
<td><p>《心中的节日》</p></td>
<td><p>客串</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2003</p></td>
<td></td>
<td><p>《团圆两家亲》</p></td>
<td><p>欣然</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2008</p></td>
<td></td>
<td><p>《<a href="../Page/二十四城记.md" title="wikilink">二十四城记</a>》</p></td>
<td><p>大丽</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2010</p></td>
<td></td>
<td><p>《堵车》</p></td>
<td><p>年华</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>《<a href="../Page/山楂树之恋_(电影).md" title="wikilink">山楂树之恋</a>》</p></td>
<td><p>客串</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>《<a href="../Page/玩酷青春.md" title="wikilink">玩酷青春</a>》</p></td>
<td><p>罗素芳</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>《百合》</p></td>
<td><p>女作家</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2011</p></td>
<td></td>
<td><p>《<a href="../Page/一八九四·甲午大海戰.md" title="wikilink">一八九四·甲午大海戰</a>》</p></td>
<td><p><a href="../Page/慈禧太后.md" title="wikilink">慈禧太后</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2012</p></td>
<td></td>
<td><p>《<a href="../Page/今世姻缘.md" title="wikilink">今世姻缘</a>》</p></td>
<td><p>姚金娥</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2016</p></td>
<td></td>
<td><p>《非同小可》</p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 电视剧

| 年                                                   | 中文名                                          | 角色                                      | 备注   |
| --------------------------------------------------- | -------------------------------------------- | --------------------------------------- | ---- |
| 1987                                                | 《[山月](../Page/山月.md "wikilink")》             | 山月                                      |      |
| 1989                                                | 《[围城](../Page/围城_\(电视剧\).md "wikilink")》     | 孙柔嘉                                     |      |
| 1990                                                | 《[编辑部的故事](../Page/编辑部的故事.md "wikilink")》     | 戈玲                                      |      |
| 1996                                                | 《[雷锋的死与我有关](../Page/雷锋的死与我有关.md "wikilink")》 | 乔安山妻子                                   |      |
| 1997                                                | 《[康熙情锁金殿](../Page/康熙情锁金殿.md "wikilink")》     | 韩母                                      |      |
| 《[看不见的太阳](../Page/看不见的太阳.md "wikilink")》            | 客串                                           |                                         |      |
| 1998                                                | 《[来来往往](../Page/来来往往.md "wikilink")》         | 段丽娜                                     |      |
| 《[家有仙妻2](../Page/家有仙妻2.md "wikilink")》              | 卓寡妇                                          |                                         |      |
| 2001                                                | 《[少年黄飞鸿](../Page/少年黄飞鸿.md "wikilink")》       | 吴娴                                      |      |
| 《[七品钦差](../Page/七品钦差.md "wikilink")》                | 满夫人                                          |                                         |      |
| 《[激情燃烧的岁月](../Page/激情燃烧的岁月.md "wikilink")》          | 褚琴                                           |                                         |      |
| 《[大栅栏](../Page/大栅栏.md "wikilink")》                  | 慈禧太后                                         |                                         |      |
| 2002                                                | 《[群英会](../Page/群英会.md "wikilink")》           | 凤姨                                      |      |
| 《[十三格格](../Page/十三格格.md "wikilink")》                | [慈禧太后](../Page/慈禧太后.md "wikilink")           |                                         |      |
| 《[大脚马皇后](../Page/大脚马皇后.md "wikilink")》              | 马皇后                                          |                                         |      |
| 2003                                                | 《[大唐歌飞](../Page/大唐歌飞.md "wikilink")》         | 梅妃                                      |      |
| 《[射雕英雄传](../Page/射雕英雄传_\(2003年电视剧\).md "wikilink")》 | 李萍                                           |                                         |      |
| 2004                                                | 《[皇后驾到](../Page/皇后驾到.md "wikilink")》         | [马皇后](../Page/马皇后_\(明朝\).md "wikilink") |      |
| 2006                                                | 《[非常夫妻](../Page/非常夫妻.md "wikilink")》         | 田歌                                      |      |
| 《[谁为梦想买单](../Page/谁为梦想买单.md "wikilink")》            | 马翠兰                                          |                                         |      |
| 《[最亲的敌人](../Page/最亲的敌人.md "wikilink")》              | 唐大夫                                          |                                         |      |
| 2007                                                | 《[大山的儿子](../Page/大山的儿子.md "wikilink")》       | 邓淑芬                                     |      |
| 2008                                                | 《[天地民心](../Page/天地民心.md "wikilink")》         | 刘氏                                      |      |
| 《[天伦劫](../Page/天伦劫.md "wikilink")》                  | 林秋霞                                          |                                         |      |
| 2009                                                | 《[人活一张脸](../Page/人活一张脸.md "wikilink")》       | 潘凤霞                                     |      |
| 2010                                                | 《[你是我兄弟](../Page/你是我兄弟.md "wikilink")》       | 秦大姐                                     |      |
| 2011                                                | 《[傻春](../Page/傻春.md "wikilink")》             | 许敏荣                                     |      |
| 2012                                                | 《[从将军到士兵](../Page/从将军到士兵.md "wikilink")》     | 李露瑾                                     |      |
| 《[六块六毛六那点事](../Page/六块六毛六那点事.md "wikilink")》        | 那校长                                          |                                         |      |
| 2013                                                | 《[新编辑部的故事](../Page/新编辑部的故事.md "wikilink")》   | 戈玲                                      |      |
| 2014                                                | 《[让生活充满阳光](../Page/让生活充满阳光.md "wikilink")》   | 张玉英                                     |      |
| 《[生活永远沸腾](../Page/生活永远沸腾.md "wikilink")》            | 李新凤                                          |                                         |      |
| 2015                                                | 《[8848](../Page/8848.md "wikilink")》         |                                         | 戲份被刪 |
| 2017                                                | 《[东方有大海](../Page/东方有大海.md "wikilink")》       | [慈禧太后](../Page/慈禧太后.md "wikilink")      |      |
| 待播                                                  | 《[传承](../Page/传承.md "wikilink")》             | 客串                                      |      |
|                                                     |                                              |                                         |      |

## 奖项与提名

<table style="width:178%;">
<colgroup>
<col style="width: 72%" />
<col style="width: 20%" />
<col style="width: 11%" />
<col style="width: 11%" />
<col style="width: 64%" />
</colgroup>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>颁奖典礼</p></th>
<th><p>奖项</p></th>
<th><p>作品</p></th>
<th><p>结果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1988年</p></td>
<td><p>第八届中国电影金鸡奖</p></td>
<td><p>最佳女配角</p></td>
<td><p>《老井》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第11届大众电影百花奖</p></td>
<td><p>最佳女配角</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>首届中国电影表演艺术学会金凤凰奖</p></td>
<td><p>表演学会奖</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>1991年</p></td>
<td><p>第11届中国电影金鸡奖</p></td>
<td><p>最佳女主角</p></td>
<td><p>《遭遇激情》</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1992年</p></td>
<td><p>第15届大众电影百花奖</p></td>
<td><p>最佳女配角</p></td>
<td><p>《青春无悔》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第六届东京国际电影节</p></td>
<td><p>最佳女演员</p></td>
<td><p>《蓝风筝》</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>首届新加坡国际电影节</p></td>
<td><p>最佳女演员</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第12届中国电视剧飞天奖</p></td>
<td><p>最佳女主角</p></td>
<td><p>《编辑部的故事》</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>1998年</p></td>
<td><p>第18届中国电影金鸡奖</p></td>
<td><p>最佳女配角</p></td>
<td><p>《爱情麻辣烫》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1999年</p></td>
<td><p>第36届台湾电影金马奖</p></td>
<td><p>最佳女配角</p></td>
<td><p>《天上人间》</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2002年</p></td>
<td><p>第20届中国电视金鹰奖</p></td>
<td><p>观众喜爱的女演员</p></td>
<td><p>《激情燃烧的岁月》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第二届华语电影传媒大奖</p></td>
<td><p>内地最佳女主角</p></td>
<td><p>《虽说我不在乎》</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2003年</p></td>
<td><p>第21届中国电视金鹰奖</p></td>
<td><p>观众喜爱的女演员</p></td>
<td><p>《大脚马皇后》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第三届华语电影传媒大奖</p></td>
<td><p>最受欢迎女演员银奖</p></td>
<td><p>《假装没感觉》</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2010年</p></td>
<td><p>第13届上海国际电影节传媒大奖</p></td>
<td><p>最佳女主角</p></td>
<td><p>《玩酷青春》</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第47届台湾电影金马奖</p></td>
<td><p>最佳女主角</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2011年</p></td>
<td><p>第14届中国电影华表奖</p></td>
<td><p>优秀女演员</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第28届中国电影金鸡奖</p></td>
<td><p>最佳女主角</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 参考文献

## 外部链接

  -
  -
  -
  -
  -
  -
[Category:中华人民共和国新教徒](../Category/中华人民共和国新教徒.md "wikilink")
[Category:中华人民共和国时期中国共产党女党员](../Category/中华人民共和国时期中国共产党女党员.md "wikilink")
[Category:中国电视女演员](../Category/中国电视女演员.md "wikilink")
[Category:中國電影女演員](../Category/中國電影女演員.md "wikilink")
[Category:中央戏剧学院校友](../Category/中央戏剧学院校友.md "wikilink")
[Category:金馬獎最佳女主角獲得者](../Category/金馬獎最佳女主角獲得者.md "wikilink")
[Category:东京影展获奖者](../Category/东京影展获奖者.md "wikilink")
[Category:華語電影傳媒大獎最佳女主角得主](../Category/華語電影傳媒大獎最佳女主角得主.md "wikilink")
[Category:北京人](../Category/北京人.md "wikilink")
[L](../Category/吕姓.md "wikilink")
[Category:北京女演员](../Category/北京女演员.md "wikilink")
[Category:中国电影金凤凰奖表演学会奖得主](../Category/中国电影金凤凰奖表演学会奖得主.md "wikilink")

1.

2.
3.  [1](http://weibo.com/1997726860/eCR80MQBz3R)

4.  [](http://weibo.com/1195201334/eCU8icazauL)

5.  [2](http://roll.sohu.com/20110701/n312156139.shtml)

6.  [3](http://ent.163.com/11/0629/23/77OLL38F00031H2L.html)

7.  [4](http://ent.163.com/11/0630/12/77Q11JRU00031H2L.html)

8.  [央视批吕丽萍"反同"言论:同性恋者权利不容侵犯](http://ent.163.com/11/0705/09/786JE5KA00031H2L.html)