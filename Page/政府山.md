[HK_Battery_Path_6-1.jpg](https://zh.wikipedia.org/wiki/File:HK_Battery_Path_6-1.jpg "fig:HK_Battery_Path_6-1.jpg")\]\]
**政府山**（**Government
Hill**）是[香港的一個山丘](../Page/香港.md "wikilink")，位於[香港島](../Page/香港島.md "wikilink")[中環南部](../Page/中環.md "wikilink")，自[香港開埠以來直至](../Page/香港開埠.md "wikilink")2011年，一直是[香港政府的心臟地帶](../Page/香港政府.md "wikilink")，是香港名副其實的政治權力中心。然而，這個地名較為少用，一般以[中環作為統稱](../Page/中環.md "wikilink")。

## 歷史

「政府山」可溯源自[英國於](../Page/英國.md "wikilink")1841年管治香港後，將[花園道](../Page/花園道.md "wikilink")、[上亞厘畢道](../Page/上亞厘畢道.md "wikilink")、[己連拿利](../Page/己連拿利.md "wikilink")、[雪廠街及](../Page/雪廠街.md "wikilink")[炮台里所圍繞的山坡劃為行政及管治中心](../Page/炮台里.md "wikilink")，稱為「政府山」。

### 遊行集會

[Hong_Kong_1_July_march.jpg](https://zh.wikipedia.org/wiki/File:Hong_Kong_1_July_march.jpg "fig:Hong_Kong_1_July_march.jpg")中，香港市民沿炮台里進入西閘，到達中區政府合署。這是最後一次遊行以中區政府合署作為終點。\]\]
在2011年前，中區政府合署因作為政府總部，成為香港遊行、集會的主要場地。

1997年[香港回歸後](../Page/香港回歸.md "wikilink")，政府當局在1997年11月以保安理由，在中區政府合署周圍築起高鐵欄，由保安員把守，而市民從此不能任意從[炮台里或](../Page/炮台里.md "wikilink")[下亞厘畢道進出中區政府合署](../Page/下亞厘畢道.md "wikilink")\[1\]\[2\]。

1999年12月3日，約2,000名聲稱自己及子女擁有居港權的示威者，因不滿[香港終審法院宣判結果](../Page/香港終審法院.md "wikilink")，在中區政府合署正門外聚集，兩度與警方發生衝突﹐期間有人投擲花盆﹑石塊及擴音器等雜物，警方出動胡椒噴霧鎮壓，最後需由手持盾牌、全副武裝的機動部隊清場，亦是政府總部首次進行清場行動\[3\]。

因應遊行集會數字上升，政府在2002年起劃定西閘作為示威區，而中座正門外的示威區則在周二早上開放，供示威者向行政會議請願，但在工作日在中座及西座之間進行集會申請，一般不獲批准；周日及公眾假期在中區政府合署範圍內進行集會請願，則由[行政署個別審批](../Page/行政署.md "wikilink")，集會人數逾50人或遊行人數逾30人，更需通知警方\[4\]，而當局規定中區政府合署範圍內最多只能同時容納3000人\[5\]。

[2003年香港七一遊行](../Page/2003年香港七一遊行.md "wikilink")，逾50萬名市民上街遊行，以中區政府合署為遊行終點，當年因人潮太多主辦單位在遊行尾聲改以雪廠街為終點，其後七一遊行均以中區政府合署為終點，直至2011年。

2011年，隨着香港政府總部遷往[金鐘](../Page/金鐘.md "wikilink")[添馬艦](../Page/添馬艦_\(香港\).md "wikilink")，主辦遊行的[民間人權陣線在](../Page/民間人權陣線.md "wikilink")2012年，改以[新政府總部東翼廣場為終點](../Page/香港特別行政區政府總部_\(建築物\).md "wikilink")。其後遊行的終點站多次轉變，如[中環](../Page/中環.md "wikilink")[遮打道](../Page/遮打道.md "wikilink")、[金鐘](../Page/金鐘.md "wikilink")[添馬公園及](../Page/添馬公園.md "wikilink")[添美道等](../Page/添美道.md "wikilink")。

## 範圍

政府山具體範圍東至[花園道](../Page/花園道.md "wikilink")，南至[上亞厘畢道](../Page/上亞厘畢道.md "wikilink")，西至[己連拿利](../Page/己連拿利.md "wikilink")，北至[皇后大道中](../Page/皇后大道.md "wikilink")。在這個範圍的政府建築計有[中區政府合署](../Page/中區政府合署.md "wikilink")（舊[政府總部](../Page/香港特別行政區政府總部.md "wikilink")）、[禮賓府](../Page/禮賓府.md "wikilink")（[香港行政長官官邸](../Page/香港行政長官.md "wikilink")）、[前法國外方傳道會大樓](../Page/前法國外方傳道會大樓.md "wikilink")（前[香港終審法院](../Page/香港終審法院.md "wikilink")）等等。非政府建築則主要包括[香港聖公會](../Page/香港聖公會.md "wikilink")[聖約翰座堂](../Page/聖約翰座堂.md "wikilink")、[會督府及](../Page/會督府.md "wikilink")[香港動植物公園](../Page/香港動植物公園.md "wikilink")。

## 保育政府山

## 参考文献

## 外部链接

  - [政府山關注組](http://notforsalehk.wordpress.com)
  - [不要地產山 : 人民規劃政府山](http://www.facebook.com/cgonotforsale)
  - [保育政府山](https://web.archive.org/web/20090424215123/http://www.cahk.org.hk/articles/docs/20060116.htm)
  - [政府山不是一個天價](http://appledaily.atnext.com/template/apple/art_main.cfm?iss_id=20060920&sec_id=4104&subsec_id=15337&art_id=6329413)
  - [「全面保護『政府山』」動議修正案辯論](http://www.alanleong.net/public/contents/article?revision_id=6809&item_id=6808)
  - [民主黨就「全面保護『政府山』」議案之發言稿](http://www.dphk.org/2003/research/research.asp?iCommentId=2921&szColumnId=researchland)
  - [保護政府山有何意義](http://paper.wenweipo.com/2006/07/10/WW0607100006.htm)
  - [決定政府山用途言之尚早](http://www3.news.gov.hk/ISD/ebulletin/tc/Category/ontherecord/060706/html/060706tc11002.htm)

[Category:香港山峰](../Category/香港山峰.md "wikilink")
[Category:中環](../Category/中環.md "wikilink")

1.  [新聞公報](http://www.info.gov.hk/gia/general/dib/c1127.htm)，香港政府新聞公報：新聞資料庫，1997年11月27日(星期四)
2.
3.  「警胡椒噴霧驅請願者」《明報》A02，1999年12月4日
4.  [立法會四題：中區政府合署外的請願活動](http://www.info.gov.hk/gia/general/200211/27/1127194.htm)，香港特區政府新聞公報，2002年11月27日
5.  [警方協助所有合法及和平的公眾集會及遊行](http://www.info.gov.hk/gia/general/200406/14/0614224.htm)，香港特區政府新聞公報，2004年6月14日