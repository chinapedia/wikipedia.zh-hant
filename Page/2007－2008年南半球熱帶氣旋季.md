<table>
<thead>
<tr class="header">
<th><p><big>西南印度洋</big></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>名稱</p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td><hr></td>
</tr>
<tr class="even">
<td><p><strong>熱帶擾動</strong></p></td>
</tr>
<tr class="odd">
<td><p><strong>熱帶低氣壓</strong></p></td>
</tr>
<tr class="even">
<td><p><strong>中級熱帶風暴</strong></p></td>
</tr>
<tr class="odd">
<td><p><strong>強烈熱帶風暴</strong></p></td>
</tr>
<tr class="even">
<td><p><strong>熱帶氣旋</strong></p></td>
</tr>
<tr class="odd">
<td><p><strong>強烈熱帶氣旋</strong></p></td>
</tr>
<tr class="even">
<td><p><strong>非常強烈熱帶氣旋</strong></p></td>
</tr>
</tbody>
</table>

<table>
<thead>
<tr class="header">
<th><p><big>澳洲</big></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>名稱</p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td><hr></td>
</tr>
<tr class="even">
<td><p><strong>熱帶低氣壓</strong></p></td>
</tr>
<tr class="odd">
<td><p><strong>1級 熱帶氣旋</strong></p></td>
</tr>
<tr class="even">
<td><p><strong>2級 熱帶氣旋</strong></p></td>
</tr>
<tr class="odd">
<td><p><strong>3級 強烈熱帶氣旋</strong></p></td>
</tr>
<tr class="even">
<td><p><strong>4級 強烈熱帶氣旋</strong></p></td>
</tr>
<tr class="odd">
<td><p><strong>5級 強烈熱帶氣旋</strong></p></td>
</tr>
</tbody>
</table>

<table>
<thead>
<tr class="header">
<th><p><big>南太平洋</big></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>名稱</p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td><hr></td>
</tr>
<tr class="even">
<td><p><strong>熱帶擾動</strong></p></td>
</tr>
<tr class="odd">
<td><p><strong>熱帶低氣壓</strong></p></td>
</tr>
<tr class="even">
<td><p><strong>1級 熱帶氣旋</strong></p></td>
</tr>
<tr class="odd">
<td><p><strong>2級 熱帶氣旋</strong></p></td>
</tr>
<tr class="even">
<td><p><strong>3級 強烈熱帶氣旋</strong></p></td>
</tr>
<tr class="odd">
<td><p><strong>4級 強烈熱帶氣旋</strong></p></td>
</tr>
<tr class="even">
<td><p><strong>5級 強烈熱帶氣旋</strong></p></td>
</tr>
</tbody>
</table>

**2007－2008年南半球熱帶氣旋季**在2007年7月1日開始，並會在2008年6月30日完結。

## 簡介

此文內容只包含在南半球形成的熱帶氣旋的介紹。三個主要熱帶氣旋區是西南印度洋、澳洲和南太平洋。很少熱帶氣旋於南大西洋形成，到現在為止只有三個。

風暴是由[馬達加斯加](../Page/馬達加斯加.md "wikilink")、[毛里求斯](../Page/毛里求斯.md "wikilink")、[澳洲](../Page/澳洲.md "wikilink")、[巴布亞紐幾內亞和](../Page/巴布亞紐幾內亞.md "wikilink")[斐濟命名的](../Page/斐濟.md "wikilink")。

在南印度洋的熱帶低氣壓會被編號為xxS，在南太平洋的就會被編號為xxP，在[斐濟警告範圍的就會被編號為xxF](../Page/斐濟.md "wikilink")，在[澳洲警告範圍的就會被編號為xxU](../Page/澳洲.md "wikilink")，在[法國警告範圍的就會被編號為xxR](../Page/法國.md "wikilink")。

## 西南印度洋

如果一個熱帶低氣壓增強為中級熱帶風暴的話，[馬達加斯加或](../Page/馬達加斯加.md "wikilink")[毛里求斯就會為它命名](../Page/毛里求斯.md "wikilink")。

## 澳洲

如果一個熱帶低氣壓增強為熱帶氣旋的話，[澳洲或](../Page/澳洲.md "wikilink")[巴布亞紐幾內亞就會為它命名](../Page/巴布亞紐幾內亞.md "wikilink")。

## 南太平洋

如果一個熱帶低氣壓增強為熱帶氣旋的話，[斐濟就會為它命名](../Page/斐濟.md "wikilink")。

## 風暴時間表

## 內部連結

  - [2007年太平洋颱風季](../Page/2007年太平洋颱風季.md "wikilink")
  - [2007年太平洋颶風季](../Page/2007年太平洋颶風季.md "wikilink")
  - [2007年大西洋颶風季](../Page/2007年大西洋颶風季.md "wikilink")
  - [2007年北印度洋氣旋季](../Page/2007年北印度洋氣旋季.md "wikilink")
  - [2008年太平洋颱風季](../Page/2008年太平洋颱風季.md "wikilink")
  - [2008年太平洋颶風季](../Page/2008年太平洋颶風季.md "wikilink")
  - [2008年大西洋颶風季](../Page/2008年大西洋颶風季.md "wikilink")
  - [2008年北印度洋氣旋季](../Page/2008年北印度洋氣旋季.md "wikilink")

[Category:南半球熱帶氣旋季](../Category/南半球熱帶氣旋季.md "wikilink")
[Category:2007－2008年南半球熱帶氣旋季](../Category/2007－2008年南半球熱帶氣旋季.md "wikilink")