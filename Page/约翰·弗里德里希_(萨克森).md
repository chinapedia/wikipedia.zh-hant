[Lucas_Cranach_d._Ä._044.jpg](https://zh.wikipedia.org/wiki/File:Lucas_Cranach_d._Ä._044.jpg "fig:Lucas_Cranach_d._Ä._044.jpg")
（宽宏的）**约翰·腓特烈**（，），[德意志的](../Page/德意志.md "wikilink")[萨克森选侯](../Page/萨克森.md "wikilink")（1532年－1547年在位）。他是[神圣罗马帝国皇帝](../Page/神圣罗马帝国.md "wikilink")[查理五世统治时期的一位著名的](../Page/查理五世_\(神圣罗马帝国\).md "wikilink")[新教](../Page/新教.md "wikilink")[王公](../Page/王公.md "wikilink")。

约翰·腓特烈属于长期统治萨克森的[韦廷家族](../Page/韦廷家族.md "wikilink")，是选侯[坚定的约翰之子](../Page/约翰_\(萨克森选侯\).md "wikilink")，生于[托尔高](../Page/托尔高.md "wikilink")。1526年，他与[克莱沃公爵](../Page/克莱沃.md "wikilink")[约翰三世之女西比尔结婚](../Page/约翰三世_\(克莱沃\).md "wikilink")。

约翰·腓特烈支持由[马丁·路德发起的](../Page/马丁·路德.md "wikilink")[宗教改革](../Page/德国宗教改革.md "wikilink")，是新教信仰的保护者。在德意志新教诸侯为抵抗查理五世而组成[施马尔卡尔登联盟之后](../Page/施马尔卡尔登联盟.md "wikilink")，约翰·腓特烈成为该联盟的领袖之一。1546年，他被查理五世下令废黜并逐出帝国。约翰·腓特烈成功地抵御了由忠于皇帝的萨克森公爵[莫里茨](../Page/莫里茨_\(萨克森\).md "wikilink")（亦为维丁家族成员）率领的军队对其领地的进攻。但由于施马尔卡尔登联盟在1547年[米尔堡战役中被查理五世彻底打败](../Page/米尔堡战役.md "wikilink")，他终于落到皇帝手里。

约翰·腓特烈选侯被查理五世判处[死刑](../Page/死刑.md "wikilink")。鉴于其妻子西比尔仍然控制着[符腾堡](../Page/符腾堡.md "wikilink")，查理五世没有立刻执行死刑，而是开始进行谈判。在同意将领地和[选侯头衔都被转交给皇帝宠信的萨克森公爵莫里茨后](../Page/选侯.md "wikilink")，约翰·腓特烈被改判为终身监禁。

1552年，莫里茨出乎意料的背叛了皇帝，并向皇帝发起进攻。约翰·腓特烈于是被查理五世释放，并得到了[魏玛作为领地](../Page/魏玛.md "wikilink")。1554年他在魏玛去世。

## 参考文献

  - [Grand Rapids: Christian Classics Ethereal Library, 2000-01-27,
    v0.1](https://web.archive.org/web/20060320235809/http://www.ccel.org/ccel/schaff/encyc06.htm)
  - A. Beck, Johann Friedrich der Mittlere, 2 vols., Weimar, 1858
  - F. von Bezold, Geschichte der deutschen Reformation. Berlin, 1886
  - [Biography on WHKLMA
    site](http://www.zum.de/whkmla/period/reformation/bioxrefrulers.html#JFrederick)

[F](../Category/萨克森选侯.md "wikilink")
[Category:韦廷王朝](../Category/韦廷王朝.md "wikilink")