**萨摩亚群岛**（，舊稱）位於[南太平洋](../Page/南太平洋.md "wikilink")，是[波里尼西亞的一部分](../Page/波里尼西亞.md "wikilink")。约当南纬13°-15°、西经168°-173°之间，人口约250,000，主要是波里尼西亞人，通用[萨摩亚语](../Page/萨摩亚语.md "wikilink")。

政治上分為两个[政治实体](../Page/政治实体.md "wikilink")：

  - ，也称[西萨摩亚](../Page/西萨摩亚.md "wikilink")（2,831 km²，185,000人）

  - ，也叫[东萨摩亚](../Page/东萨摩亚.md "wikilink")（199km²，65,000人）

2009年9月29日，薩摩亞群島暴發生8級地震\[1\]並引發海嘯，造成超過118人死亡。

## 参考文獻

<div class="references-small">

<references />

</div>

[Category:玻里尼西亞](../Category/玻里尼西亞.md "wikilink")
[薩摩亞群島](../Category/薩摩亞群島.md "wikilink")
[Category:大洋洲地理](../Category/大洋洲地理.md "wikilink")
[Category:薩摩亞地理](../Category/薩摩亞地理.md "wikilink")
[Category:美屬薩摩亞地理](../Category/美屬薩摩亞地理.md "wikilink")
[Category:分裂地區](../Category/分裂地區.md "wikilink")

1.  美國地質調查局報告的震級是8.0級，太平洋海嘯警報中心報告的震級是8.3級。[巨浪六米高 百馀人喪生 地震掀海嘯
    薩摩亞群島變地獄](http://dailynews.sina.com/bg/news/int/singtao/20090930/0513716939.html)