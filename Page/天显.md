**天显**（926年二月－938年）是辽太祖[耶律阿保机](../Page/耶律阿保机.md "wikilink")、[辽太宗耶律德光的](../Page/辽太宗.md "wikilink")[年号](../Page/年号.md "wikilink")。[辽国使用该年号共](../Page/辽国.md "wikilink")13年。

天显元年七月阿保机逝世，皇后摄政。至天显二年十一月[辽太宗即位沿用](../Page/辽太宗.md "wikilink")\[1\]。

## 纪年

| 天显                               | 元年                             | 二年                             | 三年                             | 四年                             | 五年                             | 六年                             | 七年                             | 八年                             | 九年                             | 十年                             | 十一年                            | 十二年                            | 十三年                            |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 926年                           | 927年                           | 928年                           | 929年                           | 930年                           | 931年                           | 932年                           | 933年                           | 934年                           | 935年                           | 936年                           | 937年                           | 938年                           |
| [干支](../Page/干支纪年.md "wikilink") | [丙戌](../Page/丙戌.md "wikilink") | [丁亥](../Page/丁亥.md "wikilink") | [戊子](../Page/戊子.md "wikilink") | [己丑](../Page/己丑.md "wikilink") | [庚寅](../Page/庚寅.md "wikilink") | [辛卯](../Page/辛卯.md "wikilink") | [壬辰](../Page/壬辰.md "wikilink") | [癸巳](../Page/癸巳.md "wikilink") | [甲午](../Page/甲午.md "wikilink") | [乙未](../Page/乙未.md "wikilink") | [丙申](../Page/丙申.md "wikilink") | [丁酉](../Page/丁酉.md "wikilink") | [戊戌](../Page/戊戌.md "wikilink") |

## 参見

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 同期存在的其他政权年号
      - [甘露](../Page/甘露_\(耶律倍\).md "wikilink")（926年至936年）：[東丹](../Page/東丹.md "wikilink")—[耶律倍之年號](../Page/耶律倍.md "wikilink")
      - [同光](../Page/同光.md "wikilink")（923年四月至926年四月）：[後唐](../Page/後唐.md "wikilink")—[李存勗之年號](../Page/李存勗.md "wikilink")
      - [天成](../Page/天成_\(李嗣源\).md "wikilink")（926年四月至930年二月）：後唐—[李嗣源之年號](../Page/李嗣源.md "wikilink")
      - [長興](../Page/長興_\(李嗣源\).md "wikilink")（930年二月至933年十二月）：後唐—李嗣源之年號
      - [應順](../Page/應順.md "wikilink")（934年正月至四月）：後唐—[李從厚之年號](../Page/李從厚.md "wikilink")
      - [清泰](../Page/清泰.md "wikilink")（934年四月至936閏十一月）：後唐—[李從珂之年號](../Page/李從珂.md "wikilink")
      - [天福](../Page/天福_\(石敬瑭\).md "wikilink")（936年十一月至944年六月）：[後晉](../Page/後晉.md "wikilink")—[石敬瑭之年號](../Page/石敬瑭.md "wikilink")
      - [順義](../Page/順義_\(楊溥\).md "wikilink")（921年二月至927年十月）：吳—[楊溥之年號](../Page/楊溥_\(吳\).md "wikilink")
      - [乾貞](../Page/乾貞_\(楊溥\).md "wikilink")（927年十一月至929年十月）：吳—楊溥之年號
      - [大和](../Page/大和_\(楊溥\).md "wikilink")（929年十月至935年八月）：吳—楊溥之年號
      - [天祚](../Page/天祚.md "wikilink")（935年九月至937年十月）：吳—楊溥之年號
      - [昇元](../Page/昇元.md "wikilink")（937年十月至943年二月）：[南唐](../Page/南唐.md "wikilink")—[李昪之年號](../Page/李昪.md "wikilink")
      - [宝正](../Page/宝正.md "wikilink")（926年至931年）：吳越—錢鏐之年號
      - [白龍](../Page/白龍.md "wikilink")（925年十二月至928年二月）：南漢—劉龑之年號
      - [大有](../Page/大有_\(年號\).md "wikilink")（928年三月至942年三月）：南漢—劉龑之年號
      - [龍啟](../Page/龍啟.md "wikilink")（933年至934年）：[閩](../Page/闽_\(十国\).md "wikilink")—[王延鈞之年號](../Page/王延鈞.md "wikilink")
      - [永和](../Page/永和_\(王延鈞\).md "wikilink")（935年正月至936年二月）：閩—王延鈞之年號
      - [通文](../Page/通文.md "wikilink")（936年三月至939年七月）：閩—[王繼鵬之年號](../Page/王繼鵬.md "wikilink")
      - [明德](../Page/明德_\(孟知祥\).md "wikilink")（934年四月至937年）：[後蜀](../Page/後蜀.md "wikilink")—[孟知祥之年號](../Page/孟知祥.md "wikilink")
      - [廣政](../Page/廣政_\(孟昶\).md "wikilink")（938年正月至965年正月）：後蜀—[孟昶之年號](../Page/孟昶.md "wikilink")
      - [天應](../Page/天應_\(鄭隆亶\).md "wikilink")（927年）：[大長和](../Page/大長和.md "wikilink")—[鄭隆亶之年號](../Page/鄭隆亶.md "wikilink")
      - [尊聖](../Page/尊聖.md "wikilink")（928年至929年）：[大天興](../Page/大天興.md "wikilink")—[趙善政之年號](../Page/趙善政.md "wikilink")
      - [大明](../Page/大明_\(楊干真\).md "wikilink")（931年至937年）：[大義寧](../Page/大義寧.md "wikilink")—[楊干真之年號](../Page/楊干真.md "wikilink")
      - [鼎新](../Page/鼎新.md "wikilink")：大義寧—楊干真之年號
      - [光聖](../Page/光聖.md "wikilink")：大義寧—楊干真之年號
      - [文德](../Page/文德_\(段思平\).md "wikilink")（938年起）：[大理國](../Page/大理國.md "wikilink")—[段思平之年號](../Page/段思平.md "wikilink")
      - [同慶](../Page/同慶_\(尉遲烏僧波\).md "wikilink")（912年至966年）：[-{于}-闐](../Page/于闐.md "wikilink")—[尉遲烏僧波之年號](../Page/尉遲烏僧波.md "wikilink")
      - [天授](../Page/天授_\(高麗太祖\).md "wikilink")（918年至933年）：[高麗太祖](../Page/高麗.md "wikilink")[王建之年號](../Page/高麗太祖.md "wikilink")
      - [延長](../Page/延長_\(醍醐天皇\).md "wikilink")（923年閏四月十一日至931年四月二十六日）：日本[醍醐天皇](../Page/醍醐天皇.md "wikilink")、[朱雀天皇年号](../Page/朱雀天皇.md "wikilink")
      - [承平](../Page/承平_\(日本\).md "wikilink")（931年四月二十六日至938年五月二十二日）：日本朱雀天皇年号
      - [天慶](../Page/天慶_\(朱雀天皇\).md "wikilink")（938年五月二十二日至947年四月二十二日）：日本朱雀天皇年号

## 参考資料

<div class="references-small">

<references />

</div>

[Category:辽国年号](../Category/辽国年号.md "wikilink")
[Category:10世纪中国年号](../Category/10世纪中国年号.md "wikilink")
[Category:920年代中国政治](../Category/920年代中国政治.md "wikilink")
[Category:930年代中国政治](../Category/930年代中国政治.md "wikilink")

1.