為日本厂商[特庫摩开发的恐怖游戏系列](../Page/特庫摩.md "wikilink")，自[2001年問世以来](../Page/2001年.md "wikilink")，已推出了五部正傳作品：《[零～zero～](../Page/零～zero～.md "wikilink")》、《[零～紅蝶～](../Page/零～紅蝶～.md "wikilink")》、《[零～刺青之聲～](../Page/零～刺青之聲～.md "wikilink")》、《[零～月蝕的假面～](../Page/零～月蝕的假面～.md "wikilink")》和《[零～濡鴉之巫女～](../Page/零～濡鴉之巫女～.md "wikilink")》，一部外傳作品《心靈照相機～被附身的筆記本～》，以及一部重製作品《零～真紅之蝶～》；游戏平台則橫跨了[PS2](../Page/PS2.md "wikilink")、[Xbox](../Page/Xbox.md "wikilink")、[Wii](../Page/Wii.md "wikilink")、[3DS和](../Page/3DS.md "wikilink")[Wii
U](../Page/Wii_U.md "wikilink")。

[2012年](../Page/2012年.md "wikilink")，零系列的版權商標正式公布由[任天堂與](../Page/任天堂.md "wikilink")[光榮特庫摩共同所持有](../Page/光榮特庫摩.md "wikilink")，其中任天堂佔有相當重要的份額。

## 游戏

### 零～zero～

[PS2](../Page/PS2.md "wikilink")为系列的第一作，[2001年首发于日本](../Page/2001年.md "wikilink")[PlayStation
2](../Page/PlayStation_2.md "wikilink")\[1\]。

[2002年发行的](../Page/2002年.md "wikilink")[Xbox版在日本称为](../Page/Xbox.md "wikilink")“Fatal
Frame -零 Special Edition-”，加入了新结局，更多服装，新的游戏模式和更困难的设置。

### 零～紅蝶～

[PS2](../Page/PS2.md "wikilink")是系列的第二作，[2003年於日本PlayStation](../Page/2003年.md "wikilink")
2平台首发。

翌年，在[Xbox发行的](../Page/Xbox.md "wikilink")“导演剪辑版”收录了一些新增要素。游戏被广泛认为是史上最恐怖的电子游戏之一\[2\]\[3\]。

在[北美](../Page/北美.md "wikilink")，[2013年](../Page/2013年.md "wikilink")[5月7日](../Page/5月7日.md "wikilink")
发行了可供[PlayStation 3下载的PlayStation](../Page/PlayStation_3.md "wikilink")
2版。PlayStation 3版在发行后不久因为各种模拟技术问题移除了在线商店\[4\]，直到 6月30日 修复并重新发行\[5\]。

### 零～刺青之聲～

[PS2](../Page/PS2.md "wikilink")是系列的第三部作品\[6\]，[2005年在](../Page/2005年.md "wikilink")[PlayStation
2平台发行](../Page/PlayStation_2.md "wikilink")。

游戏同时使用了旧角色和新角色，并使用了新闹鬼地点\[7\]\[8\]。

### 零～月蝕的假面～

[Wii](../Page/Wii.md "wikilink")是系列的第四作，[2008年在日本發售](../Page/2008年.md "wikilink")。本作是特库摩和[任天堂的合作项目](../Page/任天堂.md "wikilink")，特库摩负责游戏的开发制作，此外[Grasshopper
Manufacture也有参与开发工作](../Page/Grasshopper_Manufacture.md "wikilink")；而任天堂则担当游戏的监修和发行。

本作成为零系列中第一部[Wii主机独占的游戏](../Page/Wii.md "wikilink")；同时据任天堂的消息，本作也有别于前三作而不会在北美地区发行\[9\]。

### 心靈照相機～被附身的筆記本～

[3DS](../Page/3DS.md "wikilink")，是本系列的第一款外傳作品，[2012年](../Page/2012年.md "wikilink")1月在日本發行，遊戲中收錄「幽靈相機（ゴーストカメラ）」模式，將可利用[3DS攝影鏡頭模擬照相機來尋找和除靈](../Page/3DS.md "wikilink")。

而「AR日記（ARノート）」，可以體驗專屬故事，玩家要與被帶離現實世界的少女「真夜」一起探索，揭開被詛咒的日記背後的真相。此外遊戲中還收錄「驚悚日記（ホラーノート）」模式，讓玩家利用3DS
AR功能體驗許多不同的小遊戲。

### 零～真紅之蝶～

[Wii](../Page/Wii.md "wikilink")為《[零～紅蝶～](../Page/零～紅蝶～.md "wikilink")》的重製版本，[2012年在日本](../Page/2012年.md "wikilink")、澳洲和歐洲发行\[10\]\[11\]。

為配合Wii的主機特性，本作翻新人物模組、重製過場動畫和新增體感操作系統，並加入可供雙人遊玩的『鬼屋模式（お化け屋敷）』。

### 零～濡鴉之巫女～

[Wii
U](../Page/Wii_U.md "wikilink")由[光荣特库摩与](../Page/光荣特库摩.md "wikilink")[任天堂联合制作并由任天堂发行在](../Page/任天堂.md "wikilink")
Wii U 平台上，為零系列首次[高畫質化之作品](../Page/高畫質.md "wikilink")。

由于 Wii U GamePad 平板控制器的加入，使得系列游戏特色之一的“射影機”使用起来更加得心应手。

游戏[2014年](../Page/2014年.md "wikilink")9月27日在日本发售。

## 跨媒体

### 电影

《零》於[2014年推出同名电影](../Page/2014年.md "wikilink")，由安里麻里导演，大塚英志编剧，[森川葵](../Page/森川葵.md "wikilink")
和 [中條彩未](../Page/中條彩未.md "wikilink") 領銜主演。

电影与《濡鸦之巫女》交叉宣传。\[12\]

### 漫畫

《零～影巫女～》，由天樹征丸原作，HAKUS作畫，[2014年](../Page/2014年.md "wikilink")7月17日在[DeNA](../Page/DeNA.md "wikilink")
的電子漫畫平台「MangaBox」上開始連載，9月17日由講談社出版單行本。

## 设定

  - 射影機 :
    由日本民俗學家麻生邦彥博士研製，能夠拍攝到“看不見的事物”的特殊照相機。零系列最關鍵物品，在零系列中有數台不同型號的射影機存在，根據遊戲中獲得的麻生博士本人遺留的筆記來看，除了《零～zero～》中使用的以外，其餘均爲“試作品”。
    靈石收音機 :
    以用礦石做為部份迴路來收訊的礦石收音機改良而成，據說可以收到靈界的聲音，靈界的聲音可以藉由收音機而被聽見，與人能夠感受某些氣息產生預感、雙胞胎之間的共鳴有著類似的特性。
    膠卷（菲林） :
    分〇七式、一四式（十四式）、三七式（《零～zero～》特有，相當於六一式）、六一式、七四式（《零～zero～》特有，相當於九〇式）、九〇式和零式。每種膠卷皆有不同的攻擊力和填充時間，是射影機能攻擊怨靈的原因。
    鏡石 :
    數量稀少，主角身上一般只能裝備一塊。當怨靈攻擊主角，导致体力降到0时，鏡石會自动地完全回復體力，但受到的伤害仍会保留。例如，遇到超強怨靈（通常在惡夢难度下）的攻击時，可能會先爆鏡石再掉半條血。
    万葉丸 : 遊戲中的補血物品，能補充少量體力，可同時持有多個。
    御神水 : 遊戲中的補血物品，數量稀少，能完全補充體力，和鏡石一樣重要，可同時持有多個。
    Zero Shot : 靈力圈全蓄力狀態與靈力圈呈紅色狀態下拍照。（包含Fatal Frame）
    靈石 :
    《零～zero～》中用于使用照相機的強化功能（如：壓、遲、視、痺、探等）的物品，每使用一次要消耗一顆。在其后作中变为“靈子”，通过对怨灵造成伤害获得，直接填充在射影機左下角。不同的強化鏡頭消耗的靈子數也不同，例如“擊”消耗全靈力（依當時所剩靈子決定攻擊力）、“零”消耗3顆、“連”消耗1顆等。

## 音乐

系列从《零～紅蝶～》开始到《零～真紅之蝶～》的主题曲皆由[天野月](../Page/天野月.md "wikilink")（原天野月子）演唱，包括：《零～紅蝶～》主题曲《蝶》、《零～刺青之聲～》主题曲《聲》、《零～月蝕的假面～》两个难度版本的主题曲《NOiSE》和《零的調律》，以及《零～真紅之蝶～》主题曲《くれなゐ》。而《零～濡鴉之巫女～》的主题曲《HIGANBANA》改由AnJu演唱，另外在達成特殊條件之下（夕莉身穿白無垢），片尾曲將為[天野月所演唱的](../Page/天野月.md "wikilink")《鳥籠
-in this cage-》。

## 参考文献

## 外部連結

  -

  -

  -

  -

  -

  -

  -

  -

[零系列](../Category/零系列.md "wikilink")
[Category:電子遊戲系列](../Category/電子遊戲系列.md "wikilink")
[Category:特库摩电子游戏系列](../Category/特库摩电子游戏系列.md "wikilink")
[Category:任天堂电子游戏系列](../Category/任天堂电子游戏系列.md "wikilink")
[Category:2001年首发的电子游戏系列](../Category/2001年首发的电子游戏系列.md "wikilink")

1.
2.
3.
4.
5.  <http://www.destructoid.com/sony-takes-fatal-frame-2-in-for-repairs-this-week-on-psn-258841.phtml>
6.
7.
8.
9.  [Fatal Frame Never Coming to
    America?](http://wii.ign.com/articles/970/970875p1.html)
10.
11.
12. [真人版電影《零～ZERO～》公開最新劇照及上映日期](http://home.gamer.com.tw/creationDetail.php?sn=2473443).巴哈姆特.2014-06-12.\[2014-10-01\].