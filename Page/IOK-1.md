{{ Galaxy | | image = | name = IOK-1 | epoch =
[J2000](../Page/J2000.md "wikilink") | type = | ra =  | dec =  |
dist_ly = | z = 6.96 | appmag_v = | size_v = | constellation name =
[后髮座](../Page/后髮座.md "wikilink") | notes = | names = }}

**IOK-1**是已知最古老並且距離最遙遠的[星系之一](../Page/星系.md "wikilink")，距離我們有128.8億光年遠。它在2006年4月被[家正則使用](../Page/家正則.md "wikilink")[日本國家天文臺在](../Page/日本國家天文臺.md "wikilink")[夏威夷的](../Page/夏威夷.md "wikilink")[昴星團望遠鏡所發現](../Page/昴星團望遠鏡.md "wikilink")。它發射出的[萊曼α譜線](../Page/來曼系.md "wikilink")[紅移高達](../Page/紅移.md "wikilink")6.96，相當於在[大爆炸之後](../Page/大爆炸.md "wikilink")7億5000萬年的時間。雖然有些科學家宣稱有更古老的天體（例如[阿貝爾1835
IR1916星系](../Page/Galaxy_Abell_1835_IR1916.md "wikilink")）存在，然而目前對IOK-1其年齡及構成的所知還是比較可信的。\[1\]
\[2\] \[3\]

"IOK"的字源來自三位觀測者Iye（家正則）、Ota（太田）、和Kashikawa（柏川）的英文名字的第一個字母組合。

## 相關條目

  - [A1689-zD1](../Page/A1689-zD1.md "wikilink")
  - [UDFy-38135539](../Page/UDFy-38135539.md "wikilink")

## 參考資料

<div class="references-small">

<references/>

</div>

[Category:遥远星系](../Category/遥远星系.md "wikilink")
[Category:后发座](../Category/后发座.md "wikilink")

1.  McMahon, R. (2006). *Journey to the birth of the Universe*.
    *Nature*: vol. 443, issue of September 14, 2006.
2.
3.  [Press
    release](http://www.subarutelescope.org/Pressrelease/2006/09/13/index.html),
    National Astronomical Observatory of Japan, [September
    13](../Page/September_13.md "wikilink"), 2006