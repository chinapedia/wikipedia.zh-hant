[Kossel,_Albrecht_(1853-1927).jpg](https://zh.wikipedia.org/wiki/File:Kossel,_Albrecht_\(1853-1927\).jpg "fig:Kossel,_Albrecht_(1853-1927).jpg")
**阿尔布雷希特·科塞尔**（，），生於[羅斯托克](../Page/羅斯托克.md "wikilink")，[德國](../Page/德國.md "wikilink")[醫生](../Page/醫生.md "wikilink")。

科塞爾的工作範疇是[生理化學](../Page/生理學.md "wikilink")，特別是組織和細胞的化學。

他因[細胞化學的研究](../Page/細胞化學.md "wikilink")（特別是[蛋白質和](../Page/蛋白質.md "wikilink")[核酸](../Page/核酸.md "wikilink")）获得1910年的[诺贝尔生理学或医学奖](../Page/诺贝尔生理学或医学奖.md "wikilink")。

## 部分作品

  - *Untersuchungen uber die Nukleine und ihre Spaltungsprodukte* , 1881
  - *Die Gewebe des menschlichen Korpers und ihre mikroskopische
    Untersuchung* (人體內的組織和其顯微下的觀察), 1889-1891
  - *Leitfaden fur medizinisch-chemische Kurse* (醫學化學課程教科書), 1888
  - *Die Probleme der Biochemie* (生物化學之難題), 1908
  - *Die Beziehungen der Chemie zur Physiologie* (化學和生理學之關係), 1913

[Category:德國醫學家](../Category/德國醫學家.md "wikilink")
[Category:德國生理學家](../Category/德國生理學家.md "wikilink")
[Category:德國生物學家](../Category/德國生物學家.md "wikilink")
[Category:德國化學家](../Category/德國化學家.md "wikilink")
[Category:诺贝尔生理学或医学奖获得者](../Category/诺贝尔生理学或医学奖获得者.md "wikilink")
[Category:德國諾貝爾獎獲得者](../Category/德國諾貝爾獎獲得者.md "wikilink")
[Category:馬爾堡大學教師](../Category/馬爾堡大學教師.md "wikilink")
[Category:海德堡大學教師](../Category/海德堡大學教師.md "wikilink")
[Category:斯特拉斯堡大學校友](../Category/斯特拉斯堡大學校友.md "wikilink")
[Category:羅斯托克大學校友](../Category/羅斯托克大學校友.md "wikilink")
[Category:梅克倫堡-前波門人](../Category/梅克倫堡-前波門人.md "wikilink")
[Category:丹麦皇家科学院院士](../Category/丹麦皇家科学院院士.md "wikilink")