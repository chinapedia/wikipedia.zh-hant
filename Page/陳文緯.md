**陳文緯**，號**紫垣**，中國[清朝官員](../Page/清朝.md "wikilink")，[甘肅](../Page/甘肅.md "wikilink")[皋蘭縣人](../Page/皋蘭縣.md "wikilink")，祖籍[浙江](../Page/浙江.md "wikilink")[山陰縣](../Page/山陰縣.md "wikilink")，[監生出身](../Page/監生.md "wikilink")。於光緒十八年七月十九日（1892年9月9日）任[恆春縣知縣](../Page/恆春縣.md "wikilink")，由代改署，光緒二十年三月（約1894年4月）題補。其任內主持了《[恆春縣志](../Page/恆春縣志.md "wikilink")》的修史工作，後來該書因[甲午戰爭等因素未能刊行](../Page/甲午戰爭.md "wikilink")，書稿被他攜回中國大陸。至於其他政績還有像於光緒二十年勸車城紳民公建三座田頭碉堡，駐有隘勇瞭望巡防以免遭到當地原住民攻擊。

## 參考文獻

[Category:清朝恆春縣知縣](../Category/清朝恆春縣知縣.md "wikilink")
[Category:甘肃人](../Category/甘肃人.md "wikilink")
[W](../Category/陈姓.md "wikilink")