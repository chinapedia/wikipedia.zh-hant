**Windows硬體工程研討會**（）是由[微軟公司舉辦的年度性研討會](../Page/微軟公司.md "wikilink")，研討內容主要是微軟[Windows作業系統相容](../Page/Windows作業系統.md "wikilink")[硬體的研發](../Page/硬體.md "wikilink")、設計技術，與和並称為微軟三大年度研討會。

## 活動

2006年5月23日－2006年5月25日，為期三天，在[美國](../Page/美國.md "wikilink")[華盛頓州](../Page/華盛頓州.md "wikilink")[西雅圖市的](../Page/西雅圖.md "wikilink")「華盛頓州立會議與貿易中心」（Washington
State Convention and Trade Center）舉行，與會人數約3,500人。

  - 微軟公司發表[Windows Vista](../Page/Windows_Vista.md "wikilink")、Windows
    Server "Longhorn"、[Office
    2007等軟體的外部測試第二版](../Page/Office_2007.md "wikilink")（beta
    2）

<!-- end list -->

  - [自由軟體基金會的人士至WinHEC研討會場外表達抗議](../Page/自由軟體基金會.md "wikilink")，来自[Defective
    by
    Design组织的抗議者身穿黃色的化學防護衣](../Page/Defective_by_Design.md "wikilink")、手持小冊子，解釋著[微軟軟體的開發撰寫手法](../Page/微軟.md "wikilink")、程序並不光明，他們对微軟軟體中的[數位版權管理技術严重剥夺用户自由而表示强烈抗议](../Page/數位版權管理.md "wikilink")。\[1\]

2015年3月18日至19日，WinHEC在中国深圳召开。\[2\]

## 參見

## 外部連結

  - [WinHEC官方網站](https://www.winhec.com)
  - [WinHEC 2006繼續期待Vista](https://web.archive.org/web/20060630111154/http://taiwan.cnet.com/computer/systems/features/0%2C2000068557%2C20107326%2C00.htm)

[Category:微軟](../Category/微軟.md "wikilink")

1.  引自[NewsForge](http://entertainment.newsforge.com/article.pl?sid=06/05/23/2219219&from=rss)
    於2006年5月28日的發佈（但事件發生則是在更早的2006年5月23日）的「**FSF人士於WinHEC
    2006會場外發起「反DRM」活動**」一文，作者為Bruce Byfield。
2.