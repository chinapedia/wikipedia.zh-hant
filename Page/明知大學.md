**明知大學**（），是[韓國一所綜合型私立大學](../Page/韓國.md "wikilink")，創立於1948年，有兩個校區（人文校區與自然校區），分別位於[首爾特別市](../Page/首爾特別市.md "wikilink")[西大門區](../Page/西大門區.md "wikilink")、[京畿道](../Page/京畿道.md "wikilink")[龍仁市](../Page/龍仁市.md "wikilink")；另外該校的足球校隊亦相當知名。

## 簡介

明知大學校訓為：愛、真理、奉仕，學生人數為12676人，大學院為（包括碩士、博士）2970人，教授群為382人。\[1\]

## 歷史

  - 1948年財團法人無窮學園設立首爾高等家庭學校正式開啟，校長金在俊就任
  - 1952年2月改名為[槿花女子初級大學](../Page/槿花女子初級大學.md "wikilink")，校長全浩徳就任
  - 1955年3月[槿花女子初級大學校名變更](../Page/槿花女子初級大學.md "wikilink")
  - 1956年大學廢止改由[首爾文理師範大學設立](../Page/首爾文理師範大學.md "wikilink")
  - 1962年2月改名為[文理實科大學](../Page/文理實科大學.md "wikilink")
  - 1963年9月改名為學校法人[明知初級大學](../Page/明知初級大學.md "wikilink")，同年10月升格為4年制明知大學
  - 1967年12月大學院設置
  - 1972年2月合併了學校法人[關東大學](../Page/關東大學.md "wikilink")
  - 1984年設置了情報産業研究所
  - 1987年新設了社會教育研究所
  - 2005年現在的學部總共有9個單科大學，11個系（32個學科），10個學部（19個専攻），4個學科，大學院有43個碩士課程和34個博士課程。

## 著名校友

  - [白一燮](../Page/白一燮.md "wikilink")
  - [延政勳](../Page/延政勳.md "wikilink")
  - [鄭允浩](../Page/鄭允浩.md "wikilink")（[東方神起](../Page/東方神起.md "wikilink")）
  - [金俊秀](../Page/金俊秀.md "wikilink")（[JYJ](../Page/JYJ.md "wikilink")）
  - [李晟敏](../Page/李晟敏.md "wikilink")（[Super
    Junior](../Page/Super_Junior.md "wikilink")）
  - [東海](../Page/李東海.md "wikilink")（[Super
    Junior](../Page/Super_Junior.md "wikilink")）
  - [鐘鉉](../Page/金鐘鉉.md "wikilink")（[SHINee](../Page/SHINee.md "wikilink")）
  - [KEY](../Page/金基範.md "wikilink")（[SHINee](../Page/SHINee.md "wikilink")）
  - [泰民](../Page/李泰民.md "wikilink")（[SHINee](../Page/SHINee.md "wikilink")）
  - [邊伯賢](../Page/邊伯賢.md "wikilink")（[EXO](../Page/EXO.md "wikilink")）
  - [金婑斌](../Page/金婑斌.md "wikilink")（前[Wonder
    Girls成員](../Page/Wonder_Girls.md "wikilink")，休學中）
  - [寶拉](../Page/尹寶拉.md "wikilink")（[Sistar](../Page/Sistar.md "wikilink")）
  - [東玄](../Page/Boyfriend.md "wikilink")（[Boyfriend](../Page/Boyfriend.md "wikilink")）
  - [朴智星](../Page/朴智星.md "wikilink")
    (前[英超球隊](../Page/英超.md "wikilink")[曼聯隊員及](../Page/曼聯.md "wikilink")[韓國國家足球隊隊長](../Page/韓國國家足球隊.md "wikilink"))
  - [燦多](../Page/B1A4.md "wikilink")（[B1A4](../Page/B1A4.md "wikilink")）
  - [朴寶劍](../Page/朴寶劍.md "wikilink")
  - [成旼宰](../Page/成旼宰.md "wikilink")（[SONAMOO](../Page/SONAMOO.md "wikilink")）
  - [尹智聖](../Page/尹智聖.md "wikilink")

## 參考來源

[Category:韓國私立大學](../Category/韓國私立大學.md "wikilink")
[Category:首爾特別市大學](../Category/首爾特別市大學.md "wikilink")
[Category:1948年創建的教育機構](../Category/1948年創建的教育機構.md "wikilink")

1.  2005年的數據