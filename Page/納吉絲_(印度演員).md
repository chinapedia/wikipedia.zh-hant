**納吉絲**（[印地語](../Page/印地語.md "wikilink"): नर्गिस,
[烏爾都語](../Page/烏爾都語.md "wikilink"):
نرگس）是活躍於二十世紀中期的印度演員，她最成功的表演作品是在由[梅赫布·罕導演的電影](../Page/梅赫布·罕.md "wikilink")《[印度母親](../Page/印度母親.md "wikilink")》中的拉達。中國觀眾所最熟悉的角色是由[拉吉·卡浦爾導演的電影](../Page/拉吉·卡浦爾.md "wikilink")《[流浪者](../Page/流浪者_\(印度電影\).md "wikilink")》中的麗達，至今為年長者所津津樂道。

在2007年的[國際婦女節](../Page/國際婦女節.md "wikilink")，印度的《[電影觀眾](../Page/電影觀眾雜誌.md "wikilink")》雜誌將她評為歷來第二位「最受歡迎的女演員」，僅次于[瑪都麗·迪西特](../Page/瑪都麗·迪西特.md "wikilink")。

## 個人生活

**納吉絲**生于一個信奉[伊斯蘭教的旁遮普人家庭](../Page/伊斯蘭.md "wikilink")，她原名為**法替瑪·拉什德**，1958年與印度演員[蘇尼爾·杜特](../Page/蘇尼爾·杜特.md "wikilink")（Sunil
Dutt）結婚后易名<U>納吉絲·杜特</U>，育有一子[山傑·杜特](../Page/山傑·杜特.md "wikilink")（Sanjay
Dutt）和二女安菊·杜特（Anju Dutt）、[枇婭·杜特](../Page/枇婭·杜特.md "wikilink")（Priya
Dutt），山傑和枇婭後來也成了演員。

## 電影事業

納吉絲很早年即開始從影，1935年，六歲的她在*Talashe Haq*片中頭一次扮演童星。當時的藝名為「納吉絲童子」，意思是「水仙花童子」。

1935年以後她繼續在銀幕上露面，但是成名是在她進入成年以後。在[四十年代至](../Page/四十年代.md "wikilink")[五十年代](../Page/五十年代.md "wikilink")，她在很多[印地語](../Page/印地語.md "wikilink")、[烏爾都語的電影中都參加演出](../Page/烏爾都語.md "wikilink")。在多數電影中她是同男影星[拉吉·卡浦爾和](../Page/拉吉·卡浦爾.md "wikilink")[第力·庫馬配對的](../Page/第力·庫馬.md "wikilink")。

1957年，納吉絲終於憑在《[印度母親](../Page/印度母親.md "wikilink")》中的出色演出獲得有一些遲來到的[印度電影觀眾獎](../Page/印度電影觀眾獎.md "wikilink")。

隨即她同演員[蘇尼爾·杜特結婚](../Page/蘇尼爾·杜特.md "wikilink")，退出影壇。直到1967年，她才在電影《*[Raat
Aur
Din](../Page/Raat_Aur_Din.md "wikilink")*》中再次出現，並籍此獲得當年的[印度國家電影獎——最佳女演員稱號](../Page/印度國家電影獎——最佳女演員.md "wikilink")。

1981年，納吉絲死于癌症，終年52歲。她的生命最後兩年多是在[美國醫院里度過的](../Page/美國.md "wikilink")，她身患絕症還不忘印度貧民沒有條件得到她所得到的治療機會，毅然捐出巨款成立[納吉絲癌症基金會力求幫助窮人獲得更好的醫療條件](../Page/納吉絲癌症基金會.md "wikilink")。

在她逝世同年她兒子[山傑·杜特的第一部電影](../Page/山傑·杜特.md "wikilink")《洛奇（*Rocky*）》推出，不但宣告他正式進軍影壇，而且代表著家族的第二代將前輩的名字在發揚光大。

## 得獎

  - 1957年——《[印度電影觀眾獎](../Page/印度電影觀眾獎.md "wikilink")——最佳女演員》，電影：《[印度母親](../Page/印度母親.md "wikilink")
    （*[Mother India](../Page/Mother_India.md "wikilink")*）》
  - 1968年——《[印度国家电影奖——最佳女演员奖](../Page/印度国家电影奖——最佳女演员奖.md "wikilink")》，電影：《*[Raat
    Aur Din](../Page/Raat_Aur_Din.md "wikilink")*》
  - 《*Urvashy Award*》——她是迄今獲得這一最高獎項的唯一印度女演員。\<\!-- the highest honour

that can be conferred on a movie actress in India. --\>\[1\]

## 其他榮譽

  - 2001年1月8日，被《[星塵雜誌](../Page/星塵雜誌.md "wikilink")》追授——《[印度百年最佳女演員獎](../Page/印度百年最佳女演員獎.md "wikilink")》。同日獲得[最佳男演員獎的是影帝](../Page/印度百年最佳男演員獎.md "wikilink")[阿米塔布·巴沙坎](../Page/阿米塔布·巴沙坎.md "wikilink")。
    \[2\]

## 纳吉斯·杜特癌症基金会

## 出演電影

  - *[Talashe Haq](../Page/Talashe_Haq.md "wikilink")* (1935)
  - *[Tamanna](../Page/Tamanna_\(film\).md "wikilink")* (1942)
  - *[Taqdeer](../Page/Taqdeer.md "wikilink")* (1943)
  - *[Humayun](../Page/Humayun_\(film\).md "wikilink")* (1945)
  - *[Bisvi Sadi](../Page/Bisvi_Sadi.md "wikilink")* (1945)
  - *[Nargis](../Page/Nargis_\(film\).md "wikilink")* (1946)
  - *[Mehandi](../Page/Mehandi_\(film\).md "wikilink")* (1947)
  - *[Mela](../Page/Mela_\(1948_film\).md "wikilink")* (1948)
  - *[Anokha Pyar](../Page/Anokha_Pyar.md "wikilink")* (1948)
  - *[Anjuman](../Page/Anjuman_\(film\).md "wikilink")* (1948)
  - *[Aag](../Page/Aag_\(1948_film\).md "wikilink")* (1948)
  - *[Roomal](../Page/Roomal.md "wikilink")* (1949)
  - *[Lahore](../Page/Lahore_\(film\).md "wikilink")* (1949)
  - *[Darogaji](../Page/Darogaji.md "wikilink")* (1949)
  - *[Barsaat](../Page/Barsaat.md "wikilink")* (1949)
  - *[Andaz](../Page/Andaz.md "wikilink")* (1949)
  - *[Pyaar](../Page/Pyaar.md "wikilink")* (1950)
  - *[Meena Bazaar](../Page/Meena_Bazaar.md "wikilink")* (1950)
  - *[Khel](../Page/Khel_\(film\).md "wikilink")* (1950)
  - *[Jogan](../Page/Jogan_\(1950_film\).md "wikilink")* (1950)
  - *[Jan Pahchan](../Page/Jan_Pahchan.md "wikilink")* (1950)
  - *[Chhoti Bhabbi](../Page/Chhoti_Bhabbi.md "wikilink")* (1950)
  - *[Babul](../Page/Babul_\(1950_film\).md "wikilink")* (1950)
  - *[Aadhi Raat](../Page/Aadhi_Raat.md "wikilink")* (1950)
  - *[Saagar](../Page/Saagar_\(1951_film\).md "wikilink")* (1951)
  - *[Pyar Ki Baaten](../Page/Pyar_Ki_Baaten.md "wikilink")* (1951)
  - *[Hulchul](../Page/Hulchul_\(1951_film\).md "wikilink")* (1951)
  - *[Deedar](../Page/Deedar.md "wikilink")* (1951)
  - *[流浪者](../Page/流浪者_\(印度電影\).md "wikilink")* -
    *[Awaara](../Page/Awaara.md "wikilink")* (1951)
  - *[Sheesha](../Page/Sheesha_\(film\).md "wikilink")* (1952)
  - *[Bewafaa](../Page/Bewafaa.md "wikilink")* (1952)
  - *[Ashiana](../Page/Ashiana.md "wikilink")* (1952)
  - *[Anhonee](../Page/Anhonee.md "wikilink")* (1952)
  - *[Amber](../Page/Amber_\(film\).md "wikilink")* (1952)
  - *[Shikast](../Page/Shikast.md "wikilink")* (1953)
  - *[Paapi](../Page/Paapi.md "wikilink")* (1953)
  - *[Dhoon](../Page/Dhoon.md "wikilink")* (1953)
  - *[Aah](../Page/Aah_\(film\).md "wikilink")* (1953)
  - *[Angarey](../Page/Angarey.md "wikilink")* (1954)
  - *[Shree 420](../Page/Shree_420.md "wikilink")* (1955)
  - *[Jagte Raho](../Page/Jagte_Raho.md "wikilink")* (1956)
  - *[Chori Chori](../Page/Chori_Chori.md "wikilink")* (1956)
  - *[Pardesi (1957 film)](../Page/Pardesi_\(1957_film\).md "wikilink")*
  - *[Mother India](../Page/Mother_India.md "wikilink")* (1957)
  - *[Lajwanti](../Page/Lajwanti.md "wikilink")* (1958)
  - *[Ghar Sansar](../Page/Ghar_Sansar.md "wikilink")* (1958)
  - *[Adalat](../Page/Adalat_\(film\).md "wikilink")* (1958)
  - *[Yaadein](../Page/Yaadein_\(1964_film\).md "wikilink")* (1964)
  - *[Raat Aur Din](../Page/Raat_Aur_Din.md "wikilink")* (1967)

## 參考

<references />

## 外部連結

  -
  - [納吉絲·杜特癌症基金會](https://web.archive.org/web/20071021182859/http://ndcfglobal.org/)

[Category:印度电影女演员](../Category/印度电影女演员.md "wikilink")
[Category:1929年出生](../Category/1929年出生.md "wikilink")
[Category:前穆斯林](../Category/前穆斯林.md "wikilink")
[Category:1981年逝世](../Category/1981年逝世.md "wikilink")
[Category:旁遮普人](../Category/旁遮普人.md "wikilink")

1.  [Nargis Dutt was the recipient of the "Urvashy
    Award".](http://nrcw.nic.in/shared/sublinkimages/142.htm)
2.  [Nargis Dutt honoured with the "Best Artists of the Millennium"
    award by \<<Hero Honda>\> and file magazine
    "Stardust".](https://www.webcitation.org/query?id=1256488091845348&url=www.geocities.com/anisharaja/honda-stardust.html)