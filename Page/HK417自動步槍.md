**HK417**是[德國](../Page/德國.md "wikilink")[黑克勒-科赫所推出的](../Page/黑克勒-科赫.md "wikilink")[7.62×51毫米NATO](../Page/7.62×51mm_NATO.md "wikilink")[戰鬥步槍](../Page/戰鬥步槍.md "wikilink")，以[HK416的內部設計修改而成](../Page/HK416突擊步槍.md "wikilink")。

## 設計

HK417採用短行程活塞傳動式系統，比[AR-10](../Page/AR-10自動步槍.md "wikilink")、[M16及](../Page/M16突擊步槍.md "wikilink")[M4的導氣管傳動式更為可靠](../Page/M4卡賓槍.md "wikilink")，\[1\]，有效減低維護次數，從而提高效能，並且可以在擊錘未被扳倒的情況下撥至保險位置。

早期的HK417樣槍採用來自[HK
G3](../Page/HK_G3自動步槍.md "wikilink")，沒有空槍掛機功能的20發金屬彈匣，後期改用了類似[HK
G36的半透明聚合塑料彈匣](../Page/HK_G36突擊步槍.md "wikilink")，這種彈匣具空槍掛機功能。\[2\]\[3\]。

HK417採用伸縮[槍托設計](../Page/槍托.md "wikilink")，槍托底部裝有緩衝塑料墊以降低射擊時的[後座力](../Page/後座力.md "wikilink")，[機匣及護木設有](../Page/機匣.md "wikilink")5條戰術導軌，採用自由浮動槍管設計，整個前護木可完全拆下，以節省維護時間。

HK417具有準確度高和可靠性高等的優點，因此主要以精確射手步槍作主要用途，用於與[狙擊步槍作高低搭配](../Page/狙擊步槍.md "wikilink")，必要時仍可作[全自動射擊](../Page/自動火器.md "wikilink")。

12寸槍管的HK417突擊型（Assaulter）是HK417系列中唯一能與[Mk 14
EBR作競爭的產品](../Page/Mk_14增強型戰鬥步槍.md "wikilink")，因為它縮起槍托後的長度只有805毫米。

## 衍生型

  - HK417突擊型（Assaulter）－12寸槍管
  - HK417偵察型（Recce）－16寸槍管
  - HK417狙擊型（Sniper）－20寸槍管
  - MR308－半自動民用型，目前只在德國發售
  - [HK G28](../Page/HK_G28狙擊步槍.md "wikilink")-精確射手型，由MR308改進而來。
  - MR762（在美國發售的半自動民用型，於2009年推出）

## 使用國

[Skarpskytter.jpg](https://zh.wikipedia.org/wiki/File:Skarpskytter.jpg "fig:Skarpskytter.jpg")
[Australian_Army_Pvt._Levi_Mooney,_right,_bumps_fists_with_a_child_during_a_patrol_in_Tarin_Kowt,_Uruzgan_province,_Afghanistan,_July_26,_2013_130726-Z-FS372-401.jpg](https://zh.wikipedia.org/wiki/File:Australian_Army_Pvt._Levi_Mooney,_right,_bumps_fists_with_a_child_during_a_patrol_in_Tarin_Kowt,_Uruzgan_province,_Afghanistan,_July_26,_2013_130726-Z-FS372-401.jpg "fig:Australian_Army_Pvt._Levi_Mooney,_right,_bumps_fists_with_a_child_during_a_patrol_in_Tarin_Kowt,_Uruzgan_province,_Afghanistan,_July_26,_2013_130726-Z-FS372-401.jpg")
[ARW_30th_Anniversary_-_4478683088.jpg](https://zh.wikipedia.org/wiki/File:ARW_30th_Anniversary_-_4478683088.jpg "fig:ARW_30th_Anniversary_-_4478683088.jpg")

  -   - \[4\]\[5\]

  -   - [澳大利亞陸軍](../Page/澳大利亞陸軍.md "wikilink")（D16RS，配備6倍[ACOG光學瞄準鏡作](../Page/ACOG光學瞄準鏡.md "wikilink")[精確射手步槍用途](../Page/精確射手步槍.md "wikilink")）\[6\]\[7\]\[8\]

  -   - [巴西聯邦警察](../Page/巴西聯邦警察.md "wikilink")\[9\]

  -   - 及（D14.5RS；用作精確射手步槍用途） \[10\]

  -   - （D20RS；用作精確射手步槍用途）

  -   - （D20RS，作精確射手步槍使用）\[11\]

  -   - [芬蘭國防軍特種部隊單位](../Page/芬蘭國防軍.md "wikilink")
      - 芬蘭國家調查局\[12\]

  -   - [法國陸軍](../Page/法國陸軍.md "wikilink")

      - \[13\]

      - [法國國家憲兵](../Page/法國國家憲兵.md "wikilink")[干預組](../Page/國家憲兵干預組.md "wikilink")

      - [干預組](../Page/國家警察干預組.md "wikilink")

      - 法國國家警察

  -   - [德國聯邦國防軍](../Page/德國聯邦國防軍.md "wikilink")（13寸槍管型417A2命名為「G27」\[14\]；狙擊步槍版命名為[G28](../Page/HK_G28狙擊步槍.md "wikilink")\[15\]\[16\]）
      - [德國聯邦警察](../Page/德國聯邦警察.md "wikilink")[第九國境守備隊](../Page/德國聯邦警察第九國境守備隊.md "wikilink")[狙擊小組](../Page/狙擊手.md "wikilink")\[17\]

  -   -
  -   - [愛爾蘭國防軍](../Page/愛爾蘭國防軍.md "wikilink")[陸軍遊騎兵狙擊小組](../Page/愛爾蘭陸軍遊騎兵.md "wikilink")\[18\]\[19\]

  -   - [義大利武裝部隊及特種部隊單位](../Page/意大利軍事.md "wikilink")（D12RS）\[20\]

  -   - [日本陸上自衛隊及](../Page/日本陸上自衛隊.md "wikilink")[特殊作戰群](../Page/特殊作戰群.md "wikilink")\[21\]

  -   -
  -   - [馬來西亞皇家海軍](../Page/馬來西亞皇家海軍.md "wikilink")[特種作戰部隊狙擊小組](../Page/海軍特種作戰部隊.md "wikilink")\[22\]\[23\]

  -   - [荷蘭皇家陸軍](../Page/荷蘭皇家陸軍.md "wikilink")（為D16RS，裝上Schmidt & Bender
        3-12×50光學瞄準鏡或[Aimpoint Comp
        M2紅點鏡](../Page/Aimpoint_Comp_M2紅點鏡.md "wikilink")，主要作為精確射手步槍使用）\[24\]\[25\]\[26\]

  -   - [挪威國防軍](../Page/挪威國防軍.md "wikilink")（用作精確射手步槍）\[27\]\[28\]\[29\]

  -   - \[30\]\[31\]

  -   -
      - 警察部門

  -   - [俄羅斯聯邦安全局](../Page/俄羅斯聯邦安全局.md "wikilink")[阿爾法小組](../Page/阿爾法小組.md "wikilink")（MR308，用作精確射手步槍）\[32\]
      - 執法機構特種部隊（MR308，用作精確射手步槍）\[33\]\[34\]

  -
  -   - 特警單位（S20RS）\[35\]

  -   - [瑞典國防軍](../Page/瑞典國防軍.md "wikilink")[特戰任務組](../Page/瑞典特戰任務組.md "wikilink")（D20RS）\[36\]

  -   -
      - 警察部門

  -   - （命名為“L2A1”）

      - [英國特種部隊狙擊小組](../Page/英國特種部隊.md "wikilink")\[37\]\[38\]（命名為“L2A1”）

      - \[39\]\[40\]

      -
  -   - [美國陸軍](../Page/美國陸軍.md "wikilink")（G28的改進型G28E，採用了M-Lock導軌護木並被美國陸軍命名為M110A1，作為下一代的緊湊型半自動狙擊系統"CSASS:Compact
        Semi-Automatic Sniper System"）\[41\]

      -
      - 警察部門

## 流行文化

### [電影](../Page/電影.md "wikilink")

  - 2012年—《[-{zh-hans:猎杀本·拉登; zh-hk:追擊拉登行動;
    zh-tw:00:30凌晨密令;}-](../Page/00:30凌晨密令.md "wikilink")》（Zero
    Dark
    Thirty）：12吋[槍管型](../Page/槍管.md "wikilink")，裝有瞄準鏡、[兩腳架和](../Page/兩腳架.md "wikilink")[抑制器](../Page/抑制器.md "wikilink")，被參與[海神之矛行動的](../Page/海神之矛行動.md "wikilink")[美國海軍特種作戰研究大隊](../Page/美國海軍特種作戰研究大隊.md "wikilink")[狙擊手所使用](../Page/狙擊手.md "wikilink")。
  - 2016年—《[-{zh-hk:13小時：班加西無名英雄;zh-tw:13小時：班加西的秘密士兵;zh-cn:危机13小时;}-](../Page/13小時：班加西的秘密士兵.md "wikilink")》（13
    Hours: The Secret Soldiers of
    Benghazi）：沙色塗裝並裝上[狙擊鏡](../Page/瞄準鏡.md "wikilink")、[抑制器和](../Page/抑制器.md "wikilink")[兩腳架](../Page/兩腳架.md "wikilink")，被馬克·「奧茲」·蓋斯特（[馬克斯·馬丁尼飾演](../Page/馬克斯·馬丁尼.md "wikilink")）和戴夫·「布恩」·班頓（[大衛·丹曼飾演](../Page/大衛·丹曼.md "wikilink")）所使用。

### [電子遊戲](../Page/電子遊戲.md "wikilink")

  - 2007年—《[戰地之王](../Page/戰地之王.md "wikilink")》：命名為HK417 Sniper，歸類為狙擊步槍。
  - 2011年—《[-{zh-hans:戰地3;
    zh-hant:戰地風雲3;}-](../Page/戰地風雲3.md "wikilink")》（Battlefield
    3）：被命名為「M417」，奇怪地使用灰色彈匣，只能使用半自動射擊，歸類為特等射手步槍。
  - 2013年—《[-{zh-hans:收获日;
    zh-hant:劫薪日;}-2](../Page/劫薪日2.md "wikilink")》（Payday
    2）：為scarface角色包的武器，掛載[M203榴彈發射器](../Page/M203榴彈發射器.md "wikilink")，命名為“Little
    Friend 7.62”。
  - 2013年—《[-{zh-hans:武装突袭;
    zh-hant:武裝行動;}-3](../Page/武装突袭3.md "wikilink")》（ArmA
    3）：型號為HK417A2，命名为“SPAR-17 7.62mm”，以特等射手步槍的形象登場。
  - 2015年—《[-{zh-hans:彩虹六号：围攻;
    zh-hant:虹彩六號：圍攻行動;}-](../Page/虹彩六號：圍攻行動.md "wikilink")》（Rainbow
    Six:
    Siege）：為16寸槍管型，命名為「417」，10發彈匣，只能使用半自動射擊，被[法國國家憲兵干預組](../Page/國家憲兵干預組.md "wikilink")（包括奇美拉赛季干员Lion）所使用。
  - 2016年（臺版2017年）—《[少女前線](../Page/少女前線.md "wikilink")》：命名為G28，為四星戰術人形，只能透過工廠重造取得。設定上為HK416的妹妹。

### [動畫](../Page/動畫.md "wikilink")

  - 2015年—《[GATE
    奇幻自衛隊](../Page/GATE_奇幻自衛隊.md "wikilink")》：於第1季第9話被[日本](../Page/日本.md "wikilink")[自衛隊](../Page/自衛隊.md "wikilink")[特殊作戰群的隊員所使用](../Page/陸上自衛隊特殊作戰群.md "wikilink")，裝上[夜視瞄準鏡和](../Page/夜視儀.md "wikilink")[抑制器](../Page/抑制器.md "wikilink")。

## 參見

  - [SR-25](../Page/SR-25.md "wikilink")
  - [M110](../Page/M110.md "wikilink")
  - [L129A1](../Page/L129A1精確射手步槍.md "wikilink")
  - [AR-10](../Page/AR-10.md "wikilink")
  - [HK416](../Page/HK416.md "wikilink")

## 参考文献

## 外部链接

  - —[黑克勒-科赫官方頁面－HK417](http://www.heckler-koch.de/HKWebText/detailProd/1928/345/4/19)

[Category:黑克勒-科赫](../Category/黑克勒-科赫.md "wikilink")
[Category:自动步枪](../Category/自动步枪.md "wikilink")
[Category:戰鬥步槍](../Category/戰鬥步槍.md "wikilink")
[Category:狙擊步槍](../Category/狙擊步槍.md "wikilink")
[Category:精確射手步槍](../Category/精確射手步槍.md "wikilink")
[Category:7.62×51毫米槍械](../Category/7.62×51毫米槍械.md "wikilink")
[Category:德國槍械](../Category/德國槍械.md "wikilink")

1.  [Newer carbines outperform M4 in dust
    test](http://www.armytimes.com/news/2007/12/army_carbine_dusttest_071217/)

2.

3.  [gun-world.net—HK417步槍系統](http://firearmsworld.net/german/hk/hkm4/hk417.htm)

4.
5.  [Special Operations Battalion
    (Albania)](../Page/Special_Operations_Battalion_\(Albania\).md "wikilink")

6.
7.

8.

9.

10. <https://upload.wikimedia.org/wikipedia/commons/0/0d/HK_417_080810_44.jpg>

11. <https://sites.google.com/site/worldinventory/wiw_eu_estonia>

12. <https://sites.google.com/site/worldinventory/wiw_eu_finland>

13.

14.

15.
16.

17.

18.
19.

20. <https://sites.google.com/site/worldinventory/wiw_eu_italy>

21. <http://www.mod.go.jp/gsdf/gmcc/hoto/hkou/14hk093.pdf>

22.
23.

24.
25.

26.

27.
28. Offisersbladet nr. 3, May 2007: *Heckler & Koch 416: Vårt nye
    håndvåpen*

29.

30.
31.

32. <http://www.hkpro.com/forum/chef-s-corner-hk-action-series-continues/187480-hk-russia-2.html>

33. <https://m.youtube.com/watch?v=bna78e-h3RI&feature=youtu.be>

34.

35. <https://sites.google.com/site/worldinventory/wiw_eu_spain>

36. <https://sites.google.com/site/worldinventory/wiw_eu_sweden>

37.
38.

39.

40.

41. [H\&K confirms: This is the Army's new and improved sniper
    rifle](http://www.armytimes.com/story/military/2016/04/08/hk-confirms-armys-new-and-improved-sniper-rifle/82788202/)
    - Armytimes.com, 8 April 2016