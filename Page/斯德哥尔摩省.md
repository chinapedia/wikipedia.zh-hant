**斯德哥尔摩省**（）位于[瑞典东海岸](../Page/瑞典.md "wikilink")。它与[南曼兰省和](../Page/南曼兰省.md "wikilink")[乌普萨拉省相邻](../Page/乌普萨拉省.md "wikilink")，也以[波罗的海和](../Page/波罗的海.md "wikilink")[梅拉伦湖为界](../Page/梅拉伦湖.md "wikilink")。首府[斯德哥尔摩也是瑞典](../Page/斯德哥尔摩.md "wikilink")[首都](../Page/首都.md "wikilink")。

## 主要城市

[Municipalities_of_Stockholm.png](https://zh.wikipedia.org/wiki/File:Municipalities_of_Stockholm.png "fig:Municipalities_of_Stockholm.png")

  - [胡丁厄](../Page/胡丁厄.md "wikilink") - Huddinge
  - [尼奈斯港](../Page/尼奈斯港.md "wikilink") - Nynäshamn
  - [塞勒姆](../Page/塞勒姆.md "wikilink") - Salem
  - [锡格蒂纳](../Page/锡格蒂纳.md "wikilink") - Sigtuna
  - [索伦蒂纳](../Page/索伦蒂纳.md "wikilink") - Sollentuna
  - [索尔纳](../Page/索尔纳.md "wikilink") - Solna
  - [斯德哥尔摩](../Page/斯德哥尔摩.md "wikilink") - Stockholm
  - [松德比贝里](../Page/松德比贝里.md "wikilink") - Sundbyberg
  - [南泰利耶](../Page/南泰利耶.md "wikilink") - Södertälje
  - [泰比](../Page/泰比.md "wikilink") - Täby
  - [乌普兰斯韦斯比](../Page/乌普兰斯韦斯比.md "wikilink") - Upplands Väsby

## 外部链接

  - [斯德哥尔摩省官方网站](http://www.ab.lst.se/)
  - [斯德哥尔摩省议会网站](http://www.sll.se/)
  - [斯德哥尔摩省2006年大选结果](http://www.val.se/val/val2006/slutlig/L/landsting/01/roster.html)
  - [斯德哥尔摩大学信息](https://web.archive.org/web/20080706210917/http://www.educationguide.cn/liuxuedaohang/ruidian/yuanxiaozhuanye/yuanxiaoyilan/812.html)

[\*](../Category/斯德哥尔摩省.md "wikilink")
[Category:瑞典省份](../Category/瑞典省份.md "wikilink")