[黄土岭战斗中缴获日军药箱.jpg](https://zh.wikipedia.org/wiki/File:黄土岭战斗中缴获日军药箱.jpg "fig:黄土岭战斗中缴获日军药箱.jpg")
**黃土嶺戰鬥**發生於1939年11月，地點是[河北省](../Page/河北省.md "wikilink")[涞源县的黄土岭地区](../Page/涞源县.md "wikilink")。是[抗日戰爭中的戰鬥之一](../Page/抗日戰爭.md "wikilink")，交戰一方為[中国共产党領導的](../Page/中国共产党.md "wikilink")[國民革命軍第十八集團軍](../Page/國民革命軍.md "wikilink")，即[八路軍](../Page/八路軍.md "wikilink")，另一方則為[日本军独立混成第](../Page/日本军.md "wikilink")2旅团。

雁宿崖战斗结束之后，11月4日，驻[张家口日军出动](../Page/张家口.md "wikilink")[华北方面军](../Page/华北方面军.md "wikilink")[驻蒙军独立混成第](../Page/驻蒙军.md "wikilink")2旅团所属各部实行报复性“扫荡”。11月7日，日军进入黄土岭伏击圈，八路军当即发起攻击，日军伤亡过半。11月8日晨，日军多路增援，八路军遂撤出战斗，黄土岭战斗结束，日軍至少陣亡百人，受傷一百一十人\[1\]，独立混成第2旅团旅团长[阿部规秀中将陣亡](../Page/阿部规秀.md "wikilink")。阿部规秀是共产党部队在抗战中，正面战斗击毙的日军最高指挥官。\[2\]\[3\]\[4\]\[5\]\[6\]

## 參見

  - [抗日戰爭戰鬥列表](../Page/抗日戰爭戰鬥列表.md "wikilink")

## 參考文獻

<references />

  - [中華民國](../Page/中華民國.md "wikilink")[國防大學編](../Page/國防大學_\(臺灣\).md "wikilink")，《中國現代軍事史主要戰役表》

[Category:1940年中国战役](../Category/1940年中国战役.md "wikilink")
[Category:抗日战争主要战役](../Category/抗日战争主要战役.md "wikilink")
[Category:河北历次战争与战役](../Category/河北历次战争与战役.md "wikilink")
[Category:保定历史](../Category/保定历史.md "wikilink")
[Category:涞源县](../Category/涞源县.md "wikilink")
[Category:易县](../Category/易县.md "wikilink")

1.  JACAR(アジア歴史資料センター)Ref.C07091447900、昭和１５年「陸支普大日記第５号」(防衛省防衛研究所)
2.
3.
4.
5.
6.