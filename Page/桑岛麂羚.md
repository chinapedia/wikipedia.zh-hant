**桑岛麂羚**（學名*Cephalophus
adersi*），别名**桑岛霓羚**，是一种小型森林[麂羚](../Page/麂羚.md "wikilink")，仅分布与[坦桑尼亚的](../Page/坦桑尼亚.md "wikilink")[桑给巴尔岛和](../Page/桑给巴尔岛.md "wikilink")[肯尼亚的](../Page/肯尼亚.md "wikilink")[阿拉布口索口科國家公園](../Page/阿拉布口索口科國家公園.md "wikilink")。有些学者曾经认为桑岛麂羚是[红麂羚](../Page/红麂羚.md "wikilink")，[哈氏麂羚或](../Page/哈氏麂羚.md "wikilink")[青臀麂羚的](../Page/青臀麂羚.md "wikilink")[亚种](../Page/亚种.md "wikilink")。

桑岛麂羚肩高30厘米。分布于东部桑给巴尔的麂羚有12千克重，而南部桑给巴尔的麂羚体重仅7.5千克。桑岛麂羚有著赤褐色的毛皮，頸部呈灰色，背部及腹部較淺色。在頭頂上有一細少紅色的冠狀物。桑岛麂羚有細少的角，約長3-6厘米。

桑岛麂羚生活在靠岸的[森林及](../Page/森林.md "wikilink")[林地](../Page/林地.md "wikilink")，以當中從樹上掉下的[花](../Page/花.md "wikilink")、[樹葉及](../Page/樹葉.md "wikilink")[果實為食物](../Page/果實.md "wikilink")。桑岛麂羚是白天活動的[動物](../Page/動物.md "wikilink")，由[黎明時份至中午出沒覓食](../Page/黎明.md "wikilink")，在正午休息，並再次活躍直至[黃昏](../Page/黃昏.md "wikilink")。牠們是一對的生活並擁有細少的領土。

據估計約有1400頭桑岛麂羚存在世上。牠們受到棲息地的破壞、[流浪犬及](../Page/流浪犬.md "wikilink")，尤其是過度的獵殺所威脅。桑岛麂羚被癢殺的原因是牠們所擁有的軟皮及甜美的肉質。

## 參考

  -
  -
[Category:麂羚屬](../Category/麂羚屬.md "wikilink")
[Category:肯亞動物](../Category/肯亞動物.md "wikilink")
[Category:坦桑尼亞動物](../Category/坦桑尼亞動物.md "wikilink")