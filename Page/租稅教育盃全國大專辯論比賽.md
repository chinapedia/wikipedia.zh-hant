**租稅盃**全名為**租稅教育盃全國大專辯論比賽**，為[台灣辯論圈最大的盃賽](../Page/台灣辯論圈.md "wikilink")，不單單因為其獎金高、隊伍多，還因其歷史悠久達三十年以上，且其題目的深度、廣度也夠，是為[政策性命題中最具代表性的盃賽](../Page/政策性命題.md "wikilink")。

## 歷屆成績

待補齊

  - 第十六屆 79學年度
      - 冠軍：[東吳大學](../Page/東吳大學正言社.md "wikilink")
      - 亞軍：[文化大學](../Page/中國文化大學華岡演辯社.md "wikilink")
      - 季軍：[待補](../Page/待補.md "wikilink")
      - 殿軍：[待補](../Page/待補.md "wikilink")

<!-- end list -->

  - 第十七屆 80年度
      - 冠軍：[東吳大學](../Page/東吳大學正言社.md "wikilink")
      - 亞軍：[中央大學](../Page/國立中央大學.md "wikilink")
      - 季軍：[文化大學](../Page/中國文化大學華岡演辯社.md "wikilink")
      - 殿軍：[中興法商](../Page/臺北大學滔滔社.md "wikilink")

<!-- end list -->

  - 第十八屆 81年度
      - 冠軍：[臺灣大學](../Page/臺灣大學健言社.md "wikilink")
      - 亞軍：[輔仁大學](../Page/輔仁大學健言社.md "wikilink")
      - 季軍：[銘傳大學](../Page/銘傳大學.md "wikilink")
      - 殿軍：[交通大學](../Page/國立交通大學.md "wikilink")

<!-- end list -->

  - 第十九屆 82年度
      - 冠軍：[臺灣大學](../Page/臺灣大學健言社.md "wikilink")
      - 亞軍：[東吳大學](../Page/東吳大學正言社.md "wikilink")
      - 季軍：[輔仁大學](../Page/輔仁大學健言社.md "wikilink")
      - 殿軍：[中央大學](../Page/國立中央大學.md "wikilink")

<!-- end list -->

  - 第二十屆 83年度
      - 冠軍：[中興法商](../Page/臺北大學滔滔社.md "wikilink")
      - 亞軍：[輔仁大學](../Page/輔仁大學健言社.md "wikilink")
      - 季軍：[淡江大學](../Page/淡江大學.md "wikilink")
      - 殿軍：[銘傳大學](../Page/銘傳大學.md "wikilink")

<!-- end list -->

  - 第二十一屆 84年度
      - 冠軍：[中興法商](../Page/臺北大學滔滔社.md "wikilink")
      - 亞軍：[東吳大學](../Page/東吳大學正言社.md "wikilink")
      - 季軍：[中山大學](../Page/國立中山大學.md "wikilink")
      - 殿軍：[靜宜大學](../Page/靜宜大學.md "wikilink")

<!-- end list -->

  - 第二十二屆 85年度
      - 冠軍：[中興法商](../Page/臺北大學滔滔社.md "wikilink")
      - 亞軍：[輔仁大學](../Page/輔仁大學健言社.md "wikilink")
      - 季軍：[臺灣大學](../Page/臺灣大學健言社.md "wikilink")
      - 殿軍：[東海大學](../Page/東海大學_\(台灣\).md "wikilink")

<!-- end list -->

  - 第二十三屆 86年度
      - 冠軍：[銘傳大學](../Page/銘傳大學.md "wikilink")
      - 亞軍：[中央大學](../Page/國立中央大學.md "wikilink")
      - 季軍：[政治大學](../Page/政治大學演辯社.md "wikilink")
      - 殿軍：[輔仁大學](../Page/輔仁大學健言社.md "wikilink")

<!-- end list -->

  - 第二十四屆 87年度
      - 冠軍：[清華大學](../Page/清華大學思言社.md "wikilink")
      - 亞軍：[東吳大學](../Page/東吳大學正言社.md "wikilink")
      - 季軍：[政治大學](../Page/政治大學演辯社.md "wikilink")
      - 殿軍：[中山大學](../Page/國立中山大學.md "wikilink")

<!-- end list -->

  - 第二十五屆 88年度
      - 冠軍：[臺灣大學](../Page/臺灣大學健言社.md "wikilink")
      - 亞軍：[臺北大學](../Page/臺北大學滔滔社.md "wikilink")
      - 季軍：[銘傳大學](../Page/銘傳大學.md "wikilink")
      - 殿軍：[淡江大學](../Page/淡江大學.md "wikilink")

<!-- end list -->

  - 第二十六屆 90年度
      - 冠軍：[交通大學](../Page/國立交通大學.md "wikilink")
      - 亞軍：[輔仁大學](../Page/輔仁大學健言社.md "wikilink")
      - 季軍：[臺北大學](../Page/臺北大學滔滔社.md "wikilink")
      - 殿軍：[東吳大學](../Page/東吳大學正言社.md "wikilink")

<!-- end list -->

  - 第二十七屆 91年度
      - 冠軍：[臺北大學](../Page/臺北大學滔滔社.md "wikilink")
      - 亞軍：[中央大學](../Page/國立中央大學.md "wikilink")
      - 季軍：東海大學
      - 殿軍：[中山大學](../Page/國立中山大學.md "wikilink")

<!-- end list -->

  - 第二十八屆 92年度
      - 冠軍：[臺灣大學](../Page/臺灣大學健言社.md "wikilink")
      - 亞軍：[東吳大學](../Page/東吳大學正言社.md "wikilink")
      - 季軍：[政治大學](../Page/政治大學演辯社.md "wikilink")
      - 殿軍：[淡江大學](../Page/淡江大學.md "wikilink")

<!-- end list -->

  - 第二十九屆 93年度
      - 冠軍：[東吳大學](../Page/東吳大學正言社.md "wikilink")
      - 亞軍：[臺灣大學](../Page/臺灣大學健言社.md "wikilink")
      - 季軍：[輔仁大學](../Page/輔仁大學健言社.md "wikilink")
      - 殿軍：[高雄醫學院](../Page/高雄醫學大學.md "wikilink")

<!-- end list -->

  - 第三十屆 94年度
      - 冠軍：[臺灣大學](../Page/臺灣大學健言社.md "wikilink")
      - 亞軍：[政治大學](../Page/政治大學演辯社.md "wikilink")
      - 季軍：[文化大學](../Page/中國文化大學華岡演辯社.md "wikilink")
      - 殿軍：[元智大學](../Page/元智大學.md "wikilink")

<!-- end list -->

  - 第三十一屆 95年度
      - 冠軍：[清華大學](../Page/清華大學思言社.md "wikilink")
      - 亞軍：[淡江大學](../Page/淡江大學.md "wikilink")
      - 季軍：[輔仁大學](../Page/輔仁大學健言社.md "wikilink")
      - 殿軍：[文化大學](../Page/中國文化大學華岡演辯社.md "wikilink")

<!-- end list -->

  - 102年度全國大專盃租稅辯論賽比賽
      - 冠軍：[台北大學A](../Page/台北大學A.md "wikilink") 最佳辯士：黃上豪
      - 亞軍：[政大演辯B](../Page/政大演辯B.md "wikilink") 最佳辯士：江運澤
      - 季軍：[台灣大學B](../Page/台灣大學B.md "wikilink") 最佳辯士：藍郁婷
      - 殿軍：[東吳大學B](../Page/東吳大學B.md "wikilink") 最佳辯士：徐紫縈

<!-- end list -->

  - 103年度全國大專盃租稅辯論比賽
      - 冠軍：[中國文化大學B](../Page/中國文化大學B.md "wikilink") 最佳辯士：張庭嘉
      - 亞軍：[國立政治大學演辯社A](../Page/國立政治大學演辯社A.md "wikilink") 最佳辯士：溫怡婷
      - 季軍：[國立中興大學](../Page/國立中興大學.md "wikilink") 最佳辯士：李庭偉
      - 殿軍：[國立臺灣大學B](../Page/國立臺灣大學B.md "wikilink") 最佳辯士：丁冠羽

<!-- end list -->

  - 104年度統一發票大專盃租稅辯論比賽
      - 冠軍：[政大演辯B](../Page/政大演辯B.md "wikilink") 最佳辯士：蕭靖穎
      - 亞軍：[東吳大學B](../Page/東吳大學B.md "wikilink") 最佳辯士：曾厚恩
      - 季軍：[政大演辯A](../Page/政大演辯A.md "wikilink") 最佳辯士：江運澤
      - 殿軍：[東吳大學A](../Page/東吳大學A.md "wikilink") 最佳辯士：張哲耀

<!-- end list -->

  - 105年度統一發票盃大專租稅辯論比賽
      - 冠軍：[臺灣大學A](../Page/臺灣大學健言社.md "wikilink") 最佳辯士：何奕萱
      - 亞軍：[政治大學A](../Page/政治大學演辯社.md "wikilink") 最佳辯士：周景賀
      - 季軍：[東吳大學D](../Page/東吳大學正言社.md "wikilink") 最佳辯士：曾厚恩
      - 殿軍：[政大政治](../Page/政大政治.md "wikilink") 最佳辯士：歐陽正霆

<!-- end list -->

  - 106年度統一發票盃大專租稅辯論比賽
      - 冠軍：[國立臺灣大學](../Page/國立臺灣大學.md "wikilink") 最佳辯士：丁冠羽
      - 亞軍：[東吳大學](../Page/東吳大學正言社.md "wikilink") 最佳辯士：林聖偉
      - 季軍：[淡江大學](../Page/淡江大學.md "wikilink") 最佳辯士：洪惇旻
      - 殿軍：[國立中山大學](../Page/國立中山大學.md "wikilink") 最佳辯士：丁啟翔

<!-- end list -->

  - 107年度統一發票盃大專租稅辯論比賽
      - 冠軍：[成功大學](../Page/成功大學.md "wikilink") 最佳辯士：王詣昕、李承哲
      - 亞軍：[台北科技大學](../Page/台北科技大學.md "wikilink")
      - 季軍：[東吳大學B](../Page/東吳大學B.md "wikilink") 最佳辯士：陳文謙
      - 殿軍：[台北大學](../Page/台北大學.md "wikilink") 最佳辯士：江杏霙

[category:台灣辯論圈](../Page/category:台灣辯論圈.md "wikilink")