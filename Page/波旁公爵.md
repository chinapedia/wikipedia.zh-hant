**波旁公爵**（[法语](../Page/法语.md "wikilink")：**Duc de
Bourbon**）是[法国的一个](../Page/法国.md "wikilink")[贵族爵位](../Page/贵族.md "wikilink")。它最早出现于14世纪前半叶。第一位有此称号的领主是[克莱蒙伯爵](../Page/克莱蒙.md "wikilink")[法兰西的罗贝尔和](../Page/法兰西的罗贝尔.md "wikilink")[波旁领地的女继承人](../Page/波旁.md "wikilink")[勃艮第的](../Page/勃艮第.md "wikilink")[比阿特丽斯的长子](../Page/比阿特丽斯.md "wikilink")[路易一世公爵](../Page/路易一世·德·波旁.md "wikilink")。波旁公爵家族的后代中衍生出波旁家族，他们在法国和[西班牙建立起强大的](../Page/西班牙.md "wikilink")[波旁王朝](../Page/波旁王朝.md "wikilink")。从那以后，波旁公爵的头衔仅颁给过几位[孔代亲王和其他宗室亲王](../Page/孔代亲王.md "wikilink")。

## 波旁公爵列表

[Blason_france_departement_allier.jpg](https://zh.wikipedia.org/wiki/File:Blason_france_departement_allier.jpg "fig:Blason_france_departement_allier.jpg")
**貴族地位**

  - [路易一世](../Page/路易一世·德·波旁.md "wikilink") (1279年—1342年)
  - [皮埃尔一世](../Page/皮埃尔一世·德·波旁.md "wikilink") (1311年–1356年)
  - [路易二世](../Page/路易二世·德·波旁_\(波旁公爵\).md "wikilink") (1356年–1410年)
  - [让一世](../Page/让一世·德·波旁.md "wikilink") (1410年–1434年)
  - [夏尔一世](../Page/夏尔一世·德·波旁.md "wikilink") (1434年–1456年)
  - [让二世](../Page/让二世·德·波旁.md "wikilink") (1456年–1488年)
  - [夏尔二世](../Page/夏尔二世·德·波旁.md "wikilink") (1488年–1488年)，里昂大主教
  - [皮埃尔二世](../Page/皮埃尔二世·德·波旁.md "wikilink") (1488 年–1503年)
  - [苏珊](../Page/苏珊·德·波旁.md "wikilink") (1503年–1525年)
  - [夏尔三世·德·波旁—蒙庞西埃](../Page/夏尔三世·德·波旁.md "wikilink") (1505年–1527年)

**1523年後個人的爵位**

  - [波旁公爵路易四世·亨利](../Page/路易四世·亨利·德·波旁_\(孔代親王\).md "wikilink")
    (1692年–1740年)
  - [孔代親王路易六世·亨利](../Page/路易六世·亨利·德·波旁_\(孔代親王\).md "wikilink")
    (1756年–1830年)

**1793年後頭銜稱謂**

  - 1950年–1975年 :
    [安茹公爵](../Page/安茹公爵.md "wikilink")[阿方索](../Page/阿方索_\(安茹及加的斯公爵\).md "wikilink")（1975年後改稱[加的斯公爵](../Page/加的斯公爵.md "wikilink")、安茹公爵）
  - 1975年–1984年 :
    [弗朗索瓦](../Page/弗朗索瓦·德·波旁_\(1972－1984\).md "wikilink")（安茹公爵阿方索長子，前稱[布列塔尼公爵](../Page/布列塔尼公爵.md "wikilink")）
  - 1984年至今 :
    安茹公爵[路易·阿方索](../Page/路易·阿方索_\(安茹公爵\).md "wikilink")（安茹公爵阿方索次子，前稱[圖賴訥公爵](../Page/圖賴訥公爵.md "wikilink")，1989年後改稱安茹公爵）

## 相关条目

  - [波旁王朝](../Page/波旁王朝.md "wikilink")

## 外部链接

  - [波旁公爵](http://genealogy.euweb.cz/capet/capet22.html)

[Category:波旁公爵](../Category/波旁公爵.md "wikilink")