**广陵琴派**[古琴派別](../Page/古琴.md "wikilink")，创始人為[揚州人](../Page/揚州.md "wikilink")[徐常遇](../Page/徐常遇.md "wikilink")，而揚州古稱廣陵而得名。

徐常遇傳子[徐祜](../Page/徐祜.md "wikilink")、[徐袆](../Page/徐袆.md "wikilink")，孫[徐锦堂以及](../Page/徐锦堂.md "wikilink")[鲁鼐](../Page/鲁鼐.md "wikilink")。徐常遇編著《[響山堂琴譜](../Page/響山堂琴譜.md "wikilink")》、《[琴譜指法](../Page/琴譜指法.md "wikilink")》，其子於1702年校勘成書，為《[澄鑒堂琴譜](../Page/澄鑒堂琴譜.md "wikilink")》。

徐祜、徐袆曾於[北京](../Page/北京.md "wikilink")[报国寺琴台角艺](../Page/报国寺.md "wikilink")，因名倾京都而被称为“江南二徐”，被[康熙两次在](../Page/康熙.md "wikilink")“畅春院”召见。徐锦堂则专心授徒，傳[沈江门](../Page/沈江门.md "wikilink")、[吴重光](../Page/吴重光.md "wikilink")、[王友衡](../Page/王友衡.md "wikilink")、[曹礼周](../Page/曹礼周.md "wikilink")、[江丽田](../Page/江丽田.md "wikilink")、[释宝月](../Page/释宝月.md "wikilink")、[吴灴](../Page/吴灴.md "wikilink")、[李廷敬](../Page/李廷敬.md "wikilink")、[乔钟吴](../Page/乔钟吴.md "wikilink")、[张敦仁](../Page/张敦仁.md "wikilink")、[吴官心](../Page/吴官心.md "wikilink")、[李光塽等](../Page/李光塽.md "wikilink")，吴重光传[吴文焕](../Page/吴文焕.md "wikilink")。徐錦堂一脈讲求演习，为广陵琴派的传承和发展作出了巨大贡献。鲁鼐传[马兆辰](../Page/马兆辰.md "wikilink")、[申涵诚](../Page/申涵诚.md "wikilink")、[鲁宗文](../Page/鲁宗文.md "wikilink")、[鲁宗鲁](../Page/鲁宗鲁.md "wikilink")、[蔡升元](../Page/蔡升元.md "wikilink")、[傅王露](../Page/傅王露.md "wikilink")、[胡开今](../Page/胡开今.md "wikilink")、[马文灏](../Page/马文灏.md "wikilink")、[汪士鋐](../Page/汪士鋐.md "wikilink")、[王弘](../Page/王弘.md "wikilink")、[陈瑨](../Page/陈瑨.md "wikilink")、[史顺](../Page/史顺.md "wikilink")、[吴观](../Page/吴观.md "wikilink")、[汪鹤孙等](../Page/汪鹤孙.md "wikilink")。此輩間，吴文焕著《[存古堂琴谱](../Page/存古堂琴谱.md "wikilink")》，[吴灴著](../Page/吴灴.md "wikilink")《[自远堂琴谱](../Page/自远堂琴谱.md "wikilink")》，吴官心著《[吴官心谱](../Page/吴官心谱.md "wikilink")》，李光塽著《[兰田馆琴谱](../Page/兰田馆琴谱.md "wikilink")》，马兆辰著《[卧云楼琴谱](../Page/卧云楼琴谱.md "wikilink")》。

《澄鉴堂琴谱》及揚州琴家父子[徐祺](../Page/徐祺.md "wikilink")、[徐俊於](../Page/徐俊.md "wikilink")1724年編著出版的《[五知齋琴譜](../Page/五知齋琴譜.md "wikilink")》以後，广陵琴派在[乾隆](../Page/乾隆.md "wikilink")、[嘉庆年间进入鼎盛时期](../Page/嘉庆.md "wikilink")。而[吴灴所编著](../Page/吴灴.md "wikilink")《自远堂琴谱》提供比較完整的古琴音樂理論。

吴灴传颜夫人（真名不详）、[先机和尚](../Page/先机和尚.md "wikilink")，為俗、释两派之始。俗家一派，由颜夫人传[梅植之](../Page/梅植之.md "wikilink")、[姚仲虞](../Page/姚仲虞.md "wikilink")、[周璜](../Page/周璜.md "wikilink")、[符南樵等](../Page/符南樵.md "wikilink")。梅植之傳[薛介白](../Page/薛介白.md "wikilink")、[王竹溪](../Page/王竹溪.md "wikilink")、[黄慎台](../Page/黄慎台.md "wikilink")、[沈战门](../Page/沈战门.md "wikilink")、[任汉等](../Page/任汉.md "wikilink")。释家一派，由先机和尚传[明辰和尚](../Page/明辰和尚.md "wikilink")（号问樵）、[袁澄](../Page/袁澄.md "wikilink")（道士）、[牧村和尚](../Page/牧村和尚.md "wikilink")、[逸梅和尚](../Page/逸梅和尚.md "wikilink")、[秦维翰](../Page/秦维翰.md "wikilink")。

秦维翰传[孙檀生](../Page/孙檀生.md "wikilink")、[胡鉴](../Page/胡鉴.md "wikilink")、[赵逸峰](../Page/赵逸峰.md "wikilink")（道士）、[何本祖](../Page/何本祖.md "wikilink")、[向子衡](../Page/向子衡.md "wikilink")、[丁玉田](../Page/丁玉田.md "wikilink")、[解石琴](../Page/解石琴.md "wikilink")、[徐北海](../Page/徐北海.md "wikilink")、[乔子峰](../Page/乔子峰.md "wikilink")、[王素](../Page/王素.md "wikilink")、[王耀先](../Page/王耀先.md "wikilink")、[徐卓卿](../Page/徐卓卿.md "wikilink")、[闻溪和尚](../Page/闻溪和尚.md "wikilink")、[海琴和尚及四大琴僧](../Page/海琴和尚.md "wikilink")[雨山](../Page/雨山.md "wikilink")、[莲溪](../Page/莲溪.md "wikilink")、[皎然](../Page/皎然.md "wikilink")、[普禅](../Page/普禅.md "wikilink")。秦维翰编著重要的《[蕉庵琴谱](../Page/蕉庵琴谱.md "wikilink")》4卷共32首琴曲，於1876年隐遁山林不知所终

胡鉴传[胡滋甫](../Page/胡滋甫.md "wikilink")，胡滋甫传[胡斗东](../Page/胡斗东.md "wikilink")、[胡兰](../Page/胡兰.md "wikilink")、[胥桐华](../Page/胥桐华.md "wikilink")、[陈泰芳](../Page/陈泰芳.md "wikilink")、[姜育华](../Page/姜育华.md "wikilink")；海琴和尚传[王芳谷](../Page/王芳谷.md "wikilink")、[广霞和尚](../Page/广霞和尚.md "wikilink")，广霞和尚传[王艺之](../Page/王艺之.md "wikilink")、[孙阆仙](../Page/孙阆仙.md "wikilink")。

牧村和尚传[空尘和尚](../Page/空尘和尚.md "wikilink")，空尘和尚於1893年刊行《[枯木禅琴谱](../Page/枯木禅琴谱.md "wikilink")》，对当今广陵琴派“刚中有柔、柔中有刚、缓中有急、急中有缓”的琴曲处理方法产生很大影响。

空尘和尚傳[肇慈](../Page/肇慈.md "wikilink")、[印恒](../Page/印恒.md "wikilink")、[起海](../Page/起海.md "wikilink")、[朱渚](../Page/朱渚.md "wikilink")、[如恒](../Page/如恒.md "wikilink")、[钱鎬龄](../Page/钱鎬龄.md "wikilink")、[钱发荣](../Page/钱发荣.md "wikilink")、[朱兆蓉](../Page/朱兆蓉.md "wikilink")、[邵鼎](../Page/邵鼎.md "wikilink")、[黄勉之](../Page/黄勉之.md "wikilink")。黄勉之在北京办[金陵琴社](../Page/金陵琴社.md "wikilink")，传[杨宗稷](../Page/杨宗稷.md "wikilink")、[史荫美](../Page/史荫美.md "wikilink")、[张之洞](../Page/张之洞.md "wikilink")、[溥侗](../Page/溥侗.md "wikilink")、[叶诗梦](../Page/叶诗梦.md "wikilink")、[贾阔峰等](../Page/贾阔峰.md "wikilink")，杨宗稷著《[琴学丛书](../Page/琴学丛书.md "wikilink")》；杨宗稷传[杨葆元](../Page/杨葆元.md "wikilink")、[李静](../Page/李静.md "wikilink")、[金致淇](../Page/金致淇.md "wikilink")、[黄则均](../Page/黄则均.md "wikilink")、[彭祉卿等](../Page/彭祉卿.md "wikilink")；彭祉卿传[柳希庐](../Page/柳希庐.md "wikilink")、[沈烈炎等](../Page/沈烈炎.md "wikilink")。

[辛亥革命前后](../Page/辛亥革命.md "wikilink")，操缦世家出生的[孙绍陶师从丁玉田](../Page/孙绍陶.md "wikilink")、解石琴，得广陵琴派之真传，琴艺一时称绝，1912年，以孙绍陶为首，与同好王方谷、胡滋甫、[夏友柏](../Page/夏友柏.md "wikilink")、[高治平等创建广陵](../Page/高治平.md "wikilink")[琴社](../Page/古琴#琴社.md "wikilink")，孙绍陶被推举为社长，主持琴社20余年。其间，[张子谦](../Page/张子谦.md "wikilink")、[刘少椿](../Page/刘少椿.md "wikilink")、[翟小坡](../Page/翟小坡.md "wikilink")、[胡斗东等相继入社](../Page/胡斗东.md "wikilink")，1935年，社友由10余人发展到50多人。1936年秋，在孙绍陶的主持下，扬州广陵琴社在[史公祠内梅花岭举行雅集](../Page/史公祠.md "wikilink")，孙绍陶、胡滋甫、高治平、[朱敬吾](../Page/朱敬吾.md "wikilink")、张子谦、刘少椿、胡斗东和上海琴人[查阜西](../Page/查阜西.md "wikilink")、[仇淼之](../Page/仇淼之.md "wikilink")、[彭祉卿亦參與雅集](../Page/彭祉卿.md "wikilink")。孙绍陶傳张子谦、刘少椿、翟小坡、胡斗东、程孔阶、张伯儒、朱敬吾、仇淼之、吴小仙、施起之、林蕴如、武若渔等；张子谦曾任上海民族乐团演奏员，刘少椿曾任[南京艺术学院古琴教师](../Page/南京艺术学院.md "wikilink")。

从广陵琴派的传承沿革来看，自徐常遇1686年著《澄鉴堂琴谱》开始到1937年7月7日抗日战争爆发为止，据不完全统计，广陵琴派共传10代，有弟子140余人；著有琴谱24部，其中有代表性的有《澄鉴堂琴谱》、《五知斋琴谱》、《自远堂琴谱》、《蕉庵琴谱》、《枯木禅琴谱》。

## 外部連結

  - “[《澄鉴堂琴谱》电子版](https://web.archive.org/web/20160828101936/https://www.qinzhijie.com/books/47)”

  - “[《五知斋琴谱》电子版](https://web.archive.org/web/20160828103625/https://www.qinzhijie.com/books/48)”

  - “[《自远堂琴谱》电子版](https://web.archive.org/web/20160828115009/https://www.qinzhijie.com/books/11)”

  - “[《蕉庵琴谱》电子版](https://web.archive.org/web/20160828102238/https://www.qinzhijie.com/books/74)”

  - “[《枯木禅琴谱》电子版](https://web.archive.org/web/20160828112915/https://www.qinzhijie.com/books/80)”

  - “[《蘭田館琴谱》电子版](https://web.archive.org/web/20160828120310/https://www.qinzhijie.com/books/52)”

  - “[《卧雲樓琴譜》电子版](https://web.archive.org/web/20160828111256/https://www.qinzhijie.com/books/92)”

  - “[《琴學叢書》电子版](https://web.archive.org/web/20160828121537/https://www.qinzhijie.com/books/86)”

  - “[《存古堂琴譜》电子版](https://web.archive.org/web/20160828110817/https://www.qinzhijie.com/books/49)”

## 参考文献

  - 《澄鉴堂琴谱》
  - 徐俊与李澄宇等人的师承关系参见《查阜西琴学文粹》第142—143页。
  - 刘少椿，《广陵琴学源流》。
  - 黄勉之与杨宗稷等人的师承关系参见《优美的旋律飘香的歌—江苏历代音乐家》第62页；杨宗稷与彭祉卿的师承关系参见《今虞琴刊》之《琴人问讯录》。

[Category:古琴流派](../Category/古琴流派.md "wikilink")
[Category:扬州文化](../Category/扬州文化.md "wikilink")