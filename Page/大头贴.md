是發源自[日本](../Page/日本.md "wikilink")、在多國流行的一种[攝影](../Page/攝影.md "wikilink")[机器](../Page/机器.md "wikilink")。

## 概要

大頭貼是1995年由[ATLUS與](../Page/ATLUS.md "wikilink")[Sega公司合作的](../Page/Sega.md "wikilink")「Print
Club」自動快照機改裝後誕生，是一種功能十分多樣化的個人快照機器，使用者除了可以在狹小的空間中與友人合照外，還可自由在照片中加入[編框](../Page/編框.md "wikilink")、[花纹](../Page/花纹.md "wikilink")、[文字及各式各樣的圖案](../Page/文字.md "wikilink")。在按鈕[攝影與簡單的影像編輯後](../Page/攝影.md "wikilink")，照片可即時輸出為大小不一的組圖。[相紙後均有背膠](../Page/相紙.md "wikilink")，方便使用者黏貼在個人用品上。

剛推出的大頭貼機器只有[螢光燈的設計](../Page/螢光燈.md "wikilink")，後來為求皮膚看起來白皙，開始出現[閃光燈](../Page/閃光燈.md "wikilink")，同時也開始有可容納三至四人的空間設計，一些大型大頭貼專門店還免費出租[Cosplay服裝](../Page/Cosplay.md "wikilink")。虽稱为大头贴、貼紙相，但照相范围并不局限于头部，也不限於可以當作[貼紙張貼的照片](../Page/貼紙.md "wikilink")，亦是其他同類機器的照片，如照片卡等的統稱。

最早大頭貼文化是少女同儕團體間藉交換大頭貼，證明友情與親密度的活動，也有許多從事[援助交際的女孩們會在](../Page/援助交際.md "wikilink")[電話亭黏貼附上電話號碼的大頭貼作為宣傳](../Page/電話亭.md "wikilink")，後來成為情侶間的約會項目之一。約在1997年起，大頭貼文化開始流行於[日本女高中生之間](../Page/日本女高中生.md "wikilink")。2000年之後，大頭貼機器進軍[歐洲](../Page/歐洲.md "wikilink")，在[巴黎](../Page/巴黎.md "wikilink")、[倫敦等都市都受到少女喜愛](../Page/倫敦.md "wikilink")。目前在[中国大陆地区也相当普遍](../Page/中国大陆.md "wikilink")，可以在很多大街小巷找到其踪影。

在[台灣有大頭貼服務大多出現在鬧區](../Page/台灣.md "wikilink")，如西門町、東區等。另也有台灣廠商推出沖印亭[立可得](../Page/立可得.md "wikilink")，在全家超商、萊爾富、家樂福等提供機器，除可以拍大頭貼之外，也能透過[記憶卡](../Page/記憶卡.md "wikilink")，網路服務[Facebook](../Page/Facebook.md "wikilink")、[Picasa列印相片與拍摄](../Page/Picasa.md "wikilink")[證件照](../Page/證件照.md "wikilink")。

## 参考文献

  - 四方田犬彥。《「かわいい」論》。東京：[筑摩書房](../Page/筑摩書房.md "wikilink")。2006。

## 外部連結

  - [立可得](http://www.likoda.com.tw/)

[en:Photo
booth\#Purikura](../Page/en:Photo_booth#Purikura.md "wikilink")

[Category:日本流行文化](../Category/日本流行文化.md "wikilink")
[Category:摄影](../Category/摄影.md "wikilink")
[Category:摄影器材](../Category/摄影器材.md "wikilink")
[Category:自動販賣機](../Category/自動販賣機.md "wikilink")