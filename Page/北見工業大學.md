<div style="float: right; margin: 0 0 1em 2em; width: 26em; text-align: right; font-size: 0.86em; line-height: normal;">

<div style="border: 1px solid #ccd2d9; background: #f0f6fa; text-align: left; padding: 0.5em 1em; text-align: center;">

<div style="text-align: center;">

<big>**北見工業大学**</big>

</div>

[北見工業大学正門.jpg](https://zh.wikipedia.org/wiki/File:北見工業大学正門.jpg "fig:北見工業大学正門.jpg")

<table>
<thead>
<tr class="header">
<th><p>大学設立</p></th>
<th><p>1966年：<strong>北見工業大学</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>創立</p></td>
<td><p>1960年：<strong>北見工業短期大学</strong></p></td>
</tr>
<tr class="even">
<td><p>学校種別</p></td>
<td><p><strong><a href="../Page/国立大学.md" title="wikilink">国立</a></strong></p></td>
</tr>
<tr class="odd">
<td><p>設置者</p></td>
<td><p><strong><a href="../Page/国立大学法人.md" title="wikilink">国立大学法人北見工業大学</a></strong></p></td>
</tr>
<tr class="even">
<td><p>学長</p></td>
<td><p>常本　秀幸</p></td>
</tr>
<tr class="odd">
<td><p>所在地</p></td>
<td><p><span style="color: #FF0000;">〒</span>090-8507<br />
<a href="../Page/北海道.md" title="wikilink">北海道</a><a href="../Page/北見市.md" title="wikilink">北見市公園町</a>165番地</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/学部.md" title="wikilink">学部</a></p></td>
<td><p><a href="../Page/工学部.md" title="wikilink">工学部</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/大学院.md" title="wikilink">大学院</a></p></td>
<td><p><a href="../Page/工学.md" title="wikilink">工学</a><a href="../Page/研究科.md" title="wikilink">研究科</a></p></td>
</tr>
<tr class="even">
<td><p>附属施設</p></td>
<td><p>┏情報処理センター<br />
┣地域共同研究センター<br />
┣機器分析センター<br />
┣国際交流センター<br />
┣保健管理センター<br />
┣サテライト・ベンチャー・ビジネス・<br />
┃ラボラトリー (SVBL)<br />
┣地域連携・研究戦略室<br />
┗未利用エネルギー研究センター</p></td>
</tr>
<tr class="odd">
<td><p>ウェブサイト</p></td>
<td><p><a href="http://www.kitami-it.ac.jp/">北見工業大学</a></p></td>
</tr>
</tbody>
</table>

</div>

</div>

**北见工业大学**（きたみこうぎょうだいがく、英文:Kitami Institute of
Technology），略称**北見大**,是位于[北海道](../Page/北海道.md "wikilink")[北见市的](../Page/北见市.md "wikilink")[日本国立大学](../Page/日本.md "wikilink")。前身是1960年设立的北见工业短期大学，1966年更名为北见工业大学。为日本国立大学中少见的仅设工学部的大学。亦是日本86所国立大学中位置最北的一所。

## 学部・学科

  - 工学部
  - 化学系统工学科
  - 电气电子天子工学科
  - 情報系统工学科
  - 机械系统工学科
  - 机能材料工学科
  - 土木开发工学科

## 付属施設

  - 机械分析中心
  - 信息处理中心
  - 地域共同研究中心
  - 未利用能源研究中心

## 外部链接

  - [北见工业大学主页](http://www.kitami-it.ac.jp/)

[Category:北海道的大學](../Category/北海道的大學.md "wikilink")
[Category:日本的國立大學](../Category/日本的國立大學.md "wikilink")
[Category:1966年創建的教育機構](../Category/1966年創建的教育機構.md "wikilink")
[Category:北見市](../Category/北見市.md "wikilink")