**馬里亞納海溝**（或稱馬里亞納群島海溝）為目前所知[最深的](../Page/地理之最列表.md "wikilink")[海溝](../Page/海溝.md "wikilink")。該海溝地處北[太平洋西方](../Page/太平洋.md "wikilink")[海床](../Page/海床.md "wikilink")，位於，即近[關島的](../Page/關島.md "wikilink")[馬里亞納群島東方](../Page/馬里亞納群島.md "wikilink")。此海溝為兩[板塊輻輳之俯衝帶](../Page/板塊.md "wikilink")，[太平洋板塊於此俯衝於](../Page/太平洋板塊.md "wikilink")[菲律賓海板塊](../Page/菲律賓海板塊.md "wikilink")（或细分出的[马里亚纳板块](../Page/马里亚纳板块.md "wikilink")）之下。海溝底部於[海平面下之深度](../Page/海平面.md "wikilink")，遠勝于[珠穆朗瑪峰海平面上的高度](../Page/珠穆朗瑪峰.md "wikilink")。

海溝最大深度為海平面下11,034[公尺](../Page/公尺.md "wikilink")（35,798[英呎](../Page/英呎.md "wikilink")）。若參考其[緯度與](../Page/緯度.md "wikilink")[地球之](../Page/地球.md "wikilink")[赤道隆突](../Page/赤道隆突.md "wikilink")，此深度位置距[地心為](../Page/地心.md "wikilink")6,366.4[公里](../Page/公里.md "wikilink")。相較之下，[北冰洋深](../Page/北冰洋.md "wikilink")4至4.5公里，其海床距地心6,352.8公里，僅比馬里亞納海溝距地心近13.6公里。

馬里亞納海溝底部，[水壓為](../Page/壓力.md "wikilink")1086[巴](../Page/巴.md "wikilink")，即108.6百萬[帕斯卡](../Page/帕斯卡.md "wikilink")（MPa）、每平方英寸15,751磅（PSI）或1071.8[標準大氣壓](../Page/標準大氣壓.md "wikilink")。

## 探索歷史

1951年[英國皇家海軍航具](../Page/英國皇家海軍.md "wikilink")[挑戰者二號首度測量海溝](../Page/挑戰者二號.md "wikilink")，其最深處便以[挑戰者深淵為名](../Page/挑戰者深淵.md "wikilink")。挑戰者二號以[回波定位方式於](../Page/回波定位.md "wikilink")，量測出5,960[噚](../Page/噚.md "wikilink")（相當於10,900公尺）的深度。这种方式是用探針通過漸層深度，反覆發送[聲波](../Page/声波.md "wikilink")，再用[耳機捕捉回波](../Page/耳機.md "wikilink")，根据回波器的速度，结合手持[碼錶計時完成](../Page/碼錶.md "wikilink")。因此正式提報新的最深距離時，根据均认可的謹慎作法，將所測深度減去一個尺度（20噚），從而得出5,940噚（10,863公尺）的數據。

1957年[俄羅斯航具](../Page/俄羅斯.md "wikilink")「維塔茲號」（Vityaz）回報測得11,034公尺（36,201呎）深度，將該處命名為「馬里亞納深凹」；但此數據從未再測得，故不以為準確。1962年機動載具「史賓塞·傅樂頓·拜爾德號」（Spencer
F.
Baird）測得最深10,915公尺（35,810呎）。1984年[日本人將高能專業探測航具](../Page/日本人.md "wikilink")「拓洋號」（Takuyo）送入馬里亞納海溝，以多窄波束回波定位儀蒐集資料，測得最大深度為11,040.4公尺\[1\]（也記錄為10,920±10公尺\[2\]）。國外一般則採用深10,924公尺，如[美國中央情報局出版的](../Page/美國中央情報局.md "wikilink")《[世界概况](../Page/世界概况.md "wikilink")》（*The
World
Factbook*）。最為精確的紀錄則由[日本探測艇](../Page/日本.md "wikilink")[海溝號](../Page/海溝號.md "wikilink")（Kaiko）於1995年3月24日測得深度10,911公尺（35,798呎）\[3\]。

1960年1月23日午後1點06分，[美國海軍中尉](../Page/美國海軍.md "wikilink")[唐纳德·沃尔什](../Page/唐纳德·沃尔什.md "wikilink")（Don
Walsh）與[雅克·皮卡德](../Page/雅克·皮卡德.md "wikilink")（Jacques
Piccard）駕駛[深潛艇](../Page/深潛艇.md "wikilink")[的里雅斯特号](../Page/的里雅斯特号.md "wikilink")（Trieste），以鐵球壓艙，以[汽油為浮槽](../Page/汽油.md "wikilink")，第一次史無前例的潛航抵達海溝底部。艇上系統顯示深度為37,800呎（11,521公尺），但後修正為35,813呎（10,916公尺）。華許與皮卡於海溝底部，驚訝的發現近1呎長（30公分）的[鰈魚或](../Page/鰈魚.md "wikilink")[比目魚及](../Page/比目魚.md "wikilink")[蝦](../Page/蝦.md "wikilink")。依皮卡所述：「海溝底部看起來光亮清澈，是片堅硬的[矽藻泥荒地](../Page/矽藻.md "wikilink")。」

当地时间2012年3月26日7点52分，[加拿大导演](../Page/加拿大.md "wikilink")[詹姆斯·卡麥隆驾驶单人深潜艇](../Page/詹姆斯·卡麥隆.md "wikilink")[深海挑战者号](../Page/深海挑战者号.md "wikilink")（Deepsea
Challenger）下潜到了10,898公尺的挑战者海渊底部\[4\]。卡梅隆是抵达海沟底部的第三个人，也是单独下潜的第一人。卡梅隆此次潜水除了为科学研究搜集样本之外，还拍摄了一些[照片和](../Page/照片.md "wikilink")[影片](../Page/影片.md "wikilink")。

## 參見

  - [海溝列表](../Page/海溝列表.md "wikilink")
  - [挑戰者深淵](../Page/挑戰者深淵.md "wikilink")

## 参考文献

[Category:太平洋](../Category/太平洋.md "wikilink")
[Category:海溝](../Category/海溝.md "wikilink")
[Category:地理之最](../Category/地理之最.md "wikilink")
[Category:海洋保護區](../Category/海洋保護區.md "wikilink")

1.
2.
3.
4.