**圖里亞龍類**（Turiasauria）是[蜥腳下目](../Page/蜥腳下目.md "wikilink")[恐龍的一個未定位](../Page/恐龍.md "wikilink")[演化支](../Page/演化支.md "wikilink")，是以[圖里亞龍作為名稱來源](../Page/圖里亞龍.md "wikilink")，圖里亞龍是種發現於[歐洲西南部的巨大](../Page/歐洲.md "wikilink")[真蜥腳類恐龍](../Page/真蜥腳類.md "wikilink")。這個演化支還包括另外兩屬：[露絲娜龍](../Page/露絲娜龍.md "wikilink")、[加爾瓦龍](../Page/加爾瓦龍.md "wikilink")。圖里亞龍的三個屬都發現於[西班牙的Villar](../Page/西班牙.md "wikilink")
del
Arzobispo組，該地層年代為晚[侏儸紀](../Page/侏儸紀.md "wikilink")[提通階到早](../Page/提通階.md "wikilink")[白堊紀](../Page/白堊紀.md "wikilink")[貝利阿斯階](../Page/貝利阿斯階.md "wikilink")。圖里亞龍類的定義為：[真蜥腳類之中](../Page/真蜥腳類.md "wikilink")，親緣關係接近[圖里亞龍](../Page/圖里亞龍.md "wikilink")，而離[薩爾塔龍較遠的所有物種](../Page/薩爾塔龍.md "wikilink")。

在2007年，一個針對33個物種與309個特徵的[親緣分支分類法研究](../Page/親緣分支分類法.md "wikilink")，顯示出圖里亞龍類不屬於[新蜥腳類](../Page/新蜥腳類.md "wikilink")，並且形成一個[單系群](../Page/單系群.md "wikilink")。這個演化支可從以下特徵來辨別：擁有垂直[神經棘](../Page/神經棘.md "wikilink")、[背椎缺乏脊前板](../Page/背椎.md "wikilink")（Prespinal
laminae）與脊後板（Postspinal
laminae）、[肩胛骨缺乏肩峰突](../Page/肩胛骨.md "wikilink")、[肱骨擁有明顯的](../Page/肱骨.md "wikilink")[三角嵴](../Page/三角嵴.md "wikilink")（Deltopectoral
crest）、肱骨近端的中度偏轉、以及[尺骨尾端的明顯垂直棱線](../Page/尺骨.md "wikilink")。

目前仍不清楚圖里亞龍類的生物地理與生物地層範圍，但Royo-Torres等人假設圖里亞龍類演化支可能代表一個，時間早於[提通階](../Page/提通階.md "wikilink")，起源於[歐洲的蜥腳下目](../Page/歐洲.md "wikilink")[演化輻射演化支](../Page/演化輻射.md "wikilink")。到目前為止，圖里亞龍類的未承認成員包括：[新牙龍](../Page/新牙龍.md "wikilink")、[央齒龍](../Page/央齒龍.md "wikilink")，牠們的類牙齒似[圖里亞龍](../Page/圖里亞龍.md "wikilink")，被發現於[侏儸紀的](../Page/侏儸紀.md "wikilink")[葡萄牙](../Page/葡萄牙.md "wikilink")、[法國](../Page/法國.md "wikilink")、以及[英格蘭](../Page/英格蘭.md "wikilink")。

圖里亞龍類証實巨大體型的演化，並不僅限於新蜥腳類例如[梁龍與](../Page/梁龍.md "wikilink")[泰坦巨龍](../Page/泰坦巨龍.md "wikilink")，而至少有一個支系曾獨立演化出巨大體型。

在2009年，José
Barco提出[露絲娜龍與](../Page/露絲娜龍.md "wikilink")[加爾瓦龍都不是圖里亞龍類](../Page/加爾瓦龍.md "wikilink")。同年，Francisco
Gascó、Royo-Torres等人分別提出圖里亞龍類是有效[演化支](../Page/演化支.md "wikilink")。

## 參考資料

  - Royo-Torres, R., Cobos, A., and Alcalá, L. (2006). "A Giant European
    Dinosaur and a New Sauropod Clade." **Science**, **314**: 1925-1927.
  - Barco, J. L., Canudo, J. L., Cuenca-Bescós, G. & Ruíz-Omeñaca, J.
    I., (2005): Un nuevo dinosaurio saurópodo, *Galvesaurus herreroi*
    gen. nov., sp. nov., del tránsito Jurásico-Cretácico en Galve
    (Teruel, NE de España). *Naturaleza Aragonesa*: Vol. 15, pp 4-17
  - Casanovas, M. L., Santafe, J. V. & Sanz, J. L.(2001): *Losillasaurus
    giganteus*, un nuevo saurópodo del tránsito Jurásico-Cretácico de la
    Cuenca de “Los Serranos" (Valencia, España). *Paleontologia i
    Evolució* 32-33:99-122
  - José Luis Barco Rodríguez, *Sistemática e implicaciones
    filogenéticas y paleobiogeográficas del saurópodo* Galvesaurus
    herreroi *(Formación Villar del Arzobispo, Galve, España)*, 2009,
    Universidad de Zaragoza
  - Royo-Torres, R., Cobos, A., Aberasturi , A., Espilez, E., Fierro,
    I., González, A., Luque, L., Mampel, L. y Alcalá , L. (2009): High
    European sauropod dinosaur diversity during Jurassic-Cretaceous
    transition in Riodeva (Teruel, Spain). *Palaeontology* 52(5),
    1009-1027.
  - Gascó, F (2009): Sistemática y anatomía funcional de *Losillasaurus
    giganteus* Casanovas, Santafé & Sanz, 2001 (Turiasauria, Sauropoda).
    Universidad Autónoma de Madrid.

## 外部連結

  - [Las huellas de Dinosaurios del Levante
    Ibérico](http://www.dinoslevante.com)

[圖里亞龍類](../Category/圖里亞龍類.md "wikilink")