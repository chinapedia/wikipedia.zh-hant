**金牛镇**，旧名复兴乡，为[中国](../Page/中华人民共和国.md "wikilink")[四川省](../Page/四川省.md "wikilink")[广安市](../Page/广安市.md "wikilink")[武胜县下辖](../Page/武胜县.md "wikilink")[镇](../Page/镇.md "wikilink")，位于[武胜县西北部](../Page/武胜县.md "wikilink")，与[金光](../Page/金光乡.md "wikilink")、[万善](../Page/万善镇.md "wikilink")、[宝箴塞](../Page/宝箴塞乡.md "wikilink")、[胜利](../Page/胜利镇.md "wikilink")、[烈面等乡镇交界](../Page/烈面镇.md "wikilink")，[复兴河川流而过](../Page/复兴河.md "wikilink")。全镇面积30.7平方公里，人口24102人。镇人民政府驻沙牛滩，距县城20公里\[1\]。

## 历史沿革

唐宋属[汉初县沙溪镇](../Page/汉初县.md "wikilink")；[明属沙溪里熊家场](../Page/明.md "wikilink")；[清末更名为复兴场](../Page/清.md "wikilink")；[民国](../Page/民国.md "wikilink")2年(1913年)废场设复兴乡；1958年乡治所由老街村迁沙牛滩；1981年8月更名为金牛镇。

## 行政区划

辖沙牛滩、双桥、红旗、石院墙、桅子湾、雷波寨、青连、三合、双朝门、望乡坪、吊嘴岩、敖滩、金星、龙王桥、桂花湾、辛寨子、老街等17个村。

## 学校

金牛初中、金牛小学和村小18所。

## 名人

  - [唐毅](../Page/唐毅.md "wikilink")，曾任四川省警察局局长、陪都重庆市警察局局长、[内政部警察总署副署长](../Page/内政部警察总署.md "wikilink")、[第一届国民大会代表](../Page/第一届国民大会.md "wikilink")。
  - [唐化治](../Page/唐化治.md "wikilink")，唐毅之弟，[中央陆军军官学校毕业](../Page/中央陆军军官学校.md "wikilink")，曾任[暂编第九师师长](../Page/暂编第九师.md "wikilink")。

## 参考资料

[镇](../Category/武胜县行政区划.md "wikilink")
[武胜县](../Category/广安建制镇.md "wikilink")

1.