**卫州**，[中国](../Page/中国.md "wikilink")[南北朝时设置的](../Page/南北朝.md "wikilink")[州](../Page/州.md "wikilink")，在今[河南省境](../Page/河南省.md "wikilink")。

[西晋永嘉四年](../Page/西晋.md "wikilink")（310年）为[漢趙](../Page/漢趙.md "wikilink")[刘渊所攻陷汲县](../Page/刘渊.md "wikilink")，于汲县设立[殷州及卫州](../Page/殷州_\(漢趙\).md "wikilink")。[汉昌元年](../Page/汉昌.md "wikilink")（318年）[石勒得此地](../Page/石勒.md "wikilink")，废卫州、殷州等州，复立汲郡，治汲县，领汲、朝歌、共、林虑、获嘉、脩武六县。

[北周](../Page/北周.md "wikilink")[宣政时置卫州](../Page/宣政.md "wikilink")。[隋朝](../Page/隋朝.md "wikilink")[大业年间](../Page/大业_\(年号\).md "wikilink")，天下州改[郡](../Page/郡.md "wikilink")，改为[汲郡](../Page/汲郡.md "wikilink")。[唐朝](../Page/唐朝.md "wikilink")[武德初](../Page/武德.md "wikilink")，天下郡改州，复为卫州。武德四年（621年），[义州废](../Page/义州_\(隋朝\).md "wikilink")，[汲县改属卫州](../Page/汲县.md "wikilink")。[贞观元年](../Page/貞觀_\(唐朝\).md "wikilink")（627年），[治所由](../Page/治所.md "wikilink")[卫县迁移至汲县](../Page/卫县.md "wikilink")。贞观十七年（643年），[清淇县并入卫县](../Page/清淇县.md "wikilink")，同年，[黎阳县改属卫州](../Page/黎阳县_\(西汉\).md "wikilink")。土贡：绫、绢、绵、胡粉。户四万八千五十六，口二十八万四千六百三十。下领五县：汲县、卫县、[共城县](../Page/共城县.md "wikilink")、[新乡县](../Page/新乡县.md "wikilink")、黎阳县。

[北宋](../Page/北宋.md "wikilink")[天圣四年](../Page/天圣.md "wikilink")（1026年），所辖卫县隶[安利军](../Page/安利军.md "wikilink")。[熙宁三年](../Page/熙宁.md "wikilink")（1070年），安利军废为县，属卫州。[元祐元年](../Page/元祐_\(年號\).md "wikilink")（1086年），复置安利军，后为[浚州](../Page/浚州.md "wikilink")。[崇宁时](../Page/崇宁.md "wikilink")，卫州户三万三千二百四，口四万六千三百六十五。贡绢、绵。终北宋一朝，卫州下领一[监](../Page/监.md "wikilink")：[黎阳监](../Page/黎阳监.md "wikilink")，四县：汲县、新乡县、[获嘉县](../Page/获嘉县.md "wikilink")、共城县。

[元朝](../Page/元朝.md "wikilink")[中统元年](../Page/中統_\(年號\).md "wikilink")（1260年），升为[卫辉路](../Page/衛輝路.md "wikilink")\[1\]，[明朝时](../Page/明朝.md "wikilink")，为[卫辉府](../Page/卫辉府.md "wikilink")。

## 注释

## 参考资料

  - 《[新唐书](../Page/新唐书.md "wikilink")·志第二十九·地理三》
  - 《[宋史](../Page/宋史.md "wikilink")·志第三十九·地理二》

[Category:北周的州](../Category/北周的州.md "wikilink")
[Category:隋朝的州](../Category/隋朝的州.md "wikilink")
[Category:唐朝的州](../Category/唐朝的州.md "wikilink")
[Category:五代十国的州](../Category/五代十国的州.md "wikilink")
[Category:北宋的州](../Category/北宋的州.md "wikilink")
[Category:金朝的州](../Category/金朝的州.md "wikilink")
[Category:元朝的州](../Category/元朝的州.md "wikilink")
[Category:河南的州](../Category/河南的州.md "wikilink")
[Category:新乡行政区划史](../Category/新乡行政区划史.md "wikilink")
[Category:鹤壁行政区划史](../Category/鹤壁行政区划史.md "wikilink")
[Category:578年建立的行政区划](../Category/578年建立的行政区划.md "wikilink")
[Category:1260年废除的行政区划](../Category/1260年废除的行政区划.md "wikilink")

1.  《[元史](../Page/元史.md "wikilink")·志第十·地理一》怀庆路......卫辉路，下。唐义州，又为卫州，又为汲郡。金改河平军。元中统元年，升卫辉路总管府，设录事司。户二万二千一百一十九，口一十二万七千二百四十七。领司一、县四、州二......