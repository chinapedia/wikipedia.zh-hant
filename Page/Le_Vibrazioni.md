**Le Vibrazioni**是一支1999年成立於[義大利](../Page/義大利.md "wikilink")
[米蘭的](../Page/米蘭.md "wikilink")[搖滾](../Page/搖滾.md "wikilink")[樂團](../Page/樂團.md "wikilink")。

## 成員

  - [法蘭西斯可·沙西那](../Page/法蘭西斯可·沙西那.md "wikilink")：[主唱](../Page/歌手.md "wikilink")、[吉他手](../Page/吉他手.md "wikilink")
  - [史蒂芬諾·維德里](../Page/史蒂芬諾·維德里.md "wikilink")：吉他手、[鍵盤手](../Page/鍵盤樂器.md "wikilink")、[西塔琴](../Page/西塔琴.md "wikilink")
  - [馬可·卡斯特拉尼](../Page/馬可·卡斯特拉尼.md "wikilink")：[貝斯手](../Page/貝斯手.md "wikilink")
  - [亞歷山卓·戴伊達](../Page/亞歷山卓·戴伊達.md "wikilink")：[鼓手](../Page/鼓手.md "wikilink")

## 音樂作品

### 專輯

  - Le Vibrazioni（2003年）
  - Le Vibrazioni II（2005年）
  - Officine Meccaniche（2006年）

### 單曲

  - 2003年－Dedicato a te
  - 2003年－In Una Notte D'estate
  - 2003年－E se ne va
  - 2003年－Vieni Da Me
  - 2004年－Sono Più Sereno
  - 2005年－Raggio di Sole
  - 2005年－Ovunque Andrò
  - 2005年－Angelica
  - 2005年－Aspettando
  - 2005年－Ogni Giorno Ad Ogni Ora
  - 2006年－Fermi Senza Forma (lanciato solo in interet sul sito di
    MtvOverdrive)
  - 2006年－Se
  - 2007年－Portami Via
  - 2007年－Dimmi
  - 2007年－Drammaturgia

## DVD

  - Live all'Alcatraz（2004年）
  - Le Vibrazioni II（雙碟）（2005年）

## 外部連結

  - [Le Vibrazioni 官方網站](http://www.vibraland.it/)

[Category:搖滾樂團](../Category/搖滾樂團.md "wikilink")
[Category:義大利樂團](../Category/義大利樂團.md "wikilink")
[Category:流行音樂](../Category/流行音樂.md "wikilink")