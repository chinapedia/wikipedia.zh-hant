**四氯化钒**（[化学式](../Page/化学式.md "wikilink")：[V](../Page/钒.md "wikilink")[Cl](../Page/氯.md "wikilink")<sub>4</sub>）是[钒](../Page/钒.md "wikilink")(IV)的[氯化物](../Page/氯化物.md "wikilink")，为亮红色液体，可用于制备其他很多[钒化合物](../Category/钒化合物.md "wikilink")，包括[氯化二茂钒](../Page/氯化二茂钒.md "wikilink")。它与许多[配体形成](../Page/配体_\(化学\).md "wikilink")[加合物](../Page/加合物.md "wikilink")，如与[四氢呋喃反应生成VCl](../Page/四氢呋喃.md "wikilink")<sub>4</sub>([THF](../Page/THF.md "wikilink"))<sub>2</sub>。

四氯化钒呈[顺磁性](../Page/顺磁性.md "wikilink")，它比[反磁性的](../Page/反磁性.md "wikilink")[四氯化钛多一个价电子](../Page/四氯化钛.md "wikilink")。它是少数室温下为液体且为顺磁性的[化合物之一](../Page/化合物.md "wikilink")。

与同族的[VF<sub>5</sub>](../Page/五氟化钒.md "wikilink")、[NbCl<sub>5</sub>和](../Page/五氯化铌.md "wikilink")[TaCl<sub>5</sub>不同](../Page/五氯化钽.md "wikilink")，VCl<sub>4</sub>可由金属钒氯化制备，氯气的[氧化性不足以将钒氧化至VCl](../Page/氧化性.md "wikilink")<sub>5</sub>。此外，四氯化钒在沸点下分解，生成[三氯化钒和](../Page/三氯化钒.md "wikilink")[氯气](../Page/氯气.md "wikilink")：

  -
    2 VCl<sub>4</sub> → 2 VCl<sub>3</sub> + Cl<sub>2</sub>

## 反应及应用

  - [有机合成中](../Page/有机合成.md "wikilink")，VCl<sub>4</sub>可使[酚](../Page/酚.md "wikilink")[偶联](../Page/偶联.md "wikilink")，比如与[苯酚反应生成](../Page/苯酚.md "wikilink")4,4'-联苯酚：\[1\]

<!-- end list -->

  -

      -
        2 C<sub>6</sub>H<sub>5</sub>OH + 2 VCl<sub>4</sub> →
        HOC<sub>6</sub>H<sub>4</sub>-C<sub>6</sub>H<sub>4</sub>OH + 2
        [VCl<sub>3</sub>](../Page/三氯化钒.md "wikilink") + 2 HCl

<!-- end list -->

  - 橡胶工业中，VCl<sub>4</sub>可以催化[烯烃的](../Page/烯烃.md "wikilink")[聚合反应](../Page/聚合反应.md "wikilink")，机理与[齐格勒-纳塔催化剂类似](../Page/齐格勒-纳塔催化剂.md "wikilink")。
  - VCl<sub>4</sub>与[HBr反应生成](../Page/溴化氢.md "wikilink")[VBr<sub>3</sub>](../Page/三溴化钒.md "wikilink")。反应经由中间产物VBr<sub>4</sub>，室温下分解放出Br<sub>2</sub>。<ref>Calderazzo,
    F.; Maichle-Mossmer, C.; Pampaloni, G. and Strähle, J.,
    "Low-temperature Syntheses of Vanadium(III) and Molybdenum(IV)
    Bromides by Halide Exchange", Journal of the Chemical Society,
    Dalton Transactions, 1993, pages 655-8.

</ref>

  -

      -
        2 VCl<sub>4</sub> + 8 HBr → 2 VBr<sub>3</sub> + 8 HCl +
        Br<sub>2</sub>

## 安全

室温下，VCl<sub>4</sub>是可挥发且有较强[氧化性的液体](../Page/氧化性.md "wikilink")，遇水迅速[水解生成](../Page/水解.md "wikilink")[HCl](../Page/氯化氢.md "wikilink")。

## 参考资料

[Category:四价钒化合物](../Category/四价钒化合物.md "wikilink")
[Category:氯化物](../Category/氯化物.md "wikilink")
[Category:金属卤化物](../Category/金属卤化物.md "wikilink")
[Category:有机化学试剂](../Category/有机化学试剂.md "wikilink")

1.  M. K. O’Brien, B. Vanasse, “Vanadium(IV) Chloride” in Encyclopedia
    of Reagents for Organic Synthesis (Ed: L. Paquette) 2004, J. Wiley &
    Sons, New York.