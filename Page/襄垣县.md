**襄垣县**是[中国](../Page/中国.md "wikilink")[山西省](../Page/山西省.md "wikilink")[长治市所辖的一个](../Page/长治市.md "wikilink")[县](../Page/县.md "wikilink")。总面积为1158平方公里，2010年常住人口270216人。

## 历史

周贞定王十四年（前455年），赵襄子筑城于甘水之北，取名襄垣。\[1\]

## 地理

境内以山地丘陵为主，主要河流有[浊漳河](../Page/浊漳河.md "wikilink")。

## 行政区划

下辖8个[镇](../Page/行政建制镇.md "wikilink")、3个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

## 交通

  - [太焦铁路](../Page/太焦铁路.md "wikilink")
  - [太长高速公路](../Page/太长高速公路.md "wikilink")（[二广高速公路](../Page/二广高速公路.md "wikilink")）
  - [208国道](../Page/208国道.md "wikilink")

## 人口

2010年第六次全国人口普查襄垣县常住人口270216人。\[2\]

## 经济

2012年GDP226.70亿元。

## 风景名胜

  - [全国重点文物保护单位](../Page/全国重点文物保护单位.md "wikilink")：[灵泽王庙](../Page/灵泽王庙.md "wikilink")、[昭泽王庙](../Page/昭泽王庙.md "wikilink")、[襄垣文庙](../Page/襄垣文庙.md "wikilink")、[襄垣永惠桥](../Page/襄垣永惠桥.md "wikilink")、[襄垣昭泽王庙](../Page/襄垣昭泽王庙.md "wikilink")、[襄垣五龙庙](../Page/襄垣五龙庙.md "wikilink")
  - [山西省文物保护单位](../Page/山西省文物保护单位.md "wikilink")：[仙堂山古建筑群](../Page/仙堂山古建筑群.md "wikilink")、[石勒城遗址](../Page/石勒城遗址.md "wikilink")
  - [襄垣县文物保护单位](../Page/襄垣县文物保护单位.md "wikilink")

## 名人

## 参考文献

[\*](../Category/襄垣县.md "wikilink")
[长治](../Category/山西省县份.md "wikilink")

1.  [襄垣历史](http://www.xiangyuan.gov.cn/index.php?id=112)
2.  [长治市2010年第六次全国人口普查主要数据公报](http://www.changzhi.gov.cn/info/news/2011/nry/203579.htm)