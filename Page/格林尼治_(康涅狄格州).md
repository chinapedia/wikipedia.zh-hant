[US-CT-Greenwich.png](https://zh.wikipedia.org/wiki/File:US-CT-Greenwich.png "fig:US-CT-Greenwich.png")
**格林尼治**（），是[美國](../Page/美國.md "wikilink")[康乃狄克州](../Page/康乃狄克州.md "wikilink")[費爾菲爾德縣的一個城鎮](../Page/費爾菲爾德縣_\(康乃狄克州\).md "wikilink")，位於該州最南部，面臨[長島海灣](../Page/長島海灣.md "wikilink")。格林尼治的面積共174.0[平方公里](../Page/平方公里.md "wikilink")，於2006年的[人口共](../Page/人口.md "wikilink")62,077人。\[1\]

格林尼治於1640年開埠，於1665年建鎮。

## 姐妹城市

  - [伊澤爾省](../Page/伊澤爾省.md "wikilink")[維埃納](../Page/維埃納.md "wikilink")\[2\]

## 参考文献

<div class="references-small">

<references />

</div>

[Category:康涅狄格州城市](../Category/康涅狄格州城市.md "wikilink")

1.  [Greenwich, Connecticut - Population Finder - American
    FactFinder](http://factfinder.census.gov/servlet/SAFFPopulation?_event=Search&geo_id=16000US0973000&_geoContext=01000US%7C04000US09%7C16000US0973000&_street=&_county=Greenwich+town&_cityTown=Greenwich+town&_state=04000US09&_zip=&_lang=en&_sse=on&ActiveGeoDiv=geoSelect&_useEV=&pctxt=fph&pgsl=160&_submenuId=population_0&ds_name=null&_ci_nbr=null&qr_name=&reg=%3Anull&_keyword=&_industry=)
2.  Greenspon, Zack. "Greenwich, Vienne Share Joie de Vivre", *The
    Greenwich Citizen*, 2007年3月23日。