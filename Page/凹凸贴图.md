[Bump-map-demo-full.png](https://zh.wikipedia.org/wiki/File:Bump-map-demo-full.png "fig:Bump-map-demo-full.png")。\]\]

**凹凸贴图**（bump
mapping），又稱為**凸凹纹理映射**、**皺面貼圖**，是一项[计算机图形学技术](../Page/计算机图形学.md "wikilink")，在这项技术中每个待[渲染的像素在计算照明之前都要加上一个从](../Page/渲染.md "wikilink")[高度图中找到的扰动](../Page/高度图.md "wikilink")。这样得到的结果表面表现更加丰富、细致，更加接近物体在自然界本身的模样。[法线贴图是一项常用的凹凸贴图技术](../Page/法线贴图.md "wikilink")，另外还有许多其它的实现技术，如[视差映射等等](../Page/视差映射.md "wikilink")。

## 具体实现

### BMEM技术

BMEM技术通过一张叫做高度图（Height map）的灰度图来储存每一点的高度信息然后直接由API处理。\[1\]

### 法线贴图法

[Bump_map_vs_isosurface2.png](https://zh.wikipedia.org/wiki/File:Bump_map_vs_isosurface2.png "fig:Bump_map_vs_isosurface2.png")

但事实上游戏编程员却通常并不喜欢使用BMEM技术，因为他执行速度慢，因此他们通常使用DP3技术：直接把高度图（Height
map）转换成一张法线图（Normal
Map），其图的RGB分别是原高度图该点的法线指向：Nx、Ny、Nz，这张图可由[Direct3D的专门函数帮助我们计算](../Page/Direct3D.md "wikilink")。最后在渲染的时候直接将该高度图的每个像素与光源的向量[点乘](../Page/数量积.md "wikilink")，即可得到表示每一点的明暗系数的图：根据高度图，越突出的地方，法线与光源夹角越小，该点的数值越大。接着将这张图乘到渲染线中即可，这样就使模型在背光的凹处有阴影而在面向光源处更亮的效果，这样的3D模型看起来就像真的凹凸不平一样！这些都可以直接在渲染流水线中由机器完成。

具体可以使用以下简单的语句来实现：

``` cpp

//将光源位置转换成ARGB
DWORD Vector2ARGB(D3DXVECTOR3 *v,float height)
{
DWORD r=(DWORD)(127.0f*v->x+128.0f);
DWORD g=(DWORD)(127.0f*v->y+128.0f);
DWORD b=(DWORD)(127.0f*v->z+128.0f);
DWORD a=(DWORD)(255.0f*height);
return((a<<24L)+(r<<16L)+(g<<8L)+b);
}
//生成法线图
D3DXComputeNormalMap(pNormalMap,pHeightMap,NULL,0,D3D_CHANNEL_RED,1.0f);//pHeightMap为原高度图的指针，pNormalMap为一张空纹理，用于存放法线图
//在渲染程序段中可以这样写：
DWORD F=Vector2ARGB(&light,0.0f); //light是单位化的光源向量
pD->SetRenderState(D3DRS_TEXTUREFACTOR,F);//pD是D3D的设备指针，这句将光源法线参数输入
pD->SetTexture(1,TEXTURE);//设置原纹理，如上面的球，如有需要可以贴上纹理样式
pD->SetTexture(0,normalmap);//使用上面生成好的法线图
pD->SetTextureStageState(0,D3DTSS_COLORARG1,D3DTA_TEXTURE);//设置“来源1”为法线图
pD->SetTextureStageState(0,D3DTSS_COLOROP,D3DTOP_DOTPRODUCT3);//将“来源1”（法线图）与“来源2”（光源法线）进行点乘
pD->SetTextureStageState(0,D3DTSS_COLORARG2,D3DTA_TFACTOR);//设置“来源2”为光线的光源法线参数
pD->SetTextureStageState(1,D3DTSS_COLORARG1,D3DTA_TEXTURE);//这步和下面几步将图片的原纹理加上
pD->SetTextureStageState(1,D3DTSS_COLOROP,D3DTOP_MODULATE);
pD->SetTextureStageState(1,D3DTSS_COLORARG2,D3DTA_CURRENT);
```

## 假凹凸贴图

[三维计算机图形程序员有时使用计算量较小的假凹凸贴图模拟凹凸贴图效果](../Page/三维计算机图形.md "wikilink")。其中一个方法是使用[纹素索引变化取代曲面法线变化](../Page/纹素.md "wikilink")，这种方法经常用于二维凹凸贴图。在[GeForce
2类型的图形加速硬件中就使用了这项技术](../Page/GeForce_2.md "wikilink")。

全屏的二维假凹凸贴图，可以很容易地用简单快速的渲染循环实现，在二十世纪九十年代的示范影像是一个非常普通的[视觉效果](../Page/demo_effect.md "wikilink")。

## 与位移映射之间的差别

[位移映射与凹凸贴图之间区别在例图中已经很明显地显现出来了](../Page/位移映射.md "wikilink")：在凹凸贴图中，只有法线进行了扰动，而几何体本身没有扰动，这样的结果就是人为改变只出现在物体的轮廓上，而球体本身仍然是原来的圆形。即凹凸贴图只是视觉上的改变，就像一个画得很透视的图片；而位移映射却真的将3D物体变得“凹凸不平”。

## 参考文献

  - Blinn, James F. "皱纹表面模拟", Computer Graphics, Vol. 12 (3), pp.
    286-292 SIGGRAPH-ACM (August 1978)

## 链接

  - <https://www.webcitation.org/5msQZJdyo?url=http://www.blacksmith-studios.dk/projects/downloads/bumpmapping_using_cg.php>
    Bump Mapping tutorial using CG and C++

## 参见

  - [纹理映射](../Page/纹理映射.md "wikilink")
  - [法向映射](../Page/法向映射.md "wikilink")
  - [视差映射](../Page/视差映射.md "wikilink")
  - [位移映射](../Page/位移映射.md "wikilink")
  - [浮雕纹理映射](../Page/浮雕纹理映射.md "wikilink")

[Category:三维计算机图形学](../Category/三维计算机图形学.md "wikilink")
[Category:演示效果](../Category/演示效果.md "wikilink")
[Category:纹理贴图](../Category/纹理贴图.md "wikilink")

1.