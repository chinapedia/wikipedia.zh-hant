[MAC_CulturalClub.JPG](https://zh.wikipedia.org/wiki/File:MAC_CulturalClub.JPG "fig:MAC_CulturalClub.JPG")
**文化會館**，位於[澳門](../Page/澳門.md "wikilink")[新馬路内的綜合展覽館](../Page/新馬路.md "wikilink")，內設[典當業展示館](../Page/典當業展示館.md "wikilink")、藝品廣場、[金庸圖書館](../Page/金庸圖書館.md "wikilink")、茶藝軒和文化展覽館。會館房產屬私人物業，管理則由[澳門文化局負責](../Page/澳門文化局.md "wikilink")。

## 建築物

[Taksheng.JPG](https://zh.wikipedia.org/wiki/File:Taksheng.JPG "fig:Taksheng.JPG")

會館建築物原為富商[高可寧之物業](../Page/高可寧.md "wikilink")，原為大型老[當鋪](../Page/當鋪.md "wikilink")「德成按」。建築物建於1917年(即[民國六年](../Page/民國.md "wikilink")），被評定為“具建築藝術價值之建築物”並收錄於《[澳門文物名錄](../Page/澳門文物名錄.md "wikilink")》。自1993年德成按結業後便被閒置，及至2000年業主有意將其出售及將建築物改建。[澳門政府主動與業主接洽](../Page/澳門政府.md "wikilink")，2002年被澳門文化局協定由政府出資進行修葺和整固。\[1\]修葺為現時模樣後，政府利用當樓底層及貨樓用作典當業展示館；當樓的其他層及相鄰的「富衡銀號」則交由業主使用。此修復項目獲評審為「2004[聯合國教科文組織亞太文化遺產保護獎](../Page/聯合國教科文組織.md "wikilink")」，嘉許在回復[文物原面貌和使人們了解建築物原功能的貢獻](../Page/文物.md "wikilink")。\[2\]

## 設施

文化會館內設典當業展示館、藝品廣場、金庸圖書館、茶藝軒和文化展覽館，以推動澳門文化。文化會館分為3層，地下及貨樓用作**典當業展示館**，展示澳門[典當業的發展](../Page/典當業.md "wikilink")。一樓前室為**精點廊**及**藝品廣場**，售賣澳門特色食品和精品。一樓後室闢為**金庸圖書館**，展示其多部名著。二樓前室為**水茶軒**，提供茶品。二樓後室的**文化展覽館**，用作舉辦文化藝術展覽。

## 開放時間

  - 星期一至星期五：上午10時30分至晚上8時
  - 星期六至星期日：上午10時30分至晚上9時

## 結業

因與業主商談續租沒有共識，文化會館於2014年1月15日結業\[3\], 而典當業展示館則如常運作\[4\]。

## 参考文献

<div class="references-small">

<references />

</div>

## 外部链接

  - [文化會館](http://www.culturalclub.net/)

[Category:澳門旅遊景點](../Category/澳門旅遊景點.md "wikilink")
[Category:澳门博物馆](../Category/澳门博物馆.md "wikilink")
[Category:澳門文化](../Category/澳門文化.md "wikilink")
[Category:唐樓](../Category/唐樓.md "wikilink")
[Category:文化會館](../Category/文化會館.md "wikilink")

1.  [澳門城市指南：文化會館及典當業展示館](http://www.cityguide.gov.mo/visit/detail.aspx?id=02051400000000000000)

2.  [德成按榮獲聯合國亞太文化遺產保護獎](http://www.macauheritage.net/Trends/TopicC.asp?id=89)
    ，26/10/2004，澳門文物網 - 文保動態
3.  [文化會館下月中結業](http://www.macaodaily.com/html/2013-12/24/content_864695.htm)
    , 24/12/2013, 澳門日報
4.  [典當業展示館如常運作](http://www.macaodaily.com/html/2013-12/24/content_864699.htm)
    , 24/12/2013, 澳門日報