**角川Sneaker文庫**（，），是[日本](../Page/日本.md "wikilink")[出版社](../Page/出版社.md "wikilink")[KADOKAWA旗下](../Page/KADOKAWA.md "wikilink")[角川書店出版的](../Page/角川書店.md "wikilink")[輕小說](../Page/輕小說.md "wikilink")[文庫系列](../Page/文庫.md "wikilink")。當初以「[角川文庫](../Page/角川文庫.md "wikilink")·青版」的名義成立，在1988年3月公開徵選後決定以「Sneaker文庫」的名義創刊。現在可稱為角川文庫的輕小說部門之一，並和[電擊文庫](../Page/電擊文庫.md "wikilink")、[富士見Fantasia文庫並列](../Page/富士見Fantasia文庫.md "wikilink")[角川集團的輕小說主力文庫](../Page/角川集團.md "wikilink")。

創刊目標為少年向小說，例如[富野由悠季的](../Page/富野由悠季.md "wikilink")《[機動戰士鋼彈](../Page/機動戰士鋼彈.md "wikilink")》系列，渡邊由自的以及富田祐弘的《[超時空要塞](../Page/超時空要塞.md "wikilink")》。後來，[水野良的](../Page/水野良.md "wikilink")《[羅德斯島戰記](../Page/羅德斯島戰記.md "wikilink")》開啟了奇幻小說的時代。

其中也有出版如[乙一的](../Page/乙一.md "wikilink")《寂寞的頻率》，瞄準一般讀者的作品出版。在2002年2月，成立了「角川Sneaker文庫<Sneaker Mystery俱樂部>」，出版只有少量插畫的作品，如[米澤穗信的](../Page/米澤穗信.md "wikilink")《[愚者的片尾](../Page/古籍研究社系列.md "wikilink")》、稻生平太郎的，以及乙一、[恩田陸等人的作品](../Page/恩田陸.md "wikilink")，卻因為對輕小說及一般小說讀者都缺乏吸引力而失敗，但依然成為此種出版方式先驅，後來[MediaWorks也依照此方式推出一系列的精裝書籍](../Page/MediaWorks.md "wikilink")。

在2007年，乙一的《只有你聽到》（）為Sneaker文庫首部電影化作品。

此外，也出版成人遊戲公司[Nitro+](../Page/Nitro+.md "wikilink")、[Alicesoft等公司作品的小說版本](../Page/Alicesoft.md "wikilink")。

## 作品一覽

  -
    由於未出版書籍居多，完整列表請見日語版條目。

### 加珈文化

  - [MAZE☆爆熱時空](../Page/MAZE☆爆熱時空.md "wikilink")（[赤堀悟](../Page/赤堀悟.md "wikilink")/）

### 台灣角川

  - [機動戰士GUNDAM
    SEED](../Page/機動戰士GUNDAM_SEED.md "wikilink")（/[小笠原智史](../Page/小笠原智史.md "wikilink")）
  - [機動戰士GUNDAM SEED
    DESTINY](../Page/機動戰士GUNDAM_SEED_DESTINY.md "wikilink")（/As'MARIA）
  - [新羅德斯島戰記](../Page/羅德斯島戰記.md "wikilink")（[水野良](../Page/水野良.md "wikilink")/[美樹本晴彥](../Page/美樹本晴彥.md "wikilink")）
  - [涼宮春日系列](../Page/涼宮春日系列.md "wikilink")（[谷川流](../Page/谷川流.md "wikilink")/）
  - [.hack//AI
    buster](../Page/.hack/AI_buster.md "wikilink")（[濱崎達也](../Page/濱崎達也.md "wikilink")/）
  - [.hack//Another
    Birth](../Page/.hack/Another_Birth.md "wikilink")（[川崎美羽](../Page/川崎美羽.md "wikilink")/）
  - [.hack//CELL](../Page/.hack/CELL.md "wikilink")（[涼風涼](../Page/涼風涼.md "wikilink")/）
  - [聖魔之血](../Page/聖魔之血.md "wikilink")（[吉田直](../Page/吉田直.md "wikilink")/[THORES柴本](../Page/THORES柴本.md "wikilink")）
  - [打工魔法師](../Page/打工魔法師.md "wikilink")（[椎野美由貴](../Page/椎野美由貴.md "wikilink")/）
  - [薔薇的瑪利亞](../Page/薔薇的瑪利亞.md "wikilink")（[十文字青](../Page/十文字青.md "wikilink")/[BUNBUN](../Page/BUNBUN.md "wikilink")）
  - [蟲之歌](../Page/蟲之歌.md "wikilink")（[岩井恭平](../Page/岩井恭平.md "wikilink")/）
  - [憐
    Ren](../Page/憐_Ren.md "wikilink")（[水口敬文](../Page/水口敬文.md "wikilink")/）
  - [魔法人力派遣公司](../Page/魔法人力派遣公司.md "wikilink")（[三田誠](../Page/三田誠.md "wikilink")/[pako](../Page/pako.md "wikilink")）
  - [神之遊戲](../Page/神之遊戲.md "wikilink")（[宮崎柊羽](../Page/宮崎柊羽.md "wikilink")/[七草](../Page/七草.md "wikilink")）
  - [CODE GEASS 反叛的魯路修](../Page/Code_Geass反叛的魯路修.md "wikilink")(）
  - [SUGAR DARK
    被埋葬的黑闇與少女](../Page/SUGAR_DARK_被埋葬的黑闇與少女.md "wikilink")([新井円侍](../Page/新井円侍.md "wikilink")/[mebae](../Page/mebae.md "wikilink")）
  - [東京皇帝☆北條戀歌](../Page/東京皇帝☆北條戀歌.md "wikilink")（[竹井10日](../Page/竹井10日.md "wikilink")/）
  - [超自然異象研究社](../Page/超自然異象研究社.md "wikilink")（[耳目口司](../Page/耳目口司.md "wikilink")/）
  - [不迷途的羔羊](../Page/不迷途的羔羊.md "wikilink")（[玩具堂](../Page/玩具堂.md "wikilink")/[籠目](../Page/籠目.md "wikilink")）
  - [丹特麗安的書架](../Page/丹特麗安的書架.md "wikilink")（[三雲岳斗](../Page/三雲岳斗.md "wikilink")/）
  - [R-15](../Page/R-15.md "wikilink")（/[藤真拓哉](../Page/藤真拓哉.md "wikilink")）
  - [問題兒童都來自異世界？](../Page/問題兒童都來自異世界？.md "wikilink")（[龍之湖太郎](../Page/龍之湖太郎.md "wikilink")/）
  - [夏日大作戰Crisis of OZ](../Page/夏日大作戰.md "wikilink")（/）
  - [殭屍少女的災難](../Page/殭屍少女的災難.md "wikilink")（[池端亮](../Page/池端亮.md "wikilink")／[蔓木鋼音](../Page/蔓木鋼音.md "wikilink")）
  - [我的腦內戀礙選項](../Page/我的腦內戀礙選項.md "wikilink")（／）
  - [新妹魔王的契約者](../Page/新妹魔王的契約者.md "wikilink")（[上栖綴人](../Page/上栖綴人.md "wikilink")／[大熊貓介](../Page/大熊貓介.md "wikilink")）
  - [迷幻魔域](../Page/迷幻魔域.md "wikilink")（[久慈政宗](../Page/久慈政宗.md "wikilink")／）

### 青春文化

  - [失蹤HOLIDAY](../Page/失蹤HOLIDAY.md "wikilink")（[乙一](../Page/乙一.md "wikilink")）
  - [只有你聽到 CALLING YOU](../Page/只有你聽到_CALLING_YOU.md "wikilink")（乙一）
  - [寂寞的頻率](../Page/寂寞的頻率.md "wikilink")（乙一）

### 尖端出版

  - [機動戰士Z
    GUNDAM](../Page/機動戰士Z_GUNDAM.md "wikilink")（[富野由悠季](../Page/富野由悠季.md "wikilink")）
  - [機動戰士GUNDAM
    0083](../Page/機動戰士Gundam_0083:Stardust_Memory.md "wikilink")（[山口宏](../Page/山口宏.md "wikilink")）
  - [聖・魔・少・女](../Page/聖・魔・少・女.md "wikilink")（[林智明](../Page/林智明.md "wikilink")/）
  - [虛界怪造學](../Page/虛界怪造學.md "wikilink")（[日日日](../Page/日日日.md "wikilink")/）
  - [馭時少女Rinne](../Page/馭時少女.md "wikilink")（[清野靜](../Page/清野靜.md "wikilink")/）
  - [圓環少女](../Page/圓環少女.md "wikilink")（[長谷敏司](../Page/長谷敏司.md "wikilink")/[深遊](../Page/深遊.md "wikilink")）
  - [魔裝學園H×H](../Page/魔裝學園H×H.md "wikilink")（[久慈政宗](../Page/久慈政宗.md "wikilink")/[Hisasi](../Page/Hisasi.md "wikilink")）

### 蓋亞文化

  - [羅德斯島戰記](../Page/羅德斯島戰記.md "wikilink")（水野良/[出淵裕](../Page/出淵裕.md "wikilink")）

### 獨步文化

  - [勇者物語](../Page/勇者物語.md "wikilink")（[宮部美幸](../Page/宮部美幸.md "wikilink")）

### 青文出版社

  - [斬魔大聖Demonbane](../Page/斬魔大聖Demonbane.md "wikilink")（[涼風涼](../Page/涼風涼.md "wikilink")/[Ｎｉθ](../Page/Ｎｉθ.md "wikilink")）
  - [吸血殲鬼VJEDOGONIA](../Page/吸血殲鬼VJEDOGONIA.md "wikilink")（[虛淵玄](../Page/虛淵玄.md "wikilink")/[山田秀樹](../Page/山田秀樹.md "wikilink")）

### 東立出版社

  - [密斯瑪路卡興國物語](../Page/密斯瑪路卡興國物語.md "wikilink")（/）
  - [CANAAN](../Page/CANAAN.md "wikilink")（[杉原智則](../Page/杉原智則.md "wikilink")/[關口可奈味](../Page/關口可奈味.md "wikilink")）
  - [Mix！](../Page/Mix！.md "wikilink")（/[CARNELIAN](../Page/CARNELIAN.md "wikilink")）
  - [大奧之櫻](../Page/大奧之櫻.md "wikilink")（[日日日](../Page/日日日.md "wikilink")/）
  - [十字架×王之證](../Page/十字架×王之證.md "wikilink")（[三田誠](../Page/三田誠.md "wikilink")/）
  - [七星降靈學園的惡魔](../Page/七星降靈學園的惡魔.md "wikilink")（[田口仙年堂](../Page/田口仙年堂.md "wikilink")/[夕仁](../Page/夕仁.md "wikilink")）
  - [我的欲望(女主角)難以招架](../Page/我的欲望\(女主角\)難以招架.md "wikilink")（/）

### 四季國際

  - [重啟咲良田](../Page/重啟咲良田.md "wikilink")（[河野裕](../Page/河野裕.md "wikilink")/[椎名優](../Page/椎名優.md "wikilink")）

### 天闻角川

  - [SUGAR DARK
    被埋葬的黑暗](../Page/SUGAR_DARK_被埋葬的黑暗.md "wikilink")([新井円侍](../Page/新井円侍.md "wikilink")/[mebae](../Page/mebae.md "wikilink")）
  - [不迷途的羔羊](../Page/不迷途的羔羊.md "wikilink")（[玩具堂](../Page/玩具堂.md "wikilink")/[笼目](../Page/笼目.md "wikilink")）
  - [凉宫春日的惊愕](../Page/凉宫春日的惊愕.md "wikilink")（[谷川流](../Page/谷川流.md "wikilink")/[伊东杂音](../Page/伊东杂音.md "wikilink")）
  - [丹特丽安的书架](../Page/丹特丽安的书架.md "wikilink")（[三云岳斗](../Page/三云岳斗.md "wikilink")/[G
    Yusuke](../Page/G_Yusuke.md "wikilink")）
  - [某重生少女的灾难](../Page/某重生少女的灾难.md "wikilink")（[池端亮](../Page/池端亮.md "wikilink")/[蔓木钢音](../Page/蔓木钢音.md "wikilink")）
  - [问题儿童都来自异世界？](../Page/问题儿童都来自异世界？.md "wikilink")（[龙之湖太郎](../Page/龙之湖太郎.md "wikilink")/[天之有](../Page/天之有.md "wikilink")）

### 未代理

  - （飛火野耀）

  - [交響詩篇艾蕾卡7](../Page/交響詩篇.md "wikilink")（[BONES](../Page/BONES_\(動畫製作公司\).md "wikilink")）

  - [鋼鐵天使](../Page/鋼鐵天使.md "wikilink")（[とまとあき](../Page/とまとあき.md "wikilink")，[臺灣東販代理漫畫版](../Page/臺灣東販.md "wikilink")）

  - [極道君漫遊記](../Page/極道君漫遊記.md "wikilink")（）

  - [消閒的挑戰者](../Page/消閒的挑戰者.md "wikilink")（[岩井恭平](../Page/岩井恭平.md "wikilink")/[四季童子](../Page/四季童子.md "wikilink")）

  - （[園田英樹](../Page/園田英樹.md "wikilink")）

  - [超時空要塞馬克羅斯](../Page/超時空要塞.md "wikilink")（[富田祐弘](../Page/富田祐弘.md "wikilink")）

  - [幸運騎士](../Page/幸運騎士.md "wikilink")（[深澤美潮](../Page/深澤美潮.md "wikilink")/[迎夏生](../Page/迎夏生.md "wikilink")，[尖端出版代理漫畫版](../Page/尖端出版.md "wikilink")）

  - [BLOOD+](../Page/血戰.md "wikilink")（[池端亮](../Page/池端亮.md "wikilink")）

  - [FLCL](../Page/FLCL.md "wikilink")（[榎戶洋司](../Page/榎戶洋司.md "wikilink")）

  - [魔動王](../Page/魔動王.md "wikilink")（[廣井王子](../Page/廣井王子.md "wikilink")＆[Re
    Company](../Page/Red_Entertainment.md "wikilink")）

  - [幸運☆星 (小說)](../Page/幸運☆星_\(小說\).md "wikilink")（原作:
    著：[竹井10日](../Page/竹井10日.md "wikilink")/)

  - [最後流亡](../Page/最後流亡.md "wikilink")
    ([神山修一](../Page/神山修一.md "wikilink"))

  - [DOORS](../Page/DOORS_\(小說\).md "wikilink")（[神坂一](../Page/神坂一.md "wikilink")/）

## 關聯條目

  - [Sneaker大賞](../Page/Sneaker大賞.md "wikilink")
  - [Light Novel Award](../Page/Light_Novel_Award.md "wikilink")
      - [富士見Fantasia文庫](../Page/富士見Fantasia文庫.md "wikilink")
      - [富士見Mystery文庫](../Page/富士見Mystery文庫.md "wikilink")
      - [電擊文庫](../Page/電擊文庫.md "wikilink")
      - [Fami通文庫](../Page/Fami通文庫.md "wikilink")

## 外部連結

  - [角川Sneaker文庫官方網站
    ](https://web.archive.org/web/20140305131310/http://www.sneakerbunko.jp//)
  - [](http://www.kadokawa.co.jp/lnovel/series_16.html)
  - [台灣角川](https://www.kadokawa.com.tw/)

[角川Sneaker文庫](../Category/角川Sneaker文庫.md "wikilink")
[Category:1988年日本建立](../Category/1988年日本建立.md "wikilink")