**陳大受**（），[字](../Page/表字.md "wikilink")**占咸**，[湖南](../Page/湖南.md "wikilink")[祁阳人](../Page/祁阳.md "wikilink")。[清朝官員](../Page/清朝.md "wikilink")。

[雍正十一年](../Page/雍正.md "wikilink")（1733年）[进士](../Page/进士.md "wikilink")，選[庶吉士](../Page/庶吉士.md "wikilink")，[乾隆元年](../Page/乾隆.md "wikilink")（1736年）[散館](../Page/散館.md "wikilink")，授[翰林院编修](../Page/翰林院编修.md "wikilink")，次年超擢，五遷至[吏部侍郎](../Page/吏部侍郎.md "wikilink")。[乾隆十一年](../Page/乾隆.md "wikilink")（1746年）加[太子少保](../Page/太子少保.md "wikilink")，調任[福建巡撫](../Page/福建巡撫.md "wikilink")。[乾隆十二年](../Page/乾隆.md "wikilink")（1747年）任[兵部尚書](../Page/兵部尚書.md "wikilink")。[乾隆十三年](../Page/乾隆.md "wikilink")（1748年），進[協辦大學士](../Page/協辦大學士.md "wikilink")、[軍機處](../Page/軍機處.md "wikilink")[行走](../Page/行走.md "wikilink")。[乾隆十四年](../Page/乾隆.md "wikilink")（1749年）加[太子太傅](../Page/太子太傅.md "wikilink")。[乾隆十五年](../Page/乾隆.md "wikilink")（1750年）出任[兩廣總督](../Page/兩廣總督.md "wikilink")，次年卒于任上。賜祭葬，[諡](../Page/諡.md "wikilink")**文肃**。有《陈文肃奏议》。有子[陳輝祖](../Page/陳輝祖.md "wikilink")、陳嚴祖。

## 參考文獻

  - 《[清史稿](../Page/清史稿.md "wikilink")》卷三○七

{{-}}

[Category:清朝庶吉士](../Category/清朝庶吉士.md "wikilink")
[Category:清朝翰林](../Category/清朝翰林.md "wikilink")
[Category:清朝吏部侍郎](../Category/清朝吏部侍郎.md "wikilink")
[Category:清朝福建巡抚](../Category/清朝福建巡抚.md "wikilink")
[Category:清朝兵部尚書](../Category/清朝兵部尚書.md "wikilink")
[Category:清朝吏部尚書](../Category/清朝吏部尚書.md "wikilink")
[Category:清朝两广總督](../Category/清朝两广總督.md "wikilink")
[Category:祁阳人](../Category/祁阳人.md "wikilink")
[D](../Category/陳姓.md "wikilink")
[Category:諡文肅](../Category/諡文肅.md "wikilink")