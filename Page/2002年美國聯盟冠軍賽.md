**2002年美國職棒大聯盟美國聯盟冠軍系列賽**(American League Championship
Series)。由當年[外卡](../Page/外卡.md "wikilink")（wild
card）的[安那罕天使隊](../Page/安那罕天使隊.md "wikilink")（Anaheim
Angels）對戰中區冠軍[明尼蘇達雙城隊](../Page/明尼蘇達雙城.md "wikilink")（Minnesota
Twins）。天使隊在先前[美國聯盟分區系列賽](../Page/美國聯盟分區系列賽.md "wikilink")（American
League Division
Series,ALDS）中，以3勝1負淘汰連續四年美聯冠軍得主[紐約洋基隊](../Page/紐約洋基.md "wikilink")（New
York Yankees）；雙城隊則以3勝2敗氣走[奧克蘭運動家隊](../Page/奧克蘭運動家.md "wikilink")（Oakland
Athletics）晉級。

天使最後以4勝1負贏得本系列賽，進入2002年[世界大賽](../Page/世界大賽.md "wikilink")（World
Series），並擊敗[舊金山巨人隊](../Page/舊金山巨人.md "wikilink")（San Francisco
Giants）取得世界冠軍。

**總教練**

天使隊：[麥克·梭夏](../Page/麥克·梭夏.md "wikilink")（Mike Sciosia）雙城隊：朗·嘉登海爾（Ron
Gardenhire）

**裁判組**

艾德·蒙太奇（Ed Montague）、麥克·艾佛列特（Mike Everitt）、賴瑞·楊（Larry Young）、大拿·德穆斯（Dana
DeMuth）、艾德·拉普亞諾（Ed Rapuano）

**系列戰最有價值球員(MVP)**

天使隊的[亞當·甘迺迪](../Page/亞當·甘迺迪.md "wikilink") (Adam Kennedy)

**轉播**

[福斯電視網](../Page/福斯電視網.md "wikilink")（FOX），播報員為湯姆·本納門（Thom
Brennaman）、及史提夫·里恩斯（Steve Lyons）

## 總記

| |場次 | 比數                  | 日期     | 地點                                                                                 | 觀眾人數   |
| --- | ------------------- | ------ | ---------------------------------------------------------------------------------- | ------ |
|     |                     |        |                                                                                    |        |
| 1   | 天使 - 1, **雙城** - 2  | 10月8日  | [Hubert H. Humphrey Metrodome](../Page/Hubert_H._Humphrey_Metrodome.md "wikilink") | 55,562 |
| 2   | **天使** - 6，雙城 - 3   | 10月9日  | [Hubert H. Humphrey Metrodome](../Page/Hubert_H._Humphrey_Metrodome.md "wikilink") | 55,990 |
| 3   | 雙城 - 1, **天使** - 2  | 10月11日 | [Edison Field](../Page/Edison_Field.md "wikilink")                                 | 44,234 |
| 4   | 雙城 - 1, **天使** - 7  | 10月12日 | [Edison Field](../Page/Edison_Field.md "wikilink")                                 | 44,830 |
| 5   | 雙城 - 5, **天使** - 13 | 10月13日 | [Edison Field](../Page/Edison_Field.md "wikilink")                                 | 44,835 |

## 盒子分數

### 第一戰

10月8日，於胡伯特·漢福瑞 大都會球場（Hubert H. Humphrey Metrodome）

| 球隊                                                                                                                                                                                                                | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | R     | H     | E     |
| :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | - | - | - | - | - | - | - | - | - | ----- | ----- | ----- |
| 天使                                                                                                                                                                                                                | 0 | 0 | 1 | 0 | 0 | 0 | 0 | 0 | 0 | **1** | **4** | **0** |
| **雙城**                                                                                                                                                                                                            | 0 | 1 | 0 | 0 | 1 | 0 | 0 | 0 | X | **2** | **5** | **1** |
| <small>**勝投**: [喬·梅斯](../Page/喬·梅斯.md "wikilink")（Joe Mays） (1-0)  **敗投**: [Kevin Appier](../Page/Kevin_Appier.md "wikilink") (0-1)  **救援成功**: [艾迪·瓜達鐸](../Page/艾迪·瓜達鐸.md "wikilink")（Eddie Guardado） (1)</small> |   |   |   |   |   |   |   |   |   |       |       |       |
| <small>**全壘打**：無 </small>                                                                                                                                                                                         |   |   |   |   |   |   |   |   |   |       |       |       |

### 第二戰

10月9日，於胡伯特·漢福瑞 大都會球場（Hubert H. Humphrey Metrodome）

| 球隊                                                                                                                                                                                                                       | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | R     | H      | E     |
| :----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | - | - | - | - | - | - | - | - | - | ----- | ------ | ----- |
| **天使**                                                                                                                                                                                                                   | 1 | 3 | 0 | 0 | 0 | 2 | 0 | 0 | 0 | **6** | **10** | **0** |
| 雙城                                                                                                                                                                                                                       | 0 | 0 | 0 | 0 | 0 | 3 | 0 | 0 | 0 | **3** | **11** | **1** |
| <small>**勝投**: [雷蒙·歐提茲](../Page/雷蒙·歐提茲.md "wikilink") (Ramon Ortiz) (1-0)  **敗投**: [瑞克·瑞德](../Page/瑞克·瑞德.md "wikilink") (Rick Reed)(0-1)  **救援成功**: [特洛依·帕西佛](../Page/特洛依·帕西佛.md "wikilink")（Troy Percival）（ (1)</small> |   |   |   |   |   |   |   |   |   |       |        |       |
| <small>**全壘打**: **天使** – [戴林·厄斯泰](../Page/戴林·厄斯泰.md "wikilink")（Darin Erstad） (1), [布拉德·福默](../Page/布拉德·福默.md "wikilink")（Brad Fullmer） (1) </small>                                                                     |   |   |   |   |   |   |   |   |   |       |        |       |

### 第三戰

10月11日，於艾迪森球場（Edison Field）

| 球隊                                                                                                                                                                                                                                             | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | R     | H     | E     |
| :--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | - | - | - | - | - | - | - | - | - | ----- | ----- | ----- |
| 雙城                                                                                                                                                                                                                                             | 0 | 0 | 0 | 0 | 0 | 0 | 1 | 0 | 0 | **1** | **6** | **0** |
| **天使**                                                                                                                                                                                                                                         | 0 | 1 | 0 | 0 | 0 | 0 | 0 | 1 | X | **2** | **7** | **2** |
| <small>**勝投**: [法蘭西斯科·羅德里奎茲](../Page/法蘭西斯科·羅德里奎茲.md "wikilink")（Francisco Rodriguez）(1-0)  **敗投**: [J·C·羅梅洛](../Page/J·C·羅梅洛.md "wikilink")（J. C. Romero） (0-1)  **救援成功**: [特洛依·帕西佛](../Page/特洛依·帕西佛.md "wikilink")（Troy Percival） (2)</small> |   |   |   |   |   |   |   |   |   |       |       |       |
| <small>**全壘打**: **天使** – [蓋瑞·安德森](../Page/蓋瑞·安德森.md "wikilink")（Garret Anderson） (1), [特洛伊·格勞斯](../Page/特洛伊·格勞斯.md "wikilink")（Troy Glaus） (1)</small>                                                                                         |   |   |   |   |   |   |   |   |   |       |       |       |

### 第四戰

10月12日，於艾迪森球場（Edison Field）

| 球隊                                                                                                                                                | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | R     | H      | E     |
| :------------------------------------------------------------------------------------------------------------------------------------------------ | - | - | - | - | - | - | - | - | - | ----- | ------ | ----- |
| 雙城                                                                                                                                                | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 1 | **1** | **6**  | **2** |
| **天使**                                                                                                                                            | 0 | 0 | 0 | 0 | 0 | 0 | 2 | 5 | X | **7** | **10** | **0** |
| <small>**勝投**:[約翰·雷基](../Page/約翰·雷基.md "wikilink")（John Lackey） (1-0)  **敗投**: [布瑞德·拉克](../Page/布瑞德·拉克.md "wikilink")（Brad Radke） (0-1)  </small> |   |   |   |   |   |   |   |   |   |       |        |       |
| <small>**全壘打**：無 </small>                                                                                                                         |   |   |   |   |   |   |   |   |   |       |        |       |

### 第五戰

10月13日，於艾迪森球場（Edison Field）

| 球隊                                                                                                                                                        | 1 | 2 | 3 | 4 | 5 | 6 | 7  | 8 | 9 | R      | H      | E     |
| :-------------------------------------------------------------------------------------------------------------------------------------------------------- | - | - | - | - | - | - | -- | - | - | ------ | ------ | ----- |
| 雙城                                                                                                                                                        | 1 | 1 | 0 | 0 | 0 | 0 | 3  | 0 | 0 | **5**  | **9**  | **0** |
| **天使**                                                                                                                                                    | 0 | 0 | 1 | 0 | 2 | 0 | 10 | 0 | X | **13** | **18** | **0** |
| <small>**勝投**: [法蘭西斯科·羅德里奎茲](../Page/法蘭西斯科·羅德里奎茲.md "wikilink")(2-0)  **敗投**: [約翰·山塔納](../Page/約翰·山塔納.md "wikilink")（Johan Santana） (0-1)  </small>       |   |   |   |   |   |   |    |   |   |        |        |       |
| <small>**全壘打**: **天使** – [亞當·甘迺迪](../Page/亞當·甘迺迪.md "wikilink")（Adam Kennedy） (3), [斯考特·史皮奇歐](../Page/斯考特·史皮奇歐.md "wikilink")（Scott Spiezio） (1) </small> |   |   |   |   |   |   |    |   |   |        |        |       |

## 大事紀

[亞當·甘迺迪在季賽中只有](../Page/亞當·甘迺迪.md "wikilink")7轟；但決定性的第5戰中，他一口氣打了三發全壘打，也得到最有價值球員獎。

天使成為大聯盟史上第一個季後賽三輪首戰都落敗，最後卻還能得到世界冠軍的球隊。

## 外部連結

  - [Baseball-Reference.com的 2002美聯冠軍戰紀錄](http://www.baseball-reference.com/postseason/2002_ALCS.shtml)

[Category:American League Championship
Series](../Category/American_League_Championship_Series.md "wikilink")
[American League Championship
Series](../Category/2002_in_baseball.md "wikilink")
[Category:明尼蘇達雙城](../Category/明尼蘇達雙城.md "wikilink")
[Category:Los Angeles Angels of
Anaheim](../Category/Los_Angeles_Angels_of_Anaheim.md "wikilink")
[Category:MLB on FOX](../Category/MLB_on_FOX.md "wikilink")