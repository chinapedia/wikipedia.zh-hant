**NGC
1097**是位於[天爐座的一個](../Page/天爐座.md "wikilink")[棒旋星系](../Page/棒旋星系.md "wikilink")，距離大約4,500萬[光年遠](../Page/光年.md "wikilink")。在1790年10月9日被[弗里德里希·威廉·赫歇爾發現](../Page/弗里德里希·威廉·赫歇爾.md "wikilink")。迄2006年，已經在這而發現三顆[超新星](../Page/超新星.md "wikilink")：[SN
1992bd](../Page/SN_1992bd.md "wikilink")、[SN
1999eu](../Page/SN_1999eu.md "wikilink")、和[SN
2003B](../Page/SN_2003B.md "wikilink")。

[NGC_1097_center_Hubble.jpg](https://zh.wikipedia.org/wiki/File:NGC_1097_center_Hubble.jpg "fig:NGC_1097_center_Hubble.jpg")：主鏡視野
0.9\&prime。\]\] NGC
1097也是一個有噴流從核心射出的[西佛星系](../Page/西佛星系.md "wikilink")，並且和許多星系一樣，在NGC
1097的核心有個[超大質量黑洞](../Page/超大質量黑洞.md "wikilink")。環繞著中心黑洞的是一圈[恆星形成環](../Page/恆星.md "wikilink")，氣體和塵埃形成一個從環到黑洞的網路。

[Phot-33a-05.jpg](https://zh.wikipedia.org/wiki/File:Phot-33a-05.jpg "fig:Phot-33a-05.jpg")。\]\]

[Phot-35d-04-fullres.jpg](https://zh.wikipedia.org/wiki/File:Phot-35d-04-fullres.jpg "fig:Phot-35d-04-fullres.jpg")中的第三架8.2米南十字的多模式VIMOS設備拍攝，以三色合成產生接近真實色彩的影像。創建者：[ESO](../Page/歐州南方天文台.md "wikilink")\]\]

NGC
1097有兩個[衛星星系](../Page/衛星星系.md "wikilink")：[矮橢圓星系](../Page/矮橢圓星系.md "wikilink")[NGC
1097A是兩個中較大的](../Page/NGC_1097A.md "wikilink")，它是一個奇特的[橢圓星系](../Page/橢圓星系.md "wikilink")，在離NGC
1097的核心42,000光年的距離上環繞著。[矮星系](../Page/矮星系.md "wikilink")[NGC
1097B在更外側](../Page/NGC_1097B.md "wikilink")，我們對它所知無幾。

## 相關條目

  - [NGC 1300](../Page/NGC_1300.md "wikilink")(棒旋星系)
  - [NGC 1232](../Page/NGC_1232.md "wikilink")(中間螺旋星系)

## 參考資料

## 外部連結

  - [NGC 1097 - The Galaxy with the Longest known Optical
    Jets](https://web.archive.org/web/20061112082610/http://weblore.com/richard/ngc_1097.htm)

  - [**Antilhue-Chile**: NGC 1097 in
    Fornax](http://www.astrosurf.com/antilhue/ngc_1097_in_fornax.htm)

  - [Very Large Telescope observations of
    NGC 1097](https://web.archive.org/web/20080304135504/http://www.eso.org/public/outreach/press-rel/pr-2005/phot-33-05.html)

  - [Astronomy Picture of the Day for 1
    December 2006](http://apod.nasa.gov/apod/ap061201.html)

  -
[Category:棒旋星系](../Category/棒旋星系.md "wikilink")
[Category:天爐座星系團](../Category/天爐座星系團.md "wikilink")
[Category:天爐座](../Category/天爐座.md "wikilink")
[1097](../Category/NGC天體.md "wikilink")
[10488](../Category/PGC天體.md "wikilink")
[077](../Category/阿普天體.md "wikilink")
[67](../Category/科德韦尔天体.md "wikilink")