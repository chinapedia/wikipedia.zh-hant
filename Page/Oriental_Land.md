**Oriental
Land**（****，）是一間[日本的公司](../Page/日本.md "wikilink")，負責經營、管理以[東京迪士尼樂園](../Page/東京迪士尼樂園.md "wikilink")、[東京迪士尼海洋為中心的](../Page/東京迪士尼海洋.md "wikilink")[東京迪士尼度假區](../Page/東京迪士尼度假區.md "wikilink")。

在成立初期的主要出資股東是[京成電鐵和](../Page/京成電鐵.md "wikilink")[三井不動產](../Page/三井不動產.md "wikilink")。此外，[千葉縣政府也是主要股東之一](../Page/千葉縣.md "wikilink")。公共團體出資的比率為3.96%。

公司成立的最初目標是要在日本提供更多大眾休閒設施，例如建立游泳池、健身房等。在1980年，Oriental
Land首次與[華特迪士尼公司接洽](../Page/華特迪士尼公司.md "wikilink")，負責管理第一座國際迪士尼樂園的財務。[東京迪士尼樂園在](../Page/東京迪士尼樂園.md "wikilink")1983年完工並正式開幕，而Oriental
Land決定與華特迪士尼公司以[特許經營模式營運](../Page/特許經營.md "wikilink")，每年Oriental
Land公司須付出授權費用給華特迪士尼公司，而華特迪士尼公司也派出[幻想工程師設計和建造整個](../Page/幻想工程師.md "wikilink")[東京迪士尼度假區](../Page/東京迪士尼度假區.md "wikilink")，以及在2001年開幕的第二座主題樂園—[東京迪士尼海洋](../Page/東京迪士尼海洋.md "wikilink")。

Oriental Land同時也持有並負責營運日本所有[迪士尼商店](../Page/迪士尼商店.md "wikilink")（Disney
Store）。但隨著迪士尼商店經營績效不佳，Oriental
Land已經決議在2010年3月底，將迪士尼商店股份全數移交給日本華特迪士尼股份有限公司。\[1\]

## 外部連結

  - [Oriental Land官方網站](http://www.olc.co.jp/)

## 資料來源

[Category:東京迪士尼度假區](../Category/東京迪士尼度假區.md "wikilink")
[Category:日本公司](../Category/日本公司.md "wikilink")
[Category:1960年建立](../Category/1960年建立.md "wikilink")

1.  [子会社の異動（譲渡）に関するお知らせ](http://www.olc.co.jp/news_parts/20100204_01.pdf)