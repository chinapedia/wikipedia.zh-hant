[Pak_Tin_Estate_Tower_1&3_2014.jpg](https://zh.wikipedia.org/wiki/File:Pak_Tin_Estate_Tower_1&3_2014.jpg "fig:Pak_Tin_Estate_Tower_1&3_2014.jpg")
[Pak_Tin_Shopping_Centre.jpg](https://zh.wikipedia.org/wiki/File:Pak_Tin_Shopping_Centre.jpg "fig:Pak_Tin_Shopping_Centre.jpg")
[Pak_Tin_Estate_2013_part1.JPG](https://zh.wikipedia.org/wiki/File:Pak_Tin_Estate_2013_part1.JPG "fig:Pak_Tin_Estate_2013_part1.JPG")
[Pak_Tin_Market.jpg](https://zh.wikipedia.org/wiki/File:Pak_Tin_Market.jpg "fig:Pak_Tin_Market.jpg")
[Pak_Tin_Estate_2013_part8.JPG](https://zh.wikipedia.org/wiki/File:Pak_Tin_Estate_2013_part8.JPG "fig:Pak_Tin_Estate_2013_part8.JPG")
[Pak_Tin_Estate_Phase_3_open_space_2014.JPG](https://zh.wikipedia.org/wiki/File:Pak_Tin_Estate_Phase_3_open_space_2014.JPG "fig:Pak_Tin_Estate_Phase_3_open_space_2014.JPG")
[Pak_Tin_Estate_2013_part5.JPG](https://zh.wikipedia.org/wiki/File:Pak_Tin_Estate_2013_part5.JPG "fig:Pak_Tin_Estate_2013_part5.JPG")
**白田邨**（）是[香港公共屋邨](../Page/香港公共屋邨.md "wikilink")，位於[九龍](../Page/九龍.md "wikilink")[深水埗區](../Page/深水埗區.md "wikilink")[石硤尾](../Page/石硤尾.md "wikilink")，於1969年入伙，於1990年起開始分期局部重建，重建後的樓宇於1993年至2023年分期入伙，整個白田邨重建計劃歷時將長達30年，現由[香港房屋委員會負責屋邨](../Page/香港房屋委員會.md "wikilink")[管理](../Page/管理.md "wikilink")；而當中的瑞田樓、昌田樓及盛田樓則由[領先物業服務顧問有限公司管理](../Page/領先物業服務顧問有限公司.md "wikilink")。白田邨第7，8，10及11期由房屋署總建築師（4）及周余石建築師事務所負責設計，而第9期的白田社區綜合大樓則由房屋署總建築師（4）及周古梁建築師事務所負責設計。

## 歷史

興建白田邨的主要目的是當年為了重建[石硤尾](../Page/石硤尾.md "wikilink")、[大坑東及](../Page/大坑東.md "wikilink")[李鄭屋這三個最舊的](../Page/李鄭屋.md "wikilink")[徙置區而興建的公共屋邨](../Page/徙置區.md "wikilink")，於1969年落成的第4至6座由[徙置事務處管理](../Page/徙置事務處.md "wikilink")，稱為**白田徙置區**，俗稱「白田新區」。

其後落成的第7、8及14至17座分別於1971年8月至1972年7月落成，這批大廈主要用來接收受到[石硤尾徙置區重建影響的居民](../Page/石硤尾徙置區.md "wikilink")，由[屋宇建設委員會管理](../Page/屋宇建設委員會.md "wikilink")，稱為**[白田政府新邨](../Page/政府廉租屋#政府廉租屋邨列表.md "wikilink")**。

1973年[香港房屋委員會成立](../Page/香港房屋委員會.md "wikilink")，白田邨便交由香港房屋委員會管理，白田新區改稱**白田下邨**，白田政府新邨改稱**白田上邨**。第1至3座及第9至13座則於1975年至1979年間落成，並納入白田上邨；1984年合併為白田邨。

1986年，白田邨部分樓宇被發現出現結構問題，原因是承建商偷工減料，[於興建時混入了鹹水和](../Page/鹹水樓.md "wikilink")[英泥成分不足](../Page/英泥.md "wikilink")，此批樓宇於1989年年中被拆卸重建，成為翠田樓及裕田樓。第7、8及17座則於1995年初拆卸。第4至6座則於2000年初拆卸。其後新建樓宇於1993年7月至2004年4月分階段入伙。這些新建樓宇部分是用來接收附近[石硤尾邨第](../Page/石硤尾邨.md "wikilink")10、11、37、39、40及41座重建工程影響的居民（尤其是2000年3月落成啟用的昌田樓、盛田樓、瑞田樓及2004年4月20日落成啟用的太田樓、麗田樓和運田樓）。

2012年4月，房屋委員會宣佈於2013年起將會分三期清拆及重新興建白田邨（第七型徙置大廈部分，為香港首個七型徙置大廈重建項目），重新興建後可以提供5,650個單位。於2013年將會先行拆卸第1、2、3及12座。於2016年將會拆卸[白田商場](../Page/白田商場.md "wikilink")（由於居民反對而延至2019年拆卸），至2021年拆卸餘下的9、10、11及13座，合共有3,500個單位。屆時，居民可以選擇搬遷到新落成的石硤尾邨二期及五期單位。同年9月24日，房屋委員會表示為了回應受到影響的居民希望盡快完成整項重新興建計劃的訴求，及策劃小組仔細地考慮提早完成原地重新興建的興建屋邨效益後，決定一拼提前於2018年拆卸原定於2019年拆卸的第13座大樓及原定於2021年拆卸的石硤尾白田邨第9座、10及11座大樓，以將重新興建計劃完成日期提前約兩年半\[1\]。由於屆時蘇屋邨第二期的新單位將會落成，屆時房屋委員會會用作安置受提前清拆影響的住戶，整個重建計畫亦可提前兩年半完成，即由2025年年中提前至2023年上半年\[2\]。至於第1、2、3及12座，以及白田商場，則會按照原定計畫，在2014年或者之前率先清拆。重新興建計畫各階段完成後，房屋委員會將會於2018年至2023年在原地興建約共5,650個單位，比較重新興建前增加約2,150個單位。發言人表示，為到受到影響的租戶的清拆安排詳情預計將會於2015年公布，[房屋署會盡量讓受到影響的租戶遷往鄰近或者其所選擇的地區的公共屋邨單位](../Page/房屋署.md "wikilink")\[3\]\[4\]\[5\]，以[蘇屋邨二期單位為主](../Page/蘇屋邨.md "wikilink")；此外，9-11及13座居民亦可選擇動用優先權，購買在[綠表置居計劃](../Page/綠表置居計劃.md "wikilink")2018中發售的[麗翠苑單位](../Page/麗翠苑.md "wikilink")，或居屋2018發售的三個屋苑（當中以[凱樂苑為主](../Page/凱樂苑.md "wikilink")）單位。

2013年2月，因應9、10、11及13座居民強烈要求加快重新興建工程及原邨安置，[房屋署將會提早兩年清拆](../Page/房屋署.md "wikilink")12座，並且將其連同公共運輸交匯處一併發展，騰空的土地可以供予興建兩幢大廈，於2018年至2019年提供約1,050個單位\[6\]。

房署曾希望在2018年4月收回白田商場舖位後拆卸重建，但由於白田商場設有兩部升降機連接白田街和白雲街，令不少長者不用繞道上落斜前往較高的樓宇，因此遭受居民反對\[7\]，居民提出讓商場升降機運作至新商場落成後才拆卸，但房署最後只將拆卸日期延遲，商場最後在2019年2月開始拆卸。

<File:Pak> Tin Estate Block 1-3 Basketball Court 2014.jpg|第1座與3座之間的籃球場
<File:Pak> Tin Shopping Complex Podium overview 2014.jpg|白田商場連接第3座之間的平台
<File:Alliance> Primary School Kowloon Tong Pak Tin Estate
2014.jpg|白田邨的小學校舍曾作[九龍塘宣道小學臨時校舍](../Page/九龍塘宣道小學.md "wikilink")
<File:Pak> Tin Estate Block 12 2014.JPG|已於2015年拆卸的第12座 <File:Pak> Tin
Estate Block 12 VA 2014.jpg|第12座車輛通道

## 屋邨資料

### 現存樓宇

| 樓宇名稱               | 樓宇類型                                                                          | 落成年份 | 樓宇層數 |
| ------------------ | ----------------------------------------------------------------------------- | ---- | ---- |
| 第9座                | [舊長型](../Page/舊長型.md "wikilink")（第七型）                                         | 1979 | 16   |
| 第10座               |                                                                               |      |      |
| 第11座               |                                                                               |      |      |
| 第13座               | 15                                                                            |      |      |
| 翠田樓                | [和諧3C型連](../Page/和諧3c型.md "wikilink")[和諧附翼第1型](../Page/和諧附翼第1型.md "wikilink") | 1993 | 16   |
| 裕田樓                | [和諧3B型](../Page/和諧3b型.md "wikilink")                                          |      |      |
| 澤田樓                | [和諧3C型](../Page/和諧3c型.md "wikilink")                                          | 1997 |      |
| 富田樓                |                                                                               |      |      |
| 潤田樓                | [和諧1A型](../Page/和諧1a型.md "wikilink")                                          |      |      |
| 安田樓                | 小型單位大廈（[和諧式設計](../Page/和諧式大廈.md "wikilink")）                                  | 1998 | 9    |
| 瑞田樓                | 長者住屋大廈                                                                        | 2000 | 4    |
| 昌田樓                | [和諧一型](../Page/和諧一型.md "wikilink")                                            | 40   |      |
| 盛田樓                |                                                                               |      |      |
| 太田樓                | [新和諧一型](../Page/新和諧一型.md "wikilink")                                          | 2004 |      |
| 麗田樓                |                                                                               |      |      |
| 運田樓                | 新和諧附翼大廈第5型（特別設計）                                                              | 28   |      |
| 康田樓（第7期第1座）\[8\]   | [非標準設計大廈](../Page/非標準設計大廈.md "wikilink")(T字型)                                 | 2020 | 30   |
| 健田樓（第7期第2座）\[9\]   | 29                                                                            |      |      |
| 詠田樓（第8期第3座）\[10\]  | [非標準設計大廈](../Page/非標準設計大廈.md "wikilink")(十字型)                                 |      |      |
| 心田樓（第8期第4座）\[11\]  |                                                                               |      |      |
| 朗田樓（第11期第6座）\[12\] | [非標準設計大廈](../Page/非標準設計大廈.md "wikilink")(L字型)                                 | 2021 | 33   |
| 清田樓（第11期第7座）\[13\] | [非標準設計大廈](../Page/非標準設計大廈.md "wikilink")(T字型)                                 |      |      |

### 已拆卸樓宇

<table>
<thead>
<tr class="header">
<th><p>樓宇名稱</p></th>
<th><p>樓宇類型</p></th>
<th><p>落成年份</p></th>
<th><p>拆卸年份</p></th>
<th><p>重建後樓宇</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>第1座</p></td>
<td><p><a href="../Page/舊長型.md" title="wikilink">舊長型</a>（第七型）</p></td>
<td><p>1975</p></td>
<td><p>2014</p></td>
<td><p>重建中</p></td>
<td><p>重建後將提供2030個單位</p></td>
</tr>
<tr class="even">
<td><p>第2座</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第3座</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第4座</p></td>
<td><p><a href="../Page/徙置大廈.md" title="wikilink">第四型</a></p></td>
<td><p>1969</p></td>
<td><p>2000</p></td>
<td><p>太田樓</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第5座</p></td>
<td><p>麗田樓</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第6座</p></td>
<td><p>運田樓</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第7座</p></td>
<td><p><a href="../Page/徙置大廈.md" title="wikilink">第六型</a></p></td>
<td><p>1971-1972</p></td>
<td><p>1995</p></td>
<td><p>瑞田樓</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第8座</p></td>
<td><p>昌田樓<br />
盛田樓</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第12座</p></td>
<td><p><a href="../Page/舊長型.md" title="wikilink">舊長型</a>（第七型）</p></td>
<td><p>1979</p></td>
<td><p>2014</p></td>
<td><p>重建中</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>第14座</p></td>
<td><p><a href="../Page/徙置大廈.md" title="wikilink">第六型</a></p></td>
<td><p>1971-1972</p></td>
<td><p>1989</p></td>
<td><p>翠田樓</p></td>
<td><p>第14至16座為<a href="../Page/26座問題公屋.md" title="wikilink">26座問題公屋</a></p></td>
</tr>
<tr class="odd">
<td><p>第15座</p></td>
<td><p>裕田樓</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第16座</p></td>
<td><p>潤田樓</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第17座</p></td>
<td><p>1994</p></td>
<td><p>富田樓</p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

Pak Tin Estate Redevelopment Phase 7 and 8 in October
2018.jpg|重建第七至八期（2018年10月） Pak Tin Estate Phase 7 under
rebuild in June 2018.jpg|重建第七至八期（2018年6月） Pak Tin Estate Phase 7 under
rebuild in November 2017.jpg|重建第七至八期（2017年11月） Pak Tin Estate phase 7
under rebuild in October 2015.JPG|重建第七至八期（2015年10月） Pak Tin Estate
Redevelopment Phase 11 in October 2018.jpg|重建第十一期（2018年10月） Pak Tin
Estate Phase 8 under rebuild in June 2018.jpg|重建第十一期（2018年6月） Pak Tin
Estate Phase 8 under rebuild in November 2017.jpg|重建第十一期（2017年11月） Pak
Tin Estate phase 8 under rebuild in November 2015.jpg|重建第十一期（2015年11月）

### 計劃重建樓宇／設施 \[14\]

<table>
<thead>
<tr class="header">
<th><p>預計清拆年份</p></th>
<th><p>舊樓宇名稱</p></th>
<th><p>受影響單位數目</p></th>
<th><p>重建計劃期數</p></th>
<th><p>重建後落成年份</p></th>
<th><p>落成建築物</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2019年</p></td>
<td><p>白田商場及<br />
社區會堂</p></td>
<td><p>不適用</p></td>
<td><p>第9至11期</p></td>
<td><p>2019年初（第9期）<br />
2020/21 年度（第10-11期）</p></td>
<td><p>2,000 個新建公屋單位，新商場將在旁邊第7-8期重置</p></td>
<td><p>社區綜合大樓（重建第9期）完成後，<br />
社區會堂將會遷入該處</p></td>
</tr>
<tr class="even">
<td><p>2019年</p></td>
<td><p>第13座</p></td>
<td><p>約900個單位</p></td>
<td><p>第12期</p></td>
<td><p>2021/22 年度</p></td>
<td><p>新建小學</p></td>
<td><p>現有租戶在 2019/20 年度或之前遷入適合安置資源，<br />
包括第7及第8期的新落成樓宇</p></td>
</tr>
<tr class="odd">
<td><p>2021年</p></td>
<td><p>第9至<br />
第11座</p></td>
<td><p>約1,650個單位</p></td>
<td><p>第13期</p></td>
<td><p>2025/26 年度</p></td>
<td><p>2,100 個公屋單位</p></td>
<td><p>現有租戶在 2021/22 年度或之前遷入適合安置資源，<br />
包括第10及第11期的新落成樓宇</p></td>
</tr>
</tbody>
</table>

## 教育及福利設施

### 幼稚園

  - [救世軍白田幼兒學校](http://www.salvationarmy.org.hk/esd/ptnsc/)（1999年創辦）（位於富田樓C翼地下）
  - [崇真會白田美善幼稚園](http://www.ptgraceful.edu.hk)（2000年創辦）（位於盛田樓地下）

### 早期教育及訓練中心

  - [協康會白田中心](https://www.heephong.org/center/detail/pak-tin-centre)（位於瑞田樓B座3樓3號）

### 小學

  - [白田天主教小學](https://www.paktin.edu.hk)

## 設施及志願機構

  - 白田商場，設有街市及停車場。房屋署已在2018年4月收回所有商舖，並於2019年2月開始拆卸。
  - [港鐵特惠站](../Page/港鐵特惠站.md "wikilink")（曾設於白田商場近三樓入口，商場重建時遷到富田樓）
  - [龍耳](../Page/龍耳.md "wikilink")[（白田）聾人及弱聽綜合服務中心](../Page/聽障人士職業支援及社會服務中心.md "wikilink")（翠田樓）
  - 慧進會

## 圖片集

Pak Tin Estate 2013 part2.JPG|白田邨潤田樓、澤田樓及富田樓 Pak Tin Estate On Tin House
2014.jpg|白田邨安田樓 Pak Tin Estate 2013 part3.JPG|白田邨舊長型大廈，於2014年起開始重建 Pak
Tin Community Hall, Pak Tin Estate (Hong Kong).jpg|白田社區會堂 Silence
Association.JPG|白田邨聾人及弱聽綜合服務中心

## 交通

<div class="NavFrame collapsed" style="color: black; background-color: yellow; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: yellow; margin: 0 auto; padding: 0 10px; font-weight:normal;">

交通路線列表

</div>

<div class="NavContent" style="background-color: yellow; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{觀塘綫色彩}}">█</font>[觀塘綫](../Page/觀塘綫.md "wikilink")：[石硤尾站](../Page/石硤尾站.md "wikilink")
    (步行10至15分鐘)

<!-- end list -->

  - [白雲街](../Page/白雲街.md "wikilink")

<!-- end list -->

  - [南昌街](../Page/南昌街.md "wikilink")

</div>

</div>

## 事件

  - 2009年11月10日，該邨第11座有人高空擲下大鐵鎚被捕。\[15\]
  - 2009年11月13日，該邨第11座有東西墮下，[警方認為是因大風所致](../Page/警方.md "wikilink")。\[16\]
  - 2009年11月22日，昌田樓有大菜刀墮進簷篷。
  - 2018年6月，當區區議員[甄啟榮連同過百名居民](../Page/甄啟榮.md "wikilink")，因反對封閉白田商場而發起包圍白田邨房署辦事處行動。事後甄啟榮被控一項刑事毁壞、兩項普通襲擊及兩項強行進入罪\[17\]。
  - 2019年1月，無綫電視節目《[東張西望](../Page/東張西望.md "wikilink")》揭發白田邨麗田樓有一住戶在深夜長期發出敲擊聲響的噪音，嚴重滋擾附近居民超過1000日。居民和區議員多次投訴，但房署一直未有行動。區議員批評相關政府部門推卸責任。\[18\]\[19\]

## 參考資料

## 外部連結

  - [房屋署白田邨資料](http://www.housingauthority.gov.hk/tc/global-elements/estate-locator/detail.html?propertyType=1&id=3190)
  - [反貪實錄
    - 26座問題公屋貪污案](http://www.icac.org.hk/big5/cases/26p/cat1_01.html)

[Category:石硤尾](../Category/石硤尾.md "wikilink")
[Category:以地名／鄉村名命名的公營房屋](../Category/以地名／鄉村名命名的公營房屋.md "wikilink")

1.  [白田邨四座提前於2018年拆卸](http://hk.news.yahoo.com/白田邨四座提前於2018年拆卸-125400748.html)

2.  \[<http://hk.news.yahoo.com/白田邨重建後可提供逾5千單位-071500751.html>;
    白田邨重建後可提供逾5千單位\]

3.  [白田邨四座提前清拆](http://hk.news.yahoo.com/白田-四座提前清拆-223000605.html)
    《星島日報》 2012年9月25日

4.  [白田邨４座提早拆
    冀早2年完成重建](http://hk.news.yahoo.com/白田邨-座提早拆-冀早2年完成重建-211939312.html)
    《明報》 2012年9月25日

5.  [白田邨提前2.5年完成重建](http://orientaldaily.on.cc/cnt/news/20120925/00176_021.html)
    《東方日報》 2012年9月25日

6.  [白田邨重建
    千戶原區安置](http://orientaldaily.on.cc/cnt/news/20130223/00176_085.html)
    《東方日報》 2013年2月23日

7.
8.  [1](http://cyclub.happyhongkong.com/attachments/month_1903/19031512075aad59222ae8b80a.jpeg)

9.
10.
11.
12.
13.
14. [房委會有關白田邨第9-13座重建的相關安排的文件](https://www.housingauthority.gov.hk/tc/common/pdf/about-us/housing-authority/ha-paper-library/SHC43-17,COC21-17R.pdf)

15. [警方到白田邨高空墮物現場重組案情](http://www.rthk.org.hk/rthk/news/expressnews/20091111/news_20091111_55_625927.htm)，香港電台

16. [白田有花盆疑被風吹倒墮街](http://cablenews.i-cable.com/webapps/news_video/index.php?news_id=318362)，有線新聞

17.

18.

19.