[Camp_fuel.jpg](https://zh.wikipedia.org/wiki/File:Camp_fuel.jpg "fig:Camp_fuel.jpg")
[Steinkohle_aus_dem_Bergbau.jpg](https://zh.wikipedia.org/wiki/File:Steinkohle_aus_dem_Bergbau.jpg "fig:Steinkohle_aus_dem_Bergbau.jpg")
[Nuclear_fuel_pellets.jpeg](https://zh.wikipedia.org/wiki/File:Nuclear_fuel_pellets.jpeg "fig:Nuclear_fuel_pellets.jpeg")
**燃料**（），是一種透過[化學反應或](../Page/化學反應.md "wikilink")[核反應釋放本身的內](../Page/核反應.md "wikilink")[能以供其它方面使用的物質](../Page/能量.md "wikilink")。

燃料可分成天然燃料與人工燃料。天然燃料从大自然获得并可以直接使用，比如木柴、煤等；人工燃料是经过工艺加工后获得的燃料，比如焦炭、燃油等。

燃料的质量由它所产生[热量的能力](../Page/热量.md "wikilink")[热值来决定](../Page/热值.md "wikilink")。利用[废气中所含水蒸气的能量](../Page/废气.md "wikilink")，人们可以在技术上提高热值。

## 化學燃料

  - [生質燃料](../Page/生質燃料.md "wikilink")（至少80％的體積，由十年內生產的有機活體物質所提煉出的燃料）
  - [化石燃料](../Page/化石燃料.md "wikilink")
  - [高能燃料](../Page/高能燃料.md "wikilink")（比同质量汽油燃烧时放出的热量高的燃料）

## 核燃料

  - [核分裂](../Page/核分裂.md "wikilink")
  - [核融合](../Page/核融合.md "wikilink")

## 价格

随经济情况的发展燃料的世界市场价格升降剧烈。以下为一些燃料在1970年到2004年之间的波动范畴：

  - [原油](../Page/原油.md "wikilink")：900%
  - [天然气](../Page/天然气.md "wikilink")：700%
  - [石煤](../Page/石煤.md "wikilink")：200%

重要燃料的价格对于[国民经济有非常大的影响](../Page/国民经济.md "wikilink")。燃料价格的提高比如可以加强[通货膨胀](../Page/通货膨胀.md "wikilink")。

## 应用

燃料的应用很多：

  - 固体燃料今天在发达国家主要用来生产蒸汽发电。此外在机械工艺和金属冶炼生产[铁和](../Page/铁.md "wikilink")[钢的过程中也需要固体燃料](../Page/钢.md "wikilink")。在[发达国家固体燃料很少用来加热](../Page/发达国家.md "wikilink")。但是在[发展中国家及](../Page/发展中国家.md "wikilink")[戶外活動中固体燃料依然是加热取暖和煮食的重要原料](../Page/戶外活動.md "wikilink")。
  - 液体燃料是[交通和取暖所使用的燃料的重要组成部分](../Page/交通.md "wikilink")，此外液体燃料还是[化学工业的重要原材料](../Page/化学工业.md "wikilink")。
  - 气体燃料主要用在日常生活中、取暖和发电。此外在化学工业中气体燃料也是重要的燃料和原材料。

## 常见化學燃料的热值

[Burning_gas01.jpg](https://zh.wikipedia.org/wiki/File:Burning_gas01.jpg "fig:Burning_gas01.jpg")
氣體燃料的热值\[MJ/m³\]

  - [一氧化碳](../Page/一氧化碳.md "wikilink")：11.5
  - [天然气](../Page/天然气.md "wikilink")：31.7\~41.8
  - [煤气](../Page/煤气.md "wikilink")：17.6
  - [甲烷](../Page/甲烷.md "wikilink")：32.8
  - [乙炔](../Page/乙炔.md "wikilink")：51.6
  - [乙烯](../Page/乙烯.md "wikilink")：54.2
  - [乙烷](../Page/乙烷.md "wikilink")：58.9
  - [丙烷](../Page/丙烷.md "wikilink")：83.4
  - [丁烷](../Page/丁烷.md "wikilink")：108.4

液体燃料及氢气的热值\[MJ/kg\]

  - [氢气](../Page/氢气.md "wikilink")：141.6
  - 机油：36.0
  - [石蜡](../Page/石蜡.md "wikilink")：45.0
  - [甲醇](../Page/甲醇.md "wikilink")：19.6
  - [乙醇](../Page/乙醇.md "wikilink")：26.9
  - [异丙醇](../Page/异丙醇.md "wikilink")：30.9
  - [苯](../Page/苯.md "wikilink")：40.2
  - [生質柴油](../Page/生質柴油.md "wikilink")：37.0
  - [柴油](../Page/柴油.md "wikilink")：43.0
  - [取暖油](../Page/取暖油.md "wikilink")：42.7\~40.2
  - [汽油](../Page/汽油.md "wikilink")：42.5

固體燃料的[热值](../Page/热值.md "wikilink")\[MJ/kg\]

  - [木头](../Page/木头.md "wikilink")：15.1
  - [纸](../Page/纸.md "wikilink")：15.0
  - [泥炭](../Page/泥炭.md "wikilink")：14.7
  - [煤炭](../Page/煤炭.md "wikilink")：8.0
  - [煤球](../Page/煤球.md "wikilink")：19.7
  - [石煤](../Page/石煤.md "wikilink")：27.2\~31.4
  - [木炭](../Page/木炭.md "wikilink")：30.1
  - [碳](../Page/碳.md "wikilink")：32.8
  - [磷](../Page/磷.md "wikilink")：25.2
  - [硫](../Page/硫.md "wikilink")：9.3
  - [镁](../Page/镁.md "wikilink")：25.2
  - [橡胶](../Page/橡胶.md "wikilink")：35.0

## 相關

  - [化石燃料](../Page/化石燃料.md "wikilink")
  - [核燃料](../Page/核燃料.md "wikilink")
  - [燃料電池](../Page/燃料電池.md "wikilink")

## 參考文獻

  -
[\*](../Category/燃料.md "wikilink")
[Category:能源开发](../Category/能源开发.md "wikilink")
[Category:爆炸物](../Category/爆炸物.md "wikilink")