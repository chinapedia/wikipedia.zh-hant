[Boris_Nemtsov_2003_RussiaMeeting.JPG](https://zh.wikipedia.org/wiki/File:Boris_Nemtsov_2003_RussiaMeeting.JPG "fig:Boris_Nemtsov_2003_RussiaMeeting.JPG")\]\]

**鲍里斯·叶菲莫维奇·涅姆佐夫**（[俄语](../Page/俄语.md "wikilink")：****，），[俄罗斯政治家](../Page/俄罗斯.md "wikilink")，[右翼力量联盟创始人之一](../Page/右翼力量联盟.md "wikilink")，曾任[俄罗斯联邦政府第一](../Page/俄罗斯联邦政府.md "wikilink")[副总理](../Page/俄罗斯副总理.md "wikilink")、[国家杜马副主席](../Page/国家杜马.md "wikilink")（[下議院副議長](../Page/下議院.md "wikilink")）。2015年2月27日在[莫斯科](../Page/莫斯科.md "wikilink")[克林姆林宮附近遇刺身亡](../Page/克林姆林宮.md "wikilink")。2018年2月27日，美國首都華盛頓哥倫比亞特區（Washington,
D.C.）正式將俄羅斯使館前廣場改名為「湼姆佐夫廣場」（Boris Nemtsov Plaza）

## 生平

### 早年

涅姆佐夫生於[索契](../Page/索契.md "wikilink")。1981年毕业于高尔基（今[下诺夫哥罗德](../Page/下诺夫哥罗德.md "wikilink")）国立大学无线电物理系，1985年获得该校副博士学位。1981年起开始在高尔基无线电物理科研所任研究员。\[1\]

1995年12月7日涅姆佐夫当选[下诺夫哥罗德州州长](../Page/下诺夫哥罗德州.md "wikilink")。1993年至1997年3月担任[俄罗斯联邦委员会](../Page/俄罗斯联邦委员会.md "wikilink")（[上議院](../Page/上議院.md "wikilink")）成员。1999年12月右翼力量联盟在国家杜马选举中胜出，因此担任[国家杜马副主席](../Page/国家杜马.md "wikilink")、右翼力量联盟议会党团领袖、以及国家杜马法制委员会成员。\[2\]

1997年涅姆佐夫进入政府任燃料与能源部长、第一副总理。1998年组建自由运动“俄罗斯青年”，后在此基础上建立“右翼事业”联盟（1998-2000年）和“右翼力量联盟党”。1999到2003年，涅姆佐夫担任“右翼力量联盟党”国家杜马党团主席。2003年后转入商界，并担任乌克兰总统非专职顾问。\[3\]

2008年，涅姆佐夫发起建立了反对派民主运动“团结”。2010年，该运动加入“为没有专横和腐败的俄罗斯而战”联盟。从2012年起，他担任“俄罗斯共和党-人民自由党”两主席之一。2013年9月他当选[雅罗斯拉夫尔州议员](../Page/雅罗斯拉夫尔州.md "wikilink")。\[4\]

2012年涅姆佐夫指稱[俄國總統](../Page/俄國總統.md "wikilink")[普京坐擁](../Page/普京.md "wikilink")20座[宮殿與](../Page/宮殿.md "wikilink")[別墅](../Page/別墅.md "wikilink")、58架[飛機與](../Page/飛機.md "wikilink")[直升機](../Page/直升機.md "wikilink")、4艘豪華[遊艇](../Page/遊艇.md "wikilink")。\[5\]

### 政治立場

涅姆佐夫本人以相對自由的政治立場聞名於俄國政壇，同時也是反對派的領袖。較為典型的是在烏克蘭事件中，涅姆佐夫反對普京[出兵烏克蘭](../Page/乌克兰亲俄罗斯武装冲突_\(2014年－2015年\).md "wikilink")。

### 遇刺

2015年2月27日晚上23时40分（格林尼治标准时间20时40分），涅姆佐夫在[莫斯科遭受槍擊身亡](../Page/莫斯科.md "wikilink")。\[6\][俄罗斯内政部声明](../Page/俄罗斯内政部.md "wikilink")，涅姆佐夫與二十歲烏克蘭名模女友安娜·杜蕾斯卡雅（Anna
Duritskaya）在[紅場的高級餐廳約完會](../Page/紅場.md "wikilink")，走在[克林姆林宮](../Page/克林姆林宮.md "wikilink")、[聖瓦西里大教堂附近的卡梅尼桥上](../Page/聖瓦西里大教堂.md "wikilink")，正打算回涅姆佐夫的住處，一輛淺色汽車疾駛而過，車內的[刺客向涅姆佐夫連開四槍](../Page/刺客.md "wikilink")。\[7\]

死后，俄罗斯总统普京表示哀悼，并建立调查组负责该起案件。[乌克兰总统](../Page/乌克兰.md "wikilink")[波罗申科声称涅姆佐夫是因为准备公布](../Page/波罗申科.md "wikilink")[普京支持乌克兰东部亲俄武装的证据而遭刺杀的](../Page/普京.md "wikilink")。

2015年3月8日，俄罗斯当局指控两名车臣人谋杀涅姆佐夫，分別是车臣一个警察分队的副队长达达耶夫及为莫斯科一家私营保安公司工作的库巴舍夫。\[8\]達達耶夫承認涉案。此外有三名疑犯被扣查，另有一名疑犯在[格羅茲尼被警察圍捕時引爆手榴彈自殺身亡](../Page/格羅茲尼.md "wikilink")。\[9\]

## 参考文献

## 外部链接

  -
  - [Nemtsov](https://web.archive.org/web/20110628073123/http://rusolidarnost.ru/leaders/boris-nemtsov)
    at Solidarity

  - [Nemtsov](https://web.archive.org/web/20071114124515/http://b-nemtsov.livejournal.com/)
    at LiveJournal

  - [Reports on Putin](http://www.putin-itogi.ru/)

  - [Interview](http://news.bbc.co.uk/2/hi/programmes/hardtalk/9389667.stm)
    with Boris Nemtsov on [BBC](../Page/BBC.md "wikilink")'s
    [HARDtalk](../Page/HARDtalk.md "wikilink")（2011年2月7日播出）

  - [莉娜•哈得斯的鲍里斯•涅姆佐夫畫像](http://www.lenahades.co.uk/#!boris-nemtsov/czut)

## 参见

  - [涅姆佐夫謀殺案](../Page/涅姆佐夫謀殺案.md "wikilink")

{{-}}

[Category:俄國政治人物](../Category/俄國政治人物.md "wikilink")
[Category:俄罗斯副总理](../Category/俄罗斯副总理.md "wikilink")
[Category:下诺夫哥罗德州州长](../Category/下诺夫哥罗德州州长.md "wikilink")
[Category:俄羅斯遇刺身亡者](../Category/俄羅斯遇刺身亡者.md "wikilink")
[Category:火器致死](../Category/火器致死.md "wikilink")
[Category:索契人](../Category/索契人.md "wikilink")

1.

2.
3.

4.
5.

6.
7.

8.

9.