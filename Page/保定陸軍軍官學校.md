**保定陸軍軍官學校**（簡稱**保定军校**）是[中国近代](../Page/中国.md "wikilink")[军事教育歷史上成立最早](../Page/军事教育.md "wikilink")、规模最大、学制最正规的军事学府，位處[直隶](../Page/直隶.md "wikilink")（今[河北省](../Page/河北省.md "wikilink")）[保定城东郊](../Page/保定.md "wikilink")，占地3000余亩，前身為[清朝](../Page/清朝.md "wikilink")**北洋陸軍速成武备学堂**、**陸軍速成學堂**、**陸軍軍官學堂**。

1912年至1923年期間，保定軍校共辦九期，畢業生有6300餘人，其中不少人後來成為[黃埔軍校](../Page/黃埔軍校.md "wikilink")[教官](../Page/教官.md "wikilink")。在[北洋政府](../Page/北洋政府.md "wikilink")、[國民黨及](../Page/國民黨.md "wikilink")[中國共產黨阵营都有保定军校毕业學生](../Page/中國共產黨.md "wikilink")，尤其是大清帝國滅亡後[国民政府時期内](../Page/国民政府.md "wikilink")。若從**北洋行营将弁学堂**（1902年）算起，保定各军事学堂（校）共培养、訓練了11000余名軍官，當中超过2000人獲得[將軍的銜頭](../Page/將軍.md "wikilink")；仅保定陆军军官学校，在短短的11年中的（1912年－1923年）九期毕业生中就培养了后来成长为[少将以上的将军达](../Page/少将.md "wikilink")1700余名。

保定軍校的主要功能為訓練初級軍官，學習期為兩年，分步兵、骑兵、炮兵、工程兵、輜重兵五科，學制章程參照[日本陸軍士官學校](../Page/日本陸軍士官學校.md "wikilink")，教官亦以日本陸軍士官學校畢業者居多。第二任校長為[蔣方震](../Page/蔣方震.md "wikilink")。保定军校前七期只招收全国四所[陆军中学堂](../Page/陆军中学堂.md "wikilink")（民国后改称陆军预备学校）学制二年之毕业生；而陆中是只招收全国十八所（每省设一所）陆军小学堂学制三年之毕业生；陆小则是招收14-15岁的具初小或私塾学历的子弟。故从陆小、陆中至保定军校执行的是一整套的、严格的军事养成式教育。

## 歷史

  - 1901年11月
    [李鴻章病逝](../Page/李鴻章.md "wikilink")，[袁世凱到保定接任直隸](../Page/袁世凱.md "wikilink")[總督兼北洋大臣](../Page/總督.md "wikilink")。舉辦[小站練兵教學](../Page/定武军.md "wikilink")，但是根據《[辛丑條約](../Page/辛丑條約.md "wikilink")》不准駐扎[天津附近](../Page/天津.md "wikilink")，於是亦移至保定。
  - 1902年5月 於保定開辦“**北洋行營將弁學堂**”，[雷震春任总办](../Page/雷震春.md "wikilink")。
  - 1903年2月
    [袁世凱奏請開辦](../Page/袁世凱.md "wikilink")**[陸軍小學堂](../Page/陸軍小學堂.md "wikilink")**、中學堂、大學堂，進行正規軍事教育訓練。之後於保定建成「**北洋陸軍速成武備學堂**」，即為保定軍校前身。每省设一所陆军小学堂，学制三年。全国设四所陆军中学堂，学制两年。保定设陆军入伍生总队，分为步、马、炮、工、辎重各队，学制4个月。保定设陆军兵官学堂，学制一年六个月，入原队半年为“学习官”。毕业后入个部队为排长。在部队任职2年以上择优入陆军大学堂，学制两年。
  - 1906年
    分別於保定校址開辦：**[陸軍速成學堂](../Page/陸軍速成學堂.md "wikilink")**（定额八百，学制2年加6个月实习）、**[陸軍軍官學堂](../Page/陸軍軍官學堂.md "wikilink")**（1910年7月更名为**[陸軍預備大學堂](../Page/陸軍預備大學堂.md "wikilink")**）。
  - 1912年
    袁世凱任[中華民國總統後](../Page/中華民國總統.md "wikilink")，把陸軍預備大學堂搬至[北京](../Page/北京.md "wikilink")，並更名為**陸軍大學**。10月，於保定东郊3里地的[陸軍預備大學堂与](../Page/陸軍預備大學堂.md "wikilink")[陸軍速成學堂原址创辦](../Page/陸軍速成學.md "wikilink")**保定陸軍軍官學校**。北洋政府的陆军军校体系规定：原有的的陆军中学堂、陆军小学堂不再招生，陆军中学堂立即改为陆军预备学校，陆军小学堂则可以继续开办至所有学生毕业；由此形成了陆军预备学校、保定陆军军官学校、陆军大学的体系。由陆军预备学校升入保定军官军校需半年左右的入伍生训练。这个陆军军校体系类似于日本的陆军预备学校、日本士官学校、陆军大学体系。
  - 1920年[暑假](../Page/暑假.md "wikilink")
    駐扎在軍校的軍人因未得薪餉發生嘩變，搶劫軍校，之後[校舍被放火燒毀](../Page/校舍.md "wikilink")。
  - 1923年8月 保定軍校停辦
  - 1993年7月，军校旧址成为[河北省文物保护单位](../Page/河北省文物保护单位.md "wikilink")。
  - 1995年，军校旧址上建起了保定军校纪念馆。
  - 2006年，入选第六批[全国重点文物保护单位](../Page/全国重点文物保护单位.md "wikilink")。[1](http://news.xinhuanet.com/newscenter/2006-06/28/content_4761041.htm)

## 历任校长

  - [赵理泰](../Page/赵理泰.md "wikilink")（1912年10月－12月15日）
  - [蒋方震](../Page/蒋方震.md "wikilink")（1912年12月15日－1913年9月）
  - [曲同丰](../Page/曲同丰.md "wikilink")（1913年9月－1915年9月）
  - [王汝贤](../Page/王汝贤.md "wikilink")（1915年9月－1916年6月）
  - [杨祖德](../Page/杨祖德.md "wikilink")（1916年8月－1919年2月）
  - [贾德耀](../Page/贾德耀.md "wikilink")（1919年3月－1921年5月）
  - [张鸿绪](../Page/张鸿绪.md "wikilink")（1921年6月－1922年9月）
  - [孙树林](../Page/孙树林.md "wikilink")（1922年10月－1923年8月）

## 曾就讀保定的名將

(包括保定陸軍軍官學校及其前身)

### 北洋行营將弁學堂

[武國棟](../Page/武國棟.md "wikilink")

### 北洋陸軍速成武備學堂

[孫傳芳](../Page/孫傳芳.md "wikilink")(一期)、[李樹春](../Page/李樹春.md "wikilink")(三期德文班)、[齊燮元](../Page/齊燮元.md "wikilink")(炮兵二期)、[王承斌](../Page/王承斌.md "wikilink")(步兵二期)、[蔣鴻遇](../Page/蔣鴻遇.md "wikilink")、[李炳之](../Page/李炳之.md "wikilink")、[商震](../Page/商震.md "wikilink")(加入同盟會被退學)

### 北洋陸軍部陸軍速成學堂

[劉郁芬](../Page/劉郁芬.md "wikilink")、[陳樹藩](../Page/陳樹藩.md "wikilink")、[張群](../Page/張群.md "wikilink")、[王柏齡](../Page/王柏齡.md "wikilink")(肄業)、[蔣中正](../Page/蔣中正.md "wikilink")(陸軍速成學堂留學生預備班)

### 陸軍軍官學堂

[李濟深](../Page/李濟深.md "wikilink")、[鄧錫侯](../Page/鄧錫侯.md "wikilink")、[陳調元](../Page/陳調元.md "wikilink")、[何遂](../Page/何遂.md "wikilink")、[孫岳](../Page/孫岳.md "wikilink")

### 陸軍預備大學堂

於1912年 袁世凱任中華民國總統後，把陸軍預備大學堂搬至北京，並更名為陸軍大學。

### 保定陸軍軍官學校

於1912年10月，於保定原址開辦保定陸軍軍官學校。

#### 第一期

(1912年8月秋入學、1914年11月畢業、1114名，括弧中數字代表各科人數，來源：[張力云](../Page/張力云.md "wikilink")-《從北洋武備學堂到保定陸軍軍官學校》

  - 步兵科(565)：[萬耀煌](../Page/萬耀煌.md "wikilink")、[唐生智](../Page/唐生智.md "wikilink")、[李樹春](../Page/李樹春.md "wikilink")、[孫震](../Page/孫震.md "wikilink")、[李品仙](../Page/李品仙.md "wikilink")、[王天培](../Page/王天培.md "wikilink")、[楊愛源](../Page/楊愛源.md "wikilink")、[孙楚](../Page/孙楚.md "wikilink")、[傅汝鈞](../Page/傅汝鈞.md "wikilink")、[張樾亭](../Page/張樾亭.md "wikilink")、[曹士傑](../Page/曹士傑.md "wikilink")、[夏首勛](../Page/夏首勛.md "wikilink")、[邱林](../Page/邱林.md "wikilink")
  - 騎兵科(189)：[鄭大章](../Page/鄭大章.md "wikilink")、[龔浩](../Page/龔浩.md "wikilink")、[門炳岳](../Page/門炳岳.md "wikilink")、[張鉞](../Page/張鉞.md "wikilink")、[榮鴻臚](../Page/榮鴻臚.md "wikilink")、[王鎮淮](../Page/王鎮淮.md "wikilink")、[鄭潤成](../Page/鄭潤成.md "wikilink")
  - 炮兵科(185)：[董宋珩](../Page/董宋珩.md "wikilink")、[周玳](../Page/周玳.md "wikilink")、[魏益三](../Page/魏益三.md "wikilink")、[榮臻](../Page/榮臻.md "wikilink")、[李思愬](../Page/李思愬.md "wikilink")、[鄧演存](../Page/鄧演存.md "wikilink")（[漢陽兵工廠廠長兼任](../Page/漢陽兵工廠.md "wikilink")[漢陽兵工專門學校校長](../Page/漢陽兵工專門學校.md "wikilink")）
  - 工程兵科(94)：[張定璠](../Page/張定璠.md "wikilink")
  - 錙重兵科(81)：[張维藩](../Page/張维藩.md "wikilink")

<!-- end list -->

  - 肄業：[劉文島](../Page/劉文島.md "wikilink")、[曹浩森](../Page/曹浩森.md "wikilink")、[蔣光鼐](../Page/蔣光鼐.md "wikilink")、[季方](../Page/季方.md "wikilink")、[田頌堯](../Page/田頌堯.md "wikilink")

#### 第二期

(1914年初入學、1916年5-6月畢業、956名)

  - 步兵科(555)：[熊式輝](../Page/熊式輝.md "wikilink")、[劉峙](../Page/劉峙.md "wikilink")、[秦德純](../Page/秦德純.md "wikilink")、[廖磊](../Page/廖磊.md "wikilink")、[陶峙岳](../Page/陶峙岳.md "wikilink")、[祝紹周](../Page/祝紹周.md "wikilink")、[陳繼承](../Page/陳繼承.md "wikilink")、[劉尚志](../Page/劉尚志.md "wikilink")、[劉興](../Page/劉興.md "wikilink")、[陳驥](../Page/陳驥.md "wikilink")、[周斕](../Page/周斕.md "wikilink")、[朱傳經](../Page/朱傳經.md "wikilink")、[王懋功](../Page/王懋功.md "wikilink")(第一期肄業)、鲍文越、甄纪印
  - 騎兵科(137)：郑大章
  - 炮兵科(118)：[劉文輝](../Page/劉文輝.md "wikilink")、[施北衡](../Page/施北衡.md "wikilink")、[馮鵬翥](../Page/馮鵬翥.md "wikilink")、[劉超常](../Page/劉超常.md "wikilink")
  - 工程兵科(80)：[李雲傑](../Page/李雲傑.md "wikilink")、[蔡润生](../Page/蔡润生.md "wikilink")
  - 肄業：[陳銘樞](../Page/陳銘樞.md "wikilink")

#### 第三期

(1914年8月入學、1916年8月畢業、801名)

  - 步兵科(505)：[白崇禧](../Page/白崇禧.md "wikilink")、[張治中](../Page/張治中.md "wikilink")、[徐庭瑤](../Page/徐庭瑤.md "wikilink")、[何鍵](../Page/何鍵.md "wikilink")、[夏威](../Page/夏威.md "wikilink")、[黃紹竑](../Page/黃紹竑.md "wikilink")、[賀維珍](../Page/賀維珍.md "wikilink")、[陳安寶](../Page/陳安寶.md "wikilink")、[蕭山令](../Page/蕭山令.md "wikilink")、[戴戟](../Page/戴戟.md "wikilink")、[劉和鼎](../Page/劉和鼎.md "wikilink")、[毛秉文](../Page/毛秉文.md "wikilink")、[於達](../Page/於達.md "wikilink")、[姚純](../Page/姚純.md "wikilink")、[林薰南](../Page/林薰南.md "wikilink")、[李善後](../Page/李善後.md "wikilink")、[张责夫](../Page/张责夫.md "wikilink")、[吳石](../Page/吳石.md "wikilink")、[鄭仰榕](../Page/鄭仰榕.md "wikilink")、[葉琪](../Page/葉琪.md "wikilink")
  - 騎兵科(90)：
  - 炮兵科(127)：[劉建緒](../Page/劉建緒.md "wikilink")、[周碞](../Page/周碞.md "wikilink")、[陳焯](../Page/陳焯.md "wikilink")、[張義純](../Page/張義純.md "wikilink")、[徐祖貽](../Page/徐祖貽.md "wikilink")、[孔慶桂](../Page/孔慶桂.md "wikilink")、[呂煥炎](../Page/呂煥炎.md "wikilink")
  - 工程兵科(41)：不明
  - 錙重兵科(38)：不明
  - 肄業：[周鳳歧](../Page/周鳳歧.md "wikilink")

#### 第四期

(1915年秋入學、1917年秋畢業、209名)

  - 步兵科(209)：[胡宗鐸](../Page/胡宗鐸.md "wikilink")、[朱懷冰](../Page/朱懷冰.md "wikilink")、[尹呈輔](../Page/尹呈輔.md "wikilink")、[彭進之](../Page/彭進之.md "wikilink")、[汪之斌](../Page/汪之斌.md "wikilink")、[程汝懷](../Page/程汝懷.md "wikilink")、
    [余泽錢](../Page/余泽錢.md "wikilink")、 [陶鈞](../Page/陶鈞.md "wikilink")

#### 第五期

(1916年6月入學、1918年9月畢業、630名)

  - 步兵科(382)：[傅作義](../Page/傅作義.md "wikilink")、[張蔭梧](../Page/張蔭梧.md "wikilink")、[董英斌](../Page/董英斌.md "wikilink")、[楚溪春](../Page/楚溪春.md "wikilink")、[王靖國](../Page/王靖國.md "wikilink")、[劉翼飛](../Page/劉翼飛.md "wikilink")、延宗山、張赫炎
  - 騎兵科(79)：[趙承綬](../Page/趙承綬.md "wikilink")、[白濡青](../Page/白濡青.md "wikilink")
  - 炮兵科(91)：[吳克仁](../Page/吳克仁.md "wikilink")
  - 工程兵科(38)：、[嚴重](../Page/嚴重.md "wikilink")
  - 錙重兵科(40)：不明

#### 第六期

(1917年初入學、1919年春畢業、1333名)

  - 步兵科(875)：[顧祝同](../Page/顧祝同.md "wikilink")、[上官雲相](../Page/上官雲相.md "wikilink")、[郝夢齡](../Page/郝夢齡.md "wikilink")、[余漢謀](../Page/余漢謀.md "wikilink")、[鄧龍光](../Page/鄧龍光.md "wikilink")、[吳奇偉](../Page/吳奇偉.md "wikilink")、[韓德勤](../Page/韓德勤.md "wikilink")、[歐陽駒](../Page/歐陽駒.md "wikilink")、[趙博生](../Page/趙博生.md "wikilink")、[黃鎮球](../Page/黃鎮球.md "wikilink")、[繆培南](../Page/繆培南.md "wikilink")、[周渾元](../Page/周渾元.md "wikilink")、[陳公俠](../Page/陳公俠.md "wikilink")、[李漢魂](../Page/李漢魂.md "wikilink")、[葉肇](../Page/葉肇.md "wikilink")、[阮玄武](../Page/阮玄武.md "wikilink")、[韓漢英](../Page/韓漢英.md "wikilink")、[朱暉日](../Page/朱暉日.md "wikilink")、[周毓英](../Page/周毓英.md "wikilink")、[胡祖玉](../Page/胡祖玉.md "wikilink")、[呂瑞英](../Page/呂瑞英.md "wikilink")、[孔繁瀛](../Page/孔繁瀛.md "wikilink")、[譚邃](../Page/譚邃.md "wikilink")、[華振中](../Page/華振中.md "wikilink")、[李文田](../Page/李文田.md "wikilink")、[呂競存](../Page/呂競存.md "wikilink")
  - 騎兵科(141)：[覃連芳](../Page/覃連芳.md "wikilink")、[何柱國](../Page/何柱國.md "wikilink")、[彭毓斌](../Page/彭毓斌.md "wikilink")、[魏文華](../Page/魏文華.md "wikilink")
  - 炮兵科(148)：[彭位仁](../Page/彭位仁.md "wikilink")、[郭懺](../Page/郭懺.md "wikilink")、[黃琪翔](../Page/黃琪翔.md "wikilink")、[邵百昌](../Page/邵百昌.md "wikilink")、[朱煥文](../Page/朱煥文.md "wikilink")、[方清湘](../Page/方清湘.md "wikilink")
  - 工程兵科(88)：[鄧演達](../Page/鄧演達.md "wikilink")、[葉挺](../Page/葉挺.md "wikilink")、[樊崧甫](../Page/樊崧甫.md "wikilink")、[楊宏光](../Page/楊宏光.md "wikilink")、[李振球](../Page/李振球.md "wikilink")、[林廷華](../Page/林廷華.md "wikilink")、甘芳、卢佐、[于永泉(鋆生)](../Page/于永泉\(鋆生\).md "wikilink")
  - 錙重兵科(81)：[劉茂恩](../Page/劉茂恩.md "wikilink")、[李揚敬](../Page/李揚敬.md "wikilink")
  - 肄業：[薛岳](../Page/薛岳.md "wikilink")

#### 第七期

(1917年秋入學、1919年秋畢業、191名)

  - 步兵科(146)：[黃維剛](../Page/黃維剛.md "wikilink")、[張新三](../Page/張新三.md "wikilink")
  - 騎兵科(45)：[陳長捷](../Page/陳長捷.md "wikilink")、[劉堯宸](../Page/劉堯宸.md "wikilink")
  - 其他兵科：不明

#### 第八期

(1918年8月入學、1922年7月畢業、638名)

  - 步兵科(411)：[周至柔](../Page/周至柔.md "wikilink")、[裴昌會](../Page/裴昌會.md "wikilink")、[劉膺古](../Page/劉膺古.md "wikilink")、[王以哲](../Page/王以哲.md "wikilink")、[劉珍年](../Page/劉珍年.md "wikilink")、[胡伯翰](../Page/胡伯翰.md "wikilink")、[劉春榮](../Page/劉春榮.md "wikilink")、[王育瑛](../Page/王育瑛.md "wikilink")、[劉奉濱](../Page/劉奉濱.md "wikilink")、[劉廣濟](../Page/劉廣濟.md "wikilink")、[韓錫侯](../Page/韓錫侯.md "wikilink")、[孔令恂](../Page/孔令恂.md "wikilink")、[李士林](../Page/李士林.md "wikilink")、[王景宋](../Page/王景宋.md "wikilink")、[史澤波](../Page/史澤波.md "wikilink")、[古鼎華](../Page/古鼎華.md "wikilink")、[郜子舉](../Page/郜子舉.md "wikilink")、[傅仲芳](../Page/傅仲芳.md "wikilink")、[陳孔達](../Page/陳孔達.md "wikilink")、[杨晓轩](../Page/杨晓轩.md "wikilink")
    金德洋
  - 騎兵科(65)：[郗恩綏](../Page/郗恩綏.md "wikilink")
  - 炮兵科(97)：[陳誠](../Page/陳誠.md "wikilink")、[于浚都](../Page/于浚都.md "wikilink")、[羅卓英](../Page/羅卓英.md "wikilink")、[宋肯堂](../Page/宋肯堂.md "wikilink")、[鄒洪](../Page/鄒洪.md "wikilink")、[史文桂](../Page/史文桂.md "wikilink")、[郭思演](../Page/郭思演.md "wikilink")、[馬法五](../Page/馬法五.md "wikilink")、[高卓東](../Page/高卓東.md "wikilink")
  - 工程兵科(37)：[柳際明](../Page/柳際明.md "wikilink")、[馬鳳崗](../Page/馬鳳崗.md "wikilink")
  - 錙重兵科(28)：不明

#### 第九期

(1919年8月入學、1923年8月畢業、702名)

  - 步兵科(423)：[何基灃](../Page/何基灃.md "wikilink")、[張克俠](../Page/張克俠.md "wikilink")、[李覺](../Page/李覺.md "wikilink")、[周福成](../Page/周福成.md "wikilink")、[邊章五](../Page/邊章五.md "wikilink")、[宋邦榮](../Page/宋邦榮.md "wikilink")、[夏國璋](../Page/夏國璋.md "wikilink")、[施中誠](../Page/施中誠.md "wikilink")、[張壽齡](../Page/張壽齡.md "wikilink")、[張文清](../Page/張文清.md "wikilink")、[牟中珩](../Page/牟中珩.md "wikilink")、[劉萬春](../Page/劉萬春.md "wikilink")、[賀粹之](../Page/賀粹之.md "wikilink")、[黃延禎](../Page/黃延禎.md "wikilink")
  - 騎兵科(76)：[李宗弼](../Page/李宗弼.md "wikilink")
  - 炮兵科(121)：[董振堂](../Page/董振堂.md "wikilink")、[郭寄嶠](../Page/郭寄嶠.md "wikilink")、[黎行恕](../Page/黎行恕.md "wikilink")、[劉多荃](../Page/劉多荃.md "wikilink")、[王晉](../Page/王晉.md "wikilink")
  - 工程兵科(40)：[陳寶倉](../Page/陳寶倉.md "wikilink")、林柏森 、桂丰荣
  - 錙重兵科(42)：不明
  - 肄業：[展書堂](../Page/展書堂.md "wikilink")

## 辎重科六期一连同学录

信文灿、唐宗尧、程堪、杨正坤、李宗程、李如枫、黄志勋、赵恩煦、胡祖舜、彭德云、成杰、万耀中、侯尊召、江孝纯、窦桂芳、梁殿枢、金锋、侯谷、
范树珍、饶汉杰、侯光龙、郭之缙、周庆桐、卢景鉴、温力正、陈国梁、任为凯、粟福畴、梁宗标、张联辉、刘珩、高岳嵩、尹邦光、徐鸿晋、魏朝纲、
彭济伟、蔡嗣雄、叶裳、沈文诰、刘福德、岑荣宗、金履新、曾泽寰、温定国、李务滋、陈荣宗、杨义、谭时、张伟、梁毓藻、王世康、李扶疏、李扬敬、林振镛、林廷华、黄盛昆、简作桢、马克仑、徐金龙、刘茂恩、李经田、孙毓英、王经邦、李世儒、汤文彦、孙棣生、张家锟、马负龙、蔡春田、段桂明、王清瀚、李铨、贺学礼、董汝梅、范锡庚、董汝桂、杨泽沛、陈宝增、王得来、董玉玢、贾成勋，共81名。

## 參考文獻

  - 《保定陸軍軍官學校同學錄》，中國社會科學院近代史研究所提供。

## 外部連結

  - [中國保定市信息](https://web.archive.org/web/20050320011028/http://www.bd.gov.cn/shsh/lyzc/jdjs/jx.htm)
  - [國防大學](http://www.ndu.edu.tw)
  - [千余名將軍的搖籃：風雲之中的保定軍校](http://culture.ifeng.com/wenhuadaguanyuan/detail_2011_06/02/6788625_0.shtml)

[Category:中华民国大陆时期军事学校](../Category/中华民国大陆时期军事学校.md "wikilink")
[Category:河北全国重点文物保护单位](../Category/河北全国重点文物保护单位.md "wikilink")
[Category:1902年創建的教育機構](../Category/1902年創建的教育機構.md "wikilink")
[Category:1923年廢除](../Category/1923年廢除.md "wikilink")
[Category:清朝军事学校](../Category/清朝军事学校.md "wikilink")
[保定陸軍軍官學校](../Category/保定陸軍軍官學校.md "wikilink")