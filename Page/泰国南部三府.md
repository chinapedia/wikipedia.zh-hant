[Souththailandmap.GIF](https://zh.wikipedia.org/wiki/File:Souththailandmap.GIF "fig:Souththailandmap.GIF")
[Flag_of_Pattani.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Pattani.svg "fig:Flag_of_Pattani.svg")

[泰国南部总共有十三府](../Page/泰国南部地区.md "wikilink")，其中[也拉府](../Page/也拉府.md "wikilink")、[北大年府和](../Page/北大年府.md "wikilink")[那拉提瓦府在](../Page/那拉提瓦府.md "wikilink")[馬來語被稱作](../Page/馬來語.md "wikilink")「**Patani**」（[爪夷文](../Page/爪夷文.md "wikilink")：
ڤتاني），為泰國在1909年根據《[英暹條約](../Page/1909年英國－暹羅條約.md "wikilink")》取得，居民主要為信[伊斯蘭教的](../Page/伊斯蘭教.md "wikilink")[馬來人](../Page/馬來人.md "wikilink")，與泰國其他地區較不同，因而在文化上有很大的差別，並存在[分離主義](../Page/分離主義.md "wikilink")。

## 宗教冲突

2004年4月28日清晨，在[北大年](../Page/北大年府.md "wikilink")、[宋卡府的部分地区发生](../Page/宋卡府.md "wikilink")‘[库塞清真寺惨案](../Page/北大年府#社会动荡.md "wikilink")’，也种下了本地区[宗教](../Page/宗教.md "wikilink")、[民族冲突的祸根](../Page/民族.md "wikilink")。

这里[佛教](../Page/佛教.md "wikilink")[庙宇几乎每天都面对被恐怖](../Page/庙.md "wikilink")[炸弹袭击的阴影](../Page/炸弹.md "wikilink")，[泰国军方特别增派官兵实行](../Page/皇家泰国陆军.md "wikilink")[军事管治](../Page/军事管治.md "wikilink")。电视上时常播出[士兵保护穿着](../Page/士兵.md "wikilink")[防弹背心的](../Page/防弹背心.md "wikilink")[和尚们朝晨到](../Page/和尚.md "wikilink")[市镇上](../Page/市镇.md "wikilink")[化缘的镜头](../Page/化缘.md "wikilink")。

但是近来(2007年以来)三府内也有几家[清真寺](../Page/清真寺.md "wikilink")（包括上文中的库塞清真寺）遭到恐怖[炸弹](../Page/炸弹.md "wikilink")、[手榴弹和土制](../Page/手榴弹.md "wikilink")[火箭炮](../Page/火箭炮.md "wikilink")（英文缩写：）袭击，数名[朝拜者不幸遇难](../Page/朝拜.md "wikilink")。

## 教育体系

泰南三府的主要语言是[亚维语而非](../Page/亚维语.md "wikilink")[泰语](../Page/泰语.md "wikilink")，亚维语即用泰文字母书写的[马来语](../Page/马来语.md "wikilink")。这三府中的学校有遵照[泰国教育大纲的](../Page/泰国.md "wikilink")[泰语学校及](../Page/泰语.md "wikilink")[伊斯蘭學校](../Page/伊斯蘭學校.md "wikilink")（当地称为ponoh）。

当地的[泰语学校因为被某些穆斯林反叛者认为她们代表泰国当局推行强制性](../Page/泰语.md "wikilink")[泰化政策而招愤恨](../Page/泰化.md "wikilink")，经常被人[纵火袭击](../Page/纵火.md "wikilink")，至2007年3月，已经有二百多所中、小学校被纵火烧毁，六十名多教师被袭丧生，甚至有一名女教师在对学生进行家访后被人用乱棒活活打死，有二名教师在教室门外被当着学生的面[枪杀后再被](../Page/枪杀.md "wikilink")[纵火焚烧](../Page/纵火.md "wikilink")。

但是作为对穆斯林暴力恐怖的报复，近来三府内也有数间马来穆斯林学校被恐怖[炸弹袭击和遭到乱枪](../Page/炸弹.md "wikilink")[扫射](../Page/扫射.md "wikilink")，有[穆斯林学童不幸遇难](../Page/穆斯林.md "wikilink")。

[联合国官员因此曾呼吁在泰国南部停止针对孩童的恐怖暴力事件](../Page/联合国.md "wikilink")。

## 人口迁移

因为[暴力事件增多](../Page/暴力.md "wikilink")，极大影响了当地人民的正常生活。许多人离乡背井到外处谋生，据报导已有二万五千人迁移到泰国南部其他相对太平一些的府份。而至少五万五千人则流落到[马来西亚及](../Page/马来西亚.md "wikilink")[印度尼西亚非法长期居留及工作](../Page/印度尼西亚.md "wikilink")。大规模人口迁移使得当地的[民族问题变得](../Page/民族问题.md "wikilink")[国际化](../Page/国际问题.md "wikilink")。

## 经济影响

泰南三府原是世界上出产[橡胶最多的地区之一](../Page/橡胶.md "wikilink")。但是[暴力事件使得](../Page/暴力.md "wikilink")[种植园](../Page/种植园.md "wikilink")[橡胶产量急剧下降](../Page/橡胶.md "wikilink")，当地政府的税收因此减少約百分之十至二十之間。

## 邻近府份

[宋卡府的渣那区](../Page/宋卡府.md "wikilink")（จะนะ
，Chana）、堤坝区（เทพา，Thepa）和沙巴愉区（สะบ้าย้อย
，Saba
Yoi），由于这三个区原先也是从[北大年府中划分割去的](../Page/北大年府.md "wikilink")，所以在广义上往往也将这三个小区域看作是泰南三府的一部份。

[泰国](../Page/泰国.md "wikilink")[警方怀疑有](../Page/警方.md "wikilink")
[恐怖分子在堤坝区及渣那区藏身](../Page/恐怖分子.md "wikilink")。并曾在[合艾国际机场](../Page/合艾国际机场.md "wikilink")、[合艾火车站](../Page/合艾火车站.md "wikilink")、戴安娜百货公司、欧鼎百货公司、莲花超市、[家乐福超市制造过多起恐怖炸弹袭击事件](../Page/家乐福.md "wikilink")。

其他邻近的[博他侖府](../Page/博他侖府.md "wikilink")、[沙敦府](../Page/沙敦府.md "wikilink")、[宋卡府](../Page/宋卡府.md "wikilink")（除了上文提到两地区）、[董里府各自维持表面平静](../Page/董里府.md "wikilink")。但近来有一些[政府机关接获以](../Page/政府.md "wikilink")[亚维语书写的炸弹袭击](../Page/亚维语.md "wikilink")[恐吓传单](../Page/恐吓信.md "wikilink")。所以泰国警方增派人手及在市镇上加装电视监控摄影机，[军方也增强了戒备](../Page/皇家泰国陆军.md "wikilink")。

## 邻近国家

[马来西亚的](../Page/马来西亚.md "wikilink")[玻璃市](../Page/玻璃市.md "wikilink")、[吉打州](../Page/吉打州.md "wikilink")、[吉兰丹州与](../Page/吉兰丹.md "wikilink")[霹雳州同三府接壤](../Page/霹雳州.md "wikilink")，鉴于马来西亚人民与以上三府人民长期以来保持极浓厚的[亲缘关系](../Page/亲缘关系.md "wikilink")，马来西亚政府长期以来致力于帮助泰国政府谋求以[和平方式解决矛盾与争端](../Page/和平.md "wikilink")。

[马来西亚於](../Page/马来西亚.md "wikilink")2007年初鉴于泰方请求，也同意加强[边境戒备](../Page/边境.md "wikilink")，并与[泰国合筑高墙以阻挡](../Page/泰国.md "wikilink")[非法越境者偷运](../Page/非法越境.md "wikilink")[武器](../Page/武器.md "wikilink")。

{{-}}

## 參考資料

[Category:泰南三府](../Category/泰南三府.md "wikilink")
[Category:泰國南部](../Category/泰國南部.md "wikilink")