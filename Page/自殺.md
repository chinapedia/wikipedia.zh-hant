**自殺**是指當事者蓄意使自己[死亡的行為](../Page/死亡.md "wikilink")\[1\]。試圖自殺或非致命的自殺行為一般視為[自殘](../Page/自殘.md "wikilink")，有自殺的意圖，但後來沒有死亡\[2\]。[协助自杀是指一個人藉由提供建議或是其他方式](../Page/协助自杀.md "wikilink")，協助有死亡意願者達成目的，但沒有直接參與導致死亡的過程\[3\]，這和[安樂死不同](../Page/安樂死.md "wikilink")，在安樂死中他人有直接參與導致死亡的過程\[4\]。是有自殺的想法\[5\]，有可能會演變成。

自殺的風險因素包括[憂鬱症](../Page/憂鬱症.md "wikilink")、[躁鬱症](../Page/躁鬱症.md "wikilink")、[思覺失調症](../Page/思覺失調症.md "wikilink")、[人格障礙或是](../Page/人格障礙.md "wikilink")[物質濫用在內的](../Page/物質濫用.md "wikilink")[心理疾病](../Page/心理疾病.md "wikilink")\[6\]\[7\]，其他因素還有因為經濟問題、人際問題或是[霸凌而有的壓力而產生的](../Page/霸凌.md "wikilink")[衝動行為](../Page/衝動_\(心理學\).md "wikilink")\[8\]\[9\]。以往曾試圖自殺的人，再度自殺的可能性也較高\[10\]。[自殺防制的方式包括管制可能用來自殺的物品](../Page/自殺防制.md "wikilink")（如[槍枝及毒藥](../Page/槍枝.md "wikilink")），提高取得的難度，治療心理疾病及物質濫用，改善媒體報導自殺的方式，及提升經濟條件等\[11\]。[生命線之類的協談電話雖常見](../Page/生命線_\(熱線\).md "wikilink")，但到2016年時，有關其成效的研究還不太充份\[12\]\[13\]。

常用的[自殺方法會隨著地區而不同](../Page/自殺的方法.md "wikilink")，也會和自殺工具是否容易取得有些關係\[14\]。常用的方式包括[上吊](../Page/上吊自杀.md "wikilink")、及[枪械](../Page/枪械.md "wikilink")\[15\]。自殺造成2013年842000人死亡，較1990的712000人增加\[16\]，使之成為全球第十大死因\[17\]\[18\]。通常男性自殺完成的比率高於女性，而男性想要自殺的比率也是女性的四至五倍\[19\]。估計每年有一千萬至二千萬人試圖自殺\[20\]，試圖自殺可能會造成受傷甚至長期的失能
。在西方國家中，年輕人較容易有試圖自殺的情形，女性發生比率是男性的五倍\[21\]。

一直以來，人們對於自殺的看法受到宗教、[榮譽感和](../Page/榮譽感.md "wikilink")[人生意義這類普遍](../Page/人生意義.md "wikilink")[存在的議題所影響](../Page/存在.md "wikilink")\[22\]\[23\]。[亞伯拉罕諸教認為](../Page/亞伯拉罕諸教.md "wikilink")，所以自殺[有違上帝旨意](../Page/原罪.md "wikilink")\[24\]。在[日本武士時代](../Page/日本武士.md "wikilink")，[切腹自殺象徵對失敗負責或表達抗議](../Page/切腹.md "wikilink")\[25\]。印度習俗「[娑提](../Page/娑提_\(習俗\).md "wikilink")」，即丈夫過世後，[寡婦因為自願或迫於家庭和社會壓力](../Page/寡婦.md "wikilink")，在丈夫的中跟著[自焚殉葬](../Page/自焚.md "wikilink")，後來在[英國統治期間遭到禁止](../Page/英屬印度.md "wikilink")\[26\]。現在大部分西方國家已不再將自殺和自殺未遂視為非法\[27\]，但自殺在許多國家仍屬於犯罪行為\[28\]。在20和21世紀，自殺已成為特殊情況下的抗議形式，
[敢死隊和](../Page/敢死隊.md "wikilink")
[自殺炸彈則用來作為軍事戰略或恐怖攻擊](../Page/自殺炸彈.md "wikilink")\[29\]。自殺一詞源於拉丁語suicidium，意為「將自己殺死」\[30\]。

2011年發表在上，追蹤台灣1080位社區自殺嘗試（自我傷害）者5年的研究，去探討這些被認為自殺最高危險群的人最後真的死於自殺原因的人有多少。在嘗試自殺後5年內，死於自殺的比例有3.8%\[31\]。

## 詞源

自殺在中文又名自盡、自絕，特指某種自殺方式的詞彙有自縊、自裁、自刎、飲彈。

「自杀」这个字（源自[Latin拉丁语](../Page/Latin.md "wikilink")*suicidium*一字，是sui（「本身的」）与cidium（「杀」）的结合，代表「杀害自己」的意思），是故意杀害自己的行为。自杀通常是由于[绝望](../Page/绝望.md "wikilink")（Despair
(emotion)）, 其原因经常是归咎于[精神障碍](../Page/精神障碍.md "wikilink")（mental
disorder）例如[抑郁症](../Page/抑郁症.md "wikilink")（major depressive
disorder）、 [躁郁症](../Page/躁郁症.md "wikilink")（bipolar disorder）、
[精神分裂症](../Page/精神分裂症.md "wikilink")（schizophrenia）、酗酒或[药物滥用](../Page/药物滥用.md "wikilink")（Substance
abuse）。\[32\]压力因素如[经济困难](../Page/经济困难.md "wikilink")（financial
difficulties）或[人际关系](../Page/人际关系.md "wikilink")（interpersonal
relationship）问题通常与之有关。防止自杀工作包括限制接近枪械的机会、治疗精神障碍和药物误用、以及改善经济发展。

## 人類的自殺

现实的研究大多集中在人类的自杀行为上，尽管已经存在一些研究活动是在研究其它[物种的自杀行为](../Page/物种.md "wikilink")。人类的自杀就是指自杀的当事者是人类，一般情况下自杀的当事者是一个当事人。

在很多[宗教中](../Page/宗教.md "wikilink")，自殺是罪惡的，有些[國家的](../Page/國家.md "wikilink")[法律禁止自殺或協助他人自殺](../Page/法律.md "wikilink")，但在部份[國家的](../Page/國家.md "wikilink")[文化中](../Page/文化.md "wikilink")，自殺有時是具有榮譽感的行為，並獲得[輿論的肯定](../Page/輿論.md "wikilink")，如历史上舰只沉没船长可能会因其职责而拒绝逃生。阿拉伯宗教认为自杀是不光彩的行为；[佛教认为](../Page/佛教.md "wikilink")，自杀仍是殺生\[33\]，不能逃脱罪孽和痛苦，而是犯了非常重的杀戒，是大罪，会堕落地狱；在西方，出于宗教信仰中生命的神圣属性，自杀经常被看作严重的犯罪行为和对上帝的冒犯。如[天主教及](../Page/天主教.md "wikilink")[基督教反對任性](../Page/基督教.md "wikilink")、無意義的自杀，但不反對能成就人性意義的自殺（如殉道）\[34\]。

自殺的原因多和[心理層面相關](../Page/心理.md "wikilink")，因此是[心理學的](../Page/心理學.md "wikilink")[研究課題之一](../Page/研究.md "wikilink")，另外也與[憂鬱症](../Page/憂鬱症.md "wikilink")、[躁鬱症等](../Page/躁鬱症.md "wikilink")[精神疾病有相當的關連](../Page/精神疾病.md "wikilink")，故亦為[精神醫學的研究範疇](../Page/精神醫學.md "wikilink")。在[現代](../Page/現代.md "wikilink")[精神醫學中](../Page/精神醫學.md "wikilink")，對自殺有一個基本的假定：「自殺總是發生在非正常狀態下，或是[社會偏離了常態](../Page/社會.md "wikilink")，或是一個人的[精神狀況偏離了常態](../Page/精神.md "wikilink")。」

於中小學校之心理教育輔導中自殺亦為重要課題，一般認為[青少年的自殺易受](../Page/青少年.md "wikilink")[新聞報導及同伴影響](../Page/新聞.md "wikilink")。[中華人民共和國衛生部於](../Page/中華人民共和國衛生部.md "wikilink")2004年稱每年有28.7萬多人自殺\[35\]。在[台灣](../Page/台灣.md "wikilink")，自殺於1997年首次進入十大死因排行榜，2005年有超過四千人自殺身亡。\[36\][世界卫生组织的统计显示](../Page/世界卫生组织.md "wikilink")，2015年平均每40秒就有一个人自杀，使自杀成为全球15\~29歲青少年主要致死因素之一\[37\]。

## 自殺的分類

在動機上主要可分為兩種：

主動型自殺，為達成個人因素以外的目的而進行的自殺。例如，自殺性襲擊是為達到某種目的而自殺。

被動型自殺，僅個人因素導致的自殺。比如逃離性自殺，例如社會壓力而產生[心理疾病](../Page/心理疾病.md "wikilink")、藥物、[醉酒](../Page/醉酒.md "wikilink")、[物質濫用等](../Page/物質濫用.md "wikilink")，是外因導致其自殺。

一般來說，主動者受時空背景影響至甚。相較於主動者，被動者在自殺意念發生時，呈現較明顯的低[自尊與負向](../Page/自尊.md "wikilink")[思維傾向](../Page/思維.md "wikilink")。缺乏[自信的人較可能發生被動型自殺](../Page/自信.md "wikilink")，[憂鬱症患者的自殺幾乎全屬被動型](../Page/憂鬱症.md "wikilink")。

[文明發展愈高的地區](../Page/文明.md "wikilink")，對社會的不滿愈少，故被動型的比率愈高。

主動型自殺

  - 自殺式襲擊：犧牲自己以完成攻擊目的。如[神風特攻隊](../Page/神風特攻隊.md "wikilink")、[恐怖攻擊中的自殺炸彈客](../Page/恐怖攻擊.md "wikilink")。
  - 政治目的性自殺：為傳達某種理念而自殺，例如死諫、殉情或以死明志，例如在某重要地標自焚。此類自殺亦常見要求他人協助完成者。

被動型自殺

  - 脅迫性自殺：為了控制他人（罪惡感、恐懼）而自殺。因認為自己遭到某人惡意待遇（因動機來自感受到他人惡意，故列為被動型），而以死的方式作為報復手段。某些地方文化認為人死後產生的[鬼可代為復仇](../Page/鬼.md "wikilink")，亦屬這一類。
  - 責任性自殺：為了顯示責任心而自殺，如早期日本在戰敗時的切腹自殺（因具備文化因素，亦含主動性成份）。在現代，此類自殺常見於[強迫性格傾向者](../Page/強迫型人格障礙.md "wikilink")。
  - 逃離性自殺：為了躲避某種事物（如惡疾、痛苦、恐慌、空虛等感受），或是被某事逼迫（如被逼債、畏罪、政權垮台），因而自殺。

在行為模式上的類型有：

  - 安樂死：個體以藉助他人幫助的形式來完成自己的自殺。
  - 謀殺自殺：一個人用謀殺的方式，讓另一個人或幾個人在他本人之前或同時死去。
  - 相約自殺：兩個或兩個以上的個體之間立下協定，通常是因個人原因，同時或先後自殺。
  - [集體自殺](../Page/集體自殺.md "wikilink")：集體自殺指一群人為了同一目的而自殺或互相殺害，而這通常與真實或意識到的迫害有關。

## 有自殺傾向的初期警號

想自殺的人可能會在自殺前數天、數星期或數月有以下的徵狀：\[38\]

  - 表示自己一事無成、沒有希望或感到絕望
  - 感到極度挫敗、羞恥或內疚
  - 心中感到愤怒，痛苦，负面情绪无法倾诉和宣泄，感到自己一点一点被瓦解，希望得到解脱或重生
  - 曾經寫出或說出想自殺
  - 談及「死亡」、「離開」及在不尋常情況下說「再見」
  - 將至愛的物品送走
  - 了無生趣，對任何事情都不感興趣
  - 避開朋友或親人、不想和人溝通或希望獨處
  - 性格或儀容劇變
  - 作出一些失去理性或怪異的行為
  - 情緒反覆不定，由沮喪或低落變得異常平靜開心
  - 長期失業
  - 久病厭世
  - 長期無法產生悲傷的情緒或壓抑自己的情緒過久
  - 精神狀況處於失控
  - 情感失控

## 统计数字

\[\[<File:Self-inflicted> injuries world map - Death -
WHO2004.svg|thumb|2004世界各国自杀率（每年每10万人中自杀人数）示意图\[39\]

<div class="references-small" style="-moz-column-count:3; column-count:3;">

</div>

\]\]

企图自杀的女性数量大约是男性的三倍，她们通常采用服用大量安眠药或其他药剂的方式来自杀。男性一般会选择更为有效的措施来自杀，例如开枪或者[上吊这种很快就能结束生命的方式](../Page/上吊.md "wikilink")。因而真正因自杀而死亡人数，男性是女性的3倍还要多。\[40\]

### 中国大陆

{{-}}

### 台灣

[中華民國79-106全國自殺死亡人數.jpg](https://zh.wikipedia.org/wiki/File:中華民國79-106全國自殺死亡人數.jpg "fig:中華民國79-106全國自殺死亡人數.jpg")
2012年自殺人數3766人，2012年男性自殺已遂人數較前年上升1%，女性則較前年上升19%，自殺原因則以「憂鬱傾向」因素為最大宗，「家庭成員問題」以及「感情」分別為第二、第三順位之誘發因素。\[41\]

近年來，台灣的全年自殺身亡人數平均落在3500到4000人附近，平均每兩個小時就有一個人自殺身亡。
自殺曾經在1993年到2009年名列台灣的十大死因。現為台灣第十一大死因。

### 香港

2017年全年自殺死亡個案共916宗，按年跌38宗，惟19歲以下青少年自殺死亡個案卻增加，從2016年24宗增至2017年36宗，增加了五成。青少年自殺主要原因為學業及家庭問題，其次為生活不開心及情緒病\[42\]\[43\]\[44\]。

性別比例方面，男性自殺死亡佔62%，女性則佔38%，[香港撒瑪利亞防止自殺會認為數字上的迴異源於兩性表達能力的差異](../Page/香港撒瑪利亞防止自殺會.md "wikilink")，並推斷男性自尊心較強，不願面對面與人傾談心事，宣洩情感\[45\]。

### 韓國

### 日本

日本[警察厅公布自杀统计显示](../Page/警察廳_\(日本\).md "wikilink")，2000年后已经连续14年有超过3万人自杀。2009年日本全国自杀人数暂定值3万2753人中，男性为2万3406人，女性为9347人，比2008年再增504人。从月份来看，去年3月至5月自杀人数最多，每月超过3千人，这一时期正是年度决算期，经济因素可能是自杀的诱因。2008年版日本自杀[白皮书显示](../Page/白皮书.md "wikilink")，自杀动机主要是经济、健康和家庭等。自杀群体中无业者所占比率过半数，为56.7%，远远超过其他群体的自杀人数。2008年版自杀白皮书显示，2007年日本自杀死亡率（10万人中自杀人数）达25.3，这大大高于[世界卫生组织做出的世界平均自杀率每](../Page/世界卫生组织.md "wikilink")10万16人的估算。在工业国家当中，日本自杀率居榜首。\[46\]

日本政府自2008年提出要在未来九年里将自杀率减少20%的目标。[民主党新政权成立后也立即组建了自杀对策紧急战略小组](../Page/民主党_\(1998年\).md "wikilink")，并在职业介绍所配备了精神保健专业人员。\[47\]

### 德国

### 奥地利

## 刑責

在大部份地區，自殺不是犯罪。但與人合謀自殺（相約一起自殺），或是協助他人自殺，且如果未全部死亡，则尚存活的人要負刑責，且大多為重罪。

现在在大部分西方国家已不再被刑事处罚。在大部分伊斯兰国家它仍是刑事罪。在20和21世纪，以[自焚](../Page/自焚.md "wikilink")（self-immolation）形式的自杀已被用作一种抗议手段，而[神风特攻队](../Page/神风特攻队.md "wikilink")（kamikaze）和[自杀性爆炸](../Page/自杀性爆炸.md "wikilink")（suicide
bombings）被用作一种军事或恐怖主义策略。\[48\]

## 危险因素

[Suicide_cases_from_16_American_states_(2008).png](https://zh.wikipedia.org/wiki/File:Suicide_cases_from_16_American_states_\(2008\).png "fig:Suicide_cases_from_16_American_states_(2008).png")

影响自杀风险的因素包括：

  - [精神疾病](../Page/精神疾病.md "wikilink")（mental disorder）
  - [药物误用](../Page/药物误用.md "wikilink")（drug misuse）
  - 心理状态、文化、家庭和社会情况、以及遗传。\[49\]

[精神疾病](../Page/精神疾病.md "wikilink")（mental
disorder）和药物误用经常共同存在\[50\]。其他危险因素包括曾企图自杀\[51\]、执行自杀方法的易可得性、自杀的家族史或的存在\[52\]。例如，发现自杀率在存在枪械的家庭高于没有枪械的家庭\[53\]。[社会经济](../Page/社会经济.md "wikilink")（socio-economic）因素如失业、贫穷、[无家可归](../Page/露宿者.md "wikilink")（homelessness）和歧视可能会引发自杀念头\[54\]约15–40%的人会留下[自杀遗书](../Page/遗书.md "wikilink")（suicide
note）。\[55\]。遗传似乎解释了38%至55%的自杀行为\[56\]。[退伍军人](../Page/退伍军人.md "wikilink")（War
veteran）在一定程度上存在与[战争](../Page/战争.md "wikilink")（war）有关的精神疾病和身体健康问题的较高发生率，所以有较高的自杀风险\[57\]。

### 精神障碍

[精神障碍](../Page/精神障碍.md "wikilink")（mental
disorder）通常在自杀时出现，估计发生率由27%\[58\]。至多于90%\[59\]。其中曾入住精神病院的人，他们完结自杀的终生风险是约8.6%\[60\]。全部自杀死亡者的半数可能有[重抑症](../Page/重性抑郁障碍.md "wikilink")（major
depressive disorder）；出现这个疾病或其他[情绪障碍](../Page/情绪障碍.md "wikilink")（mood
disorder）的其中一种，例如[躁郁症](../Page/躁郁症.md "wikilink")（bipolar
disorder），皆会增加自杀率20倍\[61\]。其他牵连的情况包括[思覺失調症](../Page/思覺失調症.md "wikilink")(14%)、[人格障碍](../Page/人格障碍.md "wikilink")（personality
disorder） (14%)\[62\]、[躁郁症](../Page/躁郁症.md "wikilink")（bipolar
disorder）\[63\]和[創傷後壓力症候群](../Page/創傷後壓力症候群.md "wikilink")\[64\]。约5%[思覺失調症病人死于自杀](../Page/思覺失調症.md "wikilink")\[65\]。[饮食失调症](../Page/饮食失调症.md "wikilink")（Eating
disorder）是另一个高危情况\[66\]。

过去有自杀企图的病史是最终完成自杀的最佳预测指标\[67\]。约20%的自杀者曾有自杀企图，并曾企图自杀的1%人在一年内会完成自杀\[68\]；多于5%的人在10 年后会自杀\[69\]。虽然[自残的行为并不视为自杀企图](../Page/自残.md "wikilink")，但是自我伤害行为的出现与自杀风险增高有关\[70\]。

约80%的完结自杀者在其死亡前的一年内曾就诊，\[71\]包括45%的人在过去的一个月内曾就诊\[72\]。约25–40%完成自杀的人在过去的一年内曾接触心理卫生服务\[73\]\[74\]。

### 药物滥用

[The_Drunkard's_Progress_1846.jpg](https://zh.wikipedia.org/wiki/File:The_Drunkard's_Progress_1846.jpg "fig:The_Drunkard's_Progress_1846.jpg")

[药物滥用](../Page/药物滥用.md "wikilink")（substance
abuse）是继[重抑症](../Page/重性抑郁障碍.md "wikilink")（major
depression）和[躁郁症](../Page/躁郁症.md "wikilink")（bipolar
disorder）之后第二最常见自杀的[风险因素](../Page/风险因素.md "wikilink")（risk
factor）\[75\]。慢性药物误用与有关连\[76\]\[77\]。如果合并个人的悲伤例如[丧亲](../Page/丧恸.md "wikilink")（grief），风险便会进一步增加\[78\]。同时，药物误用与精神障碍有关连\[79\]。

大多数的人自杀时皆受到[镇静安眠药](../Page/镇静安眠药.md "wikilink")（sedative）（例如酒精或苯二氮卓类药物）的影响\[80\]，其中15%至61%的人出现酗酒问题\[81\]。有较高的酒精使用率和较高密度酒吧之国家通常亦有较高的自杀率\[82\]，而此关连主要是与[烈酒](../Page/烈酒.md "wikilink")（distilled
spirit）使用而并不是与全部酒精使用有关\[83\]。在其一生中某个
时间点曾接受酗酒治疗的约2.2–3.4%人死于自杀\[84\]。有自杀企图的酗酒者通常是男性、年纪较大并过去曾企图自杀\[85\]。3至35%使用海洛英的死亡个案是死于自杀（高于不使用海洛英的人约14倍）\[86\]。

[可卡因](../Page/可卡因.md "wikilink")（cocaine）和[甲基苯丙胺](../Page/甲基苯丙胺.md "wikilink")（methamphetamine）的误用与自杀有高相关\[87\]\[88\]。使用可卡因的人在戒断期的风险最高\[89\]。使用[吸入剂](../Page/吸入剂.md "wikilink")（inhalant
abuse）的人亦有较高的风险，其中约20%的人在某个时间点曾企图自杀，同时多於65%曾考虑自杀。\[90\][吸烟](../Page/吸烟.md "wikilink")（tobacco
smoking）与自杀风险有关连\[91\]。关于此关连出现的原因的证据极少；不过假设有吸烟倾向的人亦存在自杀倾向，并且吸烟引起的健康问题也使人其后想结束生命，并且吸烟影响脑化学作用，引起自杀倾向\[92\]。但[大麻](../Page/大麻.md "wikilink")（cannabis）似乎并不单独增加此风险\[93\]。

### 嗜赌

与一般人群相比，[嗜赌与日益增涨的](../Page/赌博成瘾.md "wikilink")[自杀意念和企图紧密相联](../Page/自杀意念.md "wikilink")\[94\]。约有12%至24%的病态赌徒有自杀的倾向\[95\]。而他们的配偶的自杀率则比一般人群要高出三倍\[96\]。其他导致嗜赌者自杀风险加大的因素还包括心理疾病、酗酒和药物滥用\[97\]。

### 医疗状况

自杀与生理健康问题之间还存在着联系，包括：\[98\][慢性疼痛](../Page/慢性疼痛.md "wikilink")\[99\]、[创伤性脑损伤](../Page/创伤性脑损伤.md "wikilink")\[100\]、癌症\[101\]、需要采用[血液透析的病患](../Page/血液透析.md "wikilink")、[艾滋病毒](../Page/艾滋病毒.md "wikilink")、[系统性红斑狼疮等等](../Page/系统性红斑狼疮.md "wikilink")\[102\]癌症确诊可能会导致接下来病患的自杀风险翻番\[103\]。且在治疗抑郁症和酗酒的过程中，自杀风险也会普遍增加。而拥有多种医疗状况的人群则更是自杀高危人群。在日本，健康问题被列为是自杀的主要理由\[104\]。

睡眠障碍，例如：[失眠](../Page/失眠.md "wikilink")\[105\]和[睡眠呼吸暂停](../Page/睡眠呼吸暂停.md "wikilink")，也是导致抑郁症和自杀的风险因素。在某些情况下，睡眠障碍还有可能是独立于抑郁症之外的一种风险因素\[106\]。一些其他的医疗状况也可能带来与情绪障碍类似的症状，包括：
[甲状腺功能减退](../Page/甲状腺功能减退.md "wikilink")、[阿兹海默病](../Page/阿兹海默病.md "wikilink")、[脑肿瘤](../Page/脑肿瘤.md "wikilink")、[系统性红斑狼疮和一些药物带来的副作用](../Page/系统性红斑狼疮.md "wikilink")（例如[β受体阻滞剂和](../Page/β受体阻滞剂.md "wikilink")[类固醇](../Page/类固醇.md "wikilink")）\[107\]。

### 心理状态

一系列心理状态均可增加自杀的风险，它们包括：
[绝望](../Page/绝望.md "wikilink")、生活乐趣的丧失、[抑郁和](../Page/抑郁.md "wikilink")[焦虑](../Page/焦虑症.md "wikilink")\[108\]。解决问题能力较差、丧失以往拥有的能力、以及难以控制冲动的情绪等均在自杀风险中扮演着一定的角色\[109\]\[110\]。在老年人群中，觉得自己是他人的负担这种想法也在自杀中扮演着重要角色\[111\]\[112\]。

近期生活上的压力，例如：失去一名家庭成员或好友、失业、社交孤立（例如：独自生活）等均能导致风险的增加\[113\]。而那些从未踏入婚姻殿堂的人群自杀的风险也比较高\[114\]。宗教信仰可能会降低自杀的风险。\[115\]。这一方面是因为许多宗教对自杀均持负面立场，另一方面是因为宗教信仰可能会使得人们加强联系\[116\]。[穆斯林与其他宗教人群相比](../Page/穆斯林.md "wikilink")，其自杀率则更低\[117\]。

有人可能会通过自杀的方式来逃避[欺凌或](../Page/欺凌.md "wikilink")[歧视](../Page/歧视.md "wikilink")\[118\]。童年时期经历的[性虐待](../Page/性虐待.md "wikilink")\[119\]和在[寄养家庭中生活的经验均是自杀的风险因素](../Page/寄养.md "wikilink")\[120\]。而性虐待则被认为占到了整体风险中约20%的比例\[121\]。

[演化心理学](../Page/演化心理学.md "wikilink")（evolutionary
psychology）对自杀的解释是，它可能会改善[整体适应度](../Page/整体适应度.md "wikilink")。如果某人无法再次生育，且在存活期间不断吞噬家人所拥有的资源，则他/她选择自杀后，就有可能会改善整体适应度。针对这一观点的一个反对意见是，健康的青年人士若选择以自杀的方式来结束自己的生命，则不一定会改善整体适应度。[适应一个完全不同的原始环境可能与适应当前环境截然不同](../Page/适应.md "wikilink")\[122\]\[123\]。

贫穷也和自杀风险有所联系\[124\]。而贫富差距的拉大更是能够加大某人自杀的风险\[125\]。[印度自](../Page/印度.md "wikilink")1997年以来有超过20万农民选择了[自杀](../Page/印度农民自杀问题.md "wikilink")，其中一部分原因就是[债务缠身](../Page/债务.md "wikilink")\[126\]。而中国农村地区的自杀风险则比城镇地区的自杀风险高出三倍，其部分原因是该国这一地区的经济仍旧比较困难\[127\]。

### 媒体

媒体（包括互联网）也起着重要的作用\[128\]。媒体对于最具影响力的自杀事件的描述通常采取大量、突出和重复的报道，对其进行美化，使其充满了浪漫主义的色彩，而这种做法可能会产生负面的影响\[129\]。一旦对某种具体的自杀方式进行了详细的描述，则整体自杀人群中采用这一方式的比例可能会增加\[130\]。

这种自杀传染的引发，又称[自杀模仿](../Page/自杀模仿.md "wikilink")，也就是大家熟知的[维特效应](../Page/维特效应.md "wikilink")，该效应是根据[歌德](../Page/歌德.md "wikilink")（Johann
Wolfgang von
Goethe）所著的《[少年维特的烦恼](../Page/少年维特的烦恼.md "wikilink")》一书中最终选择用自杀来结束自己生命的主人翁来命名的\[131\]。因此对于可能将死亡浪漫化的青少年来说，他们的自杀风险更高\[132\]。另外，除了新闻媒体对自杀现象有着重大影响以外，娱乐媒体的影响却有时表现得含混不清\[133\]。与维特效应相对的是拟定的帕帕基诺效应，即指如果媒体避免报道复制自杀的细节，则可能会起到保护性的作用。这一术语则源于[莫扎特](../Page/莫扎特.md "wikilink")（Wolfgang
Amadeus
Mozart）歌剧《[魔笛](../Page/魔笛.md "wikilink")》劇中角色因害怕失去所爱之人企图自杀，後來得救\[134\]。因此，一旦媒体遵守恰当的报道规则，自杀的风险是可以得到降低的\[135\]。但是，从长远来看，让该行业采取这种做法可能比较困难\[136\]

[世界衛生組織在](../Page/世界衛生組織.md "wikilink")2000年時曾提出《PREVENTING SUICIDE：A
RESOURCE FOR MEDIA
PROFESSIONALS》\[137\]，其中提到有關媒體報導自殺新聞時，六個建議及六個避免的事項（「六要」和「六不」）\[138\]。

### 理性型

[理性自杀是指某人在理由充分的情况下结束自己的生命的做法](../Page/理性自杀.md "wikilink")\[139\]。尽管有些人认为自杀行为无论如何也不合逻辑\[140\]。以自杀来使他人获益的行为被称为[利他性自杀](../Page/利他性自杀.md "wikilink")\[141\]。这方面的例子包括：一名老者结束自己的生命，使得社区中的青年人能够获得更多的食物\[142\]。在某些[爱斯基摩文化中](../Page/爱斯基摩.md "wikilink")，这种做法是一种受人尊敬、极富勇气、充满智慧的行为\[143\]。[楢山節考也建基於此等原則](../Page/楢山節考.md "wikilink")。

[自杀性袭击是一种袭击者通过牺牲自己的生命对他人实施暴力袭击的一种政治行为](../Page/自杀攻击.md "wikilink")\[144\]。某些人体炸弹则通过这种方式获得[烈士的身份](../Page/烈士.md "wikilink")\[145\]。[神风特攻队就是在追求崇高事业或道德义务的基础上](../Page/神风特攻队.md "wikilink")，完成一次次使命的\[146\]。[谋杀后自杀是指某人在](../Page/谋杀后自杀.md "wikilink")[谋杀他人后一周内自杀的行为](../Page/谋杀.md "wikilink")\[147\]。[集体自杀通常是指自杀成员在臣服于某一领导人时](../Page/集体自杀.md "wikilink")，出于[同侪压力](../Page/同侪压力.md "wikilink")（peer
pressure）而走向自杀之路\[148\]。集体自杀甚至包括只有两人的情况，这种现象通常被视为[自杀协定](../Page/自杀协定.md "wikilink")\[149\]。

在无法忍受生活的煎熬时，为了减轻痛苦，有些人会选择自杀的方式来获得解脱\[150\]。许多曾经在[纳粹](../Page/纳粹.md "wikilink")（Nazi
Germany）[集中营里饱受煎熬的囚犯则通过故意触摸带电栅栏来结束自己的生命](../Page/集中营.md "wikilink")\[151\]。

## 自杀方式

[SuicideCFR.png](https://zh.wikipedia.org/wiki/File:SuicideCFR.png "fig:SuicideCFR.png")
各国内部的主要自杀方式也不尽相同。不同地区的主要自杀方式包括：[上吊](../Page/上吊.md "wikilink")（suicide by
hanging）、[服毒和](../Page/服毒.md "wikilink")[使用枪支自杀](../Page/吞槍.md "wikilink")。\[152\]自杀方式出现如此差异，人们认为一部分原因是由于各种方式在不同地区的可用程度不同。\[153\]根据一份针对56个国家的调查来看，上吊仍是大部分国家自杀人口使用的最常见方式，\[154\]其在男性自杀者中占到了53%的比例，而在女性自杀者中则占到了39%的比例。\[155\]

全球有30%的自杀案例采取的是服毒的方式。然而，欧洲仅有4%的自杀人口采取这种方式，而在太平洋地区，采取服毒自杀的比例则超过了总自杀人口的50%。\[156\]服毒自杀在[拉丁美洲也比较常见](../Page/拉丁美洲.md "wikilink")，这是因为农民们很容易就可获得农药来自杀。\[157\]在许多国家中，约有60%的女性自杀者和30%的男性自杀者采取过度服用药物来自杀。\[158\]但许多这样的自杀案例却纯属偶然，通常是在自杀者情绪极不稳定时发生的。\[159\]不同自杀方式的死亡率也有所不同：
持枪自杀死亡率为80-90%、投河自尽死亡率为65-80%、上吊死亡率为60-85%、汽车尾气中毒死亡率为40-60%、跳楼死亡率为35-60%、[烧炭自杀死亡率为](../Page/烧炭自杀.md "wikilink")40-50%、服毒自杀死亡率为6-75%、过度用药死亡率为1.5-4%。\[160\]最常见的自杀未遂方式与最常见的成功自杀方式又有所不同，在发达国家，自杀未遂案例中有85%的自杀人口均是采用的过度用药的方式。\[161\]

在美国，自杀人口中约有57%选择使用枪支，且采用这种方式自杀的男性比女性要多。\[162\]接下来，男性中比较常见的自杀方式是上吊，而女性中比较常见的方式为服毒。\[163\]这几种自杀方式的总和占到了美国自杀人口的约40%的比例。\[164\]在瑞士，尽管几乎人人都拥有一支手枪，但该国自杀人口中采用最常见的方式却是上吊。\[165\][跳楼自杀在](../Page/跳楼.md "wikilink")[香港和](../Page/香港.md "wikilink")[新加坡均比较常见](../Page/新加坡.md "wikilink")，分别占到了自杀人口的50%和80%。\[166\]而在中国，服用农药自杀则是最为常见的方式。\[167\]在日本，自行切腹，又称[切腹自杀或剖腹自杀的方式依然存在](../Page/切腹.md "wikilink")，\[168\]但上吊仍旧是最为常见的自杀方式。\[169\]

## 病态生理学

对于任何一种自杀或抑郁症来说，尚未找到一种统一的基本[病态生理学来进行解释](../Page/病态生理学.md "wikilink")。\[170\]但普遍认为这是由于行为因素、社会环境因素和精神因素相互影响所导致的。\[171\]

低水平的[脑源性神经营养因子](../Page/脑源性神经营养因子.md "wikilink")（BDNF）与自杀有直接相关性，\[172\]并与重度抑郁症、创伤后应激障碍、精神分裂症和[强迫症有着间接相关性](../Page/强迫症.md "wikilink")。\[173\][尸体剖检](../Page/验尸.md "wikilink")（Autopsy）研究发现[海马体和](../Page/海马体.md "wikilink")[前额叶皮质中的BDNF有所下降](../Page/前额叶皮质.md "wikilink")，不论该死者是否患有精神疾病。\[174\][血清素是一种大脑中的](../Page/血清素.md "wikilink")[神经递质](../Page/神经递质.md "wikilink")，它被认为在自杀人群中水平较低。一方面，人们发现在死亡后，人体中的[5-HT2A受体水平有所提高](../Page/5-HT2A受体.md "wikilink")。\[175\]其他一些证据包括[脑脊液中的血清素和](../Page/脑脊液.md "wikilink")[5-羟吲哚乙酸的水平出现下降](../Page/5-羟吲哚乙酸.md "wikilink")。\[176\]但与此相关的直接证据却是很难搜集的。\[177\][表观遗传学是一种研究不改变](../Page/表观遗传学.md "wikilink")[DNA基因序列的环境因素所致的基因表达水平变化的科学](../Page/DNA基因序列.md "wikilink")，而它也被认为在决定自杀风险时起到了一定作用。\[178\]

## 预防

[suicidemessageggb01252006.JPG](https://zh.wikipedia.org/wiki/File:suicidemessageggb01252006.JPG "fig:suicidemessageggb01252006.JPG")上设立一特殊电话，可直接与[心理危机干预热线进行通话](../Page/心理危机干预热线.md "wikilink")。\]\]
[NTPC_Public_Health_Department_suicide_prevention_board_on_Taipei_Bridge_20100204.jpg](https://zh.wikipedia.org/wiki/File:NTPC_Public_Health_Department_suicide_prevention_board_on_Taipei_Bridge_20100204.jpg "fig:NTPC_Public_Health_Department_suicide_prevention_board_on_Taipei_Bridge_20100204.jpg")
自杀预防是指人们通过使用一些预防措施，集体防范以减少自杀事件的行为。其中包括减少某些自杀方式的可获得性，比如减少枪支或毒素的可用性，以降低自杀风险。\[179\]\[180\]其他措施包括减少焦碳的可获得性，防止人们靠近大桥栏杆和地铁站台栏杆，在搜索引擎搜索“自杀”时给予搜索者及时就医的劝告和提供自杀干预热线等。\[181\]针对吸毒、酗酒、抑郁症和自杀未遂人群的治疗也可能有一定效果。\[182\]有人还提议将减少酒水的获取做为一种预防措施（例如：减少酒吧的数量）。\[183\]
尽管[心理危机干预热线比较常见](../Page/心理危机干预热线.md "wikilink")，但却很少有证据证明或反驳它们的有效性。\[184\]\[185\]对于近期曾经考虑过自杀的年轻人来说，[认知行为疗法似乎可以起到有效的改善作用](../Page/认知行为疗法.md "wikilink")。\[186\][经济发展则可通过减贫而降低自杀率](../Page/经济发展.md "wikilink")。\[187\]加强人与人之间，尤其是老年人之间的社会联系也可起到一定效果。\[188\]

### 筛查

在针对一般人口进行的筛查过程中，很少有数据显示该筛查对降低最终的自杀率有何效果。\[189\]

[锂元素的使用](../Page/锂.md "wikilink")<u>似乎</u>能够将躁郁症患者和单向抑郁患者的自杀风险降低至接近一般人群的水平。\[190\]\[191\]

### 醫療體系

根據研究顯示，自殺死亡者在自殺身亡前7天就醫的比例皆在60%左右，自殺身亡前90天更可達80%，此一數據凸顯醫療體系在自殺防治的重要性。而醫療與社會工作者在面臨自殺風險個案時，需要評估及辨識危險程度，並能及時因應與處置。兩者皆為自殺防治第一線專業人員，因此提升其自殺防治技巧和能力，刻不容緩。\[192\]

## 流行病学

[2004年间每10万 居民中由自残导致的死亡人数。\[193\]
](https://zh.wikipedia.org/wiki/File:Self-inflicted_injuries_world_map_-_Death_-_WHO2004.svg "fig:2004年间每10万 居民中由自残导致的死亡人数。                ")

大约有0.5%-1.4%的人群会以自杀的形式来结束自己的生命。\[194\]\[195\]从全球来看，截止2008、2009年，自杀已经成为第十大主要死亡原因，\[196\]每年约造成80万至100万人死亡，每10万人的年[死亡率达到](../Page/死亡率.md "wikilink")11.6人。\[197\]自上世纪60年代以来到2012年，自杀率已经上升了60%，\[198\]且新增部分主要位于[发展中国家](../Page/发展中国家.md "wikilink")（developing
country）。\[199\]每10到40件自杀未遂事件中，就有一件最终致死。\[200\]

自杀率在不同国家和不同时间段的表现也大相径庭。\[201\]2008年的各地区死亡率分别为：非洲0.5%、东南亚1.9%、美洲1.2%、欧洲1.4%。\[202\]不同国家的每10万人死亡率为：澳大利亚8.6、加拿大11.1、中国12.7、印度23.2、英国7.6、美国11.4。\[203\]2009年美国自杀案件约达3.6万起，位居美国主要[死亡原因](../Page/死亡原因.md "wikilink")（death）第十位。\[204\]每年约有65万人因自杀未遂而被送入急诊室。\[205\][立陶宛](../Page/立陶宛.md "wikilink")、日本和匈牙利则拥有最高的自杀未遂率。\[206\]而中国和印度则拥有最高水平的绝对自杀数据，占到了全球的一半以上。\[207\]在中国，自杀已经成为第五大主要死亡原因。\[208\]

### 性别

在西方国家，男性的自杀死亡率比女性要高3至4倍，尽管女性的自杀未遂率要比男性高出4倍。\[209\]\[210\]这是因为男性结束自己生命的方式往往更易致命。\[211\]而这一差别在65岁以上的人群中的表现则更为明显，这一人群中男性的自杀率是女性的10倍。\[212\][中国拥有全球最高的女性自杀率的国家之一](../Page/中国自杀率.md "wikilink")，也是唯一一个女性自杀率高于男性自杀率的国家（比率为0.9）。\[213\]\[214\]在[地中海东部地区](../Page/地中海东部地区.md "wikilink")，男性和女性的自杀率基本持平。\[215\][韩国则拥有最高的女性自杀率](../Page/韩国.md "wikilink")，约为每10万人中就有22人自杀。女性自杀率在东南亚国家和太平洋西岸地区也普遍较高。\[216\]
一般來說，同性戀者自殺率比異性戀高。[跨性別者自殺率比順性別者高很多](../Page/跨性別.md "wikilink")。

### 年龄

在许多国家中，自杀率最高的人群通常为中年人\[217\]或老年人。\[218\]但自杀绝对死亡数据则是在15岁至29岁的人群中最高，这是因为这一年龄层的人口较多。\[219\]在美国，[白种人](../Page/白种人.md "wikilink")（caucasian
race）中80岁以上的人群自杀率最高，尽管年轻人更容易尝试自杀。\[220\]自杀也成为[青少年](../Page/青少年.md "wikilink")（adolescence）死亡的第二大常见原因。\[221\]且在年轻男性中，自杀是仅次于意外死亡的第二大死亡原因。\[222\]在发达国家，自杀占到了年轻男性死亡率的近30%。\[223\]在发展中国家，这一比率也比较类似，但是该比率占总死亡人数的比重较小，这是由于其他类型的[创伤](../Page/创伤.md "wikilink")（trauma
(medicine)）造成的死亡率较高所引起的。\[224\]与其他地区相比，东南亚国家的年轻女性自杀率则比老年女性的自杀率要高。\[225\]

## 历史

[106_Conrad_Cichorius,_Die_Reliefs_der_Traianssäule,_Tafel_CVI.jpg](https://zh.wikipedia.org/wiki/File:106_Conrad_Cichorius,_Die_Reliefs_der_Traianssäule,_Tafel_CVI.jpg "fig:106_Conrad_Cichorius,_Die_Reliefs_der_Traianssäule,_Tafel_CVI.jpg")自杀身亡，来源于[图拉真柱](../Page/图拉真柱.md "wikilink")\]\]

在[古雅典时期](../Page/古雅典.md "wikilink")（Classical
Athens），未经国家允许就擅自自杀而死的人无法获得举行正常葬礼的权利。他将被单独埋在城市边缘处，没有墓碑或任何标记。\[226\]在[古希腊和](../Page/古希腊.md "wikilink")[古罗马](../Page/古罗马.md "wikilink")（Ancient
Rome）时期，自杀被认为是军队战败后的一种可接受的死亡方式。\[227\]在古罗马，尽管自杀行为最初获得了许可，但日后却由于其经济成本被视作一种违背国家的罪行。\[228\]法国[路易十四于](../Page/路易十四.md "wikilink")1670年颁布的一项刑法条例对自杀人群的惩罚则更为残酷：死者尸体将被拖入市区街道，面朝土地被倒挂起来，然后被扔入垃圾堆。另外，此人的所有财产也将被没收。\[229\]\[230\]在历史的长河中，自杀未遂的[天主教会人士将被處以](../Page/天主教会.md "wikilink")[破門律](../Page/破門律.md "wikilink")（[逐出教会](../Page/逐出教会.md "wikilink")，excommunication），且自杀而死的人群则被埋在神圣墓地之外。\[231\]在19世纪末期的英国，自杀未遂被等同于[谋杀未遂](../Page/谋杀未遂.md "wikilink")，当事人可以被判绞刑致死。\[232\]而在19世纪的欧洲，导致自杀行为的原因从最初的[罪转变为由](../Page/罪.md "wikilink")[精神错乱所导致](../Page/精神错乱.md "wikilink")。\[233\]

## 社会和文化

### 立法

[Wakisashi-sepukku-p1000699.jpg](https://zh.wikipedia.org/wiki/File:Wakisashi-sepukku-p1000699.jpg "fig:Wakisashi-sepukku-p1000699.jpg")而准备的[军刀](../Page/军刀.md "wikilink")。\]\]

在大多数西方国家，自杀已经不再是一项犯罪\[234\]；尽管它在19世纪时，大多数西欧国家都把它视作犯罪。\[235\]许多伊斯兰国家仍旧将自杀视为犯罪。\[236\]

在澳大利亚，自杀并不是犯罪。\[237\]但鼓动、[煽动](../Page/煽动.md "wikilink")（incitement）、协助或唆使他人尝试自杀则属于犯罪行为，法律明确规定任何人有权使用“任何必要合理的行为”来阻止他人自杀。\[238\]澳大利亚北领地在1996年至1997年的短暂时期内，曾允许医生通过合法形式辅助自杀。\[239\]

目前，没有任何欧洲国家认为自杀或自杀未遂是一种犯罪。\[240\]英格兰和威尔士也在[1961年自杀法案中规定自杀并不犯罪](../Page/1961年自杀法案.md "wikilink")，爱尔兰共和国则在1993年认定自杀无罪。\[241\]“犯（自杀）罪”（commit）一词曾用于自杀行为属于违法行为的情况下，但现在许多组织已经因该词所带来的负面涵义而停用该词。\[242\]\[243\]

在印度，自杀仍为违法行为，且自杀未遂者的家庭可能会陷入法律上的困境。\[244\]在德国，主动要求安乐死被视为违法，且自杀事件中任何目击证人可能被指控为在紧急情况下无作为的罪状。\[245\][瑞士近期开始采取步骤来合法化针对慢性精神病患者实施](../Page/瑞士.md "wikilink")[辅助自杀的行为](../Page/辅助自杀.md "wikilink")。[洛桑市最高法院在](../Page/洛桑市.md "wikilink")2006年的一项判决中赋予一名长期患有精神疾病的无名氏患者采用自杀结束自己生命的权利。\[246\]

在美国，自杀并非属于违法行为，但未遂自杀者却有可能会面临罚款的处罚。\[247\]医生辅助自杀行为在俄勒冈州和\[248\]华盛顿州是合法的。\[249\]

### 宗教观念

[A_Hindoo_Widow_Burning_Herself_with_the_Corpse_of_her_Husband.jpg](https://zh.wikipedia.org/wiki/File:A_Hindoo_Widow_Burning_Herself_with_the_Corpse_of_her_Husband.jpg "fig:A_Hindoo_Widow_Burning_Herself_with_the_Corpse_of_her_Husband.jpg")孀妇正在丈夫尸体旁边自焚。\]\]
在[基督宗教大多数](../Page/基督宗教.md "wikilink")[教派中](../Page/教派.md "wikilink")，自杀被视作一种[罪](../Page/罪.md "wikilink")，其主要基础源于[中世纪时期许多颇具影响力的基督教思想家的著作](../Page/中世纪.md "wikilink")，例如[圣奥古斯丁和](../Page/圣奥古斯丁.md "wikilink")[圣托马斯·阿奎那](../Page/托马斯·阿奎那.md "wikilink")；但在[拜占庭基督教](../Page/拜占庭.md "wikilink")[查士丁尼法典中自杀却不被视作罪](../Page/查士丁尼法典.md "wikilink")。\[250\]\[251\]在天主教的教义中，对此的争论则主要基于[十诫](../Page/十诫.md "wikilink")（Ten
Commandments）中的“汝不能杀戮”一条（源于[新约中](../Page/新约.md "wikilink")[马太福音19:18中耶稣的指示](../Page/馬太福音.md "wikilink")），以及生命为主的赐福，不得摒弃的观点，他们认为自杀是违背“自然秩序”的行为，与主对全球的总体规划不相符合。\[252\]

但人们相信，精神上的疾病或者对苦难的极度恐惧使得自杀人群的责任心减少。\[253\]而相反的说法则包括：十诫中的第六条（Ten
Commandments）更为准确的翻译应当是“汝不可谋杀”，而不一定适用于当事人本身；主赋予了人类自由的意志；自己剥夺自身的生命并不一定比因治愈某一疾病而违反主的意志的程度要高；而在圣经的记载中，一些主的追随者在自杀后并未受到悲惨的谴责。\[254\]

犹太教则主张关注此生价值的重要性，因此，自杀等同于否定主在全世界的善举。此外，在极端的情况下，当犹太人面对要么被杀、要么叛教，而没有其他选择时，他们就会采取自杀的形式或[集体自杀](../Page/集体自杀.md "wikilink")（请参见[马萨达和](../Page/马萨达.md "wikilink")[约克城等例子](../Page/约克城.md "wikilink")），在犹太教祷告文中甚至有一篇祷词以冷酷的方式提醒那些“当刀架在脖子上”的人和以死亡的方式“将生命献给主”的人（请参见[殉教](../Page/殉教.md "wikilink")）。不同的犹太教机构对这种行为的看法褒贬不一，有些人认为这种行为是英勇殉教的典范，有些则认为他们不应以自杀的形式来期望获得殉教的名声。\[255\]

伊斯兰教與其他的[亞伯拉罕諸教相同](../Page/亞伯拉罕諸教.md "wikilink")，不允许自杀。\[256\]

在[印度教中不允許自殺](../Page/印度教.md "wikilink")，在现代印度教社会中，自杀所犯的罪恶等同于谋杀他人所犯的罪恶。[印度教经文](../Page/印度教经文.md "wikilink")（Hindu
texts）指出，自杀者将成为孤魂，在地球上游荡直至他如果没有自杀而最终死亡的那一刻为止。\[257\]但印度教却接受人们拥有通过非暴力的形式加速死亡而[结束自己生命的权利](../Page/结束自己生命的权利.md "wikilink")（right
to
die），这一行为在印度教中称为[非暴力实践空腹死刑](../Page/非暴力实践空腹死刑.md "wikilink")。\[258\]但这种非暴力实践空腹死刑的人群范围严格限制在那些没有任何生的欲望或抱负、且在此生也再无其他责任的人。\[259\][耆那教有一种类似的修行](../Page/耆那教.md "wikilink")，被称之为[三塔拉](../Page/三塔拉.md "wikilink")。在中世纪时期，[殉夫自焚](../Page/娑提_\(习俗\).md "wikilink")（Sati），又称寡妇自焚在印度教中非常普遍。

[佛教與](../Page/佛教.md "wikilink")[印度教接近](../Page/印度教.md "wikilink")，除非是[殉教](../Page/殉教.md "wikilink")、為[公共利益或者為了拯救他人而自殺](../Page/公共利益.md "wikilink")，其餘自殺視為殺害一人。[釋迦佛制戒中](../Page/釋迦佛.md "wikilink")，凡[比丘自殺以](../Page/比丘.md "wikilink")「波羅夷」（Parajika）罪論處，相當於殺人罪，而自殺未遂則相當於「偷蘭遮」（Sthulatyaya）罪。不過在佛教故事中，許多已經[覺悟的](../Page/覺悟.md "wikilink")[阿羅漢自行選擇](../Page/阿羅漢.md "wikilink")「[入滅](../Page/入滅.md "wikilink")」，這是一種欣求[解脫而厭惡](../Page/解脫.md "wikilink")[五濁惡世的作為](../Page/五濁惡世.md "wikilink")，如俗人為之，則為自殺罪，將墮落[三惡道](../Page/三惡道.md "wikilink")。

[道教接受](../Page/道教.md "wikilink")[佛教思想](../Page/佛教.md "wikilink")，除非因為忠君孝親、救國救民等正當理由犧牲自己，其餘視為殺人，自殺者的靈魂每天都必須重複自己自殺的痛苦過程，此外，還將有兩種下場，一是監禁於[枉死城](../Page/枉死城.md "wikilink")，待註定的年壽已到，再由[十殿閻君依照殺人罪](../Page/十殿閻君.md "wikilink")，打入[地獄受刑](../Page/地獄.md "wikilink")。另一種下場則更加悽慘，靈魂被自殺處的靈氣所困，必須造成另外一個人身亡，是為[替身](../Page/替身.md "wikilink")，方能使自己脫身，投入下一次的[輪迴](../Page/輪迴.md "wikilink")。

[儒教或者](../Page/儒教.md "wikilink")[儒家也反對自殺](../Page/儒家.md "wikilink")，如同前述，除非因為忠君孝親、救國救民等正當理由，其餘視為殺人，《[孝經](../Page/孝經.md "wikilink")》：「身體髮膚，受之父母，不敢毀傷，孝之始也。」所以自殺犯了[不孝的大罪](../Page/不孝.md "wikilink")。

### 哲学

[The_way_out.jpg](https://zh.wikipedia.org/wiki/File:The_way_out.jpg "fig:The_way_out.jpg")（Suicidal
ideation）： [George Grie著](../Page/George_Grie.md "wikilink")2007年版。\]\]

自杀哲学提出了一系列问题，包括什么构成了自杀、自杀是否可能是理智的选择、以及自杀的道德容许度如何\[260\]。自杀是否在道义上可以接受，哲学家们对这一问题的看法褒贬不一：有的强烈反对（认为自杀是违背伦理和道德的），有的则认为自杀是任何人[神圣而不可侵犯的权利](../Page/神圣而不可侵犯.md "wikilink")（即使对健康的年轻人也是如此），认为他们是在理智而不违背良心的情况下做出结束自己生命的决定的。
反对自杀的哲学家包括一些基督教的哲学家，例如[希波的奥古斯丁和](../Page/希波的奥古斯丁.md "wikilink")[托马斯·阿奎那](../Page/托马斯·阿奎那.md "wikilink")\[261\]、[伊曼努尔·康德](../Page/伊曼努尔·康德.md "wikilink")\[262\]和颇具争议的[约翰·斯图尔特·密尔](../Page/约翰·斯图尔特·密尔.md "wikilink")——密尔重视[自由和](../Page/自由.md "wikilink")[自主的重要性](../Page/自主.md "wikilink")，因此他反对可能阻碍某人无法自主做出未来决策的选择\[263\]。其他哲学家则认为自杀是个人的合法选择。这一观点的支持者则认为任何人都不应在违背自己意愿的情况下被迫受苦受难，特别是在患有无法治愈的疾病、精神疾病或年纪老迈而没有改善余地时。他们拒绝承认自杀行为永远是不理智的说法，认为自杀对于长期受到巨大疼痛或创伤的困扰的人群来说，是一个有效的最终解脱方式\[264\]。更为激进的一种说法则认为，应当允许人们自由选择是生是死，不论他们是否受到折磨。这一[思想流派的著名支持者包括](../Page/思想流派.md "wikilink")：苏格兰经验主义者[大卫·休谟](../Page/大卫·休谟.md "wikilink")\[265\]和美国生物伦理学家[雅各布·阿佩尔](../Page/雅各布·阿佩尔.md "wikilink")\[266\]\[267\]。

### 提倡

[Alexandre-Gabriel_Decamps_-_The_Suicide_-_Walters_3742.jpg](https://zh.wikipedia.org/wiki/File:Alexandre-Gabriel_Decamps_-_The_Suicide_-_Walters_3742.jpg "fig:Alexandre-Gabriel_Decamps_-_The_Suicide_-_Walters_3742.jpg")
|url=<http://art.thewalters.org/detail/1589> |title= The
Suicide}}</ref>\]\]

在许多文化和[次文化中都不反對](../Page/次文化.md "wikilink")，甚至提倡自杀。[日本早期](../Page/日本.md "wikilink")，就有許多敗戰[武士](../Page/武士.md "wikilink")，甚至沒有敗戰，只是因為宣揚個人的理念，就選擇[切腹自殺](../Page/切腹.md "wikilink")。[第二次世界大战期间](../Page/第二次世界大战.md "wikilink")，[大日本帝國](../Page/大日本帝國.md "wikilink")[政府就极力鼓励并吹捧](../Page/政府.md "wikilink")[神风敢死队式的袭击](../Page/神风敢死队.md "wikilink")，認為是一種勇敢、光榮，这种袭击是[太平洋戰爭接近尾声时](../Page/太平洋戰爭.md "wikilink")，[日軍](../Page/日軍.md "wikilink")[飞行员为抵抗](../Page/飞行员.md "wikilink")[盟军](../Page/盟军.md "wikilink")，所采用的一种[自杀式攻击](../Page/自杀式攻击.md "wikilink")。時至今日[日本自殺率仍高](../Page/日本.md "wikilink")，但社会总体来说被视为对自杀行为表示“宽容”\[268\]（请参见[日本的自杀现象](../Page/日本的自杀现象.md "wikilink")）。

[通过互联网搜索有关自杀的信息](../Page/通过互联网搜索有关自杀的信息.md "wikilink")（Suicide and the
Internet）所得到的页面中，约10-30%均鼓励或促进自杀的企图。 有人担心，这类网站可能将那些有自杀倾向的人群最终推向行动的边缘。
有些人还在网络上与此前的朋友或近期在[聊天室或](../Page/聊天室.md "wikilink")[留言板](../Page/留言板.md "wikilink")（Internet
forum）上认识的人们一起达成[自杀协定](../Page/自杀协定.md "wikilink")。
但是，互联网还能用来为边缘人士们提供社交场所，从而防止出现自杀的情况。\[269\]

### 地点

有些地标因出现较高的自杀率而闻名于世\[270\]。它们包括旧金山的[金门大桥](../Page/金门大桥.md "wikilink")、日本的[青木原](../Page/青木原.md "wikilink")\[271\]、英国的[比奇角](../Page/比奇角.md "wikilink")\[272\]和[多伦多的](../Page/多伦多.md "wikilink")[布卢尔街高架桥](../Page/布卢尔街高架桥.md "wikilink")\[273\]。

自1937年金门大桥建成至2010年，已有1300多人从这里跳下自杀\[274\]。许多出现频繁自杀的地点已经修建了围栏，以防止人们自杀\[275\]。这包括多伦多的[Luminous
Veil大桥](../Page/Luminous_Veil大桥.md "wikilink")\[276\]、巴黎[艾菲尔铁塔和纽约](../Page/艾菲尔铁塔.md "wikilink")[帝国大厦上的围栏](../Page/帝国大厦.md "wikilink")\[277\]。截至2011年，金门大桥上也修建了围栏\[278\]。总体来看，这些围栏非常奏效\[279\]。

### 注目案例

一個集体自殺的例子是在1978年的[瓊斯鎮](../Page/瓊斯鎮.md "wikilink")，被殺和自殺有909位成員是[人民圣殿教](../Page/人民圣殿教.md "wikilink")，此教由[吉姆·琼斯所領導](../Page/吉姆·琼斯.md "wikilink")。結束生命的方法是喝下摻有[氰化钾和許多處方藥的葡萄口味](../Page/氰化钾.md "wikilink")。\[280\]\[281\]\[282\]
幾千位日本居民在1944年[塞班島戰役的最後幾天犧牲生命](../Page/塞班島戰役.md "wikilink")，一些人從及跳下。\[283\]

由[波比·山德士所領導的](../Page/波比·山德士.md "wikilink")[1981年爱尔兰绝食抗议](../Page/1981年爱尔兰绝食抗议.md "wikilink")，導致10人死亡。造成死亡的原因，被[驗屍官記為](../Page/驗屍官.md "wikilink")"飢餓，自願性"而非自殺。但這在死亡的士兵家屬抗議後，死亡證明改為簡單的"飢餓"。\[284\]在二次世界大戰期間，[埃尔温·隆美尔被發現預知](../Page/埃尔温·隆美尔.md "wikilink")[7月20日暗殺希特勒](../Page/7月20日密谋案.md "wikilink")；在上，他被威脅除非自盡否則將對他的家人展開報復。\[285\]

## 人類以外的自殺

在人類以外的動物也有相當於自殺的行為或者疑似自殺的行為，例如世界各地曾多次發現[鯨魚和](../Page/鯨魚.md "wikilink")[海豚擱淺海灘](../Page/海豚.md "wikilink")，牠們被人們送回[海中後又掉頭重返海灘](../Page/海.md "wikilink")，最終死亡，原因不明。有說是海中環境有變，令牠們寧願冒死上岸也不願回海中；另一說是其中的成員的身體機能出現問題，引致擱淺。而動物的群體性生活習慣令其他同伴跟隨或「盲從」，引發集體死亡，看似自殺，但應視為意外而非有意識結束生命或意圖自殺較為妥當，例如：[綿羊跳崖](../Page/綿羊.md "wikilink")。

[旅鼠常被傳說有自殺傾向](../Page/旅鼠.md "wikilink")，其實是人類主觀下的誤會。近期研究顯示:旅鼠只有在族群出現數量爆炸，現有環境無法再負擔牠們所需時才會出現跳海行為。和一般人的想像相反，旅鼠是游泳健將，加上體積小，體重輕，牠們能夠借水旅行相當長的距離。牠們之所以跳水乃是為了尋求新的、良好適當的生活環境，即使有相當的數量死於途中，這也絕不是自殺，反而是為了求取生存的非常手段。因此牠們能夠和人類一樣遍佈全世界。

[沙门氏菌就有自杀式的行为](../Page/沙门氏菌.md "wikilink")，它们为了压制竞争细菌，则会对自身启动[免疫系统反应](../Page/免疫系统.md "wikilink")。\[286\]
一种叫做“Forelius
pusillus”的巴西蚂蚁则采用自杀性的防御行为，每天晚上，均由一小批蚂蚁为了巢穴的安全，从外部将巢穴封死，然后离开默默死去。\[287\]

[碗豆蚜当受到](../Page/碗豆蚜.md "wikilink")[瓢虫的威胁时会自身引爆](../Page/瓢虫.md "wikilink")，四处飞溅的爆炸物能够保护它们的同胞，有时甚至能够杀死瓢虫。\[288\]某些种类的[白蚁群中也有能够引爆自身的兵蚁](../Page/白蚁.md "wikilink")，使得天敌身上覆满粘稠物质。\[289\]\[290\]

尽管在动物轶事中也曾报道有关狗、[马和海豚自杀的现象](../Page/马.md "wikilink")，但仍旧缺乏确凿的证据。\[291\]有关动物自杀方面的科学研究实在是寥寥无几。\[292\]

非生物物體也會有一些被稱作「自殺」的設計。例如[人工智慧在被](../Page/人工智慧.md "wikilink")[電腦病毒入侵的情況下](../Page/電腦病毒.md "wikilink")，會啟動殺毒[程式](../Page/程式.md "wikilink")，同時[刪除自己體內的](../Page/刪除.md "wikilink")[檔案](../Page/檔案.md "wikilink")，若情況嚴重者甚至會將自己關機、銷毀[系統所有](../Page/系統.md "wikilink")[資料](../Page/資料.md "wikilink")。這也算是一種自動化的自殺行為。

## 参考文献

### 引用

<div style = "height:800px; overflow:auto; padding:3px; border:1px solid #aaa;">

<references responsive="" />

</div>

### 来源

  - 吳飛：《無言的游魂——“理解自殺”札記之一》，载于《[讀者](../Page/讀者.md "wikilink")》，2005(7)，pp. 3–10.

## 外部連結

  - [台湾自殺防治中心](http://tspc.tw)
  - [國際預防自殺協會](http://www.iasp.info/)
  - [WHO自殺預防項目網站](http://www.who.int/mental_health/prevention/suicide/supresuicideprevent/en/)
  - [北京心理危機研究與干預中心](http://www.crisis.org.cn/)
  - [順談預防及處理自殺個案的技巧](http://www.cheu.gov.hk/b5/info/files/Handout.doc)
  - [預防老人自殺講座](http://www.cheu.gov.hk/b5/info/files/Seminar2.ppt)
  - [Suicide
    Prevention](https://www.cdc.gov/violenceprevention/suicide/)
  - [Preventing Suicide: A Technical Package of Policy, Programs, and
    Practices](https://www.cdc.gov/violenceprevention/pdf/suicide-technicalpackage.pdf)
  - [Suicide
    Prevention](https://www.cdc.gov/violenceprevention/suicide/)
  - [Youth Suicide
    Prevention](https://www.cdc.gov/violenceprevention/suicide/youth_suicide.html)
  - [Know the Warning Signs of
    Suicide](http://www.suicidology.org/resources/warning-signs)
  - [Preventing
    Suicide](https://www.cdc.gov/features/preventingsuicide/)
  - [Suicide Prevention If You Know Someone in
    Crisis](https://www.nimh.nih.gov/health/topics/suicide-prevention/index.shtml)
  - [Warning Signs of
    Suicide](https://www.nimh.nih.gov/health/topics/suicide-prevention/suicide-prevention-studies/warning-signs-of-suicide.shtml)
  - [what-to-look-for/suicidal-behavior](https://www.mentalhealth.gov/what-to-look-for/suicidal-behavior/)
  - [Policy paper Suicide prevention: third annual
    report](https://www.gov.uk/government/publications/suicide-prevention-third-annual-report)
  - [Featured Topic: World Health Organization's (WHO) Report on
    Preventing
    Suicide](https://www.cdc.gov/violenceprevention/suicide/WHO-report.html)
  - [自殺前的徵兆 /
    自杀前的征兆](http://www.befrienders.org/http-www-befrienders-org-int-chinese-warningsigns)

## 參見

  - [自殺學家](../Page/自殺學家.md "wikilink")
  - [神聖的人](../Page/神聖的人.md "wikilink")
  - [死亡](../Page/死亡.md "wikilink")
  - [他殺](../Page/他殺.md "wikilink")
  - [被自杀](../Page/被自杀.md "wikilink")

<!-- end list -->

  - 相關列表

<!-- end list -->

  - [中华人民共和国自杀政治人物列表](../Page/中华人民共和国自杀政治人物列表.md "wikilink")
  - [日本自殺人物列表](../Page/日本自殺人物列表.md "wikilink")
  - [各国自杀率列表](../Page/各国自杀率列表.md "wikilink")
  - [中国自杀率](../Page/中国自杀率.md "wikilink")

<!-- end list -->

  - 相關作品

<!-- end list -->

  - [爱米尔·涂尔干](../Page/爱米尔·涂尔干.md "wikilink")：[自殺論](../Page/自殺論.md "wikilink")
  - [解剖自殺心靈](../Page/解剖自殺心靈.md "wikilink")（*Autopsy of a Suicidal Mind*
    by Edwin S. Shneidman）
  - [完全自殺手冊](../Page/完全自殺手冊.md "wikilink")

<!-- end list -->

  - 相關分類

<!-- end list -->

  - [自殺者](../Category/自殺者.md "wikilink")

{{-}}

[Category:行為](../Category/行為.md "wikilink")
[Category:死亡](../Category/死亡.md "wikilink")
[自殺](../Category/自殺.md "wikilink")
[Category:社會問題](../Category/社會問題.md "wikilink")

1.

2.

3.

4.
5.
6.

7.
8.
9.

10.
11.

12.

13.

14.

15.

16.

17.
18.
19.

20.

21.

22.

23.

24.

25.

26.

27.

28.

29.

30.

31.

32.
33.

34.

35.

36.

37.

38.

39.

40.

41.

42.

43.

44.
45.

46.

47.
48.

49.
50.
51.
52.

53.

54.

55.

56.

57.

58.

59.
60.
61.

62.

63.
64.
65.

66.

67.
68.
69.
70.

71.
72.

73.
74.

75.

76.
77.
78.

79.
80.

81.
82.
83.
84.

85.
86.

87.
88.

89.

90.
91.

92.
93.
94.

95.
96.

97.

98.
99.

100.

101.

102.
103.
104.

105.

106.

107.
108.
109.
110.

111.

112.
113.
114.
115.

116.
117.
118.

119.

120.

121.
122.
123.

124.

125.

126.

127.

128.
129.

130.
131.
132.

133.

134.

135.
136.
137. [WHO PREVENTING SUICIDE：A RESOURCE FOR MEDIA
     PROFESSIONALS](http://www.who.int/mental_health/media/en/426.pdf)

138. [自殺防治網通訊第四卷．第一期](http://tspc.tw/tspc/upload/tbepaper/20090507174412_file1.pdf)
     page 21

139.

140.
141.

142.
143.
144.

145.
146.

147.

148.

149.

150.

151.

152.

153.
154. Ajdacic-Gross, Vladeta, *et al*.. [Bulletin of the World Health
     Organization](../Page/Bulletin_of_the_World_Health_Organization.md "wikilink")
     **86** (9): 726–732. September 2008. Accessed 2 August
     2011.[Archived](http://www.webcitation.org/60dtCtOLM?url=http://www.scielosp.org/pdf/bwho/v86n9/a17v86n9.pdf)
     2 August 2011. See[html
     version](http://www.who.int/bulletin/volumes/86/9/07-043489/en/index.html).
     The data can be seen here
     [1](http://www.who.int/bulletin/volumes/86/9/0042-9686_86_07-043489-table-T1.html)

155.

156.

157.
158.

159.
160.
161.
162.
163.
164.

165.

166.
167.

168.
169.

170.
171.
172.

173.

174.

175.

176.

177.
178.

179.
180.
181.
182.
183.

184.

185.

186.

187.
188.

189.

190.

191.

192.

193.

194.
195.
196.
197.
198.

199.
200.
201.

202.
203.

204.

205.
206.
207.
208.
209.
210.
211.

212.
213.
214.

215.
216.
217.

218.
219.
220.
221.

222.
223.
224.
225.
226.

227.

228.

229.

230.

231.
232.
233.
234.

235.

236.
237.

238.

239.

240.
241.
242. Holt, Gerry.["When suicide was
     illegal"](http://www.bbc.co.uk/news/magazine-14374296). [BBC
     News](../Page/BBC_News.md "wikilink"). 3 August 2011. Accessed 11
     August 2011.

243.

244.

245. "German politician Roger Kusch helped elderly woman to die"[Times
     Online](http://www.timesonline.co.uk/tol/news/world/europe/article4251894.ece)
     July 2, 2008

246.

247.

248.

249.

250.

251.

252.

253.

254.

255.

256.
257. Hindu Website. [Hinduism and
     suicide](http://www.hinduwebsite.com/hinduism/h_suicide.asp)

258.

259.
260.

261.
262. 、 Kant, Immanuel. (1785) *Kant: The Metaphysics of Morals*, M.
     Gregor (trans.), Cambridge: Cambridge University Press, 1996. ISBN
     978-0-521-56673-5. p. 177.

263.

264. Raymond Whiting: A natural right to die: twenty-three centuries of
     debate, pp. 13–17; Praeger (2001) ISBN 978-0-313-31474-2

265.
266.
267. [Wesley J. Smith](../Page/Wesley_J._Smith.md "wikilink"), Death on
     Demand: The assisted-suicide movement sheds its fig leaf, *The
     Weekly Standard*, June 5, 2007.

268.

269.

270.
271.

272.

273.

274.

275.

276.
277.
278.

279.
280. Hall 1987, p.282

281. *Alternative Considerations of Jonestown and Peoples Temple*. San
     Diego State University.

282. "1978:[Mass Suicide Leaves 900
     Dead](http://news.bbc.co.uk/onthisday/hi/dates/stories/november/18/newsid_2540000/2540209.stm)
     ". Retrieved 9 November 2011.

283. [John Toland](../Page/John_Toland_\(author\).md "wikilink"), *[The
     Rising Sun: The Decline and Fall of the Japanese Empire
     1936–1945](../Page/The_Rising_Sun:_The_Decline_and_Fall_of_the_Japanese_Empire_1936–1945.md "wikilink")*,
     Random House, 1970, p. 519

284. [Suicide and Self-Starvation](https://www.jstor.org/pss/3750951),
     Terence M. O'Keeffe,
     [*Philosophy*](../Page/Philosophy_\(journal\).md "wikilink"), Vol.
     59, No. 229 (Jul., 1984), pp. 349–363

285.

286.

287.

288.

289.

290.

291.

292.