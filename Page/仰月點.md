[Chandrabindu.svg](https://zh.wikipedia.org/wiki/File:Chandrabindu.svg "fig:Chandrabindu.svg")

**仰月點**（Chandrabindu、梵語中意為“月亮點”，還被拼寫為 **candrabindu**, **chandravindu**,
**candravindu** 或
**chôndrobindu**）\[1\]是在下半圓上有一點的一種[變音符號](../Page/變音符號.md "wikilink")。它用在[天城文中](../Page/天城文.md "wikilink")
(ँ)，[孟加拉文中](../Page/孟加拉文.md "wikilink") (),
[古吉拉特文中](../Page/古吉拉特文.md "wikilink") (ઁ),
[奥里亚文中](../Page/奥里亚文.md "wikilink") (ଁ) 和
[泰卢固文中](../Page/泰卢固文.md "wikilink") (ఁ)。

它通常意味著前面的元音要[鼻音化](../Page/鼻音化.md "wikilink")。它在
[Unicode](../Page/Unicode.md "wikilink") 中表示為天城體的 U+0901，孟加拉文的 U+0981,
Gujarati文的 U+0A81, Oriya文的 U+0B01, 和 Telugu文的
U+0C01。還有通用的[組合變音符號](../Page/組合變音符號.md "wikilink")
COMBINING CANDRABINDU 代碼點為 U+0310 ( <big> ̐</big>)，但它意圖用於拉丁字母在印度語言的轉寫中。

在古典[梵語中](../Page/梵語.md "wikilink")，它只出現在 lla 連輔音上，用來展示它被發音為鼻音化的雙長 l，它出現於
-nl-
在[連音中被](../Page/連音.md "wikilink")[同化的時候](../Page/同化_\(語言學\).md "wikilink")。

在[吠陀梵語中](../Page/吠陀梵語.md "wikilink")，它用來替代
[anusvara](../Page/anusvara.md "wikilink") 來表示叫做
[anunaasika](../Page/anunaasika.md "wikilink")
的聲音，在下一個詞開始於元音的時候。它通常出現在早期一個詞結束於
-ans 的時候。

## 計算代碼

[Unicode](../Page/Unicode.md "wikilink") 編碼：

  - ◌̐ : U+0310 tchandrabindou [附加符號](../Page/附加符號.md "wikilink") （組合方式）
  - ँ : U+0901 [梵文](../Page/梵文.md "wikilink") tchandrabindou 附加符號符號
  - ঁ : U+0981 [孟加拉語](../Page/孟加拉語.md "wikilink") tchandrabindou 附加符號
  - ઁ : U+0A81 [古吉拉特语](../Page/古吉拉特语.md "wikilink") tchandrabindou 附加符號
  - ଁ : U+0B01 [奥里亚语](../Page/奥里亚语.md "wikilink") tchandrabindou 附加符號
  - ఁ : U+0C01 [泰卢固语tchandrabindou](../Page/泰卢固语.md "wikilink") 附加符號

## 參見

  - [Anunaasika](../Page/Anunaasika.md "wikilink")
  - [Anusvara](../Page/Anusvara.md "wikilink")
  - [唵](../Page/唵.md "wikilink")
  - [結合附加符號](../Page/結合附加符號.md "wikilink")

## 註釋

[Category:婆羅米系文字](../Category/婆羅米系文字.md "wikilink")

1.  *[The Sanskrit Heritage
    Dictionary](../Page/The_Sanskrit_Heritage_Dictionary.md "wikilink")*
    of [Gérard Huet](../Page/Gérard_Huet.md "wikilink")