**C
Album**是[日本二人組合](../Page/日本.md "wikilink")[近畿小子的第](../Page/近畿小子.md "wikilink")3張[專輯](../Page/音樂專輯.md "wikilink")。於1999年8月4日由[傑尼斯娛樂唱片公司發行](../Page/傑尼斯娛樂.md "wikilink")。

## 解說

本單曲是與前作『[B
album](../Page/B_album.md "wikilink")』相隔一年後推出的第3張專輯。雖然首批銷售比上兩張專輯回落，但仍然穩佔[Oricon銷量榜的新上榜第一位](../Page/Oricon.md "wikilink")，延續紀錄。

本專輯繼續前作的做法，把專輯推出前發行的單曲主打歌收錄在內。本專輯收錄了第4張單曲「[擁抱全部／青春時代](../Page/擁抱全部/青之時代.md "wikilink")」、第6張單曲「[不要停止的純真](../Page/不要停止的PURE.md "wikilink")」的專輯混音版本及第7張單曲[Flower](../Page/Flower.md "wikilink")。不過，作限定生產的第5張單曲「[Happy
Happy Greeting／Cinderella
Christmas](../Page/Happy_Happy_Greeting/Cinderella_Christmas.md "wikilink")」卻未有收錄在內。

一如既往，專輯名字繼續以[英文字母順序排列](../Page/英文字母.md "wikilink")。本專輯是第3張專輯，所以使用了英文字母中排行第3的「C」作名稱。不過除此之外，這個「C」字亦同時蘊含了**C調**的意思在內，在CD封面亦有提及。「C調」其實是業界用語中所謂的「好調子」的意思，顧名思義，就是說這張專輯對比上兩張專輯更加重視旋律及節奏上的運用。

另外，本專輯同樣收錄了兩位成員獨唱的各自作曲作詞作品。在本專輯其後推出的專輯有單曲精選專輯『[KinKi Single
Selection](../Page/KinKi_Single_Selection.md "wikilink")』及相隔1年4個月的原創專輯『[D
album](../Page/D_album.md "wikilink")』。

## 收錄歌曲

1.  **It's All Right**
      - 作曲：[飯田建彥](../Page/飯田建彥.md "wikilink")
      - 作詞：[上野浩司](../Page/上野浩司.md "wikilink")
      - 編曲：[長岡成貢](../Page/長岡成貢.md "wikilink")
      - 和唱編排：[松下　誠](../Page/松下誠.md "wikilink")
2.  **Flying People '99**（**'99**）
      - 作曲：飯田建彥
      - 作詞：[戶澤暢美](../Page/戶澤暢美.md "wikilink")
      - 編曲：[CHOKKAKU](../Page/CHOKKAKU.md "wikilink")
3.  **哭泣成堅強**（****）
      - 作曲：[荒木真樹彥](../Page/荒木真樹彥.md "wikilink")
      - 作詞：[三井　拓](../Page/三井拓.md "wikilink")
      - 編曲：[石塚知生](../Page/石塚知生.md "wikilink")
      - 和音編排：[岩田雅之](../Page/岩田雅之.md "wikilink")
4.  **擁抱全部**（****）
      - ※KinKi Kids參與演出的[富士電視台音樂節目](../Page/富士電視台.md "wikilink")『LOVE
        LOVE我愛你』主題曲。第4張單曲。
      - 作曲：[吉田拓郎](../Page/吉田拓郎.md "wikilink")
      - 作詞：[康　珍化](../Page/康珍化.md "wikilink")
      - 編曲：[武部聰志](../Page/武部聰志.md "wikilink")
5.  **那時的天空**（****）
      - 作曲：[知野芳彥](../Page/知野芳彥.md "wikilink")
      - 作詞：知野芳彥
      - 編曲：知野芳彥
6.  **Peaceful World**
      - ※堂本光一自己作曲作詞的獨唱作品。
      - 作曲：[堂本光一](../Page/堂本光一.md "wikilink")
      - 作詞：堂本光一
      - 編曲：石塚知生
      - 和唱編排：岩田雅之
7.  **不要停止的純真**（****，**PURE**）
      - ※堂本剛參與演出的[日本電視台](../Page/日本電視台.md "wikilink")[連續劇](../Page/連續劇.md "wikilink")『為了與你的未來～I'll
        be back～』主題曲。第6張單曲。
      - 作曲：[筒美京平](../Page/筒美京平.md "wikilink")
      - 作詞：[伊達　步](../Page/伊達步.md "wikilink")
      - 編曲：[WACKY KAKI](../Page/WACKY_KAKI.md "wikilink")
8.  **Natural Thang**
      - 作曲：[米倉利德](../Page/米倉利德.md "wikilink")
      - 作詞：米倉利德
      - 編曲：[木村賢一](../Page/木村賢一.md "wikilink")
      - 和音編排：米倉利德
9.  **Flower**（****）
      - ※KinKi
        Kids參與演出的日本[全日空](../Page/全日本空輸.md "wikilink")[航空公司廣告](../Page/航空公司.md "wikilink")『'99
        Paradise [沖繩](../Page/沖繩.md "wikilink")』主題曲。第7張單曲。
      - 作曲：[HΛL](../Page/HΛL.md "wikilink")、[音妃](../Page/音妃.md "wikilink")
      - 作詞：[HΛL](../Page/HΛL.md "wikilink")
      - 編曲：[船山基紀](../Page/船山基紀.md "wikilink")
      - 和音編排：[松下　誠](../Page/松下誠.md "wikilink")
10. **BRAND NEW DAY**
      - 作曲：[川上明彥](../Page/川上明彥.md "wikilink")
      - 作詞：[山本英美](../Page/山本英美.md "wikilink")
      - 編曲：岩田雅之
11. **各式各樣的愛**（****）
      - ※堂本剛自己作曲作詞的獨唱作品。
      - 作曲：[堂本　剛](../Page/堂本剛.md "wikilink")
      - 作詞：堂本　剛
      - 編曲：知野芳彥
12. **Rocketman**
      - 作曲：[西川　進](../Page/西川進.md "wikilink")
      - 作詞：[相田　毅](../Page/相田毅.md "wikilink")
      - 編曲：[松永寛樹](../Page/松永寛樹.md "wikilink")
      - 和音編排：松下　誠
13. **青春時代**（****）
      - ※[堂本剛參與演出的](../Page/堂本剛.md "wikilink")[東京放送](../Page/東京放送.md "wikilink")[連續劇](../Page/連續劇.md "wikilink")『青之時代』主題曲。第4張單曲。
      - 作曲：[canna](../Page/canna.md "wikilink")
      - 作詞：canna
      - 編曲：[新川　博](../Page/新川博.md "wikilink")

[Category:近畿小子專輯](../Category/近畿小子專輯.md "wikilink")
[Category:1999年音樂專輯](../Category/1999年音樂專輯.md "wikilink")
[Category:1999年Oricon專輯週榜冠軍作品](../Category/1999年Oricon專輯週榜冠軍作品.md "wikilink")