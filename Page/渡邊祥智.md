**渡邊祥智**（、[血型B型](../Page/血型.md "wikilink")）。[日本](../Page/日本.md "wikilink")[女性](../Page/女性.md "wikilink")[漫畫家](../Page/漫畫家.md "wikilink")。[新潟縣出身](../Page/新潟縣.md "wikilink")。代表作是《[銀色勇者](../Page/銀色勇者.md "wikilink")》\[1\]。
2013年在月刊コミックアヴァウルス4月号上開始新連載「ひゃくイチ」。

## 作品

  - 單行本

<!-- end list -->

  - [銀色勇者](../Page/銀色勇者.md "wikilink")、全5冊、原名《》
  - [FUN FUN工房](../Page/FUN_FUN工房.md "wikilink")、全4冊、原名《》
  - [對面的對面](../Page/對面的對面.md "wikilink")、全6冊、原名《》
  - [CARAT\!光之魔法國](../Page/CARAT!光之魔法國.md "wikilink")、全2冊、原名《》 ISBN
    9789861009520
  - [docca 異世界](../Page/docca_異世界.md "wikilink")、全3冊、原名《》

<!-- end list -->

  - 單行本未收錄作品

<!-- end list -->

  - MORPHEUS（[LaLaDX](../Page/LaLaDX.md "wikilink")1994年7月10日號）

  - （[LaLa](../Page/LaLa.md "wikilink")1999年3月號）

  - （[LaLa](../Page/LaLa.md "wikilink")1999年4月號）

  - （[LaLa](../Page/LaLa.md "wikilink")2002年2月號）

  - （[LaLa](../Page/LaLa.md "wikilink")2002年3月號付録）

  - （[LaLa](../Page/LaLa.md "wikilink")2003年3月號 - 5月號連載。全3話完結）

<!-- end list -->

  - DVD

<!-- end list -->

  -
<!-- end list -->

  - 畫冊

<!-- end list -->

  - KIRARA

<!-- end list -->

  - 其他

<!-- end list -->

  -
## 参考

## 外部連結

  - [月刊コミックブレイドアヴァルス avarus OFFICIAL WEB
    SITE](http://www.mag-garden.co.jp/comic-blade/avarus/top.html)
  - [コミックホームズ 渡辺祥智（白泉社
    作品データベース）](http://comich.net/cr/wa/watanabe_yoshitomo.html)

[Category:日本漫画家](../Category/日本漫画家.md "wikilink")
[Category:新潟縣出身人物](../Category/新潟縣出身人物.md "wikilink")
[Category:少女漫畫家](../Category/少女漫畫家.md "wikilink")

1.