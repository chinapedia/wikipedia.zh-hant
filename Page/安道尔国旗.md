**安道尔国旗**是[西欧小](../Page/西欧.md "wikilink")[内陆国](../Page/內陸國家.md "wikilink")[安道尔公国的唯一合法代表国家的](../Page/安道尔.md "wikilink")[旗帜](../Page/国旗.md "wikilink")。此面旗是于1866年即為民間所用的旗幟。国旗是由蓝、黄、红三条垂直排列。其中蓝色与红色也是安道尔的北邻国[法国](../Page/法国.md "wikilink")[旗帜上所拥有的颜色](../Page/法国国旗.md "wikilink")，黄色与红色也是安道尔的南邻国[西班牙](../Page/西班牙.md "wikilink")[旗帜上所拥有的颜色](../Page/西班牙国旗.md "wikilink")。安道尔公国与其邻国法国与西班牙拥有着密切的关系，公国国旗上显示着两国的颜色也是一种妥协。有人则说当今的国旗源于[拿破仑三世的皇旗](../Page/拿破仑三世.md "wikilink")。此面国旗的设计属欧洲与前欧洲殖民地地区的旗帜中所常见的。常常会与[罗马尼亚](../Page/罗马尼亚国旗.md "wikilink")、[摩尔多瓦和](../Page/摩尔多瓦国旗.md "wikilink")[乍得闹混淆](../Page/乍得国旗.md "wikilink")。此旗本分出两套，一面是仅显示蓝、黄、红三色的[民用旗](../Page/民用旗.md "wikilink")。另一面中央显示着[国徽](../Page/安道尔国徽.md "wikilink")，是[政府旗](../Page/政府旗.md "wikilink")。但由于民用旗容易与[乍得与](../Page/查德國旗.md "wikilink")[罗马尼亚的旗帜混淆](../Page/羅馬尼亞國旗.md "wikilink")，所以现今安道尔居民基本上都使用显示着国徽的政府旗。

[Bandera_de_Andorra.jpg](https://zh.wikipedia.org/wiki/File:Bandera_de_Andorra.jpg "fig:Bandera_de_Andorra.jpg")

## 國旗格式

[centre](https://zh.wikipedia.org/wiki/File:Flag_of_Andorra_\(construction\).svg "fig:centre")

## 歷史國旗

Flag of Andorra 1806.svg|1806-1866 Flag of Andorra(1934).svg|1866-1934
Flag of Andorra (civil).svg|民用旗（现已少用） 比例：7:10

## 辨明

[罗马尼亚国旗的蓝](../Page/罗马尼亚国旗.md "wikilink")、黄、红均比安道尔国旗上的浅。[乍得国旗的黄比安道尔的浅](../Page/乍得国旗.md "wikilink")，并且乍得国旗比安道尔的宽（乍得国旗宽长比例为2:3，而安道尔为7:10）。

<File:Flag_of_Moldova.svg>|[摩尔多瓦国旗](../Page/摩尔多瓦国旗.md "wikilink")
<File:Flag_of_Chad.svg>|[乍得国旗](../Page/乍得国旗.md "wikilink")
<File:Flag_of_Romania.svg>|[罗马尼亚国旗](../Page/罗马尼亚国旗.md "wikilink")
<File:Flag> of Andorra
(civil).svg|[安道尔](../Page/安道尔.md "wikilink")[民用旗](../Page/民用旗.md "wikilink")
<File:Flag> of Andorra.svg|安道尔国旗

[A](../Category/国旗.md "wikilink")
[Category:安道爾](../Category/安道爾.md "wikilink")
[Category:1866年面世](../Category/1866年面世.md "wikilink")