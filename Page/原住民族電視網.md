**原住民族電視網**（**Aboriginal Peoples Television
Network**），簡稱**APTN**，是[加拿大的](../Page/加拿大.md "wikilink")[地區性](../Page/地區.md "wikilink")[廣播及](../Page/廣播.md "wikilink")[有線電視網絡](../Page/有線電視.md "wikilink")。APTN主要業務為播放和製作關於加拿大[原住民的節目](../Page/原住民.md "wikilink")。APTN是全球第一個為原住民設立的[電視台](../Page/電視台.md "wikilink")，其總部設於[曼尼托巴省首府](../Page/曼尼托巴省.md "wikilink")[溫尼伯](../Page/溫尼伯.md "wikilink")。

## 節目

APTN提供許多種類不同的原住民節目，如[紀錄片](../Page/紀錄片.md "wikilink")、[新聞雜誌](../Page/新聞.md "wikilink")、[劇集](../Page/劇集.md "wikilink")、[綜藝節目](../Page/綜藝節目.md "wikilink")、[兒童節目](../Page/兒童節目.md "wikilink")、[烹飪節目和](../Page/烹飪.md "wikilink")[教育節目等](../Page/教育.md "wikilink")。以使用的語言來分，約有60%的APTN節目是以[英語播放](../Page/英語.md "wikilink")，15%為[法語](../Page/法語.md "wikilink")，至於以原住民語言播出的節目則佔了約25%。

節目包括：

  - 《》
  - 《》
  - 《》
  - 《》
  - 《》
  - 《》

## 外部連結

  - [官方網頁](http://www.aptn.ca/)
  - [廣播博物館](http://www.museum.tv/archives/etv/T/htmlT/televisionno/televisionno.htm)

[Category:加拿大電視台](../Category/加拿大電視台.md "wikilink")
[Category:溫尼伯](../Category/溫尼伯.md "wikilink")
[Category:加拿大原住民](../Category/加拿大原住民.md "wikilink")
[Category:1992年成立的电视台或电视频道](../Category/1992年成立的电视台或电视频道.md "wikilink")