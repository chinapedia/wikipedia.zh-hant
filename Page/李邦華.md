**李邦华**（），字**孟闇**，江西[吉水人](../Page/吉水.md "wikilink")。明末政治人物。

受业于同里[邹元标](../Page/邹元标.md "wikilink")，[万历三十一年与父](../Page/万历.md "wikilink")[李廷谏同举](../Page/李廷谏.md "wikilink")[乡试](../Page/乡试.md "wikilink")。万历三十二年（1604年）[进士](../Page/进士.md "wikilink")，授[泾县知县](../Page/泾县.md "wikilink")，有政聲。當時朝士多詆毀[顾宪成](../Page/顾宪成.md "wikilink")，邦华為之辯解，遂被指為[东林黨人](../Page/东林黨.md "wikilink")。陈法祖用人十事：“曰内阁不当专用词臣，曰词臣不当专守馆局，曰词臣不当教习内书堂，曰六科都给事中不当内外间阻，曰御史升迁不当概论考满，曰吏部乞假不当积至正郎，曰关仓诸差不当专用举贡任子，曰调简推知不当骤迁京秩，曰进士改教不当概从内转，曰边方州县不当尽用乡贡。”沒有得到回應。万历四十四年以疾归鄉。

万历四十五年出为山东参议。天启元年恢復原官，整頓易州兵备，迁光禄少卿，擢右佥都御史。後被[魏黨削官](../Page/魏黨.md "wikilink")。

[崇祯元年](../Page/崇祯.md "wikilink")（1628年）四月，起[工部右侍郎](../Page/工部.md "wikilink")，旋改兵部。十二年（1639年）四月，起南京[兵部尚书](../Page/兵部尚书.md "wikilink")。崇禎十五年冬，代[刘宗周为左都御史](../Page/刘宗周.md "wikilink")，十六年三月抵九江。安撫宣稱要就食南京的[左良玉](../Page/左良玉.md "wikilink")，權宜發九江庫銀十五萬及糧食六個月，遂退其兵。

崇禎十七年（1644年）二月，[李自成陷山西](../Page/李自成.md "wikilink")。邦华请帝固守京师，未得反應。十八日，外城陷，逃至文信國祠\[1\]，十九日，内城亦陷，乃三揖信国曰：“邦华死国难，请从先生于[九京矣](../Page/九京.md "wikilink")。”有詩曰：“堂堂丈夫兮圣贤为徒，忠孝大节兮誓死靡渝，临危授命兮吾无愧吾。”遂投缳而死。赠太保、吏部尚书，谥**忠文**。清朝時赐谥**忠肃**。

## 注釋

<div class="references-small">

<references />

</div>

[Category:萬曆三十一年癸卯科舉人](../Category/萬曆三十一年癸卯科舉人.md "wikilink")
[Category:明朝涇縣知縣](../Category/明朝涇縣知縣.md "wikilink")
[Category:明朝山東布政使司參議](../Category/明朝山東布政使司參議.md "wikilink")
[Category:明朝光祿寺少卿](../Category/明朝光祿寺少卿.md "wikilink")
[Category:明朝僉都御史](../Category/明朝僉都御史.md "wikilink")
[Category:明朝工部侍郎](../Category/明朝工部侍郎.md "wikilink")
[Category:明朝兵部侍郎](../Category/明朝兵部侍郎.md "wikilink")
[Category:南京兵部尚書](../Category/南京兵部尚書.md "wikilink")
[Category:明朝左都御史](../Category/明朝左都御史.md "wikilink")
[Category:明朝自殺人物](../Category/明朝自殺人物.md "wikilink")
[Category:吉水人](../Category/吉水人.md "wikilink")
[B邦華](../Category/李姓.md "wikilink")
[Category:諡忠文](../Category/諡忠文.md "wikilink")
[Category:諡忠肅](../Category/諡忠肅.md "wikilink")

1.  [文天祥曾被封為信國公](../Page/文天祥.md "wikilink")