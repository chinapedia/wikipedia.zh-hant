**保罗·史蒂文斯·欧德宁**（，\[1\]），美國企業家，[Intel公司第五任](../Page/Intel.md "wikilink")[CEO](../Page/CEO.md "wikilink")，並擔任[Google公司](../Page/Google公司.md "wikilink")[董事會的](../Page/董事會.md "wikilink")[獨立董事](../Page/獨立董事.md "wikilink")。

## 教育背景

歐德寧畢業於St. Ignatius College
Preparatory，之後於1972年擁有了[舊金山大學](../Page/舊金山大學.md "wikilink")（University
of San
Francisco）[經濟學](../Page/經濟學.md "wikilink")[學士](../Page/學士.md "wikilink")[學位](../Page/學位.md "wikilink")。1974年，歐德寧又在[柏克萊加州大學](../Page/柏克萊加州大學.md "wikilink")[Haas商業學院取得](../Page/Haas商業學院.md "wikilink")[企業管理碩士學位](../Page/企業管理碩士.md "wikilink")。

## INTEL的經歷

歐德寧於1974年加入[Intel](../Page/Intel.md "wikilink")，之後1994年到1996年擔任業務與市場行銷部資深副總。1996年到1998年，升任至業務與市場行銷部執行副總。1998年到2002年擔任副[總經理與Intel架構事業部經理](../Page/總經理.md "wikilink")，負責公司的[微處理器與](../Page/微處理器.md "wikilink")[晶片組銷售](../Page/晶片組.md "wikilink")。

其中在1993年時，歐德寧特別向微處理器產品集團介紹[奔騰微處理器](../Page/奔騰.md "wikilink")。此外亦擔任了Intel與[IBM的窗口](../Page/IBM.md "wikilink")，且擔任[安迪·葛洛夫的技術助理](../Page/安迪·葛洛夫.md "wikilink")。2002年他當選為[董事會董事](../Page/董事會.md "wikilink")，並升任為總裁兼[首席營運長](../Page/首席營運長.md "wikilink")，負責營運整間公司。

2005年3月18日，歐德寧擔任了Intel全球總經理；同年5月18日，他取代了[Craig
Barrett成為了Intel公司第五任](../Page/Craig_Barrett.md "wikilink")[CEO](../Page/CEO.md "wikilink")。根據報導，歐德寧向來是說服[蘋果公司共同合作的重要力量](../Page/蘋果公司.md "wikilink")，同時也比較傳統的[Windows更喜歡](../Page/Windows.md "wikilink")[Mac
OS X](../Page/Mac_OS_X.md "wikilink")，並在[Windows
Vista發行時說](../Page/Windows_Vista.md "wikilink")：
「我們已經接觸Windows有一段很長時間，應該是要跟蘋果公司更加親密了。」此外，歐德寧亦特別注重中國IT產業發展和本土人才培養，從而影響了中國IT產業蓬勃發展。

2006年歐德寧負責處理自Intel有史以來最大規模的裁員，有10500名受雇員（近公司員工數的10%）被迫下崗。這次裁員無論是在產品製造、設計以及其他的冗餘處，大約在2008年時減少損失約30億美元。2007年3月26日，歐德寧親自在[人民大會堂宣布將要於](../Page/人民大會堂.md "wikilink")[中國](../Page/中國.md "wikilink")[大連市建造近](../Page/大連市.md "wikilink")25億美元、能生產300毫米晶圓的半導體製造工廠（Intel
Fab
68號廠），這也是Intel在亞洲的第一個晶圓廠。2009年11月4日，歐德寧在[北京表示](../Page/北京.md "wikilink")[摩爾定律仍然有效](../Page/摩爾定律.md "wikilink")，公司將會遵循摩爾定律繼續推動產業的發展。

2013年欧德宁正式[退休](../Page/退休.md "wikilink")，接任CEO职位，接任总裁职位。

2017年10月2日，欧德宁在其[加州](../Page/加州.md "wikilink")[索諾馬縣](../Page/索諾馬縣.md "wikilink")（Sonoma
County）的家中逝世，享年66歲\[2\]。英特爾公司並未公開欧德宁的死因\[3\]。

## 個人資訊

歐德寧的哥哥史蒂芬·歐德寧（Steven Otellini）是一位住在舊金山的羅馬天主教神父。

## 參考文獻

## 外部連結

  - [Intel 網站](http://www.intel.com/pressroom/kits/bios/otellini.htm)

[Category:英特爾人物](../Category/英特爾人物.md "wikilink")
[Category:舊金山大學校友](../Category/舊金山大學校友.md "wikilink")
[Category:加州大學柏克萊分校校友](../Category/加州大學柏克萊分校校友.md "wikilink")
[Category:義大利裔美國人](../Category/義大利裔美國人.md "wikilink")

1.
2.  <https://www.nytimes.com/2017/10/03/obituaries/paul-s-otellini-who-led-intel-and-saw-it-grow-even-more-dies-at-66.html?rref=collection%2Fsectioncollection%2Fobituaries&action=click&contentCollection=obituaries&region=rank&module=package&version=highlights&contentPlacement=2&pgtype=sectionfront>
3.  CNBC,['Former Intel CEO Paul Otellini has died at
    age 66'](https://www.cnbc.com/2017/10/03/former-intel-ceo-paul-otellini-dies-age-66.html),
    3 October 2017