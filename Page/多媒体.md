[SMPlayer_icon.svg](https://zh.wikipedia.org/wiki/File:SMPlayer_icon.svg "fig:SMPlayer_icon.svg")

**多媒体（Multimedia）**，在電腦應用系统中，组合两种或两种以上媒体的一种人机交互式資訊交流和传播[媒体](../Page/媒体.md "wikilink")。使用的媒体包括[文字](../Page/文字.md "wikilink")、[图片](../Page/图片.md "wikilink")、[照片](../Page/照片.md "wikilink")、[声音](../Page/声音.md "wikilink")（包含音樂、語音旁白、特殊音效）、[动画和](../Page/动画.md "wikilink")[影片](../Page/影片.md "wikilink")，以及[程序所提供的](../Page/程序.md "wikilink")[互動功能](../Page/互動.md "wikilink")。

超媒体（）是多媒体系统中的一个子集，超媒体系统是使用超連結（）构成的全球資訊系統，全球資訊系统是網際網路上使用[TCP](../Page/TCP.md "wikilink")/IP协议和[UDP](../Page/UDP.md "wikilink")/IP协议的应用系统。2D的多媒体网页使用[HTML](../Page/HTML.md "wikilink")、[XML等语言编写](../Page/XML.md "wikilink")，3d的多媒体网页使用[VRML等语言编写](../Page/VRML.md "wikilink")。目前许多多媒体作品使用光碟发行，以后将更多地使用网络发行。[Acer.aspire-522.amd-fusion.debian_1c555_screen02.png](https://zh.wikipedia.org/wiki/File:Acer.aspire-522.amd-fusion.debian_1c555_screen02.png "fig:Acer.aspire-522.amd-fusion.debian_1c555_screen02.png")

## 多媒体的应用层面

目前为止，多媒体的应用领域已涉足诸如[广告](../Page/广告.md "wikilink")、[艺术](../Page/艺术.md "wikilink")，[教育](../Page/教育.md "wikilink")，[娱乐](../Page/娱乐.md "wikilink")，工程，医药，商业及科学研究等行业。

利用多媒体网页，商家可以将广告变成有声有画的互动形式，可以更吸引用家之余，也能够在同一时间内向准买家提供更多商品的消息，但[下载时间太长](../Page/下载.md "wikilink")，是采用多媒体制作广告的一大缺点。

利用多媒体作教学用途，除了可以增加自学过程的[互动性](../Page/互动.md "wikilink")，更可以吸引学生学习、提升学习兴趣、以及利用[视觉](../Page/视觉.md "wikilink")、[听觉及](../Page/听觉.md "wikilink")[触觉三方面的](../Page/触觉.md "wikilink")[反馈来增强学生对知识的吸收](../Page/反馈.md "wikilink")。

多媒体技术是一种迅速发展的综合性[电子信息技术](../Page/电子信息技术.md "wikilink")，它给传统的電腦系统、聲音和影片设备带来了方向性的变革，将对[大众传媒产生深远的影响](../Page/大众传媒.md "wikilink")。多媒体應用將加速電腦進入家庭和社会各个方面的進步，给人们的工作、生活和娱乐带来深刻的革命。

多媒体还可以应用于數位圖書館、數位博物館领域此外，交通监控等也可使用多媒体技术进行相关监控。

## 参見

  - [多媒體短訊](../Page/多媒體短訊.md "wikilink")
  - [後文字社會](../Page/後文字社會.md "wikilink")

## 外部連結

  - [History of Multimedia from the University of
    Calgary](http://www.acs.ucalgary.ca/~edtech/688/hist.htm)
  - [Multimedia in Answers.com](http://www.answers.com/topic/multimedia)

[Category:藝術類型](../Category/藝術類型.md "wikilink")
[Category:设计](../Category/设计.md "wikilink")
[Category:媒體](../Category/媒體.md "wikilink")