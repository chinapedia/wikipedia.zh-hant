[虎尾郡役所02.JPG](https://zh.wikipedia.org/wiki/File:虎尾郡役所02.JPG "fig:虎尾郡役所02.JPG")
[Old_Kobi_Library.jpg](https://zh.wikipedia.org/wiki/File:Old_Kobi_Library.jpg "fig:Old_Kobi_Library.jpg")
**虎尾郡**（）為[臺灣日治時期的行政區劃之一](../Page/臺灣日治時期.md "wikilink")，隸屬[臺南州](../Page/臺南州.md "wikilink")，[虎尾郡役所設於虎尾](../Page/虎尾郡役所.md "wikilink")-{庄}-（後改[虎尾街](../Page/虎尾街.md "wikilink")），下轄虎尾-{庄}-（街）、[西螺街](../Page/西螺街.md "wikilink")、土庫-{庄}-（後改[土庫街](../Page/土庫街.md "wikilink")）、[二崙-{庄}-](../Page/二崙庄.md "wikilink")、[崙背-{庄}-](../Page/崙背庄.md "wikilink")、[海口-{庄}-](../Page/海口庄.md "wikilink")。轄域即今[雲林縣](../Page/雲林縣.md "wikilink")[虎尾鎮](../Page/虎尾鎮.md "wikilink")、[西螺鎮](../Page/西螺鎮.md "wikilink")、[土庫鎮](../Page/土庫鎮.md "wikilink")、[褒忠鄉](../Page/褒忠鄉.md "wikilink")、[二崙鄉](../Page/二崙鄉.md "wikilink")、[崙背鄉](../Page/崙背鄉.md "wikilink")、[麥寮鄉](../Page/麥寮鄉.md "wikilink")、[台西鄉](../Page/臺西鄉.md "wikilink")、[東勢鄉等地](../Page/東勢鄉.md "wikilink")。

## 土庫設郡期成同盟會

由於土庫長期為雲林中西部地區的政商中心，1920年廢廳改州前也設有土庫支廳，土庫區為支廳所在。因此要設置虎尾郡時，土庫-{庄}-的仕紳曾對此感到不滿，發起土庫設郡期成同盟會，要求臺南州廳將郡名改為「土庫郡」，郡役所改設於土庫-{庄}-，但因為在虎尾設糖廠的大日本製糖藤山家族勢力，以及專家學者認為虎尾尚有發展潛力，遂仍依原計劃設虎尾郡，土庫設郡期成同盟會被迫解散，不了了之。

## 統計

<table>
<caption>1942年本籍國籍別常住戶口[1]</caption>
<thead>
<tr class="header">
<th style="text-align: center;"><p>街-{庄}-名</p></th>
<th style="text-align: center;"><p>面積<br />
(km²)</p></th>
<th style="text-align: center;"><p>人口 (人)</p></th>
<th style="text-align: center;"><p>人口密度<br />
(人/km²)</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p>本籍</p></td>
<td style="text-align: center;"><p>國籍</p></td>
<td style="text-align: center;"><p>計</p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>臺灣</p></td>
<td style="text-align: center;"><p>內地</p></td>
<td style="text-align: center;"><p>朝鮮</p></td>
<td style="text-align: center;"><p>中華民國</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>虎尾街</p></td>
<td style="text-align: center;"><p>66.7420</p></td>
<td style="text-align: center;"><p>27,526</p></td>
<td style="text-align: center;"><p>3,277</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>西螺街</p></td>
<td style="text-align: center;"><p>51.2985</p></td>
<td style="text-align: center;"><p>30,952</p></td>
<td style="text-align: center;"><p>308</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>二崙-{庄}-</p></td>
<td style="text-align: center;"><p>62.0625</p></td>
<td style="text-align: center;"><p>20,391</p></td>
<td style="text-align: center;"><p>84</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>崙背-{庄}-</p></td>
<td style="text-align: center;"><p>127.6508</p></td>
<td style="text-align: center;"><p>29,744</p></td>
<td style="text-align: center;"><p>122</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>海口-{庄}-</p></td>
<td style="text-align: center;"><p>92.4545</p></td>
<td style="text-align: center;"><p>30,974</p></td>
<td style="text-align: center;"><p>76</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p>土庫-{庄}-</p></td>
<td style="text-align: center;"><p>84.0764</p></td>
<td style="text-align: center;"><p>28,807</p></td>
<td style="text-align: center;"><p>675</p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><p>虎尾郡</p></td>
<td style="text-align: center;"><p>484.2846</p></td>
<td style="text-align: center;"><p>168,394</p></td>
<td style="text-align: center;"><p>4,542</p></td>
</tr>
<tr class="even">
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

## 參考資料

[Category:臺南州](../Category/臺南州.md "wikilink")
[虎尾郡](../Category/虎尾郡.md "wikilink")

1.