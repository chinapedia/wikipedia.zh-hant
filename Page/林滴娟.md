**林滴娟**，[台灣政治人物](../Page/台灣.md "wikilink")，出生於[雲林縣](../Page/雲林縣.md "wikilink")。在中國被綁架殺害，而[中華人民共和國官方不認爲其身為](../Page/中華人民共和國.md "wikilink")[高雄市議員的林滴娟具有](../Page/高雄市議員.md "wikilink")「特殊身份」，而引發一些台灣人的情緒反彈。

## 生平

在就讀[國立台灣大學歷史系時](../Page/國立台灣大學.md "wikilink")，便參與[台大大論社與](../Page/台大大論社.md "wikilink")[台大濁水溪社的活動](../Page/台大濁水溪社.md "wikilink")，並且參與[學生運動](../Page/學生運動.md "wikilink")。1989年[鄭南榕自焚時](../Page/鄭南榕.md "wikilink")，林滴娟在台大發起靜坐運動聲援，同年加入[民主進步黨並協助](../Page/民主進步黨.md "wikilink")[周慧瑛參選](../Page/周慧瑛.md "wikilink")[台灣省議員](../Page/台灣省議員.md "wikilink")，1991年到[高雄市替](../Page/高雄市.md "wikilink")[陳菊參選](../Page/陳菊.md "wikilink")[國大代表助選](../Page/國大代表.md "wikilink")。1993年，與名主持人[魚夫在當時](../Page/魚夫.md "wikilink")­還是[地下電台的](../Page/地下電台.md "wikilink")[南台灣之聲廣播電台](../Page/南台灣之聲.md "wikilink")，每天下午五點的南北連線主持「黃昏南台灣」節目，轟動一時，打響知名度。

1994年，當選[中華民國](../Page/中華民國.md "wikilink")[高雄市議員](../Page/高雄市議員.md "wikilink")，時年28歲。在[高雄市議會質詢同為台大歷史系畢業的](../Page/高雄市議會.md "wikilink")[高雄市長](../Page/高雄市長.md "wikilink")[吳敦義](../Page/吳敦義.md "wikilink")，常被媒體稱為「小學妹對抗老學長」。

## 林滴娟事件

1998年7月，林滴娟與男友[韋殿剛前往中國](../Page/韋殿剛.md "wikilink")[大連](../Page/大連.md "wikilink")。結果因韋殿剛的債務問題，兩人被當地人綁架，林滴娟被施打過量藥物而[休克死亡](../Page/休克.md "wikilink")。\[1\]當時的[中國官方認定這是](../Page/北京政府.md "wikilink")「民間糾紛」事件，由地方政府辦理整起事件­。偵辦刑事部份，由地方公安辦理；牽扯到與林滴娟親友的商談，則­由地方的[台灣事務辦公室辦理](../Page/國務院臺灣事務辦公室.md "wikilink")。

當尋獲遺體時，當地台辦和公安忽视林滴娟為高雄市議員的身份，因此引發台灣官方與民間的一致譴責。[新華社此後發佈一項簡短的消息說道](../Page/新華社.md "wikilink")，僅稱林滴娟的身份為一台灣女子，並没有提到其議員身份與整個事件的始末。

遺體於1998年8月6日被運回台灣，8月18日火化安葬，墓園在[高雄縣](../Page/高雄縣.md "wikilink")[內門鄉的寶山陵園](../Page/內門區.md "wikilink")。涉嫌綁架殺害的兇手雖然被槍斃並判定賠償，但因被告並無財產，所以林滴娟的家屬也無法拿到賠償金。

### 中共官方態度

中共當局僅同意林滴娟家屬、好友及台灣媒體到[遼寧辦理相關事宜](../Page/遼寧.md "wikilink")，而不同意民進黨的五人小組、[海基會與](../Page/海基會.md "wikilink")[陸委會相關人士協助處理](../Page/陸委會.md "wikilink")。

### 民進黨態度

民進黨表示：「中國政府在此一事件的處理上，充分顯示出對於[人權價值的漠視與對人身安全重要性的輕忽](../Page/人權.md "wikilink")。」民進黨也表示，海基會是處理善後事宜台灣理所當然的法定代表。

## 參考文獻

[Category:第4屆高雄市議員
(縣市改制前)](../Category/第4屆高雄市議員_\(縣市改制前\).md "wikilink")
[Category:臺灣民主運動者](../Category/臺灣民主運動者.md "wikilink")
[Category:民主進步黨黨員](../Category/民主進步黨黨員.md "wikilink")
[Category:臺灣海峽兩岸關係史](../Category/臺灣海峽兩岸關係史.md "wikilink")
[Category:國立臺灣大學文學院校友](../Category/國立臺灣大學文學院校友.md "wikilink")
[Category:在中國被謀殺身亡者](../Category/在中國被謀殺身亡者.md "wikilink")
[Category:被杀害的政治人物](../Category/被杀害的政治人物.md "wikilink")
[Category:雲林人](../Category/雲林人.md "wikilink")
[Di滴](../Category/林姓.md "wikilink")

1.