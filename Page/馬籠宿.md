[Magome-Juku_Milestone.jpg](https://zh.wikipedia.org/wiki/File:Magome-Juku_Milestone.jpg "fig:Magome-Juku_Milestone.jpg")
**馬籠宿**（）是一位於[日本](../Page/日本.md "wikilink")[岐阜縣](../Page/岐阜縣.md "wikilink")[中津川市](../Page/中津川市.md "wikilink")（原屬[長野縣](../Page/長野縣.md "wikilink")[木曾郡](../Page/木曾郡.md "wikilink")，但在2005年2月時的中被編入岐阜縣內）的日本古驛道[宿場遺跡](../Page/宿場.md "wikilink")，其與隔鄰的[妻籠宿一同都是該縣著名的觀光景點](../Page/妻籠宿.md "wikilink")。馬籠宿是[中山道六十九次](../Page/中山道六十九次.md "wikilink")（[中山道沿線的](../Page/中山道.md "wikilink")69個宿場）之一，名列第43次，也是[信濃國境內木曾十一宿中位置最南的一個](../Page/信濃國.md "wikilink")。

由於曾經在1895年（[明治](../Page/明治.md "wikilink")28年）與1915年（[大正](../Page/大正.md "wikilink")4年）時發生過[大火](../Page/火災.md "wikilink")，整條老街除了中央的石造階梯與被稱為「枡形\[1\]」的隘口之外，皆為事後復原重建。但由於石板坡道旁的老街建築大抵維持了[江戶時代的原樣](../Page/江戶時代.md "wikilink")，頗受觀光客歡迎，其中以宿場[本陣](../Page/本陣.md "wikilink")（主屋）所改成的（舊本陣
島崎藤村生家跡）是用於紀念生長在此屋、明治時代著名的[小說家與](../Page/小說家.md "wikilink")[詩人](../Page/詩人.md "wikilink")[島崎藤村](../Page/島崎藤村.md "wikilink")，他的長篇歷史小說《》（）就是以故鄉馬籠作為舞台所撰著的。

## 圖片集

<File:Magome-Juku> StoneSlope.jpg|馬籠宿的坡道與兩旁的江戶建築 <File:Magome-Juku>
Touson Memorial Hall.jpg|島崎紀念館，原本曾是馬籠宿的舊本陣（供貴族階級住宿用的宿場主屋，通常都是借用自當地仕紳的住宅）
<File:Kisokaido43>
Magome.jpg|[溪齋英泉所繪的](../Page/溪齋英泉.md "wikilink")《木曾街道六十九次、馬籠》
<File:Foursquare> of Magome-juku 1.jpg|[水車](../Page/水車.md "wikilink")
<File:Magome-juku(post> town) , 馬籠宿 - panoramio
(20).jpg|[荞麦面](../Page/荞麦面.md "wikilink")(左)[咖啡店](../Page/咖啡店.md "wikilink")(右)

## 註釋

## 參考文獻

  - [馬籠觀光協會](http://kiso-magome.com/)

[fr:Magome](../Page/fr:Magome.md "wikilink")

[Category:岐阜縣觀光地](../Category/岐阜縣觀光地.md "wikilink")
[Category:長野縣歷史](../Category/長野縣歷史.md "wikilink")
[Category:中山道六十九次](../Category/中山道六十九次.md "wikilink")
[Category:中津川市](../Category/中津川市.md "wikilink")

1.  「枡」發音同「升」