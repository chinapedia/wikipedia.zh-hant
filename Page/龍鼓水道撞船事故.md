**龍鼓水道撞船意外**是於2008年3月22日，發生在[香港](../Page/香港.md "wikilink")[水域的](../Page/領海.md "wikilink")[船難](../Page/船難.md "wikilink")，共18人[死亡](../Page/死亡.md "wikilink")，為1971年[佛山號翻沉事故後至](../Page/佛山號翻沉事故.md "wikilink")2012年[南丫島撞船事故前](../Page/南丫島撞船事故.md "wikilink")，香港最嚴重的海上事故。

2008年3月22日星期六[晚上](../Page/晚上.md "wikilink")9時26分，一艘[烏克蘭遠洋拖船](../Page/烏克蘭.md "wikilink")[*Neftegaz*-67](../Page/Neftegaz-67.md "wikilink")，在香港[龍鼓水道近](../Page/龍鼓水道.md "wikilink")[大小磨刀洲對開海面](../Page/大小磨刀洲.md "wikilink")，跟一艘[中國大陸散裝船發生碰撞](../Page/中國大陸.md "wikilink")，拖船其後沉沒，船上25位[船員中](../Page/船員.md "wikilink")，7名獲救，包括[船長](../Page/船長.md "wikilink")，其餘18人[失蹤](../Page/失蹤.md "wikilink")。事後，廣州打撈局的起重船南天馬第一時間到達現場，可惜水流太急，沉船太重，其後拖船公司租用[亞洲大型的起重船](../Page/亞洲.md "wikilink")[華天龍](../Page/華天龍.md "wikilink")[打撈](../Page/打撈.md "wikilink")[沉船](../Page/沉船.md "wikilink")。

烏克蘭政府表示關切，並且派遣[烏克蘭駐華大使館館員處理事後工作](../Page/烏克蘭駐華大使館.md "wikilink")。

涉案船隻是中國大陸貨船耀海，全長150米，總噸位70,000噸\[1\]。沉沒船隻在2003年曾經來香港，當時因為安全設施不合格，被[海事處一度扣查](../Page/海事處.md "wikilink")。

## 時序

### 3月22日

  - 21:13，撞船意外。
  - 21:36，海事處、[香港水警及](../Page/香港水警.md "wikilink")[消防船等到場](../Page/消防船.md "wikilink")，救起7名墮海船員。
  - 23:54，消防處[蛙人落水搜查](../Page/蛙人.md "wikilink")，找不到沉船位置。

### 3月23日

  - 04:20，[水道測量船發現沉船在](../Page/水道測量船.md "wikilink")400米外的海底，船底向天。
  - 06:00，蛙人接近沉船，敲打船身，但是回應全無，估計凶多吉少。

### 4月16日

  - 中午，[廣州打撈船](../Page/廣州.md "wikilink")[華天龍駛達](../Page/華天龍.md "wikilink")[屯門對開海面](../Page/屯門.md "wikilink")[大小磨刀](../Page/大小磨刀.md "wikilink")。預計打撈工作需時2個星期\[2\]。

### 4月27日

  - 沉船開始由華天龍撈上水面，下一步是修補船身，令之能夠自浮。

### 4月28日

  - 華天龍號將烏克蘭補給船打撈上海面，逾百名[災難遇害者辨認組](../Page/災難遇害者辨認組.md "wikilink")[警務人員登船大規模](../Page/警務人員.md "wikilink")[搜索](../Page/搜索.md "wikilink")，發現14具屍體。另打撈人員在沉船甲板上檢查，評估船隻損毀程度及自浮能力。\[3\]

### 4月29日

  - 最後一具失蹤船員屍體已被尋獲。

## 刑事起訴

烏克蘭補給船船長Kulemesin
Yuriy、耀海號船長劉波、領航員鄧鐸華、副領航員秦華德被控以[危害他人海上安全](../Page/危害他人海上安全.md "wikilink")，在2010年1月13日於[區域法院被判罪名成立](../Page/區域法院.md "wikilink")，Kulemesin
Yuriy和鄧鐸華均被判入獄36個月，其餘兩被告監禁28個月。

## 影響

  - [華天龍號來香港打撈烏克蘭沉船](../Page/華天龍號.md "wikilink")，日租費200萬。
  - [馬來西亞一家公司乘華天龍是次來香港的機會](../Page/馬來西亞.md "wikilink")，向香港法院申請[扣押令](../Page/扣押令.md "wikilink")，事原一單與華天龍船主的商業合約訴訟，涉款約一億[美元](../Page/美元.md "wikilink")。

## 參考文獻

  - [起吊烏克蘭沉船，香港政府無計可施，期待中國政府出手協助救援](https://web.archive.org/web/20080329232121/http://hk.news.yahoo.com/080324/12/2r3v8.html)
  - 星島日報2008年5月30日第A12法庭版，華天龍扣押涉保證金6千5百萬。

## 類似事件

[Category:香港航海事故](../Category/香港航海事故.md "wikilink")
[Category:2008年香港](../Category/2008年香港.md "wikilink")
[Category:2008年3月](../Category/2008年3月.md "wikilink")
[Category:大小磨刀](../Category/大小磨刀.md "wikilink")
[Category:2008年航海事故](../Category/2008年航海事故.md "wikilink")

1.
2.
3.