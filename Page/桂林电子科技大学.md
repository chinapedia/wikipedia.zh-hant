[Guilin001.jpg](https://zh.wikipedia.org/wiki/File:Guilin001.jpg "fig:Guilin001.jpg")
**桂林電子科技大學**（），原名**桂林电子工业学院**，是一所以[电子信息学科为优势](../Page/电子.md "wikilink")，机电结合、理、工、文、管、法等多学科相互渗透的[大学](../Page/大学.md "wikilink")，也是全国四所电子科技大学之一\[1\]。位于世界著名的风景旅游城市和历史文化名城[中国](../Page/中华人民共和国.md "wikilink")[广西壮族自治区](../Page/广西壮族自治区.md "wikilink")[桂林市东部](../Page/桂林市.md "wikilink")。该校有东校区、西校区和正在兴建的花江校区和位于广西北海市的北海校区共四个校区。

## 历史

1960年，[中华人民共和国第一机械工业部批准在](../Page/中华人民共和国.md "wikilink")[桂林建立桂林机械专科学校](../Page/桂林.md "wikilink")，同年，桂林机械专科学校和桂林技工学校合并，成立桂林机械专科学校。

1962年，桂林机械专科学校更名为桂林机械工业学校，学校由专科改为中专。

1972年，学校更名为桂林无线电学校。

1980年，经[中华人民共和国国务院批准](../Page/中华人民共和国国务院.md "wikilink")，在桂林无线电学校的基础上建立桂林电子工业学院。

1990年11月19日，时任[中共中央总书记](../Page/中共中央总书记.md "wikilink")[江泽民亲临学院并题词](../Page/江泽民.md "wikilink")“为发展电子工业培养更多的合格人才”。

2006年3月1日，更名为桂林电子科技大学。

2009年2月，[中国科学院院士](../Page/中国科学院院士.md "wikilink")[孙家栋受广西壮族自治区人民政府聘请出任桂林电子科技大学名誉校长](../Page/孙家栋.md "wikilink")。

## 现状

桂林電子科技大學的学科主要以电子信息类的工学为主，还有机电类工学、
[管理学](../Page/管理学.md "wikilink")、[法学](../Page/法学.md "wikilink")、[文学等几大类学科](../Page/文学.md "wikilink")。2010年，有在校各类学生32000名
，专任教师1500余名，其中50%以上拥有[博士](../Page/博士.md "wikilink")、[硕士学历](../Page/硕士.md "wikilink")。学院现有50个本科专业
，34个硕士学位授予学科，7个学科领域具有工程硕士专业学位授予权，1个“与合作高校联合培养
、招生计划单列”博士点，是工商管理硕士（MBA）培养单位。

## 图书馆

桂林电子科技大学图书馆始建于1960年，现有馆舍面积13646平方米，分为东校区图书馆和尧山校区图书馆，
尧山校区图书馆工程是2009年自治区庆祝新中国成立60周年重大项目中单体建筑规模最大的工程，也是该校
建校以来建筑面积最大的一项工程。项目设计充分依托桂林的山秀水美和尧山风景区的地理及生态环，该项目
建筑面积4.2万平方米，计划投资5000万元，于2013年内建成并投入使用。图书馆设有办公室、技术信息部、
采编部、流通部、阅览部、尧山管理部等业务部门。 图书馆现有纸质图书总量超过170万册，电子图书118万种
学位论文150万篇,中外文现刊2928种，中外文电子期刊16084种。几年来先后引进美国IEEE/IET
Electronic Library、荷兰Elsevier Science期刊全文、美国John Wiley期刊全文
、SPIE国际光学工程学会、美国计算机学会ACM等24个国外电子数据库,同时也引进CNKI等27个国内电子图书、
期刊数据库，目前拥有51个中外电子文献数据库，形成了电子文献与纸质文献相结合、网络资源与本地资源相配套
，以电子信息类文献十分丰富为特色，涵盖机械、材料、交通、法律、管理、环境、语言、艺术、数理科学以及其他
社会科学文献协调发展的馆藏体系。 1995年引进ILAS系统，全面实现图书馆工作的自动化。电子阅览室有250台微机
供读者免费使用，读者可以非常方便地利用图书馆的电子资源，进行信息检索、文献传递、读者借阅信息查询和网上
咨询解答等。2009年实现无线上网。图书馆的网上文献信息服务系统全天24小时开放。
北海图书馆属于初建状态，目前馆舍较为简陋，只有两层小楼。

## 师资力量

桂林电子科技大学现有教职工2100多人，其中教授168人，博士241人，博士生导师27人。经过多年建设，形成了
一批以学科带头人为核心的结构合理、优势互补、团结协作、勇于攻关的学术梯队，有国家级教学团队2个，自治区级
教学团队8个，广西高校人才小高地创新团队5个，享受国务院特殊津贴专家32人，全国新世纪“百千万人才工程”人选、
教育部“新世纪优秀人才支持计划”人选、全国杰出专业技术人才等国家级和省部级各类高层次人才80余人。

## 学科特色

学校建有28个实验中心（室），1个省部共建教育部重点实验室，3个原信息产业部重点实验室，3个广西
重点实验室，6个广西高校重点实验室，1个广西高校重点建设研究基地，2个国家实验教学示范中心，5个
自治区级实验教学示范中心，1个自治区级实验教学示范中心建设单位。拥有国家软件与集成电路公共服务
平台（CSIP）广西分中心。此外，学校还与国内外大企业建立了多个联合实验室。拥有14个省部级重点学科，
在信息与通信工程、控制科学与工程、机械工程、材料科学与工程、仪器科学与技术、计算机科学与技术等 学科领域的研究已经进入国内先进行列。

依托学科优势，学校积极开展前沿性基础研究和高水平探索研究。近五年，共承担了国家“973”项目、
“863”项目、国家自然科学基金重点项目、国家电子信息产业发展基金项目，国防科技攻关项目和国家
社会科学基金等国家级科研项目143项；省部级科研基金、科技攻关项目等地方政府项目361项。“十五”
以来，学校主持和参研的科研项目获得包括国家科技进步二等奖、国防科学技术二等奖、广西科技进步一等奖
在内的科研成果奖77项。

## 院系设置

  - 职业技术学院NO.0
  - 机电工程学院NO.1
  - 信息与通信学院NO.2
  - 计算机与信息安全学院NO.3
  - 艺术与设计学院 NO.4
  - 商学院NO.5
  - 外国语学院NO.6
  - 数学与计算科学学院NO.7
  - 电子工程与自动化学院NO.8
  - 法学院NO.9
  - 材料科学与工程学院NO.10
  - 成人教育学院NO.11
  - 应用科技学院NO.12
  - 国际学院NO.13
  - 信息科技学院NO.14
  - 建筑与交通工程学院NO.15
  - 海洋信息工程学院NO.16

## 参考资料

  - [学校简介](https://web.archive.org/web/20060904031045/http://www.gliet.edu.cn/gaikuang/)，桂林电子科技大学网站。

## 参看

  - [中国大学列表](../Page/中国大学列表.md "wikilink")

## 外部链接

  - [桂林電子科技大學主页](http://www.gliet.edu.cn)

[Category:桂林高等院校](../Category/桂林高等院校.md "wikilink")
[Category:中國科技高等院校](../Category/中國科技高等院校.md "wikilink")
[Category:1960年創建的教育機構](../Category/1960年創建的教育機構.md "wikilink")
[Category:桂林电子科技大学](../Category/桂林电子科技大学.md "wikilink")

1.