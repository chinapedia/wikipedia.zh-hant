**古神翼龍科**（学名：*Tapejaridae*）屬於[翼龍目](../Page/翼龍目.md "wikilink")[翼手龍亞目](../Page/翼手龍亞目.md "wikilink")，生存於早[白堊紀](../Page/白堊紀.md "wikilink")。古神翼龍科的成員發現於[巴西與](../Page/巴西.md "wikilink")[中國](../Page/中國.md "wikilink")，目前最原始的屬發現於中國，顯示古神翼龍科起源於[亞洲](../Page/亞洲.md "wikilink")。\[1\]

## 分類

### 分類學

古神翼龍科包含兩個亞科：古神翼龍亞科、掠海翼龍亞科。古神翼龍亞科包含：[華夏翼龍](../Page/華夏翼龍.md "wikilink")、[中國翼龍](../Page/中國翼龍.md "wikilink")、[沃氏古神翼龍](../Page/古神翼龍.md "wikilink")、*T.
navigans*、[雷神翼龍](../Page/雷神翼龍.md "wikilink")；掠海翼龍亞科包含：[掠海翼龍](../Page/掠海翼龍.md "wikilink")、[妖精翼龍](../Page/妖精翼龍.md "wikilink")。\[2\]在2008年，[呂君昌等人提出掠海翼龍亞科較接近](../Page/呂君昌.md "wikilink")[神龍翼龍科](../Page/神龍翼龍科.md "wikilink")，而離古神翼龍科較遠，\[3\]因此建立為[翼龍科](../Page/翼龍科.md "wikilink")，\[4\]但是掠海翼龍亞科的命名較早。\[5\]

  - **古神翼龍科 Tapejaridae**
      - **古神翼龍亞科 Tapejarinae** <small>Kellner, 1989</small>
          - [包科尼翼龍](../Page/包科尼翼龍.md "wikilink") *Bakonydraco*
            <small>（分類未定）Ősi *et al.*, 2005</small>
          - [凱瓦神翼龍](../Page/凱瓦神翼龍.md "wikilink") *Caiuajara*
            <small>Manzig *et al.*, 2014</small>
          - [歐洲神翼龍](../Page/歐洲神翼龍.md "wikilink") *Europejara*
            <small>Vullo *et al.*, 2012</small>
          - [華夏翼龍](../Page/華夏翼龍.md "wikilink") *Huaxiapterus* <small>Lü
            & Yuan, 2005</small>
          - [中國翼龍](../Page/中國翼龍.md "wikilink") *Sinopterus* <small>Wang
            & Zhou, 2003</small>
          - [古神翼龍](../Page/古神翼龍.md "wikilink") *Tapejara*
            <small>Kellner, 1989</small>
          - [雷神翼龍](../Page/雷神翼龍.md "wikilink") *Tupandactylus*
            <small>Kellner & Campos, 2007</small>
      - **掠海翼龍亞科 Thalassodrominae**
          - [掠海翼龍](../Page/掠海翼龍.md "wikilink") *Thalassodromeus*
          - [妖精翼龍](../Page/妖精翼龍.md "wikilink") *Tupuxuara*

### 系統發生學

古神翼龍科與其他翼龍類的關係，以及在[神龍翼龍超科的演化位置](../Page/神龍翼龍超科.md "wikilink")，長期以來處在爭論中，已有許多研究提出不同的[演化樹](../Page/演化樹.md "wikilink")。古神翼龍科的成員也有爭議，某些研究人員主張掠海翼龍亞科屬於古神翼龍科，\[6\]而其他研究人員認為掠海翼龍亞科較接近[新神龍翼龍類的](../Page/新神龍翼龍類.md "wikilink")[神龍翼龍科](../Page/神龍翼龍科.md "wikilink")。\[7\]某些研究指出，古神翼龍科可能是個[並系群](../Page/並系群.md "wikilink")，並非天然生物群，部份物種演化成新神龍翼龍類。\[8\]以下兩個演化樹是由David
M. Martill、Martill與[德恩·奈許](../Page/德恩·奈許.md "wikilink")（Darren
Naish）在2006年提出，第一個是[單系群版本](../Page/單系群.md "wikilink")，第二個是[並系群版本](../Page/並系群.md "wikilink")。

## 參考資料

[Category:神龍翼龍超科](../Category/神龍翼龍超科.md "wikilink")

1.  Lü, J., Jin, X., Unwin, D.M., Zhao, L., Azuma, Y., and Ji, Q.
    (2006). A new species of *Huaxiapterus* (Pterosauria:
    Pterodactyloidea) from the Lower Cretaceous of western Liaoning,
    China with comments on the systematics of tapejarid pterosaurs.
    *Acta Geologica Sinica* **80**(3):315-326.

2.

3.  Lü, J., Unwin, D.M., Xu, L., and Zhang, X. (2008). "A new
    azhdarchoid pterosaur from the Lower Cretaceous of China and its
    implications for pterosaur phylogeny and evolution."
    *Naturwissenschaften*,

4.  Martill, D.M., Bechly, G., and Heads, S.W. (2007). "Appendix:
    species list for the Crato Formation." In: Martill, D.M., Bechly,
    G., and Loveridge, R.F. (eds.), 2007. *The Crato Fossil Beds of
    Brazil: Window into an Ancient World.* Cambridge University Press,
    Cambridge. Pp. 582–607.

5.
6.
7.  Martill, D.M. and Naish, D. (2006). "Cranial crest development in
    the azhdarchoid pterosaur *Tupuxuara*, with a review of the genus
    and tapejarid monophyly." *Palaeontology*, **49**: 925-941.

8.