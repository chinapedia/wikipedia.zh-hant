《**Separate
Ways**》是[香港](../Page/香港.md "wikilink")[歌手](../Page/歌手.md "wikilink")[王菲的第四張](../Page/王菲.md "wikilink")[單曲](../Page/單曲.md "wikilink")，第一張[日語](../Page/日語.md "wikilink")[單曲](../Page/單曲.md "wikilink")，於2001年7月25日由[東芝EMI株式會社在](../Page/東芝EMI株式會社.md "wikilink")[日本出版](../Page/日本.md "wikilink")。一共5首歌，1首是之前發表過的歌曲。

## 簡介

新歌只有1首，「Separate
Ways」，是王菲主演的[日劇](../Page/日劇.md "wikilink")《弄假成真》的主題曲。另附有128
Beat Mix版和Instrumental版，並收錄了之前發表過的「Eyes on Me」。

## 唱片版本

  - 日本版：紅色日文側標，單曲薄盒裝，加收《香奈兒》一曲。
  - 香港版：紅色英文側標，單曲薄盒裝；另有進口日本版，多一張紅色圓形貼標。
  - 台灣版：紅色英文側標，單曲薄盒裝，與港版側標略有不同；另有進口日本版，多一張綠色貼標。
  - 大陸版：紅色中文側標，普通CD盒裝，有封底。

## 曲目

1、**Separate Ways (Original Version)**

  -
    曲：；詞：；編曲：Envers

2、**Separate Ways (128 Beat Mix)**

  -
    曲：；詞：

3、**[Eyes On Me](../Page/Eyes_On_Me.md "wikilink")** featured in [Final
Fantasy VIII](../Page/最終幻想VIII.md "wikilink")

  -
    曲：[植松伸夫](../Page/植松伸夫.md "wikilink")；詞：染谷和美；編曲：浜口史郎

4、**香奈儿**

  -
    曲：[王菲](../Page/王菲.md "wikilink")；词：[林夕](../Page/林夕.md "wikilink")；编曲：[张亚东](../Page/张亚东.md "wikilink")
      -
        日本盤Bonus Track

5、**Separate Ways (Instrumental)**

  -
    曲：；編曲：Envers

[Category:王菲歌曲](../Category/王菲歌曲.md "wikilink")
[Category:2001年單曲](../Category/2001年單曲.md "wikilink")
[Category:EMI日本歌曲](../Category/EMI日本歌曲.md "wikilink")
[Category:富士火十劇主題曲](../Category/富士火十劇主題曲.md "wikilink")