**亞洲奧林匹克理事會**（，[縮寫](../Page/縮寫.md "wikilink")****），簡稱**亞洲奧會**或**亞奧理事會**，是[國際奧委會授權](../Page/國際奧委會.md "wikilink")，管理[亞洲地區的](../Page/亞洲.md "wikilink")[奧運會](../Page/奧運會.md "wikilink")、洲際賽事和國際體育賽事，並負責亞洲地區奧運的發展和將其精神發揚光大的機構，還負責解決亞洲國家、地區之間或與其他非亞洲國家、地區之間發生的體育糾紛。

亞洲奧林匹克理事會的宗旨是：促進亞洲體育運動的發展。亞奧理事會現擁有45個成員，其主辦的最大活動是每4年一屆的[亞洲運動會](../Page/亞洲運動會.md "wikilink")。為了提高亞洲人的體育水平及提供更多的較量機會，亞奧理事會除設立亞洲室內運動會，亦設立了[亞洲冬季運動會和](../Page/亞洲冬季運動會.md "wikilink")[亞洲沙灘運動會](../Page/亞洲沙灘運動會.md "wikilink")，使區內有更豐富的體育交流活動。

## 歷史

亞洲奧會的前身為1949年2月13日在[印度](../Page/印度.md "wikilink")[新德里成立的](../Page/新德里.md "wikilink")**亞洲業餘體育聯合會**，後來更名為****，總部設在[科威特](../Page/科威特.md "wikilink")。1981年11月26日和1982年11月16日，**亞洲運動會聯合會**先後舉行兩次會議，決定將亞洲運動會聯合會改名為**亞洲奧林匹克理事會**。

### 大事記

  - 1949年3月13日，亞洲運動會聯合會在印度新德里成立。
  - 1981年11月26日成立，亞洲運動會聯合會改為現名，總部設在科威特至今。
  - 1990年，[以色列奧林匹克委員會遭到阿拉伯國家的壓迫而離開亞洲奧委會](../Page/以色列奧林匹克委員會.md "wikilink")，轉為加入[歐洲奧林匹克委員會](../Page/歐洲奧林匹克委員會.md "wikilink")。
  - 1990年，亞洲奧林匹克理事會總部所在地科威特遭到[伊拉克入侵](../Page/海灣戰爭.md "wikilink")，總部暫時遷至英國，直至1991年美國進入科威特為止。
  - 1992年4月，接納中亞5國[哈薩克](../Page/哈薩克.md "wikilink")、[吉爾吉斯](../Page/吉爾吉斯.md "wikilink")、[塔吉克](../Page/塔吉克.md "wikilink")、[土庫曼和](../Page/土庫曼.md "wikilink")[烏茲別克為會員國](../Page/烏茲別克.md "wikilink")。

## 會徽

[ASIAN_GAMES_logo.png](https://zh.wikipedia.org/wiki/File:ASIAN_GAMES_logo.png "fig:ASIAN_GAMES_logo.png")

第一代會徽採用至[2006年亞洲運動會](../Page/2006年亞洲運動會.md "wikilink")。會徽中間的[太陽代表亞洲運動的發展及推廣](../Page/太陽.md "wikilink")。上面是奧運五環，而在下面寫上「Ever
Onward」；而在會徽下面則寫上「Olympic Council of Asia」的字樣。

第二代會徽2006年12月2日在多哈亞運會舉行期間公佈。新會徽中央是一輪太陽，太陽上面盤繞一條象徵東亞的[龍](../Page/中国龙.md "wikilink")，下面環繞一隻西亞和中亞的[鷹](../Page/鷹.md "wikilink")\[1\]，寓意全亞洲的團結。而在會徽的下面有國際奧委會的會徽及「Olympic
Council of
Asia」的字樣。新的會徽即日開始使用，但首次使用新會徽的運動會是[2007年亞洲室內運動會](../Page/2007年亞洲室內運動會.md "wikilink")\[2\]。12月底，在亞洲奧會的官方網頁上已經逐步更換新的會徽。

## 成員

**東亞地區（8）**

  -
  -
  -
  -
  -
  -
  -
  -
**東南亞地區（11）**

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
**南亞地區（8）**

  -
  -
  -
  -
  -
  -
  -
  -
**中亞地區（5）**

  -
  -
  -
  -
  -
**西亞地區（13）**

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
### 会员资格

1.  必须具有真正的独立自主性、能够抵制一切来自政治、经济、宗教方面的压力。
2.  拥有参加亚运会项目的全国或地区单项体育组织。
3.  拥有处理本国或地区一切有关亚运会事务的权力机构。

### 会员申请程序

1.  将申请报告递交主席
2.  主席审阅后提交执行委员会讨论
3.  执行委员会通过后交代表大会表决

<table>
<thead>
<tr class="header">
<th><p>國家</p></th>
<th><p><a href="../Page/國際奧委會國家編碼列表.md" title="wikilink">代碼</a></p></th>
<th><p><a href="../Page/國家奧林匹克委員會.md" title="wikilink">國家奧林匹克委員會</a></p></th>
<th><p>成立</p></th>
<th><p>Ref.</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>AFG</p></td>
<td><p><a href="../Page/阿富汗奥林匹克委员会.md" title="wikilink">阿富汗奥林匹克委员会</a></p></td>
<td><p>1935/1936</p></td>
<td><p>[3]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>BRN</p></td>
<td><p><a href="../Page/巴林奥林匹克委员会.md" title="wikilink">巴林奥林匹克委员会</a></p></td>
<td><p>1978/1979</p></td>
<td><p>[4]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>BAN</p></td>
<td><p><a href="../Page/孟加拉奥林匹克协会.md" title="wikilink">孟加拉奥林匹克协会</a></p></td>
<td><p>1979/1980</p></td>
<td><p>[5]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>BHU</p></td>
<td><p><a href="../Page/不丹奥林匹克委员会.md" title="wikilink">不丹奥林匹克委员会</a></p></td>
<td><p>1983</p></td>
<td><p>[6]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>BRU</p></td>
<td><p><a href="../Page/文莱奥林匹克委员会.md" title="wikilink">文莱奥林匹克委员会</a></p></td>
<td><p>1984</p></td>
<td><p>[7]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>CAM</p></td>
<td><p><a href="../Page/柬埔寨奥林匹克委员会.md" title="wikilink">柬埔寨奥林匹克委员会</a></p></td>
<td><p>1983/1994</p></td>
<td><p>[8]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>CHN</p></td>
<td><p><a href="../Page/中国奥林匹克委员会.md" title="wikilink">中国奥林匹克委员会</a></p></td>
<td><p>1910/1979</p></td>
<td><p>[9]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>HKG</p></td>
<td><p><a href="../Page/中国香港体育协会暨奥林匹克委员会.md" title="wikilink">中国香港体育协会暨奥林匹克委员会</a></p></td>
<td><p>1950/1951</p></td>
<td><p>[10]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>IND</p></td>
<td><p><a href="../Page/印度奥林匹克协会.md" title="wikilink">印度奥林匹克协会</a></p></td>
<td><p>1927</p></td>
<td><p>[11]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>INA</p></td>
<td><p><a href="../Page/印度尼西亚国家体育委员会.md" title="wikilink">印度尼西亚国家体育委员会</a></p></td>
<td><p>1946/1952</p></td>
<td><p>[12]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>IRI</p></td>
<td><p><a href="../Page/伊朗奥林匹克委员会.md" title="wikilink">伊朗奥林匹克委员会</a></p></td>
<td><p>1947</p></td>
<td><p>[13]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>IRQ</p></td>
<td><p><a href="../Page/伊拉克奥林匹克委员会.md" title="wikilink">伊拉克奥林匹克委员会</a></p></td>
<td><p>1948</p></td>
<td><p>[14]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>JPN</p></td>
<td><p><a href="../Page/日本奥林匹克委员会.md" title="wikilink">日本奥林匹克委员会</a></p></td>
<td><p>1911/1912</p></td>
<td><p>[15]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>JOR</p></td>
<td><p><a href="../Page/约旦奥林匹克委员会.md" title="wikilink">约旦奥林匹克委员会</a></p></td>
<td><p>1957/1963</p></td>
<td><p>[16]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>KAZ</p></td>
<td><p><a href="../Page/哈萨克斯坦共和国国家奥林匹克委员会.md" title="wikilink">哈萨克斯坦共和国国家奥林匹克委员会</a></p></td>
<td><p>1990/1993</p></td>
<td><p>[17]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>PRK</p></td>
<td><p><a href="../Page/朝鲜民主主义人民共和国奥林匹克委员会.md" title="wikilink">朝鲜民主主义人民共和国奥林匹克委员会</a></p></td>
<td><p>1953/1957</p></td>
<td><p>[18]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>KOR</p></td>
<td><p><a href="../Page/大韩体育会.md" title="wikilink">大韩体育会</a></p></td>
<td><p>1946/1947</p></td>
<td><p>[19]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>KUW</p></td>
<td><p><a href="../Page/科威特奥林匹克委员会.md" title="wikilink">科威特奥林匹克委员会</a></p></td>
<td><p>1957/1966</p></td>
<td><p>[20]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>KGZ</p></td>
<td><p><a href="../Page/吉尔吉斯斯坦奥林匹克委员会.md" title="wikilink">吉尔吉斯斯坦奥林匹克委员会</a></p></td>
<td><p>1991/1993</p></td>
<td><p>[21]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>LAO</p></td>
<td><p><a href="../Page/老挝奥林匹克委员会.md" title="wikilink">老挝奥林匹克委员会</a></p></td>
<td><p>1975/1979</p></td>
<td><p>[22]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>LIB</p></td>
<td><p><a href="../Page/黎巴嫩奥林匹克委员会.md" title="wikilink">黎巴嫩奥林匹克委员会</a></p></td>
<td><p>1947/1948</p></td>
<td><p>[23]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>MAC</p></td>
<td><p><a href="../Page/中国澳门体育暨奥林匹克委员会.md" title="wikilink">中国澳门体育暨奥林匹克委员会</a></p></td>
<td><p>1989</p></td>
<td><p>[24]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>MAS</p></td>
<td><p><a href="../Page/马来西亚奥林匹克委员会.md" title="wikilink">马来西亚奥林匹克委员会</a></p></td>
<td><p>1953/1954</p></td>
<td><p>[25]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>MDV</p></td>
<td><p><a href="../Page/马尔代夫奥林匹克委员会.md" title="wikilink">马尔代夫奥林匹克委员会</a></p></td>
<td><p>1985</p></td>
<td><p>[26]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>MGL</p></td>
<td><p><a href="../Page/蒙古奥林匹克委员会.md" title="wikilink">蒙古奥林匹克委员会</a></p></td>
<td><p>1956/1962</p></td>
<td><p>[27]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>MYA</p></td>
<td><p><a href="../Page/缅甸奥林匹克委员会.md" title="wikilink">缅甸奥林匹克委员会</a></p></td>
<td><p>1947</p></td>
<td><p>[28]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>NEP</p></td>
<td><p><a href="../Page/尼泊尔奥林匹克委员会.md" title="wikilink">尼泊尔奥林匹克委员会</a></p></td>
<td><p>1962/1963</p></td>
<td><p>[29]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>OMA</p></td>
<td><p><a href="../Page/阿曼奥林匹克委员会.md" title="wikilink">阿曼奥林匹克委员会</a></p></td>
<td><p>1982</p></td>
<td><p>[30]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>PAK</p></td>
<td><p><a href="../Page/巴基斯坦奥林匹克协会.md" title="wikilink">巴基斯坦奥林匹克协会</a></p></td>
<td><p>1948</p></td>
<td><p>[31]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>PLE</p></td>
<td><p><a href="../Page/巴勒斯坦奥林匹克委员会.md" title="wikilink">巴勒斯坦奥林匹克委员会</a></p></td>
<td><p>1931/1995</p></td>
<td><p>[32]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>PHI</p></td>
<td><p><a href="../Page/菲律宾奥林匹克委员会.md" title="wikilink">菲律宾奥林匹克委员会</a></p></td>
<td><p>1911/1929</p></td>
<td><p>[33]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>QAT</p></td>
<td><p><a href="../Page/卡塔尔奥林匹克委员会.md" title="wikilink">卡塔尔奥林匹克委员会</a></p></td>
<td><p>1979/1980</p></td>
<td><p>[34]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>KSA</p></td>
<td><p><a href="../Page/沙特阿拉伯奥林匹克委员会.md" title="wikilink">沙特阿拉伯奥林匹克委员会</a></p></td>
<td><p>1964/1965</p></td>
<td><p>[35]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>SIN</p></td>
<td><p><a href="../Page/新加坡奥林匹克理事会.md" title="wikilink">新加坡奥林匹克理事会</a></p></td>
<td><p>1947/1948</p></td>
<td><p>[36]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>SRI</p></td>
<td><p><a href="../Page/斯里兰卡奥林匹克委员会.md" title="wikilink">斯里兰卡奥林匹克委员会</a></p></td>
<td><p>1937</p></td>
<td><p>[37]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>SYR</p></td>
<td><p><a href="../Page/叙利亚奥林匹克委员会.md" title="wikilink">叙利亚奥林匹克委员会</a></p></td>
<td><p>1948</p></td>
<td><p>[38]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>TPE</p></td>
<td><p><a href="../Page/中華台北奥林匹克委员会.md" title="wikilink">中華台北奥林匹克委员会</a></p></td>
<td><p>1960</p></td>
<td><p>[39]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>TJK</p></td>
<td><p><a href="../Page/塔吉克斯坦奥林匹克委员会.md" title="wikilink">塔吉克斯坦奥林匹克委员会</a></p></td>
<td><p>1992/1993</p></td>
<td><p>[40]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>THA</p></td>
<td><p><a href="../Page/泰国奥林匹克委员会.md" title="wikilink">泰国奥林匹克委员会</a></p></td>
<td><p>1948/1950</p></td>
<td><p>[41]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>TLS</p></td>
<td><p><a href="../Page/东帝汶奥林匹克委员会.md" title="wikilink">东帝汶奥林匹克委员会</a></p></td>
<td><p>2003</p></td>
<td><p>[42]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>TKM</p></td>
<td><p><a href="../Page/土库曼奥林匹克委员会.md" title="wikilink">土库曼奥林匹克委员会</a></p></td>
<td><p>1990/1993</p></td>
<td><p>[43]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>UAE</p></td>
<td><p><a href="../Page/阿联酋奥林匹克委员会.md" title="wikilink">阿联酋奥林匹克委员会</a></p></td>
<td><p>1979/1980</p></td>
<td><p>[44]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>UZB</p></td>
<td><p><a href="../Page/乌兹别克奥林匹克委员会.md" title="wikilink">乌兹别克奥林匹克委员会</a></p></td>
<td><p>1992/1993</p></td>
<td><p>[45]</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>VIE</p></td>
<td><p><a href="../Page/越南奥林匹克委员会.md" title="wikilink">越南奥林匹克委员会</a></p></td>
<td><p>1976/1979</p></td>
<td><p>[46]</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>YEM</p></td>
<td><p><a href="../Page/也门奥林匹克委员会.md" title="wikilink">也门奥林匹克委员会</a></p></td>
<td><p>1971/1981</p></td>
<td><p>[47]</p></td>
</tr>
</tbody>
</table>

### 其他成员

<table>
<thead>
<tr class="header">
<th><p>國家</p></th>
<th><p><a href="../Page/國際奧委會國家編碼列表.md" title="wikilink">代碼</a></p></th>
<th><p><a href="../Page/國家奧林匹克委員會.md" title="wikilink">國家奧林匹克委員會</a></p></th>
<th><p>成立</p></th>
<th><p>Ref.</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>ISR</p></td>
<td><p><a href="../Page/以色列奥林匹克委员会.md" title="wikilink">以色列奥林匹克委员会</a></p></td>
<td><p>1933/1952</p></td>
<td><p>[48]</p></td>
</tr>
</tbody>
</table>

## 组成机构

  - 代表大会
  - 执行委员会（由主席、副主席、秘书长、司库和每个小组委员会的主席共23人组成）
  - 主席

### 歷屆主席

1.  [法赫德親王](../Page/法赫德親王.md "wikilink")（[科威特](../Page/科威特.md "wikilink")）
2.  [艾哈邁德·法赫德·薩巴赫](../Page/艾哈邁德·法赫德·薩巴赫.md "wikilink")（[科威特](../Page/科威特.md "wikilink")）

## 亞奧理事會執行委員會

<table>
<thead>
<tr class="header">
<th><p>職稱</p></th>
<th><p>姓名</p></th>
<th><p>國家</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>主席</strong></p></td>
<td><p><a href="../Page/Ahmad_Al-Fahad_Al-Sabah.md" title="wikilink">Ahmad Al-Fahad Al-Sabah</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>副主席</strong></p></td>
<td><p><a href="../Page/Charouck_Arirachakaran.md" title="wikilink">Charouck Arirachakaran</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/霍震霆.md" title="wikilink">霍震霆</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Syed_Arif_Hasan.md" title="wikilink">Syed Arif Hasan</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Isa_Bin_Rashed_Al_Khalifa.md" title="wikilink">Isa Bin Rashed Al Khalifa</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/帖木爾·庫里巴耶夫.md" title="wikilink">帖木爾·庫里巴耶夫</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/竹田恒和.md" title="wikilink">竹田恒和</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Hoàng_Vĩnh_Giang.md" title="wikilink">Hoàng Vĩnh Giang</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Sapardurdy_Toylyyev.md" title="wikilink">Sapardurdy Toylyyev</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/宋鲁增.md" title="wikilink">宋鲁增</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/于再清.md" title="wikilink">于再清</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Rita_Sri_Wahyusih_Subowo.md" title="wikilink">Rita Sri Wahyusih Subowo</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>執行委員</strong></p></td>
<td><p><a href="../Page/Haji_Surfi_Bolkiah.md" title="wikilink">Haji Surfi Bolkiah</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Mirabror_Usmanov.md" title="wikilink">Mirabror Usmanov</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Kioumars_Hashemi.md" title="wikilink">Kioumars Hashemi</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Raad_Hammoudi.md" title="wikilink">Raad Hammoudi Salman Al-Dulaimi</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>總幹事兼技術總監</strong></p></td>
<td><p><a href="../Page/Husain_A_H_Z_Al-Musallam.md" title="wikilink">Husain A H Z Al-Musallam</a></p></td>
<td></td>
</tr>
</tbody>
</table>

## 亞奧理事會榮譽委員

<table>
<thead>
<tr class="header">
<th><p>職稱</p></th>
<th><p>姓名</p></th>
<th><p>國家</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>終生榮譽副主席</strong></p></td>
<td><p><a href="../Page/Randhir_Singh_(sport_shooter).md" title="wikilink">Randhir Singh</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/魏纪中.md" title="wikilink">魏纪中</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>榮譽副主席</strong></p></td>
<td><p><a href="../Page/Hemasiri_Fernando.md" title="wikilink">Hemasiri Fernando</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>榮譽委員</strong></p></td>
<td><p><a href="../Page/张吉龙.md" title="wikilink">张吉龙</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Habib_Abdul_Nabi_Yousuf_Macki.md" title="wikilink">Habib Abdul Nabi Yousuf Macki</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Souhail_Khoury.md" title="wikilink">Souhail Khoury</a></p></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 亞奧理事會委員會

<table>
<thead>
<tr class="header">
<th><p>委員會</p></th>
<th><p>主席</p></th>
<th><p>國家</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong><a href="../Page/亞洲奧運理事會紀律委員會.md" title="wikilink">亞洲奧運理事會紀律委員會</a></strong></p></td>
<td><p><a href="../Page/Talal_Fahad_Ahmed_Al-Sabah.md" title="wikilink">Talal Fahad Ahmad Al-Sabah</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/亞洲奧運理事會女性及體育委員會.md" title="wikilink">亞洲奧運理事會女性及體育委員會</a></strong></p></td>
<td><p><a href="../Page/Natalya_Sipovich.md" title="wikilink">Natalya Sipovich</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/亞洲奧運理事會諮詢委員會.md" title="wikilink">亞洲奧運理事會諮詢委員會</a></strong></p></td>
<td><p><a href="../Page/Ng_Ser_Miang.md" title="wikilink">Ng Ser Miang</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/亞洲奧運理事會和平調解體育委員會.md" title="wikilink">亞洲奧運理事會和平調解體育委員會</a></strong></p></td>
<td><p><a href="../Page/費索·本·侯賽因王子.md" title="wikilink">費索·本·侯賽因王子</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/亞洲奧運理事會運動員委員會.md" title="wikilink">亞洲奧運理事會運動員委員會</a></strong></p></td>
<td><p><a href="../Page/室伏广治.md" title="wikilink">室伏广治</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/亞洲奧運理事會運動環境委員會.md" title="wikilink">亞洲奧運理事會運動環境委員會</a></strong></p></td>
<td><p><a href="../Page/柳京善.md" title="wikilink">柳京善</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/亞洲奧運理事會醫療委員會.md" title="wikilink">亞洲奧運理事會醫療委員會</a></strong></p></td>
<td><p><a href="../Page/Mani_Jegathesan.md" title="wikilink">Mani Jegathesan</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/亞洲奧運理事會全體運動委員會.md" title="wikilink">亞洲奧運理事會全體運動委員會</a></strong></p></td>
<td><p><a href="../Page/Mowaffak_Joumaa.md" title="wikilink">Mowaffak Joumaa</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/亞洲奧運理事會媒體委員會.md" title="wikilink">亞洲奧運理事會媒體委員會</a></strong></p></td>
<td><p><a href="../Page/卢景昭.md" title="wikilink">卢景昭</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/亞洲奧運理事會財務委員會.md" title="wikilink">亞洲奧運理事會財務委員會</a></strong></p></td>
<td><p><a href="../Page/陳國儀.md" title="wikilink">陳國儀</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/亞洲奧運理事會文化委員會.md" title="wikilink">亞洲奧運理事會文化委員會</a></strong></p></td>
<td><p><a href="../Page/Mohammad_A._Alkamali.md" title="wikilink">Mohammad A. Alkamali</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/亞洲奧運理事會陪同委員會.md" title="wikilink">亞洲奧運理事會陪同委員會</a></strong></p></td>
<td><p><a href="../Page/文大成.md" title="wikilink">文大成</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/亞洲奧運理事會教育委員會.md" title="wikilink">亞洲奧運理事會教育委員會</a></strong></p></td>
<td><p><a href="../Page/Abdullah_bin_Musa&#39;ed_bin_Abdulaziz_Al_Saud.md" title="wikilink">Abdullah bin Musa'ed bin Abdulaziz Al Saud王子</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/亞洲奧運理事會協調委員會.md" title="wikilink">亞洲奧運理事會協調委員會</a></strong></p></td>
<td><p><a href="../Page/竹田恒和.md" title="wikilink">竹田恒和</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/亞洲奧運理事會倫理委員會.md" title="wikilink">亞洲奧運理事會倫理委員會</a></strong></p></td>
<td><p><a href="../Page/魏纪中.md" title="wikilink">魏纪中</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/亞洲奧運理事會資訊及統計委員會.md" title="wikilink">亞洲奧運理事會資訊及統計委員會</a></strong></p></td>
<td><p><a href="../Page/Demchigjav_Zagdsuren.md" title="wikilink">Demchigjav Zagdsuren</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/亞洲奧運理事會國際關係委員會.md" title="wikilink">亞洲奧運理事會國際關係委員會</a></strong></p></td>
<td><p><a href="../Page/Jigyel_Ugyen_Wangchuck.md" title="wikilink">Jigyel Ugyen Wangchuck王子</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/亞洲奧運理事會體育委員會.md" title="wikilink">亞洲奧運理事會體育委員會</a></strong></p></td>
<td><p><a href="../Page/宋鲁增.md" title="wikilink">宋鲁增</a></p></td>
<td></td>
</tr>
</tbody>
</table>

## 官方刊物

  - [亚洲地平线](../Page/亚洲地平线.md "wikilink")

## 注釋

## 参考文献

## 外部連結

  - [亞洲奧會官方網站](http://www.ocasia.org)

## 参见

  - [非洲國家奧林匹克委員會聯合會](../Page/非洲國家奧林匹克委員會聯合會.md "wikilink")
  - [泛美體育組織](../Page/泛美體育組織.md "wikilink")
  - [歐洲奧林匹克委員會](../Page/歐洲奧林匹克委員會.md "wikilink")
  - [大洋洲國家奧林匹克委員會](../Page/大洋洲國家奧林匹克委員會.md "wikilink")
  - [亚洲奥林匹克理事会大会](../Page/亚洲奥林匹克理事会大会.md "wikilink")
  - [亚洲奥林匹克理事会执行委员会会议](../Page/亚洲奥林匹克理事会执行委员会会议.md "wikilink")

{{-}}

[Category:国家奥林匹克委员会协会](../Category/国家奥林匹克委员会协会.md "wikilink")
[Category:亞洲體育管理機構](../Category/亞洲體育管理機構.md "wikilink")
[Category:亚洲奥林匹克理事会](../Category/亚洲奥林匹克理事会.md "wikilink")

1.  [亞奧理事會新會徽發佈：中國巨龍在會徽上騰飛](http://news.xinhuanet.com/sports/2006-12/03/content_5425432.htm)
2.  [澳門日報在](../Page/澳門日報.md "wikilink")2006年12月5日的報導
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13.
14.
15.
16.
17.
18.
19.
20.
21.
22.
23.
24.
25.
26.
27.
28.
29.
30.
31.
32.
33.
34.
35.
36.
37.
38.
39.
40.
41.
42.
43.
44.
45.
46.
47.
48.