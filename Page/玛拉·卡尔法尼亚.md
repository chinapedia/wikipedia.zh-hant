**玛丽亚·罗萨里亚·卡尔法尼亚**（意大利语：Maria Rosaria "Mara"
Carfagna，），[意大利原主持人](../Page/意大利.md "wikilink")、模特，现为政治人物。

卡尔法尼亚出生于[萨莱诺](../Page/萨莱诺.md "wikilink")，1997年曾参加[意大利小姐评选](../Page/:en:Miss_Italy.md "wikilink")，获得第六名。2001年，卡尔法尼亚毕业于[萨莱诺大学](../Page/:en:University_of_Salerno.md "wikilink")，获得[法律学位](../Page/法律.md "wikilink")。她曾工作于[西尔维奥·贝卢斯科尼旗下的Mediaset电视台](../Page/西尔维奥·贝卢斯科尼.md "wikilink")，担任主持人、模特兒。

贝卢斯科尼曾当众表示，如果没有结婚，就会追求卡尔法尼亚。2006年，卡尔法尼亚当选议员，2008年5月8日，[贝卢斯科尼任命其为公平机会部部长](../Page/贝卢斯科尼.md "wikilink")。有报道称其为“世界上最美丽的部长”。

[Category:義大利眾議員](../Category/義大利眾議員.md "wikilink")
[Category:意大利女性政府部长](../Category/意大利女性政府部长.md "wikilink")
[C](../Category/意大利罗马天主教徒.md "wikilink")
[Category:坎帕尼亚大区人](../Category/坎帕尼亚大区人.md "wikilink")