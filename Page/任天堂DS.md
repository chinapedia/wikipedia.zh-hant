是[日本電玩遊戲商任天堂公司開發的第三代掌上遊樂器](../Page/日本.md "wikilink")。DS是Dual
Screen（雙螢幕）的縮寫。主要的特徵包括雙螢幕顯示，其中下方的螢幕為[觸控式螢幕](../Page/觸控式螢幕.md "wikilink")；並配備有[麥克風聲音輸入裝置和](../Page/麥克風.md "wikilink")[Wi-Fi](../Page/Wi-Fi.md "wikilink")[無線網路功能](../Page/無線網路.md "wikilink")。

任天堂DS在[北美地區于](../Page/北美.md "wikilink")2004年11月21日發售，12月2日日本上市。[台灣於](../Page/台灣.md "wikilink")12月13日正式發售。\[1\]而[歐洲在](../Page/歐洲.md "wikilink")2005年3月11日發售。[中国大陆由](../Page/中国大陆.md "wikilink")[神遊科技使用](../Page/神遊科技.md "wikilink")「iQue
DS（iDS）」的名稱推出，於2005年7月23日發售。

任天堂DS推出过三款改良机型，依时间顺序分别為[任天堂DS
Lite](../Page/任天堂DS_Lite.md "wikilink")、[任天堂DSi](../Page/任天堂DSi.md "wikilink")、[任天堂DSi
LL](../Page/任天堂DSi_LL.md "wikilink")。

在2006年7月底，任天堂正式宣佈DS主機的日本銷量突破1000萬大關，為日本[電玩史上銷售最快的主機](../Page/電玩.md "wikilink")。截至2009年3月6日NDS主机在全球的累计销量突破1亿台。2004年11月21日问世的NDS主机用时4年3个月零2周的时间达到了1亿台的销售成绩，创造了家用游戏机史上的最快纪录。
截止2011年11月5日世界累计销量1.5亿台，即一星期平均銷售約40萬部。\[2\]打破同公司生產的[GameBoy](../Page/GameBoy.md "wikilink")（約1.18億部）的銷量紀錄，成為全世界最高銷量的掌上型遊戲機。

任天堂DS的下一代掌机为[任天堂3DS](../Page/任天堂3DS.md "wikilink")。

## 特性

### 雙螢幕

任天堂DS有上下兩個包含背光的3英吋[液晶螢幕](../Page/液晶.md "wikilink")，可顯示26萬色。以往許多遊戲中必須要透過畫面切換才能看到的訊息，現在可以使用第二個螢幕來看。

### 觸控式螢幕

觸控式螢幕為任天堂DS最大的特點之一。使用下方的觸控式螢幕和附帶的觸控筆，玩家可以靈活自如地控制遊戲進行。而且觸控式螢幕引入了許多以前通過按鍵不能實現的遊戲方式。

### 聲音識別

通過機身內的麥克風，玩家可以通過聲音來操縱遊戲。

### 網路通信

主機支援Wi-Fi通訊協定，可以透過任天堂公司的無線網路服務「Nintendo Wi-Fi Connection」與世界各地的玩家一起進行遊戲。

### 無線通信

任天堂DS主機內建的無線通信功能，可以將多部任天堂DS主機直接連線進行遊戲。分為「Wireless
Play（無線通信遊玩）」和「Download Play（下載遊玩）」兩種方式。

「Wireless
Play（無線通信遊玩）」需要準備與人數相同的NDS主機和遊戲軟體，最大人數無上限，依照遊戲軟體而有不同，例如「大合奏\!バンドブラザーズ（大合奏）」遊戲可支援無上限的同時遊玩的人數。

「Download Play（下載遊玩）」只需要準備一份遊戲軟體，其他的NDS使用者就能夠從裝有遊戲軟體的NDS主機上下載遊戲，並進行連線遊玩。

因為 Game Boy Advance 通信連線接頭在任天堂DS上被取消的關係，Game Boy Advance的遊戲軟體在任天堂DS
上無法連線對戰。

### 雙卡帶插槽

機身上同時配備有任天堂DS和Game Boy Advance的遊戲卡帶插槽。

## 技術規格

  - [CPU](../Page/CPU.md "wikilink")：ARM946E-S 67MHz + ARM7TDMI 33MHz
  - [記憶體](../Page/記憶體.md "wikilink")：4MB（中國大陆iQue
    DS具備[簡體中文操作界面](../Page/簡體中文.md "wikilink")，內存擴大至10MB）
  - [VRAM](../Page/VRAM.md "wikilink")：656KB
  - [顯示器](../Page/顯示器.md "wikilink")：3英吋（對角線）附有背光的[TFT彩色](../Page/TFT.md "wikilink")[液晶顯示器](../Page/液晶顯示器.md "wikilink")
      - 解析度256x192，顏色數26萬色
  - [ROM](../Page/ROM.md "wikilink")：日本[MegaChips公司研發的特殊規格記憶體卡匣](../Page/MegaChips.md "wikilink")，最大支持8Gbit（1024MB）以上
  - 三維性能：每秒十二萬個多邊形
  - 二維性能：每秒三千萬點
  - 輸入：十字鍵，A，B，L，R，X，Y，START，SELECT，觸控式螢幕，麥克風
  - 網路功能：[IEEE
    802.11b](../Page/IEEE_802.11#IEEE_802.11b.md "wikilink")(DS
    Lite採用[IEEE
    802.11g](../Page/IEEE_802.11#IEEE_802.11g.md "wikilink"))（Wi-Fi）
  - 電源：內置充電式鋰[電池](../Page/電池.md "wikilink")（3.7V/850mAh）
  - [聲音](../Page/聲音.md "wikilink")：[立體聲喇叭](../Page/立體聲.md "wikilink")
  - 重量 : 約275克（NDS Lite約218克）

## 主機顏色

NDS 有六種主機顏色可供選擇，分別是：

  - 鉑銀(Platinum Silver)：2004年12月2日發售
  - 墨黑(Graphite Black)：2005年3月24日發售
  - 純白(Pure White)：2005年3月24日發售
  - 海藍(Turquoise Blue)：2005年4月21日發售
  - 甜蜜粉紅(Candy Pink)：2005年4月21日發售
  - 紅(Red)：2005年8月8日發售

## 改良机型

### 任天堂DS Lite

任天堂DS Lite（Lite
即比舊版的任天堂DS更輕巧和更光亮的意思）已於2006年3月2日推出，比起舊版的任天堂DS主機體積和重量上都更加縮小，推出之際在日本造成一股搶購熱潮。

### 任天堂DSi

任天堂於2008年10月2日任天堂戰略發表會上公佈的新款NDS系列主機。 NDSi是NDS家族繼NDS Lite後最新的改版主機，改變之處有：

1.  取消GBA卡匣插槽，也不支援GB/GBA向下相容
2.  主機厚度比起NDSL薄2.6mm，重量減輕12%
3.  上下螢幕皆擴大為3.25吋 LCD螢幕
4.  內建兩個30萬畫素相機鏡頭，分別放置於轉軸與上蓋外側，拍攝的照片可直接傳到Wii照片頻道觀看
5.  增加音樂播放功能，可自由調整播放速度，支援格式為AAC(延伸副檔名為：mp4、m4a)
6.  新增SD記憶卡插槽，並可以支援SDHC規格
7.  主機內部增加Flash記憶體，並開放DSi
    Ware服務，與WiiWare一樣可上網下載購買最新遊戲，沿用原本Wii點數，且Wii點數改名為任天堂點數
8.  內建應用程式，類似Wii頻道設計，並內建[Opera瀏覽器](../Page/Opera.md "wikilink")
9.  2010年3月之前登記DSi即可得到1000點任天堂點數

於2008年11月1日在日本發售，第一波為黑、白兩色，定價18900日元。

自任天堂DSi起开始加入强制锁区功能，并一直延伸至任天堂3DS。\[3\]

### 任天堂DSi LL/XL

[任天堂於](../Page/任天堂.md "wikilink")2009年11月21日在日本開始發售任天堂DSi LL/XL。 任天堂DSi
LL/XL是[NDSi的改良版](../Page/NDSi.md "wikilink")，主要變更為︰

1.  上下[螢幕皆擴大為](../Page/螢幕.md "wikilink")4.2[吋](../Page/吋.md "wikilink")[LCD螢幕](../Page/LCD.md "wikilink")
2.  [電池續航力提昇](../Page/電池.md "wikilink")
3.  機身隨螢幕變大，重量增加約100[公克](../Page/公克.md "wikilink")
4.  介面系統變更

## 參考資料

## 外部連結

  - [Nintendo DS](http://www.nintendo.co.jp/ds/)（任天堂公司網站內的NDS網頁）
  - [iQue
    DS](https://web.archive.org/web/20160310062113/http://ds.ique.com/DSi/DS_Series.htm)（神遊科技iDS系列官方網頁）
  - [Nintendo Wi-Fi](http://wifi.nintendo.co.jp/)（任天堂Wi-Fi無線網路服務網頁）
  - [Nintendo Inside](http://www.nintendo-inside.jp/)（日本任天堂新聞）

[Category:掌上遊戲機](../Category/掌上遊戲機.md "wikilink")
[任天堂DS](../Category/任天堂DS.md "wikilink")
[Category:任天堂遊戲機](../Category/任天堂遊戲機.md "wikilink")
[Category:第七世代遊戲機](../Category/第七世代遊戲機.md "wikilink")
[Category:2000年代玩具](../Category/2000年代玩具.md "wikilink")
[Category:2004年面世的產品](../Category/2004年面世的產品.md "wikilink")

1.  [NDS 台灣專用機 12 月 13
    日推出](http://gnn.gamer.com.tw/4/17934.html).巴哈姆特.2004-12-01.
2.  <http://www.vgchartz.com/>
3.  <http://psp.tgbus.com/news/etc/201204/20120425165345.shtml>