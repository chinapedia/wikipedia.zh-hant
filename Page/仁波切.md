\-{zh-hans:**仁波切**;zh-hk:**寧波車**或**仁寶哲**;zh-tw:**仁波切**}-（），是對[藏传佛教](../Page/藏传佛教.md "wikilink")[上師的一种尊称](../Page/上師.md "wikilink")。

## 詞語釋義

仁波切源于[藏语](../Page/藏语.md "wikilink")，原義是“珍宝”，意義與[和尚](../Page/和尚.md "wikilink")、[阿闍黎](../Page/阿闍黎.md "wikilink")、[喇嘛](../Page/喇嘛.md "wikilink")、高[僧相近](../Page/僧.md "wikilink")。-{zh-hans:在[港](../Page/香港.md "wikilink")[澳译作](../Page/澳門.md "wikilink")**宁波车**或**仁宝哲**;zh-tw:在[港](../Page/香港.md "wikilink")[澳譯作](../Page/澳門.md "wikilink")**寧波車**或**仁寶哲**;zh-hk:在[中國大陸和](../Page/中國大陸.md "wikilink")[台灣譯作](../Page/台灣.md "wikilink")**仁波切**}-。在汉语中仁波切一词的用法并不普遍；尽管在意义上仁波切与[上師和](../Page/上師.md "wikilink")[活佛并不完全相同](../Page/活佛.md "wikilink")，但在汉语中大众常用上師和活佛来称呼仁波切。

僧人在三種情形下會被稱作仁波切，一是轉世高僧（即[活佛](../Page/活佛.md "wikilink")）并被認證；二是學問堪為世人楷模者；三是此世有很高修行的成就者。也就是說轉世高僧一定是仁波切，但被稱為仁波切的人不一定是轉世高僧。

在[西藏與](../Page/西藏.md "wikilink")[不丹](../Page/不丹.md "wikilink")，此詞單獨使用時，亦指[蓮花生](../Page/蓮花生.md "wikilink")。

这个称号并不一定用作称呼人物，有些物也称作“仁波切”。例如，西藏[大昭寺的释迦牟尼像也被称为](../Page/大昭寺.md "wikilink")“觉窝仁宝哲”（Jowo
Rinpoche）。\[1\]
又如，[西藏三大神山之首](../Page/西藏.md "wikilink")[冈底斯山主峰被称为](../Page/冈底斯山.md "wikilink")[冈仁波钦](../Page/冈仁波钦.md "wikilink")，既“雪山珍宝”之意。

## 具备仁波切的条件

1.  须有正统传承的根本金刚上师之密法[灌顶](../Page/灌顶.md "wikilink")。
2.  从[金刚持至自己的根本上师](../Page/金刚萨埵.md "wikilink")，其间所有密法之传承灌顶皆须圆满，不可间断。
3.  须受[阿闍黎灌顶](../Page/阿闍黎.md "wikilink")，精通显密[佛法及](../Page/佛法.md "wikilink")[菩提心学與](../Page/菩提心学.md "wikilink")[怛特羅](../Page/怛特羅.md "wikilink")，并具备[火供等修法材料](../Page/火坛仪式.md "wikilink")，熟悉所有经文仪规中说的修行方法。
4.  自己有能力传授密法时，须经根本金刚[上师许可](../Page/上师.md "wikilink")，方可對人传法灌顶。

## 冒充现象

两岸三地娱乐圈和商界的部分人物信仰[藏傳佛教](../Page/藏傳佛教.md "wikilink")，且一般都拜有[上师](../Page/上师.md "wikilink")。\[2\]\[3\]受此风气影响，中国大陆的中产阶级也开始推崇[藏传佛教](../Page/藏传佛教.md "wikilink")，以寻求心灵上的抚慰，但这种现象有时被视为自我标榜身份的工具\[4\]，而非纯粹信仰。\[5\]对活佛的迷信导致“仁波切”被讹传为活佛的代名词\[6\]，也导致了假冒活佛的出现，这些假冒活佛并未完整地看过一本[佛经](../Page/佛经.md "wikilink")，很少遵守[佛教戒律](../Page/佛教戒律.md "wikilink")，但往往自称“仁波切”且有一定数目的信徒，并与弟子存在供养关系，被网民调侃为“**散养仁波切**”或是“[朝阳区仁波切](../Page/朝阳区_\(北京市\).md "wikilink")”（因[北京市朝阳区为](../Page/北京市.md "wikilink")[中产阶级聚居区](../Page/中产阶级.md "wikilink")）\[7\]\[8\]。这些冒牌活佛中不乏假借宗教之名骗财骗色者。\[9\]

## 相關条目

  - [藏傳佛教](../Page/藏傳佛教.md "wikilink")
  - [祖古](../Page/祖古.md "wikilink")
  - [活佛](../Page/活佛.md "wikilink")

## 参见

  - [佛教](../Page/佛教.md "wikilink")
  - [祈竹仁波切](../Page/祈竹仁寶哲.md "wikilink")
  - [夏壩仁波切](../Page/夏壩仁波切.md "wikilink")
  - [清定](../Page/清定.md "wikilink")[上师](../Page/上师.md "wikilink")
  - [能海](../Page/能海.md "wikilink")[上师](../Page/上师.md "wikilink")
  - [仁清](../Page/仁清.md "wikilink")[上师](../Page/上师.md "wikilink")

## 参考文献

[Category:藏传佛教称谓](../Category/藏传佛教称谓.md "wikilink")
[Category:仁波切](../Category/仁波切.md "wikilink")

1.  [狮吼棒喝--大藏寺祈竹仁波切问答选录](http://khejokrinpoche.blogspot.com/2011/08/blog-post_8297.html)

2.

3.

4.
5.

6.

7.
8.

9.