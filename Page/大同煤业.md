**大同煤業股份有限公司**是一間中國上市煤炭公司。它是[中華人民共和國第三大](../Page/中華人民共和國.md "wikilink")煤炭[國有企業](../Page/國有企業.md "wikilink")[大同煤礦集團的](../Page/大同煤礦集團.md "wikilink")[子公司](../Page/子公司.md "wikilink")，主要從事[煤炭產品的](../Page/煤炭.md "wikilink")[採掘](../Page/採掘.md "wikilink")、[加工和](../Page/加工.md "wikilink")[銷售](../Page/銷售.md "wikilink")，相關業務包括[機械](../Page/機械.md "wikilink")[製造及](../Page/製造.md "wikilink")[維修](../Page/維修.md "wikilink")、[鐵路](../Page/鐵路.md "wikilink")[運輸及](../Page/運輸.md "wikilink")[維修等](../Page/維修.md "wikilink")\[1\]。

大同煤業是由[大同煤礦集團](../Page/大同煤礦集團.md "wikilink")、[中國中煤能源集團](../Page/中國中煤能源集團.md "wikilink")、[秦皇島港務集團](../Page/秦皇島港務集團.md "wikilink")、[中國華能集團](../Page/中國華能集團.md "wikilink")、[上海宝钢国际经济贸易](../Page/上海寶鋼集團.md "wikilink")、大同同鐵實業發展集團、[煤炭科學研究總院](../Page/煤炭科学技术研究院.md "wikilink")\[2\]、大同市地方煤炭集團，共八家發起人共同參與，在2001年7月25日成立\[3\]

大同煤業在2006年於[上海證券交易所上市](../Page/上海證券交易所.md "wikilink")。

## 主要股東

\[4\]

1.  [大同煤矿集团](../Page/大同煤矿集团.md "wikilink")（57.46%）
2.  [中央汇金](../Page/中央汇金.md "wikilink") （1.91%）
3.  [河北港口集团](../Page/河北港口集团.md "wikilink") （1.59%；前秦皇岛港务集团）

### 2006年

來源\[5\]：

1.  [大同煤礦集團](../Page/大同煤礦集團.md "wikilink") （90.89%）
2.  [中國中煤能源集團](../Page/中國中煤能源集團.md "wikilink") （3.14%）
3.  [秦皇島港務集團](../Page/秦皇島港務集團.md "wikilink") （2.51%）
4.  [中國華能集團](../Page/中國華能集團.md "wikilink") （1.26%）
5.  [上海宝钢国际经济贸易](../Page/上海寶鋼集團.md "wikilink") （0.63%）
6.  大同同鐵實業發展集團 （0.63%）
7.  [煤炭科學研究總院](../Page/煤炭科学技术研究院.md "wikilink") （0.63%）
8.  大同市地方煤炭集團 （0.31%）

## 參考

## 連結

  -
[category:煤炭公司](../Page/category:煤炭公司.md "wikilink")
[category:中煤能源集團](../Page/category:中煤能源集團.md "wikilink")

[Category:总部在大同的中华人民共和国国有公司](../Category/总部在大同的中华人民共和国国有公司.md "wikilink")

1.  [大同煤業簡介](http://money.finance.sina.com.cn/corp/go.php/vCI_CorpInfo/stockid/601001.html)
    新浪

2.  [煤科院概况](http://www.ccri.com.cn/mkygk.jhtml) 煤炭科学技术研究院

3.

4.

5.