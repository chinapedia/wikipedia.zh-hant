**鼎金系統交流道**，座落於[中華民國](../Page/中華民國.md "wikilink")（[台灣](../Page/台灣.md "wikilink")）[高雄市](../Page/高雄市.md "wikilink")[三民區](../Page/三民區.md "wikilink")[覆鼎金與](../Page/覆鼎金.md "wikilink")[仁武區交界](../Page/仁武區.md "wikilink")，為全臺灣複雜程度第二的交流道（僅次於[五股交流道](../Page/五股交流道.md "wikilink")）。位於[國道一號指標](../Page/中山高速公路.md "wikilink")362.4公里；[國道十號指標](../Page/國道十號_\(中華民國\).md "wikilink")1.9公里處，於1999年11月14日啟用。

[交流道型式為雙葉型](../Page/交流道.md "wikilink")，國道一號左轉國道十號均以環道、起伏高架設計連接。2015年6月17日，南下往東向的出口分支：鼎力路匝道通車啟用，正式成為此交流道的一部分。

<table>
<thead>
<tr class="header">
<th><p><a href="https://zh.wikipedia.org/wiki/File:TWHW1.svg" title="fig:TWHW1.svg">TWHW1.svg</a></p></th>
<th></th>
<th><p>南下：<a href="https://zh.wikipedia.org/wiki/File:TWHW3.svg" title="fig:TWHW3.svg">TWHW3.svg</a> <a href="https://zh.wikipedia.org/wiki/File:TWHW10.svg" title="fig:TWHW10.svg">TWHW10.svg</a><strong>旗山</strong> <a href="https://zh.wikipedia.org/wiki/File:TWHW10.svg" title="fig:TWHW10.svg">TWHW10.svg</a><strong>左營</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>南下：<strong>鼎力路</strong> <strong>民族一路</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>北上：<a href="https://zh.wikipedia.org/wiki/File:TWHW10.svg" title="fig:TWHW10.svg">TWHW10.svg</a><strong>左營</strong> <strong></strong> <a href="https://zh.wikipedia.org/wiki/File:TWHW10.svg" title="fig:TWHW10.svg">TWHW10.svg</a><strong>仁武</strong> <strong>旗山</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:TWHW10.svg" title="fig:TWHW10.svg">TWHW10.svg</a></p></td>
<td></td>
<td><p>東向：<strong>高雄</strong></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>西向：<strong>民族一路</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>西向：<strong>高雄</strong> <strong>台南</strong></p></td>
<td></td>
</tr>
</tbody>
</table>

## 鼎力路匝道

**鼎力路匝道**為一座僅設南下出口的交流道，位於[台灣](../Page/台灣.md "wikilink")[國道一號](../Page/國道一號.md "wikilink")[中山高速公路](../Page/中山高速公路.md "wikilink")362K處，由鼎金系統交流道南下接[國道十號東向的出口匝道分出](../Page/國道十號_\(中華民國\).md "wikilink")，連結平面[鼎力路](../Page/鼎力路.md "wikilink")、[仁雄路](../Page/仁雄路.md "wikilink")。以改善[仁武區](../Page/仁武區.md "wikilink")、[三民區與](../Page/三民區.md "wikilink")[國道一號](../Page/國道一號.md "wikilink")、[國道十號相接及平面道路車流現況](../Page/國道十號.md "wikilink")，於2014年動工\[1\]。至2015年4月底完工，同年6月17日正式開放通車\[2\]。

## 爭議事件

  - 2015年6月17日：鼎力路匝道通車典禮上[國民黨](../Page/國民黨.md "wikilink")[立委](../Page/立委.md "wikilink")[黃昭順帶著拿手持抗議布條支持者佔據舞臺](../Page/黃昭順.md "wikilink")，指控[民進黨割稻仔尾搶功](../Page/民進黨.md "wikilink")\[3\]。

## 鄰近交流道

|-

## 後續計畫

  - 增設國道10號東向銜接國道1號北上匝道工程。
  - 民族一路出口匝道獨立成民族交流道，並新建西入東出匝道。

## 參考文獻

4\.

## 外部連結

  - [國道一號鼎金系統交流道動線圖](http://www.freeway.gov.tw/content/Jpg%B9%CF%C0%C9/%B0%EA1%A5%E6%ACy%B9D/01-60-%B9%A9%AA%F7%A8t%B2%CE.jpg)（國道高速公路局）
  - [國道十號鼎金系統交流道動線圖](http://www.freeway.gov.tw/content/Jpg%B9%CF%C0%C9/%B0%EA10%A5%E6%ACy%B9D/10-01-%A5%AA%C0%E7%BA%DD-02-%B9%A9%AA%F7%A8t%B2%CE.jpg)（國道高速公路局）

[系](../Category/高雄市交流道.md "wikilink")
[Category:仁武區](../Category/仁武區.md "wikilink")
[Category:三民區](../Category/三民區.md "wikilink")

1.
2.
3.