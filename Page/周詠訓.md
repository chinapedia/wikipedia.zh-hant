**阿布**（），本名**周詠訓**，[台灣](../Page/台灣.md "wikilink")[節目主持人](../Page/節目主持人.md "wikilink")、[男演員](../Page/男演員.md "wikilink")。以主持旅遊節目《[冒險王](../Page/冒險王_\(旅遊節目\).md "wikilink")》聞名。並於2007年以此節目獲得[第42屆金鐘獎綜合節目主持人獎](../Page/第42屆金鐘獎.md "wikilink")。曾任[東森財經新聞台](../Page/東森財經新聞台.md "wikilink")《夢想街57號-預約你的夢想》節目主持人。

## 主持

| 年份 | 播出頻道                                   | 節目                                            | 備註     |
| -- | -------------------------------------- | --------------------------------------------- | ------ |
|    | [三立都會台](../Page/三立都會台.md "wikilink")   | [中國那麼大](../Page/中國那麼大.md "wikilink")          | 代班     |
|    | [三立都會台](../Page/三立都會台.md "wikilink")   | [冒險王](../Page/冒險王_\(旅遊節目\).md "wikilink")     | 第三代主持人 |
|    | [中天娛樂台](../Page/中天娛樂台.md "wikilink")   | [娘娘駕到](../Page/娘娘駕到.md "wikilink")            |        |
|    | [新加坡U頻道](../Page/新加坡U頻道.md "wikilink") | [明星志工隊](../Page/明星志工隊.md "wikilink")          |        |
|    | [廣州衛視](../Page/廣州衛視.md "wikilink")     | [大珠三角新知勢](../Page/大珠三角新知勢.md "wikilink")      |        |
|    | [東森綜合台](../Page/東森綜合台.md "wikilink")   | [跟著偶像去旅行](../Page/跟著偶像去旅行.md "wikilink")      |        |
|    | [八大電視台](../Page/八大電視台.md "wikilink")   | [世界第一等](../Page/世界第一等.md "wikilink")          |        |
|    | [央視](../Page/央視.md "wikilink")         | 第七屆[海峽兩岸知識大賽](../Page/海峽兩岸知識大賽.md "wikilink") | 外景主持人  |
|    | [央視](../Page/央視.md "wikilink")         | 第八屆[海峽兩岸知識大賽](../Page/海峽兩岸知識大賽.md "wikilink") | 外景主持人  |
|    |                                        |                                               |        |

## 演出作品

### 電視劇

| 年份    | 播出頻道                                 | 劇名                                               | 飾演      |
| ----- | ------------------------------------ | ------------------------------------------------ | ------- |
| 2003年 | [中視](../Page/中視.md "wikilink")       | [熟女慾望日記](../Page/熟女慾望日記.md "wikilink")           | 大雄      |
| 2009年 | [大愛](../Page/大愛.md "wikilink")       | [千江有水千江月](../Page/千江有水千江月_\(電視劇\).md "wikilink") | 黃勝璧     |
| 2009年 | [華視](../Page/華視.md "wikilink")       | [開封有個包青天](../Page/開封有個包青天.md "wikilink")         | 周志偉     |
| 2009年 | [客家電視台](../Page/客家電視台.md "wikilink") | [十里桂花香](../Page/十里桂花香.md "wikilink")             | 少年-楊秉彝  |
| 2014年 | [公視](../Page/公視.md "wikilink")/中視    | [門當父不對](../Page/門當父不對_\(電視劇\).md "wikilink")     | 陳廷鋒     |
| 2014年 | [大愛](../Page/大愛.md "wikilink")       | [心語](../Page/心語.md "wikilink")                   | 蔡國喜     |
| 2015年 | [華視](../Page/華視.md "wikilink")       | [愛你沒條件](../Page/愛你沒條件.md "wikilink")             | 邱明輝     |
| 2015年 | [大愛](../Page/大愛.md "wikilink")       | [如果還有明天](../Page/如果還有明天.md "wikilink")           | 江俊廷     |
| 2017年 | [大愛](../Page/大愛.md "wikilink")       | [奔跑吧！阿飛](../Page/奔跑吧！阿飛.md "wikilink")           | 楊嘉雄     |
| 2018年 | [民視](../Page/民視.md "wikilink")       | [實習醫師鬥格](../Page/實習醫師鬥格.md "wikilink")           | 郭-{于}-華 |
| 2019年 | [公視](../Page/公視.md "wikilink")       | [我們與惡的距離](../Page/我們與惡的距離.md "wikilink")         | 金鼎凱     |

### 廣告

  - [仙境傳說肩上波利篇](../Page/仙境傳說.md "wikilink")
  - [OKwap手機](../Page/OKwap.md "wikilink")
  - [桂格三合一麥片](../Page/桂格.md "wikilink")
  - 澎澎Man激COOL系列

## 慈善

  - 紅心字會向日葵愛心大使
  - 菲律賓旅遊親善大使

## 獲獎

|- |2007年 |《冒險王》 |[第42屆金鐘獎](../Page/第42屆金鐘獎.md "wikilink") 綜合節目主持人獎 | |-
|}

## 參考資料

## 外部連結

  -
  -
  -
[Category:台灣電視主持人](../Category/台灣電視主持人.md "wikilink")
[Category:臺灣電視男演員](../Category/臺灣電視男演員.md "wikilink")
[Category:金鐘獎綜合節目主持人獲得者](../Category/金鐘獎綜合節目主持人獲得者.md "wikilink")