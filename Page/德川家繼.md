[Tokugawa_ietsugu.jpg](https://zh.wikipedia.org/wiki/File:Tokugawa_ietsugu.jpg "fig:Tokugawa_ietsugu.jpg")

**德川家繼**（1709年7月3日—1716年4月30日），幼名鍋松丸，[江戶幕府第七代將軍](../Page/江戶幕府.md "wikilink")。六代將軍家宣長子。母側室[月光院](../Page/月光院.md "wikilink")（阿喜世之方），五歲繼任將軍，八歲便過世，是德川家歷代將軍中最年幼也最早夭的一位。

因年幼繼位故大權旁落，夭折後將軍位由中興幕府的明君[德川吉宗繼任](../Page/德川吉宗.md "wikilink")。

[Ietsugu](../Category/德川幕府将军.md "wikilink")
[Category:1709年出生](../Category/1709年出生.md "wikilink")
[Category:1716年逝世](../Category/1716年逝世.md "wikilink")