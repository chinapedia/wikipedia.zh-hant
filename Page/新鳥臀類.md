**新鳥臀類**（Neornithischia）是[鳥臀目](../Page/鳥臀目.md "wikilink")[恐龍的一個](../Page/恐龍.md "wikilink")[演化支](../Page/演化支.md "wikilink")，屬於[頜齒類](../Page/頜齒類.md "wikilink")，是[裝甲亞目的姊妹](../Page/裝甲亞目.md "wikilink")[分類單元](../Page/分類單元.md "wikilink")。新鳥臀類的特徵是下頜牙齒有一層不對稱的厚琺瑯質。牠們的牙齒咀嚼時磨損不平均，並發展出銳利的脊狀，可讓角足亞目比其他鳥臀目恐龍切碎更堅硬的植物。

## 分類

在1985年，新鳥臀類這個名稱首次被提出，範圍包含：所有頜齒類恐龍裡，親緣關係接近於[沃克氏副櫛龍](../Page/副櫛龍.md "wikilink")，而離[大面甲龍](../Page/甲龍.md "wikilink")、[狹臉劍龍較遠的所有物種](../Page/劍龍.md "wikilink")\[1\]。

以下[演化樹來自於](../Page/演化樹.md "wikilink")[理察·巴特勒](../Page/理察·巴特勒.md "wikilink")（Richard
J. Butler）、金利勇等人的2011年研究\[2\]：

## 參考資料

<div class="references-small">

<references>

</references>

  -
  -

</div>

## 外部連結

  - [Encyclopædia
    Britannica](http://www.britannica.com/eb/article-225959)
  - [Palæos](https://web.archive.org/web/20060313160201/http://www.palaeos.com/Vertebrates/Units/320Ornithischia/500.html)

[\*](../Category/鳥臀目.md "wikilink")

1.
2.