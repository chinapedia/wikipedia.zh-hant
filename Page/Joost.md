**Joost**（与'juiced'[果汁谐音](../Page/果汁.md "wikilink")）是一个交互式軟體共享电视节目和其他形式的視訊在网上使用点对点电视技术，出自
[KaZaA](../Page/KaZaA.md "wikilink")、[Skype](../Page/Skype.md "wikilink")
开发人员之手，自从问世以来就引来无数关注。

出售公司获得巨额资金回报之后，[Skype公司的创始人弗里斯和曾斯特姆拿出部分资金投资到名为](../Page/Skype.md "wikilink")“威尼斯工程”的[網路电视服务](../Page/網路电视.md "wikilink")。公司高層過去一直以代號"The
Venice
Project."（威尼斯計畫）稱呼這家新公司，2007年1月16日，公布正式名称为"joost"會選擇Joost是因為喜歡這個字的發音，在丹麥文中沒有任何意義。

## Joost发展

## 节目内容

Joost已经和多个版权内容厂商达成合作意向，其中包括华纳唱片公司、电视节目制作公司Endemol、英国的九月影片公司，以及美国的印地赛车联盟。
Joost公司还联系了一些有意愿在免费網路电视节目中做广告的公司，其中有手機生產商[諾基亞](../Page/諾基亞.md "wikilink")、行動營運商T-Mobile、[口香糖厂商以及](../Page/口香糖.md "wikilink")[化妆品品牌](../Page/化妆品.md "wikilink")[美宝莲](../Page/美宝莲.md "wikilink")。大约在六个月之后，他们将会正式发布Joost軟體。在发布前提供的电视内容中，主要包括[音乐](../Page/音乐.md "wikilink")、[喜剧](../Page/喜剧.md "wikilink")、[体育和](../Page/体育.md "wikilink")[紀录片等电视节目](../Page/紀录片.md "wikilink")。
Joost将是一个基於[P2P技术的全球性的軟體和服务商](../Page/P2P.md "wikilink")，所有内容在全球各个地区都可以看到，除非版权影视内容提供商限制传播范围。

## Joost中文版

### 台灣

台灣知名入口網站[PChome
Online原有意於](../Page/PChome_Online.md "wikilink")2008年引進Joost\[1\]，但在該年年底因合約談不攏而使合作計劃暫緩\[2\]，使得Joost正體中文版的發行計劃因而延後。

### 中國大陸

在中国大陸，与[Skype營運方式类似](../Page/Skype.md "wikilink")，Joost与[TOM集团合作](../Page/TOM集团.md "wikilink")，藉由合资公司为[中國大陸用户提供簡體中文视频节目](../Page/中國大陸.md "wikilink")。TOM负责Joost在中国的市场开拓与业务營運，Joost负责技术开发支援。双方藉由与国内众多的内容提供商合作获取正版节目资源。

目前,与TOM合作的中国版joost已经停止服务。

## 註釋

<div class="references-small">

<references />

</div>

## 參見

  - [網路電視](../Page/網路電視.md "wikilink")

## 外部連結

  - [Joost官方網站](https://web.archive.org/web/20070416235519/http://www.joost.com/)
  - [Joost中文官方網站](https://web.archive.org/web/20080821101218/http://joost.tom.com/)（已关闭）

[Category:網路電視](../Category/網路電視.md "wikilink")
[Category:视频分享网站](../Category/视频分享网站.md "wikilink")
[Category:隨選視訊](../Category/隨選視訊.md "wikilink")

1.  <http://www.ithome.com.tw/itadm/article.php?c=48208>
2.