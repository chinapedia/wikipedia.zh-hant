**1997年德國聯賽盃**為首屆舉行[德國聯賽盃](../Page/德國聯賽盃.md "wikilink")，於1997年7月18日至7月26日進行，賽事由1996至97年[德國甲組足球聯賽首五名及](../Page/德國甲組足球聯賽.md "wikilink")[德國盃冠軍參加](../Page/德國盃.md "wikilink")，但由於斯图加特上季同時奪得聯賽殿軍及德國盃冠軍，因此需要由聯賽第六名補上。最後決賽上屆聯賽冠軍[拜仁慕尼黑擊敗](../Page/拜仁慕尼黑.md "wikilink")[斯图加特成為首屆賽事冠軍](../Page/斯图加特足球俱乐部.md "wikilink")。

## 參賽球會

  - [拜仁慕尼黑](../Page/拜仁慕尼黑.md "wikilink")（德甲聯賽冠軍）
  - [勒沃库森](../Page/勒沃库森足球俱乐部.md "wikilink")（德甲聯賽亞軍）
  - [多特蒙德](../Page/多特蒙德足球俱乐部.md "wikilink")（德甲聯賽季軍）
  - [斯图加特](../Page/斯图加特足球俱乐部.md "wikilink")（德甲聯賽殿軍及德國盃冠軍）
  - [波鸿](../Page/波鸿足球俱乐部.md "wikilink")（德甲聯賽第五名）
  - [卡爾斯魯厄](../Page/卡尔斯鲁厄体育俱乐部.md "wikilink")｛德甲聯賽第六名）

## 賽事

### 半準決賽

<table style="width:72%;">
<colgroup>
<col style="width: 16%" />
<col style="width: 12%" />
<col style="width: 8%" />
<col style="width: 12%" />
<col style="width: 24%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>7月18日 20:30</p></td>
<td><p><strong><a href="../Page/卡尔斯鲁厄体育俱乐部.md" title="wikilink">卡爾斯魯厄</a></strong></p></td>
<td><p><strong>2 : 2</strong><br />
<strong>(6 : 5)</strong><br />
<small>十二碼</small></p></td>
<td><p><a href="../Page/勒沃库森足球俱乐部.md" title="wikilink">勒沃库森</a></p></td>
<td><p><a href="../Page/奥厄.md" title="wikilink">奥厄</a></p></td>
</tr>
<tr class="even">
<td><p>7月20日 20:30</p></td>
<td><p><strong><a href="../Page/多特蒙德足球俱乐部.md" title="wikilink">多特蒙德</a></strong></p></td>
<td><p><strong>1 : 0</strong></p></td>
<td><p><a href="../Page/波鸿足球俱乐部.md" title="wikilink">波鸿足球俱乐部</a></p></td>
<td><p><a href="../Page/科布伦茨.md" title="wikilink">科布伦茨</a></p></td>
</tr>
</tbody>
</table>

### 準決賽

|             |                                             |           |                                           |                                        |
| ----------- | ------------------------------------------- | --------- | ----------------------------------------- | -------------------------------------- |
| 7月22日 19:00 | **[斯图加特](../Page/斯图加特足球俱乐部.md "wikilink")** | **3 : 0** | [卡爾斯魯厄](../Page/卡尔斯鲁厄体育俱乐部.md "wikilink") | [奥斯纳布吕克](../Page/奥斯纳布吕克.md "wikilink") |
| 7月23日 19:00 | **[拜仁慕尼黑](../Page/拜仁慕尼黑.md "wikilink")**    | **2 : 0** | [多特蒙德](../Page/多特蒙德足球俱乐部.md "wikilink")   | [奥格斯堡](../Page/奥格斯堡.md "wikilink")     |

### 決賽

|             |                                          |           |                                         |                                    |
| ----------- | ---------------------------------------- | --------- | --------------------------------------- | ---------------------------------- |
| 7月26日 19:30 | **[拜仁慕尼黑](../Page/拜仁慕尼黑.md "wikilink")** | **2 : 0** | [斯图加特](../Page/斯图加特足球俱乐部.md "wikilink") | [勒沃库森](../Page/勒沃库森.md "wikilink") |

## 外部連結

  - [德國聯賽盃官方網站](https://web.archive.org/web/20060720215440/http://www.dfb.de/national/liga-pokal/index.html)

[Category:德國聯賽盃](../Category/德國聯賽盃.md "wikilink")
[Category:1997年足球](../Category/1997年足球.md "wikilink")