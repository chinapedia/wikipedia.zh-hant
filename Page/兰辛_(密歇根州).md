**兰辛**（）是[美国](../Page/美国.md "wikilink")[密歇根州首府](../Page/密歇根州.md "wikilink")，城市主要位于[英厄姆县](../Page/英厄姆县_\(密歇根州\).md "wikilink")，还有少部分位于[伊顿县和](../Page/伊顿县_\(密歇根州\).md "wikilink")[克林顿县](../Page/克林顿县_\(密歇根州\).md "wikilink")，人口119,128（2000年），[都會區人口](../Page/都會區.md "wikilink")454,044，其中[白人占](../Page/白人.md "wikilink")65.28%、[非裔美国人占](../Page/非裔美国人.md "wikilink")21.91%、[亚裔美国人占](../Page/亚裔美国人.md "wikilink")2.13%。

蘭辛都會區主要產業為政府機構、教育機構、汽車產業與保險業，其中密西根州政府、[密西根州立大學](../Page/密西根州立大學.md "wikilink")、[通用汽車為該地區前三大雇用單位](../Page/通用汽車.md "wikilink")。

[L](../Category/密歇根州城市.md "wikilink")
[L](../Category/美国各州首府.md "wikilink")