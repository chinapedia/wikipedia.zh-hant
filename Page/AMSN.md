**aMSN**（alvaro's Messenger）是一款功能丰富的[MSN
Messenger软件](../Page/MSN_Messenger.md "wikilink")，遵照[GNU
GPL进行发布](../Page/GNU_GPL.md "wikilink")，除了[Windows](../Page/Windows.md "wikilink")，还可在其他多种平台下运行，比如[Linux](../Page/Linux.md "wikilink")、[FreeBSD](../Page/FreeBSD.md "wikilink")、[Mac
OS X等](../Page/Mac_OS_X.md "wikilink")。

## 功能

  - 显示图片
  - 表情可定制
  - 多语言支持（目前约40种语言，包括简体、繁体中文）
  - 一次可登录几个帐号
  - 支持文件传送
  - 可进行分组
  - 动画表情可带声音
  - 可保存聊天记录
  - 事件警告
  - 支持摄像头
  - 支持时间戳
  - 支持网络会议
  - 分页式的聊天窗口
  - 支持MSN的移动服务
  - 可选择登录后的各种默认状态，比如隐身等
  - 有不少插件可供下载使用
  - 直接通过aMSN下载新版本，并可自动对语言文件、插件进行升级

## 参见

  - [即时通讯软件列表](../Page/即时通讯软件列表.md "wikilink")
  - [即时通讯软件比较](../Page/即时通讯软件比较.md "wikilink")
  - 目前For Mac的aMSN
    0.98版仍无法支持任何中文输入法，亦无任何外掛可用，只能勉强使用剪贴的方式打中文字，有时还有掉字发生或出现乱码。

## 外部链接

  - [aMSN首页](http://www.amsn-project.net/)

[Category:SourceForge专案](../Category/SourceForge专案.md "wikilink")
[Category:自由的即时通讯软件](../Category/自由的即时通讯软件.md "wikilink")
[Category:自由軟體](../Category/自由軟體.md "wikilink")
[Category:跨平台軟體](../Category/跨平台軟體.md "wikilink")
[Category:自由跨平台軟體](../Category/自由跨平台軟體.md "wikilink")