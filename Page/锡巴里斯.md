锡巴里斯(；；)，是[古希腊在](../Page/古希腊.md "wikilink")[意大利南部](../Page/意大利.md "wikilink")[大希腊地区的一座重要港口](../Page/大希腊.md "wikilink")，位于[塔兰托湾的](../Page/塔兰托湾.md "wikilink")[克拉蒂河与](../Page/克拉蒂河.md "wikilink")入海口之间。

锡巴里斯于公元前720年由[亚该亚人建立](../Page/亚该亚人.md "wikilink")，因其是重要的通商口岸而积累了巨大财富，以其[享樂主義而闻名](../Page/享樂主義.md "wikilink")。

[Category:希臘歷史](../Category/希臘歷史.md "wikilink")
[古希腊地理](../Category/古希腊地理.md "wikilink")