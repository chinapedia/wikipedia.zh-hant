**八宿县**()是[中華人民共和國](../Page/中華人民共和國.md "wikilink")[西藏自治區](../Page/西藏自治區.md "wikilink")[昌都市的一個](../Page/昌都市.md "wikilink")[縣](../Page/縣.md "wikilink")。該縣為1953年由民國時代成立的八宿宗改制而成。

## 资源

  - 矿产资源主要有煤、铁、锡、石膏、碱土等。
  - 野生动植物资源主要有游猴、马鹿、樟子、革狐、水獭、紫招、岩羊、黄羊、盘羊、野牛、旱獭、贝母鸡、马鸡、虫草、贝母、知母、大黄、雪莲等。

## 行政区划

下辖4个[镇](../Page/镇.md "wikilink")、10个[乡](../Page/乡.md "wikilink")，126个行政村：\[1\]
。

## 参考资料

  - A. Gruschke: *The Cultural Monuments of Tibet’s Outer Provinces:
    Kham - Volume 1. The Xizang Part of Kham (TAR)*, White Lotus Press,
    Bangkok 2004. ISBN 974-480-049-6
  - Tsering Shakya: *The Dragon in the Land of Snows. A History of
    Modern Tibet Since 1947*, London 1999, ISBN 0-14-019615-3

[八宿县](../Category/八宿县.md "wikilink")
[Category:昌都地区县份](../Category/昌都地区县份.md "wikilink")
[Category:国家级贫困县](../Category/国家级贫困县.md "wikilink")

1.