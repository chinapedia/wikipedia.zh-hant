**众数**（mode）指一组[数据中出现次数最多的数据值](../Page/数据.md "wikilink")。例如{2,3,3,3}中，出現最多的是3，因此眾數是3，众数可能是一個數，但也可能是多個數。

在[離散概率分布中](../Page/概率分布.md "wikilink")，众数是指[概率质量函数有最大值的數據](../Page/概率质量函数.md "wikilink")，也就是最容易取様到的數據。在連續概率分布中，众数是指[機率密度函數有最大值的數據](../Page/機率密度函數.md "wikilink")，也就是機率密度函數的峰值。

在統計學上，众数和[平均數](../Page/平均數.md "wikilink")、[中位數類似](../Page/中位數.md "wikilink")，都是[总体或](../Page/总体.md "wikilink")[随机变量有關](../Page/随机变量.md "wikilink")[集中趨勢的重要資訊](../Page/集中趨勢.md "wikilink")。在[高斯分佈](../Page/高斯分佈.md "wikilink")（[正態分佈](../Page/正態分佈.md "wikilink")）中，眾數位於峰值，和[平均數](../Page/平均數.md "wikilink")、[中位數相同](../Page/中位數.md "wikilink")。但若分佈是高度偏斜分佈，眾數可能會和平均數、中位數有很大的差異。

分佈中的众数不一定只有一個，若概率质量函数或機率密度函數在*x*<sub>1</sub>,
*x*<sub>2</sub>……等多個點都有最大值，就會有多個众数，最極端的情形是[離散型均勻分佈](../Page/離散型均勻分佈.md "wikilink")，所有的點概率都相同，所有的點都是眾數。若機率密度函數有數個[局部最大值](../Page/極值.md "wikilink")，一般會將這幾個極值都稱為众数，此連續機率分佈會稱為（和相反）。

若是[對稱的單峰分布](../Page/軸對稱.md "wikilink")（例如[正態分佈](../Page/正態分佈.md "wikilink")），眾數和[平均數](../Page/平均數.md "wikilink")、[中位數會重合](../Page/中位數.md "wikilink")\[1\]。若一随机变量是由對稱的总体中產生，可以用取樣的平均值來估計總體的眾數。

## 特征

用众数代表一组数据，适合于数据量较多时使用，且众数不受[极端数据的影响](../Page/极端数据.md "wikilink")\[2\]，并且求法简便。在一组数据中，如果个别数据有很大的变动，选择[中位数表示这组数据的](../Page/中位数.md "wikilink")“集中趋势”就比较适合。

當數值或被觀察者沒有明顯次序（常發生於非數值性資料）時特別有用，由於可能無法良好定義[算術平均數和](../Page/算術平均數.md "wikilink")[中位數](../Page/中位數.md "wikilink")。例子：{蘋果，蘋果，香蕉，橙，橙，橙，桃}的眾數是橙。

## 使用

主要用于[分类数据](../Page/分类数据.md "wikilink")，也可用于[顺序数据和](../Page/顺序数据.md "wikilink")[数值型数据](../Page/数值型数据.md "wikilink")。

## 歷史

众数的英文mode最早是由[卡尔·皮尔逊在](../Page/卡尔·皮尔逊.md "wikilink")1895年開始使用\[3\]。

## 参考文献

## 参见

  - [集中趨勢](../Page/集中趨勢.md "wikilink")：[平均數](../Page/平均數.md "wikilink")、[中位數](../Page/中位數.md "wikilink")

  - [常態分佈](../Page/常態分佈.md "wikilink")

  -
  - [描述统计学](../Page/描述统计学.md "wikilink")

  - [矩 (數學)](../Page/矩_\(數學\).md "wikilink")

  -
{{-}}

[Category:统计学](../Category/统计学.md "wikilink")
[Category:平均数](../Category/平均数.md "wikilink")

1.  [現代統計學的發展](http://episte.math.ntu.edu.tw/articles/mm/mm_03_3_09/)
2.
3.  Pearson, Karl (1895). "Contributions to the Mathematical Theory of
    Evolution. II. Skew Variation in Homogeneous Material",
    *Philosophical Transactions of the Royal Society of London, Ser. A*,
    186, 343-414