**蘇永欽**（)，籍貫浙江杭州，生於台灣[宜蘭縣](../Page/宜蘭縣.md "wikilink")，台灣法學家。曾任國家通訊傳播委員會首任主任委員，並於2010年就任[大法官並為](../Page/司法院大法官.md "wikilink")[司法院副院長](../Page/司法院副院長.md "wikilink")，2016年請辭獲准，9月30日卸任。

## 學術經歷

1972年於[國立臺灣大學](../Page/國立臺灣大學.md "wikilink")[法律系畢業](../Page/法律系.md "wikilink")，與[馬英九是同學](../Page/馬英九.md "wikilink")。1981年取得[德國](../Page/德國.md "wikilink")[慕尼黑大學法學博士](../Page/慕尼黑大學.md "wikilink")。

  - [國立政治大學法律系教授](../Page/國立政治大學.md "wikilink")
  - 法學院院長兼法律系主任
  - 政治大學法學院公法中心主任

## 從政經歷

在[李登輝執政期間](../Page/李登輝.md "wikilink")，曾銜命前往遊說[陶百川接受修憲](../Page/陶百川.md "wikilink")，未果，但頗獲重用，並對堅持護憲的憲法耆宿[胡佛批判尤烈](../Page/胡佛_\(台灣\).md "wikilink")。李登輝屢次發動修憲，為總統職位擴權，蘇永欽辯論各方，功不可沒。後多次借調到行政機關擔任公職，1996年擔任[行政院公平交易委員會副主任委員](../Page/行政院公平交易委員會.md "wikilink")，亦曾任[行政院新聞局法規委員會](../Page/行政院新聞局.md "wikilink")、訴願委員會委員。

1997年，政大法學院該學年度第一學期的教職申請在該年三月底已截止，[馬英九於](../Page/馬英九.md "wikilink")5月8日辭去[政務委員後才提出申請](../Page/政務委員.md "wikilink")，法學院為此召開臨時院務會議通過受理申請，並將申請截止日期為馬先生量身打造延長到五月底止。為此，[許玉秀和](../Page/許玉秀.md "wikilink")[陳惠馨兩位政大法律系教授撰文](../Page/陳惠馨.md "wikilink")〈特權很難反省〉，批評蘇永欽等為馬破例的特權問題，文中強調「往後至少政大法律系所公告的期限再也很難取信於人」\[1\]\[2\]。

自2006年起，因[國家通訊傳播委員會採政黨比例代表制](../Page/國家通訊傳播委員會.md "wikilink")，因此蘇永欽又受中國國民黨推薦，專任國家通訊傳播委員會主任委員。

2010年8月24日，蘇永欽受總統[馬英九提名為司法院副院長暨大法官人選](../Page/馬英九.md "wikilink")。獲提名後[民間司改會的評價是](../Page/民間司改會.md "wikilink")「非常不適任、非常不推薦」，司改會評價認為他雖有專業能力，但曾多次提出支持威權、壓縮民主與人權的意見，道德也有嚴重瑕疵：例如違背程序正義，護航馬英九到政大法律系任專任教授等。\[3\]同年10月8日立法院行使同意權，獲73票贊成出任司法院副院長；於10月13日上任。

2016年7月12日，司法院副院長蘇永欽臨別砲轟立法院民進黨團總召[柯建銘阻礙觀審制](../Page/柯建銘.md "wikilink")。柯建銘反駁，這種「只讓你看，不讓你判」的假改革距離人民期待太遠，毫無學理基礎，並非民進黨團所能阻擋。

## 相關親友

  - 妻[彭鳳至與蘇永欽為臺灣大學法律系及德國慕尼黑大學同學](../Page/彭鳳至.md "wikilink")，現任最高行政法院院長，曾任[司法院大法官](../Page/司法院大法官.md "wikilink")。在國家通訊傳播委員會爭議釋憲時，彭鳳至即因蘇永欽擔任該會主任委員而迴避。
  - 兄為[國家安全會議前秘書長的](../Page/國家安全會議.md "wikilink")[蘇起](../Page/蘇起.md "wikilink")。
  - 嫂[陳月卿為資深媒體從業人員](../Page/陳月卿.md "wikilink")。
  - 蘇永欽與[中華民國總統](../Page/中華民國總統.md "wikilink")[馬英九是建中及台大法律系的同學](../Page/馬英九.md "wikilink")。

## 榮譽

  - 行政院國家科學委員會傑出研究獎，1994年、1998年
  - 政大傑出研究講座教師，2001年、2002年

## 著作

  - 《聯合政府：台灣民主體制的新選擇？》（主編），新台灣人文教基金會2001年出版，ISBN 957-30797-1-2
  - 《國會改革：台灣民主憲政的新境界？》（主編），新台灣人文教基金會2001年出版，ISBN 957-30797-5-5
  - 《政府再造：政府角色功能的新定位》（主編），新台灣人文教基金會2002年出版，ISBN 957-30797-7-1
  - 《地方自治：落實人民主權的第一步》（主編），新台灣人文教基金會2002年出版，ISBN 957-30797-8-X

## 注釋

[蘇永欽公開辭職信
反對總統主導司改](http://news.ltn.com.tw/news/society/breakingnews/1759925)
[反駁蘇永欽
柯建銘：「觀審制」距離人民太遙遠](http://news.ltn.com.tw/news/politics/breakingnews/1760269)

## 外部連結

  - [蘇永欽教授個人網頁](http://www3.nccu.edu.tw/~suyc/)

|- |colspan="3"
style="text-align:center;"|**[Executive_Yuan,ROC_LOGO.svg](https://zh.wikipedia.org/wiki/File:Executive_Yuan,ROC_LOGO.svg "fig:Executive_Yuan,ROC_LOGO.svg")[行政院](../Page/行政院.md "wikilink")**
|-     |- |colspan="3"
style="text-align:center;"|**[ROC_Judicial_Yuan_Seal.svg](https://zh.wikipedia.org/wiki/File:ROC_Judicial_Yuan_Seal.svg "fig:ROC_Judicial_Yuan_Seal.svg")[司法院](../Page/司法院.md "wikilink")**
|-

[category:台灣法學家](../Page/category:台灣法學家.md "wikilink")

[Category:中華民國司法院副院長](../Category/中華民國司法院副院長.md "wikilink")
[Category:司法院大法官](../Category/司法院大法官.md "wikilink")
[Category:中華民國國家通訊傳播委員會主任委員](../Category/中華民國國家通訊傳播委員會主任委員.md "wikilink")
[Category:中國國民黨黨員](../Category/中國國民黨黨員.md "wikilink")
[Category:國立政治大學教授](../Category/國立政治大學教授.md "wikilink")
[Category:慕尼黑大學校友](../Category/慕尼黑大學校友.md "wikilink")
[Category:國立臺灣大學法律學院校友](../Category/國立臺灣大學法律學院校友.md "wikilink")
[Category:臺北市立建國高級中學校友](../Category/臺北市立建國高級中學校友.md "wikilink")
[Category:臺北市立萬華國民中學校友](../Category/臺北市立萬華國民中學校友.md "wikilink")
[Category:臺北市萬華區老松國民小學校友](../Category/臺北市萬華區老松國民小學校友.md "wikilink")
[Category:杭州裔台灣人](../Category/杭州裔台灣人.md "wikilink")
[Category:宜蘭人](../Category/宜蘭人.md "wikilink")
[U](../Category/苏姓.md "wikilink")

1.  [馬屁由來有自
    特權很難反省](http://www.libertytimes.com.tw/2008/new/jun/23/today-o2.htm)
    , 自由時報, 2008年6月23日
2.  [特權很難反省](http://bbs.cs.nccu.edu.tw/gmore?A_Law&F0RBFN2Q&16), 1997年5月
3.  [2010年大法官暨司法院正、副院長被提名人評鑑報告：](http://www.jrf.org.tw/newjrf/RTE/myform_detail.asp?id=2732)