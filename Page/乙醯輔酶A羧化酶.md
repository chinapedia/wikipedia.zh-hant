**乙醯輔酶A羧化酶**（）是一種[生物素依賴的](../Page/生物素.md "wikilink")[酵素](../Page/酵素.md "wikilink")，專門催化生物體內由[乙醯輔酶A轉變成](../Page/乙醯輔酶A.md "wikilink")[丙二醯輔酶A的化學反應](../Page/丙二醯輔酶A.md "wikilink")。

這種酵素可分為3個不同區域，分別是生物素載體蛋白（biotin carrier protein）、生物素羧化酶（biotin
carboxylase）以及轉羧酶（transcarboxylase）。對細菌來說，這3個區域分別是3個次單位；對動物來說則相連在同一[多肽上](../Page/多肽.md "wikilink")；而植物細胞則同時擁有兩種。

[乙醯輔酶A在此酵素的催化之下](../Page/乙醯輔酶A.md "wikilink")，會經過兩個步驟的反應，首先生物素載體蛋白上的生物素臂，會經由生物素羧化酶的催化，並消耗[ATP](../Page/三磷酸腺苷.md "wikilink")，而與[二氧化碳](../Page/二氧化碳.md "wikilink")（HCO<sub>3</sub><sup>−</sup>，溶於水後的狀態）結合；接著二氧化碳又在轉羧酶催化下與乙醯輔酶A，形成[丙二醯輔酶A](../Page/丙二醯輔酶A.md "wikilink")，並脫離乙醯輔酶A羧化酶。

## 结构

## 反应机理

[ACAC_mechanism_(zh-cn).svg](https://zh.wikipedia.org/wiki/File:ACAC_mechanism_\(zh-cn\).svg "fig:ACAC_mechanism_(zh-cn).svg")

[Category:EC 6.4.1](../Category/EC_6.4.1.md "wikilink")