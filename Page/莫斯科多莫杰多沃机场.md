**莫斯科多莫傑多沃機場**（；），又譯**-{多莫捷多沃}-机场**、**多莫德多禾機場**、**多莫德多夫機場**、**杜莫迪多沃机场**，是一個位於[俄羅斯](../Page/俄羅斯.md "wikilink")[莫斯科的民用](../Page/莫斯科.md "wikilink")[國際機場](../Page/國際機場.md "wikilink")。本機場是俄羅斯最大的機場，也是[S7航空公司及](../Page/S7航空公司.md "wikilink")[VIM航空的樞杻港](../Page/VIM航空.md "wikilink")。在三個莫斯科機場中，多莫傑多沃機場為最重要的機場，其餘兩個機場為[謝列梅捷沃國際機場和](../Page/謝列梅捷沃國際機場.md "wikilink")[伏努科沃國際機場](../Page/伏努科沃國際機場.md "wikilink")。

在2003年，機場擴展機場的計劃，以使[廣體飛機可以使用這機場](../Page/廣體飛機.md "wikilink")。跑道、滑行道和停機坪都被擴大加強。在2009年3月，機場批准了一個允許[空中巴士A380在該機場停靠的計畫](../Page/空中巴士A380.md "wikilink")。這批准標誌著機場的等級提升至[ICAO類別F](../Page/ICAO.md "wikilink")。\[1\]

在2011年1月24日，機場發生了[2011年多莫傑多沃國際機場炸彈襲擊事件](../Page/2011年多莫傑多沃國際機場炸彈襲擊事件.md "wikilink")，之後，機場都明顯地加強保安。

## 歷史

1962年，多莫傑多沃國際機場啟用，提供一個往斯維爾德洛夫斯克（現[葉卡捷琳堡](../Page/葉卡捷琳堡.md "wikilink")）飛行的航班，機型使用[圖波列夫
圖-104型飛機](../Page/圖-104.md "wikilink")。并帶動了[蘇聯的國內長途航線的成長](../Page/蘇聯.md "wikilink")。在1965年5月，官方宣布機場啟用。在18個月後，與第一條跑道平行的第二條跑道正式啟用。在1975年12月26日，由此機場前往[阿拉木圖的航班啟航](../Page/阿拉木圖.md "wikilink")，此航線利用[圖波列夫
圖-144型飛機](../Page/圖-144.md "wikilink")。

在1996年後，多莫傑多沃國際機場由[東線集團管理](../Page/東線集團.md "wikilink")，期限為75年，但是跑道的操控權還在州政府手中。東線集團對機場進行了重建，包括調動海關部門，給予乘客更大的方便。另外，因為謝列梅捷沃國際機場發生諸多問題，[英國航空](../Page/英國航空.md "wikilink")、[以色列航空](../Page/以色列航空.md "wikilink")、[瑞士國際航空](../Page/瑞士國際航空.md "wikilink")、[日本航空和](../Page/日本航空.md "wikilink")[奧地利航空等航空公司都把航班轉到這機場](../Page/奧地利航空.md "wikilink")，後來[阿聯酋航空](../Page/阿聯酋航空.md "wikilink")、[布魯塞爾航空](../Page/布魯塞爾航空.md "wikilink")、[泰國國際航空](../Page/泰國國際航空.md "wikilink")、[新加坡航空](../Page/新加坡航空.md "wikilink")、[卡塔爾航空](../Page/卡塔爾航空.md "wikilink")、[皇家約旦航空和](../Page/皇家約旦航空.md "wikilink")[德國漢莎航空也加入其行列](../Page/德國漢莎航空.md "wikilink")。同時，[俄羅斯航空也把貨運航班轉到此機場](../Page/俄羅斯航空.md "wikilink")。因此該機場成為全俄羅斯最大的機場\[2\]。

2003年，空中交通管制塔重建，機場可以每小時最高處理70班出發和抵達航班。現時機場有五個商務出發[貴賓室](../Page/貴賓室.md "wikilink")，由不同航空公司擁有。俄羅斯最大的航空公司之一的[俄羅斯全祿航空和另一間較小型的航空公司](../Page/俄羅斯全祿航空.md "wikilink")[S7航空作此為樞紐機場](../Page/S7航空.md "wikilink")。

東線集團的目標是令機場在未來可以穩定地營運，以及建立一個主要的國際和多式運輸樞紐。在2004年的前三個季度，國際乘客比起上年增加了52.8%。國內乘客和貨運量也顯著上升。比起其餘兩座莫斯科機場，該機場的好處是較接近莫斯科市中心，因而有良好的運輸網絡。

2011年1月24日，機場發生了炸彈襲擊事件，事件發生在行李領取處，造成35人死亡和150人多人受傷\[3\]。

## 未來發展

東線集團將擴展客運大樓至22.5萬平方米，并宣布投資3億美元去在未來兩年建造和升級客運大樓。例如佔地97,600平方米的第一客運大樓（T1）會擴展至124,600平方米，第二客運大樓（T2，將來會負責處理國際航線）將會有更多空間去設置[辦理登機手續櫃位和出發閘口](../Page/辦理登機手續.md "wikilink")。

在第二客運大樓的第一階段工程會在2006年尾完工，到時便可每年處理700萬位乘客。接下來會將會再擴展至225,000平方米。

2012年，新的國內客運大樓（第三客運大樓）完工，機場可以每年處理2400萬至2800萬位乘客。當第四客運大樓完工，那時機場便可以每年處理約3000萬至3500萬位乘客。所有的客運大樓會連接起來，以便可以提高效率。

## 地理位置与周边交通

[Aeroport-domodedovo-station.jpg](https://zh.wikipedia.org/wiki/File:Aeroport-domodedovo-station.jpg "fig:Aeroport-domodedovo-station.jpg")
机场離莫斯科市中心西南方约40公里，有快速公路及鐵路连通莫斯科市及机场。

### 鐵路運輸和客車運輸

在莫斯科市中心的[帕韋列茨站](../Page/帕韋列茨站.md "wikilink")（）有[電氣化的列車往返機場](../Page/機場快線_\(俄羅斯\).md "wikilink")，每一小時開出一班，高峰時30分鐘一班，票價500[盧布](../Page/俄罗斯卢布.md "wikilink")（約[人民幣](../Page/人民币.md "wikilink")54元／[港幣](../Page/港幣.md "wikilink")68元），行車時間約45分钟，有分為直達列車和非直達兩種，非直通車依停靠站數不同，因此行車時間會有不同。此外，還有一個常規的客車服務，來往與機場和城市南面[莫斯科地鐵的](../Page/莫斯科地鐵.md "wikilink")[多莫傑多沃站之間](../Page/多莫傑多沃站.md "wikilink")。

### 道路

在多莫傑多沃國際機場內，提供了租車服務。這裡有數間租車公司，而租車者需要一些必須的文件，以及建議去了解這些租車公司去得到更多資訊\[4\]。

## 机场设施

目前为止有一座主航站楼，一侧与火车站相连。多莫傑多沃國際機場是目前为止莫斯科最现代化的国际机场，但其规模仍然有限。

## 客運大樓、航空公司及目的地

現時多莫傑多沃國際機場有一座客運大樓，當中有兩座建築物，分別負責處理國內和國際航班，機場共有22座[空橋](../Page/空橋.md "wikilink")。
[Domodedovo_airport.jpg](https://zh.wikipedia.org/wiki/File:Domodedovo_airport.jpg "fig:Domodedovo_airport.jpg")
[Domodedovo-terminal.jpg](https://zh.wikipedia.org/wiki/File:Domodedovo-terminal.jpg "fig:Domodedovo-terminal.jpg")
[Domodedovo_International_Airport_terminal_building.jpg](https://zh.wikipedia.org/wiki/File:Domodedovo_International_Airport_terminal_building.jpg "fig:Domodedovo_International_Airport_terminal_building.jpg")
[SwissAir_Airbus_A320-214.jpg](https://zh.wikipedia.org/wiki/File:SwissAir_Airbus_A320-214.jpg "fig:SwissAir_Airbus_A320-214.jpg")
[空中巴士A320](../Page/空中巴士A320.md "wikilink")\]\]
[Domodedovo-airport.jpg](https://zh.wikipedia.org/wiki/File:Domodedovo-airport.jpg "fig:Domodedovo-airport.jpg")
[波音767](../Page/波音767.md "wikilink")\]\]
[S7_Boeing-737-800.jpg](https://zh.wikipedia.org/wiki/File:S7_Boeing-737-800.jpg "fig:S7_Boeing-737-800.jpg")
[波音737-800](../Page/波音737-800.md "wikilink")\]\]

## 航空管制

| 種類      | 周波数                      |
| ------- | ------------------------ |
| GND     | 119MHz                   |
| TWR     | 118.6MHz,119.7MHz        |
| APRON   | 130.6MHz                 |
| TRANSIT | 129.15MHz                |
| APP     | 120.6MHz,124.4MHz,129MHz |
| VOLMET  | 127.875                  |
| ATIS    | 128.3MHz                 |

## 航空保安無線設施

| 局名         | 種類  | 周波数      | 識別信号 |
| ---------- | --- | -------- | ---- |
| DOMODEDOVO | NDB | 437.0KHz | DW   |

## 事件和事故

  - 2004年8月，两名车臣女自杀炸弹袭击者炸毀兩架分別从莫斯科飞往[伏尔加格勒和](../Page/伏尔加格勒.md "wikilink")[索契的航班](../Page/索契.md "wikilink")，造成90名乘客死亡，多人受傷\[5\]。
  - 2010年3月22日，一架飛自[埃及赫尔格达的客機试图降落在多莫杰多沃机场時在森林中發生坠毁](../Page/埃及.md "wikilink")。没有人死亡，但是四名机组人员重伤\[6\]。
  - 2010年12月4日，达吉斯坦航空公司372次航班在多莫杰多沃機場緊急降落時造成2人死亡，56人受傷\[7\]。
  - 2010年12月25日，莫斯科及附近地區的[凍雨造成两條电源給多莫杰多沃机场供電的電線短路](../Page/凍雨.md "wikilink")，致使机场和高速铁路完全癱瘓。數百名乘客滯留在機場內。\[8\]
  - 2011年1月24日机场发生爆炸，造成至少35人遇难，180人受伤。初步被怀疑是一起自杀式炸弹袭击\[9\]。

## 参考

## 外部链接

  - [莫斯科多莫傑多沃機場官方網頁](http://www.domodedovo.ru/)
  - [多莫杰多沃国际机场（照片）](http://phototravelguide.ru/aeroport-vokzal/domodedovo-foto/)
  - [Plane Finder–Live Flight Status Tracker](http://planefinder.net/)

[Д](../Category/俄羅斯機場.md "wikilink")
[Category:莫斯科建築物](../Category/莫斯科建築物.md "wikilink")

1.  *重金屬*, [Aviation Week & Space
    Technology](../Page/Aviation_Week_&_Space_Technology.md "wikilink"),
    **70**, 10 (2009年3月9日), p. 14
2.
3.
4.
5.  <http://rmrb.com.cn/GB/guoji/1029/2747199.html>
6.
7.
8.
9.