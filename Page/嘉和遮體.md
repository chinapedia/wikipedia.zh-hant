**嘉和遮體**位於[台灣](../Page/台灣.md "wikilink")[屏東縣](../Page/屏東縣.md "wikilink")[枋山鄉](../Page/枋山鄉.md "wikilink")，是[臺灣鐵路管理局](../Page/臺灣鐵路管理局.md "wikilink")[南迴線的一座](../Page/南迴線.md "wikilink")[假隧道](../Page/假隧道.md "wikilink")，和與[臺灣鐵路管理局](../Page/臺灣鐵路管理局.md "wikilink")[縱貫線北段的](../Page/縱貫線北段.md "wikilink")[山佳隧道是相同的](../Page/山佳隧道.md "wikilink")，位置在[加祿車站和](../Page/加祿車站.md "wikilink")[內獅車站之間](../Page/內獅車站.md "wikilink")，全長1,180[公尺](../Page/公尺.md "wikilink")。

## 緣由

[中華民國海軍與](../Page/中華民國海軍.md "wikilink")[中華民國空軍](../Page/中華民國空軍.md "wikilink")[防空砲兵部隊過去在](../Page/空軍防空砲兵指揮部.md "wikilink")1960－1990年代曾以鄰近[平埔靶場為實彈射擊岸轟訓練靶場](../Page/平埔靶場.md "wikilink")\[1\]，[交通部臺灣鐵路管理局過去為了防止](../Page/交通部臺灣鐵路管理局.md "wikilink")[臺鐵列車遭到](../Page/臺鐵列車.md "wikilink")[中華民國國軍](../Page/中華民國國軍.md "wikilink")[砲彈誤擊](../Page/砲彈.md "wikilink")，故興建此[掩體](../Page/掩體.md "wikilink")[隧道](../Page/隧道.md "wikilink")，此隧道的結構經過強化。在正式通車前，[中華民國國防部已將靶場移駐](../Page/中華民國國防部.md "wikilink")[保力山靶場](../Page/保力山靶場.md "wikilink")，此隧道失去原有作用。嘉和遮體為全臺[鐵路系統中唯一建於平地的隧道](../Page/鐵路.md "wikilink")，於1991年9月峻工。因阻擋西南風吹入，導致[芒果植栽通風不佳](../Page/芒果.md "wikilink")，生長不良，而引起村民埋怨，被認為是一項錯誤的[工程](../Page/工程.md "wikilink")[設計](../Page/設計.md "wikilink")，但也因為這條假隧道而降低列車因落山風而脫軌的機率
\[2\]。

## 相關條目

  - [假隧道](../Page/假隧道.md "wikilink")

## 資料來源

<references/>

[Category:台灣鐵路隧道](../Category/台灣鐵路隧道.md "wikilink")
[Category:屏東縣交通](../Category/屏東縣交通.md "wikilink")
[Category:枋山鄉](../Category/枋山鄉.md "wikilink")

1.  黃金歲月五十年-黃宏基將軍憶往,第19頁,國防部史政編譯室編印,民國96年
2.  [台灣地區地名查詢系統-南迴鐵路嘉和遮體](http://placesearch.moi.gov.tw/search/place_list.php?id=28169)