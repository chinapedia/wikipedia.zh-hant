**龘**（[漢語拼音](../Page/漢語拼音.md "wikilink")：dá\[1\]/tà\[2\]；[注音符號](../Page/注音符號.md "wikilink")：ㄊㄚˋ\[3\]/ㄉㄚˊ；[粵拼](../Page/粵拼.md "wikilink")：daap6\[4\]）字是現時[Big5碼編碼當中](../Page/Big5碼.md "wikilink")[筆劃最多的一個字](../Page/筆劃.md "wikilink")，筆劃達48劃。這個字亦是[中華民國一般](../Page/中華民國.md "wikilink")[字典中筆劃最多的一個字](../Page/字典.md "wikilink")。除此，該字也是全世界所有常用語言中，極為少見筆劃相當多的[單音節](../Page/單音節.md "wikilink")[單字](../Page/單字.md "wikilink")。

## 字義

該字隸屬[龍部](../Page/龍部.md "wikilink")，古代同「[龖](../Page/龖.md "wikilink")」，是「龖」字之異體字。《[說文](../Page/說文.md "wikilink")》解作「飛龍」。《[廣韻](../Page/廣韻.md "wikilink")》說該字之義為「龍飛之狀」，龍騰飛的樣子。

## 筆劃更多的字

其實還有一些筆劃比「龘」更多的字，但目前一些舊的電腦系統尚不支援顯示。

例如有一個由四條「-{[龍](../Page/龍.md "wikilink")}-」分據左上、右下、左下、右上四方所組成的「𪚥」，更達64劃，Unicode中已收錄（[𪚥](https://www.unicode.org/cgi-bin/GetUnihanData.pl?codepoint=2A6A5)），但目前一些舊的電腦系統不能顯示。該字被[中華民國教育部列為](../Page/中華民國教育部.md "wikilink")「正字」，發音[注音符號標示為](../Page/注音符號.md "wikilink")「ㄓㄜˊ(zhé)」，明刊本《四聲篇海》中釋其義為「多言也」。\[5\]

## 参考资料

[Category:汉字](../Category/汉字.md "wikilink")
[Category:世界之最](../Category/世界之最.md "wikilink")

1.  [搜狗输入法五一热词：人 人 人
    人_科技_环球网](http://tech.huanqiu.com/Enterprise/2014-05/4987605.html)
2.  [漢字「龘」：基本資料](http://chardb.iis.sinica.edu.tw/char/27685)，中央研究院
3.  [汉典“龘”字](http://www.zdic.net/z/29/js/9F98.htm)
4.  [粵語審音配詞字庫](http://humanum.arts.cuhk.edu.hk/Lexis/lexi-can/search.php?q=%F9%D5)
5.