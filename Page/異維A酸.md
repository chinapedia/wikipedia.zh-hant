**異維A酸（Isotretinoin）**，是[维A酸的](../Page/维A酸.md "wikilink")[异构体](../Page/异构体.md "wikilink")，最初是一種治療腦癌、胰臟癌和皮膚癌等多種癌病的藥物，但現時常用作治療[青春痘](../Page/青春痘.md "wikilink")，通常為口服用A酸，外用A酸則通常為維A酸成份，有時亦用作醫治[斑色鱼鳞癣](../Page/斑色鱼鳞癣.md "wikilink")。它属于衍生自[维生素A的](../Page/维生素A.md "wikilink")[类视色素](../Page/类视色素.md "wikilink")，人体可自然形成小量的此化学物。

自2002年2月起，[羅氏藥廠對異維A酸專利權屆滿後](../Page/羅氏藥廠.md "wikilink")，市面有多間廠商生産通用名药物。其中中國商品名稱叫作「泰尔絲」或「特維絲」，其餘常見藥名包括Accutane、Amnesteem、Claravis、Decutan、Isotane、Sotret、Oratane、Isotrex和Isotrexin等。

異維A酸的副作用近年引起廣泛關注。有不同研究探討該藥是否會引致抑鬱，增加自殺風險。中国国家食品药品监督管理局亦警告該藥可能影響成長中的骨骼，建议18岁以下青少年尽量不要使用。孕婦服用後亦會大幅增加生出畸胎的機會，因而被绝对禁止使用。

## 参考文献

<div class="references-small">

<references />

</div>

[category:类视黄醇](../Page/category:类视黄醇.md "wikilink")

[Category:抗痤疮制剂](../Category/抗痤疮制剂.md "wikilink")