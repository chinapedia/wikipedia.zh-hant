**鼴形田鼠屬**（******）是[哺乳綱](../Page/哺乳綱.md "wikilink")[囓齒目](../Page/囓齒目.md "wikilink")[倉鼠科](../Page/倉鼠科.md "wikilink")[田鼠亚科的一屬](../Page/田鼠亚科.md "wikilink")，而與鼴形田鼠屬（鼴形田鼠）同科的動物尚有[東方兔尾鼠屬](../Page/東方兔尾鼠屬.md "wikilink")（帕氏兔尾鼠）、[絨鼠屬](../Page/絨鼠屬.md "wikilink")（中國絨鼠）、[環頸旅鼠屬](../Page/環頸旅鼠屬.md "wikilink")（環頸旅鼠）等數種[哺乳動物](../Page/哺乳動物.md "wikilink")。它们主要分布在亚洲西部和中部。它们与[鼹鼠无关](../Page/鼴科.md "wikilink")，只是在外貌上相似。它们终年生活在地下。

鼴形田鼠屬分两个[亚属和五个](../Page/亚属.md "wikilink")[种](../Page/种.md "wikilink")：

  - 鼴形田鼠亚屬（）

      - [鼹形田鼠](../Page/鼹形田鼠.md "wikilink")（），分布区域从[乌克兰东部经过](../Page/乌克兰.md "wikilink")[哈萨克斯坦至](../Page/哈萨克斯坦.md "wikilink")[阿富汗北部](../Page/阿富汗.md "wikilink")
      - [坦氏鼹形田鼠](../Page/坦氏鼹形田鼠.md "wikilink")（），[土库曼斯坦](../Page/土库曼斯坦.md "wikilink")、[乌兹别克斯坦](../Page/乌兹别克斯坦.md "wikilink")、哈萨克斯坦、[新疆](../Page/新疆.md "wikilink")
      - [阿赖鼹形田鼠](../Page/阿赖鼹形田鼠.md "wikilink")（），[阿赖山脉](../Page/阿赖山脉.md "wikilink")

  - 亚属

      - [土黄鼹形田鼠](../Page/土黄鼹形田鼠.md "wikilink")（），[高加索山脉](../Page/高加索山脉.md "wikilink")、[安纳托](../Page/安纳托.md "wikilink")、[伊拉克](../Page/伊拉克.md "wikilink")
      - [阿富汗鼹形田鼠](../Page/阿富汗鼹形田鼠.md "wikilink")（），[伊朗](../Page/伊朗.md "wikilink")、阿富汗、土库曼斯曼、[巴基斯坦](../Page/巴基斯坦.md "wikilink")

鼴形田鼠躯体粗壮，身长在10至15厘米之间，[尾短](../Page/尾.md "wikilink")，只有5毫米至2厘米长。[背部](../Page/背部.md "wikilink")[毛色为](../Page/毛色.md "wikilink")[棕色或](../Page/棕色.md "wikilink")[灰色](../Page/灰色.md "wikilink")，[腹部为灰色或](../Page/腹部.md "wikilink")[白色](../Page/白色.md "wikilink")。没有[耳壳](../Page/耳壳.md "wikilink")，[眼睛非常小](../Page/眼睛.md "wikilink")，几乎完全被[毛遮住](../Page/毛.md "wikilink")。特别显著的是很大的向前伸出[嘴的](../Page/嘴.md "wikilink")[门牙](../Page/门牙.md "wikilink")。它们被用来挖洞。

鼴形田鼠的洞在地下20至30厘米深。它们组成一个分支很多的[隧道系统](../Page/隧道.md "wikilink")，它们的[窝在地下](../Page/窝.md "wikilink")50厘米的深处。它们在挖洞同时寻找地下的[植物](../Page/植物.md "wikilink")[组织如](../Page/组织.md "wikilink")[根](../Page/根.md "wikilink")、[块茎等作为它们的](../Page/块茎.md "wikilink")[食物](../Page/食物.md "wikilink")。因此，在一些[地区它们被看作是害兽](../Page/地区.md "wikilink")。

学者一般认为鼴形田鼠屬是田鼠亚科中的一个[属](../Page/属.md "wikilink")，但是也有些[生物学家把它完全从](../Page/生物学家.md "wikilink")[田鼠中分出来](../Page/田鼠.md "wikilink")，不过大多数学者还是把它们看成田鼠，只不过是非常特殊的，与其它田鼠很不相同的田鼠而已。在[更新世它们的分布比今天还要广](../Page/更新世.md "wikilink")，当时甚至在[巴勒斯坦和](../Page/巴勒斯坦.md "wikilink")[北美洲也有鼴形田鼠](../Page/北美洲.md "wikilink")。

## 参考资料

  - Musser, G. G. and M. D. Carleton. 2005. Superfamily Muroidea. Pp.
    894-1531 *in* Mammal Species of the World a Taxonomic and Geographic
    Reference. D. E. Wilson and D. M. Reeder eds. Johns Hopkins
    University Press, Baltimore.

[Category:鼴形田鼠屬](../Category/鼴形田鼠屬.md "wikilink")
[Category:田鼠亞科](../Category/田鼠亞科.md "wikilink")