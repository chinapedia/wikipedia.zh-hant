**楊乃文**（），出生於[臺灣](../Page/臺灣.md "wikilink")[臺北](../Page/臺北.md "wikilink")，畢業於[雪梨大學](../Page/雪梨大學.md "wikilink")，華語女[歌手](../Page/歌手.md "wikilink")，有「搖滾女王」的稱號，曾獲[第11屆金曲獎](../Page/第11屆金曲獎.md "wikilink")[最佳國語女演唱人獎](../Page/最佳國語女歌手獎_\(金曲獎\).md "wikilink")。楊乃文亦擔任過[模特兒和參與](../Page/模特兒.md "wikilink")[電影](../Page/電影.md "wikilink")、[舞台劇的演出](../Page/舞台劇.md "wikilink")。
2019年，以補位歌手身份參加[歌手2019](../Page/歌手2019.md "wikilink")｡

## 經歷

### 早期生活

楊乃文於1974年3月2日出生於[台湾](../Page/台湾.md "wikilink")[台北市](../Page/台北市.md "wikilink")\[1\]，從六歲開始學習[鋼琴](../Page/鋼琴.md "wikilink")，同時也會演奏其他多種樂器（[琵琶](../Page/琵琶.md "wikilink")、[三弦](../Page/三弦.md "wikilink")、[鼓等](../Page/鼓.md "wikilink")）。在小學五年級時，楊乃文和家人移民至[澳洲](../Page/澳洲.md "wikilink")，居住於[雪梨](../Page/雪梨.md "wikilink")。在就讀澳洲的中學期間，開始接觸[長笛](../Page/長笛.md "wikilink")\[2\]。楊乃文在中學期間曾參加過[雪梨歌劇院的音樂劇工作營](../Page/雪梨歌劇院.md "wikilink")，在為期兩週的工作營中，接受了音樂劇相關的專業訓練。\[3\]

楊乃文在就讀[雪梨大學期間](../Page/雪梨大學.md "wikilink")，主修[遺傳學與](../Page/遺傳學.md "wikilink")[生物學](../Page/生物學.md "wikilink")。由於對家鄉台灣的想念，她在大一學期結束時決定暫時休學一年回到台灣，找尋自己未來可能的發展機會。剛回台灣時，並沒有確定的目標或方向，於是經常到[酒館](../Page/酒館.md "wikilink")（pub）或[迪斯可舞廳](../Page/迪斯可舞廳.md "wikilink")（[discothèque](../Page/:en:discothèque.md "wikilink")）聽音樂，並打工和擔任兼職[模特兒以賺取生活費](../Page/模特兒.md "wikilink")。就在這段期間，楊乃文結識了[李雨寰](../Page/李雨寰.md "wikilink")、[林暐哲](../Page/林暐哲.md "wikilink")、[黃大煒](../Page/黃大煒.md "wikilink")、[黃舒駿等音樂業界人士](../Page/黃舒駿.md "wikilink")。當時她和李雨寰和林暐哲合作，在「[dMDM](../Page/dMDM.md "wikilink")」\[4\]的專輯《[愛上你只是我的錯](../Page/愛上你只是我的錯.md "wikilink")》中演唱了「愛上你只是我的錯」、「就要天堂」、「再見離別」以及「藍色森林」等四首歌曲\[5\]。

### 1997年：One

一年後，楊乃文回到澳洲繼續完成學業，當時[唱片製作人林暐哲認為楊乃文會是一個有潛力的歌手](../Page/唱片製作人.md "wikilink")，因此他開始和楊乃文討論製作[個人專輯的計劃](../Page/音樂專輯.md "wikilink")，並和[魔岩唱片簽約](../Page/魔岩唱片.md "wikilink")。1996年，楊乃文正式完成大學學業，並回到台灣進行個人專輯的準備和錄音工作。1997年7月29日，楊乃文的首張個人專輯《[One](../Page/One_\(楊乃文專輯\).md "wikilink")》推出\[6\]。在未有大量宣傳的情況下，銷售量達竟數萬張以上，與當時國語專輯動輒破百萬張的年代相比，此成績並不特別好，但是在非主流音樂市場中成績斐然\[7\]。

### 1999年：Silence

距離上一張專輯兩年後的1999年9月9日，楊乃文推出了第二張專輯《[Silence](../Page/Silence.md "wikilink")》，製作人同樣是林暐哲。在這張專輯中提供創作的音樂人包括[張震嶽](../Page/張震嶽.md "wikilink")、[中國大陸的歌手](../Page/中國大陸.md "wikilink")[高旗](../Page/高旗.md "wikilink")、[花兒樂隊](../Page/花兒樂隊.md "wikilink")，以及擅長電子曲風的[陳珊妮](../Page/陳珊妮.md "wikilink")。此外，楊乃文也翻唱了英國樂團「[流行尖端](../Page/流行尖端.md "wikilink")」（[Depeche
Mode](../Page/:en:Depeche_Mode.md "wikilink")）主唱[馬汀·高爾](../Page/馬汀·高爾.md "wikilink")（[Martin
Gore](../Page/:en:Martin_Gore.md "wikilink")）的〈Somebody〉一曲，由林暐哲填上中文詞。楊乃文在2000年憑著《Silence》專輯獲得[金曲獎最佳國語女演唱人獎](../Page/金曲獎.md "wikilink")\[8\]。

### 2001年：應該

2001年5月3日，楊乃文推出了第三張個人專輯《[應該](../Page/應該.md "wikilink")》。和前兩張相同，製作人是林暐哲，專輯中的創作集結了許多音樂人，例如陳珊妮、李雨寰、[陳綺貞](../Page/陳綺貞.md "wikilink")、[李焯雄等](../Page/李焯雄.md "wikilink")，也收錄了兩首楊乃文的個人作品—〈我們〉和〈SURRENDER〉。憑藉著目前為止三張專輯累積出的知名度，楊乃文於2001年7月7日在[台北市](../Page/台北市.md "wikilink")[台大綜合體育館舉辦了名為](../Page/台大綜合體育館.md "wikilink")「Back
to Music」的首次個人售票演唱會。《應該》也獲選為中華音樂人交流協會評定的「年度十大專輯」之一。

### 2004年：楊乃文第一張精選2CD

除了六年三張專輯的精選曲目，更收錄在發片前的首度曝光DMDM專輯中，楊乃文獨唱的『愛上你只是我的錯』，更有在校園Live演唱會中首度發表自己的詞曲創作『FEAR』，還有為了電視原聲帶薰衣草所開口唱的經典電影第凡內的早餐的主題曲『Moon
River』。

### 2006年：女爵

楊乃文和[魔岩唱片之間的合約到期](../Page/魔岩唱片.md "wikilink")，她轉到新東家[亞神音樂](../Page/亞神音樂.md "wikilink")。在上一張唱片發行的五年後，楊乃文的第四張專輯《[女爵](../Page/女爵.md "wikilink")》於2006年12月1日發行。和前三張專輯不同的是，這次楊乃文親自擔任專輯製作人。她也和以前的音樂夥伴再度合作，包括[賈敏恕](../Page/賈敏恕.md "wikilink")、李雨寰、林暐哲、陳珊妮等。此外，繼在《Silence》專輯中翻唱中國大陸花兒樂隊的〈靜止〉之後，這次楊乃文再度翻唱大陸樂團作品—[便利商店樂隊的](../Page/便利商店樂隊.md "wikilink")〈電視機〉。專輯的同名主打單曲〈女爵〉是由台灣[蘇打綠樂團主唱](../Page/蘇打綠.md "wikilink")[吳青峰創作](../Page/吳青峰.md "wikilink")，這是兩人首次合作。

### 2009年：Self-Selected我自選

英文歌是楊乃文的音樂源頭，楊乃文特別從她最喜歡的歌曲中精選10首，加上[德國獨立樂手](../Page/德國.md "wikilink")
Maximilian Hecker為乃文寫的（ Miss
Underwater），自選了Radiohead、Coldplay、Blur、U2、The Verve;
Leonard Cohen（或後來翻唱他的”Hallelujah”的Jeff Buckley）、 The Velvet Underground（
Andy Warhol） 、Lou Reed; 007金剛鑽主題曲（Diamonds Are
Forever）、2007奧斯卡最佳電影主題曲（Once）（《曾經，愛是唯一》）的（Falling
Slowly） 、 被T Rex同名歌曲（20th Century
Boy）感召而繪成的日本經典動漫《20世紀少年》一一浮現了楊乃文以至一世代人們最愛的樂團、傳奇的人物、Cult
Movies（被膜拜的／邪典電影）、共同的文化符號、共通的情感，不是一般的老歌翻唱。

### 2013年：ZERO

2013年12月，楊乃文在暌違樂壇七年之後，推出個人第五張全新專輯《[ZERO](../Page/ZERO_\(楊乃文專輯\).md "wikilink")》，專輯以個人擅長的搖滾曲風為主。專輯製作群包括[亂彈阿翔](../Page/亂彈阿翔.md "wikilink")、[張震嶽](../Page/張震嶽.md "wikilink")、[范曉萱](../Page/范曉萱.md "wikilink")、[徐佳瑩等人](../Page/徐佳瑩.md "wikilink")\[9\]，直至此張專輯，楊乃文才舉行個人出道後的首次簽名會。

2014年，楊乃文再度以此張專輯入圍[第25屆金曲獎最佳國語女歌手獎](../Page/第25屆金曲獎.md "wikilink")，由張震嶽作曲的專輯單曲〈未接來電〉入圍最佳作曲人獎。

### 2016年：離心力

這是楊乃文第一次參與專輯所有環節的製作，包括選歌、編曲、製作、混音皆親力親為。其中專輯的同名主打歌〈離心力〉被稱為是出道以來口氣最「輕」的一首作品，整首歌曲語調平淡，旋律也沒有太大波動。對於過去都以搖滾歌曲當作主打歌的楊乃文而言，這次可以說是反其道而行。〈離心力〉一曲廣受好評，甚至被譽為是「2016年催淚神曲」之一。

2017年，楊乃文以此張專輯叩關[第28屆金曲獎](../Page/第28屆金曲獎.md "wikilink")，並入圍了最佳國語女歌手、年度歌曲、最佳單曲製作人及最佳作曲人獎。

### 2018年：蒙面唱將猜猜猜

2019年，楊乃文化身「花粉過敏的花房姑娘」，參加江蘇衛視第一季《蒙面唱將猜猜猜》。

### 2019：歌手2019

2019年，楊乃文以補位歌手之姿加盟《歌手2019》演唱《女爵》，《推開世界的門》，《證據》等作品。

## 作品

### 錄音室專輯


{| class="wikitable" \!發行順序 \! 資訊 |- |1
|***[One](../Page/One_\(楊乃文專輯\).md "wikilink")***

  - 發行日期：1997年7月29日
  - 發行公司：[魔岩唱片](../Page/魔岩唱片.md "wikilink")

|- |2 |***[Silence](../Page/Silence_\(楊乃文專輯\).md "wikilink")***

  - 發行日期：1999年9月9日
  - 發行公司：[魔岩唱片](../Page/魔岩唱片.md "wikilink")

|- |3 |**[應該](../Page/應該.md "wikilink")**

  - 發行日期：2001年5月3日
  - 發行公司：[魔岩唱片](../Page/魔岩唱片.md "wikilink")

|- |4 |**[女爵](../Page/女爵_\(楊乃文\).md "wikilink")**

  - 發行日期：2006年12月1日
  - 發行公司：[亞神音樂](../Page/亞神音樂.md "wikilink")

|- |5 |**[Self-Selected 我自選](../Page/Self-Selected_我自選.md "wikilink")**

  - 發行日期：2009年4月10日
  - 發行公司：[新力音樂](../Page/新力音樂.md "wikilink")

|- |6 |**[ZERO](../Page/ZERO_\(楊乃文專輯\).md "wikilink")**

  - 發行日期：2013年12月20日

  - 發行公司：[亞神音樂](../Page/亞神音樂.md "wikilink")

  -
|- |7 |**[離心力](../Page/離心力_\(楊乃文專輯\).md "wikilink")**

  - 發行日期：2016年11月16日

  - 發行公司：[亞神音樂](../Page/亞神音樂.md "wikilink")

  -
|}

### 現場錄音專輯

<table>
<thead>
<tr class="header">
<th><p>專輯資訊</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong><a href="../Page/楊乃文_TIMEQUAKE_演唱會現場實錄_2CD.md" title="wikilink">楊乃文 TIMEQUAKE 演唱會現場實錄 2CD</a></strong></p>
<ul>
<li>發行日期：2015年6月5日</li>
<li>發行公司：亞神音樂</li>
</ul></td>
</tr>
</tbody>
</table>

### 精選輯

<table>
<thead>
<tr class="header">
<th><p>專輯資訊</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong><a href="../Page/楊乃文_第一張精選.md" title="wikilink">楊乃文 第一張精選</a></strong></p>
<ul>
<li>發行日期：2004年1月9日</li>
<li>發行公司：滾石唱片</li>
</ul></td>
</tr>
</tbody>
</table>

### 其他／影像作品

  - 1994年：[L.A Boyz](../Page/L.A_Boyz.md "wikilink")《夏日戀曲》（MV）
  - 1996年：dMDM專輯《愛上你只是我的錯》收錄《愛上你只是我的錯》(acoustic)、《再見離別》
  - 1999年：《靜止》（MV + 卡拉OK VCD）
  - 2002年：張震嶽《雙手插口袋》（MV）
  - 2003年：《薰衣草-電視原聲帶》收錄《Moon River》
  - 2004年：《第一張精選》（MV + 卡拉OK DVD）
  - 2007年：《幾米幸運兒-音樂劇原聲專輯》收錄《貝阿提絲》
  - 2008年：黃立行《黑夜盡頭》（MV）
  - 2008年：INORAN專輯《Shadow》收錄《Journey》
  - 2009年：Benq廣告配唱《True Colors》
  - 2010年：柯有倫專輯《無畏無懼》收錄對唱曲《我恨你》
  - 2012年：《愛-電影原聲帶》收錄《放輕點》

### 電影／舞台劇

  - 2000年：《175度色盲》（網路電影）
  - 2005年9月：[幾米](../Page/幾米.md "wikilink")《幸運兒》音樂舞台劇（飾演女主角：貝阿提絲）

## 演唱會

  - 1997年：誕生前夕校園巡迴演唱會
  - 1997年：秋季魔岩校園巡迴演唱會
  - 1997年：春光乍現校園巡迴演唱會
  - 1997年：伍佰& China Blue演唱會（演唱嘉賓）
  - 1998年：MONSTER LIVE校園巡迴演唱會
  - 1999年：OPEN UP校園巡迴演唱會
  - 1999年10月：打破沈默街頭義演
  - 1999年11月：「我給的愛」預唱會
  - 2000年：魔岩春季校園巡迴演唱會
  - 2001年6月29日：HK HP 演唱會
  - 2001年7月7日：《Back to Music》
    售票演唱會（[台北市](../Page/台北市.md "wikilink")[台大綜合體育館](../Page/台大綜合體育館.md "wikilink")）
  - 2001年8月：HK 添馬艦演唱會
  - 2002年：伍佰& China Blue《九重天演唱會》（演唱嘉賓）
  - 2006年2月：香港The Neo Concert演唱會
  - 2006年5月：幾米迷宮天使概念演唱會
    （[台北市](../Page/台北市.md "wikilink")[台北國際會議中心](../Page/台北國際會議中心.md "wikilink")）
  - 2006年7月15日、9月2日：Johnnie-Walker 黑次元演唱會
  - 2006年11月21日：新專輯快閃搶聽演唱會（[台北市](../Page/台北市.md "wikilink")[這牆音樂藝文展演空間](../Page/這牆音樂藝文展演空間.md "wikilink")）
  - 2006年12月5日：第一屆簡單生活節（受邀演唱）（[台北市](../Page/台北市.md "wikilink")[華山藝文特區](../Page/華山藝文特區.md "wikilink")）
  - 2006年12月31日：台北市政府跨年演唱會（受邀演唱）（[台北市](../Page/台北市.md "wikilink")[市府廣場](../Page/市府廣場.md "wikilink")）
  - 2007年2月11日：Be In Love with 楊乃文數位演唱會 Champagne 3（松德路171號 B1）
  - 2007年4月8日：墾丁春天吶喊（受邀演唱）
  - 2007年4月14日：樂多女爵北京演唱會（售票）（[北京](../Page/北京.md "wikilink")[星光現場](../Page/星光現場.md "wikilink")）
  - 2007年5月6日：第二屆台客搖滾嘉年華（受邀演唱）（[台中市](../Page/台中市.md "wikilink")[大都會戲劇院預定地](../Page/大都會戲劇院預定地.md "wikilink")）
  - 2007年7月13日：白色海洋音樂祭（受邀演唱）（[台南市](../Page/台南市.md "wikilink")）
  - 2007年8月26日：OKGO衝浪音樂節演唱會（[臺北縣](../Page/新北市.md "wikilink")[金山鄉](../Page/金山區_\(新北市\).md "wikilink")）
  - 2007年9月15日：《中國成都市百威啤酒晚會活動》演唱會
  - 2007年10月12日：台北國慶晚會（[台北市](../Page/台北市.md "wikilink")[中正紀念堂](../Page/中正紀念堂.md "wikilink")）
  - 2007年11月12日：Johnnie Walker 「 黑牌 」中國之旅上海站 演唱會
  - 2007年11月25日：「行政院新聞局補助樂團錄製有聲出版品成果發表會」演唱會
  - 2007年12月1日：《Nokia》
    音樂讓我說演唱會（受邀演唱）（[台北市](../Page/台北市.md "wikilink")[台大綜合體育館](../Page/台大綜合體育館.md "wikilink")）
  - 2007年12月15日：伍佰 & China
    Blue《妳是我的花朵演唱會》（演唱嘉賓）（[台北市](../Page/台北市.md "wikilink")[台北小巨蛋](../Page/台北小巨蛋.md "wikilink")）
  - 2007年12月31日：高雄市政府港都跨年煙火演唱會（受邀演唱）（[高雄市](../Page/高雄市.md "wikilink")[高雄港碼頭](../Page/高雄港碼頭.md "wikilink")）
  - 2008年1月12日：香港Wlid Day Out演唱會
  - 2008年1月18日：MINI
    CONCERT售票演唱會（[台北市](../Page/台北市.md "wikilink")[這牆音樂藝文展演空間](../Page/這牆音樂藝文展演空間.md "wikilink")）
  - 2008年4月5日：墾丁春天吶喊（第二度受邀演唱）
  - 2008年7月13日：貢寮海洋音樂祭（受邀演唱）
  - 2008年8月23日：張震嶽 n FREE 9
    演唱會（演唱嘉賓）（[台北市](../Page/台北市.md "wikilink")[南港101](../Page/南港101.md "wikilink")）
  - 2008年10月2日：Samsung《 Live is
    Life》演唱會（[香港](../Page/香港.md "wikilink")[機場國際會議廳](../Page/機場國際會議廳.md "wikilink")）
  - 2008年10月10日：嘉義國慶晚會（[嘉義市](../Page/嘉義市.md "wikilink")[嘉義體育館](../Page/嘉義體育館.md "wikilink")）
  - 2008年12月6日：第二屆簡單生活節（Jarvis
    演唱嘉賓）（[台北市](../Page/台北市.md "wikilink")[華山藝文特區](../Page/華山藝文特區.md "wikilink")）
  - 2008年11月21日：楊乃文 Selected
    2008售票演唱會（[台北市](../Page/台北市.md "wikilink")[這牆音樂藝文展演空間](../Page/這牆音樂藝文展演空間.md "wikilink")）
  - 2008年12月20日：楊乃文 Selected
    2008售票演唱會（[台北市](../Page/台北市.md "wikilink")[這牆音樂藝文展演空間](../Page/這牆音樂藝文展演空間.md "wikilink")）
  - 2008年12月30日：Inoran日本演唱會（演唱嘉賓）
  - 2009年6月13日：Have Faith\!
    楊乃文2009演唱會（[台北市](../Page/台北市.md "wikilink")[台北小巨蛋](../Page/台北小巨蛋.md "wikilink")）
  - 2010年4月24日：音躍潮拜：義大搖滾之夜售票演唱會（受邀演唱）（[高雄市](../Page/高雄市.md "wikilink")[義大世界](../Page/義大世界.md "wikilink")）
  - 2010年11月27日：快樂天堂，滾石30演唱會（受邀演唱）（[台北市](../Page/台北市.md "wikilink")[台北小巨蛋](../Page/台北小巨蛋.md "wikilink")）
  - 2010年12月31日：三天兩夜一起High台中跨年演唱會（受邀演唱）（[台中市](../Page/台中市.md "wikilink")[台中體育館](../Page/台中體育館.md "wikilink")）
  - 2011年2月：Las Vegas Wynn Hotel商演
  - 2011年2月18日：柯有倫無畏無懼演唱會（演唱嘉賓）（[台北市](../Page/台北市.md "wikilink")[傳音樂展演空間](../Page/傳音樂展演空間.md "wikilink")）
  - 2011年5月1日：快樂天堂，滾石30演唱會（受邀演唱）（[北京](../Page/北京.md "wikilink")[鳥巢](../Page/鳥巢.md "wikilink")）
  - 2011年5月21日：賓士不冷靜之夜（受邀演唱）（[台北市](../Page/台北市.md "wikilink")[台北小巨蛋](../Page/台北小巨蛋.md "wikilink")）
  - 2011年10月5日：快樂天堂，滾石30演唱會（受邀演唱）（[上海](../Page/上海.md "wikilink")[世博文化中心](../Page/世博文化中心.md "wikilink")）
  - 2011年10月10日：大彩虹音樂節（受邀演唱）（[高雄市](../Page/高雄市.md "wikilink")[真愛碼頭](../Page/真愛碼頭.md "wikilink")）
  - 2011年11月2日：第二屆金音獎頒獎典禮（受邀演唱）（[台北市](../Page/台北市.md "wikilink")[南港101](../Page/南港101.md "wikilink")）
  - 2011年12月18日：快樂天堂，滾石30演唱會（受邀演唱）（[深圳](../Page/深圳.md "wikilink")）
  - 2011年12月31日：張宇二十年仰望個人演唱會（演唱嘉賓）（[北京](../Page/北京.md "wikilink")[首都體育館](../Page/首都體育館.md "wikilink")）
  - 2012年3月24日：Monkey pilot 猴子飛行員 BIG CHILD
    專輯首賣演唱會（演唱嘉賓）（[台北市](../Page/台北市.md "wikilink")[西門大河岸](../Page/西門大河岸.md "wikilink")）
  - 2012年4月29日：瑪克思大爬梯（受邀演唱）（[北京](../Page/北京.md "wikilink")）
  - 2013年8月17日：【MTV X SPORT B. PLUGGED LIVE CONCERT
    2013】（受邀演唱）（[台北市](../Page/台北市.md "wikilink")）
  - 2013年8月18日：無懈可擊亞洲巔峰音樂節 Sonic Shanghai
    2013（受邀演唱）（[上海](../Page/上海.md "wikilink")）
  - 2013年8月30日：楊乃文 Fearless 2013售票演唱會
    （[台北市](../Page/台北市.md "wikilink")【Legacy Taipei 傳】
    音樂展演空間）
  - 2013年8月31日：楊乃文 Fearless 2013售票演唱會 （台北市【Legacy Taipei 傳】 音樂展演空間）
  - 2014年1月4日：楊乃文ZERO個人售票演唱會（ATT SHOW BOX）
  - 2014年1月11日：楊乃文ZERO個人售票演唱會（高雄駁二The Wall）
  - 2014年5月18日：楊乃文ZERO Mini Live音乐会（上海MAO Live House）
  - 2014年8月9日：楊乃文ZERO Mini Live音乐会（北京糖果TANGO）
  - 2014年8月23日：[超犀利趴](../Page/超犀利趴.md "wikilink")5〈犀利好大趴〉演唱會（受邀演唱）（[台北市](../Page/台北市.md "wikilink")[台北小巨蛋](../Page/台北小巨蛋.md "wikilink")）
  - 2014年9月20日：楊乃文ZERO Mini Live音乐会（长沙红咖俱乐部）
  - 2014年10月12日：楊乃文ZERO Mini Live音乐会（成都AMC Live House）
  - 2014年12月20日：楊乃文 TIMEQUAKE 演唱會 (台北南港展覽館)
  - 2015年5月2日：楊乃文U-Music北京音乐会（北京国家图书馆艺术中心）
  - 2015年7月18日：楊乃文『BACK TO LIVE』 Legacy演唱會(Legacy Taipei 音樂展演空間)
  - 2015年10月8日：楊乃文『BACK TO LIVE』 Legacy演唱會(Legacy Taichung 傳 音樂展演空間)

## 資料來源

<div class="references-small">

<references />

</div>

## 外部連結

  - [楊乃文官方網站（2006－2008年）](http://www.faithyang.com/)

  - [楊乃文官方網站（2009年至今）](https://web.archive.org/web/20090325182211/http://www.yangnaiwen.com/)

  - （2011年至今）

  -
  -
[Category:台灣女歌手](../Category/台灣女歌手.md "wikilink")
[Category:華語流行音樂歌手](../Category/華語流行音樂歌手.md "wikilink")
[Category:雪梨大學校友](../Category/雪梨大學校友.md "wikilink")
[Category:澳大利亞華人](../Category/澳大利亞華人.md "wikilink")
[Category:台北市人](../Category/台北市人.md "wikilink")
[N乃](../Category/楊姓.md "wikilink")
[Category:台灣作詞家](../Category/台灣作詞家.md "wikilink")

1.  [G-Music
    藝人基本資料網頁](http://www.g-music.com.tw/GMusicSinger.aspx?Param1=48&Param2=2A-82-7C-83-4F-86-94-44-67-62-B1-69-05-FF-E3-55)，2007年5月30日引用

2.  1999年10月1日，陳雅雯，{{〈}}[全台灣脾氣最大的女歌手：楊乃文](http://publish.pots.com.tw/Chinese//CoverStory/1999/10/01/OldData434/)
    {{〉}}，《[破報](../Page/破報.md "wikilink")》，2007年5月30日引用

3.
4.  dMDM 官方念做「惦惦」（拼音：*dian
    dian*，音同「電電」意），為[閩南語中](../Page/閩南語.md "wikilink")「安靜」之意。參見：[KKBOX
    介紹頁面](http://www.kkbox.com.tw/funky/web_info/mWQNsHaRRFeTGLU0002vM08J.html)


5.
6.
7.
8.  [第十一屆金曲獎得獎名單](http://www.gio.gov.tw/info/publish/2000/890432.htm)
    ，台灣行政院新聞局，2007年5月31日引用

9.  [楊乃文　無法靜止 Faith Yang　BREAK THE
    SILENCE](http://mag.nownews.com/article.php?mag=2-100-20126&page=1)