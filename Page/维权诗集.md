《**维权诗集**》是一本“记载中国维权历史、特别是新世纪以来维权运动的诗集，收录了众多诗人反映人们对权利的觉悟和奋起捍卫权利的诗作”。书中描述“由于目前中国正处于专制向民主社会转型的前夜，百姓人权遭受政治践踏几乎就是中国现实的常态，因此对权利的觉悟和对人权的捍卫就是对中国专制整体的威胁与进攻”，特别是本集出版发行在北京举办奥运之前，中国政府担心诗集的流传会扰乱他们需要的和谐\[1\]，为此本诗集尚在编辑过程中就如《[六四诗集](../Page/六四诗集.md "wikilink")》一样触怒了中国当局，而在内地被下令查堵。

《维权诗集》由美国六四文化传播协会（June Fourth Heritage & Culture
Association）编辑出版，主编为《六四诗集(Collection of June
Fourth Poems)》主编[蒋品超](../Page/蒋品超.md "wikilink")，策划为美国宗教自由最高奖
“2007年度约翰·李德兰宗教自由奖(ERLC’s Religious Liberty Award) ”
得主[傅希秋](../Page/傅希秋.md "wikilink")。
编委有六四学生领袖[周锋锁](../Page/周锋锁.md "wikilink")，[陶君以及异议作家](../Page/陶君.md "wikilink")[焦国标](../Page/焦国标.md "wikilink")，[杜导斌等](../Page/杜导斌.md "wikilink")。

顾问有美国国会图书馆[克鲁格人文与社会科学终身成就奖](../Page/克鲁格人文与社会科学终身成就奖.md "wikilink")(Kluge
Prize rewards lifetime
achievement)得主华裔学者[余英時](../Page/余英時.md "wikilink")，2007年雅虎人权案原告律师Morton
Sklar，香港民主党主席[何俊仁](../Page/何俊仁.md "wikilink")，香港《开放》杂志主编[金钟](../Page/金钟.md "wikilink")，被中国政府判刑3年半的中国维权人士[胡佳](../Page/胡佳.md "wikilink")，美国普林斯顿中国学社执行主席、《观察》网站主编[陈奎德](../Page/陈奎德.md "wikilink")，台湾中华大学行政管理学系助理教授、台湾教授协会法政组召集人[曾建元](../Page/曾建元.md "wikilink")，美国民主教育基金会前会长[蒋亨兰](../Page/蒋亨兰.md "wikilink")，香港笔会前会长、香港政府文学委员会前主席、艺术发展局委员[胡志伟](../Page/胡志伟.md "wikilink")，美国第十四大上市公司UnitedHealthCare市场部总监Amber
Jia。

编顾委邀请在太石村选举事件中的中国维权人士[郭飞雄任荣誉主编](../Page/郭飞雄.md "wikilink")。

诗集由[香港市民支援爱国民主运动联合会](../Page/香港市民支援爱国民主运动联合会.md "wikilink")(Hong Kong
Alliance In Support Of Patriotic Democratic Movements Of
China)主席[司徒华题写书名](../Page/司徒华.md "wikilink")，由异议学者[焦国标撰写序言](../Page/焦国标.md "wikilink")。
内容主要由五部分组成，分别是: 民主思潮、民生思潮、维权事件、维权人物、维权历史。

## 维权诗集电子书与网络版

1.  《维权诗集》电子书由美国《民主论坛》总编洪哲胜制作：[1](https://web.archive.org/web/20110718163041/http://asiademo.org/gb/news/2008/20080510_jiangpinchao_wqpoems.htm)
2.  《维权诗集》网络版由大纪元报业集团与《维权诗集》编委会共同推出：[2](http://weiquanshiji.blog.epochtimes.com)

## 维权诗集相关报道

1.
2.
3.
4.
## 书评

《亚洲周刊》江迅：维权诗三百颂扬维权运动
[3](http://www.yzzk.com/cfm/Content_Archive.cfm?Channel=as&Path=241229811/23as1a.cfm)

《明报》梁慧思：新世代私房书：维权人士血泪史
[4](http://news.sina.com.hk/cgi-bin/nw/show.cgi/229/3/1/756995/1.html)

美国自由亚洲电台：邓小桦书评《维权诗集》
[5](http://www.rfa.org/cantonese/features/bookclub/rights-06202008140828.html?encoding=simplified)

## 參考文獻

[议](../Category/中文書籍.md "wikilink")
[Category:中國維權運動](../Category/中國維權運動.md "wikilink")
[议](../Category/中华人民共和国诗集.md "wikilink")

1.  \[<http://www.yzzk.com/cfm/Content_Archive.cfm?Channel=as&Path=241229811/23as1a.cfm2008年6月3日《亚洲周刊·书籍与文学》江迅《维权诗三百颂扬维权运动>》\]