**瞬烯**（[化学式](../Page/化学式.md "wikilink")：C<sub>10</sub>H<sub>10</sub>）是一个[碳氢化合物](../Page/碳氢化合物.md "wikilink")。瞬烯是[流变分子之一](../Page/流变分子.md "wikilink")，其分子内所有的[化学键持续不断地发生着](../Page/化学键.md "wikilink")[Cope重排](../Page/科普重排反应.md "wikilink")，从而存在多个[价互变异构体](../Page/互变异构.md "wikilink")。

  -
    [Bullvalene_rearrangements.png](https://zh.wikipedia.org/wiki/File:Bullvalene_rearrangements.png "fig:Bullvalene_rearrangements.png")

分子内的重排，使分子的所有碳原子和氢原子都是等同的，没有一条共价键是固定不变的。重排可能产物多达10\!/3 =
1,209,600个，下图仅列出其中的五个。瞬烯在室温下的[质子NMR谱有一个宽峰](../Page/核磁共振.md "wikilink")，表明可以分辨出分子中不同的氢原子。但120°C时，瞬烯的<sup>1</sup>H
NMR谱就只有4.2ppm的一个吸收峰，说明质子交换速度超过机器的时间间隔，无法被分辨出来（[海森堡不确定原理](../Page/海森堡不确定原理.md "wikilink")）。

瞬烯首先由[G.
Schröder于](../Page/G._Schröder.md "wikilink")1963年通过二聚[环辛四烯光解放出](../Page/环辛四烯.md "wikilink")[苯得到](../Page/苯.md "wikilink")。

## 参考文献

[Category:环烯烃](../Category/环烯烃.md "wikilink")
[Category:桥环化合物](../Category/桥环化合物.md "wikilink")