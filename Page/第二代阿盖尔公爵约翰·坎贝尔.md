**约翰·坎贝尔**（，），第二代**阿蓋爾公爵**（）和第一代**格林威治公爵**（），[蘇格蘭军人和贵族](../Page/蘇格蘭.md "wikilink")。

阿盖尔公爵第二出生于[萨里的Petersham](../Page/萨里.md "wikilink")，1703年繼承**[阿盖尔公爵](../Page/阿盖尔公爵.md "wikilink")**爵位和坎贝尔家族的领袖，1705年，因他曾積極推動[英格蘭和蘇格蘭的聯合](../Page/英格蘭.md "wikilink")，支持联合法案，成為英格蘭貴族，被授于**查塔姆男爵**和**格林威治伯爵**，曾參加[西班牙王位繼承战争](../Page/西班牙王位繼承战争.md "wikilink")。

1710年他被授[嘉德骑士勋章](../Page/嘉德勳章.md "wikilink")，1736年他授于[英国陆军元帅军衔](../Page/英国陆军元帅.md "wikilink")。在他去世前一年，他任英国总司令职位。
 [A](../Page/category:蘇格蘭公爵.md "wikilink")
[A](../Page/category:英國陸軍元帥.md "wikilink")

[A](../Category/苏格兰人.md "wikilink") [A](../Category/嘉德騎士.md "wikilink")