**張草**（），本名**張容嵻**，[小說家](../Page/小說家_\(職業\).md "wikilink")，生於[馬來西亞](../Page/馬來西亞.md "wikilink")[沙巴州首府](../Page/沙巴州.md "wikilink")[亞庇](../Page/亞庇.md "wikilink")（Kota
Kinabalu），馬來西亞華僑第三代，祖籍[廣東](../Page/廣東.md "wikilink")[南海](../Page/南海區.md "wikilink")。

## 簡歷

  - 1998年畢業於[國立臺灣大學牙醫學系](../Page/國立臺灣大學.md "wikilink")。
  - 最早出版的作品是1998年的《雲空行》系列，以南北[宋期間一位道士雲遊天下的傳奇](../Page/宋朝.md "wikilink")，內容涵蓋歷史、科幻、奇幻等元素。
  - 1999年以[科幻小說](../Page/科幻小說.md "wikilink")《北京滅亡》（Ruin of
    Beijing）獲「第三屆[皇冠大眾小說獎](../Page/皇冠大眾小說獎.md "wikilink")」首獎，其後並出版《諸神滅亡》（Ruin
    of Gods）及《明日滅亡》（Ruin of Tomorrow），構成「滅亡三部曲」（Ruin Trilogy）。
  - 2000年在[法鼓山](../Page/法鼓山.md "wikilink")[農禪寺正式皈依三寶](../Page/農禪寺.md "wikilink")，法名常運。同年與妻子共偕連理。

## 作品列表

| 作品列表        |
| ----------- |
| 出版日期        |
| 1998年-2000年 |
| 1999年       |
| 2000年       |
| 2001年       |
| 2003年       |
| 2004年       |
| 2005年       |
| 2007年       |
| 2010年-2014年 |
| 2016年8月15日  |
| 2017年2月6日   |
| 2017年10月16日 |

## 外部連結

  - [個人網頁](https://web.archive.org/web/20160304195106/http://reocities.com/Tokyo/Harbor/9651/)
  - [個人部落格](http://zhangcao72.pixnet.net/blog/post/252021985)

[Z](../Category/國立臺灣大學醫學院校友.md "wikilink")
[Z](../Category/科幻小說作家.md "wikilink")
[Category:馬來西亞作家](../Category/馬來西亞作家.md "wikilink")
[Z](../Category/馬來西亞小說家.md "wikilink")
[Z](../Category/馬來西亞華人.md "wikilink")
[Z](../Category/在台灣的馬來西亞人.md "wikilink")
[Z](../Category/马来西亚广府人.md "wikilink")