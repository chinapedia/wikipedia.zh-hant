**適應度**（），又可稱**適存度**或**生殖成就**，是[生物學](../Page/生物學.md "wikilink")，特別是[群體遺傳學](../Page/群體遺傳學.md "wikilink")、[數理生物學中用來描述擁有某一特定](../Page/數理生物學.md "wikilink")[基因型的](../Page/基因型.md "wikilink")[個體](../Page/個體.md "wikilink")，在[繁殖上的成功率或能力](../Page/繁殖.md "wikilink")。假如帶有不同基因型的個體擁有不同的適應度，那麼這些基因型的比例將會在世代交替之後有所變動。而造成這種比例變動的機制是[自然選擇](../Page/自然選擇.md "wikilink")（天擇）。

## 參考文獻

  - Haldane, J.B.S. (1924) "A mathematical theory of natural and
    artificial selection" Part 1 *Transactions of the Cambridge
    philosophical society*: 23: 19-41 [link (pdf
    file)](http://www.blackwellpublishing.com/ridley/classictexts/haldane1.pdf)
  - Hamilton, W.D. (1964) "The evolution of social behavior" *Journal of
    Theoretical Biology* 1:...
  - Hartl, D. L. A Primer of Population Genetics. Sinauer, Sunderland,
    Massachusetts, 1981.
  - Maynard Smith, J. Evolutionary Genetics. Oxford University Press,
    1998.

## 延伸閱讀

  - Sober, E. (2001). The Two Faces of Fitness. In R. Singh, D. Paul, C.
    Krimbas, and J. Beatty (Eds.), *Thinking about Evolution:
    Historical, Philosophical, and Political Perspectives*. Cambridge
    University Press, pp.309-321. [Full
    text](http://philosophy.wisc.edu/sober/tff.pdf)

## 參見

  - [基因中心演化觀點](../Page/基因中心演化觀點.md "wikilink")
  - [整體適應度](../Page/整體適應度.md "wikilink")
  - [自然選擇](../Page/自然選擇.md "wikilink")
  - [生殖成功率](../Page/生殖成功率.md "wikilink")
  - [適應](../Page/適應.md "wikilink")

## 外部連結

  - [Evolution A-Z:
    Fitness](http://www.blackwellpublishing.com/ridley/a-z/Fitness.asp)
  - [Stanford Encyclopedia of Philosophy
    entry](http://plato.stanford.edu/entries/fitness/)

[Category:演化生物學](../Category/演化生物學.md "wikilink")
[Category:群體遺傳學](../Category/群體遺傳學.md "wikilink")