**奈尔温度**（），T<sub>N</sub>，指的是[反铁磁性材料转变为](../Page/反铁磁性.md "wikilink")[顺磁性材料所需要达到的](../Page/顺磁性.md "wikilink")[温度](../Page/温度.md "wikilink")。在这个温度的时候，[晶体内部的](../Page/晶体.md "wikilink")[原子](../Page/原子.md "wikilink")[内能会大到足以破坏材料内部](../Page/内能.md "wikilink")[宏观](../Page/宏观.md "wikilink")[磁性排列](../Page/磁性.md "wikilink")，从而发生[相变](../Page/相变.md "wikilink")，由反铁磁性转变为顺磁性。\[1\]

奈尔温度可类比於[居-{里}-温度T](../Page/居里温度.md "wikilink")<sub>C</sub>（相对于[铁磁性而言](../Page/铁磁性.md "wikilink")）。它是因纪念1970年[诺贝尔物理学奖得主](../Page/诺贝尔物理学奖.md "wikilink")、法國物理學者[路易·奈尔](../Page/路易·奈尔.md "wikilink")（1904年-2000年）而得名。

下表為一些物質的奈尔温度：\[2\]

| 物質                           | 奈尔温度 ([K](../Page/克耳文.md "wikilink")) |
| ---------------------------- | ------------------------------------- |
| MnO                          | 116                                   |
| MnS                          | 160                                   |
| MnTe                         | 307                                   |
| MnF<sub>2</sub>              | 67                                    |
| FeF<sub>2</sub>              | 79                                    |
| FeCl<sub>2</sub>             | 24                                    |
| FeI<sub>2</sub>              | 9                                     |
| FeO                          | 198                                   |
| FeOCl                        | 80                                    |
| CoCl<sub>2</sub>             | 25                                    |
| CoI<sub>2</sub>              | 12                                    |
| CoO                          | 291                                   |
| NiCl<sub>2</sub>             | 50                                    |
| NiI<sub>2</sub>              | 75                                    |
| NiO                          | 525                                   |
| Cr                           | 308                                   |
| Cr<sub>2</sub>O<sub>3</sub>  | 307                                   |
| Nd<sub>5</sub>Ge<sub>3</sub> | 50                                    |

## 參閱

[Category:物理小作品](../Category/物理小作品.md "wikilink")
[N](../Category/磁学.md "wikilink") [N](../Category/温度.md "wikilink")
[Category:临界现象](../Category/临界现象.md "wikilink")

1.
2.