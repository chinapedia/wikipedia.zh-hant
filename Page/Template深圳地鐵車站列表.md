|list2 =

|group2 = <small><font color={{深圳地鐵色彩|2}}>█</font></small>
[2號綫](深圳地鐵2號綫.md "wikilink")
（包括以[8號綫名義興建部分](深圳地鐵8號綫.md "wikilink")） |group2style= border-right:solid
5px ; |list2 = （[赤灣](赤灣站_\(地鐵\).md "wikilink") – 已开通段 –
[新秀](新秀站.md "wikilink")）–
[<span style="color: #888;">莲塘口岸</span>](莲塘口岸站.md "wikilink")–
[<span style="color: #888;">仙湖路</span>](仙湖路站.md "wikilink") –
[<span style="color:#888;">蓮塘</span>](蓮塘站_\(深圳\).md "wikilink")-
[<span style="color: #888;">梧桐山南</span>](梧桐山南站.md "wikilink")–
[<span style="color: #888;">沙頭角</span>](沙頭角站（深圳）.md "wikilink") –
[<span style="color: #888;">海山</span>](海山站（深圳）.md "wikilink") –
[<span style="color: #888;">鹽田港</span>](鹽田港站.md "wikilink") –
[<span style="color: #888;">深外</span>](深外站.md "wikilink") –
[<span style="color: #888;">鹽田</span>](鹽田站.md "wikilink") –
[<span style="color: #888;">北山道</span>](北山道站.md "wikilink")–
[<span style="color: #888;">鹽田食街</span>](鹽田食街站.md "wikilink")–
[<span style="color: #888;">大梅沙</span>](大梅沙站.md "wikilink")–
[<span style="color: #888;">小梅沙</span>](小梅沙站.md "wikilink")–
[<span style="color: #888;">溪涌</span>](溪涌站.md "wikilink")

|group3 = <small><font color={{深圳地鐵色彩|3}}>█</font></small>
[3號綫](深圳地鐵3號綫.md "wikilink") |group3style=
border-right:solid 5px ; |list3 =
[<span style="color: #888;">福保</span>](福保站.md "wikilink") –
（[益田](益田站_\(深圳\).md "wikilink") – 已开通段 –
[雙龍](雙龍站_\(深圳\).md "wikilink")）{{-w}}[<span style="color:#888;">梨園</span>](梨園站_\(深圳地鐵\).md "wikilink")
– [<span style="color:#888;">新生</span>](新生站_\(深圳地鐵\).md "wikilink") –
[<span style="color:#888;">坪西</span>](坪西站.md "wikilink") –
[<span style="color:#888;">低碳城</span>](低碳城站.md "wikilink") –
[<span style="color:#888;">白石塘</span>](白石塘站.md "wikilink") –
[<span style="color:#888;">富坪</span>](富坪站_\(深圳地鐵\).md "wikilink") –
[<span style="color:#888;">坪地六联</span>](坪地六联站.md "wikilink")

|group4 = <small><font color={{深圳地鐵色彩|4}}>█</font></small>
[4號綫](深圳地鐵4號綫.md "wikilink") |group4style=
border-right:solid 5px ; |list4 =
[<span style="color:#888;">牛湖</span>](牛湖站.md "wikilink") –
[<span style="color:#888;">觀瀾湖</span>](觀瀾湖站.md "wikilink") –
[<span style="color:#888;">松元廈</span>](松元廈站.md "wikilink") –
[<span style="color:#888;">觀瀾</span>](觀瀾站.md "wikilink") –
[<span style="color:#888;">長湖</span>](長湖站.md "wikilink") –
[<span style="color:#888;">茜坑</span>](茜坑站.md "wikilink") –
[<span style="color:#888;">竹村</span>](竹村站.md "wikilink") –
[<span style="color:#888;">清湖北</span>](清湖北站.md "wikilink")–
（[清湖](清湖站_\(地铁\).md "wikilink") – 已开通段 –
[福田口岸](福田口岸站.md "wikilink")）

|group5 = <small><font color={{深圳地鐵色彩|5}}>█</font></small>
[5號綫](深圳地鐵5號綫.md "wikilink") |group5style=
border-right:solid 5px ; |list5 =
[<span style="color: #888;">郵輪母港</span>](郵輪母港站.md "wikilink") ⇔
[<span style="color: #888;">赤灣</span>](赤灣站_\(地鐵\).md "wikilink") –
[<span style="color: #888;">荔灣</span>](荔灣站.md "wikilink") –
[<span style="color: #888;">鐵路公園</span>](鐵路公園站.md "wikilink") –
[<span style="color: #888;">媽灣</span>](媽灣站.md "wikilink") –
[<span style="color: #888;">前海公園</span>](前海公園站.md "wikilink") –
[<span style="color: #888;">前灣</span>](前灣站.md "wikilink") –
[<span style="color: #888;">桂灣</span>](桂灣站.md "wikilink") –
（[前海灣](前海灣站.md "wikilink") – 已开通段 –
[黃貝嶺](黃貝嶺站.md "wikilink")）–
[<span style="color: #888;">大劇院</span>](大劇院站_\(深圳\).md "wikilink")

|group6 = <small><font color={{深圳地鐵色彩|6}}>█</font></small>
[<span style="color: #888;">6號綫</span>](深圳地鐵6號綫.md "wikilink")
|group6style= border-right:solid 5px ; |list6 =
[<span style="color: #888;">科學館</span>](科學館站.md "wikilink") –
[<span style="color: #888;">通新岭</span>](通新岭站.md "wikilink") –
[<span style="color: #888;">體育中心</span>](體育中心站.md "wikilink") –
[<span style="color: #888;">八卦岭</span>](八卦岭站.md "wikilink") –
[<span style="color: #888;">银湖</span>](银湖站.md "wikilink") –
[<span style="color: #888;">翰嶺</span>](翰嶺站.md "wikilink") –
[<span style="color: #888;">梅林關</span>](梅林關站.md "wikilink") –
[<span style="color: #888;">深圳北站</span>](深圳北站#地铁车站.md "wikilink") –
[<span style="color: #888;">红山</span>](红山站_\(深圳\).md "wikilink") –
[<span style="color: #888;">上塘北</span>](上塘北站.md "wikilink") –
[<span style="color: #888;">元芬</span>](元芬站.md "wikilink") –
[<span style="color: #888;">大浪</span>](大浪站.md "wikilink") –
[<span style="color: #888;">石岩</span>](石岩站.md "wikilink") –
[<span style="color: #888;">上屋北</span>](上屋北站.md "wikilink") –
[<span style="color: #888;">长圳</span>](长圳站.md "wikilink") –
[<span style="color: #888;">觀光</span>](觀光站.md "wikilink") –
[<span style="color: #888;">光明中心</span>](光明中心站.md "wikilink") –
[<span style="color: #888;">翠湖</span>](翠湖站_\(深圳\).md "wikilink") –
[<span style="color: #888;">光明北</span>](光明北站.md "wikilink") –
[<span style="color: #888;">楼村</span>](楼村站.md "wikilink") –
[<span style="color: #888;">南庄</span>](南庄站.md "wikilink") –
[<span style="color: #888;">公明广场</span>](公明广场站.md "wikilink") –
[<span style="color: #888;">合水口</span>](合水口站.md "wikilink") –
[<span style="color: #888;">薯田埔</span>](薯田埔站.md "wikilink") –
[<span style="color: #888;">山門</span>](山門站.md "wikilink") –
[<span style="color: #888;">溪头</span>](溪头站.md "wikilink") –
[<span style="color: #888;">松岗</span>](松岗站.md "wikilink")

|group7 = <small><font color={{深圳地鐵色彩|6}}>█</font></small>
[<span style="color: #888;">6號綫支綫</span>](深圳地鐵6號綫.md "wikilink")[<span style="color: #888;">東莞軌道交通1號綫</span>](東莞軌道交通1號綫.md "wikilink")
|group7style= border-right:solid 5px ; |list7 =
[<span style="color: #888;">光明城</span>](光明城站_\(地鐵\).md "wikilink") –
[<span style="color: #888;">華夏</span>](華夏站.md "wikilink") –
[<span style="color: #888;">光明小鎮</span>](光明小鎮.md "wikilink") –
[<span style="color: #888;">翠湖</span>](翠湖站_\(深圳\).md "wikilink") –
[<span style="color: #888;">圳美</span>](圳美站.md "wikilink") –
[<span style="color: #888;">中大</span>](中大站_\(深圳\).md "wikilink") –
[<span style="color: #888;">科學城東</span>](科學城東站.md "wikilink") ⇔
[<span style="color: #888;">東莞軌道交通1號綫東莞段](東莞軌道交通1號綫.md "wikilink")</span>

|group8 = <small><font color={{深圳地鐵色彩|7}}>█</font></small>
[7號綫](深圳地鐵7號綫.md "wikilink") |group8style=
border-right:solid 5px ; |list8 =
[<span style="color:#888;">學府醫院</span>](學府醫院站.md "wikilink") –
[<span style="color:#888;">北大</span>](北大站.md "wikilink") –
（[西麗湖](西麗湖站.md "wikilink") – 已开通段 –
[太安](太安站.md "wikilink")）

|group9 = <small><font color={{深圳地鐵色彩|9}}>█</font></small>
[9號綫](深圳地鐵9號綫.md "wikilink") |group9style=
border-right:solid 5px ; |list9 =
[<span style="color: #888;">寶安公園</span>](寶安公園站.md "wikilink") ⇔
[<span style="color:#888;">前灣</span>](前灣站.md "wikilink") –
[<span style="color:#888;">夢海</span>](夢海站.md "wikilink") –
[<span style="color:#888;">怡海</span>](怡海站.md "wikilink") –
[<span style="color:#888;">荔林</span>](荔林站.md "wikilink") –
[<span style="color:#888;">南油西</span>](南油西站.md "wikilink") –
[<span style="color:#888;">南油</span>](南油站.md "wikilink") –
[<span style="color:#888;">南山書城</span>](南山書城站.md "wikilink") –
[<span style="color:#888;">深大南</span>](深大南站.md "wikilink") –
[<span style="color:#888;">粵海門</span>](粵海門站.md "wikilink") –
[<span style="color:#888;">高新南</span>](高新南站.md "wikilink") –
（[红树湾南](红树湾南站.md "wikilink") – 已开通段 –
[文锦](文锦站.md "wikilink")）

|group10 = <small><font color={{深圳地鐵色彩|10}}>█</font></small>
[<span style="color: #888;">10號綫</span>](深圳地鐵10號綫.md "wikilink")
|group10style= border-right:solid 5px ; |list10 =
[<span style="color: #888;">金琥道</span>](金琥道站.md "wikilink") -
[<span style="color: #888;">藍花道</span>](藍花道站.md "wikilink") -
[<span style="color: #888;">福田口岸</span>](福田口岸站.md "wikilink") –
[<span style="color: #888;">福民</span>](福民站.md "wikilink") –
[<span style="color: #888;">岗厦</span>](岗厦站.md "wikilink") –
[<span style="color: #888;">岗厦北</span>](岗厦北站.md "wikilink") –
[<span style="color: #888;">莲花村</span>](莲花村站_\(深圳\).md "wikilink") –
[<span style="color: #888;">冬瓜嶺</span>](冬瓜嶺站.md "wikilink") –
[<span style="color: #888;">孖岭</span>](孖岭站.md "wikilink") –
[<span style="color: #888;">雅宝</span>](雅宝站.md "wikilink") –
[<span style="color: #888;">南坑</span>](南坑站.md "wikilink") –
[<span style="color: #888;">光雅園</span>](光雅園站.md "wikilink") –
[<span style="color: #888;">五和</span>](五和站.md "wikilink") –
[<span style="color: #888;">坂田北</span>](坂田北站.md "wikilink") –
[<span style="color: #888;">貝爾路</span>](貝爾路站.md "wikilink") –
[<span style="color: #888;">華為</span>](華為站.md "wikilink") –
[<span style="color: #888;">崗頭</span>](崗頭站.md "wikilink") –
[<span style="color: #888;">雪象</span>](雪象站.md "wikilink") –
[<span style="color: #888;">甘坑</span>](甘坑站.md "wikilink") –
[<span style="color: #888;">凉帽山</span>](凉帽山站.md "wikilink") –
[<span style="color: #888;">上李朗</span>](上李朗站_\(深圳\).md "wikilink") –
[<span style="color: #888;">木古</span>](木古站.md "wikilink") –
[<span style="color: #888;">华南城</span>](华南城站.md "wikilink") –
[<span style="color: #888;">禾花</span>](禾花站.md "wikilink") –
[<span style="color: #888;">平湖站</span>](平湖站.md "wikilink") –
[<span style="color: #888;">雙擁街</span>](雙擁街站.md "wikilink") -
[<span style="color: #888;">龍平路</span>](龍平路站.md "wikilink") -
[<span style="color: #888;">油柑埔</span>](油柑埔站.md "wikilink") -
[<span style="color: #888;">官井頭</span>](官井頭站.md "wikilink") -
[<span style="color: #888;">大運城邦</span>](大運城邦站.md "wikilink") -
[<span style="color: #888;">黃閣</span>](黃閣站.md "wikilink")

|group11 = <small><font color={{深圳地鐵色彩|11}}>█</font></small>
[11號綫](深圳地鐵11號綫.md "wikilink") |group11style=
border-right:solid 5px ; |list11 =
[<span style="color: #888;">東莞市</span>](東莞市.md "wikilink")[<span style="color: #888;">長安鎮</span>](長安鎮_\(東莞市\).md "wikilink")
⇔ （[碧頭](碧頭站.md "wikilink") – 已开通段 – [福田](福田站_\(地鐵\).md "wikilink")） –
[<span style="color: #888;">岗厦北</span>](岗厦北站.md "wikilink") –
[<span style="color: #888;">上海賓館</span>](上海賓館站.md "wikilink") –
[<span style="color: #888;">大劇院</span>](大劇院站_\(深圳\).md "wikilink")

|group12 = <small><font color={{深圳地鐵色彩|12}}>█</font></small>
[<span style="color: #888;">12號綫</span>](深圳地鐵12號綫.md "wikilink")
|group12style= border-right:solid 5px ; |list12 =
[<span style="color: #888;">松崗</span>](松崗站.md "wikilink") -
[<span style="color: #888;">沙浦</span>](沙浦站.md "wikilink") -
[<span style="color: #888;">步涌</span>](步涌站.md "wikilink") -
[<span style="color: #888;">沙三</span>](沙三站.md "wikilink") -
[<span style="color: #888;">蠔鄉</span>](蠔鄉站.md "wikilink") -
[<span style="color: #888;">海上田園東</span>](海上田園東站.md "wikilink") –
[<span style="color: #888;">国展中心北</span>](国展中心北站.md "wikilink") –
[<span style="color: #888;">国展中心</span>](国展中心站.md "wikilink") –
[<span style="color: #888;">国展中心南</span>](国展中心南站.md "wikilink") –
[<span style="color: #888;">福海西</span>](福海西站.md "wikilink") –
[<span style="color: #888;">桥头西</span>](桥头西站.md "wikilink") –
[福永](福永站.md "wikilink") –
[<span style="color: #888;">懷德</span>](懷德站.md "wikilink") –
[<span style="color: #888;">福围</span>](福围站.md "wikilink") –
[機場東](機場東站_\(深圳\).md "wikilink") –
[<span style="color: #888;">兴围</span>](兴围站.md "wikilink") –
[<span style="color: #888;">黃田</span>](黃田站.md "wikilink") –
[<span style="color: #888;">鍾屋南</span>](鍾屋南站.md "wikilink") –
[<span style="color: #888;">西乡桃源</span>](西乡桃源站.md "wikilink") –
[<span style="color: #888;">臣田北</span>](臣田北站.md "wikilink") –
[<span style="color: #888;">凤凰岗</span>](凤凰岗站.md "wikilink") –
[<span style="color: #888;">寶安客運站</span>](寶安客運站.md "wikilink") –
[<span style="color: #888;">流塘</span>](流塘站.md "wikilink") –
[<span style="color: #888;">上川</span>](上川站.md "wikilink") –
[靈芝](靈芝站.md "wikilink") –
[<span style="color: #888;">新安公園</span>](新安公園站.md "wikilink") –
[<span style="color: #888;">同乐南</span>](同乐南站.md "wikilink") –
[<span style="color: #888;">中山公园</span>](中山公园站_\(深圳\).md "wikilink") –
[<span style="color: #888;">南頭</span>](南頭站.md "wikilink") –
[桃園](桃園站_\(深圳\).md "wikilink") –
[南山](南山站_\(深圳\).md "wikilink") –
[<span style="color: #888;">南光</span>](南光站.md "wikilink") –
[<span style="color: #888;">南油</span>](南油站.md "wikilink") –
[<span style="color: #888;">四海</span>](四海站.md "wikilink") –
[<span style="color: #888;">花果山</span>](花果山站.md "wikilink") –
[<span style="color: #888;">海上世界</span>](海上世界站.md "wikilink") –
[<span style="color: #888;">太子灣</span>](太子灣站.md "wikilink") –
[<span style="color: #888;">左炮台东</span>](左炮台东站.md "wikilink")

|group13 = <small><font color={{深圳地鐵色彩|13}}>█</font></small>
[<span style="color: #888;">13號線</span>](深圳地鐵13號線.md "wikilink")
|group13style= border-right:solid 5px ; |list13 =
[<span style="color: #888;">歌劇院</span>](歌劇院站_\(深圳\).md "wikilink") -
[<span style="color: #888;">深圳灣公園</span>](深圳灣公園站2.md "wikilink") -
[<span style="color: #888;">深圳灣口岸</span>](深圳灣口岸站.md "wikilink") –
[<span style="color: #888;">人才公园</span>](人才公园站.md "wikilink") –
[后海](后海站.md "wikilink") –
[<span style="color: #888;">科苑</span>](科苑站.md "wikilink") –
[<span style="color:#888;">粤海门</span>](粤海门站.md "wikilink") –
[<span style="color: #888;">深大</span>](深大站.md "wikilink") –
[<span style="color: #888;">高新园中</span>](高新园中站.md "wikilink") –
[<span style="color: #888;">高新园北</span>](高新园北站.md "wikilink") –
[<span style="color: #888;">西麗火车站</span>](西麗火车站.md "wikilink") –
[<span style="color: #888;">石鼓</span>](石鼓站.md "wikilink") –
[<span style="color: #888;">留仙洞</span>](留仙洞站.md "wikilink") –
[<span style="color: #888;">白芒</span>](白芒站.md "wikilink") –
[<span style="color: #888;">應人石</span>](應人石站.md "wikilink") –
[<span style="color: #888;">罗租</span>](罗租站.md "wikilink") –
[<span style="color: #888;">石岩</span>](石岩站.md "wikilink") –
[<span style="color: #888;">上屋北</span>](上屋北站.md "wikilink") -
[<span style="color: #888;">光明中醫院</span>](光明中醫院站.md "wikilink") –
[<span style="color: #888;">光明城</span>](光明城站.md "wikilink") –
[<span style="color: #888;">同觀路</span>](同觀路站.md "wikilink") –
[<span style="color: #888;">觀光</span>](觀光站.md "wikilink") –
[<span style="color: #888;">東周路</span>](東周路站.md "wikilink") –
[<span style="color: #888;">公明南</span>](公明南站.md "wikilink") –
[<span style="color: #888;">公明广场</span>](公明广场站.md "wikilink") –
[<span style="color: #888;">長春路</span>](長春路站.md "wikilink") -
[<span style="color: #888;">下村</span>](下村站.md "wikilink") –
[<span style="color: #888;">公明北</span>](公明北站.md "wikilink")

|group14 = <small><font color={{深圳地鐵色彩|14}}>█</font></small>
[<span style="color: #888;">14號綫</span>](深圳地鐵14號綫.md "wikilink")
|group14style= border-right:solid 5px ; |list14 =
[<span style="color: #888;">會展中心西</span>](會展中心西站.md "wikilink") -
[<span style="color: #888;">崗廈北</span>](崗廈北站.md "wikilink") –
[<span style="color: #888;">黃木崗</span>](黃木崗站.md "wikilink") –
[<span style="color: #888;">清水河</span>](清水河站.md "wikilink") –
[布吉](布吉站.md "wikilink") –
[<span style="color: #888;">石芽嶺</span>](石芽嶺站.md "wikilink") –
[<span style="color: #888;">六約北</span>](六約北站.md "wikilink") –
[<span style="color: #888;">四聯</span>](四聯站.md "wikilink") –
[<span style="color: #888;">坳背</span>](坳背站.md "wikilink") –
[大運](大運站.md "wikilink")
–[<span style="color: #888;">寶荷</span>](寶荷站.md "wikilink")-
[<span style="color: #888;">寶龍</span>](寶龍站.md "wikilink") –
[<span style="color: #888;">沙湖</span>](沙湖站.md "wikilink") –
[<span style="color: #888;">坪山围</span>](坪山围站.md "wikilink") –
[<span style="color: #888;">坪山廣場</span>](坪山廣場站.md "wikilink") -
[<span style="color: #888;">朱洋坑</span>](朱洋坑.md "wikilink") –
[<span style="color: #888;">坑梓</span>](坑梓站.md "wikilink") –
[<span style="color: #888;">沙田</span>](沙田站_\(深圳\).md "wikilink") ⇔
[<span style="color: #888;">惠州市</span>](惠州市.md "wikilink")[<span style="color: #888;">惠陽區</span>](惠陽區.md "wikilink")

|group16 = <small><font color={{深圳地鐵色彩|16}}>█</font></small>
[<span style="color: #888;">16號綫</span>](深圳地鐵16號綫.md "wikilink")
|group16style= border-right:solid 5px ; |list16 =
[<span style="color: #888;">安良</span>](安良站.md "wikilink") -
[<span style="color: #888;">福坑</span>](福坑站.md "wikilink") -
[<span style="color: #888;">龍興</span>](龍興站.md "wikilink") -
[<span style="color: #888;">圓山</span>](園山站_\(深圳\).md "wikilink") -
[<span style="color: #888;">阿波羅南</span>](阿波羅南站.md "wikilink") -
[<span style="color: #888;">阿波羅</span>](阿波羅站.md "wikilink") -
[<span style="color: #888;">長江埔</span>](長江埔站.md "wikilink") -
[<span style="color: #888;">大运</span>](大运站.md "wikilink") –
[<span style="color: #888;">大运中心</span>](大运中心站.md "wikilink") –
[<span style="color: #888;">龙城公园</span>](龙城公园站.md "wikilink") –
[<span style="color: #888;">黄阁</span>](黄阁站_\(深圳\).md "wikilink") –
[<span style="color: #888;">回龙埔</span>](回龙埔站.md "wikilink") –
[<span style="color: #888;">愉园</span>](愉园站.md "wikilink") –
[<span style="color: #888;">尚景</span>](尚景站.md "wikilink") –
[<span style="color: #888;">盛平</span>](盛平站.md "wikilink") –
[<span style="color: #888;">龙园</span>](龙园站.md "wikilink") –
[<span style="color: #888;">雙龍</span>](雙龍站.md "wikilink") –
[<span style="color: #888;">新塘围</span>](新塘围站.md "wikilink") –
[<span style="color: #888;">龙东</span>](龙东站.md "wikilink") –
[<span style="color: #888;">宝龙同乐</span>](宝龙同乐站.md "wikilink") –
[<span style="color: #888;">坪山</span>](坪山站.md "wikilink") –
[<span style="color: #888;">新和</span>](新和站.md "wikilink") –
[<span style="color: #888;">六和</span>](六和站.md "wikilink") –
[<span style="color: #888;">坪山围</span>](坪山围站.md "wikilink") –
[<span style="color: #888;">坪环</span>](坪环站.md "wikilink") –
[<span style="color: #888;">江岭</span>](江岭站.md "wikilink") –
[<span style="color: #888;">沙壆</span>](沙壆站.md "wikilink") –
[<span style="color: #888;">燕子岭东</span>](燕子岭东站.md "wikilink") –
[<span style="color: #888;">石井</span>](石井站.md "wikilink") –
[<span style="color: #888;">技术大学</span>](技术大学站.md "wikilink") –
[<span style="color: #888;">田心</span>](田心站.md "wikilink")

|group20 = <small><font color={{深圳地鐵色彩|20}}>█</font></small>
[<span style="color: #888;">20號綫</span>](深圳地鐵20號綫.md "wikilink")
|group20style= border-right:solid 5px ; |list20 =
[<span style="color: #888;">皇崗口岸</span>](皇崗口岸站.md "wikilink") ⇔
[<span style="color: #888;">會展中心西</span>](會展中心西站.md "wikilink") –
[車公廟](車公廟站_\(深圳\).md "wikilink") –
[<span style="color: #888;">白石洲</span>](白石洲站.md "wikilink") –
[<span style="color: #888;">深大</span>](深大站.md "wikilink") –
[<span style="color: #888;">深大北</span>](深大北站.md "wikilink") –
[<span style="color: #888;">南頭古城</span>](南頭古城站.md "wikilink") –
[<span style="color: #888;">南头关</span>](南头关站.md "wikilink") –
[翻身](翻身站.md "wikilink") –
[<span style="color: #888;">西乡公园</span>](西乡公园站.md "wikilink") –
[<span style="color: #888;">臣田西</span>](臣田西站.md "wikilink") –
[<span style="color: #888;">三围</span>](三围站.md "wikilink") –
[<span style="color: #888;">機場東</span>](機場東站_\(深圳\).md "wikilink") –
[<span style="color: #888;">机场北</span>](机场北站_\(深圳\).md "wikilink") –
[<span style="color: #888;">重庆路</span>](重庆路站.md "wikilink") –
[<span style="color: #888;">会展南</span>](会展南站.md "wikilink") –
[<span style="color: #888;">会展北</span>](会展北站.md "wikilink") ⇔
[<span style="color: #888;">東莞市</span>](東莞市.md "wikilink")[<span style="color: #888;">長安鎮</span>](長安鎮_\(東莞市\).md "wikilink")
}}

|below =
**圖例**：「■」表示該站可轉乘的路綫（以顔色區分）；「<span style="color: #888;">灰色</span>」為興建中或研究階段車站；「<span style="color: #888;">-</span>」為興建中或建議中路段；「<span style="color: #888;">⇔</span>」代表車站設置不詳的路段。
}}

</div>

<noinclude>  </noinclude>