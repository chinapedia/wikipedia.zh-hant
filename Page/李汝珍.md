**李汝珍**（1763年？─1830年\[1\]），字**松石**\[2\]，號**松石道人**，直隸大興（今[北京](../Page/北京.md "wikilink")[大興縣](../Page/大興縣.md "wikilink")）人。[清代文人](../Page/清朝.md "wikilink")，小說《[鏡花缘](../Page/鏡花缘.md "wikilink")》為其代表作。

他的一生大約經歷了[清](../Page/清.md "wikilink")[乾隆](../Page/乾隆.md "wikilink")、[嘉慶](../Page/嘉慶.md "wikilink")、[道光三朝](../Page/道光.md "wikilink")，但考場不得意。\[3\]乾隆四十七年壬寅（1782年），十九歲隨兄[鹽課司大使](../Page/鹽課司.md "wikilink")[李汝璜到](../Page/李汝璜.md "wikilink")[海州](../Page/海州.md "wikilink")[板浦](../Page/板浦.md "wikilink")（今江蘇连云港海州区板浦鎮）等地，師事[凌廷堪](../Page/凌廷堪.md "wikilink")，结识许乔林、许桂林、吴振勃等。\[4\]後因[水患捐官得](../Page/水患.md "wikilink")[縣丞一職](../Page/縣丞.md "wikilink")。李汝珍是道地的文人，“於學無所不窺，尤通音韻”，精通[文學](../Page/文學.md "wikilink")、[詩詞](../Page/詩詞.md "wikilink")、[音韻](../Page/聲韻學.md "wikilink")、[經學](../Page/經學.md "wikilink")、[字學](../Page/文字學.md "wikilink")、[醫學](../Page/醫學.md "wikilink")、[算數](../Page/算數.md "wikilink")、[茶經](../Page/茶經.md "wikilink")、[棋譜](../Page/棋譜.md "wikilink")，還「旁及雜流」，如[象緯](../Page/象緯.md "wikilink")、[篆](../Page/篆.md "wikilink")[隸等](../Page/隸.md "wikilink")。嘉慶二十三年（1818年）寫成《[鏡花緣](../Page/鏡花緣.md "wikilink")》，花了近二十年心血，三易其稿，行文對中處處展現知識\[5\]，是一部賣弄學問作品\[6\]，原擬寫兩百回，但只寫一百回。旅美學人[夏志清評論此書幾乎沒有前後文矛盾之處](../Page/夏志清.md "wikilink")\[7\]。另著有《李氏音鑑》、《字母五聲圖》、《受子譜》等。

今[連雲港](../Page/連雲港.md "wikilink")[板浦鎮](../Page/板浦鎮.md "wikilink")[東大街](../Page/東大街.md "wikilink")，有“[李汝珍紀念館](../Page/李汝珍紀念館.md "wikilink")”。

## 注釋

## 參考書目

  - 李明友《李汝珍師友年譜》

[L](../Category/清朝作家.md "wikilink") [R](../Category/李姓.md "wikilink")
[Category:19世紀小說家](../Category/19世紀小說家.md "wikilink")

1.  胡適考證李汝珍約生於乾隆中葉（1763年），卒於道光十年（1830年）。（胡適，〈《鏡花緣》的引論〉，《[胡適文存](../Page/胡適文存.md "wikilink")》第二集（台北：遠東圖書公司，1961），頁404）
2.  胡适在《〈镜花缘〉的引论》一文中确认“李汝珍，字松石，大兴人。”[吴振勷在](../Page/吴振勷.md "wikilink")《音学臆说》的“识语”記李汝珍的字号除了
    “松石”外，还有“聘斋”。
3.  《顺天府志》找不到此人傳記。
4.  《李氏音鑒》卷五“第三十三問”：“壬寅之秋，珍隨兄佛雲宦遊朐陽，受業於淩氏廷堪仲子夫子。論文之暇，旁及音韻，受益極多。”
5.  [许乔林在](../Page/许乔林.md "wikilink")《〈镜花缘〉序》里说此书“枕经葄史，子秀集华，兼贯九流，旁涉百戏，聪明绝世，异境天开”。
6.  [钱静方](../Page/钱静方.md "wikilink")《小说丛考》卷上，页68—72
7.  夏志清，〈文人小說家和中國文化──《鏡花緣》研究〉