**波特蘭**（）位於[緬因灣岸](../Page/緬因灣.md "wikilink")，是[美國](../Page/美國.md "wikilink")[緬因州最大的城市](../Page/緬因州.md "wikilink")、[坎伯蘭縣縣治](../Page/坎伯蘭縣_\(緬因州\).md "wikilink")。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，人口為64,249人。這個數字於2010年上升至66,214人。\[1\]

1632年開埠，1786年改用現名\[2\]。歷史上曾經歷四次大火，故其格言為「我將再起」（）\[3\]\[4\]。[奧勒岡州的](../Page/奧勒岡州.md "wikilink")[波特蘭即取名自此城](../Page/波特蘭_\(俄勒岡州\).md "wikilink")。\[5\]

## 地理与气候

根據[2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，本城面積為，其中為陸地，為水域。\[6\]

波特兰属于新英格兰典型的[大陆性气候](../Page/大陆性气候.md "wikilink")，\[7\]但仍然受大西洋的部分影响。冬季寒冷而漫长，潮湿，日最高气温低于的平均日数为42天，日最低气温低于的平均日数为17天，低于的有3.7天；夏季相对潮湿，日最高气温超过的日数年均只有13天，以上的气温为罕见。\[8\]最冷月（1月）均温，极端最低气温（1943年2月16日）。\[9\]最热月（7月）均温，极端最高气温（1911年7月4日、1975年8月2日）。\[10\]无霜期平均为158天（5月3日至10月7日）。\[11\]年均降水量约，年均降雪量为；1979–80年的降雪量最少，积累降雪量只有，1970–71年的降雪量最多，积累降雪量为。\[12\]

## 姐妹城市

  - [阿爾漢格爾斯克](../Page/阿爾漢格爾斯克.md "wikilink")

  - [品川市](../Page/品川區.md "wikilink")

  - [海地角](../Page/海地角.md "wikilink")

  - [米蒂利尼](../Page/米蒂利尼.md "wikilink")

## 参考文献

[P](../Category/緬因州城市.md "wikilink")
[Category:缅因州波特兰](../Category/缅因州波特兰.md "wikilink")
[Category:缅因州县城](../Category/缅因州县城.md "wikilink")
[Category:缅因州坎伯兰县城市](../Category/缅因州坎伯兰县城市.md "wikilink")
[Category:缅因州沿海定居点](../Category/缅因州沿海定居点.md "wikilink")

1.  [2010 census factfinder2](http://factfinder2.census.gov/)

2.  [Christopher Levett, of York: The Pioneer Colonist in Casco Bay,
    James Baxter
    Phinney,1893](http://books.google.com/books?hl=en&id=gwKwEbZhv3cC&dq=%22christopher+levett%22&printsec=frontcover&source=web&ots=UQCWMF5PBW&sig=pdiiURhalN_V28YtQQP-BVtEVxU)

3.  [Falmouth Fire](http://www.mainehistory.org/pdf/Falmouth_Fire.pdf)

4.

5.

6.

7.  Peel, M. C., Finlayson, B. L., and McMahon, T. A.: [Updated world
    map of the Köppen-Geiger climate
    classification](../Page/Media:Americas_Köppen_Map_original_colors.png.md "wikilink"),
    Hydrol. Earth Syst. Sci., 11, 1633–1644, 2007.

8.
9.
10.
11.
12.