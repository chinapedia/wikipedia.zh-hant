《**拉瑪任務**》（）是[英國作家](../Page/英國.md "wikilink")[亞瑟·克拉克於](../Page/亞瑟·克拉克.md "wikilink")1972年出版的[科幻小說](../Page/科幻小說.md "wikilink")。故事敘述在22世紀時有一個五十公里長的[圓柱體形外星太空船闖入](../Page/圓柱體.md "wikilink")[太陽系](../Page/太陽系.md "wikilink")，人類派出探險隊前去調查的過程。本書獲得1973年[星雲獎和](../Page/星雲獎.md "wikilink")1974年[雨果獎的最佳長篇小說獎](../Page/雨果獎.md "wikilink")。

## 中譯書目

  - 《拉瑪任務》，[胡瑛譯](../Page/胡瑛.md "wikilink")，[小知堂文化出版](../Page/小知堂文化.md "wikilink")，2005年（繁體，台灣）
  - 《与拉玛-{}-相会》，外国科幻名家精品丛书·拉玛系列之一，蔡南德译，ISBN 7536521677，四川少年儿童出版社

## 参考资料

[Category:1972年長篇小說](../Category/1972年長篇小說.md "wikilink")
[Category:科幻小说](../Category/科幻小说.md "wikilink")
[Category:星云奖获奖作品](../Category/星云奖获奖作品.md "wikilink")
[Category:雨果奖获奖作品](../Category/雨果奖获奖作品.md "wikilink")
[Category:英国小说](../Category/英国小说.md "wikilink")
[Category:太陽系背景作品](../Category/太陽系背景作品.md "wikilink")
[Category:亞瑟·查理斯·克拉克](../Category/亞瑟·查理斯·克拉克.md "wikilink")
[Category:英格蘭小說](../Category/英格蘭小說.md "wikilink")
[Category:軌跡獎獲獎作品](../Category/軌跡獎獲獎作品.md "wikilink")