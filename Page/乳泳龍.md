**乳泳龍**（屬名：*Opallionectes*）意為「乳白色游泳者」，是種身長5公尺的[蛇頸龍類](../Page/蛇頸龍類.md "wikilink")，生存於早[白堊紀中](../Page/白堊紀.md "wikilink")[阿普第階的](../Page/阿普第階.md "wikilink")[澳洲淺海](../Page/澳洲.md "wikilink")，約1億1500萬年前。

乳泳龍的化石包含[脊椎](../Page/脊椎.md "wikilink")、[肋骨](../Page/肋骨.md "wikilink")、牙齒、以及[胃石](../Page/胃石.md "wikilink")，發現於澳洲南部安達穆卡的[蛋白石礦區](../Page/蛋白石.md "wikilink")，使得化石也呈現乳白色，是由Kear在2006年所敘述。乳泳龍擁有針狀、銳利牙齒，類似[上龍類](../Page/上龍類.md "wikilink")，可能用來抓住小型獵物，例如[魚類與](../Page/魚類.md "wikilink")[魷魚](../Page/魷魚.md "wikilink")。乳泳龍被認為是生存於1億6500萬年前的古老蛇頸龍類，與6500萬年前的[白堊紀末期蛇頸龍類之間的遺失連結](../Page/白堊紀.md "wikilink")，這時間的蛇頸龍類在過去的[化石紀錄中出現斷層](../Page/化石紀錄.md "wikilink")。對於沉積層、化石、[同位素](../Page/同位素.md "wikilink")、以及氣候模式的研究，顯示乳泳龍的生存環境有季節性寒冷，牠們可能已有辦法適應寒冷水域，例如季節性遷徙與先進的[新陳代謝](../Page/新陳代謝.md "wikilink")。

## 參考資料

  - Kear B.P., 2006, "Marine Reptiles from the lower Cretaceous of South
    Australia: Elements of a high-latitude cold-water assemblage",
    Palaeontology, Vol. 49, Part 4, pp. 837–856

[Category:蛇頸龍亞目](../Category/蛇頸龍亞目.md "wikilink")
[Category:白堊紀爬行動物](../Category/白堊紀爬行動物.md "wikilink")