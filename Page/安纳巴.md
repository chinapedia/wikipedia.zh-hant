**安纳巴**（）是位于[阿尔及利亚北部](../Page/阿尔及利亚.md "wikilink")[安纳巴省](../Page/安纳巴省.md "wikilink")[地中海沿岸的一座城市](../Page/地中海.md "wikilink")，人口257359人（2008年）。

## 友好城市

  - [法国](../Page/法国.md "wikilink")[圣艾蒂安](../Page/圣艾蒂安.md "wikilink")

## 交通

  - [安納巴机场](../Page/安納巴机场.md "wikilink")

## 参见

  - [安纳巴大学](../Page/安纳巴大学.md "wikilink")
  - [安纳巴圣奥斯定圣殿](../Page/安纳巴圣奥斯定圣殿.md "wikilink")

## 外部链接

  - [安纳巴城市网站](http://www.el-annabi.com/)
  - [阿尔及利亚波尼](http://www.wdl.org/zh/item/8787/)

[Category:阿爾及利亞城市](../Category/阿爾及利亞城市.md "wikilink")