《**大元大一統志**》，简称《**大一统志**》是[中国](../Page/中国.md "wikilink")[元代官方修的](../Page/元代.md "wikilink")[地理總志](../Page/地理.md "wikilink")。是古代中国最大的一部輿地書。

## 编辑过程

  - [回回人](../Page/回回人.md "wikilink")[札馬剌丁创议编辑官方地理總志](../Page/札馬剌丁.md "wikilink")，并与[虞應龍合作领导组织撰修](../Page/虞應龍.md "wikilink")。从[元世祖至元二十二年](../Page/元世祖.md "wikilink")（1285年）开始；到至元三十一年（1294年）共经历9年完成初稿755卷。
  - 而后因获得《雲南圖志》、《甘肅圖志》、《遼陽圖志》故而继续重修，由[孛蘭盻](../Page/孛蘭盻.md "wikilink")、[岳鉉等主持编辑](../Page/岳鉉.md "wikilink")，到[元成宗大德七年](../Page/元成宗.md "wikilink")(1303年)全書完成，共1300卷，600册。定名为《大元大一統志》。
  - 成书后藏于秘府，一直到[元順帝至正六年](../Page/元順帝.md "wikilink")（1346年）方在[杭州开始刻版](../Page/杭州.md "wikilink")，而后[許有壬為此书写序](../Page/許有壬.md "wikilink")。

## 内容

《大元大一統志》所记录的各路、州、縣内容多来自[唐代](../Page/唐代.md "wikilink")《[元和郡縣圖志](../Page/元和郡縣圖志.md "wikilink")》、[宋代](../Page/宋代.md "wikilink")《[太平寰宇記](../Page/太平寰宇記.md "wikilink")》、《[輿地紀勝](../Page/輿地紀勝.md "wikilink")》等書。其中[长江以](../Page/长江.md "wikilink")[南各行省多取材](../Page/南.md "wikilink")《輿地紀勝》和宋、元二代的其他地理誌；长江以[北各行省则多取材於](../Page/北.md "wikilink")《元和郡縣圖志》、《太平寰宇記》和其他[金](../Page/金朝.md "wikilink")、元地理志。

概书分成以下部分介绍各行省，建置沿革、坊郭鄉鎮、里至、山川、土產、風俗形勝、古蹟、宦跡、人物、仙釋。

## 流传过程

  - 1285年《大元大一統志》开始修书。由扎马剌丁、虞應龍等主持编辑。
  - 1294年完成初稿755卷。而后续修。由孛蘭盻、岳鉉等主持编辑。
  - 1303年全書完成，共1300卷，600册。定名为《大元大一統志》。
  - 1346年在[杭州开始刻版](../Page/杭州.md "wikilink")，而后許有壬為此书写序。
  - 明代的《[文淵閣書目](../Page/文淵閣書目.md "wikilink")》记录有《大元一統志》兩部。一部182冊；另一部600冊，但未记录卷数。但历史学者依据后一部有600册，与全書刚完成时数目相同，认为明[嘉靖年间](../Page/嘉靖.md "wikilink")《大元大一統志》全书尚存。
  - [中华人民共和国时期残存的](../Page/中华人民共和国.md "wikilink")《大元大一統志》，包括《[遼海叢書](../Page/遼海叢書.md "wikilink")》、《[玄覽堂叢書](../Page/玄覽堂叢書.md "wikilink")》所錄，仅44卷，不及原来卷数的5％。并且某些卷内不全甚至只存2－3页。这些残卷由[北京圖書館及私人收藏家所收藏](../Page/北京圖書館.md "wikilink")。

## 相关书籍

  - [元文宗时代纂修的](../Page/元文宗.md "wikilink")《[經世大典](../Page/經世大典.md "wikilink")》，其中的
    “都邑”部分以 《大元大一統志》为据。
  - [明代官修的地理誌](../Page/明代.md "wikilink")《[大明一統志](../Page/大明一統志.md "wikilink")》、《寰宇通誌》参考《大元大一統志》，其中有引用《大元大一統志》原文。
  - 明代修的《[永樂大典](../Page/永樂大典.md "wikilink")》，记录了大量《大元大一統志》的文字。后来《[遼東志](../Page/遼東志.md "wikilink")》、《[滿洲源流考](../Page/滿洲源流考.md "wikilink")》、《[熱河志](../Page/熱河志.md "wikilink")》、《[乾隆盛京通志](../Page/乾隆盛京通志.md "wikilink")》、《[蒙古遊牧記](../Page/蒙古遊牧記.md "wikilink")》、《[嘉慶四川通志](../Page/嘉慶四川通志.md "wikilink")》、《[湖北通志](../Page/湖北通志.md "wikilink")》、《[乾隆東昌府志](../Page/乾隆東昌府志.md "wikilink")》、《[嘉慶延安府志](../Page/嘉慶延安府志.md "wikilink")》、《[日下舊聞考](../Page/日下舊聞考.md "wikilink")》、《[愚谷文存](../Page/愚谷文存.md "wikilink")》都有引用《大元大一統志》的原文，但学者推测实际上是引自《[永樂大典](../Page/永樂大典.md "wikilink")》的记录。
  - [清代](../Page/清代.md "wikilink")[宣統三年聊城鄒氏石印本的](../Page/宣統.md "wikilink")《[至正集](../Page/至正集.md "wikilink")》卷35中有元代許有壬的《大一統志序》。
  - 近代学者[金毓黻曾经蒐集整理](../Page/金毓黻.md "wikilink")，刊有《大元大一統志》殘本十五卷，輯本四卷。
  - 学者[趙萬里以](../Page/趙萬里.md "wikilink")《[元史](../Page/元史.md "wikilink")‧地理志》為綱，將《大元大一統志》的元刻殘帙，各家抄本與群書所引，彙輯為一書，分編十卷，编成《元一統志》。1966年[北京的](../Page/北京.md "wikilink")[中華書局有出版此书](../Page/中華書局.md "wikilink")。

## 参考资料

  - 《[中国大百科全书](../Page/中国大百科全书.md "wikilink")》"大元大一統誌"条目。
  - [中国科学史上的扎马剌丁](https://web.archive.org/web/20051125000543/http://www.makuielys.info/wenz1/show.php?type=hzslg&id=1051382743)

[Category:史部地理類](../Category/史部地理類.md "wikilink")
[Category:地理總志](../Category/地理總志.md "wikilink")
[Category:元朝官修典籍](../Category/元朝官修典籍.md "wikilink")
[Category:地理书籍](../Category/地理书籍.md "wikilink")