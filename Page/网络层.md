**网络层**（**N**etwork
**L**ayer）是[OSI模型中的第三層](../Page/OSI模型.md "wikilink")（[TCP/IP模型中的网际层](../Page/TCP/IP协议族.md "wikilink")），提供[路由和尋址的功能](../Page/路由.md "wikilink")，使兩終端系統能夠互連且決定最佳路徑，並具有一定的擁塞控制和流量控制的能力。相當於傳送郵件時需要地址一般重要。由于TCP/IP協議體系中的網路層功能由IP協議規定和實現，故又稱IP層。

## 功能

### 寻址

对网络层而言使用[IP地址来唯一标识互联网上的设备](../Page/IP地址.md "wikilink")，网络层依靠IP地址进行相互通信（类似于[数据链路层的](../Page/数据链路层.md "wikilink")[MAC地址](../Page/MAC地址.md "wikilink")），详细的编址方案参见[IPv4和](../Page/IPv4.md "wikilink")[IPv6](../Page/IPv6.md "wikilink")。

### 路由

在同一个网络中的内部通信并不需要网络层设备，仅仅靠数据链路层就可以完成相互通信，对于不同的网络之间相互通信则必须借助路由器等三层设备。

## 虚电路和数据报网络

在[传输层每个应用可以被提供两个服务](../Page/传输层.md "wikilink")：无连接的[UDP和有链接的](../Page/用户数据报协议.md "wikilink")[TCP](../Page/传输控制协议.md "wikilink")，在网络层也能为主机之间提供无连接和有链接的服务。

### 特点

  - 在网络层中这些服务（无论是有链接还是无连接）都是提供主机到主机的服务，在传输层中提供的则是提供应用层进程之间的服务。
  - 在至今为止的所有的主要计算机网络结构体系中（因特网、ATM、帧中继等），网络層提供了主机到主机无连接或者有连接服务，而不同时提供两种服务。仅提供无连接的的网络称为[数据报网络](../Page/数据报.md "wikilink")(Datagram
    Network)，仅提供有连接的网络称为[虚电路网络](../Page/虚电路.md "wikilink")（Virtual-Circuit，VC）。

## 网络层协议

  - [IP](../Page/网际协议.md "wikilink") （[V4](../Page/IPv4.md "wikilink")
    [V6](../Page/IPv6.md "wikilink")）
  - [IPX](../Page/IPX.md "wikilink")
  - [X.25](../Page/X.25.md "wikilink")
  - [RARP](../Page/RARP.md "wikilink")
  - [ICMP](../Page/ICMP.md "wikilink")（[V4](../Page/ICMP.md "wikilink")、[V6](../Page/ICMPv6.md "wikilink")）
  - [IGMP](../Page/IGMP.md "wikilink")
  - [IPsec](../Page/IPsec.md "wikilink")
  - [RIP](../Page/路由信息協議.md "wikilink")

## 具有网络层功能的设备

  - [路由器](../Page/路由器.md "wikilink")

  -
## 参看

  - [OSI模型](../Page/OSI模型.md "wikilink")
  - [IP](../Page/IP.md "wikilink")
  - [ICMP](../Page/ICMP.md "wikilink")

[Category:网络层协议](../Category/网络层协议.md "wikilink")
[Category:OSI协议](../Category/OSI协议.md "wikilink")