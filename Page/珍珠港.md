[PearlHarbor_Sm.jpg](https://zh.wikipedia.org/wiki/File:PearlHarbor_Sm.jpg "fig:PearlHarbor_Sm.jpg")（Hickam
AFB）與[檀香山國際機場](../Page/檀香山國際機場.md "wikilink")。\]\]
**珍珠港**（）是一個位於[美國](../Page/美國.md "wikilink")[夏威夷州](../Page/夏威夷州.md "wikilink")[歐胡島上的](../Page/歐胡島.md "wikilink")[海港](../Page/海港.md "wikilink")，位於該州首府[檀香山西方](../Page/檀香山.md "wikilink")。其命名的由來，是該地曾經盛產珍珠而得名。珍珠港港區與鄰近島嶼上大部分的設施都屬於[美國海軍所有](../Page/美國海軍.md "wikilink")，作為深水軍港使用，也是美國海軍[太平洋艦隊的總部所在地](../Page/美國太平洋艦隊.md "wikilink")。1941年12月7日珍珠港曾遭[日軍突襲轟炸造成重大傷亡](../Page/日軍.md "wikilink")，史稱「[珍珠港事件](../Page/珍珠港事件.md "wikilink")」。此事件直接導致美國對日宣戰，正式加入[第二次世界大戰中](../Page/第二次世界大戰.md "wikilink")[太平洋戰場部分的戰事](../Page/太平洋戰場.md "wikilink")。

2010年，珍珠港與空軍基地合併成為「[珍珠港-希卡姆聯合基地](../Page/珍珠港-希卡姆聯合基地.md "wikilink")」。

電影《[虎！虎！虎！](../Page/虎！虎！虎！.md "wikilink")》及《[珍珠港](../Page/珍珠港_\(電影\).md "wikilink")》就是以此事件為背景所拍攝，[電影原聲帶亦從此熱銷](../Page/電影原聲帶.md "wikilink")。

## 相關條目

  - [珍珠港-希卡姆聯合基地](../Page/珍珠港-希卡姆聯合基地.md "wikilink")

## 參考資料

## 外部連結

  - [British Pathé](http://www.britishpathe.com/workspace.php?id=4902)
    Online archive of Pearl Harbor and related footage

  -
  - [Pearl Harbor](http://www.history.com/topics/pearl-harbor) on The
    History Channel

[Category:檀香山市縣地理](../Category/檀香山市縣地理.md "wikilink")
[Category:美國海灣](../Category/美國海灣.md "wikilink")
[Category:歐胡島地形](../Category/歐胡島地形.md "wikilink")
[Category:太平洋港口](../Category/太平洋港口.md "wikilink")
[Category:美国海军基地](../Category/美国海军基地.md "wikilink")
[Category:美國港口](../Category/美國港口.md "wikilink")
[Category:珍珠港事件](../Category/珍珠港事件.md "wikilink")