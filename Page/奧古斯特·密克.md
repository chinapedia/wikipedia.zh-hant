**奧古斯特·密克**（****），[法國](../Page/法國.md "wikilink")[數學家](../Page/數學家.md "wikilink")，曾發表數條圓和多邊形的定理，稱為[密克定理](../Page/密克定理.md "wikilink")。

他在[楠蒂阿擔任數學助教時](../Page/楠蒂阿.md "wikilink")，1838年在[約瑟夫·劉維爾的數學刊物發表數篇論文](../Page/約瑟夫·劉維爾.md "wikilink")，論及曲線、圓的交、球的交。他在[卡斯特爾學院任數學教師時](../Page/卡斯特爾.md "wikilink")，自1844年到1846年，分三部份發表幾何研究報告。

## 外部連結

以下連結到密克的原作（法文）：

  - '' Sur quelques questions relatives à la Théorie des courbes''
    (1838)（關於曲線論的問題數則） [Gallica
    (BnF)](http://visualiseur.bnf.fr/Visualiseur?O=NUMM-16382&I=210)
  - *Théorèmes de Géométrie* (1838)（幾何定理數條） [Gallica
    (BnF)](http://visualiseur.bnf.fr/Visualiseur?O=NUMM-16382&I=493)
  - *Théorèmes sur les Intersections des cercles et des sphères*
    (1838)（圓和球的交的定理數條） [Gallica
    (BnF)](http://visualiseur.bnf.fr/Visualiseur?O=NUMM-16382&I=525)
  - *Mémoire de géométrie* (1844)（幾何研究報告） [Gallica
    (BnF)](http://visualiseur.bnf.fr/Visualiseur?O=NUMM-16388&I=26)
  - *Mémoire de Géométrie (deuxième partie)* (1845)（幾何研究報告（第二部份））
    [Gallica
    (BnF)](http://visualiseur.bnf.fr/Visualiseur?O=NUMM-16389&I=355)
  - *Mémoire de géométrie (troisième partie)* (1846)（幾何研究報告（第三部份））
    [Gallica
    (BnF)](http://visualiseur.bnf.fr/Visualiseur?O=NUMM-16390&I=73)

[Miquel](../Category/法國數學家.md "wikilink")
[Miquel](../Category/19世紀數學家.md "wikilink")