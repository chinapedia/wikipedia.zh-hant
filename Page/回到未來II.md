《**回到未來續集**》（）是經典[美國](../Page/美國.md "wikilink")[科幻](../Page/科幻.md "wikilink")[喜劇](../Page/喜劇.md "wikilink")[電影](../Page/電影.md "wikilink")《[回到未來系列](../Page/回到未來系列.md "wikilink")》的第二集，於1989年上映。由[Robert
Zemeckis執導](../Page/Robert_Zemeckis.md "wikilink")，[Bob
Gale編劇](../Page/Bob_Gale.md "wikilink")，[迈克尔·J·福克斯](../Page/迈克尔·J·福克斯.md "wikilink")（Michael
J. Fox）， [Christopher
Lloyd](../Page/Christopher_Lloyd.md "wikilink")，[Lea
Thompson和](../Page/Lea_Thompson.md "wikilink")[Thomas F.
Wilson主演](../Page/Thomas_F._Wilson.md "wikilink")。

## 劇情簡介

這次，[博士跟馬蒂說](../Page/博士.md "wikilink")2015年，他的[兒子將會有危機](../Page/兒子.md "wikilink")，於是他們來到了[科技先进的](../Page/科技.md "wikilink")2015年10月21日。原来，马蒂的[儿子小马蒂](../Page/儿子.md "wikilink")·麦夫莱（Marty
McFly Jr.）在[未来即将因为与毕夫的孙子格里夫](../Page/未来.md "wikilink")·谭能（Griff
Tannen）惹事生非而遭受牢狱之灾，为了避免这一切，[博士决定让马蒂化装成小马蒂的样子去阻止这一切的发生](../Page/博士.md "wikilink")，经过和格里夫的一番争斗，总算是避免了让小马蒂坐牢的[命运](../Page/命运.md "wikilink")，却引起了老毕夫的注意。在书店，马蒂买下了一本记载所有[体育](../Page/体育.md "wikilink")[比赛结果的](../Page/比赛.md "wikilink")[体育](../Page/体育.md "wikilink")[年鉴](../Page/年鉴.md "wikilink")，意图回到过去时以此为参考去下賭注，被[博士发现后训斥了一番](../Page/博士.md "wikilink")，并将[年鉴扔进了垃圾桶](../Page/年鉴.md "wikilink")。

但这时，昏睡的珍妮佛却被[警察发现而送到了三十年后马蒂的家](../Page/警察.md "wikilink")。为了避免珍妮佛和中年的自己碰面而引起麻烦，马蒂和[博士又马不停蹄地赶往马蒂在](../Page/博士.md "wikilink")[未来的家去接回珍妮佛](../Page/未来.md "wikilink")。而醒来的珍妮佛惊讶地看到了三十年后马蒂一家的情况：马蒂由于年前的一场[车祸](../Page/车祸.md "wikilink")[受伤而无法再弹](../Page/受伤.md "wikilink")[結他](../Page/結他.md "wikilink")，也放弃了成为[歌星的梦想](../Page/歌星.md "wikilink")，成为普通的上班族——此时，（美國俚而幹出违规行为的马蒂惨遭解雇，“You
are
fired\!”（你被解雇了！）的[传真出现在了马蒂家的每个角落](../Page/传真.md "wikilink")。就在珍妮佛刚刚遇见中年的自己而晕倒时，[博士和马蒂及时赶到并把珍妮佛带走](../Page/博士.md "wikilink")，返回1985年。

回到1985年，两人惊讶地发现一切都变了：Hill
Valley变成了犯罪横行、暴徒肆虐的[地狱](../Page/地狱.md "wikilink")，毕夫成了有权有势的巨富，[父亲乔治已经在](../Page/父亲.md "wikilink")1973年遭到暗殺而身亡，而[母亲则被毕夫强娶](../Page/母亲.md "wikilink")，过着牛马般的生活。他們發現原來又是畢夫搞的鬼：2015年的老毕夫拿走了[博士扔掉的](../Page/博士.md "wikilink")[体育](../Page/体育.md "wikilink")[年鉴并偷走](../Page/年鉴.md "wikilink")[时光机将](../Page/迪羅倫時光機.md "wikilink")[年鉴給了](../Page/年鉴.md "wikilink")1955年的自己，毕夫以此透过彩票发家最终成为富翁。马蒂在向畢夫確認事件發生地點后，为了将偏离的历史拉回正轨，他們又來到了1955年，这一回，马蒂不仅要设法从毕夫手中追回[年鉴](../Page/年鉴.md "wikilink")，还要避免和正在撮合父母的另一个自己相遇。一番追逐之后，马蒂终于将[年鉴奪回并烧毁](../Page/年鉴.md "wikilink")。但在最後要回去的時候，一道[闪电击中时光車](../Page/闪电.md "wikilink")，时光車带着[博士消失无踪](../Page/博士.md "wikilink")。

马蒂正在茫然无措时，一个邮递员送来了一封信，居然是[博士在](../Page/博士.md "wikilink")1885年寫的，时光車被[闪电击中回到了](../Page/闪电.md "wikilink")1885年，信中告诉了马蒂时光車所在的位置，并交待他去找1955年的[博士寻求帮助](../Page/博士.md "wikilink")。这时，在[钟楼刚刚送走前一个马蒂的](../Page/钟楼.md "wikilink")[博士看到另一个马蒂又出现在面前](../Page/博士.md "wikilink")，不禁昏倒在地……

## 演員表

  - [Michael J. Fox](../Page/Michael_J._Fox.md "wikilink") as [Marty
    McFly](../Page/Marty_McFly.md "wikilink"), [Marty, Jr., and Marlene
    McFly](../Page/Marlene_McFly#Marty_.28Jr..29_and_Marlene_McFly.md "wikilink")
  - [Christopher Lloyd](../Page/Christopher_Lloyd.md "wikilink") as [Dr.
    Emmett "Doc" Brown](../Page/Emmett_Brown.md "wikilink")
  - [Thomas F. Wilson](../Page/Thomas_F._Wilson.md "wikilink") as [Biff
    Tannen](../Page/Biff_Tannen.md "wikilink") and [Griff
    Tannen](../Page/List_of_Back_to_the_Future_characters#Griff.md "wikilink")
  - [Lea Thompson](../Page/Lea_Thompson.md "wikilink") as [Lorraine
    Baines-McFly](../Page/List_of_Back_to_the_Future_characters#Lorraine_Baines-McFly.md "wikilink")
  - [Elisabeth Shue](../Page/Elisabeth_Shue.md "wikilink") as [Jennifer
    Parker](../Page/List_of_Back_to_the_Future_characters#Jennifer_Parker.md "wikilink")
  - [James Tolkan](../Page/James_Tolkan.md "wikilink") as [Mr.
    Strickland](../Page/List_of_Back_to_the_Future_characters#Gerald_Strickland.md "wikilink")
  - [Jeffrey Weissman](../Page/Jeffrey_Weissman.md "wikilink") as
    [George
    McFly](../Page/List_of_Back_to_the_Future_characters#George_McFly.md "wikilink")
      - [Crispin Glover](../Page/Crispin_Glover.md "wikilink") as George
        McFly (archival footage)
  - [Flea](../Page/Flea_\(musician\).md "wikilink") as [Douglas J.
    Needles](../Page/List_of_Back_to_the_Future_characters#Douglas_J._Needles.md "wikilink")

## 發展

Robert
Zemeckis說起初他並沒有拍續集的計劃，但[票房成功使拍續集勢在必行](../Page/票房.md "wikilink")。後來他同意拍續集，只是一定要[Michael
J. Fox和Christopher](../Page/Michael_J._Fox.md "wikilink")
Lloyd也回來拍攝。他們也同意後，[Robert
Zemeckis和Bob](../Page/Robert_Zemeckis.md "wikilink")
Gale一起為續集編寫[劇本](../Page/劇本.md "wikilink")。他們後悔第一集完結時把馬蒂的女朋友珍妮佛也加上去，以致他們編寫續集[劇本要加入珍妮佛的角色](../Page/劇本.md "wikilink")，而不能創作一個全新的冒險旅程。

因為當時[Robert
Zemeckis正忙於製作](../Page/Robert_Zemeckis.md "wikilink")[威探闖通關](../Page/威探闖通關.md "wikilink")，大部份[劇本內容都是由Bob](../Page/劇本.md "wikilink")
Gale編寫。當初，回到未來II的背景是1967年，但後來[Robert
Zemeckis認為](../Page/Robert_Zemeckis.md "wikilink")[回到未來的](../Page/回到未來.md "wikilink")[時間悖論提供了一個很好的機會去回到](../Page/時間悖論.md "wikilink")1955年，以另一角度看第一集的情節。大部份[演員也回來拍續集](../Page/演員.md "wikilink")，Crispin
Glover因酬金問題和監製一方談判成了一個頗大的絆腳石。他答覆不回來演出後，有佐治·麥佛萊的角色的劇情要從頭再寫，佐治·麥佛萊便在1985年劇情裏死去了。

最大的挑戰是要設計[未來的山谷市](../Page/未來.md "wikilink")，設計師Rick
Carter想要設計一個非常明顯地與[銀翼殺手沒有](../Page/銀翼殺手.md "wikilink")[煙和鉻合金](../Page/煙.md "wikilink")、不同氣氛的[未來](../Page/未來.md "wikilink")，他和最有天賦的人一起將山谷市轉變成未來的山谷市。

寫[劇本時](../Page/劇本.md "wikilink")，Bob Gale想加入更多笑料，[Robert
Zemeckis有點擔心描繪](../Page/Robert_Zemeckis.md "wikilink")[未來](../Page/未來.md "wikilink")，因為他不想冒險設計瘋狂、不準確的預測。根據Bob
Gale，他們希望[未來是好的](../Page/未來.md "wikilink")，即使有不好的地方，都只是因為人為而非[科技](../Page/科技.md "wikilink")，反對悲觀主義，[歐威爾主義在](../Page/歐威爾主義.md "wikilink")[科幻](../Page/科幻.md "wikilink")[小說中最為常見](../Page/小說.md "wikilink")。為保持成本低和趁著[Michael
J.
Fox有更多時間](../Page/Michael_J._Fox.md "wikilink")，回到未來II和[回到未來III同時拍攝](../Page/回到未來III.md "wikilink")。

## 製作

重建山谷市場景和編寫[劇本用了兩年時間](../Page/劇本.md "wikilink")。部分[角色的](../Page/角色.md "wikilink")[演員要化妝成老的樣子](../Page/演員.md "wikilink")，過程十分保密，[Michael
J.
Fox形容化妝過程很費時間](../Page/Michael_J._Fox.md "wikilink")，他說：「每次化妝都需要4小時才能完成。」回到未來II於1989年2月20日開拍，並在回到未來II將近完成的三個星期前，工作人員被分開。當大部份剩餘時間工作人員都在拍攝[回到未來III時](../Page/回到未來III.md "wikilink")，少數工作人員，其中包括[編劇兼](../Page/編劇.md "wikilink")[製片人Bob](../Page/製片人.md "wikilink")
Gale，集中完成回到未來II。拍攝期間，[導演](../Page/導演.md "wikilink")[Robert
Zemeckis每天只睡幾個小時](../Page/Robert_Zemeckis.md "wikilink")，一天內要來回製作回到未來II的[柏本克和拍攝](../Page/柏本克.md "wikilink")[回到未來III位於](../Page/回到未來III.md "wikilink")[加州的其他地方](../Page/加州.md "wikilink")。

### Crispin Glover的替換

[回到未來的製作人員請Crispin](../Page/回到未來.md "wikilink")
Glover繼續在回到未來II飾演佐治·麥佛萊的[角色](../Page/角色.md "wikilink")，他對此感興趣，但因酬金問題引起雙方爭執，Bob
Gale要求他在兩星期內答覆會否演出，兩星期後，他的經理人答覆他不會演出。後來，Crispin
Glover在電視節目中表示他不滿酬金只有125,000美金，不及其他回來演出的演員的一半，要求與[Michael
J.
Fox有相等的酬金](../Page/Michael_J._Fox.md "wikilink")，回到未來II那方不答應，他便辭演了。Bob
Gale斷言以Crispin Glover當時的名氣和地位，他要求的酬金簡直是「獅子開大口」。2012年的訪問中\[1\]，Crispin
Glover公開反駁了Bob
Gale的說法，他表示爭執的來源並非酬金問題，而是因為他在片場對第一集結局的處理方式提出質疑，他認為不該強調主角一家物質生活的改善，甚至讓主角獲得酷炫的新車。他認為因為自己提出質疑，使得Bob
Gale等人有意惡整他，設計他的角色在第二集必須辛苦的倒掛著演出，提供的片酬卻比其他配角還要少，他透過經紀人要求將片酬提高到與其他配角相等未果，才因此未演出第二集。

不過，佐治·麥佛萊的[角色還是要出現](../Page/角色.md "wikilink")，[Robert
Zemeckis取用了第一集裏Crispin](../Page/Robert_Zemeckis.md "wikilink")
Glover的一些片段，接駁第二集裏Jeffrey Weissman演出的片段。Jeffrey
Weissman在[電影裏要戴義肢](../Page/電影.md "wikilink")，包括假下巴、[鼻子](../Page/鼻子.md "wikilink")、[頰骨](../Page/頰骨.md "wikilink")，並用了不同混淆觀眾的方法，包括[背景](../Page/背景.md "wikilink")、戴[太陽眼鏡](../Page/太陽眼鏡.md "wikilink")、只出現他的背面、甚至把身體倒轉，令Jeffrey
Weissman看上去與Crispin Glover無異。Crispin
Glover對這樣的結果不滿，他也質疑同樣是替換演員演出的主角女友的角色，卻並沒有像他的角色一樣讓續集的演員刻意模仿前任演員的特徵。對此他正式提出[訴訟](../Page/訴訟.md "wikilink")，反對一眾監製，包括[Steven
Spielberg](../Page/Steven_Spielberg.md "wikilink")，根據是監製沒有Crispin
Glover的肖像權，亦沒有得到他的許可去使用。這促成了美國演員工會的交易條款：[監製和](../Page/監製.md "wikilink")[演員不能用任何方法去模仿其他](../Page/演員.md "wikilink")[演員的樣子](../Page/演員.md "wikilink")。

### Claudia Wells的替換

Claudia
Wells原來要在第二集裏重演[馬蒂麥佛萊的](../Page/馬蒂麥佛萊.md "wikilink")[女朋友珍妮花](../Page/女朋友.md "wikilink")·柏卡，但因為她的[母親病危](../Page/母親.md "wikilink")，所以不能演出。[監製換了Elisabeth](../Page/監製.md "wikilink")
Shue去飾演珍妮花·柏卡，因此第一集的結尾要重拍，[演員重拍多次以配合原來第一集的結尾](../Page/演員.md "wikilink")，但仍有一些漏洞，如博士安慰馬蒂他的[未來沒有問題時明顯地猶豫了一會等](../Page/未來.md "wikilink")。

Claudia
Wells幾乎是在回到未來II的十年後才回到[好萊塢](../Page/好萊塢.md "wikilink")，她是少數不於2002年版[回到未來系列](../Page/回到未來系列.md "wikilink")[DVD的額外特輯裏出現的](../Page/DVD.md "wikilink")[演員](../Page/演員.md "wikilink")。2010年，[回到未來系列推出藍光版本](../Page/回到未來系列.md "wikilink")，她在藍光版本裏全新的紀錄片Tales
from the
Future出現接受訪問。2011年，她終於有機會在[回到未來遊戲系列裏重演珍妮花](../Page/回到未来：游戏版.md "wikilink")·柏卡的[角色](../Page/角色.md "wikilink")，為遊戲系列的[角色](../Page/角色.md "wikilink")[配音](../Page/配音.md "wikilink")。

### 盤旋板騙局

[Robert
Zemeckis在第二集的製作特輯裏說盤旋板很久以前已發明了](../Page/Robert_Zemeckis.md "wikilink")，只是[家長因安全的緣故而沒公開出售](../Page/家長.md "wikilink")，他便取用了大批盤旋板。[觀眾信以為真](../Page/觀眾.md "wikilink")，到玩具店找査。Thomas
F. Wilson接受訪問時表示最熱門的問題都是關於盤旋板的真假。後來，Robert
Zemeckis在[第三集首映後表示盤旋板是經多種](../Page/第三集.md "wikilink")[特效製成](../Page/特效.md "wikilink")。

## 電影裏描述的未來

根據Robert
Zemeckis，《回到未來II》裏的2015年不是要準確預測[未來](../Page/未來.md "wikilink")，他説：「對於我來説，拍攝[未來的場景是我最不享受的](../Page/未來.md "wikilink")，因為我真的不喜歡嘗試預測[未來的](../Page/未來.md "wikilink")[電影](../Page/電影.md "wikilink")，唯一我真正享受的是[史丹利·庫柏力克的作品](../Page/史丹利·庫柏力克.md "wikilink")，他拍攝[發條橙時](../Page/發條橙.md "wikilink")，甚至沒有預測到[個人電腦](../Page/個人電腦.md "wikilink")。我們決定只要令[未來變得有趣就行](../Page/未來.md "wikilink")，而不是作準確的科學預測，因為我們很可能預測錯誤。」除此之外，[監製們做了些](../Page/監製.md "wikilink")[科學家預測的](../Page/科學家.md "wikilink")2015年的資料搜集。Bob
Gale說：「我們知道2015年不會有飛天車，但我們必須讓飛天車出現在續集裏。」

但是，《回到未來II》準確地預測了不少[未來發明](../Page/未來.md "wikilink")，如無處不在的[相機](../Page/相機.md "wikilink")，亞洲裔在[美國的影響](../Page/美國.md "wikilink")(儘管[電影上映時](../Page/電影.md "wikilink")[亞裔在](../Page/亞裔.md "wikilink")[美國的人口已經上升](../Page/美國.md "wikilink"))、能固定在牆上的平板電視、同時看六個[頻道的功能](../Page/頻道.md "wikilink")、網上\[視像聊天如[Skype](../Page/Skype.md "wikilink")、整容術的普及、[頭戴式顯示器和自動加油系統](../Page/頭戴式顯示器.md "wikilink")。《回到未來II》亦預測到不需要用手的電子遊戲如微軟[Kinect](../Page/Kinect.md "wikilink")，或至少，不需要用傳統遙控器的電玩如[Wii遙控器](../Page/Wii遙控器.md "wikilink")。
馬蒂穿的[Nike自動繋帶網球鞋](../Page/Nike.md "wikilink")(Nike Mag)很高需求，有些影迷以為Nike
Mag真的存在。[Nike最終發行了外形像馬蒂穿的Nike](../Page/Nike.md "wikilink")
Mag、最高的Hyperdunk鞋，2008年7月，影迷給Hyperdunk Supreme鞋命名為「Air
McFly」。2010年，一位受啟發的影迷Blake
Bevin創作了自動繫帶的鞋子。2010年8月尾，[Nike正式提出自動繫帶鞋的專利權](../Page/Nike.md "wikilink")，設計師造了一雙《回到未來II》裏馬蒂穿的Nike
Mag的複製品。2011年9月，Nike揭露Nike
Mag沒有自動繋帶功能。2014年，[Nike的設計師Tinker](../Page/Nike.md "wikilink")
Hatfield表示Nike將在2015年傳入自動繋帶的[科技](../Page/科技.md "wikilink")。

盤旋板（Hoverboard）的概念是：一塊能在空中飄浮的[滑板](../Page/滑板.md "wikilink")，這[科技在](../Page/科技.md "wikilink")《回到未來II》上映後一直有不同團體探討。在地上噴射[空氣](../Page/空氣.md "wikilink")、用途相似的盤旋船，已證明是可行的。最近與之概念相似的、是由[巴黎第七大學研究生發明的Magboard](../Page/巴黎第七大學.md "wikilink")。Magboard使用大量[超導現象板在Magboard的底部](../Page/超導現象.md "wikilink")，被液體[氮冷卻以達到](../Page/氮.md "wikilink")[邁斯納效應](../Page/邁斯納效應.md "wikilink")，讓Magboard在特定軌道上盤旋;Magboard在實際示範中，證明了它是有能力負擔一個人的重量。可是，因超導現象需要更高的溫度，周遭環境的溫度不能使Magboard應用在生活裏。2014年3月，一所名HURv
Tech的公司展示一塊運作中的盤旋板，短片裏還有一些[名人包括Christopher](../Page/名人.md "wikilink")
Lloyd，這段短片在數天內瀏覽人數即上升至一百萬，儘管這段短片不久後便被Funny and Die網站揭發是一場惡作劇。

片中2015年10月21日的3D看板快報播放[國家聯盟](../Page/國家聯盟.md "wikilink")[芝加哥小熊橫掃](../Page/芝加哥小熊.md "wikilink")[美國聯盟的](../Page/美國聯盟.md "wikilink")[邁阿密球隊](../Page/邁阿密.md "wikilink")，贏得[世界大賽冠軍的消息](../Page/2015年世界大賽#流行文化.md "wikilink")。現實中最接近的是[佛羅里達馬林魚於](../Page/佛羅里達馬林魚.md "wikilink")[2003年國家聯盟冠軍賽擊敗芝加哥小熊](../Page/2003年國家聯盟冠軍賽.md "wikilink")（馬林魚隨後於[2003年世界大賽擊敗](../Page/2003年世界大賽.md "wikilink")[紐約洋基](../Page/紐約洋基.md "wikilink")）；小熊在2015年10月21日遭[紐約大都會橫掃](../Page/紐約大都會.md "wikilink")，未能晉級世界大賽。此外，馬林魚隊於2015年球季藉《回到未來》推出「改寫未來」（Rewrite
The
Future）相關活動\[2\]；然而一年後的2016年10月22日，小熊在[2016年國家聯盟冠軍賽擊敗](../Page/2016年國家聯盟冠軍賽.md "wikilink")[洛杉磯道奇成功闖進世界大賽](../Page/洛杉磯道奇.md "wikilink")。並且在2016年11月3日以8比7擊敗[克里夫蘭印第安人贏得](../Page/克里夫蘭印第安人.md "wikilink")[2016年世界大賽](../Page/2016年世界大賽.md "wikilink")。

可能最大的矛盾是電影裏的飛天車和空中高速公路在未來中是否真的存在。不過，一輛原形飛天車在2013年公布了，價錢牌大約是$35,000，和「轉換飛行功能」的價錢相似。

## 反響

### 專業評價

[爛番茄網站上對這部](../Page/爛番茄.md "wikilink")[電影的好評度為](../Page/電影.md "wikilink")63%，在60條專業評論中，38條專業評論贊同，22條專業評論反對，平均分為6.1/10
，觀眾的評價則是85%，獲得全方面的讚譽。

### 票房

回到未來II於1989年11月22日(星期三)，感恩節之前一天在北美各戲院上映，在星期五至星期日內獲27.8百萬美金[票房](../Page/票房.md "wikilink")，在連續五天假期內獲43百萬美金收入。在接著的週末，收入跌了56%，只有12.1百萬美金，但仍保持第一位的成績。回到未來II在美國共獲得118.5百萬票房，在海外獲213百萬[票房](../Page/票房.md "wikilink")。回到未來II在1989年國内最成功的電影中排名第6位，全球最成功的電影中排名第3，僅次於[聖戰奇兵和](../Page/聖戰奇兵.md "wikilink")[蝙蝠俠](../Page/蝙蝠俠.md "wikilink")。

## 原聲帶

MCA, Inc.和Varese
Sarabande於1989年11月22日發行了回到未來II的原聲帶，Allmusic評價為4.5分(5分為滿分)。

與回到未來的原聲帶不同，回到未來II的原聲帶只收錄由Alan Silvestri的音樂作品，不收錄任何在回到未來II中出現的歌曲。

以下是原聲帶不收錄的歌曲:

  - [麥可·傑克森主唱](../Page/麥可·傑克森.md "wikilink")「Beat It」
  - Sammy Hagar主唱「I Can't Drive 55」
  - The Four Aces主唱「Mr. Sandman」
  - [派瑞·寇摩主唱](../Page/佩里·科莫.md "wikilink")「Papa Loves Mambo」

## 獎項及提名

  - [土星獎最佳特效獎](../Page/土星獎.md "wikilink")
  - [土星獎最佳科幻片](../Page/土星獎.md "wikilink")
  - [土星獎最佳化妝獎](../Page/土星獎.md "wikilink")
  - BMI電影配樂獎
  - [兒童選擇獎最喜愛的男演員](../Page/兒童選擇獎.md "wikilink")（米高霍士）
  - [兒童選擇獎最喜愛的女演員](../Page/兒童選擇獎.md "wikilink")（莉亞·湯遜）
  - [青年藝術家獎最佳家庭電影](../Page/青年藝術家獎.md "wikilink")-音樂劇或奇幻類
  - [奧斯卡金像獎最佳視覺效果獎](../Page/奧斯卡金像獎.md "wikilink")（提名）
  - [英國電影學院獎最佳特效獎](../Page/英國電影學院獎.md "wikilink")

## 回到未來日

[10-21-2015.jpg](https://zh.wikipedia.org/wiki/File:10-21-2015.jpg "fig:10-21-2015.jpg")不同緣故。\]\]
[2015_Back_to_the_Future_Day_by_The_White_House.jpg](https://zh.wikipedia.org/wiki/File:2015_Back_to_the_Future_Day_by_The_White_House.jpg "fig:2015_Back_to_the_Future_Day_by_The_White_House.jpg")慶祝「回到未來日」，卻使用第三集道具車構圖。\]\]
在這部電影中，布朗博士帶著主角馬蒂等人乘時光車，由1985年回到2015年10月21日，這天也因此被稱為「回到未來日」（）。相關慶祝活動如下：

  - 片商及贊助商

<!-- end list -->

  - [環球影業釋出片中](../Page/環球影業.md "wikilink")《大白鯊19》前導預告\[3\]，同[美泰兒將設計製作一套](../Page/美泰兒.md "wikilink")《回到未來》系列玩偶\[4\]；並在這天販售《[回到未來三部曲](../Page/回到未來系列.md "wikilink")》[Blu-ray](../Page/Blu-ray.md "wikilink")30週年紀念版\[5\]。
  - [Telltale Games也授權推出](../Page/Telltale_Games.md "wikilink")《回到未來：30
    週年紀念版遊戲》。\[6\]\[7\]\[8\]
  - [百事可樂在當天推出片中的](../Page/百事可樂.md "wikilink")「完美百事」（Pepsi
    Perfect），限量6500瓶。\[9\]
  - [福特汽車在](../Page/福特汽車.md "wikilink")[Focus](../Page/Ford_Focus.md "wikilink")、[Fiesta網頁上打出](../Page/Ford_Fiesta.md "wikilink")121萬美元追加「通量電容器」（Flux
    Capacitor）配備。\[10\]\[11\]\[12\]
  - [豐田汽車推出氫氣能源車](../Page/豐田汽車.md "wikilink")「Mirai」，找來當年主角拍攝廣告，並打造[鷗翼車門的概念車致敬](../Page/鷗翼車門.md "wikilink")；以2016年式Tacoma重現當年片中[Hilux樣式](../Page/豐田Hilux.md "wikilink")。\[13\]\[14\]
  - [任天堂推出片中](../Page/任天堂.md "wikilink")《[荒野槍手](../Page/荒野槍手.md "wikilink")》[Wii
    U](../Page/Wii_U.md "wikilink") [Virtual
    Console版](../Page/Virtual_Console.md "wikilink")。\[15\]
  - 《[今日美國](../Page/今日美國.md "wikilink")》於隔日販售同電影編排「山谷市版」特刊，與電影差別是修掉「[黛安娜皇后將訪華府](../Page/黛安娜王妃.md "wikilink")」（Queen
    Diana Will Visit Washington）小頭條、原「華府為黛安娜皇后來訪做準備」（Washington Prepares
    for Queen Diana's Visit）置換成「3D看板：言論自由或交通隱憂？」（3D Billboards: Free
    Speech or Traffic Hazards?）\[16\]\[17\]\[18\]\[19\]\[20\]
  - [Nike推出片中](../Page/Nike,_Inc..md "wikilink")[Nike
    Mag](../Page/Nike_Mag.md "wikilink")，除了讓[米高·福克斯穿上外](../Page/米高·福克斯.md "wikilink")，這款Nike
    Mag將於2016年透過拍賣方式售出，收益悉數捐給米高·福克斯帕金森氏症研究基金會。\[21\]

<!-- end list -->

  - 其它

<!-- end list -->

  - 美國[白宮也在這天追隨慶祝活動](../Page/白宮.md "wikilink")\[22\]；加拿大交通部亦針對該台改裝[DeLorean
    DMC-12發布召回](../Page/迪羅倫時光機.md "wikilink")\[23\]。
  - 本片要角在當天參加[今天和](../Page/今天_\(NBC\).md "wikilink")[吉米夜現場節目](../Page/吉米夜現場.md "wikilink")。\[24\]\[25\]\[26\]
  - 維吉尼亞州雷斯敦（Reston, Virginia）於2015年10月21日至25日電影節期間改名「山谷市」。 \[27\]

## 參考資料

## 參見

  - [回到未來系列](../Page/回到未來系列.md "wikilink")
  - [回到未來](../Page/回到未來.md "wikilink")
  - [回到未來III](../Page/回到未來III.md "wikilink")
  - [馬蒂麥佛萊](../Page/馬蒂麥佛萊.md "wikilink")

## 外部連結

  -
  -
  -
  -
  -
  - {{@movies|fBatm0784003}}

  -
  -
[Category:1989年電影](../Category/1989年電影.md "wikilink")
[Category:1980年代科幻片](../Category/1980年代科幻片.md "wikilink")
[Category:回到未來系列](../Category/回到未來系列.md "wikilink")
[Category:未來背景電影](../Category/未來背景電影.md "wikilink")
[Category:1955年背景電影](../Category/1955年背景電影.md "wikilink")
[Category:1985年背景電影](../Category/1985年背景電影.md "wikilink")
[Category:2015年背景電影](../Category/2015年背景電影.md "wikilink")
[Category:時間旅行電影](../Category/時間旅行電影.md "wikilink")
[Category:罗伯特·泽米吉斯电影](../Category/罗伯特·泽米吉斯电影.md "wikilink")
[Category:安培林娛樂電影](../Category/安培林娛樂電影.md "wikilink")

1.  <http://www.avclub.com/article/crispin-glover-67635>
2.  [Marlins Announce Back To The Future Season-Long Promotion to
    \#RewriteTheFuture](http://oncloudconine.mlblogs.com/2015/03/31/marlins-announce-back-to-the-future-season-long-promotion-to-rewritethefuture/)
    . On Cloud Conine (MLB.com Blogs), 2015-03-31.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13. ["Great Scott\! Back To The Future Toyota Tacoma Concept is
    awesome"](http://www.autoblog.com/2015/10/21/back-to-the-future-toyota-tacoma-concept/)
    from *[Autoblog.com](../Page/Autoblog.com.md "wikilink")* (October
    21, 2015)
14. ["WATCH: 'Back to the Future' Toyota commercial filmed in
    N.J."](http://www.nj.com/entertainment/index.ssf/2015/10/watch_back_to_the_future_toyota_commercial_filmed.html)
    from *[nj.com](../Page/nj.com.md "wikilink")* (October 21, 2015)
15.
16. [30 'Back to the Future' facts that'll make you say 'Great
    Scott\!'](http://ftw.usatoday.com/2015/10/back-to-the-future-facts-about-trilogy-future-day-eric-stoltz-video-trivia-marty-mcfly-spielberg-zemeckis).
    For The Win ([USA Today](../Page/USA_Today.md "wikilink")).
    2015-10-21.
17. ['Back to the Future' auction sells USA TODAY (and I want
    it)](http://content.usatoday.com/communities/popcandy/post/2010/10/back-to-the-future-auction-sells-usa-today-and-i-want-it/1).
    Pop Candy ([USA Today](../Page/USA_Today.md "wikilink")).
    2010-10-18.
18. [Print journalism takes back the future, with the help of Doc Brown
    and Marty
    McFly](http://www.hitfix.com/the-dartboard/print-journalism-takes-back-the-future-with-the-help-of-doc-brown-and-marty-mcfly).
    [HitFix](../Page/HitFix.md "wikilink"). 2015-10-21.
19. ['Back to the Future Part II' newspaper's Oct. 22 headlines feel
    strangely
    spot-on](http://www.today.com/popculture/back-future-part-ii-newspapers-oct-22-headlines-feel-strangely-t51756).
    [Today](../Page/今天_\(NBC\).md "wikilink"). 2015-10-22.
20.
21.
22. [Follow Along: It's Back to the Future
    Day](https://www.whitehouse.gov/blog/2015/10/20/back-to-the-future-day).
    White House. 2015-10-21.
23.
24. ['Back to the Future' cast reunites, flashes back to 'monumental'
    time
    together](http://www.today.com/popculture/back-future-cast-reunites-flashes-back-monumental-time-together-t51401).
    [Today](../Page/今天_\(NBC\).md "wikilink"). 2015-10-21.
25. [Fox, Lloyd Go 'Back To The Future’ On ‘Jimmy Kimmel
    Live\!'](http://losangeles.cbslocal.com/2015/10/22/fox-lloyd-go-back-to-the-future-on-jimmy-kimmel-live).
    CBS Los Angeles. 2015-10-22.
26.
27.