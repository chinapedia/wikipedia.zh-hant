**会计信息系统**
是指以[会计语言为主要表达方式](../Page/会计.md "wikilink")，以会计理论为理论基础，对会计主体内会计信息所进行的收集、加工、传递、反馈等一系列工作所构成的整个信息体系。

在利用电子计算机技术以后，由于计算手段的改进，使得信息在收集、传递、反馈等多方面变的更为高效。

在台灣稱之為「會計資訊系統」

## 著名的會計信息系統軟件

  - SunSystem
  - CICS
  - Platinum
  - Macola
  - [Peachtree Accounting](../Page/Peachtree_Accounting.md "wikilink")
    [1](http://en.wikipedia.org/wiki/Peachtree_Accounting)
    [2](http://www.peachtree.com.hk/products/cproducts.htm)
  - [DacEasy Accounting](../Page/DacEasy_Accounting.md "wikilink")
    [3](http://www.daceasy.com/daceasy/default.asp)
  - [SAP](../Page/SAP公司.md "wikilink")
  - [MYOB](../Page/MYOB.md "wikilink")
    [4](http://en.wikipedia.org/wiki/MYOB_%28software%29)
  - [AccPac](../Page/AccPac.md "wikilink")
    [5](http://en.wikipedia.org/wiki/Accpac)
  - [Oracle Financials](../Page/Oracle_Financials.md "wikilink")
    [6](http://en.wikipedia.org/wiki/Oracle_Applications)
  - [QuickBooks](../Page/QuickBooks.md "wikilink")
    [7](http://en.wikipedia.org/wiki/QuickBooks)
  - [金蝶國際](../Page/金蝶國際.md "wikilink")
    [8](https://web.archive.org/web/20070129011856/http://www.kingdee.com/solutions05/operation_financial.jsp)
  - [webERP](../Page/webERP.md "wikilink")
    [9](https://web.archive.org/web/20100117224602/http://www.weberp.org/HomePage)

## 相關

  - [會計系統](../Page/會計系統.md "wikilink")

[Category:会计学](../Category/会计学.md "wikilink")
[Category:应用软件](../Category/应用软件.md "wikilink")