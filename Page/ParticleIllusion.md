  -

[Pi-explosion.jpg](https://zh.wikipedia.org/wiki/File:Pi-explosion.jpg "fig:Pi-explosion.jpg")。左上角陳列了此效果使用了的[貼圖](../Page/貼圖.md "wikilink")。爆炸發射器本身應用了[超級發射器的功能](../Page/超級發射器.md "wikilink")，在製作煙花及爆炸效果時經常會被用上。\]\]
**particleIllusion**（官方簡稱為**pIllusion**；中文直譯為**粒子幻覺**），是一個主要以[Windows為平臺獨立運作的](../Page/Windows.md "wikilink")[電腦動畫](../Page/電腦動畫.md "wikilink")[軟體](../Page/軟體.md "wikilink")。pIllusion的唯一主力範疇是以[粒子系统的技術創作諸如](../Page/粒子系统.md "wikilink")[火](../Page/火.md "wikilink")、[爆炸](../Page/爆炸.md "wikilink")、[煙霧及](../Page/煙霧.md "wikilink")[煙花等動畫效果](../Page/煙花.md "wikilink")。pIllusion的前身為由Impulse
Inc. 代理的**Illusion
2**（1999\~2001），由於主[程序員與該公司意見不合而離開及創立新公司Wondertouch](../Page/程序員.md "wikilink")，將Illusion
2功能升級及更新商標為**particleIllusion 3.0**。

## 概觀

pIllusion
3及以下版本本身只以[二維空間作平臺](../Page/二維.md "wikilink")，不設光源及粒子相互碰撞計算。在接受[OpenGL的支持下可以做出高準確度的實時預瀏](../Page/OpenGL.md "wikilink")。粒子動態的隨機性可以提供有如在[三維空間的錯覺](../Page/三維.md "wikilink")。發射器提供充足的圖表式數據輸入更改及控制粒子發射後的各種行為。由於pIllusion不具有過多“額外”濾鏡功能，因此[渲染速度極快](../Page/渲染.md "wikilink")，加上直觀的控制界面，使用者可以很快上手製作粒子動畫。

### 3.0其他功能

  - [動態模糊除了原有的級別外](../Page/動態模糊.md "wikilink")，新增「高品質」級別。高品質動態模糊不會如原級般劣化畫面質量。
  - [超級發射器](../Page/超級發射器.md "wikilink")，將原是「發射粒子的發射器」改變為「發射粒子發射器的發射器」。
  - 障礙邊，阻塞或反彈接觸線邊的粒子活動。
  - 遮蓋面，遮蓋在平面下的圖像。
  - 力場，更改進入力場平面的粒子移動方向。
  - 與[After
    Effects以txt純文字檔案互換發射器的移動軌道](../Page/After_Effects.md "wikilink")。
  - 多[圖層作業](../Page/圖層.md "wikilink")。
  - 每圖層可輸入單格／動畫圖組或影像檔案作背景或[阿尔法通道用](../Page/阿尔法通道.md "wikilink")。
  - 粒子可從圖層背景取色，多用於製造瓦解圖像或模擬折射效果。

## 應用

中國大陸及臺灣出產的[電腦遊戲中](../Page/電腦遊戲.md "wikilink")，有很多都是從官網免費發佈的發射器收藏庫（emitter
library）直接提取現成發射器作魔法及武技動畫效果。但pIllusion一直都是將自己定位為動畫中後期製作，它不支持對實時渲染輸出。並且高階使用者一般都不會將pIllusion當作最終渲染階段，而是將其渲染的動畫圖組輸入到如[After
Effects等的軟體作最後合成](../Page/After_Effects.md "wikilink")。Wondertouch的官方討論區經常會有要求推出真正三維版pIllusion的意見，但開發者一般都會以不希望犧牲程式作業效率為理由而拒絕。

## 外部連結

  - [美國particleIllusion官方網頁](https://web.archive.org/web/20061108143415/http://www.wondertouch.com/)

  - [臺灣中文版代理商網頁](http://www.digivision.com.tw/pillusion/index.html)

[Category:動畫軟件](../Category/動畫軟件.md "wikilink")