《**倉庫番**》是一款經典[電子遊戲](../Page/電子遊戲.md "wikilink")，1982年由[日本](../Page/日本.md "wikilink")[Thinking
Rabbit公司首次發行](../Page/Thinking_Rabbit.md "wikilink")。之後其他遊戲开发者爭相製作仿製或衍生作品。致使*倉庫番*成为此类游戏的代名词。遊戲要求玩家在[二維](../Page/二維.md "wikilink")[地圖上把](../Page/地圖.md "wikilink")[箱子推到指定地點](../Page/箱子.md "wikilink")，當中牽涉到大量的空間邏輯推理。

## 倉庫番的規則

[KSokoban-screenshot.png](https://zh.wikipedia.org/wiki/File:KSokoban-screenshot.png "fig:KSokoban-screenshot.png")內建的倉庫番遊戲KSokoban\]\]
第一個《倉庫番》的遊戲規則，則是扮演工人的玩家，以「推」的方式，推動箱子。可以在沒有阻礙物（如牆壁等的阻礙物）的情況下，向[左](../Page/左.md "wikilink")、[右](../Page/右.md "wikilink")、[上](../Page/上.md "wikilink")、[下的地方移動](../Page/下.md "wikilink")。將箱子移動到指定點，達到指定數量，即可過關。

但玩家移動箱子，有下列條件要注意：

  - 推到牆壁的箱子，玩家就不可以背對牆壁，把箱子推回到空處。即箱子只能以「被推」的方式被移動，不是以「被拉」的方式被移動。但如果玩家推至牆壁後，垂直牆壁的兩側沒有阻礙物，則玩家可以朝這兩個不同的方向推移箱子。
  - 一旦箱子被移動到角落，玩家沒有任何方法再移動這個被推到角落的箱子。
  - 玩家不可同時推動兩個或以上的箱子。假設工人面前有一個箱子，箱子的正前方又有一個箱子，則這兩個箱子是不能被推動的。

## 倉庫番與數學

倉庫番的解的[複雜度被](../Page/複雜度類.md "wikilink")[數學家證明了達到](../Page/數學家.md "wikilink")[NP難](../Page/NP_\(複雜度\).md "wikilink")（NP-hard）的程度\[1\]，後來更被證明了它是[PSPACE完全的](../Page/PSPACE.md "wikilink")\[2\]。

## 規則變體

基於基礎的規則，有的遊戲添加了數量有限的炸彈破壞牆壁方可達成目標；重力添加到遊戲中；可以改變特性的牆壁、機關；可以收集的物品（原本是障礙物）。

## 參見

  - [迷宮](../Page/迷宮.md "wikilink")
  - [倉庫](../Page/倉庫.md "wikilink")

## 其他

  - [Rocks'n'Diamonds](../Page/Rocks'n'Diamonds.md "wikilink")
  - [Xye](../Page/Xye.md "wikilink")

## 參考文獻

<references/>

## 外部連結

  - [Official Sokoban site](http://www.sokoban.jp/)（in Japanese）

  - [推箱子游戏的自动求解](http://blog.csdn.net/hellwolf/archive/2005/01/04/239939.aspx)

  - [倉庫番史上完全版](http://www.gamebase.com.tw/wekey/pc/newgame.php?gno=3229)〈[大宇製作的仿製品](../Page/大宇資訊股份有限公司.md "wikilink")〉

[Category:1982年电子游戏](../Category/1982年电子游戏.md "wikilink")
[Category:Linux遊戲](../Category/Linux遊戲.md "wikilink") [Category:Mac
OS游戏](../Category/Mac_OS游戏.md "wikilink") [Category:Windows Mobile
Professional游戏](../Category/Windows_Mobile_Professional游戏.md "wikilink")
[Category:Windows游戏](../Category/Windows游戏.md "wikilink")
[Category:免費遊戲](../Category/免費遊戲.md "wikilink")
[Category:益智游戏](../Category/益智游戏.md "wikilink")
[Category:邏輯謎題](../Category/邏輯謎題.md "wikilink")

1.
2.  Joseph C. Culberson, [Sokoban is
    PSPACE-complete](http://citeseer.ist.psu.edu/culberson97sokoban.html).
    Technical Report TR 97-02, Dept. of Computing Science, University of
    Alberta, 1997. Also:
    <http://web.cs.ualberta.ca/~joe/Preprints/Sokoban>