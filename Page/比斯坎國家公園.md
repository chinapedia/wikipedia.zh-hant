**比斯坎國家公園**（）是[美國](../Page/美國.md "wikilink")[國家公園](../Page/國家公園.md "wikilink")，位於南[佛羅里達](../Page/佛羅里達.md "wikilink")，[霍姆斯特德以东](../Page/霍姆斯特德_\(佛羅里達州\).md "wikilink")。比斯坎國家公園保护的[比斯坎灣是美國](../Page/比斯坎灣.md "wikilink")[水肺潛水聖地之一](../Page/水肺潛水.md "wikilink")。公园面积700平方公里，其中95%是水域面积。此外比斯坎灣的海岸有很大的[红树林](../Page/红树林.md "wikilink")。

公园内最大的岛被看作是佛罗里达最早由[珊瑚形成的岛屿](../Page/珊瑚.md "wikilink")。公园最北部的岛屿是珊瑚岛和[沙洲之间的过度](../Page/沙洲.md "wikilink")。

## 历史

1968年10月18日设立比斯坎国家保护区，1980年6月28日被提升为国家公园。

## 名胜

公园最大的名胜是在海湾的[珊瑚礁区水肺潜水](../Page/珊瑚礁.md "wikilink")。公园里有底部是玻璃的船供游客游览海湾，也可以租[皮艇来考察海湾和](../Page/皮艇.md "wikilink")[佛羅里達礁島群](../Page/佛羅里達礁島群.md "wikilink")。

## 相片

<center>

[File:Biscaynemap.jpg|衛星照片](File:Biscaynemap.jpg%7C衛星照片)
[File:Biscayne_National_Park.jpg|公園大部份是水域](File:Biscayne_National_Park.jpg%7C公園大部份是水域)

</center>

## 参考资料

## 外部链接

  - Official site: [Biscayne National
    Park](http://www.nps.gov/bisc/index.htm)
  - [Florida Keys Tourism Board](http://www.fla-keys.com)

[Category:佛羅里達州國家公園](../Category/佛羅里達州國家公園.md "wikilink")
[Category:海洋國家公園](../Category/海洋國家公園.md "wikilink")
[比斯坎國家公園](../Category/比斯坎國家公園.md "wikilink")
[Category:1980年設立的保護區](../Category/1980年設立的保護區.md "wikilink")
[Category:1980年佛羅里達州建立](../Category/1980年佛羅里達州建立.md "wikilink")