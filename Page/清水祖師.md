[三峽長福巖.jpg](https://zh.wikipedia.org/wiki/File:三峽長福巖.jpg "fig:三峽長福巖.jpg")\]\]
[清水祖師像.jpg](https://zh.wikipedia.org/wiki/File:清水祖師像.jpg "fig:清水祖師像.jpg")清水祖師像\]\]

**清水祖師**，法號**普足**，俗名**陳昭應**（一說為**陳榮祖**、**陳昭**或**陳應**，），本籍[福建](../Page/福建.md "wikilink")[永春](../Page/永春.md "wikilink")，是[北宋時代福建](../Page/北宋.md "wikilink")[泉州](../Page/泉州.md "wikilink")[安溪的高僧](../Page/安溪.md "wikilink")。由於在蓬萊山（今屬安溪縣[蓬萊鎮](../Page/蓬萊鎮.md "wikilink")）[清水巖修道](../Page/安溪清水巖.md "wikilink")，鋪橋造路，廣施醫藥，被尊稱為**清水祖師**，又稱**蓬萊祖師**、**麻章上人**，俗稱「祖師公」，[圓寂後](../Page/圓寂.md "wikilink")，求雨驅蟲，屢禱屢靈，[南宋](../Page/南宋.md "wikilink")[宋孝宗時成為朝廷封賜的神靈](../Page/宋孝宗.md "wikilink")，直到[宋寧宗](../Page/宋寧宗.md "wikilink")[追封為](../Page/追封.md "wikilink")**昭應廣惠慈濟善利大師**\[1\]，原屬於[佛教](../Page/佛教.md "wikilink")[禪宗的清水祖師](../Page/禪宗.md "wikilink")，逐漸民間化、[道教化](../Page/道教.md "wikilink")（道教信徒稱之為「[黑帝化身清水真人](../Page/黑帝.md "wikilink")」），被安溪人視為地方最重要的守護神，是[安溪諸聖之首](../Page/安溪諸聖.md "wikilink")\[2\]。許多安溪人以種茶為業，再加上清水祖師以求雨聞名，也被視為安溪[鐵觀音的保護神](../Page/鐵觀音.md "wikilink")。另外，許多安溪人信奉的神祇，如[保儀尊王](../Page/保儀尊王.md "wikilink")、[保儀大夫](../Page/保儀大夫.md "wikilink")、清水祖師、[法主真君都被視為有保護茶葉種植的職能](../Page/法主真君.md "wikilink")。在[閩南](../Page/閩南.md "wikilink")，清水祖師是重要的鄉土神靈，是[應惠靈天之一](../Page/應惠靈天.md "wikilink")。隨著安溪移民來臺，清水祖師的信仰在[臺灣也蓬勃發展](../Page/臺灣.md "wikilink")，在臺灣，1994年時有清水祖師為主神的廟宇有98座，[大臺北就有](../Page/大臺北.md "wikilink")63座，尤其是[大文山地區](../Page/大文山.md "wikilink")。[大臺北地區可說是清水祖師信仰最盛之地](../Page/大臺北.md "wikilink")，[三峽長福巖祖師廟](../Page/三峽長福巖祖師廟.md "wikilink")、[艋舺清水巖](../Page/艋舺清水巖.md "wikilink")、[淡水清水巖](../Page/淡水清水巖.md "wikilink")、[瑞芳祖師廟](../Page/瑞芳祖師廟.md "wikilink")，號稱「大臺北四大祖師廟」\[3\]\[4\]。

## 家世

清水祖師生於書香[世家](../Page/世家.md "wikilink")，是[宋福建名家](../Page/宋.md "wikilink")[陳瑊的後裔](../Page/陳瑊.md "wikilink")，清水祖師之太祖、曾祖、大伯、二伯、四叔、弟陳夢得均考中[進士](../Page/進士.md "wikilink")。祖師之父陳機更是滿腹經綸，以作[詩聞名一時](../Page/詩.md "wikilink")。

## 生平

清水祖師生于[宋仁宗](../Page/宋仁宗.md "wikilink")[慶曆七年](../Page/慶曆.md "wikilink")（1047年）\[5\]，福建省[永春县小岵鄉](../Page/永春县.md "wikilink")（古作**小姑鄉**）人\[6\]。

清水祖師七歲時，即在大雲院[出家為](../Page/出家.md "wikilink")[沙彌](../Page/沙彌.md "wikilink")，法名**普足**，後來师从大静山[长老](../Page/长老.md "wikilink")「明禅师」，習經、[坐禪三年](../Page/坐禪.md "wikilink")，後返高泰山，劝造数十條橋梁。又移住麻章庵，多次舉辦法會，為民眾祈雨消災，無不感應，獲得檀越崇信，人称**麻章上人**。遍訪名山後，居於永春縣，以「道行精嚴」聞名[閩南](../Page/閩南.md "wikilink")。

[宋神宗](../Page/宋神宗.md "wikilink")[元豐六年](../Page/元豐.md "wikilink")（1083年），清水祖師被素仰其道行的劉公銳請到福建省[安溪縣求雨](../Page/安溪.md "wikilink")，非常成功。故當地百姓極力挽留之。於是清水祖師定居安溪，而劉公銳慨然捐出大筆土地，在安溪蓬萊山「張巖」建庵，故又称**蓬萊祖師**。祖師見「張巖」之「清泉不竭」，便改[張巖之名為](../Page/張巖.md "wikilink")[清水巖](../Page/清水巖.md "wikilink")。清水巖之庵本為草庵，多賴清水祖師和徒弟楊道、周明等幾度拓展，終於開拓寺宇。

劉公銳篤信佛法，堅守[梵行](../Page/梵行.md "wikilink")，不僅布施，也跟祖師有深厚的交情。祖師生前即交代劉公銳，不願意製作[全身舍利](../Page/全身舍利.md "wikilink")。《清水祖師本傳》：「乘劉公銳至巖，祖師囑以後事，謂：『形骸外物，漆身無益』。」《清水祖師本傳》：「立（劉公銳）為[檀越主](../Page/檀越.md "wikilink")，祀於岩左東軒。凡春日抬大師像下山迎香，必以公銳像配迎駕前，蓋所以報其功也。」

清水祖師熱心於[公益](../Page/公益.md "wikilink")，多次募捐款項，號召造橋鋪路，一生興建過幾十座[橋樑](../Page/橋樑.md "wikilink")。並且醫道高明，常常施藥救助人民。不僅閩南泉州，連[建](../Page/建寧.md "wikilink")、[邵](../Page/邵武.md "wikilink")、[興](../Page/興化府.md "wikilink")、[劍](../Page/劍州.md "wikilink")、[汀](../Page/汀州府.md "wikilink")、[漳各](../Page/漳州府.md "wikilink")[郡人士皆相當崇敬](../Page/郡.md "wikilink")，[施主尊崇](../Page/施主.md "wikilink")，[布施極盛](../Page/布施.md "wikilink")。[明朝](../Page/明朝.md "wikilink")[何喬遠](../Page/何喬遠.md "wikilink")《[閩書](../Page/閩書.md "wikilink")》：普足（清水祖師）術行建、劍、汀、漳間，檀施為盛，居巖十九年。[清朝道光重纂](../Page/清朝.md "wikilink")《福建通志》：普足名重建、劍、汀、漳間，檀施為盛，居巖十九年。

[宋徽宗](../Page/宋徽宗.md "wikilink")[建中靖國元年](../Page/建中靖國.md "wikilink")（1101年）五月十三，清水祖師[圓寂於清水巖](../Page/圓寂.md "wikilink")，安溪縣民感激清水祖師一生的義舉，自發性地运石建造[卒塔婆](../Page/卒塔婆.md "wikilink")，供奉祖師的[舍利子](../Page/舍利子.md "wikilink")，名曰“真空塔”，并以[沈香之木](../Page/沈香.md "wikilink")，刻祖師像，奉於清水巖殿中，而故日後奉祀清水祖師之寺剎，多名為「清水巖」，如[艋舺清水巖](../Page/艋舺清水巖.md "wikilink")、[淡水清水巖等](../Page/淡水清水巖.md "wikilink")。從此，清水祖師成為安溪縣民信仰最誠的地方神祇。[政和三年](../Page/政和.md "wikilink")（1113年），[县令陈浩然作清水祖師傳](../Page/县令.md "wikilink")。

### 異說

有人認為清水祖師出生於書香[門第](../Page/門第.md "wikilink")，不應自幼[剃髮為僧](../Page/剃髮.md "wikilink")。於是出現了幾種說法：

一、隱居說：清水祖師有濃厚的愛國思想，被其父影響，在亂世之中，以隱居明志，並非真正遁入空門。但清水祖師幼年懵懂時即出家，考其出家原因，似乎無法聯繫「隱居明志」、「愛國思想」。

二、抗金說：有人認為清水祖師出家的目的，是掩飾其對抗金活動的支持，甚至，其本人也是抗金志士。還有廟宇居然說清水祖師本是[岳飛或](../Page/岳飛.md "wikilink")[韓世忠](../Page/韓世忠.md "wikilink")[幕府的](../Page/幕府.md "wikilink")[軍師](../Page/軍師.md "wikilink")，因[漢人對](../Page/漢人.md "wikilink")[女真](../Page/女真.md "wikilink")[稱藩](../Page/藩屬.md "wikilink")[納貢](../Page/納貢.md "wikilink")，一怒之下才[剃髮](../Page/剃髮.md "wikilink")[出家](../Page/出家.md "wikilink")。但據研究，祖師[圓寂於](../Page/圓寂.md "wikilink")1101年，而[金朝則是](../Page/金朝.md "wikilink")1115年才初創，到1125年[遼朝亡後](../Page/遼朝.md "wikilink")，金始攻宋，而宋朝的抗金活動才開始。抗金之說，根本於年代不合。

三、抗元說：與上述說法雷同，只是把抵抗的金朝改為[元朝](../Page/元朝.md "wikilink")，甚至說是[文天祥部下的](../Page/文天祥.md "wikilink")[軍官](../Page/軍官.md "wikilink")，因[起義事敗](../Page/起義.md "wikilink")，[隱居於安溪縣清水巖](../Page/隱居.md "wikilink")，此說更是無稽。祖師在北宋時已然[涅槃歸西](../Page/涅槃.md "wikilink")，根本沒有元朝可抗。為了掩飾此說，又有好事者將祖師[籍貫](../Page/籍貫.md "wikilink")、生年隨意塗改，更為[南宋末年](../Page/南宋.md "wikilink")[汴梁](../Page/汴梁.md "wikilink")[開封府人](../Page/開封府.md "wikilink")，毫無理據，甚至還說[朱元璋建立](../Page/朱元璋.md "wikilink")[明朝之後](../Page/明朝.md "wikilink")，為了感動其[民族英雄義行](../Page/民族英雄.md "wikilink")，[追封為](../Page/追封.md "wikilink")「[護國公](../Page/護國公.md "wikilink")」或「[護國大將軍](../Page/護國大將軍.md "wikilink")」，更是無中生有。

在宋代福建，佛教非常興盛，寺院之多，出家人數之眾，都是中國的首位，時人有「閩中塔廟之盛甲於天下」和「山路逢人半是僧」的說法。而且當時的福建，出家為僧不但受人景仰，更是一種時尚，許多書香門第的子弟都出家為僧，清水祖師只是這些人當中的一個，並非異數。沒有必要將之特異化，並加上「抗金」、「抗元」傳說。

另外，《桃源南山陳氏族譜》記載：「（清水祖師）兒時[持齋誦經](../Page/持齋.md "wikilink")，日常與山下里人牧牛子戲，日暮吟經，牛自知歸。」可見得祖師幼時對閩地盛行的[佛法也有相當興趣](../Page/佛法.md "wikilink")，因一心向佛而出家，並非如坊間誤傳，以出家掩飾其反抗[女真人或](../Page/女真人.md "wikilink")[蒙古人的身分](../Page/蒙古人.md "wikilink")。

## 功績

清水祖師的功績主要如下：

造橋：造橋鋪路，便民甚多。清水祖師於永春「勸造橋樑數十」。到了安溪，又募捐勸造通泉橋、谷口橋、汰口橋等。清水祖師一生募捐，修造幾十座橋樑，對民眾生活、行動幫助極大，自然得到民眾的感佩。

施藥：所謂救人一命，勝造七級[浮屠](../Page/浮屠.md "wikilink")。施藥活人，是極大的善舉。清水祖師以其高超之醫術，在瘴癘之氣盛行的閩南一帶，救治了不少患病的民眾，而患者無不感激、愛戴。

求雨：一般人認為，普通人不能掌握自然現象，如下雨與否，唯有神聖，方能成之。但清水祖師在世時，就已經屢次祈禱成功，讓蒼天降下甘霖，被認為有功於解決多次的旱災。也因為求雨屢次的成功，被視為神蹟，清水祖師也被賦予相當的神秘色彩。

## 封號

由於清水祖師圓寂後，被奉為神，不忘蒼生。多次求雨驅蟲，每禱則應，靈驗非常。地方人士上報[朝廷](../Page/朝廷.md "wikilink")，屢屢賜封。封號如下：

[南宋孝宗](../Page/南宋.md "wikilink")[隆興二年](../Page/隆興.md "wikilink")（1164年），[敕封清水祖師為](../Page/敕.md "wikilink")「**昭應大師**」。

[宋孝宗](../Page/宋孝宗.md "wikilink")[淳熙十一年](../Page/淳熙.md "wikilink")（1184年），加封為「**昭應慈濟大師**」。

[宋寧宗](../Page/宋寧宗.md "wikilink")[嘉泰元年](../Page/嘉泰.md "wikilink")（1201年），再加封為「**昭應廣惠慈濟大師**」。

宋寧宗[嘉定三年](../Page/嘉定.md "wikilink")（1210年），再加封為「**昭應廣惠慈濟善利大師**」。

## 從神

楊[尊者](../Page/尊者.md "wikilink")（楊道）、周尊者（周明）、劉[檀越](../Page/檀越.md "wikilink")（劉公銳）、陳[邑宰](../Page/邑宰.md "wikilink")（陳浩然）、薛[員外](../Page/員外.md "wikilink")（薛顓）、[四大護法張黃蘇李四將軍](../Page/四大護法#福建.md "wikilink")（一說：趙王蘇李四將軍）等。

宜蘭市光明寺將清水祖師之兩侍者稱為招應尊者、護應尊者，並裝設有大型神偶。\[7\]

## 廟宇

除了「大臺北四大祖師廟」、[石泉巖](../Page/石泉巖.md "wikilink")、[景美萬慶巖](../Page/景美萬慶巖.md "wikilink")、[萬隆會元巖](../Page/景美會元洞清水祖師廟.md "wikilink")、[土城永福巖](../Page/土城永福巖.md "wikilink")、桃園市蘆竹區福隆巖（在地人士稱「新莊仔廟」）、[新竹慈雲庵等與數十座](../Page/新竹慈雲庵.md "wikilink")[北臺灣地區的祖師廟之外](../Page/北臺灣.md "wikilink")，清水祖師在[南臺灣也不乏其廟](../Page/南臺灣.md "wikilink")，如[台南三級古蹟佳里興震興宮](../Page/台南三級古蹟佳里興震興宮.md "wikilink")（古天興縣治所在地）[嘉義縣](../Page/嘉義縣.md "wikilink")[蘭潭](../Page/蘭潭.md "wikilink")[清水寺](../Page/清水寺.md "wikilink")（台灣奇案曾在此拍攝）、[臺南市](../Page/臺南市.md "wikilink")[南化區](../Page/南化區.md "wikilink")[溪尾清水寺](../Page/溪尾清水寺.md "wikilink")、[臺南四鯤鯓龍山寺](../Page/四鯤鯓龍山寺.md "wikilink")、[善化](../Page/善化區.md "wikilink")[北仔店清水宮](../Page/北仔店清水宮.md "wikilink")、[高雄市](../Page/高雄市.md "wikilink")[前金萬興宮](../Page/前金萬興宮.md "wikilink")、[新興區](../Page/新興區.md "wikilink")[大港埔祖師廟](../Page/大港埔祖師廟.md "wikilink")、[內惟赤龍宮](../Page/內惟赤龍宮.md "wikilink")、[左營洲仔清水宮](../Page/左營洲仔清水宮.md "wikilink")、[仁武福清宮](../Page/仁武福清宮.md "wikilink")、[屏東縣](../Page/屏東縣.md "wikilink")[崁頂鄉力社村](../Page/崁頂鄉.md "wikilink")[北院廟](../Page/北院廟.md "wikilink")、[屏東縣](../Page/屏東縣.md "wikilink")[萬丹鄉](../Page/萬丹鄉.md "wikilink")[清水寺](../Page/清水寺.md "wikilink")、[台南市](../Page/台南市.md "wikilink")[後壁區](../Page/後壁區.md "wikilink")[長短樹永安堂](../Page/長短樹永安堂.md "wikilink")、[台南市](../Page/台南市.md "wikilink")[後壁區](../Page/後壁區.md "wikilink")[安溪寮](../Page/安溪寮.md "wikilink")[福安寺](../Page/福安寺.md "wikilink")、[台南市](../Page/台南市.md "wikilink")[新化區](../Page/新化區.md "wikilink")[清水寺](../Page/清水寺.md "wikilink")、[澎湖](../Page/澎湖.md "wikilink")[文澳祖師廟等皆以清水祖師為主神](../Page/文澳祖師廟.md "wikilink")。另外，[深坑集順廟](../Page/深坑集順廟.md "wikilink")、[汐止濟德宮](../Page/汐止濟德宮.md "wikilink")、[松山慈祐宮](../Page/松山慈祐宮.md "wikilink")、[桃園市](../Page/桃園市.md "wikilink")[八德三元宮](../Page/八德三元宮.md "wikilink")、[大庄浩天宮](../Page/大庄浩天宮.md "wikilink")、[高雄代天宮](../Page/高雄代天宮.md "wikilink")、[鳳山區](../Page/鳳山區.md "wikilink")[五甲龍成宮](../Page/五甲龍成宮.md "wikilink")、[旗津天后宮](../Page/旗津天后宮.md "wikilink")、[台南市下營紅甲里清水祖師廟等各地著名廟宇](../Page/台南市下營紅甲里清水祖師廟.md "wikilink")，皆以清水祖師為配祀神。

據《台灣省通志》記載，台灣可考的最早清水祖師廟是[明鄭](../Page/明鄭.md "wikilink")[永曆十九年](../Page/永曆.md "wikilink")（西元1665年），隨[延平王](../Page/延平王.md "wikilink")[鄭成功來臺的泉州府安溪縣軍民](../Page/鄭成功.md "wikilink")，自安溪清水巖迎來清水祖師神像、自安溪泰山巖迎來[顯應祖師神像](../Page/顯應祖師.md "wikilink")；[漳州府](../Page/漳州府.md "wikilink")[平和縣移民自三平寺迎來](../Page/平和縣.md "wikilink")[三平祖師神像](../Page/三平祖師.md "wikilink")。合於四鯤鯓立廟祭拜，為[四鯤鯓龍山寺](../Page/四鯤鯓龍山寺.md "wikilink")。

## 祭典

每逢[舊曆](../Page/舊曆.md "wikilink")[正月初六清水祖師](../Page/正月初六.md "wikilink")[生日](../Page/生日.md "wikilink")、[五月初六清水祖師得道日](../Page/五月初六.md "wikilink")\[8\]，各地祖師廟常以[神豬](../Page/神豬.md "wikilink")、[三牲祭拜](../Page/犧牲.md "wikilink")**清水祖師**，以示崇敬，敬考清水祖師一生，為[漢傳佛教](../Page/漢傳佛教.md "wikilink")[禪宗得道高僧](../Page/禪宗.md "wikilink")，且其在世時未見有食肉之舉，故有人認為以祭祀素果糕餅為宜，不適合以[肉類](../Page/肉類.md "wikilink")、[葷菜奉敬](../Page/葷菜.md "wikilink")。

三峽祖師廟廟方解釋：神豬祭典原本不是祭祀清水祖師，原是因為三峽地區的[福建](../Page/福建.md "wikilink")[安溪移民在](../Page/安溪.md "wikilink")[三峽](../Page/三峽.md "wikilink")、[鶯歌與](../Page/鶯歌.md "wikilink")[桃園](../Page/桃園.md "wikilink")[大溪開墾之際](../Page/大溪.md "wikilink")，常遭[野生動物及當地](../Page/野生動物.md "wikilink")[原住民](../Page/原住民.md "wikilink")[出草攻擊](../Page/出草.md "wikilink")，因此產生[除夕殺神豬拜](../Page/除夕.md "wikilink")[山靈](../Page/山靈.md "wikilink")（或[山神](../Page/山神.md "wikilink")）以求平安的習俗，後來因大年初六是祖師誕辰，便將二者合併祭祀，此習俗實際上與祖師本人無關。三峽祖師廟總務組長劉金達說：「祖師公喫素，不喫豬肉。」\[9\]\[10\]\[11\]\[12\]也有不少人相信，山神亦屬於清水祖師廟的[護法神](../Page/護法神.md "wikilink")。另外民間相信祖師本身[茹素](../Page/素食.md "wikilink")，但祖師部下的[五營神將與天兵天將則不一定為素食者](../Page/五營神將.md "wikilink")。\[13\]\[14\]\[15\]

清水祖師的祭典也有以[迎神賽會的方式來舉行](../Page/迎神賽會.md "wikilink")，例如臺灣[新北市](../Page/新北市.md "wikilink")[淡水清水巖](../Page/淡水清水巖.md "wikilink")，每年則於農曆五月初五舉行[暗訪](../Page/暗訪.md "wikilink")，農曆五月初六舉行[繞境](../Page/繞境.md "wikilink")，鑼鼓喧天萬人空巷，祈求掃除瘟疫，合境平安，世稱「淡水大拜拜」。2013年新北市政府已將「淡水大拜拜」，列為「無形文化資產」\[16\]。

## 傳說

### 岩洞出米

在「張巖」修建殿宇時，苦無水源，眾人口渴之極，清水祖師持錫杖往地上一拄，立刻「清泉不竭」，故稱「清水巖」；而後祖師又以錫杖打破岩壁，竟然從岩石中流出許多白米，供應工人、僧眾、[檀越食用](../Page/檀越.md "wikilink")，眾人嘖嘖稱奇。《閩書》上說：清水祖師建造清水巖，白米自動出現；[顯應祖師建造泰山巖](../Page/顯應祖師.md "wikilink")，磚瓦自動出現；[惠應祖師建造泰湖巖](../Page/惠應祖師.md "wikilink")，木材自動出現。被視為三大神蹟。

### 真真人

傳說[南宋時](../Page/南宋.md "wikilink")，泉州[知府](../Page/知府.md "wikilink")[真德秀](../Page/真德秀.md "wikilink")，乃[程朱理學名儒](../Page/程朱理學.md "wikilink")，以清廉自居，向來不喜勞民傷財。[泉州大旱時](../Page/泉州.md "wikilink")，在官民幕僚不斷建議之下，方不得已率眾舉行祭典，向清水祖師[求雨](../Page/求雨.md "wikilink")，並在清水祖師神像下方堆滿[紙錢](../Page/紙錢.md "wikilink")，意思暗示若求雨不成，即要燒毀神像。

祭祀完畢，立刻下起大雨，而且還飄落一片梧桐葉，上面寫著四句詩：「雨是江西雨，移來泉州府，老佛若無靈，渾身成火灰。」意思是「清水祖師施展法力，將本來[江西應該下的雨](../Page/江西.md "wikilink")，挪到泉州來抒解[旱災](../Page/旱災.md "wikilink")。如果清水祖師我不能靈驗，可能就要被你知府真德秀燒成灰燼。」

真德秀為感謝神威，於是在祖師廟「[真人](../Page/真人.md "wikilink")」之匾額之上，添上一個「真」字，成為「真真人」，其意為誇獎清水祖師是「真人」之中的「真人」。從此，真德秀篤信清水祖師。

據說，[泉州](../Page/泉州.md "wikilink")[安溪祖師廟第十首籤詩](../Page/安溪祖師廟.md "wikilink")：「火發連天炎，嶺危去路難。若無天降雨，禍福在人間。」就是在講**真真人**這個故事，故此籤雖凶猶吉，最後會是好結局。

### 烏面祖師

關於此傳說有二：

  - 傳說中，[安溪清水巖是塊福地](../Page/安溪.md "wikilink")，適合修道。而祖師初居清水巖，遇到原先佔據此地的四個山鬼，[張、黃、蘇、李](../Page/張、黃、蘇、李.md "wikilink")（另說：[趙、王、蘇、李](../Page/趙、王、蘇、李.md "wikilink")）四大將來挑戰，明言此地為其所據，要求祖師離開。清水祖師於是與他們鬥法，四大將失敗逃離，卻在夜半回到清水巖來，把出口悉數封死，縱火燒山，連燒了七天七夜，就正當四大將歡欣鼓舞，自度清水祖師已死，要重新佔據清水巖時，卻發現清水祖師無恙端坐，只是臉被燻黑了。從此四大將心悅誠服，成為清水祖師的[護法神](../Page/護法神.md "wikilink")，是為[四大護法](../Page/四大護法.md "wikilink")。而清水祖師的塑像，其面部也必為黑色。此說為通行說法。

<!-- end list -->

  - 另說，祖師未曾出家之前，嫂嫂臨盆[生產](../Page/生產.md "wikilink")，無法炊事，請祖師上山砍柴。但不久後，嫂因看見沒有柴薪，而火依舊燃燒，不由得大吃一驚，方知祖師以己身為柴薪，嫂欲將祖師拉起，不料祖師身陷火中，祖師出[灶後](../Page/灶.md "wikilink")，安然無恙，唯煙燻黑祖師面孔。

### 落鼻祖師

傳說有一座清水祖師神像非常靈驗，每次只要有天災、人禍，祖師神像的鼻子或下巴就會掉落，以向信徒示警，信眾皆稱之「落鼻祖師」或「落鼻祖」，又稱為「[蓬萊老祖](../Page/蓬萊老祖.md "wikilink")」，終年接受上百個宮廟、聚落（迎請地域轄[大台北](../Page/大台北.md "wikilink")、[基隆市](../Page/基隆市.md "wikilink")、[宜蘭縣](../Page/宜蘭縣.md "wikilink")、[桃園市等](../Page/桃園市.md "wikilink")）以及私人民宅所迎請，由於其鼻子不定時於廟中或各迎請單位中掉落，衍生了許多傳奇性的落鼻故事，譬如鼻子會化作蟬黏在樹上，並且發出蟬的叫聲等情節，落鼻祖師的故事也出現在許多童話故事的文本，使得落鼻祖師的傳說故事不僅僅充滿神秘性，而且擁有許多趣味性。

[清法戰爭時](../Page/清法戰爭.md "wikilink")，[法軍進犯](../Page/法軍.md "wikilink")[臺灣](../Page/臺灣.md "wikilink")[淡水](../Page/淡水區.md "wikilink")，俗稱「[西仔反](../Page/西仔反.md "wikilink")」，善信迎去了此尊神像，焚香禱告，居然擊退法軍，這尊神像引發了[艋舺與](../Page/艋舺.md "wikilink")[淡水兩派信徒之間的紛爭](../Page/淡水區.md "wikilink")。艋舺方面說，這尊神像是艋舺祖師廟的神像，是淡水善信借去祈福，以抵抗法軍的。淡水方面，則說此神像本是淡水信徒所有，是因為淡水無[祖師廟](../Page/祖師廟.md "wikilink")，而寄放於艋舺祖師廟奉祀的。在兩派紛爭的結果之下，[日治時期經](../Page/日治時期.md "wikilink")[官府協調](../Page/官府.md "wikilink")，由艋舺與淡水二廟輪流奉祀。

另一方面，落鼻祖師由於全身均是[沈香木所雕成](../Page/沈香木.md "wikilink")，沈香木在[中藥學來說](../Page/中藥.md "wikilink")，具有許多實質的療效，因此又被稱為「藥祖祖師」，有著許多治病的神蹟流傳。最後，值得注意的是，[臺灣北部有許多宮廟神壇模仿其落鼻造型](../Page/臺灣北部.md "wikilink")，而雕塑許多清水祖師[分靈神像](../Page/分靈.md "wikilink")，諸如：[臺北市](../Page/臺北市.md "wikilink")[北投區稻香里集應廟](../Page/北投區.md "wikilink")、臺北市北投山腳廣福宮、[新北市](../Page/新北市.md "wikilink")[瑞芳龍巖宮](../Page/瑞芳龍巖宮.md "wikilink")、新北市[瑞芳區忠仁廟](../Page/瑞芳區.md "wikilink")、新北市[汐止區忠順廟](../Page/汐止區.md "wikilink")、新北市[汐止濟德宮](../Page/汐止濟德宮.md "wikilink")、新北市[新莊區如意堂等](../Page/新莊區.md "wikilink")（可參見辜神徹，2009）。

## 民間咒語

依據各[巖仔](../Page/巖仔.md "wikilink")、[伽藍](../Page/伽藍.md "wikilink")、[神壇不同](../Page/神壇.md "wikilink")，詞句大同小異。

### 清水真人咒

敕封清水真人，黑帝化身，風火乘脚，沙界蹤橫，七星寶劍，斬斷妖精，雨暘隨禱，護國安民，陰間有聲，陽間有名，上天下地，應物現形。

### 恩主清水真人咒

拜請恩主清水真人，黑帝化身，風火隨其足，沙界縱橫，七星寶劍，寸斬妖精，雨陽隨其到，護國安民，陽界有聲，陰間有名，上天下地，應禱全靈。法門弟子專拜請，清水祖師降臨來，神兵火急如律令。

### 昭應祖師咒

拜請昭應大祖師，神通顯化閻浮遍，清水岩上化法身，蓬萊山中真靈現，當初不捨絕世嶺，修橋造路結善緣，深受大宋天子眷，敕賜昭應大師名，帶領張黃蘇李將，遊行天下救萬民，唯見世間多疾病，扶攝生童在壇前，居尾居前皆有應，二六時中渡有情，法門弟子專拜請，昭應祖師降臨來，神兵火急如律令。

### 麻章上人咒

拜請麻章上人公，神靈顯化萬代殊，當初開闢清水巖，張黃蘇李拜吾師，求雨濟藥多造路，度化生靈名悠悠，小姑顯化清溪道，能把瘟神邪魔收，當年屈服真郡守，今日慈悲降道場，法門弟子專拜請，麻章上人降臨來，神兵火急如律令。

### 蓬萊老祖咒

拜請蓬萊老祖公，為吾顯化三寶堂，毘羅揭諦金剛將，道家丁甲來護持，佛法演化不思議，廣度有情二六時，老祖佛法化三千，張黃蘇李四將賢，吾今志心焚香-{煙}-，祈求老祖降臨先。法門弟子專拜請，蓬萊老祖降臨來，神兵火急如律令。

## 参考文献

### 引用

### 书目

  - 林國平（1999）：〈清水祖師信仰探索〉，《圓光佛學學報》第四期，頁217-251。
  - 辜神徹（2009）：《臺灣清水祖師信仰--落鼻祖師的歷史與文化》，臺北：博揚文化。

## 參見

  - [應惠靈天](../Page/應惠靈天.md "wikilink")
  - [安溪諸聖圖](../Page/安溪諸聖圖.md "wikilink")
  - [清水巖](../Page/清水巖.md "wikilink")
  - [安溪清水巖](../Page/安溪清水巖.md "wikilink")
  - [三峽長福巖](../Page/三峽長福巖.md "wikilink")
  - [艋舺清水巖](../Page/艋舺清水巖.md "wikilink")
  - [淡水清水巖](../Page/淡水清水巖.md "wikilink")
  - [六張犁石泉巖祖師廟](../Page/石泉巖.md "wikilink")
  - [景美祖師廟](../Page/景美祖師廟.md "wikilink")
  - [萬隆祖師廟](../Page/萬隆祖師廟.md "wikilink")
  - [瑞芳祖師廟](../Page/瑞芳祖師廟.md "wikilink")
  - [土城祖師廟](../Page/土城祖師廟.md "wikilink")
  - [蓮池潭祖師廟](../Page/蓮池潭祖師廟.md "wikilink")
  - [四鯤鯓祖師廟](../Page/四鯤鯓祖師廟.md "wikilink")
  - [文澳祖師廟](../Page/文澳祖師廟.md "wikilink")
  - [五甲龍成宮](../Page/五甲龍成宮.md "wikilink")
  - [臺灣民間信仰](../Page/臺灣民間信仰.md "wikilink")
  - [高僧信仰](../Page/台灣佛教#高僧信仰.md "wikilink")
  - [長短樹永安堂](../Page/長短樹永安堂.md "wikilink")

[C清](../Category/清水祖師廟.md "wikilink")
[C清](../Category/人物神.md "wikilink")
[C清](../Category/道教神祇.md "wikilink")
[C清](../Category/護法神.md "wikilink")
[C清](../Category/宋朝僧人.md "wikilink")
[C陳](../Category/禪僧.md "wikilink")
[C陳](../Category/永春人.md "wikilink")
[C陳](../Category/安溪人.md "wikilink")
[C陳](../Category/台灣民間信仰.md "wikilink")
[C清](../Category/中國民間信仰.md "wikilink")
[Category:陳姓](../Category/陳姓.md "wikilink")

1.  [三峽長福巖祖師廟
    清水祖師公生平神蹟略傳](http://www.longfuyan.org.tw/front/bin/ptdetail.phtml?Part=Title-005&Category=101181)
2.  [文化部 藝文活動
    長福岩清水祖師聖誕活動](http://event.moc.gov.tw/sp.asp?xdurl=ccEvent2016/ccEvent_cp.asp&cuItem=2238246&ctNode=676&mp=1)
3.
4.  [公民新聞 淡水清水巖祖師廟](https://www.peopo.org/news/368243)
5.
6.
7.  [宜蘭市光明寺-歷史沿革](http://jses1335.pixnet.net/blog/post/296973956)
8.  [清水祖師遶境祈福　朱立倫祈求合境平安](https://www.nownews.com/news/20180619/2773801)
9.  [祖師廟神豬祭
    特等獎1487斤](http://www.chinatimes.com/newspapers/20180207000784-260107)
10. [清水祖師誕辰
    三峽賽神豬](http://www.chinatimes.com/realtimenews/20140205005078-260402)
11. [三峽祖師廟：神豬文化源自祭拜山靈](http://news.ltn.com.tw/news/life/paper/654337)
12. [「好大」1061公斤神豬亮相好胃口嚼檳榔年初六就要獻祭](http://tw.nextmedia.com/applenews/article/art_id/33980492/IssueID/20120124)
13. 《臺灣各地「祖師廟」神豬祭祀現況訪查報告》
14. 《臺北縣三峽長福岩沿革志》
15. 《三峽長福岩藝文探微》
16. [淡水清水巖清水祖師遶境](https://nchdb.boch.gov.tw/assets/overview/folklore/20130130000002)