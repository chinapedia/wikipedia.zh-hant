《**龍刻
RYU-KOKU**》是[KID於](../Page/KID.md "wikilink")2006年9月21日發售的[戀愛冒險遊戲](../Page/戀愛冒險遊戲.md "wikilink")，也是KID[破產前的最後一套遊戲](../Page/破產.md "wikilink")。[英特衛多媒體在台灣發行繁體中文PC版](../Page/英特衛多媒體.md "wikilink")，列為《遊戲偶像劇系列》第4集，分為「平裝版」與「特典版」。

2009年9月底，[北京娱乐通在中国大陆发行了基于](../Page/北京娱乐通.md "wikilink")[古月游戏引擎的简体中文PC版](../Page/古月游戏引擎.md "wikilink")。这个版本放弃了传统的基于光盘验证的数字版权保护技术，完全依赖于网络激活，是中国大陆少有的几个不需要插光盘就能运行的[单机游戏之一](../Page/单机游戏.md "wikilink")。

下列譯名及用語，除特別註明者外，以英特衛版為準。

## 故事簡介

主角「水內司」是個就讀皆森高中的高中生。身為劍道社主將的他，與青梅竹馬
「笠木愛娜」一同為即將到來的劍道大會而努力練習著。但近來時常作的那個怪夢，卻讓他感到困惑不已。就在這
時候，突然出現了一名叫「水乃內美流」的神秘轉學生，於是平靜的日子便慢慢地開始發生了變化……。

## 登場人物

  -
    17歲，4月16日出生，身高171cm，血型A型，皆森高校[劍道部男子大將](../Page/劍道.md "wikilink")，擅長[日本歷史](../Page/日本歷史.md "wikilink")。
    故事主角。就讀皆森高校2年級，學業優秀、擅長運動的典型模範生。小時候經常受欺負，於是練習劍道，其實原因是爲了保護笠木愛娜。
    實際上是數千年前的封師「」的轉生，水内司仍擁有水乃内司的靈魂及記憶。
  -
    16歲，7月16日出生，身高160cm，擅長[和裁和](../Page/和裁.md "wikilink")[古文](../Page/古文.md "wikilink")，不擅[理科](../Page/理科.md "wikilink")。非常喜歡[電視](../Page/電視.md "wikilink")（特別是[電視劇](../Page/電視劇.md "wikilink")）和[番茄](../Page/番茄.md "wikilink")[薯片](../Page/薯片.md "wikilink")。
    剛轉校皆森高校2年級，自稱是水内司的堂妹兼未婚妻。性格嫻熟，但知識意外地十分的偏，不知道很多單字的意思，特別是[外來語](../Page/外來語.md "wikilink")。
    實際上是數千年前活到現在的[龍](../Page/龍.md "wikilink")，為了再見水乃内司而等待了數千年。
  -
    16歲，9月2日出生，身高163cm，血型AB型，皆森高校劍道部女子大將，喜歡[卡啦OK](../Page/卡啦OK.md "wikilink")、[布偶](../Page/布偶.md "wikilink")，擅長[日語](../Page/日語.md "wikilink")、日本歷史、[英語](../Page/英語.md "wikilink")，不擅[數學](../Page/數學.md "wikilink")。
    司的[青梅竹馬](../Page/青梅竹馬.md "wikilink")，每當司被欺負時便挺身而出。其實喜歡著司，但一直口不對心。在劍道上的實力強于司。
  -
    15歲，7月21日出生，身高154cm，皆森高校生物部飼養系成員，喜歡[書法](../Page/書法.md "wikilink")、[動物](../Page/動物.md "wikilink")。
    剛轉校皆森高校1年級，無甚麼表情和反應的女孩，在班級中被孤立，似乎來自有錢人家。
    實際上是由(北京娱乐通版譯作玖印智嘉)
    創造的[人造人](../Page/人造人.md "wikilink")，但身體中是數千年前水乃内司的妹妹、符咒系封師「玖印智佳」。
  -
    24歲，3月2日出生，身長166cm，血型B型，喜歡吸煙、喝酒。
    皆森高校的保健醫生，喜歡和學生談天。好奇心強，正在研究六年前恩師（兼男朋友）失蹤等相關事件。
  -
    身高約140cm，性格有點幼稚的小孩，稱美流與智佳為姐姐。真身其實是剛出生的妖。
  -
    17歲，喜歡[科幻](../Page/科幻.md "wikilink")、[超自然](../Page/超自然.md "wikilink")，擅長[體育](../Page/體育.md "wikilink")、[數學](../Page/數學.md "wikilink")，不太喜歡姐姐刹那。
    司的好友。擅長製造氣氛，表面上有點輕浮，但其實內裡成熟。暗戀愛娜，但因為知道愛娜喜歡司而一直沒有告白。
  -
    170cm，外表年輕但其實是36歲。皆森高校的新任教師，擔任數學教師、劍道部顧問（但不黯劍道）和生物部顧問。性格溫和，但很難看清他究竟在想甚麼。
    实际身份是草薙制药的研究人员，玖印智嘉的同事。在美流线中，为了掌握妖和龙的能力而绑架美流，最后被消灭。
  -
    一直消滅「無心靈的妖」，但在追擊美流。
    其真实身份是千年前水内司收养的狐妖，由于嫉妒司和美流的关系，加之霸流的封印举动而狂暴化，引发大乱，进而导致司的死亡。其肉体被霸流和美流消灭，灵魂则在世上游荡，后来寄宿在爱娜身上。
  -
    和美流相識，是類似美流「哥哥般的人」。左手用於戰鬥。
    實際上是和美流一樣是數千年前活到現在的龍。
  -
    [土地神](../Page/土地神.md "wikilink")，[神社千年](../Page/神社.md "wikilink")[銀杏的主人](../Page/銀杏.md "wikilink")，外表是女性但其實是[雌雄同体](../Page/雌雄同体.md "wikilink")，實際是一位有強大力量的妖。

## 用語及設定

  -


## 音樂

### 主題曲

  - 開頭歌曲「Private place」
    作詞、作曲：[志倉千代丸](../Page/志倉千代丸.md "wikilink")（[5pb](../Page/5pb.md "wikilink")），編曲：磯江俊道
    主唱：[彩音](../Page/彩音.md "wikilink")
  - 結尾歌曲「」
    作詞、作曲：志倉千代丸（5pb），編曲：川越好博
    主唱：彩音

### 原聲帶

<table>
<thead>
<tr class="header">
<th><p>Disc1</p></th>
<th><p>Disc2</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>曲目</p></td>
<td><p>標題</p></td>
</tr>
<tr class="even">
<td><p>1</p></td>
<td><p>Private place</p></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p>鼓動</p></td>
</tr>
<tr class="even">
<td><p>3</p></td>
<td><p>美流</p></td>
</tr>
<tr class="odd">
<td><p>4</p></td>
<td><p>勾玉</p></td>
</tr>
<tr class="even">
<td><p>5</p></td>
<td><p>想起</p></td>
</tr>
<tr class="odd">
<td><p>6</p></td>
<td><p>我許</p></td>
</tr>
<tr class="even">
<td><p>7</p></td>
<td><p>唯心</p></td>
</tr>
<tr class="odd">
<td><p>8</p></td>
<td><p>苦悶</p></td>
</tr>
<tr class="even">
<td><p>9</p></td>
<td><p>待宵</p></td>
</tr>
<tr class="odd">
<td><p>10</p></td>
<td><p>天咲</p></td>
</tr>
<tr class="even">
<td><p>11</p></td>
<td><p>現界</p></td>
</tr>
<tr class="odd">
<td><p>12</p></td>
<td><p>徒然</p></td>
</tr>
<tr class="even">
<td><p>13</p></td>
<td><p>仲間</p></td>
</tr>
<tr class="odd">
<td><p>14</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>15</p></td>
<td><p>龍刻</p></td>
</tr>
<tr class="odd">
<td><p>16</p></td>
<td><p>新史</p></td>
</tr>
</tbody>
</table>

## 外部連結

  - [PS2版官方網站](https://web.archive.org/web/20070224233358/http://www.kid-game.co.jp/ryukoku/index.html)，存於[網際網路檔案館](../Page/網際網路檔案館.md "wikilink")

  - [KID](https://web.archive.org/web/20070105182250/http://www.kid-game.co.jp/index_1.html)，存於[網際網路檔案館](../Page/網際網路檔案館.md "wikilink")

  - [PSP版官方網站](https://web.archive.org/web/20120107125235/http://www.cyberfront.co.jp/title/ryukoku/)，存於[網際網路檔案館](../Page/網際網路檔案館.md "wikilink")

  - [繁體中文PC平裝版官方網站](https://web.archive.org/web/20100126111138/http://www.interwise.com.tw/showroom/view.php?C=5070594)，存於[網際網路檔案館](../Page/網際網路檔案館.md "wikilink")

  - [繁體中文PC特典版官方網站](https://web.archive.org/web/20091210210043/http://www.interwise.com.tw/showroom/view.php?C=5070616)，存於[網際網路檔案館](../Page/網際網路檔案館.md "wikilink")

[Category:2006年电子游戏](../Category/2006年电子游戏.md "wikilink")
[Category:PlayStation 2游戏](../Category/PlayStation_2游戏.md "wikilink")
[Category:PlayStation
Portable游戏](../Category/PlayStation_Portable游戏.md "wikilink")
[Category:美少女遊戲](../Category/美少女遊戲.md "wikilink")
[Category:戀愛冒險遊戲](../Category/戀愛冒險遊戲.md "wikilink")
[Category:使用网络激活的单机游戏](../Category/使用网络激活的单机游戏.md "wikilink")
[Category:不需要光盘验证的单机游戏](../Category/不需要光盘验证的单机游戏.md "wikilink")
[Category:KID](../Category/KID.md "wikilink")
[Category:Windows游戏](../Category/Windows游戏.md "wikilink")