**霍建寧**（，）\[1\]是[長江集團的高層管理人之一](../Page/長江集團.md "wikilink")。霍建寧是[香港著名的超級](../Page/香港.md "wikilink")「打工皇帝」之首，也是[香港稅務局](../Page/香港稅務局.md "wikilink")[薪俸稅的大戶之一](../Page/薪俸稅.md "wikilink")。在2006至2007年，他的收入是1.3亿港元，是当年香港收入最高的人，有媒體指霍氏自1994-2014的收入達25.4亿港元。\[2\]
\[3\]

## 早年

1960年代，霍建寧就讀於福榮街官立小學以及[拔萃男書院](../Page/拔萃男書院.md "wikilink")。1970年代，霍建寧以[文學學士於](../Page/文学士.md "wikilink")[香港大學畢業](../Page/香港大學.md "wikilink")，然後留學[美國](../Page/美國.md "wikilink")，其後亦於[澳洲](../Page/澳洲.md "wikilink")[新英格蘭大學取得財務管理文憑](../Page/新英格蘭大學.md "wikilink")。

## 创业

1979年返港加入[長江集團](../Page/長江集團.md "wikilink")。晉升為會計主任後，再取得澳洲特許會計師協會會員資格，于1983年離開与友人合资开会计师行。可是，会计师行不久便告结业。

## 发掘

翌年回巢，1985年出任長實董事，年僅33歲。\[4\]1993年，霍建宁到日本公干，被李嘉诚的另一间公司和黄集团借调过去帮忙，结果和黄集团的董事总经理[马世民意外发现此人是一个出色的谈判专家](../Page/马世民.md "wikilink")。由日本回到香港后，马很快就向李嘉诚推荐了霍。马世民离职后，霍建宁接替其走马上任，成为和黄新的“一把手”（董事总经理）。\[5\]

## 軼聞

2006年4月霍建寧以3.5億購買[深水灣道](../Page/深水灣道.md "wikilink")37號的豪宅 \[6\]。

## 職稱

1.  [長江基建集團有限公司](../Page/長江基建集團有限公司.md "wikilink") - 執行董事兼董事局副主席
2.  [電能實業有限公司](../Page/電能實業有限公司.md "wikilink") - 董事局主席
3.  [長江和記實業有限公司](../Page/長江和記實業有限公司.md "wikilink") - 集團聯席董事總經理
4.  [和記電訊國際有限公司](../Page/和記電訊國際有限公司.md "wikilink") - 董事局主席
5.  [和記港陸有限公司](../Page/和記港陸有限公司.md "wikilink") - 董事局主席
6.  Hutchison Telecommunications ( Australia ) Limited - 董事會主席
7.  Partner Communications Company Ltd - 董事會主席
8.  [赫斯基能源公司](../Page/赫斯基能源公司.md "wikilink") - 董事會聯席主席
9.  [和記電訊香港控股有限公司董事局主席](../Page/和記電訊香港控股有限公司.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

[Category:長和集團](../Category/長和集團.md "wikilink")
[Category:香港財經界人士](../Category/香港財經界人士.md "wikilink")
[Category:香港會計師](../Category/香港會計師.md "wikilink")
[Category:香港億萬富豪](../Category/香港億萬富豪.md "wikilink")
[Category:香港大學校友](../Category/香港大學校友.md "wikilink")
[Category:拔萃男書院校友](../Category/拔萃男書院校友.md "wikilink")
[J建](../Category/霍姓.md "wikilink")
[Category:香港之最](../Category/香港之最.md "wikilink")
[Category:香港商人](../Category/香港商人.md "wikilink")
[Category:會計師出身的商人](../Category/會計師出身的商人.md "wikilink")

1.
2.
3.  [趣BLOGBLOG：霍建寧袋25億代價](http://hk.apple.nextmedia.com/financeestate/art/20150514/19146522)
4.  <http://hk.apple.nextmedia.com/financeestate/art/20150525/19159054>
5.
6.  [福布斯全球調查霍建寧新居上榜亞洲5大豪宅香港佔4間](https://hk.news.appledaily.com/local/daily/article/20070223/6841423)