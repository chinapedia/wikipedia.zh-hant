**克劳斯·冯·克利青**（，），[德国物理学家](../Page/德国.md "wikilink")。

他因于1980年2月5日在[格勒诺布尔高强度磁场实验室发现](../Page/格勒诺布尔.md "wikilink")[量子霍尔效应而获](../Page/量子霍尔效应.md "wikilink")1985年[诺贝尔物理学奖](../Page/诺贝尔物理学奖.md "wikilink")。

## 家庭

冯·克利青是出生于一个相当古老的[勃兰登堡贵族家庭](../Page/勃兰登堡州.md "wikilink")。这个贵族家庭最早于1265年就在文献上有提及。克劳斯·冯·克利青的父亲是一位资深林业管理员。他的祖父曾是[普鲁士波森省的农业局局长和普鲁士上院的成员](../Page/普鲁士.md "wikilink")。

## 生平

1943年生於[德國](../Page/德國.md "wikilink")[波森省](../Page/波森省.md "wikilink")[希羅達](../Page/大波蘭地區希羅達.md "wikilink")（今[波蘭](../Page/波蘭.md "wikilink")），1945年冯·克利青随家逃到[下萨克森州](../Page/下萨克森州.md "wikilink")[费希塔县的](../Page/费希塔县.md "wikilink")[路滕](../Page/路滕.md "wikilink")（）。从1948年至1951年他们住在[奥尔登堡](../Page/奥尔登堡.md "wikilink")，在这里1949年克劳斯·冯·克利青进入小学。1951年他家迁往[克洛彭堡县](../Page/克洛彭堡县.md "wikilink")[埃森](../Page/埃森_\(奥尔登堡\).md "wikilink")，至1968年他们住在市政府的楼上。1962年2月克劳斯·冯·克利青在[奥斯纳布吕克县](../Page/奥斯纳布吕克县.md "wikilink")[夸肯布吕克](../Page/夸肯布吕克.md "wikilink")（）中学毕业。

此后冯·克利青在[不伦瑞克工业大学学物理](../Page/不伦瑞克工业大学.md "wikilink")。1969年3月他大学毕业。

至1980年11月他在[维尔茨堡大学研究](../Page/维尔茨堡大学.md "wikilink")，在此期间他于1972年获得博士学位，他的博士论文题目为《[碲在强](../Page/碲.md "wikilink")[磁场中的电磁特性](../Page/磁场.md "wikilink")》。1978年他获得在大学任教的学术地位。

在他在维尔茨堡期间冯·克利青也获得了在国外进行研究的机会，1975年至1978年他在[牛津](../Page/牛津.md "wikilink")，1979年至1980年在格勒诺布尔进行研究工作。在格勒诺布尔他也发现了量子霍尔效应。

1980年[慕尼黑工业大学聘请冯](../Page/慕尼黑工业大学.md "wikilink")·克利青任[凝聚态物理学教授](../Page/凝聚态物理学.md "wikilink")。1985年春冯·克利青移居[斯图加特](../Page/斯图加特.md "wikilink")，成为[马克斯·普朗克凝聚态研究所所长团的成员](../Page/马克斯·普朗克凝聚态研究所.md "wikilink")。同年[斯图加特大学授予他荣誉教授](../Page/斯图加特大学.md "wikilink")。

冯·克利青是[德国未来奖和德国经济界颁发的创新奖的评审员](../Page/德国未来奖.md "wikilink")。此外他是由[奥尔登堡大学参与颁发的以他命名的](../Page/奥尔登堡大学.md "wikilink")[克劳斯·冯·克利青奖的评审员](../Page/克劳斯·冯·克利青奖.md "wikilink")。该奖授予在自然科学方面表现突出的教师。

冯·克利青强烈宣传[基础研究](../Page/基础研究.md "wikilink")，他始终试图在公众中唤起对物理学的好奇心和热情。他是多个国家科学院的成员。六个国家的不同大学授予他荣誉博士学位。

## 诺贝尔奖

1985年克劳斯·冯·克利青因发现了量子霍尔效应而获得诺贝尔物理学奖。这个发现决定性的试验是在1980年2月5日晚上进行的。这个发现证实了[电阻是量子性的](../Page/电阻.md "wikilink")，其最小单位由两个[物理常数决定](../Page/物理常数.md "wikilink")：[普朗克常数](../Page/普朗克常数.md "wikilink")**和[电子电荷](../Page/电子.md "wikilink")**。因此它本身也是一个物理常数。通过量子霍尔效应今天可以对电阻进行绝对的和极精确的测量。从1990年开始电阻的单位[欧姆的定义是根据量子霍尔效应确定的](../Page/欧姆.md "wikilink")。量子霍尔效应也是纳米电子工程的出发点。通过它可以研究比今天的[微电子学小得多的半导体元件的物理特性](../Page/微电子学.md "wikilink")。

## 参考资料

  - [诺贝尔官方网站关于克劳斯·冯·克利青介绍](https://web.archive.org/web/20071011073341/http://nobelprize.org/nobel_prizes/physics/laureates/1985/klitzing-cv.html)

## 外部链接

  - [克劳斯·冯·克利青在马克斯·普朗克研究所的官方网站](http://www.fkf.mpg.de/klitzing/)

[Category:德国物理学家](../Category/德国物理学家.md "wikilink")
[Category:半導體物理學家](../Category/半導體物理學家.md "wikilink")
[Category:诺贝尔物理学奖获得者](../Category/诺贝尔物理学奖获得者.md "wikilink")
[Category:德國諾貝爾獎獲得者](../Category/德國諾貝爾獎獲得者.md "wikilink")
[Category:德國貴族](../Category/德國貴族.md "wikilink")
[Category:慕尼黑工業大學教師](../Category/慕尼黑工業大學教師.md "wikilink")
[Category:維爾茨堡大學校友](../Category/維爾茨堡大學校友.md "wikilink")
[Category:下薩克森人](../Category/下薩克森人.md "wikilink")
[Category:波森人](../Category/波森人.md "wikilink")
[Category:中国科学院外籍院士](../Category/中国科学院外籍院士.md "wikilink")
[Category:马克斯·普朗克学会人物](../Category/马克斯·普朗克学会人物.md "wikilink")
[Category:UNSW狄拉克奖章和讲座得主](../Category/UNSW狄拉克奖章和讲座得主.md "wikilink")
[Category:宗座科学院院士](../Category/宗座科学院院士.md "wikilink")