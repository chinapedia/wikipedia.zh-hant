**胡鴻烈**[博士](../Page/博士.md "wikilink")<span style="font-size:smaller;">，[PhD](../Page/哲學博士.md "wikilink")</span>（Dr
Henry H.L.
Hu，），生於[浙江](../Page/浙江.md "wikilink")[紹興](../Page/紹興.md "wikilink")。擁有[香港](../Page/香港.md "wikilink")[執業大律師資格](../Page/訟務律師.md "wikilink")，並且執業有50多年。曾任[立法局](../Page/立法局.md "wikilink")[議員](../Page/議員.md "wikilink")、[全國政協委員](../Page/全國政協.md "wikilink")。香港第一所私立[大學](../Page/大學.md "wikilink")——[香港樹仁大學的](../Page/香港樹仁大學.md "wikilink")[校監及創辦人](../Page/校監.md "wikilink")。其妻子是已故[樹仁大學校長](../Page/樹仁大學.md "wikilink")[鍾期榮女士](../Page/鍾期榮.md "wikilink")。

胡鴻烈於1971年與妻子創立香港[樹仁學院](../Page/樹仁學院.md "wikilink")。為促使[樹仁學院成為香港官方認許的私立](../Page/樹仁學院.md "wikilink")[大學](../Page/大學.md "wikilink")，夫婦倆與樹仁師生共同努力了35年後，[樹仁學院於](../Page/樹仁學院.md "wikilink")2006年12月獲香港政府認可成為[香港樹仁大學](../Page/香港樹仁大學.md "wikilink")。

## 生平

胡鴻烈因自幼清貧，至9歲才就學，但卻在兩年半內修畢小學所有課程，並自行參加省升中試，在浙江省30,000多名考生的考試中名列前茅。高中畢業後，他入讀[中央政治大學修讀法學](../Page/中央政治大學.md "wikilink")，1942年畢業。大學畢業後，他參加了[全國高等文官考試](../Page/全國高等文官考試.md "wikilink")，取得外交組考試第一名的佳績。他後來亦在國家文官的培訓當中邂逅[全國高等文官考試司法組第一名的鍾期榮女士](../Page/全國高等文官考試.md "wikilink")，二人於1945年11月12日成婚。同年，胡鴻烈攜妻子到[蘇聯開展其外交官生涯](../Page/蘇聯.md "wikilink")，任駐塔什干總領事館隨習領事\[1\]。1949年，國家政府交替，委任胡鴻烈擔任中國駐外外交官的原[國民黨政府已不再擁有對中國大陸的管治權力](../Page/國民黨.md "wikilink")，胡鴻烈的外交官身份已名存實亡。他與妻子遂決定到[巴黎大學進修](../Page/巴黎大學.md "wikilink")，取得國際法及國際事務高等研修文憑，並於1952年取得法學博士學位。他於1954年在[英國考取了](../Page/英國.md "wikilink")[訴訟律師資格](../Page/大律師.md "wikilink")，並於1955年開始在香港開展其大律師業務。

胡鴻烈熱愛中國，這是一直為各界所公認的。據說，他為了要保留中國國籍才會放棄考取[事務律師資格而考取](../Page/事務律師.md "wikilink")[訴訟律師](../Page/大律師.md "wikilink")（又稱「訟務律師」或「大律師」）資格，因為當時要在香港擔任[事務律師是必須為英國國籍的](../Page/事務律師.md "wikilink")，當時，[事務律師的收入普遍較](../Page/事務律師.md "wikilink")[訴訟律師的為高](../Page/大律師.md "wikilink")。執業期間，他曾處理過一些著名案件，包括[歐陽炳強案](../Page/歐陽炳強.md "wikilink")。\[2\]

1976年，獲委任為非官守[立法局議員](../Page/立法局.md "wikilink")。1979年，胡鴻烈獲邀到大陸出席中國國慶30周年紀念，並獲[鄧小平接見](../Page/鄧小平.md "wikilink")，是第一位到內地的香港[立法局議員](../Page/立法局.md "wikilink")。他在1983年卸任[立法局議員職務](../Page/立法局.md "wikilink")，其後亦沒有再擔任議員。1987年，他獲委任為第六屆的全國[政協委員](../Page/政協.md "wikilink")，自1993年全國[政協第](../Page/政協.md "wikilink")8屆會議開始，他連續兩屆獲委任為[政協常委](../Page/政協.md "wikilink")。

胡鴻烈最為人熟悉的成就是和妻子在1971年創辦了前[樹仁學院](../Page/樹仁學院.md "wikilink")（即如今的[香港樹仁大學](../Page/香港樹仁大學.md "wikilink")），夫妻二人為樹仁大學的建設與發展付出鉅大，至2006年[樹仁學院學生宿舍和文康大樓建成為止](../Page/樹仁學院.md "wikilink")，估計胡鴻烈對該校的資金投入有4-5億港元。作為香港第一家私立大學的創辦人，胡鴻烈對香港高等教育界的貢獻及教育家之名是毋庸置疑的。

胡鴻烈在法學研究方面，精研中國近現代法律，更是《[大清律例](../Page/大清律例.md "wikilink")》方面的學術權威。

由於胡鴻烈的背景和與中國官方的良好關係，他曾多次獲邀代表[香港大律師公會和](../Page/香港大律師公會.md "wikilink")[香港律師會出席與中國相關的活動](../Page/香港律師會.md "wikilink")。

## 軼事

  - 胡鴻烈伉儷共同獲[中國中央電視台](../Page/中國中央電視台.md "wikilink")《[感動中國](../Page/感動中國.md "wikilink")》系列節目評選為2007年度感動中國的人物。
  - 2007年，獲美籍業餘[天文學家](../Page/天文學家.md "wikilink")[楊光宇將一顆於](../Page/楊光宇.md "wikilink")2001年首次被發現的[小行星](../Page/小行星.md "wikilink")「34778」命名為「胡鴻烈」\[3\]。

## 專業資格

  - [英國大律師公會會員](../Page/英國大律師公會.md "wikilink")（1954年）
  - [香港大律師公會會員](../Page/香港大律師公會.md "wikilink")（1955年）

## 其他公職

  - 中國國際經濟貿易仲裁委員會委員
  - 中國青少年犯罪研究會顧問。
  - 第7屆[中國人民政治協商會議全國委員會香港地區委員](../Page/中國人民政治協商會議全國委員會香港地區委員.md "wikilink")\[4\]

## 著作

  - 《Le Problème coréen（韓國問題）》（1953），巴黎A. Pedone出版（法文）
  - 胡鴻烈、鍾期榮 《香港的婚姻與繼承法》（1957，2009年再版）（ISBN 9789888039586）

## 榮譽

  - [太平紳士](../Page/太平紳士.md "wikilink")（1976年）
  - [O.B.E.](../Page/OBE.md "wikilink")（1976年）
  - [金紫荊星章](../Page/金紫荊星章.md "wikilink")（1998年）
  - [香港教育學院榮譽教育學博士](../Page/香港教育學院.md "wikilink")（2007年）
  - [香港理工大學榮譽社會科學博士](../Page/香港理工大學.md "wikilink")（2007年）
  - [大紫荊勳章](../Page/大紫荊勳章.md "wikilink")（2008年）
  - [香港大學名譽社會科學博士](../Page/香港大學.md "wikilink")（2015年）

## 参考文献

## 外部連結

  - [Bar List at
    HKBA](https://web.archive.org/web/20070311035202/http://barlist.hkba.org/hkba/Seniority_JuniorCN/79.htm)
  - [胡鴻烈 鍾期榮 相濡以沫](http://hk.news.yahoo.com/061116/12/1wkfb.html)

{{-}}

[H](../Page/category:胡姓.md "wikilink")

[Category:全國政協香港委員](../Category/全國政協香港委員.md "wikilink")
[Category:獲頒授香港金紫荊星章者](../Category/獲頒授香港金紫荊星章者.md "wikilink")
[Category:香港大律師](../Category/香港大律師.md "wikilink")
[H](../Category/前香港立法局議員.md "wikilink")
[H](../Category/國立政治大學校友.md "wikilink")
[Category:香港樹仁大學](../Category/香港樹仁大學.md "wikilink")
[H](../Category/OBE勳銜.md "wikilink")
[Category:绍兴人](../Category/绍兴人.md "wikilink")
[Category:感动中国年度人物](../Category/感动中国年度人物.md "wikilink")
[Category:香港大學名譽博士](../Category/香港大學名譽博士.md "wikilink")
[Category:香港理工大學榮譽博士](../Category/香港理工大學榮譽博士.md "wikilink")
[Category:香港教育學院榮譽博士](../Category/香港教育學院榮譽博士.md "wikilink")

1.  《國民政府公報》第967.2805號
2.  [難忘為紙盒藏屍犯辯護](http://ol.mingpao.com/cfm/style5.cfm?File=20061117/sta12/yia2.txt)，明報，2006年11月17日
3.
4.  <http://cn.chinagate.cn/indepths/2009-02/18/content_17299113_2.htm>