[MapleStory_Login_Screenshot.png](https://zh.wikipedia.org/wiki/File:MapleStory_Login_Screenshot.png "fig:MapleStory_Login_Screenshot.png")版「新楓-{}-之谷」的登入畫面\]\]
是一款[橫向捲軸](../Page/橫向捲軸遊戲.md "wikilink")[平面动作操作的](../Page/平台游戏.md "wikilink")[免費](../Page/網路遊戲免費模式.md "wikilink")[大型多人線上角色扮演遊戲](../Page/大型多人線上角色扮演遊戲.md "wikilink")，由[韓國](../Page/韓國.md "wikilink")[Wizet開發](../Page/Wizet.md "wikilink")，[Nexon發行](../Page/Nexon.md "wikilink")，於臺灣由[遊戲橘子公司代理](../Page/遊戲橘子.md "wikilink")，中國由[盛大网络代理](../Page/盛大网络.md "wikilink")。在[新楓之谷世界中](../Page/新楓之谷世界.md "wikilink")，可以經由扮演來自各地的角色來進行冒險，透過[技能](../Page/技能.md "wikilink")[攻擊](../Page/攻擊.md "wikilink")[怪物](../Page/怪物.md "wikilink")、解決任務以[提升等級](../Page/等級提升.md "wikilink")。新楓之谷在[中華民國](../Page/中華民國.md "wikilink")[遊戲軟體分級管理辦法中的分級為輔](../Page/遊戲軟體分級管理辦法.md "wikilink")12級。新楓之谷源自於舊楓之谷。2010年12月22日，《新楓之谷》史無前例地全面翻新遊戲架構與內容，在升級速度、職業平衡、技能平衡、戰鬥公式、地圖設計等內容都做了全面性的更新調整。\[1\]

## 伺服器訊息

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>国家／地区</p></th>
<th style="text-align: left;"><p>官方译名</p></th>
<th style="text-align: left;"><p>代理商</p></th>
<th style="text-align: left;"><p>時間</p></th>
<th style="text-align: left;"><p>伺服器</p></th>
<th style="text-align: left;"><p>英文簡寫</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p><a href="../Page/韩国.md" title="wikilink">韩国</a></p></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"><p><a href="../Page/NEXON.md" title="wikilink">NEXON</a></p></td>
<td style="text-align: left;"><p>2003年4月29日－至今</p></td>
<td style="text-align: left;"><p>12個伺服器、2個REBOOT伺服器、2個測試伺服器</p></td>
<td style="text-align: left;"><p>KMS</p></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><a href="../Page/日本.md" title="wikilink">日本</a></p></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"><p><a href="../Page/NEXON.md" title="wikilink">NEXON</a></p></td>
<td style="text-align: left;"><p>2003年12月3日－至今</p></td>
<td style="text-align: left;"><p>3個伺服器、1個REBOOT伺服器、1個測試伺服器</p></td>
<td style="text-align: left;"><p>JMS</p></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p><a href="../Page/中國.md" title="wikilink">中國</a></p></td>
<td style="text-align: left;"><p>冒　险　岛</p></td>
<td style="text-align: left;"><p><a href="../Page/盛大网络.md" title="wikilink">盛大游戏</a></p></td>
<td style="text-align: left;"><p>2004年7月23日－至今</p></td>
<td style="text-align: left;"><ul>
<li><strong>風之大陸</strong>：共20個伺服器（一區，<a href="../Page/中國電信.md" title="wikilink">中國電信</a>，其中蘑菇仔和青鱷魚互通，綠水靈和火野豬互通，章魚怪和星精靈互通，頑皮猴和小白兔互通，胖企鵝和小青蛇互通）。</li>
<li><strong>光之大陸</strong>：共14個伺服器（二區，中國電信，其中水晶鑽，貓眼石和月石川互通，紫水晶和黑珍珠互通，黃水晶，藍水晶，海藍石和星石（申）互通，蛋白石和石榴石互通，黃水晶和藍水晶互通）。</li>
<li><strong>雲之大陸</strong>：共5個伺服器（三區，<a href="../Page/中國網通.md" title="wikilink">中國網通</a>，其中瑪麗亞和薇薇安互通）。</li>
<li><strong>暗之大陸</strong>：共1個伺服器（四區，中國網通）。</li>
<li><strong>水之大陸</strong>：共13個伺服器（五區，中國電信，其中童話村和萬神殿互通）。</li>
</ul></td>
<td style="text-align: left;"><p>CMS</p></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><a href="../Page/美國.md" title="wikilink">美國</a></p></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"><p><a href="../Page/NEXON.md" title="wikilink">NEXON</a></p></td>
<td style="text-align: left;"><p>2005年5月11日－至今</p></td>
<td style="text-align: left;"><p>7個伺服器、1個REBOOT伺服器</p></td>
<td style="text-align: left;"><p>GMS</p></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p><a href="../Page/臺灣.md" title="wikilink">臺灣</a></p></td>
<td style="text-align: left;"><p>新 楓 之 谷</p></td>
<td style="text-align: left;"><p><a href="../Page/遊戲橘子.md" title="wikilink">遊戲橘子</a></p></td>
<td style="text-align: left;"><p>2005年5月31日－至今</p></td>
<td style="text-align: left;"><p>6個伺服器、1個REBOOT伺服器、2個測試伺服器</p></td>
<td style="text-align: left;"><p>TMS</p></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><a href="../Page/東南亞.md" title="wikilink">東南亞</a></p></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"><p>Asiasoft</p></td>
<td style="text-align: left;"><p>2005年6月23日－至今</p></td>
<td style="text-align: left;"><p>9個伺服器</p></td>
<td style="text-align: left;"><p>MSEA</p></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p><a href="../Page/泰国.md" title="wikilink">泰国</a></p></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"><p>Asiasoft(前代理商)</p></td>
<td style="text-align: left;"><p>2005年8月16日－2012年6月29日</p></td>
<td style="text-align: left;"><p>4个伺服器(舊版)</p></td>
<td style="text-align: left;"><p>MSTH</p></td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><a href="../Page/NEXON.md" title="wikilink">NEXON</a></p></td>
<td style="text-align: left;"><p>2017年10月31日-至今</p></td>
<td style="text-align: left;"><p>3個伺服器</p></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><a href="../Page/欧洲.md" title="wikilink">欧洲</a></p></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"><p><a href="../Page/NEXON.md" title="wikilink">NEXON</a></p></td>
<td style="text-align: left;"><p>2007年4月12日－2016年11月10日</p></td>
<td style="text-align: left;"><p>被併入國際版Luna伺服器</p></td>
<td style="text-align: left;"><p>EMS(被併入美國版本)</p></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p><a href="../Page/香港.md" title="wikilink">香港</a></p></td>
<td style="text-align: left;"><p>新楓之谷</p></td>
<td style="text-align: left;"><p><a href="../Page/游戏橘子.md" title="wikilink">游戏橘子</a></p></td>
<td style="text-align: left;"><p>2007年7月1日－2009年2月23日</p></td>
<td style="text-align: left;"><p>被併入臺版愛麗西亞伺服器</p></td>
<td style="text-align: left;"><p>TMS(被併入臺灣版本)</p></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><a href="../Page/巴西.md" title="wikilink">巴西</a></p></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"><p>Level Up</p></td>
<td style="text-align: left;"><p>2008年6月10日－2011年10月22日</p></td>
<td style="text-align: left;"><p>1个伺服器</p></td>
<td style="text-align: left;"><p>BMS</p></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p><a href="../Page/越南.md" title="wikilink">越南</a></p></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"><p><a href="../Page/VinaGame.md" title="wikilink">VinaGame</a></p></td>
<td style="text-align: left;"><p>2008年6月25日－2010年9月14日</p></td>
<td style="text-align: left;"><p>2个伺服器</p></td>
<td style="text-align: left;"><p>VMS</p></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p><a href="../Page/印尼.md" title="wikilink">印尼</a></p></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"><p><a href="../Page/lytogame.md" title="wikilink">lytogame</a></p></td>
<td style="text-align: left;"><p>2014年6月18日－2015年12月16日</p></td>
<td style="text-align: left;"><p>1个伺服器</p></td>
<td style="text-align: left;"><p>IMS</p></td>
</tr>
</tbody>
</table>

## 職業

### 一般職業

新楓之谷遊戲中職業群組目前分為10種:-{zh-tw:冒險家、皇家騎士團、末日反抗軍、英雄團、曉之陣、超新星、神之子、幻獸師、凱內西斯、雷普族;zh-cn:冒险者、冒险骑士团、新英雄、曉之陣、反抗者、诺巴、神之子、幻獸師、超能力者、翼人族}-

| 職業身分                             | \-{zh-tw:劍士;zh-cn:战士}-        | \-{zh-tw:法師;zh-cn:魔法师}-       | \-{zh-tw:弓箭手;zh-cn:弓箭手}-   | \-{zh-tw:盜賊;zh-cn:飞侠}-     | \-{zh-tw:海盗;zh-cn:海盗}-       |
| -------------------------------- | ----------------------------- | ----------------------------- | -------------------------- | -------------------------- | ---------------------------- |
| \-{zh-tw:冒險家;zh-cn:冒险者}-         | \-{zh-tw:英雄;zh-cn:英雄}-        | \-{zh-tw:火毒大魔導士;zh-cn:火毒魔导师}- | \-{zh-tw:箭神;zh-cn:神射手}-    | \-{zh-tw:夜使者;zh-cn:隐士}-    | \-{zh-tw:拳霸;zh-cn:冲锋队长}-     |
| \-{zh-tw:聖騎士;zh-cn:圣骑士}-         | \-{zh-tw:冰雷大魔導士;zh-cn:冰雷魔导师}- | \-{zh-tw:神射手;zh-cn:箭神}-       | \-{zh-tw:暗影神偷;zh-cn:侠盗}-   | \-{zh-tw:槍神;zh-cn:船长}-     |                              |
| \-{zh-tw:黑騎士;zh-cn:黑骑士}-         | \-{zh-tw:主教;zh-cn:主教}-        | \-{Pathfinder}-               | \-{zh-tw:影武者;zh-cn:暗影双刀}-  | \-{zh-tw:重砲指揮官;zh-cn:神炮王}- |                              |
| \-{zh-tw:皇家騎士團;zh-cn:冒险骑士团}-     | \-{zh-tw:聖魂劍士;zh-cn:魂骑士}-     | \-{zh-tw:烈焰巫師;zh-cn:炎术士}-     | \-{zh-tw:破風使者;zh-cn:风灵使者}- | \-{zh-tw:暗夜行者;zh-cn:夜行者}-  | \-{zh-tw:閃雷悍將;zh-cn:奇袭者}-    |
| \-{zh-tw:皇家騎士團團長;zh-cn:冒险骑士团团长}- | \-{zh-tw:米哈逸;zh-cn:米哈尔}-      |                               |                            |                            |                              |
| \-{zh-tw:英雄團;zh-cn:新英雄}-         | \-{zh-tw:狂狼勇士;zh-cn:战神}-      | \-{zh-tw:龍魔導士;zh-cn:龙神}-      | \-{zh-tw:精靈遊俠;zh-cn:双弩精灵}- | \-{zh-tw:幻影俠盜;zh-cn:幻影}-   | \-{zh-tw:隱月;zh-cn:隐月}-       |
| \-{zh-tw:夜光;zh-cn:夜光法师}-         |                               |                               |                            |                            |                              |
| \-{zh-tw:末日反抗軍;zh-cn:反抗者}-       | \-{zh-tw:爆拳槍神;zh-cn:爆破手}-     | \-{zh-tw:煉獄巫師;zh-cn:唤灵斗师}-    | \-{zh-tw:狂豹獵人;zh-cn:豹驽游侠}- |                            | \-{zh-tw:機甲戰神;zh-cn:机械师}-    |
| \-{zh-tw:末日反抗軍支援者;zh-cn:反抗支援者}-  | \-{zh-tw:惡魔殺手;zh-cn:恶魔猎手}-    |                               |                            | \-{zh-tw:傑諾;zh-cn:尖兵}-     |                              |
| \-{zh-tw:惡魔復仇者;zh-cn:恶魔复仇者}-     |                               |                               |                            |                            |                              |
| \-{zh-tw:超新星;zh-cn:诺巴}-          | \-{zh-tw:凱薩;zh-cn:狂龙战士}-      |                               |                            | \-{zh-tw:卡蒂娜;zh-cn:魔链影士}-  | \-{zh-tw:天使破壞者;zh-cn:爆莉萌天使}- |
| \-{zh-tw:凱內西斯;zh-cn:皇家神兽学院超能者}-  |                               | \-{zh-tw:凱內西斯;zh-cn:超能力者}-    |                            |                            |                              |
| \-{zh-tw:神之子;zh-cn:神之子}-         | \-{zh-tw:神之子;zh-cn:神之子}-      |                               |                            |                            |                              |
| \-{zh-tw:雷普族;zh-cn:翼人族}-         |                               | \-{zh-tw:伊利恩;zh-cn:圣晶使徒}-     |                            |                            | \-{zh-tw:亞克;zh-cn:影魂异人}-     |

### 海外特色職業

：可以創立，：原先可以創立，：不可以創立

<table>
<tbody>
<tr class="odd">
<td></td>
<td><p>職業類別</p></td>
<td><p>日本</p></td>
<td><p>國際</p></td>
<td><p>中國</p></td>
<td><p>臺灣</p></td>
<td><p>東南亞</p></td>
<td><p>泰國</p></td>
</tr>
<tr class="even">
<td><p>-{zh-tw:蒼龍俠客;zh-cn:龍的傳人}-</p></td>
<td><p>海盜</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>Jett[2]</p></td>
<td><p>海盜</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>劍豪</p></td>
<td><p>-{zh-tw:劍士;zh-cn:战士}-</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>陰陽師</p></td>
<td><p>-{zh-tw:法師;zh-cn:魔法师}-</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>-{zh-tw:幻獸師;zh-cn:林之灵}-</p></td>
<td><p>-{zh-tw:法師;zh-cn:魔法师}-</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 活動限定職業

\-{zh-tw:皮卡啾;zh-cn:品客缤}-：職業類別為劍士

## 登場人物

### 黑魔法師

  -
    軍團長

:; 獅子王凡雷恩

:; 阿卡伊農

:; 希拉

:; 惡魔

:; 戴米安

:: 聲優:石谷春貴

:; 殺人鯨

:: 聲優:久保ユリカ

:; 史烏

:: 聲優:蒼井翔太

:; 威爾

:; 露希妲

  -

      -
        聲優:愛美

### 皇家騎士團

  - 西格諾斯
    神獸
    那因哈特
    **米哈逸**
    **奧茲**
    **伊麗娜**
    **伊卡勒特**
    **鷹眼**

### 英雄

  - 狂狼勇士
    聲優:矢野亜沙美
  - 龍魔島士
    聲優:古川慎
  - 精靈遊俠
    聲優:三澤紗千香
  - 幻影俠盜
    聲優:西山宏太朗
  - 夜光
    聲優:森嶋秀太
  - 隠月
    聲優:武内駿輔

## 遊戲問題及爭議

  - 電磁紀錄
    遊戲橘子保留電磁紀錄一百天，期限已過則不得追查，每次追查將花費新台幣五百元整。

<!-- end list -->

  - 伺服器回溯問題
    臺灣新楓之谷伺服器，時常在玩家上線人數較多時發生延遲(Lag)情形，嚴重時會發生伺服器大斷線甚至[回溯情形](../Page/系統回溯.md "wikilink")，導致玩家經驗值、金幣、道具被回復到先前某時間點的狀態，又因為遊戲橘子宣稱無遊戲歷程相關電磁紀錄，因此玩家在伺服器發生回溯情形時的非現金道具損失皆無法補償，最早2007年的無預警大回朔，對玩家投下了史無前例的震撼彈，從此開始對遊戲失去信賴。

<!-- end list -->

  - 伺服器維護時間
    臺灣新楓之谷維護時間為每週三8:00-14:00，改版維修則通常為0:00-10:00（官方會公告）。目前最長為2013年5月8日的「超能再起」改版，由5月8日0:00維護到5月9日0:00，歷時24小時維修時間，伺服器短暫開放後，又於5月9號上午11時關閉進行超技能改版臨時維修。

<!-- end list -->

  - 外掛問題
    在新楓之谷中，有些玩家會利用修改記憶體，來改變新楓之谷主程式的運行，藉此取得大量遊戲虛擬幣。但由於這類玩家所創的角色數量龐大，使官方難以查緝，而使許多玩家因而受到影響。
    例如有名的惡劣外掛超越者玩家，館長阿帝冥，長期霸佔新楓之谷武陵道場第一排名、The
    Seed綠野仙蹤海底之塔第一排名，世人稱其名為『館長』導致新楓之谷不得不加強玩家遊玩排名外掛偵測，詳情見官方遊戲公告。

### 其他BUG

自遊戲正式營運至今，新楓之谷出現過無數[BUG](../Page/BUG.md "wikilink")，每個月不斷產生各種問題，造成玩家困擾，而部分問題尚未解決而持續至今。\[3\]

## 外掛防護

新楓之谷之外掛程式撰寫容易，使用[Visual
Basic或更高階程式語言便能完成](../Page/Visual_Basic.md "wikilink")。其一原因在於遊戲一開始的對話方塊（防外掛系統未載入前的視窗畫面），使程序容易被鎖定或注入DLL檔（V046到V054版本時楓之谷都沒有對話方塊，於V054更新後才出現），而不會遭防外掛系統轉向。(
自從台版V184改成使用 [XIGNCODE](../Page/XIGNCODE.md "wikilink") 防掛軟件後 ，使用[Visual
Basic所寫的外掛皆會遭](../Page/Visual_Basic.md "wikilink")
[XIGNCODE](../Page/XIGNCODE.md "wikilink")
偵測，因此當前楓之谷外掛皆使用[C++來編寫](../Page/C++.md "wikilink")。

### 外掛防護系統

  - nProtect GameGuard

<!-- end list -->

  -
    初期各版本楓之谷使用**「[nProtect
    GameGuard](../Page/nProtect_GameGuard.md "wikilink")」**進行[外掛防護措施](../Page/遊戲外掛.md "wikilink")
    在開始遊戲及遊戲進行時自動偵測玩家行為以及確認[電腦病毒](../Page/電腦病毒.md "wikilink")。

<!-- end list -->

  - Ahnlab HackShield

<!-- end list -->

  -
    中期各版本楓之谷使用**「[Ahnlab
    HackShield](../Page/Ahnlab_HackShield.md "wikilink")」**進行[外掛防護措施](../Page/遊戲外掛.md "wikilink")。
    由於NEXON將其旗下多款遊戲防外掛軟體由「nProtect GameGuard」改成「Ahnlab
    HackShield」，臺灣楓之谷也於2009年4月28日的V092版更新後使用了「Ahnlab
    HackShield」防外掛軟體。

<!-- end list -->

  - XIGNCODE

<!-- end list -->

  -
    現在各版本楓之谷陸續使用**「[XIGNCODE3](../Page/XIGNCODE3.md "wikilink")」**進行[外掛防護措施](../Page/遊戲外掛.md "wikilink")。
    韓國楓之谷防外掛軟體從Ahnlab
    HackShield更改為XIGNCODE3，臺灣楓之谷也於2015年10月21日的V184版更新後使用了「XIGNCODE」防外掛軟體。但現今韓國；臺灣楓之谷已不再使用「XIGNCODE3」防護系統。

<!-- end list -->

  - BlackCipher (Nexon Game Security)

<!-- end list -->

  -
    NEXON旗下遊戲的防外掛軟體，臺灣楓之谷於2013年5月22日的V157.2版更新後新增「BlackCipher」防護系統。

<!-- end list -->

  - Game Protect Kit

<!-- end list -->

  -
    中国大陆的冒险岛于2017年1月18日起的V139版更新后使用Game Protect Kit配合BlackCipher反外挂。

## 衍生作品

### 同名動畫

2006年11月9日，[NEXON
Japan發表了與](../Page/NEXON.md "wikilink")[MADHOUSE的合作](../Page/MADHOUSE.md "wikilink")，將楓之谷製成[動畫](../Page/動畫.md "wikilink")。目的在於可使尚未接觸楓之谷的玩家也能參予楓之谷的世界，達到增加玩家人數效果。動畫在2007年10月7日開始放送，全25話。

### NDS游戏

2007年1月9日韓國[NEXON在發表了預定移植MapleStory至](../Page/NEXON.md "wikilink")[Nintendo
DS的消息](../Page/Nintendo_DS.md "wikilink")。

### 网页游戏

2012年3月12日，雅虎日本开启MapleStory的网页游戏版本《-{zh-hans:冒险;zh-hant:皇家}-骑士团》()，同年腾讯中文版《冒险岛之旅》上线，但二者运营没多久就宣布关服。\[4\]

### 手机游戏

2014年9月，NEXON推出智能手机平台新作《-{zh-hans:冒险岛手游;zh-hant:口袋楓之谷}-》(，)。\[5\]2015年8月5日宣布上架日区。\[6\]

2015年，遊戲橘子旗下的蜂玩娛樂代理該作，于8月初推出台港澳地區服務器，譯名為口袋楓之谷，台港澳地區服務器於2017/3/31關閉。\[7\]中国则由世纪天成代理，于2015年9月22日正式运营，译作冒险岛手游。\[8\]

2018年2月22日，Nexon推出新作即時戰略遊戲《楓之谷 : 突擊》（Maple Blitz
X／메이플블리츠X，海外版名稱：MapleStory Blitz）

2016年，Nexon推出新作《MapleStory M》，於2016年10月13日下午3點公測。\[9\]

### 冒险岛2

新楓之谷的續作。與前作不同的是，《楓之谷2》採用的是3D視角。

## 相关事件

### 抄襲事件

2011年6月，北京奇客星空網路技術有限公司推出了一款名叫《[冒險王](../Page/冒險王_\(遊戲\).md "wikilink")》的[網頁遊戲](../Page/網頁遊戲.md "wikilink")，在遊戲系統、畫風乃至怪物形態上大量模仿《MapleStory》，引發MapleStory玩家非議。

2015年，奇客创想公司又推出了《冒险島——口袋里的冒险岛》手游再次引起争议。因正值Nexon发行正版手游之际，盛大网络与世纪天成相继将其告上法院，不久该游戏宣布关闭。目前Play商店仍可搜尋到《冒險島》\[10\]\[11\]

## 註釋

## 參考資料

  - [盛大網路（中國）-{冒險島}-官方網站遊戲公告](http://mxd.sdo.com/web5/news/newsList.asp?CategoryID=a)
  - [遊戲橘子（臺灣）-{新楓之谷}-官方網站遊戲公告](http://tw.beanfun.com/maplestory/BullentinList.aspx?cate=65)

## 外部連結

**官方網站**

  - [韓國](http://maplestory.nexon.com/)
  - [日本](http://maplestory.nexon.co.jp/)
  - [國際](http://maplestory.nexon.net)
  - [臺灣](http://tw.beanfun.com/maplestory/index.aspx)
  - [中國](http://mxd.sdo.com/web5/home/home.asp)
  - [東南亞](http://maple.asiasoftsea.com/)
  - [泰國](https://th.nexon.com/maplestory/th/home/home.aspx/)

[M](../Category/大型多人在线角色扮演游戏.md "wikilink")
[M](../Category/Windows遊戲.md "wikilink")
[M](../Category/2003年電子遊戲.md "wikilink")
[\*](../Category/楓之谷.md "wikilink")
[Category:盛大游戏](../Category/盛大游戏.md "wikilink")

1.
2.  技能模組與-{zh-tw:蒼龍俠客;zh-cn:龍的傳人}-共用，只有技能特效不一樣。
3.
4.
5.
6.
7.
8.
9.
10.
11.