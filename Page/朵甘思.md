**朵甘思**（），又名**朵甘**、**朵康**\[1\]、**多康**等，为[中国古代一地区名](../Page/中国.md "wikilink")。范围大致相当于今[西藏自治区](../Page/西藏自治区.md "wikilink")[昌都地区](../Page/昌都地区.md "wikilink")、[青海省](../Page/青海省.md "wikilink")[玉树藏族自治州东部和](../Page/玉树藏族自治州.md "wikilink")[四川省](../Page/四川省.md "wikilink")[甘孜藏族自治州](../Page/甘孜藏族自治州.md "wikilink")。

朵甘思的意思是汇合的区域。近代一般简称[康](../Page/康区.md "wikilink")（）。

## 吐蕃王朝

[吐蕃王朝](../Page/吐蕃王朝.md "wikilink")7世纪开始对外扩张，其向东征服的地区被称为朵甘思，与[卫藏四茹相区别](../Page/卫藏四茹.md "wikilink")。后来朵甘思北部被分出[朵思麻](../Page/朵思麻.md "wikilink")，南部仍被称为朵甘思。

## 元朝

[忽必烈曾经过此处南征](../Page/忽必烈.md "wikilink")[大理](../Page/大理.md "wikilink")。[元](../Page/元朝.md "wikilink")[中统和](../Page/中統_\(年號\).md "wikilink")[至元年间](../Page/至元_\(忽必烈\).md "wikilink")，该地区各部落相继投向元朝政府。至元十三年（1276年），设宁远府（府城在今四川[乾宁北](../Page/乾寧縣.md "wikilink")），至元十五年（1278年），又设李唐州（今四川[理塘](../Page/理塘.md "wikilink")）。

元灭[南宋之后](../Page/南宋.md "wikilink")，将原[吐蕃之地划归](../Page/吐蕃.md "wikilink")[宣政院管理](../Page/宣政院.md "wikilink")，朵甘思属[吐蕃等路宣慰使司都元帅府](../Page/吐蕃等路宣慰使司都元帅府.md "wikilink")，又被称为朵甘思宣慰司。

## 明朝

[明朝在当地置](../Page/明朝.md "wikilink")[朵甘衛](../Page/朵甘衛.md "wikilink")，後來升為[朵甘行都指挥使司](../Page/朵甘行都指挥使司.md "wikilink")、[朵甘都指挥使司](../Page/朵甘行都指挥使司.md "wikilink")。

## 参考文献

## 参見

  - [安多地区](../Page/安多地区.md "wikilink")、[康區](../Page/康區.md "wikilink")
  - [宣政院](../Page/宣政院.md "wikilink")

{{-}}

[Category:中国古地名](../Category/中国古地名.md "wikilink")
[Category:西藏历史](../Category/西藏历史.md "wikilink")

1.