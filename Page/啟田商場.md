[Kai_Tin_Shopping_Centre_(old_wing,_clear_view).jpg](https://zh.wikipedia.org/wiki/File:Kai_Tin_Shopping_Centre_\(old_wing,_clear_view\).jpg "fig:Kai_Tin_Shopping_Centre_(old_wing,_clear_view).jpg")
[Kai_Tin_Shopping_Centre_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Kai_Tin_Shopping_Centre_\(Hong_Kong\).jpg "fig:Kai_Tin_Shopping_Centre_(Hong_Kong).jpg")
[Kai_Tin_Shopping_Centre_old_wing.JPG](https://zh.wikipedia.org/wiki/File:Kai_Tin_Shopping_Centre_old_wing.JPG "fig:Kai_Tin_Shopping_Centre_old_wing.JPG")\]\]
[Kai_Tin_Shopping_Centre.jpg](https://zh.wikipedia.org/wiki/File:Kai_Tin_Shopping_Centre.jpg "fig:Kai_Tin_Shopping_Centre.jpg")
[FOODIN.jpg](https://zh.wikipedia.org/wiki/File:FOODIN.jpg "fig:FOODIN.jpg")
[FOODIN_(6).jpg](https://zh.wikipedia.org/wiki/File:FOODIN_\(6\).jpg "fig:FOODIN_(6).jpg")
[FOODIN_Entrance.jpg](https://zh.wikipedia.org/wiki/File:FOODIN_Entrance.jpg "fig:FOODIN_Entrance.jpg")
**啟田商場**位於[香港](../Page/香港.md "wikilink")[九龍東部](../Page/九龍.md "wikilink")[藍田半山](../Page/藍田_\(香港\).md "wikilink")，地址為啟田道
50
號，貼鄰[港鐵](../Page/港鐵.md "wikilink")[藍田站](../Page/藍田站.md "wikilink")，為藍田區內屋苑居民提供購物及消閒設施。啟田商場樓高四層，舊翼於1998年中落成，而新翼則於2004年落成。單是興建舊翼商場已耗資近3億元。

## 介紹

商場樓高四層，總面積達一萬多平方米，適合各種零售服務、[銀行](../Page/銀行.md "wikilink")、大型[超級市場](../Page/超級市場.md "wikilink")、[便利店](../Page/便利店.md "wikilink")、餅店、[酒樓及](../Page/酒樓.md "wikilink")[快餐](../Page/快餐.md "wikilink")。地下並設有一個面積逾1200平方米的整體出租街市，供應各類乾濕貨品，街市於2018年初進行翻新，7月16日重開，改稱啟田市場（英語：FOODIN）。該商場為領展的重點商場。

  - 啟田市場

FOODIN (2).jpg|[燒味檔](../Page/燒味.md "wikilink")、藥房及鮮肉檔 FOODIN
(3).jpg|瓜菜及豆腐芽菜凍肉檔 FOODIN
(4).jpg|糧油雞蛋[海味及水果檔](../Page/海味.md "wikilink")
FOODIN (5).jpg|水電工程店 FOODIN Stairs.jpg|通往商場的樓梯，以英倫風格設計 Kai Tin
Market.jpg|翻新前的啟田街市

## 建築

啟田商場第一期於1999年年初落成，2001年4月榮獲「[香港工程師學會結構分部](../Page/香港工程師學會.md "wikilink")」頒發「卓越結構大獎2001」。商場總面積有20,760平方米。全部工程於2004年竣工。
領展已經為商場進行第一期的翻新工程，預計2018年底完成。

## 商店

### 舊翼

  - [7-11便利店](../Page/7-11便利店.md "wikilink")
  - [惠康超級市場](../Page/惠康超級市場.md "wikilink")
  - 潮福[酒樓](../Page/酒樓.md "wikilink")
  - 私家診所
  - 眼鏡店
  - 珠寶店
  - 理髮店
  - 涼茶舖
  - [賽馬會投注站](../Page/香港賽馬會#.E5.A0.B4.E5.A4.96.E6.8A.95.E6.B3.A8.E8.99.95.md "wikilink")
  - [大昌食品市場](../Page/大昌食品市場.md "wikilink")
  - [啟田郵政局](../Page/啟田郵政局.md "wikilink")
  - [啟田市場](../Page/啟田市場.md "wikilink")（由建華集團承包的[街市](../Page/街市.md "wikilink")）
  - [多層停車場](../Page/多層停車場.md "wikilink")

### 新翼

  - [必勝客](../Page/必勝客.md "wikilink")
  - [AEON超級市場](../Page/永旺百貨.md "wikilink")
  - [三聯書局](../Page/三聯書局.md "wikilink")
  - [美國冒險樂園](../Page/美國冒險樂園.md "wikilink")
  - [759阿信屋](../Page/759阿信屋.md "wikilink")
  - [7-11](../Page/7-11.md "wikilink")
  - [Sasa化裝品店](../Page/莎莎國際.md "wikilink")
  - 中西藥行
  - 文具店
  - [日本城](../Page/日本城.md "wikilink")
  - 通寶行

## 公共交通

<table>
<thead>
<tr class="header">
<th><p>交通路線列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><dl>
<dt><a href="../Page/港鐵.md" title="wikilink">港鐵</a></dt>

</dl>
<ul>
<li><font color="{{觀塘綫色彩}}">█</font><a href="../Page/觀塘綫.md" title="wikilink">觀塘綫</a>：<a href="../Page/藍田站.md" title="wikilink">藍田站A出口</a></li>
</ul>
<dl>
<dt><a href="../Page/香港巴士.md" title="wikilink">巴士</a></dt>

</dl>
<dl>
<dt><a href="../Page/香港小巴.md" title="wikilink">專線小巴</a></dt>

</dl></td>
</tr>
</tbody>
</table>

## 參考

  - [香港房屋委員會](../Page/香港房屋委員會.md "wikilink")2001-2002年報

[Category:觀塘區商場](../Category/觀塘區商場.md "wikilink") [Category:藍田
(香港)](../Category/藍田_\(香港\).md "wikilink")
[Category:領展商場及停車場](../Category/領展商場及停車場.md "wikilink")