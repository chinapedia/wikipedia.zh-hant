《**Click入黃金屋**》（；前名《**新網中人**》）是[香港](../Page/香港.md "wikilink")[電視廣播有限公司拍攝製作的時裝溫情](../Page/電視廣播有限公司.md "wikilink")[劇集](../Page/劇集.md "wikilink")，全劇共20集，由[秦沛](../Page/秦沛.md "wikilink")、[黎耀祥](../Page/黎耀祥.md "wikilink")、[孫耀威](../Page/孫耀威.md "wikilink")、[郭羨妮及](../Page/郭羨妮.md "wikilink")[楊思琦領銜主演](../Page/楊思琦.md "wikilink")，並由[陳敏之](../Page/陳敏之.md "wikilink")、[韓馬利及](../Page/韓馬利.md "wikilink")[楊秀惠聯合主演](../Page/楊秀惠.md "wikilink")，[梅小青監製](../Page/梅小青.md "wikilink")。

## 演員表

### 方家

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/關係</strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/秦沛.md" title="wikilink">秦　沛</a></strong></p></td>
<td><p><strong>方學文</strong></p></td>
<td><p>「萬華書店」職員<br />
顏如玉之夫<br />
方心明、方心正、方心美之父<br />
「開卷有益」書店老闆</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/韓馬利.md" title="wikilink">韓馬利</a></strong></p></td>
<td><p><strong>顏如玉</strong></p></td>
<td><p>家庭主妇<br />
方學文之妻<br />
方心明、方心正、方心美之母</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/黎耀祥.md" title="wikilink">黎耀祥</a></strong></p></td>
<td><p><strong>方心明</strong></p></td>
<td><p><strong>Stephen、OK明</strong><br />
方學文、顏如玉之長子<br />
方心正、方心美之兄<br />
袁慧中之鬥氣冤家，後為男友</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/孫耀威.md" title="wikilink">孫耀威</a></strong></p></td>
<td><p><strong>方心正</strong></p></td>
<td><p><strong>Alex</strong><br />
INSS网络保安公司职员<br />
方學文、顏如玉之次子<br />
方心明之弟<br />
方心美之兄<br />
雷婉儀之男友，後分手，最後復合<br />
徐希敏之男友，後分手<br />
童年由<a href="../Page/马俊荣.md" title="wikilink">马俊荣饰演</a></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/何傲兒.md" title="wikilink">何傲兒</a></strong></p></td>
<td><p><strong>方心美</strong></p></td>
<td><p><strong>Sammi</strong><br />
高中生<br />
方學文、顏如玉之幼女<br />
方心明、方心正之妹<br />
暗戀雷樹培，後為女友</p></td>
</tr>
</tbody>
</table>

### 袁家

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/關係</strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/郭羨妮.md" title="wikilink">郭羨妮</a></strong></p></td>
<td><p><strong>袁慧中</strong></p></td>
<td><p><strong>Wendy、中瘟雞</strong><br />
「小妮子」紙黏土模型手工藝店店主（第14集被迫搬遷商場）<br />
與方心明原為冤家，于第7集化解誤會并成为好友<br />
方心明之女友、後為妻（第20集）<br />
魯德輝之前妻<br />
袁家曦之家姐<br />
黎佩慈之好友兼利用對象<br />
于第3集遭黎佩慈所騙，被迫搬家<br />
于第11集遭黎佩慈騙取10萬港幣投資水貨名錶、于第20集歸還<br />
于第19集遭袁家曦哄騙以投資畫廊名義，將樓層按押銀行貸款80萬港幣<br />
為救袁家曦而將貸款80萬元予其還債</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鄭俊弘.md" title="wikilink">鄭俊弘</a></p></td>
<td><p>袁家曦</p></td>
<td><p>袁慧中之弟</p></td>
</tr>
</tbody>
</table>

### 雷家

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/關係</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黃文標.md" title="wikilink">黃文標</a></p></td>
<td><p>雷文生</p></td>
<td><p>钱淑芬之前夫<br />
雷婉儀之父<br />
已去世</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/惠英紅.md" title="wikilink">惠英紅</a></p></td>
<td><p>錢淑芬</p></td>
<td><p>雷文生之前妻<br />
雷婉儀、徐希敏之母</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/楊思琦.md" title="wikilink">楊思琦</a></strong></p></td>
<td><p><strong>雷婉儀</strong></p></td>
<td><p><strong>Abbie、鴨髀</strong><br />
營養師<br />
錢淑芬之長女<br />
方心正之女友，後分手，最後復合<br />
雷樹培之姑姐<br />
徐希敏同母異父之姊<br />
童年由<a href="../Page/鄭可琳.md" title="wikilink">鄭可琳飾演</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/楊秀惠.md" title="wikilink">楊秀惠</a></strong></p></td>
<td><p><strong>徐希敏</strong></p></td>
<td><p><strong>Hillary</strong><br />
錢淑芬之幼女<br />
雷婉儀同母異父之妹<br />
方心正之女友，後分手</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/陳智燊.md" title="wikilink">陳智燊</a></strong></p></td>
<td><p><strong>雷樹培</strong></p></td>
<td><p><strong>Patrick</strong><br />
樓上咖啡店東主<br />
雷婉儀之姪<br />
方心美之好友，後為男友</p></td>
</tr>
</tbody>
</table>

### 唐家

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/關係</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/周驄.md" title="wikilink">周　驄</a></p></td>
<td><p>唐　伯</p></td>
<td><p>「萬華書店」前東主<br />
唐文豪之父<br />
唐琪琪之爺爺</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/羅浩楷.md" title="wikilink">羅浩楷</a></p></td>
<td><p>唐文豪</p></td>
<td><p>唐伯之子<br />
唐琪琪之父<br />
方學文前上司</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/李美慧.md" title="wikilink">李美慧</a></p></td>
<td><p>唐琪琪</p></td>
<td><p>高中學生<br />
唐文豪之女<br />
方心美、史盈盈、高詠欣之天敵</p></td>
</tr>
</tbody>
</table>

### INSS網路保安公司

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/關係</strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/孫耀威.md" title="wikilink">孫耀威</a></strong></p></td>
<td><p><strong>方心正</strong></p></td>
<td><p><strong>Alex</strong><br />
「技術支援部」總經理<br />
徐希敏之前男友<br />
參見<strong><a href="../Page/#方家.md" title="wikilink">方家</a></strong></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/楊秀惠.md" title="wikilink">楊秀惠</a></strong></p></td>
<td><p><strong>徐希敏</strong></p></td>
<td><p><strong>Hillary</strong><br />
「技術支援部」副經理<br />
方心正之前女友</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/張達倫.md" title="wikilink">張達倫</a></p></td>
<td><p>高家岳</p></td>
<td><p><strong>K.O</strong><br />
「INSS網路保安公司」經理<br />
徐希敏之外遇对象、于第12集分手<br />
因誤會徐希敏與方心正有曖昧而報復兩人<br />
于第12集勾結駭客，被方心正、徐希敏聯手合作揭發，遭公司開除</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/布偉傑.md" title="wikilink">布偉傑</a></p></td>
<td><p>Ken Wilson</p></td>
<td><p>「INSS網路保安公司」新任經理<br />
方心正之美國大學師兄兼上司</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阮兒.md" title="wikilink">阮　兒</a></p></td>
<td><p>Emily</p></td>
<td><p>「技術支援部」職員<br />
方心正之祕書<br />
暗恋方心正<br />
雷婉儀之情敌</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鄭世豪.md" title="wikilink">鄭世豪</a></p></td>
<td><p>Frankie</p></td>
<td><p>「技術支援部」職員<br />
方心正、徐希敏之下屬</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/曾愛媚.md" title="wikilink">曾愛媚</a></p></td>
<td><p>Daisy</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李霖恩.md" title="wikilink">李霖恩</a></p></td>
<td><p>Herman</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/王基信.md" title="wikilink">王基信</a></p></td>
<td><p>Albert</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蔡考藍.md" title="wikilink">蔡考藍</a></p></td>
<td><p>Ceci Chan</p></td>
<td></td>
</tr>
</tbody>
</table>

### 魯家

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/關係</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/馮素波.md" title="wikilink">馮素波</a></p></td>
<td><p>蘭　姨</p></td>
<td><p>魯德輝之母<br />
袁慧中之前奶奶<br />
張翠玲之奶奶<br />
長居老人院，第8集被家人接回同住</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/張松枝.md" title="wikilink">張松枝</a></p></td>
<td><p>魯德輝</p></td>
<td><p><strong>冇德輝</strong><br />
阿蘭之子<br />
袁慧中前夫<br />
張翠玲之夫<br />
方心明、口水昌、戴文強、大麻成中學舊同學，合稱「五虎將」<br />
阿英之客戶兼外遇對象</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/楊卓娜.md" title="wikilink">楊卓娜</a></p></td>
<td><p>張翠玲</p></td>
<td><p>魯德輝之祕書兼第二任妻子</p></td>
</tr>
</tbody>
</table>

### 其他演員

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>暱稱/關係</strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/陳敏之.md" title="wikilink">陳敏之</a></strong></p></td>
<td><p><strong>黎佩慈</strong></p></td>
<td><p><strong>Pansy</strong><br />
地產經紀<br />
袁慧中之好友</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/龔茜彤.md" title="wikilink">龔茜彤</a></strong></p></td>
<td><p><strong>高詠欣</strong></p></td>
<td><p><strong>Olive</strong><br />
方心美、史盈盈之同學兼好友<br />
唐琪琪之天敌</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/林漪娸.md" title="wikilink">林漪娸</a></strong></p></td>
<td><p><strong>宋書琦</strong></p></td>
<td><p>精神病患者<br />
暗戀方學文<br />
陸知賢之母<br />
於第20集刺傷雷婉儀</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳美詩.md" title="wikilink">陳美詩</a></p></td>
<td><p>Michelle</p></td>
<td><p>職業模特兒、女同性戀者<br />
MOSS成員之一<br />
雷樹培之暗戀對象<br />
喜歡方心美</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/蔣家旻.md" title="wikilink">蔣家旻</a></p></td>
<td><p>史盈盈</p></td>
<td><p><strong>Stephy</strong><br />
高中學生、SOS、MOSS成员之一<br />
方心美、高詠欣之同學兼好友<br />
唐琪琪之天敌<br />
Norman之前女友</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/林淑敏.md" title="wikilink">林淑敏</a></p></td>
<td><p>June</p></td>
<td><p>「小妮子」紙黏土模型店店員<br />
袁慧中之下屬</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/謝兆韵.md" title="wikilink">謝兆-{韵}-</a></p></td>
<td><p>Apple</p></td>
<td><p>高中學生<br />
唐琪琪之友</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鍾煌.md" title="wikilink">鍾　煌</a></p></td>
<td><p>芳　芳</p></td>
<td><p>高中學生<br />
方心美、高詠欣、史盈盈同學兼好友<br />
第16集誤為方心美網上偷試卷而疏離</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳勉良.md" title="wikilink">陳勉良</a></p></td>
<td><p>標　叔</p></td>
<td><p>「美豪大夏」看更</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/招石文.md" title="wikilink">招石文</a></p></td>
<td><p>華　叔</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/魏惠文.md" title="wikilink">魏惠文</a></p></td>
<td><p>何俊峰</p></td>
<td><p>「萬華書店」職員</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黃梓瑋.md" title="wikilink">黃梓瑋</a></p></td>
<td><p>阿　娥</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/許文翹.md" title="wikilink">許文翹</a></p></td>
<td><p>阿　雄</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/張笑千.md" title="wikilink">張笑千</a></p></td>
<td><p>阿　發</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/劉桂芳.md" title="wikilink">劉桂芳</a></p></td>
<td><p>蔡　太</p></td>
<td><p>顏如玉之好友</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李麗麗.md" title="wikilink">李麗麗</a></p></td>
<td><p>程　太</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/彭冠中.md" title="wikilink">彭冠中</a></p></td>
<td><p>戴少強</p></td>
<td><p><strong>打靶強</strong><br />
二手店老闆<br />
與方心明、口水昌、大麻成、魯德輝合稱「五虎將」<br />
涉嫌賣贓物，後自首</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/邵卓堯.md" title="wikilink">邵卓堯</a></p></td>
<td><p>口水昌</p></td>
<td><p>茶餐廳夥記<br />
與方心明、戴少強、大麻成、魯德輝合稱「五虎將」</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/羅天池.md" title="wikilink">羅天池</a></p></td>
<td><p>大麻成</p></td>
<td><p>水電師傅<br />
與方心明、口水昌、戴少強、魯德輝合稱「五虎將」</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/曾守明.md" title="wikilink">曾守明</a></p></td>
<td><p>阿　賢</p></td>
<td><p>警察</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黃子衡.md" title="wikilink">黃子衡</a></p></td>
<td><p>阿　維</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/何婷恩.md" title="wikilink">何婷恩</a></p></td>
<td><p>Eve</p></td>
<td><p>「流樓地咖啡Shop」店員<br />
雷樹培之下屬</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/趙嘉迪.md" title="wikilink">趙嘉迪</a></p></td>
<td><p>Liza</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/廖麗麗.md" title="wikilink">廖麗麗</a></p></td>
<td></td>
<td><p>顏如玉之好友（第1集）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/馮康寧.md" title="wikilink">馮康寧</a></p></td>
<td></td>
<td><p>「輝明傢具店」老闆（第1集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/何偉業.md" title="wikilink">何偉業</a></p></td>
<td></td>
<td><p>電腦維修員（第1集）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/周寶霖.md" title="wikilink">周寶霖</a></p></td>
<td><p>Ceci</p></td>
<td><p>方心正之朋友（第1集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黃俊伸.md" title="wikilink">黃俊伸</a></p></td>
<td><p>Peter</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/裘卓能.md" title="wikilink">裘卓能</a></p></td>
<td><p>Billy</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/賀文傑.md" title="wikilink">賀文傑</a></p></td>
<td></td>
<td><p>CID（第1集）</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/林遠迎.md" title="wikilink">林遠迎</a></p></td>
<td><p>財　哥</p></td>
<td><p>商場保安（第1-2集）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/孫季卿.md" title="wikilink">孫季卿</a></p></td>
<td><p>劍　叔</p></td>
<td><p>「萬華書店」老主顧（第1-2集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/梁珈詠.md" title="wikilink">梁珈詠</a></p></td>
<td><p>Ann</p></td>
<td><p>「小妮子」紙黏土模型店前店員<br />
向袁慧中討取加班費（第2集）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/凌禮文.md" title="wikilink">凌禮文</a></p></td>
<td><p>中醫師</p></td>
<td><p>（第2集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/曾玉娟.md" title="wikilink">曾玉娟</a></p></td>
<td><p>師　奶</p></td>
<td><p>（第2集）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/蘇恩磁.md" title="wikilink">蘇恩磁</a></p></td>
<td><p>張　太</p></td>
<td><p>袁慧中所租住樓層之包租婆<br />
與黎佩慈聯手騙袁慧中，而順利賣掉其樓層（第3集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/馬貫東.md" title="wikilink">馬貫東</a></p></td>
<td><p>-</p></td>
<td><p>藥局店員（第3集）</p></td>
</tr>
<tr class="even">
<td><p>-</p></td>
<td><p>名牌手袋廣告片美術指導（第14集）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鄭家俊.md" title="wikilink">鄭家俊</a></p></td>
<td><p>Dicky</p></td>
<td><p>在機場準備出國讀書之兒子（第4集）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陸駿光.md" title="wikilink">陸駿光</a></p></td>
<td><p>-</p></td>
<td><p>地產經紀<br />
遭黎佩慈搶生意（第4集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/楊瑞麟.md" title="wikilink">楊瑞麟</a></p></td>
<td><p>陳啟發</p></td>
<td><p>「港全保安公司」人事部經理<br />
方心明之上司（第5集）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/郭德信.md" title="wikilink">郭德信</a></p></td>
<td><p>程　生</p></td>
<td><p>程太之夫</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/張漢斌.md" title="wikilink">張漢斌</a></p></td>
<td><p>關　炳</p></td>
<td><p>「港全保安公司」保安（「ITC大樓」）<br />
陳啟發之下屬<br />
方心明之舊同事</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/余子明.md" title="wikilink">-{余}-子明</a></p></td>
<td><p>鑑　叔</p></td>
<td><p>長居老人院<br />
唐伯、蘭姨、譚伯之院友</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李海生.md" title="wikilink">李海生</a></p></td>
<td><p>譚　伯</p></td>
<td><p>長居老人院<br />
唐伯、鑑叔、蘭姨之院友</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>-</p></td>
<td><p>老人院之護士</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/劉天龍.md" title="wikilink">劉天龍</a></p></td>
<td><p>-</p></td>
<td><p>「發記<a href="../Page/潮州.md" title="wikilink">潮州</a><a href="../Page/打冷.md" title="wikilink">打冷</a>」餐廳夥計</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/游飈.md" title="wikilink">游　飈</a></p></td>
<td><p>肥　龍</p></td>
<td><p>大麻成朋友（第9集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/麥皓兒.md" title="wikilink">麥皓兒</a></p></td>
<td><p>-</p></td>
<td><p>「百囍酒樓」櫃台侍應（第9集）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳志健.md" title="wikilink">陳志健</a></p></td>
<td><p>Leon</p></td>
<td><p>唐琪琪前男友之一<br />
Michele之粉絲（第10集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳嘉露_(香港).md" title="wikilink">陳嘉露</a></p></td>
<td><p>-</p></td>
<td><p>商場時裝售貨員（第10集）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/何俊軒.md" title="wikilink">何俊軒</a></p></td>
<td><p>-</p></td>
<td><p>「嬌美化粧品」廣告片導演（第10集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/沈可欣.md" title="wikilink">沈可欣</a></p></td>
<td><p>高　太</p></td>
<td><p>高家岳之妻（第11集）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/單俊偉.md" title="wikilink">單俊偉</a></p></td>
<td><p>Nadal</p></td>
<td><p>唐琪琪前男友<br />
網球教練<br />
輸給雷樹培（第10集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/趙永洪.md" title="wikilink">趙永洪</a></p></td>
<td><p>Kenny Lam</p></td>
<td><p>地產經紀<br />
黎佩慈同事<br />
因不滿常受黎佩慈所欺，向其鼓吹投資港幣20萬炒作名錶（投資失敗）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鄭毅邦.md" title="wikilink">鄭毅邦</a></p></td>
<td><p>阿　海</p></td>
<td><p>第11集</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李豪.md" title="wikilink">李　豪</a></p></td>
<td><p>-</p></td>
<td><p>與方心明、方心正踢球之少年（第11集）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黃長發.md" title="wikilink">黃長發</a></p></td>
<td><p>邦　少</p></td>
<td><p>蘇豪富貴集團太子爺<br />
對Michele毛手毛腳當街遭摑（第12集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳錫江.md" title="wikilink">陳錫江</a></p></td>
<td><p>梁松柏</p></td>
<td><p>「開卷有益」書店前老闆<br />
方學文之老友<br />
第13集頂讓給方學文</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鄭嘉雯.md" title="wikilink">鄭嘉雯</a></p></td>
<td><p>-</p></td>
<td><p>名錶店售貨員（第12集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鄭健樂.md" title="wikilink">鄭健樂</a></p></td>
<td><p>-</p></td>
<td><p>沙灘壯男（第13集）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黃智文.md" title="wikilink">黃智文</a></p></td>
<td><p>Bennie</p></td>
<td><p>唐琪琪之沙灘排球隊友（第13集）</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>Coco</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/莊狄生.md" title="wikilink">莊狄生</a></p></td>
<td><p>-</p></td>
<td><p>「開卷有益」書店應徵者（第13集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/賀文傑.md" title="wikilink">賀文傑</a></p></td>
<td><p>-</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/溫尚儒.md" title="wikilink">溫尚儒</a></p></td>
<td><p>-</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳宙希.md" title="wikilink">陳宙希</a></p></td>
<td><p>-</p></td>
<td><p>陪朋友至藥局看病青年（第13集）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/歐梓浩.md" title="wikilink">歐梓浩</a></p></td>
<td><p>-</p></td>
<td><p>藥局看病青年（第13集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/江梓瑋.md" title="wikilink">江梓瑋</a></p></td>
<td><p>-</p></td>
<td><p>藥局店員（第13集）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鄧英敏.md" title="wikilink">鄧英敏</a></p></td>
<td><p>Jeff</p></td>
<td><p>名牌手袋廣告投資商（第14集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/楊證樺.md" title="wikilink">楊證樺</a></p></td>
<td><p>David</p></td>
<td><p>Michelle經紀人</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/李穎芝.md" title="wikilink">李穎芝</a></p></td>
<td><p>-</p></td>
<td><p>名牌手袋廣告之Model（第14集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/曾婉莎.md" title="wikilink">曾婉莎</a></p></td>
<td><p>-</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/曾健明.md" title="wikilink">曾健明</a></p></td>
<td><p>彭經理</p></td>
<td><p>商場經理（第14集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳狄克.md" title="wikilink">陳狄克</a></p></td>
<td><p>志　康</p></td>
<td><p>「開卷有益」書店職員（第15集）<br />
第16集因健康因素逼迫方學文補錢離職</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/寧進.md" title="wikilink">寧　進</a></p></td>
<td><p>-</p></td>
<td><p>「L'amour」餐廳侍應（第15集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/周麗欣.md" title="wikilink">周麗欣</a></p></td>
<td><p>阿　英</p></td>
<td><p>魯德輝客戶兼外遇對象（第15集）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/戴耀明.md" title="wikilink">戴耀明</a></p></td>
<td><p>阿　威</p></td>
<td><p>「港全保安公司」保全（「ITC大樓」）<br />
方心明前同事<br />
關炳同事<br />
陳啟發之下屬（第15集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鄒永彪.md" title="wikilink">鄒永彪</a></p></td>
<td><p>客　人</p></td>
<td><p>第17集</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/蘇麗明.md" title="wikilink">蘇麗明</a></p></td>
<td><p>-</p></td>
<td><p>警察（罪案調查科）<br />
調查「網上偷試卷」一案（第17集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/沈震軒.md" title="wikilink">沈震軒</a></p></td>
<td><p>-</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/張穎康.md" title="wikilink">張穎康</a></p></td>
<td><p>-</p></td>
<td><p>督察<br />
調查「網上偷試卷」一案（第17集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/李楓.md" title="wikilink">李　楓</a></p></td>
<td><p>徐王麗霞</p></td>
<td><p>徐期浩之母（第18集）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黃長興.md" title="wikilink">黃長興</a></p></td>
<td><p>徐期浩</p></td>
<td><p><strong>Norman</strong><br />
整型醫生<br />
王麗霞之子<br />
黎佩慈男友</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黃淑君.md" title="wikilink">黃淑君</a></p></td>
<td><p>Irena</p></td>
<td><p>整型醫院護士（第18集）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/楊鴻俊.md" title="wikilink">楊鴻俊</a></p></td>
<td><p>-</p></td>
<td><p>袁家曦朋友（第18集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/蔡康年.md" title="wikilink">蔡康年</a></p></td>
<td><p>陳大良</p></td>
<td><p>北京「清仁南大學美術系」畢業<br />
香港著名畫家協會成員之一<br />
「良辰美景」畫廊老闆<br />
偶爾客串舞台劇、電視劇集演出<br />
受袁家曦所騙，誤為與其合作畫廊生意（第19集）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/湯俊明.md" title="wikilink">湯俊明</a></p></td>
<td><p>-</p></td>
<td><p>「開卷有益」之顧客（第19集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/卓躒.md" title="wikilink">卓　躒</a></p></td>
<td><p>-</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/麥子樂.md" title="wikilink">麥子樂</a></p></td>
<td><p>陸知賢</p></td>
<td><p>宋書琦之子（第20集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/吳香倫.md" title="wikilink">吳香倫</a></p></td>
<td><p>何秀瑩</p></td>
<td></td>
</tr>
</tbody>
</table>

## 播放軼事

  - 該劇於翡翠台首播時的播映時段本來播放《[盛世仁傑](../Page/盛世仁傑.md "wikilink")》，宣傳片已播放出街，後來電視台管理層臨時抽調《Click入黃金屋》播放，原因爲近期劇集之劇情[頗重](../Page/頗重.md "wikilink")，鑑於《[盛世仁傑](../Page/盛世仁傑.md "wikilink")》劇情也頗重，所以希望改播一些溫情小品劇，劇情也比較輕鬆。巧合的是，《盛》及《C》劇均有[郭羨妮](../Page/郭羨妮.md "wikilink")、[黎耀祥爲主要演員](../Page/黎耀祥.md "wikilink")。《[盛世仁傑](../Page/盛世仁傑.md "wikilink")》最後延至2012年4月才在翡翠台首播。

## 收視

以下為本劇於[香港](../Page/香港.md "wikilink")[無綫電視](../Page/無綫電視.md "wikilink")[翡翠台之收視紀錄](../Page/翡翠台.md "wikilink")：

|        |                         |                                  |                                  |         |                        |
| ------ | ----------------------- | -------------------------------- | -------------------------------- | ------- | ---------------------- |
| **週次** | **日期**                  | **香港平均收視**                       | **最高收視**                         | **百分比** | 广州CSM收视                |
| 1      | 2008年12月8日-2008年12月12日  | 27[點](../Page/收視點.md "wikilink") | 31[點](../Page/收視點.md "wikilink") | 87%     | 1-18集16.73 19-20集16.99 |
| 2      | 2008年12月15日-2008年12月19日 | 25[點](../Page/收視點.md "wikilink") |                                  | 86%     |                        |
| 3      | 2008年12月22日-2008年12月26日 | 24[點](../Page/收視點.md "wikilink") |                                  | 85%     |                        |
| 4      | 2008年12月29日-2009年1月2日   | 28[點](../Page/收視點.md "wikilink") | 33[點](../Page/收視點.md "wikilink") | 85%     |                        |

## 註腳

## 電視節目的變遷

|align="center" colspan="4"|[畢打自己人](../Page/畢打自己人.md "wikilink")
2008年10月20日-2010年2月12日 |- |align="center" colspan="1"|**上一套：**
[東山飄雨西關晴](../Page/東山飄雨西關晴.md "wikilink")
\-12月5日 |align="center" colspan="2"
|**翡翠台/高清翡翠台第二綫劇集（[2008](../Page/翡翠台電視劇集列表_\(2008年\)#第二線劇集.md "wikilink")-[2009](../Page/翡翠台電視劇集列表_\(2009年\)#第二線劇集.md "wikilink")）**
**Click入黃金屋**
12月8日-1月2日 |align="center" colspan="1"|**下一套：**
[鹿鼎記](../Page/鹿鼎記_\(2008年電視劇\).md "wikilink")
1月5日- |- |align="center"
colspan="4"|[珠光寶氣](../Page/珠光寶氣_\(電視劇\).md "wikilink")
2008年10月20日-2009年2月13日

[Category:2008年無綫電視劇集](../Category/2008年無綫電視劇集.md "wikilink")
[Category:2009年無綫電視劇集](../Category/2009年無綫電視劇集.md "wikilink")