**OGLE-TR-56b**是一顆距離地球約1500秒差距的[太陽系外行星](../Page/太陽系外行星.md "wikilink")，位於[人馬座](../Page/人馬座.md "wikilink")，母恆星是
[OGLE-TR-56](../Page/OGLE-TR-56.md "wikilink")。該行星發現於2002年11月3日\[1\]，由波蘭[華沙大學的](../Page/華沙大學.md "wikilink")[光學重力透鏡實驗計畫以凌日法發現](../Page/光學重力透鏡實驗.md "wikilink")，並於2003年1月4日以[都卜勒光譜學確認](../Page/都卜勒光譜學.md "wikilink")\[2\]。該行星在
[WASP-12b](../Page/WASP-12b.md "wikilink")
被發現以前是已確認行星中公轉週期最短的行星\[3\]。該行星的週期暗示它和母恆星距離相當近，因此被列為[熱木星](../Page/熱木星.md "wikilink")。

[OGLE-TR-56_b_rv.pdf](https://zh.wikipedia.org/wiki/File:OGLE-TR-56_b_rv.pdf "fig:OGLE-TR-56_b_rv.pdf")

根據理論，該行星表面會有液態鐵成分的雨\[4\]。

## 參見

  - [光學重力透鏡實驗](../Page/光學重力透鏡實驗.md "wikilink")

## 參考資料

## 外部連結

  -
  -
  -
[de:OGLE-TR-56 b](../Page/de:OGLE-TR-56_b.md "wikilink")

[Category:熱木星](../Category/熱木星.md "wikilink")
[Category:人馬座](../Category/人馬座.md "wikilink")
[Category:凌星現象](../Category/凌星現象.md "wikilink")
[Category:2002年发现的系外行星](../Category/2002年发现的系外行星.md "wikilink")

1.
2.
3.
4.