\-{zh:;zh-tw:;zh-hk:;zh-cn:;zh-sg:;}-

**蔥**（[学名](../Page/学名.md "wikilink")：**），別名**青蔥**、**大蔥**、**葉蔥**、胡蔥、蔥仔、菜伯、水蔥和事草\[1\]，为多年生[草本植物](../Page/草本植物.md "wikilink")，叶子圆筒形，中间空，脆弱易折，呈青色。在東亞國家以及各處亞裔地區中，蔥常作為一种很普遍的香料[调味品或](../Page/调味品.md "wikilink")[蔬菜食用](../Page/蔬菜.md "wikilink")，在東方烹調中佔有重要的角色。

亞洲人习惯于在炒菜前将蔥和[薑切碎一起下油锅中炒至金黄](../Page/薑.md "wikilink")（俗稱之為「爆香」或“炝锅”），爾後再将其他蔬菜下入锅中炒。此外，将葱生吃在中国[山东省和](../Page/山东省.md "wikilink")[东北三省的某些地区也比较流行并且是一种常见的飲食習慣](../Page/东北三省.md "wikilink")(或[食疗手段](../Page/食疗.md "wikilink"))，比如山东的传统名吃“煎饼卷大葱”。在做汤麵如清湯麵或[牛肉麵时](../Page/牛肉麵.md "wikilink")，在麵條熟后可将切碎的葱末（也称葱花）撒在麵上。[日本料理中](../Page/日本料理.md "wikilink")，比如[味噌湯](../Page/味噌湯.md "wikilink")，碎蔥也是不可或缺的。

## 语源

属名“[Allium](../Page/wikt:Allium#拉丁语.md "wikilink")”为[中性名词](../Page/性_\(语法\).md "wikilink")，意为“葱/蒜”；种名“fistulosum”为形容词“[fistulosus](../Page/wikt:fistulosus#拉丁语.md "wikilink")”的中性[主格形式](../Page/主格.md "wikilink")，意为“中空的”。

## 生物学特性

## 中醫學藥效

《[神農本草經](../Page/神農本草經.md "wikilink")》謂蔥白「主傷寒，寒熱，出汗，中風，面目腫。」取用部位為蔥白部份或全株。

### 性味歸經

  - 味辛
  - 性溫
  - 入[肺](../Page/肺.md "wikilink")、[胃經](../Page/胃.md "wikilink")

### 功效

  - 本草，。

<!-- end list -->

  - 《[神農本草經](../Page/神農本草經.md "wikilink")》民國復古輯本：蔥實，味辛，溫，無毒。主明目，補中不足。其莖，平。作湯，治傷寒寒熱，出汗，中風，面目腫。

<!-- end list -->

  - 內科

<!-- end list -->

  - 《[傷寒雜病論](../Page/傷寒雜病論.md "wikilink")》少陰病篇：[經方用全蔥或蔥白](../Page/經方.md "wikilink")，以達所謂[通陽的效果](../Page/通陽.md "wikilink")，如[白通湯](../Page/白通湯.md "wikilink")，或者是[通脈四逆湯加減法中](../Page/通脈四逆湯.md "wikilink")——面色赤、戴陽者，加蔥九莖。

  - [時方](../Page/時方.md "wikilink")[漢醫學](../Page/漢醫.md "wikilink")：蔥白搭配[豆豉可以拿來發汗](../Page/豆豉.md "wikilink")，解除[外感病症](../Page/外感.md "wikilink")。

  - 葱含有[揮發油](../Page/揮發油.md "wikilink")，油中為[大蒜辣素](../Page/大蒜辣素.md "wikilink")。

  -
  - 炒熱外熨臍腹治小便不利,腹脹,腹痛。

## 居家種植

[TaiwanOnions.PNG](https://zh.wikipedia.org/wiki/File:TaiwanOnions.PNG "fig:TaiwanOnions.PNG")
蔥一般種植在土壤裡，亦可以[水耕栽培的方式種植](../Page/水耕栽培.md "wikilink")。若要自行生產，可將市場購買的整株蔥去除蔥葉及部分蔥白供食用後，剩餘的部分蔥白及根部可拿來種植，只要繼續施肥及適度澆灌，即可重新長出蔥葉。如果想要生產出較多的蔥白，可追加覆土。日光無法照射到的部分即成為蔥白。

## 相關條目

  - [薑](../Page/薑.md "wikilink")
  - [大蒜](../Page/大蒜.md "wikilink")
  - [洋蔥](../Page/洋蔥.md "wikilink")
  - [韭](../Page/韭.md "wikilink")
  - [薤](../Page/薤.md "wikilink")
  - [葷菜](../Page/葷菜.md "wikilink")
  - 《[甩葱歌](../Page/甩葱歌.md "wikilink")》

## 图集

[File:Cong-Guizhou.jpg|在](File:Cong-Guizhou.jpg%7C在)[中國大陸](../Page/中國大陸.md "wikilink")[贵州种植的葱](../Page/贵州.md "wikilink")。
[File:地ネギ.JPG|在](File:地ネギ.JPG%7C在)[日本相州种植的葱](../Page/日本.md "wikilink")。
<File:Spring> Onion Singapore.jpg|在新加坡種蔥。 <File:Allium> fistulosum
MHNT.BOT.2011.3.23.jpg|葱的果实和种子。

## 參考資料

  - 《[神農本草經](../Page/神農本草經.md "wikilink")》
  - 《中藥方劑學》
  - 《[傷寒雜病論](../Page/傷寒雜病論.md "wikilink")》
  - 特有生物研究保育中心

## 参考文献

  - 《台灣蔬果實用百科第一輯》，薛聰賢 著，，2001年，ISDN:957-97452-1-8

## 外部連結

  - [蔥,
    Cong](http://libproject.hkbu.edu.hk/was40/detail?lang=ch&channelid=1288&searchword=herb_id=D01047)
    藥用植物圖像數據庫 (香港浸會大學中醫藥學院)

[fistulosum](../Category/蔥屬.md "wikilink")
[Category:調味料](../Category/調味料.md "wikilink")
[Category:食療](../Category/食療.md "wikilink")
[Category:葉菜類](../Category/葉菜類.md "wikilink")
[Category:藥用植物](../Category/藥用植物.md "wikilink")
[fistulosum](../Category/洋葱组.md "wikilink")
[Category:卡尔·林奈命名的生物分类](../Category/卡尔·林奈命名的生物分类.md "wikilink")

1.  徐培嘉。2002。青蔥及紅蔥頭抗氧化及抑菌功能之探討。碩士論文。[中山醫學大學營養科學研究所](../Page/中山醫學大學.md "wikilink")。