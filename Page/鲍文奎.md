**鲍文奎**，[浙江](../Page/浙江.md "wikilink")[宁波人](../Page/宁波.md "wikilink")，[作物遗传育种学家](../Page/作物遗传育种.md "wikilink")，中国[植物多倍体](../Page/植物多倍体.md "wikilink")[遗传育种](../Page/遗传育种.md "wikilink")（如[八倍体小黑麦](../Page/八倍体小黑麦.md "wikilink")）研究的创始人，[中国科学院院士](../Page/中国科学院.md "wikilink")。\[1\]

## 生平

1916年5月8日，生于[浙江省](../Page/浙江省.md "wikilink")[宁波市](../Page/宁波市.md "wikilink")[鄞县](../Page/鄞县.md "wikilink")。

1935年，考入[国立中央大学农学院农艺系](../Page/国立中央大学.md "wikilink")。1939年，毕业，任[四川省农业改进所麦作股技佐](../Page/四川省.md "wikilink")、技士。1942年，参加[李先闻领导的](../Page/李先闻.md "wikilink")[细胞](../Page/细胞.md "wikilink")[遗传研究](../Page/遗传.md "wikilink")。1945年，兼任[四川大学农学院农艺系和](../Page/四川大学.md "wikilink")[华西协和大学理学院农艺系讲师](../Page/华西协和大学.md "wikilink")。

1947年夏，赴[美国](../Page/美国.md "wikilink")[加州理工学院生物系](../Page/加州理工学院.md "wikilink")，进行链孢霉菌的生物化学遗传研究。1950年6月，获加州理工学院[博士学位并回国](../Page/博士.md "wikilink")。

归国后，任[中国农业科学院作物所研究员](../Page/中国农业科学院.md "wikilink")、副所长，[北京农业大学](../Page/北京农业大学.md "wikilink")（现[中国农业大学](../Page/中国农业大学.md "wikilink")）农学系教授，[中国植物学会常务理事](../Page/中国植物学会.md "wikilink")、[中国遗传学会常务理事](../Page/中国遗传学会.md "wikilink")、国际小黑麦协会副主席，第五、六届[全国人大代表](../Page/全国人大.md "wikilink")。

## 著述

  - 《禾谷类作物的同源多倍体和双二倍体》（专著）
  - 《中国的八倍体小黑麦》（论文）
  - 《小麦矮生性的遗传》（论文）
  - 《关于五倍体小麦杂种的遗传评论》（论文）
  - 《普通小麦的减数不配对基因》（论文）

## 注释

## 外部链接

  - [人民网 \>\> 中国两院院士资料库 \>\> 鲍文奎院士
    简介](http://www.people.com.cn/GB/keji/25509/29829/2103633.html)
  - [遗传育种学家鲍文奎](https://web.archive.org/web/20070927212550/http://scholar.ilib.cn/Abstract.aspx?A=jrzj200416022)
  - [鲍文奎与小黑麦](http://www.ndcnc.gov.cn/datalib/2004/Science/DL/DL-167150)

[Category:中国生物学家](../Category/中国生物学家.md "wikilink")
[Category:中国农学家](../Category/中国农学家.md "wikilink")
[農鲍Bao](../Category/國立中央大学校友.md "wikilink")
[農鲍Bao](../Category/中央大学校友.md "wikilink")
[鲍Bao](../Category/南京大学校友.md "wikilink")
[鲍Bao](../Category/南京农业大学校友.md "wikilink")
[Category:鄞县人](../Category/鄞县人.md "wikilink")
[Category:加州理工学院校友](../Category/加州理工学院校友.md "wikilink")
[Category:浙江科学家](../Category/浙江科学家.md "wikilink")
[Category:鲍姓](../Category/鲍姓.md "wikilink")

1.