**左舜生**（），[湖南](../Page/湖南.md "wikilink")[长沙人](../Page/长沙.md "wikilink")，谱名学训，别号仲平\[1\]。字舜生，[以字行](../Page/以字行.md "wikilink")，政治活动家，历史学家。

## 出身

[上海](../Page/上海.md "wikilink")[震旦大学毕业](../Page/震旦大学.md "wikilink")\[2\]（[法文系](../Page/法文.md "wikilink")）。1918年參加少年中國學會\[3\]。1919年7月，与[曾琦](../Page/曾琦.md "wikilink")、[恽代英](../Page/恽代英.md "wikilink")、[毛泽东](../Page/毛泽东.md "wikilink")、[张国焘](../Page/张国焘.md "wikilink")、[李大钊](../Page/李大钊.md "wikilink")、[张闻天](../Page/张闻天.md "wikilink")、[邓中夏](../Page/邓中夏.md "wikilink")、[李璜](../Page/李璜.md "wikilink")、何鲁之、[余家菊](../Page/余家菊.md "wikilink")、[陈启天](../Page/陈启天.md "wikilink")、[黄日葵](../Page/黄日葵.md "wikilink")、[刘仁静](../Page/刘仁静.md "wikilink")、[段锡朋](../Page/段锡朋.md "wikilink")、[罗家伦](../Page/罗家伦.md "wikilink")、[易家钺](../Page/易家钺.md "wikilink")、易嶷之、熊梦飞、[田汉](../Page/田汉.md "wikilink")、[沈泽民](../Page/沈泽民.md "wikilink")、何公敢等发起组织[少年中国学会](../Page/少年中国学会.md "wikilink")，并任《少年中国》月刊主编；后任该会执行部主任。1920年任[中华书局编译所新书部主任](../Page/中华书局.md "wikilink")\[4\]，出版《新文化丛书》等。1923年，与[曾琦](../Page/曾琦.md "wikilink")、[李璜等发起组织](../Page/李璜.md "wikilink")[中国青年党](../Page/中国青年党.md "wikilink")。1924年，任[中国青年党党刊](../Page/中国青年党.md "wikilink")《[醒狮周报](../Page/醒狮周报.md "wikilink")》总经理。1925年加入中國青年黨\[5\]。1935年，任青年党中央执行委员会委员长\[6\]。[抗日战争初期回长沙](../Page/抗日战争.md "wikilink")，参加湖南文化界抗敌后援会，当选为理事。1937年，以青年党代表身份，任[国民参政会参政员](../Page/国民参政会参政员.md "wikilink")\[7\]。1945年7月，曾访问[延安](../Page/延安.md "wikilink")。1946年，在上海创办《中华时报》、《青年生活》。1946年11月，中国青年党与[民盟分裂](../Page/民盟.md "wikilink")。

1947年3月9日，左舜生對記者稱：青年黨仍希望地方政府之改組能早日進行，並亟願在黨員較多之地方參加省、市、縣各級政府\[8\]。同年，任国民政府农林部长\[9\]。1948年12月，蔣介石特任左舜生為農林部長並為行政院政務委員。\[10\]。1949年去香港\[11\]，创办反共刊物《[自由阵线](../Page/自由阵线.md "wikilink")》。先后在香港[新亚书院](../Page/新亚书院.md "wikilink")、香港[清华书院任教](../Page/清华书院.md "wikilink")。1969年去台湾，任[总统府国策顾问](../Page/总统府.md "wikilink")\[12\]。10月病逝于台湾。终年76岁。著有《中国近代史四讲》、《黄兴评传》、《近代中日外交关系小史》、《左舜生选集》等。

## 參考資料

[Category:中華民國總統府國策顧問](../Category/中華民國總統府國策顧問.md "wikilink")
[Category:第1屆中華民國國民大會代表](../Category/第1屆中華民國國民大會代表.md "wikilink")
[Category:第一届国民参政会参政员](../Category/第一届国民参政会参政员.md "wikilink")
[Category:第二届国民参政会参政员](../Category/第二届国民参政会参政员.md "wikilink")
[Category:第三届国民参政会参政员](../Category/第三届国民参政会参政员.md "wikilink")
[Category:第四届国民参政会参政员](../Category/第四届国民参政会参政员.md "wikilink")
[Category:中國青年黨黨員](../Category/中國青年黨黨員.md "wikilink")
[Category:中華民國歷史學家](../Category/中華民國歷史學家.md "wikilink")
[Category:國立政治大學教授](../Category/國立政治大學教授.md "wikilink")
[Category:長沙人](../Category/長沙人.md "wikilink")
[S舜](../Category/左姓.md "wikilink")
[Category:台灣戰後湖南移民](../Category/台灣戰後湖南移民.md "wikilink")

1.

2.
3.
4.
5.
6.
7.
8.

9.
10.
11.
12.