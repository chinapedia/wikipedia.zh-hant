**库尔特·勒温**（**Kurt Zadek
Lewin**，）是一位德裔美国[心理学家](../Page/心理学家.md "wikilink")，他是现代[社会心理学](../Page/社会心理学.md "wikilink")、[组织心理学和](../Page/组织心理学.md "wikilink")[应用心理学的创始人](../Page/应用心理学.md "wikilink")，常被称为“社会心理学之父”，最早研究[群体动力学和组织发展](../Page/群体动力学.md "wikilink")。

## 生平

1890年9月9日，库尔特·勒温出生在德国普鲁士波森省[莫吉尔诺乡村](../Page/莫吉尔诺.md "wikilink")（今属[波兰](../Page/波兰.md "wikilink")）的一个[犹太人家庭](../Page/犹太人.md "wikilink")。1905年，全家迁往首都[柏林](../Page/柏林.md "wikilink")。1909年，他进入[弗萊堡大学学习医学](../Page/弗萊堡大学.md "wikilink")，以后转入[慕尼黑大学学习](../Page/慕尼黑大学.md "wikilink")[生物学](../Page/生物学.md "wikilink")，1910年又转入[柏林大学](../Page/柏林大学.md "wikilink")，在[卡尔·斯图姆夫的指导下](../Page/卡尔·斯图姆夫.md "wikilink")，攻读心理学。当时，后来[格式塔心理学的三位创始人](../Page/格式塔心理学.md "wikilink")[马科斯·韦特墨](../Page/马科斯·韦特墨.md "wikilink")、[沃尔夫冈·苛勒和](../Page/沃尔夫冈·苛勒.md "wikilink")[科特·考夫卡也都是斯图姆夫的学生](../Page/科特·考夫卡.md "wikilink")。勒温原本应该在1914年获得博士学位，但是恰逢[第一次世界大战爆发](../Page/第一次世界大战.md "wikilink")，于是他加入德国陆军作战，官至陆军中尉。由于在战争中负伤，他获得[铁十字勋章](../Page/铁十字勋章.md "wikilink")。然后他回到[柏林大学](../Page/柏林大学.md "wikilink")，终于获得博士学位。

库尔特·勒温起初属于行为主义心理学学派，后来由于与[马科斯·韦特墨](../Page/马科斯·韦特墨.md "wikilink")、[沃尔夫冈·苛勒等共事](../Page/沃尔夫冈·苛勒.md "wikilink")，转而接受了[格式塔心理学](../Page/格式塔心理学.md "wikilink")。勒温与早期的[法兰克福学派有密切的联系](../Page/法兰克福学派.md "wikilink")，[法兰克福学派起源于一个主要由犹太人](../Page/法兰克福学派.md "wikilink")[马克思主义者构成的有影响的团体](../Page/马克思主义.md "wikilink")
。1933年，[阿道夫·希特勒在德国掌权](../Page/阿道夫·希特勒.md "wikilink")，学会解散，成员经英国前往美国 。

1933年8月，纳粹在德国掌权以后，开始迫害[犹太人](../Page/犹太人.md "wikilink")，于是库尔特·勒温移居[美国](../Page/美国.md "wikilink")，在1940年成为美国公民。勒温任教于[康奈尔大学以及爱荷华大学儿童福利研究工作站](../Page/康奈尔大学.md "wikilink")。1944年，他又在麻省理工学院建立了群体动力学研究中心。1946年，

勒温任教于[杜克大学一段时间](../Page/杜克大学.md "wikilink")。\[1\].

勒温铸造了genidentity的概念\[2\]

1947年2月12日，他在美国马萨诸塞州的牛顿维尔因心脏病发作去世，运回家乡埋葬。

## 场论

## 参考文献

<div class="references-small">

<references />

</div>

## 参考书目

  - Marrow, Alfred J. *The Practical Theorist: The Life and Work of Kurt
    Lewin* (1969, 1984) ISBN 0-934698-22-8 (Marrow studied as one of
    Lewin's students)
  - White, Ralph K., and Ronald O. Lippitt, *Autocracy and Democracy*
    (1960, 1972) ISBN 0-8371-5710-2 (White and Lippitt carried out the
    research described here under Lewin as their thesis-advisor;
    Marrow's book also briefly describes the same work in chapter 12.)
  - Weisbord, Marvin R., *Productive Workplaces Revisited* (2004) ISBN
    0-7879-7117-0 (Chapters 4: Lewin: the Practical Theorist, Chapter 5:
    The Learning Organization: Lewin's Legacy to Management.)

## 参见

  - [社会心理学](../Page/社会心理学.md "wikilink")
  - [群体动力学](../Page/群体动力学.md "wikilink")

## 外部链接

  - [The Kurt Lewin Center for Psychological
    Research](https://web.archive.org/web/20070927095505/http://www.lewincenter.ukw.edu.pl/index.php)
    at Kazimierz Wielki University
  - [International Conference on "Kurt Lewin: Contribution to
    Contemporary
    Psychology"](https://web.archive.org/web/20060512085723/http://www.lewin2004.ab.edu.pl/)
  - [Kurt Lewin](http://www.worldofbiography.com/9059-Kurt%20Lewin/) at
    World of Biography
  - [Kurt Lewin: groups, experiential learning and action
    research](http://www.infed.org/thinkers/et-lewin.htm)
  - [Kurt
    Lewin](https://web.archive.org/web/20070820114559/http://fates.cns.muskingum.edu/~psych/psycweb/history/lewin.htm)
    (biography and overview of theory)
  - [Kurt Lewin
    Foundation](https://web.archive.org/web/20070825232816/http://www.kurtlewin.hu/index_en.htm)
    (an NGO - promoting tolerance)
  - [Kurt Lewin Institute](http://www.kurtlewininstitute.nl/) (A
    graduate student training institute in Social Psychology in the
    Netherlands)

[Category:美国心理学家](../Category/美国心理学家.md "wikilink")
[Category:德国心理学家](../Category/德国心理学家.md "wikilink")
[Category:社会心理学](../Category/社会心理学.md "wikilink")
[Category:杜克大學教師](../Category/杜克大學教師.md "wikilink")
[Category:康乃爾大學教師](../Category/康乃爾大學教師.md "wikilink")
[Category:柏林洪堡大學校友](../Category/柏林洪堡大學校友.md "wikilink")
[Category:慕尼黑大學校友](../Category/慕尼黑大學校友.md "wikilink")
[Category:弗萊堡大學校友](../Category/弗萊堡大學校友.md "wikilink")
[Category:美國犹太人](../Category/美國犹太人.md "wikilink")
[Category:德國裔美國人](../Category/德國裔美國人.md "wikilink")
[Category:德國猶太人](../Category/德國猶太人.md "wikilink")
[Category:波森人](../Category/波森人.md "wikilink")

1.
2.  Lewin, K. (1922). *Der Begriff der Genese in Physik, Biologie und
    Entwicklungsgeschichte.* (Lewin's Habilitationsschrift)