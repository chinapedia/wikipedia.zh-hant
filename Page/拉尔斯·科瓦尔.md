**拉尔斯·科瓦尔**（[挪威语](../Page/挪威语.md "wikilink")：****
，），[挪威](../Page/挪威.md "wikilink")[基督教民主党政治家](../Page/挪威基督教民主党.md "wikilink")。从1972年到1973年他担任[挪威首相](../Page/挪威首相.md "wikilink")。

## 早期的生活和事业

拉尔斯·科瓦尔出生在[布斯克吕郡](../Page/布斯克吕.md "wikilink")自治市一个传统[基督徒家庭](../Page/基督徒.md "wikilink")。他1943年毕业于[挪威生命科学大学](../Page/挪威生命科学大学.md "wikilink")。他在任职，1952年他起来成为教务长。

## 议会生涯

1961年他第一次代表[东福尔郡选进](../Page/东福尔.md "wikilink")[挪威议会](../Page/挪威议会.md "wikilink")，1965年他被任命为议会领袖，1967年成为党团领袖。科瓦尔总共担当了五届任期议员。1981年他从政党政治退休，成为东福尔郡州长。

## 首相

科瓦尔的[内阁从](../Page/内阁.md "wikilink")1972年10月18日到1973年10月16日領導挪威自[二次大戰結束以來第一個中間路線政府](../Page/二次大戰.md "wikilink")。虽然短命，但它担当了一个在挪威政治上的重要里程碑，因为它标记了挪威在[欧共体会员资格苦涩和分裂的辩论](../Page/欧共体.md "wikilink")。他是[挪威基督教民主党的第一个首相](../Page/挪威基督教民主党.md "wikilink")，在一个非常困难和过渡政治的情况中，科瓦尔被证明是一位有效率的首相。他的内阁与欧共体交涉的一个商业条约和设立了挪威的第一项[石油政策](../Page/石油.md "wikilink")。

## 外在链接

  - [AP](../Page/美联社.md "wikilink"): ["Former Norway prime minister
    Korvald
    dies"](http://seattlepi.nwsource.com/national/1103AP_Obit_Korvald.html)
    (in the *Seattle Post-Intelligencer*, July 4 2006)

[Category:挪威政治人物](../Category/挪威政治人物.md "wikilink")
[Category:挪威首相](../Category/挪威首相.md "wikilink")
[Category:挪威生命科學大學校友](../Category/挪威生命科學大學校友.md "wikilink")