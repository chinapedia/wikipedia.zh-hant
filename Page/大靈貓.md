**大灵猫**（学名：*Viverra
zibetha*），又名**五間狸**、**九節狸**，是[食肉目](../Page/食肉目.md "wikilink")[灵猫科的一種动物](../Page/灵猫科.md "wikilink")。

## 特征

大灵猫的身体要比家猫大，长约65—85厘米，尾长约40厘米；毛色为灰黄带褐，背部有黑纹和斑点，颈部有黑白相间的波状纹，尾部有黑白色环纹；雌雄兽的阴部附近均有囊状腺，分泌油质液体，称为“灵猫香”。

## 分佈

分布于[克什米尔地区](../Page/克什米尔.md "wikilink")、[尼泊尔](../Page/尼泊尔.md "wikilink")、[中南半岛](../Page/中南半岛.md "wikilink")、[不丹](../Page/不丹.md "wikilink")、[孟加拉](../Page/孟加拉.md "wikilink")、[印度](../Page/印度.md "wikilink")（东北部）以及[中国大陆的](../Page/中国大陆.md "wikilink")[江西](../Page/江西.md "wikilink")、[广西](../Page/广西.md "wikilink")、[陕西](../Page/陕西.md "wikilink")、[贵州](../Page/贵州.md "wikilink")、[安徽](../Page/安徽.md "wikilink")、[云南](../Page/云南.md "wikilink")、[湖南](../Page/湖南.md "wikilink")、[海南](../Page/海南.md "wikilink")、[四川](../Page/四川.md "wikilink")、[江苏](../Page/江苏.md "wikilink")、[广东](../Page/广东.md "wikilink")、[福建](../Page/福建.md "wikilink")、[浙江](../Page/浙江.md "wikilink")、[湖北等地](../Page/湖北.md "wikilink")，一般生活于[热带和](../Page/热带.md "wikilink")[亚热带的林缘种类](../Page/亚热带.md "wikilink")、主要栖息在热带季雨林、亚热带\]常绿阔叶林的林缘灌丛以及草丛。该物种的模式产地在孟加拉。\[1\]以前大靈貓是很常見的動物，現在已越來越少。

## 習性

大靈貓生性孤獨，[夜行性](../Page/夜行性.md "wikilink")，喜歡於樹樁、[岩石上摩擦](../Page/岩石.md "wikilink")[陰部](../Page/陰部.md "wikilink")。食物包括[老鼠](../Page/老鼠.md "wikilink")、[青蛙](../Page/青蛙.md "wikilink")、[雀鳥](../Page/雀鳥.md "wikilink")、[蝸牛](../Page/蝸牛.md "wikilink")、[昆蟲](../Page/昆蟲.md "wikilink")、[果實等](../Page/果實.md "wikilink")。部份未能消化的食物會遺留在[糞便之內](../Page/糞便.md "wikilink")。

## 繁殖

大靈貓於[春季發情](../Page/春季.md "wikilink")，[夏季生育](../Page/夏季.md "wikilink")，每胎3至4隻。

## 与人类的关系

大靈貓的經濟價值很高，[毛皮可製成](../Page/毛皮.md "wikilink")[裘](../Page/裘.md "wikilink")，而分泌的[靈貓香是](../Page/靈貓香.md "wikilink")[香料工業重要原料](../Page/香料.md "wikilink")，也因此大灵猫遭到猎杀。在生態上，大靈貓對抑制[鼠害](../Page/鼠害.md "wikilink")、[蟲害也有重要作用](../Page/蟲害.md "wikilink")，也是重要的[種子傳播者](../Page/種子.md "wikilink")，在[中華人民共和國屬於](../Page/中華人民共和國.md "wikilink")[國家二級受保護動物](../Page/中國國家重點保護野生動物名錄.md "wikilink")。在[中国濒危动物红皮书中记录为渐危](../Page/中国濒危动物红皮书.md "wikilink")。

## 亚种

  - **大灵猫华东亚种**（[学名](../Page/学名.md "wikilink")：），Swinhoe于1864年命名。在[中国大陆](../Page/中国大陆.md "wikilink")，分布于[江西](../Page/江西.md "wikilink")、[陕西](../Page/陕西.md "wikilink")、[贵州](../Page/贵州.md "wikilink")、[安徽](../Page/安徽.md "wikilink")、[湖南](../Page/湖南.md "wikilink")、[江苏](../Page/江苏.md "wikilink")（南部）、[四川](../Page/四川.md "wikilink")、[西藏](../Page/西藏.md "wikilink")、[广西](../Page/广西.md "wikilink")（东部）、[广东](../Page/广东.md "wikilink")、[福建](../Page/福建.md "wikilink")、[浙江](../Page/浙江.md "wikilink")、[湖北](../Page/湖北.md "wikilink")、[云南](../Page/云南.md "wikilink")（东北部）等地。该物种的模式产地在福建[闽江的水口](../Page/闽江.md "wikilink")。\[2\]
  - **大灵猫海南亚种**（[学名](../Page/学名.md "wikilink")：），Wang et
    Xu于1983年命名。在[中国大陆](../Page/中国大陆.md "wikilink")，分布于[海南等地](../Page/海南.md "wikilink")。该物种的模式产地在海南[吊罗山](../Page/吊罗山.md "wikilink")。\[3\]
  - **大灵猫印缅亚种**（[学名](../Page/学名.md "wikilink")：），Wroughton于1915年命名。分布于印度（阿萨姆）、缅甸（北部）以及[中国大陆的](../Page/中国大陆.md "wikilink")[云南](../Page/云南.md "wikilink")（西北部）、[西藏等地](../Page/西藏.md "wikilink")。该物种的模式产地在缅甸北部[亲敦江](../Page/亲敦江.md "wikilink")。\[4\]
  - **大灵猫印度支那亚种**（[学名](../Page/学名.md "wikilink")：），Thomas于1927年命名。分布于缅甸（东南部）、越南、老挝、泰国（北部）以及[中国大陆的](../Page/中国大陆.md "wikilink")、贵州（西南部）、云南（南部）、广西（西南部）等地。该物种的模式产地在老挝[杭康XiangKhunang](../Page/杭康.md "wikilink")。\[5\]

## 参考文献

[Category:靈貓屬](../Category/靈貓屬.md "wikilink")
[Category:中國哺乳動物](../Category/中國哺乳動物.md "wikilink")
[Category:香港動物](../Category/香港動物.md "wikilink")
[Category:香港原生物種](../Category/香港原生物種.md "wikilink")
[Category:印度哺乳動物](../Category/印度哺乳動物.md "wikilink")
[Category:尼泊爾動物](../Category/尼泊爾動物.md "wikilink")
[Category:不丹動物](../Category/不丹動物.md "wikilink")
[Category:孟加拉動物](../Category/孟加拉動物.md "wikilink")
[Category:越南動物](../Category/越南動物.md "wikilink")
[Category:柬埔寨動物](../Category/柬埔寨動物.md "wikilink")
[Category:老撾動物](../Category/老撾動物.md "wikilink")

1.
2.

3.

4.

5.