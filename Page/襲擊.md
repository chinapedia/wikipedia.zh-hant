**襲擊**（raiding；depredation），**突襲**，是一種[攻擊行動或軍事手段](../Page/攻擊.md "wikilink")，常常是趁目標不注意或無特別防範之處打擊它。單字**襲**有時也是同樣的意思。

## 軍事

**襲擊**是歷史久遠的軍事[戰術](../Page/戰術.md "wikilink")，成功的襲擊是[攻其不備](../Page/攻其不備.md "wikilink")，是故，偷襲也是一種很常見的說法。

### 史例

87年時的例子，東漢護羌校尉[傅育想要討伐叛](../Page/傅育.md "wikilink")[羌中以](../Page/羌.md "wikilink")[迷吾為首的](../Page/迷吾.md "wikilink")[燒當羌](../Page/燒當羌.md "wikilink")，組織了河西、陝西等幾個郡準備合攻。但傅育沒有等到聯軍會師就獨自進軍，夜晚追敵時便被迷吾**襲擊**成功。「……<span style="text-decoration: underline;">育</span>請發諸郡兵數萬人共擊羌。未及會，三月，<span style="text-decoration: underline;">育</span>獨進軍。<span style="text-decoration: underline;">迷吾</span>聞之，徙廬落\[1\]去。<span style="text-decoration: underline;">育</span>遣精騎三千窮追之，夜，至三兜谷，不設備，<span style="text-decoration: underline;">迷吾</span>襲擊，大破之，殺<span style="text-decoration: underline;">育</span>及吏士八百八十人。\[2\]……」

### 目標

襲擊有各種不同的目標，這些目標是有價值且值得犧牲的：

  - 殺死主要[軍事將領](../Page/軍事將領.md "wikilink")。
  - [搶劫](../Page/搶劫.md "wikilink")、掠奪物資，例如糧草或彈藥。
  - 破壞物資，當條件無法掠奪為己用時便破壞以令敵人無法使用。
  - 打擊敵人[士氣](../Page/士氣.md "wikilink")，使敵人未來的作戰失去衝勁，例如偷襲敵人本營。
  - [綁架或](../Page/綁架.md "wikilink")[殺死要員](../Page/謀殺.md "wikilink")，讓敵人失去[護衛的目標](../Page/護衛.md "wikilink")。
  - 造成威脅，可令人認為不安全而避開。
  - 侵擾道路，使人不敢使用該路線，進而能製造[包圍機會](../Page/包圍.md "wikilink")。
  - 援救[人質](../Page/人質.md "wikilink")。
  - 探敵[虛實](../Page/虛實.md "wikilink")，獲得[情報](../Page/軍情.md "wikilink")。

### 進行

通常進行襲擊的都是小規模部隊，與大規模部隊相比，移動速度較快，也容易隱藏行蹤。如古代[騎兵要避免產生煙塵被敵人發現](../Page/騎兵.md "wikilink")，會減少規模，而且容易藏身在林中。為了能攻其不備，選擇[夜間或是晨昏時展開行動是相當普遍的](../Page/夜間.md "wikilink")。

一再的確認敵人無防備之後，便可展開襲擊行動。然而，達到目標便可立刻收兵。由於戰況一直在變化，小規模部隊也利於立即指揮。

### 裝備與技術

這些技術或新科技有利於進行一場襲擊。

  - [無線電波對講機](../Page/無線電波對講機.md "wikilink")
  - [夜視鏡](../Page/夜視鏡.md "wikilink")

## 参考

## 参见

  - [空袭](../Page/空袭.md "wikilink")，空中發動的襲擊，或是轟炸。
  - [突击队](../Page/突击队.md "wikilink")
  - [游擊隊](../Page/游擊隊.md "wikilink")

[分类:军事](../Page/分类:军事.md "wikilink")

1.  廬是塞外民族居住的棚，廬落就是廬組成可以住人的地方。通鑑蝴注云「廬．穹廬。落．居也。」
2.  肅宗孝章皇帝下章和元年（丁亥，公元八七年）資治通鑑/卷047