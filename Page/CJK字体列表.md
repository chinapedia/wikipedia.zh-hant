**CJK字体列表**，是[中日韓統一表意文字在電腦的所有字體列表](../Page/中日韓統一表意文字.md "wikilink")。

<span style="background-color:#90FF90;">\[F\]</span>
代表[自由及開放原始碼軟體](../Page/自由及開放原始碼軟體.md "wikilink")
(FOSS)；<span style="background-color:#FFEF00;"><s>\[F\]</s></span>
代表曾經獲普遍認為是自由及開源軟件，但現在有爭議。

## 傳統印刷體

| 字体名称                                | 样例（简）                                       | 样例（繁）                                                                                                                                                    |
| ----------------------------------- | ------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [宋體 / 明體](../Page/宋体.md "wikilink") | [80px](../Page/图像:songti.png.md "wikilink") | [80px](../Page/图像:Songti-zh-hant.png.md "wikilink") [Mingti-zh-hant.png](https://zh.wikipedia.org/wiki/File:Mingti-zh-hant.png "fig:Mingti-zh-hant.png") |

### 泛Unicode

  - [Bitstream Cyberbit](../Page/Bitstream_Cyberbit.md "wikilink")

### 中文

  - [新細明體／細明體](../Page/新細明體.md "wikilink") – 隨 Microsoft Windows 附贈
  - [中易宋体](../Page/中易宋体.md "wikilink")
  - [儷宋 Pro](../Page/儷宋_Pro.md "wikilink") – 隨 Mac OS 9 或 X 10.3–10.4 附贈
  - [蘋果儷細宋](../Page/蘋果儷細宋.md "wikilink") – 隨 Mac OS 9 或 X 10.2–10.4 附贈
  - [STSong](../Page/STSong.md "wikilink") – 隨 Mac OS X 10.2–10.4 附贈
  - <span style="background-color:#90FF90;">\[F\]</span>
    **文鼎PL細上海宋**及**文鼎PL簡報宋** – 文鼎公共授權（Arphic Public
    License）自由字体，由[文鼎科技釋出](../Page/文鼎科技.md "wikilink")，現時大部份 Linux
    分發版本都有
  - <span style="background-color:#90FF90;">\[F\]</span> **文鼎PL新宋**（AR
    PL New Sung）–
    文鼎公共授權自由字型，由開源社群將文鼎科技釋出之兩套自由字型合併，再加上[點陣字而成](../Page/點陣字.md "wikilink")\[1\]
  - <span style="background-color:#90FF90;">\[F\]</span> **[AR PL
    UMing](../Page/UMing.md "wikilink")** –
    文鼎公共授權自由字型，由「文鼎PL新宋」[復刻而來](../Page/复刻_\(软件工程\).md "wikilink")，現時大部份
    Linux 分發版本都有
  - 「-{文鼎ＰＬ明體U20-L}-」及「-{文鼎ＰＬ報宋2GBK}-」，文鼎公共授權自由字型，但修改了授權條款，只能用於非商業用途。
  - <span style="background-color:#90FF90;">\[F\]</span> **[BabelStone
    Han](http://www.babelstone.co.uk/Fonts/Han.html)** –
    文鼎公共授權自由字型，基於「-{文鼎PL細上海宋}-」及「-{文鼎PL簡報宋}-」（即舊授權字型）擴展而來。
  - <span style="background-color:#90FF90;">\[F\]</span>
    [文泉驿点阵宋体](../Page/文泉驿.md "wikilink") – GPL授权
  - H-秀月（H-SiuNiu）
  - <span style="background-color:#90FF90;">\[F\]</span>
    **[I.明體](http://founder.acgvlyric.org/iu/doku.php/%E9%80%A0%E5%AD%97:%E9%96%8B%E6%BA%90%E5%AD%97%E5%9E%8B_i.%E6%98%8E%E9%AB%94)**
    – 基于 IPAex Mincho 字体开发，使用IPA字体授权。

### 日文

  - [MS 明朝](../Page/MS_明朝.md "wikilink")（**MS Mincho**）及MS P明朝（**MS
    PMincho**） – Windows 95 及以後提供的日文版字体
  - <span style="background-color:#90FF90;">\[F\]</span>
    **TakaoMincho**、**TakaoPMincho** 及 **TakaoExMincho**（在 Ubuntu
    安裝「日文支援」會安裝此等字型）
  - <span style="background-color:#90FF90;">\[F\]</span> **[IPAex
    Mincho](http://ossipedia.ipa.go.jp/ipafont/index.html#en)**、**[IPAmj
    Mincho](http://mojikiban.ipa.go.jp/1300.html)** –
    独立行政法人情报处理推进机构开发，使用[IPA字体授权](http://ipafont.ipa.go.jp/ipa_font_license_v1.html#LicenceEng)（，IPA
    font licence）。

### 韓文

  - Batang（）、BatangChe（）、Gungsuh（）、GungsuhChe（）、HYMyeongJo（）
  - <span style="background-color:#90FF90;">\[F\]</span>
    UnBatang（）、UnGungsuh（）– 大部份 Linux 發行版都有。
  - Seoul Hangang（）- [首尔市政府的官方字体](../Page/首尔市政府.md "wikilink")。
  - <span style="background-color:#90FF90;">\[F\]</span> [Nanum
    Myeongjo](../Page/共享體.md "wikilink")（）–
    Nanum系列字体之一，由[Naver公司发布](../Page/Naver.md "wikilink")，[OFL授权](../Page/OFL.md "wikilink")。

### 越南文

  - 喃那宋 Light，仿照《》的字体风格设计。\[2\]

  - <span style="background-color:#90FF90;">\[F\]</span> [Han Nom Font
    Set](http://vietunicode.sourceforge.net/fonts/fonts_hannom.html)，覆盖CJK扩展A区及B区，GPL授权。

## 黑體

### 泛Unicode

  - [Arial Unicode MS](../Page/Arial_Unicode_MS.md "wikilink") – 附贈於
    Microsoft Office 2000 及更高版本
  - <span style="background-color:#90FF90;">\[F\]</span> [Droid Sans
    Fallback](../Page/Droid.md "wikilink") – 由  為 Google 的-{zh-hans:移动;
    zh-tw:行動; zh-hk:流動;}--{zh-hans:操作系统;zh-hk:作業系統;zh-tw:作業系統;}-
    [Android](../Page/Android.md "wikilink") 製作，以 Apache License 2 授權
  - <span style="background-color:#90FF90;">\[F\]</span>
    [思源黑体](../Page/思源黑体.md "wikilink") -
    由[Adobe与](../Page/Adobe.md "wikilink")[Google所领导开发的开源字体家族](../Page/Google.md "wikilink")，支持[繁体中文](../Page/繁体中文.md "wikilink")、[简体中文](../Page/简体中文.md "wikilink")、[日文及](../Page/日文.md "wikilink")[韩文](../Page/韩文.md "wikilink")，并且各有7种字体粗细

### 中文

  - [微軟正黑體](../Page/微軟正黑體.md "wikilink")
  - [微软雅黑](../Page/微软雅黑.md "wikilink")
  - [中易黑体](../Page/中易黑体.md "wikilink")
  - [华文细黑](../Page/华文细黑.md "wikilink") – 附贈於 Microsoft Office 2000 或以上
  - 苹方-简、蘋方-繁、蘋方-港 – 附贈於 OS X 10.11
  - [黑体-简](../Page/黑体-简.md "wikilink")、[黑體-繁](../Page/黑體-繁.md "wikilink")
    – 附贈於 Mac OS X 10.6
  - Hiragino Sans GB – 附贈於 Mac OS X 10.6
  - [华文黑体](../Page/华文黑体.md "wikilink")（STHeiti）– 附贈於 Mac OS X 10.2 或以上
  - [儷黑 Pro](../Page/儷黑_Pro.md "wikilink") – 附贈於 Mac OS X 10.3 或以上
  - [蘋果儷中黑](../Page/蘋果儷中黑.md "wikilink") – 附贈於 Mac OS 9 及 Mac OS X 10.2
    或以上
  - <span style="background-color:#90FF90;">\[F\]</span>
    [文泉驿正黑](../Page/文泉驿正黑.md "wikilink")
  - <span style="background-color:#90FF90;">\[F\]</span>
    [文泉驿微米黑](../Page/文泉驿微米黑.md "wikilink")
  - H-明蘭（H-Minglan）
  - H-雲林（H-YunLin）
  - H-儷黑（H-LiHei）
  - 信黑體

### 日文

  - [明-{瞭}-體](../Page/明瞭體.md "wikilink")（Meiryo）
  - MS Gothic（）– 日文版[Windows
    3.1及以后版本附赠的默认系统字体](../Page/Windows_3.1.md "wikilink")，所有语言的[Windows
    2000至](../Page/Windows_2000.md "wikilink")[Windows
    8.1](../Page/Windows_8.1.md "wikilink")、[Windows
    10日文版](../Page/Windows_10.md "wikilink")、Microsoft Office
    v.X至2004、Internet Explorer 3的日文字体包、Microsoft Global IME
    5.02（日文版）、Office XP Tool: Japanese Language Pack、Windows
    10的日文附加字体。
  - MS PGothic（）– 随日文版[Windows
    95及以后版本](../Page/Windows_95.md "wikilink")、所有语言的[Windows
    XP](../Page/Windows_XP.md "wikilink")、Microsoft Office 2004附赠。
  - MS UI Gothic – Windows 98至Windows XP的默认界面字体，随日文版[Windows
    98及以后版本](../Page/Windows_98.md "wikilink")、所有语言的[Windows
    XP附赠](../Page/Windows_XP.md "wikilink")。
  - <span style="background-color:#90FF90;">\[F\]</span> Takao
    Gothic、Takao PGothic 及 Takao ExGothic（在 Ubuntu 安裝「日文支援」會安裝此等字型）
  - <span style="background-color:#90FF90;">\[F\]</span> **[IPAex
    Gothic](http://ossipedia.ipa.go.jp/ipafont/index.html#en)** –
    独立行政法人情报处理推进机构开发，使用IPA字体授权。
  - <span style="background-color:#90FF90;">\[F\]</span> [Mona
    Font](../Page/Mona_Font.md "wikilink") – 附赠于多个Linux发行版的自由字体，公有领域。
  - <span style="background-color:#90FF90;">\[F\]</span>? VL Gothic（）–
    起于[Vine Linux的字体](../Page/Vine_Linux.md "wikilink")。包含由M+
    FONTS及Sazanami Gothic字体衍生而来的字形，由此使用两种许可条款。
  - Osaka – 旧板[Mac OS的默认系统字体](../Page/Mac_OS.md "wikilink")。
  - [Hiragino](../Page/Hiragino.md "wikilink") Kaku Gothic（）、Hiragino
    Maru Gothic（）– [Mac OS X日文版的默认字体](../Page/Mac_OS_X.md "wikilink")。
  - [Kozuka Gothic](../Page/Kozuka_Gothic.md "wikilink")（）– 由[Adobe
    Illustrator新版提供](../Page/Adobe_Illustrator.md "wikilink")。
  - GothicBBB-Medium –
    由[Adobe用于其文档中的示例](../Page/Adobe_Systems.md "wikilink")。
  - <span style="background-color:#90FF90;">\[F\]</span> Kochi Gothic（）–
    原名渡边字体（，Watanabe），曾经被认为是自由字体并附赠于多个Linux发行版。后来因为被发现其前身渡边字体是从TypeBank
    and Design Laboratory, Hitachi, Ltd.开发的TypeBank
    Mincho-M字体复制而来而转向基于和田研字体开发。\[3\]\[4\]\[5\]
  - <span style="background-color:#FFEF00;"><s>\[F\]</s></span> Sazanami
    Gothic（）\[6\] – 同样曾经被认为是自由字体并附赠于多个Linux发行版。\[7\]

### 韓文

  - （Malgun Gothic）

  - （HYGothic）

  - <span style="background-color:#90FF90;">\[F\]</span> UnDotum（）

  - AppleGothic（）– 苹果 [Mac OS](../Page/Mac_OS.md "wikilink") 9至[Mac OS
    X](../Page/Mac_OS_X.md "wikilink")
    10.7及[iOS](../Page/iOS.md "wikilink") 1至5.0的默认韩文字体。自OS
    X起完全支持Unicode。

  - Seoul Namsan（）- [首尔市政府的官方字体](../Page/首尔市政府.md "wikilink")。

  - <span style="background-color:#90FF90;">\[F\]</span> [Nanum
    Gothic](../Page/共享體.md "wikilink")（）–
    Nanum系列字体之一，由[Naver公司发布](../Page/Naver.md "wikilink")，[OFL授权](../Page/OFL.md "wikilink")。

  - <span style="background-color:#90FF90;">\[F\]</span> [Hamchorom
    Dotum](http://www.hancom.co.kr/downLoad.downView.do?targetRow=1&seqno=3136&mcd_save=005)（）-
    [Hancom公司开发](../Page/Hancom.md "wikilink")。

## 楷書

| 字体名称                           | 样例（简）                                      | 样例（繁）                                               |
| ------------------------------ | ------------------------------------------ | --------------------------------------------------- |
| [楷體](../Page/楷書.md "wikilink") | [70px](../Page/图像:kaiti.svg.md "wikilink") | [100px](../Page/图像:MSKAI-zh-hant.svg.md "wikilink") |

### 中文

  - [標楷體](../Page/標楷體.md "wikilink")
  - [中易楷体](../Page/中易楷体.md "wikilink") 或 楷体_GB2312
  - [华文楷体](../Page/华文楷体.md "wikilink") (STKaiti)
  - Kai – 附贈於 [Mac OS](../Page/Mac_OS.md "wikilink") 簡體中文版
  - BiauKai – 附贈於 Mac OS 繁體中文版
  - <span style="background-color:#90FF90;">\[F\]</span> **文鼎PL中楷** 及
    **文鼎PL簡中楷** – 文鼎公共授權自由字体，由文鼎科技釋出，現時大部份 Linux 分發版本都有
  - <span style="background-color:#90FF90;">\[F\]</span> **文鼎PL新中楷** 及
    **文鼎PL新中楷 Mono** – 文鼎公共授權自由字体，由開源社群將文鼎科技釋出之兩套自由楷書字體合併而成\[8\]
  - <span style="background-color:#90FF90;">\[F\]</span> **[AR PL
    UKai](../Page/CJK_Unifonts.md "wikilink")** –
    文鼎公共授權自由字体，由開源社群將文鼎科技釋出之兩套自由楷書字體合併而成，現時大部份
    Linux 分發版本都有
  - <span style="background-color:#90FF90;">\[F\]</span>
    **[I.顏體](http://founder.acgvlyric.org/iu/doku.php/%E9%80%A0%E5%AD%97:%E9%96%8B%E6%BA%90%E5%AD%97%E5%9E%8B_i.%E9%A1%8F%E9%AB%94)**
    – 基于王汉宗的一款颜体字体开发，GPL授权。
  - <span style="background-color:#90FF90;">\[F\]</span>
    **[自由香港楷書](https://github.com/freehkfonts/freehkkai)**（Free
    HK Kai）–
    自由香港字-{}-型专案依據香港教育局《常用字字形表》2007年重排本中的小學生常用字及修改台灣全字庫楷體而成，以[共享創意署名4.0國際授權條款釋出](../Page/創作共用授權條款.md "wikilink")。

### 日文

  - （HGSeikaishotai）

### 韓文

  - （，HYhaeseo）

  - （UnGungseo）

## 其他各種手書體

| 字体名称                               | 样例（简）                                               | 样例（繁）                                                |
| ---------------------------------- | --------------------------------------------------- | ---------------------------------------------------- |
| [篆体](../Page/篆书.md "wikilink")     |                                                     | [70px](../Page/图像:zhuanti.png.md "wikilink")         |
| [隶体](../Page/隶书.md "wikilink")     | [70px](../Page/图像:liti.png.md "wikilink")           | [70px](../Page/图像:litif.png.md "wikilink")           |
| [柏青体](../Page/柏青体.md "wikilink")   | [100px](../Page/文件:baiqingti.png.md "wikilink")     | [100px](../Page/文件:baiqingtif.png.md "wikilink")     |
| [平和体](../Page/平和体.md "wikilink")   | [100px](../Page/文件:pingheti.svg.md "wikilink")      | [100px](../Page/文件:pinghetif.svg.md "wikilink")      |
| [汉简体](../Page/汉简体.md "wikilink")   | [100px](../Page/图像:hanjianti.svg.md "wikilink")     |                                                      |
| [魏碑体](../Page/魏碑.md "wikilink")    | [100px](../Page/图像:weibeiti.svg.md "wikilink")      | [100px](../Page/图像:weibeitif.svg.md "wikilink")      |
| [佛经体](../Page/佛经体.md "wikilink")   |                                                     | [100px](../Page/图像:fojingti.svg.md "wikilink")       |
| [欧体](../Page/欧体.md "wikilink")     | [70px](../Page/图像:outi.svg.md "wikilink")           | [70px](../Page/图像:outif.svg.md "wikilink")           |
| [颜体](../Page/颜体.md "wikilink")     | [70px](../Page/图像:yanti.svg.md "wikilink")          | [70px](../Page/图像:yantif.svg.md "wikilink")          |
| [柳体](../Page/柳体.md "wikilink")     | [70px](../Page/图像:liuti.svg.md "wikilink")          | [70px](../Page/图像:liutif.svg.md "wikilink")          |
| [瘦金体](../Page/瘦金体.md "wikilink")   | [100px](../Page/图像:shoujintij.svg.md "wikilink")    | [100px](../Page/图像:shoujintif.svg.md "wikilink")     |
| [南宫体](../Page/南宫体.md "wikilink")   | [100px](../Page/文件:nangongti.svg.md "wikilink")     |                                                      |
| [硬笔楷书](../Page/硬笔楷书.md "wikilink") | [100px](../Page/文件:yingbikaishu.svg.md "wikilink")  | [100px](../Page/文件:yingbikaishuf.svg.md "wikilink")  |
| [草體](../Page/草書.md "wikilink")     |                                                     |                                                      |
| [黄草体](../Page/黄草体.md "wikilink")   | [100px](../Page/图像:huangcaoti.svg.md "wikilink")    |                                                      |
| [大草体](../Page/大草体.md "wikilink")   |                                                     | [100px](../Page/图像:dacaoti.svg.md "wikilink")        |
| [行楷体](../Page/行楷.md "wikilink")    |                                                     | [100px](../Page/图像:xingkaitif.svg.md "wikilink")     |
| [雪君体](../Page/雪君体.md "wikilink")   | [100px](../Page/文件:xuejunti.svg.md "wikilink")      | [100px](../Page/文件:xuejuntif.svg.md "wikilink")      |
| [启体](../Page/启体.md "wikilink")     | [70px](../Page/图像:qiti.svg.md "wikilink")           | [70px](../Page/图像:qitif.svg.md "wikilink")           |
| [行體](../Page/行書.md "wikilink")     |                                                     |                                                      |
| [舒体](../Page/舒体.md "wikilink")     | [70px](../Page/文件:Shuti.svg.md "wikilink")          | [70px](../Page/文件:shutif.svg.md "wikilink")          |
| [硬笔行书](../Page/硬笔行书.md "wikilink") | [100px](../Page/文件:yingbixingshu.svg.md "wikilink") | [100px](../Page/文件:yingbixingshuf.svg.md "wikilink") |
| [破體](../Page/破體.md "wikilink")     |                                                     |                                                      |
| [康体](../Page/康体.md "wikilink")     | [70px](../Page/文件:kangti.svg.md "wikilink")         | [70px](../Page/文件:kangtif.svg.md "wikilink")         |

### 手書系列

  - [手書風](../Page/手書風.md "wikilink")
  - [字跡](../Page/方正字跡.md "wikilink")

## 現代字體

| 字体名称                               | 样例（简）                                                | 样例（繁）                                                |
| ---------------------------------- | ---------------------------------------------------- | ---------------------------------------------------- |
| [仿宋體](../Page/仿宋體.md "wikilink")   | [100px](../Page/文件:fangsongtij.svg.md "wikilink")    | [100px](../Page/文件:仿宋體.jpg.md "wikilink")            |
| [幼圆体](../Page/幼圆体.md "wikilink")   | [100px](../Page/文件:youyuanti.svg.md "wikilink")      |                                                      |
| [正黑体](../Page/微軟正黑體.md "wikilink") |                                                      | [120px](../Page/文件:MSZH.svg.md "wikilink")           |
| [雅黑体](../Page/微軟雅黑.md "wikilink")  | [120px](../Page/文件:MSYH.svg.md "wikilink")           |                                                      |
| [线体](../Page/线体.md "wikilink")     | [100px](../Page/文件:youxianti.svg.md "wikilink")      | [100px](../Page/文件:youxiantif.svg.md "wikilink")     |
| [姚体](../Page/姚体.md "wikilink")     | [120px](../Page/文件:yaoti.svg.md "wikilink")          | [100px](../Page/文件:yaotif.svg.md "wikilink")         |
| [圓體](../Page/圓體.md "wikilink")     |                                                      | [70px](../Page/文件:Yuanti.gif.md "wikilink")          |
| [黑體](../Page/中易黑体.md "wikilink")   | [100px](../Page/文件:zhongyiheitij.svg.md "wikilink")  | [100px](../Page/文件:zhongyiheitif.svg.md "wikilink")  |
| [宋体](../Page/中易宋体.md "wikilink")   | [100px](../Page/文件:zhongyisongtij.svg.md "wikilink") | [100px](../Page/文件:zhongyisongtif.svg.md "wikilink") |
| [综艺体](../Page/综艺体.md "wikilink")   | [100px](../Page/文件:zongyiti.svg.md "wikilink")       | [100px](../Page/文件:zongyitif.svg.md "wikilink")      |
| [白棋体](../Page/白棋体.md "wikilink")   | [100px](../Page/文件:baiqiti.svg.md "wikilink")        |                                                      |
| [布丁体](../Page/布丁体.md "wikilink")   | [100px](../Page/文件:budingti.svg.md "wikilink")       |                                                      |
| [彩带体](../Page/彩带体.md "wikilink")   |                                                      | [100px](../Page/文件:caidaiti.svg.md "wikilink")       |
| [彩云体](../Page/彩云体.md "wikilink")   | [100px](../Page/文件:Kaiyunti.svg.md "wikilink")       | [100px](../Page/文件:caiyuntif.svg.md "wikilink")      |
| [齿轮体](../Page/齿轮体.md "wikilink")   | [100px](../Page/文件:chilunti.svg.md "wikilink")       |                                                      |
| [粗活意体](../Page/粗活意体.md "wikilink") | [100px](../Page/文件:cuhuoyiti.svg.md "wikilink")      | [100px](../Page/文件:cuhuoyitif.svg.md "wikilink")     |
| [黛玉体](../Page/黛玉体.md "wikilink")   | [100px](../Page/文件:daiyuti.svg.md "wikilink")        |                                                      |
| [雕刻体](../Page/雕刻体.md "wikilink")   | [100px](../Page/文件:diaoketi.svg.md "wikilink")       |                                                      |
| [蝶语体](../Page/蝶语体.md "wikilink")   | [100px](../Page/文件:dieyuti.svg.md "wikilink")        |                                                      |
| [疊圓體](../Page/疊圓體.md "wikilink")   | [100px](../Page/文件:dieyuanti.svg.md "wikilink")      |                                                      |
| [抖抖体](../Page/抖抖体.md "wikilink")   |                                                      |                                                      |
| [嘟嘟体](../Page/嘟嘟体.md "wikilink")   | [100px](../Page/文件:duduti.svg.md "wikilink")         |                                                      |
| [橄榄体](../Page/橄榄体.md "wikilink")   | [100px](../Page/文件:ganlanti.svg.md "wikilink")       | [100px](../Page/文件:ganlantif.svg.md "wikilink")      |
| [广告体](../Page/POP字体.md "wikilink") |                                                      |                                                      |
| [哈哈体](../Page/哈哈体.md "wikilink")   | [100px](../Page/文件:hahati.svg.md "wikilink")         |                                                      |
| [海報體](../Page/海報體.md "wikilink")   | [100px](../Page/文件:haibaoti.svg.md "wikilink")       |                                                      |
| [海纹体](../Page/海纹体.md "wikilink")   | [100px](../Page/文件:haiwenti.svg.md "wikilink")       |                                                      |
| [海韵体](../Page/海韵体.md "wikilink")   | [100px](../Page/文件:haiyunti.svg.md "wikilink")       |                                                      |
| [黑棋体](../Page/黑棋体.md "wikilink")   | [100px](../Page/文件:heiqiti.svg.md "wikilink")        |                                                      |
| [黑咪体](../Page/黑咪体.md "wikilink")   | [100px](../Page/文件:heimiti.svg.md "wikilink")        | [100px](../Page/文件:heimitif.svg.md "wikilink")       |
| [琥珀体](../Page/琥珀体.md "wikilink")   | [100px](../Page/文件:hupoti.svg.md "wikilink")         | [100px](../Page/文件:hupotif.svg.md "wikilink")        |
| [胡子体](../Page/胡子体.md "wikilink")   | [100px](../Page/文件:huziti.svg.md "wikilink")         |                                                      |
| [花瓣体](../Page/花瓣体.md "wikilink")   | [100px](../Page/文件:huabanti.svg.md "wikilink")       |                                                      |
| [花蝶体](../Page/花蝶体.md "wikilink")   | [100px](../Page/文件:huadieti.svg.md "wikilink")       |                                                      |
| [華綜體](../Page/華綜體.md "wikilink")   |                                                      |                                                      |
| [火柴体](../Page/火柴体.md "wikilink")   | [100px](../Page/文件:huochaiti.svg.md "wikilink")      |                                                      |
| [家书体](../Page/家书体.md "wikilink")   | [100px](../Page/文件:jiashuti.svg.md "wikilink")       | [100px](../Page/文件:jiashutif.svg.md "wikilink")      |
| [贱狗体](../Page/贱狗体.md "wikilink")   | [100px](../Page/文件:jiangouti.svg.md "wikilink")      |                                                      |
| [剑体](../Page/剑体.md "wikilink")     | [100px](../Page/文件:jianti.svg.md "wikilink")         |                                                      |
| [荆棘体](../Page/荆棘体.md "wikilink")   | [100px](../Page/文件:jingjiti.svg.md "wikilink")       |                                                      |
| [卡通体](../Page/卡通体.md "wikilink")   | [100px](../Page/文件:katongti.svg.md "wikilink")       | [100px](../Page/文件:katongtif.svg.md "wikilink")      |
| [立黑体](../Page/立黑体.md "wikilink")   | [100px](../Page/文件:liheiti.svg.md "wikilink")        |                                                      |
| [凌波体](../Page/凌波体.md "wikilink")   | [100px](../Page/文件:lingboti.svg.md "wikilink")       | [100px](../Page/文件:lingbotif.svg.md "wikilink")      |
| [菱心体](../Page/菱心体.md "wikilink")   | [100px](../Page/文件:lingxinti.svg.md "wikilink")      |                                                      |
| [流行体](../Page/流行体.md "wikilink")   | [100px](../Page/文件:liuxingti.svg.md "wikilink")      | [100px](../Page/文件:liuxingtif.svg.md "wikilink")     |
| [流叶体](../Page/流叶体.md "wikilink")   | [100px](../Page/文件:liuyetij.svg.md "wikilink")       | [100px](../Page/文件:liuyetif.svg.md "wikilink")       |
| [萝卜体](../Page/萝卜体.md "wikilink")   | [100px](../Page/文件:luoboti.svg.md "wikilink")        |                                                      |
| [漫步体](../Page/漫步体.md "wikilink")   | [100px](../Page/文件:manbuti.svg.md "wikilink")        | [100px](../Page/文件:manbutif.svg.md "wikilink")       |
| [咪咪体](../Page/咪咪体.md "wikilink")   | [100px](../Page/文件:mimiti.svg.md "wikilink")         | [100px](../Page/文件:mimitif.svg.md "wikilink")        |
| [妞妞体](../Page/妞妞体.md "wikilink")   | [100px](../Page/文件:niuniuti.svg.md "wikilink")       |                                                      |
| [胖头鱼体](../Page/胖头鱼体.md "wikilink") | [109px](../Page/文件:pangtouyuti.svg.md "wikilink")    |                                                      |
| [胖娃体](../Page/胖娃体.md "wikilink")   | [100px](../Page/文件:pangwati.svg.md "wikilink")       | [100px](../Page/文件:pangwatif.svg.md "wikilink")      |
| [霹雳体](../Page/霹雳体.md "wikilink")   | [100px](../Page/文件:piliti.svg.md "wikilink")         |                                                      |
| [倩体](../Page/倩体.md "wikilink")     |                                                      |                                                      |
| [清韵体](../Page/清韵体.md "wikilink")   | [100px](../Page/文件:qingyunti.svg.md "wikilink")      |                                                      |
| [趣体](../Page/趣体.md "wikilink")     |                                                      |                                                      |
| [珊瑚体](../Page/珊瑚体.md "wikilink")   | [100px](../Page/文件:xishanhuti.svg.md "wikilink")     | [100px](../Page/文件:xishanhutif.svg.md "wikilink")    |
| [少儿体](../Page/少儿体.md "wikilink")   | [100px](../Page/文件:shaoerti.svg.md "wikilink")       | [100px](../Page/文件:shaoertif.svg.md "wikilink")      |
| [少女體](../Page/少女體.md "wikilink")   |                                                      |                                                      |
| [神工体](../Page/神工体.md "wikilink")   | [100px](../Page/文件:shengongti.svg.md "wikilink")     |                                                      |
| [石头体](../Page/石头体.md "wikilink")   |                                                      |                                                      |
| [水波体](../Page/水波体.md "wikilink")   | [100px](../Page/文件:shuiboti.svg.md "wikilink")       |                                                      |
| [水滴体](../Page/水滴体.md "wikilink")   | [100px](../Page/文件:shuiditi.svg.md "wikilink")       | [100px](../Page/文件:shuiditif.svg.md "wikilink")      |
| [水管体](../Page/水管体.md "wikilink")   | [100px](../Page/文件:shuiguanti.svg.md "wikilink")     |                                                      |
| [水柱体](../Page/水柱体.md "wikilink")   | [100px](../Page/文件:shuizhuti.svg.md "wikilink")      |                                                      |
| [随意体](../Page/随意体.md "wikilink")   |                                                      |                                                      |
| [太极体](../Page/太极体.md "wikilink")   |                                                      |                                                      |
| [弹簧体](../Page/弹簧体.md "wikilink")   | [100px](../Page/文件:tanhuangti.svg.md "wikilink")     |                                                      |
| [娃娃体](../Page/娃娃体.md "wikilink")   | [100px](../Page/文件:wawati.svg.md "wikilink")         |                                                      |
| [习字体](../Page/习字体.md "wikilink")   | [100px](../Page/文件:xiziti.svg.md "wikilink")         |                                                      |
| [潇洒体](../Page/潇洒体.md "wikilink")   | [100px](../Page/文件:xiaosati.svg.md "wikilink")       |                                                      |
| [行黑体](../Page/行黑体.md "wikilink")   | [100px](../Page/文件:xingheiti.svg.md "wikilink")      |                                                      |
| [醒示体](../Page/醒示体.md "wikilink")   | [100px](../Page/文件:xingshiti.svg.md "wikilink")      | [100px](../Page/文件:xingshitif.svg.md "wikilink")     |
| [秀英体](../Page/秀英体.md "wikilink")   | [100px](../Page/文件:xiuyingti.svg.md "wikilink")      | [100px](../Page/文件:xiuyingtif.svg.md "wikilink")     |
| [雪峰体](../Page/雪峰体.md "wikilink")   | [100px](../Page/文件:xuefengti.svg.md "wikilink")      | [100px](../Page/文件:xuefengtif.svg.md "wikilink")     |
| [丫丫体](../Page/丫丫体.md "wikilink")   | [100px](../Page/文件:yayati.svg.md "wikilink")         |                                                      |
| [雁翎体](../Page/雁翎体.md "wikilink")   | [100px](../Page/文件:yanlingti.svg.md "wikilink")      |                                                      |
| [幼幼體](../Page/幼幼體.md "wikilink")   |                                                      |                                                      |
| [藏体](../Page/藏体.md "wikilink")     | [100px](../Page/文件:zangti.svg.md "wikilink")         |                                                      |
| [毡笔黑体](../Page/毡笔黑体.md "wikilink") | [100px](../Page/文件:zhanbiheiti.svg.md "wikilink")    | [100px](../Page/文件:zhanbiheitif.svg.md "wikilink")   |
| [竹节体](../Page/竹节体.md "wikilink")   | [100px](../Page/文件:zhujieti.svg.md "wikilink")       | [100px](../Page/文件:zhujietif.svg.md "wikilink")      |
| [竹子体](../Page/竹子体.md "wikilink")   | [100px](../Page/文件:zhuziti.svg.md "wikilink")        |                                                      |

## 注釋

<references />

## 另見

  - [Mac OS X字体列表](../Page/Mac_OS_X字体列表.md "wikilink")
  - [漢字標準列表](../Page/漢字標準列表.md "wikilink")

## 外部链接

  - [韩文TTF自由字体下载](http://www.ktranslation.com/korean-fonts.php)

  - [自由简体中文字体列表](http://www.wazu.jp/gallery/Fonts_ChineseSimplified.html)

  - [自由-{zh-hant:繁體; zh-hans:繁体;
    zh-tw:正體;}-中文字体列表](http://www.wazu.jp/gallery/Fonts_ChineseTraditional.html)

  - [自由日文字体列表](http://www.wazu.jp/gallery/Fonts_Japanese.html)

  - [自由韩文字体列表](http://www.wazu.jp/gallery/Fonts_Korean.html)

  - [Free Chinese Font](https://www.freechinesefont.com/)

  - [Free Japanese Font](https://www.freejapanesefont.com/)

  - [Free Korean Fonts](https://www.freekoreanfont.com/)

  - [Arphic Public
    License](http://ftp.gnu.org/gnu/non-gnu/chinese-fonts-truetype/LICENSE)：文鼎科技公司授权的自由字体协议

  - [免费中文字体](http://wiki.ubuntu.org.cn/%E5%85%8D%E8%B4%B9%E4%B8%AD%E6%96%87%E5%AD%97%E4%BD%93)

  - [適用於 GNU/Linux
    的字型](https://web.archive.org/web/20100630083107/http://wiki.debian.org.hk/w/Where_can_I_find_fonts_for_GNU/Linux)

  - [Nôm Font](http://nomfoundation.org/nom-tools/Nom-Font): The Hán-Nôm
    Coded Character Set

[\*](../Category/東亞字體.md "wikilink")

1.

2.   "[Quy trình Nôm Na: *Giúp đọc Nôm và Hán Việt* và chữ Nôm trên
    mạng](http://www.tapchithoidai.org/ThoiDai5/200505_NhomNomNa.pdf)",
    *Tạp chí Nghiên cứu và Thảo luận*, 5 July 2005.

3.

4.

5.

6.  [Author page](http://wiki.fdiary.net/font/?sazanami) (in Japanese)

7.  [Deprecated ttf-sazanami-gothic Debian
    package](http://packages.debian.org/wheezy/ttf-sazanami-gothic)

8.