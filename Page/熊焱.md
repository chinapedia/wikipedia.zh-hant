[Xiong_Yan_is_interviewing_in_Hong_Kong_in_May_31_2009.JPG](https://zh.wikipedia.org/wiki/File:Xiong_Yan_is_interviewing_in_Hong_Kong_in_May_31_2009.JPG "fig:Xiong_Yan_is_interviewing_in_Hong_Kong_in_May_31_2009.JPG")

**熊焱**（\[1\]），[華裔美國人](../Page/華裔美國人.md "wikilink")、[美國陸軍退役](../Page/美國陸軍.md "wikilink")[少校](../Page/少校.md "wikilink")，生於中國[湖南省](../Page/湖南省.md "wikilink")[双峰县](../Page/双峰县.md "wikilink")，為中國[持不同政見者及](../Page/持不同政見者.md "wikilink")[八九民運的學生領袖之一](../Page/八九民運.md "wikilink")，后加入[美国国籍](../Page/美国国籍.md "wikilink")，曾为[美軍駐](../Page/美軍.md "wikilink")[伊拉克的](../Page/伊拉克.md "wikilink")[隨軍牧師](../Page/隨軍牧師.md "wikilink")\[2\]。

## 生平

他曾为[北京大学法律系代培研究生](../Page/北京大学.md "wikilink")，於1985年加入[中國共產黨](../Page/中國共產黨.md "wikilink")，直至1989年[六四事件爆發才退出黨籍](../Page/六四事件.md "wikilink")。熊焱是六四事件中被当局列为二十一名学生领袖之一受到通缉。后来在[内蒙古自治区被捕](../Page/内蒙古自治区.md "wikilink")，1991年出狱。1992年通过特殊渠道到[香港](../Page/香港.md "wikilink")，同年流亡[美国并加入教会](../Page/美国.md "wikilink")，在[马萨诸塞州](../Page/马萨诸塞州.md "wikilink")[波士頓](../Page/波士頓.md "wikilink")[戈登康维尔神学院](../Page/戈登康维尔神学院.md "wikilink")(Gordon-Conwell
Theological
Seminary)進修[神學](../Page/神學.md "wikilink")\[3\]，之後成為美国陆军的隨軍牧師，在伊拉克服役\[4\]\[5\]。2005年，熊焱對於由[大紀元時報成員所發起的退出中國共產黨運動表示支持](../Page/大紀元時報.md "wikilink")\[6\]。

2009年5月，熊焱到訪香港參與[六四事件二十週年紀念活動](../Page/六四事件二十週年.md "wikilink")，是他自1992年逃離中國後，首次成功入境香港\[7\]。

2014年，熊焱对[自由亚洲电台发表谈话](../Page/自由亚洲电台.md "wikilink")，要求改变中国政治制度，主张中国人要“非常严肃的抗争”。2015年，熊焱接受媒体采访并发表公开信，要求回中国探望病重的母亲。《[环球时报](../Page/环球时报.md "wikilink")》评论，“熊焱要求回国探母，这当中的亲情没人要否认。但熊焱以政治公开信的方式吸引西方主流媒体关注，制造压力，也在把亲情搞成迎合西方舆论兴奋点的政治表演。”因此在其转变态度前，无法获准到中国探亲。试图经香港进入内地，最终被香港方面拒绝入境，遣返美国\[8\]。

## 參考文獻

## 外部链接

  -
{{-}}

[Category:八九民运学生领袖](../Category/八九民运学生领袖.md "wikilink")
[Category:中华人民共和国民主运动人物](../Category/中华人民共和国民主运动人物.md "wikilink")
[Category:北京大學校友](../Category/北京大學校友.md "wikilink")
[Category:美国华人牧师](../Category/美国华人牧师.md "wikilink")
[Category:華裔美國軍事人物](../Category/華裔美國軍事人物.md "wikilink")
[Category:美國陸軍少校](../Category/美國陸軍少校.md "wikilink")
[Category:华裔美国人](../Category/华裔美国人.md "wikilink")
[Y焱](../Category/熊姓.md "wikilink")
[Category:双峰县人](../Category/双峰县人.md "wikilink")

1.
2.  [Where Are Some of the “Most Wanted” Participants
    Today?](http://www.hrw.org/campaigns/china/scholars/t15/xiongyan.htm)
    Human Rights Watch
3.  [Tiananmen's Most Wanted-Where Are They
    Now?](http://www.hrichina.org/fs/view/downloadables/pdf/downloadable-resources/b6_TiananmensMost6.2004.pdf)
     Human Rights in China
4.
5.
6.
7.
8.