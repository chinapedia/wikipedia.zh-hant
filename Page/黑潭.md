**黑潭**（中國大陸譯為**布莱-{}-克浦**；[香港譯為](../Page/香港.md "wikilink")**黑-{}-池**；[台灣譯為](../Page/台灣.md "wikilink")**黑-{}-潭**，，讀音：)
，[英國](../Page/英國.md "wikilink")[英格蘭](../Page/英格蘭.md "wikilink")[西北區域的](../Page/西北英格蘭.md "wikilink")[單一管理區](../Page/單一管理區.md "wikilink")，屬於[名譽的](../Page/名譽郡.md "wikilink")[蘭開夏郡一部分](../Page/蘭開夏.md "wikilink")，有142,700[人口](../Page/人口.md "wikilink")，[佔地](../Page/面積.md "wikilink")34.92平方公里，行政總部位於[布萊克本](../Page/布萊克本.md "wikilink")。

黑潭西臨[愛爾蘭海](../Page/愛爾蘭海.md "wikilink")，東、南、北三方被蘭開夏郡包圍：東北與[懷爾區相鄰](../Page/懷爾區.md "wikilink")，東南與[法爾德區相鄰](../Page/法爾德區.md "wikilink")。黑潭以東南325公里（202英里）達英國首都[倫敦](../Page/倫敦.md "wikilink")，以東南偏東24公里（15英里）達蘭開夏郡[郡治](../Page/郡治.md "wikilink")[普雷斯頓](../Page/普雷斯頓.md "wikilink")。

## 語源

從前有一條[排水道流經一個](../Page/排水道.md "wikilink")[泥煤](../Page/泥煤.md "wikilink")[沼澤](../Page/沼澤.md "wikilink")，把被[污染的水排入](../Page/污染.md "wikilink")[愛爾蘭海](../Page/愛爾蘭海.md "wikilink")，途中形成一個[黑色](../Page/黑色.md "wikilink")（black）的池（pool），該地故此得名「Blackpool」。

## 地方沿革

《[1888年地方政府法案](../Page/1888年地方政府法案.md "wikilink")》在1889年4月1日生效，黑潭成為[郡自治市鎮](../Page/郡自治市鎮.md "wikilink")（County
Borough，獨立於蘭開夏郡議會的Borough）。《[1972年地方政府法案](../Page/1972年地方政府法案.md "wikilink")》在1974年4月1日生效，黑潭成為蘭開夏郡的[非都市區](../Page/非都市區.md "wikilink")（Non-metropolitan
district）。1992年成立的[英格蘭地方政府委員會](../Page/英格蘭地方政府委員會_\(1992年\).md "wikilink")（Local
Government Commission for
England）建議黑潭成為[單一管理區](../Page/單一管理區.md "wikilink")，不再是非都市區，在1998年4月1日生效至今。

[蘭開夏郡既是](../Page/蘭開夏郡.md "wikilink")[非都市郡](../Page/非都市郡.md "wikilink")，又是[名譽郡](../Page/名譽郡.md "wikilink")，而黑潭是[單一管理區](../Page/單一管理區.md "wikilink")。如將蘭開夏郡看待為[非都市郡](../Page/非都市郡.md "wikilink")，黑潭不屬它的一部分；如將蘭開夏郡看待為[名譽郡](../Page/名譽郡.md "wikilink")，黑潭行政上仍屬它的一部分。

## 旅遊

**黑潭**著名的現代休閒度假地。[海勒姆·馬克沁建造的](../Page/海勒姆·馬克沁.md "wikilink")[遊樂場](../Page/遊樂場.md "wikilink")、[摩天輪](../Page/摩天輪.md "wikilink")，及1950年开始舉辦的[國際標準舞比賽也很有名](../Page/國際標準舞.md "wikilink")。

### 景點

  - Blackpool Zoo & Dinosaur Safari\[1\]
  - Sandcastle Waterpark
  - Pleasure Beach

## 交通

[黑池電車是英國僅存的唯一一條第一代電車線](../Page/黑池電車.md "wikilink")，來往英格蘭蘭開夏郡的黑池與弗利特伍德。

## 地理

[ [蘭開夏郡行政區劃圖](../Page/蘭開夏郡.md "wikilink")：
1\. [蘭開夏郡西區](../Page/蘭開夏郡西區.md "wikilink")
2\. [喬利區](../Page/喬利區.md "wikilink")
3\. [南-{里}-布爾區](../Page/南里布爾區.md "wikilink")
4\. [法爾德區](../Page/法爾德區.md "wikilink")
5\. [普雷斯頓](../Page/普雷斯頓.md "wikilink")
6\. [懷爾區](../Page/懷爾區.md "wikilink")
7\. [蘭開斯特市](../Page/蘭開斯特市.md "wikilink")
8\. [-{里}-布爾河谷區](../Page/里布爾河谷區.md "wikilink")
9\. [彭德爾區](../Page/彭德爾區.md "wikilink")
10\. [伯恩利區](../Page/伯恩利區.md "wikilink")
11\. [羅森代爾區](../Page/羅森代爾區.md "wikilink")
12\. [欣德本區](../Page/欣德本區.md "wikilink")
**13. 黑潭**
14\.
[布萊克本-達溫](../Page/布萊克本-達溫.md "wikilink")](https://zh.wikipedia.org/wiki/File:Lancashire_Ceremonial_Numbered.png "fig: 蘭開夏郡行政區劃圖： 1. 蘭開夏郡西區 2. 喬利區 3. 南-{里}-布爾區 4. 法爾德區 5. 普雷斯頓 6. 懷爾區 7. 蘭開斯特市 8. -{里}-布爾河谷區 9. 彭德爾區 10. 伯恩利區 11. 羅森代爾區 12. 欣德本區 13. 黑潭 14. 布萊克本-達溫")
**黑潭**在[英格蘭](../Page/英格蘭.md "wikilink")[西北部](../Page/英格蘭西北.md "wikilink")，西岸臨[愛爾蘭海](../Page/愛爾蘭海.md "wikilink")，東、南、北三方被[蘭開夏郡包圍](../Page/蘭開夏郡.md "wikilink")：東北與[懷爾區相鄰](../Page/懷爾區.md "wikilink")，東南與[法爾德區相鄰](../Page/法爾德區.md "wikilink")。

### 鄰近大城鎮

以下列出黑潭行政總部[布萊克本附近的大城市](../Page/布萊克本.md "wikilink")（[方位以布萊克本為量度點](../Page/方位.md "wikilink")）：

| 城市                                                               | 方位 | 距離（公里 / 英里） | 備註     |
| ---------------------------------------------------------------- | -- | ----------- | ------ |
| [倫敦](../Page/倫敦.md "wikilink")（[西敏宮](../Page/西敏宮.md "wikilink")） | 東南 | 325 km      | 202 mi |
| [普雷斯頓](../Page/普雷斯頓.md "wikilink")                               | 東南 | 24 km       | 15 mi  |
| [列斯](../Page/列斯.md "wikilink")                                   | 東  | 99 km       | 62 mi  |
| [利物浦](../Page/利物浦.md "wikilink")                                 | 南  | 45 km       | 28 mi  |
| [曼徹斯特](../Page/曼徹斯特.md "wikilink")                               | 東南 | 66 km       | 41 mi  |
| [布拉德福德](../Page/布拉德福德.md "wikilink")                             | 東  | 86 km       | 53 mi  |
| [設菲爾德](../Page/設菲爾德.md "wikilink")                               | 東南 | 116 km      | 72 mi  |
| [特倫特河畔斯托克](../Page/特倫特河畔斯托克.md "wikilink")                       | 東南 | 113 km      | 70 mi  |

## 圖片集

<File:Pepsi> BM.JPG|[過山車](../Page/過山車.md "wikilink")「Pepsi Max Big One」
<File:BlackpoolTower> OwlofDoom.jpg|地標「[Blackpool
Tower](../Page/布萊克浦爾塔.md "wikilink")」 <File:Blackpool>
centralpier winter.jpg|中央碼頭
[File:BlackpoolTN.JPG|夜景](File:BlackpoolTN.JPG%7C夜景)

## 相關條目

  - [黑池舞蹈節](../Page/黑池舞蹈節.md "wikilink")
  - [黑池電車](../Page/黑池電車.md "wikilink")

## 註釋

## 外部連結

  - 黑潭[議會](http://www.blackpool.gov.uk)

  - 《[紐約時報](../Page/紐約時報.md "wikilink")》《[Tacky, Wonderful
    Blackpool](http://query.nytimes.com/gst/fullpage.html?res=9F0DE6D61531F934A3575BC0A962958260&scp=4&sq=blackpool&st=cse)》

[Category:英格蘭的單一管理區](../Category/英格蘭的單一管理區.md "wikilink")
[Category:蘭開夏郡](../Category/蘭開夏郡.md "wikilink")

1.  <http://www.blackpoolzoo.org.uk/>