是由[角川書店發行的](../Page/角川書店.md "wikilink")[日語](../Page/日語.md "wikilink")[電子遊戲](../Page/電子遊戲.md "wikilink")[雜誌](../Page/雜誌.md "wikilink")，原由[ASCII](../Page/ASCII_\(企業\).md "wikilink")（創刊至2000年3月）和[Enterbrain](../Page/Enterbrain.md "wikilink")（2000年4月至2013年9月）發行。
1986年，从计算机杂志《Login》派生的电子游戏专门杂志《ファミコン通信（Famicom通信）》创刊。自1995年起，杂志将正式名称定为从创刊伊始就常用的简称《Fami通》。其中週刊Fami通是最早發行的Fami通刊物，被視為[日本最受尊敬的電子遊戲新聞雜誌](../Page/日本.md "wikilink")\[1\]\[2\]\[3\]。

## 週刊Fami通

週刊Fami通集中評測電子遊戲與報道電子遊戲業界的新聞，名字Fami通是縮寫自初時的名稱**Famicom通信**（），刊行當初的名稱是按照在1980年代後期支配日本市場的[任天堂電子遊戲機](../Page/任天堂.md "wikilink")[Famicom而命名](../Page/FC遊戲機.md "wikilink")。首期雜誌於1986年出版發行，曾授權內容給[尖端出版](../Page/尖端出版.md "wikilink")[中文電子遊戲雜誌](../Page/中文.md "wikilink")《[電視遊樂報導](../Page/電視遊樂報導.md "wikilink")》（*TV
Game Report*）。週刊Fami通逢星期五發行。

### 電玩通週刊

於2004年2月27日由[台灣](../Page/台灣.md "wikilink")[青文出版社以台灣版中文雙週刊創刊](../Page/青文出版社.md "wikilink")，並於第13期（2004年8月20日）起改為週刊。與週刊Fami通相同每逢星期四發行，2014年1月23日休刊並發行最後一期\[4\]\[5\]\[6\]\[7\]\[8\]。

## 过往副刊与增刊

[FamitsuWaveDVDApril05.jpg](https://zh.wikipedia.org/wiki/File:FamitsuWaveDVDApril05.jpg "fig:FamitsuWaveDVDApril05.jpg")》的角色“日之本零子”與“Dixie
Clemets”\]\] 《Fami通》旗下曾还有按不同游戏机等细分的副刊与增刊：

  - **Fami通DS+Wii**
    報導[任天堂裝置平台的消息](../Page/任天堂.md "wikilink")（現時為[任天堂DS與](../Page/任天堂DS.md "wikilink")[Wii](../Page/Wii.md "wikilink")），該雜誌曾按任天堂早前的平台取名為Fami通64及Fami通Cube，2016年3月号发行后已停刊。
  - **Fami通Xbox 360** 報導[Xbox與](../Page/Xbox_\(遊戲機\).md "wikilink")[Xbox
    360的消息](../Page/Xbox_360.md "wikilink")，2013年起已停刊。
  - **Fami通Wave DVD**
    月刊，每期雜誌均包含一張載有電子遊戲影像的[DVD-Video](../Page/DVD-Video.md "wikilink")（[NTSC制式](../Page/NTSC制式.md "wikilink")，[DVD區域碼第](../Page/DVD區域碼.md "wikilink")2區），前身為GameWave
    DVD，而GameWave
    DVD的前身則是F.Wave其中的電玩遊戲介紹欄目；F.Wave是Enterbrain首次出版帶有DVD影像光碟的刊物，跟Fami通Wave刊物名稱一樣同讀作「ファミツウウェーブ」但內容則略有分別，此刊物於2000年5月16日創刊，除了電玩以外亦收錄有[電影](../Page/電影.md "wikilink")、[音樂](../Page/音樂.md "wikilink")、[藝能](../Page/藝能.md "wikilink")、[時裝](../Page/時裝.md "wikilink")、[模型等情報](../Page/模型.md "wikilink")，但只出了四期便告停刊，2000年9月29日改為出版只帶電玩情報內容的GameWave，2002年10月30日改名為Fami通Wave
    DVD。2011年5月号发行后已停刊。

現時已停刊的Fami通还包括*'Fami通Bros.
**（集中報導遊戲攻略提示而非當前的遊戲消息）、**Fami通Sister**（集中報導[美少女遊戲](../Page/美少女遊戲.md "wikilink")）、**Fami通DC**（集中報導[Dreamcast的消息](../Page/Dreamcast.md "wikilink")）與**Fami通PSP+PS3*'（集中報導[索尼電腦娛樂旗下電子主機遊戲的消息](../Page/索尼電腦娛樂.md "wikilink")）。

## 評分

Fami通因對當今電子遊戲非常嚴格的評分而全球知名\[9\]\[10\]，電子遊戲會經由一組四人的Fami通電子遊戲評論家評分，每人會給出1至10分（10分最高），然後將四個分數相加，最高可得到40分，以評分滿分為日本本土遊戲居多。Fami通的評論家長期以來被認為過分強硬，雖然近年他們的評分平均有明顯的上升。近期Fami通數個評分備受到爭議，如《[地狱犬的挽歌
-最终幻想VII-](../Page/地狱犬的挽歌_-最终幻想VII-.md "wikilink")》的評分\[11\]，該雜誌被指為取悅廣告客戶與產業中的大型廠商而「背叛」。有一個詳盡評論指出Fami通的編輯會給予擁有固有玩家群的流行遊戲較高的分數，通常為9到10分，而不顧該遊戲的品質\[12\]；例如《[JOJO奇妙冒险全明星战斗](../Page/JOJO奇妙冒险全明星战斗.md "wikilink")》。

評分35分以上是白金殿堂，32分以上是金殿堂，30分以上是銀殿堂，因此在35分以上的遊戲基本上都是很經典的遊戲了。

但一款遊戲要獲得四名Fami通評論家評為滿分仍然非常困難。Fami通Wave DVD不會為電子遊戲評分。

### 滿分評分

至今為止有二十五款遊戲獲滿分40評分，以下按年份列出：

1.  [薩爾達傳說
    時之笛](../Page/薩爾達傳說_時之笛.md "wikilink")（1998，[任天堂](../Page/任天堂.md "wikilink")，[任天堂64](../Page/任天堂64.md "wikilink")）
2.  [劍魂](../Page/劍魂_\(格鬥遊戲\).md "wikilink")（1999，[南夢宮](../Page/南夢宮.md "wikilink")，[Dreamcast](../Page/Dreamcast.md "wikilink")）
3.  [放浪冒險譚](../Page/放浪冒險譚.md "wikilink")（2000，[史克威爾](../Page/史克威爾.md "wikilink")，[PlayStation](../Page/PlayStation_\(遊戲機\).md "wikilink")）
4.  [薩爾達傳說
    風之律動](../Page/薩爾達傳說_風之律動.md "wikilink")（2003，[任天堂](../Page/任天堂.md "wikilink")，[GameCube](../Page/GameCube.md "wikilink")）
5.  [任天狗](../Page/任天狗.md "wikilink")（2005，[任天堂](../Page/任天堂.md "wikilink")，[Nintendo
    DS](../Page/Nintendo_DS.md "wikilink")）
6.  [Final Fantasy
    XII](../Page/Final_Fantasy_XII.md "wikilink")（2006，[史克威爾艾尼克斯](../Page/史克威爾艾尼克斯.md "wikilink")，[PlayStation
    2](../Page/PlayStation_2.md "wikilink")）
7.  [任天堂明星大亂鬥X](../Page/任天堂明星大亂鬥X.md "wikilink")（2008，[任天堂](../Page/任天堂.md "wikilink")，[Wii](../Page/Wii.md "wikilink")）
8.  [潛龍諜影4：愛國者之槍](../Page/潛龍諜影4：愛國者之槍.md "wikilink")（2008，[科樂美](../Page/科樂美.md "wikilink")，[PlayStation
    3](../Page/PlayStation_3.md "wikilink")）
9.  [428
    被封鎖的澀谷](../Page/428_被封鎖的澀谷.md "wikilink")（2008，[SEGA](../Page/SEGA.md "wikilink")，[Wii](../Page/Wii.md "wikilink")）
10. [勇者斗恶龙IX
    星空的守护者](../Page/勇者斗恶龙IX_星空的守护者.md "wikilink")（2009，[史克威爾艾尼克斯](../Page/史克威爾艾尼克斯.md "wikilink")，[Nintendo
    DS](../Page/Nintendo_DS.md "wikilink")）
11. [魔物獵人3](../Page/魔物獵人3.md "wikilink")（2009，[卡普空](../Page/卡普空.md "wikilink")，[Wii](../Page/Wii.md "wikilink")）
12. [魔兵驚天錄](../Page/魔兵驚天錄.md "wikilink")（2009，[SEGA](../Page/SEGA.md "wikilink")，[Xbox
    360](../Page/Xbox_360.md "wikilink")\[13\]）
13. [新超级马里奥兄弟Wii](../Page/新超级马里奥兄弟Wii.md "wikilink")
    （2009，[任天堂](../Page/任天堂.md "wikilink")，[Wii](../Page/Wii.md "wikilink")）
14. [潛龍諜影：和平先驅](../Page/潛龍諜影：和平先驅.md "wikilink")
    （2010，[科乐美](../Page/科乐美.md "wikilink")，[PlayStation
    Portable](../Page/PlayStation_Portable.md "wikilink")）
15. [精灵宝可梦 黑·白](../Page/精灵宝可梦_黑·白.md "wikilink")
    （2010，[精灵宝可梦公司](../Page/精灵宝可梦公司.md "wikilink")，[Nintendo
    DS](../Page/Nintendo_DS.md "wikilink")）
16. [薩爾達傳說 天空之劍](../Page/薩爾達傳說_天空之劍.md "wikilink")
    （2011，[任天堂](../Page/任天堂.md "wikilink")，[Wii](../Page/Wii.md "wikilink")）
17. [上古卷軸V：天際](../Page/上古卷軸V：天際.md "wikilink")
    （2011，[貝塞斯達軟體公司](../Page/貝塞斯達軟體公司.md "wikilink")，[Xbox
    360](../Page/Xbox_360.md "wikilink")、[PlayStation
    3](../Page/PlayStation_3.md "wikilink")）- 首次非日本遊戲獲得此殊榮
18. [Final Fantasy XIII-2](../Page/Final_Fantasy_XIII-2.md "wikilink")
    （2011，[史克威爾艾尼克斯](../Page/史克威爾艾尼克斯.md "wikilink")，[Xbox
    360](../Page/Xbox_360.md "wikilink")、[PlayStation
    3](../Page/PlayStation_3.md "wikilink")）
19. [新光神話 帕爾提娜之鏡](../Page/新光神話_帕爾提娜之鏡.md "wikilink")
    （2012，[任天堂](../Page/任天堂.md "wikilink")，[任天堂3DS](../Page/任天堂3DS.md "wikilink")）
20. [人中之龍5 夢想的實現者](../Page/人中之龍5_夢想的實現者.md "wikilink")
    （2012，[SEGA](../Page/SEGA.md "wikilink")，[PlayStation
    3](../Page/PlayStation_3.md "wikilink")）
21. [JOJO奇妙冒险全明星战斗](../Page/JOJO奇妙冒险全明星战斗.md "wikilink")
    （2013，[南梦宫万代](../Page/南梦宫万代游戏.md "wikilink")，[PlayStation
    3](../Page/PlayStation_3.md "wikilink")）
22. [俠盜獵車手V](../Page/俠盜獵車手V.md "wikilink") （2013，[Rockstar
    Games](../Page/Rockstar_Games.md "wikilink")，[Xbox
    360](../Page/Xbox_360.md "wikilink")、[PlayStation
    3](../Page/PlayStation_3.md "wikilink")）
23. [潛龍諜影V 幻痛](../Page/潛龍諜影V_幻痛.md "wikilink") （2015，科乐美，[Xbox
    360](../Page/Xbox_360.md "wikilink")、[PlayStation
    3](../Page/PlayStation_3.md "wikilink")、[PlayStation
    4](../Page/PlayStation_4.md "wikilink")、[Xbox
    One](../Page/Xbox_One.md "wikilink")）
24. [塞尔达传说 旷野之息](../Page/塞尔达传说_旷野之息.md "wikilink") （2017，任天堂，[Wii
    U](../Page/Wii_U.md "wikilink")、[任天堂Switch](../Page/任天堂Switch.md "wikilink")）
25. [勇者鬥惡龍XI 追尋逝去的時光](../Page/勇者鬥惡龍XI_追尋逝去的時光.md "wikilink")
    （2017，史克威尔艾尼克斯，[PlayStation
    4](../Page/PlayStation_4.md "wikilink")、[任天堂3DS](../Page/任天堂3DS.md "wikilink")）

獲得39分，接近滿分的遊戲：

1.  [薩爾達傳說
    眾神的三角神力](../Page/薩爾達傳說_眾神的三角神力.md "wikilink")（1991，[任天堂](../Page/任天堂.md "wikilink")，[超級任天堂](../Page/超級任天堂.md "wikilink")）
2.  [-{zh-cn:VR战士2; zh-hk:VR戰士2;
    zh-tw:VR快打2;}-](../Page/VR快打.md "wikilink")（1995，[SEGA](../Page/SEGA.md "wikilink")，[SEGA
    Saturn](../Page/SEGA_Saturn.md "wikilink")）
3.  [實感賽車Revolution](../Page/實感賽車Revolution.md "wikilink")（1995，[南夢宮](../Page/南夢宮.md "wikilink")，[PlayStation](../Page/PlayStation_\(遊戲機\).md "wikilink")）
4.  [超級瑪利歐64](../Page/超級瑪利歐64.md "wikilink")（1996，[任天堂](../Page/任天堂.md "wikilink")，[任天堂64](../Page/任天堂64.md "wikilink")）
5.  [鐵拳3](../Page/鐵拳3.md "wikilink")（1998，[南夢宮](../Page/南夢宮.md "wikilink")，[PlayStation](../Page/PlayStation_\(遊戲機\).md "wikilink")）
6.  [電腦戰機](../Page/電腦戰機.md "wikilink")（1999，[世嘉](../Page/世嘉.md "wikilink")，[Dreamcast](../Page/Dreamcast.md "wikilink")）
7.  [跑車浪漫旅3:
    A-Spec](../Page/跑車浪漫旅#Gran_Turismo_3:_A-Spec.md "wikilink")（2001，[索尼電腦娛樂](../Page/索尼電腦娛樂.md "wikilink")，[PlayStation
    2](../Page/PlayStation_2.md "wikilink")）
8.  [Final Fantasy
    X](../Page/Final_Fantasy_X.md "wikilink")（2001，[史克威爾](../Page/史克威爾.md "wikilink")，[PlayStation
    2](../Page/PlayStation_2.md "wikilink")）
9.  [惡靈古堡](../Page/惡靈古堡.md "wikilink")（2002，[卡普空](../Page/卡普空.md "wikilink")，[Nintendo
    GameCube](../Page/Nintendo_GameCube.md "wikilink")）
10. [勇者斗恶龙VIII
    天空、海洋、大地与被诅咒的公主](../Page/勇者斗恶龙VIII_天空、海洋、大地与被诅咒的公主.md "wikilink")（2004，[史克威爾艾尼克斯](../Page/史克威爾艾尼克斯.md "wikilink")，[PlayStation
    2](../Page/PlayStation_2.md "wikilink")）
11. [跑車浪漫旅4](../Page/跑車浪漫旅#Gran_Turismo_4.md "wikilink")（2004，[索尼電腦娛樂](../Page/索尼電腦娛樂.md "wikilink")，[PlayStation
    2](../Page/PlayStation_2.md "wikilink")）
12. [潛龍諜影3：食蛇者](../Page/潛龍諜影3：食蛇者.md "wikilink")（2005，[科樂美](../Page/科樂美.md "wikilink")，[PlayStation
    2](../Page/PlayStation_2.md "wikilink")）
13. [生死格鬥4](../Page/生死格鬥#Dead_or_Alive_4.md "wikilink")（2005，[TECMO](../Page/TECMO.md "wikilink")，[Xbox
    360](../Page/Xbox_360.md "wikilink")）
14. [王國之心II](../Page/王國之心II.md "wikilink")（2006，[史克威爾艾尼克斯](../Page/史克威爾艾尼克斯.md "wikilink")，[PlayStation
    2](../Page/PlayStation_2.md "wikilink")）
15. [大神](../Page/大神_\(游戏\).md "wikilink")（2006，[卡普空](../Page/卡普空.md "wikilink")，[PlayStation
    2](../Page/PlayStation_2.md "wikilink")）
16. [薩爾達傳說
    幻影沙漏](../Page/薩爾達傳說_幻影沙漏.md "wikilink")（2007，[任天堂](../Page/任天堂.md "wikilink")，[Nintendo
    DS](../Page/Nintendo_DS.md "wikilink")）
17. [俠盗獵車手4](../Page/俠盗獵車手4.md "wikilink")（2008，[Rockstar
    Games](../Page/Rockstar_Games.md "wikilink")，[Xbox
    360](../Page/Xbox_360.md "wikilink")／[PlayStation
    3](../Page/PlayStation_3.md "wikilink")）
18. [決勝時刻：現代戰爭2](../Page/決勝時刻：現代戰爭2.md "wikilink")（2009，[Infinity
    Ward](../Page/Infinity_Ward.md "wikilink")，[Xbox
    360](../Page/Xbox_360.md "wikilink")／[PlayStation
    3](../Page/PlayStation_3.md "wikilink")）
19. [最终幻想XIII](../Page/最终幻想XIII.md "wikilink")（2009，[史克威尔艾尼克斯](../Page/史克威尔艾尼克斯.md "wikilink")，[PlayStation
    3](../Page/PlayStation_3.md "wikilink")）
20. [碧血狂殺](../Page/碧血狂殺.md "wikilink")（2010，[Rockstar
    Games](../Page/Rockstar_Games.md "wikilink")，[Xbox
    360](../Page/Xbox_360.md "wikilink")／[PlayStation
    3](../Page/PlayStation_3.md "wikilink")）
21. [火影忍者：究极忍者风暴2](../Page/火影忍者：究极忍者风暴2.md "wikilink") (2010,
    [南梦宫万代](../Page/南梦宫万代游戏.md "wikilink")，Xbox360 /
    PlayStation 3)
22. [魔物獵人攜帶版3rd](../Page/魔物獵人攜帶版3rd.md "wikilink")（2010，[卡普空](../Page/卡普空.md "wikilink")，[PlayStation
    Portable](../Page/PlayStation_Portable.md "wikilink")）
23. [決勝時刻：黑色行动](../Page/決勝時刻：黑色行动.md "wikilink")（2010，[Treyarch](../Page/Treyarch.md "wikilink")，[Xbox
    360](../Page/Xbox_360.md "wikilink")／[PlayStation
    3](../Page/PlayStation_3.md "wikilink")）
24. [決勝時刻：現代戰爭3](../Page/決勝時刻：現代戰爭3.md "wikilink")（2011，[Infinity
    Ward](../Page/Infinity_Ward.md "wikilink") & [Sledgehammer
    Games](../Page/Sledgehammer_Games.md "wikilink")，[Xbox
    360](../Page/Xbox_360.md "wikilink")／[PlayStation
    3](../Page/PlayStation_3.md "wikilink")）
25. [最终幻想
    零式](../Page/最终幻想_零式.md "wikilink")（2011，[史克威尔艾尼克斯](../Page/史克威尔艾尼克斯.md "wikilink")，[PlayStation
    Portable](../Page/PlayStation_Portable.md "wikilink")）
26. [戰爭機器3](../Page/戰爭機器3.md "wikilink")（2011，[Epic
    Games](../Page/Epic_Games.md "wikilink") &
    [微軟遊戲工作室](../Page/微軟遊戲工作室.md "wikilink")，[Xbox
    360](../Page/Xbox_360.md "wikilink")）
27. [潛龍諜影：和平先驅](../Page/潛龍諜影：和平先驅.md "wikilink")
    HD（2011，[科乐美](../Page/科乐美.md "wikilink")，[PlayStation
    3](../Page/PlayStation_3.md "wikilink")39分／[Xbox
    360](../Page/Xbox_360.md "wikilink")37分）
28. [黑色洛城](../Page/黑色洛城.md "wikilink")（2011，[Rockstar
    Games](../Page/Rockstar_Games.md "wikilink") & [Team
    Bondi](../Page/Team_Bondi.md "wikilink")，[Xbox360](../Page/Xbox360.md "wikilink")
    / [PlayStation 3](../Page/PlayStation_3.md "wikilink"))
29. [无尽传奇](../Page/无尽传奇.md "wikilink") (2011,
    [南梦宫万代](../Page/南梦宫万代游戏.md "wikilink")，PlayStation
    3)
30. [FIFA 12](../Page/FIFA_12.md "wikilink") (2011, [Electronic
    Arts](../Page/Electronic_Arts.md "wikilink")，Xbox360 / PlayStation
    3)
31. [生化危機：啟示](../Page/生化危機：啟示.md "wikilink")（2012，[Capcom](../Page/Capcom.md "wikilink")，Nintendo
    3DS）
32. [铁拳 Tag Tournament 2](../Page/铁拳_Tag_Tournament_2.md "wikilink")
    (2012, [南梦宫万代](../Page/南梦宫万代游戏.md "wikilink")，Xbox360 / PlayStation
    3)
33. [生化危機6](../Page/生化危機6.md "wikilink") (2012,
    [Capcom](../Page/Capcom.md "wikilink")，Xbox360 / PlayStation 3)
34. [来吧！动物之森](../Page/来吧！动物之森.md "wikilink")
    （2012，[任天堂](../Page/任天堂.md "wikilink")，[Nintendo
    3DS](../Page/Nintendo_3DS.md "wikilink")）
35. [潜龙谍影崛起：再复仇](../Page/潜龙谍影崛起：再复仇.md "wikilink")（2013，[科乐美](../Page/科乐美.md "wikilink")，Xbox360
    / PlayStation 3）
36. [火影忍者：究极忍者风暴3](../Page/火影忍者：究极忍者风暴3.md "wikilink")（2013，[南梦宫万代](../Page/南梦宫万代游戏.md "wikilink")，Xbox360
    / PlayStation 3）
37. [神奇101](../Page/神奇101.md "wikilink")
    （2013，[任天堂](../Page/任天堂.md "wikilink")，[Wii
    U](../Page/Wii_U.md "wikilink")）
38. [最终幻想XIV：重生之境](../Page/最终幻想XIV：重生之境.md "wikilink")（2013，[史克威尔艾尼克斯](../Page/史克威尔艾尼克斯.md "wikilink")，[PlayStation
    3](../Page/PlayStation_3.md "wikilink")）
39. [精灵宝可梦 X·Y](../Page/精灵宝可梦_X·Y.md "wikilink")
    （2013，[精灵宝可梦公司](../Page/精灵宝可梦公司.md "wikilink")，[Nintendo
    3DS](../Page/Nintendo_3DS.md "wikilink")）
40. [如龙
    维新！](../Page/如龙_维新！.md "wikilink")（2014，[SEGA](../Page/SEGA.md "wikilink")，[PlayStation
    4](../Page/PlayStation_4.md "wikilink")）
41. [神秘海域4
    盗贼末路](../Page/神秘海域4_盗贼末路.md "wikilink")（2016，[索尼互动娱乐](../Page/索尼互动娱乐.md "wikilink")，PlayStation
    4）
42. [勇者斗恶龙
    英雄2](../Page/勇者斗恶龙_英雄2.md "wikilink")（2016，[史克威尔艾尼克斯](../Page/史克威尔艾尼克斯.md "wikilink")，PlayStation
    4/ PlayStation 3/ PlayStation Vita）
43. [女神异闻录5](../Page/女神异闻录5.md "wikilink")（2016，[Atlus](../Page/Atlus.md "wikilink")，[PlayStation
    4](../Page/PlayStation_4.md "wikilink") / [PlayStation
    3](../Page/PlayStation_3.md "wikilink")）
44. [如龙6
    命之诗](../Page/如龙6_命之诗.md "wikilink")（2016，[SEGA](../Page/SEGA.md "wikilink")，[PlayStation
    4](../Page/PlayStation_4.md "wikilink")）
45. [尼尔：机械纪元](../Page/尼尔：机械纪元.md "wikilink")
    (2017，[史克威尔艾尼克斯](../Page/史克威尔艾尼克斯.md "wikilink")，[PlayStation
    4](../Page/PlayStation_4.md "wikilink") /
    [Windows](../Page/Windows.md "wikilink") )
46. [超級瑪利歐 奧德賽](../Page/超級瑪利歐_奧德賽.md "wikilink")
    (2017，[任天堂](../Page/任天堂.md "wikilink")，[Nintendo
    Switch](../Page/Nintendo_Switch.md "wikilink")）
47. [魔物獵人 世界](../Page/魔物獵人_世界.md "wikilink")
    (2018，[Capcom](../Page/Capcom.md "wikilink")，[PlayStation
    4](../Page/PlayStation_4.md "wikilink") / [Xbox
    One](../Page/Xbox_One.md "wikilink") /
    [Windows](../Page/Windows.md "wikilink"))
48. [碧血狂殺2](../Page/碧血狂殺2.md "wikilink") (2018，[Rockstar
    Games](../Page/Rockstar_Games.md "wikilink")，[PlayStation
    4](../Page/PlayStation_4.md "wikilink") / [Xbox
    One](../Page/Xbox_One.md "wikilink"))

## 參考

## 外部連結

  -
[Category:1986年建立的出版物](../Category/1986年建立的出版物.md "wikilink")
[Category:ENTERBRAIN](../Category/ENTERBRAIN.md "wikilink")
[Category:日本電子遊戲雜誌](../Category/日本電子遊戲雜誌.md "wikilink")
[Category:日本週刊](../Category/日本週刊.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.
10.
11.
12.
13. PlayStation 3版為38分。