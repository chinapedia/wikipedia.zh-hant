**拉丁语**（，），可追溯至[羅馬帝國的](../Page/羅馬帝國.md "wikilink")[奧古斯都皇帝時期](../Page/屋大维.md "wikilink")，當時所使用的書面語稱為「[古典拉丁語](../Page/古典拉丁語.md "wikilink")」，屬於[印欧语系](../Page/印欧语系.md "wikilink")[意大利語族](../Page/意大利語族.md "wikilink")。拉丁語是最早在[拉提姆地区](../Page/拉齐奥_\(古代罗马\).md "wikilink")（今意大利的[拉齐奥区](../Page/拉齐奥.md "wikilink")）和[罗马帝国使用的語言之一](../Page/罗马帝国.md "wikilink")。虽然现在拉丁语通常被认为是一种[死语言](../Page/死语言.md "wikilink")，但仍有少数[基督宗教神职人员及学者可以流利地使用拉丁语](../Page/基督宗教.md "wikilink")。[罗马天主教传统上用拉丁语作为正式](../Page/天主教.md "wikilink")[會議的语言和](../Page/會議.md "wikilink")[礼拜仪式用的语言](../Page/礼拜.md "wikilink")，此外，许多[西方国家的大学仍然提供有关拉丁语的课程](../Page/西方国家.md "wikilink")。\[1\]

在[英语和其他西方语言创造新词的过程中](../Page/英语.md "wikilink")，拉丁语一直得以使用。拉丁语及其后代[罗曼诸语是意大利语族中仅存的一支](../Page/罗曼语族.md "wikilink")。通过对早期[意大利遗留文献的研究](../Page/意大利.md "wikilink")，可以证实其他意大利语族分支的存在，之后这些分支在[罗马共和国时期逐步被拉丁语同化](../Page/罗马共和国.md "wikilink")。拉丁语的亲属语言包括[法利斯克语](../Page/法利斯克语.md "wikilink")、[奥斯坎语和](../Page/奥斯坎语.md "wikilink")[翁布里亚语](../Page/翁布里亚语.md "wikilink")。\[2\]但是，[威尼托语可能是一个例外](../Page/威尼托语.md "wikilink")。在罗马时代，作为[威尼斯居民的语言](../Page/威尼斯.md "wikilink")，[威尼托语得以和拉丁语并列使用](../Page/威尼托语.md "wikilink")。

拉丁语是一种高度[屈折的语言](../Page/屈折语.md "wikilink")。它有三种不同的[性](../Page/性_\(语法\).md "wikilink")，名词有七[格](../Page/格_\(语法\).md "wikilink")，动词有四种词性变化、六种[时态](../Page/时态.md "wikilink")、六种[人称](../Page/人称.md "wikilink")、三种[语气](../Page/语气.md "wikilink")、三种[语态](../Page/语态.md "wikilink")、两种[体](../Page/体_\(语法\).md "wikilink")、两个[数](../Page/数_\(语法\).md "wikilink")。七格当中有一格是[方位格](../Page/方位格.md "wikilink")，通常只和方位名词一起使用。呼格与主格高度相似，因此拉丁语一般只有五个不同的格。不同的作者在行文中可能使用五到七种格。形容词与副词类似，按照格、性、数曲折变化。虽然拉丁语中有指示代词指代远近，它却没有冠词。后来拉丁语通过不同的方式简化词尾的曲折变化，形成了[罗曼语族](../Page/罗曼语族.md "wikilink")。

拉丁语與[希腊语同為影響](../Page/希腊语.md "wikilink")[歐美](../Page/歐美.md "wikilink")[學術與](../Page/學術.md "wikilink")[宗教最深的](../Page/宗教.md "wikilink")[语言](../Page/语言.md "wikilink")。在[中世纪](../Page/中世纪.md "wikilink")，拉丁语是当时[欧洲不同国家交流的](../Page/欧洲.md "wikilink")[媒介语](../Page/媒介语.md "wikilink")，也是研究[科学](../Page/科学.md "wikilink")、[哲学和](../Page/哲学.md "wikilink")[神學所必须的语言](../Page/神學.md "wikilink")。直到近代，通晓拉丁语曾是研究任何[人文学科教育的前提条件](../Page/人文学科.md "wikilink")；直到20世纪，拉丁语的研究才逐渐衰落，重点转移到对當代语言的研究。\[3\]

## 異名

拉丁语的[中文名称在](../Page/中文.md "wikilink")[明](../Page/明朝.md "wikilink")、[清](../Page/清.md "wikilink")[文献中曾有](../Page/文献.md "wikilink")40多种异译，如“大西字”、“红毛字”、“番字”、“喇提诺”、“腊底诺”、“辣第诺”、“拉替努”、“赖丁”、“罗典”等。\[4\]

## 在语言史中的定位

在英语中，“I”（我\[主格\]）、“me”（我\[宾格\]）、“is”（是）、“mother”（母亲）、“brother”（兄弟）、“ten”（十），这些词实际上是以某种方式被[欧洲人和](../Page/欧洲人.md "wikilink")[亚洲人已经说了上千年的词](../Page/亚洲人.md "wikilink")。

目前为止，并不清楚这些词到底有多么古老。虽然它们的拼写和发音因时空的差异而有所不同，但这些人类思想符号的基本要素却能够越过这样的时空跨度，一直留存到今天。从下面这个简表就可以看出这一点。\[5\]

<table>
<thead>
<tr class="header">
<th><p><a href="../Page/梵语.md" title="wikilink">梵语</a></p></th>
<th><p>希腊语</p></th>
<th><p>拉丁语</p></th>
<th><p><a href="../Page/盎格鲁-撒克逊语.md" title="wikilink">盎格鲁-撒克逊语</a></p></th>
<th><p><a href="../Page/古愛爾蘭語.md" title="wikilink">古爱尔兰语</a></p></th>
<th><p><a href="../Page/立陶宛语.md" title="wikilink">立陶宛语</a></p></th>
<th><p><a href="../Page/俄语.md" title="wikilink">俄语</a></p></th>
<th><p><a href="../Page/英语.md" title="wikilink">英语</a></p></th>
<th><p>現代漢語翻譯</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>aham</p></td>
<td><p>ἐγώ<br />
(egṓ)</p></td>
<td><p>ego</p></td>
<td><p>ie</p></td>
<td></td>
<td><p>aš</p></td>
<td><p>я<br />
(ya)</p></td>
<td><p>I</p></td>
<td><p>我<br />
(主格)</p></td>
</tr>
<tr class="even">
<td><p>mā</p></td>
<td><p>με<br />
(me)</p></td>
<td><p>mē</p></td>
<td><p>mē</p></td>
<td><p>mé</p></td>
<td><p>manè</p></td>
<td><p>меня<br />
(menya)</p></td>
<td><p>me</p></td>
<td><p>我<br />
(賓格)</p></td>
</tr>
<tr class="odd">
<td><p>asti</p></td>
<td><p>ἐστί<br />
(esti)</p></td>
<td><p>est</p></td>
<td><p>is</p></td>
<td><p>is</p></td>
<td><p>esti</p></td>
<td><p>есть<br />
(yest')</p></td>
<td><p>is</p></td>
<td><p>是</p></td>
</tr>
<tr class="even">
<td><p>mātar-</p></td>
<td><p>μήτηρ<br />
(mētēr)</p></td>
<td><p>māter</p></td>
<td><p>mōdor</p></td>
<td><p>máthir</p></td>
<td><p>motė</p></td>
<td><p>мать<br />
(mat')</p></td>
<td><p>mother</p></td>
<td><p>母亲</p></td>
</tr>
<tr class="odd">
<td><p>bhrātar-</p></td>
<td><p>φράτηρ<br />
(phrātēr)</p></td>
<td><p>frāter</p></td>
<td><p>brōthor</p></td>
<td><p>bráthir</p></td>
<td><p>broterėlis</p></td>
<td><p>брат<br />
(brat)</p></td>
<td><p>brother</p></td>
<td><p>兄弟</p></td>
</tr>
<tr class="even">
<td><p>daśam</p></td>
<td><p>δέκα<br />
(déka)</p></td>
<td><p>decem</p></td>
<td><p>tīen</p></td>
<td><p>deich</p></td>
<td><p>dešimitis</p></td>
<td><p>десять<br />
(desyat')</p></td>
<td><p>ten</p></td>
<td><p>十</p></td>
</tr>
</tbody>
</table>

表中的这些[同源詞相互之间是有关联的](../Page/同源词.md "wikilink")。然而在它们当中，除英语是从盎格鲁-撒克逊语派生出来的以外，没有任何一种语言是直接从另一种语言派生出来的。其他语言都可以回溯到一种共同的[祖語](../Page/祖語.md "wikilink")。这种语言现已消亡，但根据留存下来的语言证据，可以推断它的存在。

所有这些“亲戚语言”或同源语的源始语（现己消亡）一般称为[原始印欧语](../Page/原始印歐語.md "wikilink")，因为它的派生语既出现在[印度附近](../Page/印度.md "wikilink")（梵语、[伊朗语](../Page/波斯语.md "wikilink")），也出现在欧洲（希腊语、拉丁语、[日耳曼语](../Page/日耳曼语族.md "wikilink")、[凯尔特语](../Page/凯尔特语族.md "wikilink")、[斯拉夫语](../Page/斯拉夫语族.md "wikilink")、[波罗的语](../Page/波罗的语族.md "wikilink")）。据文献考证，这些语言中最古老的是梵语、伊朗语、希腊语和拉丁语，这些文献均可追溯到公元前。\[6\]

[英语是从与拉丁语同源的](../Page/英语.md "wikilink")[盎格鲁撒克逊语中派生出来的](../Page/盎格鲁撒克逊语.md "wikilink")。盎格鲁撒克逊语早先从拉丁语中借用过一些词汇。

公元7世纪时，又有更多的拉丁词被吸收进来，这主要是由于[坎特伯雷的圣奥古斯丁](../Page/聖奧古斯丁_\(坎特伯雷\).md "wikilink")（不是那个著名的希波的圣奥古斯丁）著作的影响，他曾受[教皇格列高利差遣](../Page/教宗额我略一世.md "wikilink")，力图使盎格鲁人[皈依](../Page/皈依.md "wikilink")[基督教](../Page/基督教.md "wikilink")。

在[征服者威廉于](../Page/威廉一世_\(英格兰\).md "wikilink")1066年統治[英格蘭之后](../Page/英格蘭.md "wikilink")，[诺曼法语成为上流语言](../Page/諾曼語.md "wikilink")，盎格鲁-撒克逊语被看作是战败者和农奴讲的劣等语言。盎格鲁-撒克逊语不再是文学语言，而是成了日常生活中的土话。

然而，又过了大约两个世纪，随着[诺曼人的后裔最终与当地](../Page/諾曼人.md "wikilink")[英国人融合](../Page/英国人.md "wikilink")，盎格鲁-撒克逊语又重新得到了肯定。但是由于自身的贫乏，它在成为文学语言之前，不得不在文学、思想和文化上借用数百个[法语词](../Page/法语.md "wikilink")。

到了13、14世纪，随着这种借用的不断增多，[中古英语慢慢发展出来](../Page/中古英语.md "wikilink")，其代表人物便是于1400年去世的[杰弗里·乔叟](../Page/杰弗里·乔叟.md "wikilink")。除了这些含有拉丁词根的法语词被吸收进来，还有一些词是直接从拉丁语借过来的。

到了16、17世纪，[文艺复兴重新唤起了人们对于古典作品的兴趣](../Page/文艺复兴.md "wikilink")，从而使这一过程得到加强。从那以后，拉丁语一直是许多新词特别是科学语汇的来源。

| 英语     | 中文对照     | 拉丁语同源词         | 英语派生词     | 現代漢語翻譯     |
| ------ | -------- | -------------- | --------- | ---------- |
| mother | 母亲       | māter          | maternal  | 母亲的；母性的    |
| two    | 二        | duo            | dual/duel | 双的；双倍的；双重的 |
| tooth  | 牙齿       | dēns （词干dent-） | dental    | 牙齿的;牙科的    |
| foot   | 脚（单数）    | pēs （词干ped-）   | pedal     | 踏板         |
| heart  | 心脏       | cor （词干cord-）  | cordial   | 衷心的；真挚的    |
| bear   | 负担、生（小孩） | ferō           | fertile   | 肥沃的、多产的    |

**英语中由拉丁语派生词汇举例\[7\]**

既然英语经由[盎格鲁-撒克逊语而与拉丁语同源而且英语从拉丁语中直接或间接地借用了许多词汇](../Page/盎格鲁-撒克逊语.md "wikilink")，所以很容易用英语的词汇来说明同源和派生现象。

例如，“brother”（兄弟）一词与拉丁词 “frāter”（兄弟）同源，而 “fraternal”（兄弟的）显然是由
“frāter”派生出来的。\[8\]

## 历史

[Italy_Regions_Latium_Map.png](https://zh.wikipedia.org/wiki/File:Italy_Regions_Latium_Map.png "fig:Italy_Regions_Latium_Map.png")

拉丁語原本是[意大利](../Page/意大利.md "wikilink")[亚平宁半岛中西部](../Page/意大利半島.md "wikilink")、[台伯河下游](../Page/台伯河.md "wikilink")[拉提姆](../Page/拉齐奥_\(古代罗马\).md "wikilink")（），一个称为拉丁人的部落的[方言](../Page/方言.md "wikilink")。

公元前8世纪，[罗马](../Page/罗马.md "wikilink")（）成为拉提姆地区的中心，于是周围各部落也自称罗马人()。罗马建国后，经历了[王政时期](../Page/罗马王政时代.md "wikilink")（公元前7世纪
- 前510年）、[共和时期](../Page/罗马共和国.md "wikilink")（公元前510年 -
前27年）和[帝国时期](../Page/羅馬帝國.md "wikilink")（公元前27年\~公元476年）。随着国家版图的扩大，[拉丁语不仅在](../Page/拉丁语.md "wikilink")[亚平宁半岛取得统治地位](../Page/亚平宁半岛.md "wikilink")，淘汰了其他亲属语言，而且作为官方语言推广到罗马帝国的各个行省——西起[伊比利亚半岛](../Page/伊比利亚半岛.md "wikilink")，东抵[黑海之滨](../Page/黑海.md "wikilink")，北自[布列塔尼半岛](../Page/布列塔尼半岛.md "wikilink")，南达[非洲](../Page/非洲.md "wikilink")[地中海沿岸](../Page/地中海.md "wikilink")。

拉丁语的历史发展可大致分为以下几个时期：

### 史前拉丁語

史前拉丁語（）（公元前250年以前），最古文献是公元前6世纪的「普雷内斯太金饰针」 上的4词铭文：

> MANIOS MED FHEFHAKED NUMASIOI

相当于古典拉丁语的「Manius me fecit Numerio」，意味「此饰针乃马尼乌斯为努梅利乌所制」。\[9\]

### 古体拉丁语

古体拉丁语（）（公元前250年 -
前90年），罗马自公元前4世纪开始向外扩张，至公元前1世纪已使整个意大利拉丁化。不过，在此期间拉丁语尚没有统一的规范。公元前3\~2世纪，古拉丁语的代表人物包括詩人[恩紐斯以及深受](../Page/恩紐斯.md "wikilink")[希腊影响的喜剧作家](../Page/希腊.md "wikilink")[普劳图斯和](../Page/普劳图斯.md "wikilink")[泰伦提乌斯](../Page/泰伦提乌斯.md "wikilink")。

  - [恩紐斯](../Page/恩紐斯.md "wikilink")（）（前239年 -
    大約前169年）。著有史詩《編年史》，被譽為羅馬詩歌之父。
  - [普劳图斯](../Page/普劳图斯.md "wikilink")（）（大约前254年 -
    前184年）。著有《商人》、《凶宅》等21种诗体喜剧，是现存最早的拉丁语文学作品。
  - [泰伦提乌斯](../Page/泰伦提乌斯.md "wikilink")（）（前185年 -
    前161年）。著有《婆母》、《两兄弟》、《安德罗斯女子》等多部诗剧传世。\[10\]

### 古典拉丁语

[GiorcesBardo42.jpg](https://zh.wikipedia.org/wiki/File:GiorcesBardo42.jpg "fig:GiorcesBardo42.jpg")和[墨尔波墨涅之间](../Page/墨尔波墨涅.md "wikilink")。来自[哈德卢密塔姆](../Page/哈德卢密塔姆.md "wikilink")（今[突尼斯的](../Page/突尼斯.md "wikilink")[苏斯](../Page/苏斯.md "wikilink")）。维吉尔被誉为古罗马最伟大诗人，他的《埃涅阿斯记》长达十二册，是代表着罗马帝国的巨著。\[11\]\]\]

羅馬帝國的[奧古斯都皇帝時期使用的書面語稱為](../Page/屋大维.md "wikilink")「[古典拉丁語](../Page/古典拉丁語.md "wikilink")」（）（）（前80年
\~
公元2世纪，即共和晚期至帝国初期）大致相当于拉丁语文学史上的[黄金时代](../Page/黄金时代.md "wikilink")（前90年\~公元14年，即[屋大维逝世時](../Page/屋大维.md "wikilink")）和[白银时代](../Page/白银时代.md "wikilink")（14年\~117年，即[图拉真逝世時](../Page/图拉真.md "wikilink")；或者
180年，即[奥勒留逝世時](../Page/奥勒留.md "wikilink")）。到[黄金时代](../Page/黄金时代.md "wikilink")，拉丁语已经形成有统一规范的标准语，词汇丰富，句法完善，表现力强。
[黄金时代拉丁语的主要代表作者有](../Page/黄金时代.md "wikilink")：

  - [西塞罗](../Page/西塞罗.md "wikilink")（）（前106年 -
    前43年），曾任执政官，作家，演说家。著有《论演说家》、《[论共和国](../Page/论共和国.md "wikilink")》、《反腓力辞》（14篇演说）、长诗《[我的执政](../Page/我的执政.md "wikilink")》和《[我的时代](../Page/我的时代.md "wikilink")》，另遗下书信900多封。
  - [凯撒](../Page/凯撒.md "wikilink")（）（前102年或100年 -
    前44年），曾任执政官、军事家、演说家，散文作家。著有《[高卢战记](../Page/高盧戰記.md "wikilink")》（）等，其著作和演说皆有文学价值。
  - [尼波斯](../Page/康涅利乌斯·尼波斯.md "wikilink")（)（前99年 -
    前24年），史学家，傳記文學家。著有《名人传》、《年代学》、《轶事集》等。
  - [卢克莱修](../Page/卢克莱修.md "wikilink")（，前96年 - 前55年），著有长诗《物性论》（）和许多哲学论文。
  - [萨卢斯特](../Page/萨卢斯特.md "wikilink")（，前86年 -
    前35年或34年），政治家，历史学家。著有《喀提林战争》、《朱古达战争》、《历史》（仅存残篇）。
  - [卡图卢斯](../Page/卡图卢斯.md "wikilink")（，大约前84年 -
    大约前54年），抒情诗人。作品於2-13世纪失传，手稿在13世纪为人发现，流传下来的有116首诗。
  - [维吉尔](../Page/维吉尔.md "wikilink")（，前70年 -
    前19年），诗人。著有民族史诗《[埃涅阿斯记](../Page/埃涅阿斯纪.md "wikilink")》（12卷）和牧歌多首。
  - [贺拉斯](../Page/贺拉斯.md "wikilink")（）（前70年 -
    前19年），诗人。著有《讽刺诗集》、《长短句集》、《歌集》（4卷）、《书札》（多卷，其中第3卷通称《诗艺》）。
  - [李维](../Page/李维.md "wikilink")（Titus Livius）（前59年 -
    公元17年），历史学家。著有《罗马史》（142卷）。
  - [奥维德](../Page/奥维德.md "wikilink")（Publius Ovidius Naso）（前43年 -
    公元17年），诗人。著有《爱情诗》（5卷）、《变形记》（15卷）、《岁时记》（12卷）等。

这些代表人物反映的古典拉丁语，成为后世欧洲学校的必修科目和学术界研究的主要对象。\[12\]

[Gaius_Cornelius_Tacitus.jpg](https://zh.wikipedia.org/wiki/File:Gaius_Cornelius_Tacitus.jpg "fig:Gaius_Cornelius_Tacitus.jpg")

### 古典后拉丁语

古典后拉丁语（14年-200年）包括文学史上的[白银时代](../Page/白银时代.md "wikilink")（14年-117年或118年）是早期罗马帝国的语言。它基本上仍遵循[黄金时代的语法规范](../Page/黄金时代.md "wikilink")，但句法结构有所革新，散文中吸收了某些诗歌成分，修辞手法更加丰富。

[白银时代拉丁语的代表作者有](../Page/白银时代.md "wikilink")：

  - [塞内卡](../Page/塞内卡.md "wikilink")（，约前4年 -
    公元65年）哲学家，雄辩家，悲剧作家。著有哲学论文《安慰》、《道德论》等，悲剧《特洛伊妇人》、《奥狄浦斯》以及政治讽刺文章。
  - [昆提良](../Page/昆提利安.md "wikilink")（，35年 -
    95年）修辞学家，教育家。有巨著《雄辩家的培训》以及《长篇雄辩术》、《短篇雄辩术》。
  - [马提亚尔](../Page/马提亚尔.md "wikilink")（，38年或41年 -
    104年）铭辞作家。写了1500多首铭辞，从多方面反映了当时的社会情况。许多诗句后来成为警句。
  - [塔吉突士](../Page/塔西佗.md "wikilink")（，名亦可能為Gaius，约55年 -
    117年）历史学家，曾任执政官。著有《日耳曼尼亚》、《历史》（12卷）、《[编年史](../Page/编年史.md "wikilink")》（18卷，今仍存若干残篇）。
  - [尤维纳利斯](../Page/尤维纳利斯.md "wikilink")（，55年或60年 - 约127年）诗人。著有《讽刺诗》。
  - [老普林尼](../Page/老普林尼.md "wikilink")（，23年 -
    79年）科学家，散文作家。曾写过7部作品，但现存的仅有《博物志》（37卷）。
  - [小普林尼](../Page/小普林尼.md "wikilink")（，61年或62年 -
    113年）老普林尼的养子，曾任执政官，遗有9卷信札选集，反映罗马全盛时期的状况，具有社会价值。
  - [阿普列尤斯](../Page/阿普列尤斯.md "wikilink")（，约124年 -
    170年以后）哲学家，散文作家。因小说《[金驢記](../Page/金驢記.md "wikilink")》（自称《变形记》）成名，另著有《佛罗里达》此外，写过数卷论述[柏拉图的哲学著作](../Page/柏拉图.md "wikilink")（多已遗失）。\[13\]

### 晚期拉丁语

[Simone_Martini_003.jpg](https://zh.wikipedia.org/wiki/File:Simone_Martini_003.jpg "fig:Simone_Martini_003.jpg")\]\]

[晚期拉丁语](../Page/晚期拉丁语.md "wikilink")（公元3世紀至6世紀，即羅馬帝國後期至西羅馬帝國覆沒之後一百年這段時期）,2-6世紀民眾所使用的口語則稱為「[通俗拉丁语](../Page/通俗拉丁语.md "wikilink")」（）。主要代表人物是一些基督教神职人员。这些人都受过良好教育，由于传教的需要，比较了解人民大众的口语，从而对[通俗拉丁语采取开明的态度](../Page/通俗拉丁语.md "wikilink")。

早期的重要人物有：

[特尔图良](../Page/特土良.md "wikilink")（Tertullianus，160年\~220年以后）、[西普里安](../Page/居普良.md "wikilink")（Cyprianus,St.，200年?
- 258年）、[拉克坦提乌斯](../Page/拉克坦提乌斯.md "wikilink")（Lactantius，240年 -
326年）、[安布罗斯](../Page/安波罗修.md "wikilink")（Ambrosius,St.，339年? -
397年)等

影响最大的人物是：

[哲罗姆](../Page/耶柔米.md "wikilink")（Jerome,St.，347年 -
419年或420年）学识渊博，在383年\~405年期间，将[希伯来文文本](../Page/希伯来语.md "wikilink")《[旧约](../Page/旧约圣经.md "wikilink")》和希腊文本《[新约](../Page/新约.md "wikilink")》译成接近大众口语的拉丁语。他宣称：“为使读者容易理解，我特地采用普通人所说的口语。”这个译本后世习称Vulgate，即[通俗拉丁文译本](../Page/武加大译本.md "wikilink")，对[中世纪影响很大](../Page/中世纪.md "wikilink")。[Calligraphy.malmesbury.bible.arp.jpg](https://zh.wikipedia.org/wiki/File:Calligraphy.malmesbury.bible.arp.jpg "fig:Calligraphy.malmesbury.bible.arp.jpg")写成的[圣经可以看出](../Page/聖經.md "wikilink")，这个古罗马的语言对后世文化（主要指欧洲文化）产生了深远的影响\]\][圣奥古斯丁](../Page/圣奥古斯丁.md "wikilink")（Augustine,St.，354年
-
430年）著有《忏悔录》、《[论上帝之城](../Page/上帝之城.md "wikilink")》等，语言接近大众口语（）。他声称，只要蛮人是基督徒，即使他们把罗马征服，也无关紧要。\[14\]

### 衰落

罗马帝国衰落时期的拉丁语发生了变化。6至8世纪的通俗拉丁语变化迅速。变格词尾大部分消失，介词和助动词使用得日益广泛，虚拟式常与陈述式混用。结果古典拉丁语渐渐与通俗拉丁语脱节，终于变成一種[書面語](../Page/書面語.md "wikilink")。\[15\]而中世纪时，通俗拉丁语的地域变体（方言）则在此后数百年中逐渐分化，又衍生出了若干独立的「[罗曼诸语](../Page/罗曼语族.md "wikilink")」。其中包括[東部羅曼語](../Page/東部羅曼語.md "wikilink")：[羅馬尼亞語](../Page/羅馬尼亞語.md "wikilink")，[达尔马提亚语](../Page/达尔马提亚语.md "wikilink")（1898年已灭亡）；[薩丁語](../Page/薩丁尼亞語.md "wikilink")；[意大利語](../Page/意大利语.md "wikilink")；[西部羅曼語](../Page/西部羅曼語.md "wikilink")：[法語](../Page/法语.md "wikilink")、[罗曼什语](../Page/罗曼什语.md "wikilink")、[加泰罗尼亚语](../Page/加泰罗尼亚语.md "wikilink")、[奥克语](../Page/奥克语.md "wikilink")（包括有名的[普罗旺斯方言](../Page/奥克语.md "wikilink")），[西班牙語](../Page/西班牙语.md "wikilink")、[葡萄牙語](../Page/葡萄牙語.md "wikilink")、[加里西亚语](../Page/加里西亞語.md "wikilink")，[阿拉贡语](../Page/阿拉贡语.md "wikilink")。十六世紀後[西班牙與](../Page/西班牙.md "wikilink")[葡萄牙勢力擴張到整個](../Page/葡萄牙.md "wikilink")[中美洲和](../Page/中美洲.md "wikilink")[南美洲](../Page/南美洲.md "wikilink")，因此中南美洲又稱「[拉丁美洲](../Page/拉丁美洲.md "wikilink")」。\[16\]

拉丁語向罗曼语族諸語言演化的過程中，失去了很多单词的语法变化词尾。特别是名词的变格词尾，在很多[羅曼語中已经完全丧失](../Page/罗曼语族.md "wikilink")（一些代词除外）。但名词变格在罗马尼亚语中仍然有所保留。

### 后续影响

[罗曼诸语在世界中的分布
](https://zh.wikipedia.org/wiki/File:Map-Romance_Language_World.png "fig:罗曼诸语在世界中的分布  ")
[Former_logo_of_the_European_Council_and_Council_of_the_European_Union_(2009).svg](https://zh.wikipedia.org/wiki/File:Former_logo_of_the_European_Council_and_Council_of_the_European_Union_\(2009\).svg "fig:Former_logo_of_the_European_Council_and_Council_of_the_European_Union_(2009).svg")为了在语言方面达成妥协并显示各国共同的文化继承，选择拉丁语作为其部分机构标识所使用的语言。（图中为[欧盟理事会的标识](../Page/欧盟理事会.md "wikilink")：Consilium）\]\]
虽然拉丁语作为口语消亡了，但它作为欧洲社会的书面共同语继续使用了1000年左右。[基督教在](../Page/基督教.md "wikilink")[歐洲兴起後](../Page/欧洲.md "wikilink")，拉丁語的影響力进一步加深。從[中世纪至](../Page/中世纪.md "wikilink")20世纪初葉，拉丁语不仅是[天主教的礼仪语言和公用語](../Page/天主教.md "wikilink")，而且是学术著作、学术论文乃至文学作品的写作语言。许多世界著名学者都曾经用拉丁语著述，如：[波兰的](../Page/波兰.md "wikilink")[哥白尼](../Page/哥白尼.md "wikilink")、[法国的](../Page/法国.md "wikilink")[笛卡尔](../Page/勒内·笛卡儿.md "wikilink")、[英国的](../Page/英国.md "wikilink")[培根和](../Page/法蘭西斯·培根.md "wikilink")[牛顿](../Page/艾薩克·牛頓.md "wikilink")、[德国的](../Page/德国.md "wikilink")[莱布尼茨和](../Page/莱布尼茨.md "wikilink")[高斯](../Page/高斯.md "wikilink")、[荷兰的](../Page/荷兰.md "wikilink")[斯宾诺莎](../Page/斯宾诺莎.md "wikilink")、[瑞士的](../Page/瑞士.md "wikilink")[欧拉](../Page/欧拉.md "wikilink")、[瑞典的](../Page/瑞典.md "wikilink")[林奈等](../Page/林奈.md "wikilink")。\[17\]此外，拉丁语作为外交语言使用到18世纪，而[医学](../Page/医学.md "wikilink")、[生物学等领域里一些學術的詞彙或文章](../Page/生物学.md "wikilink")，例如[生物分類法的命名規則至今仍然得以广泛使用](../Page/生物分類法.md "wikilink")。

拉丁语在一定程度上也曾直接或间接地对[汉语施加过影响](../Page/汉语.md "wikilink")。由于英语中有50%到80%的单字来自拉丁语（越是学术性的文献，其拉丁语成分越高），许多拉丁语词通过英语进入了汉语。如汉语“卡”（英语：card）来自拉丁语的charta（最早来自希腊语）。[公共汽车的音译](../Page/公共汽车.md "wikilink")“巴士”（英语：bus）这个称呼来自19世纪的[法国](../Page/法国.md "wikilink")，当时法国人称公交车为omnibus（即拉丁语：“给一切人的”、“公用的”），这个词在今天的罗曼语族语言中仍存在，如西班牙文的公共汽车可用ómnibus。此外，一些[现代汉语中的说法虽然没有明显的](../Page/现代汉语.md "wikilink")“外国味”但实际上也和拉丁语有关系。比如“我热爱我的祖國”中“热爱”来自英文的ardently
love（来自拉丁语的ardenter
amare）。这种说法在[古代汉语中是找不到的](../Page/古代汉语.md "wikilink")。\[18\]

現在只有[梵蒂冈仍在使用拉丁語](../Page/梵蒂冈.md "wikilink")。拉丁语和[希腊语是](../Page/希腊语.md "wikilink")[西欧文化史上影响最大的两种语言](../Page/西欧.md "wikilink")。所以欧洲语言中都有大量拉丁语借词和利用拉丁语词（语）素创造而后广为通用的所谓[国际词](../Page/国际词.md "wikilink")。

<table>
<caption>'''拉丁语和其他衍生语言的相似之处[19] '''</caption>
<thead>
<tr class="header">
<th><p>拉丁语</p></th>
<th><p>意大利语</p></th>
<th><p>法语</p></th>
<th><p>西班牙语</p></th>
<th><p>罗马尼亚语</p></th>
<th><p>中文对照</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>朋友</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>眼睛</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>手</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>脚</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>时间</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>书</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>教师</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>儿子</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>人民，人们</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>数目</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>少</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>好的</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>好（副）</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>有</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>做</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>说</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>读</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><p>（介词）</p></td>
</tr>
</tbody>
</table>

## 字母和发音

### 字母

现在所使用[罗马字母已经有数百年的历史](../Page/拉丁字母.md "wikilink")。它们可以追溯到公元9世纪[法国](../Page/法国.md "wikilink")[图尔](../Page/图尔.md "wikilink")[圣马丁修道院修士们的](../Page/都尔的圣玛尔定.md "wikilink")[加洛林手书体](../Page/加洛林手书体.md "wikilink")，[加洛林文艺复兴使之得以完善](../Page/卡洛林文艺复兴.md "wikilink")，后来通过11、12世纪的手稿以及15世纪最古老的意大利印本流传下来。这些修士根据[半安色尔字体发展出了](../Page/半安色尔字体.md "wikilink")[小写字母](../Page/小寫字母.md "wikilink")，而半安色尔字体又源于[罗马帝国时期的](../Page/罗马帝国.md "wikilink")[安色尔字体和方块](../Page/安色尔字体.md "wikilink")[大写字](../Page/大写字.md "wikilink")。现今，人们习惯于将罗马字母与[希腊字母区分开来](../Page/希腊字母.md "wikilink")，但事实上，罗马人从[伊特鲁里亚人那里学会了书写](../Page/伊特拉斯坎文明.md "wikilink")，而伊特鲁里亚人又是从公元前8世纪定居于[那不勒斯附近的希腊移民那里学会了书写](../Page/那不勒斯.md "wikilink")。因此，罗马字母实际上只是希腊字母的一种形式。但在这方面，希腊字母是从[闪米特人的一支即](../Page/闪米特人.md "wikilink")[腓尼基人那里得到的](../Page/腓尼基.md "wikilink")。而再往前追溯，早期[闪米特人似乎又受到了](../Page/闪米特人.md "wikilink")[埃及象形文字的启发](../Page/圣书体.md "wikilink")。

罗马字母是[英文字母的源頭](../Page/英文字母.md "wikilink")，只是其中缺少字母j（long-I）和W（double-U），而且字母v最初既相当于[元音u](../Page/元音.md "wikilink")，也相当于[辅音w](../Page/辅音.md "wikilink")。虽然直到公元2世纪，圆底的u的形式才出现，但是为了方便，现在的版本大多v和u并用。字母k（發音同於c）比较罕用，仅在少数几个词中出现于a之前。到了[罗马共和国晚期](../Page/罗马共和国.md "wikilink")，字母y和z开始被用于拼写源于[希腊的词](../Page/希腊.md "wikilink")。

拉丁语的字母如下：

| 字母                                              | [A](../Page/A.md "wikilink") | [B](../Page/B.md "wikilink") | [C](../Page/C.md "wikilink") | [D](../Page/D.md "wikilink") | [E](../Page/E.md "wikilink") | [F](../Page/F.md "wikilink") | [G](../Page/G.md "wikilink") | [H](../Page/H.md "wikilink") |
| ----------------------------------------------- | ---------------------------- | ---------------------------- | ---------------------------- | ---------------------------- | ---------------------------- | ---------------------------- | ---------------------------- | ---------------------------- |
| 名称                                              | ā                            | bē                           | cē                           | dē                           | ē                            | ef                           | gē                           | hā                           |
| 发音（[IPA](../Page/Wikipedia:國際音標.md "wikilink")） | /aː/                         | /beː/                        | /keː/                        | /deː/                        | /eː/                         | /ef/                         | /geː/                        | /haː/                        |
|                                                 |                              |                              |                              |                              |                              |                              |                              |                              |
| 字母                                              | [I](../Page/I.md "wikilink") | [K](../Page/K.md "wikilink") | [L](../Page/L.md "wikilink") | [M](../Page/M.md "wikilink") | [N](../Page/N.md "wikilink") | [O](../Page/O.md "wikilink") | [P](../Page/P.md "wikilink") | [Q](../Page/Q.md "wikilink") |
| 名称                                              | ī                            | kā                           | el                           | em                           | en                           | ō                            | pē                           | qū                           |
| 发音（[IPA](../Page/Wikipedia:國際音標.md "wikilink")） | /iː/                         | /kaː/                        | /el/                         | /em/                         | /en/                         | /oː/                         | /peː/                        | /kʷuː/                       |
|                                                 |                              |                              |                              |                              |                              |                              |                              |                              |
| 字母                                              | [R](../Page/R.md "wikilink") | [S](../Page/S.md "wikilink") | [T](../Page/T.md "wikilink") | [V](../Page/V.md "wikilink") | [X](../Page/X.md "wikilink") | [Y](../Page/Y.md "wikilink") | [Z](../Page/Z.md "wikilink") |                              |
| 名称                                              | er                           | es                           | tē                           | ū                            | ex                           | ÿ Graeca                     | zēta                         |                              |
| 发音（[IPA](../Page/Wikipedia:國際音標.md "wikilink")） | /er/                         | /es/                         | /teː/                        | /uː/                         | /eks/                        | /y:greka/                    | /ˈzeːta/                     |                              |

**古典拉丁语字母表**

拉丁语并不使用。在[中世纪之前](../Page/中世纪.md "wikilink")，拉丁语以代替,
代替，亦未有[小写字母](../Page/小寫字母.md "wikilink")。

例如当[耶稣被钉死在十字架时](../Page/耶稣.md "wikilink")，用来形容他的拉丁文“拿撒勒之耶稣——[犹太人之王](../Page/犹太人.md "wikilink")”，现在一般都写为“”；但其实原文是“”。

### 发音

  -
    {| class="wikitable" align=center

\! colspan="2" rowspan="2" |   \! rowspan="2" |
[唇音](../Page/唇音.md "wikilink") \! rowspan="2" |
[齿音](../Page/齿音.md "wikilink") \! rowspan="2" |
[颚音](../Page/颚音.md "wikilink") \! colspan="2" |
[软颚音](../Page/舌根音.md "wikilink") \! rowspan="2" |
[喉音](../Page/聲門音.md "wikilink") |- style="font-size:x-small;" \! 普通
\! [唇音](../Page/唇音化.md "wikilink") |- style="text-align:center;" \!
style="text-align:left;" rowspan="3" | [塞音](../Page/塞音.md "wikilink") \!
style="font-size:x-small;text-align:left;" | 濁音 | b | d | | ɡ | |   |-
style="text-align:center;" \! style="font-size:x-small;text-align:left;"
| 清音 | p | t |   | k | kʷ | |- style="text-align:center;" \!
style="font-size:x-small;text-align:left;" | 送气 | pʰ | tʰ |   | kʰ | |
|- style="text-align:center;" \! style="text-align:left;" rowspan="2" |
[擦音](../Page/擦音.md "wikilink") \!
style="font-size:x-small;text-align:left;" | 濁音 |   | z | | | | |-
style="text-align:center;" \! style="font-size:x-small;text-align:left;"
| 清音 | f | s | | | | h |- style="text-align:center;" \!
style="text-align:left;" colspan="2" |
[鼻音](../Page/鼻音_\(辅音\).md "wikilink") | m | n |   | ŋ |   |
  |- style="text-align:center;" \! style="text-align:left;" colspan="2"
| [颤音](../Page/颤音.md "wikilink") | | r |   | |   |   |-
style="text-align:center;" \! style="text-align:left;" colspan="2" |
[近音](../Page/近音.md "wikilink") |   | l ɫ | j | | w | |}

发音拉丁语中元音的发音有长和短两种。[长元音的持续时间一般是](../Page/元音長度.md "wikilink")[短元音的两倍](../Page/元音長度.md "wikilink")
（就像音乐中的二分音符之于四分音符），许多教科书书中以长音符号标出（比如ā）；没有长音符号的元音就是短元音。长音符号所指示的发音区别往往对于含义极为关键（例如，Liber是名词，意为“书”，而Līber则是形容词，意为“自由的”）。\[20\]

| 长元音   | 发音方式                    | 短元音   | 发音方式                    | 双元音    | 发音方式                                  |
| ----- | ----------------------- | ----- | ----------------------- | ------ | ------------------------------------- |
| **ā** | \[aː\] **dās**，**cārā** | **a** | \[a\] **dat**，**casa**  | **ae** | \[aɪ\] **cārae**，**saepe**            |
| **ē** | \[eː\] **mē**，**sēdēs** | **e** | \[ɛ\] **et**,**sed**    | **au** | \[aʊ\]（即如普通话ao）**aut**,**laudō**      |
| **ī** | \[iː\] **hīc**，**sīca** | **i** | \[ɪ\] **hic**，**sicca** | **ei** | \[eɪ\] **deinde**                     |
| **ō** | \[oː\] **ōs**，**mōrēs** | **o** | \[ɔ\] **os**,**mora**   | **eu** | 如同拉丁语**e** **u**:**seu**（单音节）         |
| **ū** | \[uː\] **tū**，**sūmō**  | **u** | \[ʊ\] **tum**，**sum**   | **oe** | 如同oil中的oi:**coepit**,**proelium**     |
| **y** | 可长可短                    |       |                         | **ui** | 如同拉丁语**u** **i**，gooey：**huius**（单音节） |

**长元音、短元音及双元音**

|                                                 |
| ----------------------------------------------- |
| 为浊音b:**bibēbant**。可在s和t前发清化的p音:**urbs，obtineō** |
| **c**                                           |
| **g**                                           |
| **k**                                           |
| **h**                                           |
| **i**                                           |
| **m**                                           |
| **q**                                           |
| **r**                                           |
| **s**                                           |
| **t**                                           |
| **v**                                           |
| **x**                                           |
| **ch**                                          |
| **ph**                                          |
| **th**                                          |

*'拉丁语中的辅音 **（和英语基本相同） \!辅音||发音方式 |- |**b*'

## 拉丁语语法

拉丁语是个综合语，复杂的[词形变化体系构成了拉丁语语法的主要部分](../Page/屈折变化.md "wikilink")。这些变化通常使用在词尾添加后缀构成（外部屈折）或者变化词干的辅音或元音（内部屈折）。对于名词、形容词和代词，这种变化叫做“[变格](../Page/变格.md "wikilink")”（），对于动词，叫做“[变位](../Page/变位.md "wikilink")”（）。初學者一開始就得熟記相當數量的規則，常常招致挫折。[德国詩人](../Page/德国.md "wikilink")[海涅曾因不能熟記](../Page/海因里希·海涅.md "wikilink")，感歎「要是羅馬人得先學好拉丁文，他們大概沒剩多少時間征服世界」。\[21\]

### 名詞

一般每个名词都有六个[格的区别](../Page/格.md "wikilink")；更多的可以有七个，少的可能只有两个。名词的七个格是：

  - “[主格](../Page/主格.md "wikilink")”（\[casus\] nominativus;
    ）在通常情况下，罗马人用主格来指示一个限定动词的主语。
  - “[宾格](../Page/宾格.md "wikilink")”（\[casus\] accusativus;
    ）罗马人用宾格来表示动词动作的直接宾语，即受到动词动作直接影响的人或事物。它也可以用作某些介词（例如**ad**,to;**in**,into;**post**,after,
    behind）的宾语。
  - “[与格](../Page/与格.md "wikilink")”（\[casus\] dativus;
    ）罗马人用与格来表示间接受到动词动作影响的人或事物，类似于英语中的“（to）sb.”，即间接宾语，这是与格最常见的用法。在大多数情况下，与格的含义可以通过把to或for与名词连用来确定。
  - “[夺格](../Page/夺格.md "wikilink")”（\[casus\] ablativus;
    ）夺格有时又称离格或状语（adverbial）格，罗马人在用下述观念修饰或限制动词时会使用夺格：手段（或工具）、施事者、伴随、方式、地点、时间。罗马人有时把夺格与介词连用，有时不连用。翻译这个复杂的格没有什么简单规则可循。不过，如果有拉丁语介词（**ab**,by,from;**cum**，with;**dē**和**ex**,from;**in**,in,on）出现，问题就不大。一般而言，总可以把夺格与by,with,from,in,on,at等英语介词联系起来。
  - “[属格](../Page/属格.md "wikilink")”（\[casus\] genetivus, \[casus\]
    genitivus;
    ）当用一个名词来修饰另一个名词时，罗马人把修饰名词写成属格形式，类似于英语中的“'s”。属格通常表示属有关系，虽然除此之外还有其他一些用法，但属格的意思一般都可以用英语中的介词of来翻译。属格的拉丁语名词通常位于它所修饰的名词之后。
  - “[方位格](../Page/方位格.md "wikilink")”（\[casus\]
    locativus)（用于一些特定的词来表示方位）\[22\]
  - “[呼格](../Page/呼格.md "wikilink")”（\[casus\] vocativus;
    ）罗马人用呼格（有时与感叹词**Ō**连用）来直接呼唤（**vocāre**,to
    call）某人或某物。例如，（**Ō**）**Caesar**,（O）Caesar;**Ō fortūna**,O
    fortune。在现代的标点符号中，呼语（或直接称呼的名词）用逗号与句中其余部分隔开。在以**-us**结尾的第二变格法名词和形容词的单数情况下，呼格的拼写才与主格不同，所以通常不列入词形变化表。

因为[格变化已经表达了拉丁语的名词动词之间的语法关系](../Page/格变化.md "wikilink")，因此拉丁语的词序高度自由，并不遵守主-谓-宾之類的順序。例如：「他愛錢」，此句在[中文](../Page/汉语.md "wikilink")、[英文](../Page/英语.md "wikilink")、[法文等現今主要語言中](../Page/法语.md "wikilink")，皆僅有一种[语序](../Page/语序.md "wikilink")，即[主语-谓语-宾语](../Page/主语-谓语-宾语.md "wikilink")。但在拉丁文與俄語中，因格變化已闡明其關係，故可自由排列，產生六種語序。（一般而言，羅馬人會將欲強調的觀念放置句首。）

>

>

>

>

>

>

又，拉丁語之中的[語序雖較不重要](../Page/語序.md "wikilink")，但最常用的語序為[主賓謂結構](../Page/主賓謂結構.md "wikilink")（SOV），亦即例句中的第一條。另外，拉丁文的動詞變化可以看得出來主詞是單複數的第一、二或三人稱（共6個組合），所以這句也能進一步簡化：

>

意思是他/她/它愛錢。

拉丁语名词有五类变格法、形容词有两类变格法，每类变格法用不同的变格方式来区别上述六个[格](../Page/格_\(語法\).md "wikilink")。名詞以**單數屬格詞尾**確定變格法。

### 五类變格法

以下列表列示拉丁文的五类變格法：

<table>
<thead>
<tr class="header">
<th><p>例词</p></th>
<th><p>中文对照</p></th>
<th><p>詞性</p></th>
<th><p>變格种类</p></th>
<th><p>單數主格</p></th>
<th><p>單數呼格</p></th>
<th><p>單數屬格</p></th>
<th><p>單數與格</p></th>
<th><p>單數宾格</p></th>
<th><p>單數奪格</p></th>
<th><p>複數主、呼格</p></th>
<th><p>複數屬格</p></th>
<th><p>複數與、奪格</p></th>
<th><p>複數宾格</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>女孩</p></td>
<td><p>陰</p></td>
<td><p>一</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>奴隶</p></td>
<td><p>陽</p></td>
<td><p>二</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>儿子</p></td>
<td><p>陽</p></td>
<td><p>二</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>戰爭</p></td>
<td><p>中</p></td>
<td><p>二</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>父亲</p></td>
<td><p>陽</p></td>
<td><p>三</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>河</p></td>
<td><p>中</p></td>
<td><p>三</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>城市</p></td>
<td><p>阴</p></td>
<td><p>三</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>海</p></td>
<td><p>中</p></td>
<td><p>三</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>手</p></td>
<td><p>阴</p></td>
<td><p>四</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>触角</p></td>
<td><p>中</p></td>
<td><p>四</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>白天、一天</p></td>
<td><p>阳</p></td>
<td><p>五</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>希望</p></td>
<td><p>阴</p></td>
<td><p>五</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 動詞

动词有人称、数、时态、语气（直陈、虚拟、命令）和态（主动、被动）的区别。拉丁语动词有四类不同的变位法，另外还包括一些不规则动词。

大部分规则的动词以它们的**不定式词尾**来区分它们的变位法：第一类变位法的不定式结尾是“”，第二类变位法是“”，第三类变位法是“”，第四类变位法是“”。

兹举一例，演示拉丁语动词的主动语态的变位：

（爱，属于第一类变位法）

<table>
<thead>
<tr class="header">
<th></th>
<th><p>现在时</p></th>
<th><p>未完成时 (过去进行时)</p></th>
<th><p>将来时</p></th>
<th><p>完成时</p></th>
<th><p>过去完成时</p></th>
<th><p>将来完成时</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>直陈式</p></td>
<td><p><br />
<br />
<br />
<br />
<br />
<br />
</p></td>
<td><p><br />
<br />
<br />
<br />
<br />
<br />
</p></td>
<td><p><br />
<br />
<br />
<br />
<br />
<br />
</p></td>
<td><p><br />
<br />
<br />
<br />
<br />
<br />
</p></td>
<td><p><br />
<br />
<br />
<br />
<br />
<br />
</p></td>
<td><p><br />
<br />
<br />
<br />
<br />
<br />
</p></td>
</tr>
<tr class="even">
<td><p>虚拟式</p></td>
<td><p><br />
<br />
<br />
<br />
<br />
<br />
</p></td>
<td><p><br />
<br />
<br />
<br />
<br />
<br />
</p></td>
<td></td>
<td><p><br />
<br />
<br />
<br />
<br />
<br />
</p></td>
<td><p><br />
<br />
<br />
<br />
<br />
<br />
</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>命令式</p></td>
<td><p><br />
<br />
</p></td>
<td></td>
<td><p><br />
<br />
<br />
（第三人称单数）<br />
（第三人称复数）<br />
</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>不定式</p></td>
<td><p><br />
</p></td>
<td></td>
<td><p><br />
</p></td>
<td><p><br />
</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>分词</p></td>
<td><p><br />
</p></td>
<td></td>
<td><p><br />
</p></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

被动語態则只需把上述主动语态的结尾改成被动语态专用的结尾即可。

## 名言

  - **Scientia potentia est**：“知識就是力量。”
  - **Jus est ars boni et aequi**：“法律就是善良和正義的藝術。”
  - **Nec hostium timete, nec amicum reusate**：“不要怕敵人，也不要拒絕朋友。”
  - **[VENI VIDI
    VICI](../Page/Veni,_vidi,_vici.md "wikilink")**：“我来，我见，我征服。”——[尤利乌斯·凱撒](../Page/尤利乌斯·凱撒.md "wikilink")
  - **Fortiter in rē, suāviter in modō**：“行动要坚决，态度要温和。”
  - **Sī vīs pācem, parā
    bellum**：“如果你想要和平，先备战。”——[韦格蒂乌斯](../Page/弗拉维乌斯·韦格蒂乌斯·雷纳图斯.md "wikilink")
  - *' Nil desperandum*'：“永遠不要絕望。”
  - '''Nemo me impune lacessit '''：“誰也不可以欺我而不受懲罰。”
  - **Tempus fugit**：“光阴似箭。”——[维吉尔](../Page/维吉尔.md "wikilink")
  - **Vox populi, vox Dei**：“民意就是天意。”
  - **Salus populi suprema lex
    esto**：“人民利益高于一切。”（直译为：人民的利益是最高法律）——[维吉尔](../Page/维吉尔.md "wikilink")
  - *' Non sibi, sed omnibus*'：“不為了自己，而為了所有人。”
  - **cōgitō ergō
    sum**：“[我思故我在](../Page/我思故我在.md "wikilink")。”——[笛卡尔](../Page/笛卡尔.md "wikilink")
  - **annus
    mirabilis**：“奇迹迭出的一年”或“令人惊异的一年。”——源自[英国诗人](../Page/英国.md "wikilink")[德莱顿纪念](../Page/约翰·德莱顿.md "wikilink")[1666年伦敦大火的诗篇](../Page/1666年伦敦大火.md "wikilink")
  - **annus
    horribilis**：“可怕的一年”或“多灾多难的一年。”——[英国女王](../Page/英国君主.md "wikilink")[伊丽莎白二世称呼](../Page/伊丽莎白二世.md "wikilink")1992年的用句
  - **E pluribus
    unum**：“[合众为一](../Page/合众为一.md "wikilink")。”——[美国国徽上的格言之一](../Page/美国.md "wikilink")
  - **Qui tacet consentit**：“沉默即默認。”
  - **[Carpe
    diem](../Page/及時行樂.md "wikilink")**：“及時行樂。”——出自[賀拉斯](../Page/賀拉斯.md "wikilink")
  - **Unus pro omnibus, omnes pro uno**：“我为人人，人人为我。”（对应英语的：One for all,
    all for
    one）——[瑞士国家格言](../Page/瑞士.md "wikilink")、[大仲马的座右铭](../Page/大仲马.md "wikilink")
  - **Veritas**：“真理。”——[哈佛大学校训](../Page/哈佛大学.md "wikilink")
  - **Mens et Manus**：“手脑并用。”——[麻省理工学院校训](../Page/麻省理工学院.md "wikilink")
  - **Velut arbor
    ævo**：“像大树一样茁壮”——[多伦多大学校训](../Page/多伦多大学.md "wikilink")
  - **Dei sub numine
    viget**：“让她以上帝的名义繁荣。”——[普林斯顿大学校训](../Page/普林斯顿大学.md "wikilink")
  - **Lux et veritas**：“光明与真知。”——[耶鲁大学校训](../Page/耶鲁大学.md "wikilink")
  - **Novus ordo
    seclorum**：“时代新秩序。”——[耶鲁商学院校训](../Page/耶鲁商学院.md "wikilink")
  - **Rerum cognoscere
    causas**：“了解萬物發生的緣故。”——[倫敦大學倫敦政治經濟學院校训](../Page/倫敦大學倫敦政治經濟學院.md "wikilink")
  - **Tempus omnia revelat**：“时间会揭露一切。”
  - **Hinc lucem et pocula
    sacra**：“此乃启蒙之所，智识之源。”——[剑桥大学校训](../Page/剑桥大学.md "wikilink")
  - **Dominus Illuminatio
    Mea**：“主照亮我。”——[牛津大学校训](../Page/牛津大学.md "wikilink")
  - **Sapientia Et
    Virtus**：“智慧和品德（字面直译）”（官方中文校训为“明德格物”，语出《大学》。）——[香港大学校训](../Page/香港大学.md "wikilink")\[23\]
  - **Via Veritas
    Vita**：“道路、真理、生命。”——据传为[耶稣基督](../Page/耶稣.md "wikilink")，[格拉斯哥大学校训](../Page/格拉斯哥大学.md "wikilink")\[24\]

## 拉丁语学习资料

  - 一、**教程**：
  - 1、肖原编著，《拉丁语基础》，商务印书馆1983
  - 2、张卜天译，《韦洛克拉丁语教程》，北京联合出版公司2009（新版2017）
  - 二、**词典**：
  - 1、谢大任等编纂，《拉丁语汉语词典》，商务印书馆1988
  - 2、吴金瑞编译，《拉丁汉文辞典》，光启出版社1965（再版1980）
  - 3、张维笃、甘增佑、苗德秀（Theodorus Mittler）、彭加德（Ernestus
    Röhm）编纂，《中华拉丁大辞典》，保禄印书馆1956（再版1983）
  - 三、**语法工具书**：
  - 1、信德麟编著《拉丁语和希腊语》，外语教学与研究出版社2007
  - 2、信德麟编著《拉丁语语法》，外语教学与研究出版社2015
  - 3、顾枝鹰、杨志城等译《拉丁语语法新编》，华东师范大学出版社2017

## 参见

  - [拉丁语发音](../Page/拉丁语发音.md "wikilink")
  - [拉丁语语法](../Page/拉丁语语法.md "wikilink")
  - [新拉丁语](../Page/新拉丁语.md "wikilink")
  - [通俗拉丁语](../Page/通俗拉丁语.md "wikilink")
  - [拉丁文常用短语](../Page/拉丁文常用短语.md "wikilink")
  - [拉丁民族](../Page/拉丁民族.md "wikilink") -
    [罗马帝国](../Page/罗马帝国.md "wikilink")

## 参考文献

## 外部链接

  - [《民族語》关于拉丁语的报告](http://www.ethnologue.com/show_language.asp?code=lat)

{{-}}

[Category:拉丁-法利希语支](../Category/拉丁-法利希语支.md "wikilink")
[拉丁语](../Category/拉丁语.md "wikilink")
[Category:古代語言](../Category/古代語言.md "wikilink")
[Category:古羅馬](../Category/古羅馬.md "wikilink")
[Category:屈折語](../Category/屈折語.md "wikilink")
[Category:主賓謂語序語言](../Category/主賓謂語序語言.md "wikilink")

1.

2.  《拉丁语和希腊语》（2007年1月第一版）信德麟 p13 外语教学与研究出版社

3.  \>

4.  （美）F. M .韦洛克，《韦洛克拉丁语教程》（第六版）世界图书出版社，第8页

5.  （美）F. M .韦洛克《韦洛克拉丁语教程》（第六版），世界图书出版社，第39页

6.  （美）F. M. 韦洛克《韦洛克拉丁语教程》（第六版）世界图书出版社，第40页

7.  《韦洛克拉丁语教程》（第六版，美）F. M. 韦洛克 p43 世界图书出版社

8.  （美）F. M .韦洛克《韦洛克拉丁语教程》（第六版），世界图书出版社

9.  《拉丁语和希腊语》（2007年1月第一版）信德麟外语教学与研究出版社

10.
11. [A Tunisian stamp featuring the
    mosaic](../Page/:Image:Virgil-Mosaic-Tunesia.jpg.md "wikilink").

12.
13.
14.
15.
16.

17.
18. （美）F. M .韦洛克《韦洛克拉丁语教程》（第六版），世界图书出版社

19. （美）F. M .韦洛克《韦洛克拉丁语教程》（第六版），世界图书出版社

20. 《韦洛克拉丁语教程》（第六版，美）F. M . p52-53韦洛克世界图书出版社

21. 拉丁文帝國（*Le latin ou l'empire d'un signe*），瓦克（Françoise Waquet），ISBN
    978-986-7001-80-1

22. （美）F. M .韦洛克《韦洛克拉丁语教程》（第六版），世界图书出版社

23.

24.