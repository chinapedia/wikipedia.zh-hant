**強納森·桑德斯**（，中文名**桑德斯**，）是[臺灣](../Page/臺灣.md "wikilink")[超級籃球聯賽的初代兩位洋將其中一位](../Page/超級籃球聯賽.md "wikilink")。自從在2006年加入之後，桑德斯分別待過[緯來獵人隊](../Page/台灣大雲豹籃球隊.md "wikilink")（第四季）、[米迪亞精靈](../Page/米迪亞精靈.md "wikilink")（第五季）隊和[璞園建築](../Page/璞園建築籃球隊.md "wikilink")（第七季）；身高200公分，體重96公斤\[1\]。

## 經歷

  - 大學就讀美國[聖瑪莉學院](../Page/聖瑪莉學院.md "wikilink")
  - 曾在[立陶宛和](../Page/立陶宛.md "wikilink")[日本鵝皇隊打球](../Page/日本鵝皇隊.md "wikilink")

## 個人生活和家庭：

  - 喜好詩歌
  - 家庭成員有繼父，母親，以及弟弟布萊森。

## 攻守數據

| 年       | 球队                                         | 场次 | 上場時間 | 抄截  | 阻攻  | 籃板   | 助攻  | 命中率  | 得分   |
| ------- | ------------------------------------------ | -- | ---- | --- | --- | ---- | --- | ---- | ---- |
| 2000-01 | [科羅拉多州立大學](../Page/科羅拉多州立大學.md "wikilink") | 27 | 7.9  | 0.3 | 0.1 | 1.4  | 0.5 | .333 | 1.3  |
| 2002-03 | 聖瑪莉[加州大學](../Page/加州大學.md "wikilink")      | 23 | 26.0 | 0.3 | 0.2 | 2.8  | 1.4 | .409 | 5.0  |
| 2003-04 | 聖瑪莉[加州大學](../Page/加州大學.md "wikilink")      | 31 | 26.0 | 0.8 | 0.4 | 4.6  | 2.5 | .449 | 6.4  |
| 2004-05 | 聖瑪莉[加州大學](../Page/加州大學.md "wikilink")      | 33 | 28.5 | 0.7 | 0.7 | 4.4  | 1.8 | .486 | 7.7  |
| 2005-06 | Akademija MRU                              | 49 | 30.3 | 2.2 | 0.9 | 10.1 | 1.9 | .527 | 13.8 |
| 2006-07 | [緯來獵人](../Page/緯來獵人.md "wikilink")         | 30 | 28.5 | 1.3 | 0.4 | 16.3 | 2.4 | .510 | 23.5 |
| 2007-08 | [米迪亞精靈](../Page/米迪亞精靈.md "wikilink")       | 29 | 36.2 | 1.7 | 1.6 | 16.6 | 5.8 | .520 | 21.3 |
| 2009-10 | [璞園建築](../Page/璞園建築籃球隊.md "wikilink")      | 28 | 32.4 | 1.4 | 1.2 | 12.8 | 4.7 | .493 | 18.0 |

<ref name="stats 1">{{ cite web

`|title        = Sanders' College Stats`
`|publisher    = Stats LLC`
`|url          = `<http://statsheet.com/mcb/players/jonathan-sanders>
`|date         = 2007`
`|accessdate   = 2008-04-11`
`|archive-url  = `<https://web.archive.org/web/20080426005544/http://statsheet.com/mcb/players/jonathan-sanders>
`|archive-date = 2008-04-26`
`|dead-url     = yes`

}}</ref><ref name="stats 2"> {{ cite web

`|title       = Sanders' SBL Stats 08`
`|publisher   = Yahoo`
`|url         = `<http://tw.sports.yahoo.com/sbl/sbl_player_bygame/playerid/0047/>
`|date        = 2008`
`|accessdate  = 2008-04-11`
`|deadurl     = yes`
`|archiveurl  = `<https://web.archive.org/web/20091215081706/http://tw.sports.yahoo.com/sbl/sbl_player_bygame/playerid/0047>
`|archivedate = 2009-12-15`

}}</ref>\[2\]<ref name="stats 4"> {{ cite web

`|title       = Sanders' Lithuanian Stats`
`|publisher   = KK Akademija-LTU`
`|url         = `<http://akademija-mru.lt/index.php?fuseaction=basket.view_player&id=2904&player_id=20>
`|date        = 2004`
`|accessdate  = 2008-04-11`
`|deadurl     = yes`
`|archiveurl  = `<https://web.archive.org/web/20080424133944/http://akademija-mru.lt/index.php?fuseaction=basket.view_player&id=2904&player_id=20>
`|archivedate = 2008-04-24`

}}</ref>

## 外部連結

  - [SBL攻守數據](https://web.archive.org/web/20080828130825/http://tw.sports.yahoo.com/sbl/sbl_player_profile/playerid/0047/)

## 參考文獻

[S](../Category/臺灣籃球運動員.md "wikilink")
[Category:超級籃球聯賽球員](../Category/超級籃球聯賽球員.md "wikilink")
[Category:布魯克林區出身人物](../Category/布魯克林區出身人物.md "wikilink")

1.  {{ cite web |title = Taiwan Hoops |publisher = Chris Wang |url =
    <http://twhoops.blogspot.com/2007/12/07-08-sbl-preview-dmedia-genies.html>
    |date = Dec 7, 2007 |accessdate = 2008-04-10 }}
2.  {{ cite web |title = Sanders' SBL Stats 07 |publisher = Yam.com |url
    = <http://sports.yam.com/special/sbl/player.php?id=100> |date = 2007
    |accessdate = 2008-04-11 }}