**四氢呋喃**
(**THF**)无色、可与水混溶、在[常温常压下有较小](../Page/常温常压.md "wikilink")[粘稠度的有机液体](../Page/黏度.md "wikilink")。这种[环狀](../Page/杂环化合物.md "wikilink")[醚的化学式可写作](../Page/醚.md "wikilink")(CH<sub>2</sub>)<sub>4</sub>O。由于它的液态范围很长，所以是一种常用的中等[极性](../Page/极性.md "wikilink")[非质子性溶剂](../Page/非质子性溶剂.md "wikilink")。它的主要用途是作[高分子](../Page/高分子.md "wikilink")[聚合物的](../Page/聚合物.md "wikilink")[前体](../Page/前体.md "wikilink")。

## 生产

四氢呋喃每年的产量大约是二十万吨。\[1\]被应用得最广的制备方法是[1,4-丁二醇在酸催化条件下](../Page/1,4-丁二醇.md "wikilink")[脱水](../Page/脱水.md "wikilink")，这类似于通过对[乙醇脱水来制备](../Page/乙醇.md "wikilink")[乙烯](../Page/乙烯.md "wikilink")。而丁二醇可以通过对[乙炔](../Page/乙炔.md "wikilink")[羰基化之后](../Page/羰基化.md "wikilink")[加氢得到](../Page/加氢.md "wikilink")。[杜邦公司发展出一种通过](../Page/杜邦.md "wikilink")[氧化](../Page/氧化.md "wikilink")[正丁烷来粗制](../Page/正丁烷.md "wikilink")[顺酐](../Page/顺酐.md "wikilink")，再进行催化[氢化的生产方式](../Page/氢化.md "wikilink")。\[2\]也可以先对[烯丙醇施以](../Page/烯丙醇.md "wikilink")[氢甲酰化反应](../Page/氢甲酰化反应.md "wikilink")，再将产品氢化来生产丁二醇。

THF也可以通过催化氢化[呋喃来制备](../Page/呋喃.md "wikilink")。\[3\]\[4\]而呋喃可以来自于[戊糖](../Page/戊糖.md "wikilink")。不过这种方法的应用并不广泛。

## 应用

THF可以在强酸性环境中[聚合成链状的](../Page/聚合.md "wikilink")[聚四氢呋喃](../Page/聚四氢呋喃.md "wikilink")
（PTMEG）， [CAS号](../Page/CAS号.md "wikilink") \[25190-06-1\]，也可简写作
PTMO，义为“聚环丁烷氧化物”（polytetramethylene
oxide）。它的基本用途是用来制造[弹性](../Page/弹性.md "wikilink")[聚氨酯](../Page/聚氨酯.md "wikilink")[纤维](../Page/纤维.md "wikilink")，比如[氨纶](../Page/氨纶.md "wikilink")。\[5\]

### 溶剂

THF的另一个主要用途是在[PVC和](../Page/PVC.md "wikilink")[漆的生产中作工业溶剂](../Page/漆.md "wikilink")。\[6\]它的[介電係數为](../Page/介電係數.md "wikilink")7.6，是一种[非质子溶剂](../Page/非质子溶剂.md "wikilink")，可以溶解很多[极性或者](../Page/极性.md "wikilink")[非极性物质](../Page/非极性.md "wikilink")。\[7\]
THF可与水混溶，并在低温下形成[包合物](../Page/包合物.md "wikilink")。\[8\]

### 实验室

THF常作为实验室中沸点较高的醚性溶剂。它的氧原子可以与[路易斯酸发生](../Page/路易斯酸.md "wikilink")[配位作用](../Page/配位.md "wikilink")，包括Li<sup>+</sup>、
Mg<sup>2+</sup>等，并可以与[硼氢化合物形成](../Page/硼氢化合物.md "wikilink")[加合物](../Page/加合物.md "wikilink")。因此，THF和乙醚一样，不仅可以在[硼氢化反应中作](../Page/硼氢化反应.md "wikilink")[伯醇的溶剂](../Page/伯醇.md "wikilink")，也可以做比如[有机锂化合物和](../Page/有机锂化合物.md "wikilink")[格氏试剂的溶剂](../Page/格氏试剂.md "wikilink")。\[9\]尽管性质与用途与乙醚相似，但THF的性能往往更好，\[10\]因此，乙醚是很多无水但要求不太苛刻的反应的优良溶剂（比如Grignard试剂参加的反应），而在更大温度范围内，THF是一个更可靠的[配体](../Page/配体.md "wikilink")，并在很多精细的化学反应中起到对混合溶剂性质（比如混合溶剂的电学性质）进行微调的作用。

THF被广泛应用在对[聚合物的生产研究中](../Page/聚合物.md "wikilink")。由于它超强的溶解能力，PVC等高聚物都能轻松溶解。

#### 2-甲基四氢呋喃

[2-甲基四氢呋喃](../Page/2-甲基四氢呋喃.md "wikilink")（2MeTHF）是THF的一种对环境污染更小的[同系物](../Page/同系物.md "wikilink")。\[11\]。相比THF也较廉价，
2MeTHF
的溶剂性能介于乙醚和THF，与水的互溶能力也有限，并与水形成[共沸物](../Page/共沸物.md "wikilink")。2MeTHF相比于THF的另一个优点是它的熔点更低而沸点更高。

## 安全

THF通常被认为是毒性较小的溶剂。其[半数致死量](../Page/半数致死量.md "wikilink")
(LD<sub>50</sub>)与[丙酮接近](../Page/丙酮.md "wikilink")。因其优良的溶解性能，THF可以渗入皮肤造成失水。THF可以溶解乳胶，因此使用时通常戴[丁腈橡胶或](../Page/丁腈橡胶.md "wikilink")[氯丁橡胶制的手套](../Page/氯丁橡胶.md "wikilink")。其高度可燃的性质也是安全隐患之一。

THF最大的安全隐患在于露置于空气中时会缓慢形成高爆炸性的[有机过氧化物](../Page/有机过氧化物.md "wikilink")。为了减小这个隐患，市售的THF中通常有加入[2,6-二叔丁基对甲酚](../Page/2,6-二叔丁基对甲酚.md "wikilink")（BHT）来抑制有机过氧化物的产生。同时，THF不应当被蒸干，因为有机过氧化物会被富集在蒸馏残渣中。

## 参见

  - [单体](../Page/单体.md "wikilink")
  - [聚四氢呋喃](../Page/聚四氢呋喃.md "wikilink")
  - [四氢呋喃硼烷](../Page/四氢呋喃硼烷.md "wikilink")

## 引用

## 参考

  - Loudon, G. Mark. *Organic Chemistry 4th ed.* New York: [Oxford
    University Press](../Page/Oxford_University_Press.md "wikilink").
    2002. pg 318

## 外部链接

  - [International Chemical Safety
    Card 0578](http://www.ilo.org/public/english/protection/safework/cis/products/icsc/dtasht/_icsc05/icsc0578.htm)

  - [NIOSH Pocket Guide to Chemical
    Hazards](http://www.cdc.gov/niosh/npg/npgd0602.html)

  - [THF
    usage](https://web.archive.org/web/20110605134300/http://www.orgsyn.org/orgsyn/chemname.asp?nameID=35996)

  - [THF
    info](https://web.archive.org/web/20050420133211/http://www.camd.lsu.edu/msds/t/tetrahydrofuran.htm)

  - [U.S. OSHA info on
    THF](https://web.archive.org/web/20050511033159/http://www.osha-slc.gov/SLTC/healthguidelines/tetrahydrofuran/)

  -
[Category:醚类溶剂](../Category/醚类溶剂.md "wikilink")
[Category:四氢呋喃](../Category/四氢呋喃.md "wikilink")

1.
2.  *Merck Index of Chemicals and Drugs*, 9th ed.
3.  Morrison, Robert Thornton; Boyd, Robert Neilson: *Organic
    Chemistry*, 2nd ed., Allyn and Bacon 1972, p. 569
4.
5.
6.  Herbert Müller, "Tetrahydrofuran" in Ullmann's Encyclopedia of
    Industrial Chemistry 2002, Wiley-VCH, Weinheim.
7.
8.
9.  Elschenbroich, C.; Salzer, A. ”Organometallics : A Concise
    Introduction” (2nd Ed) (1992) Wiley-VCH: Weinheim. ISBN
    3-527-28165-7
10. E.g., B.L. Lucht, D.B. Collum "Lithium Hexamethyldisilazide: A View
    of Lithium Ion Solvation through a Glass-Bottom Boat" Accounts of
    Chemical Research, 1999, volume 32, 1035–1042
11.