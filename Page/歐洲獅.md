**歐洲獅**（*Panthera leo
europaea*）是[獅的已](../Page/獅.md "wikilink")[滅絕](../Page/滅絕.md "wikilink")[亞種](../Page/亞種.md "wikilink")，分佈於南[歐洲](../Page/歐洲.md "wikilink")。牠們一般被認為是[亞洲獅的一部份](../Page/亞洲獅.md "wikilink")，但其他的則認為是一個獨立的亞種。牠們有可能是[穴獅的餘種](../Page/穴獅.md "wikilink")。

已知有兩種生活於史前歐洲的獅，分別是[原始獅及穴獅](../Page/原始獅.md "wikilink")。\[1\]

## 分佈

歐洲獅分佈於[伊比利亞半島](../Page/伊比利亞半島.md "wikilink")、[法國南部](../Page/法國.md "wikilink")、[義大利及](../Page/義大利.md "wikilink")[巴爾幹半島南部至](../Page/巴爾幹半島.md "wikilink")[希臘北部的地區](../Page/希臘.md "wikilink")。\[2\]牠們生活於[地中海及](../Page/地中海.md "wikilink")[溫帶](../Page/溫帶.md "wikilink")[森林](../Page/森林.md "wikilink")，獵食[歐洲野牛](../Page/歐洲野牛.md "wikilink")、[麋鹿](../Page/麋鹿.md "wikilink")、[原牛](../Page/原牛.md "wikilink")、[鹿及其他歐洲的](../Page/鹿.md "wikilink")[有蹄類](../Page/有蹄類.md "wikilink")。

## 滅絕

由於歐洲獅很早就已經[滅絕](../Page/滅絕.md "wikilink")，所以對牠們所知甚少。[亞里士多德及](../Page/亞里士多德.md "wikilink")[希羅多德曾描述於前](../Page/希羅多德.md "wikilink")1000年在[巴爾幹半島發現](../Page/巴爾幹半島.md "wikilink")[獅子](../Page/獅子.md "wikilink")。當[澤克西斯一世於前](../Page/澤克西斯一世.md "wikilink")480年穿越[亞歷山大帝國時](../Page/亞歷山大帝國.md "wikilink")，亦曾遇上幾隻獅子。\[3\]\[4\]\[5\]在前20年前及1年，歐洲獅分別在[義大利及](../Page/義大利.md "wikilink")[西歐滅絕](../Page/西歐.md "wikilink")。\[6\]約於70年，牠們只有在[希臘北部介乎](../Page/希臘.md "wikilink")[阿利阿克蒙河及](../Page/阿利阿克蒙河.md "wikilink")[尼斯圖斯河找到](../Page/尼斯圖斯河.md "wikilink")。最終牠們於100年在[東歐消失](../Page/東歐.md "wikilink")。\[7\]自此之後，直至10世紀，歐洲大陸的獅只有在[高加索生活](../Page/高加索.md "wikilink")。

歐洲獅的滅絕是因過度獵殺、過度開發及與[流浪犬競爭所致](../Page/流浪犬.md "wikilink")。連同[巴巴裏獅及](../Page/巴巴裏獅.md "wikilink")[亞洲獅](../Page/亞洲獅.md "wikilink")，歐洲獅會在[羅馬鬥獸場與勇士](../Page/羅馬鬥獸場.md "wikilink")、[里海虎](../Page/里海虎.md "wikilink")、[原牛及](../Page/原牛.md "wikilink")[熊戰鬥](../Page/熊.md "wikilink")。由於牠們的地理分佈，羅馬人較容易找到牠們。當歐洲獅步入滅絕時期，羅馬人開始從[北非及](../Page/北非.md "wikilink")[中東進口獅子](../Page/中東.md "wikilink")。

## 參考

[PLE](../Category/絕滅物種.md "wikilink") [E](../Category/獅.md "wikilink")

1.

2.

3.  Asiatic Lion Information Centre. *2001 Past and present distribution
    of the lion in North Africa and Southwest Asia*. Downloaded 1 June
    2006 from

4.
5.

6.  <http://rg.ancients.info/lion/lions.html>

7.