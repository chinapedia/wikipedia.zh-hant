**小林沙苗**（）為[日本女性](../Page/日本.md "wikilink")[聲優](../Page/聲優.md "wikilink")。出生於[靜岡縣](../Page/靜岡縣.md "wikilink")[濱松市](../Page/濱松市.md "wikilink")[濱北區](../Page/濱北區.md "wikilink")。2015年6月1日隸屬公司從[Production
baobab移至](../Page/Production_baobab.md "wikilink")[Sigma
Seven](../Page/Sigma_Seven.md "wikilink")。

## 概要

  - 最初的夢想是成為演員，高中時為地區音樂劇團「ステージ97」所屬團員，在該劇團[東京公演時](../Page/東京.md "wikilink")，被前來觀戲的聲優賞識其演技力並網羅至聲優界。
  - 剛出道時因為仍在該地區大學就讀的關係，以新幹線來往東京通勤工作。
  - 主要工作是外國片、動畫、遊戲、以及廣播劇配音，近期以幼教片、外國片日文配音居多。
  - 聲線範圍廣泛，是少數擁有「七色之聲」美名的聲優之一，屬於高演技力的實力派，從少年到少女、年長的女性到冷艷氣質的大姊、角色的人格分裂都能完美詮釋。
  - 第一次配音的作品是外國片的一名小配角，台詞只有結尾的一段話，因而等候錄音等到睡著。
  - 有潛水員執照、劍道初段，但為了保護聲帶而放棄需要用力吶喊的劍道。
  - 在戲劇之外的興趣是繪畫，曾在訪談中提到若沒成為聲優就會想走美術系。
  - 曾與[永野椎菜的企劃](../Page/永野椎菜.md "wikilink")「ShiinaTactix」合作，錄製『ShiinaTactix-Sana.K』專輯，但隨後則婉拒第二張專輯合作計畫。

## 人物

**交友關係**

  - 和[鈴村健一](../Page/鈴村健一.md "wikilink")、[櫻井孝宏在工作上有多次合作因此成為好友](../Page/櫻井孝宏.md "wikilink")。
  - 與[松來未祐交情深厚](../Page/松來未祐.md "wikilink")，曾在生日時一同前往東京迪斯尼樂園慶祝。在出演《[D.Gray-man](../Page/D.Gray-man.md "wikilink")》之後，也與飾演利娜莉一角的[伊藤靜相互結識](../Page/伊藤靜.md "wikilink")。
  - 與[豐口惠](../Page/豐口惠.md "wikilink")、[水樹奈奈](../Page/水樹奈奈.md "wikilink")、[能登麻美子因在](../Page/能登麻美子.md "wikilink")《[草莓100%](../Page/草莓100%.md "wikilink")》中共演而深交，成為所謂的「」。

**角色佚話**

  - 在《[草莓100%](../Page/草莓100%.md "wikilink")》中，小林飾演北大路皐月一角，但最初小林所試鏡的角色是西野司，反而現在飾演西野司的[豐口惠最初試鏡的角色是北大路皐月](../Page/豐口惠.md "wikilink")。
  - 在《[魔法少女奈葉A's](../Page/魔法少女奈葉A's.md "wikilink")》中，小林為闇之書／Reinforce一角獻聲，但最初試鏡的角色是守護騎士的席格娜，反之現在飾演席格娜的[清水香里最初則試鏡的角色是闇之書](../Page/清水香里.md "wikilink")／Reinforce。

## 演出作品

※**粗體字**表示說明飾演的主要角色。

### 電視動畫

  - 2000年

<!-- end list -->

  - [超級小黑咪](../Page/超級小黑咪.md "wikilink")（京子）
  - [第一神拳](../Page/第一神拳.md "wikilink")（**間柴久美**、女學生）
  - [幻影死神](../Page/幻影死神.md "wikilink")（**如月真名花**）

<!-- end list -->

  - 2001年

<!-- end list -->

  - [旋風之用心棒](../Page/旋風之用心棒.md "wikilink")（**田野倉美雪**、電台DJ）

  - [超激力戰鬥車TURBO](../Page/超激力戰鬥車TURBO.md "wikilink")（）

  - [彗星公主](../Page/彗星公主_\(動畫\).md "wikilink")（有希先生、、太一）

  - [熱帶雨林的爆笑生活](../Page/熱帶雨林的爆笑生活.md "wikilink")（白衣女）

  -
  - [娜姬卡電擊大作戰](../Page/娜姬卡電擊大作戰.md "wikilink")（**海槻玲奈**）

  - [棋魂](../Page/棋魂.md "wikilink")（**塔矢亮**）

  -
<!-- end list -->

  - 2002年

<!-- end list -->

  - [水瓶戰記 Sign for Evolution](../Page/水瓶戰記.md "wikilink")（）

  - [阿波特戰記五九](../Page/阿波特戰記五九.md "wikilink")（）

  -
  - [真·女神轉生D 惡魔之子 光之書與闇之書](../Page/真·女神轉生_惡魔之子.md "wikilink")（）

  - [東京地夜奇兵](../Page/東京地夜奇兵.md "wikilink")（）

  - [東京喵喵](../Page/東京喵喵.md "wikilink")（部員）

  - [花田少年史](../Page/花田少年史.md "wikilink")（加奈）

  -
  - （**宇津木沙織**）

<!-- end list -->

  - 2003年

<!-- end list -->

  - [AVENGER](../Page/AVENGER.md "wikilink")（）

  - [妮嘉尋親記](../Page/妮嘉尋親記.md "wikilink")（）

  - [自動機械人 小飛俠阿童木](../Page/小飛俠阿童木.md "wikilink")（）

  - [E'S OTHERWISE](../Page/E'S_OTHERWISE.md "wikilink")（**明日香·篤川**）

  - [萬花筒之星](../Page/萬花筒之星.md "wikilink")（）

  - [京極夏彥 巷說百物語](../Page/巷說百物語.md "wikilink")（****）

  - [Gilgamesh](../Page/Gilgamesh.md "wikilink")（）（**御室風子**）

  - [閃靈二人組](../Page/閃靈二人組.md "wikilink")（水野真紀）

  - [魁！！天兵高校](../Page/魁！！天兵高校.md "wikilink")（女性）

  - [音速小子X](../Page/音速小子X.md "wikilink")（**克里斯**）

  -
  - [天使怪盜](../Page/天使怪盜.md "wikilink")（福田律子）

  - （雨宮的母親、 他）

  - [PAPUWA](../Page/PAPUWA.md "wikilink")（）

  - [真珠美人魚](../Page/真珠美人魚.md "wikilink")（瑪莉亞）

  - [魔法使的條件](../Page/魔法使的條件.md "wikilink")（菊池春）

  - [闇與帽子與書的旅人](../Page/闇與帽子與書的旅人.md "wikilink")（**莉莉絲**）

  - （良太的媽媽）

<!-- end list -->

  - 2004年

<!-- end list -->

  -
  - [變異體少女](../Page/變異體少女.md "wikilink")（**妮悠**／**露西**／**露西DNA之聲**）

  - [現視研](../Page/現視研.md "wikilink")（北川）

  - [最遊記RELOAD](../Page/最遊記.md "wikilink")（金閣、銀閣）

  -
  - [蒼穹之戰神](../Page/蒼穹之戰神.md "wikilink")（**卡農·梅芬絲**）

  - [超變身巫女祈鬥士](../Page/超變身巫女祈鬥士.md "wikilink")（）

      - （**桂木洋子**）

  - [忘卻的旋律](../Page/忘卻的旋律.md "wikilink")（遠音）

  - [神奇寶貝超世代](../Page/神奇寶貝超世代.md "wikilink")（、黃瓜香）

  - [真珠美人魚Pure](../Page/真珠美人魚.md "wikilink")（蝙蝠小姐、、瑪莉亞）

  - [舞-HiME](../Page/舞-HiME.md "wikilink")（**尾久崎晶**）

  - [異域天使](../Page/異域天使.md "wikilink")（**瑪德拉絲**）

  -
  - [美鳥日記](../Page/美鳥日記.md "wikilink")（瀧口由真）

  - [遊戲王GX](../Page/遊戲王GX.md "wikilink")（**天上院明日香**）

  - [LOVE♥LOVE?](../Page/超變身巫女祈鬥士.md "wikilink")（**桂木洋子**）

<!-- end list -->

  - 2005年

<!-- end list -->

  - [草莓100%](../Page/草莓100%.md "wikilink")（**北大路五月**）
  - [英國戀物語艾瑪](../Page/艾瑪_\(漫畫\).md "wikilink")（**愛蕾諾·康貝爾**）
  - [-{zh-cn:爱丽丝学园; zh-tw:學園愛麗絲;
    zh-hk:愛麗絲學園;}-](../Page/學園愛麗絲.md "wikilink")（園生要）
  - [玻璃假面（2005版本）](../Page/玻璃假面.md "wikilink")（**北島真夜**）
  - [蠟筆小新](../Page/蠟筆小新.md "wikilink")（麗莎·阿斯匹靈）
  - [創聖的大天使](../Page/創聖的大天使.md "wikilink")（**紅麗花**）
  - [翼之奇幻旅程](../Page/翼之奇幻旅程.md "wikilink")（星火）
  - [火影忍者](../Page/火影忍者.md "wikilink")（）
  - [BUZZER BEATER](../Page/零秒出手_\(漫畫\).md "wikilink")（****）
  - [舞-乙HiME](../Page/舞-乙HiME.md "wikilink")（尾久崎晶）
  - [魔法少女奈葉A's](../Page/魔法少女奈葉A's.md "wikilink")（闇之書的意志／Reinforce）
  - [雪之女王](../Page/雪之女王.md "wikilink")（）
  - [LOVELESS](../Page/LOVELESS.md "wikilink")（目渚）

<!-- end list -->

  - 2006年

<!-- end list -->

  - [歡迎加入NHK\!](../Page/歡迎加入NHK!.md "wikilink")（**柏瞳**）

  - [死亡代理人](../Page/死亡代理人.md "wikilink")（**戴得拉斯**、）

  -
  - [地獄少女 二籠](../Page/地獄少女.md "wikilink")（新田紀和子）

  - [少年陰陽師](../Page/少年陰陽師.md "wikilink")（**藤原彰子**）

  -
  - [驅魔少年](../Page/驅魔少年.md "wikilink")（**亞連·沃克**、）

  - [.hack//Roots](../Page/.hack.md "wikilink")（****）

  - [仰望半月的夜空](../Page/仰望半月的夜空.md "wikilink")（與謝野美紗子）

<!-- end list -->

  - 2007年

<!-- end list -->

  - [英國戀物語艾瑪 第二幕](../Page/艾瑪_\(漫畫\).md "wikilink")（**愛蕾諾·康貝爾**）

  - [魔女獵人](../Page/魔女獵人.md "wikilink")（）

  - [現視研2](../Page/現視研.md "wikilink")（北川）

  - [灼眼的夏娜Ⅱ](../Page/灼眼的夏娜.md "wikilink")（“戲睡鄉”梅亞）

  -
  - [DARKER THAN BLACK
    黑之契約者](../Page/DARKER_THAN_BLACK.md "wikilink")（石崎香那美）

  - [奔向地球](../Page/奔向地球.md "wikilink")（**菲西斯**、蕾蒂西亞）

  - [桃華月憚](../Page/桃華月憚.md "wikilink")（）

  - [交響情人夢](../Page/交響情人夢.md "wikilink")（三木清良）

  -
  - [BUZZER BEATER](../Page/BUZZER_BEATER.md "wikilink")（****）

  - [怪傑佐羅力2](../Page/怪俠索羅利.md "wikilink")（**小奇奇**：[熊井統子的代役](../Page/熊井統子.md "wikilink")）

  -
  - [BAMBOO BLADE](../Page/BAMBOO_BLADE.md "wikilink")（凱莉·西川）

<!-- end list -->

  - 2008年

<!-- end list -->

  -
  - [夏目友人帳](../Page/夏目友人帳.md "wikilink")（**夏目鈴子**）

  - [新·安琪莉可 Abyss](../Page/新·安琪莉可.md "wikilink")（）

  - [破天荒遊戲](../Page/破天荒遊戲.md "wikilink")（**拉賽爾**／**拉瑟西亞·蘿絲**）

  - [女神異聞錄
    ～三位一體之魂～](../Page/PERSONA_-trinity_soul-.md "wikilink")（**二階堂映子**）

  - [超時空要塞 Frontier](../Page/超時空要塞_Frontier.md "wikilink")（凱瑟琳·格拉斯）

  - [魔人偵探腦嚙尼奧洛](../Page/魔人偵探腦嚙尼奧洛.md "wikilink")（本城剎那）

  - [魔法人力派遣公司](../Page/魔法人力派遣公司.md "wikilink")（石動朔夜）

  - [我家有個狐仙大人](../Page/我家有個狐仙大人.md "wikilink")（豆翡翠）

  - [地獄少女三鼎](../Page/地獄少女.md "wikilink")（新山美和）

  - [閃電十一人](../Page/閃電十一人.md "wikilink")（**雷門夏未**）

<!-- end list -->

  - 2009年

<!-- end list -->

  - [RIDEBACK](../Page/RIDEBACK.md "wikilink")（依田惠）
  - [續 夏目友人帳](../Page/續_夏目友人帳.md "wikilink")（**夏目鈴子**）
  - [元素獵人](../Page/元素獵人.md "wikilink")（歐莉·康諾利）

<!-- end list -->

  - 2010年

<!-- end list -->

  - [無頭騎士異聞錄
    DuRaRaRa\!\!](../Page/無頭騎士異聞錄_DuRaRaRa!!.md "wikilink")（**矢霧波江**）
  - [交響情人夢 Final](../Page/交響情人夢_\(動畫\).md "wikilink")（三木清良）
  - [學生會長是女僕](../Page/學生會長是女僕.md "wikilink")（兵藤渚）
  - [銀魂](../Page/銀魂.md "wikilink")（靈）

<!-- end list -->

  - 2011年

<!-- end list -->

  - [閃電十一人GO](../Page/閃電十一人.md "wikilink")（**圓堂夏未**）
  - [夏目友人帳 参](../Page/夏目友人帳.md "wikilink")（**夏目鈴子**、想象中的玲子）
  - [灼眼的夏娜Ⅲ](../Page/灼眼的夏娜.md "wikilink")（“戲睡鄉”梅亞）

<!-- end list -->

  - 2012年

<!-- end list -->

  - [夏目友人帳 肆](../Page/夏目友人帳.md "wikilink")（**夏目鈴子**）
  - [魯邦三世 -名為峰不二子的女人-](../Page/魯邦三世_-名為峰不二子的女人-.md "wikilink")（愛莎）
  - [加速世界](../Page/加速世界.md "wikilink")（能美征二／Dusk Taker）
  - [超譯百人一首戀歌](../Page/超譯百人一首戀歌.md "wikilink")（弘子）
  - [來自新世界](../Page/來自新世界.md "wikilink")（岡野）

<!-- end list -->

  - 2013年

<!-- end list -->

  - [THE UNLIMITED 兵部京介](../Page/楚楚可憐超能少女組.md "wikilink")（派蒂·庫魯）
  - [黑色嘉年華](../Page/黑色嘉年華.md "wikilink")（米涅）
  - [噬血狂襲](../Page/噬血狂襲.md "wikilink")（曉深森）
  - [名偵探柯南](../Page/名偵探柯南_\(動畫\).md "wikilink")（高森賈斯汀）※第720～721話

<!-- end list -->

  - 2014年

<!-- end list -->

  - [HAMATORA ─超能偵探社─](../Page/HAMATORA_─超能偵探社─.md "wikilink")（幼年雷修）
  - [世界征服 謀略之星](../Page/世界征服_謀略之星.md "wikilink")（椿）
  - [Happiness Charge
    光之美少女！](../Page/Happiness_Charge_光之美少女！.md "wikilink")（冰川麻理亞／溫柔天使）
  - [我們大家的河合莊](../Page/我們大家的河合莊.md "wikilink")（**住子**）
  - [地球隊長](../Page/地球隊長.md "wikilink")（毬村真緒）
  - [月刊少女野崎同學](../Page/月刊少女野崎同學.md "wikilink")（凱西）
  - [晨曦公主](../Page/晨曦公主.md "wikilink")（青龍〈幼少〉）

<!-- end list -->

  - 2015年

<!-- end list -->

  - [蒼穹之戰神 EXODUS](../Page/蒼穹之戰神.md "wikilink")（**羽佐間卡農**）
  - [無頭騎士異聞錄 DuRaRaRa\!\!×2
    承](../Page/無頭騎士異聞錄_DuRaRaRa!!.md "wikilink")（矢霧波江）
  - [靈感少女](../Page/靈感少女.md "wikilink")（保險醫師）
  - 無頭騎士異聞錄 DuRaRaRa\!\!×2 轉（矢霧波江）
  - 蒼穹之戰神 EXODUS 第2期（**羽佐間卡農**）
  - [偵探小隊KZ事件簿](../Page/偵探小隊KZ事件簿.md "wikilink")（彩的媽媽）
  - [金田一少年之事件簿 R](../Page/金田一少年之事件簿_\(動畫\).md "wikilink")（丸谷瑠奈）\#37
    \[1\]

<!-- end list -->

  - 2016年

<!-- end list -->

  - [昭和元祿落語心中](../Page/昭和元祿落語心中.md "wikilink")（菊比古〈幼少〉）
  - 無頭騎士異聞錄 DuRaRaRa\!\!×2 結（矢霧波江）
  - [房東妹子青春期](../Page/房東妹子青春期.md "wikilink")（佐佐木健吾、大嬸、麻美）
  - [JoJo的奇妙冒險 不碎鑽石](../Page/JoJo的奇妙冒險.md "wikilink")（女學生）
  - [文豪Stray Dogs](../Page/文豪Stray_Dogs.md "wikilink")（佐佐城信子）
  - [遊戲王ARC-V](../Page/遊戲王ARC-V.md "wikilink")（天上院明日香）
  - 夏目友人帳 伍（**夏目鈴子**、女高中生貓咪老師）
  - [超自然9人組](../Page/超自然9人組.md "wikilink")（相模少年、播報員A）

<!-- end list -->

  - 2017年

<!-- end list -->

  - 昭和元祿落語心中 -助六再現篇-（菊比古〈幼少〉）
  - 夏目友人帳 陸（**夏目鈴子**）
  - [Fate/Apocrypha](../Page/Fate/Apocrypha.md "wikilink")（愛因茲貝倫的聖女）
  - [牙狼〈GARO〉-VANISHING
    LINE-](../Page/牙狼〈GARO〉-VANISHING_LINE-.md "wikilink")（恩尼絲）
  - [鬼燈的冷徹](../Page/鬼燈的冷徹.md "wikilink") 第貳期（伊邪那美）

<!-- end list -->

  - 2018年

<!-- end list -->

  - [甜心戰士 Universe](../Page/甜心戰士.md "wikilink")（炎爪）
  - [京都寺町三條商店街的福爾摩斯](../Page/京都寺町三條商店街的福爾摩斯.md "wikilink")（**瀧山利休**\[2\]）
  - [Happy Sugar Life](../Page/Happy_Sugar_Life.md "wikilink")（女店長）
  - [刀劍神域 Alicization](../Page/刀劍神域.md "wikilink")（神代凜子）
  - [CONCEPTION](../Page/CONCEPTION_產子救世錄.md "wikilink")（**米蕾**\[3\]）

<!-- end list -->

  - 2019年

<!-- end list -->

  - 名侦探柯南（遠堂深美 ）

### OVA

  - （**黑澤麻衣**、****）

  - [.hack//Liminality](../Page/.hack.md "wikilink")（水無瀨舞）

  - [勇往直前2](../Page/勇往直前2.md "wikilink")（蛇紋石雙胞胎姊妹中的姊姊 — 皮雅婕）

  - [Macross Zero](../Page/Macross_Zero.md "wikilink")（莎拉諾姆）

  - [翼 TOKYO REVELATIONS](../Page/TSUBASA翼#OVA.md "wikilink")（星火）

  - [少女的秘密心事](../Page/少女的秘密心事.md "wikilink")（**山吹八重**）

  - [聖鬥士星矢 THE LOST CANVAS
    冥王神話](../Page/THE_LOST_CANVAS_冥王神話_\(動畫\).md "wikilink")（**天鶴座讓葉**）

  - [只有神知道的世界 天理篇](../Page/只有神知道的世界.md "wikilink")（**桂木桂馬**〈幼年〉）

**2015年**

  - [無頭騎士異聞錄 DuRaRaRa\!\!×2 承 外傳！？ 第4.5話
    我的心是火鍋的形狀](../Page/無頭騎士異聞錄_DuRaRaRa!!.md "wikilink")（矢霧波江）※劇場先行上映\[4\]
  - [無頭騎士異聞錄 DuRaRaRa\!\!×2 轉 外傳！？ 第13.5話
    戀愛故事鏗鏗鏗](../Page/無頭騎士異聞錄_DuRaRaRa!!.md "wikilink")（矢霧波江）※劇場先行上映\[5\]

**2016年**

  - [無頭騎士異聞錄 DuRaRaRa\!\!×2 結 外傳！？ 第19.5話
    DuFuFuFu\!\!](../Page/無頭騎士異聞錄_DuRaRaRa!!.md "wikilink")（矢霧波江）※劇場先行上映\[6\]

**2017年**

  - [夏目友人帳 伍 一夜盃](../Page/夏目友人帳.md "wikilink")（**夏目玲子**）
  - 夏目友人帳 陸 夢幻的碎片（**夏目玲子**）

### 動畫電影

  - 2004年

<!-- end list -->

  - [蒸汽男孩](../Page/蒸汽男孩.md "wikilink")（艾瑪）

<!-- end list -->

  - 2011年

<!-- end list -->

  - [永遠之久遠](../Page/永遠之久遠.md "wikilink")（若月綠）

<!-- end list -->

  - 2012年

<!-- end list -->

  - [魔法少女奈葉 The MOVIE 2nd
    A's](../Page/魔法少女奈葉_The_MOVIE_2nd_A's.md "wikilink")（**夜天的魔導書的意志**）

<!-- end list -->

  - 2017年

<!-- end list -->

  - [虐殺器官](../Page/虐殺器官.md "wikilink")（露西亞·修克羅普）

### 電玩遊戲

  - [櫻花大戰](../Page/櫻花大戰.md "wikilink")5（）
  - [秋之回憶：從今以後](../Page/秋之回憶：從今以後.md "wikilink")（**陵祈**）
  - [秋之回憶：從今以後again](../Page/秋之回憶：從今以後again.md "wikilink")（**陵祈**）
  - [洛克人ZX](../Page/洛克人.md "wikilink")（**梵／艾兒**）
  - [洛克人ZXA](../Page/洛克人.md "wikilink")（**艾兒**）
  - [夢幻奇緣2☆☆☆](../Page/夢幻奇緣2.md "wikilink")（**水璃**）
  - [召喚夜響曲4](../Page/召喚夜響曲4.md "wikilink")（路西安）
  - [棋魂系列](../Page/棋魂.md "wikilink")
      - 棋魂 平安幻想異聞錄（賀茂明）
      - 棋魂2（塔矢亮）
      - 棋魂 院生頂上決戰（塔矢亮）
      - 棋魂3（塔矢亮）
  - [灼眼的夏娜](../Page/灼眼的夏娜.md "wikilink")（美亞）
  - [新 鬼武者 DAWN OF DREAMS](../Page/鬼武者.md "wikilink")（お初）
  - [武裝神姬BATTLE](../Page/武裝神姬.md "wikilink") RONDO（花型MMS Zyrdarya）
  - [天使計畫](../Page/天使計畫.md "wikilink")（[加百列](../Page/加百列.md "wikilink")）
  - [惡魔城 曉月圓舞曲](../Page/惡魔城～曉月的圓舞曲～.md "wikilink")（白馬彌那）
  - [超級機器人大戰系列](../Page/超級機器人大戰.md "wikilink")
      - [超級機器人大戰Z](../Page/超級機器人大戰Z.md "wikilink")（紅麗花）
      - [第2次超級機器人大戰Z
        破界篇](../Page/第2次超級機器人大戰Z_破界篇.md "wikilink")（紅麗花、凱瑟琳·格拉斯、希歐妮·雷吉斯）
      - 第2次超級機器人大戰Z 再世篇（紅麗花、凱瑟琳·格拉斯）
  - [穿越領域計劃](../Page/穿越領域計劃.md "wikilink")（潔蜜妮·桑萊茲）
  - [魔法少女奈叶系列](../Page/魔法少女奈叶.md "wikilink")
      - 魔法少女奈叶A's 王牌之战（**琳芙斯**）
      - 魔法少女奈叶A's 命运齿轮（琳芙斯）
      - [魔法少女奈叶A's The
        Pachinko](../Page/魔法少女奈叶_The_MOVIE_2nd_A's.md "wikilink")（**琳芙斯**）
  - [重力異想世界](../Page/重力異想世界.md "wikilink")（**凱特**）
  - [重力異想世界完結篇](../Page/重力異想世界完結篇.md "wikilink")（**凱特**）

### 廣播劇CD

  - [夢幻奇緣2](../Page/夢幻奇緣2.md "wikilink") 系列（**水璃**）
  - [學生會長是女僕](../Page/學生會長是女僕.md "wikilink")（**鮎澤美咲**）
  - [魔法少女奈叶INNOCENT Sound
    Stage](../Page/魔法少女奈葉_INNOCENT.md "wikilink")（八神琳芙斯·艾茵斯）

### 外國片日文配音

  - Power ranger金剛戰士系列（美版恐龍戰隊系列）
      - Power ranger in Space（**阿什莉／太空黃戰士**）
      - Power ranger in Galaxy（阿什莉／太空黃戰士客串兩集）
  - [雷鳥神機隊](../Page/雷鳥神機隊.md "wikilink")（2004年美國電影版）（潘夫人）
  - [變形金剛3](../Page/變形金剛3.md "wikilink")（卡莉·史賓賽）
  - [童話鎮](../Page/童話鎮.md "wikilink")（**瑪莉·瑪格麗特·布蘭察德=白雪公主**）

## 參考資料

## 外部連結

  - [事務所的介紹頁](http://sigma7.co.jp/profile/w_kobayashi.html)
  - [ANN
    Encyclopedia的資料](http://www.animenewsnetwork.com/encyclopedia/people.php?id=3160)

[Category:日本女性配音員](../Category/日本女性配音員.md "wikilink")
[Category:靜岡縣出身人物](../Category/靜岡縣出身人物.md "wikilink")

1.
2.
3.
4.
5.
6.  第19.5話 劇場上映|accessdate=2016年7月30日|author= |date=
    |publisher=電視動畫「無頭騎士異聞錄 DuRaRaRa\!\!×2」官方網站
    |language=ja}}