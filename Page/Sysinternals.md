**Sysinternals**是一個由位於[德克薩斯州](../Page/德克薩斯州.md "wikilink")[奧斯汀的](../Page/奧斯汀.md "wikilink")**Winternals
Software
LP**公司營運的[網站](../Page/網站.md "wikilink")，原名為**ntinternals**，於1996年由[軟體設計師Bryce](../Page/軟體設計師.md "wikilink")
Cogswell與Mark
Russinovich開辦，並在2006年7月18日被[微軟收購](../Page/微軟.md "wikilink")\[1\]。

該網站提供數個用來管理與監察運行[Microsoft
Windows操作系统電腦的](../Page/Microsoft_Windows.md "wikilink")[免費工具](../Page/免費軟體.md "wikilink")，該公司亦有銷售[資料恢復工具與該些免費工具的專業版](../Page/資料恢復.md "wikilink")。

Mark
Russinovich於2005年10月在Sysinternals網誌上貼出的文章首次揭露了2005年[新力博德曼的](../Page/新力博德曼.md "wikilink")[光碟防複製保護技術醜聞](../Page/新力博德曼#XCP防拷软体争议.md "wikilink")\[2\]。

2006年7月18日[微軟收購了兩個Russinovich的網站](../Page/微軟.md "wikilink")，Russinovich表示Sysinternals將保持現狀直至微軟達成分發該網站工具方法的協議\[3\]，但一個用來恢復Windows密碼的工具NT
Locksmith被立刻移除。

## 產品

Winternals製作了許多-{}-工具，現可在微軟網站免費下載，包括了重組工具[Contig與](../Page/Contig.md "wikilink")[PageDefrag](../Page/PageDefrag.md "wikilink")、診斷工具如[Process
Explorer與](../Page/Process_Explorer.md "wikilink")[RootkitRevealer](../Page/RootkitRevealer.md "wikilink")；以及填補微軟產品中已知分歧的工具，如允許[MS-DOS操作系统讀取](../Page/MS-DOS.md "wikilink")[NTFS磁區的](../Page/NTFS.md "wikilink")[NTFSDOS](../Page/NTFSDOS.md "wikilink")。

## 參考

<references/>

## 外部連結

  - [Sysinternals
    Suite](http://technet.microsoft.com/en-us/sysinternals/bb842062)
  - [Windows
    Sysinternals](http://technet.microsoft.com/zh-tw/sysinternals/default%28en-us%29.aspx)
  - [搶救Windows疑難雜症Windows Sysinternals
    Suite](https://web.archive.org/web/20080922030937/http://www.ithome.com.tw/itadm/article.php?c=44794)

[Category:網站](../Category/網站.md "wikilink")
[Category:1996年成立的公司](../Category/1996年成立的公司.md "wikilink")
[Category:軟體公司](../Category/軟體公司.md "wikilink")
[Category:美國電腦公司](../Category/美國電腦公司.md "wikilink")
[Category:微软的部门与子公司](../Category/微软的部门与子公司.md "wikilink")

1.
2.
3.