[RioGrandedoSul_Municip_Viamao.svg](https://zh.wikipedia.org/wiki/File:RioGrandedoSul_Municip_Viamao.svg "fig:RioGrandedoSul_Municip_Viamao.svg")
**维亚芒**（[葡萄牙语](../Page/葡萄牙语.md "wikilink")：****）是[巴西](../Page/巴西.md "wikilink")[南里奥格兰德州的一座城市](../Page/南里奥格兰德州.md "wikilink")，人口约26万（2006年）。它也是[南里奥格兰德州的第七大城市](../Page/南里奥格兰德州.md "wikilink")。

[V](../Category/巴西城市.md "wikilink")