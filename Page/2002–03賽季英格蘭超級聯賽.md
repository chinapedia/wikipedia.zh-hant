**2002年至2003年度球季**英格蘭超級聯賽，由2002年8月17日起至2003年5月11日止，由[曼聯取得冠軍](../Page/曼聯.md "wikilink")；[阿仙奴一直鬥到最後](../Page/阿仙奴.md "wikilink")，至球季末段後勁不繼才能分出勝負。

## 聯賽積分榜

| **排名** | **球隊**                               | **比賽** | **勝** | **-{zh-hans:平;zh-hk:和;zh-tw:平;}-** | **負** | **進球** | **失球** | **-{zh:得失球差; zh-hans:净胜; zh-hk:得失球;}-** | **積分** |
| ------ | ------------------------------------ | ------ | ----- | ---------------------------------- | ----- | ------ | ------ | --------------------------------------- | ------ |
| 1      | [曼聯](../Page/曼聯.md "wikilink")       | 38     | 25    | 8                                  | 5     | 74     | 34     | \+40                                    | **83** |
| 2      | [阿仙奴](../Page/阿仙奴.md "wikilink")     | 38     | 23    | 9                                  | 6     | 85     | 42     | \+43                                    | **78** |
| 3      | [紐卡素](../Page/紐卡素.md "wikilink")     | 38     | 21    | 6                                  | 11    | 63     | 48     | \+15                                    | **69** |
| 4      | [車路士](../Page/車路士.md "wikilink")     | 38     | 19    | 10                                 | 9     | 68     | 38     | \+30                                    | **67** |
| 5      | [利物浦](../Page/利物浦.md "wikilink")     | 38     | 18    | 10                                 | 10    | 61     | 41     | \+20                                    | **64** |
| 6      | [布力般流浪](../Page/布力般流浪.md "wikilink") | 38     | 16    | 12                                 | 10    | 52     | 43     | \+9                                     | **60** |
| 7      | [愛華頓](../Page/愛華頓.md "wikilink")     | 38     | 17    | 8                                  | 13    | 48     | 49     | \-1                                     | **59** |
| 8      | [修咸頓](../Page/修咸頓.md "wikilink")     | 38     | 13    | 13                                 | 12    | 43     | 46     | \-3                                     | **52** |
| 9      | [曼城](../Page/曼城.md "wikilink")       | 38     | 15    | 6                                  | 17    | 47     | 54     | \-7                                     | **51** |
| 10     | [熱刺](../Page/熱刺.md "wikilink")       | 38     | 14    | 8                                  | 16    | 51     | 62     | \-11                                    | **50** |
| 11     | [米杜士堡](../Page/米杜士堡.md "wikilink")   | 38     | 13    | 10                                 | 15    | 48     | 44     | \+4                                     | **49** |
| 12     | [查爾頓](../Page/查爾頓.md "wikilink")     | 38     | 14    | 7                                  | 17    | 45     | 56     | \-11                                    | **48** |
| 13     | [伯明翰](../Page/伯明翰.md "wikilink")     | 38     | 13    | 9                                  | 16    | 41     | 49     | \-8                                     | **48** |
| 14     | [富咸](../Page/富咸.md "wikilink")       | 38     | 13    | 9                                  | 16    | 41     | 50     | \-9                                     | **48** |
| 15     | [列斯聯](../Page/列斯聯.md "wikilink")     | 38     | 14    | 5                                  | 19    | 58     | 57     | \+1                                     | **47** |
| 16     | [阿士東維拉](../Page/阿士東維拉.md "wikilink") | 38     | 12    | 9                                  | 17    | 42     | 47     | \-5                                     | **45** |
| 17     | [保頓](../Page/保頓.md "wikilink")       | 38     | 10    | 14                                 | 14    | 41     | 51     | \-10                                    | **44** |
| 18     | [韋斯咸](../Page/韋斯咸.md "wikilink")     | 38     | 10    | 12                                 | 16    | 42     | 59     | \-17                                    | **42** |
| 19     | [西布朗](../Page/西布朗.md "wikilink")     | 38     | 6     | 8                                  | 24    | 29     | 65     | \-36                                    | **26** |
| 20     | [新特蘭](../Page/新特蘭.md "wikilink")     | 38     | 4     | 7                                  | 27    | 21     | 65     | \-44                                    | **19** |

<table>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>獲得參與來屆<a href="../Page/歐洲聯賽冠軍杯.md" title="wikilink">歐洲聯賽冠軍杯分組賽資格</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>獲得參與來屆<a href="../Page/歐洲聯賽冠軍杯.md" title="wikilink">歐洲聯賽冠軍杯外圍賽第三圈資格</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>獲得參與來屆<a href="../Page/歐洲足協杯.md" title="wikilink">歐洲足協杯第一圈資格</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>降班至<a href="../Page/英格兰足球联赛.md" title="wikilink">英格蘭足球甲組（第二級）聯賽</a></p></td>
</tr>
<tr class="even">
<td><ol>
<li><p>修咸頓獲得<a href="../Page/英格蘭足總盃.md" title="wikilink">足總盃亞軍</a>（冠軍球隊阿仙奴已取得歐聯席位）</p></li>
<li><p>曼城獲得歐洲足協公平競技獎</p></li>
</ol></td>
<td></td>
</tr>
</tbody>
</table>

## 射手榜

<table>
<thead>
<tr class="header">
<th><p><strong>進球</strong></p></th>
<th><p><strong>球員</strong></p></th>
<th><p><strong>效力球會</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>25</strong></p></td>
<td><p><a href="../Page/雲尼斯杜萊.md" title="wikilink">雲尼斯杜萊</a></p></td>
<td><p><a href="../Page/曼聯.md" title="wikilink">曼聯</a></p></td>
</tr>
<tr class="even">
<td><p><strong>24</strong></p></td>
<td><p><a href="../Page/亨利.md" title="wikilink">亨利</a></p></td>
<td><p><a href="../Page/阿仙奴.md" title="wikilink">阿仙奴</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>23</strong></p></td>
<td><p><a href="../Page/占士·比亞堤.md" title="wikilink">比亞堤</a></p></td>
<td><p><a href="../Page/修咸頓.md" title="wikilink">修咸頓</a></p></td>
</tr>
<tr class="even">
<td><p><strong>20</strong></p></td>
<td><p><a href="../Page/維杜卡.md" title="wikilink">維杜卡</a></p></td>
<td><p><a href="../Page/列斯聯.md" title="wikilink">列斯聯</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>19</strong></p></td>
<td><p><a href="../Page/奧雲.md" title="wikilink">奧雲</a></p></td>
<td><p><a href="../Page/利物浦.md" title="wikilink">利物浦</a></p></td>
</tr>
<tr class="even">
<td><p><strong>17</strong></p></td>
<td><p><a href="../Page/舒利亞.md" title="wikilink">舒利亞</a></p></td>
<td><p><a href="../Page/紐卡素.md" title="wikilink">紐卡素</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>14</strong></p></td>
<td><p><a href="../Page/安歷卡.md" title="wikilink">安歷卡</a></p></td>
<td><p><a href="../Page/曼城.md" title="wikilink">曼城</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/羅比·堅尼.md" title="wikilink">羅比·堅尼</a></p></td>
<td><p><a href="../Page/熱刺.md" title="wikilink">熱刺</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/基維爾.md" title="wikilink">基維爾</a></p></td>
<td><p><a href="../Page/列斯聯.md" title="wikilink">列斯聯</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/皮里斯.md" title="wikilink">皮里斯</a></p></td>
<td><p><a href="../Page/阿仙奴.md" title="wikilink">阿仙奴</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/史高斯.md" title="wikilink">史高斯</a></p></td>
<td><p><a href="../Page/曼聯.md" title="wikilink">曼聯</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/蘇拿.md" title="wikilink">蘇拿</a></p></td>
<td><p><a href="../Page/車路士.md" title="wikilink">車路士</a></p></td>
<td></td>
</tr>
</tbody>
</table>

## 其他數據

|                                                                        |                                                                         |
| ---------------------------------------------------------------------- | ----------------------------------------------------------------------- |
| 總比賽場數                                                                  | 380                                                                     |
| 總入球數                                                                   | 1000                                                                    |
| 主隊勝出場數                                                                 | 187(49.2%)                                                              |
| 客隊勝出場數                                                                 | 103(27.1%)                                                              |
| 和局場數                                                                   | 90(23.7%)                                                               |
| 主隊平均入球                                                                 | 1.50                                                                    |
| 客隊平均入球                                                                 | 1.13                                                                    |
| 每場平均入球                                                                 | 2.63                                                                    |
| 入球多於2.5球場數                                                             | 194(51.1%)                                                              |
| 入球少於2.5球場數                                                             | 186(48.9%)                                                              |
| 平均每週入球數                                                                | 26.32                                                                   |
| 入球最多的比賽                                                                | 11月23日[曼聯](../Page/曼聯.md "wikilink")5-3[紐卡素](../Page/紐卡素.md "wikilink") |
| 2月23日[紐卡素](../Page/紐卡素.md "wikilink")2-6[曼聯](../Page/曼聯.md "wikilink") |                                                                         |

[Category:英格蘭足球超級聯賽](../Category/英格蘭足球超級聯賽.md "wikilink")
[Category:2002年至2003年本土足球联赛](../Category/2002年至2003年本土足球联赛.md "wikilink")