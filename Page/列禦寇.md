**列禦寇**，或称列圄寇，[春秋时期](../Page/春秋时期.md "wikilink")\[1\][郑國人](../Page/鄭國_\(諸侯國\).md "wikilink")，[道家学派的先驱者](../Page/道家.md "wikilink")，人称**列子**，主张贵虚。

列禦寇於《[史記](../Page/史記.md "wikilink")》無傳，其名散見於《[莊子](../Page/莊子_\(書\).md "wikilink")》、《[管子](../Page/管子_\(书\).md "wikilink")》、《[晏子](../Page/晏子春秋.md "wikilink")》、《[墨子](../Page/墨子_\(书\).md "wikilink")》、《[韓非子](../Page/韓非子.md "wikilink")》、《[-{尸}-子](../Page/尸子_\(書\).md "wikilink")》\[2\]、《[呂氏春秋](../Page/呂氏春秋.md "wikilink")》\[3\]等書。列禦寇成名于《[列子](../Page/列子.md "wikilink")》一书，有章以其名为章名，主旨在于宣扬不可炫智于外而应养神于心，达到“天而不入”顺从自然，达到无用之用的境界。

列子是郑国人\[4\]，先於[莊子](../Page/莊子.md "wikilink")\[5\]，与[郑缪公同时](../Page/郑缪公.md "wikilink")。其学本于[黄帝](../Page/黄帝.md "wikilink")[老子](../Page/老子.md "wikilink")，主张清静无为。相传他曾向[关尹子问道](../Page/关尹子.md "wikilink")，拜[壶丘子为师](../Page/壶丘子.md "wikilink")，壺子對列子說：“鄉吾示之以未始出吾宗。吾與之虛而委蛇，不知其誰何，因以為弟靡，因以為波流，故逃也。”後来又先後师事[老商氏和](../Page/老商氏.md "wikilink")[支伯高子](../Page/支伯高子.md "wikilink")，修道九年之後，他就能御风而行\[6\]。[唐玄宗](../Page/唐玄宗.md "wikilink")[天宝年间诏封为](../Page/天宝.md "wikilink")“冲虚真人”，[宋徽宗](../Page/宋徽宗.md "wikilink")[宣和年间封为](../Page/宣和.md "wikilink")“冲虚观妙真君”。

## 列子故里

列子故里位于河南[郑州市东区的圃田一带](../Page/郑州市.md "wikilink")。2009年6月，位于郑州市[管城区](../Page/管城区.md "wikilink")[圃田乡的列子祠成为市级文保单位](../Page/圃田乡.md "wikilink")。\[7\]\[8\]

## 参考文献

## 外部链接

## 参见

  - [道家](../Page/道家.md "wikilink")
  - 《[列子](../Page/列子.md "wikilink")》

{{-}}

[Category:诸子百家](../Category/诸子百家.md "wikilink")
[Category:春秋戰國人物](../Category/春秋戰國人物.md "wikilink")
[Category:神仙](../Category/神仙.md "wikilink")
[Category:列姓](../Category/列姓.md "wikilink")
[Category:道家](../Category/道家.md "wikilink")
[Category:郑国人](../Category/郑国人.md "wikilink")
[Category:郑州人](../Category/郑州人.md "wikilink")

1.  列子的生存年代並不能確定，從鄭繆公(前627年在位)至[鄭康公](../Page/鄭康公.md "wikilink")(前376年為韓哀侯所滅)的二百五十年間皆有列子活動記錄。
2.  《爾雅疏》引《-{尸}-子·廣澤篇》：“[墨子貴兼](../Page/墨子.md "wikilink")，[孔子貴公](../Page/孔子.md "wikilink")，皇子貴衷，[田子貴均](../Page/田子.md "wikilink")，列子貴虛，[料子貴別囿](../Page/料子.md "wikilink")，其學之相非也數世矣，而己皆弇於私也。”
3.  《呂氏春秋·不二篇》載“列子貴虛”。
4.  [劉向撰](../Page/劉向.md "wikilink")《列子新書目錄》說：“列子者，鄭人也”
5.  《[漢書](../Page/漢書.md "wikilink")·藝文志》著錄《列子》八篇，注曰：“名圄寇，先莊子，莊子稱之。”
6.  《庄子·逍遥游》说：“列子御风而行，泠然善也，旬有五日而后反。”
7.
8.