**张松溪**，[明](../Page/明.md "wikilink")[嘉靖時](../Page/嘉靖.md "wikilink")[浙江](../Page/浙江.md "wikilink")[鄞县](../Page/鄞县.md "wikilink")（[宁波](../Page/宁波.md "wikilink")）人，著名拳術家。個性沈毅寡言，恂恂有如儒者，其師父為大梁街人孫十三老\[1\]\[2\]。

張松溪練拳有五字訣，即「勤、緊、徑、敬、切」。傳徒僅三、四人，以葉近泉為之最。得近泉之傳者，有吳崑山、周雲泉、單思南、陳貞石、孫繼槎等人，皆各有授受。崑山傳李天目、徐岱岳。天目傳餘波仲、陳茂弘、吳七郎。雲泉傳盧紹岐。貞石傳夏枝溪、董扶輿。繼槎傳柴元明、姚石門、僧耳、僧尾。而思南之傳，則有[王征南](../Page/王征南.md "wikilink")。

張松溪所習拳術源自[南北朝](../Page/南北朝.md "wikilink")[劉宋](../Page/劉宋.md "wikilink")[張三豐所傳之](../Page/張三豐.md "wikilink")[內家拳](../Page/內家拳.md "wikilink")，而非[太極拳](../Page/太極拳.md "wikilink")。

他終身未娶，一生侍奉母親，最後於家鄉去世\[3\]。

## 金庸小說中的張松溪

张松溪也是[金庸小说](../Page/金庸小说.md "wikilink")《[倚天屠龍記](../Page/倚天屠龍記.md "wikilink")》中的人物，被設定為是[张三丰的第四个徒弟](../Page/张三丰.md "wikilink")，最足智多謀。

曾參與六大派遠征[光明頂一戰](../Page/光明頂.md "wikilink")，力戰[明教](../Page/明教.md "wikilink")[五行旗](../Page/五行旗.md "wikilink")，殺入光明頂後，與[明教四大護教法王之一](../Page/明教.md "wikilink")「白眉鷹王」[殷天正比內力](../Page/殷天正.md "wikilink")。

下山時[武當派和六大派也被](../Page/武當派.md "wikilink")[蒙古軍抓去](../Page/蒙古.md "wikilink")[萬安寺幽禁](../Page/萬安寺.md "wikilink")，各人被逼服下「十香軟筋散」，以致使不出武功，最後連同六大派被明教第三十四代教主[張無忌解救出](../Page/張無忌.md "wikilink")。

屠獅英雄會過後不久，適逢[元朝](../Page/元朝.md "wikilink")「汝陽王」[察罕帖木儿率二萬蒙古軍攻上](../Page/察罕帖木儿.md "wikilink")[少室山](../Page/少室山.md "wikilink")[少林寺打算一舉纖滅武林羣雄](../Page/少林寺.md "wikilink")，张松溪也有份與武林羣雄殺敵報國。

## 參見

  - [武當派](../Page/武當派.md "wikilink")
  - [太極拳](../Page/太極拳.md "wikilink")
  - [拳術發展歷史](../Page/拳術發展歷史.md "wikilink")

## 參考文獻

<div class="references-small">

<references>

</references>

</div>

## 延伸閱讀

  - 《少林武當考》[唐豪著](../Page/唐豪.md "wikilink")
  - 《國技論略》[徐哲東](../Page/徐哲東.md "wikilink")([徐震](../Page/徐震.md "wikilink"))著
  - 《搏者·張松溪傳》[沈一貫](../Page/沈一貫.md "wikilink")
  - 《寧波府誌．張松溪傳》明[張時徹等編](../Page/張時徹.md "wikilink")
  - 《太極拳勢圖解》許禹生著，1921年出版
  - 《太極拳之研究》[吳圖南](http://ja.wikipedia.org/wiki/%E5%91%89%E5%9B%B3%E5%8D%97)著，香港商務出版，1984年版

## 影視形象

  - [廖偉雄](../Page/廖偉雄.md "wikilink")：1978 無綫電視《倚天屠龍記》
  - [姜大川](../Page/姜大川.md "wikilink")：1984 台灣台視《倚天屠龍記》
  - [艾威](../Page/艾威.md "wikilink")：1986無綫電視 《倚天屠龍記》
  - [李岡](../Page/李岡.md "wikilink")：2001 無綫電視《倚天屠龍記》
  - [李晟毓](../Page/李晟毓.md "wikilink")：2003 台灣華視《倚天屠龍記》
  - 郭軍：2009 中國《倚天屠龍記》
  - [曲吉](../Page/曲吉.md "wikilink")：2019 中國《倚天屠龍記》

[Z張](../Category/明朝武術家.md "wikilink") [S](../Category/张姓.md "wikilink")
[Z張](../Category/中国武术家.md "wikilink")
[Z張](../Category/鄞县人.md "wikilink")
[倚](../Category/金庸筆下真實人物.md "wikilink")
[Z張](../Category/倚天屠龙记角色.md "wikilink")

1.  《宁波府志：張松溪傳》
2.  沈一貫，《搏者張松溪傳》
3.