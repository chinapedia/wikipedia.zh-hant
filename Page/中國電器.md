**中國電器股份有限公司**，簡稱**[中電](../Page/中電.md "wikilink")**、**[CE](../Page/CE.md "wikilink")**，成立於1955年2月1日，為[台灣一家專業](../Page/台灣.md "wikilink")[照明製造服務](../Page/照明.md "wikilink")[公司](../Page/公司.md "wikilink")。1962年上市，沿用至今。

## 歷史

[China_Electric_headquarters_20161031.jpg](https://zh.wikipedia.org/wiki/File:China_Electric_headquarters_20161031.jpg "fig:China_Electric_headquarters_20161031.jpg")[中正區](../Page/中正區_\(臺北市\).md "wikilink")[忠孝東路二段](../Page/忠孝東路.md "wikilink")，中國電器舊總部\]\]
[China_Electric_headquarters_with_Chen_Cheng's_calligraphy_2010-10-05.jpg](https://zh.wikipedia.org/wiki/File:China_Electric_headquarters_with_Chen_Cheng's_calligraphy_2010-10-05.jpg "fig:China_Electric_headquarters_with_Chen_Cheng's_calligraphy_2010-10-05.jpg")

  - 1955年2月1日：中電成立，總部由[陳誠題字](../Page/陳誠.md "wikilink")。（見圖）
  - 1956年：中電向[東芝購買技術](../Page/東芝.md "wikilink")，以生產[電燈泡為主](../Page/電燈泡.md "wikilink")，但因經營體質不佳與[玻璃生產過剩而](../Page/玻璃.md "wikilink")[虧損以致被迫](../Page/虧損.md "wikilink")[減資](../Page/減資.md "wikilink")。中電[總經理](../Page/總經理.md "wikilink")[陳全生親赴](../Page/陳全生.md "wikilink")[日本說服](../Page/日本.md "wikilink")[三菱電機與](../Page/三菱電機.md "wikilink")[三菱商事籌資](../Page/三菱商事.md "wikilink")[新台幣](../Page/新台幣.md "wikilink")1000萬元，三菱電機與三菱商事協助中電轉型並給予[日光燈生產技術](../Page/日光燈.md "wikilink")。
  - 1962年：中電以「東亞」（TOA）為品牌行銷於台灣市場。
  - 1963年4月：中電[新竹廠落成](../Page/新竹縣.md "wikilink")，正式開始生產東亞日光燈。
  - 1967年10月：中電向日本[來福電球購買技術](../Page/來福電球.md "wikilink")，生產[汽車專用電燈泡](../Page/汽車.md "wikilink")。
  - 1969年：中電與三菱電機購買技術，生產[水銀燈](../Page/水銀燈.md "wikilink")。
  - 1973年6月1日：從中電新竹廠分離出來的[桃園縣](../Page/桃園市.md "wikilink")[桃園市](../Page/桃園區.md "wikilink")（今桃園市桃園區）中電桃園廠正式開工，主要生產[燈具與](../Page/燈具.md "wikilink")[安定器](../Page/安定器.md "wikilink")。
  - 1979年5月：中電成立「電氣測試室」，後改名「電性測試[實驗室](../Page/實驗室.md "wikilink")」。
  - 1989年8月5日：中電[新營廠落成](../Page/新營區.md "wikilink")、開工，佔地3000坪。
  - 1990年1月16日：中電[股票在](../Page/股票.md "wikilink")[台灣證券交易所上市](../Page/台灣證券交易所.md "wikilink")。
  - 1993年：中電向瑞典購買台灣第一套[靜電塗裝燈泡自動生產設備](../Page/靜電塗裝燈泡.md "wikilink")。
  - 1991年：中電成立「產品研發群」，研發新產品。
  - 1994年：中電向CKD購買台灣第一套「一秒一支」日光燈高速生產設備。
  - 1996年9月：通過[中華民國實驗室認證委員會評鑑](../Page/中華民國實驗室認證委員會.md "wikilink")，取得[中華民國實驗室認證體系](../Page/中華民國實驗室認證體系.md "wikilink")（CNLA）認證，可接受對內、對外委託測試業務。
  - 1997年：中電與三菱電機合資成立[美東菱公司](../Page/美東菱公司.md "wikilink")（Meltonic Co.,
    Ltd.），主要生產電子安定器。
  - 1999年：中電購買台灣第一套「3U燈管」自動化生產設備及第二套「一秒一支」日光燈管高速生產設備。
  - 2001年：中電向[西班牙](../Page/西班牙.md "wikilink")[Isofoton購買技術](../Page/Isofoton.md "wikilink")，開發[太陽能與](../Page/太陽能.md "wikilink")[風能](../Page/風能.md "wikilink")[光電照明系統](../Page/光電.md "wikilink")。
  - 2002年：中電桃園廠設置太陽能光電[模板封裝生產線](../Page/模板.md "wikilink")。
  - 2009年1月7日：中電與美國Cree Inc.簽署合作協定。
  - 2009年2月9日：中電與美國Cree Inc.簽署技術及市場開發合約。
  - 2009年3月：中電轉投資[鋰鐵電池製造商](../Page/鋰鐵電池.md "wikilink")[冠碩電池](../Page/冠碩電池.md "wikilink")。
  - 2009年7月28日：中電[董事會通過以新台幣](../Page/董事會.md "wikilink")4.06億元取得[台北市](../Page/台北市.md "wikilink")[內湖區舊宗路](../Page/內湖區.md "wikilink")42-9與42-10號地，總面積為405坪，將興建廠辦大樓；同日，中電發表新品牌「東亞綠能」，並與冠碩電池共同舉辦「跨世代綠能產品發表會」。
  - 2010年3月:中電榮獲[台灣區照明燈具輸出業同業公會](../Page/台灣區照明燈具輸出業同業公會.md "wikilink")「照明菁英獎」之照明產業典範獎。
  - 2010年10月:中電榮獲[中華民國經濟部第](../Page/中華民國經濟部.md "wikilink")11屆公司標準化獎。
  - 2011年3月:中電在[南科](../Page/南科.md "wikilink")[樹谷園區購置土地](../Page/樹谷園區.md "wikilink")。
  - 2011年5月:中電榮獲[行政院勞工委員會](../Page/行政院勞工委員會.md "wikilink")99年度推行無災害工時紀錄績優單位。
  - 2012年1月:中電增購南科樹谷園區土地。
  - 2013年10月:中電公開收購[啟耀光電](../Page/啟耀光電.md "wikilink")，與啟耀光電[策略聯盟生產LED產品](../Page/策略聯盟.md "wikilink")。

## 外部連結

  - [中國電器](http://www.chinaelectric.com.tw/)

[Category:臺灣家用電器製造商](../Category/臺灣家用電器製造商.md "wikilink")
[Z中](../Category/家用電器品牌.md "wikilink")
[1](../Category/臺灣證券交易所上市公司.md "wikilink")
[Z中](../Category/1955年成立的公司.md "wikilink")
[Z中](../Category/總部位於臺北市松山區的工商業機構.md "wikilink")