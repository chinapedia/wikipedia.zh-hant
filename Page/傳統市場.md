[Pasig_City_3.jpg](https://zh.wikipedia.org/wiki/File:Pasig_City_3.jpg "fig:Pasig_City_3.jpg")的傳統市場\]\]
[HK_Wanchai_Market_Stone_Nullah_Lane.jpg](https://zh.wikipedia.org/wiki/File:HK_Wanchai_Market_Stone_Nullah_Lane.jpg "fig:HK_Wanchai_Market_Stone_Nullah_Lane.jpg")
[Niamtougou4.jpg](https://zh.wikipedia.org/wiki/File:Niamtougou4.jpg "fig:Niamtougou4.jpg")[尼亞姆圖古的](../Page/尼亞姆圖古.md "wikilink")[非洲傳統市場](../Page/非洲.md "wikilink")\]\]
[Wet_market_in_Tekka_Centre,_Singapore_-_20030228.jpg](https://zh.wikipedia.org/wiki/File:Wet_market_in_Tekka_Centre,_Singapore_-_20030228.jpg "fig:Wet_market_in_Tekka_Centre,_Singapore_-_20030228.jpg")的傳統市場也稱作**巴刹**（Bazaar）\]\]
[Tai_Yuen_Market_Interior_2010-1.JPG](https://zh.wikipedia.org/wiki/File:Tai_Yuen_Market_Interior_2010-1.JPG "fig:Tai_Yuen_Market_Interior_2010-1.JPG")內貌\]\]
**-{zh-hans:传统市场;zh-hant:傳統市場;zh-hk:街市;}-**又可稱為**-{zh-hans:街市;zh-hant:街市;zh-hk:傳統市場;}-**或**墟市**，是一種相對於[超級市場的賣場](../Page/超級市場.md "wikilink")，通常在一個[社區中會有一個](../Page/社區.md "wikilink")。它所販賣的商品主要是各式各樣新鮮的[蔬菜](../Page/蔬菜.md "wikilink")、尚未宰殺的[雞](../Page/雞.md "wikilink")、[魚等等](../Page/魚.md "wikilink")，除此之外，在這之中，也可能有[餐飲店](../Page/餐館.md "wikilink")、[雜貨店](../Page/雜貨店.md "wikilink")、[服飾店等商店座落於其中](../Page/服飾.md "wikilink")。傳統市場[新年前會賣](../Page/新年.md "wikilink")[年貨](../Page/年貨.md "wikilink")（[年宵市場](../Page/年宵市場.md "wikilink")）；以營業時間區分，專賣早中午的為「**早市**」，專賣下午至[通勤下班時間的為](../Page/通勤.md "wikilink")「**黃昏市場**」，夜晚營業則為「**[夜市](../Page/夜市.md "wikilink")**」。

由於[超級市場的出現](../Page/超級市場.md "wikilink")，搶去傳統市場不少客源，但因顧客的購物習慣（主要受年齡影響）不同，傳統市場仍然佔有一定的地位。傳統市場可讓人[殺價或要求](../Page/議價.md "wikilink")[贈品](../Page/贈品.md "wikilink")；有些人也認為傳統市場販賣的食材較新鮮。

## 歷史

傳統市場是集市，西方社會也有，或者叫作[跳蚤市場](../Page/跳蚤市場.md "wikilink")。[農村經濟時代](../Page/農村經濟.md "wikilink")，[商業不發達](../Page/商業.md "wikilink")，墟市不是每天24小時一年365日存在的。農民生產出現有余，所以農民之間互通有無，可以是[以物易物或者用](../Page/以物易物.md "wikilink")[貨幣購買](../Page/貨幣.md "wikilink")。是一種約定時日墟期出現的傳統市場，通常是秋收之後。墟期過後，人潮散去，買賣雙方又要等下一次墟期。

## 環境

傳統市場的环境有封闭的、半封閉、以及露天的。即使是露天的，許多攤商也會自備遮陽棚或遮雨棚。

有些傳統市場在環境上並不讓人感到十分舒適，地上髒亂且多有水漬。此外，市場中人潮洶湧，需要花費較多時間來選取商品，且由于人们互相讲价，所以傳統市場的吵杂声非常的大。

## 各地情況

### 中国大陆

目前，大多数的傳統市場主要存在[中国大陆的中小城市](../Page/中国大陆.md "wikilink")，但是大城市像[北京](../Page/北京.md "wikilink")、[上海也有](../Page/上海.md "wikilink")，只是由于大型跨国超市的进入，為数並不多。大多大陆传统市场属于[农贸市场](../Page/农贸市场.md "wikilink")。在中国大陆的城市，由于经济的发展，现在人们购买食品，越来越多地去往超市。但是喜欢讲价的人，还是去傳統市場。尤其是中老年妇女喜欢去那里购买食品。

### 香港

香港現存著名的傳統市場有[大埔墟](../Page/大埔墟.md "wikilink")、[聯和墟](../Page/聯和墟.md "wikilink")、[元朗墟](../Page/元朗墟.md "wikilink")、[旺角花墟](../Page/旺角花墟.md "wikilink")、[金魚街](../Page/金魚街.md "wikilink")、[女人街](../Page/女人街.md "wikilink")、西營盤[正街](../Page/正街.md "wikilink")、[北角](../Page/北角.md "wikilink")[春秧街和](../Page/春秧街.md "wikilink")[筲箕灣東大街等等](../Page/筲箕灣東大街.md "wikilink")，[香港房屋委員會在興建公共房屋時亦會在部分屋邨興建](../Page/香港房屋委員會.md "wikilink")「街市」，大部分均已在2005年售予[領匯房地產信託基金](../Page/領匯房地產信託基金.md "wikilink")（已改名為[領展房地產信託基金](../Page/領展房地產信託基金.md "wikilink")）。此外少數發展商在興建私人屋苑時亦會興建「街市」，如[置富花園](../Page/置富花園.md "wikilink")、[新港城](../Page/新港城.md "wikilink")、[新都城等](../Page/新都城.md "wikilink")。香港早年的傳統市場多設於[街道旁邊](../Page/街道.md "wikilink")，故稱為「街市」（/）。由於現時很多街市已遷入[食物環境衞生署管理的市政大廈內的室內地方](../Page/食物環境衞生署.md "wikilink")，所以現在已經甚少見墟市，而且現在的墟已經不再是指街市，而是廣泛用作一個地方的名稱，例如[大埔墟及](../Page/大埔墟.md "wikilink")[聯和墟](../Page/聯和墟.md "wikilink")，現時兩地的街市稱為[大埔墟街市及](../Page/大埔墟街市.md "wikilink")[聯和墟街市](../Page/聯和墟街市.md "wikilink")。

領展及其[外判街市承辦商曾將旗下不少街市翻新](../Page/領展街市外判化.md "wikilink")，首個翻新的街市為[大元街市](../Page/大元街市.md "wikilink")。另外，領展亦會將部分街市轉為零售店舖（如天耀街市、黃大仙中心街市），也會將部分街市擴大面積，將部分商場面積改為街市（如天盛街市、翻新中的TKO
Gateway街市）。經領展翻新後的街市一般通道較寬闊，環境也較佳，亦提供電子付款服務，部分街市更引入特色裝修和檔舖（如逸東街市翻新後改名香港街市，並以九龍城為造型），但由於翻新後租金上升，甚至原有檔戶不能繼續在翻新後的街市經營，而部分街市承辦商亦同時經營檔舖，因此被部分關注團體非議。

此外，部分地區的小販會趁天未光時在街上甚至一些公共地方擺檔，稱為[天光墟](../Page/天光墟.md "wikilink")。

由於香港曾在1997年爆發[禽流感](../Page/禽流感.md "wikilink")，香港對售賣活禽有嚴格限制，亦導致不少販商改售預先屠宰的冰鮮肉類。

WetmarketHK.jpg|香港室內「街市」的[豬肉店](../Page/豬肉.md "wikilink") HK Yau Ma Tei
night Shanghai Street 眾坊街夜市場 Public Square Street market stall Laser
lighting Apr-2013.JPG|採用雷射裝飾的菜市場

### 澳門

隨著[澳門人口的發展](../Page/澳門人口.md "wikilink")，澳門區內出現了不少大小[街市](../Page/街市.md "wikilink")。除了一些獨立建築的[街市](../Page/街市.md "wikilink")，澳門還有一些露天的[街市](../Page/街市.md "wikilink")，如：[義字街](../Page/義字街.md "wikilink")、[青草街等](../Page/青草街.md "wikilink")。昔日澳門的街市大多不只是菜市場的單一用途。例如昔日在海傍的[祐漢街市](../Page/祐漢街市.md "wikilink")，除菜市場功能外，其“美基內市場圍”是社區活動中心。另一些昔日澳門的街市，與[戲院連結在一起](../Page/戲院.md "wikilink")，戲院設在樓上，街市在樓下。昔日澳門古老的街市，已隨社會的變遷而消失、擴建或遷址，被現代化的街市所取代。

### 臺灣

[臺灣雖然擁有高密度的](../Page/臺灣.md "wikilink")[超市和](../Page/超市.md "wikilink")[便利商店](../Page/便利商店.md "wikilink")，但傳統市場仍十分興盛，在大都市亦然。臺灣有一些公共街市（如[臺北市公有南門市場](../Page/南門市場_\(臺北\).md "wikilink")），也有觀光夜市（如[士林夜市](../Page/士林夜市.md "wikilink")）。由於傳統市場可議價及濃厚的人情味等，因此有不少人仍偏好到傳統市場購買食品。

臺灣許多市場擁有非常久遠之歷史，幾乎與聚落同時形成。但諸多早年市場衛生情況不佳，政府已著手輔導改建並定期抽查。

## 參見

  - [农贸市场](../Page/农贸市场.md "wikilink")
  - [超級市場](../Page/超級市場.md "wikilink")
  - [便利店](../Page/便利店.md "wikilink")
  - [雜貨店](../Page/雜貨店.md "wikilink")
  - [士多](../Page/士多.md "wikilink")

[\*](../Category/传统市场.md "wikilink")