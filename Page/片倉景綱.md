**片倉景綱**（）是[日本戰國時代至](../Page/日本戰國時代.md "wikilink")[江戶時代前期的武將](../Page/江戶時代.md "wikilink")。[伊達氏家臣](../Page/伊達氏.md "wikilink")。[伊達政宗的近習](../Page/伊達政宗.md "wikilink")，後來擔任[軍師](../Page/軍師.md "wikilink")。[仙台藩](../Page/仙台藩.md "wikilink")[片倉氏初代](../Page/片倉氏.md "wikilink")，通稱「**小十郎**」成為片倉家代代相傳世襲的當家名稱。父親是[置賜郡](../Page/置賜郡.md "wikilink")[永井庄](../Page/永井庄.md "wikilink")[八幡神社](../Page/八幡神社.md "wikilink")（現今[成島八幡神社](../Page/成島八幡神社.md "wikilink")）的[神職](../Page/神職_\(神道\).md "wikilink")[片倉景重](../Page/片倉景重.md "wikilink")。

異父同母的姐姐[喜多是政宗的](../Page/片倉喜多.md "wikilink")[乳母](../Page/乳母.md "wikilink")。而[鬼庭綱元](../Page/鬼庭綱元.md "wikilink")（喜多同父異母的弟弟）是義理上的兄長。

## 生涯

[弘治](../Page/弘治_\(後奈良天皇\).md "wikilink")3年（1557年）出生，為家中次男。年幼時，雙親相繼死去，因為與姐姐喜多相差近20歲，喜多等同於其母親而養育他，不久後，以養子身份被送到親戚藤田家中，不過因為藤田家中誕下男子，於是回到喜多身邊，兩人再次一起生活。姐姐喜多被認為[文武兩道](../Page/文武兩道.md "wikilink")，又喜好[兵書](../Page/兵書.md "wikilink")，因此景綱自小也受喜多的親自教育。

[永祿](../Page/永祿.md "wikilink")10年（1567年），主君[伊達輝宗誕下嫡子](../Page/伊達輝宗.md "wikilink")[政宗後](../Page/伊達政宗.md "wikilink")，姐姐喜多被任命為政宗的[乳母](../Page/乳母.md "wikilink")。[天正初年期間](../Page/天正_\(日本\).md "wikilink")，伊達家城下的[米澤發生大火](../Page/米澤.md "wikilink")，此時景綱的活躍得到承認，於是以徒[小姓的身份服侍輝宗](../Page/小姓.md "wikilink")。此後，因為[遠藤基信推薦而在天正](../Page/遠藤基信.md "wikilink")3年（1575年）成為政宗的近侍，後來成為重臣而得到其重用。

此後，參與天正13年（1585年）的[人取橋之戰](../Page/人取橋之戰.md "wikilink")、天正16年（1588年）的[郡山合戰](../Page/郡山合戰.md "wikilink")、天正17年（1589年）的[摺上原之戰](../Page/摺上原之戰.md "wikilink")、天正18年（1590年）的[小田原征伐](../Page/小田原征伐.md "wikilink")、[文祿](../Page/文祿.md "wikilink")2年（1593年）的[文祿慶長之役](../Page/文祿慶長之役.md "wikilink")、[慶長](../Page/慶長.md "wikilink")5年（1600年）的[關原之戰等大部份政宗參與的主要戰役](../Page/關原之戰.md "wikilink")，數度化解[伊達氏的危機](../Page/伊達氏.md "wikilink")。其中，在小田原征伐之際，建議政宗應加入[豐臣秀吉一方參戰](../Page/豐臣秀吉.md "wikilink")，令政宗因此決定前往小田原參戰。

此外還被任命為[城代](../Page/城代.md "wikilink")，曾擔任[安達郡](../Page/安達郡.md "wikilink")[二本松城的在番](../Page/二本松城.md "wikilink")、[信夫郡](../Page/信夫郡.md "wikilink")[大森城城主](../Page/大森城.md "wikilink")，在[奧州仕置後](../Page/奧州仕置.md "wikilink")，被任命為[佐沼城城主](../Page/佐沼城.md "wikilink")、[亙理城城主等](../Page/亙理城.md "wikilink")。亦擔當伊達氏對外交渉的[取次](../Page/取次.md "wikilink")（類似代理人的角色），政宗發出的外交文書中，多數還會附上景綱的副狀。

關原之戰後的慶長7年（1602年），主君政宗成為[仙台藩主後](../Page/仙台藩.md "wikilink")，雖然[德川幕府頒佈](../Page/德川幕府.md "wikilink")[一國一城令](../Page/一國一城令.md "wikilink")，但片倉家被允許破例成為[白石城](../Page/白石城.md "wikilink")1万3千石的城主，但因為生病，於是景綱在亙理領內的神宮寺村接受療養，慶長10年（1605年）春天，移至白石城。

慶長19年（1614年）開始的[大坂之陣時發病](../Page/大坂之陣.md "wikilink")，無法跟隨政宗參戰，於是命令兒子[重長代他參戰](../Page/片倉重長.md "wikilink")。[元和元年](../Page/元和_\(後水尾天皇\).md "wikilink")（1615年）病逝，享年59歲。[家督由兒子重長](../Page/家督.md "wikilink")（重綱）繼承。

## 人物

  - 在伊達家中，與「武之[伊達成實](../Page/伊達成實.md "wikilink")」（）並稱，被稱為「**智之片倉景綱**」（）。一説指擅長[劍術](../Page/日本劍術.md "wikilink")，亦曾擔任[政宗年幼時的劍術指導等](../Page/伊達政宗.md "wikilink")，因此被認為是智勇兼備的武將。另外據說亦是相當厲害的吹笛高手。

<!-- end list -->

  - 在內政、戰時亦有謀略，以優秀的計策支持政宗，政宗亦多數會直接採納。

<!-- end list -->

  - 在妻子懷了[重長時](../Page/片倉重長.md "wikilink")，據說因為忌憚當時主君政宗還沒有孩子，所以企圖殺害自己的兒子，不過後來因為政宗說服而無事。

<!-- end list -->

  - 才能連當時的天下人[豐臣秀吉都有很高評價](../Page/豐臣秀吉.md "wikilink")。在[奧州仕置時](../Page/奧州仕置.md "wikilink")，秀吉希望景綱成為自己的直臣，於是打算立景綱為[三春藩](../Page/三春藩.md "wikilink")5万石[大名](../Page/大名.md "wikilink")，不過景綱因為對政宗的忠義而辭退。

<!-- end list -->

  - 晩年非常肥胖。根據『[伊達家世臣家譜](../Page/伊達家世臣家譜.md "wikilink")』記載，在[慶長](../Page/慶長.md "wikilink")7年（1602年），政宗因為「年長後肥胖的身體已經不適合重型鎧甲吧」（）而下賜較輕的鎧甲。從症狀推測，被認為是患了[糖尿病](../Page/糖尿病.md "wikilink")。

<!-- end list -->

  - 死後，據說有6名家臣因為景仰景綱的人德而[殉死](../Page/殉死.md "wikilink")。

## 逸話　

  - 在[政宗在](../Page/伊達政宗.md "wikilink")[人取橋之戰中](../Page/人取橋之戰.md "wikilink")，因為過於深入追擊敵兵，反遭敵兵包圍。這時片倉以「我就是大將伊達政宗，政宗在此，來取我首級。」（）以自己是伊達政宗來騙取敵兵的攻擊，以此拯救政宗。

<!-- end list -->

  - 政宗在小時候因為[天花使得右眼失去視力](../Page/天花.md "wikilink")，病後患病的眼球卻從眼窩突出，這樣的醜貌使得政宗有著大大的卑劣感和沉默的性格。景綱為了導正政宗的性格，於是將政宗拉到侍醫所在的房間，親手抱住政宗的頭，並以短刀一口氣挑出政宗的右眼球。此後，政宗從陰暗的性格，轉變成有活力並且精進於[文武兩道的少年](../Page/文武兩道.md "wikilink")。

<!-- end list -->

  - 在其他逸話中，擔任[劍術指南時向政宗問道](../Page/日本劍術.md "wikilink")「在戰場中，如果右眼被找到的話，怎麼辦」（），此時政宗拔出[脇差並刺入右眼中](../Page/脇差.md "wikilink")，命令景綱把其挑出。

<!-- end list -->

  - 在政宗所寫的信中，有簡略固有名詞的習慣（例如把田村寫為「田」、相馬寫為「相」等），給景綱的信中亦簡寫成「片小」（）。當景綱把[家督讓給兒子](../Page/家督.md "wikilink")[重長後](../Page/片倉重長.md "wikilink")，重長亦繼承了「小十郎」之名，而政宗給重長的信中，把他的名字簡寫成「片備」（）（片倉備中守的簡寫）。

<!-- end list -->

  - [慶長](../Page/慶長.md "wikilink")5年（1600年），政宗要在相馬領地留宿，在此的前日，迎接政宗的景綱率領7百至8百士兵進入現今[南相馬市](../Page/南相馬市.md "wikilink")[鹿島區](../Page/鹿島區.md "wikilink")，並在該地留宿。此時景綱與[相馬盛胤的付](../Page/相馬盛胤_\(彈正大弼\).md "wikilink")[家老](../Page/家老.md "wikilink")[加藤左近會談](../Page/加藤左近.md "wikilink")，希望可以勸導政宗加入德川一方，左近對此答應。（『[奧相茶話記](../Page/奧相茶話記.md "wikilink")』）

<!-- end list -->

  - 慶長5年（1600年），當[越後上杉氏侵入](../Page/越後上杉氏.md "wikilink")[最上氏領內時](../Page/最上氏.md "wikilink")，最上家向政宗求援，片倉進言「不用馬上去救援，趁他們兩軍交戰，達到最疲勞的階段時攻入，則一定能消滅上杉軍」（）。從作戰方面來看相當合理，但這時政宗因為擔心住在[山形城的母親](../Page/山形城.md "wikilink")[保春院的安全](../Page/保春院.md "wikilink")，於是退回了這套戰略。

<!-- end list -->

  - 片倉家直到[明治時代為止](../Page/明治時代.md "wikilink")，11代都管治白石之地。現今[白石市的](../Page/白石市.md "wikilink")[市章](../Page/市章.md "wikilink")「[黑釣鐘](../Page/黑釣鐘.md "wikilink")」，是景綱的姐姐[喜多決定使用的](../Page/片倉喜多.md "wikilink")[旗指物的](../Page/旗指物.md "wikilink")[家紋](../Page/家紋.md "wikilink")。在[仙台藩中](../Page/仙台藩.md "wikilink")，片倉氏的家格是御一家，[慶安](../Page/慶安.md "wikilink")4年（1651年）12月1日，在兒子重長時期被列入。

<!-- end list -->

  - 嫡男重長在[大坂夏之陣中的](../Page/大坂夏之陣.md "wikilink")[道明寺之戰中](../Page/道明寺之戰.md "wikilink")，殺死[後藤基次等人](../Page/後藤基次.md "wikilink")，並多次奮戰，於是有「鬼之小十郎」（）的異名。而重長的兒子[景長亦以](../Page/片倉景長.md "wikilink")「小十郎」為名，在[伊達騷動中](../Page/伊達騷動.md "wikilink")，支持年幼的主君[綱村](../Page/伊達綱村.md "wikilink")。於是代代仕於伊達氏的「片倉小十郎」名號，以後就被稱為伊達家忠臣之鑑。

## 肖像畫

景綱在生前或死後亦沒有立即制作肖像或肖像畫。最舊的肖像畫已經是在[明治時代所畫](../Page/明治時代.md "wikilink")、被收藏於[仙台市博物館中的畫](../Page/仙台市博物館.md "wikilink")。關於景綱所着用的甲冑，因為在畫下肖像時，已經沒有現存的實物，因此是想像出來的。

## 登場作品

  - 小說

<!-- end list -->

  - 『』（作者：[近衛龍春](../Page/近衛龍春.md "wikilink")，[PHP研究所](../Page/PHP研究所.md "wikilink")，2007年）
  - 『』（作者：[江宮隆之](../Page/江宮隆之.md "wikilink")，，2009年）
  - 『』（作者：[飯田勝彦](../Page/飯田勝彦.md "wikilink")，，1987年）
  - 『』（作者：飯田勝彦，新人物往來社，[新人物文庫](../Page/新人物文庫.md "wikilink")，2009年）

<!-- end list -->

  - 漫畫

<!-- end list -->

  - 『』（作者：，，『』2013年）

<!-- end list -->

  - 遊戲

<!-- end list -->

  - [戰國無雙系列](../Page/戰國無雙.md "wikilink")
      - [戰國無雙4](../Page/戰國無雙4.md "wikilink")（配音：[竹內良太](../Page/竹內良太.md "wikilink")）
  - [戰國BASARA系列](../Page/戰國BASARA.md "wikilink")（[Capcom](../Page/Capcom.md "wikilink")，配音：[森川智之](../Page/森川智之.md "wikilink")）

## 外部連結

  - [武家家伝＿片倉氏](http://www2.harimaya.com/sengoku/html/katakura.html)

## 相關條目

  - [片倉氏](../Page/片倉氏.md "wikilink")

[category:出羽國出身人物](../Page/category:出羽國出身人物.md "wikilink")
[category:陸奧國出身人物](../Page/category:陸奧國出身人物.md "wikilink")

[Category:戰國武將](../Category/戰國武將.md "wikilink")
[Category:仙台藩](../Category/仙台藩.md "wikilink")
[Category:片倉氏](../Category/片倉氏.md "wikilink")