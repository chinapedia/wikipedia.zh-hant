**許效舜**（1962年7月19日－），暱稱「舜子」，[臺灣男性](../Page/臺灣.md "wikilink")[搞笑藝人](../Page/搞笑藝人.md "wikilink")、[主持人](../Page/主持人.md "wikilink")、[演員](../Page/演員.md "wikilink")，[國立基隆高級海事職業學校畢業](../Page/國立基隆高級海事職業學校.md "wikilink")。許效舜與搭檔夥伴暨師父[澎恰恰合作多年](../Page/澎恰恰.md "wikilink")，有極佳默契，兩人的代表作為《[鐵獅玉玲瓏](../Page/鐵獅玉玲瓏.md "wikilink")》相關系列，也是兩人演藝事業的顛峰之作。許效舜提拔的藝人有[九孔](../Page/九孔_\(藝人\).md "wikilink")、[白雲](../Page/白雲_\(台灣藝人\).md "wikilink")、[NONO](../Page/NONO.md "wikilink")、-{[洪都拉斯](../Page/洪勝德.md "wikilink")}-、[黃鐙輝](../Page/黃鐙輝.md "wikilink")、[馬國畢和](../Page/馬國畢.md "wikilink")[蜆仔等人](../Page/劉俊峰.md "wikilink")。

## 演藝生涯

1984年服役退伍後，許效舜曾在[基隆地方法院擔任](../Page/基隆地方法院.md "wikilink")[法警](../Page/法警.md "wikilink")5年，工作期間考上[華視編劇班](../Page/中華電視公司.md "wikilink")，加入基隆話劇社，報考[蘭陵劇坊演員訓練班](../Page/蘭陵劇坊.md "wikilink")，也在電視台當二、三線演員。1985年，許效舜接到第一個電視節目通告，擔任[台視](../Page/台灣電視公司.md "wikilink")[連續劇](../Page/連續劇.md "wikilink")《江湖夜雨十年燈》的[臨時演員](../Page/臨時演員.md "wikilink")。

1989年，許效舜在蘭陵劇坊三位創辦人之一的[金士傑引薦之下](../Page/金士傑.md "wikilink")，進入知名[製作人](../Page/製作人.md "wikilink")[王偉忠製作的華視經典](../Page/王偉忠.md "wikilink")[綜藝節目](../Page/綜藝節目.md "wikilink")《[連環泡](../Page/連環泡.md "wikilink")》演出短劇單元《中國電視史》之《[軍教篇](../Page/軍事教育.md "wikilink")》（扮演一個傻兵）而走紅。

1990年，許效舜首度主持綜藝節目《連環泡》。1994年至1995年間，許效舜開始培養新進藝人接秀，當時許練習製造道具、編劇、美術等項目一手包辦，進一步強化成為一代笑匠的基礎。許效舜成名前期由王偉忠大力相助，至「石頭家族」成立前，許已成為獨當一面的諧星與主持人。

1999年，許效舜與搭檔夥伴暨師父與[澎恰恰在](../Page/澎恰恰.md "wikilink")[三立台灣台攜手主持的](../Page/三立台灣台.md "wikilink")《[鐵獅玉玲瓏](../Page/鐵獅玉玲瓏.md "wikilink")》大受歡迎，熱潮延續到2002年。由於《[鐵獅玉玲瓏](../Page/鐵獅玉玲瓏.md "wikilink")》的成功，許效舜乘勢而起，在2001年2月24日與[馬世莉為首](../Page/馬世莉.md "wikilink")，帶領一眾子弟兵（主要是1988年《[連環泡](../Page/連環泡.md "wikilink")》內的演員）成立「石頭家族」\[1\]，在[台北市](../Page/台北市.md "wikilink")[市民大道設立](../Page/市民大道.md "wikilink")「歡樂石族」公司作為家族總部（曾出現於[中視](../Page/中視.md "wikilink")《[歡喜玉玲龍](../Page/歡喜玉玲龍.md "wikilink")》的整人特別企劃）。「石頭家族」主攻[搞笑綜藝表演和節目](../Page/搞笑.md "wikilink")，旗下成員有-{[洪都拉斯](../Page/洪勝德.md "wikilink")}-、[白雲](../Page/白雲_\(藝人\).md "wikilink")、[郭子乾](../Page/郭子乾.md "wikilink")、[九孔](../Page/呂孔維.md "wikilink")、[馬國畢](../Page/馬國畢.md "wikilink")、[楊麗音](../Page/楊麗音.md "wikilink")、[高明偉](../Page/高明偉.md "wikilink")、[陳為民](../Page/陳為民.md "wikilink")、[江冠宏](../Page/江冠宏.md "wikilink")、[羅敏瑜](../Page/羅敏瑜.md "wikilink")、[王彩樺](../Page/王彩樺.md "wikilink")、[何妤玟](../Page/何妤玟.md "wikilink")、-{[于緁](../Page/于緁.md "wikilink")}-、[黃鐙輝等人](../Page/黃鐙輝.md "wikilink")。歡樂石族公司下轄[經紀部](../Page/經紀公司.md "wikilink")、[編劇組](../Page/編劇.md "wikilink")、[製作組等部門](../Page/製作.md "wikilink")，石頭家族亦在綜藝節目的收視表現上頗受好評，家族成員主要在三立台灣台《[黃金夜總會](../Page/黃金夜總會.md "wikilink")》演出。

2002年5月，許效舜與家族成員離開[三立電視](../Page/三立電視.md "wikilink")、跳槽[MUCH
TV](../Page/MUCH_TV.md "wikilink")，投入MUCH
TV午間時段節目《台灣樂麗屋》；然而同年10月《台灣樂麗屋》停播，「石頭家族」往後因此缺乏固定舞台。至2005年11月26日，「石頭家族」旗下藝人的合約正好約滿到期時，許效舜以解除合約、各奔前程為由解散家族，名噪一時的「石頭家族」正式宣告解散。

2010年9月，許效舜開始在[基隆市](../Page/基隆市.md "wikilink")[崇右技術學院](../Page/崇右技術學院.md "wikilink")（今[崇右影藝科技大學](../Page/崇右影藝科技大學.md "wikilink")）教授「劇場實務」課程（通識課程），得到眾多教師與學生的肯定。2011年初與年中，許效舜演出兩部[音樂劇受到不少校內人員讚賞](../Page/音樂劇.md "wikilink")。2012年3月起，許擔任該校影視傳播系助理教授。2015年，許效舜與[澎恰恰及友人共同成立](../Page/澎恰恰.md "wikilink")「鐵人文創」娛樂公司，致力發展台灣影視產業，培養演藝圈人才\[2\]。

## 個人生活

許效舜生長於[台灣](../Page/台灣.md "wikilink")[基隆市](../Page/基隆市.md "wikilink")[暖暖區](../Page/暖暖區.md "wikilink")。父親許義隆曾任[基隆市議員](../Page/基隆市議員.md "wikilink")；母親楊美鳳於2004年因[肝癌逝世](../Page/肝癌.md "wikilink")；祖父[許朝宗任職](../Page/許朝宗.md "wikilink")[臺灣鐵路管理局](../Page/臺灣鐵路管理局.md "wikilink")[八堵車站副站長](../Page/八堵車站.md "wikilink")，因[二二八事件受害身亡](../Page/二二八事件.md "wikilink")。

1992年，許效舜與李優君結婚，曾被外界稱為「美女與野獸」的婚姻。

  - 1995年，兩人生下女兒。
  - 2001年，李優君與女兒[移民](../Page/移民.md "wikilink")[加拿大](../Page/加拿大.md "wikilink")。
  - 2005年8月4日，因夫妻生活觀念不同，兩人感情轉淡。《[台灣蘋果日報](../Page/台灣蘋果日報.md "wikilink")》誤報李優君過世消息，被女方認為是許效舜惡意造謠中傷。
  - 2006年3月14日，兩人離婚，許的演藝事業亦走下坡。
  - 2010年1月，許效舜被李優君指控未支付[贍養費](../Page/贍養費.md "wikilink")，當時的許效舜靠副業維生，經濟狀況不佳\[3\]。

2011年，許效舜梅開二度，與後來交往的女友陳愉結婚，後一子一女陸續誕生。

許效舜篤信[佛教](../Page/佛教.md "wikilink")，亦有收藏住家附近大小珍奇石頭的習慣，從對家族與祖家店鋪的命名突顯他對石頭的熱愛。2006年6月，許效舜在基隆市暖暖區東勢街開設「舜子的家—石頭族茶棧」，替人算命\[4\]。

## 師承關係

若僅依許效舜和相關藝人拜師之承啟關係彙整，關係如下：

  - **師公：**
  - [張菲](../Page/張菲.md "wikilink")

<!-- end list -->

  - **師父輩：**
  - [胡瓜](../Page/胡瓜.md "wikilink")
  - [鄭進一](../Page/鄭進一.md "wikilink")
  - [康康](../Page/康康.md "wikilink")

<!-- end list -->

  - **師兄弟：**
  - [澎恰恰](../Page/澎恰恰.md "wikilink") (師承鄭進一)
  - [王識賢](../Page/王識賢.md "wikilink") (師承鄭進一)
  - [歐漢聲](../Page/歐漢聲.md "wikilink") (師承胡瓜)
  - [羅志祥](../Page/羅志祥.md "wikilink") (師承胡瓜)
  - [浩角翔起](../Page/浩角翔起.md "wikilink") (師承胡瓜)
  - [瑪莉亞](../Page/葉欣眉.md "wikilink") (師承胡瓜)

<!-- end list -->

  - **徒弟輩：**
  - [九孔](../Page/九孔_\(藝人\).md "wikilink") (師承許效舜)
  - [NONO](../Page/NONO.md "wikilink") (師承許效舜)
  - [白雲](../Page/白雲_\(台灣藝人\).md "wikilink") (師承許效舜)
  - \-{[洪都拉斯](../Page/洪勝德.md "wikilink")}- (師承許效舜)
  - [黃鐙輝](../Page/黃鐙輝.md "wikilink") (師承許效舜)
  - [馬國畢](../Page/馬國畢.md "wikilink") (師承許效舜)
  - [蜆仔](../Page/劉俊峰.md "wikilink") (師承許效舜)
  - [黃鴻升](../Page/黃鴻升.md "wikilink") (師承羅志祥)

<!-- end list -->

  - **徒孫輩：**
  - [曾子余](../Page/曾子余.md "wikilink") (師承黃鐙輝)

## 作品

### 綜藝節目

  - 目前主持

|                                      |                                        |
| ------------------------------------ | -------------------------------------- |
| **頻道**                               | **節目**                                 |
| [衛視中文台](../Page/衛視中文台.md "wikilink") | 《[瘋神無雙](../Page/瘋神無雙.md "wikilink")》   |
| [三立台灣台](../Page/三立台灣台.md "wikilink") | 《[超級夜總會](../Page/超級夜總會.md "wikilink")》 |

  - 已停播

|                                                     |                                              |
| --------------------------------------------------- | -------------------------------------------- |
| **頻道**                                              | **節目**                                       |
| [台視](../Page/台灣電視公司.md "wikilink")                  | 《週末晚點名》                                      |
| 《醜人俱樂部》                                             |                                              |
| 《台灣強強滾》                                             |                                              |
| 《綜藝大順利》                                             |                                              |
| 《[台視群星會](../Page/台視群星會.md "wikilink")》              |                                              |
| 《卡拉永遠OK》                                            |                                              |
| [中視](../Page/中國電視公司.md "wikilink")                  | 《歡樂傳真》                                       |
| 《[娛樂星聞 小燕有約](../Page/娛樂星聞_小燕有約.md "wikilink")》      |                                              |
| 《誰來開口》                                              |                                              |
| 《[紅白勝利](../Page/紅白勝利.md "wikilink")》                |                                              |
| 《[歡喜玉玲龍](../Page/歡喜玉玲龍.md "wikilink")》              |                                              |
| 《週日玉玲龍》                                             |                                              |
| 《綜藝OK棒》                                             |                                              |
| 《戀愛OK棒》                                             |                                              |
| 《愛情萬萬歲》                                             |                                              |
| 《[2010高雄縣感恩祈福跨年晚會](../Page/義大世界跨年晚會.md "wikilink")》 |                                              |
| 《[綜藝大哥大](../Page/綜藝大哥大.md "wikilink")》之〈黑白郎君黑白講〉單元  |                                              |
| 《筆記台灣》                                              |                                              |
| 《[沒玩沒了](../Page/沒玩沒了.md "wikilink")》                |                                              |
| [華視](../Page/中華電視公司.md "wikilink")                  | 《[連環泡](../Page/連環泡.md "wikilink")》           |
| 《[綜藝萬花筒](../Page/綜藝萬花筒.md "wikilink")》              |                                              |
| 《歡樂周末派》                                             |                                              |
| 《紅白勝利》                                              |                                              |
| 《[百戰百勝](../Page/百戰百勝.md "wikilink")》                |                                              |
| 《電視俱樂部》                                             |                                              |
| 《無敵星期六》                                             |                                              |
| [民視](../Page/民間全民電視公司.md "wikilink")                | 《歡喜就好》                                       |
| 《辣妹向前衝》                                             |                                              |
| 《王牌攝影棚》                                             |                                              |
| 《雙喜俱樂部》                                             |                                              |
| [博新一台](../Page/博新多媒體.md "wikilink")                 | 《現象追蹤點》                                      |
| [衛視中文台](../Page/衛視中文台.md "wikilink")                | 《台灣發明王》                                      |
| 《喂～是在搞什麼鬼！》                                         |                                              |
| 《[歡樂幸運星](../Page/歡樂幸運星.md "wikilink")》              |                                              |
| 《[旅行應援團](../Page/旅行應援團.md "wikilink")》              |                                              |
| [東森綜合台](../Page/東森綜合台.md "wikilink")                | 《布魯樂翻天》                                      |
| [三立台灣台](../Page/三立台灣台.md "wikilink")                | 《石頭族樂園》                                      |
| 《黃金夜總會》                                             |                                              |
| 《在台灣的故事》                                            |                                              |
| 《鐵獅玉玲瓏》                                             |                                              |
| [傳訊電視](../Page/傳訊電視.md "wikilink")                  | [大地電視台](../Page/大地電視台.md "wikilink")《電視笑話冠軍》 |
| [緯來綜合台](../Page/緯來綜合台.md "wikilink")                | 《笑彈總動員》                                      |
| 《電視笑話冠軍》                                            |                                              |
| 《愛喲我的媽》                                             |                                              |
| [MUCH TV](../Page/MUCH_TV.md "wikilink")            | 《台灣樂麗屋》                                      |
| 《婆婆媽媽發財車》                                           |                                              |
| [東風衛視](../Page/東風衛視.md "wikilink")                  | 《無敵鐵金鋼》                                      |
| [高點電視台](../Page/高點電視台.md "wikilink")                | 《全民大卡拉》                                      |
| [中天綜合台](../Page/中天綜合台.md "wikilink")                | 《[笑林練舞功](../Page/笑林練舞功.md "wikilink")》       |
| [麥卡貝網路頻道](../Page/麥卡貝.md "wikilink")                | 《周五兩光秀》                                      |

### 電影

|              |                                                 |         |
| ------------ | ----------------------------------------------- | ------- |
| **年份**       | **片名**                                          | **角色**  |
| |《成功嶺2：全面出擊》 |                                                 |         |
| |《梅珍》        |                                                 |         |
| |《鄭進一的鬼故事》   |                                                 |         |
| 1994年        | 《[新烏龍院](../Page/笑林小子2：新乌龙院.md "wikilink")》      | 戲中戲導演   |
| |《超級三等兵》     |                                                 |         |
| |《小鬼遇到兵》     |                                                 |         |
| 1997年        | 《[忍者兵](../Page/忍者兵.md "wikilink")》              | 綁架大將    |
| 1996年        | 《狗蛋大兵》                                          | 瘋子軍官    |
| 2004年        | 《[五月之戀](../Page/五月之戀.md "wikilink")》            |         |
| 2007年        | 《[練習曲](../Page/練習曲.md "wikilink")》              |         |
| |《小溫馨》       |                                                 |         |
| 2013年        | 《[天后之戰](../Page/天后之戰.md "wikilink")》            | 朱雙火     |
| 2013年        | 《[世界第一麥方](../Page/世界第一麥方.md "wikilink")》        | 沙鹿麵包店老闆 |
| 2014年        | 《[鐵獅玉玲瓏1](../Page/鐵獅玉玲瓏_\(電影\).md "wikilink")》  | 林隆      |
| 2015年        | 《[鐵獅玉玲瓏2](../Page/鐵獅玉玲瓏2_\(電影\).md "wikilink")》 | 舜哥      |
| 2015年        | 《[我們全家不太熟](../Page/我們全家不太熟.md "wikilink")》      |         |
| 2017年        | 《[痴情男子漢](../Page/痴情男子漢_\(電影\).md "wikilink")》   | 陳有力     |

### 電視劇

|                                     |                                            |                                                 |         |
| ----------------------------------- | ------------------------------------------ | ----------------------------------------------- | ------- |
| **年份**                              | **頻道**                                     | **劇名**                                          | **角色**  |
| |[台視](../Page/台灣電視公司.md "wikilink") | 《[江湖夜雨十年燈](../Page/江湖夜雨十年燈.md "wikilink")》 |                                                 |         |
| 1995年                               | [中視](../Page/中國電視公司.md "wikilink")         | 《[媽祖拜觀音](../Page/媽祖拜觀音.md "wikilink")》          | 順風耳     |
| 1995年                               | [中視](../Page/中國電視公司.md "wikilink")         | 《[今夜作夢也會笑](../Page/今夜作夢也會笑.md "wikilink")》      | 吳東平     |
| 2003年                               | [中視](../Page/中國電視公司.md "wikilink")         | 《[我的秘密花園](../Page/我的秘密花園.md "wikilink")》        | 客串：公車司機 |
| 2003年                               | [華視](../Page/中華電視公司.md "wikilink")         | 《[流星花園Ⅱ](../Page/流星花园_\(台湾电视剧\).md "wikilink")》 | 傻哥      |
| 2004年                               | [中視](../Page/中國電視公司.md "wikilink")         | 《[我們兩家都是人](../Page/我們兩家都是人.md "wikilink")》      |         |
| 2005年                               | [東森綜合台](../Page/東森綜合台.md "wikilink")       | 《[好美麗診所](../Page/好美麗診所.md "wikilink")》          |         |
| 2006年                               | [東森綜合台](../Page/東森綜合台.md "wikilink")       | 《[艾曼紐要怎樣](../Page/艾曼紐要怎樣.md "wikilink")》        |         |
| 2006年                               | [東森綜合台](../Page/東森綜合台.md "wikilink")       | 《[麻辣一家親](../Page/麻辣一家親.md "wikilink")》          | 曾國棟     |
| 2007年                               | [華視](../Page/中華電視公司.md "wikilink")         | 《[親親小爸](../Page/親親小爸.md "wikilink")》            | 洪芋頭     |
| 2008年                               | [台視](../Page/台灣電視公司.md "wikilink")         | 《[懷玉傳奇 千金媽祖](../Page/懷玉傳奇_千金媽祖.md "wikilink")》  | 許孝順     |
| 2008年                               | [衛視中文台](../Page/衛視中文台.md "wikilink")       | 《[黑糖群俠傳](../Page/黑糖群俠傳.md "wikilink")》          | 周大通     |
| 2009年                               | [八大第一台](../Page/八大第一台.md "wikilink")       | 《[青春歌仔](../Page/青春歌仔.md "wikilink")》            |         |
| 2011年                               | [安徽衛視](../Page/安徽衛視.md "wikilink")         | 《[幸福一定強](../Page/幸福一定強.md "wikilink")》          | 潘父      |
| 2011年                               | [公視](../Page/公共電視.md "wikilink")           | 《[愛在桐花紛飛時](../Page/愛在桐花紛飛時.md "wikilink")》      | 鄭雲山     |
| 2014年                               | [TVBS歡樂台](../Page/TVBS歡樂台.md "wikilink")   | 《[俏摩女搶頭婚](../Page/俏摩女搶頭婚.md "wikilink")》        | 錢樹德     |
| 2014年                               | [樂視網](../Page/樂視網.md "wikilink")           | 《[光環之后](../Page/光環之后.md "wikilink")》            | 舜哥      |
| 2014年                               | [樂視網](../Page/樂視網.md "wikilink")           | 《[PMAM美好偵探社](../Page/PMAM美好偵探社.md "wikilink")》  | 麵攤老闆    |
| 2016年                               | [台視](../Page/台灣電視公司.md "wikilink")         | 《[加油！美玲](../Page/加油！美玲.md "wikilink")》          | 算命師     |
| 2016年                               | [台視](../Page/台灣電視公司.md "wikilink")         | 《[幸福不二家](../Page/幸福不二家.md "wikilink")》          | 客串：餐廳店長 |

### 唱片

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>年份</p></th>
<th style="text-align: left;"><p>專輯名稱</p></th>
<th style="text-align: left;"><p>唱片公司</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>1991年</p></td>
<td style="text-align: left;"><p>《三口組Ⅰ》合輯</p></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>1991年</p></td>
<td style="text-align: left;"><p>《三口組Ⅱ》合輯</p></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>1996年</p></td>
<td style="text-align: left;"><p>《憨愿頭》</p></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>2000年7月26日</p></td>
<td style="text-align: left;"><p>《<a href="../Page/口白歪歌系列2_簡簡單單.md" title="wikilink">口白歪歌系列2 簡簡單單</a>》</p></td>
<td style="text-align: left;"><p>大信唱片</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>2003年4月9日</p></td>
<td style="text-align: left;"><p><a href="../Page/鄭進一.md" title="wikilink">鄭進一</a>、許效舜《樂透狂想曲》</p></td>
<td style="text-align: left;"><p>艾迪昇傳播／金炮／全員集合國際多媒體</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>2012年</p></td>
<td style="text-align: left;"><p>《<a href="../Page/阿嬤！妳是我的寶貝.md" title="wikilink">阿嬤！妳是我的寶貝</a>》</p></td>
<td style="text-align: left;"><p>美樂帝唱片</p></td>
<td style="text-align: left;"></td>
</tr>
</tbody>
</table>

### 廣告（代言）

  - 1980年[三陽工業](../Page/三陽工業.md "wikilink")「三陽機車」
  - 1996年[成元興業](../Page/成元興業.md "wikilink")「怡親安嬰兒紙尿褲」
  - 1999年[史克美占](../Page/史克美占.md "wikilink")「普拿疼」
  - 2001年[台灣菸酒公司](../Page/台灣菸酒公司.md "wikilink")「玉山高粱」
  - 2001年[統一企業](../Page/統一企業.md "wikilink")「統一麵」蔥燒牛肉麵
  - 2001年[真口味食品](../Page/真口味食品.md "wikilink")「古道杭菊普洱茶」

### 相關書籍

| 年份            | 書名           | 作者   | 出版社                                | ISBN            |
| ------------- | ------------ | ---- | ---------------------------------- | --------------- |
| 1992年7月1日初版   | 《同花大舜》       | 許效舜著 | [希代書版](../Page/希代書版.md "wikilink") | ISBN 9575441958 |
| 1995年11月1日初版  | 《許效舜說鬼比較順》   | 許效舜著 | 精美出版                               | ISBN 957716384X |
| 1998年9月1日初版   | 《舜李成章》       | 許效舜著 | 希代書版                               | ISBN 9578112572 |
| 1998年12月12日初版 | 《磊跡：舜子詩畫筆記書》 | 許效舜著 | 高談文化                               | ISBN 9579806608 |
| 2001年8月初版     | 《石頭舜子玉玲瓏》    | 張雅雅著 | [時報文化](../Page/時報文化.md "wikilink") | ISBN 9571334340 |
|               |              |      |                                    |                 |

### 音樂劇

  - [音樂時代劇場](../Page/音樂時代劇場.md "wikilink")《[隔壁親家](../Page/隔壁親家_\(音樂劇\).md "wikilink")》

### 網路影音

  - [國立歷史博物館](../Page/國立歷史博物館.md "wikilink")《數位典藏成果體驗網》宣傳片《歷史玉玲瓏》

## 獎項紀錄

### 電視金鐘獎

|- |2012年 |超級夜總會 |[第47屆金鐘獎綜藝節目主持人獎](../Page/第47屆金鐘獎.md "wikilink") | |-
|2014年 |超級夜總會 |[第49屆金鐘獎綜藝節目主持人獎](../Page/第49屆金鐘獎.md "wikilink") | |-
|2018年 |瘋神無雙 |[第53屆金鐘獎綜藝節目主持人獎](../Page/第53屆金鐘獎.md "wikilink") | |- |}

## 參考資料

## 外部連結

  -
  -
  -
  -
  -
  -
  -
[Category:金鐘獎綜藝節目主持人獲得者](../Category/金鐘獎綜藝節目主持人獲得者.md "wikilink")
[H許](../Category/台灣綜藝節目主持人.md "wikilink")
[H許](../Category/台湾男歌手.md "wikilink")
[H許](../Category/台灣喜劇演員.md "wikilink")
[Category:國立基隆高級海事職業學校校友](../Category/國立基隆高級海事職業學校校友.md "wikilink")
[Category:基隆市立暖暖國民中學校友](../Category/基隆市立暖暖國民中學校友.md "wikilink")
[Category:基隆市立暖暖國民小學校友](../Category/基隆市立暖暖國民小學校友.md "wikilink")
[H許](../Category/暖暖人.md "wikilink") [X](../Category/許姓.md "wikilink")
[Category:台湾佛教徒](../Category/台湾佛教徒.md "wikilink")
[Category:模仿藝人](../Category/模仿藝人.md "wikilink")

1.  「[澎哥 舜子
    師徒點「石」成金](http://www.libertytimes.com.tw/2001/new/feb/25/today-show4.htm)」，《自由時報》，2001年2月25日
2.  [打不死！澎恰恰開公司繼續拍電影　還要開班培植新人](http://star.ettoday.net/news/493424?t=%E6%89%93%E4%B8%8D%E6%AD%BB%EF%BC%81%E6%BE%8E%E6%81%B0%E6%81%B0%E9%96%8B%E5%85%AC%E5%8F%B8%E7%B9%BC%E7%BA%8C%E6%8B%8D%E9%9B%BB%E5%BD%B1%E3%80%80%E9%82%84%E8%A6%81%E9%96%8B%E7%8F%AD%E5%9F%B9%E6%A4%8D%E6%96%B0%E4%BA%BA)
3.  「[許效舜1年未付贍養費
    哀怨女兒將從母姓](http://tw.nextmedia.com/applenews/article/art_id/32415768/IssueID/20100407)」，《蘋果日報》，2010年4月7日
4.  「[演藝事業走下坡改行算命？
    許效舜：我要幫9999個人解惑](http://www.nownews.com/2006/07/13/91-1965521.htm)」，《NOW
    news》，2006年7月13日