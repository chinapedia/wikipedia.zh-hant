**鼓浪屿近代建筑群**位于中国[福建省](../Page/福建省.md "wikilink")[厦门市](../Page/厦门市.md "wikilink")[思明区](../Page/思明区.md "wikilink")[鼓浪屿](../Page/鼓浪屿.md "wikilink")。2006年，美国领事馆旧址等十处十三座建筑被联合列为[第六批全国重点文物保护单位](../Page/第六批全国重点文物保护单位.md "wikilink")。\[1\]

2013年，厦门海关税务司公馆旧址、海关理船厅公署旧址、海关验货员公寓旧址、会审公堂旧址、[救世医院旧址](../Page/故宫鼓浪屿外国文物馆.md "wikilink")、博爱医院旧址、毓德女学堂旧址、“万国俱乐部”旧址、海天堂构、黄荣远堂10处共17幢建筑在[第七批全国重点文物保护单位公布时被列为](../Page/第七批全国重点文物保护单位.md "wikilink")“鼓浪屿近代建筑群”的扩展合并项目，归入[第六批全国重点文物保护单位](../Page/第六批全国重点文物保护单位.md "wikilink")“鼓浪屿近代建筑群”内\[2\]。

2017年7月8日，在波兰克拉科夫举行的世界遗产大会上，鼓浪屿获准列入[世界文化遗产名录](../Page/世界文化遗产.md "wikilink")\[3\]。

## 名单

第六批全国重点文物保护单位（10处13座）：

<table>
<thead>
<tr class="header">
<th><p>名称</p></th>
<th><p>地址</p></th>
<th><p>图片</p></th>
<th><p>备注</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/美国领事馆旧址.md" title="wikilink">美国领事馆旧址</a></p></td>
<td><p>三明路26号</p></td>
<td></td>
<td><p>1930年建，现为酒店</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/日本领事馆旧址.md" title="wikilink">日本领事馆旧址</a><br />
（含警察署本部两座建筑）</p></td>
<td><p>鹿礁路26号</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Xiamen_Gulangyu_20120226-12.jpg" title="fig:Xiamen_Gulangyu_20120226-12.jpg">Xiamen_Gulangyu_20120226-12.jpg</a></p></td>
<td><p>1897-1898年建，警察署本部两座建筑（图）1928年建</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/汇丰银行旧址.md" title="wikilink">汇丰银行旧址</a></p></td>
<td><p>鼓新路57号</p></td>
<td></td>
<td><p>1910-1920年建</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鼓浪屿耶稣君王堂.md" title="wikilink">天主堂</a></p></td>
<td><p>鹿礁路34号</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Xiamen_Gulangyu_20120226-19.jpg" title="fig:Xiamen_Gulangyu_20120226-19.jpg">Xiamen_Gulangyu_20120226-19.jpg</a></p></td>
<td><p>1917年建</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鼓浪屿三一堂.md" title="wikilink">三一堂</a></p></td>
<td><p>笔山洞口</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Xiamen_Gulangyu_20120226-26.jpg" title="fig:Xiamen_Gulangyu_20120226-26.jpg">Xiamen_Gulangyu_20120226-26.jpg</a></p></td>
<td><p>1934-1945年建</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/安献楼.md" title="wikilink">安献楼</a></p></td>
<td><p>鸡山路18号</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Xiamen_Gulangyu_20120226-27.jpg" title="fig:Xiamen_Gulangyu_20120226-27.jpg">Xiamen_Gulangyu_20120226-27.jpg</a></p></td>
<td><p>1934年建</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/八卦楼.md" title="wikilink">八卦楼</a></p></td>
<td><p>鼓新路43号</p></td>
<td></td>
<td><p>1907年<a href="../Page/林鹤寿.md" title="wikilink">林鹤寿建</a>，是鼓浪屿标志建筑，现为<a href="../Page/厦门市博物馆.md" title="wikilink">厦门市博物馆</a>、<a href="../Page/管风琴.md" title="wikilink">管风琴博物馆</a>。</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/瞰青别墅.md" title="wikilink">瞰青别墅</a><br />
（含<a href="../Page/西林别墅.md" title="wikilink">西林别墅</a>）</p></td>
<td><p>永春路72号、73号</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Gulangyu_koxinga_memorial_hall_2011_12.jpg" title="fig:Gulangyu_koxinga_memorial_hall_2011_12.jpg">Gulangyu_koxinga_memorial_hall_2011_12.jpg</a></p></td>
<td><p>分别建于1918年、1927年，西林别墅现为郑成功纪念馆（图）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/亦足山庄.md" title="wikilink">亦足山庄</a></p></td>
<td><p>笔山路9号</p></td>
<td></td>
<td><p>1919年建</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/菽庄花园.md" title="wikilink">菽庄花园</a></p></td>
<td><p>西南部海滨</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Shuzhuang_garden.jpg" title="fig:Shuzhuang_garden.jpg">Shuzhuang_garden.jpg</a></p></td>
<td><p>1913年<a href="../Page/林爾嘉.md" title="wikilink">林爾嘉建</a></p></td>
</tr>
</tbody>
</table>

## 参考文献

[鼓浪屿近代建筑群](../Category/鼓浪屿近代建筑群.md "wikilink")
[闽](../Category/中国世界遗产.md "wikilink")

1.
2.
3.