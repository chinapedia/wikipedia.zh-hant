**缙云县**在[中國](../Page/中國.md "wikilink")[浙江省中部偏南](../Page/浙江省.md "wikilink")，是[丽水市下辖的一个县](../Page/丽水市.md "wikilink")。相传[黄帝在](../Page/黄帝.md "wikilink")[仙都鼎湖炼丹升天](../Page/仙都.md "wikilink")，因黄帝又名缙云氏，“缙云县”由此得名。\[1\]

## 历史

始建于[唐](../Page/唐朝.md "wikilink")[万岁登封元年](../Page/万岁登封.md "wikilink")（696年）。[張守節認為縉雲可能是](../Page/張守節.md "wikilink")[上古時代黃帝一支族](../Page/上古時代.md "wikilink")[縉雲氏的封地](../Page/縉雲氏.md "wikilink")\[2\]，當地超過千年以來都設有[江南最大的黃帝祭祀祠和例行祭祀活動](../Page/江南.md "wikilink")。\[3\]
缙云，是丽水经济强县，方言独特。缙云属于丽水管辖，却与金华地理相近，虽然缙云方言在语言学上划归吴方言的处衢小片，却与金华方言有许多相近之处，由于古时缙云的土著居民是百越族，缙云话中有相当多的百越话的遗音，一些语汇与汉语官话截然不同。同时，缙云话与日语也有一定的关系，这主要是因为古时江南人，特别是浙江一带的越人亡国，越人大量外迁，有一部分东渡到了日本，之后又有一大批人东渡到了日本。缙云话与古汉语有很密切的相承关系，友好城市日本：[静冈县](../Page/静冈县.md "wikilink")[三岛市](../Page/三岛市.md "wikilink")。

## 地理

缙云东临[仙居](../Page/仙居.md "wikilink")、[永嘉](../Page/永嘉县.md "wikilink")，南连[青田](../Page/青田.md "wikilink")，西邻[莲都](../Page/莲都区.md "wikilink")、[武义](../Page/武义.md "wikilink")，北接[永康](../Page/永康市.md "wikilink")、[磐安](../Page/磐安县.md "wikilink")。地处[括苍山和](../Page/括苍山.md "wikilink")[仙霞岭的过渡地带](../Page/仙霞岭.md "wikilink")，地势东南高，西北低，地貌以[丘陵](../Page/丘陵.md "wikilink")、低山为主。\[4\]

## 行政区划

辖9个镇、15个乡，6个社区、6个居民区、642个行政村：

  - 镇：五云镇、壶镇镇、新建镇、新碧镇、舒洪镇、大洋镇、大源镇、东渡镇、东方镇。
  - 乡：溶江乡、七里乡、胡源乡、南溪乡、木栗乡、三溪乡、前路乡、方溪乡、石笕乡、双溪口乡。

## 交通

[201701_G7326_at_Jinyunxi_Station.jpg](https://zh.wikipedia.org/wiki/File:201701_G7326_at_Jinyunxi_Station.jpg "fig:201701_G7326_at_Jinyunxi_Station.jpg")\]\]
[330国道](../Page/330国道.md "wikilink")、[金丽温高速公路](../Page/金丽温高速公路.md "wikilink")、[金温铁路](../Page/金温铁路.md "wikilink")。\[5\]

## 矿产

缙云有丰富的矿产资源，[沸石](../Page/沸石.md "wikilink")、[花岗岩](../Page/花岗岩.md "wikilink")、[凝灰岩](../Page/凝灰岩.md "wikilink")、[珍珠岩](../Page/珍珠岩.md "wikilink")、[氟石为](../Page/氟石.md "wikilink")“缙云五宝石”，其中天然沸石蕴藏量为全国第一。

## 经济

## 教育

[缙云中学是知名的浙江省重点中学](../Page/缙云中学.md "wikilink")。

## 风景名胜

  - [国家重点风景名胜区](../Page/国家重点风景名胜区.md "wikilink")：[仙都](../Page/仙都.md "wikilink")
  - [全国重点文物保护单位](../Page/全国重点文物保护单位.md "wikilink")：[仙都摩崖题记河阳古民居](../Page/仙都摩崖题记.md "wikilink")
  - [浙江省文物保护单位](../Page/浙江省文物保护单位.md "wikilink")：[九进厅](../Page/九进厅.md "wikilink")、[白茅云衢坊](../Page/白茅云衢坊.md "wikilink")、[大溪滩窑址群](../Page/大溪滩窑址群.md "wikilink")、[双港桥贞节坊石刻](../Page/双港桥贞节坊石刻.md "wikilink")、[慕义桥](../Page/慕义桥.md "wikilink")、[仙都石梁桥](../Page/仙都石梁桥.md "wikilink")

## 历代名人

  - [智威](../Page/智威.md "wikilink")
  - [李阳冰](../Page/李阳冰.md "wikilink")
  - [少康](../Page/少康.md "wikilink")
  - [杜光庭](../Page/杜光庭.md "wikilink")，唐末五代著名[道士](../Page/道士.md "wikilink")，[文学家](../Page/文学家.md "wikilink")。
  - [赵顺孙](../Page/赵顺孙.md "wikilink")
  - [潜说友](../Page/潜说友.md "wikilink")
  - [李棠](../Page/李棠.md "wikilink")
  - [郑汝壁](../Page/郑汝壁.md "wikilink")
  - [呂逢樵](../Page/呂逢樵.md "wikilink")

## 参考文献

[缙云县](../Category/缙云县.md "wikilink")
[丽水市](../Category/浙江省县份.md "wikilink")
[县](../Category/丽水区县市.md "wikilink")

1.  [缙云县概况](http://cn.zhaoshang-sh.com/Zhejiang/Lishui/LishuiTZHJ/27384.html)


2.

3.

4.

5.