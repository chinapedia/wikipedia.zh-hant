**嘉峪龍屬**（[學名](../Page/學名.md "wikilink")：*Chiayusaurus*）是[大鼻龍類的一](../Page/大鼻龍類.md "wikilink")[屬](../Page/屬.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，[化石是在](../Page/化石.md "wikilink")[亞洲發現的](../Page/亞洲.md "wikilink")[牙齒](../Page/牙齒.md "wikilink")。目前已有兩個[物種](../Page/物種.md "wikilink")。嘉裕龍的學名是取自[萬里長城的](../Page/萬里長城.md "wikilink")[嘉峪關](../Page/嘉峪關.md "wikilink")，原先是「*Chiayüsaurus*」，但[國際動物命名法規不允許特別的字元](../Page/國際動物命名法規.md "wikilink")，所以學名改為「*Chiayusaurus*」。舊有的名字仍可以在一些較舊的文獻中見到。

## 歷史及分類

在1953年，[步林](../Page/步林.md "wikilink")（Birger
Bohlin）根據在[中國科學院](../Page/中國科學院.md "wikilink")[古脊椎動物與古人類研究所的](../Page/古脊椎動物與古人類研究所.md "wikilink")[正模標本](../Page/正模標本.md "wikilink")[牙齒](../Page/牙齒.md "wikilink")，命名為[湖泊嘉裕龍](../Page/湖泊嘉裕龍.md "wikilink")（*C.
lacustris*
）。這顆牙齒發現於[中國](../Page/中國.md "wikilink")[新疆的](../Page/新疆.md "wikilink")[喀拉扎組](../Page/喀拉扎組.md "wikilink")，地質年代可能屬於[晚侏羅紀](../Page/侏羅紀.md "wikilink")[提通階](../Page/提通階.md "wikilink")。這顆匙狀的牙齒長27毫米，且與較年代較晚的[亞洲龍相似](../Page/亞洲龍.md "wikilink")\[1\]。由於嘉峪龍只有這麼少的化石材料，牠經常都被忽略，或被認為是其他已知的[恐龍](../Page/恐龍.md "wikilink")。例如[戴爾·羅素](../Page/戴爾·羅素.md "wikilink")（Dale
Russell）及[趙喜進就發現牠們與](../Page/趙喜進.md "wikilink")[馬門溪龍的牙齒無法分辨](../Page/馬門溪龍.md "wikilink")\[2\]。

布林在1953年也描述了另一顆較大的牙齒，並發表為「嘉峪龍近似種」（aff.
*Chiayüsaurus*），但他認為這牙齒有可能屬於湖泊嘉峪龍\[3\]。在1997年，在[南韓有一個新的物種被發表](../Page/南韓.md "wikilink")，也是根據一顆牙齒（編號KPE
8001），而這顆牙齒完全與嘉峪龍近似種的牙齒相當一致。這個南韓新物種被命名為[亞洲嘉裕龍](../Page/亞洲嘉裕龍.md "wikilink")（*C.
asianensis*），牙齒化石發現於[慶尚道的Hasandong組地層](../Page/慶尚道.md "wikilink")，地質年代為[下白堊紀](../Page/下白堊紀.md "wikilink")[阿普第階至](../Page/阿普第階.md "wikilink")[阿爾布階](../Page/阿爾布階.md "wikilink")。這些南韓學們反對布林的理論，步林認為這兩顆牙齒是在頜部的不同位置，他們根據牙齒表面的磨蝕及隆起的位置而將牠們分為兩個物種。亞洲嘉峪龍的牙齒長46毫米\[4\]。

在2002年，[保羅·巴雷特](../Page/保羅·巴雷特.md "wikilink")（Paul M.
Barrett）等人重新研究這兩個物種。他們發現湖泊嘉峪龍的牙齒與[盤足龍的牙齒幾乎完全一樣](../Page/盤足龍.md "wikilink")，但不足以確認牠們是相同物種，只能將嘉峪龍歸類為[真蜥腳類的分類未定屬](../Page/真蜥腳類.md "wikilink")。他們也發現湖泊嘉峪龍及亞洲嘉峪龍有同樣的衍徵，而非不同。亞洲嘉峪龍有可能是[大鼻龍類](../Page/大鼻龍類.md "wikilink")，但分類依然不明確\[5\]。在最近的[蜥腳下目重新研究中](../Page/蜥腳下目.md "wikilink")，兩個物種都被認為是疑名\[6\]。

## 古生物學

嘉峪龍是一種大型的四足[草食性](../Page/草食性.md "wikilink")[蜥腳下目](../Page/蜥腳下目.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")。

## 參考資料

<div class="references-small">

<references>

</references>

</div>

[Category:大鼻龍類](../Category/大鼻龍類.md "wikilink")
[Category:侏羅紀恐龍](../Category/侏羅紀恐龍.md "wikilink")
[Category:下白堊紀恐龍](../Category/下白堊紀恐龍.md "wikilink")
[Category:亞洲恐龍](../Category/亞洲恐龍.md "wikilink")
[Category:可疑的恐龍](../Category/可疑的恐龍.md "wikilink")

1.

2.

3.
4.

5.

6.