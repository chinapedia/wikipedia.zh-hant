**鹰科**在[鸟类传统分类系统中是](../Page/鸟类传统分类系统.md "wikilink")[鸟纲中的](../Page/鸟纲.md "wikilink")[隼形目中的一个](../Page/隼形目.md "wikilink")[科](../Page/科_\(生物\).md "wikilink")，但現在改分至[鷹形目](../Page/鷹形目.md "wikilink")。是[鷹形目](../Page/鷹形目.md "wikilink")[鸟类两个最大的科之一](../Page/鸟.md "wikilink")。

在狹義的定義上，小型鷹科猛禽泛稱為[鷹](../Page/鷹.md "wikilink")，大型鷹科猛禽泛稱為[鵰](../Page/鵰.md "wikilink")，某些中型鷹科猛禽稱為[鵟](../Page/鵟.md "wikilink")。\[1\]

## 亞科與屬

  - **[鸢亚科](../Page/鸢亚科.md "wikilink") Elaninae**

      - [黑翅鸢](../Page/黑翅鸢.md "wikilink") *Elanus caeruleus*

      - [黑肩鸢](../Page/黑肩鸢.md "wikilink") *Elanus axillaris*

      - [白尾鸢](../Page/白尾鸢.md "wikilink") *Elanus leucurus*

      - [纹翅鸢](../Page/纹翅鸢.md "wikilink") *Elanus scriptus*

      - [剪尾鸢](../Page/剪尾鸢.md "wikilink") *Chelictinia riocourii*

      - [蝠鹞](../Page/蝠鹞.md "wikilink") *Machaerhamphus alcinus*

      - [珠鸢](../Page/珠鸢.md "wikilink") *Gampsonyx swainsonii*

      -
  - **[蜂鹰亚科](../Page/蜂鹰亚科.md "wikilink") Perninae**

      - [西非鹃隼](../Page/西非鹃隼.md "wikilink") *Aviceda cuculoides*
      - [马达加斯加鹃隼](../Page/马达加斯加鹃隼.md "wikilink") *Aviceda
        madagascariensis*
      - [褐冠鹃隼](../Page/褐冠鹃隼.md "wikilink") *Aviceda jerdoni*
      - [凤头鹃隼](../Page/凤头鹃隼.md "wikilink") *Aviceda subcristata*
      - [黑冠鹃隼](../Page/黑冠鹃隼.md "wikilink") *Aviceda leuphotes*
      - [长尾鵟](../Page/长尾鵟.md "wikilink") *Hernicopernis longicauda*
      - [黑长尾鹰](../Page/黑长尾鹰.md "wikilink") *Hernicopernis infuscatus*
      - [蜂鹰](../Page/蜂鹰.md "wikilink") *Pernis apivorus*
      - [凤头蜂鹰](../Page/凤头蜂鹰.md "wikilink") *Pernis ptilorhynchus*
      - [菲律宾蜂鹰](../Page/菲律宾蜂鹰.md "wikilink") *Pernis celebensis*
      - [黑头鸢](../Page/黑头鸢.md "wikilink") *Leptodon cayanensis*
      - [白颈鸢](../Page/白颈鸢.md "wikilink") *Leptodon forbesi*
      - [钩嘴鸢](../Page/钩嘴鸢.md "wikilink") *Chondrohierax uncinatus*
      - *[燕尾鸢](../Page/燕尾鸢.md "wikilink") Elanoides forficatus*

  -
  - **[齿鹰亚科](../Page/齿鹰亚科.md "wikilink") Milvinae**

      - [雙齒鷹](../Page/雙齒鷹.md "wikilink") *Harpagus bidentatus*
      - [棕腿齿鹰](../Page/棕腿齿鹰.md "wikilink") *Harpagus diodon*
      - [密西西比灰鸢](../Page/密西西比灰鸢.md "wikilink") *Ictinia
        mississippiensis*
      - [铅灰鸢](../Page/铅灰鸢.md "wikilink") *Ictinia plumbea*
      - [螺鸢](../Page/螺鸢.md "wikilink") *Rostrhamus sociabilis*
      - [细嘴螺鸢](../Page/细嘴螺鸢.md "wikilink") *Rostrhamus hamatus*
      - [啸栗鸢](../Page/啸栗鸢.md "wikilink") *Haliastur sphenurus*
      - [栗鸢](../Page/栗鸢.md "wikilink") *Haliastur indus*
      - [红鸢](../Page/红鸢.md "wikilink") *Milvus milvus*
      - [黑鸢](../Page/黑鸢.md "wikilink") *Milvus migrans*
      - [黑耳鸢](../Page/黑耳鸢.md "wikilink") *Milvus lineatus*
      - [方尾鸢](../Page/方尾鸢.md "wikilink") *Lophoictinia isura*
      - [黑胸钩嘴鸢](../Page/黑胸钩嘴鸢.md "wikilink") *Hamirostra melanosternon*

  - **[鹰亚科](../Page/鹰亚科.md "wikilink") Accipitrinae**

      - [苍鹰](../Page/苍鹰.md "wikilink") *Accipiter gentilis*
      - [雀鹰](../Page/雀鹰.md "wikilink") *Accipiter nisus*
      - [灰腹鹰](../Page/灰腹鹰.md "wikilink") *Accipiter poliogaster*
      - [凤头鹰](../Page/凤头鹰.md "wikilink") *Accipiter trivirgatus*
      - [灰头凤头鹰](../Page/灰头凤头鹰.md "wikilink") *Accipiter griseiceps*
      - [红胸鹰](../Page/红胸鹰.md "wikilink") *Accipiter toussenelii*
      - [非洲鹰](../Page/非洲鹰.md "wikilink") *Accipiter tachiro*
      - [赤腹鹰](../Page/赤腹鹰.md "wikilink") *Accipiter soloensis*
      - [马达加斯加鹰](../Page/马达加斯加鹰.md "wikilink") *Accipiter francesii*
      - [斑点尾鹰](../Page/斑点尾鹰.md "wikilink") *Accipiter trinotatus*
      - [灰鹰](../Page/灰鹰.md "wikilink") *Accipiter novaehollandiae*
      - [褐鹰](../Page/褐鹰.md "wikilink") *Accipiter fasciatus*
      - [黑背鹰](../Page/黑背鹰.md "wikilink") *Accipiter melanochlamys*
      - [鹊色鹰](../Page/鹊色鹰.md "wikilink") *Accipiter albogularis*
      - [棕颈鹰](../Page/棕颈鹰.md "wikilink") *Accipiter rufitorques*
      - [白腹鹰](../Page/白腹鹰.md "wikilink") *Accipiter haplochrous*
      - [摩鹿加雀鷹](../Page/摩鹿加雀鷹.md "wikilink") *Accipiter henicogrammus*
      - [苍头鹰](../Page/苍头鹰.md "wikilink") *Accipiter poliocephalus*
      - [灰头鹰](../Page/灰头鹰.md "wikilink") *Accipiter princeps*
      - [黑白雀鹰](../Page/黑白雀鹰.md "wikilink") *Accipiter melanoleucus*
      - [亨氏鹰](../Page/亨氏鹰.md "wikilink") *Accipiter henstii*
      - [白颊雀鹰](../Page/白颊雀鹰.md "wikilink") *Accipiter meyerianus*
      - [栗胁鹰](../Page/栗胁鹰.md "wikilink") *Accipiter castanilius*
      - [尼科巴群岛雀鹰](../Page/尼科巴群岛雀鹰.md "wikilink") *Accipiter butleri*
      - [东方雀鹰](../Page/东方雀鹰.md "wikilink") *Accipiter brevipes*
      - [兰灰雀鹰](../Page/兰灰雀鹰.md "wikilink") *Accipiter luteoschistaceus*
      - [拟雀鹰](../Page/拟雀鹰.md "wikilink") *Accipiter imitator*
      - [红腿雀鹰](../Page/红腿雀鹰.md "wikilink") *Accipiter erythropus*
      - [非洲小雀鹰](../Page/非洲小雀鹰.md "wikilink") *Accipiter minullus*
      - [日本松雀鹰](../Page/日本松雀鹰.md "wikilink") *Accipiter gularis*
      - [小雀鹰](../Page/小雀鹰.md "wikilink") *Accipiter nanus*
      - [红颈雀鹰](../Page/红颈雀鹰.md "wikilink") *Accipiter erythrauchen*
      - [领雀鹰](../Page/领雀鹰.md "wikilink") *Accipiter cirrocephalus*
      - [短尾雀鹰](../Page/短尾雀鹰.md "wikilink") *Accipiter brachyurus*
      - [葡萄胸雀鹰](../Page/葡萄胸雀鹰.md "wikilink") *Accipiter rhodogaster*
      - [马达加斯加雀鹰](../Page/马达加斯加雀鹰.md "wikilink") *Accipiter
        madagascariensis*
      - [南非雀鹰](../Page/南非雀鹰.md "wikilink") *Accipiter ovampensis*
      - [棕胸雀鹰](../Page/棕胸雀鹰.md "wikilink") *Accipiter rufiventris*
      - [褐耳雀鹰](../Page/褐耳雀鹰.md "wikilink") *Accipiter badius*
      - [侏儒鹰](../Page/侏儒鹰.md "wikilink") *Accipiter superciliosus*
      - [半领鹰](../Page/半领鹰.md "wikilink") *Accipiter collaris*
      - [条纹鹰](../Page/条纹鹰.md "wikilink") *Accipiter striatus*
      - [白胸鹰](../Page/白胸鹰.md "wikilink") *Accipiter chionogaster*
      - [平胸鹰](../Page/平胸鹰.md "wikilink") *Accipiter ventralis*
      - [红腿鹰](../Page/红腿鹰.md "wikilink") *Accipiter erythronemius*
      - [鸡鹰](../Page/鸡鹰.md "wikilink") *Accipiter cooperii*
      - [古巴鹰](../Page/古巴鹰.md "wikilink") *Accipiter gundlachi*
      - [双色鹰](../Page/双色鹰.md "wikilink") *Accipiter bicolor*
      - [松雀鹰](../Page/松雀鹰.md "wikilink") *Accipiter virgatus*
      - [红脸歌鹰](../Page/红脸歌鹰.md "wikilink") *Micronisus gabar*
      - [歌鹰](../Page/歌鹰.md "wikilink") *Melierax metabates*
      - [灰歌鹰](../Page/灰歌鹰.md "wikilink") *Melierax poliopterus*
      - [淡色歌鹰](../Page/淡色歌鹰.md "wikilink") *Melierax canorus*
      - [非洲长尾鹰](../Page/非洲长尾鹰.md "wikilink") *Urotriorchis macrourus*
      - [赤鹰](../Page/赤鹰.md "wikilink") *Erythrotriorchis radiatus*
      - [褐肩鹰](../Page/褐肩鹰.md "wikilink") *Erythrotriorchis buergersi*
      - [巨赤鹰](../Page/巨赤鹰.md "wikilink") *Megatriorchis doriae*

  - **[鵟亚科](../Page/鵟亚科.md "wikilink") Buteoninae**

      - [鵟雕](../Page/鵟雕.md "wikilink") *Geranoaetus melanoleucus*
      - [普通鵟](../Page/普通鵟.md "wikilink") *Buteo buteo*
      - [红尾鵟](../Page/红尾鵟.md "wikilink") *Buteo jamaicensis*
      - [棕尾鵟](../Page/棕尾鵟.md "wikilink") *Buteo rufinus*
      - [毛腿鵟](../Page/毛腿鵟.md "wikilink") *Buteo lagopus*
      - [王鵟](../Page/王鵟.md "wikilink") *Buteo regalis*
      - [赤肩鵟](../Page/赤肩鵟.md "wikilink") *Buteo lineatus*
      - [巨翅鵟](../Page/巨翅鵟.md "wikilink") *Buteo platypterus*
      - [斯温氏鵟](../Page/斯温氏鵟.md "wikilink") *Buteo swainsoni*
      - [大嘴鵟](../Page/大嘴鵟.md "wikilink") *Buteo magnirostris*
      - [岛鵟](../Page/岛鵟.md "wikilink") *Buteo ridgwayi*
      - [白腰鵟](../Page/白腰鵟.md "wikilink") *Buteo leucorrhous*
      - [短尾鵟](../Page/短尾鵟.md "wikilink") *Buteo brachyurus*
      - [白颈鵟](../Page/白颈鵟.md "wikilink") *Buteo albigula*
      - [白尾鵟](../Page/白尾鵟.md "wikilink") *Buteo albicaudatus*
      - [加拉帕戈斯群岛鵟](../Page/加拉帕戈斯群岛鵟.md "wikilink") *Buteo galapagoensis*
      - [红背鵟](../Page/红背鵟.md "wikilink") *Buteo polyosoma*
      - [变色鵟](../Page/变色鵟.md "wikilink") *Buteo poecilochrous*
      - [带尾鵟](../Page/带尾鵟.md "wikilink") *Buteo albonotatus*
      - [孤鵟](../Page/孤鵟.md "wikilink") *Buteo solitarius*
      - [美洲棕尾鵟](../Page/美洲棕尾鵟.md "wikilink") *Buteo ventralis*
      - [山鵟](../Page/山鵟.md "wikilink") *Buteo oreophilus*
      - [短翅鵟](../Page/短翅鵟.md "wikilink") *Buteo brachypterus*
      - [大鵟](../Page/大鵟.md "wikilink") *Buteo hemilasius*
      - [非洲赤尾鵟](../Page/非洲赤尾鵟.md "wikilink") *Buteo auguralis*
      - [棕鵟](../Page/棕鵟.md "wikilink") *Buteo augur*
      - [阿切氏鵟](../Page/阿切氏鵟.md "wikilink") *Buteo archeri*
      - [暗棕鵟](../Page/暗棕鵟.md "wikilink") *Buteo rufofuscus*
      - [栗翅鹰](../Page/栗翅鹰.md "wikilink") *Parabuteo unicinctus*
      - [黑鸡鵟](../Page/黑鸡鵟.md "wikilink") *Buteogallus anthracinus*
      - [红树黑鸡鵟](../Page/红树黑鸡鵟.md "wikilink") *Buteogallus subtilis*
      - [大黑鸡鵟](../Page/大黑鸡鵟.md "wikilink") *Buteogallus urubitinga*
      - [棕鸡鵟](../Page/棕鸡鵟.md "wikilink") *Buteogallus aequinoctialis*
      - [草原鹰](../Page/草原鹰.md "wikilink") *Buteogallus meridionalis*
      - [黑领鹰](../Page/黑领鹰.md "wikilink") *Busarellus nigricollis*
      - [铅头南美鵟](../Page/铅头南美鵟.md "wikilink") *Leucopternis plumbea*
      - [鼠灰南美鵟](../Page/鼠灰南美鵟.md "wikilink") *Leucopternis schistacea*
      - [横斑南美鵟](../Page/横斑南美鵟.md "wikilink") *Leucopternis princeps*
      - [黑脸南美鵟](../Page/黑脸南美鵟.md "wikilink") *Leucopternis melanops*
      - [白眉南美鵟](../Page/白眉南美鵟.md "wikilink") *Leucopternis kuhli*
      - [白颈南美鵟](../Page/白颈南美鵟.md "wikilink") *Leucopternis lacernulata*
      - [淡灰南美鵟](../Page/淡灰南美鵟.md "wikilink") *Leucopternis semiplumbea*
      - [白领南美鵟](../Page/白领南美鵟.md "wikilink") *Leucopternis albicollis*
      - [灰背南美鵟](../Page/灰背南美鵟.md "wikilink") *Leucopternis occidentalis*
      - [斗蓬南美鵟](../Page/斗蓬南美鵟.md "wikilink") *Leucopternis polionota*
      - [蜥鵟](../Page/蜥鵟.md "wikilink") *Kaupifalco monogrammicus*
      - [蝗鵟鹰](../Page/蝗鵟鹰.md "wikilink") *Butastur rufipennis*
      - [白眼鵟鹰](../Page/白眼鵟鹰.md "wikilink") *Butastur teesa*
      - [棕翅鵟鹰](../Page/棕翅鵟鹰.md "wikilink") *Butastur liventer*
      - [灰脸鵟鹰](../Page/灰脸鵟鹰.md "wikilink") *Butastur indicus*
      - [冕雕](../Page/冕雕.md "wikilink") *Harpyhaliaetus coronatus*
      - [孤冕雕](../Page/孤冕雕.md "wikilink") *Harpyhaliaetus solitarius*
      - [冠雕](../Page/冠雕.md "wikilink") *Morphnus guianensis*
      - [角雕](../Page/角雕.md "wikilink") *Harpia harpyja*
      - [食猴鹰](../Page/食猴鹰.md "wikilink") *Pithecophaga jefferyi*
      - [新几内亚角雕](../Page/新几内亚角雕.md "wikilink") *Harpyopsis novaeguineae*
      - [黑栗雕](../Page/黑栗雕.md "wikilink") *Oroaetus isidori*
      - [黑白鹰雕](../Page/黑白鹰雕.md "wikilink") *Spizastur melanoleucus*
      - [叫鹰雕](../Page/叫鹰雕.md "wikilink") *Spizaetus africanus*
      - [凤头鹰雕](../Page/凤头鹰雕.md "wikilink") *Spizaetus cirrhatus*
      - [鹰雕](../Page/鹰雕.md "wikilink") *Spizaetus nipalensis*
      - [马来鹰雕](../Page/马来鹰雕.md "wikilink") *Spizaetus alboniger*
      - [爪哇鹰雕](../Page/爪哇鹰雕.md "wikilink") *Spizaetus bartelsi*
      - [苏拉威西岛鹰雕](../Page/苏拉威西岛鹰雕.md "wikilink") *Spizaetus lanceolatus*
      - [菲律宾鹰雕](../Page/菲律宾鹰雕.md "wikilink") *Spizaetus philippensis*
      - [小鹰雕](../Page/小鹰雕.md "wikilink") *Spizaetus nanus*
      - [黑鹰雕](../Page/黑鹰雕.md "wikilink") *Spizaetus tyrannus*
      - [饰冠鹰雕](../Page/饰冠鹰雕.md "wikilink") *Spizaetus ornatus*
      - [高冠鹰雕](../Page/高冠鹰雕.md "wikilink") *Lophaetus occipitalis*
      - [冠鹰雕](../Page/冠鹰雕.md "wikilink") *Stephanoaetus coronatus*
      - [猛雕](../Page/猛雕.md "wikilink") *Polemaetus bellicosus*
      - [白腹隼雕](../Page/白腹隼雕.md "wikilink") *Hieraaetus fasciatus*
      - [靴雕](../Page/靴雕.md "wikilink") *Hieraaetus pennatus*
      - [非洲隼雕](../Page/非洲隼雕.md "wikilink") *Hieraaetus spilogaster*
      - [小雕](../Page/小雕.md "wikilink") *Hieraaetus morphnoides*
      - [艾氏雕](../Page/艾氏雕.md "wikilink") *Hieraaetus ayresii*
      - [棕腹隼雕](../Page/棕腹隼雕.md "wikilink") *Hieraaetus kienerii*
      - [金雕](../Page/金雕.md "wikilink") *Aquila chrysaetos*
      - [白肩雕](../Page/白肩雕.md "wikilink") *Aquila heliaca*
      - [西班牙白肩雕](../Page/西班牙白肩雕.md "wikilink") *Aquila (heliaca)
        adalberti*
      - [亚洲草原雕](../Page/亚洲草原雕.md "wikilink") *Aquila nipalensis*
      - [棕色草原雕](../Page/棕色草原雕.md "wikilink") *Aquila rapax*
      - [乌雕](../Page/乌雕.md "wikilink") *Aquila clanga*
      - [小乌雕](../Page/小乌雕.md "wikilink") *Aquila pomarina*
      - [黑雕](../Page/黑雕.md "wikilink") *Aquila verreauxii*
      - [格尼氏雕](../Page/格尼氏雕.md "wikilink") *Aquila gurneyi*
      - [细嘴雕](../Page/细嘴雕.md "wikilink") *Aquila wahlbergi*
      - [楔尾雕](../Page/楔尾雕.md "wikilink") *Aquila audax*
      - [林雕](../Page/林雕.md "wikilink") *Ictinaetus malayensis*
      - [白尾海雕](../Page/白尾海雕.md "wikilink") *Haliaeetus albicilla*
      - [白头海雕](../Page/白头海雕.md "wikilink") *Haliaeetus leucocephalus*
      - [虎头海雕](../Page/虎头海雕.md "wikilink") *Haliaeetus pelagicus*
      - [吼海雕](../Page/吼海雕.md "wikilink") *Haliaeetus vocifer*
      - [白腹海雕](../Page/白腹海雕.md "wikilink") *Haliaeetus leucogaster*
      - [所罗门群岛海雕](../Page/所罗门群岛海雕.md "wikilink") *Haliaeetus sanfordi*
      - [马达加斯加海雕](../Page/马达加斯加海雕.md "wikilink") *Haliaeetus
        vociferoides*
      - [玉带海雕](../Page/玉带海雕.md "wikilink") *Haliaeetus leucoryphus*
      - [小渔雕](../Page/小渔雕.md "wikilink") *Ichthyophaga humilis*
      - [灰头渔雕](../Page/灰头渔雕.md "wikilink") *Ichthyophaga ichthyaetus*

  - **[秃鹫亚科](../Page/秃鹫亚科.md "wikilink") Aegypiinae**

      - [黑兀鹫](../Page/黑兀鹫.md "wikilink") *Sarcogyps calvus*
      - [秃鹫](../Page/禿鷲_\(物種\).md "wikilink") *Aegypius monachus*
      - [肉垂秃鹫](../Page/肉垂秃鹫.md "wikilink") *Torgos tracheliotus*
      - [白头秃鹫](../Page/白头秃鹫.md "wikilink") *Trigonoceps occipitalis*
      - [西域兀鷲](../Page/西域兀鷲.md "wikilink") *Gyps fulvus*
      - [黑白兀鹫](../Page/黑白兀鹫.md "wikilink") *Gyps rueppellii*
      - [高山兀鹫](../Page/高山兀鹫.md "wikilink") *Gyps himalayensis*
      - [南非兀鹫](../Page/南非兀鹫.md "wikilink") *Gyps coprotheres*
      - [非洲白背兀鹫](../Page/非洲白背兀鹫.md "wikilink") *Gyps africanus*
      - [印度白背兀鹫](../Page/印度白背兀鹫.md "wikilink") *Gyps bengalensis*
      - [长喙兀鹫](../Page/长喙兀鹫.md "wikilink") *Gyps indicus*
      - [白兀鹫](../Page/白兀鹫.md "wikilink") *Neophron percnopterus*
      - [头巾兀鹫](../Page/头巾兀鹫.md "wikilink") *Necrosyrtes monachus*
      - [棕榈鹫](../Page/棕榈鹫.md "wikilink") *Gypohierax angolensis*
      - [胡兀鹫](../Page/胡兀鹫.md "wikilink") *Gypaetus barbatus*

  - **[鹞亚科](../Page/鹞亚科.md "wikilink") Circinae**

      - [乌灰鹞](../Page/乌灰鹞.md "wikilink") *Circus pygargus*
      - [白尾鹞](../Page/白尾鹞.md "wikilink") *Circus cyaneus*
      - [白头鹞](../Page/白头鹞.md "wikilink") *Circus aeruginosus*
      - [东方沼泽鹞](../Page/东方沼泽鹞.md "wikilink") *Circus spilonotus*
      - [非洲沼泽鹞](../Page/非洲沼泽鹞.md "wikilink") *Circus ranivorus*
      - [沼泽鹞](../Page/沼泽鹞.md "wikilink") *Circus approximans*
      - [马达加斯加鹞](../Page/马达加斯加鹞.md "wikilink") *Circus maillardi*
      - [长翅鹞](../Page/长翅鹞.md "wikilink") *Circus buffoni*
      - [斑点鹞](../Page/斑点鹞.md "wikilink") *Circus assimilis*
      - [黑鹞](../Page/黑鹞.md "wikilink") *Circus maurus*
      - [灰鹞](../Page/灰鹞.md "wikilink") *Circus cinereus*
      - [草原鹞](../Page/草原鹞.md "wikilink") *Circus macrourus*
      - [鹊鹞](../Page/鹊鹞.md "wikilink") *Circus melanoleucos*
      - [马达加斯加猎鹰](../Page/马达加斯加猎鹰.md "wikilink") *Polyboroides radiatus*
      - [非洲猎鹰](../Page/非洲猎鹰.md "wikilink") *Polyboroides typus*
      - [鹤鹰](../Page/鹤鹰.md "wikilink") *Geranospiza caerulescens*

  - **[蛇雕亚科](../Page/蛇雕亚科.md "wikilink") Circaetinae**

      - [短尾雕](../Page/短尾雕.md "wikilink") *Terathopius ecaudatus*
      - [短趾雕](../Page/短趾雕.md "wikilink") *Circaetus gallicus*
      - [黑胸短趾雕](../Page/黑胸短趾雕.md "wikilink") *Circaetus pectoralis*
      - [灰短趾雕](../Page/灰短趾雕.md "wikilink") *Circaetus cinereus*
      - [斑短趾雕](../Page/斑短趾雕.md "wikilink") *Circaetus fasciolatus*
      - [小斑短趾雕](../Page/小斑短趾雕.md "wikilink") *Circaetus cinerascens*
      - [大冠鷲](../Page/大冠鷲.md "wikilink") *Spilornis cheela*
      - [尼科巴群岛蛇雕](../Page/尼科巴群岛蛇雕.md "wikilink") *Spilornis minimus*
      - [山蛇雕](../Page/山蛇雕.md "wikilink")*Spilornis kinabaluensis*
      - [苏拉威西岛蛇雕](../Page/苏拉威西岛蛇雕.md "wikilink") *Spilornis rufipectus*
      - [菲律宾蛇雕](../Page/菲律宾蛇雕.md "wikilink") *Spilornis holospilus*
      - [安达曼群岛蛇雕](../Page/安达曼群岛蛇雕.md "wikilink") *Spilornis elgini*

## 參考文獻

  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
  -
## 外部链接

  - [鹰科介绍（英文）](http://www.pbs.org/wnet/nature/eagles/index.html)
  - [Accipitridae
    videos](http://ibc.lynxeds.com/family/hawks-eagles-accipitridae) on
    the Internet Bird Collection
  - xeno-canto.org: [Accipitridae
    sounds](http://www.xeno-canto.org/browse.php?query=accipitridae).
    Retrieved 2006-DEC-01.

[Category:鷹形目](../Category/鷹形目.md "wikilink")
[\*](../Category/鹰科.md "wikilink")

1.  《猛禽觀察圖鑑》，林文宏/著，第18頁。