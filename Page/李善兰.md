[Li_Shanlan.jpg](https://zh.wikipedia.org/wiki/File:Li_Shanlan.jpg "fig:Li_Shanlan.jpg")
[则古昔宅算学.jpg](https://zh.wikipedia.org/wiki/File:则古昔宅算学.jpg "fig:则古昔宅算学.jpg")

**李善兰**（）字**壬叔**，号**秋纫**，[中國](../Page/中國.md "wikilink")[清朝](../Page/清朝.md "wikilink")[數學家](../Page/數學家.md "wikilink")。[浙江省](../Page/浙江省.md "wikilink")[杭州府](../Page/杭州府.md "wikilink")[海宁县人](../Page/海宁县.md "wikilink")。为[清代数学史上的杰出代表](../Page/清代.md "wikilink")，中国近代数学的[先驱](../Page/先驱.md "wikilink")。通詩文，曾幫基督教傳教士翻譯[聖經](../Page/聖經.md "wikilink")。

## 生平

李善兰于清嘉庆十五年（1810年）1月2日生于浙江海宁县硖石镇。10岁即通《[九章算术](../Page/九章算术.md "wikilink")》，15岁通习《[几何原本](../Page/几何原本.md "wikilink")》六卷，17岁参加杭州乡试未中。道光二十五年（1845年）以所著《四元解》二卷呈浙江名士[顾观光](../Page/顾观光.md "wikilink")，说“深思七昼夜，尽通其法”\[1\]。从此钻研[天文](../Page/天文.md "wikilink")、[历算](../Page/历学.md "wikilink")，成为远近闻名的[数学家](../Page/数学家.md "wikilink")。

1852年－1866年受聘於[墨海书馆任编译](../Page/墨海书馆.md "wikilink")。同治二年（1863年）被招至[曾国藩幕中](../Page/曾国藩.md "wikilink")。同治五年（1866年）曾国藩出资三百金为李善兰刻《几何原本》后九卷\[2\]。1868年，入同文馆总教习，执教算法，前后八年。同治十三年（1874年）升户部主事。光绪二年（1876年）升员外郎。光绪八年（1882年）升郎中。

## 成就

曾独立发明[对数](../Page/对数.md "wikilink")[微积分](../Page/微积分.md "wikilink")，並在[組合](../Page/組合數學.md "wikilink")[恆等式方面提出](../Page/恆等式.md "wikilink")[李善蘭恆等式](../Page/李善蘭恆等式.md "wikilink")。35岁時刻印《[方圆阐幽](../Page/方圆阐幽.md "wikilink")》、《[弧矢启秘](../Page/弧矢启秘.md "wikilink")》和《[对数探源](../Page/对数探源.md "wikilink")》三种数学著作。

1867年，刊行《[则古昔斋算学十三种](../Page/则古昔斋算学十三种.md "wikilink")》（其中包括《方圆阐幽》，《弧矢启秘》，《[对数探源](../Page/对数探源.md "wikilink")》，《垛积比类》，《四元解》，《麟德术解》，《椭圆正术解》，《椭圆新术》，《椭圆拾遗》，《火器真诀》，《尖锥变法解》，《级数徊求》，（天算或问》）。

1872年著《考数根法》，发表于《中西闻见录》第二期，这是中算史上最早的一篇关于[素数的论文](../Page/素数.md "wikilink")\[3\]。

在1852年－1866年，与[伟烈亚力合译](../Page/伟烈亚力.md "wikilink")《几何原本》后9卷，完成[明代](../Page/明代.md "wikilink")[利玛窦](../Page/利玛窦.md "wikilink")、[徐光启未竟之业](../Page/徐光启.md "wikilink")。

又与伟烈亚力、[韦廉臣](../Page/韦廉臣.md "wikilink")、[艾约瑟合译](../Page/艾约瑟.md "wikilink")《[谈天](../Page/谈天.md "wikilink")》、《[代数学](../Page/代数学.md "wikilink")》、《[代微积拾级](../Page/代微积拾级.md "wikilink")》（美国[伊莱亚斯·罗密士著](../Page/伊莱亚斯·罗密士.md "wikilink")）、《[圆锥曲线说](../Page/圆锥曲线说.md "wikilink")》、《[奈端数理](../Page/奈端数理.md "wikilink")》、《[重学](../Page/重学.md "wikilink")》、《[植物学](../Page/植物学.md "wikilink")》等书，由墨海书馆雕版刊行，对中国知识界有很大影响。

## 影響

[LE-SHEN-LAN_AND_HIS_PUPILS.jpg](https://zh.wikipedia.org/wiki/File:LE-SHEN-LAN_AND_HIS_PUPILS.jpg "fig:LE-SHEN-LAN_AND_HIS_PUPILS.jpg")

在1852年至1859年中，共译书七、八部，计七、八十万字，直接引进大量[数学符号](../Page/数学符号.md "wikilink")：[＝](../Page/等於.md "wikilink")、[×](../Page/乘.md "wikilink")、[÷](../Page/除.md "wikilink")、[＜](../Page/小於.md "wikilink")、[＞](../Page/大於.md "wikilink")，而且他的翻译工作具独创性，创译了许多数学名词：[代数](../Page/代数.md "wikilink")、[常数](../Page/常数.md "wikilink")、[变数](../Page/变数.md "wikilink")、[已知数](../Page/已知数.md "wikilink")、[函数](../Page/函数.md "wikilink")、[系数](../Page/系数.md "wikilink")、[指数](../Page/指数.md "wikilink")、[级数](../Page/级数.md "wikilink")、[单项式](../Page/单项式.md "wikilink")、[多项式](../Page/多项式.md "wikilink")、[微分](../Page/微分.md "wikilink")、[横轴](../Page/横轴.md "wikilink")、[纵轴](../Page/笛卡儿坐标系.md "wikilink")、[切线](../Page/切线.md "wikilink")、[法线](../Page/法线.md "wikilink")、[曲线](../Page/曲线.md "wikilink")、[渐近线](../Page/渐近线.md "wikilink")、[相似等](../Page/相似.md "wikilink")，其他学科如：[植物等](../Page/植物.md "wikilink")，这些译名独具匠心，自然贴切，其中许多译名随同他的译著被引入[日本](../Page/日本.md "wikilink")，且沿用至今。

## 参考文献

### 引用

### 来源

  - 书籍

<!-- end list -->

  - 《[清史稿](../Page/清史稿.md "wikilink")》[卷507　列传二百九十四　畴人二](http://www.guoxue.com/shibu/24shi/qingshigao/qsgx_507.htm)

## 外部連結

  - 洪萬生：〈[《張文虎日記》中的李善蘭](http://mnie.myweb.hinet.net/6_6.pdf)〉。
  - [李善兰](http://www.nichinoken.co.jp/column/essay/sansu/2011_m02.html#no05/)
    创译了许多数学名词
  - [Biography](http://www-history.mcs.st-and.ac.uk/Biographies/Li_Shanlan.html)
    at the [MacTutor History of Mathematics
    archive](../Page/MacTutor数学史档案.md "wikilink")

## 参见

  - [华蘅芳](../Page/华蘅芳.md "wikilink")

{{-}}

[Category:清朝戶部主事](../Category/清朝戶部主事.md "wikilink")
[Category:清朝戶部員外郎](../Category/清朝戶部員外郎.md "wikilink")
[Category:清朝戶部郎中](../Category/清朝戶部郎中.md "wikilink")
[Category:清朝數學家](../Category/清朝數學家.md "wikilink")
[Category:清朝翻译家](../Category/清朝翻译家.md "wikilink")
[Category:湘军人物](../Category/湘军人物.md "wikilink")
[Category:嘉兴人](../Category/嘉兴人.md "wikilink")
[S善兰](../Category/李姓.md "wikilink")

1.  《李俨.钱宝琮科学史全集》卷8 《李善兰年谱》 320-349 页
2.  《李俨.钱宝琮科学史全集》卷8 《李善兰年谱》 338 页
3.  《李俨.钱宝琮科学史全集》卷8 《李善兰年谱》 341页