，是一部[華語電影](../Page/華語電影.md "wikilink")，由[馬偉豪](../Page/馬偉豪.md "wikilink")[導演](../Page/導演.md "wikilink")，首映於2003年，原著來自[幾米的同名](../Page/幾米.md "wikilink")[繪本](../Page/繪本.md "wikilink")《[地下鐵](../Page/地下鐵_\(繪本\).md "wikilink")》。

地下鐵[電影的](../Page/電影.md "wikilink")[故事分為兩個](../Page/故事.md "wikilink")，[粵語及](../Page/粵語.md "wikilink")[國語各一](../Page/國語.md "wikilink")，前者由[楊千嬅及](../Page/楊千嬅.md "wikilink")[梁朝偉當男女主角](../Page/梁朝偉.md "wikilink")，後者由[張震飾演一位青年](../Page/張震_\(演員\).md "wikilink")。
\[1\]

## 劇情

地下鐵電影故事描寫[友誼及](../Page/友誼.md "wikilink")[愛情](../Page/愛情.md "wikilink")，角色中沒有[壞人](../Page/壞人.md "wikilink")，只有梁朝偉飾演的[婚姻介紹所](../Page/婚姻介紹所.md "wikilink")[創辦人](../Page/創辦人.md "wikilink")，他的[性格有較多問題](../Page/性格.md "wikilink")、[自尊心重](../Page/自尊心.md "wikilink")、愛[說謊](../Page/說謊.md "wikilink")，也不失是個好人。

## 演員

|                                       |
| ------------------------------------- |
| **演員**                                |
| [梁朝偉](../Page/梁朝偉.md "wikilink")      |
| [楊千嬅](../Page/楊千嬅.md "wikilink")      |
| [葛民輝](../Page/葛民輝.md "wikilink")      |
| [范植偉](../Page/范植偉.md "wikilink")      |
| [黃婉君](../Page/黃婉君.md "wikilink")      |
| [董潔](../Page/董潔.md "wikilink")        |
| [張震](../Page/張震_\(演員\).md "wikilink") |
| [林雪](../Page/林雪.md "wikilink")        |
| [方力申](../Page/方力申.md "wikilink")      |
| [徐天佑](../Page/徐天佑.md "wikilink")      |
| [森美](../Page/森美.md "wikilink")        |
| [桂綸鎂](../Page/桂綸鎂.md "wikilink")      |
| [鄒凱光](../Page/鄒凱光.md "wikilink")      |
| [黃婉伶](../Page/黃婉伶.md "wikilink")      |
| [陳達志](../Page/陳達志.md "wikilink")      |
|                                       |

## 原聲帶

1.  地下鐵：[黃義達](../Page/黃義達.md "wikilink")
2.  迷宮：[梁朝偉](../Page/梁朝偉.md "wikilink")、[楊千嬅](../Page/楊千嬅.md "wikilink")
3.  開始
4.  序曲
5.  天使之歌
6.  Tout va Bien
7.  詠歎曲
8.  Quest-ce que je peux faire?
9.  放音樂
10. 快樂時光
11. 巧克力
12. 離開
13. 地下鐵
14. 張開你的眼睛
15. 你
16. 耶誕晚餐
17. 地下鐵：[蕭亞軒](../Page/蕭亞軒.md "wikilink")
18. 幸運列車
19. 星之碎片：[梁朝偉](../Page/梁朝偉.md "wikilink")、[楊千嬅](../Page/楊千嬅.md "wikilink")

## 提名

<table style="width:185%;">
<colgroup>
<col style="width: 58%" />
<col style="width: 21%" />
<col style="width: 15%" />
<col style="width: 14%" />
<col style="width: 8%" />
<col style="width: 67%" />
</colgroup>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>頒獎典禮</p></th>
<th><p>獎項</p></th>
<th><p>作品</p></th>
<th><p>角色</p></th>
<th><p>結果</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2004</p></td>
<td><p><a href="../Page/第9屆香港電影金紫荊獎.md" title="wikilink">第9屆香港電影金紫荊獎</a></p></td>
<td><p>最佳女主角</p></td>
<td><p>《<a href="../Page/地下鐵_(電影).md" title="wikilink">地下鐵</a>》</p></td>
<td><p><a href="../Page/楊千嬅.md" title="wikilink">楊千嬅</a> 張海約</p></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第4屆<a href="../Page/華語電影傳媒大獎.md" title="wikilink">華語電影傳媒大獎</a></p></td>
<td><p><a href="../Page/華語電影傳媒大獎最受歡迎女演員.md" title="wikilink">港台最受欢迎女演员</a></p></td>
<td><p>《<a href="../Page/地下鐵_(電影).md" title="wikilink">地下鐵</a>》</p></td>
<td><p><a href="../Page/楊千嬅.md" title="wikilink">楊千嬅</a> 張海約</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 参考文献

<div class="references-small">

<references />

</div>

## 外部連結

  -
  - {{@movies|fshk30311201|地下鐵}}

  -
  -
  -
  -
  -
  -
[Category:馬偉豪電影](../Category/馬偉豪電影.md "wikilink")
[3](../Category/2000年代香港電影作品.md "wikilink")
[Category:香港浪漫劇情片](../Category/香港浪漫劇情片.md "wikilink")
[Category:美亞電影](../Category/美亞電影.md "wikilink")

1.  [電影《地下鐵》簡介](http://www.dianying.com/ft/title/dxt2003)