**Sempron**是美國[AMD公司的入門級](../Page/AMD.md "wikilink")[微處理器](../Page/微處理器.md "wikilink")，中文官方名稱為「**閃龍**」（不過一般玩家則多根据其英文发音俗称为「**散步龍**」），用以取代[Duron處理器及與](../Page/Duron.md "wikilink")[英特爾公司的](../Page/英特爾.md "wikilink")[Celeron和](../Page/Celeron.md "wikilink")[Celeron
D處理器競爭](../Page/Celeron_D.md "wikilink")。

名字方面，“Sempron”來自拉丁文的“semper”，意即「每天」，代表Sempron為每日的運算之選。

## 開發歷程及功能

第一代Sempron處理器採用[Athlon
XP的](../Page/Athlon.md "wikilink")“Thoroughbred”及“Thorton”內核，使用[Socket
A插座](../Page/Socket_A.md "wikilink")，擁有256KB第二層快取及166 MHz
[FSB](../Page/前端总线.md "wikilink")（FSB 333）。而最後一款使用[Socket
A的](../Page/Socket_A.md "wikilink")3000+則使用Barton內核，擁有512KB第二層快取。從硬件及用家的角度來看，Socket
A(462)的Sempron算是Athlon XP的更名。現時所有Socket A的Sempron處理器已經停產。

第二代Sempron處理器使用[Athlon
64的](../Page/Athlon_64.md "wikilink")“Paris”及“Palermo”內核，以及[Socket
754插座](../Page/Socket_754.md "wikilink")，其第二層快取僅提供128KB和256KB。這些Sempron與Athlon
64擁有不少共通點，包括集成記憶體控制器、[HyperTransport匯流排及](../Page/HyperTransport.md "wikilink")[NX位元](../Page/NX位元.md "wikilink")，但早期的K8
Sempron不支援[AMD64](../Page/AMD64.md "wikilink")。第一款使用Socket
754的Sempron是3100+，然後是稍慢的2600+和2800+。

自Intel把[EM64T加進Celeron處理器後](../Page/EM64T.md "wikilink")，AMD也於2005年6月推出64位元的Sempron，支援NX防毒功能，但不支援Dual
DDR記憶體。型號方面，分別有2500+、2600+、2800+、3000+、3100+和3300+。這些處理器被用家稱為“Sempron
64”，但並非官方名稱。

2006年，AMD推出[Socket
AM2版本的Sempron處理器](../Page/Socket_AM2.md "wikilink")，其功能與之前的版本相同，但支援[DDR2
SDRAM記憶體控制器](../Page/DDR2_SDRAM.md "wikilink")。普通版本的[TDP功耗值為](../Page/TDP功耗.md "wikilink")62
W，低功耗小型化版本則為35 W。現正發售的Sempron處理器為S754及AM2版本。

2008年，AMD推出雙核版本的Sempron處理器，首款產品型號為2100+，時脈1800MHz，\[1\] TDP功耗值為65w。

2014年，AMD推出首款 Sempron
[APU](../Page/AMD加速處理器.md "wikilink")，使用[Kabini核心](../Page/AMD加速處理器列表#Kabini.md "wikilink")。以廉價及低功耗市場作為定位，具有雙核心和四核心兩個系列、全部均是28nm製程及25W熱設計功耗，全部Sempron
APU整合 AMD Radeon R3 圖形處理器、北橋及南橋，使用全新[Socket
AM1](../Page/Socket_AM1.md "wikilink") CPU 插座。

| AMD Sempron 處理器家族                                                                                                                           |
| ------------------------------------------------------------------------------------------------------------------------------------------- |
| 標誌                                                                                                                                          |
| 代號                                                                                                                                          |
| [AMD_Sempron_Processor_Logo.svg](https://zh.wikipedia.org/wiki/File:AMD_Sempron_Processor_Logo.svg "fig:AMD_Sempron_Processor_Logo.svg") |
| [Sempron_logo.png](https://zh.wikipedia.org/wiki/File:Sempron_logo.png "fig:Sempron_logo.png")                                             |
|                                                                                                                                             |
| <small>[AMD Sempron處理器列表](../Page/AMD_Sempron處理器列表.md "wikilink")</small>                                                                   |

## Socket A型號

### Thoroughbred B/Thorton核心 (130 nm)

  - L1快取：64 + 64 KB（數據 + 指令）
  - L2快取：256 KB，全速
  - [MMX](../Page/MMX.md "wikilink")，[3DNow\!](../Page/3DNow!.md "wikilink")，[SSE](../Page/SSE.md "wikilink")
  - [Socket A](../Page/Socket_A.md "wikilink") (EV6)
  - [FSB](../Page/FSB.md "wikilink")：166 MHz (FSB 333)
  - VCore: 1.60 V
  - 推出日期：2004年7月28日
  - 時脈：1500 MHz - 2000 MHz (2200+ 至 2800+)

### Barton核心 (130 nm)

  - L1快取：64 + 64 KB（數據 + 指令）
  - L2快取：512 KB，全速
  - [MMX](../Page/MMX.md "wikilink")，[3DNow\!](../Page/3DNow!.md "wikilink")，[SSE](../Page/SSE.md "wikilink")
  - [Socket A](../Page/Socket_A.md "wikilink") (EV6)
  - [FSB](../Page/FSB.md "wikilink")：166 MHz (FSB 333) - 200 MHz (FSB
    400)
  - VCore: 1.6 - 1.65 V
  - 推出日期：2004年9月17日
  - 時脈：2000 - 2200 MHz (3000+ 和 3300+)

## Socket 754型號

### Paris核心 (130 nm SOI)

  - L1快取：64 + 64 KB（數據 + 指令）
  - L2快取：128/256 KB，全速
  - [MMX](../Page/MMX.md "wikilink")，[3DNow\!](../Page/3DNow!.md "wikilink")，[SSE](../Page/SSE.md "wikilink")，[SSE2](../Page/SSE2.md "wikilink")
  - 整合記憶體控制器
  - [Socket 754](../Page/Socket_754.md "wikilink")，800 MHz
    [HyperTransport](../Page/HyperTransport.md "wikilink")
  - VCore: 1.40 V
  - 推出日期：2004年7月28日
  - 時脈：1800 MHz (3000+ 和 3100+)

### Palermo核心 (90 nm SOI)

  - Early models are downlabeled "Oakville" mobile Athlon64
  - L1快取：64 + 64 KB（數據 + 指令）
  - L2快取：128/256 KB，全速
  - [MMX](../Page/MMX.md "wikilink")，[3DNow\!](../Page/3DNow!.md "wikilink")，[SSE](../Page/SSE.md "wikilink")，[SSE2](../Page/SSE2.md "wikilink")，[SSE3](../Page/SSE3.md "wikilink")
  - Sempron 3000+
    或更高型號支援[Cool'n'Quiet](../Page/Cool'n'Quiet.md "wikilink")
  - 由步進 E6 開始支援[AMD64](../Page/AMD64.md "wikilink")
  - 加強防毒功能（[NX bit](../Page/NX_bit.md "wikilink")）
  - 整合記憶體控制器
  - [Socket 754](../Page/Socket_754.md "wikilink")，800 MHz
    [HyperTransport](../Page/HyperTransport.md "wikilink")
  - VCore: 1.40 V
  - 推出日期：2005年2月
  - 時脈：1400 - 2000 MHz
      - 128 KB L2快取：1600 - 2000 MHz (2600+ 至 3300+)
      - 256 KB L2快取：1400 - 2000 MHz (2500+ 至 3400+)
  - 步進：**D0** (Part No.: \*BA), **E3** (Part No.: \*BO), **E6** (Part
    No.: \*BX)

## Socket 939型號

### Palermo核心 (90 nm SOI)

  - L1快取：64 + 64 KB（數據 + 指令）
  - L2快取：128/256 KB，全速
  - [MMX](../Page/MMX.md "wikilink")，[3DNow\!](../Page/3DNow!.md "wikilink")，[SSE](../Page/Streaming_SIMD_Extensions.md "wikilink")，[SSE2](../Page/SSE2.md "wikilink")，[SSE3](../Page/SSE3.md "wikilink")，[AMD64](../Page/AMD64.md "wikilink")（只限步進E6）,
    [Cool'n'Quiet](../Page/Cool'n'Quiet.md "wikilink")，[NX
    bit](../Page/NX_bit.md "wikilink")
  - 整合記憶體控制器
  - [Socket 939](../Page/Socket_939.md "wikilink")，800 MHz
    [HyperTransport](../Page/HyperTransport.md "wikilink")
  - VCore: 1.35/1.4 V
  - 推出日期：2005年10月
  - 時脈：1800 - 2000 MHz
      - 128 KB L2快取（3000+ 和 3400+）
      - 256 KB L2快取（3200+ 和 3500+）
  - 步進：**E3** (Part No.: \*BP), **E6** (Part No.: \*BW)

## Socket AM2型號

### Manila核心 (90 nm SOI)

  - L1快取：64 + 64 KB（數據 + 指令）
  - L2快取：128/256 KB，全速
  - [MMX](../Page/MMX.md "wikilink")，[3DNow\!](../Page/3DNow!.md "wikilink")，[SSE](../Page/SSE.md "wikilink")，[SSE2](../Page/SSE2.md "wikilink")，[SSE3](../Page/SSE3.md "wikilink")，[AMD64](../Page/AMD64.md "wikilink")，[NX
    bit](../Page/NX_bit.md "wikilink")
  - Sempron 3200+
    或更高型號支援[Cool'n'Quiet](../Page/Cool'n'Quiet.md "wikilink")
  - 整合128-bit（雙通道）[DDR2](../Page/DDR2.md "wikilink") 記憶體控制器
  - Socket AM2, 800 MHz
    [HyperTransport](../Page/HyperTransport.md "wikilink")
  - VCore: 1.25 - 1.4 V (Energy Efficient SFF 版本為 1.20 - 1.25V)
  - 推出日期：2006年5月23日
  - 時脈：1600 - 2200 MHz
      - 128 KB L2快取：1600 - 2000 MHz (2800+ 至 3500+)
      - 256 KB L2快取：1600 - 2200 MHz (3000+ 至 3800+)
  - 步進：**F2** (Part No.: \*CN, \*CW)

### Sparta核心 (65 nm SOI)

  - L1快取：64 + 64 KB（數據 + 指令）
  - L2快取：256 KB，全速
  - [MMX](../Page/MMX.md "wikilink")，[3DNow\!](../Page/3DNow!.md "wikilink")，[SSE](../Page/SSE.md "wikilink")，[SSE2](../Page/SSE2.md "wikilink")，[SSE3](../Page/SSE3.md "wikilink")，[AMD64](../Page/AMD64.md "wikilink")，[NX
    bit](../Page/NX_bit.md "wikilink")，[Cool'n'Quiet](../Page/Cool'n'Quiet.md "wikilink")
  - 整合128-bit（雙通道）[DDR2](../Page/DDR2.md "wikilink") 記憶體控制器
  - [Socket AM2](../Page/Socket_AM2.md "wikilink")，800 MHz
    [HyperTransport](../Page/HyperTransport.md "wikilink")
  - VCore: 1.20/1.40 V
  - 推出日期：2007年8月20日
  - 時脈：1900 - 2300 MHz
      - 256 KB L2快取：1900 - 2000 MHz (LE-1100 至 LE-1150)
      - 512 KB L2快取：2200 - 2300 MHz (LE-1250 至 LE-1300)
  - 步進：**G1**、**G2** (Part No.: \*DE, \*DP)

### Sherman核心 (65 nm SOI)

  - L1快取：64KB x 2 + 64KB x 2（數據 + 指令）
  - L2快取：256KB x 2，全速
  - [MMX](../Page/MMX.md "wikilink")，[3DNow\!](../Page/3DNow!.md "wikilink")，[SSE](../Page/SSE.md "wikilink")，[SSE2](../Page/SSE2.md "wikilink")，[SSE3](../Page/SSE3.md "wikilink")，[AMD64](../Page/AMD64.md "wikilink")，[NX
    bit](../Page/NX_bit.md "wikilink")，[Cool'n'Quiet](../Page/Cool'n'Quiet.md "wikilink")
  - 整合128-bit（雙通道）[DDR2](../Page/DDR2.md "wikilink") 記憶體控制器
  - [Socket AM2](../Page/Socket_AM2.md "wikilink")，800 MHz
    [HyperTransport](../Page/HyperTransport.md "wikilink")
  - VCore: 1.30 V
  - 推出日期：2008年2月19日
  - 時脈：1800 - 2200 Mhz
      - 256 KB x 2 L2快取：1800 MHz (Sempron X2-2100+) (SDO2100IAA4DO)
      - 256 KB x 2 L2快取：2000 MHz (Semrpon X2-2200+) (SDO2200IAA4DO)
      - 256 KB x 2 L2快取：2200 MHz (Sempron X2-2300+) (SDO2300IAA4DO)
  - 步進：**G2**

## Socket AM3型號

### Sargas核心 (45 nm SOI)

  - L1快取:64 + 64 KB（數據 + 指令）
  - L2快取:1024 KB，全速
  - [MMX](../Page/MMX.md "wikilink")，[3DNow\!](../Page/3DNow!.md "wikilink")，[SSE](../Page/SSE.md "wikilink")，[SSE2](../Page/SSE2.md "wikilink")，[SSE3](../Page/SSE3.md "wikilink")，[SSE4a](../Page/SSE4a.md "wikilink")，[AMD64](../Page/AMD64.md "wikilink")，[NX
    bit](../Page/NX_bit.md "wikilink")
  - 支援[Cool'n'Quiet](../Page/Cool'n'Quiet.md "wikilink")
  - 時脈：2700 MHz
  - 功耗：45 W

### Regor核心 (45 nm SOI)

  - L1快取: 64 + 64 KB (数据 + 指令）
  - L2快取:1024 KB,全速
  - [MMX](../Page/MMX.md "wikilink"),[3DNow\!](../Page/3DNow!.md "wikilink"),
    [SSE](../Page/SSE.md "wikilink"),
    [SSE2](../Page/SSE2.md "wikilink"),
    [SSE3](../Page/SSE3.md "wikilink"),
    [SSE4a](../Page/SSE4a.md "wikilink"),
    [AMD64](../Page/AMD64.md "wikilink"),[AMD-V](../Page/AMD-V.md "wikilink")
  - 支持[Cool'n'Quiet](../Page/Cool'n'Quiet.md "wikilink")
  - 时脉：2400-2500 MHz
  - 功耗：45 W

## Socket AM1型號

### Kabini核心 (28 nm SOI)

  - L1快取:64 + 64 KB（數據 + 指令）
  - L2快取:1024 KB，全速
  - [MMX](../Page/MMX.md "wikilink")，[SSE](../Page/SSE.md "wikilink")，[SSE2](../Page/SSE2.md "wikilink")，[SSE3](../Page/SSE3.md "wikilink")，[SSSE3](../Page/SSSE3.md "wikilink")，[SSE4.1](../Page/SSE4.1.md "wikilink")，[SSE4.2](../Page/SSE4.2.md "wikilink")，[SSE4A](../Page/SSE4A.md "wikilink")，[AMD64](../Page/AMD64.md "wikilink")，[AMD-V](../Page/AMD-V.md "wikilink")，[AES](../Page/AES.md "wikilink")，[AVX](../Page/AVX.md "wikilink")
  - 支援[Cool'n'Quiet](../Page/Cool'n'Quiet.md "wikilink")
  - 時脈：雙核心 1450 MHz
  - 整合 AMD Radeon R3 圖形處理器 (8個CU單元共128個流處理器, 時脈：400 MHz)
  - 功耗：25 W

<!-- end list -->

  - L1快取:128 + 128 KB（數據 + 指令）
  - L2快取:2048 KB，全速
  - [MMX](../Page/MMX.md "wikilink")，[SSE](../Page/SSE.md "wikilink")，[SSE2](../Page/SSE2.md "wikilink")，[SSE3](../Page/SSE3.md "wikilink")，[SSSE3](../Page/SSSE3.md "wikilink")，[SSE4.1](../Page/SSE4.1.md "wikilink")，[SSE4.2](../Page/SSE4.2.md "wikilink")，[SSE4A](../Page/SSE4A.md "wikilink")，[AMD64](../Page/AMD64.md "wikilink")，[AMD-V](../Page/AMD-V.md "wikilink")，[AES](../Page/AES.md "wikilink")，[AVX](../Page/AVX.md "wikilink")
  - 支援[Cool'n'Quiet](../Page/Cool'n'Quiet.md "wikilink")
  - 時脈：四核心 1300 MHz
  - 整合 AMD Radeon R3 圖形處理器 (8個CU單元共128個流處理器, 時脈：450 MHz)
  - 功耗：25 W

## 參看

  - [AMD處理器列表](../Page/AMD處理器列表.md "wikilink")
  - [AMD Sempron處理器列表](../Page/AMD_Sempron處理器列表.md "wikilink")
  - [AMD Athlon處理器列表](../Page/AMD_Athlon處理器列表.md "wikilink")
  - [AMD Athlon XP處理器列表](../Page/AMD_Athlon_XP處理器列表.md "wikilink")
  - [AMD Athlon 64處理器列表](../Page/AMD_Athlon_64處理器列表.md "wikilink")
  - [AMD Athlon X2處理器列表](../Page/AMD_Athlon_X2處理器列表.md "wikilink")
  - [AMD Phenom處理器列表](../Page/AMD_Phenom處理器列表.md "wikilink")

## 參考文獻

[Category:AMD处理器](../Category/AMD处理器.md "wikilink")
[Category:X86架構](../Category/X86架構.md "wikilink")

1.  [亮出秘密武器
    AMD雙核閃龍2100+到貨](http://financenews.sina.com/sinacn/304-000-106-109/2008-02-25/0252712907.html)