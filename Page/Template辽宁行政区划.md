[大东区](../Page/大东区.md "wikilink"){{.w}}[皇姑区](../Page/皇姑区.md "wikilink"){{.w}}[铁西区](../Page/铁西区_\(沈阳市\).md "wikilink"){{.w}}[苏家屯区](../Page/苏家屯区.md "wikilink"){{.w}}[浑南区](../Page/浑南区.md "wikilink"){{.w}}[沈北新区](../Page/沈北新区.md "wikilink"){{.w}}[于洪区](../Page/于洪区.md "wikilink"){{.w}}[辽中区](../Page/辽中区.md "wikilink"){{.w}}[新民市](../Page/新民市.md "wikilink"){{.w}}[康平县](../Page/康平县.md "wikilink"){{.w}}[法库县](../Page/法库县.md "wikilink")

|group3 = [大连市](../Page/大连市.md "wikilink") |list3 =
[中山区](../Page/中山区_\(大连市\).md "wikilink"){{.w}}[西岗区](../Page/西岗区.md "wikilink"){{.w}}[沙河口区](../Page/沙河口区.md "wikilink"){{.w}}[甘井子区](../Page/甘井子区.md "wikilink"){{.w}}[旅顺口区](../Page/旅顺口区.md "wikilink"){{.w}}[金州区](../Page/金州区.md "wikilink"){{.w}}[普兰店区](../Page/普兰店区.md "wikilink"){{.w}}[瓦房店市](../Page/瓦房店市.md "wikilink"){{.w}}-{[庄河市](../Page/庄河市.md "wikilink")}-{{.w}}[长海县](../Page/长海县.md "wikilink")
}}

|group3style = text-align: center; |group3 =
[地级市](../Page/地级市.md "wikilink") |list3 =
[铁西区](../Page/铁西区_\(鞍山市\).md "wikilink"){{.w}}[立山区](../Page/立山区.md "wikilink"){{.w}}[千山区](../Page/千山区.md "wikilink"){{.w}}[海城市](../Page/海城市.md "wikilink"){{.w}}[台安县](../Page/台安县.md "wikilink"){{.w}}[岫岩满族自治县](../Page/岫岩满族自治县.md "wikilink")

|group5 = [抚顺市](../Page/抚顺市.md "wikilink") |list5 =
[顺城区](../Page/顺城区.md "wikilink"){{.w}}[新抚区](../Page/新抚区.md "wikilink"){{.w}}[东洲区](../Page/东洲区.md "wikilink"){{.w}}[望花区](../Page/望花区.md "wikilink"){{.w}}[抚顺县](../Page/抚顺县.md "wikilink"){{.w}}[清原满族自治县](../Page/清原满族自治县.md "wikilink"){{.w}}[新宾满族自治县](../Page/新宾满族自治县.md "wikilink")

|group6 = [本溪市](../Page/本溪市.md "wikilink") |list6 =
[平山区](../Page/平山区.md "wikilink"){{.w}}[溪湖区](../Page/溪湖区.md "wikilink"){{.w}}[明山区](../Page/明山区.md "wikilink"){{.w}}[南芬区](../Page/南芬区.md "wikilink"){{.w}}[本溪满族自治县](../Page/本溪满族自治县.md "wikilink"){{.w}}[桓仁满族自治县](../Page/桓仁满族自治县.md "wikilink")

|group7 = [丹东市](../Page/丹东市.md "wikilink") |list7 =
[振兴区](../Page/振兴区.md "wikilink"){{.w}}[元宝区](../Page/元宝区.md "wikilink"){{.w}}[振安区](../Page/振安区.md "wikilink"){{.w}}[东港市](../Page/东港市.md "wikilink"){{.w}}[凤城市](../Page/凤城市.md "wikilink"){{.w}}[宽甸满族自治县](../Page/宽甸满族自治县.md "wikilink")

|group8 = [锦州市](../Page/锦州市.md "wikilink") |list8 =
[太和区](../Page/太和区.md "wikilink"){{.w}}[古塔区](../Page/古塔区.md "wikilink"){{.w}}[凌河区](../Page/凌河区.md "wikilink"){{.w}}[凌海市](../Page/凌海市.md "wikilink"){{.w}}[北镇市](../Page/北镇市.md "wikilink"){{.w}}[黑山县](../Page/黑山县.md "wikilink"){{.w}}[义县](../Page/义县.md "wikilink")

|group9 = [营口市](../Page/营口市.md "wikilink") |list9 =
[站前区](../Page/站前区.md "wikilink"){{.w}}[西市区](../Page/西市区.md "wikilink"){{.w}}[鲅鱼圈区](../Page/鲅鱼圈区.md "wikilink"){{.w}}[老边区](../Page/老边区.md "wikilink"){{.w}}[盖州市](../Page/盖州市.md "wikilink"){{.w}}[大石桥市](../Page/大石桥市.md "wikilink")

|group10 = [阜新市](../Page/阜新市.md "wikilink") |list10 =
[海州区](../Page/海州区_\(阜新市\).md "wikilink"){{.w}}[新邱区](../Page/新邱区.md "wikilink"){{.w}}[太平区](../Page/太平区_\(阜新市\).md "wikilink"){{.w}}[清河门区](../Page/清河门区.md "wikilink"){{.w}}[细河区](../Page/细河区.md "wikilink"){{.w}}[彰武县](../Page/彰武县.md "wikilink"){{.w}}[阜新蒙古族自治县](../Page/阜新蒙古族自治县.md "wikilink")

|group11 = [辽阳市](../Page/辽阳市.md "wikilink") |list11 =
[文圣区](../Page/文圣区.md "wikilink"){{.w}}[白塔区](../Page/白塔区.md "wikilink"){{.w}}[宏伟区](../Page/宏伟区.md "wikilink"){{.w}}[弓长岭区](../Page/弓长岭区.md "wikilink"){{.w}}[太子河区](../Page/太子河区.md "wikilink"){{.w}}[灯塔市](../Page/灯塔市.md "wikilink"){{.w}}[辽阳县](../Page/辽阳县.md "wikilink")

|group12 = [盘锦市](../Page/盘锦市.md "wikilink") |list12 =
[兴隆台区](../Page/兴隆台区.md "wikilink"){{.w}}[双台子区](../Page/双台子区.md "wikilink"){{.w}}[大洼区](../Page/大洼区.md "wikilink"){{.w}}[盘山县](../Page/盘山县.md "wikilink")

|group13 = [铁岭市](../Page/铁岭市.md "wikilink") |list13 =
[银州区](../Page/银州区.md "wikilink"){{.w}}[清河区](../Page/清河区_\(铁岭市\).md "wikilink"){{.w}}[调兵山市](../Page/调兵山市.md "wikilink"){{.w}}[开原市](../Page/开原市.md "wikilink"){{.w}}[铁岭县](../Page/铁岭县.md "wikilink"){{.w}}[西丰县](../Page/西丰县.md "wikilink"){{.w}}[昌图县](../Page/昌图县.md "wikilink")

|group14 = [朝阳市](../Page/朝阳市.md "wikilink") |list14 =
[双塔区](../Page/双塔区.md "wikilink"){{.w}}[龙城区](../Page/龙城区_\(朝阳市\).md "wikilink"){{.w}}[北票市](../Page/北票市.md "wikilink"){{.w}}[凌源市](../Page/凌源市.md "wikilink"){{.w}}[朝阳县](../Page/朝阳县.md "wikilink"){{.w}}[建平县](../Page/建平县.md "wikilink"){{.w}}[喀喇沁左翼蒙古族自治县](../Page/喀喇沁左翼蒙古族自治县.md "wikilink")

|group15 = [葫芦岛市](../Page/葫芦岛市.md "wikilink") |list15 =
[龙港区](../Page/龙港区.md "wikilink"){{.w}}[连山区](../Page/连山区.md "wikilink"){{.w}}[南票区](../Page/南票区.md "wikilink"){{.w}}[兴城市](../Page/兴城市.md "wikilink"){{.w}}[绥中县](../Page/绥中县.md "wikilink"){{.w}}[建昌县](../Page/建昌县.md "wikilink")
}} |belowstyle = text-align: left; font-size: 80%; |below =
注：[沈阳市](../Page/沈阳市.md "wikilink")、[大连市为](../Page/大连市.md "wikilink")[副省级市](../Page/副省级市.md "wikilink")。
参见：[中华人民共和国县级以上行政区列表](../Page/中华人民共和国县级以上行政区列表.md "wikilink")、[辽宁省乡级以上行政区列表](../Page/辽宁省乡级以上行政区列表.md "wikilink")。
}}<noinclude> </noinclude>

[Category:中华人民共和国各省级行政区行政区划模板](../Category/中华人民共和国各省级行政区行政区划模板.md "wikilink")
[\*](../Category/辽宁行政区划.md "wikilink")
[辽宁行政区划导航模板](../Category/辽宁行政区划导航模板.md "wikilink")