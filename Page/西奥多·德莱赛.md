**西奥多·赫曼·阿尔伯特·德莱赛**(**Theodore Herman Albert
Dreiser**，)，是一位以探索充满磨难的现实生活著称的美国[自然主义作家](../Page/自然主义.md "wikilink")，他是美国现代小说的先驱和代表作家，被认为是同[海明威](../Page/海明威.md "wikilink")、[福克纳并列的美国现代小说的三巨头之一](../Page/福克纳.md "wikilink")\[1\]。

## 生平

他出生在[印地安纳州](../Page/印地安纳州.md "wikilink")[特雷霍特的一个](../Page/特雷霍特.md "wikilink")[德裔美国人家庭](../Page/德裔美国人.md "wikilink")。著名的词作者[保罗·德莱赛](../Page/保罗·德莱赛.md "wikilink")(Paul
Dresser)(1859年-1906年)是他的哥哥。在1889年-1890年间，西奥多在由于考试不及格退学之前一直就读于[印地安纳大学布卢明顿分校](../Page/印第安那大學.md "wikilink")。不出几年，他开始为芝加哥环球报(*Chicago
Globe*)撰稿，然后是圣路易斯环球---民主党报(*St. Louis
Globe-Democrat*)。1892年，他与莎拉·怀特(Sara
White)结婚。虽然他们在1909年分开，却一直没有正式离婚。

### 写作生涯

[Theodore_Dreiser_2.jpg](https://zh.wikipedia.org/wiki/File:Theodore_Dreiser_2.jpg "fig:Theodore_Dreiser_2.jpg")
他的第一部小说是1900年的《[嘉莉妹妹](../Page/嘉莉妹妹.md "wikilink")》(Sister
Carrie)，讲述了一个青年女子逃离乡村生活奔向[芝加哥追求幸福生活的故事](../Page/芝加哥.md "wikilink")，展现了年轻女性在成长中的渐渐独立(不再依靠男人养活，靠自己的能力付出而生活)和觉醒(金钱和物质享受并不等于幸福)的过程。發行上并没有花力气推广此书，因此销量不佳。

德莱赛此后开始编辑女性杂志，一直到1910年由于办公室恋情不得不辞去工作。他的第二本小说《[珍妮姑娘](../Page/珍妮姑娘.md "wikilink")》
(Jennie
Gerhardt)于1911年发表，描写穷姑娘珍妮和富家子弟莱斯特相爱、后来孤独一生的惨状，作品对照鲜明、生动刻画了美国社会中的贫富悬殊的状况。德莱赛许多随后的小说都探讨社会不公现象。

他的第一部取得商业成功的小说是《[美国的悲剧](../Page/美国的悲剧.md "wikilink")》(An American
Tragedy)
(1925)，也是他艺术创作的高峰。它展示了对20世纪初[美国人经历的深刻理解](../Page/美国人.md "wikilink")、无止境的欲望和普遍的幻灭感。作品通过描写穷[教士子克莱特](../Page/教士.md "wikilink")·格里菲斯为追逐金钱、地位而堕落为故意杀人犯的故事，揭示利己主义恶性膨胀的严重后果，更揭露了金钱至上的生活方式对人性的毒害\[2\]。这部作品使他的声望盛极一时，并分别于1931年及1951年拍摄成电影。

其他的作品包括*[欲望三部曲](../Page/欲望三部曲.md "wikilink")* (Trilogy of
Desire)：*[金融家](../Page/金融家.md "wikilink")*(The
Financier)(1912年)，*[巨人](../Page/巨人.md "wikilink")*(The
Titan)(1914年)，及*[斯多葛派](../Page/斯多葛派.md "wikilink")*(The
Stoic)(于1947年死后完成)。这三部曲的中心人物是金融巨头柯帕乌，描写了他从[南北战争开始从一个普通的](../Page/南北战争.md "wikilink")[经纪人发展成百万富翁](../Page/经纪人.md "wikilink")，作品从19世纪中叶写到20世纪初，从美洲大陆写到欧洲大陆，在广阔的空间和时间里，生动而深刻地描绘了美国的历史风貌\[3\]。

在1935年[印地安纳州Warsaw地区图书馆理事下令将德莱赛所有著作烧毁](../Page/印地安纳州.md "wikilink")。

1917年后德莱赛思想开始倾向[共产主义](../Page/共产主义.md "wikilink")，并于1928年应邀访问[苏联](../Page/苏联.md "wikilink")，1945年8月加入[美国共产党](../Page/美国共产党.md "wikilink")，同年12月28日逝世。

德莱赛的作品充满长句，对细节有着极大的关注。由于他的作品探讨社会地位及人们对物欲的追求，他作品的现实主义风格及对细节的描写有力地烘托了主题。值得注意的是，德莱赛并不以他的风格著称，而是他作品的现实性，他笔下人物性格的发展变化以及他对美国生活的看法使他闻名。

## 小说列表

| 出版年份 | 英文名                 | 中文名                                     | 备注                                             |
| ---- | ------------------- | --------------------------------------- | ---------------------------------------------- |
| 1900 | Sister Carrie       | [嘉莉妹妹](../Page/嘉莉妹妹.md "wikilink")      | [20世紀百大英文小說](../Page/20世紀百大英文小說.md "wikilink") |
| 1911 | Jennie Gerhardt     | [珍妮姑娘](../Page/珍妮姑娘.md "wikilink")      |                                                |
| 1912 | The Financier       | [金融家](../Page/金融家.md "wikilink")        | [欲望三部曲](../Page/欲望三部曲.md "wikilink")           |
| 1914 | The Titan           | [巨人](../Page/巨人_\(小说\).md "wikilink")   | [欲望三部曲](../Page/欲望三部曲.md "wikilink")           |
| 1915 | The Genius          | [天才](../Page/天才_\(小说\).md "wikilink")   |                                                |
| 1925 | An American Tragedy | [美国的悲剧](../Page/美国的悲剧.md "wikilink")    | [20世紀百大英文小說](../Page/20世紀百大英文小說.md "wikilink") |
| 1946 | The Bulwark         | [堡垒](../Page/堡垒.md "wikilink")          |                                                |
| 1947 | The Stoic           | [斯多葛](../Page/斯多葛_\(小说\).md "wikilink") | [欲望三部曲](../Page/欲望三部曲.md "wikilink")           |

## 参考资料

## 外部链接

  - 英文

<!-- end list -->

  - [The International Theodore Dreiser
    Society](https://web.archive.org/web/20120212224242/http://www.uncw.edu/dreiser/)

  - [Review-Essay: Dreiser on the
    Web](https://web.archive.org/web/20120523232159/http://www.uncw.edu/dreiser/TDweb.htm)

  - [DreiserWebSource. University of Pennsylvania Library, Philadelphia,
    PA.](http://www.library.upenn.edu/collections/rbm/dreiser/)

  -
<!-- end list -->

  - 中文

<!-- end list -->

  - [美国现代小说的先驱西奥多·德莱塞](http://ewen.cc/cache/books/81/bkview-80811-192208.htm)

[Category:美国小说家](../Category/美国小说家.md "wikilink")
[Category:美国自传作家](../Category/美国自传作家.md "wikilink")
[Category:德國裔美國人](../Category/德國裔美國人.md "wikilink")
[Category:印第安納州人](../Category/印第安納州人.md "wikilink")
[Category:葬于加利福尼亚州格伦代尔森林草坪纪念公园](../Category/葬于加利福尼亚州格伦代尔森林草坪纪念公园.md "wikilink")
[Category:美国进步时代](../Category/美国进步时代.md "wikilink")

1.

2.
3.