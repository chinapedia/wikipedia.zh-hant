**马克西米利安·埃曼努埃尔·路德维希·玛利亚**（***Maximilian Emanuel Ludwig
Maria***，），简称巴伐利亚的马克斯，[巴伐利亚王子](../Page/巴伐利亚.md "wikilink")，巴伐利亞的公爵（Herzog
**in**
Bayern），已被[废黜的](../Page/废黜.md "wikilink")[巴伐利亞王國](../Page/巴伐利亞王國.md "wikilink")[王室后裔](../Page/王室后裔.md "wikilink")，巴伐利亚王位第一顺位[继承人](../Page/巴伐利亚王位继承顺序.md "wikilink")。

马克斯1937年出生于[慕尼黑](../Page/慕尼黑.md "wikilink")，[巴伐利亚王位继承人](../Page/巴伐利亚统治者列表.md "wikilink")[阿尔布雷希特与第一任妻子玛利亚](../Page/阿尔布雷希特_\(巴伐利亚\).md "wikilink")·德拉什科维奇·冯·特拉科什乾的次子，巴伐利亚末王[路德维希三世的曾孙](../Page/路德维希三世_\(巴伐利亚\).md "wikilink")。他出生时并没有得到“巴伐利亚王子”的头衔，因为他父母的婚姻不符合巴伐利亚王室法律，1941年他的祖父[鲁普雷希特修改了王位继承法](../Page/鲁普雷希特_\(巴伐利亚王储\).md "wikilink")，将此次婚姻合法化，他才得到“巴伐利亚王子”的称号。

1965年3月18日，马克斯被巴伐利亚的公爵路德维希·威廉
（1884年-1968年）收为养子。路德维希·威廉的祖先是[维特尔斯巴赫家族的幼支](../Page/维特尔斯巴赫家族.md "wikilink")，[普法尔茨](../Page/普法尔茨.md "wikilink")-茨魏布吕肯-比肯费尔德-格伦豪森支的后裔。同年11月5日路德维希·威廉去世，没有子嗣，他继位成为巴伐利亚的公爵。1967年马克斯与[瑞典贵族伊莉莎白](../Page/瑞典.md "wikilink")·道格拉斯女伯爵结婚，婚后有五个女儿：

  - 长女***[索非亚](../Page/索菲王储妃_\(列支敦斯登\).md "wikilink")***（*Sophie
    Elizabeth Marie
    Gabrielle*，1967年10月28日生），1993年与[列支敦斯登的](../Page/列支敦斯登.md "wikilink")[阿洛伊斯王储结婚](../Page/阿洛伊斯王储_\(列支敦斯登\).md "wikilink")。
  - 次女***玛利亚·卡罗琳娜***（*Marie Caroline Hedwig
    Eleonore*，1969年生），1991年与[符腾堡的菲利普公爵结婚](../Page/符腾堡.md "wikilink")。
  - 三女***海伦娜***（*Helene Eugenie Maria Donatha Mechthild*，1972年生）
  - 四女***伊莉莎白***（*Elisabeth Marie Christine
    Franziska*，1973年生），2004年与丹尼尔·特尔贝格尔结婚。
  - 五女***玛利亚·安娜***（*Maria Anna Henriette Gabrielle Julie*）

马克斯现与妻子居住在慕尼黑附近的维尔登瓦尔特城堡。

[Category:巴伐利亚王室后裔](../Category/巴伐利亚王室后裔.md "wikilink")
[Category:慕尼黑人](../Category/慕尼黑人.md "wikilink")