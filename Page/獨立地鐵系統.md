**獨立地鐵系統**（），簡稱**IND**或**ISS**，過去稱為「獨立市營地鐵系統」（ICOS）或「獨立市營捷運鐵路」\[1\]，是[紐約市的](../Page/紐約市.md "wikilink")[軌道交通鐵路系統](../Page/軌道交通.md "wikilink")，現在是[紐約地鐵一部分](../Page/紐約地鐵.md "wikilink")\[2\]。它興建的第一條路線是1932年[曼哈頓的](../Page/曼哈頓.md "wikilink")[IND第八大道線](../Page/IND第八大道線.md "wikilink")\[3\]。

作為三個組成紐約地鐵網絡的其中之一，IND原意是由市政府全權擁有和經營，而不是私人公司營運或聯合資助營運的[跨區捷運公司](../Page/跨區捷運公司.md "wikilink")（IRT）和[布魯克林-曼哈頓運輸股份有限公司](../Page/布魯克林-曼哈頓運輸股份有限公司.md "wikilink")（BMT）。IND網絡在1940年與這兩間公司的網絡合併\[4\]。

原初IND服務路線分別是現時[A線](../Page/紐約地鐵A線.md "wikilink")、[B線](../Page/紐約地鐵B線.md "wikilink")、[C線](../Page/紐約地鐵C線.md "wikilink")、[D線](../Page/紐約地鐵D線.md "wikilink")、[E線](../Page/紐約地鐵E線.md "wikilink")、[F線及](../Page/紐約地鐵F線.md "wikilink")[G線列車](../Page/紐約地鐵G線.md "wikilink")。另外，BMT的[M線](../Page/紐約地鐵M線.md "wikilink")、[N線](../Page/紐約地鐵N線.md "wikilink")、[Q線及](../Page/紐約地鐵Q線.md "wikilink")[R線現在也部分在IND軌道營運](../Page/紐約地鐵R線.md "wikilink")。[洛克威公園接駁線輔助](../Page/洛克威公園接駁線.md "wikilink")[A線列車服務](../Page/紐約地鐵A線.md "wikilink")。為了營運需要，IND和BMT路線和列車共同劃為\[5\]。

## 参考文献

[Category:紐約地鐵](../Category/紐約地鐵.md "wikilink")

1.
2.

3.
4.
5.