**從三位**為日本[品秩與](../Page/品秩.md "wikilink")[神階的一種](../Page/神階.md "wikilink")，位於[正三位之下](../Page/正三位.md "wikilink")[正四位](../Page/正四位.md "wikilink")（正四位上）之上。律令制度中曾任[參議或](../Page/參議.md "wikilink")**從三位**以上便可稱為[公卿](../Page/公卿.md "wikilink")，追贈時則稱為**贈從三位**。

## 解說

[律令制下](../Page/律令制.md "wikilink")**從三位**以上被稱為貴，也就是所謂上級貴族的位階。以[勳等來說就是](../Page/勳等.md "wikilink")[勳二等](../Page/勳二等.md "wikilink")，擁有[正四位上](../Page/正四位.md "wikilink")[參議或](../Page/參議.md "wikilink")**從三位**以上官位的公卿，又被稱為[上達部](../Page/上達部.md "wikilink")。

律令制度下，[正四位上](../Page/正四位.md "wikilink")[參議或](../Page/參議.md "wikilink")**從三位**以上者於姓氏後擁有朝臣稱呼，名諱之後則擁有[卿之敬稱](../Page/卿.md "wikilink")，而擔任過[大臣者的敬稱為公](../Page/大臣.md "wikilink")。[從四位以上而非公卿者](../Page/從四位.md "wikilink")，則在姓氏與名諱後皆接朝臣。另外，三位以上的公卿去世時，和皇族一樣使用薨去一詞，並稱呼為「三位殿」或「三位樣」。**從三位**以上者可以在自家設立[政所](../Page/政所.md "wikilink")，並雇請[家司管理家中事務](../Page/家司.md "wikilink")。

和從三位相當的官職為[大宰帥](../Page/大宰帥.md "wikilink")、[彈正尹](../Page/彈正尹.md "wikilink")、[中納言](../Page/中納言.md "wikilink")、左右[近衛大將等](../Page/近衛大將.md "wikilink")，實際上太宰帥通常由親王擔任，近衛大將通常由左大臣與右大臣兼任，所以通常**從三位**為左右[近衛中將](../Page/近衛中將.md "wikilink")。因此稱呼**從三位**的中將為三位中將。

[平安時代的](../Page/平安時代.md "wikilink")[平清盛為首次成為公卿一員的武士](../Page/平清盛.md "wikilink")，在此之前[源氏](../Page/源氏.md "wikilink")、[平氏最高都只有](../Page/平氏.md "wikilink")[正四位](../Page/正四位.md "wikilink")。隨著平家一門位列三位以上，當時[攝津源氏的](../Page/攝津源氏.md "wikilink")[源賴政也成為首位昇至](../Page/源賴政.md "wikilink")**從三位**的源氏。作為異例，世稱源三位（*げんざんみ*）。

[室町時代以後](../Page/室町時代.md "wikilink")、從三位為歷代[足利將軍家](../Page/足利將軍家.md "wikilink")[連枝或](../Page/連枝.md "wikilink")[鎌倉公方之位](../Page/鎌倉公方.md "wikilink")，另外[三管領筆頭](../Page/管領.md "wikilink")[斯波氏的](../Page/斯波氏.md "wikilink")[斯波義重](../Page/斯波義重.md "wikilink")、[斯波義敏也獲得此位](../Page/斯波義敏.md "wikilink")，作為管領而支配幕政的[畠山持國也升為從三位](../Page/畠山持國.md "wikilink")。另外[室町時代後期的](../Page/室町時代.md "wikilink")[赤松政則等](../Page/赤松政則.md "wikilink")，也是足利氏以外的大名也可獲得此位的例子。

[戰國時代初期至中期之後更是逐漸頻繁](../Page/戰國時代_\(日本\).md "wikilink")，如[大内義興](../Page/大内義興.md "wikilink")、[若狹國的](../Page/若狹國.md "wikilink")[武田元信](../Page/武田元信.md "wikilink")、[日向國的](../Page/日向國.md "wikilink")[伊東義祐都是例子](../Page/伊東義祐.md "wikilink")。不僅如此，事實上[大内義隆之後還昇至](../Page/大内義隆.md "wikilink")[從二位](../Page/從二位.md "wikilink")。最後昇至[右大臣的](../Page/右大臣.md "wikilink")[織田信長也曾經擁有此位](../Page/織田信長.md "wikilink")，其[嫡長子](../Page/嫡長子.md "wikilink")[織田信忠後來也成為從三位](../Page/織田信忠.md "wikilink")[左近衛中將](../Page/左近衛中將.md "wikilink")。

首位武家出身的[關白](../Page/關白.md "wikilink")[豐臣秀吉統一天下後](../Page/豐臣秀吉.md "wikilink")，其[一門或](../Page/一門.md "wikilink")[上杉景勝](../Page/上杉景勝.md "wikilink")、[毛利輝元等大名便獲得](../Page/毛利輝元.md "wikilink")**從三位**。

[江戸幕府時](../Page/江戸幕府.md "wikilink")[征夷大將軍的嫡孫](../Page/征夷大將軍.md "wikilink")、[御三家](../Page/德川御三家.md "wikilink")、[御三卿](../Page/御三卿.md "wikilink")、或以將軍女兒為正室的[加賀藩藩主皆獲得此位](../Page/加賀藩.md "wikilink")。

近代**從三位**在爵位上來說相當於[子爵](../Page/子爵.md "wikilink")，[軍人來說跟陸軍上將相當](../Page/軍人.md "wikilink")。今天則主要作為國會議員、各都道府縣知事或學者等擁有勳二等或相當功績者逝世後追贈之用。

## 參見

  - [從三品](../Page/從三品.md "wikilink")：東亞其他地區的相等品秩

## 外部連結

  - [位階令](https://web.archive.org/web/20080619092936/http://law.e-gov.go.jp/htmldata/T15/T15CO325.html)

[Category:日本古代官制](../Category/日本古代官制.md "wikilink")
[Category:日本貴族](../Category/日本貴族.md "wikilink")
[Category:位階](../Category/位階.md "wikilink")