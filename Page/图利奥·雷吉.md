**图利奥·雷吉**（，），意大利理论物理学家，都灵理工大学教授。他的主要贡献包括在[弦理论的前身](../Page/弦理论.md "wikilink")—对偶共振模型中的工作（雷吉轨道）和[广义相对论](../Page/广义相对论.md "wikilink")（）。

[R](../Page/category:意大利物理学家.md "wikilink")

[Category:都靈大學校友](../Category/都靈大學校友.md "wikilink")
[Category:普林斯顿高等研究院教职员](../Category/普林斯顿高等研究院教职员.md "wikilink")
[Category:丹尼·海涅曼数学物理奖获得者](../Category/丹尼·海涅曼数学物理奖获得者.md "wikilink")
[Category:ICTP狄拉克奖章获得者](../Category/ICTP狄拉克奖章获得者.md "wikilink")
[Category:阿尔伯特·爱因斯坦奖获得者](../Category/阿尔伯特·爱因斯坦奖获得者.md "wikilink")
[Category:波梅兰丘克奖获得者](../Category/波梅兰丘克奖获得者.md "wikilink")