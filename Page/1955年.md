<div style="float:right; border:1px; border-style:solid; padding:2px">

**请参看：**

  - [1955年电影](../Page/1955年电影.md "wikilink")
  - [1955年文学](../Page/1955年文学.md "wikilink")
  - [1955年音乐](../Page/1955年音乐.md "wikilink")
  - [1955年体育](../Page/1955年体育.md "wikilink")
  - [1955年电视](../Page/1955年电视.md "wikilink")

</div>

## 大事记

  - [畢闕博士](../Page/畢闕博士.md "wikilink")（Henry K.
    Beecher）提出了至今仍困擾著科學家的迷團：[安慰劑效應](../Page/安慰劑效應.md "wikilink")。
  - [東海大學成立](../Page/東海大學_\(台灣\).md "wikilink")，為台灣第一所私立大學。
  - [國立藝術學校成立](../Page/國立藝術學校.md "wikilink")，為[國立臺灣藝術大學的前身](../Page/國立臺灣藝術大學.md "wikilink")。設立[影劇](../Page/影劇.md "wikilink")、[國劇](../Page/國劇.md "wikilink")、[美術印刷三科](../Page/美術印刷.md "wikilink")。
  - [1月1日](../Page/1月1日.md "wikilink")——[光明日報編排方式改為由左至右橫排](../Page/光明日報.md "wikilink")。上海《每周广播电视报》创刊
  - [1月2日](../Page/1月2日.md "wikilink")——[南斯拉夫社会主义联邦共和国与](../Page/南斯拉夫社会主义联邦共和国.md "wikilink")[中华人民共和国建交](../Page/中华人民共和国.md "wikilink")。
  - [1月3日](../Page/1月3日.md "wikilink")——[中国足球协会成立](../Page/中国足球协会.md "wikilink")。
  - [1月15日](../Page/1月15日.md "wikilink")——中国启动代号为“02”的[核武器研制计划](../Page/核武器.md "wikilink")。
  - [1月18日](../Page/1月18日.md "wikilink")——[一江山戰役開始](../Page/一江山戰役.md "wikilink")。
  - [1月20日](../Page/1月20日.md "wikilink")——解放軍猛犯[一江山戰役](../Page/一江山戰役.md "wikilink")，守軍奮戰，全部壯烈。\[1\]
  - [1月29日](../Page/1月29日.md "wikilink")——[美國通過](../Page/美國.md "wikilink")[台灣決議案](../Page/台灣決議案.md "wikilink")。
  - [1月31日](../Page/1月31日.md "wikilink")——[肯尼亚著名的反抗英殖民主义的](../Page/肯尼亚.md "wikilink")[茅茅运动达到高潮](../Page/茅茅运动.md "wikilink")。
  - [1月31日](../Page/1月31日.md "wikilink")——中国[卫生部党组向](../Page/卫生部.md "wikilink")[中共中央报送](../Page/中共中央.md "wikilink")《[关于节制生育问题的报告](../Page/关于节制生育问题的报告.md "wikilink")》。
  - [2月](../Page/2月.md "wikilink")——[美國國務院宣稱](../Page/美國國務院.md "wikilink")：國際任何討論台灣問題之會議，倘無中華民國代表出席，美國決不參加。\[2\]
  - [2月6日](../Page/2月6日.md "wikilink")——[中華民國政府開始進行](../Page/中華民國.md "wikilink")[浙江](../Page/浙江.md "wikilink")[大陳島撤退](../Page/大陳島撤退.md "wikilink")。
  - [2月7日至](../Page/2月7日.md "wikilink")[2月10日](../Page/2月10日.md "wikilink")——[大陳島軍民轉移工作](../Page/大陳島.md "wikilink")，全部順利完成，[撤運來台義胞共](../Page/大陳島撤退.md "wikilink")1萬6487人。\[3\]
  - [2月16日](../Page/2月16日.md "wikilink")——中共中央[统战工作方针确定](../Page/统战.md "wikilink")。
  - [2月28日](../Page/2月28日.md "wikilink")——[台灣共和國臨時政府成立](../Page/台灣共和國.md "wikilink")。
  - [3月1日](../Page/3月1日.md "wikilink")——新版[人民币开始发行](../Page/人民币.md "wikilink")。
  - [4月至](../Page/4月.md "wikilink")[7月](../Page/7月.md "wikilink")——泰国[銮披汶·颂堪对欧美进行了一次访问](../Page/銮披汶·颂堪.md "wikilink")，回国后决心进行“新民主试验”
  - [4月11日](../Page/4月11日.md "wikilink")——“[喀什米爾公主號](../Page/喀什米爾公主號.md "wikilink")”事件。
  - [4月11日](../Page/4月11日.md "wikilink")——“[《科学家要求废止战争》](../Page/《科学家要求废止战争》.md "wikilink")”。
  - [4月18日](../Page/4月18日.md "wikilink")——召開[萬隆會議](../Page/萬隆會議.md "wikilink")。
  - [4月29日](../Page/4月29日.md "wikilink")——[西贡敌对派系发动](../Page/胡志明市.md "wikilink")[内战](../Page/内战.md "wikilink")。
  - [5月2日](../Page/5月2日.md "wikilink")——[西贡](../Page/胡志明市.md "wikilink")[政变失败](../Page/政变.md "wikilink")。
  - [5月5日](../Page/5月5日.md "wikilink")——[巴黎协定生效](../Page/巴黎协定.md "wikilink")。
  - [5月9日](../Page/5月9日.md "wikilink")——[西德加入](../Page/德意志联邦共和国.md "wikilink")[北大西洋公约组织](../Page/北大西洋公约组织.md "wikilink")。
  - [5月14日](../Page/5月14日.md "wikilink")——成立[华沙条约组织](../Page/华沙条约.md "wikilink")。
  - [5月26日](../Page/5月26日.md "wikilink")——[赫鲁晓夫访问](../Page/赫鲁晓夫.md "wikilink")[南斯拉夫社会主义联邦共和国](../Page/南斯拉夫社会主义联邦共和国.md "wikilink")。
  - [6月3日](../Page/6月3日.md "wikilink")——[法国同意](../Page/法国.md "wikilink")[突尼斯自治](../Page/突尼斯.md "wikilink")——[法国总理总理艾得加](../Page/法国.md "wikilink")·佛尔和[突尼斯总理本](../Page/突尼斯.md "wikilink")·阿玛签订核准书，保证[突尼斯的区内自治权](../Page/突尼斯.md "wikilink")。
  - [6月11日](../Page/6月11日.md "wikilink")——在[法國](../Page/法國.md "wikilink")[利曼舉行的](../Page/利曼.md "wikilink")[利曼24小時耐久賽中發生嚴重](../Page/利曼24小時耐久賽.md "wikilink")[意外造成](../Page/1955年利曼慘劇.md "wikilink")84死120傷，是歷史上傷亡最慘重的賽車意外。
  - [7月17日](../Page/7月17日.md "wikilink")——第一座[迪士尼主题公园](../Page/迪士尼.md "wikilink")[迪士尼乐园在](../Page/迪士尼乐园.md "wikilink")[美國](../Page/美國.md "wikilink")[加州正式對外開放](../Page/加州.md "wikilink")。
  - [8月20日](../Page/8月20日.md "wikilink")——[台灣發生](../Page/台灣.md "wikilink")[孫立人事件](../Page/孫立人事件.md "wikilink")，[孫立人被迫下台](../Page/孫立人.md "wikilink")，並被[軟禁](../Page/軟禁.md "wikilink")。
  - [8月27日](../Page/8月27日.md "wikilink")——第一版《[金氏世界紀錄大全](../Page/金氏世界紀錄大全.md "wikilink")》（[英語](../Page/英語.md "wikilink")：Guinness
    World Records）
  - [9月](../Page/9月.md "wikilink")——聯合國第十屆大會通過美國提案，本屆大會不考慮任何主張排除台灣代表或中共入會之提議。\[4\]
  - [9月8日](../Page/9月8日.md "wikilink")——上海警方逮捕[天主教上海教区主教](../Page/天主教上海教区.md "wikilink")[龚品梅](../Page/龚品梅.md "wikilink")、30多名神父及300多名教徒。
  - [9月27日](../Page/9月27日.md "wikilink")——[十大元帅](../Page/十大元帅.md "wikilink")：[朱德](../Page/朱德.md "wikilink")、[彭德怀等十人在](../Page/彭德怀.md "wikilink")[北京获颁十人](../Page/北京.md "wikilink")[中国人民解放军元帅军衔](../Page/中国人民解放军.md "wikilink")。
  - [9月30日](../Page/9月30日.md "wikilink")——[新疆维吾尔自治区成立](../Page/新疆维吾尔自治区.md "wikilink")。
  - [10月8日](../Page/10月8日.md "wikilink")——[錢學森坐上了](../Page/錢學森.md "wikilink")[美國總統輪船公司](../Page/美國.md "wikilink")（American
    President Lines）的克利夫蘭總統號（Pres.
    Cleveland）到達[香港折返](../Page/香港.md "wikilink")[中國](../Page/中國.md "wikilink")。
  - [10月26日](../Page/10月26日.md "wikilink")——[越南共和國成立](../Page/越南共和國.md "wikilink")。
  - [11月15日](../Page/11月15日.md "wikilink")——[日本的自由黨與民主黨合併](../Page/日本.md "wikilink")，組成了[自由民主黨](../Page/自由民主黨_\(日本\).md "wikilink")〔一般簡稱「自民黨」〕。

## 出生

  - [沙希杜尔阿拉姆](../Page/沙希杜尔阿拉姆.md "wikilink"), 孟加拉国摄影师。
  - [1月6日](../Page/1月6日.md "wikilink")——[路雲·雅堅遜](../Page/路雲·雅堅遜.md "wikilink"),英國喜劇演員
  - [1月28日](../Page/1月28日.md "wikilink")——[尼古拉·薩科齊](../Page/尼古拉·薩科齊.md "wikilink")，法国政治家。
  - [2月17日](../Page/2月17日.md "wikilink")——[蘇施黃](../Page/蘇施黃.md "wikilink")，香港主持。
  - [2月24日](../Page/2月24日.md "wikilink")——[史蒂夫·賈伯斯](../Page/史蒂夫·賈伯斯.md "wikilink")，[美國科技人物](../Page/美國.md "wikilink")、企业家，[苹果电脑公司创办人之一](../Page/苹果电脑公司.md "wikilink")。
  - [3月2日](../Page/3月2日.md "wikilink")——[麻原彰晃](../Page/麻原彰晃.md "wikilink")，[日本宗教人物](../Page/日本.md "wikilink")。（2018年被处决）
  - [3月17日](../Page/3月17日.md "wikilink")——[殷琪](../Page/殷琪.md "wikilink")，[台灣企業家](../Page/台灣.md "wikilink")。
  - [4月2日](../Page/4月2日.md "wikilink")——[玛哈·扎克里·诗琳通](../Page/玛哈·扎克里·诗琳通.md "wikilink")，[泰国公主](../Page/泰国.md "wikilink")。
  - [4月5日](../Page/4月5日.md "wikilink")——[鸟山明](../Page/鸟山明.md "wikilink")，日本[漫画家](../Page/漫画家.md "wikilink")。
  - [4月6日](../Page/4月6日.md "wikilink")——[亨利大公](../Page/亨利_\(盧森堡大公\).md "wikilink")，[盧森堡大公](../Page/盧森堡大公.md "wikilink")。
  - [4月22日](../Page/4月22日.md "wikilink")——[杜琪峰](../Page/杜琪峰.md "wikilink")，[香港電影導演](../Page/香港電影.md "wikilink")。
  - [4月26日](../Page/4月26日.md "wikilink")——[陳道明](../Page/陳道明.md "wikilink")，[中國電影](../Page/中國電影.md "wikilink")、電視演員。
  - [5月7日](../Page/5月7日.md "wikilink")——[向陽](../Page/向陽.md "wikilink")，[台灣詩人](../Page/台灣.md "wikilink")、作家、學者。
  - [5月13日](../Page/5月13日.md "wikilink")——[陳明文](../Page/陳明文.md "wikilink")，[台灣政治人物](../Page/台灣.md "wikilink")。
  - [5月15日](../Page/5月15日.md "wikilink")——[穆罕默德·布拉米](../Page/穆罕默德·布拉米.md "wikilink")，突尼西亞政治人物。
  - [5月17日](../Page/5月17日.md "wikilink")——[孫泳恩](../Page/孫泳恩.md "wikilink")，首屆[香港小姐](../Page/香港小姐.md "wikilink")[冠軍](../Page/冠軍.md "wikilink")。
  - [5月18日](../Page/5月18日.md "wikilink")——[周潤發](../Page/周潤發.md "wikilink")，[香港男演員](../Page/香港.md "wikilink")。
  - [6月28日](../Page/6月28日.md "wikilink")——[李逸洋](../Page/李逸洋.md "wikilink")，[台灣政治人物](../Page/台灣.md "wikilink")。
  - [7月1日](../Page/7月1日.md "wikilink")——[明石家秋刀魚](../Page/明石家秋刀魚.md "wikilink")，[日本演員](../Page/日本.md "wikilink")、主持人。
  - [7月1日](../Page/7月1日.md "wikilink")——[韓馬利](../Page/韓馬利.md "wikilink"),
    [香港](../Page/香港.md "wikilink")[TVB女藝員](../Page/TVB.md "wikilink")
  - [7月17日](../Page/7月17日.md "wikilink")——[費玉清](../Page/費玉清.md "wikilink")，[台灣歌手](../Page/台灣.md "wikilink")、主持人。
  - [9月16日](../Page/9月16日.md "wikilink")——[羅曼菲](../Page/羅曼菲.md "wikilink")，[台灣舞蹈家](../Page/台灣.md "wikilink")（[2006年逝世](../Page/2006年.md "wikilink")）。
  - [9月30日](../Page/9月30日.md "wikilink")——[葉家寶](../Page/葉家寶.md "wikilink")，前[亞視執行董事](../Page/亞洲電視.md "wikilink")。
  - [10月7日](../Page/10月7日.md "wikilink")——[马友友](../Page/马友友.md "wikilink")，[美籍華裔](../Page/美籍華人.md "wikilink")[大提琴家](../Page/大提琴.md "wikilink")。
  - [12月22日](../Page/12月22日.md "wikilink")——[托馬斯·聚德霍夫](../Page/托馬斯·聚德霍夫.md "wikilink")，德國生物化學家
  - [10月28日](../Page/10月28日.md "wikilink")——[比尔·盖茨](../Page/比尔·盖茨.md "wikilink")，[美國科技人物](../Page/美國.md "wikilink")、企业家、[微軟](../Page/微軟.md "wikilink")（）创办人之一。
  - [12月28日](../Page/12月28日.md "wikilink")——[劉曉波](../Page/劉曉波.md "wikilink")，[中國作家](../Page/中國.md "wikilink")，[持不同政見者](../Page/持不同政見者.md "wikilink")，[2010年諾貝爾和平獎得主](../Page/2010年諾貝爾和平獎.md "wikilink")。(2017年逝世)
  - [12月30日](../Page/12月30日.md "wikilink")——[李國修](../Page/李國修.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[舞台劇](../Page/舞台劇.md "wikilink")[導演](../Page/導演.md "wikilink")。
  - [祝维沙](../Page/祝维沙.md "wikilink")，民营企业家。

## 逝世

  - [1月23日](../Page/1月23日.md "wikilink")——[余嘉錫](../Page/余嘉錫.md "wikilink")
    (季豫)，([中央研究院院士](../Page/中央研究院院士.md "wikilink"))。（[1884年出生](../Page/1884年.md "wikilink")）[中央研究院](https://academicians.sinica.edu.tw/index.php?func=1-D)
  - [2月9日](../Page/2月9日.md "wikilink")——[张澜](../Page/张澜.md "wikilink")，[中国民主同盟主席](../Page/中国民主同盟.md "wikilink")。
  - [3月11日](../Page/3月11日.md "wikilink")——[弗莱明](../Page/弗莱明.md "wikilink")，英国[细菌学家](../Page/细菌学家.md "wikilink")（[1881年出生](../Page/1881年.md "wikilink")）。
  - [3月12日](../Page/3月12日.md "wikilink")——[查利·帕克](../Page/查利·帕克.md "wikilink")，“[辟波普](../Page/比波普.md "wikilink")”爵士乐演奏家。
  - [4月1日](../Page/4月1日.md "wikilink")——[林徽因](../Page/林徽因.md "wikilink")，[中國建築師](../Page/中國.md "wikilink")、詩人（[1904年出生](../Page/1904年.md "wikilink")）。
  - [4月18日](../Page/4月18日.md "wikilink")——[爱因斯坦](../Page/爱因斯坦.md "wikilink")，[美國知名物理學家](../Page/美國.md "wikilink")，[相對論的创立者](../Page/相對論.md "wikilink")（[1879年出生](../Page/1879年.md "wikilink")）。
  - [5月25日](../Page/5月25日.md "wikilink")——[侯志律](../Page/侯志律.md "wikilink")，[香港正按察司](../Page/香港正按察司.md "wikilink")（[1899年出生](../Page/1899年.md "wikilink")）。
  - [9月17日](../Page/9月17日.md "wikilink")——[林日高](../Page/林日高.md "wikilink")，[台灣](../Page/台灣.md "wikilink")[白色恐怖受難者](../Page/白色恐怖.md "wikilink")（[1904年出生](../Page/1904年.md "wikilink")）。
  - [11月4日](../Page/11月4日.md "wikilink")——[賽·揚](../Page/賽·揚.md "wikilink")，[美國](../Page/美國.md "wikilink")[棒球選手](../Page/棒球.md "wikilink")，[大聯盟史上最多勝](../Page/大聯盟.md "wikilink")[投手](../Page/投手.md "wikilink")（[1867年出生](../Page/1867年.md "wikilink")）。

## [诺贝尔奖](../Page/诺贝尔奖.md "wikilink")

  - [物理](../Page/诺贝尔物理学奖.md "wikilink")：[威利斯·蘭姆](../Page/威利斯·蘭姆.md "wikilink")、[波利卡普·庫施](../Page/波利卡普·庫施.md "wikilink")。
  - [化学](../Page/诺贝尔化学奖.md "wikilink")：[文森特·迪維尼奧](../Page/文森特·迪維尼奧.md "wikilink")
  - [生理和医学](../Page/诺贝尔医学奖.md "wikilink")：[胡戈·特奧雷爾](../Page/胡戈·特奧雷爾.md "wikilink")
  - [文学](../Page/诺贝尔文学奖.md "wikilink")：
    [赫爾多爾·奇里揚·拉克斯內斯](../Page/赫爾多爾·奇里揚·拉克斯內斯.md "wikilink")，[冰島作家](../Page/冰島.md "wikilink")。
  - [和平](../Page/诺贝尔和平奖.md "wikilink")：未頒獎

## [奥斯卡金像奖](../Page/奥斯卡金像奖.md "wikilink")

（第28届，[1956年颁发](../Page/1956年.md "wikilink")）

  - [奥斯卡最佳影片奖](../Page/奥斯卡最佳影片奖.md "wikilink")——《[马蒂](../Page/君子好逑_\(美國電影\).md "wikilink")》（Marty）
  - [奥斯卡最佳导演奖](../Page/奥斯卡最佳导演奖.md "wikilink")——[德尔伯特·曼](../Page/德尔伯特·曼.md "wikilink")（Delbert
    Mann）《马蒂》
  - [奥斯卡最佳男主角奖](../Page/奥斯卡最佳男主角奖.md "wikilink")——[欧内斯特·博格宁](../Page/欧内斯特·博格宁.md "wikilink")（Ernest
    Borgnine）《马蒂》
  - [奥斯卡最佳女主角奖](../Page/奥斯卡最佳女主角奖.md "wikilink")——[安娜·马格纳尼](../Page/安娜·马格纳尼.md "wikilink")（Anna
    Magnani）《[玫瑰纹身](../Page/玫瑰纹身.md "wikilink")》
  - [奥斯卡最佳男配角奖](../Page/奥斯卡最佳男配角奖.md "wikilink")——[杰克·莱蒙](../Page/杰克·莱蒙.md "wikilink")（Jack
    Lemmon）《[罗伯茨先生](../Page/罗伯茨先生.md "wikilink")》
  - [奥斯卡最佳女配角奖](../Page/奥斯卡最佳女配角奖.md "wikilink")——[乔·范·弗利特](../Page/乔·范·弗利特.md "wikilink")（Jo
    Van Fleet）《[伊甸之东](../Page/伊甸之东.md "wikilink")》

（其他奖项参见[奥斯卡金像奖获奖名单](../Page/奥斯卡金像奖获奖名单.md "wikilink")）

## 參考文獻

[\*](../Category/1955年.md "wikilink")
[5年](../Category/1950年代.md "wikilink")
[5](../Category/20世纪各年.md "wikilink")

1.  [陳-{布}-雷等編著](../Page/陳布雷.md "wikilink")：《蔣介石先生年表》，台北：[傳記文學出版社](../Page/傳記文學.md "wikilink")，1978年6月1日

2.
3.
4.