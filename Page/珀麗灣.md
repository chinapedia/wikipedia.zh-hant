[Park_Island_Aerial.jpg](https://zh.wikipedia.org/wiki/File:Park_Island_Aerial.jpg "fig:Park_Island_Aerial.jpg")
[Logo_parkisland.jpg](https://zh.wikipedia.org/wiki/File:Logo_parkisland.jpg "fig:Logo_parkisland.jpg")
[Park_Island_Landscape_area_2011.jpg](https://zh.wikipedia.org/wiki/File:Park_Island_Landscape_area_2011.jpg "fig:Park_Island_Landscape_area_2011.jpg")
[Park_Island_at_night_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:Park_Island_at_night_\(Hong_Kong\).jpg "fig:Park_Island_at_night_(Hong_Kong).jpg")
[Park_Island_03.jpg](https://zh.wikipedia.org/wiki/File:Park_Island_03.jpg "fig:Park_Island_03.jpg")
[Park_Island_04.jpg](https://zh.wikipedia.org/wiki/File:Park_Island_04.jpg "fig:Park_Island_04.jpg")
[AnaCapri,_Park_Island_(Hong_Kong).jpg](https://zh.wikipedia.org/wiki/File:AnaCapri,_Park_Island_\(Hong_Kong\).jpg "fig:AnaCapri,_Park_Island_(Hong_Kong).jpg")

**珀麗灣**（**Park
Island**）是[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")[荃灣區](../Page/荃灣區.md "wikilink")[馬灣的大型私人屋苑項目](../Page/馬灣.md "wikilink")，由發展商[新鴻基地產於](../Page/新鴻基地產.md "wikilink")2002年至2011年分階段發展，由關善明建築師事務所設計。該屋苑參照[大嶼山](../Page/大嶼山.md "wikilink")[愉景灣發展模式](../Page/愉景灣.md "wikilink")，一向並不容許私家車及電單車登島，故與香港市區有一定的隔離。初期僅容許接駁巴士及屋苑專車行駛，於2012年起逐步放寬市區的士於馬灣上行駛。於2015年至今，只有市區的士24小時於馬灣指定區域行駛，貨車則可於早上10時至下午4時行駛。\[1\]

[香港無綫電視戲集](../Page/香港無綫電視.md "wikilink")《[最美麗的第七天](../Page/最美麗的第七天.md "wikilink")》曾於珀麗灣內拍攝，並在2008年於[翡翠台播放](../Page/翡翠台.md "wikilink")。

## 屋苑資料

  - 地址：馬灣珀麗路8號
  - 住宅座數：32座
      - 第1期：第12-13、15-18座
      - 第2期：第2-3、5-11座
      - 第3期（Oceanfront）：第1、19-23及25座
      - 第5A及5B期（Oceancrest）：第26-31座
      - 第6期（海珏）（AnaCapri）：第32-35座

<!-- end list -->

  - 入伙年份：
      - 第1及2期：2002年12月11日（藍色會所）
      - 第3期（Oceanfront）：2004年7月20日（綠色會所）
      - 第5A及5B期（Oceancrest）：2007年4月30日（天色會所）
      - 第6期（海珏）：2011年3月
          - （括號為會所電話及開張日期）
  - 單位總數：5,281個

## 業主委員會

  - 第一屆業主委員會於2003年7月26日成立，首任業主委員會主席為曾文典先生，而翌年主席為陳嘉敏小姐
  - 第二屆業主委員會主席為周國紹先生，2007年6月21日起由倫智偉先生接任（[1](http://parkisland.no-ip.org/viewthread.php?tid=187)）
  - 第三屆業主委員會首年主席為蕭劉藹宜女士，而翌年主席為曾文典先生
  - 第四屆業主委員會首年主席為黃正邦先生，而翌年主席為林惠文先生
  - 第五屆業主委員會主席為黎錦浩先生

## 私人泳灘藍鯨灣

新鴻基於2002年時以5,000萬購買位於[大嶼山](../Page/大嶼山.md "wikilink")[竹篙灣附近的沙灘花坪灣](../Page/竹篙灣.md "wikilink")，將之私有化並命名為「藍鯨灣」。發展商更動用了約
1,000萬元作為美化沙灘及設施，改建為馬爾代夫式休閒度假勝地，珀麗灣的住客可乘搭豪華遊艇「藍鯨號」，抵達藍鯨灣碼頭並享用泳灘上的設施，包括燒烤、釣魚、野戰遊戲、沙灘排球、野外露營及私人派對等。
自2008年起，「藍鯨灣」已荒廢一段時間，到了2011年已成為一個廢墟。

## 附近設施

  - [珀麗灣商場](../Page/珀麗灣商場.md "wikilink")
  - [珀麗灣碼頭](../Page/珀麗灣碼頭.md "wikilink")
  - [東灣沙灘](../Page/東灣_\(馬灣\).md "wikilink")
  - [馬灣公園](../Page/馬灣公園.md "wikilink")

<File:Park> Island 01.jpg|珀麗灣一期A <File:Park> Island 02.jpg|珀麗灣一期B及二期
<File:Park> Island Phase 5 Swimming Pool 2011.jpg|第5A及5B期游泳池 <File:Park>
Island Pier 2 HK Central.jpg|珀麗灣及東灣 <File:HK> Ma Wan Park Island n
HKCCCC Kei Wai School.JPG|珀麗灣[中華基督教會基慧小學
(馬灣)](../Page/中華基督教會基慧小學_\(馬灣\).md "wikilink")
<File:LL5808> NR332 05-10-2017.jpg|居民巴士服務 <File:Park> Island 5 Tsuen Wan
to Park Island 10-07-2018.jpg|渡輪服務

## 事件

2002年9月21日，在馬灣珀麗灣地盤工作及居住的市民懷疑感染[登革熱病](../Page/登革熱.md "wikilink")，須抽血化驗，[衛生署相信珀麗灣地盤為致病源頭](../Page/衛生署.md "wikilink")。職員曾到地盤進行流行病學調查，訪問了244名工人，其中2名年齡分別28及38歲的工人，9月初曾出現登革熱病徵。食環署表示，該地盤處處積水及衛生環境欠佳，署方已先後在同年4月及9月向地盤發出兩次警告及一張告票。\[2\]

發展商[新鴻基地產表示無法證實患者是因為在馬灣島上工作而感染](../Page/新鴻基地產.md "wikilink")，不過配合政府行動，連日派出100多名員工到地盤滅蚊，清理積水。\[3\]

## 交通

客運公司在2012年削減前往荃灣的渡輪服務。

2018年6月，珀麗灣客運向運輸署申請削減平日上午10時至下午4時來往珀麗灣及中環的6班渡輪班次；同時，另外2條巴士線亦會削減班次及加價，其中加幅最高為25%。有關安排引起居民不滿，認為令居民生活更加不便。6月24日，約10名馬灣居民在馬灣游水抗議，高舉「反對CUT船」標語。\[4\]7月8日，近千名居民在屋苑有蓋廣場舉行集會，抗議新鴻基附屬的客運公司削減渡輪及巴士班次並加價。珀麗灣業主委員會副主席黃杏雲批評發展商違背當年的承諾，包括至今仍未興建馬灣公園二期，規劃失當的責任不應由居民承擔。\[5\]

  - [珀麗灣客運](../Page/珀麗灣客運.md "wikilink")

### 巴士路線

  - [巴士服務班期表](http://www.pitcl.com.hk/chi/html/bus.htm)

### 渡輪航線

  - 珀麗灣←→[荃灣碼頭](../Page/荃灣碼頭.md "wikilink")（航線已於2012年12月14日停辦\[6\]），但再於2013年6月8日重投作有限度服務。而在停辦日起[**<span style="color: red;">市區的士</span>**擴展至](../Page/香港的士.md "wikilink")24小時均可駛入馬灣島。
  - 珀麗灣←→[中環碼頭](../Page/中環碼頭.md "wikilink")
  - [渡輪服務班期表](http://www.mawanpark.com/chn/transportation.jsp)

## 外部連結

  - [珀麗灣官方網站](http://www.parkisland.com.hk)
  - [馬灣社群網 - MaWan
    Community](https://archive.is/20130415140654/http://www.parkisland.org/)
  - [海珏官方網站](http://www.anacapri.com.hk)

## 參考

<references/>

[Category:新鴻基地產物業](../Category/新鴻基地產物業.md "wikilink")
[Category:荃灣區私人屋苑](../Category/荃灣區私人屋苑.md "wikilink")
[Category:馬灣](../Category/馬灣.md "wikilink")

1.
2.
3.
4.
5.
6.  [珀麗灣至荃灣渡輪將停辦](http://hk.news.yahoo.com/珀麗灣至荃灣渡輪將停辦-220948414.html)
    《星島日報》