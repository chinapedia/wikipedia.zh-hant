**埃德蒙德·卡普拉尼**（****，）是一位[阿尔巴尼亚足球运动员](../Page/阿尔巴尼亚.md "wikilink")，场上位置为前锋。身高1米84，双脚皆擅长使用。头球是其强项，尽管身形彪悍，但一对一自信稍显不足。

## 生平

### 球會

卡普拉尼初期在阿尔巴尼亚国内联赛效力，自2004年起为[卡尔斯鲁厄队效力](../Page/卡尔斯鲁厄体育俱乐部.md "wikilink")。2009年以自由身加盟[德乙的](../Page/德乙.md "wikilink")[奧格斯堡](../Page/奥格斯堡足球俱乐部.md "wikilink")，簽約到2011年。上半季僅獲派在兩場聯賽及一場[德國盃上陣](../Page/德國盃.md "wikilink")，於2010年1月7日被外借到另一支[德乙球隊](../Page/德乙.md "wikilink")[科布倫斯直到球季結束](../Page/TuS科布倫斯.md "wikilink")。

### 國家隊

此外卡普拉尼还曾为[阿尔巴尼亚国家足球队出场](../Page/阿尔巴尼亚国家足球队.md "wikilink")25次，有6粒入球。

## 外部連結

  - [Edmond Kapllani's official website](http://www.kapllani.de)
  - [Edmond Kapllani's profile at official KSC
    website](https://web.archive.org/web/20071231145323/http://www.ksc.de/profis/team/0607/edmond-kapllani.html)

  - [Weltfussball.de provides profile and stats for Edmond
    Kapllani](https://web.archive.org/web/20080517215103/http://www.weltfussball.de/spieler_profil.php?id=22841)

  - [Career stats for Edmond Kapllani at
    fussballdaten.de](http://fussballdaten.de/spieler/kapllaniedmond/)

[Category:阿尔巴尼亚足球运动员](../Category/阿尔巴尼亚足球运动员.md "wikilink")
[Category:卡爾斯魯厄球員](../Category/卡爾斯魯厄球員.md "wikilink")
[Category:奧格斯堡球員](../Category/奧格斯堡球員.md "wikilink")
[Category:科布倫斯球員](../Category/科布倫斯球員.md "wikilink")
[Category:德甲球員](../Category/德甲球員.md "wikilink")
[Category:德國外籍足球運動員](../Category/德國外籍足球運動員.md "wikilink")