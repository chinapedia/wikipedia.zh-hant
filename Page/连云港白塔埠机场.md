**连云港白塔埠机场**（），位于[中华人民共和国](../Page/中华人民共和国.md "wikilink")[江苏省](../Page/江苏省.md "wikilink")[连云港市西部](../Page/连云港市.md "wikilink")25公里，并和[徐连](../Page/徐连高速公路.md "wikilink")、[宁连等高速公路连接](../Page/宁连高速公路.md "wikilink")。

## 历史

[南京军区空军于](../Page/南京军区空军.md "wikilink")1955年在[东海县](../Page/东海县.md "wikilink")[白塔埠镇征集土地](../Page/白塔埠镇.md "wikilink")，建设二级永备机场。1956年动工，1957年竣工。1984年，[国务院确定连云港市为](../Page/中华人民共和国国务院.md "wikilink")[沿海开放城市](../Page/沿海开放城市.md "wikilink")。4月，[江苏省人民政府向国务院](../Page/江苏省人民政府.md "wikilink")、[中央军事委员会呈送](../Page/中央军事委员会.md "wikilink")《关于要求在连云港市开辟民用航线的请求》。1985年3月16日，上海至连云港航线试飞成功，连云港白塔埠机场于26日正式开通民航班机。同年7月起，[中国民航局进行第一期工程建设](../Page/中国民航局.md "wikilink")\[1\]。当年运用旅客8453人次。次年达到12970人次\[2\]。

## 机场设施

飞行跑道长2500米、宽50米，[机场飞行区等级为](../Page/机场飞行区等级.md "wikilink")4D。

## 航空公司與航点

2018-2019冬春航季

## 相关条目

  - [中国大陆机场列表](../Page/中国大陆机场列表.md "wikilink")

## 參考資料

## 外部链接

  -
  - [连云港白塔埠机场](http://www.lygairport.com/)

[Category:江苏机场](../Category/江苏机场.md "wikilink")
[Category:连云港交通](../Category/连云港交通.md "wikilink")
[Category:连云港建筑物](../Category/连云港建筑物.md "wikilink")
[Category:1957年完工建築物](../Category/1957年完工建築物.md "wikilink")

1.
2.