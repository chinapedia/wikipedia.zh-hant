**E線第八大道慢車**（）是[紐約地鐵](../Page/紐約地鐵.md "wikilink")的[地鐵系統](../Page/地鐵.md "wikilink")。由於該線使用[IND第八大道線通過](../Page/IND第八大道線.md "wikilink")[曼哈頓主要地區](../Page/曼哈頓.md "wikilink")，因此其路線徽號為藍色\[1\]。

E線在任何時候都營運，來往的[牙買加中心-帕森斯/射手和](../Page/牙買加中心-帕森斯/射手車站_\(射手大道線\).md "wikilink")[曼哈頓下城的](../Page/曼哈頓下城.md "wikilink")[錢伯斯街-世界貿易中心](../Page/錢伯斯街-世界貿易中心／公園廣場車站.md "wikilink")，有限度服務以[179街為總站](../Page/牙買加-179街車站_\(IND皇后林蔭路線\).md "wikilink")而不是牙買加中心。日間列車在皇后區以快車營運，在曼哈頓以慢車營運，深夜列車停靠全線所有車站。

早期，E線列車沿[羅格斯街隧道和](../Page/羅格斯街隧道.md "wikilink")[IND卡爾弗線前往](../Page/IND卡爾弗線.md "wikilink")[布魯克林](../Page/布魯克林.md "wikilink")，但這營運模式在1940年代就已經中止了。直至1976年，此線都是途經[IND福爾頓街線和](../Page/IND福爾頓街線.md "wikilink")[IND洛克威線前往布魯克林和皇后區](../Page/IND洛克威線.md "wikilink")。

## 歷史

<div class="thumb tright" style="width:auto;">

<table>
<tbody>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><div class="thumbcaption">
<p><a href="../Page/R1_(紐約地鐵車輛).md" title="wikilink">R1</a><a href="../Page/方向幕.md" title="wikilink">方向幕</a></p>
</div></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

</div>

<div class="thumb tright" style="width:auto;">

<table>
<tbody>
<tr class="odd">
<td></td>
<td></td>
</tr>
<tr class="even">
<td><div class="thumbcaption">
<p>1967-1979年使用（圓形）</p>
</div></td>
<td></td>
</tr>
</tbody>
</table>

</div>

1933年8月19日[皇后大道線通車](../Page/皇后大道線.md "wikilink")，E線開始羅斯福大道-傑克森高地至欽伯斯街的服務。

1936年1月1日[IND第六大道線第二大道車站以北路段通車](../Page/IND第六大道線.md "wikilink")，E線行駛此線西四街車站以南路線。三個月後通至傑里街-區公所，E線取代線駛至教堂大道車站。

同年12月31日及1937年4月24日皇后大道線分別擴展至聯合公路-克猶花園車站、169街車站。

1940年12月15日開始運行第六大道全線取代線列車，訖站為百老匯-拉斐特街車站。

1950年12月10日，將終點站改至剛開幕的牙買加-179街車站，開行皇后廣場車站至七十一大道車站的快車。

1956年於尖峰改為慢車，延駛至尤克利德大道車站。

1963年於[布魯克林內改為快車](../Page/布魯克林.md "wikilink")，延駛至[洛克威區](../Page/洛克威區.md "wikilink")。

1973年1月2日於布魯克林內改行慢車，駛至[洛克威公園-臨海116街](../Page/洛克威公園-臨海116街.md "wikilink")。

1976年8月27日取消布魯克林路段，訖站改至世貿中心。

1988年12月11日[IND射手大道線通車](../Page/IND射手大道線.md "wikilink")，E線改走此支線至牙買加中心-帕森斯/射手，並於週末不停七十五大道車站及布里雅塢-凡維克大道車站。同時開行線至牙買加-179街車站，但不久後便取消。

1997年E線於皇后區行駛凌晨慢車。

2001年受到[911事件事件影響](../Page/911事件.md "wikilink")，E線改行非凌晨慢車，經福爾頓街線至尤克利德大道車站以取代暫時停駛的線服務。9月21日線復駛，E線改行至運河街車站。同年12月16日[63街隧道至皇后大道線的線路完工通車](../Page/63街隧道.md "wikilink")，線列車走此線駛至牙買加-179街車站。

## 路線

### 服務形式

以下表格顯示E線所使用路線，特定時段在有陰影的格的路段內營運：\[2\]

| 路線                                                           | 起點                                                             | 終點                                                      | 軌道 | 時段    |
| ------------------------------------------------------------ | -------------------------------------------------------------- | ------------------------------------------------------- | -- | ----- |
| 繁忙時段                                                         | 平日                                                             | 晚上、週末                                                   | 深夜 |       |
| [IND射手大道線](../Page/IND射手大道線.md "wikilink")（全線）               | [牙買加中心-帕森斯／射手](../Page/牙買加中心-帕森斯／射手車站_\(射手大道線\).md "wikilink") | [牙買加-凡維克](../Page/牙買加-凡維克車站_\(IND射手大道線\).md "wikilink") | 全部 | 大部分列車 |
| [IND皇后林蔭路線](../Page/IND皇后林蔭路線.md "wikilink")（全線）             | [牙買加-179街](../Page/牙買加-179街車站_\(IND皇后林蔭路線\).md "wikilink")     | [蘇特芬林蔭路](../Page/蘇特芬林蔭路車站_\(IND皇后林蔭路線\).md "wikilink")  | 快車 | 有限度服務 |
| [布里亞伍德](../Page/布里亞伍德車站_\(IND皇后林蔭路線\).md "wikilink")         | [75大道](../Page/75大道車站_\(IND皇后林蔭路線\).md "wikilink")             |                                                         |    |       |
| 慢車                                                           |                                                                |                                                         |    |       |
| [森林小丘-71大道](../Page/森林小丘-71大道車站_\(IND皇后林蔭路線\).md "wikilink") | [皇后廣場](../Page/皇后廣場車站_\(IND皇后林蔭路線\).md "wikilink")             | 快車                                                      |    |       |
| 慢車                                                           |                                                                |                                                         |    |       |
| [法庭廣場-23街](../Page/法庭廣場-23街車站_\(IND皇后林蔭路線\).md "wikilink")   | [第七大道](../Page/第七大道車站_\(IND皇后林蔭路線\).md "wikilink")             | 全部                                                      |    |       |
| [IND第八大道線](../Page/IND第八大道線.md "wikilink")                   | [50街](../Page/50街車站_\(IND第八大道線\).md "wikilink")                | [世界貿易中心](../Page/世界貿易中心車站_\(IND第八大道線\).md "wikilink")   | 慢車 |       |

四班E線列車在早上繁忙時段從179街開出\[3\]，三班E線列車在黃昏繁忙時段從179街開出，黃昏時段有四班E線列車以179街為總站\[4\]。

### 車站

更詳細的車站列表參見上方列出的路線。

<table style="width:184%;">
<colgroup>
<col style="width: 30%" />
<col style="width: 30%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 30%" />
<col style="width: 36%" />
<col style="width: 30%" />
</colgroup>
<thead>
<tr class="header">
<th><p><a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-E.svg" title="fig:NYCS-bull-trans-E.svg">NYCS-bull-trans-E.svg</a><br />
</p></th>
<th><p><a href="https://zh.wikipedia.org/wiki/File:NYCS-bull-trans-E.svg" title="fig:NYCS-bull-trans-E.svg">NYCS-bull-trans-E.svg</a><br />
</p></th>
<th><p>中文站名</p></th>
<th><p>英文站名</p></th>
<th></th>
<th><p>轉乘</p></th>
<th><p>連接/備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong><a href="../Page/皇后區.md" title="wikilink">皇后區</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/IND皇后林蔭路線.md" title="wikilink">山側大道支線</a></strong>（只限繁忙時段有限度服務）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>align=center rowspan=2 </p></td>
<td></td>
<td><p><a href="../Page/牙買加-179街車站_(IND皇后林蔭路線).md" title="wikilink">牙買加-179街</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>線巴士往<a href="../Page/約翰·甘迺迪國際機場.md" title="wikilink">JFK機場</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/帕森斯林蔭路車站_(IND皇后林蔭路線).md" title="wikilink">帕森斯林蔭路</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/射手大道線.md" title="wikilink">射手大道支線</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>align=center rowspan=3 </p></td>
<td><p><a href="../Page/牙買加中心-帕森斯／射手車站_(射手大道線).md" title="wikilink">牙買加中心-帕森斯／射手</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/蘇特芬林蔭路-射手大道-JFK機場車站_(射手大道線).md" title="wikilink">蘇特芬林蔭路-射手大道-JFK機場</a> </p></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="../Page/長島鐵路.md" title="wikilink">長島鐵路</a>，<br />
<a href="../Page/AirTrain_JFK.md" title="wikilink">AirTrain JFK</a><br />
</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/牙買加-凡維克車站_(IND射手大道線).md" title="wikilink">牙買加-凡維克</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/IND皇后林蔭路線.md" title="wikilink">皇后林蔭路線</a></strong>（由<a href="../Page/牙買加-179街車站_(IND皇后林蔭路線).md" title="wikilink">牙買加-179街和</a><a href="../Page/牙買加中心-帕森斯/射手車站_(射手大道線).md" title="wikilink">牙買加中心開出列車匯合</a>）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>|</p></td>
<td><p><a href="../Page/布里亞伍德車站_(IND皇后林蔭路線).md" title="wikilink">布里亞伍德</a></p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Aiga_elevator.svg" title="fig:Aiga_elevator.svg">Aiga_elevator.svg</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/克猶花園-聯合公路車站_(IND皇后林蔭路線).md" title="wikilink">克猶花園-聯合公路</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p>線巴士往<a href="../Page/約翰·甘迺迪國際機場.md" title="wikilink">JFK機場</a><br />
部分北行繁忙時段列車以此站為總站</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>|</p></td>
<td><p><a href="../Page/75大道車站_(IND皇后林蔭路線).md" title="wikilink">75大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/森林小丘-71大道車站_(IND皇后林蔭路線).md" title="wikilink">森林小丘-71大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p><a href="../Page/長島鐵路.md" title="wikilink">長島鐵路</a>，</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>|</p></td>
<td><p><a href="../Page/67大道車站_(IND皇后林蔭路線).md" title="wikilink">67大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>|</p></td>
<td><p><a href="../Page/63徑-雷哥公園車站_(IND皇后林蔭路線).md" title="wikilink">63徑-雷哥公園</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>|</p></td>
<td><p><a href="../Page/伍德哈文林蔭路車站_(IND皇后林蔭路線).md" title="wikilink">伍德哈文林蔭路</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>|</p></td>
<td><p><a href="../Page/格蘭大道-新城車站_(IND皇后林蔭路線).md" title="wikilink">格蘭大道-新城</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>|</p></td>
<td><p><a href="../Page/埃姆赫斯特大道車站_(IND皇后林蔭路線).md" title="wikilink">埃姆赫斯特大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/傑克遜高地-羅斯福大道車站_(IND皇后林蔭路線).md" title="wikilink">傑克遜高地-羅斯福大道</a></p></td>
<td></td>
<td></td>
<td><p>（<a href="../Page/IRT法拉盛線.md" title="wikilink">IRT法拉盛線</a>）<br />
</p></td>
<td><p>線巴士往<a href="../Page/拉瓜迪亞機場.md" title="wikilink">拉瓜迪亞機場</a><br />
<br />
往<a href="../Page/拉瓜迪亞機場.md" title="wikilink">拉瓜迪亞機場</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p>|</p></td>
<td><p><a href="../Page/65街車站_(IND皇后林蔭路線).md" title="wikilink">65街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>|</p></td>
<td><p><a href="../Page/北方林蔭路車站_(IND皇后林蔭路線).md" title="wikilink">北方林蔭路</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>|</p></td>
<td><p><a href="../Page/46街車站_(IND皇后林蔭路線).md" title="wikilink">46街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p>|</p></td>
<td><p><a href="../Page/斯坦威街車站_(IND皇后林蔭路線).md" title="wikilink">斯坦威街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p>|</p></td>
<td><p><a href="../Page/36街車站_(IND皇后林蔭路線).md" title="wikilink">36街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/皇后廣場車站_(IND皇后林蔭路線).md" title="wikilink">皇后廣場</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/法庭廣場-23街車站_(IND皇后林蔭路線).md" title="wikilink">法庭廣場-23街</a></p></td>
<td></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Aiga_elevator.svg" title="fig:Aiga_elevator.svg">Aiga_elevator.svg</a></p></td>
<td><p><br />
（<a href="../Page/IND跨城線.md" title="wikilink">IND跨城線</a>）<br />
（<a href="../Page/IRT法拉盛線.md" title="wikilink">IRT法拉盛線</a>）</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/曼哈頓.md" title="wikilink">曼哈頓</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/萊辛頓大道-53街車站_(IND皇后林蔭路線).md" title="wikilink">萊辛頓大道-53街</a></p></td>
<td></td>
<td></td>
<td><p>（<a href="../Page/IRT萊辛頓大道線.md" title="wikilink">IRT萊辛頓大道線</a>，<a href="../Page/51街車站_(IRT萊辛頓大道線).md" title="wikilink">51街</a>）<br />
</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/第五大道／53街車站_(IND皇后林蔭路線).md" title="wikilink">第五大道／53街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/第七大道車站_(IND皇后林蔭路線).md" title="wikilink">第七大道</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/IND第八大道線.md" title="wikilink">第八大道線</a></strong></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/50街車站_(IND第八大道線).md" title="wikilink">50街</a></p></td>
<td></td>
<td><p> ↓</p></td>
<td><p>（<a href="../Page/IND第八大道線.md" title="wikilink">IND第八大道線</a>）</p></td>
<td><p>只限南行可無障礙通行</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/42街-航港局客運總站車站_(IND第八大道線).md" title="wikilink">42街-航港局客運總站</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/IRT百老匯-第七大道線.md" title="wikilink">IRT百老匯-第七大道線</a>）<br />
（<a href="../Page/IRT法拉盛線.md" title="wikilink">IRT法拉盛線</a>）<br />
（<a href="../Page/BMT百老匯線.md" title="wikilink">BMT百老匯線</a>）<br />
（<a href="../Page/42街接駁線.md" title="wikilink">42街接駁線</a>）<br />
，<a href="../Page/時報廣場-42街車站.md" title="wikilink">時報廣場-42街</a></p></td>
<td><p><a href="../Page/紐新航港局客運總站.md" title="wikilink">紐新航港局客運總站</a><br />
</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/34街-賓州車站_(IND第八大道線).md" title="wikilink">34街-賓州車站</a></p></td>
<td></td>
<td></td>
<td></td>
<td><p><br />
<a href="../Page/美鐵.md" title="wikilink">美鐵</a>、<a href="../Page/長島鐵路.md" title="wikilink">長島鐵路及</a><a href="../Page/新澤西通勤鐵路.md" title="wikilink">新澤西交通</a>，<a href="../Page/賓夕法尼亞車站_(紐約市).md" title="wikilink">賓州車站</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/23街車站_(IND第八大道線).md" title="wikilink">23街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/14街車站_(IND第八大道線).md" title="wikilink">14街</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/BMT卡納西線.md" title="wikilink">BMT卡納西線</a>）</p></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/西四街-華盛頓廣場車站_(IND第八大道線).md" title="wikilink">西四街-華盛頓廣場</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/IND第六大道線.md" title="wikilink">IND第六大道線</a>）</p></td>
<td><p><a href="../Page/紐新航港局過哈德遜河捷運.md" title="wikilink">PATH</a>，<a href="../Page/第九街車站_(PATH).md" title="wikilink">第九街</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/泉街車站_(IND第八大道線).md" title="wikilink">泉街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td><p><a href="../Page/堅尼街車站_(IND第八大道線).md" title="wikilink">堅尼街</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td><p><a href="../Page/世界貿易中心車站_(IND第八大道線).md" title="wikilink">世界貿易中心</a></p></td>
<td></td>
<td></td>
<td><p><br />
（<a href="../Page/IRT百老匯-第七大道線.md" title="wikilink">IRT百老匯-第七大道線</a>，<a href="../Page/公園廣場車站_(IRT百老匯-第七大道線).md" title="wikilink">公園廣場</a>）<br />
（<a href="../Page/BMT百老匯線.md" title="wikilink">BMT百老匯線</a>，<a href="../Page/科特蘭街車站_(BMT百老匯線).md" title="wikilink">科特蘭街</a>）</p></td>
<td><p><a href="../Page/紐新航港局過哈德遜河捷運.md" title="wikilink">PATH</a>，<a href="../Page/世界貿易中心車站_(PATH).md" title="wikilink">世界貿易中心</a><br />
霍博肯及澤西市渡輪</p></td>
</tr>
</tbody>
</table>

## 備註

<references />

## 外部連結

  - [MTA NYC Transit -
    路線介紹](https://web.archive.org/web/20061110014023/http://mta.info/nyct/service/eline.htm)
  - [E線歷史](https://web.archive.org/web/20011202055509/http://members.aol.com/bdmnqr2/linehistory.html)
  - [MTA NYC Transit -
    E線時刻表（PDF）](https://web.archive.org/web/20070421124846/http://www.mta.info/nyct/service/pdf/tecur.pdf)

[E](../Category/紐約地鐵服務路線.md "wikilink")

1.  <http://web.mta.info/developers/resources/line_colors.htm>
2.
3.
4.