[thumb遷台之前的台灣南島語言分布圖](../Page/文件:Formosan_languages.png.md "wikilink")(按
Blust,
1999)\[1\]\[2\].東台灣"[蘭嶼島](../Page/蘭嶼.md "wikilink")(深紅色)表示為使用[馬來-玻里尼西亞語族](../Page/馬來-玻里尼西亞語族.md "wikilink")[巴丹語群](../Page/巴丹語群.md "wikilink")[達悟語的區域](../Page/達悟語.md "wikilink").\]\]

**巴宰語**（；巴則海語）為[台灣](../Page/台灣.md "wikilink")[平埔族](../Page/平埔族.md "wikilink")[巴宰族人所使用的](../Page/巴宰族.md "wikilink")[语言](../Page/语言.md "wikilink")，亦為[台灣南島語言之一種](../Page/台灣南島語言.md "wikilink")，屬於原始南島語系的次語群。亦歸類為[台灣南島語言第](../Page/台灣南島語言.md "wikilink")1群，和[泰雅語](../Page/泰雅語.md "wikilink")（）同列、與[賽夏語](../Page/賽夏語.md "wikilink")（）並列。\[3\]\[4\]
與[噶哈巫語屬同語種](../Page/噶哈巫語.md "wikilink")。

## 巴宰語與噶哈巫語關係

巴宰語與[噶哈巫語基本上可部分互通](../Page/噶哈巫語.md "wikilink")。之間還是有[語音結構上的差異性](../Page/語音.md "wikilink")、與表達法上的不同。語音方面比如：巴宰語有[捲舌音](../Page/捲舌音.md "wikilink")
"**r**"；而噶哈巫語用[齒後音](../Page/齿龈音.md "wikilink") "**l**" 替代、或省略
"**r**"。語句上部分類同，而句型方面有結構上的分野。\[5\]或許二者有如[賽德克語裡的](../Page/賽德克語.md "wikilink")「德克達雅語」與「[德路固語](../Page/德路固語.md "wikilink")」之別。巴宰語現行採用[拉丁字母來拼寫](../Page/拉丁字母.md "wikilink")，比如："家園"寫為："xumak
a ribu"。\[6\]\[7\]

## 族語與族群發展

巴宰族人亦如[邵族人一樣](../Page/邵族.md "wikilink")，已大部份被台灣[閩南人同化](../Page/閩南人.md "wikilink")。人種幾已全部混入[漢族圈裡](../Page/漢族.md "wikilink")，絕大部份使用[闽南语為主要族語](../Page/闽南语.md "wikilink")。\[8\]

### 族語發展

這些年來經巴宰族長老[南投](../Page/南投縣.md "wikilink")[埔里](../Page/埔里鎮.md "wikilink")[潘金玉女士](../Page/潘金玉.md "wikilink")（1914年7月21日－2010年10月24日）及族人全力的投入巴宰語復甦運動，巴宰語漸漸為人所重視。而潘金玉長老幾十年來日常生活幾乎全部使用[閩南語](../Page/閩南語.md "wikilink")，因為少有人能用流利的巴宰語來跟她對話。\[9\]潘金玉長老亦參予編纂過〈國民中小學九年一貫課程語文學習領域
原住民語〉、其中主編〈巴宰語學習手冊第（1-9）階〉。\[10\]于2001年潘金玉長老協助過台灣[中央研究院語言學研究所](../Page/中央研究院.md "wikilink")[李壬癸](../Page/李壬癸.md "wikilink")[院士](../Page/中央研究院院士.md "wikilink")、及日籍學者[土田滋編纂成](../Page/土田滋.md "wikilink")《[巴宰語詞典](../Page/巴宰語詞典.md "wikilink")》。\[11\]\[12\]
[南投](../Page/南投縣.md "wikilink")[埔里](../Page/埔里鎮.md "wikilink")[愛蘭教會](../Page/愛蘭教會.md "wikilink")（愛蘭路45號）每星期六上午（09:00-11:00）有在進行巴宰語班課程，由巴宰族各[長老主持語言教授課程](../Page/長老.md "wikilink")。另有[教育部與](../Page/教育部.md "wikilink")[行政院原住民族委員會發行](../Page/行政院原住民族委員會.md "wikilink")、[政治大學原住民族語言教育文化研究中心主編所發行之原住民語教科書](../Page/政治大學.md "wikilink")〈國民中小學九年一貫課程語文學習領域
原住民語〉系列，中有巴宰語學習手冊第（1-9）階教科書。\[13\]南投縣[巴宰族文化協會已於](../Page/巴宰族文化協會.md "wikilink")2006年12月26日正式發出公文向[行政院原住民族委員會申請巴宰族正名](../Page/行政院原住民族委員會.md "wikilink")。\[14\]\[15\]

### 族群認定

至2017年[巴宰族還不是政府承認的](../Page/巴宰族.md "wikilink")[原住民](../Page/原住民.md "wikilink")，因此巴宰語也未受官方承认。\[16\]\[17\]\[18\]

## 語音系統

巴宰語的音系之[音位大都使用適當的](../Page/音位.md "wikilink")[Unicode符號來標示](../Page/Unicode.md "wikilink")。在[台灣南島語的](../Page/台灣南島語.md "wikilink")[書寫系統的訂定上](../Page/書寫系統.md "wikilink")，元輔音之音系表原則上都是先「[發音部位](../Page/發音部位.md "wikilink")」（橫列）、後「[發音方法](../Page/發音方法.md "wikilink")」（縱列）、再考量「[清濁音](../Page/清濁音.md "wikilink")」，來訂定其音系之音位架構。\[19\]

## 巴宰語語法

在語法的分類上[台灣南島語並不同於一般的](../Page/台灣南島語.md "wikilink")[分析語或其它](../Page/分析語.md "wikilink")[综合语裡的動詞](../Page/综合语.md "wikilink")、名詞、形容詞、介詞和副詞等之基本[詞類分類](../Page/詞類.md "wikilink")。比如台灣南島語裡普遍沒有副詞，而副詞的概念一般以動詞方式呈現、可稱之為「副動詞」，類之於[俄语裡的副動詞](../Page/俄语.md "wikilink")。\[20\]巴宰語語法分類是將基礎的語法之詞類、詞綴、字詞結構及分類法，對比[分析語等之詞類分類法加以條析判別](../Page/分析語.md "wikilink")。\[21\]而在語詞的表達上以[噶哈巫語作為一般通行參考](../Page/噶哈巫語.md "wikilink")。

## 巴宰族詩歌

巴宰族詩歌艾煙[詩歌](../Page/诗歌.md "wikilink")（Ayan epic）系列其中一曲、

長輩們的叮嚀〈Apuwan kinawas; The Elderly lecture〉\[22\]\[23\]

  - 巴宰語

<!-- end list -->

1.  Ayan no ayan ayan no laita.
2.  Ita ita dadua ka abasan o suadi.
3.  Abasan o suadi ka maakariariaki.
4.  Ma'isa’isakup di dini ka maakahahatan.
5.  Mukawas ki apuwan riak ki kaakawas.
6.  Apuwan kinawas ka tumala ka siana.
7.  Reten a baban saw ka maahatahatan.
8.  Xauda ka kinita pakatahayak imu.
9.  Xauda ka kinita pakatahayak imu.
10. Xauda ka kinita pakatahayak imu.
11. Tumala ka ana paxzlihan.
12. Ayan no ayan, saysay yawira.

<!-- end list -->

  - [英語](../Page/英语.md "wikilink")

<!-- end list -->

1.  Oh\! ayan, my ancestor\! we are singing ayan epic.
2.  We are all like a family living together.
3.  We get along very well incessantly.
4.  We live in a happy life.
5.  The elderly lecture is good for us.
6.  We must follow the elderly lecture.
7.  The villager are very happy.
8.  We see and learn very much, thank you.
9.  We see and learn very much, thank you.
10. We see and learn very much, thank you.
11. Don't forget what we have a listen.
12. Oh\! ayan, my ancestor\! it's time to take a rest.

## 註釋

## 參考文獻

  - 林鴻瑞著/黃慧娟指導,"噶哈巫語音韻研究"[12](http://ndltd.ncl.edu.tw/cgi-bin/gs32/gsweb.cgi/ccd=PLRBif/search?q=auc=%22%E6%9E%97%E9%B4%BB%E7%91%9E%22.&searchmode=basic),清大語言所碩士論文,2016年.（[台灣語言學學會的年度碩士論文佳作獎](../Page/台灣語言學學會.md "wikilink")）
  - 林鴻瑞,"噶哈巫語時間詞與空間詞研究"[13](https://docs.google.com/file/d/0Byp3zYDPsoGaZ3hVMHkzMHNrTlU/edit?pli=1),學士論文/[暨南大學中文系](../Page/暨南大學.md "wikilink"),June
    2012.
  - 林英津,\<巴則海語—埔里愛蘭調查報告\>[14](https://folkways.twcenter.org.tw/catalog_detail.jsp?vol=0039&pha=1),臺灣風物
    176-200,1989年3月31日.
  - [衛惠林](../Page/衛惠林.md "wikilink"),〈埔里巴宰七社志〉,《中研院民族所專刊》甲種27，1981年1e/1999年10月2e.
  - 衛惠林,〈巴宰族的親屬結構〉,《臺大考古人類學刊》35,36,pp.1-11,1974年。
  - 劉斌雄,〈埔里巴則海親屬結構的研究〉,《中研院民族所集刊》36,pp.79-111,1973年.
  - 洪秀桂，〈南投巴宰海人的宗教信仰〉，《臺大文史哲學報》22,pp.445-509,1973年.
  - [伊能嘉矩](../Page/伊能嘉矩.md "wikilink"),〈臺灣ピイポオ蕃の一支族ぱゼッヘ(PAZEHHE)の舊慣一斑〉,[東京人類學會雜誌](../Page/東京人類學會雜誌.md "wikilink"),23卷,269號,1908年(明治41年).
  - 伊能嘉矩,〈埔里社平原に於ける熟蕃〉,[蕃情研究會誌](../Page/蕃情研究會誌.md "wikilink"),n002期,1899/04/30(明治32年4月30日),pp.31-55..
  - \-{余}-文儀(臺灣府知府)主修，《[續修臺灣府志](../Page/臺灣府志.md "wikilink")》(余志)卷2,8,14,15,16，“26卷本”，乾隆29年(1764年)。

## 參見

  - [原住民族語能力認證](../Page/原住民族語能力認證.md "wikilink")
  - [原住民族語言書寫系統 (中華民國)](../Page/原住民族語言書寫系統_\(中華民國\).md "wikilink")
  - [族語新聞](../Page/族語新聞.md "wikilink")
  - [原住民族電視台](../Page/原住民族電視台.md "wikilink")
  - [大肚王國](../Page/大肚王國.md "wikilink")
  - [原始人類語言](../Page/原始人類語言.md "wikilink")

## 外部連結

  -
  - [厝角鳥兮春天-巴宰語版](http://mypaper.pchome.com.tw/profpaul/post/1320515499)


  -
  -
  -
  -
{{-}}

[Category:巴宰語](../Category/巴宰語.md "wikilink")
[Category:瀕危南島語言](../Category/瀕危南島語言.md "wikilink")

1.  Blust, R. (1999). "Subgrouping, circularity and extinction: some
    issues in Austronesian comparative linguistics" in E. Zeitoun &
    P.J.K Li (Ed.) Selected papers from the Eighth International
    Conference on Austronesian Linguistics (pp. 31-94). Taipei: Academia
    Sinica.

2.  Paul Jen-kuei Li," Some Remarks on the DNA Study on Austronesian
    Origins"[1](http://www.ling.sinica.edu.tw/eip/FILES/journal/2007.3.9.65909975.1080359.pdf),Languages
    and Linguistics 2.1:237-239,2001.

3.  [齊莉莎](../Page/齊莉莎.md "wikilink")(Zeitoun,
    Elizabeth)，"鄒語參考語法"，臺北，[遠流出版事業](../Page/遠流出版事業.md "wikilink")，台北，p.40，2000年。

4.  王教授的部落格,"即將消失的巴宰語",Yahoo奇摩部落格,2008/07/02 14:25.

5.  [李壬癸](../Page/李壬癸.md "wikilink")、[土田滋](../Page/土田滋.md "wikilink")，"巴宰語詞典(Pazih
    Dictionary)，[中研院](../Page/中研院.md "wikilink")，台北，2001年9月。 ISBN
    957-671-790-6

6.  國立政治大學原住民民族研究中心,"教育部2007年原住民族語文學創作獎",教育部,2008年6月,pp.288-293. ISBN
    978-986-01-4528-1

7.  郭弘斌
    編著,"台灣原住民的語言"[2](http://www.taiwanus.net/history/1/55.htm),臺灣海外網,2003.

8.  David Crystal, "Language Death", Cambridge University Press (April
    29, 2002). ISBN 978-0521012713

9.  "巴宰平埔族語言寶庫潘金玉女士的生平"[3](http://publish.lihpao.com/Aborigines/2005/08/01/05J07311/)-[台灣立報](../Page/台灣立報.md "wikilink")

10. 政治大學原住民族語言教育文化研究中心主編,〈國民中小學九年一貫課程語文學習領域 原住民語〉,台北,教育部、原住民族委員會發行。

11.
12. Elizabeth Zeitoun,"Book Review: Pazih
    Dictionary"[4](http://www.ling.sinica.edu.tw/eip/FILES/journal/2007.3.9.74786012.6169317.pdf),Academia
    Sinica,LANGUAGE AND LINGUISTICS/3.2:481-490,2002,

13.
14. 王鵬捷：\<台灣/原民語文學創作獎得獎者：為巴宰族發聲\>。[中央日報](../Page/中央日報.md "wikilink")。2008年6月25日。[5](http://www.cdnews.com.tw/cdnews_site/docDetail.jsp?coluid=121&docid=100425442)


15. 徐詠絮：<原住民族語文學創作頒獎 落實族語文字化>。[國立教育廣播電臺](../Page/國立教育廣播電臺.md "wikilink")。2008年6月25日。

16. <原民文學獎 巴宰語新詩為族群發聲>。[人間福報](../Page/人間福報.md "wikilink")。2008年6月27日。[6](http://www.cdnews.com.tw/cdnews_site/docDetail.jsp?coluid=121&docid=100425442)


17.
18. 潘大和,"平埔族正名
    還要加把勁"[7](http://news.chinatimes.com/forum/0,5252,110514x112010061700177,00.html),[中國時報](../Page/中國時報.md "wikilink"),2010-06-17。

19. 行政院原住民族委員會,"原住民族語言書寫系統"[8](http://www.edu.tw/files/list/M0001/aboriginal.pdf),台語字第0940163297號,原民教字第09400355912號公告,中華民國94年12月15日.

20. 張永利,"台灣南島語言語法：語言類型與理論的啟示(Kavalan)"[9](http://vietnam.nsc.gov.tw/public/Data/012714292271.pdf),語言學門熱門前瞻研究,2010年12月/12卷1期,pp.112-127.

21. Barbara B.H. Partee, A.G. ter Meulen, R. Wall,"Mathematical Methods
    in Linguistics (Studies in Linguistics and
    Philosophy)(語言研究的數學方法)"[10](http://acl.ldc.upenn.edu/J/J92/J92-1009.pdf)[11](http://www.amazon.com/Mathematical-Methods-Linguistics-Studies-Philosophy/dp/9027722455),Springer,1/e
    1993 edition(April 30, 1990).

22. "巴宰族歌謠-岸裡尋根之旅-Pazeh探索",[台中縣](../Page/台中縣.md "wikilink")[岸裡國民小學](../Page/岸裡國民小學.md "wikilink"),2003年2月10日。

23. [李壬癸](../Page/李壬癸.md "wikilink")(Paul Jen-kuei
    Li),[土田滋](../Page/土田滋.md "wikilink")(Shigeru
    Tsuchida),"巴宰族傳說歌謠集(Pazih Texts and Songs)",台北,11月,2002年. ISBN
    957-671-888-0