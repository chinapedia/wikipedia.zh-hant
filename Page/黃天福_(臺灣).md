**黃天福**（），[台灣政治人物](../Page/台灣.md "wikilink")，[台北市](../Page/台北市.md "wikilink")[大龙峒](../Page/大龙峒.md "wikilink")（今[大同區](../Page/大同區_\(台北市\).md "wikilink")）人，[祖籍](../Page/祖籍.md "wikilink")[福建省](../Page/福建省.md "wikilink")[泉州府](../Page/泉州府.md "wikilink")[安溪縣](../Page/安溪縣.md "wikilink")，祖父是前清[秀才](../Page/秀才.md "wikilink")[黃宗河](../Page/黃宗河.md "wikilink")，兄長是前[民進黨主席](../Page/民進黨主席.md "wikilink")[黃信介](../Page/黃信介.md "wikilink")。

曾代表[民主進步黨任職](../Page/民主進步黨.md "wikilink")[立法委員](../Page/立法委員.md "wikilink")，早期因《[蓬萊島](../Page/蓬萊島.md "wikilink")》雜誌提到[馮滬祥所寫論文其實是](../Page/馮滬祥.md "wikilink")「以翻譯代替著作」而遭判徒期八個月，與[陳水扁](../Page/陳水扁.md "wikilink")、[李逸洋並稱](../Page/李逸洋.md "wikilink")「[蓬萊島三君子](../Page/蓬萊島三君子.md "wikilink")」\[1\]\[2\]\[3\]；在[江國慶被](../Page/江國慶.md "wikilink")[處決前](../Page/處決.md "wikilink")，曾召開記者會指出該案可能是冤獄，而在2011年確認江國慶冤死。

## 家庭

其妻子為[立法委員](../Page/立法委員.md "wikilink")[藍美津](../Page/藍美津.md "wikilink")，次子[黃向羣在](../Page/黃向羣.md "wikilink")[2006年台北市議員選舉中](../Page/2006年中華民國直轄市市長暨市議員選舉.md "wikilink")，當選臺北市議員。其女兒黃心儀則在2004年10月底因[憂鬱症自盡身亡](../Page/憂鬱症.md "wikilink")。

<center>

|                                                                                                               |
| :-----------------------------------------------------------------------------------------------------------: |
|                                  **[蓬萊島三君子](../Page/蓬萊島三君子.md "wikilink")**                                   |
| [李逸洋](../Page/李逸洋.md "wikilink") - [陳水扁](../Page/陳水扁.md "wikilink") - [黃天福](../Page/黃天福_\(台灣\).md "wikilink") |

</center>

## 註解

## 外部連結

[立法院
黃天福委員簡介](http://www.ly.gov.tw/ly/01_introduce/0103_leg/leg_main/leg_ver01_01.jsp?ItemNO=01030100&lgno=00111&stage=3)


[Category:民主進步黨黨員](../Category/民主進步黨黨員.md "wikilink")
[Category:大同區人](../Category/大同區人.md "wikilink")
[Category:臺北市立建國高級中學校友](../Category/臺北市立建國高級中學校友.md "wikilink")
[Category:國立臺灣大學校友](../Category/國立臺灣大學校友.md "wikilink")
[Tian天福](../Category/黃姓.md "wikilink")
[H](../Category/中華民國總統府國策顧問.md "wikilink")
[Category:美麗島事件受難者](../Category/美麗島事件受難者.md "wikilink")
[Category:黃姓](../Category/黃姓.md "wikilink")

1.  [我們的牢不會白坐！放棄上訴
    入監記者會](http://taiwantt.org.tw/books/cryingtaiwan4/content/1980/19850122a-24.htm),
    《東北風週刊》, 1985.01.22
2.  [由蓬萊島案件之審判論法官之獨立公正](http://www.wufi.org.tw/由蓬萊島案件之審判論法官之獨立公正/)
3.  [支援台灣民主運動─芝加哥大學廖述宗教授的故事](http://ccycenter.ning.com/forum/topics/zhi-jia-ge-da-xue-liao-shu)