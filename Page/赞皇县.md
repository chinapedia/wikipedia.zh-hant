**赞皇县**是[中国](../Page/中国.md "wikilink")[河北省](../Page/河北省.md "wikilink")[石家庄市所辖的一个](../Page/石家庄市.md "wikilink")[县](../Page/县.md "wikilink")。

## 历史沿革

  - [隋](../Page/隋.md "wikilink")[开皇十六年](../Page/开皇.md "wikilink")（596年）置赞皇县。
  - [宋](../Page/宋.md "wikilink")[熙宁五年](../Page/熙宁.md "wikilink")（1072年）并入[高邑县](../Page/高邑县.md "wikilink")，[元祐元年](../Page/元祐.md "wikilink")（1086年）复置赞皇县。
  - [元](../Page/元.md "wikilink")[至元二年再次并入高邑](../Page/至元.md "wikilink")，[至元七年复置](../Page/至元.md "wikilink")。
  - 1958年并入[元氏县](../Page/元氏县.md "wikilink")，1961年复置。\[1\]

## 行政区划

下辖4个[镇](../Page/镇.md "wikilink")、7个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

## 地理

西为[太行山](../Page/太行山.md "wikilink")，东为[华北平原](../Page/华北平原.md "wikilink")。总面积为1210平方公里。

## 交通

  - [202省道](../Page/202省道.md "wikilink")
  - [232省道](../Page/232省道.md "wikilink")

## 经济

2010年GDP为16.08亿元。

## 自然资源

[硅石储量约](../Page/硅石.md "wikilink")10亿吨，[石灰石已探明B](../Page/石灰石.md "wikilink")＋C＋D级储量2亿吨。[花岗岩储量](../Page/花岗岩.md "wikilink")2亿立方米以上。\[2\]

## 方言

赞皇县境内的[汉语方言多属于](../Page/汉语方言.md "wikilink")[晋语](../Page/晋语.md "wikilink")[大包片](../Page/大包片.md "wikilink")。

## 风景名胜

  - 西南部[嶂石岩为](../Page/嶂石岩.md "wikilink")[国家重点风景名胜区](../Page/国家重点风景名胜区.md "wikilink")、[国家地质公园](../Page/国家地质公园.md "wikilink")
  - [琉璃光净业寺](../Page/琉璃光净业寺.md "wikilink")
  - 全国重点文物保护单位：[治平寺石塔](../Page/治平寺石塔.md "wikilink")
  - 河北省文物保护单位：[李氏墓群](../Page/李氏墓群.md "wikilink")、[万坡顶遗址](../Page/万坡顶遗址.md "wikilink")

## 参考文献

## 外部链接

  - [赞皇县政府网站](http://www.zanhuang.gov.cn/)

{{-}}

[赞皇县](../Category/赞皇县.md "wikilink")
[县](../Category/石家庄县级行政区.md "wikilink")
[石家庄](../Category/河北省县份.md "wikilink")
[冀](../Category/国家级贫困县.md "wikilink")

1.  <http://www.zanhuang.gov.cn/art/2008/09/04/art_27296_266420.html>
    赞皇县概况

2.