**張子岱**（**Cheung Chi
Doy**，），生於[香港](../Page/香港.md "wikilink")，已退役[香港著名足球運動員](../Page/香港.md "wikilink")，司職[前鋒](../Page/前鋒_\(足球\).md "wikilink")，綽號「**阿香**」，籍貫[江蘇](../Page/江蘇.md "wikilink")[上海](../Page/上海.md "wikilink")，前[中華民國國腳](../Page/中華民國國家足球隊.md "wikilink")，為叱吒[香港及](../Page/香港.md "wikilink")[亞洲球壇的球星](../Page/亞洲.md "wikilink")，於1960至62年效力[英格蘭甲組聯賽球會](../Page/英格蘭足球甲級聯賽.md "wikilink")[黑池](../Page/黑池足球會.md "wikilink")，是歷來首位加盟[歐洲頂級聯賽的](../Page/歐洲.md "wikilink")[華人及](../Page/華人.md "wikilink")[亞洲球員](../Page/亞洲.md "wikilink")，在[1998年世界盃前夕全球體育記者就五大洲選出本世紀各洲的最佳陣容](../Page/1998年世界盃足球賽.md "wikilink")，其中張子岱在[亞洲最佳陣容中榜上有名](../Page/亞洲.md "wikilink")，亦是唯一入選的[華人球員](../Page/華人.md "wikilink")，與父親[張金海及四弟](../Page/張金海.md "wikilink")[張子慧都是前](../Page/張子慧.md "wikilink")[中華民國國腳](../Page/中華民國國家足球隊.md "wikilink")。於2016年協拍攝紀錄片《[尋找張子岱](../Page/尋找張子岱.md "wikilink")》，在同年8月，年屆75歲的張子岱遠赴[英國重返現於](../Page/英國.md "wikilink")[英格蘭乙組聯賽作賽的](../Page/英格蘭足球乙級聯賽.md "wikilink")[黑池](../Page/黑池足球會.md "wikilink")，與前[英格蘭國腳](../Page/英格蘭足球代表隊.md "wikilink")[占美安菲及](../Page/占美·岩菲路.md "wikilink")等昔日隊友及球迷重聚，獲球會高規格接待，而[英國傳媒亦有廣泛報導](../Page/英國.md "wikilink")。

## 球會生涯

### 初生之犢

張子岱生於[香港](../Page/香港.md "wikilink")，幼年居住於[香港島](../Page/香港島.md "wikilink")[黃泥涌](../Page/黃泥涌.md "wikilink")，父親[張金海為](../Page/張金海.md "wikilink")[上海著名足球員](../Page/上海.md "wikilink")。其父於1938年從[上海移居](../Page/上海.md "wikilink")[香港後](../Page/香港.md "wikilink")，投效勁旅[星島](../Page/星島體育會.md "wikilink")。由於年幼的張子岱常跟隨父親常於[跑馬地球場進行操練](../Page/跑馬地馬場.md "wikilink")，使他球技日漸磨練成熟，為日後的足球事業打下了穩實基礎。他在小學階段就讀[慈幼小學](../Page/慈幼英文學校_\(小學部\).md "wikilink")，由於學校對推行足球運動不遺餘力，令張子岱在小學足球隊已顯露其足球天賦及能力，升讀[慈幼中學後](../Page/慈幼英文學校_\(中學部\).md "wikilink")，於學界比賽初嚐比賽經驗，並成為學界足球精英。

他在15歲加入[星島預備組](../Page/星島體育會.md "wikilink")，並大放異彩。於1959年5月24日的[士丹利木盾七人賽帶領](../Page/士丹利木盾.md "wikilink")[星島乙組一眾小將迎戰勁旅](../Page/星島體育會.md "wikilink")[東華甲組](../Page/東華體育會.md "wikilink")，張子岱單憑其個人突破能力，先後於比賽1分鐘和6分鐘兩度攻破[東華大門](../Page/東華體育會.md "wikilink")，並隨即採取死守戰術，最終爆冷擊敗大熱門[東華甲組](../Page/東華體育會.md "wikilink")。

在1959年，他獲[甲組勁旅](../Page/香港甲組足球聯賽.md "wikilink")[東華賞識](../Page/東華體育會.md "wikilink")，邀請加盟，與當時灸手可熱的球星[姚卓然](../Page/姚卓然.md "wikilink")、[陳輝洪及](../Page/陳輝洪.md "wikilink")[吳偉文等同隊作賽](../Page/吳偉文.md "wikilink")，其後於1959年9月12日，繼[姚卓然後亦加入](../Page/姚卓然.md "wikilink")[東華](../Page/東華體育會.md "wikilink")，更被時任教練[宋靈聖和](../Page/宋靈聖.md "wikilink")[姚卓然盛讚他](../Page/姚卓然.md "wikilink")「具備成為優秀球員之根底，假以時日必成大器」，加盟[東華後](../Page/東華體育會.md "wikilink")，張子岱很快就成為了球隊的主力球員，更為球隊作出莫大的貢獻。1960年2月22日，他在隊友[姚卓然和](../Page/姚卓然.md "wikilink")[陳輝洪等名將的指導下](../Page/陳輝洪.md "wikilink")，已從邊路球員轉型為正中鋒，並代表[東華迎戰母會](../Page/東華體育會.md "wikilink")[星島中梅開二度](../Page/星島體育會.md "wikilink")，協助球會以5–0勝出。

### 遠赴英倫

在1960年8月10日，只有19歲的張子岱因為其精湛球技，獲時任《[英文虎報](../Page/英文虎報.md "wikilink")》及電台足球評述員[麥他維殊和英藉球證馬士頓韋大力推薦下](../Page/麥他維殊.md "wikilink")，成功以職業球員身份加盟當時於[歐洲頂級聯賽中打滾的球會](../Page/歐洲.md "wikilink")[黑池](../Page/黑池足球會.md "wikilink")，並在隊中司職正中鋒（當年並未成立[英格蘭超級足球聯賽](../Page/英格蘭超級足球聯賽.md "wikilink")，[甲組聯賽已是](../Page/英格蘭足球甲級聯賽.md "wikilink")[英格蘭最頂級聯賽](../Page/英格蘭.md "wikilink")），因而成為歷來第一位效力[歐洲頂級聯賽球會的](../Page/歐洲.md "wikilink")[華人球員](../Page/華人.md "wikilink")，此事亦為[香港球壇引以為傲](../Page/香港.md "wikilink")，除了麥他士維和馬士頓韋外，時為[黑池球員的](../Page/黑池足球會.md "wikilink")[馬菲士亦親至寫信予](../Page/史丹利·馬菲士.md "wikilink")[黑池高層人員](../Page/黑池足球會.md "wikilink")，力勸該球會給予年紀輕輕的張子岱一個上陣機會。

由於當時[黑池的領隊](../Page/黑池足球會.md "wikilink")喜歡重用年紀大而名氣響的球員，如年逾40歲的[馬菲士](../Page/斯坦利·馬修斯.md "wikilink")，張子岱只能安排在預備隊上陣，兩年間共上陣43場射入23球，終於在1961年1月14日在[黑池一隊對陣](../Page/黑池足球會.md "wikilink")[狼隊的賽事首度正選披甲](../Page/狼隊足球會.md "wikilink")，惟開戰約9分鐘後，比賽就因為濃霧而被逼取消。

同月，他在迎戰[保頓的比賽中再次得到在一隊正選上陣的機會](../Page/保頓足球會.md "wikilink")，惜球隊以1–3落敗。事隔10個月即1961年11月25日，張子岱才再有機會為[黑池正選出戰](../Page/黑池足球會.md "wikilink")[錫周三](../Page/錫周三足球會.md "wikilink")，雖然今次同樣以1–3的比數落敗，但張子岱於這場比賽中為[黑池破蛋](../Page/黑池足球會.md "wikilink")，射破當時[英格蘭國腳門將](../Page/英格蘭足球代表隊.md "wikilink")[朗史賓格的大門](../Page/朗·史賓格.md "wikilink")，是首位和至今唯一在[歐洲及](../Page/歐洲.md "wikilink")[英格蘭頂級聯賽中取得入球的](../Page/英格蘭.md "wikilink")[香港球員](../Page/香港.md "wikilink")，比較同是[華人的](../Page/華人.md "wikilink")[中國球員](../Page/中國.md "wikilink")[孫繼海於](../Page/孫繼海.md "wikilink")2002年10月在[曼城的入球早了](../Page/曼城足球會.md "wikilink")40年，儘管遠赴[英國時被大家寄予厚望](../Page/英國.md "wikilink")，不過在1962年[黑池更換主教練後](../Page/黑池足球會.md "wikilink")，張子岱缺乏上陣機會，加上受不了諸如思鄉等壓力，決定與[黑池提前解約回港](../Page/黑池足球會.md "wikilink")，雖然在[黑池苦無上陣機會](../Page/黑池足球會.md "wikilink")，但他在那裡進行的預備組賽事和操練還是大大提昇了其足球技術。

### 噴射機起飛

1962年，張子岱回流[香港後加盟有](../Page/香港.md "wikilink")「鄉紳班」之稱的[元朗](../Page/元朗足球會.md "wikilink")，同時在[匯豐銀行兼職](../Page/匯豐銀行.md "wikilink")，其後轉投[光華](../Page/光華體育會.md "wikilink")，翌年，張子岱再投效[傑志](../Page/傑志體育會.md "wikilink")，為專心踢足球決定辭去[匯豐銀行的工作](../Page/匯豐銀行.md "wikilink")，協助[傑志奪得](../Page/傑志體育會.md "wikilink")1963/64年球季的[甲組聯賽及](../Page/香港甲組足球聯賽.md "wikilink")[特別銀牌雙料冠軍](../Page/香港足球高級組銀牌.md "wikilink")。

當綽號「噴射機」的[星島於](../Page/星島體育會.md "wikilink")1964年從[乙組回升甲組](../Page/香港乙組足球聯賽.md "wikilink")，而張子岱就重投母會，協助[星島贏取](../Page/星島體育會.md "wikilink")1964/65年、1965/66年及1966/67年三屆甲組聯賽季軍及1967/68年聯賽亞軍、1966/67年[特別銀牌冠軍和](../Page/香港足球高級組銀牌.md "wikilink")1965/66年及1966/67年[金禧盃冠軍](../Page/香港金禧盃盃.md "wikilink")。

1966年歇暑期間，張子岱先獲[星島外借隨](../Page/星島體育會.md "wikilink")[南華遠征](../Page/南華足球隊.md "wikilink")[南非洲三島](../Page/南部非洲.md "wikilink")[留尼旺](../Page/留尼汪.md "wikilink")、[毛里裘斯及](../Page/毛里裘斯.md "wikilink")[馬達加斯加](../Page/馬達加斯加.md "wikilink")，於13戰中保持不敗，接著再隨[星島到](../Page/星島體育會.md "wikilink")[澳洲和](../Page/澳洲.md "wikilink")[紐西蘭比賽](../Page/紐西蘭.md "wikilink")，11戰未逢敗績，短短兩個月間踢了24場比賽。

1966/67年度球季，[香港足球總會首次設立甲組聯賽神射手獎項](../Page/香港足球總會.md "wikilink")，張子岱以36球奪得神射手獎項，而其弟[張子慧以](../Page/張子慧.md "wikilink")34球屈居亞軍，兩兄弟在一個球季合共射入70球之多，成為一時佳話。\[1\]

### 失意北美

1965年6月17日，[黑池與](../Page/黑池足球會.md "wikilink")[錫菲聯一同訪港](../Page/錫菲聯足球會.md "wikilink")，參加[香港足球總會的金禧紀念表演賽](../Page/香港足球總會.md "wikilink")，期間再遇張子岱在陣的華聯隊大勝7–2，賽後翌晨，[黑池領隊](../Page/黑池足球會.md "wikilink")邀約張子岱到居住的酒店商討重投事宜，不過當時已婚及在港已有一定成就的張子岱以家庭為由婉拒。

在1967/68球季，他與其弟[張子慧一同獲選](../Page/張子慧.md "wikilink")[亞洲明星隊赴](../Page/亞洲.md "wikilink")[馬來西亞出戰](../Page/馬來西亞.md "wikilink")[富咸](../Page/富咸足球會.md "wikilink")，當時已接近退役的[英格蘭名宿](../Page/英格蘭.md "wikilink")[波比笠臣指對兩人的表現印象深刻](../Page/波比笠臣.md "wikilink")，直到[波比笠臣在](../Page/波比笠臣.md "wikilink")1967年在[北美足球聯賽球會](../Page/北美足球聯賽.md "wikilink")擔任球員兼教練一職後，他於1968年2月邀請張氏兄弟張子岱及[張子慧加盟](../Page/張子慧.md "wikilink")，當時張子岱年薪高達14,000美元，另加3,000美元簽紙費，是整隊最高薪的球員，但在兩人加盟後僅1個月，[波比笠臣便因與班主不和而離隊](../Page/波比笠臣.md "wikilink")，轉為由[匈牙利球王](../Page/匈牙利.md "wikilink")[普斯卡斯接任](../Page/普斯卡斯.md "wikilink")，並引入多名[拉丁語系球員](../Page/拉丁語系.md "wikilink")，與原有的[英國球員格格不入](../Page/英國.md "wikilink")，導致球會成績低落，在中敬陪末席僅一年便告散班，在球隊解散前，張子慧返港加盟[消防](../Page/消防足球隊.md "wikilink")，而張子岱則留守[北美轉投](../Page/北美.md "wikilink")，完成[北美足球聯賽的首個賽季然後才返回](../Page/北美足球聯賽.md "wikilink")[香港](../Page/香港.md "wikilink")。\[2\]

### 再闖高峰

[英資的](../Page/英國.md "wikilink")[怡和於](../Page/怡和體育會.md "wikilink")1968年升上[甲組](../Page/香港甲組足球聯賽.md "wikilink")，並積極網羅優秀球員組織巨型班參賽，陣中包括[陳鴻平](../Page/陳鴻平.md "wikilink")、[黃文偉](../Page/黃文偉.md "wikilink")、[龔華傑等名將及新星](../Page/龔華傑.md "wikilink")[葉尚華](../Page/葉尚華.md "wikilink")，又透過[張金海遊說其子張子岱返港加盟](../Page/張金海.md "wikilink")，而張子岱在季中離開[北美返港加盟](../Page/北美.md "wikilink")[怡和](../Page/怡和體育會.md "wikilink")，月薪2,500港元，是同期職業球員中最高薪的一員，更獲安排在[怡和洋行機械部工作任職冷氣機推廣](../Page/怡和洋行.md "wikilink")，[怡和升班後的首個賽季便獲得季軍及](../Page/怡和體育會.md "wikilink")[特別銀牌冠軍的理想成績](../Page/香港足球高級組銀牌.md "wikilink")，在接下來的1969/70年球季更囊括甲組聯賽、[職業盃](../Page/香港足球總督盃.md "wikilink")、[金禧盃及](../Page/香港足總盃.md "wikilink")[士丹利木盾賽成為史無前例的](../Page/士丹利木盾賽.md "wikilink")「四冠王」，可是[\[怡和僅維持三年便於](../Page/怡和體育會.md "wikilink")1971年宣佈退出甲組行列。

1970年12月30日在[花墟球場舉行](../Page/警察會球場.md "wikilink")[怡和對](../Page/怡和體育會.md "wikilink")[九巴的職業盃比賽中](../Page/九龍巴士足球隊.md "wikilink")，張子岱與對方的[廖榮礎大打出手](../Page/廖榮礎.md "wikilink")，雙方職球員隨即勸止，場內警察更需出場震壓才能平息打鬥，最終兩人同被罰紅牌離場，而張子岱下巴受傷需入院縫針，事後[石硤尾警署更要求兩人](../Page/石硤尾警署.md "wikilink")、球隊代表及球旁證等到警局落口供，最終兩人各被判罰停賽6個月。[怡和退出後全體球員獲准自由轉會](../Page/怡和體育會.md "wikilink")，張子岱聯同[張耀國](../Page/張耀國.md "wikilink")、[鄭潤如及](../Page/鄭潤如.md "wikilink")[葉尚華轉投](../Page/葉尚華.md "wikilink")[消防](../Page/消防足球隊.md "wikilink")，與弟弟[張子慧再度併肩作戰](../Page/張子慧.md "wikilink")，但由於與[消防的管理層不和](../Page/消防足球隊.md "wikilink")，張子岱僅效力一年便離隊他投。

在1972/73年球季，張子岱轉投[愉園](../Page/愉園體育會.md "wikilink")，由於張子岱曾代表[中華民國參賽](../Page/中華民國國家足球隊.md "wikilink")，而當時[愉園則是](../Page/愉園體育會.md "wikilink")[左派球隊](../Page/左派.md "wikilink")，他遭到[右派報章如](../Page/右派.md "wikilink")《[香港時報](../Page/香港時報.md "wikilink")》口誅筆伐稱他「棄明投暗」，隨著班霸[精工崛起](../Page/精工足球隊.md "wikilink")，各項賽事的競爭更加激烈，在強隊林立下，張子岱仍能協助[愉園奪得](../Page/愉園體育會.md "wikilink")[1975/76年總督盃及](../Page/香港足球總督盃.md "wikilink")[1977/78年高級組銀牌的冠軍](../Page/香港足球高級組銀牌.md "wikilink")。

在[1998年法國世界盃前夕](../Page/1998年國際足協世界盃.md "wikilink")，張子岱被全球的體育記者共同選他為20世紀[亞洲最佳陣容之一](../Page/亞洲.md "wikilink")，是為唯一入選的[香港人以及](../Page/香港人.md "wikilink")[華人](../Page/華人.md "wikilink")。\[3\]

2016年8月，張子岱為拍攝紀錄片《[尋找張子岱](../Page/尋找張子岱.md "wikilink")》在55年後再次重返[黑池](../Page/黑池足球會.md "wikilink")，在[黑池主場聯賽對](../Page/黑池足球會.md "wikilink")[普利茅夫的](../Page/普利茅夫足球會.md "wikilink")[英乙聯賽再次回到球場觀戰](../Page/英格蘭足球乙級聯賽.md "wikilink")，並以特別嘉賓出場，獲得全場球迷掌聲，最終[黑池最終以](../Page/黑池足球會.md "wikilink")0–1落敗。\[4\]

## 教練生涯

1978年，已達37歲的張子岱開始轉任教練工作，他先在[愉園擔任助教其後在](../Page/愉園體育會.md "wikilink")[長和擔任球員兼教練](../Page/長和足球隊.md "wikilink")，及後執教過[寶路華](../Page/寶路華足球隊.md "wikilink")、[花花](../Page/花花足球會.md "wikilink")、[駒騰及](../Page/宏輝駒騰.md "wikilink")[傑志可是他執教球隊的成績一般](../Page/傑志體育會.md "wikilink")，直到80年代後球市下滑而張子岱決定離開球圈工餘時代表[星島元老隊踢友誼聯賽](../Page/星島體育會.md "wikilink")。\[5\]

## 退役生活

張子岱於1985年離開球圈後轉職[的士司機](../Page/的士司機.md "wikilink")，其弟張子慧後來亦移民[加拿大](../Page/加拿大.md "wikilink")，任職其間經常在機場接載外國遊客並濫收車資，後來張子岱在1988年被法院檢控在[啟德機場接載一對外藉夫婦時](../Page/啟德機場.md "wikilink")，以顯示為32.3元的咪錶收取對方250元的費用，外藉夫婦下車後抄下他的車牌並報警，最終張子岱被法庭罰款2,000元及賠償210元予該夫婦。

直至近年，張子岱於2011年3月[東涌的超市內帶走](../Page/東涌.md "wikilink")4支紅酒、三樽果仁和數罐汽水，但只為汽水付款後便離去，遭當值保安發現即時將他攔住並捕警將其拘捕，最後他在[荃灣法院承認一項偷竊罪被罰款港幣](../Page/荃灣法院.md "wikilink")2,000元。

## 家庭生活

張子岱的父親是前[上海足球名將](../Page/上海.md "wikilink")，綽號「拚命三郎」的[張金海](../Page/張金海.md "wikilink")，張父於1938年移居[香港](../Page/香港.md "wikilink")，育有四子，張子岱排名第二，因在[香港出生故取乳名為](../Page/香港.md "wikilink")「阿香」，幼弟[張子慧在](../Page/張子慧.md "wikilink")1946年和平後出生，故取乳名「阿平」。除了三弟之外，其餘三人均曾經是甲組球員，長兄[張子文曾效力](../Page/張子文.md "wikilink")[光華及](../Page/光華體育會.md "wikilink")[警察](../Page/警察足球隊.md "wikilink")，幼弟[張子慧則效力過](../Page/張子慧.md "wikilink")、[消防](../Page/消防足球隊.md "wikilink")、[精工等球隊](../Page/精工足球隊.md "wikilink")，退役後移居加拿大。

張子岱與妻育有三名子女，均已成年，其中兒子[張學潤是香港著名的形象設計師](../Page/張學潤.md "wikilink")\[6\]。

## 個人榮譽

  - [香港甲組足球聯賽神射手](../Page/香港甲組足球聯賽.md "wikilink")（1963–64、1966–67年度）

## 參考資料

## 參考文獻

  - 《香港足球誌》，[莫逸風](../Page/莫逸風.md "wikilink")、[黃海榮著](../Page/黃海榮.md "wikilink")，上書局，2008，ISBN
    978-988-17735-2-4
  - 《香港十大名將》，青森文化，[賴文輝著](../Page/賴文輝.md "wikilink")，2013，ISBN
    978-988-82232-5-1
  - [Pool pioneers of the
    Orient](http://www.blackpoolgazette.co.uk/sports-news/Pool-pioneers-of-the-Orient.3814324.jp)
  - [NASL：張子岱](http://www.nasljerseys.com/Players/C/Cheung.Chi-Doy.htm)
  - [昔日愉園](http://www.hvaafc.com/?goto=history&id=2)

## 外部連結

[Category:張姓](../Category/張姓.md "wikilink")
[Category:港甲球員](../Category/港甲球員.md "wikilink")
[Category:東華球員](../Category/東華球員.md "wikilink")
[Category:黑池球員](../Category/黑池球員.md "wikilink")
[Category:元朗球員](../Category/元朗球員.md "wikilink")
[Category:光華球員](../Category/光華球員.md "wikilink")
[Category:傑志球員](../Category/傑志球員.md "wikilink")
[Category:星島球員](../Category/星島球員.md "wikilink")
[Category:怡和球員](../Category/怡和球員.md "wikilink")
[Category:消防球員](../Category/消防球員.md "wikilink")
[Category:愉園球員](../Category/愉園球員.md "wikilink")
[Category:足球中場](../Category/足球中場.md "wikilink")
[Category:寶路華教練](../Category/寶路華教練.md "wikilink")
[Category:花花教練](../Category/花花教練.md "wikilink")
[Category:駒騰教練](../Category/駒騰教練.md "wikilink")
[Category:傑志教練](../Category/傑志教練.md "wikilink")
[Category:英格蘭外籍足球運動員](../Category/英格蘭外籍足球運動員.md "wikilink")
[Category:香港旅外足球運動員](../Category/香港旅外足球運動員.md "wikilink")
[Category:中華民國國家足球隊球員](../Category/中華民國國家足球隊球員.md "wikilink")
[Category:香港足球運動員](../Category/香港足球運動員.md "wikilink")
[Category:香港罪犯](../Category/香港罪犯.md "wikilink")

1.  [九十後眾籌拍攝《尋找張子岱》續集 冀和張子岱重回黑池](http://www.inmediahk.net/node/1043162)
    香港獨立媒體 2016-06-30
2.  [港波佬Vol.4：淺談「港足」百餘載
    之四──「征歐第一人」張子岱](http://www.goal.com/hk/news/3636/%E9%A6%99%E6%B8%AF/2011/10/07/2699763/%E6%B8%AF%E6%B3%A2%E4%BD%ACvol4%E6%B7%BA%E8%AB%87%E6%B8%AF%E8%B6%B3%E7%99%BE%E9%A4%98%E8%BC%89-%E4%B9%8B%E5%9B%9B%E5%BE%81%E6%AD%90%E7%AC%AC%E4%B8%80%E4%BA%BA%E5%BC%B5%E5%AD%90%E5%B2%B1)
    Goal.com 2011年10月7日
3.  [專訪港球王張子岱　「我一早應該在黑池踢正選」](https://www.hk01.com/%E9%AB%94%E8%82%B2/40780/%E5%B0%88%E8%A8%AA%E6%B8%AF%E7%90%83%E7%8E%8B%E5%BC%B5%E5%AD%90%E5%B2%B1-%E6%88%91%E4%B8%80%E6%97%A9%E6%87%89%E8%A9%B2%E5%9C%A8%E9%BB%91%E6%B1%A0%E8%B8%A2%E6%AD%A3%E9%81%B8-)
    [香港01](../Page/香港01.md "wikilink") 2016-09-01
4.  [90後導演發起眾籌　開拍《尋找張子岱》續集](https://www.hk01.com/%E9%AB%94%E8%82%B2/33554/90%E5%BE%8C%E5%B0%8E%E6%BC%94%E7%99%BC%E8%B5%B7%E7%9C%BE%E7%B1%8C-%E9%96%8B%E6%8B%8D-%E5%B0%8B%E6%89%BE%E5%BC%B5%E5%AD%90%E5%B2%B1-%E7%BA%8C%E9%9B%86)
    [香港01](../Page/香港01.md "wikilink") 2016-07-26
5.  [張子岱55年後重返黑池　獲球迷報以英雄式掌聲](https://www.hk01.com/%E9%AB%94%E8%82%B2/40037/%E5%BC%B5%E5%AD%90%E5%B2%B155%E5%B9%B4%E5%BE%8C%E9%87%8D%E8%BF%94%E9%BB%91%E6%B1%A0-%E7%8D%B2%E7%90%83%E8%BF%B7%E5%A0%B1%E4%BB%A5%E8%8B%B1%E9%9B%84%E5%BC%8F%E6%8E%8C%E8%81%B2)
    [香港01](../Page/香港01.md "wikilink") 2016-08-29
6.  [張學潤張子岱關係公開](http://www.singtao.com/archive/fullstory.asp?andor=or&year1=2000&month1=7&day1=1&year2=2000&month2=7&day2=1&category=all&id=20000701f06&keyword1=&keyword2=)