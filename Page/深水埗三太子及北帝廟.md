[HK_SamTaiTze_and_PakTai_Temple.JPG](https://zh.wikipedia.org/wiki/File:HK_SamTaiTze_and_PakTai_Temple.JPG "fig:HK_SamTaiTze_and_PakTai_Temple.JPG")
**三太子廟**又稱**三太子宮**，是[香港唯一供奉](../Page/香港.md "wikilink")[中國神話](../Page/中國神話.md "wikilink")[封神榜主角的](../Page/封神榜.md "wikilink")[廟宇](../Page/廟宇.md "wikilink")，內裡供奉[哪吒三太子](../Page/哪吒.md "wikilink")。廟宇位於[九龍](../Page/九龍.md "wikilink")[深水埗](../Page/深水埗.md "wikilink")[汝洲街](../Page/汝洲街.md "wikilink")，建於[清朝](../Page/清朝.md "wikilink")[光緒年間](../Page/光緒.md "wikilink")，現為[香港二級歷史建築](../Page/香港二級歷史建築.md "wikilink")。三太子宮側建有**北帝廟**，又稱**北帝宮**。兩廟同於1931年撥歸[華人廟宇委員會負責管理](../Page/華人廟宇委員會.md "wikilink")。1985年及2006年曾進行重建工程。

## 歷史及結構

1894年香港發生[鼠疫](../Page/1894年香港鼠疫爆發.md "wikilink")，位於[界限街以北的深水埗仍屬](../Page/界限街.md "wikilink")[清朝土地](../Page/清朝.md "wikilink")，聚居不少以打石為業的[客家人](../Page/客家人.md "wikilink")，於疫症期間從[惠陽迎來三太子神像出巡](../Page/惠陽.md "wikilink")，其後疫症減退，因而於[光緒廿四年](../Page/光緒.md "wikilink")（1898年）集資建廟以保佑街坊健康。

三太子宮為傳統二進建築物，大門石額題「三太子宮」，大門對聯：「驅除癘疫何神也
功德生民則祀之」，擋中上書「至聖至靈」，對聯：「聖德巍巍宇內羣生咸沾雨露　神恩蕩蕩國中黎庶盡沐威光」，主祀哪吒三太子，配祀[觀音及](../Page/觀音.md "wikilink")[包公](../Page/包公.md "wikilink")，另供奉[太歲及](../Page/太歲.md "wikilink")[金花娘娘](../Page/金花娘娘.md "wikilink")。

深水埗北帝廟由該區漁民興建，原建於海旁，於1920年拆遷移到三太子宮旁，北帝廟供奉[北帝](../Page/北帝.md "wikilink")、[玄奘法師](../Page/玄奘.md "wikilink")（唐三藏）及[文昌帝君](../Page/文昌帝君.md "wikilink")。三太子宮外有小廟供奉[城隍](../Page/城隍.md "wikilink")。

北帝誕及三太子誕分別在每年[農曆三月初三及三月十八日舉行](../Page/農曆.md "wikilink")。

[File:HK_SamTaiTzeTemple_Front.JPG|三太子廟正門](File:HK_SamTaiTzeTemple_Front.JPG%7C三太子廟正門)
[File:HK_ShamShuiPoPakTaiTemple_Front.JPG|深水埗北帝廟正門](File:HK_ShamShuiPoPakTaiTemple_Front.JPG%7C深水埗北帝廟正門)
[File:HK_SamTaiTzeTemple_RoofDeco.JPG|精美脊飾](File:HK_SamTaiTzeTemple_RoofDeco.JPG%7C精美脊飾)

## 交通

<div class="NavFrame collapsed" style="background-color: yellow;clear: no; border: 1px solid #999; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: yellow; margin: 0 auto; padding: 0 10px; text-align: center; font-weight: bold;">

交通路線列表

</div>

<div class="NavContent" style="background-color: yellow;margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{荃灣綫色彩}}">█</font>[荃灣綫](../Page/荃灣綫.md "wikilink")：[深水埗站](../Page/深水埗站.md "wikilink")

<!-- end list -->

  - [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - [專線小巴](../Page/專線小巴.md "wikilink")

</div>

</div>

## 參見

  - [深水埗天后廟](../Page/深水埗天后廟.md "wikilink")
  - [哪吒](../Page/哪吒.md "wikilink")

## 参考文献

  - 《香港市區文化之旅》，蕭國健、沈思 編著，萬里機構，ISBN 962-14-2090-3

## 外部連結

  - [華人廟宇委員會：深水埗三太子及北帝廟](http://www.ctc.org.hk/b5/directcontrol/temple14.asp)
  - [福山堂：三太子廟](http://www.fushantang.com/1005/e1027.html)

[三](../Category/香港廟宇.md "wikilink")
[Category:哪吒廟](../Category/哪吒廟.md "wikilink")
[Category:香港二級歷史建築](../Category/香港二級歷史建築.md "wikilink")
[Category:深水埗](../Category/深水埗.md "wikilink")
[Category:香港北帝廟](../Category/香港北帝廟.md "wikilink")