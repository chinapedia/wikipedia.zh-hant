**天橋立車站**（）是一位於[日本](../Page/日本.md "wikilink")[京都府](../Page/京都府.md "wikilink")[宮津市字文殊](../Page/宮津市.md "wikilink")，由[北近畿丹後鐵道](../Page/北近畿丹後鐵道.md "wikilink")（）所經營的[鐵路車站](../Page/鐵路車站.md "wikilink")。天橋立車站是北近畿丹後鐵道所屬的[宮津線之沿線車站](../Page/宮津線.md "wikilink")，站名源自於所在地附近、名列[日本三景之一的著名觀光勝地](../Page/日本三景.md "wikilink")——[天橋立](../Page/天橋立.md "wikilink")。在2000年時，該站曾入選第一回的[近畿車站百選](../Page/近畿車站百選.md "wikilink")（）。

由於是名景天橋立的主要進出門戶，為服務來此觀光的旅客，每日都有自[京都](../Page/京都市.md "wikilink")、[大阪等都市發車的](../Page/大阪市.md "wikilink")[特急列車在天橋立停靠](../Page/特急.md "wikilink")，例如由[京都發車的](../Page/京都車站.md "wikilink")「[橋立](../Page/橋立號列車.md "wikilink")」，[新大阪發車的](../Page/新大阪車站.md "wikilink")「[東方白鸛](../Page/東方白鸛號列車.md "wikilink")」（隸屬於[JR西日本](../Page/JR西日本.md "wikilink")）。除此之外，自2013年起北近畿丹後鐵道也新增由[西舞鶴站開出的特別觀光列車](../Page/西舞鶴站.md "wikilink")[丹後赤松號](../Page/丹後赤松號列車.md "wikilink")（）和[青松號](../Page/丹後青松號列車.md "wikilink")（）\[1\]，部分途經[福知山站的列車會在](../Page/福知山站.md "wikilink")[宮津站開始以倒車方式駛進此站](../Page/宮津站.md "wikilink")。由於這些列車是由近畿丹後鐵道與JR西日本兩家公司合作、互相駛入對方路線的直通運行列車，因此在天橋立車站內仍可以見到[JR系統的票務窗口](../Page/JR.md "wikilink")、[綠窗口](../Page/綠窗口.md "wikilink")（），是非常稀有、在[第三部門鐵路公司的車站內還設有綠窗口的例子](../Page/第三部門.md "wikilink")。

車站內設有小賣部和遊客中心。

## 車站結構

此站是設有側式、島式複合型2面3線月台與2條[留置線](../Page/留置線.md "wikilink")，並可[列車交會和待避的](../Page/列車交會.md "wikilink")[地面車站](../Page/地面車站.md "wikilink")。側式3號月台側是站舍，島式1、2號月台可通過樓梯的跨線橋或升降機的跨線橋聯絡。此站是KTR線內15個有人站之一，是KTR的直營站。

### 月台配置

<table>
<thead>
<tr class="header">
<th><p>月台</p></th>
<th><p>路線</p></th>
<th><p>方向</p></th>
<th><p>目的地</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p>宮豐線</p></td>
<td><p>上行</p></td>
<td><p><a href="../Page/宮津站.md" title="wikilink">宮津</a>、<a href="../Page/西舞鶴站.md" title="wikilink">西舞鶴</a>、<a href="../Page/福知山站.md" title="wikilink">福知山方向</a></p></td>
<td><p>只限一部分列車</p></td>
</tr>
<tr class="even">
<td><p>下行</p></td>
<td><p>、、<a href="../Page/豐岡站_(兵庫縣).md" title="wikilink">豐岡方向</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p>宮豐線</p></td>
<td><p>下行</p></td>
<td><p>網野、久美濱、豐岡方向</p></td>
<td><p>通常在此月台</p></td>
</tr>
<tr class="even">
<td><p>上行</p></td>
<td><p>宮津、西舞鶴、福知山方向</p></td>
<td><p>此站始發的一部分</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p>宮豐線</p></td>
<td><p>上行</p></td>
<td><p>宮津、西舞鶴、福知山方向</p></td>
<td><p>通常在此月台</p></td>
</tr>
</tbody>
</table>

## 相鄰車站

  - WILLER TRAINS（京都丹後鐵道）
    宮豐線（宮津線）
      - 特急「[橋立](../Page/橋立號列車.md "wikilink")」「」停靠站
    <!-- end list -->
      -
        快速「丹後路」（只限下行運行）
          -
            **天橋立** → （T17）
        普通
          -
            [宮津](../Page/宮津站.md "wikilink")（14）－**天橋立（T15）**－（T16）

## 參考

## 外部連結

  - [天橋立車站（北近畿丹後鐵道）](https://web.archive.org/web/20080203043836/http://www.ktr-tetsudo.jp/route-station/hashidate_st.html)

[manohashidate](../Category/日本鐵路車站_A.md "wikilink")
[Category:京都府鐵路車站](../Category/京都府鐵路車站.md "wikilink")
[Category:宮豐線車站](../Category/宮豐線車站.md "wikilink")
[Category:1925年启用的铁路车站](../Category/1925年启用的铁路车站.md "wikilink")
[Category:宮津市](../Category/宮津市.md "wikilink")
[Category:以公園命名的鐵路車站](../Category/以公園命名的鐵路車站.md "wikilink")
[Category:西日本旅客鐵道廢站](../Category/西日本旅客鐵道廢站.md "wikilink")

1.  [京都旅人／關西最美觀光列車－「青松號」、「赤松號」](http://www.ettoday.net/news/20131217/306999.htm)《ETtoday》