**邯山区**，中国[河北省](../Page/河北省.md "wikilink")[邯郸市的一个区](../Page/邯郸市.md "wikilink")。邯郸由邯山而得名。

## 历史

3000多年前，邯郸是[殷商之京畿](../Page/殷商.md "wikilink")。战国时期是[赵都的重要组成部分](../Page/赵.md "wikilink")，也是当时最负盛名的冶铁中心。[秦](../Page/秦.md "wikilink")[汉而后](../Page/汉.md "wikilink")，逐渐凋零。建国后，邯山区的行政区划几经更易。1980年10月，邯山区建制。1986年4月，郊区建制撤消，所辖[马头镇](../Page/马头镇.md "wikilink")、[马庄乡划归邯山区辖](../Page/马庄乡.md "wikilink")。

## 行政区划

下辖11个[街道办事处](../Page/街道办事处.md "wikilink")、5个[镇](../Page/行政建制镇.md "wikilink")、5个[乡](../Page/乡_\(中华人民共和国\).md "wikilink")：

。

## 官方网站

  - [邯山区人民政府](https://web.archive.org/web/20100612070734/http://hsq.hd.gov.cn/)

## 参考文献

[邯山区](../Page/category:邯山区.md "wikilink")
[区](../Page/category:邯郸区县市.md "wikilink")
[邯郸](../Page/category:河北市辖区.md "wikilink")