[GreeceNumberedPerepheries.png](https://zh.wikipedia.org/wiki/File:GreeceNumberedPerepheries.png "fig:GreeceNumberedPerepheries.png")
[GreeceDioikiseis.png](https://zh.wikipedia.org/wiki/File:GreeceDioikiseis.png "fig:GreeceDioikiseis.png")
**希臘**目前的**行政區劃**生效於2011年。由七個管理區、13個大區、和325個自治市三個級別組成。另外[聖山是一個神權自治共和國](../Page/阿索斯山.md "wikilink")-[阿苏斯神权共和国](../Page/阿苏斯神权共和国.md "wikilink")。此前，希腊行政区划分为13个大区，54个州及1033个自治市及社区。

## 管理区与大区

| 希腊管理区与大区                                                      |
| ------------------------------------------------------------- |
| 管理区                                                           |
| align ="center"|阿提卡                                           |
| rowspan="2" align ="center"|马其顿-色雷斯                           |
| align ="center"|[东马其顿和色雷斯](../Page/东马其顿和色雷斯.md "wikilink")    |
| rowspan="2" align ="center"|伊庇鲁斯-西马其顿                         |
| align ="center"|[西马其顿](../Page/西马其顿.md "wikilink")            |
| rowspan="2" align ="center"|色萨利-中希腊                           |
| align ="center"|[中希腊](../Page/中希腊.md "wikilink")              |
| rowspan="3" align ="center"|伯罗奔尼撒-西希腊-爱奥尼亚群岛                  |
| align ="center"|[伯罗奔尼撒](../Page/伯罗奔尼撒_\(大区\).md "wikilink")   |
| align ="center"|[愛奧尼亞群島](../Page/愛奧尼亞群島_\(大區\).md "wikilink") |
| rowspan="2" align ="center"|爱琴群岛                              |
| align ="center"|[南爱琴](../Page/南爱琴.md "wikilink")              |
| align ="center"|克里特                                           |

## 参考文献

{{-}}

[Greece](../Category/欧洲国家行政区划.md "wikilink")
[希臘行政區劃](../Category/希臘行政區劃.md "wikilink")