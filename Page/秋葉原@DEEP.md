《**秋葉原@DEEP**》（，ISBN
4-16-323530-2），是[日本](../Page/日本.md "wikilink")[作家](../Page/作家.md "wikilink")[石田衣良所創作的一部以日本](../Page/石田衣良.md "wikilink")[東京都知名電氣街](../Page/東京都.md "wikilink")[秋葉原為背景的長篇](../Page/秋葉原.md "wikilink")[小說](../Page/小說.md "wikilink")。此小說於2004年連載於《[文藝春秋](../Page/文藝春秋.md "wikilink")》的別冊。並且在2006年6月被改編為[電視劇由](../Page/電視劇.md "wikilink")[TBS播映](../Page/TBS.md "wikilink")；亦改編為[漫畫](../Page/漫畫.md "wikilink")，由執筆，但因人物與小說間的變動太大而在日本有批評的聲浪；[電影版由](../Page/電影.md "wikilink")[源孝志執導](../Page/源孝志.md "wikilink")，於同年9月上映。

## 情節概要

3個常在秋葉原一帶活動的[御宅族](../Page/御宅族.md "wikilink")，由於是某一個[聊天室](../Page/聊天室.md "wikilink")[網站的常客](../Page/網站.md "wikilink")，因而結識。透過該聊天室站長的牽線，又另外有3人加入。這六人各有所專長，但也都各有缺陷。這六人決定合組一家[IT](../Page/IT.md "wikilink")[公司](../Page/公司.md "wikilink")，稱為「秋葉原@DEEP」。起初以接案製作網站獲得收入，後來開始發展一套具有[人工智慧能力的](../Page/人工智慧.md "wikilink")[搜尋引擎](../Page/搜尋引擎.md "wikilink")。這套搜尋引擎經由一名[駐日美軍的](../Page/駐日美軍.md "wikilink")[部落格所披露介紹後](../Page/部落格.md "wikilink")，聲名大噪，使用者增多。但是也引起一家稱為「數位資本」（簡稱「數資」）的大型IT公司注意，該公司起先出了高價欲收購其搜尋引擎，但遭拒絕後開始用各種手段要奪取該搜尋引擎以納入自己的網站。雙方因此展開了一場爭鬥。

## 小說特點

此部小說較偏向[科幻類小說](../Page/科幻.md "wikilink")，為作者以東京都為題材所寫的作品之一（前作《[池袋西口公園](../Page/池袋西口公園_\(小說\).md "wikilink")》即以[豐島區的](../Page/豐島區.md "wikilink")[池袋為背景](../Page/池袋.md "wikilink")），文中有時會以搜尋引擎為第一人稱視角敘述，將寫出該搜尋引擎的「秋葉原@DEEP」六人稱為「父母」。

## 人物

### 秋葉原@DEEP公司成員

  -
    <small>以[台灣的木馬文化出版之中譯版小說](../Page/台灣.md "wikilink")（ISBN
    986-7475-90-9）譯名為主，括號內為原名以及電視劇版的飾演者。以下人物介紹概以小說內情節為準。</small>
  -
    本名嶋浩志，有很好的寫作能力，但卻有[口吃的毛病](../Page/口吃.md "wikilink")，常需以電腦打字的方式才能順利與人交談。
  -
    本名宮前定繼，精通[平面設計](../Page/平面設計.md "wikilink")，但有強烈的[潔癖](../Page/潔癖.md "wikilink")，雙手總是要戴上手套。
  -
    本名方南驅，精通[數位音樂](../Page/數位音樂.md "wikilink")，看到閃光物就會[癲癇發作](../Page/癲癇.md "wikilink")，發作時像是[當機一樣一動也不動](../Page/當機.md "wikilink")。
  -
    精通[格鬥](../Page/格鬥.md "wikilink")，喜好軍服[cosplay](../Page/cosplay.md "wikilink")，在一家叫作「小茜」的cosplay咖啡廳擔任店員。
  -
    本名清瀨泉虫，因[父親的工作因素](../Page/父親.md "wikilink")，從小就接觸[電腦](../Page/電腦.md "wikilink")，有[程式設計及](../Page/程式設計.md "wikilink")[駭客本領](../Page/駭客.md "wikilink")。
  -
    本名牛久昇，精通[法律](../Page/法律.md "wikilink")，總是穿著同一套西裝。但曾是[隱蔽青年](../Page/隱蔽青年.md "wikilink")，留著如[達摩禪師一樣的長](../Page/達摩禪師.md "wikilink")[鬍子而得名](../Page/鬍子.md "wikilink")。

### 其他人物

  - 結衣（，Yui）

<!-- end list -->

  -
    本名故千川結，聊天室的站長，曾因[藥物濫用數度](../Page/藥物.md "wikilink")[自殺未遂](../Page/自殺.md "wikilink")，但在聊天室內卻有如心理輔導者的角色。在與秋葉原@DEEP六人見面之前一刻因藥物服用過量而死。

<!-- end list -->

  - 中込威

<!-- end list -->

  -
    「數資」的董事長。

<!-- end list -->

  - 強森（Johnson Joint）

<!-- end list -->

  -
    是駐日美軍，任務是搜集日本的最新科技資料匯報給[美國](../Page/美國.md "wikilink")，在他的部落格介紹了秋葉原@DEEP的搜尋引擎。

<!-- end list -->

  - 遠阪直樹

<!-- end list -->

  -
    「數資」的資深程式設計師之一。

## 電視劇

從2006年6月19日起，在日本[TBS電視台播出](../Page/TBS.md "wikilink")，共有11集（加上幕後花絮為12集）。

### 工作人員

  - 原作 - [石田衣良](../Page/石田衣良.md "wikilink")
  - 劇本 - [河原雅彦](../Page/河原雅彦.md "wikilink")、川邊優子
  - 導演 - [大根仁](../Page/大根仁.md "wikilink")
  - 製作 -
    [Geneon](../Page/Geneon.md "wikilink")、[讀賣廣告社](../Page/讀賣廣告社.md "wikilink")
  - 企劃協力 - 文藝春秋

### 演員

  - 阿頁 - [風間俊介](../Page/風間俊介.md "wikilink")
  - 阿欄 - [生田斗真](../Page/生田斗真.md "wikilink")
  - 太鼓 - [星野源](../Page/星野源.md "wikilink")
  - 小光 - [小阪由佳](../Page/小阪由佳.md "wikilink")
  - 泉虫 - [松嶋初音](../Page/松嶋初音.md "wikilink")
  - 達磨 - [日村勇紀](../Page/日村勇紀.md "wikilink")
  - 結衣 - [本上真奈美](../Page/本上真奈美.md "wikilink")
  - 中込威 - [北村一輝](../Page/北村一輝.md "wikilink")

## 電影

2006年9月2日由[東映公司在日本發行上映](../Page/東映.md "wikilink")。導演為[源孝志](../Page/源孝志.md "wikilink")。演員包括[成宮寬貴](../Page/成宮寬貴.md "wikilink")、[山田優](../Page/山田優.md "wikilink")、[忍成修吾](../Page/忍成修吾.md "wikilink")、[荒川良良](../Page/荒川良良.md "wikilink")、[三浦春馬](../Page/三浦春馬.md "wikilink")、[佐佐木藏之介](../Page/佐佐木藏之介.md "wikilink")、[萩原聖人等](../Page/萩原聖人.md "wikilink")。

## 外部連結

  - [文藝春秋小説紹介ページ](http://www.bunshun.co.jp/book_db/html/3/23/53/4163235302.shtml)
    小說
  - [週刊コミックバンチ漫画紹介ページ](https://web.archive.org/web/20070619041403/http://www.comicbunch.com/comic_info/akiba_deep/index.html)
    漫畫
  - [テレビドラマ版公式サイト](http://www.akihabaradeep.com/) 電視劇
  - [映画版公式サイト](https://web.archive.org/web/20060719033813/http://www.a-deep.jp/)
    電影
  - [Yam樂多網誌](http://blog.yam.com/ecus2005/archives/1364594.html) 中譯本部落格

[Category:日本小說](../Category/日本小說.md "wikilink")
[Category:日本漫畫作品](../Category/日本漫畫作品.md "wikilink")
[Category:搞笑漫畫](../Category/搞笑漫畫.md "wikilink")
[Category:2006年日本電視劇集](../Category/2006年日本電視劇集.md "wikilink")
[Category:日本小說改編電視劇](../Category/日本小說改編電視劇.md "wikilink")
[Category:2006年電影](../Category/2006年電影.md "wikilink")
[Category:日本電影作品](../Category/日本電影作品.md "wikilink")
[Category:東映電影](../Category/東映電影.md "wikilink")
[Category:日本小說改編電影](../Category/日本小說改編電影.md "wikilink")
[Category:秋葉原背景作品](../Category/秋葉原背景作品.md "wikilink")