**[马来西亚](../Page/马来西亚.md "wikilink")**（）是位于[东南亚的一个](../Page/东南亚.md "wikilink")[联邦](../Page/联邦制.md "wikilink")[议会](../Page/西敏制.md "wikilink")[君主立宪制国家](../Page/君主立宪制.md "wikilink")，在马来西亚当地简称为大马，部分國外地区简称为马国。国家整体依地理位置可区分为两大部分：

  - 位于[马来西亚半岛的部分称为](../Page/马来西亚半岛.md "wikilink")**[西马](../Page/西马.md "wikilink")**，又细分为[中马](../Page/中马.md "wikilink")、[北马](../Page/北马.md "wikilink")、[南马和东海岸](../Page/南马.md "wikilink")4部分，半岛上有11个州属和2个[联邦直辖区](../Page/联邦直辖区.md "wikilink")；
  - 位于[婆罗洲的部分称为](../Page/婆罗洲.md "wikilink")**[东马](../Page/东马.md "wikilink")**，有[沙巴和](../Page/沙巴.md "wikilink")[砂拉越](../Page/砂拉越.md "wikilink")2个州属以及1个联邦直辖区。

根据[联邦宪法](../Page/马来西亚宪法.md "wikilink")，马来西亚由十三个州（）和三个联邦直辖区（）组成，州的界线是以马来西亚成立日（，1963年9月16日）之时为准，任何意图将州界线改变的决定必须先获得有关州政府及[统治者会议的同意方为有效](../Page/统治者会议.md "wikilink")。

自马来西亚成立以来，第一级行政区的变更有：

  - 1966年宪法(修正)法案－[新加坡独立](../Page/新加坡.md "wikilink")
  - 1973年宪法(修正)(2)法案－[吉隆坡划为联邦直辖区](../Page/吉隆坡.md "wikilink")
  - 1984年宪法(修正)(2)法案－[纳闽划为联邦直辖区](../Page/纳闽.md "wikilink")
  - 2001年宪法(修正)法案－[布城划为联邦直辖区](../Page/布城.md "wikilink")

## 州属

州和联邦直辖区是马来西亚的第一级行政区划，现时在马来西亚共有13州和3联邦直辖区，根据历史背景和地理位置的差异，州又有马来州属、海峡州属和婆罗洲州属的分别。州拥有独立的[立法权和](../Page/立法权.md "wikilink")[行政权](../Page/行政权.md "wikilink")，州内的3个主要机构为州议会、州行政议会或州内阁和州政府秘书处，除了州政府以下机构及官联机构外，联邦政府机构的人事调动是由[公共服务委员会负责](../Page/公共服务委员会.md "wikilink")。至于[司法方面](../Page/马来西亚司法.md "wikilink")，半岛州属隶属[马来亚高等法庭](../Page/马来亚高等法庭.md "wikilink")；婆罗洲州属隶属[沙巴与砂拉越高等法庭](../Page/沙巴与砂拉越高等法庭.md "wikilink")，每个州属皆设有一处高等法庭。

州的立法机构为[州议会](../Page/马来西亚州议会.md "wikilink")（），每个州属将根据人口和发展程度的不同区划若干州选区，通过[选举制度直选州议员](../Page/选举制度.md "wikilink")，获得最多议席的政党将成为执政党组织州行政议会或州内阁，州议会的最高负责人为[议长](../Page/议长.md "wikilink")。

州的行政机构为[州行政议会](../Page/州行政议会.md "wikilink")（；半岛州属）或[州内阁](../Page/州内阁.md "wikilink")（；婆罗洲州属），一般称为州政府，它是由州议会中占有最多议席的政党组成。州行政机构的最高负责人为[州务大臣或](../Page/州务大臣.md "wikilink")[首席部长](../Page/首席部长.md "wikilink")，之下设有4至10个行政议员或部长职，州政府同时拥有[土地资源和](../Page/土地资源.md "wikilink")[地方政府的支配权](../Page/地方政府.md "wikilink")。

州政府秘书处（）隶属州政府，主要负责州政府的行政管理工作，同时也负责州政府与联邦政府以及州政府与地方政府之间的协调工作。

联邦直辖区是由联邦政府直接管辖的属地，其立法机构如市政局及行政首长皆由[联邦直辖区部委任](../Page/马来西亚内阁.md "wikilink")。

### 基本概况

\-{zh-hans:; zh-hant:;}-

| 行政区划                                                                                                                                                                                                                          | 首府                                   | 皇城                                   | 地理位置                                           | 代码                                           | 土地        | 人口       |
| ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------ | ------------------------------------ | ---------------------------------------------- | -------------------------------------------- | --------- | -------- |
|                                                                                                                                                                                                                               |                                      | JPJ                                  | [ISO 3166-2](../Page/ISO_3166-2.md "wikilink") | [FIPS 10-4](../Page/FIPS_10-4.md "wikilink") | 面积（km²）   | 比率（%）    |
| [Flag_of_Selangor.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Selangor.svg "fig:Flag_of_Selangor.svg") [雪兰莪州](../Page/雪兰莪州.md "wikilink")                                                                               | [莎阿南](../Page/莎阿南.md "wikilink")     | [巴生市](../Page/巴生市.md "wikilink")     | [中马](../Page/中马.md "wikilink")                 | SGR                                          | SL        | B        |
| [Flag_of_Perak.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Perak.svg "fig:Flag_of_Perak.svg") [霹雳州](../Page/霹雳州.md "wikilink")                                                                                          | [怡保](../Page/怡保.md "wikilink")       | [江沙](../Page/江沙.md "wikilink")       | [北马](../Page/北马.md "wikilink")                 | PRK                                          | PK        | A        |
| [Flag_of_Pahang.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Pahang.svg "fig:Flag_of_Pahang.svg") [彭亨州](../Page/彭亨州.md "wikilink")                                                                                       | [关丹](../Page/关丹.md "wikilink")       | [北根](../Page/北根.md "wikilink")       | 东海岸                                            | PHG                                          | PH        | C        |
| [Flag_of_Johor.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Johor.svg "fig:Flag_of_Johor.svg") [柔佛州](../Page/柔佛州.md "wikilink")                                                                                          | [新山](../Page/新山.md "wikilink")       | [麻坡](../Page/麻坡.md "wikilink")       | [南马](../Page/南马.md "wikilink")                 | JHR                                          | JH        | J        |
| [Flag_of_Kedah.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Kedah.svg "fig:Flag_of_Kedah.svg") [吉打州](../Page/吉打州.md "wikilink")                                                                                          | [亚罗士打](../Page/亚罗士打.md "wikilink")   | [安南武吉](../Page/安南武吉.md "wikilink")   | [北马](../Page/北马.md "wikilink")                 | KDH                                          | KH        | K        |
| [Flag_of_Kelantan.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Kelantan.svg "fig:Flag_of_Kelantan.svg") [吉兰丹州](../Page/吉兰丹州.md "wikilink")                                                                               | [哥打峇鲁](../Page/哥打峇鲁.md "wikilink")   | [哥打峇鲁](../Page/哥打峇鲁.md "wikilink")   | 东海岸                                            | KTN                                          | KN        | D        |
| [Flag_of_Terengganu.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Terengganu.svg "fig:Flag_of_Terengganu.svg") [登嘉楼州](../Page/登嘉楼州.md "wikilink")                                                                         | [瓜拉登嘉楼](../Page/瓜拉登嘉楼.md "wikilink") | [瓜拉登嘉楼](../Page/瓜拉登嘉楼.md "wikilink") | 东海岸                                            | TRG                                          | TE        | T        |
| [Flag_of_Negeri_Sembilan.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Negeri_Sembilan.svg "fig:Flag_of_Negeri_Sembilan.svg") [森美兰州](../Page/森美兰州.md "wikilink")                                                         | [芙蓉市](../Page/芙蓉市.md "wikilink")     | [神安池](../Page/神安池.md "wikilink")     | [南马](../Page/南马.md "wikilink")                 | NSN                                          | NS        | N        |
| [Flag_of_Perlis.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Perlis.svg "fig:Flag_of_Perlis.svg") [玻璃市州](../Page/玻璃市州.md "wikilink")                                                                                     | [加央](../Page/加央.md "wikilink")       | [亚娄](../Page/亚娄.md "wikilink")       | [北马](../Page/北马.md "wikilink")                 | PLS                                          | PL        | R        |
| [Flag_of_Penang_(Malaysia).svg](https://zh.wikipedia.org/wiki/File:Flag_of_Penang_\(Malaysia\).svg "fig:Flag_of_Penang_(Malaysia).svg") [槟城州](../Page/槟城州.md "wikilink")                                                   | [乔治市](../Page/乔治市.md "wikilink")     |                                      |                                                |                                              |           |          |
| [北马](../Page/北马.md "wikilink")                                                                                                                                                                                                | PNG                                  | PG                                   | P                                              | MY-07                                        | MY09      | 1,046    |
| [Flag_of_Malacca.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Malacca.svg "fig:Flag_of_Malacca.svg") [马六甲州](../Page/马六甲州.md "wikilink")                                                                                  | [马六甲市](../Page/马六甲市.md "wikilink")   |                                      |                                                |                                              |           |          |
| [南马](../Page/南马.md "wikilink")                                                                                                                                                                                                | MLK                                  | ME                                   | M                                              | MY-04                                        | MY04      | 1,650    |
| [Flag_of_Sabah.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Sabah.svg "fig:Flag_of_Sabah.svg") [沙巴州](../Page/沙巴州.md "wikilink")                                                                                          | [亚庇](../Page/亚庇.md "wikilink")       |                                      |                                                |                                              |           |          |
| [东马](../Page/东马.md "wikilink")                                                                                                                                                                                                | SBH                                  | SA                                   | S                                              | MY-12                                        | MY16      | 76,115   |
| [Flag_of_Sarawak.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Sarawak.svg "fig:Flag_of_Sarawak.svg") [砂拉越州](../Page/砂拉越州.md "wikilink")                                                                                  | [古晋](../Page/古晋.md "wikilink")       |                                      |                                                |                                              |           |          |
| [东马](../Page/东马.md "wikilink")                                                                                                                                                                                                | SRW                                  | SK                                   | Q                                              | MY-13                                        | MY11      | 124,450  |
| [Flag_of_the_Federal_Territory_-_Malaysia.png](https://zh.wikipedia.org/wiki/File:Flag_of_the_Federal_Territory_-_Malaysia.png "fig:Flag_of_the_Federal_Territory_-_Malaysia.png") [联邦直辖区](../Page/联邦直辖区.md "wikilink") |                                      |                                      |                                                |                                              |           |          |
|                                                                                                                                                                                                                               |                                      |                                      |                                                |                                              |           |          |
|                                                                                                                                                                                                                               |                                      |                                      |                                                |                                              |           |          |
|                                                                                                                                                                                                                               |                                      |                                      |                                                |                                              |           |          |
| WP                                                                                                                                                                                                                            |                                      |                                      |                                                |                                              |           |          |
|                                                                                                                                                                                                                               |                                      |                                      |                                                |                                              |           |          |
| MY14                                                                                                                                                                                                                          | \-                                   | \-                                   | \-                                             | \-                                           | \-        |          |
| [Flag_of_Kuala_Lumpur_Malaysia.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Kuala_Lumpur_Malaysia.svg "fig:Flag_of_Kuala_Lumpur_Malaysia.svg") [吉隆坡联邦直辖区](../Page/吉隆坡.md "wikilink")                                   |                                      |                                      |                                                |                                              |           |          |
|                                                                                                                                                                                                                               |                                      |                                      |                                                |                                              |           |          |
| [中马](../Page/中马.md "wikilink")                                                                                                                                                                                                | KUL                                  | KL                                   | W、V                                            | MY-14                                        |           |          |
| 243                                                                                                                                                                                                                           | 0.1                                  | 1,887,674                            | 6.8                                            | 7747.5                                       |           |          |
| [Putrajaya_flag.gif](https://zh.wikipedia.org/wiki/File:Putrajaya_flag.gif "fig:Putrajaya_flag.gif") [布城联邦直辖区](../Page/布城.md "wikilink")                                                                                     |                                      |                                      |                                                |                                              |           |          |
|                                                                                                                                                                                                                               |                                      |                                      |                                                |                                              |           |          |
| [中马](../Page/中马.md "wikilink")                                                                                                                                                                                                | PJY                                  |                                      |                                                |                                              |           |          |
| Putrajaya、F                                                                                                                                                                                                                   | MY-16                                |                                      |                                                |                                              |           |          |
| 46                                                                                                                                                                                                                            | \>0.1                                | 50,000                               | 0.2                                            | 1087.0                                       |           |          |
| [Flag_of_Labuan.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Labuan.svg "fig:Flag_of_Labuan.svg") [纳闽联邦直辖区](../Page/纳闽.md "wikilink")                                                                                    |                                      |                                      |                                                |                                              |           |          |
|                                                                                                                                                                                                                               |                                      |                                      |                                                |                                              |           |          |
| [东马](../Page/东马.md "wikilink")                                                                                                                                                                                                | LBN                                  |                                      |                                                |                                              |           |          |
| L                                                                                                                                                                                                                             | MY-15                                | MY15                                 | 92                                             | \>0.1                                        | 85,000    | 0.3      |
|                                                                                                                                                                                                                               | **总计**                               | **329,847**                          | **100.0**                                      | **27,730,000**                               | **100.0** | **83.0** |

<div style="margin-left:10px; font-size:90%;">

1\. FIPS 10-4代码中并没有使用MY10，但在FIPS 10-3代码是用来表示沙巴州。
2\. 在[FIPS代码中](../Page/FIPS.md "wikilink")，MY14（Wilayah
Persekutuan）同时用以表示吉隆坡和布特拉再城。
3\. 2007年统计数据，需要更新。

</div>

## 州以下行政区划

州以下的行政区划在不同的州属有不同的架构，一般分为：

### 省（Bahagian）

[省是沙巴和砂拉越州以下的行政区划](../Page/马来西亚省份.md "wikilink")，在砂拉越，省的最高机构为[专署](../Page/专署.md "wikilink")，专署的功能是对省内各县的建设发展做出规划与协调，最高负责人为[专员](../Page/专员.md "wikilink")；而在沙巴，省并不设有独立的最高机构，建设发展的规划与协调由各县自行负责。

### 县（Daerah/Jajahan）

县是半岛州及东马省以下的行政区划，县的主要机构为县署及土地局和[地方政府](../Page/地方政府_\(马来西亚\).md "wikilink")，县署的功能是对县内各区的建设发展做出规划与协调，最高负责人为[县长](../Page/县长.md "wikilink")；地方政府根据各县的人口与发展程度不同，可分为[市政局](../Page/市政局.md "wikilink")/[市政厅](../Page/市政厅.md "wikilink")、[市议会和](../Page/市议会.md "wikilink")[县议会](../Page/县议会.md "wikilink")，地方政府的功能与县署相同，差异在于县署主要负责马来保留区的规划，地方政府主要负责城市发展区的规划；土地局隶属县署，主要功能是处理土地相关事务。

### 副县（Daerah kecil）

副县是县署在工作量超出负荷的情况下，在县内分划出一个副县署，副县署所管辖的区域既成为副县。

### 区（Mukim）

区（也称[巫金](../Page/巫金.md "wikilink")）是县（和副县）以下的行政区划，区的最高负责人为[区长](../Page/区长.md "wikilink")，区长办事处附设在县署下。

### 乡村（Kampung）

乡村是最低一级的行政区划，根据乡村类型的不同而有不同的行政归属，行政上归属州政府的传统甘榜、渔村、重组村和新村设有[乡村发展及安全委员会](../Page/乡村发展及安全委员会.md "wikilink")，对乡村的发展做出规划与协调；但在反对党执政的州属，联邦政府特设有[联邦发展协调委员会](../Page/联邦发展协调委员会.md "wikilink")，从中牵制乡村发展及安全委员会。

  - **传统甘榜**（）－是以自然方式形成的聚落，行政归属州政府。
  - **渔村**（）－是以自然方式形成的聚落，行政归属州政府。
  - **馬來新村**（）－是在自然灾害后，以人工迁移方式形成的聚落，行政归属州政府。
  - **華人新村**（）－是在[馬來亞緊急狀態](../Page/馬來亞緊急狀態.md "wikilink")（1948\~60年）以人工迁移方式形成的聚落，行政归属州政府。
  - **垦殖区**（）－是[联邦土地发展局在特定地区发展形成的聚落](../Page/联邦土地发展局.md "wikilink")，行政归属[乡村与区域发展部](../Page/马来西亚内阁.md "wikilink")。
  - **原住民部落**（）－是原住民的传统聚落，在西马，行政归属[乡村与区域发展部](../Page/马来西亚内阁.md "wikilink")；在东马，行政归属州政府。
  - **水上部落**（）－是沙巴水上原住民的传统聚落，行政归属州政府。
  - **非法木屋区**（）－是在政府未授权的情况下自然形成的聚落，土地行政归属州政府。
  - **种植园**（）－是有规划性在农用地段形成的聚落，土地行政归属州政府。

## 地方政府

马来西亚各州的行政区划和地方政府的设置没有直接的挂钩，主要是因为两者的权限归属不同，虽然两者都由州政府所设置，但省、县、区是州政府直属机构所管理的分区，在，中地方政府则是州委地方议会的自治管理的区域。所以会有一个县设置一个地方政府、一个县内设置多个地方政府、一个县被分置给多个地方政府、两个县并入一个地方政府等情况。地方政府根据各县的人口与发展程度不同，可分为隶属州政府的[市政局](../Page/市政局.md "wikilink")/[市政厅](../Page/市政厅.md "wikilink")、[市议会和](../Page/市议会.md "wikilink")[县议会](../Page/县议会.md "wikilink")，和隶属于联邦的特别地方政府：

| 第一级行政区划                                                                                                                                                                                        | 市政局/市政厅 | 市议会    | 县议会    | 特别地方政府 | 总计      |
| :--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | :------ | :----- | :----- | :----- | :------ |
| [Flag_of_Selangor.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Selangor.svg "fig:Flag_of_Selangor.svg") 雪兰莪州                                                                              | 2       | 6      | 4      | 0      | 12      |
| [Flag_of_Perak.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Perak.svg "fig:Flag_of_Perak.svg") 霹雳州                                                                                        | 1       | 4      | 10     | 0      | 15      |
| [Flag_of_Pahang.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Pahang.svg "fig:Flag_of_Pahang.svg") 彭亨州                                                                                     | 0       | 3      | 8      | 1      | 12      |
| [Flag_of_Johor.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Johor.svg "fig:Flag_of_Johor.svg") 柔佛州                                                                                        | 2       | 6      | 7      | 1      | 16      |
| [Flag_of_Kedah.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Kedah.svg "fig:Flag_of_Kedah.svg") 吉打州                                                                                        | 1       | 3      | 7      | 1      | 12      |
| [Flag_of_Kelantan.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Kelantan.svg "fig:Flag_of_Kelantan.svg") 吉兰丹州                                                                              | 0       | 1      | 11     | 0      | 12      |
| [Flag_of_Terengganu.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Terengganu.svg "fig:Flag_of_Terengganu.svg") 登嘉楼州                                                                        | 1       | 2      | 4      | 0      | 7       |
| [Flag_of_Negeri_Sembilan.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Negeri_Sembilan.svg "fig:Flag_of_Negeri_Sembilan.svg") 森美兰州                                                        | 0       | 3      | 5      | 0      | 8       |
| [Flag_of_Perlis.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Perlis.svg "fig:Flag_of_Perlis.svg") 玻璃市州                                                                                    | 0       | 1      | 0      | 0      | 1       |
| [Flag_of_Penang_(Malaysia).svg](https://zh.wikipedia.org/wiki/File:Flag_of_Penang_\(Malaysia\).svg "fig:Flag_of_Penang_(Malaysia).svg") 槟城州                                                 | 1       | 1      | 0      | 0      | 2       |
| [Flag_of_Malacca.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Malacca.svg "fig:Flag_of_Malacca.svg") 马六甲州                                                                                 | 1       | 3      | 0      | 0      | 4       |
| [Flag_of_Sabah.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Sabah.svg "fig:Flag_of_Sabah.svg") 沙巴州                                                                                        | 1       | 2      | 21     | 0      | 24      |
| [Flag_of_Sarawak.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Sarawak.svg "fig:Flag_of_Sarawak.svg") 砂拉越州                                                                                 | 3       | 4      | 19     | 0      | 26      |
| [Flag_of_the_Federal_Territory_-_Malaysia.png](https://zh.wikipedia.org/wiki/File:Flag_of_the_Federal_Territory_-_Malaysia.png "fig:Flag_of_the_Federal_Territory_-_Malaysia.png") 联邦直辖区 | 1       | 0      | 0      | 2      | 3       |
| **总计**                                                                                                                                                                                         | **14**  | **39** | **96** | **5**  | **154** |

### 联邦、州和地方政府比较

| 联邦、州和地方政府比较                                                                                                                                      |
| ------------------------------------------------------------------------------------------------------------------------------------------------ |
| 层级                                                                                                                                               |
| 立法机构                                                                                                                                             |
| 联邦                                                                                                                                               |
| 下议院                                                                                                                                              |
| 州属                                                                                                                                               |
| [Flag_of_Perak.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Perak.svg "fig:Flag_of_Perak.svg") 霹雳州政府                                        |
| [Flag_of_Pahang.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Pahang.svg "fig:Flag_of_Pahang.svg") 彭亨州政府                                     |
| [Flag_of_Johor.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Johor.svg "fig:Flag_of_Johor.svg") 柔佛州政府                                        |
| [Flag_of_Kedah.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Kedah.svg "fig:Flag_of_Kedah.svg") 吉打州政府                                        |
| [Flag_of_Kelantan.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Kelantan.svg "fig:Flag_of_Kelantan.svg") 吉兰丹州政府                              |
| [Flag_of_Terengganu.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Terengganu.svg "fig:Flag_of_Terengganu.svg") 登嘉楼州政府                        |
| [Flag_of_Negeri_Sembilan.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Negeri_Sembilan.svg "fig:Flag_of_Negeri_Sembilan.svg") 森美兰州政府        |
| [Flag_of_Perlis.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Perlis.svg "fig:Flag_of_Perlis.svg") 玻璃市州政府                                    |
| [Flag_of_Penang_(Malaysia).svg](https://zh.wikipedia.org/wiki/File:Flag_of_Penang_\(Malaysia\).svg "fig:Flag_of_Penang_(Malaysia).svg") 槟城州政府 |
| [Flag_of_Malacca.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Malacca.svg "fig:Flag_of_Malacca.svg") 马六甲州政府                                 |
| [Flag_of_Sabah.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Sabah.svg "fig:Flag_of_Sabah.svg") 沙巴州政府                                        |
| [Flag_of_Sarawak.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Sarawak.svg "fig:Flag_of_Sarawak.svg") 砂拉越州政府                                 |
| 地方                                                                                                                                               |
| [Putrajaya_flag.gif](https://zh.wikipedia.org/wiki/File:Putrajaya_flag.gif "fig:Putrajaya_flag.gif") 布城联邦直辖区政府（行政中心）                            |
| [Flag_of_Labuan.svg](https://zh.wikipedia.org/wiki/File:Flag_of_Labuan.svg "fig:Flag_of_Labuan.svg") 纳闽联邦直辖区政府（离岸金融中心）                         |
| [Drapeau_blanc.svg](https://zh.wikipedia.org/wiki/File:Drapeau_blanc.svg "fig:Drapeau_blanc.svg") 地方政府                                          |
| 市议会                                                                                                                                              |
| 县议会                                                                                                                                              |

<div style="margin-left:10px; font-size:90%;">

**摘要：**

1.  在1950年代中期，[英国殖民政府开始在](../Page/大英帝国.md "wikilink")[英属马来亚实验性地举行国家](../Page/英属马来亚.md "wikilink")、州和地方的全面性[选举以为](../Page/选举.md "wikilink")[马来亚联合邦的独立做准备](../Page/马来亚联合邦.md "wikilink")；50年代末至60年代初，独立政府开始落实法令逐步推行各级选举；[马印对抗之后](../Page/马印对抗.md "wikilink")，中央以地方政府通敌卖国、过分管制和管理失当等理由中止地方政府选举的运作，并引以[委任制取代之](../Page/委任制.md "wikilink")。
2.  马来西亚名义上为[联邦制国家](../Page/联邦制.md "wikilink")，却普遍被评为是个高度[中央集权制国家](../Page/中央集权制.md "wikilink")，州政府虽然在[土地资源及地方政府运作上拥有](../Page/土地资源.md "wikilink")[自主权](../Page/自主权.md "wikilink")，但在实际运作中往往也受到联邦法令及中央特设理事会的牵制。
3.  1974年，中央政府以国家首都必须直接由中央管辖为由，通过[联邦直辖区法令接手管理吉隆坡地方政府](../Page/联邦直辖区法令.md "wikilink")。
4.  [最高元首是经由](../Page/马来西亚最高元首.md "wikilink")[统治者议会](../Page/统治者议会.md "wikilink")（Majlis
    Raja-raja）遴选选出。
5.  [苏丹](../Page/苏丹_\(称谓\).md "wikilink")、[嚴端和](../Page/嚴端.md "wikilink")[拉惹为世袭统治者](../Page/拉惹.md "wikilink")。
6.  [州元首是由最高元首所委任](../Page/州元首.md "wikilink")。
7.  [上议员](../Page/上议员.md "wikilink")（ADN; Ahli Dewan
    Negara）是由最高元首和州政府分别委任。
8.  [下议员](../Page/下议员.md "wikilink")（ADR; Ahli Dewan
    Rakyat）及[州议员](../Page/州议员.md "wikilink")（ADUN; Ahli Dewan
    Undangan Negeri）是由选举直接选出。
9.  [联邦宪法中阐明](../Page/马来西亚宪法.md "wikilink")：唯有联邦、沙巴及砂拉越的行政机构能被称为[内阁](../Page/内阁.md "wikilink")（Kabinet），其他州属的行政机构则被称为[行政议会](../Page/行政议会.md "wikilink")（MMKN;
    Majlis Mesyuarat Kerajaan Negeri）。
10. [地方议员](../Page/地方议员.md "wikilink")（Ahli Majlis）是由州政府所委任。

</div>

## 参见

  - [马来西亚地理](../Page/马来西亚地理.md "wikilink")
  - [马来西亚城市列表](../Page/马来西亚城市列表.md "wikilink")

## 外部連結

  - [Malaysia States](http://www.talkmalaysia.com/malaysia-states) -
    Articles about each states in Malaysia

[馬來西亞行政區劃](../Category/馬來西亞行政區劃.md "wikilink")