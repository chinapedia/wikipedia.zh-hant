**奥托·海因里希·瓦尔堡**（****，），[德国](../Page/德国.md "wikilink")[生理学家和](../Page/生理学.md "wikilink")[医生](../Page/医生.md "wikilink")。1931年因「发现呼吸酶的性质及作用方式」被授予[诺贝尔生理学或医学奖](../Page/诺贝尔生理学或医学奖.md "wikilink")。在第一次世界大战期间，他在精英Uhlan（骑兵团）担任一名官员，并由于勇敢而被授予[铁十字勋章](../Page/铁十字勋章.md "wikilink")（一级）。瓦尔堡被认为是20世纪著名的[生物化学家之一](../Page/生物化学.md "wikilink")\[1\]。

## 另見

  - [瓦氏效應](../Page/瓦氏效應.md "wikilink")
  - [瓦尔堡假说](../Page/瓦尔堡假说.md "wikilink")

## 参考文献

[Category:德国生理学家](../Category/德国生理学家.md "wikilink")
[Category:德国医生](../Category/德国医生.md "wikilink")
[Category:诺贝尔生理学或医学奖获得者](../Category/诺贝尔生理学或医学奖获得者.md "wikilink")
[Category:柏林洪堡大學校友](../Category/柏林洪堡大學校友.md "wikilink")
[Category:海德堡大学校友](../Category/海德堡大学校友.md "wikilink")

1.