**羊蹄甲属**（[学名](../Page/学名.md "wikilink")：**）是[豆目](../Page/豆目.md "wikilink")[豆科](../Page/豆科.md "wikilink")[紫荆亚科的一个](../Page/紫荆亚科.md "wikilink")[属](../Page/属_\(生物\).md "wikilink")。

該属属名用来纪念[瑞士](../Page/瑞士.md "wikilink")-[法国](../Page/法国.md "wikilink")[植物学家](../Page/植物学家.md "wikilink")[鮑欣(Bauhin)兄弟](../Page/鮑欣家族.md "wikilink")。英文通称包括orchid
tree、purple orchid tree、mountain ebony和poor man's
orchid。中文俗称有**洋-{}-紫荆**、**紫荆**及**羊-{}-蹄甲**等。

羊蹄甲属原产于[印度北部](../Page/印度.md "wikilink")、[越南](../Page/越南.md "wikilink")
和[中国东南部](../Page/中国.md "wikilink")。

該屬中的[洋紫荊](../Page/洋紫荊.md "wikilink")（*Bauhinia* ×
*blakeana*）在[香港](../Page/香港.md "wikilink")[首先發現](../Page/香港首先發現物種列表.md "wikilink")，由於[洋紫荊貌似蘭花](../Page/洋紫荊.md "wikilink")，因此别名**香港蘭**（），由於[洋紫荊是混種植物](../Page/洋紫荊.md "wikilink")，不能自行繁殖，這亦即是表示，現時香港所有的洋紫荊都是該棵於1880年首次於野外發現
（亦是唯一一次於野外發現）
的洋紫荊的複製品。早在1965年，洋紫荊已經獲被定為[香港市花](../Page/香港市花.md "wikilink")，以及被繪畫於[香港市政局旗上的圖案](../Page/香港市政局.md "wikilink")，寓意香港這個遠東海港，有如該棵於1880年唯一一次於野外發現的洋紫荊一樣唯一和珍貴。此特有種在1967年引入台灣，並在1984年成為[嘉義市的市花及市樹](../Page/嘉義市.md "wikilink")。[台灣的](../Page/台灣.md "wikilink")[國立中正大學及](../Page/國立中正大學.md "wikilink")[國立臺灣科技大學也是使用洋紫荊作為](../Page/國立臺灣科技大學.md "wikilink")[校花](../Page/校花.md "wikilink")。1997年後[香港特別行政區繼續採納洋紫荊花的元素作為](../Page/香港特別行政區.md "wikilink")[區徽](../Page/香港區徽.md "wikilink")、[區旗及](../Page/香港區旗.md "wikilink")[硬幣](../Page/香港硬幣.md "wikilink")\[1\]的設計圖案。雖然洋紫荊原為紫紅色，區旗只用紅白兩色，故洋紫荊圖案改成白色。

## 名稱混淆

本属中有3个种的中文名经常被混淆导致混乱，以下為它們在[兩岸三地較常用的中文俗名](../Page/兩岸三地.md "wikilink")：

\-{zh-hans;zh-hant|

| 學名                    | 中國                                    | 香港                                        | 台灣        |
| --------------------- | ------------------------------------- | ----------------------------------------- | --------- |
| *Bauhinia purpurea*   | [羊蹄甲](../Page/羊蹄甲.md "wikilink")\[2\] | [紅花羊蹄甲](../Page/紅花羊蹄甲.md "wikilink")\[3\] | 洋紫荆\[4\]  |
| *Bauhinia variegata*  | 洋紫荊\[5\]                              | [宮粉羊蹄甲](../Page/宮粉羊蹄甲.md "wikilink")\[6\] | 羊蹄甲\[7\]  |
| *Bauhinia × blakeana* | 紅花羊蹄甲\[8\]                            | [洋紫荊](../Page/洋紫荊.md "wikilink")\[9\]     | 艷紫荊\[10\] |

}-

就以上所見，單“洋-{}-紫荆”一詞就分別指三種不同的羊蹄甲屬植物。

## 參考文獻

## 外部链接

  -
[\*](../Category/羊蹄甲属.md "wikilink")
[Category:卡尔·林奈命名的生物分类](../Category/卡尔·林奈命名的生物分类.md "wikilink")

1.  [洋紫荊硬幣](http://www.info.gov.hk/hkma/chi/viewpt/991118c.htm)
2.
3.
4.
5.
6.
7.
8.
9.
10.