[South_entrance_of_Zheng_Qi_Overpass_20110114.jpg](https://zh.wikipedia.org/wiki/File:South_entrance_of_Zheng_Qi_Overpass_20110114.jpg "fig:South_entrance_of_Zheng_Qi_Overpass_20110114.jpg")
**正氣橋**是[台灣](../Page/台灣.md "wikilink")[台北市一座雙層](../Page/台北市.md "wikilink")[高架道路](../Page/高架道路.md "wikilink")，位於[基隆路](../Page/基隆路.md "wikilink")、[南京東路交叉口](../Page/南京東路_\(台北市\).md "wikilink")，為[台北東區重要的交通樞紐](../Page/台北東區.md "wikilink")。

## 概說

正氣橋是配合台灣第一條高速公路──[麥帥公路的開闢而興建](../Page/麥克阿瑟公路.md "wikilink")，於1974年建造完成，當時為台北市唯一的一座[圓環型高架橋](../Page/圓環.md "wikilink")\[1\]\[2\]，共有東（連結麥帥公路）、西（連接南京東路）、南（連接基隆路）三處[匝道出口](../Page/匝道.md "wikilink")。

正氣橋原本僅提供台北市區車輛與往[內湖](../Page/內湖區.md "wikilink")、[汐止](../Page/汐止區.md "wikilink")、[基隆車輛分流所需](../Page/基隆市.md "wikilink")，然隨著經濟起飛、工商發達，該路段車流量不斷的直線成長，圓環型的道路反而阻礙了交通順暢，上下班時間每每造成嚴重的[塞車情形](../Page/塞車.md "wikilink")，連帶影響週邊道路的交通。[台北市政府有鑑於此](../Page/台北市政府.md "wikilink")，同時為配合[堤頂大道](../Page/堤頂大道.md "wikilink")、[環東大道的開闢](../Page/環東大道.md "wikilink")，決定改建正氣橋，設計以兩條獨立的高架道路分別連接南京東路及基隆路。1999年2月16日（大年初一）凌晨，正氣橋正式拆除。

經過約兩年時間的改建，正氣橋於2001年9月29日重新通車\[3\]。重新啟用後的正氣橋為雙層高架道路，東側以[麥帥一橋與](../Page/麥帥一橋.md "wikilink")[環東大道](../Page/環東大道.md "wikilink")、南京東路六段連接，西側則與[市民大道高架道路](../Page/市民大道.md "wikilink")、基隆路一段銜接，成為台北市[快速道路路網的一部分](../Page/快速道路.md "wikilink")。

## 參考資料

<div class="references-small">

<references />

</div>

[Category:臺北市橋梁](../Category/臺北市橋梁.md "wikilink")
[Category:台北市快速道路](../Category/台北市快速道路.md "wikilink")
[Category:高架橋](../Category/高架橋.md "wikilink")
[Category:1974年完工橋梁](../Category/1974年完工橋梁.md "wikilink")
[Category:2001年完工橋梁](../Category/2001年完工橋梁.md "wikilink")

1.  [一.走入歷史的台北陸橋--正氣橋 (44) | Flickr –
    相片分享！](http://www.flickr.com/photos/piyen/4702074279/)（為改建前正氣橋的圖片）
2.  [正氣橋圓環空拍圖（1980年代中後）](http://b.blog.xuite.net/b/4/7/2/13165016/blog_55823/txt/9629167/11.jpg)－來源：[臺灣第一條高速公路
    @ Hic et ubique :: 隨意窩
    Xuite日誌](http://blog.xuite.net/amu390/CYWBCC/8657285)
3.  [正氣橋完工通車台北市東西向交通動脈打通，通車使台北矽谷成為可能有助企業根留台灣 -
    臺北市政府新聞稿 2001.09.30](http://www.taipei.gov.tw/cgi-bin/Message/MM_msg_control?mode=viewnews&ts=4635d072:6ca2)