颱-{}-風海棠}}
**颱風海棠**（，國際編號：**0505**，[聯合颱風警報中心](../Page/聯合颱風警報中心.md "wikilink")：**05W**，[菲律賓大氣地球物理和天文管理局](../Page/菲律賓大氣地球物理和天文管理局.md "wikilink")：**Feria**）為[2005年太平洋颱風季第五個被命名的風暴](../Page/2005年太平洋颱風季.md "wikilink")。「海棠」一名由[中國提供](../Page/中國.md "wikilink")，指的就是「[海棠](../Page/海棠.md "wikilink")」這種[植物](../Page/植物.md "wikilink")。

海棠也是2005年太平洋颱風季第一個登陸[台灣及](../Page/台灣.md "wikilink")[中国大陆的颱風](../Page/中国大陆.md "wikilink")；其與同年稍後的[颱風泰利](../Page/颱風泰利_\(2005年\).md "wikilink")、[颱風龍王](../Page/颱風龍王_\(2005年\).md "wikilink")2個強烈颱風（台灣[中央氣象局分級](../Page/中央氣象局.md "wikilink")）接連侵襲並登陸台灣，使2005年繼1994年後，台灣第一次於同年對2個以上的強烈颱風發布陸上颱風警報；這也是繼1965年後，第一次有3個強烈颱風於同年登陸台灣。

## 簡介

在北太平洋西部生成的颱風，多會受到副熱帶高壓脊所影響。自2005年7月初開始，日本南方持續有高氣壓滯留，是導引颱風海棠向西行進的主要原因之一。

7月16日，[中華民國](../Page/中華民國.md "wikilink")[行政院國家科學委員會的追風計畫執行單位](../Page/行政院國家科學委員會.md "wikilink")，研究人員搭乘飛機於颱風海棠周圍台灣東方1,000[公里高空](../Page/公里.md "wikilink")，投出14個衛星定位的[投落送](../Page/投落送.md "wikilink")（大氣偵測探空儀），以利全球氣象單位分析颱風結構與行進方向。

7月17日下午，首先進入暴風圈的陸地是[日本](../Page/日本.md "wikilink")[石垣島](../Page/石垣島.md "wikilink")，台灣全島於17日晚間陸續進入暴風圈的範圍。

海棠7月18日上午移動到台灣[花蓮東方約](../Page/花蓮市.md "wikilink")60公里的位置，其氣流使於位背風面的[臺東地區出現了溫度高而濕度低的](../Page/臺東縣.md "wikilink")[焚風效應](../Page/焚風.md "wikilink")，清晨溫度達37.4[℃](../Page/℃.md "wikilink")，位於台東的成功氣象站於台灣6點30分，測得的溫度為38.1℃，濕度只有約40%。

根據資料顯示，海棠在[花蓮縣沿海](../Page/花蓮縣.md "wikilink")[逆時針打轉一圈後](../Page/逆時針.md "wikilink")，於18日14時50分自[宜蘭縣](../Page/宜蘭縣.md "wikilink")[蘇澳鎮](../Page/蘇澳鎮_\(宜蘭縣\).md "wikilink")[東澳溪附近登陸](../Page/東澳溪.md "wikilink")，22時左右自[苗栗縣](../Page/苗栗縣.md "wikilink")[後龍鎮出海](../Page/後龍鎮_\(台灣\).md "wikilink")。19日17時10分，海棠在[福建省](../Page/福建省.md "wikilink")[福州市](../Page/福州市.md "wikilink")[連江縣](../Page/連江縣.md "wikilink")[筱埕鎮登陸](../Page/筱埕鎮.md "wikilink")，暴風圈逐漸縮小。海棠于7月20日早上在福建省[寧德市](../Page/寧德市.md "wikilink")[周寧縣](../Page/周寧縣.md "wikilink")[禮門鄉境内减弱为](../Page/禮門鄉.md "wikilink")[热带风暴](../Page/热带风暴.md "wikilink")。

由於[珠江三角洲處於颱風海棠外圍](../Page/珠江三角洲.md "wikilink")，因此受到下沉氣流的影響，加劇了[廣東與](../Page/廣東.md "wikilink")[香港的酷熱天氣](../Page/香港.md "wikilink")。在香港，7月18日及7月19日都錄得34℃-37℃的氣溫，其中[香港天文台在](../Page/香港天文台.md "wikilink")7月19日錄得35.4℃，是1990年8月18日香港錄得歷史高溫36.1℃以來的最高氣溫。而[澳門在](../Page/澳門.md "wikilink")7月19日更錄得39.0℃的氣溫，是澳門氣象局自1901年有紀錄以來的最高溫度。在7月17日、7月20日及7月21日，因熱成雨影響，珠三角多次受局部地區性雷暴影響，下起大驟雨和雷暴，引致7月20日香港部分地區出現停電，[九廣西鐵出現信號系統故障](../Page/九廣西鐵.md "wikilink")。7月20日及21日香港均有[冰雹報告](../Page/冰雹.md "wikilink")。

## 影響

###

  - 台灣國內線班機18日全面停飛，國際線班機亦大部分停飛。
  - 因南部蔬菜產地受損嚴重，致使葉菜類價格狂飆，[青蔥](../Page/蔥.md "wikilink")1公斤更漲到超過[新台幣](../Page/新台幣.md "wikilink")400元，[法務部下令各地](../Page/中華民國法務部.md "wikilink")[檢察官介入調查有無哄抬價格情事](../Page/檢察官.md "wikilink")。
  - [第十一屆台北國際電信展因為海棠颱風來襲的關係](../Page/台北國際電信暨網路展覽會.md "wikilink")，原本的展期也提前於7月17日結束，創下[台北世界貿易中心舉辦展覽以來](../Page/台北世界貿易中心.md "wikilink")，第一次因颱風而停止展覽的紀錄。
  - 原訂7月20日至7月24日於[福隆海水浴場舉辦的搖滾盛事](../Page/福隆海水浴場.md "wikilink")－[貢寮國際海洋音樂祭](../Page/貢寮國際海洋音樂祭.md "wikilink")－亦受到海棠颱風的影響，無法如期舉辦，並決定延後日期至8月3日至8月7日舉行。
  - 宜蘭縣[太平山森林遊樂區因聯外道路坍方及園內設施受損](../Page/太平山森林遊樂區.md "wikilink")，自20日起休園半個月。
  - 原定7月21日下午實施的中部地區[萬安二十八號演習](../Page/萬安二十八號演習.md "wikilink")，因南投縣、台中縣、嘉義縣需進行災後復原而另行擇期實施。

#### 災害

  - [台灣鐵路管理局於](../Page/台灣鐵路管理局.md "wikilink")17日晚間宣布18日的各級列車全面停駛，為台灣鐵路史上首次的全面停駛，估計損失達[新台幣](../Page/新台幣.md "wikilink")5,000萬元。19日除[南迴線外](../Page/南迴線.md "wikilink")，全線恢復正常行駛。
  - 台灣東部18日出現的焚風現象，雖然只持續了2小時，但仍嚴重地影響到台東地區的[農業生產](../Page/農業.md "wikilink")，許多將屆收成的[水果作物例如](../Page/水果.md "wikilink")[葡萄與](../Page/葡萄.md "wikilink")[釋迦](../Page/釋迦.md "wikilink")，都因持續的乾燥熱風而脫水乾枯，造成當地果農損失慘重。
  - 台灣西部[彰化地區](../Page/彰化縣.md "wikilink")18日也受到颱風中心北方的東北風過山下沈而出現的[焚風現象](../Page/焚風.md "wikilink")，有部分民眾因悶熱氣溫無法入睡，但有農民一大早到田間搶收[蔬菜時](../Page/蔬菜.md "wikilink")，卻發現蔬菜遭焚風刮得枯萎，嚴重的有如被火燒過的痕跡。
  - 由於7月18日適逢[大潮再加上颱風所帶來的暴雨與強大西北風產生的大浪等影響](../Page/大潮.md "wikilink")，台灣西南部沿海低窪地區出現海水暴漲的情況，其中[台南縣](../Page/台南縣.md "wikilink")[北門鄉甚至發生海水倒灌導致潰堤的情況](../Page/北門區.md "wikilink")。
  - [台一線](../Page/台一線.md "wikilink")[枋山鄉楓港大橋於](../Page/枋山鄉.md "wikilink")19日被暴漲的[楓港溪溪水沖毀](../Page/楓港溪.md "wikilink")，[南迴公路](../Page/南迴公路.md "wikilink")、南迴鐵路坍方，恆春[五里亭機場也因氣候不佳關閉](../Page/五里亭機場.md "wikilink")，加上停水停電，[恆春半島六鄉鎮](../Page/恆春半島.md "wikilink")20萬人及數千名遊客受困。待天氣好轉後，政府於20日緊急派遣救援專機前往恆春將旅客運送出去，並於晚間緊急將鋼管橋架設完成。
  - [中部橫貫公路多處交通中斷](../Page/中部橫貫公路.md "wikilink")，[台中縣](../Page/台中縣.md "wikilink")[和平鄉](../Page/和平區_\(臺中市\).md "wikilink")、[谷關](../Page/谷關.md "wikilink")、[梨山](../Page/梨山.md "wikilink")、[松鶴部落等地上千人受困](../Page/松鶴部落.md "wikilink")。
  - [高雄縣三民鄉](../Page/高雄縣.md "wikilink")（即今[高雄市](../Page/高雄市.md "wikilink")[那瑪夏區](../Page/那瑪夏區.md "wikilink")）、[茂林鄉](../Page/茂林區.md "wikilink")、[桃源鄉對外交通中斷](../Page/桃源區.md "wikilink")。
  - 台東縣[金峰鄉](../Page/金峰鄉_\(台灣\).md "wikilink")[太麻里溪溪水暴漲](../Page/太麻里溪.md "wikilink")，將當地衛生所、教堂及16棟民宅沖毀。
  - [國家二級古蹟](../Page/台灣古蹟.md "wikilink")[下淡水溪鐵橋遭暴漲的](../Page/下淡水溪鐵橋.md "wikilink")[高屏溪水沖毀](../Page/高屏溪.md "wikilink")。
  - [林邊溪溪水暴漲沖毀堤防](../Page/林邊溪.md "wikilink")，[屏東縣](../Page/屏東縣.md "wikilink")[林邊鄉水深及膝](../Page/林邊鄉.md "wikilink")。另外[九如鄉](../Page/九如鄉.md "wikilink")、[新園鄉](../Page/新園鄉_\(台灣\).md "wikilink")、[東港鎮](../Page/東港鎮_\(台灣\).md "wikilink")、[佳冬鄉](../Page/佳冬鄉.md "wikilink")、[車城鄉亦傳出淹水災情](../Page/車城鄉_\(台灣\).md "wikilink")。
  - 因海棠颱風夾帶的旺盛[西南氣流於](../Page/西南氣流.md "wikilink")19日晚間降下驚人豪雨，使台南縣[麻豆鎮嚴重淹水](../Page/麻豆區.md "wikilink")，19日晚間緊急封閉[國道一號麻豆交流道](../Page/國道一號.md "wikilink")，國道一號[新營](../Page/新營區.md "wikilink")－麻豆路段也因積水於20日早上7時雙向封閉。另外[七股鄉](../Page/七股區.md "wikilink")、[佳里鎮](../Page/佳里區.md "wikilink")、[學甲鎮](../Page/學甲區.md "wikilink")、[北門鄉](../Page/北門區.md "wikilink")、[官田鄉](../Page/官田區.md "wikilink")、[善化鎮](../Page/善化區.md "wikilink")、[安定鄉](../Page/安定區_\(臺南市\).md "wikilink")、[西港鄉](../Page/西港區.md "wikilink")、[仁德鄉等鄉鎮都出現豪雨淹水災情](../Page/仁德區.md "wikilink")。
  - [嘉義縣](../Page/嘉義縣.md "wikilink")[朴子市](../Page/朴子市.md "wikilink")、[東石鄉](../Page/東石鄉_\(台灣\).md "wikilink")、[布袋鎮](../Page/布袋鎮.md "wikilink")、[臺南市](../Page/臺南市_\(省轄市\).md "wikilink")[南區](../Page/南區_\(台南市\).md "wikilink")、[安南區](../Page/安南區.md "wikilink")、[高雄縣](../Page/高雄縣.md "wikilink")[岡山鎮](../Page/岡山區.md "wikilink")、[美濃鎮](../Page/美濃區.md "wikilink")、[阿蓮鄉](../Page/阿蓮區.md "wikilink")、[梓官鄉](../Page/梓官區.md "wikilink")、[永安鄉及](../Page/永安區.md "wikilink")[湖內鄉因](../Page/湖內區.md "wikilink")19日一夜豪雨而傳出淹水災情，其中[美濃鎮自](../Page/美濃區.md "wikilink")18日起3天內已淹水5次。
  - [石門水庫原水濁度暴增](../Page/石門水庫_\(台灣\).md "wikilink")，加上石門後池堰抽水站滲水故障，[桃園縣](../Page/桃園市.md "wikilink")[中壢市](../Page/中壢區.md "wikilink")、[八德市](../Page/八德區.md "wikilink")、[平鎮市及](../Page/平鎮區.md "wikilink")[龍潭鄉等地約有](../Page/龍潭區_\(桃園市\).md "wikilink")10萬戶因而停水。
  - 砂石船永昌輪於18日在高雄縣永安鄉外海擱淺，船上21人中19人獲救，1人死亡，1人落海失蹤。
  - 高雄市區超過3000棵路樹傾倒。

#### 各地最強陣風

台灣各地最強陣風主要發生在18日。

  - 17級以上：[彭佳嶼](../Page/彭佳嶼.md "wikilink")
  - 17級：[花蓮](../Page/花蓮市.md "wikilink")、[蘭嶼](../Page/蘭嶼.md "wikilink")
  - 14級：[蘇澳](../Page/蘇澳鎮_\(宜蘭縣\).md "wikilink")、[鞍部](../Page/鞍部.md "wikilink")、[梧棲](../Page/梧棲區.md "wikilink")
  - 13級：[臺北市](../Page/臺北市.md "wikilink")、[恆春](../Page/恆春鎮.md "wikilink")、[東吉島](../Page/東吉島.md "wikilink")、[馬祖](../Page/馬祖.md "wikilink")
  - 12級：[基隆市](../Page/基隆市.md "wikilink")、[宜蘭市](../Page/宜蘭市.md "wikilink")、[臺中市](../Page/臺中市_\(省轄市\).md "wikilink")、[臺南市](../Page/臺南市_\(省轄市\).md "wikilink")
  - 11級：[高雄市](../Page/高雄市.md "wikilink")、[成功](../Page/成功鎮.md "wikilink")

#### 休假

  - 18日除[金門縣](../Page/金門縣.md "wikilink")、[連江縣](../Page/連江縣_\(中華民國\).md "wikilink")（[馬祖](../Page/馬祖.md "wikilink")）外，台灣各縣市皆停止上班上課一天，[台北股市亦休市](../Page/台灣證券交易所.md "wikilink")。
  - 19日[澎湖縣](../Page/澎湖縣.md "wikilink")、金門縣、連江縣（馬祖）、苗栗縣泰安鄉、台中縣和平鄉、台東縣[成功鎮](../Page/成功鎮.md "wikilink")、[長濱鄉](../Page/長濱鄉.md "wikilink")、金峰鄉、[大武鄉](../Page/大武鄉_\(臺灣\).md "wikilink")、[達仁鄉](../Page/達仁鄉.md "wikilink")、蘭嶼鄉、[綠島鄉](../Page/綠島鄉.md "wikilink")、[東河鄉停止上班上課](../Page/東河鄉_\(台灣\).md "wikilink")。
  - 19日苗栗縣以南及台東縣，除前述鄉鎮停止上班上課，均照常上班。台中市及彰化縣正常上班上課外，高中職以下停止上課。
  - 20日台南縣照常上班，高中職以下停止上課。

###

  - 日本政府於7月17日下午宣布琉球石垣島、[宮古島與日本國內班機船隻航班全部暫停起降航行](../Page/宮古島.md "wikilink")。

###

  - [浙江省气象局](../Page/浙江省气象局.md "wikilink")17日15时发布「颱風紧急警报」，浙江各地紧急启动「防颱抗颱應急預案」。

#### 災害

  - [福建出現多起决堤事件](../Page/福建.md "wikilink")。
  - [浙江](../Page/浙江.md "wikilink")[溫州的災情亦很嚴重](../Page/溫州.md "wikilink")。

## 熱帶氣旋警告使用紀錄

### 台灣

－{{\#time:Y.m.d H:i|2005-07-20 02:30}}
|上一熱帶氣旋=[中度颱風-{}-南瑪都](../Page/颱風南瑪都_\(2004年\).md "wikilink")
|下一熱帶氣旋=[中度颱風-{}-馬莎](../Page/颱風麥莎.md "wikilink")
|2熱帶氣旋警告=[陸上颱風警報](../Page/陸上颱風警報.md "wikilink")
|2使用時間={{\#time:Y.m.d H:i|2005-07-16 23:30}}－{{\#time:Y.m.d
H:i|2005-07-20 02:30}}
|2上一熱帶氣旋=[中度颱風-{}-南瑪都](../Page/颱風南瑪都_\(2004年\).md "wikilink")
|2下一熱帶氣旋=[中度颱風-{}-馬莎](../Page/颱風麥莎.md "wikilink") }}

## 參考資料

## 參見

  - [2005年太平洋颱風季](../Page/2005年太平洋颱風季.md "wikilink")

## 外部連結

  - <http://www.jma.go.jp/> [日本氣象廳首頁](../Page/日本氣象廳.md "wikilink")

  - <http://www.usno.navy.mil/JTWC/>
    [聯合颱風警報中心首頁](../Page/聯合颱風警報中心.md "wikilink")

  - <http://www.nmc.gov.cn/> [中央氣象台首頁](../Page/中央氣象台.md "wikilink")

  - <http://www.cwb.gov.tw/> [中央氣象局首頁](../Page/中央氣象局.md "wikilink")

  - <http://www.hko.gov.hk/> [香港天文台首頁](../Page/香港天文台.md "wikilink")

  - <http://www.smg.gov.mo/>
    [澳門地球物理暨氣象局首頁](../Page/澳門地球物理暨氣象局.md "wikilink")

[Category:2005年太平洋颱風季](../Category/2005年太平洋颱風季.md "wikilink")
[Category:五級熱帶氣旋](../Category/五級熱帶氣旋.md "wikilink")
[颱風](../Category/2005年台灣.md "wikilink")
[Category:影响中国大陆的热带气旋](../Category/影响中国大陆的热带气旋.md "wikilink")
[Category:影响菲律宾的热带气旋](../Category/影响菲律宾的热带气旋.md "wikilink")
[Category:台风](../Category/台风.md "wikilink")