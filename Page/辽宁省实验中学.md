**辽宁省实验中学**，是[中国](../Page/中国.md "wikilink")[辽宁省](../Page/辽宁省.md "wikilink")[沈阳市的一所](../Page/沈阳市.md "wikilink")[高级中学](../Page/高级中学.md "wikilink")，辽宁省教育厅直属[重点中学](../Page/重点中学.md "wikilink")。学校占地面积10万平方米，建筑面积6万3千平方米。\[1\]

前身为成立于1949年5月4日的东北实验学校。由[林枫](../Page/林枫.md "wikilink")、[车向忱](../Page/车向忱.md "wikilink")、[董纯才](../Page/董纯才.md "wikilink")、[郭明秋等人组建](../Page/郭明秋.md "wikilink")，[车向忱担任第一任校长](../Page/车向忱.md "wikilink")。
现任校长为[关俊奇](../Page/关俊奇.md "wikilink")，党委书记为[刘朝忠](../Page/刘朝忠.md "wikilink")，于2009年12月28日由中共辽宁省教育厅党组正式聘任。\[2\]

## 资料来源

## 外部链接

  - [辽宁省实验中学教育信息网](http://www.lnsyzx.com/Index.html)

[Category:沈阳中等教育](../Category/沈阳中等教育.md "wikilink")
[Category:1949年创建的教育机构](../Category/1949年创建的教育机构.md "wikilink")
[Category:辽宁省省级示范性普通高中](../Category/辽宁省省级示范性普通高中.md "wikilink")
[Category:辽宁中等教育](../Category/辽宁中等教育.md "wikilink")

1.  [学校简介](http://www.lnsyzx.com/about/xxjj.phtml)，辽宁省实验中学教育信息网
2.  [领导成员](http://www.lnsyzx.com/about/ldcy.phtml)，辽宁省实验中学教育信息网