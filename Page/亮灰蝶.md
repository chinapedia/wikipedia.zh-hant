**亮灰蝶**（[学名](../Page/学名.md "wikilink")：**），-{zh-hans;zh-hant|又稱曲紋灰蝶、曲斑灰蝶、长尾里波灰蝶、波紋小灰蝶}-。

亮灰蝶臀角处有2黑斑，翅展22－36mm，雄蝶翅正面紫褐色，雌蝶前翅基后半部和後翅基部青蓝色。喜食[豆科植物的果莢](../Page/豆科.md "wikilink")，成虫飞行能力强。分部於欧洲中南部，非洲北部，亚洲南部。

<File:Pea> Blue (Lampides boeticus) at Hodal Iws IMG 1246.jpg|
在印度霍德爾，法里達巴德，哈里亞納邦. <File:Pea> Blue (Lampides
boeticus) at Hodal Iws IMG 1242.jpg| 在印度霍德爾，法里達巴德，哈里亞納邦. <File:Pea> Blue
(Lampides boeticus) at Hodal Im IMG 1240.jpg| 在印度霍德爾，法里達巴德，哈里亞納邦.
<File:Common> Cerulean I IMG 6408.jpg|Pea Blue in
[Kullu](../Page/Kullu.md "wikilink")-
[Manali](../Page/Manali.md "wikilink") District of [Himachal
Pradesh](../Page/Himachal_Pradesh.md "wikilink"),
[India](../Page/India.md "wikilink"). <File:Butterfly> (7500 ft) I IMG
6673.jpg|Pea Blue in [Kullu](../Page/Kullu.md "wikilink")-
[Manali](../Page/Manali.md "wikilink") District of [Himachal
Pradesh](../Page/Himachal_Pradesh.md "wikilink"),
[India](../Page/India.md "wikilink"). <File:Gram> Blue I IMG
6667.jpg|Pea Blue in [Kullu](../Page/Kullu.md "wikilink")-
[Manali](../Page/Manali.md "wikilink") District of [Himachal
Pradesh](../Page/Himachal_Pradesh.md "wikilink"),
[India](../Page/India.md "wikilink"). <File:Lamprides>
boeticus-female.jpg|*雌灰蝶* *Lampides boeticus*

## 參考

  -
[Category:亮灰蝶屬](../Category/亮灰蝶屬.md "wikilink")
[Category:臺灣蝴蝶](../Category/臺灣蝴蝶.md "wikilink")
[Category:卡尔·林奈命名的生物分类](../Category/卡尔·林奈命名的生物分类.md "wikilink")