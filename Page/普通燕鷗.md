**普通燕鷗**（***Sterna
hirundo***）是[燕鷗科的一種](../Page/燕鷗科.md "wikilink")[海鳥](../Page/海鳥.md "wikilink")。這種[鳥分佈於](../Page/鳥.md "wikilink")[北極及附近地區](../Page/北極.md "wikilink")，繁殖區為[北極及](../Page/北極.md "wikilink")[歐洲](../Page/歐洲.md "wikilink")、[亞洲和](../Page/亞洲.md "wikilink")[北美洲東及中部](../Page/北美洲.md "wikilink")。這種鳥是候鳥，有很強的[遷移性](../Page/鳥類遷徙.md "wikilink")，在熱帶及亞熱帶海洋越冬。

## 外形特征

這是中等體型的鳥類，一般長約34至37公分，翼展長70至80公分。牠們很容易被人跟[北極燕鷗](../Page/北極燕鷗.md "wikilink")（*Sterna
paradisaea*）及[紅燕鷗](../Page/紅燕鷗.md "wikilink")（*Sterna dougalli*）混為一談。

牠們尖而幼的喙呈紅色，其尖端則呈黑色，牠們的長腿亦是呈紅色的。牠們上面的冀背不是像北極燕鷗那般呈灰色，而是有一個黑色的楔狀圖案。在牠們站著時，牠們的長尾巴只延至其翼尖，而非像北極燕鷗和紅燕鷗般長於翼尖。牠們並沒有紅燕鷗的顏色那樣淡。

## 习性

在冬天，牠們的頭頂及下體均呈白色。普通燕鷗幼鳥的顏色較鮮艷，且沒有紅燕鷗那樣鱗片。

牠們的叫聲很像笛聲，相似於[北極燕鷗但普通燕鷗的音調較低](../Page/北極燕鷗.md "wikilink")，且沒有北極燕鷗那麼刺耳。

普通燕鷗在海岸、海島及適合的內陸湖繁殖。牠們一次約可生二至四個蛋。像很多其他白燕鷗般，牠們在保衛鳥巢時會不惜攻擊大型的侵略者甚至是人類。

像所有燕鷗屬的燕鷗般，普通燕鷗會潛下水吃魚，不論是在海洋或淡水湖或大河流，而不同於北極燕鷗的是，牠們通常都會直接衝進水中。雄性把魚送給雌性是一種求愛的表示。普通燕鷗的壽命一般可達23歲，有時更可能會達23歲以上。

分佈在北美洲及古北界；冬季南遷到南美洲、非洲、印度洋、印度尼西亞及澳大利亞或附近地區。

在蘇格蘭古英語中，普通燕鷗稱為「pictar」，有時牠們會到了[蘇格蘭和](../Page/蘇格蘭.md "wikilink")[加拿大的](../Page/加拿大.md "wikilink")[加拿大大西洋省份](../Page/加拿大大西洋省份.md "wikilink")。

普通燕鷗是[非歐亞遷移性水鳥協定](../Page/非歐亞遷移性水鳥協定.md "wikilink")（African-Eurasian
Migratory Waterbird Agreement，AEWA）所包括的鳥類之一。

## 亚种

  - **普通燕鸥指名亚种**（[学名](../Page/学名.md "wikilink")：）。在[中国大陆](../Page/中国大陆.md "wikilink")，
    分布于[新疆等地](../Page/新疆.md "wikilink")。该物种的模式产地在[瑞典](../Page/瑞典.md "wikilink")。\[1\]
  - **普通燕鸥东北亚种**（[学名](../Page/学名.md "wikilink")：）。在[中国大陆](../Page/中国大陆.md "wikilink")，
    分布于[内蒙古](../Page/内蒙古.md "wikilink")、[山西](../Page/山西.md "wikilink")、[陕西](../Page/陕西.md "wikilink")、东部沿海等地。该物种的模式产地在[西伯利亚](../Page/西伯利亚.md "wikilink")。\[2\]
  - **普通燕鸥西藏亚种**（[学名](../Page/学名.md "wikilink")：）。在[中国大陆](../Page/中国大陆.md "wikilink")，
    分布于[青海](../Page/青海.md "wikilink")、[甘肃](../Page/甘肃.md "wikilink")、[四川](../Page/四川.md "wikilink")、[西藏](../Page/西藏.md "wikilink")、[河北](../Page/河北.md "wikilink")、[湖北](../Page/湖北.md "wikilink")、[福建](../Page/福建.md "wikilink")、[海南等](../Page/海南.md "wikilink")
    地。该物种的模式产地在[西藏](../Page/西藏.md "wikilink")。\[3\]

## 圖片

<File:IMG> 1918-01.jpg|在捕食的普通燕鷗
[File:SternaHirundoMissedCatch.jpg|捕食失手](File:SternaHirundoMissedCatch.jpg%7C捕食失手)
[File:SternaHirundoCatchB.jpg|成功捕食](File:SternaHirundoCatchB.jpg%7C成功捕食)

## 參考來源

  - **Austin**, Oliver L. Sr. (1953): A Common Tern at Least 23 Years
    Old. *Bird-Banding* **24**(1): 20. [PDF
    fulltext](http://elibrary.unm.edu/sora/JFO/v024n01/p0020-p0020.pdf)
  - **Harrison**, Peter (1988): *Seabirds* (2nd ed.). Christopher Helm,
    London ISBN 0-7470-1410-8
  - **National Geographic Society** (2002): *Field Guide to the Birds of
    North America*. National Geographic, Washington DC. ISBN
    0-7922-6877-6
  - **Olsen**, Klaus Malling & **Larsson**, Hans (1995): *Terns of
    Europe and North America*. Christopher Helm, London. ISBN
    0-7136-4056-1

## 外部連結

  - [普通燕鷗影片](http://ibc.hbw.com/ibc/phtml/especie.phtml?idEspecie=1648)
  - [普通燕鷗－「Sterna
    hirundo」](http://www.mbr-pwrc.usgs.gov/id/framlst/i0700id.html)
  - [普通燕鷗資料及圖片](http://www.sdakotabirds.com/species/common_tern_info.htm)
  - [普通燕鷗外型](http://www.madeirabirds.com/common_tern)

[Category:燕鷗科](../Category/燕鷗科.md "wikilink")

1.
2.
3.