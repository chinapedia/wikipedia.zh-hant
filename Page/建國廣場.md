**建國廣場**（）是一個設在[台灣的](../Page/台灣.md "wikilink")[台灣獨立運動組織](../Page/台灣獨立運動.md "wikilink")，創立於1995年7月22日，以台灣獨立建國理念的宣揚與實踐為宗旨。\[1\]

## 創立過程

1995年7月間，[中國人民解放軍因當時的](../Page/中國人民解放軍.md "wikilink")[中華民國總統](../Page/中華民國總統.md "wikilink")[李登輝訪問](../Page/李登輝.md "wikilink")[美國而](../Page/美國.md "wikilink")[擴大軍事演習](../Page/台灣海峽飛彈危機.md "wikilink")，企圖以武力威嚇台灣。[林山田](../Page/林山田.md "wikilink")（[台大法學院](../Page/台大法學院.md "wikilink")[教授](../Page/教授.md "wikilink")）於是號召有[台灣意識的](../Page/台灣意識.md "wikilink")[葉國興](../Page/葉國興.md "wikilink")（前[行政院新聞局局長](../Page/行政院新聞局.md "wikilink")）、[傅雲欽](../Page/傅雲欽.md "wikilink")（[律師](../Page/律師.md "wikilink")）、[葉博文](../Page/葉博文.md "wikilink")（前[台北二二八紀念館館長](../Page/台北二二八紀念館.md "wikilink")）、[卓榮德](../Page/卓榮德.md "wikilink")（前[青年之聲廣播電台台長](../Page/青年之聲廣播電台.md "wikilink")）等人，開始在[台北市政府前廣場定期](../Page/台北市政府.md "wikilink")（每週六）集會演講，宣揚台獨理念。當月22日（週六），舉辦第一場集會演講，宣告建國廣場創立。\[2\]

林山田在1995年7月22日為建國廣場撰寫的文章〈從廣場出發建國〉，進一步說明建國廣場的創建源由：

  - 後續發展

建國廣場成立幾個月後，葉國興、卓榮德因個人事業繁忙，相繼淡出（卓榮德於2012年7月[出家](../Page/出家.md "wikilink")，[法號](../Page/法號.md "wikilink")「生明」）。建國廣場初期委託[TNT寶島新聲廣播電台轉播集會演講過程](../Page/TNT寶島新聲廣播電台.md "wikilink")，後於1996年4月間自行成立[廣播電台](../Page/廣播電台.md "wikilink")。電台有一個有趣的全名，作為放送的[台呼](../Page/台呼.md "wikilink")：起初叫做「建國廣場台獨[基本教義我們不是](../Page/基本教義.md "wikilink")[中國人反對](../Page/中國人.md "wikilink")[大和解](../Page/大和解.md "wikilink")[戰鬥戰鬥戰鬥廣播電台](../Page/戰鬥.md "wikilink")」，後來改為「建國廣場臺灣人民鬥陣不作中國人戰鬥戰鬥戰鬥廣播電台」，再改為「建國廣場台灣牛望台灣國對人彈琴廣播電台」。

1996年9月，林山田轉忙於創立[建國黨](../Page/建國黨.md "wikilink")，傅雲欽接任建國廣場負責人。建國廣場在台北市政府前廣場的定期集會演講，風雨無阻，連續舉辦一年多，共75場。

2009年12月28日，建國廣場電台遭[中華民國政府抄台](../Page/中華民國政府.md "wikilink")，主要機具被沒收；但電台隨即又以備用小型發射機繼續作最後控訴性的播音，至2010年4月12日「彈盡援絕」而停播為止。2010年5月12日，傅雲欽接受[快樂聯播網人物訪談節目](../Page/快樂聯播網.md "wikilink")《夢中的國家》主持人[張素華訪問時表示](../Page/張素華.md "wikilink")，建國廣場以電台發聲的方式已走入歷史，未來將轉由網路發聲，繼續打拚。\[3\]

2010年9月5日，傅雲欽正式以「建國廣場網路組」成立部落閣，「作為建國廣場在網路發聲的官方管道」，同時亦將過去文章分別整理發上部落閣。傅雲欽言：「在台獨運動被傳統獨派和投機政客玩弄得奄奄一息之際，傅雲欽一方面痛加批判，一方面提出自己獨到的見解，指出一條到台灣國之路。但他知道：[耶利米雖然究天人之際](../Page/耶利米.md "wikilink")、通古今之變，他的苦口婆心終無法扭轉[猶大國被](../Page/猶大國.md "wikilink")[巴比倫併吞的命運](../Page/巴比倫.md "wikilink")，只能成一家之言、為《[聖經](../Page/聖經.md "wikilink")》留下篇章而已。嗚呼哀哉！天佑台灣！」

## 主張

  - 初期主張

初期階段，台灣由反台獨的國民黨執政。建國廣場與當時在野而有[台獨黨綱的](../Page/台獨黨綱.md "wikilink")[民進黨同屬反對陣營](../Page/民進黨.md "wikilink")，選舉時支持民進黨。

後來民進黨提出「[大和解](../Page/大和解.md "wikilink")」政策，表示台灣不必也不會宣佈獨立後，建國廣場大加批判，逐漸與民進黨分道揚鑣。1999年[陳水扁為競選](../Page/陳水扁.md "wikilink")2000年中華民國總統，而提出「[新中間路線](../Page/新中間路線.md "wikilink")」，暗示台獨路線偏激，並促成民進黨通過〈[台灣前途決議文](../Page/台灣前途決議文.md "wikilink")〉，認定台灣已是一個以中華民國為名的主權國家，明顯放棄台獨運動。建國廣場因此痛斥陳水扁是投機政客，並呼籲獨派另推總統候選人參選，否則罷選。一時有不少獨派大老認同。但此項呼籲在陳水扁逐一探訪不滿的獨派大老，加以「摸頭」安撫，並承諾、保證之後，就「有疾」而終，未能成真。陳水扁後來當選總統，就酬庸一些獨派大老擔任中華民國的總統府資政或國策顧問。

2010年2月28日，傅雲欽發表〈民進黨不排除統一，獨派不可寄望它！〉，稱：「不堅持台獨理想，現實取向，可能當選中華民區體-{制}-公職，但絕不可能實現台灣獨立建國。1991年，民進黨在原來『[台獨黨綱](../Page/台獨黨綱.md "wikilink")』草案的最後被加上『……應交由台灣全體住民以[公民投票方式選擇決定](../Page/公民投票.md "wikilink")』的尾巴，變成『台獨公投黨綱』之後，我就看-{出}-民進黨搞台獨的虛假性，漸漸由支持它轉為批判它。民進黨說『統一也是選項』、『不排除統一』，就是不堅持台獨，心態投機。這種濫黨真是不可期待。綠營或傳統獨派還整天為這個濫黨搖旗吶喊，真是墮落。有這種綠營、獨派，台灣不被中國併吞也難。」

  - 理論發展

初期獨立理論，認為台灣雖不是國家，但中華民國體制有[人民](../Page/人民.md "wikilink")、[土地](../Page/土地.md "wikilink")、[政府](../Page/政府.md "wikilink")，則是國家。但1997年間，傅雲欽在參考獨派學者[許慶雄有關](../Page/許慶雄.md "wikilink")「在台灣的中華民國體制只是中國的叛亂團體」，不是國家的理論後，頗為認同。傅雲欽於是修正並加強其理論細節，發展-{出}-下列論述：『台灣目前僅是[事實上獨立](../Page/De_facto.md "wikilink")，[法理上尚未獨立](../Page/de_jure.md "wikilink")，且屬於中國。因此台灣不是[國家](../Page/國家.md "wikilink")。台灣要依據人民自決的原則，完成宣佈獨立的法律程序，才能進一步法理上獨立，成為國家。』此論提出之初，傳統獨派很多不以為然；然而「路遙知馬力」，有些人逐漸對此理論開始探究並支持。但傳統獨派所謂「台灣已經因民主化成為國家」的觀念仍根深蒂固，「把中華民國體-{制}-當作台灣國」\[4\]。

  - 政論管道

<!-- end list -->

  - 《建國廣場通訊》（2002年3月2日[電子報版創刊](../Page/電子報.md "wikilink")，2003年7月22日電子報版停刊）
  - 《建國廣場通告》（2006年創刊，只以[電郵發送](../Page/電郵.md "wikilink")）
  - 建國廣場電台（1996年4月設立，FM頻率電台，收聽範圍在[大台北地區](../Page/大台北地區.md "wikilink")，無[商業](../Page/商業.md "wikilink")[廣告](../Page/廣告.md "wikilink")，完全靠[捐款維持運作](../Page/捐款.md "wikilink")。於2009年12月28日最後一次被抄台，2010年4月12日停播）
  - 《建國廣場之聲》網站（2010年9月5日創立，同年10月5日加上網路廣播）

## 活動

[立法院公報第85卷第51期_P51-52.pdf](https://zh.wikipedia.org/wiki/File:立法院公報第85卷第51期_P51-52.pdf "fig:立法院公報第85卷第51期_P51-52.pdf")
建國廣場舉辦過多場抗爭活動，如抗議[TVBS報導不公](../Page/TVBS.md "wikilink")、反對[台北市議會自肥](../Page/台北市議會.md "wikilink")、阻擋[佛光山迎](../Page/佛光山.md "wikilink")[佛牙舍利](../Page/佛牙舍利.md "wikilink")、反對統派打壓[小林善紀](../Page/小林善紀.md "wikilink")[漫畫](../Page/漫畫.md "wikilink")《[臺灣論](../Page/台灣論.md "wikilink")》等等。
其次，主辦、協辦過的遊行有：
1996年至1998年[民進黨主席](../Page/民進黨主席.md "wikilink")[許信良任內](../Page/許信良.md "wikilink")，不滿許信良說民進黨要走出悲情、民進黨要加速推動兩岸[三通](../Page/三通.md "wikilink")、台獨黨綱是歷史文件等語，建國廣場多次開宣傳車帶領群眾到民進黨中央黨部所在的華山商業大樓下面，抗議民進黨「掛台獨的羊頭，賣捍衛中華民國的狗肉」。其中一次抗議，群眾乘坐[升降機衝到民進黨中央黨部所在樓層](../Page/升降機.md "wikilink")，要求民進黨說明。雙方談判折衝之際，群眾把民進黨中央黨部門口釘掛在牆上的銅製黨徽敲下，群眾下樓離開時順手把銅製黨徽帶走。
1996年10月18日，建國廣場聯合[張金策的群眾之聲廣播電台](../Page/張金策.md "wikilink")，用宣傳車擴音器在[立法院門前輪番大罵民進黨在](../Page/立法院.md "wikilink")[核四](../Page/核四.md "wikilink")[覆議案中](../Page/覆議.md "wikilink")「假反核，真勾結」，罵了一整天；同日晚間，抗議群眾拆下立法院牌匾，警方強制驅離抗議群眾，後來建國廣場成員張雙福等人被以違反《[集會遊行法](../Page/集會遊行法.md "wikilink")》移送法辦。1996年10月21日，民進黨立法院黨團幹事長[沈富雄因此批評傅雲欽](../Page/沈富雄.md "wikilink")「不負責任」、「建國運動的敗類」；傅雲欽回應，建國廣場只是民進黨當天動員到立法院抗議的眾多團體之一，民進黨不能將責任推給建國廣場\[5\]。
1996年10月25日，建國廣場「臺灣尚未光復，建國愛擱打拼」（臺灣尚未光復，建國要再努力）遊行，顯示台灣還沒有獨立建國，仍在「[外來政權](../Page/外來政權.md "wikilink")」[中華民國統治之下](../Page/中華民國.md "wikilink")。臺灣必須獨立建國，才算是真正「光復」。
1997年2月28日，建國廣場舉辦[遊行與](../Page/遊行.md "wikilink")[行動劇悼念](../Page/行動劇.md "wikilink")[二二八事件](../Page/二二八事件.md "wikilink")，於行動劇中扮演[陳儀的義工](../Page/陳儀.md "wikilink")[杜文權於此次遊行活動中因表演的](../Page/杜文權.md "wikilink")[意外事故不幸喪生](../Page/意外事故.md "wikilink")。
1998年7月，[民進黨秘書長](../Page/民進黨秘書長.md "wikilink")[邱義仁應](../Page/邱義仁.md "wikilink")[廈門大學台灣研究所](../Page/廈門大學.md "wikilink")（[廈門大學台灣研究院前身](../Page/廈門大學台灣研究院.md "wikilink")）邀請參加研討會，返回臺灣後公開表示，「民進黨是選舉黨，選票就是答案，再現實不過」，所以在[臺灣海峽兩岸關係問題上](../Page/臺灣海峽兩岸關係.md "wikilink")「民進黨強扮黑臉是誤國誤民」\[6\]；1998年8月1日，在邱義仁到民進黨中央黨部參加[新舊主席交接典禮時](../Page/民主進步黨主席.md "wikilink")，建國廣場開宣傳車到現場向邱義仁與主張[大膽西進的前主席許信良表示抗議](../Page/大膽西進.md "wikilink")，抗議布條上寫著「賀邱義仁去中國沒死」九字\[7\]。
2000年4月起，建國廣場每個月舉辦「建國長-{征}-系列[行軍遊行](../Page/行軍.md "wikilink")」（效彷[中國工農紅軍](../Page/中國工農紅軍.md "wikilink")[長征之義](../Page/長征.md "wikilink")）宣揚台灣獨立建國，至2001年12月已舉辦過二十一個場次：
2000年4月23日：「[軍人出頭天](../Page/軍人.md "wikilink")，台獨真可憐」遊行。
2000年
5月20日：「[陳水扁不是](../Page/陳水扁.md "wikilink")[總統](../Page/總統.md "wikilink")：台灣建國才有總統」遊行。
2000年 6月17日：「宣佈獨立，才有兩國」遊行。
2000年 7月22日：「求和不如備戰」遊行 。
2000年 8月20日：「台灣要建國，[外交才正常](../Page/外交.md "wikilink")」遊行 。
2000年 9月17日：「[宋案不辦](../Page/宋楚瑜.md "wikilink")，掃貪騙人」遊行 。
2000年10月22日：「[戒急用忍](../Page/戒急用忍.md "wikilink")，根留台灣」遊行。
2000年11月19日：「真[反核不莽撞](../Page/反核.md "wikilink")，敢挑戰免道歉」遊行。
2000年12月17日：「民不聊生，官要加薪，豈有此理」遊行。
2001年1月13日：「抱建國夢，入新世紀」遊行。
2001年 2月17日：「政府廢核無能，[核四公投解決](../Page/核四公投.md "wikilink")」遊行。
2001年 3月17日：「『中華民國』，死路一條」遊行。
2001年
4月21日：「[媽祖已獨立](../Page/媽祖.md "wikilink")，不必認[唐山](../Page/唐山.md "wikilink")」遊行。
2001年 5月19日：「獨立不敢，三千無影，執政創啥？」遊行。
2001年 6月17日：「台灣無國格，建國保台灣」遊行。
2001年 7月22日：「不做中國人，建造台灣國」遊行。
2001年 8月18日：「用台灣心，醫中國病」遊行 。
2001年 9月29日：「拚建國，救台灣」遊行 。
2001年10月20日：「打特權，顧弱勢」遊行 。
2001年11月25日：「殺中國豬，建台灣國」遊行。
2002年5月11日：參加「[臺灣正名運動](../Page/臺灣正名運動.md "wikilink")」遊行 。
2007年9月22日：「宣佈獨立、宣佈戒嚴」遊行。

## 参考文献

<div class="references-small">

<references />

  - [傅雲欽如是說](http://taiwangok.blogspot.com/)

</div>

[Category:台灣廣播電台](../Category/台灣廣播電台.md "wikilink")
[Category:1995年建立的組織](../Category/1995年建立的組織.md "wikilink")
[Category:台灣獨立運動組織](../Category/台灣獨立運動組織.md "wikilink")

1.  [一九九○年代的基層臺獨運動－一個社會運動的觀點](http://www.drnh.gov.tw/ImagesPost/e3bc66dd-0cd2-49a1-a1f6-4129f4ccfd3f/db0cc1c7-5c47-4770-9463-8acf964c5f0e_ALLFILES.pdf)
    , [國史館學術集刊](../Page/國史館.md "wikilink"), 2004

2.
3.  [《夢中的國家》2010年5月12日錄音檔](http://blip.tv/file/get/Taiwanradious-2010512DreamNationPart1722.mp3)

4.

5.

6.

7.