**2008年電影大事紀。**

## 最高票房

按照英语语言[電影工業的惯例](../Page/電影工業.md "wikilink")，以下这些电影是在2008年内新上映的电影中票房最高的。全球、美国和加拿大以及国际；英国和澳大利亚分别如下：

| 排名 | 片名                                                   | 公司                                                                  | 全球票房            | 北美票房         | 英國票房         | 澳洲票房        |
| -- | ---------------------------------------------------- | ------------------------------------------------------------------- | --------------- | ------------ | ------------ | ----------- |
| 1  | [黑暗騎士](../Page/黑暗騎士.md "wikilink")                   | [華納兄弟](../Page/華納兄弟.md "wikilink")                                  | '''$999,110,731 | $532,916,540 | $88,939,810  | $39,854,100 |
| 2  | [印地安納瓊斯：水晶骷髏王國](../Page/印地安納瓊斯：水晶骷髏王國.md "wikilink") | [派拉蒙](../Page/派拉蒙.md "wikilink")                                    | '''$786,636,033 | $317,101,119 | $79,283,312  | $27,981,873 |
| 3  | [功夫熊貓](../Page/功夫熊貓.md "wikilink")                   | [夢工廠](../Page/夢工廠.md "wikilink")                                    | '''$631,908,951 | $215,434,591 | $39,405,501  | $24,764,811 |
| 4  | [全民超人](../Page/全民超人.md "wikilink")                   | [哥倫比亞](../Page/哥倫比亞.md "wikilink")                                  | '''$624,386,746 | $227,946,274 | $49,170,891  | $19,636,856 |
| 5  | [媽媽咪呀](../Page/媽媽咪呀.md "wikilink")                   | [環球影業](../Page/環球影業.md "wikilink")                                  | '''$584,606,592 | $144,130,063 | $132,314,599 | $29,286,256 |
| 6  | [钢铁侠](../Page/鐵甲奇俠_\(電影\).md "wikilink")             | [派拉蒙](../Page/派拉蒙.md "wikilink")                                    | '''$582,030,528 | $318,412,101 | $33,822,889  | $18,880,106 |
| 7  | [馬達加斯加2](../Page/馬達加斯加2.md "wikilink")               | [夢工廠](../Page/夢工廠.md "wikilink")                                    | '''$578,481,220 | $179,826,000 | $33,562,100  | $14,666,671 |
| 8  | [007量子危機](../Page/007量子危機.md "wikilink")             | [米高梅](../Page/米高梅.md "wikilink")／[哥倫比亞](../Page/哥倫比亞.md "wikilink") | '''$570,001,406 | $168,368,427 | $80,264,203  | $20,645,336 |
| 9  | [瓦力](../Page/瓦力_\(電影\).md "wikilink")                | [迪士尼](../Page/迪士尼.md "wikilink")（[皮克斯](../Page/皮克斯.md "wikilink")）  | '''$533,692,120 | $223,808,164 | $41,215,600  | $14,165,390 |
| 10 | [納尼亞傳奇：賈思潘王子](../Page/納尼亞傳奇：賈思潘王子.md "wikilink")     | [迪士尼](../Page/迪士尼.md "wikilink")                                    | '''$419,651,413 | $141,621,490 | $21,581,030  | $13,181,570 |

{{-}}

總體而言，**2008年**上映電影共有**57**部電影票房收入超過1億[美元](../Page/美元.md "wikilink")，達到賣座銷量。**12**部電影超過4億美元，達到國際賣座票房標準，打破2007年**11**部的紀錄。電影《[黑暗騎士](../Page/黑暗騎士.md "wikilink")》更是超過9億美元，排行[電影最高票房史上第四名](../Page/最高電影票房收入列表.md "wikilink")。同時創下北美票房在18天內達到4億美元票房紀錄，打破《[史瑞克2](../Page/史瑞克2.md "wikilink")》43天的紀錄。2008年票房排行前十電影，有三部[超級英雄電影](../Page/超級英雄電影.md "wikilink")，三部[動畫電影](../Page/動畫電影.md "wikilink")，三部[動作片和一部](../Page/動作片.md "wikilink")[歌舞片](../Page/歌舞片.md "wikilink")。

## 大事紀

**2008年**是一個[年](../Page/年.md "wikilink")，它的第一天從星期二開始。

  - [1月22日](../Page/1月22日.md "wikilink")：曾經跟[李安合作過](../Page/李安.md "wikilink")《[斷背山](../Page/斷背山.md "wikilink")》的[澳大利亞演員](../Page/澳大利亞.md "wikilink")[希斯·萊傑](../Page/希斯·萊傑.md "wikilink")，意外地在美國[紐約因不當服用藥物過世](../Page/紐約.md "wikilink")。
  - [2月7日](../Page/2月7日.md "wikilink")：[柏林影展揭幕](../Page/柏林影展.md "wikilink")，[2月17日閉幕](../Page/2月17日.md "wikilink")，[巴西影片](../Page/巴西.md "wikilink")《[菁英部隊](../Page/菁英部隊.md "wikilink")》意外地奪下[金熊獎最佳影片](../Page/金熊獎.md "wikilink")；呼聲最高的影片《[黑金企業](../Page/黑金企業.md "wikilink")》還是拿下最佳導演銀熊獎[保羅·湯瑪斯·安德森](../Page/保羅·湯瑪斯·安德森.md "wikilink")。
  - [2月22日](../Page/2月22日.md "wikilink")：[法國](../Page/法國.md "wikilink")[凱撒獎頒獎](../Page/凱撒獎.md "wikilink")，[瑪莉詠·柯蒂亞如願地以](../Page/瑪莉詠·柯蒂亞.md "wikilink")《[玫瑰人生](../Page/玫瑰人生.md "wikilink")》拿下最佳女主角獎。[阿布德拉提夫·柯奇許的](../Page/阿布德拉提夫·柯奇許.md "wikilink")《[種子跟鯔魚](../Page/種子跟鯔魚.md "wikilink")》奪下最佳影片、最佳導演、最佳原創劇本跟最有希望女演員[阿芙皙雅·艾吉](../Page/阿芙皙雅·艾吉.md "wikilink")。
  - [2月24日](../Page/2月24日.md "wikilink")：[美國](../Page/美國.md "wikilink")[奧斯卡金像獎頒獎](../Page/奧斯卡金像獎.md "wikilink")，《[險路勿近](../Page/險路勿近.md "wikilink")》拿下最佳影片、最佳導演、最佳改編劇本以及最佳男配角四項大獎。呼聲頗高的《[黑金企業](../Page/黑金企業.md "wikilink")》僅拿下最佳男主角[丹尼爾·戴-劉易斯以及最佳攝影](../Page/丹尼爾·戴-劉易斯.md "wikilink")。其中最特別地是[法國演員](../Page/法國.md "wikilink")[瑪莉詠·柯蒂亞如願地以](../Page/瑪莉詠·柯蒂亞.md "wikilink")《[玫瑰人生](../Page/玫瑰人生_\(电影\).md "wikilink")》拿下[美國奧斯卡最佳女主角獎](../Page/美國.md "wikilink")，算是近期罕見地以非英語系國家出身的身分奪下此項大獎。

## 重要獎項

### 美國奧斯卡（第80屆）

  - 最佳影片：《[險路勿近](../Page/險路勿近.md "wikilink")》（No Country for Old
    Men），[喬爾·柯恩、伊森·柯恩執導](../Page/柯恩兄弟.md "wikilink")
  - 最佳導演：[喬爾·柯恩、伊森·柯恩的](../Page/柯恩兄弟.md "wikilink")《[險路勿近](../Page/險路勿近.md "wikilink")》（No
    Country for Old Men）
  - 最佳原著劇本：Diablo Cody，《[鴻孕當頭](../Page/鴻孕當頭.md "wikilink")》（Juno）
  - 最佳改編劇本：[喬爾·柯恩、伊森·柯恩的](../Page/柯恩兄弟.md "wikilink")《[險路勿近](../Page/險路勿近.md "wikilink")》（No
    Country for Old Men）
  - 最佳男主角：[丹尼爾·戴-劉易斯](../Page/丹尼爾·戴-劉易斯.md "wikilink")，《[黑金企業](../Page/黑金企業.md "wikilink")》（There
    Will Be Blood）
  - 最佳女主角：[瑪莉詠·柯蒂亞](../Page/瑪莉詠·柯蒂亞.md "wikilink")，《[玫瑰人生](../Page/玫瑰人生_\(电影\).md "wikilink")》（La
    Môme）
  - 最佳動畫影片：[布萊德·柏德執導的](../Page/布萊德·柏德.md "wikilink")《[料理鼠王](../Page/料理鼠王.md "wikilink")》（Ratatouille）
  - 最佳紀錄片：Alex Gibney 跟 Eva Orner
    執導的《[通往暗巷的計程車](../Page/通往暗巷的計程車.md "wikilink")》（Taxi
    to the Dark Side）
  - 最佳外語片：[奧地利](../Page/奧地利.md "wikilink") Stefan Ruzowitzky
    的《[偽鈔製造者](../Page/偽鈔製造者.md "wikilink")》（Die
    Fälscher/The Counterfeiters）

### 歐洲電影獎（第21屆）

  - 最佳影片：
  - 最佳導演：
  - 最佳劇本：
  - 最佳男主角：
  - 最佳女主角：

### 台灣金馬獎（第45屆）

  - 最佳影片：[陳可辛](../Page/陳可辛.md "wikilink")
    -《[投名狀](../Page/投名狀.md "wikilink")》
  - 最佳導演：[陳可辛](../Page/陳可辛.md "wikilink")
    -《[投名狀](../Page/投名狀.md "wikilink")》
  - 最佳原著劇本：[蔡宗翰](../Page/蔡宗翰.md "wikilink")、[林書宇](../Page/林書宇.md "wikilink")
    -《[九降風](../Page/九降風.md "wikilink")》
  - 最佳改編劇本：[劉恆](../Page/劉恆.md "wikilink")
    -《[集結號](../Page/集結號.md "wikilink")》
  - 最佳男主角：[張涵予](../Page/張涵予.md "wikilink")
    -《[集結號](../Page/集結號.md "wikilink")》
  - 最佳女主角：[劉美君](../Page/劉美君.md "wikilink")
    -《[我不賣身．我賣子宮](../Page/我不賣身．我賣子宮.md "wikilink")》
  - 終身成就獎：[常楓](../Page/常楓.md "wikilink")
  - 國際影評人費比西獎：[鍾孟宏](../Page/鍾孟宏.md "wikilink")
    -《[停車](../Page/停車.md "wikilink")》

## 國際影展

### 柏林影展（第58屆）

#### 評審團

  - 評審團主席：COSTA-GAVRAS，──[柯斯達加華斯](../Page/柯斯達加華斯.md "wikilink")，評審團主席，[法國男導演](../Page/法國.md "wikilink")
  - Uli HANISCH，──烏利·哈尼許，[德國男美術指導](../Page/德國.md "wikilink")
  - Diane KRUGER，──黛安·克魯格，[德國女演員](../Page/德國.md "wikilink")
  - Walter MURCH，──華特·莫屈，[美國男音效設計師](../Page/美國.md "wikilink")
  - Alexander
    RODNIANSKY，──亞歷桑德·羅德尼安斯基，[俄羅斯男製片](../Page/俄羅斯.md "wikilink")
  - [舒淇](../Page/舒淇.md "wikilink")（SHU
    Qui），[台灣女演員](../Page/台灣.md "wikilink")

#### 得獎名單

  - [金熊獎](../Page/金熊獎.md "wikilink")：José PADILHA - **TROPA DE
    ELITE**《[菁英部隊](../Page/菁英部隊.md "wikilink")》
  - 評審團大獎：Errol MORRIS - **S.O.P. STANDARD OPERATING
    PROCEDURE**《[標準作業程序](../Page/標準作業程序（影片）.md "wikilink")》
  - 最佳導演：Paul Thomas ANDERSON - **THERE WILL BE
    BLOOD**《[黑金企業](../Page/黑金企業.md "wikilink")》
  - 最佳男演員：Reza NAJIE - **THE SONG OF SPARROWS**《麻雀輕歌》
  - 最佳女演員：Sally HAWKINS - **HAPPY-GO-LUCKY**《無憂無慮》
  - 最佳劇本：[王小帥](../Page/王小帥.md "wikilink")（WANG Xiaoshuai）-
    《[左右](../Page/左右.md "wikilink")》（**In Love We Trust**）
  - 最佳藝術貢獻：Johnny GREENWOOD - **THERE WILL BE
    BLOOD**《[黑金企業](../Page/黑金企業.md "wikilink")》
  - 榮譽金熊獎（終身成就獎）：Francesco ROSI

更詳細的名單，請見[第58屆柏林電影節](../Page/第58屆柏林電影節.md "wikilink")

### 坎城影展（第61屆）

#### 評審團

  - 評審團主席：Sean
    PENN，──[西恩·潘](../Page/西恩·潘.md "wikilink")，[美國男演員兼導演](../Page/美國.md "wikilink")

#### 得獎名單

  - [金棕櫚獎](../Page/金棕櫚獎.md "wikilink")：[洛宏·康鐵](../Page/洛宏·康鐵.md "wikilink")
    - **ENTRE LES MURS**《[圍牆之間](../Page/圍牆之間.md "wikilink")》
  - 評審團大獎：Matteo GARRONE - **GOMORRA**《蛾摩拉》
  - 最佳導演：Nuri Bilge CEYLAN - **ÜÇ MAYMUN**《三隻猴子》
  - 最佳劇本：[達頓兄弟](../Page/達頓兄弟.md "wikilink") - **LE SILENCE DE
    LORNA**《羅娜的沈默》
  - 最佳女演員：Sandra CORVELONI - Walter SALLES 執導的 **LINHA DE PASSE**《通行路線》
  - 最佳男演員：Benicio Del TORO - [史蒂芬·索德柏執導的](../Page/史蒂芬·索德柏.md "wikilink")
    **CHE**《切》
  - 評審團獎：Paolo SORRENTINO - **IL DIVO**《大牌明星》
  - [金攝影機獎](../Page/金攝影機獎.md "wikilink")：[史提夫麥昆](../Page/史提夫麥昆.md "wikilink")
    - **HUNGER**《大絕食》

更詳細的名單，請見[第61届戛纳国际电影节](../Page/第61届戛纳国际电影节.md "wikilink")

### 威尼斯影展（第65屆）

#### 評審團

  - 評審團主席：Wim Wenders
    ──[文·溫德斯](../Page/文·溫德斯.md "wikilink")，[德國導演](../Page/德國.md "wikilink")
  - Juriy Arabov ──[俄羅斯編劇](../Page/俄羅斯.md "wikilink")。
  - Valeria Golino
    ──[瓦萊莉·高利諾](../Page/瓦萊莉·高利諾.md "wikilink")，[義大利女星](../Page/義大利.md "wikilink")。
  - Douglas Gordon
    ──[道格拉斯·戈登](../Page/道格拉斯·戈登.md "wikilink")，[英國視覺藝術家](../Page/英國.md "wikilink")。
  - John Landis
    ──[約翰·蘭迪斯](../Page/約翰·蘭迪斯.md "wikilink")，[美國導演](../Page/美國.md "wikilink")。
  - Lucrecia Martel
    ──[盧奎西亞·馬特爾](../Page/盧奎西亞·馬特爾.md "wikilink")，[阿根廷導演](../Page/阿根廷.md "wikilink")。
  - [杜琪峰](../Page/杜琪峰.md "wikilink")，[香港導演](../Page/香港.md "wikilink")。

#### 得獎名單

  - [金獅獎](../Page/金獅獎.md "wikilink")：[達倫·阿洛諾夫斯基](../Page/達倫·阿洛諾夫斯基.md "wikilink")（Darren
    Aronofsky）－《[力挽狂瀾](../Page/力挽狂瀾.md "wikilink")》（The Wrestler）
  - 最佳導演銀獅獎：[小阿萊克斯·吉爾曼](../Page/小阿萊克斯·吉爾曼.md "wikilink")（Aleksey German
    Jr.）－《[紙戰士](../Page/紙戰士.md "wikilink")》（Bumažnyj Soldat (Paper
    Soldier)）
  - 評審團特別獎：[海爾·格里瑪](../Page/海爾·格里瑪.md "wikilink")（Haile
    Gerima）－《[泰莎](../Page/泰莎.md "wikilink")》（Teza）
  - 最佳男演員：[西爾維奧·奧蘭多](../Page/西爾維奧·奧蘭多.md "wikilink")（Silvio
    Orlando）－[普皮·阿瓦提](../Page/普皮·阿瓦提.md "wikilink")（Pupi
    Avati）執導的《[喬凡娜的教宗](../Page/喬凡娜的教宗.md "wikilink")》（Il papà di
    Giovanna）
  - 最佳女演員：[多米尼克·布蘭克](../Page/多米尼克·布蘭克.md "wikilink")（Dominique
    Blanc）－[帕特里克-馬裏奧·伯納德](../Page/帕特里克-馬裏奧·伯納德.md "wikilink")（Patrick
    Mario Bernard）與[皮耶爾·特里維蒂克](../Page/皮耶爾·特里維蒂克.md "wikilink")（Pierre
    Trividic）執導的《[另一個人](../Page/另一個人.md "wikilink")》（L'autre）
  - 最佳新人獎：[珍妮弗·勞倫斯](../Page/珍妮弗·勞倫斯.md "wikilink")（Jennifer
    Lawrence）－[吉勒莫·亞瑞格執導的](../Page/吉勒莫·亞瑞格.md "wikilink")《[燃燒的平原](../Page/燃燒的平原.md "wikilink")》（Burning
    Plain）
  - 最佳技術貢獻：lisher Khamidhodjaev/Maxim Drozdov －《紙戰士》（Bumažnyj Soldat
    (Paper Soldier)）
  - 最佳劇本：[海爾·格里瑪](../Page/海爾·格里瑪.md "wikilink")（Haile
    Gerima）－《[泰莎](../Page/泰莎.md "wikilink")》（Teza）
  - 特別金獅獎：[沃納·施羅特](../Page/沃納·施羅特.md "wikilink")（Wender
    Schroeter）－《[犬之夜](../Page/犬之夜.md "wikilink")》（Nuit de
    chien）
  - 最佳處女作獎：[吉安尼·迪·格雷格里奧](../Page/吉安尼·迪·格雷格里奧.md "wikilink")（Gianni Di
    Gergorio）－《[八月中旬的午餐](../Page/八月中旬的午餐.md "wikilink")》（Pranzo di
    Ferragosto）
  - 終身成就金獅獎：[艾瑪諾·奧米](../Page/艾瑪諾·奧米.md "wikilink")（Ermanno Olmi）

更詳細的名單，請見[第65届威尼斯国际电影节](../Page/第65届威尼斯国际电影节.md "wikilink")

## 逝世影人

### 第一季

  - [1月15日](../Page/1月15日.md "wikilink")：[布萊德·藍佛](../Page/布萊德·藍佛.md "wikilink")（Brad
    Renfro，[美國男演員](../Page/美國.md "wikilink")，得年25歲）
  - [1月22日](../Page/1月22日.md "wikilink")：[希斯·萊傑](../Page/希斯·萊傑.md "wikilink")（Heath
    Ledger，[澳洲男演員](../Page/澳洲.md "wikilink")，得年28歲）
  - [1月23日](../Page/1月23日.md "wikilink")：[岑範](../Page/岑範.md "wikilink")（[中國男導演](../Page/中國.md "wikilink")，享壽82歲）
  - [2月10日](../Page/2月10日.md "wikilink")：[萊·史德](../Page/萊·史德.md "wikilink")（Roy
    Scheider，[美國男演員](../Page/美國.md "wikilink")，享壽75歲）
  - [2月13日](../Page/2月13日.md "wikilink")：[市川崑](../Page/市川崑.md "wikilink")（，[日本男導演](../Page/日本.md "wikilink")，享壽92歲）
  - [2月18日](../Page/2月18日.md "wikilink")：[阿蘭·羅伯-格里耶](../Page/阿蘭·羅伯-格里耶.md "wikilink")（Alain
    Robbe-Grillet，[法國男作家兼製片](../Page/法國.md "wikilink")，享壽85歲）
  - [2月19日](../Page/2月19日.md "wikilink")：[沈殿霞](../Page/沈殿霞.md "wikilink")（[香港女演員](../Page/香港.md "wikilink")，享壽62歲）
  - [3月12日](../Page/3月12日.md "wikilink")：[李昆](../Page/李昆.md "wikilink")（[台灣男演員](../Page/台灣.md "wikilink")，享壽78歲）
  - [3月18日](../Page/3月18日.md "wikilink")：[安東尼·明格拉](../Page/安東尼·明格拉.md "wikilink")（Anthony
    Minghella，[英國男導演兼編劇](../Page/英國.md "wikilink")，享年54歲）
  - [3月19日](../Page/3月19日.md "wikilink")：[保羅·史高菲](../Page/保羅·史高菲.md "wikilink")（Paul
    Scofield，[英國男演員](../Page/英國.md "wikilink")，享壽86歲）
  - [3月31日](../Page/3月31日.md "wikilink")：[朱爾斯·達辛](../Page/朱爾斯·達辛.md "wikilink")（Jules
    Dassin，[美國男導演](../Page/美國.md "wikilink")，享壽91歲）

### 第二季

  - [4月5日](../Page/4月5日.md "wikilink")：[卻爾登·希斯頓](../Page/卻爾登·希斯頓.md "wikilink")（Charlton
    Heston，[美國男演員](../Page/美國.md "wikilink")，享壽83歲）
  - [5月15日](../Page/5月15日.md "wikilink")：[汪禹](../Page/汪禹.md "wikilink")（[香港男演員](../Page/香港.md "wikilink")，享年53歲）
  - [5月24日](../Page/5月24日.md "wikilink")：[勞勃·諾克斯](../Page/勞勃·諾克斯.md "wikilink")（Rob
    Knox，[英國男演員](../Page/英國.md "wikilink")，得年18歲）
  - [5月26日](../Page/5月26日.md "wikilink")：[薛尼·波勒](../Page/薛尼·波勒.md "wikilink")（Sydney
    Pollack，[美國男導演](../Page/美國.md "wikilink")，享壽73歲）
  - [5月27日](../Page/5月27日.md "wikilink")：[宋存壽](../Page/宋存壽.md "wikilink")（[台灣男導演](../Page/台灣.md "wikilink")，享壽78歲）
  - [6月7日](../Page/6月7日.md "wikilink")：[迪諾·李西](../Page/迪諾·李西.md "wikilink")（Dino
    Risi，[義大利男導演](../Page/義大利.md "wikilink")，享壽91歲）
  - [6月17日](../Page/6月17日.md "wikilink")：[賽德·查里斯](../Page/賽德·查里斯.md "wikilink")（Cyd
    Charisse，[美國女演員](../Page/美國.md "wikilink")，享壽86歲）
  - [6月22日](../Page/6月22日.md "wikilink")：[喬治·卡林](../Page/喬治·卡林.md "wikilink")（George
    Carlin，[美國男喜劇演員](../Page/美國.md "wikilink")，享壽71歲）

### 第三季

  - [8月9日](../Page/8月9日.md "wikilink")：[柏尼·麥克](../Page/柏尼·麥克.md "wikilink")（Bernie
    Mac，[美國男演員](../Page/美國.md "wikilink")，享年50歲）
  - [8月21日](../Page/8月21日.md "wikilink")：[李言](../Page/李言.md "wikilink")（，[韓國男演員兼模特兒](../Page/韓國.md "wikilink")，得年27歲）
  - [8月26日](../Page/8月26日.md "wikilink")：[深浦加奈子](../Page/深浦加奈子.md "wikilink")（，[日本女演員](../Page/日本.md "wikilink")，享年48歲）
  - [9月19日](../Page/9月19日.md "wikilink")：[市川準](../Page/市川準.md "wikilink")（，[日本男導演](../Page/日本.md "wikilink")，享年59歲）
  - [9月26日](../Page/9月26日.md "wikilink")：[保羅·紐曼](../Page/保羅·紐曼.md "wikilink")（Paul
    Newman，[美國男演員兼導演](../Page/美國.md "wikilink")，享壽83歲）

### 第四季

  - [10月5日](../Page/10月5日.md "wikilink")：[緒形拳](../Page/緒形拳.md "wikilink")（，[日本男演員](../Page/日本.md "wikilink")，享壽71歲）
  - [10月18日](../Page/10月18日.md "wikilink")：[謝晉](../Page/謝晉.md "wikilink")（[中國男導演](../Page/中國.md "wikilink")，享壽84歲）
  - [11月5日](../Page/11月5日.md "wikilink")：[麥可·克萊頓](../Page/麥可·克萊頓.md "wikilink")（Michael
    Crichton，[美國男編劇](../Page/美國.md "wikilink")、製片兼導演，享壽66歲）

## 參見

  - [2008年電影列表](../Page/2008年電影列表.md "wikilink")

[電影](../Category/2008年.md "wikilink")
[2008年電影](../Category/2008年電影.md "wikilink")