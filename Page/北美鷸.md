**北美鷸**（學名*Tringa semipalmata*，舊學名*Catoptrophorus semipalmatus*：Pereira
&
貝克，2005年；班克斯等，2006年），是[鷸科的一種大型](../Page/鷸科.md "wikilink")[水鳥](../Page/水鳥.md "wikilink")。它的近親是[杓鷂](../Page/杓鷂.md "wikilink")。

成年北美鷸的腿部為灰色，並長有粗長、平直的深色喙。身體下部為深灰色，上部為淺色。尾白色，末端為一條黑帶。翅膀為特別的黑白樣式，在[北美洲許多沿海海灘都有出現](../Page/北美洲.md "wikilink")。

包括兩個亞種（過去被認爲是不同的種）的繁殖棲息地和範圍不同，東部亞種分佈在[加拿大](../Page/加拿大.md "wikilink")[新斯科舍省到](../Page/新斯科舍省.md "wikilink")[墨西哥和](../Page/墨西哥.md "wikilink")[加勒比海岸地區](../Page/加勒比.md "wikilink")，在[南美的](../Page/南美.md "wikilink")[大西洋海岸過冬](../Page/大西洋.md "wikilink")。西部亞種則生活在[北美西部的淡水沼澤地帶](../Page/北美.md "wikilink")，冬天前往南美的東西兩個海岸過冬。

在地面上筑巢，通常隱藏在矮草叢重。在泥灘或淺水中覓食，主要吃昆蟲、貝類和水生蠕蟲，有時也吃植物。

北美鷸的數量在十九世紀末、二十世紀初因捕獵而大量減少，現在數量已開始增加，但由於棲息地的不斷減少而仍然面臨威脅。

## 参見

  - **Banks**, Richard C.; Cicero, Carla; Dunn, Jon L.; Kratter, Andrew
    W.; Rasmussen, Pamela C.; Remsen, J. V. Jr.; Rising, James D. &
    Stotz, Douglas F. (2006): Forty-seventh Supplement to the American
    Ornithologists' Union Check-list of North American Birds.
    *[Auk](../Page/Auk_\(journal\).md "wikilink")* **123**(3): 926–936.
    [DOI](../Page/Digital_Object_Identifier.md "wikilink"):
    10.1642/0004-8038(2006)123\[926:FSTTAO\]2.0.CO;2 [PDF
    fulltext](http://www.aou.org/checklist/Suppl47.pdf)

<!-- end list -->

  - Database entry includes justification for why this species is of
    least concern

<!-- end list -->

  - **Pereira**, Sérgio Luiz & **Baker**, Alan J. (2005): Multiple Gene
    Evidence for Parallel Evolution and Retention of Ancestral
    Morphological States in the Shanks (Charadriiformes: Scolopacidae).
    *[Condor](../Page/Condor_\(journal\).md "wikilink")* **107**(3):
    514–526. [DOI](../Page/Digital_Object_Identifier.md "wikilink"):
    10.1650/0010-5422(2005)107\[0514:MGEFPE\]2.0.CO;2 [HTML
    abstract](http://www.bioone.org/perlserv/?request=get-abstract&issn=0010-5422&volume=107&issue=03&page=0514)

## 外部連結

  - Connecticut [Department of Environmental
    Protection](../Page/Department_of_Environmental_Protection.md "wikilink"):
    [Wildlife in Connecticut - Endangered and Threatened Species Series:
    Willet](https://web.archive.org/web/20061006113828/http://dep.state.ct.us/burnatr/wildlife/factshts/willet.htm)
  - [Willet Species
    Account](http://www.birds.cornell.edu/AllAboutBirds/BirdGuide/Willet.html)
    - Cornell Lab of Ornithology
  - [Willet Information and
    Photos](http://www.sdakotabirds.com/species/willet_info.htm) - South
    Dakota Birds and Birding
  - [Willet - *Catoptrophorus
    semipalmatus*](http://www.mbr-pwrc.usgs.gov/id/framlst/i2580id.html)
    - USGS Patuxent Bird Identification InfoCenter

[TS](../Category/IUCN無危物種.md "wikilink")
[TS](../Category/美國鳥類.md "wikilink")
[semipalmata](../Category/鷸屬.md "wikilink")