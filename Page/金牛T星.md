[TTauriStarDrawing.jpg](https://zh.wikipedia.org/wiki/File:TTauriStarDrawing.jpg "fig:TTauriStarDrawing.jpg")

**金牛T星**（**T Tauri star,
TTS**）是變星的一種，他的命名是依據被發現的原型－[金牛座T星](../Page/金牛座T.md "wikilink")（）而來的。他們都在鄰近[分子雲的地方被發現](../Page/分子雲.md "wikilink")，例如[NGC
1555](../Page/NGC_1555.md "wikilink")，並且由光學上的觀測確認是一顆有著強烈的[色球譜線的](../Page/色球.md "wikilink")[變星](../Page/變星.md "wikilink")。

## 特徵

金牛T星是[前主序帶](../Page/主序前星.md "wikilink")－在F、G、K、M[光譜類型](../Page/光譜類型.md "wikilink")－能見到的最年輕恆星（質量小於2[太陽質量](../Page/太陽.md "wikilink")），表面溫度非常接近質量相同的同類型[主序星](../Page/主序星.md "wikilink")，但是因為半徑較大而顯得較為明亮，它們的中心溫度仍太低，以至於還不足以引發[氫融合](../Page/質子-質子鏈反應.md "wikilink")。取而代之的是以收縮產生的重力能量朝向[主序帶移動](../Page/主序帶.md "wikilink")，而大約在一億年後可以成為主序星。它們典型的自轉週期在1至12天之間，與太陽一個月的自轉週期比較，顯得是非常活躍和多變的。

有證據顯示有巨大的[星斑覆蓋在表面](../Page/星斑.md "wikilink")，並且有強烈和易變的[X射線和](../Page/X射線.md "wikilink")[電波輻射](../Page/電波.md "wikilink")（強度約為太陽的1,000倍），許多還都有強烈的[恆星風](../Page/恆星風.md "wikilink")。造成光度變化的另一個原因是環繞在金牛T星周圍的團塊（[原行星和](../Page/原行星.md "wikilink")[微行星](../Page/微行星.md "wikilink")）。

它們的光譜顯示有比太陽和其他主序星更高的[鋰豐度](../Page/鋰.md "wikilink")，而鋰在2,500,000K的溫度上就會被毀壞。從對53顆金牛T星的研究，發現鋰的損耗與大小有強烈的關係，認為[鋰燃燒是由](../Page/鋰燃燒.md "wikilink")[PP鏈完成的](../Page/質子-質子鏈反應.md "wikilink")，當在[前主序帶的最後階段會有強烈的對流和不穩定階段](../Page/主序前星.md "wikilink")，之後的[林收縮可能是金牛T星主要的能量來源之一](../Page/林軌跡.md "wikilink")。快速的自轉有助於將鋰混合和輸送至更深處，而在哪兒造成毀壞。因為角動量的守恆，金牛T星通常隨著年齡，經由收縮而加快自轉速度。這導致鋰的損耗也隨著年齡增長而增加，鋰的燃燒也會使溫度和質量上升，持續的時間大約是一億年上下（100,000,000年）。

在P-P鏈中鋰的燃燒如下式：

  - \(p^+ + {}^{6}Li\ \rightarrow\ {}^{7}Li\)
  - \(p^+ + {}^{7}Li\ \rightarrow\ {}^{8}Be\) (unstable)
  - \({}^{8}Be\ \rightarrow\ {}^{4}He + {}^{4}He + \mbox{energy}\)

這種反應在質量低於60個木星質量時不會發生。這樣，鋰的消耗程度就可以用來計算恆星的年齡。

[M42proplyds.jpg](https://zh.wikipedia.org/wiki/File:M42proplyds.jpg "fig:M42proplyds.jpg")的[原行星盤](../Page/原行星盤.md "wikilink")。\]\]
粗估大約有一半的金牛T星有[星周盤](../Page/星周盤.md "wikilink")，在這種情況下稱為[原行星盤](../Page/原行星盤.md "wikilink")，因為它們大概就是像太陽系的[行星系統的祖先](../Page/行星系統.md "wikilink")，而估計經過一千萬（10<sup>7</sup>）年拱星盤就會消散。許多的金牛T星都是聯星，在他們生命的不同階段中，它們都可以稱為[初期恆星體](../Page/初期恆星體.md "wikilink")（，YSOs）。有活躍的[磁場和強烈](../Page/磁場.md "wikilink")[恆星風的金牛T星被認為會有](../Page/恆星風.md "wikilink")[阿爾文波](../Page/阿爾文波.md "wikilink")，意味著[角動量能從恆星傳送到](../Page/角動量.md "wikilink")[原行星盤](../Page/原行星盤.md "wikilink")。一個為我們的[太陽系被假設的金牛T星階段是](../Page/太陽系.md "wikilink")[角動量由收縮中的](../Page/角動量.md "wikilink")[太陽轉移到原行星盤](../Page/太陽.md "wikilink")，如此最後才能產生行星。

在大質量（2至8太陽質量）－A和B[光譜類型](../Page/光譜類型.md "wikilink")－範圍內類似金牛T星的[主序前星稱為](../Page/主序前星.md "wikilink")[赫比格Ae/Be星](../Page/赫比格Ae/Be星.md "wikilink")。質量更重的[主序前星](../Page/主序前星.md "wikilink")，因為發展的太快速了，還沒有被觀測到：當它們能被看見時（這是指拱星盤周圍的氣體和雲氣消散），核心的氫已經在燃燒中，因此已經是一顆[主序星了](../Page/主序星.md "wikilink")。

## 參考資料

  - [Discussion of V471 Tauri observations and general T-Tauri
    properties](https://web.archive.org/web/20110524172358/http://universe.gsfc.nasa.gov/seminars/presentations/walter/toc.html)
  - [An empirical criterion to classify T Tauri stars and substellar
    analogs using low-resolution optical
    spectroscopy](http://arxiv.org/abs/astro-ph/0309284), David Barrado
    y Navascues, 2003

[Category:恆星類型](../Category/恆星類型.md "wikilink")
[\*](../Category/金牛T星.md "wikilink")
[Category:恆星形成](../Category/恆星形成.md "wikilink")
[Category:獵戶型變星](../Category/獵戶型變星.md "wikilink")