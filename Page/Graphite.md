**Graphite**是一套由[SIL開發的](../Page/SIL國際.md "wikilink")，跟[Unicode相容的可編程](../Page/Unicode.md "wikilink")構建（）及技術系統。它是一套[自由的編程介面](../Page/自由軟件.md "wikilink")，透過[GNU宽通用公共许可证及](../Page/GNU宽通用公共许可证.md "wikilink")發行。

## 與其他智慧字型技術的相容度與比較

Graphite是一個建基於[TrueType字型格式的技術](../Page/TrueType.md "wikilink")，並新增了三個自用的新表格。它允許各種智慧構建字體的規則，包括有：[連體字](../Page/合字.md "wikilink")、字型替換、字型插入、字型重排、[標音符定錨](../Page/標音符.md "wikilink")、[縮排及](../Page/縮排.md "wikilink")[對齊](../Page/對齊.md "wikilink")。Graphite的的規則可以對內容敏感，從而可自動調節要採用的字形。例如：把所有非結尾的
[s](../Page/s.md "wikilink") 字用 [ſ](../Page/長s.md "wikilink") 替代。

Graphite字型會把所有字型構建信息直接儲在字型檔裡，所以要顯示Graphite的智慧字型構建，應用程式只需要提供為Graphite字型的支援，而無需知道有關文字的書寫方式。因此，Graphite系統的設計極為適合作為無法依賴系統內建之字型構建信息的少數民族書寫系統。從這方面來看，Graphite的性質與蘋果電腦開發的[AAT類似](../Page/AAT.md "wikilink")，但與要求應用軟件提供對內建字型構建信息的[OpenType又不同](../Page/OpenType.md "wikilink")。

## 對Graphite的支援

Graphite本來是一套為[Windows環境上的使用而設計的系統](../Page/Microsoft_Windows.md "wikilink")，後來亦移植到[Linux](../Page/Linux.md "wikilink")，還被移植到[macOS](../Page/macOS.md "wikilink")
10.6\[1\]，即使在macOS電腦上蘋果公司已提供有，同樣提供適宜少數民族文字的支援。

目前支援Graphite的應用程式計有：SIL
WorldPad\[2\]、[XeTeX](../Page/XeTeX.md "wikilink")、[OpenOffice.org](../Page/OpenOffice.org.md "wikilink")（從3.2版本開始，不包括macOS版本）、[LibreOffice](../Page/LibreOffice.md "wikilink")（原先不包括macOS版本，5.3版本開始用於全平台\[3\]）。[Thunderbird](../Page/Mozilla_Thunderbird.md "wikilink")
11 及 [Firefox](../Page/Firefox.md "wikilink")
11也都開始內建支援\[4\]，從22版本開始預設啟用，但到45.0.1版開始又再關掉，並於49.0版恢復\[5\]\[6\]。

Graphite的支援可以用pango-graphite软件包添加到[Linux上应用](../Page/Linux.md "wikilink")\[7\]，而在Windows平台上可安裝扩展性的附加组件MultiScribe\[8\]。

## 參看

  - [OpenType](../Page/OpenType.md "wikilink")

  - [Apple Advanced
    Typography](../Page/Apple_Advanced_Typography.md "wikilink")

  - [Uniscribe](../Page/Uniscribe.md "wikilink")

  - [HarfBuzz](../Page/HarfBuzz.md "wikilink")

  -
## 参考文献

## 外部連結

  - [Graphite在SIL的官方網址](http://graphite.sil.org)

  - [Graphite字型](http://scripts.sil.org/GraphiteFonts)

  - [SIL Graphite在Sourceforge的網址](http://silgraphite.sf.net)

  -
  - [Project
    SILA](http://sila.mozdev.org/)——Graphite与[Mozilla整合计划](../Page/Mozilla.md "wikilink")

  - [Presentation of Graphite for aKademy 2007, by S
    Correll](http://www.unifont.org/TextLayout2007/presentations/SharonCorrellSILGraphiteNotes.pdf)

[Category:數碼字體排印](../Category/數碼字體排印.md "wikilink")
[Category:自由排版軟體](../Category/自由排版軟體.md "wikilink")
[Category:字型格式](../Category/字型格式.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.