**屠滽**\[1\]（），[字](../Page/字.md "wikilink")**朝宗**，[号](../Page/号.md "wikilink")**西峰**，又號**丹山**，[浙江](../Page/浙江承宣布政使司.md "wikilink")[鄞縣](../Page/鄞縣.md "wikilink")（今[宁波市](../Page/宁波市.md "wikilink")）人，[祖籍](../Page/祖籍.md "wikilink")[直隸](../Page/南直隸.md "wikilink")[無錫](../Page/無錫.md "wikilink")（今[江蘇](../Page/江蘇.md "wikilink")）\[2\]，[明朝政治人物](../Page/明朝.md "wikilink")，[進士出身](../Page/進士.md "wikilink")。

## 经历

[成化二年](../Page/成化.md "wikilink")（1466年）第三甲[进士](../Page/进士.md "wikilink")，任[监察御史](../Page/监察御史.md "wikilink")，[巡按](../Page/巡按.md "wikilink")[四川](../Page/四川.md "wikilink")、[湖广](../Page/湖广.md "wikilink")，为官有清名，勤于政事。历任[右佥都御史](../Page/右佥都御史.md "wikilink")、[右都御史](../Page/右都御史.md "wikilink")、[左都御史](../Page/左都御史.md "wikilink")。[弘治十年](../Page/弘治_\(明朝\).md "wikilink")（1497年），进[兵部尚书](../Page/兵部尚书.md "wikilink")，后调任[吏部尚书](../Page/吏部尚书.md "wikilink")，兼[都察院](../Page/都察院.md "wikilink")[御史](../Page/御史.md "wikilink")，封[太子太保](../Page/太子太保.md "wikilink")。[明武宗时](../Page/明武宗.md "wikilink")，加封[太子太傅](../Page/太子太傅.md "wikilink")，兼掌院事，继加封[柱国](../Page/柱国.md "wikilink")。[谥](../Page/谥.md "wikilink")**襄惠**。乡人称“屠天官”\[3\]。

## 家族

屠滽为甬上屠氏七世祖，天官房\[4\]。

子[屠俓](../Page/屠俓.md "wikilink")，[字](../Page/表字.md "wikilink")**貞卿**，[號](../Page/號.md "wikilink")**樾崖**，[正德六年](../Page/正德.md "wikilink")（1511年）登进士，官至[吏部](../Page/吏部.md "wikilink")[員外郎](../Page/員外郎.md "wikilink")。

## 著作

著有《丹山集》、《屠襄惠公遗集》（其孙编）

## 軼事

有一回，屠滽退朝後在[尚書官邸閒居時](../Page/尚書_\(官職\).md "wikilink")，底下的辦事人員不小心將[墨汁灑在他的白衣上](../Page/墨汁.md "wikilink")，對方頓感惶懼而屏息，急忙向屠滽磕頭請罪，屠滽則很有雅量的對他說：「去去汝何為者，吾方惡其泰白而易汙也。」
\[5\]

## 遗迹

  - 屠滽故居，位于宁波尚书街。
  - 今宁波屠滽墓神道碑，高4.16米，始立于明[正德十四年](../Page/正德_\(明朝\).md "wikilink")（1519年），位于宁波集士港镇山下庄村委会旁。
  - [江苏省](../Page/江苏省.md "wikilink")[常熟市](../Page/常熟市.md "wikilink")[虞山明](../Page/虞山.md "wikilink")[温州](../Page/温州.md "wikilink")[知府陆润夫妇合葬墓碑铭](../Page/知府.md "wikilink")[书丹](../Page/书丹.md "wikilink")

## 註釋

## 參考文獻

  - 焦竑，《國朝獻徵錄》

[Category:明朝監察御史](../Category/明朝監察御史.md "wikilink")
[Category:明朝右僉都御史](../Category/明朝右僉都御史.md "wikilink")
[Category:明朝右都御史](../Category/明朝右都御史.md "wikilink")
[Category:明朝左都御史](../Category/明朝左都御史.md "wikilink")
[Category:明朝兵部尚書](../Category/明朝兵部尚書.md "wikilink")
[Category:明朝吏部尚書](../Category/明朝吏部尚書.md "wikilink")
[Category:明朝太子三師](../Category/明朝太子三師.md "wikilink")
[Category:谥襄惠](../Category/谥襄惠.md "wikilink")
[Category:鄞县人](../Category/鄞县人.md "wikilink")
[Category:甬上屠氏](../Category/甬上屠氏.md "wikilink")
[Y](../Category/屠姓.md "wikilink")

1.  后世常訛作**屠庸**
2.  [天一阁里，屠呦呦家谱亮相 -
    钱江晚报](http://qjwb.zjol.com.cn/html/2015-12/10/content_3227480.htm?div=-1)
3.  [清明扫墓之山下庄明代屠庸（屠天官）墓遗迹_读书的明州人_新浪博客](http://blog.sina.cn/dpool/blog/s/blog_4b725d980101jcbf.html)
4.  [屠墓推案第三季：祖屠瑜，孙屠健_独立观察员_新浪博客](http://blog.sina.com.cn/s/blog_4423cedf0102vlm0.html)
5.  [明](../Page/明.md "wikilink").[焦竑](../Page/焦竑.md "wikilink")，《[國朝獻徵錄](../Page/國朝獻徵錄.md "wikilink")》（卷24）：“太宰屠襄惠公滽，部堂燕居，令辦事官捧硯時，
    公新衣白綾甚澤，其人誤傾硯汁，狼藉公衣，惶懼攝息，頓顙請罪，公曰：去去汝何為者，吾方惡其泰白而易汙也。”