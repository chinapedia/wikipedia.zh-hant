**瓦迪斯瓦夫三世**（，），[雅盖隆王朝时期的波兰国王](../Page/雅盖隆王朝时期的波兰.md "wikilink")（1434年～1444年在位）和[匈牙利国王](../Page/匈牙利.md "wikilink")（称**乌拉斯洛一世**，[匈牙利语](../Page/匈牙利语.md "wikilink")：*I.
Ulászló*；1440年-1444年）。

瓦迪斯瓦夫三世是[雅盖隆王朝时期的波兰的统治者](../Page/雅盖隆王朝时期的波兰.md "wikilink")[亚盖洛的长子](../Page/瓦迪斯瓦夫二世.md "wikilink")，继承了其父的波兰王位。他在[教皇](../Page/教皇.md "wikilink")[尤金四世的支持下](../Page/尤金四世.md "wikilink")，与[德意志兼匈牙利国王](../Page/德意志.md "wikilink")[阿尔布雷希特二世的遗孀伊丽莎白为争夺匈牙利王位进行内战](../Page/阿尔布雷希特二世_\(德意志\).md "wikilink")，最终从阿尔布雷希特年幼的儿子[拉迪斯劳斯手中夺取了匈牙利的王冠](../Page/拉斯洛五世.md "wikilink")。

瓦迪斯瓦夫三世在匈牙利建立的统治带来了与[奥斯曼帝国发生冲突的危险](../Page/奥斯曼帝国.md "wikilink")。奥斯曼的[苏丹们觊觎匈牙利已久](../Page/苏丹_\(称谓\).md "wikilink")，而瓦迪斯瓦夫三世也已向教皇许诺组织反对土耳其人的[十字军](../Page/十字军.md "wikilink")。

1444年11月10日，瓦迪斯瓦夫三世在[保加利亚的](../Page/保加利亚.md "wikilink")[瓦尔纳与苏丹](../Page/瓦尔纳.md "wikilink")[穆拉德二世率领的奥斯曼大军发生激战](../Page/穆拉德二世.md "wikilink")（[瓦尔纳战役](../Page/瓦尔纳战役.md "wikilink")），结果在这场战斗中阵亡。他因此得到外号：“瓦尔纳的”（Warneńczyk）。

## 人物生平

### 早年

[Wladyslaw_Warnenczyk.jpg](https://zh.wikipedia.org/wiki/File:Wladyslaw_Warnenczyk.jpg "fig:Wladyslaw_Warnenczyk.jpg")
瓦迪斯瓦夫是[雅盖沃和](../Page/雅盖沃.md "wikilink")的长子。自十岁登基起，瓦迪斯瓦夫便立刻被由领导的顾问团包围了，后者希望继续享有他在法庭上的显赫地位。除此之外，年轻的瓦迪斯瓦夫和他雄心勃勃的母亲都意识到反对者的存在。尽管他的父亲雅盖沃和波兰权贵间签订了协议，确保了瓦迪斯瓦夫的继承权，但反对者仍然想要另一位波兰王位的候选人——来自勃兰登堡的[腓特烈二世](../Page/腓特烈二世_\(勃兰登堡\).md "wikilink")，瓦迪斯瓦夫的亲姐姐是他的第二任妻子。但Hedwig的死令这一阴谋烟消云散，据传，她是被王后索菲娅毒死的。

### 政治和军事生涯

#### 波兰国王

[Category:波兰君主](../Category/波兰君主.md "wikilink")
[Category:匈牙利国王](../Category/匈牙利国王.md "wikilink")