**部聘教授**指由国家[教育部直接聘任的](../Page/教育部.md "wikilink")[教授](../Page/教授.md "wikilink")，一般用来特指[中华民国大陆时期](../Page/中华民国大陆时期.md "wikilink")[国民政府](../Page/国民政府.md "wikilink")[教育部遴选的两批](../Page/中华民国教育部.md "wikilink")。教育部部聘教授是当时中国教育界的最高荣誉，有人称之为“教授中的教授”。

1941年，国民政府教育部颁行《部聘教授办法》，实行部聘教授制度。由各大学各行各业的教授对本行投票推选，得票最多者被委任为教育部部聘教授。在大学任教10年以上，声誉卓著，对所在学科具有特殊贡献的教授，经教育部学术审议会三分之二以上通过，方可成为部聘教授。部聘教授任期5年，可续聘。

1941年，全国大学共有30位教授被遴选为第一批部聘教授，其中沦陷区2人。1943年，15人当选为第二批部聘教授。

## 部聘教授

  - [胡小石](../Page/胡小石.md "wikilink")（国学，[国立中央大学](../Page/国立中央大学.md "wikilink")）
  - [黎锦熙](../Page/黎锦熙.md "wikilink")（国文，[国立西北师范学院](../Page/国立西北师范学院.md "wikilink")）
  - [杨树达](../Page/杨树达.md "wikilink")（国文，[国立湖南大学](../Page/国立湖南大学.md "wikilink")）
  - [楼光来](../Page/楼光来.md "wikilink")（外文，国立中央大学）
  - [吴宓](../Page/吴宓.md "wikilink")（外文，[国立西南联合大学](../Page/国立西南联合大学.md "wikilink")）
  - [柳詒-{徵}-](../Page/柳詒徵.md "wikilink")（历史，国立中央大学）
  - [陈寅恪](../Page/陈寅恪.md "wikilink")（历史，国立西南联合大学）
  - [萧一山](../Page/萧一山.md "wikilink")（历史，[国立西北大学](../Page/国立西北大学.md "wikilink")）
  - [汤用彤](../Page/汤用彤.md "wikilink")（哲学，国立西南联合大学）
  - [孟宪承](../Page/孟宪承.md "wikilink")（教育，[湖南国立师范学院](../Page/湖南国立师范学院.md "wikilink")）
  - [徐悲鸿](../Page/徐悲鸿.md "wikilink")（艺术，国立中央大学）
  - [艾伟](../Page/艾伟.md "wikilink")（心理，国立中央大学）
  - [孙本文](../Page/孙本文.md "wikilink")（社会，国立中央大学）
  - [刘秉麟](../Page/刘秉麟.md "wikilink")（经济，[国立武汉大学](../Page/国立武汉大学.md "wikilink")）
  - [杨端六](../Page/杨端六.md "wikilink")（经济，国立武汉大学）
  - [杨佑之](../Page/杨佑之.md "wikilink")（经济，[国立四川大学](../Page/国立四川大学.md "wikilink")）
  - [周鲠生](../Page/周鲠生.md "wikilink")（法律，国立武汉大学）
  - [戴修瓒](../Page/戴修瓒.md "wikilink")（法律，国立中央大学）
  - [胡元义](../Page/胡元义.md "wikilink")（法律，国立西北大学）
  - [何鲁](../Page/何鲁.md "wikilink")（数学，[国立重庆大学](../Page/国立重庆大学.md "wikilink")）
  - [胡敦复](../Page/胡敦复.md "wikilink")（数学，[大同大学](../Page/大同大学.md "wikilink")）
  - [苏步青](../Page/苏步青.md "wikilink")（数学，[国立浙江大学](../Page/国立浙江大学.md "wikilink")）
  - [陈建功](../Page/陈建功.md "wikilink")（数学，国立浙江大学）
  - [饶毓泰](../Page/饶毓泰.md "wikilink")（物理，国立西南联合大学）
  - [吴有训](../Page/吴有训.md "wikilink")（物理，国立西南联合大学）
  - [王琎](../Page/王琎.md "wikilink")（化学，国立浙江大学）
  - [曾昭抡](../Page/曾昭抡.md "wikilink")（化学，国立西南联合大学）
  - [高济宇](../Page/高济宇.md "wikilink")（化学，国立中央大学）
  - [李四光](../Page/李四光.md "wikilink")（地质，[国立中央研究院](../Page/国立中央研究院.md "wikilink")）
  - [何杰](../Page/何杰.md "wikilink")（地质，[国立中山大学](../Page/国立中山大学.md "wikilink")）
  - [张其昀](../Page/张其昀.md "wikilink")（地理，国立浙江大学）
  - [胡焕庸](../Page/胡焕庸.md "wikilink")（地理，国立中央大学）
  - [张景钺](../Page/张景钺.md "wikilink")（生物，国立西南联合大学）
  - [常道直](../Page/常道直.md "wikilink")（教育，国立中央大学）
  - [蔡翘](../Page/蔡翘.md "wikilink")（生理，国立中央大学）
  - [洪式闾](../Page/洪式闾.md "wikilink")（病理，[国立江苏医学院](../Page/国立江苏医学院.md "wikilink")）
  - [吴耕民](../Page/吴耕民.md "wikilink")（农学，国立浙江大学）
  - [李凤荪](../Page/李凤荪.md "wikilink")（农学，[湖北省立农学院](../Page/湖北省立农学院.md "wikilink")）
  - [梁希](../Page/梁希.md "wikilink")（林学，国立中央大学）
  - [茅以升](../Page/茅以升.md "wikilink")（土木，[国立交通大学](../Page/国立交通大学.md "wikilink")）
  - [刘仙洲](../Page/刘仙洲.md "wikilink")（机械，国立西南联合大学）
  - [庄前鼎](../Page/庄前鼎.md "wikilink")（机械，国立西南联合大学）
  - [余谦六](../Page/余谦六.md "wikilink")（电机，国立西北工学院）

[中華民國大陸時期部聘教授](../Category/中華民國大陸時期部聘教授.md "wikilink")