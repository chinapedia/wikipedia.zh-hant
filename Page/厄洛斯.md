在[希腊神话中](../Page/希腊神话.md "wikilink")**厄洛斯**（**Eros**，、或;\[1\]，慾望的意思）\[2\]是[希臘神話中的愛與情慾之神](../Page/希臘神祇及神話人物列表.md "wikilink")。他的[罗马同位體是比較多人熟悉的愛神](../Page/罗马神话.md "wikilink")[邱比特](../Page/邱比特.md "wikilink")\[3\]。一些故事裡面認為他是[希腊原始神之一](../Page/希腊原始神.md "wikilink")，另一些故事裡他是[阿佛洛狄忒的兒子](../Page/阿佛洛狄忒.md "wikilink")，是一群帶有翅膀的神[厄洛特斯們之一](../Page/厄洛特斯.md "wikilink")。

## 崇拜與信仰

厄洛斯在古希臘的來源有以下幾個不同的假設，最早的來源[希臘羅馬密教文獻紀載](../Page/希臘羅馬密教.md "wikilink")，厄洛斯是形成宇宙的原始神之一，但之後的來源說明了厄洛斯是[阿佛洛狄忒的兒子](../Page/阿佛洛狄忒.md "wikilink")，是一个手拿弓箭的调皮的胖乎乎小男孩，他的金箭射入人心会产生爱情，他的銀箭射入人心会产生憎恶。這個姿態是文藝復興時期丘比特的前身，而更早期的厄洛斯被描述為一個具有性權威的成年男性，也是一位藝術家。\[4\]\[5\]

## 原始神

根據所有希臘文獻中最古老的來源：[赫西俄德的](../Page/赫西俄德.md "wikilink")[神谱中描述厄洛斯是在](../Page/神谱.md "wikilink")[卡俄斯](../Page/卡俄斯.md "wikilink")、[盖亚](../Page/盖亚.md "wikilink")、[塔耳塔罗斯之後第四個出現的神](../Page/塔耳塔罗斯.md "wikilink")。\[6\]

[荷马則沒有提到厄洛斯](../Page/荷马.md "wikilink")，而一位[前苏格拉底哲学家](../Page/前苏格拉底哲学.md "wikilink")[巴门尼德則將厄洛斯納入原始神](../Page/巴门尼德.md "wikilink")\[7\]

在[俄耳甫斯教和](../Page/俄耳甫斯教.md "wikilink")[厄琉息斯秘仪將厄洛斯作為一個非常早期的神](../Page/厄琉息斯秘仪.md "wikilink")，但不是最早的因為他是夜之女神[倪克斯的後代](../Page/倪克斯.md "wikilink")。\[8\][阿里斯托芬在](../Page/阿里斯托芬.md "wikilink")[俄耳甫斯教描述厄洛斯的誕生](../Page/俄耳甫斯教.md "wikilink")：

## 藝術上的厄洛斯

<File:Ascoli> Satriano Painter - Red-Figure Plate with Eros - Walters
482765.jpg|一個[紅彩陶器與年輕人型態的厄洛斯](../Page/紅彩陶器.md "wikilink")，於[巴爾的摩](../Page/巴爾的摩.md "wikilink")（c.
340-320 BC）。 <File:Eros> bobbin Louvre
CA1798.jpg|厄洛斯被描繪成一個成年男性，[紅彩陶器底部](../Page/紅彩陶器.md "wikilink")（c.
470–450 BC）。 <File:Roman> - Eros - Walters
54724.jpg|沒有翅膀的厄洛斯，於[巴爾的摩](../Page/巴爾的摩.md "wikilink")。
[File:Cupido4b.jpg|厄洛斯與他的弓，於](File:Cupido4b.jpg%7C厄洛斯與他的弓，於)[卡比托利欧博物馆](../Page/卡比托利欧博物馆.md "wikilink")。
<File:Psyché.jpg>|[普西莎与爱神](../Page/普西莎与爱神.md "wikilink")，[安东尼奥·卡诺瓦的作品](../Page/安东尼奥·卡诺瓦.md "wikilink")（1787–1793）。
|An Empire style  depicting the god. France, c. 1822.

## 参见

  - [阿瑞斯](../Page/阿瑞斯.md "wikilink")

  -
  - [厄洛特斯](../Page/厄洛特斯.md "wikilink")

  -
  - [赫马佛洛狄忒斯](../Page/赫马佛洛狄忒斯.md "wikilink")

  - [伽摩](../Page/伽摩.md "wikilink")

  - [天魔](../Page/天魔.md "wikilink")

  - [一見鍾情](../Page/一見鍾情.md "wikilink")

  - [法涅斯](../Page/法涅斯.md "wikilink")

## 参考资料

  - 註釋

<!-- end list -->

  - 文獻

<!-- end list -->

  - [威廉·史密斯 (辞书编纂家)](../Page/威廉·史密斯_\(辞书编纂家\).md "wikilink");
    *[Dictionary of Greek and Roman Biography and
    Mythology](../Page/Dictionary_of_Greek_and_Roman_Biography_and_Mythology.md "wikilink")*,
    London (1873).
    ["Eros"](http://www.perseus.tufts.edu/hopper/text?doc=Perseus%3Atext%3A1999.04.0104%3Aalphabetic+letter%3DE%3Aentry+group%3D6%3Aentry%3Deros-bio-1)

## 外部链接

  - [Warburg Institute Iconographic Database (ca 2,400 images of
    Eros)](http://warburg.sas.ac.uk/vpc/VPC_search/subcats.php?cat_1=5&cat_2=167)

## 相關條目

  - [邱比特](../Page/邱比特.md "wikilink")
  - [愛神](../Page/愛神.md "wikilink")

[E](../Page/分類:希臘神祇.md "wikilink") [E](../Page/分類:愛神.md "wikilink")
[E](../Page/分類:希臘神話弓箭手.md "wikilink")
[分類:希腊神话人物](../Page/分類:希腊神话人物.md "wikilink")

1.  [*Oxford Learner's Dictionaries:*
    "Eros"](http://www.oxfordlearnersdictionaries.com/definition/american_english/eros)

2.  From [ἔραμαι](../Page/wikt:ἔραμαι.md "wikilink") "to desire, love",
    of uncertain etymology;  has suggested a  origin (*Etymological
    Dictionary of Greek*, Brill, 2009, p. 449).

3.  *Larousse Desk Reference Encyclopedia*, , Haydock, 1995, p. 215.

4.
5.  "Eros", in S. Hornblower and A. Spawforth, eds., *The Oxford
    Classical Dictionary*.

6.  [Hesiod](../Page/赫西俄德.md "wikilink"),
    *[Theogony](../Page/神谱.md "wikilink")*
    [116–122](http://www.perseus.tufts.edu/hopper/text?doc=Hes.+Th.+116)
    in *The Homeric Hymns and Homerica with an English Translation* by
    Hugh G. Evelyn-White, Cambridge, MA.,Harvard University Press;
    London, William Heinemann Ltd. 1914.

7.  "First of all the gods she devised Erōs." (Parmenides, fragment 13.)
    (The identity of the "she" is unclear, as Parmenides' work has
    survived only in fragments.

8.  See the article [Eros](http://www.theoi.com/Protogenos/Eros.html) at
    the Theoi Project.