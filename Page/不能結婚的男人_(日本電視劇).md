{{〉}} | 拍攝地 = [東京都](../Page/東京都.md "wikilink") | 執行製片 = | 製片人 =
安藤和久、東城佑司、伊藤達哉 | 國家 =  | 語言 =
[日語](../Page/日語.md "wikilink") | 頻道 =
[關西電視台](../Page/關西電視台.md "wikilink") | 播出國家 =  | 開始
= 2006年7月4日 | 結束 = 2006年9月19日 | 集數 = 12集 | 官方網站 =
<https://web.archive.org/web/20061005072528/http://www.ktv.co.jp/shinsuke/>
| imdb_id = | tv_com_id = | 相關節目 = [不能結婚的男人
(韓國電視劇)](../Page/不能結婚的男人_\(韓國電視劇\).md "wikilink")
| 台灣名稱 = 熟男不結婚（[緯來日本台](../Page/緯來日本台.md "wikilink")） }}

《**熟男不結婚**》是[日本](../Page/日本.md "wikilink")[關西電視台自](../Page/關西電視台.md "wikilink")2006年7月4日至9月19日每週二晚間播放的[電視劇](../Page/日劇.md "wikilink")，由[阿部寬和](../Page/阿部寬.md "wikilink")[夏川結衣主演](../Page/夏川結衣.md "wikilink")。在2005年，兩人已經在[NHK大河劇](../Page/NHK大河劇.md "wikilink")《[義經](../Page/義經_\(大河劇\).md "wikilink")》合作過，此次是他們的第二次合演。

## 劇情簡介

伴隨著現代社會的越加成熟，日本的晚婚現象越來越嚴重，獨身主義者也越來越多。以前過了40歲還沒有結婚的日本人會遭人側目，現在則再平常不過。隨著這一現象的逐漸普遍，日本人少生孩子或者不生孩子，女性不再只能在家相夫教子，而是可以和男人們一起在工作上打拼。現今日本社會已經面臨這一翻天覆地的變化，這也成爲了新時代日本人的課題。本劇便是講述一位不擅與人交際、生活呆板的[獨身主義](../Page/獨身.md "wikilink")[建築師](../Page/建築師.md "wikilink")，一直聲稱自己「不是結不了婚，而是不想結婚」，因與三位個性迥異的女性結識，而漸漸令其生活態度產生改變。

## 劇集演員

### 主要演員

  - [阿部寬](../Page/阿部寬.md "wikilink") 飾
    桑野信介（40歲）（香港TVB配音：[陳欣](../Page/陳欣_\(配音員\).md "wikilink")）
    / （香港NOW TV配音：[葉偉麟](../Page/葉偉麟.md "wikilink")）

<!-- end list -->

  -
    建築設計師，獨立經營自己的建築事務所。設計的住宅獲得很高的評價，尤其非常擅長營造居家氛圍，強調以廚房為中心的建築設計方針，設計出的東西總給人溫暖的感覺。工作體面、穩定，收入豐厚，步入40歲卻仍然單身，雖然有著許多令人生厭的惡習，固執、偏執甚至刻薄，還有潔癖，但卻是衆人眼中的黃金單身漢。一心渴望塵埃落定，但至今沒有成功告別單身，便固執地認為自己不需要戀愛，更不需要婚姻。

<!-- end list -->

  - [夏川結衣](../Page/夏川結衣.md "wikilink") 飾
    早坂夏美（36歲）（香港TVB配音：[陳凱婷](../Page/陳凱婷.md "wikilink")）
    / （香港NOW TV配音：）

<!-- end list -->

  -
    信介的妹夫家族經營的中川醫院內科醫師。工作出色，性格獨立，曾經有過一次訂婚的經歷，對戀愛心存戒備。喜歡打[柏青哥](../Page/柏青哥.md "wikilink")、看[漫畫](../Page/漫畫.md "wikilink")、喝茶、足按摩，對時尚流行不感冒，講求健康，所以每次去拉麵店都會點蔬菜拉麵。對信介的不慍不火反而讓好勝心強的信介燃起熊熊愛火。

<!-- end list -->

  - [塚本高史](../Page/塚本高史.md "wikilink") 飾
    村上英治（25歲）（香港TVB配音：[黃榮璋](../Page/黃榮璋.md "wikilink")）
    / （香港NOW TV配音：）

<!-- end list -->

  -
    信介的助理，沙織的戀人。雖然私底下常常數落信介的怪脾氣跟怪僻，但在工作上非常尊敬信介，對信介的指示言聽計從，一直希望從信介身上學到各種各樣的東西。然而對信介固執獨斷的私生活卻不敢苟同，不贊成信介獨身主義的人生態度。感情及工作態度是認定目標就勇往直前的類型，與信介不同，戀愛方面非常執著，很怕女朋友沙織生氣。

<!-- end list -->

  - [國仲涼子](../Page/國仲涼子.md "wikilink") 飾
    田村滿（27歲）（香港TVB配音：[鄭麗麗](../Page/鄭麗麗.md "wikilink")）
    / （香港NOW TV配音：）

<!-- end list -->

  -
    信介的鄰居，[TOYOTA汽車銷售部勤務](../Page/TOYOTA.md "wikilink")。因為隔牆的噪音糾紛與信介不打不相識，三番救了腹痛的信介，兩人逐漸熟識。雖然最開始很討厭信介的自大和無理，但慢慢開始被他率直單純的個性吸引，最後因為信介在騷擾事件的挺身而出而對他産生了好感。

<!-- end list -->

  - [高島禮子](../Page/高島禮子.md "wikilink") 飾
    澤崎摩耶（38歲）（香港TVB配音：[黃麗芳](../Page/黃麗芳.md "wikilink")）
    / （香港NOW TV配音：）

<!-- end list -->

  -
    建築事務所勤務，信介工作上的夥伴，由於和信介常年一起共事而有一種獨特的默契。相當了解信介的性格，由於信介怪癖的性格經常讓客戶吃不消，摩耶總能起到緩和作用。其實在工作上非常信賴信介，信介也十分依賴摩耶幫忙打理工作上的人際關係問題。

<!-- end list -->

  - [草笛光子](../Page/草笛光子.md "wikilink") 飾
    桑野育代（65歲）（香港TVB配音：[袁淑珍](../Page/袁淑珍.md "wikilink")）
    / （香港NOW TV配音：）

<!-- end list -->

  -
    信介的母親，對於兒子的婚事很擔心，希望他早點結婚，所以多方為他尋找新娘人選。

<!-- end list -->

  - [櫻](../Page/櫻_\(演員\).md "wikilink") 飾 吉川沙織（23歲）（香港TVB配音:
    [曾秀清](../Page/曾秀清.md "wikilink")） / （香港NOW TV配音：）

<!-- end list -->

  -
    建築事務所勤務，英治的戀人，在摩耶底下工作。

<!-- end list -->

  - [尾美利德](../Page/尾美利德.md "wikilink") 飾 中川良雄（39歲）（香港TVB配音:
    [劉昭文](../Page/劉昭文.md "wikilink")） / （香港NOW TV配音：）　

<!-- end list -->

  -
    中川醫院副院長兼信介的友人及妹夫。雖然被強勢的老婆管的死死的，但還是愛去酒店和夜店。

<!-- end list -->

  - [三浦理惠子](../Page/三浦理惠子.md "wikilink") 飾 中川圭子（香港TVB配音:
    [林元春](../Page/林元春.md "wikilink")） / （香港NOW TV配音：）　

<!-- end list -->

  -
    信介的妹妹，良雄的妻子，家庭主婦。在夫妻關係的方面處於極度強勢，甚至會監看老公的手機。對母親孝順，常邀請母親和哥哥在家中聚會吃晚飯。

<!-- end list -->

  - [高知東生](../Page/高知東生.md "wikilink") 飾
    金田裕之（香港TVB配音：[潘文柏](../Page/潘文柏.md "wikilink")）
    / （香港NOW TV配音：）

<!-- end list -->

  -
    建築設計師，座車是[TOYOTA](../Page/TOYOTA.md "wikilink")
    GT2000，另一經常出現的場景為信介也常去的酒吧，幾乎每次都會在此與女性約會，但每次的女伴都不同。是信介從質疑忌妒到崇拜推崇的對象。

<!-- end list -->

  - [Kotsubu](../Page/Kotsubu.md "wikilink") 飾 KEN（5歲）

<!-- end list -->

  -
    小滿的[寵物狗](../Page/寵物.md "wikilink")（[巴哥犬](../Page/巴哥犬.md "wikilink")），頗具人性，跟信介很合得來。

### 次要演員

  - [SHEILA](../Page/SHEILA.md "wikilink") 飾
    西村千鶴（小滿的同事）（香港TVB配音：[張頌欣](../Page/張頌欣.md "wikilink")）
    / （香港NOW TV配音：）
  - [西丸優子](../Page/西丸優子.md "wikilink") 飾
    小沢真理（中川醫院護士）（香港TVB配音：[黎桂芳](../Page/黎桂芳.md "wikilink")）
    / （香港NOW TV配音：）
  - [高松郁](../Page/高松郁.md "wikilink") 飾 江森和美（中川醫院護士）（香港TVB配音:
    [謝潔貞](../Page/謝潔貞.md "wikilink")） / （香港NOW TV配音：）
  - [平岡映美](../Page/平岡映美.md "wikilink") 飾
    中川ゆみ（中川的女兒）（香港TVB配音：[何璐怡](../Page/何璐怡.md "wikilink")）
    / （香港NOW TV配音：）
  - [立花彩野](../Page/立花彩野.md "wikilink") 飾
    木崎（便利店店員）（香港TVB配音：[林雅婷](../Page/林雅婷.md "wikilink")）
    / （香港NOW TV配音：）
  - [西尾浩行](../Page/西尾浩行.md "wikilink") 飾 錄像帶出租店店員（香港TVB配音:
    [劉昭文](../Page/劉昭文.md "wikilink")） / （香港NOW TV配音：）
  - [長岡尚彥](../Page/長岡尚彥.md "wikilink") 飾
    酒吧酒保（香港TVB配音：[黎景全](../Page/黎景全.md "wikilink")）
    / （香港NOW TV配音：）
  - [松井涼子](../Page/松井涼子.md "wikilink") 飾 牛排西餐店店員（香港TVB配音:
    [陸惠玲](../Page/陸惠玲.md "wikilink")） / （香港NOW TV配音：）
  - [不破萬作](../Page/不破萬作.md "wikilink") 飾
    工地工頭（香港TVB配音：[林保全](../Page/林保全.md "wikilink")）
    / （香港NOW TV配音：）

### 客串出場

  - 第一話

<!-- end list -->

  - [遊井亮子](../Page/遊井亮子.md "wikilink") 飾 Party上的女子

<!-- end list -->

  - 第四話

<!-- end list -->

  - [加賀美早紀](../Page/加賀美早紀.md "wikilink") 飾 小林雅子（旅遊巴士導遊）

<!-- end list -->

  - 第六話

<!-- end list -->

  - [乃木涼介](../Page/乃木涼介.md "wikilink") 飾 八木（大坂燒道頓堀老闆）
  - [白井晃](../Page/白井晃.md "wikilink") 飾 結城（有名插畫家）

<!-- end list -->

  - 第七話

<!-- end list -->

  - [龍雷太](../Page/龍雷太.md "wikilink") 飾 早坂康雄（夏美的父親）

<!-- end list -->

  - 第九話

<!-- end list -->

  - [三津谷葉子](../Page/三津谷葉子.md "wikilink") 飾 長沢由紀（夜店正妹）
  - [岸博之](../Page/岸博之.md "wikilink") 飾 夏美的相親對象
  - [中谷龍](../Page/中谷龍.md "wikilink") 飾 小滿聯誼的胖男
  - [辰巳智秋](../Page/辰巳智秋.md "wikilink") 飾 小滿聯誼的瘦男
  - [富家規政](../Page/富家規政.md "wikilink") 飾 山下拓郎（挖角摩耶的獵頭）

<!-- end list -->

  - 第11話

<!-- end list -->

  - [小須田康人](../Page/小須田康人.md "wikilink") 飾 小區警察
  - [永山毅](../Page/永山毅.md "wikilink") 飾 鬼屋木乃伊男
  - [葛木英](../Page/葛木英.md "wikilink") 飾 鬼屋雪女

### 金田女友

  - 第一話：[蒲生麻由](../Page/蒲生麻由.md "wikilink")
  - 第二話：[白石亞沙子](../Page/白石亞沙子.md "wikilink")
  - 第三話：[下村真理](../Page/下村真理.md "wikilink")
  - 第四話：[浦のどか](../Page/浦のどか.md "wikilink")
  - 第六話：[SHEILA](../Page/SHEILA.md "wikilink")
  - 第七話：[福井裕佳梨](../Page/福井裕佳梨.md "wikilink")
  - 第八話：[大城美和](../Page/大城美和.md "wikilink")
  - 第九話：[吉原夏紀](../Page/吉原夏紀.md "wikilink")
  - 第十話：[清水響美](../Page/清水響美.md "wikilink")
  - 第11話：[片岡明日香](../Page/片岡明日香.md "wikilink")
  - 最終話：[關根綾佳](../Page/關根綾佳.md "wikilink")

## 每集特色

### 夏美和小滿看的漫畫

第一話

  - 夏美：[手塚治虫](../Page/手塚治虫.md "wikilink")「[怪醫黑傑克](../Page/怪醫黑傑克.md "wikilink")」

第二話

  - 夏美：[手塚治虫](../Page/手塚治虫.md "wikilink")「[怪醫黑傑克](../Page/怪醫黑傑克.md "wikilink")」

第四話

  - 夏美：[手塚治虫](../Page/手塚治虫.md "wikilink")「[怪醫黑傑克](../Page/怪醫黑傑克.md "wikilink")」

第五話

  - 小滿：[井上博和](../Page/井上博和.md "wikilink")「[小綠的日子](../Page/小綠的日子.md "wikilink")」

第六話

  - 夏美：[星野之宣](../Page/星野之宣.md "wikilink")「[2001夜物語](../Page/2001夜物語.md "wikilink")」
  - 夏美：[手塚治虫](../Page/手塚治虫.md "wikilink")「[火鳥](../Page/火鳥_\(漫畫\).md "wikilink")」
  - 小滿：大林きの子「王家新娘」（虛構作品，借鑑原作是[細川智榮子的](../Page/細川智榮子.md "wikilink")「[尼羅河女兒](../Page/尼羅河女兒.md "wikilink")」）

第九話

  - 小滿：[篠原千繪](../Page/篠原千繪.md "wikilink")「[闇河魅影](../Page/闇河魅影.md "wikilink")」
  - 夏美：[山川直人](../Page/山川直人.md "wikilink")「[咖啡再一杯](../Page/咖啡再一杯.md "wikilink")」
  - 夏美：[齋藤隆夫](../Page/齋藤隆夫.md "wikilink")「[Golgo13](../Page/Golgo13.md "wikilink")」

第十一話

  - 小滿：[篠原千繪](../Page/篠原千繪.md "wikilink")「[闇河魅影](../Page/闇河魅影.md "wikilink")」
  - 夏美：[石之森章太郎](../Page/石之森章太郎.md "wikilink")「[機器人009](../Page/機器人009.md "wikilink")」
  - 夏美：[伊月慶悟](../Page/伊月慶悟.md "wikilink")「[天醫無縫](../Page/天醫無縫.md "wikilink")」

### 信介聽的音樂

  - 第一話

<!-- end list -->

  - [馬勒第五交響曲第五樂章](../Page/古斯塔夫·馬勒.md "wikilink")
  - [蕭士塔高維契第五交響曲第四樂章](../Page/蕭士塔高維契.md "wikilink")

<!-- end list -->

  - 第二話

<!-- end list -->

  - 蕭士塔高維契第五交響曲第四樂章

<!-- end list -->

  - 第三話

<!-- end list -->

  - [華格納](../Page/華格納.md "wikilink")「[紐倫堡的名歌手](../Page/紐倫堡的名歌手.md "wikilink")」第一幕前奏曲

<!-- end list -->

  - 第四話

<!-- end list -->

  - [舒伯特](../Page/舒伯特.md "wikilink")「魔王」

<!-- end list -->

  - 第五話

<!-- end list -->

  - 舒伯特「Ave Maria」
  - [布魯克納第五交響曲](../Page/布魯克納.md "wikilink")

<!-- end list -->

  - 第六話

<!-- end list -->

  - [艾爾加進行曲](../Page/艾爾加.md "wikilink")「威風凜凜」第一章

<!-- end list -->

  - 第七話

<!-- end list -->

  - [史麥塔納連作交響詩](../Page/史麥塔納.md "wikilink")「我的祖國」之「Vltava」篇

<!-- end list -->

  - 第八話

<!-- end list -->

  - 馬勒第五交響曲第三樂章
  - [莫札特第四十一交響曲第一樂章](../Page/莫札特.md "wikilink")「Jupiter」

<!-- end list -->

  - 第九話

<!-- end list -->

  - [穆索斯基](../Page/穆索斯基.md "wikilink")（[拉威爾改編成管弦樂組曲](../Page/拉威爾.md "wikilink")）「[展覽會之畫](../Page/展覽會之畫.md "wikilink")」之「Promenade」篇

<!-- end list -->

  - 第十話

<!-- end list -->

  - [羅西尼](../Page/羅西尼.md "wikilink")「[塞維利亞的理髮師](../Page/塞維利亞的理髮師.md "wikilink")」

<!-- end list -->

  - 第11話

<!-- end list -->

  - [德弗札克E小調第九交響曲](../Page/德弗札克.md "wikilink")「新世界」

<!-- end list -->

  - 最終話

<!-- end list -->

  - [普契尼](../Page/普契尼.md "wikilink")「Gianni Schicchi」中詠歎調「我親愛的爸爸」
  - [小約翰·史特勞斯](../Page/小約翰·史特勞斯.md "wikilink")「皇帝圓舞曲」
  - [貝多芬第七交響曲第一樂章](../Page/貝多芬.md "wikilink")

### 金田的網站更新

  - 第一話：承包了田園調布K先生別墅的設計
  - 第二話：稍微去了一下巴黎
  - 第三話：在米蘭買了帽子
  - 第四話：這個週六稍微去札幌吃了一下拉麵
  - 第五話：那小子果然沒在做正經事
  - 第六話：昨日在橫濱吃了燒賣
  - 第七話：在常去的酒吧稍微嘗試了新的雞尾酒
  - 第八話：我去了趟Party
  - 第九話：前幾天，稍微去卡啦OK盡情歡唱了一下
  - 第十話：最近有點迷上了保齡球
  - 第11話：這個星期天去了夏威夷一下
  - 最終話：這個星期天交了個好朋友

## 主題曲

  - [小事樂團](../Page/小事樂團.md "wikilink")《[小黑魚](../Page/小黑魚.md "wikilink")》

## 收視率

<table>
<thead>
<tr class="header">
<th><p>各集</p></th>
<th><p>播放日期</p></th>
<th><p>中文副題</p></th>
<th><p>日文副題</p></th>
<th><p>收視率</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>第一集</p></td>
<td><p>2006年7月4日</p></td>
<td><p>獨身不可以嗎？</p></td>
<td></td>
<td><p>20.2％</p></td>
</tr>
<tr class="even">
<td><p>第二集</p></td>
<td><p>2006年7月11日</p></td>
<td><p>愛吃肉不可以嗎？</p></td>
<td></td>
<td><p><span style="color:green;">14.4％</span></p></td>
</tr>
<tr class="odd">
<td><p>第三集</p></td>
<td><p>2006年7月18日</p></td>
<td><p>花錢不可以嗎？</p></td>
<td></td>
<td><p>15.9％</p></td>
</tr>
<tr class="even">
<td><p>第四集</p></td>
<td><p>2006年7月25日</p></td>
<td><p>一個人的假日不可以嗎？</p></td>
<td></td>
<td><p>16.5％</p></td>
</tr>
<tr class="odd">
<td><p>第五集</p></td>
<td><p>2006年8月1日</p></td>
<td><p>不許進我家不可以嗎？</p></td>
<td></td>
<td><p>15.1％</p></td>
</tr>
<tr class="even">
<td><p>第六集</p></td>
<td><p>2006年8月8日</p></td>
<td><p>不谙人情不可以嗎？</p></td>
<td></td>
<td><p><span style="color:green;">14.4％</span></p></td>
</tr>
<tr class="odd">
<td><p>第七集</p></td>
<td><p>2006年8月15日</p></td>
<td><p>不善和親戚交往不可以嗎？</p></td>
<td></td>
<td><p>15.3％</p></td>
</tr>
<tr class="even">
<td><p>第八集</p></td>
<td><p>2006年8月22日</p></td>
<td><p>討厭養狗不可以嗎？</p></td>
<td></td>
<td><p>14.6％</p></td>
</tr>
<tr class="odd">
<td><p>第九集</p></td>
<td><p>2006年8月29日</p></td>
<td><p>有女友不可以嗎？</p></td>
<td></td>
<td><p>18.0％</p></td>
</tr>
<tr class="even">
<td><p>第十集</p></td>
<td><p>2006年9月5日</p></td>
<td><p>不懂女人心不可以嗎？</p></td>
<td></td>
<td><p>17.6％</p></td>
</tr>
<tr class="odd">
<td><p>第11集</p></td>
<td><p>2006年9月12日</p></td>
<td><p>討厭花圖案不可以嗎？</p></td>
<td></td>
<td><p>19.2％</p></td>
</tr>
<tr class="even">
<td><p>最終集</p></td>
<td><p>2006年9月19日</p></td>
<td><p>想要幸福不可以嗎？</p></td>
<td></td>
<td><p><span style="color:red;">22.0％</span></p></td>
</tr>
<tr class="odd">
<td><p><strong>平均收視率16.93 ％　</strong>（關東地區收視率，由Video Research調查）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 獎項

  - 第50回日劇學院賞作品賞

<!-- end list -->

  -
    讀者投票第二名
    記者投票第一名
    評審投票第一名

<!-- end list -->

  - 第50回日劇學院賞主演男優賞：阿部寬

<!-- end list -->

  -
    讀者投票第二名
    記者投票第一名
    評審投票第一名

<!-- end list -->

  - 第50回日劇學院賞助演女優賞：夏川結衣

<!-- end list -->

  -
    讀者投票第二名
    記者投票第一名
    評審投票第二名

<!-- end list -->

  - 第50回日劇學院賞劇本賞：
  - 第50回日劇學院賞導演賞：三宅喜重、小松隆志、植田尚
  - 第50回日劇學院賞電視週刊特別賞：Kotsubu

## 節目的變遷

## 外部連結

  - [《-{熟男不結婚}-》台灣官網](http://japan.videoland.com.tw/channel/shinsuke/default.htm)（緯來日本台）

[Category:關西電視台週二晚間十點連續劇](../Category/關西電視台週二晚間十點連續劇.md "wikilink")
[Category:2006年日本電視劇集](../Category/2006年日本電視劇集.md "wikilink")
[Category:MMJ電視劇](../Category/MMJ電視劇.md "wikilink")
[Category:無綫電視外購劇集](../Category/無綫電視外購劇集.md "wikilink")
[Category:日劇學院賞最佳作品](../Category/日劇學院賞最佳作品.md "wikilink")
[Category:不能結婚的男人](../Category/不能結婚的男人.md "wikilink")
[Category:男性題材電視節目](../Category/男性題材電視節目.md "wikilink")
[Category:緯來電視外購日劇](../Category/緯來電視外購日劇.md "wikilink")
[Category:日語電視劇](../Category/日語電視劇.md "wikilink")
[Category:Now寬頻電視外購劇集](../Category/Now寬頻電視外購劇集.md "wikilink")
[Category:阿部寬](../Category/阿部寬.md "wikilink")