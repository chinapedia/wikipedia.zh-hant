**世界童軍大露營**（）為國際間[童軍](../Page/童軍.md "wikilink")[大露營的一種](../Page/大露營.md "wikilink")，通常有數千甚至數以萬計來自世界各地十四至十七歲之青年參與。

第1次世界童軍大露營由[倫敦](../Page/倫敦.md "wikilink")[英國童軍總會主辦](../Page/英國童軍總會.md "wikilink")，近年則由[世界童軍組織](../Page/世界童軍組織.md "wikilink")（WOSM）舉辦。此後除戰時外，基本上世界童軍大露營每四年舉行一次。[第21次世界童軍大露營](../Page/第21次世界童軍大露營.md "wikilink")，己於2007年在[英國的](../Page/英國.md "wikilink")[海蘭德國家公園舉行](../Page/海蘭德國家公園.md "wikilink")，並慶祝童軍成立百週年，最近一次世界童軍大露營則是2015年在[日本](../Page/日本.md "wikilink")[山口市](../Page/山口市.md "wikilink")[山口県立きらら浜自然観察公園舉行的](../Page/山口県立きらら浜自然観察公園.md "wikilink")[第23次世界童軍大露營](../Page/第23次世界童軍大露營.md "wikilink")。

## 詞源

在[辭書學中](../Page/辭書學.md "wikilink")，「」這個詞可追溯自1860年至1865年，可視為一種[美國創造的詞彙](../Page/:wikt:Americanism.md "wikilink")，意義為歡樂與熱鬧的聚會\[1\]。據信，該詞彙由「」（快速與模糊的對話）與「」（嘈雜的慶典）組成，中間的「」則來自於「」（人群）\[2\]。

## 歷屆世界童軍大露營列表

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>屆次</p></th>
<th><p>舉辦國或地區</p></th>
<th><p>舉辦地點</p></th>
<th><p>主題</p></th>
<th><p>參加人數</p></th>
<th><p>代表團數量</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1920年</p></td>
<td><p><a href="../Page/第1次世界童軍大露營.md" title="wikilink">第1次</a></p></td>
<td></td>
<td><p><a href="../Page/肯辛頓郡.md" title="wikilink">肯辛頓郡</a><br />
<a href="../Page/奧林匹亞_(倫敦).md" title="wikilink">奧林匹亞</a></p></td>
<td></td>
<td><p>8,000</p></td>
<td><p>34</p></td>
</tr>
<tr class="even">
<td><p>1924年</p></td>
<td><p><a href="../Page/第2次世界童軍大露營.md" title="wikilink">第2次</a></p></td>
<td></td>
<td><p><a href="../Page/Ermelunden.md" title="wikilink">Ermelunden</a></p></td>
<td></td>
<td><p>4,549</p></td>
<td><p>40</p></td>
</tr>
<tr class="odd">
<td><p>1929年</p></td>
<td><p><a href="../Page/第3次世界童軍大露營.md" title="wikilink">第3次</a></p></td>
<td></td>
<td><p><a href="../Page/伯肯赫德.md" title="wikilink">伯肯赫德</a></p></td>
<td><p>Coming of Age<br />
時代的來臨</p></td>
<td><p>50,000</p></td>
<td><p>40</p></td>
</tr>
<tr class="even">
<td><p>1933年</p></td>
<td><p><a href="../Page/第4次世界童軍大露營.md" title="wikilink">第4次</a></p></td>
<td></td>
<td><p><a href="../Page/格德勒.md" title="wikilink">格德勒</a></p></td>
<td></td>
<td><p>25,792</p></td>
<td><p>46</p></td>
</tr>
<tr class="odd">
<td><p>1937年</p></td>
<td><p><a href="../Page/第5次世界童軍大露營.md" title="wikilink">第5次</a></p></td>
<td></td>
<td><p><a href="../Page/布魯曼達.md" title="wikilink">布魯曼達</a></p></td>
<td></td>
<td><p>28,750</p></td>
<td><p>54</p></td>
</tr>
<tr class="even">
<td><p>1947年</p></td>
<td><p><a href="../Page/第6次世界童軍大露營.md" title="wikilink">第6次</a></p></td>
<td></td>
<td><p><a href="../Page/莫桑.md" title="wikilink">莫桑</a></p></td>
<td><p>Jamboree of Peace<br />
和平大露營</p></td>
<td><p>24,152</p></td>
<td><p>38</p></td>
</tr>
<tr class="odd">
<td><p>1951年</p></td>
<td><p><a href="../Page/第7次世界童軍大露營.md" title="wikilink">第7次</a></p></td>
<td></td>
<td><p><a href="../Page/巴德伊舍.md" title="wikilink">巴德伊舍</a></p></td>
<td><p>Jamboree of Simplicity</p></td>
<td><p>12,884</p></td>
<td><p>41</p></td>
</tr>
<tr class="even">
<td><p>1955年</p></td>
<td><p><a href="../Page/第8次世界童軍大露營.md" title="wikilink">第8次</a></p></td>
<td></td>
<td><p><a href="../Page/濱湖尼亞加拉.md" title="wikilink">濱湖尼亞加拉</a></p></td>
<td></td>
<td><p>11,139</p></td>
<td><p>71</p></td>
</tr>
<tr class="odd">
<td><p>1957年</p></td>
<td><p><a href="../Page/第9次世界童軍大露營.md" title="wikilink">第9次</a></p></td>
<td></td>
<td><p><a href="../Page/索頓公園.md" title="wikilink">索頓公園</a></p></td>
<td><p>50th Anniversary of Scouting<br />
童軍50週年慶（非官方認定主題）</p></td>
<td><p>30,000</p></td>
<td><p>82</p></td>
</tr>
<tr class="even">
<td><p>1959年</p></td>
<td><p><a href="../Page/第10次世界童軍大露營.md" title="wikilink">第10次</a></p></td>
<td></td>
<td><p><a href="../Page/內湖省.md" title="wikilink">內湖省</a></p></td>
<td><p>Building Tomorrow Today</p></td>
<td><p>12,203</p></td>
<td><p>44</p></td>
</tr>
<tr class="odd">
<td><p>1963年</p></td>
<td><p><a href="../Page/第11次世界童軍大露營.md" title="wikilink">第11次</a></p></td>
<td></td>
<td><p><a href="../Page/馬拉松_(希臘).md" title="wikilink">馬拉松</a></p></td>
<td><p>Higher and Wider</p></td>
<td><p>14,000</p></td>
<td><p>88</p></td>
</tr>
<tr class="even">
<td><p>1967年</p></td>
<td><p><a href="../Page/第12次世界童軍大露營.md" title="wikilink">第12次</a></p></td>
<td></td>
<td><p><a href="../Page/法拉加特州立公園.md" title="wikilink">法拉加特州立公園</a></p></td>
<td><p>For Friendship<br />
獻給友誼</p></td>
<td><p>12,011</p></td>
<td><p>105</p></td>
</tr>
<tr class="odd">
<td><p>1971年</p></td>
<td><p><a href="../Page/第13次世界童軍大露營.md" title="wikilink">第13次</a></p></td>
<td></td>
<td><p><a href="../Page/富士宮市.md" title="wikilink">富士宮</a></p></td>
<td><p>For Understanding</p></td>
<td><p>23,758</p></td>
<td><p>87</p></td>
</tr>
<tr class="even">
<td><p>1975年</p></td>
<td><p><a href="../Page/第14次世界童軍大露營.md" title="wikilink">第14次</a></p></td>
<td></td>
<td><p><a href="../Page/利勒哈默爾.md" title="wikilink">利勒哈默爾</a></p></td>
<td><p>Five Fingers, One Hand<br />
五根手指，一隻手</p></td>
<td><p>17,259</p></td>
<td><p>91</p></td>
</tr>
<tr class="odd">
<td><p><strong>1979年</strong></p></td>
<td><p><strong><a href="../Page/第15次世界童軍大露營取消事件.md" title="wikilink">第15次（取消）</a></strong></p></td>
<td><p><strong></strong></p></td>
<td><p><strong><a href="../Page/內沙布爾.md" title="wikilink">內沙布爾</a></strong></p></td>
<td></td>
<td><p><strong>取消</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>1983年</p></td>
<td><p><a href="../Page/第15次世界童軍大露營.md" title="wikilink">第15次</a></p></td>
<td></td>
<td><p><a href="../Page/卡爾加里.md" title="wikilink">卡爾加里</a></p></td>
<td><p>The Spirit Lives On<br />
精神永在</p></td>
<td><p>14,752</p></td>
<td><p>102</p></td>
</tr>
<tr class="odd">
<td><p>1987年<br />
～<br />
1988年</p></td>
<td><p><a href="../Page/第16次世界童軍大露營.md" title="wikilink">第16次</a></p></td>
<td></td>
<td><p><a href="../Page/雪梨.md" title="wikilink">雪梨</a></p></td>
<td><p>Bringing the World Together</p></td>
<td><p>14,434</p></td>
<td><p>98</p></td>
</tr>
<tr class="even">
<td><p>1991年</p></td>
<td><p><a href="../Page/第17次世界童軍大露營.md" title="wikilink">第17次</a></p></td>
<td></td>
<td><p><a href="../Page/雪嶽山國立公園.md" title="wikilink">雪嶽山國立公園</a></p></td>
<td><p>Many Lands, One World</p></td>
<td><p>20,000</p></td>
<td><p>100</p></td>
</tr>
<tr class="odd">
<td><p>1995年</p></td>
<td><p><a href="../Page/第18次世界童軍大露營.md" title="wikilink">第18次</a></p></td>
<td></td>
<td><p><a href="../Page/弗萊福蘭.md" title="wikilink">弗萊福蘭</a></p></td>
<td><p>Future is Now<br />
未來就是現在</p></td>
<td><p>28,960</p></td>
<td><p>122</p></td>
</tr>
<tr class="even">
<td><p>1998年<br />
～<br />
1999年</p></td>
<td><p><a href="../Page/第19次世界童軍大露營.md" title="wikilink">第19次</a></p></td>
<td></td>
<td><p>Picarquín</p></td>
<td><p>Building Peace Together<br />
一同創造和平</p></td>
<td><p>31,000</p></td>
<td><p>137</p></td>
</tr>
<tr class="odd">
<td><p>2002年<br />
～<br />
2003年</p></td>
<td><p><a href="../Page/第20次世界童軍大露營.md" title="wikilink">第20次</a></p></td>
<td></td>
<td><p>Sattahip</p></td>
<td><p>Share our World, Share our Cultures<br />
分享世界，分享文化</p></td>
<td><p>24,000</p></td>
<td><p>143</p></td>
</tr>
<tr class="even">
<td><p>2007年</p></td>
<td><p><a href="../Page/第21次世界童軍大露營.md" title="wikilink">第21次</a></p></td>
<td></td>
<td><p><a href="../Page/海蘭德國家公園.md" title="wikilink">海蘭德國家公園</a></p></td>
<td><p>One World，One Promise<br />
同心許諾，世界大同</p></td>
<td><p>38074</p></td>
<td><p>158</p></td>
</tr>
<tr class="odd">
<td><p>2011年</p></td>
<td><p><a href="../Page/第22次世界童軍大露營.md" title="wikilink">第22次</a></p></td>
<td></td>
<td><p><a href="../Page/Rinkaby.md" title="wikilink">Rinkaby</a></p></td>
<td><p>Simply Scouting<br />
</p></td>
<td><p>40061</p></td>
<td><p>169</p></td>
</tr>
<tr class="even">
<td><p>2015年</p></td>
<td><p><a href="../Page/第23次世界童軍大露營.md" title="wikilink">第23次</a></p></td>
<td><p>[3]</p></td>
<td><p><a href="../Page/山口市.md" title="wikilink">山口市Kirara濱</a></p></td>
<td><p>和 Wa: A Spirit of Unity</p></td>
<td><p>33838</p></td>
<td><p>152</p></td>
</tr>
<tr class="odd">
<td><p>2019年</p></td>
<td><p><a href="../Page/第24次世界童軍大露營.md" title="wikilink">第24次</a></p></td>
<td><p><a href="../Page/美國.md" title="wikilink">美國</a><a href="../Page/加拿大.md" title="wikilink">加拿大</a><a href="../Page/墨西哥.md" title="wikilink">墨西哥共同舉辦</a></p></td>
<td><p><a href="../Page/The_Summit_Bechtel_Family_National_Scout_Reserve.md" title="wikilink">The Summit Bechtel Family National Scout Reserve</a></p></td>
<td><p>Unlock a New World</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>2023年</p></td>
<td><p><a href="../Page/第25次世界童軍大露營.md" title="wikilink">第25次</a></p></td>
<td><p><a href="../Page/韓國.md" title="wikilink">韓國</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 参考文獻

## 参见

  - [大露營](../Page/大露營.md "wikilink")
  - [世界羅浮童軍大會](../Page/世界羅浮童軍大會.md "wikilink")
  - [联合国童军](../Page/联合国童军.md "wikilink")

{{-}}

<table>
<thead>
<tr class="header">
<th><p>世界童軍大露營</p></th>
<th><p>空中大會</p></th>
<th><p>網路大會</p></th>
<th><p>世界童軍遠足日</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="https://web.archive.org/web/20080322083903/http://www.scoutbase.org.uk/library/history/inter/jambo.htm">歷屆大露營</a></li>
<li><a href="https://web.archive.org/web/20040202182421/http://pinetreeweb.com/bp-jamborees.htm">貝登堡露營</a></li>
<li><a href="https://web.archive.org/web/20051218200754/http://www.scouting2007.org/">2007世界童軍百周年大露營</a></li>
<li><a href="http://letsjamboree.org.uk/history.htm">歷屆世界童軍大露營</a></li>
</ul></td>
<td><ul>
<li><a href="http://home.hetnet.nl/~richard.middelkoop/">JOTA Information</a></li>
<li><a href="https://web.archive.org/web/20070311193456/http://www.scoutbase.org.uk/ps/inter/programme/events/jota/index.htm">JOTA at Scoutbase.org</a></li>
</ul></td>
<td><ul>
<li><a href="http://www.scoutlink.net/">ScoutLink's Website</a></li>
<li><a href="http://www.joti.org/">JOTI's main site</a></li>
</ul></td>
<td><ul>
<li><a href="http://www.jott.org/">JOTT's main site</a></li>
</ul></td>
</tr>
</tbody>
</table>

[id:Jambore](../Page/id:Jambore.md "wikilink")

[世界童軍大露營](../Category/世界童軍大露營.md "wikilink")

1.
2.
3.  2008世界領袖會議決議