**恐龍生理學**一直是個高度爭議的領域，尤其是[體溫調節](../Page/體溫調節.md "wikilink")。在[恐龍的早期研究中](../Page/恐龍.md "wikilink")，恐龍被重建為大型、四足、慵懶的[爬行動物](../Page/爬行動物.md "wikilink")。自19世紀中期之後，科學界對於恐龍的生活方式、[代謝](../Page/代謝.md "wikilink")、體溫調節等層面的理論，有過多次的變動。開始於1960年代的[恐龍文藝復興](../Page/恐龍文藝復興.md "wikilink")，對於恐龍生理學的理論，產生巨大的影響，幾乎涵蓋到每個層面，尤其是恐龍溫血理論、[鳥類起源自恐龍理論](../Page/鳥類.md "wikilink")。近年的許多新證據，使科學家得以研究[恐龍的生理特徵](../Page/恐龍.md "wikilink")，包含[代謝](../Page/代謝.md "wikilink")、體溫調節方式、[呼吸系統](../Page/呼吸系統.md "wikilink")、以及[心血管系統](../Page/心血管系統.md "wikilink")。恐龍溫血動物說逐漸成主流理論，牠們被視為活躍的動物，至少具有相當穩定的體溫。目前的爭論多在於牠們的體溫調節機制，以及牠們與鳥類、[哺乳類的代謝率相近程度](../Page/哺乳類.md "wikilink")。

**備註**：本條目中的「恐龍」一詞，並不包含[鳥類在內](../Page/鳥類.md "wikilink")。

## 研究歷史

### 早期的恐龍研究

恐龍的研究開始於1820年代的[英格蘭](../Page/英格蘭.md "wikilink")。當時的主要研究人員，包含[威廉·巴克蘭](../Page/威廉·巴克蘭.md "wikilink")（William
Buckland）、[吉迪恩·曼特爾](../Page/吉迪恩·曼特爾.md "wikilink")（Gideon
Mantell）、以及[理查·歐文](../Page/理查·歐文.md "wikilink")（Richard
Owen）在內，他們根據零碎的化石，將恐龍描述成大型的四足怪獸\[1\]。[倫敦的](../Page/倫敦.md "wikilink")[水晶宮公園豎立了幾座當時建立的恐龍雕像](../Page/水晶宮.md "wikilink")，由這些雕像可以得知當時的恐龍形象為體型接近[大象](../Page/大象.md "wikilink")、外形類似[蜥蜴的慵懶](../Page/蜥蜴.md "wikilink")[爬行動物](../Page/爬行動物.md "wikilink")\[2\]。儘管如此，歐文推測恐龍的[心臟與](../Page/心臟.md "wikilink")[呼吸系統較為類似](../Page/呼吸系統.md "wikilink")[哺乳類](../Page/哺乳類.md "wikilink")，而不是爬行動物\[3\]。

[Laelaps-Charles_Knight-1897.jpg](https://zh.wikipedia.org/wiki/File:Laelaps-Charles_Knight-1897.jpg "fig:Laelaps-Charles_Knight-1897.jpg")（現名[傷龍](../Page/傷龍.md "wikilink")）繪畫，由[查爾斯·耐特繪製](../Page/查爾斯·耐特.md "wikilink")\]\]
自1870年代開始，[美國西部發現了許多更完整的恐龍化石](../Page/美國.md "wikilink")，使科學家們可以提出更多的恐龍生理特徵理論。[愛德華·德林克·科普](../Page/愛德華·德林克·科普.md "wikilink")（Edward
Drinker
Cope）提出有部分的恐龍是活躍、行動敏捷的動物，[查爾斯·耐特](../Page/查爾斯·耐特.md "wikilink")（Charles
R.
Knight）的著名[暴風龍繪畫](../Page/暴風龍.md "wikilink")，即是根據科普的理論而繪製的\[4\]。大約是同一時期，[查爾斯·達爾文](../Page/查爾斯·達爾文.md "wikilink")（Charles
Darwin）提出[演化論](../Page/演化論.md "wikilink")，以及[始祖鳥與](../Page/始祖鳥.md "wikilink")[美頜龍的發現](../Page/美頜龍.md "wikilink")，導致[湯瑪斯·亨利·赫胥黎](../Page/湯瑪斯·亨利·赫胥黎.md "wikilink")（Thomas
Henry
Huxley）提出鳥類是恐龍後代的假設\[5\]。但是，恐龍是大型爬蟲類的形象已經根深柢固\[6\]，因此許多生理方面的理論，是參考爬行動物推測而來的，這個狀況持續到20世紀前半期\[7\]。

### 恐龍文藝復興

在1960年代晚期，[約翰·奧斯特倫姆](../Page/約翰·奧斯特倫姆.md "wikilink")（John
Ostrom）根據[恐爪龍的研究](../Page/恐爪龍.md "wikilink")，而重新提出[鳥類起源於恐龍的理論](../Page/鳥類.md "wikilink")，使得狀況開始改變\[8\]。奧斯特倫姆的學生[羅伯特·巴克](../Page/羅伯特·巴克.md "wikilink")（Robert
Bakker），自1968年的研究《*The superiority of
dinosaurs*》開始\[9\]，便在1970年代與1980年代的一系列科學研究、書籍、與大眾媒體中，不斷重申恐龍是種活躍的溫血動物，能夠維持短暫的高度活躍能力。巴克在他的著作中，將他的理論當成是19世紀後期的恐龍溫血理論的再復興，並稱之為[恐龍文藝復興](../Page/恐龍文藝復興.md "wikilink")（Dinosaur
renaissance）。他提出許多生理特徵與統計數據來說明這些理論\[10\]\[11\]，巴克的研究方法在科學界產生大量的爭論\[12\]。這些爭論最後使古生物學界採用新的研究方法，例如[骨組織學](../Page/骨組織學.md "wikilink")，骨組織學可用來鑑定恐龍的成長模式。

現在科學界普遍認為，許多恐龍具有比現存爬行動物更高的代謝率，可能所有恐龍都是。小型恐龍可能是[內溫性動物](../Page/內溫性.md "wikilink")，而大型恐龍則可能是[巨溫性動物](../Page/巨溫性.md "wikilink")\[13\]\[14\]；另一可能則是，大部分恐龍的代謝率位於兩者中間\[15\]。

## 進食

最早期的恐龍應該是[肉食性動物](../Page/肉食性.md "wikilink")，與其近親（例如[兔鱷](../Page/兔鱷.md "wikilink")）擁有許多共同的掠食動物特徵，例如：大而彎曲的刀狀牙齒、張開大的頜部、腹部小（肉食動物不需要大型[消化系統](../Page/消化系統.md "wikilink")）。後期的某些肉食性恐龍，體型相當大，也都保有相同的特徵。肉食性恐龍直接將獵物或肉塊吞下，而不經由咀嚼的階段\[16\]。

[偷蛋龍類](../Page/偷蛋龍類.md "wikilink")、[似鳥龍類的食性充滿謎題](../Page/似鳥龍類.md "wikilink")，牠們屬於[獸腳亞目](../Page/獸腳亞目.md "wikilink")[演化支](../Page/演化支.md "wikilink")，而大部分獸腳類恐龍是肉食性動物；但偷蛋龍類與似鳥龍類的頜部小、缺乏牙齒或牙齒窄小，牠們有可能是[雜食性](../Page/雜食性.md "wikilink")、[草食性動物](../Page/草食性.md "wikilink")\[17\]。

[蜥腳形亞目](../Page/蜥腳形亞目.md "wikilink")、[鳥臀目的許多特徵顯示牠們是草食性動物](../Page/鳥臀目.md "wikilink")，例如：張開小的頜部、嘴巴閉合時上下牙齒互相接觸、可容納大型消化系統的大型腹部。植物較硬、較難消化，需要更多時間消化；由於沒有已知動物可以直接消化[纖維素](../Page/纖維素.md "wikilink")，牠們的大型消化系統可能生存著[內共生的微生物](../Page/內共生.md "wikilink")，可協助消化纖維素\[18\]。

根據[蜥腳類恐龍的牙齒與頜部結構](../Page/蜥腳類.md "wikilink")，牠們進食時會直接將樹葉咬斷吞下，而不會經過咀嚼階段。鳥臀目恐龍則具有多種的協助進食方式。[劍龍下目](../Page/劍龍下目.md "wikilink")、[甲龍下目恐龍的頭部小](../Page/甲龍下目.md "wikilink")、牙齒與頜部關節的力量弱，被認為進食方式與蜥腳類恐龍類似。[厚頭龍類的頭部小](../Page/厚頭龍類.md "wikilink")、牙齒與頜部關節的力量弱，加上缺乏大型消化系統，顯示牠們可能以其他植物為食，例如[水果](../Page/水果.md "wikilink")、[種子](../Page/種子.md "wikilink")、幼芽，這些食物能提供的營養較樹葉高\[19\]。[鳥腳類恐龍](../Page/鳥腳類.md "wikilink")（例如[稜齒龍](../Page/稜齒龍.md "wikilink")、[禽龍](../Page/禽龍.md "wikilink")、各種[鴨嘴龍類](../Page/鴨嘴龍類.md "wikilink")）具有喙狀嘴，可用來切斷樹葉，頜部關節與牙齒則適合咀嚼。[角龍類恐龍也具有類似的喙狀嘴](../Page/角龍類.md "wikilink")、頜部關節與牙齒\[20\]。

許多科學家曾提出某些恐龍會吞食[胃石以協助消化](../Page/胃石.md "wikilink")，當[胃臟的肌肉蠕動時](../Page/胃臟.md "wikilink")，胃裡的胃石會研磨食物，現代鳥類也具有這種習性。在2007年，[奧利佛·維因斯](../Page/奧利佛·維因斯.md "wikilink")（Oliver
Wings）重新研究恐龍胃石的相關研究與證據，發現其中的矛盾處。他發現只有以[穀類為主食的鳥類才會吞食胃石](../Page/穀類.md "wikilink")，而且不是必定的習性；而[夏季時以](../Page/夏季.md "wikilink")[昆蟲為食](../Page/昆蟲.md "wikilink")，[冬季時以穀類為主食的鳥類](../Page/冬季.md "wikilink")，在夏季時不會吞食胃石、砂礫。科學家常認為蜥腳類恐龍有吞食胃石的習性，但是奧利佛·維因斯認為，只有少數蜥腳類化石曾發現胃石，而且數量少，胃石的硬度過軟，不足以協助消化；這些胃石的表面光滑，被認為經過高度的摩擦，而現代鳥類的胃石，表面因磨損而粗糙，或因[胃酸而出現侵蝕現象](../Page/胃酸.md "wikilink")。奧利佛·維因斯綜合以上理由，提出這些蜥腳類的胃石其實是進食時意外吞入的石頭。他同時也指出，[中國似鳥龍](../Page/中國似鳥龍.md "wikilink")、[尾羽龍等後期獸腳類恐龍的胃石](../Page/尾羽龍.md "wikilink")，外表類似鳥類的胃石；而恐龍吞食胃石以協助消化的習性，可能在早期演化階段就已出現，所以後期的獸腳類恐龍、鳥類都具有進食胃石的習性\[21\]\[22\]。

## 繁衍

部份雌性鳥類在產卵時，四肢骨頭的外部骨質與[骨髓之間會產生一種特殊骨層](../Page/骨髓.md "wikilink")，稱為[骨髓骨](../Page/骨髓骨.md "wikilink")（Medullary
bone）。骨髓骨富含[鈣](../Page/鈣.md "wikilink")，是用來製造蛋殼的。當產卵期過後，雌性鳥類會吸收骨髓骨\[23\]。目前已在[獸腳亞目的](../Page/獸腳亞目.md "wikilink")[暴龍與](../Page/暴龍.md "wikilink")[異特龍](../Page/異特龍.md "wikilink")、以及[鳥腳下目的](../Page/鳥腳下目.md "wikilink")[腱龍化石上發現骨髓骨](../Page/腱龍.md "wikilink")\[24\]\[25\]。

因為這三者與鳥類的所屬支系，在相當早的時候就已分開演化，因此可推論恐龍普遍會製造骨髓組織。在現今的生物中，恐龍的第二近親是[鱷魚](../Page/鱷魚.md "wikilink")，鱷魚無法製造骨髓骨。由此可推論，骨髓骨可能演化自[鳥頸類主龍](../Page/鳥頸類主龍.md "wikilink")，一個存在於[三疊紀到現代的](../Page/三疊紀.md "wikilink")[主龍類](../Page/主龍類.md "wikilink")[演化支](../Page/演化支.md "wikilink")，包含[翼龍目](../Page/翼龍目.md "wikilink")、恐龍、鳥類\[26\]。

由於曾在亞成年體化石發現過骨髓骨，這顯示恐龍達到性成熟的時間很快。爬行動物與中至大型的哺乳類，在亞成年階段達到性成熟；小型哺乳類與鳥類在完全成長後的一年內，才達到性成熟。這些在亞成年階段達到性成熟的動物有類似的生長模式，剛孵化或產下的幼年體已有相當程度的發育，而成年體的死亡率高\[27\]。

## 呼吸系統

[BirdRespiration.svg](https://zh.wikipedia.org/wiki/File:BirdRespiration.svg "fig:BirdRespiration.svg")、[氣囊](../Page/氣囊.md "wikilink")，在吸(上)與呼(下)時的空氣流通方向。氣囊的收縮協助空氣流通，肺臟僅有氣體交換功能。\]\]
[Dino_bird_h.jpg](https://zh.wikipedia.org/wiki/File:Dino_bird_h.jpg "fig:Dino_bird_h.jpg")

### 氣囊

自從1870年代起，科學家們普遍認為許多恐龍的身體骨骼，應該具有充滿空氣的空間，尤其是脊椎（側腔）。長久以來，這些骨內空間被認為只具減輕重量功能，但[羅伯特·巴克](../Page/羅伯特·巴克.md "wikilink")（Robert
T.
Bakker）提出它們應包含[氣囊](../Page/氣囊.md "wikilink")，類似[鳥類的呼吸系統](../Page/鳥類.md "wikilink")，可使牠們的呼吸更有效率\[28\]。

在1990年代後期以來，[約翰·魯本](../Page/約翰·魯本.md "wikilink")（John
Ruben）等人對恐龍具有氣囊提出反對意見，他們認為恐龍的呼吸系統較類似[鱷魚](../Page/鱷魚.md "wikilink")，是由[肝瓣肌肉推動腹部骨骼](../Page/肝瓣.md "wikilink")，進而將肝臟前後推動，形成力量擠壓、延展肺臟。魯本等人也根據這個理由，認為恐龍不是鳥類的祖先\[29\]\[30\]\[31\]\[32\]\[33\]。

其他古生物學家則認為，現代爬行動物缺乏鳥類的氣囊，根據其[循環系統](../Page/循環系統.md "wikilink")、[呼吸系統](../Page/呼吸系統.md "wikilink")，能使牠們的氧氣供應量達到體型相等的[哺乳動物的](../Page/哺乳動物.md "wikilink")50%到70%\[34\]；而缺乏鳥類的氣囊，將使恐龍無法成為內溫性動物\[35\]。其中一個反對研究指出，魯本等人的研究曾提到的[中華龍鳥化石過於扁平](../Page/中華龍鳥.md "wikilink")，無法證明肺臟的形狀，以及是否符合肝瓣機制\[36\]。其他古生物學家則認為，鱷魚的肝瓣機制需要寬廣、靈活的短恥骨才能實行，而恐龍的恥骨長而狹窄、不靈活。

科學家已在許多恐龍發現氣囊的證據，依照發現時間排列：

  - [蜥腳下目](../Page/蜥腳下目.md "wikilink")：早期蜥腳類恐龍只有頸椎具有氣囊，後期的[新蜥腳類的身體後段脊椎也出現氣囊](../Page/新蜥腳類.md "wikilink")。若根據[重演論](../Page/重演論.md "wikilink")，鳥類的胚胎顯示骨骼內的通道，早於氣囊的演化出現\[37\]\[38\]。
  - [虛骨龍類](../Page/虛骨龍類.md "wikilink")：較衍化的獸腳類恐龍，大部分物種的體型相當小，也包含[暴龍超科等大型恐龍](../Page/暴龍超科.md "wikilink")\[39\]。
  - [角鼻龍下目](../Page/角鼻龍下目.md "wikilink")：較原始的獸腳類恐龍，若角鼻龍類與虛骨龍類都具有氣囊，可能所有的獸腳類恐龍都具有氣囊\[40\]。
  - [腔骨龍](../Page/腔骨龍.md "wikilink")：[三疊紀晚期的原始獸腳類恐龍](../Page/三疊紀.md "wikilink")，其[頸椎具有氣囊](../Page/頸椎.md "wikilink")。同屬三疊紀晚期的[蜥腳形亞目](../Page/蜥腳形亞目.md "wikilink")[槽齒龍](../Page/槽齒龍.md "wikilink")，也具有氣囊，兩者是已知最早的具氣囊恐龍。\[41\]
  - [氣腔龍](../Page/氣腔龍.md "wikilink")（*Aerosteon*）：生存於[白堊紀晚期的](../Page/白堊紀.md "wikilink")[異特龍類](../Page/異特龍類.md "wikilink")，具有目前最類似鳥類的氣囊系統\[42\]。

到目前為止，沒有[鳥臀目恐龍曾經發現氣囊的證據](../Page/鳥臀目.md "wikilink")。但是，哺乳動物也沒有氣囊系統，所以這不意味鳥臀目恐龍的代謝率嚴重低於哺乳動物\[43\]。

關於恐龍的氣囊系統的功能，目前已有三種假設\[44\]：

  - 協助[呼吸系統](../Page/呼吸系統.md "wikilink")。這是最有可能的假設，並與恐龍具有高[代謝率的理論符合](../Page/代謝率.md "wikilink")。
  - 降低身體的重心，並降低[轉動慣量](../Page/轉動慣量.md "wikilink")，以增進平衡能力與方向的操控性。但此假設無法解釋四足的蜥腳類恐龍為何具有氣囊。
  - 冷卻體溫。[虛骨龍類的氣囊與羽毛似乎同時期演化出現](../Page/虛骨龍類.md "wikilink")。羽毛可以阻絕熱量往外消散，因此需要其他的冷卻機制來排除熱量。這個假設可能成立，但需要進一步的證據。

科學家曾以不同的方式來評估[迷惑龍的呼吸系統](../Page/迷惑龍.md "wikilink")：

  - 若假設迷惑龍像[鱷魚](../Page/鱷魚.md "wikilink")、[鳥類一樣](../Page/鳥類.md "wikilink")，沒有橫膈膜，以一個30公噸的迷惑龍，其[無效腔](../Page/無效腔.md "wikilink")（口腔、氣管、與其他空氣管道的[容積總合](../Page/容積.md "wikilink")，不包含肺臟）估計約184公升。如果該隻迷惑龍呼出的氣體量低於184公升，將無法完全呼出不新鮮的空氣，並將污濁的空氣吸回肺臟。
  - 若根據不同的呼吸系統來計算[迷惑龍的](../Page/迷惑龍.md "wikilink")[肺容積](../Page/肺容積.md "wikilink")（單次呼吸中，吸入或呼出的氣體總量），分別為：鳥類－904公升、哺乳動物－225公升、爬行動物－19公升。

根據以上結果，如果迷惑龍的呼吸系統類似爬行動物，其肺容積低於無效腔，牠們將無法完全呼出不新鮮的空氣，並將污濁的空氣吸回肺臟。如果迷惑龍的呼吸系統類似哺乳類，肺容積為225公升，無效腔為184公升，每次呼吸僅能吸入41公升的新鮮空氣。所以迷惑龍的呼吸系統可能是現今世界所未知的，或是像鳥類的呼吸系統，有著多個氣囊及容許空氣流動的肺部。再者，若具有鳥類的呼吸系統，迷惑龍只需要約600公升的肺活量；而以哺乳動物的系統來估計，迷惑龍需要2950公升的肺活量，已超越所擁有的胸部體積。一隻30公噸的迷惑龍，其胸部總體積估計是1700公升\[45\]。

如果恐龍具有類似鳥類的氣囊，牠們將具有比體型相近的動物更高的呼吸效能。恐龍的體型大、行為活躍，氣囊系統可以提供更多的氧氣量，並協助排除體內過多的熱量\[46\]。

[Airsacs-bird.jpg](https://zh.wikipedia.org/wiki/File:Airsacs-bird.jpg "fig:Airsacs-bird.jpg")是[肋骨中段的突出小鉤](../Page/肋骨.md "wikilink")。其他氣囊構造依序為：1.頸部氣囊、2.鎖骨氣囊、3.頭端胸氣囊、4.尾端胸氣囊、5.腹部氣囊、5'.腹部氣囊的骨盆分支、6.[肺臟](../Page/肺臟.md "wikilink")、7.[氣管](../Page/氣管.md "wikilink")\]\]

### 肋骨的鉤突

鳥類的[肋骨中段後方有個突出小鉤](../Page/肋骨.md "wikilink")，名為[鉤突](../Page/鉤突.md "wikilink")（Uncinate
processes）。當胸腔擴張以吸進[氧氣時](../Page/氧氣.md "wikilink")，鉤突能增進胸肌的效能。鉤突的大小與鳥類的生活方式、養氣需求有關聯：無法飛行鳥類的鉤突最短，而可游泳鳥類的鉤突最長；可游泳鳥類必須要在水面的短時間內，吸進最多氧氣。非鳥類的[手盜龍類具有鉤突](../Page/手盜龍類.md "wikilink")，就比例上而言，牠們的鉤突長於現代可游泳鳥類，顯示手盜龍類的呼吸過程需要相當大量的氧氣\[47\]\[48\]。

肋間骨板也可能具有類似鉤突的功能，目前已在[鳥臀目的](../Page/鳥臀目.md "wikilink")[奇異龍發現肋間骨板](../Page/奇異龍.md "wikilink")，被認為是氧氣需求與消耗量高、以及代謝率高的證據\[49\]。

### 鼻甲

[鼻甲是種細薄骨質結構](../Page/鼻甲.md "wikilink")，位於鼻腔外側壁上。大部分哺乳類、鳥類的鼻甲具有嗅覺功能，可以吸附空氣中的化學物質，協助嗅覺；大部分哺乳類、鳥類的鼻甲也具有黏膜，可以讓吸進的空氣溫暖、潮濕，並從呼出的空氣吸取熱量、水氣，防止[肺臟乾燥](../Page/肺臟.md "wikilink")。
[Illu_nose_nasal_cavities.jpg](https://zh.wikipedia.org/wiki/File:Illu_nose_nasal_cavities.jpg "fig:Illu_nose_nasal_cavities.jpg")位置，與其他[哺乳類相比相當簡易](../Page/哺乳類.md "wikilink")\]\]
約翰·魯本等人在數個研究中指出\[50\]\[51\]\[52\]\[53\]\[54\]：

  - 沒有恐龍化石曾發現鼻甲，包含[虛骨龍類](../Page/虛骨龍類.md "wikilink")。
  - 所以已發現具有鼻管的恐龍，由於鼻管短、狹窄，無法容納鼻甲。
  - 根據以上特徵，恐龍的肺部乾燥，無法具有哺乳類與鳥類的呼吸頻率，以及高的代謝率。

然而，目前已出現數個反對意見：

  - 某些鳥類（[鴕鳥目](../Page/鴕鳥目.md "wikilink")、[鸌形目](../Page/鸌形目.md "wikilink")、[隼形目](../Page/隼形目.md "wikilink")）與哺乳類（[鯨魚](../Page/鯨魚.md "wikilink")、[穿山甲](../Page/穿山甲.md "wikilink")、[蝙蝠](../Page/蝙蝠.md "wikilink")、大象、大部分[靈長類](../Page/靈長類.md "wikilink")）也缺乏呼吸用鼻甲，或者鼻甲非常小\[55\]\[56\]\[57\]\[58\]，卻是[內溫性動物](../Page/內溫性.md "wikilink")，其中少部分甚至是非常靈活的動物。
  - 不同的科學家提出，已發現鼻管的恐龍，牠們的鼻管長又寬，足以容納鼻甲，或是類似的防止肺臟乾燥的器官\[59\]。
  - 鼻甲是種非常脆弱的結構，不太可能在化石化的過程中保存下來。目前的鳥類化石，都沒有發現過鼻甲。

## 心血管系統

[Willoheart.jpg](https://zh.wikipedia.org/wiki/File:Willoheart.jpg "fig:Willoheart.jpg")化石，可能具有石化的心臟\]\]
科學家普遍認為，恐龍的血壓相當高，才能將血液輸送到高舉的頭部，因此恐龍應具有四腔室[心臟](../Page/心臟.md "wikilink")，[心室已經分為左右兩邊](../Page/心室.md "wikilink")。但是，[脊椎動物的](../Page/脊椎動物.md "wikilink")[肺臟無法在過高的血壓下運作良好](../Page/肺臟.md "wikilink")\[60\]。在2000年，一個名為「Willo」的[奇異龍化石](../Page/奇異龍.md "wikilink")，被發現有四腔室心臟與一個[主動脈的痕跡](../Page/主動脈.md "wikilink")，目前正存放在[北卡羅萊納自然科學博物館](../Page/北卡羅萊納自然科學博物館.md "wikilink")。研究人員根據心臟結構，認為奇異龍有較高的代謝效率，並非[變溫動物](../Page/變溫動物.md "wikilink")\[61\]。他們的結論已遭到否定；其他研究人員公佈了一個研究，宣稱這顆類似心臟的物質，其實是個[結石](../Page/結石.md "wikilink")。他們認為這個物體的結構令人誤解，例如所謂的主動脈結構雖然進入心臟結構，但缺乏所連接的[動脈](../Page/動脈.md "wikilink")、主動脈與心臟的連接處最為狹窄；實際上，該物體與一根[肋骨連接](../Page/肋骨.md "wikilink")，中心有同心圓層，右腿也有一樣的同心圓層\[62\]。而最初提出石化心臟的研究人員捍衛他們的理論，他們同意有某種形式的同心圓層存在，但其中一層則包圍了心臟與主動脈的肌肉部位\[63\]。

這顆疑似石化心臟的物體是否可以反映奇異龍的代謝率與內部結構，仍然沒有定論。現代鱷魚與鳥類都擁有四腔室心臟（鱷魚的心臟較不一樣），所以恐龍可能也有類似的心臟結構；而心臟結構與代謝率沒有必然的關係\[64\]。

## 成長率

目前所發現的恐龍蛋，大小都不超過[籃球](../Page/籃球.md "wikilink")；且已在相當小的蛋中發現相當大的恐龍胚胎，例如[慈母龍](../Page/慈母龍.md "wikilink")\[65\]。恐龍在經歷成長期後，成年期間即停止成長，類似[哺乳動物](../Page/哺乳動物.md "wikilink")；爬行動物達到成年期後，如果食物充足，仍會緩慢地持續成長。若與體型相近的現代爬行動物相比，恐龍的成長速率快於現代的爬行動物。以下為各種體型的恐龍成長率與現代溫血動物的比較圖\[66\]\[67\]：

| 體重(公斤)      | 恐龍的相對成長率                                                                               | 體型相近的現代哺乳動物                                                               |
| ----------- | -------------------------------------------------------------------------------------- | ------------------------------------------------------------------------- |
| 0.22        | 比[有袋類緩慢](../Page/有袋類.md "wikilink")                                                    | [老鼠](../Page/老鼠.md "wikilink")                                            |
| 1 - 20      | 接近有袋類，比[早熟性鳥類](../Page/早熟性.md "wikilink")（出生時即可行走）緩慢                                   | 介於[天竺鼠與](../Page/天竺鼠.md "wikilink")[安第斯禿鷹之間](../Page/安第斯禿鷹.md "wikilink") |
| 100 - 1000  | 比有袋類快速，接近早熟性鳥類，比[胎盤類哺乳類緩慢](../Page/胎盤類.md "wikilink")                                  | 介於[紅袋鼠與](../Page/紅袋鼠.md "wikilink")[北極熊間](../Page/北極熊.md "wikilink")      |
| 1500 – 3500 | 接近大部分胎盤類哺乳類                                                                            | 介於[北美野牛與](../Page/北美野牛.md "wikilink")[犀牛之間](../Page/犀牛.md "wikilink")     |
| 25000公斤以上   | 非常快速，接近現代[鯨魚](../Page/鯨魚.md "wikilink")，是[晚熟性鳥類](../Page/晚熟性.md "wikilink")（出生時無毛）的一半。 | 鯨魚                                                                        |

[Tyrantgraph.png](https://zh.wikipedia.org/wiki/File:Tyrantgraph.png "fig:Tyrantgraph.png")恐龍的推測生長曲線（體重與年齡比例）。分別為[暴龍](../Page/暴龍.md "wikilink")(黑)、[懼龍](../Page/懼龍.md "wikilink")(綠)、[蛇髮女怪龍](../Page/蛇髮女怪龍.md "wikilink")(藍)、[艾伯塔龍](../Page/艾伯塔龍.md "wikilink")(紅)。此圖表根據埃里克森等人的2004年研究\]\]
暴龍具有恐龍中最特殊的成長率\[68\]\[69\]。

  - 暴龍在十歲時，體重約0.5公噸
  - 在接下來數年的成長期間，每年成長約0.5公噸，成長速率非常快
  - 在成長期結束後，體重達到約2公噸，成長率極度緩慢。

在2008年，一份[鴨嘴龍類的](../Page/鴨嘴龍類.md "wikilink")[亞冠龍研究根據骨頭中的成長環數量與形狀](../Page/亞冠龍.md "wikilink")，指出亞冠龍的成長迅速，在15歲時成長至巨大體型。為了避免掠食動物的獵食造成群落個體的減少，被獵食動物的成長速度通常快於掠食動物；而牠們的生存環境也允許快速的成長率\[70\]。

恐龍的生命似乎相當短，舉例而言，目前最年老的[暴龍化石的年齡約](../Page/暴龍.md "wikilink")28歲，最年老的[蜥腳下目恐龍則為](../Page/蜥腳下目.md "wikilink")38歲\[71\]。幼年恐龍的極高死亡率，可能與被獵食有關；而成年恐龍的高死亡率，則是與性競爭有關\[72\]。

## 代謝

自從19世紀中期以來，科學界對於恐龍的生活方式、[代謝](../Page/代謝.md "wikilink")、[體溫調節等層面的理論](../Page/體溫調節.md "wikilink")，有過多次的變動。科學家們對於恐龍的體溫調節能力的看法相當不一致。近年來，恐龍溫血動物說逐漸成主流理論，牠們被視為活躍的動物，至少具有相當穩定的體溫。目前的爭論多在於牠們的體溫調節機制，以及牠們與[鳥類](../Page/鳥類.md "wikilink")、[哺乳類的代謝率相近程度](../Page/哺乳類.md "wikilink")。

[恆溫動物是個複雜且令人混淆的名詞](../Page/恆溫動物.md "wikilink")。当一种动物具有以下特性中的几项或全部时，它可能被称为恒温动物：

  - [內溫性](../Page/內溫性.md "wikilink")（Endothermy）：體熱來自於體內，例如燃燒[脂肪](../Page/脂肪.md "wikilink")，而非藉由曝曬陽光、肌肉活動等行為來獲得熱量。內溫性是維持穩定體溫的最佳方式，但是需要相當高的能量。舉例而言，現代哺乳動物的食物量，是現代爬行動物的10到13倍\[73\]。
  - [恆溫性](../Page/恆溫性.md "wikilink")（Homeothermy）：能夠維持自身溫定的體溫穩定。現代恆溫性動物有不同的體溫：[單孔目與](../Page/單孔目.md "wikilink")[樹懶的體溫介於](../Page/樹懶.md "wikilink")[攝氏](../Page/攝氏.md "wikilink")28度到30度之間，[有袋類的體溫介於攝氏](../Page/有袋類.md "wikilink")33度到36度間，大部分[胎盤類的體溫介於攝氏](../Page/胎盤類.md "wikilink")36度到38度之間，[鳥類的體溫約在攝氏](../Page/鳥類.md "wikilink")41度\[74\]。
  - [代謝過速](../Page/代謝過速.md "wikilink")（Tachymetabolism）：維持高的代謝率，尤其是休息期間。這需要相當高且穩定的體溫，因為體溫在攝氏10度時，[生物化學反應速度只有正常的一半](../Page/生物化學.md "wikilink")，大部分[酶的催化反應都有適宜的溫度](../Page/酶.md "wikilink")，當處在不適宜的溫度時，它們的反應速率將大幅下降\[75\]。

大型恐龍可能藉由其巨大的體型，將熱量穩定地保持在體內，此現象名為[巨溫性](../Page/巨溫性.md "wikilink")（Gigantothermy）。這些動物的體型大，[熱容量也大](../Page/熱容量.md "wikilink")，牠們必須透過多種方式才能大幅改變體溫，因此其一天內的體溫變化相當平順。某些大型的現存[烏龜](../Page/烏龜.md "wikilink")、[鱷魚可發現這種平順的體溫變化](../Page/鱷魚.md "wikilink")，體重約700公斤的[板龍](../Page/板龍.md "wikilink")，可能是最小的巨溫性恐龍。小型恐龍、大型恐龍的幼年個體可能無法具有巨溫性\[76\]。大型草食性恐龍的消化系統內的植物，發酵時會造成相當多的熱量，但是肉食性恐龍、大型草食性恐龍的幼年個體，無法藉由此種方式來獲得穩定的熱量\[77\]。

因為這些已滅絕動物的內部機能仍不清楚，目前大部分的爭論多傾向於恐龍是恆溫性或代謝過速。

若將活躍時與休息時的不同代謝率列入考量，恐龍的代謝率爭議更為複雜。大部分的鳥類與哺乳類，以及所有的爬行動物，活躍時的代謝率是休息時的代謝率的10到20倍。少數哺乳動物的代謝率差異更達到70倍。就理論上，恐龍這種大型陸地脊椎動物，可能在活躍時的代謝率接近鳥類，在休息時的代謝率接近爬行動物。但是，若恐龍在休息時的代謝率低，將無法迅速地成長。大型的草食性[蜥腳類恐龍可能持續地尋找食物](../Page/蜥腳類.md "wikilink")，所以牠們所消耗的能量無明顯變化，與休息的代謝率高低無關\[78\]。

### 假設

關於恐龍的代謝率程度，目前已有以下不同假設\[79\]：

  - 如同現代爬行動物，恐龍屬於[變溫動物](../Page/變溫動物.md "wikilink")；而大型恐龍的體型有助於穩定體溫。
  - 恐龍屬於恆溫動物，較類似現代鳥類與哺乳類，而非現代爬行動物。
  - 恐龍具有與現代動物不同的代謝模式，代謝程度介於現代恆溫動物與變溫動物之間。
  - 不同的恐龍族系，具有不同的代謝率與體溫調節方式，存在者兩種以上的代謝程度。

由於恐龍生存超過1.5億年，很有可能不同的恐龍[演化支具有不同的代謝率與體溫調節方式](../Page/演化支.md "wikilink")，而後期恐龍可能演化出不同於早期恐龍的生理特徵。

如果部分或所有的恐龍具有介於現代恆溫動物與變溫動物之間的代謝率，可能有以下特徵\[80\]：

  - 休息時的代謝率低。這將降低食物的需求量，而且比休息時代謝率高的動物，有更多的能量可用在成長上。
  - 巨溫性。
  - 可將皮膚下的血管收縮、擴張，來控制熱量的消散，如同許多現代爬行動物。
  - 四腔室[心臟](../Page/心臟.md "wikilink")，以兩個系統運作。
  - 高[攝氧量](../Page/攝氧量.md "wikilink")（Aerobic capacity），以維持穩定的活動。

Robert
Reid認為，恐龍與三疊紀的原始哺乳類，都曾具有這些特徵。由於[主龍類佔據中到大型動物的](../Page/主龍類.md "wikilink")[生態位](../Page/生態位.md "wikilink")，這些原始哺乳類的體型逐漸變小。由於表面積/體積的比例增加，原始哺乳類必須增加體內製造的熱量，最終成為完全的內溫性動物。恐龍佔據中到大型動物的生態位，因此繼續保持中等程度的代謝率\[81\]。

### 骨頭結構

在1974年，阿爾曼德·德·利克萊斯（Armand de
Ricqlès）宣稱在恐龍骨頭中發現[哈氏管](../Page/哈氏管.md "wikilink")（Haversian
canals），並認為這是恐龍溫血動物說的重要證據。哈氏管在溫血動物身上相當普遍，這些結構有助於骨骼成長、在受傷時快速復原，使動物具有高成長率及活躍的生活方式\[82\]。巴克則提出恐龍的纖維層狀骨頭代表骨頭的成長快速，也被視為恐龍溫血動物說的重要證據\[83\]。

其他科學家認為判斷恐龍、哺乳類、爬行動物的代謝率時，骨頭結構並不是個可靠的參考因素，尤其是纖維層狀結構：

  - 恐龍的骨頭具有[成長環](../Page/成長環.md "wikilink")，成長環則代表慢的成長率與慢代謝率。許多研究直接以成長環計算恐龍的年齡\[84\]\[85\]。季節性的體溫變化會影響成長環的生長，季節性的體溫變化常代表緩慢的代謝率與[變溫性動物](../Page/變溫性.md "wikilink")。極區的[熊與具冬眠習性的哺乳類也具有成長環](../Page/熊.md "wikilink")\[86\]\[87\]。
  - 幼年鱷魚的骨頭常發現纖維層狀結構，有時成年鱷魚也具有發現纖維層狀骨頭。此外，許多恐龍的長骨頭（例如[股骨](../Page/股骨.md "wikilink")）則同時具有纖維層狀與層帶狀兩種型態，而層帶狀骨頭則意味者緩慢的成長率\[88\]\[89\]。
  - 烏龜、[喙頭蜥](../Page/喙頭蜥.md "wikilink")、鱷魚的骨頭也具有哈氏管\[90\]，鳥類、[蝙蝠](../Page/蝙蝠.md "wikilink")、[鼩鼱](../Page/鼩鼱.md "wikilink")、[齧齒類的骨頭常缺乏哈氏管](../Page/齧齒類.md "wikilink")\[91\]。

在2008年，利克萊斯將研究的對象擴大到主龍類與早期的恐龍，並提出以下假設\[92\]：

  - 最早期的[主龍形類的成長相當快速](../Page/主龍形類.md "wikilink")，顯示牠們的代謝率相當高。但是，因為骨頭結構與成長率非常可能因為物種的不同而產生差異，研究人員在取樣時可能會出現偏差，因此這個比較是非常投機的。
  - 三疊紀的主龍類可分為三個群體：[鳥頸類主龍仍保持快速的成長率](../Page/鳥頸類主龍.md "wikilink")，後來演化出[翼龍類與恐龍](../Page/翼龍類.md "wikilink")；早期[鱷形超目動物](../Page/鱷形超目.md "wikilink")，演變成接近爬行動物的緩慢成長率；而大部分的其他主龍類，則擁有居中的代謝率。

### 成長率

恐龍從剛出生的胚胎，迅速成長至巨大體型。這也顯示恐龍攝取食物養分至身體相當快速，這需要相當快的代谢率才能吸收、消化食物\[93\]。

但是，一個關於成年體體型、成長率、體溫的初步研究，認為大型恐龍的體溫高於小型恐龍，研究中最大型的恐龍是迷惑龍，迷惑龍的體溫超過[攝氏](../Page/攝氏.md "wikilink")41度，較小型恐龍的體溫約攝氏25度\[94\]；而人類的體溫約攝氏37°\[95\]\[96\]。這個研究認為大型恐龍是巨溫性動物，過大的體型將熱量保持在體內；其他恐龍則是[外溫性](../Page/外溫性.md "wikilink")（Ectothermic）動物，也就是所謂的冷血動物\[97\]。這些結果與恐龍的體型/成長率變化相符合\[98\]\[99\]。

### 氧同位素比例

骨頭中的[氧<sup>16</sup>與](../Page/氧的同位素.md "wikilink")[氧<sup>18</sup>比例](../Page/氧的同位素.md "wikilink")，會依骨頭形成時的溫度而有不同，溫度越高，氧<sup>16</sup>的比例越高。在1999年，R.E.
Barrick與W.J.
Showers研究了[暴龍](../Page/暴龍.md "wikilink")（[美國](../Page/美國.md "wikilink")）與[南方巨獸龍](../Page/南方巨獸龍.md "wikilink")（[阿根廷](../Page/阿根廷.md "wikilink")）兩種恐龍的氧同位素比例，牠們都生存於的[溫帶地區](../Page/溫帶.md "wikilink")，這些地區的氣溫有季節性變化。他們有以下發現\[100\]：

  - 兩種恐龍的[背椎溫度沒有季節性變化](../Page/背椎.md "wikilink")，顯示牠們儘管生存於有季節性變化的氣候環境，身體仍能維持穩定的體溫。
  - 肋骨與腿部骨頭有較大的溫度變化，平均溫度低於脊椎的溫度。

Barrick與Showers的結論是，暴龍與南方巨獸龍是內溫性動物，但代謝率低於現代哺乳類；而當牠們成年時，成為恆溫性動物。除此之外，他們也曾研究過[白堊紀晚期的](../Page/白堊紀.md "wikilink")[鳥臀目化石](../Page/鳥臀目.md "wikilink")，得出類似的變化模式\[101\]。

其他科學家對這個研究提出質疑。他們認為這只能證明動物是恆溫性動物，但無法證明牠們的代謝率達到內溫性動物的程度。另外，四肢接近末端處的骨頭可能無法持續性生長；在[異特龍類的骨頭中](../Page/異特龍類.md "wikilink")，四肢主要骨頭的[停止生長線](../Page/停止生長線.md "wikilink")（Lines
of arrested
growth，縮寫為LAG，類似[成長環](../Page/成長環.md "wikilink")）少見或缺乏，而手指與腳指骨頭的停止生長線卻很常出現。雖然沒有明確證據顯示停止成長線與體溫的關聯，但可紀錄骨頭因寒冷而停止成長的時刻。如果身體中有部分骨頭在寒冷季節無法或緩慢生長，那骨頭的氧同位素比例將無法作為體溫的可靠參考\[102\]。

### 掠食者/獵物比例

羅伯特·巴克曾提出\[103\]：

  - 冷血掠食動物所需要的食物，遠少於溫血掠食動物，所以一個生態系統中所能支撐的冷血掠食動物數量，比溫血掠食動物更多。
  - 恐龍中的掠食者/獵物比例，較接近現代溫血動物，而非現代冷血動物。
  - 從以上可推知，掠食性恐龍是溫血動物。因為最早的恐龍也是掠食性動物，例如[南十字龍與](../Page/南十字龍.md "wikilink")[艾雷拉龍](../Page/艾雷拉龍.md "wikilink")，因此所有的後期恐龍也應該是溫血動物。

巴克的理論遭到以下反對意見\[104\]\[105\]：

  - 恐龍的體重多為估計的，而且各種版本的估計值差異大。即使是小幅度的體重差異，所產生的掠食者/獵物比例也會有差異大的結果。
  - 巴克所取樣的化石來自於[博物館的收藏](../Page/博物館.md "wikilink")，但是博物館多傾向收藏保存狀態良好的化石。再者，地層中的化石不能準確代表實際的生態系，較小與較年輕的動物的骨頭較小，較不易在化石記錄中保存下來。因此巴克的統計數值不能代表真實的掠食者/獵物比例。
  - 現今並沒有大型冷血動物可供比較，無法推斷冷血掠食動物的掠食者/獵物比例是否較高。現存的大型掠食動物非常少，大部分生存於小型島嶼；大型冷血[草食性動物也非常少](../Page/草食性.md "wikilink")。巴克所參考用的掠食者/獵物比例，是計算哺乳類、魚類與無脊椎動物得出的結果；但是這些動物的生命期長短與其他因素，也會影響最後得出的數值。
  - 就理論上，掠食動物的數量僅受限於可獲得的獵物數量。但是，築巢地點的短缺、[同類相食](../Page/同類相食.md "wikilink")、掠食動物間的獵食…等因素也會影響掠食動物的數量，進而降低掠食者/獵物比例。
  - 生態因素可能會降低掠食者/獵物比例。某些掠食動物可能以特定的動物為食；部分獵物可能死於疾病、寄生蟲、飢荒，而非被其他動物獵食。
  - 難以準確地列出哪些動物以哪些動物為食。舉例而言，[蜥蜴](../Page/蜥蜴.md "wikilink")、[蛇可能會獵食幼年草食性動物](../Page/蛇.md "wikilink")，而哺乳類可能會獵食成年草食性動物。相反地，幼年掠食性動物多以[無脊椎動物為食](../Page/無脊椎動物.md "wikilink")，成年後以脊椎動物為食。大部分的掠食動物，同時也會有食腐的行為；而大型[食腐動物](../Page/食腐動物.md "wikilink")，也會面對成群的小型食腐生物的競爭，例如[細菌](../Page/細菌.md "wikilink")、[真菌](../Page/真菌.md "wikilink")、[昆蟲](../Page/昆蟲.md "wikilink")…等。
  - 在現代的生物群中，代謝率與食性並不相關，例如，[走鵑是](../Page/走鵑.md "wikilink")[雜食性動物](../Page/雜食性.md "wikilink")，食物來源相當廣泛，而許多爬行動物也以哺乳類為食。
  - 目前並不清楚非掠食性恐龍的抵抗掠食者方式。而一個生態系中，保衛良好的動物，將會支撐較少的掠食動物。

### 步態

[Sprawling_and_erect_hip_joints_-_horiz.png](https://zh.wikipedia.org/wiki/File:Sprawling_and_erect_hip_joints_-_horiz.png "fig:Sprawling_and_erect_hip_joints_-_horiz.png")與[恐龍](../Page/恐龍.md "wikilink"))、柱狀直立方式([勞氏鱷目](../Page/勞氏鱷目.md "wikilink"))\]\]
從恐龍化石的[關節表面角度](../Page/關節.md "wikilink")，與肌肉、肌腱的附著點，可知恐龍的四肢直立於身體之下，而非如蜥蜴與[蠑螈般往身體往兩側延展](../Page/蠑螈.md "wikilink")。若將恐龍的化石重建成往兩側延展的步態，臀部、膝蓋、肩膀、手肘等部位的關節將脫臼
\[106\]。

四肢往兩側延展的脊椎動物在移動時，身體兩側的肌肉會收縮（移動緩慢、具龜殼的[陸龜與](../Page/陸龜.md "wikilink")[海龜例外](../Page/海龜.md "wikilink")）妨礙[肺臟的擴大](../Page/肺臟.md "wikilink")、收縮，因此牠們無法在移動時呼吸。這將迫使他們花大部分時間在休息，且只能做短時間移動，以縮減起身與趴下時消耗的能量\[107\]。

四肢直立的脊椎動物在快速運動時，可以同時呼吸，消耗在身體起身與趴下的能量更多。從直立四肢顯示，恐龍是群活躍的動物，這需要相當高的代謝率，才能快速製造熱量，排除廢物。從這觀點來看，恐龍被認為是某種程度的恆溫性動物。

巴克與奧斯特倫姆指出所有的恐龍都具有直立的後肢，而大部分的四足恐龍（除了角龍類與甲龍類）具有直立的前肢。另外，所有現存的內溫性[哺乳類與](../Page/哺乳類.md "wikilink")[鳥類都是後肢直立的動物](../Page/鳥類.md "wikilink")（奧斯特倫姆認為鱷魚有時可以高姿態方式行走，是個例外）。巴克認為恐龍的直立步態，代表牠們是內溫性動物；奧斯特倫姆認為這個推論是可信的，但還不足以做出確定的結論\[108\]\[109\]。

在2009的一項研究，推測至少大型恐龍是[內溫性動物](../Page/內溫性.md "wikilink")；並根據四肢的奔跑能力，推測最早從Dinosauriforms（恐龍的最近祖先）就已經是內溫性動物\[110\]。

### 羽毛

[Microraptor_gui_(dinos).jpg](https://zh.wikipedia.org/wiki/File:Microraptor_gui_\(dinos\).jpg "fig:Microraptor_gui_(dinos).jpg")化石，被視為是鳥類的演化過程中，曾經歷過四翼（包含後肢）飛行階段的可能證據。\]\]
[恐龙皮肤有多种覆盖物](../Page/恐龙皮肤.md "wikilink")，目前已發現許多[獸腳亞目恐龍具有](../Page/獸腳亞目.md "wikilink")[羽毛](../Page/羽毛.md "wikilink")，包含：[鳥面龍](../Page/鳥面龍_\(蒙古\).md "wikilink")、[中華龍鳥](../Page/中華龍鳥.md "wikilink")、[帝龍](../Page/帝龍.md "wikilink")（一種早期[暴龍超科](../Page/暴龍超科.md "wikilink")）\[111\]\[112\]\[113\]。牠們的羽毛被認為用來隔絕熱量，被視為是溫血動物的證據。

但恐龍的羽毛痕跡大多發現於[虛骨龍類](../Page/虛骨龍類.md "wikilink")（鳥類與暴龍類的祖先）之中，所以無法從羽毛得知其他恐龍演化支的身體機能，例如：[腔骨龍科](../Page/腔骨龍科.md "wikilink")、[角鼻龍下目](../Page/角鼻龍下目.md "wikilink")、[肉食龍下目](../Page/肉食龍下目.md "wikilink")、[蜥腳下目](../Page/蜥腳下目.md "wikilink")、與[鳥臀目](../Page/鳥臀目.md "wikilink")。

從[阿貝力龍科的](../Page/阿貝力龍科.md "wikilink")[食肉牛龍的化石化皮膚顯示](../Page/食肉牛龍.md "wikilink")，該種恐龍並沒有覆蓋着羽毛，而是類似蜥蜴，上有多排瘤狀物\[114\]。成年食肉牛龍的體重約一公噸，體重大於或等於食肉牛龍的哺乳類，身上不是光滑的皮膚，就是很短的毛髮。因此無法從食肉牛龍得知較小的非虛骨龍類恐龍是否具有羽毛。另一群大型的獸腳類恐龍，[鐮刀龍超科](../Page/鐮刀龍超科.md "wikilink")，可能也具有羽毛；其中的[北票龍似乎具有絨羽](../Page/北票龍.md "wikilink")。

[蜥腳下目中的](../Page/蜥腳下目.md "wikilink")[畸形龍與某些物種的皮膚痕跡](../Page/畸形龍.md "wikilink")，已發現六角形的鱗片。而部份蜥腳類恐龍則具有骨板，例如[薩爾塔龍](../Page/薩爾塔龍.md "wikilink")\[115\]。[角龍下目的](../Page/角龍下目.md "wikilink")[三角龍](../Page/三角龍.md "wikilink")，具有大型的六角形鱗片，有時散佈着圓形鱗片\[116\]。[鴨嘴龍科的一些](../Page/鴨嘴龍科.md "wikilink")[木乃伊化標本](../Page/木乃伊.md "wikilink")，也覆蓋着鱗片。[包頭龍等](../Page/包頭龍.md "wikilink")[甲龍科恐龍](../Page/甲龍科.md "wikilink")，身體覆蓋着骨質鱗甲與骨板，不太可能具有羽毛等覆蓋物\[117\]。相同地，[劍龍類也不太可能具有羽毛覆蓋物](../Page/劍龍類.md "wikilink")，牠們可能利用背部的角板來調節體溫。

### 極區恐龍

目前已在[澳洲東南部](../Page/澳洲.md "wikilink")、[南極洲](../Page/南極洲.md "wikilink")、[阿拉斯加](../Page/阿拉斯加.md "wikilink")[北灣等地發現恐龍化石](../Page/阿拉斯加北灣.md "wikilink")。在[白堊紀時](../Page/白堊紀.md "wikilink")，澳洲與南極州互相連接，且位於[南極圈內](../Page/南極圈.md "wikilink")；阿拉斯加北灣則位於[北極圈內](../Page/北極圈.md "wikilink")。沒有證據指出當時的[地軸傾斜角度](../Page/地軸.md "wikilink")，與現在有明顯差異，所以極區的恐龍與生態系統仍會面臨長達數個月的[永晝與](../Page/永晝.md "wikilink")[永夜](../Page/永夜.md "wikilink")\[118\]。這些地區氣候寒冷，但仍比現在溫暖許多，地表沒應該沒有覆蓋者[冰河](../Page/冰河.md "wikilink")。這些極地恐龍無法如其他恐龍一樣穴居，即使牠們的身體有覆蓋物，或是聚集在一起，牠們仍必須從身體製造足夠的熱量，以度過長久的冬季。

科學家研究阿拉斯加北灣的植物化石，地質年代約在白堊紀最後3500萬年時，北灣的最高溫是13°C，最低溫是2°C到8°C；這個氣溫低於現代的[奧勒岡州](../Page/奧勒岡州.md "wikilink")[波特蘭](../Page/波特蘭_\(奧勒岡州\).md "wikilink")，高於[亞伯達省](../Page/亞伯達省.md "wikilink")[卡加利](../Page/卡加利.md "wikilink")。在阿拉斯加北灣的化石中，沒有發現大型冷血動物，例如[蜥蜴](../Page/蜥蜴.md "wikilink")、[鱷魚](../Page/鱷魚.md "wikilink")；當時緯度稍晚的亞伯達省、[蒙大拿州](../Page/蒙大拿州.md "wikilink")、[懷俄明州常發現這些動物的化石](../Page/懷俄明州.md "wikilink")。由此可知至少某些恐龍是溫血動物\[119\]。也有理論認為，這些北極區恐龍可能是冷血動物，牠們在夏季時棲息於阿拉斯加，在冬季時遷徙到較溫暖的地區\[120\]。但若以阿拉斯加到蒙大拿州的旅程作為計算，遷徙的過程中所耗費的能量，將高於冷血陸地脊椎動物在一年內產生的能量。因此這些北極區恐龍無論是冬季時遷徙到其他地區，或是終年居住在北極區，牠們應該是溫血動物\[121\]。在2008年，Phil
R. Bell、Eric
Snively提出一份關於恐龍遷徙的研究，認為大部分生存於極區的恐龍可能有過冬的習性，包含：[獸腳亞目](../Page/獸腳亞目.md "wikilink")、[蜥腳下目](../Page/蜥腳下目.md "wikilink")、[甲龍下目](../Page/甲龍下目.md "wikilink")、[稜齒龍類](../Page/稜齒龍類.md "wikilink")，而[鴨嘴龍科](../Page/鴨嘴龍科.md "wikilink")[埃德蒙頓龍可能會遷徙達](../Page/埃德蒙頓龍.md "wikilink")2,600公里的路程\[122\]\[123\]。

澳洲東南部地層的年代是下白堊紀的末期，約為1億1500萬年到1億500萬年前。這些地層發現了[永凍層](../Page/永凍層.md "wikilink")、[冰楔](../Page/冰楔.md "wikilink")、以及地下冰層移動產生的圓丘，顯示當地的年均溫應介於-6°C到5°C之間。科學家研究土壤中的氧同位素，則指出當地的年均溫介於1.5°C到2.5°C之間。但是，當地發現的植被很多樣化，也發現大型植物的化石，與現今的寒冷地區不同（例如年均溫只有2.9°C的阿拉斯加州[費爾班克斯](../Page/費爾班克斯_\(阿拉斯加州\).md "wikilink")）\[124\]。當時澳洲東南部的北方有個海道，阻礙[雷利諾龍這種身長只有](../Page/雷利諾龍.md "wikilink")60到90公分的小型[草食性恐龍往北方溫暖地區遷徙](../Page/草食性.md "wikilink")\[125\]。除了雷利諾龍以外，當地還發現[似提姆龍的化石](../Page/似提姆龍.md "wikilink")，似提姆龍屬於似鳥龍類，身長3.5公尺，臀部高度為1.5公尺。似提姆龍的化石有[停止生長線](../Page/停止生長線.md "wikilink")，所以牠們可能有[冬眠習性](../Page/冬眠.md "wikilink")；雷利諾龍的化石沒有停止生長線，因此牠們可能在冬季依然活耀，沒有冬眠習性\[126\]。

### 調節體溫行為

某些恐龍的背部具有帆狀物，帆狀物由[脊椎延伸出的](../Page/脊椎.md "wikilink")[神經棘所支撐](../Page/神經棘.md "wikilink")，例如[棘龍與](../Page/棘龍.md "wikilink")[豪勇龍](../Page/豪勇龍.md "wikilink")。[合弓綱的](../Page/合弓綱.md "wikilink")[異齒龍也具有類似的帆狀物](../Page/異齒龍.md "wikilink")。這些帆狀物的功能可能有：

  - 調整帆狀物角度，吸收[太陽的熱量](../Page/太陽.md "wikilink")。
  - 躲在陰影中，或將帆狀物調整至與太陽光平行，以降低體溫。

但是，具有帆狀物與角板的恐龍僅佔少數，不能由此推知其他恐龍的體溫調節方式。有理論假設，劍龍類的背部角板具有類似[換熱器的體溫調節功能](../Page/換熱器.md "wikilink")。劍龍類的角板的確分布者血管，理論上可以吸收、散失熱量\[127\]。

[劍龍屬的體型大](../Page/劍龍屬.md "wikilink")，這個理論可能成立；但其他的劍龍類，例如[烏爾禾龍](../Page/烏爾禾龍.md "wikilink")、[沱江龍](../Page/沱江龍.md "wikilink")、[釘狀龍](../Page/釘狀龍.md "wikilink")，具有較小的角板，表面積也小，調節體溫的效率令人質疑。因此劍龍類的角板是否用作調節體溫使用，仍有爭議\[128\]。

### 演化過程的爭議

最早的恐龍已有某些會引起溫血動物爭議的特徵，尤其是直立的四肢。恐龍演化成溫血動物的最可能過程與原因有：

  - 恐龍的最近直系祖先－原始[主龍類是冷血動物](../Page/主龍類.md "wikilink")，而恐龍在很早的階段就已演化成溫血動物。這個理論顯示恐龍在少於2,000萬年內迅速演化成溫血動物。但[哺乳類的祖先](../Page/哺乳類.md "wikilink")[合弓綱演化成溫血動物的途徑](../Page/合弓綱.md "wikilink")，開始於[二疊紀中期演化出](../Page/二疊紀.md "wikilink")[次生顎](../Page/次生顎.md "wikilink")\[129\]，最後在[侏羅紀中期演化出毛髮](../Page/侏羅紀.md "wikilink")，過程至少是恐龍的兩倍以上。（目前已知最明確的最早毛髮證據是[獺形狸尾獸](../Page/獺形狸尾獸.md "wikilink")，生存於侏儸紀中期，約1億6400萬年前\[130\]\[131\]。在1950年代，有科學家提出[三疊紀早期的](../Page/三疊紀.md "wikilink")[犬齒獸類可能已具有毛髮](../Page/犬齒獸類.md "wikilink")，例如[三尖叉齒獸](../Page/三尖叉齒獸.md "wikilink")\[132\]\[133\]。但用來判斷毛髮所在的口鼻部小孔，在這些犬齒獸類身上很不明確；再者，少數現存爬行動物也有類似的小孔\[134\]\[135\]。）
  - 恐龍的最近直系祖先－原始主龍類是接近溫血的動物，恐龍再依根據此而演化至溫血動物。這個理論有兩個問題：第一，主龍類的早期演化過程仍不清楚，早[三疊紀已發現大量的主龍類](../Page/三疊紀.md "wikilink")，但晚[二疊紀的主龍類目前只發現](../Page/二疊紀.md "wikilink")[主龍與](../Page/主龍屬.md "wikilink")[原龍兩個物種](../Page/原龍.md "wikilink")；第二，鱷魚的演化出現稍早於恐龍，也是目前僅次於鳥類的恐龍近親，但鱷魚卻是冷血動物。

現代鱷魚是冷血動物，但擁有一些與[溫血動物相關的特徵](../Page/溫血動物.md "wikilink")，因為牠們的促進[氧氣供給方式](../Page/氧氣.md "wikilink")\[136\]：

  - 2個[心室與](../Page/心室.md "wikilink")2個[心房](../Page/心房.md "wikilink")。[哺乳類與](../Page/哺乳類.md "wikilink")[鳥類也有](../Page/鳥類.md "wikilink")2個心室與2個心房。非鱷魚的爬行動物的心臟有3個心房心室，這樣比較沒有效率，因為這樣會將含氧血液與缺氧血液混合在一起，因此會將部份缺氧血液送至身體各處，而非送到[肺臟](../Page/肺臟.md "wikilink")。現代鱷魚雖然擁有四腔室心臟，但與身體相比比例較小，並且與現代哺乳類與鳥類相比，血壓較低。牠們也擁有分導管，可讓牠們位在水面下時，以三腔室心臟運作，以儲存氧氣\[137\]。
  - [橫膈膜](../Page/橫膈膜.md "wikilink")，可協助呼吸。
  - [次生顎](../Page/次生顎.md "wikilink")，可允許動物在進食時可以同時呼吸。
  - [肝瓣](../Page/肝瓣.md "wikilink")，是肺臟的呼吸推動裝置。與哺乳類與鳥類的肺臟推動裝置不同，但根據某些研究人員宣稱，較為類似某些恐龍\[138\]\[139\]。

[Terrestrisuchus_BW.jpg](https://zh.wikipedia.org/wiki/File:Terrestrisuchus_BW.jpg "fig:Terrestrisuchus_BW.jpg")是種非常小型、行動敏捷的[鱷形類](../Page/鱷形類.md "wikilink")，生存於[三疊紀晚期](../Page/三疊紀.md "wikilink")。\]\]
在1980年代晚期，有些科學家提出鱷魚最初為活耀、溫血的掠食者，而牠們的主龍類祖先也是溫血動物\[140\]\[141\]。研究顯示鱷魚的胚胎具有四腔室心臟，成長後改變為三腔室心臟，以適應水中環境。這些研究人員根據[胚胎重演律](../Page/胚胎重演律.md "wikilink")，牠們提出最初的鱷魚具有四腔室心臟，因此牠們為溫血動物，而後來的鱷魚發展出旁管，重新成為冷血動物，以及水底中的伏擊掠食動物\[142\]\[143\]。

近年，科學家研究主龍類的骨頭結構與成長率的關係，提出早期的主龍類有相當高的代謝率，而[三疊紀的鱷魚祖先的代謝率下降](../Page/三疊紀.md "wikilink")，回覆成典型的爬行動物代謝率\[144\]。

如果最初的鱷魚、以及其他的[鑲嵌踝類主龍都是溫血動物](../Page/鑲嵌踝類主龍.md "wikilink")，主龍類與哺乳類演化至溫血動物的所需時間相當接近。這樣可以解決一些演化的謎題：

  - 最早期的鱷類，例如[陸鱷](../Page/陸鱷.md "wikilink")，是種纖細、長腿的陸地掠食動物，生活方式可能相當活躍，這樣需要非常快速的新陳代謝。
  - 其他的鑲嵌踝類主龍似乎擁有直立的四肢，例如[勞氏鱷目](../Page/勞氏鱷目.md "wikilink")。直立的四肢有益於活躍的動物，可讓牠們快速運動時，可以同時呼吸；但不利於緩慢的動物，因為身體站立或趴下時將消耗能量\[145\]。

## 参考文献

## 外部連結

  - [南方巨獸龍、暴龍的體溫調節、生理學比較](http://palaeo-electronica.org/1999_2/gigan/issue2_99.htm)
    by RE Barrick and WJ Showers (1999)

  - [紐約時報報導：發現恐龍的石化心臟](http://www.nytimes.com/library/national/science/042100sci-paleo-dinosaur.html)

  - [具有石化心臟的恐龍「Willo」](https://web.archive.org/web/20070401070110/http://www.dinoheart.org/)
    北卡羅萊納自然科學博物館

  - [阿德萊德大學新聞：鱷魚從溫血動物演化成冷血動物](http://www.adelaide.edu.au/adelaidean/issues/5501/news5550.html)

[Category:恐龙](../Category/恐龙.md "wikilink")
[Category:古生物學](../Category/古生物學.md "wikilink")
[Category:古动物学](../Category/古动物学.md "wikilink")

1.

2.

3.
4.  Lucas, Spencer G. (2000). *Dinosaurs: The Textbook*, 3rd,
    McGraw-Hill Companies, Inc., 3-9.

5.

6.
7.

8.

9.  Bakker, R.T., 1968, The superiority of dinosaurs, *Discovery*, v.
    3(2), p. 11-22

10. Bakker, R. T., 1986. The Return of the Dancing Dinosaurs, in
    *Dinosaurs Past and Present, vol. I* Edited by S. J. Czerkas and E.
    C. Olson, Natural History Museum of Los Angeles County, Los Angeles

11. Bakker, R. T. (1972). *Anatomical and ecological evidence of
    endothermy in dinosaurs*. Nature 238:81-85.

12. R.D.K. Thomas and E.C. Olson (Ed.s), 1980. *A Cold Look at the
    Warm-Blooded Dinosaurs*

13. Benton, M.J. (2005). *Vertebrate Palaeontology*. Oxford, 221-223.

14. Paladino, F.V., O'Connor, M.P., and Spotila, J.R., 1990. Metabolism
    of leatherback turtles, gigantothermy, and thermoregulation of
    dinosaurs. *Nature* 344, 858-860 <doi:10.1038/344858a0>

15. Barrick, R.E., Showers. W.J., Fischer, A.G. 1996. Comparison of
    Thermoregulation of Four Ornithischian Dinosaurs and a Varanid
    Lizard from the Cretaceous Two Medicine Formation: Evidence from
    Oxygen Isotopes *Palaios*, 11:4 295-305 <doi:10.2307/3515240>

16.

17.
18.
19.
20.
21.

22.

23.
24.

25.

26.
27.
28.
29.

30.
31.

32.

33.

34.

35.

36.
37.  Full text currently online at  and  Detailed anatomical analyses
    can be found at

38.

39.  This is also one of several topics featured in a post on Naish's
    blog,  - note *Mirischia* was a coelurosaur which Naish believes was
    closely related to *Compsognathus*.

40.

41.
42.

43.
44.
45. {{ citation|author=Paladino, F.V., Spotila, J.R., and Dodson, P.
    |date=1997|contribution=A Blueprint for Giants: Modeling the
    Physiology of Large Dinosaurs|editor=Farlow, J.O. and Brett-Surman,
    M.K. |title=The Complete Dinosaur |pages=491–504
    |isbn=0253213134|unused_data=|publisher=Indiana University Press}}

46.

47.  News summary at

48.

49.  But note that this paper's main subject is that the fossil provided
    strong evidence of a 4-chambered heart, which is not widely
    accepted.

50.
51.

52.
53.

54.

55.

56.

57.

58. {

59.

60.
61.

62.

63.

64. Chinsamy, Anusuya; and Hillenius, Willem J. (2004). "Physiology of
    nonavian dinosaurs". *The Dinosauria*, 2nd. 643–659.

65.

66.  Note Kristina Rogers also published papers under her maiden name,
    Kristina Curry.

67.

68.

69.

70.

71.
72.

73.
74.
75. {{ cite book | author=McGowan, C. | title=Dinosaurs, Spitfires and
    Sea Dragons | date=1991 | publisher=Hardvard University Press |
    isbn=0-674-20769-6 }}

76.
77. {{ cite journal | author=Mackie, R.I. | title=Mutualistic
    Fermentative Digestion in the Gastrointestinal Tract: Diversity and
    Evolution | journal=Integrative and Comparative Biology | volume=42
    | issue=2 | pages=319–326 | year=2002 |
    url=<http://icb.oxfordjournals.org/cgi/content/full/42/2/319> |
    accessdate=2008-09-12 | doi=10.1093/icb/42.2.319 }}

78. {{ citation | author=Paul, G.S. | chapter=Cope's rule | page=p. 211
    | title=Morrison Symposium Proceedings: A Special Issue Of The
    Journal Modern Geology | editor=Carpenter, K., Fraser, N., Chure,
    D., and Kirkland, J.I. | publisher=Taylor & Francis | date=1998 |
    isbn=9056991833 |
    url=<http://books.google.co.uk/books?id=CLEPeg_SjTcC&dq=sprawling+limbs+efficient&lr=&source=gbs_summary_s&cad=0>
    | accessdate=2008-09-12 }}

79.
80.
81.
82. Ricqles, A. J. de. (1974). *Evolution of endothermy: histological
    evidence*. Evolutionary Theory 1: 51-80

83.
84.
85.
86.

87.

88.

89.

90.

91.
92.  Abstract also online at

93.

94.
95.

96.

97.  There is a less technical summary at

98.
99.
100.

101.

102.
103.

104.

105. {{ citation |author=Farlow, J.O. |date=1980 |chapter=Predator/prey
     biomass ratios, community food webs and dinosaur physiology
     |editor=Thomas, R.D.K. and Olson, E.C. |title=A cold look at the
     warm-blooded dinosaurs |pages=55–83 |publisher=American Association
     for the Advancement of Science |location=Boulder, CO
     |url=<http://bio.fsu.edu/~amarquez/Evolutionary%20Morphology%20fall%202004/Farlow/Predator-Prey%20biomass%20ratios,%20community%20food%20webs%20and%20dinosaur%20physiology.pdf>
     |deadurl=yes
     |archiveurl=<https://web.archive.org/web/20081217091538/http://bio.fsu.edu/~amarquez/Evolutionary%20Morphology%20fall%202004/Farlow/Predator-Prey%20biomass%20ratios%2C%20community%20food%20webs%20and%20dinosaur%20physiology.pdf>
     |archivedate=2008年12月17日 |df= }}

106. This was recognized not later than 1909:  The arguments and many of
     the images are also presented in

107.
108.
109. {{ citation|author=Ostrom, J.H. |date=1980 |chapter=The evidence of
     endothermy in dinosaurs |editor=Thomas, R.D.K. and Olson, E.C.
     |title=A cold look at the warm-blooded dinosaurs |pages=82–105
     |publisher=American Association for the Advancement of Science
     |location=Boulder, CO
     |url=<http://bio.fsu.edu/~amarquez/Evolutionary%20Morphology%20fall%202004/Ostrom/The%20evidence%20for%20endothermy%20in%20dinosaurs%20-%20Ostrom.pdf>
     |deadurl=yes
     |archiveurl=<https://web.archive.org/web/20080527192758/http://bio.fsu.edu/~amarquez/Evolutionary%20Morphology%20fall%202004/Ostrom/The%20evidence%20for%20endothermy%20in%20dinosaurs%20-%20Ostrom.pdf>
     |archivedate=2008-05-27 }}

110. [Biomechanics of Running Indicates Endothermy in Bipedal
     Dinosaurs](http://www.plosone.org/article/info%3Adoi%2F10.1371%2Fjournal.pone.0007783;jsessionid=CE8F3EE637FFD712F6BF85FF02711889)

111.

112.

113.

114.

115. {{ citation |author=Czerkas, S. A. |date=1994 | contribution=The
     history and interpretation of sauropod skin impressions
     |title=Aspects of Sauropod Paleobiology | editor=Lockley, M.G., dos
     Santos, V.F., Meyer, C.A., and Hunt, A.P. | journal=Gaia |volume=10
     |location=Lisbon, Portugal }}

116.  See also image at

117.

118.  See also  and

119.
120. {{ citation |author=Hotton, N. |date=1980 |chapter=An alternative
     to dinosaur endothermy: The happy wanderers|editor=Thomas, R.D.K.
     and Olson, E.C. |title=A cold look at the warm-blooded dinosaurs
     |pages=311–350 |publisher=American Association for the Advancement
     of Science |location=Boulder, CO }}

121.

122.

123.

124.
125.
126.  See also

127. {{ cite journal | author=de Bufrenil, V., Farlow, J.O., and de
     Riqles, A. | title=Growth and function of *Stegosaurus* plates:
     evidence from bone histology | journal=Paleobiology | volume=12 |
     issue=4 | date=1986 | pages=459–473 |
     url=<http://www.jstor.org/pss/2400518>}}

128. {{ cite web |
     url=<http://www.innovations-report.de/html/berichte/biowissenschaften_chemie/bericht-44440.html>
     | title=Stegosaur plates and spikes for looks only | date=18 May
     2005 | deadurl=yes |
     archiveurl=<https://web.archive.org/web/20110927124153/http://www.innovations-report.de/html/berichte/biowissenschaften_chemie/bericht-44440.html>
     | archivedate=2011年9月27日 }}

129.

130.

131.

132.

133.

134.

135.

136.
137.

138.
139.
140.

141.

142. Summers, A.P. (2005). *Evolution: Warm-hearted crocs*. Nature 434:
     833-834

143.  See also the explanation of this, with useful illustrations, at

144.
145.