**麟嘉**（316年十一月-318年六月）是[十六国时期](../Page/十六国.md "wikilink")[前赵政权汉昭武帝](../Page/前赵.md "wikilink")[刘聪的第四个](../Page/刘聪.md "wikilink")[年号](../Page/年号.md "wikilink")，共计3年。

麟嘉三年七月汉隐帝[刘粲即位改元](../Page/刘粲.md "wikilink")[汉昌元年](../Page/汉昌.md "wikilink")。

## 纪年

| 麟嘉                               | 元年                             | 二年                             | 三年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 316年                           | 317年                           | 318年                           |
| [干支](../Page/干支纪年.md "wikilink") | [丙子](../Page/丙子.md "wikilink") | [丁丑](../Page/丁丑.md "wikilink") | [戊寅](../Page/戊寅.md "wikilink") |

## 参看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 其他时期使用的[麟嘉年号](../Page/麟嘉.md "wikilink")
  - 同期存在的其他政权年号
      - [建兴](../Page/建兴_\(晋愍帝\).md "wikilink")（313年四月-317年三月）：[西晋皇帝](../Page/西晋.md "wikilink")[晋愍帝的年号](../Page/晋愍帝.md "wikilink")
      - [建武](../Page/建武_\(晋元帝\).md "wikilink")（317年三月-318年三月）：[东晋皇帝](../Page/东晋.md "wikilink")[晉元帝的年号](../Page/晉元帝.md "wikilink")
      - [大兴](../Page/大兴_\(晋元帝\).md "wikilink")（318年三月-321年）：[东晋皇帝](../Page/东晋.md "wikilink")[晉元帝的年号](../Page/晉元帝.md "wikilink")
      - [建兴](../Page/建兴.md "wikilink")：[前凉政权年号](../Page/前凉.md "wikilink")
      - [玉衡](../Page/玉衡.md "wikilink")（311年-334年）：[成汉政权](../Page/成汉.md "wikilink")[李雄的年号](../Page/李雄.md "wikilink")

[Category:前赵年号](../Category/前赵年号.md "wikilink")
[Category:310年代中国政治](../Category/310年代中国政治.md "wikilink")