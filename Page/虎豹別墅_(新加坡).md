[Haw_Par_Villa_48,_Nov_06.JPG](https://zh.wikipedia.org/wiki/File:Haw_Par_Villa_48,_Nov_06.JPG "fig:Haw_Par_Villa_48,_Nov_06.JPG")
[新加坡的](../Page/新加坡.md "wikilink")**虎豹別墅**，位於新加坡西南部的[巴西班讓路](../Page/巴西班讓路.md "wikilink")，於1937年建造，屬於世界上第二座[虎豹別墅](../Page/虎豹別墅.md "wikilink")。

## 歷史

1937年，[胡文豹](../Page/胡文豹.md "wikilink")（[胡文虎之弟](../Page/胡文虎.md "wikilink")）於新加坡興建第二座虎豹別墅。它是佔地面積最大的「虎豹別墅」，亦是[胡文豹的住所](../Page/胡文豹.md "wikilink")，把[中國民間故事集合於一處](../Page/中國.md "wikilink")，包括[西遊記](../Page/西遊記.md "wikilink")、[八仙大鬧龍宮](../Page/八仙大鬧龍宮.md "wikilink")、[白蛇傳等人物](../Page/白蛇傳.md "wikilink")。在1986年，它由「國際主題公園（新加坡）公司」奪得，並耗資[新币](../Page/新币.md "wikilink")8,000萬改建，面積再擴大一倍，在1990年重新收費開放，成人门票收费新币16元5角。在1995年，又用了新币600萬元進行局部修整。在1997年，成人门票收费被調低至新币11元5角，希望刺激人流。

根據[新加坡旅遊局的資料](../Page/新加坡旅遊局.md "wikilink")，在1997年，9,400多名[觀光客之中](../Page/觀光客.md "wikilink")，有10.1%參觀了「虎豹別墅」，成為四大觀光熱點之一。可惜在1998年，排名降至第六。在1998年底錄得新币3,150萬元的虧損，並在1999年3月結束營業。主權退還[新加坡旅遊局](../Page/新加坡旅遊局.md "wikilink")。現時，再度每天免費開放。

## 基本資料

  - 開放時間：每日上午9:00至下午7:00
  - 入場費：收費
  - 地址：新加坡巴西班讓路262號
  - 郵區：118628

## 公共交通

  - [新加坡](../Page/新加坡.md "wikilink")
      - [環線](../Page/新加坡地鐵環線.md "wikilink")：[虎豹別墅站](../Page/虎豹別墅地鐵站.md "wikilink")

## 鄰近觀光熱點

  - [肯特崗公園](../Page/肯特崗公園.md "wikilink")
  - [西海岸公園](../Page/西海岸公園.md "wikilink")
  - [新加坡國立大學](../Page/新加坡國立大學.md "wikilink")

## 參見

  - [新加坡旅遊景點](../Page/新加坡旅遊景點.md "wikilink")
  - [虎豹別墅 (香港)](../Page/虎豹別墅_\(香港\).md "wikilink")
  - [虎豹別墅 (福建)](../Page/虎豹別墅_\(福建\).md "wikilink")
  - [虎豹兒童遊樂場](../Page/虎豹兒童遊樂場.md "wikilink")：位於[泰國](../Page/泰國.md "wikilink")[首都](../Page/首都.md "wikilink")[曼谷之](../Page/曼谷.md "wikilink")[是樂園內](../Page/是樂園.md "wikilink")

## 參考書籍

  - Tiger Balm Gardens（虎豹花園）
      - 出版商：Aw Boon Haw Foundation
      - [作者](../Page/作者.md "wikilink")：Judith Brandel及Tina Turbeville
      - [國際書號](../Page/國際書號.md "wikilink")：ISBN 962-672-052-2

[Category:新加坡旅遊景點](../Category/新加坡旅遊景點.md "wikilink")