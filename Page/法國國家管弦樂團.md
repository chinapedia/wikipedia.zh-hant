**法国国家管弦乐团**（）是[法國廣播電台旗下的兩個管弦乐团之一](../Page/法國廣播電台.md "wikilink")（另一個是[巴黎歌劇院管弦樂團](../Page/巴黎歌劇院.md "wikilink")）。該團在1934年1月18日成立，初名**國家管弦樂團**（Orchestre
national），1945年改名為**法國廣播國家管弦樂團**（Orchestre National de la Radiodiffusion
Française），1949年改名為**法國廣電國家管弦樂團**（Orchestre national de la
Radio-télévision française），1964年改名為**法國廣電總局國家管弦樂團**（Orchestre
national de l'Office de radiodiffusion-télévision
française）。1974年法國廣電總局被裁撤，樂團因而改用現名。

自1944年起，法国国家管弦乐团便將[巴黎](../Page/巴黎.md "wikilink")[香榭丽舍剧院作為樂團基地](../Page/香榭丽舍剧院.md "wikilink")，並在該劇院排演了不少歌劇。有時樂團也會移師法國廣播電台大廈的奧利佛·麥西恩演奏廳舉辦音樂會。而所有樂團的音樂會都由樂團的母機構——法國廣播電台負責錄音。

## 歷任音樂總監

| 圖片                                                                                        | 音樂總監                                                                              | 任期始於  | 任期終於  |
| ----------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------- | ----- | ----- |
|                                                                                           |                                                                                   |       |       |
|                                                                                           | [德斯瑞-艾米·安格爾布雷什特](../Page/德斯瑞-艾米·安格爾布雷什特.md "wikilink") Désiré-Emile Inghelbrecht  | 1934年 | ？     |
|                                                                                           | [曼紐爾·羅森塔](../Page/曼紐爾·羅森塔.md "wikilink") Manuel Rosenthal                         | 1944年 | 1947年 |
|                                                                                           | [羅傑·德索米耶](../Page/羅傑·德索米耶.md "wikilink") Roger Désormière                         | 1947年 | 1951年 |
|                                                                                           | [安德列·克路易坦](../Page/安德列·克路易坦.md "wikilink") André Cluytens                         | 1951年 | 1960年 |
|                                                                                           | [莫里斯·勒·魯](../Page/莫里斯·勒·魯.md "wikilink") Maurice Le Roux                          | 1960年 | 1967年 |
| [居中](https://zh.wikipedia.org/wiki/File:Charles_Munch.jpg "fig:居中")                       | [夏爾·孟許](../Page/查理·明希.md "wikilink") [Charles Münch](../Page/查理·明希.md "wikilink") | 1962年 | 1968年 |
|                                                                                           | [讓·馬提農](../Page/讓·馬提農.md "wikilink") Jean Martinon                                | 1968年 | 1973年 |
|                                                                                           | [谢尔盖·切利比达奇](../Page/谢尔盖·切利比达奇.md "wikilink") Sergiu Celibidache                   | 1973年 | 1975年 |
| [Maazel_08.jpg](https://zh.wikipedia.org/wiki/File:Maazel_08.jpg "fig:Maazel_08.jpg")    | [洛林·马泽尔](../Page/洛林·马泽尔.md "wikilink") Lorin Maazel                               | 1987年 | 1991年 |
|                                                                                           | [夏尔·迪图瓦](../Page/夏尔·迪图瓦.md "wikilink") Charles Dutoit                             | 1991年 | 2002年 |
| [Kurt_masur.jpg](https://zh.wikipedia.org/wiki/File:Kurt_masur.jpg "fig:Kurt_masur.jpg") | [库尔特·马苏尔](../Page/库尔特·马苏尔.md "wikilink") Kurt Masur                               | 2002年 | 2008  |

## 資料來源

  - [官方網站歷史簡介](https://web.archive.org/web/20080616031357/http://www.radiofrance.fr/chaines/orchestres/national/historique/)

## 外部連結

  -
[Category:法國管弦樂團](../Category/法國管弦樂團.md "wikilink")
[Category:巴黎文化](../Category/巴黎文化.md "wikilink")
[Category:巴黎組織](../Category/巴黎組織.md "wikilink")