**马切洛·马斯楚安尼**（，），[義大利](../Page/義大利.md "wikilink")[國寶級演員](../Page/國寶.md "wikilink")、[威尼斯影展藝術電影展](../Page/威尼斯影展.md "wikilink")[影帝](../Page/影帝.md "wikilink")，1949年起在電影演出，曾經以《義大利式離婚》、《特別的一天》三次入圍[奧斯卡金像獎最佳男主角卻未得獎](../Page/奧斯卡金像獎.md "wikilink")；前妻是法國著名女影星[凱撒琳·丹尼芙](../Page/凱撒琳·丹尼芙.md "wikilink")，兩人女兒現仍是歐洲影星；马斯楚安尼以他溫文儒雅微笑形象及演技，讓一世代影迷印象深刻，與義大利大導演[費里尼搭擋演出尤為人所稱道](../Page/費里尼.md "wikilink")；1996年[癌症逝世](../Page/癌症.md "wikilink")。

## 生平

### 早年

  - [羅馬大學建築學系夜間部畢業](../Page/羅馬大學.md "wikilink")；在學時期即參與[劇團](../Page/劇團.md "wikilink")[舞台劇演出](../Page/舞台劇.md "wikilink")。
  - **「[拉丁情人](../Page/拉丁.md "wikilink")」封號**：與妻子[弗羅拉·卡拉貝拉](../Page/弗羅拉·卡拉貝拉.md "wikilink")（Flora
    Carabella）維持45年婚姻關係，同在1996年故世（妻早數月先逝）；與法國影星[凱撒琳·丹尼芙未婚生一女](../Page/凱撒琳·丹尼芙.md "wikilink")，當馬斯楚安尼病逝家宅時，[凱撒琳·丹尼芙與女兒隨侍在側](../Page/凱撒琳·丹尼芙.md "wikilink")。
  - 一生演出電影120部，與[蘇菲亞·羅蘭演出](../Page/蘇菲亞·羅蘭.md "wikilink")10部最多；蘇菲亞‧羅蘭回憶時笑稱，差點被馬斯楚安尼的[魅力征服](../Page/魅力.md "wikilink")，當成他女友。

## 影片年表

## 參見

  -
  - [Obituary,
    CNN](http://www.cnn.com/SHOWBIZ/9612/19/mastroianni.obit/)

  - Chris Fujiwara, ["Dream lover: Marcello Mastroianni at the
    MFA"](https://web.archive.org/web/20160303232323/http://www.bostonphoenix.com/archive/movies/99/09/02/marcello_mastroianni.html)

  - [Marcello Mastroianni's
    Gravesite](http://www.findagrave.com/cgi-bin/fg.cgi?page=gr&GRid=7180)

[Category:罗马大学校友](../Category/罗马大学校友.md "wikilink")
[Category:義大利演員](../Category/義大利演員.md "wikilink")
[Category:意大利电影演员](../Category/意大利电影演员.md "wikilink")
[Category:文藝片演員](../Category/文藝片演員.md "wikilink")
[Category:英国电影学院奖最佳男主角获得者](../Category/英国电影学院奖最佳男主角获得者.md "wikilink")
[Category:坎城影展獲獎者](../Category/坎城影展獲獎者.md "wikilink")
[Category:凱撒電影獎獲得者](../Category/凱撒電影獎獲得者.md "wikilink")
[Category:金球奖最佳音乐或喜剧男主角奖得主](../Category/金球奖最佳音乐或喜剧男主角奖得主.md "wikilink")
[Category:威尼斯影展獲獎者](../Category/威尼斯影展獲獎者.md "wikilink")
[Category:罹患胰腺癌逝世者](../Category/罹患胰腺癌逝世者.md "wikilink")
[Category:20世纪演员](../Category/20世纪演员.md "wikilink")