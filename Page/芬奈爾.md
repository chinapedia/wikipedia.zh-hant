**費德利克．芬奈爾**（[英文](../Page/英文.md "wikilink")：**Frederick Fennell** /
**Frederick Putnam Fennell**，1914年7月2日 -
2004年12月7日），是一位國際級指揮家，並且是建立[管樂團為演奏團隊的先驅](../Page/管樂團.md "wikilink")。他對[美國本土及海外的樂團教學](../Page/美國.md "wikilink")，以及音樂教育有著深厚的影響。

## 生平

### 早年生活

**芬奈爾**生於[美國](../Page/美國.md "wikilink")[俄亥俄州](../Page/俄亥俄州.md "wikilink")[克里夫蘭市](../Page/克里夫蘭_\(俄亥俄州\).md "wikilink")\[1\]。他在7歲時選擇打擊樂器為他的第一種學習的樂器，在一個稱作[康寶·澤克](../Page/康寶·澤克.md "wikilink")（Camp
Zeke）的家族營隊（Family's
encampment）裏的鼓號樂隊中擔任鼓手。他在10歲時擁有自己的第一套[爵士鼓](../Page/爵士鼓.md "wikilink")。在[約翰·亞當斯高中](../Page/約翰·亞當斯高中.md "wikilink")（[John
Adams High
School](../Page/:en:John_Adams_High_School.md "wikilink")）[管弦樂團](../Page/管弦樂團.md "wikilink")，芬奈爾演奏[定音鼓並擔任](../Page/定音鼓.md "wikilink")[指揮](../Page/指揮.md "wikilink")。

他在[英特洛肯藝術營](../Page/英特洛肯藝術營.md "wikilink")（[Interlochen Arts
Camp](../Page/:en:Interlochen_Arts_Camp.md "wikilink")）裡學習。在1931年被著名的樂團指揮[阿爾伯特·奧斯汀·哈丁](../Page/阿爾伯特·奧斯汀·哈丁.md "wikilink")（[Albert
Austin
Harding](../Page/:en:Albert_Austin_Harding.md "wikilink")）取錄為[國家高中管樂團](../Page/國家高中管樂團.md "wikilink")（National
High School
Band）選任大鼓演奏，這個樂團在當年7月26日被[蘇沙](../Page/约翰·菲利普·苏萨.md "wikilink")（John
Philip Sousa）所指揮，並且首演了蘇沙的《北松進行曲》（Northern Pines
March）。芬奈爾在17歲時開始於[英特洛肯](../Page/英特洛肯.md "wikilink")（[Interlochen](../Page/:en:Interlochen.md "wikilink")）開始指揮工作。

芬奈爾在[伊士曼音樂學院分別在](../Page/伊士曼音樂學院.md "wikilink")1937年與1939年先後完成了學士以及碩士學位，成為了第一位以打擊樂器演奏的完成課程的畢業生。芬奈爾在[伊士曼音樂學院有一件相當重要的創舉](../Page/伊士曼音樂學院.md "wikilink")。他在學生時代為[羅徹斯特大學](../Page/羅徹斯特大學.md "wikilink")（[University
of
Rochester](../Page/:en:University_of_Rochester.md "wikilink")）的[欖球隊組職了一隻](../Page/橄欖球.md "wikilink")[步操樂隊](../Page/步操樂隊.md "wikilink")，並且在橄欖球賽非球季時舉辦室內音樂會，長達10年。

1938年，他獲得獎學金，讓他可以在[莫扎特薩爾茨堡大學](../Page/莫扎特薩爾茨堡大學.md "wikilink")（[Mozarteum
Salzburg](../Page/:en:Mozarteum_Salzburg.md "wikilink")）學習。這讓他可以上到[福特萬格勒](../Page/福特萬格勒.md "wikilink")（[Wilhelm
Furtwängler](../Page/:en:Wilhelm_Furtwängler.md "wikilink")）的課程。1942年，芬奈爾在[坦格活德](../Page/坦格活德.md "wikilink")（[Tanglewood](../Page/:en:Tanglewood.md "wikilink")）的[伯克郡音樂中心](../Page/伯克郡音樂中心.md "wikilink")（[Berkshire
Music
Center](../Page/:en:Berkshire_Music_Center.md "wikilink")）當隨當時[波士頓交響樂團的音樂總監](../Page/波士頓交響樂團.md "wikilink")[謝爾蓋·高塞夫維斯基](../Page/謝爾蓋·高塞夫維斯基.md "wikilink")（[Serge
Koussevitzky](../Page/:en:Serge_Koussevitzky.md "wikilink")）學習指揮，當時的同學有[伯恩斯坦](../Page/伯恩斯坦.md "wikilink")（[Leonard
Bernstein](../Page/:en:Leonard_Bernstein.md "wikilink")）、[盧卡斯·福斯](../Page/盧卡斯·福斯.md "wikilink")（[Lukas
Foss](../Page/:en:Lukas_Foss.md "wikilink")）、以及[沃爾特·漢杜](../Page/沃爾特·漢杜.md "wikilink")（[Walter
Hendl](../Page/:en:Walter_Hendl.md "wikilink")）。
1948年，他被中心指定為[謝爾蓋·高塞夫維斯基](../Page/謝爾蓋·高塞夫維斯基.md "wikilink")（[Serge
Koussevitzky](../Page/:en:Serge_Koussevitzky.md "wikilink")）的助理。第二次世界大戰期間，芬奈爾在[聯合服務組織](../Page/聯合服務組織.md "wikilink")（[United
Service
Organizations](../Page/:en:United_Service_Organizations.md "wikilink")）的[國家音樂諮詢者](../Page/國家音樂諮詢者.md "wikilink")（National
Musical Advisor）工作。

### [伊士曼管樂合奏團](../Page/伊士曼管樂合奏團.md "wikilink")

芬奈爾在1952年\[2\]從[肝炎康復](../Page/肝炎.md "wikilink")6個星期後，他想出一個新的合奏模式，這就是將典型的[管樂團編制轉換到](../Page/管樂團.md "wikilink")[交響樂團的](../Page/交響樂團.md "wikilink")[管樂編制](../Page/管樂.md "wikilink")。芬奈爾在1952年5月的集合了將近40位演奏者。芬奈爾解釋說：「我選擇學校中最好的學生、最好的獨奏者、還有最好的管樂合奏團演奏者（I
chose the best students in the school, and the best solo performers, and
the best ensemble players）。」

在1952年9月20日他舉行了[伊士曼管樂合奏團的第一次排練](../Page/伊士曼管樂合奏團.md "wikilink")，在1953年2月8日第一次指揮合奏團於[伊士曼音樂学院的](../Page/伊士曼音樂学院.md "wikilink")[基爾本恩音樂廳](../Page/基爾本恩音樂廳.md "wikilink")（Kilbourn
Hall）演出。為了獲得更多演出曲目，芬奈爾寫信給了全世界將近400位作曲家，邀請他們為這個新的合奏團作曲。第一位回應的作曲家是[澳洲的](../Page/澳洲.md "wikilink")[珀西·格蘭傑](../Page/珀西·格兰杰.md "wikilink")（Percy
Grainger），接下來是[美國的](../Page/美國.md "wikilink")[文森特·佩斯捷提](../Page/文森特·佩斯捷提.md "wikilink")（[Vincent
Persichetti](../Page/:en:Vincent_Persichetti.md "wikilink")）以及[英國的](../Page/英國.md "wikilink")[佛漢·威廉斯](../Page/拉尔夫·沃恩·威廉斯.md "wikilink")（Ralph
Vaughan Williams）。

### 錄音

不管是伊士曼管樂合奏團、[東京佼成管樂團](../Page/東京佼成管樂團.md "wikilink")、還是其它樂團，芬奈爾錄製了相當多的管樂曲示範專輯。他成為了美國最多錄音的指揮。

芬奈爾在1978年4月4日至4月5日於[美國跟](../Page/美國.md "wikilink")[得勒克唱片](../Page/得勒克唱片.md "wikilink")（[Telarc](../Page/:en:Telarc.md "wikilink")）及[克里夫蘭管樂團](../Page/克里夫蘭管樂團.md "wikilink")（Cleveland
Symphonic
Winds）錄製第一個交響數碼錄音\[3\]，樂曲包括[霍爾斯特](../Page/古斯塔夫·霍尔斯特.md "wikilink")（Gustav
Holst）的第一及第二軍樂隊組曲（First and Second Suites for Military Band）。

### 後伊士曼時期

芬奈爾博士在1962年至1964年於[明尼阿波利斯交響樂團](../Page/明尼阿波利斯交響樂團.md "wikilink")（Minneapolis
Symphony
Orchestra）（現在的[明尼蘇達管弦樂團](../Page/明尼蘇達管弦樂團.md "wikilink")）擔任音樂總監。1965年9月，他成為[邁阿密大學](../Page/邁阿密大學_\(佛罗里达州\).md "wikilink")[交響樂團指揮](../Page/交響樂團.md "wikilink")，他也在那裡建立[管樂團](../Page/管樂團.md "wikilink")。1974年至1975年他擔任[邁阿密愛樂樂團](../Page/邁阿密愛樂樂團.md "wikilink")（Miami
Philharmonic）的指揮。他也擔任[英特洛肯藝術學院](../Page/英特洛肯藝術學院.md "wikilink")（Interlochen
Arts
Academy）以及[達拉斯管樂團的客席指揮](../Page/達拉斯管樂團.md "wikilink")。1984年在[東京佼成管樂團團員邀請下](../Page/東京佼成管樂團.md "wikilink")，擔任了[東京佼成管樂團的指揮](../Page/東京佼成管樂團.md "wikilink")\[4\]，在1996年被該團委任為桂冠指揮。

### 去世

芬奈爾於2004年12月7日[佛羅里達州](../Page/佛羅里達州.md "wikilink")[斯爾斯達·基爾](../Page/斯爾斯達·基爾.md "wikilink")（[Siesta
Key](../Page/:en:Siesta_Key.md "wikilink")）家中去世，終年90歲\[5\]。

### 獎項及榮譽

芬奈爾於1988年由[伊士曼音樂學院接受了名譽博士學位](../Page/伊士曼音樂學院.md "wikilink")，並於1990年入選了[國家管樂協會傑出指揮名人堂](../Page/國家管樂協會傑出指揮名人堂.md "wikilink")
（[National Band Association Hall of Fame of Distinguished Band
Conductors](../Page/:en:National_Band_Association_Hall_of_Fame_of_Distinguished_Band_Conductors.md "wikilink")）。芬奈爾亦加入了[披·慕·阿爾法交響兄弟會](../Page/披·慕·阿爾法交響兄弟會.md "wikilink")（[Phi
Mu Alpha Sinfonia](../Page/:en:Phi_Mu_Alpha_Sinfonia.md "wikilink")）。
2006年4月4日，[英特洛肯藝術中心](../Page/英特洛肯藝術中心.md "wikilink")（[Interlochen Arts
Centre](../Page/:en:Interlochen_Arts_Centre.md "wikilink")）音樂藝術和學術圖書館開幕，圖書館以芬奈爾和他的妻子（Elizabeth
Ludwig Fennell）的名字命名，以作記念 \[6\]。

## 參考文獻

## 外部連結

Roger E. Rickson (1993) *Ffortissimo: a Bio-Discography of Frederick
Fennell: the First Forty Years, 1953 to 1993*, (Ludwig Music, Inc.,
publisher) ISBN 1-57134-000-9

Robert Simon (2004) book *A Tribute to Frederick Fennell* (GIA
Publications) ISBN 1-57999-472-5.

[Category:指揮家](../Category/指揮家.md "wikilink")
[Category:美國指揮家](../Category/美國指揮家.md "wikilink")
[Category:伊士曼音乐学院校友](../Category/伊士曼音乐学院校友.md "wikilink")

1.  [Frederick Fennell | Biography, Orchestras | Meredith
    Music:](http://www.meredithmusic.com/frederick-fennell-biography)
2.  [Frederick
    Fennell:](http://www.bh2000.net/special/patzak/detail.php?id=2555)
3.
4.  [Tokyo Kosei Wind
    Orchestra:](http://www.tkwo.jp/english/profile/laureate/)
5.  [Phi Mu Alpha Sinfonia - Sinfonia News: - FREDERICK FENNELL,
    SINFONIA'S 2003 MAN OF MUSIC, PASSES AWAY AT AGE
    90](http://www.sinfonia.org/News/SN-2004-12-07.asp)
6.  [Libraries | Interlochen Center for the
    Arts:](http://www.interlochen.org/libraries)