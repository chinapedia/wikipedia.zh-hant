**齊伯尼·查馬修瓦斯基**（，）是波蘭著名男演員。

查馬修瓦斯基畢業於[羅茲電影學院](../Page/羅茲電影學院.md "wikilink")，於1981年開始其演員事業，在1989年參與演出[克里斯多夫·奇士勞斯基的影片系列](../Page/克里斯多夫·奇士勞斯基.md "wikilink")：[十誡中的第十部份](../Page/十誡.md "wikilink")*(Thou
Shalt Not Covet Thy Neighbor's Goods)*。四年後，在奇士勞斯基的三色之白中擔綱演出主角卡洛卡洛（Karol
Karol）。

1985至1997年間，他在 The Studio Theater
中演出，而從1997年起他則定期在[華沙的國家戲劇院演出](../Page/華沙.md "wikilink")。

## 參與影片

  - *[Wróżby kumaka](../Page/Wróżby_kumaka.md "wikilink")* (2005)
    (post-production) aka *The Call of the Toad*
  - *[Zróbmy sobie wnuka](../Page/Zróbmy_sobie_wnuka.md "wikilink")*
    (2003) as Gustaw Mytnik
  - *[Żurek](../Page/Żurek.md "wikilink")* (2003) as Matuszek
  - *[Ciało](../Page/Ciało.md "wikilink")* (2003) as Dizel
  - *[Petite prairie aux bouleaux,
    La](../Page/Petite_prairie_aux_bouleaux,_La.md "wikilink")* (2003)
    aka *The Birch-Tree Meadow* as Gutek
  - *[曙光乍洩](../Page/曙光乍洩.md "wikilink")* (2003)（Distant
    Lights，德文片名：*Lichter*）飾演 Antoni。導演：
    [漢斯·史密德](../Page/漢斯·史密德.md "wikilink")（Hans-Christian
    Schmid）
  - *[Zmruż oczy](../Page/Zmruż_oczy.md "wikilink")* (2002) aka *Squint
    Your Eyes* as Jasiek
  - *[戰地琴人](../Page/戰地琴人.md "wikilink")* (2002) as Customer with Coins
  - *[屠龍傳奇](../Page/屠龍傳奇.md "wikilink")* (2001)（Wiedźmin，英譯：The Hexer）飾演
    Jaskier
  - *[Stacja](../Page/Stacja.md "wikilink")* (2001) aka *Station* as
    Dymecki, owner of petrol station
  - *[Cześć Tereska](../Page/Cześć_Tereska.md "wikilink")* (2001) aka
    *Hi, Tereska* as Edek
  - *[Sto minut wakacji](../Page/Sto_minut_wakacji.md "wikilink")*
    (2001) (TV)
  - *[Lightmaker](../Page/Lightmaker.md "wikilink")* (2001) as Rumo
    Ranieri
  - *[Weiser (film)](../Page/Weiser_\(film\).md "wikilink")* (2001) as
    Kołota
  - *[千驚萬險](../Page/千驚萬險.md "wikilink")* (2000) (Proof Of Life) as
    Terry's Driver
  - *[Når nettene blir
    lange](../Page/Når_nettene_blir_lange.md "wikilink")* (2000) aka
    *Cabin Fever* as Brother-in-law
  - *[Prymas - trzy lata z
    tysiąca](../Page/Prymas_-_trzy_lata_z_tysiąca.md "wikilink")*
    (2000) aka *The Primate* as Priest Stanisław Skorodecki
  - *[Pierwszy milion](../Page/Pierwszy_milion.md "wikilink")* (2000) as
    Policeman
  - *[火與劍](../Page/火與劍.md "wikilink")* (1999) （Ogniem i mieczem，英譯：*With
    Fire and Sword*）飾演 Michał Wołodyjowski
  - *[死亡密碼23](../Page/死亡密碼23.md "wikilink")* (1998) （23） as Sergej
  - *[Kochaj i rób co
    chcesz](../Page/Kochaj_i_rób_co_chcesz.md "wikilink")* (1998) aka
    *Love Me and Do Whatever You Want* as Lech Ryszka
  - *[Demony wojny według
    Goi](../Page/Demony_wojny_według_Goi.md "wikilink")* (1998) as Cpl.
    'Houdini' Moraczewski
  - *[Pułapka](../Page/Pułapka.md "wikilink")* (1997)
  - *[Szczęśliwego Nowego
    Jorku](../Page/Szczęśliwego_Nowego_Jorku.md "wikilink")* (1997)
    aka *Happy New York* as Potejto
  - *[Odwiedz mnie we snie](../Page/Odwiedz_mnie_we_snie.md "wikilink")*
    (1997)
  - *[Darmozjad polski](../Page/Darmozjad_polski.md "wikilink")* (1997)
    as Swede
  - *[Sława i chwała](../Page/Sława_i_chwała.md "wikilink")* (1997) (TV)
    as Franciszek Gołąbek
  - *[Pestka](../Page/Poznański_Szybki_Tramwaj.md "wikilink")* aka The
    Pip (1996)
  - *[Pułkownik
    Kwiatkowski](../Page/Pułkownik_Kwiatkowski.md "wikilink")* (1995)
    aka Colonel Kwiatkowski as Dudek
  - *[三色之白](../Page/三色之白.md "wikilink")* (1994) aka *Three Colours:
    White* as Karol Karol
  - *[Clandestin, Le](../Page/Clandestin,_Le.md "wikilink")* (1994) (TV)
    as Yatsek
  - *[Zawrócony](../Page/Zawrócony.md "wikilink")* (1994) (TV) aka
    *Reverted* as Tomek Siwek
  - *[Trois couleurs: Blanc](../Page/Three_Colors:_White.md "wikilink")*
    (1993) aka *Three Colours: White* as Karol Karol
  - *[Trois couleurs: Bleu](../Page/Three_Colors:_Blue.md "wikilink")*
    (1993) aka *Three Colors: Blue* as Karol Karol
  - *[Straszny sen dzidziusia
    Górkiewicza](../Page/Straszny_sen_dzidziusia_Górkiewicza.md "wikilink")*
    (1993) aka *The Terrible Dream of Babyface Gorkiewicz*
  - *[Sauna](../Page/Sauna.md "wikilink")* (1992) (TV) as Jussi
  - *[Naprawdę krótki film o miłości, zabijaniu i jeszcze jednym
    przykazaniu](../Page/Naprawdę_krótki_film_o_miłości,_zabijaniu_i_jeszcze_jednym_przykazaniu.md "wikilink")*
    (1992)
  - *[Tak, tak](../Page/Tak,_tak.md "wikilink")* (1992) aka *Yes, Yes*
  - *[Ferdydurke](../Page/Ferdydurke.md "wikilink")* (1991) aka *30 Door
    Key* as Tom
  - *[Seszele](../Page/Seszele.md "wikilink")* (1991) aka *Seychelles*
    as Stefek
  - *[Ucieczka z kina
    'Wolność'](../Page/Ucieczka_z_kina_'Wolność'.md "wikilink")*
    (1991) aka *Escape From the 'Liberty' Cinema* as Pomocnik cenzora
  - *[Korczak](../Page/Korczak_\(film\).md "wikilink")* (1990) as Ichak
    Szulc
  - *[《十诫》](../Page/十诫_\(电视剧\).md "wikilink")* (1989) (TV)
    十诫之十：*毋贪他人财物。* 饰演Artur
  - *[Sztuka kochania](../Page/Sztuka_kochania.md "wikilink")* (1989)
    aka *Art of Loving* as Ziobro
  - *[Zabij mnie, glino](../Page/Zabij_mnie,_glino.md "wikilink")*
    (1989) aka *Kill Me, Cop* as Seweryn
  - *[Possédés, Les](../Page/Possédés,_Les.md "wikilink")* (1988) aka
    *The Possessed* as Liamchine
  - *[Prywatne śledztwo](../Page/Prywatne_śledztwo.md "wikilink")*
    (1987) aka *Private Investigation* as Truck passenger
  - *[Pierścień i róża](../Page/Pierścień_i_róża.md "wikilink")* (1987)
    aka *The Ring and the Rose* as Prince Bulbo
  - *[Ucieczka](../Page/Ucieczka.md "wikilink")* (1986)
  - *[Matka Królów](../Page/Matka_Królów.md "wikilink")* (1983) aka
    *Mother of Kings*
  - *[Wielka majówka](../Page/Wielka_majówka.md "wikilink")* (1981) aka
    *The Big Picnic*

## 外部連結

  -
[Category:波蘭演員](../Category/波蘭演員.md "wikilink")