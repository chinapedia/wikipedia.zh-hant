**迈克尔·菲利普·贾格尔**爵士（Sir Michael Phillip "Mick"
Jagger，），[英国](../Page/英国.md "wikilink")[摇滚乐手](../Page/摇滚乐.md "wikilink")，[滚石樂團创始成员之一](../Page/滚石樂團.md "wikilink")，1962年开始担任樂團主唱至今，并演奏口琴、吉他和钢琴。作为演员、制片人和作曲人参与过多部电影的制作。艺名为“米克”·贾格尔。

贾格尔出生于[肯特郡的一个](../Page/肯特郡.md "wikilink")[中产阶级](../Page/中产阶级.md "wikilink")[家庭](../Page/家庭.md "wikilink")。

贾格尔的职业生涯横跨 50
年，被称为“摇滚史上最受欢迎和最有影响力的主唱之一”。\[1\]他独特的嗓音和舞台动作，与[基思·理查兹的吉他演奏风格一起成为了滚石樂團的标志](../Page/基思·理查兹.md "wikilink")。另外樂隊[魔力紅](../Page/魔力紅.md "wikilink")（Maroon
5）寫了一首《Moves like Jagger》表示對他的尊敬。

## 英語專輯

  - 《[She's the Boss](../Page/She's_the_Boss.md "wikilink")》(1985年2月25日)
  - 《[Primitive Cool](../Page/Primitive_Cool.md "wikilink")》(1987年9月14日)
  - 《[Wandering
    Spirit](../Page/Wandering_Spirit.md "wikilink")》(1993年2月8日)
  - 《[Goddess in the
    Doorway](../Page/Goddess_in_the_Doorway.md "wikilink")》(2001年11月19日)

## 参考文献

<references />

[J](../Category/英国歌手.md "wikilink")
[J](../Category/倫敦政治經濟學院校友.md "wikilink")
[J](../Category/下級勳位爵士.md "wikilink")
[Category:英格蘭歌曲作家](../Category/英格蘭歌曲作家.md "wikilink")

1.  Erlewine, Stephen Thomas. "Mick Jagger Biography". Allmusic.
    Retrieved 5 December 2010.