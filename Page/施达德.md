<table>
<tbody>
<tr class="odd">
<td><p>{{Infobox Person</p></td>
<td><p>name = 施达德<br />
Charles Thomas Studd</p></td>
<td><p>image = Charles Studd c1882.jpg</p></td>
<td><p>caption =</p></td>
<td><p>birth_date = 1860年12月2日</p></td>
<td><p>birth_place =  <a href="../Page/英国.md" title="wikilink">英国</a><a href="../Page/北安普敦郡.md" title="wikilink">北安普敦郡</a></p></td>
<td><p>death_date = 1931年7月16日</p></td>
<td><p>death_place =  <a href="../Page/比属刚果.md" title="wikilink">比属刚果Ibambi</a></p></td>
<td><p>occupation = 板球手、传教士</p></td>
<td><p>spouse = Priscilla Stewart Studd</p></td>
<td><p>parents = }}</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

**施达德**（），原名**查爾斯·托馬斯·施达德**（**Charles Thomas
Studd**）是[英国著名的板球手](../Page/英国.md "wikilink")，曾代表英国参加重要赛事；1885年改行担任传教士（[剑桥七杰之一](../Page/剑桥七杰.md "wikilink")），先后在[中国](../Page/中国.md "wikilink")、[印度和](../Page/印度.md "wikilink")[非洲传教](../Page/非洲.md "wikilink")。

## 生平

1860年12月2日，施达德出生在英国北安普敦郡的Spratton，是兄弟中最年幼的一个。父亲爱德华·施达德（Edward
Studd）因在印度栽种靛蓝染料而成为巨富。他退休回到英国以后，热衷于赌马，建立了一个庞大的赛马场。

施达德在16岁时表现出在[板球方面的天赋](../Page/板球.md "wikilink")，19岁时担任[伊顿公学的板球队长](../Page/伊顿公学.md "wikilink")；1877年毕业于伊顿公学，1881年进入[剑桥大学三一学院](../Page/剑桥大学三一学院.md "wikilink")\[1\]。他参加了世界上最负盛名的板球赛“灰烬杯”（The
Ashes），是一位出色的击球手。

1877年，由于朋友文生的介绍，施达德的父亲爱德华施达德到Drury
Lane剧院参加了[慕迪和山克](../Page/慕迪.md "wikilink")（Sankey）的布道会，而成为虔诚的基督徒，放弃了赛马事业，热心传福音。1878年暑假期间，爱德华施达德邀请传道人来到他们的庄园，通过与一位讲员私下的谈话，施达德得到[重生的经历](../Page/重生_\(基督教\).md "wikilink")，也如同父亲一样，成为一个“玩真的”基督徒\[2\]。

[Ashes_Urn.jpg](https://zh.wikipedia.org/wiki/File:Ashes_Urn.jpg "fig:Ashes_Urn.jpg")
1884年，施达德的哥哥乔治身患严重的疾病，这迫使施达德思考严肃的人生问题。于是他决定放弃板球，加入了[戴德生创立的](../Page/戴德生.md "wikilink")[中国内地会](../Page/中国内地会.md "wikilink")，在1885年2月，和另外6名剑桥大学毕业生（称为[剑桥七杰](../Page/剑桥七杰.md "wikilink")）——[章必成](../Page/章必成.md "wikilink")（Montague
Beauchamp）、[司安仁](../Page/司安仁.md "wikilink")（Stanley
Smith）、[宝耀庭](../Page/宝耀庭.md "wikilink")（Cecil H.
Polhill-Turner）、[亚瑟·端纳](../Page/亚瑟·端纳.md "wikilink")（A. T.
Polhill-Turner）、[何斯德](../Page/何斯德.md "wikilink")（Dixon Edward
Hoste）、[盖士利](../Page/盖士利.md "wikilink")（William W.
Cassels）一同前往中国传教。

1888年，施达德在中国[天津与来自爱尔兰的女传教士普莉西雅](../Page/天津.md "wikilink")·施德活（Priscilla
Stewart）结婚，。在婚礼之前，这位新娘要求施达德效法圣经中的少年财主，变卖一切财产。
施达德继承的遗产有29000英镑，其中5000英镑捐献给了美国芝加哥的[慕迪圣经学院](../Page/慕迪圣经学院.md "wikilink")，5000英镑捐献给了[乔治·慕勒在](../Page/乔治·慕勒.md "wikilink")[布里斯托尔所办的孤儿院](../Page/布里斯托尔.md "wikilink")，5000英镑捐献给了[救世军的将军](../Page/救世军.md "wikilink")，资助他们在印度的工作。

婚后生有4个女儿（Grace, Dorothy, Edith & Pauline)和2个儿子。

1931年7月16日，施达德在[比属刚果的Ibambi逝世](../Page/比属刚果.md "wikilink")。

[The_Studd_Brothers.jpg](https://zh.wikipedia.org/wiki/File:The_Studd_Brothers.jpg "fig:The_Studd_Brothers.jpg")

[Cambridge_Seven.jpg](https://zh.wikipedia.org/wiki/File:Cambridge_Seven.jpg "fig:Cambridge_Seven.jpg")

## 参考文献

<div class="references-small">

<references />

</div>

[S](../Category/英國基督徒.md "wikilink")
[S](../Category/内地会传教士.md "wikilink")

1.  [Charles
    Studd](http://content-www.cricinfo.com/england/content/player/20398.html)
2.  [施达德小传](http://www.clevelandonline.org/Chinese/Journal/200302/04/Studd1.htm)