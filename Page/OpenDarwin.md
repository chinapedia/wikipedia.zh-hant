**OpenDarwin**
是一種自由、多重平台以[XNU為內核的](../Page/XNU.md "wikilink")[類Unix系統](../Page/類Unix系統.md "wikilink")，其XNU內核採用[BSD](../Page/BSD.md "wikilink")
和[Mach 3.0](../Page/Mach_kernel.md "wikilink") 基礎。它有
[PowerPC](../Page/PowerPC.md "wikilink") 和
[IA-32](../Page/IA-32.md "wikilink") 架構的版本，最後的版本是 8.0.1。

在2002年4月成立，OpenDarwin 專案的目標，是建立一個獨立的
[Darwin](../Page/Apple_Darwin.md "wikilink") 作業系統分支，增加蘋果的開發者和
[開放原始碼](../Page/開放原始碼.md "wikilink")
社群的合作。[蘋果電腦](../Page/蘋果電腦.md "wikilink")
可從該專案獲得好處，因為 OpenDarwin 的發展也經常合併到 Darwin
的釋出；而開放原始碼設群也可得到好處，因為他們對於它所擁有的作業系統可獲得完整的控制。

OpenDarwin 的[吉祥物是](../Page/吉祥物.md "wikilink")
[鴨嘴獸](../Page/鴨嘴獸.md "wikilink")
[Hexley](../Page/Hexley.md "wikilink")。

在多重原因影響下，該項目已經於2006年7月终止\[1\]。2007年另一個[PureDarwin專案成立去接手OpenDarwin之前的目標](../Page/PureDarwin.md "wikilink")。

## 參考文獻

## 參見

  - [Apple Darwin](../Page/Apple_Darwin.md "wikilink")

## 外部連結

  - [OpenDarwin
    社群開發網站](https://web.archive.org/web/20060106092328/http://www.opendarwin.org/)
  - [DarwinPorts
    專案](https://web.archive.org/web/20031119010308/http://darwinports.opendarwin.org/)
  - [OpenDarwin 的 Bugzillla 資料庫](http://bugzilla.opendarwin.org)
  - [Hexley, Darwin 吉祥物](http://www.hexley.com/)
  - irc.freenode.net \#opendarwin
  - [PureDarwin](http://www.puredarwin.org/)

[de:OpenDarwin](../Page/de:OpenDarwin.md "wikilink") [en:Darwin
(operating
system)\#OpenDarwin](../Page/en:Darwin_\(operating_system\)#OpenDarwin.md "wikilink")

[Category:苹果操作系统](../Category/苹果操作系统.md "wikilink")
[Category:BSD](../Category/BSD.md "wikilink")
[Category:自由作業系統](../Category/自由作業系統.md "wikilink")
[Category:Mach](../Category/Mach.md "wikilink")

1.