[Beekman_Tower_fr_BB_jeh.jpg](https://zh.wikipedia.org/wiki/File:Beekman_Tower_fr_BB_jeh.jpg "fig:Beekman_Tower_fr_BB_jeh.jpg")\]\]
[Bay_Adelaide_first_tower_nearing_completion_-_cropped.jpg](https://zh.wikipedia.org/wiki/File:Bay_Adelaide_first_tower_nearing_completion_-_cropped.jpg "fig:Bay_Adelaide_first_tower_nearing_completion_-_cropped.jpg")起先在1980年採用現代建築設計，後來2009年完成時的轉變即是一例，大廈修改變成為後現代主義的風格\]\]
**新現代建築**是一種從20世紀末期到21世紀初的[建築風格](../Page/建築風格.md "wikilink")，最早在1965年有出現的蹤跡。新現代建築透過新的[簡約而](../Page/簡約主義.md "wikilink")[平民化的設計](../Page/平民主義.md "wikilink")\[1\]而對[後現代建築的複雜建築結構作出](../Page/後現代建築.md "wikilink")[折衷主義的回應](../Page/折衷主義.md "wikilink")。可以說是介於現代主義和後現代主義中間的微小地帶。\[2\]。

「新現代建築」這個名詞有時亦被用於汎指21世紀後建造的所有建築。

大致上21世紀後建築界设计上有两条发展的主脉络，其中一条是[后现代主义建築的探索](../Page/后现代主义建築.md "wikilink")，另外一条则是对[现代主义建築的重新研究和发展](../Page/现代主义建築.md "wikilink")，它们基本是并行发展的。第二條路的发展就被称为“新现代主义”，或者“新现代”设计。虽有不少设计師在70年代认为现代主义已经穷途末路，认为国际主义风格充满了与时代和當地文化不适应的成份，因此必须利用各种历史的、装饰的风格进行修正，从而引发了后现代主义运动，但有一些设计師却依然坚持现代主义的传统，完全依照现代主义的基本设计，他们根据新需要给现代主义加入了新的简单形式變化其象征意义，但从总体来说他们可以说是现代主义继续升級发展的后代。

这种依然以理性主义、功能主义、减少主义方式进行设计的新現代建筑家，虽然人数不多但影响力依然存在。

<File:Zuerich> Breitensteinstrasse
50.jpg|位於[瑞士](../Page/瑞士.md "wikilink")[蘇黎世的新現代主義建築](../Page/蘇黎世.md "wikilink")。
<File:Headquarter> of China Steel Corporation in Kaohsiung,
Taiwan-2.jpg|台灣[中鋼總部](../Page/中鋼.md "wikilink") <File:Giants> - Toronto
November 2010.jpg|[阿德萊迪灣大廈](../Page/阿德萊迪灣大廈.md "wikilink")
<File:Knights> of Columbus
headquarters.jpg|[紐哈芬市KofL總部](../Page/紐哈芬市.md "wikilink")

## 参考文献

### 引用

### 来源

  - 书籍

<!-- end list -->

  -
  -
## 參看

  - [新現代主義](../Page/新現代主義.md "wikilink") -
    [現代主義建築](../Page/現代主義建築.md "wikilink") -
    [簡約主義](../Page/簡約主義.md "wikilink")
  - [新城市運動](../Page/新城市運動.md "wikilink")
  - [再現代主義](../Page/再現代主義.md "wikilink")
  - [反概念藝術](../Page/反概念藝術.md "wikilink")（Stuckism）

## 外部連結

  - [Neomodern group
    manifesto](https://web.archive.org/web/20070930093518/http://guydenning.org/neomodern/a_new_critical_aesthetic.htm)
  - [Remodernist Manifesto](http://www.stuckism.com/remod.html)

[Category:20世纪建筑风格](../Category/20世纪建筑风格.md "wikilink")
[Category:现代主义](../Category/现代主义.md "wikilink")
[Category:藝術運動](../Category/藝術運動.md "wikilink")
[Category:後現代主義](../Category/後現代主義.md "wikilink")
[Category:后现代主义建筑](../Category/后现代主义建筑.md "wikilink")

1.
2.