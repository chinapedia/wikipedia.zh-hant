**围盘树科**又名[周位花盘科](../Page/周位花盘科.md "wikilink")、[巴西肉盘科或](../Page/巴西肉盘科.md "wikilink")[团花盘树科](../Page/团花盘树科.md "wikilink")，共有3[属](../Page/属.md "wikilink")9[种](../Page/种.md "wikilink")，其中两属为单种属，生长在[南美洲的](../Page/南美洲.md "wikilink")[热带地区](../Page/热带.md "wikilink")；*Soyauxia*
属生长在[非洲](../Page/非洲.md "wikilink")。\[1\]

本科[植物为](../Page/植物.md "wikilink")[乔木](../Page/乔木.md "wikilink")；单[叶互生](../Page/叶.md "wikilink")，叶大，革质，有托叶；[花两性](../Page/花.md "wikilink")，[花萼有毛](../Page/花萼.md "wikilink")，[花瓣](../Page/花瓣.md "wikilink")4-7；[果实为](../Page/果实.md "wikilink")[核果](../Page/核果.md "wikilink")。

## 属

  - *[Peridiscus](../Page/Peridiscus.md "wikilink")*
    [Benth.](../Page/Benth..md "wikilink")
  - *[Soyauxia](../Page/Soyauxia.md "wikilink")*
    [Oliv.](../Page/Oliv..md "wikilink")
  - *[Whittonia](../Page/Whittonia.md "wikilink")*
    [Sandwith](../Page/Sandwith.md "wikilink")

1981年的[克朗奎斯特分类法将其列在](../Page/克朗奎斯特分类法.md "wikilink")[堇菜目](../Page/堇菜目.md "wikilink")，1998年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
分类法认为其无法放在任何一个](../Page/APG_分类法.md "wikilink")[目中](../Page/目.md "wikilink")，属于系属不清的[科](../Page/科.md "wikilink")，2003年经过修订的[APG
II
分类法将其列在](../Page/APG_II_分类法.md "wikilink")[金虎尾目](../Page/金虎尾目.md "wikilink")。\[2\]
\[3\] \[4\]

## 参考文献

<references/>

### 外部链接

  - 在[L. Watson和M.J. Dallwitz
    (1992年)《有花植物分科》](https://web.archive.org/web/20070103200438/http://delta-intkey.com/angio/)中的[围盘树科](https://web.archive.org/web/20071017212935/http://delta-intkey.com/angio/www/peridisc.htm)
  - [NCBI中的围盘树科](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?lin=s&p=has_linkout&id=125048)

[\*](../Category/围盘树科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")

1.  Soltis, D. E.; Clayton, J. W.; Davis, C. C.; Gitzendanner, M. A.;
    Cheek, M.; Savolainen, V.; Amorim, A. M.; Soltis, P. S.
    (2007):Monophyly and relationships of the enigmatic family
    Peridiscaceae. *Taxon* 56(1):65-73.

2.
3.

4.