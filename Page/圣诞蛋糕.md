**聖誕蛋糕**（）是[英國](../Page/英國.md "wikilink")、[愛爾蘭和許多](../Page/愛爾蘭.md "wikilink")[英聯邦國家在](../Page/英聯邦.md "wikilink")[聖誕節時吃的](../Page/聖誕節.md "wikilink")[水果蛋糕](../Page/水果蛋糕.md "wikilink")。在[日本](../Page/日本.md "wikilink")，人們習慣在[平安夜吃聖誕蛋糕](../Page/平安夜.md "wikilink")，通常由一塊簡單[鬆糕](../Page/鬆糕.md "wikilink")，[糖霜與](../Page/糖霜_\(餐飲\).md "wikilink")[奶油構成](../Page/奶油.md "wikilink")，用[草莓和聖誕](../Page/草莓.md "wikilink")[巧克力或其它季節性](../Page/巧克力.md "wikilink")[水果](../Page/水果.md "wikilink")。

## 參見

  - [蛋糕列表](../Page/蛋糕列表.md "wikilink")

## 參考資料

[Category:蛋糕](../Category/蛋糕.md "wikilink")
[Category:聖誕節食品](../Category/聖誕節食品.md "wikilink")