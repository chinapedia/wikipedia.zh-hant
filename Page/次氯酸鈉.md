**次氯酸钠**（sodium
hypochlorite），化学式NaClO，是[钠的](../Page/钠.md "wikilink")[次氯酸盐](../Page/次氯酸盐.md "wikilink")。次氯酸钠与[二氧化碳反应产生的](../Page/二氧化碳.md "wikilink")[次氯酸是](../Page/次氯酸.md "wikilink")[漂白剂的有成分](../Page/漂白剂.md "wikilink")。

## 製備

[氯气溶於冷而且稀的](../Page/氯气.md "wikilink")[氫氧化鈉溶液產生次氯酸鈉](../Page/氫氧化鈉.md "wikilink")、[氯化鈉及](../Page/氯化鈉.md "wikilink")[水](../Page/水分子.md "wikilink")：

  -
    Cl<sub>2</sub> + 2NaOH → NaClO + NaCl + H<sub>2</sub>O

注意氯气溶於水，生成[盐酸和](../Page/盐酸.md "wikilink")[次氯酸](../Page/次氯酸.md "wikilink")。

  -
    Cl<sub>2</sub> + H<sub>2</sub>O → HClO + HCl

## 化学反应

次氯酸钠是强碱弱酸盐，会水解，水溶液呈碱性：

  -
    NaClO<sub>(aq)</sub> + H<sub>2</sub>O<sub>(l)</sub> →
    HClO<sub>(aq)</sub> + NaOH<sub>(aq)</sub>

次氯酸鈉水溶液[漂白釋出](../Page/漂白.md "wikilink")[氧後](../Page/氧.md "wikilink")，會變成[氯化鈉水溶液](../Page/氯化鈉.md "wikilink")，其反應式如下:

  -
    NaClO + H<sub>2</sub>O → NaOH + HClO
    2HClO → 2HCl + O<sub>2</sub>↑
    NaOH + HCl → NaCl +H<sub>2</sub>O

與[鹽酸反應會放出有毒的氯氣](../Page/鹽酸.md "wikilink")，因此使用相關製品時應注意避免混合：

  -
    NaClO + 2HCl<sub>(aq)</sub> → NaCl + H<sub>2</sub>O +
    Cl<sub>2</sub>↑

與[過氧化氫反應生成](../Page/過氧化氫.md "wikilink")[氧氣](../Page/氧氣.md "wikilink")：

  -
    NaClO + H<sub>2</sub>O<sub>2</sub> → H<sub>2</sub>O + NaCl +
    O<sub>2</sub>↑

### 分解

次氯酸钠在較熱的條件下易[歧化成氯化钠及氯酸钠](../Page/歧化.md "wikilink")：

  -
    3NaClO → 2NaCl + NaClO<sub>3(aq)</sub>

在水中，也會緩慢發生以下反應：

  -
    4NaClO + 2H<sub>2</sub>O → Na<sup>+</sup> + 4OH<sup>-</sup> +
    2Cl<sub>2</sub>↑ + O<sub>2</sub>↑

次氯酸根离子是极不稳定的[离子](../Page/离子.md "wikilink")，它会在光照下分解：

  -
    2ClO<sup>-</sup> → \>O<sub>2</sub>↑ + 2Cl<sup>-</sup>

以上三種分解均會發生，屬於[競爭反應](../Page/競爭反應.md "wikilink")。為延長保存期，次氯酸鈉應避光、低溫保存。

## 參考文獻

  -
  - Institut National de Recherche et de Sécurité. (2004). "Eaux et
    extraits de Javel. Hypochlorite de sodium en solution." *Fiche
    toxicologique n° 157,* Paris.

## 外部連結

  - [International Chemical Safety
    Card 0482](http://www.inchem.org/documents/icsc/icsc/eics0482.htm)
    (solutions\<10% active Cl)
  - [International Chemical Safety
    Card 1119](http://www.inchem.org/documents/icsc/icsc/eics1119.htm)
    (solutions \>10% active Cl)
  - [Institut national de recherche et de
    sécurité](https://web.archive.org/web/20100428080243/http://www.inrs.fr/fichetox/ft157.html)
    (*in French*)
  - [Home and Leisure Accident
    Statistics 2002](https://web.archive.org/web/20060112074931/http://www.hassandlass.org.uk/query/reports/2002data.pdf)
    (UK RoSPA)
  - [Emergency Disinfection of Drinking
    Water](http://www.epa.gov/safewater/faq/emerg.html) ([United States
    Environmental Protection Agency](../Page/美国国家环境保护局.md "wikilink"))
  - [Chlorinated Drinking
    Water](https://web.archive.org/web/20051012231045/http://www-cie.iarc.fr/htdocs/monographs/vol52/01-water.htm)
    ([IARC](../Page/International_Agency_for_Research_on_Cancer.md "wikilink")
    Monograph)
  - [NTP Study Report TR-392: Chlorinated & Chloraminated
    Water](http://ntp.niehs.nih.gov/ntp/htdocs/LT_rpts/tr392.pdf) (US
    [NIH](../Page/NIH.md "wikilink"))
  - [Guidelines for the Use of Chlorine Bleach as a Sanitizer in Food
    Processing
    Operations](http://pods.dasnr.okstate.edu/docushare/dsweb/Get/Document-963/FAPC-116web.pdf)
    (Oklahoma State University)

[Category:钠化合物](../Category/钠化合物.md "wikilink")
[Category:次氯酸盐](../Category/次氯酸盐.md "wikilink")
[Category:消毒剂](../Category/消毒剂.md "wikilink")
[Category:漂白剂](../Category/漂白剂.md "wikilink")
[Category:腐蝕性化學品](../Category/腐蝕性化學品.md "wikilink")