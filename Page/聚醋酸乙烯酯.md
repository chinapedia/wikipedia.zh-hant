**聚醋酸乙烯酯**（Polyvinyl
acetate，也称作**聚乙酸乙烯酯**，简称**PVA**、**PVAc**）是一种有[弹性的](../Page/橡胶.md "wikilink")[合成聚合物](../Page/合成聚合物.md "wikilink")。
聚醋酸乙烯酯是通过[醋酸乙烯酯](../Page/醋酸乙烯酯.md "wikilink")（VAM）的聚合而制备的。聚合物的部分或全部水解用于制备[聚乙烯醇](../Page/聚乙烯醇.md "wikilink")。聚乙烯醇產品的水解率一般在87%至99%之間。聚醋酸乙烯酯是[弗里茨·克拉特](../Page/弗里茨·克拉特.md "wikilink")1912年在[德国发现的](../Page/德国.md "wikilink")。

聚醋酸乙烯酯以在水中的[乳剂的形式](../Page/乳剂.md "wikilink")，作为多孔材料，特别是[木材的](../Page/木材.md "wikilink")[胶粘剂出售](../Page/胶粘剂.md "wikilink")。它是最常用的木材用胶，被称作**白胶水**（**白膠漿**）。白胶水也用广泛地用于粘合其他材料，如[纸和](../Page/纸.md "wikilink")[衣料以及将香烟粘合在一起](../Page/衣料.md "wikilink")。聚醋酸乙烯酯被广泛地应用于印刷装订和书籍艺术，这是由于它的弹性和酸性比其他聚合物的更小。[埃尔默胶是在](../Page/埃尔默胶.md "wikilink")[美国销售的白胶水的知名品牌](../Page/美国.md "wikilink")。

聚醋酸乙烯酯还被广泛地推荐用于制造[混凝纸](../Page/混凝纸.md "wikilink")。

<File:General> glue.jpg|白膠漿 <File:School> glue.JPG|Ross白膠漿 150毫升

## 外部链接

  - [Formula of
    PVA](https://web.archive.org/web/20070107125427/http://www.sas.org/E-Bulletin/2003-12-19/labNotesAS/art/1973-07-01.jpeg)

[Category:材料](../Category/材料.md "wikilink")
[Category:化学工业](../Category/化学工业.md "wikilink")
[Category:酯](../Category/酯.md "wikilink")
[Category:乙烯聚合物](../Category/乙烯聚合物.md "wikilink")
[Category:黏合剂](../Category/黏合剂.md "wikilink")