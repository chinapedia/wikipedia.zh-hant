**胡安·巴勃罗·索林**（，），[阿根廷足球員](../Page/阿根廷.md "wikilink")，司職後衛，2006年夏季轉投[德甲球隊](../Page/德甲.md "wikilink")[漢堡](../Page/汉堡体育俱乐部.md "wikilink")。

索林在球員生涯裡曾效力多國球會，除了國內球會外，還包括[巴西](../Page/巴西.md "wikilink")、[意大利](../Page/意大利.md "wikilink")、[法國和](../Page/法國.md "wikilink")[西班牙球會](../Page/西班牙.md "wikilink")。所效力球會計有[小阿根廷人](../Page/小阿根廷人.md "wikilink")(1994-95)、[祖雲達斯](../Page/祖雲達斯.md "wikilink")(1995-96）、[河床](../Page/河床足球會.md "wikilink")（1996-99）、[高士路](../Page/高士路.md "wikilink")（2000-01及2004）、[拉素](../Page/拉素足球俱樂部.md "wikilink")（2002）、[巴塞隆拿](../Page/巴塞隆拿.md "wikilink")（2003）及[巴黎圣日耳曼](../Page/巴黎圣日耳曼足球俱乐部.md "wikilink")（2003-04）。

索林最顯眼的特徵便是一頭飄逸的長髮，他在球場上專司左邊衛的位置，能守能攻，破壞力十足。索林曾代表[阿根廷國家足球隊超過](../Page/阿根廷.md "wikilink")50次，包括[2002年世界盃和](../Page/2002年世界盃.md "wikilink")[2006年世界杯](../Page/2006年世界杯.md "wikilink")，其中[2006年世界盃時](../Page/2006年世界盃.md "wikilink")，他擔任[阿根廷國家隊的隊長](../Page/阿根廷.md "wikilink")，可惜卻在柏林敗於[米夏埃爾·巴拉克腳下](../Page/米夏埃爾·巴拉克.md "wikilink")。

## 外部連結

  - [阿根廷足球總會網頁](http://www.afa.org.ar)

[Category:2006年世界盃足球賽球員](../Category/2006年世界盃足球賽球員.md "wikilink")
[Category:2005年洲際國家盃球員](../Category/2005年洲際國家盃球員.md "wikilink")
[Category:2004年美洲盃球員](../Category/2004年美洲盃球員.md "wikilink")
[Category:2002年世界盃足球賽球員](../Category/2002年世界盃足球賽球員.md "wikilink")
[Category:1999年美洲盃球員](../Category/1999年美洲盃球員.md "wikilink")
[Category:阿根廷足球運動員](../Category/阿根廷足球運動員.md "wikilink")
[Category:足球後衛](../Category/足球後衛.md "wikilink")
[Category:意甲球員](../Category/意甲球員.md "wikilink")
[Category:西甲球員](../Category/西甲球員.md "wikilink")
[Category:法甲球員](../Category/法甲球員.md "wikilink")
[Category:德甲球員](../Category/德甲球員.md "wikilink")
[Category:小阿根廷人球員](../Category/小阿根廷人球員.md "wikilink")
[Category:祖雲達斯球員](../Category/祖雲達斯球員.md "wikilink")
[Category:河床球員](../Category/河床球員.md "wikilink")
[Category:高士路球員](../Category/高士路球員.md "wikilink")
[Category:拉素球員](../Category/拉素球員.md "wikilink")
[Category:巴塞隆拿球員](../Category/巴塞隆拿球員.md "wikilink")
[Category:巴黎聖日門球員](../Category/巴黎聖日門球員.md "wikilink")
[Category:維拉利爾球員](../Category/維拉利爾球員.md "wikilink")
[Category:漢堡球員](../Category/漢堡球員.md "wikilink")
[Category:義大利外籍足球運動員](../Category/義大利外籍足球運動員.md "wikilink")
[Category:西班牙外籍足球運動員](../Category/西班牙外籍足球運動員.md "wikilink")
[Category:德國外籍足球運動員](../Category/德國外籍足球運動員.md "wikilink")
[Category:阿根廷猶太人](../Category/阿根廷猶太人.md "wikilink")
[Category:布宜諾斯艾利斯人](../Category/布宜諾斯艾利斯人.md "wikilink")