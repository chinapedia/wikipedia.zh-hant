**中国工农红军第五军团**，简称**红五军团**，[中国工农红军主力部队之一](../Page/中国工农红军.md "wikilink")。

## 简介

1931年12月，奉命进攻[中央苏区的](../Page/中央苏区.md "wikilink")[国民革命军第二十六路军](../Page/国民革命军第二十六路军.md "wikilink")（原[冯玉祥](../Page/冯玉祥.md "wikilink")[西北军一部](../Page/西北军.md "wikilink")）在[江西](../Page/江西.md "wikilink")[宁都发动](../Page/宁都.md "wikilink")[暴动](../Page/宁都暴动.md "wikilink")，加入红军，被改编为中国工农红军第五军团。军团总指挥[季振同](../Page/季振同.md "wikilink")、政治委员[萧劲光](../Page/萧劲光.md "wikilink")、副总指挥[董振堂](../Page/董振堂.md "wikilink")、参谋长[赵博生](../Page/赵博生.md "wikilink")、政治部主任[刘伯坚](../Page/刘伯坚.md "wikilink")。全军团共17000人，属[红一方面军编制](../Page/红一方面军.md "wikilink")。下辖

  - [红十三军](../Page/红十三军.md "wikilink")，军长董振堂（兼），政治委员[邝朱权](../Page/邝朱权.md "wikilink")；
  - [红十四军](../Page/红十四军.md "wikilink")，军长赵博生（兼）、政治委员[黄火青](../Page/黄火青.md "wikilink")；
  - [红十五军](../Page/红十五军.md "wikilink")，军长[黄中岳](../Page/黄中岳.md "wikilink")、政治委员[左权](../Page/左权.md "wikilink")。

1932年3月12日，根据[中革军委指示](../Page/中革军委.md "wikilink")，红三军团改辖[红三军和红十三军](../Page/红三军.md "wikilink")，不久，又将红三军划归[红一军团](../Page/红一军团.md "wikilink")。1933年6月，中央红军进行整编，取消军一级编制，红五军团下辖第十三师、第十四师和第三十四师。此时各级主官为：军团长董振堂、政治委员[朱瑞](../Page/朱瑞.md "wikilink")（后[李卓然代](../Page/李卓然.md "wikilink")）、参谋长[郑如岳](../Page/郑如岳.md "wikilink")（后[李屏仁代](../Page/李屏仁.md "wikilink")）、政治部主任刘伯坚、供给部长[胡南生](../Page/胡南生.md "wikilink")。

  - 十三师师长[陈伯钧](../Page/陈伯钧.md "wikilink")、政治委员[谢良](../Page/谢良.md "wikilink")；
  - 十四师师长[程子华](../Page/程子华.md "wikilink")（后[张宗逊代](../Page/张宗逊.md "wikilink")）、政治委员[朱良才](../Page/朱良才.md "wikilink")；
  - 三十四师师长[陈树湘](../Page/陈树湘.md "wikilink")。该师是纯闽西红军，1933年3月由福建军区第55、56、57师合编为红12军34师，首任师长周子昆兼，政治委员谭震林兼，下辖第100团(团长韩伟、政委范世英)、101团(团长陈树湘、政委杨一实)、102团(团长吕贯英、政委程翠林)。1934年2月编入红五军团。1934年4月，师长由101团团长陈树湘接任，参谋长为王光道，政治部主任为刘英。

1934年10月10日，红五军团开始[长征](../Page/长征.md "wikilink")，当时的各级主官为：军团长董振堂、政治委员李卓然、中央代表[陈云](../Page/陈云.md "wikilink")、参谋长陈伯钧、政治部主任[曾日三](../Page/曾日三.md "wikilink")，下辖两个师：

  - 第十二师师长[陈伯钧](../Page/陈伯钧.md "wikilink")（兼任）、政治委员[谢良](../Page/谢良.md "wikilink")；
  - 第三十四师师长[陈树湘](../Page/陈树湘.md "wikilink")、政委程翠霖、参谋长[王光道](../Page/王光道.md "wikilink")。第100团团长[韩伟](../Page/韩伟_\(中将\).md "wikilink")。

在长征期间，红五军团主要负责殿后任务，是各部队中损失最重的一支。红34师为红五军团的后卫师。1934年11月16日军委纵队离开道县,
开始向湘江渡口方向行进，34师被要求留在原地"坚决阻止尾追之敌"，以掩护行动缓慢并且走了弯路的第八军团，同时担任整个中央红军的后卫。命令还特别指示34师："万一被敌截断，返回湖南发展游击战争。"34师在文市镇以东地区，与追击的国民党中央军周浑元部激战持续到12月1日。1934年12月1日，根据军委电示34师完成阻击战，向东突围，在优势敌军重重围困中最终全军覆没。

1934年12月26日[黎平会议后](../Page/黎平会议.md "wikilink")，[红八军团撤销](../Page/红八军团.md "wikilink")，全体编入红五军团。1935年2月9日[遵义会议后](../Page/遵义会议.md "wikilink")，红五军团撤销师一级编制，缩编为三个团。

1935年7月，红一方面军和[红四方面军会师后](../Page/红四方面军.md "wikilink")，红五军团改称“[红五军](../Page/红五军.md "wikilink")”，军长[董振堂](../Page/董振堂.md "wikilink")、代理政治委员[曾日三](../Page/曾日三.md "wikilink")、代理参谋长[曹里怀](../Page/曹里怀.md "wikilink")。1935年11月红一、四方面军分离后，红五军随红四方面军行动，与原属红四方面军的[红三十三军合编为新的红五军](../Page/红三十三军.md "wikilink")，属红四方面军编制，后随红四方面军北上。1936年10月22日，红军三大主力在[甘肃](../Page/甘肃.md "wikilink")[会宁会师后](../Page/会宁.md "wikilink")，红五军随同四方面军主力西渡[黄河](../Page/黄河.md "wikilink")，作为[西路军的一部进军甘肃](../Page/西路军.md "wikilink")，在战斗中损失殆尽\[1\]，两任军长[董振堂](../Page/董振堂.md "wikilink")、[孙玉清均死于河西](../Page/孙玉清.md "wikilink")。

## 参考文献

<div class="references-small">

<references>

</references>

</div>

[军团](../Page/category:中国工农红军.md "wikilink")

1.