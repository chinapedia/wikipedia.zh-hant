**大英浸信会**，英語舊名：，2000年後改為英語現名：，是一個由來自[英格蘭的](../Page/英格蘭.md "wikilink")[浸信宗信徒成立的](../Page/浸信宗.md "wikilink")[基督教差會](../Page/傳福音.md "wikilink")。

## 成立

1792年成立差会。\[1\]

## 在中国

1860年来华。

### 山东

[Former_Nanguan_Christian_Church_of_Jinan_2009-08.JPG](https://zh.wikipedia.org/wiki/File:Former_Nanguan_Christian_Church_of_Jinan_2009-08.JPG "fig:Former_Nanguan_Christian_Church_of_Jinan_2009-08.JPG")，今为[齐鲁医院食堂](../Page/齐鲁医院.md "wikilink")\]\]

  - 传教站：[青州](../Page/青州.md "wikilink")（1877）\[2\]、[周村](../Page/周村.md "wikilink")（1903）、[濟南](../Page/濟南.md "wikilink")（1904）、[北镇](../Page/北镇.md "wikilink")
  - 教堂：[济南南关教堂](../Page/济南南关礼拜堂旧址.md "wikilink")、[周村东门里礼拜堂](../Page/周村东门里礼拜堂.md "wikilink")
  - 医院：[青州广德医院](../Page/青州广德医院.md "wikilink")、[周村复育医院](../Page/周村复育医院.md "wikilink")
  - 学校：[齐鲁大学](../Page/齐鲁大学.md "wikilink")（合办）\[3\]

### 山西

  - 传教站:
    [太原](../Page/太原.md "wikilink")（1878）、[忻州](../Page/忻州.md "wikilink")（1885）、[代州](../Page/代州直隸州.md "wikilink")（1892）
  - 教堂：[太原桥头街教堂](../Page/太原桥头街教堂.md "wikilink")
  - 医院：[太原博爱医院](../Page/太原博爱医院.md "wikilink")

### 陕西

  - 传教站：[三原](../Page/三原.md "wikilink")（1893）、[西安](../Page/西安.md "wikilink")（1894）\[4\]、[福音村](../Page/福音村.md "wikilink")（1903）
  - 教堂：[西安南新街教堂](../Page/西安南新街教堂.md "wikilink")，[东关教堂](../Page/东关教堂.md "wikilink")
  - 医院：[西安广仁医院](../Page/西安广仁医院.md "wikilink")

## 人物

  - Charles J. Hall
  - Hendrikadius Zwaantinus Kloekers
  - [李提摩太](../Page/李提摩太.md "wikilink")（Timothy Richard）\[5\]
  - [怀恩光](../Page/怀恩光.md "wikilink")（John Sutherland Whitewright,
    1858-1926）

## 参见

  - [英国浸礼会在华传教士列表](../Page/英国浸礼会在华传教士列表.md "wikilink")

## 參考文獻

[Category:基督教教會與教派](../Category/基督教教會與教派.md "wikilink")
[Category:基督教在华差会](../Category/基督教在华差会.md "wikilink")

1.

2.

3.
4.
5.