**教育部**是一些[国家的政府部门](../Page/国家.md "wikilink")，也是指各國家（地區）的[教育主管單位](../Page/教育.md "wikilink")。

  - [中华人民共和国教育部](../Page/中华人民共和国教育部.md "wikilink")
  - [中华民国教育部](../Page/中华民国教育部.md "wikilink")
  - [马来西亚教育部](../Page/马来西亚教育部.md "wikilink")
  - [新加坡教育部](../Page/新加坡教育部.md "wikilink")

## 特色

世界政治史上的第一个教育部是[波兰立陶宛王国的](../Page/波兰立陶宛王国.md "wikilink")[国民教育委员会](../Page/国民教育委员会.md "wikilink")，它于1773年设立。[奥地利的最高教育部門稱為](../Page/奥地利.md "wikilink")[教育科学和文化部](../Page/教育科学和文化部.md "wikilink")，[日本則稱為](../Page/日本.md "wikilink")[文部科學省](../Page/文部科學省.md "wikilink")。

## 各國教育部列表

<table>
<tbody>
<tr class="odd">
<td><ul>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
</ul></td>
<td><ul>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
</ul></td>
<td><ul>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
</ul></td>
<td><ul>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
</ul></td>
</tr>
</tbody>
</table>

### 亞洲

  - [日本](../Page/日本.md "wikilink")[文部科學省](../Page/文部科學省.md "wikilink")
  - [大韓民國教育部](../Page/大韓民國教育部.md "wikilink")
  - [教育部 (韓國) 韓文版](http://www.moe.go.kr/)
  - [教育部 (韓國) 英文版](http://english.moe.go.kr/)
  - [學部 (清朝)](../Page/學部_\(清朝\).md "wikilink")
  - [中華民國教育部](../Page/中華民國教育部.md "wikilink")
  - [教育部 (印度)](../Page/教育部_\(印度\).md "wikilink")
  - [中華人民共和國教育部](../Page/中華人民共和國教育部.md "wikilink")
  - [學部 (越南)](../Page/學部_\(越南\).md "wikilink")
  - [教育部 (越南)](../Page/教育部_\(越南\).md "wikilink")
  - [教育部 (新加坡)](http://www.moe.gov.sg)
  - [教育部 (馬來西亞)](../Page/教育部_\(馬來西亞\).md "wikilink")
  - [教育部 (以色列)](../Page/教育部_\(以色列\).md "wikilink")
  - [教育部 (伊拉克)](../Page/教育部_\(伊拉克\).md "wikilink")
  - [教育部 (沙烏地阿拉伯)](../Page/教育部_\(沙烏地阿拉伯\).md "wikilink")
  - [教育部 (不丹)](../Page/教育部_\(不丹\).md "wikilink")
  - [教育部 (巴林)](../Page/教育部_\(巴林\).md "wikilink")
  - [教育部 (約旦)](../Page/教育部_\(約旦\).md "wikilink")
  - [教育部 (科威特)](../Page/教育部_\(科威特\).md "wikilink")
  - [教育部 (沙烏地阿拉伯)](http://www.moe.gov.sa)
  - [教育部 (不丹)](http://www.education.gov.bt)
  - [教育部 (巴林)](http://www.education.gov.bh)
  - [教育部
    (約旦)](https://web.archive.org/web/20110501065326/http://www.moe.gov.jo/)
  - [教育部 (科威特)](http://www.moe.edu.kw)

### 欧洲

  - [教育部 (英國)](../Page/教育部_\(英國\).md "wikilink")
  - [國民教育部 (法國)](../Page/國民教育部_\(法國\).md "wikilink")
  - [聯邦教育與研究部 (德國)](../Page/聯邦教育與研究部_\(德國\).md "wikilink")
  - [教育部 (芬蘭)](../Page/教育部_\(芬蘭\).md "wikilink")
  - [教育部 (波蘭)](../Page/教育部_\(波蘭\).md "wikilink")
  - [教育部 (丹麥)](../Page/教育部_\(丹麥\).md "wikilink")
  - [教育部 (丹麥)](http://eng.uvm.dk)
  - [教育部 (馬耳他)](../Page/教育部_\(馬耳他\).md "wikilink")
  - [教育部 (馬爾他)](http://www.education.gov.mt)
  - [芬蘭國家教育局](../Page/芬蘭國家教育局.md "wikilink")
  - [教育與體育部 (塞爾維亞)](../Page/教育與體育部_\(塞爾維亞\).md "wikilink")

### 北美洲

  - [美國教育部](../Page/美國教育部.md "wikilink")
  - [教育委員會 (美國各州)](../Page/教育委員會_\(美國各州\).md "wikilink")
  - [教育部 (加拿大 渥太華省/ON省)](http://www.edu.gov.on.ca/)
  - [教育部 (加拿大 卑斯省/BC省)](http://www.bced.gov.bc.ca/)
  - [教育部 (加拿大 安大略省)](../Page/教育部_\(加拿大_安大略省\).md "wikilink")

### 大洋洲

  - [教育部 (澳洲)](../Page/教育部_\(澳洲\).md "wikilink")
  - [教育部 (紐西蘭)](http://www.minedu.govt.nz)

### 非洲

  - [教育部 (肯亞)](../Page/教育部_\(肯亞\).md "wikilink")
  - [教育部 (烏干達)](http://www.education.go.ug)
  - [教育部
    (波札那)](https://web.archive.org/web/20080913201204/http://www.moe.gov.bw/)

## 参见

  - [教育](../Page/教育.md "wikilink")
  - [教育局](../Page/教育局.md "wikilink")
  - [禮部](../Page/禮部.md "wikilink")

[教育部門](../Category/教育部門.md "wikilink")