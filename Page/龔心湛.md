[Gong_Xinzhan.jpg](https://zh.wikipedia.org/wiki/File:Gong_Xinzhan.jpg "fig:Gong_Xinzhan.jpg")
**龔心湛**（），原名**心瀛**，字**仙舟**，[安徽](../Page/安徽.md "wikilink")[合肥人](../Page/合肥.md "wikilink")，[祖籍](../Page/祖籍.md "wikilink")[江西](../Page/江西.md "wikilink")[臨川](../Page/臨川.md "wikilink")\[1\]\[2\]。民国初期[皖系政治人物](../Page/皖系.md "wikilink")，曾代理[国务总理](../Page/中華民國國務總理.md "wikilink")。

## 生平

他是清朝[监生](../Page/监生.md "wikilink")，[金陵同文馆毕业](../Page/金陵同文馆.md "wikilink")。後來留學日本。此後曾在中國驻日本、美國、法國、意大利、菲律賓等国使領馆任随员多年。歸國後，他曾任[廣東知府兼署理](../Page/廣東省.md "wikilink")[按察使](../Page/提刑按察使司按察使.md "wikilink")，兼廉欽兵備道。後來他曾歷任[雲南](../Page/雲南省.md "wikilink")[臨安開廣道](../Page/臨安開廣道.md "wikilink")、雲南提法使。

1912年（[民国元年](../Page/民国紀元.md "wikilink")）後，他曾歴任[武昌造幣廠廠長](../Page/武昌区.md "wikilink")、[漢口中国銀行行長](../Page/漢口.md "wikilink")、賑撫局督辦、[安徽省国税籌備處處長](../Page/安徽省_\(中華民国\).md "wikilink")、財政司司長。1914年（民国3年），他任安徽省財政廳廳長。1915年（民国4年）6月，他升任北京政府財政部次長，兼任鹽務署督辦。同年12月，他任督辦經界局事務。1916年（民国5年）4月，他任[参政院参政](../Page/参政院.md "wikilink")。

1918年（民国7年）11月他被任命為安徽省省長，未就任。1919年（民国8年）1月，他任北京政府財政部總長兼幣制局督辦兼造幣廠總裁。1919年6月13日至9月24日，他曾代理[国务总理](../Page/中華民國國務總理.md "wikilink")，因不堪直系[吴佩孚電斥而請辭](../Page/吴佩孚.md "wikilink")。

1924年（民国13年）11月，他任[段祺瑞執政政府的内務部總長兼](../Page/段祺瑞.md "wikilink")[揚子江水道討論委員会会長兼賑務督辦](../Page/揚子江水道討論委員会.md "wikilink")。1925年（民国14年）11月，他署交通部總長，12月被正式任命為交通部總長。1926年（民国15年）4月段倒台后，他辞任。翌年6月，他任[耀華玻璃公司總董](../Page/耀華玻璃公司.md "wikilink")，12月他任督辦京都事宜。

晩年他住在[天津英租界](../Page/天津英租界.md "wikilink")，退出政界辦实业，先后担任[大陆银行](../Page/大陆银行.md "wikilink")、中孚银行董事、[耀华玻璃公司总董](../Page/耀华玻璃公司.md "wikilink")、[开滦矿务局董事](../Page/开滦矿务局.md "wikilink")、[启新洋灰公司总理](../Page/启新洋灰公司.md "wikilink")、董事长等。1940年曾与[靳云鹏等人发起资助修建](../Page/靳云鹏.md "wikilink")[天津大悲院新庙](../Page/大悲禅院.md "wikilink")。1942年（民国31年）3月，他任[汪精衛政權的](../Page/汪精衛政權.md "wikilink")[華北政務委員会諮議会議委員](../Page/華北政務委員会.md "wikilink")。

1943年（民国32年）12月，他在天津病逝。享年73嵗。

## 註釋

## 参考文献

  - <span style="font-size:90%;"></span>
  - <span style="font-size:90%;"></span>

| （[北京政府](../Page/北京政府.md "wikilink")） |
| ------------------------------------ |

[Category:清朝廉欽道](../Category/清朝廉欽道.md "wikilink")
[Category:清朝臨安開廣道](../Category/清朝臨安開廣道.md "wikilink")
[Category:清朝雲南提法使](../Category/清朝雲南提法使.md "wikilink")
[G龔](../Category/中華民國大陸時期政治人物.md "wikilink")
[Category:中華民國國務總理](../Category/中華民國國務總理.md "wikilink")
[Category:中華民國財政總長](../Category/中華民國財政總長.md "wikilink")
[Category:中華民國內務總長](../Category/中華民國內務總長.md "wikilink")
[Category:中華民國交通總長](../Category/中華民國交通總長.md "wikilink")
[Category:参政院参政](../Category/参政院参政.md "wikilink")
[X心](../Category/龔姓.md "wikilink")
[Category:合肥人](../Category/合肥人.md "wikilink")

1.  [馆藏家谱—龚](http://www.ahlib.com/ah/gjb/gcjp-gongshi.htm)
2.  [四牌楼走出的国务总理：稻香村、逍遥津是他的私产，一生只对日本说“不”](https://m.sohu.com/a/279153406_611116/?pvid=000115_3w_a)