[Wuhan_german_divs.jpg](https://zh.wikipedia.org/wiki/File:Wuhan_german_divs.jpg "fig:Wuhan_german_divs.jpg")
[Bundeswehr_G36.jpg](https://zh.wikipedia.org/wiki/File:Bundeswehr_G36.jpg "fig:Bundeswehr_G36.jpg")\]\]
[
](https://zh.wikipedia.org/wiki/File:Conscription_map_of_the_world.svg "fig:    ")

**陸軍**是人類歷史上最古老的一個[軍種](../Page/軍種.md "wikilink")，主要在陸地上作戰。現代陸軍有多個[兵科](../Page/兵種.md "wikilink")，包括[步兵](../Page/步兵.md "wikilink")、[裝甲兵](../Page/裝甲兵.md "wikilink")、[炮兵](../Page/炮兵.md "wikilink")、[航空兵](../Page/陸軍航空兵.md "wikilink")、[工兵](../Page/工兵.md "wikilink")、[通訊兵](../Page/通訊兵.md "wikilink")、[運輸等等](../Page/運輸.md "wikilink")，是目前世界上人數最多的軍種。

在[冷兵器時代](../Page/冷兵器.md "wikilink")，陸軍主要以[刀](../Page/刀.md "wikilink")、[劍](../Page/劍.md "wikilink")、[斧](../Page/斧.md "wikilink")、[矛](../Page/矛.md "wikilink")、[弓](../Page/弓.md "wikilink")、[弩等作為](../Page/弩.md "wikilink")[戰鬥工具](../Page/戰鬥.md "wikilink")。而到了現代，陸軍[兵器則有](../Page/兵器.md "wikilink")[槍械](../Page/槍械.md "wikilink")、[火炮](../Page/火炮.md "wikilink")、[飛彈](../Page/飛彈.md "wikilink")、[戰車](../Page/戰車.md "wikilink")、[直昇機等類別](../Page/直昇機.md "wikilink")，可謂多不勝數。

## 任務

陸軍本質上是以佔領及控制人類賴以維生的陸地為目的的武裝力量。陸軍會對某一目標區域進行攻擊，以殲滅或驅逐原先在該處的敵軍後進行佔領，而獲得該區域後就必須進行防守，以避免該區域被敵人奪走。為了控制佔領區域以進行某種程度的利用，治安的維持也是必須的，雖然這平常是[警察在進行的工作](../Page/警察.md "wikilink")，但是在某些嚴峻的條件下，例如警方沒有足夠的力量對抗反對者時，就有可能會由陸軍等部隊接管。

陸軍平日所活動的地方一般稱為**駐地**，因為整個陸地都可以為陸軍所用，對陸軍部隊而言不太需要依賴特定的**基地**做為活動據點，只要補給部隊能夠到達，陸軍部隊可以在任何地方長時間駐紮或作戰，必要時甚至也會直接徵用當地資源。不過在陸軍中，也會有一些像[要塞](../Page/要塞.md "wikilink")、大型後勤設施、演訓場地這類不能移動的據點。

另外，由於具有充足的人力、高度的獨立性、必要的機具設備等，陸軍也常被投入救災任務，許多大規模災害中都可以見到陸軍的身影。

## 組成

陸軍主要可以區分為戰鬥部隊、戰鬥支援部隊、後勤部隊等，又依據各種兵科的不同而有各種編成，依據上下階級編制，通常在營或團級以下幾乎都是同一兵科，由數種不同兵科的部隊組成更大規模的部隊，而某些支援性質的兵科則可能分佈在各種部隊中。以兵科來看，[步兵](../Page/步兵.md "wikilink")、[裝甲兵](../Page/裝甲兵.md "wikilink")、[炮兵](../Page/炮兵.md "wikilink")、航空兵等是屬於戰鬥兵科，而支援兵科則包含了[工兵](../Page/工兵.md "wikilink")、兵工、[通信兵](../Page/通信兵.md "wikilink")、化學兵、運輸、[軍醫](../Page/軍醫.md "wikilink")、經理、行政、[憲兵等等不一而足](../Page/憲兵.md "wikilink")。

陸軍的戰鬥單位由大至小依序是[集團軍](../Page/集團軍.md "wikilink")、[軍團](../Page/軍團.md "wikilink")、[軍](../Page/軍.md "wikilink")、[師](../Page/師.md "wikilink")、[旅](../Page/旅.md "wikilink")、[團](../Page/團.md "wikilink")、[營](../Page/營.md "wikilink")、[連](../Page/連.md "wikilink")、[排](../Page/排.md "wikilink")、[班](../Page/班.md "wikilink")，其中有些層級可能有不同的名稱，也有些層級可能在裁軍的過程中消失，班之下有時還有[伍的編制](../Page/伍.md "wikilink")，不過大多-{只}-用在步兵，另外[中華民國國軍亦有單一兵種組成](../Page/中華民國國軍.md "wikilink")，略大於團級編制、小於旅級編制的[群級編制](../Page/群_\(軍隊\).md "wikilink")，編階通常為[上校指揮官](../Page/上校.md "wikilink")，如化兵群、工兵群、防空警衛群等，用於執行特定任務。而集團軍之上通常就是軍區或戰區這種包含了陸海空各軍種，以區域為劃分，而沒有機動性質的層級了。

## 相關條目

  - [戰爭](../Page/戰爭.md "wikilink")
  - [海軍](../Page/海軍.md "wikilink")
  - [海軍陸戰隊](../Page/海軍陸戰隊.md "wikilink")
  - [空軍](../Page/空軍.md "wikilink")
  - [軍事學](../Page/軍事學.md "wikilink")
  - [補給線](../Page/補給線.md "wikilink")

[\*](../Category/陸軍.md "wikilink")