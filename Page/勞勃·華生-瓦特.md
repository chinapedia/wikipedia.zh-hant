[Robert_Watson-Watt.JPG](https://zh.wikipedia.org/wiki/File:Robert_Watson-Watt.JPG "fig:Robert_Watson-Watt.JPG")

**勞勃·華生-瓦特**爵士（，），[英國](../Page/英國.md "wikilink")[蘇格蘭物理學家](../Page/蘇格蘭.md "wikilink")，[雷達的發明者](../Page/雷達.md "wikilink")。他是蒸汽机发明者[詹姆斯·瓦特的后代](../Page/詹姆斯·瓦特.md "wikilink")。

## 生平

[二次大戰時期](../Page/二次大戰.md "wikilink")，他曾被[英國空軍部招集研究](../Page/英國空軍.md "wikilink")[死光的可能性](../Page/死光.md "wikilink")。另外，他所發明的雷達在二戰期間發揮了預警的功效。在英國軍方的支持下，他率領團隊著手建造雷達站，並於1935年6月17日首次成功偵測到飛機。另外在1956年，華生-瓦特在[加拿大開車超速](../Page/加拿大.md "wikilink")，被雷達測速槍測得，因此被開了一張[加幣](../Page/加幣.md "wikilink")12.5元的罰單。華生-瓦特因此自嘲成了自己發明的受害者\[1\]。

## 參考資料

[Category:英國物理學家](../Category/英國物理學家.md "wikilink")
[Category:鄧地大學校友](../Category/鄧地大學校友.md "wikilink")
[Category:休斯奖章获得者](../Category/休斯奖章获得者.md "wikilink")
[Category:瓦尔德马尔·浦耳生金质奖章获得者](../Category/瓦尔德马尔·浦耳生金质奖章获得者.md "wikilink")
[Category:英国皇家航空学会会士](../Category/英国皇家航空学会会士.md "wikilink")

1.