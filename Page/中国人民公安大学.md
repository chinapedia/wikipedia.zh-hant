**中国人民公安大学**，简称**公安大学**，是位于[中国](../Page/中国.md "wikilink")[北京市的公安部直属重点高等院校](../Page/北京市.md "wikilink")，与**公安部高级警官学院**[合署办公](../Page/合署办公.md "wikilink")，被誉为“共和国高级警官的摇篮”，为公安系统最高学府，承担着高级警官培训、本科生、硕士研究生和博士研究生教育培养、港澳执法部门人员培训、对外警学交流等多项任务职能，与[中国刑事警察学院](../Page/中国刑事警察学院.md "wikilink")、[中国人民武装警察部队学院并列为公安部直属三大院校](../Page/中国人民武装警察部队学院.md "wikilink")。学校自1948年建立以来，已经向全国公安战线输送了十余万公安人员。党和国家领导人毛泽东、邓小平、胡锦涛等均曾对学校建设做出批示，现所用校名为邓小平书写。学校有两个校区，分别为位于[西城区的木樨地校区](../Page/西城区.md "wikilink")、位于[大兴区的团河校区](../Page/大兴区.md "wikilink")。

## 历史沿革

  - 1948年，创建**华北公安干部学校**。
  - 1950年，更名**中央公安干部学校**。
  - 1953年，升格**中央人民公安学院**。
  - 1959年，改建**中央政法干部学校**，不久被撤销。
  - 1978年，**中央政法干部学校**复校。
  - 1982年，升格**中央人民公安学院**。
  - 1984年，升格**中国人民公安大学**。
  - 1986年，创建**公安部管理干部学院**，合署办公。
  - 1995年，公安部第4研究所并入中国人民公安大学。
  - 1998年2月，[中国人民警官大学与中国人民公安大学合并](../Page/中国人民警官大学.md "wikilink")。
  - 2000年，[北京人民交通警察学校并入中国人民公安大学](../Page/北京人民交通警察学校.md "wikilink")。
  - 2006年，公安部管理干部学院更名**公安部高级警官学院**，仍为[合署办公](../Page/合署办公.md "wikilink")。

## 院系设置

  - 侦查与反恐怖学院、治安学院、犯罪学院、公安管理学院、国际警务学院、刑事科学技术学院、安全防范工程学院、交通管理工程学院、法学院、军队保卫学院、警体战训学院。
  - 理科基础课教研部、马列理论课教研部、警体战训教研部，人文社科教研部。

## 历任校长

## 知名校友

  - [任长霞](../Page/任长霞.md "wikilink")
  - [崔亚东](../Page/崔亚东.md "wikilink")
  - [王立军](../Page/王立军.md "wikilink")
  - [王小洪](../Page/王小洪.md "wikilink")
  - [盧偉聰](../Page/盧偉聰.md "wikilink")

## 参考文献

## 外部链接

  - [中国人民公安大学](https://archive.is/20130116050019/http://www.cppsu.edu.cn/)

## 参见

  - [中央警察大學](../Page/中央警察大學.md "wikilink")
  - [中华人民共和国公安部](../Page/中华人民共和国公安部.md "wikilink")
      - [中国刑事警察学院](../Page/中国刑事警察学院.md "wikilink")
      - [中国人民武装警察部队学院](../Page/中国人民武装警察部队学院.md "wikilink")
  - [中国人民武装警察部队指挥学院](../Page/中国人民武装警察部队指挥学院.md "wikilink")

{{-}}

[Category:中华人民共和国公安部](../Category/中华人民共和国公安部.md "wikilink")
[Category:公安警察院校](../Category/公安警察院校.md "wikilink")
[中国人民公安大学](../Category/中国人民公安大学.md "wikilink")
[Category:1948年創建的教育機構](../Category/1948年創建的教育機構.md "wikilink")
[Category:1948年中國建立](../Category/1948年中國建立.md "wikilink")
[Category:一流学科建设高校](../Category/一流学科建设高校.md "wikilink")