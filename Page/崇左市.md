**崇左市**是[中华人民共和国](../Page/中华人民共和国.md "wikilink")[广西壮族自治区下辖的](../Page/广西壮族自治区.md "wikilink")[地级市](../Page/地级市.md "wikilink")，位于广西西南部。市境东邻自治区首府[南宁市](../Page/南宁市.md "wikilink")，东南接[防城港市](../Page/防城港市.md "wikilink")，北与[百色市相邻](../Page/百色市.md "wikilink")，西面及南面与[越南接壤](../Page/越南.md "wikilink")。地处桂西南山地丘陵区[左江沿岸](../Page/左江.md "wikilink")，地势西高东低，西部为[大青山](../Page/大青山.md "wikilink")，南部为[公母山](../Page/公母山.md "wikilink")、[十万大山](../Page/十万大山.md "wikilink")。[左江横贯市境并流经城区](../Page/左江.md "wikilink")，往东与[右江汇合成](../Page/右江.md "wikilink")[邕江](../Page/邕江.md "wikilink")。全市总面积1.73万平方公里，总人口205.45万，[壮族人口比例超过八成](../Page/壮族.md "wikilink")。[湘桂铁路过境](../Page/湘桂铁路.md "wikilink")，中越边境凭祥口岸及[友谊关坐落市境西南部](../Page/友谊关.md "wikilink")，为中国通往[越南及](../Page/越南.md "wikilink")[东盟各国的重要陆路通道](../Page/东盟.md "wikilink")。

## 历史

“崇左”之名是取[崇善县](../Page/崇善县.md "wikilink")、[左县两县之首字而得名](../Page/左县.md "wikilink")。[唐](../Page/唐朝.md "wikilink")[贞观元年](../Page/贞观.md "wikilink")（627年）於境内置左江镇。唐末设[太平州](../Page/太平州.md "wikilink")、[左州两个](../Page/左州.md "wikilink")[羁縻州](../Page/羁縻州.md "wikilink")。[北宋](../Page/北宋.md "wikilink")[皇祐五年](../Page/皇祐.md "wikilink")（1053年）置崇善县，属[邕州](../Page/邕州.md "wikilink")。[元](../Page/元朝.md "wikilink")[至元二十九年](../Page/至元.md "wikilink")（1292年）置[太平路](../Page/太平路.md "wikilink")，治所在今崇左城区。[明](../Page/明朝.md "wikilink")[洪武二年](../Page/洪武.md "wikilink")（1369年），改太平路为[太平府](../Page/太平府_\(广西省\).md "wikilink")，隶[左江道](../Page/左江道.md "wikilink")。[清代](../Page/清代.md "wikilink")，太平府为于[太平思顺道治所](../Page/太平思顺道.md "wikilink")。

[民国二年](../Page/民国.md "wikilink")（1913年）废府存县，辖境属[镇南道](../Page/镇南道.md "wikilink")，治今[龙州县](../Page/龙州县.md "wikilink")。1930年2月1日，[中国共产党发动](../Page/中国共产党.md "wikilink")[龙州起义](../Page/龙州起义.md "wikilink")，建立左江[革命委员会](../Page/革命委员会.md "wikilink")。1949年，[解放军先后占领崇善县及左县](../Page/解放军.md "wikilink")，设立[龙州专区](../Page/龙州专区.md "wikilink")，专署驻[龙津县龙州镇](../Page/龙津县.md "wikilink")，辖崇善县及左县等14县。1951年，龙州专区更名为[崇左专区](../Page/崇左专区.md "wikilink")，专署迁驻崇善县。

1952年8月11日，崇善县与左县合并为崇左县。1953年，撤销崇左专区，并入[邕宁专区](../Page/邕宁专区.md "wikilink")。不久又撤销邕宁专区，辖县由[桂西僮族自治区直辖](../Page/桂西僮族自治区.md "wikilink")。1957年，复置邕宁专区，辖境各县属之，专署驻[南宁市](../Page/南宁市.md "wikilink")。1958年，撤销邕宁专区，所辖县市划归[南宁专区](../Page/南宁专区.md "wikilink")。1971年改置[南宁地区](../Page/南宁地区.md "wikilink")，辖境各县属之。

2003年8月6日，[南宁地区正式撤销](../Page/南宁地区.md "wikilink")，并新设立地级崇左市，将崇左县改设为崇左市[江州区](../Page/江州区.md "wikilink")，崇左市下辖原南宁地区的[扶绥县](../Page/扶绥县.md "wikilink")、[宁明县](../Page/宁明县.md "wikilink")、[龙州县](../Page/龙州县.md "wikilink")、[大新县](../Page/大新县.md "wikilink")、[天等县](../Page/天等县.md "wikilink")，代管县级[凭祥市](../Page/凭祥市.md "wikilink")。

## 资源

  - [甘蔗种植面积](../Page/甘蔗.md "wikilink")340万亩，年产蔗糖200万吨左右，约占广西的35%、全国的21%，是中国产[蔗糖第一大市](../Page/蔗糖.md "wikilink")。
  - [剑麻种植面积](../Page/剑麻.md "wikilink")16.1万亩，是广西最大的剑麻生产原料基地。

## 政治

### 现任领导

<table>
<caption>崇左市四大机构现任领导人</caption>
<thead>
<tr class="header">
<th><p>机构</p></th>
<th><p><br />
<a href="../Page/中国共产党崇左市委员会.md" title="wikilink">中国共产党<br />
崇左市委员会</a><br />
书记</p></th>
<th><p><br />
<a href="../Page/崇左市人民代表大会.md" title="wikilink">崇左市人民代表大会</a><br />
常务委员会<br />
主任</p></th>
<th><p><br />
<a href="../Page/崇左市人民政府.md" title="wikilink">崇左市人民政府</a><br />
<br />
市长</p></th>
<th><p><br />
<a href="../Page/中国人民政治协商会议崇左市委员会.md" title="wikilink">中国人民政治协商会议<br />
崇左市委员会</a><br />
主席</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>姓名</p></td>
<td><p><a href="../Page/刘有明.md" title="wikilink">刘有明</a>[1]</p></td>
<td><p><a href="../Page/何良军.md" title="wikilink">何良军</a>[2]</p></td>
<td><p><a href="../Page/黄卫革.md" title="wikilink">黄卫革</a>[3]</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>民族</p></td>
<td><p><a href="../Page/汉族.md" title="wikilink">汉族</a></p></td>
<td><p>汉族</p></td>
<td><p><a href="../Page/壮族.md" title="wikilink">壮族</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>籍贯</p></td>
<td><p><a href="../Page/广西壮族自治区.md" title="wikilink">广西壮族自治区</a><a href="../Page/全州县.md" title="wikilink">全州县</a></p></td>
<td><p><a href="../Page/湖南省.md" title="wikilink">湖南省</a><a href="../Page/道县.md" title="wikilink">道县</a></p></td>
<td><p>广西壮族自治区<a href="../Page/田东县.md" title="wikilink">田东县</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>出生日期</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>就任日期</p></td>
<td><p>2016年1月</p></td>
<td><p>2016年2月</p></td>
<td><p>2018年1月</p></td>
<td><p>2016年9月</p></td>
</tr>
</tbody>
</table>

### 行政区划

现辖1个[市辖区](../Page/市辖区.md "wikilink")、5个[县](../Page/县_\(中华人民共和国\).md "wikilink")，代管1个[县级市](../Page/县级市.md "wikilink")。

  - 市辖区：[江州区](../Page/江州区.md "wikilink")
  - 县级市：[凭祥市](../Page/凭祥市.md "wikilink")
  - 县：[扶绥县](../Page/扶绥县.md "wikilink")、[宁明县](../Page/宁明县.md "wikilink")、[龙州县](../Page/龙州县.md "wikilink")、[大新县](../Page/大新县.md "wikilink")、[天等县](../Page/天等县.md "wikilink")

<table>
<thead>
<tr class="header">
<th><p><strong>崇左市行政区划图</strong></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>区划代码[4]</p></td>
</tr>
<tr class="odd">
<td><p>街道</p></td>
</tr>
<tr class="even">
<td><p>451400</p></td>
</tr>
<tr class="odd">
<td><p>451402</p></td>
</tr>
<tr class="even">
<td><p>451421</p></td>
</tr>
<tr class="odd">
<td><p>451422</p></td>
</tr>
<tr class="even">
<td><p>451423</p></td>
</tr>
<tr class="odd">
<td><p>451424</p></td>
</tr>
<tr class="even">
<td><p>451425</p></td>
</tr>
<tr class="odd">
<td><p>451481</p></td>
</tr>
</tbody>
</table>

## 人口

<table>
<caption><strong>崇左市各区（县、市）人口数据</strong></caption>
<thead>
<tr class="header">
<th><p>区划名称</p></th>
<th><p>常住人口[5]（2010年11月）</p></th>
<th><p>户籍人口[6]<br />
（2010年末）</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>总计</p></td>
<td><p>比重<br />
（%）</p></td>
<td><p>每平方公里<br />
人口密度</p></td>
</tr>
<tr class="even">
<td><p>崇左市</p></td>
<td><p>1994285</p></td>
<td><p>100</p></td>
</tr>
<tr class="odd">
<td><p>江州区</p></td>
<td><p>316738</p></td>
<td><p>15.88</p></td>
</tr>
<tr class="even">
<td><p>扶绥县</p></td>
<td><p>379118</p></td>
<td><p>19.01</p></td>
</tr>
<tr class="odd">
<td><p>宁明县</p></td>
<td><p>337133</p></td>
<td><p>16.90</p></td>
</tr>
<tr class="even">
<td><p>龙州县</p></td>
<td><p>221768</p></td>
<td><p>11.12</p></td>
</tr>
<tr class="odd">
<td><p>大新县</p></td>
<td><p>296555</p></td>
<td><p>14.87</p></td>
</tr>
<tr class="even">
<td><p>天等县</p></td>
<td><p>330814</p></td>
<td><p>16.59</p></td>
</tr>
<tr class="odd">
<td><p>凭祥市</p></td>
<td><p>112159</p></td>
<td><p>5.62</p></td>
</tr>
</tbody>
</table>

根据2010年[第六次全国人口普查](../Page/第六次全国人口普查.md "wikilink")，全市[常住人口为](../Page/常住人口.md "wikilink")199.43万人\[7\]，与2000年[第五次全国人口普查的](../Page/第五次全国人口普查.md "wikilink")205.48万人相比，十年间共减少6.05万人，下降2.95%，年平均递减0.3%。其中，男性人口为104.31万人，占52.31%；女性人口为95.11万人，占47.69%。人口性别比（以女性为100）为109.67。0－14岁的人口为37.97万人，占19.04%；15－59岁的人口为133.02万人，占66.70%；60岁及以上人口为28.44万人，占14.26%，其中65岁及以上的人口为19.84万人，占9.95%。

### 民族

全市常住人口中，[壮族人口为](../Page/壮族.md "wikilink")174.72万人，占87.61%；汉族人口仅23.71万人，占11.89%；各[少数民族人口为](../Page/少数民族.md "wikilink")175.72万人，占88.11%。

{{-}}

| 民族名称         | [壮族](../Page/壮族.md "wikilink") | [汉族](../Page/汉族.md "wikilink") | [瑶族](../Page/瑶族.md "wikilink") | [苗族](../Page/苗族.md "wikilink") | [侗族](../Page/侗族.md "wikilink") | [维吾尔族](../Page/维吾尔族.md "wikilink") | [京族](../Page/京族.md "wikilink") | [仫佬族](../Page/仫佬族.md "wikilink") | [土家族](../Page/土家族.md "wikilink") | [布依族](../Page/布依族.md "wikilink") | 其他民族 |
| ------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ---------------------------------- | ------------------------------ | -------------------------------- | -------------------------------- | -------------------------------- | ---- |
| 人口数          | 1747233                        | 237113                         | 6423                           | 1187                           | 428                            | 245                                | 243                            | 207                              | 179                              | 166                              | 861  |
| 占总人口比例（%）    | 87.61                          | 11.89                          | 0.32                           | 0.06                           | 0.02                           | 0.01                               | 0.01                           | 0.01                             | 0.01                             | 0.01                             | 0.04 |
| 占少数民族人口比例（%） | 99.43                          | \---                           | 0.37                           | 0.07                           | 0.02                           | 0.01                               | 0.01                           | 0.01                             | 0.01                             | 0.01                             | 0.05 |

**崇左市民族构成（2010年11月）**\[8\]

## 教育

### 高等院校

  - 公办本科院校：[广西民族师范学院](../Page/广西民族师范学院.md "wikilink")、[桂林理工大学南宁分校](../Page/桂林理工大学南宁分校.md "wikilink")（空港校区）
  - 民办本科院校：[左江城市学院](../Page/左江城市学院.md "wikilink")
  - 民办专科院校：[广西理工职业技术学院](../Page/广西理工职业技术学院.md "wikilink")、[广西科技职业学院](../Page/广西科技职业学院.md "wikilink")、[广西中远职业学院](../Page/广西中远职业学院.md "wikilink")
  - 成人高等学校：[南宁地区教育学院](../Page/南宁地区教育学院.md "wikilink")

## 参考文献

{{-}}

[Category:广西地级市](../Category/广西地级市.md "wikilink")
[崇左](../Category/崇左.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.