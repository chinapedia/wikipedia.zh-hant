[Jostein_Gaarder.jpg](https://zh.wikipedia.org/wiki/File:Jostein_Gaarder.jpg "fig:Jostein_Gaarder.jpg")
**喬斯坦·賈德**（[挪威语](../Page/挪威语.md "wikilink")：，）是一位[挪威](../Page/挪威.md "wikilink")[小說](../Page/小說.md "wikilink")、[兒童文學的](../Page/兒童文學.md "wikilink")[作家](../Page/作家.md "wikilink")。

## 生平

賈德出生於挪威首都[奧斯陸](../Page/奧斯陸.md "wikilink")，曾於[奧斯陸大學攻讀](../Page/奧斯陸大學.md "wikilink")[斯堪的納維亞語言及](../Page/斯堪的納維亞.md "wikilink")[神學](../Page/神學.md "wikilink")。其後10年在[芬蘭教授](../Page/芬蘭.md "wikilink")[哲學](../Page/哲學.md "wikilink")，於1991年他成為一位全職作家。

他最暢銷的一本小說《[蘇菲的世界](../Page/蘇菲的世界.md "wikilink")》是一部哲學史的懸疑小說，已被翻釋的版本有五十三種語言；印刷了2.6亿本，單在[德國已出售了三百萬本](../Page/德國.md "wikilink")。

## 獎項

  - [挪威文學評論獎](../Page/挪威文學評論獎.md "wikilink")（Norwegian Critics Prize for
    Literature）
  - [班卡瑞拉暢銷書作者獎](../Page/班卡瑞拉暢銷書作者獎.md "wikilink")（Premio
    Bancarella）\[1\]

## 作品

  - 《賈德談人生》（，1986年）
  - 《沒有肚臍的小孩》（，1987年）
  - 《青蛙城堡》（，1988年）
  - 《紙牌的祕密》（，1990年）
  - 《[蘇菲的世界](../Page/蘇菲的世界.md "wikilink")》（，1991年）
  - 《依麗莎白的秘密》（，1992年）
  - 《Bibbi Bokkens的魔法圖書館》（，1993年）
  - 《西西莉亞的世界》（，1993年）
  - 《我從外星來》（，1996年）
  - 《瑪雅》（，1999年）
  - 《主教的情人》（，1996年）
  - 《馬戲團的女兒》（，2001年）
  - 《橘子少女》（，2003年）
  - 《將死》（，2006年）
  - 《黃色的Dwarves》（，2006年）
  - 《庇里牛斯山的城堡》（，2008年）
  - 《寂寞傀儡師》（，2018年）

## 參考

<references/>

[Category:挪威小說家](../Category/挪威小說家.md "wikilink")
[Category:挪威作家](../Category/挪威作家.md "wikilink")
[Category:兒童文學作家](../Category/兒童文學作家.md "wikilink")
[Category:奧斯陸大學校友](../Category/奧斯陸大學校友.md "wikilink")
[Category:奧斯陸人](../Category/奧斯陸人.md "wikilink")

1.