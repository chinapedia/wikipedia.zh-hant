**编劇**又稱**編劇家**、**腳本家**，是通過[文字創作出](../Page/文字.md "wikilink")[劇本的作者](../Page/劇本.md "wikilink")，其往往是[電影](../Page/電影.md "wikilink")、[電視劇集的故事源頭](../Page/電視劇.md "wikilink")。

以[舞台演出之編劇可稱為](../Page/舞台.md "wikilink")[劇作家](../Page/劇作家.md "wikilink")，乃因舞台劇台詞字句必須精心雕琢，不能像電視電影台詞那般白話，最著名的是[莎士比亞](../Page/莎士比亞.md "wikilink")、[唐滌生](../Page/唐滌生.md "wikilink")、[曹禺等](../Page/曹禺.md "wikilink")。

## 早期編劇

早期編劇多為話劇或者[歌劇等創作](../Page/歌劇.md "wikilink")，如[百老匯等](../Page/百老汇剧院.md "wikilink")，後由於電視電影技術發展，導致話劇歌劇等的衰落，不少編劇難以維生，便轉行至電影電視行業。

## 編劇工作

編劇往往是[故事以及電影電視劇原創作者](../Page/故事.md "wikilink")，一齣電影往往是由編劇首先創作好劇本，然後交付[導演進行](../Page/導演.md "wikilink")[二次創作](../Page/二次創作.md "wikilink")，而在這段二次創作，是由導演以及編劇一同完成（劇本的修改權歸編劇所有）。

例如美國著名[情景喜劇](../Page/情景喜劇.md "wikilink")《[老友記](../Page/老友記.md "wikilink")》便是如此。

## 電視編劇

電視編劇，也就是為電視劇編寫劇本的人，就編劇而言，新人往往是一組工作，而電視臺的編劇培訓往往為三個月，而要培養一個資深編劇，往往需要平均三至四年的時間，如果領悟力低的話，時間可能更長。[香港地區的編劇新人一般月薪為六千港元](../Page/香港.md "wikilink")。而[中國大陸地區新人則約為一個月三千元](../Page/中國大陸.md "wikilink")。

## 編劇地位

編劇為整部電影或電視劇集的核心與靈魂。不僅故事要靠他，劇本要靠他，演員[對白都要靠他](../Page/對白.md "wikilink")。而劇本的“本”所表示的並不僅僅是本子，還有根本、基本的含義。

不同國家地區，編劇地位亦都不盡相同，就[日本](../Page/日本.md "wikilink")、[美國](../Page/美國.md "wikilink")、華人世界而言，編劇地位是由高往低。編劇在華人地區受重視程度往往不高。

在日本，無論電視或電影，管理整個劇集或電影往往是編劇，相反導演較次。而美國，編劇年薪亦都不低，在一般人年薪最高為五萬美元之際，美國編劇的年薪卻是二十萬美元，而較低的則是華人世界。

## 編劇荒

2002年以前，[中國曾經鬧](../Page/中國.md "wikilink")[編劇荒](../Page/編劇荒.md "wikilink")，但其後獲得紓緩，而目前華人世界的編劇仍然不多。這與一方面華人地區對編劇不夠重視而引起，同時亦令整個華人影視界至今仍鮮有好劇本。

## 劇本的格式

劇本往往由地點（場景，含日間還是夜晚），事件以及環境，角色及對白組成。按場分。

## 編劇轉行

有的編劇會轉為導演或者其他職位。例如著名的[王晶](../Page/王晶_\(導演\).md "wikilink")。

而著名華人導演[李安](../Page/李安.md "wikilink")，在成為導演之前則做了足足六年的編劇。

## 外部連結

  - [香港電台：中港台的編劇短缺問題](http://www.rthk.org.hk/mediadigest/20020115_76_10451.html)

## 相關条目

  - [劇作家](../Page/劇作家.md "wikilink")
  - [粵劇編劇家列表](../Page/粵劇編劇家列表.md "wikilink")

[Category:編劇](../Category/編劇.md "wikilink")
[Category:電影人](../Category/電影人.md "wikilink")
[Category:戲劇相關職業](../Category/戲劇相關職業.md "wikilink")