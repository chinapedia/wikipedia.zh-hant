RBD為一組已解散的[墨西哥合唱團](../Page/墨西哥.md "wikilink")，團中的成員都曾經於墨西哥青春電視劇劇[叛逆中演出](../Page/叛逆.md "wikilink")。今日，他們的成功的橫掃非[西班牙語系的國家](../Page/西班牙語.md "wikilink")，尤其是[巴西](../Page/巴西.md "wikilink")。近三年為拉丁美洲國家為最佳銷售團體，必且在全球銷售超過57,000,000張。
2004年12月初次發行專輯Rebelde，由EMI發行。主要寫歌的為DJ Kafka和Max di
Carlo，事後證明歌曲暢銷程度可與其表演一較高下。前三張單曲（"Rebelde",
"Solo quédate en silencio",
"Sálvame"）在[墨西哥接下排行榜冠軍](../Page/墨西哥.md "wikilink")，第四張單曲"Un
póco de tu amor"拿下了亞軍。Rebelde (Edição
Brasil)，[葡萄牙語專輯](../Page/葡萄牙語.md "wikilink")，為了開拓[巴西市場](../Page/巴西.md "wikilink")，在2005年
發行。雖然沒有[英語版專輯](../Page/英語.md "wikilink")，但銷售依然很好，打進了唱片市場的前100名，拉丁唱片市場的第二。在七月發行了Tour
Generación RBD en
Vivo，為現場專輯，墨西哥巡迴演唱（其中單在[墨西哥城的就有六場](../Page/墨西哥城.md "wikilink")）。在十月發行了第二張[錄音室專輯](../Page/錄音室專輯.md "wikilink")，Nuestro
Amor，在第一周就賣出了160,000張，寫下了在墨西哥新的紀錄。在美國，拿下了拉丁市場銷售的冠軍，整個市場的第88/100名。前四首單曲不是第一名，但都進了前10名，"Nuestro
amor", "Aún hay algo", "Tras de mí" and "Este
corazón"。在美國方面雖然也是成功的，但只有"Aún hay
algo"（最高24），"Este corazón"（最高10），和"Nuestro Amor"（最高6）拿過冠軍。

## 参考

<references/>

  - [Brazilian
    FanSite](https://web.archive.org/web/20070905000334/http://www.live-rbd.com/)
  - <https://web.archive.org/web/20130330155645/http://www.emimusicmexico.com/?artistas=anahi>

[Category:墨西哥演唱團體](../Category/墨西哥演唱團體.md "wikilink")