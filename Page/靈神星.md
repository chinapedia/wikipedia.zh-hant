**靈神星**（**16
Psyche**）為一顆巨大的[小行星](../Page/小行星.md "wikilink")，也很有可能是最大的M-型小行星，質量估計占所有小行星帶天體的0.6%，是由[義大利](../Page/義大利.md "wikilink")[天文學家](../Page/天文學家.md "wikilink")[安尼巴莱·德·加斯帕里斯在](../Page/安尼巴莱·德·加斯帕里斯.md "wikilink")1852年3月17日所發現的，這是他發現的第5顆小行星，後來以希臘神話中的[普叙刻](../Page/普叙刻.md "wikilink")（灵魂的化身）來命名。天文學家給與最早發現的15顆小行星一個速寫的[天文符號](../Page/天文符號.md "wikilink")。但是天文學家[恩克](../Page/恩克.md "wikilink")（Johann
Franz Encke）在1851年建議之後的小行星只使用編號，於是靈神星成為第一顆適用這個方案的小行星\[1\]。

## 特徵

[雷達](../Page/雷達.md "wikilink")\[2\]\[3\]觀測顯示靈神星是完全純粹由[鐵與](../Page/鐵.md "wikilink")[鎳所構成的](../Page/鎳.md "wikilink")。靈神星似乎是一個更大天體裸露的金屬核心。與其他M型小行星不同，靈神星在表面並沒有水或含水礦物存在的跡象，與它是金屬天體的推測相符合\[4\]。目前發現有少量的[輝石現在仍然存在靈神星上](../Page/輝石.md "wikilink")\[5\]。

如果靈神星是一個更大星體的核心殘餘，那麼在相同的軌道上預期會有其他的小行星。不過靈神星並不屬於任何[小行星家族](../Page/小行星家族.md "wikilink")\[6\]。其中一個假設是這次碰撞發生在[太陽系史上非常早期的時代](../Page/太陽系.md "wikilink")，所以其他的殘餘被後來的碰撞磨成碎片，或是軌道被改變。

因為靈神星的體積大到足夠可以計算出它對其他小行星的攝動，所以也可以計算出靈神星的質量。它的[密度相對於金屬而言是比較低的](../Page/密度.md "wikilink")（雖然對這類小行星而言是比較普遍的），這顯示出靈神星擁有一個較高的多孔性，達到30–40%\[7\]，表示它很可能是一個巨大的礫石堆。

靈神星擁有相當規則的表面，它大約是個[橢球體](../Page/橢球體.md "wikilink")。光度變化曲線顯示靈神星極點的[黃道坐標](../Page/黃道坐標.md "wikilink")（ecliptic
coordinate）為黃緯（β）-9°, 黃經（λ）35°或是黃緯-2°,
黃經215°，其中大約有10°的誤差\[8\]。靈神星的[軸傾角](../Page/軸傾角.md "wikilink")（axial
tilt）為95°。

目前靈神星有兩次明顯的掩星被觀測過，一次在[墨西哥觀測到](../Page/墨西哥.md "wikilink")，發生在2002年3月22日，另一次則是在2002年5月16日。靈神星的光度變化也顯示它並不是球狀天體，與雷達的觀測結果及光度變化曲線相符合。

## 參考資料

## 外部連結

  - [由光度變化曲線推測出的靈神星形狀模型](http://www.rni.helsinki.fi/~mjk/IcarPIII.pdf)
    (PDF)
  - [靈神星遮掩恆星TYC 5783-1228的過程](https://web.archive.org/web/20071227093252/http://users.tpg.com.au/users/daveg/astrovideos/videos.htm)
    (Video)
  - [靈神星的軌道模擬與資料](http://ssd.jpl.nasa.gov/sbdb.cgi?sstr=16;orb=1) from
    JPL (Java) /
    [靈神星的星曆表](http://ssd.jpl.nasa.gov/horizons.cgi?find_body=1&body_group=sb&sstr=16)

[Category:小行星带天体](../Category/小行星带天体.md "wikilink")
[Category:1852年发现的小行星](../Category/1852年发现的小行星.md "wikilink")

1.

2.

3.

4.

5.

6.

7.
8.