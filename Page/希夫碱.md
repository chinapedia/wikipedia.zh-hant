[Imine_General_Structure_B.png](https://zh.wikipedia.org/wiki/File:Imine_General_Structure_B.png "fig:Imine_General_Structure_B.png")
**希夫碱**（），或译为**席夫碱**、**施夫碱**、**偶氮甲碱**（Azomethine），是一类在[氮原子上连有](../Page/氮.md "wikilink")[烷基或](../Page/烷基.md "wikilink")[芳基的较稳定的](../Page/芳基.md "wikilink")[亚胺](../Page/亚胺.md "wikilink")，名字来源于化学家[雨果·希夫](../Page/雨果·希夫.md "wikilink")。\[1\]它的通式为R<sub>1</sub>R<sub>2</sub>C=N-R<sub>3</sub>，R<sub>3</sub>为芳基或烷基，而不是[氢](../Page/氢.md "wikilink")。

芳香的希夫碱通常由[芳香](../Page/芳香.md "wikilink")[胺先与](../Page/胺.md "wikilink")[羰基化合物](../Page/羰基化合物.md "wikilink")（[醛](../Page/醛.md "wikilink")、[酮](../Page/酮.md "wikilink")）发生[亲核加成反应生成类似](../Page/亲核加成反应.md "wikilink")[半缩醛的](../Page/半缩醛.md "wikilink")[半胺醛](../Page/半胺醛.md "wikilink")，然后发生[失水反应生成](../Page/失水反应.md "wikilink")[亚胺得到](../Page/亚胺.md "wikilink")。以下是一个典型的希夫碱制取反应：\[2\]

|                                                                                                                                                                                                                                                                                                                                                                                         |
| --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [Schiff_Base.png](https://zh.wikipedia.org/wiki/File:Schiff_Base.png "fig:Schiff_Base.png")）与o-[香草醛](../Page/香草醛.md "wikilink")**2**（1.52 g、10.00 mmol）在[甲醇](../Page/甲醇.md "wikilink")（40.00 mL）中混合，在[室温下静待](../Page/室温.md "wikilink")1小时，会出现橙色[沉淀](../Page/沉淀.md "wikilink")，经[过滤和用](../Page/过滤.md "wikilink")[甲醇洗涤过后](../Page/甲醇.md "wikilink")，即可得到纯净的希夫碱**3**（2.27 g、97.00 %）。\]\] |

希夫碱的生成也是[酶与](../Page/酶.md "wikilink")[底物结合的常见形式](../Page/底物.md "wikilink")，例如，[醛缩酶与](../Page/醛缩酶.md "wikilink")1,6-二磷酸果糖结合，生成3-磷酸甘油醛和磷酸二羟丙酮。

## 参见

  - [亚胺](../Page/亚胺.md "wikilink")

## 参考资料

[pl:Imina](../Page/pl:Imina.md "wikilink")

[S](../Category/官能团.md "wikilink")
[Category:亚胺](../Category/亚胺.md "wikilink")

1.  [International Union of Pure and Applied
    Chemistry](../Page/IUPAC.md "wikilink"). "[Schiff
    base](http://goldbook.iupac.org/S05498.html)". *Compendium of
    Chemical Terminology* Internet edition.
2.  *Synthesis of 2-({\[4-(4-{\[(E)-1-(2-hydroxy-3-methoxyphenyl)
    methylidene\]amino}phenoxy)phenyl\]imino}methyl)- 6 -methoxy phenol*
    A. A. jarrahpour, M. Zarei [Molbank](../Page/Molbank.md "wikilink")
    **2004**, M352
    [1](http://www.mdpi.net/molbank/molbank2004/m0352.htm) [open
    access](../Page/开放获取.md "wikilink") publication.