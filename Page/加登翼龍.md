**加登翼龍**（*Kepodactylus*）為[翼龍目](../Page/翼龍目.md "wikilink")[翼手龍亞目的其中一屬](../Page/翼手龍亞目.md "wikilink")，生活於[侏羅紀晚期](../Page/侏羅紀.md "wikilink")[啟莫里奇階到](../Page/啟莫里奇階.md "wikilink")[提通階](../Page/提通階.md "wikilink")，化石於[美國](../Page/美國.md "wikilink")[科羅拉多州的Garden公園被發現](../Page/科羅拉多州.md "wikilink")，也是牠們屬名的來源。
加登翼龍的[模式標本](../Page/模式標本.md "wikilink")（標號DMNS
21684）包括[頸部](../Page/頸部.md "wikilink")[脊椎骨](../Page/脊椎骨.md "wikilink")、[肱骨](../Page/肱骨.md "wikilink")、數個[指骨](../Page/指骨.md "wikilink")、與[蹠骨](../Page/蹠骨.md "wikilink")，都來自於單一個體。加登翼龍類似[買薩翼龍](../Page/買薩翼龍.md "wikilink")，但是體型較大，翼展約為2.5公尺（8.2呎），而且在脊椎骨與肱骨有空洞\[1\]在最近期的[莫里遜組翼龍類研究中](../Page/莫里遜組.md "wikilink")，加登翼龍被認為是可能的有效屬。\[2\]

## 參考資料

[Category:準噶爾翼龍超科](../Category/準噶爾翼龍超科.md "wikilink")
[Category:侏羅紀翼龍類](../Category/侏羅紀翼龍類.md "wikilink")

1.  Harris, J.D., and Carpenter, K. (1996). [A large pterodactyloid from
    the Morrison Formation (Late Jurassic) of Garden Park,
    Colorado.](https://scientists.dmns.org/sites/kencarpenter/PDFs%20of%20publications/Kepodactylus.pdf)
    *Neues Jahrbuch für Geologie und Paläontologie Monatshefte*
    1996(8):473-484.
2.  King, L.R., Foster, J.R., and Scheetz, R.D. (2006). New pterosaur
    specimens from the Morrison Formation and a summary of the Late
    Jurassic pterosaur record of the Rocky Mountain region. In: Foster,
    J.R., and Lucas, S.G. (eds.). *Paleontology and Geology of the Upper
    Morrison Formation*. New Mexico Museum of Natural History and
    Science Bulletin 36:149-161. ISSN 1524-4156.