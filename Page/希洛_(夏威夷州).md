**希洛**
（）是[美國](../Page/美國.md "wikilink")[夏威夷州的一個無建制城市](../Page/夏威夷州.md "wikilink")\[1\]，位於[夏威夷島東北岸](../Page/夏威夷島.md "wikilink")。是[夏威夷縣縣治](../Page/夏威夷縣_\(夏威夷州\).md "wikilink")，並受縣的管轄。2000年人口40,759人。這個數字於2010年上升至43,263人。\[2\]\[3\]

[HILO,_Big_Island_Hawaii.jpg](https://zh.wikipedia.org/wiki/File:HILO,_Big_Island_Hawaii.jpg "fig:HILO,_Big_Island_Hawaii.jpg")

## 地理

希洛的座標為（19.705520,-155.085918）。根據[2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，本城面積為，其中為陸地，為水域。\[4\]

## 參考文獻

[H](../Category/夏威夷州城市.md "wikilink")

1.  "[Hilo CDP,
    Hawaii](http://factfinder.census.gov/servlet/MapItDrawServlet?geo_id=16000US1514650&_bucket_id=50&tree_id=420&context=saff&_lang=en&_sse=on)
    ." *[U.S. Census Bureau](../Page/U.S._Census_Bureau.md "wikilink")*.
    Retrieved on May 21, 2009.
2.  [2010 census factfinder2](http://factfinder2.census.gov/)
3.  [US Census Bureau – 2010 Population Finder – Hilo CDP
    -](http://www.census.gov/popfinder)
4.