**國際LGBTI聯合會**（**International Lesbian, Gay, Bisexual, Trans and
Intersex
Association**）是一個促進世界各地區[女同性戀](../Page/女同性戀.md "wikilink")、[男同性戀](../Page/男同性戀.md "wikilink")、[雙性戀](../Page/雙性戀.md "wikilink")、[跨性別和](../Page/跨性別.md "wikilink")[雙性人群體共同爭取](../Page/雙性人.md "wikilink")[平權的國際組織](../Page/平等主義.md "wikilink")。該組織於1978年成立，為**國際同性戀聯合會**（**International
Gay Association，IGA**），在1986年改名為**國際男女同性戀聯合會**（**International Lesbian
and Gay
Association，ILGA**）。ILGA現在擁有超過1100個團體會員，分佈在六大地區的110多個國家\[1\]，組織願景為確保任何人不分其[性傾向](../Page/性傾向.md "wikilink")、[性別認同](../Page/性別認同.md "wikilink")、[性別表達和](../Page/性別氣質.md "wikilink")[性徵](../Page/性別.md "wikilink")，都能享有[自由](../Page/自由.md "wikilink")[平等的](../Page/法律之前人人平等.md "wikilink")[人權](../Page/人權.md "wikilink")，並致力於實現這樣一種全球[正義和](../Page/正義.md "wikilink")[平權的世界](../Page/平等主義.md "wikilink")\[2\]。

## 介紹

ILGA在1978年8月8日，以「國際同性戀聯合會」為名，由來自澳大利亞、英格蘭、丹麥、法國、北愛爾蘭、愛爾蘭、義大利、荷蘭、蘇格蘭和美國等同性戀組織代表，合立於英國[考文垂市](../Page/考文垂.md "wikilink")，並在1986年更名為「國際男女同性戀聯合會」。IGA成立時的目標如下\[3\]：

:\* 最大化各同性戀組織的效能，採取國際層級的政治行動結盟爭取同性戀權益，尤其要對各國政府和國際機構協力施壓

:\*
設置資訊中心，在同性戀組織之間交流相關資訊，以促進對於同性戀受[壓迫情況的瞭解和辨識國際政治的角力場](../Page/壓迫.md "wikilink")

今日的ILGA則遵循其[憲章和](../Page/憲法.md "wikilink")[議事程序](../Page/議事程序.md "wikilink")，作為執行業務和成員集會的依據\[4\]\[5\]。根據憲章，ILGA及其成員應致力於\[6\]：

:\*為女同性戀、男同性戀、雙性戀、跨性別和雙性人的平等奮鬥，使其獲得免受所有形式歧視的自由

:\*為所有因為被認為或實際為該性傾向、性別認同、性別表達或而受到歧視的人，能獲得平等而奮鬥

:\*促進人權和基本自由的[普世重視和遵守](../Page/普世價值.md "wikilink")，包含消除所有形式的歧視以及瞭解下列[國際人權文件](../Page/國際人權文書.md "wikilink")的具體條款

在成立之後，ILGA立刻關切希臘和蘇聯具性傾向歧視性質的法律，向[歐洲委員會提交](../Page/歐洲委員會.md "wikilink")[除罪化材料](../Page/除罪化.md "wikilink")，要求[歐洲人權公約增添免受性傾向](../Page/歐洲人權公約.md "wikilink")[歧視的保護](../Page/歧視.md "wikilink")，鼓勵[國際特赦組織救助因為歧視性法律遭到監禁的同性戀者](../Page/國際特赦組織.md "wikilink")。ILGA亦積極要求[世界衛生組織將同性戀從](../Page/世界衛生組織.md "wikilink")[國際疾病與相關健康問題統計分類移除](../Page/國際疾病與相關健康問題統計分類.md "wikilink")，並申請成為具[聯合國諮詢地位的](../Page/聯合國.md "wikilink")[NGO](../Page/NGO.md "wikilink")\[7\]。

ILGA的主要業務為在[聯合國和其他國際組織內](../Page/聯合國.md "wikilink")，代表LGBTI公民社會發聲，支援其組織成員提昇和保護人權，並透過倡議和研究向機構、政府、媒體、以及[公民社會發布消息和喚起關注](../Page/公民社會.md "wikilink")\[8\]。

## 架構

ILGA由分別來自六個地區分會的1100多個團體會員聯合組成，該六大地區為：亞洲分會（ILGA Asia）、歐洲分會（ILGA
Europe）、拉美和加勒比分會（ILGA LAC）、北美州分會（ILGA North America）、大洋洲分會（ILGA
Oceania）和泛非洲分會（Pan Africa
ILGA）。組織營運資金來自會員繳納的會費，以及各國政府和私人基金會的捐贈\[9\]。團體會員，除各分會舉辦的區域年會（Regional
Conferences）之外，還可參與ILGA世界大會（World Conferences）。

ILGA世界大會是最高決策單位。在世界大會中，各個團體會員、執行董事會、女性秘書處、跨性別秘書處、雙性人秘書處、區域執行董事會以及各盟友組織，聚集在一起，進行決策、交流、背書、提案、修改組織憲章以及選舉秘書長和各秘書代表等事務。聚會不僅是事務決策場所，同時也是一個供社運人士分享彼此運動經驗，合作結盟的重要場合\[10\]。然而世界大會或區域年會，有時會因為遭當地激進組織攻擊或者資金缺乏，而不得不停辦或延期\[11\]\[12\]。

ILGA世界大會的舉辦地點和年份，如下\[13\]\[14\]：

:\*
[阿姆斯特丹](../Page/阿姆斯特丹.md "wikilink")/[卑爾根](../Page/卑爾根.md "wikilink")
(1979)

:\* [巴塞隆納](../Page/巴塞隆納.md "wikilink") (1980)

:\* [杜林](../Page/都灵.md "wikilink") (1981)

:\* [華盛頓特區](../Page/華盛頓哥倫比亞特區.md "wikilink") (1982)

:\* [維也納](../Page/維也納.md "wikilink") (1983)

:\* [赫爾辛基](../Page/赫爾辛基.md "wikilink") (1984)

:\* [多倫多](../Page/多倫多.md "wikilink") (1985)

:\* [哥本哈根](../Page/哥本哈根.md "wikilink") (1986)

:\* [科隆](../Page/科隆.md "wikilink") (1987)

:\* [奧斯陸](../Page/奧斯陸.md "wikilink") (1988)

:\* [維也納](../Page/維也納.md "wikilink") (1989)

:\* [斯德哥尔摩](../Page/斯德哥尔摩.md "wikilink") (1990)

:\* [阿卡普爾科](../Page/阿卡普爾科.md "wikilink") (1991)

:\* [巴黎](../Page/巴黎.md "wikilink") (1992)

:\* [巴塞隆納](../Page/巴塞隆納.md "wikilink") (1993)

:\* [紐約](../Page/紐約.md "wikilink") (1994)

:\* [里約熱內盧](../Page/里約熱內盧.md "wikilink") (1995)

:\* [科隆](../Page/科隆.md "wikilink") (1997)

:\* [約翰尼斯堡](../Page/約翰尼斯堡.md "wikilink") (1999)

:\* [羅馬](../Page/羅馬.md "wikilink") (2000)

:\* [奧克蘭](../Page/奥克兰_\(加利福尼亚州\).md "wikilink") (2001)

:\* [馬尼拉](../Page/馬尼拉.md "wikilink") (2003)

:\* [日內瓦](../Page/日內瓦.md "wikilink") (2006)

:\* [維也納](../Page/維也納.md "wikilink") (2008)

:\* [聖保羅](../Page/聖保羅_\(巴西\).md "wikilink") (2010)

:\* [斯德哥尔摩](../Page/斯德哥尔摩.md "wikilink") (2012)

:\* [墨西哥城](../Page/墨西哥城.md "wikilink") (2014)

:\* [曼谷](../Page/曼谷.md "wikilink") (2016)

:\* [威靈頓](../Page/威靈頓.md "wikilink") (2019)
ILGA的總辦公室設在[日內瓦](../Page/日內瓦.md "wikilink")，於[香港](../Page/香港.md "wikilink")、[布宜諾](../Page/布宜諾斯艾利斯.md "wikilink")、[約翰尼斯堡三地設有分區辦公室](../Page/約翰尼斯堡.md "wikilink")，負責專案計畫和抗爭行動支援，進行外交施壓，提供資訊以及和國際組織與媒體合作，以喚起公眾和政府重視LGBTI人群受到歧視的處境和案例\[15\]。身為最大的LGBTI全球聯盟，ILGA在聯合國各論壇為其[政治議程發聲](../Page/政治議程.md "wikilink")，協助團體會員赴聯合國進行人權報告\[16\]，並於每年度生產一份《國家助長恐同報告書》和《性傾向法律地圖》。

## 爭議

ILGA在1993年成為獲得[聯合國](../Page/聯合國.md "wikilink")[ECOSOC諮詢地位的NGO組織](../Page/ECOSOC.md "wikilink")，然而一年之後諮詢地位遭到撤銷。由於提倡[未成年性行為的](../Page/最低合法性交年齡.md "wikilink")[北美男人男童戀愛協會擁有ILGA會員資格](../Page/北美男人男童戀愛協會.md "wikilink")，美國國會議員[傑西·赫爾姆斯利用這點提案施壓聯合國](../Page/傑西·亞歷山大·赫爾姆斯.md "wikilink")\[17\]。ILGA對此進行澄清：在1990年，ILGA就通過了「每位兒童都有免於[性剝削和免受虐待權利](../Page/性剝削.md "wikilink")」的聲明，ILGA不提倡任何形式的未成年性行為，也從未如此作過\[18\]。ILGA盡可能驅逐所有這類偏離ILGA目標的會員，並加強了申請入會的審核程序，但聯合國最終仍撤銷其諮詢地位。直到2006年，才得以取得諮詢地位，而在經歷多次否決後，ILGA終於在2011年重新取回了諮詢地位\[19\]。

2011年7月25日，联合国经济与社会理事会（ECOSOC）在日内瓦召开了本年度的实质性全体会议，与会各国以29票赞成、14票反对、5票弃权的结果投票通过了ILGA成为非政府组织諮詢會員的申请。

投票赞成的国家有：印度、意大利、日本、拉脱维亚、马耳他、墨西哥、蒙古、尼加拉瓜、挪威、秘鲁、韩国、斯洛伐克、西班牙、瑞士、乌克兰、英国、美国、委内瑞拉、阿根廷、澳大利亚、比利时、加拿大、智利、厄瓜多尔、爱沙尼亚、芬兰、法国、德国、匈牙利。

反对的国家包括：[孟加拉国](../Page/孟加拉国.md "wikilink")、[中華人民共和國](../Page/中華人民共和國.md "wikilink")、[喀麦隆](../Page/喀麦隆.md "wikilink")、[埃及](../Page/埃及.md "wikilink")、[加纳](../Page/加纳.md "wikilink")、[伊拉克](../Page/伊拉克.md "wikilink")、[摩洛哥](../Page/摩洛哥.md "wikilink")、[纳米比亚](../Page/纳米比亚.md "wikilink")、[巴基斯坦](../Page/巴基斯坦.md "wikilink")、[卡塔尔](../Page/卡塔尔.md "wikilink")、[俄罗斯](../Page/俄罗斯.md "wikilink")、[沙特阿拉伯](../Page/沙特阿拉伯.md "wikilink")、[塞内加尔](../Page/塞内加尔.md "wikilink")、[赞比亚](../Page/赞比亚.md "wikilink")。

## 參見

  - [LGBT權利運動](../Page/LGBT權利運動.md "wikilink")

  - [各地LGBT權利](../Page/各地LGBT權利.md "wikilink")

  - [聯合國性傾向與性別認同議題](../Page/聯合國性傾向與性別認同議題.md "wikilink")

  -
  - [日惹原則](../Page/日惹原則.md "wikilink")

## 注释

## 参考文献

## 外部链接

  - [國際LGBTI聯合會](http://ilga.org/)

[Category:LGBT組織](../Category/LGBT組織.md "wikilink")
[Category:1978年建立的組織](../Category/1978年建立的組織.md "wikilink")
[Category:非政府间国际组织](../Category/非政府间国际组织.md "wikilink")

1.

2.

3.
4.
5.

6.
7.

8.

9.
10.

11.

12.

13.
14.

15.
16.
17.

18.

19.