**經濟發展及勞工局**（[英文](../Page/英文.md "wikilink")：**E**conomic **D**evelopment
and **L**abour
**B**ureau，[縮寫](../Page/縮寫.md "wikilink")：**EDLB**）是[香港特別行政區政府的前](../Page/香港特別行政區政府.md "wikilink")[決策局](../Page/三司十一局.md "wikilink")，專責[香港有關](../Page/香港.md "wikilink")[經濟發展及](../Page/經濟.md "wikilink")[勞工事務](../Page/勞工.md "wikilink")。該局唯一一任局長為為[葉澍堃](../Page/葉澍堃.md "wikilink")。

## 歷史

1983年2月，政府重組負責教育及社會事務的職位。**社會事務司**轄下的勞工事務撥歸[教育統籌司管轄](../Page/教育統籌司.md "wikilink")。

[經濟局成立於](../Page/經濟局.md "wikilink")1997年7月1日，前身是[香港政府的](../Page/香港殖民地時期#香港政府.md "wikilink")**[布政司署經濟科](../Page/布政司署.md "wikilink")**，首長是*經濟司*。

2002年7月1日，時任[香港特別行政區行政長官的](../Page/香港特別行政區行政長官.md "wikilink")[董建華推行](../Page/董建華.md "wikilink")[主要官員問責制](../Page/主要官員問責制.md "wikilink")，並將各[政策局重組為](../Page/政策局.md "wikilink")[十一局](../Page/三司十一局.md "wikilink")，將負責[勞工事務的](../Page/勞工.md "wikilink")[勞工署由](../Page/香港勞工處.md "wikilink")[教育統籌局撥歸](../Page/教育統籌局.md "wikilink")[經濟局](../Page/經濟局.md "wikilink")，並改名為**經濟發展及勞工局**。

2003年7月1日，經濟發展及勞工局轄下的*勞工科*和**[勞工處](../Page/香港勞工處.md "wikilink")**合併。

2007年7月1日起決策局重組，經濟發展及勞工事務分家，分別由[商務及經濟發展局](../Page/商務及經濟發展局.md "wikilink")、[勞工及福利局負責](../Page/勞工及福利局.md "wikilink")。

## 歷任首長

<table>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><strong>經濟</strong></p></td>
<td style="text-align: center;"><p><strong>勞工</strong></p></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/經濟司.md" title="wikilink">經濟司</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><ul>
<li><a href="../Page/鍾信.md" title="wikilink">鍾信</a>（1973年11月—1976年9月）</li>
<li><a href="../Page/謝法新.md" title="wikilink">謝法新</a>（1976年9月—1982年9月）</li>
</ul></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/經濟司.md" title="wikilink">經濟司</a></p></td>
<td style="text-align: center;"><p><a href="../Page/教育及人力統籌司.md" title="wikilink">教育及人力統籌司</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><ul>
<li><a href="../Page/翟克誠.md" title="wikilink">翟克誠</a> （1982年9月—1986年1月）</li>
<li><a href="../Page/易誠禮.md" title="wikilink">易誠禮</a> （1986年5月—1987年2月）</li>
<li><a href="../Page/陳方安生.md" title="wikilink">陳方安生</a> （1987年3月—1993年4月）</li>
<li><a href="../Page/蕭炯柱.md" title="wikilink">蕭炯柱</a> （1993年—1996年）</li>
<li><a href="../Page/葉澍堃.md" title="wikilink">葉澍堃</a> （1996年6月—1997年6月30日）</li>
</ul></td>
<td style="text-align: center;"><ul>
<li><a href="../Page/韓達誠.md" title="wikilink">韓達誠</a>（1983年－1986年）</li>
<li><a href="../Page/布立之.md" title="wikilink">布立之</a>（1986年－1989年）</li>
</ul></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/教育統籌司.md" title="wikilink">教育統籌司</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><ul>
<li><a href="../Page/楊啟彥.md" title="wikilink">楊啟彥</a>（1989年－1991年）</li>
<li><a href="../Page/陳祖澤.md" title="wikilink">陳祖澤</a>（1991年－1993年）</li>
<li><a href="../Page/梁文建.md" title="wikilink">梁文建</a>（1993年－1994年）</li>
<li><a href="../Page/王永平_(香港).md" title="wikilink">王永平</a>（1995年8月－1997年6月30日）</li>
</ul></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/經濟局局長.md" title="wikilink">經濟局局長</a></p></td>
<td style="text-align: center;"><p><a href="../Page/教育統籌局局長.md" title="wikilink">教育統籌局局長</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><ul>
<li><a href="../Page/葉澍堃.md" title="wikilink">葉澍堃</a> （1997年7月1日—2000年6月12日）</li>
<li><a href="../Page/李淑儀.md" title="wikilink">李淑儀</a> （2000年7月13日—2002年6月30日）</li>
</ul></td>
<td style="text-align: center;"><ul>
<li><a href="../Page/王永平_(香港).md" title="wikilink">王永平</a>　（1997年7月1日－2000年7月2日）</li>
<li><a href="../Page/羅范椒芬.md" title="wikilink">羅范椒芬</a>（2000年7月3日－2002年6月30日）</li>
</ul></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/經濟發展及勞工局局長.md" title="wikilink">經濟發展及勞工局局長</a></p></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><ul>
<li><a href="../Page/葉澍堃.md" title="wikilink">葉澍堃</a> （2002年7月1日—2007年6月30日）</li>
</ul></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="../Page/經濟發展及勞工局常任秘書長_(經濟發展).md" title="wikilink">經濟發展及勞工局常任秘書長（經濟發展）</a></p></td>
<td style="text-align: center;"><p><a href="../Page/經濟發展及勞工局常任秘書長_(勞工)_兼勞工處處長.md" title="wikilink">經濟發展及勞工局常任秘書長 (勞工) 兼勞工處處長</a></p></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><ul>
<li><a href="../Page/李淑儀.md" title="wikilink">李淑儀</a>（2002年7月—2006年4月）</li>
<li><a href="../Page/鄭汝樺.md" title="wikilink">鄭汝樺</a>（2006年4月—2007年6月30日）</li>
</ul></td>
<td style="text-align: center;"><ul>
<li><a href="../Page/張建宗.md" title="wikilink">張建宗</a>（2002年7月1日－2007年3月）</li>
<li><a href="../Page/鄧國威.md" title="wikilink">鄧國威</a>（2007年4月－2007年6月30日）</li>
</ul></td>
</tr>
</tbody>
</table>

## 轄下部門

  - [民航處](../Page/民航處.md "wikilink")
  - [勞工處](../Page/勞工處.md "wikilink")
  - [海事處](../Page/海事處.md "wikilink")
  - [旅遊事務署](../Page/旅遊事務署.md "wikilink")
  - [旅行代理商註冊處](../Page/旅行代理商註冊處.md "wikilink")
  - [香港郵政](../Page/香港郵政.md "wikilink")
  - [香港天文台](../Page/香港天文台.md "wikilink")

## 參見章節

  - [三司十二局](../Page/三司十二局.md "wikilink")

## 注释

## 外部連結

  - [香港特別行政區政府
    經濟發展及勞工局](https://web.archive.org/web/20050525235424/http://www.edlb.gov.hk/)

[經濟發展及勞工局](../Category/香港政府前部門機構.md "wikilink")