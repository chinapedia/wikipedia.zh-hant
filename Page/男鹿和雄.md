**男鹿和雄**（）是出身[日本](../Page/日本.md "wikilink")[秋田縣的男性](../Page/秋田縣.md "wikilink")[美術監督](../Page/美術指導.md "wikilink")，曾參與多部[川尻善昭執導或](../Page/川尻善昭.md "wikilink")[吉卜力工作室動畫作品的背景美術製作](../Page/吉卜力工作室.md "wikilink")。

## 經歷

自秋田縣立角館高校畢業後，男鹿和雄在1972年進入動畫美術公司的[小林製片工作](../Page/小林製片.md "wikilink")，中間曾有一段時間轉職從事看板設計方面的職務，之後1977年則再度回到小林製片參與動畫的背景繪製。

後續男鹿和雄轉到[MADHOUSE動畫公司下就職](../Page/MADHOUSE.md "wikilink")。在MADHOUSE期間參與由[川尻善昭改編自](../Page/川尻善昭.md "wikilink")[菊地秀行小說](../Page/菊地秀行.md "wikilink")《[魔界都市〈新宿〉](../Page/魔界都市〈新宿〉.md "wikilink")》同名動畫中，曾影響了男鹿和雄的美術表現技法\[1\]。

1987年在友人的介紹下，男鹿和雄加入了[吉卜力工作室](../Page/吉卜力工作室.md "wikilink")。在吉卜力工作室男鹿和雄首次擔當的事務是負責由[宮崎駿執導動畫](../Page/宮崎駿.md "wikilink")《[龍貓](../Page/龍貓.md "wikilink")》中的森林風景繪製，當時男鹿和雄所畫出自然風景皆獲得宮崎駿與[高畑勳的好評](../Page/高畑勳.md "wikilink")\[2\]\[3\]。

之後男鹿和雄參與了吉卜力數部動畫的美術、背景事務，並在2006年發表了第一部個人執導的動畫《[種山原之夜](../Page/種山原之夜.md "wikilink")》\[4\]，以及在2007至2010年期間在日本[東京都](../Page/東京都.md "wikilink")、[愛知縣等九個縣市巡迴展開個人作品美術展](../Page/愛知縣.md "wikilink")\[5\]。

男鹿和雄現今以位於[八王子市自宅內的畫室作為主要工作點](../Page/八王子市.md "wikilink")，除仍持續接手吉卜力動畫事務外，也曾與吉卜力另一人員[武重洋二參與由](../Page/武重洋二.md "wikilink")[細田守執導的](../Page/細田守.md "wikilink")《[跳躍吧！時空少女](../Page/跳躍吧！時空少女.md "wikilink")》、《[夏日大作戰](../Page/夏日大作戰.md "wikilink")》等動畫作品的美術背景繪製。

## 參與作品

### 執導動畫

  - [種山原之夜](../Page/種山原之夜.md "wikilink")（2006年）

### 電視動畫

  - [小木偶](../Page/小木偶_\(動畫\).md "wikilink")（1972年）：背景
  - [傻勁兒青蛙](../Page/傻勁兒青蛙.md "wikilink")（1972年）：背景
  - [魔投手](../Page/魔投手.md "wikilink")（1973年）：背景
  - [山林小獵人](../Page/山林小獵人.md "wikilink")（1974年）：背景
  - [小老鼠大冒險](../Page/小老鼠大冒險.md "wikilink")（1975年）：背景、美術設定
  - [咪咪流浪記](../Page/咪咪流浪記.md "wikilink")（1977年）：背景、美術設定
  - [金銀島](../Page/金銀島_\(動畫\).md "wikilink")（1978年）：副美術監督
  - [安妮的日記](../Page/安妮的日記_\(動畫\).md "wikilink")（1979年）：背景
  - [少爺們](../Page/少爺們.md "wikilink")（1980年）：背景
  - [小拳王第二部](../Page/小拳王.md "wikilink")（1980年）：美術
  - [夏服的少女們](../Page/夏服的少女們.md "wikilink")（1988年）：美術監督
  - [奇幻兒童](../Page/奇幻兒童.md "wikilink")（2004年）：背景

### 劇場動畫

  - [熊貓家族 雨中的馬戲團](../Page/熊貓家族_雨中的馬戲團.md "wikilink")（1973年）：背景
  - [網球甜心](../Page/網球甜心.md "wikilink")（1979年）：背景
  - [誠仔劇場版](../Page/誠仔.md "wikilink")（1980年）：美術
  - [小拳王劇場版](../Page/小拳王.md "wikilink")（1980年）：美術設定
  - [神奇獨角馬](../Page/神奇獨角馬.md "wikilink")（1981年）：美術
  - [宇宙海賊眼鏡蛇](../Page/宇宙海賊眼鏡蛇.md "wikilink")（1982年）：副美術監督
  - [幻魔大戰](../Page/幻魔大戰.md "wikilink")（1983年）：美術
  - [赤足天使](../Page/赤足天使.md "wikilink")（1983年）：美術監督
  - [神奇獨角馬 魔法之島](../Page/神奇獨角馬_魔法之島.md "wikilink")（1983年）：背景
  - [銀河戰士](../Page/銀河戰士.md "wikilink")（1984年）：背景
  - [神劍](../Page/神劍_\(1985年動畫\).md "wikilink")（1985年）：美術
  - [時空的旅人](../Page/時空的旅人.md "wikilink")（1986年）：美術監督
  - [赤足天使2](../Page/赤足天使2.md "wikilink")（1986年）：美術
  - [妖獸都市](../Page/妖獸都市.md "wikilink")（1987年）：美術監督
  - [怒吼吧小狗](../Page/怒吼吧小狗.md "wikilink")（1987年）：美術
  - [龍貓](../Page/龍貓.md "wikilink")（1988年）：美術
  - [魔女宅急便](../Page/魔女宅急便.md "wikilink")（1989年）：背景
  - [兒時的點點滴滴](../Page/兒時的點點滴滴.md "wikilink")（1991年）：美術監督
  - [福星小子 Always My
    Darling](../Page/福星小子_Always_My_Darling.md "wikilink")（1991年）：背景
  - [紅豬](../Page/紅豬.md "wikilink")（1992年）：美術
  - [獸兵衛忍風帖](../Page/獸兵衛忍風帖.md "wikilink")（1993年）：背景
  - [平成狸合戰](../Page/平成狸合戰.md "wikilink")（1994年）：美術監督
  - [On Your Mark](../Page/On_Your_Mark.md "wikilink")（1995年）：背景
  - [心之谷](../Page/心之谷.md "wikilink")（1995年）：背景
  - [魔法公主](../Page/魔法公主.md "wikilink")（1997年）：美術
  - [神劍闖江湖劇場版](../Page/神劍闖江湖.md "wikilink")（199年）：背景
  - [吸血鬼獵人D](../Page/吸血鬼獵人D#劇場版.md "wikilink")（2000年）：背景
  - [神隱少女](../Page/神隱少女.md "wikilink")（2001年）：背景
  - [犬夜叉跨越時代的思念](../Page/犬夜叉#劇場版.md "wikilink")（2001年）：背景
  - [貓的報恩](../Page/貓的報恩.md "wikilink")（2002年）：背景
  - [霍爾的移動城堡](../Page/霍爾的移動城堡.md "wikilink")（2004年）：背景
  - [跳躍吧！時空少女](../Page/跳躍吧！時空少女.md "wikilink")（2006年）：背景
  - [地海戰記](../Page/地海戰記.md "wikilink")（2006年）：背景
  - [時空英豪: 復仇之路](../Page/時空英豪:_復仇之路.md "wikilink")（2007年）：背景
  - [崖上的波妞](../Page/崖上的波妞.md "wikilink")（2008年）：背景
  - [川之光](../Page/川之光.md "wikilink")（2009年）：背景
  - [夏日大作戰](../Page/夏日大作戰.md "wikilink")（2009年）：背景
  - [借物少女艾莉緹](../Page/借物少女艾莉緹.md "wikilink")（2010年）：背景
  - [給小桃的信](../Page/給小桃的信.md "wikilink")（2012年）：背景
  - [輝耀姬物語](../Page/輝耀姬物語.md "wikilink")（2013年）：美術
  - [回憶中的瑪妮](../Page/回憶中的瑪妮.md "wikilink")（2014年）：背景
  - [怪物的孩子](../Page/怪物的孩子.md "wikilink")（2015年）：背景

### [OVA](../Page/OVA.md "wikilink")

  - [魔界都市〈新宿〉](../Page/魔界都市〈新宿〉.md "wikilink")（1988年）：背景
  - [惡魔的新娘 蘭之組曲](../Page/惡魔的新娘_蘭之組曲.md "wikilink")（1989年）：背景
  - [午夜之眼](../Page/午夜之眼.md "wikilink")（1989年）：背景美術

## 插圖提供

  - ：1993年、[C·W·尼可著](../Page/C·W·尼可.md "wikilink")

  - ：1987年

  - ：1998年、[吉永小百合著](../Page/吉永小百合.md "wikilink")

  - ：2000年、吉永小百合著

  - ：2000年、吉永小百合著

  - ：2002年、[山下景秋著](../Page/山下景秋.md "wikilink")

  - ：2003年、[山本素石著](../Page/山本素石.md "wikilink")

  - ：2004年、[藤丸麻紀著](../Page/藤丸麻紀.md "wikilink")

  - ：2008年、[野坂昭如著](../Page/野坂昭如.md "wikilink")

## 個人著書‧畫集‧圖錄

  - ：1996年

  - ：2005年

  - ：2006年

  - ：2007年

  - ：2009年

## 個人展覽

  - ：2007～2010年

## 外部連結

  - [男鹿和雄的個人美術展資訊](http://www.ntv.co.jp/oga/)

## 参考文献

[Category:日本插畫家](../Category/日本插畫家.md "wikilink")
[Category:日本動畫導演](../Category/日本動畫導演.md "wikilink")
[Category:秋田縣出身人物](../Category/秋田縣出身人物.md "wikilink")
[Category:吉卜力工作室人物](../Category/吉卜力工作室人物.md "wikilink")
[Category:動畫美術指導](../Category/動畫美術指導.md "wikilink")

1.
2.
3.
4.
5.