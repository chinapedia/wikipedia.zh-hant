**羅納德·E·諾伊曼**（，），[美國](../Page/美國.md "wikilink")[外交官](../Page/外交官.md "wikilink")，現任美國駐阿富汗大使。他曾在阿爾及利亞（1994-97年）和巴林（2001-04年）擔任大使之職。其父[羅伯特·格哈特·諾伊曼亦是外交官](../Page/羅伯特·格哈特·諾伊曼.md "wikilink")，因此羅納德的有部分童年在當地度過。他將其姓氏讀成Newmann，有時其姓亦以此串出。

1970年起在美國政府工作。在[塞內加爾工作過後](../Page/塞內加爾.md "wikilink")，他成為[中東](../Page/中東.md "wikilink")（特別在[波斯灣的](../Page/波斯灣.md "wikilink")）專家。1988年開始，他在中東負責管理[庫爾德難民](../Page/庫爾德.md "wikilink")，亦是海灣北部（Northern
Gulf）事務的指揮。

1994年成為[阿爾及尼亞大使](../Page/阿爾及尼亞.md "wikilink")，部分原因是他有中東事務的經驗，和曾在那兒工作三年。隨後亦成為代理近東事務助理秘書（Deputy
Assistant Secretary of State for Near Eastern Affairs）。

2000年被選為[巴林大使](../Page/巴林.md "wikilink")，但參議院並沒有即時核准，在這段期間他捲入一件機密檔案錯誤處理的事件。但[小布殊在](../Page/小布殊.md "wikilink")2001年上任為新總統後，便永久不追究其失誤，並批准他上任。

他任巴林大使期間，大使館在2002年4月受親[巴勒斯坦分子的攻擊](../Page/巴勒斯坦.md "wikilink")，一度關閉。在該次事件，無人受傷，但建築物受破壞，車輛遭焚燒。

2004年，他離開巴林，成為美國駐[伊拉克的政治顧問](../Page/伊拉克.md "wikilink")，直到2005年轉任阿富汗大使。

除了[英語之外](../Page/英語.md "wikilink")，他能說[阿拉伯語](../Page/阿拉伯語.md "wikilink")、[法語和部分](../Page/法語.md "wikilink")[波斯語](../Page/波斯語.md "wikilink")。

## 閑話

2005年成為阿富汗大使的諾伊曼，跟1966年為阿富汗大使的父親，是美國現代州政府制度成立以來首對父子曾在同一國家作大使的人。在之前則有美國總統[约翰·亚当斯和](../Page/约翰·亚当斯.md "wikilink")[約翰·昆西·亞當斯](../Page/約翰·昆西·亞當斯.md "wikilink")，兩人都曾為[大不列顛大使](../Page/大不列顛.md "wikilink")。

## 參考

翻譯自本頁英文版，以下為其參考：

  - "U.S. works behind scenes to ease tension in Algeria", [The Ottawa
    Citizen](../Page/The_Ottawa_Citizen.md "wikilink"), (April 23, 1994)
  - "Diplomatic Exit Some Way Off, Says Young", [Gulf Daily
    News](../Page/Gulf_Daily_News.md "wikilink"), (February 27, 2000)
  - "US Envot to Stay Till Summer", [Gulf Daily
    News](../Page/Gulf_Daily_News.md "wikilink"), (September 7, 2000)
  - "Envoy's Nomination Hits a Security Snag", [Gulf Daily
    News](../Page/Gulf_Daily_News.md "wikilink"), (September 29, 2000)
  - "Embassy Row", [The Washington
    Times](../Page/The_Washington_Times.md "wikilink"), (July 24, 2001)
  - "U.S. Embassy in Bahrain Remains Closed", [Gulf Daily
    News](../Page/Gulf_Daily_News.md "wikilink"), (April 7, 2002)
  - "Bahrain: King Reviews US Ties with Outgoing Ambassador", [Global
    News Wire](../Page/Global_News_Wire.md "wikilink"), (June 1, 2004)

[Category:美國外交官](../Category/美國外交官.md "wikilink")
[Category:美國駐阿富汗大使](../Category/美國駐阿富汗大使.md "wikilink")
[Category:美國駐巴林大使](../Category/美國駐巴林大使.md "wikilink")
[Category:美國駐阿爾及利亞大使](../Category/美國駐阿爾及利亞大使.md "wikilink")