**M108**（**[NGC](../Page/NGC天体.md "wikilink")
3556**）是一个位于[大熊座的测向](../Page/大熊座.md "wikilink")[地球的](../Page/地球.md "wikilink")[漩涡星系](../Page/漩涡星系.md "wikilink")。其[星等](../Page/星等.md "wikilink")：+10

## 参见

  - [M97](../Page/M97.md "wikilink")
  - [大熊座](../Page/大熊座.md "wikilink")
  - [梅西耶天体](../Page/梅西耶天体.md "wikilink")
  - [梅西耶天体列表](../Page/梅西耶天体列表.md "wikilink")
  - [深空天体](../Page/深空天体.md "wikilink")
  - [NGC天体列表](../Page/NGC天体列表.md "wikilink")

## 参考文献

<div class="references-small">

<references />

</div>

[108](../Category/梅西耶天體.md "wikilink")
[Category:大熊座NGC天体](../Category/大熊座NGC天体.md "wikilink")
[Category:螺旋星系](../Category/螺旋星系.md "wikilink")