是[日本](../Page/日本.md "wikilink")[世嘉在](../Page/世嘉.md "wikilink")1988年推出的16-bit家用遊戲機，1989年在美國推出。

Mega
Drive在全球共售出3075万台。此外[Tectoy在巴西售出大约](../Page/Tectoy.md "wikilink")300万台授权的兼容机。Majesco估計在美国共售出150万台授权的兼容机。

## 历史

[JP_MegaDrive_Logo.gif](https://zh.wikipedia.org/wiki/File:JP_MegaDrive_Logo.gif "fig:JP_MegaDrive_Logo.gif")

1988年10月29日，世嘉在日本地区发行了Mega
Drive。尽管推出之时，被一周前任天堂发行的《[超級瑪利歐兄弟3](../Page/超級瑪利歐兄弟3.md "wikilink")》的风头所遮盖，但是《Fami通》和《Beep\!》杂志的正面评价而使Mega
Drive得以继续。不过发行的第一年，世嘉仅售出40万台。

## 技术规格

[Sega-Genesis-Mod1-Set.jpg](https://zh.wikipedia.org/wiki/File:Sega-Genesis-Mod1-Set.jpg "fig:Sega-Genesis-Mod1-Set.jpg")
[Mega_Drive_mboard.jpg](https://zh.wikipedia.org/wiki/File:Mega_Drive_mboard.jpg "fig:Mega_Drive_mboard.jpg")\]\]
[SMD2mobo.jpg](https://zh.wikipedia.org/wiki/File:SMD2mobo.jpg "fig:SMD2mobo.jpg")\]\]

游戏机的主处理器是一个16/32位，时钟频率7.67MHz的[摩托罗拉
68000CPU](../Page/摩托罗拉_68000.md "wikilink")。\[1\]此外还使用Zilog
Z80次处理器(時脈3.58MHz)。主要是用来控制声音硬件和用来向下兼容世嘉Master System。系统拥有72kB RAM和64
kB显存。能够一次从512个调色板中显示最多至61个颜色。\[2\]游戏是卡带模式，可以从上面插入。

### 解析度

  - 320 X 224

### 音效

  - [山葉](../Page/山葉.md "wikilink") YM2612

:\*[FM身歷聲](../Page/FM.md "wikilink")6音源(第六聲道有8位元DAC，可做[PCM音源之用](../Page/PCM.md "wikilink"))\[3\]

  - [德州儀器](../Page/德州儀器.md "wikilink") SN76489

:\* 4音源(3方波+1噪聲)

## 遊戲库

Mega
Drive的游戏库最初较小，但是最终成长成为含有各种游戏来吸引各种类型的玩家。最初的捆绑游戏是《[兽王记](../Page/兽王记.md "wikilink")》，后来于1991年替换成为《[刺猬索尼克](../Page/刺猬索尼克_\(1991年游戏\).md "wikilink")》。最佳销售游戏为《刺猬索尼克》、《刺猬索尼克2》和《迪斯尼阿拉丁》。\[4\]在游戏机的制作阶段，世嘉着重于动作游戏的制作。而世嘉北美公司则着重于制作体育类游戏。在游戏机的生命周期中，大部分吸引人的游戏库是以街机为基础游戏，以及具有难度的作品，诸如《Ecco
the Dolphin》。也有如《Joe
Montana橄榄球》的体育游戏。与其竞争对手相比，世嘉通过加入一些成熟的游戏来吸引年纪更大的群体，包括含有未经审查版本的《[真人快打](../Page/真人快打_\(1992年游戏\).md "wikilink")》。

最初，Mega Drive遭受较少的第三方支持，这是由于其占有市场比例较小，以及任天堂的垄断做法。早期对于Mega
Drive支持的最大第三方是艺电。艺电的创始人和当时的总裁[特里普·霍金斯认为](../Page/特里普·霍金斯.md "wikilink")，Mega
Drive的快速绘图速度使其与超级任天堂相比，更为适合体育类游戏。

## 附加設備

除了一些诸如能向下相容[Mark III/Master
System遊戲的](../Page/Sega_Master_System.md "wikilink")[Mega
Adaptor](../Page/Mega_Adaptor.md "wikilink")（日版名：メガアダプタ，美版名：Power Base
Converter，歐版名：Master System Converter）的附属设备，Mega
Drive亦支持两项附加设备，每项设备具有其独自的游戏库。

第一项是Mega CD，第二项是32X

### Mega-CD

1991年，CD作为音乐和软件的[儲存裝置开始流行](../Page/儲存裝置.md "wikilink")。由于见到CD对于其竞争者具有巨大优势，世嘉开始与[JVC合作](../Page/JVC.md "wikilink")，开始为Mega
Drive制作一款附加的CD-ROM设备。

銷售量600萬部\[5\])

### 32X

随着世嘉土星计划在1995年的发行，世嘉开始研发一个跨越Mega Drive和土星之间差距解决方案，并作为一个更加廉价的进入32位时代的入口。

在1994年1月的冬季消费电子展，世嘉北美的研发部门主管Joe
Miller收到中山的电话。中山着重要求给予即将发行的雅达利Jaguar的快速回应。世嘉公司因此出现一个概念，那就是“木星计划”。一个全新的游戏平台。木星计划最初希望是Mega
Drive的新版本，後來更名為“海王星計劃”。具有更新的颜色调色板和比即将发行土星更为低的价格。在Miller和他的团队的建议之下，世嘉决定将32X作为Mega
Drive的外围设备。

### 其他

  - [滑鼠](../Page/滑鼠.md "wikilink")
  - [磁碟機](../Page/磁碟機.md "wikilink")
  - [Modem](../Page/Modem.md "wikilink")

## 兼容机

共有超过12种经过授权的Mega
Drive兼容机推出过。\[6\]除了世嘉自己制作的机器外，也有Majesco公司、AtGames、JVC、Pioneer
Corporation、Amstrad和Aiwa推出其他的兼容机。

### 第一方机器

1993年，世嘉推出更为轻小的第二版本。在日本、欧洲和澳大利亚称为**Mega Drive
2**。在北美称为**Genesis**。（没有世嘉前缀）。

1994年，世嘉发行了一个称为Multi-Mega（美版稱為Genesis CDX，巴西版稱為Multi-Mega CDX）的混合半移动Mega
Drive/Mega CD游戏机。

在16位时代后期，世嘉在北美地區发行了名为[Genesis
Nomad的便携式Genesis游戏机](../Page/Sega_Nomad.md "wikilink")。这个根据在日本地区发行的Mega
Jet而设计。Mega Jet是一个Mega
Drive的便携式游戏机，具有喷气式飞机样式的特征，1993年7月1日開始提供給[日本航空機上租用服務](../Page/日本航空.md "wikilink")，之後零售版於1994年3月10日發行。

### 第三方机器

JVC与世嘉公司合作，于1992年4月1日在日本地区发行了Wondermega。之后JVC重新设计了这个系统，于1994年9月以X'Eye名称在北美发行。

  - Multi-Mega
  - Super 32X

## 评价

Mega Drive通常被认为是最为出色的游戏机中的一个。2009年，IGN将其列为最佳游戏平台第五位。

## Mega Drive Mini

2018年4月14日，世嘉於「SEGA Fes」活動上宣布推出「Mega Drive
Mini」版，記念此系列主機推出30週年，內置多款如《[音速小子](../Page/音速小子.md "wikilink")》等以往經典遊戲\[7\]\[8\]，並將於2019年9月19日在全球發行\[9\]。

## 参考资料

## 外部連結

  - [SEGA官方網站](http://sega.jp/)

[Mega_Drive](../Category/Mega_Drive.md "wikilink")
[Category:第四世代遊戲機](../Category/第四世代遊戲機.md "wikilink")
[Category:世嘉遊戲機](../Category/世嘉遊戲機.md "wikilink")
[Category:家用游戏机](../Category/家用游戏机.md "wikilink")
[Category:1988年面世的产品](../Category/1988年面世的产品.md "wikilink")

1.
2.
3.
4.
5.
6.
7.
8.
9.  [メガドライブミニ（Mega Drive Mini）](http://asia.sega.com/mdmini/cht/)