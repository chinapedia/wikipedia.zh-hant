**道格拉斯DC-3**是一款由[道格拉斯飛行器公司生產的雙引擎螺旋槳飛機](../Page/道格拉斯飛行器公司.md "wikilink")。它是[DC-2的改良版](../Page/DC-2.md "wikilink")，提供多個民用或軍用版本（[C-47](../Page/C-47.md "wikilink")），使它成為最受歡迎的螺旋槳飛機之一，同時因其在第二次世界大戰中的表現，它被認為是航空史上最具代表性的運輸機之一。

## 簡介

DC-3於1935年面世，當時的[美國航空要求道格拉斯公司改進](../Page/美國航空.md "wikilink")[DC-2而生](../Page/DC-2.md "wikilink")。DC-3能載客30人，只需在中途一次加油便能橫越美國東西岸，再加上設置首次於飛機上出現的空中廚房，及能在機艙設置床位，為商業飛行帶來了革命性的突破。在此之前，所有航班都不提供熱餐服務，乘客及機组如需用餐，只能在中途站所在的[酒店享用](../Page/酒店.md "wikilink")，一旦途经一些落後地區（如[非洲](../Page/非洲.md "wikilink")）沒有[酒店就相當不便](../Page/酒店.md "wikilink")。

[CX-DC3-VRHDB2.jpg](https://zh.wikipedia.org/wiki/File:CX-DC3-VRHDB2.jpg "fig:CX-DC3-VRHDB2.jpg")於1946年開業時的首架飛機Betsy（VR-HDB），便是一架DC-3，現在於[香港科學館展出](../Page/香港科學館.md "wikilink")\]\]
DC-3性能比之前的飛機更穩定，運作成本更低，維修保養容易。到了[第二次世界大戰爆發](../Page/第二次世界大戰.md "wikilink")，DC-3被[盟軍徵召為軍機作戰](../Page/盟軍.md "wikilink")，軍用的DC-3被稱為[C-47](../Page/C-47.md "wikilink")。而作戰期間對運輸機需求大增，C-47被大量生產，曾擔任過的任務多不勝數，當中包括執行[中國戰場的](../Page/中國.md "wikilink")[驼峰航线](../Page/驼峰航线.md "wikilink")。C-47亦被視為盟軍取勝的功臣之一。

戰後，大量退役的C-47由軍轉民用，各中小航空公司皆引進這些DC-3以開拓業務及創業，這些以退役物資出售的二手C-47價廉物美，成為各航空公司的旗艦機種。它亦執行戰後冷戰時期的人道任務，當中最著名的便是為解救[柏林的](../Page/柏林.md "wikilink")[柏林空運](../Page/柏林空運.md "wikilink")，可以說當時全球各地機場皆能找到DC-3的蹤影。

[Category:道格拉斯](../Category/道格拉斯.md "wikilink")
[Category:美國航空器](../Category/美國航空器.md "wikilink")