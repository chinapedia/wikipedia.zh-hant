**常陳一**（**α CVn** / **獵犬座α**）英文傳統名為**Cor
Caroli**，是[獵犬座最明亮的](../Page/獵犬座.md "wikilink")[恆星](../Page/恆星.md "wikilink")，也是[聯星](../Page/聯星.md "wikilink")，但[國際天文學聯合會現在認定這個名字只適用於其中較亮的那一顆](../Page/國際天文學聯合會.md "wikilink")\[1\]。不過，以裸眼觀測，常陳一還是北天[獵犬座中最亮的光點](../Page/獵犬座.md "wikilink")，與[牧夫座的](../Page/牧夫座.md "wikilink")[大角星](../Page/大角星.md "wikilink")、[室女座的](../Page/室女座.md "wikilink")[角宿一](../Page/角宿一.md "wikilink")、[獅子座的](../Page/獅子座.md "wikilink")[五帝座一構成](../Page/五帝座一.md "wikilink")[春天大鑽石](../Page/春天大鑽石.md "wikilink")。

## 名稱

**常陳一**，[拜耳名稱是](../Page/拜耳命名法.md "wikilink")**α CVn** /
**獵犬座α**（[拉丁文是](../Page/名稱拉丁化.md "wikilink")*Alpha
Canum Venaticorum*）。兩顆中較亮的是''α<sup>2</sup>
*，較暗的反而是*α<sup>1</sup>''\[2\]。

在[西方世界](../Page/西方世界.md "wikilink")，直到17世紀才將這顆恆星命名為*Cor
Caroli*，意思是*查理的心臟*，但不確定是為了尊榮1649年結束第二次[英國內戰的](../Page/英國內戰.md "wikilink")[查理一世](../Page/查理一世_\(英格蘭\).md "wikilink")，還是在1660年恢復英國君主制王位的[查理二世](../Page/查理二世_\(英格蘭\).md "wikilink")。個名字被認為是精神科醫師爵士在1660年提出的，因為他宣稱在查理二世返回英國的那一晚，這顆星顯得特別明亮。R.H.艾倫在*恆星的名字*這本書中宣稱嘉士堡將這個名字建議給[愛德蒙·哈雷](../Page/愛德蒙·哈雷.md "wikilink")，有意來尊榮查理二世\[3\]。然而，[小羅伯特·伯納姆注意到](../Page/小羅伯特·伯納姆.md "wikilink")"
[約翰·波德於](../Page/約翰·波德.md "wikilink")1801年在柏林出版的一篇論文中提到名字歸屬於哈雷，但似乎沒有其它的驗證"\[4\]。在*星的故事*一書中，指出這個名字的第一次出現是法蘭西斯拉姆在1673年出版一份星圖上，他的標示是*Cor
Caroli Regis Martyris*，意思是烈士國王查理的心臟，清楚的表明它是在尊榮查理一世\[5\]。

在2016年，[國際天文學聯合會組織的](../Page/國際天文學聯合會.md "wikilink")（Working Group on
Star
Names，WGSN）\[6\]將恆星的正確名稱標準化和建立目錄，並在2016年7月發布第一個公報\[7\]，包括WSGN核准的前兩批名稱的表格；其中認定*Cor
Caroli*是獵犬座α<sup>2</sup>。

## 恆星性質

[Cor_Caroli_2011-03-01.jpeg](https://zh.wikipedia.org/wiki/File:Cor_Caroli_2011-03-01.jpeg "fig:Cor_Caroli_2011-03-01.jpeg")

常陳一是一顆[聯星](../Page/聯星.md "wikilink")，整體的[視星等為](../Page/視星等.md "wikilink")2.81，兩星在天球上相距19.6[角秒](../Page/角秒.md "wikilink")，小望遠鏡就可以很容易分辨出來。這個系統距離[太陽約](../Page/太陽.md "wikilink")110[光年](../Page/光年.md "wikilink")。

它標示者北天的[星群](../Page/星群.md "wikilink")：[大鑽石或室女座鑽石的一個頂點](../Page/大鑽石.md "wikilink")。

### 獵犬座α<sup>2</sup>

獵犬座α<sup>2</sup>的[恆星光譜類型為A](../Page/恆星光譜.md "wikilink")0，並且[視星等明顯的在](../Page/視星等.md "wikilink")2.84和2,98
之間以5.47天的周期變化\[8\]。它是一顆[特殊恆星](../Page/特殊恆星.md "wikilink")，有強大的磁場，大約是地球的5,000倍，被歸類為[Ap和Bp星](../Page/Ap和Bp星.md "wikilink")\[9\]。它的大氣富含一些元素，像是[矽](../Page/矽.md "wikilink")、[汞](../Page/汞.md "wikilink")、和[銪](../Page/銪.md "wikilink")。
這些被認為是由於恆星的引力作用使一些元素下沉，而一些倍輻射壓抬升\[10\]\[11\]。這顆恆星在[變星類型上是](../Page/變星.md "wikilink")[獵犬座α<sup>2</sup>型變星的原型](../Page/獵犬座α\<sup\>2\</sup\>型變星.md "wikilink")。這類恆星的強[磁場被認為是由巨大的星斑造成的](../Page/磁場.md "wikilink")，而由於這些星斑的亮度，造成獵犬座α<sup>2</sup>自轉期間的光度變化。

### 獵犬座α<sup>1</sup>

獵犬座α<sup>1</sup>，是[F型主序星](../Page/F型主序星.md "wikilink")，它是比較暗的伴星，視星等大約是5.60等\[12\]。

## 同名物件

[美國海軍的](../Page/美國海軍.md "wikilink") 以這顆恆星的名字命名。

## 參考資料

## 外部連結

  - [Winshop.com](https://web.archive.org/web/20070104033950/http://winshop.com.au/annew/CorCaroli.html)

  - [Redshift 3, données](http://www.redshift.de/fr/)

  - [Bright Star
    Catalogue](http://www.alcyone.de/cgi-bin/search.pl?object=HR4915)

[C](../Category/双星.md "wikilink")
[Category:主序星](../Category/主序星.md "wikilink")
[Category:猎犬座](../Category/猎犬座.md "wikilink")
[猎犬座α](../Category/拜耳天體.md "wikilink")
[常陈](../Category/常陈_\(星官\).md "wikilink")
[Category:星座最亮星](../Category/星座最亮星.md "wikilink")
[Category:獵犬座α²變星](../Category/獵犬座α²變星.md "wikilink")

1.

2.  [Cor Caroli](http://www.astro.uiuc.edu/~kaler/sow/corcaroli.html) ,
    *Stars*, Jim Kaler. Accessed on line September 15, 2008.

3.  R.H. Allen, *Star Names: Their Lore and Meaning*.

4.  Robert Burnham, Jr. *Burnham's Celestial Handbook*, Volume 1, p.
    359.

5.  [Ian Ridpath: "Star Tales", Canes
    Venatici](http://www.ianridpath.com/startales/canesvenatici.htm).
    See also Deborah J. Warner, *The Sky Explored: Celestial Cartography
    1500-1800*.

6.

7.

8.
9.  "Cor Caroli", p. 49, *The hundred greatest stars*, James B. Kaler,
    Springer, 2002, .

10.
11.
12.