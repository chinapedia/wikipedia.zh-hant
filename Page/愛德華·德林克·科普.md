[Edcope.jpg](https://zh.wikipedia.org/wiki/File:Edcope.jpg "fig:Edcope.jpg")
**愛德華·德林克·科普**（**Edward Drinker
Cope**，）是一位[美國](../Page/美國.md "wikilink")[古生物學家及](../Page/古生物學家.md "wikilink")[比較解剖學家](../Page/比較解剖學.md "wikilink")，同時也是[爬蟲類學家與](../Page/爬蟲兩棲類學.md "wikilink")[魚類學家](../Page/魚類學.md "wikilink")。科普出生於[費城](../Page/費城.md "wikilink")，1859年時將一份關於[蠑螈的研究論文送交費城的](../Page/蠑螈.md "wikilink")[自然科學學會](../Page/自然科學學會.md "wikilink")（Academy
of Natural
Sciences）。1889年時成為[賓州大學的古生物學及](../Page/賓州大學.md "wikilink")[地質學教授](../Page/地質學.md "wikilink")。

科普專注於美國的[脊椎動物化石研究](../Page/脊椎動物.md "wikilink")，他在一生當中發掘了超過1000個新[物種](../Page/物種.md "wikilink")，其中包括已滅絕動物。共有56種恐龍是由科普所發現。他曾經與另一位恐龍研究者[奧塞內爾·查利斯·馬什進行一場](../Page/奧塞內爾·查利斯·馬什.md "wikilink")「[骨頭大戰](../Page/骨頭大戰.md "wikilink")」（Bone
Wars），對新化石的發現展開競爭。

## 參考文獻

## 外部連結

  - [Bibliography of dinosaur-related
    references](https://web.archive.org/web/20070929092041/http://www.dinodata.org/index.php?option=com_content&task=view&id=2854&Itemid=66)
    at DinoData
  - [Dinosaurs](https://web.archive.org/web/20070929102631/http://www.dinodata.org/index.php?option=com_content&task=view&id=1149&Itemid=108)
    named by Cope, at DinoData
  - [Biographies of Persons honored in the Herpetological
    Nomenclature](http://ebeltz.net/herps/biogappx.html#Cope)

[Category:美國古生物學家](../Category/美國古生物學家.md "wikilink")
[Category:美國魚類學家](../Category/美國魚類學家.md "wikilink")
[Category:解剖學家](../Category/解剖學家.md "wikilink")