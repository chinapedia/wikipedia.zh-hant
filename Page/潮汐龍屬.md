**潮汐龍屬**（[屬名](../Page/學名.md "wikilink")：）是種大型[泰坦巨龍類](../Page/泰坦巨龍類.md "wikilink")[蜥腳下目](../Page/蜥腳下目.md "wikilink")[恐龍](../Page/恐龍.md "wikilink")，化石被發現於[埃及的](../Page/埃及.md "wikilink")，該地層屬於上[白堊紀的海岸沉積層](../Page/白堊紀.md "wikilink")。這些化石是從1935年以來，拜哈里耶組首度發現的[四足總綱動物](../Page/四足總綱.md "wikilink")。潮汐龍的[肱骨長達](../Page/肱骨.md "wikilink")1.69公尺，比任何已知白堊紀的蜥腳類恐龍還長。當地的屍體被保存於由[潮汐帶來的沉積層](../Page/潮汐.md "wikilink")，這些沉積層包含了[紅樹林](../Page/紅樹林.md "wikilink")[植被的化石](../Page/植被.md "wikilink")。潮汐龍所棲息的紅樹林生態系統，位於[古地中海的南岸](../Page/古地中海.md "wikilink")。潮汐龍是第一種被證實存活在紅樹林生態環境的恐龍\[1\]。

目前對於潮汐龍的所知甚少，所以很難去估計潮汐龍的正確體型大小。然而有限的化石顯示，潮汐龍是目前所發現最巨大的恐龍之一，體重被估計有59公噸\[2\]。[肯尼思·卡彭特](../Page/肯尼思·卡彭特.md "wikilink")（Kenneth
Carpenter）以[薩爾塔龍作為參考](../Page/薩爾塔龍.md "wikilink")，估計潮汐龍的身長可能約26公尺\[3\]。如同其他的泰坦巨龍類，潮汐龍擁有寬廣的姿勢，身上可能擁有防禦用的[皮內成骨](../Page/皮內成骨.md "wikilink")（Osteoderms）。潮汐龍可能被大型掠食動物所獵食，例如[鯊齒龍](../Page/鯊齒龍.md "wikilink")。

## 詞源

[模式種是](../Page/模式種.md "wikilink")**斯氏潮汐龍**（*Paralititan
stromeri*），意為「[恩斯特·斯特莫的潮汐](../Page/恩斯特·斯特莫.md "wikilink")[巨人族](../Page/泰坦.md "wikilink")」。牠們是由Joshua
B. Smith、Matthew C. Lamanna、Kenneth J.
Lacovara、[彼得·達德森](../Page/彼得·達德森.md "wikilink")、Jennifer
B. Smith、Jason C. Poole、Robert Giegengack、以及Usery
Attia在2001年所命名；種名是紀念[德國](../Page/德國.md "wikilink")[古生物學家與](../Page/古生物學家.md "wikilink")[地理學家恩斯特](../Page/地理學家.md "wikilink")·斯特莫（Ernst
Stromer），他在1900年代早期於[非洲地區發現了許多恐龍](../Page/非洲.md "wikilink")\[4\]。

## 大眾文化

潮汐龍曾出現[探索頻道的電視節目](../Page/探索頻道.md "wikilink")《*Monsters
Resurrected*》，被描述成一種繁盛的草食性恐龍，並被同時代的[鯊齒龍](../Page/鯊齒龍.md "wikilink")、[皺褶龍](../Page/皺褶龍.md "wikilink")、[棘龍所獵食](../Page/棘龍.md "wikilink")\[5\]。在2002年的紀錄片《失落的埃及恐龍》（*The
Lost Dinosaurs of Egypt*），曾提到潮汐龍的化石挖掘過程\[6\]。

## 參考資料

## 外部連結

  - [DinoData網站 (需要註冊)](http://www.dinodata.org/index.php)
  - [潮汐龍基本數據](http://www.nhm.ac.uk/jdsml/nature-online/dino-directory/detail.dsml?Genus=Paralititan)
    - Dino Directory網站
  - [*The Delaware Valley Paleontological Society*
    (DVPS)](http://www.dvps.org)

[Category:非洲恐龍](../Category/非洲恐龍.md "wikilink")
[Category:上白堊紀恐龍](../Category/上白堊紀恐龍.md "wikilink")
[Category:泰坦巨龍類](../Category/泰坦巨龍類.md "wikilink")

1.

2.  Burness, G.P. and Flannery, T. (2001). "Dinosaurs, dragons, and
    dwarfs: The evolution of maximal body size." *Proceedings of the
    National Academy of Sciences*, **98**(25): 14518-14523.

3.  Carpenter, K. (2006). "Biggest of the big: a critical re-evaluation
    of the mega-sauropod *Amphicoelias fragillimus*." In Foster, J.R.
    and Lucas, S.G., eds., 2006, *Paleontology and Geology of the Upper
    Jurassic Morrison Formation.* New Mexico Museum of Natural History
    and Science Bulletin **36**: 131-138.

4.
5.

6.  Nothdurft et al., 2002, The Lost Dinosaurs of Egypt: The Astonishing
    and Unlikely True Story of One of the Twentieth Century's Greatest
    Paleontological Discoveries, Random House, 272 p.