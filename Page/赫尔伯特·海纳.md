**赫尔伯特·海内**（****，），[德国经理人](../Page/德国.md "wikilink")，现任[阿迪达斯集团](../Page/阿迪达斯.md "wikilink")[首席执行官及德国](../Page/首席执行官.md "wikilink")[拜仁慕尼黑足球俱乐部监事会主席](../Page/拜仁慕尼黑足球俱乐部.md "wikilink")。

## 职业经历

海内于1979年加入消费日用品公司[宝洁的德国公司](../Page/宝洁.md "wikilink")。1987年他跳槽到阿迪达斯并担任过数个重要职位，刚开始，他担任过销售总监、销售执行官和阿迪达斯德国公司的总经理。1996年开始他担任负责欧洲、非洲和远东的高级副总裁，1997年他获得了董事职位。1999年他加入了董事会。在2001年3月他成为了董事会主席。

他于2014年接替因逃税获刑而辞职的[乌利·赫内斯出任](../Page/乌利·赫内斯.md "wikilink")[拜仁慕尼黑足球俱乐部监事会主席](../Page/拜仁慕尼黑足球俱乐部.md "wikilink")。\[1\]

他也是总部位于[慕尼黑的巴伐利亚保险银行](../Page/慕尼黑.md "wikilink")（Bayerische
Versicherungbank）的[监事会成员](../Page/监事会.md "wikilink")，以及位于曼海姆的Engelhorn公司的监事会成员。

## 个人生活

海内1954年7月3日出生于德国[巴伐利亚州](../Page/巴伐利亚.md "wikilink")[丁戈尔芬格市](../Page/丁戈尔芬格.md "wikilink")，拥有企业经济学硕士学位（Diplom-Betriebswirt），已婚并有2个孩子。

## 所获成就

2003年他获得了经济领域的[斑比奖](../Page/斑比奖.md "wikilink")（Bambi）。

## 参考文献

<references />

[Category:阿迪达斯人物](../Category/阿迪达斯人物.md "wikilink")
[H](../Category/德国企业家.md "wikilink")
[H](../Category/拜仁慕尼黑职员.md "wikilink")

1.