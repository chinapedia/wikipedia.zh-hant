**每用户平均收入**（Average Revenue Per User，简称**ARPU**；有时也称average revenue per
unit，每单元平均[成本](../Page/成本.md "wikilink")）是一个[电信業](../Page/電信公司.md "wikilink")[術語](../Page/術語.md "wikilink")。就电信業者而言，用戶數與通話量是公司獲利的重要指标，而ARPU為其基礎之一。

## 各國之ARPU

据WirelessWire
News在2010年的文章，各[移动电话市场的ARPU狀況如下](../Page/移动电话.md "wikilink")\[1\]：

  - [美國](../Page/美國.md "wikilink")
      - 2009年度為48.65[美元](../Page/美元.md "wikilink")。從90年代至今沒有多大變化。
  - [中国移动](../Page/中国移动.md "wikilink")
      - 50元[人民币左右](../Page/人民币.md "wikilink")。
  - [中国联通](../Page/中国联通.md "wikilink")
      - 30元人民币左右。
  - [欧洲](../Page/欧洲.md "wikilink")
      - 法國在2007年度為55[歐元](../Page/歐元.md "wikilink")\[2\]，德國T-mobile在2009年度為
        14.89歐元\[3\]等。各國之間有相當大的變化。

据[中国工程院院士](../Page/中国工程院院士.md "wikilink")[邬贺铨](../Page/邬贺铨.md "wikilink")2016年透露，[中国大陆市场上固网宽带三大运营商](../Page/中国大陆.md "wikilink")2015年用户ARPU值：中国电信为56.3元，[中国联通为](../Page/中国联通.md "wikilink")63.6元，中国移动为32元\[4\]。

## 參見

  - [加值服務](../Page/加值服務.md "wikilink")
  - [移动电话](../Page/移动电话.md "wikilink")
  - [PHS](../Page/个人手持式电话系统.md "wikilink")
  - [4G](../Page/4G.md "wikilink")

## 註釋

## 外部連結

  - [Wisegeek on ARPU](http://www.wisegeek.com/what-is-arpu.htm)
  - [ARPU definition at
    Investopedia](http://www.investopedia.com/terms/a/arpu.asp)
  - [PC Magazine
    Definition](http://www.pcmag.com/encyclopedia_term/0,2542,t=ARPU&i=37990,00.asp)

[Category:電信術語](../Category/電信術語.md "wikilink")

1.  [金額とARPUで見た市場の質](http://wirelesswire.jp/Global_Trendline/201004011200-2.html)
    WirelessWire News 2010年4月1日
2.  [フランスの2007年1世帯あたり通信費用は月93ユーロ](http://blogs.itmedia.co.jp/eu/2008/02/193-2fe7.html)ITmedia
    2008年2月22日
3.  [1Q10 Germany Mobile Operator Forecast, 2009 - 2014: Germany will
    have 120.3 million mobile subscribers in 2014 with T-Mobile's market
    share rising
    to 36.8%](http://www.marketresearch.com/product/display.asp?productid=2584552&g=1)
    IE Market Research Corp. 2010年1月28日
4.