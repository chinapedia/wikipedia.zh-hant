**键合异构**是[配位化合物](../Page/配位化合物.md "wikilink")[结构异构的一种形式](../Page/结构异构.md "wikilink")，[配体通过不同的配位原子与中心原子配位](../Page/配体_\(化学\).md "wikilink")。生成的配合物称为**键合异构体**，配体称为**两可配体**。

## 历史

首先制得的键合异构体是\[Co(NH<sub>3</sub>)<sub>5</sub>(NO<sub>2</sub>)\]Cl<sub>2</sub>，制得的两种该[钴配合物具有不同的物理性质](../Page/钴.md "wikilink")。红色异构体中亚硝酸根以氮原子配位，黄色异构体中亚硝酸根则以一个氧原子配位，写为\[Co(NH<sub>3</sub>)<sub>5</sub>(ONO)\]<sup>2+</sup>。进一步的实验表明，在紫外线照射下，红色的异构体会自发转变为黄色的异构体。

虽然键合异构现象早在十九世纪晚期就已经发现，但直到1907年才对该现象做出解释。\[1\]
[LinkageIsomers.png](https://zh.wikipedia.org/wiki/File:LinkageIsomers.png "fig:LinkageIsomers.png")

## 配体

以下配体的配合物常会产生键合[异构体](../Page/异构体.md "wikilink")：

  - [硫氰酸根离子](../Page/硫氰酸根.md "wikilink")—SCN<sup>−</sup>，分别称为“硫氰酸根”和“异硫氰酸根”
  - [硒氰酸根离子](../Page/硒氰酸根.md "wikilink")—SeCN<sup>−</sup>，类似硫氰酸根
  - [亚硝酸根离子](../Page/亚硝酸根.md "wikilink")—NO<sub>2</sub><sup>−</sup>，以氮配位称“硝基”，以氧配位称“亚硝酸根”
  - [亚硫酸根离子](../Page/亚硫酸根.md "wikilink")—SO<sub>3</sub><sup>2−</sup>
  - [氰离子](../Page/氰离子.md "wikilink")—CN<sup>−</sup>

典型的键合异构体如紫色的\[(NH<sub>3</sub>)<sub>5</sub>Co-SCN\]<sup>2+</sup>及橙黄色的\[(NH<sub>3</sub>)<sub>5</sub>Co-NCS\]<sup>2+</sup>。硫配位的化合物与氮配位的化合物之间存在互变，且异构化反应是分子内的。\[2\]

## 参考资料

<references/>

[Category:配位化学](../Category/配位化学.md "wikilink")
[Category:化学键](../Category/化学键.md "wikilink")
[Category:异构](../Category/异构.md "wikilink")

1.  [Werner](../Page/Alfred_Werner.md "wikilink"), A. “Über
    strukturisomere Salze der Rhodanwasserstoffsäure und der salpetrigen
    Säure” Berichte der deutschen chemischen Gesellschaft 1907, volume
    40, 765-788. DOI: 10.1002/cber.190704001117
2.  Buckingham, D. A.; Creaser, I. I.; Sargeson, A. M. "Mechanism of
    Base Hydrolysis for
    Co<sup>III</sup>(NH<sub>3</sub>)<sub>5</sub>X<sup>2+</sup> Ions.
    Hydrolysis and Rearrangement for the Sulfur-Bonded
    Co(NH<sub>3</sub>)<sub>5</sub>SCN<sup>2+</sup> Ion" Inorganic
    Chemistry (1970), volume 9, pages 655-61.