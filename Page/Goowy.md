Goowy是一个以[Action
Script](../Page/Action_Script.md "wikilink")（[flash](../Page/flash.md "wikilink")）为语言的服务性网站，提供[邮箱](../Page/邮箱.md "wikilink")、Minis、[游戏](../Page/游戏.md "wikilink")、[通讯簿](../Page/通讯簿.md "wikilink")、[日程表](../Page/日程表.md "wikilink")、[IM以及同](../Page/IM.md "wikilink")[Box.net合作的文件储存平台](../Page/Box.net.md "wikilink")。

## 桌面版本

Goowy已发布Goowy的桌面版本，用户可从Goowy主页上下载这个插件在桌面上使用goowy，这省去了登入网站的麻烦。

## 邮箱

Goowy创作初期提供100MB的邮箱，在逐步发展后逐渐扩展为2GB。Goowy的邮箱使用flash操作界面，拥有多种编码阅读和功能强大的邮件编辑器。

## Minis

[goowy2.jpg](https://zh.wikipedia.org/wiki/File:goowy2.jpg "fig:goowy2.jpg")Minis是Goowy的自定义主页，用户可以在这里定制[rss阅读](../Page/rss.md "wikilink")、[计算器](../Page/计算器.md "wikilink")、[便签](../Page/便签.md "wikilink")、[时间](../Page/时间.md "wikilink")、[天气和](../Page/天气.md "wikilink")“To
do List”等功能。

## 游戏

Goowy提供免费flash游戏，这意味着用户不需要任何安装、费用就可以享受游戏了。

## 通讯簿

Goowy同时提供强大的通讯簿功能，用户可以从[MSN](../Page/MSN.md "wikilink")、[Yahoo\!Mail](../Page/Yahoo!Mail.md "wikilink")、[Gmail等账号中倒入联系人](../Page/Gmail.md "wikilink")，并且支持给联系人分类、添加头像、增加联系人便签等。

## 日程表

用户可以倒入、倒出以[iCalendar格式](../Page/iCalendar.md "wikilink")，并且可以选择闹铃方式、闹铃周期、闹铃名称等。

## IM

Goowy支持用户在Goowy中登入[MSN](../Page/MSN.md "wikilink") Messenger(Live
Messenger)、[ICQ](../Page/ICQ.md "wikilink")（[AOL](../Page/AOL.md "wikilink")）、[G-Talk和](../Page/G-Talk.md "wikilink")[aim账号](../Page/aim.md "wikilink")，用户不需要下载任何软件就可以在线使用Im。

## 文件储存

Goowy同[box.net合作](../Page/box.net.md "wikilink")，增加文件储存平台，用户可以使用自己的Box.net账号登入或免费注册用户名@goowy.com的账号登入文件储存平台。

## 其他功能

Goowy同时拥有客户端收取、收藏夹、音乐、拼写检查等功能

### 客户端收取

Goowy支持收取[hotmail](../Page/hotmail.md "wikilink")／[MSN
mail](../Page/MSN_mail.md "wikilink")／[Yahoo](../Page/Yahoo.md "wikilink")！Mai/[Gmail](../Page/Gmail.md "wikilink")／[POP3的远程收取](../Page/POP3.md "wikilink")，用户只需要登入Goowy就可以收取邮件。

### 收藏夹

Goowy提供收藏夹功能，用户可以管理、增加站点。

## 连接

  - [Goowy](http://www.goowy.com)
  - [Goowy新闻](http://www.goowy.com/news.html)
  - [Goowy社区](https://web.archive.org/web/20060515225753/http://forums.goowy.com/)
  - [Goowy博客](http://blogger.goowy.com/)
  - [联系goowy](http://www.goowy.com/contact.html)

[Category:Web 2.0](../Category/Web_2.0.md "wikilink")
[Category:Flash](../Category/Flash.md "wikilink")
[Category:服务网站](../Category/服务网站.md "wikilink")