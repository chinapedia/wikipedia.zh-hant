\-{T|zh-hans:平治 (二条天皇);zh-hant:平治 (二條天皇)}-
**-{平治}-**（1159年四月二十日至1160年正月十日）是[日本的](../Page/日本.md "wikilink")[年號](../Page/年號.md "wikilink")。這個時代的[天皇是](../Page/天皇.md "wikilink")[二條天皇](../Page/二條天皇.md "wikilink")。

## 改元

  - 保元四年四月二十日（西元1159年5月9日） 因二條天皇即位而改元-{平治}-
  - \-{平治}-二年正月十日（西元1160年2月18日） 改元永曆

## 出處

《[史記卷二](../Page/史記.md "wikilink")·[夏](../Page/夏朝.md "wikilink")[本紀](../Page/s:史記:卷2.md "wikilink")》：「東漸于海，西被于流沙，朔、南暨：聲教訖于四海。於是帝錫禹玄圭，以告成功于天下。天下於是太-{平治}-。」。

## 出生

[源義經](../Page/源義經.md "wikilink")——[源義朝第九子](../Page/源義朝.md "wikilink")。

## 逝世

## 大事記

  - \-{[平治之亂](../Page/平治之亂.md "wikilink")}-

## 紀年、西曆、干支對照表

| \-{平治}-                            | 元年                             | 二年                             |
| ---------------------------------- | ------------------------------ | ------------------------------ |
| [儒略曆日期](../Page/儒略曆.md "wikilink") | 1159年                          | 1160年                          |
| [干支](../Page/干支.md "wikilink")     | [己卯](../Page/己卯.md "wikilink") | [庚辰](../Page/庚辰.md "wikilink") |

## 參考

  - [日本年號索引](../Page/日本年號索引.md "wikilink")
  - 同期存在的其他政權之紀年
      - [紹興](../Page/紹興_\(宋高宗\).md "wikilink")（1131年正月至1162年十二月）：[宋](../Page/南宋.md "wikilink")—[宋高宗趙構之年號](../Page/宋高宗.md "wikilink")
      - [龍興](../Page/龍興.md "wikilink")：[大理](../Page/大理國.md "wikilink")—段正興之年號
      - [盛明](../Page/盛明.md "wikilink")：大理—段正興之年號
      - [建德](../Page/建德.md "wikilink")：大理—段正興之年號
      - [大定](../Page/大定_\(李天祚\).md "wikilink")（1140年至1162年）：[李朝](../Page/李朝_\(越南\).md "wikilink")—李天祚之年號
      - [紹興](../Page/紹興_\(耶律夷列\).md "wikilink")（1151年至1163年）：[西遼](../Page/西遼.md "wikilink")—仁宗[耶律夷列之年號](../Page/耶律夷列.md "wikilink")
      - [正隆](../Page/正隆.md "wikilink")（1156年二月至1161年十月）：[金](../Page/金朝.md "wikilink")—[金海陵王完顏亮之年號](../Page/金海陵王.md "wikilink")
      - [天盛](../Page/天盛_\(西夏\).md "wikilink")（1149年正月至1169年十二月）：[西夏](../Page/西夏.md "wikilink")—夏仁宗[李仁孝之年號](../Page/李仁孝.md "wikilink")

## 參考文獻

  - 松橋達良，《元号はやわかり—東亜歷代建元考》，砂書房，1994年7月，ISBN 4915818276
  - 李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129
  - 所功，《年号の歴史―元号制度の史的研究》，雄山閣，1988年3月，ISBN 4639007116

[Category:日本平安時代年號](../Category/日本平安時代年號.md "wikilink")
[Category:12世纪日本年号](../Category/12世纪日本年号.md "wikilink")
[Category:1150年代日本](../Category/1150年代日本.md "wikilink")
[Category:1160年代日本](../Category/1160年代日本.md "wikilink")