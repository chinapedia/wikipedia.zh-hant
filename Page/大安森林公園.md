**大安森林公園**，位於[臺灣](../Page/臺灣.md "wikilink")[臺北市](../Page/臺北市.md "wikilink")[大安區](../Page/大安區_\(臺北市\).md "wikilink")，佔地25.9354公頃\[1\]，由[臺北市政府工務局公園路燈工程管理處負責管理與維護](../Page/臺北市政府工務局公園路燈工程管理處.md "wikilink")。公園位於台北市區中心，是一座草木濃密的生態公園，都會森林的型態被譽為台北市的「都市之肺」；其在興建之前被稱為「七號公園」，1994年3月29日正式對外開放\[2\]，當時曾是台北市區最大的公園。

園內除大量樹林，亦有規劃花壇。園外靠道路之人行道則以多層式綠籬景觀與各道路區隔，臨建國南路為[盾柱木](../Page/盾柱木.md "wikilink")，臨和平東路為[樟樹](../Page/樟樹.md "wikilink")、臨新生南路為[白千層](../Page/白千層.md "wikilink")、臨信義路則為[榕樹](../Page/榕樹.md "wikilink")，人行道中央還植有[楓香樹](../Page/楓香樹.md "wikilink")。此外，園內亦設有行人座椅、涼亭、音樂舞台、慢跑道等多種休憩設施，公園下方靠信義路建國南路口側則設置有地下停車場。

## 園內設施及生態

整個公園主要可分為：竹林區、榕樹區、香花區、水生植物區、帶狀林區、水池假山區、露天音樂台、兒童遊戲區和停車場。

2014年至2015年期間，大安森林公園之友基金會委託[臺灣大學森林環境暨資源學系在園內進野生動物之研究調查](../Page/臺灣大學.md "wikilink")，在園內共記錄到包括鳥類49種、哺乳類3種、兩棲類4種、爬蟲類5種及3種魚類，共64種脊椎動物；其中還包括[鳳頭蒼鷹](../Page/鳳頭蒼鷹.md "wikilink")、[游隼](../Page/游隼.md "wikilink")、[紅尾伯勞](../Page/紅尾伯勞.md "wikilink")、[臺灣藍鵲](../Page/臺灣藍鵲.md "wikilink")4種保育類動物，[五色鳥](../Page/五色鳥.md "wikilink")、[臺灣藍鵲](../Page/臺灣藍鵲.md "wikilink")、[小彎嘴](../Page/小彎嘴.md "wikilink")、[盤古蟾蜍](../Page/盤古蟾蜍.md "wikilink")、[斯文豪氏攀蜥](../Page/斯文豪氏攀蜥.md "wikilink")、[蓬萊草蜥](../Page/蓬萊草蜥.md "wikilink")6種臺灣特有種\[3\]，其中鳳頭蒼鷹在每年繁殖季會固定在此築巢孵育幼鳥。

也因為園內豐富的生態，在大安森林公園內曾先後針對[黑冠麻鷺](../Page/黑冠麻鷺.md "wikilink")、鳳頭蒼鷹進行繫放追蹤等多項調查，也有針對五色鳥設置人工巢箱試驗。\[4\]\[5\]\[6\]\[7\]

### 露天音樂台

[Open_air_theatre_in_Daan_Park.jpg](https://zh.wikipedia.org/wiki/File:Open_air_theatre_in_Daan_Park.jpg "fig:Open_air_theatre_in_Daan_Park.jpg")

大安森林公園的露天音樂台，可提供九百多人的觀眾參與，經常舉辦大型演出活動；遇選舉時，也常成為候選人舉辦造勢活動之場地。

台灣著名樂團[五月天於](../Page/五月天.md "wikilink")1997年在此舉辦的音樂活動[野台開唱中](../Page/野台開唱.md "wikilink")，正式成立並首次公開演出；2017年，五月天特地在此舉辦《20週年演唱會回到1997.3.29》
。\[8\]

### 兒童遊戲區

[Taipei_Daan_Park_-_Playground_-_20180715_-_08.jpg](https://zh.wikipedia.org/wiki/File:Taipei_Daan_Park_-_Playground_-_20180715_-_08.jpg "fig:Taipei_Daan_Park_-_Playground_-_20180715_-_08.jpg")

園內在靠近信義路一側設有一面積達1,300平方公尺的兒童遊戲區，其內有盪鞦韆、溜滑梯、沙坑等公園遊具，一旁還設有滑輪場和兒童小舞台。

### 生態池

[Eco_Pond_in_Daan_Forest_Park_大安森林公園生態池_-_panoramio.jpg](https://zh.wikipedia.org/wiki/File:Eco_Pond_in_Daan_Forest_Park_大安森林公園生態池_-_panoramio.jpg "fig:Eco_Pond_in_Daan_Forest_Park_大安森林公園生態池_-_panoramio.jpg")

在興建公園時特地規劃了一處人工生態池，面積達0.7公頃，池中有兩小島，水池周邊及小島上植有[金露花](../Page/金露花.md "wikilink")、[紅葉鐵莧](../Page/紅葉鐵莧.md "wikilink")、[美人蕉](../Page/美人蕉.md "wikilink")、[軟枝黃蟬等多種植物](../Page/軟枝黃蟬.md "wikilink")，加上島上無人為干擾，已有大量[夜鷺與](../Page/夜鷺.md "wikilink")[小白鷺群聚於此](../Page/小白鷺.md "wikilink")，並年在繁殖期會在此築巢孵育幼鳥。\[9\]

### 小生態池

[Taipei_Daan_Park_-_Small_Ecological_Pool_-_20180805_-_01.jpg](https://zh.wikipedia.org/wiki/File:Taipei_Daan_Park_-_Small_Ecological_Pool_-_20180805_-_01.jpg "fig:Taipei_Daan_Park_-_Small_Ecological_Pool_-_20180805_-_01.jpg")

2015年起，公園與大安森林公園之友基金會合作，利用生態工法儲存利用雨水，重新規劃植栽，重現台北之濕地原生植物，陸續設置了三處小生態池，並在小生態池內孵育[螢火蟲](../Page/螢火蟲.md "wikilink")。\[10\]\[11\]\[12\]

### 生態水圳

[Taipei_Daan_Park_-_Ecological_Water_-_20180715_-_01.jpg](https://zh.wikipedia.org/wiki/File:Taipei_Daan_Park_-_Ecological_Water_-_20180715_-_01.jpg "fig:Taipei_Daan_Park_-_Ecological_Water_-_20180715_-_01.jpg")
[Taipei_Daan_Park_-_Ecological_Water_-_20180930_-_03.jpg](https://zh.wikipedia.org/wiki/File:Taipei_Daan_Park_-_Ecological_Water_-_20180930_-_03.jpg "fig:Taipei_Daan_Park_-_Ecological_Water_-_20180930_-_03.jpg")

由台北市政府、台灣大學與大安森林公園之友基金會共同規劃，計畫建設一條從台灣大學校內沿著[新生南路](../Page/新生南路.md "wikilink")，經[龍安國小連接至大安森林公園](../Page/臺北市大安區龍安國民小學.md "wikilink")，全長共兩公里連結台大[醉月湖至大安森林公園內生態池的都市生態草溝](../Page/醉月湖.md "wikilink")「大灣草圳」，2018年已完成位於大安森林公園內之部分\[13\]\[14\]\[15\]\[16\]\[17\]，同時規畫了結合人文、自然生態與科技藝術的方式，配合水圳設計了「台北健森房」，讓民眾可以以運動休憩的方式參與此區域的活水工作。\[18\]

## 歷史

[台灣日治時期](../Page/台灣日治時期.md "wikilink")，台灣總督府於1932年公布的[大台北市都市計畫中](../Page/市區改正.md "wikilink")，首次將此地規劃為七號公園預定地，為當時眾多[臺北市戰前規劃的大型都會公園之一](../Page/臺北市戰前規劃的大型都會公園.md "wikilink")，但是直到1945年台灣日治時期結束，此地大部分區域仍為農田，也並未有任何開始興建公園的詳細計畫。\[19\]\[20\]

進入中華民國時代後，由於此處仍為閒置土地，1949年[國軍](../Page/中華民國國軍.md "wikilink")[空軍通信大隊一度在此建立基地](../Page/中華民國空軍.md "wikilink")，並在此建立[眷村建華新村](../Page/眷村.md "wikilink")，此後包括[憲兵新南營區](../Page/中華民國憲兵.md "wikilink")、憲兵藝工大隊、陸軍眷村岳廬新村、[軍中廣播電台](../Page/漢聲廣播電台.md "wikilink")、[聯勤總部北區印刷廠兼中華民國糧食北區調度中心與供外籍學生居住的](../Page/聯勤.md "wikilink")[國際學舍也陸續設立於此](../Page/國際學舍.md "wikilink")；此外大量因戰爭來到台灣的外省移民，也自行在此區域搭建違章建築，此地當時也成為台北市內違章建築戶最多的區域之一，1983年的電影[搭錯車即以當時的此地做為劇情的背景](../Page/搭錯車_\(電影\).md "wikilink")。也因為建華新村及岳廬新村兩個眷村，加上生活與此的違建戶，合計居民人數曾多達數千人，在當時在此地也曾設有龍崗里、龍飛里、萬龍里等多達四個[里](../Page/村里.md "wikilink")。\[21\]\[22\]\[23\]\[24\]\[25\]

但在1956年台北市的計畫公告中，仍延續台灣日治時期的規劃，將此地保留為七號公園預定地，此後1974年行政院也發函台北市政府要求盡速闢建七號公園；1984年台北市政府再次提出七號公園的興建計畫，原先規劃做為自然森林公園，但當時民間提議規劃在此興建[體育館](../Page/體育館.md "wikilink")，一度獲得台北市議會的支持，引起當時極大爭議，最終在1989年才確認維持森林公園的型態。\[26\]\[27\]\[28\]

1992年4月1日，[臺北市政府開始拆遷公園預定地區域的地上建築](../Page/臺北市政府.md "wikilink")\[29\]，至此公園正式動工興建。公園完成後於1994年3月29日對外開放使用，並命名為「大安森林公園」\[30\]\[31\]，同時由於原地住戶均已拆遷，園內再無居民，原大安森林公園所在地之整併後剩下的龍飛里及萬龍里再次被裁併入[龍門里](../Page/龍門里_\(台北市\).md "wikilink")。

2013年11月24日，[台北捷運](../Page/台北捷運.md "wikilink")[信義線開通](../Page/信義線.md "wikilink")，設於公園旁的捷運[大安森林公園站正式啟用](../Page/大安森林公園站.md "wikilink")。

### 爭議

[Kannon_Statue_in_Daan_Park_Nearby_Northwest_Entrance.jpg](https://zh.wikipedia.org/wiki/File:Kannon_Statue_in_Daan_Park_Nearby_Northwest_Entrance.jpg "fig:Kannon_Statue_in_Daan_Park_Nearby_Northwest_Entrance.jpg")

  - 觀音立像

在公園開放前，位於園內一尊由附近[大雄精舍於](../Page/大雄精舍.md "wikilink")1985年請知名雕塑家[楊英風所設計放置的](../Page/楊英風.md "wikilink")[持珠觀音石像](../Page/觀音.md "wikilink")，由於在拆除建築時未與周邊建築一同被拆除，曾引發同是位於公園周邊的[基督教](../Page/基督教.md "wikilink")[臺北靈糧堂信徒抗議](../Page/臺北靈糧堂.md "wikilink")；經各方協調後，市府最終以公共藝術的角度決定保留觀音像，將該像定義為藝術品，禁止任何膜拜的宗教行為。\[32\]\[33\]\[34\]\[35\]\[36\]

  - 土壤問題

2014年大安森林公園之友基金會請台灣及國際多位樹醫到大安森林公園會勘後，發現園內土壤有一萬年前[海嘯衝來的](../Page/海嘯.md "wikilink")[沖繩](../Page/沖繩.md "wikilink")[黏土](../Page/黏土.md "wikilink")，厚度約60到75公分，再加上公園開闢時欠缺園藝專業人士參與規劃，覆土厚度只達到標準值的三分之一，甚至[土壤內還有當時拆除房舍後沒清理乾淨的房屋廢料](../Page/土壤.md "wikilink")，使得[樹木不易生長](../Page/樹木.md "wikilink")。預定將分5年逐步改善，以利園內樹木生長。\[37\]\[38\]\[39\]

## 地理位置

  - 行政劃分上屬於[龍門里六鄰](../Page/龍門里_\(台北市\).md "wikilink")。
  - 公園範圍：
      - 東面為[建國南路二段](../Page/建國南北路_\(臺北市\).md "wikilink")
      - 南面為[和平東路二段](../Page/和平東路.md "wikilink")
      - 西面為[新生南路二段](../Page/新生南路.md "wikilink")
      - 北面為[信義路三段](../Page/信義路_\(台北市\).md "wikilink")

### 交通資訊

[捷運大安森林公園站_MRT_Daan_Park_Station_-_panoramio.jpg](https://zh.wikipedia.org/wiki/File:捷運大安森林公園站_MRT_Daan_Park_Station_-_panoramio.jpg "fig:捷運大安森林公園站_MRT_Daan_Park_Station_-_panoramio.jpg")

  - 捷運車站

[台北捷運](../Page/台北捷運.md "wikilink")[大安森林公園站](../Page/大安森林公園站.md "wikilink")

  - 公車站

<!-- end list -->

  - 信義路三段：捷運大安森林公園站
  - 和平東路二段：大安森林公園站
  - 建國南路二段：大安國宅站
  - 新生南路二段：信義新生路口站、金華新生路口站、和平新生路口站

<!-- end list -->

  - 自行開車

由[國道一號](../Page/中山高速公路.md "wikilink")（中山高速公路）下[圓山交流道](../Page/圓山交流道.md "wikilink")，接[建國高架道路後於](../Page/建國高架道路.md "wikilink")[信義路匝道下高速道路後直行可抵達公園地下停車場入口](../Page/信義路_\(台北市\).md "wikilink")。

## 照片集

<File:Daan> Park 061021.JPG| 公園位於信義路、新生南路口的一號入口 <File:Daan> Park No.8
Entrance and Exit Birdview 20140727.jpg| 八號入口 <File:2010> 07 21030 6731
Da'an District, Taipei, Daan Park, Taiwan.JPG| 園內步道 <File:2010> 07 21130
6752 Da'an District, Taipei, Daan Park, Taiwan.JPG| 園內步道 <File:2010> 07
20760 6690 Da'an District, Taipei, Daan Park, Taiwan.JPG| 園內草坪
<File:2010> 07 21360 6795 Da'an District, Taipei, Daan Park, Taiwan.JPG|
園內景觀 <File:Taipei> Daan Park - Coconut Tree - 20180805 - 01.jpg| 園內景觀
<File:2010> 07 21060 6741 Da'an District, Taipei, Daan Park, Pavilions,
Taiwan.JPG| 園內涼亭 <File:Taipei> Daan Park - Skates Field - 20180805 -
01.jpg| 直排輪場 <File:Taipei> Daan Park - Basketball Court - 20180805 -
01.jpg| 籃球場 <File:Taipei> Daan Park - Weather station - 20180805 -
08.jpg| 中央氣象局設於園內的氣象觀測點

## 参考資料

## 參見

  - [文心森林公園](../Page/文心森林公園.md "wikilink")
  - [嘉義都會森林公園](../Page/嘉義公園.md "wikilink")
  - [坪林森林公園](../Page/坪林森林公園.md "wikilink")
  - [凹仔底森林公園](../Page/凹仔底森林公園.md "wikilink")
  - [右昌森林公園](../Page/右昌森林公園.md "wikilink")
  - [台東森林公園](../Page/台東森林公園.md "wikilink")
  - [過嶺森林公園](../Page/過嶺森林公園.md "wikilink")

## 外部連結

  - [大安森林公園 -
    台北旅遊網](https://www.travel.taipei/zh-tw/attraction/details/190)
  - [大安森林公園 -
    公園走透透](https://parks.taipei/parks/m2/pkl_parks_m2C.php?sid=154)
  - [大安森林公園之友基金會](http://www.daanforestpark.org.tw/)
      -
  - [台北健森房](https://ecogym.taipei/) - 位於大安森林公園內生態水圳旁的休憩設施

[Category:台北市公園](../Category/台北市公園.md "wikilink") [Category:大安區
(臺北市)](../Category/大安區_\(臺北市\).md "wikilink")
[Category:森林公園](../Category/森林公園.md "wikilink")
[Category:1994年台灣建立](../Category/1994年台灣建立.md "wikilink")

1.

2.

3.

4.
5.

6.

7.

8.

9.
10.

11.

12.

13.
14.

15.

16.

17.

18.

19.

20.

21.
22.

23.

24.

25.

26.
27.
28.

29.

30.
31.
32.
33.
34.
35.

36.

37.

38.

39.