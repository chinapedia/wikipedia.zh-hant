[Tannic_acid.svg](https://zh.wikipedia.org/wiki/File:Tannic_acid.svg "fig:Tannic_acid.svg")，通过将10当量的苯丙烷衍生的[没食子酸酯化为来自初级代谢的单糖](../Page/没食子酸.md "wikilink")（葡萄糖）核心而形成。\]\]
[Phenol-phenolate_equilibrium.svg](https://zh.wikipedia.org/wiki/File:Phenol-phenolate_equilibrium.svg "fig:Phenol-phenolate_equilibrium.svg")[芳香反应性](../Page/芳香性.md "wikilink")\]\]

**多酚**\[1\]\[2\]（；也称为多羟基苯酚），是一种主要是天然的结构类，但也是[合成或](../Page/合成.md "wikilink")[半合成的](../Page/半合成.md "wikilink")[有机化合物](../Page/有机化合物.md "wikilink")，其特征在于存在大量的[酚结构单元](../Page/酚.md "wikilink")。这些[酚结构的数量和特征构成了该类特定成员独特的物理](../Page/酚.md "wikilink")，化学和生物（代谢，毒性，治疗等）特性。
实例包括[单宁酸](../Page/单宁酸.md "wikilink")（右图）和（下图）。
历史上重要的[鞣质化学类是多酚的一个子集](../Page/鞣质.md "wikilink").\[3\]\[4\]。

多酚在一些植物中起到了呈现颜色的作用，如[秋天的](../Page/秋天.md "wikilink")[叶子](../Page/叶子.md "wikilink")。

多酚类物质具有很强的[抗氧化作用](../Page/抗氧化.md "wikilink")，常見的多酚化合物有：[兒茶素](../Page/兒茶素.md "wikilink")、[綠原酸](../Page/綠原酸.md "wikilink")、[異黃酮](../Page/異黃酮.md "wikilink")、[花青素](../Page/花青素.md "wikilink")、[可可多酚](../Page/可可多酚.md "wikilink")、[薑黃素](../Page/薑黃素.md "wikilink")、[檸檬黃素](../Page/檸檬黃素.md "wikilink")、[槲皮素](../Page/槲皮素.md "wikilink")(quercetin)
\[5\]、[芸香苷](../Page/芸香苷.md "wikilink")\[6\](*rutin*)\[7\]、[白藜蘆醇](../Page/白藜蘆醇.md "wikilink")\[8\]等。[绿茶](../Page/绿茶.md "wikilink")、[葡萄及深色的蔬果都是多酚类物质的一个来源](../Page/葡萄.md "wikilink")。

## 参考文献

## 参见

  - [黄酮类](../Page/黄酮类.md "wikilink")
  - [茶多酚](../Page/茶多酚.md "wikilink")：[茶黄素](../Page/茶黄素.md "wikilink")、[单宁酸](../Page/单宁酸.md "wikilink")
  - [棉酚](../Page/棉酚.md "wikilink")

## 外部連結

{{-}}

[多酚](../Category/多酚.md "wikilink")
[Category:植物化学成分](../Category/植物化学成分.md "wikilink")
[Category:膳食抗氧化剂](../Category/膳食抗氧化剂.md "wikilink")

1.

2.

3.
4.

5.
6.

7.

8.