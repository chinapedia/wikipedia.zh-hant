**都農町**（）是位於[日本](../Page/日本.md "wikilink")[宮崎縣中部的一個](../Page/宮崎縣.md "wikilink")[城鎮](../Page/城鎮.md "wikilink")，屬[兒湯郡](../Page/兒湯郡.md "wikilink")。

## 历史

1977年[日本國有鐵道曾在此興建](../Page/日本國有鐵道.md "wikilink")[磁浮列車的測試路線](../Page/磁浮列車.md "wikilink")，[國鐵分割民營化後由](../Page/國鐵分割民營化.md "wikilink")[鐵道綜合技術研究所承接](../Page/鐵道綜合技術研究所.md "wikilink")，整個測試案於1996年結束。

2010年3月，町內的農家經檢驗證實水牛感染[口蹄疫](../Page/口蹄疫.md "wikilink")，之後兩個月周邊區域也陸續證實有其他感染，造成之事件，直到7月份才確認疫情平息。

### 年表

  - 1889年5月1日：實施[町村制](../Page/町村制.md "wikilink")，設置都農村。
  - 1920年8月1日：改制為**都農町**。

## 行政

  - 町長：河野正和（2007年就任）

## 交通

### 鐵道

  - [九州旅客鐵道](../Page/九州旅客鐵道.md "wikilink")
      - [日豐本線](../Page/日豐本線.md "wikilink")：[東都農車站](../Page/東都農車站.md "wikilink")
        - [都農車站](../Page/都農車站.md "wikilink")

### 道路

  - [高速道路](../Page/高速道路.md "wikilink")

<!-- end list -->

  - [東九州自動車道](../Page/東九州自動車道.md "wikilink")：[都農交流道](../Page/都農交流道.md "wikilink")

## 觀光資源

  - 矢研瀑布：日本瀑布百選之一
  - [都農神社](../Page/都農神社.md "wikilink")：[日向國](../Page/日向國.md "wikilink")[一宮](../Page/一宮.md "wikilink")

## 教育

### 高等學校

  - [宮崎縣立都農高等學校](../Page/宮崎縣立都農高等學校.md "wikilink")

## 姊妹、友好都市

### 日本

  - [糸滿市](../Page/糸滿市.md "wikilink")（[沖繩縣](../Page/沖繩縣.md "wikilink")）：在[二次大戰期間](../Page/二次大戰.md "wikilink")，糸滿市的學童被疏散至此，因此促成兩地的交流。兩地於1993年12月締結為姊妹都市。\[1\]
  - [佐呂間町](../Page/佐呂間町.md "wikilink")（[北海道](../Page/北海道.md "wikilink")[常呂郡](../Page/常呂郡.md "wikilink")）：兩地同為[東洋輪胎的測試場地](../Page/東洋輪胎.md "wikilink")。\[2\]

## 參考資料

## 外部連結

  - [都農町商工會](http://www.miya-shoko.or.jp/tsuno/)

  - [都農町民圖書館](http://lib.town.tsuno.miyazaki.jp/)

<!-- end list -->

1.

2.