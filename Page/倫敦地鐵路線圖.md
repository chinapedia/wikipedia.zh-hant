[Composite_Beck_and_2012_tube_map.png](https://zh.wikipedia.org/wiki/File:Composite_Beck_and_2012_tube_map.png "fig:Composite_Beck_and_2012_tube_map.png")

[London_Underground_Zone_1.svg](https://zh.wikipedia.org/wiki/File:London_Underground_Zone_1.svg "fig:London_Underground_Zone_1.svg")
**伦敦地铁线路图**（the London Underground Map or the TFL Services
Map）即英国伦敦的所有[快捷轨道交通系统](../Page/快捷轨道交通.md "wikilink")（包括[伦敦地铁](../Page/伦敦地铁.md "wikilink")、[码头区轻轨铁路](../Page/码头区轻轨铁路.md "wikilink")、[伦敦地上铁和](../Page/伦敦地上铁.md "wikilink")[阿聯酋空中纜車](../Page/阿聯酋空中纜車.md "wikilink")）的交通线路地图。由于伦敦地铁也称“the
Tube”（字面义：管子），伦敦地铁线路图在英语中常叫做“**Tube map**”（字面义：管子地图）。

伦敦地铁线路图是一种示意简图，它只反映各个车站大概的相对位置、中转情况、所在的费用区域，而不反映车站真实的地理状况、具体的距离和位置等，这种[拓扑地图的设计理念后来被全世界地铁线路图设计者所广泛地使用](../Page/拓扑地图.md "wikilink")。

早期的伦敦地铁图并非为这种拓扑地图。1931年，由英国工程师[哈利·貝克](../Page/哈利·貝克.md "wikilink")（Harry
Beck）设计的伦敦地铁图使用了这种理念，它具有三个比较明显的特点：

1.  以颜色区分路线
2.  路线大多以水平、垂直、45度角三种形式来表现
3.  路线上的车站距离与实际距离不成比例关系

如今全球各大城市的地铁系统官方大多採用这种设计原则绘制地铁线路图。当然，这种原理图的缺点对访客或观光客而言，不依实际比例可能造成旅程和时间的误判，而且无法提供其它地理信息，像巴士地图的车站过密而不适用这个原理图。所以，某些城市的交通图采取混合式设计，在地图上增加采用原理图设计、但在街道和服务设施略微调整过比例和标示方式（如纽约巴士地图）。

## 相关条目

  - [城市轨道交通系统](../Page/城市轨道交通系统.md "wikilink")
  - [伦敦地铁](../Page/伦敦地铁.md "wikilink")

## 外部链接

  - [倫敦地鐵官方網站](https://web.archive.org/web/20050411075342/http://tube.tfl.gov.uk/)（[地铁路线图页面](http://www.tfl.gov.uk/gettingaround/1106.aspx)）
  - [London metro map at
    metros.hu](http://metros.hu/terkep/k/london.html)

[Category:城市轨道交通](../Category/城市轨道交通.md "wikilink")
[Category:倫敦地鐵](../Category/倫敦地鐵.md "wikilink")
[Category:地圖](../Category/地圖.md "wikilink")