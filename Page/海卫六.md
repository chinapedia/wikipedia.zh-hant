| [<File:Galatea> moon.jpg](https://zh.wikipedia.org/wiki/File:Galatea_moon.jpg "fig:File:Galatea moon.jpg") |
| ---------------------------------------------------------------------------------------------------------- |
| 发现                                                                                                         |
| **发现者**                                                                                                    |
| **发现日**                                                                                                    |
| 轨道特性                                                                                                       |
| **[长轴](../Page/长轴.md "wikilink")**                                                                         |
| **[偏心率](../Page/偏心率.md "wikilink")**                                                                       |
| **[轨道周期](../Page/轨道周期.md "wikilink")**                                                                     |
| **[倾角](../Page/倾角.md "wikilink")**                                                                         |
| **[行星](../Page/行星.md "wikilink")**                                                                         |
| 物理特性                                                                                                       |
| **平均[直径](../Page/直径.md "wikilink")**                                                                       |
| **表面[面积](../Page/面积.md "wikilink")**                                                                       |
| **[体积](../Page/体积.md "wikilink")**                                                                         |
| **[质量](../Page/质量.md "wikilink")**                                                                         |
| **平均[密度](../Page/密度.md "wikilink")**                                                                       |
| **表面[引力加速度](../Page/引力加速度.md "wikilink")**                                                                 |
| **[逃逸速度](../Page/逃逸速度.md "wikilink")**                                                                     |
| **[自转周期](../Page/自转周期.md "wikilink")**                                                                     |
| **[轴倾斜度](../Page/轴倾斜度.md "wikilink")**                                                                     |
| **[反照率](../Page/反照率.md "wikilink")**                                                                       |
| **表面[温度](../Page/温度.md "wikilink")**                                                                       |
| 大气特性                                                                                                       |
| 無                                                                                                          |

style="margin: inherit; font-size: larger;"|**海卫六**

**海卫六**(S/1989 N 4,
Galatea)是环绕[海王星运行的一颗](../Page/海王星.md "wikilink")[卫星](../Page/卫星.md "wikilink")，以[希臘神話中](../Page/希臘神話.md "wikilink")[涅瑞伊得斯之一的](../Page/涅瑞伊得斯.md "wikilink")[伽拉忒亞命名](../Page/伽拉忒亞.md "wikilink")。

## 概述

由[航海家2號於](../Page/航海家2號.md "wikilink")1989年7月底所觀測到，並於同年的8月2日公開成果；並給予S/1989
N 4的暫時編號，但公開內容僅有5天內所拍攝到的10張照片。

## 表面特徵

海衛六是從海王星內側算來第四顆衛星，表面十分的凹凸不平；但是根據觀察結果顯示，似乎並不是因為地質運動所造成的。可能的造成原因是因為海王星捕獲[海衛一時](../Page/海衛一.md "wikilink")，海衛一初期軌道的干擾；使衛星的碎片再度結合在一起，而形成海衛六的。

由於海衛六十分靠近海王星，受[潮汐力的影響](../Page/潮汐力.md "wikilink")；持續靠近海王星，未來有可能因為小於[洛希極限而墜毀於海王星大氣或是形成一個環](../Page/洛希極限.md "wikilink")。

[M06](../Category/海王星的卫星.md "wikilink")