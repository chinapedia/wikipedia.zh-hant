**托尔讷河**（芬兰语：，瑞典语：，北萨米语：）是位于[瑞典北部和](../Page/瑞典.md "wikilink")[芬兰的河流](../Page/芬兰.md "wikilink")。大约河流的经过一半地方是两个国家的边界。它发源于[挪威的](../Page/挪威.md "wikilink")[托尔讷湖](../Page/托尔讷湖.md "wikilink")，向东南方缓慢流动，全长522公里，流入[波的尼亚湾](../Page/波的尼亚湾.md "wikilink")。以其长度和流域的面積計算，它都是瑞典[北博滕省最大的河流](../Page/北博滕省.md "wikilink")。

根据1809年签订的《[弗雷德里卡条约](../Page/弗雷德里卡条约.md "wikilink")》，瑞典的部分土地划归[俄罗斯帝国](../Page/俄罗斯帝国.md "wikilink")，这些失去的土地以前属于芬兰。托尔讷河和[穆奥尼奥河](../Page/穆奥尼奥河.md "wikilink")、[肯凯迈河一起成为瑞典和俄罗斯新的](../Page/肯凯迈河.md "wikilink")[芬蘭大公國的分界线](../Page/芬蘭大公國.md "wikilink")。

托尔讷河两岸的城市往往同时拥有瑞典名称和芬兰名称，如瑞典的[哈帕兰达](../Page/哈帕兰达.md "wikilink")（瑞典语：Haparanda，芬兰语：Haaparanta）和芬兰的[托尔尼奥](../Page/托尔尼奥.md "wikilink")（芬兰语：Tornio，瑞典语：Torneå）。
[Tornealven.jpg](https://zh.wikipedia.org/wiki/File:Tornealven.jpg "fig:Tornealven.jpg")

## 参看

  - [托尔讷河谷](../Page/托尔讷河谷.md "wikilink")

## 參考資料

[T](../Category/瑞典河流.md "wikilink") [T](../Category/芬兰河流.md "wikilink")