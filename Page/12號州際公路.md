**12號州際公路**（****，簡稱**I-12**）是[美國](../Page/美國.md "wikilink")[州際公路系統的一部份](../Page/州際公路系統.md "wikilink")，全部位於[路易斯安那州](../Page/路易斯安那州.md "wikilink")。全長85.59哩（137.74公里），呈東西走向。

1993年[州議會為其命名為](../Page/路易斯安那州议会.md "wikilink")**[西佛罗里达共和国高速公路](../Page/西佛罗里达共和国.md "wikilink")**
(Republic of West Florida Parkway)。

## 沿革

## 出口列表

<table>
<thead>
<tr class="header">
<th><p><big>途經主要城鎮</big><br />
<small>粗體字為使用在交通標誌上的正式指定定點城市</small></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><ul>
<li><strong><a href="../Page/巴吞鲁日_(路易斯安那州).md" title="wikilink">巴吞鲁日</a></strong></li>
<li><strong><a href="../Page/哈蒙德_(路易斯安那州).md" title="wikilink">哈蒙德</a></strong></li>
<li><strong><a href="../Page/斯莱德尔_(路易斯安那州).md" title="wikilink">斯莱德尔</a></strong></li>
</ul></td>
</tr>
</tbody>
</table>

## 參考資料

<div class="references-small">

<references />

</div>

[Category:州際公路系統](../Category/州際公路系統.md "wikilink")