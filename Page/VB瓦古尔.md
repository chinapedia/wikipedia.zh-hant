**VB瓦古尔**（**VB
Vágur**）是法罗群岛的足球俱乐部，位于[法罗群岛的](../Page/法罗群岛.md "wikilink")[瓦古尔](../Page/瓦古尔.md "wikilink")，球队现属于[法罗群岛足球甲级联赛](../Page/法罗群岛足球甲级联赛.md "wikilink")。球队的主体育场为。

## 球队荣誉

  - **[法罗群岛足球甲级联赛](../Page/法罗群岛足球甲级联赛.md "wikilink")：1次**
      - 2000

<!-- end list -->

  - **[法罗群岛杯](../Page/法罗群岛杯.md "wikilink")：1次**
      - 1974

## 欧洲赛场

  - 1Q = 资格赛第一轮

<table>
<thead>
<tr class="header">
<th><p>赛季</p></th>
<th><p>赛事</p></th>
<th><p>轮数</p></th>
<th></th>
<th><p>俱乐部</p></th>
<th><p>比分</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>2001/02</p></td>
<td><p><a href="../Page/欧洲冠军联赛.md" title="wikilink">欧洲冠军联赛</a></p></td>
<td><p>1Q</p></td>
<td></td>
<td><p><a href="../Page/莫兹尔斯拉维亚.md" title="wikilink">莫兹尔斯拉维亚</a></p></td>
<td><p>0-0, 0-5</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

[Category:法罗群岛足球俱乐部](../Category/法罗群岛足球俱乐部.md "wikilink")