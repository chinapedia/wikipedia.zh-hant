**馬燧**（），字洵美，[汝州](../Page/汝州_\(古代\).md "wikilink")[郟城](../Page/郏城县.md "wikilink")（今[河南省](../Page/河南省.md "wikilink")[郟縣](../Page/郟縣.md "wikilink")）人。[唐朝官员](../Page/唐朝官员.md "wikilink")、[武将](../Page/武将.md "wikilink")。[嶽麓山](../Page/嶽麓山.md "wikilink")[道林精舍之創建人](../Page/道林精舍.md "wikilink")。

## 家世

祖上自[右扶風遷居至汝州](../Page/右扶風.md "wikilink")。

  - 五代祖：马岫，梁安州刺史，西魏授荆州刺史，西梁赠太尉、荆州牧，谥曰肃。
  - 高祖：马乔卿，梁襄州主簿。
  - 曾祖：马君才，隋末为蓟令。窦建德、高开道等攻逼四境，以保境功迁上大将军、开府仪同三司。唐朝建立后，奉燕王[李艺表入奏](../Page/罗艺.md "wikilink")，擢拜右武候大将军、封南阳郡公。尚书郎[李华为其撰碑文](../Page/李华.md "wikilink")。
  - 祖父：馬珉，州举明经高第，三命为开州万岁令，官至左玉鈐衛倉曹。赠工部尚书。
  - 父親：馬季龍，武后时，建安王[武攸宜镇守蓟州](../Page/武攸宜.md "wikilink")，在其部下任职。开元年间，在[萧嵩](../Page/萧嵩.md "wikilink")、李暠手下任右金吾卫郎将、大同军副使、岚州刺史。累赠尚书左仆射。
  - 三兄：[马炫](../Page/马炫.md "wikilink")（713年—791年10月4日），字抱元，任郓州、阆州刺史、大理少卿、润州刺史，宰相[柳载荐为太子右庶子](../Page/柳载.md "wikilink")，迁左散骑常侍，转刑部侍郎，以兵部尚书、汉阳郡公致仕。年79卒，赠太子少保。子马陶，孙马寅、马贻庆、马邈。

## 生平

馬燧少與諸兄一起學習，歎曰：“方天下有事，丈夫當以功濟四海，渠老一儒哉？”，又攻兵書戰策，多謀略。

[天寶十四載](../Page/天宝_\(唐朝\).md "wikilink")（755年），[安史之亂爆發時](../Page/安史之亂.md "wikilink")，曾勸[范陽](../Page/范陽.md "wikilink")（今北京城西南）[留守](../Page/留守.md "wikilink")[賈循倒戈](../Page/賈循.md "wikilink")，事泄逃脱。[宝应中](../Page/宝应_\(年号\).md "wikilink")，被[泽潞节度使](../Page/泽潞节度使.md "wikilink")[李抱玉推荐为](../Page/李抱玉.md "wikilink")[赵城](../Page/赵城县.md "wikilink")[尉](../Page/县尉.md "wikilink")，知[仆固怀恩必反](../Page/仆固怀恩.md "wikilink")。迁左武卫[兵曹参军](../Page/兵曹参军.md "wikilink")。[大历十年](../Page/大历.md "wikilink")（775年），任河阳三城使。大历十四年（779年），迁[河东节度使](../Page/河东节度使.md "wikilink")，威震北方。[建中二年](../Page/建中_\(唐朝\).md "wikilink")（781年），加[检校](../Page/检校.md "wikilink")[兵部尚书](../Page/兵部尚书.md "wikilink")，奉诏讨[魏博叛将](../Page/魏博節度使.md "wikilink")[田悦](../Page/田悦.md "wikilink")，以解临洺（今河北永年）、邢州（今邢台）之围。斩田悦部将杨朝光以下5000余人，至临洺，大破田悦军。以功加[尚书右仆射](../Page/尚书右仆射.md "wikilink")。

[贞元元年](../Page/貞元_\(唐朝\).md "wikilink")（785年）三月，败怀光军于陶城（今永济北），破怀光军于[长春宫](../Page/长春宫_\(陕西\).md "wikilink")（今陕西大荔朝邑西北），怀光自杀，河中平定。迁[光禄大夫](../Page/光禄大夫.md "wikilink")。贞元二年（786年），[吐蕃寇边](../Page/吐蕃.md "wikilink")，攻陷盐（今宁夏盐池县北）、夏（今陕西横山县西）二州，馬燧率军出击。貞元三年四月（787年5月），因輕信吐蕃，會盟於[平涼](../Page/平涼.md "wikilink")（今甘肃省平凉市），[尚结赞预先埋伏骑兵于盟坛西部](../Page/尚结赞.md "wikilink")，數萬吐蕃骑兵一起杀出，[浑瑊逃脫](../Page/浑瑊.md "wikilink")，招致[平涼會盟之劫](../Page/平涼會盟.md "wikilink")，史稱[平凉劫盟](../Page/平凉劫盟.md "wikilink")。吐蕃尚结赞设下的圈套，“唐之名将，李晟与马燧、浑瑊耳，不去三人，必为我忧”\[1\]，果然被奪兵權，備受[德宗冷落](../Page/唐德宗.md "wikilink")。为[司徒兼](../Page/司徒.md "wikilink")[侍中](../Page/侍中.md "wikilink")。贞元五年，马燧和太尉[李晟的画相入](../Page/李晟_\(唐朝\).md "wikilink")[凌烟阁](../Page/凌烟阁.md "wikilink")。贞元九年，德宗於[延英殿召见马燧](../Page/延英殿.md "wikilink")，马燧因为足疾跌倒，皇帝亲自扶起。贞元十一年（795年），病逝，德宗为之[輟朝四日](../Page/輟朝.md "wikilink")，[追赠](../Page/追赠.md "wikilink")[太尉](../Page/太尉.md "wikilink")，[谥号莊武](../Page/谥号.md "wikilink")。

## 子孙

  - [马彙](../Page/马彙.md "wikilink")，太僕少卿
      - [马赦](../Page/马赦.md "wikilink")，左衛倉曹參軍
      - 马○，右清道率府倉曹參軍
  - [马暢](../Page/马暢.md "wikilink")，少府監
      - [马繼祖](../Page/马繼祖.md "wikilink")，殿中少監

## 注釋

<div class="references-small">

<references />

</div>

[Category:唐朝县尉](../Category/唐朝县尉.md "wikilink")
[Category:唐朝兵曹参军](../Category/唐朝兵曹参军.md "wikilink")
[Category:河東節度使](../Category/河東節度使.md "wikilink")
[Category:唐朝检校兵部尚书](../Category/唐朝检校兵部尚书.md "wikilink")
[Category:唐朝尚书右仆射](../Category/唐朝尚书右仆射.md "wikilink")
[Category:唐朝光禄大夫](../Category/唐朝光禄大夫.md "wikilink")
[Category:唐朝司徒](../Category/唐朝司徒.md "wikilink")
[Category:唐朝侍中](../Category/唐朝侍中.md "wikilink")
[Category:魏博节度使](../Category/魏博节度使.md "wikilink")
[Category:奉诚节度使](../Category/奉诚节度使.md "wikilink")
[Category:唐朝追赠太尉](../Category/唐朝追赠太尉.md "wikilink")
[Category:郟縣人](../Category/郟縣人.md "wikilink")
[Category:諡莊武](../Category/諡莊武.md "wikilink")
[S](../Category/马姓.md "wikilink")

1.  《新唐書》列傳第七十九李晟傳