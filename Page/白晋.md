[thumb](../Page/file:Joachim_Bouvet_Etat_present_de_la_Chine_1697.jpg.md "wikilink")

**白晋**（，）
，一作**白进**，[字](../Page/表字.md "wikilink")**明远**，原名**若阿基姆·布韋**，[耶稣会](../Page/耶稣会.md "wikilink")[法国](../Page/法国.md "wikilink")[传教士](../Page/传教士.md "wikilink")。

白晋出生于法国[利曼](../Page/利曼.md "wikilink")，1678年加入耶稣会，1684年受[法王](../Page/法國國王.md "wikilink")[路易十四选派出使](../Page/路易十四.md "wikilink")[中国传教](../Page/中国.md "wikilink")，出发前被授予“国王[数学家](../Page/数学家.md "wikilink")”称号，入[法国科学院为院士](../Page/法国科学院.md "wikilink")。同行者有：[洪若翰](../Page/洪若翰.md "wikilink")、[刘应](../Page/刘应.md "wikilink")、[塔夏尔](../Page/塔夏尔.md "wikilink")、[李明和](../Page/李明.md "wikilink")[张诚](../Page/张诚_\(传教士\).md "wikilink")。

1685年3月3日，使团自[布雷斯特起航](../Page/布雷斯特.md "wikilink")，途经[暹罗时](../Page/暹罗.md "wikilink")，塔夏尔被[暹罗国王留用](../Page/暹罗.md "wikilink")。其余五人于[康熙二十六年](../Page/康熙.md "wikilink")（1687年）抵达[浙江](../Page/浙江.md "wikilink")[宁波](../Page/宁波.md "wikilink")。因海禁未开，洋人不能深入内地，清政府令其回国。但经[南怀仁说明他们为法王所遣](../Page/南怀仁.md "wikilink")，精于天文历法。次年入[北京](../Page/北京.md "wikilink")，白晋与张诚为康熙留用，随侍宫中，其他三人回浙江。白晋为康熙讲授[欧几里得几何](../Page/欧几里得几何.md "wikilink")\[1\]。康熙三十二年（1693年），康熙派遣白晋为使出使法国。

康熙四十七年（1708年），白晋、[雷孝思等传教士](../Page/雷孝思.md "wikilink")，奉帝命测绘中国地图；白晋才出京门，因座马受惊，跌落马下，腰痛不能继续前行，留[陕西](../Page/陕西.md "wikilink")[神木县养病](../Page/神木县.md "wikilink")，後返[北京休养](../Page/北京.md "wikilink")，集各传教士所绘分图，汇成全中国总图。康熙五十六年（1717年），赐名为《[皇舆全览图](../Page/康熙皇輿全覽圖.md "wikilink")》\[2\]。

## 著作

  - 《中国皇帝康熙传》Portrait historique de l'empereur de la Chine (Paris, 1697)
  - 《中國現況圖像》L'Etat présent de la Chine (Paris, 1697)
  - 《天學本義》
  - 《古今敬天鑒》
  - 《易考》

## 参考文献

  - 韓琦：〈[白晉（Joachim
    Bouvet）的《易經》研究和康熙時代的「西學中源」說](http://ccsdb.ncl.edu.tw/ccs/image/01_016_001_01_08.pdf)〉。

{{-}}

[Category:在清朝的耶稣会神父](../Category/在清朝的耶稣会神父.md "wikilink")
[Category:死在清朝的法国人](../Category/死在清朝的法国人.md "wikilink")
[Category:法国天主教传教士](../Category/法国天主教传教士.md "wikilink")
[Category:法兰西科学院院士](../Category/法兰西科学院院士.md "wikilink")
[Category:法国汉学家](../Category/法国汉学家.md "wikilink")
[Category:法國數學家](../Category/法國數學家.md "wikilink")

1.  [阎宗临](../Page/阎宗临.md "wikilink")《从西方典籍所见康熙与耶稣会之关系》第七节
2.  阎宗临《关于白晋测绘皇舆全览图之资料》