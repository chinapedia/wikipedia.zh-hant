**碘甲烷**是一个[卤代烃](../Page/卤代烃.md "wikilink")，[分子式写为CH](../Page/分子式.md "wikilink")<sub>3</sub>I、[MeI](../Page/甲基.md "wikilink")，是[甲烷的一](../Page/甲烷.md "wikilink")[碘取代物](../Page/碘.md "wikilink")。室温下为密度大的挥发性液体，[偶极矩](../Page/偶极矩.md "wikilink")1.59D，[折射率](../Page/折射率.md "wikilink")1.5304（20°C、D）、1.5293（21°C、D）。碘甲烷可与常见的有机溶剂混溶，纯品无色，暴露于阳光下会分解出[I<sub>2</sub>而带紫色](../Page/碘.md "wikilink")，可通过加入金属[铜或](../Page/铜.md "wikilink")[银去除](../Page/银.md "wikilink")。自然界中碘甲烷少量存在于稻田中。\[1\]温带海洋中的藻类以及陆地上的某些真菌和细菌也可以代谢产生碘甲烷

碘甲烷是[有机合成中常用的](../Page/有机合成.md "wikilink")[甲基化试剂](../Page/甲基化.md "wikilink")。

## 化学性质

碘甲烷是[S<sub>N</sub>2取代反应的理想](../Page/SN2.md "wikilink")[底物](../Page/底物.md "wikilink")，一方面由于其[位阻效应很小](../Page/位阻效应.md "wikilink")，便于[亲核试剂进攻](../Page/亲核试剂.md "wikilink")；另一方面由于[碘离子是很好的](../Page/碘离子.md "wikilink")[离去基团](../Page/离去基团.md "wikilink")。因此，碘甲烷可用于对[酚和](../Page/酚.md "wikilink")[羧酸进行](../Page/羧酸.md "wikilink")[甲基化](../Page/甲基化.md "wikilink")：\[2\]

  -
    [Iodomethane_rxn1.png](https://zh.wikipedia.org/wiki/File:Iodomethane_rxn1.png "fig:Iodomethane_rxn1.png")

这些例子中，碱（[K<sub>2</sub>CO<sub>3</sub>或](../Page/碳酸钾.md "wikilink")[Li<sub>2</sub>CO<sub>3</sub>](../Page/碳酸锂.md "wikilink")）移除羧酸或酚的酸性[质子](../Page/质子.md "wikilink")，使之形成[羧酸根离子和酚盐离子](../Page/羧酸根.md "wikilink")，然后作为亲核试剂发生S<sub>N</sub>2反应。

根据[软硬酸碱理论](../Page/软硬酸碱理论.md "wikilink")，碘是“软”的阴离子，因而碘甲烷所参与的甲基化反应也应在[两位亲核试剂较](../Page/两位亲核试剂.md "wikilink")“软”的一段发生。比如，在与[硫氰酸根离子反应时](../Page/硫氰酸根.md "wikilink")，S更软，因而从[S原子进攻的可能性较从](../Page/硫.md "wikilink")[N原子进攻的可能性大](../Page/氮.md "wikilink")，产物主要是[硫氰酸甲酯](../Page/硫氰酸甲酯.md "wikilink")（CH<sub>3</sub>SCN）而非CH<sub>3</sub>NCS。而且，碘甲烷与1,3-[二羰基化合物生成的](../Page/二羰基化合物.md "wikilink")[烯醇盐之间发生的反应也可用以上理论来解释](../Page/烯醇盐.md "wikilink")。相对于碳原子，氧原子较硬，碘甲烷参与的反应也几乎都生成C-甲基化的产物。

虽然一般不用碘代烃来生成[格氏试剂](../Page/格氏试剂.md "wikilink")，但由于[氟甲烷](../Page/氟甲烷.md "wikilink")、[一氯甲烷和](../Page/一氯甲烷.md "wikilink")[溴甲烷在室温下都为气体](../Page/溴甲烷.md "wikilink")，所以碘甲烷也是制取一碳[格氏试剂的主要原料](../Page/格氏试剂.md "wikilink")，生成的格氏试剂称为[碘化甲基镁MeMgI](../Page/碘化甲基镁.md "wikilink")。该生成反应速率很快，常用于演示格氏试剂的制备。碘化甲基镁的应用在某些方面现在已被[甲基锂所取代](../Page/甲基锂.md "wikilink")。

在[蒙山都法中](../Page/蒙山都法.md "wikilink")，[甲醇与](../Page/甲醇.md "wikilink")[碘化氢反应生成的中间体MeI](../Page/碘化氢.md "wikilink")，很快便在[铑催化剂存在下与](../Page/铑.md "wikilink")[一氧化碳反应生成](../Page/一氧化碳.md "wikilink")[乙酰碘](../Page/乙酰碘.md "wikilink")，然后水解生成目标产物[乙酸](../Page/乙酸.md "wikilink")。

MeI在270°C水解生成[碘化氢](../Page/碘化氢.md "wikilink")、[一氧化碳和](../Page/一氧化碳.md "wikilink")[二氧化碳](../Page/二氧化碳.md "wikilink")。

## 制备

在[甲醇与](../Page/甲醇.md "wikilink")[红磷的混合物中加入](../Page/红磷.md "wikilink")[碘时](../Page/碘.md "wikilink")，会发生[放热反应而生成碘甲烷](../Page/放热反应.md "wikilink")：\[3\]

  -
    6[CH<sub>3</sub>OH](../Page/甲醇.md "wikilink") +
    2[P](../Page/磷.md "wikilink") +
    3[I<sub>2</sub>](../Page/碘.md "wikilink") → 6CH<sub>3</sub>I +
    [H<sub>3</sub>PO<sub>3</sub>](../Page/亞磷酸.md "wikilink")

反应中生成了[三碘化磷中间体](../Page/三碘化磷.md "wikilink")，起到了与甲醇发生碘化反应的作用。

此外，碘甲烷也可由[硫酸二甲酯与](../Page/硫酸二甲酯.md "wikilink")[碘化钾在](../Page/碘化钾.md "wikilink")[碳酸钙存在下反应制得](../Page/碳酸钙.md "wikilink")：\[4\]

  -
    (CH<sub>3</sub>O)<sub>2</sub>SO<sub>2</sub> + KI →
    K<sub>2</sub>SO<sub>4</sub> + 2 CH<sub>3</sub>I

先将反应产物蒸馏，然后用[Na<sub>2</sub>S<sub>2</sub>O<sub>3</sub>](../Page/硫代硫酸钠.md "wikilink")、水和[Na<sub>2</sub>CO<sub>3</sub>溶液洗涤](../Page/碳酸钠.md "wikilink")，便可得到纯净的碘甲烷。

## 保存与纯化

和其他许多有机碘化物一样，碘甲烷试剂通常存放在棕色瓶中以防止其见光分解生成单质碘而使得试剂略带紫色。商品化的碘甲烷试剂中通常会加入铜或银丝使其稳定。也可以通过先用[硫代硫酸钠洗涤后蒸馏的方法来除去碘单质](../Page/硫代硫酸钠.md "wikilink")。

## 甲基化反应

碘甲烷是很好的[甲基化试剂](../Page/甲基化.md "wikilink")，但仍有一些缺点：

  - 它相对分子质量较大，一摩尔MeI的质量几乎等于三摩尔[CH<sub>3</sub>Cl的质量](../Page/一氯甲烷.md "wikilink")；
  - 碘化合物较氯化物和溴化物仍显得比较昂贵，因而在工业上更加便宜的[硫酸二甲酯仍有不少应用](../Page/硫酸二甲酯.md "wikilink")，尽管它的毒性比碘甲烷要高出很多；
  - 碘离子离去基团可能会造成副反应；
  - 碘甲烷的毒性比相应的氯化合物和溴化合物要大，对生产工人很不利。

因此在选择甲基化试剂时，通常要均衡考虑价格、来源、毒性、化学选择性及反应容易程度等诸多因素。

## 用途

除了用作甲基化试剂外，碘甲烷的应用还有：

  - 曾被提议用作[杀菌剂](../Page/杀菌剂.md "wikilink")、[除草剂](../Page/除草剂.md "wikilink")、[杀虫剂或](../Page/杀虫剂.md "wikilink")[杀线虫剂及](../Page/杀线虫剂.md "wikilink")[灭火器组分](../Page/灭火器.md "wikilink")
  - 用作土壤消毒剂，作为[溴甲烷](../Page/溴甲烷.md "wikilink")（被[蒙特利尔公约禁止使用](../Page/蒙特利尔公约.md "wikilink")）的替代品
  - 由于折射率的缘故，碘甲烷在显微镜方面也有应用

## 毒性

小白鼠口服碘甲烷的[LD<sub>50</sub>为](../Page/LD50.md "wikilink")76mg/kg。在IARC、ACGIH、NTP或EPA分类中，碘甲烷属于可能致癌物质；IARC（[国际癌症研究机构](../Page/国际癌症研究机构.md "wikilink")）将其划为第三类，即“尚不清楚其对人体致癌作用”。

[肝脏中](../Page/肝脏.md "wikilink")，碘甲烷被代谢为S-甲基[谷胱甘肽](../Page/谷胱甘肽.md "wikilink")。\[5\]
吸入碘甲烷的烟雾可能造成对肺、肝、肾和中枢神经系统的损伤，可能导致恶心、眩晕、咳嗽或呕吐，长期皮肤接触会造成灼伤，吸入大量会造成[肺水肿](../Page/肺水肿.md "wikilink")。

## 参见

  - [卤代烃](../Page/卤代烃.md "wikilink")、[卤代甲烷](../Page/卤代甲烷.md "wikilink")
  - [氟甲烷](../Page/氟甲烷.md "wikilink")、[一氯甲烷](../Page/一氯甲烷.md "wikilink")、[溴甲烷](../Page/溴甲烷.md "wikilink")
  - [二碘甲烷](../Page/二碘甲烷.md "wikilink")、[碘仿](../Page/碘仿.md "wikilink")、[四碘化碳](../Page/四碘化碳.md "wikilink")
  - 甲基化试剂：[碳酸二甲酯](../Page/碳酸二甲酯.md "wikilink")、[硫酸二甲酯](../Page/硫酸二甲酯.md "wikilink")

## 参考资料

  - March, J. (1992). *Advanced Organic Chemistry*, 4th Edn., New York:
    Wiley. ISBN 0-471-60180-2.

  - Sulikowski, G. A.; Sulikowski, M. M. (1999). in Coates, R.M.;
    Denmark, S. E. (Eds.) *Handbook of Reagents for Organic Synthesis,
    Volume 1: Reagents, Auxiliaries and Catalysts for C-C Bond
    Formation* New York: Wiley, pp. 423–26.

  -
## 外部链接

  -
  - 国际癌症研究机构—[Vol. 15
    (1977)](http://www.inchem.org/documents/iarc/vol15/methyliodide.html),
    [Vol. 41
    (1986)](http://www.inchem.org/documents/iarc/vol41/methyliodide.html),
    [Vol. 71
    (1999)](http://www.inchem.org/documents/iarc/vol71/100-methiodi.html)

  - [MSDS—牛津大学](http://ptcl.chem.ox.ac.uk/MSDS/IO/iodomethane.html)

  - [三氘代碘甲烷的MSDS—牛津大学](http://ptcl.chem.ox.ac.uk/MSDS/IO/iodomethane-d3.html)

  - [碘甲烷在老鼠中的代谢](http://www.biochemj.org/bj/098/0038/bj0980038_browse.htm)

  - [碘甲烷的NMR谱](https://web.archive.org/web/20070210181503/http://www.muhlenberg.edu/depts/chemistry/chem201woh/1Hiodomethane.htm)



[Category:甲基化试剂](../Category/甲基化试剂.md "wikilink")
[Category:碘代烃](../Category/碘代烃.md "wikilink")
[Category:IARC第3类致癌物质](../Category/IARC第3类致癌物质.md "wikilink")

1.

2.

3.

4.
5.