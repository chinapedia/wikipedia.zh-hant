**桓彝**（），[字](../Page/表字.md "wikilink")**茂倫**，[譙國](../Page/譙國.md "wikilink")[龍亢人](../Page/龍亢.md "wikilink")。[東漢](../Page/東漢.md "wikilink")[五更](../Page/五更.md "wikilink")[桓榮的九世孫](../Page/桓榮.md "wikilink")，可能是[曹魏](../Page/曹魏.md "wikilink")[大司農](../Page/大司農.md "wikilink")[桓範的後代](../Page/桓範.md "wikilink")。父[桓顥](../Page/桓顥.md "wikilink")，官至郎中。桓彝在東晉曾任散騎常侍、宣城內史等職，在蘇峻之亂中拒降戰死。兒子[桓溫為東晉時著名的權臣](../Page/桓溫.md "wikilink")，孫兒[桓玄更曾篡奪皇位](../Page/桓玄.md "wikilink")，幾乎覆滅東晉。

## 生平

桓彝年少就喪父，家境貧寒，但安貧樂道。性格曠達開朗，年紀輕輕已獲得很高名聲，並有鑒人之才\[1\]。年輕時就与[庾亮深交](../Page/庾亮.md "wikilink")、[周顗所器重](../Page/周顗.md "wikilink")。[晉惠帝時為州主簿](../Page/晉惠帝.md "wikilink")，拜騎都尉。後隨時任安東將軍的[晉元帝南渡](../Page/晉元帝.md "wikilink")，任[逡遒](../Page/逡遒.md "wikilink")[縣令](../Page/縣令.md "wikilink")。不久任[丞相中兵](../Page/丞相.md "wikilink")[屬](../Page/屬.md "wikilink")，累迁[中书郎](../Page/中书郎.md "wikilink")、[尚书吏部郎](../Page/尚书吏部郎.md "wikilink")，有名於朝廷。

[王敦之亂發生後](../Page/王敦之亂.md "wikilink")，王敦擊敗朝廷軍隊並執掌朝政，疑忌一眾有名望的士族，桓彝因而稱病辭職。及至[太寧二年](../Page/太寧_\(晉\).md "wikilink")（324年），晉明帝快將討伐王敦時，桓彝獲任命為[散騎常侍](../Page/散騎常侍.md "wikilink")，參與機密謀略。王敦之亂平定後，獲封**萬寧男**。及後就出任[宣城内史](../Page/宣城.md "wikilink")，任內亦有德政，受當地百姓愛戴。

[咸和二年](../Page/咸和.md "wikilink")（327年），[蘇峻聯合](../Page/蘇峻.md "wikilink")[祖約以討](../Page/祖約.md "wikilink")[庾亮為名起兵叛亂](../Page/庾亮.md "wikilink")，桓彝知道後，徵集義兵打算助朝廷討伐蘇峻。但長史[裨惠以宣城郡的兵眾少而戰鬥力弱](../Page/裨惠.md "wikilink")，而且居於附近山野的外族常作侵擾，建議桓彝不要現在就舉兵支援朝廷，待日後時機適當時才舉兵。但桓彝厲聲拒絕。桓彝及後便派將軍[朱綽到](../Page/朱綽.md "wikilink")[蕪湖討伐蘇峻一支偏軍](../Page/蕪湖.md "wikilink")，並成功擊破。桓彝不久就領兵到[石硊](../Page/石硊.md "wikilink")，此時朝廷所派守[慈湖的](../Page/慈湖.md "wikilink")[司馬流被蘇峻軍將領](../Page/司馬流.md "wikilink")[韓晃領兵擊敗](../Page/韓晃.md "wikilink")，敵軍因而長驅直進，並於蕪湖擊敗桓彝。桓彝見宣城郡沒有可作堅守的城，於是退守[廣德](../Page/廣德.md "wikilink")。

次年，蘇峻領兵攻破[建康](../Page/建康.md "wikilink")，專掌朝政。桓彝知道後慷慨流涕，並進屯[涇縣](../Page/涇縣.md "wikilink")。當時裨惠以附近州郡大部份都投降蘇峻為由，勸桓彝假意投降，免被攻打。但桓彝拒絕與蘇峻通和，後更遣將軍[俞縱守](../Page/俞縱.md "wikilink")[蘭石](../Page/蘭石.md "wikilink")。蘇峻及後派韓晃進攻俞縱，俞縱力戰而死。韓晃及後進擊桓彝，桓彝唯有據守城池，但顯得勢孤力弱。圍困的蘇峻軍曾向桓彝勸降，而桓彝部將亦勸他假意投降，後再另策圖謀，但桓彝都堅拒投降，最終於當年六月被韓晃攻破城池，桓彝被俘殺，時年五十三。當時桓彝諸子都因逃命而不能治喪，桓彝的喪事就由一位叫[紀世和的宣城人處理](../Page/紀世和.md "wikilink")。

咸和四年（329年），[蘇峻之亂平定](../Page/蘇峻之亂.md "wikilink")，朝廷向桓彝追贈[廷尉](../Page/廷尉.md "wikilink")，諡**簡**。後於[咸安年間改贈](../Page/咸安.md "wikilink")[太常](../Page/太常.md "wikilink")。

## 子孫

### 子

  - [桓溫](../Page/桓溫.md "wikilink")，東晉權臣，曾三度北伐和平滅[成漢](../Page/成漢.md "wikilink")，官至[大司馬](../Page/大司馬.md "wikilink")。
  - [桓雲](../Page/桓雲.md "wikilink")，襲爵，官至[江州](../Page/江州.md "wikilink")[刺史](../Page/刺史.md "wikilink")。
  - [桓豁](../Page/桓豁.md "wikilink")，東晉將領，官至征西大將軍。
  - [桓祕](../Page/桓祕.md "wikilink")，官至輔國將軍、宣城內史。桓溫臨死時與桓溫子[桓熙和](../Page/桓熙.md "wikilink")[桓濟合謀殺死將接掌桓溫兵眾的](../Page/桓濟.md "wikilink")[桓沖](../Page/桓沖.md "wikilink")。事敗後被朝廷棄用。
  - [桓沖](../Page/桓沖.md "wikilink")，桓彝幼子，有武幹，受桓溫器重。官至車騎將軍。

### 孫

  - [桓熙](../Page/桓熙.md "wikilink")，桓溫長子，初被立為世子，但因才能不高，桓溫將兵眾都交給桓沖。桓溫臨死時曾與桓濟和桓祕謀殺桓沖，失敗後被徙至[長沙](../Page/長沙.md "wikilink")。
  - [桓濟](../Page/桓濟.md "wikilink")，與桓熙謀殺桓沖失敗，被徙至長沙。
  - [桓歆](../Page/桓歆.md "wikilink")，封臨賀公。
  - [桓禕](../Page/桓禕.md "wikilink")，愚笨，五榖不分。
  - [桓偉](../Page/桓偉.md "wikilink")，東晉將領，官至安西將軍、領南蠻校尉、荊州刺史、使持節督[荊](../Page/荊州.md "wikilink")[益](../Page/益州.md "wikilink")[寧](../Page/寧州.md "wikilink")[秦](../Page/秦州.md "wikilink")[梁五州諸軍事](../Page/梁州.md "wikilink")。
  - [桓玄](../Page/桓玄.md "wikilink")，桓溫幼子，襲桓溫爵。東晉官至相國，後篡位建立桓楚。最終被[劉裕所主持的討伐軍擊敗並被殺](../Page/劉裕.md "wikilink")。
  - [桓序](../Page/桓序.md "wikilink")，桓雲子，宣城內史。
  - [桓石虔](../Page/桓石虔.md "wikilink")，桓豁子，東晉將領。
  - [桓石秀](../Page/桓石秀.md "wikilink")，桓豁子，博覽群書，喜好[老莊](../Page/老莊.md "wikilink")，不貪榮譽爵位，亦擅長騎射。
  - [桓石民](../Page/桓石民.md "wikilink")，桓豁子，東晉將領。曾派兵擊殺[苻丕](../Page/苻丕.md "wikilink")。
  - [桓石生](../Page/桓石生.md "wikilink")，桓豁子，東晉官員。
  - [桓石綏](../Page/桓石綏.md "wikilink")，桓豁子，東晉官員，後投桓玄。桓玄敗死後被殺。
  - [桓石康](../Page/桓石康.md "wikilink")，桓豁子，東晉將領。
  - [桓蔚](../Page/桓蔚.md "wikilink")，桓祕子，散騎常侍、游擊將軍。
  - [桓嗣](../Page/桓嗣.md "wikilink")，桓沖子，官至江州刺史。
  - [桓謙](../Page/桓謙.md "wikilink")，桓沖子，受桓玄倚重。桓玄敗死後投奔[姚興](../Page/姚興.md "wikilink")，後被殺。
  - [桓脩](../Page/桓脩.md "wikilink")，桓沖子，東晉將領，官至右將軍。桓玄篡位後任撫軍大將軍。後被劉裕主持的討伐軍殺害。
  - [桓崇](../Page/桓崇.md "wikilink")，桓沖子。
  - [桓弘](../Page/桓弘.md "wikilink")，桓沖子。
  - [桓羨](../Page/桓羨.md "wikilink")，桓沖子。
  - [桓怡](../Page/桓怡.md "wikilink")，桓沖子。

## 資料來源

<div class="references-small">

<references />

</div>

## 參考書目

  - 《[晉書](../Page/晉書.md "wikilink")·[桓彝傳](../Page/s:晉書/卷074.md "wikilink")》
  - [田余慶](../Page/田余慶.md "wikilink")：《東晉門閥政治》

[H](../Category/晉朝人.md "wikilink") [H](../Category/晉朝軍事人物.md "wikilink")
[Y](../Category/桓姓.md "wikilink")
[Category:譙國桓氏](../Category/譙國桓氏.md "wikilink")

1.  《[艺文类聚](../Page/艺文类聚.md "wikilink")》卷六引《晋中兴书》：“（桓彝）年在弱冠，便有知人之鉴。”而《晉書》亦有他結識並在日後向庾亮舉薦徐寧的事跡，而徐寧最終都身居顯位。