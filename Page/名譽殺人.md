**名譽殺人**（），也称“荣誉谋杀”，是指[女性被一个或以上](../Page/女性.md "wikilink")[家族](../Page/家族.md "wikilink")、[部族或](../Page/部族.md "wikilink")[社群男性成员以維护家族](../Page/社群.md "wikilink")[名声](../Page/名譽.md "wikilink")、清理[门户等理由杀害](../Page/门户.md "wikilink")，这类事件往往发生在[封建制度时代或民俗文化较为保守落后的地区](../Page/封建制度.md "wikilink")。也有少数男性被家族内女性以名誉之名杀害的情况。[聯合國人口基金會估计](../Page/聯合國人口基金會.md "wikilink")，每年在世界各地发生的名譽殺人事件可能高达5,000件。\[1\]\[2\]

「名譽殺人」的凶手主要是跟受害者有较近[血缘关系的男性家庭成员](../Page/血缘.md "wikilink")。受害者几乎都是女性，被杀害的原因主要是“失[贞](../Page/贞操.md "wikilink")”和“不检点”，常见的情况有被[强奸](../Page/强奸.md "wikilink")，被怀疑[通奸](../Page/通奸.md "wikilink")，打扮时髦举止轻浮，拒绝[被指定的婚姻](../Page/包办婚姻.md "wikilink")，想要[离婚等](../Page/离婚.md "wikilink")。此外，也有一些更极端的情况。

封建时代的[華人社會也有荣誉谋杀的行為](../Page/華人.md "wikilink")，稱為祭[家法](../Page/家法.md "wikilink")、[洗門風](../Page/洗門風.md "wikilink")。[梁家麟在](../Page/梁家麟.md "wikilink")《剪影中國人》一書中，就記述了在封建社會下，一個家庭為得到[貞節牌坊](../Page/貞節牌坊.md "wikilink")，光大門楣，逼女兒[殉夫的殘忍故事](../Page/人祭.md "wikilink")。在古代时期的意大利也有类似的行为。

現今的名譽殺人則多分布在保守的部落地区，在少数[伊斯兰国家也存在荣誉谋杀的案例](../Page/伊斯兰国家.md "wikilink")。但值得一提的是，这属于[前伊斯兰时期一些地方的陋习](../Page/前伊斯兰时期.md "wikilink")，并非伊斯兰教所倡导的。此外，很多伊斯兰学者对存在的荣誉谋杀进行过谴责\[3\]；另外，还有[印度](../Page/印度.md "wikilink")、[巴基斯坦](../Page/巴基斯坦.md "wikilink")、[阿富汗](../Page/阿富汗.md "wikilink")，二十世紀初[美國南部等保守地区](../Page/美國南部.md "wikilink")，[意大利](../Page/意大利.md "wikilink")[黑手党亦有名譽殺人的传统](../Page/黑手党.md "wikilink")\[4\]。由於人口外移到[西方社會](../Page/西方社會.md "wikilink")，也把這種習慣帶到當地，引起了很大的爭議。在部分落後地區，甚至有經過當地部族會議所允許的名譽殺人，兇嫌及共犯會被部族成員包庇而造成調查上的困難。不過目前現今絕大多數的國家都將名譽殺人視做[犯罪](../Page/犯罪.md "wikilink")。

## 定义

[人权观察组织对](../Page/人权观察.md "wikilink")“名誉谋杀”的定义如下：名誉谋杀是男性家庭成员认为家庭女性成员犯下的错误，给男性家庭带来了耻辱，而通过复仇行为，消除这种耻辱，这种谋杀通常导致死亡。引发名誉谋杀有各种原因：拒绝包办婚姻、性侵犯的受害者、受到丈夫虐待、丧夫甚至（据称）犯有通奸罪。仅仅是认为一个女人表现的方式“羞辱”她的家人就足以引发夺取她的生命。

男人也可以为了家族成员荣誉，谋杀他们认为与其家族女性有不恰当的关系的人。广义的“荣誉杀害”一词适用于杀戮的男性和女性。

## 参考文献

### 引用

### 来源

  - 书籍

<!-- end list -->

  -
## 外部链接

  - （[联合国开发计划署报告](../Page/联合国开发计划署.md "wikilink")）

  - [英国警方展开'名誉'犯罪调查方案](http://english.aljazeera.net/news/europe/2008/10/2008102292619942890.html)（[半岛电视台新闻](../Page/半岛电视台.md "wikilink")）

  - [一名妇女勇敢揭发"名誉杀害"](http://www.pbs.org/wnet/wideangle/uncategorized/murder-in-the-name-of-honor-documents/5302/)（PBS
    WIDE ANGLE）

## 参见

  - [矯正性強姦](../Page/姦改.md "wikilink")
  - [赐死](../Page/赐死.md "wikilink")

[Category:殺害親屬](../Category/殺害親屬.md "wikilink")
[Category:对女性的暴力行为](../Category/对女性的暴力行为.md "wikilink")
[Category:伊斯蘭人權議題](../Category/伊斯蘭人權議題.md "wikilink")

1.
2.
3.  [Honor Killing ‘Un-Islamic’: Canadian
    Fatwa](http://www.onislam.net/english/news/americas/455669-honor-killing-un-islamic-canadian-fatwa.html)
4.  <http://www.dw-world.de/dw/article/0,2144,2758535,00.html>