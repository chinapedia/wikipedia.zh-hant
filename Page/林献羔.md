**林献羔**牧師（），英文名**Rev.Samuel
Lamb**，[中国著名](../Page/中国.md "wikilink")[基督教](../Page/基督教.md "wikilink")[家庭教会领袖](../Page/家庭教会.md "wikilink")。

## 名字由來

小時候，他父親喜歡按照國家使用的[普通話來翻譯](../Page/普通話.md "wikilink")，將“林”譯成‘Lin’，但林獻羔因自己名字有個“羔”字，就把林字譯成廣東音「Lam」，再稍為一變，成為一只如假包換的羔羊「lamb」，真是“獻給羔羊基督的羊”了。英文名字是Samuel
Lamb（林撒母耳）。

## 生平

林献羔牧師祖籍[广东省](../Page/广东省.md "wikilink")[台山县](../Page/台山县.md "wikilink")，1924年10月4日出生于[澳门](../Page/澳门.md "wikilink")，他的父亲林保罗牧師当时担任澳门白马巷[浸信会的](../Page/浸信会.md "wikilink")[牧师](../Page/牧师.md "wikilink")。在他未出生时，父亲为他起名献羔，意为献给羔羊[基督](../Page/基督.md "wikilink")\[1\]。1936年在[香港](../Page/香港.md "wikilink")[长洲受浸](../Page/長洲_\(香港\).md "wikilink")。1942年进入[梧州建道圣经学院](../Page/梧州.md "wikilink")，1945年起在[广州传道](../Page/广州.md "wikilink")（位于[大北堂](../Page/大北堂.md "wikilink")），1950年开始在广州市中心[大马站](../Page/大马站.md "wikilink")35号的家中开始聚会，称为“大马站福音会堂”。

因為他不願意加入[三自愛國教會](../Page/三自愛國教會.md "wikilink")，認為其政治宣傳式的教導偏離了純正的福音，結果在1955年9月14日，在[肃反运动中和](../Page/肃反运动.md "wikilink")[王国显](../Page/王国显.md "wikilink")、[张耀生作为](../Page/张耀生.md "wikilink")“大马站反革命集团”头目被捕\[2\]，1957年获释。1958年第二次被捕，被判20年徒刑。

1978年，林牧師出狱。1979年，林献羔牧師恢复了[大马站](../Page/大马站.md "wikilink")35号的聚会。曾有多位美國領事到訪,之後人数逐渐增加到数千人。2000年大马站拓宽马路时迁至德政北路雅荷塘（北）荣桂里15号。他從1979年開始編寫第一本《靈音小叢》，已陸續發表了超過一百冊，第一頁都標有「沒有版權」。之后人数逐渐增加到数千人，影响颇大。

林献羔牧师于2013年8月3日下午去世。原订于2013年8月17日（周六）14:30，在广州[银河公墓银河园白云厅举行的遗体告别仪式](../Page/银河公墓.md "wikilink").
因埸地及安全理由现改为2013年8月16日（周五）14:30，在广州银河公墓银河园白云厅举行遗体告别仪式。

## 参考文献

## 外部链接

  - [林献羔见证及相关参考人物和文集](http://www.freeforum101.com/ilovegod/viewtopic.php?t=937&mforum=ilovegod)
  - [《靈音小叢》](https://web.archive.org/web/20080229073224/http://andrewor.info/Lamb%20Book.htm)
    ， 林獻羔著
  - [林獻羔講道集](https://web.archive.org/web/20100509164428/http://andrewor.info/Lamb%20Preaching%20Notes/Masterlist.htm)

{{-}}

[L](../Category/中华人民共和国基督教新教人物.md "wikilink")
[L](../Category/中華民國大陸時期基督教新教人物.md "wikilink")
[L](../Category/中华人民共和国政治犯.md "wikilink")
[Category:台山人](../Category/台山人.md "wikilink")
[X献羔](../Category/林姓.md "wikilink")
[Category:广州历史人物](../Category/广州历史人物.md "wikilink")

1.  自始至终，耶和华都帮助我——广州市大马站家庭教会林献羔弟兄的见证
2.  廣州《南方日报1955年9月15日》“广州的基督教内破获了一个以林献羔、王國顯、張耀生為首的反革命集團”