[Marx_Reichlich_001.jpg](https://zh.wikipedia.org/wiki/File:Marx_Reichlich_001.jpg "fig:Marx_Reichlich_001.jpg")，1506年由[Marx
Reichlich所绘](../Page/Marx_Reichlich.md "wikilink")（在[慕尼黑的美术馆中展览](../Page/慕尼黑.md "wikilink")）\]\]
[AMH-6977-KB_Virasundara_is_put_to_death.jpg](https://zh.wikipedia.org/wiki/File:AMH-6977-KB_Virasundara_is_put_to_death.jpg "fig:AMH-6977-KB_Virasundara_is_put_to_death.jpg")，约1672年）\]\]

**石刑**，是一种钝击致死的[死刑执行方式](../Page/死刑.md "wikilink")，根据情况、地区的不同，采用大小相差迥异的[石头](../Page/石头.md "wikilink")。

在《[希伯來聖經](../Page/希伯來聖經.md "wikilink")》，對於律法記載死刑罪行都按例以石刑處理。根據《[米德拉什](../Page/米德拉什.md "wikilink")》，Shimon
ben Gamliel指出，“**宗教法庭於七十年內判死多於一人，就是暴政。**”
《[密西拿](../Page/密西拿.md "wikilink")》规定有以下行为者应被处以石刑.

而在《[新約聖經](../Page/新約聖經.md "wikilink")》，[耶穌曾向捉拿](../Page/耶穌.md "wikilink")[淫婦的](../Page/淫婦.md "wikilink")[法利賽人說](../Page/法利賽人.md "wikilink")：「**你們中間誰是沒有罪的，誰就可以先拿石頭打她。**」（）可見當時已有以石刑作處死犯人的刑罰，並為公眾接受。

[蒙古貴族則會將石裝進布袋](../Page/蒙古.md "wikilink")，然後以石刑處死 。

石刑在[国际社会普遍被視為過於殘酷](../Page/国际社会.md "wikilink")，故通常會使用一些較人道的[死刑执行方式](../Page/死刑.md "wikilink")。

在某些[伊斯兰国家的](../Page/伊斯兰国家.md "wikilink")[沙里亞法規下](../Page/沙里亞法規.md "wikilink")，石刑仍然存在。這些國家包括[也門](../Page/也門.md "wikilink")、[毛里塔尼亞](../Page/毛里塔尼亞.md "wikilink")、[阿富汗](../Page/阿富汗.md "wikilink")、[索馬里](../Page/索馬里.md "wikilink")、[文莱](../Page/汶莱.md "wikilink")、[伊朗](../Page/伊朗.md "wikilink")、[蘇丹](../Page/苏丹共和国.md "wikilink")、[阿拉伯聯合酋長國](../Page/阿拉伯聯合酋長國.md "wikilink")、[沙特阿拉伯和](../Page/沙特阿拉伯死刑制度.md "wikilink")[奈及利亞](../Page/奈及利亞.md "wikilink")。一個已婚的男人或已婚女子與人通奸，有四名證人的證詞，法官如斷定其通奸就可判刑。男性在腰以下的地方都要埋入沙中，女性的則較深，是胸以下的地方。其後人們就向受刑者反覆投石。石頭約拳頭大，一塊不足致死，受刑者最後死於嚴重的[腦損傷及顱內出血](../Page/頭部外傷.md "wikilink")。

在部分伊斯兰国家，[性少数群体会因为他们自身的性别认同或性取向而遭到石刑](../Page/性少數.md "wikilink")，比如[沙特阿拉伯](../Page/沙烏地阿拉伯LGBT權益.md "wikilink")[和文莱](../Page/汶萊LGBT權益.md "wikilink")。

## 出現過石刑畫面的電影

  - 2007年－《[追風箏的孩子](../Page/追風箏的孩子.md "wikilink")》 （The Kite
    Runner）：劇情後半段主角重返[塔利班控制下的故鄉](../Page/塔利班.md "wikilink")，目睹一名被控[通姦的阿富汗婦女在體育場遭石刑處決](../Page/通姦.md "wikilink")。
  - 2018年—《[-{zh-cn:12勇士; zh-tw:12猛漢;
    zh-hk:12壯士}-](../Page/12猛漢.md "wikilink")》（12 Strong）:
    在空降至阿富汗前一名特種部隊隊員透過筆電觀看一名因未婚懷孕被處以石刑的女孩錄像。

## 参考文献

## 参见

  - [伊斯兰教法](../Page/伊斯兰教法.md "wikilink")

{{@Bible|约翰福音|8|07}}

[he:ארבע מיתות בית דין](../Page/he:ארבע_מיתות_בית_דין.md "wikilink")

[钝](../Category/處決方法.md "wikilink")
[Category:伊斯蘭教與死刑](../Category/伊斯蘭教與死刑.md "wikilink")