**漸伸線**（involute）(或稱**漸開線**(evolvent))和**漸屈線**（evolute）是曲線的[微分幾何上互為表裡的概念](../Page/微分幾何.md "wikilink")。若曲線A是曲線B的漸伸線，曲線B是曲線A的漸屈線。

在曲線上選一定點*S*。有一動點*P*由*S*出發沿曲線移動，選在*P*的切線上的*Q*，使得曲線長*SP* 和直線段長*PQ*
相同。漸伸線就是Q的軌跡。

若曲線B有[參數方程](../Page/參數方程.md "wikilink")\(r:\mathbb R\to\mathbb R^n\)，其中\(|r^\prime(s)|=1\)，曲線A的方程為\(t\mapsto r(t)-tr^\prime(t)\)。

曲線的漸屈線是該曲線每點的[曲率中心的集](../Page/曲率.md "wikilink")。

若該曲線有參數方程\(r:\mathbb R\to\mathbb R^n\)（\(|r^\prime(s)|=1\)），則其漸屈線為

  -
    \(s \to r(s)+{r''(s)\over|r''(s)|^2}\)。

每條曲線可有無窮多條漸伸線，但只有一條漸屈線。

| 漸屈線                                                                 | 漸伸線                              |
| ------------------------------------------------------------------- | -------------------------------- |
| [懸鏈線](../Page/懸鏈線.md "wikilink")                                    | [曳物線](../Page/曳物線.md "wikilink") |
| [圓內螺線](../Page/圓內螺線.md "wikilink")／[外擺線](../Page/外擺線.md "wikilink") | 相似的圓內螺線／外擺線                      |
| [擺線](../Page/擺線.md "wikilink")                                      | 相同的擺線                            |
| [半立方拋物線](../Page/半立方拋物線.md "wikilink")                              | [拋物線](../Page/拋物線.md "wikilink") |

## 參數化曲線

漸開線方程曲線的參數化定義的函數**( x(t) , y(t) )** 是:

\(X[x,y]=x-\frac{x'\int_a^t \sqrt { x'^2 + y'^2 }\, dt}{\sqrt { x'^2 + y'^2 }}\)

\(Y[x,y]=y-\frac{y'\int_a^t \sqrt { x'^2 + y'^2 }\, dt}{\sqrt { x'^2 + y'^2 }}\)

## 範例

|                                                                                              |                                                                                                                                                |                                                                                                                                 |
| -------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------- |
| [Involut_cir.jpg](https://zh.wikipedia.org/wiki/File:Involut_cir.jpg "fig:Involut_cir.jpg") | [Animated_involute_of_circle.gif](https://zh.wikipedia.org/wiki/File:Animated_involute_of_circle.gif "fig:Animated_involute_of_circle.gif") | [Involute.gif](https://zh.wikipedia.org/wiki/File:Involute.gif "fig:Involute.gif")的漸開線是一個 [曳物線](../Page/曳物線.md "wikilink")。\]\] |

### 圓的漸伸線

圓的漸伸線會形成一個類似[阿基米德螺線的圖形](../Page/阿基米德螺線.md "wikilink").

  - 在[笛卡兒坐標系中](../Page/笛卡兒坐標系.md "wikilink")，一個圓的漸開線的參數方程可以寫成：

\[\, x = a \left( \cos\ t + t\sin\ t \right)\]

\[\, y = a \left( \sin\ t - t\cos\ t \right)\]

其中\(\, a\)是圓的半徑，\(\, t\)為參數

  - 在 [極坐標系中](../Page/極坐標系.md "wikilink")， \(\, r,\theta\)
    一個圓的漸開線的參數方程可以寫成：

\[\, r=a\sec\alpha\]

\[\, \theta = \tan\alpha - \alpha\]

其中 \(\, a\) 是圓的半徑 \(\, \alpha\)為參數

通常，一個圓的漸開線常被寫成寫成：

\[\, r = a \sqrt{1+t^2}\]

\[\, \theta = \arctan \frac{\cos t + t \sin t}{\sin t - t \cos t}\].

[歐拉建議使用圓的漸開線作為](../Page/歐拉.md "wikilink")[齒輪的形狀](../Page/齒輪.md "wikilink"),
這個設計普遍存在於目前使用，稱為[漸開線齒輪](../Page/漸開線齒輪.md "wikilink")。

### 懸鏈線的漸開線

一個[懸鏈線的漸開線](../Page/懸鏈線.md "wikilink")
會通過此懸鏈線的[頂點](../Page/頂點.md "wikilink")
，形成[曳物線](../Page/曳物線.md "wikilink")。
在[笛卡兒坐標系中](../Page/笛卡兒坐標系.md "wikilink")，一個懸鏈線的漸開線的參數方程可以寫成：

\(x=t-\mathrm{tanh}(t)\,\)
\(y=\mathrm{sech}(t)\,\)
其中*t* 是參數，而[sech是雙曲正割函數](../Page/sech.md "wikilink")(1/cosh(x))

***衍生***

用\(r(s)=(\sinh^{-1}(s),\cosh(\sinh^{-1}(s)))\,\)

我們得到 \(r^\prime(s)=(1,s)/\sqrt{1+s^2}\,\)

且\(r(t)-tr^\prime(t)=(\sinh^{-1}(t)-t/\sqrt{1+t^2},1/\sqrt{1+t^2})\).

替代成 \(t=\sqrt{1-y^2}/y\)

可得到 \(({\rm sech}^{-1}(y)-\sqrt{1-y^2},y)\).

### 擺線的漸開線

*一個* [擺線的漸開線是另一個與它](../Page/擺線.md "wikilink")
[全等的擺線](../Page/全等.md "wikilink")
在[笛卡兒坐標系中](../Page/笛卡兒坐標系.md "wikilink")，一個擺線的漸開線的參數方程可以寫成：

\[x=r(t-\sin(t))\,\]

\[y=r(1-\cos(t))\,\]

其中 *t* 是角度， *r* 是 [半徑](../Page/半徑.md "wikilink")

## 另外參見

  - [漸屈線](../Page/漸屈線.md "wikilink")
  - [渦旋壓縮機](../Page/渦旋壓縮機.md "wikilink")
  - [漸開線齒輪](../Page/漸開線齒輪.md "wikilink")

## 外部連結

  - Xah: Special Plane Curves:
    [Involute](http://xahlee.org/SpecialPlaneCurves_dir/Involute_dir/involute.html),
    [Evolute](http://xahlee.org/SpecialPlaneCurves_dir/Evolute_dir/evolute.html)
  - [Mathworld](http://mathworld.wolfram.com/Involute.html)

[Category:曲線](../Category/曲線.md "wikilink")