**李天岩**（），生於[中华民国](../Page/中华民国.md "wikilink")[福建省的美籍](../Page/福建省.md "wikilink")[數學家](../Page/數學家.md "wikilink")，[湖南人](../Page/湖南.md "wikilink"),
[密西根州立大學杰出數學教授](../Page/密西根州立大學.md "wikilink")。

## 生平

1968年成為[國立清華大學數學系第一屆畢業生](../Page/國立清華大學.md "wikilink")。1974年在[馬里蘭大學取得博士學位](../Page/馬里蘭大學.md "wikilink")，指導教授為James
A. Yorke。

他和[James
Yorke合寫的論文](../Page/:en:James_A._Yorke.md "wikilink")《週期三則混沌》（Period
Three Implies
Chaos），是[混沌](../Page/混沌理論.md "wikilink")[動力系統的重要論文](../Page/動力系統.md "wikilink")。這個研究結果是[沙可夫斯基定理](../Page/沙可夫斯基定理.md "wikilink")（[Sharkovskii's
theorem](../Page/:en:Sharkovskii's_theorem.md "wikilink")）的特殊情況。

他对乌伦（Stanislaw Ulam）
猜想的证明是[動力系統不变测度计算研究之奠基性工作](../Page/動力系統.md "wikilink")。

他又和R.B. Kellogg、Yorke發明了一個找[布勞威爾](../Page/鲁伊兹·布劳威尔.md "wikilink")（L. E.
J.
Brouwer）[不動點的思想和数值方法](../Page/不動點.md "wikilink")，开辟了现代同伦延拓算法研究的新天地；他和他的合作者们以及学生们关于代数特征值问题以及一般多变量多项式系统同伦方法之广泛、深入研究，为他赢得此领域世界领袖人物之一之称号。

## 获奖和荣誉

  - [Guggenheim
    Fellowship](../Page/:en:Guggenheim_Fellowship.md "wikilink") (1995)
  - Distinguished Faculty Award, College of Natural Science, Michigan
    State University, 1996
  - Distinguished Faculty Award, Michigan State University, 1996
  - J.S.Frame Teaching Award, 1996
  - University Distinguished Professor, Michigan State University, 1998
  - Distinguished Alumni, College of Sciences, Tsing Hua University,
    Taiwan, 2002
  - Outstanding Academic Advisor Award, College of Natural Science,
    Michigan State University, 2006

## 外部連結

  - [網站](http://www.mth.msu.edu/~li/)
  - [Episte
    Math內李天岩的文章](http://episte.math.ntu.edu.tw/cgi/mathfield.pl?aut=%A7%F5%A4%D1%A9%A5)
  - [李天岩](http://orca.st.usm.edu/~jding/articles/ProfTYLibio.html)，丁玖，中国数学家传
  - [在逆境中拚搏的理學院傑出校友- -
    李天岩學長](http://alumni.ad.nthu.edu.tw/asp/publication/docs/142/%E7%9B%B8%E6%80%9D%E6%83%85%E8%A9%B1%5C%E6%9D%8E%E5%A4%A9%E5%B2%A9%E5%AD%B8%E9%95%B7.pdf)，林文伟，
    清華大學校友通訊，2002
  - [回首來時路，李天岩](https://web.archive.org/web/20090410160307/http://www.math.sinica.edu.tw/math_media/d314/31404.pdf)（在2005年底及2006年底兩次於清華大學講演）

[Category:台湾数学家](../Category/台湾数学家.md "wikilink")
[Category:20世纪数学家](../Category/20世纪数学家.md "wikilink")
[Category:21世纪数学家](../Category/21世纪数学家.md "wikilink")
[Category:国立清华大学校友](../Category/国立清华大学校友.md "wikilink")
[Category:马里兰大学校友](../Category/马里兰大学校友.md "wikilink")
[Category:密歇根州立大学教师](../Category/密歇根州立大学教师.md "wikilink")
[Category:沙县人](../Category/沙县人.md "wikilink")
[T](../Category/李姓.md "wikilink")
[Category:古根海姆学者](../Category/古根海姆学者.md "wikilink")