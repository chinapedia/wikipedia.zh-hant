[HK_Sheung_Shui_龍琛路_Lung_Sum_Avenue_view_Shek_Wu_Hui_building_facades_Jan_2017_Lnv2_04.jpg](https://zh.wikipedia.org/wiki/File:HK_Sheung_Shui_龍琛路_Lung_Sum_Avenue_view_Shek_Wu_Hui_building_facades_Jan_2017_Lnv2_04.jpg "fig:HK_Sheung_Shui_龍琛路_Lung_Sum_Avenue_view_Shek_Wu_Hui_building_facades_Jan_2017_Lnv2_04.jpg")\]\]
[San_Hong_Street_2016.jpg](https://zh.wikipedia.org/wiki/File:San_Hong_Street_2016.jpg "fig:San_Hong_Street_2016.jpg")行人專用區為水貨客集中地，以藥房、連鎖波鞋店為主\]\]
[Sheung_Shui_1991.jpg](https://zh.wikipedia.org/wiki/File:Sheung_Shui_1991.jpg "fig:Sheung_Shui_1991.jpg")
[ShekWuHui_MarketPlace.jpg](https://zh.wikipedia.org/wiki/File:ShekWuHui_MarketPlace.jpg "fig:ShekWuHui_MarketPlace.jpg")
[Map_of_The_Convention_for_the_Extension_of_Hong_Kong_Territory_in_1898_-_2.jpg](https://zh.wikipedia.org/wiki/File:Map_of_The_Convention_for_the_Extension_of_Hong_Kong_Territory_in_1898_-_2.jpg "fig:Map_of_The_Convention_for_the_Extension_of_Hong_Kong_Territory_in_1898_-_2.jpg")》附圖中的石湖墟（Shek-u-hü）\]\]

**石湖墟**（）是[香港](../Page/香港.md "wikilink")[北區](../Page/北區_\(香港\).md "wikilink")[上水的一個舊](../Page/上水.md "wikilink")[墟市](../Page/墟市.md "wikilink")，位於現時[東鐵綫](../Page/東鐵綫.md "wikilink")[上水站附近](../Page/上水站_\(香港\).md "wikilink")。[粉嶺／上水新市鎮就是從石湖墟和](../Page/粉嶺／上水新市鎮.md "wikilink")[粉嶺](../Page/粉嶺.md "wikilink")[聯和墟發展起來的](../Page/聯和墟.md "wikilink")。石湖墟的街道多以「新」字作開頭，並取健康、成功等等之意：新豐路、新健街、新康街、新財街、新發街、新成路、新功街、新樂街、新祥街、新勤街。

如今，石湖墟仍是上水一個商業活動十分繁忙的地方，故墟內一些街道現已由[運輸署劃為](../Page/運輸署.md "wikilink")「[行人專用區](../Page/行人專用區.md "wikilink")」。

## 歷史

由於墟內附郵政局位置有一小河，設有眝水玻頭，玻下有石形成一湖，故名「石湖」\[1\]。

石湖墟建立年份已不可考，但早在清朝[嘉慶版](../Page/嘉慶.md "wikilink")《[新安縣志](../Page/新安縣志.md "wikilink")》中的《墟市》列表中已收錄「石湖墟」，並附有註腳「舊誌天崗，今移石湖」\[2\]，表明石湖墟的前身是一個名為「天崗墟」的墟市，並曾記錄在[康熙版](../Page/康熙.md "wikilink")《[新安縣志](../Page/新安縣志.md "wikilink")》之中\[3\]。

1898年，租借[新界及](../Page/新界.md "wikilink")[新九龍的條約](../Page/新九龍.md "wikilink")《展拓香港界址專條》亦有收錄「石湖墟」，並在條約附圖中顯示位置在[粉嶺圍以北](../Page/粉嶺圍.md "wikilink")。

石湖墟建立後，成為當時區內重要的商業中心，早年由[上水鄉事委員會管理石湖墟的農業買賣活動](../Page/鄉事委員會.md "wikilink")。但1955年2月21日\[4\]及1956年12月23日\[5\]發生了兩場大火，使石湖墟淪為頹垣敗瓦，1957年開始重建計劃，直至1964年才重建完成，現有墟內建築物多是當時才建立的。於1993年政府把位於新成路的舊街市遷到現時的[石湖墟新街市](../Page/石湖墟市政大廈.md "wikilink")。

[清朝](../Page/清朝.md "wikilink")[康熙元年](../Page/康熙.md "wikilink")（1662年），清廷為了嚴防沿海居民對[台灣](../Page/台灣.md "wikilink")[鄭成功進行接濟](../Page/鄭成功.md "wikilink")，實行[遷界](../Page/遷界.md "wikilink")，沿海五省居民向內地遷界五十里，令沿海居民流離失所。康熙四年（1665年）兩廣總督[周有德及廣東巡撫](../Page/周有德.md "wikilink")[王來任上奏請求復界](../Page/王來任.md "wikilink")，至康熙8年（1669年）清廷批准復界，居民才可回鄉重建家園。居民在兩人逝世後，集資在石湖墟興建「報德祠」，以紀念兩人的恩惠。1955年大火時焚燬「報德祠」，石湖墟現時袛剩下「[巡撫街](../Page/巡撫街.md "wikilink")」紀念兩人。\[6\]

2014年，[民建聯獲](../Page/民建聯.md "wikilink")[香港建造商會研究基金撥款](../Page/香港建造商會.md "wikilink")，研究如何活化[聯和墟](../Page/聯和墟.md "wikilink")、[大埔墟和石湖墟](../Page/大埔墟.md "wikilink")。民建聯認為為了配合[新界東北發展計劃](../Page/新界東北發展計劃.md "wikilink")，原墟市亦各有文化特色，既需要保育，亦需要注入新的元素。在[香港大學建築學院的協助下](../Page/香港大學.md "wikilink")，研究團隊舉辦了多場地區工作坊，諮詢居民、商戶及學生等意見。最終，建議石湖墟[符興街列為永久](../Page/符興街.md "wikilink")[行人專用區](../Page/行人專用區.md "wikilink")，與[新康街連接](../Page/新康街.md "wikilink")，令到區內商業活動更加聚集，而位於符興街的食物安全中心辦事處亦為特色建築，可以作為社區用途，在旁擺設露天茶座，有關建議獲得有關部門接納\[7\]，不過由於石湖墟的[水貨活動自](../Page/水貨.md "wikilink")2010年後越趨熾熱，符興街一帶的商鋪多已變成爲水貨客提供貨源的藥房和化妝品店，每天有大量水貨客聚集，故該計劃後來不了了之。

## 公共交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFFFF; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{東鐵綫色彩}}">█</font>[東鐵綫](../Page/東鐵綫.md "wikilink")：[上水站](../Page/上水站_\(香港\).md "wikilink")

<!-- end list -->

  - [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - [專線小巴](../Page/香港小巴.md "wikilink")

</div>

</div>

## 圖片集

<File:ShekWuMarket> SanFungRoad.jpg|新豐路
[File:ShekWuMarket2.jpg|新祥街](File:ShekWuMarket2.jpg%7C新祥街)
<File:Fu> Hing Street (3).jpg|符興街
[File:ShekWuHuiPostOffice.jpg|石湖墟郵政局](File:ShekWuHuiPostOffice.jpg%7C石湖墟郵政局)
<File:Hang> Lok Square 2016.jpg|行樂坊

## 參見

  - [北區](../Page/北區_\(香港\).md "wikilink")
  - [上水](../Page/上水.md "wikilink")
  - [粉嶺／上水新市鎮](../Page/粉嶺／上水新市鎮.md "wikilink")
  - [聯和墟](../Page/聯和墟.md "wikilink")

## 參考

  - [運輸署 -
    上水石湖墟](https://web.archive.org/web/20070104103609/http://www.td.gov.hk/transport_in_hong_kong/pedestrianisation/pedestrianisation/shek_wu_hui_sheung_shu_/index_tc.htm)

[Category:上水](../Category/上水.md "wikilink")
[Category:香港墟市](../Category/香港墟市.md "wikilink")
[Category:香港礦業](../Category/香港礦業.md "wikilink")

1.  香港工商日報, 1964-03-16 第4頁
2.
3.
4.  [香港工商日報](../Page/香港工商日報.md "wikilink"), 1955-02-22 第7頁
5.  工商晚報, 1956-12-23 第2頁
6.
7.  [活化三區墟市
    打造特色景點](http://orientaldaily.on.cc/cnt/news/20140407/00176_022.html)
    《東方日報》 2014年4月7日