**和平基金會**是一個獨立於[華盛頓特區的非盈利研究和教育組織](../Page/華盛頓特區.md "wikilink")。1957年由[美國投資銀行家](../Page/美國.md "wikilink")[倫道夫・坎頓](../Page/倫道夫・坎頓.md "wikilink")(Randolph
Compton)創立的。基金會致力防止[戰爭和導致戰爭](../Page/戰爭.md "wikilink")。最近，研究在資金為和平主要地集中於辨認和減少衝突源於微弱或未通過的狀態。

## 外部連結

  - [和平基金會](http://www.fundforpeace.org/)

[Category:和平主義](../Category/和平主義.md "wikilink")
[Category:美國基金會](../Category/美國基金會.md "wikilink")
[Category:華盛頓哥倫比亞特區組織](../Category/華盛頓哥倫比亞特區組織.md "wikilink")
[Category:美国智库](../Category/美国智库.md "wikilink")
[Category:1957年建立的组织](../Category/1957年建立的组织.md "wikilink")