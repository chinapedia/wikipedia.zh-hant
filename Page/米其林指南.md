[Guide_michelin_1929_couverture-edit.png](https://zh.wikipedia.org/wiki/File:Guide_michelin_1929_couverture-edit.png "fig:Guide_michelin_1929_couverture-edit.png")
[Guide_michelin_1929_nancy-edit.png](https://zh.wikipedia.org/wiki/File:Guide_michelin_1929_nancy-edit.png "fig:Guide_michelin_1929_nancy-edit.png")
《**米芝蓮指南**》（）是[法国知名](../Page/法国.md "wikilink")[輪胎製造商](../Page/輪胎.md "wikilink")[米芝蓮公司所出版的美食及旅遊指南書籍的總稱](../Page/米芝蓮.md "wikilink")，其中以評鑑餐廳及旅館、封面為[红色的](../Page/红色.md "wikilink")「」（）最具代表性，所以有時《米芝蓮指南》一詞特指「紅色指南」。除了紅皮的食宿指南之外，還有[綠色封面的](../Page/綠色.md "wikilink")「」（），內容為旅遊的行程規劃、景點推薦、道路導引等等。\[1\]

近年《米芝蓮指南》在[欧洲的影響力有下降的趨勢](../Page/欧洲.md "wikilink")，因此開始開拓海外新市場，例如[美國與](../Page/美國.md "wikilink")[日本等東亞和北美的地點](../Page/日本.md "wikilink")。2007年3月14日，米其林在[东京宣布將於同年](../Page/东京.md "wikilink")11月推出[日文與](../Page/日语.md "wikilink")[英文版的](../Page/英語.md "wikilink")《米芝蓮指南－東京篇》，日本成為世界第22個、[亞洲第一個納入](../Page/亞洲.md "wikilink")《米芝蓮指南》評選的國家。2008年底《米芝蓮指南－[香港](../Page/香港.md "wikilink")、[澳門篇](../Page/澳門.md "wikilink")》推出，為繼東京之後納入評選的亞洲城市(之後2009年[京都與](../Page/京都.md "wikilink")[大阪](../Page/大阪.md "wikilink"))，並為《米芝蓮指南》前進[中國大陸市場的敲門磚](../Page/中國大陸.md "wikilink")。2016年9月21日，米其林正式发布首版2017年[上海米其林指南](../Page/上海.md "wikilink")。7月21日，2016
年版[新加坡](../Page/新加坡.md "wikilink")、11月7日，[首爾](../Page/首爾.md "wikilink")2017年版也於同年開辦首屆。2017年12月6日，[曼谷](../Page/曼谷.md "wikilink")2018年版。[台北則於](../Page/台北.md "wikilink")2018年3月14日推出[2018年版](../Page/台北米其林指南#.md "wikilink")。\[2\]

因現代化社會越非發注重餐廳飲食上的享受，米其林仍是餐廳產業的指標評鑑。在零售通路產品領域則是以近年知名的比利時Monde Selection,
荷蘭A.A. Taste Awards以及英國 Great Taste等四大美食評比結果為主要消費參考。

[Dishes_made_by_Michelin_star_restaurants.jpg](https://zh.wikipedia.org/wiki/File:Dishes_made_by_Michelin_star_restaurants.jpg "fig:Dishes_made_by_Michelin_star_restaurants.jpg")
《米其林指南》誕生於1900年的[巴黎萬國博覽會期間](../Page/1900年世界博览会.md "wikilink")，當時[米其林公司的創辦人米其林兄弟看好](../Page/米其林.md "wikilink")[汽车旅行有發展的遠景](../Page/汽车.md "wikilink")，如果汽車旅行越興盛，他們的[輪胎也能賣得越好](../Page/輪胎.md "wikilink")，因此將[地图](../Page/地图.md "wikilink")、[加油站](../Page/加油站.md "wikilink")、[旅館](../Page/酒店.md "wikilink")、汽車維修廠等等有助於汽車旅行的資訊集結起來，出版了隨身手冊大小的《米其林指南》一書，並且免費提供給客戶索取。免費提供一直持續到了1920年，米其林兄弟偶然間注意到他們費心製作的《米其林指南》被維修廠員工當作工作台的桌腳補墊來用，因而意識到免費提供的書籍反而會被人視為沒有價值的東西，所以決定從當年開始取消免費提供，改為販售。

1926年《米其林指南》開始將評價優良的旅館特別以[星號標示](../Page/星號.md "wikilink")，1931年開始啟用3個[星級的評等系統](../Page/星級.md "wikilink")。米其林公司為了維護評鑑的中立與公正，所派出的評鑑員都是喬裝成普通顧客四處暗訪，藉此觀察店家最真實的一面，《米其林指南》評鑑的權威性由此建立。\[3\]

## 評鑑

米其林依照評鑑標準，給予一至三顆星星評價，評鑑標準包括食材品質、廚師對味道以及烹調技巧的駕馭能力、料理中展現的個性、是否物有所值及餐飲水準一致性。評鑑以城市為單位。

獲得一星的餐廳表示在同類別裡出眾；二星代表廚藝高明、值得繞道前往；三星為最高星級，代表餐廳供應的料理出類拔萃、值得專程造訪。\[4\]

## 爭議

### 程序不透明

米其林評鑑主要遭人詬病的是反應太慢、標準不夠嚴謹、程序不夠透明及帶有法式偏見。有評審就曾說，所有獲獎餐廳需每18個月再次造訪評鑑，但除非遭到投訴，事實上的複查間隔是3年半，不夠嚴謹。\[5\]

### 平民美食「死亡之吻」

死亡之吻是指，店主擔憂在業主得知商家獲得米其林美食推薦後，會大幅提升房租，逼遷或結業。自2012年起，香港版米其林食評加入平民化美食推薦後，連同「佳佳甜品」，先後導致數家平民食肆遭業主加租逼遷或結業\[6\]\[7\]，分別為：

  - 2012年6月，位於西洋菜街南之「好旺角麵家」總店，被業主加租五成結業，只餘下同區兩家分店。\[8\]
  - 2012年12月，「添好運點心專門店」因加租，被迫由街舖遷入商場。\[9\]
  - 2014年8月，「阿鴻小吃」，原本評級一星，獲評級後遭業主加租六成結業。\[10\]
  - 2015年12月，於早一個月前獲米芝蓮推介的23間香港街頭小食，榜上有名的荃灣川龍街祥興記上海生煎包，貼出「再見」告示，指因業主加租，被逼搬遷，再一次應驗「死亡之吻」。\[11\]

### 地方認知缺失

首版[广州米其林指南計劃](../Page/广州市.md "wikilink")2018年6月尾面世，廣州市旅遊局連同米其林公司共同開展米其林指南發佈的預熱和宣傳工作，出版廣州旅遊定製版的廣州米其林指南，製作宣傳文案
\[12\]
，開初文案以“**米到羊城**”為特寫口號（<small>[粵語中含咪的前置句式均為否定句](../Page/粤语.md "wikilink")，此口號地方理解即是**不要來羊城**</small>），發佈後惹來不少質疑，英文名則使用普通話拼音而不是傳統的**Canton**，認為米其林推廣未有充分了解地方歷史文化，要求重新製作文案。

## 資料來源

## 参见

  - [米其林餐廳列表](../Page/米其林餐廳列表.md "wikilink")
  - [台北米其林指南](../Page/台北米其林指南.md "wikilink")

## 外部連結

  -
  - [Michelin Guide restaurants in the UK and abroad -
    ViaMichelin](http://www.viamichelin.co.uk/web/Restaurants)

  -
  -
  -
  -
  -
  -
  -
[米其林指南](../Category/米其林指南.md "wikilink")
[Category:旅游指南](../Category/旅游指南.md "wikilink")
[Category:1900年書籍](../Category/1900年書籍.md "wikilink")
[Category:各主題書籍](../Category/各主題書籍.md "wikilink")
[Category:餐館](../Category/餐館.md "wikilink")

1.

2.

3.

4.  [米其林台北指南 20家星級餐廳完整名單](http://www.cna.com.tw/news/firstnews/201803145006-1.aspx),中央社，2018年3月14日

5.  [美食聖經米其林指南
    光環背後也有爭議](http://www.cna.com.tw/news/firstnews/201803140352-1.aspx),中央社，2018年3月14日

6.

7.

8.

9.
10.

11.

12.