[AMD_K6-II_Processor_Logo.png](https://zh.wikipedia.org/wiki/File:AMD_K6-II_Processor_Logo.png "fig:AMD_K6-II_Processor_Logo.png")
**K6-2**是一個由AMD製造的[x86處理器](../Page/x86.md "wikilink")，也是AMD整个奔腾时期最成功的一个处理器序列。K6-2可以在200至550
MHz的時鐘速度運行。它有64Kb一級緩存（32KB指令集和32KB資料），在2.2[伏特的電壓下運行](../Page/伏特.md "wikilink")。使用[0.25微米製程](../Page/0.25微米製程.md "wikilink")，擁有930萬個[電晶體](../Page/電晶體.md "wikilink")，以及只能使用於處理器插座是[Socket
7或](../Page/Socket_7.md "wikilink")[Super Socket
7的主機版](../Page/Super_Socket_7.md "wikilink")。

## K6-2的歷史

[Amdk62_arch.png](https://zh.wikipedia.org/wiki/File:Amdk62_arch.png "fig:Amdk62_arch.png")

K6-2设计目的是作为当时比较老、更重要的是比较贵的[Intel](../Page/Intel.md "wikilink") [Pentium
II的竞争对手](../Page/Pentium_II.md "wikilink")。这两种CPU的性能很相似：K6-2的前代[K6趋向于日常计算应用](../Page/AMD_K6.md "wikilink")，而Intel的在浮点任务中无疑更强大。K6-2是一款非常成功的CPU，并为[AMD提供了市场基础和财政稳定性](../Page/AMD.md "wikilink")，为日后发布[Athlon做好准备](../Page/Athlon.md "wikilink")。

K6-2是第一款引入浮点单指令多数据结构（[SIMD](../Page/SIMD.md "wikilink")）指令集——[3DNow\!的处理器](../Page/3DNow!.md "wikilink")，这套指令集大大推动了执行3维立体处理能力的进步。3D
Now\!成功打败了Intel在数月后推出的、相似但更复杂的[SSE](../Page/SSE.md "wikilink")。

几乎所有的K6-2都是在100MHz [前端总线](../Page/前端总线.md "wikilink")（FSB）的[Super
7平台上开发的](../Page/Super_7.md "wikilink")，这套平台为CPU性能提高提供了基础。在K6-2初期，K6-2
300MHz版本是卖得最好的。这个版本的热卖，为K6-2在市场上造就了一个极好的名声，并与[Intel](../Page/Intel.md "wikilink")
[Celeron](../Page/Celeron.md "wikilink")
300A在市场上剧烈地竞争。[Celeron提供了一个小但快的缓存和卓越的浮点运算单元](../Page/Celeron.md "wikilink")；K6-2提供更快的RAM访问（得益于Super
7主板）和3D Now\!的3D绘图能力的扩充。两者不仅热卖，并且吸引的很多双方的支持者使用对方的系统。（在那时候，最快的[Pentium
II尽管比最快K](../Page/Pentium_II.md "wikilink")6-2和Celeron快一点，但价格却高出不少）

由于市场继续向前进，AMD发布了一系列更快的K6-2，而最畅销的型号从最初的300MHz，一直到了500MHz。到了450和500MHz的K6-2成为市场主流的时候，更新、更快的CPU已经占领了高端市场，但K6-2和它的竞争对手Celeron依然在各自公司的CPU种类预算中。K6-2的100MHz
FSB的主板允许K6-2还算温柔地抵挡着不断增长的CPU倍频值，并且在剩余的生命依然令人惊奇地在市场上竞争着。

## K6-2+

鲜为人知的K6-2+是整合了128KiB二级缓存并采用[0.18微米工艺的处理器](../Page/0.18微米製程.md "wikilink")（从本质上来说，这是[K6-III的简化版](../Page/AMD_K6-III.md "wikilink")）。K6-2+是明确为低功耗移动型（但它的發熱量很大）CPU，而且推出的时候正是主流台式机快速转型到诸如Athlon之类的新平台。这款CPU在其目标市场销量一般，但尽管AMD没有努力为其作宣传，K6-2+依然出现在传统台式机市场中销售。台式机版本的K6-2+在市场中完全被Athlon和K6-III遮盖着锋芒（两者都比K6-2+快多了），甚至连较慢、只便宜一点点的原版K6-2也不例外，因为K6-2更知名和更容易找到匹配的主板。K6-2+最高频率为570MHz。

## K6-2的版本

### K6-2 (Chomper, 250 nm)

[AMD-K6-2_350AMZ.jpg](https://zh.wikipedia.org/wiki/File:AMD-K6-2_350AMZ.jpg "fig:AMD-K6-2_350AMZ.jpg")

  - CPUID: Family 5, Model 8，步进 0
  - 一級緩存：32 + 32 KiB（数据 + 指令）
  - 指令集：[MMX](../Page/MMX.md "wikilink")，[3DNow\!](../Page/3DNow!.md "wikilink")
  - 插座：[Super Socket 7](../Page/Super_Socket_7.md "wikilink")
  - [前端总线](../Page/前端总线.md "wikilink")：66, 100 MHz
  - 核心電壓：2.2V
  - 首次發表：1998年5月28日
  - 时钟频率：233, 266, 300, 333 & 350 MHz

### K6-2 (Chomper Extended (CXT), 250 nm)

  - CPUID: Family 5, Model 8，步进 12
  - 一级缓存：32 + 32 KiB（数据 + 指令）
  - 指令集：[MMX](../Page/MMX.md "wikilink")，[3DNow\!](../Page/3DNow!.md "wikilink")
  - 插座：[Super Socket 7](../Page/Super_Socket_7.md "wikilink")
  - [前端总线](../Page/前端总线.md "wikilink")：66, 95, 97, 100 MHz
  - 核心電壓：2.0(mobile)/2.2/2.3/2.4V
  - 首次發表：1998年12月16日
  - 时钟频率：266 - 550 MHz

### K6-2+ (180 nm)

  - 一級緩存：32 + 32 KiB（数据 + 指令）
  - 二級緩存：128 KiB，全速
  - 指令集：[MMX](../Page/MMX.md "wikilink")，扩展[3DNow\!](../Page/3DNow!.md "wikilink")，[PowerNow\!](../Page/PowerNow!.md "wikilink")
  - 插座：[Super Socket 7](../Page/Super_Socket_7.md "wikilink")
  - [前端总线](../Page/前端总线.md "wikilink")：100 MHz
  - 核心電壓：2.0 V
  - 首次發表：2000年4月18日
  - 时钟频率：450, 475, 500, 533, 550 & 570 MHz

## 外部链接

  - [AMD: AMD-K6®-2
    Processor](http://www.amd.com/us-en/Processors/ProductInformation/0,,30_118_1260_1217,00.html)
  - [Tomshardware: Socket 7 Tuning With AMD
    K6-2+](http://www.tomshardware.com/cpu/20000713/)
  - [Anandtech: Super7 Upgrade
    Guide](http://www.anandtech.com/cpuchipsets/showdoc.aspx?i=1406)
  - [Geek.com: AMD - K6-2 (K6 3D
    MMX)](https://web.archive.org/web/20031009032008/http://www.geek.com/procspec/amd/k63dmmx.htm)
  - [Technical overview of the AMD-K6®
    series](http://www.pcguide.com/ref/cpu/fam/g6.htm)
  - [Pictures of AMD-K6®-2 chips at
    CPUShack.com](http://www.cpushack.net/gallery/index.php?folder=/chippics/AMD/K6-2/)
  - [AMD K6-2 technical
    specifications](https://web.archive.org/web/20051122153856/http://balusc.xs4all.nl/ned/har-cpu-amd-k6.php)

[Category:AMD处理器](../Category/AMD处理器.md "wikilink")
[Category:X86架構](../Category/X86架構.md "wikilink")