| abovestyle =
background:\#ccc;font-size:500%;line-height:1.3;font-weight:normal;font-family:serif;
| above =  | subheader1 =

<div style="background:#ccc;font-size:125%;font-weight:bold; <!--modified {{longitem}}:-->padding:0.1em 0.25em 0.2em;line-height:1.2em;">

{{\#if:}}} |}}} |}}

</div>

| subheader2 = {{\#if: |

<div style="background:#ccc;padding:0 0 0.4em;line-height:1.2em;">

</div>

}} | headerstyle =
background:\#e0e0e0;padding:0.15em;padding-left:0.3em; | rowclass1 =
noprint | data1 =

`  `

| rowclass4 = noprint | data4 =

`  `

| rowclass5 = noprint | data5 =

`  `



<div class="wrap nounderlines" style="max-width:17.5em; line-height:1.1em; padding-left:1em;">

[؋](../Page/阿富汗尼.md "wikilink") ​[₳](../Page/阿根廷奧斯特拉爾.md "wikilink") ​
[฿](../Page/泰銖.md "wikilink") ​[₿](../Page/比特币.md "wikilink") ​
[₵](../Page/迦納塞地.md "wikilink") ​[¢](../Page/分_\(貨幣\).md "wikilink")
​[₡](../Page/哥斯大黎加科朗.md "wikilink") ​ ​ [$](../Page/$.md "wikilink")
​[₫](../Page/越南盾.md "wikilink") ​[₯](../Page/希腊德拉克马.md "wikilink")
​[֏](../Page/亚美尼亚德拉姆符号.md "wikilink") ​
[₠](../Page/欧洲货币单位.md "wikilink")
​[€](../Page/€.md "wikilink") ​  ​[₣](../Page/法国法郎.md "wikilink")
​ [₲](../Page/巴拉圭瓜拉尼.md "wikilink") ​  ​
[₭](../Page/寮國基普.md "wikilink") ​
[₺](../Page/土耳其里拉符号.md "wikilink")
​[₾](../Page/格鲁吉亚拉里.md "wikilink") ​
[₼](../Page/阿塞拜疆马纳特.md "wikilink")
​[ℳ](../Page/黃金馬克.md "wikilink")
​[₥](../Page/文_\(貨幣\).md "wikilink") ​
[₦](../Page/奈及利亞奈拉.md "wikilink") ​
[₧](../Page/比塞塔.md "wikilink") ​ ​[₰](../Page/芬尼.md "wikilink")
​[£](../Page/£.md "wikilink") ​ [元 圆 圓](../Page/人民币.md "wikilink")
​[﷼](../Page/伊朗里亞爾.md "wikilink") ​[៛](../Page/柬埔寨瑞爾.md "wikilink")
​ ​ [₨](../Page/盧比.md "wikilink") ​  ​
[৳](../Page/孟加拉塔卡.md "wikilink")
​[₸](../Page/哈萨克斯坦坚戈.md "wikilink")
​[₮](../Page/蒙古图格里克.md "wikilink") ​
[₩](../Page/₩.md "wikilink") ​ [¥ 円](../Page/日圓.md "wikilink")

</div>

| rowclass6 = noprint | data6 =

`  `

| rowclass7 = noprint | header7 = 相關符號 | rowclass8 = noprint | data8 =

  -
  - s

| rowclass9 = noprint | header9 = 其他[文字](../Page/文字.md "wikilink") |
rowclass10 = noprint | data10 =

| belowclass = hlist noprint | belowstyle =
background:\#e0e0e0;padding:0.25em 0.25em
0.4em;line-height:1.75em;font-weight:bold; | below =
}}<noinclude></noinclude>