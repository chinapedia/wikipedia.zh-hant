## 家庭背景

。其妻子[李彥秀是](../Page/李彥秀.md "wikilink")[中國國民黨籍現任中華民國立法委員](../Page/中國國民黨.md "wikilink")，兩人育有一女。

## 演藝時期

1983年，侯冠群自[華岡藝術學校國劇科畢業後主演中國電視公司](../Page/華岡藝術學校.md "wikilink")[八點檔連續劇](../Page/八點檔.md "wikilink")《[少年十五二十時](../Page/少年十五二十時.md "wikilink")》一炮而紅，後來於1988年轉赴大陸求學並且經商。而後在1995年重回台灣演藝圈，專職節目主持人。2000年，侯冠群主持[八大綜合台](../Page/八大綜合台.md "wikilink")[政治戲仿節目](../Page/政治戲仿節目.md "wikilink")《[主席有約](../Page/主席有約.md "wikilink")》（首播期間：2000年1月3日至2002年3月29日），因扮演「李祖惜」（諧音「李主席」）模仿時任[總統兼](../Page/中華民國總統.md "wikilink")[中國國民黨主席](../Page/中國國民黨主席.md "wikilink")[李登輝而聲名大噪](../Page/李登輝.md "wikilink")，并开啟了台灣藝人模仿[政治人物之風潮](../Page/政治人物.md "wikilink")。1999年8月26日循古禮正式拜相聲大師[吳兆南為師](../Page/吳兆南.md "wikilink")，成為了[侯寶林徒孫](../Page/侯寶林.md "wikilink")、中國相聲第八代弟子。

## 從政時期

2002年，侯冠群棄影從政，代表新黨參選[台北市議員](../Page/台北市.md "wikilink")（中正萬華選區﹞，競選口號為：「做一個堂堂正正的[中國人](../Page/中國人.md "wikilink")、當一個清清白白的好議員！」該年年底順利當選，並於2006年連任。

2008年，侯冠群與同為臺北市議員的[李彥秀結婚](../Page/李彥秀.md "wikilink")（[中國國民黨籍](../Page/中國國民黨.md "wikilink")，內湖南港選區）。隔年兩人產下一女，取名為「侯貽寶」，意為上天賜予侯家的寶貝，亦隱有台灣寶貝之意。

在[2004年中華民國總統選舉及](../Page/2004年中華民國總統選舉.md "wikilink")[百萬人民倒扁運動中發聲的搖滾樂手](../Page/百萬人民倒扁運動.md "wikilink")[吳勇鋒](../Page/吳勇鋒.md "wikilink")（[砰砰阿峰](../Page/砰砰阿峰.md "wikilink")）拜侯冠群為師，學習相聲。

2010年8月19日侯冠群收中德混血模特兒袁曉婷(女)為第二名學習相聲的弟子.

侯冠群在[第十一屆台北市議會選舉中爭取連任失利](../Page/2010年中華民國直轄市公職人員選舉.md "wikilink")。

2011年二月一日起侯冠群應台北市長郝龍斌之邀擔任[台北市文化基金會文創藝術論壇召集人](../Page/台北市文化基金會.md "wikilink")。

2012年四月十二起擔任台北市文化基金會總監([松山文創園區](../Page/松山文創園區.md "wikilink"))，並兼任台北市政府會展產業發展基金會([花博公園](../Page/花博公園.md "wikilink"))文化藝術顧問。

## 參選記錄

### 2002年臺北市第9屆議員選舉

  - 第五選區（中正、萬華區）
  - 7席

<table>
<thead>
<tr class="header">
<th><p>號次</p></th>
<th><p>黨籍</p></th>
<th><p>姓名</p></th>
<th><p>得票數</p></th>
<th><p>得票率</p></th>
<th><p>當選</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td></td>
<td><p>李振豐</p></td>
<td><p>1,212</p></td>
<td><p>0.68%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td></td>
<td><p>車正國</p></td>
<td><p>1,837</p></td>
<td><p>0.98%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td></td>
<td><p>劉耀仁</p></td>
<td><p>16,698</p></td>
<td><p>8.93%</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td></td>
<td><p>謝千惠</p></td>
<td><p>2,363</p></td>
<td><p>1.26%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td></td>
<td><p>鍾小平</p></td>
<td><p>9,698</p></td>
<td><p>5.18%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td></td>
<td><p>李仁人</p></td>
<td><p>28,104</p></td>
<td><p>15.02%</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td></td>
<td><p>陳卯樂</p></td>
<td><p>440</p></td>
<td><p>0.24%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td></td>
<td><p>顏聖冠</p></td>
<td><p>11,967</p></td>
<td><p>6.4%</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td></td>
<td><p>謝建平</p></td>
<td><p>8,223</p></td>
<td><p>4.4%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td></td>
<td><p>賴志威</p></td>
<td><p>8,299</p></td>
<td><p>4.44%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td></td>
<td><p>周威佑</p></td>
<td><p>13,671</p></td>
<td><p>7.31%</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td></td>
<td><p>曹鵬山</p></td>
<td><p>11,834</p></td>
<td><p>6.33%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td></td>
<td><p>陳惠敏</p></td>
<td><p>22,341</p></td>
<td><p>11.93%</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td></td>
<td><p>曲兆祥</p></td>
<td><p>10,700</p></td>
<td><p>5.72%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td></td>
<td><p><strong>侯冠群</strong></p></td>
<td><p>18,400</p></td>
<td><p>9.84%</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
<tr class="even">
<td><p>16</p></td>
<td></td>
<td><p>陳嘉銘</p></td>
<td><p>18,365</p></td>
<td><p>9.82%</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
<tr class="odd">
<td><p>17</p></td>
<td></td>
<td><p>陳光</p></td>
<td><p>2,896</p></td>
<td><p>1.55%</p></td>
<td></td>
</tr>
</tbody>
</table>

### 2006年臺北市第10屆議員選舉

  - 第五選區（中正、萬華區）
  - 7席

<table>
<thead>
<tr class="header">
<th><p>號次</p></th>
<th><p>黨籍</p></th>
<th><p>姓名</p></th>
<th><p>得票數</p></th>
<th><p>得票率</p></th>
<th><p>當選</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td></td>
<td><p>鍾小平</p></td>
<td><p>9,685</p></td>
<td><p>5.62%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td></td>
<td><p>蔡鴻鵬</p></td>
<td><p>2,187</p></td>
<td><p>1.27%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td></td>
<td><p>陳惠敏</p></td>
<td><p>14,147</p></td>
<td><p>8.21%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td></td>
<td><p>梁熾誠</p></td>
<td><p>121</p></td>
<td><p>0.07%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td></td>
<td><p>林御紹</p></td>
<td><p>296</p></td>
<td><p>0.17%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td></td>
<td><p>曹鵬山</p></td>
<td><p>8,038</p></td>
<td><p>4.67%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td></td>
<td><p>陳光</p></td>
<td><p>7,143</p></td>
<td><p>4.15%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td></td>
<td><p><strong>侯冠群</strong></p></td>
<td><p>16,572</p></td>
<td><p>9.62%</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td></td>
<td><p>蕭炳賢</p></td>
<td><p>197</p></td>
<td><p>0.11%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td></td>
<td><p>周威佑</p></td>
<td><p>16,118</p></td>
<td><p>9.36%</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td></td>
<td><p>陳嘉銘</p></td>
<td><p>16,097</p></td>
<td><p>9.35%</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td></td>
<td><p>李仁人</p></td>
<td><p>20,293</p></td>
<td><p>11.78%</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td></td>
<td><p>劉耀仁</p></td>
<td><p>14,169</p></td>
<td><p>8.23%</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td></td>
<td><p>詹銘洲</p></td>
<td><p>336</p></td>
<td><p>0.2%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td></td>
<td><p>顏聖冠</p></td>
<td><p>19,844</p></td>
<td><p>11.02%</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
<tr class="even">
<td><p>16</p></td>
<td></td>
<td><p>吳志剛</p></td>
<td><p>23,149</p></td>
<td><p>13.44%</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
<tr class="odd">
<td><p>17</p></td>
<td></td>
<td><p>杜淑觀</p></td>
<td><p>3,858</p></td>
<td><p>2.24%</p></td>
<td></td>
</tr>
</tbody>
</table>

### 2010年臺北市第11屆議員選舉

  - 第五選區（中正、萬華區）
  - 8席（婦女保障名額2席）

<table>
<thead>
<tr class="header">
<th><p>號次</p></th>
<th><p>黨籍</p></th>
<th><p>姓名</p></th>
<th><p>得票數</p></th>
<th><p>得票率</p></th>
<th><p>當選</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td></td>
<td><p>郭昭巖</p></td>
<td><p>19,674</p></td>
<td><p>10.37%</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td></td>
<td><p>王碧華</p></td>
<td><p>2,078</p></td>
<td><p>1.1%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td></td>
<td><p>應曉薇</p></td>
<td><p>18,764</p></td>
<td><p>9.89%</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td></td>
<td><p>宋佳倫</p></td>
<td><p>2,215</p></td>
<td><p>1.17%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td></td>
<td><p>陳嘉銘</p></td>
<td><p>10,852</p></td>
<td><p>5.72%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td></td>
<td><p>童仲彥</p></td>
<td><p>15,832</p></td>
<td><p>8.35%</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td></td>
<td><p>陳光</p></td>
<td><p>5,384</p></td>
<td><p>2.84%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td></td>
<td><p>吳志剛</p></td>
<td><p>27,003</p></td>
<td><p>14.23%</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td></td>
<td><p><strong>侯冠群</strong></p></td>
<td><p>9,835</p></td>
<td><p>5.18%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td></td>
<td><p>顏聖冠</p></td>
<td><p>16,843</p></td>
<td><p>8.88%</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td></td>
<td><p>鍾小平</p></td>
<td><p>21,160</p></td>
<td><p>11.15%</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td></td>
<td><p>李振光</p></td>
<td><p>3,017</p></td>
<td><p>1.59%</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td></td>
<td><p>鍾君竺</p></td>
<td><p>443</p></td>
<td><p>0.23%</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td></td>
<td><p>劉耀仁</p></td>
<td><p>21,843</p></td>
<td><p>11.51%</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td></td>
<td><p>周威佑</p></td>
<td><p>14,762</p></td>
<td><p>7.78%</p></td>
<td><p><a href="https://zh.wikipedia.org/wiki/File:Vote1.svg" title="fig:Vote1.svg">Vote1.svg</a></p></td>
</tr>
</tbody>
</table>

## 表演作品

2011年起擔任每年六月於福建省廈門市舉行的海峽論壇開幕式主持人。

### 電視劇

|        |        |                                                  |             |
| ------ | ------ | ------------------------------------------------ | ----------- |
| **年份** | **頻道** | **劇名**                                           | **角色**      |
| 1983年  | 中視     | 《少年十五二十時》                                        | 謝木森         |
| 1984年  | 中視     | 《書劍千秋》                                           | 明武宗         |
| 1985年  | 中視     | 《[一代女皇](../Page/一代女皇.md "wikilink")》             | 李賢          |
| 1986年  | 中視     | 《[一代公主](../Page/一代公主.md "wikilink")》             | 李隆基         |
| 1986年  | 中視     | 《家和萬事興》                                          | 林永華         |
| 1987年  | 中視     | 《珍珠傳奇》                                           | 李适          |
| 1987年  | 中視     | 《含羞草》                                            | 林嘉義         |
| 1993年  | 華視     | 《[劉伯溫傳奇](../Page/劉伯溫傳奇.md "wikilink")－桑榆恨》       | 鄭金昇         |
| 1996年  | 中視     | 《紅塵》                                             | 鍾阿強         |
| 1996年  | 中視     | 《天師鍾馗－紅梅閣》                                       | 廖瑩中         |
| 1996年  | 華視     | 《[台灣靈異事件](../Page/台灣靈異事件.md "wikilink")－天堂路》     | 小白          |
| 1997年  | 華視     | 《[施公奇案](../Page/施公奇案_\(台灣\).md "wikilink")－父子情深》 | 西門英         |
| 1997年  | 華視     | 《施公奇案－再世鴛鴦》                                      | 田盛          |
| 1997年  | 華視     | 《施公奇案－考場怪談》                                      | 石玉崑         |
| 1997年  | 華視     | 《施公奇案－新官上任》                                      | 曾本潭         |
| 2013年  | 中國大陸   | 《微時代之戀》（楊冪工作室製作）                                 | 客串上海酒店大亨吳宇霆 |

### 電影

|        |         |        |
| ------ | ------- | ------ |
| **年份** | **片名**  | **角色** |
| 1983年  | 《竹劍少年》  | 麥傑倫    |
| 1984年  | 《排行榜》   | 鐵頭     |
| 1986年  | 《老師有問題》 | 國文老師   |

### 舞台劇、相聲

  - 1987年轉赴香港無線電視台擔任配音員。
  - 1988年\~1993年前往大陸投靠親戚，承包台灣影視在大陸拍攝所需各項業務及學習各地方言、戲劇、凡六年，足跡遍及大陸各地。
  - 1994年\~1995年返台參加屏風表演班舞台劇的演出，磨鍊演技：《西出陽關》、《太平天國》、《半里長城》。
  - 1999年7月26日于台北市君悅飯店循古禮拜投[吳兆南門下學習相聲](../Page/吳兆南.md "wikilink")。
  - 1999年9月25日於台北市社教館發表了其個人第一次相聲演出，演出傳統與自創的《鈐鐺譜》、《金陵塔》、《方言雜說》等三個段子。
  - 2000年5月於台灣全省展開師徒相聲聯演《佛曰不可說、
    夫子曰大聲說！》8月26日，循古禮拜投[吳兆南門下學習相聲](../Page/吳兆南.md "wikilink")。
  - 2001年春節與中國京劇院青年劇團於台北合作演出現代京劇《紅燈記》。
  - 2002年1月於台北新舞台《2002年吳兆南師徒大聯演》演出傳統相聲《討白朗》。5月發表自創現代相聲《中華民國在台灣》。

#### 相聲演出作品列表

  - 2000年 《佛曰：不可說！夫子曰：大聲說！》
  - 2002年 《吳中聲有》
  - 2002年 《中華民國在台灣》
  - 2003年 《吳室聲蜚》
  - 2004年 《吳党所踪》
  - 2005年 《吳雞之談》
  - 2006年 《天下吳男事》
  - 2006年
    《[中華民國在台灣第二集](../Page/中華民國在台灣第二集.md "wikilink")－[台灣好](../Page/台灣好.md "wikilink")》
  - 2008年 《吳間道》
  - 2010年
    《[中華民國在台灣第三集](../Page/中華民國在台灣第三集.md "wikilink")－[寶島一甲子](../Page/寶島一甲子.md "wikilink")》洛杉磯
  - 2010年 《吳聊》
  - 2013年 《相聲，愛你一生》
  - 2014年11月於台北市中山堂演出由北京人民藝術劇院導演李六乙所創作，內容為毛澤東、蔣介石於另一個時空相遇，探討生死哲學的舞臺劇《再見》，由侯冠群飾演毛澤東，樊光耀飾演蔣介石。

### 著作

| 年份      | 書名     | 出版社   | ISBN               |
| ------- | ------ | ----- | ------------------ |
| 2002年出版 | 《假面人生》 | 聯經出版社 | ISBN 9789570824315 |
| 2010年出版 | 《家國大祭》 | 武漢出版社 | ISBN 9787543049833 |

### 電視節目主持

| 年份                                                    | 頻道                 | 節目            |
| ----------------------------------------------------- | ------------------ | ------------- |
| 1996年起                                                | 華衛電視台              | 《世紀末情愛神話》     |
| |[GTV28](../Page/GTV28.md "wikilink")                 | 《獨漏腥聞》             |               |
| |[三立都會台](../Page/三立都會台.md "wikilink")                 | 《社會探險隊》            |               |
| |民視                                                   | 《台灣愛說笑》            |               |
| |八大                                                   | 《SRE娛樂調查報告》        |               |
| |民視                                                   | 《喜劇有限公司》           |               |
| |華視                                                   | 《歡喜住台灣》            |               |
| |八大                                                   | 《神出鬼没》             |               |
| |TVBS                                                 | 《武林大會》             |               |
| |華視                                                   | 《當我們同在一起》          |               |
| |八大綜合台                                                | 《獨漏60》             |               |
| |[傳訊電視大地頻道](../Page/傳訊電視.md "wikilink")               | 《台灣傳奇》             |               |
| |八大綜合台                                                | 《主席有約》             |               |
| |台視                                                   | 《電視白皮書》            |               |
| |[中視衛星](../Page/中視衛星.md "wikilink")                   | 《中華料理炒翻天》          |               |
| |中天                                                   | 《新聞對對碰》            |               |
| |[MUCH TV](../Page/MUCH_TV.md "wikilink")             | 《搞笑VERY MUCH現代北京話》 |               |
| |中天                                                   | 《選舉考碴團》            |               |
| |華視                                                   | 《我愛頭家》             |               |
| 2001年                                                 |                    | 《台灣電影金馬獎星光大道》 |
| |[中國中央電視台中文國際頻道](../Page/中國中央電視台中文國際頻道.md "wikilink") | 《天涯共此時》            |               |

### 廣播節目主持

| 年份                                    | 頻道      | 節目 |
| ------------------------------------- | ------- | -- |
| |[中廣流行網](../Page/中廣流行網.md "wikilink") | 《霹靂十八點》 |    |
| |洛杉磯中文廣播電台                            | 《八點有約》  |    |

### 廣告

  - 可口可樂
  - 百事可樂
  - 花仙子芳香劑
  - 花仙子保鮮膜
  - ATALI電玩
  - 千百力口服液
  - 捷安特自行車
  - 黑旋風殺蟲劑

## 參考資料

## 外部連結

  -
  -
|- |colspan="3" style="text-align:center;"|**臺北市議會** |-    |-

[Category:臺灣電視男演員](../Category/臺灣電視男演員.md "wikilink")
[Category:臺灣電影男演員](../Category/臺灣電影男演員.md "wikilink")
[8](../Category/台灣相聲演員.md "wikilink")
[Category:新黨黨員](../Category/新黨黨員.md "wikilink")
[Category:第9屆臺北市議員](../Category/第9屆臺北市議員.md "wikilink")
[Category:第10屆臺北市議員](../Category/第10屆臺北市議員.md "wikilink")
[Category:大安區人](../Category/大安區人.md "wikilink")
[Category:藝人出身的政治人物](../Category/藝人出身的政治人物.md "wikilink")
[Category:東北裔台灣人](../Category/東北裔台灣人.md "wikilink")
[Category:臺北市私立華岡藝術學校校友](../Category/臺北市私立華岡藝術學校校友.md "wikilink")
[Category:臺北市立仁愛國民中學校友](../Category/臺北市立仁愛國民中學校友.md "wikilink")
[Category:臺北市大安區仁愛國民小學校友](../Category/臺北市大安區仁愛國民小學校友.md "wikilink")
[Guan冠](../Category/侯姓.md "wikilink")