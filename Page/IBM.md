**国际商业机器股份有限公司**（，[首字母縮略字](../Page/首字母縮略字.md "wikilink")：IBM，曾译**万国商用机器公司**）是美國一家跨國科技公司及諮詢公司，總部位於[紐約州](../Page/紐約州.md "wikilink")[阿蒙克市](../Page/阿蒙克市.md "wikilink")。IBM主要客户是政府和企业。IBM生产并销售计算机硬件及软件，并且为系统架构和网络托管提供咨询服务。截止2013年，IBM已在全球拥有12个研究实验室和大量的软件开发基地。IBM雖然是一家商業公司，但在[材料](../Page/材料.md "wikilink")、[化学](../Page/化学.md "wikilink")、[物理等科学领域卻也有很高的成就](../Page/物理.md "wikilink")，利用這些學術研究為基礎，发明很多产品。比较有名的IBM发明的产品包括[硬盘](../Page/硬盘.md "wikilink")、[自動櫃員機](../Page/自動櫃員機.md "wikilink")、[通用产品代码](../Page/通用产品代码.md "wikilink")、[SQL](../Page/SQL.md "wikilink")、[关系数据库管理系统](../Page/关系数据库管理系统.md "wikilink")、[DRAM及](../Page/DRAM.md "wikilink")[沃森](../Page/沃森_\(人工智能程序\).md "wikilink")。

## 历史

### 1880年至1929年

IBM實际创始人[托馬斯·沃森的先人十九世紀中期因](../Page/托馬斯·J·沃森.md "wikilink")[愛爾蘭大饑荒移民美國](../Page/爱尔兰大饥荒.md "wikilink")，托馬斯·沃森年輕時先後從事過賣[鋼琴](../Page/鋼琴.md "wikilink")、豬肉與證券的工作，原本計劃要存錢開肉店，但卻遭到證券營業員所欺騙而損失所有金錢。托馬斯·沃森改去NCR（National
cash
register，國民收銀機公司，另譯安訊公司）當業務員，成績優秀，但因銷售手法觸犯[反托拉斯法](../Page/反托拉斯法.md "wikilink")，被判刑一年，後來案件因某專業因素被撤銷。NCR老闆後來與沃森不和，並要求沃森離開NCR。

托馬斯·沃森改加入CTR（Computing Tabulating
Recording，計算列表紀錄公司，1911年創立），這家公司銷售的機器可以分配資料卡並列印統計表，主要客戶是[美國人口普查局](../Page/美國人口普查局.md "wikilink")。沃森借了許多錢發展公司，採用將機器出租的新銷售手法，沃森自己也借了很多錢買公司的[股票](../Page/股票.md "wikilink")。1924年沃森將CTR改名為IBM。\[1\]

### 1930年至1979年

[IBM_Electronic_Data_Processing_Machine_-_GPN-2000-001881.jpg](https://zh.wikipedia.org/wiki/File:IBM_Electronic_Data_Processing_Machine_-_GPN-2000-001881.jpg "fig:IBM_Electronic_Data_Processing_Machine_-_GPN-2000-001881.jpg")的研究员在使用IBM
704型电子数据处理机\]\]
1940年代末期沃森長子建議公司將機械式計算機改為美國[陸軍使用的](../Page/陸軍.md "wikilink")[真空管與電子式計算機](../Page/真空管.md "wikilink")。1956年沃森將公司交棒給自己的長子小托馬斯·沃森，而IBM亦在1957年進軍大中華市場，並成立香港分公司。1960年代初期小沃森用公司年營收三倍的巨資，花費五十億美元開發360系列大型電腦，採用最新的[積體電路技術](../Page/積體電路.md "wikilink")，奠定IBM在大型電腦稱霸的定位。小托馬斯·沃森曾對一位讓公司損失一千萬美元的員工說，他不會開除這員工因為他是幫員工付學費。

1970年小托马斯·沃森將公司董事長職位交棒給，Learson兩年後交棒給，Cary設立密集的員工訓練並以許多考核關卡謹慎挑選經理人。1980年Cary交棒給John
Opel，Opel與[比爾·蓋茲的母親皆為美國聯合勸募組織](../Page/比爾·蓋茲.md "wikilink")（United
Way）的成員，當IBM經理擔憂將個人電腦[作業系統交給](../Page/作業系統.md "wikilink")[比爾·蓋茲的小公司承包是否妥當時](../Page/比爾·蓋茲.md "wikilink")，Opel表示他認識蓋茲的母親，他支持比爾·蓋茲的產品。1986年Opel交棒給艾克斯，艾克斯任內IBM面臨營運危機。1993年艾克斯交棒給Louis
V. Gerstner。除了1993年挖角來擔任董事長兼執行長的Louis V.
Gerstner，IBM歷任董事長皆從[業務員做起](../Page/業務員.md "wikilink")。IBM在1980年代的十年裏使用了一千億美元的研發經費，開發出許多電腦技術。\[2\]

### 1980年至今

[IBM_Blue_Gene_P_supercomputer.jpg](https://zh.wikipedia.org/wiki/File:IBM_Blue_Gene_P_supercomputer.jpg "fig:IBM_Blue_Gene_P_supercomputer.jpg")（Blue
Gene）超级计算机获得了由[美国总统](../Page/美国总统.md "wikilink")[奥巴马于](../Page/奥巴马.md "wikilink")[2009年9月18日颁发的](../Page/2009年9月18日.md "wikilink")“国家技术与创新奖章”\]\]
IBM在90年代初一度面臨[個人電腦與](../Page/個人電腦.md "wikilink")[工作站功能增強](../Page/工作站.md "wikilink")，[大型计算机](../Page/大型计算机.md "wikilink")（System/360、z系列）銷售減少，陷入虧損困境，1993年放棄終身雇用制開始大裁員。但1993年從食品公司挖角郭士納（Louis
V. Gerstner,
Jr.）擔任董事長兼[執行長後](../Page/執行長.md "wikilink")，組織與企業經營方向進行了巨大改革，以提供客戶全套軟硬體設計全套解決方案為主要銷售策略，重新振興IBM，讓IBM營收獲利皆創新高。

1994年8月16日，首次研發出世界第一台全觸控螢幕的行動電話，比2007年蘋果公司發表的第一支iPhone和NOKIA於1999年發表的全彩觸控螢幕行動電話還要早，堪稱智慧型手機中最早的始祖，但後來仍未引起全面性流行。

2003年，[彭明盛](../Page/彭明盛.md "wikilink")（Samuel
Palmisano，又译为“潘盛诺”，1973年加入IBM擔任業務員，2003年1月擔任董事長，[約翰霍普金斯大學](../Page/約翰霍普金斯大學.md "wikilink")[歷史學士](../Page/歷史.md "wikilink")）成为IBM董事長兼[首席执行官兼總裁](../Page/首席执行官.md "wikilink")，在他2008年任內IBM年度營收首度突破一千億[美元](../Page/美元.md "wikilink")。

IBM为[计算机产业长期的领导者](../Page/计算机.md "wikilink")，在大型／小型机和便携机（[ThinkPad](../Page/ThinkPad.md "wikilink")）方面的成就最为瞩目。其创立的[个人计算机标准](../Page/个人计算机.md "wikilink")，至今被不断地沿用和发展。2002年12月以20.5亿美元的价格将台式机硬盘业务出售给[日立](../Page/日立.md "wikilink")。2004年12月8日其PC部门出售给联想公司，金额17.5亿美元并持有联想公司股份。收購後，联想在五年内可以使用IBM的品牌。[ThinkPad和](../Page/ThinkPad.md "wikilink")[ThinkCentre品牌归](../Page/ThinkCentre.md "wikilink")[联想集团所有](../Page/联想集团.md "wikilink")，IBM只會對server級的電腦進行維護。借由联想收购PC部门的契机，IBM开始向管理服务公司转型。

2007年11月20日，IBM 公司宣布以50億美元現金收購[Cognos](../Page/Cognos.md "wikilink")，成為
IBM 創建以來金額最高的收購交易。IBM 於2008年第一季度完成收購。

2011年10月26日，IBM宣布任命[弗吉尼亚·罗曼提](../Page/吉妮·罗梅蒂.md "wikilink")（Virginia
Rometty）为总裁兼CEO，这是IBM历史上首位女性总裁兼CEO。\[3\]

2011年11月14日，[巴菲特斥資](../Page/巴菲特.md "wikilink")107億美元。入股IBM6400萬股，收購成本約每股167.19美元，占其流通股股本的5.4%成為最大股東。

2013年6月4日，IBM宣布收购美国云计算公司SoftLayer Technologies，以强化公司在云计算市场的地位。

2013年9月11日，IBM以5.05億美元（約39.3億港元）出售環球客戶服務業務外判部門予Synnex
Corp，IBM將收現金加對方股份。同年9月17日，IBM宣布投资10亿美元支持客户使用[Linux操作系统](../Page/Linux.md "wikilink")。\[4\]

2014年1月，IBM同意将x86服务器业务以23亿美元的价格出售给联想。

2014年8月11日，IBM公司宣布，已经收购云安全服务提供商Lighthouse（Lighthouse Security Group,
LLC）。IBM未公布收购细节。\[5\]

2015年8月6日，IBM宣布将以10亿美元收购医学成像及临床系统供应商Merge Healthcare，并将其与旗下“沃森健康”(Watson
Health)部门合并，并整合来自Merge Healthcare医疗成像管理平台的数据和图像与旗下沃森计算平台的图像分析业务。

2015年10月28日，IBM宣布收购了The Weather Company的B2B数据业务，这笔交易将巩固其在物联网方面的布局。

2016年1月21日，IBM宣布收购了网络视频直播服务商[Ustream](../Page/Ustream.md "wikilink")，将组建“云视频服务业务”。有报道指这次并购的金额为1.3亿美元，但这一金额并未获得IBM的证实。\[6\]

2016年2月19日，IBM斥資26億美元收購健康分析公司[Truven](../Page/Truven.md "wikilink")。私募基金Veritas於2012年以13億美元收購Truven。

2016年2月24日，IBM
向美国证券交易委员会（SEC）提交的报告显示，该公司去年收购私有云计算公司Cleversafe的金额超过13亿美元。\[7\]

2018年10月28日，IBM以每股190美元現金收購全球最大混合雲服務供應商[紅帽公司](../Page/紅帽公司.md "wikilink")，較後者上周五(26日)收市價116.68美元高出63%，連債估值340億美元。收購交易料於2019年下半年完成，交易完成後，紅帽公司將併入IBM的混合雲部門，紅帽CEO吉姆·懷特赫斯特（Jim
Whitehurst）將加入IBM高管團隊，向IBM CEO羅睿蘭（Ginni Rometty）匯報

### 中國分公司

  - 周伟焜
  - 钱大群

## 參看

  - [IBM X系列伺服器](../Page/IBM_X系列伺服器.md "wikilink")
  - [深藍 (電腦)](../Page/深藍_\(電腦\).md "wikilink")
  - [藍色基因](../Page/藍色基因.md "wikilink")
  - [沃森](../Page/湯瑪斯·J·華生.md "wikilink")

## 参考文献

## 外部链接

  -
  - [IBM - 中国](https://www.ibm.com/cn-zh/)

  - [IBM - Hong Kong](https://www.ibm.com/hk-en/)

  - [IBM - Taiwan](https://www.ibm.com/tw-zh/)

[IBM](../Category/IBM.md "wikilink")
[IBM](../Category/道瓊斯工業平均指數成份股.md "wikilink")
[IBM](../Category/跨国公司.md "wikilink")
[IBM](../Category/東京證券交易所已除牌公司.md "wikilink")
[Category:1911年成立的公司](../Category/1911年成立的公司.md "wikilink")
[Category:外包公司](../Category/外包公司.md "wikilink")

1.
2.  憂鬱巨人IBM，智庫文化出版
3.  [IBM任命首位女CEO罗曼提彭明盛任董事长](http://money.163.com/11/1026/09/7H9H4O1U00253B0H.html)
4.
5.
6.
7.