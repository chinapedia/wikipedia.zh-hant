[Iron_ladle_from_Sirpur_Excavation_(9).jpg](https://zh.wikipedia.org/wiki/File:Iron_ladle_from_Sirpur_Excavation_\(9\).jpg "fig:Iron_ladle_from_Sirpur_Excavation_(9).jpg")
[Ladle_silver_1876-7.JPG](https://zh.wikipedia.org/wiki/File:Ladle_silver_1876-7.JPG "fig:Ladle_silver_1876-7.JPG")汤勺
- 纯度标记为London银 1876-7 （背景为5厘米宽的方格）\]\]
[Ladle_steel_18j07.JPG](https://zh.wikipedia.org/wiki/File:Ladle_steel_18j07.JPG "fig:Ladle_steel_18j07.JPG")汤勺（背景为5厘米宽的方格）\]\]
[Chaco_Anasazi_ladle_NPS.jpg](https://zh.wikipedia.org/wiki/File:Chaco_Anasazi_ladle_NPS.jpg "fig:Chaco_Anasazi_ladle_NPS.jpg")出土的公元前10世纪的汤勺\]\]

**汤勺**是一种用来盛[汤](../Page/汤.md "wikilink")、[濃湯等食品的](../Page/濃湯.md "wikilink")[勺子](../Page/勺子.md "wikilink")。\[1\]汤勺的设计大同小异，通常有着长长的勺柄，末端则是碗状的头部，用来将液体从锅等容器中转移至[碗里](../Page/碗.md "wikilink")。有的汤勺的边缘会有个小凸起，以便精细控制汤的流量，但这也会为[左利手者带来不便](../Page/左利手.md "wikilink")，为此有的汤勺会设计成两边都有突起的样式。

与其他厨具类似，现代社会中的汤勺通常是由[不鏽鋼](../Page/不鏽鋼.md "wikilink")[合金制成](../Page/合金.md "wikilink")，但有的也会用[铝](../Page/铝.md "wikilink")、[銀](../Page/銀.md "wikilink")、[塑料](../Page/塑料.md "wikilink")、[三聚氰胺-甲醛树脂](../Page/三聚氰胺-甲醛树脂.md "wikilink")、[木材](../Page/木材.md "wikilink")、[竹子等其他材料制作而成](../Page/竹.md "wikilink")。汤勺因用途不同而大小不一。比如，有的汤勺不到，用来处理调味酱；也有的汤勺大到，用来处理汤或者[賓治酒](../Page/賓治酒.md "wikilink")。\[2\]

在古代，汤勺常常是用[葫芦](../Page/葫芦.md "wikilink")、海贝等物品制成。\[3\]

## 参考资料

[Category:匙](../Category/匙.md "wikilink")

1.
2.
3.