**加利福尼亞魚龍屬**（屬名：*Californosaurus*）是一種生存在晚[三疊紀](../Page/三疊紀.md "wikilink")[卡尼階的魚龍類](../Page/卡尼階.md "wikilink")，化石發現於[加州的下Hosselkus石灰岩層](../Page/加州.md "wikilink")。加利福尼亞魚龍也被稱為[皮氏薩斯特魚龍與](../Page/薩斯特魚龍.md "wikilink")*Delphinosaurus*。牠們也是目前已知最原始的[真魚龍類](../Page/真魚龍類.md "wikilink")。
[Californoscale.png](https://zh.wikipedia.org/wiki/File:Californoscale.png "fig:Californoscale.png")
與身體其他部分相比，加利福尼亞魚龍的長口鼻部與頭部較小，與[混魚龍](../Page/混魚龍.md "wikilink")、[杯椎魚龍等基礎魚龍類相似](../Page/杯椎魚龍.md "wikilink")。牠們的尾巴銳利地往下彎，與更先進的魚龍類相同，並有垂直的錨尾。加利福尼亞魚龍擁有小型的背鰭。[薦椎前的脊椎骨有](../Page/薦椎.md "wikilink")45到50個。牠們的[掌骨呈圓形](../Page/掌骨.md "wikilink")，並且平坦分布，使鰭狀肢呈現圓形外表。加利福尼亞魚龍身長為3公尺。牠們以魚類及其他小型海洋生物為主食。和其他魚龍類一樣，加利福尼亞魚龍很可能一生都不會離開水域，所以生產也是在水中進行。

## 參考資料

  - [Palaeos
    Vertebrates網站](https://web.archive.org/web/20101221003314/http://www.palaeos.com/Vertebrates/Units/210Eureptilia/400.html)
  - [加利福尼亞漁龍](https://web.archive.org/web/20070929130906/http://home.scarlet.be/ichthyosaurus/californopas.html)

[category:魚龍目](../Page/category:魚龍目.md "wikilink")

[Category:三疊紀爬行動物](../Category/三疊紀爬行動物.md "wikilink")