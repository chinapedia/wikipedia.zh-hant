**溴酸**的[化学式为HBrO](../Page/化学式.md "wikilink")<sub>3</sub>，是[溴的](../Page/溴.md "wikilink")[含氧酸之一](../Page/含氧酸.md "wikilink")，其中溴的[氧化态为](../Page/氧化态.md "wikilink")+5。它形成的[盐类称为](../Page/盐.md "wikilink")[溴酸盐](../Page/溴酸盐.md "wikilink")，衍生出的酸根离子称为“溴酸根”离子。固态溴酸及溴酸盐与[氯酸](../Page/氯酸.md "wikilink")/[氯酸盐类似](../Page/氯酸盐.md "wikilink")，都具有强[氧化性](../Page/氧化性.md "wikilink")，可与还原性物质剧烈反应。

溴酸是[Belousov-Zhabotinsky振荡反应中的试剂之一](../Page/Belousov-Zhabotinsky振荡反应.md "wikilink")。

## 参考资料

  -
[Category:溴化合物](../Category/溴化合物.md "wikilink")
[\*](../Category/溴酸盐.md "wikilink")
[Category:无机酸](../Category/无机酸.md "wikilink")
[Category:腐蝕性化學品](../Category/腐蝕性化學品.md "wikilink")
[Category:缺少物质图片的化学品条目](../Category/缺少物质图片的化学品条目.md "wikilink")