**Comodo Firewall
Pro**是美国[科摩多集團开发的一款免费的](../Page/科摩多集團.md "wikilink")[防火墙软件](../Page/防火墙.md "wikilink")。

## 特点

  - Comodo Firewall是少数完全实现TCP/UDP全状态检测的个人防火墙。Comodo
    Firewall自身集成了COMODO已认证程序数据库，包括了近20,000个通过认证的可执行文件的数字认证（校验）信息。
  - 提供两层保护，对外的网络防火墙和对内的应用程序防火墙（应用程序＋组件检测）。
  - 提供多种语言界面，其中包含简体及繁体中文版。\[1\]
  - 5.0版本开始加入了[云分析](../Page/云计算.md "wikilink")（Cloud）。

## 參見

  - [Comodo Group](../Page/Comodo_Group.md "wikilink")
  - [Comodo AntiVirus](../Page/Comodo_AntiVirus.md "wikilink")
  - [Comodo Dragon](../Page/Comodo_Dragon.md "wikilink")

## 注釋

<div class="references-small">

<references />

</div>

## 外部链接

  - [Comodo Firewall Pro官方网站](http://www.personalfirewall.comodo.com/)
  - 在[知名信息安全机构Matousec.com对个人防火墙进行的防漏洞测试（Leak
    test）](http://www.matousec.com/projects/firewall-challenge/results.php)中Comodo
    Firewall排名第一。（测试）
  - [COMODO
    V5.9版防火牆用的正體中文語系檔](http://home.gamer.com.tw/creationDetail.php?sn=1509180)

[Category:防火墙软件](../Category/防火墙软件.md "wikilink")
[Category:免費軟件](../Category/免費軟件.md "wikilink")

1.  最新版（5.9）開始已無官方繁體版本，但仍有許多熱心網友自行翻譯、釋出正體中文語系檔