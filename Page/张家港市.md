**张家港市**位于[长江下游南岸](../Page/长江.md "wikilink")，是[中國](../Page/中國.md "wikilink")[江苏省](../Page/江苏省.md "wikilink")[苏州市代管](../Page/苏州市.md "wikilink")[县级市](../Page/县级市.md "wikilink")。张家港市其综合竞争力[列2010年全国百强县（市）第一梯队](../Page/中国百强县#县域经济论坛评价.md "wikilink")，全市总面积998.48平方公里，2012年户籍总人口91.02万。

## 行政区划

  - 张家港市共辖[杨舍](../Page/杨舍镇.md "wikilink")、[塘桥](../Page/塘桥镇.md "wikilink")、[金港](../Page/金港镇.md "wikilink")、[大新](../Page/大新镇_\(张家港市\).md "wikilink")、[乐余](../Page/乐余镇.md "wikilink")、[锦丰](../Page/锦丰镇.md "wikilink")、[南丰](../Page/南丰镇_\(张家港市\).md "wikilink")、[凤凰和由江苏省农垦划归的原江苏国营常阴沙农场](../Page/凤凰镇_\(张家港市\).md "wikilink")（[张家港市现代农业示范园区](../Page/张家港市现代农业示范园区.md "wikilink")）九个乡镇。

## 历史

张家港市所在区域的北部原是长江的一部分，成陆较晚；南部地区的发展则与附近地区类似，最早发现的人类活动在新石器时代[马家浜文化以前](../Page/马家浜文化.md "wikilink")，自商末起属吴国；秦代属[会稽郡](../Page/会稽郡.md "wikilink")，晋代置[暨阳县](../Page/暨阳县_\(刘宋\).md "wikilink")。[南朝梁设](../Page/南朝梁.md "wikilink")[梁丰县](../Page/梁丰县.md "wikilink")。隋唐之后分属常熟、江阴两县。清代至民国，常通港以北属南通县。

抗日战争时期，中国共产党曾一度在北部沙洲地区建立沙洲县，南部及常熟、江阴两县的边界地区设立虞西县。解放后，东部属常熟县，西部属江阴县。

1962年，由江阴县划出九个公社，常熟县划出十四个公社以及常阴沙农场，建立沙洲县，隶属苏州地区。张家港从此成为一个县级行政单位。

1986年9月，国务院批准撤消沙洲县，改设张家港市，隶属地级苏州市。

2004年，江苏农垦将原江苏国营常阴沙农场划归张家港市，实行属地管理。

## 经济

2012年，实现地区生产总值2050亿元，完成全社会固定资产投资703亿元，实现地方公共财政预算收入149.6亿元，规模以上工业总产值4702.9亿元，城镇居民人均可支配收入、农村居民人均纯收入分别达39695元和19460元。实际利用外资9.5亿美元，完成进出口总额319.62亿美元，口岸货物吞吐量2.5亿吨，集装箱运量150万标箱。

## 地理

张家港市东南毗邻[常熟市](../Page/常熟市.md "wikilink")，西面毗邻[江阴市](../Page/江阴市.md "wikilink")，北面隔[长江与](../Page/长江.md "wikilink")[南通市](../Page/南通市.md "wikilink")、[如皋市及](../Page/如皋市.md "wikilink")[靖江市相望](../Page/靖江市.md "wikilink")。

张家港属长江下游冲积平原，属[亚热带季风气候](../Page/亚热带季风气候.md "wikilink")，海洋性强，年平均气温15.5℃。张家港市全市总面积999平方公里（其中陆域面积777平方公里），全市人口85万人。

## 方言

张家港方言主要有虞西话、澄东话、澄要话、老沙话、常阴沙话（又称崇明话）五大类，另有少量苏北话。其中虞西话、澄东话、老沙话和常阴沙话分布地域最广、人口最多，成为境内有代表性的方言；苏北话有南通话、如皋话、泰兴话和靖江话。

## 交通

### 公路

  - 第一条公路——澄([江陰](../Page/江陰.md "wikilink"))巫([張家港巫山](../Page/張家港.md "wikilink"))公路、澄(江陰)豐(張家港錦豐)公路和常([常熟](../Page/常熟.md "wikilink"))十(张家港十二圩港)公路，由張家港錦豐人、上海[交通大學畢業生](../Page/交通大學.md "wikilink")、抗日愛國將領[陳一白少將出資](../Page/陳一白.md "wikilink")、設計、勘測、建造。
      - 1932年“[一·二八](../Page/一·二八.md "wikilink")”淞滬抗戰爆發，[朱家驊任交通部長](../Page/朱家驊.md "wikilink")，全力發展交通。1933年7月，國民政府指派[陳一白鋪築了澄](../Page/陳一白.md "wikilink")(江陰)巫(張家港巫山)軍用公路，18公里，路基寬9米，1934年1月完工。
      - 1937年春國民政府鞏固江防，再次由[陳一白自江陰要塞向東](../Page/陳一白.md "wikilink")，經楊舍、福前鎮東、東萊、合興至錦豐，搶築軍用公路。春季測量，盛夏施工，建橋築基。同時常(熟)十(二圩港)公路段始築，43公里，路基寬7米，路面寬3米。1943年1月9日常熟鹿苑公路段試行通車。

张家港距[上海](../Page/上海.md "wikilink")98.1公里、[苏州](../Page/苏州.md "wikilink")58公里、[无锡](../Page/无锡.md "wikilink")40公里、[常州](../Page/常州.md "wikilink")70公里、[南通](../Page/南通.md "wikilink")30公里、[南京](../Page/南京.md "wikilink")200公里，距[苏南硕放国际机场](../Page/苏南硕放国际机场.md "wikilink")37公里、[上海虹桥国际机场](../Page/上海虹桥国际机场.md "wikilink")98公里、[上海浦东国际机场](../Page/上海浦东国际机场.md "wikilink")150公里、[南京禄口国际机场](../Page/南京禄口国际机场.md "wikilink")200公里。

### 航道

  - [张家港港](../Page/张家港港.md "wikilink")：沿长江岸线达64多公里，其中可建万吨级以上泊位的深水岸线达33公里。现有万吨级以上泊位65个。
  - 2012年货物吞吐量达到2.5亿吨，其中集装箱运量150.2万标箱。口岸货物吞吐量连续三年超2亿吨，居全国县域口岸之首。

## 工业经济

张家港市是2007年[中国百强县第三名](../Page/中国百强县.md "wikilink")，拥有全国首个也是仅有的两个的内河保税区之一的[张家港保税区](../Page/张家港保税区.md "wikilink")（另一个为[重庆寸滩保税港区](../Page/重庆寸滩保税港区.md "wikilink")）。

2009年百强县与江阴、昆山并列第一。

十大集团企业：[江苏沙钢集团](../Page/江苏沙钢集团.md "wikilink")（线材、板材、铜业、宽厚板、不锈钢、镀锌板等）、[东海粮油](../Page/东海粮油.md "wikilink")、[华芳](../Page/华芳.md "wikilink")、[永钢](../Page/永钢.md "wikilink")、[牡丹（汽车）](../Page/牡丹（汽车）.md "wikilink")、[张铜](../Page/张铜.md "wikilink")、[澳洋](../Page/澳洋.md "wikilink")、[华润（玻璃）](../Page/华润（玻璃）.md "wikilink")、[联合铜业](../Page/联合铜业.md "wikilink")、[骏马化纤](../Page/骏马化纤.md "wikilink")。

根据2011年数据，沙钢集团销售额2075亿人民币，为中国第一大民营企业，同时是江苏省内最大的企业。东海粮油为亚洲最大食用油生产基地。

## 教育

  - 沙洲工学院：1984年创办，全国第一家县办大学。

江苏科技大学（张家港校区）：占地856亩，校舍建筑面积26万平方米，规划全日制在校本科生规模为5000人。
有国家级重点中学梁丰高级中学，江苏省四星级中学：张家港职业教育中心校、沙洲中学、暨阳高级中学、张家港高级中学，塘桥高级中学,其余各高级中学全为省级重点中学，教育软硬件设施极为雄厚。

## 城市荣誉

  - 张家港市先后荣获“全国文明城市”、“国家卫生城市”、“全国环保模范城市”、“全国文化先进县（市）”、“全国计划生育优质服务示范市”和“全国双拥模范城”，全市各镇全部建成“国家卫生镇”。2005年获得“国际花园城市”称号。张家港由于在城乡综合发展方面的开拓创新，获得2008年的联合国人居奖。

## 历史名人

  - [陳一白](../Page/陳一白.md "wikilink")(楚寶、南琛、維邨)(1903.-1952.11.)：[張家港錦豐鬱家橋鎮人](../Page/張家港.md "wikilink")，中國兩個半電訊專家之一，國民政府航空委員會防空總臺首任少將總臺長。
  - [錢昌祚](../Page/钱昌祚_\(航空工程师\).md "wikilink")：[張家港鹿苑人](../Page/張家港.md "wikilink")，著名航空機械專家。
  - [錢昌照](../Page/錢昌照.md "wikilink")：[張家港鹿苑人](../Page/張家港.md "wikilink")，響應毛主席號召參加新中國建設
  - [曹振民](../Page/曹振民.md "wikilink")：青果巷人，[抗日戰爭中雲南壘允防空指揮部總台長](../Page/抗日戰爭.md "wikilink")[陳一白少將的助手](../Page/陳一白.md "wikilink")。
  - [黃元明](../Page/黃元明.md "wikilink")：[張家港塘桥镇黃家橋人](../Page/張家港.md "wikilink")，[抗日戰爭中雲南壘允防空指揮部總台長](../Page/抗日戰爭.md "wikilink")[陳一白少將的助手](../Page/陳一白.md "wikilink")。
  - [樊友諒](../Page/樊友諒.md "wikilink")：[張家港福全鎮二圩港人](../Page/張家港.md "wikilink")。昆明第十修理廠，雲南壘允中央飛機製造廠飛虎隊地勤發動機組技術員。發動機聽診專家。
  - [周光祚](../Page/周光祚.md "wikilink")(1919年出生)：[張家港鹿苑人](../Page/張家港.md "wikilink")。南昌航空機械學校老師，重慶航空機械學校分校助教，四川溫江機場協助蘇聯志願隊、陳納德飛虎隊翻譯，成都華西協會大學英語教師
    。

## 外部链接

  - [中国张家港](http://www.zjg.gov.cn)
  - [江阴沙洲分图](http://www.wdl.org/zh/item/11377/)

[张家港市](../Category/张家港市.md "wikilink")
[市](../Category/苏州区市.md "wikilink")
[Category:中国国家级生态市区县](../Category/中国国家级生态市区县.md "wikilink")
[苏州市](../Category/江苏县级市.md "wikilink")
[苏](../Category/中国中等城市.md "wikilink")
[1](../Category/全国文明城市.md "wikilink")
[苏](../Category/国家卫生城市.md "wikilink")