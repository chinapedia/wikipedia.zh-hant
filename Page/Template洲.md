[盤古大陸](../Page/盤古大陸.md "wikilink"){{·}}
[潘諾西亞大陸](../Page/潘諾西亞大陸.md "wikilink"){{·}}
[羅迪尼亞大陸](../Page/羅迪尼亞大陸.md "wikilink"){{·}}
[哥倫比亞大陸](../Page/哥倫比亞大陸.md "wikilink"){{·}}
[凱諾蘭大陸](../Page/凱諾蘭大陸.md "wikilink"){{·}}
[妮娜大陸](../Page/妮娜大陸.md "wikilink"){{·}}
[烏爾大陸](../Page/烏爾大陸.md "wikilink"){{·}}
[瓦巴拉大陸](../Page/瓦巴拉大陸.md "wikilink")

</div>

</div>

`|col2width = 4%`
`|col2 = '' ''`
`|col3width = 58%`
`|col3 = `

<div>

**[地质史上的大陆](../Page/洲.md "wikilink")**

<div style="padding-bottom:0.5em; line-height:1.3em;">

[北極大陸](../Page/北極大陸.md "wikilink"){{·}}
[亚美大陆](../Page/亚美大陆.md "wikilink"){{·}}
[大西洋大陆](../Page/大西洋大陆.md "wikilink"){{·}}
[阿瓦隆尼亞大陸](../Page/阿瓦隆尼亞大陸.md "wikilink"){{·}}
[波羅的大陸](../Page/波羅的大陸.md "wikilink"){{·}}
[辛梅利亞大陸](../Page/辛梅利亞大陸.md "wikilink"){{·}}
[剛果克拉通](../Page/剛果克拉通.md "wikilink"){{·}}
[歐美大陸](../Page/歐美大陸.md "wikilink"){{·}}
[喀拉哈里克拉通](../Page/喀拉哈里克拉通.md "wikilink"){{·}}
[哈薩克大陸](../Page/哈薩克大陸.md "wikilink"){{·}}
[劳伦大陆](../Page/劳伦大陆.md "wikilink"){{·}}
[華北陸塊](../Page/華北陸塊.md "wikilink")
{{·}}[西伯利亞大陸](../Page/西伯利亞大陸.md "wikilink"){{·}}
[扬子克拉通](../Page/扬子克拉通.md "wikilink"){{·}}
[烏爾大陸](../Page/烏爾大陸.md "wikilink"){{·}}
[東南極克拉通](../Page/東南極克拉通.md "wikilink") {{·}}
[印度次大陸](../Page/印度次大陸.md "wikilink")

</div>

</div>

}}

<hr />

[西兰大陆](../Page/西兰大陆.md "wikilink")

</div>

`|col2 = `

<div>

**[未来可能形成的](../Page/超大陸#未來可能形成的超大陸.md "wikilink")[超大陸](../Page/超大陸.md "wikilink")**
[終極盤古大陸](../Page/終極盤古大陸.md "wikilink"){{·}}
[阿美西亞大陸](../Page/阿美西亞大陸.md "wikilink"){{·}}[新盤古大陸](../Page/新盤古大陸.md "wikilink")

</div>

`|col3 = `

<div>

**[神话与猜测的的大陆](../Page/神话中的地点列表.md "wikilink")**
[庫馬里坎達姆大陸](../Page/庫馬里坎達姆大陸.md "wikilink"){{·}}
[雷姆利亞大陸](../Page/雷姆利亞大陸.md "wikilink"){{·}}
[麦若普斯大陆](../Page/麦若普斯大陆.md "wikilink"){{·}}
[姆大陆](../Page/姆大陆.md "wikilink"){{·}}
[未知的南方大陸](../Page/未知的南方大陸.md "wikilink"){{·}}
[亚特兰蒂斯](../Page/亚特兰蒂斯.md "wikilink")

</div>

}}

</div>

|below = *另见[世界分区](../Page/Template:世界分区.md "wikilink")* }}<noinclude>
</noinclude>