**植龍屬**（屬名：*Phytosaurus*）是[植龍目的一個屬](../Page/植龍目.md "wikilink")，也是第一種被命名的植龍目，由G.Jaeger在1828年所敘述、命名，但目前的狀態是[疑名](../Page/疑名.md "wikilink")；植龍目是一群外表類似[鱷魚的已滅絕](../Page/鱷魚.md "wikilink")[主龍類](../Page/主龍類.md "wikilink")\[1\]
。植龍擁有類似鱷魚的身體結構，但牠們的鼻孔位在頭上方，而非口鼻部前端。植龍的屬名意為「植物蜥蜴」，但牠們現在被認為是[肉食性](../Page/肉食性.md "wikilink")，[模式種是](../Page/模式種.md "wikilink")*P.
cylindricodon*。

## 參考資料

<div class="references-small">

<references>

</references>

</div>

[Category:植龍目](../Category/植龍目.md "wikilink")

1.