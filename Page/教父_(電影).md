是一部1972年的[美國](../Page/美國.md "wikilink")[幫派電影](../Page/幫派電影.md "wikilink")，根據[馬里奧·普佐](../Page/馬里奧·普佐.md "wikilink")（Mario
Puzo）的[同名暢銷小說改編](../Page/教父_\(小说\).md "wikilink")，[法蘭西斯·柯波拉執導](../Page/法蘭西斯·柯波拉.md "wikilink")，由[馬龍·白蘭度和](../Page/馬龍·白蘭度.md "wikilink")[艾爾·帕西諾主演](../Page/艾爾·帕西諾.md "wikilink")。

《教父》是《教父三部曲》\[1\]的第一集，掀起[黑幫電影新潮流](../Page/黑幫.md "wikilink")，榮獲[第45屆奧斯卡金像獎最佳電影](../Page/第45屆奧斯卡金像獎.md "wikilink")、最佳男主角及最佳改編劇本三大獎項，被視為是經典電影之一。《教父》在[網際網路電影資料庫](../Page/網際網路電影資料庫.md "wikilink")（IMDb）的史上最佳250部電影評選中，獲影迷票選為第二名，而其續集《[教父2](../Page/教父2.md "wikilink")》則榮獲第三名\[2\]。

## 情節介紹

1945年[二次大戰結束後](../Page/二次大戰.md "wikilink")，[维托·柯里昂作為](../Page/维托·柯里昂.md "wikilink")[柯里昂家族的](../Page/柯里昂家族.md "wikilink")[族長](../Page/族長.md "wikilink")、[紐約最有勢力的](../Page/紐約.md "wikilink")[黑手黨](../Page/黑手黨.md "wikilink")[教父之一](../Page/教父.md "wikilink")，在女兒康妮的婚礼上，维托的幼子[陸戰隊](../Page/海军陆战队员.md "wikilink")[軍官](../Page/軍官.md "wikilink")[麥可·柯里昂](../Page/麥可·柯里昂.md "wikilink")，在婚禮宴會上將自己的女友凱伊·亞當斯介绍给家人們。在另外一間黑暗的辦公室裏，维托聆聽著同鄉們的請求，答應懲治[輪姦且將少女打成重傷的惡徒們](../Page/輪姦.md "wikilink")，也答應幫助同鄉的糕點學徒不要被[移民局](../Page/移民局.md "wikilink")[驅逐出境](../Page/驅逐出境.md "wikilink")。维托的[教子约翰尼](../Page/教子.md "wikilink")·馮塔納是一位著名的歌手，為了獲得一個[好萊塢電影角色尋求维托的幫助](../Page/好萊塢.md "wikilink")，维托迅速派遣他的養子兼軍師湯姆·黑根前往[加州](../Page/加州.md "wikilink")[洛杉磯](../Page/洛杉磯.md "wikilink")，[游說電影制片人杰克](../Page/游說.md "wikilink")·沃爾茨把那個角色给约翰尼。沃爾茨拒绝了，隔日沃爾茨在床上醒來，驚覺自己最珍爱種馬的斷頭正血淋淋地裹在床單裡，他立刻屈服。

湯姆回紐約後，毒梟索拉索找上教父維托，因為維托供養許多[美國國會的](../Page/美國國會.md "wikilink")[參議員](../Page/參議員.md "wikilink")、[眾議員](../Page/眾議員.md "wikilink")，在[政壇力量極大](../Page/美國政治.md "wikilink")，索拉索想與維托洽談合作[販毒](../Page/販毒.md "wikilink")，但維托認為「盜亦-{}-有道」，[賭博](../Page/賭博.md "wikilink")、[飲酒](../Page/飲酒.md "wikilink")、[女色都是人類生活偶爾的](../Page/作愛.md "wikilink")[奢侈品](../Page/奢侈品.md "wikilink")，但[吸毒卻足以毁掉人的一生](../Page/吸毒.md "wikilink")，而且他供養的[政客也不會支持他這樣做](../Page/政客.md "wikilink")，[賭場與](../Page/賭場.md "wikilink")[妓院已足以撐起他的產業](../Page/妓院.md "wikilink")，決意不涉足[毒品](../Page/毒品.md "wikilink")，因此惹來索拉索的不滿。索拉索不但買兇[行刺維托](../Page/行刺.md "wikilink")，維托身負重傷，一息僅存；索拉索又[綁架維托的](../Page/綁架.md "wikilink")[義子律師兼軍師湯姆](../Page/義子.md "wikilink")；加上麥考斯基警長與索拉索串通，令麥可處處求救碰壁。麥可決定約索拉索和麥考斯基會談，假意求和，實則將二人[刺殺](../Page/刺殺.md "wikilink")。維托與湯姆的安全無虞，而麥可槍殺二人後，聽從兄長的勸告和安排，拋下美國的一切，逃回[義大利老家](../Page/義大利.md "wikilink")[西西里去避風頭](../Page/西西里.md "wikilink")，並在該地認識了一名美麗女孩阿波羅妮亞，進而相戀成婚。

1948年，[紐約黑幫為爭地盤互相廝殺](../Page/紐約.md "wikilink")，維托長子桑尼，乘機殺了菲立浦·塔塔基利亞之子布諾·塔塔基利亞，又由於妹妹被夫婿[家暴](../Page/家暴.md "wikilink")，桑尼在接到妹妹打來的哭訴電話後，怒氣沖沖地獨自前去妹妹家，卻遇到敵人設好的圈套遭亂槍掃射而亡。而次子弗雷多没有領袖氣度，被認為無能接手家族事業，教父維托也決定[退位](../Page/退位.md "wikilink")，[禪讓予幼子麥可](../Page/禪讓.md "wikilink")。遠在西西里的麥可，也因為其身為柯里昂家族的身份而被對手盯上，貼身保鑣被敵人買通，麥可的義大利嬌妻，更因此成為他的[替死鬼](../Page/替死鬼.md "wikilink")，在麥可面前被炸死於車內。嬌妻慘死的衝擊，帶給麥可巨大的刺激，麥可知道他的一生除了黑幫之外，恐怕再無他路，因此麥可接受了父親的召喚，回到美國，與原女友凱伊成婚，接手家族事業。

[Al_Pacino_and_Robert_Duvall_in_the_Godfather.jpg](https://zh.wikipedia.org/wiki/File:Al_Pacino_and_Robert_Duvall_in_the_Godfather.jpg "fig:Al_Pacino_and_Robert_Duvall_in_the_Godfather.jpg")
老教父維托，先與[黑手黨](../Page/黑手黨.md "wikilink")[五大家族開了](../Page/五大家族.md "wikilink")[黑手黨委員會](../Page/黑手黨委員會.md "wikilink")，在某些部分對五大家族讓步，換取各家族不追殺麥可的承諾之後，才讓麥可返美。維托也察覺到長子桑尼之死，極有可能是五大家族之一巴西尼家族所為，並且向麥可叮嚀，家族內任何一人通知他巴西尼要會面，那人就是叛徒。老教父維托與孫子安東尼嬉戲時病發去世，[告別式上五大家族的人車馬輻輳](../Page/告別式.md "wikilink")，冠蓋雲集，而當麥可在對眾人察言觀色之時，心腹泰西歐來通知巴西尼要與麥可開會，於是麥可得知泰西歐是叛徒。

在康妮新生子的[洗禮上](../Page/洗禮.md "wikilink")，麥可成為其[教父](../Page/教父.md "wikilink")，但其實麥可要一次[肅清家族所有之敵人](../Page/肅清.md "wikilink")，此時影片以交叉式手法，將[屠殺式報復貫穿在](../Page/屠殺.md "wikilink")[教堂的宗教儀式](../Page/教堂.md "wikilink")，麥可一面向[神父承諾自己篤信](../Page/神父.md "wikilink")[三位一體與](../Page/三位一體.md "wikilink")[羅馬公教](../Page/羅馬公教.md "wikilink")，不接受「[撒旦的罪行](../Page/撒旦.md "wikilink")」，卻一面指揮了大量的[刺殺行動](../Page/刺殺.md "wikilink")，包括：

  - 五大黑幫家族之一史特基家族首領——安東尼·史特基，與他的貼身護衛從電梯出來時，遭到彼特·克雷曼沙刺殺身亡。

<!-- end list -->

  - 五大黑幫家族之一克里安諾家族首領——歐提里歐·克里安諾，離開旅館時被困在旋轉門中，遭殺手槍殺而死。

<!-- end list -->

  - 五大黑幫家族之一塔塔基利亞家族首領——菲立浦·塔塔基利亞，與一名女人在旅館做愛時，雙雙遭到羅可（Rocco）與另一名殺手槍擊而死。

<!-- end list -->

  - [拉斯維加斯旅館大亨莫](../Page/拉斯維加斯.md "wikilink")·格林，在他的旅館按摩時，被泰西歐以槍射入右眼而死。

<!-- end list -->

  - 五大黑幫家族之一巴西尼家族首领——艾米立歐·巴西尼，也是殺害桑尼的幕後元兇，在法院前，與他的司機兩人，遭到假扮警員的艾爾·奈瑞（Al
    Neri）槍殺而死。

最後，麥可也處決了家族叛徒泰西歐與姐夫卡洛，麥可已經發現姐夫卡洛家暴的當天，桑尼去關切就遇刺，他認為可能是卡洛被收買而害死桑尼，哄騙卡洛坦白從寬，讓他退出家族事業，去拉斯维加斯享清福，於是卡洛承認了與巴西尼串通，殺害桑尼。隨後命令克雷曼沙把卡洛[縊死](../Page/縊死.md "wikilink")，麥可的新一任教父地位穩固。最後麥可領悟到重要的一點，就算投敵的是親人，也絕對不可饒赦。

麥可在門內接受眾人的[吻手禮](../Page/吻手禮.md "wikilink")，手下把他辦公室的門扉關上，這扇門將麥可與妻子隔開，間接暗示了兩人未來的婚姻危機，以及麥可邁向了與先前截然不同的人生。

## 演員表

  - [马龙·白兰度飾演](../Page/马龙·白兰度.md "wikilink")[维托·柯里昂](../Page/维托·柯里昂.md "wikilink")，[柯里昂家族的](../Page/柯里昂家族.md "wikilink")[當家](../Page/老板_\(黑手党\).md "wikilink")，[西西里裔美國人](../Page/義大利裔美國人.md "wikilink")，娶了卡梅拉·柯里昂，育有桑尼、弗雷多、麥克和康妮，還有一名義子湯姆。
  - [艾爾·帕西諾飾演](../Page/艾尔·帕西诺.md "wikilink")[麥克·柯里昂](../Page/迈克尔·柯里昂.md "wikilink")，维托的第四個小孩，第三個兒子，[第二次世界大戰中由於](../Page/第二次世界大戰.md "wikilink")[日軍](../Page/日軍.md "wikilink")[偷襲珍珠港](../Page/偷襲珍珠港.md "wikilink")，憤而從軍，成為[美國海軍陸戰隊軍官](../Page/美國海軍陸戰隊.md "wikilink")，家族中唯一從大學畢業的成員，最初遠離江湖，在一連串的事件後，決意協助父親，繼承家業，從家族的幼子，繼任為無情的黑手黨教父，是電影的主要角色。
  - [詹姆斯·肯恩飾演](../Page/詹姆斯·肯恩.md "wikilink")[圣提诺·“桑尼”·柯里昂](../Page/桑尼·柯里昂.md "wikilink")，维托的长子，生性急躁鲁莽，作为[二當家](../Page/二當家.md "wikilink")，他是柯里昂家族的[儲君](../Page/儲君.md "wikilink")。
  - [罗伯特·杜瓦尔飾演](../Page/罗伯特·杜瓦尔.md "wikilink")[湯姆·黑根](../Page/汤姆·黑根.md "wikilink")，维托的[義子](../Page/義子.md "wikilink")，屬於家族的外圍，是家族的律師和[軍師](../Page/顾问.md "wikilink")，與柯里昂家族不同，湯姆是[德裔和](../Page/德裔美国人.md "wikilink")[爱爾蘭裔的混血](../Page/爱尔兰裔美国人.md "wikilink")。
  - [黛安·基顿饰演](../Page/黛安·基顿.md "wikilink")[凯伊·亚当斯-柯里昂](../Page/凯伊·亚当斯-柯里昂.md "wikilink")，麥克的女友，不是意大利裔，后成为迈克尔第二任妻子，生有两个孩子。
  - [约翰·卡佐尔饰演](../Page/约翰·卡佐尔.md "wikilink")[弗雷德里克·“弗雷多”·柯里昂](../Page/弗雷多·柯里昂.md "wikilink")，维托的次子，性格庸懦，也不太机灵，被认为是柯里昂兄弟中最软弱的一个。
  - [塔利亚·夏尔饰演](../Page/塔利亚·夏尔.md "wikilink")[康斯坦齐亚·“康妮”·柯里昂](../Page/康妮·柯里昂.md "wikilink")，柯里昂家族第三個孩子和唯一的女儿，她的婚礼宴会开启了电影。
  - [吉亚尼·卢索饰演](../Page/吉亚尼·卢索.md "wikilink")[卡尔洛·瑞兹](../Page/卡尔洛·瑞兹.md "wikilink")，原是桑尼的朋友，是桑尼介绍给康妮，後成為康妮的丈夫，長時間對康妮施以[家暴](../Page/家暴.md "wikilink")，最终叛变倒向巴西尼家族。
  - [理查·卡斯特拉诺饰演](../Page/理查德·S·卡斯特拉诺.md "wikilink")[彼得·克雷曼沙](../Page/彼得·克雷曼沙.md "wikilink")，柯里昂家族的副頭目，是维托·柯里昂和泰西欧的老朋友；外表和藹可親，實際上是殺人如麻的笑面殺手。
  - [阿贝·维高达饰演](../Page/艾巴·维戈达.md "wikilink")[萨尔瓦托雷·泰西欧](../Page/萨尔瓦托雷·泰西欧.md "wikilink")，柯里昂家族的第二個副頭目，是维托·柯里昂和彼得·克雷曼沙的老朋友。
  - [艾尔·勒提埃里饰演](../Page/艾尔·勒提埃里.md "wikilink")[维吉尔·索拉索](../Page/维吉尔·索拉索.md "wikilink")，绰号“土耳其人”，[海洛英毒贩](../Page/海洛英.md "wikilink")，他想要通过维托·柯里昂的政治关系，同时寻找金融投资以及对塔塔基利亚家族毒品生意的保护。
  - [斯特林·海登饰演](../Page/斯特林·海登.md "wikilink")[马克·麦考斯基](../Page/马克·麦考斯基.md "wikilink")，一名[纽约市警察局的腐败警官](../Page/纽约市警察局.md "wikilink")，與索拉索合作。
  - [兰尼·蒙塔纳饰演](../Page/兰尼·蒙塔纳.md "wikilink")[卢卡·布拉西](../Page/卢卡·布拉西.md "wikilink")，效忠维托·柯里昂黑帮杀手：其看似笨拙遲鈍，處事一板一眼，被稱為「不被任何人阻止的可怕殺手」。
  - [理查德·孔特饰演](../Page/理查德·孔特.md "wikilink")[艾米利奥·巴西尼](../Page/艾米利奥·巴西尼.md "wikilink")，巴西尼家族的當家。
  - [艾尔·马蒂诺饰演](../Page/艾尔·马蒂诺.md "wikilink")[约翰尼·冯塔纳](../Page/约翰尼·冯塔纳.md "wikilink")，维托的教子，一位举世闻名的歌手，该角色原型为[弗兰克·西纳特拉](../Page/弗兰克·西纳特拉.md "wikilink")。
  - [约翰·马雷](../Page/约翰·马雷.md "wikilink")
    饰演[杰克·沃尔茨](../Page/杰克·沃尔茨.md "wikilink")，一位有权有势的好莱坞制片人。
  - [亚历克斯·罗科饰演](../Page/亚历克斯·罗科.md "wikilink")[莫·格林](../Page/莫·格林.md "wikilink")，一位柯里昂家族长期的合伙人，拥有拉斯维加斯宾馆，该角色原型为[本杰明·西格尔](../Page/本杰明·西格尔.md "wikilink")。
  - [莫加娜·金饰演](../Page/莫加娜·金.md "wikilink")[卡梅拉·柯里昂](../Page/卡梅拉·柯里昂.md "wikilink")，维托的妻子。
  - 萨尔瓦托雷·科尔西托饰演[亚美利哥·包纳萨拉](../Page/亚美利哥·包纳萨拉.md "wikilink")，一位[葬儀社老板](../Page/葬儀社.md "wikilink")，在最开场时因为自己女儿被两个男孩强奸未遂而打成重傷，请求维托·柯里昂協助复仇。
  - [克拉多·加伊帕饰演](../Page/克拉多·加伊帕.md "wikilink")[唐·托马西诺](../Page/唐·托马西诺.md "wikilink")，维托·柯里昂的意大利老友，为迈克尔逃到西西里时提供庇护。
  - [弗朗哥·希提饰演卡洛](../Page/弗朗哥·希提.md "wikilink")，迈克尔在西西里时的保镖之一。
  - [安杰洛·因范蒂饰演法布里奇奥](../Page/安杰洛·因范蒂.md "wikilink")，迈克尔在西西里时的保镖之一，他帮助准备了针对迈克尔的刺杀，结果誤殺了阿波罗妮亚。
  - [约翰尼·马蒂诺饰演](../Page/约翰尼·马蒂诺.md "wikilink")[保利·加托](../Page/保利·加托.md "wikilink")，彼得·克雷曼沙的部下，维托·柯里昂的司机，参与了针对维托的刺杀。
  - [维克托·伦迪纳饰演](../Page/维克托·伦迪纳.md "wikilink")[菲利普·塔塔基利亚](../Page/菲利普·塔塔基利亚.md "wikilink")，塔塔基利亚家族的當家。
  - [托尼·乔治奥饰演](../Page/托尼·乔治奥.md "wikilink")[布鲁诺·塔塔基利亚](../Page/布鲁诺·塔塔基利亚.md "wikilink")，菲利普·塔塔基利亚的儿子和家族的二當家，桑尼·柯里昂暗杀了他以报复维托·柯里昂的遇刺。
  - [西莫内塔·斯特凡内利饰演](../Page/西莫内塔·斯特凡内利.md "wikilink")[阿波罗妮亚·维特利-柯里昂](../Page/阿波罗妮亚·维特利-柯里昂.md "wikilink")，迈克尔在西西里島遇到的美少女，后与迈克尔在西西里结婚，在一场针对迈克尔的刺杀中遭到池魚之殃而成為[替死鬼](../Page/替死鬼.md "wikilink")。
  - [鲁迪·邦德饰演克里安諾阁下](../Page/鲁迪·邦德.md "wikilink")，纽约克里安諾家族的當家。
  - [路易斯·古斯饰演史特基阁下](../Page/路易斯·古斯.md "wikilink")，底特律史特基家族的當家。
  - [汤姆·罗斯奎饰演](../Page/汤姆·罗斯奎.md "wikilink")[罗科·兰波内](../Page/罗科·兰波内.md "wikilink")，彼得·克雷曼沙属下的士兵，最终成为柯里昂家族的副頭目。
  - [乔伊·斯皮内尔饰演](../Page/乔伊·斯皮内尔.md "wikilink")[威利·奇奇](../Page/威利·奇奇.md "wikilink")，柯里昂家族的士兵。
  - [理查德·布莱特饰演](../Page/理查德·布莱特.md "wikilink")[艾尔·奈里](../Page/艾尔·奈里.md "wikilink")，迈克尔·柯里昂的私人保镖和职业杀手，個性沉默寡言，手段兇殘。
  - [朱丽叶·格雷格饰演](../Page/朱丽叶·格雷格.md "wikilink")[桑德拉·柯里昂](../Page/桑德拉·柯里昂.md "wikilink")，桑尼的妻子，与桑尼生有四个孩子。
  - [珍妮·利内罗饰演](../Page/珍妮·利内罗.md "wikilink")[露西·曼西尼](../Page/露西·曼西尼.md "wikilink")，桑尼的情妇。
  - [索菲亚·科波拉饰演婴儿迈克尔](../Page/索菲亚·科波拉.md "wikilink")·弗朗西斯·瑞兹，迈克尔·柯里昂的外甥和教子。

## 製作

### 柯波拉與派拉蒙

[Francis_Ford_Coppola_2011_CC.jpg](https://zh.wikipedia.org/wiki/File:Francis_Ford_Coppola_2011_CC.jpg "fig:Francis_Ford_Coppola_2011_CC.jpg")|alt=A
photo of Francis Ford Coppola.\]\]
[法蘭西斯·柯波拉最初並非派拉蒙的第一選擇](../Page/法蘭西斯·柯波拉.md "wikilink")，其他至少有兩位導演是派拉蒙的首選。義大利導演[塞吉歐·李昂尼也有受到派拉蒙的邀請](../Page/塞吉歐·李昂尼.md "wikilink")，但是他對於這部歌頌[黑手黨的影片沒有興趣](../Page/黑手黨.md "wikilink")。（他之後執導他唯一的[黑幫電影](../Page/黑幫電影.md "wikilink")《[美國往事](../Page/美国往事.md "wikilink")》，是描述[美國猶太人幫派的故事](../Page/美國猶太人.md "wikilink")。）而根據當時派拉蒙總裁羅勃·艾文（Robert
Evans）的說法，柯波拉在最初也並非想接下執導工作，同樣也深怕著讚揚黑手黨與暴力的效應。另一方面，艾文也獨鍾義大利裔的美國導演接掌工作，主要是因過去非義大利裔的美國導演所作的黑手黨電影票房都很慘淡，也因此，艾文希望觀眾能夠看電影像「吃義大利麵」。當柯波拉想到可將影片隱喻美式資本主義時，他決定爭取執導工作\[3\]。在當時，柯波拉曾執導過8部影片，其中最著名的為改編自舞臺音樂劇的《[彩虹柚子](../Page/彩虹柚子.md "wikilink")》，而柯波拉也曾因與人合編《[巴頓將軍](../Page/巴頓將軍.md "wikilink")》獲得奧斯卡獎\[4\]。柯波拉在當時也欠債40萬美元，主因是他監製的[喬治·盧卡斯影片](../Page/喬治·盧卡斯.md "wikilink")《[五百年後](../Page/五百年後.md "wikilink")》超出[華納兄弟提供的預算所致](../Page/華納兄弟.md "wikilink")。最後柯波拉在盧卡斯的建議下，接掌《教父》執導工作\[5\]。

柯波拉與派拉蒙影業之間也有嚴重衝突，而柯波拉甚至差點被換掉。派拉蒙認為製作開始時不順利，但是柯波拉卻認為首周製作相當順利。派拉蒙認為柯波拉都無法按照進度，頻頻發生製作上與選角的失誤，徒增不必要的開銷。柯波拉在DVD的語音記事說過，他當時籠罩在替換導演的陰影下，儘管當時也極大的壓力，他還是堅持他的決定，並且設法避免被替換。

派拉蒙在當時製作也面臨財務危機，只能將《教父》作為孤注一擲，期望能重振派拉蒙影業，因此柯波拉也面臨了相當大的壓力。他們希望《教父》為廣大觀眾接受，並且要求柯波拉增加多一點刺激場面。而柯波拉也只好增加一些暴力場面，來滿足派拉蒙的期待。

### 選角

柯波拉在選角方面與派拉蒙又有些牴觸，其中以[馬龍·白蘭度所飾演的維托](../Page/馬龍·白蘭度.md "wikilink")·柯里昂為最。派拉蒙最初是希望[勞倫斯·奧利佛擔任此角](../Page/勞倫斯·奧利佛.md "wikilink")（後以健康問題拒絕），並且不希望由白蘭度飾演，主要鑒於白蘭度近期拍戲上的困難。一名派拉蒙高層提議由[丹尼·湯瑪斯擔任](../Page/丹尼·湯瑪斯.md "wikilink")，因為維托這個角色相當愛家，也因為這個觀點，當時的派拉蒙高層甚至說到「馬龍·白蘭度永遠不會在這部電影出現。」後來在一番懇求游說後，柯波拉才被允許讓白蘭度擔任此角色，但是白蘭度必須接受比自己之前片酬還低的價錢、螢幕試鏡，以及全面配合不得延誤製作進度\[6\]。白蘭度最後在試鏡上脫穎而出，並且在隔年奧斯卡獲得[最佳男主角獎](../Page/奧斯卡最佳男主角獎.md "wikilink")，但是他為了抗議[美國政府對待](../Page/美國政府.md "wikilink")[印第安人不公而拒絕領獎](../Page/印第安人.md "wikilink")。

派拉蒙最初希望[勞勃·瑞福或](../Page/勞勃·瑞福.md "wikilink")[雷恩·歐尼爾擔任](../Page/雷恩·歐尼爾.md "wikilink")[麥可·柯里昂的角色](../Page/麥可·柯里昂.md "wikilink")，但是柯波拉想找個看起來像義大利人的美國人擔任此角，也因此[艾爾·帕西諾獲得了柯波拉的青睞](../Page/艾爾·帕西諾.md "wikilink")\[7\]。帕西諾在此時並不出名，他在先前只演過兩部小影片，也因此派拉蒙影業並不認為他適合這個角色，而主因更是因為帕西諾的身高並不高（5呎7吋，約170公分）\[8\]。後來在柯波拉威脅退出劇組的情況下，帕西諾才得以獲得演出角色的機會。其他如[傑克·尼克遜](../Page/傑克·尼克遜.md "wikilink")、[達斯汀·霍夫曼](../Page/達斯汀·霍夫曼.md "wikilink")、[華倫·比提](../Page/華倫·比提.md "wikilink")、[馬丁·辛](../Page/馬丁·辛.md "wikilink")、[詹姆斯·肯恩都有參加試鏡](../Page/詹姆斯·肯恩.md "wikilink")\[9\]。[艾維斯·普里斯萊](../Page/艾維斯·普里斯萊.md "wikilink")（貓王）對麥可這角色也有興趣，但是並沒有參加試鏡。

其他角色甄選的部分，[布魯斯·鄧](../Page/布魯斯·鄧.md "wikilink")、[保羅·紐曼](../Page/保羅·紐曼.md "wikilink")、[史提夫·麥昆曾被認為是適合湯姆](../Page/史提夫·麥昆.md "wikilink")·哈金的角色，但是最終由[勞勃·杜瓦擔任](../Page/勞勃·杜瓦.md "wikilink")；[席維斯·史特龍曾徵選卡洛](../Page/席維斯·史特龍.md "wikilink")·瑞茲和保利·蓋托（*Paulie
Gatto*）的角色；[安東尼·柏金斯則曾爭取桑尼的角色](../Page/安東尼·柏金斯.md "wikilink")；[米亞·法羅則參加凱伊角色的徵選](../Page/米亞·法羅.md "wikilink")。[威廉·迪凡曾被視為是莫](../Page/威廉·迪凡.md "wikilink")·格林的人選；[馬里奧·阿多夫也曾爭取一個角色的演出](../Page/馬里奧·阿多夫.md "wikilink")。在當時不甚出名的[勞勃·狄·尼洛](../Page/勞勃·狄·尼洛.md "wikilink")，也曾參加麥可、桑尼、卡洛、保利的角色試鏡，他原本被選為保利的人選，但柯波拉與將他和《The
Gang That Couldn't Shoot
Straight》中的艾爾·帕西諾交換，而狄·尼洛在《[教父2](../Page/教父2.md "wikilink")》演出年輕的維托·柯里昂，藉由此角獲得[奧斯卡最佳男配角獎](../Page/奧斯卡最佳男配角獎.md "wikilink")。

在另一範圍上，《教父》與柯波拉家族也有相當的淵源。柯波拉的父親卡明·柯波拉，一生中曾擔任指揮家、作曲家，也為本片寫了一些配樂，甚至在片中擔任鋼琴師，而卡明的妻子也曾擔任片中的臨時演員。柯波拉的妹妹[泰莉亞·雪爾飾演康妮](../Page/泰莉亞·雪爾.md "wikilink")·柯里昂，而柯波拉的嬰兒女兒[蘇菲亞·柯波拉甚至飾演康妮的新生兒子](../Page/蘇菲亞·柯波拉.md "wikilink")\[10\]；演員大衛馬密說：「當我知道站在我面前的蘇菲亞，是二十年前第一集中的小嬰兒，我突然有一種毛骨悚然之感」。柯波拉也讓他的兒子們飾演湯姆·哈金的兩個兒子，分別出現在桑尼揍卡洛與葬禮的鏡頭中。

### 拍攝

大多數的拍攝行程主要是1971年3月29日至同年的8月6日，雖然有一幕帕西諾與基頓的戲是在秋天拍攝，但是總計77天的拍攝日還是少於公司原先預估的83日。

拍攝地點\[11\]主要是在紐約以及其他的地方，紐約第五大道上已經關閉的「Best &
Company」被裝成帕西諾與基頓聖誕節購物的商店。[洛杉磯則不只一個地方被拍攝](../Page/洛杉磯.md "wikilink")（主要是好萊塢製作人傑克·華爾斯的戲），其中有一幕帕西諾與基頓則是在[加州](../Page/加州.md "wikilink")[羅斯拍攝](../Page/羅斯_\(加利福尼亞州\).md "wikilink")。另外的外景包括拍攝西西里風景的Savoca與Forza
d'Agrò小村莊。內景則是在紐約的Filmways影業攝影棚拍攝。

其中一個電影最早的震驚畫面，也就是杰克·華爾斯的種馬馬頭被放置在床時，引來了動物保育人士的抨擊。柯波拉後來澄清片中的馬頭來自於一家狗食公司，劇組並沒有特地宰殺馬來拍攝影片。這段鏡頭則是在紐約華盛頓港拍攝\[12\]\[13\]。

小說中，杰克·華爾斯是一個有[戀童癖的老人](../Page/戀童癖.md "wikilink")，是藉由湯姆走出華爾斯的房間時，看到一名年輕女孩哭而發覺的。這段鏡頭電影被剪下，但是DVD中有收錄。

《教父》電影開始的鏡頭，也就是委託人包納薩拉（*Bonasera*）向教父請求委託的鏡頭，其中使用了連續變焦攝影鏡頭。這段維持約3分鐘的特別鏡頭，是由湯尼·卡普（*Tony
Karp*）設計的電腦控制連續變焦攝影鏡頭所拍攝的\[14\]。

### 配樂爭議

《教父》配樂師[尼諾·羅塔的配樂在當年最初雖被提名](../Page/尼諾·羅塔.md "wikilink")[奧斯卡最佳原創配樂獎](../Page/奧斯卡最佳原創配樂獎.md "wikilink")，但是最後卻被取消資格。原因在於羅塔曾使用他在[艾杜亞多·德·菲利浦的喜劇作品](../Page/艾杜亞多·德·菲利浦.md "wikilink")《Fortunella》所寫的主題曲作為《教父》的主題曲，即使在那部喜劇中的主題曲是輕快斷奏的，但是旋律與教父的主題曲無異，也因此《教父》的配樂無法獲得奧斯卡的肯定。怪異的是，1974年的《[教父2](../Page/教父2.md "wikilink")》同樣採取《教父》的主題配樂，但是卻獲得了當年的最佳配樂獎。

## 評價

《教父》一片一直廣獲全球影評的讚賞以及大眾公認的影史經典，被認為可能是史上最棒的電影。美國影評網站[爛番茄根据](../Page/爛番茄.md "wikilink")84篇評論得出99%的新鮮度。《娛樂週刊》的投票中，《教父》同樣被認為是影史最棒的影片。《教父》目前還是[AFI百年百大系列最新版本的亞軍](../Page/AFI百年百大系列.md "wikilink")，僅次於《[大國民](../Page/大國民.md "wikilink")》後\[15\]，最初的版本則為季軍，位於《[北非諜影](../Page/北非諜影.md "wikilink")》後。而在2008年6月，AFI又票選了美國影史10種類型電影的前10名，《教父》一片經由1500名以上的成員票選後，成為[幫派電影的冠軍](../Page/幫派電影.md "wikilink")\[16\]。

2002年，英國雜誌《Sight &
Sound》舉辦的國際影評投票中，《教父》獲選為影史第四棒影片。《教父》與《[教父2](../Page/教父2.md "wikilink")》同樣被美國[國家影片登記部典藏](../Page/國家影片登記部.md "wikilink")，不過教父三部曲的最終曲《[教父3](../Page/教父3.md "wikilink")》並未被收錄。

配樂方面，尼諾·羅塔所創的主題曲─（Speak Softly Love）同樣也廣為人讚賞，並且在後世常為人所使用。

《教父》同樣在票房上也取得成功，打破當時其他影片的票房紀錄。開幕首週，《教父》有$5,264,402的票房收入，並且在下映後一共賺進$81,500,000的收入\[17\]，是其預算及行銷活動費用的14倍之多。後來在重新上映後，票房收入提高到1億3400萬美元。

名導演[史丹利·庫柏力克認為](../Page/史丹利·庫柏力克.md "wikilink")《教父》可能是影史最棒的影片，而其陣容也無庸置疑的出色\[18\]。

## 獎項

| 奧斯卡金像獎                                                                                         |
| ---------------------------------------------------------------------------------------------- |
| **1.最佳男主角**─[馬龍·白蘭度](../Page/馬龍·白蘭度.md "wikilink")                                             |
| **2.最佳影片**─[艾伯特·魯迪](../Page/艾伯特·魯迪.md "wikilink")                                              |
| **3.最佳改編劇本**─[馬里奧·普佐](../Page/馬里奧·普佐.md "wikilink")、[法蘭西斯·柯波拉](../Page/法蘭西斯·柯波拉.md "wikilink") |
| 金球獎                                                                                            |
| **1.最佳戲劇類影片**                                                                                  |
| **2.最佳導演**─[法蘭西斯·柯波拉](../Page/法蘭西斯·柯波拉.md "wikilink")                                          |
| **3.最佳戲劇類男主角**─馬龍·白蘭度                                                                          |
| **4.最佳原創配樂**─[尼諾·羅塔](../Page/尼諾·羅塔.md "wikilink")                                              |
| **5.最佳劇本**─馬里歐·普佐、法蘭西斯·柯波拉                                                                     |
|                                                                                                |
| 英國電影學院獎                                                                                        |
| **1.最佳原創配樂**─尼諾·羅塔                                                                             |

《教父》亦在當年[奧斯卡金像獎上大放異彩](../Page/第45屆奧斯卡金像獎.md "wikilink")，獲得11項提名後拿回[最佳影片](../Page/奧斯卡最佳影片獎.md "wikilink")、[最佳男主角獎與](../Page/奧斯卡最佳男主角獎.md "wikilink")[最佳改編劇本獎](../Page/奧斯卡最佳改編劇本獎.md "wikilink")。其他的八項提名包括[詹姆斯·肯恩](../Page/詹姆斯·肯恩.md "wikilink")、[勞勃·杜瓦](../Page/勞勃·杜瓦.md "wikilink")、[艾爾·帕西諾分別獲得](../Page/艾爾·帕西諾.md "wikilink")[最佳男配角獎提名](../Page/奧斯卡最佳男配角獎.md "wikilink")、最佳導演、最佳服裝設計、最佳剪輯、最佳原創配樂、最佳混音。另外本片同樣也獲得了5項[金球獎](../Page/金球獎.md "wikilink")、1項[葛萊美獎及其他大大小小的獎項](../Page/葛萊美獎.md "wikilink")。

## 發行

《教父》於1972年3月15日於[美國上映](../Page/美國.md "wikilink")，海外地區上映時間如下：

| 國家／地區 | 上映日期                                          |
| ----- | --------------------------------------------- |
| ****  | 1972年7月15日                                    |
| ****  | 1972年8月24日                                    |
| ****  | 1972年9月14日                                    |
| ****  | 1972年9月20日                                    |
| ****  | 1972年9月27日                                    |
| ****  | 1972年9月29日                                    |
| ****  | 1972年10月16日（[奧斯陸](../Page/奧斯陸.md "wikilink")） |
| ****  | 1972年10月18日                                   |
| ****  | 1972年11月2日                                    |
| ****  | 1973年1月18日                                    |
| ****  | 1973年10月11日                                   |
|       |                                               |

### 各地电影分级制度

<table>
<thead>
<tr class="header">
<th><p>國家／地區</p></th>
<th><p>級別</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><p>PG</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>PG</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>15</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>PG</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>12</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_British_Columbia.svg" title="fig:Flag_of_British_Columbia.svg">Flag_of_British_Columbia.svg</a> <a href="../Page/加拿大.md" title="wikilink">加拿大</a><a href="../Page/不列颠哥伦比亚.md" title="wikilink">不列颠哥伦比亚</a></p></td>
<td><p>11</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Alberta.svg" title="fig:Flag_of_Alberta.svg">Flag_of_Alberta.svg</a>加拿大<a href="../Page/艾伯塔.md" title="wikilink">艾伯塔</a></p></td>
<td><p>12+</p></td>
</tr>
<tr class="even">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Manitoba.svg" title="fig:Flag_of_Manitoba.svg">Flag_of_Manitoba.svg</a>加拿大<a href="../Page/曼尼托巴.md" title="wikilink">曼尼托巴</a></p></td>
<td><p>PA</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Ontario.svg" title="fig:Flag_of_Ontario.svg">Flag_of_Ontario.svg</a>加拿大<a href="../Page/安大略.md" title="wikilink">安大略</a></p></td>
<td><p>AA</p></td>
</tr>
<tr class="even">
<td><p>(大西洋沿岸省份)</p></td>
<td><p>15</p></td>
</tr>
<tr class="odd">
<td><p><a href="https://zh.wikipedia.org/wiki/File:Flag_of_Quebec.svg" title="fig:Flag_of_Quebec.svg">Flag_of_Quebec.svg</a>加拿大<a href="../Page/魁北克.md" title="wikilink">魁北克</a></p></td>
<td><p>13+</p></td>
</tr>
<tr class="even">
<td><p>(Home Video)</p></td>
<td><p>18</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>18</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>18</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>15</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>K-15</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>-12</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>16</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>IIB</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>16</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>11</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>PG</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>M14</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>R-12</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>18</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>C</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>9+</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>R18</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>15</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>15</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>PG</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>M/18</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>M18</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>15</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>13</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>15</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>PG</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>18</p></td>
</tr>
<tr class="odd">
<td></td>
<td><p>R</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
</tr>
</tbody>
</table>

### DVD

2008年9月23日，[派拉蒙影業將會發行](../Page/派拉蒙影業.md "wikilink")《教父》系列三部曲完整收藏版（*The
Godfather: The Coppola Restoration Collection*）的[Blu-ray
Disc與DVD](../Page/Blu-ray_Disc.md "wikilink")，這次最新的版本將會收錄先前發行DVD的內容以及其他更完整的花絮。Blu-ray
Disc的版本將會有4片光碟，DVD版本則會有5片。

幕後花絮除了收錄先前發行的DVD版本內原先有的外，其他還有一連串的額外內容，將會在全新的版本中呈現：

  - Godfather World
    </li>
  - The Masterpiece That Almost Wasn't
  - …when the shooting stopped
  - Emulsional Rescue Revealing The Godfather
  - The Godfather on the Red Carpet

<!-- end list -->

  - Four Short Films on The Godfather
  - The Godfather vs. The Godfather, Part II
  - Cannoli
  - Riffing on the Riffing
  - Clemenza

《教父》最初是在2001年10月9日所發行的教父三部曲典藏版DVD內收錄，其中包括三部曲以及導演[法蘭西斯·柯波拉的訪談](../Page/法蘭西斯·柯波拉.md "wikilink")、幕後花絮（*The
Godfather Family: A Look Inside*）、紀錄片、刪除片段等。其他也包括（*Francis Coppola's
Notebook*），內容描述著柯波拉拍攝期間隨身攜帶筆記本的內容；攝影鏡頭的掌控、尼諾·羅塔與卡明·柯波拉的配樂。或者是影片內柯里昂家族的谱系、時間走向，甚至是奧斯卡獎上的得獎感言。

### 原聲帶

《教父》的配樂由[尼諾·羅塔負責主要配樂](../Page/尼諾·羅塔.md "wikilink")，[卡明·柯波拉則輔助配樂的工作](../Page/卡明·柯波拉.md "wikilink")。《教父》的配樂一直廣為人讚賞與喜愛，主題曲（The
Godfather Waltz）與配樂（Love Theme From The
Godfather）都是家喻戶曉的經典名曲，後世一直廣為使用與流傳。

#### 曲目

1.  （Main Title）– 3:04
2.  （I Have But One Heart）– 2:57
3.  （The Pickup）– 2:56
4.  （Connie's Wedding）（[卡明·柯波拉](../Page/卡明·柯波拉.md "wikilink")）– 1:33
5.  （The Halls Of Fear）– 2:12
6.  （Sicilian Pastorale）– 3:01
7.  （Love Theme From The Godfather）（*Speak Softly Love*）– 2:41
8.  （The Godfather Waltz）– 3:38
9.  （Apollonia）– 1:21
10. （The New Godfather）– 1:58
11. （The Baptism）– 1:49
12. （The Godfather Finale）– 3:50

### 電玩遊戲

2006年，[美商藝電首先發布了教父的遊戲版本](../Page/美商藝電.md "wikilink")。[馬龍·白蘭度在去世之前的配音因為與過去落差太大](../Page/馬龍·白蘭度.md "wikilink")，改由另一個相似聲音取代。其他演員如[詹姆斯·肯恩](../Page/詹姆斯·肯恩.md "wikilink")、[勞勃·杜瓦](../Page/勞勃·杜瓦.md "wikilink")、[艾巴·維戈達等都有配音](../Page/艾巴·維戈達.md "wikilink")，唯[艾爾·帕西諾並未配音](../Page/艾爾·帕西諾.md "wikilink")，原因在於帕西諾已經將他的聲音使用權賣給另一款遊戲。

## 參考資料

## 外部連結

  - 《[教父](http://www.filmsite.org/godf.html)》在Filmsite.org

  - [Godfather Films](http://www.GodfatherFilms.com/)

  - [The Godfather Trilogy](http://www.TheGodfatherTrilogy.com/)

  - {{@movies|fGatm0632001|教父I}}

  -
  -
  -
  -
  -
  -
  -
  -
[Category:1972年電影](../Category/1972年電影.md "wikilink")
[Category:1970年代剧情片](../Category/1970年代剧情片.md "wikilink")
[Category:美國電影作品](../Category/美國電影作品.md "wikilink")
[Category:英語電影](../Category/英語電影.md "wikilink")
[Category:義大利語電影](../Category/義大利語電影.md "wikilink")
[Category:弗朗西斯·科波拉电影](../Category/弗朗西斯·科波拉电影.md "wikilink")
[Category:教父系列](../Category/教父系列.md "wikilink")
[Category:美国小说改编电影](../Category/美国小说改编电影.md "wikilink")
[Category:美國劇情犯罪片](../Category/美國劇情犯罪片.md "wikilink")
[Category:黑幫電影](../Category/黑幫電影.md "wikilink")
[Category:移民题材电影](../Category/移民题材电影.md "wikilink")
[Category:黑手党](../Category/黑手党.md "wikilink")
[Category:纽约市背景电影](../Category/纽约市背景电影.md "wikilink")
[Category:西西里島背景電影](../Category/西西里島背景電影.md "wikilink")
[Category:拉斯維加斯背景電影](../Category/拉斯維加斯背景電影.md "wikilink")
[Category:1940年代背景電影](../Category/1940年代背景電影.md "wikilink")
[Category:1950年代背景電影](../Category/1950年代背景電影.md "wikilink")
[Category:紐約市取景電影](../Category/紐約市取景電影.md "wikilink")
[Category:加利福尼亚州取景电影](../Category/加利福尼亚州取景电影.md "wikilink")
[Category:派拉蒙影業電影](../Category/派拉蒙影業電影.md "wikilink")
[Category:奧斯卡最佳影片](../Category/奧斯卡最佳影片.md "wikilink")
[Category:奧斯卡最佳男主角獲獎電影](../Category/奧斯卡最佳男主角獲獎電影.md "wikilink")
[Category:奧斯卡最佳改編劇本獲獎電影](../Category/奧斯卡最佳改編劇本獲獎電影.md "wikilink")
[Category:金球獎最佳劇情片](../Category/金球獎最佳劇情片.md "wikilink")
[Category:金球奖最佳导演获奖电影](../Category/金球奖最佳导演获奖电影.md "wikilink")
[Category:金球獎最佳戲劇類男主角獲獎電影](../Category/金球獎最佳戲劇類男主角獲獎電影.md "wikilink")
[Category:美國國家電影保護局典藏](../Category/美國國家電影保護局典藏.md "wikilink")

1.  《教父》、《[教父2](../Page/教父2.md "wikilink")》、《[教父3](../Page/教父3.md "wikilink")》

2.

3.  《The Kid Stays in the Picture》．（2002年）．艾文生涯紀錄片．

4.

5.

6.  《教父》DVD收藏記事《一窺內幕》（*A Look Inside*）

7.  《教父》DVD收藏記事─法蘭西斯·柯波拉

8.
9.
10. [蘇菲亞·柯波拉曾在](../Page/蘇菲亞·柯波拉.md "wikilink")《教父2》飾演一名無名的移民女孩，帶領維托到紐約。她在《教父3》則擔綱重任，飾演麥可的女兒瑪莉·柯里昂。

11. [教父：拍攝場景](http://thegodfathertrilogy.com/gf1/gf1scene.html)

12.
13.
14. ﹒["Doing the impossible - Part 1 - The Godfather" - - Art and the
    Zen of Design](http://artzen2.com/artzen2-0027.htm)

15. ．美國電影學會．["Citizen Kane Stands the test of
    Time"](http://www.afi.com/Docs/about/press/2007/100movies07.pdf)

16.

17.

18. ．[Michael Herr for Vanity
    Fair](http://www.visual-memory.co.uk/sk/memories/mh.htm)