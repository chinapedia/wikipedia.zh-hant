**柯克·道格拉斯**（，），美國[好萊塢電影演員](../Page/好萊塢.md "wikilink")，猶太人\[1\]。寇克曾1950年代三次入圍[奧斯卡金像獎最佳男主角皆未得獎](../Page/奧斯卡金像獎.md "wikilink")，近年1996年獲頒[奧斯卡終身成就獎](../Page/奧斯卡終身成就獎.md "wikilink")，2001年也獲得第51屆[柏林影展終身成就獎](../Page/柏林影展.md "wikilink")，獲獎原因是「表現出義勇先鋒形象及美國式[英雄主義](../Page/英雄主義.md "wikilink")」；實至名歸。1999年，他被美國電影學會選為[百年來最偉大的男演員第](../Page/百年百大明星.md "wikilink")17名。

## 早年境況

寇克·道格拉斯出生於美國紐約\[2\]。他的父母是[猶太人](../Page/猶太人.md "wikilink")\[3\]\[4\]\[5\]\[6\]。他的表演天賦在紐約的美國戲劇藝術學院被注意到。

## 家庭

寇克·道格拉斯曾結過兩次婚：第一任妻子是Diana Douglas（原名是Diana Love
Dill），兩人於1943年結婚，1951年離異，育有二名兒子，包括亦是著名演員的[麥克·道格拉斯](../Page/麥克·道格拉斯.md "wikilink")；第二任妻子是Anne
Buydens，兩人於1954年結婚，育有二子\[7\]。

## 作品

| 年份                                                                              | 影片                                                                                  | 角色                                                               | 備註                                                        |
| ------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------- | ---------------------------------------------------------------- | --------------------------------------------------------- |
| 譯名                                                                              | 原名                                                                                  |                                                                  |                                                           |
| [1946](../Page/1946年電影.md "wikilink")                                           | 《[玛莎·依瓦丝的爱](../Page/玛莎·依瓦丝的爱.md "wikilink")》                                        | *The Strange Love of Martha Ivers*                               | Walter O'Neil                                             |
| [1947](../Page/1947年電影.md "wikilink")                                           | 《[漩涡之外](../Page/漩涡之外.md "wikilink")》                                                | *Out of the Past*                                                | Whit Sterling                                             |
| 《[埃里特拉戴孝](../Page/埃里特拉戴孝.md "wikilink")》                                        | *Mourning Becomes Electra*                                                          | Peter Niles                                                      |                                                           |
| [1948](../Page/1948年電影.md "wikilink")                                           |                                                                                     | *My Dear Secretary*                                              | Owen Waterbury                                            |
| 《[天涯独行](../Page/天涯独行.md "wikilink")》                                            | *I Walk Alone*                                                                      | Noll 'Dink' Turner                                               |                                                           |
| [1949](../Page/1949年電影.md "wikilink")                                           | 《[夺得锦标归](../Page/夺得锦标归.md "wikilink")》                                              | *Champion*                                                       | Michael 'Midge' Kelly                                     |
| 《[三妻艳史](../Page/三妻艳史.md "wikilink")》                                            | *A Letter to Three Wives*                                                           | George Phipps                                                    |                                                           |
| [1950](../Page/1950年電影.md "wikilink")                                           | 《[雙凰奪鸞](../Page/雙凰奪鸞.md "wikilink")》                                                | *Young Man with a Horn*                                          | Rick Martin                                               |
| 《[荆钗恨](../Page/荆钗恨.md "wikilink")》                                              | *The Glass Menagerie*                                                               | Jim O'Connor                                                     |                                                           |
| [1951](../Page/1951年電影.md "wikilink")                                           | 《[侦探故事](../Page/侦探故事.md "wikilink")》                                                | *Detective Story*                                                | Det. James 'Jim' McLeod                                   |
| 《[盛大的狂欢节](../Page/盛大的狂欢节.md "wikilink")》                                        | *Ace in the Hole*                                                                   | Charles 'Chuck' Tatum                                            |                                                           |
| 《[木兰远征](../Page/木兰远征.md "wikilink")》                                            | *Along the Great Divide*                                                            | Marshal Len Merrick                                              |                                                           |
| [1952](../Page/1952年電影.md "wikilink")                                           | 《[大树岭恩仇记](../Page/大树岭恩仇记.md "wikilink")》                                            | *The Big Trees*                                                  | Jim Fallon                                                |
| 《[峰火弥天](../Page/峰火弥天.md "wikilink")》                                            | *The Big Sky*                                                                       | Jim Deakins                                                      |                                                           |
| 《[玉女奇遇](../Page/玉女奇遇.md "wikilink")》                                            | *The Bad and the Beautiful*                                                         | Jonathan Shields                                                 | 提名 - [奧斯卡最佳男主角獎](../Page/奧斯卡最佳男主角獎.md "wikilink")         |
| [1953](../Page/1953年電影.md "wikilink")                                           | 《[风尘知已](../Page/风尘知已.md "wikilink")》/《[天涯知己](../Page/天涯知己.md "wikilink")》           | *The Juggler*                                                    | Hans Muller                                               |
| 《[爱情三部曲](../Page/爱情三部曲.md "wikilink")》                                          | *The Story of Three Loves*                                                          | Pierre Narval                                                    | 片段"Equilibrium"                                           |
| 《[此恨绵绵](../Page/此恨绵绵.md "wikilink")》                                            | *Un acte d'amour*                                                                   | Robert Teller                                                    |                                                           |
| [1954](../Page/1954年電影.md "wikilink")                                           | 《[海底二万里](../Page/海底二万里_\(电影\).md "wikilink")》                                       | *20,000 Leagues Under the Sea*                                   | Ned Land                                                  |
| [1955](../Page/1955年電影.md "wikilink")                                           | 《[飞车壮史万里追踪](../Page/飞车壮史万里追踪.md "wikilink")》                                        | *The Racers*                                                     | Gino Borgesa                                              |
| 《[霸王伏妖](../Page/霸王伏妖.md "wikilink")》                                            | *Ulisse*                                                                            | Ulysses                                                          |                                                           |
| 《[印第安勇士](../Page/印第安勇士.md "wikilink")》                                          | *The Indian Fighter*                                                                | Johnny Hawks                                                     |                                                           |
| 《[男子汉大丈夫](../Page/男子汉大丈夫.md "wikilink")》                                        | *Man Without a Star*                                                                | Dempsey Rae                                                      |                                                           |
| [1956](../Page/1956年電影.md "wikilink")                                           | 《[梵谷传](../Page/梵谷传.md "wikilink")》                                                  | *Lust for Life*                                                  | [文森特·梵高](../Page/文森特·梵高.md "wikilink")                    |
| [1957](../Page/1957年電影.md "wikilink")                                           | 《[光榮之路](../Page/光榮之路_\(1957年電影\).md "wikilink")》                                    | *Paths of Glory*                                                 | Col. Dax                                                  |
| 《[OK镇大决斗](../Page/OK镇大决斗.md "wikilink")》                                        | *Gunfight at the O.K. Corral*                                                       | Dr. John 'Doc' Holliday                                          |                                                           |
| [1958](../Page/1958年電影.md "wikilink")                                           | 《[海盜](../Page/海盜_\(電影\).md "wikilink")》                                             | *The Vikings*                                                    | Einar                                                     |
| [1959](../Page/1959年電影.md "wikilink")                                           | 《[魔鬼门徒](../Page/魔鬼门徒.md "wikilink")》                                                | *The Devil's Disciple*                                           | Richard 'Dick' Dudgeon                                    |
| 《[岗山最后列车](../Page/岗山最后列车.md "wikilink")》                                        | *Last Train from Gun Hill*                                                          | Marshal Matt Morgan                                              |                                                           |
| [1960](../Page/1960年電影.md "wikilink")                                           | 《[萬夫莫敵](../Page/萬夫莫敵.md "wikilink")》                                                | *Spartacus*                                                      | \-{[斯巴达克斯](../Page/斯巴达克斯.md "wikilink")}-                 |
| 《[相逢何必曾相識](../Page/相逢何必曾相識_\(1960年電影\).md "wikilink")》                          | *Strangers When We Meet*                                                            | Larry Coe                                                        |                                                           |
| [1961](../Page/1961年電影.md "wikilink")                                           |                                                                                     | *The Last Sunset*                                                | Brendan O'Malley                                          |
| 《[暴雨狂云](../Page/暴雨狂云.md "wikilink")》                                            | *Town Without Pity*                                                                 | Maj. Steve Garrett                                               |                                                           |
| [1962](../Page/1962年電影.md "wikilink")                                           | 《[罗马之光](../Page/罗马之光.md "wikilink")》                                                | *Two Weeks in Another Town*                                      | Jack Andrus                                               |
| 《[自古英雄多寂寞](../Page/自古英雄多寂寞.md "wikilink")》                                      | *Lonely Are the Brave*                                                              | Jack Burns/John W. Burns                                         | 提名 - [英國電影學院獎最佳男主角](../Page/英國電影學院獎.md "wikilink")        |
| [1963](../Page/1963年電影.md "wikilink")                                           |                                                                                     | The Hook                                                         | Sgt. P.J. Briscoe                                         |
| 《[假面凶手](../Page/假面凶手.md "wikilink")》                                            | *The List of Adrian Messenger*                                                      | George Brougham/Vicar Atlee/Mr. Pythian/Arthur Henderson         |                                                           |
| [1964](../Page/1964年電影.md "wikilink")                                           | 《[五月中的七天](../Page/五月中的七天.md "wikilink")》                                            | *Seven Days in May*                                              | Col. Martin 'Jiggs' Casey                                 |
| [1965](../Page/1965年電影.md "wikilink")                                           | 《[海上長城](../Page/海上長城.md "wikilink")》                                                | *In Harm's Way*                                                  | Cmdr./Capt. Paul Eddington, Adm. Torrey's Chief of Staff  |
| 《[铁勒马九壮士](../Page/铁勒马九壮士.md "wikilink")》                                        | *The Heroes of Telemark*                                                            | Dr. Rolf Pedersen                                                |                                                           |
| [1966](../Page/1966年電影.md "wikilink")                                           | 《[巴黎战火](../Page/巴黎战火.md "wikilink")》                                                | *Paris brûle-t-il?*                                              | Gen. George S. Patton Jr.                                 |
| 《[血肉长城](../Page/血肉长城.md "wikilink")》                                            | *Cast a Giant Shadow*                                                               | Col. David 'Mickey' Marcus                                       |                                                           |
| [1967](../Page/1967年電影.md "wikilink")                                           | 《[西部新天地](../Page/西部新天地.md "wikilink")》                                              | *The Way West*                                                   | Sen. William J. Tadlock (Captain of Oregon Liberty Train) |
| 《[戰車](../Page/戰車_\(電影\).md "wikilink")》                                         | *The War Wagon*                                                                     | Lomax                                                            |                                                           |
| [1968](../Page/1968年電影.md "wikilink")                                           | 《[牡丹花血案](../Page/牡丹花血案.md "wikilink")》                                              | *A Lovely Way to Die*                                            | Jim Schuyler                                              |
| [1969](../Page/1969年電影.md "wikilink")                                           | 《[天倫刼](../Page/天倫刼.md "wikilink")／[枭城喋血](../Page/枭城喋血.md "wikilink")》               | *The Brotherhood*                                                | Frank Ginetta                                             |
| 《[我就爱你](../Page/我就爱你.md "wikilink")》/《[稱心如意巧安排](../Page/稱心如意巧安排.md "wikilink")》 | *The Arrangement*                                                                   | Eddie Anderson                                                   |                                                           |
| [1970](../Page/1970年電影.md "wikilink")                                           | 《[大逃獄](../Page/大逃獄.md "wikilink")》                                                  | *There Was a Crooked Man...*                                     | Paris Pittman Jr.                                         |
| [1971](../Page/1971年電影.md "wikilink")                                           | 《[天边的灯光](../Page/天边的灯光.md "wikilink")》                                              | *The Light at the Edge of the World*                             | Will Denton                                               |
| 《[枪战](../Page/枪战.md "wikilink")》                                                | *A Gunfight*                                                                        | Will Tenneray                                                    |                                                           |
| [1972](../Page/1972年電影.md "wikilink")                                           | 《[賊王](../Page/賊王.md "wikilink")》                                                    | *Un Uomo da rispettare*                                          | Steve Wallace                                             |
| [1975](../Page/1975年電影.md "wikilink")                                           | 《[深情](../Page/深情_\(電影\).md "wikilink")》                                             | *Jacqueline Susann's Once Is Not Enough*                         | Mike Wayne                                                |
| 《[大鹰啸](../Page/大鹰啸.md "wikilink")》                                              | *Posse*                                                                             | Howard Nightingale                                               | 兼任監製                                                      |
| [1976](../Page/1976年電影.md "wikilink")                                           | 《[恩德培的胜利](../Page/恩德培的胜利.md "wikilink")》/《[突襲烏干達機場](../Page/突襲烏干達機場.md "wikilink")》 | *Victory at Entebbe*                                             | Hershel Vilnofsky                                         |
|                                                                                 | *Arthur Hailey's the Moneychangers*                                                 | Alex Vandervoort                                                 |                                                           |
| [1977](../Page/1977年電影.md "wikilink")                                           |                                                                                     | *Holocaust 2000*                                                 | Robert Caine                                              |
| [1977](../Page/1977年電影.md "wikilink")                                           | 《[憤怒](../Page/憤怒_\(電影\).md "wikilink")》                                             | The Fury                                                         | Peter Sandza                                              |
| [1979](../Page/1979年電影.md "wikilink")                                           | 《[疯狂大镖客](../Page/疯狂大镖客.md "wikilink")》                                              | *The Villain*                                                    | Cactus Jack                                               |
|                                                                                 | *Home Movies*                                                                       | Doctor Tuttle 'The Maestro'                                      |                                                           |
| [1980](../Page/1980年電影.md "wikilink")                                           | 《[碧血長天](../Page/碧血長天_\(1980年電影\).md "wikilink")》                                    | Final Countdown, The                                             | Capt. Matthew Yelland                                     |
| 《[土星三号](../Page/土星三号.md "wikilink")》                                            | *Saturn 3*                                                                          | Adam                                                             |                                                           |
| [1982](../Page/1982年電影.md "wikilink")                                           | 《[来自雪河的人](../Page/来自雪河的人.md "wikilink")》                                            | *The Man from Snowy River*                                       | Harrison/Spur                                             |
| [1983](../Page/1983年電影.md "wikilink")                                           | 《[奔向墨西哥](../Page/奔向墨西哥.md "wikilink")/[趕到盡](../Page/趕到盡.md "wikilink")》             | *Eddie Macon's Run*                                              | Carl 'Buster' Marzack                                     |
| [1984](../Page/1984年電影.md "wikilink")                                           |                                                                                     | *Draw\!*                                                         | Harry H. Holland aka Handsome Harry Holland               |
| [1985](../Page/1985年電影.md "wikilink")                                           |                                                                                     | *Amos*                                                           | Amos Lasher                                               |
| [1986](../Page/1986年電影.md "wikilink")                                           | 《[再上梁山](../Page/再上梁山.md "wikilink")》                                                | *Tough Guys*                                                     | Archie Long                                               |
| [1987](../Page/1987年電影.md "wikilink")                                           |                                                                                     | *Queenie*                                                        | David Konig                                               |
| [1988](../Page/1988年電影.md "wikilink")                                           | 《[新向上帝挑战](../Page/新向上帝挑战.md "wikilink")》                                            | *Inherit the Wind*                                               | Matthew Harrison Brady                                    |
| [1991](../Page/1991年電影.md "wikilink")                                           | 《[弹指威龙](../Page/弹指威龙.md "wikilink")／[龙哥，你好嘢](../Page/龙哥，你好嘢.md "wikilink")》         | *Oscar*                                                          | Eduardo Provolone                                         |
| 《[幽冥神话](../Page/幽冥神话.md "wikilink")》                                            | *Two-Fisted Tales*                                                                  | General                                                          | 片段"Yellow"                                                |
| [1992](../Page/1992年電影.md "wikilink")                                           | 《[秘密情事](../Page/秘密情事.md "wikilink")》                                                | *The Secret*                                                     | Grandpa Mike Dunmore                                      |
| [1994](../Page/1994年電影.md "wikilink")                                           | 《[世纪电影](../Page/世纪电影.md "wikilink")》                                                | *A Century of Cinema*                                            | 本人                                                        |
| 《[貪婪](../Page/貪婪_\(電影\).md "wikilink")》                                         | *Greedy*                                                                            | Uncle Joe McTeague                                               |                                                           |
| [1997](../Page/1997年電影.md "wikilink")                                           |                                                                                     | *Completely Cuckoo*                                              | 本人                                                        |
| [1999](../Page/1999年電影.md "wikilink")                                           | 《[鑽石](../Page/鑽石_\(電影\).md "wikilink")》                                             | *Diamonds*                                                       | Harry Agensky                                             |
| [2001](../Page/2001年電影.md "wikilink")                                           |                                                                                     | *Walt: The Man Behind the Myth*                                  | Ned Land                                                  |
|                                                                                 | *Pulp Cinema*                                                                       | 本人                                                               | (archive footage)                                         |
| 《[AFI百年百大驚悚電影](../Page/AFI百年百大驚悚電影.md "wikilink")》                              | AFI's 100 Years, 100 Thrills: America's Most Heart-Pounding Movies                  | 本人                                                               | archive footage                                           |
| [2002](../Page/2002年電影.md "wikilink")                                           |                                                                                     | *Kirk Douglas and Vincente Minnelli*                             | 本人                                                        |
|                                                                                 | *Darkness at High Noon: The Carl Foreman Documents*                                 |                                                                  |                                                           |
| 《[光影流情](../Page/光影流情.md "wikilink")》                                            | The Kid Stays In the Picture                                                        | Actor in Clip from The Brotherhood                               | (archive footage) (uncredited)                            |
| [2003](../Page/2003年電影.md "wikilink")                                           |                                                                                     | *Making of '20,000 Leagues Under the Sea*'                       | 本人                                                        |
| 《[AFI百年百大英雄與反派](../Page/AFI百年百大英雄與反派.md "wikilink")》                            | *AFI's 100 Years... 100 Heroes & Villains*                                          | 本人                                                               |                                                           |
|                                                                                 | *Anthony Quinn and Kirk Douglas*                                                    | 本人                                                               | (archive footage)                                         |
| 《[三代同堂](../Page/三代同堂.md "wikilink")》                                            | It Runs in the Family                                                               | Mitchell Gromberg                                                |                                                           |
| [2004](../Page/2004年電影.md "wikilink")                                           |                                                                                     | ''Les 40 ans de la 2                                             | 本人                                                        |
| 《[幻覺](../Page/幻覺_\(電影\).md "wikilink")》                                         | *Illusion*                                                                          | Donal Baines                                                     |                                                           |
| [2006](../Page/2006年電影.md "wikilink")                                           | 《[美国百年百部经典电影：励志篇](../Page/美国百年百部经典电影：励志篇.md "wikilink")》                            | *AFI's 100 Years... 100 Cheers: America's Most Inspiring Movies* | 本人                                                        |
|                                                                                 |                                                                                     |                                                                  |                                                           |

### 特別嘉賓

  - 《第5屆Annual Screen Actors Guild Awards 》 (1999)... 本人
  - 《[第75届奥斯卡金像奖颁奖典礼](../Page/第75届奥斯卡金像奖.md "wikilink")》 (2003) ... 本人 -
    Co-Presenter: Best Picture/Past winner
  - *Drawing First Blood* (2002) ... 特別鳴謝

### 監製

  - [1966](../Page/1966年電影.md "wikilink")：《[霹雳神风](../Page/霹雳神风.md "wikilink")》（*Grand
    Prix*）——執行監製

## 外部链接

  -
  -
  -
  -
  - [Profile](http://www.tcm.com/thismonth/article/?cid=161312) @
    [Turner Classic Movies](../Page/Turner_Classic_Movies.md "wikilink")

  - [Personal papers of Kirk Douglas at the Wisconsin Historical Society
    - Roughly 180,000 documents on Douglas's personal and professional
    life](https://web.archive.org/web/20071014184342/http://digicoll.library.wisc.edu/cgi-bin/ead-idx?c=shs&id=uw-whs-us0102an)

  - [Newcritics.com: Meeting Kirk
    Douglas](http://newcritics.com/blog1/2007/04/27/meeting-kirk-douglas/)

  - [Kirk Douglas, le champion de
    Hollywood](http://kirk.douglas.free.fr)

## 参考资料

[Category:金球獎最佳電影男主角獲得者](../Category/金球獎最佳電影男主角獲得者.md "wikilink")
[Category:美國電影男演員](../Category/美國電影男演員.md "wikilink")
[Category:美國電視男演員](../Category/美國電視男演員.md "wikilink")
[Category:西部片演員](../Category/西部片演員.md "wikilink")
[Category:美國猶太人](../Category/美國猶太人.md "wikilink")
[Category:好萊塢星光大道](../Category/好萊塢星光大道.md "wikilink")
[Category:奧斯卡終身成就獎獲得者](../Category/奧斯卡終身成就獎獲得者.md "wikilink")
[Category:犹太演员](../Category/犹太演员.md "wikilink")
[Category:美國人瑞](../Category/美國人瑞.md "wikilink")
[Category:奥斯卡荣誉奖获得者](../Category/奥斯卡荣誉奖获得者.md "wikilink")
[Category:20世纪美国男演员](../Category/20世纪美国男演员.md "wikilink")
[Category:美國電影導演](../Category/美國電影導演.md "wikilink")
[Category:美國第二次世界大戰軍事人物](../Category/美國第二次世界大戰軍事人物.md "wikilink")
[Category:圣劳伦斯大学校友](../Category/圣劳伦斯大学校友.md "wikilink")
[Category:總統自由勳章獲得者](../Category/總統自由勳章獲得者.md "wikilink")
[Category:美國國家藝術獎章獲得者](../Category/美國國家藝術獎章獲得者.md "wikilink")
[Category:美國電影監製](../Category/美國電影監製.md "wikilink")
[Category:21世紀美國男演員](../Category/21世紀美國男演員.md "wikilink")

1.
2.
3.
4.
5.
6.
7.