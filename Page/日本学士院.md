**日本学士院**（，）是[日本](../Page/日本.md "wikilink")[文部科学省所属的特别机关](../Page/文部科学省.md "wikilink")。该院是根据以优待学术上取得功绩显著的学者，促进学术的发达为目的《[日本学士院法](../Page/日本学士院法.md "wikilink")》第1条而设立的。对于日本的学者来说，成为日本学士院的会员是仅次于获得[文化勋章或者](../Page/文化勋章.md "wikilink")[文化功劳者的荣誉](../Page/文化功劳者.md "wikilink")。日本学士院設址于[東京都](../Page/東京都.md "wikilink")[台東区](../Page/台東区.md "wikilink")[上野恩赐公园内](../Page/上野恩赐公园.md "wikilink")。

学士院会员可以获得低于文化功劳者的薪水（上述法律第9条），这属于非常勤[国家公务员的待遇](../Page/国家公务员.md "wikilink")。会员的评选是根据各部分科会员的投票所进行的。会员为终身制（3条2项），名额为150名（2条2项）。

学士院既是荣誉机构又是研究机构。外国的科学院经常进行科学研究，与此相比，日本学士院主要对目前国内研究的成果进行评价和归纳，作为研究机构的色彩不是特别浓厚。该院也颁发“[日本学士院恩賜奖](../Page/日本学士院恩賜奖.md "wikilink")”，“[日本学士院奖](../Page/日本学士院奖.md "wikilink")”以及“[爱丁堡公爵奖](../Page/爱丁堡公爵奖.md "wikilink")”。爱丁堡公爵是日本学士院的名誉会员。

## 沿革

  - 1879年 - 東京学士会院设立（会員定额40名）。
  - 1906年 - 帝国学士院規程公布（会員定额60名）。
  - 1890年 - 東京学士会院規程公布。
  - 1911年 - 开设日本学士院恩賜賞。
  - 1911年 - 帝国学士院賞成立。
  - 1919年 - 加入（UAI）
  - 1947年 - 改称日本学士院。
  - 1949年 - 成为[日本学术会议的附属机构](../Page/日本学术会议.md "wikilink")（会員定额150名）。
  - 1956年 - 公布日本学士院法，独立于日本学术会议。
  - 1987年 - 开设爱丁堡公爵奖。

## 会员的构成

  - 第1部 - 人文科学・社会科学部門
      - 第1分科 -
        [文学](../Page/文学.md "wikilink")・[史学](../Page/史学.md "wikilink")・[哲学](../Page/哲学.md "wikilink")・[人类学](../Page/人类学.md "wikilink")・[语言学](../Page/语言学.md "wikilink")・[宗教学](../Page/宗教学.md "wikilink")（定额30）
      - 第2分科 -
        [法学](../Page/法学.md "wikilink")・[政治学](../Page/政治学.md "wikilink")（定额24）
      - 第3分科 -
        [经济学](../Page/经济学.md "wikilink")・[商学](../Page/商学.md "wikilink")（定额16）
  - 第2部 - 自然科学部門
      - 第4分科 -
        [理学](../Page/理学.md "wikilink")（[天文学](../Page/天文学.md "wikilink")・[物理学](../Page/物理学.md "wikilink")・[数学](../Page/数学.md "wikilink")・[地球科学](../Page/地球科学.md "wikilink")・[化学](../Page/化学.md "wikilink")・[生物学](../Page/生物学.md "wikilink")・[矿物学](../Page/矿物学.md "wikilink")）（定额31）
      - 第5分科 - [工学](../Page/工学.md "wikilink")（定额17）　　
      - 第6分科 - [农学](../Page/农学.md "wikilink")（定额12）
      - 第7分科 -
        [医学](../Page/医学.md "wikilink")・[药学](../Page/药学.md "wikilink")・[齿学](../Page/齿学.md "wikilink")（定额20）

### 現在的会員

2012年2月11日的\[1\]\[2\]

<table>
<thead>
<tr class="header">
<th><p>部门</p></th>
<th><p>分科<br />
定員上限</p></th>
<th><p>姓名</p></th>
<th><p>専业分类</p></th>
<th><p>選出年</p></th>
<th><p>参考</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>第1部<br />
<a href="../Page/人文科学.md" title="wikilink">人文科学部門</a></p></td>
<td><p>第1分科<br />
（<a href="../Page/文学.md" title="wikilink">文学</a>・<a href="../Page/史学.md" title="wikilink">史学</a>・<a href="../Page/哲学.md" title="wikilink">哲学</a>）<br />
30名</p></td>
<td><p><a href="../Page/久保正彰_(西洋古典文学者).md" title="wikilink">久保正彰</a></p></td>
<td><p><a href="../Page/西洋古典学.md" title="wikilink">西洋古典学</a></p></td>
<td><p>1992年</p></td>
<td><p>2007年任院長</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/中根千枝.md" title="wikilink">中根千枝</a></p></td>
<td><p><a href="../Page/文化人類学.md" title="wikilink">文化人類学</a></p></td>
<td><p>1995年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/中川久定.md" title="wikilink">中川久定</a></p></td>
<td><p><a href="../Page/法语文学.md" title="wikilink">法语文学</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/永井博_(哲学者).md" title="wikilink">永井博</a></p></td>
<td><p><a href="../Page/哲学.md" title="wikilink">哲学</a>・<a href="../Page/科学哲学.md" title="wikilink">科学哲学</a></p></td>
<td><p>1996年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/西田龍雄.md" title="wikilink">西田龍雄</a></p></td>
<td><p><a href="../Page/言語学.md" title="wikilink">言語学</a></p></td>
<td><p>1999年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/岩崎英二郎.md" title="wikilink">岩崎英二郎</a></p></td>
<td><p><a href="../Page/ドイツ語学.md" title="wikilink">ドイツ語学</a></p></td>
<td><p>2000年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/秋山虔.md" title="wikilink">秋山虔</a></p></td>
<td><p><a href="../Page/日本文学.md" title="wikilink">日本文学</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/原實.md" title="wikilink">原實</a></p></td>
<td><p><a href="../Page/インド古典学.md" title="wikilink">インド古典学</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/田仲一成.md" title="wikilink">田仲一成</a></p></td>
<td><p><a href="../Page/中国文学.md" title="wikilink">中国文学</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/源了圓.md" title="wikilink">源了圓</a></p></td>
<td><p><a href="../Page/日本思想史.md" title="wikilink">日本思想史</a></p></td>
<td><p>2001年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/荒井献.md" title="wikilink">荒井献</a></p></td>
<td><p><a href="../Page/新約聖書学.md" title="wikilink">新約聖書学</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/尾藤正英.md" title="wikilink">尾藤正英</a></p></td>
<td><p><a href="../Page/日本史.md" title="wikilink">日本史</a></p></td>
<td><p>2002年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/伊藤貞夫.md" title="wikilink">伊藤貞夫</a></p></td>
<td><p><a href="../Page/西洋史学.md" title="wikilink">西洋史学</a>（<a href="../Page/古典古代史.md" title="wikilink">古典古代史</a>）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/武田恒夫.md" title="wikilink">武田恒夫</a></p></td>
<td><p><a href="../Page/日本絵画史.md" title="wikilink">日本絵画史</a></p></td>
<td><p>2003年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/上田閑照.md" title="wikilink">上田閑照</a></p></td>
<td><p><a href="../Page/哲学.md" title="wikilink">哲学</a>・<a href="../Page/宗教哲学.md" title="wikilink">宗教哲学</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/斯波義信.md" title="wikilink">斯波義信</a></p></td>
<td><p><a href="../Page/中国史.md" title="wikilink">中国史</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/久保田淳.md" title="wikilink">久保田淳</a></p></td>
<td><p><a href="../Page/日本文学.md" title="wikilink">日本文学</a></p></td>
<td><p>2005年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/吉川忠夫.md" title="wikilink">吉川忠夫</a></p></td>
<td><p><a href="../Page/中国史.md" title="wikilink">中国史</a></p></td>
<td><p>2006年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/御牧克己.md" title="wikilink">御牧克己</a></p></td>
<td><p><a href="../Page/インド・チベット仏教学.md" title="wikilink">インド・チベット仏教学</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/難波精一郎.md" title="wikilink">難波精一郎</a></p></td>
<td><p><a href="../Page/心理学.md" title="wikilink">心理学</a></p></td>
<td><p>2007年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/玉泉八州男.md" title="wikilink">玉泉八州男</a></p></td>
<td><p><a href="../Page/英文学.md" title="wikilink">英文学</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/青柳正規.md" title="wikilink">青柳正規</a></p></td>
<td><p><a href="../Page/美術史学.md" title="wikilink">美術史学</a>・<a href="../Page/古典考古学.md" title="wikilink">古典考古学</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/川本皓嗣.md" title="wikilink">川本皓嗣</a></p></td>
<td><p><a href="../Page/比較文学.md" title="wikilink">比較文学</a>・<a href="../Page/比較文化.md" title="wikilink">比較文化</a></p></td>
<td><p>2009年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/佐藤彰一.md" title="wikilink">佐藤彰一</a></p></td>
<td><p><a href="../Page/西洋中世史.md" title="wikilink">西洋中世史</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/塩川徹也.md" title="wikilink">塩川徹也</a></p></td>
<td><p><a href="../Page/法语文学.md" title="wikilink">法语文学</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/富永健一.md" title="wikilink">富永健一</a></p></td>
<td><p><a href="../Page/社会学.md" title="wikilink">社会学</a></p></td>
<td><p>2010年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/東野治之.md" title="wikilink">東野治之</a></p></td>
<td><p><a href="../Page/日本史.md" title="wikilink">日本史</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/薗田坦.md" title="wikilink">薗田坦</a></p></td>
<td><p><a href="../Page/西洋近世哲学史.md" title="wikilink">西洋近世哲学史</a>・<a href="../Page/宗教哲学.md" title="wikilink">宗教哲学</a></p></td>
<td><p>2011年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>空席</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>空席</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第2分科<br />
（<a href="../Page/法律学.md" title="wikilink">法律学</a>・<a href="../Page/政治学.md" title="wikilink">政治学</a>）<br />
24名</p></td>
<td><p><a href="../Page/團藤重光.md" title="wikilink">團藤重光</a></p></td>
<td><p><a href="../Page/刑法.md" title="wikilink">刑法</a>・<a href="../Page/刑事訴訟法.md" title="wikilink">刑事訴訟法</a></p></td>
<td><p>1981年</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/小田滋.md" title="wikilink">小田滋</a></p></td>
<td><p><a href="../Page/国際法.md" title="wikilink">国際法</a></p></td>
<td><p>1994年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/河本一郎.md" title="wikilink">河本一郎</a></p></td>
<td><p><a href="../Page/商法.md" title="wikilink">商法</a>・<a href="../Page/証券取引法.md" title="wikilink">証券取引法</a></p></td>
<td><p>1995年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/星野英一.md" title="wikilink">星野英一</a></p></td>
<td><p><a href="../Page/民法.md" title="wikilink">民法</a></p></td>
<td><p>1996年</p></td>
<td><p>第1部部長</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/京極純一.md" title="wikilink">京極純一</a></p></td>
<td><p><a href="../Page/政治学.md" title="wikilink">政治学</a>・<a href="../Page/政治過程論.md" title="wikilink">政治過程論</a></p></td>
<td><p>1997年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/中野貞一郎.md" title="wikilink">中野貞一郎</a></p></td>
<td><p><a href="../Page/民事手続法.md" title="wikilink">民事手続法</a></p></td>
<td><p>1998年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/塩野宏.md" title="wikilink">塩野宏</a></p></td>
<td><p><a href="../Page/行政法.md" title="wikilink">行政法</a></p></td>
<td><p>1999年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/三宅一郎.md" title="wikilink">三宅一郎</a></p></td>
<td><p><a href="../Page/政治学.md" title="wikilink">政治学</a>・<a href="../Page/投票行動.md" title="wikilink">政治行動論</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/樋口陽一.md" title="wikilink">樋口陽一</a></p></td>
<td><p><a href="../Page/憲法学.md" title="wikilink">憲法学</a></p></td>
<td><p>2000年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/村上淳一.md" title="wikilink">村上淳一</a></p></td>
<td><p><a href="../Page/德国法.md" title="wikilink">德国法</a></p></td>
<td><p>2001年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/石本泰雄.md" title="wikilink">石本泰雄</a></p></td>
<td><p><a href="../Page/国際法.md" title="wikilink">国際法</a></p></td>
<td><p>2002年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/三谷太一郎.md" title="wikilink">三谷太一郎</a></p></td>
<td><p><a href="../Page/日本政治史.md" title="wikilink">日本政治外交史</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/奥田昌道.md" title="wikilink">奥田昌道</a></p></td>
<td><p><a href="../Page/民法.md" title="wikilink">民法</a></p></td>
<td><p>2004年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/松尾浩也.md" title="wikilink">松尾浩也</a></p></td>
<td><p><a href="../Page/刑事法.md" title="wikilink">刑事法</a></p></td>
<td><p>2005年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/西尾勝.md" title="wikilink">西尾勝</a></p></td>
<td><p><a href="../Page/行政学.md" title="wikilink">行政学</a></p></td>
<td><p>2007年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/竹下守夫.md" title="wikilink">竹下守夫</a></p></td>
<td><p><a href="../Page/民事訴訟法.md" title="wikilink">民事訴訟法</a></p></td>
<td><p>2008年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/龍田節.md" title="wikilink">龍田節</a></p></td>
<td><p><a href="../Page/商法.md" title="wikilink">商法</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/菅野和夫.md" title="wikilink">菅野和夫</a></p></td>
<td><p><a href="../Page/劳动法.md" title="wikilink">劳动法</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/石井紫郎.md" title="wikilink">石井紫郎</a></p></td>
<td><p><a href="../Page/日本法制史.md" title="wikilink">日本法制史</a></p></td>
<td><p>2009年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/小山貞夫.md" title="wikilink">小山貞夫</a></p></td>
<td><p><a href="../Page/西洋法制史.md" title="wikilink">西洋法制史</a></p></td>
<td><p>2010年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/平井宜雄.md" title="wikilink">平井宜雄</a></p></td>
<td><p><a href="../Page/民法.md" title="wikilink">民法</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/佐々木毅.md" title="wikilink">佐々木毅</a></p></td>
<td><p><a href="../Page/政治学史.md" title="wikilink">政治学史</a>・<a href="../Page/政治学.md" title="wikilink">政治学</a></p></td>
<td><p>2011年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>空席</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>空席</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第3分科<br />
（<a href="../Page/经济学.md" title="wikilink">经济学</a>・<a href="../Page/商学.md" title="wikilink">商学</a>）<br />
16名</p></td>
<td><p><a href="../Page/宇澤弘文.md" title="wikilink">宇澤弘文</a></p></td>
<td><p><a href="../Page/经济学.md" title="wikilink">经济学</a></p></td>
<td><p>1989年</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/小宮隆太郎.md" title="wikilink">小宮隆太郎</a></p></td>
<td><p><a href="../Page/经济学.md" title="wikilink">经济学</a></p></td>
<td><p>1990年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/石川滋.md" title="wikilink">石川滋</a></p></td>
<td><p><a href="../Page/開発经济論.md" title="wikilink">開発经济論</a></p></td>
<td><p>1998年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/水田洋.md" title="wikilink">水田洋</a></p></td>
<td><p><a href="../Page/社会思想史.md" title="wikilink">社会思想史</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/根岸隆.md" title="wikilink">根岸隆</a></p></td>
<td><p><a href="../Page/经济理論.md" title="wikilink">经济理論</a>・<a href="../Page/经济学史.md" title="wikilink">经济学史</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/速水融.md" title="wikilink">速水融</a></p></td>
<td><p><a href="../Page/日本経済史.md" title="wikilink">日本経済史</a>・<a href="../Page/歴史人口学.md" title="wikilink">歴史人口学</a></p></td>
<td><p>2001年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/伊藤誠.md" title="wikilink">伊藤誠</a></p></td>
<td><p><a href="../Page/经济理論.md" title="wikilink">经济理論</a>・<a href="../Page/現状分析.md" title="wikilink">現状分析</a></p></td>
<td><p>2003年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/貝塚啓明.md" title="wikilink">貝塚啓明</a></p></td>
<td><p><a href="../Page/財政学.md" title="wikilink">財政学</a>・<a href="../Page/金融論.md" title="wikilink">金融論</a></p></td>
<td><p>2006年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/新開陽一.md" title="wikilink">新開陽一</a></p></td>
<td><p><a href="../Page/经济学.md" title="wikilink">经济学</a></p></td>
<td><p>2008年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/石井寛治.md" title="wikilink">石井寛治</a></p></td>
<td><p><a href="../Page/日本经济史.md" title="wikilink">日本经济史</a></p></td>
<td><p>2009年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/藤田昌久.md" title="wikilink">藤田昌久</a></p></td>
<td><p><a href="../Page/都市・地域经济学.md" title="wikilink">都市・地域经济学</a>（<a href="../Page/空間经济学.md" title="wikilink">空間经济学</a>）</p></td>
<td><p>2010年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鈴村興太郎.md" title="wikilink">鈴村興太郎</a></p></td>
<td><p><a href="../Page/劳工经济学.md" title="wikilink">劳工经济学</a>・<a href="../Page/社会選択理論.md" title="wikilink">社会選択理論</a></p></td>
<td><p>2011年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>空席</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>空席</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>空席</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>空席</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第2部<br />
<a href="../Page/自然科学.md" title="wikilink">自然科学部門</a></p></td>
<td><p>第4分科<br />
（<a href="../Page/理学.md" title="wikilink">理学</a>）<br />
31名</p></td>
<td><p><a href="../Page/藤田良雄.md" title="wikilink">藤田良雄</a></p></td>
<td><p><a href="../Page/天文学.md" title="wikilink">天文学</a></p></td>
<td><p>1965年</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/江崎玲於奈.md" title="wikilink">江崎玲於奈</a></p></td>
<td><p><a href="../Page/物理学.md" title="wikilink">物理学</a></p></td>
<td><p>1975年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/広中平祐.md" title="wikilink">広中平祐</a></p></td>
<td><p><a href="../Page/数学.md" title="wikilink">数学</a></p></td>
<td><p>1976年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/古在由秀.md" title="wikilink">古在由秀</a></p></td>
<td><p><a href="../Page/天文学.md" title="wikilink">天文学</a></p></td>
<td><p>1980年</p></td>
<td><p>第2部部長</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/長倉三郎.md" title="wikilink">長倉三郎</a></p></td>
<td><p><a href="../Page/物理化学.md" title="wikilink">物理化学</a></p></td>
<td><p>1984年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/関集三.md" title="wikilink">関集三</a></p></td>
<td><p><a href="../Page/物理化学.md" title="wikilink">物理化学</a></p></td>
<td><p>1985年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/富澤純一.md" title="wikilink">富澤純一</a></p></td>
<td><p><a href="../Page/分子生物学.md" title="wikilink">分子生物学</a></p></td>
<td><p>1990年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/久城育夫.md" title="wikilink">久城育夫</a></p></td>
<td><p><a href="../Page/岩石学.md" title="wikilink">岩石学</a></p></td>
<td><p>1993年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/向山光昭.md" title="wikilink">向山光昭</a></p></td>
<td><p><a href="../Page/有機合成化学.md" title="wikilink">有機合成化学</a></p></td>
<td><p>1994年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/大澤文夫.md" title="wikilink">大澤文夫</a></p></td>
<td><p><a href="../Page/生物物理学.md" title="wikilink">生物物理学</a></p></td>
<td><p>1995年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/横山泉.md" title="wikilink">横山泉</a></p></td>
<td><p><a href="../Page/火山物理学.md" title="wikilink">火山物理学</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/井口洋夫.md" title="wikilink">井口洋夫</a></p></td>
<td><p><a href="../Page/物理化学.md" title="wikilink">物理化学</a></p></td>
<td><p>1996年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/上田誠也.md" title="wikilink">上田誠也</a></p></td>
<td><p><a href="../Page/地球物理学.md" title="wikilink">地球物理学</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/近藤淳.md" title="wikilink">近藤淳</a></p></td>
<td><p><a href="../Page/物理学.md" title="wikilink">物理学</a></p></td>
<td><p>1997年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/森重文.md" title="wikilink">森重文</a></p></td>
<td><p><a href="../Page/数学.md" title="wikilink">数学</a></p></td>
<td><p>1998年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/竹市雅俊.md" title="wikilink">竹市雅俊</a></p></td>
<td><p><a href="../Page/发育生物学.md" title="wikilink">发育生物学</a>・<a href="../Page/細胞生物学.md" title="wikilink">細胞生物学</a></p></td>
<td><p>2000年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/大村智.md" title="wikilink">大村智</a></p></td>
<td><p><a href="../Page/天然物有機化学.md" title="wikilink">天然物有機化学</a></p></td>
<td><p>2001年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/小柴昌俊.md" title="wikilink">小柴昌俊</a></p></td>
<td><p><a href="../Page/物理学.md" title="wikilink">物理学</a></p></td>
<td><p>2002年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/山崎敏光.md" title="wikilink">山崎敏光</a></p></td>
<td><p><a href="../Page/原子核物理学.md" title="wikilink">原子核物理学</a></p></td>
<td><p>2005年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/松野太郎.md" title="wikilink">松野太郎</a></p></td>
<td><p><a href="../Page/气象学.md" title="wikilink">气象学</a>・<a href="../Page/地球物理学.md" title="wikilink">地球物理学</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/物理学.md" title="wikilink">物理学</a></p></td>
<td><p>2007年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/柏原正樹.md" title="wikilink">柏原正樹</a></p></td>
<td><p><a href="../Page/数学.md" title="wikilink">数学</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/西田篤弘.md" title="wikilink">西田篤弘</a></p></td>
<td><p><a href="../Page/宇宙空間物理学.md" title="wikilink">宇宙空間物理学</a></p></td>
<td><p>2008年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/深谷賢治.md" title="wikilink">深谷賢治</a></p></td>
<td><p><a href="../Page/数学.md" title="wikilink">数学</a></p></td>
<td><p>2009年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/益川敏英.md" title="wikilink">益川敏英</a></p></td>
<td><p><a href="../Page/物理学.md" title="wikilink">物理学</a></p></td>
<td><p>2010年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/黒岩常祥.md" title="wikilink">黒岩常祥</a></p></td>
<td><p><a href="../Page/生物科学.md" title="wikilink">生物科学</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/小林誠.md" title="wikilink">小林誠</a></p></td>
<td><p><a href="../Page/物理学.md" title="wikilink">物理学</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/長田重一.md" title="wikilink">長田重一</a></p></td>
<td><p><a href="../Page/分子生物学.md" title="wikilink">分子生物学</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鈴木章.md" title="wikilink">鈴木章</a></p></td>
<td><p><a href="../Page/合成化学.md" title="wikilink">合成化学</a></p></td>
<td><p>2011年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/田中靖郎.md" title="wikilink">田中靖郎</a></p></td>
<td><p><a href="../Page/天文学.md" title="wikilink">天文学</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/深尾良夫.md" title="wikilink">深尾良夫</a></p></td>
<td><p><a href="../Page/地震学.md" title="wikilink">地震学</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>第5分科<br />
（<a href="../Page/工学.md" title="wikilink">工学</a>）<br />
17名</p></td>
<td><p><a href="../Page/荒田吉明.md" title="wikilink">荒田吉明</a></p></td>
<td><p><a href="../Page/高温工学.md" title="wikilink">高温工学</a>・<a href="../Page/溶接工学.md" title="wikilink">溶接工学</a></p></td>
<td><p>1988年</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/西澤潤一.md" title="wikilink">西澤潤一</a></p></td>
<td><p><a href="../Page/電子工学.md" title="wikilink">電子工学</a>・<a href="../Page/通信工学.md" title="wikilink">通信工学</a></p></td>
<td><p>1995年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/横堀武夫.md" title="wikilink">横堀武夫</a></p></td>
<td><p><a href="../Page/材料工学.md" title="wikilink">材料工学</a>・<a href="../Page/機械工学.md" title="wikilink">機械工学</a></p></td>
<td><p>1996年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/乾崇夫.md" title="wikilink">乾崇夫</a></p></td>
<td><p><a href="../Page/造船学.md" title="wikilink">造船学</a>（<a href="../Page/船型学.md" title="wikilink">船型学</a>）</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/野崎一.md" title="wikilink">野崎一</a></p></td>
<td><p><a href="../Page/有機合成化学.md" title="wikilink">有機合成化学</a>・<a href="../Page/有機金属化学.md" title="wikilink">有機金属化学</a></p></td>
<td><p>1999年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/白川英樹.md" title="wikilink">白川英樹</a></p></td>
<td><p><a href="../Page/物質科学.md" title="wikilink">物質科学</a>・<a href="../Page/高分子化学.md" title="wikilink">高分子化学</a></p></td>
<td><p>2001年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/野依良治.md" title="wikilink">野依良治</a></p></td>
<td><p><a href="../Page/有機化学.md" title="wikilink">有機化学</a></p></td>
<td><p>2002年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/岩崎俊一.md" title="wikilink">岩崎俊一</a></p></td>
<td><p><a href="../Page/電子通信工学.md" title="wikilink">電子通信工学</a>・<a href="../Page/磁气工学.md" title="wikilink">磁气工学</a></p></td>
<td><p>2003年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/岡村總吾.md" title="wikilink">岡村總吾</a></p></td>
<td><p><a href="../Page/電子工学.md" title="wikilink">電子工学</a>・<a href="../Page/通信工学.md" title="wikilink">通信工学</a></p></td>
<td><p>2005年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/井上明久.md" title="wikilink">井上明久</a></p></td>
<td><p><a href="../Page/金属材料学.md" title="wikilink">金属材料学</a></p></td>
<td><p>2006年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/田中耕一.md" title="wikilink">田中耕一</a></p></td>
<td><p><a href="../Page/質量分析.md" title="wikilink">質量分析</a></p></td>
<td><p>最年青会員</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/堀川清司.md" title="wikilink">堀川清司</a></p></td>
<td><p><a href="../Page/土木工学.md" title="wikilink">土木工学</a>（<a href="../Page/海岸工学.md" title="wikilink">海岸工学</a>）</p></td>
<td><p>2007年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/霜田光一.md" title="wikilink">霜田光一</a></p></td>
<td><p><a href="../Page/物理学.md" title="wikilink">物理学</a></p></td>
<td><p>2010年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/内田祥哉.md" title="wikilink">内田祥哉</a></p></td>
<td><p><a href="../Page/建築学.md" title="wikilink">建築学</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/飯島澄男.md" title="wikilink">飯島澄男</a></p></td>
<td><p><a href="../Page/物質科学.md" title="wikilink">物質科学</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/堀幸夫.md" title="wikilink">堀幸夫</a></p></td>
<td><p><a href="../Page/機械工学.md" title="wikilink">機械工学</a></p></td>
<td><p>2011年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>空席</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第6分科<br />
（<a href="../Page/農学.md" title="wikilink">農学</a>）<br />
12名</p></td>
<td><p><a href="../Page/田村三郎.md" title="wikilink">田村三郎</a></p></td>
<td><p><a href="../Page/生物有機化学.md" title="wikilink">生物有機化学</a></p></td>
<td><p>1989年</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/沢田敏男.md" title="wikilink">沢田敏男</a></p></td>
<td><p><a href="../Page/農業農村工学.md" title="wikilink">農業農村工学</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/山田康之.md" title="wikilink">山田康之</a></p></td>
<td><p><a href="../Page/植物分子細胞生物学.md" title="wikilink">植物分子細胞生物学</a></p></td>
<td><p>1995年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/入谷明.md" title="wikilink">入谷明</a></p></td>
<td><p><a href="../Page/家畜繁殖学.md" title="wikilink">家畜繁殖学</a></p></td>
<td><p>2000年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/和田光史.md" title="wikilink">和田光史</a></p></td>
<td><p><a href="../Page/土壌学.md" title="wikilink">土壌学</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/四方英四郎.md" title="wikilink">四方英四郎</a></p></td>
<td><p><a href="../Page/植物病理学.md" title="wikilink">植物病理学</a>・<a href="../Page/病毒学.md" title="wikilink">病毒学</a></p></td>
<td><p>2001年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/稲上正.md" title="wikilink">稲上正</a></p></td>
<td><p><a href="../Page/分子細胞生理学.md" title="wikilink">分子細胞生理学</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/別府輝彦.md" title="wikilink">別府輝彦</a></p></td>
<td><p><a href="../Page/応用微生物学.md" title="wikilink">応用微生物学</a></p></td>
<td><p>2004年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/常脇恒一郎.md" title="wikilink">常脇恒一郎</a></p></td>
<td><p><a href="../Page/植物遺伝学.md" title="wikilink">植物遺伝学</a></p></td>
<td><p>2005年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/佐々木惠彦.md" title="wikilink">佐々木惠彦</a></p></td>
<td><p><a href="../Page/森林資源科学.md" title="wikilink">森林資源科学</a>・<a href="../Page/樹木生理学.md" title="wikilink">樹木生理学</a></p></td>
<td><p>2006年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/喜田宏.md" title="wikilink">喜田宏</a></p></td>
<td><p><a href="../Page/獣医微生物学.md" title="wikilink">獣医微生物学</a></p></td>
<td><p>2007年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>空席</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>第7分科<br />
（<a href="../Page/医学.md" title="wikilink">医学</a>・<a href="../Page/药学.md" title="wikilink">药学</a>・<a href="../Page/歯学.md" title="wikilink">歯学</a>）<br />
20名</p></td>
<td><p><a href="../Page/早石修.md" title="wikilink">早石修</a></p></td>
<td><p><a href="../Page/生化学.md" title="wikilink">生化学</a>・<a href="../Page/医化学.md" title="wikilink">医化学</a></p></td>
<td><p>1974年</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/杉村隆.md" title="wikilink">杉村隆</a></p></td>
<td><p><a href="../Page/生化学.md" title="wikilink">生化学</a>・<a href="../Page/肿瘤学.md" title="wikilink">肿瘤学</a></p></td>
<td><p>1982年</p></td>
<td><p>幹事</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/山川民夫.md" title="wikilink">山川民夫</a></p></td>
<td><p><a href="../Page/生化学.md" title="wikilink">生化学</a></p></td>
<td><p>1987年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/伊藤正男.md" title="wikilink">伊藤正男</a></p></td>
<td><p><a href="../Page/生理学.md" title="wikilink">生理学</a>・<a href="../Page/神经科学.md" title="wikilink">神经科学</a></p></td>
<td><p>1989年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/柴田承二.md" title="wikilink">柴田承二</a></p></td>
<td><p><a href="../Page/天然物化学.md" title="wikilink">天然物化学</a>・<a href="../Page/生药学.md" title="wikilink">生药学</a></p></td>
<td><p>1993年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/豊島久真男.md" title="wikilink">豊島久真男</a></p></td>
<td><p><a href="../Page/病毒学.md" title="wikilink">病毒学</a>・<a href="../Page/肿瘤学.md" title="wikilink">肿瘤学</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/織田敏次.md" title="wikilink">織田敏次</a></p></td>
<td><p><a href="../Page/内科学.md" title="wikilink">内科学</a></p></td>
<td><p>1994年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/井村裕夫.md" title="wikilink">井村裕夫</a></p></td>
<td><p><a href="../Page/内科学.md" title="wikilink">内科学</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/大塚正徳.md" title="wikilink">大塚正徳</a></p></td>
<td><p><a href="../Page/薬理学.md" title="wikilink">薬理学</a></p></td>
<td><p>1995年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/岸本忠三.md" title="wikilink">岸本忠三</a></p></td>
<td><p><a href="../Page/免疫学.md" title="wikilink">免疫学</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/濱清.md" title="wikilink">濱清</a></p></td>
<td><p><a href="../Page/解剖学.md" title="wikilink">解剖学</a></p></td>
<td><p>1996年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/石坂公成.md" title="wikilink">石坂公成</a></p></td>
<td><p><a href="../Page/免疫学.md" title="wikilink">免疫学</a></p></td>
<td><p>1997年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/廣川信隆.md" title="wikilink">廣川信隆</a></p></td>
<td><p><a href="../Page/分子細胞生物学.md" title="wikilink">分子細胞生物学</a></p></td>
<td><p>2004年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/関谷剛男.md" title="wikilink">関谷剛男</a></p></td>
<td><p><a href="../Page/药学.md" title="wikilink">药学</a>・<a href="../Page/核酸有機化学.md" title="wikilink">核酸有機化学</a></p></td>
<td><p>2005年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/本庶佑.md" title="wikilink">本庶佑</a></p></td>
<td><p><a href="../Page/医化学.md" title="wikilink">医化学</a>・<a href="../Page/分子免疫学.md" title="wikilink">分子免疫学</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/須田立雄.md" title="wikilink">須田立雄</a></p></td>
<td><p><a href="../Page/基礎歯科医学.md" title="wikilink">基礎歯科医学</a></p></td>
<td><p>2007年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鈴木邦彦.md" title="wikilink">鈴木邦彦</a></p></td>
<td><p><a href="../Page/神经化学.md" title="wikilink">神经化学</a>・<a href="../Page/神经内科.md" title="wikilink">神经内科</a>・<a href="../Page/遗传性神经疾患.md" title="wikilink">遗传性神经疾患</a></p></td>
<td><p>2008年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/中西重忠.md" title="wikilink">中西重忠</a></p></td>
<td><p><a href="../Page/分子神经科学.md" title="wikilink">分子神经科学</a></p></td>
<td><p>2009年</p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p>空席</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>空席</p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 歷任院長

### 東京學士會院會長

| 任次     | 姓名                                     | 任期開始時間 | 任期結束時間 | 所屬大學                                    |
| ------ | -------------------------------------- | ------ | ------ | --------------------------------------- |
| **1.** | [福泽谕吉](../Page/福泽谕吉.md "wikilink")     | 1879   | 1879   | [慶應義塾](../Page/學校法人慶應義塾.md "wikilink")  |
| **2.** | [西周](../Page/西周_\(启蒙家\).md "wikilink") | 1879   | 1880   | [養老館](../Page/養老館.md "wikilink")        |
| **3.** | [加藤弘之](../Page/加藤弘之.md "wikilink")     | 1880   | 1882   | [東京大學](../Page/東京大學.md "wikilink")      |
| **4.** | 西周                                     | 1882   | 1886   | 養老館                                     |
| **5.** | 加藤弘之                                   | 1886   | 1895   | 東京大學                                    |
| **6.** | [細川潤次郎](../Page/細川潤次郎.md "wikilink")   | 1895   | 1897   | [文武館](../Page/文武館.md "wikilink")（土佐藩藩校） |
| **7.** | 加藤弘之                                   | 1897   | 1906   | 東京大學                                    |

### 帝國學士院院長

| 任次      | 姓名                                   | 任期開始時間 | 任期結束時間 | 所屬大學                                     |
| ------- | ------------------------------------ | ------ | ------ | ---------------------------------------- |
| **8.**  | 加藤弘之                                 | 1906   | 1909   | 東京大學                                     |
| **9.**  | [菊池大麓](../Page/菊池大麓.md "wikilink")   | 1909   | 1917   | [蕃書調所](../Page/蕃書調所.md "wikilink")（東京大學） |
| **10.** | [穗積陳重](../Page/穗積陳重.md "wikilink")   | 1917   | 1925   | 東京大學                                     |
| **11.** | [岡野敬次郎](../Page/岡野敬次郎.md "wikilink") | 1925   | 1925   | 東京大學                                     |
| **12.** | [櫻井錠二](../Page/櫻井錠二.md "wikilink")   | 1925   | 1939   | [開成學校](../Page/開成學校.md "wikilink")（東京大學） |
| **13.** | [長岡半太郎](../Page/長岡半太郎.md "wikilink") | 1939   | 1948   | 東京大學                                     |

### 日本學士院院長

| 任次      | 姓名                                             | 任期開始時間 | 任期結束時間 | 所屬大學                                      |
| ------- | ---------------------------------------------- | ------ | ------ | ----------------------------------------- |
| **14.** | [山田三良](../Page/山田三良.md "wikilink")             | 1948   | 1961   | 東京大學                                      |
| **15.** | [柴田雄次](../Page/柴田雄次.md "wikilink")             | 1961   | 1970   | 東京大學                                      |
| **16.** | [南原繁](../Page/南原繁.md "wikilink")               | 1970   | 1974   | 東京大學                                      |
| **17.** | [和達清夫](../Page/和達清夫.md "wikilink")             | 1974   | 1980   | 東京大學                                      |
| **18.** | [有澤廣巳](../Page/有澤廣巳.md "wikilink")             | 1980   | 1986   | 東京大學                                      |
| **19.** | [黑川利雄](../Page/黑川利雄.md "wikilink")             | 1986   | 1988   | [东北大学](../Page/东北大学_\(日本\).md "wikilink") |
| **20.** | [脇村義太郎](../Page/脇村義太郎.md "wikilink")           | 1988   | 1994   | 東京大學                                      |
| **21.** | [藤田良雄](../Page/藤田良雄.md "wikilink")             | 1994   | 2000   | 東京大學                                      |
| **22.** | [市古貞次](../Page/市古貞次.md "wikilink")             | 2000   | 2001   | 東京大學                                      |
| **23.** | [長倉三郎](../Page/長倉三郎.md "wikilink")             | 2001   | 2007   | 東京大學                                      |
| **24.** | [久保正彰](../Page/久保正彰_\(西洋古典文學家\).md "wikilink") | 2007   | 2013   | 東京大學|東京大學                                 |
| **25.** | [杉村隆](../Page/杉村隆.md "wikilink")               | 2013   | 現任     | 東京大學|東京大學                                 |

## 主要活动

  - 颁奖
      - [日本学士院恩賜奖](../Page/日本学士院恩賜奖.md "wikilink")
      - [日本学士院奖](../Page/日本学士院奖.md "wikilink")
      - [爱丁堡公爵奖](../Page/爱丁堡公爵奖.md "wikilink")
  - 编辑发行纪要
      - 日本学士院紀要
      - PROCEEDINGS OF THE JAPAN ACADEMY
  - 进行其他奖励学术研究的活动
  - 国際交流
  - 研究費補助活動
  - 公開演讲会

## 参考资料

## 外部链接

  - [日本学士院](http://www.japan-acad.go.jp)

[日本學士院](../Category/日本學士院.md "wikilink")
[Category:国家科学院](../Category/国家科学院.md "wikilink")
[Category:文部科学省](../Category/文部科学省.md "wikilink")

1.  [会員一覧](http://www.japan-acad.go.jp/japanese/members/)（日本学士院、2011年12月13日閲覧）
2.  [新会員的选举（2011年12月12日）](http://www.japan-acad.go.jp/japanese/news/2011/121201.html)（日本学士院、2011年12月13日閲覧）