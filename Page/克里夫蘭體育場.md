[6a29310r.jpg](https://zh.wikipedia.org/wiki/File:6a29310r.jpg "fig:6a29310r.jpg")

**克里夫蘭體育場**（又名**湖濱體育場**，**克里夫蘭市立體育場**以及**湖畔/湖上的錯誤**）是一個座落於[俄亥俄州克里夫蘭的](../Page/克里夫蘭_\(俄亥俄州\).md "wikilink")[棒球和](../Page/棒球.md "wikilink")[美式足球運動場](../Page/美式足球.md "wikilink")。在這個體育場存在的最後幾年裡，它在棒球比賽中可容納74,000名觀眾，美式足球比賽更可容納78,000人。

體育場是由[華克─維克斯建築公司和](../Page/華克─維克斯建築公司.md "wikilink")[奧斯本工程公司所設計](../Page/奧斯本工程公司.md "wikilink")，[威廉‧霍普金斯](../Page/威廉‧霍普金斯.md "wikilink")(William
R. Hopkins)和[丹尼爾‧摩根](../Page/丹尼爾‧摩根.md "wikilink")(Daniel E.
Morgan)兩位[市行政官監造而成](../Page/市行政官.md "wikilink")，並且是最早使用[鋁結構的工程之一](../Page/鋁.md "wikilink")。[唐納‧葛雷花園](../Page/唐納‧葛雷花園.md "wikilink")(Donald
Gray Gardens)在1936年設置於體育場北側，成為[大湖博覽會](../Page/大湖博覽會.md "wikilink")(Great
Lakes Exposition)的景點之一。

在公民投票以接近二比一的差距認可體育場的興建之後，體育場於1931年7月1日正式啟用，並在兩天後舉行[麥克斯‧施梅林](../Page/麥克斯‧施梅林.md "wikilink")(Max
Schmeling)迎戰[楊‧斯特里布林](../Page/楊‧斯特里布林.md "wikilink")(Young
Stribling)的[重量級](../Page/重量級.md "wikilink")[拳擊賽](../Page/拳擊賽.md "wikilink")。

據說，這座體育場是為了爭辦[1932年夏季奧運而興建的](../Page/1932年夏季奧運.md "wikilink")，但最後是由[洛杉磯取得奧運主辦權](../Page/洛杉磯.md "wikilink")。然而此說不確；事實上，早在體育場動土興建之前，1932年奧運即已決定由洛杉磯舉辦。\[1\]更確切地說，它是為了舉辦高中和大學的美式足球賽，以及[克里夫蘭印地安人隊的球賽而興建的](../Page/克里夫蘭印地安人隊.md "wikilink")。自1932年球季中段開始到1933年，印地安人在體育場裡進行所有主場球賽；然而，即使是多達四萬名的觀眾數，也會被體育場龐大的空間給吞沒，於是他們在1934年又將大部分主場比賽搬回老家[聯盟球場去](../Page/聯盟球場.md "wikilink")。

但在1936年，印地安人開始在夏季移師市立體育場，舉行週日和假日球賽，自1938年起，一些精挑細選的重要比賽也在這兒舉行。1939年開始，連夜間比賽都到這兒打（因為聯盟球場缺乏燈光）；到了1940年，印地安人隊絕大部分的主場球賽都在市立體育場舉辦，並在1945年球季之後完全放棄聯盟球場。他們在市立體育場一直打到1993年球季結束，搬進[傑克布斯球場為止](../Page/傑克布斯球場.md "wikilink")。\[2\]

由於體育場主要是美式足球場，空間對棒球而言大到深不見底，因此在1947年增設一道內牆，以縮短外野距離。從來沒有球員能把一支[全壘打打進將近](../Page/全壘打.md "wikilink")480呎之外的中外野[看台](../Page/看台.md "wikilink")。印地安人隊老闆[比爾‧維克後來在他的自傳](../Page/比爾‧維克.md "wikilink")*維克─宛如殘廢*(Veeck
- As in
Wreck)寫到，他會把這道圍牆向內或向外移動，來回多達15呎之遠，以爭取對印地安人隊最有利的距離，直到[美國聯盟特別立法禁止在球季進行中移動圍牆為止](../Page/美國聯盟.md "wikilink")。

如同許多在[警告跑道成為標準配備時即已落成的球場](../Page/警告跑道.md "wikilink")，市立體育場的圍牆邊也有一條陡升的狹道。即使在加裝內牆之後，這條狹道在美式足球季也還是清晰可見。

這幢建築物隔著一條街就是[伊利湖](../Page/伊利湖.md "wikilink")，因此最為人熟知的就是在整個冬季，以及大半個春天和秋天裡不斷灌入體育場的刺骨寒風。炎熱的夏夜則有成群結隊的[蚊蠅和](../Page/蚊蠅.md "wikilink")[蜉蝣前來填補空缺](../Page/蜉蝣.md "wikilink")。它在後半生裡被稱為「湖上的錯誤」，在整個克里夫蘭市都淪為笑柄的那些日子裡受盡冷嘲熱諷。

然而，這幢建築也有過自己的光輝歲月和快樂時光。1948年，在[投手](../Page/投手.md "wikilink")[鮑伯‧斐勒和兼任](../Page/鮑伯‧斐勒.md "wikilink")[總教練的](../Page/總教練.md "wikilink")[游擊手](../Page/游擊手.md "wikilink")[路‧波德魯帶領下](../Page/路‧波德魯.md "wikilink")，印地安人隊贏得[美聯冠軍和](../Page/美國聯盟.md "wikilink")[世界大賽](../Page/世界大賽.md "wikilink")。1949年，在聯盟冠軍被[紐約洋基隊奪去之後](../Page/紐約洋基隊.md "wikilink")，他們將1948年的冠軍旗埋葬在外野。1954年，印地安人由總教練[艾爾‧羅培茲和以](../Page/艾爾‧羅培茲.md "wikilink")[鮑伯‧李蒙為首的傑出投手群領軍之下](../Page/鮑伯‧李蒙.md "wikilink")，以刷新紀錄的111勝再次拿下美聯冠軍，但卻在[世界大賽被](../Page/1954年世界大賽.md "wikilink")[紐約巨人隊直落四橫掃](../Page/舊金山巨人.md "wikilink")。它在1935、1954、1963和1981年分別主辦過四次[明星賽](../Page/大聯盟明星賽.md "wikilink")。1993年10月3日，在它作為印地安人主場的最後一場比賽中，印地安人的球迷在[喜劇演員](../Page/喜劇演員.md "wikilink")[鮑伯‧霍伯](../Page/鮑伯‧霍伯.md "wikilink")（他從小到大都是印地安人迷，也曾是印地安人隊的一位股東）帶領下，齊唱他的[成名曲](../Page/成名曲.md "wikilink")「[感謝記憶](../Page/感謝記憶.md "wikilink")」(Thanks
for the
Memory)，以專為這一刻而寫的歌詞（這正是他在許多[電視節目中的拿手絕活](../Page/電視節目.md "wikilink")），向這個舊體育場說再見。
[1954_World_Series_game_two.jpeg](https://zh.wikipedia.org/wiki/File:1954_World_Series_game_two.jpeg "fig:1954_World_Series_game_two.jpeg")場內一景\]\]

[NFL的](../Page/國家美式足球聯盟.md "wikilink")[克里夫蘭布朗隊自](../Page/克里夫蘭布朗.md "wikilink")1946年開始在這兒比賽，直到1995年為止。市立體育場在1946、1948和1949年，是全美美式足球協會(AAFC)冠軍戰的場地，也是國家美式足球聯盟(NFL)1945年（華盛頓紅人對克里夫蘭公羊），1950年（洛杉磯公羊對布朗），1952年（底特律對布朗），1954年（底特律對布朗），1964年（巴爾的摩小馬對布朗），以及1968年（巴爾的摩小馬對布朗）的冠軍戰場地。它也是1987年1月11日[美國美式足球聯會冠軍戰時](../Page/美國美式足球聯會.md "wikilink")，[丹佛野馬隊和](../Page/丹佛野馬.md "wikilink")[約翰‧艾爾威](../Page/約翰‧艾爾威.md "wikilink")(John
Elway)上演那次留名青史的（或者在布朗隊球迷看來惡名昭彰的）[大推進](../Page/大推進.md "wikilink")(the
Drive)之舞台所在。

中外野看台是該隊許多狂熱球迷的家，到了1980年代更因球迷所製造，用以干擾敵隊進攻的狗吠聲，而以[惡犬欄](../Page/惡犬欄.md "wikilink")(Dawg
Pound)聞名。球迷們模仿的是布朗隊選手[漢佛‧狄克森](../Page/漢佛‧狄克森.md "wikilink")(Hanford
Dickson)和[法蘭克‧米尼菲爾德](../Page/法蘭克‧米尼菲爾德.md "wikilink")(Frank
Minnifield)，他們看起來總像是在對彼此，以及對手狂吠。有些球迷甚至還戴著狗面具，並且向對方進攻球員扔狗餅乾。

大學美式足球的唯一一次[大湖盃](../Page/大湖盃.md "wikilink")(Great Lake
Bowl)賽，於1947年在此舉行。體育場還在1942、1943、1945、1947、1950、1952、1976和1978年舉辦過[聖母大學和](../Page/聖母大學.md "wikilink")[海軍的友誼賽](../Page/海軍.md "wikilink")。最後一場大學美式足球賽是在1991年10月19日舉行的，[西北大學野貓隊在這兒扮演](../Page/西北大學野貓隊.md "wikilink")「主場」，迎戰[俄亥俄州大七葉樹隊](../Page/俄亥俄州大七葉樹隊.md "wikilink")。儘管西北大學分到了主場球隊應得的門票收入，絕大多數球迷卻是幫俄亥俄州大加油的。

除了運動賽事之外，這個體育場還舉辦過許多場[搖滾音樂會](../Page/搖滾音樂會.md "wikilink")，包括1966年[披頭四](../Page/披頭四樂隊.md "wikilink")(the
Beatles)的演唱會。1970年代還辦過一次[世界搖滾大賽](../Page/世界搖滾大賽.md "wikilink")(World
Series of Rock)，有許多當紅的樂團登台演出，包括[滾石樂隊](../Page/滾石樂隊.md "wikilink")(the
Rolling
Stones)；據報導，他們1978年7月1日的那場演唱會，是有史以來第一場營收超過百萬美元的演唱會。[搖滾樂名人堂的開幕演唱會](../Page/搖滾樂名人堂.md "wikilink")，也是1995年在體育館舉行的。體育場最後的大型活動之一，是[葛培理](../Page/葛培理.md "wikilink")(Billy
Graham)的布道會。

對於擁有體育場，並且自始至終一直營運著的克里夫蘭市政府而言，它是財政上的一個無底洞。1970年代中期，布朗隊老闆[亞特‧莫戴爾](../Page/亞特‧莫戴爾.md "wikilink")(Art
Modell)同意以每年一美元租下這個場館。莫戴爾的**體育場公司**(Stadium
Corporation)接手營運，並投資改善設施，包括豪華包廂。這些包廂是莫戴爾的搖錢樹，為他賺進大量利潤；但莫戴爾卻拒絕和職棒的印地安人隊分享包廂的營收，即使相當一部分的營收是棒球比賽所帶來的。最後，印地安人隊爭取到地方政府和選民，說服他們為球隊興建自己的球場，好讓他們自由支配包廂營收。堅信營收不至於被威脅的莫戴爾，則拒絕參與為[印地安人隊建造](../Page/克里夫蘭印地安人.md "wikilink")[傑克布斯球場](../Page/傑克布斯球場.md "wikilink")，以及為[騎士隊興建](../Page/克里夫蘭騎士隊.md "wikilink")[岡德體育館](../Page/岡德體育館.md "wikilink")(Gund
Arena)的[通道計劃案](../Page/通道計劃案.md "wikilink")。莫戴爾的設想最終證明是錯誤的，在1994年印地安人隊從體育場搬進傑克布斯球場之後，包廂營收大減。隔年，莫戴爾決定在1995年球季結束後，將美式足球隊遷到[馬里蘭州的](../Page/馬里蘭州.md "wikilink")[巴爾的摩](../Page/巴爾的摩.md "wikilink")。

莫戴爾搬遷球隊的行為，事實上毀棄了他們對體育場的租約，克里夫蘭市政府因而提出告訴。訴訟程序告一段落之後，體育場在隔年被拆除，殘骸則被丟進對街的湖裡，為漁民和潛水伕建造一個[人工魚礁](../Page/人工魚礁.md "wikilink")。

新建的[克里夫蘭布朗體育場正座落於市立體育場的原址](../Page/克里夫蘭布朗體育場.md "wikilink")。

## 參考資料

<references />

## 外部連結

  - [Cleveland Municipal Stadium at
    Ballparks.com](http://football.ballparks.com/NFL/ClevelandBrowns/oldindex.htm)
  - [克里夫蘭歷史百科全書](http://ech.cwru.edu/ech-cgi/article.pl?id=CMS5)

| colspan = 3 align = center |
**[印地安人隊主場](../Page/克里夫蘭印地安人.md "wikilink")**
|- | width = 30% align = center | 前任:
**[聯盟球場](../Page/聯盟球場.md "wikilink")
1901年-1946年** | width = 40% align = center | 1932年-1993年 | width = 30%
align = center | 後任 :
**[傑克布斯球場](../Page/傑克布斯球場.md "wikilink")
1994年至今** |- | colspan = 3 align = center |
**[克里夫蘭公羊隊主場](../Page/聖路易公羊.md "wikilink")**
|- | width = 30% align = center | 前任:
**最初使用球場** | width = 40% align = center | 1937年 | width = 30% align =
center | 後任 :
**[蕭體育場](../Page/蕭體育場.md "wikilink")
1938年** |- | colspan = 3 align = center |
**[克里夫蘭公羊隊主場](../Page/聖路易公羊.md "wikilink")**
|- | width = 30% align = center | 前任:
**[蕭體育場](../Page/蕭體育場.md "wikilink")
1938年** | width = 40% align = center | 1939年-1941年 | width = 30% align =
center | 後任 :
**[聯盟球場](../Page/聯盟球場.md "wikilink")
1942年，1944年-1945年** |- | colspan = 3 align = center |
**[克里夫蘭公羊隊主場](../Page/聖路易公羊.md "wikilink")** |- |
width = 30% align = center | 前任:
**[聯盟球場](../Page/聯盟球場.md "wikilink")
1942年，1944年-1945年** | width = 40% align = center | 1945年 | width = 30%
align = center | 後任 :
**[洛杉磯紀念體育場](../Page/洛杉磯紀念體育場.md "wikilink")
1946年-1979年** |- | colspan = 3 align = center |
**[克里夫蘭布朗隊主場](../Page/克里夫蘭布朗隊.md "wikilink")**
|- | width = 30% align = center | 前任:
**無** | width = 40% align = center | 1946年-1995年 | width = 30% align =
center | 後任 :
**[克里夫蘭布朗體育場](../Page/克里夫蘭布朗體育場.md "wikilink")
1999年至今** |-

[Category:美国体育场地](../Category/美国体育场地.md "wikilink") [Category:克里夫蘭
(俄亥俄州)](../Category/克里夫蘭_\(俄亥俄州\).md "wikilink")
[Category:已拆除的建築物](../Category/已拆除的建築物.md "wikilink")
[Category:1932年建立](../Category/1932年建立.md "wikilink")
[Category:1996年廢除](../Category/1996年廢除.md "wikilink")

1.
2.