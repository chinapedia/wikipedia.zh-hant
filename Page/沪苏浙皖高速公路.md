**沪苏浙皖高速公路**，又称**申苏浙皖高速公路**，是连接[中国](../Page/中华人民共和国.md "wikilink")[上海市与](../Page/上海市.md "wikilink")[江苏省](../Page/江苏省.md "wikilink")[苏州市](../Page/苏州市.md "wikilink")、[浙江省](../Page/浙江省.md "wikilink")[湖州市和](../Page/湖州市.md "wikilink")[安徽省](../Page/安徽省.md "wikilink")[宣城市之间的一条重要](../Page/宣城市.md "wikilink")[高速公路干线](../Page/高速公路.md "wikilink")。其全线均为[中国国家高速公路网](../Page/中国国家高速公路网.md "wikilink")[G50国道的组成部分](../Page/沪渝高速公路.md "wikilink")。

**沪苏浙皖高速公路**起自[上海外环沪青平立交](../Page/上海.md "wikilink")，经[上海](../Page/上海.md "wikilink")、[江苏](../Page/江苏.md "wikilink")、[浙江](../Page/浙江.md "wikilink")、[安徽](../Page/安徽.md "wikilink")，止于安徽[宣城市](../Page/宣城市.md "wikilink")，全长260公里。

  - 上海段，称为[A9公路](../Page/沪青平高速公路.md "wikilink")，亦称[沪青平高速公路](../Page/沪青平高速公路.md "wikilink")，起自外环沪青平立交，止于[青浦区](../Page/青浦区.md "wikilink")[金泽镇与](../Page/金泽镇.md "wikilink")[江苏省](../Page/江苏省.md "wikilink")[苏州市](../Page/苏州市.md "wikilink")[吴江](../Page/吴江.md "wikilink")[芦墟镇交会的省界](../Page/芦墟镇.md "wikilink")，全长约49公里。
  - 江苏段，称为[沪苏浙高速公路或](../Page/沪苏浙高速公路.md "wikilink")[苏浙沪高速公路](../Page/苏浙沪高速公路.md "wikilink")，起自[上海市](../Page/上海市.md "wikilink")[青浦区](../Page/青浦区.md "wikilink")[金泽镇与](../Page/金泽镇.md "wikilink")[苏州市](../Page/苏州市.md "wikilink")[吴江](../Page/吴江.md "wikilink")[芦墟镇交会的省界](../Page/芦墟镇.md "wikilink")，终于[苏州市](../Page/苏州市.md "wikilink")[吴江](../Page/吴江.md "wikilink")[震泽镇与](../Page/震泽镇.md "wikilink")[浙江省](../Page/浙江省.md "wikilink")[湖州市](../Page/湖州市.md "wikilink")[南浔交会的省界](../Page/南浔.md "wikilink")，全长约50公里。
  - 浙江段，即称**申苏浙皖高速公路**，起自[江苏省](../Page/江苏省.md "wikilink")[苏州市](../Page/苏州市.md "wikilink")[吴江](../Page/吴江.md "wikilink")[震泽镇与](../Page/震泽镇.md "wikilink")[湖州市](../Page/湖州市.md "wikilink")[南浔交会的省界](../Page/南浔.md "wikilink")，终于[湖州市](../Page/湖州市.md "wikilink")[长兴县与](../Page/长兴县.md "wikilink")[安徽省](../Page/安徽省.md "wikilink")[宣城市](../Page/宣城市.md "wikilink")[广德县交会的省界](../Page/广德县.md "wikilink")，全长约88公里。
  - 安徽段，称为[宣广高速公路](../Page/宣广高速公路.md "wikilink"),起自[浙江省](../Page/浙江省.md "wikilink")[湖州市](../Page/湖州市.md "wikilink")[长兴县与](../Page/长兴县.md "wikilink")[宣城市](../Page/宣城市.md "wikilink")[广德县交会的省界](../Page/广德县.md "wikilink")，终于[宣城](../Page/宣城.md "wikilink")，全长约73公里。

## 参考资料

  - [申苏浙皖高速公路全线贯通](http://society.people.com.cn/GB/41158/6766615.html)

[G](../Category/浙江省高速公路.md "wikilink")
[G](../Category/上海市高速公路.md "wikilink")
[G](../Category/江苏省高速公路.md "wikilink")
[G](../Category/安徽省高速公路.md "wikilink")