**國興衛視**（[英文名稱](../Page/英文.md "wikilink")：Gold Sun
TV；英文簡稱：GSTV），是台灣專門提供[日本](../Page/日本.md "wikilink")[電視節目的](../Page/電視節目.md "wikilink")[電視頻道之一](../Page/電視頻道.md "wikilink")。

## 歷史

1992年，[國產實業集團成立](../Page/國產實業集團.md "wikilink")**國興傳播股份有限公司**經營國興衛視，英文名稱「Gold
Sun
TV」、英文簡稱「GSTV」，是台灣首家向日本各[電視台合法購買電視節目](../Page/電視台.md "wikilink")[公開播映權](../Page/公開播映權.md "wikilink")、租用[衛星播放的電視頻道](../Page/人造衛星.md "wikilink")，開播時商標採用國產實業集團商標。2003年7月，國興傳播被[媒體棧國際行銷事業股份有限公司](../Page/媒體棧國際行銷.md "wikilink")[收購合併](../Page/收購.md "wikilink")，國興衛視成為媒體棧國際行銷旗下頻道。2010年3月，國興衛視商標從國產實業集團商標改為「GS」，英文簡稱改為「GS」。

國興衛視所播映的日本電視節目以日本五大[商業](../Page/商業.md "wikilink")[電視網](../Page/電視網.md "wikilink")（[NNS](../Page/日本電視網協議會.md "wikilink")、[FNS](../Page/FNS.md "wikilink")、[ANN](../Page/全日本新聞網.md "wikilink")、[JNN](../Page/JNN.md "wikilink")、[TXN](../Page/TXN.md "wikilink")）的[綜藝節目與](../Page/綜藝節目.md "wikilink")[日劇為主](../Page/日本電視劇.md "wikilink")，加註[中文](../Page/中文.md "wikilink")[字幕後作全天候播放](../Page/字幕.md "wikilink")。尤其成功引入《[電視冠軍](../Page/電視冠軍.md "wikilink")》（已播畢），使《電視冠軍》成為台灣最長壽的日本綜藝節目。

2000年以後，[韓劇流行](../Page/韓國電視劇.md "wikilink")，業界競爭激烈，加上日劇能直接通過其他途徑（如[網際網路](../Page/網際網路.md "wikilink")）傳播的關係，致國興衛視播出的日劇[收視率不濟](../Page/收視率.md "wikilink")。2005年11月中旬，國興衛視暫停日劇時段，轉而開始製作以日本來台發展的藝人主持的介紹台灣的節目，如《WU
MAI
亞洲NO.1》等。2007年1月起增加[韓國電視綜藝節目](../Page/韓國.md "wikilink")（以原音中文字幕播出），2007年3月以「亞洲劇星劇場」名義播放韓劇。目前播放節目，以日本綜藝節目、日本戲劇及日本動畫為主。

2017年1月初，啟用高畫質版本「**國興衛視**」。

## 播出節目

以下時間以當地時間（[台灣時間](../Page/國家標準時間.md "wikilink")）為準。

### 播出中

#### 綜藝

（[日語原音](../Page/日語.md "wikilink")/[中文字幕](../Page/中文.md "wikilink")）

<table>
<thead>
<tr class="header">
<th><p>節目名稱</p></th>
<th><p>播出時間</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><center>
<p><a href="../Page/NHK電視台.md" title="wikilink">NHK電視台</a>/《》</p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p><a href="../Page/三重電視台.md" title="wikilink">三重電視台</a>/《》</p></td>
<td><p>週日 19：00</p></td>
</tr>
<tr class="odd">
<td><center>
<p>《<a href="../Page/來去北海道特集.md" title="wikilink">阿多仔fun日本</a>》</p></td>
<td><p>週日 21：00</p></td>
</tr>
<tr class="even">
<td><center>
<p>《<a href="../Page/來去北海道特集.md" title="wikilink">鐵馬自由行</a>》</p></td>
<td><p>週六至日 21：00</p></td>
</tr>
<tr class="odd">
<td><center>
<p><a href="../Page/北海道電視台.md" title="wikilink">北海道電視台</a>《》</p></td>
<td><p>週五19：00（首播）、22：00</p></td>
</tr>
<tr class="even">
<td><center>
<p><a href="../Page/北海道放送.md" title="wikilink">北海道放送</a>《》</p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>《<a href="../Page/來去鄉下吃好料.md" title="wikilink">來去鄉下吃好料</a>》</p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p><a href="../Page/朝日放送.md" title="wikilink">朝日放送</a>《<a href="../Page/全民家庭醫學.md" title="wikilink">全民家庭醫學</a>》</p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p><a href="../Page/岡山放送.md" title="wikilink">岡山放送</a>《》</p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>《<a href="../Page/電車逍遙遊.md" title="wikilink">電車逍遙遊</a>》</p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>《<a href="../Page/難以置信!這家店有多好吃.md" title="wikilink">難以置信!這家店有多好吃</a>》</p></td>
<td><p>週一至五 18：00</p></td>
</tr>
<tr class="even">
<td><center>
<p><a href="../Page/朝日放送.md" title="wikilink">朝日放送</a>《》</p></td>
<td><p>週五 20：00<br />
週六 17：00</p></td>
</tr>
<tr class="odd">
<td><center>
<p>《<a href="../Page/鹿兒島嬉遊記.md" title="wikilink">鹿兒島嬉遊記</a>》</p></td>
<td><p>週一至四 19：00</p>
<p>週二至五 10：00</p></td>
</tr>
<tr class="even">
<td><center>
<p>《<a href="../Page/修造成長學園.md" title="wikilink">修造成長學園</a>》</p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p>《<a href="../Page/來去北海道特集.md" title="wikilink">來去北海道特集</a>》</p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p>《》</p></td>
<td></td>
</tr>
<tr class="odd">
<td><center>
<p><a href="../Page/札幌電視台.md" title="wikilink">札幌電視台</a>《》</p></td>
<td></td>
</tr>
<tr class="even">
<td><center>
<p><a href="../Page/東京電視台.md" title="wikilink">東京電視台</a>《<a href="../Page/搶救貧窮大作戰.md" title="wikilink">搶救貧窮大作戰</a>》</p></td>
<td></td>
</tr>
</tbody>
</table>

## 曾播過的日劇

### 日劇

| 電視劇名稱                                                                                                                                                                           |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [NHK電視台](../Page/NHK电视台.md "wikilink")《》（）                                                                                                                                      |
| [富士電視台](../Page/富士電視台.md "wikilink")《》（）                                                                                                                                        |
| [NHK電視台](../Page/NHK电视台.md "wikilink")《》（）                                                                                                                                      |
| [NHK電視台](../Page/NHK电视台.md "wikilink")《》（）                                                                                                                                      |
| [NHK電視台](../Page/NHK电视台.md "wikilink")《明天也是好天氣》（）                                                                                                                               |
| [朝日電視台](../Page/朝日電視台.md "wikilink")《》（）                                                                                                                                        |
| [NHK電視台](../Page/NHK电视台.md "wikilink")《》（）                                                                                                                                      |
| [富士電視台](../Page/富士電視台.md "wikilink")《》（）                                                                                                                                        |
| [NHK電視台](../Page/NHK电视台.md "wikilink")《》（）                                                                                                                                      |
| [TBS電視台](../Page/TBS電視台.md "wikilink")《》（）                                                                                                                                      |
| [東海電視台](../Page/東海電視台.md "wikilink")《》（）                                                                                                                                        |
| [讀賣電視台](../Page/讀賣電視台.md "wikilink")《》（）                                                                                                                                        |
| [NHK電視台](../Page/NHK电视台.md "wikilink")《刑事們的夏天》（）                                                                                                                                |
| [TBS電視台](../Page/TBS電視台.md "wikilink")《》（）                                                                                                                                      |
| [富士電視台](../Page/富士電視台.md "wikilink")《[處女之路](../Page/處女之路_\(電視劇\).md "wikilink")》（）                                                                                              |
| [富士電視台](../Page/富士電視台.md "wikilink")《[戀戀情深](../Page/戀戀情深_\(電視劇\).md "wikilink")》（）                                                                                              |
| [讀賣電視台](../Page/讀賣電視台.md "wikilink")《》（）                                                                                                                                        |
| [關西電視台](../Page/關西電視台.md "wikilink")《》（）                                                                                                                                        |
| [富士電視台](../Page/富士電視台.md "wikilink")《[熱烈的中華飯店](../Page/熱烈的中華飯店.md "wikilink")》（）                                                                                                |
| 朝日電視台《》（）                                                                                                                                                                       |
| 朝日電視台《[黑色筆記本](../Page/黑色筆記本.md "wikilink")》（）                                                                                                                                   |
| [富士電視台](../Page/富士電視台.md "wikilink")《》（）                                                                                                                                        |
| [富士電視台](../Page/富士電視台.md "wikilink")《[奉子成婚](../Page/奉子成婚_\(电视剧\).md "wikilink")》（）                                                                                              |
| [富士電視台](../Page/富士電視台.md "wikilink")《》（）                                                                                                                                        |
| [TBS電視台](../Page/TBS電視台.md "wikilink")《[樂透家族](../Page/樂透家族.md "wikilink")》（）                                                                                                    |
| [富士電視台](../Page/富士電視台.md "wikilink")《》（）                                                                                                                                        |
| [TBS電視台](../Page/TBS電視台.md "wikilink")《[暴風雨之戀](../Page/暴风雨之恋.md "wikilink")》（）                                                                                                  |
| [富士電視台](../Page/富士電視台.md "wikilink")《[午餐女王](../Page/午餐女王.md "wikilink")》（）                                                                                                      |
| [讀賣電視台](../Page/讀賣電視台.md "wikilink")《》（）                                                                                                                                        |
| [TBS電視台](../Page/TBS電視台.md "wikilink")《》（）                                                                                                                                      |
| 朝日電視台《[黑色推銷員](../Page/黑色推銷員.md "wikilink")》（）                                                                                                                                   |
| [富士電視台](../Page/富士電視台.md "wikilink")《》（）                                                                                                                                        |
| [讀賣電視台](../Page/讀賣電視台.md "wikilink")《》（）                                                                                                                                        |
| [讀賣電視台](../Page/讀賣電視台.md "wikilink")《》（）                                                                                                                                        |
| [TBS電視台](../Page/TBS電視台.md "wikilink")《》（）                                                                                                                                      |
| [TBS電視台](../Page/TBS電視台.md "wikilink")《[新高校教師](../Page/高校教師#2003年的高校教師.md "wikilink")》（）                                                                                        |
| [富士電視台](../Page/富士電視台.md "wikilink")《》（）                                                                                                                                        |
| [富士電視台](../Page/富士電視台.md "wikilink")《[東京仙履奇緣](../Page/東京仙履奇緣.md "wikilink")》（）                                                                                                  |
| [TBS電視台](../Page/TBS電視台.md "wikilink")《[冷暖人間](../Page/冷暖人間.md "wikilink")》（）（2004年播映權轉移後由[八大電視播映](../Page/八大電視.md "wikilink")，2007年播映權再轉移至[緯來戲劇台](../Page/緯來戲劇台.md "wikilink")） |
| [讀賣電視台](../Page/讀賣電視台.md "wikilink")《》（）                                                                                                                                        |
| TBS電視台《》（）                                                                                                                                                                      |
| [關西電視台](../Page/關西電視台.md "wikilink")《[獻花給倉鼠](../Page/獻給阿爾吉儂的花束.md "wikilink")》（）                                                                                                |
| [中部日本放送](../Page/中部日本放送.md "wikilink")《[孩子們的戰爭](../Page/孩子們的戰爭.md "wikilink")》（）                                                                                                |
| TBS電視台《[三年B班金八老師](../Page/3年B組金八先生.md "wikilink")》（）                                                                                                                            |
| 朝日電視台《》（）（2007年12月21日起逢週五晚間9時播出，播至2008年2月15日，共8集）                                                                                                                               |
| [朝日電視台](../Page/朝日電視台.md "wikilink")《》（）                                                                                                                                        |
| [關西電視台](../Page/關西電視台.md "wikilink")《[向牛許願 Love\&Farm](../Page/向牛許願.md "wikilink")》（）                                                                                           |
| [朝日放送](../Page/朝日放送.md "wikilink")《》（）                                                                                                                                          |
| 朝日電視台《[天國與地獄](../Page/天國與地獄_\(電視\).md "wikilink")》（）                                                                                                                            |
| 朝日電視台《[未來講師](../Page/未來講師.md "wikilink")》（）                                                                                                                                     |
| TBS電視台《[魔女的條件](../Page/魔女的條件.md "wikilink")》（）                                                                                                                                  |
| TBS電視台《[美麗人生](../Page/美麗人生_\(日本電視劇\).md "wikilink")》（）                                                                                                                          |
| 中部日本放送《[幽婚奇緣](../Page/幽婚奇緣.md "wikilink")》（）                                                                                                                                    |
| [琉球放送](../Page/琉球放送.md "wikilink")《[沖繩恐怖怪談](../Page/沖繩恐怖怪談.md "wikilink")》（）                                                                                                    |
| 中部日本放送《[女人30最犀利](../Page/女人30最犀利.md "wikilink")》（）                                                                                                                              |
| 中部日本放送《[未婚六姐妹](../Page/未婚六姐妹.md "wikilink")》（）                                                                                                                                  |
| 神奈川電視台《[親愛的貝斯](../Page/親愛的貝斯.md "wikilink")》（）                                                                                                                                  |
| 朝日電視台《[刑警110公斤](../Page/刑警110公斤.md "wikilink")》（）                                                                                                                               |
| 中部日本放送《[月花女將的生存之道](../Page/月花女將的生存之道.md "wikilink")》（）                                                                                                                          |
| 神奈川電視台《》（）                                                                                                                                                                      |
| 中部日本放送《[新孩子們的戰爭](../Page/新孩子們的戰爭.md "wikilink")》（）                                                                                                                              |
| [東京電視台](../Page/東京電視台.md "wikilink")《[美食不孤單](../Page/孤獨的美食家.md "wikilink")》（）                                                                                                   |
| 神奈川電視台《》（）                                                                                                                                                                      |
| 神奈川電視台《》（）                                                                                                                                                                      |
| 富士電視台《[王牌搜查官](../Page/王牌搜查官.md "wikilink")》（）                                                                                                                                   |
| 中部日本放送《[紳士大主廚](../Page/紳士大主廚.md "wikilink")》（）                                                                                                                                  |
| 神奈川電視台《[貓咪計-{程}-車](../Page/貓咪計程車.md "wikilink")》（）                                                                                                                              |
| [NHK電視台](../Page/NHK电视台.md "wikilink")《[深夜中的麵包店](../Page/深夜中的麵包店.md "wikilink")》（）                                                                                              |
| NHK電視台《[玻璃之家](../Page/玻璃之家.md "wikilink")》（）                                                                                                                                    |
| 朝日放送《[王牌公關 公平桑](../Page/王牌男公關.md "wikilink")》（）                                                                                                                                 |
| [西日本電視台](../Page/西日本電視台.md "wikilink")《[明太子夫婦](../Page/明太子夫婦.md "wikilink")》（）                                                                                                  |
| 神奈川電視台《[黑貓露西](../Page/黑貓露西.md "wikilink")》（）                                                                                                                                    |
| 東京電視台《[道士美少女](../Page/道士美少女.md "wikilink")》（）                                                                                                                                   |
| 中部日本放送《[向月亮祈禱](../Page/向月亮祈禱.md "wikilink")》（）                                                                                                                                  |
| [福岡放送](../Page/福岡放送.md "wikilink")《[拔出一片天](../Page/拔出一片天.md "wikilink")》（）                                                                                                      |
| [NOTTV](../Page/NOTTV.md "wikilink")《[女人的規則](../Page/女人的規則.md "wikilink")》（）                                                                                                    |
| 中部日本放送《[美好人生奮鬥記](../Page/美好人生奮鬥記.md "wikilink")》（{{lang|ja|キッパリ\! / キッパリ                                                                                                        |
| 東京電視台《[三匹歐吉桑](../Page/三匹歐吉桑.md "wikilink")》（）                                                                                                                                   |
| 神奈川電視台《[貓咪心動奇蹟](../Page/貓咪心動奇蹟.md "wikilink")》（）                                                                                                                                |
| 神奈川電視台《[愛在貓咪療癒時](../Page/愛在貓咪療癒時.md "wikilink")》（）                                                                                                                              |
| [NHK電視台](../Page/NHK電視台.md "wikilink")/《[深夜中的麵包店](../Page/深夜中的麵包店.md "wikilink")》                                                                                               |

### 韓劇

| 電視劇名稱                                                                                 |
| ------------------------------------------------------------------------------------- |
| [KBS電視台](../Page/KBS.md "wikilink")《[愛情中毒](../Page/愛情中毒.md "wikilink")》（러브홀릭）         |
| [KBS電視台](../Page/KBS.md "wikilink")《[跨越時空的戀人](../Page/她回來了.md "wikilink")》（그녀가 돌아왔다）  |
| [SBS電視台](../Page/SBS.md "wikilink")《[藍色愛爾蘭](../Page/愛爾蘭_\(電視劇\).md "wikilink")》（아일랜드） |
| [MBC電視台](../Page/MBC.md "wikilink")《[來生](../Page/还生-NEXT.md "wikilink")》（환생-넥스트）      |

### 綜藝節目

#### 外購

| 節目名稱                                                                                                                                |
| ----------------------------------------------------------------------------------------------------------------------------------- |
| [朝日電視台](../Page/朝日電視台.md "wikilink")《[黃金傳說](../Page/黃金傳說.md "wikilink")》（）                                                          |
| [富士電視台](../Page/富士電視台.md "wikilink")《[戀愛巴士](../Page/戀愛巴士.md "wikilink")》（）                                                          |
| 東京電視台《[電視冠軍](../Page/電視冠軍.md "wikilink")》（）                                                                                         |
| [中京電視台](../Page/中京電視台.md "wikilink")《》（）                                                                                            |
| 中京電視台《》（）                                                                                                                           |
| 中京電視台《》（）                                                                                                                           |
| 東京電視台《[新早安少女組](../Page/Hello!_Morning.md "wikilink")》（）                                                                             |
| 朝日電視台《》（）                                                                                                                           |
| 東京電視台《[稀世珍寶 開運鑑定團](../Page/稀世珍寶_開運鑑定團.md "wikilink")》（）                                                                             |
| 朝日放送《[最終警告！恐怖的家庭醫學](../Page/最終警告！恐怖的家庭醫學.md "wikilink")》（）                                                                          |
| 東京電視台《》（ASAYAN）                                                                                                                     |
| 富士電視台《[堂本兄弟](../Page/堂本兄弟.md "wikilink")》 （）                                                                                        |
| 富士電視台《》（VivaVivaV6）                                                                                                                 |
| 朝日電視台《》（）                                                                                                                           |
| [NHK](../Page/日本放送協會.md "wikilink")《》（）                                                                                             |
| 東京電視台《》（）                                                                                                                           |
| 東京電視台《》（）                                                                                                                           |
| 朝日電視台《[珍奇百景進香團](../Page/珍奇百景進香團.md "wikilink")》（）                                                                                   |
| 東京電視台《》（）                                                                                                                           |
| NHK《》（）                                                                                                                             |
| NHK《》（）                                                                                                                             |
| 朝日電視台《志村健美食旅遊精選》《加藤茶美食旅遊精選》                                                                                                         |
| [北海道電視台](../Page/北海道電視台.md "wikilink")《》（）                                                                                          |
| [TBS電視台](../Page/TBS電視.md "wikilink")《2010[極限體能王](../Page/極限體能王SASUKE.md "wikilink")》（SASUKE）（原[JET之節目](../Page/JET.md "wikilink")） |
| NHK《》（）                                                                                                                             |
| 日本電視台《[超級變變變](../Page/超級變變變.md "wikilink")》（）                                                                                       |
| [朝日放送](../Page/朝日放送.md "wikilink")《[超級全能住宅改造王](../Page/超級全能住宅改造王.md "wikilink")》（{{lang|ja|大改造                                      |
| [每日放送](../Page/每日放送.md "wikilink")《》（）                                                                                              |
| 日本電視台《[狗狗猩猩大冒險](../Page/狗狗猩猩大冒險.md "wikilink")》（）（原JET之節目）                                                                          |
| 東京電視台《》（）                                                                                                                           |
| 朝日電視台《》（）                                                                                                                           |
| [BS日本](../Page/BS日本.md "wikilink")《[寵物當家之旅](../Page/寵物當家之旅.md "wikilink")》（{{lang|ja|だいすけ君が行く                                       |
| 日本電視台《[DERO密室遊戲大脫逃](../Page/DERO密室遊戲大脫逃.md "wikilink")》（）                                                                           |
| 朝日電視台《{{link-ja|SMAP綜藝特集|SMAP☆がんばりますっ                                                                                               |
| [札幌電視台](../Page/札幌電視台.md "wikilink")《》（）                                                                                            |
| 朝日電視台《[珍奇百景鑑定團](../Page/珍奇百景鑑定團.md "wikilink")》（）                                                                                   |
| 朝日放送《[全民家庭醫學](../Page/全民家庭醫學.md "wikilink")》（）                                                                                      |
| 富士電視台《》 （）                                                                                                                          |
| 朝日電視台《》 （）(不定期播出；原JET之節目)                                                                                                           |
| 富士電視台《》 （）                                                                                                                          |
| 東京電視台《》 （）                                                                                                                          |
| [東北放送](../Page/東北放送.md "wikilink")《》 （）                                                                                             |
| 日本電視台《[TORE全力大挑戰](../Page/TORE全力大挑戰.md "wikilink")》（）                                                                               |
| 東京電視台《》 （）                                                                                                                          |
| [Family劇場](../Page/Family劇場.md "wikilink")《[AKB48神TV大挑戰](../Page/AKB48神TV.md "wikilink")》 （）                                        |
| 朝日放送《》 （）                                                                                                                           |
| 富士電視台《[五花八門眾議院](../Page/五花八門眾議院.md "wikilink")》 （）                                                                                  |
| [琉球放送](../Page/琉球放送.md "wikilink")《》（<span lang="ja">沖縄BON\!</span>）                                                                |
| 東京電視台《》 （）                                                                                                                          |
| [鹿兒島電視台](../Page/鹿兒島電視台.md "wikilink")《》（TEGE2）                                                                                     |
| [BS JAPAN](../Page/BS_JAPAN.md "wikilink")《》 （）                                                                                     |
| [BS JAPAN](../Page/BS_JAPAN.md "wikilink")《》 （）                                                                                     |
| [日本電視台](../Page/日本電視台.md "wikilink")《》 （）                                                                                           |
| 富士電視台《》 （）                                                                                                                          |
| [鹿兒島電視台](../Page/鹿兒島電視台.md "wikilink")《》 （）                                                                                         |
| 朝日電視台《[王牌綜藝同樂會](../Page/王牌綜藝同樂會.md "wikilink")》 （）                                                                                  |
| 北海道電視台《》 （）（原[緯來日本台外購之節目](../Page/緯來日本台.md "wikilink")）                                                                             |
| [福岡放送](../Page/福岡放送.md "wikilink")《》 （）                                                                                             |
| [ICS](../Page/上海外語頻道.md "wikilink")《[我愛北海道](../Page/戀上北海道.md "wikilink")》 （、）                                                       |
| 朝日放送《》 （）                                                                                                                           |
| 日本電視台《》 （）                                                                                                                          |
| 東京電視台《[使命必達！爸爸我來了](../Page/使命必達！爸爸我來了.md "wikilink")》 （）                                                                            |
| 日本電視台《》 （）                                                                                                                          |
| 日本電視台《》 （）                                                                                                                          |
| [宮崎放送](../Page/宮崎放送.md "wikilink")《》 （）                                                                                             |
| 東京電視台《[日本！我來了](../Page/日本！我來了.md "wikilink")》 （）                                                                                    |
| 中京電視台《》 （）                                                                                                                          |
| 朝日電視台《》 （）                                                                                                                          |
| 朝日放送《》 （）                                                                                                                           |
| 朝日電視台《》 （）                                                                                                                          |
| 日本電視台《{{link-ja|我家好宅大公開|スッキリ                                                                                                        |
| 朝日放送《[關八冒險中](../Page/關八冒險中.md "wikilink")》 （）                                                                                       |
| [大阪電視台](../Page/大阪電視台.md "wikilink")《》 （）                                                                                           |
| [東京都會電視台](../Page/東京都會電視台.md "wikilink")《》 （）                                                                                       |
| 朝日電視台《》 （）                                                                                                                          |
| 《[理想美宅造訪中](../Page/理想美宅造訪中.md "wikilink")》                                                                                          |
| 《[我家完美改造物語](../Page/我家完美改造物語.md "wikilink")》                                                                                        |
| 北海道放送《[北海道王國](../Page/北海道王國.md "wikilink")》                                                                                         |
| 北海道放送《[北海道物產](../Page/北海道物產.md "wikilink")》                                                                                         |
| [朝日電視台](../Page/朝日電視台.md "wikilink")《[黃金傳說](../Page/黃金傳說.md "wikilink")》                                                            |
| 朝日放送《[超級全能住宅改造王](../Page/超級全能住宅改造王.md "wikilink")》                                                                                  |
| [BS JAPAN](../Page/BS_JAPAN.md "wikilink")《[寵物當家之旅](../Page/寵物當家之旅.md "wikilink")》                                                  |
| 《[無所不教！全民大學校](../Page/無所不教！全民大學校.md "wikilink")》                                                                                    |

#### 自製

| 節目名稱                                                                                                                |
| ------------------------------------------------------------------------------------------------------------------- |
| 《[新瀨上剛in台灣](../Page/新瀨上剛in台灣.md "wikilink")》（原JET之節目《瀨上剛in台灣》）                                                      |
| 《[大口吃遍台灣](../Page/大口吃遍台灣.md "wikilink")》（）（原JET之節目，在[日本電視台旗下的頻道也有播出](../Page/日本電視台.md "wikilink")）                  |
| 《[幸福空間](../Page/幸福空間.md "wikilink")》（原[東森超視之節目](../Page/東森超視.md "wikilink")）(現與[中視共播之節目](../Page/中視.md "wikilink")) |
| 《[明潮時尚](../Page/明潮時尚.md "wikilink")》(原《[明周時尚](../Page/明周時尚.md "wikilink")》)（原[東森超視之節目](../Page/東森超視.md "wikilink")） |
| 《[我的閨房秘蜜](../Page/我的閨房秘蜜.md "wikilink")》                                                                            |

### 卡通

| 卡通名稱                                            |
| ----------------------------------------------- |
| 《[極道鮮師](../Page/極道鮮師.md "wikilink")》（）          |
| 《[花田少年史](../Page/花田少年史.md "wikilink")》（）        |
| 《[清秀佳人](../Page/清秀佳人_\(動畫\).md "wikilink")》（）   |
| 《[小英的故事](../Page/小英的故事.md "wikilink")》（）        |
| 《[靈犬雪麗](../Page/靈犬雪麗.md "wikilink")》（）          |
| 《[小浣熊](../Page/浣熊拉斯卡爾.md "wikilink")》（）         |
| 《》（）                                            |
| 《[小神童](../Page/騎鵝歷險記.md "wikilink")》（）          |
| 《[妙手小廚師](../Page/妙手小廚師.md "wikilink")》（）        |
| 《[天方夜譚](../Page/天方夜譚_\(電視動畫\).md "wikilink")》（） |
| 《[天才小釣手](../Page/天才小釣手.md "wikilink")》（）        |
| 《[桃太郎傳說](../Page/桃太郎傳說.md "wikilink")》（）        |
| 《[新·桃太郎傳說](../Page/桃太郎傳說.md "wikilink")》（）      |
| 《[漫畫 水戶黃門](../Page/漫畫_水戶黃門.md "wikilink")》（）    |
| 《[小王子](../Page/小王子_\(小说\).md "wikilink")》（）     |
| 《[格林童話](../Page/格林名作劇場.md "wikilink")》（）        |
| 《[咪咪流浪记](../Page/咪咪流浪记.md "wikilink")》（）        |

### 特攝

| 卡通名稱                                                                             |
| -------------------------------------------------------------------------------- |
| [富士電視台](../Page/富士電視台.md "wikilink")《[快傑獅子丸](../Page/快杰狮子丸.md "wikilink")》（）     |
| [富士電視台](../Page/富士電視台.md "wikilink")《[風雲獅子丸](../Page/风云狮子丸.md "wikilink")》（）     |
| [富士電視台](../Page/富士電視台.md "wikilink")《[戰鬥電人查勃卡](../Page/戰鬥電人查勃卡.md "wikilink")》（） |

### 電影

| 電影名稱                                             |
| ------------------------------------------------ |
| 《[吉貓出租](../Page/吉貓出租.md "wikilink")》（）           |
| 《[獵戶座的散場電影](../Page/獵戶座的散場電影.md "wikilink")》（）   |
| 《[Tokyo Kid](../Page/Tokyo_Kid.md "wikilink")》（） |

## 相關條目

  - [JET綜合台](../Page/JET綜合台.md "wikilink")
  - [國產實業集團](../Page/國產實業集團.md "wikilink")
  - [臺灣電視台列表](../Page/臺灣電視台列表.md "wikilink")

## 參考資料

  - [《從日本綜藝節目看日本文化》](http://www.fjweb.fju.edu.tw/fbj/reading_nccu/report/91_2/91-2bangumi.pdf)

## 外部連結

  - [國興衛視](http://www.goldsuntv.com.tw/)

  -
[G國](../Category/台灣電視台.md "wikilink")
[G國](../Category/台灣電視播放頻道.md "wikilink")
[G國](../Category/國產實業集團.md "wikilink")
[G國](../Category/總部位於臺北市內湖區的工商業機構.md "wikilink")