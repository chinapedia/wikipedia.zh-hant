**大话西游BBS**是中国[教育网上两个历史比较悠久的](../Page/教育网.md "wikilink")[BBS站](../Page/BBS.md "wikilink")。
其中[北京大学的大话西游BBS建立于](../Page/北京大学.md "wikilink")1999年3月，简称**DHXY**。[清华大学的大话西游BBS](../Page/清华大学.md "wikilink")（又名ZIXIA
BBS）创建于1999年9月，简称**ZIXIA**。

## 大话西游(DHXY)站

原本是北京大学学生宿舍自建局域网内的BBS，主要覆盖了[北京大学](../Page/北京大学.md "wikilink")32楼,35楼,31楼三个本科宿舍楼。由于为当时无法联入教育网的同学提供了方便的交流平台和多种网络服务，成为当时[北大校内最有活力和影响力的BBS之一](../Page/北大.md "wikilink")。

### 简史

  - 1998年 北京大学97级男生宿舍32楼计算机系建成局域网
  - 1999年3月21日 Richard在自己的电脑上架设Firebird BBS，命名为**大话西游BBS**。
  - 1999年7月
    在al和leo的组织下，由学生自发组织、自筹资金、自行设计并施工的局域网改造工程开始。局域网由总线结构改为以交换机为中心的星型结构，联网电脑从十多台扩充到近百台。地理范围也扩展到整个32楼并联通了相邻的35楼（大部分98级和部分97级女生宿舍）。服务器也更新为专职的赛扬300A电脑。至此，大话西游BBS的主要用户群已经形成。
  - 2000年3月 自架网络继续扩展到31楼女生宿舍。
  - 2001年初
    北大宿舍接入校园网，大话西游BBS也联入了教育网，使用域名dhxy.dhs.org。经站友讨论确定了稳定内部圈子，不对外扩张的发展方向。
  - 2006年初 改用域名dhxy.info

### 外部链接

  - [大话西游BBS(DHXY)](http://dhxy.info)

## 大话西游(ZIXIA)站

[Bbs.zixia.net.2005-03-19.gif](https://zh.wikipedia.org/wiki/File:Bbs.zixia.net.2005-03-19.gif "fig:Bbs.zixia.net.2005-03-19.gif")
创建于1999年9月17日，是一个完全民间性质的[BBS站点](../Page/BBS.md "wikilink")，是[清华大学非常具有影响力的一个BBS](../Page/清华大学.md "wikilink")，是[教育网内平均在线人数较多的BBS](../Page/教育网.md "wikilink")。在其管理理念的影响下，BBS上的[黑帮管理风格较大](../Page/黑帮.md "wikilink")，[精英政治氛围较浓](../Page/精英政治.md "wikilink")，受此影响，BBS又称为[斧头帮](../Page/斧头帮.md "wikilink")。

### 简史

  - 1999年由[清华大学机械系本科生](../Page/清华大学.md "wikilink")[李卓桓](../Page/李卓桓.md "wikilink")([zixia](../Page/zixia.md "wikilink"))架设，采用FireBird2.6系统，命名为[机械志途](../Page/机械志途.md "wikilink")，为清华大学28号宿舍楼提供楼内服务。服务器使用一台486-40Mhz主频的淘汰电脑；在2001年，服务器在清华大学28号楼内的电机系机房内丢失，未能够找到；
  - 2001年1月6日，采用[水木清华BBS系统重新架设](../Page/水木清华BBS.md "wikilink")，更名为**大话西游BBS**，主要面向于清华校内，域名为bbs.zixia.net。后来逐渐发展为两大主题：[DiabloII网络游戏与](../Page/DiabloII.md "wikilink")[出国留学社区](../Page/出国留学.md "wikilink")。

### 社群文化

斧头帮

### 特色版面

#### DiabloII

有13个DiabloII相关版面，是中国教育网内最大的D2讨论区。

`DiabloII 暗黑天堂 `
`D2Server 大菠萝服务器`
`D2Auction 暗黑拍卖`
`D2HC 暗黑英雄`
`D2hell 暗黑地狱     `
`D2mod 暗黑异次元    `
`D2PKUHC 暗黑北大HC版`
`D2PKUREALM 暗黑北大版`
`D2PKUtrade 暗黑北大版`

`D2Trade 暗黑交易 `
`D2Inside 技术原理`
`D2Item 物品装备`
`D2Strategy 角色战术`

#### GoAbroad

#### 硬件

大话西游BBS的机器设备与网络费用由[北京阿卡信息技术有限公司赞助](../Page/北京阿卡信息技术有限公司.md "wikilink")。

### 参见

  - [BBS](../Page/BBS.md "wikilink")

### 外部链接

  - [大话西游BBS站点(ZIXIA)](http://wforum.zixia.net)

[Category:中国大学BBS](../Category/中国大学BBS.md "wikilink")
[Category:北京大学](../Category/北京大学.md "wikilink")
[Category:清华大学](../Category/清华大学.md "wikilink")