**奥斯伍尔德·西奥多·埃弗里**（，），[美国](../Page/美国.md "wikilink")[医生](../Page/医生.md "wikilink")、最早的[分子生物学家之一](../Page/分子生物学.md "wikilink")、[免疫化学先驱](../Page/免疫化学.md "wikilink")，曾长期在[纽约市](../Page/纽约市.md "wikilink")[洛克菲勒研究院附属医院工作](../Page/洛克菲勒研究院.md "wikilink")。埃弗里最大成就是1944年与[科林·麦克劳德和](../Page/科林·麦克劳德.md "wikilink")[麦克林恩·麦卡蒂共同发现](../Page/麦克林恩·麦卡蒂.md "wikilink")[脱氧核糖核酸](../Page/脱氧核糖核酸.md "wikilink")（[DNA](../Page/DNA.md "wikilink")）是[染色体的主要成分及构成](../Page/染色体.md "wikilink")[基因的主要材料](../Page/基因.md "wikilink")。

## 生平

1877年生於[加拿大](../Page/加拿大.md "wikilink")[新斯科舍省](../Page/新斯科舍省.md "wikilink")[哈利法克斯](../Page/哈利法克斯_\(新斯科舍省\).md "wikilink")。

1955年逝世於[田纳西州](../Page/田纳西州.md "wikilink")[纳什维尔](../Page/纳什维尔_\(田纳西州\).md "wikilink")。

## 荣誉

为了纪念他的贡献，[月球上的一个环形山被命名为](../Page/月球.md "wikilink")[埃弗里](../Page/埃弗里_\(环形山\).md "wikilink")。

[Category:美國生物學家](../Category/美國生物學家.md "wikilink")
[Category:美国醫學家](../Category/美国醫學家.md "wikilink")
[Category:加拿大醫學家](../Category/加拿大醫學家.md "wikilink")
[Category:哥倫比亞大學校友](../Category/哥倫比亞大學校友.md "wikilink")
[Category:新斯科舍省人](../Category/新斯科舍省人.md "wikilink")
[Category:拉斯克基礎醫學研究獎得主](../Category/拉斯克基礎醫學研究獎得主.md "wikilink")
[Category:科普利獎章獲得者](../Category/科普利獎章獲得者.md "wikilink")