**库尔德斯坦旗帜**（[库尔德语](../Page/库尔德语.md "wikilink"): **Alay
Kurdistan**或**Alaya Kurdistanê**，\[1\] **ئاڵای کوردستان**，也叫**Alaya
Rengîn**“彩旗”），最早出现于[奥斯曼帝国时期的](../Page/奥斯曼帝国.md "wikilink")[库尔德人独立运动](../Page/库尔德人.md "wikilink")。据说它是1900年代由库尔德民族主义组织Xoybûn（Khoyboon，自我）设计的。\[2\]该旗帜的一个早期版本曾在1927年-1931年[土耳其的阿拉拉特共和国独立时使用](../Page/土耳其.md "wikilink")。在随后的1946年，苏联扶植的库尔德人共和国[马哈巴德共和国使用了一面类似的旗帜](../Page/马哈巴德共和国.md "wikilink")。目前，它被用作由[库尔德人自治的](../Page/库尔德斯坦.md "wikilink")[伊拉克库尔德斯坦地区的官方旗帜](../Page/伊拉克库尔德斯坦.md "wikilink")。但该旗在[土耳其及](../Page/土耳其.md "wikilink")[伊朗和](../Page/伊朗.md "wikilink")[叙利亚被禁止](../Page/叙利亚.md "wikilink")。\[3\]\[4\]

这面旗帜中最主要的库尔德元素是中央那个耀眼的金色太阳，它是库尔德人的一个古老的文化和宗教符号，在拜火教中与火的含义相近，是智慧的象征。这个太阳共辐射出21道大小形状都相同的光芒，21在库尔德人[雅兹迪族群的古老信仰传统中具有重要的意义](../Page/雅兹迪.md "wikilink")。\[5\]

旗帜各个颜色的含义：

  - 红色代表了库尔德斯坦烈士的鲜血与库尔德斯坦和她的人民为自由与尊严所作的不懈的斗争。
  - 绿色代表了库尔德斯坦的风景地貌和美丽。
  - 白色表示生活在库尔德斯坦的人们的和平、平等与自由，无论其种族和信仰。
  - 黄色代表了生命的起源和人类的光明，同时太阳也代表了库尔德人的古老雅兹迪信仰。\[6\]

## 库尔德人使用过的旗帜

<File:Flag> of the Emirate of Soran.svg|索兰酋长国旗帜，\[7\] 1816 - 1835
<File:Flag> of Kingdom of Kurdistan
(1922-1924).svg|[库尔德斯坦王国旗帜](../Page/库尔德斯坦王国.md "wikilink")，\[8\]
1922 - 1924 <File:Flag> of the Republic of Ararat.svg|阿拉拉特共和国旗帜，\[9\]
1927-1930 <File:Flag> of the Republic of
Mahabad.svg|[马哈巴德共和国旗帜](../Page/马哈巴德共和国.md "wikilink")，1946–1947
<File:Flag> of Syrian
Kurdistan.svg|[叙利亚内战期间](../Page/叙利亚内战.md "wikilink")[叙利亚库尔德斯坦使用的一面旗帜](../Page/叙利亚库尔德斯坦.md "wikilink")，2012

## 参考

[Category:库尔德斯坦](../Category/库尔德斯坦.md "wikilink")
[Category:伊拉克庫德斯坦](../Category/伊拉克庫德斯坦.md "wikilink")
[旗](../Category/庫德族.md "wikilink")

1.

2.  , Kurdish Institute of Paris.

3.
4.

5.

6.

7.  <http://www.crwflags.com/fotw/flags/krd_slvd.html>

8.
9.