**貝類學**（英語：**Conchology**）是一門專門研究[貝類的學科](../Page/貝類.md "wikilink")。貝類學在西方世界存在已久，早期的貝類學包括了單純的[貝殼收集](../Page/貝殼.md "wikilink")，直到[卡爾·林奈建立了](../Page/卡爾·林奈.md "wikilink")[生物分類方法之後](../Page/分類學_\(生物學\).md "wikilink")，才確立為[動物學的一支](../Page/動物學.md "wikilink")。

## 參見

  - [軟體動物學](../Page/軟體動物學.md "wikilink")

## 外部連結

  - [Conchology.be](http://www.conchology.be) The largest online
    resource for conchology with over 210000 pictures of seashells.
  - [Femorale Shells](http://www.femorale.com) Thousands of images,
    information and links related to shell collecting.
  - [The Bailey-Matthews Shell
    Museum](https://web.archive.org/web/20071119040433/http://coconet03.coconet.com/sanibel-captiva/bm_shell.html)
    in [Sanibel Island](../Page/Sanibel_Island.md "wikilink"), Florida,
    which is the only museum in the world dedicated entirely to shells.
  - [Conchologists of
    America](https://web.archive.org/web/20060206223251/http://www.conchologistsofamerica.org/home/)
  - [Conchological Society of Great Britain and
    Ireland](http://www.conchsoc.org/)
  - [Club Conchylia, the German Society for Shell
    Collecting](http://www.club-conchylia.de/)
  - [Belgian Society for Conchology](http://www.bvc-gloriamaris.be/)
  - [Worldwide Conchology](http://www.worldwideconchology.com/)
  - [ShellMonster, A large personal shell
    collection](http://shellmonster.com/)

[贝类学](../Category/贝类学.md "wikilink")
[Category:動物學](../Category/動物學.md "wikilink")