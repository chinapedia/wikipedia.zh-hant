[通用](../Page/通用汽车公司.md "wikilink")[EMD的](../Page/EMD.md "wikilink")[EMD
GP40型柴電機車](../Page/EMD_GP40型柴電機車.md "wikilink")，在其“标准”配置中，主要是用于货运。但是它的基本[底盘](../Page/底盘.md "wikilink")，也很让人有兴致将其开发成能用于客运服务的改型。但是这需要额外的组件来提供[蒸气或者](../Page/蒸气.md "wikilink")[列车供电](../Page/列车供电.md "wikilink")(HEP)来给旅客车厢提供暖气与照明。

## GP40P

GP40P是基于[EMD
GP40型柴電機車而设计的](../Page/EMD_GP40型柴電機車.md "wikilink")。这些改型中的大部分都采用了和[EMD
SD45类似的喇叭状侧面散热器](../Page/EMD_SD45.md "wikilink")。编号为为3671-3683的十三台GP40P在1968年为[新泽西中央铁路公司](../Page/新泽西中央铁路.md "wikilink")(CNJ)建造，并由[新泽西州交通部出资](../Page/新泽西州.md "wikilink")。CNJ公司将这些机车用于[瑞谷线以及](../Page/瑞谷线.md "wikilink")[北泽西海岸线的运营上](../Page/北泽西海岸线.md "wikilink")。

CNJ公司在1976年被合并到Conrail公司。1983年，新州捷运系统开始在新泽西州提供旅客列车服务。不久之后，那些机车被重新送回工厂进行改装。占据了机车引擎室平端的蒸气發電機，被柴油发电机所取代。而这批机车也被重新命名为GP40PH。它们在之后会被重新改为[GP40PH-2型号](../Page/#GP40PH-2.md "wikilink")。

### 重新编号

这些机车成为了CR/NJT
4100-4112。Conrail于1991-1992年间为NJT重新建造这批机车。它们中的全部，除了4101之外，都被赋予新的编号。

| NJT (1991) | [Conrail](../Page/Conrail.md "wikilink")/NJDOT (1976) | CNJ (1968) |
| ---------- | ----------------------------------------------------- | ---------- |
| 4100       | 4110                                                  | 3681       |
| 4101       | 4101                                                  | 3672       |
| 4102       | 4104                                                  | 3674       |
| 4103       | 4105                                                  | 3676       |
| 4104       | 4111                                                  | 3682       |
| 4105       | 4108                                                  | 3679       |
| 4106       | 4100                                                  | 3671       |
| 4107       | 4109                                                  | 3680       |
| 4108       | 4112                                                  | 3683       |
| 4109       | 4106                                                  | 3677       |
| 4110       | 4102                                                  | 3673       |
| 4111       | 4103                                                  | 3674       |
| 4112       | 4107                                                  | 3678       |

CNJ GP40P重新编号

## GP40P-2

[南太平洋铁路定购了三辆改型](../Page/南太平洋铁路.md "wikilink")，称为GP40P-2。南太平洋铁路将它们用于[旧金山的公交服务](../Page/旧金山.md "wikilink")。当南太平洋铁路退出客運服務之后，这些机车又被用于货运服务，直至今日。两辆在[联合太平洋铁路](../Page/联合太平洋铁路.md "wikilink")，另一辆在[印第安纳港口带铁路公司](../Page/印第安纳港口带铁路公司.md "wikilink")。联合太平洋铁路重新将其中一辆涂成装甲黄，而另一辆被补缀而涂成以前南太平洋铁路的灰色。

## GP40TC

GP40TC是为[安大略省](../Page/安大略.md "wikilink")[多伦多市市的](../Page/多伦多.md "wikilink")[安省公营客运](../Page/安省公营客运.md "wikilink")(GO
Transit)制造的（TC就代表'多伦多公交'）。在服务了Go
Transit多年之后，这些GP40TC被几种其它型号所取代，包括GP40-2W以及更新的F59PH。退出的GP40TC被卖到[美国铁路公司](../Page/美国铁路公司.md "wikilink")(Amtrak)，并放置在[芝加哥用于短途列车](../Page/芝加哥.md "wikilink")。现时这些机车被用于MOW服务，并且被[诺福克南方铁路](../Page/诺福克南方铁路.md "wikilink")（Norfolk
Southern）重整为“GP38-H3”机车。

## GP40FH-2

1987年，[新州捷运铁路和](../Page/新州捷运铁路.md "wikilink")[大都会北方铁路从](../Page/大都会北方铁路.md "wikilink")[摩里逊·洛特臣](../Page/摩里逊·洛特臣.md "wikilink")（Morrison-Knudsen）定购了一批机车。这些机车被称为GP40FH-2s，集合了GP40的标准车体以及位于车体后部的前[伯灵顿北方公司F](../Page/伯灵顿北方公司.md "wikilink")45上的通风帽（其中某些通风帽由摩里逊·洛特臣制造）。该型号一共生产了21辆：编号4130至4144的15辆，被送往新州捷运铁路；从4181到4189的6辆，是为大都会北方铁路制造的。

新州捷运铁路的GP40FH-2被拟定很快退出使用，取而代之的是[阿尔斯通的新型机车](../Page/阿尔斯通.md "wikilink")[PL42AC](../Page/PL42AC.md "wikilink")。GP40FH-2的4130-4134号车被送往[原动力](../Page/原动力工业.md "wikilink")(MotivePower
Industries)进行重整以改成转辙式机车。大都会北方铁路也开始将它们的GP40FH-2进行大修和升级。编号4188的机车已经完成检修并重新开始运营。

## GP40PH-2, GP40PH-2A, GP40PH-2B

1991年，[新州捷运铁路将其GP](../Page/新州捷运铁路.md "wikilink")40PH型号的机车进行改装。改装之后的机车称为GP40PH-2。除了4101号之外，其它的机车都被重新编号。在当时，这些机车被限制只能在[纽瓦克分支行走](../Page/纽瓦克.md "wikilink")，大部分都在[霍伯肯分支](../Page/霍伯肯.md "wikilink")。2000年，这些机车通常只在[帕斯卡克谷线行走](../Page/帕斯卡克谷.md "wikilink")，因为当时只有它们具有该线路所需要的加速系统。在[阿尔斯通的](../Page/阿尔斯通.md "wikilink")[PL42AC机车供应之后](../Page/PL42AC.md "wikilink")，这些GP40-PH2就将若干年内逐渐退出使用。

新州捷运铁路晚些时候可能再定购两批GP40PH-2机车。这些机车是由[宾州中央运输公司的GP](../Page/宾州中央运输公司.md "wikilink")40机车改装而成的，而不是以前第一批新州捷运铁路定购的那批GP40。第一张订单包括六台机车，编号4145-4150，是由[摩里逊·洛特臣改装的](../Page/摩里逊·洛特臣.md "wikilink")。这些机车，除了编号为4148的一台之外，都被归类于GP40PH-2A。虽然在他们的标志上没有A字。而4148号被改造成GP40PH-2B，在一次撞车事故后又被重新编号。第二批订单是由Conrail负责的，从4200-4218的19台机车。这些机车被归类于GP40PH-B。第二十台机车，之前的4148号GP40PH-2A,也被改造成这一型号，并重编号为4219。

[大都会北方铁路订购了一台单独的GP](../Page/大都会北方铁路.md "wikilink")40PH-2机车，编号为4190，其正式的型号为GP40PH-2M。该台机车之后被Conrail公司所改造。

## GP40WH-2

在1990年代早期，摩里逊·洛特臣与[馬里蘭區域通勤鐵路签约](../Page/馬里蘭區域通勤鐵路.md "wikilink")，用在GP40FH-2订单中剩下的GP40部件来制造一批新的GP40。这些机车被称为GP40WH-2。其具有一个类似于早期[美国铁路公司机车的红色鼻端](../Page/美国铁路公司.md "wikilink")。这些机车装配了Trans-Lite公司的[回旋警告灯](../Page/回旋警告灯.md "wikilink")(Gyralites)，型号为\#
20585。由于這些具有回旋警告灯的機車是在强制使用沟型灯之前就已经下了订单，[联邦铁路管理局](../Page/联邦铁路管理局.md "wikilink")（FRA）豁免了這些車輛一定要使用三角形灯的规定。這批機車在1994年投入服務，編號為51至69號。在新型的[MP36型機車投入後](../Page/MPI_MPXpress_柴電機車.md "wikilink")，逐漸退役。有幾部本型車由MPI公司租賃給[马萨诸塞湾交通局](../Page/马萨诸塞湾交通局.md "wikilink")。

## GP39H-2

在1980年代晚期，摩里逊·洛特臣为[馬里蘭區域通勤鐵路改造了六台GP](../Page/馬里蘭區域通勤鐵路.md "wikilink")40，編號為70至75號。更換為12汽缸引擎後，它们的功率从3000马力降级到了2300马力，并装备了[康明斯的集中供電發電機](../Page/康明斯.md "wikilink")（HEP）以供應客車電力。其中，在1996年2月16日，由第73号机车作為尾端推動機車的馬里蘭區域通勤鐵路P286班車，與[美国铁路公司的](../Page/美国铁路公司.md "wikilink")[首都特快發生了有人傷亡的車禍](../Page/首都特快.md "wikilink")。之後第73号机车被修復，并继续在[馬里蘭區域通勤鐵路服务](../Page/馬里蘭區域通勤鐵路.md "wikilink")。大约在1996年，该机车在其防攀金属网上装备了沟型灯。在90年代末期，它又在侧墙和防攀金属网周围装备了反光带。車重為
268,000磅的 GP39H-2 機車，最多可牽引或推動四節車廂達90 英里／小時（145公里／小時）。

## GP40MC

[马萨诸塞湾交通局](../Page/马萨诸塞湾交通局.md "wikilink")(MBTA)现时拥有一批名为GP40MC的GP40客用柴油机车。这些機車可以在MBTA系统的北部看到。它们也具有位于机车工程师侧中央的階梯。

## 圖輯

image:MARC52.jpg|[馬里蘭區域通勤鐵路GP](../Page/馬里蘭區域通勤鐵路.md "wikilink")40WH-2第52号停在[马里兰州](../Page/马里兰州.md "wikilink")[巴尔的摩市的坎登车站](../Page/巴尔的摩市.md "wikilink")
image:MNCR4184b.jpg|[大都会北方铁路GP](../Page/大都会北方铁路.md "wikilink")40FH-2第4184号停在[新泽西州的](../Page/新泽西州.md "wikilink")[丹佛市](../Page/丹佛市.md "wikilink")。
image:MNCR4188.JPG|大都会北方铁路GP40FH-2第4188号停在[新泽西州的](../Page/新泽西州.md "wikilink")[多佛市](../Page/多佛市.md "wikilink")。
image:NJT4109.jpg|[新州捷运铁路的GP](../Page/新州捷运铁路.md "wikilink")40PH-2第4109:2号停在[新泽西州的](../Page/新泽西州.md "wikilink")[多佛市](../Page/多佛市.md "wikilink")

## 外部链接

  - [Railroad.net
    关于CNJ的机车](http://www.railroad.net/forums/viewtopic.php?t=19596)

  - [hobokenterminal.com上的非官方的新州捷运铁路的花名册](https://web.archive.org/web/20060312193401/http://www.hobokenterminal.com/alltime.html)

[category:美國國鐵鐵路車輛](../Page/category:美國國鐵鐵路車輛.md "wikilink")

[Category:美國柴油機車](../Category/美國柴油機車.md "wikilink")
[Category:Bo-Bo軸式機車](../Category/Bo-Bo軸式機車.md "wikilink")
[Category:EMD制铁路机车](../Category/EMD制铁路机车.md "wikilink")