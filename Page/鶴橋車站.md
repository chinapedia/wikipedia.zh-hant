**鶴橋車站**（）是一個位於[日本](../Page/日本.md "wikilink")[大阪府](../Page/大阪府.md "wikilink")[大阪市](../Page/大阪市.md "wikilink")[生野區](../Page/生野區.md "wikilink")、[天王寺區](../Page/天王寺區.md "wikilink")，屬於[近畿日本鐵道](../Page/近畿日本鐵道.md "wikilink")（近鐵）、[西日本旅客鐵道](../Page/西日本旅客鐵道.md "wikilink")（JR西日本）、[大阪市高速電氣軌道](../Page/大阪市高速電氣軌道.md "wikilink")（大阪地下鐵）的[鐵路車站](../Page/鐵路車站.md "wikilink")。JR西日本的車站編號是**JR-O04**。近畿日本鐵道的車站編號是**A04**、**D04**。大阪地下鐵的車站編號是**S19**。JR西日本的車站象徵花卉是「[繡球花](../Page/繡球_\(植物\).md "wikilink")」。

## 概要

在幾家業者所屬的站區中，近鐵所屬的站區位於生野區境內，而JR西日本與[大阪市高速電氣軌道的站區則位於天王寺區內](../Page/大阪市高速電氣軌道.md "wikilink")。鶴橋車站有多條鐵路路線經過，其中包括近鐵所屬的[大阪線與](../Page/近鐵大阪線.md "wikilink")[奈良線](../Page/近鐵奈良線.md "wikilink")（共用大阪線的路軌），JR西日本的[大阪環狀線](../Page/大阪環狀線.md "wikilink")，與市營地下鐵[千日前線](../Page/千日前線.md "wikilink")。由於站內各路線匯集，每日超過20萬人次以上的使用率讓鶴橋車站成為JR西日本排名第六與近鐵排名第一的大型車站（根據2006年數據）。鶴橋車站是JR西日本旗下大阪近郊鐵路路線群[都市網路](../Page/都市網路.md "wikilink")（，Urban
Network）所屬的車站，根據[JR的](../Page/JR.md "wikilink")[特定都區市內制度](../Page/特定都區市內.md "wikilink")，被劃分為「大阪市內」的車站之一。

## 車站結構

### 近鐵

[島式月台](../Page/島式月台.md "wikilink")2面4線的[高架車站](../Page/高架車站.md "wikilink")，可停靠10節列車。月台位於2樓。

#### 月台配置

<table>
<thead>
<tr class="header">
<th><p>月台</p></th>
<th><p>路線</p></th>
<th><p>方向</p></th>
<th><p>目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p><strong></strong></p></td>
<td><p>（大阪難波始發）  <a href="../Page/近鐵名古屋站.md" title="wikilink">名古屋方向</a>　 <a href="../Page/賢島站.md" title="wikilink">伊勢志摩方向</a>　 <a href="../Page/近鐵奈良站.md" title="wikilink">奈良方向</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p>奈良線</p></td>
<td><p>下行</p></td>
<td><p>、<a href="../Page/大和西大寺站.md" title="wikilink">大和西大寺</a>、奈良方向</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>2</p></td>
<td><p><strong></strong></p></td>
<td><p>（大阪上本町始發）  伊勢志摩方向</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>大阪線</p></td>
<td><p>下行</p></td>
<td><p>、方向　 名古屋方向　 伊勢志摩方向</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p>奈良線</p></td>
<td><p>上行</p></td>
<td><p><a href="../Page/大阪上本町站.md" title="wikilink">大阪上本町</a>（地下）、<a href="../Page/大阪難波站.md" title="wikilink">大阪難波方向</a> <a href="https://zh.wikipedia.org/wiki/File:Number_prefix_Hanshin_Railway.png" title="fig:Number_prefix_Hanshin_Railway.png">Number_prefix_Hanshin_Railway.png</a> 、、<a href="../Page/三宮站.md" title="wikilink">神戶三宮方向</a></p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p>大阪線</p></td>
<td><p>上行</p></td>
<td><p>往 大阪上本町（地面）</p></td>
</tr>
</tbody>
</table>

#### 配置圖

</div>

</div>

{{-}}

### JR西日本

[對向式月台](../Page/對向式月台.md "wikilink")2面2線的高架車站。由於不設[轉轍器與絕對信號機](../Page/轉轍器.md "wikilink")，因此被分類為停留所。

#### 月台配置

<table>
<thead>
<tr class="header">
<th><p>月台</p></th>
<th><p>路線</p></th>
<th><p>方向</p></th>
<th><p>目的地</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p>大阪環狀線</p></td>
<td><p>內環</p></td>
<td><p><a href="../Page/京橋站_(大阪府).md" title="wikilink">京橋</a>、<a href="../Page/大阪站.md" title="wikilink">大阪</a>、<a href="../Page/環球影城站.md" title="wikilink">環球影城方向</a></p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>外環</p></td>
<td><p><a href="../Page/天王寺站.md" title="wikilink">天王寺</a>、<a href="../Page/新今宮站.md" title="wikilink">新今宮方向</a></p></td>
<td></td>
</tr>
</tbody>
</table>

### 大阪市高速電氣軌道

島式月台1面2線的[地下車站](../Page/地下車站.md "wikilink")。閘機分別設於東西兩處。JR鶴橋站附近的出入口為5 -
7號出入口（東閘機附近）、此處西面的交差點為1 - 4號出入口（西閘機附近）。轉乘JR與近鐵最近為6號出口。

#### 月台配置

| 月台 | 路線                                                                                                                    | 目的地                               |
| -- | --------------------------------------------------------------------------------------------------------------------- | --------------------------------- |
| 1  | [5S.png](https://zh.wikipedia.org/wiki/File:5S.png "fig:5S.png") 千日前線                                                 | [南巽方向](../Page/南巽站.md "wikilink") |
| 2  | [難波](../Page/難波站_\(大阪市高速電氣軌道\).md "wikilink")、[阿波座](../Page/阿波座站.md "wikilink")、[野田阪神方向](../Page/野田阪神站.md "wikilink") |                                   |

## 相鄰車站

  - 近畿日本鐵道

<!-- end list -->

  -
<!-- end list -->

  -

    大阪線

      -

        快速急行

          -
            [大阪上本町](../Page/大阪上本町站.md "wikilink")（D-03）－**鶴橋（D-04）**－（D-23）

        急行、準急、區間準急

          -
            大阪上本町（D-03）－**鶴橋（D-04）**－（D-06）

        普通

          -
            大阪上本町（D-03）－**鶴橋（D-04）**－（D-05）

    奈良線

      -

        快速急行（此站起至[難波線](../Page/難波線_\(阪神電氣鐵道\).md "wikilink")[西九條站為止各站停車](../Page/西九條站.md "wikilink")）

          -
            大阪上本町（A-03）－**鶴橋（A-04）**－[生駒](../Page/生駒站.md "wikilink")（A-17）

        急行、準急、區間準急

          -
            大阪上本町（A-03）－**鶴橋（A-04）**－布施（A-06）

        普通

          -
            大阪上本町（A-03）－**鶴橋（A-04）**－今里（A-05）

  - 西日本旅客鐵道（JR西日本）

    大阪環狀線

      -

        大和路快速、區間快速、關空快速、紀州路快速、快速、直通快速、普通

          -
            [桃谷](../Page/桃谷站.md "wikilink")（JR-O03）－**鶴橋（JR-O04）**－[玉造](../Page/玉造站.md "wikilink")（JR-O05）

  - 大阪市高速電氣軌道（大阪地下鐵）
    [5S.png](https://zh.wikipedia.org/wiki/File:5S.png "fig:5S.png")
    千日前線

      -
        [谷町九丁目](../Page/谷町九丁目站.md "wikilink")（S-18）－**鶴橋（S-19）**－[今里](../Page/今里站_\(大阪市高速電氣軌道\).md "wikilink")（S-20）

## 參考資料

## 外部連結

  - [近畿日本鐵道
    鶴橋站](http://www.kintetsu.co.jp/station/station_info/station02006.html)

  - [鶴橋車站（JR西日本）](http://www.jr-odekake.net/eki/top.php?id=0610508)

  - [車站導覽：鶴橋站](http://www.osakametro.co.jp/station-guide/S/s19.html) -
    Osaka Metro

[Ruhashi](../Category/日本鐵路車站_Tsu.md "wikilink")
[Category:大阪線車站](../Category/大阪線車站.md "wikilink")
[Category:大阪環狀線車站](../Category/大阪環狀線車站.md "wikilink")
[Category:千日前線車站](../Category/千日前線車站.md "wikilink")
[Category:1914年啟用的鐵路車站](../Category/1914年啟用的鐵路車站.md "wikilink")
[Category:天王寺區鐵路車站](../Category/天王寺區鐵路車站.md "wikilink")
[Category:生野區鐵路車站](../Category/生野區鐵路車站.md "wikilink")
[Category:以建築物命名的鐵路車站](../Category/以建築物命名的鐵路車站.md "wikilink")