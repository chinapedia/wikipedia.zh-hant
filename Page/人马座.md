**人马座**（，[天文符号](../Page/天文符号.md "wikilink")：♐），又稱**射手座**，是一个[南天](../Page/天球.md "wikilink")[黄道带](../Page/黄道带.md "wikilink")[星座](../Page/星座.md "wikilink")，面积867.43平方度，占全天面积的2.103%\[1\]，在全天88个星座中，[面积排行第十五](../Page/星座面积列表.md "wikilink")。人马座中亮于5.5等的恒星有65颗，最亮星为[箕宿三](../Page/箕宿三.md "wikilink")（人马座ε），视星等为1.85。每年7月7日子夜人马座中心经过[上中天](../Page/中天_\(天文学\).md "wikilink")。

## 名称

人马座又称射手座。“射手座”是从[拉丁语Sagittarius意译而来](../Page/拉丁语.md "wikilink")，sagittarius一词[拉丁语中意为](../Page/拉丁语.md "wikilink")“持箭者”，箭的[拉丁语是sagitta](../Page/拉丁语.md "wikilink")，[天箭座即Sagitta](../Page/天箭座.md "wikilink")；相似情况也可以对比[拉丁语中的水aqua与持水者aquarius](../Page/拉丁语.md "wikilink")，[宝瓶座](../Page/宝瓶座.md "wikilink")/[水瓶座即Aquarius](../Page/水瓶座.md "wikilink")。人马座原为[巴比伦天文学中黄道星座之一](../Page/巴比伦天文学.md "wikilink")，其原型为[苏美尔神祇](../Page/苏美尔.md "wikilink")[内尔伽勒](../Page/内尔伽勒.md "wikilink")（Nergal），形态为拉弓射箭的带翼人马。\[2\]
黄道星座随后被希腊的星座系统沿用，但射手座在[希腊神话代表](../Page/希腊神话.md "wikilink")[人马](../Page/半人马.md "wikilink")[喀戎](../Page/喀戎.md "wikilink")，传说其发明了射箭。

## 特点

人马座是[夏季夜空中最大](../Page/夏季.md "wikilink")、最显著的[星座之一](../Page/星座.md "wikilink")，它西接[天蝎座](../Page/天蝎座.md "wikilink")、東連[摩羯座](../Page/摩羯座.md "wikilink")，北面是[蛇夫座](../Page/蛇夫座.md "wikilink")、[盾牌座和](../Page/盾牌座.md "wikilink")[巨蛇座](../Page/巨蛇座.md "wikilink")，南边则是一系列小型星座，如[望远镜座](../Page/望远镜座.md "wikilink")、[显微镜座](../Page/显微镜座.md "wikilink")、[南冕座等](../Page/南冕座.md "wikilink")。

[Sagittarius_constellation_detail_long_exposure.jpg](https://zh.wikipedia.org/wiki/File:Sagittarius_constellation_detail_long_exposure.jpg "fig:Sagittarius_constellation_detail_long_exposure.jpg")

人马座并不难认，因为它主要的星排列得像一个茶壶：[箕宿二](../Page/箕宿二.md "wikilink")（δ）、[箕宿三](../Page/箕宿三.md "wikilink")（ε）、[斗宿六](../Page/斗宿六.md "wikilink")（ζ）及[斗宿三](../Page/斗宿三.md "wikilink")（φ）组成壶身；[斗宿二](../Page/斗宿二.md "wikilink")（λ）为壶盖；[箕宿一](../Page/箕宿一.md "wikilink")（γ）为壶嘴；[斗宿四](../Page/斗宿四.md "wikilink")（σ）与[斗宿五](../Page/斗宿五.md "wikilink")（τ）为壶柄。另外其中六顆星排列相-{斗}-杓：[斗宿一](../Page/斗宿一.md "wikilink")（μ）、斗宿二（λ）、斗宿三（φ）、斗宿四（σ）、斗宿五（τ）和斗宿六（ζ），在古代中国稱為[南斗六星](../Page/南斗六星.md "wikilink")，也就是[斗宿名稱的來源](../Page/斗宿.md "wikilink")。

在人马座天区，[银河达到最大的亮度和宽度](../Page/银河.md "wikilink")，银河系的中心位于射电源人马座A\*附近。

## 神话与历史

在[希腊神话中](../Page/希腊神话.md "wikilink")，射手座的箭对着位于它前方的[天蝎座的胸](../Page/天蝎座.md "wikilink")，射手座在天上是为了给去取[金羊毛的](../Page/金羊毛.md "wikilink")[伊阿宋指路](../Page/伊阿宋.md "wikilink")，[古希腊人将射手座与](../Page/古希腊人.md "wikilink")[萨杜恩](../Page/萨杜恩.md "wikilink")（半人半[羊](../Page/羊.md "wikilink")）以及[克洛托斯联系起来](../Page/克洛托斯.md "wikilink")。[罗马人却将他认为是优雅而智慧的](../Page/罗马.md "wikilink")[喀戎](../Page/喀戎.md "wikilink")，这导致人们常常把它与南天的[半人马座混淆](../Page/半人马座.md "wikilink")，然而这两种[生物有一个明显的区别](../Page/生物.md "wikilink")，射手座是一名猎手，这可以追溯到[美索不达米亚的箭神涅伽尔](../Page/美索不达米亚.md "wikilink")（Nergal），而他与暴躁的[战神和](../Page/战神.md "wikilink")[火神伊拉](../Page/火神.md "wikilink")（Irra）有关。

[Sagittarius_Hevelius.jpg](https://zh.wikipedia.org/wiki/File:Sagittarius_Hevelius.jpg "fig:Sagittarius_Hevelius.jpg")筆下的射手座\]\]

## 恒星

[天渊三虽然是人马座的α星](../Page/天渊三.md "wikilink")，但它的[视星等只有](../Page/视星等.md "wikilink")4.0，比星座中不少的星还要暗。

[SagittariusCC.jpg](https://zh.wikipedia.org/wiki/File:SagittariusCC.jpg "fig:SagittariusCC.jpg")

### 重要主星表

| [拜耳命名法](../Page/拜耳命名法.md "wikilink")           | 中國星名 | 英文名字               | 英文含意  | 視星等  |
| ---------------------------------------------- | ---- | ------------------ | ----- | ---- |
| [人马座α](../Page/人马座α.md "wikilink")             | 天渊三  | Rukbat/Alrami      | 膝/射手  | 3.97 |
| [人马座β<sup>1</sup>](../Page/人馬座β.md "wikilink") | 天渊二  | Arkab              | 腿的前面  | 3.90 |
| [人马座β<sup>2</sup>](../Page/人馬座β.md "wikilink") | 天渊一  | Arkab Posterior    | 腿的后面  | 4.92 |
| [人马座γ](../Page/人马座γ.md "wikilink")             | 箕宿一  | Alnasl             | 箭尖    | 2.99 |
| [人马座δ](../Page/人马座δ.md "wikilink")             | 箕宿二  | Kaus Meridiandlis  | 弓的中部  | 2.70 |
| [人马座ε](../Page/人马座ε.md "wikilink")             | 箕宿三  | Kaus Australis     | 弓的南部  | 1.85 |
| [人马座ζ](../Page/斗宿六.md "wikilink")              | 斗宿六  | Ascella            | 半马人的臂 | 2.60 |
| [人马座η](../Page/人马座η.md "wikilink")             | 箕宿四  | Arkab              | 腿     | 3.11 |
| 人马座θ<sup>1</sup>                               | 天渊增二 | \-                 | \-    | 4.37 |
| 人马座θ<sup>2</sup>                               | 狗国增二 | \-                 | \-    | 5.30 |
| [人马座ι](../Page/人马座ι.md "wikilink")             | 天渊增一 | \-                 | \-    | 4.12 |
| 人马座κ<sup>1</sup>                               | \-   | \-                 | \-    | 5.60 |
| 人马座κ<sup>2</sup>                               | \-   | \-                 | \-    | 5.64 |
| [人马座λ](../Page/斗宿二.md "wikilink")              | 斗宿二  | Kaus Borealis      | 弓的北部  | 2.81 |
| [人马座μ](../Page/人马座μ.md "wikilink")             | 斗宿一  | Polis              | 马驹    | 3.84 |
| 人马座ν<sup>1</sup>                               | 建增六  | Ain al Rami        | 射手之眼  | 4.86 |
| 人马座ν<sup>2</sup>                               | 建增七  | Ain al Rami        | 射手之眼  | 5.00 |
| [人马座ξ<sup>1</sup>](../Page/人马座ξ.md "wikilink") | 建增二  | \-                 | \-    | 5.02 |
| [人马座ξ<sup>2</sup>](../Page/人马座ξ.md "wikilink") | 建一   | Nergal             | 战神    | 3.52 |
| [人马座ο](../Page/人马座ο.md "wikilink")             | 建二   | Manubrij/Manubrium | 柄     | 3.76 |
| [人马座π](../Page/人马座π.md "wikilink")             | 建三   | Albaldah           | \-    | 2.88 |
| [人马座ρ<sup>1</sup>](../Page/人马座ρ.md "wikilink") | 建五   | Cappa              | 斗篷    | 3.92 |
| [人马座ρ<sup>2</sup>](../Page/人马座ρ.md "wikilink") | 建增八  | \-                 | \-    | 5.84 |
| [人马座σ](../Page/斗宿四.md "wikilink")              | 斗宿四  | Nunki              | \-    | 2.02 |
| [人马座τ](../Page/人马座τ.md "wikilink")             | 斗宿五  | Hecatebolus        | \-    | 3.32 |
| [人马座υ](../Page/人马座υ.md "wikilink")             | 建六   | \-                 | \-    | 4.52 |
| [人马座φ](../Page/斗宿三.md "wikilink")              | 斗宿三  | \-                 | \-    | 3.17 |
| 人马座χ<sup>1</sup>                               | 狗二   | \-                 | \-    | 5.02 |
| 人马座χ<sup>3</sup>                               | 狗增四  | \-                 | \-    | 5.45 |
| [人马座ψ](../Page/人马座ψ.md "wikilink")             | 狗增六  | \-                 | \-    | 4.86 |
| [人马座ω](../Page/人马座ω.md "wikilink")             | 狗国一  | \-                 | \-    | 4.70 |
| [人马座3](../Page/人马座3.md "wikilink")             | 天籥八  | \-                 | \-    | 4.58 |
| [人马座43](../Page/人马座43.md "wikilink")           | 建四   | \-                 | \-    | 4.90 |
| [人马座55](../Page/人马座55.md "wikilink")           | 天鸡一  | \-                 | \-    | 5.08 |
| [人马座56](../Page/人马座56.md "wikilink")           | 天鸡二  | \-                 | \-    | 4.89 |
| [人马座59](../Page/人马座59.md "wikilink")           | 狗国四  | \-                 | \-    | 4.45 |
| [人马座60](../Page/人马座60.md "wikilink")           | 狗国二  | \-                 | \-    | 4.86 |
| [人马座62](../Page/人马座62.md "wikilink")           | 狗国三  | \-                 | \-    | 4.45 |

## [深空天体](../Page/深空天体.md "wikilink")

從[地球看來](../Page/地球.md "wikilink")，[本银河系的中心位于人马座](../Page/银河系.md "wikilink")，雖然银心被人马臂上的[星云和尘埃带所遮挡](../Page/星云.md "wikilink")，但是人马座的银河仍是非常浓密，中间还有很多明亮的[星团](../Page/星团.md "wikilink")。

这个星座中的天体主要是银河深处的[宇宙天体](../Page/宇宙.md "wikilink")，包括[发射星云和](../Page/发射星云.md "wikilink")[暗星云](../Page/暗星云.md "wikilink")，[疏散星团和](../Page/疏散星团.md "wikilink")[球状星团以及](../Page/球状星团.md "wikilink")[行星状星云](../Page/行星状星云.md "wikilink")。人马座有多达15个[梅西耶天体](../Page/梅西耶天体.md "wikilink")——这是所有星座中最多的。其中很多用[双筒望远镜就可以观测到](../Page/双筒望远镜.md "wikilink")。

与银河系中心有关的[人马座A是一个复杂的](../Page/人马座A.md "wikilink")[无线电源](../Page/无线电.md "wikilink")，[天文学家相信它或许包含了一个超大](../Page/天文学家.md "wikilink")[质量的](../Page/质量.md "wikilink")[黑洞](../Page/黑洞.md "wikilink")。

### [梅西耶天体](../Page/梅西耶天体.md "wikilink")

[M8HunterWilson.jpg](https://zh.wikipedia.org/wiki/File:M8HunterWilson.jpg "fig:M8HunterWilson.jpg")

  - [礁湖星云](../Page/礁湖星云.md "wikilink")（M8）：位于斗宿二附近，这个明亮的发射星云被一条遮掩物质形成的暗带切开了。用双筒望远镜容易看到，这个星云中包含了一个稀疏的疏散星团，即[NGC
    6530](../Page/NGC_6530.md "wikilink")。
  - [奥米加星云](../Page/奥米加星云.md "wikilink")（M17）：接近[盾牌座边界的M](../Page/盾牌座.md "wikilink")17是天空中最明亮的发射星云之一，它亦称天鹅星云、马蹄星云，用双筒望远镜就很容易看见它。这是因为其中最明亮的一部分让人想起在湖面游动的一只[天鹅的身体](../Page/天鹅.md "wikilink")。用一架8英寸（20厘米）或更大的望远镜装上[星云滤光器可以去除几乎充满这个区域的较暗的星云状物质](../Page/星云滤光器.md "wikilink")。
  - [M18](../Page/M18.md "wikilink")：这个小的疏散星团有十几个星等+9到+10的天体。
  - [三裂星云](../Page/三裂星云.md "wikilink")（M20）：亦稱三叶星云，是发散和发射混和型星云，包含了不少年轻[恒星](../Page/恒星.md "wikilink")。它的彩色照片非常美丽，可以看见它桃红色和亮蓝色的部分。用小型望远镜看卻難以分辨——由遮掩物质形成的暗沟把这个星云分为三个区域，所以它被称为三叶星云。在星云的中心还有一颗耀眼的[三合星](../Page/三合星.md "wikilink")。M20相距地球五千[光年](../Page/光年.md "wikilink")，其中炽热年轻的恒星被尘埃和气体所包围。

[trifid.nebula.arp.750pix.jpg](https://zh.wikipedia.org/wiki/File:trifid.nebula.arp.750pix.jpg "fig:trifid.nebula.arp.750pix.jpg")

  - [M21](../Page/M21.md "wikilink")：这是一个明亮但相当小的[疏散星团](../Page/疏散星团.md "wikilink")，用小型望远镜很容易看见，总星等為+5.9。
  - [M22](../Page/M22_\(星團\).md "wikilink")：它的直径约18[弧分](../Page/弧分.md "wikilink")，是个结构松散的球状星团，用8英寸（20厘米）望远镜可以很好的观测，甚至，能分辨出它中心部分很多单个恒星。M22是相距地球[较近的星团](../Page/较近.md "wikilink")，只有一万光年。总星等为+5.1。
  - [M23](../Page/M23.md "wikilink")：这是个大而壮观的疏散星团，约100颗星等在+9或更暗的恒星。它的星等是+5.5。
  - [小人马恒星云](../Page/小人马恒星云.md "wikilink")（M24）：它的直径超过1度，并不是真正的星团，而是[银河中的一个明亮部分](../Page/银河.md "wikilink")。裸眼可见。但用双筒望远镜看的更清晰。用小型望远镜可以看见数不清的成片恒星。
  - [M25](../Page/M25.md "wikilink")：这是一个明亮稀疏的疏散星团，有大约50颗+6星等的恒星，还有更多更暗的。
  - [M28](../Page/M28.md "wikilink")：这是一个小的中等亮度但很致密的球状星团。需要约口径12英寸（30厘米）的望远镜才能分辨出单个恒星。其星等是+6.9。
  - [M54](../Page/M54.md "wikilink")：这是一个中等亮度但小而致密的小球状星团，需要使用大望远镜才能分辨出它，它的星等是+7.7，距离地球68000光年。
  - [M55](../Page/M55.md "wikilink")：在箕宿二西面约7.5°的M55。这是一个明亮结构松散的球状星团，用8英寸（20厘米）望远镜容易分辨。其星等是+7.0，距离地球20000光年。
  - [M69](../Page/M69.md "wikilink")：这是个又小又圆的球状星团，相当致密。使用8英寸（20厘米）的望远镜的高倍放大可以分辨出一些処于边缘的星，星等+7.7。
  - [M75](../Page/M75.md "wikilink")：也稱為**NGC
    6864**，是一個[球狀星團](../Page/球狀星團.md "wikilink")，於1780年被[皮埃尔·梅尚發現](../Page/皮埃尔·梅尚.md "wikilink")，並且在同年被[梅西爾發現並登錄在他的疑似](../Page/查尔斯·梅西耶.md "wikilink")[彗星目錄中](../Page/彗星.md "wikilink")。

## 中国星官

中国古代的[星宫系统中人马座天区包括](../Page/三垣二十八宿.md "wikilink")[箕宿的箕](../Page/箕宿.md "wikilink")、[斗宿的斗](../Page/斗宿.md "wikilink")、天渊、建、天籥、狗国、狗、天鸡、农丈人等[星官](../Page/星官.md "wikilink")。

  - 箕Winnowing Basket（箕4）：人马座γ、δ、ε、η
  - 斗Dipper（斗6）：人马座μ、λ、φ、σ、τ、ζ
  - 天渊Celestial Spring（斗3）：人马座α、β<sup>1</sup>、β<sup>2</sup>
  - 建Establishment（斗6）：人马座ξ<sup>2</sup>、ο、π、43、ρ、υ
  - 天籥Celestial Keyhole（斗8）：HD 162978（天籥一）、HD 161664（天籥二）、人马座3
  - 狗国Territory of Dog（斗4）：人马座ω、60、62、59
  - 狗Dog（斗2）：人马座52、χ<sup>1</sup>
  - 天鸡Celestial Cock（斗2）：人马座55、56
  - 农丈人Peasant（斗1）：HD 172910

## 相关文化

[北宋](../Page/北宋.md "wikilink")[文学家](../Page/文学家.md "wikilink")[苏轼的名句](../Page/苏轼.md "wikilink")“月出于东山之上，徘徊于-{斗牛}-之间”中的斗指的就是人马座的斗宿天区。

在[爆旋陀螺
钢铁战魂中](../Page/爆旋陀螺_钢铁战魂.md "wikilink")，日本陀螺手湯宮健太所持有的陀螺是終極射手145S/烈焰射手C145S/激閃射手230WD，它拥有射手座的力量

## 参见

  - [梅西耶天体](../Page/梅西耶天体.md "wikilink")
  - [梅西耶天体列表](../Page/梅西耶天体列表.md "wikilink")
  - [深空天体](../Page/深空天体.md "wikilink")
  - [NGC天体列表](../Page/NGC天体列表.md "wikilink")

## 注释

## 参考资料

  - [AEEA天文教育資訊網 箕宿天區](http://aeea.nmns.edu.tw/2006/0605/ap060511.html)
  - [AEEA天文教育資訊網 斗宿天區](http://aeea.nmns.edu.tw/2006/0605/ap060512.html)

[Ren2](../Category/星座.md "wikilink") [\*](../Category/人馬座.md "wikilink")
[Category:托勒密星座](../Category/托勒密星座.md "wikilink")
[Category:黃道星座](../Category/黃道星座.md "wikilink")

1.

2.