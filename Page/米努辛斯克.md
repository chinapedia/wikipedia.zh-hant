[Coat_of_Arms_of_Minusinsk_(1854).png](https://zh.wikipedia.org/wiki/File:Coat_of_Arms_of_Minusinsk_\(1854\).png "fig:Coat_of_Arms_of_Minusinsk_(1854).png")
**米努辛斯克**
（）是[俄羅斯](../Page/俄羅斯.md "wikilink")[克拉斯諾亞爾斯克邊疆區的一個城市](../Page/克拉斯諾亞爾斯克邊疆區.md "wikilink")，位於[葉尼塞河右側水道上](../Page/葉尼塞河.md "wikilink")，與[哈卡斯共和國首府](../Page/哈卡斯共和國.md "wikilink")[阿巴坎相距](../Page/阿巴坎.md "wikilink")25千米。2002年人口72,561人。

1739年開埠，1822年建鎮。

## 歷史

[俄羅斯帝國的定居點](../Page/俄羅斯帝國.md "wikilink")“泯余辛斯克也”（）創建於1739-1740年在迷努薩河和[葉尼塞河的交匯處](../Page/葉尼塞河.md "wikilink")。\[1\]在[突厥语族中](../Page/突厥语族.md "wikilink")，迷努薩意味著“我的小河”\[2\]
或者“千條河”。\[3\] 在1810年，名字就改變了成“米努辛斯克也” （）。

## 友好城市

  - [諾里爾斯克](../Page/諾里爾斯克.md "wikilink")

## 参考资料

[М](../Category/克拉斯諾亞爾斯克邊疆區城市.md "wikilink")

1.  Н. И. Дроздов, В. С. Боровец "Енисейский энциклопедический словарь".
    克拉斯诺亚尔斯克, 1998年 (ISBN 5883290051), 第391页. （俄語）
2.
3.