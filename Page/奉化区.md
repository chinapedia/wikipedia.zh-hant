**奉化区**是[中國](../Page/中國.md "wikilink")[浙江省](../Page/浙江省.md "wikilink")[宁波市的一个市辖区](../Page/宁波市.md "wikilink")，位于宁波市区南部，為前[中華民國總統](../Page/中華民國總統.md "wikilink")[蔣中正](../Page/蔣中正.md "wikilink")、[蔣經國故鄉](../Page/蔣經國.md "wikilink")。

## 历史

[秦灭](../Page/秦国.md "wikilink")[楚国后](../Page/楚国.md "wikilink")，越地归入秦国疆域，[秦王政于统一六国的前一年即公元前](../Page/秦王政.md "wikilink")222年设置了鄞、鄮、句章（句读音为勾）三县，均属[会稽郡下辖](../Page/会稽郡.md "wikilink")。[鄞县的区域包括今天](../Page/鄞县.md "wikilink")[宁波的](../Page/宁波.md "wikilink")[海曙及奉化](../Page/海曙区.md "wikilink")、[象山一带](../Page/象山县.md "wikilink")，县治设在今奉化白杜。鄮县的区域包括今宁波[鄞州](../Page/鄞州区.md "wikilink")、[北仑及](../Page/北仑区.md "wikilink")[舟山群岛](../Page/舟山群岛.md "wikilink")，治所设在宝幢附近的鄮山同谷，即今[五乡镇同岙村](../Page/五乡镇.md "wikilink")，旧称鄮廓。句章县包括今市区江北和今海曙、旧时[慈溪](../Page/慈溪.md "wikilink")、[余姚一带地方](../Page/余姚.md "wikilink")，治所在今慈城附近的乍山城山渡。

公元前206年，[楚霸王](../Page/楚霸王.md "wikilink")[项羽封](../Page/项羽.md "wikilink")[英布为](../Page/英布.md "wikilink")[九江王](../Page/九江.md "wikilink")，统有[九江](../Page/九江.md "wikilink")、彰、[会稽三郡之境](../Page/会稽.md "wikilink")，鄞县与鄮县、句章均属九江国。

秦亡后，[汉袭秦制](../Page/汉朝.md "wikilink")，仍置鄞县。[西汉初期分封刘姓诸侯王和有功](../Page/西汉.md "wikilink")[异姓王](../Page/异姓王.md "wikilink")，鄞、鄮、句章等县先属[刘贾的荆国](../Page/刘贾.md "wikilink")，后属[刘濞的吴国](../Page/刘濞.md "wikilink")。

[汉景帝时](../Page/汉景帝.md "wikilink")（公元前154年），发生了诸侯国反叛中央的“[七王之乱](../Page/七王之乱.md "wikilink")”，吴王刘濞是反叛首领。三个月后，叛乱平息，刘濞被诛，吴地削藩，鄞县复属会稽郡。[新莽天凤元年](../Page/新莽.md "wikilink")（公元14年），[王莽把鄮县改为海治县](../Page/王莽.md "wikilink")，把鄞县改为谨县，仍属会稽郡。

[东汉建立后](../Page/东汉.md "wikilink")，[光武帝在建武](../Page/光武帝.md "wikilink")（公元25－56年）初年，废谨县名，复改为鄞县。

纵观秦汉两代，鄞县县治在白杜，历来归会稽郡所辖，其县境兼有今海曙、鄞州、奉化及宁海、象山部分之地，重心在今奉化。

[唐](../Page/唐朝.md "wikilink")[开元二十六年](../Page/开元.md "wikilink")（738年）设[奉化县](../Page/奉化县.md "wikilink")。奉化名由来，有三说：

  - 元丰《九域志》：“州曰明，郡曰奉化，以郡名名县。”
  - [南宋乡人陈著](../Page/南宋.md "wikilink")：“奉化为邑，以民皆乐于奉承王化得名。”
  - [明](../Page/明朝.md "wikilink")[嘉靖](../Page/嘉靖.md "wikilink")《奉化县图志》：“县东南五里，有山特起曰奉化。”

奉化自建县以来，区划多有变动。[元元贞元年](../Page/元朝.md "wikilink")（1295年）升为州，明[洪武二年](../Page/洪武.md "wikilink")
（1369年）复为县。

1988年10月13日撤县设市。2001年，配合经济和社会发展，对乡镇行政区划进行调整，形成5[街道](../Page/街道_\(行政区划\).md "wikilink")、6[镇的格局](../Page/镇.md "wikilink")。2016年10月，奉化撤市设区，成立宁波市奉化区。\[1\]

## 行政区划

### 乡级

下辖6个街道、6个镇：

  - 街道办事处：[锦屏街道](../Page/锦屏街道_\(宁波市\).md "wikilink")、[岳林街道](../Page/岳林街道.md "wikilink")、[江口街道](../Page/江口街道_\(宁波市\).md "wikilink")、[西坞街道](../Page/西坞街道.md "wikilink")、[萧王庙街道](../Page/萧王庙街道.md "wikilink")、[方桥街道](../Page/方桥街道.md "wikilink")
  - 镇：[溪口镇](../Page/溪口镇_\(宁波市\).md "wikilink")、[莼湖镇](../Page/莼湖镇.md "wikilink")、[尚田镇](../Page/尚田镇.md "wikilink")、[大堰镇](../Page/大堰镇.md "wikilink")、[裘村镇](../Page/裘村镇.md "wikilink")、[松岙镇](../Page/松岙镇.md "wikilink")。

2010年3月10日，宁波溪口雪窦山风景名胜区管理委员会成立，溪口镇由该委员会代为管理，奉化区主要负责人兼任该委员会负责人。

### 村级

截至2016年7月，原奉化市共有40个社区和353个行政村，另有未经民政部门确认的类似行政村的单位（生活区）5个（锦屏街道三溪农场生活区、岳林街道土埭良种繁殖场生活区、西坞街道西坞林场生活区、西坞街道西坞茶场生活区、溪口镇商量岗林场生活区）。

<table>
<thead>
<tr class="header">
<th><p>地图</p></th>
<th><p>区划名称</p></th>
<th><p>面积</p></th>
<th><p>人口</p></th>
<th><p>邮政编码</p></th>
<th><p>区划代码</p></th>
<th><p>村级区划数</p></th>
<th><p>治所</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>{{Image label|x=217.0|y=48.0|text=<strong>[[方桥街道| {{Image label|x=198.0|y=75.0|text=</strong>[[江口街道_(宁波市)| {{Image label|x=183.0|y=105.0|text=<strong>[[锦屏街道_(宁波市)| {{Image label|x=213.0|y=106.0|text=</strong>[[岳林街道| {{Image label|x=255.0|y=103.0|text=<strong>[[西坞街道| {{Image label|x=150.0|y=95.0|text=</strong>[[萧王庙街道| {{Image label|x=75.0|y=125.0|text=<strong>[[溪口镇_(宁波市)| {{Image label|x=307.0|y=155.0|text=</strong>[[裘村镇| {{Image label|x=357.0|y=143.0|text=<strong>[[松岙镇| {{Image label|x=250.0|y=165.0|text=</strong>[[莼湖镇| {{Image label|x=175.0|y=160.0|text=<strong>[[尚田镇| {{Image label|x=120.0|y=195.0|text=</strong>[[大堰镇| </p></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p>锦屏街道</p></td>
<td><p>44.03平方千米</p></td>
<td><p>8.0万</p></td>
<td><p>315500</p></td>
<td><p>330213001</p></td>
<td><p>12社区，19行政村</p></td>
<td><p>茗山路251号</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>岳林街道</p></td>
<td><p>26.69平方千米</p></td>
<td><p>3.7万</p></td>
<td><p>315500</p></td>
<td><p>330213002</p></td>
<td><p>8社区，22行政村</p></td>
<td><p>天峰路62号</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>江口街道</p></td>
<td><p>36.66平方千米</p></td>
<td><p>3万</p></td>
<td><p>315504</p></td>
<td><p>330213003</p></td>
<td><p>1居民区，28行政村</p></td>
<td><p>江宁路101号</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>西坞街道</p></td>
<td><p>108.39平方千米</p></td>
<td><p>4.0万</p></td>
<td><p>315505</p></td>
<td><p>330213004</p></td>
<td><p>1社区，1居民区，23行政村</p></td>
<td><p>西坞南路18号</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>萧王庙街道</p></td>
<td><p>86.13平方千米</p></td>
<td><p>3.4万</p></td>
<td><p>315503</p></td>
<td><p>330213005</p></td>
<td><p>1居民区，21行政村</p></td>
<td><p>长寿路6号</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>方桥街道</p></td>
<td><p>26.99平方千米</p></td>
<td><p>2.3万</p></td>
<td><p>315514</p></td>
<td><p>330213006</p></td>
<td><p>1居民区，24行政村</p></td>
<td><p>方港路16号</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>溪口镇</p></td>
<td><p>368.14平方千米</p></td>
<td><p>7.9万</p></td>
<td><p>315502</p></td>
<td><p>330213100</p></td>
<td><p>4居民区，55行政村</p></td>
<td><p>溪口中山路8号</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>尚田镇</p></td>
<td><p>154.64平方千米</p></td>
<td><p>4.0万</p></td>
<td><p>315511</p></td>
<td><p>330213103</p></td>
<td><p>1居民区，42行政村</p></td>
<td><p>尚田镇西路14号</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>莼湖镇</p></td>
<td><p>145.32平方千米</p></td>
<td><p>6.0万</p></td>
<td><p>315506</p></td>
<td><p>330213104</p></td>
<td><p>3居民区，51行政村</p></td>
<td><p>莼湖横街28号</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>裘村镇</p></td>
<td><p>89.53平方千米</p></td>
<td><p>2.6万</p></td>
<td><p>315507</p></td>
<td><p>330213106</p></td>
<td><p>1居民区，18行政村</p></td>
<td><p>裘村银河路86号</p></td>
<td></td>
</tr>
<tr class="even">
<td><p>大堰镇</p></td>
<td><p>129.19平方千米</p></td>
<td><p>2.6万</p></td>
<td><p>315509</p></td>
<td><p>330213107</p></td>
<td><p>1居民区，40行政村</p></td>
<td><p>大堰村大登路8号</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p>松岙镇</p></td>
<td><p>51.75平方千米</p></td>
<td><p>1.3万</p></td>
<td><p>315507</p></td>
<td><p>330213108</p></td>
<td><p>1居民区，12行政村</p></td>
<td><p>松岙振兴路80号</p></td>
<td></td>
</tr>
</tbody>
</table>

## 地理与气候

奉化西部处于[天台山脉与](../Page/天台山_\(浙江\).md "wikilink")[四明山脉交接地带](../Page/四明山_\(浙江\).md "wikilink")，多高山峻岭，[黄泥浆岗海拔](../Page/黄泥浆岗.md "wikilink")976米，为境内最高峰。东北部地势平坦，河网纵横，属[宁奉平原的一部分](../Page/宁奉平原.md "wikilink")。西南多山区和河谷，沿海尚有小块狭长低平地带。全区海岸线长61公里，岛屿24个。

奉化属[亚热带季风气候](../Page/亚热带季风气候.md "wikilink")，年均气温16.3°C，降雨量1350至1600毫米，日照时数1850小时，无霜期232天。

## 人物

奉化中部的[溪口镇是](../Page/溪口镇_\(宁波市\).md "wikilink")[中华民国前总统](../Page/中华民国.md "wikilink")[蒋中正](../Page/蒋中正.md "wikilink")、[蒋经国父子的故乡](../Page/蒋经国.md "wikilink")。他们出生和成长于此处，并且在后来的政治生涯中也经常来此居住。溪口镇旁的[雪窦山上亦有多处与蒋氏相关的建筑和景点](../Page/雪窦山.md "wikilink")。附近的[滕头村则被](../Page/滕头村.md "wikilink")[联合国命名为生态村](../Page/联合国.md "wikilink")。

奉化古代名人有：[戴表元](../Page/戴表元.md "wikilink")、[莊崧甫](../Page/莊崧甫.md "wikilink")

奉化近代名人还有[奉化三俞](../Page/奉化三俞.md "wikilink")（即：[俞飞鹏](../Page/俞飞鹏.md "wikilink")、[俞济时和](../Page/俞济时.md "wikilink")[俞国华](../Page/俞国华.md "wikilink")）、[王任叔](../Page/王任叔.md "wikilink")（巴人）、[毛思诚](../Page/毛思诚.md "wikilink")、[邬华扬](../Page/邬华扬.md "wikilink")、[鄔維庸](../Page/鄔維庸.md "wikilink")、中国奥运之父[王正廷](../Page/王正廷.md "wikilink")、红帮裁缝先驱[王才运](../Page/王才运.md "wikilink")。

奉化现代名人：中华民国第二十六任行政院长[毛治国](../Page/毛治国.md "wikilink")、[网易总裁](../Page/网易.md "wikilink")[丁磊](../Page/丁磊.md "wikilink")，[银泰商业董事长兼总裁](../Page/银泰商业.md "wikilink")[沈国军](../Page/沈国军.md "wikilink")。

## 经济

奉化的[服装制造业非常发达](../Page/服装.md "wikilink")，在各乡镇都建有大型服装厂，著名的品牌有“罗蒙”西服、“荣昌翔”等。

[手机制造商](../Page/手机.md "wikilink")[波导曾经是奉化经济的主体](../Page/波导股份.md "wikilink")。

## 地理

境内除西南部为四明山脉丘陵地形外，其余皆属平原，水网密布。全区年平均气温16℃，降水量1400毫米。农业也区分为平原和山区两种类型。平原主产[水稻](../Page/水稻.md "wikilink")、[芋艿等](../Page/芋艿.md "wikilink")，山区则盛产[竹](../Page/竹.md "wikilink")、[茶叶](../Page/茶叶.md "wikilink")、[杨梅和](../Page/杨梅.md "wikilink")[水蜜桃](../Page/水蜜桃.md "wikilink")。

## 物产

  - 金属资源有铅、锌，非金属资源有磷、明矾石、砂、花岗岩、紫砂土等。
  - 高等植物有185科，1500种，如银杏、杜仲。
  - 动物有兽类、鸟类、爬行类、两栖类、鱼类、贝类、甲壳类等千种。
  - 土特产品有水蜜桃、芋艿头、蚶子、羊尾笋干、溪口千层饼。

## 文化和旅游

名胜古迹有溪口镇景区、雪窦山景区、亭下湖景区、大桥镇锦屏山景区。其中，溪口-滕头旅游区是[国家5A旅游景区](../Page/国家5A旅游景区.md "wikilink")。

  - [国家重点风景名胜区](../Page/国家重点风景名胜区.md "wikilink")：[雪窦山](../Page/雪窦山.md "wikilink")
  - [全国重点文物保护单位](../Page/全国重点文物保护单位.md "wikilink")：[蒋氏故居](../Page/蒋氏故居.md "wikilink")

### 图集

Fenghao House, 2019-04-13 05.jpg|丰镐房 Second storey of Chiang Kai Shek's
house Miaogaotai in Xikou, Fenghua, Ningbo, Zhejiang, China -
20061230.jpg |妙高台 Wuling School, 2019-04-13 06.jpg|武岭学校礼堂 Blood for
Blood by Chiang Ching-kuo.jpg|蒋经国手书“以血洗血”，摄于奉化溪口 Chiang Ching-kuo and
Chiang
Kai-shek.jpg|[蔣經國和](../Page/蔣經國.md "wikilink")[蔣介石](../Page/蔣介石.md "wikilink")
Deeds of the Zen Masters
Hotei.jpg|[布袋和尚契此和尚与](../Page/布袋和尚.md "wikilink")[蔣宗霸](../Page/蔣宗霸.md "wikilink")

## 相邻县市区

## 参考文献

## 外部链接

  - [宁波市奉化区人民政府](http://www.fh.gov.cn/)

{{-}}

[宁波](../Page/category:浙江省市辖区.md "wikilink")

[奉化区](../Category/奉化区.md "wikilink")
[区](../Category/宁波区县市.md "wikilink")
[浙](../Category/国家卫生城市.md "wikilink")

1.