[Millennium_City_5_2014.jpg](https://zh.wikipedia.org/wiki/File:Millennium_City_5_2014.jpg "fig:Millennium_City_5_2014.jpg")
[HK_Kwun_Tong_APM_BEA_Tower_Elevators.JPG](https://zh.wikipedia.org/wiki/File:HK_Kwun_Tong_APM_BEA_Tower_Elevators.JPG "fig:HK_Kwun_Tong_APM_BEA_Tower_Elevators.JPG")
[HK_Millennium_City_5_Office_Lobby.jpg](https://zh.wikipedia.org/wiki/File:HK_Millennium_City_5_Office_Lobby.jpg "fig:HK_Millennium_City_5_Office_Lobby.jpg")
[HK_apm_Void_2008view.jpg](https://zh.wikipedia.org/wiki/File:HK_apm_Void_2008view.jpg "fig:HK_apm_Void_2008view.jpg")
**創紀之城五期**（****）是[香港的一座購物中心與辦公室混合的建築物](../Page/香港.md "wikilink")，位於[九龍東](../Page/九龍東.md "wikilink")[觀塘](../Page/觀塘.md "wikilink")[觀塘道](../Page/觀塘道.md "wikilink")418號，連接[港鐵](../Page/港鐵.md "wikilink")[觀塘站A](../Page/觀塘站.md "wikilink")2出口，屬於[創紀之城第四個發展項目](../Page/創紀之城.md "wikilink")\[1\]。創紀之城五期由兩個建築部分組成，分別為[apm商場以及東亞銀行中心寫字樓](../Page/apm_\(香港\).md "wikilink")，為[創紀之城所有項目中規模最大的一個](../Page/創紀之城.md "wikilink")\[2\]。

## 入口及行人天橋

創紀之城五期共有四條[冷氣行人天橋連接](../Page/冷氣.md "wikilink")，分別連接[港鐵](../Page/港鐵.md "wikilink")[觀塘站](../Page/觀塘站.md "wikilink")、[裕民坊與觀塘道](../Page/裕民坊.md "wikilink")（東行）、[巧明街的港貿中心以及開源道的](../Page/巧明街.md "wikilink")[鱷魚恤中心](../Page/鱷魚恤中心.md "wikilink")。此外，行人可從觀塘道正門進入創紀之城五期。

而連接港鐵站的天橋亦被重建，重建後的天橋比原本的擴闊了一倍多。

## apm

apm是一個大型購物商場，為創紀之城五期的商場部分，其範圍除了在創紀之城五期外，亦包括[港貿中心大堂的一層](../Page/港貿中心.md "wikilink")。發展商為[新鴻基地產](../Page/新鴻基地產.md "wikilink")。apm早於2005年3月落成試業，並於2005年7月17日正式開幕。

## 東亞銀行中心

東亞銀行中心於2004年11月落成，寫字樓面積超過74萬平方呎，其中15層為[東亞銀行後勤總部](../Page/東亞銀行.md "wikilink")，寫字樓高層可以觀看到鯉魚門和[維多利亞港的景色](../Page/維多利亞港.md "wikilink")。

2002年6月28日，東亞銀行耗資13.3億元，呎價3280元，購入其中15層共40.67萬平方呎為東亞銀行後勤總部，並取得該大樓的[命名權](../Page/命名權.md "wikilink")。內設資訊科技部、人力資源部、培訓及發展部、核數部、財務管理部和設施管理部等各個後勤部門，容納約2,000名員工。寫字樓大堂位於2樓，大樓的7樓設有一個368座位的演講廳，及可容納300人的演講前廳，用於東亞銀行員工培訓活動和舉辦客戶投資講座。42樓是員工飯堂。

2018年6月[星巴克以每呎](../Page/星巴克.md "wikilink")40元租用[創紀之城五期](../Page/創紀之城五期.md "wikilink")12樓全層樓面，以及13樓部分樓層，合共面積約3.5萬平方呎，

## 來往交通

## 鄰近屋苑

  - [凱匯](../Page/凱匯.md "wikilink")
  - [觀月樺峰](../Page/觀月樺峰.md "wikilink")
  - [麗港城](../Page/麗港城.md "wikilink")
  - [鯉安苑](../Page/鯉安苑.md "wikilink")
  - [匯景花園](../Page/匯景花園.md "wikilink")

## 參考文獻及註釋

<div class="references-small">

<references>

</references>

</div>

## 外部链接

  - [apm官方網站](http://www.apm-millenniumcity.com/)
  - [創紀之城官方網站](http://www.millenniumcity.hk/)
  - [In-media民間記者報道/評論文章](https://web.archive.org/web/20080503162425/http://www.inmediahk.net/public/article?item_id=81387&group_id=11)

[Category:觀塘](../Category/觀塘.md "wikilink")
[Category:觀塘道](../Category/觀塘道.md "wikilink")
[Category:觀塘區商場](../Category/觀塘區商場.md "wikilink")
[Category:觀塘區寫字樓](../Category/觀塘區寫字樓.md "wikilink")
[Category:香港摩天大樓](../Category/香港摩天大樓.md "wikilink")
[Category:新鴻基地產物業](../Category/新鴻基地產物業.md "wikilink")
[Category:東亞銀行](../Category/東亞銀行.md "wikilink")

1.  由於[四的禁忌](../Page/四的禁忌.md "wikilink")，故跳過四期而改稱五期。
2.  [星級主力租戶進駐 APM 2005
    年全港最新購物熱點](http://www.shkp.com.hk/zh-hk/scripts/news/news_press_detail.php?press_id=3174)