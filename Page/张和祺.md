**张和祺**，[浙江](../Page/浙江.md "wikilink")[镇海人](../Page/镇海.md "wikilink")，[中国](../Page/中国.md "wikilink")[天体物理学家](../Page/天体物理学.md "wikilink")，中国[863计划空间天文首席科学家](../Page/863计划.md "wikilink")，曾任[紫金山天文台台长](../Page/紫金山天文台.md "wikilink")，是中国[空间天文学开创人之一](../Page/空间天文学.md "wikilink")。

## 简历

  - 1951年7月，[镇海中学](../Page/镇海中学.md "wikilink")（现蛟川书院）毕业（初中）
  - 1954年7月，[宁波中学毕业](../Page/宁波中学.md "wikilink")（高中）。
  - 1958年，毕业于[复旦大学物理系](../Page/复旦大学.md "wikilink")，后工作于[中国科学院紫金山天文台](../Page/中国科学院.md "wikilink")。
  - 1978年，任中国科学院紫金山天文台副研究员。
  - 1985年，任中国科学院紫金山天文台研究员。后历任台长、[博士生导师](../Page/博士生导师.md "wikilink")。
  - 1986年，任[美国](../Page/美国.md "wikilink")[新罕伯什尔大学客座](../Page/新罕伯什尔大学.md "wikilink")[教授](../Page/教授.md "wikilink")。
  - 1985年－1989年，任《天文学报》主编。
  - 1980年－1982年，任《[中国大百科全书](../Page/中国大百科全书.md "wikilink")─天文学卷》空间天文学分主编。
  - 1991年4月─1996年9月，任中国科学院紫金山天文台台长。

## 任职

担任有：[中国空间科学学会常务理事](../Page/中国空间科学学会.md "wikilink")，[中国天文学会秘书长](../Page/中国天文学会.md "wikilink")，中国天文学会常务理事，[美国行星学会国外名誉会员](../Page/美国行星学会.md "wikilink")。

## 学术

  - 1959年，记录到百年罕见特大[辉斑完整光谱资料](../Page/辉斑.md "wikilink")，并得到可靠分析结果，引起国际同行注意
  - 最早提出超高能[宇宙线电子流强涨落可能性](../Page/宇宙线.md "wikilink")。
  - 最早提出X射线类星体内禀[光度与](../Page/光度.md "wikilink")[红移关系](../Page/红移.md "wikilink")。

## 著作

  - 《超高能宇宙线电子在银河中的传播》
  - 《Studies on X-Ray Quasars》
  - 《Studies on Post-Flare Loop Promienence of 1981 April 27》

## 获奖

  - 1985年，因关于[X射线脉冲星的研究](../Page/X射线脉冲星.md "wikilink")，获得[国家教委科技成果二等奖](../Page/国家教委.md "wikilink")。
  - 1979年，因[太阳耀斑脉冲相动力学过程](../Page/太阳耀斑.md "wikilink")，以及宇宙[伽玛射线暴的研究获得](../Page/伽玛射线.md "wikilink")[中国科学院科技成果三等奖和](../Page/中国科学院.md "wikilink")[江苏省科技成果三等奖](../Page/江苏省.md "wikilink")。

## 参考

  - [天文学、地质学名人
    张和祺（1935－　）](https://web.archive.org/web/20070927045340/http://www.liuqiaoyun.com.cn/ZXLDMR/P_25.htm)
  - [张和祺：探索宇宙奥秘的人](http://culture.zjol.com.cn/05culture/system/2005/12/29/006423132.shtml)

[Z张](../Category/中国物理学家.md "wikilink")
[Z张](../Category/中国天文学家.md "wikilink")
[Z张](../Category/镇海县人.md "wikilink")
[Category:镇海中学校友](../Category/镇海中学校友.md "wikilink")
[Category:复旦大学校友](../Category/复旦大学校友.md "wikilink")
[HE](../Category/张姓.md "wikilink")
[Category:宁波中学校友](../Category/宁波中学校友.md "wikilink")