**變形菌門**（Proteobacteria）是[細菌中主要的一](../Page/細菌.md "wikilink")[門](../Page/门_\(生物\).md "wikilink")，包括很多[病原菌](../Page/病原菌.md "wikilink")，如[大腸杆菌](../Page/大腸杆菌.md "wikilink")、[沙門氏菌](../Page/沙門氏菌.md "wikilink")、[霍乱弧菌](../Page/霍乱弧菌.md "wikilink")、[幽门螺杆菌等著名的](../Page/幽门螺杆菌.md "wikilink")[属](../Page/属_\(生物\).md "wikilink")。\[1\]也有自由生活（非[寄生](../Page/寄生.md "wikilink")）的種類，包括很多可以進行[固氮的細菌](../Page/固氮.md "wikilink")。

[卡尔·乌斯於](../Page/卡尔·乌斯.md "wikilink")1987年建立這個群組，非正式的稱這是“[紫細菌及其親屬](../Page/紫細菌.md "wikilink")”。\[2\]變形菌門主要是由[核糖體](../Page/核糖體.md "wikilink")[RNA序列定義的](../Page/RNA.md "wikilink")，名稱取自[希臘神話中能夠變形的神](../Page/希臘神話.md "wikilink")[普羅透斯](../Page/普羅透斯.md "wikilink")（這同時也是變形菌門中[變形桿菌屬的名字](../Page/變形桿菌屬.md "wikilink")），因爲該門細菌的形狀具有極爲多樣的形狀。\[3\]\[4\]

## 特性

所有的變形菌門細菌爲[革蘭氏陰性菌](../Page/革蘭氏陰性菌.md "wikilink")，其[外膜主要由](../Page/外膜.md "wikilink")[脂多糖組成](../Page/脂多糖.md "wikilink")。很多種類利用[鞭毛運動](../Page/鞭毛.md "wikilink")，但有一些非運動性的種類，或者依靠[滑行來運動](../Page/滑行_\(細菌\).md "wikilink")。此外還有一類獨特的[粘细菌](../Page/粘细菌.md "wikilink")，可以聚集形成多細胞的[子實體](../Page/子實體.md "wikilink")。變形菌門包含多種[代謝種類](../Page/代謝.md "wikilink")。大多數細菌營兼性或者專性[厭氧及](../Page/厭氧.md "wikilink")[異養或者](../Page/異養.md "wikilink")[自養](../Page/自養.md "wikilink")[化能生物生活](../Page/化能生物.md "wikilink")，但有很多例外。很多並非緊密相關的屬可以利用[光合作用儲存能量](../Page/光合作用.md "wikilink")。因其多數具有紫紅色的色素，被稱爲[紫細菌](../Page/紫細菌.md "wikilink")。

## 分類

變形菌門根據[rRNA序列被分爲五類](../Page/rRNA.md "wikilink")（通常作爲五個綱），用[希臘字母α](../Page/希臘字母.md "wikilink")、β、γ、δ和ε命名。其中有的類別可能是[併系的](../Page/并系群.md "wikilink")。

[α-變形菌除包括光合的種類外](../Page/α-變形菌.md "wikilink")，還有代謝[C1化合物的種類](../Page/C1化合物.md "wikilink")、和植物共生的細菌（如[根瘤菌屬](../Page/根瘤菌屬.md "wikilink")）、與動物共生的細菌和一類危險的致病菌[立克次體目](../Page/立克次體目.md "wikilink")。此外真核生物的[線粒體的前身也很可能屬於這一類](../Page/線粒體.md "wikilink")（參見[內共生學說](../Page/內共生學說.md "wikilink")）。

[β-變形菌包括很多](../Page/β-變形菌.md "wikilink")[好氧或兼性細菌](../Page/好氧.md "wikilink")，通常其降解能力可變，但也有一些無機化能種類（如可以氧化氨的亞硝化單胞菌屬(*Nitrosomonas*)）和光合種類（[紅環菌屬](../Page/紅環菌屬.md "wikilink")(*Rhodocyclus*)和[紅長命菌屬](../Page/紅長命菌屬.md "wikilink")(*Rubrivivax*)）。很多種類可以在環境樣品中發現，如廢水或土壤中。該綱的致病菌有[奈氏球菌目](../Page/奈氏球菌目.md "wikilink")(Neisseriales)的中一些細菌（可導致[淋病和](../Page/淋病.md "wikilink")[腦膜炎](../Page/腦膜炎.md "wikilink")）和[伯克氏菌屬](../Page/伯克氏菌屬.md "wikilink")(*Burkholderia*)。
在海洋中很少能發現β-變形菌。

[γ-變形菌包括一些醫學上和科學研究中很重要的類群](../Page/γ-變形菌.md "wikilink")，如[腸桿菌科](../Page/腸桿菌科.md "wikilink")(Enterobacteraceae)、[弧菌科](../Page/弧菌科.md "wikilink")(Vibrionaceae)和[假單胞菌科](../Page/假單胞菌科.md "wikilink")(Pseudomonadaceae)。很多重要的病原菌屬於這個綱，如[沙門氏菌屬](../Page/沙門氏菌.md "wikilink")(*Salmonella*)（腸炎和傷寒）、[耶爾辛氏菌屬](../Page/鼠疫桿菌.md "wikilink")(*Yersinia*)（鼠疫）、[弧菌屬](../Page/弧菌.md "wikilink")(*Vibrio*)（霍亂）、銅綠[假單胞菌](../Page/假單胞菌.md "wikilink")(*Pseudomonas
aeruginosa*)（就醫時引發的肺部感染或者囊性纖維化）。重要的模式生物[大腸桿菌也屬於此綱](../Page/大腸桿菌.md "wikilink")。

[δ-變形菌包括基本好氧的形成子實體的](../Page/δ-變形菌.md "wikilink")[粘细菌和嚴格厭氧的一些種類](../Page/粘细菌.md "wikilink")，如[硫酸鹽還原菌](../Page/硫酸鹽還原菌.md "wikilink")（[脫硫弧菌屬](../Page/脫硫弧菌屬.md "wikilink")(*Desulfovibrio*)、[脫硫菌屬](../Page/脫硫菌屬.md "wikilink")(*Desulfobacter*)、[脫硫球菌屬](../Page/脫硫球菌屬.md "wikilink")(*Desulfococcus*)、[脫硫線菌屬](../Page/脫硫線菌屬.md "wikilink")(*Desulfonema*)等）和[硫還原菌](../Page/硫還原菌.md "wikilink")（如除硫單胞菌屬(*Desulfuromonas*)），以及具有其它生理特徵的厭氧細菌，如還原三價鐵的[地桿菌屬](../Page/地桿菌屬.md "wikilink")(*Geobacter*)和共生的[暗桿菌屬](../Page/暗桿菌屬.md "wikilink")(*Pelobacter*)和[互營菌屬](../Page/互營菌屬.md "wikilink")(*Syntrophus*)。

[ε-變形菌只有少數幾個屬](../Page/ε-變形菌.md "wikilink")，多數是彎曲或螺旋形的細菌，如[沃林氏菌屬](../Page/沃林氏菌屬.md "wikilink")(*Wolinella*)、[螺桿菌屬](../Page/螺桿菌屬.md "wikilink")(*Helicobacter*)和[彎曲菌屬](../Page/彎曲菌屬.md "wikilink")(*Campylobacter*)。它們都生活在動物或人的消化道中，爲共生菌（沃林氏菌在牛中）或致病菌（螺桿菌在胃中或彎曲菌在十二指腸中）。

## 參見

  - [細菌分類表\#變形菌門(Proteobacteria)](../Page/細菌分類表#變形菌門\(Proteobacteria\).md "wikilink")
  - [紫細菌](../Page/紫細菌.md "wikilink")

## 參考文獻

[Category:变形菌门](../Category/变形菌门.md "wikilink")
[Category:革兰氏阴性菌](../Category/革兰氏阴性菌.md "wikilink")

1.
2.
3.
4.