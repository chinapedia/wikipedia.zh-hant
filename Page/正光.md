**正光**（520年七月—525年六月）是[北魏的君主孝明帝](../Page/北魏.md "wikilink")[元詡的第三个](../Page/元詡.md "wikilink")[年号](../Page/年号.md "wikilink")，共计近5年。

## 大事记

## 出生

## 逝世

## 纪年

| 正光                               | 元年                             | 二年                             | 三年                             | 四年                             | 五年                             | 六年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 520年                           | 521年                           | 522年                           | 523年                           | 524年                           | 525年                           |
| [干支](../Page/干支纪年.md "wikilink") | [庚子](../Page/庚子.md "wikilink") | [辛丑](../Page/辛丑.md "wikilink") | [壬寅](../Page/壬寅.md "wikilink") | [癸卯](../Page/癸卯.md "wikilink") | [甲辰](../Page/甲辰.md "wikilink") | [乙巳](../Page/乙巳.md "wikilink") |

## 參看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 同期存在的其他政权年号
      - [普通](../Page/普通_\(萧衍\).md "wikilink")（520年正月—527年三月）：[南朝梁梁武帝](../Page/南朝梁.md "wikilink")[萧衍的年号](../Page/萧衍.md "wikilink")
      - [真王](../Page/真王_\(破六韩拔陵\).md "wikilink")（523年三月—525年六月）：[北魏時期](../Page/北魏.md "wikilink")[六鎮領導](../Page/六鎮.md "wikilink")[破六韩拔陵年号](../Page/破六韩拔陵.md "wikilink")
      - [天建](../Page/天建.md "wikilink")（524年六月—527年九月）：[北魏時期領導](../Page/北魏.md "wikilink")[莫折念生年号](../Page/莫折念生.md "wikilink")
      - [天啓](../Page/天启_\(元法僧\).md "wikilink")（525年正月—三月）：[北魏時期領導](../Page/北魏.md "wikilink")[元法僧年号](../Page/元法僧.md "wikilink")
      - [真王](../Page/真王_\(杜洛周\).md "wikilink")（525年八月—528年二月）：[北魏時期領導](../Page/北魏.md "wikilink")[杜洛周年号](../Page/杜洛周.md "wikilink")
      - [神嘉](../Page/神嘉.md "wikilink")（525年十二月—535年三月）：[北魏時期領導](../Page/北魏.md "wikilink")[劉蠡升年号](../Page/劉蠡升.md "wikilink")
      - [建昌](../Page/建昌_\(柔然\).md "wikilink")（508年-520年）：[柔然政权豆罗伏跋豆伐可汗](../Page/柔然.md "wikilink")[丑奴年号](../Page/丑奴.md "wikilink")
      - [義熙](../Page/义熙_\(麴嘉\).md "wikilink")：[高昌政权](../Page/高昌.md "wikilink")[麴嘉年号](../Page/麴嘉.md "wikilink")
      - [甘露](../Page/甘露_\(麴光\).md "wikilink")：[高昌政权](../Page/高昌.md "wikilink")[麴光年号](../Page/麴光.md "wikilink")

## 參考文獻

  - 李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129

[Category:北魏年号](../Category/北魏年号.md "wikilink")
[Category:6世纪中国年号](../Category/6世纪中国年号.md "wikilink")
[Category:520年代中国政治](../Category/520年代中国政治.md "wikilink")