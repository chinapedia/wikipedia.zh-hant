[Dronning_victoria.jpg](https://zh.wikipedia.org/wiki/File:Dronning_victoria.jpg "fig:Dronning_victoria.jpg")，1837年登基后搬入，成为第一位入驻白金汉宫的英王\]\]

**白金汉宫**（）是[英国君主位于伦敦的主要寝宫及办公处](../Page/英国君主.md "wikilink")。宫殿坐落在大倫敦[西敏市](../Page/西敏市.md "wikilink")，是国家庆典和王室欢迎礼举行场地之一，也是一处重要的旅游景点。在英国历史上的欢庆或危机时刻，白金汉宫也是一处重要的集会场所。

1703年至1705年，[白金汉和诺曼比公爵](../Page/白金汉公爵.md "wikilink")在此兴建了一处大型镇厅建筑“白金汉屋”，构成了今天的主體建築，1761年，[喬治三世获得该府邸](../Page/喬治三世_\(英國\).md "wikilink")\[1\]，并作为一处私人寝宫。此后宫殿的扩建工程持续超过了75年，主要由[建筑师](../Page/建筑师.md "wikilink")[约翰·纳西和](../Page/约翰·纳西.md "wikilink")[爱德华·布罗尔主持](../Page/爱德华·布罗尔.md "wikilink")，为中央庭院构筑了三侧建筑。1837年，维多利亚女王登基后，白金汉宫成为英王正式宫寝。19世纪末20世纪初，宫殿公共立面修建，形成延续至今天白金汉宫形象。二战期间，宫殿礼拜堂遭一枚德国炸弹袭击而毁；在其址上建立的女王画廊于1962年向公众开放，展示皇家收藏品。现在的白金漢宮對外開放參觀，每天清晨都会進行著名的[禁衛軍交接典禮](../Page/英國御林軍.md "wikilink")，成为英国王室文化的一大景观。\[2\]

## 历史

[Buckingham_House_1710.jpeg](https://zh.wikipedia.org/wiki/File:Buckingham_House_1710.jpeg "fig:Buckingham_House_1710.jpeg")(William
Winde)為[白金汉公爵所設計](../Page/白金汉公爵.md "wikilink")。\]\]

  - 1703年至1705年，[白金汉公爵在此建造了白金漢屋](../Page/白金汉公爵.md "wikilink")。
  - 1762年：[乔治三世把该住宅买了下来](../Page/乔治三世.md "wikilink")，成为王后的私人宫殿。在隨後的75年主要由建築師[約翰·納西及](../Page/約翰·納西.md "wikilink")[愛德華·布羅爾](../Page/愛德華·布羅爾.md "wikilink")（Edward
    Blore）負責擴建，设计了三翼圍繞中央庭院的佈局。
  - 1825年：[乔治四世聘请著名建筑师](../Page/乔治四世.md "wikilink")[約翰·納西对白金汉府邸进行扩展](../Page/約翰·納西.md "wikilink")，并将其改名为白金汉宫。
  - 1837年：[维多利亚女王登上王位](../Page/维多利亚女王.md "wikilink")，皇室由[圣詹姆斯宫迁往白金汉宫](../Page/圣詹姆斯宫.md "wikilink")。这里成为英国王室的正式宫殿。
  - 1851年：对白金汉宫进行扩建，在正面增加了一座建筑。原正面入口处的大理石拱门（Marble
    Arch）被迁移到[海德公园](../Page/海德公园.md "wikilink")。
  - 1940年：在[第二次世界大战期间](../Page/第二次世界大战.md "wikilink")，白金汉宫遭到[德国空军的轰炸](../Page/納粹德國空軍.md "wikilink")，英国国王[乔治六世仍然留在宫中](../Page/乔治六世.md "wikilink")，以鼓励国民的[抗战勇气](../Page/抗战.md "wikilink")。
  - 1992年：[温莎城堡发生火灾](../Page/温莎城堡.md "wikilink")。为了筹建温莎城堡的修复经费，白金汉宫的主要大厅向公众开放。

## 結構

[Queens.guard.buck.palace.arp.jpg](https://zh.wikipedia.org/wiki/File:Queens.guard.buck.palace.arp.jpg "fig:Queens.guard.buck.palace.arp.jpg")\]\]

白金漢宮不同於許多國家首都的著名歷史建築，白金漢宮并不是只供遊人參觀的博物館。這座宮殿是君主制的中心，是女王陛下作為英國國家元首和英聯邦領袖辦公和履行禮儀性職責的地方。女王平常會在白金漢宮工作，通常在周末會回到溫莎城堡。要知女王在不在宮殿里，只要看一看白金漢宮中央的旗桿就可以了﹔因為如果女王在宮里，旗桿上飄揚的會是君主旗，否則人們便會看到聯合王國旗（即英國國旗）。如遇到盛大的禮儀場合，在天氣允許的情況下，宮殿則會掛一面加大號的旗幟。

通常在聖誕節和一月期間，女王會住在諾福克的桑德林漢，她的私人莊園。而八、九月則住在蘇格蘭高地上的巴爾莫勒爾堡。近年來，公眾可以趁女王公務日程的暑期間參觀白金漢宮的典禮廳，也就是其他時候用作官方職能和接見用的房間。迄今為止，白金漢宮已接待了來自世界各地的近400萬名參觀者。

白金汉宫的[建筑风格为](../Page/建筑风格.md "wikilink")[新古典主义](../Page/新古典主义.md "wikilink")，主体建筑为五层，其中两层为服务人员使用的附属层，高度较低。所以[立面可以视为纵](../Page/立面.md "wikilink")、横三段式处理。

白金汉宫建筑外立面装修材料为[巴斯](../Page/巴斯.md "wikilink")[石灰岩](../Page/石灰岩.md "wikilink")。內部裝修則以[人造大理石及](../Page/人造大理石.md "wikilink")[青金岩為主](../Page/青金岩.md "wikilink")。正面广场围以[铸铁栅栏](../Page/铸铁.md "wikilink")，为皇家卫队换岗仪式的场所。广场外为手持[权杖](../Page/权杖.md "wikilink")、塑造为[天使形象的](../Page/天使.md "wikilink")[维多利亚女王雕像](../Page/维多利亚女王.md "wikilink")。宫殿正面入口面向东北方，通过[林荫路](../Page/林蔭路_\(倫敦\).md "wikilink")（The
Mall）与[特拉法尔加广场相连](../Page/特拉法尔加.md "wikilink")。

白金汉宫的附属建筑包括皇家畫廊、[皇家馬廄和花园](../Page/英國皇家馬廄.md "wikilink")。皇家画廊和皇家马厩均对公众开放参观，其中[皇家畫廊内收藏有](../Page/英國皇家畫廊.md "wikilink")[鲁本斯](../Page/鲁本斯.md "wikilink")、[伦勃朗](../Page/伦勃朗.md "wikilink")、[弗美尔](../Page/弗美尔.md "wikilink")、[盖恩斯巴勒](../Page/盖恩斯巴勒.md "wikilink")、[卡拉内罗等人的绘画作品](../Page/卡拉内罗.md "wikilink")、[卡诺瓦等人的雕塑作品](../Page/卡诺瓦.md "wikilink")。

## 注释

## 文献

  - Blaikie, Thomas (2002). *You look awfully like the Queen: Wit and
    Wisdom from the House of Windsor*. London: Harper Collins. ISBN
    978-0-00-714874-5.

  - Goring, O.G. (1937). *From Goring House to Buckingham Palace*.
    London:Ivor Nicholson & Watson.

  - Harris, John; de Bellaigue, Geoffrey; & Miller, Oliver (1968).
    *Buckingham Palace*. London:Nelson. ISBN 978-0-17-141011-2

  - Healey, Edma (1997). *The Queen's House: A Social History of
    Buckingham Palace*. London:Penguin Group. ISBN 0718170893.

  - Hedley, Olwen (1971) *The Pictorial History of Buckingham Palace*.
    Pitkin, ISBN 978-0-85372-086-7

  -
  - Nash, Roy (1980). *Buckingham Palace: The Place and the People*.
    London: Macdonald Futura. ISBN 978-0-354-04529-2

  - Robinson, John Martin (1999). *Buckingham Palace*. Published by The
    Royal Collection, St. James's Palace, London ISBN 978-1-902163-36-9.

  - Williams, Neville (1971). *Royal Homes*. Lutterworth Press. ISBN
    978-0-7188-0803-7.

  - Woodham-Smith, Cecil (1973). *Queen Victoria* *(vol 1)* Hamish
    Hamilton Ltd.

  - Wright, Patricia (1999; first published 1996). *The Strange History
    of Buckingham Palace*. Stroud, Gloucs.: Sutton Publishing Ltd. ISBN
    978-0-7509-1283-9

## 註解

## 参见

  - [白宫](../Page/白宫.md "wikilink")
  - [威斯敏斯特宫](../Page/威斯敏斯特宫.md "wikilink")
  - [白厅](../Page/白厅.md "wikilink")
  - [肯辛頓宮](../Page/肯辛頓宮.md "wikilink")
  - [伦敦塔](../Page/伦敦塔.md "wikilink")
  - [英國皇家馬廄](../Page/英國皇家馬廄.md "wikilink")
  - [英国君主列表](../Page/英国君主列表.md "wikilink")

## 外部链接

  - [英国王室](http://www.royal.gov.uk/)

[Category:伦敦宫殿](../Category/伦敦宫殿.md "wikilink")
[Category:倫敦旅遊景點](../Category/倫敦旅遊景點.md "wikilink")
[Category:英國地標](../Category/英國地標.md "wikilink")

1.  Robinson, p. 14
2.  [白金漢宮](http://www.edutop.com/fedutop/fazhan-3-3.htm)，英國語言文化中心