[Ballads_of_bravery_(1877)_(14785021975).jpg](https://zh.wikipedia.org/wiki/File:Ballads_of_bravery_\(1877\)_\(14785021975\).jpg "fig:Ballads_of_bravery_(1877)_(14785021975).jpg")

[Bedivere.jpg](https://zh.wikipedia.org/wiki/File:Bedivere.jpg "fig:Bedivere.jpg")畫作。\]\]

**王者之劍**（英語：**Excalibur**，又譯為**斷鋼聖劍**、**斬鐵劍**、**湖中劍**）據說持有該劍可帶來統治[不列顛的魔力](../Page/不列顛.md "wikilink")。在[威爾士傳說中又被稱為](../Page/威爾士.md "wikilink")**卡里德福洛斯**（**Caledfwlch**）。王者之劍是在[亞瑟王傳說中所登場的魔法聖劍](../Page/亞瑟王傳說.md "wikilink")，可以稱得上是後世騎士文學中，英雄多半配持著名劍、寶劍傳統的開端。

## 歷史

在[亞瑟王傳說中](../Page/亞瑟王傳說.md "wikilink")，和Pellinore王的對決中，[亞瑟王在違反騎士道的狀況下拔劍而出](../Page/亞瑟王.md "wikilink")，卻因此讓[石中劍折斷了](../Page/石中劍.md "wikilink")，後來他在魔法師[梅林的指引下](../Page/梅林_\(亞瑟王傳說\).md "wikilink")，從[湖夫人的手中得到了王者之劍](../Page/湖中妖女.md "wikilink")。王者之劍在精靈國度[阿瓦隆所打造](../Page/阿瓦隆.md "wikilink")，[劍鍔由黃金所鑄](../Page/劍鍔.md "wikilink")、劍柄上鑲有寶石，並因其鋒刃削鐵如泥，故[湖夫人以](../Page/湖中妖女.md "wikilink")「Excalibur」（即古塞爾特語中「斷鋼」之意）命名之。此時梅林告誡亞瑟：「王者之劍雖強大，但其劍鞘卻較其劍更為貴重。配戴王者之劍的劍鞘者將永不流血，你絕不可遺失了它。」

但後來亞瑟王還是遺失劍鞘（被[摩根勒菲丟入湖中](../Page/摩根勒菲.md "wikilink")），因此他雖擁有削鐵如泥的劍，但最後仍被私生子兼外甥的騎士[莫德雷德重傷致死](../Page/莫德雷德.md "wikilink")。而王者之劍最後則在亞瑟王的囑咐下，由「斟酒人」[貝德維爾將軍投回湖中](../Page/貝德維爾.md "wikilink")，與亞瑟王一齊回到傳說中的仙境[阿瓦隆去](../Page/阿瓦隆.md "wikilink")。

## 湖中劍和石中劍

在殘存的古老亞瑟王傳說中，關於劍的起源傳奇分別有兩個版本。「石中劍」傳奇的原始起源來自於的詩歌魔法師「梅林」\[1\]。

Geoffrey of
Monmouth所写的《不列颠列王传》中说这把剑是在阿瓦隆铸造的并把本地语中的Caledfwlch加以拉丁化变成Caliburnus。之后他的著作传到欧洲大陆后变成Excalibur，其他中世纪的阿瑟王诗歌和编年史中的写法有：
Calabrun, Calabrum, Caliburn, Calibourne, Callibourc, Calliborc,
Calibourch,
Escaliborc,和Escalibor。这些故事也影响到了以圣杯文化著名的拉丁圣经文化圈，起初这些传奇中还包括梅林韵文但是后拉丁圣经圈的作者们去掉梅林的部分，而加上自创的亚瑟王早期人生及Excalibur的来源。具体来说，在拉丁圣经圈中的《梅林传》中明确写到从石中拔出的剑就是Excalibur，但是后来的版本中变成此剑是从湖中仙女处得到。\[2\]而《亚瑟王之死》将两个说法都写入从而产生两把剑。因此Excalibur和石中剑系屬同源。有观点认为“石中拔出剑”是在暗指冶金术。\[3\]

## 註釋

[E](../Category/亞瑟王傳說.md "wikilink")
[E](../Category/神話中的刀劍.md "wikilink")
[Category:虛構刀劍](../Category/虛構刀劍.md "wikilink")

1.  [Robert de Boron](http://en.wikipedia.org/wiki/Robert_de_Boron)
2.  Merlin: roman du XIIIe siècle ed. M. Alexandre (Geneva: Droz, 1979)
3.  デイヴィッド・デイ著/山本史郎訳：『アーサー王の世界』原書房