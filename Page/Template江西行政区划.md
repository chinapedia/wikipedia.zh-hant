[青云谱区](../Page/青云谱区.md "wikilink"){{.w}}[湾里区](../Page/湾里区.md "wikilink"){{.w}}[青山湖区](../Page/青山湖区.md "wikilink"){{.w}}[新建区](../Page/新建区.md "wikilink"){{.w}}[南昌县](../Page/南昌县.md "wikilink"){{.w}}[安义县](../Page/安义县.md "wikilink"){{.w}}[进贤县](../Page/进贤县.md "wikilink")

|group3 = [景德镇市](../Page/景德镇市.md "wikilink") |list3 =
[昌江区](../Page/昌江区.md "wikilink"){{.w}}[珠山区](../Page/珠山区.md "wikilink"){{.w}}[乐平市](../Page/乐平市.md "wikilink"){{.w}}[浮梁县](../Page/浮梁县.md "wikilink")

|group4 = [萍乡市](../Page/萍乡市.md "wikilink") |list4 =
[安源区](../Page/安源区.md "wikilink"){{.w}}[湘东区](../Page/湘东区.md "wikilink"){{.w}}[莲花县](../Page/莲花县.md "wikilink"){{.w}}[上栗县](../Page/上栗县.md "wikilink"){{.w}}[芦溪县](../Page/芦溪县.md "wikilink")

|group5 = [九江市](../Page/九江市.md "wikilink") |list5 =
[浔阳区](../Page/浔阳区.md "wikilink"){{.w}}[濂溪区](../Page/濂溪区.md "wikilink"){{.w}}[柴桑区](../Page/柴桑区.md "wikilink"){{.w}}[瑞昌市](../Page/瑞昌市.md "wikilink"){{.w}}[共青城市](../Page/共青城市.md "wikilink"){{.w}}[庐山市](../Page/庐山市.md "wikilink"){{.w}}[武宁县](../Page/武宁县.md "wikilink"){{.w}}[修水县](../Page/修水县.md "wikilink"){{.w}}[永修县](../Page/永修县.md "wikilink"){{.w}}[德安县](../Page/德安县.md "wikilink"){{.w}}[都昌县](../Page/都昌县.md "wikilink"){{.w}}[湖口县](../Page/湖口县.md "wikilink"){{.w}}[彭泽县](../Page/彭泽县.md "wikilink")

|group6 = [新余市](../Page/新余市.md "wikilink") |list6 =
[渝水区](../Page/渝水区.md "wikilink"){{.w}}[分宜县](../Page/分宜县.md "wikilink")

|group7 = [鹰潭市](../Page/鹰潭市.md "wikilink") |list7 =
[月湖区](../Page/月湖区.md "wikilink"){{.w}}[-{zh-hans:余江;
zh-hant:餘江;}-区](../Page/余江区.md "wikilink"){{.w}}[贵溪市](../Page/贵溪市.md "wikilink")

|group8 = [赣州市](../Page/赣州市.md "wikilink") |list8 =
[章贡区](../Page/章贡区.md "wikilink"){{.w}}[南康区](../Page/南康区.md "wikilink"){{.w}}[赣县区](../Page/赣县区.md "wikilink"){{.w}}[瑞金市](../Page/瑞金市.md "wikilink"){{.w}}[信丰县](../Page/信丰县.md "wikilink"){{.w}}[大余县](../Page/大余县.md "wikilink"){{.w}}[上犹县](../Page/上犹县.md "wikilink"){{.w}}[崇义县](../Page/崇义县.md "wikilink"){{.w}}[安远县](../Page/安远县.md "wikilink"){{.w}}[龙南县](../Page/龙南县.md "wikilink"){{.w}}[定南县](../Page/定南县.md "wikilink"){{.w}}[全南县](../Page/全南县.md "wikilink"){{.w}}[宁都县](../Page/宁都县.md "wikilink"){{.w}}[于都县](../Page/于都县.md "wikilink"){{.w}}[兴国县](../Page/兴国县.md "wikilink"){{.w}}[会昌县](../Page/会昌县.md "wikilink"){{.w}}[寻乌县](../Page/寻乌县.md "wikilink"){{.w}}[石城县](../Page/石城县.md "wikilink")

|group9 = [吉安市](../Page/吉安市.md "wikilink") |list9 =
[吉州区](../Page/吉州区.md "wikilink"){{.w}}[青原区](../Page/青原区.md "wikilink"){{.w}}[井冈山市](../Page/井冈山市.md "wikilink"){{.w}}[吉安县](../Page/吉安县.md "wikilink"){{.w}}[吉水县](../Page/吉水县.md "wikilink"){{.w}}[峡江县](../Page/峡江县.md "wikilink"){{.w}}[-{zh-hans:新干;
zh-hant:新幹;}-县](../Page/新干县.md "wikilink"){{.w}}[永丰县](../Page/永丰县.md "wikilink"){{.w}}[泰和县](../Page/泰和县.md "wikilink"){{.w}}[遂川县](../Page/遂川县.md "wikilink"){{.w}}[万安县](../Page/万安县.md "wikilink"){{.w}}[安福县](../Page/安福县.md "wikilink"){{.w}}[永新县](../Page/永新县.md "wikilink")

|group10 = [宜春市](../Page/宜春市.md "wikilink") |list10 =
[袁州区](../Page/袁州区.md "wikilink"){{.w}}[丰城市](../Page/丰城市.md "wikilink"){{.w}}[樟树市](../Page/樟树市.md "wikilink"){{.w}}[高安市](../Page/高安市.md "wikilink"){{.w}}[奉新县](../Page/奉新县.md "wikilink"){{.w}}[万载县](../Page/万载县.md "wikilink"){{.w}}[上高县](../Page/上高县.md "wikilink"){{.w}}[宜丰县](../Page/宜丰县.md "wikilink"){{.w}}[靖安县](../Page/靖安县.md "wikilink"){{.w}}[铜鼓县](../Page/铜鼓县.md "wikilink")

|group11 = [抚州市](../Page/抚州市.md "wikilink") |list11 =
[临川区](../Page/临川区.md "wikilink"){{.w}}[东乡区](../Page/东乡区.md "wikilink"){{.w}}[南城县](../Page/南城县.md "wikilink"){{.w}}[黎川县](../Page/黎川县.md "wikilink"){{.w}}[南丰县](../Page/南丰县.md "wikilink"){{.w}}[崇仁县](../Page/崇仁县.md "wikilink"){{.w}}[乐安县](../Page/乐安县.md "wikilink"){{.w}}[宜黄县](../Page/宜黄县.md "wikilink"){{.w}}[金溪县](../Page/金溪县.md "wikilink"){{.w}}[资溪县](../Page/资溪县.md "wikilink"){{.w}}[广昌县](../Page/广昌县.md "wikilink")

|group12 = [上饶市](../Page/上饶市.md "wikilink") |list12 =
[信州区](../Page/信州区.md "wikilink"){{.w}}[广丰区](../Page/广丰区.md "wikilink"){{.w}}[德兴市](../Page/德兴市.md "wikilink"){{.w}}[上饶县](../Page/上饶县.md "wikilink"){{.w}}[玉山县](../Page/玉山县.md "wikilink"){{.w}}[铅山县](../Page/铅山县.md "wikilink"){{.w}}[-{zh-hans:横峰;
zh-hant:横峯;}-县](../Page/横峰县.md "wikilink"){{.w}}[弋阳县](../Page/弋阳县.md "wikilink"){{.w}}[-{zh-hans:余干;
zh-hant:餘干;}-县](../Page/余干县.md "wikilink"){{.w}}[鄱阳县](../Page/鄱阳县.md "wikilink"){{.w}}[万年县](../Page/万年县.md "wikilink"){{.w}}[婺源县](../Page/婺源县.md "wikilink")
}}

|belowstyle = text-align: left; font-size: 80%; |below =
参见：[中华人民共和国县级以上行政区列表](../Page/中华人民共和国县级以上行政区列表.md "wikilink")、[江西省乡级以上行政区列表](../Page/江西省乡级以上行政区列表.md "wikilink")
}}<noinclude>

</noinclude>

[Category:中华人民共和国各省级行政区行政区划模板](../Category/中华人民共和国各省级行政区行政区划模板.md "wikilink")
[\*](../Category/江西行政区划.md "wikilink")
[江西行政区划模板](../Category/江西行政区划模板.md "wikilink")