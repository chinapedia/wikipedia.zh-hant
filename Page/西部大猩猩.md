**西部大猩猩**（*Gorilla gorilla*\[1\]）是為人熟悉的[大猩猩](../Page/大猩猩.md "wikilink")。

## 分類

[Gorilla_Male_perspective_5.jpg](https://zh.wikipedia.org/wiki/File:Gorilla_Male_perspective_5.jpg "fig:Gorilla_Male_perspective_5.jpg")
西部大猩猩可分成两个[亚种](../Page/亚种.md "wikilink")，即[西部低地大猩猩](../Page/西部低地大猩猩.md "wikilink")（*G.
g. gorilla*）及[克羅斯河大猩猩](../Page/克羅斯河大猩猩.md "wikilink")（*G. g. diehli*）。

大部份西部大猩猩都是屬於[西部低地大猩猩](../Page/西部低地大猩猩.md "wikilink")，數量約有94000頭。而[克羅斯河大猩猩的數量則少於](../Page/克羅斯河大猩猩.md "wikilink")300頭。\[2\]

## 外表

西部大猩猩的毛色較[東部大猩猩淺色](../Page/東部大猩猩.md "wikilink")。[西部低地大猩猩是褐色及灰色的](../Page/西部低地大猩猩.md "wikilink")，前額呈紅色。牠的鼻端起鈎，但東部大猩猩的則沒有。雄性高1.7-1.8米及重140-180公斤；雌性高1.4-1.5米及重60-100公斤。西部大猩猩較東部大猩猩纖細。[克羅斯河大猩猩在](../Page/克羅斯河大猩猩.md "wikilink")[頭顱骨及](../Page/頭顱骨.md "wikilink")[牙齒尺寸都與西部低地大猩猩](../Page/牙齒.md "wikilink")。

## 簡介

西部大猩猩可以靈活的攀樹，且較[東部大猩猩多生活在樹上](../Page/東部大猩猩.md "wikilink")。牠喜歡吃[生果](../Page/生果.md "wikilink")，差不多100種不同樹木的果實牠都會吃。西部大猩猩難以追蹤及研究。

[西部低地大猩猩的族群很細小](../Page/西部低地大猩猩.md "wikilink")，只有4-8個成員。野生的西部大猩猩是會使用工具的。\[3\]

## 保育

[世界自然保護聯盟將西部大猩猩列為](../Page/世界自然保護聯盟.md "wikilink")[極危物種](../Page/極危物種.md "wikilink")，差不多接近[滅絕階段](../Page/滅絕.md "wikilink")。[埃博拉病毒在西部大猩猩的族群中蔓延](../Page/埃博拉病毒.md "wikilink")，令牠們的數量根本不可能增長。\[4\]

## 參考

[Category:大猩猩](../Category/大猩猩.md "wikilink")

1.

2.

3.  [PLOS Journal "First Observation of Tool Use in Wild
    Gorillas"](http://biology.plosjournals.org/perlserv/?request=get-document&doi=10.1371/journal.pbio.0030380)

4.