**海龍科**（學名Thalattosauridae），[希臘文意思為](../Page/希臘文.md "wikilink")“海洋蜥蜴”，是群已滅絕[雙孔類](../Page/雙孔類.md "wikilink")[海生爬行動物](../Page/海生爬行動物.md "wikilink")，生存於晚[三疊紀](../Page/三疊紀.md "wikilink")[北美的](../Page/北美.md "wikilink")[太平洋海岸](../Page/太平洋.md "wikilink")。牠們的大多數化石發現於[加州](../Page/加州.md "wikilink")。

典型的海龍類外表類似大型[蜥蜴](../Page/蜥蜴.md "wikilink")，身長可達2公尺，這群動物半數有長而側向平坦的尾巴。儘管外表類似蜥蜴，海龍類與其他[雙孔動物的關係非常不明確](../Page/雙孔動物.md "wikilink")，大多數專家暫時將牠們置於[主龍類與](../Page/主龍類.md "wikilink")[魚龍類之間](../Page/魚龍類.md "wikilink")。牠們最近的近親是[阿氏開普吐龍](../Page/阿氏開普吐龍.md "wikilink")，牠們也是[三疊紀的海生雙孔類動物](../Page/三疊紀.md "wikilink")。

目前本科已有數個屬：體型較大、以[貝類為食的](../Page/貝類.md "wikilink")[海龍](../Page/海龍.md "wikilink")，以及體型較小、以[魚類為食的](../Page/魚類.md "wikilink")*Nectosaurus*。在1993年，Nicholls與Brinkman命名了*Paralonectes*與*Agkistrognathus*，化石都發現於[加拿大](../Page/加拿大.md "wikilink")[卑詩省](../Page/卑詩省.md "wikilink")，使海龍科的生存範圍更為擴大。

## 參考資料

\[<http://links.jstor.org/sici?sici=0022-3360(199303)67%3A2%3C263%3ANT(DFT%3E2.0.CO%3B2-6>.
Abstract of the paper describing *Paralonectes* and *Agkistrognathus*\]

[\*](../Category/海龍目.md "wikilink")