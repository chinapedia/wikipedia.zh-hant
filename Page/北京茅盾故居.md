**北京茅盾故居**位于[北京市](../Page/北京市.md "wikilink")[东城区](../Page/东城区_\(北京市\).md "wikilink")[后圆恩寺胡同](../Page/后圆恩寺胡同.md "wikilink")13号，是作家[茅盾于](../Page/茅盾.md "wikilink")1974年至1981年居住的地方。茅盾于1981年逝世后，这里被辟为茅盾故居，向公众开放。

## 简介

[Mdgj.JPG](https://zh.wikipedia.org/wiki/File:Mdgj.JPG "fig:Mdgj.JPG")
1974年12月，茅盾全家迁至该院。在这座院子，茅盾写作了最后一部长篇回忆录《我走过的路》，此书未完成便在1981年3月27日逝世。1984年5月24日，被公布为第三批[北京市文物保护单位](../Page/北京市文物保护单位.md "wikilink")。经[中共中央批准](../Page/中共中央.md "wikilink")，1985年此院被辟为“茅盾故居”对外开放。\[1\]

茅盾故居是一座两进的四合院，坐北朝南，占地面积大约800平方米，是[中华民国时期建的合瓦过垄脊平房](../Page/中华民国.md "wikilink")。\[2\]

  - 大门：位于该院东南位，为[如意门](../Page/如意门.md "wikilink")，面阔一间，[合瓦](../Page/仰合瓦.md "wikilink")[清水脊屋面](../Page/清水脊.md "wikilink")。
  - [倒座房](../Page/倒座房.md "wikilink")：位于大门两侧，东一间，西三间，合瓦清水脊屋面。
  - 影壁：大门内的影壁位于前院东厢房耳房的南山墙上，影壁上镶有[邓颖超题写的](../Page/邓颖超.md "wikilink")
    “茅盾故居”金字黑大理石横匾。
  - 前院：北房三间，东西两侧各有一间耳房；东西厢房各三间，南侧各有一间耳房。北房原来是茅盾的工作室兼卧室，西厢房原来是茅盾的会客室及藏书房，东厢房是饭厅，其他是家属及服务人员住房。北房前面有一尊[汉白玉的茅盾半身像](../Page/汉白玉.md "wikilink")。
  - 后院：从北房东侧的一条很狭窄的通道可抵达后院。后院有五间北房带东耳房一间，一间西厢房，东侧还有两间南房。院内的北房左右各有一棵[太平花](../Page/太平花.md "wikilink")。\[3\]

茅盾故居中的前院北房和东厢房开设陈列室，展出茅盾的生平和著作。前院西厢房和后院北房为起居室和卧室、书房等等，现恢复当时的陈设。

## 图片

<File:Daozuofang> in Mao Dun Guju, Beijing.jpg|倒座房 <File:Mdgj>
yingbi.JPG|内影壁 <File:Mdgj> yuanzi.JPG|庭院
[File:Maoduntouxiang.JPG|茅盾像，1981年](File:Maoduntouxiang.JPG%7C茅盾像，1981年)[曹春生作](../Page/曹春生.md "wikilink")

## 参考文献

{{-}}

[category:北京名人故居](../Page/category:北京名人故居.md "wikilink")

[Category:茅盾](../Category/茅盾.md "wikilink")
[Category:北京四合院](../Category/北京四合院.md "wikilink")
[Category:北京市博物馆](../Category/北京市博物馆.md "wikilink")

1.  [茅盾故居，东华流韵，于2014-01-03查阅](http://www.bjdclib.com/subdb/laneculture/historybuilding/200906/t20090615_2134.html)

2.
3.