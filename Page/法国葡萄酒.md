[20120812_Solutré_from_SW.jpg](https://zh.wikipedia.org/wiki/File:20120812_Solutré_from_SW.jpg "fig:20120812_Solutré_from_SW.jpg")的葡萄園\]\]
**法国葡萄酒**是指法国出产的葡萄酒。法国是世界著名的葡萄酒产地，其生产葡萄酒的历史悠久。

## 历史

[葡萄在大约公元前](../Page/葡萄.md "wikilink")1000年到前500年之间在法国南部出现，而后它开始在地中海盆地的大部分地区进行繁衍传播。在葡萄酒出现的早期，这种饮料一直被视认是一种只有贵族才能享用的高尚饮品，也是一种用来敬拜酒神[巴古斯](../Page/巴古斯.md "wikilink")（Bacchus）的祭神用品。

葡萄酒在全世界的[基督教徒的心中还代表了耶稣基督的血液](../Page/基督教.md "wikilink")，这点促进了葡萄酒的平民化，使得平常民众也拥有了饮用甘美的葡萄酒的权利。（參見[聖餐禮](../Page/聖餐禮.md "wikilink")）

## 种类

[French_taste_of_wines.JPG](https://zh.wikipedia.org/wiki/File:French_taste_of_wines.JPG "fig:French_taste_of_wines.JPG")\]\]

  - [夏朗德省的](../Page/夏朗德省.md "wikilink")[干邑](../Page/干邑.md "wikilink")，
  - 法国北部[加莱海峡地区的卡维酒](../Page/加莱海峡.md "wikilink")。
  - [波尔多地区](../Page/波尔多.md "wikilink")：[梅多克](../Page/梅多克.md "wikilink")（Médoc
    AOC），[宝媚好罗](../Page/宝媚好罗.md "wikilink")（Pomerol
    AOC），[圣艾米里昂](../Page/圣艾米里昂.md "wikilink")（Saint-Émilion
    AOC），[格拉夫](../Page/格拉夫.md "wikilink")（Graves）和[索特尔尼](../Page/索特尔尼.md "wikilink")，
  - [勃艮第酒](../Page/勃艮第.md "wikilink")，
  - [薄酒莱酒](../Page/薄酒莱.md "wikilink")，
  - [香槟大区出产的](../Page/香槟大.md "wikilink")[香槟](../Page/香槟.md "wikilink")。

## 命名系統

1935年法國通過了大量關於葡萄酒質量控制的法律。這些法律建立了一個原產地控制命名系統，並由一專門的監督委員會（原產地命名國家學會）來管理。此後，法國便擁有了一個世界上最早的葡萄酒命名系統，以及最嚴格的關於葡萄酒製作和生產的法律。歐洲其他許多國家的類似系統都是模仿自法國的。法國法律將葡萄酒分成四個級別，其中包括：

級別簡稱AOC，是法國葡萄酒最高級別。AOC在法文意思為「原產地管制命名」。原產地地區的葡萄品種、種植數量、釀造過程、酒精含量等都要得到專家認證。只能用原產地種植的葡萄釀製，絕對不可和別地葡萄汁勾兌。AOC產量大約佔法國葡萄酒總產量的53.4%。酒瓶標籤標示為Appellation+產區名+Contrôlée，例如Appellation
Bordeaux Contrôlée。2009年8月份發生了一次改革。AOC葡萄酒變成AOP葡萄酒。

### 法定產區

級別簡稱VDQS，是普通地區餐酒向AOC級別過渡所必須經歷的級別。如果在VDQS時期酒質表現良好，則會升級為AOC。產量只佔法國葡萄酒總產量的0.9%。酒瓶標籤標示為Appellation+產區名+Vin
Délimité de Qualité Superieure。

### 地區餐酒

英文意思為Wine of
Country，日常餐酒中最好的酒被升級為地區餐酒。地區餐酒的標籤上可以標明產區。可以用標明產區內的葡萄汁勾兌，但僅限於該產區內的葡萄。產量約佔法國葡萄酒總產量的33.9%。酒瓶標籤標示為Vin
de Pays +產區名，例如Vin de Pays
d'Oc。法國絕大部分的地區餐酒產自南部地中海沿岸。為了配合歐洲農產品級別標註形式，法國葡萄酒的級別於2009年8月份發生了一次改革VDP葡萄酒變成IGP葡萄酒。然而此週一項官方調查卻發現，儘管IGP葡萄酒佔所有法國葡萄酒消費的30%，但74%的消費者表示並不熟悉“IGP”這個評級術語。在法國，能被評為IGP等級的有3.5萬塊葡萄園，產量為每年12億升，近16億瓶葡萄酒，佔法國葡萄酒總產量的1/4。

IGP葡萄酒對產區、土壤類型、氣候、最大產量和葡萄品種等都有比較嚴格的規定，只有遵循嚴格的釀酒標準並得到法國政府和歐盟委員會的批准才能被評為“IGP”葡萄酒。

目前，大部分法國葡萄酒酒標上還是傾向於使用VDP，而不是IGP。

### 日常餐酒

英文意思為Wine of the
table，是最低檔的葡萄酒，作日常飲用。可以由不同地區的葡萄汁勾兌而成，如果葡萄汁限於法國各產區，可稱法國日常餐酒。不得用歐共體外國家的葡萄汁，產量約佔法國葡萄酒總產量的11.7%。酒瓶標籤標示為Vin
de Table，例如Vin de Table Français。

最新修正（2009年10月6日） 法国议会09年10月6日决议，随着法国葡萄酒业共同组织（OCM）的改革以及不标注葡萄品种和年份的Vin de
France这一新类别的创立，L'ANIVIT（法国国家日常餐酒和地方餐酒行业协会）更名为L'ANIVIN
DEFRANCE（法国国家葡萄酒行业协会），Vin De
Table（日常餐酒）以及L'ANIVIT中的「T」都将消失。法国日常餐酒VDT更名为VDF（Vin de
France）。09年8月1号以后，欧盟所有VDP更名为IGP。

## 法国葡萄酒产区

[French_vineyards法國酒區地圖.svg](https://zh.wikipedia.org/wiki/File:French_vineyards法國酒區地圖.svg "fig:French_vineyards法國酒區地圖.svg")
产区（Terroir）原是法语中有关葡萄酒和咖啡鉴赏的术语。它被用于表示由于不同地域而赋予物产的独特性。它也可以理解为"某地的感觉"，这种感觉具体表达了一定的品质，以及当地环境对所出产产品品质的综合影响。一般来说，产区是指同一地域葡萄园的统称，它们属于同一特定的命名，具有同样类型的土壤、气候条件、葡萄和酿酒工艺。有些还涉及到历史、传统、葡萄园所有人和其他因素。

法国葡萄酒的大产区有：

  - [阿尔萨斯产区](../Page/阿尔萨斯.md "wikilink")（Alsace）
  - [博若莱产区](../Page/博若莱.md "wikilink")（Beaujolais）
  - [波尔多产区](../Page/波尔多.md "wikilink")（Bordeaux）
  - [勃艮第产区](../Page/勃艮第.md "wikilink")（Bourgogne）
  - [香槟产区](../Page/香槟.md "wikilink")（Champagne）
  - [朗格多克产区](../Page/朗格多克.md "wikilink")（Languedoc）
  - [普罗旺斯产区](../Page/普罗旺斯.md "wikilink")（Provence）
  - [鲁西雍产区](../Page/鲁西雍.md "wikilink")（Roussillon）
  - [萨瓦产区](../Page/萨瓦.md "wikilink")（Savoie）
  - 西南产区（Sud-Ouest）
  - [卢瓦尔河谷产区](../Page/卢瓦尔河谷_\(葡萄酒产区\).md "wikilink")（Vallée de la Loire）
  - [隆河谷地](../Page/隆河.md "wikilink")（Vallée du Rhône）

其中最知名的法国葡萄酒产区主要有[波尔多](../Page/波尔多.md "wikilink")、[勃艮第和](../Page/勃艮第.md "wikilink")[香槟区](../Page/香槟.md "wikilink")。波尔多以产浓郁型的红酒而著称，而勃艮第则以产清淡型红酒和清爽典雅型白酒著称，香槟区酿制世界闻名、优雅浪漫的汽酒。

[\*](../Category/法國葡萄酒.md "wikilink")