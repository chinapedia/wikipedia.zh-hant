此列表延伸自[欧洲友好城市列表](../Page/欧洲友好城市列表.md "wikilink")。

|                                               |                                                                                                                            |
| --------------------------------------------: | :------------------------------------------------------------------------------------------------------------------------- |
|             〖[科靈](../Page/科靈.md "wikilink")〗- | <small>[日本](../Page/日本.md "wikilink")[愛知縣](../Page/愛知縣.md "wikilink")[安城市](../Page/安城市_\(日本\).md "wikilink")（1997年）        |
|                                               | <small>[德國](../Page/德國.md "wikilink")[代爾門霍斯特](../Page/代爾門霍斯特.md "wikilink")（1979年）                                         |
|                                               | <small>[挪威](../Page/挪威.md "wikilink")[德拉門](../Page/德拉門.md "wikilink")（1946年）                                               |
|                                               | <small>[西班牙](../Page/西班牙.md "wikilink")[威斯喀](../Page/威斯喀.md "wikilink")（1982年）                                             |
|                                               | <small>[芬蘭](../Page/芬蘭.md "wikilink")[拉彭蘭塔](../Page/拉彭蘭塔.md "wikilink")（1947年）                                             |
|                                               | <small>[立陶宛](../Page/立陶宛.md "wikilink")[帕內韋日斯](../Page/帕內韋日斯.md "wikilink")（2000年）                                         |
|                                               | <small>[冰島](../Page/冰島.md "wikilink")[Stykkisholmur](../Page/Stykkisholmur.md "wikilink")（1979年）                           |
|                                               | <small>[匈牙利](../Page/匈牙利.md "wikilink")[松博特海伊](../Page/松博特海伊.md "wikilink")（1991年）                                         |
|                                               | <small>[瑞典](../Page/瑞典.md "wikilink")[厄勒布魯](../Page/厄勒布魯.md "wikilink")（1946年）                                             |
|           〖[奧爾堡](../Page/奧爾堡.md "wikilink")〗- | <small>[英國](../Page/英國.md "wikilink")[愛丁堡](../Page/愛丁堡.md "wikilink")（1964年）                                               |
|                                               | <small>[挪威](../Page/挪威.md "wikilink")[腓特烈克斯塔](../Page/腓特烈克斯塔.md "wikilink")（1951年）                                         |
|                                               | <small>[法羅群島](../Page/法羅群島.md "wikilink")[Fuglefjord](../Page/Fuglefjord.md "wikilink")（1986年）                             |
|                                               | <small>[愛爾蘭](../Page/愛爾蘭共和國.md "wikilink")[戈爾韋](../Page/戈爾韋.md "wikilink")（1997年）                                          |
|                                               | <small>[以色列](../Page/以色列.md "wikilink")[海法](../Page/海法.md "wikilink")（1972年）                                               |
|                                               | <small>[冰島](../Page/冰島.md "wikilink")[胡薩維克](../Page/胡薩維克.md "wikilink")（1996年）                                             |
|                                               | <small>[英國](../Page/英國.md "wikilink")[蘭開斯特](../Page/蘭開斯特.md "wikilink")（1977年）                                             |
|                                               | <small>[英國](../Page/英國.md "wikilink")[格陵蘭](../Page/格陵蘭.md "wikilink")（2002年）                                               |
|                                               | <small>[瑞士](../Page/瑞士.md "wikilink")[Rapperswil](../Page/Rapperswil.md "wikilink")（1968年）                                 |
|                                               | <small>[德國](../Page/德國.md "wikilink")[倫茨堡](../Page/倫茨堡.md "wikilink")（1967年）                                               |
|                                               | <small>[拉脫維亞](../Page/拉脫維亞.md "wikilink")[-{里}-加](../Page/里加.md "wikilink")（1989年）                                         |
|                                               | <small>[美國](../Page/美國.md "wikilink")[加利福尼亞州](../Page/加利福尼亞州.md "wikilink")[Solvang](../Page/Solvang.md "wikilink")（1971年） |
|                                               | <small>[保加利亞](../Page/保加利亞.md "wikilink")[瓦爾納](../Page/瓦爾納.md "wikilink")（1976年）                                           |
|                                               | <small>[立陶宛](../Page/立陶宛.md "wikilink")[維爾紐斯](../Page/維爾紐斯.md "wikilink")（1997年）                                           |
|                                               | <small>[德國](../Page/德國.md "wikilink")[維斯馬](../Page/維斯馬.md "wikilink")（1961年）                                               |
|           〖[希勒勒](../Page/希勒勒.md "wikilink")〗- | <small>[冰島](../Page/冰島.md "wikilink")[olafsfjordur](../Page/olafsfjordur.md "wikilink")                                    |
|           〖[歐登塞](../Page/歐登塞.md "wikilink")〗- | <small>[捷克](../Page/捷克.md "wikilink")[布爾諾](../Page/布爾諾.md "wikilink")                                                      |
|                                               | <small>[美國](../Page/美國.md "wikilink")[俄亥俄州](../Page/俄亥俄州.md "wikilink")[哥倫布](../Page/哥倫布.md "wikilink")                    |
|                                               | <small>[日本](../Page/日本.md "wikilink")[千葉縣](../Page/千葉縣.md "wikilink")[船橋市](../Page/船橋市.md "wikilink")                      |
|                                               | <small>[荷蘭](../Page/荷蘭.md "wikilink")[格羅寧根](../Page/格羅寧根.md "wikilink")                                                    |
|                                               | <small>[南韓](../Page/南韓.md "wikilink")[益山市](../Page/益山市.md "wikilink")                                                      |
|                                               | <small>[土耳其](../Page/土耳其.md "wikilink")[伊士麥](../Page/伊士麥.md "wikilink")                                                    |
|                                               | <small>[波蘭](../Page/波蘭.md "wikilink")[卡托維治](../Page/卡托維治.md "wikilink")                                                    |
|                                               | <small>[立陶宛](../Page/立陶宛.md "wikilink")[考納斯](../Page/考納斯.md "wikilink")（1992年）                                             |
|                                               | <small>[烏克蘭](../Page/烏克蘭.md "wikilink")[基輔](../Page/基輔.md "wikilink")                                                      |
|                                               | <small>[法羅群島](../Page/法羅群島.md "wikilink")[克拉克斯維克](../Page/克拉克斯維克.md "wikilink")                                            |
|                                               | <small>[冰島](../Page/冰島.md "wikilink")[科帕沃-{于}-爾](../Page/科帕沃于爾.md "wikilink")                                              |
|                                               | <small>[瑞典](../Page/瑞典.md "wikilink")[諾爾雪平](../Page/諾爾雪平.md "wikilink")                                                    |
|                                               | <small>[以色列](../Page/以色列.md "wikilink")[貝達蒂克法](../Page/貝達蒂克法.md "wikilink")                                                |
|                                               | <small>[德國](../Page/德國.md "wikilink")[什未林](../Page/什未林.md "wikilink")                                                      |
|                                               | <small>[中國](../Page/中國.md "wikilink")[紹興](../Page/紹興.md "wikilink")                                                        |
|                                               | <small>[英國](../Page/英國.md "wikilink")[聖阿本斯](../Page/聖阿本斯.md "wikilink")                                                    |
|                                               | <small>[芬蘭](../Page/芬蘭.md "wikilink")[坦佩雷](../Page/坦佩雷.md "wikilink")                                                      |
|                                               | <small>[挪威](../Page/挪威.md "wikilink")[特隆赫姆](../Page/特隆赫姆.md "wikilink")                                                    |
|                                               | <small>[格陵蘭](../Page/格陵蘭.md "wikilink")[烏佩納維克](../Page/烏佩納維克.md "wikilink")（1980年）                                         |
|                                               | <small>[瑞典](../Page/瑞典.md "wikilink")[厄斯特松德](../Page/厄斯特松德.md "wikilink")                                                  |
|           〖[靈克賓](../Page/靈克賓.md "wikilink")〗- | <small>[挪威](../Page/挪威.md "wikilink")（1950年）                                                                               |
|         〖[奧爾胡斯](../Page/奧爾胡斯.md "wikilink")〗- | <small>[德國](../Page/德國.md "wikilink")[羅斯托克](../Page/羅斯托克.md "wikilink")（1964年）                                             |
|                                               | <small>[英國](../Page/英國.md "wikilink")[斯旺西](../Page/斯旺西.md "wikilink")                                                      |
|         〖[埃斯比約](../Page/埃斯比約.md "wikilink")〗- | <small>[芬蘭](../Page/芬蘭.md "wikilink")[-{于}-韋斯屈萊](../Page/于韋斯屈萊.md "wikilink")（1947年）                                       |
|         〖[赫爾辛格](../Page/赫爾辛格.md "wikilink")〗- | <small>[波蘭](../Page/波蘭.md "wikilink")[格但斯克](../Page/格但斯克.md "wikilink")                                                    |
| 〖[Ballerup](../Page/Ballerup.md "wikilink")〗- | <small>[英國](../Page/英國.md "wikilink")[東基爾布賴德](../Page/東基爾布賴德.md "wikilink")                                                |

[Category:姐妹城市列表](../Category/姐妹城市列表.md "wikilink")
[\*](../Category/丹麦城市.md "wikilink")
[Category:丹麥相關列表](../Category/丹麥相關列表.md "wikilink")