**何信祿龍**（[学名](../Page/学名.md "wikilink")：*Hexinlusaurus*）是種原始[鸟臀目恐龍](../Page/鸟臀目.md "wikilink")，是一种小型的二足[草食性恐龍](../Page/草食性.md "wikilink")，生活在[侏罗纪中期](../Page/侏罗纪.md "wikilink")，化石發現於[中国](../Page/中国.md "wikilink")[四川省](../Page/四川省.md "wikilink")[自贡市](../Page/自贡市.md "wikilink")[大山铺](../Page/大山铺.md "wikilink")，由[成都理工大學的](../Page/成都理工大學.md "wikilink")[何信禄和](../Page/何信禄.md "wikilink")[蔡开基在](../Page/蔡开基.md "wikilink")1983年敘述及命名。

何信祿龍的[正模標本](../Page/正模標本.md "wikilink")（編號ZDM
T6001）包含一個幾乎完整的頭顱骨，以及部份頭顱後骨骸，發現於大山铺的[下沙溪廟組陸相沙岩層](../Page/下沙溪廟組.md "wikilink")，年代相當於[侏羅紀的](../Page/侏羅紀.md "wikilink")[巴柔階](../Page/巴柔階.md "wikilink")。[副模標本](../Page/副模標本.md "wikilink")（編號ZDM
T6002）是一個部份頭顱骨與顱後的骨骼。

多齒何信祿龍原本為多齒鹽都龍（*Yandusaurus
multidens*），[保羅·巴雷特](../Page/保羅·巴雷特.md "wikilink")（Paul
Barrett）等人在2005年將牠們成立獨立的屬，並認為牠們與其他基礎鳥腳類恐龍的差別在於單一[獨有衍徵](../Page/獨有衍徵.md "wikilink")（Autapomorphy）：眶後的側面有一個明顯的凹面。屬名是以最初的命名者何信祿為名。

其他生存於大山铺地區的恐龍包括：[蜥腳下目的](../Page/蜥腳下目.md "wikilink")[蜀龍](../Page/蜀龍.md "wikilink")、[獸腳亞目的](../Page/獸腳亞目.md "wikilink")[氣龍](../Page/氣龍.md "wikilink")、以及[劍龍下目的](../Page/劍龍下目.md "wikilink")[華陽龍](../Page/華陽龍.md "wikilink")。

## 原鹽都龍

這個物種被正式命名為何信祿龍之前，這種恐龍被非正式名稱為原鹽都龍（"Proyandusaurus"）。這個名稱最初出現在Fabien
Knoll的研究概論之中\[1\]\[2\]。

## 參考資料

  - Barrett, P.M., Butler, R. J., et Knoll, F. 2005. Small-bodied
    ornithischian dinosaurs from the Middle Jurassic of Sichuan, China.
    *Journal of Vertebrate Paleontology* 25:823-834.

  - Knoll, F. 1999. The family Fabrosauridae. In: J. I. Canudo & G.
    Cuenca-Bescós (Eds.): IV European Workshop on Vertebrate
    Palaeontology, Albarracin (Teruel, Spain), junio de 1999. Programme
    and Abstracts, Field guide. Servicio Publicaciones Universidad de
    Zaragoza, 54.

  - He, X.-L. et Cai, K.-J. 1983. A new species of *Yandusaurus*
    (hypsilophodont dinosaur) from the Middle Jurassic of Dashanpu,
    Zigong, Sichuan. *Journal of Chengdu College of Geology, Supplement
    1*:5-14.

  -
## 外部連結

  - <http://www.thescelosaurus.com/ornithischia>

[Category:鳥臀目](../Category/鳥臀目.md "wikilink")
[Category:亚洲恐龙](../Category/亚洲恐龙.md "wikilink")
[Category:侏羅紀恐龍](../Category/侏羅紀恐龍.md "wikilink")

1.  <http://dml.cmnh.org/2003Sep/msg00169.html>
2.