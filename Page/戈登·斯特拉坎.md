**戈登·斯特拉坎**（，），生於[蘇格蘭](../Page/蘇格蘭.md "wikilink")[爱丁堡](../Page/爱丁堡.md "wikilink")，是前[職業](../Page/職業.md "wikilink")[足球](../Page/足球.md "wikilink")[運動員](../Page/運動員.md "wikilink")，現時任職[領隊](../Page/領隊_\(足球\).md "wikilink")，曾分別任教[高雲地利](../Page/考文垂足球俱乐部.md "wikilink")、[修咸頓](../Page/南安普顿足球俱乐部.md "wikilink")、[些路迪](../Page/凯尔特人足球俱乐部.md "wikilink")、[米杜士堡及](../Page/米德尔斯堡足球俱乐部.md "wikilink")[蘇格蘭國家隊](../Page/蘇格蘭足球代表隊.md "wikilink")。斯特拉坎光輝的球員生涯分別效力[登地](../Page/邓迪足球俱乐部.md "wikilink")、[鴨巴甸](../Page/阿伯丁足球俱乐部.md "wikilink")、[曼聯](../Page/曼徹斯特聯足球俱樂部.md "wikilink")、[列斯聯及](../Page/列斯聯足球會.md "wikilink")[高雲地利](../Page/考文垂足球俱乐部.md "wikilink")。同時為[蘇格蘭出席](../Page/蘇格蘭足球代表隊.md "wikilink")50場國際賽及入選[蘇格蘭足球名人堂](../Page/蘇格蘭足球名人堂.md "wikilink")。史特根身材矮小，但技術純熟及踢法積極，司職右中場。

## 球員生涯

史特根在[蘇超球隊](../Page/蘇格蘭足球聯賽超級聯賽.md "wikilink")[登地開展其足球事業](../Page/邓迪足球俱乐部.md "wikilink")，但在改投[鴨巴甸後才出現突破](../Page/阿伯丁足球俱乐部.md "wikilink")，1980年代的鴨巴甸在[費格遜爵士領導下打破](../Page/費格遜.md "wikilink")“[老字號](../Page/老字號_\(足球\).md "wikilink")”的長年壟斷，分別奪得兩次聯賽冠軍、三次[蘇格蘭杯冠軍](../Page/蘇格蘭杯.md "wikilink")、[欧洲优胜者杯及](../Page/欧洲优胜者杯.md "wikilink")[歐洲超級盃各一次](../Page/欧洲超级杯足球赛.md "wikilink")。

1984年[曼聯對這位具優良技術的球星大感興趣](../Page/曼聯.md "wikilink")，當時的領隊[朗·艾堅遜在](../Page/朗·艾堅遜.md "wikilink")8月出價50萬[英鎊將史特根帶到](../Page/英鎊.md "wikilink")[奧脫福球場](../Page/奧脫福球場.md "wikilink")。史特根在[曼聯的初期十分成功](../Page/曼聯.md "wikilink")，於1985年引領球隊在決賽加時1-0擊敗[愛華頓奪得](../Page/埃弗顿足球俱乐部.md "wikilink")[足總盃冠軍](../Page/英格蘭足總盃.md "wikilink")。但史特根的狀態逐漸下滑，最終更失去正選球員的資格。

幸而[列斯聯的領隊](../Page/利兹联足球俱乐部.md "wikilink")[侯活·韋堅遜在](../Page/侯活·韋堅遜.md "wikilink")1989年伸出援手，付出20萬[英鎊羅致史特根](../Page/英鎊.md "wikilink")。狀態回升的史特根協助列斯聯奪得1990年[乙組聯賽冠軍](../Page/英格兰足球联赛.md "wikilink")，闊別八年後重回甲組行列。翌季列斯聯獲得第四位，史特根獲選[英格蘭足球先生](../Page/英格蘭足球先生.md "wikilink")。1992年[超聯成立前最後一屆甲組聯賽](../Page/英格兰足球超级联赛.md "wikilink")，列斯聯戲劇性最後趕過[曼聯奪得冠軍](../Page/曼聯.md "wikilink")。史特根繼續留效列斯聯，在超聯的首三個球季列斯聯成績不過不失，除間中受背傷困擾，史特根仍然正選上場。

1995年3月史特根加盟[高雲地利](../Page/考文垂足球俱乐部.md "wikilink")，追隨舊上司[朗·艾堅遜出任球員兼教練](../Page/朗·艾堅遜.md "wikilink")。在，史特根並非必然正選，但協助訓練球員成功在球末以得失球差壓倒[南安普顿及](../Page/南安普顿足球俱乐部.md "wikilink")[曼城逃出降級厄運](../Page/曼城足球俱乐部.md "wikilink")，保留[英超席位](../Page/英超.md "wikilink")。

## 領隊生涯

當[朗·艾堅遜於](../Page/朗·艾堅遜.md "wikilink")1996年11月出任[高雲地利的足球總監時](../Page/考文垂足球俱乐部.md "wikilink")，史特根獲晉升為領隊。而史特根亦於季後完全退出球員生涯，最後一個球季史特根已達40歲的高齡，是[英超歷史上最年長的球員](../Page/英超.md "wikilink")。同季[高雲地利再次死裡逃生](../Page/考文垂足球俱乐部.md "wikilink")，在尾二輪聯賽主場1-2負於[德比郡](../Page/德比郡足球俱乐部.md "wikilink")，從第15位跌到第18位降級的邊沿，幸好最反一輪作客2-1擊敗[熱刺](../Page/托特纳姆足球俱乐部.md "wikilink")，而護級對手[米杜士堡](../Page/米德尔斯堡足球俱乐部.md "wikilink")（季中因球員傷病缺席一場比賽被扣3分）及[新特蘭齊齊輸掉比賽](../Page/桑德兰足球俱乐部.md "wikilink")，與[諾定咸森林一同降級](../Page/诺丁汉森林足球俱乐部.md "wikilink")，[高雲地利幸保](../Page/考文垂足球俱乐部.md "wikilink")30年頂級聯賽參賽的金漆招牌。史特根只能維持[高雲地利多留在](../Page/考文垂足球俱乐部.md "wikilink")[英超四季](../Page/英超.md "wikilink")，2001年終於降級，而史特根在甲組的新球季剛展開便被撤職。

只數星期後史特根便回到領隊岡位，接手任教剛革除[-{zh-hans:斯图亚特·格雷;
zh-hant:史超活·格雷;}-](../Page/斯图尔特·格雷_\(1960年出生足球运动员\).md "wikilink")（Stuart
Gray）在[英超掙扎的](../Page/英超.md "wikilink")[南安普顿](../Page/南安普顿足球俱乐部.md "wikilink")，球隊首季在[聖瑪麗球場](../Page/聖瑪麗球場.md "wikilink")（St
Mary's
Stadium）作賽，開季的九月份6戰只得6分，主場未開勝利之門，僅排第16位，大部分球評家已估計修咸頓將在季尾降級。自史特根在10月上任後即逐漸扭轉形勢，最終排中游的第11位，保留[英超席位](../Page/英超.md "wikilink")。翌季成績大幅提升，[英超排第](../Page/英超.md "wikilink")8位，更晉身[足總盃決賽](../Page/英格蘭足總盃.md "wikilink")，0-1負於[阿仙奴只能屈居亞軍](../Page/阿仙奴.md "wikilink")。由於[阿仙奴己獲得](../Page/阿仙奴.md "wikilink")[歐聯參賽權](../Page/歐洲聯賽冠軍盃.md "wikilink")，[南安普顿可以參賽來年的](../Page/南安普顿足球俱乐部.md "wikilink")[欧洲联盟杯](../Page/歐洲足協盃.md "wikilink")。

## 中途小休

在較早前宣佈約滿後不續的史特根提前於2004年3月辭去[南安普顿領隊職務](../Page/南安普顿足球俱乐部.md "wikilink")，爭取更多時間陪伴家人。在同年尾[-{zh-hans:贝尔蒂·福格茨;
zh-hant:福士;}-退任](../Page/贝尔蒂·福格茨.md "wikilink")[蘇格蘭領隊](../Page/蘇格蘭足球代表隊.md "wikilink")，史特根被廣泛佈道將會接任，但最終由[-{zh-hans:沃爾特·史密斯;
zh-hant:華達史密夫;}-出任領隊](../Page/沃爾特·史密斯.md "wikilink")。史特根亦曾傳出與修咸頓的對頭[樸茨茅夫有連繫](../Page/朴茨茅斯足球俱乐部.md "wikilink")，但史特根拒絕受聘。

無官一身輕的史特根閒來為傳媒評論足球比賽，最著名是在[BBC與](../Page/英国广播公司.md "wikilink")[艾德里安·奇尔斯](../Page/艾德里安·奇尔斯.md "wikilink")（Adrian
Chiles）拍檔主持[焦點球賽2](../Page/焦點球賽2.md "wikilink")（Match of the Day
2）節目。史特根的[-{冷面笑匠}-形象及對球賽的精闢見解使他大受觀眾及球迷歡迎](../Page/冷面笑匠.md "wikilink")。

## 回歸蘇格蘭

史特根在2005年6月1日終於重回領隊行列，接替[馬田·奧尼爾出任](../Page/馬田·奧尼爾.md "wikilink")[蘇超老牌班霸](../Page/蘇格蘭足球超級聯賽.md "wikilink")[些路迪的領隊職務](../Page/凯尔特人足球俱乐部.md "wikilink")，首要任務是重奪剛剛戲劇性失落給[格拉斯哥流浪的聯賽錦標](../Page/流浪者足球俱乐部.md "wikilink")。但落筆打三更，執教的首場正式比賽於2005年7月27日[歐聯資格賽第二圈首回合作客](../Page/欧洲足球联盟冠军联赛.md "wikilink")[斯洛伐克的](../Page/斯洛伐克.md "wikilink")[-{zh-hans:佩特薩爾卡;
zh-hant:帕沙卡;}-](../Page/佩特薩爾卡足球俱樂部.md "wikilink")，0-5大敗而回，三天後驚魂未定的[些路迪](../Page/些路迪.md "wikilink")-在[蘇超首場只能](../Page/蘇超.md "wikilink")4-4打和[馬瑟韋爾](../Page/马瑟韦尔足球俱乐部.md "wikilink")，兩場比賽共失9球是球隊近年來最差的紀錄。雖然其後[歐聯次回合在主場回敬](../Page/歐聯.md "wikilink")-{zh-hans:佩特薩爾卡;
zh-hant:帕沙卡;}-4球，但累計4-5黯然出局。

[些路迪不穩的防守仍然為傳媒及球迷所詬病](../Page/些路迪.md "wikilink")，但在史特根領導下成績不斷提升，聯賽曾一度領先多達20分。稍微不足是2006年1月8日[蘇格蘭杯第三](../Page/蘇格蘭杯.md "wikilink")-{zh-hans:轮;
zh-hant:圈;}-被甲組黑馬[克萊德](../Page/克莱德足球俱乐部.md "wikilink")（Clyde）淘汰，但在2月些路迪以8-1大破[鄧弗姆林](../Page/邓弗姆林足球俱乐部.md "wikilink")，創下[蘇超最大勝球紀錄](../Page/蘇超.md "wikilink")。

史特根在些路迪的首季可稱為成功，在上季主將星散後重組班底，於2006年4月5日3-0擊敗[鄧弗姆林奪得](../Page/邓弗姆林足球俱乐部.md "wikilink")[蘇格蘭聯賽杯冠軍](../Page/蘇格蘭聯賽杯.md "wikilink")，亦是史特根領隊生涯的首項冠軍，聯賽更在尚餘六輪比賽便提早封王。4月13日史特根獲選為[蘇格蘭年度最佳領隊](../Page/蘇格蘭.md "wikilink")。

2009年5月25日，他因戰績嚴重欠佳而被辭去些路迪教練職務。

## 重返英格蘭

2009年10月26日史特根獲聘執教剛降班[英冠的](../Page/英冠.md "wikilink")[米杜士堡](../Page/米德尔斯堡足球俱乐部.md "wikilink")<small>\[1\]</small>，從[蘇超引入數名球員成為球隊骨幹](../Page/蘇超.md "wikilink")，但球隊成績不升反跌，於新年前從接手時的第4位降至中游位置，最終以第11位結束球季。2010年夏季史特根再度增兵，引入包括[-{zh-hans:克里斯·博伊德;
zh-hk:基斯·保特;}-](../Page/克里斯·博伊德.md "wikilink")、[-{zh-hans:凯文·汤姆森;
zh-hk:奇雲·湯臣;}-](../Page/凯文·汤姆森.md "wikilink")（Kevin
Thomson）、[-{zh-hans:斯蒂芬·麦克马努斯;
zh-hk:麥曼奴斯;}-](../Page/斯蒂芬·麦克马努斯.md "wikilink")（Stephen
McManus）等球員，開季成績仍未如理想，到10月中球隊位列第20位，是米杜士堡近20年來在[聯賽最低排名](../Page/英格蘭足球聯賽.md "wikilink")。10月18日史特根與球隊主席（Steve
Gibson）會面後宣佈辭職，更當面撕毀合約不要求離隊賠償，總結帶隊出戰46場，僅取得13場勝仗\[2\]。

## 執教國家隊

2013年1月15日獲[蘇格蘭委任為國家隊新教頭](../Page/蘇格蘭足球代表隊.md "wikilink")，簽約至[2016年歐洲足球錦標賽外圍賽結束](../Page/2016年歐洲足球錦標賽.md "wikilink")\[3\]。

## 國際足聯/蘇格蘭SOS大使

史特根被選為國際足聯/蘇格蘭SOS大使（FIFA/SOS Ambassador for
Scotland），作為[國際足聯與](../Page/国际足球联合会.md "wikilink")[SOS兒童村合作以](../Page/SOS兒童村.md "wikilink")[2006年世界盃為號召稱為](../Page/2006年世界盃足球賽.md "wikilink")“2006年6條村”（6
villages for
2006）的慈善項目，為[巴西](../Page/巴西.md "wikilink")、[墨西哥](../Page/墨西哥.md "wikilink")、[尼日利亞](../Page/尼日利亞.md "wikilink")、[南非](../Page/南非.md "wikilink")、[烏克蘭及](../Page/烏克蘭.md "wikilink")[越南的孤兒及被遺棄兒童各建立一條兒童村](../Page/越南.md "wikilink")，計劃自2003年開展直到2006年世界盃完成後才結束，其他參與的包括[朗尼](../Page/韦恩·鲁尼.md "wikilink")（[英格蘭](../Page/英格蘭.md "wikilink")）、[雲尼斯杜萊](../Page/路德·范尼斯特尔鲁伊.md "wikilink")（[荷蘭](../Page/荷蘭.md "wikilink")）、[舒夫真高](../Page/安德烈·舍甫琴科.md "wikilink")（[乌克兰](../Page/乌克兰.md "wikilink")）、[-{zh-hans:亚历山大·赫莱布;
zh-hant:希比;}-](../Page/亞歷山大·希比.md "wikilink")（[白俄羅斯](../Page/白俄羅斯.md "wikilink")）、[-{zh-hans:杰伊·杰伊·奥科查;
zh-hant:奧高查;}-](../Page/杰伊·杰伊·奥科查.md "wikilink")（[尼日利亞](../Page/尼日利亞.md "wikilink")）、[-{zh-hans:卢卡斯·拉德贝;
zh-hant:拉迪比;}-](../Page/盧卡斯·拉迪比.md "wikilink")（[南非](../Page/南非.md "wikilink")）、[-{zh-hans:小儒尼奥;
zh-hant:P祖連奴;}-](../Page/儒尼尼奥·佩南布卡诺.md "wikilink")（[巴西](../Page/巴西.md "wikilink")）、[-{zh-hans:克里斯蒂安·维耶里;
zh-hant:韋利;}-](../Page/克里斯蒂安·维耶里.md "wikilink")（[義大利](../Page/義大利.md "wikilink")）、[-{zh-hans:罗贝尔·皮雷斯;
zh-hant:羅拔·皮里斯;}-](../Page/罗贝尔·皮雷斯.md "wikilink")（[法國](../Page/法國.md "wikilink")）及[-{zh-hans:乔治·维阿;
zh-hant:韋亞;}-](../Page/乔治·维阿.md "wikilink")（[利比里亚](../Page/利比里亚.md "wikilink")）等80位前任或現任球星。2006年SOS兒童村的小朋友更免費獲贈包含從[維基百科中挑選](../Page/維基百科.md "wikilink")8000張圖片及4000頁文字的光碟。

## 參考資料

## 外部連結

  - [The priceless sayings of the inimitable Gordon
    Strachan\!](https://web.archive.org/web/20050527222429/http://thesonsofscotland.co.uk/gingerbites.htm)

[Category:蘇格蘭足球運動員](../Category/蘇格蘭足球運動員.md "wikilink")
[Category:足球中場](../Category/足球中場.md "wikilink")
[Category:登地球員](../Category/登地球員.md "wikilink")
[Category:鴨巴甸球員](../Category/鴨巴甸球員.md "wikilink")
[Category:曼聯球員](../Category/曼聯球員.md "wikilink")
[Category:列斯聯球員](../Category/列斯聯球員.md "wikilink")
[Category:高雲地利球員](../Category/高雲地利球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:英格蘭足球聯賽球員](../Category/英格蘭足球聯賽球員.md "wikilink")
[Category:1982年世界盃足球賽球員](../Category/1982年世界盃足球賽球員.md "wikilink")
[Category:1986年世界盃足球賽球員](../Category/1986年世界盃足球賽球員.md "wikilink")
[Category:苏格兰足球领队](../Category/苏格兰足球领队.md "wikilink")
[Category:高雲地利領隊](../Category/高雲地利領隊.md "wikilink")
[Category:修咸頓領隊](../Category/修咸頓領隊.md "wikilink")
[Category:英超領隊](../Category/英超領隊.md "wikilink")
[Category:些路迪領隊](../Category/些路迪領隊.md "wikilink")
[Category:米杜士堡領隊](../Category/米杜士堡領隊.md "wikilink")
[Category:蘇格蘭足球代表隊領隊](../Category/蘇格蘭足球代表隊領隊.md "wikilink")
[Category:蘇超領隊](../Category/蘇超領隊.md "wikilink")
[Category:英格蘭足球聯賽領隊](../Category/英格蘭足球聯賽領隊.md "wikilink")
[Category:OBE勳銜](../Category/OBE勳銜.md "wikilink")
[Category:蘇格蘭足球名人堂入選人](../Category/蘇格蘭足球名人堂入選人.md "wikilink")

1.
2.
3.