**阿拉伯承包商體育俱樂部**\[1\]（**Al-Mokawloon
al-Arab**，部分媒體譯為**阿拉伯建築**），是[埃及](../Page/埃及.md "wikilink")[纳斯尔市的一家职业體育俱乐部](../Page/纳斯尔市.md "wikilink")，成立於1973年，由埃及人Osman
Ahmed Osman所創立。當前在[埃及足球甲级联赛參賽](../Page/埃及足球甲级联赛.md "wikilink")。

俱乐部拥有人为 El-Mokawloon El-Arab（阿拉伯承建商）。主体育场为阿拉伯承包商体育场（Arab Contractors
Stadium）。

## 球队荣誉

  - **[非洲优胜者杯](../Page/非洲优胜者杯.md "wikilink")**：3次

1982、1983、1996

  - **[非洲超级杯](../Page/非洲超级杯.md "wikilink")**：

亚军：1996

  - **[阿拉伯优胜者杯](../Page/阿拉伯优胜者杯.md "wikilink")**：

亚军：1991

  - **[埃及足球甲级联赛](../Page/埃及足球甲级联赛.md "wikilink")**：1次

1983

  - **[埃及足球杯](../Page/埃及足球杯.md "wikilink")**：3次

1990、1995、2004

  - **[埃及超级杯](../Page/埃及超级杯.md "wikilink")**：1次

2004

## 參考來源

[Category:埃及足球俱乐部](../Category/埃及足球俱乐部.md "wikilink")

1.  [当过建筑师学过潜水
    埃及新总理啥都能干](http://renwu.people.com.cn/n/2014/0317/c357069-24653674-2.html)