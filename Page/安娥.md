[剧作家安娥.jpg](https://zh.wikipedia.org/wiki/File:剧作家安娥.jpg "fig:剧作家安娥.jpg")
**安娥**（），[剧作家](../Page/剧作家.md "wikilink")。原名张式源，曾用名何平、张菊生，[河北](../Page/河北.md "wikilink")[获鹿人](../Page/获鹿.md "wikilink")。

## 生平

安娥1925年肄业于[北京国立艺术专科学校](../Page/北京国立艺术专科学校.md "wikilink")，同年加入[中国共产主义青年团](../Page/中国共产主义青年团.md "wikilink")，不久后加入[中国共产党](../Page/中国共产党.md "wikilink")。翌年，在[大连从事宣传](../Page/大连.md "wikilink")、女工工作。1927年赴[莫斯科中山大学学习](../Page/莫斯科中山大学.md "wikilink")。1929年回国，在[上海](../Page/上海.md "wikilink")[中共中央特工部工作](../Page/中共中央特工部.md "wikilink")，任国民党中央驻沪特派员[杨登瀛秘书](../Page/杨登瀛.md "wikilink")。曾发展[田汉入党](../Page/田汉.md "wikilink")，并开始诗歌创作。1931年与田汉未婚生一子。1932年，因中共机关遭破坏，与党组织失去联系。1932年后，她在上海参加进步文艺运动，曾任[百代唱片公司歌曲部主任](../Page/百代唱片公司.md "wikilink")。

[抗日战争期间](../Page/中國抗日戰爭.md "wikilink")，安娥任[战地记者](../Page/战地记者.md "wikilink")，1938年与[田汉结婚](../Page/田汉.md "wikilink")。后辗转[武汉](../Page/武汉.md "wikilink")、[重庆](../Page/重庆.md "wikilink")、[桂林](../Page/桂林.md "wikilink")、[昆明等地](../Page/昆明.md "wikilink")。
安娥是[中国战时儿童保育会的发起人和组织者之一](../Page/中国战时儿童保育会.md "wikilink")，并任保育会常务理事，
拯救、收容和培育了战争难童三万余名。 她为《战时儿童保育院院歌》作词， 保育生少儿时期随着院歌长大。

抗日战争胜利后回上海，1946年起在上海市实验戏剧学校执教。1948年赴解放区，次年重新加入中国共产党。

1949年后，安娥先后在[北京人民艺术剧院](../Page/北京人民艺术剧院.md "wikilink")、[中央实验歌剧院任创作员](../Page/中央实验歌剧院.md "wikilink")。1956年因病失去工作能力。

## 主要作品

  - 诗集《燕赵儿女》
  - 诗剧《高粱红了》、《洪波曲》、《战地之春》等
  - 儿童剧《假佬佬》、《海石花》
  - 戏曲剧本《情探》（与田汉合作）
  - 歌词《卖报歌》、《渔光曲》、《三个姑娘》、《节日的晚上》等

## 外部链接

  - [上海市地方志办公室
    《上海妇女志》](http://www.shtong.gov.cn/newsite/node2/node2245/node64804/node64821/node64958/node64962/userobject1ai59396.html)

## 參考网址

  - [二闲堂](http://www.edubridge.com/an-e/index.html)

[A](../Category/鹿泉人.md "wikilink")
[Category:中華民國劇作家](../Category/中華民國劇作家.md "wikilink")
[Category:中華人民共和國劇作家](../Category/中華人民共和國劇作家.md "wikilink")
[Category:張姓](../Category/張姓.md "wikilink")
[Category:莫斯科中山大学校友](../Category/莫斯科中山大学校友.md "wikilink")