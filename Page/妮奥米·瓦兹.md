**娜歐蜜·艾倫·華茲**（英語：**Naomi Ellen
Watts**，），生于[英格兰](../Page/英格兰.md "wikilink")，[英国和](../Page/英国.md "wikilink")[澳大利亚](../Page/澳大利亚.md "wikilink")[女演员和女](../Page/女演员.md "wikilink")[制作人](../Page/電影製作人.md "wikilink")。著名作品有《[七夜怪談西洋篇](../Page/七夜冤靈.md "wikilink")》、《[鳥人](../Page/鳥人_\(2014年電影\).md "wikilink")》、《[分歧者2](../Page/分歧者2：叛亂者.md "wikilink")》、《[分歧者3](../Page/分歧者3：赤誠者.md "wikilink")》。

## 生平

華茲在8岁前一直生活在出生地[西萨克赛斯的修略翰](../Page/西萨克赛斯.md "wikilink")。她的父亲彼得·瓦兹是[摇滚乐队](../Page/摇滚.md "wikilink")[平克·佛洛伊德的](../Page/平克·佛洛伊德.md "wikilink")[录音师](../Page/录音师.md "wikilink")。華茲的父母在她四岁时离异，三年后她的父亲身故，全家就搬到北[威尔士](../Page/威尔士.md "wikilink")[兰格费尼的祖父母家](../Page/兰格费尼.md "wikilink")，她在那里间断生活到14岁。華茲的母亲在一趟澳大利亚游之后认为，澳洲是真正的“机会的天堂”，於是1982年娜歐蜜和兄長隨著母親迁往[悉尼](../Page/悉尼.md "wikilink")。華茲的祖母是澳大利亚人，这使得她们一家更容易入澳大利亚籍。

在悉尼，華茲上过几个[演艺学校](../Page/演艺学校.md "wikilink")，就在第一节课上，她認識了[妮可·基曼](../Page/妮可·基曼.md "wikilink")，两人还乘同一辆計程車回家。

1986年華茲暂時告别了自己的演艺生涯，而到了[日本当](../Page/日本.md "wikilink")[模特](../Page/模特.md "wikilink")。可惜一无所获，她后来描述这段为期4个月的日本生涯是自己过去最暗的一段。她回到悉尼，在一家商店工作，之後则转到一家时尚杂志社工作。

她受一位同事的邀请而参演一部小[戏剧](../Page/戏剧.md "wikilink")，轻松的[剧本喚起華茲内在的对](../Page/剧本.md "wikilink")[舞台艺术的熱情](../Page/舞台艺术.md "wikilink")，於是華茲辞掉了自己的工作，重新踏上影視之路。

在一次电视广告的演出后，她在1988年於澳大利亚[连续剧](../Page/连续剧.md "wikilink")《*Home and
Away*》中得到了一个角色。之後她在1995年出演的《Tank
Girl》，是她在美國[好莱坞的演艺生涯中的突破](../Page/好莱坞.md "wikilink")。

2001年華茲担任[大卫·林奇导演的深奥](../Page/大卫·林奇.md "wikilink")[惊悚片](../Page/惊悚片.md "wikilink")
《[穆赫兰大道](../Page/穆赫兰大道.md "wikilink")》主角。虽然她的演出大放光彩，备受评论家的赞誉，她却未得到[奥斯卡金像奖的提名](../Page/奥斯卡金像奖.md "wikilink")。

在《穆赫兰大道》的成功之后，華茲片约不断，她出演了卖座电影《[七夜冤靈](../Page/七夜冤靈.md "wikilink")》（美国版[午夜凶铃](../Page/午夜凶铃.md "wikilink")）和《[七夜冤靈2](../Page/七夜冤靈2.md "wikilink")》（2005年美国版）。她在2004年因在电影《[21克](../Page/靈魂的重量.md "wikilink")》首度獲[奥斯卡金像獎提名](../Page/奥斯卡金像獎.md "wikilink")。

2012年[西班牙导演](../Page/西班牙.md "wikilink")[胡安·安东尼奥·巴亚纳執導改編真人實事的灾难片](../Page/胡安·安东尼奥·巴亚纳.md "wikilink")《[海啸奇迹](../Page/海啸奇迹.md "wikilink")》为她获得[奥斯卡最佳女主角奖提名](../Page/奥斯卡最佳女主角奖.md "wikilink")、[金球奖最佳戏剧类电影女主角提名和](../Page/金球奖.md "wikilink")[美国演员工会奖最佳女主角提名](../Page/美国演员工会奖.md "wikilink")。

## 私生活

[Flickr_-_csztova_-_Naomi_Watts_-_TIFF_09'_(1).jpg](https://zh.wikipedia.org/wiki/File:Flickr_-_csztova_-_Naomi_Watts_-_TIFF_09'_\(1\).jpg "fig:Flickr_-_csztova_-_Naomi_Watts_-_TIFF_09'_(1).jpg")
[Schreiber_and_Watts.jpg](https://zh.wikipedia.org/wiki/File:Schreiber_and_Watts.jpg "fig:Schreiber_and_Watts.jpg")
娜歐蜜2002到2004年間曾與已故澳洲男演員，[希斯·萊傑交往](../Page/希斯·萊傑.md "wikilink")。2005年春天，她跟美國男演員[李佛·薛伯約會](../Page/李佛·薛伯.md "wikilink")。娜歐蜜和李佛的長子，Alexander
Sasha Pete在2007年7月25日於洛杉磯出生；次子，Samuel
Kai則在2008年12月13日於紐約出生。另外，知名澳洲女演員[妮可·基曼是娜歐蜜認識多年的好友之一](../Page/妮可·基曼.md "wikilink")，基曼結婚前，亦邀請娜歐蜜參加她的單身派對。2016年9月，娜歐蜜和李佛透過平面媒體對外宣布分居決定，結束維持11年多的戀情，共同撫養兩個兒子。

## 电影作品

  - *For Love Alone*（1986年）
  - *The Custodian*（1986年）
  - [情挑玉女心](../Page/情挑玉女心.md "wikilink")（''Flirting '')( 1991年)
  - [異鄉情愁](../Page/異鄉情愁.md "wikilink")（*Wide Sargasso Sea*) (1993年)
  - *Matinee*（1993年）
  - [連鎖殺機](../Page/連鎖殺機.md "wikilink")（*Gross Misconduct*) (1993年)
  - [坦克女郎](../Page/坦克女郎.md "wikilink")（*Tank Girl*) (1995年)
  - [聚魔地](../Page/聚魔地.md "wikilink")（*Children of the Corn IV: The
    Gathering*) (1996年)
  - [神機暗算](../Page/神機暗算.md "wikilink")（*Persons Unknown*)(1996年)
  - [今生只愛你](../Page/今生只愛你.md "wikilink")（*Under the Lighthouse
    Dancing*)（1997年）
  - *A House Divided*（1998年）
  - [絕代寵妓](../Page/絕代寵妓.md "wikilink")（*Dangerous Beauty*)（1998年）
  - [我很乖因為我要出國](../Page/我很乖因為我要出國.md "wikilink")（*Babe: Pig in the
    City*)(1998年) ([配音](../Page/配音.md "wikilink")）
  - *The Christmas Wish*（1998年）
  - *Strange Planet*（1999年）
  - ''Never Date an Actress ''（2001年）
  - *Ellie Parker*（2001年）
  - *Down*（2001年）
  - [穆赫蘭大道](../Page/穆赫蘭大道.md "wikilink")（*Mulholland Drive*)（2001年）
  - *Rabbits*（2002年）
  - [七夜怪談西洋篇](../Page/七夜怪談西洋篇.md "wikilink")（*The Ring*)（2002年）
  - *Undertaking Betty*（2002年）
  - [法外狂徒](../Page/法外狂徒.md "wikilink")（*Ned Kelly*)（2003年）
  - [愛情合作社](../Page/愛情合作社.md "wikilink")（*Le Divorce*)（2003年）
  - [靈魂的重量](../Page/靈魂的重量.md "wikilink")（*21 Grams*)（2003年）
      - 提名[奧斯卡最佳女主角獎](../Page/奧斯卡最佳女主角獎.md "wikilink")
      - 提名[英國電影學院獎最佳女主角](../Page/英國電影學院獎最佳女主角.md "wikilink")
      - 提名[美國演員工會獎最佳女主角](../Page/美國演員工會獎最佳女主角.md "wikilink")
  - [爱不再回来](../Page/爱不再回来.md "wikilink")（*We Don't Live Here
    Anymore*)（2004年）
  - [刺杀尼克松](../Page/刺杀尼克松.md "wikilink")（*The Assassination of Richard
    Nixon*)（2004年）
  - [心靈偵探社](../Page/心靈偵探社.md "wikilink")（*I ♥ Huckabees*)（2004年）
  - [爱丽·帕克](../Page/爱丽·帕克.md "wikilink")（*Ellie Parker*)（2005年）
  - [剎靈](../Page/剎靈.md "wikilink")（*The Ring Two*)（2005年）
  - [回魂](../Page/回魂.md "wikilink")（*Stay*)（2005年）
  - [金刚](../Page/金刚_\(2005年电影\).md "wikilink")（*King Kong*)（2005年）
  - [內陸帝國](../Page/內陸帝國.md "wikilink")（*Inland Empire*)（2006年）(配音)
  - [面纱](../Page/面纱_\(电影\).md "wikilink")（*The Painted Veil*)（2006年）
  - [黑幕謎情](../Page/黑幕謎情.md "wikilink")（*Eastern Promises*)（2007年）
  - [大劊人心](../Page/大劊人心.md "wikilink")（*Funny Games*)（2008年）
  - [黑暗金控](../Page/黑暗金控_\(电影\).md "wikilink")（*The
    International*)（2009年）
  - [孕育心世界](../Page/孕育心世界.md "wikilink")（*Mother and Child*)（2010年）
  - [命中注定,遇見愛](../Page/命中注定,遇見愛.md "wikilink")（*You Will Meet a Tall
    Dark Stranger*)（2010年）
  - [不公平的戰爭](../Page/不公平的戰爭.md "wikilink")（*Fair Game*)（2010年）
  - [強·艾德格](../Page/強·艾德格.md "wikilink")（*J. Edgar*)（2011年）
  - [靈異豪宅](../Page/靈異豪宅.md "wikilink")（*Dream House*)（2011年）
  - [海嘯-{奇}-蹟](../Page/海嘯奇蹟.md "wikilink")（(西班牙语：*Lo Imposible*)（2012年）
      - 提名[奧斯卡最佳女主角獎](../Page/奧斯卡最佳女主角獎.md "wikilink")
      - 提名[金球獎最佳戲劇類電影女主角](../Page/金球獎最佳戲劇類電影女主角.md "wikilink")
      - 提名[美國演員工會獎最佳女主角](../Page/美國演員工會獎最佳女主角.md "wikilink")
  - [激愛543](../Page/激愛543.md "wikilink")（*Movie 43*)（2013年）
  - [戴安娜](../Page/戴安娜_\(電影\).md "wikilink")（*Diana*)（2013年）
  - [愛‧墮落](../Page/愛‧墮落.md "wikilink") (Adore) (2013)
    (主演兼任[執行製作人](../Page/執行製作人.md "wikilink"))
  - [開啟陽光的錀匙](../Page/開啟陽光的錀匙.md "wikilink") /
    [小小陽光](../Page/小小陽光.md "wikilink") (*Sunlight
    Jr.*)（2013年）
  - [歐吉桑鄰好](../Page/歐吉桑鄰好.md "wikilink")(*St. Vincent de Van
    Nuys*)（2014年）
  - [鳥人](../Page/鳥人_\(2014年電影\).md "wikilink")（2014年）
  - [分歧者2：叛亂者](../Page/分歧者2：叛亂者.md "wikilink")(*The Divergent Series:
    Insurgent*)（2015年）
  - [青春倒退嚕](../Page/青春倒退嚕.md "wikilink")(*While We're Young*)（2015年）
  - [崩壞人生](../Page/崩壞人生.md "wikilink")(*Demolition* )（2016年）
  - [分歧者3：赤誠者](../Page/分歧者3：赤誠者.md "wikilink")(*The Divergent Series:
    Allegiant*)（2016年）
  - [育陰房](../Page/育陰房.md "wikilink")(*Shut In* )（2016年）
  - [幸福不設限](../Page/幸福不設限.md "wikilink")(*About Ray* / *3 Generations*
    )（2017年）[(3 Generations
    IMDb)](http://www.imdb.com/title/tt4158624/)
  - [亨利之書](../Page/亨利之書.md "wikilink")(*The Book of Henry*) (2017年)
    (美國上映)
  - [玻璃城堡](../Page/玻璃城堡_\(電影\).md "wikilink")（*The Glass Castle*)
    (2017年)



## 電視作品

  - [聚散離合](../Page/聚散離合.md "wikilink") ( *Home and Away* )（1988年）
  - 夢遊者（''Sleepwalkers '') (1998年)
  - *The Hunt for the Unicorn
    Killer*（1999年）([電視電影](../Page/電視電影.md "wikilink"))
  - *The Wyvern Mystery*（2000年）(電視電影)
  - *The Outsider*（2002年）(電視電影)
  - [雙峰](../Page/雙峰_\(2017年電視劇\).md "wikilink")（*Twin Peaks season: The
    Return*) (2017年)
  - 越界（*Gypsy* ) ( 2017年）(網路電視劇)
    (主演兼任[執行製作人](../Page/執行製作人.md "wikilink"))
    [(Gypsy IMDb)](http://www.imdb.com/title/tt5503718/)

## 外部链接

  -
  - [Naomi-Watts.org](https://web.archive.org/web/20170911152933/http://naomi-watts.org/)

  - [官方網站](http://www.naomiwatts.com/)

  - [The Official Naomi Watts](http://www.myspace.com/naomiwatts) at
    MySpace.com

[Category:澳大利亞女演員](../Category/澳大利亞女演員.md "wikilink")
[Category:21世纪英格兰女演员](../Category/21世纪英格兰女演员.md "wikilink")
[Category:在美國的澳大利亞人](../Category/在美國的澳大利亞人.md "wikilink")
[Category:澳大利亞電影演員](../Category/澳大利亞電影演員.md "wikilink")
[Category:威爾斯裔澳大利亞人](../Category/威爾斯裔澳大利亞人.md "wikilink")
[Category:澳大利亞電視演員](../Category/澳大利亞電視演員.md "wikilink")
[Category:在美國的英國人](../Category/在美國的英國人.md "wikilink")
[Category:英格兰女电影演员](../Category/英格兰女电影演员.md "wikilink")
[Category:英國電影監製](../Category/英國電影監製.md "wikilink")
[Category:英國舞台演員](../Category/英國舞台演員.md "wikilink")
[Category:英格兰女电视演员](../Category/英格兰女电视演员.md "wikilink")
[Category:美国演员工会奖最佳电影群体演出奖得主](../Category/美国演员工会奖最佳电影群体演出奖得主.md "wikilink")