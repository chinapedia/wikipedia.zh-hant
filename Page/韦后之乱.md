**韋后之乱**是指[唐中宗](../Page/唐中宗.md "wikilink")[李顯之](../Page/李顯.md "wikilink")[皇后](../Page/皇后.md "wikilink")[韦氏](../Page/韦后.md "wikilink")，在中宗[復辟之後的专权乱政](../Page/復辟.md "wikilink")。

[韦皇后被指控為重演](../Page/韦皇后.md "wikilink")[武則天故事篡位稱帝](../Page/武則天.md "wikilink")，進而協同女兒[安樂公主共謀弒君](../Page/安樂公主.md "wikilink")，毒死中宗。中宗死后，韦皇后立少帝為[傀儡政權](../Page/傀儡政權.md "wikilink")，以[皇太后身份](../Page/皇太后.md "wikilink")[摄政](../Page/摄政.md "wikilink")，[挾天子以令諸侯](../Page/挾天子以令諸侯.md "wikilink")。

[唐少帝](../Page/唐少帝.md "wikilink")[唐隆元年](../Page/唐隆.md "wikilink")（710年），[临淄王](../Page/临淄.md "wikilink")[李隆基聯合](../Page/李隆基.md "wikilink")[太平公主發動](../Page/太平公主.md "wikilink")[兵變](../Page/兵變.md "wikilink")，杀韦太后、安乐公主等人，以李隆基之父[李旦](../Page/李旦.md "wikilink")[復辟为帝](../Page/復辟.md "wikilink")，即[唐睿宗](../Page/唐睿宗.md "wikilink")。

## 簡介

韦氏（？-710年），[京兆](../Page/京兆.md "wikilink")[万年](../Page/万年.md "wikilink")（今[陕西](../Page/陕西.md "wikilink")[长安](../Page/长安.md "wikilink")）人，唐中宗李顯之妻。

[弘道元年](../Page/弘道.md "wikilink")（683年），[高宗](../Page/高宗.md "wikilink")[李治崩](../Page/李治.md "wikilink")，太子李顯即位，是為中宗第一次即位。次年，立韦氏为[皇后](../Page/皇后.md "wikilink")。同年，中宗因寵愛韋皇后，欲封韋皇后之父[韋玄貞為](../Page/韋玄貞.md "wikilink")[侍中](../Page/侍中.md "wikilink")，[中書令](../Page/中書令.md "wikilink")[裴炎反對](../Page/裴炎.md "wikilink")，中宗不滿，對裴炎說：「朕是皇帝，就是把天下送給韋玄真，也無不可。」裴炎密告武太后
( [武则天](../Page/武则天.md "wikilink") )
，太后藉中宗君出戲言為由，废黜中宗為[廬陵王](../Page/廬陵王.md "wikilink")
( 684 年
)，迁于[房州](../Page/房州.md "wikilink")（今[湖北](../Page/湖北.md "wikilink")[房县](../Page/房县.md "wikilink")），韦氏随行。

後[睿宗](../Page/睿宗.md "wikilink")[李旦即位](../Page/李旦.md "wikilink")，李顯一家人在房州克勤克儉，生女[李裹兒](../Page/李裹兒.md "wikilink")
( 後來的[安樂公主](../Page/安樂公主.md "wikilink")
)，因物資缺乏，在裹兒出生時，李顯即時脫下上衣包裹為襁褓，故賜名裹兒。他們在房州歷經睿宗禪位、武則天稱帝、遷都[洛陽等政局的變化](../Page/洛陽.md "wikilink")，終於
14 年後 ( 698 年 )，武則天召李顯回京，並重立為太子，韋氏為太子妃，女裹兒為安樂郡主。

[神龙元年](../Page/神龙.md "wikilink")（705年）[革命](../Page/神龍革命.md "wikilink")，[張柬之等人逼宮](../Page/張柬之.md "wikilink")，武則天退位，扶持太子李顯[復辟](../Page/復辟.md "wikilink")，韋氏亦復其后位，尊母親武則天為「聖神皇帝」。每临朝，韦后都要置幔坐在殿上，预闻政事。中宗任用曾为武则天掌文书的[昭容](../Page/昭容.md "wikilink")（宫中[女官](../Page/女官.md "wikilink")）[上官婉儿為](../Page/上官婉儿.md "wikilink")[蘭台令史](../Page/蘭台令史.md "wikilink")，主持撰述[诏](../Page/诏.md "wikilink")[敕](../Page/敕.md "wikilink")，以[武三思为](../Page/武三思.md "wikilink")[宰相](../Page/宰相.md "wikilink")。安樂郡主初長成，改封為安樂公主，嫁于武三思之子[武崇訓](../Page/武崇訓.md "wikilink")。当时朝中形成一个以韦氏为首的**武、韦专政集团**。武三思通过韦后及其爱女[安乐公主](../Page/安乐公主.md "wikilink")，诬陷并迫害拥戴中宗复位的[张柬之](../Page/张柬之.md "wikilink")、[敬晖等功臣](../Page/敬晖.md "wikilink")。中宗对揭发武、韦丑行的人处以极刑，武三思因而权倾人主，作威作福。

中宗的太子[李重俊非韦氏所生](../Page/李重俊.md "wikilink")，遭到韦后厌恶；安乐公主与其夫武崇训经常侮辱重俊。安樂公主亦受祖母武則天的影響，想當[皇嗣](../Page/皇嗣.md "wikilink")
( 皇太女
)，以其天生麗質為傲，大膽淫亂，廣納[面首](../Page/面首.md "wikilink")，並視重俊為眼中釘，呼之為奴，重俊地位更加危險。

神龙三年（708年）七月重俊與[兵部尚書](../Page/兵部尚書.md "wikilink")[魏元忠通謀](../Page/魏元忠.md "wikilink")，聯合成王[李千里](../Page/李千里.md "wikilink")、左羽林大將軍[李多祚等](../Page/李多祚.md "wikilink")，发动[重俊之變](../Page/重俊之變.md "wikilink")，以少數[羽林军斬殺了武三思与武崇训](../Page/羽林军.md "wikilink")，又攻進宮城，谋诛韦后、安乐公主等，但因中宗親自[心戰喊話](../Page/心戰.md "wikilink")，随从的羽林军倒戈，斬殺李多祚等[將領](../Page/將領.md "wikilink")，[政变失败](../Page/政变.md "wikilink")，重俊、千里等人皆死。中宗優柔寡斷，聽從韋皇后的說話，[伐取李重俊的](../Page/獵首.md "wikilink")[首級祭祀武三思父子](../Page/首級.md "wikilink")。

此时武、韦集团权势依旧不减。内地水旱为灾，户口逃散，民不聊生。中宗却与韦后恣意淫乐，不理[朝政](../Page/朝政.md "wikilink")，还[处死上书告发韦氏乱政的人](../Page/处死.md "wikilink")。

景龙四年（710年）中宗駕崩 ( 多方顯示中宗為韋皇后與安樂公主所毒害，但未經證實
)，韦后立[李重茂为帝](../Page/李重茂.md "wikilink")，史称[唐少帝](../Page/唐少帝.md "wikilink")，改元[唐隆](../Page/唐隆.md "wikilink")，韦后以皇太后身份临朝摄政，又任用韦氏子弟统领南北衙军队，并欲效法[武则天](../Page/武则天.md "wikilink")，自居[帝位](../Page/帝位.md "wikilink")。

[相王李旦之子临淄王](../Page/相王.md "wikilink")[李隆基](../Page/李隆基.md "wikilink")（后来的唐玄宗）与姑母[太平公主](../Page/太平公主.md "wikilink")（武则天之女）发动[唐隆之變](../Page/唐隆之變.md "wikilink")，以[禁军攻入宫城](../Page/禁军.md "wikilink")，杀韦太后、安乐公主、上官婉儿及诸韦子弟。韦后之乱，至此结束。

事后，韦后被追贬为庶人，安乐公主被追贬为悖逆庶人，分别改以一品和三品礼葬。后李隆基与太平公主更迫韦后所立的少帝[退位](../Page/退位.md "wikilink")，迎回相王李旦復辟。

## 疑点

中宗被韦后毒杀一事《[旧唐书](../Page/旧唐书.md "wikilink")》没有记载，只见于时间更往后的《[新唐书](../Page/新唐书.md "wikilink")》和《[资治通鉴](../Page/资治通鉴.md "wikilink")》，《旧唐书》只说人们**怀疑**是安乐公主与[太医合谋毒死了唐中宗](../Page/太医.md "wikilink")，并不确定，且韦后被李隆基杀死后虽被追废为庶人，仍以一品诰命夫人的待遇厚葬，不似弑君凶手之待遇。综上，《新唐书》和《资治通鉴》关于韦后毒死中宗一事的记载甚不严谨，有编造历史之嫌。

## 参考文献

  - 《[旧唐书](../Page/旧唐书.md "wikilink") 列传第一》
  - 《新唐书》
  - 《[资治通鉴](../Page/资治通鉴.md "wikilink")》

[Category:唐朝政治事件](../Category/唐朝政治事件.md "wikilink")
[Category:西安政治史](../Category/西安政治史.md "wikilink")
[Category:710年](../Category/710年.md "wikilink")
[Category:唐朝歷史事件](../Category/唐朝歷史事件.md "wikilink")