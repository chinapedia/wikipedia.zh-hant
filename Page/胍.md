**胍**（英语：**Guanidine**）是一个含氮的有机化合物，晶状固体，有强[碱性](../Page/碱性.md "wikilink")，也称“氨基甲[脒](../Page/脒.md "wikilink")”，可由[瓜氨酸氧化制得](../Page/瓜氨酸.md "wikilink")。一般以盐的形式使用，是[有机合成](../Page/有机合成.md "wikilink")（合成[杂环化合物](../Page/杂环化合物.md "wikilink")）、药物、染料合成的中间体。胍也存在于[尿液中](../Page/尿液.md "wikilink")，是[蛋白质的](../Page/蛋白质.md "wikilink")[代谢产物](../Page/代谢.md "wikilink")。

胍可由[碘化铵与](../Page/碘化铵.md "wikilink")[氰氨化钙反应或](../Page/氰氨化钙.md "wikilink")[尿素与](../Page/尿素.md "wikilink")[氨加压反应制得](../Page/氨.md "wikilink")。

## 碱性

胍的碱性与无机强碱相当，可以和酸成盐。其[共轭酸胍盐正离子](../Page/共轭酸.md "wikilink")（或称胍鎓）是\[CH<sub>6</sub>N<sub>3</sub>\]<sup>+</sup>，[p*K*<sub>a</sub>为](../Page/pKa.md "wikilink")13.5，是体内胍的存在形式。这种特殊的性质可归结于[Y—芳香性](../Page/Y—芳香性.md "wikilink")。

<table>
<tbody>
<tr class="odd">
<td style="text-align: center;"><p><a href="File:Guanidinium-ion-3D-balls.png">File:Guanidinium-ion-3D-balls.png</a>|</p>
<center>
<p><a href="../Page/球棍模型.md" title="wikilink">球棍模型</a></p>
</center>
<p><a href="File:Guanidinium-ion-2D-skeletal.png">File:Guanidinium-ion-2D-skeletal.png</a>|</p>
<center>
<p><a href="../Page/共振论.md" title="wikilink">共振杂化体</a></p>
</center></td>
</tr>
<tr class="even">
<td style="text-align: center;"><p><a href="https://zh.wikipedia.org/wiki/File:Guanidinium-ion-canonical-forms-2D-skeletal.png" title="fig:Guanidinium-ion-canonical-forms-2D-skeletal.png">Guanidinium-ion-canonical-forms-2D-skeletal.png</a></p></td>
</tr>
</tbody>
</table>

常见的胍盐包括：[盐酸胍](../Page/盐酸胍.md "wikilink")，属于[离液剂](../Page/离液剂.md "wikilink")（chaotropic），用于蛋白质的[变性](../Page/变性.md "wikilink")，以及[硫氰酸胍](../Page/硫氰酸胍.md "wikilink")、[硝酸胍](../Page/硝酸胍.md "wikilink")、[碳酸胍等](../Page/碳酸胍.md "wikilink")。

[Guanidine-group-2D-skeletal.png](https://zh.wikipedia.org/wiki/File:Guanidine-group-2D-skeletal.png "fig:Guanidine-group-2D-skeletal.png")

## 衍生物

**胍**也可以指母体化合物胍形成的衍生物，通式为(R<sup>1</sup>R<sup>2</sup>N)(R<sup>3</sup>R<sup>4</sup>N)C=N-R<sup>5</sup>。胍衍生物属于[胺醛](../Page/胺醛.md "wikilink")，也具有[亚胺的结构](../Page/亚胺.md "wikilink")，例子包括[TBD和](../Page/TBD.md "wikilink")[石房蛤毒素](../Page/石房蛤毒素.md "wikilink")。

## 註釋

## 参见

  - [硫氰酸胍](../Page/硫氰酸胍.md "wikilink")
  - [硝酸胍](../Page/硝酸胍.md "wikilink")

[G](../Category/官能团.md "wikilink")
[Category:有机氮化合物](../Category/有机氮化合物.md "wikilink")
[\*](../Category/胍.md "wikilink")
[Category:一碳有机物](../Category/一碳有机物.md "wikilink")