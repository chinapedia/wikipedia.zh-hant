《**查拉圖斯特拉如是說**》（），[徐梵澄译本译为](../Page/徐梵澄.md "wikilink")《**苏鲁支语录**》、[鲁迅最初翻译为](../Page/鲁迅.md "wikilink")《**察罗堵斯德罗绪言**》，是[德國](../Page/德國.md "wikilink")[哲學家](../Page/哲學家.md "wikilink")[尼采假托](../Page/尼采.md "wikilink")[古波斯](../Page/古波斯.md "wikilink")[祆教創始人](../Page/祆教.md "wikilink")[查拉圖斯特拉](../Page/查拉圖斯特拉.md "wikilink")（又譯[瑣羅亞斯德](../Page/瑣羅亞斯德.md "wikilink")）之口於1885年寫作完成的書，是哲学史上最著名的[哲學書籍之一](../Page/哲學.md "wikilink")。

## 簡介

此書不同部分於不同年份完成，共四部，前二部分1883年出版，第三部於1884年出版；第四部雖於1885年初寫完，但因為前三部銷售不佳，出版社拒印，尼采自行少量印刷；直至1892年才與前三部合併出版。1891年，尼采妹妹伊莉莎白（Elisabeth
Förster-Nietzsche）據說因為擔心對神褻瀆而遭禁，曾企圖阻止查拉圖斯特拉第四部出版。1894年後妹妹編輯尼采檔案，並出版全集共十九卷數次，尼采於1900年過世。

此書以記事方式描寫一個哲學家的流浪及教導，與其說本書是一本哲學專書，毋寧說是一本寫的偏向探討哲學的小說。

查拉圖斯特拉是古代[波斯](../Page/波斯.md "wikilink")[祆教的先知](../Page/祆教.md "wikilink")、創始人的名字，該先知的希臘名字可能更為人知：「[瑣羅亞斯德](../Page/瑣羅亞斯德.md "wikilink")」（中国古书中则称为“苏鲁支”）。此書使用詩及小說的方式，時常諷刺[新約聖經](../Page/新約聖經.md "wikilink")，來探索尼采的許多觀念。

书中的查拉图斯特拉有两个动物相伴，分别为[蛇和](../Page/蛇.md "wikilink")[鹰](../Page/鹰.md "wikilink")。一般来说，这两种动物分别对应了尼-{采}-在此书上的两个重要观点。即以盘旋的蛇对应[永恒轮回学说](../Page/永恒轮回.md "wikilink")，以飞翔的雄鹰来意喻[超人說](../Page/超人說.md "wikilink")。

查拉圖斯特拉的中心概念，人類是猿猴及尼采稱為「超人」（）之間的一個過渡形式。「超人」這個名詞是此書裏其中一個雙關語，無疑象徵著拂曉時太陽從遠方地平線而來，如同戰勝的基本想法。

大部分情節不連貫，查拉圖斯特拉書裏的故事能用任何方式閱讀。查拉圖斯特拉有句名言：「[上帝已死](../Page/上帝已死.md "wikilink")」(實則歷史上的[查拉圖斯特拉宣稱](../Page/查拉圖斯特拉.md "wikilink")[阿胡拉·馬茲達為造物主](../Page/阿胡拉·馬茲達.md "wikilink")，並且從未否認其永恆地位與主宰權)；雖然這句話在尼采之前一本書《[快樂的科學](../Page/快樂的科學.md "wikilink")》中也出現過\[1\]。

此書原本預計要寫下最後兩章，但最後停在第四卷，以「預兆」章節作結。

未寫的最後兩部計畫描寫查拉圖斯特拉的佈道工作以及最後的死亡。

## 音樂、電影與美術

[Buch_Also_sprach_Zarathustra.jpg](https://zh.wikipedia.org/wiki/File:Buch_Also_sprach_Zarathustra.jpg "fig:Buch_Also_sprach_Zarathustra.jpg")\]\]
《查拉圖斯特拉如是說》後來由[理查·史特勞斯從書中所獲得的靈感寫作同名的](../Page/理查·史特勞斯.md "wikilink")[交響詩](../Page/查拉圖斯特拉如是說_\(史特勞斯\).md "wikilink")，於1896年完成。乐曲没有具体的故事情节，其内容完全按照作者自己对[尼采](../Page/尼采.md "wikilink")[哲学与](../Page/哲学.md "wikilink")[美学思想的想象来创造的](../Page/美学.md "wikilink")。为描摹抽象对立的哲学思想，作者使用了两个差异很大的调性——代表人类的B调和代表自然的C调，在它们转换和对立的基础上，赋予交响诗戏剧性的冲突。全曲共分九段，除第一段《日出》的标题是理查·施特劳斯自己添加外，其余八段标题均来自尼采的原著，分别是《关于后世界人》、《关于强烈的渴念》、《关于欢乐与激情》、《挽歌》、《关于科学》、《久病初愈之人》、《舞曲》和《夜曲》\[2\]。

後來此曲於[斯坦利·库布里克於一九六八年的電影](../Page/斯坦利·庫布里克.md "wikilink")《[2001太空漫遊](../Page/2001太空漫遊.md "wikilink")》中做為電影配樂。該電影的部分靈感也被認為來自於查拉圖斯特拉如是說這本書。

## 評論

[王偉雄認為](../Page/王偉雄.md "wikilink")，[尼采](../Page/尼采.md "wikilink")《[查拉圖斯特拉如是說](../Page/查拉圖斯特拉如是說.md "wikilink")》能讓人全新或更深刻領會人生和世事：

> 「自有人類以來，人們自尋歡樂的事太少；單單這一點，我的兄弟們，那是我們的原罪！如果我們學會了更好地自尋歡樂，就最能使我們忘掉使他人受苦和想出折磨他人的詭計。」([尼采](../Page/尼采.md "wikilink")《[查拉圖斯特拉如是說](../Page/查拉圖斯特拉如是說.md "wikilink")》．第二部：〈同情者〉)

王偉雄認為，尼采是認為自得其樂和折磨他人有對立關係：越是要「使他人受苦和想出折磨他人的詭計」，就越難自得其樂。王偉雄從中得到一個觀點：我們可以「遺忘使他人受苦和想出折磨他人的詭計」，而享受自得其樂的時刻，例如：當打算報復某人在Facebook的惡意留言，不如即刻去沖一杯香濃咖啡，邊飲邊細心閱讀尼采《查拉圖斯特拉如是說》。\[3\]

## 參考

## 外部連結

  - [AlsoSprachZaratustra in c2.com
    wiki](http://c2.com/cgi/wiki?AlsoSprachZaratustra)
  - [audio file (MIDI) Thus Spake
    Zarathustra](http://www.midisite.co.uk/midi_search/strauss.html)
  - [Also Sprach
    Zaratustra](http://gutenberg.spiegel.de/nietzsch/zara/also.htm)
  - [系列畫作《查拉圖斯特拉如是說》,
    俄国画家莉娜閻王](http://www.lenahades.co.uk/#!chinese-zarathustra/ckmv)

[Category:哲学书籍](../Category/哲学书籍.md "wikilink")
[Category:交響詩](../Category/交響詩.md "wikilink")
[Category:弗里德里希·尼采](../Category/弗里德里希·尼采.md "wikilink")

1.  C. Guignon, D. Pereboom. *Existentialism: Basic Writings, 2nd ed.*,
    Hackett, 2001. pp. 101–113
2.
3.