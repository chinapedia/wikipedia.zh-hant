**FOXP2**（Forkhead box protein P2
(FOXP2)）基因，即**叉头框P2**基因，是一个与[言语功能的](../Page/言语.md "wikilink")[发育有关的](../Page/发育.md "wikilink")[基因](../Page/基因.md "wikilink")。在人类，FOXP2基因位于第7对[染色体上](../Page/染色体.md "wikilink")。FOXP2基因在许多其它具有复杂发声、及发声学习能力的动物，例如[鸣禽中](../Page/鸣禽.md "wikilink")，也有发现。该基因的异常在人类导致特定的先天性[言语障碍](../Page/言语障碍.md "wikilink")。

## 表型

语言学家一般认为FOXP2不仅与言语[运动控制有关](../Page/运动控制.md "wikilink")，也与[语法](../Page/语法.md "wikilink")、[语义等更高级的语言功能有关](../Page/语义.md "wikilink")。其主要理由在于

  - FOXP2异常同时导致语言理解困难；
  - 通过[脑功能成像发现](../Page/脑功能成像.md "wikilink")，患有FOXP2异常的病人在语言相关的[皮层区域也有异常表现](../Page/皮层.md "wikilink")，而不局限于脑的运动系统。

## FOXP2基因的发现

寻找FOXP2的工作始于对“KE”（或称“K”）家族的研究。该家族的一些成员患有遗传性的言语障碍，且有三代在世的家族成员受其害。进一步的研究显示，该基因为[常染色体](../Page/常染色体.md "wikilink")[显性](../Page/显性.md "wikilink")。通过对患病与无病的家族成员的染色体的扫描，研究人员将该基因定位在第7号染色体上。这个基因最初被称为SPCH1。接下来又通过[细菌人工染色体手段对该染色体上的相关区域进行了](../Page/细菌人工染色体.md "wikilink")[测序](../Page/基因测序.md "wikilink")。在基因测序开展期间，他们又找到了另一位患有这种先天性言语障碍，但不属于KE家族的病人。对这个新样本的染色体扫描显示，第7对染色体一处有断裂。

## 进化

虽然FOXP2在许多动物中存在，但是[人类的FOXP](../Page/人类.md "wikilink")2基因在最近二十万年间经历了一系列[进化](../Page/进化.md "wikilink")。该事实可能支持该基因与人类独有的语言功能和创造力有关的猜测。

## 参考文献

  - Lai CSL, Fisher SE, Hurst JA, et al.: A forkhead-domain gene is
    mutated in a severe speech and language disorder. Nature 2001,
    413:519-523.
  - Enard W, Przeworski M, Fisher SE, et al.: Molecular evolution of
    FOXP2, a gene involved in speech and language. Nature 2002, 418:
    869-872.

## 参见

  - [布洛卡区](../Page/布洛卡区.md "wikilink")

  - [人类进化](../Page/人类进化.md "wikilink")

  -
## 外部链接

  - [Gene information at NCBI](https://www.ncbi.nlm.nih.gov/gene/93986)

  - [Gene information at Genetic Home
    Reference](http://ghr.nlm.nih.gov/gene/FOXP2)

  - [Language and Genetics
    Research](http://www.mpi.nl/departments/language-and-genetics) at
    the [Max Planck Institute for
    Psycholinguistics](../Page/Max_Planck_Institute_for_Psycholinguistics.md "wikilink")

  - [The FOXP2
    story](https://web.archive.org/web/20091116190757/http://genome.wellcome.ac.uk/doc_WTD020797.html)

  - [Revisiting FOXP2 and the origins of
    language](http://scienceblogs.com/notrocketscience/2009/11/11/revisiting-foxp2-and-the-origins-of-language/)

  - [FOXP2 and the Evolution of
    Language](http://www.evolutionpages.com/FOXP2_language.htm)

  -
[Category:叉头转录因子](../Category/叉头转录因子.md "wikilink")
[Category:基因](../Category/基因.md "wikilink")
[Category:语言的演变](../Category/语言的演变.md "wikilink")