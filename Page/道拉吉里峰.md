[Dhaulagiri_I.jpg](https://zh.wikipedia.org/wiki/File:Dhaulagiri_I.jpg "fig:Dhaulagiri_I.jpg")
**道拉吉里峰**（**Dhaulagiri**）位于[喜马拉雅山脉中段](../Page/喜马拉雅山脉.md "wikilink")[尼泊尔境内](../Page/尼泊尔.md "wikilink")，海拔8,167-{zh:米;
zh-hans:米; zh-hant:公尺;}-，是世界第七高峰。「道拉吉里」的字面意思是「白山」。

## 参见

  - [山峰列表](../Page/山峰列表.md "wikilink")

[Category:八千米高山](../Category/八千米高山.md "wikilink")