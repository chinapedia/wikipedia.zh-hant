**刘荻**（），毕业于[北京师范大学](../Page/北京师范大学.md "wikilink")[心理學系](../Page/心理學.md "wikilink")。“不锈钢老鼠”是她在[西祠胡同](../Page/西祠胡同.md "wikilink")[BBS的](../Page/BBS.md "wikilink")[笔名](../Page/笔名.md "wikilink")。曾任自由中国论坛常务副站长。2013年起任独立中文笔会理事\[1\]。

## 事蹟

刘荻是《[人民日报](../Page/人民日报.md "wikilink")》老记者[刘衡的](../Page/刘衡.md "wikilink")[孙女](../Page/孙女.md "wikilink")，刘衡1950年代被划为“[右派](../Page/右派.md "wikilink")”\[2\]。

她在[留言板上的言论包括](../Page/留言板.md "wikilink")《今天，我們都是自由人》、《柿油派網蟲集體向黨和政府投誠──虛擬現實主義方案》等文章支援創辦公益性網站[六四天網而被逮捕的](../Page/六四天網.md "wikilink")[黃琦](../Page/黃琦.md "wikilink")。

在2002年11月9日被北京市的便衣警察「[國保總隊](../Page/国保.md "wikilink")」以涉嫌參加「非法組織」的名義从校园抓走，关在北京[秦城監獄一年多](../Page/秦城監獄.md "wikilink")，引發各界聲援。民间爱滋义工[胡佳等向](../Page/胡佳_\(1973年\).md "wikilink")[北京市公安局申请游行抗议非法超期羁押刘荻](../Page/北京市公安局.md "wikilink")，但未果。在2003年11月28日連同另外兩人得到假释，后被免于起诉。之後她以相同筆名活躍於內地《[觀點](../Page/觀點.md "wikilink")》論壇、[凱迪社區](../Page/凱迪社區.md "wikilink")。

[太石村罢免事件后](../Page/太石村罢免事件.md "wikilink")，刘荻与[赵昕发起为太石村刑拘村民筹集法律援助款项](../Page/赵昕.md "wikilink")。

2007年获[赫尔曼-哈米特奖](../Page/赫尔曼-哈米特奖.md "wikilink")。2012年获德国金鸽奖\[3\]。

2014年5月因参加“六四研讨会”被传唤和刑拘，其他被捕人士包括人权律师[浦志强](../Page/浦志强.md "wikilink")﹑人权活动家[胡石根](../Page/胡石根.md "wikilink")、学者[徐友渔以及](../Page/徐友渔.md "wikilink")[北京电影学院教授](../Page/北京电影学院.md "wikilink")[郝建](../Page/郝建.md "wikilink")\[4\]。同年6月5日获得释放。\[5\]

## 参见

  - [中华人民共和国持不同政见者列表](../Page/中华人民共和国持不同政见者列表.md "wikilink")

## 参考文献

## 外部链接

  - [個人博客](https://web.archive.org/web/20140202151128/http://www.scifimouse.com/)

  -
  - 杜导斌等(2003)，《[我们愿陪刘荻坐牢](http://www.epochtimes.com/gb/3/9/30/n385043.htm)》

  - [刘荻：谁来保卫我们的规则](http://www.chinalaborunion.org/society.php?id=533)

[D](../Category/刘姓.md "wikilink")
[L](../Category/北京师范大学校友.md "wikilink")
[L](../Category/网民.md "wikilink")
[L](../Category/中华人民共和国持不同政见者.md "wikilink")
[L](../Category/零八宪章签署者.md "wikilink")

1.
2.  海涛，[《公开信：中国当局应宣布反右违宪》](http://www.voanews.com/chinese/w2007-03-04-voa32.cfm)，[美国之音中文版](../Page/美国之音.md "wikilink")，2007年3月4日，2010年1月5日访问
3.  [德国之声](../Page/德国之声.md "wikilink")
4.
5.