**启历**（[1052年五月](../Page/1052年.md "wikilink")-[1053年正月](../Page/1053年.md "wikilink")）是[北宋时期](../Page/北宋.md "wikilink")[儂智高的](../Page/儂智高.md "wikilink")[年号](../Page/年号.md "wikilink")，共计2年。

另《[玉海](../Page/玉海.md "wikilink")》还有“端懿”年号，不详何时。《纪元编》又有“大历”年号，李崇智認為是儂智高國號\[1\]。

## 紀年

|                                |                                      |                                      |
| ------------------------------ | ------------------------------------ | ------------------------------------ |
| 启历                             | 元年                                   | 二年                                   |
| [公元](../Page/公元.md "wikilink") | [1052年](../Page/1052年.md "wikilink") | [1053年](../Page/1053年.md "wikilink") |
| [干支](../Page/干支.md "wikilink") | [壬辰](../Page/壬辰.md "wikilink")       | [癸巳](../Page/癸巳.md "wikilink")       |

## 参看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 同期存在的其他政權之紀年
      - [皇祐](../Page/皇祐.md "wikilink")（1049年-1054年三月）：[宋仁宗的年號](../Page/宋仁宗.md "wikilink")
      - [政安](../Page/政安.md "wikilink")：[大理](../Page/大理.md "wikilink")—[段思廉](../Page/段思廉.md "wikilink")（1045年至1075年在位）之年號
      - [重熙](../Page/重熙.md "wikilink")（1032年十一月至1055年八月）：[契丹](../Page/契丹.md "wikilink")—遼興宗[耶律宗真之年號](../Page/耶律宗真.md "wikilink")
      - [永承](../Page/永承.md "wikilink")（1046年[四月十四至](../Page/四月十四.md "wikilink")1053年[正月初一](../Page/正月初一.md "wikilink")）：[日本](../Page/日本.md "wikilink")[後冷泉天皇年号](../Page/後冷泉天皇.md "wikilink")
      - [天喜](../Page/天喜.md "wikilink")（1053年正月初一至1058年[八月廿九](../Page/八月廿九.md "wikilink")）：日本後冷泉天皇年号
      - [崇興大寶](../Page/崇興大寶.md "wikilink")（1049年至1054年）：[越南](../Page/越南.md "wikilink")[李佛瑪之年號](../Page/李佛瑪.md "wikilink")
      - [天祐垂聖](../Page/天祐垂聖.md "wikilink")（1050年十月至1052年）：[西夏](../Page/西夏.md "wikilink")—夏毅宗[李諒祚之年號](../Page/李諒祚.md "wikilink")
      - [福聖承道](../Page/福聖承道.md "wikilink")（1053年至1056年）：西夏—夏毅宗李諒祚之年號

## 参考文献

<div class="references-small">

<references />

</div>

[Category:北宋民变政权年号](../Category/北宋民变政权年号.md "wikilink")
[Category:11世纪中国年号](../Category/11世纪中国年号.md "wikilink")
[Category:1050年代中国政治](../Category/1050年代中国政治.md "wikilink")
[Category:1052年](../Category/1052年.md "wikilink")
[Category:1053年](../Category/1053年.md "wikilink")

1.