**一般財團法人東京大學出版會**（簡稱：東京大學出版會、[英語稱為](../Page/英語.md "wikilink")：University
of Tokyo
Press）為[東京大學出版部](../Page/東京大學.md "wikilink")，屬[財團法人組織](../Page/財團法人.md "wikilink")。東京大學校長擔任會長，主要出版有關於東京大學各式學術活動方面的[書籍刊物](../Page/書籍.md "wikilink")。總部設於[東京都](../Page/東京都.md "wikilink")[文京區](../Page/文京區.md "wikilink")[本鄉東京大學本鄉總校區](../Page/本鄉_\(文京區\).md "wikilink")。

## 概要

  - 1951年3月依東京大學校長[南原繁的提案](../Page/南原繁.md "wikilink")，設立了東京大學出版會，是日本[國立大學最早的大學出版部](../Page/國立大學.md "wikilink")，目的在掲櫫「學問的普及、學術的振興」，主要出版有關以大學的研究成果為基礎的學術書籍、大學內部用的教科書與教材、[東京大學史料編纂所編纂的](../Page/東京大學史料編纂所.md "wikilink")《[大日本史料](../Page/大日本史料.md "wikilink")》等之史料集、一般用途的教養書，每年所刊行的書籍達到數百部以上之多。所出版之東京大學[教養學部](../Page/教養學部.md "wikilink")「[基礎演習](../Page/東京大學大學院總合文化研究科・教養學部#前期課程教育.md "wikilink")」使用的教科書《知の技法》，也成為[暢銷書](../Page/暢銷書.md "wikilink")。
  - 1958年以來設置的「學術書刊行基金」，進一步幫助年輕研究者以其研究成果為基礎來進行書刊出版。
  - 來自學部及[研究所的理事](../Page/研究所.md "wikilink")，也加深與東京大學本體之關係，本會一度成為一獨立組織。
  - 每月發行一回[PR雜誌](../Page/PR雜誌.md "wikilink")《UP》。

## 參考文獻

  -
  -
## 外部連結

  - [東京大學出版會](http://www.utp.or.jp/)

[Category:大學出版社](../Category/大學出版社.md "wikilink")
[Category:日本出版社](../Category/日本出版社.md "wikilink")
[Category:財團法人](../Category/財團法人.md "wikilink")
[Category:東京大學](../Category/東京大學.md "wikilink")
[Category:1951年建立](../Category/1951年建立.md "wikilink")