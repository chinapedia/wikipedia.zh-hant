**王霸**（[878年二月](../Page/878年.md "wikilink")-[880年十一月](../Page/880年.md "wikilink")）是[唐朝末年农民起义首领](../Page/唐朝.md "wikilink")[黄巢所用的](../Page/黄巢.md "wikilink")[年号](../Page/年号.md "wikilink")，共计3年。

## 纪年

  -
    {| border=1 cellspacing=0

|-
style="font-weight:bold;background-color:\#CCCCCC;color:\#000000;text-align:right"
|王霸||元年||二年||三年 |- style="background-color:\#FFFFFF;text-align:center"
|[公元](../Page/公元纪年.md "wikilink")||[878年](../Page/878年.md "wikilink")||[879年](../Page/879年.md "wikilink")||[880年](../Page/880年.md "wikilink")
|- style="background-color:\#FFFFFF;text-align:center"
|[干支](../Page/干支纪年.md "wikilink")||[戊戌](../Page/戊戌.md "wikilink")||[己亥](../Page/己亥.md "wikilink")||[庚子](../Page/庚子.md "wikilink")
|}

## 參看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 同期存在的其他政权年号
      - [乾符](../Page/乾符.md "wikilink")（874年十一月至879年十二月）：[唐僖宗的年号](../Page/唐僖宗.md "wikilink")
      - [廣明](../Page/廣明_\(唐僖宗\).md "wikilink")（880年正月至881年七月）：唐僖宗的年号
      - [元慶](../Page/元慶.md "wikilink")（877年四月十六日至885年二月二十一日）：平安時代陽成天皇、[光孝天皇之年號](../Page/光孝天皇.md "wikilink")
      - [建極](../Page/建極.md "wikilink")（860年起）：[南詔領袖](../Page/南詔.md "wikilink")[世隆之年號](../Page/世隆.md "wikilink")
      - [貞明](../Page/貞明_\(隆舜\).md "wikilink")（878年起）：南詔領袖[隆舜之年號](../Page/隆舜.md "wikilink")
      - [大同](../Page/大同_\(隆舜\).md "wikilink")（至888年）：南詔領袖隆舜之年號

[齐](../Category/唐朝民变政权年号.md "wikilink")
[Category:黄巢之乱](../Category/黄巢之乱.md "wikilink")
[Category:9世纪中国年号](../Category/9世纪中国年号.md "wikilink")
[Category:870年代中国政治](../Category/870年代中国政治.md "wikilink")
[Category:880年代中国政治](../Category/880年代中国政治.md "wikilink")