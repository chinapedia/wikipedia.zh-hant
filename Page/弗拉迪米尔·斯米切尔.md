**華特米亞·史米沙**（**Vladimír
Šmicer**，），[捷克足球運動員](../Page/捷克.md "wikilink")，前[捷克國家足球隊成員](../Page/捷克國家足球隊.md "wikilink")，於2009年11月宣佈退役。

史米沙成名於[法甲](../Page/法甲.md "wikilink")[朗斯](../Page/朗斯足球會.md "wikilink")，1999年被[利物浦主教練](../Page/利物浦足球會.md "wikilink")[侯利亞賞識](../Page/侯利亞.md "wikilink")，代替離隊的[麥馬拿文](../Page/麥馬拿文.md "wikilink")。在利物浦期間由於傷患較多，而且侯利亞的戰術保守，因此令擔任進攻中場的史米沙不能發揮所長，並非利物浦主力球員，多為作左、右中場替工。[2005年歐洲冠軍聯賽決賽中](../Page/2005年歐洲冠軍聯賽決賽.md "wikilink")，史米沙於23分鐘代替受傷的[基維爾上場](../Page/基維爾.md "wikilink")，射入20多碼的遠射，成為利物浦逆轉勝的關鍵，最終助利物浦奪得歐冠盃。該季後史米沙離開利物浦，加盟法甲[波爾多](../Page/波爾多足球會.md "wikilink")。2007年決定回歸母會[布拉格斯拉維亞](../Page/布拉格斯拉維亞.md "wikilink")。

## 榮譽

###  朗斯

  - 1997/98 [法甲](../Page/法甲.md "wikilink")
  - 1998/99 [法國聯賽盃](../Page/法國聯賽盃.md "wikilink")

###  利物浦

  - 2000/01 [英格蘭聯賽盃](../Page/英格蘭聯賽盃.md "wikilink")
  - 2000/01 [英格蘭足總盃](../Page/英格蘭足總盃.md "wikilink")
  - 2000/01 [歐洲足協盃](../Page/歐洲足協盃.md "wikilink")
  - 2001/02 [英格蘭社區盾](../Page/英格蘭社區盾.md "wikilink")
  - 2001/02 [歐洲超級盃](../Page/歐洲超級盃.md "wikilink")
  - 2002/03 [英格蘭聯賽盃](../Page/英格蘭聯賽盃.md "wikilink")
  - 2004/05 [歐洲聯賽冠軍盃](../Page/歐洲聯賽冠軍盃.md "wikilink")

###  波爾多

  - 2006/07 [法國聯賽盃](../Page/法國聯賽盃.md "wikilink")

## 參考資料

  - [利物浦官方網頁
    史米沙資料](https://web.archive.org/web/20080524050745/http://www.liverpoolfc.tv/team/past_players/players/smicer/)
  - [soccerbase
    史米沙數據](https://web.archive.org/web/20080610021311/http://www.soccerbase.com/players_details.sd?playerid=9029)

[Category:捷克足球運動員](../Category/捷克足球運動員.md "wikilink")
[Category:足球中場](../Category/足球中場.md "wikilink")
[Category:法甲球員](../Category/法甲球員.md "wikilink")
[Category:英超球員](../Category/英超球員.md "wikilink")
[Category:朗斯球員](../Category/朗斯球員.md "wikilink")
[Category:利物浦球員](../Category/利物浦球員.md "wikilink")
[Category:波爾多球員](../Category/波爾多球員.md "wikilink")