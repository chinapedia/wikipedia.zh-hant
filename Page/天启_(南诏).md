**天启**（840年-859年）是[南诏](../Page/南诏.md "wikilink")[勸豐祐的](../Page/勸豐祐.md "wikilink")[年号](../Page/年号.md "wikilink")，共计20年。阮元聲《南詔野史》修訂本記載「太和五年（832年），王改元天啟」有誤，當是「開成五年（840年），王改元天啟」。\[1\]

## 纪年

| 天启                               | 元年                             | 二年                             | 三年                             | 四年                             | 五年                             | 六年                             | 七年                             | 八年                             | 九年                             | 十年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 840年                           | 841年                           | 842年                           | 843年                           | 844年                           | 845年                           | 846年                           | 847年                           | 848年                           | 849年                           |
| [干支](../Page/干支纪年.md "wikilink") | [庚申](../Page/庚申.md "wikilink") | [辛酉](../Page/辛酉.md "wikilink") | [壬戌](../Page/壬戌.md "wikilink") | [癸亥](../Page/癸亥.md "wikilink") | [甲子](../Page/甲子.md "wikilink") | [乙丑](../Page/乙丑.md "wikilink") | [丙寅](../Page/丙寅.md "wikilink") | [丁卯](../Page/丁卯.md "wikilink") | [戊辰](../Page/戊辰.md "wikilink") | [己巳](../Page/己巳.md "wikilink") |
| 天启                               | 十一年                            | 十二年                            | 十三年                            | 十四年                            | 十五年                            | 十六年                            | 十七年                            | 十八年                            | 十九年                            | 二十年                            |
| [公元](../Page/公元纪年.md "wikilink") | 850年                           | 851年                           | 852年                           | 853年                           | 854年                           | 855年                           | 856年                           | 857年                           | 858年                           | 859年                           |
| [干支](../Page/干支纪年.md "wikilink") | [庚午](../Page/庚午.md "wikilink") | [辛未](../Page/辛未.md "wikilink") | [壬申](../Page/壬申.md "wikilink") | [癸酉](../Page/癸酉.md "wikilink") | [甲戌](../Page/甲戌.md "wikilink") | [乙亥](../Page/乙亥.md "wikilink") | [丙子](../Page/丙子.md "wikilink") | [丁丑](../Page/丁丑.md "wikilink") | [戊寅](../Page/戊寅.md "wikilink") | [己卯](../Page/己卯.md "wikilink") |

## 参见

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 其他时期使用的[天启年号](../Page/天启.md "wikilink")
  - 同期存在的其他政权年号
      - [開成](../Page/開成.md "wikilink")（836年正月—840年十二月）：唐文宗的年號
      - [会昌](../Page/会昌_\(唐武宗\).md "wikilink")（841年正月—846年十二月）：[唐武宗李炎的年号](../Page/唐武宗.md "wikilink")
      - [大中](../Page/大中_\(唐宣宗\).md "wikilink")（847年正月—860年十月）：[唐宣宗的年號](../Page/唐宣宗.md "wikilink")
      - [承和](../Page/承和_\(仁明天皇\).md "wikilink")（834年正月三日至848年六月十三日）：平安時代[仁明天皇之年號](../Page/仁明天皇.md "wikilink")
      - [嘉祥](../Page/嘉祥_\(仁明天皇\).md "wikilink")（848年六月十三日至851年四月二十八日）：平安時代[仁明天皇](../Page/仁明天皇.md "wikilink")、[文德天皇之年號](../Page/文德天皇.md "wikilink")
      - [仁壽](../Page/仁壽_\(文德天皇\).md "wikilink")（851年四月二十八日至854年十一月三十日）：平安時代[仁明天皇](../Page/仁明天皇.md "wikilink")、[文德天皇之年號](../Page/文德天皇.md "wikilink")
      - [齊衡](../Page/齊衡.md "wikilink")（854年十一月三十日至857年二月二十一日）：平安時代[文德天皇之年號](../Page/文德天皇.md "wikilink")
      - [天安](../Page/天安_\(文德天皇\).md "wikilink")（857年二月二十一日至859年四月十五日）：平安時代[文德天皇](../Page/文德天皇.md "wikilink")、[清和天皇之年號](../Page/清和天皇.md "wikilink")
      - [貞觀](../Page/貞觀_\(清和天皇\).md "wikilink")（859年四月十五日至877年四月十六日）：平安時代[清和天皇](../Page/清和天皇.md "wikilink")、[陽成天皇之年號](../Page/陽成天皇.md "wikilink")
      - [咸和](../Page/咸和_\(大彝震\).md "wikilink")（831年至857年）：渤海宣王[大彝震之年號](../Page/大彝震.md "wikilink")

## 注释

[Category:南诏年号](../Category/南诏年号.md "wikilink")
[Category:9世纪中国年号](../Category/9世纪中国年号.md "wikilink")
[Category:840年代中国政治](../Category/840年代中国政治.md "wikilink")
[Category:850年代中国政治](../Category/850年代中国政治.md "wikilink")

1.  张增祺. 关于南诏、大理国纪年资料的订正\[J\]. 考古, 1983(1):68-69.