本表尽量彻底地记录世界各国的航天工程中，有史以来以探测[月球为目的而发射出的探测器的简略概况](../Page/月球.md "wikilink")。

## 月球探测任务列表

<table>
<thead>
<tr class="header">
<th><p>月球探测任务列表</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>colspan="7" align:left id="顶"|* <strong><a href="../Page/#1.md" title="wikilink">第一轮探月活动</a></strong>：<a href="../Page/#1950年代.md" title="wikilink">1950年代</a> · <a href="../Page/#1960年代.md" title="wikilink">1960年代</a> · <a href="../Page/#1970年代.md" title="wikilink">1970年代</a><br />
* <strong><a href="../Page/#2.md" title="wikilink">第二轮探月活动</a></strong>：<a href="../Page/#1980年代.md" title="wikilink">1980年代</a> · <a href="../Page/#1990年代.md" title="wikilink">1990年代</a> · <a href="../Page/#2000年代.md" title="wikilink">2000年代</a> · <a href="../Page/#2010年代.md" title="wikilink">2010年代</a> · <a href="../Page/#未来任务.md" title="wikilink">未来任务</a> · <strong><a href="../Page/#3.md" title="wikilink">长远计划</a></strong> __NOTOC__</p></td>
</tr>
<tr class="even">
<td><p>序号</p></td>
</tr>
<tr class="odd">
<td><p>第一轮探月活动 <a href="../Page/#顶.md" title="wikilink">↑</a></p></td>
</tr>
<tr class="even">
<td><p>1950年代：<a href="../Page/#58.md" title="wikilink">1958年</a>－<a href="../Page/#59.md" title="wikilink">1959年</a> <a href="../Page/#顶.md" title="wikilink">↑</a></p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>1.</p></td>
</tr>
<tr class="odd">
<td><p>2.</p></td>
</tr>
<tr class="even">
<td><p>3.</p></td>
</tr>
<tr class="odd">
<td><p>4.</p></td>
</tr>
<tr class="even">
<td><p>5.</p></td>
</tr>
<tr class="odd">
<td><p>6.</p></td>
</tr>
<tr class="even">
<td><p>7.</p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>8.</p></td>
</tr>
<tr class="odd">
<td><p>9.</p></td>
</tr>
<tr class="even">
<td><p>10.</p></td>
</tr>
<tr class="odd">
<td><p>11.</p></td>
</tr>
<tr class="even">
<td><p>–</p></td>
</tr>
<tr class="odd">
<td><p>12.</p></td>
</tr>
<tr class="even">
<td><p>13.</p></td>
</tr>
<tr class="odd">
<td><p>1960年代：<a href="../Page/#60.md" title="wikilink">1960年</a>－<a href="../Page/#61.md" title="wikilink">1961年</a>－<a href="../Page/#62.md" title="wikilink">1962年</a>－<a href="../Page/#63.md" title="wikilink">1963年</a>－<a href="../Page/#64.md" title="wikilink">1964年</a>－<a href="../Page/#65.md" title="wikilink">1965年</a>－<a href="../Page/#66.md" title="wikilink">1966年</a>－<a href="../Page/#67.md" title="wikilink">1967年</a>－<a href="../Page/#68.md" title="wikilink">1968年</a>－<a href="../Page/#69.md" title="wikilink">1969年</a> <a href="../Page/#顶.md" title="wikilink">↑</a></p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td><p>14.</p></td>
</tr>
<tr class="even">
<td><p>15.</p></td>
</tr>
<tr class="odd">
<td><p>16.</p></td>
</tr>
<tr class="even">
<td><p>17.</p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>18.</p></td>
</tr>
<tr class="odd">
<td><p>19.</p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td><p>20.</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/游騎兵3号#月球硬着陆器.md" title="wikilink">月球硬着陆器</a></p></td>
</tr>
<tr class="odd">
<td><p>21.</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/游騎兵4号#月球硬着陆器.md" title="wikilink">月球硬着陆器</a></p></td>
</tr>
<tr class="odd">
<td><p>22.</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/游騎兵5号#月球硬着陆器.md" title="wikilink">月球硬着陆器</a></p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>23.</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/月球4C号#月球软着陆器.md" title="wikilink">月球软着陆器</a></p></td>
</tr>
<tr class="even">
<td><p>24.</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/月球4D号#月球软着陆器.md" title="wikilink">月球软着陆器</a></p></td>
</tr>
<tr class="even">
<td><p>25.</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/月球4号#月球软着陆器.md" title="wikilink">月球软着陆器</a></p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td><p>26.</p></td>
</tr>
<tr class="even">
<td><p>27.</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/月球5A号#月球软着陆器.md" title="wikilink">月球软着陆器</a></p></td>
</tr>
<tr class="even">
<td><p>28.</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/月球5B号#月球软着陆器.md" title="wikilink">月球软着陆器</a></p></td>
</tr>
<tr class="even">
<td><p>29.</p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>30.</p></td>
</tr>
<tr class="odd">
<td><p>31.</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/宇宙60号#月球软着陆器.md" title="wikilink">月球软着陆器</a></p></td>
</tr>
<tr class="odd">
<td><p>32.</p></td>
</tr>
<tr class="even">
<td><p>33.</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/月球5C号#月球软着陆器.md" title="wikilink">月球软着陆器</a></p></td>
</tr>
<tr class="even">
<td><p>34.</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/月球5号#月球软着陆器.md" title="wikilink">月球软着陆器</a></p></td>
</tr>
<tr class="even">
<td><p>35.</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/月球6号#月球软着陆器.md" title="wikilink">月球软着陆器</a></p></td>
</tr>
<tr class="even">
<td><p>36.</p></td>
</tr>
<tr class="odd">
<td><p>37.</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/月球7号#月球软着陆器.md" title="wikilink">月球软着陆器</a></p></td>
</tr>
<tr class="odd">
<td><p>38.</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/月球8号#月球软着陆器.md" title="wikilink">月球软着陆器</a></p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>39.</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/月球9号#月球软着陆器.md" title="wikilink">月球软着陆器</a></p></td>
</tr>
<tr class="even">
<td><p>40.</p></td>
</tr>
<tr class="odd">
<td><p>41.</p></td>
</tr>
<tr class="even">
<td><p>42.</p></td>
</tr>
<tr class="odd">
<td><p>43.</p></td>
</tr>
<tr class="even">
<td><p>44.</p></td>
</tr>
<tr class="odd">
<td><p>45.</p></td>
</tr>
<tr class="even">
<td><p>46.</p></td>
</tr>
<tr class="odd">
<td><p>47.</p></td>
</tr>
<tr class="even">
<td><p>48.</p></td>
</tr>
<tr class="odd">
<td><p>49.</p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td><p>50.</p></td>
</tr>
<tr class="even">
<td><p>51.</p></td>
</tr>
<tr class="odd">
<td><p>52.</p></td>
</tr>
<tr class="even">
<td><p>53.</p></td>
</tr>
<tr class="odd">
<td><p>54.</p></td>
</tr>
<tr class="even">
<td><p>55.</p></td>
</tr>
<tr class="odd">
<td><p>56.</p></td>
</tr>
<tr class="even">
<td><p>57.</p></td>
</tr>
<tr class="odd">
<td><p>58.</p></td>
</tr>
<tr class="even">
<td><p>59.</p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>60.</p></td>
</tr>
<tr class="odd">
<td><p>61.</p></td>
</tr>
<tr class="even">
<td><p>62.</p></td>
</tr>
<tr class="odd">
<td><p>63.</p></td>
</tr>
<tr class="even">
<td><p>64.</p></td>
</tr>
<tr class="odd">
<td><p>65.</p></td>
</tr>
<tr class="even">
<td><p>66.</p></td>
</tr>
<tr class="odd">
<td><p>67.</p></td>
</tr>
<tr class="even">
<td><p>68.</p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>69.</p></td>
</tr>
<tr class="odd">
<td><p>70.</p></td>
</tr>
<tr class="even">
<td><p>71.</p></td>
</tr>
<tr class="odd">
<td><p>72.</p></td>
</tr>
<tr class="even">
<td><p>73.</p></td>
</tr>
<tr class="odd">
<td><p>74.</p></td>
</tr>
<tr class="even">
<td><p>75.</p></td>
</tr>
<tr class="odd">
<td><p>76.</p></td>
</tr>
<tr class="even">
<td><p>77.</p></td>
</tr>
<tr class="odd">
<td><p>78.</p></td>
</tr>
<tr class="even">
<td><p>79.</p></td>
</tr>
<tr class="odd">
<td><p>80.</p></td>
</tr>
<tr class="even">
<td><p>1970年代：<a href="../Page/#70.md" title="wikilink">1970年</a>－<a href="../Page/#71.md" title="wikilink">1971年</a>－<a href="../Page/#72.md" title="wikilink">1972年</a>－<a href="../Page/#73.md" title="wikilink">1973年</a>－<a href="../Page/#74.md" title="wikilink">1974年</a>－<a href="../Page/#75.md" title="wikilink">1975年</a>－<a href="../Page/#76.md" title="wikilink">1976年</a> <a href="../Page/#顶.md" title="wikilink">↑</a></p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>81.</p></td>
</tr>
<tr class="odd">
<td><p>82.</p></td>
</tr>
<tr class="even">
<td><p>83.</p></td>
</tr>
<tr class="odd">
<td><p>84.</p></td>
</tr>
<tr class="even">
<td><p>85.</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/月球车1号.md" title="wikilink">月球车1号</a></p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td><p>86.</p></td>
</tr>
<tr class="even">
<td><p>87.</p></td>
</tr>
<tr class="odd">
<td><p>88.</p></td>
</tr>
<tr class="even">
<td><p>89.</p></td>
</tr>
<tr class="odd">
<td><p>90.</p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td><p>91.</p></td>
</tr>
<tr class="even">
<td><p>92.</p></td>
</tr>
<tr class="odd">
<td><p>93.</p></td>
</tr>
<tr class="even">
<td><p>94.</p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>95.</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/月球车2号.md" title="wikilink">月球车2号</a></p></td>
</tr>
<tr class="even">
<td><p>96.</p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>97.</p></td>
</tr>
<tr class="odd">
<td><p>98.</p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td><p>99.</p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td><p>100.</p></td>
</tr>
<tr class="even">
<td><p>第二轮探月活动 <a href="../Page/#顶.md" title="wikilink">↑</a></p></td>
</tr>
<tr class="odd">
<td><p>1980年代：<a href="../Page/#89.md" title="wikilink">1989年</a> <a href="../Page/#顶.md" title="wikilink">↑</a></p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td><p>101.</p></td>
</tr>
<tr class="even">
<td><p>1990年代：<a href="../Page/#90.md" title="wikilink">1990年</a>－<a href="../Page/#94.md" title="wikilink">1994年</a>－<a href="../Page/#97.md" title="wikilink">1997年</a>－<a href="../Page/#98.md" title="wikilink">1998年</a> <a href="../Page/#顶.md" title="wikilink">↑</a></p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>102.</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/飞天号.md" title="wikilink">羽衣</a></p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td><p>103.</p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td><p>104.</p></td>
</tr>
<tr class="even">
<td><p>105.</p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>106.</p></td>
</tr>
<tr class="odd">
<td><p>2000年代：<a href="../Page/#03.md" title="wikilink">2003年</a>－<a href="../Page/#07.md" title="wikilink">2007年</a>－<a href="../Page/#08.md" title="wikilink">2008年</a>－<a href="../Page/#09.md" title="wikilink">2009年</a> <a href="../Page/#顶.md" title="wikilink">↑</a></p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td><p>107.</p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td><p>108.</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/月亮女神_(航天器)#中继星(翁).md" title="wikilink">翁</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/月亮女神_(航天器)#甚长基线干涉测量星(嫗).md" title="wikilink">妪</a></p></td>
</tr>
<tr class="even">
<td><p>109.</p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>110.</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/月船1号.md" title="wikilink">月球撞击探测器</a></p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>111.</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/月球坑观测和传感卫星.md" title="wikilink">月球坑观测和传感卫星</a></p></td>
</tr>
<tr class="even">
<td><p>2010年代：<a href="../Page/#10.md" title="wikilink">2010年</a> －<a href="../Page/#11.md" title="wikilink">2011年</a> －<a href="../Page/#12.md" title="wikilink">2012年</a> －<a href="../Page/#13.md" title="wikilink">2013年</a> －<a href="../Page/#14.md" title="wikilink">2014年</a> －<a href="../Page/#18.md" title="wikilink">2018年</a>－<a href="../Page/#已發射19.md" title="wikilink">2019年</a> <a href="../Page/#顶.md" title="wikilink">↑</a></p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>112.</p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>113.</p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>114.</p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>115.</p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>116.</p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>117.</p></td>
</tr>
<tr class="odd">
<td><p>月球车，<a href="../Page/嫦娥三号.md" title="wikilink">嫦娥三号备份星</a></p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td><p>118.</p></td>
</tr>
<tr class="even">
<td><p>未来任务：<a href="../Page/#未發射19.md" title="wikilink">2019年</a>－<a href="../Page/#20.md" title="wikilink">2020年</a>－<a href="../Page/#21.md" title="wikilink">2021年</a>－<a href="../Page/#24.md" title="wikilink">2024年</a>－<a href="../Page/#25.md" title="wikilink">2025年</a> <a href="../Page/#顶.md" title="wikilink">↑</a></p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>–</p></td>
</tr>
<tr class="odd">
<td><p>–</p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>–</p></td>
</tr>
<tr class="odd">
<td><p>–</p></td>
</tr>
<tr class="even">
<td><p>–</p></td>
</tr>
<tr class="odd">
<td><p>–</p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td><p>–</p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>–</p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>–</p></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>–</p></td>
</tr>
<tr class="odd">
<td><p>–</p></td>
</tr>
<tr class="even">
<td><p>长远计划 <a href="../Page/#顶.md" title="wikilink">↑</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/#美.md" title="wikilink">美国</a> – <a href="../Page/#中.md" title="wikilink">中国</a>– <a href="../Page/#日.md" title="wikilink">日本</a> – <a href="../Page/#印.md" title="wikilink">印度</a> – <a href="../Page/#欧.md" title="wikilink">欧洲</a> – <a href="../Page/#俄.md" title="wikilink">俄罗斯</a> <a href="../Page/#顶.md" title="wikilink">↑</a></p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td><p>–</p></td>
</tr>
<tr class="odd">
<td><p>–</p></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
<tr class="odd">
<td></td>
</tr>
<tr class="even">
<td></td>
</tr>
</tbody>
</table>

## 参阅

  - [月球](../Page/月球.md "wikilink")
  - [月球探测](../Page/月球探测.md "wikilink")
  - [月球殖民](../Page/月球殖民.md "wikilink")
  - [月球人造物体列表](../Page/月球人造物体列表.md "wikilink")
  - [火星探测任务列表](../Page/火星探测任务列表.md "wikilink")
  - [太陽系探測器列表](../Page/太陽系探測器列表.md "wikilink")

## 注释

## 参考资料

<references group="參" />

{{-}}

[Category:月球航天器](../Category/月球航天器.md "wikilink")
[Category:科技相關列表](../Category/科技相關列表.md "wikilink")