**萨沃纳**（[意大利语](../Page/意大利语.md "wikilink")：****）位于[意大利](../Page/意大利.md "wikilink")[利古里亚大区](../Page/利古里亚大区.md "wikilink")[热那亚湾畔](../Page/热那亚湾.md "wikilink")，是[萨沃纳省的首府](../Page/萨沃纳省.md "wikilink")。面积65.55 km²
(25.3 sq mi)，人口约6.2万（2004年）。

## 历史

古代，这里是[利古里亚人的居住地](../Page/利古里亚人.md "wikilink")。[西罗马帝国陷落后](../Page/西罗马帝国.md "wikilink")，经过东哥特王国与拜占庭帝国短暂的统治后，641年，该地被[伦巴底人统治](../Page/伦巴底人.md "wikilink")。

[文藝復興以後](../Page/文藝復興.md "wikilink")，薩沃納為[熱那亞共和國一部分](../Page/熱那亞共和國.md "wikilink")。至1815年，又被落入[薩丁尼亞王國之手](../Page/薩丁尼亞王國.md "wikilink")，一直至十九世紀末意大利統一為止。

## 經濟

以港口經濟為主，是繼[熱那亞後](../Page/熱那亞.md "wikilink")，利古里亞大區第二大港口。

## 交通

### 道路

  - [A10高速公路](../Page/A10高速公路_\(義大利\).md "wikilink")

## 著名人物

  - [克里斯蒂安·帕努奇](../Page/克里斯蒂安·帕努奇.md "wikilink")，足球运动员。
  - [尤利乌斯二世](../Page/尤利乌斯二世.md "wikilink")
  - [西克斯特四世](../Page/西克斯特四世.md "wikilink")

## 參考

[S](../Category/萨沃纳省市镇.md "wikilink")