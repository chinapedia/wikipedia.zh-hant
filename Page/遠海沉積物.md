**遠海沉積物**（Pelagic sediments 或 marine
sediments），為那些在[深海平原累積的](../Page/深海平原.md "wikilink")[沉積物](../Page/沉積物.md "wikilink")，遠離[陸源沉積物的陸地來源](../Page/陸源.md "wikilink")。陸源沉積物主要由[大陸棚所限制](../Page/大陸棚.md "wikilink")，並由河流所沉積\[1\]。遠海沉積物與陸源沉積物混合在一起稱為**半遠洋性沉積物**（hemipelagic
sediment）。

## 主要類別

遠海沉積物主要分為三大類：\[2\]

### 矽藻軟泥

矽藻[軟泥](../Page/軟泥.md "wikilink")（Siliceous
oozes）由含有[二氧化矽的浮游生物的殘骸組成](../Page/二氧化矽.md "wikilink")，如[矽藻類](../Page/矽藻.md "wikilink")（diatom）及[放射蟲類](../Page/放射蟲.md "wikilink")（radiolaria）。這些軟泥只可以在高生物生產力的地區找到，如兩極海洋及近[赤道的](../Page/赤道.md "wikilink")[上升流](../Page/上升流.md "wikilink")（upwelling）地區。此為最少有的遠海沉積物，只佔海床15%的地方。她比鈣質軟泥累積更慢，為每1000年0.2-1厘米\[3\]。

### 鈣質軟泥

鈣質軟泥（Calcareous
oozes）由貝殼組成，以及由被稱為[有孔蟲類](../Page/有孔蟲類.md "wikilink")（foraminifera）、[顆石藻](../Page/顆石藻.md "wikilink")（coccolithophore）、[翼足類](../Page/翼足類.md "wikilink")（pteropod）等殼體共同組成。為最普遍的遠海沉積物，佔海床48%的地方。這類軟泥受[碳酸鹽補償深度](../Page/碳酸鹽補償深度.md "wikilink")（carbonate
compensation
depth）的限制，只可在埋葬時高於此深度才能形成。她比其他遠海沉積物累積得更快，為每1000年0.3-5厘米\[4\]。

### 紅土

紅土（Red clays），亦稱遠海土（pelagic
clay），在海洋最深及最遠處累積，含有少於30%生物物質，其成分為不同份量的幼細[石英及](../Page/石英.md "wikilink")[黏土礦物質](../Page/黏土.md "wikilink")，自生的（authigenic）沉積物由海水及[微流星體累積而成](../Page/微流星體.md "wikilink")。雖然稱為紅土，但其實亦間中含有[氧化](../Page/氧化.md "wikilink")[鐵礦石](../Page/鐵.md "wikilink")，發出[棕色顏色](../Page/棕色.md "wikilink")。來源不明，但似乎是由遠方河流及風吹塵埃而來\[5\]；佔海床38%地方，並為累積速度最慢的遠海沉積物，為每1000年0.1-0.5厘米\[6\]。

## 軟泥與紅土的劃分

[軟泥](../Page/軟泥.md "wikilink")（Ooze）並不是指其沉積物的硬度，而是指其來源。軟泥主要為生物所造成的（biogenic），由[浮游生物界的殘骸組成](../Page/浮游生物界.md "wikilink")。[紅土為非生物所造成的](../Page/紅土.md "wikilink")（non-biogenic），因為其含有少數有機物質\[7\]。更精細劃分的話，任何沉積物含有30%以上的微小骨骼殘骸被稱為軟泥。\[8\]。無論她們從何而來，所有遠海沉積物的累積速度均是十分緩慢，為小於每一千年幾厘米\[9\]。

## 不同類別的成因

在固定地點的沉積物累積類別由其與陸地距離、水深、總體肥沃程度決定\[10\]。例如當壓力令海水[二氧化碳的](../Page/二氧化碳.md "wikilink")[溶解性提高時](../Page/溶解性.md "wikilink")，[水柱](../Page/水柱.md "wikilink")（water
column）便會在較深的地方有較高的腐蝕性。在碳酸鹽補償深度下約4.5公里時，[碳酸鹽](../Page/碳酸鹽.md "wikilink")（carbonate）的溶解速度便與沉積速度等同\[11\]。

## 外部連結

  - [Overview of marine
    sediments](https://web.archive.org/web/20080103023043/http://www.articlemyriad.com/23.htm)
  - [1](https://web.archive.org/web/20071018023102/http://www.odp.usyd.edu.au/odp_CD/oceplat/opindex.html#top)

## 參見

  - [海洋學](../Page/海洋學.md "wikilink")
  - [物理海洋學](../Page/物理海洋學.md "wikilink")
  - [陸源](../Page/陸源.md "wikilink")
  - [半遠洋性沉積物](../Page/半遠洋性沉積物.md "wikilink")

## 註腳

## 參考資料

  - Pinet, Paul R. *Invitation to Oceanography* St. Paul: West
    Publishing Company, 1996. ISBN 0-314-06339-0
  - Rothwell, R. G., "Deep Ocean Pelagic Oozes", Vol. 5. of Selley,
    Richard C., L. Robin McCocks, and Ian R. Plimer, *Encyclopedia of
    Geology*, Oxford: Elsevier Limited, 2005. ISBN 0-12-636380-3

[Y](../Category/地質學.md "wikilink") [Y](../Category/海洋學.md "wikilink")
[Category:沉积物](../Category/沉积物.md "wikilink")

1.  Pinet 83, Rothwell 70.
2.  Rothwell 70.
3.  Rothwell 75, 77.
4.  Rothwell 74-5, 77.
5.  Pinet 99-100, Rothwell 76.
6.  Rothwell 75, 77.
7.  Pinet 99.
8.  Pinet 101.
9.  Rothwell 77.
10. Rothwell 73.
11. Rothwell 73.