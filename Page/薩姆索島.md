[DenmarkSamsø.png](https://zh.wikipedia.org/wiki/File:DenmarkSamsø.png "fig:DenmarkSamsø.png")

**萨姆索岛**()是[丹麥的一個](../Page/丹麥.md "wikilink")[島](../Page/島.md "wikilink")。

遊客常在6、7月時來採摘[草莓](../Page/草莓.md "wikilink")。在丹麥，這裏以早熟的[馬鈴薯馳名](../Page/馬鈴薯.md "wikilink")。

## 地理

它在[北海](../Page/北海_\(马来西亚\).md "wikilink")[卡特加特海峽](../Page/卡特加特海峽.md "wikilink")，離[日德蘭半島](../Page/日德蘭半島.md "wikilink")15公里。

島東的海峽稱為珊索海峽()，它將萨姆索岛和西蘭島分開了。最接近的縣是西面，在日德蘭半島上的[奧德縣](../Page/奧德縣.md "wikilink")()。

萨姆索岛面積為112平方公里，約有四千名居民。

## 能源

1997年，萨姆索岛在一個比賽獲勝了。她要開始一個計劃：在十年之內，全面以[可再生能源發電](../Page/可再生能源.md "wikilink")。1998年，島上只有8%的[電力和](../Page/電力.md "wikilink")15%的熱能來自可再生能源。2000年，借助11個新[風車](../Page/風車.md "wikilink")，島的電力已能自給自足。全島近二十戶人家中，有440人佔有風車的股份。[1](http://ide.idebanken.no/bibliotek_engelsk/ProsjektID.asp?ProsjektID=189)

萨姆索岛的能源為以木、太陽能和稻草為基礎的城市供熱系統、風車和太陽能。

實踐這個全面使用可再生能源的最大的困難來自交通。2003年，島上只有三輛[電力車](../Page/電力車.md "wikilink")。

有些人已經將萨姆索岛視作未來能源供求上的「[示範單位](../Page/示範單位.md "wikilink")」。

## [北歐神話](../Page/北歐神話.md "wikilink")

[Maarup_Østerstrand_mod_nord.jpg](https://zh.wikipedia.org/wiki/File:Maarup_Østerstrand_mod_nord.jpg "fig:Maarup_Østerstrand_mod_nord.jpg")

[薩克索·格拉瑪提庫斯指](../Page/薩克索·格拉瑪提庫斯.md "wikilink")，傳說中瑞典的英雄和他的朋友曾在這個島上對抗狂戰士的12個兒子。這場戰爭記錄了在、[薩迦和](../Page/薩迦_\(文學\).md "wikilink")薩迦內。

## 行政

萨姆索岛屬[中日德兰大区的](../Page/中日德兰大区.md "wikilink")**[萨姆索自治市](../Page/萨姆索自治市.md "wikilink")**。這個縣的範圍除了萨姆索岛，還有位於萨姆索岛東北面的小島，整體面積為114平方公里。縣會議在舉行。縣長是，屬[自由黨](../Page/自由黨_\(丹麥\).md "wikilink")。

## 外部連結

  - [萨姆索岛官方網站](http://www.samsoe.dk/)
  - [萨姆索岛能源公司](http://www.veo.dk)
  - [新能源天堂
    丹麥萨姆索岛，曹欽榮](https://web.archive.org/web/20070928000812/http://www.newtaiwan.com.tw/bulletinview.jsp?period=388&bulletinid=12526)

[S](../Category/丹麥岛屿.md "wikilink")