**Launchpad**（发射台）是[Canonical有限公司所架设的](../Page/Canonical有限公司.md "wikilink")[网站](../Page/网站.md "wikilink")，是一个提供维护、支持或联络[Ubuntu开发者的平台](../Page/Ubuntu.md "wikilink")。其中Launchpad提供了在线[编译软件的功能](../Page/编译.md "wikilink")，使用者可以自由的参与Ubuntu或相关[自由软件的开发或编译工作](../Page/自由软件.md "wikilink")。而使用者也可以利用该网站的汇报机制来汇报相关软件的[Bug](../Page/Bug.md "wikilink")，或者进一步提供建议。

## 參見

  - [Transifex](../Page/Transifex.md "wikilink")

## 參考資料

## 相關連結

  - [Ubuntu正體中文Wiki -
    Launchpad](http://wiki.ubuntu-tw.org/index.php?title=Launchpad)→詳細的使用說明及教學

[Category:軟體在地化工具](../Category/軟體在地化工具.md "wikilink")
[Category:專案管理軟體](../Category/專案管理軟體.md "wikilink")