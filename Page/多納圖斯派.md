**多納圖斯派**（**Donatism**），又稱**多納徒派**、**多納徒主義**，是指由[柏柏爾人](../Page/柏柏爾人.md "wikilink")[基督徒](../Page/基督徒.md "wikilink")（Donatus
Magnus）與其追隨者所建立的教派。該教派被正統的天主教會視為是[異端](../Page/異端.md "wikilink")。該教派活動於[羅馬帝國](../Page/羅馬帝國.md "wikilink")[阿非利加行省](../Page/阿非利加行省.md "wikilink")，於4世紀與5世紀最為興盛。

330年時該教派已有172名主教，最多時擁有主教約300人。348年，[羅馬帝國派軍隊鎮壓](../Page/羅馬帝國.md "wikilink")，雙方在[北非對峙達](../Page/北非.md "wikilink")13年。

## 歷史背景

據說此派別的問世，始於在[迦太基一位虔誠且富有的婦人](../Page/迦太基.md "wikilink")，在[聖餐儀式中親吻了一位](../Page/聖餐.md "wikilink")[殉道者的遺物](../Page/殉道.md "wikilink")。而受到其副主教的責難，此女士因此憤而離去。這位女士在努米底亞主教團的支持下，受任其大管家為另個迦太基的主教。這問題不單單是個人的恩怨，其原因已經欲釀許久。這樣的糾葛與三世紀中葉的[諾窪天派的興起相似](../Page/諾窪天派.md "wikilink")。\[1\]

在[戴克理先作](../Page/戴克理先.md "wikilink")[羅馬皇帝時](../Page/羅馬皇帝.md "wikilink")，極力逼迫[基督徒](../Page/基督徒.md "wikilink")。當時[基督徒若要保命則需交出](../Page/基督徒.md "wikilink")[聖經並宣示效忠](../Page/聖經.md "wikilink")[羅馬皇帝](../Page/羅馬皇帝.md "wikilink")，當時有些神職人員為保護會眾故屈服威權並交出[聖經](../Page/聖經.md "wikilink")，以免除信徒被[處死](../Page/處死.md "wikilink")。但當[君士坦丁對於](../Page/君士坦丁一世_\(羅馬帝國\).md "wikilink")[基督徒停止逼迫後](../Page/基督徒.md "wikilink")，信徒們開始想回到正統教會之中參與教會崇拜，但是在[北非有一群以](../Page/北非.md "wikilink")[馬約里努斯](../Page/馬約里努斯.md "wikilink")（Majorinus）（後來由[多納圖斯·馬格努斯繼承其位](../Page/多納圖斯·馬格努斯.md "wikilink")，故以「多納圖斯」為名）為首的激烈派信徒拒絕接納這些他們視為叛徒的「[叛教者](../Page/叛教.md "wikilink")」再次進入教會裡，甚至堅持取消曾經「以經換命」者的牧會職務及地位。更主張這些人行的洗禮及按牧禮都當視為無效。這樣的反對勢力在教會中擴張，當教會主教並未依此標準來對待所謂的「叛教者」時，多納圖斯信徒則另立一主教與其抗衡。

當時[君士坦丁為要以基督教會穩定國家團結力量的源頭](../Page/君士坦丁一世_\(羅馬帝國\).md "wikilink")，卻面臨這教會內部的紛爭與混亂。所以[君士坦丁接納](../Page/君士坦丁一世_\(羅馬帝國\).md "wikilink")[羅馬主教的意見](../Page/羅馬主教.md "wikilink")，在314年（Council
of
Arles）中正式反對多納圖斯派。但激烈的多納圖斯派拒絕服從，並群起抗議上訴[君士坦丁](../Page/君士坦丁一世_\(羅馬帝國\).md "wikilink")，在其統治期間勉強容忍他們的存在。一百年後，多納圖斯派的人數漸漸增加甚至多過正統派信徒，其間[奧古斯丁也公開反對之](../Page/奧古斯丁.md "wikilink")，這樣與正統教派的抗爭持續到7世紀、8世紀之久，直到[撒拉森人將其推翻](../Page/撒拉森人.md "wikilink")。\[2\]

## 參考文獻

## 延伸閱讀

  - *The Donatist Church: A Movement of Protest in Roman North Africa*,
    [W. H. C.
    Frend](../Page/:en:William_Hugh_Clifford_Frend.md "wikilink")
    (Oxford University Press, 1952) ISBN 0-19-826408-9.
  - *The Bible in Christian North Africa: The Donatist World*, Maureen
    A. Tilley (Fortress Press, 1997) ISBN 0-8006-2880-2.
  - *Donatist martyr stories: the Church in conflict in Roman North
    Africa*. Translated with notes and introduction by Maureen A. Tilley
    (Liverpool University Press, 1996) ISBN 0-85323-931-2.
  - *This Holy Seed: Faith, Hope and Love in the Early Churches of North
    Africa*, Robin Daniel (Harpenden: Tamarisk Publications, 1993) ISBN
    0-9520435-0-5.

## 外部連結

  - [Donatus & the Donatist
    Schism](https://web.archive.org/web/20070927023742/http://www.earlychurch.org.uk/donatism.html)
    - 多納圖斯派的主要與次要資料來源

  - [多納圖斯派](http://www.newadvent.org/cathen/05121a.htm)在[天主教百科全書](../Page/天主教百科全書.md "wikilink")，有許多在早期教會政治的細節，提供了多納圖斯派興起與衰落的背景

  - [Letter of Petilian the
    Donatist](http://www.seanmultimedia.com/Pie_Petilian_Encyclical_Letter.html)
    -多納圖斯派領導人的真實書信。這是少數可以得知多納圖教派事務的主要資料來源之一。

[Category:基督教神学](../Category/基督教神学.md "wikilink")
[Category:基督教历史](../Category/基督教历史.md "wikilink")
[Category:基督教的分裂](../Category/基督教的分裂.md "wikilink")

1.  John McMannners（牛津基督教史），張景龍等譯（貴州：牛津大學出版社，1996)，45。
2.  比爾·奧斯丁，《基督教發展史》，馬傑偉、許建人（香港：種子出版社有限公司，2002），96。