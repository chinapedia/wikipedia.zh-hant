**艾馬拉語**（），或譯**艾瑪拉語**，是一種由住在[安地斯山脈的](../Page/安地斯山脈.md "wikilink")[艾馬拉人的語言](../Page/艾馬拉人.md "wikilink")，為[玻利維亞的](../Page/玻利維亞.md "wikilink")[官方語言之一](../Page/官方語言.md "wikilink")。艾馬拉語和[克丘亞語有許多相同的字彙](../Page/克丘亞語.md "wikilink")，這使得一些[語言學家認為這兩種語言系出同源](../Page/語言學家.md "wikilink")，並因此主張將這兩個語言劃入同一個[克丘亞語系](../Page/克丘亞語系.md "wikilink")，這一點至今仍有不少的爭議。艾馬拉語是一種具有[詞形變化的語言](../Page/詞形變化.md "wikilink")，並且主要使用[主賓謂結構之語序](../Page/主賓謂結構.md "wikilink")。

艾馬拉語的使用人口正在逐渐减少，他们转用[西班牙语或](../Page/西班牙语.md "wikilink")[克丘亞语](../Page/克丘亞语.md "wikilink")。

## 特點

艾馬拉語的語法是建立在[三值邏輯](../Page/三值邏輯.md "wikilink")，而非一般語言所使用的[雙值邏輯之上的](../Page/雙值邏輯.md "wikilink")，[翁贝托·埃可在他的著作](../Page/翁贝托·埃可.md "wikilink")《The
Search for the Perfect Language》認為這種語言具有極大的彈性可容納許多的新辭彙；Ludovico
Bertonio在他1603年的著作《Arte de la lengua
aymara》中認為這種語言在表達抽象概念方面非常有用；在1860年，Emeterio
Villamil de Rada認為這種語言是「亞當的語言」(la lengua de Adán)；而Guzmán de
Rojas則認為這種語言可用作[机器翻译的中介語言](../Page/机器翻译.md "wikilink")

## 參見

  - [美洲原住民語言](../Page/美洲原住民語言.md "wikilink")
  - [语言系属分类](../Page/语言系属分类.md "wikilink")
  - [美洲原住民](../Page/美洲原住民.md "wikilink")
  - [美洲原住民語起源之西班牙單詞列表](../Page/美洲原住民語起源之西班牙單詞列表.md "wikilink")([List of
    Spanish words of Indigenous American Indian
    origin](../Page/:en:List_of_Spanish_words_of_Indigenous_American_Indian_origin.md "wikilink"))
  - [伊良普山](../Page/伊良普山.md "wikilink")
  - [卡梅洛·弗洛雷斯·羅拉](../Page/卡梅洛·弗洛雷斯·羅拉.md "wikilink")
  - [卡米洛·佛勒斯·羅拉](../Page/卡米洛·佛勒斯·羅拉.md "wikilink")

## 參考文獻

  - Rafael E. Núñez, & Eve Sweetser *[With the Future Behind Them :
    Convergent Evidence From Aymara Language and Gesture in the
    Crosslinguistic Comparison of Spatial Construals of
    Time.](http://www.cogsci.ucsd.edu/~nunez/web/NSaymaraproofs.pdf)*
    Cognitive Science, 30(3), 1-49.

## 延伸閱讀

<div class="references-small">

  - Gifford, Douglas. *Time Metaphors in Aymara and Quechua*. St.
    Andrews: University of St. Andrews, 1986.
  - Guzmán de Rojas, Iván. *Logical and Linguistic Problems of Social
    Communication with the Aymara People*. Manuscript report /
    International Development Research Centre, 66e. \[Ottawa\]:
    International Development Research Centre, 1985.
  - Hardman, Martha James. *The Aymara Language in Its Social and
    Cultural Context: A Collection Essays on Aspects of Aymara Language
    and Culture*. Gainesville: University Presses of Florida, 1981. ISBN
    0-8130-0695-3
  - Hardman, Martha James, Juana Vásquez, and Juan de Dios Yapita.
    *Aymara Grammatical Sketch: To Be Used with Aymar Ar Yatiqañataki*.
    Gainesville, Fla: Aymara Language Materials Project, Dept. of
    Anthropology, University of Florida, 1971.
  - Hardman, Martha James. [Primary research materials online as
    full-text in the University of Florida's Digital
    Collections](http://www.uflib.ufl.edu/UFDC/?c=jaqi), on [Dr.
    Hardman's
    website](https://web.archive.org/web/20071026003850/http://grove.ufl.edu/~hardman/),
    and [learning Aymara resources by Dr.
    Hardman](https://web.archive.org/web/20071124115734/http://www.latam.ufl.edu/hardman/aymara/AYMARA.html).

</div>

## 外部連結

  - [線上艾瑪拉語─英語辭典](https://web.archive.org/web/20040404084337/http://www.websters-online-dictionary.org/definition/Aymara-english/)
  - [一个宣传普及艾馬拉語的网站](http://www.jaqi-aru.org/)

[Category:美洲原住民語言](../Category/美洲原住民語言.md "wikilink")