**義和**（431年六月-433年四月）是[十六国時期](../Page/十六国.md "wikilink")[北涼君主](../Page/北涼.md "wikilink")，太祖武宣王[沮渠蒙逊的](../Page/沮渠蒙逊.md "wikilink")[年號](../Page/年號.md "wikilink")，共計2年餘。

[甘肅出土的](../Page/甘肅.md "wikilink")[石刻有](../Page/石刻.md "wikilink")“……**[緣禾](../Page/緣禾.md "wikilink")**三年嵗次[甲戌](../Page/甲戌.md "wikilink")”字樣。\[1\]。學者認爲義和年號可能緣和之錯誤，\[2\]但“緣禾三年嵗次甲戌”則指“緣禾元年”應為壬申年，與史書記載的義和的干支不合。

## 纪年

  -
    {| border=1 cellspacing=0

|-
style="font-weight:bold;background-color:\#CCCCCC;color:\#000000;text-align:right"
|義和||元年||二年||三年 |- style="background-color:\#FFFFFF;text-align:center"
|[公元](../Page/公元纪年.md "wikilink")||431年||432年||433年 |-
style="background-color:\#FFFFFF;text-align:center"
|[干支](../Page/干支纪年.md "wikilink")||[辛未](../Page/辛未.md "wikilink")||[壬申](../Page/壬申.md "wikilink")||[癸酉](../Page/癸酉.md "wikilink")
|}

## 参看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
  - 同期存在的其他政权年号
      - [元嘉](../Page/元嘉_\(南朝宋文帝\).md "wikilink")（424年八月—453年十二月）：[南朝宋皇帝](../Page/南朝宋.md "wikilink")[宋文帝刘义隆的年号](../Page/宋文帝.md "wikilink")
      - [勝光](../Page/胜光.md "wikilink")（428年二月-431年六月）：[夏国政权](../Page/夏国.md "wikilink")[赫连定年号](../Page/赫连定.md "wikilink")
      - [太平](../Page/太平_\(冯跋\).md "wikilink")（409年十月-430年十二月）：[北燕政权](../Page/北燕.md "wikilink")[冯跋年号](../Page/冯跋.md "wikilink")
      - [太興](../Page/太兴_\(冯弘\).md "wikilink")（431年正月-436年五月）：[北燕政权](../Page/北燕.md "wikilink")[冯弘年号](../Page/冯弘.md "wikilink")
      - [神䴥](../Page/神䴥.md "wikilink")（428年二月-431年十二月）：[北魏政权](../Page/北魏.md "wikilink")[北魏太武帝拓跋燾年号](../Page/北魏太武帝.md "wikilink")
      - [延和](../Page/延和_\(北魏太武帝\).md "wikilink")（432年正月-435年正月）：[北魏政权](../Page/北魏.md "wikilink")[北魏太武帝拓跋燾年号](../Page/北魏太武帝.md "wikilink")
      - [泰始](../Page/泰始_\(趙廣\).md "wikilink")（432年正月-437年四月）：[南朝時](../Page/南朝.md "wikilink")[益州領導](../Page/益州.md "wikilink")[趙廣](../Page/趙廣.md "wikilink")、[程道養年号](../Page/程道養.md "wikilink")

## 参考文献

<references/>

[Category:北凉年号](../Category/北凉年号.md "wikilink")
[Y義和](../Category/有爭議的年號.md "wikilink")
[Category:5世纪](../Category/5世纪.md "wikilink")

1.  李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129

2.