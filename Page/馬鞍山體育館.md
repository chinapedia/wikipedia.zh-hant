[HK_MaOnShanSportsCentre2008.jpg](https://zh.wikipedia.org/wiki/File:HK_MaOnShanSportsCentre2008.jpg "fig:HK_MaOnShanSportsCentre2008.jpg")
[Ma_On_Shan_Sports_Centre_Lobby_201511.jpg](https://zh.wikipedia.org/wiki/File:Ma_On_Shan_Sports_Centre_Lobby_201511.jpg "fig:Ma_On_Shan_Sports_Centre_Lobby_201511.jpg")
[Ma_On_Shan_Sports_Centre_Multi_Purpose_Hall_201511.jpg](https://zh.wikipedia.org/wiki/File:Ma_On_Shan_Sports_Centre_Multi_Purpose_Hall_201511.jpg "fig:Ma_On_Shan_Sports_Centre_Multi_Purpose_Hall_201511.jpg")
[Ma_On_Shan_Sports_Centre_Interior_201511.jpg](https://zh.wikipedia.org/wiki/File:Ma_On_Shan_Sports_Centre_Interior_201511.jpg "fig:Ma_On_Shan_Sports_Centre_Interior_201511.jpg")
**馬鞍山體育館**（）位於[香港](../Page/香港.md "wikilink")[新界](../Page/新界.md "wikilink")[沙田區](../Page/沙田區.md "wikilink")[馬鞍山](../Page/馬鞍山_\(香港市鎮\).md "wikilink")，是區內第5所體育場地，於2004年10月1日啟用，總面積6,390[平方米](../Page/平方米.md "wikilink")，其橢圓形的天花頂以及中間凹槽做成的天窗，以採納天然光照射的新穎設計最具特色。天花頂全長70[米](../Page/米_\(單位\).md "wikilink")，最高點有22.1米高。

## 場內設備

該體育館設有1個多用途主場，同時可以用作2個[籃球場](../Page/籃球.md "wikilink")、2個[排球場](../Page/排球.md "wikilink")、8個[羽毛球場或](../Page/羽毛球.md "wikilink")1個[手球場](../Page/手球.md "wikilink")。有1個可以容納1,500人的觀眾席。其他設施包括3個多用途活動室、乒乓球室、[舞蹈室及](../Page/舞蹈.md "wikilink")[健身室](../Page/康文署健身室.md "wikilink")，更有一個以「奇趣礦洞」為主題的兒童遊戲室。另外，亦設有一間體育用品商店。

### 臨時體操訓練中心

2004年體育學院清拆全港唯一合規格的體操館（騰龍館），體操運動員被迫暫用馬鞍山體育館作為訓練基地，直到[順利邨體育館的體操館於](../Page/順利邨體育館.md "wikilink")2005年12月15日落成後才再次獲得永久訓練場地。

[File:HK_MOS_SportsCentre_ScoreBoard.jpg|電子記分牌](File:HK_MOS_SportsCentre_ScoreBoard.jpg%7C電子記分牌)
[File:HK_MOS_SportsCentre_Bear.jpg|花花大熊人](File:HK_MOS_SportsCentre_Bear.jpg%7C花花大熊人)

## 奧運馬術

因舉辦[2008年奧運馬術項目而遷拆的](../Page/2008年夏季奧林匹克運動會馬術項目.md "wikilink")[香港體育學院](../Page/香港體育學院.md "wikilink")，暫時遷往[烏溪沙青年新村](../Page/烏溪沙青年新村.md "wikilink")，需使用[康文署轄下位於](../Page/康文署.md "wikilink")[沙田區的體育設施作為個別精英項目的臨時訓練場地](../Page/沙田區.md "wikilink")，馬鞍山體育館暫時被利用為[羽毛球及](../Page/羽毛球.md "wikilink")[武術運動員訓練場地](../Page/武術.md "wikilink")，[公眾人士在這段期間](../Page/公眾人士.md "wikilink")，只能夠在精英運動員訓練時段以外使用。

## 開放時間

開放時間為每日07:00-23:00。

## 附近建築物

  - [馬鞍山公園](../Page/馬鞍山公園_\(香港\).md "wikilink")
  - [馬鞍山游泳池](../Page/馬鞍山游泳池.md "wikilink")
  - [馬鞍山公共圖書館](../Page/馬鞍山公共圖書館.md "wikilink")

## 地址

沙田馬鞍山鞍駿街14號

## 交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{馬鞍山綫色彩}}">█</font>[馬鞍山綫](../Page/馬鞍山綫.md "wikilink")：[馬鞍山站](../Page/馬鞍山站_\(香港\).md "wikilink")

<!-- end list -->

  - [巴士](../Page/香港巴士.md "wikilink")

<!-- end list -->

  - [專線小巴](../Page/香港小巴.md "wikilink")

</div>

</div>

## 參見

  - [香港康樂及文化事務署](../Page/香港康樂及文化事務署.md "wikilink")
  - [馬鞍山運動場](../Page/馬鞍山運動場.md "wikilink")
  - [順利邨體育館](../Page/順利邨體育館.md "wikilink")

## 外部連結

  - [馬鞍山體育館](https://www.lcsd.gov.hk/tc/facilities/facilitieslist/facilities.php?fid=411)
      - [兒童遊戲室](https://web.archive.org/web/20060615113052/http://www.lcsd.gov.hk/b5/ls_fac_playroom.php)
      - [健身室](https://www.lcsd.gov.hk/tc/fitness/room/rooms_details.php?id=1415)

[Category:馬鞍山 (香港)](../Category/馬鞍山_\(香港\).md "wikilink")