**勞勃·懷斯**（****，），美國[荷里活著名導演及製作人](../Page/荷里活.md "wikilink")。1914年美國[印第安納州出生](../Page/印第安納州.md "wikilink")，年少時因[抑鬱症而輟學](../Page/抑鬱症.md "wikilink")，後得兄弟幫助，在攝影棚找到一份工作，1933年加入影圈當剪接，六年後晉升導演，曾拍攝三十九部電影，四度獲得[奧斯卡金像獎](../Page/奧斯卡金像獎.md "wikilink")。2005年9月14日因心臟衰竭與世長辭。

## 简介

怀斯1958年執導《我要活下去》被提名最佳導演，其後更與[謝洛美·羅賓斯](../Page/謝洛美·羅賓斯.md "wikilink")
(Jerome Robbins)
聯合把荷里活經典[音樂劇](../Page/音樂劇.md "wikilink")《[夢斷城西](../Page/夢斷城西.md "wikilink")》
(West Side
Story)搬上銀幕，1962年憑此片贏得十一項奧斯卡獎項，其中包括「最佳影片」與「最佳導演」，更掀起荷李活及百老匯的[歌舞片熱潮](../Page/歌舞片.md "wikilink")。4年後他又藉《[仙樂飄飄處處聞](../Page/音乐之声_\(电影\).md "wikilink")》
(The Sound of
Music)，再次贏得第38屆奧斯卡最佳故事片獎、最佳導演獎、最佳音樂配樂等5項大獎。1998年怀斯獲頒[美國電影學会](../Page/美國電影學会.md "wikilink")[終身成就獎](../Page/AFI終身成就獎.md "wikilink")。怀斯最後拍攝的作品是2000年由彼德福主演的電視電影《A
Storm in
Summer》。怀斯也是出色的剪接師，其作品包括為人熟知的1941年《[大國民](../Page/大國民.md "wikilink")》，此片獲提名奧斯卡最佳剪接奖。

在導演工作外，怀斯曾於1971年至1975年任[美國導演工會主席](../Page/美國導演工會.md "wikilink")；1985年至1988年又曾獲邀做[美國影藝學院主席](../Page/美國影藝學院.md "wikilink")。

[Category:1914年出生](../Category/1914年出生.md "wikilink")
[Category:2005年逝世](../Category/2005年逝世.md "wikilink")
[Category:印第安纳州人](../Category/印第安纳州人.md "wikilink")
[Category:美国电影导演](../Category/美国电影导演.md "wikilink")
[Category:美國電影監製](../Category/美國電影監製.md "wikilink")
[Category:美国剪接师](../Category/美国剪接师.md "wikilink")
[Category:奧斯卡最佳導演獎獲獎者](../Category/奧斯卡最佳導演獎獲獎者.md "wikilink")
[Category:获得奥斯卡最佳影片奖的制片人](../Category/获得奥斯卡最佳影片奖的制片人.md "wikilink")