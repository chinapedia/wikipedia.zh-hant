**安德鲁·迈克尔·斯彭斯**（**Andrew Michael
Spence**，），[美国](../Page/美国.md "wikilink")[经济学家](../Page/经济学家.md "wikilink")。他与[乔治·阿克洛夫](../Page/乔治·阿克洛夫.md "wikilink")、[约瑟夫·斯蒂格利茨一起获得了](../Page/约瑟夫·斯蒂格利茨.md "wikilink")2001年[诺贝尔经济学奖](../Page/诺贝尔经济学奖.md "wikilink")。斯彭斯當年也是[羅德獎學金得主之一](../Page/羅德獎學金.md "wikilink")。

## 参考资料

  - [诺贝尔官方网站迈克尔·斯彭斯自传](http://nobelprize.org/nobel_prizes/economics/laureates/2001/spence-autobio.html)

[Category:诺贝尔经济学奖获得者](../Category/诺贝尔经济学奖获得者.md "wikilink")
[Category:美国经济学家](../Category/美国经济学家.md "wikilink")
[Category:哈佛大學教師](../Category/哈佛大學教師.md "wikilink")
[Category:哈佛大學校友](../Category/哈佛大學校友.md "wikilink")
[Category:牛津大学麦格达伦学院校友](../Category/牛津大学麦格达伦学院校友.md "wikilink")
[Category:美国罗德学者](../Category/美国罗德学者.md "wikilink")
[Category:古根海姆学者](../Category/古根海姆学者.md "wikilink")
[Category:普林斯頓大學校友](../Category/普林斯頓大學校友.md "wikilink")
[Category:新澤西州人](../Category/新澤西州人.md "wikilink")
[Category:克拉克奖得主](../Category/克拉克奖得主.md "wikilink")
[Category:胡佛研究所人物](../Category/胡佛研究所人物.md "wikilink")