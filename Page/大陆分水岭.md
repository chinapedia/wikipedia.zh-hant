[NorthAmericaDivides.gif](https://zh.wikipedia.org/wiki/File:NorthAmericaDivides.gif "fig:NorthAmericaDivides.gif")

[北美洲的](../Page/北美洲.md "wikilink")**大陆分水岭**（**Continental
Divide**）又稱**大分水嶺**（**Great
Divide**），是一系列將水系[流域大致分隔成](../Page/流域.md "wikilink")[太平洋出海與](../Page/太平洋.md "wikilink")[大西洋](../Page/大西洋.md "wikilink")/[北冰洋出海的](../Page/北冰洋.md "wikilink")[山脊](../Page/山脊.md "wikilink")。北起[阿拉斯加的](../Page/阿拉斯加.md "wikilink")[威爾斯王子角](../Page/威爾斯王子角.md "wikilink")，往南經過[育空地區](../Page/育空地區.md "wikilink")、[卑詩省](../Page/卑詩省.md "wikilink")、[蒙大拿州](../Page/蒙大拿州.md "wikilink")、[懷俄明州](../Page/懷俄明州.md "wikilink")、[科羅拉多州](../Page/科羅拉多州.md "wikilink")、[新墨西哥州以及](../Page/新墨西哥州.md "wikilink")[墨西哥](../Page/墨西哥.md "wikilink")；且實際上一直延伸到[南美洲與](../Page/南美洲.md "wikilink")[安地斯山脈相連](../Page/安地斯山脈.md "wikilink")。

## 外部連結

  - [A detailed map of watersheds in North
    America](http://www.cec.org/naatlas/watersheds.cfm?varlan=english)
  - [A detailed overview of isolated wetlands from the
    USFWS](http://www.fws.gov/nwi/Pubs_Reports/isolated/report_files/2_section/overview.htm)
  - [Detailed article, maps, and boundary
    data](https://web.archive.org/web/20080513163803/http://nationalatlas.gov/articles/geology/a_continentalDiv.html)
    from *The National Atlas of the United States*
  - [Parting of the Waters: a creek that flows to two
    oceans](https://web.archive.org/web/20010814053626/http://www.geocities.com/Heartland/Garden/9907/2OP/)

[Category:北美洲地形](../Category/北美洲地形.md "wikilink")
[Category:北美洲流域](../Category/北美洲流域.md "wikilink")
[Category:落基山脉历史](../Category/落基山脉历史.md "wikilink")