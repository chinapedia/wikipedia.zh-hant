**HBO
Signature**（**HBO原創鉅獻**）在2005年4月正式引入亞洲，與美國本土的版本一樣，播放有爭議性、刺激性的節目，最新的荷里活的電影與HBO獲獎和最新的原創系列。在香港，[now寬頻電視於](../Page/now寬頻電視.md "wikilink")2005年初正式引入繼HBO和[Cinemax](../Page/Cinemax.md "wikilink")，香港的第三條HBO\&Max系列頻道，並於2005年8月中與[HBO
Asia簽訂長期獨家播放協議](../Page/HBO#HBO_Asia.md "wikilink")，於2006年7月生效。

## 歷史

[HBO_Signature_logo_anni.jpg](https://zh.wikipedia.org/wiki/File:HBO_Signature_logo_anni.jpg "fig:HBO_Signature_logo_anni.jpg")

**HBO Signature**在1990年中開始於美國有線電視網絡及衛星網絡首播，當時名為HBO
3，是HBO頻道系列中第三個頻道。在2000年初開此改用HBO Signature。

## 時差頻道

**'HBO Signature**'與HBO一樣，提供美國東部和太平洋時區的兩條主頻道（HBO Signature East & HBO
Signature West），使用戶能夠在三個小時前後（根據不同的地理位置）看同一個節目。

## 參考資料

  - HBO Signature 官方網頁
  - HBO Signature 亞洲頻道
  - [now寬頻電視網頁](http://www.now-tv.com)

## 外部連結

  - [HBO Signature](http://www.hbo.com/hbosignature)
  - [HBO Signature 亞洲](http://www.hboasia.com/signature)

[en:HBO\#Channels](../Page/en:HBO#Channels.md "wikilink")

[Category:香港電視播放頻道](../Category/香港電視播放頻道.md "wikilink")
[Category:台灣電視播放頻道](../Category/台灣電視播放頻道.md "wikilink")
[Category:国际媒体](../Category/国际媒体.md "wikilink")
[Category:HBO](../Category/HBO.md "wikilink")