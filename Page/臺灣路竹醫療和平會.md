**臺灣路竹醫療和平會**（英語：Taiwan Root Medical Peace
Corps），簡稱：**臺灣路竹會**、**路竹會**、**TRMPC**，是在[臺灣發起醫療志願服務的社會團體](../Page/臺灣.md "wikilink")，為非宗教性、非營利性之民間非政府組織。路竹會每月定期帶領醫護人員、志工於臺灣偏遠山區的[原住民部落實施](../Page/臺灣原住民.md "wikilink")[義診](../Page/義診.md "wikilink")，每年也有2至6次前往急需醫療的[第三世界國家義診的長期援助活動](../Page/第三世界.md "wikilink")。目前路竹會的主要服務項目在醫療義診、人文與生態環境關懷、部落孩童陪伴閱讀，但只要是社會有不公平的現象，都願盡棉薄之力，替需要的人服務。

## 組織源起

一位在[新北市](../Page/新北市.md "wikilink")[新店區開業行醫的](../Page/新店區.md "wikilink")[劉啟群](../Page/劉啟群.md "wikilink")[牙醫師在](../Page/牙醫師.md "wikilink")1995年成立「路竹會」並擔任會長，開始號召醫護人員及志工組成醫療團隊，前往臺灣偏遠山區缺乏醫療資源的原住民部落義診。

路竹會的成立與[宗教無關](../Page/宗教.md "wikilink")，其會址也不在[高雄市](../Page/高雄市.md "wikilink")[路竹區](../Page/路竹區.md "wikilink")，取名「路竹」的主要原因為在前往臺灣山區義診的路上常會有竹林，同時引申其路途不斷，竹謙有節的意義。英文名「Taiwan
Root」除音似中文名外，並象徵醫療服務以臺灣為根，從臺灣出發。

路竹會的會徽以[甲骨文的](../Page/甲骨文.md "wikilink")「竹」字為基本造型，和[紅十字相融合](../Page/紅十字.md "wikilink")，同時以[紅色為主色](../Page/紅色.md "wikilink")，象徵熱血與大愛，無限燃燒完全奉獻
\[1\]\[2\]。

## 歷史

路竹會初期以到臺灣偏遠山區的原住民部落義診為主，第一次義診的地方為[新竹縣](../Page/新竹縣.md "wikilink")[尖石鄉的](../Page/尖石鄉.md "wikilink")[鎮西堡與](../Page/鎮西堡.md "wikilink")[新光部落](../Page/新光.md "wikilink")。1999年開始海外義診，第一個義診的地方為[馬其頓](../Page/馬其頓.md "wikilink")[科索夫難民營](../Page/科索夫難民營.md "wikilink")。之後陸續前往[非洲的](../Page/非洲.md "wikilink")[賴比瑞亞](../Page/賴比瑞亞.md "wikilink")、[史瓦濟蘭](../Page/史瓦濟蘭.md "wikilink")、[馬達加斯加](../Page/馬達加斯加.md "wikilink")，[亞洲的](../Page/亞洲.md "wikilink")[印度](../Page/印度.md "wikilink")、[克什米爾](../Page/克什米爾.md "wikilink")、[斯里蘭卡](../Page/斯里蘭卡.md "wikilink")、[印尼地震災區](../Page/印尼.md "wikilink")、[菲律賓](../Page/菲律賓.md "wikilink")、[蒙古](../Page/蒙古.md "wikilink")、[中亞](../Page/中亞.md "wikilink")，[南美洲的](../Page/南美洲.md "wikilink")[玻利維亞](../Page/玻利維亞.md "wikilink")、[巴拉圭](../Page/巴拉圭.md "wikilink")、[祕魯](../Page/祕魯.md "wikilink")、[亞馬遜河流域以及](../Page/亞馬遜河.md "wikilink")[大洋洲的](../Page/大洋洲.md "wikilink")[諾魯](../Page/諾魯.md "wikilink")、中美洲海地、多明尼加、瓜地馬拉、貝里斯等地義診。

2003年開始，路竹會邀請臺灣學者到非洲[迦納及](../Page/迦納.md "wikilink")[奈及利亞學術講學](../Page/奈及利亞.md "wikilink")。

2004年起與[中華至善協會](../Page/中華至善協會.md "wikilink")、[知風草文教協會](../Page/知風草文教協會.md "wikilink")、[伊甸基金會](../Page/伊甸基金會.md "wikilink")、[羅慧夫顱顏基金會等臺灣援外團體組織合組](../Page/羅慧夫顱顏基金會.md "wikilink")「[台灣援外組織聯誼會](../Page/台灣援外組織聯誼會.md "wikilink")」聯合服務行動，陸續前住[越南](../Page/越南.md "wikilink")、[多明尼加](../Page/多明尼加.md "wikilink")、[柬埔寨等國家服務](../Page/柬埔寨.md "wikilink")。

2005年初也前往緊急救援甫遭[2004年印度洋大地震所引發的南亞大](../Page/2004年印度洋大地震.md "wikilink")[海嘯侵襲的](../Page/海嘯.md "wikilink")[斯里蘭卡](../Page/斯里蘭卡.md "wikilink")。

2006年組成醫療學者團包含前[行政院衛生署署長](../Page/行政院衛生署.md "wikilink")[塗醒哲等十人](../Page/塗醒哲.md "wikilink")，前往非洲迦納、[烏干達及](../Page/烏干達.md "wikilink")[肯亞三國的](../Page/肯亞.md "wikilink")[大學](../Page/大學.md "wikilink")、[醫學院](../Page/醫學院.md "wikilink")，交流[愛滋病防治經驗](../Page/愛滋病.md "wikilink")、指導疾病感染控制技術、交流[牙醫臨床技術](../Page/牙醫.md "wikilink")。同年，也援助[東非](../Page/東非.md "wikilink")[索馬利蘭醫療訓練](../Page/索馬利蘭.md "wikilink")，組成[外科](../Page/外科.md "wikilink")[手術醫療示範團](../Page/手術.md "wikilink")，除了在醫院學術與臨床交流外，也派遣護理師在索馬利蘭常駐。在[國家衛生醫院](../Page/國家衛生醫院.md "wikilink")（Hargeisa
Group Hospital）、[伊坦雅旦婦產醫院](../Page/伊坦雅旦婦產醫院.md "wikilink")（Edna Adan
Maternity Hospital）協助培訓護理人員。

2007年 國外義診延伸觸角，除了東非索馬利蘭的義診也前往蒙古國烏蘭巴托
6月持續援助蒙古、8月援助海地、多明尼加、10月援助菲律賓、12月前往印度佛教聖地菩提迦耶義診。

2008年 路竹會義診以電子病歷掛號、看診、檢驗、拿藥，全面e 化 。\[3\]

2009年 路竹會與台北醫學大學合作，推動非洲肯亞醫學生來台研習。\[4\]

2010年 海地發生強烈地震，路竹會1月19日前往醫援，隨後，2月前往甘比亞義診 。

2012年
路竹會長期深入山區，早期10餘年前即進行部落田野調查、疾病研究、學術訪談，為讓團隊更關懷環生態環境，決定長期投入原住民部落的人文與生態與產業、田野調查。路竹會恢復田野調查團，「關懷健康，也要關懷大地環境」，透過各種實地參與的經歷與過程，取得第一手原始資料。

2014年
路竹會田調從中發掘問題，經由社工與醫療人員居家訪視與關懷，連結社會各界與政府資源，讓住民受到更實質幫助，甚至探討議題，促使政府重視。本年11月也開啟「陪伴閱讀」計畫，希望透過原住民偏鄉的陪伴學童，開啟其閱讀興趣，打開另一道希望之窗。

2015年 路竹會2月第303梯次前往非洲聖多美義診，第306梯次為5月前往尼泊爾為強震重災區居民，提供醫療人道救援服務。

\[5\]

## 現況

路竹會以組成醫療服務團，透過義診、衛生教育、物資援助等方式，在國內外醫療資源不足地區提供醫療援助。2006年曾獲得第十六屆[醫療奉獻獎](../Page/醫療奉獻獎.md "wikilink")。同年在[美國成立分會](../Page/美國.md "wikilink")，未來可能如同[無國界醫生成為](../Page/無國界醫生.md "wikilink")[國際非政府組織](../Page/國際非政府組織.md "wikilink")（INGO）\[6\]\[7\]。

路竹會也加入[非政府組織協會](../Page/非政府組織協會.md "wikilink")（CONGO），並具有投票權。將參與國際性組織合作，共同執行健康、醫療等國際組織計畫。

2007年十月，路竹會 與 日本PWJ（ Peace Winds Japan ） 共同創立全球性聯盟， 路竹會長 劉啟群 與 PWJ 執行長
大西健丞 先生共同簽署聯盟備忘錄。 這是台灣的非政府組織（ NGO
）走出國際的創舉，以期在救濟貧困與人道救援發揮全球性的力量，並在國際論壇及機構擴大影響力。

2015年
台灣路竹會3月17日在外交部NGO事務會，與亞洲醫師協會（AMDA）會長菅波茂（SUGANAMI），一起簽署合作備忘錄，將來在國際天然災害緊急救援，或需人道醫療援助時，路竹會醫療團將與亞洲醫師協會成員，合作救災。路竹會亞洲醫師協會簽署合作備忘錄，雙方結盟可以
擴大服務的深度與廣度 ， 深入服務的觸角與地區， 充分結合教育、醫療、照顧、等資源，讓照顧層面更周延。 \[8\]。

## 國內義診

路竹會始終相信，奉獻所能，是人生最美的祝福。健康是普世價值，不分貴賤的權利。醫生不是上帝，無法決定生命的長度，卻因所有醫療、車隊、後勤、廚房等志工聚在一起，改變生命的寬度。國內義診由路竹會提供醫療器材、伙食，加上臺灣各地志願服務的醫護人員、[廚師](../Page/廚師.md "wikilink")、志工以及[路竹車隊](../Page/路竹車隊.md "wikilink")提供越野[車輛與](../Page/車輛.md "wikilink")[駕駛志工組成移動醫療團](../Page/駕駛.md "wikilink")。

依照每半年巡迴一次的行程分批走遍全臺各偏遠山區部落義診。每次行程為2至3日。所有成員利用假期，提供免費的義務醫療服務。服務內容除診治外，也提供衛生教育以及營養品、衛生器材、需求物品、陪伴閱讀、社區居家關懷、衛生教育、用藥諮詢與宣導。此外，也與學術研究單位合作一齊上山作各類型研究調查，一般以原住民疫病調查為主。

在國內義診的同時，路竹會也進行田野調查，關於民住民文化、生態、人文、地景、藝術、車隊等資訊「國內義診是我們的根，國外救援是力量的延伸，如果說我們踏實的做，­每年都會有田野調查成果，但絕對不要迷失在這個掌聲裏面。」，路竹會會長劉啟群這樣說。「義診；或許能從活動中得到快樂，但不會因此變偉大。這些榮耀都歸於所有工作夥伴，因為有了所有人的支援與參與，才成就了路竹會。」

愛，就是在別人的需要上，看見自己的責任。克難的住宿環境，惡劣的路況，讓志工受到考驗與印象深刻，但刻苦的環境也是國外義診的培訓基地，資深志工帶領新近志工與醫學與醫療科系大學生參與，也是承傳人文關懷的理念。志工參與「工作假期」公益活動，埋下的種子，日後在各領域發光發熱。志工們堅信，蜿蜒險路的盡頭，一定有最需要幫助的人；犧牲幾天的假期，卻從病患的感謝與微笑中，尋回心底的富足快樂。

\[9\]。

### 義診部落列表

以下列出路竹會曾經義診過的臺灣原住民部落\[10\]：

|                                                            |                                                                                               |                                                                                                                                                                                                  |                    |
| ---------------------------------------------------------- | --------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ------------------ |
| align ="center"|縣市                                         | align ="center"|鄉鎮市                                                                           | align ="center"|部落（村）                                                                                                                                                                            | align ="center"|備註 |
| align ="center"|[桃園市](../Page/桃園市.md "wikilink")           | align ="center"|[復興區](../Page/復興區_\(桃園市\).md "wikilink")                                      | [雪霧鬧](../Page/雪霧鬧.md "wikilink")、下逵輝                                                                                                                                                             | \-                 |
| rowspan=2 align ="center"|[新竹縣](../Page/新竹縣.md "wikilink") | align ="center"|[尖石鄉](../Page/尖石鄉.md "wikilink")                                              | [養老](../Page/養老_\(原住民部落\).md "wikilink")、[新光](../Page/新光.md "wikilink")、[鎮西堡](../Page/鎮西堡.md "wikilink")、[石磊](../Page/石磊.md "wikilink")、玉峰、錦路、宇抬、馬美、 司馬庫斯、泰崗、下馬美、[梅花](../Page/梅花村.md "wikilink") | \-                 |
| align ="center"|[五峰鄉](../Page/五峰鄉_\(台灣\).md "wikilink")    | [竹林](../Page/竹林.md "wikilink")                                                                | \-                                                                                                                                                                                               |                    |
| align ="center"|[苗栗縣](../Page/苗栗縣.md "wikilink")           | align ="center"|[泰安鄉](../Page/泰安鄉.md "wikilink")                                              | [天狗](../Page/天狗.md "wikilink")、[大安](../Page/大安_\(泰安鄉\).md "wikilink")、象鼻、蘇魯、雪山坑、中興、[桃山](../Page/桃山.md "wikilink")                                                                                | \-                 |
| align ="center"|[台中市](../Page/台中市.md "wikilink")           | align ="center"|[和平區](../Page/和平區_\(臺中市\).md "wikilink")                                      | [環山](../Page/環山.md "wikilink")                                                                                                                                                                   | \-                 |
| align ="center"|[南投縣](../Page/南投縣.md "wikilink")           | align ="center"|[仁愛鄉](../Page/仁愛鄉.md "wikilink")                                              | [萬豐](../Page/萬豐.md "wikilink")、[紅香](../Page/紅香.md "wikilink")、瑞岩、新望洋、合作、親愛、平靜、馬力觀、武界、松林、[翠巒](../Page/翠巒.md "wikilink")                                                                           | \-                 |
| align ="center"|[嘉義縣](../Page/嘉義縣.md "wikilink")           | align ="center"|[阿里山鄉](../Page/阿里山鄉.md "wikilink")                                            | [新美](../Page/新美.md "wikilink")、[特富野](../Page/特富野.md "wikilink")、[里佳](../Page/里佳.md "wikilink")、山美、豐山、茶山、[來吉](../Page/來吉.md "wikilink")、[達邦](../Page/達邦.md "wikilink")、【番路鄉】草山、【梅山鄉】太和            | \-                 |
| rowspan=2 align ="center"|[高雄市](../Page/高雄市.md "wikilink") | align ="center"|[茂林區](../Page/茂林區.md "wikilink")                                              | [寶山](../Page/寶山里.md "wikilink")、多納、[萬山](../Page/萬山.md "wikilink")                                                                                                                                | \-                 |
| align ="center"|[桃源區](../Page/桃源區.md "wikilink")           | [梅山](../Page/梅山里.md "wikilink")、[二集團](../Page/二集團.md "wikilink")                              | \-                                                                                                                                                                                               |                    |
| rowspan=5 align ="center"|[屏東縣](../Page/屏東縣.md "wikilink") | align ="center"|[泰武鄉](../Page/泰武鄉.md "wikilink")                                              | [泰武](../Page/泰武村.md "wikilink")                                                                                                                                                                  | \-                 |
| align ="center"|[來義鄉](../Page/來義鄉.md "wikilink")           | [大後](../Page/大後.md "wikilink")、【春日鄉】士文、                                                       | \-                                                                                                                                                                                               |                    |
| align ="center"|[三地門鄉](../Page/三地門鄉.md "wikilink")         | [大社](../Page/大社村.md "wikilink")、【牡丹鄉】高士、中間路（中興）、                                              | \-                                                                                                                                                                                               |                    |
| align ="center"|[霧台鄉](../Page/霧台鄉.md "wikilink")           | [佳暮](../Page/佳暮.md "wikilink")、[大武](../Page/大武村.md "wikilink")、[阿禮](../Page/阿禮.md "wikilink") | \-                                                                                                                                                                                               |                    |
| align ="center"|[瑪家鄉](../Page/瑪家鄉.md "wikilink")           | [瑪家](../Page/瑪家村.md "wikilink")                                                               | \-                                                                                                                                                                                               |                    |
| rowspan=2 align ="center"|[花蓮縣](../Page/花蓮縣.md "wikilink") | align ="center"|[瑞穗鄉](../Page/瑞穗鄉.md "wikilink")                                              | [奇美](../Page/奇美.md "wikilink")                                                                                                                                                                   | \-                 |
| align ="center"|[富里鄉](../Page/富里鄉.md "wikilink")           | [永豐](../Page/永豐.md "wikilink")                                                                | \-                                                                                                                                                                                               |                    |
| rowspan=6 align ="center"|[台東縣](../Page/台東縣.md "wikilink") | align ="center"|[長濱鄉](../Page/長濱鄉.md "wikilink")                                              | [南溪](../Page/南溪.md "wikilink")                                                                                                                                                                   | \-                 |
| align ="center"|[東河鄉](../Page/東河鄉_\(台灣\).md "wikilink")    | [尚德](../Page/尚德村.md "wikilink")                                                               | \-                                                                                                                                                                                               |                    |
| align ="center"|[金峰鄉](../Page/金峰鄉_\(臺灣\).md "wikilink")    | [歷坵](../Page/歷坵.md "wikilink")、[多良](../Page/多良.md "wikilink")                                 | \-                                                                                                                                                                                               |                    |
| align ="center"|[大武鄉](../Page/大武鄉_\(臺灣\).md "wikilink")    | [加羅板](../Page/加羅板.md "wikilink")、[愛國埔](../Page/愛國埔.md "wikilink")、【達仁鄉】新化、台坂、土坂               | \-                                                                                                                                                                                               |                    |
| align ="center"|[延平鄉](../Page/延平鄉.md "wikilink")           | [紅葉](../Page/紅葉.md "wikilink")、[鸞山](../Page/鸞山.md "wikilink")                                 | \-                                                                                                                                                                                               |                    |
| align ="center"|[海端鄉](../Page/海端鄉.md "wikilink")           | [利稻](../Page/利稻.md "wikilink")、[霧鹿](../Page/霧鹿.md "wikilink")、[新武](../Page/新武.md "wikilink")  | \-                                                                                                                                                                                               |                    |

## 國外義診

會長劉啟群的理念是「台灣以前接受別人幫助，現在我們有能力，是該幫助人家了。」國外義診地區，由劉啟群先行前往探路了解醫療需求後，再由臺灣有義診經驗的醫護人員組成醫療團，加上路竹會自身的醫療器材及各界捐助醫療物資前往受助國5天至半個月實施長期巡迴義診。也與學術研究單位合作研究受助國當地流行疫病，如[瘧疾](../Page/瘧疾.md "wikilink")、[愛滋病等](../Page/愛滋病.md "wikilink")。此外，近期也提供當地人員[教育訓練](../Page/教育訓練.md "wikilink")，讓醫療服務延續不間斷。

1998年中，基於醫療無國界及人道關懷的理念，開始籌畫到國外幫助急需醫療的開發中國家，並且在地震、颱風、海嘯等各種重大災難時，給予緊急救援與醫療服務，展現醫療無國界的精神。期待點滴拋磚引玉的服務中，實踐人道救援的願景。

在諸多的國際人道救援行動中，台灣醫療團隊深入災區，路竹會的機動性很高，在災害發生短短一星期內，召集專業醫療人員與志工，帶著醫療器材前往救助。發揮人飢己飢精神，成功救助了不少極弱勢病患。甚至在2005年4月的尼泊爾地震災情慘重，路竹會也發起義診活動，深入加德滿都以外的重災區。

成立24年來，足跡遍佈全球48幾個國家。­哪裡有需要，路竹會就到哪裡去，路竹會目前是國內第一個，也是唯一以台灣名義加入與聯合國具有諮詢地位CONGO
INGOs的國內非政府組織。在2009年11月，前往菲律賓接受由菲律賓古西和平奬基金會，所頒發的2009年古西和平奬(Gusi Peace
Prize Award)，此奬項被稱為亞洲的等效諾貝爾和平獎，可說是真正­的台灣之光。

\[11\]。

### 義診國家或地區列表

以下列出路竹會曾經義診過的國家或地區\[12\]：

|                                                    |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |                    |
| -------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------ |
| align ="center"|洲別                                 | align ="center"|國家或地區                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             | align ="center"|備註 |
| align ="center"|[歐洲](../Page/歐洲.md "wikilink")     | [馬其頓](../Page/馬其頓.md "wikilink")                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  | \-                 |
| align ="center"|[亞洲](../Page/亞洲.md "wikilink")     | [印度](../Page/印度.md "wikilink")、印屬[克什米爾](../Page/克什米爾.md "wikilink")、印北[德蘭莎拉](../Page/德蘭莎拉.md "wikilink")、[拉達克](../Page/拉達克.md "wikilink")[西藏難民營](../Page/西藏.md "wikilink")、[斯里蘭卡](../Page/斯里蘭卡.md "wikilink")、[印尼](../Page/印尼.md "wikilink")[蘇門達臘災區](../Page/蘇門達臘.md "wikilink")、[菲律賓](../Page/菲律賓.md "wikilink")、[蒙古](../Page/蒙古.md "wikilink")、[巴基斯坦](../Page/巴基斯坦.md "wikilink")、[柬埔寨](../Page/柬埔寨.md "wikilink")、[寮國](../Page/寮國.md "wikilink")、[中國大陸](../Page/中國大陸.md "wikilink")、[泰國](../Page/泰國.md "wikilink")、[緬甸](../Page/緬甸.md "wikilink")、[尼泊爾（2015）](../Page/尼泊爾（2015）.md "wikilink") | \-                 |
| align ="center"|[非洲](../Page/非洲.md "wikilink")     | [賴比瑞亞](../Page/賴比瑞亞.md "wikilink")、[史瓦濟蘭](../Page/史瓦濟蘭.md "wikilink")、[馬達加斯加](../Page/馬達加斯加.md "wikilink")、[塞內加爾](../Page/塞內加爾.md "wikilink")、[馬拉威](../Page/馬拉威.md "wikilink")、[索馬利蘭](../Page/索馬利蘭.md "wikilink")、[甘比亞](../Page/甘比亞.md "wikilink")、[南非](../Page/南非.md "wikilink")、[肯亞](../Page/肯亞.md "wikilink")、[聖多美普林西比(2015)](../Page/聖多美普林西比\(2015\).md "wikilink")、[奈及利亞](../Page/奈及利亞.md "wikilink")、[烏干達](../Page/烏干達.md "wikilink")、[查德](../Page/查德.md "wikilink")                                                                                                                        | \-                 |
| align ="center"|[中南美洲](../Page/中南美洲.md "wikilink") | [玻利維亞](../Page/玻利維亞.md "wikilink")、[海地](../Page/海地.md "wikilink")、[多明尼加](../Page/多明尼加.md "wikilink")、[巴拉圭](../Page/巴拉圭.md "wikilink")、[祕魯](../Page/祕魯.md "wikilink")、[厄瓜多](../Page/厄瓜多.md "wikilink")、[宏都拉斯](../Page/宏都拉斯.md "wikilink")、[尼加拉瓜](../Page/尼加拉瓜.md "wikilink")、[巴拿馬](../Page/巴拿馬.md "wikilink")、[亞馬遜河流域](../Page/亞馬遜河流域.md "wikilink")、[聖克里斯多福](../Page/聖克里斯多福.md "wikilink")、[薩爾瓦多](../Page/薩爾瓦多.md "wikilink")                                                                                                                                                                     | \-                 |
| align ="center"|[大洋洲](../Page/大洋洲.md "wikilink")   | [諾魯](../Page/諾魯.md "wikilink")、[索羅門群島、](../Page/索羅門群島.md "wikilink")[馬紹爾群島](../Page/馬紹爾群島.md "wikilink")                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          | \-                 |
| align ="center"|[中東](../Page/中東.md "wikilink")     | [約旦難民營](../Page/約旦難民營.md "wikilink")                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              | \-                 |

## 參見

  - [無國界醫生](../Page/無國界醫生.md "wikilink")
  - [世界展望會](../Page/世界展望會.md "wikilink")
  - [慈濟基金會](../Page/慈濟基金會.md "wikilink")

## 發行會刊

  - 路竹會雙語會刊

## 上班時間

  - 週一至週五 9am-6pm

## 交通

### 開車

  - 北二高＼新店交流道下＼中興路三段＼民權路＼中正路（右轉）＼右轉四維巷直走到底
  - 台北市環河快速道路＼走到底＼接中正路＼穿越復興路＼第一個紅綠燈左轉四維巷到底
  - 台北縣新莊、板橋、中和：東西向64號快速道路＼秀朗橋＼右轉中正路＼左轉四維巷直走到底
  - 台北市羅斯福路＼新北市新店區北新路＼民權路＼中正路（右轉）＼四維巷直走到底
  - 台北市景美景文街＼新北巿新店區復興路--\>（左轉）中正路--\>第二個紅綠燈（左轉）四維巷

### 捷運

  - [大坪林站](../Page/大坪林站.md "wikilink")(１號出口 ：同仁醫院（北新路西側、民權路口）
    )--\>（選下列方案）

      - 步行約20分鐘，民權路往中正路走到底--\>右轉中正路--\>四維巷--\>遠東工業區--\>過圓環直走到底--\>8弄1號5樓
      - 轉搭下列公車路線至慈濟醫院、莊敬中學、遠東工業城

### 公車

  - 綠2，綠5，綠6，綠8，673，918，905，906，909，290

## 註釋

## 參考文獻

  - [「1999
    馬其頓醫療救援之旅」專題報導](https://web.archive.org/web/20070929170830/http://www.taiwan-for-who.org.tw/chinese/support/experience/content.asp?id=15)
  - [東非義診
    黃正龍戴頭燈看診](https://web.archive.org/web/20070929125048/http://www.libertytimes.com.tw/2007/new/mar/31/today-center1.htm)
  - [路竹會
    始終堅持不間斷的義診](https://web.archive.org/web/20070927132906/http://www.newstory.info/2000/05/post_46.html)
  - [路竹會在印北西藏難民營義診服務三千餘病患](https://web.archive.org/web/20070927192657/http://www.xizang-zhiye.org/b5/xzxinwen/0109/index.html)
  - [台灣路竹醫療和平會
    玻利維亞義診紀實](https://web.archive.org/web/20070927175700/http://www.npo.org.tw/PhilNews/Show_news.asp?NewsID=2288)
  - [路竹會會長劉啟群醫師率團赴我西非友邦甘比亞義診](https://web.archive.org/web/20070930202303/http://www.mofa.gov.tw/webapp/ct.asp?xItem=8707&ctNode=94)

## 外部連結

  - [台灣路竹醫療和平會](http://www.taiwanroot.org/)

  - [美國路竹會](http://www.ripcusa.org/)

  - 粉絲

[Category:臺灣醫療與健康組織](../Category/臺灣醫療與健康組織.md "wikilink")
[Category:台灣原住民](../Category/台灣原住民.md "wikilink")
[Category:1995年建立的組織](../Category/1995年建立的組織.md "wikilink")
[Category:新北市组织](../Category/新北市组织.md "wikilink")
[Category:新店區](../Category/新店區.md "wikilink")

1.  林孟婷，[路竹會
    醫療服務行萬里路](http://www.newtaiwan.com.tw/bulletinview.jsp?period=300&bulletinid=7892)
    ，[新台灣新聞週刊](http://www.newtaiwan.com.tw/index.jsp) ，第300期， 2001.12.26

2.  臺灣路竹醫療和平會，[路竹精神](http://www.taiwanroot.org/htm/invest09.htm)
    ，2007.6.11查參

3.  臺灣路竹醫療和平會，[關於路竹](http://www.taiwanroot.org/htm/000.htm) ，2006-09-14

4.  臺灣路竹醫療和平會，[關於路竹](http://www.taiwanroot.org/htm/000.htm) ，2006-09-14

5.  臺灣路竹醫療和平會，[關於路竹](http://www.taiwanroot.org/about.php?id=194)，2006-09-14

6.  黃旭昇，[繞著台灣與地球跑
    台灣路竹會獲醫療奉獻獎](http://www.epochtimes.com/b5/6/3/24/n1265403.htm)，[大紀元時報](../Page/大紀元時報.md "wikilink")，2006-03-24

7.
8.
9.  臺灣路竹醫療和平會，[義診會微笑](http://www.taiwanroot.org/htm/020.htm) ，1995-2006

10. [國內義診](http://www.taiwanroot.org/internal.php).台灣路竹醫療和平會

11. 台灣路竹醫療和平會，[台灣路竹醫療和平會全球資訊網](http://www.taiwanroot.org/)，1995-2006

12. [國外義診](http://www.taiwanroot.org/international.php).台灣路竹醫療和平會