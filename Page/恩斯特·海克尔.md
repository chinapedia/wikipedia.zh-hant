<small></small> |image = Ernst Haeckel.jpg |caption = |birth_date =
|birth_place =
[普魯士](../Page/普魯士王國.md "wikilink")[波茨坦](../Page/波茨坦.md "wikilink")
|death_date =  |death_place =
[德國](../Page/威瑪共和.md "wikilink")[耶拿](../Page/耶拿.md "wikilink")
|residence = |citizenship = |nationality =  |ethnicity = |field =
|work_institutions = |alma_mater = [耶拿大學](../Page/耶拿大學.md "wikilink")
|doctoral_advisor = |doctoral_students = |known_for =
|author_abbrev_bot = Haeckel |author_abbrev_zoo = Haeckel |prizes =
[达尔文-华莱士奖章](../Page/达尔文-华莱士奖章.md "wikilink") |religion = |footnotes = }}

**恩斯特·海因里希·菲利普·奥古斯特·海克尔**（，）生于[波茨坦卒于](../Page/波茨坦.md "wikilink")[耶拿](../Page/耶拿.md "wikilink")，[德国](../Page/德国.md "wikilink")[生物学家](../Page/生物学家.md "wikilink")、[博物学家](../Page/博物学家.md "wikilink")、[哲学家](../Page/哲学家.md "wikilink")、[艺术家](../Page/艺术家.md "wikilink")，同时也是[医生](../Page/医生.md "wikilink")、[教授](../Page/教授.md "wikilink")。海克尔将[查尔斯·罗伯特·达尔文的](../Page/查尔斯·罗伯特·达尔文.md "wikilink")[進化論引入](../Page/進化論.md "wikilink")[德国并在此基础上继续完善了](../Page/德国.md "wikilink")[人类的进化论理论](../Page/人.md "wikilink")。

海克尔本来的职务是[医生](../Page/医生.md "wikilink")，后来任[比较解剖学的](../Page/比较解剖学.md "wikilink")[教授](../Page/教授.md "wikilink")。他是最早将[心理学看作是](../Page/心理学.md "wikilink")[生理学的一个分支的人之一](../Page/生理学.md "wikilink")。他引入了一些今天在[生物学中非常普遍的术语如](../Page/生物学.md "wikilink")[生态学](../Page/生态学.md "wikilink")、[门等](../Page/门_\(生物\).md "wikilink")，他将[政治学称为是](../Page/政治学.md "wikilink")“应用生物学”。他的一些[理论和主张后来被](../Page/理论.md "wikilink")[纳粹理论家利用](../Page/纳粹主义.md "wikilink")，成为其[种族主义和](../Page/种族主义.md "wikilink")[社会达尔文主义的理由](../Page/社会达尔文主义.md "wikilink")。海克尔也是[优生学的先驱](../Page/优生学.md "wikilink")。

海克尔比较知名的文章主要是他的科普、嘲讽文章或者他的游记，但他的学术文章今天依然可以提供新的启发。比如他1866年的《形态学大纲》是世界上第一部[达尔文的进化论的教科书](../Page/查爾斯·達爾文.md "wikilink")，在他的1874年的《人类学》中他使用比较解剖学的方法来探讨人从动物世界的[进化和人的来源](../Page/进化.md "wikilink")。他的三卷长的《系统发生学》至今很少有人读，这是一部从1894年到1896年发表的巨著，其中海克尔描述了他对整个动物世界的进化和亲属关系的认识。

[Ernst_Haeckel_1860.jpg](https://zh.wikipedia.org/wiki/File:Ernst_Haeckel_1860.jpg "fig:Ernst_Haeckel_1860.jpg")

## 艺术和自然

海克尔认为生物学在许多方面与[艺术类似](../Page/艺术.md "wikilink")。自然界中的[对称](../Page/对称.md "wikilink")，比如[单细胞生物中的](../Page/单细胞生物.md "wikilink")[放射虫对他的艺术天赋有很大的启发](../Page/放射虫目.md "wikilink")。尤其著名的是他画的[浮游生物和](../Page/浮游生物.md "wikilink")[水母的画](../Page/水母.md "wikilink")，这些图画生动地体现了生物世界的美。不论是在他的学术著作还是在他的科普著作中他都画有优美的插图。他的图画对20世纪初的艺术也有影响。[新艺术运动就是从他的一些插图中获得启发形成的](../Page/新艺术运动.md "wikilink")。

海克尔在耶拿的住宅和那里的一个他资助的[博物馆的建筑也体现出了他的艺术天才](../Page/博物馆.md "wikilink")。

海克尔非常勤劳。仅使用[英国的](../Page/英国.md "wikilink")“挑战号”的数据他描写了3500多个放射虫的[种](../Page/种_\(生物\).md "wikilink")。他的这份报告共三卷，2750页，包括140个非常细腻的放射虫的图像。尤其是他的第一位夫人逝世后他往往每天工作18小时。

## 复演说

[Haeckel_drawings.jpg](https://zh.wikipedia.org/wiki/File:Haeckel_drawings.jpg "fig:Haeckel_drawings.jpg")
海克尔对于[个体发育和](../Page/个体发育.md "wikilink")[种系遗传学的平行观察促使他提出了个体发育与进化过程中的因果关系的理论](../Page/种系遗传学.md "wikilink")。这就是所谓的[复演说](../Page/复演说.md "wikilink")。今天这个理论已被推翻。

## 哲学

由于海克尔对进化论的证明不精确，而且他本人将自然科学的认识与宗教对立起来，因此他的理论成为试图驳倒进化论的[创造论者的攻击对象](../Page/创造论.md "wikilink")。他也受到后来的生物学家如[史蒂芬·杰·古尔德的批评](../Page/史蒂芬·杰·古尔德.md "wikilink")。在哲学上他持[一元论](../Page/一元论.md "wikilink")，认为“世界与上帝是同一的”。在他的《宇宙之谜》（Die
Welträthsel）中他写道：

> 随着每年我们对自然的认识的发展过去的对立不断缩小，因此最基本的世界观问题的解决也越来越近了。我们可以希望在将来的20世纪越来越多这些对立统一起来，越来越多的人接受这个希求的世界观的统一的[一元论](../Page/一元论.md "wikilink")。

海克尔并非严格的[无神论者](../Page/无神论.md "wikilink")。他虽然反对[创世论](../Page/创世论.md "wikilink")，但他出生于一个[基督教家庭](../Page/基督教.md "wikilink")，他认为世界万物（包括无机的矿物）是有灵魂的。他的[唯物主义是一个有](../Page/唯物主义.md "wikilink")[灵魂的物质的物质主义](../Page/灵魂.md "wikilink")。他认为[上帝与万能的](../Page/上帝.md "wikilink")[自然规律是同一的](../Page/自然规律.md "wikilink")。他使用“细胞记忆”和“晶体灵魂”之类的词语。

## 社会达尔文主义

海克尔无疑是[德国优生论的启发人](../Page/德国.md "wikilink")。在他的《生命奇迹》中他写道：“我们的文明国家人为地养育着成千上万得了不治之症的人，比如神经病者、麻风病人、癌症病人等等，这对这些人本身和对整个社会没有任何好处。”从1905年起海克尔是“优生论社团”的成员。

在海克尔的文章中常常体现出他的[德国民族](../Page/德国.md "wikilink")[沙文主义](../Page/沙文主义.md "wikilink")。比如在他的《永久》中他写道：“每个教育良好的[德国战士](../Page/德国.md "wikilink")……在智慧和道德价值上要比上百个[英国](../Page/英国.md "wikilink")、[法国](../Page/法国.md "wikilink")、[俄国和](../Page/俄国.md "wikilink")[意大利所能提供的原始的自然人要高](../Page/意大利.md "wikilink")。”在《形态学大纲》中他说：“高等人与低等人之间的差别比低等人与高级动物之间的差别要大。”

这些和其它许多海克尔的言论使他在[德国思想史上起了一个非常不光彩的作用](../Page/德国.md "wikilink")：他是[纳粹主义的铺路人](../Page/纳粹主义.md "wikilink")。尤其是海克尔利用他在学术界的权威地位来普及他的政治观点。

## 著名学者

海克尔的理论对进化论的历史非常重要。海克尔也是提出[生态学这个概念的人](../Page/生态学.md "wikilink")。尤其他在[解剖学上的知识给进化论提供了很多新的观点](../Page/解剖学.md "wikilink")。他还描述了上百新的物种。此外他引入了使用[演化树来描写生物学中的进化关系的表示方式](../Page/演化树.md "wikilink")。不过今天这样的演化树已经被认为是过时和不准确的。他提出了所有生物是从共同的祖先发展出来的设想。他推测这些祖先可能分三个组。大多数他的理论今天已经过时了。

## 批评

从现代的生物学来看，海克尔1866年推出的复演说是错误的，这个理论并非自然规律。虽然如此这个观察依然有一定的意义。它虽然不是一个自然规律，但是一个经常发生的现象。由于海克尔让他的哲学观点影响他的学术观点，因此他的一些生物的图画部分故意画错。从哲学上来说，他更加偏向[拉马克的](../Page/拉马克.md "wikilink")[用进废退的理论](../Page/拉馬克主義.md "wikilink")。他有时故意错误地描写自然现象或者至少不精确严格地从各个方面来观察这些现象来使得他的描写更加符合他的一元论的哲学理论。

因為海克爾讓演化生物學牽扯上種族主義、法西斯和納粹，反而對演化論的推廣不利，因此有人稱他是演化論的豬隊友。\[1\]

## 参考文献

## 參見

  - 《[自然界的藝術形態](../Page/自然界的藝術形態.md "wikilink")》（）

{{-}}

[Category:20世紀哲學家](../Category/20世紀哲學家.md "wikilink")
[Category:19世紀哲學家](../Category/19世紀哲學家.md "wikilink")
[Category:德國動物學家](../Category/德國動物學家.md "wikilink")
[Category:德国哲学家](../Category/德国哲学家.md "wikilink")
[Category:演化生物學家](../Category/演化生物學家.md "wikilink")
[Category:科學插畫家](../Category/科學插畫家.md "wikilink")
[Category:德國畫家](../Category/德國畫家.md "wikilink")
[Category:耶拿大學教師](../Category/耶拿大學教師.md "wikilink")
[Category:維爾茨堡大學校友](../Category/維爾茨堡大學校友.md "wikilink")
[Category:柏林洪堡大學校友](../Category/柏林洪堡大學校友.md "wikilink")
[Category:布蘭登堡人](../Category/布蘭登堡人.md "wikilink")
[Category:科學種族主義](../Category/科學種族主義.md "wikilink")

1.