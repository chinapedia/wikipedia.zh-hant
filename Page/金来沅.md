**金來沅**\[1\]（，），身高183公分，[韩国](../Page/韩国.md "wikilink")[男演员](../Page/演员.md "wikilink")。1997年通過MBC青春偶像劇《我》出道。2009年8月13日，於[韓國陸軍](../Page/韓國陸軍.md "wikilink")報到入伍，因為患有退化性腰痛而服替代役，2011年8月23日退伍。

## 演出作品

### 电视剧

  - 1997年：[MBC](../Page/文化廣播_\(韓國\).md "wikilink")《》飾演 金來沅
  - 1998年：[SBS](../Page/SBS株式會社.md "wikilink")《[順風婦產科](../Page/順風婦產科.md "wikilink")》飾演
    金雷恩／金來沅
  - 1999年：[KBS](../Page/韓國放送公社.md "wikilink")《》飾演 李漢
  - 1999年：MBC《[神鳥](../Page/傳說的故鄉#1999年製作.md "wikilink")》飾演 高句麗將帥
  - 2000年：KBS《》飾演 崔正植
  - 2000年：SBS《》飾演 金德京
  - 2001年：KBS《》飾演 李在民
  - 2001年：MBC《》飾演 李榮勳
  - 2002年：KBS《－少女與小偷》飾演 劉彬
  - 2002年：MBC《[紅豆女之戀](../Page/紅豆女之戀.md "wikilink")》飾演 金賢成
  - 2003年：MBC《》飾演 車成俊
  - 2003年：MBC《[屋塔房小猫](../Page/屋塔房小猫.md "wikilink")》飾演 李慶民
  - 2004年：MBC《[說你愛我](../Page/說你愛我.md "wikilink")》飾演 金秉修
  - 2004年：SBS《[愛在哈佛](../Page/愛在哈佛.md "wikilink")》飾演 金賢宇
  - 2006年：MBC《[你來自哪顆星](../Page/你來自哪顆星.md "wikilink")》飾演 崔勝熙
  - 2008年：SBS《[食客](../Page/食客_\(電視劇\).md "wikilink")》飾演 李成燦
  - 2011年：SBS《[千日的約定](../Page/千日的約定.md "wikilink")》飾演 朴志亨
  - 2014年：SBS《[Punch](../Page/Punch_\(電視劇\).md "wikilink")》 朴政煥
  - 2016年：SBS《[Doctors](../Page/Doctors.md "wikilink")》飾演 洪智弘
  - 2017年：KBS《[黑騎士](../Page/黑騎士_\(韓國電視劇\).md "wikilink")》飾演 文秀浩

### 電影

  - 1998年：《》飾演 權赫秀（青年）
  - 2000年：《[Harpy](../Page/Harpy.md "wikilink")》飾演 姜賢宇
  - 2000年：《》飾演 金子孝
  - 2002年：《》飾演 韓益洙
  - 2003年：《》飾演 英宰
  - 2004年：《[幼齒老婆Oh My God](../Page/幼齒老婆.md "wikilink")》飾演 朴尚民
  - 2005年：《》飾演 具東赫
  - 2006年：《》飾演 吳泰植
  - 2007年：《[花的影子](../Page/花的影子.md "wikilink")》飾演 昇佑
  - 2009年：《》飾演 李康俊
  - 2013年：《》飾演 劉一韓
  - 2014年：《[江南1970](../Page/江南1970.md "wikilink")》飾演 白鎔基
  - 2017年：《[叛獄無間](../Page/The_Prison.md "wikilink")》飾演 宋有健
  - 2017年：《[死者的審判](../Page/死者的審判.md "wikilink")》飾演 徐振宏

### 綜藝節目

  - 2017年9月20日 : JTBC《[請給一頓飯Show](../Page/請給一頓飯Show.md "wikilink")》

## 获奖

  - 2000年：第21屆[青龍電影獎](../Page/青龍電影獎.md "wikilink")－男子新人奖（）
  - 2003年：[MBC演技大赏](../Page/MBC演技大赏.md "wikilink")－男子最优秀演技赏、男子人氣賞（[屋塔房小猫](../Page/屋塔房小猫.md "wikilink")）
  - 2004年：第41届[大鐘獎](../Page/大鐘獎.md "wikilink")－男子新人奖（[幼齒老婆Oh My
    God](../Page/幼齒老婆.md "wikilink")）
  - 2004年：[SBS演技大赏](../Page/SBS演技大赏.md "wikilink")－男子人气赏（[愛在哈佛](../Page/愛在哈佛.md "wikilink")）
  - 2005年：第41屆[百想藝術大賞](../Page/百想藝術大賞.md "wikilink")－電視部門男子人氣賞（愛在哈佛）
  - 2008年：第2屆[韓國電視劇節](../Page/韓國電視劇節.md "wikilink")－男子優秀賞（[食客](../Page/食客_\(電視劇\).md "wikilink")）
  - 2008年：SBS演技大賞－十大明星賞（食客）
  - 2011年：SBS演技大賞－十大明星賞、特別企劃部門
    男子最優秀演技賞（[千日的約定](../Page/千日的約定.md "wikilink")）
  - 2015年：[SBS演技大賞](../Page/2015_SBS演技大賞.md "wikilink")－放送三社PD賞（[逆轉人生180天](../Page/Punch_\(電視劇\).md "wikilink")）
  - 2016年：[SBS演技大賞](../Page/2016_SBS演技大賞.md "wikilink")－體裁、幻想類電視劇
    男子最優秀演技賞（[Doctors](../Page/Doctors.md "wikilink")）

## 腳註

## 外部連結

  -
  -
  - [EPG](https://web.archive.org/web/20080624004717/http://epg.epg.co.kr/star/profile/index.asp?actor_id=288)


[K](../Category/韓國男演員.md "wikilink")
[K](../Category/韓國電視演員.md "wikilink")
[K](../Category/韓國電影演員.md "wikilink")
[K](../Category/韓國中央大學校友.md "wikilink")
[K](../Category/江原道出身人物.md "wikilink")
[K](../Category/韓國天主教徒.md "wikilink")
[K](../Category/金姓.md "wikilink")
[K](../Category/1981年出生.md "wikilink")

1.  [김래원, '1일 명예민원봉사실장' 위촉](http://news.nate.com/view/20090303n16954)