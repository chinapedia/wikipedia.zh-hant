《**智代After ～It's a Wonderful
Life～**》（）是由[日本](../Page/日本.md "wikilink")[Key公司所製作發行的](../Page/key_\(公司\).md "wikilink")[戀愛冒險類型](../Page/戀愛冒險.md "wikilink")[成人遊戲](../Page/日本成人遊戲.md "wikilink")，後來陸續移植到不同平台並改編成漫畫。主要描敘《[CLANNAD](../Page/CLANNAD.md "wikilink")》中智代路線之後續故事。

## 遊戲

[Tomoyo_After_screenshot.jpg](https://zh.wikipedia.org/wiki/File:Tomoyo_After_screenshot.jpg "fig:Tomoyo_After_screenshot.jpg")
《智代After ～It's a Wonderful
Life～》是一款[戀愛冒險遊戲](../Page/戀愛冒險.md "wikilink")，[玩家扮演角色岡崎朋也](../Page/玩家.md "wikilink")\[1\]。玩家在遊玩時將會花費大多數時間觀看出現在螢幕上的文字[敘述](../Page/叙事.md "wikilink")、人物[對話以及角色心理等](../Page/对话.md "wikilink")，藉由這樣的方式除了讓故事獲得進展外，也可以與遊戲角色對話進行交流。在每次閱讀故事發展以及與其他角色對話後，玩家便有機會能夠在與角色互動時從遊戲提供的數個選項中挑選其中一項，此時遊戲會暫停一會兒以提供玩家依據先前的對話作出決定。遊戲中會有坂上智代與岡崎朋也[性交的](../Page/性交.md "wikilink")[色情](../Page/變態_\(日語\).md "wikilink")[場景](../Page/計算機圖形.md "wikilink")，不過Key公司之後推出無18禁劇情的全年齡版本。

## 人物介紹

  -
    10月30日生。身長：173cm。体重：61kg。血液型：A型。主角，同時也是有前後關係的作品CLANNAD的主角。與坂上智代交往並同居，在廢品回收公司工作。\[2\]
  -
    10月14日生。身長：161cm。体重：47kg。血液型：O型。3圍：86/57/82。女主角，朋也之女友。有別於CLANNAD中的強悍形像，本作中特別凸顯其與主角交往後的個性變化。相當關心主角，對小孩子（本作中主要是とも）總是無法抗拒。
  -
    智代之弟，與朋也關係甚佳。相當關心自己的家人，曾經在本作時間的三年前，為了挽救家庭免除離異的可能性，以自虐的手段使所有人團結一致。本作許多相關事件因此而起。電腦功力頗強，能流利使用英文在網絡上與外國人交流。
  -
    年紀與鷹文相仿之少女，個性直率，與鷹文似乎有一段過去。某天被朋也發現，由於河南子的目的是尋找鷹文，故被帶回朋也的公寓，進入所有人的生活中。
  -
    還在就讀幼稚園的小女孩，被其母遺棄於坂上家門前，被鷹文發現。因鷹文得知其為父親之私生女，而又不願因此破壞家庭和諧，故將とも帶至朋也的公寓共同生活。很重感情，愛看繪本，不喜歡別人吵架。
  -
    とも之母，基於某些原因放棄とも的實際撫養權。
  -
    朋也之父，一人獨立將朋也拉拔成人，然而朋也卻對其態度十分惡劣，兩人之間交流不深；可參考[CLANNAD中兩人之生活型態](../Page/CLANNAD.md "wikilink")。
  -
    朋也之舊友。在“事故”發生後曾經來探訪。

## 音樂

《智代After ～It's a Wonderful
Life～》的[片头曲和](../Page/片头曲.md "wikilink")[片尾曲各為](../Page/片尾曲.md "wikilink")『Light
Colors』和『Life is like a
Melody』，均由[Lia演唱](../Page/Lia.md "wikilink")。游戏的[原声带](../Page/原声带.md "wikilink")《[智代After
Original
SoundTrack](../Page/智代After_Original_SoundTrack.md "wikilink")》于2005年11月25日与游戏的初回限定版同捆发售。\[3\]2005年12月29日，《》发售，其中收录了《[CLANNAD](../Page/CLANNAD.md "wikilink")》、《智代After
～It's a Wonderful Life～》中许多歌曲的重混版本。\[4\]以上各个唱片的发售者均为Key旗下的[Key Sounds
Label](../Page/Key_Sounds_Label.md "wikilink")。

## 漫畫

由作畫的[漫画于](../Page/漫画.md "wikilink")2007年4月20日至2007年10月20日在[角川書店下的杂志](../Page/角川書店.md "wikilink")《[Dragon
Age
Pure](../Page/Dragon_Age_Pure.md "wikilink")》连载，漫画的标题也为《》\[5\]\[6\]。漫画于2007年12月6日由[富士見書房发售一本](../Page/富士見書房.md "wikilink")[单行本](../Page/单行本.md "wikilink")。\[7\]

## 評價

根據有限公司Peaks曾發行的[PC
NEWS所公佈的日本全國](../Page/PC_NEWS.md "wikilink")[美少女遊戲銷售量排行榜](../Page/美少女遊戲.md "wikilink")，《智代After
～It's a Wonderful
Life～》在2005年11月25日推出之後，於遊戲銷售量排行榜上保持第1名\[8\]，之後在2次遊戲銷售量排名各排名第35名以及第36名\[9\]。《智代After
～It's a Wonderful
Life～》在日本美少女游戏与动画相关商品销售网站Getchu.com的2005年销量榜上排名第8。\[10\]

## 參考來源

## 外部連結

  - [Key Official Home Page](http://key.visualarts.gr.jp/)
  - [PC版官網](http://key.visualarts.gr.jp/product/tomoyo/)
  - [PS2版官網](http://prot.co.jp/ps2/tomoyoafter/index.html)
  - [PS3版官網](http://www.prot.co.jp/ps3/tomoyo/index.html)
  - [Xbox360版官網](http://www.prot.co.jp/xbox/tomoyoafter/)
  - [PSP版官網](http://www.prot.co.jp/psp/tomoyoafter/index.html)
  - [Android版官網](http://sp.v-motto.jp/pc/appli/tomoyo/index.php)

[Category:CLANNAD](../Category/CLANNAD.md "wikilink")
[Category:美少女遊戲](../Category/美少女遊戲.md "wikilink")
[Category:戀愛冒險遊戲](../Category/戀愛冒險遊戲.md "wikilink")
[Category:2007年电子游戏](../Category/2007年电子游戏.md "wikilink")
[Category:Key游戏](../Category/Key游戏.md "wikilink")
[Category:電子遊戲衍生作品](../Category/電子遊戲衍生作品.md "wikilink")
[Category:2005年日本成人遊戲](../Category/2005年日本成人遊戲.md "wikilink")
[Category:Android遊戲](../Category/Android遊戲.md "wikilink")
[Category:PlayStation 2游戏](../Category/PlayStation_2游戏.md "wikilink")
[Category:PlayStation
Portable游戏](../Category/PlayStation_Portable游戏.md "wikilink")
[Category:Xbox_360遊戲](../Category/Xbox_360遊戲.md "wikilink")
[Category:PlayStation 3游戏](../Category/PlayStation_3游戏.md "wikilink")
[Category:Windows遊戲](../Category/Windows遊戲.md "wikilink")
[Category:Steam商店遊戲](../Category/Steam商店遊戲.md "wikilink")
[Category:失憶症題材遊戲](../Category/失憶症題材遊戲.md "wikilink")
[Category:日本漫畫作品](../Category/日本漫畫作品.md "wikilink")

1.
2.
3.

4.

5.

6.

7.

8.

9.

10.