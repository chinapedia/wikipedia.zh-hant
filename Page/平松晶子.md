**平松晶子**，[日本女性](../Page/日本.md "wikilink")[配音員](../Page/配音員.md "wikilink")。出身於[東京都](../Page/東京都.md "wikilink")\[1\]。O型[血](../Page/血型.md "wikilink")。

## 簡介

原屬[81
Produce](../Page/81_Produce.md "wikilink")，現在是[賢Production所屬](../Page/賢Production.md "wikilink")\[2\]。東京動畫養成所畢業。

為人熟悉的代表配音作品有動畫《[Keroro軍曹](../Page/Keroro軍曹_\(動畫\).md "wikilink")》日向家的媽媽〈〉、《[逮捕令](../Page/逮捕令_\(動畫\).md "wikilink")》系列的小早川美幸、《[笑園漫畫大王](../Page/笑園漫畫大王.md "wikilink")》的谷崎由佳里、《[青出於藍](../Page/青出於藍_\(漫畫\).md "wikilink")》的神樂崎雅、《[機械女神](../Page/機械女神.md "wikilink")》系列的紅莓、《[地球防衛企業](../Page/地球防衛企業.md "wikilink")》的桃井伊吹、《[烈火之炎](../Page/烈火之炎.md "wikilink")》的霧澤風子、《[蜜柑繪日記](../Page/蜜柑繪日記.md "wikilink")》的草凪吐夢、《[灌籃高手](../Page/灌籃高手.md "wikilink")》的赤木晴子、《[叮噹貓](../Page/叮噹貓.md "wikilink")》的木之葉美琪、《[機甲戰記威龍](../Page/機甲戰記威龍.md "wikilink")》的蘿絲·帕提頓、《》的艾蕾因·里德／黛安娜·里德；美國動畫《[X戰警](../Page/X戰警.md "wikilink")》的[歡歡等](../Page/歡歡.md "wikilink")。

### 來歷、人物

平松在國中畢業時期很喜歡活動，後來受到漫畫《》的影響下，高中時期原本想組樂團，卻因為成員不足而放棄。但是不久之後收看[電視動畫](../Page/電視動畫.md "wikilink")《[銀河鐵道999](../Page/銀河鐵道999.md "wikilink")》、《[魔法小天使](../Page/我係小忌廉.md "wikilink")》便得知有聲優這項職業而產生興趣，於是平松選擇前往東京動畫養成所進修\[3\]。

1986年，平松以[電視動畫](../Page/電視動畫.md "wikilink")《[相聚一刻](../Page/相聚一刻.md "wikilink")》（聲演第27話幫路人角色〈洋子〉）她的聲優出道作
。1987年，平松為另一齣[電視動畫](../Page/電視動畫.md "wikilink")《[機甲戰記威龍](../Page/機甲戰記威龍.md "wikilink")》登場角色蘿絲·帕提頓配音是她首次聲演主要角色的作品。

平松在廣播節目經常表明自己是[李奧納多·狄卡皮歐的大影迷](../Page/萊昂納多·迪卡普里奥.md "wikilink")。還有，她也喜歡。

### 交友關係

與同期出道的[林原惠](../Page/林原惠.md "wikilink")、[白鳥由里是同行好友](../Page/白鳥由里.md "wikilink")。因此，平松曾經代替請[產假的林原惠在](../Page/產假與育嬰假.md "wikilink")[電視動畫](../Page/電視動畫.md "wikilink")《[神奇寶貝超世代](../Page/神奇寶貝超世代.md "wikilink")》聲演[武藏](../Page/精靈寶可夢動畫角色列表#火箭隊兩人一貓組.md "wikilink")。

## 演出作品

※**粗體字**表示說明飾演的主要角色。

### 電視動畫

  - 1986年

<!-- end list -->

  - [相聚一刻](../Page/相聚一刻.md "wikilink")（洋子）

<!-- end list -->

  - 1987年

<!-- end list -->

  -
  - [機甲戰記威龍](../Page/機甲戰記威龍.md "wikilink")（**蘿絲·帕提頓**）\[4\]

<!-- end list -->

  - 1988年

<!-- end list -->

  -
<!-- end list -->

  - 1989年

<!-- end list -->

  - [機動警察](../Page/機動警察.md "wikilink")
  - [亂馬½、亂馬½ 熱鬥篇](../Page/亂馬½.md "wikilink")

<!-- end list -->

  - 1991年

<!-- end list -->

  - （貝魯貝魯）

  - [金肉人 肉星王位爭奪篇](../Page/金肉人.md "wikilink")（翔野夏子、羅伯特、〈少年時期〉）

  - [崔普一家物語](../Page/崔普一家物語.md "wikilink")（安妮）

  - （岩崎老師）

  - [閃電霹靂車](../Page/閃電霹靂車.md "wikilink")（艾蓮娜）

<!-- end list -->

  - 1992年

<!-- end list -->

  - （瑪格莉特·艾瑞克）

  - [麵包超人](../Page/麵包超人.md "wikilink")（紅麻糬人〈初代〉、）

  - （江古六次太）

  - [超電動機器人 鐵人28號FX](../Page/超電動機器人_鐵人28號FX.md "wikilink")（仁科詩織）

  - [美少女戰士](../Page/美少女戰士_\(動畫\).md "wikilink")（松野浩美、妖魔卡斯特爾）

  - [蜜柑繪日記](../Page/蜜柑繪日記.md "wikilink")（**草凪吐夢**\[5\]）

<!-- end list -->

  - 1993年

<!-- end list -->

  - [GS美神](../Page/GS美神_極樂大作戰！！.md "wikilink")（）
  - [灌籃高手](../Page/灌籃高手.md "wikilink")（**赤木晴子**）
  - 麵包超人（、〈初代〉、）
  - [美少女戰士R](../Page/美少女戰士R.md "wikilink")（靈媒〈〉的）
  - [叮噹貓](../Page/叮噹貓.md "wikilink")（**木之葉美琪**）

<!-- end list -->

  - 1994年

<!-- end list -->

  - [小紅帽恰恰](../Page/小紅帽恰恰.md "wikilink")（索普拉諾）

  - （）

  - （）

  - [美少女戰士S](../Page/美少女戰士S.md "wikilink")（）

<!-- end list -->

  - 1995年

<!-- end list -->

  - [怪盜Saint Tail](../Page/怪盜Saint_Tail.md "wikilink")（滿）
  - [新世紀福音戰士](../Page/新世紀福音戰士.md "wikilink")（UNKNOWS）
  - [NINKU -忍空-](../Page/忍空.md "wikilink")（梅基拉大佐）
  - [爆走獵人](../Page/爆走獵人.md "wikilink")（邦巴）
  - [美少女戰士SS](../Page/美少女戰士SS.md "wikilink")（子）

<!-- end list -->

  - 1996年

<!-- end list -->

  - [機械女神J](../Page/機械女神.md "wikilink")（**紅莓**）

  - 麵包超人（）

  - [逮捕令](../Page/逮捕令_\(動畫\)#第1期.md "wikilink")（**小早川美幸**）

  - [鋼鐵神兵](../Page/鋼鐵神兵.md "wikilink")（鳳·拉斐內〈少年時期〉）

  - [名偵探柯南](../Page/名偵探柯南.md "wikilink")（赤木量子）

  - （朵莉絲·安德森）

  - [無家可歸的孩子蕾米](../Page/無家可歸的孩子蕾米.md "wikilink")（珂賽特‧米歇爾）

<!-- end list -->

  - 1997年

<!-- end list -->

  - （）

  - [甜心戰士F](../Page/甜心戰士F.md "wikilink")（葉月聖羅／夜霧甜心）

  - [蠟筆小新](../Page/蠟筆小新.md "wikilink")（林瑪莉）

  - [靈異獵人](../Page/靈異獵人.md "wikilink")（山田闇子）

  - [烈火之炎](../Page/烈火之炎.md "wikilink")（**霧澤風子**\[6\]）

  - [浪客劍心](../Page/浪客劍心.md "wikilink")（椿）

<!-- end list -->

  - 1998年

<!-- end list -->

  - [魔法陣都市](../Page/魔法陣都市.md "wikilink")（藍菁）

  - [超級娃娃戰士★莉卡](../Page/麗佳公主.md "wikilink")（蜜絲娣）

  - [機械女神J to X](../Page/機械女神.md "wikilink")（**紅莓**）

  - （納爾）

  - [萬能文化貓娘](../Page/萬能文化貓娘.md "wikilink")（有吉今日子）

  - [神奇寶貝](../Page/神奇寶貝_\(1997-2002年動畫\).md "wikilink")（）

<!-- end list -->

  - 1999年

<!-- end list -->

  - （1999年，演出）

  - （甲斐孝子）

  - [金田一少年之事件簿](../Page/金田一少年之事件簿_\(動畫\).md "wikilink")（阿嘉莎）

  - [格鬥小霸王](../Page/格鬥小霸王.md "wikilink")（活力）

  - （）

  - [地球防衛企業](../Page/地球防衛企業.md "wikilink")（**桃井伊吹**\[7\]）

  - （志野）

  - [炸彈人 彈珠人爆外傳V](../Page/炸彈人_彈珠人爆外傳.md "wikilink")（紅寶）

  - 名偵探柯南（保本光）

<!-- end list -->

  - 2000年

<!-- end list -->

  - （笑笑老師）

  - 神奇寶貝（卡茲柯）

<!-- end list -->

  - 2001年

<!-- end list -->

  - （桐原夕映）

  - （瓦尼拉）

  - （茱莉亞·賽多）

  - [通靈王](../Page/通靈王.md "wikilink")（）

  - [索斯機械獸III](../Page/索斯機械獸III.md "wikilink")（瑪莉·強普）

  - [逮捕令 Second
    Season](../Page/逮捕令_\(動畫\)#第2期.md "wikilink")（**小早川美幸**\[8\]）

  - [小小雪精靈](../Page/小小雪精靈.md "wikilink")（漢娜老師、肉桂）

  - [七虹香電擊作戰](../Page/七虹香電擊作戰.md "wikilink")（淑女）

  - [花右京女侍隊 La Verite](../Page/花右京女侍隊.md "wikilink")（劍近衛、導師）

  - （店員）

  - [HELLSING](../Page/Hellsing.md "wikilink")（海蓮娜）

  - 神奇寶貝（）

  - [魔法戰士李維](../Page/魔法戰士李維.md "wikilink")（莉莉）

  - 名偵探柯南（石原亞紀）

<!-- end list -->

  - 2002年

<!-- end list -->

  - [青出於藍](../Page/青出於藍_\(漫畫\).md "wikilink")（**神樂崎雅**）
  - [笑園漫畫大王](../Page/笑園漫畫大王.md "wikilink")（谷崎由佳里）
  - [攻殻機動隊 STAND ALONE
    COMPLEX](../Page/攻殻機動隊_STAND_ALONE_COMPLEX.md "wikilink")（辻崎沙織）
  - [十二國記](../Page/十二國記.md "wikilink")（清秀）
  - 麵包超人（〈第3代〉）
  - [.hack//SIGN](../Page/.hack/SIGN.md "wikilink")（BT）
  - [爆鬥宣言大鋼彈](../Page/爆鬥宣言大鋼彈.md "wikilink")（蒂芬妮·布瑞克法斯特）
  - [超齡細公主](../Page/超齡細公主.md "wikilink")（雷納）
  - [彩夢芭蕾](../Page/彩夢芭蕾.md "wikilink")（伊蝶爾小姐）
  - [YOU'RE UNDER ARREST
    ～逮捕令～](../Page/逮捕令_\(動畫\)#特別篇2.md "wikilink")（**小早川美幸**\[9\]）
  - [翼神世音](../Page/翼神世音.md "wikilink")（如月樹〈小時候〉）

<!-- end list -->

  - 2003年

<!-- end list -->

  - [青出於藍 ～緣～](../Page/青出於藍_\(漫畫\).md "wikilink")（**神樂崎雅**）

  - （伊歐）

  - [原子小金剛 (2003年版)](../Page/鐵臂阿童木_\(2003年動畫\).md "wikilink")（）

  - [E'S OTHERWISE](../Page/星際少年隊.md "wikilink")（瑪莉亞、艾尼耶爾）

  - （久保千鶴）

  - [偵探學園Q](../Page/偵探學園Q.md "wikilink")（大鳥真弓）

  - [人間交差點](../Page/人間交差點.md "wikilink")（田村）

  - [驚爆危機？校園篇](../Page/驚爆危機#驚爆危機？校園篇（ふもっふ）.md "wikilink")（若菜陽子）

  - [冒險遊記Pluster World](../Page/冒險遊記Pluster_World.md "wikilink")（香塔公主）

  - [魔法使的條件](../Page/魔法使的條件.md "wikilink")（岩下美由紀〈米琳達〉）

<!-- end list -->

  - 2004年

<!-- end list -->

  - （、、 等）

  - [詩∽片](../Page/詩∽片.md "wikilink")（白坂志穗）

  - [吟遊默示錄](../Page/吟遊默示錄.md "wikilink")（[伊絲塔](../Page/伊絲塔.md "wikilink")）

  - [Keroro軍曹](../Page/Keroro軍曹_\(動畫\).md "wikilink")（****、**[Giroro伍長〈幼年時期〉／小Giroro](../Page/K隆星人#K隆軍人.md "wikilink")**、日向秋奈〈少女時期〉、星人、、、電視播報）

  - [花右京女侍隊 La Verite](../Page/花右京女侍隊.md "wikilink")（劍近衛）

  - [光與水的女神](../Page/光與水的女神.md "wikilink")（水樹美恵子）

  - [神奇寶貝超世代](../Page/神奇寶貝超世代.md "wikilink")（**[武藏](../Page/精靈寶可夢動畫角色列表#火箭隊兩人一貓組.md "wikilink")**〈代替〉）

  - （老師、教務主任、）

  - [ONE PIECE](../Page/ONE_PIECE_\(動畫\).md "wikilink")（柯芭特）

<!-- end list -->

  - 2005年

<!-- end list -->

  - [我的太太是魔法少女](../Page/我的太太是魔法少女.md "wikilink")（美杉紀／瑪拉·艾克斯）
  - [極上生徒會](../Page/極上生徒會.md "wikilink")（平田若菜）
  - [JINKI:EXTEND](../Page/人機系列.md "wikilink")（西巴）
  - [TSUBASA翼](../Page/TSUBASA翼.md "wikilink")（嵐）
  - [BLEACH](../Page/BLEACH_\(動畫\).md "wikilink")（志波空鶴）

<!-- end list -->

  - 2006年

<!-- end list -->

  - [我們這一家](../Page/我們這一家.md "wikilink")（坂田愛）

  - （克里克）

  - [砂沙美☆魔法少女俱樂部](../Page/砂沙美魔法少女俱樂部.md "wikilink")（小塚鷲羽）

  - TSUBASA翼 (第2期)（嵐）

  - （波爾多）

  - [NANA](../Page/NANA.md "wikilink")（小松奈津子）

  - [南瓜剪刀](../Page/南瓜剪刀.md "wikilink")（韋柏納）

  - [貧窮姊妹物語](../Page/貧窮姊妹物語.md "wikilink")（三枝蘭子）

  - [MAJOR 2nd season](../Page/棒球大聯盟.md "wikilink")（佐藤壽也的導師）

<!-- end list -->

  - 2007年

<!-- end list -->

  - [CODE-E](../Page/CODE-E.md "wikilink")（海老原美月）
  - [地獄少女 二籠](../Page/地獄少女.md "wikilink")（武田里奈）
  - [逮捕令 全速前進](../Page/逮捕令_\(動畫\)#第3期.md "wikilink")（**小早川美幸**\[10\]）
  - [交響情人夢](../Page/交響情人夢_\(動畫\).md "wikilink")（江藤佳緒里\[11\]）

<!-- end list -->

  - 2008年

<!-- end list -->

  - [女神異聞錄 ～三位一體之魂～](../Page/女神異聞錄_～三位一體之魂～.md "wikilink")

<!-- end list -->

  - 2009年

<!-- end list -->

  - [元素高手](../Page/元素高手.md "wikilink")（安·卡拉斯）

  - （）

  - [旋風管家！](../Page/旋風管家_\(動畫\).md "wikilink")（鷺之宮初穗）

  - [FRESH光之美少女！](../Page/FRESH光之美少女！.md "wikilink")（茱莉亞諾）

<!-- end list -->

  - 2011年

<!-- end list -->

  - Keroro軍曹（電視播報）
  - [偶像大師](../Page/偶像大師_\(動畫\).md "wikilink")（如月千種）
  - [魔法禁書目錄II](../Page/魔法禁書目錄.md "wikilink")（前方的風）
  - [日常](../Page/日常.md "wikilink")（）

<!-- end list -->

  - 2012年

<!-- end list -->

  - [旋風管家！CAN'T TAKE MY EYES OFF
    YOU](../Page/旋風管家_\(動畫\).md "wikilink")（鷺之宮初穗）

  - 名偵探柯南（大橋彩代）

  - （）

<!-- end list -->

  - 2013年

<!-- end list -->

  - [來自新世界](../Page/來自新世界.md "wikilink")（偽擬蓑白）
  - [斷裁分離的罪惡之剪](../Page/斷裁分離的罪惡之剪.md "wikilink")（美墨賢的母親）
  - [悠悠哉哉少女日和](../Page/悠悠哉哉少女日和.md "wikilink")（越谷雪子）
  - [八犬傳 -東方八犬異聞- 第二期](../Page/八犬傳－東方八犬異聞－.md "wikilink")（大黑堂·妻）
  - [旋風管家！Cuties](../Page/旋風管家_\(動畫\).md "wikilink")（鷺之宮初穗）

<!-- end list -->

  - 2014年

<!-- end list -->

  - [KERORO
    ～keroro～](../Page/Keroro軍曹_\(動畫\).md "wikilink")（**日向秋**\[12\]）

<!-- end list -->

  - 2015年

<!-- end list -->

  - [悠悠哉哉少女日和 Repeat](../Page/悠悠哉哉少女日和.md "wikilink")（越谷雪子、貝奇）

<!-- end list -->

  - 2016年

<!-- end list -->

  - [她與她的貓 -Everything Flows-](../Page/她與她的貓.md "wikilink")（**母親**）

<!-- end list -->

  - 2017年

<!-- end list -->

  - [側車搭檔](../Page/側車搭檔.md "wikilink")（宮田百合的媽媽）

<!-- end list -->

  - 2018年

<!-- end list -->

  - 魔法禁書目錄Ⅲ（前方的風）
  - [刀劍神域 Alicization](../Page/刀劍神域.md "wikilink")（阿滋利卡）

### OVA

  - 1986年

<!-- end list -->

  - （直美）

<!-- end list -->

  - 1987年

<!-- end list -->

  - [吹泡糖危機](../Page/吹泡糖危機.md "wikilink")（妮妮·羅馬諾瓦）

  - （知世）

  - [微笑標的](../Page/微笑標的.md "wikilink")（女學生B）

<!-- end list -->

  - 1991年

<!-- end list -->

  - （博子）

  - （瑪布兒）

<!-- end list -->

  - 1992年

<!-- end list -->

  - [萬能文化貓娘](../Page/萬能文化貓娘.md "wikilink")（小板橋今日子）

  - （雛子）

<!-- end list -->

  - 1993年

<!-- end list -->

  - （**艾蕾因·里德／黛安娜·里德**）

  - （白瀨真織）

<!-- end list -->

  - 1994年

<!-- end list -->

  - （）

  - （由貴）

  - [湘南純愛組！](../Page/湘南純愛組！.md "wikilink")（藤崎志乃美）

  - [逮捕令](../Page/逮捕令_\(動畫\)#OVA.md "wikilink")（**小早川美幸**）

  - （山口春美）

<!-- end list -->

  - 1995年

<!-- end list -->

  - （樹雨）

  - （凱西／凱特）

  - [機械女神R](../Page/機械女神.md "wikilink")（**紅莓**）

  - [戰少女](../Page/戰少女.md "wikilink")（克羅絲）

<!-- end list -->

  - 1997年

<!-- end list -->

  - [AIKa](../Page/海底嬌娃藍華.md "wikilink")（〈副官〉）

  - [機械女神J again](../Page/機械女神.md "wikilink")（**紅莓**）

  - [鋼鐵神兵NEO](../Page/鋼鐵神兵.md "wikilink")（鳳·拉斐內〈少年時期〉）

  - （布朗）

<!-- end list -->

  - 1998年

<!-- end list -->

  - [萬能文化貓娘DASH！](../Page/萬能文化貓娘.md "wikilink")（有吉今日子）

<!-- end list -->

  - 2001年

<!-- end list -->

  - [花右京女侍隊](../Page/花右京女侍隊.md "wikilink")（劍近衛）
  - [魔神皇帝](../Page/魔神皇帝.md "wikilink")（蘿莉）

<!-- end list -->

  - 2002年

<!-- end list -->

  - [.hack//GIFT](../Page/.hack/SIGN.md "wikilink")（BT）
  - [魔法護士小麥](../Page/魔法護士小麥.md "wikilink")（桐原夕映）

<!-- end list -->

  - 2003年

<!-- end list -->

  - [魔神皇帝 死鬥！暗黑大將軍](../Page/魔神皇帝.md "wikilink")（蘿莉）

<!-- end list -->

  - 2004年

<!-- end list -->

  - （久保千鶴）

  - 魔法護士小麥Z（桐原夕映）

<!-- end list -->

  - 2005年

<!-- end list -->

  - STRATOS 4 Advance（久保千鶴）

<!-- end list -->

  - 2006年

<!-- end list -->

  - STRATOS 4 Advance 完結篇（久保千鶴）

<!-- end list -->

  - 2007年

<!-- end list -->

  - [萌少女的戀愛時光 ～休息時間～](../Page/萌少女的戀愛時光.md "wikilink")（青木大介的母親）

<!-- end list -->

  - 2009年

<!-- end list -->

  - [DARKER THAN BLACK -黑之契約者-
    外傳](../Page/DARKER_THAN_BLACK.md "wikilink")（克羅德）

<!-- end list -->

  - 2014年

<!-- end list -->

  - [旋風管家](../Page/旋風管家_\(動畫\).md "wikilink")（鷺之宮初穗）\[13\]

<!-- end list -->

  - 2016年

<!-- end list -->

  - [悠悠哉哉少女日和 Repeat](../Page/悠悠哉哉少女日和.md "wikilink")（越谷雪子、佩琪）\[14\]

### 電影動畫

  - 1988年

<!-- end list -->

  - [龍貓](../Page/龍貓.md "wikilink")（車掌小姐）

<!-- end list -->

  - 1994年

<!-- end list -->

  - （**赤木晴子**）

  - （**赤木晴子**）

<!-- end list -->

  - 1995年

<!-- end list -->

  - （**赤木晴子**）

  - （**赤木晴子**）

<!-- end list -->

  - 1996年

<!-- end list -->

  - [秀逗魔導士
    RETURN](../Page/秀逗魔導士_\(動畫\)#Slayers_RETURN.md "wikilink")（賽莉娜）
  - [勇者鬥惡龍 羅德的紋章](../Page/勇者鬥惡龍_羅德的紋章.md "wikilink")（**亞魯**）

<!-- end list -->

  - 1997年

<!-- end list -->

  - [劇場版 甜心戰士F](../Page/甜心戰士.md "wikilink")（葉月聖羅）

<!-- end list -->

  - 1999年

<!-- end list -->

  - [劇場版 神奇寶貝：夢幻之神奇寶貝
    洛奇亞爆誕](../Page/神奇寶貝劇場版：夢幻之神奇寶貝_洛奇亞爆誕.md "wikilink")（芙蘆拉）

  - [劇場版 超級娃娃戰士★莉卡：麗佳絕體絕命！奇蹟的娃娃騎士](../Page/麗佳公主.md "wikilink")（）

  - [逮捕令 the MOVIE](../Page/逮捕令_\(動畫\)#劇場版.md "wikilink")（**小早川美幸**）

  - （藤原靜代〈12歲〉）

<!-- end list -->

  - 2001年

<!-- end list -->

  - [笑園漫畫大王 The Movie](../Page/笑園漫畫大王#劇場版.md "wikilink")（谷崎由佳里）

<!-- end list -->

  - 2002年

<!-- end list -->

  - （**帕姆**）

<!-- end list -->

  - 2006年

<!-- end list -->

  - [超劇場版Keroro軍曹](../Page/超劇場版_Keroro軍曹.md "wikilink")（日向秋）

<!-- end list -->

  - 2007年

<!-- end list -->

  - [福音戰士新劇場版：序](../Page/福音戰士新劇場版：序.md "wikilink")
  - [超劇場版Keroro軍曹2：深海的公主](../Page/超劇場版_Keroro軍曹_2_深海的公主.md "wikilink")（日向秋）
  - 小Kero K隆球的秘密\!?（**小Giroro**）

<!-- end list -->

  - 2008年

<!-- end list -->

  - [超劇場版Keroro軍曹3：Keroro VS Keroro
    天空大決戰](../Page/超劇場版_Keroro軍曹_3_Keroro_VS_Keroro_天空大決戰.md "wikilink")（日向秋）

<!-- end list -->

  - 2009年

<!-- end list -->

  - [超劇場版Keroro軍曹4：逆襲的龍勇士](../Page/超劇場版_Keroro軍曹_4_逆襲的龍勇士.md "wikilink")（**日向秋**）

<!-- end list -->

  - 2010年

<!-- end list -->

  - [超劇場版Keroro軍曹5：誕生！究極Keroro奇蹟的時空之島\!\!](../Page/超劇場版_Keroro軍曹_5_誕生！究極Keroro奇蹟的時空之島！！.md "wikilink")（日向秋）

<!-- end list -->

  - 2013年

<!-- end list -->

  - [劇場版 怪傑佐羅利：守護恐龍蛋](../Page/怪俠佐羅利.md "wikilink")（猩猩媽媽）

<!-- end list -->

  - 2014年

<!-- end list -->

  - （正也的母親）

<!-- end list -->

  - 2016年

<!-- end list -->

  - [劇場版 聲之形](../Page/電影版_聲之形.md "wikilink")（西宮家的母親〈西宮八重子〉）

### 網路動畫

  - 2000年

<!-- end list -->

  - [笑園漫畫Web大王](../Page/笑園漫畫大王#網路動畫.md "wikilink")（黑澤美奈茂）

### 遊戲

  - 系列（**吉尼**）

<!-- end list -->

  - 1992年

<!-- end list -->

  - [山村美紗推理劇場 ～金盞花 京繪皿殺人事件～](../Page/山村美紗.md "wikilink")（小早川優子）

<!-- end list -->

  - 1993年

<!-- end list -->

  - （**吉澤弓美**）

  - [夢幻模擬戰 ～光輝的末裔～](../Page/夢幻模擬戰系列.md "wikilink")（克里斯）

<!-- end list -->

  - 1995年

<!-- end list -->

  - （西野繪利佳）

<!-- end list -->

  - 1996年

<!-- end list -->

  - 美少女雀士II Limited（西野繪利佳）

  - 時空探偵DD ～幻～（雷妮·拉瓦爾）

  - [Der Langrisser FX](../Page/夢幻模擬戰II.md "wikilink")（光之女神露西里斯）

  - （、）

  - （佐奈惠）

<!-- end list -->

  - 1997年

<!-- end list -->

  - （）

  - （橋本枝奈）

  - ～青海少女～

  - （**-{鮎}-川螢**）

<!-- end list -->

  - 1998年

<!-- end list -->

  - [星際牛仔](../Page/星際牛仔#電玩版.md "wikilink")（瑪莉亞）\[15\]

  - （小糸亞弓）

  - [雙面嬌娃](../Page/雙面嬌娃_\(遊戲\).md "wikilink")（**赤坂美月**、志穗）

  - 2（神條摩樹奈）

  - （桐生綾乃）

<!-- end list -->

  - 1999年

<!-- end list -->

  - （塞絲·葛拉地）

  - [私立正義學園 熱血青春日記2](../Page/私立正義學園#太陽學園高校部.md "wikilink")（響蘭）

  - （索菲雅）

  - （志野）

  - 街 ～命運的交差點～（小糸亞弓）

<!-- end list -->

  - 2000年

<!-- end list -->

  - （[伊絲塔](../Page/伊絲塔.md "wikilink")）

  - 燃燒吧！正義學園（響蘭）

<!-- end list -->

  - 2001年

<!-- end list -->

  - （北川真奈瀨）

<!-- end list -->

  - 2002年

<!-- end list -->

  - （電腦聲音）

  - （谷崎由佳里）

  - （番原美香）

  - （上代麻里）

  - [命運傳奇2](../Page/命運傳奇2.md "wikilink")（**哈洛露特·貝理賽利歐斯**）

  - （青柳千歳）

<!-- end list -->

  - 2003年

<!-- end list -->

  - [笑園漫畫大王Advance](../Page/笑園漫畫大王.md "wikilink")（谷崎由佳里）

  - [一擊殺虫\!\! 小惠惠](../Page/一擊殺虫！！小惠惠.md "wikilink")（**小惠惠姊姊**）\[16\]

  - （米莉）

<!-- end list -->

  - 2004年

<!-- end list -->

  - （日向秋）

<!-- end list -->

  - 2005年

<!-- end list -->

  - （日向秋）

  - [極上生徒會](../Page/極上生徒會.md "wikilink")（平田若菜）

  - 雙面嬌娃 攜帶版（**赤坂美月**、志穂）\[17\]

  - MEDICAL91（美-{咲}-千歲）

<!-- end list -->

  - 2006年

<!-- end list -->

  - [時空幻境 世界傳奇 閃耀神話](../Page/世界傳奇_光明神話.md "wikilink")（哈洛露特·貝理賽利歐斯）

  - [.hack//G.U.](../Page/.hack/G.U._\(遊戲\).md "wikilink")（波爾多）

  - （瀧澤玉惠）\[18\]

  - 街 ～命運的交差點～ 特別篇（小糸亞弓）

<!-- end list -->

  - 2007年

<!-- end list -->

  - [Keroro軍曹演習啦！全員集合2](../Page/Keroro軍曹_\(動畫\).md "wikilink")（日向秋）

<!-- end list -->

  - 2008年

<!-- end list -->

  - （日向秋）

  - 第一卷·崇（公由春子）

<!-- end list -->

  - 2009年

<!-- end list -->

  - （哈洛露特·貝理賽利歐斯）

  - （哈洛露特·貝理賽利歐斯）

<!-- end list -->

  - 2010年

<!-- end list -->

  - [.hack//Link](../Page/.hack/Link.md "wikilink")（BT、波爾多）

<!-- end list -->

  - 2011年

<!-- end list -->

  - [第2次超級機器人大戰Z 破界篇](../Page/第2次超級機器人大戰Z.md "wikilink")（桃井伊吹）

  - （哈洛露特·貝理賽利歐斯）

<!-- end list -->

  - 2012年

<!-- end list -->

  - 第2次超級機器人大戰Z 再世篇（桃井伊吹）

  - （莉莉\[19\]）

<!-- end list -->

  - 2014年

<!-- end list -->

  - [第3次超級機器人大戰Z 時獄篇](../Page/第3次超級機器人大戰Z.md "wikilink")（桃井伊吹）

  - （哈洛露特·貝理賽利歐斯\[20\]）

<!-- end list -->

  - 2015年

<!-- end list -->

  - 第3次超級機器人大戰Z 天獄篇（桃井伊吹）
  - 暮蟬悲鳴時粹（公由春子）

### 外語配音

※作品依英文名稱及英文原名排序。

#### 電影

  - [獨領風騷](../Page/獨領風騷.md "wikilink")（**Tai Frasier**〈[Brittany
    Murphy](../Page/布蘭妮·墨菲.md "wikilink") 主演〉）

  - （Candi〈[Sarah O'Hare](../Page/莎拉·梅鐸.md "wikilink") 飾演〉）

  - [血魔](../Page/血魔.md "wikilink")（Barbara、受理人員）※[TBS版](../Page/TBS電視台.md "wikilink")。

  - （Stacy〈[Jennifer Tilly](../Page/珍妮佛·提莉.md "wikilink") 飾演〉）

  - ※[朝日電視台版](../Page/朝日電視台.md "wikilink")。

#### 動畫

  - [X戰警
    (1994年東京電視台版)](../Page/X戰警.md "wikilink")（[Jubilee](../Page/歡歡.md "wikilink")）

### 廣播

  - 平松晶子的東京SHADOW

  - 通3番地

  - 奇蹟山脈

  - （嘉賓出演）

  - 平松晶子的VOICE OF WONDERLAND

### CD

  - [戰少女](../Page/戰少女.md "wikilink")（克羅絲、女性顧客B）

  - （托帕絲）

  - [Weiß kreuz Dramatic Collection III Kaleidoscope
    Memory](../Page/白色十字架.md "wikilink")（美樹）

  - HAPPINESS（迷你專輯）

  - PAIN（專輯）

  - [英雄傳說IV 朱紅的淚](../Page/英雄傳說IV_朱紅的淚.md "wikilink") 廣播劇CD（露蒂斯）

  - 形象專輯 （樹雨）

  - 音盤（村上百合子）

  - 音盤（頭·、·W、·N）

  - [Keroro軍曹](../Page/Keroro軍曹.md "wikilink") 地球（）侵略CD（日向秋）

  - （朱音夏）

  -
  - （戶川茅惠香）

  - 廣播劇CD [WILD ARMS](../Page/狂野歷險_\(遊戲\).md "wikilink")（／）

  - （鬼瓦）

  - 廣播劇CD [SNOW](../Page/SNOW.md "wikilink")（雪月小夜里）

  - 廣播劇CD [貧窮姊妹物語](../Page/貧窮姊妹物語.md "wikilink")（三枝蘭子）

  - 廣播劇CD「[洛克人千鈞一髪](../Page/洛克人系列.md "wikilink")」（蘿露）

  - [最终神話戰爭伊特奥佩拉](../Page/大前茜#最终神話戰爭伊特奥佩拉.md "wikilink") 原聲廣播劇CD 第1章
    罅割（）

  - Top of the World（Jambalaya, ）

  - 廣播劇CD（葛城里奈）

  - （尤莉希卡）

### 廣告

  - [萬代](../Page/萬代.md "wikilink")『[SLAM
    DUNK](../Page/灌籃高手.md "wikilink") 』
    與作品主角[櫻木花道的聲優](../Page/櫻木花道.md "wikilink")[草尾毅共同](../Page/草尾毅.md "wikilink")

### 其它

  - （購物中心賣場廣播）

  - [傳奇系列祭](../Page/傳奇系列.md "wikilink") 2008

  - [傳奇系列祭](../Page/傳奇系列.md "wikilink") 2008

## 相關人物

  - [林原惠](../Page/林原惠.md "wikilink")

  - [白鳥由里](../Page/白鳥由里.md "wikilink")

  - [岩永哲哉](../Page/岩永哲哉.md "wikilink")

  - [久川綾](../Page/久川綾.md "wikilink")

  - [三木真一郎](../Page/三木真一郎.md "wikilink")

  -
## 腳註

### 注釋

<references group="注"/>

### 來源

## 外部連結

  - [賢Production公式官網的聲優簡介](http://www.kenproduction.co.jp/talent/member.php?mem=w47)


[Category:日本女性配音員](../Category/日本女性配音員.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")
[Category:賢Production](../Category/賢Production.md "wikilink")

1.
2.
3.  倉田幸雄編「今友WA\!\!（平松晶子→？）」《AnimeV》1993年11月號[學習研究社](../Page/學研控股.md "wikilink")，1993年10月1日，第62頁
    雑誌01591-10

4.  初演主要角色作品。

5.

6.

7.

8.

9.

10.

11.

12.

13. 隨著單行本第行本第43冊特裝版的發行附贈OVA。

14. 隨著單行本第10冊特裝版的發行附贈OAD。

15. [PS版](../Page/PlayStation.md "wikilink")。

16. [PS2版](../Page/PlayStation_2.md "wikilink")。

17. [PSP版](../Page/PlayStation_Portable.md "wikilink")。

18.
19.

20.