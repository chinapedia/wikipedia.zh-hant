[Dongshandiaohualou.jpg](https://zh.wikipedia.org/wiki/File:Dongshandiaohualou.jpg "fig:Dongshandiaohualou.jpg")\]\]
[雕刻大楼.JPG](https://zh.wikipedia.org/wiki/File:雕刻大楼.JPG "fig:雕刻大楼.JPG")
[苏州东山镇雕花楼格子门.JPG](https://zh.wikipedia.org/wiki/File:苏州东山镇雕花楼格子门.JPG "fig:苏州东山镇雕花楼格子门.JPG")
[东山镇.JPG](https://zh.wikipedia.org/wiki/File:东山镇.JPG "fig:东山镇.JPG")
**东山镇**是[中国](../Page/中国.md "wikilink")[江苏省](../Page/江苏省.md "wikilink")[蘇州市](../Page/蘇州市.md "wikilink")[吴中区下辖的一个镇](../Page/吴中区.md "wikilink")，位于苏州西南24公里处[太湖的一个半岛上](../Page/太湖.md "wikilink")，又称**洞庭东山**。

洞庭东山原系太湖中小岛，[元](../Page/元.md "wikilink")、[明后开始与陆地连成半岛](../Page/明.md "wikilink")；原属江苏吴县。2001年吴县并入苏州成为吴中区及相城区，东山镇一并划入。东山镇面积97平方公里，人口五万余，产业以[唐代贡品洞庭红桔](../Page/唐代.md "wikilink")、清代贡品[碧螺春茶和太湖](../Page/碧螺春.md "wikilink")[大闸蟹最著名](../Page/大闸蟹.md "wikilink")，东山杨梅及白沙枇杷也相当著名。苏州和东山间有直达公共汽车，还有一条观光专用的湖滨大道；东山正在发展成一个休闲度假区。著名的景点包括有众多保存完好的明清古建筑的[陆巷村](../Page/陆巷村.md "wikilink")、可登高眺望太湖的[莫厘峰](../Page/莫厘峰.md "wikilink")(海拔293米)、[紫金庵](../Page/紫金庵.md "wikilink")、[轩辕宫](../Page/轩辕宫.md "wikilink")、湖畔园林[启园和号称](../Page/启园.md "wikilink")“江南第一楼”的雕花楼——[春在楼](../Page/春在楼.md "wikilink")。而雕花楼以造型独特曾作为[好莱坞](../Page/好莱坞.md "wikilink")[环球电影公司的](../Page/环球电影公司.md "wikilink")《[庭院里的女人](../Page/庭院里的女人.md "wikilink")》、电视剧《[宰相刘罗锅](../Page/宰相刘罗锅.md "wikilink")》等上百部电影、电视剧的场景。

“洞庭山”在[唐代属](../Page/唐代.md "wikilink")[苏州府](../Page/苏州.md "wikilink")[长州县](../Page/长州.md "wikilink")，见[陆羽](../Page/陆羽.md "wikilink")《[茶经](../Page/茶经.md "wikilink")》：“苏州长州生洞庭山”。“洞庭山”也常见唐代诗中，例如[白乐天](../Page/白乐天.md "wikilink")《守姑苏游太湖诗》：*“十只画舡何处宿，洞庭山脚太湖心”*；[张松龄](../Page/张松龄.md "wikilink")《渔父》：“*乐是风波钓是闲，草堂松桧已胜攀。太湖水，洞庭山，狂风浪起且须还*。

[赵彦卫](../Page/赵彦卫.md "wikilink")《[云麓漫钞](../Page/云麓漫钞.md "wikilink")》：“洞庭山有山水之分，[吴中太湖内乃洞庭山](../Page/吴中.md "wikilink")，产柑桔，香味胜绝。”

1995年，东山镇被列为第一批江苏省历史文化名镇。

## 外部链接

[蘇州 东山](http://www.szdsw.cn)

[苏](../Category/中国历史文化名镇.md "wikilink")
[吴中区](../Category/苏州建制镇.md "wikilink")
[Category:吴中区行政区划](../Category/吴中区行政区划.md "wikilink")