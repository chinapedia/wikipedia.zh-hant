[Yang_Heling.jpg](https://zh.wikipedia.org/wiki/File:Yang_Heling.jpg "fig:Yang_Heling.jpg")

**楊鶴齡**（），字**禮遐**。[廣東](../Page/廣東.md "wikilink")[香山縣](../Page/香山縣.md "wikilink")[翠亨村人](../Page/翠亨村.md "wikilink")。香港商人，中国民主革命家。

## 生平

[Si_Da_Kou.jpg](https://zh.wikipedia.org/wiki/File:Si_Da_Kou.jpg "fig:Si_Da_Kou.jpg")（1890年攝于香港）。前排左起：楊鶴齡、[孫中山](../Page/孫中山.md "wikilink")、[陳少白](../Page/陳少白.md "wikilink")、[尢列](../Page/尢列.md "wikilink")。後立者為[關景良並不是四大寇之一](../Page/關景良.md "wikilink")。\]\]
楊鶴齡生于[澳门](../Page/澳门.md "wikilink")。他的老家[翠亨村也是](../Page/翠亨村.md "wikilink")[孙中山](../Page/孙中山.md "wikilink")、[陆皓东的老家](../Page/陆皓东.md "wikilink")，三个人自幼便认识。1886年，他入[廣州算學館](../Page/廣州算學館.md "wikilink")，和[尢列是同学](../Page/尢列.md "wikilink")。\[1\]1888年他毕业后到香港，住在父亲开设的[上環](../Page/上環.md "wikilink")“[楊耀記](../Page/楊耀記.md "wikilink")”店舖。\[2\]1890年前后，他與[孫中山](../Page/孫中山.md "wikilink")、[陳少白](../Page/陳少白.md "wikilink")、[尢列等人於](../Page/尢列.md "wikilink")“[楊耀記](../Page/楊耀記.md "wikilink")”內密談反清革命，時人稱之為「[四大寇](../Page/四大寇.md "wikilink")」。\[3\]1892年在[澳門](../Page/澳門.md "wikilink")，[孫中山向](../Page/孫中山.md "wikilink")[鏡湖醫院借钱兴辦](../Page/鏡湖醫院.md "wikilink")[中西藥局](../Page/中西藥局.md "wikilink")，“擔保還銀人”是[澳門紳商](../Page/澳門.md "wikilink")、楊鶴齡的妹夫[吳節薇](../Page/吳節薇.md "wikilink")。後来，[孫中山因在澳門发展不顺利](../Page/孫中山.md "wikilink")，遂到[廣州及](../Page/廣州.md "wikilink")[香山](../Page/香山縣.md "wikilink")[石岐開藥局](../Page/石岐区.md "wikilink")，據楊鶴齡的儿子[楊國鏗回憶](../Page/楊國鏗.md "wikilink")，當時楊鶴齡将[澳門](../Page/澳門.md "wikilink")[龍嵩街的一座房子卖给了](../Page/龍嵩街.md "wikilink")[吳節薇](../Page/吳節薇.md "wikilink")，将所得钱款全部贈与孫中山發展事業。1895年，他加入[興中會](../Page/興中會.md "wikilink")。後來，孫中山接连發動反清起義，楊鶴齡在[香港和](../Page/香港.md "wikilink")[澳门籌募革命經費并宣传反清](../Page/澳门.md "wikilink")，还曾经在[陳少白創辦的](../Page/陳少白.md "wikilink")《[中國日報](../Page/中國日報.md "wikilink")》社任職。\[4\]

1912年[南京临时政府成立](../Page/南京临时政府.md "wikilink")，他任总统府秘书，同年4月他随临时大总统[孙中山解职而去职](../Page/孙中山.md "wikilink")。\[5\]此后，他居住在澳門，並把住宅命名為“楊四寇堂”，纪念[四大寇时期](../Page/四大寇.md "wikilink")。\[6\]1919年5月16日楊鶴齡致信孫中山求職，被孙中山婉拒。1920年，他再次致信孫中山求職，但再次被孙中山拒绝。\[7\]1921年9月，孫中山在[廣州就任](../Page/廣州.md "wikilink")[非常大總統](../Page/非常大總統.md "wikilink")，9月14日楊鶴齡被聘為總統府顧問，並每月获養老金。孙中山还命[许崇智拨款整修了](../Page/许崇智.md "wikilink")[越秀山南麓的](../Page/越秀山.md "wikilink")[文瀾閣并更名为](../Page/文澜阁_\(越秀山\).md "wikilink")“三老楼”，請楊鶴齡、[陳少白](../Page/陳少白.md "wikilink")、[尢列入住](../Page/尢列.md "wikilink")。1923年，孫中山在[廣州設立](../Page/廣州.md "wikilink")[陸海軍大元帥大本營](../Page/陸海軍大元帥大本營.md "wikilink")，4月4日楊鶴齡被任命为為港澳特務調查員。1925年[孙中山逝世](../Page/孙中山.md "wikilink")，他再次退隐于[澳门](../Page/澳门.md "wikilink")。\[8\]\[9\]\[10\]

1934年8月，他因[脑溢血病逝于](../Page/脑溢血.md "wikilink")[澳門](../Page/澳門.md "wikilink")，享年67岁。后葬於[翠亨村南的](../Page/翠亨村.md "wikilink")[金檳榔山](../Page/金檳榔山.md "wikilink")。\[11\]\[12\]\[13\]

## 参考文献

[Category:中華民國大陸時期政治人物](../Category/中華民國大陸時期政治人物.md "wikilink")
[Category:香山县人 (中山)](../Category/香山县人_\(中山\).md "wikilink")
[Category:澳門人](../Category/澳門人.md "wikilink")
[H](../Category/杨姓.md "wikilink")

1.  [楊鶴齡，孙中山故居纪念馆，于2012-2-17查阅](http://www.sunyat-sen.org:1980/b5/192.168.0.100/cuiheng/show3.php?id=84)


2.  [王奋强，杨鹤龄：忠贞不贰的“四寇堂主”
    (4)，中国共产党新闻网，2011年10月21日](http://dangshi.people.com.cn/GB/15973999.html)

3.
4.
5.  [杨鹤龄，辛亥革命网，2010年12月29日](http://www.xhgmw.org/archive-50898.shtml)

6.
7.
8.
9.
10.
11.
12.
13.