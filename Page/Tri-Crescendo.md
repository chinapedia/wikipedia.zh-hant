**株式會社tri-Crescendo**（**tri-Crescendo
Inc.**）是一間[電子遊戲軟件開發公司](../Page/電子遊戲.md "wikilink")，於1999年2月成立。現時總社位於[東京都](../Page/東京都.md "wikilink")[目黑區](../Page/目黑區.md "wikilink")。主要進行[tri-Ace遊戲音樂相關的開發](../Page/tri-Ace.md "wikilink")，現在亦有獨自開發遊戲。

## 沿革

**tri-Crescendo**由隸屬[tri-Ace的](../Page/tri-Ace.md "wikilink")[初芝弘也成為代表成立](../Page/初芝弘也.md "wikilink")，資本為1000萬日元。工作人員總數40名（2006年4月現在，除去幹事）。於2002年在年[目黑區開設開發室](../Page/目黑區.md "wikilink")，2004年總社也遷移至目黑區。

與[tri-Ace有很多共同開發](../Page/tri-Ace.md "wikilink")，但不是[tri-Ace的子公司而是獨立的公司](../Page/tri-Ace.md "wikilink")，所以亦有跟其他公司合作。

## 概要

**tri-Crescendo**成立後，從「[女神戰記](../Page/女神戰記.md "wikilink")」開始，[tri-Ace開發遊戲的音樂部分都由tri](../Page/tri-Ace.md "wikilink")-Crescendo開發。2002年與[MONOLITHSOFT共同開發](../Page/MONOLITHSOFT.md "wikilink")「[Baten
Kaitos系列](../Page/Baten_Kaitos.md "wikilink")」。在這個作品中，tri-Crescendo除了負責音樂之外，還負責戰鬥程式及系統等。

「[信賴鈴音 \~蕭邦之夢\~](../Page/信賴鈴音_~蕭邦之夢~.md "wikilink")」就是第一款獨自開發的遊戲。

## 開發作品

### 獨自開發

  - [信賴鈴音 \~蕭邦之夢\~](../Page/信賴鈴音_~蕭邦之夢~.md "wikilink")

### MONOLITHSOFT共同開發

  - [霸天开拓史 無終之翼與失落的海洋](../Page/霸天开拓史_無終之翼與失落的海洋.md "wikilink")
  - [霸天开拓史II 開端之翼與諸神的嗣子](../Page/霸天开拓史II_開端之翼與諸神的嗣子.md "wikilink")

### 音樂部分的開發

  - [女神戰記](../Page/女神戰記.md "wikilink")
  - [星海遊俠 Blue Sphere](../Page/星海遊俠_Blue_Sphere.md "wikilink")
  - [星海遊俠 Till the End of
    Time](../Page/星海遊俠_Till_the_End_of_Time.md "wikilink")
  - 星海遊俠 Till the End of Time Director's Cut
  - [Radiata Stories](../Page/Radiata_Stories.md "wikilink")
  - [女神戰記2：希爾梅莉亞](../Page/女神戰記2：希爾梅莉亞.md "wikilink")
  - [The FEAR](../Page/The_FEAR.md "wikilink")

## 外部連結

  - [株式會社tri-Crescendo官方網站](http://www.tri-crescendo.co.jp/)

[Category:1999年開業電子遊戲公司](../Category/1999年開業電子遊戲公司.md "wikilink")
[Category:電子遊戲開發公司](../Category/電子遊戲開發公司.md "wikilink")
[Category:日本電子遊戲公司](../Category/日本電子遊戲公司.md "wikilink")
[Category:目黑區](../Category/目黑區.md "wikilink")