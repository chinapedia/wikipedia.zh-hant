**國家文藝獎**，是[中華民國一個重要文藝獎項](../Page/中華民國.md "wikilink")。

## 歷史

國家文藝基金管理委員會成立於[1974年](../Page/1974年.md "wikilink")[5月1日](../Page/5月1日.md "wikilink")（[民國六十三年](../Page/民國.md "wikilink")），是中華民國政府為獎助優良文藝創作及各項文藝活動而設，除設置國家文藝獎以外，獎助文藝作品翻譯、文藝活動、文藝人才、文藝事業以及國際文藝交流等；獎助分12類、36獎項，得獎者可獲得獎金[新台幣](../Page/新台幣.md "wikilink")20萬元及金質獎章；自[1982年](../Page/1982年.md "wikilink")（第八屆）起，設立特別貢獻獎得獎人；自[1989年以後](../Page/1989年.md "wikilink")，獎金增至新台幣30萬元以至40萬元。

此獎項[1997年](../Page/1997年.md "wikilink")（民國八十六年）起改由[國家文化藝術基金會創辦於](../Page/國家文化藝術基金會.md "wikilink")1997年，原稱為「**國家文化藝術基金會文藝獎**」，官方及媒體稱「**文藝獎**」，以便與舊制「**國家文藝獎**」有所區別；但官方於[2001年經過](../Page/2001年.md "wikilink")[文建會更名為](../Page/文建會.md "wikilink")「**國家文藝獎**」，至此兩者名稱混同。

舊制已辦二十二屆，新制卻從第一屆開始。舊制與新制最大差別：舊制「類別與項目」較為細密完整，比如文學部分，舊制細分小說、散文、兒童文學、新詩、舊詩、歌詞、傳記文學、新聞文學、戲劇、文藝理論、電影劇本等，每屆申請人夠水準則頒發，無則從缺。新制則較重視終身成就，未必與當年成就有關，如文學類則僅一人得獎，為獎勵以生命力量堅持持續藝術創作，並積極創造獨特且具現代性的風格之藝文工作者。原分為[舞蹈](../Page/舞蹈.md "wikilink")、[戲劇](../Page/戲劇.md "wikilink")、[文學](../Page/文學.md "wikilink")、[美術](../Page/美術.md "wikilink")、[音樂五類](../Page/音樂.md "wikilink")，[2003年加入](../Page/2003年.md "wikilink")[電影及](../Page/電影.md "wikilink")[建築兩類](../Page/建築.md "wikilink")，並將獎金增至新台幣60萬元，2009年又修訂為每名得主獲得100萬元。舊制得獎人亦皆當代文學藝術碩彥，少數人在舊制與新制上重複得獎；舊制得獎名單有待補遺。

[2015年改為兩年一頒](../Page/2015年.md "wikilink")，文化部也要求國藝會在頒獎典禮部分，預算比例不得像過去幾屆過高，避免模糊挹注藝術資源的焦點。國藝會執行長陳錦誠表示：「會有這樣的變革，是因為考慮藝術創作的卓越，需要時間累積與淬鍊。」至於每隔一年舉辦國家文藝獎所省下的1500萬預算，將會回到其他補助機制，繼續挹注藝術文化。

為了推廣得獎者的創作思維與歷程，國家文化藝術基金會與民間[出版社合作出版每位得獎者的](../Page/出版社.md "wikilink")[傳記](../Page/傳記.md "wikilink")，並委託[公共電視文化事業基金會製作](../Page/公共電視文化事業基金會.md "wikilink")[紀錄片](../Page/紀錄片.md "wikilink")《文化容顏》，使社會大眾得以深刻瞭解他們的藝術生命。

## 歷屆得主

### 舊制

#### 舊制第一屆（1975年僅一人）

  - 美術類[郭儀](../Page/郭儀.md "wikilink")，攝影項目（作品：國家重大經濟建設）

#### 舊制第二屆（1976年六人）

  - 詩歌類[曾霽虹](../Page/曾霽虹.md "wikilink")，舊詩項目《五朝湘詩家史詠》，廣文書局，1974年11月
  - 新聞文學類[姚朋](../Page/姚朋.md "wikilink")（[彭歌](../Page/彭歌.md "wikilink")），雜文項目《成熟的時代》，[聯合報社](../Page/聯合報.md "wikilink")，1975年7月
  - 小說類[謝文玖](../Page/謝文玖.md "wikilink")（[謝霜天](../Page/謝霜天.md "wikilink")），長篇小說項目
    《梅村心曲》（秋夜、冬暮、春晨）
  - 美術類[朱川泰](../Page/朱川泰.md "wikilink")（[朱銘](../Page/朱銘.md "wikilink")），雕刻項目《同心協力》等作品
  - 戲劇類[張永祥](../Page/張永祥_\(編劇\).md "wikilink")，電視劇本項目《[寒流](../Page/寒流_\(影集\).md "wikilink")》
  - 戲劇類[丁善璽](../Page/丁善璽.md "wikilink")，電影劇本項目《[八百壯士](../Page/八百壮士_\(1975年电影\).md "wikilink")》

#### 舊制第三屆（1977年七項八人）

  - 文藝理論類[王禮卿](../Page/王禮卿.md "wikilink")，文藝批評項目《遺山論證詩證》，[台灣書店](../Page/台灣書店.md "wikilink")，1966年4月
  - 詩歌類[宋天正](../Page/宋天正.md "wikilink")，詞項目，《海曙詞》，廣文書局，1977年6月
  - 新聞文學類[趙玉明](../Page/趙玉明.md "wikilink")，新聞文學項目《飛向白日青天》，1977年8月
  - 美術類[曹緯初](../Page/曹緯初.md "wikilink")，書法項目《隸書朱子家訓》等作品
  - 美術類[陳霽](../Page/陳霽.md "wikilink")，攝影項目，《開創歷史的巨擘：國家重大建設剪影》
  - 音樂類[董榕森](../Page/董榕森.md "wikilink")，國樂項目，一、〈偉大的建設〉，二、〈奔〉
  - 戲劇類二人合得：[貢敏](../Page/貢敏.md "wikilink")（[貢宗耀](../Page/貢宗耀.md "wikilink")）、[趙琦彬](../Page/趙琦彬.md "wikilink")，電影劇本項目《風雨生信心》

#### 舊制第四屆（1978年六人）

  - 散文類[陳曉林](../Page/陳曉林.md "wikilink")，散文項《青青子衿》，言心出版社，1977年12月
  - 散文類[陳克環](../Page/陳克環.md "wikilink")，散文項《悠悠我思》，彥博出版社，1978年8月
  - 新聞文學類[荊瑞先](../Page/荊瑞先.md "wikilink")，新聞文學項《照妖鏡》，[黎明文化](../Page/黎明文化.md "wikilink")，1977年3月
  - 美術類[陳若海](../Page/陳若海.md "wikilink")，書法項《楷書臨夫子廟堂碑等》
  - 美術類[杜立坤](../Page/杜立坤.md "wikilink")，美術工藝項《玉器雕琢》
  - 戲劇類[王生善](../Page/王生善.md "wikilink")，電視劇本項《愛心》，華岡出版有限公司，1976年10月

#### 舊制第五屆（1979年十一人）

  - 文藝理論類[黃永武](../Page/黃永武.md "wikilink")，文藝理論項目《中國詩學：思想、設計、考據、鑑賞》巨流圖書公司，1977年4月
  - 詩歌類[王志健](../Page/王志健.md "wikilink")（上官予），新詩項目《愛的暖流》[台灣商務印書館](../Page/台灣商務印書館.md "wikilink")，1979年5月
  - 散文類[沙錚](../Page/沙錚.md "wikilink")（又錚），散文項目《古都風情畫》白雲文化事業公司，1979年2月
  - 新聞文學類[趙滋蕃](../Page/趙滋蕃.md "wikilink")（文壽），新聞文學項目《十大建設速寫》[中央日報社](../Page/中央日報.md "wikilink")，1979年6月
  - 傳記文學類[蔣君章](../Page/蔣君章.md "wikilink")（惜秋），傳記文學項目《民初風雲人物》[三民書局](../Page/三民書局.md "wikilink")，1976年10月
  - 傳記文學類[徐詠平](../Page/徐詠平.md "wikilink")，傳記文學項目《陳果夫傳》[正中書局](../Page/正中書局.md "wikilink")，1978年1月
  - 美術類[劉萬航](../Page/劉萬航.md "wikilink")，美術工藝項目《首飾──我國固有工藝技術的金發揚》
  - 美術類[何明績](../Page/何明績.md "wikilink")，雕塑項目《伯夷叔齊》等（二人合得）
  - 美術類[何恆雄](../Page/何恆雄.md "wikilink")，雕塑項目《田梗弄孫》等（二人合得）
  - 舞蹈類[林懷民](../Page/林懷民.md "wikilink")，藝術舞蹈項目《雲門舞集──薪傳》

#### 舊制第九屆（1983年五項六人，特別貢獻獎四人）

  - 詩歌類[向陽](../Page/向陽_\(詩人\).md "wikilink")，新詩《種籽》，東大圖書公司，1980年4月
  - 詩歌類[鄧禹平](../Page/鄧禹平.md "wikilink")，歌詞《我存在因為歌因為愛》，純文學出版社，1983年3月
  - 傳記文學類[林衡道](../Page/林衡道.md "wikilink")，雜文項《鯤島探源》（二人合得），[青年戰士報](../Page/青年戰士報.md "wikilink")，1984年9月
  - 傳記文學類[林鴻博](../Page/林鴻博.md "wikilink")，雜文項《鯤島探源》（二人合得），青年戰士報，1984年9月
  - 新聞文學類[金達凱](../Page/金達凱.md "wikilink")，《盱衡集》，[香港時報](../Page/香港時報.md "wikilink")，1983年5月
  - 書法[寇培深](../Page/寇培深.md "wikilink")，書法項
  - 特別貢獻獎[黃君璧](../Page/黃君璧.md "wikilink")，對繪畫藝術有特殊殊獻
  - 特別貢獻獎[臺靜農](../Page/臺靜農.md "wikilink")，對文學教育、文藝創作有特殊貢獻
  - 特別貢獻獎[梁實秋](../Page/梁實秋.md "wikilink")，對文學教育、文藝創作有特殊貢獻

#### 舊制第十一屆（1986年六項十一人，特別貢獻獎四人）

  - 文藝理論類：文藝史「中國話劇史」，作者[吳慕風筆名](../Page/吳慕風.md "wikilink")[吳若](../Page/吳若.md "wikilink")、[賈亦棣二人合著](../Page/賈亦棣.md "wikilink")。
  - 文藝理論類：文藝批評「人性與抗議文學」，作者[張子樟](../Page/張子樟.md "wikilink")。
  - 散文類：作品「我站在金門望大陸」，作者[卜乃夫筆名無名氏](../Page/卜乃夫.md "wikilink")。
  - 散文類：作品「鄉夢已遠」，作者[姜保真](../Page/姜保真.md "wikilink")。
  - 散文類：作品「此處有仙桃」，作者[潘希真筆名琦君](../Page/潘希真.md "wikilink")。
  - 小說類：作品「花落花開」，作者[嚴停雲筆名華嚴](../Page/嚴停雲.md "wikilink")。
  - 新聞文學類：作品「破曉時分」，作者[張奕](../Page/張奕.md "wikilink")。
  - 新聞文學類：雜文作品「成功人生」，作者[傅佩榮](../Page/傅佩榮.md "wikilink")。
  - 傳記文學類：作品「烽火十五年」，作者[干衡](../Page/干衡.md "wikilink")。
  - 音樂類：國樂曲「七夕雨」，作者[鄭思森](../Page/鄭思森.md "wikilink")。
  - 特別貢獻獎[顏水龍](../Page/顏水龍.md "wikilink")、[王夢鷗](../Page/王夢鷗.md "wikilink")、[楊三郎](../Page/楊三郎.md "wikilink")、[郭小莊](../Page/郭小莊.md "wikilink")。

#### 舊制第十三屆（1987年十人，特別貢獻獎二人）

  - 文藝理論類[鄭明娳](../Page/鄭明娳.md "wikilink")，現代散文類型論
  - 詩歌類[王蓉芷](../Page/王蓉芷.md "wikilink")（[蓉子](../Page/蓉子.md "wikilink")），新詩作品蓉子詩選九輯
  - 散文類[逯耀東](../Page/逯耀東.md "wikilink")，《那年初一》
  - 小說類[趙淑敏](../Page/趙淑敏.md "wikilink")，長篇小說《松花江的浪》
  - 國畫類[江明賢](../Page/江明賢.md "wikilink")，國畫
  - 西畫類[吳炫三](../Page/吳炫三.md "wikilink")，西畫
  - 音樂類[梁銘越](../Page/梁銘越.md "wikilink")，器樂曲《夢蝶曲列》
  - 戲劇類[賴聲川](../Page/賴聲川.md "wikilink")，話劇劇本《暗戀桃花源》
  - 演藝類[汪其楣](../Page/汪其楣.md "wikilink")，戲劇導演《人間孤兒》等五種
  - 演藝類[朱苔麗](../Page/朱苔麗.md "wikilink")，聲樂演唱《弄臣》等歌劇多種
  - 特別貢獻獎[朱玖瑩](../Page/朱玖瑩.md "wikilink")，對書法教育研究有特別貢獻
  - 特別貢獻獎[梁在平](../Page/梁在平.md "wikilink")，對國樂教育研究有特別貢獻

#### 舊制第十四屆（1988年六人，特別貢獻獎兩人）

  - 詩歌類[張振翱](../Page/張振翱.md "wikilink")（[張錯](../Page/張錯.md "wikilink")），新詩項目《漂泊者》[爾雅出版社](../Page/爾雅出版社.md "wikilink")，1986年5月
  - 散文類
    [林文月](../Page/林文月.md "wikilink")，散文項目《交談》[九歌出版社](../Page/九歌出版社.md "wikilink")，1988年2月
  - 兒童文學類
    [黃炳煌](../Page/黃炳煌.md "wikilink")（[黃海](../Page/黃海_\(作家\).md "wikilink")），兒童文學項目《大鼻國歷險記》洪建全文化基金會，1988年5月
  - 美術類[涂璨琳](../Page/涂璨琳.md "wikilink")，國畫項目《山水花鳥》
  - 美術類[林吉基](../Page/林吉基.md "wikilink")，攝影項目《自然生態攝影》
  - 新聞文學類[祝基瀅](../Page/祝基瀅.md "wikilink")，雜文項目《現代人的深思》九歌出版社，1986年1月
  - 特別貢獻獎[熊式一](../Page/熊式一.md "wikilink")，對戲劇著述及翻譯有特別貢獻
  - 特別貢獻獎[呂佛庭](../Page/呂佛庭.md "wikilink")，對繪畫藝教育有特別貢獻

#### 舊制第十五屆（1989年七項八人）

  - 詩歌[余光中](../Page/余光中.md "wikilink")，新詩《夢與地理》[洪範書店](../Page/洪範書店.md "wikilink")，1989年7月
  - 傳記文學[楊艾俐](../Page/楊艾俐.md "wikilink")，傳記文學《孫運璿傳》[天下雜誌](../Page/天下雜誌.md "wikilink")，1989年4月
  - 散文[高大鵬](../Page/高大鵬.md "wikilink")，散文《追尋》[聯合文學出版社](../Page/聯合文學出版社.md "wikilink")，1989年2月
  - 兒童文學[賴西安](../Page/賴西安.md "wikilink")（李潼），兒童文學《博士、布都與我》[民生報](../Page/民生報.md "wikilink")，1989年5月
  - 美術[吳榮隆](../Page/吳榮隆.md "wikilink")，西畫《白鴿呈祥圖》等
  - 演藝[葉綠娜](../Page/葉綠娜.md "wikilink")、[魏樂富](../Page/魏樂富.md "wikilink")，音樂《雙鋼琴演奏》兩人
  - 演藝[魏敏（魏海敏）](../Page/魏海敏.md "wikilink")，戲劇《楊金花》、《玉堂春》等

#### 舊制第十六屆（1990年五項六人）

  - 文藝理論[鍾玲](../Page/鍾玲.md "wikilink")，文藝史及文藝批評《現代中國繆斯─台灣女詩人作品析論》[聯經出版公司](../Page/聯經出版公司.md "wikilink")，1989年６月
  - 詩歌[莫洛夫](../Page/莫洛夫.md "wikilink")，新詩體《月光房子》[九歌出版社](../Page/九歌出版社.md "wikilink")，1989年３月
  - 散文[梁丹丰](../Page/梁丹丰.md "wikilink")，《走過中國大地》[聯經出版公司](../Page/聯經出版公司.md "wikilink")，1989年６月
  - 散文[朱炎](../Page/朱炎.md "wikilink")，《我和你在一起》[九歌出版社](../Page/九歌出版社.md "wikilink")，1989年７月
  - 傳記文學[林太乙](../Page/林太乙.md "wikilink")，《林語堂傳》[聯經出版公司](../Page/聯經出版公司.md "wikilink")，1989年11月
  - 演藝[林昭亮](../Page/林昭亮.md "wikilink")，音樂《貝多芬降E大調第三號小提琴奏鳴曲作品12-3號等》

#### 舊制第十八屆 (1992年六項六人，特別貢獻獎一人)

  - 特別貢獻獎[鍾肇政](../Page/鍾肇政.md "wikilink")，對文學創作有特別貢獻
  - 文藝理論類莊祖煌，文藝理論《一首詩的誕生》九歌出版社，1991年12月
  - 詩歌類陳膺文，新體詩《親密書》書林出版有限公司，1992年五月
  - 散文類[黃永武](../Page/黃永武.md "wikilink")，散文《愛廬小品》洪範書店有限公司，1992年七月
  - 美術類楊明迭，版畫《37痕跡系列》
  - 美術類杜忠誥，書法《趙甌北論詩絕句五首行草書屏》等
  - 音樂類盧炎，器樂曲《管弦幻想曲》

#### 舊制第十九屆（1993年七項七人）

  - 文藝理論[王德威](../Page/王德威.md "wikilink")，文藝批評《小說中國》[麥田出版](../Page/麥田出版.md "wikilink")，1993年6月
  - 散文[張拓蕪](../Page/張拓蕪.md "wikilink")，《我家有個渾小子》[九歌出版](../Page/九歌出版.md "wikilink")，199２年７月
  - 美術[王南雄](../Page/王南雄.md "wikilink")，國畫《森林野趣等》
  - 美術[王南雄](../Page/王南雄.md "wikilink")，攝影《玉山圓柏的生命世界》
  - 音樂[郭芝苑](../Page/郭芝苑.md "wikilink")，器樂曲《交響組曲：「天人師」─釋迦傳》

/\* 参考资料 \*/
　\*\[<https://photo.xuite.net/bhhwang_2013.9/20420594/2.jpg>　國家文藝基金會編印：第十九屆國家文藝獎、第二屆翻譯獎－獲獎人及作品簡介，民國八十三年五月十一日\]詳載舊制國家文藝獎第一屆至第十九屆全部名單。

### 新制

<table>
<thead>
<tr class="header">
<th><p>屆</p></th>
<th><p>年</p></th>
<th><p>文學</p></th>
<th><p>美術</p></th>
<th><p>音樂</p></th>
<th><p>舞蹈</p></th>
<th><p>戲劇</p></th>
<th><p>建築</p></th>
<th><p>電影</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1</p></td>
<td><p>1997年</p></td>
<td><p><a href="../Page/周夢蝶.md" title="wikilink">周夢蝶</a></p></td>
<td><p><a href="../Page/鄭善禧.md" title="wikilink">鄭善禧</a></p></td>
<td><p><a href="../Page/杜黑_(指揮家).md" title="wikilink">杜黑</a></p></td>
<td><p><a href="../Page/劉鳳學.md" title="wikilink">劉鳳學</a></p></td>
<td><p><a href="../Page/李國修.md" title="wikilink">李國修</a></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p>2</p></td>
<td><p>1998年</p></td>
<td><p><a href="../Page/黃春明.md" title="wikilink">黃春明</a></p></td>
<td><p><a href="../Page/廖修平.md" title="wikilink">廖修平</a></p></td>
<td><p><a href="../Page/盧炎.md" title="wikilink">盧炎</a></p></td>
<td><p><a href="../Page/劉紹爐.md" title="wikilink">劉紹爐</a></p></td>
<td><p><a href="../Page/廖瓊枝.md" title="wikilink">廖瓊枝</a></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p>3</p></td>
<td><p>1999年</p></td>
<td><p><a href="../Page/鍾肇政.md" title="wikilink">鍾肇政</a></p></td>
<td><p><a href="../Page/張照堂.md" title="wikilink">張照堂</a></p></td>
<td><p><a href="../Page/馬水龍.md" title="wikilink">馬水龍</a></p></td>
<td><p><a href="../Page/平珩.md" title="wikilink">平珩</a></p></td>
<td><p><a href="../Page/聶光炎.md" title="wikilink">聶光炎</a></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p>4</p></td>
<td><p>2000年</p></td>
<td><p><a href="../Page/楊牧_(臺灣).md" title="wikilink">楊牧</a></p></td>
<td><p><a href="../Page/夏陽.md" title="wikilink">夏陽</a></p></td>
<td><p><a href="../Page/朱宗慶.md" title="wikilink">朱宗慶</a></p></td>
<td><p><a href="../Page/羅曼菲.md" title="wikilink">羅曼菲</a></p></td>
<td><p><a href="../Page/王海玲.md" title="wikilink">王海玲</a></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p>5</p></td>
<td><p>2001年</p></td>
<td><p><a href="../Page/葉石濤.md" title="wikilink">葉石濤</a></p></td>
<td><p><a href="../Page/王攀元.md" title="wikilink">王攀元</a></p></td>
<td><p>從缺</p></td>
<td><p>從缺</p></td>
<td><p><a href="../Page/許王.md" title="wikilink">許王</a><br />
<a href="../Page/賴聲川.md" title="wikilink">賴聲川</a></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p>6</p></td>
<td><p>2002年</p></td>
<td><p><a href="../Page/陳千武.md" title="wikilink">陳千武</a></p></td>
<td><p><a href="../Page/蕭勤.md" title="wikilink">蕭勤</a></p></td>
<td><p>從缺</p></td>
<td><p><a href="../Page/林懷民.md" title="wikilink">林懷民</a></p></td>
<td><p><a href="../Page/黃海岱.md" title="wikilink">黃海岱</a></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p>7</p></td>
<td><p>2003年</p></td>
<td><p><a href="../Page/白先勇.md" title="wikilink">白先勇</a></p></td>
<td><p><a href="../Page/陳慧坤.md" title="wikilink">陳慧坤</a></p></td>
<td><p><a href="../Page/潘皇龍.md" title="wikilink">潘皇龍</a></p></td>
<td><p>從缺</p></td>
<td><p><a href="../Page/顧正秋.md" title="wikilink">顧正秋</a></p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p>8</p></td>
<td><p>2004年</p></td>
<td><p><a href="../Page/林亨泰.md" title="wikilink">林亨泰</a></p></td>
<td><p><a href="../Page/陳其寬.md" title="wikilink">陳其寬</a></p></td>
<td><p><a href="../Page/蕭泰然.md" title="wikilink">蕭泰然</a></p></td>
<td><p><a href="../Page/李靜君_(舞蹈者).md" title="wikilink">李靜君 (舞蹈者)</a></p></td>
<td><p>從缺</p></td>
<td><p>-</p></td>
<td><p><a href="../Page/杜篤之.md" title="wikilink">杜篤之</a></p></td>
</tr>
<tr class="odd">
<td><p>9</p></td>
<td><p>2005年</p></td>
<td><p><a href="../Page/鄭清文.md" title="wikilink">鄭清文</a></p></td>
<td><p>從缺</p></td>
<td><p><a href="../Page/錢南章.md" title="wikilink">錢南章</a></p></td>
<td><p><a href="../Page/林麗珍.md" title="wikilink">林麗珍</a></p></td>
<td><p><a href="../Page/王安祈.md" title="wikilink">王安祈</a></p></td>
<td><p>-</p></td>
<td><p><a href="../Page/侯孝賢.md" title="wikilink">侯孝賢</a></p></td>
</tr>
<tr class="even">
<td><p>10</p></td>
<td><p>2006年</p></td>
<td><p><a href="../Page/李喬.md" title="wikilink">李喬</a></p></td>
<td><p><a href="../Page/柯錫杰.md" title="wikilink">柯錫杰</a></p></td>
<td><p><a href="../Page/郭芝苑.md" title="wikilink">郭芝苑</a></p></td>
<td><p><a href="../Page/林璟如.md" title="wikilink">林璟如</a></p></td>
<td><p><a href="../Page/黃俊雄.md" title="wikilink">黃俊雄</a></p></td>
<td><p><a href="../Page/漢寶德.md" title="wikilink">漢寶德</a></p></td>
<td><p><a href="../Page/廖慶松.md" title="wikilink">廖慶松</a></p></td>
</tr>
<tr class="odd">
<td><p>11</p></td>
<td><p>2007年</p></td>
<td><p><a href="../Page/李敏勇.md" title="wikilink">李敏勇</a></p></td>
<td><p><a href="../Page/林磐聳.md" title="wikilink">林磐聳</a></p></td>
<td><p><a href="../Page/楊秀卿.md" title="wikilink">楊秀卿</a></p></td>
<td><p><a href="../Page/許芳宜.md" title="wikilink">許芳宜</a></p></td>
<td><p><a href="../Page/魏海敏.md" title="wikilink">魏海敏</a></p></td>
<td><p><a href="../Page/姚仁喜.md" title="wikilink">姚仁喜</a></p></td>
<td><p><a href="../Page/王童.md" title="wikilink">王童</a></p></td>
</tr>
<tr class="even">
<td><p>12</p></td>
<td><p>2008年</p></td>
<td><p><a href="../Page/施叔青.md" title="wikilink">施叔青</a></p></td>
<td><p><a href="../Page/劉國松.md" title="wikilink">劉國松</a></p></td>
<td><p><a href="../Page/李泰祥.md" title="wikilink">李泰祥</a></p></td>
<td><p>從缺</p></td>
<td><p><a href="../Page/劉若瑀.md" title="wikilink">劉若瑀</a></p></td>
<td><p><a href="../Page/李祖原.md" title="wikilink">李祖原</a></p></td>
<td><p><a href="../Page/李屏賓.md" title="wikilink">李屏賓</a></p></td>
</tr>
<tr class="odd">
<td><p>13</p></td>
<td><p>2009年</p></td>
<td><p><a href="../Page/王文興.md" title="wikilink">王文興</a></p></td>
<td><p><a href="../Page/陳界仁.md" title="wikilink">陳界仁</a></p></td>
<td><p><a href="../Page/廖年賦.md" title="wikilink">廖年賦</a></p></td>
<td><p>從缺</p></td>
<td><p><a href="../Page/金士傑.md" title="wikilink">金士傑</a></p></td>
<td><p><a href="../Page/王大閎.md" title="wikilink">王大閎</a></p></td>
<td><p><a href="../Page/陳博文.md" title="wikilink">陳博文</a></p></td>
</tr>
<tr class="even">
<td><p>14</p></td>
<td><p>2010年</p></td>
<td><p><a href="../Page/七等生.md" title="wikilink">七等生</a></p></td>
<td><p><a href="../Page/張光賓.md" title="wikilink">張光賓</a></p></td>
<td><p><a href="../Page/賴德和.md" title="wikilink">賴德和</a></p></td>
<td><p>從缺</p></td>
<td><p><a href="../Page/吳興國.md" title="wikilink">吳興國</a></p></td>
<td><p>從缺</p></td>
<td><p>從缺</p></td>
</tr>
<tr class="odd">
<td><p>15</p></td>
<td><p>2011年</p></td>
<td><p><a href="../Page/陳若曦.md" title="wikilink">陳若曦</a></p></td>
<td><p><a href="../Page/莊靈.md" title="wikilink">莊靈</a></p></td>
<td><p><a href="../Page/曾道雄.md" title="wikilink">曾道雄</a></p></td>
<td><p>從缺</p></td>
<td><p><a href="../Page/李小平.md" title="wikilink">李小平</a></p></td>
<td><p>從缺</p></td>
<td><p><a href="../Page/張作驥.md" title="wikilink">張作驥</a></p></td>
</tr>
<tr class="even">
<td><p>16</p></td>
<td><p>2012年</p></td>
<td><p><a href="../Page/子敏.md" title="wikilink">子敏</a></p></td>
<td><p><a href="../Page/李錫奇.md" title="wikilink">李錫奇</a></p></td>
<td><p><a href="../Page/賴碧霞.md" title="wikilink">賴碧霞</a></p></td>
<td><p>從缺</p></td>
<td><p><a href="../Page/唐美雲.md" title="wikilink">唐美雲</a></p></td>
<td><p><a href="../Page/謝英俊.md" title="wikilink">謝英俊</a></p></td>
<td><p>從缺</p></td>
</tr>
<tr class="odd">
<td><p>17</p></td>
<td><p>2013年</p></td>
<td><p><a href="../Page/宋澤萊.md" title="wikilink">宋澤萊</a></p></td>
<td><p>從缺</p></td>
<td><p><a href="../Page/陳茂萱.md" title="wikilink">陳茂萱</a></p></td>
<td><p>從缺</p></td>
<td><p><a href="../Page/紀蔚然.md" title="wikilink">紀蔚然</a></p></td>
<td><p>從缺</p></td>
<td><p><a href="../Page/李安.md" title="wikilink">李安</a></p></td>
</tr>
<tr class="even">
<td><p>18</p></td>
<td><p>2014年</p></td>
<td><p><a href="../Page/王鼎鈞.md" title="wikilink">王鼎鈞</a></p></td>
<td><p><a href="../Page/陳正雄_(畫家).md" title="wikilink">陳正雄</a></p></td>
<td><p><a href="../Page/簡文彬.md" title="wikilink">簡文彬</a></p></td>
<td><p>從缺</p></td>
<td><p><a href="../Page/王孟超.md" title="wikilink">王孟超</a></p></td>
<td><p><a href="../Page/陳邁.md" title="wikilink">陳邁</a></p></td>
<td><p><a href="../Page/王小棣.md" title="wikilink">王小棣</a></p></td>
</tr>
<tr class="odd">
<td><p>19</p></td>
<td><p>2016年</p></td>
<td><p><a href="../Page/李永平.md" title="wikilink">李永平</a></p></td>
<td><p><a href="../Page/吳瑪悧.md" title="wikilink">吳瑪悧</a></p></td>
<td><p><a href="../Page/莊進才.md" title="wikilink">莊進才</a></p></td>
<td><p><a href="../Page/何曉玫.md" title="wikilink">何曉玫</a></p></td>
<td><p>從缺</p></td>
<td><p><a href="../Page/潘冀.md" title="wikilink">潘冀</a></p></td>
<td><p>從缺</p></td>
</tr>
<tr class="even">
<td><p>20</p></td>
<td><p>2018年</p></td>
<td><p><a href="../Page/李魁賢.md" title="wikilink">李魁賢</a></p></td>
<td><p><a href="../Page/撒古流･巴瓦瓦隆.md" title="wikilink">撒古流･巴瓦瓦隆</a></p></td>
<td><p><a href="../Page/金希文.md" title="wikilink">金希文</a></p></td>
<td><p><a href="../Page/姚淑芬.md" title="wikilink">姚淑芬</a></p></td>
<td><p><a href="../Page/陳勝國.md" title="wikilink">陳勝國</a></p></td>
<td><p><a href="../Page/黃聲遠.md" title="wikilink">黃聲遠</a></p></td>
<td><p><a href="../Page/林強.md" title="wikilink">林強</a></p></td>
</tr>
</tbody>
</table>

/\* 参考资料 \*/
　\*\[<https://photo.xuite.net/bhhwang_2013.9/20420594/2.jpg>　國家文藝基金會編印：第十九屆國家文藝獎、第二屆翻譯獎－獲獎人及作品簡介，民國八十三年五月十一日\]詳載舊制國家文藝獎第一屆至第十九屆全部名單。

## 外部連結

  - [財團法人國家文化藝術基金會](http://www.ncafroc.org.tw/)

<!-- end list -->

  - [黃海：文學部落格/相簿](https://photo.xuite.net/bhhwang_2013.9/20420594)

[\*](../Category/台灣國家文藝獎.md "wikilink")