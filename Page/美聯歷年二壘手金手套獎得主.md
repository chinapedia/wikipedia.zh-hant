以下列出[美國職棒大聯盟](../Page/美國職棒大聯盟.md "wikilink")[美聯歷年在擔任](../Page/美國聯盟.md "wikilink")[二壘手這個位置時獲得](../Page/二壘手.md "wikilink")[金手套獎](../Page/美國職棒大聯盟金手套獎.md "wikilink")（）的球員。

-----

| **[金手套獎](../Page/美國職棒大聯盟金手套獎.md "wikilink")** |
| :-------------------------------------------: |
|      **[美聯](../Page/美聯.md "wikilink"):**      |
|      **[國聯](../Page/國聯.md "wikilink"):**      |

-----

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>球員</p></th>
<th><p>球隊</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>1957年</p></td>
<td><p><a href="../Page/Nellie_Fox.md" title="wikilink">Nellie Fox</a></p></td>
<td><p><a href="../Page/芝加哥白襪.md" title="wikilink">芝加哥白襪</a></p></td>
</tr>
<tr class="even">
<td><p>1958年</p></td>
<td><p><a href="../Page/Frank_Bolling.md" title="wikilink">Frank Bolling</a></p></td>
<td><p><a href="../Page/底特律老虎.md" title="wikilink">底特律老虎</a></p></td>
</tr>
<tr class="odd">
<td><p>1959年</p></td>
<td><p><a href="../Page/Nellie_Fox.md" title="wikilink">Nellie Fox</a></p></td>
<td><p><a href="../Page/芝加哥白襪.md" title="wikilink">芝加哥白襪</a></p></td>
</tr>
<tr class="even">
<td><p>1960年</p></td>
<td><p><a href="../Page/Nellie_Fox.md" title="wikilink">Nellie Fox</a></p></td>
<td><p><a href="../Page/芝加哥白襪.md" title="wikilink">芝加哥白襪</a></p></td>
</tr>
<tr class="odd">
<td><p>1961年</p></td>
<td><p><a href="../Page/Bobby_Richardson.md" title="wikilink">Bobby Richardson</a></p></td>
<td><p><a href="../Page/紐約洋基.md" title="wikilink">紐約洋基</a></p></td>
</tr>
<tr class="even">
<td><p>1962年</p></td>
<td><p><a href="../Page/Bobby_Richardson.md" title="wikilink">Bobby Richardson</a></p></td>
<td><p><a href="../Page/紐約洋基.md" title="wikilink">紐約洋基</a></p></td>
</tr>
<tr class="odd">
<td><p>1963年</p></td>
<td><p><a href="../Page/Bobby_Richardson.md" title="wikilink">Bobby Richardson</a></p></td>
<td><p><a href="../Page/紐約洋基.md" title="wikilink">紐約洋基</a></p></td>
</tr>
<tr class="even">
<td><p>1964年</p></td>
<td><p><a href="../Page/Bobby_Richardson.md" title="wikilink">Bobby Richardson</a></p></td>
<td><p><a href="../Page/紐約洋基.md" title="wikilink">紐約洋基</a></p></td>
</tr>
<tr class="odd">
<td><p>1965年</p></td>
<td><p><a href="../Page/Bobby_Richardson.md" title="wikilink">Bobby Richardson</a></p></td>
<td><p><a href="../Page/紐約洋基.md" title="wikilink">紐約洋基</a></p></td>
</tr>
<tr class="even">
<td><p>1966年</p></td>
<td><p><a href="../Page/Bobby_Knoop.md" title="wikilink">Bobby Knoop</a></p></td>
<td><p><a href="../Page/洛杉磯安那罕天使.md" title="wikilink">加州天使</a></p></td>
</tr>
<tr class="odd">
<td><p>1967年</p></td>
<td><p><a href="../Page/Bobby_Knoop.md" title="wikilink">Bobby Knoop</a></p></td>
<td><p><a href="../Page/洛杉磯安那罕天使.md" title="wikilink">加州天使</a></p></td>
</tr>
<tr class="even">
<td><p>1968年</p></td>
<td><p><a href="../Page/Bobby_Knoop.md" title="wikilink">Bobby Knoop</a></p></td>
<td><p><a href="../Page/洛杉磯安那罕天使.md" title="wikilink">加州天使</a></p></td>
</tr>
<tr class="odd">
<td><p>1969年</p></td>
<td><p><a href="../Page/Davey_Johnson.md" title="wikilink">Davey Johnson</a></p></td>
<td><p><a href="../Page/巴爾的摩金鶯.md" title="wikilink">巴爾的摩金鶯</a></p></td>
</tr>
<tr class="even">
<td><p>1970年</p></td>
<td><p><a href="../Page/Davey_Johnson.md" title="wikilink">Davey Johnson</a></p></td>
<td><p><a href="../Page/巴爾的摩金鶯.md" title="wikilink">巴爾的摩金鶯</a></p></td>
</tr>
<tr class="odd">
<td><p>1971年</p></td>
<td><p><a href="../Page/Davey_Johnson.md" title="wikilink">Davey Johnson</a></p></td>
<td><p><a href="../Page/巴爾的摩金鶯.md" title="wikilink">巴爾的摩金鶯</a></p></td>
</tr>
<tr class="even">
<td><p>1972年</p></td>
<td><p><a href="../Page/Doug_Griffin.md" title="wikilink">Doug Griffin</a></p></td>
<td><p><a href="../Page/波士頓紅襪.md" title="wikilink">波士頓紅襪</a></p></td>
</tr>
<tr class="odd">
<td><p>1973年</p></td>
<td><p><a href="../Page/Bobby_Grich.md" title="wikilink">Bobby Grich</a></p></td>
<td><p><a href="../Page/巴爾的摩金鶯.md" title="wikilink">巴爾的摩金鶯</a></p></td>
</tr>
<tr class="even">
<td><p>1974年</p></td>
<td><p><a href="../Page/Bobby_Grich.md" title="wikilink">Bobby Grich</a></p></td>
<td><p><a href="../Page/巴爾的摩金鶯.md" title="wikilink">巴爾的摩金鶯</a></p></td>
</tr>
<tr class="odd">
<td><p>1975年</p></td>
<td><p><a href="../Page/Bobby_Grich.md" title="wikilink">Bobby Grich</a></p></td>
<td><p><a href="../Page/巴爾的摩金鶯.md" title="wikilink">巴爾的摩金鶯</a></p></td>
</tr>
<tr class="even">
<td><p>1976年</p></td>
<td><p><a href="../Page/Bobby_Grich.md" title="wikilink">Bobby Grich</a></p></td>
<td><p><a href="../Page/巴爾的摩金鶯.md" title="wikilink">巴爾的摩金鶯</a></p></td>
</tr>
<tr class="odd">
<td><p>1977年</p></td>
<td><p><a href="../Page/Frank_White.md" title="wikilink">Frank White</a></p></td>
<td><p><a href="../Page/堪薩斯市皇家.md" title="wikilink">堪薩斯市皇家</a></p></td>
</tr>
<tr class="even">
<td><p>1978年</p></td>
<td><p><a href="../Page/Frank_White.md" title="wikilink">Frank White</a></p></td>
<td><p><a href="../Page/堪薩斯市皇家.md" title="wikilink">堪薩斯市皇家</a></p></td>
</tr>
<tr class="odd">
<td><p>1979年</p></td>
<td><p><a href="../Page/Frank_White.md" title="wikilink">Frank White</a></p></td>
<td><p><a href="../Page/堪薩斯市皇家.md" title="wikilink">堪薩斯市皇家</a></p></td>
</tr>
<tr class="even">
<td><p>1980年</p></td>
<td><p><a href="../Page/Frank_White.md" title="wikilink">Frank White</a></p></td>
<td><p><a href="../Page/堪薩斯市皇家.md" title="wikilink">堪薩斯市皇家</a></p></td>
</tr>
<tr class="odd">
<td><p>1981年</p></td>
<td><p><a href="../Page/Frank_White.md" title="wikilink">Frank White</a></p></td>
<td><p><a href="../Page/堪薩斯市皇家.md" title="wikilink">堪薩斯市皇家</a></p></td>
</tr>
<tr class="even">
<td><p>1982年</p></td>
<td><p><a href="../Page/Frank_White.md" title="wikilink">Frank White</a></p></td>
<td><p><a href="../Page/堪薩斯市皇家.md" title="wikilink">堪薩斯市皇家</a></p></td>
</tr>
<tr class="odd">
<td><p>1983年</p></td>
<td><p><a href="../Page/Lou_Whitaker.md" title="wikilink">Lou Whitaker</a></p></td>
<td><p><a href="../Page/底特律老虎.md" title="wikilink">底特律老虎</a></p></td>
</tr>
<tr class="even">
<td><p>1984年</p></td>
<td><p><a href="../Page/Lou_Whitaker.md" title="wikilink">Lou Whitaker</a></p></td>
<td><p><a href="../Page/底特律老虎.md" title="wikilink">底特律老虎</a></p></td>
</tr>
<tr class="odd">
<td><p>1985年</p></td>
<td><p><a href="../Page/Lou_Whitaker.md" title="wikilink">Lou Whitaker</a></p></td>
<td><p><a href="../Page/底特律老虎.md" title="wikilink">底特律老虎</a></p></td>
</tr>
<tr class="even">
<td><p>1986年</p></td>
<td><p><a href="../Page/Frank_White.md" title="wikilink">Frank White</a></p></td>
<td><p><a href="../Page/堪薩斯市皇家.md" title="wikilink">堪薩斯市皇家</a></p></td>
</tr>
<tr class="odd">
<td><p>1987年</p></td>
<td><p><a href="../Page/Frank_White.md" title="wikilink">Frank White</a></p></td>
<td><p><a href="../Page/堪薩斯市皇家.md" title="wikilink">堪薩斯市皇家</a></p></td>
</tr>
<tr class="even">
<td><p>1988年</p></td>
<td><p><a href="../Page/Harold_Reynolds.md" title="wikilink">Harold Reynolds</a></p></td>
<td><p><a href="../Page/西雅圖水手.md" title="wikilink">西雅圖水手</a></p></td>
</tr>
<tr class="odd">
<td><p>1989年</p></td>
<td><p><a href="../Page/Harold_Reynolds.md" title="wikilink">Harold Reynolds</a></p></td>
<td><p><a href="../Page/西雅圖水手.md" title="wikilink">西雅圖水手</a></p></td>
</tr>
<tr class="even">
<td><p>1990年</p></td>
<td><p><a href="../Page/Harold_Reynolds.md" title="wikilink">Harold Reynolds</a></p></td>
<td><p><a href="../Page/西雅圖水手.md" title="wikilink">西雅圖水手</a></p></td>
</tr>
<tr class="odd">
<td><p>1991年</p></td>
<td><p><a href="../Page/Roberto_Alomar.md" title="wikilink">Roberto Alomar</a></p></td>
<td><p><a href="../Page/多倫多藍鳥.md" title="wikilink">多倫多藍鳥</a></p></td>
</tr>
<tr class="even">
<td><p>1992年</p></td>
<td><p><a href="../Page/Roberto_Alomar.md" title="wikilink">Roberto Alomar</a></p></td>
<td><p><a href="../Page/多倫多藍鳥.md" title="wikilink">多倫多藍鳥</a></p></td>
</tr>
<tr class="odd">
<td><p>1993年</p></td>
<td><p><a href="../Page/Roberto_Alomar.md" title="wikilink">Roberto Alomar</a></p></td>
<td><p><a href="../Page/多倫多藍鳥.md" title="wikilink">多倫多藍鳥</a></p></td>
</tr>
<tr class="even">
<td><p>1994年</p></td>
<td><p><a href="../Page/Roberto_Alomar.md" title="wikilink">Roberto Alomar</a></p></td>
<td><p><a href="../Page/多倫多藍鳥.md" title="wikilink">多倫多藍鳥</a></p></td>
</tr>
<tr class="odd">
<td><p>1995年</p></td>
<td><p><a href="../Page/Roberto_Alomar.md" title="wikilink">Roberto Alomar</a></p></td>
<td><p><a href="../Page/多倫多藍鳥.md" title="wikilink">多倫多藍鳥</a></p></td>
</tr>
<tr class="even">
<td><p>1996年</p></td>
<td><p><a href="../Page/Roberto_Alomar.md" title="wikilink">Roberto Alomar</a></p></td>
<td><p><a href="../Page/巴爾的摩金鶯.md" title="wikilink">巴爾的摩金鶯</a></p></td>
</tr>
<tr class="odd">
<td><p>1997年</p></td>
<td><p><a href="../Page/Chuck_Knoblauch.md" title="wikilink">Chuck Knoblauch</a></p></td>
<td><p><a href="../Page/明尼蘇達雙城.md" title="wikilink">明尼蘇達雙城</a></p></td>
</tr>
<tr class="even">
<td><p>1998年</p></td>
<td><p><a href="../Page/Roberto_Alomar.md" title="wikilink">Roberto Alomar</a></p></td>
<td><p><a href="../Page/巴爾的摩金鶯.md" title="wikilink">巴爾的摩金鶯</a></p></td>
</tr>
<tr class="odd">
<td><p>1999年</p></td>
<td><p><a href="../Page/Roberto_Alomar.md" title="wikilink">Roberto Alomar</a></p></td>
<td><p><a href="../Page/克里夫蘭印地安人.md" title="wikilink">克-{里}-夫蘭印地安人</a></p></td>
</tr>
<tr class="even">
<td><p>2000年</p></td>
<td><p><a href="../Page/Roberto_Alomar.md" title="wikilink">Roberto Alomar</a></p></td>
<td><p><a href="../Page/克里夫蘭印地安人.md" title="wikilink">克-{里}-夫蘭印地安人</a></p></td>
</tr>
<tr class="odd">
<td><p>2001年</p></td>
<td><p><a href="../Page/Roberto_Alomar.md" title="wikilink">Roberto Alomar</a></p></td>
<td><p><a href="../Page/克里夫蘭印地安人.md" title="wikilink">克-{里}-夫蘭印地安人</a></p></td>
</tr>
<tr class="even">
<td><p>2002年</p></td>
<td><p><a href="../Page/Bret_Boone.md" title="wikilink">Bret Boone</a></p></td>
<td><p><a href="../Page/西雅圖水手.md" title="wikilink">西雅圖水手</a></p></td>
</tr>
<tr class="odd">
<td><p>2003年</p></td>
<td><p><a href="../Page/Bret_Boone.md" title="wikilink">Bret Boone</a></p></td>
<td><p><a href="../Page/西雅圖水手.md" title="wikilink">西雅圖水手</a></p></td>
</tr>
<tr class="even">
<td><p>2004年</p></td>
<td><p><a href="../Page/Bret_Boone.md" title="wikilink">Bret Boone</a></p></td>
<td><p><a href="../Page/西雅圖水手.md" title="wikilink">西雅圖水手</a></p></td>
</tr>
<tr class="odd">
<td><p>2005年</p></td>
<td><p><a href="../Page/奥兰多·哈德森.md" title="wikilink">奥兰多·哈德森</a></p></td>
<td><p><a href="../Page/多倫多藍鳥.md" title="wikilink">多倫多藍鳥</a></p></td>
</tr>
<tr class="even">
<td><p>2006年</p></td>
<td><p><a href="../Page/Mark_Grudzielanek.md" title="wikilink">Mark Grudzielanek</a></p></td>
<td><p><a href="../Page/堪薩斯市皇家.md" title="wikilink">堪薩斯市皇家</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/普拉西多·波蘭柯.md" title="wikilink">普拉西多·波蘭柯</a></p></td>
<td><p><a href="../Page/底特律老虎.md" title="wikilink">底特律老虎</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/达斯汀·佩德罗亚.md" title="wikilink">达斯汀·佩德罗亚</a></p></td>
<td><p><a href="../Page/波士頓紅襪.md" title="wikilink">波士頓紅襪</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/普拉西多·波蘭柯.md" title="wikilink">普拉西多·波蘭柯</a></p></td>
<td><p><a href="../Page/底特律老虎.md" title="wikilink">底特律老虎</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/羅賓森·坎諾.md" title="wikilink">羅賓森·坎諾</a></p></td>
<td><p><a href="../Page/紐約洋基.md" title="wikilink">紐約洋基</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/达斯汀·佩德罗亚.md" title="wikilink">达斯汀·佩德罗亚</a></p></td>
<td><p><a href="../Page/波士頓紅襪.md" title="wikilink">波士頓紅襪</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/羅賓森·坎諾.md" title="wikilink">羅賓森·坎諾</a></p></td>
<td><p><a href="../Page/紐約洋基.md" title="wikilink">紐約洋基</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/达斯汀·佩德罗亚.md" title="wikilink">达斯汀·佩德罗亚</a></p></td>
<td><p><a href="../Page/波士頓紅襪.md" title="wikilink">波士頓紅襪</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/达斯汀·佩德罗亚.md" title="wikilink">达斯汀·佩德罗亚</a></p></td>
<td><p><a href="../Page/波士頓紅襪.md" title="wikilink">波士頓紅襪</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/荷西·奧圖維.md" title="wikilink">荷西·奧圖維</a></p></td>
<td><p><a href="../Page/休士頓太空人.md" title="wikilink">休士頓太空人</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/伊恩·金斯勒.md" title="wikilink">伊恩·金斯勒</a></p></td>
<td><p><a href="../Page/底特律老虎.md" title="wikilink">底特律老虎</a></p></td>
</tr>
<tr class="odd">
<td></td>
<td><p><a href="../Page/Brian_Dozier.md" title="wikilink">Brian Dozier</a></p></td>
<td><p><a href="../Page/明尼蘇達雙城.md" title="wikilink">明尼蘇達雙城</a></p></td>
</tr>
<tr class="even">
<td></td>
<td><p><a href="../Page/伊恩·金斯勒.md" title="wikilink">伊恩·金斯勒</a></p></td>
<td><p><a href="../Page/洛杉磯天使.md" title="wikilink">洛杉磯天使</a><br />
<a href="../Page/波士頓紅襪.md" title="wikilink">波士頓紅襪</a></p></td>
</tr>
</tbody>
</table>

[en:List of AL Gold Glove winners at second
base](../Page/en:List_of_AL_Gold_Glove_winners_at_second_base.md "wikilink")

[Gold Glove Award, 4-AL](../Category/美國職棒獎項.md "wikilink")