**Windows Defender**，曾用名**Microsoft
AntiSpyware**，是用来移除、隔离和预防[间谍软件的程序](../Page/间谍软件.md "wikilink")，可以运行在[Windows
XP以及更高版本的操作系统上](../Page/Windows_XP.md "wikilink")，並已經內置在[Windows
Vista以及以後的版本中](../Page/Windows_Vista.md "wikilink")。测试版于[2005年](../Page/2005年.md "wikilink")[1月6日发布](../Page/1月6日.md "wikilink")，在2005年[6月23日](../Page/6月23日.md "wikilink")、[2006年](../Page/2006年.md "wikilink")[2月17日微软又发布了更新的测试版本](../Page/2月17日.md "wikilink")。Windows
Defender的定义库更新很频繁。在[Windows
8中与](../Page/Windows_8.md "wikilink")[Microsoft Security
Essentials整合](../Page/Microsoft_Security_Essentials.md "wikilink")。

Windows
Defender不像某些其他同类免费产品一样只能扫描系统，还可以对系统进行实时监控，移除已經安装的[ActiveX插件](../Page/ActiveX.md "wikilink")\[1\]，清除大多数微软的程序和其他常用程序的历史纪录。

## 功能

  - Windows Defender提供的扫描类型分为三种，分别是完全扫描、快速扫描、自定义扫描。
  - 在「工具」（Tools）页面，用户可以透过「选项」（options）对Windows Defender的「即時防护」（Real-time
    Protection）、「自动扫描计划」（Automatic
    scanning）进行设置修改，或进行進階的設定。用户可以在工具（Tools）页面中决定加入[Microsoft
    SpyNet社区](../Page/Microsoft_SpyNet.md "wikilink")，以及对已經隔离程序的处理操作。用户可以透过點击「检查更新」（Check
    for updates）或是使用[microsoft
    Update对Windows](../Page/microsoft_Update.md "wikilink")
    Defender进行升级。
  - 在[Windows 7以及以後的版本中](../Page/Windows_7.md "wikilink")，Windows
    Defender会配合[行動作業中心](../Page/Windows_7新功能#.E8.A1.8C.E5.8B.95.E4.BD.9C.E6.A5.AD.E4.B8.AD.E5.BF.83.md "wikilink")[Windows
    8以後](../Page/Windows_8.md "wikilink")[重要訊息中心防范恶意软件以维护Windows稳定安全執行](../Page/重要訊息中心.md "wikilink")。
  - [Windows 10的版本中](../Page/Windows_10.md "wikilink")，Windows
    Defender會自訂一個時間自動更新（通常1天至少更新1次）（Build 10240的Windows
    Update可以看到是否有安裝，此功能在後面的Build被移除）。從Windows 10
    1709起，更名**Windows Defender Security
    Center**，雖然有提供更多保護選項，但不再是一般視窗，而是一個應用程式視窗
  - 在Windows 10 version 1703之后，Windows
    Defender可运行在[沙盒环境中](../Page/沙盒_\(電腦安全\).md "wikilink")\[2\]。

## 参考文献

## 外部链接

  - [Windows
    Defender](http://www.microsoft.com/athome/security/spyware/software/default.mspx)

[Defender](../Category/微软软件.md "wikilink")
[Category:反间谍软件](../Category/反间谍软件.md "wikilink")
[Category:Windows组件](../Category/Windows组件.md "wikilink")
[Category:Microsoft
Windows安全技术](../Category/Microsoft_Windows安全技术.md "wikilink")
[Category:2006年软件](../Category/2006年软件.md "wikilink")
[Category:Windows安全软件](../Category/Windows安全软件.md "wikilink")

1.
2.   ZDNet
    |url=<https://www.zdnet.com/article/windows-defender-becomes-first-antivirus-to-run-inside-a-sandbox/>
    |accessdate=2018-10-30 |work=ZDNet |language=en}}