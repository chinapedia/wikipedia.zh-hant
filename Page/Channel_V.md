**-{Channel
\[V\]}-**是一個國際性的音樂[電視頻道](../Page/電視台.md "wikilink")，隸屬於[星空傳媒](../Page/星空傳媒.md "wikilink")，在[東亞](../Page/東亞.md "wikilink")、[東南亞](../Page/東南亞.md "wikilink")、[南亞](../Page/南亞.md "wikilink")、[中東地區等地多個國家播放](../Page/中東地區.md "wikilink")。該頻道主要播放英文主流音樂和一些區域性語言的歌曲，其中在[台灣與](../Page/台灣.md "wikilink")[中國大陸主要播放](../Page/中國大陸.md "wikilink")[華語流行音樂](../Page/華語流行音樂.md "wikilink")。2010年8月星空傳媒把中国大陆頻道控股權易手予[上海市人民政府牽頭成立的華人文化產業投資基金](../Page/上海市人民政府.md "wikilink")\[1\]。

## V的含義

官方並無提供字母**V**的確切含義，以下是流傳的說法：

  - 最廣泛的說法是V代表胜利（victory）。
  - 另一種說法是V是隨机取的字母，就像習語xyz一樣。
  - V是[羅馬數字](../Page/羅馬數字.md "wikilink")5。星空傳媒的前身[衛星電視有限公司成立時只有](../Page/衛星電視有限公司.md "wikilink")5個頻道，即中文台、電影台、體育台、合家歡台與音樂台（Channel
    \[V\]前身）。Channel \[V\]取名可能借用「第5頻道」之意。

隨著星空傳媒頻道的增加，這個名稱便只是一個代號，不再具有實質意義。

## Channel \[V\]國際頻道

Channel \[V\]國際頻道是主要的頻道。1992年至2002年，Channel
\[V\]在[香港製作和播送節目](../Page/香港.md "wikilink")，被[Double
Vision (M) Sdn
Bhd買下後](../Page/Double_Vision_\(M\)_Sdn_Bhd.md "wikilink")，轉移到了[馬來西亞](../Page/馬來西亞.md "wikilink")，而港澳兩地的免費服務因應重組於2006年4月1日全面結束，2008年又搬回香港與Channel
\[V\]台灣、印度、中國大陸頻道共用同一廠。因為Channel
\[V\]國際頻道在許多國家播放，亞洲區的收視率比[MTV東南亞頻道還要高](../Page/MTV.md "wikilink")。它有很多知名的節目，例如By
Demand、 Top 5 @ 5、 The Mint等。

2017年9月1日，Channel V 國際頻道正式更名為 \[V\] by FOX，但播出節目卻不受影響，換言之，Channel V
目前在中國大陸、台灣、泰國、印度、澳洲等。 近年來較著名的主持人有：

  - [Dominic Lau](../Page/Dominic_Lau.md "wikilink")
  - [Laila Rouass](../Page/Laila_Rouass.md "wikilink")
  - [Trey](../Page/Trey.md "wikilink")
  - [Sophiya](../Page/Sophiya.md "wikilink")
  - [Asha Gill](../Page/Asha_Gill.md "wikilink")
  - [Adrian da Silva](../Page/Adrian_da_Silva.md "wikilink")

<!-- end list -->

  - [Joey](../Page/Joey_\(Channel_V\).md "wikilink")
  - [Vivian Tan](../Page/Vivian_Tan.md "wikilink")
  - [Paula Malai Ali](../Page/Paula_Malai_Ali.md "wikilink")
  - [Amanda Griffith](../Page/Amanda_Griffith.md "wikilink")
  - [Cindy Burbridge](../Page/Cindy_Burbridge.md "wikilink")
  - [Sarah Tan](../Page/Sarah_Tan.md "wikilink")

## Channel \[V\]臺灣頻道

1994年開播，主要對象為[台灣的](../Page/台灣.md "wikilink")[年輕觀眾](../Page/青年.md "wikilink")。於2006年起改稱「Channel
\[V\] 娛樂台」（\[V\]娛樂），以配合Channel
\[V\]在台灣的節目改革，旗艦節目為[我愛黑澀會及](../Page/我愛黑澀會.md "wikilink")[模范棒棒堂](../Page/模范棒棒堂.md "wikilink")，還有[CIRCUS
ACTION](../Page/CIRCUS_ACTION.md "wikilink")、[就是愛JK](../Page/就是愛JK.md "wikilink")、[V
NEWS](../Page/V_NEWS.md "wikilink")、[校園SUPERMAN](../Page/校園SUPERMAN.md "wikilink")、[音樂飆榜等節目](../Page/音樂飆榜.md "wikilink")。

  - 2007年7月起，推出海外版，黃金節目時段(星期一至五晚上6時至12時、星期六及日中午12時至晚上12時)和台灣版同步播放，其後以6小時(星期六及日為12小時)循環播放，廣告時段只播放[音樂錄影帶及自製節目預告片](../Page/音樂錄影帶.md "wikilink")。外購節目(如[超級名模生死鬥](../Page/超級名模生死鬥.md "wikilink"))因為播放版權問題，在海外版會被抽起，以重播其他節目替代。海外版播出涵蓋範圍包括：[香港](../Page/香港.md "wikilink")、[新加坡](../Page/新加坡.md "wikilink")、[馬來西亞](../Page/馬來西亞.md "wikilink")、[北美等地區](../Page/北美.md "wikilink")。

<!-- end list -->

  - 2011年起，國內版改以播放外購節目為主，大幅度影響海外版的同步播放。

<!-- end list -->

  - 2012年9月1日，Channel
    \[V\]台灣頻道(國內版)更名為[FOX娛樂台](../Page/FOX娛樂台.md "wikilink")，並開始引入[福斯國際電視網的外購劇集](../Page/福斯國際電視網.md "wikilink")。海外版維持原頻道名稱不變。

<!-- end list -->

  - 2013年7月，FOX娛樂台不再播放自製節目，和海外版的同步播放安排終止。

<!-- end list -->

  - 2014年1月1日，配合FOX頻道在台灣在地化，FOX娛樂台和另一頻道FOX
    HD進行整合，並更名為[FOX台灣頻道](../Page/FOX台灣頻道.md "wikilink")，從\[V\]台品牌脫鈎。此後Channel
    \[V\]台灣頻道只維持海外版播放，部分時段維持播放音樂錄影帶(普普風、新嗓門)，其他時段重播\[V\]娛樂時期自製節目及[衛視中文台精選節目](../Page/衛視中文台.md "wikilink")。和[FOX亞洲頻道毫無關係](../Page/FOX_\(亞洲\).md "wikilink")。

<!-- end list -->

  - 2018年7月15日，Channel
    \[V\]台灣頻道正式停播。實質上，香港和馬來西亞早於2017年8月31日和2018年6月1日起停播，新加坡於2018年7月5日起停播，而最後播出地區為美國和加拿大於2018年7月12日和2018年7月15日起停播。

## Channel \[V\]中國頻道

該頻道主要對象為中國大陸的年輕觀眾，該頻道以音樂為主，頻道包裝用字為[简体中文](../Page/简体中文.md "wikilink")。播出過的主要節目有《劲爆点》、《周末星派对》、《\[V\]
Niche》、《\[V\] ROCK》、《\[V\] Classic》、《\[V\] OST原声大碟》、《\[V\]
LIVE》、《特别献礼》、《音樂飆榜》、《国际酷》、《台湾通》、《日韩流》、《神州热》、《偶像派》、《流行速递》，《劲爆舞曲》等。此外，還一度提供中國內地觀眾透過[手機下載純音樂節目所播歌曲的铃声](../Page/手機.md "wikilink")。

Channel \[V\]中國大陸頻道還播放過來自「Channel
\[V\]娛樂台（台灣）」節目，例如「\[V\]接客」、「特別獻禮」等。中國大陸頻道曾經的主持人有[李晨_(主持人)](../Page/李晨_\(主持人\).md "wikilink")、[吳大維](../Page/吳大維.md "wikilink")、[周瑛琦及](../Page/周瑛琦.md "wikilink")[丁嘵鋒等](../Page/丁嘵鋒.md "wikilink")。

目前，中国大陆部分地区的[有线](../Page/有线电视.md "wikilink")[数字电视用户可以收看到此频道](../Page/数字电视.md "wikilink")，但一些地区的运营商将此频道设为收费加密。例如[湖北楚天数字电视](../Page/湖北楚天数字电视.md "wikilink")、[浙江杭州数字电视](../Page/浙江杭州数字电视.md "wikilink")，需用户付费后，才能开通此频道的信号。

[中国国际电视总公司境外卫星代理部接收Channel](../Page/中国国际电视总公司.md "wikilink")
\[V\]中国频道信号，通过[亚太6号卫星发射ku波段信号](../Page/亞太衛星.md "wikilink")。该服务一般只提供给三星级以上的[酒店和政府指定的居住区](../Page/酒店.md "wikilink")。

## 其它的Channel \[V\]頻道

其他地區性的Channel \[V\]頻道还包括：

  - 泰國台
  - 澳洲台
  - 印度台

## 参见

  - [全球華語音樂榜中榜](../Page/全球華語音樂榜中榜.md "wikilink")

## 參考資料

## 外部链接

  - [泰國频道](http://www.channelvthailand.com/)

[Category:1991年成立的电视台或电视频道](../Category/1991年成立的电视台或电视频道.md "wikilink")
[V](../Category/星空傳媒.md "wikilink")
[Category:東南亞電視台](../Category/東南亞電視台.md "wikilink")
[Category:台灣已停播的電視頻道](../Category/台灣已停播的電視頻道.md "wikilink")
[Category:香港電視播放頻道](../Category/香港電視播放頻道.md "wikilink")
[Category:新加坡電視播放頻道](../Category/新加坡電視播放頻道.md "wikilink")
[Category:国际媒体](../Category/国际媒体.md "wikilink")

1.  [Press Releases:China Media Capital and News Corporation Enter
    Acquisition Agreement for STAR China's TV
    Business](http://www.newscorp.com/news/news_456.html)