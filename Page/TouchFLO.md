[TouchFlo.jpg](https://zh.wikipedia.org/wiki/File:TouchFlo.jpg "fig:TouchFlo.jpg")
**TouchFLO**是[智能手机廠商](../Page/智能手机.md "wikilink")[HTC開發的一種針對](../Page/HTC.md "wikilink")[Windows
Mobile平台上的使用者界面](../Page/Windows_Mobile.md "wikilink")，被内置在HTC
Touch系列手機上使用。其升级版为[TouchFLO
3D及](../Page/TouchFLO_3D.md "wikilink")[HTC
Sense](../Page/HTC_Sense.md "wikilink")。

TouchFLO為[軟體與](../Page/軟體.md "wikilink")[硬體兩部分](../Page/硬體.md "wikilink")，硬體方面採用最低2.8英寸的65536色[QVGA觸摸屏](../Page/QVGA.md "wikilink")。（上附高耐磨保護屏，擁有極其細小的點陣，通過這些點敏銳感應手指在屏幕上的劃動與點擊，感應屏的觸點數量應高於普通屏幕的30%），與其兼容的不低於200MHz的[CPU](../Page/CPU.md "wikilink")，擁有獨立[顯卡](../Page/顯卡.md "wikilink")。軟體方面即3D
Cube主界面與TouchFLO劃動感應驅動，兩者相互配合帶給了用家Windows Mobile的全新操控體驗。

TouchFLO是HTC智慧手機最為吸引人的因素之一，成功改變了人們對Windows
Mobile手機的呆板印象。這個技術也被視為[蘋果](../Page/蘋果.md "wikilink")[iPhone採用的](../Page/iPhone.md "wikilink")[Multi-Touch技術的競爭對手](../Page/多點觸控.md "wikilink")。雖然TouchFLO只是單點觸控，但已顯現出Multi-Touch的雛形；它使使用者擺脫了使用觸控筆操作屏幕的傳統方式，並且可以單手操作手機。不過也有部分觀點認為TouchFLO只是給Windows
Mobile加的一層殼，並沒有革命性的改變。有部分HTC愛好者修改了TouchFLO，替換了3D
Cube的界面，並將其移植到其他的HTC手機上。

## 支援机型

  - [HTC Touch](../Page/HTC_Touch.md "wikilink")
  - [HTC Touch Dual](../Page/HTC_Touch_Dual.md "wikilink")
  - [HTC Touch Cruise](../Page/HTC_Touch_Cruise.md "wikilink")
  - [HTC Touch Viva](../Page/HTC_Touch_Viva.md "wikilink")
  - [HTC Touch 3G](../Page/HTC_Touch_3G.md "wikilink")
  - [HTC Touch2](../Page/HTC_Touch2.md "wikilink")

## 參見

  - [TouchFLO 3D](../Page/TouchFLO_3D.md "wikilink")
  - [HTC Sense](../Page/HTC_Sense.md "wikilink")

## 外部連結

  - [HTC 宏達國際](http://www.htc.com/)
  - [HTC
    Touch](https://web.archive.org/web/20080623202900/http://www.htctouch.com/)

[Category:顯示科技](../Category/顯示科技.md "wikilink")
[Category:HTC](../Category/HTC.md "wikilink")