**Chrome 400**系列顯示卡是[威盛旗下子公司](../Page/VIA.md "wikilink")[S3
Graphics的產品](../Page/S3_Graphics.md "wikilink")，特色是其高清晰影片播放功能，和核心的低功耗。由於顯示核心只支援64-bit顯示記憶體，普遍認為VIA已調整策略，不再追求核心的效能，已轉注顯示卡的影像播放表現。首批Chrome
400系列顯示卡於2008年2月底推出，型號包括Chrome 430和Chrome 440等。

## 產品技術

Chrome 400 顯示核心採用 65 nm
製程，支援64-bit顯示記憶體頻寬，使用可程式化統一渲染架構，支援[微軟的](../Page/微軟.md "wikilink")[DirectX
10.1](../Page/DirectX.md "wikilink")，[OpenGL](../Page/OpenGL.md "wikilink")
2.1、[PCI-Express](../Page/PCI-Express.md "wikilink")
2.0規格。並且支援[Chromotion HD
2.0影片加速技術和](../Page/Chromotion.md "wikilink")[Power
Wise節能技術](../Page/Power_Wise.md "wikilink")。

由於[ATI和](../Page/ATI.md "wikilink")[NVIDIA的影像加速技術已支援新的高清格式](../Page/NVIDIA.md "wikilink")，而舊有的Chromotion影像引擎已不能滿足要求。所以VIA將Chromotion版本提升至HD
2.0，並新增支援對[H.264/MPEG-4
AVC和](../Page/H.264/MPEG-4_AVC.md "wikilink")[VC-1的硬體加速](../Page/VC-1.md "wikilink")。與NVIDIA的[PureVideo技術一樣](../Page/PureVideo.md "wikilink")，Chrome
400亦支援[AES](../Page/AES.md "wikilink") 128硬體解碼。

**Power
Wise**與ATI的[PowerPlay技術有异曲同工之妙](../Page/PowerPlay.md "wikilink")，採用新的演算法和功耗控制機制，能隨時保持性能與功耗之間的最佳平衡，以滿足筆記型電腦和小型PC對性能與功耗的雙重要求，並延長電池續航時間。

Chrome 400 顯示核心支援[HDMI輸出及內建](../Page/HDMI.md "wikilink")[HD
Audio控制器](../Page/HD_Audio.md "wikilink")，無須外接音效接線就可以提供HDMI連音效輸出，並支援[HDCP保護技術](../Page/HDCP.md "wikilink")，並且同時支援最新的[DisplayPort輸出](../Page/DisplayPort.md "wikilink")。

為了彰顯核心的GPGPU能力，S3
Graphics推出了S3FotoPro軟體。透過核心運算，可以提升圖片的質量，例如加強清晰度和饱和度。\[1\]

## 產品型號

### Chrome 430 GT

VIA Chrome
400系列第一張發佈的顯示卡。此卡的定位是入门级，核心頻率是625MHz；顯示記憶體採用了[DDR2規格](../Page/DDR2.md "wikilink")，頻率是1000MHz。與NVIDIA的顯示卡相似，核心和Stream
Processor的頻率是非同步的。而此卡的Stream Processor頻率是900MHz\[2\]。

### Chrome 440 GTX

是系列中的第二款顯示卡，核心頻率是725MHz；顯示記憶體採用了[GDDR3規格](../Page/GDDR3.md "wikilink")，但頻寬只有64-bit\[3\]。核心支援Chromotion影像引擎，用作加速視頻播放。與前作相比，加入了双高清影片播放功能，與[NVIDIA看齊](../Page/NVIDIA.md "wikilink")\[4\]。

### Chrome 400 ULP

在2008年9月，S3推出了流動產品線的Chrome 400
ULP系列低功耗[顯示卡](../Page/顯示卡.md "wikilink")。其中Chrome
430 ULP的最低功耗不足7w。支援[DirectX](../Page/DirectX.md "wikilink")
10.1，以及H.264、MPEG2、MPEG-4、VC-1、WMV-HD、AVS、AVC等高清晰[影片解碼](../Page/影片.md "wikilink")，支援藍光播放，PowerWise節能技術。

Chrome 400
ULP具備三種不同型號：針對[Netbook設計的Chrome](../Page/Netbook.md "wikilink")
430 ULP、針對輕薄手提電腦的Chrome 435 ULP以及針對台式機替代型手提電腦的Chrome 440 ULP。

## 另見

  - [威盛處理器列表](../Page/威盛處理器列表.md "wikilink")

## 參見

  - [威盛電子](../Page/威盛電子.md "wikilink")
  - [S3 Graphics](../Page/S3_Graphics.md "wikilink")
  - [VIA Envy](../Page/VIA_Envy.md "wikilink")
  - [ViRGE](../Page/S3_ViRGE.md "wikilink") 系列
  - [Savage](../Page/S3_Savage.md "wikilink") 系列
  - [DeltaChrome S8](../Page/DeltaChrome_S8.md "wikilink") 系列
  - [GammaChrome S18](../Page/GammaChrome_S18.md "wikilink") 系列
  - [Chrome S20](../Page/ChromeS20_Series.md "wikilink") 系列
  - [Chrome 500](../Page/Chrome_500系列显卡.md "wikilink") 系列

## 外部連結

  - [S3 Graphics Chrome 400
    Series](https://web.archive.org/web/20100213203907/http://www.s3graphics.com/en/products/class2.aspx?seriesId=1)
  - [PCPOP.COM -
    S3新DX10.1显卡解析测试](http://www.pcpop.com/doc/0/275/275431.shtml)

## 參考

[Category:威盛電子](../Category/威盛電子.md "wikilink")
[Category:顯示卡](../Category/顯示卡.md "wikilink")

1.  [S3发布GPGPU加速图像处理软件S3FotoPro](http://news.mydrivers.com/1/119/119031.htm)
2.  [S3重出江湖！全能DX10.1独立显卡发布](http://www.pcpop.com/doc/0/273/273007_2.shtml)
3.  [S3 Chrome 440
    GTX显卡详细规格](http://news.mydrivers.com/1/107/107517.htm)
4.  [S3发布Chrome 440 GTX显卡
    仅售69美元](http://news.mydrivers.com/1/107/107495.htm)