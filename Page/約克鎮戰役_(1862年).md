**約克鎮戰役
(1862年)**於1862年4月5日至5月4日發生在[維吉尼亞的](../Page/維吉尼亞.md "wikilink")[約克郡](../Page/約克郡.md "wikilink"),是[南北戰爭中北軍為奪得](../Page/南北戰爭.md "wikilink")[維吉尼亞半島而發動的一場戰役](../Page/維吉尼亞半島.md "wikilink")，稱為[半島會戰](../Page/半島會戰.md "wikilink")。

北軍軍官[麥克萊倫率](../Page/麥克萊倫.md "wikilink")146,000大軍向里奇蒙推進時，於約克鎮遇到11,000南軍，估計約克鎮之戰導致482人傷亡，但雙方都不是勝利者。而聯邦則繼續向維吉尼亞半島的里奇蒙前進。

北軍傷亡人數為182;南軍則有300人。

## 註腳

[Category:維吉尼亞歷史戰役](../Category/維吉尼亞歷史戰役.md "wikilink")
[Category:半島會戰戰役](../Category/半島會戰戰役.md "wikilink")
[Category:南北战争期间的弗吉尼亚州](../Category/南北战争期间的弗吉尼亚州.md "wikilink")