**[九龍巴士](../Page/九龍巴士.md "wikilink")868線**是[香港一條馬場巴士路線](../Page/香港.md "wikilink")，往來[沙田馬場及](../Page/沙田馬場.md "wikilink")[屯門市中心](../Page/屯門.md "wikilink")，以屯門區及荃灣區居民為主要服務對象。

此路線為歷史上非空調收費最高的專營巴士路線，於1995年4月2日至6月下旬期間全程收費為$30.6。

## 歷史

  - 1991年4月13日：本線投入服務，提供接載屯門及[荃灣區的馬迷往來沙田馬場的巴士服務](../Page/荃灣區.md "wikilink")，只在沙田馬場賽馬日開出。
  - 1995年9月10日：提升為全空調巴士服務，全程收費維持不變。
  - 2009年9月13日：往沙田馬場之服務改為祇於假日的沙田賽馬日提供。

## 服務時間及班次

  - 沙田馬場開

<!-- end list -->

  - 於沙田馬場賽馬日尾場賽事開跑前一小時起至完場後一小時提供服務，客滿即開。

<!-- end list -->

  - 屯門市中心開

<!-- end list -->

  - 賽馬日固定一班：11:40

## 車費

全程：$38.9

  - 由[荃景圍往](../Page/荃景圍.md "wikilink")[沙田馬場](../Page/沙田馬場.md "wikilink")：$27.7
  - [城門隧道轉車站往](../Page/城門隧道.md "wikilink")[沙田馬場](../Page/沙田馬場.md "wikilink")：$21.3

## 行車路線

**屯門市中心開**經：[屯門鄉事會路](../Page/屯門鄉事會路.md "wikilink")、[屯興路](../Page/屯興路.md "wikilink")、[屯門公路](../Page/屯門公路.md "wikilink")、[青山公路](../Page/青山公路.md "wikilink")—荃灣段、[城門道](../Page/城門道.md "wikilink")、[西樓角路](../Page/西樓角路.md "wikilink")、[蕙荃路](../Page/蕙荃路.md "wikilink")、[荃錦交匯處](../Page/荃錦交匯處.md "wikilink")、[象鼻山路](../Page/象鼻山路.md "wikilink")、[城門隧道](../Page/城門隧道.md "wikilink")、[城門隧道公路](../Page/城門隧道公路.md "wikilink")、[大埔公路](../Page/大埔公路.md "wikilink")—沙田段、[火炭路](../Page/火炭路.md "wikilink")、[源禾路](../Page/源禾路.md "wikilink")、大埔公路—沙田段及沙田馬場通道。

**沙田馬場開**經：沙田馬場通道、大埔公路—沙田段、城門隧道公路、城門隧道、象鼻山路、荃錦交匯處、蕙荃路、[廟崗街](../Page/廟崗街.md "wikilink")、城門道、青山公路—荃灣段、屯門公路、[屯喜路及](../Page/屯喜路.md "wikilink")[屯匯街](../Page/屯匯街.md "wikilink")。

### 沿線車站

[868RtMap.png](https://zh.wikipedia.org/wiki/File:868RtMap.png "fig:868RtMap.png")

| [屯門市中心開](../Page/屯門市中心.md "wikilink") | [沙田馬場開](../Page/沙田馬場.md "wikilink")      |
| ------------------------------------- | ---------------------------------------- |
| **序號**                                | **車站名稱**                                 |
| 1                                     | [屯門市中心巴士總站](../Page/屯門市中心.md "wikilink") |
| 2                                     | [荃景圍天橋](../Page/荃景圍.md "wikilink")       |
| 3                                     | [愉景新城](../Page/愉景新城.md "wikilink")       |
| 4                                     | [荃灣站](../Page/荃灣站.md "wikilink")         |
| 5                                     | 眾安街                                      |
| 6                                     | 城門隧道轉車站                                  |
| 7                                     | 沙田馬場巴士總站                                 |

## 參考書籍

  - 《二十世紀巴士路線發展史－－渡海、離島及機場篇》，容偉釗編著，BSI出版
  - 《巴士路線發展綱要(3)－－沙田．大埔．北區》，BSI(香港)，ISBN 9628414739005，172頁

## 外部連結

  - [九巴868線—九龍巴士](http://www.kmb.hk/chinese.php?page=search&prog=route_no.php&route_no=868)
  - [九巴868線—681巴士總站](http://www.681busterminal.com/868.html)
  - [九巴868線—i-busnet.com](http://www.i-busnet.com/busroute/kmb/kmbr868.php)

[868](../Category/九龍巴士路線.md "wikilink")
[868](../Category/沙田區巴士路線.md "wikilink")
[868](../Category/屯門區巴士路線.md "wikilink")
[868](../Category/香港賽馬日特別巴士路線.md "wikilink")