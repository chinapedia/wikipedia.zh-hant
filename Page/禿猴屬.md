**禿猴屬**（學名
*Cacajao*）屬於[新世界猴](../Page/新世界猴.md "wikilink")，[僧面猴科的一屬](../Page/僧面猴科.md "wikilink")，共有兩種：[黑面禿猴分佈在](../Page/黑面禿猴.md "wikilink")[亞馬遜河的北岸](../Page/亞馬遜河.md "wikilink")，[禿猴](../Page/禿猴.md "wikilink")（赤禿猴）則分佈于河的南岸。食物以水果和昆虫为主。

## 参考文献

[Category:僧面猴科](../Category/僧面猴科.md "wikilink")
[Category:禿猴屬](../Category/禿猴屬.md "wikilink")