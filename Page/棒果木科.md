[left](../Page/文件:Corynocarpus_laevigatus2.jpg.md "wikilink")
**棒果木科**（学名：）是[葫芦目中的一个科](../Page/葫芦目.md "wikilink")，只有1[属](../Page/属.md "wikilink")5-6[种](../Page/种.md "wikilink")，分布在[新几内亚](../Page/新几内亚.md "wikilink")、[新西兰和](../Page/新西兰.md "wikilink")[澳大利亚东部](../Page/澳大利亚.md "wikilink")。

## 分布

棒果木科植物本来出产于新几内亚、[瓦努阿图](../Page/瓦努阿图.md "wikilink")、[新喀里多尼亞](../Page/新喀里多尼亞.md "wikilink")、澳大利亚[昆士蘭州和新西兰](../Page/昆士蘭州.md "wikilink")。[夏威夷的](../Page/夏威夷.md "wikilink")是被人带到那里去的。

## 描述

本科[植物为常绿](../Page/植物.md "wikilink")[乔木或](../Page/乔木.md "wikilink")[灌木](../Page/灌木.md "wikilink")，除果肉外其它所有部位均非常有毒。单[叶互生](../Page/叶.md "wikilink")，革质，全缘，叶柄内有托叶。

[花小](../Page/花.md "wikilink")，辐射对称，异性同花，丛生在花梗上，[花瓣五](../Page/花瓣.md "wikilink")。

[果实为](../Page/果实.md "wikilink")[核果](../Page/核果.md "wikilink")，果内只有一枚核。

果实含有[黄酮类和苦的](../Page/黄酮类.md "wikilink")[葡萄糖苷](../Page/葡萄糖苷.md "wikilink")。

## 分类

棒果木科有一属，五个中、两个亚种（或者六个种）：

  - **
  - **
  - **（[毛利果](../Page/毛利果.md "wikilink")）
  - **
  - ''：
      - **
      - **

1981年的[克朗奎斯特分类法将本](../Page/克朗奎斯特分类法.md "wikilink")[科分入](../Page/科.md "wikilink")[卫矛目](../Page/卫矛目.md "wikilink")，1998年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
分类法将其列入](../Page/APG_分类法.md "wikilink")[葫芦目](../Page/葫芦目.md "wikilink")。

## 应用

毛利果的果实可生食，味道如过熟的桃子。核有剧毒，但是[毛利人通过多个步骤的加工后用它磨的麵粉来烤麵包](../Page/毛利人.md "wikilink")。种子含11%的蛋白质和58%的碳氢化合物。叶被毛利人用来医治创伤。白色的木头可燃，毛利人用它来制船。从这种植物中可以提取一种杀虫剂\[1\]。

### 参考资料

## 外部链接

  - <http://www.pfaf.org/database/search_name.php?ALLNAMES=Corynocarpus>
    （英语）
  - 在[L. Watson和M.J.
    Dallwitz（1992年）《有花植物分科》](http://delta-intkey.com/angio/)中的[棒果木科](http://delta-intkey.com/angio/www/corynoca.htm)
  - [APG
    网站中的棒果木科](http://www.mobot.org/MOBOT/Research/APWeb/orders/cucurbitalesweb.htm#Corynocarpaceae)
  - [NCBI中的棒果木科](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?lin=s&p=has_linkout&id=4310)

[\*](../Category/棒果木科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")

1.  [毛利果的使用](https://web.archive.org/web/20080201072117/http://www.geocities.com/RainForest/7109/karaka.htm)