\-{zh-hk:**海莉·迪·衛斯頓華**，又譯**海莉·薇思特拉**、;zh-hans:**海莉·迪·薇思特拉**，又譯;}-**海莉·韦斯特娜**、**海莉·韋斯頓**（**Hayley
Dee
Westenra**，），[新西兰古典](../Page/新西兰.md "wikilink")[跨界音乐](../Page/跨界音乐.md "wikilink")[女高音](../Page/女高音.md "wikilink")，歌曲作家，出生于新西兰[基督城](../Page/基督城.md "wikilink")。至今在亚洲她已成功发行过八张专辑，第一张《[纯](../Page/纯.md "wikilink")》（*Pure*）曾攀升到[英国](../Page/英国.md "wikilink")[古典音乐榜第一名的位置](../Page/古典音乐榜.md "wikilink")，并在全世界售出了200万张。

海莉·薇思特拉和英国歌唱家[莎拉·布萊曼](../Page/莎拉·布萊曼.md "wikilink")，意大利著名歌唱家[安德烈·波伽利等同属](../Page/安德烈·波伽利.md "wikilink")[古典音乐与](../Page/古典音乐.md "wikilink")[流行音乐的](../Page/流行音乐.md "wikilink")[跨界音乐歌唱家](http://www.classical-crossover.co.uk/artprofiles/125-hayley-westenra.html)。她曾是新西兰著名美声歌唱家[玛尔维娜·梅杰女爵士的学生](../Page/玛尔维娜·梅杰.md "wikilink")。在年仅16岁时她就成为了一名国际明星。

海莉也是一名[慈善家](../Page/慈善家.md "wikilink")，并在慈善事业上奉献了许多时间与钱财，比如她是新西兰[联合国儿童基金会的爱心大使](../Page/联合国儿童基金会.md "wikilink")，英国的皇家军队的爱心大使，还有残疾儿童帮助中心，新西兰的儿童音乐康复中心等。她有源自英国、荷兰、爱尔兰等的血統，包括有古代欧洲英国、法国等国的皇室血统，其祖父母上世纪初移民到新西兰。现在她大部分时间居住在英国伦敦。

2006年8月，海莉曾加入愛爾蘭民乐美聲女子組合——[凯尔特女人](../Page/凯尔特女人.md "wikilink")（又名[天使女伶](../Page/天使女伶.md "wikilink")、[美麗人聲](../Page/美麗人聲.md "wikilink")），并出现在了她们的第3张主要[DVD](../Page/DVD.md "wikilink")/[CD大碟](../Page/CD.md "wikilink")
《Celtic Woman: A New Journey》Live at Slane
Castle裡。但因支氣管炎於2007-2008時退出[凯尔特女人](../Page/凯尔特女人.md "wikilink")。

## 早年

海莉·薇思特拉是吉尔和杰拉德·薇思特拉夫妇的女儿，她有一个弟弟一个妹妹，分别叫蘇菲（Sophie）和伊萨克（Isaac）。海莉出生于一个音乐世家。她的祖母是一名[歌唱家](../Page/歌唱家.md "wikilink")，祖父是一名[钢琴师](../Page/钢琴师.md "wikilink")，并会弹奏[手风琴](../Page/手风琴.md "wikilink")。

她的音乐之路从六岁开始。当时在她就读的小学的一次[圣诞演出里她扮演](../Page/圣诞.md "wikilink")“最小的星”（the
Littlest
Star）一角，并有一齣唱戲。在那次表演之后，一位老师发掘了她的天賦，联系了她的父母。（事实上，她经常被叫做“女[乔許·葛洛班](../Page/乔許·葛洛班.md "wikilink")”。）海莉的老师鼓励她学习一件乐器以提升她的乐感。于是海莉便开始学习小提琴。在学小提琴的同时，海莉还练起了钢琴並录音，并在七岁时就能辨出音符。随后她开始了声乐学习，并燃起了对[音乐剧的热情](../Page/音乐剧.md "wikilink")。到11岁时，海莉已上台演出近40次。

她回憶說："我經常得演男孩演的角色，由於芭蕾舞劇中往往男孩不足，因此，我常常拿到假髮以及灰色西裝而非漂亮的衣服。在聖誕頌歌時，我得扮演一个病得很重的小提姆角色，在嚴重欠缺男孩的情況裡，我總是擔任男孩的角色，當然這很令人失望。\[1\]"

12岁时，海莉去了一间专业的录音室录制了《Walking in the
Air》。这是一张分发送给家人朋友的初定碟。当时制作了1000份副本。在录完那张碟后，海莉就和她的歌手妹妹一起去基督城的街头演唱。这一对姐妹吸引了一大群人驻足欣赏，其中一名CTV女记者邀请海莉去电视台作表演。一个演唱推广公司看到了这个节目，便把海莉签约到了新西兰[环球唱片公司](../Page/环球唱片.md "wikilink")。在此，海莉发行了一张以自己名字命名的唱片，其中包括一些很经典的曲目。之后又发行了一张圣诞大碟《My
Gift to You》。

## 歌唱事业

[Hayley_Westenra_Manchester.jpg](https://zh.wikipedia.org/wiki/File:Hayley_Westenra_Manchester.jpg "fig:Hayley_Westenra_Manchester.jpg")
尽管海莉在新西兰国内已经十分成功，可是她在国际上成名则是在签约[迪卡唱片公司](../Page/迪卡唱片公司.md "wikilink")，发行她第一张全球发行的唱片《纯》（*Pure*）这张融合[古典](../Page/古典.md "wikilink")、[轻流行和](../Page/轻流行.md "wikilink")[毛利族传统曲风的唱片之后才开始的](../Page/毛利族.md "wikilink")。《纯》成为了英国古典音乐榜历史上销量增长最快的[处女专辑](../Page/处女专辑.md "wikilink")，在发行的第一周《纯》便获得19068张的销量，升至英国古典音乐榜的首位，并进入到英国流行音乐榜上第八名的位置。迄今《纯》已经卖出了200万张，成为迄今为止二十一世纪最畅销的古典音乐专辑。在海莉的家乡新西兰，《纯》被授予12白金的荣誉，这也使得海莉成为该国历史上所有[音乐流派中唱片销量最好的歌手](../Page/音乐类型.md "wikilink")。

海莉曾经在一次演唱会上为[布莱尔](../Page/布莱尔.md "wikilink")[首相](../Page/首相.md "wikilink")、[布什](../Page/乔治·沃克·布什.md "wikilink")[总统和](../Page/总统.md "wikilink")[英国女王献艺](../Page/英国女王.md "wikilink")。在发行了《纯》之后，海莉和她的家人移居至了英国伦敦。在2006年的绝大多数时间裡，海莉都在为她的唱片推广作[巡回演出](../Page/巡回演出.md "wikilink")。她的父母後来又回到了家乡基督城。海莉每有空闲即尽可能地回乡探访。

海莉为[迪士尼电影](../Page/迪士尼.md "wikilink")《[花木兰2](../Page/花木兰2.md "wikilink")》录制了主题曲。她的首次银幕演出是在[全国广播公司](../Page/全国广播公司.md "wikilink")（[NBC](../Page/NBC.md "wikilink")）电视剧《[American
Dreams](../Page/American_Dreams.md "wikilink")》第46回里扮演一名酒吧里的歌手。剧中海莉手弹吉它唱着《[Who
Painted the Moon
Black?](../Page/Who_Painted_the_Moon_Black?.md "wikilink")》。[美国公共电视网](../Page/美国公共电视网.md "wikilink")（[PBS](../Page/PBS.md "wikilink")）的节目组《[Great
Performances](../Page/Great_Performances.md "wikilink")》也为她在新西兰着身定制了高品质DVD《[海莉·薇思特拉新西兰现场演唱会](../Page/海莉·薇思特拉新西兰现场演唱会.md "wikilink")》(Hayley
Westenra Live From New Zealand).

海莉是2004年[沃达丰杯](../Page/沃达丰.md "wikilink")[新西兰音乐奖之](../Page/新西兰音乐奖.md "wikilink")"[新西兰最高销量唱片奖](../Page/新西兰最高销量唱片奖.md "wikilink")"和"[国际成就奖](../Page/国际成就奖.md "wikilink")"的得主。她还赢得过两个[日本葛莱美奖](../Page/日本葛莱美奖.md "wikilink")（年度单曲《[奇异恩典](../Page/奇异恩典.md "wikilink")》（*Amazing
Grace*）和年度唱片《纯》）。同样在2004年，她开始了她在新西兰、[澳大利亚](../Page/澳大利亚.md "wikilink")、日本和英国的世界[巡演](../Page/巡演.md "wikilink")。她在[雪梨歌剧院](../Page/雪梨歌剧院.md "wikilink")（即悉尼歌剧院）演出之时，全场座无虚席，场面极为罕见。2004年的最后一场巡演海莉应邀与[波士顿交响乐团共同演出](../Page/波士顿交响乐团.md "wikilink")，至此海莉成功地以一场圣诞演出在[美国东海岸结束了她](../Page/美国东海岸.md "wikilink")2004年的巡演。

2005年，海莉为[电影](../Page/电影.md "wikilink")[威尼斯商人录制演唱了歌曲](../Page/威尼斯商人.md "wikilink")《[Bridal
Ballad](../Page/Bridal_Ballad.md "wikilink")》。同年，海莉发行了一张全新的国际专辑《[奇幻历险](../Page/奇幻历险.md "wikilink")》（*Odyssey*），寓意其成功的音乐之路。这张专辑融合了古典、[流行](../Page/流行.md "wikilink")、[凯尔特传统和](../Page/凯尔特.md "wikilink")[新世纪等流派](../Page/新世纪.md "wikilink")。海莉又亲自重写和编排了其中的几首歌，并于10月18日发行了美国版。最近的一个版本是2006年4月10日发行的英国版，其中添加了几首最初版本裡没有的新歌。12月18日，海莉出现在全国广播公司的Kurt
Browning溜冰表演节目上，并同[安德烈·波切利](../Page/安德烈·波切利.md "wikilink")([Andrea
Bocelli](../Page/Andrea_Bocelli.md "wikilink"))共同演出。\[2\]

2006年，海莉又在电影《[新大陆](../Page/新大陆.md "wikilink")》（*The New
World*）的[原声碟裡演唱了](../Page/原声碟.md "wikilink")《听风》（*Listen
to the
Wind*）这首歌。她2006年的许多时间都花在了同流行[歌剧四重唱](../Page/歌剧.md "wikilink")[美声男伶](../Page/美声男伶.md "wikilink")(Il
Divo)组合一起的世界巡演上。在3月份的时候，她在美国也有过一些自己的专场演出。这次巡演原定结束时间是6月，可却又延迟了6天时间，为此海莉失去了一次回家乡新西兰的机会。她正安排在夏季结束前在英国和[德国举办几场](../Page/德国.md "wikilink")[演唱会](../Page/演唱会.md "wikilink")。9月5日,
海莉被 Junior Chamber International 评为年度全世界十佳年轻人奖项,
成为[新西兰第一个获此殊荣的人](../Page/新西兰.md "wikilink").
10月27日, 海莉和英国男高音艾尔菲(Alfie
Boe)在英国中世纪宗教圣地[坎特伯雷座堂举办演唱会](../Page/坎特伯雷座堂.md "wikilink").\[3\]
11月13日, 海莉代表新西兰参加了在英国伦敦举办的二次世界大战的纪念活动, 她的三位叔祖父代表英联邦服役此战, 其中一位不幸牺牲.\[4\]
同年圣诞节期间, 海莉在英国[威尔士千喜年中心](../Page/威尔士.md "wikilink")(the city's Wales
Millennium Centre), 与 Cardiff Howell's Junior School
青少年和儿童唱诗班同台演出圣诞颂歌节目,
此演唱会在ITV实况转播.\[5\]

2007年,
海莉曾加入了[爱尔兰民乐美声女子組合](../Page/爱尔兰.md "wikilink")—[凯尔特女士](../Page/凯尔特.md "wikilink")(又名天使女伶,
美丽人生)在[美国的音乐会巡演](../Page/美国.md "wikilink"), 并出现在了她们的第二张主要DVD/CD大碟
《Celtic Woman - A New Journey: Live at Slane Castle》里. 3月,
海莉在美国推出新的国际专辑《[凯尔特宝贝](../Page/凯尔特宝贝.md "wikilink")》(Celtic
Treasure),于美国[纽约献唱](../Page/纽约.md "wikilink"). 5月6日,
海莉在美国[水晶大教堂献唱赞美歌主与我同在](../Page/水晶大教堂.md "wikilink")(Abide
With Me). 7月，她参与录制了[伯恩斯坦](../Page/李奧納德·伯恩斯坦.md "wikilink")(Leonard
Bernstein)的大作——伟大的[音乐剧](../Page/音乐剧.md "wikilink")[西区故事的](../Page/西区故事.md "wikilink")50周年重录，该专辑被提名第50届[葛莱美奖](../Page/葛莱美奖.md "wikilink")。
年底，海莉在[日本的](../Page/日本.md "wikilink")[福冈](../Page/福冈.md "wikilink")，[札幌](../Page/札幌.md "wikilink")，[东京](../Page/东京.md "wikilink")，[仙台](../Page/仙台.md "wikilink")，[名古屋](../Page/名古屋.md "wikilink")，[横滨](../Page/横滨.md "wikilink")，[广岛](../Page/广岛.md "wikilink")，[大阪等城市成功举行了](../Page/大阪.md "wikilink")“宝贝”系列演唱会。\[6\]\[7\]

2008年6月，海莉在日本推出新的亚洲地区专辑《Hayley sings Japanese
Songs》。10月，海莉在英国和新西兰推出一张精选专辑《River
of
Dreams》。11月，海莉在英国[皇家阿尔伯特音乐厅举办的英国皇家军队纪念日的演唱会中](../Page/皇家阿尔伯特音乐厅.md "wikilink")，演唱了
"River of Dreams" 并且与英国男高音乔纳森(Jonathan Ansell)合唱了"Today Won't Come
Again"。\[8\] 11月，海莉作为世界闻名的女高音在英国Trafalgar Square举办的纪念英国皇家军队第90周年的停战日献唱。

2009年3月，海莉在日本推出另一张亚洲地区专辑《Hayley sings Japanese Songs 2》。5月23日,
海莉在[苏格兰格拉斯哥皇家大会堂](../Page/苏格兰.md "wikilink")(Glasgow
Royal Concert Hall)成功举行了演唱会.\[9\] 7月16日,
海莉为台湾高雄[世运会开幕献唱](../Page/世运会.md "wikilink"),
她用[英语演唱了经典名曲](../Page/英语.md "wikilink")[奇异恩典](../Page/奇异恩典.md "wikilink")(Amazing
Grace), 用英语与众明星合唱了We Are The Champions,
用[意大利语合唱了The](../Page/意大利语.md "wikilink")
Prayer, 用新西兰的[毛利语合唱了Pokarekare](../Page/毛利语.md "wikilink") Ana,
还演唱了翻译自[日语的Nada](../Page/日语.md "wikilink") Sousou,
用[中文合唱了月亮代表我的心](../Page/中文.md "wikilink"),
全场约4万5千观众被她美妙的歌声所轰动.\[10\] 10月3日,
海莉中秋節時在[台北國家戲劇院演唱Ave](../Page/台北.md "wikilink") Maria(萬福瑪麗亞),
A Thousand Winds(化作千風), Danny Boy(丹尼男孩), Hana, Hine E Hine (女孩啊,
女孩)...等曲目, 並再次演唱月亮代表我的心, 淚光閃閃, Pokarekare Ana, The Prayer.
10月，海莉还在[日本](../Page/日本.md "wikilink")[东京](../Page/东京.md "wikilink")，[神户](../Page/神户.md "wikilink")，[名古屋](../Page/名古屋.md "wikilink")，[埼玉县](../Page/埼玉县.md "wikilink")，[横滨成功举行演唱会](../Page/横滨.md "wikilink")。\[11\]
\[12\] 2009年圣诞节节期，海莉在[英国成功举行魅力冬日](../Page/英国.md "wikilink")(Winter
Magic)演唱会。\[13\]

2010年7月7日至8月2日，海莉作为英国的皇家军队的爱心大使参加了英国战役70周年纪念活动和巡回演出。\[14\]
10月27日，11月1日，海莉在[台湾](../Page/台湾.md "wikilink")[高雄](../Page/高雄.md "wikilink")，台湾[台北举办演唱会](../Page/台北.md "wikilink")，仅高雄演唱会就有近1.5万观众参加，场面空前壮观。海莉从忙碌的英国[伦敦](../Page/伦敦.md "wikilink")，[意大利日程中安排了这次东方之行](../Page/意大利.md "wikilink")。10月30日，海莉在[上海大剧院成功举行演唱会](../Page/上海大剧院.md "wikilink")，她演唱了海莉商标式的赞美歌[奇异恩典](../Page/奇异恩典.md "wikilink")(Amazing
Grace)，祈祷(Prayer)，斯卡堡集市(Scarborough Fair)
等经典曲目，当她用标准的中文演唱完第二次谢幕的中国传统民歌月亮代表我的心，全场观众起立喝彩。\[15\]
2010年圣诞节节期，海莉在[曼彻斯特](../Page/曼彻斯特.md "wikilink")(Manchester Bridgewater
Hall)，[格拉斯哥](../Page/格拉斯哥.md "wikilink")(Glasgow Royal Concert
Hall)，[伯明翰](../Page/伯明翰.md "wikilink")(Birmingham
Sun)，[伦敦](../Page/伦敦.md "wikilink")(London – Barbican)
成功举行了[圣诞节的祝愿巡回演唱会](../Page/圣诞节.md "wikilink")。2010年，海莉在[意大利](../Page/意大利.md "wikilink")[罗马与世界著名作曲家](../Page/罗马.md "wikilink")[恩尼奥·莫里科内合作了一张新的国际专辑](../Page/恩尼奥·莫里科内.md "wikilink")《[天堂之音](../Page/天堂之音.md "wikilink")》。恩尼奥·莫里科内不仅为此专辑谱写新歌和他的成名作品，还安排他在罗马的管弦乐队为此专辑配乐，他高度评价海莉是当今世界乐坛罕见的人才，非常赏识她的歌唱天赋和语言天赋。\[16\]

2011年4月18日，新的国际专辑《[天堂之音](../Page/天堂之音.md "wikilink")》自新西兰开始向全球发布。\[17\]\[18\]
5月,
海莉作为特邀嘉宾参加了[意大利男高音](../Page/意大利.md "wikilink")[安德烈·波伽利在亚洲举行的真爱奇迹巡回演唱会](../Page/安德烈·波伽利.md "wikilink")，他们曾经在海莉的国际专辑《[奇幻历险](../Page/奇幻历险.md "wikilink")》和实况转播节目中有过合作。\[19\]
\[20\]\[21\]
7月，海莉被英国ITV广播公司命名为[橄榄球世界杯之音](../Page/世界盃橄欖球賽.md "wikilink")，而且还联手多位世界级音乐家用五种不同语言为世界杯录制了官方专辑《全球一家亲》。\[22\]\[23\]
7月31日，海莉在荷兰小提琴家[安德烈·瑞欧的](../Page/安德烈·瑞欧.md "wikilink")《永恒圆舞曲》专辑中献唱了由安德烈亲自谱写的《梦回新西兰》。\[24\]

2011年10月23日，
海莉在橄榄球世界杯决赛上领唱“[上帝保佑新西兰](../Page/天佑新西兰.md "wikilink")”，随后与法国队的决赛中，[新西兰国家橄榄球队赢得了世界冠军](../Page/新西兰国家橄榄球队.md "wikilink")。

2011年9月17日至2012年3月8日，海莉将横跨东西南北中在英国，新西兰 [基督城](../Page/基督城.md "wikilink")，
[汉密尔顿](../Page/汉密尔顿_\(新西兰\).md "wikilink")，
[奧克蘭](../Page/奧克蘭都會區.md "wikilink")，
[惠灵顿](../Page/惠灵顿.md "wikilink")，
[坎特伯雷](../Page/坎特伯雷_\(紐西蘭\).md "wikilink")，
[但尼丁](../Page/但尼丁.md "wikilink")，
[北帕莫斯顿](../Page/北帕莫斯顿.md "wikilink")，
[內皮爾](../Page/內皮爾_\(紐西蘭\).md "wikilink")，
[新普利茅斯](../Page/新普利茅斯.md "wikilink")，
[因弗卡吉尔](../Page/因弗卡吉尔.md "wikilink") 和
[台南](../Page/台南市.md "wikilink")，[台中](../Page/台中市.md "wikilink")，[台北等地举行天堂美声巡迴演唱会](../Page/臺北市.md "wikilink")。\[25\]\[26\]\[27\]\[28\]\[29\]\[30\]

2012年4月28日，
海莉，男高音丁毅和男高音聂建华三大歌唱家与英国皇家爱乐交响乐团在[北京奥林匹克公园的第二届](../Page/奥林匹克公园_\(北京\).md "wikilink")[北京国际电影节交响音乐会上同台演出](../Page/北京国际电影节.md "wikilink")。\[31\]\[32\]\[33\]\[34\]\[35\]
\[36\]\[37\] 5月，
海莉在庆祝[伊丽莎白二世登基钻禧纪念的纪念专辑中录制了](../Page/伊利沙伯二世登基鑽禧紀念.md "wikilink")《奇异恩典》.\[38\]
11月，海莉应邀参加了《巨星闪耀：[大卫·福斯特和他的朋友们](../Page/大卫·沃尔特·福斯特.md "wikilink")》2012亚洲巡演。\[39\]\[40\]\[41\]
\[42\]

2013年4月， 海莉在台北國家音樂廳、臺南億載金城、高雄至德堂举行了巡迴演唱会。\[43\]\[44\]\[45\]\[46\]
4月29日，海莉携手中国旅澳著名男高音歌唱家丁毅在人民大会堂正式启动由北京演艺集团主办，北京演艺集团、北京京演文化传媒有限责任公司共同出品，北京京演体育文化发展有限公司协办的“美妙的和谐”――全球巡回演唱会的第一站。\[47\]\[48\]\[49\]\[50\]\[51\]
11月30号和12月1号，海莉在香港携手香港城市室樂團和唱诗班举行了演唱会。\[52\]\[53\]

2017年4月15日，海莉参与演出了[湖南卫视的热播综艺节目](../Page/湖南卫视.md "wikilink")[《歌手》第五季第十三期](../Page/我是歌手.md "wikilink")，与台湾歌手[林志炫搭档](../Page/林志炫.md "wikilink")，同台联袂演唱欧美经典曲目[《The
Prayer》](http://www.mgtv.com/l/100002146/3901746.html)。

## 公益与慈善

海莉热心于公益和慈善事业，并且做了许多贡献：

2003年，16岁的海莉成为当时世界上最年轻的[联合国儿童基金会的爱心大使](../Page/联合国儿童基金会.md "wikilink")。[新西兰联合国儿童基金会](http://www.unicef.org.nz/)的负责人评价海莉是奇妙的年轻人楷模，可以用她的音乐天赋感召世人对世界儿童成长的关心和帮助。\[54\]
11月, 海莉在英国[皇家阿尔伯特音乐厅举办的英国皇家军队纪念日的演唱会中](../Page/皇家阿尔伯特音乐厅.md "wikilink"),
献唱了开幕曲「Tonight, We'll Light A Candle」，并且參與合唱最后的赞美歌「The Day Thou Gavest」。

2005年，海莉积极为「单车帮助加纳女孩上学」项目募捐，
当时许多[非洲](../Page/非洲.md "wikilink")[加纳女孩因上学路途遥远](../Page/加纳.md "wikilink")，走读不方便且容易发生被性侵犯而不去上学。海莉通过她在新西兰联合国儿童基金会的各方影响力，
在乐迷支持者们（如2003年9月22日在英国圣詹姆士教堂的演唱会； 2007年6月8日在英国维多利亚大会堂的演唱会等）和伦敦Alfred
Salter School学校师生的帮助下，
这个新西兰联合国儿童基金会的项目帮助了约6,000非洲加纳女孩上学。\[55\]\[56\]同年海莉成为新西兰音乐康复中心(The
Raukatauri Music Therapy Centre)的资助人。\[57\]

2006年，一款在新西兰塔斯曼海湾种植的清新美丽的[玫瑰花以海莉的名字命名](../Page/玫瑰花.md "wikilink")，这个慈善项目的收益贡献给[联合国儿童基金会](../Page/联合国儿童基金会.md "wikilink")。\[58\]海莉玫瑰随后被评为2010年度玫瑰奖和极品茶花玫瑰奖！\[59\]\[60\]同年，她出席了英国举办的反对孩童[霸凌](../Page/霸凌.md "wikilink")（Act
Against Bullying）的慈善活动，成为Global Angels 慈善机构的大使，并参加了在伦敦Cadogan
Hall举行的慈善演唱会。\[61\]

2007年，海莉成为the Women's Environmental Network
的总干事。\[62\]2月24日，海莉参加了在伦敦Cadogan
Hall举行的由英国公主亚历山德拉（Alexandra）做为贊助者的“孩子帮助孩子”的慈善演出。4月9日，海莉在英国Cadogan
Hall与[皇家爱乐乐团](../Page/皇家爱乐乐团.md "wikilink")（Royal Philharmonic
Orchestra）Debbie Wiseman Conductor/Composer, Stephen Fry
Narrator成功举行了Different Voices演唱会。

2008年，海莉被新西兰授予the Global Kiwis Young Achiever Award
奖项，她还得到过许多国际上的奖项，包括在伦敦举办的the
Variety Club's annual awards，2008年的年度古典音樂家（classical performer of the
year）奖项。海莉还成為香港“拯救孩子”项目的大使，也是新西兰抗乳腺癌组织成员，还支持了英国the Headway
Centre的残疾人项目。4月25日，海莉代表Poppy
Appeal慈善机构在英军司令部接受了50位摩托车骑士捐赠的10,000英镑的支票。6月25日，海莉成为Classic
FM
的音乐制作慈善活动的大使。在许多音乐界名流参加的[伦敦](../Page/伦敦.md "wikilink")[梅费尔的艺术俱乐部](../Page/梅费尔.md "wikilink")（The
Arts Club in Mayfair）举办的招待会上，海莉和乌鸦弦乐四重奏（[The Raven
Quartet](http://www.ravenquartet.co.uk/)）为大家演出节目。当晚，海莉还代表Classic
FM和西蒙一起向The Prince's Foundation for Children & The
Arts儿童慈善机构捐赠100,000英镑的支票。\[63\]
9月，海莉在她父亲杰拉尔德和新西兰联合国儿童基金会成员的陪同下再次到非洲加纳为解决当地儿童饮用干净水和学校运动场设施问题而集资募捐。\[64\]
9月4日，海莉成为[新西兰皇家空军的赞助人](../Page/紐西蘭國防軍.md "wikilink")。9月，海莉受邀请在英国陆军官校（Sandhurst）的
Music On Fire at
Sandhurst慈善活动中献唱，此次慈善活动的收益投入从[伊拉克](../Page/伊拉克.md "wikilink")、[阿富汗退役的](../Page/阿富汗.md "wikilink")[退伍军人的基金](../Page/退伍军人.md "wikilink")。

10月，海莉在伊拉克参加了Poppy Appeal 慈善机构组织的的慈善活动。\[65\]10月31日，海莉和英国男高音乔纳森（Jonathan
Ansell）在[滑铁卢车站为Poppy](../Page/滑铁卢车站.md "wikilink")
Appeal慈善机构的募捐献唱。\[66\]

2009年2月27日，[英国女王在伦敦为新的英军司令部开幕](../Page/伊丽莎白二世.md "wikilink")，海莉作为支持者参加了开幕活动。

3月10日，海莉参加了在[日本](../Page/日本.md "wikilink")[东京孤儿院](../Page/东京.md "wikilink")（seibi
children's home tokyo）举办的慈善活动。

9月7日，海莉成为著名的自二战开始就被命名为英国军队爱心大使的维拉女爵士所创办的the Dame Vera Lynn School
残疾儿童学校的副校长。\[67\]

10月，海莉与最早的[英军爱心大使维拉女爵士开始了这一年的Poppy](../Page/英军.md "wikilink") Appeal
慈善活动。

11月，海莉参加了著名流行歌手Ronan为纪念他的乳腺癌去世的母亲而创办的乳腺癌研究机构慈善募捐演唱会。\[68\]

11月，海莉在英国[皇家阿尔伯特音乐厅举办的英军纪念日的演唱会中](../Page/皇家阿尔伯特音乐厅.md "wikilink")，与著名的the
Fron Male Voice Choir唱诗班合唱了 "We'll Meet Again" 。

12月10日，海莉参加了英国[利物浦基督座堂的圣诞节颂歌慈善音乐会](../Page/利物浦基督座堂.md "wikilink")，她与，Jimmy
Tarbuck，教堂唱诗班同台演出。\[69\] \[70\]

12月，海莉作为新的英軍的爱心大使，在英国[St Clements
Church](http://www.stclementchurch.org/)教堂，与英軍官兵，英国女中音歌手Faryl
Smith和女高音歌手Camilla
Kerslake为正在[阿富汗服役的部队联合录制了圣诞节颂歌的节目](../Page/阿富汗.md "wikilink")。\[71\]

2009年和2010年，海莉参加了BBC Children In
Need慈善募捐活动，在BBC1的一期英国电视慈善节目中，海莉和英国流行歌手Lee
Mead举着一张募捐而来的1,869,394英镑的大支票。

2010年3月，海莉在伦敦应邀参加了由 [FINANCIAL MAIL WOMEN'S FORUM](http://www.fmwf.com/)
主办的以帮助年轻妇女面对社会挑战为主题的会议，她作为成功的榜样向各界与会者发言。

6月20日，海莉作为英军的爱心大使，为了支持[英国皇家空军慈善基金](../Page/英国皇家空军.md "wikilink")，参加了在[科茨沃尔德举办的航空展览](../Page/科茨沃尔德.md "wikilink")。\[72\]

11月，海莉在英国[皇家阿尔伯特音乐厅举办的英军纪念日的演唱会中](../Page/皇家阿尔伯特音乐厅.md "wikilink")，献唱了著名威尔士作曲家Karl
Jenkin的最新作曲的歌"For The Fallen"。 \[73\]
她还在英国北爱尔兰的[贝尔法斯特举办的](../Page/贝尔法斯特.md "wikilink")
[BBC](../Page/BBC.md "wikilink") [Children in
Need](http://www.bbc.co.uk/pudsey/)（BBC待助孩童慈善基金）慈善演唱会中献唱。\[74\]

2011年3月18日，海莉在新西兰基督城举行的纪念仪式上清唱了[奇异恩典](../Page/奇异恩典.md "wikilink")，包括[威廉王子](../Page/威廉王子_\(威爾斯\).md "wikilink")、[新西兰](../Page/新西兰.md "wikilink")、[澳大利亚的政府首脑](../Page/澳大利亚.md "wikilink")、新西兰[基督教领袖](../Page/基督教.md "wikilink")、[玛尔维娜·梅杰女爵士](../Page/玛尔维娜·梅杰.md "wikilink")、[基督城唱诗班和来自世界各国的搜救团队等在内的数万人参加了此次聚会](../Page/基督城.md "wikilink")，旨在纪念发生在二月份地震中的遇难者和重建家园。\[75\]
3月27日，海莉在伦敦[西敏寺为新西兰基督城举行的纪念仪式上作见证](../Page/西敏寺.md "wikilink")，包括[威爾士親王](../Page/威爾士親王.md "wikilink")，、新西兰政府官员、教堂神职人员等在内的数千人参加了此次教堂聚会，旨在纪念发生在二月份地震中的遇难者和感谢[英国对新西兰重建家园的慷慨援助](../Page/英国.md "wikilink")。\[76\]
4月2日，海莉在享有声望的伦敦Lancaster London Hotel举办的慈善晚会献唱，为维拉女爵士所创办的the Dame Vera
Lynn School 残疾儿童学校募捐。 \[77\] 5月29日，
海莉应邀参加了由PBS举办的2011年美国国家纪念日音乐会的演出。\[78\]
海莉于下半年在新西兰举办了“天堂美声”巡回演唱会和筹划慈善事宜帮助重建家园。\[79\]

## 唱片

  - 《Walking in the Air》（2000）
  - 《Hayley Westenra》（2001）
  - 《My Gift to You》（2001）
  - 《pure》(2004)
  - 《Odyssey》 (2005)
  - 《West side story》 (2007)
  - 《Prayer》(2007)
  - 《Treasure》(2007)
  - 《River of dreams》(2008)
  - 《Hayley Sings Japanese Songs》（2008）
  - 《Hayley Sings Japanese Songs 2》（2009）
  - 《Winter magic》(2009)
  - 《The Best Of Pure
    Voice》（2010）2010年，海莉的《最纯净的歌声》专辑取得了在台湾唱片市场的“西洋专辑年度排行榜”中第七名。
  - 《Paradiso》（2011）
  - 《The Best of - Hayley Sings Japanese Songs 》（2012）

**全球发行**

  - 《[纯](../Page/纯.md "wikilink")》（*Pure*）（2003）（第一张大洋洲以外发行的唱片）新西兰第1，澳大利亚第7，英国第7，美国第70
  - 《[奇幻历险](../Page/奇幻历险.md "wikilink")》（*Odyssey*）（2005）（8月8日新西兰首发国际版，10月18日发行美国版，次年4月10日发行英国版）新西兰第1，英国第10
  - 《[瑰宝](../Page/凯尔特瑰宝.md "wikilink")》（*Treasure*）（2007年2月26日）（英国第9，新西兰第1）
  - 《*[凯尔特瑰宝](../Page/凯尔特瑰宝.md "wikilink")*》（2007年3月13日）（*Celtic
    Treasure*美国、澳大利亚、新西兰又版）
  - 《[圣诞奇迹](../Page/圣诞奇迹.md "wikilink")》(《Christmas magic》or《winter
    magic》)（2009）
  - 《[天堂之音](../Page/天堂之音.md "wikilink")》(《Paradiso》)（2011）

**演唱会**

  - 《[海莉·薇思特拉新西兰现场演唱会](../Page/海莉·薇思特拉新西兰现场演唱会.md "wikilink")》（*Live
    From New Zealand*）（2005）（DVD 包含作为特色内容的传记）
  - 《美丽人声：新历程》（*Celtic Woman: A New Journey*）（2006）（CD和DVD）
  - 《演唱家海莉與北市交合作的音樂會》(10/3 台北國家音樂廳)(2009)
  - 《純淨美聲‧夢幻天籟 海莉演唱會》( 10/27 高雄巨蛋、11/01 台北國家音樂廳 )(2010)
  - 《新天堂樂園 海莉演唱會》( 12/14 台南文化中心演藝廳、12/16 台中圓滿戶外劇場、12/17 台北國家音樂廳 )(2011)
  - 《櫻花之戀 海莉演唱會》( 暫定4月 台南、台北 )(2013)

**参与演出**

  - 《[罗素·华生现场演唱会](../Page/罗素·华生.md "wikilink")》（*Russel Watson
    Live*）（2003）
  - 2005年在丹麦哥本哈根的 HC. Andersen 欢庆盛会上与挪威乐队 a-ha 主唱 Morten Harket
    共唱联合国儿童基金会官方歌曲“儿童第一”（Children First）。
  - 《[花木兰](../Page/花木兰.md "wikilink")2原声碟》（*Mulan II Soundtrack*）（2005）
  - 《[威尼斯商人原声碟](../Page/威尼斯商人.md "wikilink")》（*The Merchant of Venice
    Soundtrack*）（2005）
  - 《[星际宝贝](../Page/星际宝贝.md "wikilink")2》（*"Lilo & Stitch 2"*）（2005）
  - 美声男伶2006世界巡演（Il Divo World Tour 2006）
  - 《新大陆原声碟》（*The New World Soundtrack*）（2006）
  - 《[西区故事](../Page/西区故事.md "wikilink")》五十周年纪念版（''West Side Story - 50th
    Anniversary recording ''）（2007年7月）
  - 海莉的新西兰派对（*Hayley's Kiwi Ceili*）（2007年8月）
  - 《[第八屆世界運動會](../Page/第八屆世界運動會.md "wikilink")》（*The World Games
    2009*）（2009年7月）

## 外部链接

  - [海莉·薇思特拉官方网站](http://www.hayleywestenra.com)
  - [海莉·薇思特拉中国歌迷网站](http://www.hwcfc.com/)
  - [古典跨界音乐歌唱家](http://www.classical-crossover.co.uk/artprofiles/125-hayley-westenra.html)
  - [Hayley Westenra Worldwide](http://belindahutchison.tripod.com)
  - [Hayley Westenra
    International](http://www.hayley-westenra-international.com)
  - [Hayley at Universal Music
    Japan](http://www.universal-music.co.jp/classics/hayley_westenra/index.html)
  - [Hayley Westenra 追っ掛け隊 in
    NZ\!\!](http://homepage2.nifty.com/~oha/profile/hayley)
  - [Hayley Westenra International
    Forum](http://hwi.proboards20.com/index.cgi)
  - [Hayley Westenra German Official
    Site](https://web.archive.org/web/20081206015922/http://www.hayleywestenra.de/)
  - [Hayley Westenra - One of the best
    voices](http://www.freewebs.com/hwestenra)
  - [UNICEF Official Web
    Site](http://www.unicef.org/girlseducation/denmark_25858.html)

## 有关图书

  - 《海莉·薇思特拉：世界尽在伊足下》（Hayley Westenra: The World At Her Feet.）作者：Little
    Paul NZ: Penguin Books, 2005. ISBN 0-14-301926-0

## 参考文献

[Category:紐西蘭女高音](../Category/紐西蘭女高音.md "wikilink")
[Category:街头艺人](../Category/街头艺人.md "wikilink")
[Category:荷蘭裔新西蘭人](../Category/荷蘭裔新西蘭人.md "wikilink")
[Category:愛爾蘭裔新西蘭人](../Category/愛爾蘭裔新西蘭人.md "wikilink")
[Category:基督城人](../Category/基督城人.md "wikilink")

1.  Hayley Westenra: Live from New Zealand. \[DVD\]. Universal Music.
    2005 海莉薇思特拉：現場新西蘭 \[影碟\] 環球音樂

2.  <http://www.hayley-westenra-international.com/content/hwi-archives-1996-2009>

3.
4.
5.
6.  <http://www.hayleywestenra.com/> News Archive

7.

8.

9.
10.

11.

12.

13.
14.
15.

16.

17.

18. <http://www.chinadaily.com.cn/hqgj/jryw/2011-09-28/content_3924021.html>

19.

20.

21. <http://www.chinadaily.com.cn/hqpl/yssp/2011-05-16/content_2628889.html>

22. <http://www.rugbyworldcup.com/destinationnewzealand/news/newsid=2044689.html#nz+voices+bring+rwc+2011+anthems+li>


23.

24.

25.

26.

27.

28.

29.

30.

31.

32.

33.

34.

35.

36.

37.

38.

39.

40.

41.

42.

43.

44.

45.

46.

47.

48.

49.

50.

51.

52.

53.

54.

55.
56.

57.

58.

59.

60.

61.
62.

63.

64.
65.

66.

67.

68.
69.

70.
71. <http://www.itv.com/presscentre/christmascarolswiththetroops/wk52christmascarolswiththetroops/default.html>

72.

73.

74.

75. <http://tvnz.co.nz/national-news/hayley-westenra-sings-chch-memorial-service-3-23-video-4072086>

76.

77.
78.

79.