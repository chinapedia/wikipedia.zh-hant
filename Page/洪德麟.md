**洪德麟**是[台灣的](../Page/台灣.md "wikilink")[漫畫家及台灣漫畫史家](../Page/漫畫家.md "wikilink")，出生於[台中縣](../Page/台中縣.md "wikilink")[烏日鄉](../Page/烏日區.md "wikilink")[頭前厝](../Page/頭前厝.md "wikilink")（今[台中市烏日區前竹](../Page/台中市.md "wikilink")-{里}-一帶），[早稻田大學大眾傳播研究所肄業](../Page/早稻田大學.md "wikilink")，現任[淡江大學通識與核心課程組兼任教授](../Page/淡江大學.md "wikilink")\[1\]。曾任《漫畫秀》總編輯、《漫畫街》總編輯、《漫畫族》總編輯、《[自由時報](../Page/自由時報.md "wikilink")》漫畫主筆、《[兒童日報](../Page/兒童日報_\(台灣\).md "wikilink")》漫畫版企劃主編、《[自立晚報](../Page/自立晚報.md "wikilink")》漫畫單元〈名人肖像畫〉畫者、《[中國時報](../Page/中國時報.md "wikilink")》趣味休閒版專欄〈漫畫暢談〉主筆、[漫畫天堂網路漫畫博物館主筆](../Page/漫畫天堂網路.md "wikilink")、[國立臺南藝術學院](../Page/國立臺南藝術學院.md "wikilink")（今[國立臺南藝術大學](../Page/國立臺南藝術大學.md "wikilink")）音像動畫研究所兼任副教授、[台灣漫畫博物館籌備處負責人](../Page/台灣漫畫博物館.md "wikilink")。

## 人物

除了漫畫著作外，他對台灣從[日治時代到現代的漫畫發展史有深入的研究](../Page/台灣日治時期.md "wikilink")，並出版數本書籍來記錄台灣漫畫界的歷史。要研究台灣早期的漫畫，其相關著作可以提供不少珍貴的資料。除此之外，他還為台灣漫畫界推廣做了許多貢獻。

  - 1970年[逢甲大學](../Page/逢甲大學.md "wikilink")[國際貿易學系畢業](../Page/國際貿易.md "wikilink")。
  - 1976年[日本映畫研究所畢業](../Page/日本映畫研究所.md "wikilink")。
  - 1980年任《[烏龍院](../Page/烏龍院.md "wikilink")》編劇，與[敖幼祥合作](../Page/敖幼祥.md "wikilink")。
  - 1991年11月獨資創立台灣首座漫畫圖書館。

## 作品一覽

  - 《[爸爸的童年](../Page/爸爸的童年.md "wikilink")》，[臺灣省政府教育廳出版](../Page/臺灣省政府.md "wikilink")，年份不詳
  - 《[台灣漫畫四十年初探](../Page/台灣漫畫四十年初探.md "wikilink")》，[時報文化出版](../Page/時報文化.md "wikilink")，1994年1月，ISBN
    957-13-0871-4
  - 《[傑出漫畫家](../Page/傑出漫畫家.md "wikilink")-亞洲篇》，[雄獅美術出版](../Page/雄獅美術.md "wikilink")，2000年，ISBN
    9574740056
  - 《[漫畫名家畫展](../Page/漫畫名家畫展.md "wikilink")》，駿馬出版社，1989年
  - 《[風城台灣漫畫五十年](../Page/風城台灣漫畫五十年.md "wikilink")》，[新竹市政府](../Page/新竹市政府.md "wikilink")、[新竹市立文化中心出版](../Page/新竹市立文化中心.md "wikilink")，1999年，ISBN
    9570247452（台灣漫畫史暨世界漫畫現象展）
  - 《[台灣漫畫史特刊](../Page/台灣漫畫史特刊.md "wikilink")》，[國立歷史博物館出版](../Page/國立歷史博物館.md "wikilink")，2000年（台灣漫畫史特展）
  - 《[台灣漫畫閱覽](../Page/台灣漫畫閱覽.md "wikilink")》，[玉山社出版](../Page/玉山社.md "wikilink")，2003年3月，ISBN
    9867819195
  - 《[台灣漫畫歷史漫步](../Page/台灣漫畫歷史漫步.md "wikilink")》，[甲府文化出版](../Page/甲府文化.md "wikilink")，2006年
  - 《[日本漫畫大驚奇](../Page/日本漫畫大驚奇.md "wikilink")》（上、下冊），甲府文化，2006年

## 參考文獻

<div class="references-small">

1.  《傑出漫畫家-亞洲篇》，雄獅美術出版，1995年，ISBN 9574740056

<references />

</div>

[H](../Category/臺灣漫畫家.md "wikilink")
[Category:臺灣漫畫工作者](../Category/臺灣漫畫工作者.md "wikilink")
[D](../Category/洪姓.md "wikilink") [H](../Category/台中市人.md "wikilink")
[H](../Category/逢甲大學校友.md "wikilink")
[H](../Category/早稻田大學校友.md "wikilink")
[H](../Category/淡江大學教授.md "wikilink")

1.  [臺灣藝術教育網－洪德麟](http://ed.arte.gov.tw/eng/Talent/content_3.aspx?AE_SNID=342)