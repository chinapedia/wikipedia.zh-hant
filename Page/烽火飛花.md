《**烽火飛花**》（[英語](../Page/英語.md "wikilink")：），[香港](../Page/香港.md "wikilink")[無綫電視](../Page/無綫電視.md "wikilink")[翡翠台民初抗戰](../Page/翡翠台.md "wikilink")[電視劇](../Page/電視劇.md "wikilink")，改編自[徐速小說](../Page/徐速.md "wikilink")《櫻子姑娘》，於1981年5月11日首播，共20集，監製為[馮粹帆](../Page/馮粹帆.md "wikilink")，[呂良偉](../Page/呂良偉.md "wikilink")、[趙雅芝](../Page/趙雅芝.md "wikilink")、[鄭少秋主演](../Page/鄭少秋.md "wikilink")。背景為[八年抗戰](../Page/八年抗戰.md "wikilink")[中國剛淪陷於](../Page/中國.md "wikilink")[日軍時](../Page/日軍.md "wikilink")，故事描述一家師範學校裡發生的愛情故事，劇中交織著青年人在大時代下，國家使命、個人愛情的恩怨情仇。

## 角色

  - [鄭少秋](../Page/鄭少秋.md "wikilink") 飾 汪東原

<!-- end list -->

  -
    來自東北的中國人，原名李國光，國民黨陸軍上校，後加入日軍做翻譯官，日軍佔領洛平後，到學校任教日文。眾人都以為他是賣國賊，其實他是[重慶派來](../Page/重慶.md "wikilink")，潛伏於日軍中刺探敵情的[軍統](../Page/軍統.md "wikilink")[間諜](../Page/間諜.md "wikilink")，代字「長江九號」。洛平光復後，晉升陸軍少將，調職洛平市[綏靖公署主任](../Page/綏靖公署.md "wikilink")，但最後被誤會他的吳漢聲錯殺而死。

<!-- end list -->

  - [趙雅芝](../Page/趙雅芝.md "wikilink") 飾 犬養櫻子

<!-- end list -->

  -
    [日本憲兵隊長犬養光雄的女兒](../Page/日本軍.md "wikilink")，到學校讀書，為人純真善良。母親是日本人惠子（趙雅芝分飾），其母最初戀上李光中，後李返回中國，才嫁給其父。初時傾慕汪東原，後來誤會汪的為人，轉而愛上吳漢聲。

<!-- end list -->

  - [呂良偉](../Page/呂良偉.md "wikilink") 飾 吳漢聲

<!-- end list -->

  -
    學生領袖，激進份子。學校淪陷以後復課被迫學習日文而結識犬養櫻子，最初對她非常厭惡，後來真正認識她的為人，二人開始相愛。吳跟隨老師楊尚恕投入地下抗日運動，在愛情、家國間難以抉擇，最後離開家鄉從軍。

<!-- end list -->

  - [馮粹帆](../Page/馮粹帆.md "wikilink") 飾 楊尚恕

<!-- end list -->

  -
    原名李光中，游擊隊長，共產黨員，早年留學日本，抗日時期，為掩飾身份，在學校中做教員，帶領部分學生地下抗日。被揭發後自殺前，才發現汪東原的真正身份。臨死前給學生的遺囑，表面上叫他們努力學習，不要輕言犧牲，和平後中國的建設非常需要他們，但其實內含汪東原為「長江九號」搜集之情報，後來鈴木破解內容後為汪東原所殺。

<!-- end list -->

  - [曾慶瑜](../Page/曾慶瑜.md "wikilink") 飾 穆莉
  - [楊盼盼](../Page/楊盼盼.md "wikilink") 飾 孫勝男
  - [李成昌](../Page/李成昌.md "wikilink") 飾 楊山
  - [龍天生](../Page/龍天生.md "wikilink") 飾 朱慕道
  - [甘國衛](../Page/甘國衛.md "wikilink") 飾 丁玉如
  - [程思俊](../Page/程思俊.md "wikilink") 飾 田牧青
  - [李建川](../Page/李建川.md "wikilink") 飾 侯朝二
  - [霍潔貞](../Page/霍潔貞.md "wikilink") 飾 王英
  - [藍　天](../Page/藍天.md "wikilink") 飾 犬養光雄
  - [關海山](../Page/關海山.md "wikilink") 飾 鈴木浩二
  - [譚一清](../Page/譚一清.md "wikilink") 飾 丁新齋
  - [周　吉](../Page/周吉.md "wikilink") 飾 丁六
  - [曾楚霖](../Page/曾楚霖.md "wikilink") 飾 丁九
  - [薛彩霞](../Page/薛彩霞.md "wikilink") 飾 護士
  - [談泉慶](../Page/談泉慶.md "wikilink") 飾 剛占山
  - [陳榮輝](../Page/陳榮輝.md "wikilink") 飾 馬副官
  - [陳有后](../Page/陳有后.md "wikilink") 飾 校長
  - [曹　濟](../Page/曹濟.md "wikilink") 飾 馬興財
  - [駱　恭](../Page/駱恭.md "wikilink") 飾 游擊隊首領
  - [劉國誠](../Page/劉國誠.md "wikilink") 飾 胡三（游擊隊員）
  - [黎少芳](../Page/黎少芳.md "wikilink") 飾 朱五嫂（游擊隊員）
  - [李壽棋](../Page/李壽棋.md "wikilink") 飾 穆父

此信是楊尚恕臨死前寫給學生的，表面說是自己遠赴日本求學卻不能報效國家，但其實隱藏證據藏於汪東原處，詩最後寫（詩贈二之友），就是要學生想明白將詩每行第二個字抽出來，然後就會出現「信證存於汪東原處」的秘密。

## 主題曲

本劇主題曲是由[顧嘉輝作曲](../Page/顧嘉輝.md "wikilink")、編曲，[黃霑填詞](../Page/黃霑.md "wikilink")，[鄭少秋主唱](../Page/鄭少秋.md "wikilink")，頗有動盪時代下必須於家國愛情兩者間抉擇的氣魄。

## 外部連結

  - [烽火飛花TVB網頁](http://www.tvb.com/tvb_classic/drama/inloveandwar)

[Category:1981年無綫電視劇集](../Category/1981年無綫電視劇集.md "wikilink")
[Category:無綫電視抗日戰爭背景劇集](../Category/無綫電視抗日戰爭背景劇集.md "wikilink")
[Category:抗日戰爭題材電視劇](../Category/抗日戰爭題材電視劇.md "wikilink")