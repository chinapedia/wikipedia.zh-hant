**透骨草科** (Schauer
1847)以前只分成一[属](../Page/属.md "wikilink")—[透骨草属](../Page/透骨草属.md "wikilink")（*Phryma*）一种[透骨草](../Page/透骨草.md "wikilink")（*Phryma
leptostachya*
Linn.），但最新的[基因分析认为许多原来在](../Page/基因.md "wikilink")[玄参科的属应该列入本](../Page/玄参科.md "wikilink")[科](../Page/科.md "wikilink")，重新分成11属约190[种](../Page/种.md "wikilink")，。主要分布在[北美洲](../Page/北美洲.md "wikilink")，约有160种，其次在[澳洲有约](../Page/澳洲.md "wikilink")30种，另外30种分布在世界各地。

本科[植物为一年生或多年生](../Page/植物.md "wikilink")[草本](../Page/草本.md "wikilink")，也有高达4[米的](../Page/米.md "wikilink")[灌木](../Page/灌木.md "wikilink")，[花萼](../Page/花萼.md "wikilink")5，管状有齿；心皮内有两个薄板，[果实为](../Page/果实.md "wikilink")[蒴果开裂](../Page/蒴果.md "wikilink")。

1981年的[克朗奎斯特分类法将其列入](../Page/克朗奎斯特分类法.md "wikilink")[马鞭草科内](../Page/马鞭草科.md "wikilink")，1998年根据[基因亲缘关系分类的](../Page/基因.md "wikilink")[APG
分类法将新扩大的本科列入](../Page/APG_分类法.md "wikilink")[唇形目](../Page/唇形目.md "wikilink")。

## 属

  - 通泉草亚科
      - [通泉草属](../Page/通泉草属.md "wikilink") *Mazus* Lour.
      - [肉果草属](../Page/肉果草属.md "wikilink") *Lancea*
        [Hook.f.](../Page/Hook.f..md "wikilink") &
        [Thomson](../Page/Thomas_Thomson_\(1817-1878\).md "wikilink")
  - 透骨草亚科（约160种）
      - [沟酸浆属](../Page/沟酸浆属.md "wikilink") *Mimulus* L.
      - [野胡麻属](../Page/野胡麻属.md "wikilink") *Dodartia* L.
      - [旋柱草属](../Page/旋柱草属.md "wikilink") *Glossostigma* Wight & Arn.
      - [微癣草属](../Page/微癣草属.md "wikilink") *Peplidium* Delile
      - [透骨草属](../Page/透骨草属.md "wikilink") *Phryma* L.
      - [亮腕草属](../Page/亮腕草属.md "wikilink") *Leucocarpus* D.Don
      - [波染荑属](../Page/波染荑属.md "wikilink") *Berendtiella* Wettst. &
        Harms
      - [猴迷菜属](../Page/猴迷菜属.md "wikilink") *Hemichaena* Benth. 1841
      - [拉蛛萝属](../Page/拉蛛萝属.md "wikilink") *Elacholoma* F.Muell. & Tate

## 参考文献

  - Beardsley, P. M. & Olmstead, R. G. 2002. Redefining Phrymaceae: the
    placement of *Mimulus*, tribe Mimuleae, and *Phryma*. *American
    Journal of Botany* 89: 1093-1102 (available online
    [here](http://www.amjbot.org/cgi/content/full/89/7/1093)).
  - Oxelman, B.; Kornhall, P.; Olmstead, R.G.; Bremer, B. 2005. Further
    disintegration of the Scrophulariaceae. *Taxon* 54(2): 411-425.

[\*](../Category/透骨草科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")