**PCMan檔案管理程式**(PCMan File
Manager)是一個以輕巧快速為設計目的的[檔案管理器](../Page/檔案管理器.md "wikilink")，为[LXDE](../Page/LXDE.md "wikilink")[桌面环境的默认](../Page/桌面环境.md "wikilink")[档案管理器](../Page/档案管理器.md "wikilink")。

## 特色

  - 快速輕巧
  - 在2006年大多數電腦下只要1秒時間來開啟
  - 分頁瀏覽
  - 内置卷管理功能（通过[HAL挂载](../Page/HAL.md "wikilink")、卸载、弹出卷）
  - 内置文件搜索工具（[UNIX的find](../Page/UNIX.md "wikilink")+grep[命令的](../Page/命令_\(计算机\).md "wikilink")[GUI](../Page/GUI.md "wikilink")[前端](../Page/前端.md "wikilink")）
  - 支持[拖放方式操作](../Page/拖放.md "wikilink")（Drag & Drop）
  - 可以在不同的分頁間拖放檔案
  - 可以在合理時間內開啟存有大多檔案的資料夾
  - 開以設定檔案關聯（預設開啟程式）
  - 支持簡單的預覽圖
  - 支持[書籤功能](../Page/書籤.md "wikilink")
  - 能夠正常處理以非[萬國碼](../Page/萬國碼.md "wikilink")（non-[UTF-8](../Page/UTF-8.md "wikilink")）為[編碼的檔案名稱](../Page/編碼.md "wikilink")
  - 提供圖示檢索與清單檢索
  - 迎合[Freedesktop.org的標準](../Page/Freedesktop.org.md "wikilink")
  - [GTK+](../Page/GTK+.md "wikilink")[介面](../Page/介面.md "wikilink")

[Category:SourceForge专案](../Category/SourceForge专案.md "wikilink")
[Category:LXDE](../Category/LXDE.md "wikilink")
[Category:使用Qt的軟體](../Category/使用Qt的軟體.md "wikilink")