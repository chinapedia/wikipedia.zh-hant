**泰晤士河**（，，）是位於[南英格兰的一条河流](../Page/南英格兰.md "wikilink")，全長約346[公里](../Page/公里.md "wikilink")，流經英格蘭的三個[郡](../Page/郡_\(英格蘭\).md "wikilink")，為英格蘭最長之河流、英國第二长河，次于354公里的[塞文河](../Page/塞文河.md "wikilink")，也是全世界水面交通最繁忙的都市河流和[倫敦地標之一](../Page/倫敦.md "wikilink")。在泰晤士流域形成了許多英格蘭城市，除去倫敦之外，還有[牛津](../Page/牛津.md "wikilink")、[雷丁和](../Page/雷丁.md "wikilink")[溫莎等](../Page/溫莎_\(伯克郡\).md "wikilink")。

泰晤士河在英國有非常重要的[經濟地位](../Page/英國經濟.md "wikilink")，在[工業革命之後因為大規模的河上運輸導致河流污染](../Page/工業革命.md "wikilink")，因而引發了包括瘟疫、[大惡臭等一系列的問題](../Page/大惡臭.md "wikilink")，隨著重工業的減少其情況已經改善了許多。

而其於[英國文化意義也不可忽視](../Page/英國文化.md "wikilink")。泰晤士河是許多英國水上運動，如[牛津劍橋賽艇對抗賽](../Page/牛津劍橋賽艇對抗賽.md "wikilink")、[1908年夏季奧運會划艇賽](../Page/1908年夏季奧運會.md "wikilink")、[1948年夏季奧運會划艇賽的舉辦地](../Page/1948年夏季奧運會.md "wikilink")。《[柳林風聲](../Page/柳林風聲.md "wikilink")》等文學作品也是以泰晤士河流域的風物為背景寫成的。

## 語源學

“Thames”一詞來自[中古英語](../Page/中古英語.md "wikilink")“Temese”，後者來自[凱爾特語](../Page/凱爾特語.md "wikilink")“Tamesas”（起源自\**tamēssa*），\[1\]
[拉丁語記為](../Page/拉丁語.md "wikilink")“Tamesis”，即現代該河[威爾士語](../Page/威爾士語.md "wikilink")“Tafwys”的來源。
此名的意思可能是“黑暗”，與[俄語](../Page/俄語.md "wikilink")“темно”、[梵語](../Page/梵語.md "wikilink")“tamas”及[愛爾蘭語](../Page/愛爾蘭語.md "wikilink")“teimheal”、威爾士語“tywyll”同源，此外與[中古愛爾蘭語的](../Page/中古愛爾蘭語.md "wikilink")“teimen”（深灰）也有關係。\[2\]
但也有一些學者認為它並非來自[印歐語系](../Page/印歐語系.md "wikilink")，\[3\]\[4\]
也有人認為它的确来自印歐語系，只不過出自[凱爾特不列顛時代之前](../Page/凱爾特不列顛.md "wikilink")。\[5\]

英國的海員常把它叫做“倫敦河”（London River）。\[6\]

## 地理

[ThamesMarker.JPG](https://zh.wikipedia.org/wiki/File:ThamesMarker.JPG "fig:ThamesMarker.JPG")
據研究在50萬年前，泰晤士河已經基本上開始沿現有河道路線流動了。而在2萬年前，英國乃與歐洲大陸版塊連接，而泰晤士河的源頭亦據稱是位處[威爾斯](../Page/威爾斯.md "wikilink")，一直流到與[萊茵河等其他歐洲大陸河流匯合](../Page/萊茵河.md "wikilink")。至後期版塊發生移動，源頭有所改變，而泰晤士河盡頭也變成北海\[7\]。

### 地理史

大約5800萬年前[古新世末期的](../Page/古新世.md "wikilink")[贊尼特階](../Page/贊尼特階.md "wikilink")，泰晤士河形成了單獨的河道。\[8\]
直到大約50萬年前，泰晤士河已經大致沿現有河道路線流動，即途經[牛津](../Page/牛津.md "wikilink")、[倫敦等地而到達](../Page/倫敦.md "wikilink")[伊普斯維奇市流入](../Page/伊普斯維奇.md "wikilink")[北海](../Page/北海_\(大西洋\).md "wikilink")。在[冰河時期末端](../Page/冰河時期.md "wikilink")，源頭的冰層開始溶解，大量冰水湧入泰晤士河，使河道進一步擴展，而成為今日的形態。

大約45萬年前[更新世](../Page/更新世.md "wikilink")[盎格魯階的](../Page/盎格魯階.md "wikilink")[冰河時期](../Page/冰河時期.md "wikilink")，冰川最遠覆蓋到了倫敦東部的[霍恩彻奇](../Page/霍恩彻奇.md "wikilink")。\[9\]
它阻塞了流經相當於現在[赫特福德郡的河流](../Page/赫特福德郡.md "wikilink")，從而形成了一個大湖。後來河水衝破阻攔，改道流經現在的倫敦。\[10\]

上一次冰河時期冰川的最後一次擴張使得其覆蓋了現在西北[米德爾塞克斯的大部分土地](../Page/米德爾塞克斯.md "wikilink")，最終迫使史前泰晤士河幾乎完全改至現在的河道。大約2萬年前的末次冰期時，不列顛經由北海盆地排乾形成的[多格蘭與歐洲大陸相連](../Page/多格蘭.md "wikilink")，泰晤士河流到現在的荷蘭和比利時一帶，與[萊茵河](../Page/萊茵河.md "wikilink")、[默兹河和](../Page/默兹河.md "wikilink")[斯凱爾特河匯合](../Page/斯凱爾特河.md "wikilink")，\[11\]
它們最終合流形成[海峽河](../Page/海峽河.md "wikilink")（Channel
River），後者最終注入[英吉利海峽西部的大西洋](../Page/英吉利海峽.md "wikilink")。

#### 現在

在泰晤士河改道到現在的位置後，[泰晤士河口](../Page/泰晤士河口.md "wikilink")、[泰晤士河谷和周邊的](../Page/泰晤士河谷.md "wikilink")[利河谷下游形成了](../Page/利河谷下游.md "wikilink")[沼澤](../Page/沼澤.md "wikilink")。例如現在[蘭貝斯教區北部在古時就稱](../Page/蘭貝斯.md "wikilink")“蘭貝斯沼澤”（Lambeth
Marshe），但在18世紀時被排乾，而現在當地還有名為“Lower
Marsh”的街道。此外，此時[利河](../Page/利河_\(英格蘭\).md "wikilink")、[泰伯恩河匯入泰晤士河](../Page/泰伯恩河.md "wikilink")，也形成了[索尼島等島嶼](../Page/索尼島_\(倫敦\).md "wikilink")。\[12\]

## 生物

[River_Thames,_The_swans_at_Walton-on-Thames_-_geograph.org.uk_-_367728.jpg](https://zh.wikipedia.org/wiki/File:River_Thames,_The_swans_at_Walton-on-Thames_-_geograph.org.uk_-_367728.jpg "fig:River_Thames,_The_swans_at_Walton-on-Thames_-_geograph.org.uk_-_367728.jpg")河段的一群天鵝\]\]
雖然泰晤士河的生物因人類活動而受到威脅，一些物種甚至在當地瀕危，但泰晤士河流域仍然有許多野生動物生活，常見的包括[綠頭鴨](../Page/綠頭鴨.md "wikilink")、[普通鸕鶿](../Page/普通鸕鶿.md "wikilink")、[棕頭鷗和](../Page/棕頭鷗.md "wikilink")[銀鷗](../Page/銀鷗.md "wikilink")。[疣鼻天鵝也較為常見](../Page/疣鼻天鵝.md "wikilink")，偶然的情況下還可以看到[黑天鵝](../Page/黑天鵝.md "wikilink")。引入的非本土生物有[加拿大雁](../Page/加拿大雁.md "wikilink")、[埃及雁和](../Page/埃及雁.md "wikilink")[斑頭雁等](../Page/斑頭雁.md "wikilink")。

鮭魚和鰻魚都是泰晤士河中常見的魚類，後者還被做成了倫敦地區的一道名菜[鱔魚凍](../Page/鱔魚凍.md "wikilink")。此外常見的魚種還包括[歐鰱](../Page/歐鰱.md "wikilink")、[湖擬鯉](../Page/湖擬鯉.md "wikilink")、[白斑狗魚和各種比目魚](../Page/白斑狗魚.md "wikilink")。

2008年發現[歐洲海馬曾出現在泰晤士河中](../Page/歐洲海馬.md "wikilink")，\[13\]
一些水生哺乳類，如[灰海豹和](../Page/灰海豹.md "wikilink")[斑海豹在泰晤士河口聚居](../Page/斑海豹.md "wikilink")，在河上游的一些地區偶然也能見到他們。\[14\]
也曾有人在河中目擊到[寬吻海豚和](../Page/寬吻海豚.md "wikilink")[鼠海豚](../Page/鼠海豚.md "wikilink")。\[15\]

2006年1月20日，一條長達的[拜氏鲸出現在泰晤士河倫敦市中心地區](../Page/拜氏鲸.md "wikilink")，而這種鯨一般指生活在深海，上一次在泰晤士河中看到這種鯨已經是1913年的事情了。這種極不正常的當時得到了大量的關注，人們在圍觀這頭被稱為“泰晤士河鯨”（River
Thames
whale）的鯨時發現它幾乎擱淺，又因為撞到了小船而流血。12小時之後它再次出現在[格林尼治附近水域](../Page/格林尼治.md "wikilink")。[倫敦港務局和](../Page/倫敦港務局.md "wikilink")[倫敦警察廳將它捕捉上來用試圖運到海中釋放](../Page/倫敦警察廳.md "wikilink")，但最終這頭鯨因為體力過差而在運送途中死亡。\[16\]

## 人類史

[London_Bridge_(1616)_by_Claes_Van_Visscher.jpg](https://zh.wikipedia.org/wiki/File:London_Bridge_\(1616\)_by_Claes_Van_Visscher.jpg "fig:London_Bridge_(1616)_by_Claes_Van_Visscher.jpg")的版畫，其上是[倫敦橋和](../Page/倫敦橋.md "wikilink")[薩瑟克座堂](../Page/薩瑟克座堂.md "wikilink")\]\]

### 早期

有考古證據顯示在[新石器時代就已經有人類在泰晤士河流域生活了](../Page/新石器時代.md "wikilink")。\[17\]
[大英博物館有一隻公元前](../Page/大英博物館.md "wikilink")3300年至前2700年的雕花碗，發掘地點就是流經[白金漢郡小村Hedsor的泰晤士河](../Page/白金漢郡.md "wikilink")。人工湖[多尼湖的挖掘過程中也發現了大量的人類用具](../Page/多尼湖.md "wikilink")。\[18\]
泰晤士河沿岸的[莱赫雷德](../Page/莱赫雷德.md "wikilink")、[庫克漢姆及](../Page/庫克漢姆.md "wikilink")[泰晤士河河畔森伯里也有許多](../Page/泰晤士河河畔森伯里.md "wikilink")[青銅時代遺址](../Page/青銅時代.md "wikilink")。\[19\]

許多記載[凱撒大帝在前](../Page/凱撒大帝.md "wikilink")54年第二次進軍不列顛的文獻中也提到了泰晤士河之名。\[20\]
實際上，泰晤士河河畔有許多古羅馬和[薩克森人的聚居地](../Page/薩克森人.md "wikilink")，例如泰晤士河流域地名中帶有“ing”的[泰晤士河畔戈林](../Page/泰晤士河畔戈林.md "wikilink")（Goring-on-Thames）和[雷丁](../Page/雷丁.md "wikilink")（Reading）都有一定的薩克森起源。\[21\]

### 中世紀到近代

早在1300年代，泰晤士河就成為了倫敦的排污河。在1357年時，[愛德華三世說道](../Page/愛德華三世.md "wikilink")：“糞與其他污物积聚在河岸上...煙與惡臭因此而瀰漫”。\[22\]
除去污穢之外，冬季有時泰晤士河會結冰，因此後來形成了[泰晤士河冰凍博覽會](../Page/泰晤士河冰凍博覽會.md "wikilink")（Frost
fair）這樣的傳統，最早的冰凍博覽會舉行於1607年。

倫敦國際地位的提高使得泰晤士河上的貿易頻繁起來，到了十八世紀，它已經是[大英帝國和世界上最繁忙的河道之一](../Page/大英帝國.md "wikilink")。河上遊開始修建船閘來解決通航不暢的問題。1814年氣候轉暖，之後泰晤士河就再也沒有完全凍結過了。\[23\]
而1825年建造的新的[倫敦橋減少了橋墩數量](../Page/倫敦橋.md "wikilink")，因此河流更加暢通，這也減少了其凍結的可能性。\[24\]

### 維多利亞時代到現代

[Monster_Soup_commonly_called_Thames_Water._Wellcome_V0011218.jpg](https://zh.wikipedia.org/wiki/File:Monster_Soup_commonly_called_Thames_Water._Wellcome_V0011218.jpg "fig:Monster_Soup_commonly_called_Thames_Water._Wellcome_V0011218.jpg")的諷刺卡通畫，描繪的是一名婦女在觀看一滴泰晤士河水中的怪物\]\]
19世紀[倫敦市河段的泰晤士河水質愈加惡化](../Page/倫敦市.md "wikilink")，導致多起[霍亂爆發](../Page/霍亂.md "wikilink")，1832年至1865年爆發的四起嚴重霍亂殺死了數萬人。[阿爾伯特親王之死也是由於在](../Page/阿爾伯特親王.md "wikilink")[溫莎城堡旁邊骯髒泰晤士河傳染了傷寒](../Page/溫莎城堡.md "wikilink")\[25\]。1850年代[抽水馬桶流行開來之後情況就更加不堪了](../Page/抽水馬桶.md "wikilink")。\[26\]
1858年的[大惡臭使得河邊](../Page/大惡臭.md "wikilink")-{zh-hans:威斯敏斯特;
zh-hant:西敏}-宮的[下議院都不得不停止工作](../Page/英國下議院.md "wikilink")。[約瑟夫·巴瑟傑特修建了一系列的](../Page/約瑟夫·巴瑟傑特.md "wikilink")[衛生下水道之後](../Page/衛生下水道.md "wikilink")，加上水庫和抽水站的陸續建立，其污染情況稍稍好轉。

隨著水路交通和大英帝國的衰落，雖然[倫敦港還是英國主要港口之一](../Page/倫敦港.md "wikilink")，泰晤士河的重要性已經不可避免地開始降低了。而後重工業和其他一些污染企業也開始減少，使得泰晤士河水質有了明顯的好轉。

泰晤士河亦有水患，1928年、1947年和1957年發生三次大洪水。1980年代前期，防洪設備[泰晤士河水閘建成啟用](../Page/泰晤士河水閘.md "wikilink")。1990年代末長的[銀禧河](../Page/銀禧河.md "wikilink")（Jubilee
River）也提供了額外的排洪渠道。\[27\]

## 經濟

[Houseboats_-_Richmond,_London,_UK.jpg](https://zh.wikipedia.org/wiki/File:Houseboats_-_Richmond,_London,_UK.jpg "fig:Houseboats_-_Richmond,_London,_UK.jpg")附近泰晤士河上的[船屋](../Page/船屋.md "wikilink")\]\]
泰晤士河是周邊地區飲用水的重要來源之一，這些來自泰晤士河的飲用水由[泰晤士水務公司負責處理](../Page/泰晤士水務.md "wikilink")。[泰晤士水務環路是倫敦的主要供水設施](../Page/泰晤士水務環路.md "wikilink")。過去泰晤士河上漁業（特別是鰻魚捕撈）是除去航運之外較主要的經濟活動之一，此外周邊城市的[水磨也需要泰晤士河提供動力](../Page/水磨.md "wikilink")。\[28\]

倫敦中心有許多大廈是沿著泰晤士河建造的，“河景”是其賣點之一。而河上也常能看到各種有人居住的[船屋](../Page/船屋.md "wikilink")。

### 旅遊

[Waterloo_Pier_1.jpg](https://zh.wikipedia.org/wiki/File:Waterloo_Pier_1.jpg "fig:Waterloo_Pier_1.jpg")
倫敦有許多的河上觀光船，[倫敦河務公司提供倫敦段泰晤士河上的交通服務](../Page/倫敦河務.md "wikilink")。在夏季，[牛津到](../Page/牛津.md "wikilink")[特丁頓之間有](../Page/特丁頓.md "wikilink")“Salters
Steamers”和“French Brothers”兩大公司提供旅客運輸服務。\[29\]\[30\]
實際上泰晤士河上還有各種小型私人公司提供著類似的服務。\[31\]

## 運動

[Finish_of_2007_Oxford-Cambridge_boat_race.JPG](https://zh.wikipedia.org/wiki/File:Finish_of_2007_Oxford-Cambridge_boat_race.JPG "fig:Finish_of_2007_Oxford-Cambridge_boat_race.JPG")中率先衝過終點\]\]

泰晤士河上最著名的運動就是[划船](../Page/划船.md "wikilink")，英國有數目眾多的划船協會、俱樂部，如隸屬於[國際划船聯盟的](../Page/國際划船聯盟.md "wikilink")“British
Rowing”。\[32\]
[牛津大學和](../Page/牛津大學.md "wikilink")[劍橋大學的划船俱樂部會在泰晤士河上舉辦一年一度的](../Page/劍橋大學.md "wikilink")[牛津劍橋賽艇對抗賽](../Page/牛津劍橋賽艇對抗賽.md "wikilink")，除去這項賽事之外，還有[河王之爭](../Page/河王之爭.md "wikilink")（Head
of the River
Race）等一些其他的划船比賽。[1908年和](../Page/1908年夏季奧運會.md "wikilink")[1948年夏季奧運會划艇賽都是在這條河上舉行的](../Page/1948年夏季奧運會.md "wikilink")。

而帆船、快艇、獨木舟等也都有相應的泰晤士河賽事。成立於1866年，號稱世界上最古老的獨木舟俱樂部“皇家獨木舟俱樂部”（Royal Canoe
Club）自1950年開始幾乎每年復活節都會在河上舉辦現在叫做“迪韦齐斯到威斯敏斯特國際獨木舟賽”的比賽，\[33\]
其比賽航道從[肯尼特和埃文運河一直延伸到](../Page/肯尼特和埃文運河.md "wikilink")[西敏橋](../Page/西敏橋.md "wikilink")。此外1948年夏季奧運會的獨木舟賽也是在泰晤士河上舉辦的。

2006年英國游泳運動員[路易斯·皮尤成為世界上第一個遊完泰晤士河全程](../Page/路易斯·皮尤.md "wikilink")325千米的人，他總共花費了21天才達成此項記錄。\[34\]
考慮到河上游泳的危險性和對其他河上事務的影響，2012年6月[倫敦港務局宣佈](../Page/倫敦港務局.md "wikilink")：未經允許任何人不得在[普特尼橋到](../Page/普特尼橋.md "wikilink")[克羅斯密斯之間的河段游泳](../Page/克羅斯密斯.md "wikilink")，這一規定實際上使得整個倫敦中心區的泰晤士河段都變成了禁游區。\[35\]

## 相關條目

  -
## 参考资料

## 外部链接

  - [Where Thames Smooth Waters Glide – by John
    Eade](http://thames.me.uk/)
  - [Floating Down the River: River Thames and Boaty
    Things](http://www.the-river-thames.co.uk/riverthames.htm)
  - [Thames Path.com – Includes news
    features](http://www.thames-path.com/)
  - [BBC interactive map of the River
    Thames](http://www.bbc.co.uk/berkshire/content/articles/2008/12/23/tales_along_the_thames_feature.shtml)
  - [The River Thames Guide](http://riverthames.co.uk/) – Covers all of
    the Thames and many aspects including Accommodation, Thames
    Information, etc.
  - [The River Thames Society](http://www.riverthamessociety.org.uk)
  - [Thames Path Online Guide](http://www.thames-path.com/)
  - [River Thames Conditions](http://visitthames.co.uk/)
  - [Thames Path National Trail](http://thames-path.org.uk/)

[Category:倫敦地理](../Category/倫敦地理.md "wikilink")
[Category:泰晤士河](../Category/泰晤士河.md "wikilink")
[Category:倫敦旅遊景點](../Category/倫敦旅遊景點.md "wikilink")
[Category:英國地標](../Category/英國地標.md "wikilink")

1.  Mallory, J.P. and D.Q. Adams. *The Encyclopedia of Indo-European
    Culture*. London: Fitzroy and Dearborn, 1997: 147.

2.
3.

4.   in

5.

6.  Cultural Heritage Resources (2005). *[Legendary Origins and the
    Origin of London's place name](http://chr.org.uk/legends.htm)*.
    Retrieved 1 November 2005.

7.

8.

9.  [Essex Wildlife Trust, The Geology of
    Essex](http://www.essexwt.org.uk/geology/geology3.htm)

10.
11.

12.

13. [Rare seahorses breeding in
    Thames](http://news.bbc.co.uk/1/hi/england/london/7333980.stm) BBC
    News, 7 April 2008

14.

15.

16.

17.

18.

19. [The Physique of
    Middlesex](http://www.british-history.ac.uk/report.asp?compid=22094),
    A History of the County of Middlesex: Volume 1: Physique,
    Archaeology, Domesday, Ecclesiastical Organization, The Jews,
    Religious Houses, Education of Working Classes to 1870, Private
    Education from Sixteenth Century (1969), pp. 1–10. Date Retrieved 11
    August 2007.

20. Gaius Julius Caesar *De Bello Gallico*, Book 5, §§ 11, 18

21. Stephen Oppenheimer *The Origins of the British* 2006

22. Peter Ackroyd, Thames: The Biography, New York: Doubleday, 2007.
    "Filthy River"

23.

24.

25.
26. Peter Ackroyd, *Thames: The Biography*. 272 & 274.

27. Environment Agency (2005). *[Jubilee
    River](http://www.environment-agency.gov.uk/subjects/recreation/345623/631029/346131/348128/349190/349293/?lang=_e&theme=&region=&subject=&searchfor=Jubilee+River&any_all=all&choose_order=&exactphrase=&withoutwords=&exclude_itemtype=Station%2C&include_itemtype=Acrobat%20Document%2CAttached%20File_e%2CAttached%20File_w%2CHTML%20Page%2C)
    *.

28.

29.

30.

31.

32. [British Rowing — Clubs](http://www.britishrowing.org/clubs)

33. [Devizes to Westminster International Canoe
    Race](http://www.dwrace.org.uk/index.php). Dwrace.org.uk. Retrieved
    on 17 July 2013.

34.

35.