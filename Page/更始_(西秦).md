**更始**（409年七月－412年八月）是[十六國時期](../Page/十六國.md "wikilink")[西秦政權](../Page/西秦.md "wikilink")，高祖武元王[乞伏-{乾}-歸的](../Page/乞伏乾歸.md "wikilink")[年號](../Page/年號.md "wikilink")，共計3年餘。

## 纪年

| 更始                               | 元年                             | 二年                             | 三年                             | 四年                             |
| -------------------------------- | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| [公元](../Page/公元纪年.md "wikilink") | 409年                           | 410年                           | 411年                           | 412年                           |
| [干支](../Page/干支纪年.md "wikilink") | [己酉](../Page/己酉.md "wikilink") | [庚戌](../Page/庚戌.md "wikilink") | [辛亥](../Page/辛亥.md "wikilink") | [壬子](../Page/壬子.md "wikilink") |

## 参看

  - [中国年号索引](../Page/中国年号索引.md "wikilink")
      - 其他时期使用的[更始年号](../Page/更始.md "wikilink")
  - 同期存在的其他政权年号
      - [义熙](../Page/义熙_\(晋安帝\).md "wikilink")（405年正月－418年十二月）：東晉皇帝[晋安帝司马德宗的年号](../Page/晋安帝.md "wikilink")
      - [弘始](../Page/弘始.md "wikilink")（399年九月－416年正月）：[后秦政权](../Page/后秦.md "wikilink")[姚兴年号](../Page/姚兴.md "wikilink")
      - [太上](../Page/太上.md "wikilink")（405年十一月－410年二月）：[南燕政权](../Page/南燕.md "wikilink")[慕容超年号](../Page/慕容超.md "wikilink")
      - [正始](../Page/正始_\(北燕\).md "wikilink")（407年七月－409年十月）：[北燕政权](../Page/北燕.md "wikilink")[高云年号](../Page/高云.md "wikilink")
      - [太平](../Page/太平_\(北燕\).md "wikilink")（409年十月－430年十二月）：[北燕政权](../Page/北燕.md "wikilink")[冯跋年号](../Page/冯跋.md "wikilink")
      - [龙升](../Page/龙升.md "wikilink")（407年六月－413年二月）：[夏国政权](../Page/夏国.md "wikilink")[赫连勃勃年号](../Page/赫连勃勃.md "wikilink")
      - [建初](../Page/建初_\(西凉\).md "wikilink")（405年正月－417年二月）：[西凉政权](../Page/西凉.md "wikilink")[李暠年号](../Page/李暠.md "wikilink")
      - [嘉平](../Page/嘉平_\(南凉\).md "wikilink")（408年十一月－414年七月）：[南凉政权](../Page/南凉.md "wikilink")[秃发傉檀年号](../Page/秃发傉檀.md "wikilink")
      - [永安](../Page/永安_\(北凉\).md "wikilink")（401年六月－412年十月）：[北凉政权](../Page/北凉.md "wikilink")[沮渠蒙逊年号](../Page/沮渠蒙逊.md "wikilink")
      - [天赐](../Page/天赐_\(北魏\).md "wikilink")（404年十月－409年十月）：[北魏政权](../Page/北魏.md "wikilink")[拓跋珪年号](../Page/拓跋珪.md "wikilink")
      - [永兴](../Page/永兴_\(北魏明元帝\).md "wikilink")（409年闰十月－413年十二月）：[北魏政权](../Page/北魏.md "wikilink")[拓跋嗣年号](../Page/拓跋嗣.md "wikilink")

## 参考文献

1.  李崇智，《中国历代年号考》，中华书局，2001年1月 ISBN 7101025129

[Category:西秦年号](../Category/西秦年号.md "wikilink")
[Category:5世纪](../Category/5世纪.md "wikilink")