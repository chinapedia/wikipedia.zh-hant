[HK_WC_Oi_Kwan_Road_TSK_Victoria_Gov_Sec_School.jpg](https://zh.wikipedia.org/wiki/File:HK_WC_Oi_Kwan_Road_TSK_Victoria_Gov_Sec_School.jpg "fig:HK_WC_Oi_Kwan_Road_TSK_Victoria_Gov_Sec_School.jpg")

**鄧肇堅維多利亞官立中學**（簡稱**維官**；）位於[香港](../Page/香港.md "wikilink")[灣仔](../Page/灣仔.md "wikilink")[愛群道](../Page/愛群道.md "wikilink")5號。

## 歷史

鄧肇堅維多利亞官立中學的前身初級工業學校在1933年於[灣仔加路連山道建校](../Page/灣仔.md "wikilink")，附設於前「維多利亞英童學校」，校長為佐治韋特先生，其時只有教員4人和學生40人，提供四年制之學徒前期訓練，為香港第一所政府創辨的工業學院。

1948年，初級工業學校於戰後復課，當時為香港官立高級工業學院的連繫學校。 1950年，9月本校與摩理臣山小學共用校舍，佔用校舍頂層，開設七班。
1957年該校更名為維多利亞工業學校(紅磚屋)。
直至1979年，因獲鄧肇堅爵士的捐助，得以獲遷往現時灣仔愛群道之校舍，並與[香港理工學院](../Page/香港理工大學.md "wikilink")、[摩利臣山工業學院正式分家](../Page/香港專業教育學院摩理臣山分校.md "wikilink")。
1979年9月7日前香港總督[麥理浩爵士來校主持奠基典禮](../Page/麥理浩.md "wikilink")；同年十一月遷入現今之校舍，成為港島唯一採用1980年代標準中學校舍設計的中學。為紀念[鄧肇堅爵士慷慨捐助](../Page/鄧肇堅.md "wikilink")，本校易名為「鄧肇堅維多利亞工業學校」；並由前布政司姬達爵士主持開幕典禮；是年開始招收女生。
最後該校於1997年9月正式定名為「鄧肇堅維多利亞官立中學」。

1997年起，鄧肇堅維多利亞官立中學被[教育司署要求試辦兩班母語教學班別](../Page/教育司署.md "wikilink")，並自1998年起因應當時[教育統籌局要求轉為母語授課](../Page/教育局_\(香港\).md "wikilink")，同時又在中四及中五級開辦一班以英語授課的理科班別。但隨著時任校長趙淑芬女士與及該校各英文科老師共同努力下，並在2002年率先引入英語戲劇教授英語。

該校在三三四新學制實施時，改為英語授課。為了配合[新高中課程](../Page/三三四高中教育改革.md "wikilink")，時任校長梁文姬女士額外投放資源，以培訓老師，並加設更多的課後學生增潤課程，以協助學生及老師適應新學制。

此外，為了應乎日後工商業社會對資訊人材的需要，該校亦在2009年向政府申請撥款，以興建新的多媒體電腦學習資源中心。有關中心於2010年8月落成。

2005年起，[教統局協辦第一屆官立中學航天修學團](../Page/教統局_\(香港\).md "wikilink")。2006年起，又開始在初中舉辦航天科普課程，以取代原有的設計與科技課程。2008年起該校再開始在中四級的文科（4C2）開辦一班以全英語授課的商科課程。學生於DSE中取得良好成績，近年亦有不少學生考進香港大學、香港中文大學等院校。

### 開辦科目

  - 中文
  - 英文
  - 數學
  - 地理
  - 普通電腦
  - 資訊及通訊科技
  - 音樂
  - 科學
  - 物理
  - 化學
  - 生物
  - 經濟
  - 中國歷史
  - 企業、會計與財務概論
  - 組合科學
  - 科技與生活
  - 普通話
  - 設計與科技
  - 設計與應用科技
  - 視覺藝術
  - 生命教育
  - 通識教育
  - 體育

### 歷任校長

| 歷任校長                 | 年份        |
| -------------------- | --------- |
| Mr. Grorge White     | 1933-1941 |
| 1941-1948 太平洋戰爭停課    |           |
| Mr. J Feguson        | 1948-1950 |
| 劉國禎校長                | 1950-1951 |
| Mr. L.C.H. Griffiths | 1951-1957 |
| 劉國禎校長                | 1957-1962 |
| 譚國榮校長                | 1962-1970 |
| Mr. A.G. Martin      | 1970-1977 |
| 呂保羅校長                | 1977-1987 |
| 韓沛霖校長                | 1987-1988 |
| 馬智修校長                | 1988-1992 |
| 馬紹良校長                | 1992-1994 |
| 葉德滋校長                | 1994-1997 |
| 趙淑芬校長                | 1997-2007 |
| 伍小冰校長                | 2007-2009 |
| 梁文姬校長                | 2009-2014 |
| 陸施燕燕校長               | 2014-2016 |
| 陳欽麒校長                | 2016-現在   |

## 著名/傑出校友

### 商界

  - [蘇樹輝](../Page/蘇樹輝.md "wikilink")：[澳博控股行政總裁](../Page/澳博控股.md "wikilink")
  - [莊紹綏](../Page/莊紹綏.md "wikilink")：[全國政協委員](../Page/全國政協委員.md "wikilink")、[莊士集團主席](../Page/莊士集團.md "wikilink")
  - [王國強](../Page/王國強.md "wikilink")：[香港科技大學榮譽大學院士](../Page/香港科技大學.md "wikilink")、香港廣東社團總會主席、前九龍城區議會主席
  - [伍達倫](../Page/伍達倫.md "wikilink")：前[香港理工大學校董會副主席](../Page/香港理工大學.md "wikilink")、
    前[僱員再培訓局主席](../Page/僱員再培訓局.md "wikilink")
  - [伍偉雄](../Page/伍偉雄.md "wikilink")：[新科實業有限公司主席](../Page/新科實業有限公司.md "wikilink")
  - 陳大倫：曾任美國通用汽車公司總經理、董事
  - 陳華偉：香港測量師學會建築測量組前主席
  - 麥炳球：金城營造集團執行董事、香港大學學電機及電子工程系榮譽首席講師

### 政界及公務員

  - [鄺志堅](../Page/鄺志堅.md "wikilink")：前[立法會議員（勞工界）](../Page/第四屆香港立法會.md "wikilink")
  - [蘇樹輝](../Page/蘇樹輝.md "wikilink")：[第十一屆全國政協委員](../Page/第十一屆全國政協委員.md "wikilink")
  - [鄔滿海](../Page/鄔滿海.md "wikilink")：[香港房屋協會主席](../Page/香港房屋協會.md "wikilink")、前屋宇署署長
  - [陳裘大](../Page/陳裘大.md "wikilink")：前香港[房屋署總屋宇裝備工程師](../Page/房屋署.md "wikilink")，香港著名男歌手[陳奕迅之父](../Page/陳奕迅.md "wikilink")
  - [彭贊榮](../Page/彭贊榮.md "wikilink")：前[差餉物業估價署署長](../Page/差餉物業估價署.md "wikilink")
  - [黎仕海](../Page/黎仕海.md "wikilink")：前[機電工程署署長](../Page/機電工程署.md "wikilink")
  - [梁德輝](../Page/梁德輝.md "wikilink")：[運輸署助理署長](../Page/運輸署.md "wikilink")
  - [黃偉雄](../Page/黃偉雄.md "wikilink")：前政府土地工程測量員協會主席
  - [潘兆初](../Page/潘兆初.md "wikilink")：[高等法院上訴法庭法官](../Page/高等法院.md "wikilink")
  - [周世傑](../Page/周世傑.md "wikilink")：[青年新政發言人](../Page/青年新政.md "wikilink")
  - [黃台仰](../Page/黃台仰.md "wikilink")：[本土民主前線召集人](../Page/本土民主前線.md "wikilink")
  - [鄧炳強](../Page/鄧炳強.md "wikilink")：[警務處](../Page/香港警務處.md "wikilink")[副處長(行動)](../Page/警務處副處長.md "wikilink")
  - 陳吉輝：前教統局首席助理秘書長(行政)
  - 黎偉雄：前機電工程署總工程師
  - 蘇禮賢：前警務處副處長（管理）

### 演藝界

  - [盧巧音](../Page/盧巧音.md "wikilink")：著名歌手、演員
  - [鄭秀文](../Page/鄭秀文.md "wikilink")：著名歌手、演員
  - [姜濤](../Page/姜濤.md "wikilink")：[ViuTV](../Page/ViuTV.md "wikilink")[真人秀節目](../Page/真人秀.md "wikilink")《[Good
    Night Show 全民造星](../Page/Good_Night_Show_全民造星.md "wikilink")》冠軍
  - [葉揚堃](../Page/葉揚堃.md "wikilink"): 網劇《反黑》飾演 蛇仔明
  - [劉朝健（Gordon)](../Page/劉朝健（Gordon\).md "wikilink"): 人氣YouTuber

### 學術及文化界

  - [鄧澤賢](../Page/鄧澤賢.md "wikilink")：[香港理工大學工業及系統工程學系教授](../Page/香港理工大學.md "wikilink")
  - [張學明](../Page/張學明_\(歷史學家\).md "wikilink")：[香港中文大學歷史系教授](../Page/香港中文大學.md "wikilink")，前香港中文大學歷史系副系主任
  - [陳錫僑](../Page/陳錫僑.md "wikilink")：[聖若瑟大學科學及環境研究所榮譽教授](../Page/聖若瑟大學.md "wikilink")
  - [嚴志明](../Page/嚴志明.md "wikilink")：[香港理工大學設計學院教授](../Page/香港理工大學.md "wikilink")（產品設計）、[香港設計中心](../Page/香港設計中心.md "wikilink")、[香港設計委員會](../Page/香港設計委員會.md "wikilink")、[香港傢俬裝飾廠商總會](../Page/香港傢俬裝飾廠商總會.md "wikilink")、[香港出口信用保險局和創新及科技基金科技券計劃委員會主席以及](../Page/香港出口信用保險局.md "wikilink")[香港工業總會和](../Page/香港工業總會.md "wikilink")[職業訓練局副主席](../Page/職業訓練局.md "wikilink")
  - 米耀榮：澳大利亞華裔材料科學與斷裂力學專家，畢業於[香港大學工程學院](../Page/香港大學.md "wikilink")，1969年獲學士學位，1972年獲博士學位。此後赴美國密西根大學、英國倫敦帝國學院從事博士後研究。1976年起任教於雪梨大學。1990年出任[雪梨大學工學院副院長](../Page/雪梨大學.md "wikilink")。
  - 黃杰波：前[香港理工大學電機工程系系主任](../Page/香港理工大學.md "wikilink")
  - 勞虔基：[職業訓練局高級副執行幹事](../Page/職業訓練局_\(香港\).md "wikilink")，前香港專業教育學院青衣分校院長
  - 張健波：前[《明報》總編輯](../Page/明報.md "wikilink")
  - 曹景鈞：[香港中文大學政治與行政學系客座教授](../Page/香港中文大學.md "wikilink")
  - 黃燕鑑：前[香港理工大學工業及系統工程學系講師](../Page/香港理工大學.md "wikilink")
  - 胡錦生：[香港中文大學前內科講座教授兼心臟科專科醫生](../Page/香港中文大學.md "wikilink")、現任香港中文大學未來城市研究所客座教授
  - 李錫勳 **：**前香港生產力促進局副總裁(主理產品發展)、前香港科技學院製造工程系系主任
  - 袁國培：《信報》副總編輯
  - 許智文：[香港理工大學建築及房地產學系教授](../Page/香港理工大學.md "wikilink")，該系副系主任。現時為上訴審裁團(《建築物條例》)成員、香港房屋協會監事會委員及市建局董事會非執行董事。
  - 陳冠男：中大文物館助理研究主任（書畫）

### 教育界

  - 林華煦：前政府教育人員職工會成員(前身為官立學校非學位教師職工會，又稱「官非會」)，曾任該會會長。在1972年文憑教師事件中，代表該會參與十三教育團體聯合秘書處（簡稱「聯秘處」)，領導教師行動，並同時出任秘書處副主席，司徒華則任主席。1976-1993年在維工任教。
  - 李國佳：前[金文泰中學校長](../Page/金文泰中學.md "wikilink")
  - 王燕振：前賽馬會官立工業中學校長
  - 陳祥偉：[伊利沙伯中學校長](../Page/伊利沙伯中學.md "wikilink")

### 醫學界

  - [岑思勁](../Page/岑思勁.md "wikilink")：[香港聖約翰救傷隊牙科總區高級監督醫生](../Page/香港聖約翰救傷隊.md "wikilink")

### 體育界

  - [程愛祖](../Page/程愛祖.md "wikilink")：[國際足球協會助理球証](../Page/國際足球協會.md "wikilink")
  - [蘇寶生](../Page/蘇寶生.md "wikilink")：香港著名業餘拳擊選手

### 其他

  - 容建文：前灣仔區童軍總監

## 外部連結

  - [鄧肇堅維多利亞官立中學](http://www.tskvgss.edu.hk/)
  - [鄧肇堅維多利亞官立中學校友會](http://www.tskvgss.edu.hk/alumni/)

[T](../Category/灣仔區中學.md "wikilink") [T](../Category/灣仔.md "wikilink")
[T](../Category/香港官立中學.md "wikilink")
[Category:1933年創建的教育機構](../Category/1933年創建的教育機構.md "wikilink")