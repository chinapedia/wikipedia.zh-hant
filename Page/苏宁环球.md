**苏宁环球股份有限公司**（，**Suning
Universal**，简称**苏宁环球**）是在[深圳证券交易所上市的一家主要以](../Page/深圳证券交易所.md "wikilink")[房地产开发与管理](../Page/房地产.md "wikilink")，及建筑材料生产与销售的企业。苏宁环球为控股公司，所有房地产项目均由下属子公司开发。目前，公司共有六家全资子公司和控股子公司，包括南京天华百润投资发展有限公司，南京华浦高科建材有限公司，江苏乾阳房地产开发有限公司，长春苏宁环球房地产开发有限公司，北京苏宁环球有限公司和吉林市苏宁环球有限公司。\[1\]

苏宁环球於2005年借殼上市(吉纸)。

## 資料來源

<div class="references-small">

<references />

</div>

## 外部链接

  - [苏宁环球集团](http://www.suning.com.cn/)
  - [苏宁环球股份有限公司](http://www.suning-universal.com/)

[category:中国民营企业](../Page/category:中国民营企业.md "wikilink")

[Category:深圳证券交易所上市公司](../Category/深圳证券交易所上市公司.md "wikilink")
[Category:中國房地產開發公司](../Category/中國房地產開發公司.md "wikilink")
[Category:中國民營企業](../Category/中國民營企業.md "wikilink")
[Category:江蘇公司](../Category/江蘇公司.md "wikilink")
[Category:南京商业](../Category/南京商业.md "wikilink")

1.  [1](http://www.suning-universal.com/xsgs.asp) 公司概况及下属子公司