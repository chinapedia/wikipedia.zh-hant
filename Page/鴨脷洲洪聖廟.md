[HK_ALC_Hung_Shing_Temple_Front_Door_1a.jpg](https://zh.wikipedia.org/wiki/File:HK_ALC_Hung_Shing_Temple_Front_Door_1a.jpg "fig:HK_ALC_Hung_Shing_Temple_Front_Door_1a.jpg")
[HK_ALC_Hung_Shing_Temple_Wishes.jpg](https://zh.wikipedia.org/wiki/File:HK_ALC_Hung_Shing_Temple_Wishes.jpg "fig:HK_ALC_Hung_Shing_Temple_Wishes.jpg")

**鴨脷洲洪聖廟**是[香港一所](../Page/香港.md "wikilink")[洪聖廟](../Page/洪聖廟.md "wikilink")，位於[香港島南部的](../Page/香港島.md "wikilink")[鴨脷洲](../Page/鴨脷洲.md "wikilink")[洪聖街](../Page/洪聖街.md "wikilink")9號，鄰近[鴨脷洲市政大廈及](../Page/鴨脷洲市政大廈.md "wikilink")[鴨脷洲海濱長廊](../Page/鴨脷洲海濱長廊.md "wikilink")，現已被列為[香港法定古蹟](../Page/香港法定古蹟.md "wikilink")。現由華人廟宇委員會管理。

## 歷史

鴨脷洲洪聖廟於[清朝](../Page/清朝.md "wikilink")[乾隆](../Page/乾隆.md "wikilink")38年（[1773年](../Page/1773年.md "wikilink")）由當地漁民集資建成，為[南區現存歷史最悠久的建築物](../Page/南區_\(香港\).md "wikilink")。廟宇先後有多次重修。\[1\]

## 建築

鴨脷洲洪聖廟屬於2進3間式設計，而前殿與正殿之間的[天井](../Page/天井.md "wikilink")，被加建了亭蓋。另外廟前豎立了兩支刻有龍紋圖案的木柱，街坊們都稱它為[龍柱](../Page/龍柱.md "wikilink")，是用作擋煞。

## 交通

<div class="NavFrame collapsed" style="background-color: #FFFF00; clear: no; border: 1px solid #999; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: center; font-weight: bold;">

交通路線列表

</div>

<div class="NavContent" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px;">

  - [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{南港島綫色彩}}">█</font>[南港島綫](../Page/南港島綫.md "wikilink")：[利東站](../Page/利東站.md "wikilink")

<!-- end list -->

  - [專線小巴](../Page/專線小巴.md "wikilink")

</div>

</div>

## 參見

<references />

|                                                                                                                                                                                 |                                                                                                                                                                                             |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [HK_ALC_Hung_Shing_Temple_Dragon_Heads.jpg](https://zh.wikipedia.org/wiki/File:HK_ALC_Hung_Shing_Temple_Dragon_Heads.jpg "fig:HK_ALC_Hung_Shing_Temple_Dragon_Heads.jpg") | [HK_ALC_Hung_Shing_Temple_Foundation_Stone.jpg](https://zh.wikipedia.org/wiki/File:HK_ALC_Hung_Shing_Temple_Foundation_Stone.jpg "fig:HK_ALC_Hung_Shing_Temple_Foundation_Stone.jpg") |

## 外面連結

  - [鴨脷洲洪聖廟](http://www.ctc.org.hk/b5/directcontrol/temple9.asp)
  - [三古廟同列法定古蹟-東方日報](http://orientaldaily.on.cc/cnt/news/20141025/mobile/odn-20141025-1025_00176_056.html)

[Category:香港洪聖廟](../Category/香港洪聖廟.md "wikilink")
[Category:鴨脷洲](../Category/鴨脷洲.md "wikilink")
[Category:香港非物質文化遺產](../Category/香港非物質文化遺產.md "wikilink")

1.  華僑日報, 1973-03-13 第9頁