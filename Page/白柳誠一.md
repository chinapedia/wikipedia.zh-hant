**白柳誠一**（）是[天主教](../Page/天主教.md "wikilink")[日本籍](../Page/日本.md "wikilink")[司鐸級樞機](../Page/樞機.md "wikilink")。也曾是[東京總教區總主教](../Page/天主教東京總教區.md "wikilink")。

## 生平

白柳誠一生於1928年6月17日，在[日本](../Page/日本.md "wikilink")[東京都](../Page/東京都.md "wikilink")[八王子市](../Page/八王子市.md "wikilink")。他於1954年12月21日晉鐸。1966年5月8日晉牧，並被時任[教宗](../Page/教宗.md "wikilink")[保祿六世任命為東京總教區輔理主教](../Page/保祿六世.md "wikilink")。1969年，任東京總教區助理主教。1994年11月26日，被時任[教宗](../Page/教宗.md "wikilink")[若望·保祿二世再冊封為](../Page/若望·保祿二世.md "wikilink")[司鐸級樞機](../Page/樞機.md "wikilink")。

他亦是[世界宗教者和平會議日本委員会理事長](../Page/世界宗教者和平會議.md "wikilink")、[日本宗教聯盟理事長](../Page/日本宗教聯盟.md "wikilink")。除了出任宗教和平團體的職務以外，亦擁有[SkyPerfect電視頻道](../Page/SkyPerfect.md "wikilink")、及擔當精神文化映像社株式會社的最高顧問。

直到1970年2月21日，由於[土井辰雄](../Page/土井辰雄.md "wikilink")[樞機出任去世](../Page/樞機.md "wikilink")，因此白柳誠一椄替成為[東京總教區正權總主教](../Page/天主教東京總教區.md "wikilink")。2000年2月17日，榮休並由[岡田武夫椄替出任總主教](../Page/岡田武夫.md "wikilink")。2009年12月30日去世。

## 學歷

  - 1951年[上智大学哲学科畢業](../Page/上智大学.md "wikilink")。
  - 1954年上智大学神学科修業，晉[司鐸職](../Page/司鐸.md "wikilink")。
  - 1960年[羅馬](../Page/羅馬.md "wikilink")[烏爾巴羅大學教会法学科畢業](../Page/烏爾巴羅大學.md "wikilink")，並取得法学博士資格。

## 晉鐸後簡歷

  - 1966年成為天主教東京總教區辅理主教。
  - 1969年成為天主教東京總教區助理主教。
  - 1970年成為天主教東京總教區總主教。
  - 1994年領天主教[司鐸級樞機職](../Page/樞機.md "wikilink")。

## 外部連結

  - [カトリック中央協議会](http://www.cbcj.catholic.jp/jpn/index.htm)
  - [カトリック新聞社サイト
    拉致――高まる不信の中でキリスト者の視座を問う](http://www.cwjpn.com/kijiback/y2002/3685/1p-rachi.htm)
  - [株式会社　精神文化映像社](https://web.archive.org/web/20050404222216/http://www.seishinbunka.co.jp/)

[Category:日本樞機](../Category/日本樞機.md "wikilink")
[Category:上智大學校友](../Category/上智大學校友.md "wikilink")
[Category:東京都出身人物](../Category/東京都出身人物.md "wikilink")