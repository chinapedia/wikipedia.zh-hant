**大曲市**（）是[秋田縣的一個已不存在的](../Page/秋田縣.md "wikilink")[市](../Page/市.md "wikilink")。2005年，與其他市町村[合併設立](../Page/市町村合併.md "wikilink")[大仙市](../Page/大仙市.md "wikilink")。
日本动漫作品[蜡笔小新中小新的父亲](../Page/蜡笔小新.md "wikilink")[野原广志出生在](../Page/野原广志.md "wikilink")[大曲市](../Page/大曲市.md "wikilink")，在动画中也多次出现该地的场景。

## 地理

  - 山:[大平山](../Page/大平山.md "wikilink")（姬神山）
  - 河川:[雄物川](../Page/雄物川.md "wikilink")、[丸子川](../Page/丸子川.md "wikilink")、[玉川](../Page/玉川.md "wikilink")
  - 湖沼:

## 历史

  - 1954年（昭和29年）5月3日 -
    仙北郡[大曲町](../Page/大曲町.md "wikilink")、[花館村](../Page/花館村.md "wikilink")、[內小友村](../Page/內小友村.md "wikilink")、[大川西根村](../Page/大川西根村.md "wikilink")、[藤木村](../Page/藤木村.md "wikilink")、[四屋村合併](../Page/四屋村.md "wikilink")，大曲市誕生。
  - 1955年（昭和30年）4月1日 - 平鹿郡[角間川町編入](../Page/角間川町.md "wikilink")。
  - 2005年（平成17年）3月22日 - 合併為大仙市。

[Omagari-shi](../Category/日本已廢除的市.md "wikilink")
[Omagari-shi](../Category/秋田縣行政區劃_\(廢除\).md "wikilink")
[Category:大仙市](../Category/大仙市.md "wikilink")