**钻石吧**（）中文又音译为**戴蒙德吧**，是位于[美国](../Page/美国.md "wikilink")[加州](../Page/加州.md "wikilink")[洛杉矶县郊外的一个城市](../Page/洛杉矶县.md "wikilink")。“钻石吧”这个名字源于Frederick
E. Lewis所注册的钢铁品牌“diamond over a bar”。目前市长为Ron
Everett。\[1\]根据[美国人口调查局](../Page/美国人口调查局.md "wikilink")2000年统计，人口为56,287。其中[亚裔占](../Page/亚裔美国人.md "wikilink")42.76%、[白人占](../Page/白人.md "wikilink")41.05%、[非洲裔占](../Page/非裔美国人.md "wikilink")4.76%、[美国原住民占](../Page/美国原住民.md "wikilink")0.33%、[太平洋岛民占](../Page/太平洋岛民.md "wikilink")0.12%、其他族裔占6.78%。有4.21%拥有两种族裔以上血统。[西语裔或](../Page/西语裔.md "wikilink")[拉丁裔为](../Page/拉丁裔.md "wikilink")18.46%。

## 參考資料

<div class="references-small">

<references />

</div>

## 外部链接

  - [市政厅网址](https://web.archive.org/web/20070318005551/http://www.ci.diamond-bar.ca.us/home/index.asp)

[category:洛杉矶县城市](../Page/category:洛杉矶县城市.md "wikilink")

[D](../Category/洛杉磯縣.md "wikilink")

1.