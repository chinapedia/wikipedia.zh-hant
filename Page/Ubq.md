**Ubq**是一種尚未被發現的化學[元素](../Page/元素.md "wikilink")，它的暫定[化学符号是](../Page/化学符号.md "wikilink")**Ubq**，[原子序数是](../Page/原子序数.md "wikilink")124，位于第8周期、g4族，属于[g區元素之一](../Page/g區元素.md "wikilink")。

## 歴史

2006年，國家重離子加速器研究所（法國國家研究所）的一個研究小組試圖透過一種用天然鍺照射鈾的新方法研究多種同位素的半衰期\[1\]。

## 参考资料

[8F](../Category/第8周期元素.md "wikilink")
[8F](../Category/化学元素.md "wikilink")

1.  [Direct experimental evidence for very long fission times of
    super-heavy
    elements](http://hal.archives-ouvertes.fr/docs/00/12/91/31/PDF/WAPHE06_EPJ_preprint1.pdf)HAL（フランスのオープンアーカイブ）