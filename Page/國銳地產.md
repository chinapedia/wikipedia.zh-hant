**國銳地產有限公司**，簡稱**國銳地產**（****，）前身為「高昇地產發展有限公司」和「建懋國際有限公司」，成立於1973年，同時在[香港交易所主板上市](../Page/香港交易所.md "wikilink")，當時原本於澳門和香港地產商，現時於中國房地產開發公司，業務為物業投資、物業貿易和物業管理。當時主席為[盧象乾](../Page/盧象乾.md "wikilink")（[盧嘉錫兒子](../Page/盧嘉錫.md "wikilink")），而[首席執行官為](../Page/首席執行官.md "wikilink")[黃海平](../Page/黃海平.md "wikilink")。但是現時主要股東為魏純暹（主席及首席執行官）。前股東為[莊漢傑和](../Page/莊漢傑.md "wikilink")[盧象乾](../Page/盧象乾.md "wikilink")。

大股東為[國銳集團有限公司](../Page/國銳集團有限公司.md "wikilink")。

當時是最早澳門澳門概念股，也是80年代購入澳門地皮，由於效益欠佳，故此把原旗下[嘉輝置業有限公司](../Page/嘉輝置業有限公司.md "wikilink")，以及Gladiolus
Trading
Limited所有權益出售給[莊漢傑](../Page/莊漢傑.md "wikilink")，2007年已正式開始轉移福建物業發展\[1\]。

2015年2月26日，國銳地產旗下East Pacific Properties LLC，以2500萬美元（1.94億港元）收購Wilshire
West Car Wash,
LLC旗下位於[美國](../Page/美國.md "wikilink")[加利福尼亞州](../Page/加利福尼亞州.md "wikilink")[洛杉磯](../Page/洛杉磯.md "wikilink")[聖莫尼卡市Chelsea](../Page/聖莫尼卡市.md "wikilink")
Green Tract第4區地段1、2、3、4、5、6及7物業，面積約為40,615平方呎\[2\]。

2016年7月11日，該公司旗下GR Properties UK
Limited，以2778萬英鎊（2.79億港元\[3\]），收購位於[英國](../Page/英國.md "wikilink")[倫敦Boundary](../Page/倫敦.md "wikilink")
House, 7-17 Jewry Street, London EC3N 2EXX（受該等租約所規限）物業權益，賣方為倫敦上市公司Picton
Property Income Limited若干旗下公司\[4\]。

## 參考

## 參考

  - [莊漢傑家族](../Page/莊漢傑家族.md "wikilink")
  - [盧嘉錫家族](../Page/盧嘉錫家族.md "wikilink")

## 外部链接

  - [GRProperties.com.hk](http://www.grproperties.com.hk)

[Category:香港上市地產公司](../Category/香港上市地產公司.md "wikilink")
[Category:中國民營企業](../Category/中國民營企業.md "wikilink")
[Category:中國房地產開發公司](../Category/中國房地產開發公司.md "wikilink")
[Category:福建公司](../Category/福建公司.md "wikilink")
[Category:1973年成立的公司](../Category/1973年成立的公司.md "wikilink")
[Category:北京市公司](../Category/北京市公司.md "wikilink")

1.  [上車落車：資產清一色建懋簡而清](http://appledaily.atnext.com/template/apple/art_main.cfm?iss_id=20060805&sec_id=15307&subsec_id=15326&art_id=6191747)
2.  [國銳地產非常重大收購事項
    有關收購該物業通告](http://www.hkexnews.hk/listedco/listconews/SEHK/2015/0309/LTN20150309014_C.pdf)
3.  截至2016年7月11日止，英鎊兌港元收盤報為10.05，由[湯森路透提供](../Page/湯森路透.md "wikilink")
4.  [國銳地產主要交易
    收購英國倫敦一座辦公大樓通告](http://www.hkexnews.hk/listedco/listconews/SEHK/2016/0711/LTN20160711822_C.pdf)