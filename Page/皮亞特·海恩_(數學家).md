[Piet_Hein_and_H.C._Andersen_(cropped).jpg](https://zh.wikipedia.org/wiki/File:Piet_Hein_and_H.C._Andersen_\(cropped\).jpg "fig:Piet_Hein_and_H.C._Andersen_(cropped).jpg")

**皮亞特·海恩**（，），生於[丹麥](../Page/丹麥.md "wikilink")[哥本哈根](../Page/哥本哈根.md "wikilink")，是科學家、[數學家](../Page/數學家.md "wikilink")、發明家、詩人和作家，筆名“Kumbel”（意即“墓碑”）。

## 生平

他曾於[哥本哈根大學的理論物理學研究院](../Page/哥本哈根大學.md "wikilink")（後來的[尼爾波爾研究院](../Page/尼爾波爾研究院.md "wikilink"))和[丹麥科技大學修讀](../Page/丹麥科技大學.md "wikilink")。1972年，[耶魯大學頒了榮譽博士學位予他](../Page/耶魯大學.md "wikilink")。1996年，他死於[菲英島](../Page/菲英島.md "wikilink")，自己的家裏。

他是[荷蘭英雄](../Page/荷蘭.md "wikilink")、另一個[皮亞特·海恩的後代](../Page/皮亞特·海恩_\(荷蘭\).md "wikilink")。

## 工作

  - 多篇的短小、富哲理的[Grook](../Page/Grook.md "wikilink")
  - 主張使用[超橢圓線在](../Page/超橢圓.md "wikilink")[城市規劃](../Page/城市規劃.md "wikilink")、[傢俬設計及其他範疇](../Page/傢俬設計.md "wikilink")

發明的遊戲：

  - [六貫棋](../Page/六貫棋.md "wikilink")
  - [Morra](../Page/Morra.md "wikilink")
  - [Polytaire](../Page/Polytaire.md "wikilink")
  - [Tangloids](../Page/Tangloids.md "wikilink")
  - [TacTix](../Page/TacTix.md "wikilink")
  - [Tower](../Page/Tower.md "wikilink")
  - [索瑪立方塊](../Page/索瑪立方塊.md "wikilink")

## 引用書目

  - "A Poet with a Slide Rule: Piet Hein Bestrides Art and Science," by
    Jim Hicks, *Life Magazine*, Vol. 61 No. 16, 10/14/66, pp.55-66
  - *Grooks*, by Piet Hein, (1998) Borgens Forlag; ISBN 8741810791
  - *Collected Grooks I*, by Piet Hein Borgens Forlag; ISBN 8721018596
  - *Collected Grooks II*, by Piet Hein Borgens Forlag; ISBN 8721018618

## 參考

  - [马丁·加德纳](../Page/马丁·加德纳.md "wikilink"): Piet Hein's Superellipse. -
    in Gardner, Martin: Mathematical Carnival. A New Round-Up of
    Tantalizers and Puzzles from Scientific American. New York: Vintage,
    1977, pp. 240-254.
  - Johan Gielis: Inventing the circle. The geometry of nature. -
    Antwerpen : Geniaal Press, 2003. - ISBN 9080775614

## 外部連結

  - [Piet Hein Homepage](http://www.piethein.com)
  - [Notes on Piet
    Hein](https://web.archive.org/web/20050305223250/http://www.ctaz.com/~dmn1/hein.htm)
  - [Grooks by Piet
    Hein](https://web.archive.org/web/20060209024728/http://chat.carleton.ca/~tcstewar/grooks/grooks.html)

[Category:智力遊戲設計師](../Category/智力遊戲設計師.md "wikilink")
[Category:丹麥詩人](../Category/丹麥詩人.md "wikilink")
[Category:丹麦数学家](../Category/丹麦数学家.md "wikilink")
[Category:丹麦发明家](../Category/丹麦发明家.md "wikilink")
[Category:哥本哈根大學校友](../Category/哥本哈根大學校友.md "wikilink")
[Category:丹麥技術大學校友](../Category/丹麥技術大學校友.md "wikilink")