**副巨犀屬**（[學名](../Page/學名.md "wikilink")：）為[巨犀類下的一個屬](../Page/巨犀類.md "wikilink")，為一種已經滅絕的[犀牛](../Page/犀牛.md "wikilink")，生活在[漸新世](../Page/漸新世.md "wikilink")。巨犀也是目前已知第二大的陸行[哺乳動物](../Page/哺乳動物.md "wikilink")，僅次於[納瑪象](../Page/納瑪象.md "wikilink")，估計成年巨犀平均高5.2公尺（18呎），身長為8.2公尺（27呎），重約22噸，為草食性，巨犀使用似獠牙狀的上顎齒與下顎齒來覓食樹葉。

巨犀擁有一個無角的長頭部，鼻骨與額骨成拱狀。巨犀的化石在[亞洲中部與](../Page/亞洲.md "wikilink")[巴基斯坦南部](../Page/巴基斯坦.md "wikilink")[俾路支省都有出土](../Page/俾路支省.md "wikilink")，尚未在其他地區發現。

<File:Indricotherium> transouralicum.jpg|*P. transouralicum* 頭顱骨化石。

<File:Indricotherium>
skeleton.jpg|巨犀全身骨骼，[日本](../Page/日本.md "wikilink")[国立科学博物館](../Page/国立科学博物館.md "wikilink")

## 外部連結

  - [Discovery
    Channel](https://web.archive.org/web/20070808185456/http://dsc.discovery.com/convergence/beasts/know/fortelius.html)
    - Answers from Dr. Mikael Fortelius
  - [《中国的巨犀化石》](https://book.douban.com/subject/2273526/)

[Category:巨犀亞科](../Category/巨犀亞科.md "wikilink")
[Category:漸新世哺乳類](../Category/漸新世哺乳類.md "wikilink")
[Category:亞洲史前哺乳動物](../Category/亞洲史前哺乳動物.md "wikilink")