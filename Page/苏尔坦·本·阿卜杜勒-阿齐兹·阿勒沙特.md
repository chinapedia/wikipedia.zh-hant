[Prince_Sultan.jpg](https://zh.wikipedia.org/wiki/File:Prince_Sultan.jpg "fig:Prince_Sultan.jpg")
**苏尔坦·本·阿卜杜勒-阿齐兹·本·阿卜杜-拉赫曼·本·费萨尔·阿勒沙特[亲王](../Page/亲王.md "wikilink")[殿下](../Page/殿下.md "wikilink")**（，），曾任[沙特阿拉伯王国](../Page/沙特阿拉伯.md "wikilink")[王储兼](../Page/王储.md "wikilink")[第一副首相](../Page/沙特阿拉伯首相.md "wikilink")、[国防](../Page/国防.md "wikilink")[航空](../Page/航空.md "wikilink")[大臣和军队总监](../Page/大臣.md "wikilink")，沙特阿拉伯王国开国[君主](../Page/君主.md "wikilink")[阿卜杜勒-阿齐兹·本·阿卜杜-拉赫曼·阿勒沙特](../Page/阿卜杜勒-阿齐兹·本·阿卜杜-拉赫曼·阿勒沙特.md "wikilink")（即[伊本·沙特](../Page/伊本·沙特.md "wikilink")）第十三子。

2005年8月[阿卜杜拉国王即位后被选为正](../Page/阿卜杜拉·本·阿卜杜勒-阿齐兹·阿勒沙特.md "wikilink")[王储](../Page/王储.md "wikilink")。但苏尔坦因为不幸患上[癌症](../Page/癌症.md "wikilink")，2011年6月到美國接受治療，於10月22日在[纽约医院逝世](../Page/纽约.md "wikilink")，享年86岁。\[1\]2011年10月25日在利雅得举行葬礼。仅仅8个月后，他的弟弟，2012年6月16日，[沙特阿拉伯王国新王储](../Page/沙特阿拉伯王国.md "wikilink")[纳伊夫在](../Page/纳伊夫·本·阿卜杜勒－阿齐兹·阿勒沙特.md "wikilink")[瑞士](../Page/瑞士.md "wikilink")[日内瓦也逝世](../Page/日内瓦.md "wikilink")，享年78岁。這位副王儲的葬礼于2012年6月17日在沙特阿拉伯[麦加大清真寺举行](../Page/麦加大清真寺.md "wikilink")。這二位都沒能擔任國王一職。

## 生平事蹟

1930年12月30日生于[利雅得](../Page/利雅得.md "wikilink")，是前任国王[法赫德的胞弟](../Page/法赫德·本·阿卜杜勒-阿齐兹·阿勒沙特.md "wikilink")。自幼接受[伊斯兰教育和军事训练](../Page/伊斯兰.md "wikilink")，年轻时即任皇家卫队司令，1947年任[利雅得地区行政长官](../Page/利雅得.md "wikilink")，1955年任农业大臣，1962年任国防大臣及航空部大臣，1982年起任第二副首相。思想较为开明，与西方国家尤其是美、英、法等关系密切。

子女方面，苏尔坦有二个儿子，长子[班达尔·本·苏尔坦·阿勒沙特亲王生于](../Page/班达尔·本·苏尔坦·阿勒沙特.md "wikilink")1949年，1983年至2005年任[沙烏地阿拉伯驻美国大使](../Page/沙烏地阿拉伯.md "wikilink")；次子[哈立德·本·苏尔坦·阿勒沙特](../Page/哈立德·本·苏尔坦·阿勒沙特.md "wikilink")。

## 參考資料

## 外部链接

  - [Saudi Info website](http://www.saudinf.com/main/b472.htm)
  - [Prince Sultan Bin Abdulaziz International Prize for Water -
    Official Website](http://www.psipw.org/)
  - [King Fahd's death,
    succession](https://web.archive.org/web/20060619132641/http://english.aljazeera.net/NR/exeres/32968774-4B27-4A5B-9C72-B0B382948A24.htm)
  - [Ministry of Foreign
    Affairs](http://www.mofa.gov.sa/Detail.asp?InNewsItemID=34437)

[Category:沙特阿拉伯储君](../Category/沙特阿拉伯储君.md "wikilink")
[Category:沙特阿拉伯副首相](../Category/沙特阿拉伯副首相.md "wikilink")
[Category:沙特阿拉伯农业大臣](../Category/沙特阿拉伯农业大臣.md "wikilink")
[Category:沙特阿拉伯国防大臣](../Category/沙特阿拉伯国防大臣.md "wikilink")
[Category:未继位的储君](../Category/未继位的储君.md "wikilink")
[Category:利雅德人](../Category/利雅德人.md "wikilink")

1.  [《沙特王储苏尔坦在纽约医院去世》](http://www.bbc.co.uk/zhongwen/simp/rolling_news/2011/10/111022_rolling_saudi_sultan_death.shtml)，BBC中文網，2011年10月22日。