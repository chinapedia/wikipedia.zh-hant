**李國祥**（；），1990年代[香港](../Page/香港.md "wikilink")[男歌手](../Page/男歌手.md "wikilink")。

李國祥畢業於[香港調景嶺中學](../Page/香港調景嶺中學.md "wikilink")（小學部及初中），為該校1978年第27屆小六畢業生\[1\]。之後入讀[萃華英文書院](../Page/萃華英文書院.md "wikilink")\[2\]、[元朗中山紀念英文書院](../Page/元朗.md "wikilink")（中三至中五）。他在1989年參加[無線電視主辦第八屆](../Page/無線電視.md "wikilink")[新秀歌唱大賽更奪得當天五位評審一致裁定的](../Page/新秀歌唱大賽.md "wikilink")《歌星特選獎》，同年簽約香港嘉禾電影與日本波麗佳音\]合作的[嘉音唱片而入行](../Page/嘉音唱片.md "wikilink")。1991年正式出道成為歌手，推出首張粵語專輯《藍色Sha
La La》，並於該年獲[商業電台頒發](../Page/商業電台.md "wikilink")「1991年度叱咤樂壇新力軍男歌手金獎」。

1993年初，李國祥的主打歌曲《餘情未了》得到各傳媒熱播而在坊間迅速流行起來，歌曲的流行為他的音樂事業帶來突破，更為他贏得首個歌曲獎項，人氣一度急升；下半年，再與歌手[倫永亮推出合唱歌曲](../Page/倫永亮.md "wikilink")《總有你鼓勵》(改編自[吳奇隆的](../Page/吳奇隆.md "wikilink")《祝你一路順風》)得到不俗反應及再度成為得獎金曲，是他同一年內第二首流行代表作品。李國祥曾先後推出多張[粵語及](../Page/粵語.md "wikilink")[國語專輯](../Page/國語.md "wikilink")，更曾於1994年以國語專輯打入新加坡最佳新人獎五強之列。李國祥在1996年推出最後一張專輯《離愛出走》後約滿嘉音唱片。現時，李國祥除了唱歌之外，亦從事催眠諮詢的工作。

李國祥歌唱代表作品包括：《摘星的晚上》、《餘情未了》、《總有你鼓勵》、《最美世界=你＋我》、《愛在地球毀滅時》等。

## 近年發展

2018年在麥花臣舉行《李國祥Share With You演唱會》

2015年在麥花臣舉行《餘情未了》演唱會\[3\]

於2016年度擔任「微笑企業」和「微笑員工」的評審，並頒獎予港澳地區多個於微笑服務表現優異的企業及員工。\[4\]

2016年李國祥在《壹週刊》1394期訪問中公開出櫃\[5\]，表明其同性戀者身份。

2017年李國祥在《流行經典50強》演繹《餘情未了》，被網民大讚「零瑕疵」\[6\]

## 獎項

1989年：

  - [無線電視第八屆新秀歌唱大賽奪得五位評審一致裁定](../Page/無線電視.md "wikilink")【歌星特選獎】

1991年：

  - [商業電台叱吒樂壇流行榜頒獎典禮](../Page/商業電台.md "wikilink")【叱吒樂壇新力軍男歌手金獎】
  - [香港電台第](../Page/香港電台.md "wikilink")14屆十大中文金曲頒獎禮【最有前途新人獎優異獎】

1993年：

  - 香港[無線電視勁歌金曲第一季季選](../Page/無線電視.md "wikilink")【金曲獎《餘情未了》】
  - 香港[無線電視勁歌金曲第三季季選](../Page/無線電視.md "wikilink")【金曲獎《總有你鼓勵》】

2013年：

  - 華語金曲獎2013【優秀醇聲歌手獎】

2015年：

  - 華語金曲獎2015【年度最佳HIFI人聲大碟】－李國祥《發燒情未了》

2016年：

  - 廣東音像城《發燒天碟榜》－【廣州樂壇至尊歌手大獎】

## 音樂作品

### 個人專輯

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>#</p></th>
<th style="text-align: left;"><p>專輯名稱</p></th>
<th style="text-align: left;"><p>專輯類型</p></th>
<th style="text-align: left;"><p>發行公司</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>1</p></td>
<td style="text-align: left;"><p>Blue</p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/嘉音唱片.md" title="wikilink">嘉音唱片</a></p></td>
<td style="text-align: left;"><p>1991年6月</p></td>
<td style="text-align: left;"><p>曲目</p>
<ol>
<li>夜</li>
<li>心倦</li>
<li>藍色Sha La La</li>
<li>獨來獨往</li>
<li>從前</li>
<li>手為誰牽</li>
<li>藍夜</li>
<li>夕陽路上</li>
<li>One way love</li>
<li>祈望</li>
<li>情感句號</li>
<li>心倦（acoustic mix）</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>2</p></td>
<td style="text-align: left;"><p>摘星的晚上</p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/嘉音唱片.md" title="wikilink">嘉音唱片</a></p></td>
<td style="text-align: left;"><p>1992年3月</p></td>
<td style="text-align: left;"><p>曲目</p>
<ol>
<li>摘星的晚上</li>
<li>你是否還記得那從前</li>
<li>任性</li>
<li>Miss You Fever</li>
<li>哭一秒鐘</li>
<li>我的女人</li>
<li>將往事忘了</li>
<li>只因留著你</li>
<li>依依不捨</li>
<li>最後的浪漫</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>3</p></td>
<td style="text-align: left;"><p>餘情未了</p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/嘉音唱片.md" title="wikilink">嘉音唱片</a></p></td>
<td style="text-align: left;"><p>1992年12月</p></td>
<td style="text-align: left;"><p>曲目</p>
<ol>
<li>全為了你</li>
<li>餘情未了</li>
<li>苦酒滿杯</li>
<li>你好</li>
<li>一人派對</li>
<li>寧願</li>
<li>流逝</li>
<li>無盡冷雨下</li>
<li>舊時情．今時情</li>
<li>倒後鏡</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>4</p></td>
<td style="text-align: left;"><p>餘情停不了</p></td>
<td style="text-align: left;"><p>新曲+精選</p></td>
<td style="text-align: left;"><p><a href="../Page/嘉音唱片.md" title="wikilink">嘉音唱片</a></p></td>
<td style="text-align: left;"><p>1993年5月</p></td>
<td style="text-align: left;"><p>曲目</p>
<ol>
<li>杜鵑花的日子（國）</li>
<li>我的心停不下來（國）</li>
<li>我這麼愛你錯了嗎（國）</li>
<li>一人派對</li>
<li>夕陽路上</li>
<li>藍色Sha La La</li>
<li>我的女人</li>
<li>從前</li>
<li>只因留著你</li>
<li>摘星的晚上</li>
<li>你好</li>
<li>餘情未了</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>5</p></td>
<td style="text-align: left;"><p>多一點溫柔</p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/嘉音唱片.md" title="wikilink">嘉音唱片</a></p></td>
<td style="text-align: left;"><p>1993年9月</p></td>
<td style="text-align: left;"><p>曲目</p>
<ol>
<li>1/2戀愛</li>
<li>總有你鼓勵</li>
<li>一生的歌</li>
<li>掩飾</li>
<li>有你在途</li>
<li>多一點溫柔</li>
<li>在我眼內唯有你</li>
<li>La pa pa</li>
<li>降罪情人</li>
<li>俏佳人</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>6</p></td>
<td style="text-align: left;"><p>醉（國語）</p></td>
<td style="text-align: left;"><p>EP</p></td>
<td style="text-align: left;"><p><a href="../Page/奇花音樂.md" title="wikilink">奇花音樂</a><br />
<a href="../Page/波麗佳音.md" title="wikilink">波麗佳音</a></p></td>
<td style="text-align: left;"><p>1994年3月</p></td>
<td style="text-align: left;"><p>{{hidden| 曲目|</p>
<ol>
<li>醉</li>
<li>杜鵑花的日子</li>
<li>醉（心碎激情版）</li>
<li>我的心停不下來</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>7</p></td>
<td style="text-align: left;"><p>KC真性情</p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/嘉音唱片.md" title="wikilink">嘉音唱片</a></p></td>
<td style="text-align: left;"><p>1994年5月</p></td>
<td style="text-align: left;"><p>{{hidden| 曲目|</p>
<ol>
<li>最美世界＝你＋我</li>
<li>真性情</li>
<li>妒忌</li>
<li>每天心痛半點鐘</li>
<li>請妳相信我好嗎？</li>
<li>找到最深愛</li>
<li>雙人床</li>
<li>飛身</li>
<li>生命凝聚</li>
<li>無名字的妳</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>8</p></td>
<td style="text-align: left;"><p>醉生梦死（國語）<br />
(台灣版名為《餘情未了》)</p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/奇花音樂.md" title="wikilink">奇花音樂</a><br />
<a href="../Page/波麗佳音.md" title="wikilink">波麗佳音</a></p></td>
<td style="text-align: left;"><p>1994年11月</p></td>
<td style="text-align: left;"><p>曲目</p>
<ol>
<li>醉</li>
<li>愛不能隨便說說</li>
<li>杜鵑花日子</li>
<li>隨風聚散</li>
<li>我這樣愛你錯了嗎</li>
<li>我的心停不下來</li>
<li>滿天星</li>
<li>充滿光彩</li>
<li>單身Party</li>
<li>最後還有你</li>
<li>醉（心碎激情版）</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>9</p></td>
<td style="text-align: left;"><p>情到濃時（國語）</p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/奇花音樂.md" title="wikilink">奇花音樂</a><br />
<a href="../Page/波麗佳音.md" title="wikilink">波麗佳音</a></p></td>
<td style="text-align: left;"><p>1995年</p></td>
<td style="text-align: left;"><p>曲目</p>
<ol>
<li>情到濃時</li>
<li>終於明白</li>
<li>不願再作傷心人</li>
<li>不再愛</li>
<li>莫名愛上你</li>
<li>了解</li>
<li>孤單夜漫長</li>
<li>有夢是因為你愛我</li>
<li>刻骨銘心</li>
<li>你是否還記得那從前</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>10</p></td>
<td style="text-align: left;"><p>九五變奏</p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/嘉音唱片.md" title="wikilink">嘉音唱片</a></p></td>
<td style="text-align: left;"><p>1995年10月</p></td>
<td style="text-align: left;"><p>曲目</p>
<ol>
<li>為未來療傷</li>
<li>非愛不可</li>
<li>傻瓜</li>
<li>像我這麼的身份</li>
<li>遺憾</li>
<li>禁不住</li>
<li>答應我妳會愛上好男人</li>
<li>純愛星球</li>
<li>飛</li>
<li>躲不開這結果</li>
<li>情到濃時</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>11</p></td>
<td style="text-align: left;"><p>離愛出走（國語）</p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/奇花音樂.md" title="wikilink">奇花音樂</a><br />
<a href="../Page/波麗佳音.md" title="wikilink">波麗佳音</a></p></td>
<td style="text-align: left;"><p>1996年2月</p></td>
<td style="text-align: left;"><p>曲目</p>
<ol>
<li>情難再續</li>
<li>手裡的感情線</li>
<li>深情不移</li>
<li>離愛出走</li>
<li>人在情長在</li>
<li>我不要再聽</li>
<li>你是我的黑夜</li>
<li>愛太多</li>
<li>我是否能讓愛回頭</li>
<li>錯過</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>12</p></td>
<td style="text-align: left;"><p>李國祥精選17首</p></td>
<td style="text-align: left;"><p>精選</p></td>
<td style="text-align: left;"><p><a href="../Page/嘉音唱片.md" title="wikilink">嘉音唱片</a></p></td>
<td style="text-align: left;"><p>2000年4月</p></td>
<td style="text-align: left;"><p>曲目</p>
<ol>
<li>藍色Sha La La</li>
<li>心倦</li>
<li>從前</li>
<li>摘星的晚上</li>
<li>餘情未了</li>
<li>苦酒滿杯</li>
<li>你好</li>
<li>總有你鼓勵</li>
<li>多一點溫柔</li>
<li>最美世界＝你＋我</li>
<li>無名字的你</li>
<li>為未來療傷</li>
<li>答應我你會愛上好男人</li>
<li>我這樣愛你錯了嗎</li>
<li>我的心停不下來</li>
<li>情到濃時</li>
<li>醉</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>13</p></td>
<td style="text-align: left;"><p>至愛精選</p></td>
<td style="text-align: left;"><p>精選</p></td>
<td style="text-align: left;"><p><a href="../Page/嘉音唱片.md" title="wikilink">嘉音唱片</a></p></td>
<td style="text-align: left;"><p>2001年5月24日</p></td>
<td style="text-align: left;"><p>曲目</p>
<ol>
<li>餘情未了</li>
<li>摘星的晚上</li>
<li>藍色Sha la la</li>
<li>最美世界＝你＋我</li>
<li>無名字的你</li>
<li>為未來療傷</li>
<li>醉</li>
<li>雙人床</li>
<li>答應我妳會愛上好男人</li>
<li>總有你鼓勵</li>
<li>全為了你</li>
<li>心倦</li>
<li>任性</li>
<li>將往事忘了</li>
<li>禁不住</li>
<li>飛</li>
<li>非愛不可</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>14</p></td>
<td style="text-align: left;"><p>循環再唱</p></td>
<td style="text-align: left;"><p>翻唱大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/東亞唱片.md" title="wikilink">東亞唱片</a></p></td>
<td style="text-align: left;"><p>2005年8月11日</p></td>
<td style="text-align: left;"><p>曲目</p>
<ol>
<li>為什麼</li>
<li>一首獨唱的歌</li>
<li>願</li>
<li>如風</li>
<li>內心戲</li>
<li>忘記他</li>
<li>零時十分</li>
<li>我和春天有個約會</li>
<li>我可以抱你嗎</li>
<li>童年</li>
<li>味道</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>15</p></td>
<td style="text-align: left;"><p>The Best Song of KC Lee</p></td>
<td style="text-align: left;"><p>精選</p></td>
<td style="text-align: left;"><p><a href="../Page/豐華唱片.md" title="wikilink">豐華唱片</a></p></td>
<td style="text-align: left;"><p>2010年8月6日</p></td>
<td style="text-align: left;"><p>曲目 Disc 1：</p>
<ol>
<li>餘情未了</li>
<li>摘星的晚上</li>
<li>藍色Sha la la</li>
<li>最美世界＝你＋我</li>
<li>無名字的你</li>
<li>為未來療傷</li>
<li>多一點溫柔</li>
<li>雙人床</li>
<li>答應我妳會愛上好男人</li>
<li>總有你鼓勵</li>
<li>全為了你</li>
<li>心倦</li>
<li>任性</li>
<li>禁不住</li>
<li>飛</li>
<li>非愛不可</li>
</ol>
<p>Disc 2：<br />
# 杜鵑花的日子</p>
<ol>
<li>我的心停不下來</li>
<li>我這樣愛你錯了嗎</li>
<li>情到濃時</li>
<li>不願再作傷心人</li>
<li>不再愛</li>
<li>莫名愛上你</li>
<li>瞭解</li>
<li>孤單夜漫長</li>
<li>有夢是因為你愛我</li>
<li>刻骨銘心</li>
<li>你是否還記得那從前</li>
<li>醉-心碎激情版</li>
<li>隨風聚散</li>
<li>滿天星</li>
<li>單身Party</li>
<li>最後還有你</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>16</p></td>
<td style="text-align: left;"><p>循環再唱（K2CD）</p></td>
<td style="text-align: left;"><p>翻唱大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/New_Century_Workshop_(HK).md" title="wikilink">New Century Workshop (HK)</a></p></td>
<td style="text-align: left;"><p>2011年5月20日</p></td>
<td style="text-align: left;"><p>曲目</p>
<ol>
<li>為什麼</li>
<li>一首獨唱的歌</li>
<li>願</li>
<li>如風</li>
<li>內心戲</li>
<li>忘記他</li>
<li>零時十分</li>
<li>我和春天有個約會</li>
<li>我可以抱你嗎</li>
<li>童年</li>
<li>味道</li>
<li>藍色Sha La La</li>
<li>摘星的晚上</li>
<li>餘情未了</li>
<li>總有你鼓勵</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>17</p></td>
<td style="text-align: left;"><p>Love Actually</p></td>
<td style="text-align: left;"><p>翻唱大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/New_Century_Workshop_(HK).md" title="wikilink">New Century Workshop (HK)</a></p></td>
<td style="text-align: left;"><p>2011年12月23日</p></td>
<td style="text-align: left;"><p>曲目</p>
<ol>
<li>父母恩</li>
<li>父親的鋼琴</li>
<li>（媽媽） I Love You</li>
<li>陪著你走</li>
<li>愛是無涯</li>
<li>偶遇</li>
<li>說不出（2011新歌）</li>
<li>一些</li>
<li>裝飾的眼淚</li>
<li>人生可有知己</li>
</ol></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>18</p></td>
<td style="text-align: left;"><p>發燒情未了</p></td>
<td style="text-align: left;"><p>翻唱大碟</p></td>
<td style="text-align: left;"><p><a href="../Page/無比傳真.md" title="wikilink">無比傳真</a></p></td>
<td style="text-align: left;"><p>2014年11月28日</p></td>
<td style="text-align: left;"><p>曲目</p>
<ol>
<li>恰似你的溫柔</li>
<li>赤子</li>
<li>你的眼神</li>
<li>餘情未了</li>
<li>情人</li>
<li>限期（2014新歌）</li>
<li>明星</li>
<li>雪中情</li>
<li>回旋木馬的終端</li>
<li>浪子心聲</li>
<li>無價</li>
<li>多一點溫柔</li>
</ol></td>
</tr>
<tr class="odd">
<td style="text-align: left;"><p>19</p></td>
<td style="text-align: left;"><p>心倦 (3"CD) (限量編號版)</p></td>
<td style="text-align: left;"><p>細碟</p></td>
<td style="text-align: left;"><p><a href="../Page/New_Century_Workshop_(HK).md" title="wikilink">New Century Workshop (HK)</a></p></td>
<td style="text-align: left;"><p>2015年7月29日</p></td>
<td style="text-align: left;"><p>曲目</p>
<ol>
<li>心倦 (Acoustic Mix)</li>
<li>杜鵑花日子</li>
<li>情到濃時(國語版)</li>
<li>餘情未了</li>
<li>全為了你 Remix</li>
</ol></td>
</tr>
</tbody>
</table>

### 合輯

<table>
<thead>
<tr class="header">
<th style="text-align: left;"><p>#</p></th>
<th style="text-align: left;"><p>專輯名稱</p></th>
<th style="text-align: left;"><p>專輯類型</p></th>
<th style="text-align: left;"><p>發行公司</p></th>
<th style="text-align: left;"><p>發行日期</p></th>
<th style="text-align: left;"><p>曲目</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><p>1</p></td>
<td style="text-align: left;"><p>《<a href="../Page/我和殭屍有個約會2.md" title="wikilink">我和殭屍有個約會2</a>》原聲大碟</p></td>
<td style="text-align: left;"><p>大碟</p></td>
<td style="text-align: left;"><p>英皇娛樂</p></td>
<td style="text-align: left;"><p>2000年6月</p></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;"><p>2</p></td>
<td style="text-align: left;"><p><a href="../Page/有Folk氣-非一般民歌演唱會.md" title="wikilink">有Folk氣-非一般民歌演唱會</a></p></td>
<td style="text-align: left;"><p>現場錄音</p></td>
<td style="text-align: left;"><p><a href="../Page/環星音樂.md" title="wikilink">環星音樂</a></p></td>
<td style="text-align: left;"><p>2003年3月</p></td>
<td style="text-align: left;"></td>
</tr>
</tbody>
</table>

### 單曲

2015年：《好愛他》
2016年：《給孩子的話》

## 派台歌曲成績

<div class="notice metadata" id="disambig">

<small>[Nuvola_apps_kalarm.png](https://zh.wikipedia.org/wiki/File:Nuvola_apps_kalarm.png "fig:Nuvola_apps_kalarm.png")这是一個**[动态的](../Category/动态列表.md "wikilink")[未完成列表](../Category/未完成列表.md "wikilink")**，内容可能会随时增加，所以它可能永远也不会補充完整，但歡迎您隨時修改并[列明来源](../Page/Wikipedia:列明来源.md "wikilink")。</small>

</div>

<includeonly></includeonly><noinclude>

| **派台歌曲成績**                                                                    |
| ----------------------------------------------------------------------------- |
| 唱片                                                                            |
| **1991年**                                                                     |
| [Blue](../Page/Blue_\(李國祥專輯\).md "wikilink")                                  |
| Blue                                                                          |
| Blue                                                                          |
| Blue                                                                          |
| Blue                                                                          |
| **1992年**                                                                     |
| [摘星的晚上](../Page/摘星的晚上.md "wikilink")                                          |
| 摘星的晚上                                                                         |
| 摘星的晚上                                                                         |
| 摘星的晚上                                                                         |
| [餘情未了](../Page/餘情未了.md "wikilink")                                            |
| **1993年**                                                                     |
| 餘情未了                                                                          |
| 餘情未了                                                                          |
| 餘情未了                                                                          |
| [多一點溫柔](../Page/多一點溫柔.md "wikilink") / [一個人在途上](../Page/一個人在途上.md "wikilink") |
| 多一點溫柔                                                                         |
| **1994年**                                                                     |
| [K C真性情](../Page/K_C真性情.md "wikilink")                                        |
| K C真性情                                                                        |
| **1995年**                                                                     |
| [九五變奏](../Page/九五變奏.md "wikilink")                                            |
| 九五變奏                                                                          |
| 九五變奏                                                                          |
| **2015年**                                                                     |
|                                                                               |
| **2016年**                                                                     |
|                                                                               |

| **各台冠軍歌總數** |
| ----------- |
| 903         |
| **2**       |

  - 仍在榜上（\*）
  - 兩星期冠軍（**(1)**）

## 電視劇（[無綫電視](../Page/無綫電視.md "wikilink")）

  - 《[CATWALK俏佳人](../Page/CATWALK俏佳人.md "wikilink")》（1994）飾 陳國強（Sam）

## 電視劇（[香港電台](../Page/香港電台.md "wikilink")）

  - 《唱談普通話-夢中情人》（1994）飾 文章

## 電影

  - 《海根》（1995）客串

## 注釋

## 参考文献

## 外部連結

  -
  -
[](../Category/动态列表.md "wikilink")
[Category:香港男歌手](../Category/香港男歌手.md "wikilink")
[G](../Category/李姓.md "wikilink")
[Category:LGBT歌手](../Category/LGBT歌手.md "wikilink")
[Category:香港男同性戀者](../Category/香港男同性戀者.md "wikilink")

1.  [調景嶺中學〈1978年畢業同學錄〉，1978年](https://sites.google.com/site/tiukenglengmiddleschool/diao-jing-ling-zhong-xue-jian-shi/1978nian-bi-ye-tong-xue-lu)
2.
3.  ，全場爆滿，大鑊好評 [1](http://timable.com/zh-hk/blog/931716)
4.  [2015-2016微笑員工面試](https://www.facebook.com/mysteryshoppers/posts/10153943246106665)，評審主席：著名歌星李國祥先生；評審委員：愛笑愉加主席余狄夫先生，理工大學高級講師Billy
    Chow小姐；神秘顧客服務協會主席黃兆良先生。
5.  [《豪語直擊》哭訴出櫃 52歲李國祥：我partner係男人](http://hk.apple.nextmedia.com/nextplus/%E8%B1%AA%E8%AA%9E%E9%8C%84/article/20161124/2_454559_0/%E8%B1%AA%E8%AA%9E%E7%9B%B4%E6%93%8A-%E5%93%AD%E8%A8%B4%E5%87%BA%E6%AB%83-52%E6%AD%B2%E6%9D%8E%E5%9C%8B%E7%A5%A5-%E6%88%91partner%E4%BF%82%E7%94%B7%E4%BA%BA)
6.  [2](https://www.hk01.com/%E9%9F%B3%E6%A8%82/81993/%E6%9D%8E%E5%9C%8B%E7%A5%A5%E6%BC%94%E7%B9%B9-%E9%A4%98%E6%83%85%E6%9C%AA%E4%BA%86-%E9%9B%B6%E7%91%95%E7%96%B5-%E5%8B%BE%E8%B5%B7%E9%82%A3%E4%BA%9B%E5%B9%B4%E7%9A%84%E9%9B%86%E9%AB%94%E5%9B%9E%E6%86%B6)