[Weissmuller.jpg](https://zh.wikipedia.org/wiki/File:Weissmuller.jpg "fig:Weissmuller.jpg")


**约翰尼·维斯穆勒**（，），[美国著名](../Page/美国.md "wikilink")[游泳运动员和影星](../Page/游泳.md "wikilink")，曾获得6枚[奥运会奖牌](../Page/奥运会.md "wikilink")（包括5枚金牌），67次打破游泳[世界纪录](../Page/世界纪录.md "wikilink")。他曾在12部[电影中出演](../Page/电影.md "wikilink")[人猿泰山](../Page/人猿泰山.md "wikilink")，被誉为史上最成功的泰山扮演者。他首创的[泰山吼](../Page/泰山吼.md "wikilink")（Tarzan
Yell）已经成为泰山的标志。

维斯穆勒出生于[奥匈帝国](../Page/奥匈帝国.md "wikilink")[弗萊多夫](../Page/蒂米什瓦拉.md "wikilink")（現[羅馬尼亞](../Page/羅馬尼亞.md "wikilink")），原名**亞諾什·维斯穆勒**（János
Weissmüller），具体地点有两种不同的说法，分别是在今日的[罗马尼亚和](../Page/罗马尼亚.md "wikilink")[塞尔维亚境内](../Page/塞尔维亚.md "wikilink")。其父是[天主教教徒](../Page/天主教.md "wikilink")，其母是[犹太人](../Page/犹太人.md "wikilink")，均说[德语](../Page/德语.md "wikilink")。1905年1月26日，维斯穆勒一家移民[美国](../Page/美国.md "wikilink")，根据[纽约市](../Page/纽约市.md "wikilink")[埃利斯岛的纪录](../Page/埃利斯岛.md "wikilink")，其一家都被列为[德国裔](../Page/德国.md "wikilink")[匈牙利人](../Page/匈牙利.md "wikilink")。

## 参考资料

## 扩展阅读

  -
  -
## 外部链接

  - [纽约时报2007年2月17日的报道，称维斯穆勒生于塞尔维亚](http://www.nytimes.com/2007/02/17/world/europe/17briefs-tarzanmonument.html?ex=1172725200&en=a945f654fa0398cc&ei=5070)

  - [官方网站](http://www.johnnyweissmuller.ro)

  -
  - [国立美国历史博物馆档案中关于维斯穆勒的记载](https://web.archive.org/web/20070904093904/http://americanhistory.si.edu/archives/d9443f.htm#weiss)

  - [埃利斯岛档案：载有维斯穆勒姓名的旅客名单](http://www.ellisisland.org/search/shipManifest.asp?MID=01465013330011512576&LNM=WEISSMULLER&PLNM=WEISSMULLER&first_kind=1&last_kind=0&RF=46&PID=102352080350)

[Category:20世纪美国男演员](../Category/20世纪美国男演员.md "wikilink")
[Category:美國電影男演員](../Category/美國電影男演員.md "wikilink")
[Category:美國電視男演員](../Category/美國電視男演員.md "wikilink")
[Category:德國裔美國人](../Category/德國裔美國人.md "wikilink")
[Category:移民美國的奧匈帝國人](../Category/移民美國的奧匈帝國人.md "wikilink")
[Category:前游泳世界纪录保持者](../Category/前游泳世界纪录保持者.md "wikilink")
[Category:國際游泳名人堂成員](../Category/國際游泳名人堂成員.md "wikilink")
[Category:美國奧林匹克運動會金牌得主](../Category/美國奧林匹克運動會金牌得主.md "wikilink")
[Category:美國奧林匹克運動會銅牌得主](../Category/美國奧林匹克運動會銅牌得主.md "wikilink")
[Category:1924年夏季奧林匹克運動會獎牌得主](../Category/1924年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:1928年夏季奧林匹克運動會獎牌得主](../Category/1928年夏季奧林匹克運動會獎牌得主.md "wikilink")
[Category:奧林匹克運動會水球獎牌得主](../Category/奧林匹克運動會水球獎牌得主.md "wikilink")
[Category:美國奧運水球運動員](../Category/美國奧運水球運動員.md "wikilink")