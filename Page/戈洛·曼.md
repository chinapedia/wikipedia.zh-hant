[Golo-mann-1978.jpg](https://zh.wikipedia.org/wiki/File:Golo-mann-1978.jpg "fig:Golo-mann-1978.jpg")
**戈洛·曼**（**Golo Mann**，），又译**葛楼·曼**，全名为安格路斯·哥特佛莱德·托马斯·曼（Angelus
Gottfried Thomas
Mann），生于[慕尼黑](../Page/慕尼黑.md "wikilink")，逝于[勒沃库森](../Page/勒沃库森.md "wikilink")。是德国[历史学家](../Page/历史学家.md "wikilink")、[作家和](../Page/作家.md "wikilink")[哲学家](../Page/哲学家.md "wikilink")。

戈洛·曼是著名德國作家[托马斯·曼](../Page/托马斯·曼.md "wikilink")（Thomas
Mann）与卡提亚的第三个孩子。从日记中看来，戈洛·曼并不得父母宠爱。他上有姐姐埃里卡·曼和哥哥[克劳斯·曼](../Page/克劳斯·曼.md "wikilink")（Klaus
Mann），下有妹妹莫尼卡、伊莉莎白和弟弟米歇尔。戈洛·曼是[同性恋者](../Page/同性恋.md "wikilink")。

1927年至1932年间，戈洛·曼在慕尼黑、[柏林和](../Page/柏林.md "wikilink")[海德堡修读](../Page/海德堡.md "wikilink")[哲学和](../Page/哲学.md "wikilink")[历史](../Page/历史.md "wikilink")，在[卡尔·雅斯贝斯](../Page/卡尔·雅斯贝斯.md "wikilink")（Karl
Jaspers）指导下取得[博士学位](../Page/博士.md "wikilink")。1933年，他移居[瑞士](../Page/瑞士.md "wikilink")，后到[法国](../Page/法国.md "wikilink")，先后在[圣克鲁](../Page/圣克鲁.md "wikilink")、[雷恩任德国历史讲师](../Page/雷恩.md "wikilink")，1937年到1940年间在[苏黎世从事编辑工作](../Page/苏黎世.md "wikilink")。

1940年，他与叔父[亨利希·曼](../Page/亨利希·曼.md "wikilink")（Heinrich
Mann）一起逃难到[西班牙](../Page/西班牙.md "wikilink")，再转避[美国](../Page/美国.md "wikilink")。他在美国若干大学任教，1942年获历史学教授职位，1958年离任。同年，他著成《19世纪到20世纪的德国历史》（*Deutsche
Geschichte des 19. und 20. Jahrhunderts*），这部著作至今仍是同题研究中的标志性作品。

1958年，戈洛·曼回到[欧洲](../Page/欧洲.md "wikilink")，在[斯图加特大学讲授](../Page/斯图加特大学.md "wikilink")[政治史](../Page/政治史.md "wikilink")。在大学里授课、当个规规矩矩的教授的心愿，却毁于[马克斯·霍克海默](../Page/马克斯·霍克海默.md "wikilink")（Max
Horkheimer）和[特奥多尔·W·阿多诺](../Page/特奥多尔·W·阿多诺.md "wikilink")（Theodor W.
Adorno）之手，他们给[威斯巴登的文化部写信](../Page/威斯巴登.md "wikilink")，向公众揭露戈洛·曼的同性恋者身份。

1961年，戈洛·曼开始写作十卷本[世界史](../Page/世界史.md "wikilink")，1964年完笔。1965年，他获颁[曼海姆的](../Page/曼海姆.md "wikilink")[席勒奖](../Page/席勒.md "wikilink")，1968年获[毕希纳奖](../Page/毕希纳.md "wikilink")。1971年，戈洛·曼写就他最著名的作品《[华伦斯坦](../Page/华伦斯坦.md "wikilink")（Albrecht
Eusebius Wenzel von Wallenstein）传》，这部史传巨作文笔如诗，也极具文学价值。

## 作品

  - 1947 *"Friedrich von Gentz."*
  - 1958 *"Deutsche Geschichte des 19. und 20. Jahrhunderts."*
  - 1964 *"Wilhelm II"*
  - 1970 *"Von Weimar nach Bonn. Fünfzig Jahre deutsche Republik."*
  - 1971'' "Wallenstein. Sein Leben erzählt von Golo Mann"''
  - 1986 *"Erinnerungen und Gedanken. Eine Jugend in Deutschland."*
  - 1989 *"Wir alle sind, was wir gelesen".*
  - 1992 *"Wissen und Trauer"*

## 褒奖

  - 1965 席勒奖　[曼海姆](../Page/曼海姆.md "wikilink")
  - 1968 毕希纳奖
  - 1972 联邦贡献大十字奖章、德国[共济会文学奖](../Page/共济会.md "wikilink")
  - 1973 法国南特大学名誉博士
  - 1977 席勒纪念奖
  - 1985
    [歌德奖](../Page/歌德.md "wikilink")　美茵河畔的[法兰克福](../Page/法兰克福.md "wikilink")
  - 1987
    [博登湖文学奖](../Page/博登湖.md "wikilink")、[英格兰](../Page/英格兰.md "wikilink")[巴斯大学名誉博士](../Page/巴斯.md "wikilink")

## 文献

  - [Bücher Wiki: Golo
    Mann（德文）](http://www.wikiservice.at/buecher/wiki.cgi?GoloMann)

[分類:20世紀哲學家](../Page/分類:20世紀哲學家.md "wikilink")
[分类:德國作家](../Page/分类:德國作家.md "wikilink")
[分类:德語作家](../Page/分类:德語作家.md "wikilink")
[分類:男同性戀作家](../Page/分類:男同性戀作家.md "wikilink")
[分类:德国历史学家](../Page/分类:德国历史学家.md "wikilink")
[分类:德国哲学家](../Page/分类:德国哲学家.md "wikilink")
[分類:海德堡大學校友](../Page/分類:海德堡大學校友.md "wikilink")
[分類:美國第二次世界大戰軍事人物](../Page/分類:美國第二次世界大戰軍事人物.md "wikilink")
[分類:美國LGBT人物](../Page/分類:美國LGBT人物.md "wikilink")
[分類:德國裔美國人](../Page/分類:德國裔美國人.md "wikilink")

[Category:德國散文家](../Category/德國散文家.md "wikilink")
[Category:猶太作家](../Category/猶太作家.md "wikilink")
[Category:猶太歷史學家](../Category/猶太歷史學家.md "wikilink")
[Category:斯圖加特大學教師](../Category/斯圖加特大學教師.md "wikilink")
[Category:柏林洪堡大學校友](../Category/柏林洪堡大學校友.md "wikilink")
[Category:慕尼黑大學校友](../Category/慕尼黑大學校友.md "wikilink")
[Category:美國陸軍士兵](../Category/美國陸軍士兵.md "wikilink")
[Category:德國LGBT人物](../Category/德國LGBT人物.md "wikilink")
[Category:德國猶太人](../Category/德國猶太人.md "wikilink")
[Category:慕尼黑人](../Category/慕尼黑人.md "wikilink")
[Category:德国难民](../Category/德国难民.md "wikilink")