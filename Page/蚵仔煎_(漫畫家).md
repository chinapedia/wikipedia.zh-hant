**蚵仔煎**\[1\]，是一個[日本](../Page/日本.md "wikilink")[漫畫家二人組的共用筆名](../Page/漫畫家.md "wikilink")。成員包括編劇[嶋田隆司和作畫](../Page/嶋田隆司.md "wikilink")[中井義則](../Page/中井義則.md "wikilink")。

他們受歡迎的作品有[金肉人](../Page/金肉人.md "wikilink")、[金肉人二世](../Page/金肉人二世.md "wikilink")、[鬥將拉麵男](../Page/鬥將拉麵男.md "wikilink")。

## 成員

### 嶋田隆司

（ - しまだ たかし - Shimada Takashi）

編劇。[大阪市](../Page/大阪市.md "wikilink")[西淀川區出生](../Page/西淀川區.md "wikilink")，1960年10月28日誕生。私立初芝高中畢業。

兩人參加[電影的試映會等活動](../Page/電影.md "wikilink")，接受[媒體的訪問等的露面](../Page/媒體.md "wikilink")，以嶋田隆司比較多。即使在單行本的作者近影方面，也是多半以使用嶋田的休閒[照片居多](../Page/照片.md "wikilink")。

少年時代很調皮，在[幼稚園時是個常會作出突然親吻](../Page/幼稚園.md "wikilink")[女孩子等行為的](../Page/女孩子.md "wikilink")[小孩](../Page/小孩.md "wikilink")。最喜歡的[電視節目是](../Page/電視節目.md "wikilink")[電視劇](../Page/電視劇.md "wikilink")「PLAY
GIRL」。

經常看[漫畫](../Page/漫畫.md "wikilink")，即使自己也畫。「金肉人」是從嶋田認識中井之前，在筆記本內就常畫的東西。

### 中井義則

（ - なかい よしのり - Nakai Yoshinori）

作畫。大阪市[西成區出生](../Page/西成區.md "wikilink")，1961年1月11日誕生。私立初芝高中畢業。

現在都把[錢花費在](../Page/錢.md "wikilink")[眼鏡上](../Page/眼鏡.md "wikilink")。不愛出門。前[AV女優](../Page/AV女優.md "wikilink")是其喜愛的類型。

小學入學前是熱衷於[棒球的少年](../Page/棒球.md "wikilink")，將來的夢想是[職業棒球選手](../Page/職業棒球.md "wikilink")。

喜歡[繪畫](../Page/繪畫.md "wikilink")，然而據說在還未認識嶋田之前並沒有看漫畫的習慣。

## 從相遇到首次出道

### [小學](../Page/小學.md "wikilink")

小學4年級第3學期時（1971年），在嶋田所就讀的大阪市立住之江小學，中井轉學進入。

二人班級不同，但是住在相同的住宅區，上下學搭[公車時](../Page/公車.md "wikilink")，在常偶然同乘的情況下認識。

小學5年級時，在中井去嶋田的家裡玩時，因喜歡上嶋田所畫的「金肉人」而意氣相投。

### [初中](../Page/初中.md "wikilink")

兩人共同就讀大阪市立南稜初中，正式地開始漫畫的合作。

最早所畫的作品是動作漫畫「野獸之牙（）」，從此開始挑戰棒球、空手、純愛等各式各樣的種類。
在初中2年級時，首次以大學交換筆記本的方式，雙方互相輪流繪畫的「拉麵店的東洋（）」投稿近鐵漫畫賞得獎。當時的筆名為元山隆義。

### [高中](../Page/高中.md "wikilink")

兩人也一同就讀初芝高中，繼續以投稿成為漫畫家為目標。編劇和作畫分別由嶋田和中井各自負責。

16歲時候，同時在赤塚賞投稿「是銅鑼喔（）」、和在手塚賞投稿「長毛象（）」（都是職業[摔角漫畫](../Page/摔角.md "wikilink")）雙雙落選。此時卻被[集英社的](../Page/集英社.md "wikilink")[編輯給看上](../Page/編輯.md "wikilink")。

### 正式出道

1978年，用「金肉人」準入選第9屆的赤塚獎，這個短篇作品被《[週刊少年Jump](../Page/週刊少年Jump.md "wikilink")》1979年2號（1978年12月）刊登初次登場。

即使在編輯部內被評論為幼稚的作品，評價並不好。但是當時為[總編輯](../Page/總編輯.md "wikilink")[西村繁男](../Page/西村繁男.md "wikilink")，看上了有資質作為針對低[年齡的漫畫](../Page/年齡.md "wikilink")，前往大阪說服雙方的[父母](../Page/父母.md "wikilink")，等到畢業之後安排兩人的職業漫畫家之路。

這個時候兩人原已打算另尋其他工作，但據說西村向兩人承諾會照顧他們，並還提供了位於[東京的](../Page/東京.md "wikilink")[公寓](../Page/公寓.md "wikilink")。隔年1979年5月，開始連載「金肉人」。

## 作品

### [金肉人](../Page/金肉人.md "wikilink")

（ - Kinnikuman）（Muscleman）

  - [週刊少年Jump](../Page/週刊少年Jump.md "wikilink")（[集英社](../Page/集英社.md "wikilink")）1979年22号（同年5月）
    - 1987年21号（同年4月）

### [鬥將拉麵男](../Page/鬥將拉麵男.md "wikilink")

（ - Tatakae\!\! Ramenman）（Fight\!\! Ramenman）

  - Fresh Jump（集英社）創刊号（1982年6月） - 1989年1月号（1988年12月）

### [幽靈小和尚](../Page/幽靈小和尚.md "wikilink")

（ - Yūrei Kozō ga Yattekita\!）（Here Comes Phantom Kid\!）

  - 週刊少年Jump（集英社） 1987年34号（同年8月） - 1988年24号（同年5月）

### [鬥將三太夫](../Page/鬥將三太夫.md "wikilink")

（ - Scrap Sandayū）

  - 週刊少年Jump（集英社） 1989年24号（同年5月） - 同年40号（同年8月）

### [拳撃手Mamoru](../Page/拳撃手Mamoru.md "wikilink")

（ - Kikku Bokusā Mamoru）（Kick Boxer Mamoru）

  - 週刊少年Jump（集英社） 1990年33号（同年7月） - 1991年13号（同年2月）

### [得分鬥士K](../Page/得分鬥士K.md "wikilink")

（ - Tōtaru Faitā Kao）（Total Fighter Kao）

  - DELUXE BONBON（[講談社](../Page/講談社.md "wikilink")）1993年8月号（同年7月） -
    1995年1月号（1994年12月）

### [獅子心](../Page/獅子心.md "wikilink")

（ - Raionhāto）（Lion Heart）

  - [月刊少年GANGAN](../Page/月刊少年GANGAN.md "wikilink")（[Enix](../Page/Enix.md "wikilink")）1993年9月号（同年8月）
    - 1995年4月号（同年3月）

### [美食小天王](../Page/美食小天王.md "wikilink")

（ - Guruman-kun）

  - [月刊少年Ace](../Page/月刊少年Ace.md "wikilink")（[角川書店](../Page/角川書店.md "wikilink")）1994年11月号（同年10月）
    - 1996年6月号（同年5月）

### [金肉人二世](../Page/金肉人二世.md "wikilink")

（ - Kinnikuman Nisei）（Ultimate Muscle）

  - 週刊Playboy（集英社）1998年19・20合併号（同年4月） - 2007年1月現在連載中

#### 金肉人二世〜All超人大進撃〜

（ - Kinnikuman Nisei \~All Choujin Dai Shingeki\~）（Kinnikuman Second
Generations \~All Choujin Great Attack\~）

  - V-Jump（集英社）2001年7月号（同年5月） - 2007年5月号（同年3月）

## 註釋

<div class="references-small">

<references />

</div>

## 外部連結

  -
  - [Yudetamago's Shimada-sensei talks about
    Kinnikuman: 1](https://web.archive.org/web/20071019115121/http://allabout.co.jp/sports/prowrestling/closeup/CU20060510A/index.htm)
    -
    [2](https://web.archive.org/web/20071118190445/http://allabout.co.jp/sports/prowrestling/closeup/CU20060517A/index.htm)
    -
    [3](https://web.archive.org/web/20070630075931/http://allabout.co.jp/sports/prowrestling/closeup/CU20060525A/index.htm)
    （Japanese）

  - [Yudetamago](http://www.animenewsnetwork.com/encyclopedia/people.php?id=18548)
    at the Anime News Network

[Category:日本漫画家](../Category/日本漫画家.md "wikilink")
[Category:共用筆名](../Category/共用筆名.md "wikilink")
[Category:二人组](../Category/二人组.md "wikilink")

1.  在[日文中原意指](../Page/日文.md "wikilink")[白煮蛋](../Page/白煮蛋.md "wikilink")，但據[東立翻譯](../Page/東立.md "wikilink")《[金肉人II世](../Page/金肉人II世.md "wikilink")》的編輯者曾於[巴哈姆特金肉人綜合討論區](http://forum.gamer.com.tw/A.php?bsn=03652)指出，[台灣將其譯為](../Page/台灣.md "wikilink")「-{[蚵仔煎](../Page/蚵仔煎.md "wikilink")}-」乃為日方的要求。