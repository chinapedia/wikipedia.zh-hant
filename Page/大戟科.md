**大戟科**（[学名](../Page/学名.md "wikilink")：）属[真双子叶植物](../Page/真双子叶植物.md "wikilink")[金虎尾目中的一个大](../Page/金虎尾目.md "wikilink")[科](../Page/科.md "wikilink")，有240[属大约](../Page/属.md "wikilink")6,000[种](../Page/种.md "wikilink")。

## 形态

  - 大部分为单[叶](../Page/叶.md "wikilink")，稀为复叶的，大部分互生，少数对生；有托叶，叶子的基部或叶柄上有时具有腺体。
  - [花单性](../Page/花.md "wikilink")，雌雄同株或异株，总状、圆锥花序，或为杯状聚伞花序，花被两轮，各轮3～5枚，大多数不具花瓣，或全无花被；雄蕊从1个到10个以上不等，分离或合生；子房上位，通常3室；花盘发达，环状、杯状或分裂成腺体状，无花盘的稀少。
  - 果实多为[蒴果](../Page/蒴果.md "wikilink")，成熟时分裂成3瓣，也有不开裂的[浆果或](../Page/浆果.md "wikilink")[核果](../Page/核果.md "wikilink")；[种子有丰富的胚乳](../Page/种子.md "wikilink")。

## 分类

大戟科原來包含非常廣泛的植物，但在重新分類後，很多原屬大戟科的植物被重新分配到[葉下珠科及其他科裡](../Page/葉下珠科.md "wikilink")。

[大戟亚科的种类有杯状的聚伞花序](../Page/大戟亚科.md "wikilink")，外表看像一朵花，实际是由一朵雌花和外围的多朵雄花组成的，单个花没有[花瓣](../Page/花瓣.md "wikilink")，外边包围着杯状的苞片。

## 分布

广泛分布在全世界，主要生长在[热带地区](../Page/热带.md "wikilink")，尤其是[中南半岛和热带](../Page/中南半岛.md "wikilink")[美洲种类最多](../Page/美洲.md "wikilink")，在热带[非洲也有许多种](../Page/非洲.md "wikilink")。大部分为[草本](../Page/草本.md "wikilink")，但生长在[热带地区的种类有许多是](../Page/热带.md "wikilink")[灌木或](../Page/灌木.md "wikilink")[乔木](../Page/乔木.md "wikilink")，也有类似[仙人掌或](../Page/仙人掌.md "wikilink")[石楠的](../Page/石楠.md "wikilink")[肉质型的种类](../Page/多肉植物.md "wikilink")。[中国有](../Page/中国.md "wikilink")61属大约360种，主要生长在[长江以南地区](../Page/长江.md "wikilink")。

## 用途

[巴豆亚科和](../Page/巴豆亚科.md "wikilink")[大戟亚科的种类都含有乳汁](../Page/大戟亚科.md "wikilink")，大戟亚科的乳汁有毒，巴豆亚科的无毒，有许多种类有毒，可以制[农药](../Page/农药.md "wikilink")。重要的经济种包括[橡胶树](../Page/橡胶树.md "wikilink")、[油桐](../Page/油桐.md "wikilink")、[蓖麻](../Page/蓖麻.md "wikilink")、[木薯](../Page/木薯.md "wikilink")、[巴豆以及作为观赏](../Page/巴豆.md "wikilink")[花卉的](../Page/花卉.md "wikilink")[一品红等](../Page/一品红.md "wikilink")。近年來，[桐油樹的油亦被用以製造](../Page/桐油樹.md "wikilink")[生物柴油](../Page/生物柴油.md "wikilink")。

## 图片

<File:Euphorbia> cyparissias quadrat.jpg|大戟科大戟属的欧洲柏大戟（*Euphorbia
cyparissias*） <File:E> baylissii ies.jpg|一种大戟科植物的花序

## 参考文献

  -
  - [Data from GRIN
    Taxonomy](http://www.ars-grin.gov/~sbmljw/cgi-bin/family.pl?433)

  - 在[L. Watson和M.J. Dallwitz
    (1992年)《有花植物分科》](https://web.archive.org/web/20070103200438/http://delta-intkey.com/angio/)中的[大戟科](http://delta-intkey.com/angio/www/Euphorbi.htm)

  -
## 參看

  -
## 外部連結

  - \[<http://libproject.hkbu.edu.hk/was40/search?lang=ch&channelid=1288&searchword=family_latin='Euphorbiaceae>'
    大戟科 Euphorbiaceae\] 藥用植物圖像數據庫 (香港浸會大學中醫藥學院)

[\*](../Category/大戟科.md "wikilink")
[Category:植物科名](../Category/植物科名.md "wikilink")