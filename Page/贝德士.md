[缩略图](https://zh.wikipedia.org/wiki/File:贝德士.jpg "fig:缩略图")
**贝德士**（，），历史学家。

## 生平

1897年，生于[美国](../Page/美国.md "wikilink")[俄亥俄州](../Page/俄亥俄州.md "wikilink")[紐瓦克](../Page/紐瓦克_\(俄亥俄州\).md "wikilink")。1916年，获[哈莱姆学院学士学位](../Page/哈莱姆学院.md "wikilink")。1920年，获[英国](../Page/英国.md "wikilink")[牛津大学硕士学位](../Page/牛津大学.md "wikilink")。

1920年，作为传教士前往[中国](../Page/中国.md "wikilink")[金陵大学任教](../Page/金陵大学.md "wikilink")。1927年，[北伐期間爆發](../Page/國民革命軍北伐.md "wikilink")「[南京事件](../Page/南京事件_\(1927年\).md "wikilink")」，進入[南京城的軍隊遭到中國共產黨與蘇聯策動暴力排外](../Page/南京城.md "wikilink")，對教堂、學校以至外籍人士展開攻擊、焚燒、搶劫的行動。金陵大學副校長[文懷恩](../Page/文懷恩.md "wikilink")（Dr.
John Elias
Williams）遇害身亡，貝德士被一群士兵拘捕。獲釋後，貝德士與妻兒撤往日本，風潮平息後返回金陵大學。1935年，获美国[耶鲁大学博士学位](../Page/耶鲁大学.md "wikilink")。1935年，返回金陵大学历史系。

1937年，[抗战爆发](../Page/抗战.md "wikilink")，贝德士以副校长名义负责留守金大校产。南京沦陷前后，参与发起和组织南京安全区国际委员会（后改为南京国际救济委员会），并担任最后一届主席。1950年，被迫离开中国。

## 书目

  - 《贝德士文献》
  - 《貝德士的名單》

## 链接

  - [Guide to the Miner Searle Bates
    Papers](http://webtext.library.yale.edu/xml2html/divinity.010.nav.html)

[category:南京大屠杀见证人](../Page/category:南京大屠杀见证人.md "wikilink")

[Category:美国历史学家](../Category/美国历史学家.md "wikilink")
[Category:南京安全区国际委员会成员](../Category/南京安全区国际委员会成员.md "wikilink")