《**薔薇之戀**》是一部[日本](../Page/日本.md "wikilink")[漫畫](../Page/漫畫.md "wikilink")，,
作者[吉村明美](../Page/吉村明美.md "wikilink")，[單行本全](../Page/單行本.md "wikilink")16冊，文庫版共9冊。

本漫畫於2003年被[臺灣](../Page/臺灣.md "wikilink")[台視改編成同名](../Page/台視.md "wikilink")[電視劇](../Page/電視劇.md "wikilink")，2003年5月25日在台首播，並在同年11月2日播出精采大結局，導演為[瞿友寧](../Page/瞿友寧.md "wikilink")，並由女子團體[S.H.E](../Page/S.H.E.md "wikilink")、[陸明君](../Page/陸明君.md "wikilink")、[鄭元暢和](../Page/鄭元暢.md "wikilink")[黃志瑋主演](../Page/黃志瑋.md "wikilink")。該劇並獲得2004年度[電視金鐘獎](../Page/電視金鐘獎.md "wikilink")「年度最受歡迎戲劇節目獎」及入圍「金鐘獎戲劇節目獎」。

[台灣有三家媒體播過此劇](../Page/台灣.md "wikilink")，包括：台視（首播）、東風衛視、東森戲劇台

[日本有六家](../Page/日本.md "wikilink")[電視台播放該劇](../Page/電視台.md "wikilink")，分別是[九州朝日放送](../Page/九州朝日放送.md "wikilink")、[神奈川電視台](../Page/神奈川電視台.md "wikilink")、[千葉電視台](../Page/千葉電視台.md "wikilink")、[埼玉電視台](../Page/埼玉電視台.md "wikilink")、[熊本朝日放送](../Page/熊本朝日放送.md "wikilink")、[SUN電視台](../Page/SUN電視台.md "wikilink")(2008年3月26日起)。

[南韓](../Page/南韓.md "wikilink")2008年3月17日於ETN電視台開始放此劇。

## 故事角色

### 韓家

<table>
<tbody>
<tr class="odd">
<td><p><strong>演　員</strong></p></td>
<td><p><strong>角　色</strong></p></td>
<td><p><strong>漫畫原名</strong></p></td>
<td><p><strong>關係/暱稱</strong></p></td>
<td><p><strong>代表植物</strong></p></td>
<td><p><strong>性質</strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/葉童.md" title="wikilink">葉　童</a></strong></p></td>
<td><p><strong>韓　俐</strong></p></td>
<td><p>花井 子</p></td>
<td><p><strong>媽媽</strong><br />
韓芙蓉、韓菫、韓葵之母<br />
鄭百合之養母<br />
李子櫻之好友<br />
鄭野之前妻</p></td>
<td><p><a href="../Page/紅薔薇.md" title="wikilink">紅薔薇</a></p></td>
<td><p>第一女配角</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/陸明君.md" title="wikilink">陸明君</a></strong></p></td>
<td><p><strong>韓芙蓉</strong></p></td>
<td><p>花屋敷 芙蓉</p></td>
<td><p><strong>芙蓉</strong><br />
韓俐之女<br />
韓菫、韓葵之姊<br />
鄭百合無血緣關係之姊<br />
貓吉之女友</p></td>
<td><p><a href="../Page/木芙蓉.md" title="wikilink">木芙蓉</a></p></td>
<td><p>第二女主角</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/黃志瑋.md" title="wikilink">黃志瑋</a></strong></p></td>
<td><p><strong>韓　菫</strong></p></td>
<td><p>花屋敷 菫</p></td>
<td><p><strong>菫</strong><br />
韓俐之子<br />
韓芙蓉之弟<br />
Cherry、韓葵之兄<br />
莊哲芹之未婚夫<br />
鄭百合無血緣關係之兄<br />
曾與鄭百合發生關係，後為其夫</p></td>
<td><p><a href="../Page/菫花.md" title="wikilink">菫花</a></p></td>
<td><p>第一男主角</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/任家萱.md" title="wikilink">任家萱</a>（Selina）</strong></p></td>
<td><p><strong>莊哲芹</strong></p></td>
<td><p>椿沢 </p></td>
<td><p><strong>芹</strong><br />
韓菫之未婚妻<br />
已去世<br />
於第3集登場</p></td>
<td><p><a href="../Page/旱芹.md" title="wikilink">旱芹</a></p></td>
<td><p>第三女主角</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/陳嘉樺.md" title="wikilink">陳嘉樺</a>（Ella）</strong></p></td>
<td><p><strong>鄭百合</strong></p></td>
<td><p>枕野 </p></td>
<td><p><strong>百合</strong><br />
李子櫻、曹智之女<br />
韓俐、鄭野養女<br />
韓芙蓉、韓菫無血緣關係之妹<br />
韓葵同父異母之姊<br />
曾與韓菫發生關係，後為其妻<br />
狄雅蔓之好友</p></td>
<td><p><a href="../Page/百合花.md" title="wikilink">百合花</a></p></td>
<td><p>第一女主角</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/鄭元暢.md" title="wikilink">鄭元暢</a></strong></p></td>
<td><p><strong>韓　葵</strong></p></td>
<td><p>花屋敷 葵</p></td>
<td><p><strong>葵</strong><br />
韓俐、曹智之子<br />
韓芙蓉、韓菫之弟<br />
鄭百合同父異母之弟<br />
暗戀鄭百合、韓菫<br />
曉楓之表哥</p></td>
<td><p><a href="../Page/向日葵.md" title="wikilink">向日葵</a></p></td>
<td><p>第二男主角</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/田馥甄.md" title="wikilink">田馥甄</a>（Hebe）</strong></p></td>
<td><p><strong>曉　楓</strong></p></td>
<td><p>楓</p></td>
<td><p><strong>曉楓</strong><br />
韓俐第四任丈夫的姊姊的女兒<br />
韓俐外甥<br />
韓葵之表妹<br />
暗戀韓葵<br />
鄭百合之好友<br />
於第16集登場<br />
(於第16~18集出現)</p></td>
<td><p><a href="../Page/楓.md" title="wikilink">楓</a></p></td>
<td><p>特別客串</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/任家萱.md" title="wikilink">任家萱</a>（Selina）</strong></p></td>
<td><p><strong>狄雅蔓</strong></p></td>
<td><p>荻原 藤子</p></td>
<td><p><strong>雅蔓</strong><br />
韓俐朋友的女兒<br />
到韓家暫住<br />
長得與莊哲芹一模一樣<br />
鄭百合之好友<br />
木村信貴之前女友<br />
來韓家是為了不想與木村信貴的緋聞曝光<br />
於第8集登場<br />
(於第8~10集出現)</p></td>
<td><p><a href="../Page/毒芹.md" title="wikilink">毒芹</a></p></td>
<td><p>第三女主角</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/比莉.md" title="wikilink">比　莉</a></p></td>
<td><p>婆　婆</p></td>
<td></td>
<td><p>韓家傭人<br />
於第3集登場</p></td>
<td><p>-</p></td>
<td><p>女配角</p></td>
</tr>
</tbody>
</table>

### 莊家

<table>
<tbody>
<tr class="odd">
<td><p><strong>演　員</strong></p></td>
<td><p><strong>角　色</strong></p></td>
<td><p><strong>漫畫原名</strong></p></td>
<td><p><strong>關係/暱稱</strong></p></td>
<td><p><strong>代表植物</strong></p></td>
<td><p><strong>性質</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/應采靈.md" title="wikilink">應采靈</a></p></td>
<td><p>莊媽媽</p></td>
<td></td>
<td><p>莊哲芹之母</p></td>
<td><p>-</p></td>
<td><p>客串</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/任家萱.md" title="wikilink">任家萱</a>（Selina）</strong></p></td>
<td><p><strong>莊哲芹</strong></p></td>
<td><p>椿沢 </p></td>
<td><p><strong>芹</strong><br />
莊家長女<br />
莊媽媽之女<br />
韓菫未婚妻<br />
叶瑞树好友<br />
長得與狄雅蔓一模一樣<br />
已去世<br />
於第3集登場</p></td>
<td><p><a href="../Page/旱芹.md" title="wikilink">旱芹</a></p></td>
<td><p>第三女主角</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/黃志瑋.md" title="wikilink">黃志瑋</a></strong></p></td>
<td><p><strong>韓　菫</strong></p></td>
<td><p>花屋敷 菫</p></td>
<td><p><strong>菫</strong><br />
莊哲芹未婚夫</p></td>
<td><p><a href="../Page/菫花.md" title="wikilink">菫花</a></p></td>
<td><p>第一男主角</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 其他演員

<table>
<tbody>
<tr class="odd">
<td><p><strong>演　員</strong></p></td>
<td><p><strong>角　色</strong></p></td>
<td><p><strong>漫畫原名</strong></p></td>
<td><p><strong>關係/暱稱</strong></p></td>
<td><p><strong>性質</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/李秀.md" title="wikilink">李　秀</a></p></td>
<td><p>奶　奶</p></td>
<td></td>
<td><p><strong>奶奶</strong><br />
鄭野之母<br />
百合奶奶<br />
(於第1集出現)</p></td>
<td><p>客串</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/紅膠囊.md" title="wikilink">紅膠囊</a></p></td>
<td><p>陶貓吉</p></td>
<td><p>桃家 猫吉</p></td>
<td><p><strong>貓吉</strong><br />
韓芙蓉男友<br />
於第5集登場</p></td>
<td><p>男配角</p></td>
</tr>
</tbody>
</table>

### 客串

<table>
<tbody>
<tr class="odd">
<td><p><strong>演　員</strong></p></td>
<td><p><strong>角　色</strong></p></td>
<td><p><strong>漫畫原名</strong></p></td>
<td><p><strong>關係/暱稱</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/愛紗.md" title="wikilink">愛　紗</a></p></td>
<td><p>瑪　麗</p></td>
<td></td>
<td><p><strong>瑪麗</strong><br />
客串第一集<br />
鄭百合之好友</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/曾國城.md" title="wikilink">曾國城</a></p></td>
<td><p>蔡導演</p></td>
<td></td>
<td><p>韓俐之好友</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/柯以敏.md" title="wikilink">柯以敏</a></p></td>
<td><p>李子櫻</p></td>
<td><p>櫻井 萌子</p></td>
<td><p>曹智之妻<br />
鄭百合之母<br />
韓俐之好友</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/貴島功一朗（谷炫淳）.md" title="wikilink">貴島功一朗（谷炫淳）</a></p></td>
<td><p>鄭　野</p></td>
<td><p>枕野 一郎</p></td>
<td><p>韓俐之前夫<br />
鄭百合之養父</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/余發揚.md" title="wikilink">余發揚</a></p></td>
<td><p>出租司機</p></td>
<td></td>
<td><p>客串第十集<br />
雅曼的出租司機</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/郭品超.md" title="wikilink">郭品超</a></p></td>
<td><p>Kevin</p></td>
<td></td>
<td><p>客串第十集<br />
韓俐的小白臉</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/吳克群.md" title="wikilink">吳克群</a></p></td>
<td><p>學　弟</p></td>
<td></td>
<td><p>客串第十集</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/焦志方.md" title="wikilink">焦志方</a></p></td>
<td><p>經紀人</p></td>
<td></td>
<td><p>客串第十集</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/張智成.md" title="wikilink">張智成</a></p></td>
<td><p>石山柏</p></td>
<td></td>
<td><p>客串第十三集<br />
鄭百合之高中同學</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳幼芳.md" title="wikilink">陳幼芳</a></p></td>
<td><p>媒　婆</p></td>
<td></td>
<td><p>客串第十三集</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/羅北安.md" title="wikilink">羅北安</a></p></td>
<td><p>柯　父</p></td>
<td></td>
<td><p>客串第十三集<br />
柯立文之父</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/隆宸翰.md" title="wikilink">隆宸翰</a></p></td>
<td><p>柯立文</p></td>
<td></td>
<td><p>客串第十三集</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳進興.md" title="wikilink">陳進興</a></p></td>
<td><p>房　東</p></td>
<td></td>
<td><p>客串第十四集</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/張永正.md" title="wikilink">張永正</a></p></td>
<td><p>張醫師</p></td>
<td></td>
<td><p>客串第十四集</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳譯賢.md" title="wikilink">陳譯賢</a></p></td>
<td><p>葉瑞樹</p></td>
<td></td>
<td><p>客串第十五集</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/王琄.md" title="wikilink">王　琄</a></p></td>
<td><p>姑　姑</p></td>
<td></td>
<td><p>客串第十五集</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/瞿友寧.md" title="wikilink">瞿友寧</a></p></td>
<td><p>老　師</p></td>
<td></td>
<td><p>客串第十七集<br />
小芙蓉的老師</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/乾德門.md" title="wikilink">乾德門</a></p></td>
<td><p>李爺爺</p></td>
<td></td>
<td><p>客串第二十二集<br />
李子櫻之父<br />
鄭百合之外公</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/劉亮佐.md" title="wikilink">劉亮佐</a></p></td>
<td><p>李子茂</p></td>
<td></td>
<td><p>客串第二十二集<br />
李子櫻之弟<br />
鄭百合之舅舅</p></td>
</tr>
</tbody>
</table>

## 電視原聲帶

**《薔薇之戀》電視原聲帶**由[華研國際音樂製作發行](../Page/華研國際音樂.md "wikilink")，原聲帶中收錄了劇中所有歌曲和部分配樂。[台灣於](../Page/台灣.md "wikilink")2003年6月20日發行，此張原聲帶當年在台灣賣破15萬張。

## 外部連結

  - [《薔薇之戀》台灣台視官方網頁](http://www.ttv.com.tw/drama/2003/RoseLove/)

  - [《薔薇之戀》日本官方網頁](http://4rose.tv/)

  - [《薔薇之戀》韓國官方網頁](https://web.archive.org/web/20080408180040/http://www.ietn.co.kr/2007/roseprogram.htm)

## 作品的變遷

[en:The Rose (TV
series)](../Page/en:The_Rose_\(TV_series\).md "wikilink")

[Category:日本漫畫作品](../Category/日本漫畫作品.md "wikilink")
[Category:戀愛漫畫](../Category/戀愛漫畫.md "wikilink")
[Chiang薔薇之戀](../Category/2003年台灣電視劇集.md "wikilink")
[Chiang薔薇之戀](../Category/中文連續劇.md "wikilink")
[Chiang薔薇之戀](../Category/台灣偶像劇.md "wikilink")
[Chiang薔薇之戀](../Category/日本漫畫改編台灣電視劇.md "wikilink")
[Chiang薔薇之戀](../Category/可米製作電視劇.md "wikilink")
[Category:異父母之兄弟姊妹相愛作品](../Category/異父母之兄弟姊妹相愛作品.md "wikilink")
[Category:亂倫題材電視劇](../Category/亂倫題材電視劇.md "wikilink")