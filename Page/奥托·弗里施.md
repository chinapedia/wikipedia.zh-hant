**奥托·弗里施**（[德语](../Page/德语.md "wikilink")：****，[皇家学会会士](../Page/皇家学会.md "wikilink")\[1\],
），[奥地利](../Page/奥地利.md "wikilink")-[英国](../Page/英国.md "wikilink")[物理学家](../Page/物理学家.md "wikilink")，1940年与[鲁道夫·佩尔斯](../Page/鲁道夫·佩尔斯.md "wikilink")\[2\]合作设计出[原子弹爆炸的理论架构](../Page/原子弹.md "wikilink")\[3\]。

[Frisch,Otto_1963_Kopenhagen.JPG](https://zh.wikipedia.org/wiki/File:Frisch,Otto_1963_Kopenhagen.JPG "fig:Frisch,Otto_1963_Kopenhagen.JPG")

## 参考文献

[F](../Category/奥地利物理学家.md "wikilink")
[F](../Category/英国物理学家.md "wikilink")
[F](../Category/維也納大學校友.md "wikilink")

1.
2.
3.