是1977年[邵氏電影公司出品](../Page/邵氏電影公司.md "wikilink")，[張徹執導的電影](../Page/張徹.md "wikilink")。改編自[金庸著名](../Page/金庸.md "wikilink")[武俠小說](../Page/武俠小說.md "wikilink")[射鵰英雄傳](../Page/射鵰英雄傳.md "wikilink")。有《[射鵰英雄傳續集](../Page/射鵰英雄傳續集.md "wikilink")》及《[射鵰英雄傳第三集](../Page/射鵰英雄傳第三集.md "wikilink")》兩部續篇。

## 電影內容

劇情以原作故事為主幹，自[郭靖與](../Page/郭靖.md "wikilink")[黃蓉結識至](../Page/黃蓉.md "wikilink")[桃花島求親](../Page/桃花島_\(門派\).md "wikilink")。其間穿插[楊康認賊作父的經過及郭靖與](../Page/楊康.md "wikilink")[歐陽克結下之恩怨等](../Page/歐陽克.md "wikilink")。
詳細內容請參閱《[射鵰英雄傳](../Page/射鵰英雄傳.md "wikilink")》條目。

## 人物角色

| 角色名稱                                     | 演員                                    | 備註                                 |
| ---------------------------------------- | ------------------------------------- | ---------------------------------- |
| [郭靖](../Page/郭靖.md "wikilink")           | [傅聲](../Page/傅聲.md "wikilink")        |                                    |
| [黃蓉](../Page/黃蓉.md "wikilink")           | [恬妞](../Page/恬妞.md "wikilink")        |                                    |
| [黃藥師](../Page/黃藥師.md "wikilink")         | [顧冠忠](../Page/顧冠忠.md "wikilink")      | 東邪                                 |
| [梅超風](../Page/梅超風.md "wikilink")         | [余海倫](../Page/余海倫.md "wikilink")      |                                    |
| [陳玄風](../Page/陳玄風.md "wikilink")         | [王慶良](../Page/王慶良.md "wikilink")      |                                    |
| [陸乘風](../Page/陸乘風.md "wikilink")         | [葉天行](../Page/葉天行.md "wikilink")      |                                    |
| [歐陽鋒](../Page/歐陽鋒.md "wikilink")         | [王龍威](../Page/王龍威.md "wikilink")      | 西毒                                 |
| [歐陽克](../Page/歐陽克.md "wikilink")         | [李修賢](../Page/李修賢.md "wikilink")      |                                    |
| [段智興](../Page/段智興_\(金庸\).md "wikilink")  | [狄龍](../Page/狄龍.md "wikilink")        | 南帝                                 |
| [洪七公](../Page/洪七公.md "wikilink")         | [谷峰](../Page/谷峰.md "wikilink")        | 北丐                                 |
| [周伯通](../Page/周伯通.md "wikilink")         | [郭追](../Page/郭追.md "wikilink")        |                                    |
| [丘處機](../Page/丘處機.md "wikilink")         | [楊爾秋](../Page/楊雄.md "wikilink")       | [全真七子](../Page/全真七子.md "wikilink") |
| [王處一](../Page/王處一.md "wikilink")         | [李少華](../Page/李少華.md "wikilink")      | 全真七子                               |
| [楊康](../Page/楊康.md "wikilink")           | [李藝民](../Page/李藝民.md "wikilink")      |                                    |
| [柯鎮惡](../Page/柯鎮惡.md "wikilink")         | [蔡弘](../Page/蔡弘.md "wikilink")        | [江南七怪](../Page/江南七怪.md "wikilink") |
| [朱聰](../Page/朱聰.md "wikilink")           | [林輝煌](../Page/林輝煌.md "wikilink")      | 江南七怪                               |
| [韓寶駒](../Page/韓寶駒.md "wikilink")         | [羅坤霖](../Page/羅莽.md "wikilink")       | 江南七怪                               |
| [南希仁](../Page/南希仁.md "wikilink")         | [陸劍明](../Page/陸劍明.md "wikilink")      | 江南七怪                               |
| [張阿生](../Page/張阿生.md "wikilink")         | [儲陸峰](../Page/鹿峰.md "wikilink")       | 江南七怪                               |
| [全金發](../Page/全金發.md "wikilink")         | [趙中興](../Page/趙中興.md "wikilink")      | 江南七怪                               |
| [韓小瑩](../Page/韓小瑩.md "wikilink")         | [周潔](../Page/周潔.md "wikilink")        | 江南七怪                               |
| [郭嘯天](../Page/郭嘯天.md "wikilink")         | [唐炎燦](../Page/唐炎燦.md "wikilink")      |                                    |
| [李萍](../Page/李萍_\(射鵰英雄傳\).md "wikilink") | [祝菁](../Page/祝菁.md "wikilink")        |                                    |
| [楊鐵心](../Page/楊鐵心.md "wikilink")         | [凃吉龍](../Page/狄威.md "wikilink")       |                                    |
| [包惜弱](../Page/包惜弱.md "wikilink")         | [劉慧玲](../Page/劉慧玲.md "wikilink")      |                                    |
| [穆念慈](../Page/穆念慈.md "wikilink")         | [惠英紅](../Page/惠英紅.md "wikilink")      |                                    |
| [完顏洪烈](../Page/完顏洪烈.md "wikilink")       | [于榮](../Page/于榮.md "wikilink")        |                                    |
| [梁子翁](../Page/梁子翁.md "wikilink")         | [樊梅生](../Page/樊梅生.md "wikilink")      |                                    |
| [靈智上人](../Page/靈智上人.md "wikilink")       | [詹森](../Page/詹森_\(演員\).md "wikilink") |                                    |
| [沙通天](../Page/沙通天.md "wikilink")         | [孫樹培](../Page/孫樹培.md "wikilink")      |                                    |
|                                          | [陳思佳](../Page/陳思佳.md "wikilink")      | 白駝山弟子                              |
|                                          | [曾楚霖](../Page/曾楚霖.md "wikilink")      | 店小二                                |
|                                          | [王憾塵](../Page/王憾塵.md "wikilink")      | 店小二                                |
|                                          | [張作舟](../Page/張作舟.md "wikilink")      | 掌櫃                                 |
|                                          | [馮敬文](../Page/馮敬文.md "wikilink")      |                                    |
|                                          | [王力](../Page/王力.md "wikilink")        | 趙王府侍衛                              |
|                                          | [周堅平](../Page/周堅平.md "wikilink")      | 趙王府侍衛                              |
|                                          | [韋白](../Page/韋白.md "wikilink")        | 歸雲莊弟子                              |
|                                          | [江生](../Page/江生.md "wikilink")        | 桃花島啞僕                              |
|                                          |                                       |                                    |

## 相關條目

  - [金庸](../Page/金庸.md "wikilink")
  - [射鵰英雄傳](../Page/射鵰英雄傳.md "wikilink")

## 備註

<references/>

## 外部連結

  - {{@movies|fbhk50078249|射鵰英雄傳}}

  -
  -
  -
  -
  -
  -
[Category:邵氏電影](../Category/邵氏電影.md "wikilink")
[7](../Category/1970年代香港電影作品.md "wikilink")
[Category:1970年代動作片](../Category/1970年代動作片.md "wikilink")
[Category:射鵰英雄傳改編影片](../Category/射鵰英雄傳改編影片.md "wikilink")
[Category:香港動作片](../Category/香港動作片.md "wikilink")
[Category:张彻电影](../Category/张彻电影.md "wikilink")