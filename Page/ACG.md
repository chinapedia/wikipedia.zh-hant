[Last_Wikipe-tan_by_Kasuga.png](https://zh.wikipedia.org/wiki/File:Last_Wikipe-tan_by_Kasuga.png "fig:Last_Wikipe-tan_by_Kasuga.png")

**ACG**即[日本動畫](../Page/日本動畫.md "wikilink")（**A**nime）、[漫畫](../Page/日本漫畫.md "wikilink")（**C**omics）與[遊戲](../Page/電子遊戲.md "wikilink")（**G**ames）的英文[首字母縮略字](../Page/首字母縮略字.md "wikilink")。该词汇一般不翻译为中文，需要时可能会被译为「**動漫遊戲**」、「**二次元**」或「**動漫遊**」等。

该词汇来自于[華人地區的](../Page/華人地區.md "wikilink")[次文化](../Page/次文化.md "wikilink")，多指來自[日本的](../Page/日本.md "wikilink")[动漫及](../Page/动漫.md "wikilink")[電子遊戲作品](../Page/電子遊戲.md "wikilink")，包括传统日式[电子角色扮演游戏以及](../Page/电子角色扮演游戏.md "wikilink")[美少女游戏](../Page/美少女游戏.md "wikilink")。并随着「[二次元](../Page/二次元.md "wikilink")」这一词汇的延伸性，ACG这些词所容纳的东西正渐渐成为不再是单纯指代日本产品的词汇。

## 源流與發展

1995年，[台灣的](../Page/台灣.md "wikilink")[動漫愛好者AIplus在](../Page/動漫.md "wikilink")[國立中山大學山抹微雲](../Page/國立中山大學.md "wikilink")[BBS站開設新版面时](../Page/BBS.md "wikilink")，使用了「ACG_Review板」作為版面名稱，以代指[動畫](../Page/動畫.md "wikilink")、[漫畫](../Page/漫畫.md "wikilink")、[遊戲](../Page/遊戲.md "wikilink")，这是「ACG」一詞首次出现\[1\]。後来在[傻呼嚕同盟推廣之下](../Page/傻呼嚕同盟.md "wikilink")，三個字母的順序漸漸固定下來，並逐渐流傳至[中國大陸](../Page/中國大陸.md "wikilink")、[香港等華人社會](../Page/香港.md "wikilink")。\[2\]

在[輕小說改編的動畫](../Page/輕小說.md "wikilink")、漫畫、遊戲越來越多之際，又衍生出「**ACGN**」这一新词汇，其中的字母N代表小說（**N**ovels）。

## 使用區域

[日本不使用源自英文ACG這個詞](../Page/日本.md "wikilink")，較常使用的類似概念为**MAG**（）一词，即[日本漫畫](../Page/日本漫畫.md "wikilink")（Manga）、[日本動畫](../Page/日本動畫.md "wikilink")（Anime）和[游戏](../Page/游戏.md "wikilink")（Games）的縮寫。日本的爱好者间常用**[二次元](../Page/二次元.md "wikilink")**来代指一系列的动漫文化，包括[轻小说](../Page/轻小说.md "wikilink")、[手办等](../Page/手办.md "wikilink")，而**[御宅文化](../Page/御宅.md "wikilink")**（）一詞則泛指相關領域[次文化](../Page/次文化.md "wikilink")\[3\]，在英語社會裡一般還會使用動畫和漫畫（Anime
and Manga）一詞，但其中沒有包括遊戲（Game）。

## 相關條目

  - [酷日本](../Page/酷日本.md "wikilink")
  - [萌文化](../Page/萌文化.md "wikilink")

## 参考文献

[Category:动漫](../Category/动漫.md "wikilink")
[Category:動畫](../Category/動畫.md "wikilink")
[Category:漫畫](../Category/漫畫.md "wikilink")

1.
2.
3.