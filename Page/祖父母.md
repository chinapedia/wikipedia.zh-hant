**祖父母**是指[父親的](../Page/父親.md "wikilink")[父母親](../Page/父母親.md "wikilink")，又称“爷爷”、“嫲嫲”。[母親的父母稱為](../Page/母親.md "wikilink")**外祖父母**，在中国南方称为**外公**、**外婆**，在中国北方则称为**姥姥**（或姥娘）、**姥爷**。自己則分別是祖父母和外祖父母的[孫兒或](../Page/孫兒.md "wikilink")[孫女](../Page/孫女.md "wikilink")、[外孫或](../Page/外孫.md "wikilink")[外孫女](../Page/外孫女.md "wikilink")。

不同地方及方言對祖父母和外祖父母有不同的[稱謂](../Page/稱謂.md "wikilink")，但現代中文正式稱呼是祖父母和外祖父母。吴语区称祖父母为**爹爹**、**姆奶**，称外祖父母为**外爺**、**外婆**。例如[中國北方官話大多稱祖父母為](../Page/中國.md "wikilink")**爺爺**、**奶奶**（也有稱爷爷为**大大**\[1\]，或称奶奶為**嫲嫲**的，如山東部份地區），稱外祖父母為**姥爺**、**姥姥**/**姥娘**。[四川多數地方称祖父母为](../Page/四川话.md "wikilink")**爺爺**、**奶奶**，称外祖父母為**家公**、**家婆**等。[客家話中稱祖父為](../Page/客家話.md "wikilink")**阿公**(a-gong)，祖母則為**阿婆**(a-po)；外祖父為**姐公**(d͡za-gong)，祖母則為**姐婆**(d͡za-po)。
[粵語稱祖父母為](../Page/粵語.md "wikilink")**爺爺**、**嫲嫲**或**阿爺**、**阿嫲**，稱外祖父母為**公公**、**婆婆**或**阿公**、**阿婆**。[閩南語不論是祖父母還是外祖父母都稱](../Page/閩南語.md "wikilink")**阿公**(a-gong)、**阿嬤**（**阿媽**/a-ma）（外祖父母也可較專一地稱**外公**(ggua-gong)、**外嬤**(ggua-ma)），[湘語祖父母為爹爹](../Page/湘语.md "wikilink")（dia-dia），娭毑，外祖父母為外公外婆。

由于社会发展的变迁，某些地方出现了父母双方的父母都称“爷爷”、“奶奶”的现象。

## 稱謂

同樣的稱謂在不同方言中所指稱的對象可能不同。例如公公、婆婆在中國北方話是[兒媳稱丈夫的](../Page/兒媳.md "wikilink")[父母](../Page/父母.md "wikilink")，但在粵語卻是指母親的父母。此外，對祖父母和外祖父母的口語稱呼也有用來稱呼與自己無親屬關係的[老人家](../Page/老人家.md "wikilink")，是[擬親屬稱謂](../Page/擬親屬.md "wikilink")。

<table>
<caption>各地方及母語相關稱謂</caption>
<thead>
<tr class="header">
<th></th>
<th><p>祖父<br />
祖母</p></th>
<th><p>外祖父<br />
外祖母</p></th>
<th><p>丈夫父親<br />
丈夫母親</p></th>
<th><p>無親屬關係的<a href="../Page/長者.md" title="wikilink">長者</a></p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="../Page/中國北方.md" title="wikilink">中國北方</a> (東部)</p></td>
<td><p>爺爺<br />
奶奶（個別地區稱“嫲嫲”）</p></td>
<td><p>姥爺<br />
姥姥/姥娘</p></td>
<td><p>公公<br />
婆婆</p></td>
<td><p>老爷爷/老大爷/大爺<br />
老奶奶/大娘</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/吳語.md" title="wikilink">吳語</a></p></td>
<td><p>爹爹/阿爺<br />
唔奶/嗯娘/亲娘/阿娘</p></td>
<td><p>外公<br />
外婆</p></td>
<td><p>阿公<br />
阿婆</p></td>
<td><p>?</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/粵語.md" title="wikilink">粵語</a></p></td>
<td><p>爺爺/阿爺<br />
嫲嫲/阿嫲</p></td>
<td><p>公公/阿公<br />
婆婆/阿婆</p></td>
<td><p>老爺/家公<br />
奶奶/家婆（部份地方舊稱「安人」）</p></td>
<td><p>公公/阿公<br />
婆婆/阿婆</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/客家語.md" title="wikilink">客家語</a></p></td>
<td><p>阿公<br />
阿婆/阿嫲</p></td>
<td><p>姐公<br />
姐婆</p></td>
<td><p>家倌<br />
家娘</p></td>
<td><p>阿公<br />
阿婆</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/閩南語.md" title="wikilink">閩南語</a></p></td>
<td><p>阿公/俺公<br />
阿媽/俺媽</p></td>
<td><p>阿公/外公<br />
阿媽/外媽</p></td>
<td><p>大官<br />
大家</p></td>
<td><p>叔公/阿伯<br />
阿婆/阿姆</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/晉語.md" title="wikilink">晉語</a></p></td>
<td><p>爺(爺)<br />
孃(孃)/奶(奶)</p></td>
<td><p>簡(爺/兒)/舅舍爺/舅莊爺/外爺/姥爺<br />
簡婆/板板/舅舍孃/舅莊孃/婆婆/姥孃</p></td>
<td><p>(老)爺爺/公公/阿公/<br />
(老)孃孃/婆婆/阿婆/阿家(母)</p></td>
<td><p>老漢(家/兒)/爺爺<br />
老婆(家/兒)/孃孃/奶奶</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/湖北話.md" title="wikilink">湖北話</a></p></td>
<td><p>爺爺（舊稱“爹爹”）<br />
奶奶（舊稱“太”）</p></td>
<td><p>家公爹爹<br />
家家</p></td>
<td><p>爹爹<br />
婆婆</p></td>
<td><p>爹爹<br />
太</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/四川话.md" title="wikilink">四川话</a></p></td>
<td><p>爺爺—奶奶[2]<br />
阿公—阿婆<br />
公公—婆婆<br />
公—婆</p></td>
<td><p>家公—家家、家婆、家母[3]<br />
外公、外爺—外婆<br />
公公—婆婆<br />
阿公—阿婆<br />
公—婆</p></td>
<td><p>老人公/公公<br />
老人婆/婆婆</p></td>
<td><p>爺爺、老爺爺、老爺、大爺[4]—奶奶、老奶奶<br />
公公—婆婆<br />
阿公—阿婆</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/南京话.md" title="wikilink">南京话</a></p></td>
<td><p>爺爺<br />
奶奶</p></td>
<td><p>公公<br />
婆婆</p></td>
<td><p>老公公<br />
老婆婆</p></td>
<td><p>?</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/湘语.md" title="wikilink">湘語</a></p></td>
<td><p>爹爹<br />
娭毑</p></td>
<td><p>外公<br />
外婆</p></td>
<td><p>家爺<br />
家娘</p></td>
<td><p>爹爹<br />
娭毑</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/閩東語.md" title="wikilink">閩東語</a></p></td>
<td><p>伊公<br />
伊嬤</p></td>
<td><p>外公<br />
外嬤</p></td>
<td><p>老官<br />
臺家</p></td>
<td><p>叔公<br />
伊婆<br />
伊伯<br />
伊姆</p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

<table>
<caption>各地方及方言容易混淆相關稱謂</caption>
<thead>
<tr class="header">
<th></th>
<th><p>北方</p></th>
<th><p>閩南語</p></th>
<th><p>吳語</p></th>
<th><p>客語　</p></th>
<th><p>粵語</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>稱謂「<strong>爹爹</strong>」指的是：</p></td>
<td><p>父親</p></td>
<td><p>-</p></td>
<td><p>父親的父親</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p>稱謂「<strong>阿公</strong>」指的是：</p></td>
<td><p>-</p></td>
<td><p>父親的父親<br />
母親的父親</p></td>
<td><p>丈夫的父親</p></td>
<td><p>父親的父親</p></td>
<td><p>母親的父親</p></td>
</tr>
<tr class="odd">
<td><p>稱謂「<strong>爺爺</strong>」指的是：</p></td>
<td><p>父親的父親</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>父親的父親</p></td>
</tr>
<tr class="even">
<td><p>稱謂「<strong>阿婆</strong>」指的是：</p></td>
<td><p>-</p></td>
<td><p>(一般老嫗皆可稱阿婆)</p></td>
<td><p>丈夫的母親</p></td>
<td><p>父親的母親</p></td>
<td><p>母親的母親</p></td>
</tr>
<tr class="odd">
<td><p>稱謂「<strong>姐公</strong>」指的是：</p></td>
<td><p>丈夫的父親</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>母親的父親</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p>稱謂「<strong>公公</strong>」指的是：</p></td>
<td><p>丈夫的父親</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>母親的父親</p></td>
</tr>
<tr class="odd">
<td><p>稱謂「<strong>姐婆</strong>」指的是：</p></td>
<td><p>丈夫的父親</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>母親的母親</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p>稱謂「<strong>婆婆</strong>」指的是：</p></td>
<td><p>丈夫的母親</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>母親的母親</p></td>
</tr>
<tr class="odd">
<td><p>稱謂「<strong>舅公</strong>」指的是：</p></td>
<td><p>-</p></td>
<td><p>父親的舅舅<br />
母親的舅舅</p></td>
<td><p>父親的舅舅<br />
母親的舅舅</p></td>
<td><p>父親的舅舅<br />
母親的舅舅</p></td>
<td><p>父親的舅舅<br />
母親的舅舅</p></td>
</tr>
<tr class="even">
<td><p>稱謂「<strong>奶奶</strong>」指的是：</p></td>
<td><p>父親的母親</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>-</p></td>
<td><p>丈夫的母親</p></td>
</tr>
</tbody>
</table>

## 祖宗十八代

自己上下各九代，共十八代：

  - 上九代：鼻祖，遠祖，太祖，烈祖，天祖，高祖，曾祖，祖，父\[5\]\[6\]\[7\]

<!-- end list -->

  - 下九代：子，孫，曾孫，玄孫，來孫，晜孫，仍孫，雲孫，耳孫\[8\]\[9\]\[10\]\[11\]

所謂祖宗十八代是指自己上下九代的宗族成員。

上按次序稱謂：

生己者爲父母，

父之父爲祖，

祖父之父爲曾祖，

曾祖之父爲高祖，

高祖之父爲天祖，

天祖之父爲烈祖，

烈祖之父爲太祖，

太祖之父爲遠祖，

遠祖之父爲鼻祖。

即：父、祖、曾、高、天、烈、太、遠、鼻。書中說：因人懷胎，鼻先受形，故鼻祖爲始祖。

下按次序稱謂：

父之子爲子，

子之子爲孫，

孫之子爲曾孫，

曾孫之子爲玄孫，

玄孫之子爲來孫，

來孫之子爲晜（讀kūn；客語：Kun）孫，

晜孫之子爲仍孫，

仍孫之子爲雲孫，

雲孫之子爲耳孫。

即：子、孫、曾、玄、來、晜、仍、雲、耳。書中說：耳孫者，謂祖甚遠，僅耳目聞之也。

## 其他用途

[粵語中的](../Page/粵語.md "wikilink")「阿爺」有其他含義，[港澳](../Page/港澳地區.md "wikilink")[親共人士會稱呼位於北京的](../Page/香港親共人士.md "wikilink")[中華人民共和國政府或](../Page/中華人民共和國政府.md "wikilink")[中共政權為](../Page/中共.md "wikilink")「**阿爺**」。[泛民主派人士以此稱呼用以批評](../Page/泛民主派.md "wikilink")[建制派及](../Page/建制派.md "wikilink")[親共人士](../Page/親共.md "wikilink")，諷喻他們對中共當局無條件言聽計從。

## 用词争议

2018年6月20日，一位家长爆料称，[上海教育出版社出版的二年级第二学期语文课本](../Page/上海教育出版社.md "wikilink")（试用本）中第24课，将《打碗碗花》原著中的“外婆”一词改为“姥姥”，第5课《马鸣加的新书包》一文中也有同样的更改\[12\]。

[上海市教育委员会教研室认为](../Page/上海市教育委员会.md "wikilink")，根据《[现代汉语词典](../Page/现代汉语词典.md "wikilink")》第六版的解释，“姥姥”是[普通话词汇](../Page/普通话.md "wikilink")，“外婆”“外公”属于[方言](../Page/方言.md "wikilink")\[13\]。有媒体查阅了第6版《现代汉语词典》，发现“外婆”一词上确实标注“方言”，而语义为外祖母的“姥姥”一词无此标注\[14\]。

一些网友和媒体对此改动表达异议和批评\[15\]\[16\]\[17\]\[18\]。《打碗碗花》原著作者李天芳则表示，无论“外婆”还是“姥姥”，南方北方都会知道这两个词的含义。李天芳同时指出，上海教育出版社并未就文章使用和修改与她联系，没有做到尊重作者、保障作者的权益\[19\]\[20\]。

6月23日，上海市教委发文，责成市教委教研室会同上海教育出版社迅速整改、向作者和社会各界致歉、与作者沟通，以及将教科书中的“姥姥”恢复回原文所用“外婆”一词\[21\]。

## 相關

  - [祖父母節](../Page/祖父母節.md "wikilink")
  - [母亲节](../Page/母亲节.md "wikilink")
  - [父親節](../Page/父親節.md "wikilink")

## 參考與注釋

[Category:親屬稱謂](../Category/親屬稱謂.md "wikilink")

1.

2.  多數地方叫爺爺—奶奶。爺爺也叫作老爺，有些地方叫嗲嗲。
    有些地方奶奶叫孃孃（似與嬢嬢不同），多數地方稱姨媽、姑媽為嬢嬢，或嬢嬢等同阿姨，用來稱呼父母的同輩中的女子。嬢嬢指父母的的姐妹，有些地方或稱某（排行數字）姑嬢，如大姑嬢、二姑嬢，而最小的嬢嬢也或稱為幺姑嬢（如果父母兩邊都有某排行的嬢嬢，則可以冠以嬢嬢丈夫的姓等方式加以區分，如張二姑嬢、李二姑嬢）。嬢，四川話中為一聲。而普通話中的姑娘（年輕女子），四川話稱為姑女（读作姑女兒，兒化音，女兒連讀，一聲），或也稱姑娘，娘為四聲（非特殊，因為普通話三聲一般對應四川話四聲）。爸爸的兄弟的妻子稱某（排行數字）孃（或為娘，不同於嬢。發音為一聲或四聲），如大孃、二孃、三孃、么孃，或稱大媽、二媽、三媽、幺媽。（也有个别家庭會稱媽媽為大娘，爸爸為大大）

3.  家念佳，部分地方念嘎。外祖父母稱家公、家母的地方曾經很普遍。[顏之推](../Page/顏之推.md "wikilink")：“河北士人皆呼外祖父母為家公家母，江南田裡間亦言之。”

4.  有些地方大爺指爺爺，有些地方稱父親的長兄為大爺(其後的兄弟依次為二爺、三爺...。多數稱大爸、二爸、三爸...。部分地方稱大爹、二爹、三爹...。也有不同稱號系統交叉組合的，常見的如大爺、二爸、三爸...，但應該沒有類似大爸、二爺、三爺...這樣的。

5.  《[爾雅](../Page/爾雅.md "wikilink")‧釋親》

6.  明《[幼學瓊林](../Page/幼學瓊林.md "wikilink")·卷二·祖孫父子類》

7.  清《稱謂錄》

8.
9.
10.
11. 《[大清仁宗睿皇帝實錄](../Page/清實錄.md "wikilink")》：敬念皇父年躋上壽。諸福備膺。五世一堂。即日可見來孫之喜。

12. [上海课本“外婆”改“姥姥”　引发“方言”争议](https://www.zaobao.com.sg/realtime/china/story20180623-869542).联合早报.

13. [出版社改教材引发“外婆”与“姥姥”的南北之争](http://www.bbc.com/zhongwen/simp/chinese-news-44573959).BBC中文.

14. [上海课本让"外婆"变"姥姥"，网友：来一首姥姥的澎湖湾](http://www.infzm.com/content/136875).infzm.

15.
16.
17. [“外婆”改成“姥姥”，编改教材不必这么刻意](http://www.xinhuanet.com/comments/2018-06/22/c_1123018898.htm).新华网.

18. [媒体:课本外婆改成姥姥
    以后唱《姥姥的澎湖湾》?](http://news.sina.com.cn/s/2018-06-21/doc-ihefphqm0071185.shtml).新浪网.

19.
20.

21.