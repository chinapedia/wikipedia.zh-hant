[Popunjavanje_pukotin_propolisom.jpg](https://zh.wikipedia.org/wiki/File:Popunjavanje_pukotin_propolisom.jpg "fig:Popunjavanje_pukotin_propolisom.jpg")
[Propolis_in_beehives.jpg](https://zh.wikipedia.org/wiki/File:Propolis_in_beehives.jpg "fig:Propolis_in_beehives.jpg")
**蜂膠**，是[蜜蜂採集植物的汁液](../Page/蜜蜂.md "wikilink")、[花粉或](../Page/花粉.md "wikilink")[花蜜](../Page/花蜜.md "wikilink")，混合自己分泌的[唾液與](../Page/唾液.md "wikilink")[蜜臘](../Page/蜜臘.md "wikilink")，所形成之膠狀物，用來修補蜂巢，並作為天氣不佳而無法外出覓食狀況下之備份糧食。蜂膠因具有抗菌成份，可以防止黴菌、細菌或病毒入侵，有效保護幼蟲、幼蜂及[蜂后的安全](../Page/蜂后.md "wikilink")。\[1\]

## 醫療用途

在[传统医学中](../Page/传统医学.md "wikilink")，已使用蜂胶上千年之久\[2\]\[3\]。

[美國國立衛生研究院認為蜂胶在治療](../Page/國立衛生研究院_\(美國\).md "wikilink")[唇瘡](../Page/唇瘡.md "wikilink")、[生殖器皰疹及手術後的口部疼痛](../Page/生殖器皰疹.md "wikilink")「可能有效」。蜂胶也用來作止咳糖浆，可以止咳及減緩喉咙发炎\[4\]。目前對於蜂胶在其他症狀上的療效，還沒有足夠的資料得以佐證\[5\]。

## 種類

市售蜂胶根据不同的用途，主要有以下几个类型：

1.  毛胶：直接从蜂箱中取得，未经过加工，含有蜜蜂肢体的木屑，泥沙、麻布纤维等杂质，其形态为不透明的团块状或碎屑状，适于外用或用于鸡猪牛羊饲料强化剂。　
2.  原胶：毛胶经工厂化加工，杂质与[重金属含量符合国际标准](../Page/重金属.md "wikilink")。形态为不规则条块状，适合于做食品、药品、化妆品原料。
    　
3.  蜂胶标准溶液：是一种蜂胶的[乙醇溶液](../Page/乙醇.md "wikilink")，适于外用或用于食品、药品添加剂及食品防腐剂。
4.  蜂胶水溶液：适于稀释后口服，或用于食品、药品、添加剂。 　　
5.  蜂胶油：蜂胶油溶液，适用于美容化妆品，或日常皮肤保养。 　　
6.  蜂胶乾膏：蜂胶提纯物，胶片状，适用于食品、药品、化妆品、并用于工业、农业、畜牧业、水产养殖业。 　　
7.  蜂胶制品：各种含蜂胶的食品、药品、化妆品。
8.  蜜溶蜂胶：适用于稀释后口服，或外用。是由蜂蜜溶蜂胶液，不添加化学添加剂。

## 參考文獻

[Category:蜜蜂](../Category/蜜蜂.md "wikilink")
[Category:材料](../Category/材料.md "wikilink")
[Category:膳食补充品](../Category/膳食补充品.md "wikilink")

1.  [蔡英敏，《蜂膠之探討》，臺北市立松山高級工農職業學校官網](http://aca4.saihs.edu.tw/pps/pdf/journal/no1/2-03.pdf)

2.  Fearnely J. (2001) Bee propolis. Souvenir Press Ltd. London.
3.
4.  Example of Cough Drops: Casa de Apicultor C. 38 No. 220 B x 49 y 51
    Col San Juan. C.P. 97780 Valladolid. Yucatan, Mexico
5.