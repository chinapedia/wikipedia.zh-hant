**Novell
Netware**即**Novell網路作業系統**，該[網路作業系統是](../Page/網路作業系統.md "wikilink")[Novell公司的產品](../Page/Novell.md "wikilink")。在[Windows系統盛行之前](../Page/Windows.md "wikilink")，[个人電腦是運作在Command](../Page/个人電腦.md "wikilink")
Line的環境，此時的網路架構，都是以Novell Netware來建構而成。

目前常用的版本有3.11、3.12和4.10 、V4.11，V5.0等中英文版本，而主流的是NETWARE
5版本，支持所有的重要台式操作系统（DOS,Windows,OS/2,Unix和Macintosh）以及IBM
SAA环境，为需要在多厂商产品环境下进行复杂的网络计算的企事业单位提供了高性能的综合平台。NetWare是具有多任务、多用户的网络操作系统，它的较高版本提供系统容错能力（SFT）。使用开放协议技术（OPT），各种协议的结合使不同类型的工作站可与公共服务器通信。这种技术满足了广大用户在不同种类网络间实现互相通信的需要，实现了各种不同网络的无缝通信，即把各种网络协议紧密地连接起来，可以方便地与各种小型机、中大型机连接通信。NetWare可以不用专用服务器，任何一种PC机均可作为服务器。NetWare服务器对无盘站和游戏的支持较好，常用于教学网和游戏厅。

只有 [VGA](../Page/VGA.md "wikilink") CARD
才能顯示中文，[EGA](../Page/EGA.md "wikilink")、[CGA](../Page/CGA.md "wikilink")、[MGA只能顯示英文](../Page/MGA.md "wikilink")。

[Category:1983年软件](../Category/1983年软件.md "wikilink")
[Category:Netware](../Category/Netware.md "wikilink")
[Category:专有软件](../Category/专有软件.md "wikilink")