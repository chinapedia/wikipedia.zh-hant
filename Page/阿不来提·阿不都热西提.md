**阿不來提·阿不都熱西提**（[維吾爾語](../Page/維吾爾語.md "wikilink")：ئابلەت
ئابدۇرىشىت，[拉丁维文](../Page/拉丁维文.md "wikilink")：Ablet
Abdurishit，），男，[維吾爾族](../Page/維吾爾族.md "wikilink")，[新疆](../Page/新疆.md "wikilink")[伊寧人](../Page/伊寧.md "wikilink")。[中国人民政治协商会议第十届](../Page/中国人民政治协商会议.md "wikilink")、[第十一届全国委员会副主席](../Page/中国人民政治协商会议第十一届全国委员会.md "wikilink")。[中共第十五](../Page/中国共产党第十五届中央委员会委员.md "wikilink")、[十六](../Page/中国共产党第十六届中央委员会委员.md "wikilink")、[十七屆中央委員](../Page/中国共产党第十七届中央委员会委员.md "wikilink")。\[1\]

## 生平

大學學歷，高級工程師。1960年7月加入[中國共產黨](../Page/中國共產黨.md "wikilink")。1965年9月參加工作，[新疆工學院機電系電氣專業畢業](../Page/新疆工學院.md "wikilink")。

1965年至1983年任[新疆維吾爾自治區建築勘察設計院技術員](../Page/新疆維吾爾自治區建築勘察設計院.md "wikilink")、工程師、副院長。1983年至1991年任新疆維吾爾自治區計委副主任、黨組成員、黨組書記。1991年至1993年任[新疆維吾爾自治區人民政府副主席](../Page/新疆維吾爾自治區.md "wikilink")。1993年至1994年任新疆維吾爾自治區黨委副書記、自治區政府代主席。1994年至2003年任新疆維吾爾自治區主席。2003年1月至5月任新疆維吾爾自治區人大常委會主任。2003年3月，在[第十届全国政协一次會議上當選為](../Page/第十届全国政协.md "wikilink")[全國政協副主席](../Page/全國政協副主席.md "wikilink")；并于2008年3月连任。

## 参考文献

## 外部链接

  - [阿不来提·阿不都热西提活动报道集](http://cppcc.people.com.cn/GB/34961/130378/131177/index.html)

{{-}}

[Category:第十届全国政协副主席](../Category/第十届全国政协副主席.md "wikilink")
[Category:第十一届全国政协副主席](../Category/第十一届全国政协副主席.md "wikilink")
[Category:新疆维吾尔自治区人民政府主席](../Category/新疆维吾尔自治区人民政府主席.md "wikilink")
[Category:新疆维吾尔自治区人大常委会主任](../Category/新疆维吾尔自治区人大常委会主任.md "wikilink")
[Category:中国共产党第十五届中央委员会委员](../Category/中国共产党第十五届中央委员会委员.md "wikilink")
[Category:中国共产党第十六届中央委员会委员](../Category/中国共产党第十六届中央委员会委员.md "wikilink")
[Category:中国共产党第十七届中央委员会委员](../Category/中国共产党第十七届中央委员会委员.md "wikilink")
[Category:维吾尔族全国政协副主席](../Category/维吾尔族全国政协副主席.md "wikilink")
[Category:中华人民共和国时期维吾尔族中国共产党党员](../Category/中华人民共和国时期维吾尔族中国共产党党员.md "wikilink")
[Category:中国共产党党员
(1960年入党)](../Category/中国共产党党员_\(1960年入党\).md "wikilink")
[Category:新疆大学校友](../Category/新疆大学校友.md "wikilink")
[Category:伊宁人](../Category/伊宁人.md "wikilink")
[Category:维吾尔族人](../Category/维吾尔族人.md "wikilink")

1.