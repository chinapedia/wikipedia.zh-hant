《**书谱**》，為[唐代](../Page/唐代.md "wikilink")[书法家](../Page/书法家.md "wikilink")[孙过庭所著的](../Page/孙过庭.md "wikilink")[书法理论著作](../Page/书法.md "wikilink")。成书于[武后](../Page/武則天.md "wikilink")[垂拱三年](../Page/垂拱.md "wikilink")（687年），他亲自以[草书书写全书](../Page/草书.md "wikilink")。原本现藏于[臺北](../Page/臺北.md "wikilink")[國立故宮博物院](../Page/國立故宮博物院.md "wikilink")。\[1\]

《书谱》原书共分六章两卷，第二卷作者尚未完成，现仅存第一卷。宋明清各朝皆有[刻本问世](../Page/刻本.md "wikilink")。民国初年，[延光室首先应用](../Page/延光室.md "wikilink")[摄影技术](../Page/摄影.md "wikilink")，以黑白照片的形式[影印出版了](../Page/影印.md "wikilink")《孙虔礼书谱序真迹》的墨迹，此后又应用珂罗版印刷出版发行。北平故宫博物院成立后，也有类似的影印本刊出。

《书谱》对[中国书法的影响是非常巨大的](../Page/中国书法.md "wikilink")，奠定了书法理论的基本框架。其中提到反对写字如同[绘画](../Page/绘画.md "wikilink")“巧涉丹青功亏翰墨”，认为书法审美观念要“趋变适时”，所谓“质文三变，驰骛沿革，物理常然”，反对把书法当作秘诀，择人而授的保守态度。孫過庭認為習書法應要篆、隸、楷、草四書融合交滙，否則想談書法，是不可能的。「草不兼真，殆於專謹；真不通草，殊非翰札，真以點畫為形質，使轉為情性；草以點畫為情性，使轉為形質。草乖使轉，不能成字；真虧點畫，猶可記文。回互雖殊，大體相涉。故亦傍通二篆，俯貫八分，包括篇章，涵泳飛自。若毫釐不察，則胡越殊風者焉。」

[清代思想家](../Page/清代.md "wikilink")、书法家[包世臣评价读](../Page/包世臣.md "wikilink")《书谱》的感想为「余今日不啻亲承狮子吼也」，认为如果不是《书谱》这样明白地把[草书笔法流传下来](../Page/草书.md "wikilink")，草书很可能失传。

## 原文

## 參考資料

  - [英譯
    《書譜》](http://www.vincentpoon.com/a-narrative-on-calligraphy.html)

[Category:子部藝術類](../Category/子部藝術類.md "wikilink")
[Category:中國書法](../Category/中國書法.md "wikilink")
[Category:唐朝典籍](../Category/唐朝典籍.md "wikilink")
[Category:中国书法作品](../Category/中国书法作品.md "wikilink")
[帖](../Category/国立故宫博物院藏品.md "wikilink")
[Category:中華民國國寶](../Category/中華民國國寶.md "wikilink")
[Category:草書](../Category/草書.md "wikilink")

1.