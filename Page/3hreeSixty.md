[Threesixtyhk_logo.jpg](https://zh.wikipedia.org/wiki/File:Threesixtyhk_logo.jpg "fig:Threesixtyhk_logo.jpg")
[3hreeSixty_Elements_Interior_201709.jpg](https://zh.wikipedia.org/wiki/File:3hreeSixty_Elements_Interior_201709.jpg "fig:3hreeSixty_Elements_Interior_201709.jpg")旗艦店在2017年翻新後以白色作主調\]\]
[3hreeSixty.jpg](https://zh.wikipedia.org/wiki/File:3hreeSixty.jpg "fig:3hreeSixty.jpg")分店\]\]
[ThreeSixty_Supermarket_Elements.jpg](https://zh.wikipedia.org/wiki/File:ThreeSixty_Supermarket_Elements.jpg "fig:ThreeSixty_Supermarket_Elements.jpg")

**3hreeSixty**（讀作「ThreeSixty」，前稱**ThreeSixty**），是[怡和集團旗下](../Page/怡和集團.md "wikilink")[牛奶公司擁有的](../Page/牛奶公司.md "wikilink")[超級市場及](../Page/超級市場.md "wikilink")[美食廣場](../Page/美食廣場.md "wikilink")，主要出售及提供[有機食品](../Page/有機食品.md "wikilink")，是全[亞洲首間以全有機食品為主題的](../Page/亞洲.md "wikilink")[超級市場](../Page/超級市場.md "wikilink")，主要顧客對象是[中產階層人士](../Page/中產階層.md "wikilink")。此外，該超市為支持[環保及](../Page/環保.md "wikilink")[可持續發展](../Page/可持續發展.md "wikilink")，減低對地球的破壞，，更是全港首個無[免費](../Page/免費.md "wikilink")[膠袋超市](../Page/膠袋.md "wikilink")。

## 歷史

  - 2006年11月20日，ThreeSixty於[中環](../Page/中環.md "wikilink")[置地廣塲](../Page/置地廣塲.md "wikilink")3樓及4樓開設首間分店。
  - 2007年10月1日，ThreeSixty於[九龍站](../Page/九龍站.md "wikilink")[港鐵上蓋商場](../Page/港鐵.md "wikilink")[圓方開設旗艦店](../Page/圓方.md "wikilink")，佔地33,000平方呎，成為全港最大的分店。
  - 2013年2月25日，置地廣場分店2013年2月25日結業。
  - 2017年8月，品牌名稱由ThreeSixty改名3hreeSixty。
  - 2017年9月，赤柱分店開業，ThreeSixty圓方旗艦店翻新，唯面積縮細。

## 特色

3hreeSixty是全[亞洲首間以全](../Page/亞洲.md "wikilink")[有機食品為主題的](../Page/有機食品.md "wikilink")[超級市場及](../Page/超級市場.md "wikilink")[美食廣場](../Page/美食廣場.md "wikilink")，店內主要出售有機食品、天然產品及對地球破壞較少商品的出售。由於此類商品是由[外國直接採購](../Page/外國.md "wikilink")，因此大部份商品價格略高，主要顧客對象是[中產階層人士](../Page/中產階層.md "wikilink")。

該為支持[環保及](../Page/環保.md "wikilink")[可持續發展的概念](../Page/可持續發展.md "wikilink")，超市內的裝修採用了環保建材，而且店內所有冷藏櫃不含二氧化碳，減少排放溫室氣溫，超市更首度引入以[可降解物料製造的膠袋](../Page/可降解物料.md "wikilink")。另外，自2006年12月最後一個無膠袋日開始，超市將不會向顧客供應免費膠袋，顧客如索取膠袋，每個需支付[港幣](../Page/港幣.md "wikilink")0.5元，有關款項將捐給[環境保護運動委員會作推廣環保之用](../Page/環境保護運動委員會.md "wikilink")，成為全港首個並無免費膠袋超市。

## 門市分佈

  - [香港島](../Page/香港島.md "wikilink")

<!-- end list -->

  - [赤柱](../Page/赤柱.md "wikilink")[赤柱廣場](../Page/赤柱廣場.md "wikilink")2樓201-203號(2017年9月29日開幕，前身為百佳超級市場旗下[Taste](../Page/Taste.md "wikilink"))

<!-- end list -->

  - [九龍](../Page/九龍.md "wikilink")

<!-- end list -->

  - [西九龍](../Page/西九龍.md "wikilink")[九龍站](../Page/九龍站_\(港鐵\).md "wikilink")[圓方](../Page/圓方.md "wikilink")1樓1090號舖

## 已結業門市

  - [香港島](../Page/香港島.md "wikilink")

<!-- end list -->

  - [中環](../Page/中環.md "wikilink")[置地廣塲](../Page/置地廣塲.md "wikilink")3樓及4樓（於2013年2月25日結業）\[1\]

ThreeSixty Supermarket TheLandmark.jpg|結業前的置地廣塲分店

  - [新加坡](../Page/新加坡.md "wikilink")

<!-- end list -->

  - [ION
    Orchard](../Page/ION_Orchard.md "wikilink")4樓（2009年開業，2012年10月結業）

## 參考資料

## 外部連結

  - [3hreeSixty 官方網頁](http://www.3hreesixtyhk.com/)
  - [Facebook專頁](https://www.facebook.com/3hreesixtyhk/)

[Category:2006年成立的公司](../Category/2006年成立的公司.md "wikilink")
[Category:尖沙咀](../Category/尖沙咀.md "wikilink")
[Category:赤柱](../Category/赤柱.md "wikilink")
[Category:牛奶公司](../Category/牛奶公司.md "wikilink")
[Category:環境保護](../Category/環境保護.md "wikilink")
[Category:香港超級市場](../Category/香港超級市場.md "wikilink")

1.  [ThreeSixty撤出中環　置地店月底結業](http://hk.apple.nextmedia.com/news/art/20130214/18164779)