[Kasuga_no_tsubone.jpg](https://zh.wikipedia.org/wiki/File:Kasuga_no_tsubone.jpg "fig:Kasuga_no_tsubone.jpg")

**春日局**（），本名**齋藤福**，父親是[明智光秀的家臣](../Page/明智光秀.md "wikilink")[齋藤利三](../Page/齋藤利三.md "wikilink")，母親是[稻葉一鐵](../Page/稻葉一鐵.md "wikilink")（良通）之女。後來成為[江戶幕府三代將軍](../Page/江戶幕府.md "wikilink")[德川家光的乳母](../Page/德川家光.md "wikilink")，奉命前往皇宮覲見[後水尾天皇](../Page/後水尾天皇.md "wikilink")，得到天皇賜號「春日局」。

## 生涯

春日局（齋藤福）年幼的時候，父親**齋藤利三**跟隨其主君[明智光秀發動了](../Page/明智光秀.md "wikilink")[本能寺之變](../Page/本能寺之變.md "wikilink")，將[織田信長逼死於](../Page/織田信長.md "wikilink")[本能寺](../Page/本能寺.md "wikilink")。後來發生[山崎合戰](../Page/山崎合戰.md "wikilink")，明智軍戰敗，利三被逮捕後遭處死。因為身為罪人的家屬，阿福的母親帶著子女投奔在[京都的舅舅三條西公國家中](../Page/京都.md "wikilink")，正因此，阿福成長過程中有機會學習到公家文化的[書道](../Page/書道.md "wikilink")、[歌道](../Page/歌道.md "wikilink")、[香道等藝文活動](../Page/香道.md "wikilink")。之後以稻葉一鐵之子[稻葉重通養女的身份](../Page/稻葉重通.md "wikilink")，嫁給重通義子、喪妻的[稻葉正成做第二任正室](../Page/稻葉正成.md "wikilink")。但是兩人後來離婚，原因是阿福決定要前往[大奧做當時剛出生的德川家光乳母](../Page/大奧.md "wikilink")。

1604年起，擔任[德川家光的](../Page/德川家光.md "wikilink")[乳母兼老師](../Page/乳母.md "wikilink")。家光的生母[江因為被強迫與自己兒子分離](../Page/崇源院.md "wikilink")，心裡非常難過，因此生下第二個兒子[德川忠長時](../Page/德川忠長.md "wikilink")，堅持親自養育。[秀忠夫妻因與次子比較親近](../Page/德川秀忠.md "wikilink")，都感覺次子較可愛，也希望以後讓忠長繼承將軍之位。阿福風聞此事，為家光的未來很是擔心，就親自前往大御所[德川家康所居住的](../Page/德川家康.md "wikilink")[駿府城](../Page/駿府城.md "wikilink")，希望家康主持公道，確立家光的地位。不久後，家康以行動表示自己力挺家光的態度，使秀忠夫妻頗為尷尬，也讓家光繼承權不可動搖。

隨著秀忠的退位、江與的過世，實質上幾乎與家光父母一般的阿福，掌握了[大奧極高的權力與地位](../Page/大奧.md "wikilink")。1629年，她在前往[伊勢神宮參拜的途中](../Page/伊勢神宮.md "wikilink")，以將軍家光的名義以及貴族[三條西實條義妹的頭銜入宮覲見](../Page/三條西實條.md "wikilink")[後水尾天皇與他的中宮](../Page/後水尾天皇.md "wikilink")[德川和子](../Page/德川和子.md "wikilink")，天皇賜她「**春日局**」之號，官階從三位。但由於她實際上是沒有任何官階的，僅是將軍身邊的一個乳母，卻前來皇宮參見天皇，這讓天皇與許多公卿認為是種羞辱，幕府沒有把他們的權威放在眼裡。於是後水尾天皇在當年憤而退位給年僅七歲的[明正天皇](../Page/明正天皇.md "wikilink")，以示抗議。（電視劇[大奧中春日局拜托](../Page/大奧.md "wikilink")[三條西家強行獲得](../Page/三條西家.md "wikilink")**春日局**之號並非歷史）三年後，寬永九年1632年7月20日，再次上京謁見[明正天皇](../Page/明正天皇.md "wikilink")，官階晉升至從二位，同時獲得紅色的袴及御賜的天酌杯，因此也被稱為「二位局」，與[平清盛的正妻](../Page/平清盛.md "wikilink")[平時子](../Page/平時子.md "wikilink")、[鎌倉時代的](../Page/鎌倉時代.md "wikilink")[北條政子的官階相同](../Page/北條政子.md "wikilink")。

得到「春日局」名號後的齋藤福，由以前只在[江戶城](../Page/江戶城.md "wikilink")[大奧開始掌權](../Page/大奧.md "wikilink")，到後來在政治上扮演重要角色（[公家與](../Page/公家.md "wikilink")[幕府的中間人](../Page/幕府.md "wikilink")），風頭一時無兩。而作為首位[大奧](../Page/大奧.md "wikilink")[御年寄](../Page/御年寄.md "wikilink")（即總管事）的春日局，自家光即位為三代將軍後已開始著手整治[大奧](../Page/大奧.md "wikilink")。當中頒布的《大奧法度》（以二代將軍秀忠頒布的大奧法度作基礎加以改革）更是日後[大奧二百多年](../Page/大奧.md "wikilink")（直至江戶無血開城）的法則。

1643年，春日局過世，年六十五歲。法名為「**麟祥院殿仁淵了義尼大姊**」，據說麟祥院與紹太寺都是其墓地所在。她的辭世之句是「相邀西沉之月　悟得個中道理　今日之娑婆　得以逃離乎」（）。

## 關聯項目

  - 關於春日局的[NHK大河劇](../Page/NHK大河劇.md "wikilink")
      - [葵德川三代](../Page/葵德川三代.md "wikilink")
      - [春日局](../Page/春日局_\(大河劇\).md "wikilink")
      - [江\~公主們的戰國\~](../Page/江~公主們的戰國~.md "wikilink")
  - 電視劇
      - 大奧（1968年）
      - 德川的女人們 第一部 華麗春日局
      - [大奧](../Page/大奧_\(電視劇\).md "wikilink")（1983年）
      - [大奧](../Page/大奧_\(電視劇\).md "wikilink")（2004 木曜劇場）
      - [大奧](../Page/大奧_\(2012年電視劇\).md "wikilink")（2012 週五電視劇）
      - [家政夫三田園](../Page/家政夫三田園.md "wikilink")（第一季第三集的春日井福子被影射像春日局一樣。）
  - 動漫畫
  - [甲賀忍法帖](../Page/甲賀忍法帖.md "wikilink")（其中將春日局描寫為一個，為了讓竹千代繼承將軍，而用盡各種手段的惡毒女人。）

## 外部連結

  - [春日局之墓](http://www.cnet-ta.ne.jp/p/pddlib/photo/yushima/kasuga.htm)

[Category:乳母](../Category/乳母.md "wikilink")
[Category:御年寄](../Category/御年寄.md "wikilink")
[Category:齋藤氏](../Category/齋藤氏.md "wikilink")
[Category:稻葉氏](../Category/稻葉氏.md "wikilink")
[Category:江戶時代女性](../Category/江戶時代女性.md "wikilink")
[Category:安土桃山時代女性](../Category/安土桃山時代女性.md "wikilink")