**艺术运动**，或**艺术流派**，是指一种在[艺术上具有共同宗旨和目标](../Page/艺术.md "wikilink")，被一群艺术家在一段时期内（从几个月至数十年不等）所遵循的潮流或风格。每个连贯的艺术运动都被归为一种新的[先锋派](../Page/先锋派.md "wikilink")，所以艺术运动在现代艺术中占有特别重要的地位。

艺术运动已不再被视为西方艺术独有的现象。这个专业名词包括了[视觉艺术](../Page/视觉艺术.md "wikilink")、[设计潮流和](../Page/设计.md "wikilink")[建筑风格的趋向](../Page/建筑.md "wikilink")，有时也包括[文学](../Page/文学.md "wikilink")。在[音乐方面](../Page/音乐.md "wikilink")，它更常指作品的类型和风格的更替。

## 艺术运动列表

*依英文字母排序*

  - [抽象派Abstract](../Page/抽象派.md "wikilink") expressionism
  - [學院藝術Academic](../Page/學院藝術.md "wikilink") Art
  - [行動繪畫Action](../Page/行動繪畫.md "wikilink") painting
  - [唯美主義Aestheticism](../Page/唯美主義.md "wikilink")
  - [反寫實主義Anti](../Page/反寫實主義.md "wikilink")-realism
  - Arabesque
  - [装饰艺术运动Art](../Page/装饰艺术运动.md "wikilink") Deco
  - [新艺术运动](../Page/新艺术运动.md "wikilink")(Art Nouveau
  - [贫穷艺术Arte](../Page/贫穷艺术.md "wikilink") Povera又稱貧民藝術
  - [藝術與工藝運動Arts](../Page/藝術與工藝運動.md "wikilink") and Crafts Movement
  - [垃圾箱画派Ashcan](../Page/垃圾箱画派.md "wikilink") School
  - [巴比松派Barbizon](../Page/巴比松派.md "wikilink") school
  - [巴洛克Baroque](../Page/巴洛克.md "wikilink")
  - [包豪斯Bauhaus](../Page/包豪斯.md "wikilink")
  - [古典主義Classicism](../Page/古典主義.md "wikilink")
  - [色面派Color](../Page/色面派.md "wikilink") Field
  - [概念藝術Conceptual](../Page/概念藝術.md "wikilink") art
  - [构成主义Constructivism](../Page/构成主义.md "wikilink")
  - [立体派Cubism](../Page/立体派.md "wikilink")
  - [达达主义Dadaism](../Page/达达主义.md "wikilink")
  - [荷兰风格派运动De](../Page/荷兰风格派运动.md "wikilink") Stijl (also know as Neo
    Plasticism)
  - [解构主义Deconstructivism](../Page/解构主义.md "wikilink")
  - [表现主义Expressionism](../Page/表现主义.md "wikilink")
  - [奇幻現實主義Fantastic](../Page/奇幻現實主義.md "wikilink") realism
  - [野兽派Fauvism](../Page/野兽派.md "wikilink")
  - [具象主義Figurative](../Page/具象主義.md "wikilink")
  - [新浪潮Fluxus或激浪派](../Page/新浪潮.md "wikilink")
  - [未来主义Futurism](../Page/未来主义.md "wikilink")
  - [哈莱姆文艺复兴Harlem](../Page/哈莱姆文艺复兴.md "wikilink") Renaissance
  - [印象派Impressionism](../Page/印象派.md "wikilink")
  - [国际哥特式艺术International](../Page/国际哥特式艺术.md "wikilink") Gothic
  - [那比派Les](../Page/那比派.md "wikilink") Nabis
  - Lowbrow
  - [矯飾主義Mannerism](../Page/矯飾主義.md "wikilink")
  - [大众超现实主义](../Page/大众超现实主义.md "wikilink") Massurrealism
  - [豐盛主義Maximalism](../Page/豐盛主義.md "wikilink")
  - Metaphysical painting
  - [極簡主義Minimalism又叫最低限藝術](../Page/極簡主義.md "wikilink")
  - [現代主義Modernism](../Page/現代主義.md "wikilink")
  - [新古典主義Neoclassicism](../Page/新古典主義.md "wikilink")
  - [新表現主義Neo](../Page/新表現主義.md "wikilink")-expressionism
  - Neoprimitivism
  - [奥普艺术Op](../Page/奥普艺术.md "wikilink") Art
  - [奧費主義Orphism](../Page/奧費主義.md "wikilink")
  - [照相写实主义Photorealism](../Page/照相写实主义.md "wikilink")
  - [点彩画派Pointillism又稱點描派](../Page/点彩画派.md "wikilink")
  - [波普艺术Pop](../Page/波普艺术.md "wikilink") art又稱普普艺术
  - [流行超現實主義Pop](../Page/流行超現實主義.md "wikilink") surrealism
  - [后印象派Post](../Page/后印象派.md "wikilink")-impressionism
  - [后现代主义Postmodernism](../Page/后现代主义.md "wikilink")
  - [前拉斐尔派Pre](../Page/前拉斐尔派.md "wikilink")-Raphaelites
  - Primitivism
  - [现实主义Realism又稱写实主义](../Page/现实主义.md "wikilink")
  - [文藝復興Renaissance](../Page/文藝復興.md "wikilink")
  - Renaissance Classicism
  - [洛可可Rococo](../Page/洛可可.md "wikilink")
  - [罗马式美术Romanesque](../Page/罗马式美术.md "wikilink")
  - [浪漫主义Romanticism](../Page/浪漫主义.md "wikilink")
  - [社會寫實主義](../Page/社會寫實主義.md "wikilink") Socialist Realism
  - [反概念藝術](../Page/反概念藝術.md "wikilink") Stuckism
  - [絕對主義Suprematism](../Page/絕對主義.md "wikilink")
  - [超现实主义Surrealism](../Page/超现实主义.md "wikilink")
  - [象征主义Symbolism](../Page/象征主义.md "wikilink") (arts)
  - [维也纳分离派Vienna](../Page/维也纳分离派.md "wikilink") Secession

## 参见

  - [文化运动](../Page/文化运动.md "wikilink") - 包涵更广泛的专有名词
  - [设计运动](../Page/设计运动.md "wikilink") - 专指设计方面的潮流和风格

## 編註

  - 在維基現有條目裡，[藝術流派與藝術運動有相同的內容](../Page/藝術流派.md "wikilink")，經審查之後，兩者在意義上是相同的，由於大部份的分類是由英語理論「藝術運動」而來，因此將之合併到此。

## 外部連結

  - [國立新竹師範學院美勞科教師進修網——西洋藝術流派](https://web.archive.org/web/20050228234531/http://www.aerc.nhctc.edu.tw/2-0-1/top-index.htm)

[Category:藝術運動](../Category/藝術運動.md "wikilink")
[Category:风格](../Category/风格.md "wikilink")