**李建良**，[香港](../Page/香港.md "wikilink")[男](../Page/男性.md "wikilink")[配音員](../Page/配音員.md "wikilink")，1990年代成為自由身配音員。

## 人物

  - 擁有多年配音經驗，無論聲演任何角色都十分稱職。
  - 1990年入行，由電影工作開始配音。\[1\]
  - 近年多參與[Animax](../Page/Animax.md "wikilink")、DisneyChannel等頻道的配音工作。
  - 2000年起也為[香港電台聲演](../Page/香港電台.md "wikilink")[粵語廣播劇](../Page/粵語廣播劇.md "wikilink")。
  - 2010年開始嘗試開辦專業配音班，成為香港其中一位配音行內的導師。\[2\]
  - 不時應邀為中學生的藝術教育課程分享心得，\[3\] 或與人分享保養聲線的竅門。\[4\]
  - 近年又跟著名配音員[謝月美共同演出舞台劇](../Page/謝月美.md "wikilink")：《歌聲無淚》（2012）\[5\]、《歌聲無淚》（2014）\[6\]、《沙膽大娘和她的兒女》（2016）\[7\]。

## 配音作品

※主要角色以**粗體**顯示

### 電視動畫／OVA

<table>
<thead>
<tr class="header">
<th><p>播映年份</p></th>
<th><p>作品名稱</p></th>
<th><p>配演角色</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>不明</strong></p></td>
<td><p><a href="../Page/Barbie之天鵝湖篇.md" title="wikilink">Barbie之天鵝湖篇</a></p></td>
<td><p>雷基爾</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Barbie_長髮公主.md" title="wikilink">Barbie 長髮公主</a></p></td>
<td><p>法適</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td><p><strong>柏德倫</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td><p><strong>小豬</strong><br />
鯊魚</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>1996</strong></p></td>
<td><p><a href="../Page/不思議遊戲.md" title="wikilink">不思議遊戲</a></p></td>
<td><p><strong>井宿</strong></p></td>
<td><p><a href="../Page/亞視.md" title="wikilink">亞視</a>/影碟版本</p></td>
</tr>
<tr class="even">
<td><p><strong>1999</strong></p></td>
<td><p><a href="../Page/泰山.md" title="wikilink">泰山</a></p></td>
<td><p><strong>丹丹</strong></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2006</strong></p></td>
<td></td>
<td><p><strong>丹尼斯</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2007</strong></p></td>
<td><p><a href="../Page/彩雲國物語.md" title="wikilink">彩雲國物語第一季</a></p></td>
<td><p><strong>紫劉輝</strong></p></td>
<td><p><a href="../Page/Animax_Asia.md" title="wikilink">Animax</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>2008</strong></p></td>
<td><p><a href="../Page/Fate/stay_night.md" title="wikilink">Fate/stay night</a>（命運守護夜）</p></td>
<td><p><strong>Archer</strong></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/星球大戰：複製戰紀.md" title="wikilink">星球大戰：複製戰紀</a></p></td>
<td><p>Kit Fisto</p></td>
<td><p><a href="../Page/明珠台.md" title="wikilink">明珠台</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/王牌投手_振臂高揮.md" title="wikilink">王牌投手 振臂高揮</a></p></td>
<td><p>河合和己<br />
畠篤史</p></td>
<td><p><a href="../Page/Animax_Asia.md" title="wikilink">Animax</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/冰上萬花筒.md" title="wikilink">冰上萬花筒</a></p></td>
<td><p>記者</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2010</strong></p></td>
<td><p><a href="../Page/FAIRY_TAIL_(動畫).md" title="wikilink">魔導少年</a></p></td>
<td><p>旁白<br />
馬卡羅夫‧多雷亞<br />
艾爾夫曼<br />
瓦卡巴‧米涅<br />
菲利德‧賈斯汀<br />
索爾<br />
悠卡<br />
卡伽亞馬<br />
塔羅斯</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/旋風管家_(動畫).md" title="wikilink">爆笑管家工作日誌第二季</a></p></td>
<td><p>天之聲（旁白）<br />
小8<br />
球球<br />
三千院帝<br />
瀨川虎鐵</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/寶石寵物_(第一季).md" title="wikilink">寶石寵物</a></p></td>
<td><p>總理</p></td>
<td><p><a href="../Page/電視廣播有限公司.md" title="wikilink">TVB</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/鬼太郎.md" title="wikilink">鬼太郎</a>5</p></td>
<td><p>眼珠老爹</p></td>
<td><p><a href="../Page/Animax_Asia.md" title="wikilink">Animax</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/迷宮塔_(動畫).md" title="wikilink">迷宮塔~烏魯克之盾~</a></p></td>
<td><p>烏托<br />
基爾加梅斯（國王）<br />
帕茲滋</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/家庭教師HITMAN_REBORN!_(動畫).md" title="wikilink">家庭教師</a></p></td>
<td><p>S史華路<br />
笹川了平</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/BLEACH_(動畫).md" title="wikilink">Bleach</a></p></td>
<td><p>吉良井鶴<br />
朽木白哉<br />
四楓院夜一（貓）<br />
志波岩鷲</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2011</strong></p></td>
<td><p><a href="../Page/迷宮塔.md" title="wikilink">迷宮塔～烏魯克之劍～</a></p></td>
<td><p>烏托<br />
基爾加梅斯（國王）<br />
帕茲滋</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/家庭教師HITMAN_REBORN!.md" title="wikilink">家庭教師 第二季</a></p></td>
<td><p>笹川了平</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/傑克與夢幻島海盜.md" title="wikilink">傑克與夢幻島海盜</a></p></td>
<td><p><strong>大傻</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/星際寶貝：神奇大冒險.md" title="wikilink">星際寶貝：神奇大冒險</a></p></td>
<td><p><strong>倉鼠威</strong></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 劇場版／動畫電影

<table>
<thead>
<tr class="header">
<th><p>首播年份</p></th>
<th><p>作品名稱</p></th>
<th><p>配演角色</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>1992</strong></p></td>
<td><p><a href="../Page/百變狸貓.md" title="wikilink">百變狸貓</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>1993</strong></p></td>
<td><p><a href="../Page/阿拉丁之魔王回歸.md" title="wikilink">阿拉丁之魔王回歸</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>1994</strong></p></td>
<td><p><a href="../Page/獅子王.md" title="wikilink">獅子王</a></p></td>
<td><p>班仔</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>1995</strong></p></td>
<td><p><a href="../Page/風中奇緣_(電影).md" title="wikilink">風中奇緣</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>1996</strong></p></td>
<td><p><a href="../Page/101忠狗.md" title="wikilink">101班點狗</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>1997</strong></p></td>
<td><p><a href="../Page/大力士_(1997年電影).md" title="wikilink">大力士</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>1998</strong></p></td>
<td><p><a href="../Page/蟲蟲危機.md" title="wikilink">蟲蟲危機</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/花木蘭_(1998年電影).md" title="wikilink">花木蘭</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>1999</strong></p></td>
<td><p><a href="../Page/反斗奇兵2.md" title="wikilink">反斗奇兵2</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2000</strong></p></td>
<td><p><a href="../Page/騎呢大帝.md" title="wikilink">騎呢大帝</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/102真狗.md" title="wikilink">102斑點狗</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2001</strong></p></td>
<td><p><a href="../Page/史力加.md" title="wikilink">史力加</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/衰仔樂園大電影.md" title="wikilink">衰仔樂園大電影</a></p></td>
<td><p><strong>粉皮</strong></p></td>
<td><p>【<a href="https://www.youtube.com/watch?v=5b9ZgdwvQoc">按此播放</a>】</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2002</strong></p></td>
<td><p><a href="../Page/怪獸公司.md" title="wikilink">怪獸公司</a></p></td>
<td><p><strong>史米迪</strong>（Smitty）</p></td>
<td><p>公映／VCD[8]</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/扮嘢小魔星.md" title="wikilink">扮嘢小魔星</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/熊之歷險.md" title="wikilink">熊之歷險</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/貓之報恩.md" title="wikilink">貓之報恩</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/飛天紅豬俠.md" title="wikilink">飛天紅豬俠</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/雙瞳_(電影).md" title="wikilink">雙瞳</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2004</strong></p></td>
<td><p><a href="../Page/史力加2.md" title="wikilink">史力加2</a></p></td>
<td><p>國王</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/獅子王3.md" title="wikilink">獅子王3</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2006</strong></p></td>
<td><p><a href="../Page/鼠國流浪記.md" title="wikilink">沖出水世界</a></p></td>
<td><p>法國蛙</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/地海傳說.md" title="wikilink">地海傳說</a></p></td>
<td><p>狡兔</p></td>
<td><p>公映／影碟版本</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/反斗車王.md" title="wikilink">反斗車王</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/踢躂小企鵝.md" title="wikilink">踢躂小企鵝</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/打獵季節.md" title="wikilink">森林反恐隊</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/穿越時空的少女.md" title="wikilink">穿越時空的少女</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/露寶治的世界.md" title="wikilink">露寶治的世界</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/魔女宅急便.md" title="wikilink">魔女宅急便</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Keroro軍曹_(劇場版).md" title="wikilink">軍曹大電影</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2007</strong></p></td>
<td><p><a href="../Page/史力加3.md" title="wikilink">史力加3</a></p></td>
<td><p>國王</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/阿森一族大電影.md" title="wikilink">阿森一族大電影</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/超劇場版_Keroro軍曹_2_深海的公主.md" title="wikilink">軍曹大電影2</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/仙履奇緣3：時間魔法.md" title="wikilink">仙履奇緣3：魔法逆轉</a></p></td>
<td><p>主教（Bishop）[9]</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2008</strong></p></td>
<td><p><a href="../Page/超劇場版_Keroro軍曹_3_Keroro_VS_Keroro_天空大決戰.md" title="wikilink">軍曹大電影3</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/小魚仙3.md" title="wikilink">小魚仙3</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/芭比之聖誕歡歌.md" title="wikilink">芭比之聖誕歡歌</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Gundam_0083_自護之殘光.md" title="wikilink">Gundam 0083 自護之殘光</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/打獵季節2.md" title="wikilink">森林反恐隊2</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/超級零零狗.md" title="wikilink">超級零零狗</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/大象阿鈍救細界.md" title="wikilink">大象阿鈍救細界</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2009</strong></p></td>
<td><p><a href="../Page/天煞撞正怪怪獸.md" title="wikilink">天煞撞正怪怪獸</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/崖上的波兒.md" title="wikilink">崖上的波兒</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/冰河世紀3.md" title="wikilink">冰河世紀3</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/美食風球.md" title="wikilink">美食風球</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/超劇場版_Keroro軍曹_4_逆襲的龍勇士.md" title="wikilink">軍曹大電影4</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鼠膽妙算.md" title="wikilink">鼠膽妙算</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/原子小金剛_(電影).md" title="wikilink">阿童木</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2010</strong></p></td>
<td><p><a href="../Page/史力加萬歲萬萬歲.md" title="wikilink">史力加萬歲萬萬歲</a></p></td>
<td><p>國王</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/反斗奇兵3.md" title="wikilink">反斗奇兵3</a></p></td>
<td><p>書蟲<br />
火爆史巴克<br />
釘子褲先生</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/超劇場版_Keroro軍曹_5_誕生！究極Keroro奇蹟的時空之島！！.md" title="wikilink">Keroro軍曹大電影5：反转复活岛</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/貓頭鷹守護神.md" title="wikilink">守護神傳奇</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/公主與青蛙.md" title="wikilink">公主與青蛙</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/馴龍記.md" title="wikilink">馴龍記</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/壞蛋獎門人.md" title="wikilink">壞蛋獎門人</a></p></td>
<td><p>費麥</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2011</strong></p></td>
<td><p><a href="../Page/借物少女艾莉緹.md" title="wikilink">借東西的小矮人 - 亞莉亞蒂</a></p></td>
<td><p>滅蟲專家</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/藍精靈_(電影).md" title="wikilink">藍精靈</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2012</strong></p></td>
<td><p><a href="../Page/荒失失奇兵3：歐洲逐隻捉.md" title="wikilink">荒失失奇兵3：歐洲逐隻捉</a></p></td>
<td><p>卒仔</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/綠野仙生.md" title="wikilink">綠野仙生</a></p></td>
<td><p>奧哈里</p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/勇敢傳說.md" title="wikilink">勇敢傳說之幻险森林</a></p></td>
<td><p>马田</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2013</strong></p></td>
<td><p><a href="../Page/怪獸大學.md" title="wikilink">怪獸大學</a></p></td>
<td><p>史米迪</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2014</strong></p></td>
<td><p><a href="../Page/魔雪奇緣.md" title="wikilink">魔雪奇緣</a></p></td>
<td><p>杂角</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/北斗之拳.md" title="wikilink">北斗之拳劇場版</a></p></td>
<td><p>雷奧</p></td>
<td><p><a href="../Page/J2.md" title="wikilink">J2</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/大英雄聯盟.md" title="wikilink">大英雄聯盟</a></p></td>
<td><p>将军</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2015</strong></p></td>
<td><p><a href="../Page/STAND_BY_ME_哆啦A夢.md" title="wikilink">STAND BY ME：多啦A夢3D</a></p></td>
<td><p><a href="../Page/野比大助.md" title="wikilink">野比大助</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/荒失失企鵝.md" title="wikilink">荒失失企鵝</a></p></td>
<td><p>卒仔</p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2016</strong></p></td>
<td><p><a href="../Page/優獸大都會.md" title="wikilink">優獸大都會</a></p></td>
<td><p>保哥局長 [10]</p></td>
<td></td>
</tr>
</tbody>
</table>

### 特攝片

| 首播年份     | 作品名稱                               | 配演角色     | 角色演員  | 備註                                 |
| -------- | ---------------------------------- | -------- | ----- | ---------------------------------- |
| **1998** | [超人迪加](../Page/超人迪加.md "wikilink") | **堀井正美** | 增田由紀夫 | [VCD版本](../Page/VCD.md "wikilink") |
|          |                                    |          |       |                                    |

### 韩剧

| 首播年份     | 作品名稱                                           | 配演角色    | 角色演員                             | 備註                                           |
| -------- | ---------------------------------------------- | ------- | -------------------------------- | -------------------------------------------- |
| **2013** | [特殊案件專案組TEN](../Page/特殊案件專案組TEN.md "wikilink") | **白道式** | [金相浩](../Page/金相浩.md "wikilink") | [channel M](../Page/channel_M.md "wikilink") |
|          |                                                |         |                                  |                                              |

### 台剧

<table>
<thead>
<tr class="header">
<th><p>首播年份</p></th>
<th><p>作品名稱</p></th>
<th><p>配演角色</p></th>
<th><p>角色演員</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>2010</strong></p></td>
<td><p><a href="../Page/霹靂_MIT.md" title="wikilink">霹靂 MIT</a></p></td>
<td><p>陶英明</p></td>
<td><p><a href="../Page/洪都拉斯_(藝人).md" title="wikilink">洪都拉斯</a></p></td>
<td><p><a href="../Page/J2.md" title="wikilink">J2</a></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/熊貓人_(電視劇).md" title="wikilink">熊貓人</a></p></td>
<td><p><strong>王虎</strong></p></td>
<td><p><a href="../Page/田京泉.md" title="wikilink">田京泉</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2011</strong></p></td>
<td><p><a href="../Page/智勝鮮師.md" title="wikilink">智勝鮮師</a></p></td>
<td><p>沈軍山</p></td>
<td><p><a href="../Page/简义哲.md" title="wikilink">简义哲</a></p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2013</strong></p></td>
<td><p><a href="../Page/熱血青春.md" title="wikilink">熱血青春</a></p></td>
<td><p><strong>菜老板<br />
刘德华</strong></p></td>
<td><p><a href="../Page/白雲_(台灣藝人).md" title="wikilink">白云</a></p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2014</strong></p></td>
<td><p><a href="../Page/愛情急整室.md" title="wikilink">愛情急整室</a></p></td>
<td><p>韩伯恩</p></td>
<td><p><a href="../Page/宗華.md" title="wikilink">宗華</a></p></td>
<td><p><a href="../Page/煲劇1台.md" title="wikilink">煲劇1台</a></p></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 電影

<table>
<thead>
<tr class="header">
<th><p>播映年份</p></th>
<th><p>作品名稱</p></th>
<th><p>配演角色</p></th>
<th><p>角色演员</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>1991</strong></p></td>
<td><p><a href="../Page/契媽唔易做.md" title="wikilink">契媽唔易做</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>1992</strong></p></td>
<td><p><a href="../Page/西楚霸王上、下.md" title="wikilink">西楚霸王上、下</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>1995</strong></p></td>
<td><p><a href="../Page/南京的基督.md" title="wikilink">南京的基督</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>1998</strong></p></td>
<td><p><a href="../Page/Dr._Dolittle.md" title="wikilink">Dr. Dolittle</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>1999</strong></p></td>
<td><p><a href="../Page/秦山與珍妮.md" title="wikilink">秦山與珍妮</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/星球大戰前傳：魅影危機.md" title="wikilink">星球大戰前傳：魅影危機</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/一家之鼠.md" title="wikilink">一家之鼠</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2001</strong></p></td>
<td><p><a href="../Page/哈利波特—神秘的魔法石_(電影).md" title="wikilink">哈利波特 神秘的魔法石</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/我的野蠻女友.md" title="wikilink">我的野蠻女友</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/非常小特務.md" title="wikilink">非常小特務</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/Dr._Dolittle_2.md" title="wikilink">Dr. Dolittle 2</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2002</strong></p></td>
<td><p><a href="../Page/哈利波特—消失的密室_(電影).md" title="wikilink">哈利波特 消失的密室</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/星球大戰前傳II：複製人侵略.md" title="wikilink">星球大戰前傳II：複製人侵略</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/狗狗震.md" title="wikilink">狗狗震</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/一家之鼠II.md" title="wikilink">一家之鼠II</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2003</strong></p></td>
<td><p><a href="../Page/樂一通反斗特攻隊.md" title="wikilink">樂一通反斗特攻隊</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2004</strong></p></td>
<td><p><a href="../Page/哈利波特—阿茲卡班的逃犯_(電影).md" title="wikilink">哈利波特 阿茲卡班的逃犯</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/北極特快車.md" title="wikilink">北極特快車</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/十面埋伏.md" title="wikilink">十面埋伏</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/千機變II之花都大戰.md" title="wikilink">千機變II之花都大戰</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/狗狗震多震.md" title="wikilink">狗狗震多震</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2005</strong></p></td>
<td><p><a href="../Page/朱古力掌門人.md" title="wikilink">朱古力掌門人</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/哈利波特－火盃的考驗_(電影).md" title="wikilink">哈利波特 火杯的考驗</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/星球大戰前傳III：黑帝君臨.md" title="wikilink">星球大戰前傳III 黑帝君臨</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/吾妻十六歲.md" title="wikilink">吾妻十六歲</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/霍元甲.md" title="wikilink">霍元甲</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/冬蔭功_(電影).md" title="wikilink">冬蔭功</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2006</strong></p></td>
<td><p><a href="../Page/双猫记.md" title="wikilink">加菲貓電影 雙貓記</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2007</strong></p></td>
<td><p><a href="../Page/投名狀.md" title="wikilink">投名狀</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/集結號.md" title="wikilink">集結號</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/哈利波特—鳳凰會的密令_(電影).md" title="wikilink">哈利波特 鳳皇會密令</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/Bee_Movie蜜蜂.md" title="wikilink">Bee Movie蜜蜂</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/芭比之十二公主.md" title="wikilink">芭比之十二公主</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/拳霸.md" title="wikilink">拳霸</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/滿城盡帶黃金甲.md" title="wikilink">滿城盡帶黃金甲</a></p></td>
<td></td>
<td></td>
<td><p>DVD／VCD</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/衝浪企鵝.md" title="wikilink">衝浪企鵝</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2008</strong></p></td>
<td><p><a href="../Page/梅比斯--超人兄弟世紀之戰.md" title="wikilink">梅比斯--超人兄弟世紀之戰</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/野蠻的溫柔.md" title="wikilink">野蠻的溫柔</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/奇妙仙子.md" title="wikilink">奇妙仙子</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/保持通話.md" title="wikilink">保持通話</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/我老婆係賭聖.md" title="wikilink">我老婆係賭聖</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/星際狗狗_Space_Buddies.md" title="wikilink">星際狗狗 Space Buddies</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/大耳仔走天涯.md" title="wikilink">大耳仔走天涯</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/芭比之拇指姑娘.md" title="wikilink">芭比之拇指姑娘</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/變種警察.md" title="wikilink">變種警察</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2009</strong></p></td>
<td><p><a href="../Page/拳霸3.md" title="wikilink">拳霸3</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/機器俠.md" title="wikilink">機器俠</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/一百萬零一夜.md" title="wikilink">一百萬零一夜</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/哈利波特—混血王子的背叛_(電影).md" title="wikilink">哈利波特 混血王子的背叛</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/奇妙仙子與失落的寶藏.md" title="wikilink">奇妙仙子與失落的寶藏</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/聖誕快樂頌.md" title="wikilink">聖誕快樂頌</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/海雲臺_(電影).md" title="wikilink">海雲臺</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/十月圍城.md" title="wikilink">十月圍城</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2010</strong></p></td>
<td><p><a href="../Page/蘇乞兒_(2010年電影).md" title="wikilink">蘇乞兒</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/海洋天堂.md" title="wikilink">海洋天堂</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/哈利波特—死神的聖物1.md" title="wikilink">哈利波特 死神的聖物 - 上</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/愛麗絲夢遊仙境_(2010年電影).md" title="wikilink">愛麗絲夢遊仙境</a></p></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td><p><strong>2011</strong></p></td>
<td><p><a href="../Page/哈利波特—死神的聖物2.md" title="wikilink">哈利波特 死神的聖物 - 下</a></p></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2015</strong></p></td>
<td><p><a href="../Page/捉妖记_(2015年电影).md" title="wikilink">捉妖记</a></p></td>
<td><p>有待确认</p></td>
<td></td>
<td><p>粤语公映版</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

### 綜藝節目／紀錄片

| 首播年份     | 作品名稱                                     | 主要配演 | 備註 |
| -------- | ---------------------------------------- | ---- | -- |
| **2015** | [鄭成功古船重現](../Page/鄭成功古船重現.md "wikilink") | 旁白   |    |
|          |                                          |      |    |

## 廣播劇

<table>
<thead>
<tr class="header">
<th><p>年份</p></th>
<th><p>廣播劇名稱</p></th>
<th><p>配演角色</p></th>
<th><p>備註</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>1999</strong></p></td>
<td><p><a href="../Page/書劍恩仇錄.md" title="wikilink">書劍恩仇錄</a></p></td>
<td><p>十當家·章進</p></td>
<td><p>香港電台<a href="../Page/金庸.md" title="wikilink">金庸武俠小說</a>[11]</p></td>
</tr>
<tr class="even">
<td><p><strong>2000</strong></p></td>
<td><p><a href="../Page/鹿鼎記.md" title="wikilink">鹿鼎記</a></p></td>
<td><p><strong>瘦頭陀</strong></p></td>
<td><p>香港電台<a href="../Page/金庸.md" title="wikilink">金庸武俠小說</a></p></td>
</tr>
<tr class="odd">
<td><p><strong>2001</strong></p></td>
<td><p><a href="../Page/雍正皇帝_(小說).md" title="wikilink">雍正皇帝</a></p></td>
<td><p>何柱兒</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><strong>2006</strong></p></td>
<td><p><a href="../Page/獅子山下.md" title="wikilink">獅子山下</a></p></td>
<td><p>徒弟</p></td>
<td><p>第10-12集<br />
與<a href="../Page/謝月美.md" title="wikilink">謝月美共同演出</a>[12]</p></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

## 外部連結

[Category:香港配音員](../Category/香港配音員.md "wikilink")
[J](../Category/李姓.md "wikilink")

1.  [專業配音訓練班－－入門班](http://www.hkslmt.com/208542018230456383643550631243.html)：【導師簡介】（2012年）
2.  [專業配音訓練班－－入門班](http://www.hkslmt.com/208542018230456383643550631243.html)（2012年），[香港手語歌舞劇團](http://www.hkslmt.com/)網頁。
3.  【[李建良先生(配音導師)講座](https://get.google.com/albumarchive/102991942167227740826/album/AF1QipN_NdZkx4zV9-qok9Tp1i8mz-LlJ3SdexWpI_0V)】，香港[保良局胡忠中學](../Page/保良局胡忠中學.md "wikilink")「藝術教育」講座，2012年2月21。
4.  「[聲音與性格](http://talks.club-o.org/Thu20130314/)」，[Club-O
    星期四療癒夜](http://www.club-o.org/talks-2013.php)系列，2013年3月14日。
5.  [香港影視劇團《歌聲無淚》官網](http://www.hkmtts.com/)；[《歌聲無淚2012》](http://archive.fo/oUVXx)，[朱克原著](../Page/朱克.md "wikilink")，[陳哲民編導](../Page/陳哲民.md "wikilink")，演員包括[石修](../Page/石修.md "wikilink")、[謝月美](../Page/謝月美.md "wikilink")、[潘芳芳](../Page/潘芳芳.md "wikilink")、[安德尊](../Page/安德尊.md "wikilink")、[鄧安迪](../Page/鄧安迪.md "wikilink")、[鄭佩嘉](../Page/鄭佩嘉.md "wikilink")、[梁惠安](../Page/梁惠安.md "wikilink")、[黃紫嫻](../Page/黃紫嫻.md "wikilink")、[許淑嫻](../Page/許淑嫻.md "wikilink")、**李建良**等。2012年3月10至11日，元朗劇院演藝廳。
6.  [香港影視劇團製作](../Page/香港影視劇團.md "wikilink")：[《歌聲無淚2014》](https://www.art-mate.net/?a=doc&id=29081&name=%E3%80%8A%E6%AD%8C%E8%81%B2%E7%84%A1%E6%B7%9A2014%E3%80%8B)，[朱克原著](../Page/朱克.md "wikilink")，[陳哲民編導](../Page/陳哲民.md "wikilink")，演員包括[石修](../Page/石修.md "wikilink")、[謝月美](../Page/謝月美.md "wikilink")、[潘芳芳](../Page/潘芳芳.md "wikilink")、[安德尊](../Page/安德尊.md "wikilink")、[麥智鈞](../Page/麥智鈞.md "wikilink")、[龍天生](../Page/龍天生.md "wikilink")、**李建良**、[劉昭文等](../Page/劉昭文.md "wikilink")。2014年10月10至12日，荃灣大會堂演奏廳上演。
7.  [第四線](http://www.horizonte.hk/)主辦，[香港話劇團](../Page/香港話劇團.md "wikilink")[合辦](http://www.hkrep.com/events/mothercourageandherchildren/)，[布萊希特](../Page/贝托尔特·布莱希特.md "wikilink")（Bertolt
    Brecht）原著，[盧偉力導演](../Page/盧偉力.md "wikilink")，演員包括[謝月美](../Page/謝月美.md "wikilink")、[余德銘](../Page/余德銘.md "wikilink")、[潘芳芳](../Page/潘芳芳.md "wikilink")、[鍾珍珍](../Page/鍾珍珍.md "wikilink")、[王少萍](../Page/王少萍.md "wikilink")、[利順琼](../Page/利順琼.md "wikilink")、**李建良**等。2016年5月12至15日上環文娛中心香港話劇團黑盒劇場。
8.  [VCD片尾報幕截圖](https://drive.google.com/file/d/0B11vUMKxjH0Eb2s4VnJlanJJb1k/view)
9.  [Character
    voice](http://disneyinternationaldubbings.weebly.com/cinderella--cantonese-cast.html)，迪士尼官網香港版。
10. [片末報幕截圖](https://upload.cc/i/LoM5S1.jpg)；參考youtube片段｛[Zootopia 優獸大都會
    Cantonese voice cast
    粵語配音報幕](https://www.youtube.com/watch?v=JEwwyLs7zuE)｝
11. 「演員表」，[金庸武俠天地－－《書劍恩仇錄》](http://rthk9.rthk.hk/classicschannel/bookandsword.htm)，香港電台經典重溫。
12. 「演員表」，[《獅子山下》](http://rthk9.rthk.hk/radiodrama/6contemperary/f_lionrock_53.htm)，香港電台經典重溫。