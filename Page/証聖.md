**證聖**（695年正月—九月）是[武則天的年号](../Page/武則天.md "wikilink")。

## 大事记

  - 證聖元年正月初一辛巳（694年11月23日），改元。
  - 證聖元年九月初九甲寅（695年10月22日），改元天冊萬歲。

## 出生

## 逝世

## 纪年

| 証聖                             | 元年                             |
| ------------------------------ | ------------------------------ |
| [公元](../Page/公元.md "wikilink") | 695年                           |
| [干支](../Page/干支.md "wikilink") | [乙未](../Page/乙未.md "wikilink") |

### 註解

## 參看

  - [中国年号列表](../Page/中国年号列表.md "wikilink")
  - 同期存在的其他政权年号

[Category:武周年号](../Category/武周年号.md "wikilink")
[Category:7世纪中国年号](../Category/7世纪中国年号.md "wikilink")
[Category:690年代中国](../Category/690年代中国.md "wikilink")
[Category:695年](../Category/695年.md "wikilink")