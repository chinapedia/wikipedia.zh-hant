[Chen_Huacheng_-_DSCF9885.JPG](https://zh.wikipedia.org/wiki/File:Chen_Huacheng_-_DSCF9885.JPG "fig:Chen_Huacheng_-_DSCF9885.JPG")
**陈化成**（），[字](../Page/表字.md "wikilink")**业章**、**蓮峰**\[1\]，[諡忠愍](../Page/諡.md "wikilink")\[2\]，[福建](../Page/福建.md "wikilink")[同安人](../Page/同安縣.md "wikilink")，成長於[臺灣府](../Page/臺灣府.md "wikilink")[淡水廳](../Page/淡水廳.md "wikilink")[新莊頭前庄](../Page/新莊區.md "wikilink")（今[臺北捷運](../Page/臺北捷運.md "wikilink")[頭前庄站附近](../Page/頭前庄站.md "wikilink")），[清朝高階](../Page/清朝.md "wikilink")[軍官](../Page/軍官.md "wikilink")，擊敗海盜於艋舺、萬華一帶，升任[台灣總兵](../Page/台灣總兵.md "wikilink")、福建水师[提督](../Page/提督.md "wikilink")、[江南提督等要職](../Page/江南提督.md "wikilink")，在[鴉片戰爭中陣亡](../Page/鴉片戰爭.md "wikilink")，被視為民族英雄。

## 生平

陈化成[籍貫](../Page/籍貫.md "wikilink")[福建](../Page/福建.md "wikilink")[同安](../Page/同安.md "wikilink")，童年移居[台灣](../Page/台灣.md "wikilink")，於[淡水厅](../Page/淡水厅.md "wikilink")[興直堡頭前庄](../Page/興直堡.md "wikilink")（今[新北市](../Page/新北市.md "wikilink")[新莊區](../Page/新莊區.md "wikilink")）成長。新莊民間傳說陳化成早年父母雙亡，自幼跟隨長兄夫婦共同生活，唯化成自幼身長體能過人，食量亦過人，長兄無力供養，遂促其投入軍旅，而其大嫂甚為不捨，臨別時贈與親手縫製[布鞋一雙](../Page/布鞋.md "wikilink")（另說特意赴廟宇求之[香火](../Page/香火.md "wikilink")、[香包等](../Page/香包.md "wikilink")），陳化成感撫養之恩，行伍間經常把布鞋繫於腰間懷內，不稍離身。陳化成初入軍中，不會使用[火槍](../Page/火槍.md "wikilink")，亦不曉刀棍，但身高過人，[軍官任之為](../Page/軍官.md "wikilink")[軍旗手](../Page/軍旗.md "wikilink")，一日，清軍與匪兵交戰失利，苦候援軍不至，只好撤退，此時陳化成的布鞋突然遺失，心急狂奔，欲找尋之。古時鳴金收兵，聞鼓前進，皆以軍旗為指標，[士卒見軍旗奔向](../Page/士卒.md "wikilink")[前線](../Page/前線.md "wikilink")，以為援軍來臨，發動總攻，士氣大振，人人冒死前進，終於打敗敵軍凱旋而歸，陳化成因舉旗有功，順遂昇官。

後因镇压亦是同安人的[海盜](../Page/海盜.md "wikilink")[蔡牵](../Page/蔡牵.md "wikilink")，因此由[把总一路擢升至](../Page/把总.md "wikilink")[金门](../Page/金门.md "wikilink")[总兵](../Page/总兵.md "wikilink")\[3\]。[清宣宗](../Page/清宣宗.md "wikilink")[道光十年](../Page/道光.md "wikilink")（1830年）任福建水师[提督](../Page/提督.md "wikilink")\[4\]，驻守[厦门](../Page/厦门.md "wikilink")，在[闽浙总督](../Page/闽浙总督.md "wikilink")[邓廷桢的支持下多次击退来犯的](../Page/邓廷桢.md "wikilink")[英国舰队](../Page/英国.md "wikilink")。[清宣宗](../Page/清宣宗.md "wikilink")[道光二十年](../Page/道光.md "wikilink")（1840年）七月，调任[江南提督](../Page/江南提督.md "wikilink")\[5\]。时[第一次鸦片战争已经爆发](../Page/第一次鸦片战争.md "wikilink")，陈化成在[两江总督](../Page/两江总督.md "wikilink")[裕谦支持下](../Page/裕谦.md "wikilink")，完善了位于[长江和](../Page/长江.md "wikilink")[黄浦江江口](../Page/黄浦江.md "wikilink")[吴淞炮台的防御措施](../Page/吴淞炮台.md "wikilink")，誓死保卫长江水道。道光二十一年（1841年）[英军占](../Page/英军.md "wikilink")[浙江](../Page/浙江.md "wikilink")[舟山](../Page/舟山.md "wikilink")[定海](../Page/定海.md "wikilink")，裕谦、[葛云飞等](../Page/葛云飞.md "wikilink")[牺牲](../Page/牺牲.md "wikilink")。

[道光二十二年](../Page/道光.md "wikilink")（1842年）6月，英军攻打吴淞炮台，陈化成拒绝[两江总督](../Page/两江总督.md "wikilink")[-{zh-hans:牛鉴;zh-hant:牛鑑}-的议和主张](../Page/牛鑑.md "wikilink")，率军坚守六昼夜，击伤英舰8艘，毙伤[英军士兵](../Page/英军.md "wikilink")500余人，于6月16日中弹牺牲。屍首於12日後才被尋獲。有子陳廷芳、陳廷棻，孙陳振世。

## 紀念

清朝扬威将军、[乾隆帝曾孙](../Page/乾隆帝.md "wikilink")[奕经命貝青喬为陈化成做挽词](../Page/奕经.md "wikilink")

[上海人奉其像於當地著名之](../Page/上海人.md "wikilink")[上海城隍廟](../Page/上海城隍廟.md "wikilink")，陳化成也與[霍光](../Page/霍光.md "wikilink")、[秦裕伯合稱](../Page/秦裕伯.md "wikilink")「上海三大[城隍](../Page/城隍.md "wikilink")」。在[上海](../Page/上海.md "wikilink")，除了[黄浦区](../Page/黄浦区.md "wikilink")[城隍廟外](../Page/城隍廟.md "wikilink")，[宝山区有](../Page/宝山区.md "wikilink")[陈化成紀念館和化成路](../Page/宝山文庙.md "wikilink")，[松江区有](../Page/松江区.md "wikilink")[陈化成祠](../Page/方塔园.md "wikilink")。在[福建](../Page/福建.md "wikilink")[厦门金榜山](../Page/厦门.md "wikilink")，有陈化成墓。

在臺灣，陳化成曾隨伯母蔡氏移居[新莊頭前庄](../Page/新莊區.md "wikilink")，庄內的主要道路被命名為「化成路」。陳化成姪子陳光淵中舉之後，在頭前庄內豎立旗桿紀念，後被移至中平公園，新莊市長蔡家福立有〈民族英雄陳化成紀念碑文〉。

[File:民族英雄陳化成紀念碑.JPG|新北市新莊區中平公園民族英雄陳化成紀念碑](File:民族英雄陳化成紀念碑.JPG%7C新北市新莊區中平公園民族英雄陳化成紀念碑)
[File:民族英雄陳化成紀念碑文.JPG|民族英雄陳化成紀念碑文](File:民族英雄陳化成紀念碑文.JPG%7C民族英雄陳化成紀念碑文)

## 參考文獻

{{-}}

[category:第一次鴉片戰爭人物](../Page/category:第一次鴉片戰爭人物.md "wikilink")

[Category:清朝江南提督](../Category/清朝江南提督.md "wikilink")
[Category:清朝福建水师提督](../Category/清朝福建水师提督.md "wikilink")
[Category:澎湖水師協副將](../Category/澎湖水師協副將.md "wikilink")
[Category:台灣水師協副將](../Category/台灣水師協副將.md "wikilink")
[Category:台灣鎮總兵](../Category/台灣鎮總兵.md "wikilink")
[Category:清朝戰爭身亡者](../Category/清朝戰爭身亡者.md "wikilink")
[Category:人物神](../Category/人物神.md "wikilink")
[Category:新莊人](../Category/新莊人.md "wikilink")
[Category:同安人](../Category/同安人.md "wikilink")
[H化成](../Category/陈姓.md "wikilink")
[Category:城隍](../Category/城隍.md "wikilink")

1.  國立故宮博物院圖書文獻處清史館傳稿 ,701006650號
2.  國立故宮博物院圖書文獻處清國史館傳稿,702003435-6號
3.  國立故宮博物院圖書文獻處清國史館傳包 ,702003435-5號
4.  國立故宮博物院圖書文獻處清國史館傳包 ,702003435-5號
5.  國立故宮博物院圖書文獻處清國史館傳包 ,702003435-5號