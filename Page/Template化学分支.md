[無機化合物列表](../Page/無機化合物列表.md "wikilink"){{·}}[有機化合物列表](../Page/有機化合物列表.md "wikilink"){{·}}[元素周期表](../Page/元素周期表.md "wikilink")

| group1=[物理化學](../Page/物理化學.md "wikilink") | list1=

  - **[電化學](../Page/電化學.md "wikilink")**
  - [光化學](../Page/光化學.md "wikilink")
  - [熱化學](../Page/熱化學.md "wikilink")
  - [飞秒化学](../Page/飞秒化学.md "wikilink")
  - [地球化學](../Page/地球化學.md "wikilink")
  - [固體化學](../Page/固體化學.md "wikilink")
  - **[量子化學](../Page/量子化學.md "wikilink")**
  - [表面科學](../Page/表面科學.md "wikilink")
  - [化学热力学](../Page/化学热力学.md "wikilink")
  - [化学动力学](../Page/化学动力学.md "wikilink")
  - [化学物理学](../Page/化学物理学.md "wikilink")
  - [光谱学](../Page/光谱学.md "wikilink")
  - [立体化学](../Page/立体化学.md "wikilink")

| group2=[有機化學](../Page/有機化學.md "wikilink") | list2=

  - **[生物化學](../Page/生物化學.md "wikilink")**

  - [生物有機化學](../Page/生物有機化學.md "wikilink")

  -
  - [化学生物学](../Page/化学生物学.md "wikilink")

  - [临床化学](../Page/临床化学.md "wikilink")

  - [藥物化學](../Page/藥物化學.md "wikilink")

  - [神經化學](../Page/神經化學.md "wikilink")

  - [藥學](../Page/藥學.md "wikilink")

  -
  - **[高分子化學](../Page/高分子化學.md "wikilink")**

  - [富勒烯化学](../Page/富勒烯化学.md "wikilink")

| group3=[無機化學](../Page/無機化學.md "wikilink") | list3=

  - [生物無機化學](../Page/生物無機化學.md "wikilink")
  - [金屬有機化學](../Page/金屬有機化學.md "wikilink")
  - [原子簇化學](../Page/原子簇化學.md "wikilink")
  - [配位化學](../Page/配位化學.md "wikilink")
  - **[材料科學](../Page/材料科學.md "wikilink")**
  - [核化學](../Page/核化學.md "wikilink")
  - [放射化學](../Page/放射化學.md "wikilink")

| group4=[分析化学](../Page/分析化学.md "wikilink") | list4=

  - [仪器分析](../Page/仪器分析.md "wikilink")
  - [光谱法](../Page/光谱学.md "wikilink")
  - [电化学分析](../Page/电化学分析.md "wikilink")
  - [色谱法](../Page/色谱法.md "wikilink")
  - [质谱法](../Page/质谱法.md "wikilink")
  - “联用”技术

| group5=其他化學 | list5=

  -
  - [天體化學](../Page/天體化學.md "wikilink")

  - [大气化学](../Page/大气化学.md "wikilink")

  - [分析化学](../Page/分析化学.md "wikilink")

  -
  - [宇宙化學](../Page/宇宙化學.md "wikilink")

  - [點擊化學](../Page/點擊化學.md "wikilink")

  - [計算化學](../Page/計算化學.md "wikilink")

  - [環境化學](../Page/環境化學.md "wikilink")

  - [食品化学](../Page/食品化学.md "wikilink")

  -
  - [綠色化學](../Page/綠色化學.md "wikilink")

  -
  - [超分子化學](../Page/超分子化學.md "wikilink")

  - [理論化學](../Page/理論化學.md "wikilink")

  - [組合化学](../Page/組合化学.md "wikilink")

  - [溼化學](../Page/溼化學.md "wikilink")

| belowclass = hlist | below =
**[化學分類](../Category/化學.md "wikilink")**{{·}}**[化學主題](../Page/Portal:化學.md "wikilink")**{{·}}![Commons-logo.svg](https://zh.wikipedia.org/wiki/File:Commons-logo.svg
"Commons-logo.svg")
**[共享資源](../Page/commons:Category:Chemistry.md "wikilink")**{{·}}
**[專題](../Page/Wikipedia:化學專題.md "wikilink")** }}<noinclude>

</noinclude>

[Category:化學模板](../Category/化學模板.md "wikilink")