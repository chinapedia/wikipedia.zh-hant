**孫森焱**（），[台灣台南市人](../Page/台灣.md "wikilink")，台灣法學家，[中華民國前](../Page/中華民國.md "wikilink")[司法院大法官](../Page/司法院大法官.md "wikilink")。

## 經歷

孫森焱出身於一個經營雜貨店的家庭，因為成長於[日治時期](../Page/台灣日治時期.md "wikilink")，因此孫森焱能通日文。孫森焱有位著名的親戚就是日治時代台灣第一個檢察官，在[二二八事件中身故的](../Page/二二八事件.md "wikilink")[王育霖](../Page/王育霖.md "wikilink")。[二次大戰結束後](../Page/二次大戰.md "wikilink")，台灣隨即為國府接收，當時孫森焱就讀小學五年級，次年（1946年）小學畢業，考入省立台南二中（今[國立台南二中](../Page/國立臺南第二高級中學.md "wikilink")），始學中文。1952年，孫森焱進入[台灣大學法律學系就讀](../Page/台灣大學.md "wikilink")，1956年畢業後旋考取[司法官](../Page/司法官.md "wikilink")。惟因服兵役，至1959年始受司法官訓練所第四期之訓練。

1961年，孫森焱正式擔任司法官，後歷任[檢察官](../Page/檢察官.md "wikilink")、推事（[法官](../Page/法官.md "wikilink")）等職，至1990年擔任[最高法院庭長之職](../Page/中華民國最高法院.md "wikilink")。1994年，經[李登輝總統提名](../Page/李登輝.md "wikilink")，[國民大會同意](../Page/國民大會.md "wikilink")，出任第六屆大法官，2003年任期屆滿退職。

在學術方面，自1975年起，孫森焱即在[東吳大學兼授](../Page/東吳大學_\(台灣\).md "wikilink")「民法債編總論」，其後又出版不少民法方面之著作，如民法債編總論上下冊。並曾貢獻所長，參與[民法](../Page/民法.md "wikilink")、[民事訴訟法之修正](../Page/民事訴訟法.md "wikilink")。

## 家庭

孫森焱之妻[黃綠星與孫森焱為](../Page/黃綠星.md "wikilink")[司法官訓練所同期同學](../Page/司法官訓練所.md "wikilink")，亦為台灣法律實務界人物，曾任[最高行政法院法官及庭長](../Page/中華民國最高行政法院.md "wikilink")，現已退休。

孫森焱之女[孫迺翊為](../Page/孫迺翊.md "wikilink")[國立台灣大學法律學系教授](../Page/國立台灣大學法律學系.md "wikilink")，亦為律師[賴中強之妻](../Page/賴中強.md "wikilink")。孫森焱之子[孫迺翔為](../Page/孫迺翔.md "wikilink")[義守大學電機系教授](../Page/義守大學.md "wikilink")。

## 參考資料

<div style="font-size: 90%">

  - 《大法官釋憲史料》，[司法院編](../Page/司法院.md "wikilink")，1998年。

</div>

[Category:台湾法学家](../Category/台湾法学家.md "wikilink")
[Category:中華民國檢察官](../Category/中華民國檢察官.md "wikilink")
[Category:東吳大學教授](../Category/東吳大學教授.md "wikilink")
[Category:國立臺灣大學法律學院校友](../Category/國立臺灣大學法律學院校友.md "wikilink")
[Category:臺灣省立臺南第二中學校友](../Category/臺灣省立臺南第二中學校友.md "wikilink")
[Category:台南市人](../Category/台南市人.md "wikilink")
[Sen森](../Category/孙姓.md "wikilink")