**加舒尔布鲁木I峰**（**Gasherbrum
I**）位于[中国和](../Page/中華人民共和國.md "wikilink")[巴控克什米尔边境的](../Page/巴控克什米尔.md "wikilink")[喀喇昆仑山脉](../Page/喀喇昆仑山脉.md "wikilink")，距离[乔戈里峰约](../Page/乔戈里峰.md "wikilink")21-{zh-hans:千米;
zh-hant:公里;}-，海拔8,080-{zh-hans:米;
zh-hant:公尺;}-，是世界第十一高峰。“加舒尔布鲁木”在当地[巴尔蒂语中意为](../Page/巴尔蒂语.md "wikilink")“美丽的（rgasha）山（brum）”。

加舒尔布鲁木I峰最初被西方探险队称作K5，是由于1856年[英国](../Page/英国.md "wikilink")[蒙哥马利少尉](../Page/蒙哥马利.md "wikilink")（T.
G.
Montgomerie）首次考察[喀喇昆仑山脉时](../Page/喀喇昆仑山脉.md "wikilink")，用“K”开头标出了此山脉自西向东的5座主要山峰。1892年探险家因该峰渺远难寻，又称其为“隐峰（Hidden
Peak）”。1958年，一支由[由克·林奇带领的八人](../Page/由克·林奇.md "wikilink")[美国登山队首次登上加舒尔布鲁木I峰](../Page/美国.md "wikilink")，登顶者为和[安迪·考夫曼](../Page/安迪·考夫曼.md "wikilink")。

## 参见

  - [山峰列表](../Page/山峰列表.md "wikilink")

[Category:八千米高山](../Category/八千米高山.md "wikilink")
[Category:巴基斯坦山峰](../Category/巴基斯坦山峰.md "wikilink")
[Category:克什米尔](../Category/克什米尔.md "wikilink")