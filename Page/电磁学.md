**电磁学**（英語：electromagnetism）是研究**电磁力**（[電荷粒子之间的一种物理性相互作用](../Page/電荷.md "wikilink")）
的[物理学的一个分支](../Page/物理学.md "wikilink")。电磁力通常表现为[电磁场](../Page/电磁场.md "wikilink")，如[電場](../Page/電場.md "wikilink")、[磁場和](../Page/磁場.md "wikilink")[光](../Page/光.md "wikilink")。电磁力是[自然界中四种](../Page/自然.md "wikilink")[基本相互作用之一](../Page/基本相互作用.md "wikilink")。其它三种基本相互作用是[强相互作用](../Page/强相互作用.md "wikilink")、[弱相互作用](../Page/弱相互作用.md "wikilink")、[引力](../Page/引力.md "wikilink")。\[1\]
[電學與](../Page/電學.md "wikilink")[磁學領域密切相關](../Page/磁學.md "wikilink")。[電磁學可以廣義地包含電學和磁學](../Page/電磁學.md "wikilink")，但狹義來說是探討電與磁彼此之間相互關係的一門學科。
[Lightning.0257.jpg](https://zh.wikipedia.org/wiki/File:Lightning.0257.jpg "fig:Lightning.0257.jpg")是一种在两个带电区域之间传播的[静电放电现象](../Page/静电放电.md "wikilink")。\]\]
[Circular.Polarization.Circularly.Polarized.Light_Right.Handed.Animation.305x190.255Colors.gif](https://zh.wikipedia.org/wiki/File:Circular.Polarization.Circularly.Polarized.Light_Right.Handed.Animation.305x190.255Colors.gif "fig:Circular.Polarization.Circularly.Polarized.Light_Right.Handed.Animation.305x190.255Colors.gif")的電磁波輻射中，電場向量隨時間的變化示意動畫\]\]
英文单词*electromagnetism*是两个[希腊语词汇ἢλεκτρον](../Page/希腊语.md "wikilink")（*ēlektron*，“[琥珀](../Page/琥珀.md "wikilink")”）和μαγνήτης（*magnetic*源自"magnítis
líthos"（μαγνήτης
λίθος），意思是“镁石”，一种[铁矿](../Page/铁矿.md "wikilink")）的合成词。研究电磁现象的[科学是用电磁力定义的](../Page/科学.md "wikilink")，有时称作[洛伦兹力](../Page/洛伦兹力.md "wikilink")，是既含有[電也含有](../Page/電.md "wikilink")[磁的现象](../Page/磁.md "wikilink")。

电磁力在决定日常生活中大多数物体的内部性质中发挥着主要作用。常见物体的电磁力表现在物体中单个[分子之间的](../Page/分子.md "wikilink")[分子间作用力的结果中](../Page/分子间作用力.md "wikilink")。[电子被电磁波力学束缚在](../Page/电子.md "wikilink")[原子核周围形成](../Page/原子核.md "wikilink")[原子](../Page/原子.md "wikilink")，而原子是分子的构成单位。相邻原子的[电子之间的相互作用产生](../Page/分子轨道.md "wikilink")[化學过程](../Page/化學.md "wikilink")，是由电子间的电磁力与动量之间的相互作用决定的。

电磁场有很多种[数学描述](../Page/電磁場的數學表述.md "wikilink")。在[经典电磁学中](../Page/经典电磁学.md "wikilink")，电场用[欧姆定律中的](../Page/欧姆定律.md "wikilink")[電勢与](../Page/電勢.md "wikilink")[电流描述](../Page/电流.md "wikilink")，[磁場与](../Page/磁場.md "wikilink")[电磁感应和磁化强度相关](../Page/电磁感应.md "wikilink")，而[馬克士威方程組描述了由电场和磁场自身以及电荷和电流引起的电场和磁场的产生和交替](../Page/馬克士威方程組.md "wikilink")。

电磁学理论意义，特别是基于“媒介”中的传播的性质（[磁导率和](../Page/磁导率.md "wikilink")[电容率](../Page/电容率.md "wikilink")）确立的光速，推动了1905年[阿尔伯特·爱因斯坦的](../Page/阿尔伯特·爱因斯坦.md "wikilink")[狭义相对论的发展](../Page/狭义相对论.md "wikilink")。

虽然电磁力被认为是四大基本作用力之一，在高能量中[弱力和电磁力是统一的](../Page/弱相互作用.md "wikilink")。在宇宙的历史中的[夸克時期](../Page/夸克時期.md "wikilink")，[电弱力分割成电磁力和弱力](../Page/電弱交互作用.md "wikilink")。

## 电磁学与相对论

電磁學的基本方程式為麦克斯韦方程组，此方程組在經典力學的相對運動轉換（[伽利略变换](../Page/伽利略变换.md "wikilink")）下形式會變，在伽利略變換下，光速在不同慣性座標下會不同。保持麦克斯韦方程组形式不變的變換為[洛伦兹变换](../Page/洛伦兹变换.md "wikilink")，在此變換下，不同慣性座標下光速恆定。

二十世紀初[迈克耳孙-莫雷实验支持光速不變](../Page/迈克耳孙-莫雷实验.md "wikilink")，光速不變亦成為愛因斯坦的[狹義相對論的基石](../Page/狹義相對論.md "wikilink")。取而代之，洛伦兹变换亦成為較伽利略变换更精密的慣性座標轉換方式。

## 歷史

### 历史背景

[静电和](../Page/静电.md "wikilink")[静磁现象很早就被人类发现](../Page/静磁学.md "wikilink")，由于摩擦起电现象，英文中“[电](../Page/电.md "wikilink")”的语源来自[希腊文](../Page/希腊文.md "wikilink")“[琥珀](../Page/琥珀.md "wikilink")”一词。

远在[公元前](../Page/公元前.md "wikilink")2750年，[古埃及人就已经知道发电鱼](../Page/古埃及.md "wikilink")（electric
fish）会发出电击。这些鱼被称为“[尼罗河的雷使者](../Page/尼罗河.md "wikilink")”，是所有其它鱼的保护者。大约两千五百年之后，希腊人、罗马人，阿拉伯自然学者和阿拉伯医学者，才又出现关于发电鱼的记载。古代罗马医生Scribonius
Largus也在他的大作《Compositiones
Medicae》中，建议患有像痛风或头疼一类病痛的病人，去触摸电鳐，也许强力的电击会治愈他们的疾病。[阿拉伯人可能是最先了解闪电本质的族群](../Page/阿拉伯.md "wikilink")。他们也可能比其它族群都先认出电的其它来源。早于15世纪以前，阿拉伯人就创建了“[闪电](../Page/闪电.md "wikilink")”的阿拉伯字“raad”，并将这字用来称呼电鳐。

在古希腊及地中海区域的古老文化裏，很早就有文字记载，将琥珀棒与猫毛摩擦后，会吸引羽毛一类的物质。西元前600年左右，[古希腊的哲学家](../Page/古希腊.md "wikilink")[泰勒斯](../Page/泰勒斯.md "wikilink")（Thales,
640-546B.C.）做了一系列关于静电的观察。从这些观察中，他认为摩擦使琥珀变得磁性化。这与矿石像磁铁矿的性质迥然不同；磁铁矿天然地具有磁性\[4\]\[8\]。泰勒斯的见解并不正确。但后来，科学会证实磁与电之间的密切关系。

### 近代研究

1600年[英國醫生](../Page/英國.md "wikilink")[威廉·吉尔伯特發表了](../Page/威廉·吉尔伯特.md "wikilink")《論磁、磁飽和地球作為一個巨大的磁體》（Demagnete，magneticisque
corporibus et de magnomagnete
tellure）。他總結了前人對磁的研究，周密地討論了地磁的性質，記載了大量實驗，使磁學從經驗轉變為科學。書中他也記載了電學方面的研究。然而真正对电磁现象的系统研究则要等到十七世纪以后，并且静电学的研究要晚于静磁学，这是由于难以找到一个能产生稳定[静电场的方法](../Page/静电场.md "wikilink")，这种情况一直到1660年[摩擦起电机发明後才開始改變](../Page/摩擦起电机.md "wikilink")。十八世纪以前，人们一直采用这类摩擦起电机来产生研究静电场，代表人物如[本杰明·富兰克林](../Page/本杰明·富兰克林.md "wikilink")\[2\]，人们在这一时期主要了解到了[静电力的同性相斥](../Page/静电力.md "wikilink")、异性相吸的特性、[静电感应现象以及](../Page/静电感应.md "wikilink")[电荷守恒原理](../Page/电荷守恒.md "wikilink")。

[Charles_de_Coulomb.png](https://zh.wikipedia.org/wiki/File:Charles_de_Coulomb.png "fig:Charles_de_Coulomb.png")\]\]

### 静电学和库仑定律

[库仑定律是](../Page/库仑定律.md "wikilink")[静电学中的基本定律](../Page/静电学.md "wikilink")，其主要描述了静电力与[电荷](../Page/电荷.md "wikilink")[电量成正比](../Page/电量.md "wikilink")，与距离的平方反比关系。人们曾将静电力与在当时已享有盛誉的万有引力定律做类比，发现彼此在理论和实验上都有很多相似之处，包括实验观测到带电球壳内部的球体不会带电，这和有质量的球壳内部物体不会受到引力作用（由牛顿在理论上证明，是平方反比力的一个特征）的情形类似。其间苏格兰物理学家[约翰·罗比逊](../Page/约翰·罗比逊.md "wikilink")（1759年）\[3\]和英国物理学家[亨利·卡文迪什](../Page/亨利·卡文迪什.md "wikilink")（1773年）等人都进行过实验验证了静电力的平方反比律，然而他们的实验却迟迟不为人知。法国物理学家[夏尔·奥古斯丁·库仑於](../Page/夏尔·奥古斯丁·库仑.md "wikilink")1784年至1785年间进行了他著名的扭秤实验\[4\]，其实验的主要目的就是为了证实静电力的平方反比律，因为他认为“假说的前一部分无需证明”，也就是说他已经先验性地认为静电力必然和万有引力类似，和电荷电量成正比。扭秤的基本构造为：一根水平悬于细金属丝的轻导线两端分别置有一个带电小球A和一个与之平衡的物体P，而在实验中在小球A的附近放置同样大小的带电小球B，两者的静电力会在轻导线上产生[扭矩](../Page/力矩.md "wikilink")，从而使轻杆转动。通过校正悬丝上的旋钮可以将小球调回原先位置，则此时悬丝上的扭矩等于静电力产生的力矩。如此，两者之间的静电力可以通过测量这个扭矩、偏转角度和导线长度来求得。库仑的结论为：

库仑在其后的几年间也研究了[磁偶极子之间的作用力](../Page/磁偶极子.md "wikilink")，他也得出了磁力也具有平方反比律的结论。不过，他并未认识到静电力和静磁力之间有何内在联系，而且他一直将电力和磁力吸引和排斥的原因归结于假想的电流体和磁流体——具有正和负区别的，类似于“热质”一般的无质量物质。

静电力的平方反比律确定后，很多后续工作都是同万有引力做类比从而顺理成章的结果。1813年法国数学家、物理学家[西莫恩·德尼·泊松指出](../Page/西莫恩·德尼·泊松.md "wikilink")[拉普拉斯方程也适用于静电场](../Page/拉普拉斯方程.md "wikilink")，从而提出[泊松方程](../Page/泊松方程.md "wikilink")；其他例子还包括静电场的[格林函数](../Page/格林函数.md "wikilink")（[乔治·格林](../Page/乔治·格林.md "wikilink")，1828年）和[高斯定理](../Page/高斯定理.md "wikilink")（[卡尔·高斯](../Page/卡尔·高斯.md "wikilink")，1839年）。

[Ohm3.gif](https://zh.wikipedia.org/wiki/File:Ohm3.gif "fig:Ohm3.gif")\]\]

### 对稳恒电流的研究

十八[世纪末](../Page/世纪.md "wikilink")，[意大利生理学家](../Page/意大利.md "wikilink")[路易吉·伽伐尼发现蛙腿肌肉接触金属刀片时会发生痉挛](../Page/路易吉·伽伐尼.md "wikilink")，他其后在论文中认为生物中存在着一种所谓“神经电流”。意大利物理学家[亚历山德罗·伏打对这种观点并不赞同](../Page/亚历山德罗·伏打.md "wikilink")，他对这种现象进行研究后认为这不过是外部电流的作用，而蛙腿[肌肉只是起到了导体的连接作用](../Page/肌肉.md "wikilink")。1800年，伏打将锌片和铜片夹在用盐水浸湿的纸片中，得到了很强的电流，这称作伏打电堆；而将锌片和铜片浸入盐水或酸溶液中也能得到相同的效果，这称作伏打电池。伏打电堆和电池的发明为研究稳恒电流创造了条件。

1826年，[德国物理学家](../Page/德国.md "wikilink")[格奥尔格·欧姆从](../Page/格奥尔格·欧姆.md "wikilink")[傅立叶对](../Page/傅立叶.md "wikilink")[热传导规律的研究中受到启发](../Page/热传导.md "wikilink")，在傅立叶的热传导理论中，导热杆中两点的热流量正比于这两点之间的温度差<ref name="ohm">


[be-x-old:Клясычная
электрадынаміка](../Page/be-x-old:Клясычная_электрадынаміка.md "wikilink")
[bn:তড়িচ্চুম্বকত্ব](../Page/bn:তড়িচ্চুম্বকত্ব.md "wikilink")
[uk:Класична
електродинаміка](../Page/uk:Класична_електродинаміка.md "wikilink")

[Category:物理学分支](../Category/物理学分支.md "wikilink")
[电磁学](../Category/电磁学.md "wikilink")

1.
2.
3.
4.