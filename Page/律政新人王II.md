《**律政新人王II**》（）是[香港](../Page/香港.md "wikilink")[電視廣播有限公司拍攝製作的時裝](../Page/電視廣播有限公司.md "wikilink")[劇集](../Page/劇集.md "wikilink")，全劇共20集，由[陳鍵鋒](../Page/陳鍵鋒.md "wikilink")、[馬國明](../Page/馬國明.md "wikilink")、[官恩娜及](../Page/官恩娜.md "wikilink")[李詩韻領銜主演](../Page/李詩韻.md "wikilink")，監製[關永忠](../Page/關永忠.md "wikilink")。此劇為《律政新人王系列》第二輯。

## 演員表

### TB\&B律師行

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係/暱稱</strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/李子雄.md" title="wikilink">李子雄</a></strong></p></td>
<td><p><strong>崔正平</strong></p></td>
<td><p><strong>Brandon</strong><br />
錢韶涵之夫<br />
辛萬軍之<a href="../Page/師傅.md" title="wikilink">師傅</a></p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/陳秀珠.md" title="wikilink">陳秀珠</a></strong></p></td>
<td><p><strong>錢韶涵</strong></p></td>
<td><p><strong>Brenda</strong><br />
崔正平之妻<br />
辛萬軍之師母</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/陳鍵鋒.md" title="wikilink">陳鍵鋒</a></strong></p></td>
<td><p><strong>卓偉名</strong></p></td>
<td><p><strong>Vincent</strong><br />
蔣思嘉之前男友<br />
鄭彩玉之夫<br />
於第17集偷拍孫伯滔的犯罪證據<br />
於第19集因妨害司法公正而被辛萬軍揭發，後被永久吊銷律師牌照<br />
於第19集被簡明慧殺害不遂，但後意外撞傷昏迷，甦醒之後失憶<br />
於第20集被控傷害孫伯滔，後被判無罪釋放<br />
參見上集<strong><a href="../Page/律政新人王.md" title="wikilink">律政新人王</a></strong></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/朱婉儀.md" title="wikilink">朱婉儀</a></p></td>
<td><p>周小燕</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/李彩寧.md" title="wikilink">李彩寧</a></p></td>
<td><p>董冰冰</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/張美妮.md" title="wikilink">張美妮</a></p></td>
<td><p>張美倩</p></td>
<td><p>Cindy</p></td>
</tr>
</tbody>
</table>

### 辛家

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係/暱稱</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/馮素波.md" title="wikilink">馮素波</a></p></td>
<td><p>白　薇</p></td>
<td><p>辛萬金、辛萬軍之嫲嫲<br />
辛家寶之太嫲嫲</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/陳國邦.md" title="wikilink">陳國邦</a></strong></p></td>
<td><p><strong>辛萬金</strong></p></td>
<td><p>白薇之長孫<br />
辛萬軍之兄<br />
辛家寶之大伯<br />
丁麗娜之夫</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳琪_(香港藝員).md" title="wikilink">陳　琪</a></p></td>
<td><p>丁麗娜</p></td>
<td><p>辛萬金之妻<br />
被李啟賢欺騙</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/馬國明.md" title="wikilink">馬國明</a></strong></p></td>
<td><p><strong>辛萬軍</strong></p></td>
<td><p><strong>MK Sun</strong><br />
<a href="../Page/大律師.md" title="wikilink">大律師</a><br />
白薇之孫<br />
辛萬金之弟<br />
孫俐俐之夫<br />
辛家寶之養父</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/冼迪琦.md" title="wikilink">冼迪琦</a></p></td>
<td><p>辛家寶</p></td>
<td><p>蘇敏兒之女<br />
辛萬軍之養女</p></td>
</tr>
</tbody>
</table>

### 孫家

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係/暱稱</strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/郭峰_(演員).md" title="wikilink">郭　峰</a></strong></p></td>
<td><p><strong>孫伯滔</strong></p></td>
<td><p>地產發展商<br />
孫俐俐之父<br />
簡明慧之男友<br />
辛萬軍之岳父<br />
於第18集被警察抓走<br />
於第19集被控告殺害葉志開，後被判無罪釋放<br />
於第19集被簡明慧誤傷，後甦醒<br />
於第20集因隱瞞簡明慧的罪行而被判妨害司法公正，其後被判緩刑</p></td>
</tr>
<tr class="odd">
<td><p><strong><a href="../Page/官恩娜.md" title="wikilink">官恩娜</a></strong></p></td>
<td><p><strong>孫俐俐</strong></p></td>
<td><p><strong>Lily</strong><br />
大律師<br />
孫伯滔之女<br />
辛萬軍之妻<br />
簡明慧之學妹</p></td>
</tr>
</tbody>
</table>

### 鄭家

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係/暱稱</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/王青.md" title="wikilink">王　青</a></p></td>
<td><p>鄭錦棠</p></td>
<td><p>鄭彩玉之父<br />
朱淑好之夫<br />
職業商場保安</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/白茵.md" title="wikilink">白　茵</a></p></td>
<td><p>朱淑好</p></td>
<td><p>鄭彩玉之母<br />
鄭錦棠之妻</p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/李詩韻.md" title="wikilink">李詩韻</a></strong></p></td>
<td><p><strong>鄭彩玉</strong></p></td>
<td><p>鄭錦棠、朱淑好之女<br />
卓偉名之妻<br />
於第17集被人敲擊腦部導致昏迷<br />
於第19集醒过来</p></td>
</tr>
</tbody>
</table>

### 其他演員

<table>
<tbody>
<tr class="odd">
<td><p><strong>演員</strong></p></td>
<td><p><strong>角色</strong></p></td>
<td><p><strong>關係/暱稱</strong></p></td>
</tr>
<tr class="even">
<td><p><strong><a href="../Page/朱慧敏.md" title="wikilink">朱慧敏</a></strong></p></td>
<td><p><strong>簡明慧</strong></p></td>
<td><p>Noel<br />
孫俐俐之學姊<br />
孫伯滔之女友<br />
於第18集誤殺葉志開<br />
於第19集殺卓偉名不遂，但誤傷孫伯滔<br />
於第20集自首，其後被判誤殺罪成立，被判入獄</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/廖碧兒.md" title="wikilink">廖碧兒</a></p></td>
<td><p>蔣思嘉</p></td>
<td><p>卓偉名之前女友<br />
參見上集《<strong><a href="../Page/律政新人王.md" title="wikilink">律政新人王</a></strong>》<br />
（特別客串）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳山聰.md" title="wikilink">陳山聰</a></p></td>
<td><p>畢正義</p></td>
<td><p>辛萬軍之好友</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/敖嘉年.md" title="wikilink">敖嘉年</a></p></td>
<td><p>范振庭</p></td>
<td><p>孫俐俐之前男友</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳自瑤.md" title="wikilink">陳自瑤</a></p></td>
<td><p>徐思樂</p></td>
<td><p><strong>Stephy</strong><br />
孫俐俐之好友<br />
Zenbi之徒弟</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/甄志強.md" title="wikilink">甄志強</a></p></td>
<td><p>彭宇琛</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/趙永洪.md" title="wikilink">趙永洪</a></p></td>
<td><p>William</p></td>
<td><p>孫伯滔助手</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/楊英偉.md" title="wikilink">楊英偉</a></p></td>
<td><p>李啟賢</p></td>
<td><p>丁麗娜之前男友，曾偷過酒佬吧錢</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/秦煌.md" title="wikilink">秦　煌</a></p></td>
<td><p>王天保</p></td>
<td><p><a href="../Page/茶餐廳.md" title="wikilink">茶餐廳老闆</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/胡楓.md" title="wikilink">胡　楓</a></p></td>
<td><p>潘永良</p></td>
<td><p>崔正平之師傅（客串第19-20集）</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/郭卓樺.md" title="wikilink">郭卓樺</a></p></td>
<td><p>葉志開</p></td>
<td><p>辛萬軍之好友<br />
於第18集被簡明慧誤殺</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/張智軒.md" title="wikilink">張智軒</a></p></td>
<td><p>鄭志高</p></td>
<td><p>Marco<br />
辛萬軍之好友</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陳堃.md" title="wikilink">陳　堃</a></p></td>
<td><p>戴健威</p></td>
<td><p>辛萬軍之好友<br />
月之男友</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/卓姿.md" title="wikilink">卓　姿</a></p></td>
<td><p>月</p></td>
<td><p>戴健威之女友</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/羅貫峰.md" title="wikilink">羅貫峰</a></p></td>
<td><p>洪日飛</p></td>
<td><p>辛萬軍之好友</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黃文標.md" title="wikilink">黃文標</a></p></td>
<td><p>財</p></td>
<td></td>
</tr>
<tr class="even">
<td><p><a href="../Page/林遠迎.md" title="wikilink">林遠迎</a></p></td>
<td><p>熟　客</p></td>
<td></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/鄺佐輝.md" title="wikilink">鄺佐輝</a></p></td>
<td><p>吳楚雄</p></td>
<td><p>裁判官</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/陸詩韻.md" title="wikilink">陸詩韻</a></p></td>
<td><p>副校長</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/黃梓瑋.md" title="wikilink">黃梓瑋</a></p></td>
<td><p>寶老師</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/彭冠中.md" title="wikilink">彭冠中</a></p></td>
<td><p>鍾正甫</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/何慶輝.md" title="wikilink">何慶輝</a></p></td>
<td><p>甫助手</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/高俊文.md" title="wikilink">高俊文</a></p></td>
<td><p>蓋錦輝</p></td>
<td><p>裁判官</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/楊卓娜.md" title="wikilink">楊卓娜</a></p></td>
<td><p>Michelle</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/楊瑞麟.md" title="wikilink">楊瑞麟</a></p></td>
<td><p>彭一全</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/歐瑞偉.md" title="wikilink">歐瑞偉</a></p></td>
<td><p>丁榮炳</p></td>
<td><p>的士司機</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/關伊彤.md" title="wikilink">關伊彤</a></p></td>
<td><p>方嘉琳</p></td>
<td><p>Kelly（客串第2集）</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/姚瑩瑩.md" title="wikilink">姚瑩瑩</a></p></td>
<td><p>Zenbi</p></td>
<td><p>Stephy之師父</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/沈可欣.md" title="wikilink">沈可欣</a></p></td>
<td><p>沈德如</p></td>
<td><p>裁判官<br />
Elsa<br />
范振庭之妻</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/魯振順.md" title="wikilink">魯振順</a></p></td>
<td><p>裁判官</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/王維德.md" title="wikilink">王維德</a></p></td>
<td><p>主控官</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳狄克.md" title="wikilink">陳狄克</a></p></td>
<td><p>管理員</p></td>
<td><p>-</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/廖麗麗.md" title="wikilink">廖麗麗</a></p></td>
<td><p>連</p></td>
<td><p>-</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/吳幸美.md" title="wikilink">吳幸美</a></p></td>
<td><p>蔣　泳</p></td>
<td><p>第7集</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/潘冠霖.md" title="wikilink">潘冠霖</a></p></td>
<td><p>Mary</p></td>
<td><p><a href="../Page/啤酒妹.md" title="wikilink">啤酒妹</a></p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/張雪芹.md" title="wikilink">張雪芹</a></p></td>
<td><p>阿珍</p></td>
<td><p>醫院護士</p></td>
</tr>
<tr class="even">
<td></td>
<td><p>蘇敏兒</p></td>
<td><p>蘇敏詩之妹<br />
辛家寶之母<br />
辛萬軍之前女友</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳伶俐.md" title="wikilink">陳伶俐</a></p></td>
<td><p>蘇敏詩</p></td>
<td><p>蘇敏兒之姊<br />
辛家寶之姨媽<br />
林韋章之妻</p></td>
</tr>
<tr class="even">
<td><p><a href="../Page/張松枝.md" title="wikilink">張松枝</a></p></td>
<td><p>林韋章</p></td>
<td><p>蘇敏詩之夫<br />
辛家寶之姨丈，實為生父</p></td>
</tr>
<tr class="odd">
<td><p><a href="../Page/陳霽平.md" title="wikilink">陳霽平</a></p></td>
<td><p>歐陽丹</p></td>
<td><p>林韋章之女友</p></td>
</tr>
</tbody>
</table>

## 收視

### 香港收视

以下為本劇於[香港](../Page/香港.md "wikilink")[無綫電視翡翠台之收視紀錄](../Page/無綫電視翡翠台.md "wikilink")：

| 週次 | 集數    | 日期                 | 平均收視                             | 最高收視                             |
| -- | ----- | ------------------ | -------------------------------- | -------------------------------- |
| 1  | 1-5   | 2007年12月24日-12月28日 | 26[點](../Page/收視點.md "wikilink") |                                  |
| 2  | 6-9   | 2008年1月1日-1月4日     | 29[點](../Page/收視點.md "wikilink") | 31[點](../Page/收視點.md "wikilink") |
| 3  | 10-14 | 2008年1月7日-1月11日    | 31[點](../Page/收視點.md "wikilink") | 33[點](../Page/收視點.md "wikilink") |
| 4  | 15-20 | 2008年1月14日-1月18日   | 31[點](../Page/收視點.md "wikilink") | 37[點](../Page/收視點.md "wikilink") |

此劇平均收視為29.4點

### 广州收视

| 集数    | CSM  | AGB   |
| ----- | ---- | ----- |
| CSM省网 | 单集最高 | CSM市网 |
| 1-5   | 8.45 | 10.1  |
| 6-9   | 9.19 | 9.97  |
| 10-14 | 9.09 | 9.43  |
| 15-20 | 9.44 | 10.49 |

## 記事

  - 2007年12月31日：由於21:00-22:30播映特備節目慶祝[高清翡翠台啟播](../Page/高清翡翠台.md "wikilink")，當晚本劇暫停播映。
  - 2008年1月18日：此劇於20:30-22:35播映兩小時大結局（香港首播版第19集，原裝版第19-20集）。

## 外部連結

  - [無綫電視官方網頁 -
    律政新人王II](https://web.archive.org/web/20071223082107/http://tvcity.tvb.com/drama/survivor_s_law_ii/)

## 電視節目的變遷

|align="center" colspan="5"|[同事三分親](../Page/同事三分親.md "wikilink")
\-2008年8月7日 |- |align="center"
colspan="2"|[建築有情天](../Page/建築有情天.md "wikilink")
\-12月28日 |align="center"
colspan="3"|[野蠻奶奶大戰戈師奶](../Page/野蠻奶奶大戰戈師奶.md "wikilink")
2008年1月1日- |- |align="center" colspan="1"|**上一套：**
[兩妻時代](../Page/兩妻時代.md "wikilink")
\-12月21日 |align="center"
colspan="3"|**翡翠台/高清翡翠台第三綫劇集（[2007](../Page/翡翠台電視劇集列表_\(2007年\)#第三線劇集.md "wikilink")-[2008](../Page/翡翠台電視劇集列表_\(2008年\)#第三線劇集.md "wikilink")）**
**律政新人王II**
12月24日-1月18日 |align="center" colspan="1"|**下一套：**
[秀才愛上兵](../Page/秀才愛上兵.md "wikilink")
1月21日-

[Category:2007年無綫電視劇集](../Category/2007年無綫電視劇集.md "wikilink")
[Category:2008年無綫電視劇集](../Category/2008年無綫電視劇集.md "wikilink")
[Category:2007年AOD電視劇集](../Category/2007年AOD電視劇集.md "wikilink")
[律](../Category/無綫電視劇集系列.md "wikilink")
[Category:無綫電視律政劇集](../Category/無綫電視律政劇集.md "wikilink")
[Category:無綫電視2000年代背景劇集](../Category/無綫電視2000年代背景劇集.md "wikilink")
[Category:電視劇續集](../Category/電視劇續集.md "wikilink")