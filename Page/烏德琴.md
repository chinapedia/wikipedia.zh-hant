**烏德琴**，又称**厄烏德琴**（[英文](../Page/英文.md "wikilink")**Oud**；[阿拉伯文](../Page/阿拉伯文.md "wikilink")**عود**；[波斯語](../Page/波斯語.md "wikilink")**بربط**；[土耳其語](../Page/土耳其語.md "wikilink")**ud**或**ut**；[希臘語](../Page/希臘語.md "wikilink")**ούτι**；[亞美尼亞語](../Page/亞美尼亞語.md "wikilink")**ուդ**），是[中东及](../Page/中东.md "wikilink")[非洲东部及北部使用的一种传统](../Page/非洲.md "wikilink")[弦乐器](../Page/弦乐器.md "wikilink")，有“中东乐器之王”之称。

烏德琴被认为是[欧洲](../Page/欧洲.md "wikilink")[魯特琴](../Page/魯特琴.md "wikilink")。由于现代[吉它延自鲁特琴之一种类](../Page/吉它.md "wikilink")，是以烏德琴也被视为吉它之祖。

[中亚国家如](../Page/中亚.md "wikilink")[阿拉伯](../Page/阿拉伯.md "wikilink")、[土耳其](../Page/土耳其.md "wikilink")、[伊朗](../Page/伊朗.md "wikilink")、[亚美尼亚](../Page/亚美尼亚.md "wikilink")、[伊拉克](../Page/伊拉克.md "wikilink")、[敘利亞](../Page/敘利亞.md "wikilink")、[阿塞拜疆的传统音乐里乌德琴占有重要地位](../Page/阿塞拜疆.md "wikilink")。在[北非](../Page/北非.md "wikilink")[国家如](../Page/国家.md "wikilink")[埃及](../Page/埃及.md "wikilink")、[苏丹](../Page/苏丹.md "wikilink")、[摩洛哥](../Page/摩洛哥.md "wikilink")、[突尼斯](../Page/突尼斯.md "wikilink")、[阿尔及利亚等也是人们所喜爱的](../Page/阿尔及利亚.md "wikilink")[乐器之一](../Page/乐器.md "wikilink")。

## 名称

[Azerbaijani_musical_instrument_ud_in_Heydar_Aliyev_Center.jpg](https://zh.wikipedia.org/wiki/File:Azerbaijani_musical_instrument_ud_in_Heydar_Aliyev_Center.jpg "fig:Azerbaijani_musical_instrument_ud_in_Heydar_Aliyev_Center.jpg")
一般认为，烏德琴“Oud”及[魯特琴](../Page/魯特琴.md "wikilink")“lute”都源自一个阿拉伯词**العود**，此词音为al'ud，是“这（那）[木头](../Page/木.md "wikilink")”的意思。这名称的由来有各种见解，Gianfranco
Lotti认为此称呼带有贬意，这是因为早期[伊斯兰教轻视乐器的缘故](../Page/伊斯兰教.md "wikilink")。Eckhard
Neubauer则认为'ud可能是阿拉伯人对波斯文“rud”的翻译，“rud”乃[弦或](../Page/弦.md "wikilink")[弦乐器之意](../Page/弦乐器.md "wikilink")。

土耳其语称烏德琴为“ud”，阿拉伯语al'ud的前缀“al”是“这或那”的意思，“al”被土耳其人弃用，将“Oud”（由[阿拉伯字母eyn](../Page/阿拉伯字母.md "wikilink")、**“و”**waw、及dal组成）改成“ud”来称呼此乐器，会如此改动据说是由于土耳其語中没有阿拉伯字母eyn的发音。

## 历史

烏德琴的由来，至今说法不一。

根据一位古代伊斯兰科学家[法拉比研究](../Page/法拉比.md "wikilink")，烏德琴是由[创世纪中的传说人物](../Page/创世纪.md "wikilink")，[亚当的第六代孙](../Page/亚当.md "wikilink")[拉麦](../Page/拉麦.md "wikilink")所创，传说中Lamech悲伤的将其儿子的尸体吊在树上，而第一个烏德琴的创造就是来自其儿子的骨骼形状。

另有传说是[上帝赐于亚当儿子](../Page/上帝.md "wikilink")[该隐的后裔](../Page/该隐.md "wikilink")[发明](../Page/发明.md "wikilink")[乐器的本领](../Page/乐器.md "wikilink")，结果Tubal发明了[鼓](../Page/鼓.md "wikilink")、Lamak发明了乌德琴、Dilal发明了[竖琴](../Page/竖琴.md "wikilink")。

根据[伊朗的](../Page/伊朗.md "wikilink")[巴爾巴特琴大师](../Page/巴爾巴特琴.md "wikilink")，Behruzinia的说法，波斯的巴爾巴特琴是烏德琴的前身，阿拉伯人改造了巴爾巴特琴，然后将之称为烏德琴。

古代的[突厥人也有相似的乐器](../Page/突厥.md "wikilink")，叫火不思，这种乐器被认为有神奇的力量，依照一些突厥人建立的碑之碑铭记载，在突厥的军乐队中有使用，之后欧洲军队也有使用此乐器。被认为是突厥后代的土耳其人用的烏德琴与阿拉伯的烏德琴，在造型及弹奏方法上都有很大差异。在今天，[亞美尼亞及](../Page/亞美尼亞.md "wikilink")[希腊用土耳其的烏德琴和音调](../Page/希腊.md "wikilink")。

依Cinuçen
Tanrıkorur称，烏德琴源自一种中亚[突厥人的乐器](../Page/突厥人.md "wikilink")[火不思](../Page/火不思.md "wikilink")，为烏德琴加上弦的也是突厥人，今天的烏德琴已与古代造型上有很大差别。

至今所发现最早的烏德琴图像来自[美索不達米亞南部](../Page/美索不達米亞.md "wikilink")，是[烏魯克时期的作品](../Page/烏魯克时期.md "wikilink")（公元前4000年到前3100年），于一个已有5000年的[圓筒印章上面](../Page/圓筒印章.md "wikilink")，这圓筒印章是由Dr.
Dominique
Collon发现，现存于[大英博物館中](../Page/大英博物館.md "wikilink")，图像是一位在船上蹲着的女性，在用右手弹奏烏德琴。

烏德琴在美索不達米亞地域出现多次，在[非洲](../Page/非洲.md "wikilink")，[古埃及的](../Page/古埃及.md "wikilink")[第十八王朝开始就已发现烏德琴的踪迹](../Page/埃及第十八王朝.md "wikilink")，以及各种烏德琴的变体。这些例子在大英博物館以及[美国](../Page/美国.md "wikilink")[纽约的](../Page/纽约.md "wikilink")[大都會藝術博物館收藏的泥板及](../Page/大都會藝術博物館.md "wikilink")[莎草紙上都可看到](../Page/莎草紙.md "wikilink")。

烏德琴及其变体的踪迹在[地中海及](../Page/地中海.md "wikilink")[波斯湾地区的古代文明中都可见到](../Page/波斯湾.md "wikilink")，包括[蘇美爾](../Page/蘇美爾.md "wikilink")、[阿卡德](../Page/阿卡德.md "wikilink")、[波斯](../Page/波斯.md "wikilink")、[巴比伦](../Page/巴比伦.md "wikilink")、[亞述](../Page/亞述.md "wikilink")、[古希腊](../Page/古希腊.md "wikilink")、[古埃及及](../Page/古埃及.md "wikilink")[羅馬帝國](../Page/羅馬帝國.md "wikilink")。

中国的琵琶据说是也是由烏德琴演变而来。[东汉时期](../Page/东汉.md "wikilink")[刘熙所著之](../Page/刘熙.md "wikilink")《[释名乐器](../Page/释名乐器.md "wikilink")》中的记载就有“批把本出于胡中，马上所鼓也。推手前曰批，引手后曰把。”之句。说明琵琶是外族的产物。

欧洲乐器魯特琴被认为是从烏德琴衍生出来的乐器。欧洲人是在[十字军东征期间接触到烏德琴的](../Page/十字军东征.md "wikilink")，当时有人将烏德琴带回欧洲，魯特琴在[法文中是Luth](../Page/法文.md "wikilink")、英文Lute、德文Laute、意大利文Liuto、荷兰文Luit、在西班牙文中是Alaud而英文中luthier这词，即拨弦乐器制作匠，也是来自Luth。

## 构造

### 特色

**无[琴格](../Page/琴格.md "wikilink")**：烏德琴和其他[彈撥樂器不一样](../Page/彈撥樂器.md "wikilink")，就是[琴頸上没有琴格](../Page/琴頸.md "wikilink")，这使得弹奏者能更好的表现[滑音及](../Page/滑音.md "wikilink")[顫音技巧](../Page/顫音_\(樂器技巧\).md "wikilink")。没有琴格也让弹奏者能弹出[瑪卡姆](../Page/瑪卡姆.md "wikilink")[調式中的](../Page/調式.md "wikilink")[微音](../Page/微分音音樂.md "wikilink")。

烏德琴没有琴格是后来的改良，在公元1100年时烏德琴还有琴格，到1300年时琴格就消失了。烏德琴去除琴格反映了中东音乐的发展较重视[装饰音](../Page/装饰音.md "wikilink")。

**[弦](../Page/弦.md "wikilink")**：多数烏德琴有11条弦。10条弦中被分成常一起弹奏的5对[双弦](../Page/双弦.md "wikilink")。第11条弦，最下面的一条单独弹奏。弹奏烏德琴所用的力度比起现代[吉它轻很多](../Page/吉它.md "wikilink")。

**[弦轴箱](../Page/弦轴箱.md "wikilink")**：烏德琴的弦轴箱自琴頸向后弯曲45到90度。

**琴身**：烏德琴的琴身背部有个状似半个[西瓜的突出面](../Page/西瓜.md "wikilink")，不像吉它背部是平的，这个设计可让烏德琴产生[共鸣](../Page/共振.md "wikilink")，可弹出复合的调子。

**[音孔](../Page/音孔.md "wikilink")**：烏德琴有1到3个[音孔](../Page/音孔.md "wikilink")。

### 撥子

烏德琴的[撥子](../Page/撥子.md "wikilink")[长度比](../Page/长度.md "wikilink")[食指稍微长一点](../Page/食指.md "wikilink")。阿拉伯人称之为*reeshe*或*risha*，土耳其人称之为*mızrap*。传统上，制造烏德琴撥子的材料为[老鹰的](../Page/老鹰.md "wikilink")[羽毛及](../Page/羽毛.md "wikilink")[乌龟的](../Page/乌龟.md "wikilink")[壳](../Page/壳.md "wikilink")，在今天，[塑料的撥子比较普遍](../Page/塑料.md "wikilink")，这是因为塑料撥子即适用又较便宜。

烏德琴手对撥子的[质量要求很严格](../Page/质量.md "wikilink")，通常是自己制造的，材料来自其他塑料物品。烏德琴手非常照顾撥子，会以[沙纸磨去尖锐的边缘](../Page/沙纸.md "wikilink")，尽量让撥子能够撥出最好的[音色](../Page/音色.md "wikilink")。

## 地区种类

以下为各种不同地区的烏德琴种类，这些烏德琴在形状及[调律上有很大的不同](../Page/调律.md "wikilink")。

### 阿拉伯烏德琴

  - 敘利亞烏德琴：较大、较长、[音高较低](../Page/音高.md "wikilink")。
  - 伊拉克烏德琴：（Munir
    Bechir形）：大小与敘利亞烏德琴相似。特色是有浮动的[弦马](../Page/弦马.md "wikilink")，这设计可让之集中发出中度的[頻率](../Page/頻率.md "wikilink")，使得其发出的声音类似吉它。这种烏德琴是伊拉克烏德琴名师Munir
    Bechir设计的。
  - 埃及烏德琴：造型与敘利亞烏及伊拉克烏德琴相似，但琴身更似[梨形](../Page/梨.md "wikilink")。音高稍微不同。只有最低位置的一对弦才达到调律‘G’。埃及烏德琴的装饰相对而言比较华丽。

### 土耳其烏德琴

土耳其语：“ud”，包括在希腊及亞美尼亞使用的类型，在希腊叫“outi”。体形比较小、琴頸比较短、音高较高、[音色较明亮](../Page/音色.md "wikilink")。

### 波斯烏德琴（巴爾巴特琴）

体形小过阿拉伯烏德琴，与阿拉伯烏德琴的音调不一样，音高比较高，与土耳其烏德琴相似但体形较小。

### Qadim烏德琴

一种[北非的烏德琴](../Page/北非.md "wikilink")，已经没有人使用。

### 非烏德琴乐器

希腊乐器Laouto和Lavta，虽然看起来与烏德琴相似，但在弹法上有极大不同，而且都有琴格。其源流是来自[拜占庭鲁德琴](../Page/拜占庭鲁德琴.md "wikilink")（Byzantine
lutes）。Laouto是一种[克里特岛人使用的乐器](../Page/克里特岛.md "wikilink")。

## 各种乌德琴的调律

乌德琴有各种调律。以下列出的调律是依位于最下面的单弦到最上面的双弦排列。

### 阿拉伯乌德琴调律

  - **G A D G C F**，这是最常用的音高组合。
  - **D G A D G C**
  - **C F A D G C**
  - **C E A D G C**
  - **F A D G C F**

### 土耳其烏德琴及突厥巴爾巴特琴调律

  - 老式土耳其古典调律：**A D E A D G**
  - 新式土耳其古典调律：**F\# B E A D G**
  - 土耳其\\亞美尼亞式调律：**E A B E A D**
  - 土耳其\\亞美尼亞式调律变体：**C\# F\# B E A D**
  - 标准突厥调律：**D E A D G C**

## 注释

1.  一种欧洲弦乐器。

2.  Lamech是亚当儿子[该隐的后裔](../Page/该隐.md "wikilink")。

3.  Dilal是[诺亚的](../Page/诺亚.md "wikilink")[女儿](../Page/女儿.md "wikilink")。

4.  [土耳其的烏德琴名师](../Page/土耳其.md "wikilink")，生卒为1938年—2000年。

5.  Kopuz为突厥语，到现代，[柯爾克孜族还有使用此乐器](../Page/柯爾克孜族.md "wikilink")。

6.  各种乌德琴的调律参考[Lark in the
    Morning](http://larkinthemorning.com/article.asp?AI=6&bhcd2=1145737318)和[Oud
    Cafe](http://www.oudcafe.com/stringing_and_tuning.htm)。

## 参考资料

  - [吉它发展史（一）](https://web.archive.org/web/20071008090900/http://www.guitarra.cn/_culture/content.asp?ID=251)

[Category:亞洲樂器](../Category/亞洲樂器.md "wikilink")
[Category:有頸琉特琴類](../Category/有頸琉特琴類.md "wikilink")
[Category:撥弦樂器](../Category/撥弦樂器.md "wikilink")