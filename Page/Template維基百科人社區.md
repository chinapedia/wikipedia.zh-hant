<includeonly>} | width = 13em | contentclass = plainlist | contentstyle
= padding:2px; text-align:left | titlestyle = font-size: 120% |
liststyle = padding:2px; text-align:left | listtitlestyle = padding:
1px; padding-left: 6px; padding-top: 4px; text-align:left

| heading1 = | list1name = 维基人列表 | list1title =  | list1 =

  - [按用戶名](../Page/Wikipedia:维基百科人/按用户名排序.md "wikilink")
  - [按語言](../Page/Wikipedia:維基百科人/按語言排序.md "wikilink")
  - [按加入時間](../Page/Wikipedia:維基百科人/按加入時間排序.md "wikilink")

-----

  - [活跃的维基人](../Page/Wikipedia:活跃的维基人.md "wikilink")
  - [即时联系维基人](../Page/Wikipedia:即时联系维基人.md "wikilink")
  - [維基百科的社群討論](../Page/Wikipedia:維基百科的社群討論.md "wikilink")
  - [邮件列表](../Page/Wikipedia:邮件列表.md "wikilink")
  - [管理员](../Page/Wikipedia:管理员.md "wikilink")
  - [维基专家与大师](../Page/Wikipedia:维基奖励授予记录.md "wikilink")

-----

| heading2 = | list2name = 工作小組 | list2title =
[工作小組](../Page/Wikipedia:维基工作小组.md "wikilink") | list2 =

| heading3 = | list3name = 專題 | list3title =
[專題](../Page/Wikipedia:专题委员会.md "wikilink") | list3 =
中文維基百科共有項[專題](../Page/Template:维基专题列表.md "wikilink")

| heading4 = | list4name = 主題 | list4title =
[主題](../Page/Wikipedia:主題.md "wikilink") | list4 =
中文維基百科共有項[主題首頁](../Page/Wikipedia:主題.md "wikilink")

| heading5 = | list5name = 各地佈告板 | list5title =
[各地佈告板](../Page/Wikipedia:各地維基人佈告板.md "wikilink") | list5 =

| below = [变更用户名](../Page/Wikipedia:更改用户名.md "wikilink")

| navbarfontstyle = font-size: 70%
}}</includeonly><noinclude></noinclude>