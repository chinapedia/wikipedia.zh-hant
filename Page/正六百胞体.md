[cell600-4dpolytope.png](https://zh.wikipedia.org/wiki/File:cell600-4dpolytope.png "fig:cell600-4dpolytope.png")\]\]
[几何学中](../Page/几何学.md "wikilink")，**正六百胞体(Hexacosichoron)**是[四维凸正多胞体](../Page/四维凸正多胞体.md "wikilink")，[施莱夫利符号是](../Page/施莱夫利符号.md "wikilink"){3,3,5}，有時候会视为[正二十面体的四维类比](../Page/正二十面体.md "wikilink")。

正六百胞体的边界有600个[正四面体胞](../Page/正四面体.md "wikilink")、1200个[正三角形面](../Page/正三角形.md "wikilink")、720条边和120个顶点。每一顶点有20个正四面体相接。

## 几何性质

正六百胞体的[对偶多胞体是](../Page/对偶多胞体.md "wikilink")[正一百二十胞体](../Page/正一百二十胞体.md "wikilink")。
正六百胞体的[顶点形是](../Page/顶点形.md "wikilink")[正二十面体](../Page/正二十面体.md "wikilink")。
边长为a的正六百胞体超体积为\(\frac{50+25\sqrt{5}}{4}a^4\)，表体积为50√2a<sup>3</sup>。

以原点为中心，边长为 1/φ 的正六百胞体（其中φ =
(1+√5)/2是[黃金比例](../Page/黃金比例.md "wikilink")），頂点坐标如下：16个顶点形式如下

  -
    (±½,±½,±½,±½)，

8个顶点从下列坐标不同排列得出

  -
    (0,0,0,±1)，

剩下96个顶点是下列坐标的[偶置换](../Page/偶置换.md "wikilink")

  -
    ½(±1,±φ,±1/φ,0)。

如果一个正六百胞体的棱长为1，则其外接超球半径为\(\begin{smallmatrix}\frac{\sqrt{5}+1}{2} \approx1.618\end{smallmatrix}\)即[黄金分割比](../Page/黄金分割比.md "wikilink")；其外中交超球(经过正六百胞体每条棱的中点)半径为\(\begin{smallmatrix}\frac{\sqrt{2\sqrt{5}+5}}{2} \approx1.538\end{smallmatrix}\)
;其内中交超球（经过正六百胞体每个面的中心）半径为\(\begin{smallmatrix}\frac{\sqrt{15}+3\sqrt{3}}{6} \approx1.512\end{smallmatrix}\)
;其内切超球半径为\(\begin{smallmatrix}\frac{\sqrt{10}+2\sqrt{2}}{4} \approx1.498\end{smallmatrix}\)
。

注意到首16个顶点构成[超正方体](../Page/超正方体.md "wikilink")，次8个构成[正十六胞体](../Page/正十六胞体.md "wikilink")。这24个顶点一起构成[正二十四胞体](../Page/正二十四胞体.md "wikilink")，事实上，如果移除这24个顶点，就会得到另一个有意思的半正多胞体[扭棱正二十四胞体](../Page/:en:Snub_24-cell.md "wikilink")（Snub
Icositetrachoron）。

### 对称群构造

如果把坐标看作[四元数](../Page/四元数.md "wikilink")，正六百胞体的120个顶点以四元数乘法组成[群](../Page/群.md "wikilink")。这个群通常称为[双二十面体群](../Page/双二十面体群.md "wikilink")，因為它是[二十面体群](../Page/二十面体群.md "wikilink")*I*的双重覆蓋。这个双十二面体群也可被看作是正六百胞体的旋转（无反射）对称群，因为单位四元数的乘法等同于点的旋转，也因此双十二面体群是H<sub>4</sub>群的一个子群。双二十面体群[同构於](../Page/同构.md "wikilink")[特殊线性群SL](../Page/特殊线性群.md "wikilink")(2,5)。

正六百胞体的[对称群是](../Page/对称群.md "wikilink")*H*<sub>4</sub>的[外尔群](../Page/外尔群.md "wikilink")，这个群的阶是14400。

## 可视化

正六百胞体的胞众多，并且这些正四面体胞基本上没有什么规律可循，为正六百胞体的可视化带来了许多困难，但作为[正一百二十胞体的](../Page/正一百二十胞体.md "wikilink")[对偶](../Page/对偶多面体.md "wikilink")，许多正一百二十胞体的性质也表现在正六百胞体上。

### 大圆结构

正一百二十胞体的10个会首尾相连，构成“大圆”，这些胞与正六百胞体的顶点对偶，它们也会互相连接形成一个[正十边形](../Page/正十边形.md "wikilink")，这正十边形的每一条边周围都有5个正四面体共这条边，这种结构看上去就像有棱有角的飞盘。正十边形相邻的两条棱周围的两簇正四面体中间会有空隙，我们可以在填入10个正四面体使其构成正二十面体，这样你就会得到一个涉及150个胞、10条棱、100个裸露的正三角形面的环形结构，我们还可以在向这些面上填上正四面体，会得到一个涉及250个胞的有50个突出的顶点和100条凹陷的棱的大圆，它与另一条与之正交的250胞环在顶点处咬合，剩余的棱的空隙是剩余的100个胞。现在，如果我们去掉这两条大圆最初的10个顶点，我们就会得到四维唯一的[非Wythoff](../Page/:en:non-Wythoffian.md "wikilink")[凸半正多胞体](../Page/:en:convex_uniform_polychoron.md "wikilink")——[重反棱柱](../Page/:en:Grand_Antiprism.md "wikilink")，原来的大圆处留下了各10个正五[反棱柱](../Page/反棱柱.md "wikilink")，并剩下了300个正四面体胞。

## 参考

  - [600-cell](http://eusebeia.dyndns.org/4d/600-cell) 逐层剖析了正六百胞体的表分层结构
  - [Regular Convex Four-Dimensional
    Polytopes](http://davidf.faricy.net/polyhedra/Polytopes)提供了正六百胞体的几何数据

[Category:多胞体](../Category/多胞体.md "wikilink")