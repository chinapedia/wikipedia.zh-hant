**組織學**（，源自[古希腊语单词](../Page/古希腊语.md "wikilink")**）是一門对[生物組織的微觀研究](../Page/生物組織.md "wikilink")，研究它們的形成、構造和功能。組織是指生物體中相同或相似的細胞集合以執行特定功能的細胞群稱為。動物體基本上是由[上皮組織](../Page/上皮組織.md "wikilink")、[結締組織](../Page/結締組織.md "wikilink")、[肌肉組織和](../Page/肌肉組織.md "wikilink")[神經組織所構成](../Page/神經組織.md "wikilink")。與組織學相關的生物學門包括了[細胞生物學與解剖學等](../Page/細胞生物學.md "wikilink")，細胞的層級是在組織之下；[解剖學研究的對象是](../Page/解剖學.md "wikilink")[器官](../Page/器官.md "wikilink")，其層級位於組織之上；[形態學則是對整個生物體的研究](../Page/形态学_\(生物学\).md "wikilink")。

通常組織學的研究對象只有[動物的組織](../Page/動物.md "wikilink")。在組織學的研究中，[顯微鏡是非常重要的一項研究工具](../Page/顯微鏡.md "wikilink")。組織學的藝術包含了依有興趣的部位挑選適當的染劑，很多現行的染劑都是利用[抗體的化學性質](../Page/抗體.md "wikilink")，來標定有興趣的目標。1906年，[諾貝爾生理醫學獎授予意大利組織學家](../Page/諾貝爾生理醫學獎.md "wikilink")[C.Golgi和西班牙組織學家](../Page/卡米洛·高尔基.md "wikilink")[R.Cajal](../Page/圣地亚哥·拉蒙-卡哈尔.md "wikilink")，因为他们发明了镀银染色法和开创性研究了神经系统组织结构。\[1\]

## 植物的組織學研究

研究組織裡細胞層級的構造、組成、始源等議題，常仍包含在[植物解剖學的領域之中](../Page/植物解剖學.md "wikilink")，但histological仍普遍被當作一般描述辭使用，意指對組織及細胞的研究及其方法。

## 参考文献

## 外部链接

## 參見

  - [細胞生物學](../Page/細胞生物學.md "wikilink")
  - [解剖學](../Page/解剖學.md "wikilink")、[植物解剖學](../Page/植物解剖學.md "wikilink")
  - [型態學](../Page/型態學.md "wikilink")
  - [顯微鏡學](../Page/顯微鏡學.md "wikilink")

{{-}}

[Category:生物学分支](../Category/生物学分支.md "wikilink")
[組織學](../Category/組織學.md "wikilink")
[Category:组织工艺学](../Category/组织工艺学.md "wikilink")
[Category:染色法](../Category/染色法.md "wikilink")
[Category:医学术语](../Category/医学术语.md "wikilink")
[Category:医学院所教科目](../Category/医学院所教科目.md "wikilink")
[Category:希腊语外来词](../Category/希腊语外来词.md "wikilink")

1.