[Partly_disassembled_Lumix_digital_camera.jpg](https://zh.wikipedia.org/wiki/File:Partly_disassembled_Lumix_digital_camera.jpg "fig:Partly_disassembled_Lumix_digital_camera.jpg")
[Chipincamera.jpg](https://zh.wikipedia.org/wiki/File:Chipincamera.jpg "fig:Chipincamera.jpg")
[Casio_Exilim.jpg](https://zh.wikipedia.org/wiki/File:Casio_Exilim.jpg "fig:Casio_Exilim.jpg")數碼相機\]\]
[Canon_PowerShot_A95_-_front_and_back.jpg](https://zh.wikipedia.org/wiki/File:Canon_PowerShot_A95_-_front_and_back.jpg "fig:Canon_PowerShot_A95_-_front_and_back.jpg")
PowerShot A95 \]\]
[S9000.jpg](https://zh.wikipedia.org/wiki/File:S9000.jpg "fig:S9000.jpg")
[Hendys_Law.jpg](https://zh.wikipedia.org/wiki/File:Hendys_Law.jpg "fig:Hendys_Law.jpg")
**數碼相機**（），是一種利用[電子](../Page/電子.md "wikilink")[感測器把](../Page/感測器.md "wikilink")[光學影像轉換成電子數據的](../Page/光學.md "wikilink")[照相機](../Page/照相機.md "wikilink")，有别于傳統[照相機通過光線引起](../Page/照相機.md "wikilink")[底片上的化學變化來記錄圖像](../Page/底片.md "wikilink")。“數-{}-碼”一词原本是英文Digital（数字的）的港式翻译，后来传入大陆，而台灣則使用「數-{}-位」。依功能、構造與畫質的不同，目前較常見的數碼照相機可區分為[消費型數位相機](../Page/消費型數位相機.md "wikilink")（俗称[傻瓜相机](../Page/傻瓜相机.md "wikilink")）、[類單眼數位相機](../Page/類單眼數位相機.md "wikilink")、[數碼單鏡反光相機及](../Page/數碼單鏡反光相機.md "wikilink")[無反光鏡可換鏡頭相機](../Page/無反光鏡可換鏡頭相機.md "wikilink")4種。另也有針對極為專業的特殊需求而設計的數碼中片幅（120片幅）相機。

在數位相機中，光感應式[電荷耦合元件或](../Page/電荷耦合元件.md "wikilink")[互补式金属氧化物半导体](../Page/互补式金属氧化物半导体.md "wikilink")[感測器用來取代傳統相機](../Page/感測器.md "wikilink")[底片的化學](../Page/底片.md "wikilink")[感光功能](../Page/感光.md "wikilink")。被捕捉的圖像数据经集成的[微处理器通过一定算法编码后](../Page/微处理器.md "wikilink")，儲存在相機內部數位存儲設備（[記憶卡](../Page/記憶卡.md "wikilink")、微型[硬盘](../Page/硬盘.md "wikilink")、[軟碟或](../Page/軟碟.md "wikilink")[可重寫光碟](../Page/CD-RW.md "wikilink")）中。随着[闪存容量的大幅增加和价格的下降](../Page/闪存.md "wikilink")，目前绝大多数数码相机都已采用闪存作为储存方案。

雖然早期電子元件性能不佳，但由于数码相机小巧轻便、即拍即有、使用成本低、相片方便保存、分享与后期编辑等诸多优点，而且畫質進步極快，使其在短时间得到迅速普及。大部分数码相机兼具有录音、摄录动态影像等功能。2009年，全球共售出数码相机（包括带数码相机功能的手机）超过9亿部，而传统相机已近乎在市场上绝迹。目前，越来越多的设备如[手机](../Page/手机.md "wikilink")、[个人数字助理](../Page/个人数字助理.md "wikilink")、[个人电脑](../Page/个人电脑.md "wikilink")、[终端机及](../Page/终端机.md "wikilink")[平板电脑等也整合进了数码相机功能](../Page/平板电脑.md "wikilink")。

## 歷史

1975年，[柯达应用电子研究中心工程师](../Page/柯达.md "wikilink")[史蒂芬·沙森开发出了世界上第一台数码相机](../Page/史蒂芬·沙森.md "wikilink")，这台数码相机以[磁带作为](../Page/磁带.md "wikilink")[存储介质](../Page/存储介质.md "wikilink")，拥有1万[像素](../Page/像素.md "wikilink")。记录一张黑白影像需要23秒。这台“手持式电子照相机”的出现颠覆了摄影的物理本质。但柯達當時認為，數位相機的發展將威脅到公司賴以維生的底片事業，所以停止研發。

## 種類

  - 消費型數位相機〈[傻瓜相機](../Page/傻瓜相機.md "wikilink")〉

<!-- end list -->

  -
    特色是小巧輕便，操作簡單，價格較低。選擇內建的拍攝模式後，通常只要簡單的變焦構圖，即可按下快門獲得照片。調整對焦點。由於拍攝參數幾乎全部由相機自動判斷決定，因此在非泛用性場景時使用，操作較其他高階相機來的困難且品質不佳。目前數量在智慧手機出現後即逐漸減少，也正在將自動控制的技術轉移至手機中而被逐步取代。

<!-- end list -->

  - [類單眼數位相機](../Page/類單眼相機.md "wikilink")

<!-- end list -->

  -
    將相機之內部硬體與鏡頭鏡片組優質化，並加入更多的控制按鈕以達到逼近數位單眼相機的拍攝操作性，具備PASM手動調控。由於不能更換鏡頭，因此此類產品以其搭載的高倍率涵蓋多焦段的長焦鏡頭組或大光圈鏡頭組為訴求。

<!-- end list -->

  - [數位單眼相機](../Page/數位單眼相機.md "wikilink")

<!-- end list -->

  -
    功能較強，照片畫質較佳，可以更換[鏡頭](../Page/鏡頭.md "wikilink")。硬體許可範圍內，能自由調整[快門及](../Page/快門.md "wikilink")[光圈](../Page/光圈.md "wikilink")。支援手動對焦，自動對焦時允許選擇對焦點，切換單拍，連拍等模式。單眼相機由於採用“單眼反射式光學取景器”而得名。但目前除了以靠反光五稜鏡組的光學取景器（OVF）外，還有使用電路傳輸影像的電子取景器（EVF）可供使用。依影像感应器面積，此類相機可分為全片幅（36X24mm），[APS-C尺寸](../Page/APS-C.md "wikilink")（21X14至24X16），四分之一片幅（17．3X13至18．7X14），微型四分之一画幅（指采用高感光力感影器，性能等同于四分之一画幅）的照相机，消费画幅和手机画幅。感应器感光力提升一倍，光圈直径降至1．4分之1，对于焦平面清晰度的提升，等同于感应器面积提升一倍。

[Canon_CMOS.jpg](https://zh.wikipedia.org/wiki/File:Canon_CMOS.jpg "fig:Canon_CMOS.jpg")

  - [電子式取景可換鏡頭相機](../Page/電子式取景可換鏡頭相機.md "wikilink")

<!-- end list -->

  -
    被稱作微單眼，市場定位雖與類單眼類似，但是改成從數位單眼方向來做設計。需要透過將數位單眼的功能濃縮，所以在比較精進的技術的近日才得以逐步實用化，並代換了早期類單眼等的市場，功能也介於屬於消費型數位相機與數位單眼相機之間。影像感影器則與數位單眼相機雷同。

<!-- end list -->

  - [中片幅數位相機](../Page/中片幅.md "wikilink")

<!-- end list -->

  -
    中片幅相機為相映單眼相機結構，但使用尺寸更大的感光晶片（比全片幅（36X24mm）更大），而數位化後的產品為中片幅數位相機。此產品除了有可交換鏡頭功能外，還有可抽換的[數位機背可供選擇](../Page/數位機背.md "wikilink")。意指當你見到一台中片幅數位相機時，機身部分可能是集合兩家廠商的產品。目前中片幅數位相機多使用120或是220尺寸格式的感光元件，相對於135格式（使用35mm膠片尺寸）的全片幅數位單眼相機還要大上許多。價格也是非大眾所能承受的，目前多為需要大尺寸商業印刷的專業出版業者所使用。

## 特性

[數碼攝影方式比傳統膠捲攝影優越的地方包括](../Page/數碼攝影.md "wikilink")：

  - 比起傳統的照片沖洗後郵寄。數碼相片可以經由網路分享、傳遞，迅速便利。
  - 數位相機的[記憶卡](../Page/記憶卡.md "wikilink")，雖然較[底片昂貴](../Page/底片.md "wikilink")，但具有較高容量（2G記憶卡，視設定[解析度不同](../Page/解析度.md "wikilink")，約可拍100\~1000張照片。至2010年，記憶卡的價格已經很便宜，2G記憶卡已經和底片差不多價格），且可重複使用。
  - 即拍即得，[數碼攝影可以立即透過相機欣賞圖片](../Page/數碼攝影.md "wikilink")，不滿意立刻重拍。傳統攝影往往要底片沖洗完成，才會發現相片過曝，曝光不足，或者晃動等失誤。
  - 光電轉換芯片能提供多種[感光度選擇](../Page/感光度.md "wikilink")，調整調整相機設定即可改變[感光度](../Page/感光度.md "wikilink")。傳統相機根據使用的膠卷而固定[感光度](../Page/感光度.md "wikilink")。
  - 傳統相機膠卷对拍摄对象照度有较高要求，照度低无法使胶片感光。[數碼攝影可以记录到极低照度的影像](../Page/數碼攝影.md "wikilink")。
  - 家庭個人電腦普及，資料儲存方式多元，數位照片可輕易備份於[可重寫光碟](../Page/CD-RW.md "wikilink")，[硬碟](../Page/硬碟.md "wikilink")，甚至網路伺服器上。藉由多重備份，減少遺失風險。而數碼照片也不會因為年代久遠而泛黃。
  - 照片輸入電腦，加以備份後，可用編輯軟體後製，進行旋轉、裁切、調整對比等，若不滿意，還可由備份還原，重新後製。傳統底片則需要暗房技術才能後製，且後製失敗，底片無法復原。
  - 透過電腦[顯示器觀賞照片](../Page/顯示器.md "wikilink")，遠比傳統一般相紙大上許多。
  - [噴墨印表機技術的進步和普及](../Page/噴墨印表機.md "wikilink")，使用者在家即可列印出媲美傳統沖洗的相片。

[數碼攝影方式比傳統膠捲攝影缺陷的地方包括](../Page/數碼攝影.md "wikilink")：

隨著廠商陸續推出千萬以上[像素數碼相機](../Page/像素.md "wikilink")，和傳統的35mm[膠卷照相機相比](../Page/膠卷.md "wikilink")，數碼照相機的影像品質各有勝負，除非是在輸出大幅面作品的時候，才能查覺數碼像機的缺點。不過由於底片仍有一些優勢（主要在於色彩表現）是數碼相機無法完全趕上的、底片的畫質也有一種不同於數碼相機的特性；因此仍有一些專業的攝影工作者以及底片愛好者堅持保留膠卷照相機，一些人甚至表示，底片的一些優點是門外漢也能感受到的。

由於來自像[尼康和](../Page/尼康.md "wikilink")[佳能等製造商最新的](../Page/佳能.md "wikilink")[數碼單眼相機的影像品質已經進一步得到提高](../Page/數碼單眼相機.md "wikilink")，同時這些數位產品擁有更短的後期處理時間與更少的耗材需求（省卻攜帶與購買大批底片膠捲的必要性），這對於非常注重作品時效性的攝影記者而言是非常大的誘惑。通常以[數碼單鏡反光相機的光學質量](../Page/數碼單鏡反光相機.md "wikilink")，超過千萬[像素以上](../Page/像素.md "wikilink")[電荷耦合元件的圖像](../Page/電荷耦合元件.md "wikilink")，已能符合[報紙或](../Page/報紙.md "wikilink")[雜誌需要](../Page/雜誌.md "wikilink")。新聞從業人員也不需要親自送[底片或沖洗好的照片回公司](../Page/底片.md "wikilink")，只需用網路傳遞即可。因此目前[數碼單眼相機已取代了大多數媒體出版業所使用的傳統相機](../Page/數碼單眼相機.md "wikilink")。加之目前的出版業已幾乎改用[電子排版甚至](../Page/桌面出版.md "wikilink")[電子製版等後期製作方式](../Page/電子化印前作業.md "wikilink")，而傳統相機所拍出的膠捲底片尚需進一步先沖洗成像，再以[掃瞄器處理](../Page/掃瞄器.md "wikilink")，才能獲取可用的影像文件，等於比數碼照相機多了兩道額外的工序，數位照相機的這一優勢加速了其普及過程。

多數[消費型數碼相機輸出的影像寬高比是](../Page/消費型數碼相機.md "wikilink")4:3，符合傳統電腦螢幕的寬高比例，但是[數碼單眼相機則採用了類似](../Page/數碼單眼相機.md "wikilink")35mm銀鹽底片的傳統格式，寬高比為3:2，只有日本[奧林巴斯及松下兩家公司採用的](../Page/奧林巴斯.md "wikilink")4/3系統[數碼單眼相機](../Page/數碼單眼相機.md "wikilink")，以及後來的Micro
4/3系統數位相機，輸出的影像是4:3寬高比。2005年，[尼康推出了第一款符合WiFi標準的消費級數碼相機](../Page/尼康.md "wikilink")[尼康Coolpix
P1](../Page/尼康Coolpix_P1.md "wikilink")。

## 資料儲存格式

在消費數碼相機市場上使用了多種不同的儲存格式：

  - [Compact Flash](../Page/CompactFlash.md "wikilink")
  - [-{zh-hans:微型硬盤; zh-hant:微型硬碟;}-](../Page/微型硬碟.md "wikilink")
  - [SmartMedia](../Page/SmartMedia.md "wikilink")（SM卡）
  - [多媒體記憶卡](../Page/多媒體記憶卡.md "wikilink")
  - [SD卡](../Page/SD卡.md "wikilink")
  - [Secure Digital High Capacity](../Page/SDHC.md "wikilink")（SDHC卡）
  - [記憶卡](../Page/記憶卡.md "wikilink")
  - [xD圖像卡](../Page/xD圖像卡.md "wikilink")

Image:Compactflash-512mb.png|[CompactFlash](../Page/CompactFlash.md "wikilink")（CF-I）
Image:Memory Stick Front and Back.jpg|[Memory
Stick](../Page/Memory_Stick.md "wikilink")
Image:MicroDrive1GB.jpg|[Microdrive](../Page/Microdrive.md "wikilink")（CF-II）
Image:Kingston Multi Media Card 32MB front
20040702.jpg|[MultiMediaCard](../Page/MultiMediaCard.md "wikilink")（MMC）
Image:Secure Digital Kingston 512MB.jpg|[Secure Digital
card](../Page/Secure_Digital_card.md "wikilink")（SD） Image:MiniSD memory
card including adapter.jpg|[MiniSD](../Page/MiniSD.md "wikilink")

Image:Smartmedia card
closeup.jpg|[SmartMedia](../Page/SmartMedia.md "wikilink") Image:USB
flash drive.jpg|[USB隨身碟](../Page/USB隨身碟.md "wikilink") Image:XD card 16M
Fujifilm front.png|[XD影像卡](../Page/XD影像卡.md "wikilink")（xD） Image:Floppy
disk 90mm.JPG|3.5"軟碟片 Image:Mini CD vs Normal CD comparison.jpg|[Mini
CD](../Page/Mini_CD.md "wikilink")（左）
Image:MicroSD與SD記憶卡.JPG|[MicroSD與](../Page/MicroSD.md "wikilink")[SDHC記憶卡](../Page/SDHC.md "wikilink")

## 常見數碼相機品牌

  - [愛國者](../Page/愛國者.md "wikilink")
  - [明基](../Page/明基.md "wikilink")
  - [佳能](../Page/佳能.md "wikilink")
  - [卡西歐](../Page/卡西歐.md "wikilink")
  - [富士膠片](../Page/富士膠片.md "wikilink")
  - [通用電器](../Page/通用電器.md "wikilink")
  - [柯達](../Page/柯達.md "wikilink")
  - [萊卡](../Page/萊卡.md "wikilink")
  - [尼康](../Page/尼康.md "wikilink")
  - [奧林巴斯](../Page/奧林巴斯.md "wikilink")
  - [松下電器](../Page/松下電器.md "wikilink")
  - [賓得士](../Page/賓得士.md "wikilink")
  - [理光](../Page/理光.md "wikilink")
  - [祿萊](../Page/祿萊.md "wikilink")
  - [適馬](../Page/適馬.md "wikilink")
  - [三星電子](../Page/三星電子.md "wikilink")
  - [-{zh-hans:索尼; zh-hant:新力;}-](../Page/Sony.md "wikilink")

## 參見

  - [照相手機](../Page/照相手機.md "wikilink")
  - [數碼攝影](../Page/數碼攝影.md "wikilink")
  - [數碼圖像](../Page/數碼圖像.md "wikilink")
  - [數碼相框](../Page/數碼相框.md "wikilink")
  - [直接打印](../Page/直接打印.md "wikilink")
  - [PictBridge](../Page/PictBridge.md "wikilink")
  - [數位單眼相機](../Page/數位單眼相機.md "wikilink")
  - [Four Thirds](../Page/Four_Thirds.md "wikilink")
  - [網上照片輸出業](../Page/網上照片輸出業.md "wikilink")
  - [3C (商品代稱)](../Page/3C_\(商品代稱\).md "wikilink")
  - [感光耦合元件](../Page/感光耦合元件.md "wikilink")
  - [互補式金屬氧化物半導體](../Page/互補式金屬氧化物半導體.md "wikilink")
  - [Foveon X3](../Page/Foveon_X3.md "wikilink")

## 参考文献

## 外部連結

  - [數碼相機歷史](http://www.digicamhistory.com/)

{{-}}

[Category:照相机类型](../Category/照相机类型.md "wikilink")
[数码照相机](../Category/数码照相机.md "wikilink")