**Bajka**（讀「Biker」，暫時沒有中文譯名）全名**B.
Pluwatsch**（）。詩人及歌手。現居[德國](../Page/德國.md "wikilink")[慕尼黑](../Page/慕尼黑.md "wikilink")。
她在印度出生，父母是德國人。她遊歷了很多地方，曾經住在[阿富汗](../Page/阿富汗.md "wikilink")、[尼泊爾](../Page/尼泊爾.md "wikilink")、[巴基斯坦及](../Page/巴基斯坦.md "wikilink")[南非](../Page/南非.md "wikilink")。她曾就讀於[果阿](../Page/果阿.md "wikilink")、[班加羅爾](../Page/班加羅爾.md "wikilink")、[拉多斯](../Page/拉多斯.md "wikilink")、[葡萄牙](../Page/葡萄牙.md "wikilink")、[西雅圖](../Page/西雅圖.md "wikilink")、[德班及](../Page/德班.md "wikilink")[開普敦](../Page/開普敦.md "wikilink")。

她是J.Pluwatsch的女兒，J.Pluwatsch是德國Krautrock樂隊成員。

Bajka在布拉格學習音樂。她對亞洲的情誼流露於她的詩內。她曾在某些作品中表演詩歌朗誦，包括*[秘密還遊世界](../Page/秘密還遊世界.md "wikilink")(Transglobal
Underground)*的*本能旅行者(Instinctive Traveller)*及*南非祖魯國(South African Zulu
Nation)*的*泡泡搖擺舞曲（Bubblegum）*。她亦曾與*Noujum
Oazza*及他的樂隊*[城市僧侶](../Page/城市僧侶.md "wikilink")(Urban
Dervish)*合作爲*[摩洛哥皇家管弦樂隊](../Page/摩洛哥皇家管弦樂隊.md "wikilink")*(Royal
Moroccan Orchestra)''演出。

近年來，她把角色由詩人轉向歌手。她的聲音被描述爲[爵士](../Page/爵士.md "wikilink")(jazz)/[騷靈](../Page/騷靈.md "wikilink")（soul
music）。她的聲音出現於[弛放音樂](../Page/弛放音樂.md "wikilink")（chill out
music）及電子舞曲（electronic dance
music）裏。更近期，她出現在Bonobo的最後一張大碟Days To
Come裏。

## 曾推出大碟

  - The Only Religion That I Believe (7") (2005)
  - I Can No Poet Be / Love's Serenity (12") (2006)

## 客串

  - Beanfield - Human Patterns (November, 1999) (1 track)
  - Ben Mono - Dual (2003) (4 tracks)
  - Beanfield - Seek (2004) (1 track)
  - Das Goldenes Zeitalter - A Vision / Breakin' Through / Im Würgegriff
    Der Schönen Künste (2006) (1 track)
  - Radio Citizen - Berlin Serengeti (September, 2006) (6 tracks)
  - [Bonobo](http://en.wikipedia.org/wiki/Days_To_Come) - [Days To
    Come](http://en.wikipedia.org/wiki/Days_To_Come) (October, 2006) (4
    tracks)

## 外部链接

  - [Discogs - Bajka](http://www.discogs.com/artist/Bajka)

[Category:德國歌手](../Category/德國歌手.md "wikilink")
[Category:德國詩人](../Category/德國詩人.md "wikilink")