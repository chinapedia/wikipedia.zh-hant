[Yue_Fei_temple_13.jpg](https://zh.wikipedia.org/wiki/File:Yue_Fei_temple_13.jpg "fig:Yue_Fei_temple_13.jpg")

**岳王廟**是紀念及祭祀[南宋时期民族英雄](../Page/南宋.md "wikilink")[岳飞的廟宇](../Page/岳飞.md "wikilink")，最著名的岳王廟是始建於[南宋的](../Page/南宋.md "wikilink")[杭州](../Page/杭州.md "wikilink")[西湖西北角](../Page/西湖.md "wikilink")、[栖霞岭南麓的岳王廟](../Page/栖霞岭.md "wikilink")，因岳飛葬於此地，故杭州岳王廟又称**岳飞墓**。1961年，杭州岳王廟被[中华人民共和国国务院列为](../Page/中华人民共和国国务院.md "wikilink")[全国重点文物保护单位](../Page/全国重点文物保护单位.md "wikilink")。

## 岳飛簡介

[岳飛為](../Page/岳飛.md "wikilink")[中國](../Page/中國.md "wikilink")[南宋時期名將](../Page/南宋.md "wikilink")，對抗南侵的[金國軍隊期間用兵如神](../Page/金國.md "wikilink")，屢戰屢勝。後來，其卻被主和派的[秦檜及](../Page/秦檜.md "wikilink")[宋高宗以](../Page/宋高宗.md "wikilink")[莫須有的罪名在杭州](../Page/莫須有.md "wikilink")[风波亭被处死](../Page/风波亭.md "wikilink")。但他的英勇事蹟沒有被後人遺忘，其後被追封為鄂武穆王及岳王。後世對他多以正面評價，更視為民族英雄。

## 杭州岳王廟

1142年岳飞死后，遗体被狱卒[隗顺盗出](../Page/隗顺.md "wikilink")，葬于九曲丛祠旁。

1162年[宋孝宗下詔為岳飞平反昭雪](../Page/宋孝宗.md "wikilink")，悬赏寻觅岳飞遗体，并寻访岳飞子孙后代封官授爵。[隗顺的儿子看到诏书](../Page/隗顺.md "wikilink")，就将隐密多年的岳飞初葬地告诉了朝廷。宋孝宗遂将岳飞的遗骸隆重地迁葬至今址。嘉定十四年（1221年），宋宁宗賜岳飞墓旁的智果观音院為褒忠衍福禅寺，即岳王庙的前身，以表彰岳飞的功德。岳王廟歷經元明兩朝屢有毀建，現今建築樣式為清[康熙五十四年](../Page/康熙.md "wikilink")（1715年）建，民國七年（1918年）大修。“[文化大革命](../Page/文化大革命.md "wikilink")”期间，岳王庙遭到严重破坏。[墓阙被推倒](../Page/墓阙.md "wikilink")，墓地被夷为平地，遍地被砸破的断碑殘碣。1979年全面整修。

岳王庙头门为二层重檐建筑，正中悬挂“岳王庙”三字牌匾，两侧有“三十功名尘与土，八千里路云和月”的对联，为岳飞所作《[满江红](../Page/满江红.md "wikilink")》中的名句。头门后为一四方院落，正面便是正殿忠烈祠，中间高悬横匾“心昭天日”，为[叶剑英手书](../Page/叶剑英.md "wikilink")，这四个字正是来自岳飞生前所叹“天日昭昭”。大殿内塑有岳飞彩色坐像，像高4.5米。殿中高悬“还我河山”匾额，为岳飞手迹，两侧有明代人所书“精忠报国”，[中国佛教协会会长](../Page/中国佛教协会.md "wikilink")[赵朴初所书](../Page/赵朴初.md "wikilink")“碧血丹心”以及[西泠印社社长](../Page/西泠印社.md "wikilink")[沙孟海所书](../Page/沙孟海.md "wikilink")“浩气长存”等匾额。

大殿右侧为1979年按南宋建筑风格修建的岳飞墓，墓呈圆形，墓碑上写“宋岳鄂王墓”字样。旁有其子[岳云墓](../Page/岳云.md "wikilink")。正对石墓有與岳飞被殺有關的[秦桧](../Page/秦桧.md "wikilink")、王氏、[万俟卨](../Page/万俟卨.md "wikilink")、[张俊等四人跪像](../Page/张俊.md "wikilink")，为白铁铸造。四人像均无上衣，袒胸露乳，低头面向石墓。以前经常有人鼓励小孩在铁像上小便，以示侮辱。跪像后有楹联，上书“青山有幸埋忠骨，白铁无辜铸佞臣。”

墓园外有庭院，内有精忠柏亭，放置枯[柏](../Page/柏树.md "wikilink")，现代人通过科技手段测定，这些柏木为[硅化木](../Page/硅化木.md "wikilink")，已有一亿二千万年以上的历史。园内有碑廊，陈列着岳飞手迹及各代名人凭吊的作品。岳庙内辟有岳飞纪念馆，陈列有岳飞的事迹和一些文史资料等。

<File:Yue> Fei Temple Courtyard.jpg|岳王廟正殿前 <File:Yue> Fei Temple,
2015-03-22 21.jpg|正殿内部 <File:Yue> Fei Temple, 2015-03-22 37.jpg|岳飞纪念馆
<File:Yue> Fei Temple, 2015-03-22 27.jpg|岳飞墓入口与楹联 Yue Fei Temple,
Hangzhou, Feb 2017 - 2.jpg|秦桧、王氏跪像 <File:Yue> Fei Temple, 2015-03-22
31.jpg|明代石像生 <File:Tombs> of Yue Fei and Yue Yun 20161002.jpg|岳坟
[File:Yue_fei_words.jpg|盡忠報國四字](File:Yue_fei_words.jpg%7C盡忠報國四字)

## 参考

### 其他

  - 《[西湖梦寻](../Page/西湖梦寻.md "wikilink")·岳王坟》，（明）[张岱著](../Page/张岱.md "wikilink")，ISBN
    957-763-132-0

[Category:岳飛廟](../Category/岳飛廟.md "wikilink")
[Y](../Category/杭州建筑物.md "wikilink")
[国](../Category/杭州文物保护单位.md "wikilink")
[Y](../Category/中國傳統建築.md "wikilink")