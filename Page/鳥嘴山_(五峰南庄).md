**鳥嘴山**，位於[臺灣](../Page/臺灣.md "wikilink")[苗栗縣](../Page/苗栗縣.md "wikilink")[南庄鄉](../Page/南庄鄉_\(台灣\).md "wikilink")[東河村與](../Page/東河村_\(苗栗縣\).md "wikilink")[新竹縣](../Page/新竹縣.md "wikilink")[五峰鄉桃山村交界處的一座山峰](../Page/五峰鄉_\(台灣\).md "wikilink")，峰頂海拔1,551公尺\[1\]\[2\]\[3\]，屬於加里山山脈北稜線上的山峰。因山腰處向側方延伸突出，遠眺形似老鷹嘴形而得名。\[4\]
[WuFeng_NiaoZhui.jpg](https://zh.wikipedia.org/wiki/File:WuFeng_NiaoZhui.jpg "fig:WuFeng_NiaoZhui.jpg")

## 地理

### 周邊山岳

以下為沿此山主要[稜線分佈的周邊山岳](../Page/稜線.md "wikilink")\[5\]：

  - 沿東北轉北稜線：[鵝公髻山](../Page/鵝公髻山.md "wikilink")、[五指山](../Page/五指山_\(新竹縣\).md "wikilink")
  - 沿東南轉南稜線：[大窩山](../Page/大窩山.md "wikilink")、[比林山](../Page/比林山.md "wikilink")、[鹿場大山](../Page/鹿場大山.md "wikilink")

周邊山岳尚有[面托油山](../Page/面托油山.md "wikilink")（東南東側）。

### 源流河川

以下為發源於此山的河川\[6\]：

  - [土場溪支流](../Page/土場溪.md "wikilink")（[頭前溪水系](../Page/頭前溪.md "wikilink")）：發源於東南坡
  - [大東河支流](../Page/大東河.md "wikilink")（[中港溪水系](../Page/中港溪.md "wikilink")）：發源於北坡
  - [大東河支流](../Page/大東河.md "wikilink")（[中港溪水系](../Page/中港溪.md "wikilink")）：發源於西南坡

## 参考文献

<div class="references-small">

<references />

</div>

[Category:苗栗縣山峰](../Category/苗栗縣山峰.md "wikilink")
[Category:新竹縣山峰](../Category/新竹縣山峰.md "wikilink")
[Category:南庄鄉 (台灣)](../Category/南庄鄉_\(台灣\).md "wikilink")
[Category:五峰鄉 (台灣)](../Category/五峰鄉_\(台灣\).md "wikilink")

1.  《苗栗縣南-{庄}-鄉行政區域圖》，內政部，2007年5月

2.  《新竹縣五峰鄉行政區域圖》，內政部，2007年5月

3.  上河文化出版的《全臺灣道路地圖》第10圖幅中標示鳥嘴山標高為1551公尺。而中華民國內政部經建版地圖則標示為1550公尺。

4.  中華民國內政部 臺灣地區地名查詢系統
    鳥嘴山，http://placesearch.moi.gov.tw/input/detail.php?input_id=8786
    ，苗栗縣地名探源

5.  《台灣走透透：地圖王》，戶外生活圖書，2007年3月，第32－33、40－41頁

6.