[Infinitus_Plaza_2014.jpg](https://zh.wikipedia.org/wiki/File:Infinitus_Plaza_2014.jpg "fig:Infinitus_Plaza_2014.jpg")
[Infinitus_Plaza_Ground_Level.jpg](https://zh.wikipedia.org/wiki/File:Infinitus_Plaza_Ground_Level.jpg "fig:Infinitus_Plaza_Ground_Level.jpg")
[Infinitus_Plaza_Level_1.jpg](https://zh.wikipedia.org/wiki/File:Infinitus_Plaza_Level_1.jpg "fig:Infinitus_Plaza_Level_1.jpg")

**無限極廣場**（），原稱**維德廣場**（），是[香港](../Page/香港.md "wikilink")[港島](../Page/港島.md "wikilink")[中西區的一個](../Page/中西區_\(香港\).md "wikilink")[甲級寫字樓及](../Page/甲級寫字樓.md "wikilink")[商場](../Page/商場.md "wikilink")，位於[上環](../Page/上環.md "wikilink")[德輔道中](../Page/德輔道中.md "wikilink")、[干諾道中和](../Page/干諾道中.md "wikilink")[林士街交界](../Page/林士街.md "wikilink")。

## 歷史

大廈原址前身為[恆星戲院](../Page/恆星戲院.md "wikilink")（舊稱新世界戲院）及[船政署大樓](../Page/海事處.md "wikilink")，因興建[上環站而拆卸](../Page/上環站.md "wikilink")，於1988年初重建完成。

無限極廣場由[伍振民建築師事務所（香港）有限公司設計](../Page/劉榮廣.md "wikilink")，原由[地鐵公司及](../Page/地鐵公司.md "wikilink")[恆隆集團持有](../Page/恆隆集團.md "wikilink")，乃1980年代恆隆與港鐵公司合作發展的港島綫沿線上蓋物業之一，至1987年入伙前出售予[莊啟程創辦的](../Page/莊啟程.md "wikilink")[維德集團](../Page/維德集團.md "wikilink")，招租期間批給[仲量聯行作獨家租務代理](../Page/仲量聯行.md "wikilink")。

## 大廈

大廈樓高34層，包括30層甲級寫字樓，著名租戶為英國航空公司，基座4層為零售商鋪，現在租戶包括[名創優品](../Page/名創優品.md "wikilink")、[屈臣氏酒窖](../Page/屈臣氏酒窖.md "wikilink")、[專業旅運](../Page/專業旅運.md "wikilink")、[龍島朱古力](../Page/龍島.md "wikilink")、[太平洋咖啡](../Page/太平洋咖啡.md "wikilink")、[大家樂](../Page/大家樂.md "wikilink")、[Pret
A
Manger](../Page/Pret_A_Manger.md "wikilink")、[潮江春及](../Page/潮江春.md "wikilink")[利寶閣等](../Page/利寶閣.md "wikilink")。

2003年6月，[摩根士丹利以](../Page/摩根士丹利.md "wikilink")8.4億港元向維德集團購入當時的維德廣場，再於2006年3月以26億港元售給[麥格理](../Page/麥格理.md "wikilink")。

2010年7月[李錦記以](../Page/李錦記.md "wikilink")43.475億元向[麥格理購入維德廣場](../Page/麥格理.md "wikilink")\[1\]，並在2010年12月17日改名為「無限極廣場」\[2\]。

Infinitus Plaza.JPG|無限極廣場1樓南面 HK Sheung Wan Des Voeux Road 無限極廣場
Infinitus Plaza mall evening Apr-2013.JPG|外牆裝修後的「無限極廣場」(2013年) Infinitus
Plaza (Hong Kong).jpg|晚上的無限極廣場，前方的道路為[德輔道中](../Page/德輔道中.md "wikilink")
HK Vicwood Plaza Sheung Wan Des Voeux Road C Rumsey
Street.JPG|裝修前無限極廣場外貌 HK Vicwood Plaza Sheung Wan
Office Exit mark a.jpg|無限極廣場原稱「維德廣場」

## 事件

2008年1月9日，[明報報導](../Page/明報.md "wikilink")，[香港警務處](../Page/香港警務處.md "wikilink")[商業罪案調查科訛騙組](../Page/商業罪案調查科.md "wikilink")2B隊拘捕五人，他們涉嫌於上環維德廣場，冒充外國[投資銀行職員](../Page/投資銀行.md "wikilink")，以高[投資回報為賣點](../Page/投資回報.md "wikilink")，[詐騙市民](../Page/詐騙.md "wikilink")[投資者](../Page/投資者.md "wikilink")[房地產和](../Page/房地產.md "wikilink")[證券](../Page/證券.md "wikilink")[期票](../Page/期票.md "wikilink")。\[3\]

## 鄰近

  - [永安中心](../Page/永安中心.md "wikilink")
  - [金龍中心](../Page/金龍中心.md "wikilink")
  - [李寶樁大廈](../Page/李寶樁.md "wikilink")
  - [海港政府大樓](../Page/海港政府大樓.md "wikilink")
  - [中區行人天橋系統](../Page/中區行人天橋系統.md "wikilink")

## 交通

<div class="NavFrame collapsed" style="color: black; background-color: #FFFF00; margin: 0 auto; padding: 0 10px; text-align: left;">

<div class="NavHead" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px; font-weight:normal;">

**交通路線列表**

</div>

<div class="NavContent" style="background-color: #FFFF00; margin: 0 auto; padding: 0 10px;">

  - [香港電車](../Page/香港電車.md "wikilink")
    [港鐵](../Page/港鐵.md "wikilink")

<!-- end list -->

  - <font color="{{港島綫色彩}}">█</font>[港島綫](../Page/港島綫.md "wikilink")：[上環站E](../Page/上環站.md "wikilink")5出口

<!-- end list -->

  - [德輔道中](../Page/德輔道中.md "wikilink")

<!-- end list -->

  - [干諾道中](../Page/干諾道中.md "wikilink")

</div>

</div>

## 注釋

  -

    以恆隆地產為首的財團曾於1981年連奪港島綫9個車站上蓋物業的發展權，其後因經濟理由被迫放棄金鐘二段的發展權，至1985年7月再度發展港島綫剩餘的5個上蓋物業。

## 外部參考

{{-}}

[Category:港鐵公司物業](../Category/港鐵公司物業.md "wikilink") [Category:中西區商場
(香港)](../Category/中西區商場_\(香港\).md "wikilink") [Category:中西區寫字樓
(香港)](../Category/中西區寫字樓_\(香港\).md "wikilink")
[Category:上環](../Category/上環.md "wikilink")
[Category:1987年完工建築物](../Category/1987年完工建築物.md "wikilink")

1.  [李錦記逾43億購維德廣場](http://news.sina.com.hk/cgi-bin/nw/show.cgi/19/1/1/1785580/1.html)
    星島日報，2010年7月14日
2.  [維德廣場改名「無限極」有段古](http://paper.wenweipo.com/2010/09/13/QB1009130001.htm)
3.  <http://hk.news.yahoo.com/080110/12/2mtlz.html>