[Ostseestadion.JPG](https://zh.wikipedia.org/wiki/File:Ostseestadion.JPG "fig:Ostseestadion.JPG")
**漢莎羅斯托克足球俱樂部**（）
是[德国的职业足球俱乐部](../Page/德国.md "wikilink")，现属于[德国足球丙级联赛球队](../Page/德国足球丙级联赛.md "wikilink")，球队设在[梅克伦堡-前波莫瑞州的](../Page/梅克伦堡-前波莫瑞.md "wikilink")[罗斯托克](../Page/罗斯托克.md "wikilink")，球队是前共产主义时代[东德最成功的俱乐部](../Page/东德.md "wikilink")，在一些大城市如[柏林](../Page/柏林.md "wikilink")，[德累斯顿](../Page/德累斯顿.md "wikilink")，[莱比锡没有球队能撼动汉莎罗斯托克在东德足坛的统治地位](../Page/莱比锡.md "wikilink")。

## 歷史

罗斯托克的前身是1954年11月11日创建的罗斯托克上进体育俱乐部（SC Empor
Rostock）的足球部。1965年12月28日，在罗斯托克邮政总局的文化大厅内宣告成立罗斯托克足球俱乐部。经过广泛征求意见，决定命名为汉莎罗斯托克。「汉莎」一词源于[十三世纪的](../Page/十三世纪.md "wikilink")[汉萨同盟](../Page/汉萨同盟.md "wikilink")，为保护经济利益把欧洲延[波罗的海的](../Page/波罗的海.md "wikilink")150座城市联合起来的同盟组织。由于罗斯托克是德国东北部的港口，所以是当然的成员之一。

罗斯托克在1990/91赛季夺得了前东德最后一次联赛和杯赛“双冠王”，并由此获得了[两德统一后参加德甲联赛的资格](../Page/两德统一.md "wikilink")。仅一个赛季，罗斯托克於1991-92赛季降级，他们在乙级联赛拼搏了三年，在1995/1996赛季终于重新回到了甲级，并且在德甲停留了十年，在這10年的时间里罗斯托克一度成为[德甲中为数不多的东德球队之代表](../Page/德甲.md "wikilink")，罗斯托克是生命力最强的一支，他们是唯一降级之后又重新杀回来的东德球队。但在2004/2005赛季球队战绩不佳降入了乙级联赛，随着罗斯托克的降级东德在德甲已经没有任何球队了。在甲乙組之間徘徊多年後，羅斯托克在2009/2010賽季慘遭滑鐵盧，在德乙18支球隊中只得第16名，需參加附加賽；羅斯托克附加賽中以總比數0比3不敵德丙的恩歌斯達特，是球會歷史上首次降到第三級聯賽。

## 主場球場

**波罗的海球场**（****）是汉莎罗斯托克的主場球場，位於[罗斯托克](../Page/罗斯托克.md "wikilink")，[德文直譯為](../Page/德文.md "wikilink")「東海球場」，即罗斯托克海邊的[波羅的海](../Page/波羅的海.md "wikilink")。

## 球队荣誉

  - **东德甲级联赛冠军**: 1991
  - 东德甲级联赛亚军: 1962, 1963, 1964, 1968
  - **东德足协杯冠军**: 1991
  - 东德足协杯亚军: 1955, 1957, 1960, 1967, 1987
  - **德国乙级联赛冠军**: 1994/95
  - 德国乙级联赛亚军: 2006/07
  - **德国室内足球锦标赛冠军**: 1998

## 效力过球队的著名球员

*汉莎罗斯托克*一共产生了 21 名东德足球队国脚，少数也是统一后的德国国家队国脚。

  - [维克托·阿加利](../Page/维克托·阿加利.md "wikilink")（Victor Agali）

  - [Jonathan Akpoborie](../Page/Jonathan_Akpoborie.md "wikilink")

  - [-{zh-hans:马库斯·奥尔巴克;
    zh-hk:柯碧克;}-](../Page/马库斯·奥尔巴克.md "wikilink")（Marcus
    Allbäck）

  - [Sergej Barbarez](../Page/Sergej_Barbarez.md "wikilink")

  - [Stefan Beinlich](../Page/Stefan_Beinlich.md "wikilink")

  - [Olaf Bodden](../Page/Olaf_Bodden.md "wikilink")

  - |  [托马斯·多尔](../Page/托马斯·多尔.md "wikilink")（Thomas Doll）

  - [安德雷亚斯·雅科布森](../Page/安德雷亚斯·雅科布森.md "wikilink")（Andreas
    Jakobsson）：为瑞典队出战 37 场

  - [-{zh-hans:卡斯滕·扬克尔;
    zh-hk:贊卡;}-](../Page/卡斯滕·扬克尔.md "wikilink")（Carsten
    Jancker）：为德国队出战 30 场比赛并射入 10 球，效力过[上海申花](../Page/上海申花.md "wikilink")

  - [格尔德·奇舍](../Page/格尔德·奇舍.md "wikilink")（Gerd Kische）：为东德国家队出场 63 次

  - [-{zh-hans:亚里·利特马宁;
    zh-hk:列馬倫;}-](../Page/亚里·利特马宁.md "wikilink")（Jari
    Litmanen）：被誉为芬兰最出色的足球运动员

  - [Martin Max](../Page/Martin_Max.md "wikilink")

  - [-{zh-hans:奥利弗·诺伊维尔;
    zh-hk:紐維利;}-](../Page/奥利弗·诺伊维尔.md "wikilink")（Oliver
    Neuville）：为德国国家队出战 66 场并射入 9 球

  - [Marko Rehmer](../Page/Marko_Rehmer.md "wikilink")

  - [约阿希姆·斯特莱希](../Page/约阿希姆·斯特莱希.md "wikilink")（Joachim
    Streich）：东德国家队出场最多的球队，共 102 场并射入 55 球

  - [Igor Pamić](../Page/Igor_Pamić.md "wikilink")

## 參考資料

<div class="references-small">

<references />

</div>

## 外部链接

  - [球队官方网站](http://www.fc-hansa.de)
  - [HANSANEWS.DE | DAS ONLINE MAGAZIN](http://www.hansanews.de)
  - [The Abseits Guide to German
    Soccer](http://www.abseits-soccer.com/clubs/hansa.html)
  - [hansa::fans.de - From Fans for Fans](http://www.hansafans.de)

[Category:德国足球俱乐部](../Category/德国足球俱乐部.md "wikilink")
[Category:1965年建立的足球俱樂部](../Category/1965年建立的足球俱樂部.md "wikilink")
[Category:1965年東德建立](../Category/1965年東德建立.md "wikilink")