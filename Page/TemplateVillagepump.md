<table>
<tbody>
<tr class="odd">
<td><p> </p></td>
<td><p>歡迎光臨互助客棧！</p></td>
<td><p> </p></td>
</tr>
<tr class="even">
<td></td>
<td><p> </p></td>
<td></td>
</tr>
<tr class="odd">
<td><p> </p></td>
<td><p>互助客栈是維基人議事相助之處，用以討論消息、方针、技术以及编辑、求助等議題。<br />
發表議題之前請-{zh:搜索;zh-hans:搜索;zh-hant:搜尋}-先前文章，遵守<strong><a href="維基百科:討論頁指導.md" title="wikilink">指導</a></strong>及<strong><a href="Wikipedia:禮儀.md" title="wikilink">禮儀</a></strong>。<strong>任何與維基百科無關的問題</strong>，请前往<strong><a href="Wikipedia:知识问答.md" title="wikilink">知识问答</a></strong>。</p></td>
<td><p>{{#ifeq:|Wikipedia:互助客栈|}}</p></td>
</tr>
<tr class="even">
<td><p>style="width: 16%; border-right: 1px solid #aaa; padding-top: 10px | <img src="Breezeicons-apps-48-artikulate.svg" title="fig:Breezeicons-apps-48-artikulate.svg" alt="Breezeicons-apps-48-artikulate.svg" /><br />
<a href="Wikipedia:互助客栈/消息.md" title="wikilink">消息</a></p></td>
<td><p>style="width: 16%; border-right: 1px solid #aaa; padding-top: 10px | <img src="Breezeicons-apps-48-cantor.svg" title="fig:Breezeicons-apps-48-cantor.svg" alt="Breezeicons-apps-48-cantor.svg" /><br />
<a href="Wikipedia:互助客栈/方针.md" title="wikilink">方针</a></p></td>
<td><p>style="width: 16%; border-right: 1px solid #aaa; padding-top: 10px | <img src="Breezeicons-categories-32-applications-development.svg" title="fig:Breezeicons-categories-32-applications-development.svg" alt="Breezeicons-categories-32-applications-development.svg" /><br />
<a href="Wikipedia:互助客栈/技术.md" title="wikilink">技术</a></p></td>
</tr>
<tr class="odd">
<td><p>討論維基相關新聞與消息</p></td>
<td><p>討論方針與草案</p></td>
<td><p>解決或報告技術疑難</p></td>
</tr>
<tr class="even">
<td><p><span class="plainlinks">[ 发表] | [ 监视]</p></td>
<td><p><span class="plainlinks">[ 发表] | [ 监视]</p></td>
<td><p><span class="plainlinks">[ 发表] | [ 监视]</p></td>
</tr>
<tr class="odd">
<td><p><inputbox> type=fulltext prefix=Wikipedia:互助客栈 width=30 break=no searchbuttonlabel=-{zh-hans:搜索;zh-hant:搜尋}-全部-{zh-tw:課題;zh-hk:主題;zh-hans:主题;}- </inputbox> <a href="Wikipedia:互助客栈_(全部).md" title="wikilink">-{zh-cn:查看; zh-tw:檢視;}-全部討論</a></p></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table>

<table>
<tbody>
<tr class="odd">
<td><p><a href="Wikipedia:互助客栈/方针/存档.md" title="wikilink">方针</a>{{·}}<a href="Wikipedia:互助客栈/技术/存档.md" title="wikilink">技术</a>{{·}}<a href="Wikipedia:互助客栈/求助/存档.md" title="wikilink">求助</a>{{·}}<a href="Wikipedia:互助客栈/条目探讨/存档.md" title="wikilink">条目探讨</a>{{·}}<a href="Wikipedia:互助客栈/其他/存档.md" title="wikilink">其他</a> }}</p></td>
</tr>
</tbody>
</table>

| 我想要……                                  | 请前往……                                                                                                |
| -------------------------------------- | ---------------------------------------------------------------------------------------------------- |
| 如何有效並安全地-{zh-tw:存取;zh-cn:访问;}-维基百科的方法  | [如何-{zh-tw:存取;zh-cn:访问;}-维基百科](help:如何访问维基百科.md "wikilink")                                          |
| 与繁简处理有关的问题                             | [字词转换](Wikipedia:字词转换.md "wikilink")\<\!--                                                           |
| 请[管理员帮忙或对管理员提意见](WP:管理员.md "wikilink") | 「[请求管理员帮助](Wikipedia:管理员通告板.md "wikilink")」或者「[对管理员的意见和建议](Wikipedia:对管理员的意见和建议.md "wikilink")」--\>  |
| 協助或尋求條目的改善意见                           | [同行评审](Wikipedia:同行评审.md "wikilink")                                                                 |
| 对某些特定来源的讨论                             | [可靠来源布告栏](Wikipedia:可靠来源/布告栏.md "wikilink")                                                          |
| 寻找参考文献                                 | [文献传递](Wikipedia:维基百科图书馆/寻求资源.md "wikilink")                                                         |
| 參與即時讨论或-{zh-tw:透過;zh-cn:通过;}-电子邮件进行讨论  | 「[即時討論](Wikipedia:即時討論.md "wikilink")」或者「[-{zh-tw:郵寄清單;zh-cn:邮件列表;}-](Wikipedia:邮件列表.md "wikilink")」 |

<noinclude>  </noinclude>

[Category:維基百科頁眉模板](../Category/維基百科頁眉模板.md "wikilink")
[Category:互助客棧](../Category/互助客棧.md "wikilink")