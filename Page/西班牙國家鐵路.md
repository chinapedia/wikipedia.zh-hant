[Red_ferroviaria_espa_ola.png](https://zh.wikipedia.org/wiki/File:Red_ferroviaria_espa_ola.png "fig:Red_ferroviaria_espa_ola.png")
**西班牙國家鐵路**（），簡稱，是[西班牙的](../Page/西班牙.md "wikilink")[國營](../Page/國營.md "wikilink")[鐵路公司](../Page/鐵路.md "wikilink")，成立於1941年1月24日，其前身為[西班牙政府於](../Page/西班牙政府.md "wikilink")1928年到1936年間收購的數條民營鐵路。其營運路線總長度達15,000公里，使用1,668mm[寬軌距](../Page/伊比利軌距.md "wikilink")，而AVE高鐵則使用1,435mm[標準軌距](../Page/標準鐵軌.md "wikilink")。

在[西班牙內戰期間](../Page/西班牙內戰.md "wikilink")，鐵路網絡受到破壞，當時的民營公司無力修復，因此才有RENFE出現，以儘快修復全國受破壞的鐵路。後來為了符合[歐盟的鐵路業自由化措施](../Page/歐盟.md "wikilink")，西班牙在2003年制定《鐵路部門法》（，），並自2005年1月1日起，從RENFE分拆出負責鐵路設施建設與維護的[鐵路設施管理公司](../Page/鐵路設施管理公司.md "wikilink")（，），RENFE則專營[鐵路運輸服務](../Page/鐵路運輸.md "wikilink")。

除了貨運及城際、區域鐵路客運外，RENFE亦營運[AVE高速鐵路](../Page/AVE.md "wikilink")。1988年，RENFE興建全國首條[高速鐵路](../Page/西班牙高速鐵道.md "wikilink")，來往[馬德里至塞維利亞](../Page/馬德里—塞維利亞高速鐵路.md "wikilink")，1992年通車。另亦設有[往巴塞羅納、以及東北地區Tarragona及Huesca等的高速鐵路線](../Page/馬德里—巴塞隆納高速鐵路.md "wikilink")。

## 參見

  - [西班牙近郊鐵路](../Page/西班牙近郊鐵路.md "wikilink")
  - [西班牙高速鐵道](../Page/西班牙高速鐵道.md "wikilink")
  - [2013年聖地亞哥-德孔波斯特拉火車事故](../Page/2013年聖地亞哥-德孔波斯特拉火車事故.md "wikilink")

## 外部連結

  - [西班牙國家鐵路](http://www.renfe.com/)

[Category:西班牙铁路公司](../Category/西班牙铁路公司.md "wikilink")