[Hainosaurus_bernardi.JPG](https://zh.wikipedia.org/wiki/File:Hainosaurus_bernardi.JPG "fig:Hainosaurus_bernardi.JPG")（編號IRSNB
R23），位於[布魯塞爾自然史博物館](../Page/布魯塞爾.md "wikilink")\]\]
[Mosasaurus_skull.jpg](https://zh.wikipedia.org/wiki/File:Mosasaurus_skull.jpg "fig:Mosasaurus_skull.jpg")自然史博物館\]\]
[Tylosaurus_pembinensis_1DB.jpg](https://zh.wikipedia.org/wiki/File:Tylosaurus_pembinensis_1DB.jpg "fig:Tylosaurus_pembinensis_1DB.jpg")
**海諾龍屬**（屬名：*Hainosaurus*）是[滄龍科的一個屬](../Page/滄龍科.md "wikilink")，也是滄龍類中體型相當巨大的種類，是晚[白堊紀海洋的頂級掠食動物之一](../Page/白堊紀.md "wikilink")。海諾龍最初身長估計可達到17公尺\[1\]，之後被修改到約15公尺\[2\]，近年估計值則是12.2公尺\[3\]。海諾龍可能會獵食[海龜](../Page/海龜.md "wikilink")、[蛇頸龍類](../Page/蛇頸龍類.md "wikilink")、[頭足類](../Page/頭足類.md "wikilink")、[鯊魚](../Page/鯊魚.md "wikilink")、[魚類](../Page/魚類.md "wikilink")、甚至是其他的滄龍類。

海諾龍屬於[海王龍亞科](../Page/海王龍亞科.md "wikilink")，是生存於[北美洲](../Page/北美洲.md "wikilink")[海王龍的近親](../Page/海王龍.md "wikilink")，兩者都是大型海生掠食動物。海諾龍有53節脊椎骨帶有[人字骨](../Page/人字骨.md "wikilink")，而海王龍有35節脊椎骨帶有人字骨。海諾龍的帶有人字骨的尾椎數量較少，因此尾巴比海王龍還短。

海諾龍目前只有為一種（*H. bernardi*），屬名是以[比利時的海諾河](../Page/比利時.md "wikilink")（Haine
River）為名。但在2016年一份關於海諾龍的分析研究報告指出，海諾龍與海王龍實是同物種。

## 大眾文化

在[BBC的電視節目](../Page/BBC.md "wikilink")《[與恐龍共舞](../Page/與恐龍共舞.md "wikilink")》（*Walking
with Dinosaurs*）的特別節目《[海底霸王](../Page/海底霸王.md "wikilink")》（*Sea
Monsters*）中，出現了一隻巨大滄龍類。該滄龍類可能是海諾龍或是[海王龍](../Page/海王龍.md "wikilink")。

## 參考資料

<div class="references-small">

<references>

</references>

</div>

## 外部連結

  - <http://www.bbc.co.uk/sn/prehistoric_life/dinosaurs/seamonsters/>

[Category:滄龍科](../Category/滄龍科.md "wikilink")

1.  Russell, D. A. 1967. Systematics and morphology of American
    mosasaurs (Reptilia, Sauria). Yale Univ. Bull 23:241. pp.
2.  Lingham-Soliar, T. 1998. Unusual death of a Cretaceous giant.
    Lethaia 31:308–310.
3.  Johan Lindgren (2005) The first record of *Hainosaurus* (Reptilia:
    Mosasauridae) from Sweden. Journal of Paleontology: Vol. 79, No. 6,
    pp. 1157-1165