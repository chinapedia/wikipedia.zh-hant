**盧奇諾·維斯孔蒂·迪·莫德罗内，洛纳泰波佐洛伯爵**（，），[義大利](../Page/義大利.md "wikilink")[電影與](../Page/電影.md "wikilink")[舞台劇](../Page/舞台劇.md "wikilink")[導演](../Page/導演.md "wikilink")，出生於[米蘭](../Page/米蘭.md "wikilink")，對於[第二次世界大戰後的](../Page/第二次世界大戰.md "wikilink")[義大利電影有著相當重要的影響](../Page/義大利電影.md "wikilink")。

## 生平

維斯孔蒂出身於[米蘭的名門望族](../Page/米蘭.md "wikilink")[維斯孔蒂家族](../Page/維斯孔蒂家族.md "wikilink")，父親是格拉扎諾公爵朱塞佩·維斯孔蒂·迪·莫德罗内，母親是富商女兒，有六名兄弟姊妹。因母親喜愛藝術及音樂，所以維斯孔蒂自小已受良好的藝術教育，更透過家族關係認識了不少著名藝術家及音樂家，包括作家[加布里埃尔·邓南遮](../Page/加布里埃尔·邓南遮.md "wikilink")、作曲家[贾科莫·普契尼及指揮家](../Page/贾科莫·普契尼.md "wikilink")[阿图罗·托斯卡尼尼](../Page/阿图罗·托斯卡尼尼.md "wikilink")。成年後移居[巴黎](../Page/巴黎.md "wikilink")，認識了[可可·香奈尔](../Page/可可·香奈尔.md "wikilink")，更於此時左右放棄[法西斯主義](../Page/法西斯主義.md "wikilink")，轉為支持[馬克思主義](../Page/馬克思主義.md "wikilink")。

透過[可可·香奈尔的介紹](../Page/可可·香奈尔.md "wikilink")，維斯孔蒂得以在[讓·雷諾阿](../Page/讓·雷諾阿.md "wikilink")1930年代中的兩部電影《》及《》中任副導，從此進入電影界。兩片完成後，維斯孔蒂到[美國一遊](../Page/美國.md "wikilink")，期間亦曾到[荷里活](../Page/荷里活.md "wikilink")。維斯孔蒂回到意大利後，開始在雷諾阿的新片《[托斯卡](../Page/托斯卡.md "wikilink")》中工作，電影卻因[二戰爆發而被迫停工](../Page/二戰.md "wikilink")（電影最後由同是雷諾阿助手的完成）。其後，維斯孔蒂加入了[意大利共产党](../Page/意大利共产党.md "wikilink")。

維斯孔蒂與[羅伯托·羅塞里尼一同參與了](../Page/羅伯托·羅塞里尼.md "wikilink")（[贝尼托·墨索里尼之二子](../Page/贝尼托·墨索里尼.md "wikilink")）的導演工作坊，維斯孔蒂據說就是在這兒認識[费德里柯·费里尼的](../Page/费德里柯·费里尼.md "wikilink")。其後，維斯孔蒂與[贾科莫·普契尼](../Page/贾科莫·普契尼.md "wikilink")、及一起將[占士·肯恩名著](../Page/占士·肯恩.md "wikilink")《[郵差總按兩次鈴](../Page/郵差總按兩次鈴.md "wikilink")》改編成劇本，但卻找不到製片商答應投資。維斯孔蒂最後變賣了一些家傳珠寶，才籌足資金開拍此片，但電影完工後，卻受到法西斯政府的審查。幾經艱難下，維斯孔蒂的第一部電影《[驚情](../Page/驚情.md "wikilink")》才得以在1943年發行，在意大利非常賣座。

其後，意大利人民雖然推翻了[贝尼托·墨索里尼](../Page/贝尼托·墨索里尼.md "wikilink")，但德軍仍然佔霸著意大利不少地方。維斯孔蒂不但讓他的居所成為[共產黨](../Page/意大利共產黨.md "wikilink")[抵抗德軍的秘密總部](../Page/意大利抵抗運動.md "wikilink")，自己亦有參與戰鬥。

### 私人生活

維斯孔蒂是一名[同性戀者](../Page/同性戀.md "wikilink")，亦從未掩飾過他的性傾向。他最後一個伴侶是[奧地利演員](../Page/奧地利.md "wikilink")。保加曾在維斯孔蒂不少的電影中演出，包括《[納粹狂魔](../Page/納粹狂魔.md "wikilink")》（*The
Damned*）及《》，更在《[諸神的黃昏](../Page/諸神的黃昏_\(電影\).md "wikilink")》（）中扮演主角[路德维希二世](../Page/路德维希二世_\(巴伐利亚\).md "wikilink")。

維斯孔蒂其他的關係包括與曾在他不少電影及歌劇中任幕後人員的[法蘭高·齊費里尼](../Page/法蘭高·齊費里尼.md "wikilink")\[1\]。

## 作品

### 電影

  - 1943年：《[沉淪](../Page/沉淪_\(電影\).md "wikilink")》，改編自[占士·肯恩名著](../Page/占士·肯恩.md "wikilink")《[郵差總按兩次鈴](../Page/郵差總按兩次鈴.md "wikilink")》
  - 1945年：*Giorni di Gloria*, documentary
  - 1948年：《[大地震動](../Page/大地震動.md "wikilink")》 *La Terra trema*,
  - 1951年：*Appunti su un fatto di cronaca*, short film
  - 1951年：《》
  - 1953年：*Siamo donne* , episode *Anna Magnani*
  - 1954年：《[戰國妖姬](../Page/戰國妖姬.md "wikilink")》() ()
  - 1957年：《[白夜](../Page/白夜_\(電影\).md "wikilink")》 (*White Nights*),
    改編自[費奧多爾·陀思妥耶夫斯基的](../Page/費奧多爾·陀思妥耶夫斯基.md "wikilink")《[白夜](../Page/白夜.md "wikilink")》
  - 1960年：《》
  - 1961年：《》
    ，改編自[乔万尼·薄伽丘名著](../Page/乔万尼·薄伽丘.md "wikilink")《[十日談](../Page/十日談.md "wikilink")》中的
    *Il lavoro*
  - 1963年：《[浩氣蓋山河](../Page/氣蓋山河.md "wikilink")》 (*Il Gattopardo*)
  - 1965年：《[北斗七星](../Page/北斗七星_\(電影\).md "wikilink")》（*Vaghe stelle
    dell'Orsa*）
  - 1967年：《[異鄉人](../Page/異鄉人_\(電影\).md "wikilink")》 (*Lo straniero*),
    改編自[卡繆的](../Page/卡繆.md "wikilink")《[異鄉人](../Page/異鄉人.md "wikilink")》
  - 1967年：*Le streghe* , episode *La strega bruciata viva*
  - 1969年：《》（*La caduta degli dei*）
      - 提名[奧斯卡最佳原創劇本](../Page/奧斯卡最佳原創劇本.md "wikilink")
  - 1970年：*Alla ricerca di Tadzio*，電視電影
  - 1971年：《[魂斷威尼斯](../Page/魂斷威尼斯_\(電影\).md "wikilink")》，改編自[托馬斯·曼的](../Page/托馬斯·曼.md "wikilink")《[威尼斯之死](../Page/威尼斯之死.md "wikilink")》
      - 提名[英國電影學院獎最佳導演](../Page/英國電影學院獎最佳導演.md "wikilink")
  - 1972年：《[諸神的黃昏](../Page/諸神的黃昏_\(電影\).md "wikilink")》()
  - 1974年：《》（*Gruppo di famiglia in un interno*）
  - 1976年：《[淸白者](../Page/淸白者.md "wikilink")》()

### 歌劇

  - *[La vestale](../Page/La_vestale.md "wikilink")* by [Gaspare
    Spontini](../Page/Gaspare_Spontini.md "wikilink"), 1954, La Scala
    with Maria Callas
  - *[La sonnambula](../Page/La_sonnambula.md "wikilink")* by [Vincenzo
    Bellini](../Page/Vincenzo_Bellini.md "wikilink"), 1955, La Scala
    with Maria Callas, conducted by Leonard Bernstein
  - *[La traviata](../Page/La_traviata.md "wikilink")* by [Giuseppe
    Verdi](../Page/Giuseppe_Verdi.md "wikilink"), 1955, La Scala with
    Maria Callas, conducted by [Carlo Maria
    Giulini](../Page/Carlo_Maria_Giulini.md "wikilink")
  - *[Anna Bolena](../Page/Anna_Bolena.md "wikilink")* by [Gaetano
    Donizetti](../Page/Gaetano_Donizetti.md "wikilink"), 1957, La Scala
    with Maria Callas
  - *[Iphigénie en Tauride](../Page/Iphigénie_en_Tauride.md "wikilink")*
    by [Christoph Willibald
    Gluck](../Page/Christoph_Willibald_Gluck.md "wikilink"), 1957, La
    Scala with Maria Callas
  - *[Don Carlos](../Page/Don_Carlos.md "wikilink")* by [Giuseppe
    Verdi](../Page/Giuseppe_Verdi.md "wikilink"), 1958, [Royal Opera
    House](../Page/Royal_Opera_House.md "wikilink"), Covent Garden
  - *[Macbeth](../Page/Macbeth_\(opera\).md "wikilink")* by [Giuseppe
    Verdi](../Page/Giuseppe_Verdi.md "wikilink"), 1958, [Spoleto
    Festival](../Page/Spoleto_festival.md "wikilink")
  - *[Il duca d'Alba](../Page/Il_duca_d'Alba.md "wikilink")* by [Gaetano
    Donizetti](../Page/Gaetano_Donizetti.md "wikilink"), 1959, Spoleto
    Festival
  - *[Salome](../Page/Salome_\(opera\).md "wikilink")* by [Richard
    Strauss](../Page/Richard_Strauss.md "wikilink"), 1961, Spoleto
    Festival
  - *[Il diavolo in
    giardino](../Page/Il_diavolo_in_giardino.md "wikilink")* by [Franco
    Mannino](../Page/Franco_Mannino.md "wikilink") with libretto by
    Visconti, [Filippo Sanjust](../Page/Filippo_Sanjust.md "wikilink")
    and [Enrico Medioli](../Page/Enrico_Medioli.md "wikilink"), 1963,
    [Teatro Massimo](../Page/Teatro_Massimo.md "wikilink"),
    [Palermo](../Page/Palermo.md "wikilink")
  - *[La traviata](../Page/La_traviata.md "wikilink")* by [Giuseppe
    Verdi](../Page/Giuseppe_Verdi.md "wikilink"), 1963, Spoleto Festival
  - *[Le nozze di Figaro](../Page/Le_nozze_di_Figaro.md "wikilink")* by
    [Wolfgang Amadeus
    Mozart](../Page/Wolfgang_Amadeus_Mozart.md "wikilink"), 1964,
    [Teatro dell'Opera di
    Roma](../Page/Teatro_dell'Opera_di_Roma.md "wikilink") Rome
  - *[Il trovatore](../Page/Il_trovatore.md "wikilink")* by [Giuseppe
    Verdi](../Page/Giuseppe_Verdi.md "wikilink"), 1964, Royal Opera
    House, Covent Garden (Sanjust production); [Bolshoi
    Theatre](../Page/Bolshoi_Theatre.md "wikilink"), Moscow (Carlos
    Benois production)
  - *[Don Carlos](../Page/Don_Carlos.md "wikilink")* by [Giuseppe
    Verdi](../Page/Giuseppe_Verdi.md "wikilink"), 1965, Rome Opera
  - *[Falstaff](../Page/Falstaff_\(opera\).md "wikilink")* by [Giuseppe
    Verdi](../Page/Giuseppe_Verdi.md "wikilink"), 1966,
    [Staatsoper](../Page/Vienna_State_Opera.md "wikilink"),
    [Vienna](../Page/Vienna.md "wikilink"), with [Dietrich
    Fischer-Dieskau](../Page/Dietrich_Fischer-Dieskau.md "wikilink"),
    conducted by [Leonard
    Bernstein](../Page/Leonard_Bernstein.md "wikilink")
  - *[Der Rosenkavalier](../Page/Der_Rosenkavalier.md "wikilink")* by
    [Richard Strauss](../Page/Richard_Strauss.md "wikilink"), 1966,
    Royal Opera House, Covent Garden
  - *[La traviata](../Page/La_traviata.md "wikilink")* by [Giuseppe
    Verdi](../Page/Giuseppe_Verdi.md "wikilink"), 1967, Royal Opera
    House, Covent Garden with [Mirella
    Freni](../Page/Mirella_Freni.md "wikilink")
  - *[Simon Boccanegra](../Page/Simon_Boccanegra.md "wikilink")* by
    [Giuseppe Verdi](../Page/Giuseppe_Verdi.md "wikilink"), 1969,
    Staatsoper, Vienna, with [Eberhard
    Wächter](../Page/Eberhard_Waechter_\(baritone\).md "wikilink"),
    conducted by [Josef Krips](../Page/Josef_Krips.md "wikilink")
  - *[Manon Lescaut](../Page/Manon_Lescaut_\(Puccini\).md "wikilink")*
    by [Giacomo Puccini](../Page/Giacomo_Puccini.md "wikilink"), 1973,
    Spoleto Festival, with [Nancy
    Shade](../Page/Nancy_Shade.md "wikilink") and [Harry
    Theyard](../Page/Harry_Theyard.md "wikilink")

## 參考

## 外部連結

  -
  - [British Film Institute, Luchino
    Visconti](https://web.archive.org/web/20080219155322/http://www.bfi.org.uk/features/visconti/)

  - [David Thomson, "The decadent realist", *The Guardian* (London) 15
    February
    2003](http://books.guardian.co.uk/review/story/0,12084,894896,00.html)

[Category:義大利導演](../Category/義大利導演.md "wikilink")
[Category:LGBT導演](../Category/LGBT導演.md "wikilink")
[Category:坎城影展獲獎者](../Category/坎城影展獲獎者.md "wikilink")
[Category:威尼斯影展獲獎者](../Category/威尼斯影展獲獎者.md "wikilink")
[Category:米蘭人](../Category/米蘭人.md "wikilink")
[Category:義大利共產黨黨員](../Category/義大利共產黨黨員.md "wikilink")

1.