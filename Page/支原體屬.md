[缩略图](https://zh.wikipedia.org/wiki/File:Spiro.jpg "fig:缩略图")

**支原体**是一类无细胞壁结构、介于独立生活和细胞内寄生生活之间的最小的[原核生物](../Page/原核生物.md "wikilink")。许多种类可使人和动物致病，有些腐生种类生活在土壤、污水和堆肥中\[1\]。屬[厚壁菌門](../Page/厚壁菌門.md "wikilink")[柔膜菌綱](../Page/柔膜菌綱.md "wikilink")，可以在培养基上形成极小的[菌落](../Page/菌落.md "wikilink")。由於不具細胞壁，許多常見的[抗生素](../Page/抗生素.md "wikilink")，如[盤尼西林或](../Page/盤尼西林.md "wikilink")[β－內醯胺類抗生素對支原體是無效的](../Page/β－內醯胺.md "wikilink")。許多種類的支原體可導致疾病，如[肺炎支原體](../Page/肺炎支原體.md "wikilink")，是某些[非典型肺炎與其他呼吸疾病的](../Page/非典型肺炎.md "wikilink")[病原體](../Page/病原體.md "wikilink")。[生殖道支原體則會引起](../Page/生殖道支原體.md "wikilink")[骨盆腔發炎](../Page/骨盆腔.md "wikilink")。

支原体少数可以自由生活在静水中，但多数存在于人类与[动物的](../Page/动物.md "wikilink")[消化道](../Page/消化道.md "wikilink")、[呼吸道和](../Page/呼吸道.md "wikilink")[泌尿生殖道中](../Page/泌尿生殖道.md "wikilink")，可导致疾病。有的支原体可导致[植物病害](../Page/植物.md "wikilink")。

## 特徵

目前已發現的支原體超過一百多種，屬於[柔膜菌綱](../Page/柔膜菌綱.md "wikilink")。柔膜菌綱寄生於人類與其他動植物身上（包括昆蟲），但也可形成[偏利共生](../Page/偏利共生.md "wikilink")。[膽固醇是支原體以及其他柔膜菌綱菌種生存的必要條件](../Page/膽固醇.md "wikilink")。支原體最理想的生存溫度是宿主的體溫，或是宿主喪失調控自體溫度的機能時的外部環境溫度。透過16S[核糖體RNA序列與基因的分析指出](../Page/核糖體RNA.md "wikilink")，柔膜菌綱（包括支原體在內）非常接近[乳酸桿菌與](../Page/乳酸桿菌.md "wikilink")[梭狀芽胞桿菌](../Page/梭狀芽胞桿菌.md "wikilink")。

### 細胞結構

支原體及其相近菌種都缺乏細胞壁，但菌體卻能保持一定形狀。細胞的特定形狀可能使支原體能夠在不同的環境中自我繁殖。

例如，肺炎支原體從自身粒狀體細胞延伸出「尖端結構」，能夠附著於宿主細胞，也能幫助在固體表面滑行，此外在細胞分裂過程中也扮演重要的角色。肺炎支原體非常微小，形狀也很多種，但大致上像縱切的圓底燒瓶。

支原體在所有細菌中獨樹一格，需要[固醇才能維持](../Page/固醇.md "wikilink")[細胞膜的穩定](../Page/細胞膜.md "wikilink")。支原體所需的固醇來自外部環境，通常是動物宿主。支原體的[基因組一般只有](../Page/基因組.md "wikilink")0.58－1.38百萬[鹼基](../Page/鹼基.md "wikilink")，使得它的生物合成能力薄弱，必須依賴宿主才能進行合成。此外支原體遺傳密碼中的密碼子UGA能夠編譯成[色胺酸](../Page/色胺酸.md "wikilink")，而不是一般的乳白色[終止碼](../Page/終止碼.md "wikilink")。

### 染色特征

因其无细胞壁，故呈革兰氏阴性，且形态容易变化，对渗透压较为敏感，对抑制细胞壁合成的抗生素不敏感。

### 菌落特征

菌落较小（直径0.1～1.0毫米），其在固体培养基表面呈现特有的“油煎蛋”形。

### 繁殖方式

以二分裂或出芽等常见的方式进行繁殖。

### 药物耐受性

因无细胞壁，所以对青霉素等破坏细胞壁类药物不敏感，但对能抑制蛋白质生物合成的抗生素，如红霉素、四环素等，以及破坏含有甾体结构的细胞膜结构的抗生素（如两性霉素、制霉菌素等等）非常敏感。

### 基因组较小

支原体的遗传基因组很小，仅在0.6～1.1Mb左右，约为大肠杆菌的1/4～1/5.肺炎支原体（*Mycoplasma
pneumoniae*）的基因组只有0.81Mb.生殖道支原体（*M.genitalium*）的基因组更小，只有0.58Mb,470个基因。

## 菌落培養

1898年，諾卡德（Nocard）與儒克斯（Roux）兩人培養了引起[傳染性胸膜性肺炎](../Page/:en:Contagious_bovine_pleuropneumonia.md "wikilink")（CBPP）的細菌，該種細菌在當時造成了牛隻嚴重的大規模感染\[2\]\[3\]。傳染性胸膜性肺炎由[絲狀黴漿菌中的絲狀生物型小菌落](../Page/:en:Mycoplasma_mycoides.md "wikilink")（SC）所引起，而諾卡德與儒克斯是歷史上首次培養黴漿菌的人。然而由於黴漿菌複雜的生長條件，菌體的培養至今仍困難重重。

## 微小的基因組

近來拜[分子生物學與](../Page/分子生物學.md "wikilink")[基因組學的進步所賜](../Page/基因組學.md "wikilink")，基因構造簡單的黴漿菌，尤其是[肺炎黴漿菌及其近親](../Page/:en:M._pneumoniae.md "wikilink")[生殖道黴漿菌](../Page/:en:Mycoplasma_genitalium.md "wikilink")，已引起了較為廣大的注意。科學家發表了生殖道黴漿菌完整的基因組序列，指出該菌種是擁有最小基因組的生物體之一,能夠獨立存在\[4\]。稍後肺炎黴漿菌的基因組序列也得到了發表。肺炎黴漿菌是基因定序史上首次以[引物步移法對](../Page/:en:primer_walking.md "wikilink")[黏質體資料庫進行定序](../Page/黏質體.md "wikilink")，而非以傳統的[霰彈槍定序法](../Page/霰彈槍定序法.md "wikilink")\[5\]。科學家在基因組學及[蛋白質組學持續研究黴漿菌](../Page/蛋白質組學.md "wikilink")，試圖了解所謂的[辛西婭人工生命](../Page/辛西婭.md "wikilink")\[6\]、編目完整的細胞蛋白質結構\[7\]，並繼續利用這些生物體微小基因組的優勢來進行浩如煙海的生物學研究。

## 分類

  - [柔膜菌纲](../Page/柔膜菌纲.md "wikilink")
      - [支原体目](../Page/支原体目.md "wikilink")
          - [支原体科](../Page/支原体科.md "wikilink")
              - [支原体属](../Page/支原体属.md "wikilink")

## 参考文献

## 外部連結

  - [微生物免疫學-Mycoplasma
    (黴漿菌)](http://smallcollation.blogspot.com/2013/01/mycoplasma-pneumonia.html)
  - [合成細菌基因
    人造生物大突破](http://only-perception.blogspot.com/2008/01/blog-post_5352.html)
  - [改造黴漿菌基因](http://scimonth.blogspot.com/2010/07/20107487_30.html)

## 參見

  - [支原体感染](../Page/支原体感染.md "wikilink")
  - [細菌分類表](../Page/細菌分類表.md "wikilink")

{{-}}

[Category:軟壁菌門](../Category/軟壁菌門.md "wikilink")
[支原体属](../Category/支原体属.md "wikilink")
[Category:微生物](../Category/微生物.md "wikilink")

1.
2.  Nocard, E.I.E. & Roux, E.; *Le microbe de la péripneumonie.* Ann
    Inst Pasteur **12**, 240-262. (Translated as ‘The microbe of
    pleuropneumonia’ in Rev Infect Dis **12**, 354-358 (1990))
3.
4.
5.
6.
7.