Pentium D / Pentium Extreme
Edition為[英特爾推出的第七代微處理器](../Page/英特爾.md "wikilink")。

## 雙核心桌面處理器

### Pentium D

#### "Smithfield" (90 nm)

  - 指令集：[MMX](../Page/MMX.md "wikilink")，[SSE](../Page/SSE.md "wikilink")，[SSE2](../Page/SSE2.md "wikilink")，[SSE3](../Page/SSE3.md "wikilink"),
    [XD
    bit](../Page/XD_bit.md "wikilink")，[EM64T](../Page/EM64T.md "wikilink")
  - 830及840支援[EIST](../Page/EIST.md "wikilink")

| 型號            | s-Spec 代號  | 時脈頻率              | L2 快取記憶體  | [前端匯流排](../Page/前端匯流排.md "wikilink") | 電壓         | [TDP](../Page/TDP.md "wikilink") | 插槽                                       | 發行日期       |
| ------------- | ---------- | ----------------- | --------- | ------------------------------------ | ---------- | -------------------------------- | ---------------------------------------- | ---------- |
| Pentium D 805 | SL8ZH (B0) | 2.66 GHz (133×20) | 2×1024 KB | 533 MT/s                             | 1.25/1.4 V | 95 W                             | [LGA 775](../Page/LGA_775.md "wikilink") | 2005年12月   |
| Pentium D 820 | SL8CP (B0) | 2.80 GHz (200×14) | 2×1024 KB | 800 MT/s                             | 1.25/1.4 V | 95 W                             | [LGA 775](../Page/LGA_775.md "wikilink") | 2005年5月26日 |
| Pentium D 830 | SL8CN (B0) | 3.00 GHz (200×15) | 2×1024 KB | 800 MT/s                             | 1.25/1.4 V | 130 W                            | [LGA 775](../Page/LGA_775.md "wikilink") | 2005年5月26日 |
| Pentium D 840 | SL8CM (B0) | 3.20 GHz (200×16) | 2×1024 KB | 800 MT/s                             | 1.25/1.4 V | 130 W                            | [LGA 775](../Page/LGA_775.md "wikilink") | 2005年5月26日 |

#### "Presler" (65 nm)

  - 指令集：[MMX](../Page/MMX.md "wikilink")，[SSE](../Page/SSE.md "wikilink")，[SSE2](../Page/SSE2.md "wikilink")，[SSE3](../Page/SSE3.md "wikilink")，[EIST](../Page/EIST.md "wikilink")，[XD
    bit](../Page/XD_bit.md "wikilink")，[EM64T](../Page/EM64T.md "wikilink")
  - 920，930，940，950及960支援[Intel VT](../Page/Intel_VT.md "wikilink")

| 型號            | s-Spec 代號  | 時脈頻率              | L2 快取記憶體  | [前端匯流排](../Page/前端匯流排.md "wikilink") | 電壓          | [TDP](../Page/TDP.md "wikilink") | 插槽                                       | 發行日期       |
| ------------- | ---------- | ----------------- | --------- | ------------------------------------ | ----------- | -------------------------------- | ---------------------------------------- | ---------- |
| Pentium D 915 | SL9KB (D0) | 2.80 GHz (200×14) | 2×2048 KB | 800 MT/s                             | 1.2/1.337 V | 95 W                             | [LGA 775](../Page/LGA_775.md "wikilink") | 2006年7月23日 |
| Pentium D 920 | SL95Y (C1) | 2.80 GHz (200×14) | 2×2048 KB | 800 MT/s                             | 1.2/1.337 V | 95 W                             | [LGA 775](../Page/LGA_775.md "wikilink") | 2006年1月16日 |
| Pentium D 925 | SL9KA (D0) | 3.00 GHz (200×15) | 2×2048 KB | 800 MT/s                             | 1.2/1.337 V | 95 W                             | [LGA 775](../Page/LGA_775.md "wikilink") | 2006年7月23日 |
| Pentium D 930 | SL95X (C1) | 3.00 GHz (200×15) | 2×2048 KB | 800 MT/s                             | 1.2/1.337 V | 95 W                             | [LGA 775](../Page/LGA_775.md "wikilink") | 2006年1月16日 |
| Pentium D 935 | SL9QR (D0) | 3.20 GHz (200×16) | 2×2048 KB | 800 MT/s                             | 1.2/1.337 V | 95 W                             | [LGA 775](../Page/LGA_775.md "wikilink") | 2007年1月    |
| Pentium D 940 | SL94Q (B1) | 3.20 GHz (200×16) | 2×2048 KB | 800 MT/s                             | 1.2/1.337 V | 130 W                            | [LGA 775](../Page/LGA_775.md "wikilink") | 2006年1月16日 |
| SL95W (C1)    | 95 W       |                   |           |                                      |             |                                  |                                          |            |
| Pentium D 945 | SL9QQ (D0) | 3.40 GHz (200×17) | 2×2048 KB | 800 MT/s                             | 1.2/1.337 V | 95 W                             | [LGA 775](../Page/LGA_775.md "wikilink") | 2006年7月23日 |
| Pentium D 950 | SL94P (B1) | 3.40 GHz (200×17) | 2×2048 KB | 800 MT/s                             | 1.2/1.337 V | 130 W                            | [LGA 775](../Page/LGA_775.md "wikilink") | 2006年1月16日 |
| SL9K8 (D0)    | 95 W       |                   |           |                                      |             |                                  |                                          |            |
| Pentium D 960 | SL9AP (C1) | 3.60 GHz (200×18) | 2×2048 KB | 800 MT/s                             | 1.2/1.337 V | 130 W                            | [LGA 775](../Page/LGA_775.md "wikilink") | 2006年5月2日  |
| SL9K7 (D0)    | 95 W       |                   |           |                                      |             |                                  |                                          |            |

### Pentium Extreme Edition

#### "Smithfield" (90 nm)

  - 指令集：[MMX](../Page/MMX.md "wikilink")，[SSE](../Page/SSE.md "wikilink")，[SSE2](../Page/SSE2.md "wikilink")，[SSE3](../Page/SSE3.md "wikilink")，[EIST](../Page/EIST.md "wikilink")，[XD
    bit](../Page/XD_bit.md "wikilink")，[EM64T](../Page/EM64T.md "wikilink")，[超執行緒](../Page/超執行緒.md "wikilink")

| 型號                          | s-Spec 代號  | 時脈頻率              | L2 快取記憶體  | [前端匯流排](../Page/前端匯流排.md "wikilink") | 電壓        | [TDP](../Page/TDP.md "wikilink") | 插槽                                       | 發行日期      |
| --------------------------- | ---------- | ----------------- | --------- | ------------------------------------ | --------- | -------------------------------- | ---------------------------------------- | --------- |
| Pentium Extreme Edition 840 | SL8FK (A0) | 3.20 GHz (200×16) | 2×1024 KB | 800 MT/s                             | 1.2/1.4 V | 130 W                            | [LGA 775](../Page/LGA_775.md "wikilink") | 2005年5月1日 |

#### "Presler" (65 nm)

  - 指令集：[MMX](../Page/MMX.md "wikilink")，[SSE](../Page/SSE.md "wikilink")，[SSE2](../Page/SSE2.md "wikilink")，[SSE3](../Page/SSE3.md "wikilink")，[EIST](../Page/EIST.md "wikilink")，[xD](../Page/NX位元.md "wikilink")，[EM64T](../Page/EM64T.md "wikilink")，[超執行緒](../Page/超執行緒.md "wikilink")，[Intel
    VT](../Page/Intel_VT.md "wikilink")

| 型號                          | s-Spec 代號          | 時脈頻率              | L2 快取記憶體  | [前端匯流排](../Page/前端匯流排.md "wikilink") | 電壓          | [TDP](../Page/TDP.md "wikilink") | 插槽                                       | 發行日期       |
| --------------------------- | ------------------ | ----------------- | --------- | ------------------------------------ | ----------- | -------------------------------- | ---------------------------------------- | ---------- |
| Pentium Extreme Edition 955 | SL94N / SL8WM (B1) | 3.46 GHz (266×13) | 2×2048 KB | 1066 MT/s                            | 1.2/1.337 V | 130 W                            | [LGA 775](../Page/LGA_775.md "wikilink") | 2006年1月16日 |
| Pentium Extreme Edition 965 | SL9AN (C1)         | 3.73 GHz (266×14) | 2×2048 KB | 1066 MT/s                            | 1.2/1.337 V | 130 W                            | [LGA 775](../Page/LGA_775.md "wikilink") | 2006年3月22日 |

## 另見

  - [Pentium D](../Page/Pentium_D.md "wikilink")
  - [Pentium Extreme
    Edition](../Page/Pentium_Extreme_Edition.md "wikilink")

## 外部連結

[Pentium D/Pentium Extreme
Edition技術資料](https://web.archive.org/web/20060426222841/http://balusc.xs4all.nl/srv/har-cpu-int-pd.php)

[\*](../Category/Intel_x86處理器.md "wikilink")
[\*](../Category/Intel处理器列表.md "wikilink")