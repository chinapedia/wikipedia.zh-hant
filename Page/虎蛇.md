**虎蛇**（[學名](../Page/學名.md "wikilink")：**
）是[爬蟲類](../Page/爬蟲類.md "wikilink")[有鱗目](../Page/有鱗目.md "wikilink")[眼鏡蛇科下的一個種屬](../Page/眼鏡蛇科.md "wikilink")。虎蛇屬於有毒的蛇類，主要分布於[澳洲南部](../Page/澳洲.md "wikilink")，包括南澳海岸地區及[塔斯曼尼亞](../Page/塔斯曼尼亞.md "wikilink")。此類蛇有多種體色及體紋形態，深淺相間，而且紋路曲折，就像[老虎身上的斑紋一樣](../Page/老虎.md "wikilink")，因此得名。虎蛇在很多國家中都是受保護動物，捕殺或傷害虎蛇者最高罰款是[美金四千元](../Page/美金.md "wikilink")。

## 特徵

虎蛇是澳洲[亞熱帶地區一種體型較大的毒蛇](../Page/亞熱帶.md "wikilink")，牠們是蛇亞目中一個很獨特的群體，同種屬中的外型、體色、大小分別可以相當大。部分虎蛇的體色更會因應[季節性而有所改變](../Page/季節.md "wikilink")。一般虎蛇的身體長達1.45[米](../Page/米_\(單位\).md "wikilink")，體紋寬闊，有明顯的光暗落差。虎蛇的體色大致有[橄欖色](../Page/橄欖色.md "wikilink")、暗[黃色](../Page/黃.md "wikilink")、[橙](../Page/橙.md "wikilink")[棕色及](../Page/棕.md "wikilink")[黑色等](../Page/黑色.md "wikilink")，蛇腹比較淺色。虎蛇以毒素殺死獵物，也會向侵略者施展凌厲的咬擊，有令人類致命的能力。牠們能夠忍受低溫，不過對虎蛇而言，最佳的活動時刻是較為溫暖的夜晚。\[1\]雌性的虎蛇每次能誕下12至40條幼蛇，唯獨紀錄中有一條東方虎蛇曾經一次誕下64條幼蛇，是較為罕見的例子。一般而言，虎蛇沒有強烈的侵略性，遇到威脅時如果做得到的話牠們都會先選擇逃逸，而不是主動進擊。如果威脅迫近，牠們會緩緩壓平身子繃緊肌肉，昂起頭部，準備隨時彈出咬擊；有時亦會發出嘶嘶聲響，藉以恫嚇對手。

虎蛇雖然盛產於澳洲南部，但實際上牠們的分佈相當廣泛。以下介紹幾個主要的品種：

### 普通虎蛇

普通虎蛇（*Common tiger
snake*），又名澳大利亚虎蛇，或虎蛇。頭部較為巨大，身體亦較為粗壯，強健的肌肉令牠們在移動或享受日光浴時可以更有效地壓平身子，更為靈活。通常體長接近一米，有多種體色，基本上以棕色、[灰色](../Page/灰色.md "wikilink")、橄欖色為主，有時也會發現身體沒有虎紋的種類。鱗片突出，像重疊的[盾牌](../Page/盾.md "wikilink")，尤其是[頸部附近的鱗片](../Page/頸部.md "wikilink")，更為密集。

### 西方虎蛇

西方虎蛇（*Western tiger
snake*）同樣有強健的體型及巨大的頭部，最長長度達兩米。背部的體紋呈[鋼青色及黑色](../Page/鋼青色.md "wikilink")，與黃色的條紋組成虎紋，間中亦能找到沒有明顯體紋的種類。腹部呈黃色。

### 查佩爾島虎蛇

[Chappell_Island_tiger_snake.jpg](https://zh.wikipedia.org/wiki/File:Chappell_Island_tiger_snake.jpg "fig:Chappell_Island_tiger_snake.jpg")
查佩爾島虎蛇（*Chappell Island tiger
snake*）是虎蛇屬中最巨型的品種，平均長度達兩米，背部紋理呈橄欖色至黑色，腹部同樣較為淺色。這品種在幼蛇階段就已經有完整的交錯體紋。

### 帝島虎蛇

帝島虎蛇（*King Island*）及另一種[塔斯曼尼亞虎蛇](../Page/塔斯曼尼亞.md "wikilink")（*Tasmanian
tiger
snakes*）亦是虎蛇的主要品種，二者形態較為相似。牠們的幼蛇階段較為幼小，成長後體長可長達1.5米。背部體紋呈黑色並與較淺色的體紋交錯成虎紋，在某些情況下黑色的體紋會被黑色或灰色的斑點取代，形成黯淡的體紋，甚或沒有明顯體紋。

### 半島虎蛇

半島虎蛇（*Peninsula tiger
snake*）平均體長約有1.1米，體型在虎蛇屬中較為細小。嘴部附近的鱗片呈白色，背部體紋呈黑色。在[袋鼠島一帶](../Page/袋鼠島.md "wikilink")，半島虎蛇有多種體色，體紋組織亦較為豐富。

## 棲息環境

[20060306_King_Island_Tiger_Snake.jpg](https://zh.wikipedia.org/wiki/File:20060306_King_Island_Tiger_Snake.jpg "fig:20060306_King_Island_Tiger_Snake.jpg")
虎蛇主要分佈在海岸、濕地或溪澗等地帶，牠們有刻意佔據領地的習性。一些居住了大量[蛙類或其它獵物的地區](../Page/蛙.md "wikilink")，更是虎蛇經常聚居的集中地。另外，虎蛇所佔領的島嶼上亦有不少[海鷗聚集生活](../Page/海鷗.md "wikilink")，虎蛇亦會捕食這些海鷗的幼兒，不過亦有不少體型較大的虎蛇會被海鷗啄擊致盲。虎蛇的分佈地由[西澳南部伸展至](../Page/西澳大利亞州.md "wikilink")[南澳](../Page/南澳大利亞州.md "wikilink")、塔斯曼尼亞、[維多利亞州以至](../Page/維多利亞州.md "wikilink")[新南威爾士一帶](../Page/新南威爾士.md "wikilink")。除了海岸地區外，虎蛇亦常出沒於內陸水道，與及[墨瑞河等地方](../Page/墨累河.md "wikilink")。另外，一些聚居於濕地的虎蛇，由於其居地近年受到當地郊區發展的影響，虎蛇的主要食物來源如蛙類數字因而大大減少，直接影響到虎蛇的生態。

## 分類學

虎蛇隸屬於[眼鏡蛇科](../Page/眼鏡蛇科.md "wikilink")，其下有兩個較為人知並得到普遍認同的品種，分別是**澳大利亞虎蛇**（*Notechis
scutatus*）及**黑虎蛇**（*Notechis
ater*），這兩個品種的虎蛇均有著更顯著的虎蛇特徵。\[2\]很多研究者曾經為虎蛇屬的各個品種加以命名，可是這些命名大都是沒有得到正式授權的，因此不受普遍認同。\[3\]

### 品種

| 種                         | 命名者          | 亞種 | 英語                   | 地理分佈                                                   |
| ------------------------- | ------------ | -- | -------------------- | ------------------------------------------------------ |
| **黑虎蛇**（*N. ater*）        | Krefft，1866年 | 3  | Black tiger snake    | 澳洲（西澳、南澳、塔斯曼尼亞）                                        |
| **澳大利亞虎蛇**（*N. scutatus*） | Peters，1861年 | 1  | Mainland tiger snake | 澳洲（新南威爾士、[昆士蘭](../Page/昆士蘭.md "wikilink")、南澳、維多利亞州、西澳） |
|                           |              |    |                      |                                                        |

## 毒性

虎蛇分泌強烈的神經毒素、凝固劑、溶血素及蛇類特有的[肌肉毒素](../Page/肌肉.md "wikilink")，其毒性能躋身世界最強烈的蛇毒之列。被虎蛇所咬後，除了傷口劇痛之外，從傷口附近延伸的毒素更會令足部及頸部出現痛楚，身體感到麻痺、出汗，隨即開始呼吸困難及局部肢體癱瘓。即使有有效的抗蛇毒素，但如果不立即治療的話，致命率仍高達45%。

被澳洲一帶毒蛇所咬的治療方法，大致上相同。一般而言，為了有效抑制蛇毒在[淋巴系統擴散](../Page/淋巴系統.md "wikilink")，治療初步都會使用「壓力穩定法」（Pressure
Immobilization
Method）。治療時會在被咬的傷口附近縛上寬闊的繃帶，並把整個肢段（[手或](../Page/手.md "wikilink")[腳](../Page/腳.md "wikilink")）都緊緊包裹著，並以木板加以固定。如果在蛇咬傷口附近有留下痕跡的話，蛇毒便可能得到確認，並能施以針對性的治療。抗蛇毒素（或血清）的存在大大減少被虎蛇所咬傷的致命率，目前由虎蛇咬擊所致的死亡數字，已經被另一種毒蛇[褐蛇所超越了](../Page/褐蛇.md "wikilink")。\[4\]

## 備註

  -
## 參考資料

  - [整合分類學資訊系統](../Page/整合分類學資訊系統.md "wikilink")：[虎蛇](http://www.itis.gov/servlet/SingleRpt/SingleRpt?search_topic=TSN&search_value=700234)

  - [雪梨大學出版社](../Page/雪梨大學.md "wikilink")：《[澳洲蛇咬](https://web.archive.org/web/20080215234205/http://www.usyd.edu.au/anaes/venom/snakebite.html)》。

## 外部連結

  - [遭虎蛇所咬時的應變措施](http://www.survivaliq.com/survival/poisonous-snakes-and-lizards-tiger-snake.htm)
  - [snakeshow.net：關於虎蛇的種屬](http://www.snakeshow.net/downloads/snake-tigersnakesofthegenusnotechis.pdf)
  - [整合分類學資訊系統](../Page/整合分類學資訊系統.md "wikilink") -
    [虎蛇屬](http://www.itis.usda.gov/servlet/SingleRpt/SingleRpt?search_topic=TSN&search_value=700234)

[Category:眼鏡蛇科](../Category/眼鏡蛇科.md "wikilink")
[Category:西澳大利亞州](../Category/西澳大利亞州.md "wikilink")

1.
2.  [整合分類學資訊系統：虎蛇](http://www.itis.gov/servlet/SingleRpt/SingleRpt?search_topic=TSN&search_value=700234)
3.  [爬蟲類資料庫：虎蛇](http://reptile-database.reptarium.cz/species.php?genus=Notechis&species=scutatus)
4.