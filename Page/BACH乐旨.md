[B-a-c-h.svg](https://zh.wikipedia.org/wiki/File:B-a-c-h.svg "fig:B-a-c-h.svg")
在[音乐中](../Page/音乐.md "wikilink")，**BACH乐旨**指的是由 **B**、**A**、**C**、**B**
四个音符组成的序列。由于在[德语中](../Page/德语.md "wikilink") **B**
指的是[英语中的](../Page/英语.md "wikilink") **B**，而 **H**
指的是英语中的
**B**，所以这四个音符正好串成了作曲家"[J.S.巴赫](../Page/约翰·塞巴斯蒂安·巴赫.md "wikilink")"的名字（*Johann
Sebastian **BACH***，1685-1750）。

很多作曲家都使用這個旋律動機，以表示对巴赫的尊敬。但第一个已知的例子却是[扬·皮特森·斯韦林克](../Page/斯韦林克.md "wikilink")
(*Jan Pieterszoon Sweelinck*)
的一部作品。他创作该作品也有可能是为了向巴赫的某位祖先表达敬意。巴赫的祖先很多都是音乐家。

到了19世纪，由於公众再次燃起对巴赫的兴趣，这動機才再次被提起及使用。

## 巴赫的作品

[Kuhnau.tristis.jpg](https://zh.wikipedia.org/wiki/File:Kuhnau.tristis.jpg "fig:Kuhnau.tristis.jpg")
[Musik.beispiel.b-a-c-h.jpg](https://zh.wikipedia.org/wiki/File:Musik.beispiel.b-a-c-h.jpg "fig:Musik.beispiel.b-a-c-h.jpg")
巴赫的《降B大調前奏與賦格曲》，[BWV](../Page/BWV.md "wikilink")
898，是一首明乎其實以BACH動機所寫成的樂曲，在賦格曲一開頭便可以聽到由B<sub>4</sub>、A<sub>4</sub>、C<sub>5</sub>及B<sub>4</sub>。但這首曲是否真正由巴赫所寫，在學術界仍有爭議。支持的學者相信這是巴赫於1717年時所創作的作品，而巴赫的表兄，同樣是作曲家華爾特（）在其日記中亦記錄過巴赫向他示範過一首以他自己名字所寫成的樂曲。然而，持懷疑的學者則認為對此曲的寫作手法、風格及實際效果都和主流的巴赫作品有很大的出入。

而巴赫未完成的作品《[赋格的艺术](../Page/赋格的艺术.md "wikilink")》（Die Kunst der Fuge, BWV
1080，1750年）的最后部分也是用了這個動機；另外，在卡农变奏曲《從至高天我今降臨》（Canonic Variations on the
Christmas Hymn ‘Vom Himmel hoch, da komm ich her’，BWV 769
No.7）第四次变奏的末段也有用上。

至於一首稱為《小型的和聲迷宫》（Kleines harmonisches Labyrinth, BWV
591）中，引子部份（Introitus）的倒数第二小节，出現了一組以八分音符所組成的BACH動機，但嚴格來講，巴赫似乎並非刻意地用上（而且这部作品是否由巴赫所寫亦成疑問，有人推测真正作者是另一位德國作曲家）。

巴赫的儿子[约翰·克里斯蒂安·巴赫](../Page/约翰·克里斯蒂安·巴赫.md "wikilink")（*Johann Christian
Bach*，簡稱J.C.Bach）的一首作品《為BACH動機而寫的半音賦格曲》（Chromatic Fugue on
BACH），除了保留原來的音型外，亦將它進行移位和變型。\[1\]

巴赫亦曾改編過多位作曲家的作品，其中他將（Johann
Kuhnau）的一首經文歌改編，並在[女低音聲部中加插了BACH動機](../Page/女低音.md "wikilink")。

## 其他作曲家

基於巴赫自己也用上了BACH動機來創作赋格曲，因此其他作曲家亦愛把此用於自己的作品中，或者作其他複合對位法的寫作。

以下是明显带有BACH動機的作品包括有（按时间先后排序）：

  - [罗伯特·舒曼](../Page/罗伯特·舒曼.md "wikilink")(*Robert Schumann*) -
    为[管风琴](../Page/管风琴.md "wikilink")，踏板钢琴或脚踏式风琴而作的六首赋格，opus
    60（1845年）
  - [弗朗兹·李斯特](../Page/弗朗兹·李斯特.md "wikilink")(*Franz Liszt*) -
    为管风琴而作的《B-A-C-H主题幻想曲与赋格》（1855年时[改编](../Page/改编_\(音乐\).md "wikilink")
    成 [钢琴曲](../Page/钢琴.md "wikilink")）
  - [里姆斯基-柯萨柯夫](../Page/里姆斯基-柯萨柯夫.md "wikilink")(*Nikolai
    Rimsky-Korsakov*) - 为钢琴而作的《巴赫变奏曲》（1878年）
  - [马克斯·雷格](../Page/马克斯·雷格.md "wikilink")(*Max Reger*) -
    为管风琴而作的《幻想与赋格巴赫变奏曲》（1900年）
  - [卡尔·尼尔森](../Page/卡尔·尼尔森.md "wikilink")(*Carl Nielsen*) - 《小提琴协奏曲（Op.
    33/FS 61）》（1911年）
  - [布索尼](../Page/布索尼.md "wikilink")(*Ferruccio Busoni*) -
    为钢琴而作的《對位幻想曲》（第一版1910年，續版1912年及1922年）
  - [奥涅格](../Page/奥涅格.md "wikilink")(*Arthur Honegger*) -
    为钢琴而作的《前曲，詠嘆，賦格》（1932年，後來為弦樂團改編）
  - [弗朗西·普朗克](../Page/弗朗西·普朗克.md "wikilink")(*Francis Poulenc*) -
    为钢琴而作的《巴赫即興華爾茲》（1932年）
  - [安东·冯·韦伯恩](../Page/安东·冯·韦伯恩.md "wikilink")(*Anton Von Webern*) -
    《弦樂四重奏》（1937年至1938年）-其[音序是以BACH動機为基础的](../Page/音序.md "wikilink")
  - [珍·寇莎德](../Page/珍·寇莎德.md "wikilink")(*Jean Coulthard*) -
    为钢琴而作的《巴赫变奏曲》（1952年）
  - [路易吉·达拉皮科拉](../Page/路易吉·达拉皮科拉.md "wikilink")(*Luigi Dallapiccola*) -
    《安娜莉贝拉的音乐札记》（1952年）
  - [阿沃·帕特](../Page/阿沃·帕特.md "wikilink")(*Arvo Pärt*) -
    为[弦乐器](../Page/弦乐器.md "wikilink")，[双簧管](../Page/双簧管.md "wikilink")，[大键琴和](../Page/大键琴.md "wikilink")[钢琴而作的](../Page/钢琴.md "wikilink")《B-A-C-H抽象贴拼画》（1964年）
  - [鲁道夫·布鲁兹](../Page/鲁道夫·布鲁兹.md "wikilink")（*Rudolf Brucci*）-
    为弦乐器而写的《B-A-C-H變奏曲》（1974年）
  - [米罗斯·索克拉](../Page/米罗斯·索克拉.md "wikilink")(*Milos Sokola*) -
    为管风琴而写的《拟托卡塔的B-A-C-H帕萨卡里亚舞》（1976年）
  - [彼德·施格勒](../Page/彼德·施格勒.md "wikilink")(*Peter Schickele*) 【以化名PDQ
    巴赫】- 《「臭脾氣」鍵盤曲集》第12首的賦格曲
  - [阿尔弗雷德·施尼特凯](../Page/阿尔弗雷德·施尼特凯.md "wikilink")(*Alfred Schnittke*) -
    《第三号大协奏曲》（1985年）
  - [朗·尼尔森](../Page/朗·尼尔森.md "wikilink")(*Ron Nelson*) -
    ''为管乐队而写的《帕萨卡里亚舞（向B-A-C-H致敬）》（1990年代）

BACH動機还出现在很多其他作品的段落之中，包括[勋伯格的](../Page/勋伯格.md "wikilink")《为乐队而作的变奏曲》（1926－28）还有他的《第三弦乐四重奏》（1927年），[克里斯托弗·潘德列茨基的](../Page/克里斯托弗·潘德列茨基.md "wikilink")《圣路加受难曲》，和[勃拉姆斯在](../Page/勃拉姆斯.md "wikilink")[贝多芬的](../Page/贝多芬.md "wikilink")《[第四钢琴协奏曲](../Page/第四钢琴协奏曲_\(贝多芬\).md "wikilink")》第一乐章中的[华彩乐段](../Page/华彩乐段.md "wikilink")。

### 其他以名字作為音樂動機的例子

  - *A、B、B、F* 表示 [阿尔班·贝尔格](../Page/阿尔班·贝尔格.md "wikilink")（**A**lban
    Maria Johannes **B**erg）和
    [汉纳·福斯—罗伯廷](../Page/汉纳·福斯—罗伯廷.md "wikilink")（**H**anna
    **F**uchs-Robettin），出现在贝尔格的 *[抒情组曲](../Page/抒情组曲.md "wikilink")*
  - *A、C、F*，俄國作曲家[里亞多夫](../Page/里亞多夫.md "wikilink") 以自己姓氏 Liadov
    的俄語串法（Ля́дов）變成音名「Ля-до-Фа」（la-do-fa，в作結尾時為/f/，即fa），並於1913年創作了一小短細的鍵盤作品《La-do-fa主題賦格曲》，A小調寫成。\[2\]
  - *B、E、B、A* 或者 *B、A、B、E*
    表示[贝拉·巴托克](../Page/贝拉·巴托克.md "wikilink")（**BÉ**la
    **BA**rtók，后者符合[匈牙利语的习惯](../Page/匈牙利语.md "wikilink")，就是姓氏放在名字之前，请见
    [东方语言里的名字顺序](../Page/Personal_name#Name_order.md "wikilink")）
  - *C、A、G、E* 表示[约翰·基治](../Page/约翰·基治.md "wikilink") (John
    **CAGE**)，[鲍林·奥立佛洛斯](../Page/鲍林·奥立佛洛斯.md "wikilink") (Pauline
    Oliveros)
    使用了这一音阶[1](http://eamusic.dartmouth.edu/~larry/dear.john/DearJohnNotes.html)
  - [''D、E{{music](../Page/DSCH動機.md "wikilink")
    代表[季米特里·萧斯塔科维奇](../Page/季米特里·萧斯塔科维奇.md "wikilink")
    (**D**. **SCH**ostakowitsch)
  - *E、C、B、B（B）、E、G* 表示[阿诺·勋伯格](../Page/阿诺·勋伯格.md "wikilink")
    (**SCH**ön**BE**r**G**)
  - *F、E（Es）、C、B（H）* 表示[舒伯特](../Page/舒伯特.md "wikilink") (**F**.
    **SCH**ubert)
  - *G、A、D、E*，丹麥作曲家[尼爾斯·加德](../Page/尼爾斯·加德.md "wikilink")，他的簽名便是以高音譜號來標示，而把整個動機當作180旋轉及換上中音譜號時，同樣可以串出相同的音高。

### 巴赫的另類創作動機

巴赫亦使用了一种以[数字](../Page/数字.md "wikilink")[密码为基础的](../Page/密码学.md "wikilink")[密码作為音樂創作的元素](../Page/密码.md "wikilink")。这种密码的规则是A=1,
B=2,
C=3等等，因此B-A-C-H的和等於14，而連同J和S的總和則是41（巴赫时代的[德语字母I与J相同](../Page/德语字母.md "wikilink")）。在几乎每个巴赫的作品中都出现过14和41（即相当于BACH和JSBACH）。例如，在第一首赋格曲，C大调赋格曲，巴赫“平均律钢琴曲集体”第一篇章中，主旋律有14节（另：整个24篇内，有22篇是完整的，第23篇是几乎完整的，第14篇没有完成），一般认为这些密码是巴赫故意放进去的。

## 參見

  - [DSCH動機](../Page/DSCH動機.md "wikilink")

## 注譯

[Category:音樂理論](../Category/音樂理論.md "wikilink")

1.  [相關樂譜 (PDF
    格式)](http://216.129.110.22/files/imglnks/usimg/2/28/IMSLP12038-Bach__Johann_Christian_Chromatic_Fugue_in_F_on_B-A-C-H__organ_.pdf)
2.