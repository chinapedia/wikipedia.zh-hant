[NDSU_Campus_Welcome.jpg](https://zh.wikipedia.org/wiki/File:NDSU_Campus_Welcome.jpg "fig:NDSU_Campus_Welcome.jpg")

**法戈**（）位於[北紅河西岸](../Page/北紅河.md "wikilink")，是[北達科他州最大的城市](../Page/北達科他州.md "wikilink")、[卡斯縣縣治](../Page/卡斯縣_\(北達科他州\).md "wikilink")。根據[美國2000年人口普查](../Page/美國2000年人口普查.md "wikilink")，人口约100.000人。

## 流行文化

  - 電影《[冰血暴](../Page/冰血暴.md "wikilink")》 （Fargo）以當地為故事背景。
  - [唐·麥克林Don McLean名作](../Page/唐·麥克林Don_McLean.md "wikilink")《[美國派
    American
    Pie](../Page/美國派_American_Pie.md "wikilink")》中的「音樂死去的那一天」指的是1959年2月3日，載著包括[巴弟·哈利等四人的飛機在前往法戈途中墜毀](../Page/巴弟·哈利.md "wikilink")，機上所有人全部罹難一事。

## 姐妹城市

  - [哈馬爾](../Page/哈馬爾.md "wikilink")

  - [維默比](../Page/維默比.md "wikilink")

## 参考文献

[Category:北達科他州城市](../Category/北達科他州城市.md "wikilink")
[Category:1871年建立的聚居地](../Category/1871年建立的聚居地.md "wikilink")
[Category:美國大學城](../Category/美國大學城.md "wikilink")