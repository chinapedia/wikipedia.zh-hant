**凱縣**（）是[美國](../Page/美國.md "wikilink")[奧克拉荷馬州北部的一個縣](../Page/奧克拉荷馬州.md "wikilink")，北鄰[堪薩斯州](../Page/堪薩斯州.md "wikilink")，面積2,448平方公里。根據[2010年人口普查](../Page/美國2010年人口普查.md "wikilink")，本縣共有人口46,562人。\[1\]本縣縣治為[紐柯克](../Page/紐柯克.md "wikilink")（）。

## 歷史

[Cherokee_Outlet_1885.jpg](https://zh.wikipedia.org/wiki/File:Cherokee_Outlet_1885.jpg "fig:Cherokee_Outlet_1885.jpg")
凱縣一帶原本是人煙稀少的地方。南北戰爭結束後，切諾基民族不得不讓聯邦政府將其他美洲原住民部落搬遷至切諾基人的領地中。於1873年7月，到達切諾基領地中的凱縣一帶定居。於1879年，[內茲佩爾塞人遷至本縣一帶](../Page/內茲佩爾塞人.md "wikilink")，但於1885年又遷回家鄉。\[2\]

位於紐柯克以北的是一間供印第安人的寄宿學校。該校在1884年開學，於1980年關閉。1950年代是該校的鼎盛時期，學校當時擁有來自126個不同印第安族的1,300名學生。\[3\]

於1893年，本縣以「K縣」的名稱成立，該名稱並沒有任何特別意思。\[4\]於1895年，本縣改為現名並成立了縣政府。本縣的現名是原本縣名「K縣」讀音的寫法。\[5\]凱縣是州中唯一一個沒有因奧克拉荷馬州的成立而改名的縣份（僅由字母「K」改為讀音「Kay」）。

## 地理

[Kay_County_Oklahoma_Courthouse_by_Smallchief.jpg](https://zh.wikipedia.org/wiki/File:Kay_County_Oklahoma_Courthouse_by_Smallchief.jpg "fig:Kay_County_Oklahoma_Courthouse_by_Smallchief.jpg")
[Veteran's_Day_parade,_Ponca_City,_Oklahoma.jpg](https://zh.wikipedia.org/wiki/File:Veteran's_Day_parade,_Ponca_City,_Oklahoma.jpg "fig:Veteran's_Day_parade,_Ponca_City,_Oklahoma.jpg")\]\]
根據[2000年人口普查](../Page/2000年美国人口普查.md "wikilink")，凱縣的總面積為，其中有，即97.2%為陸地；，即2.8%為水域。\[6\]本縣的北部邊界是奧克拉荷馬州與堪薩斯州的疆界。於1975年完工的水庫佔據了本縣大部份的水域面積。\[7\]

### 重要高速公路

  - [I-35.svg](https://zh.wikipedia.org/wiki/File:I-35.svg "fig:I-35.svg")
    [35號州際公路](../Page/35號州際公路.md "wikilink")
  - [US_60.svg](https://zh.wikipedia.org/wiki/File:US_60.svg "fig:US_60.svg")
    [美国国道60](../Page/美国国道60.md "wikilink")
  - [US_77.svg](https://zh.wikipedia.org/wiki/File:US_77.svg "fig:US_77.svg")
    [美国国道77](../Page/美国国道77.md "wikilink")
  - [US_177.svg](https://zh.wikipedia.org/wiki/File:US_177.svg "fig:US_177.svg")
    [美国国道177](../Page/美国国道177.md "wikilink")
  - [Oklahoma_State_Highway_11.svg](https://zh.wikipedia.org/wiki/File:Oklahoma_State_Highway_11.svg "fig:Oklahoma_State_Highway_11.svg")
    [俄克拉何马州州道11](../Page/俄克拉何马州州道11.md "wikilink")

### 毗鄰縣

  - [奧克拉荷馬州](../Page/奧克拉荷馬州.md "wikilink")
    [欧塞奇县](../Page/歐塞奇縣_\(奧克拉荷馬州\).md "wikilink")：东方
  - 奧克拉荷馬州 [诺布尔县](../Page/諾布爾縣_\(奧克拉荷馬州\).md "wikilink")：南方
  - 奧克拉荷馬州 [加菲爾德縣](../Page/加菲爾德縣_\(奧克拉荷馬州\).md "wikilink")：西南方
  - 奧克拉荷馬州 [格蘭特縣](../Page/格蘭特縣_\(奧克拉荷馬州\).md "wikilink")：西方
  - [堪萨斯州](../Page/堪萨斯州.md "wikilink")
    [考利县](../Page/考利县_\(堪萨斯州\).md "wikilink")：北方

## 人口

根據[2000年人口普查](../Page/2000年美國人口普查.md "wikilink")，凱縣擁有48,080居民、19,157住戶和13,141家庭。\[8\]其[人口密度為每平方英里](../Page/人口密度.md "wikilink")52居民（每平方公里20居民）。\[9\]本縣擁有21,804間房屋单位，其密度為每平方英里24間（每平方公里9間）。\[10\]而人口是由84.16%[白人](../Page/歐裔美國人.md "wikilink")、1.79%[黑人](../Page/非裔美國人.md "wikilink")、7.53%[土著](../Page/美國土著.md "wikilink")、0.53%[亞洲人](../Page/亞裔美國人.md "wikilink")、0.02%[太平洋白民](../Page/太平洋白民.md "wikilink")、1.98%其他[種族和](../Page/種族.md "wikilink")4%[混血](../Page/混血.md "wikilink")[构成](../Page/种族构成.md "wikilink")。而[西班牙裔或](../Page/西班牙裔美国人.md "wikilink")[拉丁美洲人佔了人口](../Page/拉丁美洲人.md "wikilink")4.25%。\[11\]

在19,157住户中，有31.9%擁有一個或以上的兒童（18歲以下）、54.7%為夫妻、10.2%為單親家庭、31.4%為非家庭、27.9%為獨居、13.1%住戶有同居長者。平均每戶有2.45人，而平均每個家庭則有2.99人。在48,080居民中，有26.4%為18歲以下、8.8%為18至24歲、25%為25至44歲、22.8%為45至65歲以及17%為65歲以上。人口的年齡中位數為38歲，女子對男子的性別比為100：93.7。成年人的性別比則為100：89.9。\[12\]

本縣的住戶收入中位數為$30,762，而家庭收入中位數則為$38,144。男性的收入中位數為$30,431，而女性的收入中位數則為$19,617，[人均收入為](../Page/人均收入.md "wikilink")$16,643。約12.4%家庭和16%人口在[貧窮線以下](../Page/貧窮線.md "wikilink")，包括22.7%兒童（18歲以下）及9.5%長者（65歲以上）。\[13\]

## 参考文献

[K](../Category/俄克拉何马州行政区划.md "wikilink")

1.  [2010 census data](http://2010.census.gov/2010census/data/)

2.
3.  Brumley, Kim. *Chilocco: Memories of a Native American Boarding
    School.* Fairfax, OK: Guardian Publishing Co., 2010, p. 37

4.  Wilson, Linda D. *Encyclopedia of Oklahoma History and Culture*.
    "KayCounty."

5.  Kane, J. N. and C. C. Aiken. The American Counties: Origins of
    County Names Dates of Creation and Population Data 1950-2000.
    Lanham, MD.: Scarecrow Press, 2005.

6.

7.
8.  [Population Profile of the United
    States: 2000](http://www.census.gov/population/www/pop-profile/profile2000.html)

9.  [Statistical profile of Alger County,
    Michigan](http://censtats.census.gov/data/MI/05026003.pdf) , United
    States Census Bureau, Census 2000

10. [State and County QuickFacts](http://quickfacts.census.gov/qfd/)

11. [Census 2000 gateway](http://www.census.gov/main/www/cen2000.html)

12. [American FactFinder](http://factfinder.census.gov/)

13.