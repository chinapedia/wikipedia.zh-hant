**楊佩婷**，又名**楊沛婷**（），是一位[台灣](../Page/台灣.md "wikilink")[演員](../Page/演員.md "wikilink")，為前[TVBS的經理人合約藝員](../Page/TVBS.md "wikilink")。其代表作是《[愛情合約](../Page/愛情合約.md "wikilink")》、《[惡作劇之吻](../Page/惡作劇之吻.md "wikilink")》，還有《[惡男宅急電](../Page/惡男宅急電.md "wikilink")》。\[1\]

## 家庭

家中有媽媽，爸爸，以及哥哥。

## 演出作品

### 電視劇

| 年　度      | 播出頻道                                                                                                 | 劇　名                                                            | 飾　演 | 合 作 演 員                                                                                            |
| -------- | ---------------------------------------------------------------------------------------------------- | -------------------------------------------------------------- | --- | -------------------------------------------------------------------------------------------------- |
| 2004年6月  | [TVBS-G](../Page/TVBS-G.md "wikilink")                                                               | 《[愛情合約](../Page/愛情合約.md "wikilink")》（Love Contract）            | 小　小 | [林依晨](../Page/林依晨.md "wikilink")、[賀軍翔](../Page/賀軍翔.md "wikilink")                                  |
| 2005年9月  | [TVBS-G](../Page/TVBS-G.md "wikilink")                                                               | 《[惡男宅急電](../Page/惡男宅急電.md "wikilink")》（Express Boy）            | 妙　妙 | [賀軍翔](../Page/賀軍翔.md "wikilink")、[許瑋倫](../Page/許瑋倫.md "wikilink")                                  |
| 2005年9月  | [中視](../Page/中視.md "wikilink")、[八大](../Page/八大電視台.md "wikilink")、[翡翠台](../Page/翡翠台.md "wikilink")    | 《[惡作劇之吻](../Page/惡作劇之吻.md "wikilink")》（It started with a kiss） | 林純美 | [林依晨](../Page/林依晨.md "wikilink")、[鄭元暢](../Page/鄭元暢.md "wikilink")、[汪東城](../Page/汪東城.md "wikilink") |
| 2007年12月 | [中視](../Page/中視.md "wikilink")、[八大](../Page/八大電視台.md "wikilink")、[TVB](../Page/香港無綫電視.md "wikilink") | 《[惡作劇2吻](../Page/惡作劇2吻.md "wikilink")》（They Kiss Again）        | 林純美 | [林依晨](../Page/林依晨.md "wikilink")、[鄭元暢](../Page/鄭元暢.md "wikilink")、[汪東城](../Page/汪東城.md "wikilink") |

### 廣告

  - [YAMAHA機車廣告](../Page/YAMAHA.md "wikilink")
  - [黃箭口香糖廣告](../Page/黃箭.md "wikilink")

## 參考

## 外部連結

  - [楊沛婷官方網站](http://www.tvbs.com.tw/artiste/index.asp?name=mini&blog=payting310)

[Category:台灣電視女演員](../Category/台灣電視女演員.md "wikilink")
[Pui](../Category/楊姓.md "wikilink")
[Category:台灣企業家](../Category/台灣企業家.md "wikilink")

1.  [1](http://ent.sina.com.cn/zl/bagua/blog/2016-01-14/11144386/2036655340/7964e4ec0102wi5p.shtml)