**阿姆斯特丹国家博物馆**（，或Rijksmuseum；），位于[荷兰](../Page/荷兰.md "wikilink")[阿姆斯特丹](../Page/阿姆斯特丹.md "wikilink")，是荷兰的国家博物馆及规模最大的博物馆，藏有史前至最新的各种展品。[国家博物馆研究图书馆](../Page/国家博物馆研究图书馆.md "wikilink")（Rijksmuseum
Research
Library）是该博物馆的一部分，也是荷兰最大的公共的[艺术史](../Page/艺术史.md "wikilink")[研究图书馆](../Page/研究图书馆.md "wikilink")。与海牙[莫瑞泰斯皇家美术馆和鹿特丹](../Page/莫瑞泰斯皇家美术馆.md "wikilink")[博伊曼斯·范伯宁恩美术馆并称荷兰的三大美术馆](../Page/博伊曼斯·范伯宁恩美术馆.md "wikilink")。

## 歷史

1795年[巴達維亞共和國成立時](../Page/巴達維亞共和國.md "wikilink")，時任財政大臣[亞歷山大·戈吉爾認爲應當依照法國](../Page/亞歷山大·戈吉爾.md "wikilink")[盧浮宮來建造一所國家博物館](../Page/盧浮宮.md "wikilink")，但直到1798年11月19日政府才啓動建造博物館的計劃。\[1\]\[2\]
1800年5月31日，阿姆斯特丹国家博物馆的前身國家畫廊（Nationale
Kunst-Galerij）在[海牙成立](../Page/海牙.md "wikilink")，當時藏品為來自[荷兰省督捐贈的](../Page/荷兰省督.md "wikilink")200幅畫作。\[3\]\[4\]

1806年[荷蘭王國成立](../Page/荷蘭王國.md "wikilink")，在[路易·波拿巴的命令下](../Page/路易·波拿巴.md "wikilink")，國家畫廊在1808年搬到了[阿姆斯特丹](../Page/阿姆斯特丹.md "wikilink")。包括《[夜巡](../Page/夜巡.md "wikilink")》在内的一批阿姆斯特丹政府所有的畫作進入博物館中。1809年博物館在[阿姆斯特丹王宫再次開張](../Page/阿姆斯特丹王宫.md "wikilink")。\[5\]

1817年博物館搬入[特里普家宅](../Page/特里普家宅.md "wikilink")（Trippenhuis），但最後發現這裡並不是理想的博物館建立之地。1820年，博物館部分藏品運往海牙的[莫瑞泰斯皇家美术馆](../Page/莫瑞泰斯皇家美术馆.md "wikilink")，1838年時其中的19世紀繪畫作品又被拆分出來，藏入[哈勒姆的Villa](../Page/哈勒姆.md "wikilink")
Welgelegen。\[6\]

1863年曾有一場博物館設計比賽，但所有的上交作品均未被採納。其中[皮埃尔·库贝的作品得了第二名](../Page/皮埃尔·库贝.md "wikilink")，1876年的第二次比賽中，庫貝終于以[哥特式和](../Page/哥特式建築.md "wikilink")[文藝復興式建築的混合設計贏得冠軍](../Page/文藝復興式建築.md "wikilink")。同年10月1日新博物館動工，博物館外牆的設計則是由另一批從競賽中選出的藝術家來完成的。

20世紀之後博物館經過多次翻修\[7\]\[8\]，最近的一次完成于2012年7月16日。

## 画廊

油画藏品包括画家[雅各布·范·雷斯达尔](../Page/雅各布·范·雷斯达尔.md "wikilink")，[弗兰斯·哈尔斯](../Page/弗兰斯·哈尔斯.md "wikilink")，[扬·弗美尔和](../Page/扬·弗美尔.md "wikilink")[伦勃朗以及他的学生等人的作品](../Page/伦勃朗.md "wikilink")。

<File:Afscheid> van Neerlands kunstschatbewaarder Weeknummer 59-49 -
Open Beelden - 31178.ogv|thumb|1959年的博物館 <File:Atrium> Rijksmuseum
Amsterdam 02.jpg|thumb|2013年翻修後的博物館 <File:Isaac>
Gogel.jpg|thumb|left|[亞歷山大·戈吉爾](../Page/亞歷山大·戈吉爾.md "wikilink")（Alexander
Gogel，1765–1821 <File:The> Nightwatch by Rembrandt -
Rijksmuseum.jpg|thumb|镇馆之宝《[夜巡](../Page/夜巡.md "wikilink")》，倫勃朗，1642
<File:Vermeer> - The
Milkmaid.jpg|thumb|镇馆之宝《[倒牛奶的女僕](../Page/倒牛奶的女僕.md "wikilink")》，揚·弗美爾，1658–1660

## 参考文献

## 外部链接

  - [Rijksmuseum Amsterdam](http://www.rijksmuseum.nl/) – 六种语言版本
  - [Paintings at the
    Rijksmuseum](https://web.archive.org/web/20090926211431/http://www.rijksmuseum.nl/collectie/schilderijen?lang=en)
    (英文)

[Category:1800年建立](../Category/1800年建立.md "wikilink")
[Category:荷兰美术馆](../Category/荷兰美术馆.md "wikilink")
[Category:阿姆斯特丹博物馆](../Category/阿姆斯特丹博物馆.md "wikilink")
[Category:荷兰国立博物馆](../Category/荷兰国立博物馆.md "wikilink")
[Category:欧洲年度博物馆奖获得者](../Category/欧洲年度博物馆奖获得者.md "wikilink")
[Category:歐洲地標](../Category/歐洲地標.md "wikilink")

1.
2.   Roelof van Gelder, [Schatkamer met veel
    gezichten](http://retro.nrc.nl/W2/Lab/Profiel/Rijksmuseum/schatkamer.html),
    2000. Retrieved 15 April 2013.

3.
4.
5.
6.
7.

8.