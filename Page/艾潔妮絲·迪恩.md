**艾潔妮絲·迪恩**（\[1\]，\[2\]），[英國](../Page/英國.md "wikilink")[模特兒及](../Page/模特兒.md "wikilink")[歌手](../Page/歌手.md "wikilink")，曾是著名品牌[Giorgio
Armani及](../Page/Giorgio_Armani.md "wikilink")[Burberry的寵兒](../Page/Burberry.md "wikilink")。她被喻為「時裝界的下一位[超級名模](../Page/超級名模.md "wikilink")」
\[3\]。

## 模特兒事業

[Agyness_Deyn_in_Anna_Sui_Feb_2008,_Photographed_by_Ed_Kavishe_for_Fashion_Wire_Press.jpg](https://zh.wikipedia.org/wiki/File:Agyness_Deyn_in_Anna_Sui_Feb_2008,_Photographed_by_Ed_Kavishe_for_Fashion_Wire_Press.jpg "fig:Agyness_Deyn_in_Anna_Sui_Feb_2008,_Photographed_by_Ed_Kavishe_for_Fashion_Wire_Press.jpg")的時裝秀\]\]
艾潔妮絲為近幾年崛起的超模之一，強烈的穿著特色，一頭時常變換顏色的大膽短髮與孩子氣的外貌，使她一夕爆紅。常有人以「[凱特·摩絲的接班人](../Page/凱特·摩絲.md "wikilink")」來比喻她。Henry
Holland為艾潔妮絲的兒時玩伴，Holland近來推出自己的品牌，艾潔妮絲二話不說義氣相挺，為他的品牌House of
Holland拍攝目錄，並且穿著Holland的slogan T出席各場合，造成一股slogan
T熱潮。Agyness為Burberry與[Armani的廣告常客](../Page/Armani.md "wikilink")，最近幾季的Armani廣告皆邀她代言。而與她有血緣關係的Burberry甚至找她替該牌新推出的香水「THE
BEAT」拍攝電視廣告。

## 歌手事業

艾潔妮絲於[Five O'Clock
Heroes的單曲](../Page/Five_O'Clock_Heroes.md "wikilink")〈Who〉中初次獻唱，還在他們的MV客串演出。這首單曲只被英國音樂雜誌《[NME](../Page/NME.md "wikilink")》評為2/10分\[4\]。另外，這首單曲在英國音樂排行榜排第109位\[5\]。

## 個人生活

艾潔妮絲曾與[英國搖滾樂隊](../Page/英國.md "wikilink")[The
Paddingtons成員Josh](../Page/:en:The_Paddingtons.md "wikilink")
Hubbard約會近4年\[6\]。她亦曾與[紐約搖滾樂隊](../Page/紐約.md "wikilink")[The
Strokes成員Albert](../Page/The_Strokes.md "wikilink") Hammond,
Jr.約會\[7\] 近1年但在2009年頭結束戀情。

她最近從[倫敦搬遷到紐約居住](../Page/倫敦.md "wikilink")\[8\]。

2012年6月，艾潔妮絲與美國男演員[吉奧瓦尼·瑞比西在美國洛杉磯秘密結婚](../Page/:en:Giovanni_Ribisi.md "wikilink")。於2015年離婚。

## 經紀公司

  - [Models
    1](../Page/Models_1.md "wikilink")（[倫敦](../Page/倫敦.md "wikilink")）
  - [Elite Model
    Management](../Page/Elite_Model_Management.md "wikilink")（[紐約](../Page/紐約.md "wikilink")）
  - [Why Not Model
    Agency](../Page/Why_Not_Model_Agency.md "wikilink")（[米蘭](../Page/米蘭.md "wikilink")）
  - [Oui
    Management](../Page/Oui_Management.md "wikilink")（[巴黎](../Page/巴黎.md "wikilink")）

## 參考資料

## 外部連結

  -
  -
[Category:英國女性模特兒](../Category/英國女性模特兒.md "wikilink")
[Category:英國歌手](../Category/英國歌手.md "wikilink")
[Category:英格兰女电影演员](../Category/英格兰女电影演员.md "wikilink")

1.

2.

3.

4.  [Five O Clock Heroes - Five O’Clock Heroes Feat Agyness Deyn - Track
    Reviews -
    NME.COM](http://www.nme.com/reviews/five-o-clock-heroes/9762)

5.  [Zobbel UK Chart Database](http://www.zobbel.de/)

6.
7.   The New York Observer

8.  [Agyness Deyn - Model Profile - Fashion on New York
    Magazine](http://nymag.com/fashion/models/adeyn/agynessdeyn/)