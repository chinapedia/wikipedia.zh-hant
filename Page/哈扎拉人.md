[缩略图](https://zh.wikipedia.org/wiki/File:Little_boy_listens_to_his_teacher_in_Bamyan,_Afghanistan.jpg "fig:缩略图")

**哈扎拉人**（，）是一個使用[波斯语族](../Page/波斯語.md "wikilink")[哈扎拉吉語為母語的民族](../Page/哈扎拉吉語.md "wikilink")，拥有[突厥](../Page/突厥.md "wikilink")-[蒙古人血统](../Page/蒙古人.md "wikilink")。他们的主要居住地是[阿富汗中部](../Page/阿富汗.md "wikilink")、[伊朗东北部和](../Page/伊朗.md "wikilink")[巴基斯坦西北部](../Page/巴基斯坦.md "wikilink")（主要是在[奎達](../Page/奎達.md "wikilink")）。哈扎拉族大多信奉[伊斯兰教](../Page/伊斯兰教.md "wikilink")[什叶派](../Page/什叶派.md "wikilink")，是阿富汗的人口第三多的民族，人口占全阿人口的9%。还有许多哈扎拉人以难民身份居住在伊朗以及世界各地。

## 起源

### 名称

“哈扎拉”这个名字来自波斯字语的“哈扎尔”，意思是“一千”，原指[蒙古帝国的千人单位部队](../Page/蒙古帝国.md "wikilink")。但后来这就成了一个民族的名称。\[1\]

### 人种基因

从人种学的角度讲，哈扎拉人是[黄色人种和](../Page/黄色人种.md "wikilink")[高加索人种的混血后代](../Page/高加索人种.md "wikilink")。基因研究表明他们和[蒙古族](../Page/蒙古族.md "wikilink")\[2\]以及[维吾尔族](../Page/维吾尔族.md "wikilink")、[烏茲別克族](../Page/烏茲別克族.md "wikilink")\[3\]血缘最接近。在巴基斯坦的哈扎拉人中有
41.7％的 C3（[蒙古起源](../Page/蒙古.md "wikilink")），33.3％的
R1b1b（[月氏起源](../Page/月氏.md "wikilink")），Q1 和 O3 分别为 8.3％，R2
和 J2a 分别为 4.1％。基本上可以确定 R2 和 J2a 为
迁徙到[阿富汗以后融入的部分](../Page/阿富汗.md "wikilink")，而
C3，Q1 和 O3 来自[东亚](../Page/东亚.md "wikilink")。R1b1b
则应当来自[中亚](../Page/中亚.md "wikilink")， 或者更加东面的地方。

### 起源的理论

至少有三种哈扎拉人如何形成的理论：蒙古人的后裔、当地的中亚人、以及混血。这些说法都被学者和哈扎拉人自己普遍接受。

一些哈扎拉人自认为是[成吉思汗的男性直系后代](../Page/成吉思汗.md "wikilink")。不管这是否正确，因为哈扎拉人确实有部分的蒙古人的外貌特征、语言、文化的共同之处，哈扎拉人有蒙古人的血统的说法是普遍被接受的。大约三分之二的哈扎拉男性的Y[染色体携带一种蒙古人特有的基因](../Page/染色体.md "wikilink")。同时，在阿富汗大部分其他民族都信奉伊斯兰教[逊尼派的同时](../Page/逊尼派.md "wikilink")，哈扎拉人信奉[什叶派](../Page/什叶派.md "wikilink")，这和蒙古帝国的[伊兒汗國自](../Page/伊兒汗國.md "wikilink")[完者都开始信奉什叶派不无关系](../Page/完者都.md "wikilink")。

另一种说法是哈扎拉人是当地[貴霜人的后裔](../Page/貴霜帝國.md "wikilink")\[4\]。这个在阿富汗的古代民族曾经建造了著名的[巴米扬大佛](../Page/巴米扬大佛.md "wikilink")。支持这种说法的人从巴米扬佛像和哈扎拉人的面部特征的相似之中找到证据。然而反对这种说法人给出的证据是贵霜人是印欧人种的[吐火罗](../Page/吐火罗.md "wikilink")（[月氏](../Page/月氏.md "wikilink")），并没有蒙古人种的成分，而且史书上说由于[蒙古帝国在进攻](../Page/蒙古帝国.md "wikilink")[巴米扬时候成吉思汗的一个孙子战死](../Page/巴米扬.md "wikilink")（[察合台儿子木阿禿干](../Page/察合台.md "wikilink")），以至于巴米扬被彻底屠城，当地被迁徙来的蒙古人占据。

至于蒙古人和当地人混血的起源说法，也有几个版本。有的说是[欽察汗國的蒙古人和伊朗东部的人融合](../Page/欽察汗國.md "wikilink")；有的则说是[察合台汗國的蒙古人以及伊兒汗國](../Page/察合台汗國.md "wikilink")、[帖木兒帝國等地的蒙古人后裔和当地的波斯人融合](../Page/帖木兒帝國.md "wikilink")。

遠有一說法:哈扎拉人是不同時期來到阿富汗的突厥語部落與當地人融合而成。

## 历史

自16世纪晚期，哈扎拉人在波斯[萨非王朝的史书中和](../Page/萨非王朝.md "wikilink")[印度](../Page/印度.md "wikilink")[莫卧儿帝国开国主](../Page/莫卧儿帝国.md "wikilink")[巴卑尔的回忆录中开始出现](../Page/巴卑尔.md "wikilink")，被描述为居住在[喀布尔以西到](../Page/喀布尔.md "wikilink")[古爾省](../Page/古爾省.md "wikilink")，南到[加兹尼省这个地区](../Page/加兹尼省.md "wikilink")。

自从18世纪中叶现代阿富汗国家形成，哈扎拉人就屡屡受到阿富汗最大的民族[普什图族的迫害](../Page/普什图族.md "wikilink")，很多人从各个地方被迫逃到阿富汗中部山区\[5\]。这个地区现在被叫做“[哈扎拉贾特](../Page/哈扎拉贾特.md "wikilink")”。哈扎拉贾特山区以[巴米扬为中心](../Page/巴米扬.md "wikilink")，横跨阿富汗的多个省份，占阿富汗领土的三成。

在多斯特·穆罕默德·汗统治的[巴拉克宰王朝里](../Page/巴拉克宰王朝.md "wikilink")，居住在哈扎拉贾特的哈扎拉人被课以重税。但这段时期哈扎拉人还得以部分的地方自治。到了他的孙子[阿布杜爾·拉赫曼汗任](../Page/阿布杜爾·拉赫曼汗.md "wikilink")[埃米尔的时期](../Page/埃米尔.md "wikilink")，由于一些哈扎拉支持阿卜杜尔·拉赫曼的叔父，被阿卜杜尔·拉赫曼用军事手段镇压。在1888年和1892年哈扎拉人两次反叛，被配有[大英帝国军事顾问的阿卜杜尔](../Page/大英帝国.md "wikilink")·拉赫曼的大军镇压，并遭到血腥屠杀。1893年哈扎拉人再次反叛，并出其不意收复了哈扎拉贾特的大部分地方，但因弹尽粮绝最终再次被阿卜杜尔·拉赫曼镇压。这段时期，半数的哈扎拉人被杀死，很多哈扎拉人的财产被普什图人抢走，以至于被迫流落在各个大城市内寻找谋生之路。阿卜杜尔·拉赫曼对哈扎拉人的残酷统治造成了后世哈扎拉人和普什图人之间长期的民族仇恨。

多斯特·穆罕默德儿子哈比卜拉·汗在1901年继位，大赦所有被迫害的人。但长期的民族隔离政策造成哈扎拉人几乎在整个20世纪都被歧视\[6\]。

1940年在[穆罕默德·查希爾·沙阿的统治下](../Page/穆罕默德·查希爾·沙阿.md "wikilink")，阿富汗政府对哈扎拉人加税，同时普什图人免税并可以从政府拿到补助，这使得哈扎拉人再次反叛。在政府镇压了这个反叛之后，去掉了这个新税。

1979年[苏联占领阿富汗之后](../Page/阿富汗战争_\(1979年\).md "wikilink")，哈扎拉人没有像其他民族一样强烈抵抗，而是自己内部各派互相争斗。最终受到伊朗支持的教会武装战胜了世俗武装，取得了哈扎拉贾特的控制权。在苏军撤退之后，哈扎拉人分成两派，分别支持和反对拉巴尼政府。在1996年[塔利班占领喀布尔之后](../Page/塔利班.md "wikilink")，哈扎拉人团结一致，和[北方联盟联合](../Page/北方聯盟_\(阿富汗\).md "wikilink")。但在1998年[哈扎拉贾特被塔利班占领](../Page/哈扎拉贾特.md "wikilink")，大量哈扎拉人被普什图人为主的塔利班杀害。

[Sima_Samar.jpg](https://zh.wikipedia.org/wiki/File:Sima_Samar.jpg "fig:Sima_Samar.jpg")
2001年[美国遭到](../Page/美国.md "wikilink")[九一一袭击事件之后](../Page/九一一袭击事件.md "wikilink")，因为认为塔利班庇护[基地组织](../Page/基地组织.md "wikilink")，美国出兵阿富汗，打败了塔利班。哈扎拉人得到前所未有的权利，包括接受高等教育、参军等\[7\]。哈扎拉人哈吉·默罕默德·马哈吉吉还参加了2004年的阿富汗总统选举。

## 语言

哈扎拉人使用[哈扎拉吉語](../Page/哈扎拉吉語.md "wikilink")，这是一种接近于阿富汗人普遍使用的[达里语的波斯语方言](../Page/达利语.md "wikilink")。和其他波斯语方言相比，哈扎拉语有很多[蒙古语和](../Page/蒙古语.md "wikilink")[突厥语的词汇](../Page/突厥语.md "wikilink")。尤其是[戴孔迪省等地的哈扎拉语有大量的蒙古语影响](../Page/戴孔迪省.md "wikilink")。而在[喀布尔和](../Page/喀布尔.md "wikilink")[马扎里沙里夫的哈扎拉人则很少使用哈扎拉语](../Page/马扎里沙里夫.md "wikilink")，而是操[喀布尔方言](../Page/喀布尔方言.md "wikilink")。在西部[赫拉特等地的哈扎拉人则是使用](../Page/赫拉特.md "wikilink")[科拉撒尼语](../Page/科拉撒尼语.md "wikilink")。在巴基斯坦的哈扎拉人经常使用[乌尔都语和](../Page/乌尔都语.md "wikilink")[英语](../Page/英语.md "wikilink")。

## 宗教

哈扎拉人为[穆斯林](../Page/穆斯林.md "wikilink")，大多数伊斯兰教[什叶派](../Page/什叶派.md "wikilink")（[十二伊玛目派和](../Page/十二伊玛目派.md "wikilink")[伊斯玛仪派](../Page/伊斯玛仪派.md "wikilink")）。但也有少量[逊尼派](../Page/逊尼派.md "wikilink")。

## 人口

哈扎拉人的总数大约有350万到400万，其中在[阿富汗有](../Page/阿富汗.md "wikilink")287万，[伊朗有](../Page/伊朗.md "wikilink")28万（不包括近年的难民），[巴基斯坦](../Page/巴基斯坦.md "wikilink")18万。此外在澳大利亞、新西蘭、加拿大、美國、英國等西方国家，特別是北歐國家，如瑞典和丹麥，有顯著的哈扎拉人社區。也有去蒙古國讀書的哈扎拉人。

## 其他

阿富汗旅美作家[卡勒德·胡賽尼的小说](../Page/卡勒德·胡賽尼.md "wikilink")《[追風箏的孩子](../Page/追風箏的孩子.md "wikilink")》通过主人公少年时期和一个哈扎拉少年的经历，反映了阿富汗1970年代以来的变迁和哈扎拉人被歧视的境遇。根据这部小说改编的同名电影在2007年上映，并获得多项大奖。

哈扎拉人Abbas Alizada
因为形象酷似美籍华人影星[李小龙而被人称为](../Page/李小龙.md "wikilink")“阿富汗的李小龙”\[8\]

## 注釋

## 参考文献

## 延伸閱讀

  -
  -
  -
  -
  -
  -
  -
  -
## 外部連結

  - [Hazara tribal
    structure](http://www.nps.edu/Programs/CCs/Docs/Tribal%20Trees/Hazara.pdf),
    Program for Culture and Conflict Studies, US Naval Postgraduate
    School
  - ["The Outsiders: Afghanistan's
    Hazaras"](http://ngm.nationalgeographic.com/ngm/2008-02/afghanistan-hazara/phil-zabriskie-text.html)—*[National
    Geographic](../Page/国家地理_\(杂志\).md "wikilink")*
  - [Peril and Persecution in
    Afghanistan](https://foreignpolicy.com/2015/08/27/peril-and-persecution-in-afghanistan/)
    – Foreign Policy (2015 situation of Hazaras in Hazarajat)

[ml:ഹസാര ജനത](../Page/ml:ഹസാര_ജനത.md "wikilink")

[哈扎拉族](../Category/哈扎拉族.md "wikilink")
[Category:蒙古亲缘民族](../Category/蒙古亲缘民族.md "wikilink")

1.

2.  Genetics: Analysis Of Genes And Genomes By Daniel L. Hartl,
    Elizabeth W. Jones, pg. 309

3.  Rosenberg, Noah A. *et al.* (2002年12月) "Genetic Structure of Human
    Populations" *Science (New Series)* 298(5602): pp. 2381-2385

4.  [A Profile On Bamyan
    Civilization.](http://www.hazara.net/hazara/geography/Buddha/buddha.html)


5.

6.
7.

8.  [Meet Afghanistan's Bruce
    Lee](http://www.bbc.com/news/world-asia-30399187),
    [BBC](../Page/BBC.md "wikilink")，2014年12月1日