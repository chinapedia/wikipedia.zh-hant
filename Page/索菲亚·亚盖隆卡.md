[Zofia_Jagiellonka_2.jpg](https://zh.wikipedia.org/wiki/File:Zofia_Jagiellonka_2.jpg "fig:Zofia_Jagiellonka_2.jpg")
**索菲亚·亚盖隆卡**（，生于[克拉科夫](../Page/克拉科夫.md "wikilink")，卒于[安斯巴赫](../Page/安斯巴赫.md "wikilink")），是波兰女王，立陶宛公主，[安斯巴赫及](../Page/安斯巴赫.md "wikilink")[拜罗伊特藩侯夫人](../Page/拜罗伊特.md "wikilink")，[卡齐米日四世之女](../Page/卡齐米日四世.md "wikilink")。1479年2月14日她嫁给了[腓特烈二世](../Page/腓特烈二世_\(勃兰登堡-安斯巴赫\).md "wikilink")，[阿尔布雷希特·霍亨索伦是他们的儿子](../Page/阿爾布雷希特_\(普魯士\).md "wikilink")。

## 身世及童年

索菲亚是[卡齐米日四世和](../Page/卡齐米日四世.md "wikilink")[伊丽莎白的第六个孩子和第二个女儿](../Page/奥地利的伊丽莎白.md "wikilink")。她的名字来自她的祖母、[瓦迪斯瓦夫二世·亚盖洛的遗孀](../Page/瓦迪斯瓦夫二世·亚盖洛.md "wikilink")[索菲·霍尔赞斯卡](../Page/索菲·霍尔赞斯卡.md "wikilink")。她的兄弟有[瓦迪斯瓦夫二世·亚盖隆希克](../Page/瓦迪斯瓦夫二世·亚盖隆希克.md "wikilink")、、[扬一世·奥尔布拉赫特](../Page/扬一世·奥尔布拉赫特.md "wikilink")、[亚历山大·亚盖隆希克](../Page/亚历山大·亚盖隆希克.md "wikilink")、[日格蒙特一世·斯塔雷以及](../Page/齐格蒙特一世.md "wikilink")；姐妹有、埃尔别塔（I）、埃尔别塔（II）、[安娜](../Page/安娜·亚盖隆卡.md "wikilink")
、芭芭拉、埃尔别塔（III）。

关于索菲亚的教育和成长，没有资料保存下来。

## 联姻計畫

1468年，一个与[腓特烈三世之子](../Page/腓特烈三世_\(神圣罗马帝国\).md "wikilink")[马克西米立安一世缔结婚姻的计划出炉](../Page/马克西米连一世_\(神圣罗马帝国\).md "wikilink")。由于该提议，匈牙利王[马加什一世的使节奥洛莫乌茨的普罗塔日](../Page/马加什一世.md "wikilink")1468年4月8日立即来到了[克拉科夫](../Page/克拉科夫.md "wikilink")。他主要是为[卡齐米日四世的长女](../Page/卡齐米日四世.md "wikilink")[亚格维姬而来](../Page/亚格维加·亚盖隆卡.md "wikilink")。但索菲亚必需嫁给马克西米连。所以卡齊米日只好做出安排，将两个女儿於未來分别嫁給匈牙利与奥地利人。

但是，波蘭王室與奧、匈兩國的聯姻計畫，不久就雙雙破裂了。當卡齊米日與匈牙利王[马加什一世的主要敵手](../Page/马加什一世.md "wikilink")──波希米亞國王[波傑布拉德的伊日結盟之後](../Page/波傑布拉德的伊日.md "wikilink")，波蘭-匈牙利的聯姻自然破裂；而波蘭-奧地利的聯姻之後也因為某種原因而破裂。

## 正式結婚

1479年2月14日她正式嫁给了[勃兰登堡-安斯巴赫藩侯](../Page/勃兰登堡-安斯巴赫藩侯.md "wikilink")[阿爾布雷希特三世的次子](../Page/阿爾布雷希特三世_\(布蘭登堡\).md "wikilink")[腓特烈二世](../Page/腓特烈二世_\(勃兰登堡-安斯巴赫\).md "wikilink")，婚禮在[法蘭克福舉行](../Page/法蘭克福.md "wikilink")，這是一場低調、不鋪張的婚宴，大部分伴隨索菲亞到法蘭克福的波蘭大小貴族、神職人員，都沒收到婚宴的禮品。

婚禮完成之後，卻出現女方未能支付嫁妝的窘境。本來根據婚約，波蘭國王卡齊米日應該在年底支付六千盾給男方，當作總數三萬兩千盾嫁妝的頭期款，但因卡齊米日未能履行約定，只好請求延期付款，這也獲得男方家長的同意。

## 藩侯夫人

當她的丈夫在1486年繼位為安斯巴赫藩侯之後，索菲亞也在同時成為藩侯夫人。關於她娘家欠下的龐大嫁妝費，雖然她的哥哥──[波希米亞國王](../Page/波希米亞國王.md "wikilink")[瓦迪斯瓦夫二世·亚盖隆希克](../Page/瓦迪斯瓦夫二世·亚盖隆希克.md "wikilink")（1490年兼任[匈牙利國王](../Page/匈牙利國王.md "wikilink")）於1489年承諾會付清欠款，但事實上屢經催促仍未付清。一直要等到索菲亞夫妻都過世之後，最後的一筆欠款才終於交到兩人的子女手上。

1512年10月5日，她死在[安斯巴赫](../Page/安斯巴赫.md "wikilink")。

## 子女

## 參考資料

  - Baczkowski K., Polish history of late-medieval, Krakow 1999, ISBN
    83-85719-40-7, P. 230, 236, 249
  - Duczmal M., Jagiellons. Biographical Lexicon, Poznan-Krakow 1996,
    ISBN 83-08-02577-3, Pp 495–507.
  - Dworzaczek W., Genealogy, Warsaw, 1959.
  - Rudzki E. Polish queens, V. 1, Warsaw 1990, p. 134, 141, 148, 153,
    154
  - Wdowiszewski Z., Genealogy of the House of Vasa and Jagiello of
    Poland, Krakow 2005, p. 110-112.

[Category:波兰君主](../Category/波兰君主.md "wikilink")
[Category:立陶宛贵族](../Category/立陶宛贵族.md "wikilink")